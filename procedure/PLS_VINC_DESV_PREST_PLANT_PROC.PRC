create or replace
procedure pls_vinc_desv_prest_plant_proc(	nr_seq_lote_prest_p		number,
					nr_seq_conta_proc_prest_p		number,
					ie_opcao_p			varchar2,
					nm_usuario_p			varchar2) is 
 
cd_medico_exec_novo_w		varchar2(4000);
vl_plantonista_w			number(15,2);
vl_procedimento_ant_w		number(15,2);
vl_procedimento_novo_w		number(15,2);
nr_seq_prest_plant_w		number(15);
nr_seq_conta_proc_w		number(15);
nr_seq_competencia_w		number(15);
dt_mes_competencia_w		date;	


cd_area_procedimento_w		number(15);
cd_especialidade_w		number(15);
cd_grupo_proc_w			number(15);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(15);
nr_seq_prestador_prot_w		number(15);	

ie_tipo_tabela_w			varchar2(4000);
cd_categoria_w			varchar2(4000);
ie_tipo_contratacao_w		varchar2(4000);
cd_medico_conta_w		varchar2(4000);
ie_conta_interacao_w		varchar2(4000);
ie_tecnica_utilizada_w		varchar2(4000);
ie_tipo_guia_w			varchar2(4000);
ie_tipo_intercambio_w		varchar2(4000);
sg_estado_int_w			pessoa_juridica.sg_estado%type;
ie_carater_internacao_w		varchar2(4000);
ie_preco_informado_w		varchar2(4000);
cd_porte_anestesico_w		varchar2(4000);
ds_retorno_w			varchar2(4000);
ie_preco_plano_w		varchar2(4000);
ie_tipo_protocolo_w		varchar2(4000);
cd_cgc_w			varchar2(4000);
ie_origem_conta_w		varchar2(4000);
sg_estado_w			pessoa_juridica.sg_estado%type;
cd_estabelecimento_w		varchar2(4000);
vl_procedimento_w		number(15,2);
vl_custo_operacional_w		number(15,2);
vl_anestesista_w		number(15,2);
vl_medico_w			number(15,2);
vl_filme_w			number(15,2);
vl_auxiliares_w			number(15,2);
vl_saldo_plant_w		number(15,2);
nr_seq_conta_w			number(15);
nr_seq_congenere_w		number(15);
nr_seq_protocolo_w		number(15);
nr_seq_categoria_w		number(15);
nr_seq_tipo_acomodacao_w	number(15);
nr_seq_tipo_atendimento_w	number(15);
nr_seq_clinica_w		number(15);
nr_seq_plano_w			number(15);
nr_seq_contrato_w		number(15);					
cd_convenio_w			number(15);
qt_dias_internacao_w		number(15);
nr_seq_segurado_w		number(15);
nr_seq_intercambio_w		number(15);
cd_procedimento_conta_w		number(15);
ie_origem_proced_conta_w	number(15);
nr_seq_classificacao_prest_w	number(15);
nr_seq_cbo_saude_w		number(15);
nr_seq_regra_w			number(15);
cd_edicao_amb_w			number(15);
nr_aux_regra_w			number(15);
nr_seq_regra_autogerado_w	number(15);
nr_seq_lote_w			number(15);
nr_seq_prest_plant_item_w	number(15);
dt_procedimento_w		date;					
dt_preco_w			date;	
ie_tipo_acomodacao_ptu_w	Varchar2(2);
nr_seq_congenere_outorg_w	pls_segurado.nr_seq_congenere%type;
nr_seq_congenere_seg_w		pls_segurado.nr_seq_congenere%type;	
nr_seq_rp_combinada_w		pls_rp_cta_combinada.nr_sequencia%type;
cd_moeda_autogerado_w		Number(3);
dados_prestador_prot_w		pls_cta_valorizacao_pck.dados_prestador_prot;
vl_ch_honorarios_w		cotacao_moeda.vl_cotacao%type;
vl_ch_custo_oper_w		cotacao_moeda.vl_cotacao%type;
vl_ch_custo_filme_w		cotacao_moeda.vl_cotacao%type;
vl_ch_anestesista_w		cotacao_moeda.vl_cotacao%type;
dados_regra_preco_proc_w	pls_cta_valorizacao_pck.dados_regra_preco_proc;
nm_prestador_w			pessoa_juridica.nm_fantasia%type;
dados_conta_w			pls_cta_valorizacao_pck.dados_conta;
nr_seq_guia_w			pls_guia_plano.nr_sequencia%type;
dt_atendimento_referencia_w	date;
begin

if	(nr_seq_lote_prest_p is not null) and
	(nr_seq_conta_proc_prest_p is not null) then	
	
	select	a.nr_seq_prest_plant,
		pls_obter_dados_prestador(a.nr_seq_prest_plant, 'N') nm_prestador,
		a.cd_medico_executor,
		a.vl_plantonista,
		a.nr_seq_lote,
		a.nr_sequencia
	into	nr_seq_prest_plant_w,
		nm_prestador_w, 
		cd_medico_exec_novo_w,
		vl_plantonista_w,
		nr_seq_lote_w,
		nr_seq_prest_plant_item_w
	from	pls_lote_prest_plant a
	where	a.nr_sequencia = nr_seq_lote_prest_p;
	
	select	a.vl_procedimento_ant,
		a.vl_procedimento_novo,
		a.nr_seq_conta_proc
	into	vl_procedimento_ant_w,
		vl_procedimento_novo_w,
		nr_seq_conta_proc_w
	from	pls_conta_proc_plant a
	where	a.nr_sequencia = nr_seq_conta_proc_prest_p;
		
	if	(ie_opcao_p IN ('V','VA')) then -- VA = Vinculacao automatica, executada atraves da procedure PLS_VINCULAR_PREST_PLANT_AUT.
		select	nr_seq_conta
		into	nr_seq_conta_w
		from	pls_conta_proc
		where	nr_sequencia = nr_seq_conta_proc_w;
		
		/* Obter dados da conta */
		select	nr_sequencia,
			nr_seq_protocolo,
			nvl(nr_seq_tipo_acomodacao,0),
			nvl(nr_seq_tipo_atendimento,0),
			nr_seq_clinica,
			nr_seq_segurado,
			ie_tipo_guia,
			cd_medico_executor,
			trunc(nvl(dt_alta, sysdate) - dt_entrada) qt_dias_internacao,
			ie_origem_conta,
			ie_carater_internacao,
			substr(pls_obter_se_internado(nr_sequencia,'X'),1,1),
			nr_seq_cbo_saude,
			cd_estabelecimento,
			ie_tipo_acomodacao_ptu,
			ie_tipo_consulta,
			nr_seq_guia,
			dt_atendimento_referencia,
			nr_seq_tipo_conta
		into	nr_seq_conta_w,
			nr_seq_protocolo_w,
			nr_seq_tipo_acomodacao_w,
			nr_seq_tipo_atendimento_w,
			nr_seq_clinica_w,
			nr_seq_segurado_w,
			ie_tipo_guia_w,
			cd_medico_conta_w,
			qt_dias_internacao_w,
			ie_origem_conta_w,
			ie_carater_internacao_w,
			ie_conta_interacao_w,
			nr_seq_cbo_saude_w,
			cd_estabelecimento_w,
			ie_tipo_acomodacao_ptu_w,
			dados_conta_w.ie_tipo_consulta,
			nr_seq_guia_w,
			dt_atendimento_referencia_w,
			dados_conta_w.nr_seq_tipo_conta
		from	pls_conta 
		where	nr_sequencia	= nr_seq_conta_w;

		/* Obter dados protocolo */
		select	nvl(ie_tipo_guia_w, ie_tipo_guia),
			ie_tipo_protocolo,
			nr_seq_congenere,
			nr_seq_prestador
		into	ie_tipo_guia_w,
			ie_tipo_protocolo_w,
			nr_seq_congenere_w,
			dados_prestador_prot_w.nr_seq_prestador
		from	pls_protocolo_conta
		where	nr_sequencia	= nr_seq_protocolo_w;

		if	(dados_prestador_prot_w.nr_seq_prestador	is not null) then
			select	max(nr_seq_tipo_prestador),
				max(nr_seq_classificacao),
				max(ie_tipo_vinculo),
				max(cd_prestador)
			into	dados_prestador_prot_w.nr_seq_tipo_prestador,
				dados_prestador_prot_w.nr_seq_classificacao,
				dados_prestador_prot_w.ie_tipo_vinculo,
				dados_prestador_prot_w.cd_prestador
			from	pls_prestador
			where	nr_sequencia	= dados_prestador_prot_w.nr_seq_prestador;
		end if;

		dt_preco_w	:= pls_obter_data_preco_item(nr_seq_conta_proc_w, 'P');

		/* Obter a categoria do tipo de acomodacao */
		if	(nr_seq_tipo_acomodacao_w is not null) then
			select	max(nr_seq_categoria)
			into	nr_seq_categoria_w
			from	pls_regra_categoria
			where	nr_seq_tipo_acomodacao	= nr_seq_tipo_acomodacao_w;
		end if;

		/* Obter dados segurado */
		if	(nr_seq_segurado_w is not null) then

			--select	nvl(nr_seq_plano,0),
			select	nvl(pls_obter_produto_benef(nr_sequencia,dt_atendimento_referencia_w),0),
				nvl(nr_seq_contrato,0),
				pls_obter_conv_cat_segurado(nr_sequencia, 1),
				pls_obter_conv_cat_segurado(nr_sequencia, 2),
				nr_seq_intercambio,
				nr_seq_congenere
			into	nr_seq_plano_w,
				nr_seq_contrato_w,
				cd_convenio_w,
				cd_categoria_w,
				nr_seq_intercambio_w,
				nr_seq_congenere_seg_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_segurado_w;
		end if;

		select	nvl(max(b.nr_sequencia),0)
		into	nr_seq_congenere_outorg_w
		from	pls_congenere b,
			pls_outorgante a
		where	a.cd_cgc_outorgante	= b.cd_cgc
		and	a.cd_estabelecimento	= cd_estabelecimento_w;
		
		if	(nr_seq_congenere_seg_w is not null) and
			(nr_seq_congenere_seg_w <> nr_seq_congenere_outorg_w) then
			nr_seq_congenere_w	:= nr_seq_congenere_seg_w;
		end if;
	
		/* Obter dados do plano */
		if	(nr_seq_plano_w is not null) then
			select	max(ie_preco),
				max(ie_tipo_contratacao)
			into	ie_preco_plano_w,
				ie_tipo_contratacao_w
			from	pls_plano
			where	nr_sequencia	= nr_seq_plano_w;
		end if;

		/* Obter tipo tabela */
		if	(ie_tipo_protocolo_w	= 'R') then
			ie_tipo_tabela_w	:= 'R';
		elsif	(ie_tipo_protocolo_w	= 'I') then
			if	(ie_origem_conta_w	= 'A') then
				ie_tipo_tabela_w	:= 'IP';
			else
				ie_tipo_tabela_w	:= 'P';
			end if;
		else
			ie_tipo_tabela_w	:= 'P';
		end if;

		/* Obter dados do prestador */			
		select	nr_seq_classificacao,
			nvl(cd_cgc,'')
		into	nr_seq_classificacao_prest_w,
			cd_cgc_w
		from	pls_prestador
		where	nr_sequencia = nr_seq_prest_plant_w;

		/* Obter dados do procedimento */
		select	cd_procedimento,
			ie_origem_proced,
			nvl(dt_procedimento, sysdate),
			ie_tecnica_utilizada,
			nr_seq_rp_combinada
		into	cd_procedimento_conta_w,
			ie_origem_proced_conta_w,
			dt_procedimento_w,
			ie_tecnica_utilizada_w,
			nr_seq_rp_combinada_w
		from	pls_conta_proc
		where	nr_sequencia	= nr_seq_conta_proc_w;

		/* Obter a UF da operadora  - Tasy*/
		select	nvl(max(sg_estado),'X')
		into	sg_estado_w
		from	pessoa_juridica
		where	cd_cgc	= (	select	max(cd_cgc_outorgante)
					from	pls_outorgante
					where	cd_estabelecimento	= cd_estabelecimento_w);

		/* Obter a UF da operadora do beneficiario eventual ou que enviou o protocolo*/
		select	nvl(max(a.sg_estado),'X')
		into	sg_estado_int_w
		from	pessoa_juridica	a,
			pls_congenere	b
		where	a.cd_cgc	= b.cd_cgc
		and	b.nr_sequencia	= nr_seq_congenere_w;

		if	(sg_estado_w <> 'X') and
			(sg_estado_int_w <> 'X') then
			if	(sg_estado_w	= sg_estado_int_w) then
				ie_tipo_intercambio_w	:= 'E';
			else
				ie_tipo_intercambio_w	:= 'N';
			end if;
		else
			ie_tipo_intercambio_w	:= 'A';
		end if;
		
		if	(nr_seq_guia_w	is not null) then
			select	max(ie_pcmso)
			into	dados_conta_w.ie_atend_pcmso
			from	pls_guia_plano
			where	nr_sequencia	= nr_seq_guia_w;
		end if;
					
		pls_define_preco_proc(cd_estabelecimento_w, nr_seq_prest_plant_w, nr_seq_categoria_w,
				dt_preco_w, null, cd_procedimento_conta_w,
				ie_origem_proced_conta_w, nr_seq_tipo_acomodacao_w, nr_seq_tipo_atendimento_w,
				nr_seq_clinica_w, nr_seq_plano_w, ie_tipo_tabela_w,
				nr_seq_contrato_w, 0, nr_seq_conta_proc_w,
				'S', cd_convenio_w, cd_categoria_w,
				ie_tipo_contratacao_w, qt_dias_internacao_w, nr_seq_segurado_w,
				nr_seq_intercambio_w, '', nr_seq_classificacao_prest_w,
				nvl(cd_medico_exec_novo_w,cd_medico_conta_w), ie_conta_interacao_w, ie_tecnica_utilizada_w,
				ie_tipo_guia_w, ie_tipo_intercambio_w, sg_estado_int_w,
				nr_seq_cbo_saude_w, ie_carater_internacao_w, dt_procedimento_w, 
				ie_tipo_acomodacao_ptu_w, nr_seq_rp_combinada_w, dados_prestador_prot_w,
				null, dados_conta_w, dados_regra_preco_proc_w);			
		
		vl_procedimento_novo_w		:= dados_regra_preco_proc_w.vl_procedimento;
		vl_custo_operacional_w		:= dados_regra_preco_proc_w.vl_custo_operacional;
		vl_anestesista_w		:= dados_regra_preco_proc_w.vl_anestesista;
		vl_medico_w			:= dados_regra_preco_proc_w.vl_medico;
		vl_filme_w			:= dados_regra_preco_proc_w.vl_filme; 
		vl_auxiliares_w			:= dados_regra_preco_proc_w.vl_auxiliares;
		nr_seq_regra_w			:= dados_regra_preco_proc_w.nr_sequencia;
		cd_edicao_amb_w			:= dados_regra_preco_proc_w.cd_edicao_amb; 
		ie_preco_informado_w		:= dados_regra_preco_proc_w.ie_valor_informado;
		cd_porte_anestesico_w		:= dados_regra_preco_proc_w.cd_porte_anestesico;
		nr_aux_regra_w			:= dados_regra_preco_proc_w.nr_auxiliares;
		nr_seq_regra_autogerado_w	:= dados_regra_preco_proc_w.nr_seq_regra_autogerado;
		cd_moeda_autogerado_w		:= dados_regra_preco_proc_w.cd_moeda_autogerado; 
		vl_ch_honorarios_w		:= dados_regra_preco_proc_w.vl_ch_honorarios; 
		vl_ch_custo_oper_w		:= dados_regra_preco_proc_w.vl_ch_custo_oper; 
		vl_ch_custo_filme_w		:= dados_regra_preco_proc_w.vl_ch_custo_filme; 
		vl_ch_anestesista_w		:= dados_regra_preco_proc_w.vl_ch_anestesista;
				
		select	a.vl_saldo_plantonista
		into	vl_saldo_plant_w
		from	pls_lote_prest_plant a
		where	a.nr_sequencia 		= nr_seq_lote_prest_p;	
		
		if	(vl_procedimento_novo_w is null) and
			(vl_plantonista_w < vl_procedimento_ant_w) and
			(ie_opcao_p = 'V')  then
			
			-- A soma do valor deste procedimento com o saldo do plantonista, supera o valor do plantonista.
			wheb_mensagem_pck.exibir_mensagem_abort(195414);			
			
		elsif	(vl_plantonista_w < vl_procedimento_novo_w) and
			(ie_opcao_p = 'V')  then
			
			wheb_mensagem_pck.exibir_mensagem_abort(195414);
			
		elsif	(vl_plantonista_w = 0) and
			(vl_procedimento_novo_w > 0) and
			(ie_opcao_p = 'V')  then
			
			wheb_mensagem_pck.exibir_mensagem_abort(195414);
			
		elsif	(vl_procedimento_novo_w = 0) then
		
			-- Nao sera possivel vincular os dados ao prestador #@NR_SEQ_PRESTADOR#@ - #@NM_PRESTADOR#@ pois ha procedimentos com valor zerado na conta #@NR_SEQ_CONTA#@. 
			wheb_mensagem_pck.exibir_mensagem_abort(220302, 'NR_SEQ_CONTA=' || nr_seq_conta_w || ';' ||
									'NR_SEQ_PRESTADOR=' || nr_seq_prest_plant_w || ';' ||
									'NM_PRESTADOR=' || nm_prestador_w || ';');
			
		elsif	(vl_saldo_plant_w < nvl(vl_procedimento_novo_w,vl_procedimento_ant_w)) or
			(vl_saldo_plant_w <= 0) or
			(vl_procedimento_ant_w = 0) then
			null;
		else
			update	pls_lote_prest_plant
			set	vl_saldo_plantonista 	= nvl(vl_saldo_plantonista,0) - nvl(vl_procedimento_novo_w,vl_procedimento_ant_w),
				vl_vinculado		= nvl(vl_vinculado,0) + nvl(vl_procedimento_novo_w,vl_procedimento_ant_w)
			where	nr_sequencia 		= nr_seq_lote_prest_p;	
				
			update	pls_conta_proc_plant
			set	nr_seq_prestador_novo 	= nr_seq_prest_plant_w,
				cd_medico_exec_novo	= cd_medico_exec_novo_w,
				vl_procedimento_novo	= vl_procedimento_novo_w,
				nr_seq_regra_novo	= nr_seq_regra_w,
				nr_seq_prest_plant_item = nr_seq_prest_plant_item_w
			where	nr_sequencia 		= nr_seq_conta_proc_prest_p;
		end if;
	elsif	(ie_opcao_p = 'D') then
		update	pls_lote_prest_plant
		set	vl_saldo_plantonista 	= nvl(vl_saldo_plantonista,0) + nvl(vl_procedimento_novo_w,vl_procedimento_ant_w),
			vl_vinculado		= nvl(vl_vinculado,0) - nvl(vl_procedimento_novo_w,vl_procedimento_ant_w)
		where	nr_sequencia 		= nr_seq_lote_prest_p;		
		
		update	pls_conta_proc_plant
		set	nr_seq_prestador_novo 	= null,
			cd_medico_exec_novo	= null,
			vl_procedimento_novo	= null,
			nr_seq_regra_novo	= null,
			nr_seq_prest_plant_item = null
		where	nr_sequencia 		= nr_seq_conta_proc_prest_p;
	end if;
	
	commit;
end if;

end pls_vinc_desv_prest_plant_proc;
/