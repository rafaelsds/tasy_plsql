create or replace
procedure ATIVIA_GERAR_COBR_ITAU_400(	nr_seq_cobr_escrit_p		number,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2,
					nr_seq_grupo_inst_p		number) is 
	
ds_conteudo_w			varchar2(400)	:= null;
nr_seq_reg_arquivo_w		number(10)	:= 1;
cd_conta_cobr_w			varchar2(5);
cd_agencia_cobr_w		varchar2(4);
nm_empresa_w			varchar2(30);
dt_geracao_w			varchar2(6);
ie_digito_conta_cobr_w		varchar2(1);
cd_banco_w			number(3);
nr_seq_carteira_cobr_w		number(10);
nr_carteira_w			varchar2(3);

/* detalhe */
ie_tipo_inscricao_w			varchar2(2);
nr_inscricao_empresa_w		varchar2(14);
cd_agencia_bancaria_w		varchar2(4);
cd_conta_w			varchar2(6);
nr_nosso_numero_w		varchar2(8);
vl_multa_w			number(15,2);
nr_titulo_w			number(10);
dt_vencimento_w			date;
vl_titulo_w			number(15,2);
dt_emissao_w			date;
vl_juros_w			number(15,2);
vl_desconto_w			number(15,2);
nm_pessoa_w			varchar2(40);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(12);
cd_cep_w			varchar2(8);
ds_cidade_w			varchar2(15);
sg_estado_w			varchar2(15);
tx_juros_w			number(7,4);
tx_multa_w			number(7,4);
ds_tipo_juros_w			varchar2(255);
ds_tipo_multa_w			varchar2(255);
ie_digito_nosso_num_w		varchar2(1);
nr_telefone_w			varchar2(15);
nr_seq_carteira_cobr_tit_w		titulo_receber.nr_seq_carteira_cobr%type;
cd_flash_w			banco_estabelecimento.cd_flash%type;
nr_inscricao_w			varchar2(14);
nr_linha_um_w			number(10);
nr_linha_dois_w			number(10);
nr_linha_tres_w			number(10);

nr_seq_conta_proc_w		pls_conta_proc.nr_sequencia%type;
nr_seq_conta_mat_w		pls_conta_proc.nr_sequencia%type;

ds_mensagem_w			varchar2(140);
ds_mensagem_um_w		varchar2(140);
ds_mensagem_dois_w		varchar2(140);
ds_mensagem_tres_w		varchar2(140);
ie_aplicacao_mensagem_w		banco_instrucao_boleto.ie_aplicacao_mensagem%type;
ie_posicao_w			banco_instrucao_boleto.ie_posicao%type;
nr_linha_w			banco_instrucao_boleto.nr_linha%type;
ds_mensagem_concat_w		varchar2(4000)	:= '';
nr_linha_anterior_w			banco_instrucao_boleto.nr_linha%type;
qt_reg_w				number(10);
cont_cursor_w			number(10)	:= 0;
qt_msg_um_w			number(10);
qt_msg_dois_w			number(10);
ie_mensagem			varchar(1);

nr_seq_grupo_inst_w		number(10);
nr_count_w			number(10);
nr_seq_mensalidade_w		number(10);
vl_mensalidade_w			varchar2(255);
vl_coparticipacao_w		varchar2(255);
nr_seq_segurado_mens_w		number(15);
nr_seq_mensalidade_seg_w		number(10);
qt_coparticipacao_w		number(15);
nr_seq_plano_w			number(15);
ds_plano_w			varchar2(255);
nr_seq_segurado_w		number(15);
vl_item_w				number(15,2);
dt_contratacao_w			varchar2(255);
cd_usuario_plano_w		varchar2(255);
nr_protocolo_ans_w		varchar2(255);
vl_outros_n_w			number(15,2);
vl_outros_w			varchar2(255)	:= null;
vl_total_w			varchar2(255)	:= null;
nr_contrato_w			pls_contrato.nr_contrato%type;
cd_cgc_w			pls_contrato_pagador.cd_cgc%type;

cursor	c01 is
select	decode(b.cd_pessoa_fisica,null,'02','01') ie_tipo_inscricao,
	nvl(c.nr_cpf,b.cd_cgc) nr_inscricao_empresa,
	lpad(a.cd_agencia_bancaria,4,'0') cd_agencia_bancaria,
	lpad(substr(a.nr_conta,1,5) || substr(a.ie_digito_conta,1,1),6,'0') cd_conta,
	lpad(substr(nvl(b.nr_nosso_numero,'0'),1,8),8,'0') nr_nosso_numero,
	decode(nvl(a.vl_multa,0),0,obter_juros_multa_titulo(b.nr_titulo,sysdate,'R','M'),a.vl_multa),
	a.nr_titulo,
	b.dt_pagamento_previsto,
	b.vl_saldo_titulo,
	b.dt_emissao,
	decode(nvl(a.vl_juros,0),0,obter_juros_multa_titulo(b.nr_titulo,sysdate,'R','J'),a.vl_juros),
	a.vl_desconto,
	substr(obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc),1,40) nm_pessoa,	
	rpad(upper(elimina_acentuacao(substr(decode(d.nr_sequencia,null, obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'), pls_obter_compl_pagador(d.nr_seq_pagador,'EN')),1,40))),40,' ') ds_endereco_sacado,
	rpad(upper(elimina_acentuacao(substr(decode(d.nr_sequencia,null, obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'), pls_obter_compl_pagador(d.nr_seq_pagador,'B')),1,12))),12,' ') ds_bairro_sacado,
	lpad(substr(decode(d.nr_sequencia,null, obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'), pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8),8,'0') cd_cep_sacado,
	rpad(upper(elimina_acentuacao(substr(decode(d.nr_sequencia,null, obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'), pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),1,15))),15,' ') ds_municipio_sacado,
	rpad(substr(decode(d.nr_sequencia,null, obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'), pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2),2,' ') ds_estado_sacado,
	b.tx_juros,
	b.tx_multa,
	substr(obter_valor_dominio(707,b.cd_tipo_taxa_juro),1,255) ds_tipo_juros,
	substr(obter_valor_dominio(707,b.cd_tipo_taxa_multa),1,255) ds_tipo_multa,
	b.nr_seq_carteira_cobr,
	b.nr_seq_mensalidade
from	pessoa_fisica 	c,
	titulo_receber 	b,
	titulo_receber_cobr a,
	pls_mensalidade	d
where	b.cd_pessoa_fisica		= c.cd_pessoa_fisica(+)
and	a.nr_titulo			= b.nr_titulo
and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
and	a.nr_seq_cobranca		= nr_seq_cobr_escrit_p;

cursor	c04 is
select	f.nr_sequencia nr_seq_conta_proc,
	null nr_seq_conta_mat
from	pls_conta_proc f,
	pls_conta e,
	pls_mensalidade_seg_item 	a,
	pls_mensalidade_segurado	b,
	pls_mensalidade 		c,
	titulo_receber d
where	b.nr_sequencia	= a.nr_seq_mensalidade_seg
and	c.nr_sequencia	= b.nr_seq_mensalidade
and	e.nr_sequencia	= a.nr_seq_conta
and	e.nr_sequencia	= f.nr_seq_conta
and	a.nr_seq_conta	is not null
and	c.nr_sequencia	= d.nr_seq_mensalidade
and	d.nr_titulo		= nr_titulo_w
union all
select	null nr_seq_conta_proc,
	f.nr_sequencia nr_seq_conta_mat
from	pls_conta_mat f,
	pls_conta e,
	pls_mensalidade_seg_item 	a,
	pls_mensalidade_segurado 	b,
	pls_mensalidade 		c,
	titulo_receber 		d
where	b.nr_sequencia	= a.nr_seq_mensalidade_seg
and	c.nr_sequencia	= b.nr_seq_mensalidade
and	e.nr_sequencia	= a.nr_seq_conta
and	e.nr_sequencia	= f.nr_seq_conta
and	a.nr_seq_conta	is not null
and	c.nr_sequencia	= d.nr_seq_mensalidade
and	d.nr_titulo		= nr_titulo_w;

cursor	c05 is
select	substr(obter_instrucao_boleto_tam(nr_titulo_w,cd_banco_w,a.nr_linha,ie_posicao_w,nr_seq_conta_proc_w,nr_seq_conta_mat_w,a.nr_sequencia),1,140) ds_mensagem
from	banco_instrucao_boleto a
where	nvl(a.ie_posicao,'F')			= ie_posicao_w
and	nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
and	a.cd_banco			= cd_banco_w
and	nvl(a.nr_linha,0)			= nvl(nr_linha_w,0)
and	nvl(a.nr_seq_grupo,0)		= nvl(nr_seq_grupo_inst_w,nvl(a.nr_seq_grupo,0))
order by	a.nr_linha;

cursor	c06 is
select	distinct
	nvl(a.nr_linha,0) nr_linha
from	banco_instrucao_boleto a
where	nvl(a.ie_posicao,'F')			= nvl(ie_posicao_w,'X')
and	nvl(a.ie_aplicacao_mensagem,'T')	= nvl(ie_aplicacao_mensagem_w,'X')
and	a.cd_banco			= nvl(cd_banco_w,0)
and	nvl(a.nr_seq_grupo,0)		= nvl(nr_seq_grupo_inst_w,nvl(a.nr_seq_grupo,0))
order by	1;

Cursor C07 is
	select	campo_mascara_virgula(nvl(a.vl_mensalidade - a.vl_coparticipacao - a.vl_outros,0)) vl_mensalidade,
		campo_mascara_virgula(nvl(a.vl_coparticipacao,0)) vl_coparticipacao,
		a.nr_seq_segurado
	from	pls_mensalidade_segurado 	a
	where	a.nr_seq_mensalidade 	= nr_seq_mensalidade_w
	and	rownum 			<= 9
	order by a.nr_sequencia;

begin

delete	w_envio_banco
where	nm_usuario = nm_usuario_p;

if	(nvl(nr_seq_grupo_inst_p,0)	= 0) then

	nr_seq_grupo_inst_w	:= null;

else

	nr_seq_grupo_inst_w	:= nr_seq_grupo_inst_p;

end if;

/* header */
select	lpad(nvl(b.cd_conta,'0'),5,'0'),
	lpad(nvl(b.cd_agencia_bancaria,'0'),4,'0'),
	substr(nvl(b.ie_digito_conta,'0'),1,1),
	rpad(substr(obter_nome_estabelecimento(cd_estabelecimento_p),1,30),30,' ') nm_empresa,
	to_char(sysdate,'DDMMYY'),
	a.nr_seq_carteira_cobr,
	b.cd_banco,
	rpad(nvl(substr(b.cd_flash,1,3),'LWZ'),3,' ') cd_flash,
	c.cd_cgc nr_inscricao
into	cd_conta_cobr_w,
	cd_agencia_cobr_w,
	ie_digito_conta_cobr_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_seq_carteira_cobr_w,
	cd_banco_w,
	cd_flash_w,
	nr_inscricao_w
from	estabelecimento c,
	banco_estabelecimento b,
	cobranca_escritural a
where	a.cd_estabelecimento	= c.cd_estabelecimento
and	a.nr_seq_conta_banco	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

select	substr(max(a.cd_carteira),1,3) nr_carteira
into	nr_carteira_w
from	banco_carteira a
where	a.nr_sequencia 	= nr_seq_carteira_cobr_w
and	a.cd_banco	= cd_banco_w;

select	max(b.nr_telefone)
into	nr_telefone_w
from	pessoa_juridica b,
	estabelecimento a
where	a.cd_cgc			= b.cd_cgc
and	a.cd_estabelecimento	= cd_estabelecimento_p;

ds_conteudo_w	:= 	'0' ||
			'1' ||
			'REMESSA' ||
			'01' ||
			rpad('COBRANCA',15,' ') ||
			cd_agencia_cobr_w ||
			'00' ||
			cd_conta_cobr_w ||
			ie_digito_conta_cobr_w ||
			rpad(' ',8,' ') ||
			nm_empresa_w ||
			lpad(cd_banco_w,3,'0') ||
			rpad('BANCO ITAU SA',15,' ') ||
			dt_geracao_w ||
			rpad(' ',294,' ') ||
			lpad(nr_seq_reg_arquivo_w,6,'0');

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_reg_arquivo_w);

/* detalhe */
open	C01;
loop
fetch	C01 into	
	ie_tipo_inscricao_w,
	nr_inscricao_empresa_w,
	cd_agencia_bancaria_w,
	cd_conta_w,
	nr_nosso_numero_w,
	vl_multa_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	vl_juros_w,
	vl_desconto_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	sg_estado_w,
	tx_juros_w,
	tx_multa_w,
	ds_tipo_juros_w,
	ds_tipo_multa_w,
	nr_seq_carteira_cobr_tit_w,
	nr_seq_mensalidade_w;
exit	when C01%notfound;

	if	(nr_carteira_w	is null) then

		select	substr(max(a.cd_carteira),1,3) nr_carteira
		into	nr_carteira_w
		from	banco_carteira a
		where	a.nr_sequencia 	= nr_seq_carteira_cobr_tit_w;

	end if;

	nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

	/* Registro Detalhe (obrigatório) */
	ds_conteudo_w	:=	'1' ||
				'02' ||
				lpad(nvl(nr_inscricao_w,'0'),14,'0') ||
				lpad(cd_agencia_bancaria_w,4,'0') ||
				'00' ||
				lpad(cd_conta_w,6,'0') ||
				rpad(' ',4,' ') ||
				'0000' ||
				rpad(nr_titulo_w,25,'0') ||
				lpad('0',8,'0') ||
				'0000000000000' ||
				'112' || --lpad(nvl(nr_carteira_w,'0'),3,'0') ||
				rpad(' ',21,' ') ||
				'I' ||
				'01' ||
				rpad(' ',10,' ') ||
				to_char(dt_vencimento_w,'ddmmyy') ||
				lpad(somente_numero(to_char(nvl(vl_titulo_w,0),'99999999990.00')),13,'0') ||
				lpad(cd_banco_w,3,'0') ||
				'00000' ||
				'99' ||
				'A' ||
				to_char(dt_emissao_w,'ddmmyy') ||
				'0505' ||
				lpad(somente_numero(to_char(nvl(vl_juros_w,0),'99999999990.00')),13,'0') ||
				to_char(dt_vencimento_w,'ddmmyy') ||
				lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'99999999990.00')),13,'0') ||
				lpad('0',26,'0') ||
				ie_tipo_inscricao_w ||
				lpad(nvl(nr_inscricao_empresa_w,'0'),14,'0') ||
				rpad(nm_pessoa_w,40,' ') ||
				ds_endereco_w ||
				ds_bairro_w ||
				cd_cep_w ||
				ds_cidade_w ||
				substr(sg_estado_w,1,2) ||
				rpad(nm_empresa_w,30,' ') ||
				rpad(' ',4,' ') ||
				'000000' ||
				'00' ||
				' ' ||
				lpad(nr_seq_reg_arquivo_w,6,'0');

	insert	into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_reg_arquivo_w);

	nr_linha_um_w	:= 1;
	nr_linha_dois_w	:= 2;
	nr_linha_tres_w	:= 3;

	/* Registro mensagem frente (obrigatório) */
	ie_aplicacao_mensagem_w	:= 'T';
	ie_posicao_w		:= 'F';
	nr_linha_anterior_w		:= null;
	qt_msg_um_w		:= 0;
	qt_msg_dois_w		:= 0;
	cd_cgc_w 		:= null;

	if	(nr_seq_mensalidade_w is not null) then	
		select	max(b.cd_cgc)
		into	cd_cgc_w
		from 	pls_mensalidade a,
			pls_contrato_pagador b,
			pls_contrato c
		where	a.nr_seq_pagador	= b.nr_sequencia
		and   	b.nr_seq_contrato	= c.nr_sequencia
		and	a.nr_sequencia		= nr_seq_mensalidade_w;			
	end if;
	
	if	(cd_cgc_w is null) then
	
		open	c06;
		loop
		fetch	c06 into
			nr_linha_w;
		exit when c06%notfound;

			ds_mensagem_concat_w	:= '';
			cont_cursor_w	:= 0;
			
			select	count(*)
			into	qt_reg_w
			from	banco_instrucao_boleto a
			where	nvl(a.ie_posicao,'F')			= ie_posicao_w
			and	nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
			and	a.cd_banco			= cd_banco_w
			and	a.nr_linha				= nr_linha_w
			and	nvl(a.nr_seq_grupo,0)		= nvl(nr_seq_grupo_inst_p,nvl(a.nr_seq_grupo,0));		
			
			open	c05;
			loop
			fetch	c05 into
				ds_mensagem_w;
			exit	when c05%notfound;
				cont_cursor_w	:= cont_cursor_w + 1;
				ds_mensagem_concat_w	:= ds_mensagem_concat_w||ds_mensagem_w;
				if	(ds_mensagem_concat_w	is not null) or
					(ds_mensagem_concat_w <> '') then
					
					if	((ds_mensagem_um_w		is null) or
						(nvl(nr_linha_anterior_w,nr_linha_w)	= nr_linha_w)) and
						(qt_msg_um_w	= 0) then
						
						ds_mensagem_um_w		:= substr(ds_mensagem_concat_w,1,128);
						if	(cont_cursor_w 		= qt_reg_w) and
							(ds_mensagem_um_w	is not null) then
							qt_msg_um_w	:= 1;
						end if;	
						
					elsif	((ds_mensagem_dois_w		is null) or
						(nvl(nr_linha_anterior_w,nr_linha_w)	= nr_linha_w)) and
						(qt_msg_dois_w	= 0) then
						
						ds_mensagem_dois_w		:= substr(ds_mensagem_concat_w,1,128);
						if	(cont_cursor_w		= qt_reg_w) and
							(ds_mensagem_dois_w	is not null) then
							qt_msg_dois_w	:= 1;
						end if;	
					else
						ds_mensagem_tres_w	:= substr(ds_mensagem_concat_w,1,127);
					end if;

				end if;
				
				if	(ds_mensagem_um_w	is not null) and
					(ds_mensagem_dois_w	is not null) and
					((ds_mensagem_tres_w	is not null) and
					(cont_cursor_w 		= qt_reg_w)) then
					
					 
					nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
					ds_conteudo_w	:=	'7' ||
								cd_flash_w ||
								lpad(nr_linha_um_w,2,'0') ||
								rpad(nvl(ds_mensagem_um_w,' '),128,' ') ||
								lpad(nr_linha_dois_w,2,'0') ||
								rpad(nvl(ds_mensagem_dois_w,' '),128,' ') ||
								lpad(nr_linha_tres_w,2,'0') ||
								rpad(nvl(ds_mensagem_tres_w,' '),127,' ') ||
								' ' ||
								lpad(nr_seq_reg_arquivo_w,6,'0');

					insert	into w_envio_banco
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_estabelecimento,
						ds_conteudo,
						nr_seq_apres)
					values	(w_envio_banco_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						cd_estabelecimento_p,
						ds_conteudo_w,
						nr_seq_reg_arquivo_w);

					ds_mensagem_um_w	:= null;
					ds_mensagem_dois_w	:= null;
					ds_mensagem_tres_w	:= null;
					nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 3;
					nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 3;
					nr_linha_tres_w		:= nvl(nr_linha_tres_w,0) + 3;
					qt_msg_um_w		:= 0;
					qt_msg_dois_w		:= 0;
				end if;
			nr_linha_anterior_w	:= nr_linha_w;	
			end	loop;
			close	c05;
		

		end loop;
		close c06;
		
		/* Tratar as sobras */
		if	(ds_mensagem_um_w is not null) then

			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
			
			
			ds_conteudo_w	:=	'7' ||
						cd_flash_w ||
						lpad(nr_linha_um_w,2,'0') ||
						rpad(nvl(ds_mensagem_um_w,' '),128,' ') ||
						lpad(nr_linha_dois_w,2,'0') ||
						rpad(nvl(ds_mensagem_dois_w,' '),128,' ') ||
						lpad(nr_linha_tres_w,2,'0') ||
						rpad(nvl(ds_mensagem_tres_w,' '),127,' ') ||
						' ' ||
						lpad(nr_seq_reg_arquivo_w,6,'0');

			insert	into w_envio_banco
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
			values	(w_envio_banco_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_reg_arquivo_w);
			
			ds_mensagem_um_w	:= null;
			ds_mensagem_dois_w	:= null;
			ds_mensagem_tres_w	:= null;
			nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 3;
			nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 3;
			nr_linha_tres_w		:= nvl(nr_linha_tres_w,0) + 3;		
		end if;
		
		ie_aplicacao_mensagem_w	:= 'C';
		
		qt_msg_um_w		:= 0;
		qt_msg_dois_w		:= 0;
		ie_mensagem		:= 'N';
		
		open	c04;
		loop
		fetch	c04 into
			nr_seq_conta_proc_w,
			nr_seq_conta_mat_w;
		exit	when c04%notfound;
			nr_linha_anterior_w	:= null;	
			/* Registro mensagem frente (obrigatório) */
			
			open	c06;
			loop
			fetch	c06 into
				nr_linha_w;
			exit when c06%notfound;	
				ds_mensagem_concat_w	:= '';
				cont_cursor_w	:= 0;
				
				select	count(*)
				into	qt_reg_w
				from	banco_instrucao_boleto a
				where	nvl(a.ie_posicao,'F')			= ie_posicao_w
				and	nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
				and	a.cd_banco			= cd_banco_w
				and	a.nr_linha				= nr_linha_w
				and	nvl(a.nr_seq_grupo,0)		= nvl(nr_seq_grupo_inst_p,nvl(a.nr_seq_grupo,0));
				
				open	c05;
				loop
				fetch	c05 into
					ds_mensagem_w;
				exit	when c05%notfound;
					cont_cursor_w	:= cont_cursor_w + 1;
					ds_mensagem_concat_w	:= ds_mensagem_concat_w||ds_mensagem_w;
					if	(ds_mensagem_concat_w	is not null) or
						(ds_mensagem_concat_w <> '') then
						if	(ds_mensagem_um_w		is null) or
							(qt_msg_um_w = 0) then
							ds_mensagem_um_w		:= substr(ds_mensagem_concat_w,1,128);
							if	(cont_cursor_w 		= qt_reg_w) and
								(ds_mensagem_um_w	is not null) then
								qt_msg_um_w	:= 1;
							end if;	
						elsif	(ds_mensagem_dois_w		is null) or
							(qt_msg_dois_w = 0) then
							ds_mensagem_dois_w		:= substr(ds_mensagem_concat_w,1,128);
							if	(cont_cursor_w		= qt_reg_w) and
								(ds_mensagem_dois_w	is not null) then
								qt_msg_dois_w	:= 1;
							end if;	
						else
							ds_mensagem_tres_w	:= substr(ds_mensagem_concat_w,1,127);

						end if;

					end if;

					if	(ds_mensagem_um_w	is not null) and
						(ds_mensagem_dois_w	is not null) and
						((ds_mensagem_tres_w	is not null) and
						(cont_cursor_w = qt_reg_w)) then
						
						nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
						
						ds_conteudo_w	:=	'7' ||
									cd_flash_w ||
									lpad(nr_linha_um_w,2,'0') ||
									rpad(nvl(ds_mensagem_um_w,' '),128,' ') ||
									lpad(nr_linha_dois_w,2,'0') ||
									rpad(nvl(ds_mensagem_dois_w,' '),128,' ') ||
									lpad(nr_linha_tres_w,2,'0') ||
									rpad(nvl(ds_mensagem_tres_w,' '),127,' ') ||
									' ' ||
									lpad(nr_seq_reg_arquivo_w,6,'0');
									
						insert	into w_envio_banco
							(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_estabelecimento,
							ds_conteudo,
							nr_seq_apres)
						values	(w_envio_banco_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_estabelecimento_p,
							ds_conteudo_w,
							nr_seq_reg_arquivo_w);
						
						ie_mensagem		:= 'S';
						ds_mensagem_um_w	:= null;
						ds_mensagem_dois_w	:= null;
						ds_mensagem_tres_w	:= null;
						nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 3;
						nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 3;
						nr_linha_tres_w		:= nvl(nr_linha_tres_w,0) + 3;
						qt_msg_um_w		:= 0;
						qt_msg_dois_w		:= 0;										
					end if;
				--nr_linha_anterior_w	:= nr_linha_w;
				end	loop;
				close	c05;
				
			end loop;
			close c06;

		end	loop;
		close	c04;
		
		
		/* Tratar as sobras */
		if	(ds_mensagem_um_w is not null) then

			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
			ds_conteudo_w	:=	'7' ||
						cd_flash_w ||
						lpad(nr_linha_um_w,2,'0') ||
						rpad(nvl(ds_mensagem_um_w,' '),128,' ') ||
						lpad(nr_linha_dois_w,2,'0') ||
						rpad(nvl(ds_mensagem_dois_w,' '),128,' ') ||
						lpad(nr_linha_tres_w,2,'0') ||
						rpad(nvl(ds_mensagem_tres_w,' '),127,' ') ||
						' ' ||
						lpad(nr_seq_reg_arquivo_w,6,'0');
						
			insert	into w_envio_banco
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
			values	(w_envio_banco_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_reg_arquivo_w);
				
			ie_mensagem		:= 'S';
			ds_mensagem_um_w	:= null;
			ds_mensagem_dois_w	:= null;
			ds_mensagem_tres_w	:= null;
			nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 3;
			nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 3;
			nr_linha_tres_w		:= nvl(nr_linha_tres_w,0) + 3;		
		end if;
		
		if(ie_mensagem = 'N') and (ds_mensagem_um_w is not null) then
		
			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
			ds_conteudo_w	:=	'7' ||
						cd_flash_w ||
						lpad(nr_linha_um_w,2,'0') ||
						rpad(nvl(ds_mensagem_um_w,' '),128,' ') ||
						lpad(nr_linha_dois_w,2,'0') ||
						rpad(nvl(ds_mensagem_dois_w,' '),128,' ') ||
						lpad(nr_linha_tres_w,2,'0') ||
						rpad(nvl(ds_mensagem_tres_w,' '),127,' ') ||
						' ' ||
						lpad(nr_seq_reg_arquivo_w,6,'0');					

			insert	into w_envio_banco
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
			values	(w_envio_banco_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_reg_arquivo_w);
		end if;	
		
		if	(nr_seq_mensalidade_w is not null) then
			select	pls_obter_segurado_pagador(a.nr_seq_pagador)
			into	nr_seq_segurado_w
			from	pls_mensalidade	a
			where	a.nr_sequencia 	= nr_seq_mensalidade_w;
			
			select  	sum(nvl(a.vl_outros,0)) vl_outros,
				campo_mascara_virgula(nvl(sum(a.vl_mensalidade),0)) vl_total_mens,
				max(a.nr_sequencia)
			into	vl_outros_n_w,
				vl_total_w,
				nr_seq_mensalidade_seg_w
			from  	pls_mensalidade_segurado 	a
			where 	a.nr_seq_mensalidade 		= nr_seq_mensalidade_w
			and  	rownum 				<= 9
			order by 	a.nr_sequencia;
			
			select	sum(nvl(a.vl_item,0))
			into	vl_item_w
			from	pls_mensalidade_seg_item 	a,
				pls_mensalidade_segurado 	b
			where	a.nr_seq_mensalidade_seg		= b.nr_sequencia
			and	b.nr_seq_mensalidade   		= nr_seq_mensalidade_w
			and	a.ie_tipo_item			= 4
			and  	rownum 				<= 9
			order by 	a.nr_sequencia;

			if	(nvl(vl_item_w,0) <= nvl(vl_outros_n_w,0)) then
				vl_outros_w := campo_mascara_virgula(nvl(vl_outros_n_w,0) - nvl(vl_item_w,0));
			end if;		
		end if;	
		
		if	(nr_seq_segurado_w is not null) then
			select	a.nr_seq_plano
			into	nr_seq_plano_w
			from	pls_segurado_carteira	b,
				pls_segurado		a
			where	a.nr_sequencia	= b.nr_seq_segurado
			and	a.nr_sequencia 	= nr_seq_segurado_w;
		end if;			
			
		if	(nr_seq_plano_w is not null) then
			select	ds_plano,
				nr_protocolo_ans
			into	ds_plano_w,
				nr_protocolo_ans_w
			from	pls_plano
			where	nr_sequencia	= nr_seq_plano_w;
		end if;	
		
		ds_mensagem_concat_w :=	rpad('Numero Cartao Id.    Inicio     Reg. Prod.    Mensal.      Co-Part.      Qt.CP  ',128,' ');
		
		cont_cursor_w := 0;
		nr_count_w := 1;

		select	count(*)
		into    	cont_cursor_w
		from	pls_mensalidade_segurado 	a
		where	a.nr_seq_mensalidade 	= nr_seq_mensalidade_w
		and	rownum 			<= 9
		order by 	a.nr_sequencia;	
		
		cont_cursor_w := cont_cursor_w + 1;	
		
		open C07;
		loop
		fetch C07 into	
			vl_mensalidade_w,
			vl_coparticipacao_w,
			nr_seq_segurado_mens_w;
		exit when C07%notfound;
			begin			
			
			select	b.cd_usuario_plano,
				to_char(a.dt_contratacao,'dd/mm/yyyy')
			into	cd_usuario_plano_w,
				dt_contratacao_w
			from	pls_segurado_carteira	b,
				pls_segurado		a
			where	a.nr_sequencia		= b.nr_seq_segurado
			and	a.nr_sequencia 		= nr_seq_segurado_mens_w;
			
			select	count(1)
			into	qt_coparticipacao_w
			from	pls_mensalidade_seg_item	b,
				pls_mensalidade_segurado	a
			where	b.nr_seq_mensalidade_seg	= a.nr_sequencia
			and	a.nr_seq_mensalidade	= nr_seq_mensalidade_w
			and	a.nr_seq_segurado		= nr_seq_segurado_mens_w
			and	b.ie_tipo_item 		= 3;

			ds_mensagem_concat_w := ds_mensagem_concat_w || rpad(rpad(cd_usuario_plano_w,21,' ') || rpad(dt_contratacao_w,11,' ') || rpad(nr_protocolo_ans_w,14,' ') ||
						rpad(vl_mensalidade_w,14,' ') || rpad(vl_coparticipacao_w,14,' ') || rpad(qt_coparticipacao_w,11,' '),128,' ');

			nr_count_w := nr_count_w + 1;					

			if (nr_count_w mod 3 = 0) or (nr_count_w = cont_cursor_w) then
			
				nr_seq_reg_arquivo_w := nr_seq_reg_arquivo_w + 1;
				
				ds_conteudo_w	:=	'7' ||
							cd_flash_w ||
							lpad(nr_linha_um_w,2,'0') ||
							rpad(nvl(substr(ds_mensagem_concat_w,1,128),' '),128,' ') ||
							lpad(nr_linha_dois_w,2,'0') ||
							rpad(nvl(substr(ds_mensagem_concat_w,129,128),' '),128,' ') ||
							lpad(nr_linha_tres_w,2,'0') ||
							rpad(nvl(substr(ds_mensagem_concat_w,257,127),' '),127,' ') ||
							' ' ||
							lpad(nr_seq_reg_arquivo_w,6,'0');					

				ds_mensagem_concat_w := '';

				insert	into w_envio_banco
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_estabelecimento,
					ds_conteudo,
					nr_seq_apres)
				values	(w_envio_banco_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_estabelecimento_p,
					ds_conteudo_w,
					nr_seq_reg_arquivo_w);	
					
					
				ds_mensagem_um_w	:= null;
				ds_mensagem_dois_w	:= null;
				ds_mensagem_tres_w	:= null;				
				nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 3;
				nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 3;
				nr_linha_tres_w		:= nvl(nr_linha_tres_w,0) + 3;					
				
			end if;				
		
			end;
		end loop;
		close C07;			

		ds_mensagem_um_w	:= 	rpad(' ',128,' ');		
		ds_mensagem_dois_w	:= 	rpad(rpad('Outros eventos:',20,' ')	|| nvl(vl_outros_w, '0,00'),64,' ') || rpad(nvl(Elimina_Acentos(pls_obter_mensagem_reajuste(nr_seq_mensalidade_w,3,'RA'),'S'), '0,00'),64,' ');
		ds_mensagem_tres_w	:=	rpad('Totalizacao:',20,' ') 		|| nvl(vl_total_w,' 0,00');			
			
		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
		
		ds_conteudo_w	:=	'7' ||
					cd_flash_w ||
					lpad(nr_linha_um_w,2,'0') ||
					rpad(nvl(ds_mensagem_um_w,' '),128,' ') ||
					lpad(nr_linha_dois_w,2,'0') ||
					rpad(nvl(ds_mensagem_dois_w,' '),128,' ') ||
					lpad(nr_linha_tres_w,2,'0') ||
					rpad(nvl(ds_mensagem_tres_w,' '),127,' ') ||
					' ' ||
					lpad(nr_seq_reg_arquivo_w,6,'0');	
					
		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_reg_arquivo_w);

		ds_mensagem_um_w	:= null;
		ds_mensagem_dois_w	:= null;
		ds_mensagem_tres_w	:= null;
	
	else
		nr_linha_w := 1; --@NM_CNPJ_CPF
		
		open	c05;
		loop
		fetch	c05 into
			ds_mensagem_w;
		exit	when c05%notfound;
		
		ds_mensagem_um_w	:= ds_mensagem_w;
		ds_mensagem_dois_w 	:= null;
		ds_mensagem_tres_w 	:= null;
			
		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
		
		ds_conteudo_w	:=	'7' ||
					cd_flash_w ||
					lpad(nr_linha_um_w,2,'0') ||
					rpad(nvl(ds_mensagem_um_w,' '),128,' ') ||
					lpad(nr_linha_dois_w,2,'0') ||
					rpad(nvl(ds_mensagem_dois_w,' '),128,' ') ||
					lpad(nr_linha_tres_w,2,'0') ||
					rpad(nvl(ds_mensagem_tres_w,' '),127,' ') ||
					' ' ||
					lpad(nr_seq_reg_arquivo_w,6,'0');	
					
		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_reg_arquivo_w);

		ds_mensagem_um_w	:= null;	
		nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 3;
		nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 3;
		nr_linha_tres_w		:= nvl(nr_linha_tres_w,0) + 3;
		
		end	loop;
		close	c05;
		
		nr_linha_w := 7; --@DS_ENDERECO_COMPL
		
		open	c05;
		loop
		fetch	c05 into
			ds_mensagem_w;
		exit	when c05%notfound;
			
		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
		
		ds_conteudo_w	:=	'7' ||
					cd_flash_w ||
					lpad(nr_linha_um_w,2,'0') ||
					rpad(nvl(ds_mensagem_um_w,' '),128,' ') ||
					lpad(nr_linha_dois_w,2,'0') ||
					rpad(nvl(ds_mensagem_dois_w,' '),128,' ') ||
					lpad(nr_linha_tres_w,2,'0') ||
					rpad(nvl(ds_mensagem_tres_w,' '),127,' ') ||
					' ' ||
					lpad(nr_seq_reg_arquivo_w,6,'0');	
					
		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_reg_arquivo_w);

		ds_mensagem_um_w	:= ds_mensagem_w;	
		nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 3;
		nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 3;
		nr_linha_tres_w		:= nvl(nr_linha_tres_w,0) + 3;
		
		end	loop;
		close	c05;
		
		nr_linha_w := 11; --APOS VENC. MULTA DE @TX_MULTA % E JUROS @TX_JUROS % AO DIA. MES REF. @DT_REFERENCIA
			
		open	c05;
		loop
		fetch	c05 into
			ds_mensagem_w;
		exit	when c05%notfound;
		
		ds_mensagem_dois_w 	:= null;
		ds_mensagem_tres_w 	:= ds_mensagem_w;
			
		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
		
		ds_conteudo_w	:=	'7' ||
					cd_flash_w ||
					lpad(nr_linha_um_w,2,'0') ||
					rpad(nvl(ds_mensagem_um_w,' '),128,' ') ||
					lpad(nr_linha_dois_w,2,'0') ||
					rpad(nvl(ds_mensagem_dois_w,' '),128,' ') ||
					lpad(nr_linha_tres_w,2,'0') ||
					rpad(nvl(ds_mensagem_tres_w,' '),127,' ') ||
					' ' ||
					lpad(nr_seq_reg_arquivo_w,6,'0');	
					
		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_reg_arquivo_w);

		ds_mensagem_um_w	:= null;	
		ds_mensagem_tres_w 	:= null;		
		
		end	loop;
		close	c05;				
	end if;

	nr_linha_um_w	:= 1;
	nr_linha_dois_w	:= 2;

	/* Registro mensagem verso (opcional) */
	ie_aplicacao_mensagem_w	:= 'T';
	ie_posicao_w		:= 'V';
	nr_linha_anterior_w		:= null;
	qt_msg_um_w		:= 0;
	
	open	c06;
	loop
	fetch	c06 into
		nr_linha_w;
	exit when c06%notfound;
		ds_mensagem_concat_w	:= '';
		cont_cursor_w		:= 0;
		
		select	count(*)
		into	qt_reg_w
		from	banco_instrucao_boleto a
		where	nvl(a.ie_posicao,'F')			= ie_posicao_w
		and	nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
		and	a.cd_banco			= cd_banco_w
		and	a.nr_linha				= nr_linha_w
		and	nvl(a.nr_seq_grupo,0)		= nvl(nr_seq_grupo_inst_p,nvl(a.nr_seq_grupo,0));
		
		open	c05;
		loop
		fetch	c05 into
			ds_mensagem_w;
		exit	when c05%notfound;
			cont_cursor_w	:= cont_cursor_w + 1;
			ds_mensagem_concat_w	:= ds_mensagem_concat_w||ds_mensagem_w;
			if	(ds_mensagem_concat_w			is not null) or
				(ds_mensagem_concat_w 			<> '') then
				if	((ds_mensagem_um_w		is null) or
					(nvl(nr_linha_anterior_w,nr_linha_w)	= nr_linha_w)) and
					(qt_msg_um_w = 0) then

					ds_mensagem_um_w		:= ds_mensagem_concat_w;
					if	(cont_cursor_w 		= qt_reg_w) and
						(ds_mensagem_um_w	is not null) then
						qt_msg_um_w	:= 1;
					end if;	
				else
					ds_mensagem_dois_w		:= ds_mensagem_concat_w;

				end if;

			end if;

			if	(ds_mensagem_um_w	is not null) and
				((ds_mensagem_dois_w	is not null) and
				(cont_cursor_w = qt_reg_w)) then
				
				nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

				ds_conteudo_w	:=	'8' ||
							lpad(nr_linha_um_w,2,'0') ||
							rpad(nvl(ds_mensagem_um_w,' '),140,' ') ||
							rpad(' ',50,' ') ||
							lpad(nr_linha_dois_w,2,'0') ||
							rpad(nvl(ds_mensagem_dois_w,' '),140,' ') ||
							rpad(' ',59,' ') ||
							lpad(nr_seq_reg_arquivo_w,6,'0');

				insert	into w_envio_banco
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					cd_estabelecimento,
					ds_conteudo,
					nr_seq_apres)
				values	(w_envio_banco_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					cd_estabelecimento_p,
					ds_conteudo_w,
					nr_seq_reg_arquivo_w);

				ds_mensagem_um_w	:= null;
				ds_mensagem_dois_w	:= null;
				nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 2;
				nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 2;
				qt_msg_um_w		:= 0;

			end if;
		nr_linha_anterior_w	:= nr_linha_w;
		end	loop;
		close	c05;
		
	end loop;
	close c06;

	/* Tratar as sobras */
	if	(ds_mensagem_um_w	is not null) then

		nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

		ds_conteudo_w	:=	'8' ||
					lpad(nr_linha_um_w,2,'0') ||
					rpad(nvl(ds_mensagem_um_w,' '),140,' ') ||
					rpad(' ',50,' ') ||
					lpad(nr_linha_dois_w,2,'0') ||
					rpad(nvl(ds_mensagem_dois_w,' '),140,' ') ||
					rpad(' ',59,' ') ||
					lpad(nr_seq_reg_arquivo_w,6,'0');

		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_reg_arquivo_w);

		ds_mensagem_um_w	:= null;
		ds_mensagem_dois_w	:= null;
		nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 2;
		nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 2;

	end if;
	
	ie_aplicacao_mensagem_w	:= 'C';

	open	c04;
	loop
	fetch	c04 into
		nr_seq_conta_proc_w,
		nr_seq_conta_mat_w;
	exit	when c04%notfound;
		nr_linha_anterior_w	:= null;
		/* Registro mensagem verso (opcional) */
		qt_msg_um_w	:= 0;
		
		open	c06;
		loop
		fetch	c06 into
			nr_linha_w;
		exit when c06%notfound;
			ds_mensagem_concat_w	:= '';
			cont_cursor_w	:= 0;
			
			select	count(*)
			into	qt_reg_w
			from	banco_instrucao_boleto a
			where	nvl(a.ie_posicao,'F')			= ie_posicao_w
			and	nvl(a.ie_aplicacao_mensagem,'T')	= ie_aplicacao_mensagem_w
			and	a.cd_banco			= cd_banco_w
			and	a.nr_linha				= nr_linha_w
			and	nvl(a.nr_seq_grupo,0)		= nvl(nr_seq_grupo_inst_p,nvl(a.nr_seq_grupo,0));
			
			open	c05;
			loop
			fetch	c05 into
				ds_mensagem_w;
			exit	when c05%notfound;
				cont_cursor_w	:= cont_cursor_w + 1;
				ds_mensagem_concat_w	:= ds_mensagem_concat_w||ds_mensagem_w;
				if	(ds_mensagem_concat_w	is not null) or
					(ds_mensagem_concat_w <> '') then

					if	((ds_mensagem_um_w		is null) or
						(nvl(nr_linha_anterior_w,nr_linha_w)	= nr_linha_w)) and
						(qt_msg_um_w	= 0) then

						ds_mensagem_um_w		:= ds_mensagem_concat_w;
						if	(cont_cursor_w 		= qt_reg_w) and
							(ds_mensagem_um_w	is not null) then
							qt_msg_um_w	:= 1;
						end if;
					else

						ds_mensagem_dois_w	:= ds_mensagem_concat_w;

					end if;

				end if;

				if	(ds_mensagem_um_w	is not null) and
					((ds_mensagem_dois_w	is not null) and
					(cont_cursor_w = qt_reg_w)) then
					
					nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

					ds_conteudo_w	:=	'8' ||
								lpad(nr_linha_um_w,2,'0') ||
								rpad(nvl(ds_mensagem_um_w,' '),140,' ') ||
								rpad(' ',50,' ') ||
								lpad(nr_linha_dois_w,2,'0') ||
								rpad(nvl(ds_mensagem_dois_w,' '),140,' ') ||
								rpad(' ',59,' ') ||
								lpad(nr_seq_reg_arquivo_w,6,'0');

					insert	into w_envio_banco
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_estabelecimento,
						ds_conteudo,
						nr_seq_apres)
					values	(w_envio_banco_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						cd_estabelecimento_p,
						ds_conteudo_w,
						nr_seq_reg_arquivo_w);

					ds_mensagem_um_w	:= null;
					ds_mensagem_dois_w	:= null;
					nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 2;
					nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 2;
					qt_msg_um_w		:= 0;

				end if;
			nr_linha_anterior_w	:= nr_linha_w;
			end	loop;
			close	c05;
			
		end loop;
		close c06;

		/* Tratar as sobras */
		if	(ds_mensagem_um_w	is not null) then

			nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

			ds_conteudo_w	:=	'8' ||
						lpad(nr_linha_um_w,2,'0') ||
						rpad(nvl(ds_mensagem_um_w,' '),140,' ') ||
						rpad(' ',50,' ') ||
						lpad(nr_linha_dois_w,2,'0') ||
						rpad(nvl(ds_mensagem_dois_w,' '),140,' ') ||
						rpad(' ',59,' ') ||
						lpad(nr_seq_reg_arquivo_w,6,'0');

			insert	into w_envio_banco
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres)
			values	(w_envio_banco_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_reg_arquivo_w);

			ds_mensagem_um_w	:= null;
			ds_mensagem_dois_w	:= null;
			nr_linha_um_w		:= nvl(nr_linha_um_w,0) + 2;
			nr_linha_dois_w		:= nvl(nr_linha_dois_w,0) + 2;

		end if;

	
	end	loop;
	close	c04;

	nr_seq_conta_proc_w	:= null;
	nr_seq_conta_mat_w	:= null;
	
end	loop;
close	C01;

/* trailer */
nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

ds_conteudo_w	:= 	'9' ||
			rpad(' ',393,' ') ||
			lpad(nr_seq_reg_arquivo_w,6,'0');

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_reg_arquivo_w);

commit;

end ATIVIA_GERAR_COBR_ITAU_400;
/
