create or replace
procedure lic_cancelar_item_reg_preco(	nr_sequencia_p		number,
				ds_motivo_cancel_p	varchar2,
				nm_usuario_p		varchar2) is

begin

update	reg_compra_item
set	dt_cancelamento	= sysdate,
	ds_motivo_cancel	= substr(ds_motivo_cancel_p,1,255),
	nm_usuario_cancel	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;

commit;

end lic_cancelar_item_reg_preco;
/