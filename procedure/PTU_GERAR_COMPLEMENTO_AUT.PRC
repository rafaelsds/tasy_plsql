create or replace
procedure ptu_gerar_complemento_aut
			(	nr_seq_guia_p		Number,
				nr_seq_requisicao_p	Number,
				nr_seq_compl_p		Number,
				ie_tipo_p		Varchar2,
				nm_usuario_p		Varchar2) is
/*
ie_tipo_p
P - pedido
S - Servi�o
*/		

ie_tipo_cliente_w		varchar2(3);
cd_unimed_executora_w		number(4);
cd_unimed_beneficiario_w	number(4);
nr_seq_execucao_w		number(10);
cd_unimed_w			number(4);
cd_usuario_plano_w		varchar2(30);
nr_seq_requisicao_w		number(10);
nr_seq_guia_w			number(10);
cd_unimed_prestador_req_w	number(4);
nr_seq_prestador_req_w		number(10);
nr_seq_prestador_w		number(10);
cd_especialidade_w		number(2);
ds_observacao_w			varchar2(4000)	:= '';
ds_biometria_w			varchar2(4000)	:= '';
nr_seq_origem_w			number(10);
cd_servico_w			number(10);
ie_tipo_servico_w		varchar2(1);
ie_classificacao_w		number(2);
ie_tipo_tabela_w		varchar2(2);	
nr_seq_pacote_w			number(10);
ie_tipo_despesa_w		number(2);
ds_opme_w			varchar2(80);
ie_origem_servico_w		number(2);
nr_seq_material_w		number(10);
qt_servico_w			number(4);
nr_seq_prest_fornec_w		number(10);
nr_seq_pedido_w			number(10);
nr_seq_controle_exec_w		number(10);
cd_estabelecimento_w		number(10);
nr_seq_congenere_w		number(10);
cd_servico_conversao_w		number(15);
ie_origem_servico_ww		number(10);
nr_seq_regra_w			number(10);
ie_somente_codigo_w		pls_conversao_proc.ie_somente_codigo%type;

begin

if	(nr_seq_guia_p	is not null) then
	begin
	select	ie_tipo_cliente,
		cd_unimed_executora,
		cd_unimed_beneficiario,
		nr_seq_execucao,
		cd_unimed,
		cd_usuario_plano,
		cd_unimed_prestador_req,
		nr_seq_prestador_req,
		nr_seq_prestador,
		cd_especialidade
	into	ie_tipo_cliente_w,
		cd_unimed_executora_w,
		cd_unimed_beneficiario_w,
		nr_seq_execucao_w,
		cd_unimed_w,
		cd_usuario_plano_w,
		cd_unimed_prestador_req_w,
		nr_seq_prestador_req_w,
		nr_seq_prestador_w,
		cd_especialidade_w
	from	ptu_pedido_autorizacao
	where	nr_seq_guia	= nr_seq_guia_p;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(179718);
	end;
	
	pls_guia_gravar_historico(nr_seq_guia_p,2,'Enviado pedido de complemento de autoriza��o para a Unimed '||cd_unimed_beneficiario_w,'',nm_usuario_p);
elsif	(nr_seq_requisicao_p	is not null) then
	begin
	select	ie_tipo_cliente,
		cd_unimed_executora,
		cd_unimed_beneficiario,
		nr_seq_execucao,
		cd_unimed,
		cd_usuario_plano,
		cd_unimed_prestador_req,
		nr_seq_prestador_req,
		nr_seq_prestador,
		cd_especialidade
	into	ie_tipo_cliente_w,
		cd_unimed_executora_w,
		cd_unimed_beneficiario_w,
		nr_seq_execucao_w,
		cd_unimed_w,
		cd_usuario_plano_w,
		cd_unimed_prestador_req_w,
		nr_seq_prestador_req_w,
		nr_seq_prestador_w,
		cd_especialidade_w
	from	ptu_pedido_autorizacao
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(179718);
	end;
	
	pls_requisicao_gravar_hist(nr_seq_requisicao_p,'L','Enviado pedido de complemento de autoriza��o para a Unimed '||cd_unimed_beneficiario_w,null,nm_usuario_p);
end if;

if	(ie_tipo_p	= 'P') then	
	select	nvl(max(nr_seq_origem),0),
		nvl(substr(max(ds_observacao),1,3),'')
	into	nr_seq_origem_w,
		ds_observacao_w
	from	ptu_resposta_autorizacao
	where	nr_seq_execucao	= nr_seq_execucao_w;
	
	if	(ds_observacao_w	= 'V30') then
		insert	into ptu_pedido_compl_aut
			(nr_sequencia, ie_tipo_cliente, cd_unimed_executora,
			 cd_unimed_beneficiario, nr_seq_execucao, nr_seq_origem,
			 cd_unimed, cd_usuario_plano, dt_atualizacao,
			 nm_usuario, nr_seq_requisicao, nr_seq_guia,
			 cd_unimed_prestador_req, nr_seq_prestador_req, nr_seq_prestador,
			 cd_especialidade, ds_biometria,
			 cd_transacao, ie_utilizado, ie_status_complemento)
		values	(nr_seq_compl_p, ie_tipo_cliente_w, cd_unimed_executora_w,
			 cd_unimed_beneficiario_w, nr_seq_execucao_w, nr_seq_origem_w,
			 cd_unimed_w, cd_usuario_plano_w, sysdate,
			 nm_usuario_p, nr_seq_requisicao_p, nr_seq_guia_p,
			 cd_unimed_prestador_req_w, nr_seq_prestador_req_w, nr_seq_prestador_w,
			 cd_especialidade_w, ds_biometria_w,
			 '00505', 'N', 6);
		
		update	ptu_controle_execucao
		set	nr_seq_pedido_compl	= nr_seq_compl_p,
			nr_seq_pedido_aut	= null
		where	nr_sequencia		= nr_seq_execucao_w;
	else
		select	ptu_controle_execucao_seq.NextVal
		into	nr_seq_controle_exec_w
		from	dual;
				
		nr_seq_origem_w		:= nr_seq_execucao_w;
		nr_seq_execucao_w	:= nr_seq_controle_exec_w;
				
		insert	into ptu_pedido_compl_aut
			(nr_sequencia, ie_tipo_cliente, cd_unimed_executora,
			 cd_unimed_beneficiario, nr_seq_execucao, nr_seq_origem,
			 cd_unimed, cd_usuario_plano, dt_atualizacao,
			 nm_usuario, nr_seq_requisicao, nr_seq_guia,
			 cd_unimed_prestador_req, nr_seq_prestador_req, nr_seq_prestador,
			 cd_especialidade, ds_observacao, ds_biometria,
			 cd_transacao, ie_utilizado, ie_status_complemento)
		values	(nr_seq_compl_p, ie_tipo_cliente_w, cd_unimed_executora_w,
			 cd_unimed_beneficiario_w, nr_seq_execucao_w, nr_seq_origem_w,
			 cd_unimed_w, cd_usuario_plano_w, sysdate,
			 nm_usuario_p, nr_seq_requisicao_p, nr_seq_guia_p,
			 cd_unimed_prestador_req_w, nr_seq_prestador_req_w, nr_seq_prestador_w,
			 cd_especialidade_w, ds_observacao_w, ds_biometria_w,
			 '00505', 'N', 6);
		
		insert	into ptu_controle_execucao
			(nr_sequencia, dt_atualizacao, nm_usuario,
			 nr_seq_pedido_compl, nr_seq_pedido_aut)
		values	(nr_seq_controle_exec_w, sysdate, nm_usuario_p,
			 nr_seq_compl_p, null);
	end if;
elsif	(ie_tipo_p	= 'S') then
	select	nr_seq_pedido,
		cd_servico,
		ie_origem_servico,
		qt_servico,
		nr_seq_pacote,
		nr_seq_prest_fornec,
		ie_tipo_servico
	into	nr_seq_pedido_w,
		cd_servico_w,
		ie_origem_servico_w,
		qt_servico_w,
		nr_seq_pacote_w,
		nr_seq_prest_fornec_w,
		ie_tipo_servico_w
	from	ptu_pedido_compl_aut_serv
	where	nr_sequencia	= nr_seq_compl_p;
	
	if	(ie_tipo_servico_w	= 'P') then
		select	max(ie_classificacao)
		into	ie_classificacao_w
		from	procedimento
		where	cd_procedimento	= cd_servico_w;

		if	(nr_seq_pacote_w	is not null) then
			ie_tipo_tabela_w	:= '4';
			
			update	ptu_pedido_compl_aut_serv
			set	ie_utilizado	= 'N',
				ie_tipo_tabela	= ie_tipo_tabela_w
			where	nr_sequencia	= nr_seq_compl_p;
		else
			select	b.nr_seq_congenere
			into	nr_seq_congenere_w
			from	pls_segurado	b,
				pls_requisicao	a
			where	b.nr_sequencia	= a.nr_seq_segurado
			and	a.nr_sequencia	= nr_seq_requisicao_p;
			
			select	cd_estabelecimento
			into	cd_estabelecimento_w
			from	pls_parametros_scs;
			
			pls_obter_proced_conversao(	cd_servico_w, ie_origem_servico_w, null, 
							cd_estabelecimento_w, 3, nr_seq_congenere_w, 
							1,NULL,	null,
							null,null, null,
							null, cd_servico_conversao_w, ie_origem_servico_ww, 
							nr_seq_regra_w, ie_somente_codigo_w, sysdate,
							null, null, null);
							
			if	(cd_servico_w	= cd_servico_conversao_w) then
				cd_servico_conversao_w	:= null;
				ie_origem_servico_ww	:= null;
			end if;

			update	ptu_pedido_compl_aut_serv
			set	ie_utilizado		= 'N',
				cd_servico_conversao	= cd_servico_conversao_w	
			where	nr_sequencia		= nr_seq_compl_p;
		end if;
		
		pls_atualiza_valor_proc_aut(nr_seq_compl_p, 'COMPL', nm_usuario_p);		 
	elsif	(ie_tipo_servico_w	= 'M') then
		select	nvl(ie_tipo_despesa,0),
			nr_sequencia
		into	ie_tipo_despesa_w,
			nr_seq_material_w
		from	pls_material
		where	cd_material_ops	= cd_servico_w;

		if	(ie_tipo_despesa_w	= 7) then
			select	substr(nvl(pls_obter_desc_material(nr_seq_material_w),'N�o encontrado'),1,80)
			into	ds_opme_w
			from 	dual;
			
			update	ptu_pedido_compl_aut_serv
			set	ie_utilizado	= 'N',
				ds_opme		= ds_opme_w
			where	nr_sequencia	= nr_seq_compl_p;
		end if;
		
		ptu_gerar_mat_envio_intercamb(nr_seq_material_w,'E', null, ie_tipo_despesa_w, nm_usuario_p, cd_servico_conversao_w, ds_opme_w);
		
		if	(cd_servico_conversao_w	= 0) and (ds_opme_w	= 'X') then
			cd_servico_conversao_w	:= null;
			ds_opme_w		:= '';
		end if;
		
		update	ptu_pedido_compl_aut_serv
		set	ie_utilizado		= 'N',
			cd_servico_conversao	= cd_servico_conversao_w,
			ds_opme			= ds_opme_w
		where	nr_sequencia		= nr_seq_compl_p;
		
		pls_atualiza_valor_mat_aut(nr_seq_compl_p, 'COMPL', nm_usuario_p);
	end if;
end if;

commit;

end ptu_gerar_complemento_aut;
/