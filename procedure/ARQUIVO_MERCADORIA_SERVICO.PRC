create or replace
procedure arquivo_mercadoria_servico (cd_estabelecimento_p	number,
					dt_inicio_p		date,
					dt_fim_p		date,
					nm_usuario_p		varchar2) is

nr_sequencia_w			varchar2(10);
nr_item_nf_w			varchar2(10);
dt_atualizacao_w		varchar2(8);
cd_item_nf_w			varchar2(20);
ds_item_nf_w			varchar2(45);
cd_estabelecimento_w		varchar2(10);
contador_w			number;
ds_arquivo_ww			varchar2(4000);


cursor	c01 is
select	a.nr_sequencia							nr_sequencia,
	b.nr_item_nf							nr_item_nf,
	lpad(nvl(to_char(a.dt_atualizacao,'ddmmyyyy'),' '),8,' ')						dt_atualizacao,
	lpad(nvl(to_char(decode(b.cd_material,null,b.cd_procedimento,b.cd_material)),' '),20,' ')	cd_item_nf,
	rpad(nvl(decode(b.cd_material, null,substr(obter_descricao_procedimento(b.cd_procedimento, b.ie_origem_proced),1,240),substr(obter_desc_material(b.cd_material),1,100)),' '),45,' ') ds_item_nf,
	a.cd_estabelecimento						cd_estabelecimento
from	nota_fiscal a,
	nota_fiscal_item b
where	b.nr_sequencia = a.nr_sequencia
and	a.cd_estabelecimento = cd_estabelecimento_p
and	a.dt_emissao between to_date(to_char(dt_inicio_p,'dd/mm/yyyy'),'dd/mm/yyyy') and fim_dia(to_date(to_char(dt_fim_p,'dd/mm/yyyy'),'dd/mm/yyyy'))
order by	a.nr_sequencia, b.nr_item_nf;

begin

open c01;
loop
fetch c01 into

nr_sequencia_w,
nr_item_nf_w,
dt_atualizacao_w,
cd_item_nf_w,
ds_item_nf_w,
cd_estabelecimento_w;

exit when c01%notfound;

	begin

	contador_w := contador_w + 1;

	ds_arquivo_ww	:=	dt_atualizacao_w	||
				cd_item_nf_w		||
				ds_item_nf_w;

insert into w_inss_direp_arquivo ( nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_arquivo_w)
		values		(w_inss_direp_arquivo_seq.nextval,
				cd_estabelecimento_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_arquivo_ww);

	if (mod(contador_w,100) = 0) then
		commit;
	end if;

	end;

end loop;
close c01;

commit;

end arquivo_mercadoria_servico;
/
 