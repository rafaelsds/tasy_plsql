create or replace
procedure Gerar_prescr_gas_hor(	nr_prescricao_p		Number,
								nm_usuario_p		Varchar2,
								ie_liberar_p		varchar2,
								nr_sequencia_p		Number default null,
								ie_lib_parcial_p	Varchar2 default 'N') is
					
k						integer;
nr_seq_gasoterapia_w	number(10);
nr_atendimento_w		number(10,0);
dt_horario_gas_w		date;
dt_lib_horario_w		date;
cd_estabelecimento_w	number(4);
ds_horarios_w			varchar2(4000);
ds_hora_gas_w			varchar2(2000);
qt_dia_adic_w			number(10) := 0;
nr_seq_horario_w		number(10);
nr_etapa_w				number(4) := 0;
ie_inicio_w				varchar2(15);
ie_dose_especial_w		varchar2(2);
dt_prev_execucao_w		date;
qt_incons_gas_w			number(10);
					
cursor C01 is
select	a.nr_sequencia,
		nvl(a.dt_prev_execucao,b.dt_inicio_prescr),
		a.ds_horarios,
		a.ie_inicio
from	prescr_gasoterapia a,
		prescr_medica b
where	a.nr_prescricao = b.nr_prescricao
and		b.nr_prescricao = nr_prescricao_p
and		((nr_sequencia_p is null) or (a.nr_sequencia = nr_sequencia_p))
and		b.dt_suspensao is null
and 	not exists	(	select	1
						from	prescr_gasoterapia_hor x
						where	a.nr_sequencia = x.nr_seq_gasoterapia
						and		((x.dt_lib_horario is not null) or 
								 (x.dt_suspensao is not null))
						and		x.nr_prescricao = a.nr_prescricao);

begin

delete	
from	prescr_gasoterapia_hor
where	nr_prescricao = nr_prescricao_p
and		dt_lib_horario is null
and		dt_suspensao is null
and		((nr_sequencia_p is null) or (nr_seq_gasoterapia = nr_sequencia_p));

if	(nr_prescricao_p is not null) then
	-- Obter os dados da prescrição
	select	max(nr_atendimento),
			max(cd_estabelecimento)
	into	nr_atendimento_w,
			cd_estabelecimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	
	select	count(nr_sequencia)
	into	qt_incons_gas_w
	from	prescr_gasoterapia
	where 	nr_prescricao = nr_prescricao_p
	and		nr_seq_inconsistencia is not null;
	
	if ((ie_lib_parcial_p = 'S') and (qt_incons_gas_w > 0)) then
		dt_lib_horario_w := null;
	elsif	(ie_liberar_p = 'S') then
		dt_lib_horario_w := sysdate;
	end if;
	
	open C01;
	loop
	fetch C01 into
		nr_seq_gasoterapia_w,
		dt_prev_execucao_w,
		ds_horarios_w,
		ie_inicio_w;
	exit when C01%notfound;
		begin
		qt_dia_adic_w	:= 0;
		nr_etapa_w 	:= 0;

		ds_horarios_w	:= padroniza_horario_prescr(ds_horarios_w, to_char(dt_prev_execucao_w,'dd/mm/yyyy hh24:mi'));

		if (ds_horarios_w is not null) then
			begin
			while	(ds_horarios_w is not null) loop
				begin
				k	:= instr(ds_horarios_w, ' ');
				
				if	(k > 1) and
					(substr(ds_horarios_w, 1, k -1) is not null) then
					ds_hora_gas_w			:= substr(ds_horarios_w, 1, k-1);
					ds_hora_gas_w			:= replace(ds_hora_gas_w, ' ','');
					ds_horarios_w	:= substr(ds_horarios_w, k + 1, 2000);
				elsif	(ds_horarios_w is not null) then
					ds_hora_gas_w			:= replace(ds_horarios_w,' ','');
					ds_horarios_w	:= '';
				end if;
				
				if	(instr(ds_hora_gas_w,'A') > 0) and
					(qt_dia_adic_w = 0) then
					qt_dia_adic_w	:= 1;
				elsif	(instr(ds_hora_gas_w,'AA') > 0) then
					qt_dia_adic_w	:= qt_dia_adic_w + 1;
				end if;

				ds_hora_gas_w := replace(ds_hora_gas_w,'A','');
				dt_horario_gas_w := pkg_date_utils.get_time(dt_prev_execucao_w + qt_dia_adic_w, trim(ds_hora_gas_w), 0);

				select	prescr_gasoterapia_hor_seq.nextval
				into	nr_seq_horario_w
				from	dual;

				if	(ie_inicio_w = 'ACM') or 
					(ie_inicio_w = 'D') then
					ie_dose_especial_w := 'S';
					nr_etapa_w := null;
				else
					ie_dose_especial_w := 'N';
				end if;

				if (ie_dose_especial_w = 'N') then
					nr_etapa_w := nr_etapa_w + 1;
				end if;

				insert into prescr_gasoterapia_hor (	nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										nr_seq_gasoterapia,
										dt_horario,
										dt_fim_horario,
										dt_suspensao,
										nm_usuario_adm,
										nm_usuario_susp,
										nr_prescricao,
										dt_lib_horario,
										nr_etapa,
										ie_horario_especial)
									values(	nr_seq_horario_w,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										nr_seq_gasoterapia_w,
										dt_horario_gas_w,
										null,
										null,
										null,
										null,
										nr_prescricao_p,
										dt_lib_horario_w,
										nr_etapa_w,
										ie_dose_especial_w);
				end;
			end loop;
			end;
		else
			begin
			if	(ie_inicio_w = 'ACM') or 
				(ie_inicio_w = 'D') then
				ie_dose_especial_w := 'S';
				nr_etapa_w := null;
			else
				ie_dose_especial_w := 'N';
				nr_etapa_w := 1; --null; --OS 1118146
			end if;
			
			select	nvl(max(dt_inicio_prescr),sysdate)
			into	dt_horario_gas_w
			from	prescr_medica
			where	nr_prescricao = nr_prescricao_p;
			
			select	prescr_gasoterapia_hor_seq.nextval
			into	nr_seq_horario_w
			from	dual;
			
			insert into prescr_gasoterapia_hor (	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_seq_gasoterapia,
								dt_horario,
								dt_fim_horario,
								dt_suspensao,
								nm_usuario_adm,
								nm_usuario_susp,
								nr_prescricao,
								dt_lib_horario,
								nr_etapa,
								ie_horario_especial)
							values(	nr_seq_horario_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_seq_gasoterapia_w,
								dt_horario_gas_w,
								null,
								null,
								null,
								null,
								nr_prescricao_p,
								dt_lib_horario_w,
								nr_etapa_w,
								ie_dose_especial_w);
			end;
		end if ;
		end;
	end loop;
	close C01;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Gerar_prescr_gas_hor;
/