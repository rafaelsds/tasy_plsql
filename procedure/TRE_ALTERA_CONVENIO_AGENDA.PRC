create or replace
procedure tre_altera_convenio_agenda(	nr_seq_agenda_consulta_p	number,
					cd_convenio_p			number,
					cd_categoria_p			varchar2,
					nm_paciente_p			varchar2,
					cd_tipo_acomodacao_p		number,
					cd_usuario_convenio_p		varchar2,
					cd_pessoa_fisica_p		varchar2,
					dt_validade_carteira_p		Varchar2) is 

begin
if	(nr_seq_agenda_consulta_p is not null) then
	update	agenda_consulta
	set	cd_convenio = cd_convenio_p,
		cd_categoria = cd_categoria_p,
		cd_pessoa_fisica = cd_pessoa_fisica_p,
		nm_paciente	=	nm_paciente_p,
		cd_tipo_acomodacao = cd_tipo_acomodacao_p,
		cd_usuario_convenio = cd_usuario_convenio_p,
		dt_validade_carteira = to_date(dt_validade_carteira_p,'dd/mm/yyyy')
	where	nr_sequencia = nr_seq_agenda_consulta_p;	
end if;

commit;

end tre_altera_convenio_agenda;
/