create or replace
procedure fis_gerar_reg_m200_efd
			(	nr_seq_controle_p		number,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				dt_inicio_p			date,
				dt_fim_p			date,
				cd_empresa_p			number,
				ds_separador_p			varchar2,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is 

aliquota_pis_w			number(7,4);
cd_modelo_w			varchar2(2);
cd_tributo_pis_w		number(3);
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
ie_ind_incidencia_trib_w	number(6);		
nr_seq_registro_w		number(10)	:= nr_sequencia_p;
nr_seq_regra_efd_w		number(10);
nr_versao_efd_w			varchar2(5);
nr_linha_w			number(10)	:= qt_linha_p;
sep_w				varchar2(1)	:= ds_separador_p;
tp_registro_w			varchar2(4);
vl_base_calculo_tributo_w	number(15,2);
vl_material_incentivo_w		number(15,2);
vl_base_calculo_cumulativo_w	number(15,2)	:= 0;
vl_base_calculo_nao_cum_w	number(15,2)	:= 0;
vl_base_calculo_total_w		number(15,2)	:= 0;
vl_contribuicao_cumulativo_w	number(15,2)	:= 0;
vl_contribuicao_nao_cum_w	number(15,2)	:= 0;
vl_retido_cumulativo_w		number(15,2)	:= 0;
vl_somado_cumulativo_w		number(15,2)	:= 0;
vl_retido_nao_cum_w		number(15,2)	:= 0;
vl_deducoes_w			number(15,2)	:= 0;
ie_local_gerar_sped_w		varchar2(1);
qt_registros_w			number(10);
nr_seq_data_w			number(10);
cd_empresa_w			number(4);
ie_folha_salario_w		varchar2(1);
ie_nota_entrada_w		varchar2(1);
ie_buscar_data_w		varchar2(1);
ie_nota_cancelada_w		varchar2(1);
ie_consistir_cpf_cnpj_w		varchar2(1);
vl_deducoes_cum_w		number(15,2)	:= 0;
vl_retido_cum_w			number(15,2)	:= 0;
vl_contribuicao_cum_w		number(15,2);
vl_base_calculo_cum_w		number(15,2);
vl_deducoes_nao_cum_w		number(15,2);
ie_somar_trib_sai_w		varchar2(1);
vl_campo_02_w			number(15,2);
vl_campo_05_w			number(15,2);
vl_campo_06_w			number(15,2);
vl_campo_07_w			number(15,2);
vl_campo_08_w			number(15,2);
vl_campo_09_w			number(15,2);
vl_campo_10_w			number(15,2);
vl_campo_11_w			number(15,2);
vl_campo_12_w			number(15,2);
vl_campo_13_w			number(15,2);
ie_gerar_F700_w			fis_regra_efd_reg.ie_gerar%type;
nr_num_campo_w			varchar2(2);
vl_debito_w			number(15,2);

begin
select	nr_seq_regra_efd
into	nr_seq_regra_efd_w
from	fis_efd_controle
where	nr_sequencia	= nr_seq_controle_p;

select	nvl(max(ie_somar_trib_sai),'N')
into	ie_somar_trib_sai_w
from	fis_regra_efd_a100
where	nr_seq_regra_efd	= nr_seq_regra_efd_w;

select	nvl(max(ie_gerar),'N')
into	ie_gerar_F700_w
from	fis_regra_efd_reg
where	nr_seq_regra_efd	= nr_seq_regra_efd_w 	
and	cd_registro		= 'F700';

select	cd_empresa
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

select	ie_consistir_cpf_cnpj
into	ie_consistir_cpf_cnpj_w
from	fis_efd_controle
where	nr_sequencia	= nr_seq_controle_p;

select	nvl(max(ie_nota_entrada),'N'),
	nvl(max(ie_buscar_data),'N'),
	nvl(max(ie_nota_cancelada),'N')
into	ie_nota_entrada_w,
	ie_buscar_data_w,
	ie_nota_cancelada_w
from	fis_regra_efd_a100
where	nr_seq_regra_efd	= nr_seq_regra_efd_w;

select	count(1)
into	qt_registros_w
from	ctb_mes_ref
where	trunc(dt_referencia,'mm')	= trunc(dt_inicio_p,'mm')
and	cd_empresa			= cd_empresa_w;

if	(qt_registros_w = 1) then
	select	nr_sequencia
	into	nr_seq_data_w
	from 	ctb_mes_ref 
	where 	trunc(dt_referencia,'mm')	= trunc(dt_inicio_p,'mm')
	and	cd_empresa			= cd_empresa_w;
end if;

select	cd_tributo_pis
into	cd_tributo_pis_w
from	fis_regra_efd
where	nr_sequencia	= nr_seq_regra_efd_w;

select 	max(pr_imposto)
into	aliquota_pis_w
from   	regra_calculo_imposto	r,
       	tributo			d
where  	exists	(select	1 
		from	fis_regra_efd	e, 
       	     		tributo		d 
		where 	d.cd_tributo	= e.cd_tributo_pis
		and   	r.cd_tributo	= e.cd_tributo_pis
		and	e.nr_sequencia	= nr_seq_regra_efd_w
		and   	ie_tipo_tributo = 'PIS');
	
select	nr_seq_regra_efd
into	nr_seq_regra_efd_w
from	fis_efd_controle
where	nr_sequencia	= nr_seq_controle_p;

select	ie_ind_incidencia_trib
into	ie_ind_incidencia_trib_w
from	fis_regra_efd
where	nr_sequencia	= nr_seq_regra_efd_w;

-- Verifca se ser� gerado o PIS sobre a folha de sal�rio
select	count(*)
into	qt_registros_w
from	fis_regra_efd_reg
where	cd_registro		= 'M350'
and	ie_gerar		= 'S'
and	nr_seq_regra_efd	= nr_seq_regra_efd_w;

if	(qt_registros_w > 0) then		-- Maior do que dois porque sempre vai ter marcado o de abertura e de encerramento
	ie_folha_salario_w	:= 'V';
else
	ie_folha_salario_w	:= 'F';
end if;

obter_param_usuario(5500,25,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_local_gerar_sped_w);

if	((ie_local_gerar_sped_w = 'N') or
	(ie_local_gerar_sped_w = 'A')) and
	(ie_folha_salario_w = 'F') then
	if	((ie_ind_incidencia_trib_w = 1) or
		(ie_ind_incidencia_trib_w = 3)) then  --Apenas regime n�o-cumulativo, ou ambos
		vl_retido_nao_cum_w		:= 0;
		vl_contribuicao_nao_cum_w	:= 0;
	end if;
	
	if	((ie_ind_incidencia_trib_w = 2) or
		(ie_ind_incidencia_trib_w = 3)) then  --Apenas regime cumulativo, ou ambos
		-- Calcular o valor da base de c�lculo do PIS (vl_base_calculo_w)  e o valor total do PIS que foi retido na fonte (vl_retido_w) com a al�quota b�sica (cadastrada nos cadastros gerais [shift + F11])
		select	sum(a.vl_base_calculo),
			sum(vl_pis_retido)
		into	vl_base_calculo_cumulativo_w,
			vl_retido_cumulativo_w
		from	fis_efd_nota_arq	a
		where	a.nm_usuario		= nm_usuario_p
		and	a.nr_seq_controle	= nr_seq_controle_p;
	end if;

	-- Valor da contribui��o  cumulativa
	vl_contribuicao_cumulativo_w	:= ((vl_base_calculo_cumulativo_w * aliquota_pis_w) / 100);
	
	select	count(*)
	into	qt_registros_w
	from	fis_efd_conta_contabil	c,
		fis_efd_regra_tipo_ct	r
	where	r.nr_sequencia					= c.nr_seq_tipo_ct
	and	nvl(c.cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
	and	nvl(c.cd_empresa, cd_empresa_p)			= cd_empresa_p
	and	r.ie_tipo_ct					= 'DD';
		
		if	(qt_registros_w > 0) then
			select	sum(round(decode(nvl(c.ie_tipo_valor,'VM'),'VM',a.vl_movimento,'VC',a.vl_credito,'VD',a.vl_debito),2)) * aliquota_pis_w/100  vl_pis
			into	vl_deducoes_w
			from 	ctb_balancete_v		a,
				fis_efd_conta_contabil	c,
				fis_efd_regra_tipo_ct	r
			where 	a.cd_conta_contabil				= c.cd_conta_contabil
			and	r.nr_sequencia					= c.nr_seq_tipo_ct
			and	r.ie_tipo_ct					= 'DD'
			and	a.nr_seq_mes_ref				= nr_seq_data_w
			and	obter_empresa_estab(a.cd_estabelecimento)	= nvl(c.cd_empresa, cd_empresa_p)
			and	a.ie_normal_encerramento			= 'N';
		
			vl_base_calculo_cumulativo_w	:= vl_base_calculo_cumulativo_w - vl_deducoes_w;
		end if;
	-- Montagem dos campos do arquivo
	vl_campo_02_w	:= case when vl_contribuicao_nao_cum_w > 0 then vl_contribuicao_nao_cum_w else 0 end;
	vl_campo_05_w	:= vl_campo_02_w;
	vl_campo_06_w	:= case when vl_retido_nao_cum_w > 0 then vl_retido_nao_cum_w else 0 end;
	vl_campo_08_w	:= nvl(vl_campo_02_w - vl_campo_06_w,0);
	vl_campo_08_w	:= case when vl_campo_08_w > 0 then vl_campo_08_w else 0 end;
	vl_campo_09_w	:= case when vl_contribuicao_cumulativo_w > 0 then vl_contribuicao_cumulativo_w else 0 end;
	vl_campo_10_w	:= case when vl_retido_cumulativo_w > 0 then vl_retido_cumulativo_w else 0 end;
	
	if	(vl_campo_10_w > vl_campo_09_w) then
		vl_campo_10_w	:= vl_campo_09_w;
	end if;
	
	vl_campo_11_w	:= case when vl_deducoes_w > 0 then vl_deducoes_w else 0 end;
	vl_campo_11_w	:= case when vl_campo_11_w > 0 and ie_gerar_F700_w = 'S' then vl_campo_11_w else 0 end;
	vl_campo_12_w	:= nvl(((vl_campo_02_w - vl_campo_06_w) + (vl_campo_09_w - vl_retido_cumulativo_w - vl_campo_11_w)),0);
	vl_campo_12_w	:= case when vl_campo_12_w > 0 then vl_campo_12_w else 0 end;
	vl_campo_13_w	:= vl_campo_08_w + vl_campo_12_w;
	vl_campo_13_w	:= case when vl_campo_13_w > 0 then vl_campo_13_w else 0 end;

	-- Montagem das linhas do arquivo
	ds_linha_w	:=	sep_w || 'M200' 							|| -- Campo 01 - Registro
				sep_w || replace(campo_mascara(nvl(vl_campo_02_w,0),2),'.',',')		|| -- Campo 02 - Contribui��o n�o cumulativa
				sep_w || 0								|| -- Campo 03 - Cr�dito anterior per�odo atual (n�o cumulativo)
				sep_w || 0								|| -- Campo 04 - Cr�dito per�odos anteriores (n�o cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_05_w,0),2),'.',',')		|| -- Campo 05 - Total Contribui��o n�o cumulativa (Campo 2 - campo 3 - campo4)
				sep_w || replace(campo_mascara(nvl(vl_campo_06_w,0),2),'.',',')		|| -- Campo 06 - Valor retido na fonte (n�o cumulativo)
				sep_w || 0 								|| -- Campo 07 - Outras dedu��es (n�o cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_08_w,0),2),'.',',')		|| -- Campo 08 - Contribui��o a recolher (Campo 6 - campo7 - campo8)
				sep_w || replace(campo_mascara(nvl(vl_campo_09_w,0),2),'.',',')		|| -- Campo 09  - Contribui��o cumulativa
				sep_w || replace(campo_mascara(nvl(vl_campo_10_w,0),2),'.',',')		|| -- Campo 10 - Valor retido na fonte (cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_11_w,0),2),'.',',')		|| -- Campo 11 - Outras dedu��es (cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_12_w,0),2),'.',',')		|| -- Campo 12 - Contribui��o cumulativa  a pagar (Campo09 - campo10 - campo11 )
				sep_w || replace(campo_mascara(nvl(vl_campo_13_w,0),2),'.',',') 	|| -- Campo 13 - Contribui��o a recolher (Campo08 + campo12)
				sep_w;   

	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;
		
	insert into fis_efd_arquivo
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_controle_efd,
		nr_linha,
		ds_arquivo,
		ds_arquivo_compl,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_registro)
	values	(nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		nr_linha_w,
		ds_arquivo_w,
		ds_arquivo_compl_w,
		sysdate,
		nm_usuario_p,
		'M200'); 
		
	commit;

	vl_base_calculo_tributo_w	:= vl_base_calculo_cumulativo_w + vl_base_calculo_nao_cum_w;
elsif	(ie_local_gerar_sped_w = 'C') and
	(ie_folha_salario_w = 'F') then
	select  sum(vl_base_tributo)
	into	vl_base_calculo_cum_w
	from	w_efd_dados_arq;

	select	sum(y.vl_operacao) * 1.65/100
	into	vl_contribuicao_nao_cum_w
	from	(select	a.vl_movimento 		vl_operacao
		from 	ctb_balancete_v a
		where 	a.nr_seq_mes_ref		= nr_seq_data_w
		and	a.cd_conta_contabil		= '2441'
		and	a.cd_estabelecimento		= cd_estabelecimento_p
		and	a.ie_normal_encerramento	= 'N') y;

	vl_base_calculo_tributo_w	:= vl_base_calculo_cum_w;		--Para passar como par�metro para o M210
	vl_base_calculo_nao_cum_w 	:= vl_contribuicao_nao_cum_w;			
	vl_contribuicao_cum_w		:= (vl_base_calculo_tributo_w * aliquota_pis_w)/100;
	
	select	round(sum(y.vl_pis),2) vl_pis
	into	vl_retido_cum_w
	from	(select	a.cd_cgc,
			b.dt_recebimento,
			sum(c.vl_adicional) vl_pis,
			null vl_cofins,
			null vl_base_pis
		from 	convenio a,
			convenio_receb b,
			convenio_receb_adic c,
			transacao_financeira d
		where 	a.cd_convenio = b.cd_convenio
		and 	b.nr_sequencia = c.nr_seq_receb
		and 	c.nr_seq_trans_financ = d.nr_sequencia
		and	d.nr_sequencia = 938 -- pis
		and	b.dt_recebimento between dt_inicio_p and dt_fim_p
		group by a.cd_cgc, b.dt_recebimento
		union
		select	a.cd_cgc,
			b.dt_recebimento,
			null vl_pis,
			sum(c.vl_adicional) vl_cofins,
			null vl_base_pis
		from 	convenio a,
			convenio_receb b,
			convenio_receb_adic c,
			transacao_financeira d
		where 	a.cd_convenio = b.cd_convenio
		and 	b.nr_sequencia = c.nr_seq_receb
		and 	c.nr_seq_trans_financ = d.nr_sequencia
		and	d.nr_sequencia = 939 -- cofins
		and	b.dt_recebimento between dt_inicio_p and dt_fim_p
		group by a.cd_cgc, b.dt_recebimento
		union
		select	a.cd_cgc,
			b.dt_recebimento,
			null vl_pis,
			null vl_cofins,
			sum(b.vl_recebimento) vl_base_pis
		from 	convenio a,
			convenio_receb b,
			convenio_receb_adic c,
			transacao_financeira d
		where 	a.cd_convenio = b.cd_convenio
		and 	b.nr_sequencia = c.nr_seq_receb
		and 	c.nr_seq_trans_financ = d.nr_sequencia
		and	d.nr_sequencia = 938 -- pis
		and	b.dt_recebimento between dt_inicio_p and dt_fim_p
		group by a.cd_cgc, b.dt_recebimento) y;

		select	count(*)
		into	qt_registros_w
		from	fis_efd_conta_contabil c,
			fis_efd_regra_tipo_ct r
		where	r.nr_sequencia = c.nr_seq_tipo_ct
		and	NVL(c.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
		and	NVL(c.cd_empresa, cd_empresa_p) = cd_empresa_p
		and	r.ie_tipo_ct = 'DD';
		
		if	(qt_registros_w > 0) then
			select	sum(round(decode(nvl(c.ie_tipo_valor,'VM'),'VM',a.vl_movimento,'VC',a.vl_credito,'VD',a.vl_debito),2)) * aliquota_pis_w/100  vl_cofins
			into	vl_deducoes_cum_w
			from 	ctb_balancete_v a,
				fis_efd_conta_contabil c,
				fis_efd_regra_tipo_ct r
			where 	a.cd_conta_contabil = c.cd_conta_contabil
			and	r.nr_sequencia = c.nr_seq_tipo_ct
			and	r.ie_tipo_ct					= 'DD'
			and	a.nr_seq_mes_ref				= nr_seq_data_w
			and	obter_empresa_estab(a.cd_estabelecimento)	= nvl(c.cd_empresa, cd_empresa_p)
			and	a.ie_normal_encerramento			= 'N';
		end if;
	
	vl_retido_nao_cum_w	:= 0;
	vl_deducoes_nao_cum_w	:= 0;

	vl_campo_02_w	:= case when vl_contribuicao_nao_cum_w > 0 then vl_contribuicao_nao_cum_w else 0 end;
	vl_campo_05_w	:= vl_campo_02_w;
	vl_campo_06_w	:= vl_retido_nao_cum_w;
	vl_campo_07_w	:= vl_deducoes_nao_cum_w;
	vl_campo_08_w	:= nvl((vl_campo_02_w - vl_retido_nao_cum_w - vl_deducoes_nao_cum_w),0);
	vl_campo_08_w	:= case when vl_campo_08_w > 0 then vl_campo_08_w else 0 end;
	vl_campo_09_w	:= case when vl_contribuicao_cum_w > 0 then vl_contribuicao_cum_w else 0 end;
	vl_campo_10_w	:= case when vl_retido_cum_w > 0 then vl_retido_cum_w else 0 end;
	
	if	(vl_campo_10_w > vl_campo_09_w) then
		vl_campo_10_w	:= vl_campo_09_w;
	end if;
	
	vl_campo_11_w	:= case when vl_deducoes_cum_w > 0 then vl_deducoes_cum_w else 0 end;
	vl_campo_12_w	:= nvl((vl_campo_09_w - vl_campo_10_w - vl_campo_11_w),0);
	vl_campo_12_w	:= case when vl_campo_12_w > 0 then vl_campo_12_w else 0 end;
	vl_campo_13_w	:= nvl((vl_campo_02_w - vl_retido_nao_cum_w - vl_deducoes_nao_cum_w) + (vl_campo_09_w - vl_retido_cum_w - vl_deducoes_cum_w),0);
	vl_campo_13_w	:= case when vl_campo_13_w > 0 then vl_campo_13_w else 0 end;
	
	-- Montagem das linhas do arquivo
	ds_linha_w	:=	sep_w || 'M200' 						|| -- Campo 01 - Registro
				sep_w || replace(campo_mascara(nvl(vl_campo_02_w,0),2),'.',',')	|| -- Campo 02 - Contribui��o n�o cumulativa
				sep_w || 0							|| -- Campo 03 - Cr�dito anterior per�odo atual (n�o cumulativo)
				sep_w || 0							|| -- Campo 04 - Cr�dito per�odos anteriores (n�o cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_05_w,0),2),'.',',')	|| -- Campo 05 - Total Contribui��o n�o cumulativa (Campo 2 - campo 3 - campo4)
				sep_w || replace(campo_mascara(nvl(vl_campo_06_w,0),2),'.',',')	|| -- Campo 06 - Valor retido na fonte (n�o cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_07_w,0),2),'.',',') || -- Campo 07 - Outras dedu��es (n�o cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_08_w,0),2),'.',',')	|| -- Campo 08 - Contribui��o a recolher (Campo 6 - campo7 - campo8) (n�o cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_09_w,0),2),'.',',')	|| -- Campo 09  - Contribui��o cumulativa
				sep_w || replace(campo_mascara(nvl(vl_campo_10_w,0),2),'.',',')	|| -- Campo 10 - Valor retido na fonte (cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_11_w,0),2),'.',',')	|| -- Campo 11 - Outras dedu��es (cumulativo)
				sep_w || replace(campo_mascara(nvl(vl_campo_12_w,0),2),'.',',') || -- Campo 12 - Contribui��o cumulativa  a pagar (Campo09 - campo10 - campo11 )
				sep_w || replace(campo_mascara(nvl(vl_campo_13_w,0),2),'.',',') || -- Campo 13 - Contribui��o a recolher (Campo08 + campo12)
				sep_w;

	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_efd_arquivo
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_controle_efd,
		nr_linha,
		ds_arquivo,
		ds_arquivo_compl,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_registro)
	values	(nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		nr_linha_w,
		ds_arquivo_w,
		ds_arquivo_compl_w,
		sysdate,
		nm_usuario_p,
		'M200');
		
	commit;
elsif	(ie_folha_salario_w = 'V') then
	-- Caso seja enviado apenas o PIS sobre folha de sal�rio, o M200 deve ser enviado zerado
	
	-- Montagem das linhas do arquivo
	ds_linha_w	:=	sep_w || 'M200'	|| -- Campo 01 - Registro
				sep_w || '0,00'	|| -- Campo 02 - Contribui��o n�o cumulativa
				sep_w || 0	|| -- Campo 03 - Cr�dito anterior per�odo atual (n�o cumulativo)
				sep_w || 0	|| -- Campo 04 - Cr�dito per�odos anteriores (n�o cumulativo)
				sep_w || '0,00'	|| -- Campo 05 - Total Contribui��o n�o cumulativa (Campo 2 - campo 3 - campo4)
				sep_w || '0,00'	|| -- Campo 06 - Valor retido na fonte (n�o cumulativo)
				sep_w || 0	|| -- Campo 07 - Outras dedu��es (n�o cumulativo)
				sep_w || '0,00'	|| -- Campo 08 - Contribui��o a recolher (Campo 6 - campo7 - campo8) (n�o cumulativo)
				sep_w || '0,00'	|| -- Campo 09  - Contribui��o cumulativa
				sep_w || '0,00'	|| -- Campo 10 - Valor retido na fonte (cumulativo)
				sep_w || 0	|| -- Campo 11 - Outras dedu��es (cumulativo)
				sep_w || '0,00'	|| -- Campo 12 - Contribui��o cumulativa  a pagar (Campo09 - campo10 - campo11 )
				sep_w || '0,00'	|| -- Campo 13 - Contribui��o a recolher (Campo08 + campo12)
				sep_w;
	commit;
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	vl_base_calculo_tributo_w := 0;
	
	insert into fis_efd_arquivo
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_controle_efd,
		nr_linha,
		ds_arquivo,
		ds_arquivo_compl,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_registro)
	values	(nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		nr_linha_w,
		ds_arquivo_w,
		ds_arquivo_compl_w,
		sysdate,
		nm_usuario_p,
		'M200'); 
		
	commit;
end if;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

if	(trunc(dt_inicio_p,'month') >= to_date('01/04/2014','dd/mm/yyyy')) then
	if	(vl_campo_08_w > 0) then
		vl_debito_w	:= vl_campo_08_w;
		nr_num_campo_w	:= '08';
		
		fis_gerar_reg_m205_efd(	nr_seq_controle_p,
					nm_usuario_p,
					cd_estabelecimento_p,
					dt_inicio_p,
					dt_fim_p,
					cd_empresa_p,
					ds_separador_p,
					vl_debito_w,
					nr_num_campo_w,
					cd_tributo_pis_w,
					qt_linha_p,
					nr_sequencia_p);
	end if;
	
	if	(vl_campo_12_w > 0) then
		vl_debito_w	:= vl_campo_12_w;
		nr_num_campo_w	:= '12';
		
		fis_gerar_reg_m205_efd(	nr_seq_controle_p,
					nm_usuario_p,
					cd_estabelecimento_p,
					dt_inicio_p,
					dt_fim_p,
					cd_empresa_p,
					ds_separador_p,
					vl_debito_w,
					nr_num_campo_w,
					cd_tributo_pis_w,
					qt_linha_p,
					nr_sequencia_p);
	end if;
end if;

fis_gerar_reg_m210_efd(	nr_seq_controle_p,
			nm_usuario_p,
			cd_estabelecimento_p,
			dt_inicio_p,
			dt_fim_p,
			cd_empresa_p,
			ds_separador_p,
			vl_base_calculo_tributo_w,
			qt_linha_p,
			nr_sequencia_p);

end fis_gerar_reg_m200_efd;
/
