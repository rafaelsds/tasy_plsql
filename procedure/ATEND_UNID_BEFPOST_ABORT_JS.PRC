create or replace
procedure atend_unid_befpost_abort_js(

					nr_seq_agrupamento_p		number,
					cd_tipo_acomod_conv_p		number,
					ie_tipo_acomod_convenio_p	varchar2,
					cd_estabelecimento_p		number,
					cd_categoria_p			varchar2,
					ie_acomod_lib_categ_p		varchar2,
					cd_tipo_acomodacao_p		number,
					ie_tipo_atendimento_p		number,
					ie_setor_acomod_p		varchar2,
					ie_insert_p			varchar2,
					cd_pessoa_fisica_p		varchar2,
					cd_unidade_compl_p		varchar2,
					cd_unidade_basica_p		varchar2,
					nr_atendimento_p		number, 
					cd_convenio_p			number, 
					cd_plano_p			varchar2, 
					cd_setor_atendimento_p		number,
					ie_edicao_p			varchar2,
					dt_saida_p			date,
					dt_saida_old_p			date,
					nm_usuario_p			varchar2,
					ds_msg_abort_p      	    out varchar2) is 

ie_cont_faixa_etaria_w	varchar2(1);
ie_permite_unidade_w	varchar2(1);
ie_acomod_lib_w		varchar2(1);
qt_setor_agrup_w	number(10);
ds_agrupamento_w	varchar2(255);
ds_setor_atendimento_w	varchar2(100);
					
begin

if	(ie_edicao_p = 'S') then

	if	(dt_saida_p <> dt_saida_old_p) then
	
		ds_msg_abort_p := substr(wheb_mensagem_pck.get_texto(96387, null),1,255);
		goto final;
	end if;

end if;

consiste_regra_horario_plano(nr_atendimento_p, cd_convenio_p, cd_plano_p, cd_setor_atendimento_p, ds_msg_abort_p, null);
if	(ds_msg_abort_p is not null) then
	goto final;
end if;

select	max(ie_controle_faixa_etaria)
into 	ie_cont_faixa_etaria_w
from	unidade_atendimento
where	cd_setor_atendimento = cd_setor_atendimento_p
and	cd_unidade_basica = cd_unidade_basica_p
and	cd_unidade_compl = cd_unidade_compl_p;

if	(ie_cont_faixa_etaria_w = 'S') then
	consiste_se_faixa_etaria_pac(cd_pessoa_fisica_p, cd_setor_atendimento_p, ie_permite_unidade_w); 
	if	(nvl(ie_permite_unidade_w,'S') = 'N') then
		ds_msg_abort_p := substr(wheb_mensagem_pck.get_texto(230835, null),1,255);
		goto final;
	end if;
end if;


if	((ie_insert_p = 'S') or 
	(ie_edicao_p = 'S')) and
	(ie_setor_acomod_p <> 'S') and
	(ie_tipo_atendimento_p = 1) and
	(nvl(cd_tipo_acomodacao_p,0) = 0) then
	ds_msg_abort_p := substr(wheb_mensagem_pck.get_texto(96521, null),1,255);
	goto final;
end if;

if	(ie_acomod_lib_categ_p = 'A') then
	ie_acomod_lib_w:= obter_se_acomod_lib_estab(cd_convenio_p,cd_categoria_p,cd_tipo_acomodacao_p,cd_estabelecimento_p); 
	
	if	(ie_acomod_lib_w = 'N') then
		ds_msg_abort_p := substr(wheb_mensagem_pck.get_texto(96534, null),1,255);
		goto final;
	end if;
end if;

if	(ie_tipo_acomod_convenio_p = 'S') and
	(cd_tipo_acomod_conv_p <> cd_tipo_acomodacao_p) then
	ds_msg_abort_p := substr(wheb_mensagem_pck.get_texto(96535, null),1,255);
	goto final;
end if;

if	(nvl(nr_seq_agrupamento_p,0) > 0) then
	
	select 	count(*) 
	into	qt_setor_agrup_w
	from	setor_atendimento
	where	nr_seq_agrupamento = nr_seq_agrupamento_p
	and	cd_setor_atendimento = cd_setor_atendimento_p;
	if	(qt_setor_agrup_w = 0) then
		select 	substr(obter_desc_agrup_setor(nr_seq_agrupamento_p),1,255), 
                        substr(obter_nome_setor(cd_setor_atendimento_p),1,100) 
		into	ds_agrupamento_w,
			ds_setor_atendimento_w
                from	dual;
		if	(ds_agrupamento_w is not null) and
			(ds_setor_atendimento_w is not null) then
			ds_msg_abort_p := substr(wheb_mensagem_pck.get_texto(307869, 'DS_SETOR='||ds_setor_atendimento_w ||';DS_AGRUPAMENTO=' ||ds_agrupamento_w),1,255);
			goto final;
		end if;
	end if;
end if;

if	(ie_insert_p = 'S') then
	regra_alterar_local_status(nr_atendimento_p, 'GA', nm_usuario_p, ds_msg_abort_p, null);
	if	(ds_msg_abort_p is not null) then
		goto final;
	end if;
end if;

<<final>>

commit;

end atend_unid_befpost_abort_js;
/