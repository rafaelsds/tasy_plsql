create or replace
procedure atualiza_biometria_conv_atend (	nr_atendimento_p				number,
											nr_seq_interno_p				number,
											nr_biometria_conv_p				number,
											nr_seq_just_biometria_conv_p	number,
											nm_usuario_p					varchar2) is 
ds_erro_w		varchar2(255);
												
begin

if	(nr_atendimento_p is not null) and
	(nr_seq_interno_p is not null) and
	(nr_biometria_conv_p is not null) then
	begin
	
	update	atend_categoria_convenio
	set		NR_BIOMETRIA_CONV 			= nr_biometria_conv_p,
			NR_SEQ_JUST_BIOMETRIA_CONV	= nr_seq_just_biometria_conv_p
	where	nr_seq_interno = nr_seq_interno_p
	and		nr_atendimento = nr_atendimento_p;
	
	exception
	when others then
		ds_erro_w	:= substr(sqlerrm,1,255);
	end;	
end if;	

commit;

end atualiza_biometria_conv_atend;
/
