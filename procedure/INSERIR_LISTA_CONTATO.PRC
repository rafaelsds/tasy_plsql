create or replace
procedure inserir_lista_contato(
			nm_usuario_base_p	varchar2,
			nm_usuario_destino_p	varchar2,
			nm_usuario_nrec_p	varchar2) is

ds_apelido_w		varchar2(50);
nm_usuario_amigo_w	varchar2(15);
nr_sequencia_w		number;

				
			
cursor c01 is
select	a.ds_apelido,
	a.nm_usuario_amigo
from	usuario_amigo a,
	usuario b
where	a.nm_usuario_base = b.nm_usuario
and	b.nm_usuario = nm_usuario_base_p
and not exists (select	1
		from	usuario_amigo x
		where	x.nm_usuario_base = nm_usuario_destino_p
		and	x.nm_usuario_amigo = a.nm_usuario_amigo);
			
			
begin

open c01;
loop
fetch c01 into
	ds_apelido_w,
	nm_usuario_amigo_w;
exit when c01%notfound;
	begin
	
	select	usuario_amigo_seq.nextval
	into	nr_sequencia_w
	from	dual;

	
	insert into usuario_amigo ( 	     
		nr_sequencia,           
		nm_usuario_base,        
		nm_usuario_amigo,       
		dt_atualizacao,         
		nm_usuario,             
		ds_apelido,             
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_sequencia_w,
		nm_usuario_destino_p,
		nm_usuario_amigo_w,
		sysdate,
		nm_usuario_nrec_p,
		ds_apelido_w,
		sysdate,
		nm_usuario_nrec_p);

	end;
end loop;
close c01;

commit;

end inserir_lista_contato;
/