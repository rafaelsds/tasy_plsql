create or replace
procedure CCD_GENERATE_OS(
		cd_request_user_p		varchar2,
		nm_request_user_p		varchar2,
		ds_dano_breve_p			varchar2,
		ds_dano_p			varchar2,
		nr_seq_gerencia_p		number,
		nr_seq_grupo_desenv_p		number,
		ie_classification_p		varchar2,
		nm_exec_user_p			varchar2,
		nr_seq_order_p out		number) is

nr_seq_ordem_w			man_ordem_servico.nr_sequencia%type;
nr_seq_estagio_w		man_ordem_servico.nr_seq_estagio%type;
begin

nr_seq_estagio_w	:= 1051; --Desenv aguardando triagem

if	(nr_seq_gerencia_p = 2) then
	nr_seq_estagio_w	:= 1731; -- Tec aguardando triagem
end if;

select	man_ordem_servico_seq.nextval
into	nr_seq_ordem_w
from	dual;

insert into man_ordem_servico (
	nr_sequencia,
	dt_ordem_servico,
	cd_pessoa_solicitante,
	nr_seq_localizacao,
	nr_seq_equipamento,
	ds_dano_breve,
	ie_prioridade,
	ds_dano,
	nr_seq_grupo_des,
	ie_classificacao,
	ie_tipo_ordem,
	ie_status_ordem,
	dt_inicio_previsto,
	dt_fim_previsto,
	dt_inicio_desejado,
	dt_conclusao_desejada,
	nr_seq_estagio,
	nr_grupo_trabalho,
	nr_grupo_planej,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ie_parado,
	ie_obriga_news,
	ie_exclusiva,
	ie_atualizacao_migracao,
	ie_resp_teste,
	ie_plataforma,
	nr_seq_complex,
	ie_origem_os,
	ie_os_relatorio,
	ie_obrigar_avaliacao,
	ie_gerado_projeto)
values	(
	nr_seq_ordem_w,
	sysdate,
	cd_request_user_p,
	41,
	41,
	ds_dano_breve_p,
	'M',
	ds_dano_p,
	nr_seq_grupo_desenv_p,
	ie_classification_p,
	1,
	'1',
	sysdate,
	sysdate+7,
	sysdate,
	sysdate+7,
	nr_seq_estagio_w,
	1,
	1,
	'Tasy',
	sysdate,
	'Tasy',
	sysdate,
	'N',
	'N',
	'G',
	'P',
	'A',
	'H',
	2,
	1,
	'N',
	'N',
	'N');
	
if (nm_request_user_p is not null) then
	insert into man_ordem_servico_exec (
			nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_usuario_exec,
			qt_min_prev,
			nr_seq_tipo_exec)
	values (man_ordem_servico_exec_seq.nextval,
			nr_seq_ordem_w,
			sysdate,
			'Tasy',
			sysdate,
			'Tasy',
			nm_request_user_p,
			10,
			5);
end if;

insert into man_ordem_servico_exec (
	nr_sequencia,
	nr_seq_ordem,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nm_usuario_exec,
	qt_min_prev,
	nr_seq_tipo_exec)
values (man_ordem_servico_exec_seq.nextval,
	nr_seq_ordem_w,
	sysdate,
	'Tasy',
	sysdate,
	'Tasy',
	nm_exec_user_p,
	30,
	2);

nr_seq_order_p	:= nr_seq_ordem_w;
	
end CCD_GENERATE_OS;
/	