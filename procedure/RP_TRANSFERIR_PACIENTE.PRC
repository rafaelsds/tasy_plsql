create or replace
procedure rp_transferir_paciente(nr_seq_modelo_pac_p	number,
				 nr_seq_motivo_p	varchar2,
				 nr_seq_modelo_dest_p	varchar2,
				 nr_seq_pac_reab_p	number,
				 nm_usuario_p		Varchar2) is 

begin

/* Insere o paciente no novo modelo*/
inserir_modelo_transferencia(nr_seq_modelo_dest_p,nr_seq_modelo_pac_p,nr_seq_pac_reab_p,nm_usuario_p);

/* Finaliza o modelo e seus agendamentos */
rp_finalizar_modelo_agend(nr_seq_modelo_pac_p,nr_seq_motivo_p,nm_usuario_p,'M',sysdate);

commit;

end rp_transferir_paciente;
/
