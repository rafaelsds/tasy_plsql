create or replace
procedure pls_atualizar_ordem_regra_comb
			(	nr_seq_regra_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

nr_seq_motivo_glosa_w		number(10);
qt_glosa_w			number(10);
nr_ordem_execucao_regra_w	pls_cp_cta_combinada.nr_ordem_execucao%type;

begin
nr_ordem_execucao_regra_w	:= pls_filtro_regra_preco_cta_pck.obter_ordem_sugerida_regra(nr_seq_regra_p);

update	pls_cp_cta_combinada	
set	nr_ordem_execucao	= nr_ordem_execucao_regra_w
where	nr_sequencia		= nr_seq_regra_p;

commit;

end pls_atualizar_ordem_regra_comb;
/
