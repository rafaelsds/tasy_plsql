create or replace procedure request_remand_nur_sum(nr_sequencia_P number,
                                          ds_comment_p varchar2) is

nr_seq_sum_enf_w    sumario_enfermagem.nr_sequencia%type;
dt_aprovacao_w      sumario_enf_conf_list.dt_aprovacao%type;

begin

select nr_seq_sum_enf,
      dt_aprovacao
into nr_seq_sum_enf_w,
     dt_aprovacao_w
from sumario_enf_conf_list
where nr_sequencia = nr_sequencia_p;

if(nr_seq_sum_enf_w > 0) then

  if(dt_aprovacao_w is not null) then  
    update sumario_enf_conf_list
    set ie_remand = 'S',
        dt_aprovacao = null,
        nm_aprovador = null,
        ds_remand = ds_comment_p
    where nr_sequencia = nr_sequencia_p;    
  else 
    update sumario_enf_conf_list
    set ie_remand = 'S',
        ds_remand = ds_comment_p
    where nr_sequencia = nr_sequencia_p;
  end if;

  update sumario_enfermagem
  set dt_liberacao = null,
      ie_remand = 'S',
      ds_remand = ds_comment_p,
      DT_APPROVED = null
  where nr_sequencia = nr_seq_sum_enf_w;

  commit;

end if;

end request_remand_nur_sum;
/
