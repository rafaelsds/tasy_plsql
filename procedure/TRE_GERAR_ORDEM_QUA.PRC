create or replace
procedure tre_gerar_ordem_qua(cd_pessoa_solicitante_p   varchar2,
				ds_dano_breve_p 	varchar2,
				ds_dano_p 		varchar2,
				nr_seq_criterio_p	varchar2,
				nm_usuario_p		varchar2) is 

nr_sequencia_w			number(10);				
				
begin

select	man_ordem_servico_seq.nextval
into	nr_sequencia_w
from	dual;

insert into man_ordem_servico  (nr_sequencia, 
				cd_pessoa_solicitante, 
				dt_ordem_servico, 
				ie_prioridade, 
				ie_parado, 
				ds_dano_breve, 
				dt_atualizacao, 
				nm_usuario, 
				ds_dano, 
				ie_tipo_ordem)
values				(nr_sequencia_w,
				cd_pessoa_solicitante_p,
				sysdate,
				'M',
				'N',
				ds_dano_breve_p,
				sysdate,
				nm_usuario_p,
				ds_dano_p,
				13);
				
update 	QMS_TREIN_CLASSIF_CRIT
set	NR_SEQ_ORDEM = 	nr_sequencia_w
where 	nr_sequencia = 	nr_seq_criterio_p;	
				
commit;

end tre_gerar_ordem_qua;
/