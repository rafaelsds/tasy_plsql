create or replace
procedure altera_detalhe_intercor_js (
			nr_seq_pac_inter_p	number) is 

begin

delete	from pac_atend_interc_det 
where	nr_seq_pac_inter = nr_seq_pac_inter_p;

commit;

end altera_detalhe_intercor_js;
/