create or replace
procedure pls_consistir_reaj_inscricao
			(nr_seq_contrato_p	in	number,
			nr_seq_regra_inscricao_p in	number,
			nm_usuario_p		in	varchar2,
			cd_estabelecimento_p	in	number,
			ds_erro_p		out	varchar2) is 

qt_regra_w	number(10);

begin

select	count(*)
into	qt_regra_w
from	pls_regra_inscricao a
where	a.nr_sequencia		= nr_seq_regra_inscricao_p
and	exists	(select	1
		from	pls_regra_inscricao x
		where	x.nr_sequencia		= a.nr_sequencia
		and	x.nr_seq_contrato	= nr_seq_contrato_p
		union
		select	1
		from	pls_contrato		y,
			pls_contrato_plano	w,
			pls_plano		z,
			pls_regra_inscricao	q
		where	y.nr_sequencia		= nr_seq_contrato_p
		and	w.nr_seq_contrato	= y.nr_sequencia
		and	w.nr_seq_plano		= z.nr_sequencia
		and	q.nr_seq_plano		= z.nr_sequencia
		and	q.nr_sequencia		= a.nr_sequencia);

if	(qt_regra_w = 0) then
	ds_erro_p	:= wheb_mensagem_pck.get_texto(280076);
end if;

commit;

end pls_consistir_reaj_inscricao;
/