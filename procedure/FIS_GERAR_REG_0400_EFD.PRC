create or replace procedure
Fis_Gerar_Reg_0400_Efd(	nr_seq_controle_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p			date,
				cd_empresa_p		number,
				ds_separador_p		varchar2,
				qt_linha_p	in out	number,
				nr_sequencia_p	in out	number) is

nr_seq_regra_efd_w		number(10);
nr_versao_efd_w			varchar2(5);
tp_registro_w			varchar2(4);

nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w			number(10) := nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1)	:= ds_separador_p;

cursor c01 is
select	'0400' tp_registro,
	a.cd_natureza_operacao,
	a.ds_natureza_operacao
from	natureza_operacao a
where	a.ie_situacao	= 'A'
order by 2;

vet01	C01%rowtype;

begin

open C01;
loop
fetch C01 into
	vet01;
exit when C01%notfound;
	begin
	ds_linha_w	:= substr(	sep_w || vet01.tp_registro		||
					sep_w || vet01.cd_natureza_operacao	||
					sep_w || vet01.ds_natureza_operacao	|| sep_w,1,8000);

	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_efd_arquivo(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_controle_efd,
		nr_linha,
		cd_registro,
		ds_arquivo,
		ds_arquivo_compl)
	values(	nr_seq_registro_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_controle_p,
		nr_linha_w,
		vet01.tp_registro,
		ds_arquivo_w,
		ds_arquivo_compl_w);
	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end Fis_Gerar_Reg_0400_Efd;
/