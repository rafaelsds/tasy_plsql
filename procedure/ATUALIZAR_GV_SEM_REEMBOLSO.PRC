create or replace
procedure atualizar_gv_sem_reembolso(  nr_sequencia_p		number,
										nr_seq_relat_desp_p number,
										nm_usuario_p		varchar2) is

vl_total_desp_w			number(15,2);
ie_gv_sem_reembolso_w	via_relat_desp.ie_gv_sem_reembolso%type;
nr_seq_viagem_w			via_relat_desp.nr_seq_viagem%type;	
ds_retorno_w			varchar2(255);

begin

if (nr_sequencia_p is not null) then

	/*Atualizar  se foi pagou ou nao por terceiro*/
	update via_relat_desp
    set    ie_gv_sem_reembolso 	= decode(nvl(ie_gv_sem_reembolso, 'N'), 'N', 'S', 'N')
    where  nr_sequencia 		= nr_seq_relat_desp_p
    and    nr_seq_fech_proj 	=  nr_sequencia_p;
	
	/*Pegar a situacao atualizada da GV*/
	select	max(ie_gv_sem_reembolso)
	into	ie_gv_sem_reembolso_w
	from	via_relat_desp
	where  nr_sequencia 		= nr_seq_relat_desp_p
    and    nr_seq_fech_proj 	=  nr_sequencia_p;

	if (ie_gv_sem_reembolso_w = 'S') then
		
		select	max(a.nr_seq_viagem)
		into	nr_seq_viagem_w
		from	via_relat_desp a
		where	a.nr_sequencia	=	nr_seq_relat_desp_p; 

		if (nr_seq_viagem_w is not null) then
		
			ds_retorno_w := null;
			
			gerenciar_viagem (nr_seq_viagem_w,
							  nm_usuario_p,
							  'F', 
							  ds_retorno_w,
							  'N'); --N�o para o commit. Pois commit ocorre no final dessa proc aqui..
					
			if (ds_retorno_w is not null) then
				wheb_mensagem_pck.exibir_mensagem_abort(1020093,'ds_retorno_p='||ds_retorno_w);
			end if;	

		end if;
		
	elsif (ie_gv_sem_reembolso_w = 'N') then
		
		/*OS 1670837 - Desfazer finaliza��o da viagem quando desfazer pago terceiro.*/
		select	max(a.nr_seq_viagem)
		into	nr_seq_viagem_w
		from	via_relat_desp a
		where	a.nr_sequencia	=	nr_seq_relat_desp_p;
		
		if (nr_seq_viagem_w is not null) then
		
				update	via_viagem
				set		ie_etapa_viagem			= '5',
						dt_fim_viagem			= null,
						nm_usuario_fim_viagem	= null,
						dt_atualizacao			= sysdate
				where	nr_sequencia			= nr_seq_viagem_w;
		
		end if;

	end if;

	commit;

end if;

end atualizar_gv_sem_reembolso;
/