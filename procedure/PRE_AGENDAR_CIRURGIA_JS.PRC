create or replace
procedure pre_agendar_cirurgia_js(
				 cd_agenda_p                    number,  
				 hr_inicio_p                    date,    
				 cd_pessoa_fisica_p             varchar2,
				 cd_medico_p                    varchar2,
				 nr_seq_proc_interno_p          number,  
				 cd_convenio_p                  number,  
				 nm_usuario_p                   varchar2,
				 cd_estabelecimento_p           number,  
				 ds_orientacao_p                varchar2,
				 ie_lado_p                      varchar2,
				 qt_tempo_proc_p                number,  			 
				 nm_paciente_p                  varchar2,
				 ds_erro_p		out	varchar2,
				 ie_carater_p			varchar2,
				nr_seq_atend_futuro_p		number default null,
				nr_atendimento_p		number default 0) is 
				
ds_w			varchar2(4000) := '';
ds_orientacoes_w	varchar2(4000);
cd_w			number(5);
ds_erro_w		varchar2(255) := '';
ds_erro_2_w		varchar2(255) := '';
nr_sequencia_w		number(15);
nr_sequencia_ww		number(15);
nr_seq_rtf_srtring_w	number(15);

				
cursor c01 is
select  1,
	nvl(ds_orientacao,' ') ds_orientacao,
	0 nr_sequencia
from    orientacao_cirurgia
where   nr_sequencia         =  (select   max(nr_sequencia)
				from     orientacao_cirurgia)
union
select	2,
	nvl(ds_orientacao_usuario,' ') ds_orientacao,
	0 nr_sequencia
from	proc_interno
where	nr_sequencia		   =	nr_seq_proc_interno_p
union
select	3,
	nvl(ds_orientacao,' ') ds_orientacao,
	0 nr_sequencia
from	proc_interno_conv_orient
where	nr_seq_proc_interno	=	nr_seq_proc_interno_p
and	cd_convenio is null
union
select	4,
	nvl(ds_orientacao,' ') ds_orientacao,
	0 nr_sequencia
from	proc_interno_conv_orient
where	nr_seq_proc_interno	=	nr_seq_proc_interno_p
and	cd_convenio 		   =	cd_convenio_p
union
select	5,
	' ' ds_orientacao,
	nr_sequencia
from	convenio_doc_atend
where	cd_convenio		      =	cd_convenio_p
and	ie_documento		   =	'CIR';

begin

ds_erro_p := '';

open C01;
loop
fetch C01 into	
	cd_w,
	ds_w,
	nr_sequencia_ww;
exit when C01%notfound;
	begin
	if	(cd_w = 5) then
		converte_rtf_string('	select	ds_documento
					from	convenio_doc_atend 
					where	nr_sequencia = :nr_sequencia_p', nr_sequencia_ww, nm_usuario_p, nr_seq_rtf_srtring_w);
		select	dbms_lob.substr(ds_texto_clob,4000,1)
		into	ds_w
		from	tasy_conversao_rtf
		where	nr_sequencia = nr_seq_rtf_srtring_w;
	end if;	

	ds_orientacoes_w := ds_orientacoes_w || ds_w ||chr (10);

	end;
end loop;
close C01;

if	(ds_orientacoes_w = '') then
	ds_orientacoes_w := wheb_mensagem_pck.get_texto(278084);
end if;

pre_agendar_cirurgia(cd_agenda_p, hr_inicio_p, cd_pessoa_fisica_p, cd_medico_p, nr_seq_proc_interno_p, cd_convenio_p, nm_usuario_p, cd_estabelecimento_p, ds_orientacoes_w,
			ie_lado_p, qt_tempo_proc_p, ds_erro_w, nr_sequencia_w, nm_paciente_p,ie_carater_p,ds_erro_2_w,nr_seq_atend_futuro_p,nr_atendimento_p,null, null);

if	(nvl(nr_sequencia_w,0) > 0) then
	gerar_dados_proc_adic(nr_sequencia_w, nm_usuario_p, cd_estabelecimento_p);
end if;

ds_erro_p := ds_erro_w;

end pre_agendar_cirurgia_js;
/
