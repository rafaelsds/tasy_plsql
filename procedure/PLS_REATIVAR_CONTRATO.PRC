create or replace procedure pls_reativar_contrato
			(	nr_seq_contrato_p		number,
				nr_seq_segurado_p		number,
				nr_seq_pagador_p		number,
				nr_seq_subestipulante_p		number,
				dt_reativacao_p			date,
				ds_observacao_p			varchar2,
				ie_dependente_p			varchar2,
				ie_reativa_sub_contrato_p	varchar2,
				ie_reativar_pagador_p		varchar2,
				ie_reativar_sca_p		varchar2,
				ie_titular_p			varchar2,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				ie_commit_p			varchar2) is

nr_seq_segurado_w		number(10);
nr_seq_dependente_w		number(10);
dt_rescisao_contrato_w		date;
dt_rescisao_pagador_w		date;
dt_rescisao_subestipulante_w	date;
dt_rescisao_w			date;
nr_seq_contrato_w		number(10);
qt_dias_valid_cart_w		number(5);
dt_validade_w			date;
ie_tipo_rescisao_w		varchar2(2);
nr_seq_plano_w			number(10);
nr_seq_sub_contrato_w		number(10);
dt_rescisao_depen_w		date;
nr_seq_titular_w		number(10);
cd_matricula_familia_w		number(10);
ie_titular_familia_w		varchar2(1);
nr_seq_seg_titular_w		number(10);
nm_pessoa_fisica_w		varchar2(255);
nr_seq_pagador_w		number(10);
nr_seq_segurado_tab_w		number(10);
ds_erro_w			varchar2(255);
ie_reativar_sca_w		varchar2(1);
nr_seq_segurado_sca_w		pls_segurado.nr_sequencia%type;
nr_seq_vinculo_sca_w		pls_sca_vinculo.nr_sequencia%type;
nr_seq_seg_repasse_w		pls_segurado_repasse.nr_sequencia%type;
nr_seq_motivo_rescisao_w	pls_contrato.nr_seq_motivo_rescisao%type;
nr_seq_tabela_seg_w		pls_tabela_preco.nr_sequencia%type;
qt_vidas_w			pls_integer;
qt_vidas_ant_w			pls_integer;
qt_registro_w			pls_integer;
nr_indice_w			pls_integer;
tb_nr_seq_segurado_w		pls_util_cta_pck.t_number_table;
ds_mensagem_reativacao_pag_w	varchar2(4000);

cursor c01 is
	select	nr_sequencia
	from	(	select	nr_sequencia,
				nr_seq_titular
			from	pls_segurado
			where	nr_seq_contrato	= nr_seq_contrato_p
			and	dt_rescisao is not null
			and	nr_seq_titular is null
			and	ie_dependente_p = 'N'
			and	nvl(ie_titular_p,'N') = 'S'
			and	(ie_tipo_rescisao = 'C' or nr_seq_motivo_cancelamento = nr_seq_motivo_rescisao_w)
			union
			select	nr_sequencia,
				nr_seq_titular
			from	pls_segurado
			where	nr_seq_contrato	= nr_seq_contrato_p
			and	dt_rescisao is not null
			and	ie_dependente_p	= 'S'
			and	nvl(ie_titular_p, 'N') = 'S'
			and	(ie_tipo_rescisao = 'C' or nr_seq_motivo_cancelamento = nr_seq_motivo_rescisao_w))
	order by decode(nr_seq_titular,null,-1,1);

cursor c02 is
	select	nr_sequencia
	from	pls_segurado
	where	nr_seq_titular	= nr_seq_segurado_p
	and	trunc(dt_rescisao,'dd')	= trunc(dt_rescisao_w,'dd')
	and	ie_dependente_p	= 'S'
	order by decode(nr_seq_titular,null,-1,1);

cursor c03 is
	select	nr_sequencia
	from	(	select	nr_sequencia,
				nr_seq_titular
			from	pls_segurado
			where	nr_seq_subestipulante	= nr_seq_subestipulante_p
			and	dt_rescisao is not null
			and	nr_seq_titular is null
			and	(nvl(ie_tipo_rescisao,'S')	= 'S')
			union
			select	nr_sequencia,
				nr_seq_titular
			from	pls_segurado
			where	nr_seq_subestipulante	= nr_seq_subestipulante_p
			and	dt_rescisao is not null
			and	nr_seq_titular is not null
			and	ie_dependente_p	= 'S'
			and	(nvl(ie_tipo_rescisao,'S')	= 'S'))
	order by decode(nr_seq_titular,null,-1,1);

cursor c04 is
	select	nr_sequencia
	from	pls_segurado
	where	nr_seq_pagador	= nr_seq_pagador_p
	and	dt_rescisao is not null
	and	(nvl(ie_tipo_rescisao,'P')	= 'P')
	order by decode(nr_seq_titular,null,-1,1);

Cursor C05 is
	select	nr_sequencia
	from	pls_contrato
	where	nr_contrato_principal		= nr_seq_contrato_p
	and	ie_reativa_sub_contrato_p	= 'S';

Cursor C06 is
	select	nr_sequencia
	from	(	select	nr_sequencia,
				nr_seq_titular
			from	pls_segurado
			where	nr_seq_contrato	= nr_seq_sub_contrato_w
			and	dt_rescisao is not null
			and	nr_seq_titular is null
			and	ie_dependente_p = 'N'
			and	(nvl(ie_tipo_rescisao,'C')	= 'C')
			and	ie_reativa_sub_contrato_p	= 'S'
			union
			select	nr_sequencia,
				nr_seq_titular
			from	pls_segurado
			where	nr_seq_contrato	= nr_seq_sub_contrato_w
			and	dt_rescisao is not null
			and	ie_dependente_p	= 'S'
			and	(nvl(ie_tipo_rescisao,'C')	= 'C')
			and	ie_reativa_sub_contrato_p	= 'S')
	order by decode(nr_seq_titular,null,-1,1);

Cursor C08 is
	select	nr_sequencia
	from	pls_contrato_pagador
	where	nr_seq_contrato	= nr_seq_contrato_p
	and	dt_rescisao	is not null
	and	trunc(dt_rescisao,'dd') = dt_rescisao_contrato_w;

Cursor C09 is
	select	nr_sequencia
	from	pls_contrato_pagador
	where	nr_seq_contrato	= nr_seq_sub_contrato_w
	and	dt_rescisao	is not null
	and	trunc(dt_rescisao,'dd') = dt_rescisao_contrato_w;
	
Cursor C10 is
	select	nr_sequencia
	from	pls_segurado
	where	nr_seq_contrato	= nr_seq_contrato_w
	and	dt_liberacao is not null;

Cursor C11 is
	select	a.nr_sequencia
	from	pls_sca_vinculo a,
		pls_tabela_preco b
	where	a.nr_seq_tabela = b.nr_sequencia
	and	nvl(b.ie_preco_vidas_contrato,'N') = 'S' 
	and	a.nr_seq_segurado = nr_seq_segurado_sca_w;

-- repasse tanto quando for reativado por contrato
Cursor C12 is
	select  a.nr_sequencia,
		a.nr_seq_segurado
	from	pls_segurado_repasse 	a,
		pls_segurado 		b
	where	b.nr_sequencia 		= a.nr_seq_segurado
	and	b.nr_seq_contrato 	= nr_seq_contrato_p
	and	a.dt_fim_repasse	is not null
	and	trunc(a.dt_fim_repasse,'dd') = dt_rescisao_contrato_w;

begin

ie_titular_familia_w	:= nvl(obter_valor_param_usuario(1202,82,obter_perfil_ativo,nm_usuario_p,0),'S');

if	(ie_reativar_sca_p = 'N') then
	select	nvl(max(ie_reativar_sca_reativacao),'S')
	into	ie_reativar_sca_w
	from	pls_parametros
	where	cd_estabelecimento	= cd_estabelecimento_p;
else
	ie_reativar_sca_w	:= 'S';
end if;

nr_indice_w := 0;
tb_nr_seq_segurado_w.delete;

if	(nr_seq_contrato_p is not null) and
	(nvl(nr_seq_contrato_p,0) <> 0) then
	pls_consiste_data_sib(dt_reativacao_p, nm_usuario_p, cd_estabelecimento_p);
	
	select	trunc(dt_rescisao_contrato,'dd'),
		nr_seq_motivo_rescisao
	into	dt_rescisao_contrato_w,
		nr_seq_motivo_rescisao_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_p;
	
	open c01;
	loop
	fetch c01 into
		nr_seq_segurado_w;
	exit when c01%notfound;
		begin
		select	trunc(dt_rescisao,'dd')
		into	dt_rescisao_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
		
		if	(dt_rescisao_contrato_w	= dt_rescisao_w) then
			begin
			pls_reativar_segurado(	nr_seq_segurado_w, dt_reativacao_p, ds_observacao_p, 
						'N', nm_usuario_p);
			
			tb_nr_seq_segurado_w(nr_indice_w) := nr_seq_segurado_w;
			nr_indice_w := nr_indice_w + 1;
			
			if	(ie_reativar_sca_w = 'S') then
				update	pls_sca_vinculo
				set	dt_fim_vigencia	= null,
					dt_reativacao 	= dt_reativacao_p
				where	nr_seq_segurado	= nr_seq_segurado_w
				and	trunc(dt_fim_vigencia,'dd')	= trunc(dt_rescisao_contrato_w,'dd');
			end if;
			exception
			when others then
				ds_erro_w	:= '';
			end;
		end if;
		end;
	end loop;
	close c01;

	open c08;
	loop
	fetch c08 into
		nr_seq_pagador_w;
	exit when c08%notfound;
		begin
		update	pls_contrato_pagador
		set	dt_rescisao			= null,
			nm_usuario			= nm_usuario_p,
			dt_atualizacao			= sysdate,
			dt_reativacao			= dt_reativacao_p,
			nr_seq_motivo_cancelamento	= null,
			dt_suspensao			= null
		where	nr_sequencia			= nr_seq_pagador_w;
		
		insert into pls_pagador_historico
			(	nr_sequencia,nr_seq_pagador,cd_estabelecimento,dt_atualizacao,nm_usuario,
				dt_atualizacao_nrec,nm_usuario_nrec,ds_historico,dt_historico,nm_usuario_historico,
				ds_titulo,ie_origem,ie_tipo_historico)
		values	(	pls_pagador_historico_seq.nextval,nr_seq_pagador_w,cd_estabelecimento_p,sysdate,nm_usuario_p,
				sysdate,nm_usuario_p,wheb_mensagem_pck.get_texto(1136621),sysdate,nm_usuario_p,
				wheb_mensagem_pck.get_texto(1136622),'GC','S');
		end;
	end loop;
	close c08;

	update	pls_contrato
	set	dt_rescisao_contrato	= null,
		dt_limite_utilizacao	= null,
		ie_situacao		= '2',
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		dt_reativacao		= dt_reativacao_p,
		dt_cancelamento		= null,
		nr_seq_motivo_rescisao	= null,
		nr_seq_causa_rescisao   = null
	where	nr_sequencia		= nr_seq_contrato_p;
	
	pls_preco_beneficiario_pck.atualizar_desconto_benef(nr_seq_contrato_p, sysdate, null, 'N', nm_usuario_p, cd_estabelecimento_p);

	open c12;
	loop
	fetch c12 into
		nr_seq_seg_repasse_w,
		nr_seq_segurado_w;
	exit when c12%notfound;
		begin
		
		update	pls_segurado_repasse
		set	dt_fim_repasse	= null,
			dt_fim_real	= null,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_seg_repasse_w;
		
		pls_gerar_segurado_historico(
			nr_seq_segurado_w, '2', sysdate,
			'Repasse ' || nr_seq_seg_repasse_w ||' reativado em ' || to_char(dt_reativacao_p, 'dd/mm/yyyy'), ds_observacao_p, null, 
			dt_reativacao_p, null, null, 
			dt_reativacao_p, null, null, 
			null, null, null, 
			null, nm_usuario_p, 'S');
		
		end;
	end loop;
	close c12;
		
	open C05;
	loop
	fetch C05 into
		nr_seq_sub_contrato_w;
	exit when C05%notfound;
		begin
		
		select	trunc(dt_rescisao_contrato,'dd')
		into	dt_rescisao_contrato_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_sub_contrato_w;
		
		open C06;
		loop
		fetch C06 into
			nr_seq_segurado_w;
		exit when C06%notfound;
			begin
			select	trunc(dt_rescisao,'dd')
			into	dt_rescisao_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_segurado_w;
			
			if	(dt_rescisao_contrato_w	= dt_rescisao_w) then
				begin
				pls_reativar_segurado(	nr_seq_segurado_w, dt_reativacao_p, ds_observacao_p,
							'N', nm_usuario_p);
				
				tb_nr_seq_segurado_w(nr_indice_w) := nr_seq_segurado_w;
				nr_indice_w := nr_indice_w + 1;
				
				if	(ie_reativar_sca_w = 'S') then
					update	pls_sca_vinculo
					set	dt_fim_vigencia	= null,
						dt_reativacao 	= dt_reativacao_p
					where	nr_seq_segurado	= nr_seq_segurado_w
					and	trunc(dt_fim_vigencia,'dd')	= trunc(dt_rescisao_contrato_w,'dd');
				end if;
				exception
				when others then
					ds_erro_w	:= '';
				end;
			end if;
			end;
		end loop;
		close C06;

		open c09;
		loop
		fetch c09 into
			nr_seq_pagador_w;
		exit when c09%notfound;
			begin
			update	pls_contrato_pagador
			set	dt_rescisao			= null,
				nm_usuario			= nm_usuario_p,
				dt_atualizacao			= sysdate,
				dt_reativacao			= dt_reativacao_p,
				nr_seq_motivo_cancelamento	= null,
				dt_suspensao			= null
			where	nr_sequencia			= nr_seq_pagador_w;
			
			insert into pls_pagador_historico
				(	nr_sequencia,nr_seq_pagador,cd_estabelecimento,dt_atualizacao,nm_usuario,
					dt_atualizacao_nrec,nm_usuario_nrec,ds_historico,dt_historico,nm_usuario_historico,
					ds_titulo,ie_origem,ie_tipo_historico)
			values	(	pls_pagador_historico_seq.nextval,nr_seq_pagador_w,cd_estabelecimento_p,sysdate,nm_usuario_p,
					sysdate,nm_usuario_p,wheb_mensagem_pck.get_texto(1136621),sysdate,nm_usuario_p,
					wheb_mensagem_pck.get_texto(1136622),'GC','S');
			end;
		end loop;
		close c09;
		
		update	pls_contrato
		set	dt_rescisao_contrato	= null,
			dt_limite_utilizacao	= null,
			ie_situacao		= '2',
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			dt_reativacao		= dt_reativacao_p,
			dt_cancelamento		= null,
			nr_seq_motivo_rescisao	= null,
			nr_seq_causa_rescisao   = null
		where	nr_sequencia		= nr_seq_sub_contrato_w;
		
		pls_preco_beneficiario_pck.atualizar_desconto_benef(nr_seq_sub_contrato_w, sysdate, null, 'N', nm_usuario_p, cd_estabelecimento_p);
		
		end;
	end loop;
	close C05;
elsif	(nr_seq_segurado_p is not null) and
	(nvl(nr_seq_segurado_p,0) <> 0) then
	select 	max(nr_seq_contrato),
		max(dt_rescisao),
		max(nr_seq_titular),
		max(cd_matricula_familia),
		max(nr_seq_pagador)
	into	nr_seq_contrato_w,
		dt_rescisao_w,
		nr_seq_titular_w,
		cd_matricula_familia_w,
		nr_seq_pagador_w
	from 	pls_segurado
	where	nr_sequencia = nr_seq_segurado_p;
	
	if	(dt_rescisao_w is not null) then
		if	(trunc(dt_reativacao_p,'dd') < trunc(dt_rescisao_w,'dd')) then
			wheb_mensagem_pck.exibir_mensagem_abort(684275); --A data de reativacao deve ser posterior ou igual a data de rescisao
		end if;

		/*Consistir o parametro [82] - Permite apenas 1 titular por familia no contrato */
		if	(nvl(ie_titular_familia_w,'N') = 'S') and
			(nr_seq_titular_w is null) then
			select	max(nr_sequencia)
			into	nr_seq_seg_titular_w
			from	pls_segurado
			where	nr_seq_contrato		= nr_seq_contrato_w
			and	cd_matricula_familia	= cd_matricula_familia_w
			and	nr_sequencia	<> nr_seq_segurado_p
			and	nr_seq_titular is null
			and	dt_liberacao is not null
			and	dt_rescisao is null;
			
			if	(nr_seq_seg_titular_w is not null) then
				select	max(b.nm_pessoa_fisica)
				into	nm_pessoa_fisica_w
				from	pls_segurado	a,
					pessoa_fisica	b
				where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
				and	a.nr_sequencia		= nr_seq_seg_titular_w;
				
				if	(nm_pessoa_fisica_w is not null) then
					wheb_mensagem_pck.exibir_mensagem_abort( 192950, 'NR_SEQ_SEG_TITULAR=' || nr_seq_seg_titular_w || ';NM_PESSOA_FISICA=' || nm_pessoa_fisica_w );
					/*'E permitido apenas 1 titular ativo por matricula familiar!'||chr(13)||chr(10)||
					'Verifique parametro [82] - Permite apenas 1 titular por familia no contrato.'||chr(13)||chr(10)||
					'Titular ativo: '||nr_seq_seg_titular_w||' - '||nm_pessoa_fisica_w*/
				end if;
			end if;
		end if;
		
		pls_consiste_data_sib(dt_reativacao_p, nm_usuario_p, cd_estabelecimento_p);
		
		if	(ie_reativar_sca_w = 'S') then
			update	pls_sca_vinculo
			set	dt_fim_vigencia	= null,
				dt_reativacao 	= dt_reativacao_p
			where	nr_seq_segurado	= nr_seq_segurado_p
			and	trunc(dt_fim_vigencia,'dd')	= trunc(dt_rescisao_w,'dd');
		end if;
		
		pls_reativar_segurado(	nr_seq_segurado_p, dt_reativacao_p, ds_observacao_p, 
					'S', nm_usuario_p);
		
		if	(nvl(ie_reativar_pagador_p,'N') = 'S') then
			update	pls_contrato_pagador
			set	dt_rescisao			= null,
				nm_usuario			= nm_usuario_p,
				dt_atualizacao			= sysdate,
				dt_reativacao			= dt_reativacao_p,
				nr_seq_motivo_cancelamento	= null,
				dt_suspensao			= null
			where	nr_sequencia	= nr_seq_pagador_w;
		end if;
		
		pls_preco_beneficiario_pck.atualizar_desconto_benef(nr_seq_contrato_w, sysdate, null, 'N', nm_usuario_p, cd_estabelecimento_p);

		open c02;
		loop
		fetch c02 into
			nr_seq_dependente_w;
		exit when c02%notfound;
			begin
			
			select 	max(dt_rescisao)
			into	dt_rescisao_depen_w
			from 	pls_segurado
			where	nr_sequencia = nr_seq_dependente_w;
			
			if	(ie_reativar_sca_w = 'S') then
				update	pls_sca_vinculo
				set	dt_fim_vigencia	= null,
					dt_reativacao 	= dt_reativacao_p
				where	nr_seq_segurado	= nr_seq_dependente_w
				and	trunc(dt_fim_vigencia,'dd')	= trunc(dt_rescisao_depen_w,'dd');
			end if;
			
			pls_reativar_segurado(	nr_seq_dependente_w, dt_reativacao_p, ds_observacao_p, 
						'S', nm_usuario_p);
			end;
		end loop;
		close c02;
		
		pls_preco_beneficiario_pck.atualizar_preco_beneficiarios(null, nr_seq_contrato_w, null, null, dt_reativacao_p, null, 'N', nm_usuario_p, cd_estabelecimento_p);
		
		--Recalcula o preco do SCA dos beneficiarios do contrato pela quantidade de vidas
		open C10;
		loop
		fetch C10 into
			nr_seq_segurado_sca_w;
		exit when C10%notfound;
			begin
			
			open C11;
			loop
			fetch C11 into
				nr_seq_vinculo_sca_w;
			exit when C11%notfound;
				begin
				pls_recalcular_preco_sca(nr_seq_segurado_sca_w, 'C', cd_estabelecimento_p, sysdate, nr_seq_vinculo_sca_w, nm_usuario_p);
				end;
			end loop;
			close C11;
			
			end;
		end loop;
		close C10;
	end if;
elsif	(nr_seq_subestipulante_p is not null) and
	(nvl(nr_seq_subestipulante_p,0) <> 0) then
	pls_consiste_data_sib(dt_reativacao_p, nm_usuario_p, cd_estabelecimento_p);
	
	select	trunc(dt_rescisao,'dd')
	into	dt_rescisao_subestipulante_w
	from	pls_sub_estipulante
	where	nr_sequencia	= nr_seq_subestipulante_p;
	
	open c03;
	loop
	fetch c03 into
		nr_seq_segurado_w;
	exit when c03%notfound;
		begin
		select	trunc(dt_rescisao,'dd')
		into	dt_rescisao_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
		
		if	(dt_rescisao_subestipulante_w = dt_rescisao_w) then
			begin
			pls_reativar_segurado(	nr_seq_segurado_w, dt_reativacao_p, ds_observacao_p, 
						'N', nm_usuario_p);
			
			tb_nr_seq_segurado_w(nr_indice_w) := nr_seq_segurado_w;
			nr_indice_w := nr_indice_w + 1;
			
			if	(ie_reativar_sca_w = 'S') then
				update	pls_sca_vinculo
				set	dt_fim_vigencia	= null,
					dt_reativacao 	= dt_reativacao_p
				where	nr_seq_segurado	= nr_seq_segurado_w
				and	trunc(dt_fim_vigencia,'dd')	= trunc(dt_rescisao_contrato_w,'dd');
			end if;
			exception
			when others then
				ds_erro_w	:= '';
			end;
		end if;
		end;
	end loop;
	close c03;
	
	update	pls_sub_estipulante
	set	dt_rescisao	= null,
		dt_limite_utilizacao = null,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate,
		dt_reativacao	= dt_reativacao_p
	where	nr_sequencia	= nr_seq_subestipulante_p;
	
	select	max(nr_seq_contrato)
	into	nr_seq_contrato_w
	from	pls_segurado
	where	nr_seq_subestipulante = nr_seq_subestipulante_p;
	
	pls_preco_beneficiario_pck.atualizar_desconto_benef(nr_seq_contrato_w, sysdate, null, 'N', nm_usuario_p, cd_estabelecimento_p);
elsif	(nr_seq_pagador_p is not null) then
	pls_consiste_data_sib(dt_reativacao_p, nm_usuario_p, cd_estabelecimento_p);
	select	trunc(dt_rescisao,'dd'),
		nr_seq_contrato
	into	dt_rescisao_pagador_w,
		nr_seq_contrato_w
	from	pls_contrato_pagador
	where	nr_sequencia	= nr_seq_pagador_p;
	
	open C04;
	loop
	fetch C04 into
		nr_seq_segurado_w;
	exit when C04%notfound;
		begin
		
		select	trunc(dt_rescisao,'dd')
		into	dt_rescisao_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
		
		if	(dt_rescisao_pagador_w = dt_rescisao_w) then
			begin
			pls_reativar_segurado(	nr_seq_segurado_w, dt_reativacao_p, ds_observacao_p, 
						'N', nm_usuario_p);
			
			tb_nr_seq_segurado_w(nr_indice_w) := nr_seq_segurado_w;
			nr_indice_w := nr_indice_w + 1;
			
			if	(ie_reativar_sca_w = 'S') then
				update	pls_sca_vinculo
				set	dt_fim_vigencia	= null,
					dt_reativacao 	= dt_reativacao_p
				where	nr_seq_segurado	= nr_seq_segurado_w
				and	trunc(dt_fim_vigencia,'dd')	= dt_rescisao_pagador_w;
			end if;
			exception
			when others then
				ds_erro_w	:= '';
			end;
		end if;
		end;
	end loop;
	close C04;
	
	update	pls_contrato_pagador
	set	dt_rescisao			= null,
		nm_usuario			= nm_usuario_p,
		dt_atualizacao			= sysdate,
		dt_reativacao			= dt_reativacao_p,
		nr_seq_motivo_cancelamento	= null,
		dt_suspensao			= null
	where	nr_sequencia	= nr_seq_pagador_p;
	
	pls_preco_beneficiario_pck.atualizar_desconto_benef(nr_seq_contrato_w, sysdate, null, 'N', nm_usuario_p, cd_estabelecimento_p);
	
	ds_mensagem_reativacao_pag_w	:= substr(	wheb_mensagem_pck.get_texto(1039983) || ' ' || wheb_mensagem_pck.get_texto(1039984, 'DS_MSG='||nr_seq_pagador_p) || chr(13) ||
							wheb_mensagem_pck.get_texto(1039994) || ': '|| wheb_mensagem_pck.get_texto(1039984, 'DS_MSG='||dt_reativacao_p) || chr(13) ||
							wheb_mensagem_pck.get_texto(1039992, 'DS_OBSERVACAO='||ds_observacao_p) || chr(13) ||
							wheb_mensagem_pck.get_texto(1039995) || ': '|| ie_titular_p || chr(13) ||
							wheb_mensagem_pck.get_texto(1039997) || ': '|| ie_dependente_p, 1, 4000);
	
	insert into pls_pagador_historico
		(	nr_sequencia,nr_seq_pagador,cd_estabelecimento,dt_atualizacao,nm_usuario,
			dt_atualizacao_nrec,nm_usuario_nrec,ds_historico,dt_historico,nm_usuario_historico,
			ds_titulo,ie_origem,ie_tipo_historico)
	values	(	pls_pagador_historico_seq.nextval,nr_seq_pagador_p,cd_estabelecimento_p,sysdate,nm_usuario_p,
			sysdate,nm_usuario_p,ds_mensagem_reativacao_pag_w,sysdate,nm_usuario_p,
			wheb_mensagem_pck.get_texto(1039981),'GC','S');	
end if;

if	(tb_nr_seq_segurado_w.count > 0) then
	for i in tb_nr_seq_segurado_w.first..tb_nr_seq_segurado_w.last loop
		begin
		pls_preco_beneficiario_pck.gravar_preco_benef(tb_nr_seq_segurado_w(i), 'B', 'S', dt_reativacao_p, 'N', null, nm_usuario_p, cd_estabelecimento_p);
		end;
	end loop;
end if;
	
if	((nvl(nr_seq_pagador_p,0) <> 0) or (nvl(nr_seq_subestipulante_p,0) <> 0)) then
	pls_preco_beneficiario_pck.atualizar_preco_beneficiarios(null, nr_seq_contrato_w, null, null, sysdate, null, 'N', nm_usuario_p, cd_estabelecimento_p);
end if;
	
if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_reativar_contrato;
/
