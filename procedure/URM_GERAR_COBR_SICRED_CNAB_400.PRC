create or replace 
procedure urm_gerar_cobr_sicred_cnab_400(  nr_seq_cobr_escrit_p    	number,
											cd_estabelecimento_p    number,
											nm_usuario_p      		varchar2) is

ds_conteudo_w      		varchar2(4000);
cd_cgc_w      			varchar2(4000);
dt_remessa_w      		varchar2(4000);
nr_nosso_numero_w    	varchar2(4000);
dt_vencimento_w      	varchar2(4000);
vl_titulo_w      		varchar2(4000);
vl_desconto_w      		varchar2(4000);
nr_titulo_w      		varchar2(4000);
vl_juros_diario_w    	varchar2(4000);
vl_desconto_diario_w    varchar2(4000);
ie_tipo_pessoa_w    	varchar2(4000);
cd_cgc_cpf_w      		varchar2(4000);
nm_sacado_w      		varchar2(4000);
ds_endereco_w      		varchar2(4000);
nr_carteira_w      		varchar2(4000);
cd_cep_w      			varchar2(4000);
nr_remessa_w      		number(15);
nr_seq_apres_w      	number(15)  := 0;
nr_seq_registro_w    	number(15);
nr_seq_reg_arquivo_w    number(15)  := 1;
tx_juros_w				varchar2(4);
tx_multa_w				varchar2(4);

Cursor C01 is
  select
    --substr(obter_nosso_numero_banco(e.cd_banco,b.nr_titulo),1,40) nr_nosso_numero,
    lpad(substr(nvl(b.nr_nosso_numero,'0'),1,9),9,'0') nr_nosso_numero,
    to_char(nvl(b.dt_vencimento,sysdate),'ddmmyy') dt_vencimento,
    replace(to_char(nvl(b.vl_titulo,0), 'fm00000000000.00'),'.','') vl_titulo,
    replace(to_char(nvl(b.vl_desc_previsto,0), 'fm00000000.00'),'.','') vl_desconto,
    lpad(nvl(c.nr_titulo,'0'),10,'0') nr_titulo,
    replace(to_char(trunc(((nvl(b.vl_saldo_titulo,0)*nvl(b.tx_juros,0))/100),2), 'fm00000000000.00'),'.','') vl_juros_diario,
    replace(to_char(nvl(b.vl_titulo,0)*nvl(b.tx_desc_antecipacao,0), 'fm00000000000.00'),'.','') vl_desconto_diario,
    decode(b.cd_cgc, null,'1','2') ie_tipo_pessoa,
    lpad(nvl(b.cd_cgc_cpf,'0'),14, '0') cd_cgc_cpf,
    rpad(substr(obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc),1,255),40,' ') nm_sacado,
    rpad(substr(upper(pls_obter_end_pagador(d.nr_seq_pagador,'EN')),1,120),40,' ') ds_endereco,
    lpad(nvl(e.cd_carteira,'0'),5,'0') nr_carteira,
    rpad(substr(upper(pls_obter_end_pagador(d.nr_seq_pagador,'CEP')),1,120),8,'0') cd_cep,
	replace(to_char(b.tx_multa, 'fm00.00'),'.','') tx_multa
  from  banco_estabelecimento e,
    pls_mensalidade d,
    titulo_receber_cobr c,
    titulo_receber_v b,
    cobranca_escritural a
  where  a.nr_sequencia    = c.nr_seq_cobranca
  and  c.nr_titulo    = b.nr_titulo
  and  a.nr_seq_conta_banco  = e.nr_sequencia
  and  b.nr_seq_mensalidade  = d.nr_sequencia(+)
  and  a.nr_sequencia    = nr_seq_cobr_escrit_p;

begin
delete from w_envio_banco
where nm_usuario = nm_usuario_p;

/*  Header do Arquivo  */
select  w_envio_banco_seq.nextval,
  to_char(a.dt_remessa_retorno,'YYYYMMDD'),
  nvl(a.nr_remessa,a.nr_sequencia),
  c.cd_cgc
into  nr_seq_registro_w,
  dt_remessa_w,
  nr_remessa_w,
  cd_cgc_w
from  pessoa_juridica    c,
  estabelecimento   b,
  cobranca_escritural   a
where  a.cd_estabelecimento  = b.cd_estabelecimento
and  b.cd_cgc    = c.cd_cgc
and  a.nr_sequencia    = nr_seq_cobr_escrit_p;

ds_conteudo_w  :=   '01'       ||
      'REMESSA'     ||
      '01'       ||
      'COBRANCA       '   ||
      '54600'     ||
      cd_cgc_w     ||
      lpad(' ',31,' ')   ||
      '748'       ||
      rpad('SICREDI',15,' ')   ||
      dt_remessa_w     ||
      lpad(' ',8,' ')   ||
      lpad(nr_remessa_w,7,'0')||
      lpad(' ',273,' ')   ||
      '2.00'       ||
      lpad(nr_seq_reg_arquivo_w,6,'0');

nr_seq_reg_arquivo_w  := nr_seq_reg_arquivo_w + 1;
nr_seq_apres_w     := nr_seq_apres_w + 1;

insert into w_envio_banco
  (nr_sequencia,
  dt_atualizacao,
  nm_usuario,
  dt_atualizacao_nrec,
  nm_usuario_nrec,
  cd_estabelecimento,
  ds_conteudo,
  nr_seq_apres)
values  (nr_seq_registro_w,
  sysdate,
  nm_usuario_p,
  sysdate,
  nm_usuario_p,
  cd_estabelecimento_p,
  ds_conteudo_w,
  nr_seq_apres_w);
/*   Fim - Header do Arquivo    */

/*  Detalhe  */
open C01;
loop
fetch C01 into
  nr_nosso_numero_w,
  dt_vencimento_w,
  vl_titulo_w,
  vl_desconto_w,
  nr_titulo_w,
  vl_juros_diario_w,
  vl_desconto_diario_w,
  ie_tipo_pessoa_w,
  cd_cgc_cpf_w,
  nm_sacado_w,
  ds_endereco_w,
  nr_carteira_w,
  cd_cep_w,
  tx_multa_w;
exit when C01%notfound;
  begin
  select  w_envio_banco_seq.nextval
  into  nr_seq_registro_w
  from  dual;

  ds_conteudo_w  :=  '1' || /*01*/
        'A' || /*02*/
        'A' || /*03*/
        'A' || /*04*/
        lpad(' ',12,' ') || /*05 a 16*/
        'A' || /*17*/
        'A' || /*18*/
        'A' || /*19*/
        lpad(' ',28,' ') || /*20 a 47*/
        lpad(nvl(nr_nosso_numero_w,'0'),9,'0') || /*48 a 56*/
        lpad(' ',6,' ') || /*57 a 62*/
        to_char(sysdate,'YYYYMMDD') || /*63 a 70*/
        ' ' || /*71*/
        'N' || /*72*/
        ' ' || /*73*/
        'B' || /*74*/
        '00' || /*75 a 76*/
        '00' || /*77 a 78*/
        '    ' || /*79 a 82*/
        vl_desconto_w || /*83 a 92*/
        tx_multa_w || /*93 a 96*/
        lpad(' ',12,' ') ||
        '01' ||
        nr_titulo_w ||
        dt_vencimento_w ||
        vl_titulo_w ||
        lpad(' ',9,' ') ||
        'K' ||
        'S' ||
        to_char(sysdate,'DDMMYY') ||
        '00' ||
        '03' ||
        vl_juros_diario_w ||
        to_char(sysdate,'DDMMYY') ||
        vl_desconto_diario_w ||
        lpad('0',13,'0') ||
        lpad('0',13,'0') ||
        ie_tipo_pessoa_w ||
        '0' ||
        cd_cgc_cpf_w ||
        nm_sacado_w ||
        translate(ds_endereco_w,'��������������������������','AAAAAAAAEEEEIIOOOOOOUUUUCC')||
        --ds_endereco_w ||
        nr_carteira_w ||
        lpad('0',6,'0') ||
        ' ' ||
        cd_cep_w ||
        '00000' ||
        --cd_cgc_cpf_w ||
        lpad(' ',14,' ') ||
        lpad(' ',41,' ') ||
        --rpad(nm_sacado_w,41,' ') ||
        lpad(nr_seq_reg_arquivo_w,6,'0');

  nr_seq_reg_arquivo_w  := nr_seq_reg_arquivo_w + 1;
  nr_seq_apres_w     := nr_seq_apres_w + 1;

  insert into w_envio_banco
    (nr_sequencia,
    dt_atualizacao,
    nm_usuario,
    dt_atualizacao_nrec,
    nm_usuario_nrec,
    cd_estabelecimento,
    ds_conteudo,
    nr_seq_apres)
  values  (nr_seq_registro_w,
    sysdate,
    nm_usuario_p,
    sysdate,
    nm_usuario_p,
    cd_estabelecimento_p,
    ds_conteudo_w,
    nr_seq_apres_w);
  end;
end loop;
close C01;
/*  Fim - Detalhe  */

/*  Trailer  */
select  w_envio_banco_seq.nextval
into  nr_seq_registro_w
from  dual;

ds_conteudo_w  :=  '9' ||
      '1' ||
      '748' ||
      '54600' ||
      lpad(' ',384,' ') ||
      lpad(nr_seq_reg_arquivo_w,6,'0');

nr_seq_apres_w     := nr_seq_apres_w + 1;

insert into w_envio_banco
  (nr_sequencia,
  dt_atualizacao,
  nm_usuario,
  dt_atualizacao_nrec,
  nm_usuario_nrec,
  cd_estabelecimento,
  ds_conteudo,
  nr_seq_apres)
values  (nr_seq_registro_w,
  sysdate,
  nm_usuario_p,
  sysdate,
  nm_usuario_p,
  cd_estabelecimento_p,
  ds_conteudo_w,
  nr_seq_apres_w);
/*  Fim - Trailer  */

commit;
end  urm_gerar_cobr_sicred_cnab_400;
/