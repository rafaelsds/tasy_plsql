create or replace
procedure pls_copiar_serv_tab_prec_prest(nr_seq_prestador_p	number,
					cd_tabela_servico_p	number,
					dt_inicio_vigencia_p	date,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2,
					ie_somente_vigentes_p	varchar2) is 

qt_registro_w			number(10);
ie_origem_proced_w		number(10);
cd_procedimento_w		number(15);
dt_vigencia_final_w		preco_servico.dt_vigencia_final%type;
					
Cursor C01 is
	select	ie_origem_proced,
		cd_procedimento,
		dt_vigencia_final
	from	preco_servico
	where	cd_tabela_servico  = cd_tabela_servico_p
	and	cd_estabelecimento = cd_estabelecimento_p;
					
begin

if	(cd_tabela_servico_p is not null) then
	open C01;
	loop
	fetch C01 into	
		ie_origem_proced_w,
		cd_procedimento_w,
		dt_vigencia_final_w;
	exit when C01%notfound;
		begin
		select	count(1)
		into	qt_registro_w
		from	pls_prestador_proc
		where	nr_seq_prestador = nr_seq_prestador_p
		and	ie_origem_proced = ie_origem_proced_w
		and	cd_procedimento	 = cd_procedimento_w;
		
		if	(qt_registro_w = 0) then
		
			if	(ie_somente_vigentes_p = 'N') or
				((dt_vigencia_final_w is null) or (dt_vigencia_final_w > sysdate)) then 
					insert into pls_prestador_proc
						(nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_prestador,
						cd_procedimento,
						ie_origem_proced,
						ie_liberar,
						dt_inicio_vigencia,
						ie_internado)
					values	(pls_prestador_proc_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_prestador_p,
						cd_procedimento_w,
						ie_origem_proced_w,
						'S',
						dt_inicio_vigencia_p,
						'N');	
			end if;	
		end if;
		end;
	end loop;
	close C01;

end if;

commit;

end pls_copiar_serv_tab_prec_prest;
/
