create or replace 
procedure atualizar_conf_ajuste_mat(
			nr_interno_conta_p	number,
			cd_material_p		number,
			dt_conta_p 		date) is

	
begin

update	material_atend_paciente
set	dt_conferencia	= sysdate
where	cd_material	= cd_material_p
and	nr_interno_conta	= nr_interno_conta_p
and 	trunc( dt_conta )	= trunc( dt_conta_p )
and	cd_motivo_exc_conta is null;
	
commit;

end atualizar_conf_ajuste_mat;
/





