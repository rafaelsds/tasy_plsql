create or replace
procedure atualizar_sintoma_atend(	nr_atendimento_p	number,
					ds_sintoma_p		varchar2,
					nm_usuario_p		Varchar2	,
					cd_medico_p		varchar2) is 

begin

gerar_evolucao_sintomaPA(	nr_atendimento_p,
				nm_usuario_p,
				ds_sintoma_p,
				obter_pessoa_atendimento(nr_atendimento_p,'C'),
				cd_medico_p,
				obter_especialidade_medico(cd_medico_p,'C'));


atualiza_sintoma_paciente(	nr_atendimento_p,
				ds_sintoma_p,
				nm_usuario_p);

commit;

end atualizar_sintoma_atend;
/