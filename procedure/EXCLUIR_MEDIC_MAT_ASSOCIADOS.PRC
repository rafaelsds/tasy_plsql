create or replace
procedure excluir_medic_mat_associados(cd_protocolo_p	number,
				nr_seq_protocolo_p	number,
				nr_seq_material_p	number,
				nm_usuario_p		varchar2
				) is 
				
nr_sequencia_w	number(15,0);			

begin
select	w_exc_protocolo_seq.nextval
into	nr_sequencia_w
from	dual;

if	(cd_protocolo_p is not null) and
	(nr_seq_protocolo_p is not null) and
	(nr_seq_material_p is not null) and
	(nm_usuario_p is not null) then

	insert into w_exc_protocolo(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_protocolo,
		nr_seq_protocolo,
		nr_seq_material)
	values	(nr_sequencia_w,
	        sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_protocolo_p,
		nr_seq_protocolo_p,
		nr_seq_material_p
		);
end if;		

commit;

end excluir_medic_mat_associados;
/