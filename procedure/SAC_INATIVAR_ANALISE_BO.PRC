create or replace
procedure sac_inativar_analise_bo(	nr_sequencia_p		number,
									nr_seq_bo_p			number,
									nm_usuario_p		varchar2) is 

begin
update	sac_boletim_ocor_analise
set		ie_situacao		= 'I',
		nm_usuario		= nm_usuario_p,
		dt_atualizacao	= sysdate
where	nr_sequencia 	= nr_sequencia_p
and		nr_seq_boletim_ocorrencia = nr_seq_bo_p;
commit;

end sac_inativar_analise_bo;
/