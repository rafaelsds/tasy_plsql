create or replace
procedure FSCF_GERAR_REPASSE_ESPECIFICO(nr_repasse_terceiro_p	number) is

vl_repasse_mat_w		number(18,4);
vl_repasse_proc_w		number(18,4);
cd_medico_w		varchar2(15);
nr_sequencia_item_w	number(18);

cursor c01 is
select	a.cd_medico,
	sum(a.vl_repasse)
from	procedimento_repasse a
where	a.nr_repasse_terceiro	= nr_repasse_terceiro_p
group by	a.cd_medico;


begin

open c01;
loop
fetch c01 into
	cd_medico_w,
	vl_repasse_proc_w;
exit when c01%notfound;

	select	sum(a.vl_repasse)
	into	vl_repasse_mat_w
	from	material_repasse a
	where	a.nr_repasse_terceiro	= nr_repasse_terceiro_p
	and	a.cd_medico		= cd_medico_w;

	if	((nvl(vl_repasse_proc_w,0) + nvl(vl_repasse_mat_w,0)) > 250) then

		select	nvl(max(nr_sequencia_item),0) + 1
		into	nr_sequencia_item_w
		from	repasse_terceiro_item
		where	nr_repasse_terceiro	= nr_repasse_terceiro_p;

		insert into repasse_terceiro_item
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			vl_repasse,
			nr_repasse_terceiro,
			nr_sequencia_item,
			cd_medico,
			ds_observacao)
		values	(repasse_terceiro_item_seq.nextval,
			sysdate,
			'Tasy',
			-40,
			nr_repasse_terceiro_p,
			nr_sequencia_item_w,
			cd_medico_w,
			'Desconto gerado pela regra de repasse específico. Procedure: FSCF_GERAR_REPASSE_ESPECIFICO');

	end if;

end loop;
close c01;

commit;

end FSCF_GERAR_REPASSE_ESPECIFICO;
/