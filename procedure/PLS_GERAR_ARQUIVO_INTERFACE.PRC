create or replace
procedure pls_gerar_arquivo_interface
			(	nr_seq_lote_p		in 	pls_lote_carteira.nr_sequencia%type,
				nm_arquivo_p		in	varchar2,
				cd_interface_p		in	number,
				ds_local_p		out 	varchar2) is

arq_texto_w			utl_file.file_type;
ds_erro_w			varchar2(255);
ds_conteudo_w			varchar2(4000);
nm_atributo_w 			interface_atributo.nm_atributo%type;
qt_tamanho_w			interface_atributo.qt_tamanho%type;
ie_tipo_atributo_w		interface_atributo.ie_tipo_atributo%type;
ds_regra_validacao_w		interface_atributo.ds_regra_validacao%type;
ds_valor_w			interface_atributo.ds_valor%type;
ds_campos_w			varchar2(4000);
ds_sql_w			varchar2(4000);
bind_sql_valor_w		sql_pck.t_dado_bind;
bind_sql_valor2_w		sql_pck.t_dado_bind;
cursor_w			sql_pck.t_cursor;
cursor2_w			sql_pck.t_cursor;
ds_registro_w			varchar2(4000);
ds_local_w			evento_tasy_utl_file.ds_local%type;
ds_local_windows_w		evento_tasy_utl_file.ds_local_rede%type;
cd_reg_interface_w		interface_reg.cd_reg_interface%type;
ie_separador_reg_w		interface_reg.ie_separador_reg%type;

Cursor C01 is
	select	cd_reg_interface,
		ie_separador_reg
	from	interface_reg
	where	cd_interface = cd_interface_p
	order by 1;

Cursor C02 is
	select	nm_atributo,
		qt_tamanho,
		ie_tipo_atributo,
		ds_regra_validacao,
		ds_valor
	from	interface_atributo
	where	cd_interface = cd_interface_p
	and	cd_reg_interface = cd_reg_interface_w
	and	qt_tamanho > 0
	and	nm_atributo <> 'IE_TIPO_REG'
	order by nr_seq_atributo;
begin

begin
select	max(ds_local),
	max(ds_local_rede)
into	ds_local_w,
	ds_local_windows_w
from	evento_tasy_utl_file
where	cd_evento	= 23;

arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_p,'W', 4000);

exception
when others then
	if (sqlcode = -29289) then
		ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
	elsif (sqlcode = -29298) then
		ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
	elsif (sqlcode = -29291) then
		ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
	elsif (sqlcode = -29286) then
		ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
	elsif (sqlcode = -29282) then
		ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
	elsif (sqlcode = -29288) then
		ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
	elsif (sqlcode = -29287) then
		ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
	elsif (sqlcode = -29281) then
		ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
	elsif (sqlcode = -29290) then
		ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
	elsif (sqlcode = -29283) then
		ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
	elsif (sqlcode = -29280) then
		ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
	elsif (sqlcode = -29284) then
		ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
	elsif (sqlcode = -29292) then
		ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
	elsif (sqlcode = -29285) then
		ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
	else
		ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
	end if;
end;

if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(ds_erro_w );
end if;

begin
	open C01;
	loop
	fetch C01 into
		cd_reg_interface_w,
		ie_separador_reg_w;
	exit when C01%notfound;
		ds_campos_w := '';
		open C02;
		loop
		fetch C02 into
			nm_atributo_w,
			qt_tamanho_w,
			ie_tipo_atributo_w,
			ds_regra_validacao_w,
			ds_valor_w;
		exit when C02%notfound;
			begin
			if	(ds_regra_validacao_w like '%ValorPadr�o%') then
				ds_campos_w := ds_campos_w ||''''||ds_valor_w||'''||';
				if	(ie_separador_reg_w is not null) then
					ds_campos_w := ds_campos_w ||''''||ie_separador_reg_w||'''||';
				end if;
			elsif	(ie_tipo_atributo_w = 'NUMBER') then
				ds_campos_w := ds_campos_w ||' lpad('||nm_atributo_w||','||qt_tamanho_w||',0)||'';''||';
			else
				if	(ie_separador_reg_w = 'N') then
					ds_campos_w := ds_campos_w ||' rpad('||nm_atributo_w||','||qt_tamanho_w||')||';
				else
					ds_campos_w := ds_campos_w ||nm_atributo_w||'||'''||ie_separador_reg_w||'''||';
				end if;
			end if;
			end;
		end loop;
		close C02;
		ds_campos_w	:= substr(ds_campos_w,0,length(ds_campos_w)-2);
		ds_sql_w	:= 'Select '||ds_campos_w||' from w_pls_interface_carteira where nr_seq_lote = '||nr_seq_lote_p||' and ie_tipo_reg = '||cd_reg_interface_w;
		
		cursor_w	:= sql_pck.executa_sql_cursor(ds_sql_w, bind_sql_valor_w);
		
		loop
			fetch 	cursor_w
			into	ds_registro_w;
			exit when cursor_w%notfound;
				utl_file.put_line(arq_texto_w,substr(ds_registro_w,1,4000));
				utl_file.fflush(arq_texto_w);
		end loop;
		close cursor_w;
	
	end loop;
	close C01;

exception
	when others then
		if (cursor_w%isopen) then
			close cursor_w;
		end if;
		wheb_mensagem_pck.exibir_mensagem_abort(sqlerrm(sqlcode));
end;

utl_file.fclose(arq_texto_w);
ds_local_p := ds_local_windows_w||nm_arquivo_p;

end pls_gerar_arquivo_interface;
/