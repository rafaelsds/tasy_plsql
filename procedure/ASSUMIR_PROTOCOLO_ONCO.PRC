CREATE OR REPLACE
PROCEDURE Assumir_protocolo_onco(	
		nr_seq_paciente_p	number,
		cd_medico_p		varchar2,
		nm_usuario_p		varchar2) is


BEGIN

update	paciente_setor
set	cd_medico_resp	= cd_medico_p,
	nm_usuario	= nm_usuario_p
where	nr_seq_paciente	= nr_seq_paciente_p;

insert into paciente_setor_medico(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_paciente,
	cd_medico,
	dt_inicio)
select	paciente_setor_medico_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_paciente_p,
	cd_medico_p,
	sysdate
from	dual;

commit;

END Assumir_protocolo_onco;
/