create or replace
procedure pls_desvinc_titulo_cobranca(	nr_seq_cobranca_p	number) is 
				
nr_titulo_w		titulo_receber.nr_titulo%type;

Cursor c_titulo is
	select	a.nr_titulo
	from	titulo_receber_cobr	a
	where	a.nr_seq_cobranca	= nr_seq_cobranca_p;
	
begin

open c_titulo;
loop
fetch c_titulo into	
	nr_titulo_w;
exit when c_titulo%notfound;
	begin
	
	desvincular_tit_rec_cobranca(nr_seq_cobranca_p, nr_titulo_w, 0, 'T');
	
	end;
end loop;
close c_titulo;

commit;

end pls_desvinc_titulo_cobranca;
/