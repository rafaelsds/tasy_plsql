create or replace
procedure desfazer_dmed(	nr_seq_dmed_p	number,
			nm_usuario_p	varchar2) as



conta_w					number(6);
contador_w				number(6);



begin
	
	
	delete 	from dmed_ops_reembolso
	where	nr_seq_titular 	in (	select 	nr_sequencia 
					from 	dmed_ops_titular_plano 
					where	nr_seq_dmed = nr_seq_dmed_p) 
	or 	nr_seq_dependente 	in (	select	nr_sequencia
					from	dmed_ops_depend_titular
					where	nr_seq_dmed_titular in (	select 	nr_sequencia 
									from 	dmed_ops_titular_plano 
									where	nr_seq_dmed = nr_seq_dmed_p));
	
	delete 	from dmed_ops_depend_titular
	where	nr_seq_dmed_titular in (	select 	nr_sequencia 
					from 	dmed_ops_titular_plano 
					where	nr_seq_dmed = nr_seq_dmed_p);
	
	delete	from dmed_beneficiario_pago
	where	nr_seq_dmed_resp 	in (	select	nr_sequencia
					from	dmed_resp_pagamento
					where	nr_seq_dmed = nr_seq_dmed_p);
	
	delete 	from dmed_ops_titular_plano
	where	nr_seq_dmed	= nr_seq_dmed_p;
	
	delete	from dmed_resp_pagamento
	where	nr_seq_dmed	= nr_seq_dmed_p;
	
	update	dmed
	set	dt_geracao	= null
	where	nr_sequencia	= nr_seq_dmed_p;
	
commit;

end desfazer_dmed;
/