create or replace
procedure pcs_alterar_data_atividade(	nr_seq_ativ_agenda_p	number,
					dt_nova_atividade_p	date,
					nm_usuario_p		Varchar2) is 

dt_inicio_atividade_w	date;			
dt_fim_atividade_w	date;
dt_nova_atividade_w	date;
qt_diferenca_dias_w	number(15,6) := 0;
			
begin

select	nvl(dt_inicio,sysdate),
	nvl(dt_fim,sysdate + 1)	
into	dt_inicio_atividade_w,
	dt_fim_atividade_w
from	pcs_agenda_atividades
where	nr_sequencia = nr_seq_ativ_agenda_p;

qt_diferenca_dias_w := dt_fim_atividade_w - dt_inicio_atividade_w;

if (dt_nova_atividade_p is not null) then   
	update 	pcs_agenda_atividades
	set 	dt_inicio = dt_nova_atividade_p,	
		dt_fim	= dt_nova_atividade_p + qt_diferenca_dias_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_seq_ativ_agenda_p; 
end if;	

commit;

end pcs_alterar_data_atividade;
/