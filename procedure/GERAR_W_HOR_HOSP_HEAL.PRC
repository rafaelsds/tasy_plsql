create or replace
procedure Gerar_W_hor_hosp_heal( nr_prescricao_p	number) is 

hr_prim_horario_w	varchar2(5);
ds_hora_w		varchar2(5);
ds_horarios_w		varchar2(2000);
dt_horario_w		Date;
dt_anterior_w		Date;
dt_inicio_prescr_w	Date;
nr_pos_espaco_w		number(10);
		
Cursor C01 is
	select	a.hr_prim_horario,
		a.ds_horarios
	from	prescr_material a
	where	a.nr_prescricao = nr_prescricao_p
	and	a.ds_horarios is not null
	and	((nvl(a.ie_se_necessario,'N') = 'N') or
		 (nvl(a.ie_acm,'N') = 'N'))
	order by a.nr_sequencia;
			
begin

delete	from w_hor_hosp_heal;

select	max(dt_inicio_prescr)
into	dt_inicio_prescr_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

dt_anterior_w := dt_inicio_prescr_w;

open C01;
loop
fetch C01 into	
	hr_prim_horario_w,
	ds_horarios_w;
exit when C01%notfound;
	begin

	nr_pos_espaco_w	:= instr(ds_horarios_w,' ');
		
	if	(nr_pos_espaco_w > 0) then
		begin
		while (ds_horarios_w is not null) loop
			begin
			
			ds_hora_w		:= substr(ds_horarios_w,1,nr_pos_espaco_w-1);

			if	(ds_hora_w is not null) then
				dt_horario_w	:= to_date(to_char(dt_anterior_w,'dd/mm/yyyy ') || ds_hora_w,'dd/mm/yyyy hh24:mi');

				if	(dt_anterior_w > dt_horario_w) then
					dt_horario_w	:= dt_horario_w + 1;
					dt_anterior_w	:= dt_horario_w;
				end if;
				
				insert into w_hor_hosp_heal(
					nr_prescricao,
					dt_horario)
				values(
					nr_prescricao_p,
					dt_horario_w);
				
			end if;
			
			ds_horarios_w	:= substr(ds_horarios_w,nr_pos_espaco_w+1,length(ds_horarios_w));
			nr_pos_espaco_w	:= instr(ds_horarios_w,' ');
			end;
		end loop;
		end;
	else
		begin
		dt_horario_w	:= to_date(to_char(dt_anterior_w,'dd/mm/yyyy ') || ds_horarios_w,'dd/mm/yyyy hh24:mi');
	
		insert into w_hor_hosp_heal(
			nr_prescricao,
			dt_horario)
		values(
			nr_prescricao_p,
			dt_horario_w);
		end;
	end if;
	end;
	dt_anterior_w := dt_inicio_prescr_w;
end loop;
close C01;


commit;

end Gerar_W_hor_hosp_heal;
/