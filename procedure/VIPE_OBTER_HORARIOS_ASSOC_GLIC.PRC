create or replace 
procedure vipe_obter_horarios_assoc_glic (
		cd_estabelecimento_p		number,
		cd_setor_usuario_p		number,
		cd_perfil_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p		number,
		dt_inicial_horarios_p		date,
		dt_final_horarios_p		date,					
		dt_validade_limite_p		date,					
		ie_exibir_hor_realizados_p	varchar2,
		ie_exibir_hor_suspensos_p	varchar2,
		ie_regra_inclusao_p		varchar2,
		ie_data_lib_prescr_p		varchar2,
		ie_exibir_suspensos_p		varchar2,
		ie_prescr_setor_p		varchar2,
		cd_setor_paciente_p		number) is
dt_horario_w	date;

cursor c01 is
select	c.dt_horario
from	proc_interno w,
	prescr_procedimento y,
	prescr_material x,
	prescr_mat_hor c,
	prescr_medica a
where	w.nr_sequencia = y.nr_seq_proc_interno
and	y.nr_prescricao = x.nr_prescricao
and	y.nr_sequencia = x.nr_sequencia_proc
and	y.nr_prescricao = a.nr_prescricao
and	x.nr_prescricao = c.nr_prescricao
and	x.nr_sequencia = c.nr_seq_material
and	x.nr_prescricao = a.nr_prescricao
and	c.nr_prescricao = a.nr_prescricao
and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	obter_se_prescr_lib_adep(a.dt_liberacao_medico, a.dt_liberacao, a.dt_liberacao_farmacia, ie_data_lib_prescr_p) = 'S'
and	w.ie_tipo <> 'BS'
and	w.ie_ivc <> 'S'
and	nvl(w.ie_ctrl_glic,'NC') = 'CCG'
and	y.nr_seq_proc_interno is not null
and	y.nr_seq_prot_glic is not null
and	y.nr_seq_exame is null
and	y.nr_seq_solic_sangue is null
and	y.nr_seq_derivado is null
and	y.nr_seq_exame_sangue is null
and	x.ie_agrupador = 5
and	((ie_exibir_suspensos_p = 'S') or (x.dt_suspensao is null))
and	nvl(c.ie_situacao,'A') = 'A'
and	nvl(c.ie_adep,'S') = 'S'
and	c.ie_agrupador = 5
and	c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p
and	((nvl(c.ie_horario_especial,'N') = 'N') or (c.dt_fim_horario is not null))
and	((ie_exibir_hor_realizados_p = 'S') or (c.dt_fim_horario is null))
and	((ie_exibir_hor_suspensos_p = 'S') or (c.dt_suspensao is null))
and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = cd_setor_paciente_p)))
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
group by
	c.dt_horario;

begin

open c01;
loop
fetch c01 into
	dt_horario_w;
exit when c01%notfound;
	begin
	
	insert into w_vipe_horarios_t (
		nm_usuario,
		dt_horario)
	values (
		nm_usuario_p,
		dt_horario_w);
	end;
end loop;
close c01;
	
end vipe_obter_horarios_assoc_glic;
/