create or replace
procedure int_disp_padrao_local(
			cd_estabelecimento_p	number,
			cd_local_estoque_p	number,
			cd_material_p		number,
			qt_estoque_maximo_p	number,
			qt_estoque_minimo_p	number,
			nm_usuario_p		varchar2) is

cd_material_generico_w		number(6);
cd_classe_material_w		number(5);
ds_material_w			varchar2(255);
ds_usuario_w			varchar2(60);
ds_local_estoque_w		varchar2(40);
qt_local_valido_w		number(10);		

begin

select	nvl(max(cd_material_generico),0),
	nvl(max(cd_classe_material),0),
	max(ds_material)
into	cd_material_generico_w,
	cd_classe_material_w,
	ds_material_w
from	material
where	cd_material = cd_material_p;

ds_usuario_w := obter_pf_usuario(nm_usuario_p,'X');
ds_local_estoque_w := obter_desc_local_estoque(cd_local_estoque_p);

select	count(*)
into	qt_local_valido_w
from	dis_regra_local_setor
where	cd_local_estoque = cd_local_estoque_p;

if	(nvl(qt_local_valido_w,0) > 0) then

	insert into intdisp_padrao_local(
		cd_estabelecimento,
		cd_local_estoque,
		cd_material,
		qt_maxima,
		qt_minima,
		nm_usuario,
		cd_material_generico,
		cd_classe_material,
		ds_material,
		ds_usuario,
		ds_local_estoque,
		dt_atualizacao)
	values(	cd_estabelecimento_p,
		cd_local_estoque_p,
		cd_material_p,
		qt_estoque_maximo_p,
		qt_estoque_minimo_p,
		nm_usuario_p,
		cd_material_generico_w,
		cd_classe_material_w,
		ds_material_w,
		ds_usuario_w,
		ds_local_estoque_w,
		sysdate);
end if;

end int_disp_padrao_local;
/