create or replace procedure gera_resultado_lab_item(nr_prescricao_p number, 
													nr_seq_prescr_p number,
													nm_usuario_p varchar2) is

nr_seq_resultado_w	exame_lab_resultado.nr_seq_resultado%type;
nr_seq_exame_w		exame_laboratorio.nr_seq_exame%type;
nr_seq_material_w	material_exame_lab.nr_sequencia%type;
nr_idade_w			number(10, 2);
ie_sexo_w			pessoa_fisica.ie_sexo%type;

begin

gravar_log_lab_pragma(17121, 
    '1 - nr_prescricao_p='||nr_prescricao_p||
    'nr_seq_prescr_p='||nr_seq_prescr_p||
    'nm_usuario_p='||nm_usuario_p,
    nm_usuario_p,
    nr_prescricao_p,
    null);
    

	begin
		select	nr_seq_resultado
		into	nr_seq_resultado_w
		from	exame_lab_resultado
		where 	nr_prescricao = nr_prescricao_p;
	exception
		when no_data_found then
			nr_seq_resultado_w := null;
	end;	
    
    gravar_log_lab_pragma(17121, 
    '2 - nr_seq_resultado_w='||nr_seq_resultado_w,
    nm_usuario_p,
    nr_prescricao_p,
    null);
	
	if (nr_seq_resultado_w is not null) then
		begin
			select	a.nr_seq_exame,
					obter_material_exame_lab(null, a.cd_material_exame, 1),
					c.ie_sexo,
					(b.dt_prescricao - c.dt_nascimento) / 365.25
			into	nr_seq_exame_w,
					nr_seq_material_w,
					ie_sexo_w,
					nr_idade_w
			from 	prescr_procedimento a
			inner	join prescr_medica b on (b.nr_prescricao = a.nr_prescricao)
			inner 	join pessoa_fisica c on (c.cd_pessoa_fisica = b.cd_pessoa_fisica)
			where	a.nr_prescricao = nr_prescricao_p
			and		a.nr_sequencia = nr_seq_prescr_p;
            
            gravar_log_lab_pragma(17121, 
            '3 - nr_seq_resultado_w='||nr_seq_resultado_w||
            'nr_seq_exame_w'||nr_seq_exame_w||
			'nr_seq_material_w'||nr_seq_material_w||
			'ie_sexo_w'||ie_sexo_w||
			'nr_idade_w'||nr_idade_w,
            nm_usuario_p,
            nr_prescricao_p,
            null);
            
		exception
			when no_data_found then
			begin
				nr_seq_exame_w := null;
				nr_seq_material_w := null;
				ie_sexo_w := null;
				nr_idade_w := null;
			end;
		end;
		gera_resultado_lab(nr_seq_resultado_w, nr_prescricao_p, nr_seq_prescr_p, nr_seq_exame_w, nr_seq_material_w, nr_idade_w, ie_sexo_w, nm_usuario_p);	
	end if;
end;
/