create or replace
procedure Gerar_evolPaci_automa(	ie_tipo_item_p		varchar2,
									nm_usuario_p		varchar2,
									nr_atendimento_p	varchar2,
									ds_item_p			varchar2,
									nr_seq_horario_p	number,
									ie_opcao_p			varchar2,
									ds_hint_p			varchar2,
									dt_evento_p			date default sysdate,
									nr_prescricao_p		number default null,
									qt_dose_p			number default null,
									ds_unid_med_p		varchar2 default null,
									ie_via_adm_p		varchar2 default null,
									cd_evolucao_p		out number,
									ie_palm_p			varchar2 default 'S',
									cd_recomendacao_p	number default null) is
					    
cd_pessoa_fisica_w			varchar2(30);
cd_medico_w					varchar2(10);
ie_evolucao_clinica_w		varchar2(10);
ds_evolucao_w				varchar2(2000);
ds_evolucao_aux_w			varchar2(2000);
ds_usuario_w				varchar2(150);
ds_item_ww					varchar2(255);
ds_item_w					varchar2(255);
ds_justificativa_w			varchar2(255);
ie_tipo_evol_w				varchar2(3);
VarGerarLib_w				varchar2(3);
dt_horario_w				date;
dt_liberacao_w				date;
cd_estabelecimento_w		number(15);
cd_evolucao_w				number(10);
nr_seq_grupo_w				number(10);
ie_tipo_vinculo_w			varchar(3);
cd_perfil_w					number(5)	:= obter_perfil_ativo;
cd_material_w				number(10);
nr_seq_proc_w				number(10);
nr_seq_proc_interno_w		number(10);
cd_procedimento_w			number(15);
ie_origem_proced_w			number(10);
cd_item_w					number;
nr_prescricao_w				prescr_medica.nr_prescricao%type;
  
cursor c01 is
select	distinct substr(b.ds_material,1,60)		
from	prescr_mat_hor a,
		material b
WHERE	b.cd_material 		= a.cd_material	
and		a.nr_prescricao 	= nr_prescricao_w
and   	a.nr_seq_superior in (  select x.nr_seq_material
								from  prescr_mat_hor x
								where x.nr_sequencia = nr_seq_horario_p)
and   	a.ie_agrupador in (3,7);						 

Cursor C02 is
select	nr_seq_grupo,
		ie_tipo_vinculo
from	tipo_evolucao_pac_grupo
where	cd_tipo_evolucao = ie_evolucao_clinica_w;

begin

if	(nr_seq_horario_p is not null) then

	nr_prescricao_w	:= nr_prescricao_p;

	if	(nvl(nr_prescricao_w,0) = 0) then
	
		select	max(nr_prescricao)
		into	nr_prescricao_w
		from	prescr_mat_hor
		where	nr_sequencia = nr_seq_horario_p;
		
	end if;
	
	if	(ie_tipo_item_p	= 'E') then
		select	max(nr_seq_proc),
				max(dt_horario)
		into	nr_seq_proc_w,
				dt_horario_w
		from	pe_prescr_proc_hor
		where	nr_sequencia	= nr_seq_horario_p;
		
	elsif	(ie_tipo_item_p	= 'P') then	

		select	max(cd_procedimento),
				max(ie_origem_proced),
				max(nr_seq_proc_interno),
				max(dt_horario)
		into	cd_procedimento_w,
				ie_origem_proced_w,
				nr_seq_proc_interno_w,
				dt_horario_w
		from	prescr_proc_hor
		where	nr_sequencia	= nr_seq_horario_p;
	
	else
		select	max(dt_horario),
				max(cd_material)
		into	dt_horario_w,
				cd_material_w
		from	prescr_mat_hor
		where	nr_sequencia	= nr_seq_horario_p;

		select	substr(max(ds_justificativa),1,250)
		into	ds_justificativa_w
		from	prescr_mat_alteracao
		where	nr_seq_horario = nr_seq_horario_p;	
	end if;
end if;

if	(ie_tipo_item_p	= 'R') then
	cd_item_w := cd_recomendacao_p;
else
	cd_item_w := nvl(cd_material_w,cd_procedimento_w);
end if;

select ds_evolucao, ie_evolucao_clinica
into    ds_evolucao_w,
        ie_evolucao_clinica_w
from ( select   max(cd_perfil) cd_perfil,
                max(ds_texto) ds_evolucao,
                max(ie_evolucao_clinica) ie_evolucao_clinica
        from    adep_regra_evolucao_adm
        where   ie_tipo_item = ie_tipo_item_p
        and	    nvl(ie_tipo_mensagem,ie_opcao_p) = ie_opcao_p
        and	    cd_perfil = cd_perfil_w
        and	    obter_se_evol_automatica(nvl(nr_seq_proc_w, cd_item_w), ie_origem_proced_w, nr_seq_proc_interno_w, ie_tipo_item_p, nr_sequencia) = 'S'
        union
        select  max(cd_perfil) cd_perfil,
                max(ds_texto) ds_evolucao,
                max(ie_evolucao_clinica) ie_evolucao_clinica
        from    adep_regra_evolucao_adm
        where   ie_tipo_item = ie_tipo_item_p
        and	    nvl(ie_tipo_mensagem,ie_opcao_p) = ie_opcao_p
        and	    cd_perfil is null
        and	    obter_se_evol_automatica(nvl(nr_seq_proc_w, cd_item_w), ie_origem_proced_w, nr_seq_proc_interno_w, ie_tipo_item_p, nr_sequencia) = 'S' )
where rownum = 1;

if	((ie_tipo_item_p in ('E','S','M', 'IAH', 'R','P','G','GA','H','DISP', 'SNE', 'NPT', 'AP', 'L', 'LD', 'MAT')) and 
	 (ds_evolucao_w is not null)) then

	select	max(cd_pessoa_fisica),
			max(obter_nome_pf(cd_pessoa_fisica)),
			nvl(max(ie_tipo_evolucao),'1')
	into	cd_medico_w,
			ds_usuario_w,
			ie_tipo_evol_w
	from	usuario
	where	nm_usuario = nm_usuario_p;

	select	max(cd_pessoa_fisica),
			max(cd_estabelecimento)
	into	cd_pessoa_fisica_w,
			cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_p;

	ds_item_w	:= substr(ds_item_p,1,255);

	open C01;
	loop
	fetch C01 into	
		ds_item_ww;
	exit when C01%notfound;
		ds_item_w	:= substr(ds_item_w || ', ' || ds_item_ww,1,255);
	end loop;
	close C01;		
	
	ds_evolucao_aux_w	:= wheb_rtf_pck.GET_CABECALHO || ds_evolucao_w;

	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@DOSE', subst_ponto_virgula_adic_zero(qt_dose_p)),1,2000);
	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@UNIDADE', ds_unid_med_p),1,2000);
	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@PRESCRICAO', nr_prescricao_p),1,2000);
	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@quebra', ' \par '),1,2000);

	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@ITEM', ds_item_p),1,2000);
	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@DATA', PKG_DATE_FORMATERS.TO_VARCHAR(dt_horario_w,'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, nm_usuario_p)),1,2000);
	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@USUARIO', ds_usuario_w),1,2000);
	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@HINT', ds_hint_p),1,2000);
	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@VIA', ie_via_adm_p),1,2000);
	ds_evolucao_aux_w	:= substr(replace_macro(ds_evolucao_aux_w, '@ds_material', ds_item_w),1,2000);
	
	
	if	(trim(ds_justificativa_w) <> '') then
		ds_evolucao_aux_w       := substr(Wheb_mensagem_pck.get_texto(455998, 'ds_evolucao_aux=' || ds_evolucao_aux_w || ';ds_justificativa_w=' || ds_justificativa_w),1,2000);
	end if;
	ds_evolucao_aux_w	:= ds_evolucao_aux_w || wheb_rtf_pck.get_rodape;
	
	Obter_Param_Usuario(1113,634,cd_perfil_w,nm_usuario_p,cd_estabelecimento_w,VarGerarLib_w);
	
	select	evolucao_paciente_seq.nextval
	into	cd_evolucao_w
	from	dual;
	
	if	(VarGerarLib_w = 'S') then
		dt_liberacao_w	:= sysdate;
	end if;

	insert into evolucao_paciente(
		cd_evolucao,
		ie_tipo_evolucao,
		dt_evolucao,
		cd_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		nr_atendimento,
		ds_evolucao,
		cd_medico,
		dt_liberacao,
		ie_evolucao_clinica,
		ie_situacao,
		ie_palm)
	values	(cd_evolucao_w,
		ie_tipo_evol_w,
		nvl(dt_evento_p, sysdate),
		cd_pessoa_fisica_w,
		sysdate,
		nm_usuario_p,
		nr_atendimento_p,
		ds_evolucao_aux_w,
		cd_medico_w,
		dt_liberacao_w,
		ie_evolucao_clinica_w,
		'A',
		ie_palm_p);


	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_grupo_w,
		ie_tipo_vinculo_w;
	exit when C02%notfound;
		begin
		if	(nvl(ie_tipo_vinculo_w,'V') = 'D') then	
			desvincular_atendimento_grupo(nr_atendimento_p, nm_usuario_p, nr_seq_grupo_w);	
		else
			if	(nr_atendimento_p is null) or (nr_atendimento_p = 0) then
				vincular_atendimento_grupo(nr_atendimento_p,cd_pessoa_fisica_w,1,nr_seq_grupo_w,nm_usuario_p);
			else
				vincular_atendimento_grupo(nr_atendimento_p,cd_pessoa_fisica_w,0,nr_seq_grupo_w,nm_usuario_p);
			end if;
		end if;
		end;
	end loop;
	close C02;
	
	cd_evolucao_p := cd_evolucao_w;

end if;

end Gerar_evolPaci_automa;
/