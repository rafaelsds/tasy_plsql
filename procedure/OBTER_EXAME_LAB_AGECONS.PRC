create or replace
procedure obter_exame_lab_agecons (
		nr_atendimento_p	number,
		cd_convenio_p		number,
		cd_categoria_p		Varchar2,
		nr_seq_exame_p		number,
		nr_seq_proc_interno_p	number,
		cd_material_exame_p	number,
		cd_estabelecimento_p	number,
		ds_procedimento_p out	Varchar2,
		cd_procedimento_p out	number,
		ie_origem_proced_p out	number,
		ds_erro_p out		varchar2,
		nm_usuario_p		Varchar2) is 

ie_tipo_atendimento_w	number(3,0);
ie_tipo_convenio_w	number(2,0);
cd_procedimento_w	number(15,0);
ds_procedimento_w	PROCEDIMENTO.DS_PROCEDIMENTO%type;
ie_origem_proced_w	number(10,0);
ds_erro_w		varchar2(255);
cd_setor_w		number(5,0);
nr_seq_proc_interno_aux_w	number(10);
cd_plano_convenio_w	varchar2(10);

begin
if	(nm_usuario_p is not null) and
	(nr_seq_exame_p is not null) then
	begin
	
	ie_tipo_atendimento_w	:= obter_tipo_atendimento(nr_atendimento_p);
	ie_tipo_convenio_w	:= obter_tipo_convenio(cd_convenio_p);
	cd_plano_convenio_w	:=	obter_plano_conv_atend(nr_atendimento_p);

	obter_exame_lab_convenio(
		nr_seq_exame_p,
		cd_convenio_p,
		cd_categoria_p,
		ie_tipo_atendimento_w,
		cd_estabelecimento_p,
		ie_tipo_convenio_w,
		nr_seq_proc_interno_p,
		cd_material_exame_p,
		cd_plano_convenio_w,
		cd_setor_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		ds_erro_w,
		nr_seq_proc_interno_aux_w);
	
  if (cd_procedimento_w <> 0) and (ie_origem_proced_w <> 0) then
  select	ds_procedimento

	into	ds_procedimento_w
	from	procedimento
	where	cd_procedimento		= cd_procedimento_w
	and	ie_origem_proced	= ie_origem_proced_w;
  end if;
	
	end;
end if;
ds_procedimento_p	:= ds_procedimento_w;
cd_procedimento_p 	:= cd_procedimento_w;
ie_origem_proced_p 	:= ie_origem_proced_w;
ds_erro_p 		:= ds_erro_w;
commit;
end obter_exame_lab_agecons;
/