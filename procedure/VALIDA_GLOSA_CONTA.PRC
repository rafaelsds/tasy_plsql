create or replace 
procedure Valida_Glosa_Conta(	nr_interno_conta_p	number,
				vl_saldo_p		number,
			    	vl_glosa_p		number,
				cd_perfil_p		number,
				nm_usuario_p		varchar2,
				ie_glosa_p		out varchar2,
				cd_motivo_glosa_p	out number) is

pr_glosa_w			number(15,2);
vl_glosa_w			number(15,2);
ie_glosa_w			varchar2(255);

cd_convenio_w			number(15);
ie_tipo_atend_w		number(15);

ie_tipo_atend_regra_w	number(15);
pr_glosa_regra_w		number(15,2) := 0;
vl_glosa_regra_w		number(15,2) := 0;

ie_ok_w			boolean := True;

dt_conta_w			date;
cd_estabelecimento_w		number(4);
cd_motivo_glosa_w		number(5);

cursor Regra is
select	ie_tipo_atendimento,
	nvl(pr_glosa,0),
	nvl(vl_glosa,0),
	cd_motivo_glosa
from	convenio_regra_glosa
where	nvl(dt_fim_vigencia,sysdate + 1)		> sysdate
and	nvl(cd_convenio,nvl(cd_convenio_w,0)) 		= nvl(cd_convenio_w,0)
and 	nvl(ie_tipo_atendimento, ie_tipo_atend_w) 		= ie_tipo_atend_w
and 	((nvl(cd_estabelecimento, nvl(cd_estabelecimento_w,0)) 	= nvl(cd_estabelecimento_w,0)) or (nvl(cd_estabelecimento_w,0) = 0))
and	dt_inicio_vigencia = (select	max(dt_inicio_vigencia)
			from	convenio_regra_glosa
			where	nvl(dt_fim_vigencia,sysdate + 1)	> sysdate
			and	nvl(cd_convenio,nvl(cd_convenio_w,0))	= nvl(cd_convenio_w,0)
			and	dt_inicio_vigencia 			<= dt_conta_w
			and 	((nvl(cd_estabelecimento, nvl(cd_estabelecimento_w,0)) 	= nvl(cd_estabelecimento_w,0)) or (nvl(cd_estabelecimento_w,0) = 0)))
order by	nvl(ie_tipo_atendimento,0),
	nvl(cd_convenio,0) desc;

BEGIN

ie_glosa_p := 'N';
if	(vl_glosa_p = 0) then
	ie_glosa_p := 'S';
end if;

obter_param_usuario(27, 2, cd_perfil_p, nm_usuario_p, 0, pr_glosa_w);
--obter_param_usuario(27, 3, cd_perfil_p, nm_usuario_p, 0, vl_glosa_w); Parametro Inativado lhalves OS291698 em 28/02/2011.
obter_param_usuario(27, 4, cd_perfil_p, nm_usuario_p, 0, ie_glosa_w);

vl_glosa_w := 0;

select	wheb_usuario_pck.get_cd_estabelecimento
into	cd_estabelecimento_w
from	dual;

begin
select	a.cd_convenio_parametro,
	b.ie_tipo_atendimento,
	a.dt_mesano_referencia
into	cd_convenio_w,
	ie_tipo_atend_w,
	dt_conta_w
from	atendimento_paciente b,
	conta_paciente a
where	a.nr_atendimento 	= b.nr_atendimento
and	a.nr_interno_conta 	= nr_interno_conta_p;
exception
	when no_data_found then
		ie_ok_w := False;
end;

if	ie_ok_w then
	open Regra;
	loop
	fetch Regra into	
		ie_tipo_atend_regra_w,
		pr_glosa_regra_w,
		vl_glosa_regra_w,
		cd_motivo_glosa_w;
	exit when Regra%notfound;
		if (ie_tipo_atend_w = ie_tipo_atend_regra_w) then
			exit;
		end if;
	end loop;
	close Regra;
end if;

if	(pr_glosa_w is null) then
	pr_glosa_w := 0;
end if;

if	(vl_glosa_w is null) then
	vl_glosa_w := 0;
end if;

if	(pr_glosa_regra_w <> 0) then
	pr_glosa_w := pr_glosa_regra_w;
end if;

if	(vl_glosa_regra_w <> 0) then
	vl_glosa_w := vl_glosa_regra_w;
end if;

if	(ie_glosa_w = '') then
	ie_glosa_w := 'N';
end if;

if	(ie_glosa_w = 'S') and
	((pr_glosa_w > 0) and (vl_glosa_p <= (vl_saldo_p/100) * pr_glosa_w)) or
	((vl_glosa_w > 0) and (vl_glosa_p <= vl_glosa_w)) then
	ie_glosa_p 		:= 'S';
	cd_motivo_glosa_p	:= cd_motivo_glosa_w;
end if;

END Valida_Glosa_Conta;
/