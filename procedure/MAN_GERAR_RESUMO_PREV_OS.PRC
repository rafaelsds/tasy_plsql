create or replace
procedure man_gerar_resumo_prev_os
			(	nr_seq_gerencia_p	number,
				nr_seq_grupo_des_p	number,
				ie_considera_OS_proj_p	varchar2,
				nm_usuario_p		varchar2) IS

pr_os_antiga_w			number(15,2);
nr_seq_grupo_des_w		number(10);
nr_seq_gerencia_w			number(10);
nr_sequencia_w			number(10);
qt_60_dia_w			number(10);
qt_45_dia_w			number(10);
qt_30_dia_w			number(10);
qt_15_dia_w			number(10);
qt_07_dia_w			number(10);
qt_semana_w			number(10);
qt_os_projeto_w			number(10);
qt_total_w			number(10);
qt_os_antiga_w			number(10);
qt_projeto_w			number(10);
dt_prevista_w			date;
dt_fim_dia_w			date;

begin
delete	w_resumo_prev_os
where	nm_usuario	= nm_usuario_p;

delete w_resumo_prev_os_item
where	nm_usuario	= nm_usuario_p;

commit;

dt_fim_dia_w	:= trunc(sysdate) + 86399/86400;

insert into w_man_ordem_servico(
	nr_seq_ordem,
	dt_ordem_servico,
	nr_seq_grupo_des,
	nr_seq_estagio,
	ie_status_ordem,
	dt_prevista,
	nr_seq_gerencia,
	nr_seq_ordem_ativ,
	nm_usuario_exec)
select	a.nr_sequencia,
	a.dt_ordem_servico,
	a.nr_seq_grupo_des,
	a.nr_seq_estagio,
	a.ie_status_ordem,
	trunc(c.dt_prevista) dt_prevista,
	d.nr_seq_gerencia,
	nvl(c.nr_seq_ordem_serv,0),
	nvl(c.nm_usuario_prev,'Tasy')
from	grupo_desenvolvimento d,
	man_ordem_ativ_prev c,
	man_estagio_processo b,
	man_ordem_servico a
where	a.nr_seq_estagio	= b.nr_sequencia
and	((b.ie_desenv		= 'S')
	or (b.ie_tecnologia	= 'S')
	or (b.ie_testes		= 'S'))
and	a.nr_sequencia		= c.nr_seq_ordem_serv(+)
and	nvl(c.dt_prevista,sysdate)	>= trunc(sysdate)
and	a.nr_seq_grupo_des	= d.nr_sequencia
and exists(	select	1
		from	usuario_grupo_des p
		where	p.nr_seq_grupo = a.nr_seq_grupo_des
		and  	p.nm_usuario_grupo = nvl(c.nm_usuario_prev,'Tasy'))
group by a.nr_sequencia,
	a.dt_ordem_servico,
	a.nr_seq_grupo_des,
	a.nr_seq_estagio,
	a.ie_status_ordem,
	trunc(c.dt_prevista),
	d.nr_seq_gerencia,
	nvl(c.nr_seq_ordem_serv,0),
	c.nm_usuario_prev
union
select	a.nr_sequencia,
	a.dt_ordem_servico,
	a.nr_seq_grupo_des,
	a.nr_seq_estagio,
	a.ie_status_ordem,
	max(trunc(c.dt_prevista)) dt_prevista,
	d.nr_seq_gerencia,
	nvl(c.nr_seq_ordem_serv,0),
	nvl(c.nm_usuario_prev,'Tasy')
from	grupo_desenvolvimento d,
	man_ordem_ativ_prev c,
	man_estagio_processo b,
	man_ordem_servico a
where	a.nr_seq_estagio	= b.nr_sequencia
and	((b.ie_desenv		= 'S') 
	or (b.ie_tecnologia	= 'S')
	or (b.ie_testes		= 'S'))
and	a.nr_sequencia		= c.nr_seq_ordem_serv(+)
and	nvl(c.dt_prevista,sysdate)	< trunc(sysdate)
and	a.nr_seq_grupo_des	= d.nr_sequencia
and exists(	select	1
		from	usuario_grupo_des p
		where	p.nr_seq_grupo = a.nr_seq_grupo_des
		and	p.nm_usuario_grupo = nvl(c.nm_usuario_prev,'Tasy'))
group by a.nr_sequencia,
	a.dt_ordem_servico,
	a.nr_seq_grupo_des,
	a.nr_seq_estagio,
	a.ie_status_ordem,
	d.nr_seq_gerencia,
	nvl(c.nr_seq_ordem_serv,0),
	c.nm_usuario_prev
union
select	a.nr_sequencia,
	a.dt_ordem_servico,
	a.nr_seq_grupo_des,
	a.nr_seq_estagio,
	a.ie_status_ordem,
	trunc(x.dt_prevista) dt_prevista,
	d.nr_seq_gerencia,
	x.nr_seq_ordem_serv,
	x.nm_usuario_prev
from	grupo_desenvolvimento d,
	man_estagio_processo b,
	man_ordem_servico a,
	man_ordem_ativ_prev x
where	x.nr_seq_ordem_serv	= a.nr_sequencia
and	a.nr_seq_estagio	= b.nr_sequencia
and	nvl(b.ie_desenv,'N')	= 'N'
and	nvl(b.ie_tecnologia,'N')= 'N'
and	a.nr_seq_grupo_des	= d.nr_sequencia
and exists(	select	1
		from	usuario_grupo_des p
		where 	p.nr_seq_grupo = a.nr_seq_grupo_des
		and   	p.nm_usuario_grupo = nvl(x.nm_usuario_prev,'Tasy'))
and	x.dt_prevista between trunc(sysdate) and dt_fim_dia_w
union
select	a.nr_sequencia,
	a.dt_ordem_servico,
	a.nr_seq_grupo_des,
	a.nr_seq_estagio,
	a.ie_status_ordem,
	trunc(c.dt_prevista) dt_prevista,
	d.nr_seq_gerencia,
	nvl(c.nr_seq_ordem_serv,0),
	nvl(c.nm_usuario_prev,'Tasy')
from	grupo_desenvolvimento d,
	man_ordem_ativ_prev c,
	man_estagio_processo b,
	man_ordem_servico a
where	a.nr_seq_estagio	= b.nr_sequencia
and	((b.ie_desenv		= 'S') 
	or (b.ie_tecnologia	= 'S')
	or (b.ie_testes		= 'S'))
and	a.nr_sequencia		= c.nr_seq_ordem_serv(+)
and	nvl(c.dt_prevista,sysdate)	>= trunc(sysdate)
and	a.nr_seq_grupo_des	= d.nr_sequencia
and not exists(	select	1
		from	usuario_grupo_des p
		where	p.nr_seq_grupo = a.nr_seq_grupo_des
		and   	p.nm_usuario_grupo = nvl(c.nm_usuario_prev,'Tasy'))
group by a.nr_sequencia,
	a.dt_ordem_servico,
	a.nr_seq_grupo_des,
	a.nr_seq_estagio,
	a.ie_status_ordem,
	trunc(c.dt_prevista),
	d.nr_seq_gerencia,
	nvl(c.nr_seq_ordem_serv,0),
	c.nm_usuario_prev;

/*Total */

insert into w_resumo_prev_os
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_previsao,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	qt_total,
	qt_60_dia,
	qt_45_dia,
	qt_30_dia,
	qt_15_dia,
	qt_07_dia,
	qt_semana,
	qt_os_antiga,
	pr_os_antiga,
	qt_os_projeto,
	ie_tipo_prev)
select	w_resumo_prev_os_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	dt_prevista,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	qt_total,
	qt_60_dias,
	qt_45_dias,
	qt_30_dias,
	qt_15_dias,
	qt_07_dias,
	qt_da_semana,
	qt_tot_os_antiga,
	pr_antiga,
	qt_projeto,
	'T'
from (
	select	trunc(dt_prevista,'dd') dt_prevista,
		nr_seq_gerencia,
		nr_seq_grupo_des,
		count(*) qt_total,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -60,'dd')), 'S', 1, 0)),0) qt_60_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 59,'dd'), trunc(sysdate -45,'dd')), 'S', 1, 0)),0) qt_45_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 44,'dd'), trunc(sysdate -30,'dd')), 'S', 1, 0)),0) qt_30_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 29,'dd'), trunc(sysdate -15,'dd')), 'S', 1, 0)),0) qt_15_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 14,'dd'), trunc(sysdate -7,'dd')), 'S', 1, 0)),0) qt_07_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 6,'dd'), trunc(sysdate,'dd')), 'S', 1, 0)),0) qt_da_semana,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -30,'dd')), 'S', 1, 0)),0) qt_tot_os_antiga,
		(dividir(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -30,'dd')), 'S', 1, 0)), count(*)) *100) pr_antiga,
		nvl(sum(decode(obter_se_ordem_orig_projeto(nr_seq_ordem), 'S',1,0)),0) qt_projeto
	from	(
		select	trunc(dt_prevista,'dd') dt_prevista,
			nr_seq_gerencia,
			nr_seq_grupo_des,
			dt_ordem_servico,
			nr_seq_ordem
		from	w_man_ordem_servico a
		where	a.nr_seq_gerencia		= nr_seq_gerencia_p
		and	((nr_seq_grupo_des_p = 0) or (a.nr_seq_grupo_des = nr_seq_grupo_des_p))
		and	((ie_considera_OS_proj_p = 'S') or (proj_obter_dados_ordem_serv(a.nr_seq_ordem, 'PROJD') is null))
		and	a.dt_prevista is not null
		group by trunc(a.dt_prevista,'dd'),
			a.nr_seq_gerencia,
			a.nr_seq_grupo_des,
			a.dt_ordem_servico,
			a.nr_seq_ordem)
	group by trunc(dt_prevista,'dd'),
			nr_seq_gerencia,
			nr_seq_grupo_des,
			nr_seq_ordem,
			dt_ordem_servico);
	
	
insert into w_resumo_prev_os_item(
	nm_usuario,
	dt_previsao,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	nr_seq_ordem,
	ie_tipo_prev)
select	nm_usuario_p,
	trunc(dt_prevista,'dd'),
	nr_seq_gerencia,
	nr_seq_grupo_des,
	nr_seq_ordem,
	'T'
from	w_man_ordem_servico a
where	a.nr_seq_gerencia		= nr_seq_gerencia_p
and	((nr_seq_grupo_des_p = 0) or (a.nr_seq_grupo_des = nr_seq_grupo_des_p))
and	((ie_considera_OS_proj_p = 'S') or (proj_obter_dados_ordem_serv(a.nr_seq_ordem, 'PROJD') is null))
and	a.dt_prevista is not null
group by trunc(a.dt_prevista,'dd'),
	a.nr_seq_gerencia,
	a.nr_seq_grupo_des,
	a.nr_seq_ordem;


/*Atrasada   OK */
insert into w_resumo_prev_os
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_previsao,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	qt_total,
	qt_60_dia,
	qt_45_dia,
	qt_30_dia,
	qt_15_dia,
	qt_07_dia,
	qt_semana,
	qt_os_antiga,
	pr_os_antiga,
	qt_os_projeto,
	ie_tipo_prev)
select	w_resumo_prev_os_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	trunc(sysdate),
	nr_seq_gerencia,
	nr_seq_grupo_des,
	qt_total,
	qt_60_dias,
	qt_45_dias,
	qt_30_dias,
	qt_15_dias,
	qt_07_dias,
	qt_da_semana,
	qt_tot_os_antiga,
	pr_antiga,
	qt_projeto,
	'ATR'
from (
	select	nr_seq_gerencia,
		nr_seq_grupo_des,
		count(*) qt_total,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -60,'dd')), 'S', 1, 0)),0) qt_60_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 59,'dd'), trunc(sysdate -45,'dd')), 'S', 1, 0)),0) qt_45_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 44,'dd'), trunc(sysdate -30,'dd')), 'S', 1, 0)),0) qt_30_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 29,'dd'), trunc(sysdate -15,'dd')), 'S', 1, 0)),0) qt_15_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 14,'dd'), trunc(sysdate -7,'dd')), 'S', 1, 0)),0) qt_07_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 6,'dd'), trunc(sysdate,'dd')), 'S', 1, 0)),0) qt_da_semana,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -30,'dd')), 'S', 1, 0)),0) qt_tot_os_antiga,
		(dividir(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -30,'dd')), 'S', 1, 0)), count(*)) *100) pr_antiga,
		nvl(sum(decode(obter_se_ordem_orig_projeto(nr_seq_ordem), 'S',1,0)),0) qt_projeto
	from (
		select	nr_seq_gerencia,
			nr_seq_grupo_des,
			dt_ordem_servico,
			nr_seq_ordem
		from	man_estagio_processo p,
			w_man_ordem_servico a
		where	p.nr_sequencia		= a.nr_seq_estagio
		and	a.nr_seq_gerencia	= nr_seq_gerencia_p
		and	((nr_seq_grupo_des_p = 0) or (a.nr_seq_grupo_des = nr_seq_grupo_des_p))
		and	((ie_considera_OS_proj_p = 'S') or (proj_obter_dados_ordem_serv(a.nr_seq_ordem, 'PROJD') is null))
		and	a.ie_status_ordem <> 3
		and	((p.ie_desenv		= 'S')
			or (p.ie_tecnologia	= 'S'))
		and exists (	select	1
				from	w_man_ordem_servico b
				where	a.nr_seq_ordem	= b.nr_seq_ordem
				and	b.dt_prevista	< trunc(sysdate)
				and	b.nr_seq_grupo_des	= a.nr_seq_grupo_des)
		and not exists (select	1
				from	w_man_ordem_servico b
				where	a.nr_seq_ordem	= b.nr_seq_ordem
				and	b.dt_prevista	>= trunc(sysdate)
				and	b.nr_seq_grupo_des	= a.nr_seq_grupo_des)
		group by a.nr_seq_gerencia,
			a.nr_seq_grupo_des,
			a.nr_seq_ordem,
			a.dt_ordem_servico)
	group by nr_seq_gerencia,
		nr_seq_grupo_des,
		dt_ordem_servico,
		nr_seq_ordem);

		
insert into w_resumo_prev_os_item(
	nm_usuario,
	dt_previsao,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	nr_seq_ordem,
	ie_tipo_prev)
select	nm_usuario_p,
	trunc(sysdate,'dd'),
	nr_seq_gerencia,
	nr_seq_grupo_des,
	nr_seq_ordem,
	'ATR'
from	man_estagio_processo p,
	w_man_ordem_servico a
where	p.nr_sequencia		= a.nr_seq_estagio
and	a.nr_seq_gerencia	= nr_seq_gerencia_p
and	((nr_seq_grupo_des_p = 0) or (a.nr_seq_grupo_des = nr_seq_grupo_des_p))
and	((ie_considera_OS_proj_p = 'S') or (proj_obter_dados_ordem_serv(a.nr_seq_ordem, 'PROJD') is null))
and	a.ie_status_ordem <> 3
and	((p.ie_desenv		= 'S')
	or (p.ie_tecnologia	= 'S'))
and exists (	select	1
		from	w_man_ordem_servico b
		where	a.nr_seq_ordem	= b.nr_seq_ordem
		and	b.dt_prevista	< trunc(sysdate)
		and	b.nr_seq_grupo_des	= a.nr_seq_grupo_des)
and not exists (select	1
		from	w_man_ordem_servico b
		where	a.nr_seq_ordem	= b.nr_seq_ordem
		and	b.dt_prevista	>= trunc(sysdate)
		and	b.nr_seq_grupo_des	= a.nr_seq_grupo_des)
group by trunc(sysdate,'dd'),
	a.nr_seq_gerencia,
	a.nr_seq_grupo_des,
	a.nr_seq_ordem;

	
/*Sem atividade prevista  OK*/
insert into w_resumo_prev_os
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_previsao,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	qt_total,
	qt_60_dia,
	qt_45_dia,
	qt_30_dia,
	qt_15_dia,
	qt_07_dia,
	qt_semana,
	qt_os_antiga,
	pr_os_antiga,
	qt_os_projeto,
	ie_tipo_prev)
select	w_resumo_prev_os_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	dt_prevista,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	qt_total,
	qt_60_dias,
	qt_45_dias,
	qt_30_dias,
	qt_15_dias,
	qt_07_dias,
	qt_da_semana,
	qt_tot_os_antiga,
	pr_antiga,
	qt_projeto,
	'SAP'
from (
	select	trunc(sysdate,'dd') dt_prevista,
		nr_seq_gerencia,
		nr_seq_grupo_des,
		count(*) qt_total,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -60,'dd')), 'S', 1, 0)),0) qt_60_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 59,'dd'), trunc(sysdate -45,'dd')), 'S', 1, 0)),0) qt_45_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 44,'dd'), trunc(sysdate -30,'dd')), 'S', 1, 0)),0) qt_30_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 29,'dd'), trunc(sysdate -15,'dd')), 'S', 1, 0)),0) qt_15_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 14,'dd'), trunc(sysdate -7,'dd')), 'S', 1, 0)),0) qt_07_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 6,'dd'), trunc(sysdate,'dd')), 'S', 1, 0)),0) qt_da_semana,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -30,'dd')), 'S', 1, 0)),0) qt_tot_os_antiga,
		(dividir(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -30,'dd')), 'S', 1, 0)), count(*)) *100) pr_antiga,
		nvl(sum(decode(obter_se_ordem_orig_projeto(nr_seq_ordem), 'S',1,0)),0) qt_projeto
	from (
		select	nr_seq_gerencia,
			nr_seq_grupo_des,
			dt_ordem_servico,
			nr_seq_ordem
		from	man_estagio_processo p,
			w_man_ordem_servico a
		where	p.nr_sequencia		= a.nr_seq_estagio
		and	a.nr_seq_gerencia	= nr_seq_gerencia_p
		and	((nr_seq_grupo_des_p = 0) or (a.nr_seq_grupo_des = nr_seq_grupo_des_p))
		and	((ie_considera_OS_proj_p = 'S') or (proj_obter_dados_ordem_serv(a.nr_seq_ordem, 'PROJD') is null))
		and	a.ie_status_ordem <> 3
		and	((p.ie_desenv		= 'S')
			or (p.ie_tecnologia	= 'S'))
		and	exists (select 1
				from 	man_estagio_processo h
				where	h.ie_desenv = 'S'
				and	h.nr_sequencia = a.nr_seq_estagio)
		and not exists (select	1
				from	usuario_grupo_des c,
					w_man_ordem_servico b
				where	a.nr_seq_ordem		= b.nr_seq_ordem
				and	b.nr_seq_grupo_des	= a.nr_seq_grupo_des
				and	b.nr_seq_grupo_des	= c.nr_seq_grupo
				and	b.nm_usuario_exec	= c.nm_usuario_grupo)
		group by a.nr_seq_gerencia,
			a.nr_seq_grupo_des,
			a.nr_seq_ordem,
			a.dt_ordem_servico)
	group by trunc(sysdate,'dd'),
		nr_seq_gerencia,
		nr_seq_grupo_des,
		nr_seq_ordem);
	

insert into w_resumo_prev_os_item(
	nm_usuario,
	dt_previsao,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	nr_seq_ordem,
	ie_tipo_prev)
select	nm_usuario_p,
	trunc(sysdate,'dd'),
	nr_seq_gerencia,
	nr_seq_grupo_des,
	nr_seq_ordem,
	'SAP'
from	man_estagio_processo p,
	w_man_ordem_servico a
where	p.nr_sequencia		= a.nr_seq_estagio
and	a.nr_seq_gerencia	= nr_seq_gerencia_p
and	((nr_seq_grupo_des_p = 0) or (a.nr_seq_grupo_des = nr_seq_grupo_des_p))
and	((ie_considera_OS_proj_p = 'S') or (proj_obter_dados_ordem_serv(a.nr_seq_ordem, 'PROJD') is null))
and	a.ie_status_ordem <> 3
and	((p.ie_desenv		= 'S')
	or (p.ie_tecnologia	= 'S'))
and	exists (select 1
		from 	man_estagio_processo h
		where	h.ie_desenv = 'S'
		and	h.nr_sequencia = a.nr_seq_estagio)
and not exists (select	1
		from	usuario_grupo_des c,
			w_man_ordem_servico b
		where	a.nr_seq_ordem		= b.nr_seq_ordem
		and	b.nr_seq_grupo_des	= a.nr_seq_grupo_des
		and	b.nr_seq_grupo_des	= c.nr_seq_grupo
		and	b.nm_usuario_exec	= c.nm_usuario_grupo)
group by trunc(sysdate,'dd'),
	a.nr_seq_gerencia,
	a.nr_seq_grupo_des,
	a.nr_seq_ordem;	

	
/*Sem data prevista       OK  */  
insert into w_resumo_prev_os
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_previsao,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	qt_total,
	qt_60_dia,
	qt_45_dia,
	qt_30_dia,
	qt_15_dia,
	qt_07_dia,
	qt_semana,
	qt_os_antiga,
	pr_os_antiga,
	qt_os_projeto,
	ie_tipo_prev)
select	w_resumo_prev_os_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nvl(dt_prevista,trunc(sysdate)),
	nr_seq_gerencia,
	nr_seq_grupo_des,
	qt_total,
	qt_60_dias,
	qt_45_dias,
	qt_30_dias,
	qt_15_dias,
	qt_07_dias,
	qt_da_semana,
	qt_tot_os_antiga,
	pr_antiga,
	qt_projeto,
	'SDP'
from (
	select	dt_prevista,
		nr_seq_gerencia,
		nr_seq_grupo_des,
		count(*) qt_total,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -60,'dd')), 'S', 1, 0)),0) qt_60_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 59,'dd'), trunc(sysdate -45,'dd')), 'S', 1, 0)),0) qt_45_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 44,'dd'), trunc(sysdate -30,'dd')), 'S', 1, 0)),0) qt_30_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 29,'dd'), trunc(sysdate -15,'dd')), 'S', 1, 0)),0) qt_15_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 14,'dd'), trunc(sysdate -7,'dd')), 'S', 1, 0)),0) qt_07_dias,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), trunc(sysdate - 6,'dd'), trunc(sysdate,'dd')), 'S', 1, 0)),0) qt_da_semana,
		nvl(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -30,'dd')), 'S', 1, 0)),0) qt_tot_os_antiga,
		(dividir(sum(decode(obter_se_data_periodo(trunc(dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), trunc(sysdate -30,'dd')), 'S', 1, 0)), count(*)) *100) pr_antiga,
		nvl(sum(decode(obter_se_ordem_orig_projeto(nr_seq_ordem), 'S',1,0)),0) qt_projeto
	from (
		select	trunc(dt_prevista,'dd') dt_prevista,
			nr_seq_gerencia,
			nr_seq_grupo_des,
			dt_ordem_servico,
			nr_seq_ordem
		from	man_estagio_processo p,
			usuario_grupo_des u,
			w_man_ordem_servico a
		where	a.nr_seq_grupo_des      = u.nr_seq_grupo
		and     a.nm_usuario_exec       = u.nm_usuario_grupo
		and	p.nr_sequencia		= a.nr_seq_estagio
		and	a.nr_seq_gerencia	= nr_seq_gerencia_p
		and	((nr_seq_grupo_des_p = 0) or (a.nr_seq_grupo_des = nr_seq_grupo_des_p))
		and	((ie_considera_OS_proj_p = 'S') or (proj_obter_dados_ordem_serv(a.nr_seq_ordem, 'PROJD') is null))
		and	a.ie_status_ordem <> 3
		and	((p.ie_desenv		= 'S')
			or (p.ie_tecnologia	= 'S'))
		and not exists (select	1
				from    usuario_grupo_des c,
					w_man_ordem_servico b
				where   a.nr_seq_ordem  	= b.nr_seq_ordem
				and     b.nr_seq_grupo_des      = a.nr_seq_grupo_des
				and     b.nr_seq_grupo_des      = c.nr_seq_grupo
				and     b.nm_usuario_exec       = c.nm_usuario_grupo
				and	b.dt_prevista is not null)
		and exists (	select	1
				from	usuario_grupo_des c,
					w_man_ordem_servico b
				where   a.nr_seq_ordem  	= b.nr_seq_ordem
				and     b.nr_seq_grupo_des      = a.nr_seq_grupo_des
				and     b.nr_seq_grupo_des      = c.nr_seq_grupo
				and     b.nm_usuario_exec       = c.nm_usuario_grupo
				and	b.dt_prevista is null)
		group by trunc(a.dt_prevista,'dd'),
			nr_seq_gerencia,
			nr_seq_grupo_des,
			nr_seq_ordem,
			dt_ordem_servico)
	group by dt_prevista,
		nr_seq_gerencia,
		nr_seq_grupo_des,
		nr_seq_ordem,
		dt_ordem_servico);
		
insert into w_resumo_prev_os_item(
	nm_usuario,
	dt_previsao,
	nr_seq_gerencia,
	nr_seq_grupo_des,
	nr_seq_ordem,
	ie_tipo_prev)
select	nm_usuario_p,
	nvl(trunc(dt_prevista,'dd'),trunc(sysdate,'dd')),
	nr_seq_gerencia,
	nr_seq_grupo_des,
	nr_seq_ordem,
	'SDP'
from	man_estagio_processo p,
	usuario_grupo_des u,
	w_man_ordem_servico a
where	a.nr_seq_grupo_des      = u.nr_seq_grupo
and     a.nm_usuario_exec       = u.nm_usuario_grupo
and	p.nr_sequencia		= a.nr_seq_estagio
and	a.nr_seq_gerencia	= nr_seq_gerencia_p
and	((nr_seq_grupo_des_p = 0) or (a.nr_seq_grupo_des = nr_seq_grupo_des_p))
and	((ie_considera_OS_proj_p = 'S') or (proj_obter_dados_ordem_serv(a.nr_seq_ordem, 'PROJD') is null))
and	a.ie_status_ordem <> 3
and	((p.ie_desenv		= 'S')
	or (p.ie_tecnologia	= 'S'))
and not exists (select	1
		from    usuario_grupo_des c,
			w_man_ordem_servico b
		where   a.nr_seq_ordem  	= b.nr_seq_ordem
		and     b.nr_seq_grupo_des      = a.nr_seq_grupo_des
		and     b.nr_seq_grupo_des      = c.nr_seq_grupo
		and     b.nm_usuario_exec       = c.nm_usuario_grupo
		and	b.dt_prevista is not null)
and exists (	select	1
		from	usuario_grupo_des c,
			w_man_ordem_servico b
		where   a.nr_seq_ordem  	= b.nr_seq_ordem
		and     b.nr_seq_grupo_des      = a.nr_seq_grupo_des
		and     b.nr_seq_grupo_des      = c.nr_seq_grupo
		and     b.nm_usuario_exec       = c.nm_usuario_grupo
		and	b.dt_prevista is null)
group by nvl(trunc(dt_prevista,'dd'),trunc(sysdate,'dd')),
	nr_seq_gerencia,
	nr_seq_grupo_des,
	nr_seq_ordem;
	
commit;

exec_sql_dinamico('Tasy','truncate table w_man_ordem_servico');

end man_gerar_resumo_prev_os;
/
