create or replace
procedure ws_registrar_prescr_proc(	nr_prescricao_p			number,
				cd_exame_integracao_p	varchar2, --corrigir nr_seq_exame_p
				cd_material_integracao_p	varchar2,
				cd_setor_coleta_p		number,
				cd_setor_entrega_p		number,
				cd_setor_atendimento_p	number,
				cd_medico_exec_p		varchar2,
				qt_procedimento_p		number,
				dt_coleta_p		date,
				dt_resultado_p		date,
				dt_prev_execucao_p	date,
				ds_material_especial_p	varchar2,
				nm_usuario_p		varchar2,
				nr_seq_prescricao_p	out number,
				cd_erro_p		out varchar2,
				ds_erro_p			out varchar2) is
					

nr_seq_prescricao_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
cd_equipamento_w		number(10);
cd_material_exame_w		varchar2(20);

ie_existe_prescricao_w		varchar2(1);
ie_existe_procedimento_w	varchar2(1);
ie_existe_material_exame_w	varchar2(1);
ie_existe_setor_coleta_w	varchar2(1);
ie_existe_setor_entrega_w	varchar2(1);
ie_existe_setor_atendimento_w	varchar2(1);
ie_liberada_w			varchar2(1);
nr_seq_exame_w			exame_laboratorio.nr_seq_exame%type;


/* Lista erros
0 - SUCESSO
1 - Campos n�o informados
	1.1 - Prescri��o n�o informada.
	1.2 - Procedimento n�o informado.
	1.3 - Quantidade do procedimento n�o informado
	1.4 - Origem do procedimento n�o informado
	1.5 - Material n�o informado
	1.6 - Exame n�o informado/encontrado
	1.7 - Equipamento "WebService" n�o encontrado
2 - Registros n�o encontrados
	2.1 - Prescri��o n�o encontrada.
	2.2 - Procedimento n�o encontrado.
	2.3 - Material exame n�o encontrado.	
	2.4 - Setor de coleta n�o encontrado.
	2.5 - Setor de entrega n�o encontrado.
	2.6 - Setor de atendimento n�o encontrado	
3 - Prescri��o j� liberada. N�o � poss�vel inserir um procedimento a uma prescri��o j� liberada.
4 - Erro ao procedimento
*/

begin

cd_erro_p	:= 0;

select	max(cd_equipamento)
into	cd_equipamento_w
from	equipamento_lab a
where	a.ds_sigla =  'WebService';

select	max(b.nr_seq_exame),
		max(b.cd_procedimento),
		max(b.ie_origem_proced)
into	nr_seq_exame_w,
		cd_procedimento_w,
		ie_origem_proced_w
from	lab_exame_equip a,
		exame_laboratorio b
where	a.nr_seq_exame = b.nr_seq_exame
and		a.cd_exame_equip = cd_exame_integracao_p
and		cd_equipamento = cd_equipamento_w;

/*if	(nr_seq_exame_p is not null) then
	select	max(cd_procedimento),
		max(ie_origem_proced)
	into	cd_procedimento_w,
		ie_origem_proced_w
	from	exame_laboratorio
	where	nr_seq_exame = nr_seq_exame_p;
else
	cd_procedimento_w	:= cd_procedimento_p;
	ie_origem_proced_w	:= ie_origem_proced_p;
end if;*/

/*SELECT	max(nr_seq_exame),
		max(cd_procedimento),
		max(ie_origem_proced)
into	nr_seq_exame_w,
		cd_procedimento_w,
		ie_origem_proced_w
FROM	exame_laboratorio
WHERE	NVL(cd_exame_integracao, cd_exame) = cd_exame_p;*/

if	(nr_prescricao_p is null) then
	cd_erro_p	:= 1.1;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(277599,null);
elsif	(cd_procedimento_w is null) then
	cd_erro_p	:= 1.2;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279081,null);
elsif	(qt_procedimento_p is null) then
	cd_erro_p	:= 1.3;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279083,null);
elsif	(ie_origem_proced_w is null) then
	cd_erro_p	:= 1.4;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279092,null);
elsif	(cd_material_integracao_p is null) then
	cd_erro_p	:= 1.5;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279093,null);
elsif	(nr_seq_exame_w is null) then
	cd_erro_p	:= 1.6;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279094,null);
elsif	(cd_equipamento_w is null) then
	cd_erro_p	:= 1.7;
	ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279096,null);
end if;

if	(cd_erro_p = 0) then

	select	max(b.cd_material_exame)
	into	cd_material_exame_w
	from	material_exame_lab_int a,
			material_exame_lab b
	where	a.cd_equipamento = cd_equipamento_w
	and		a.cd_material_integracao = cd_material_integracao_p
	and		a.nr_seq_material = b.nr_sequencia;

	select	decode(count(*),0,'N','S')
	into	ie_existe_prescricao_w
	from	prescr_medica a
	where	a.nr_prescricao = nr_prescricao_p;
	
	/*select	decode(count(*),0,'N','S')
	into	ie_existe_procedimento_w
	from	procedimento a
	where	a.cd_procedimento = cd_procedimento_w
	and	a.ie_origem_proced = ie_origem_proced_w;*/
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_material_exame_w
	from	material_exame_lab a
	where	a.cd_material_exame = cd_material_exame_w;
		
	/*select	decode(count(*),0,'N','S')
	into	ie_existe_exame_w
	from	exame_laboratorio a
	where	nvl(a.ie_situacao,'A') = 'A'
	and	a.nr_seq_exame = nr_seq_exame_p;*/
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_setor_coleta_w
	from	setor_atendimento a
	where	a.cd_setor_atendimento = cd_setor_coleta_p
	and	nvl(a.ie_situacao,'A') = 'A';
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_setor_entrega_w
	from	setor_atendimento a
	where	a.cd_setor_atendimento = cd_setor_entrega_p
	and	nvl(a.ie_situacao,'A') = 'A';
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_setor_atendimento_w
	from	setor_atendimento a
	where	a.cd_setor_atendimento = cd_setor_atendimento_p
	and	nvl(a.ie_situacao,'A') = 'A';
	
	if	(ie_existe_prescricao_w  = 'N') then
		cd_erro_p	:= 2.1;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279102,null)||to_char(nr_prescricao_p);
	elsif	(cd_material_exame_w is not null) and
		(ie_existe_material_exame_w = 'N') then
		cd_erro_p	:= 2.2;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279103,null)||cd_material_integracao_p;
	elsif	(cd_setor_coleta_p is not null) and
		(ie_existe_setor_coleta_w = 'N') then
		cd_erro_p	:= 2.3;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279105,null)||to_char(cd_setor_coleta_p);
	elsif	(cd_setor_entrega_p is not null) and
		(ie_existe_setor_entrega_w = 'N') then
		cd_erro_p	:= 2.4;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279108,null)||to_char(cd_setor_entrega_p);
	elsif	(cd_setor_atendimento_p is not null) and
		(ie_existe_setor_atendimento_w = 'N') then
		cd_erro_p	:= 2.5;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(278281,null)||': '||to_char(cd_setor_atendimento_p);
	end if;
	
	select	decode(count(*),0,'N','S')
	into	ie_liberada_w
	from	prescR_medica a
	where	a.nr_prescricao = nr_prescricao_p
	and	a.dt_liberacao is not null;
	
	if	(ie_liberada_w = 'S') then
		cd_erro_p	:= 3;
		ds_erro_p	:= WHEB_MENSAGEM_PCK.get_texto(279109,null);
	end if;
	
	if	(cd_erro_p = 0) then
		
		begin		
		
		select	nvl(max(nr_sequencia),0)+1
		into	nr_seq_prescricao_w
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p;
		
		insert into prescr_procedimento(nr_prescricao,
						nr_sequencia,
						ie_origem_inf,
						cd_procedimento,
						ie_origem_proced,
						cd_material_exame,
						nr_seq_exame,
						qt_procedimento,
						dt_coleta,
						dt_resultado,
						dt_prev_execucao,
						cd_setor_coleta,
						cd_setor_entrega,
						cd_setor_atendimento,
						cd_motivo_baixa,
						cd_medico_exec,
						ie_suspenso,
						nm_usuario,
						dt_atualizacao,
						ie_pendente_amostra,
						ie_exame_bloqueado,
						ds_material_especial,
						ie_status_atend,
						ie_amostra)
			values		(	nr_prescricao_p,
						nr_seq_prescricao_w,
						1,
						cd_procedimento_w,
						ie_origem_proced_w,
						cd_material_exame_w,
						nr_seq_exame_w,
						qt_procedimento_p,
						dt_coleta_p,
						dt_resultado_p,
						dt_prev_execucao_p,
						cd_setor_coleta_p,
						cd_setor_entrega_p,
						cd_setor_atendimento_p,
						0,
						cd_medico_exec_p,
						'N',
						nm_usuario_p,
						sysdate,
						'N',
						'N',
						ds_material_especial_p,
						5,
						'N');
			
		nr_seq_prescricao_p	:= nr_seq_prescricao_w;	
			
		exception
		when others then
			cd_erro_p	:= 4;
			ds_erro_p	:= substr(sqlerrm,1,2000);
		end;		
	
	end if;

end if;

commit;

end ws_registrar_prescr_proc;
/