create or replace
procedure rop_gerar_movto_ressuprimento(	nr_sequencia_p		number,
						nm_usuario_p		varchar2) is 

	
nr_seq_local_w		number(10);
nr_seq_roupa_w		number(10);
qt_maximo_w		number(13,4);
qt_saldo_w		number(13,4);
cd_estabelecimento_w	number(10);
qt_reposicao_w		number(13,4);

cursor C01 is
select	nr_seq_roupa,
	qt_maximo,
	rop_obter_saldo_roupa(nr_seq_roupa, nr_seq_local_w, trunc(sysdate,'mm'))
from	rop_local_padrao_ressup
where	nr_seq_local = nr_seq_local_w;


begin

delete from rop_movto_roupa
where nr_sequencia = nr_sequencia_p
and ie_ressuprimento = 'S';

select	nvl(max(nr_seq_local_orig_dest),0),
	nvl(max(cd_estabelecimento),0)
into	nr_seq_local_w,
	cd_estabelecimento_w
from	rop_lote_movto
where nr_sequencia = nr_sequencia_p;

if	(nr_seq_local_w > 0) then

	open C01;
	loop
	fetch C01 into	
		nr_seq_roupa_w,
		qt_maximo_w,
		qt_saldo_w;
	exit when C01%notfound;
		begin
		
		if	(qt_saldo_w < qt_maximo_w) then
			
			qt_reposicao_w	:= 0;
			qt_reposicao_w	:= qt_maximo_w - qt_saldo_w;	
			
		
			if	(qt_reposicao_w > 0) then
				insert into rop_movto_roupa(
					nr_sequencia,
					nr_seq_lote,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_roupa,
					dt_origem,
					cd_estabelecimento,
					ie_oper_correta,
					qt_roupa,
					ie_ressuprimento)
				values(	rop_movto_roupa_seq.nextval,
					nr_sequencia_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_roupa_w,
					trunc(sysdate,'dd'),
					cd_estabelecimento_w,
					'N',
					qt_reposicao_w,
					'S');
			end if;
		end if;
		
		end;
	end loop;
	close C01;

end if;

commit;

end rop_gerar_movto_ressuprimento;
/