create or replace
procedure TISS_GERAR_ANEXO_GUIA_IMP
			(nm_usuario_p		in varchar2,
			nr_seq_lote_imp_p	in number,
			nr_guia_prestador_p	in varchar2,
			nr_guia_operadora_p	in varchar2,
			cd_senha_p		in varchar2,
			dt_autorizacao_p	in varchar2,
			dt_validade_senha_p	in varchar2,
			cd_usuario_convenio_p	in varchar2,
			nm_paciente_p		in varchar2,
			ie_status_solic_p	in varchar2,
			nr_seq_guia_p		out number,
			dt_autorizacao_date_p	in date default null,
			dt_validade_senha_date_p in date default null) is

dt_autorizacao_w	date;
dt_validade_senha_w	date;
nr_sequencia_w		number(10);			
			
begin

if	(dt_autorizacao_p = 'null') then
	dt_autorizacao_w	:= null;
else
	dt_autorizacao_w	:= to_date(dt_autorizacao_p,'dd/mm/yyyy');
end if;

if	(dt_validade_senha_p = 'null') then
	dt_validade_senha_w	:= null;
else
	dt_validade_senha_w	:= to_date(dt_validade_senha_p,'dd/mm/yyyy');
end if;

if	(dt_autorizacao_date_p is not null) then
	dt_autorizacao_w	:= dt_autorizacao_date_p;
end if;
if	(dt_validade_senha_date_p is not null) then
	dt_validade_senha_w	:= dt_validade_senha_date_p;
end if;

select	TISS_ANEXO_GUIA_IMP_seq.nextval
into	nr_sequencia_w
from	dual;

insert into TISS_ANEXO_GUIA_IMP
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_lote_imp,
	nr_guia_prestador,
	nr_guia_operadora,
	cd_senha,
	dt_autorizacao,
	dt_validade_senha,
	cd_usuario_convenio,
	nm_paciente,
	ie_status_solic)
values	(nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_lote_imp_p,
	nr_guia_prestador_p,
	nr_guia_operadora_p,
	cd_senha_p,
	dt_autorizacao_w,
	dt_validade_senha_w,
	cd_usuario_convenio_p,
	nm_paciente_p,
	ie_status_solic_p);
	
nr_seq_guia_p	:= nr_sequencia_w;	

commit;

end TISS_GERAR_ANEXO_GUIA_IMP;
/