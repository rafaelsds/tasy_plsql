create or replace
procedure Atualizar_Status_Ag_Cons_EUP     (	nr_seq_agenda_p		number,
					ie_status_agenda_p	varchar2,
					nm_usuario_p		varchar2) is
begin

update	agenda_consulta
set        	ie_status_agenda	= ie_status_agenda_p,
   	nm_usuario      	= nm_usuario_p
where      nr_sequencia    	= nr_seq_agenda_p;

commit;

end Atualizar_Status_Ag_Cons_EUP;
/