create or replace
procedure gerar_boleto_reenvio_escrit(	nr_titulo_p		Number,
				nr_seq_tit_reenvio_p	Number,
				cd_banco_p		Number,
				nr_seq_conta_banco_p	Number,
				nr_seq_carteira_cobr_p	Number,
				cd_tipo_portador_p		Number,
				cd_portador_p		Number,
				ds_observacao_p		varchar2,
				ie_alterar_vencimento_p	varchar2,
				dt_vencimento_p		Date,
				cd_motivo_portador_p	varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

ds_historico_w			Varchar2(4000);
cd_banco_w			Number(5);
nr_seq_conta_banco_w		Number(10);
nr_seq_carteira_cobr_w		Number(10);
cd_tipo_portador_w			Number(5);
cd_portador_w			Number(10);
ie_tipo_titulo_w			Varchar2(2);
ds_antigo_w			Varchar2(255);
ds_novo_w			Varchar2(255);
nr_seq_mensalidade_w		Number(10);
dt_vencimento_w			Date;
nr_seq_alteracao_w		Number(10);
dt_vencimento_novo_w		Date;
ds_obervacao_w			Varchar2(255);

begin
	/* 'Alterado os seguintes dados do t�tulo: '*/
ds_historico_w	:= substr(wheb_mensagem_pck.get_texto(302450)||chr(13)||chr(10),1,4000);

select	max(cd_banco),
	max(nr_seq_conta_banco),
	max(nr_seq_carteira_cobr),
	max(cd_tipo_portador),
	max(cd_portador),
	max(ie_tipo_titulo),
	max(nr_seq_mensalidade),
	max(dt_vencimento)
into	cd_banco_w,
	nr_seq_conta_banco_w,
	nr_seq_carteira_cobr_w,
	cd_tipo_portador_w,
	cd_portador_w,
	ie_tipo_titulo_w,
	nr_seq_mensalidade_w,
	dt_vencimento_w
from	titulo_receber
where	nr_titulo	= nr_titulo_p;

if	(ie_alterar_vencimento_p = 'S') then
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_alteracao_w
	from	alteracao_vencimento
	where	nr_titulo = nr_titulo_p;
	
	
	dt_vencimento_novo_w	:= trunc(dt_vencimento_p);
	/*'Data de vencimento alterada de '|| dt_vencimento_w ||' para '|| dt_vencimento_novo_w ||', pois o tipo do t�tulo foi alterado para "Bloqueto"!'*/
	ds_obervacao_w		:= substr(wheb_mensagem_pck.get_texto(302451, 'DT_VENCIMENTO_W=' || dt_vencimento_w || ';DT_VENCIMENTO_NOVO_W='||dt_vencimento_novo_w),1,255);
	
	
	insert into alteracao_vencimento
		(nr_sequencia, nr_titulo, dt_anterior,
		dt_vencimento, cd_motivo, dt_atualizacao,
		nm_usuario, dt_alteracao, ds_observacao)
	values	(nr_seq_alteracao_w, nr_titulo_p, dt_vencimento_w,
		dt_vencimento_novo_w, 1, sysdate,
		nm_usuario_p, sysdate, ds_obervacao_w);

	update	titulo_receber
	set	dt_pagamento_previsto	= dt_vencimento_novo_w
	where	nr_titulo		= nr_titulo_p;
end if;

if	(ds_observacao_p is not null) then
	update	titulo_receber
	set	ds_observacao_titulo	= substr(decode(ds_observacao_titulo,null,ds_observacao_p,ds_observacao_titulo||chr(13)||chr(10)||ds_observacao_p),1,4000)
	where	nr_titulo	= nr_titulo_p;
end if;

if	(cd_banco_w <> cd_banco_p) and
	(cd_banco_p is not null) then
	update	titulo_receber
	set	cd_banco		= cd_banco_p
	where	nr_titulo		= nr_titulo_p;
	
	begin
	select	ds_banco
	into	ds_antigo_w
	from	banco
	where	cd_banco	= cd_banco_w;
	exception
	when others then
		ds_antigo_w	:= ' ';
	end;
	
	begin
	select	ds_banco
	into	ds_novo_w
	from	banco
	where	cd_banco	= cd_banco_w;
	exception
	when others then
		ds_novo_w	:= ' ';
	end;
	
	ds_historico_w	:= substr(ds_historico_w || wheb_mensagem_pck.get_texto(302452,'DS_ANTIGO_W='||ds_antigo_w||';DS_NOVO_W='||ds_novo_w) ||chr(13)||chr(10),1,4000);
				
end if;
	
if	(nr_seq_conta_banco_w <> nr_seq_conta_banco_p) and
	(nr_seq_conta_banco_p is not null) then
	update	titulo_receber
	set	nr_seq_conta_banco	= nr_seq_conta_banco_p
	where	nr_titulo		= nr_titulo_p;
	
	begin
	select	ds_conta
	into	ds_antigo_w
	from	banco_estabelecimento_v
	where	nr_sequencia	= nr_seq_conta_banco_w;
	exception
	when others then
		ds_antigo_w	:= ' ';
	end;
	
	begin
	select	ds_conta
	into	ds_novo_w
	from	banco_estabelecimento_v
	where	nr_sequencia	= nr_seq_conta_banco_p;
	exception
	when others then
		ds_novo_w	:= ' ';
	end;
	--'Ag�ncia/Conta banc�ria, de '||ds_antigo_w||' para '||ds_novo_w||'.'
	ds_historico_w	:= substr(ds_historico_w || wheb_mensagem_pck.get_texto(302520,'DS_ANTIGO_W=' || ds_antigo_w || ';DS_NOVO_W=' || ds_novo_w) ||chr(13)||chr(10),1,4000);
end if;	
	
if	(nr_seq_carteira_cobr_w <> nr_seq_carteira_cobr_p) and
	(nr_seq_carteira_cobr_p is not null) then
	update	titulo_receber
	set	nr_seq_carteira_cobr	= nr_seq_carteira_cobr_p
	where	nr_titulo		= nr_titulo_p;
	
	begin
	select	ds_carteira
	into	ds_antigo_w
	from	banco_carteira
	where	nr_sequencia		= nr_seq_carteira_cobr_w
	and	((cd_estabelecimento	= cd_estabelecimento_p) or (cd_estabelecimento is null));
	exception
	when others then
		ds_antigo_w	:= ' ';
	end;
	
	begin
	select	ds_carteira
	into	ds_novo_w
	from	banco_carteira
	where	nr_sequencia 		= nr_seq_carteira_cobr_p
	and	((cd_estabelecimento	= cd_estabelecimento_p) or (cd_estabelecimento is null));
	exception
	when others then
		ds_novo_w	:= ' ';
	end;
	
	--'Carteira cobran�a, de '||ds_antigo_w||' para '||ds_novo_w||'.'
	ds_historico_w	:= substr(ds_historico_w || wheb_mensagem_pck.get_texto(302521,'DS_ANTIGO_W=' || ds_antigo_w || ';DS_NOVO_W=' || ds_novo_w) ||chr(13)||chr(10),1,4000);
end if;

if	(((cd_tipo_portador_w <> cd_tipo_portador_p) and
	  (cd_tipo_portador_p is not null)) or
	((cd_portador_w <> cd_portador_p) and
	  (cd_portador_p is not null))) then	
	alterar_portador_tit_rec(nr_titulo_p,
				cd_tipo_portador_p,
				cd_portador_p,
				cd_motivo_portador_p,
				null,
				null,
				'',
				nm_usuario_p);					
	
	select	obter_valor_dominio(703,cd_tipo_portador_w),
		obter_valor_dominio(703,cd_tipo_portador_p)
	into	ds_antigo_w,
		ds_novo_w
	from	dual;
	
	--Tipo portador, de '||ds_antigo_w||' para '||ds_novo_w||'.
	ds_historico_w	:= substr(ds_historico_w || wheb_mensagem_pck.get_texto(302522, 'DS_ANTIGO_W=' || ds_antigo_w || ';DS_NOVO_W=' || ds_novo_w) ||chr(13)||chr(10),1,4000);
	
	begin
	select	ds_portador
	into	ds_antigo_w
	from	portador
	where	cd_portador = cd_portador_w;
	exception
	when others then
		ds_antigo_w	:= ' ';
	end;
	
	begin
	select	ds_portador
	into	ds_novo_w
	from	portador
	where	cd_portador = cd_portador_p;
	exception
	when others then
		ds_novo_w	:= ' ';
	end;
	
	--'Portador, de '||ds_antigo_w||' para '||ds_novo_w||'.'
	ds_historico_w	:= substr(ds_historico_w || wheb_mensagem_pck.get_texto(302523, 'DS_ANTIGO_W=' || ds_antigo_w || ';DS_NOVO_W=' || ds_novo_w) ||chr(13)||chr(10),1,4000);
	
end if;

select	obter_valor_dominio(712,ie_tipo_titulo_w)
into	ds_antigo_w
from	dual;

--Tipo t�tulo, de '||ds_antigo_w||' para Bloqueto(Boleto Banc�rio).
ds_historico_w	:= substr(ds_historico_w || wheb_mensagem_pck.get_texto(302524,'DS_ANTIGO_W=' || ds_antigo_w),1,4000);

insert into cobr_esc_reenv_tit_hist
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	dt_historico,
	nr_seq_tit_reenvio,
	ds_historico,
	dt_liberacao)
values	(cobr_esc_reenv_tit_hist_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	sysdate,
	nr_seq_tit_reenvio_p,
	ds_historico_w,
	sysdate);
		
update	titulo_receber
set	ie_tipo_titulo		= '1',
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_titulo		= nr_titulo_p;

update	lote_cobr_esc_reenv_tit
set	dt_geracao_boleto	= sysdate,
	nm_usuario_boleto	= nm_usuario_p
where	nr_sequencia		= nr_seq_tit_reenvio_p;

gerar_bloqueto_tit_rec(nr_titulo_p, 'CE');

/*N�o dar commit */

end gerar_boleto_reenvio_escrit;
/