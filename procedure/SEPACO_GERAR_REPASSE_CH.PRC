create or replace 
procedure SEPACO_GERAR_REPASSE_CH(	nr_repasse_terceiro_p	number,
					vl_preco_ch_p		number default 0.30,
					cd_edicao_amb92_p	number default 92,
					cd_edicao_amb90_p	number default 90,
					cd_edicao_amb96_p	number default 96,
					cd_edicao_amb99_p	number default 1999,
					nm_usuario_p		varchar2) is

/* Procedure para realizar o c�lculo dos valores a serem repassados conforme a quantidade de CH de cada procedimento.
Esta Procedure deve ser vinculada ao campo PROCEDURE (DS_PROCEDURE) no cadastro do TERCEIRO.
SEMPRE QUE REALIZAR ALTERA��O NESTA ROTINA, INFORMA A PHILIPS E ENVIAR A ROTINA ATUALIZADA */

/*Faixa de CH

at� 1000 CH             paga 150% AMB92 x R$0,30
de  1001 a 1500 CH paga 180% AMB92 x R$0,30
acima  de  1500 CH  paga 200% AMB92 x R$0,30
*/

--Cursor para retornas os m�dicos vinculados ao terceiro do repasse
Cursor	c_medicos_terceiro(	nr_seq_terceiro_pc	terceiro.nr_sequencia%type,
				dt_periodo_final_pc	date) is
	select	a.cd_pessoa_fisica
	from	terceiro a
	where	a.nr_sequencia = nr_seq_terceiro_pc
	and	a.cd_pessoa_fisica is not null
	and	nvl(a.dt_fim_vigencia,dt_periodo_final_pc) >= dt_periodo_final_pc
	and	a.ie_situacao = 'A'
	union
	select	a.cd_pessoa_fisica
	from	terceiro_pessoa_fisica a
	where	a.nr_seq_terceiro = nr_seq_terceiro_pc
	and	nvl(a.dt_fim_vigencia,dt_periodo_final_pc) >= dt_periodo_final_pc;

Cursor	c_procedimentos(	cd_pessoa_fisica_pc	pessoa_fisica.cd_pessoa_fisica%type,
				dt_periodo_inicial_pc	date,
				dt_periodo_final_pc	date) is
	select	a.nr_sequencia,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.dt_procedimento,
		a.qt_procedimento,
		a.nr_atendimento,
		a.nr_interno_conta,
		a.ie_funcao_medico cd_funcao
	from	procedimento_paciente a
	where	a.cd_medico_executor = cd_pessoa_fisica_pc
	and	a.dt_procedimento between dt_periodo_inicial_pc and dt_periodo_final_pc
	and	a.cd_motivo_exc_conta is null
	union
	select	a.nr_sequencia,
		b.cd_procedimento,
		b.ie_origem_proced,
		b.dt_procedimento,
		b.qt_procedimento,
		b.nr_atendimento,
		b.nr_interno_conta,
		a.ie_funcao cd_funcao
	from	procedimento_paciente b,
		procedimento_participante a
	where	a.nr_sequencia = b.nr_sequencia
	and	a.cd_pessoa_fisica	= cd_pessoa_fisica_pc
	and	b.dt_procedimento between dt_periodo_inicial_pc and dt_periodo_final_pc
	and	b.cd_motivo_exc_conta is null;

--Par�metros gerais
dt_periodo_inicial_w	date;
dt_periodo_final_w	date;
nr_seq_terceiro_w	terceiro.nr_sequencia%type;
nr_seq_item_w		repasse_terceiro_item.nr_sequencia_item%type;
vl_medico_w		preco_amb.vl_medico%type;
vl_valor_ch_w		number(15,2);

--Vari�veis controle de qual valor buscar na CH
ie_medico_w		varchar2(1);
ie_anestesista_w	varchar2(1);
ie_auxiliar_w		varchar2(1);
ie_instrumentador_w	varchar2(1);
ie_obstetra_w		varchar2(1);
ie_pediatra_w		varchar2(1);
ie_cirurgiao_w		varchar2(1);

vl_preco_ch_w		number(15,2);
cd_edicao_amb92_w	number(10);
cd_edicao_amb90_w	number(10);
cd_edicao_amb96_w	number(10);
cd_edicao_amb99_w	number(10);

begin

vl_preco_ch_w		:=  0.30;
cd_edicao_amb92_w	:= 92;
cd_edicao_amb90_w	:= 90;
cd_edicao_amb96_w	:= 96;
cd_edicao_amb99_w	:= 1999;

--Retorna os dados da conta de repasse para a obten��o dos dados
select	a.dt_periodo_inicial,
	a.dt_periodo_final,
	a.nr_seq_terceiro
into	dt_periodo_inicial_w,
	dt_periodo_final_w,
	nr_seq_terceiro_w
from	repasse_terceiro a
where	a.nr_repasse_terceiro = nr_repasse_terceiro_p;

-- Retorno dos m�dicos do terceiro
for r_c_medicos_terceiro in c_medicos_terceiro(nr_seq_terceiro_w, dt_periodo_final_w) loop

	-- Retorno dos procedimentos
	for r_c_procedimentos in c_procedimentos(r_c_medicos_terceiro.cd_pessoa_fisica, dt_periodo_inicial_w, dt_periodo_final_w) loop
		
		begin
		select	nvl(a.ie_anestesista,'N'),
			nvl(a.ie_auxiliar,'N'),
			nvl(a.ie_instrumentador,'N'),
			nvl(a.ie_obstetra,'N'),
			nvl(a.ie_pediatra,'N'),
			nvl(a.ie_cirurgiao,'N'),
			nvl(a.ie_medico,'S')
		into	ie_anestesista_w,
			ie_auxiliar_w,
			ie_instrumentador_w,
			ie_obstetra_w,
			ie_pediatra_w,
			ie_cirurgiao_w,
			ie_medico_w
		from	funcao_medico a
		where	cd_funcao = r_c_procedimentos.cd_funcao;
		exception
		when others then
			ie_anestesista_w	:= 'N';
			ie_auxiliar_w		:= 'N';
			ie_instrumentador_w	:= 'N';
			ie_obstetra_w		:= 'N';
			ie_pediatra_w		:= 'N';
			ie_cirurgiao_w		:= 'N';
			ie_medico_w		:= 'S';
		end;
		
		vl_medico_w	:= 0;
		
		if	((ie_cirurgiao_w = 'S') or
			(ie_pediatra_w	= 'S') or
			(ie_obstetra_w = 'S') or
			(ie_instrumentador_w = 'S')) or
			(ie_medico_w = 'S' and
			ie_anestesista_w = 'N' and
			ie_auxiliar_w = 'N' and
			ie_instrumentador_w = 'N' and
			ie_obstetra_w = 'N' and
			ie_pediatra_w = 'N' and
			ie_cirurgiao_w = 'N') then
			
			begin
			select	a.vl_medico
			into	vl_medico_w
			from	preco_amb a
			where	a.cd_edicao_amb = cd_edicao_amb92_w
			and	a.cd_moeda = 2
			and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
			and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;
			exception
			when others then
				vl_medico_w := 0;
			end;
			
			if	(nvl(vl_medico_w,0) = 0) then
				
				begin
				select	a.vl_medico
				into	vl_medico_w
				from	preco_amb a
				where	a.cd_edicao_amb = cd_edicao_amb90_w
				and	a.cd_moeda = 2
				and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
				and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;				
				exception
				when others then
					vl_medico_w := 0;
				end;
				
				if	(nvl(vl_medico_w,0) = 0) then
				
					begin
					select	a.vl_medico
					into	vl_medico_w
					from	preco_amb a
					where	a.cd_edicao_amb = cd_edicao_amb96_w
					and	a.cd_moeda = 2
					and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
					and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;					
					exception
					when others then
						vl_medico_w := 0;
					end;
					
					if	(nvl(vl_medico_w,0) = 0) then
						begin
						select	a.vl_medico
						into	vl_medico_w
						from	preco_amb a
						where	a.cd_edicao_amb = cd_edicao_amb99_w
						and	a.cd_moeda = 2
						and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
						and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;						
						exception
						when others then
							vl_medico_w := 0;
						end;
						
					end if;
				end if;
			end if;
		elsif	(ie_anestesista_w = 'S') then
			
			begin
			select	a.vl_anestesista
			into	vl_medico_w
			from	preco_amb a
			where	a.cd_edicao_amb = cd_edicao_amb92_w
			and	a.cd_moeda = 2
			and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
			and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;			
			exception
			when others then
				vl_medico_w := 0;
			end;
			
			if	(nvl(vl_medico_w,0) = 0) then
			
				begin
				select	a.vl_anestesista
				into	vl_medico_w
				from	preco_amb a
				where	a.cd_edicao_amb = cd_edicao_amb90_w
				and	a.cd_moeda = 2
				and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
				and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;				
				exception
				when others then
					vl_medico_w := 0;
				end;
			
				if	(nvl(vl_medico_w,0) = 0) then
				
					begin
					select	a.vl_anestesista
					into	vl_medico_w
					from	preco_amb a
					where	a.cd_edicao_amb = cd_edicao_amb96_w
					and	a.cd_moeda = 2
					and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
					and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;
					exception					
					when others then
						vl_medico_w := 0;
					end;
					
					if	(nvl(vl_medico_w,0) = 0) then
						begin
						select	a.vl_anestesista
						into	vl_medico_w
						from	preco_amb a
						where	a.cd_edicao_amb = cd_edicao_amb99_w
						and	a.cd_moeda = 2
						and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
						and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;						
						exception
						when others then
							vl_medico_w := 0;
						end;
					end if;
				end if;
			end if;
		elsif	(ie_auxiliar_w = 'S') then
			
			begin
			select	a.vl_auxiliares
			into	vl_medico_w
			from	preco_amb a
			where	a.cd_edicao_amb = cd_edicao_amb92_w
			and	a.cd_moeda = 2
			and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
			and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;			
			exception
			when others then
				vl_medico_w := 0;
			end;
			
			if	(nvl(vl_medico_w,0) = 0) then
				
				begin
				select	a.vl_auxiliares
				into	vl_medico_w
				from	preco_amb a
				where	a.cd_edicao_amb = cd_edicao_amb90_w
				and	a.cd_moeda = 2
				and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
				and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;				
				exception
				when others then
					vl_medico_w := 0;
				end;			
			
				if	(nvl(vl_medico_w,0) = 0) then
					
					begin
					select	a.vl_auxiliares
					into	vl_medico_w
					from	preco_amb a
					where	a.cd_edicao_amb = cd_edicao_amb96_w
					and	a.cd_moeda = 2
					and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
					and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;					
					exception
					when others then
						vl_medico_w := 0;
					end;
					
					if	(nvl(vl_medico_w,0) = 0) then
						
						begin
						select	a.vl_auxiliares
						into	vl_medico_w
						from	preco_amb a
						where	a.cd_edicao_amb = cd_edicao_amb99_w
						and	a.cd_moeda = 2
						and	a.cd_procedimento = r_c_procedimentos.cd_procedimento
						and	a.ie_origem_proced = r_c_procedimentos.ie_origem_proced;						
						exception
						when others then
							vl_medico_w := 0;
						end;
					end if;
				end if;
			end if;
		end if;
		
		if	(nvl(vl_medico_w,0) <> 0) then
			
			vl_medico_w	:= vl_medico_w * r_c_procedimentos.qt_procedimento;
			
			if	(vl_medico_w < 1001) then
				vl_valor_ch_w	:= vl_medico_w * 1.5 * vl_preco_ch_w;
				
			elsif	(vl_medico_w > 1000 and vl_medico_w < 1501) then
				vl_valor_ch_w	:= vl_medico_w * 1.8 * vl_preco_ch_w;
				
			else
				vl_valor_ch_w 	:= vl_medico_w * 2 * vl_preco_ch_w;
			end if;
			
			select	nvl(max(nr_sequencia_item),0) + 1
			into	nr_seq_item_w
			from	repasse_terceiro_item
			where	nr_repasse_terceiro = nr_repasse_terceiro_p;
			
			insert	into	repasse_terceiro_item(	dt_atualizacao,
								nm_usuario,
								nr_sequencia,
								nr_sequencia_item,
								vl_repasse,
								nr_seq_terceiro,
								nr_repasse_terceiro,
								nr_atendimento,
								nr_interno_conta,
								cd_procedimento,
								ie_origem_proced,
								cd_medico)
			values		(			sysdate,
								nm_usuario_p,
								repasse_terceiro_item_seq.nextval,
								nr_seq_item_w,
								vl_valor_ch_w,
								nr_seq_terceiro_w,
								nr_repasse_terceiro_p,
								r_c_procedimentos.nr_atendimento,
								r_c_procedimentos.nr_interno_conta,
								r_c_procedimentos.cd_procedimento,
								r_c_procedimentos.ie_origem_proced,
								r_c_medicos_terceiro.cd_pessoa_fisica);
		end if;
		
	--Fim do loop do cursor c_procedimentos
	end loop;
	
--Fim do loop do cursor c_medicos_terceiro
end loop;

end SEPACO_GERAR_REPASSE_CH;
/