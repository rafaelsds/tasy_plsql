create or replace
procedure pat_calcular_deprec_fiscal(
		cd_estabelecimento_p		number,
		dt_referencia_p			date,
		nr_seq_bem_p			number,
		vl_deprec_mes_p		out	number,
		vl_deprec_acum_p	out	number,
		vl_fiscal_p		out	number,
		vl_base_deprec_p		number,
		tx_ajuste_p		out	number) is

dt_dia_baixa_w				number(2);
dt_dia_inicio_w				number(2);
dt_historico_w				date;
dt_inicio_deprec_w			date;
ie_depreciado_w				varchar2(01);
ie_primeiro_mes_w			varchar2(01);
qt_dia_baixa_w				number(2);
qt_dia_normal_w				number(2);
qt_dias_w				number(10);
qt_percentual_w				number(15,4);
tx_depreciacao_w			number(22,2);
vl_baixa_bem_w				number(22,2);
vl_baixa_deprec_w			number(22,2);
vl_base_baixa_w				number(22,2);
vl_base_deprec_ant_w			number(22,2);
vl_base_deprec_w			number(22,2);
vl_deprec_acum_ant_w			number(22,2);
vl_deprec_acum_w			number(22,2);
vl_deprec_ano_w				number(22,2);
vl_depr_baixa_w				number(22,2);
vl_deprec_bem_ant_w			number(22,2);
vl_deprec_bem_w				number(22,2);
vl_depr_normal_w			number(22,2);
vl_fiscal_w				number(22,2);
vl_original_w				number(15,2);

nr_seq_indice_w				tipo_indice_reajuste.nr_sequencia%Type;
tx_ajuste_w				indice_reajuste_valor.tx_ajuste%Type;
tx_indice_aquisicao_w			pat_valor_bem.tx_indice_aquisicao%Type;

begin

tx_depreciacao_w	:= nvl(obter_taxa_deprec_fiscal_bem(nr_seq_bem_p, dt_referencia_p),0);

if	(tx_depreciacao_w <> 0) then
	begin

	select	max(nvl(a.dt_inicio_deprec, a.dt_inicio_uso)),
		nvl(max(a.vl_original),0)
	into	dt_inicio_deprec_w,
		vl_original_w
	from	pat_bem a
	where	a.nr_sequencia	= nr_seq_bem_p;

	/* Valores de deprecia��es anteriores */
	select	nvl(max(round(vl_deprec_acum_fis,2)),0),
		decode(nvl(count(*),0),0,'S','N'),
		decode(min(round(nvl(vl_fiscal, vl_contabil),2)),0,'S','N')	/*Considerar se depreciado pelo vl fiscal, pois se o cont�bil zerar deve continuar depreciando o fiscal at� 0*/
	into	vl_deprec_acum_ant_w,
		ie_primeiro_mes_w,
		ie_depreciado_w
	from	pat_valor_bem
	where	nr_seq_bem = nr_seq_bem_p;

	select	/*nvl(max(round(vl_base_deprec,2)),vl_original_w), - Alterado conforme OS 676407*/
		nvl(max(round(vl_original_w,2)),vl_original_w),
		nvl(max(vl_deprec_mes),0)
	into	vl_base_deprec_ant_w,
		vl_deprec_bem_ant_w
	from	pat_valor_bem
	where	nr_seq_bem = nr_seq_bem_p
	and	PKG_DATE_UTILS.start_of(dt_valor, 'MONTH', 0) = PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p,-1, 0), 'MONTH', 0);

	/* Valor base para deprecia��o � o valor base do m�s anterior ou o valor original do bem se ainda n�o tiver deprecia��o */
	vl_base_deprec_w	:= (vl_base_deprec_ant_w);

	/*Matheus 24/11/2010 - Controle onde o m�s de aquisi��o � diferente do m�s de uso, e teve mes anterior zerado.*/
	if	(ie_primeiro_mes_w = 'N') and
		(vl_deprec_bem_ant_w = 0) and
		(PKG_DATE_UTILS.start_of(dt_inicio_deprec_w, 'MONTH', 0) = PKG_DATE_UTILS.start_of(dt_referencia_p, 'MONTH', 0)) then
		ie_primeiro_mes_w	:= 'S';
	end if;

	/* Se a Deprecia��o acumulada � menor ou igual ao valor base para deprecia��o, entra na rotina de c�lculo  */

	if	(vl_deprec_acum_ant_w <= vl_base_deprec_w) then
		begin
		vl_deprec_ano_w		:= (vl_base_deprec_w * tx_depreciacao_w) / 100;
		vl_deprec_bem_w		:= 0;
		vl_baixa_bem_w		:= 0;
		vl_baixa_deprec_w	:= 0;

		/* Verifica dados de baixa e venda do Imobilizado */
		select	nvl(max(a.qt_percentual),0),
			nvl(max(a.dt_historico),null)
		into	qt_percentual_w,
			dt_historico_w
		from	pat_historico_bem a,
			pat_tipo_historico b
		where	a.nr_seq_bem	= nr_seq_bem_p
		and	a.nr_seq_tipo	= b.nr_sequencia
		and	b.ie_valor	in ('B','V')
		and	PKG_DATE_UTILS.start_of(a.dt_historico, 'MONTH', 0) = PKG_DATE_UTILS.start_of(dt_referencia_p, 'MONTH', 0)
		order by a.dt_historico asc;

		/* Rotina calculos das baixas/vendas */
		if	(qt_percentual_w <> 0) then
			begin
			dt_dia_inicio_w	:= 0;
			dt_dia_baixa_w	:= PKG_DATE_UTILS.extract_field('DAY', dt_historico_w);

			if	(ie_primeiro_mes_w = 'S') then
				dt_dia_inicio_w	:= PKG_DATE_UTILS.extract_field('DAY', dt_inicio_deprec_w);
			end if;

			qt_dia_normal_w	:= dt_dia_baixa_w - dt_dia_inicio_w;
			qt_dia_baixa_w	:= 0;
			if	(qt_percentual_w <> 100) then
				qt_dia_baixa_w	:= 30 - dt_dia_baixa_w;
			end if;

			vl_baixa_bem_w	:= ((vl_base_deprec_ant_w * qt_percentual_w) / 100);
			vl_depr_normal_w	:= round(((vl_deprec_ano_w / 12) / 30) * qt_dia_normal_w,4);

			/* tratar valores muito pequenos de deprecia��o - baixa */

			if	(round(vl_depr_normal_w,4) <> 0) and
				(round(vl_depr_normal_w,2) < 0.01) then
				vl_depr_normal_w	:= vl_deprec_bem_ant_w;
			end if;

			if	(ie_depreciado_w = 'S') then
				vl_depr_normal_w:= 0;
			end if;

			vl_base_baixa_w	:= (((vl_base_deprec_w - vl_baixa_bem_w) * tx_depreciacao_w) / 100);
			vl_depr_baixa_w	:= (((vl_base_baixa_w) / 12) / 30) * qt_dia_baixa_w;
			vl_baixa_deprec_w 	:= ((vl_deprec_acum_ant_w + vl_depr_normal_w) * qt_percentual_w) / 100;
			vl_deprec_bem_w	:= vl_depr_normal_w + vl_depr_baixa_w;
			vl_base_deprec_w 	:= (vl_base_deprec_w - vl_baixa_bem_w);
			end;
		end if;

		/* INICIO CALCULO DA DEPRECIA��O */
		/* Calcula a depreciacao se ainda n�o foi totalmente depreciado e ja esta em uso */

		if	(vl_deprec_bem_w = 0) and
			(ie_depreciado_w = 'N') and
			(dt_inicio_deprec_w <= dt_referencia_p) then
			begin
			if	(ie_primeiro_mes_w = 'S') then /* Se primeiro c�lculo */
				begin
				qt_dias_w	:= (PKG_DATE_UTILS.start_of(dt_referencia_p,'DD', 0) - PKG_DATE_UTILS.start_of(dt_inicio_deprec_w,'DD', 0));
				vl_deprec_bem_w	:= vl_deprec_bem_w + ((vl_deprec_ano_w / 12) / 30) * qt_dias_w;
				end;
			else
				vl_deprec_bem_w	:= vl_deprec_bem_w + (vl_deprec_ano_w / 12);
			end if;

			end;
		end if;

		vl_deprec_acum_w		:= ((vl_deprec_acum_ant_w + vl_deprec_bem_w) - vl_baixa_deprec_w);

		if	(vl_deprec_acum_w > vl_base_deprec_w) then
			begin
			vl_deprec_acum_w	:= vl_base_deprec_w;
			vl_deprec_bem_w		:= vl_base_deprec_w - vl_deprec_acum_ant_w;
			vl_fiscal_w		:= 0;
			end;
		else
			vl_fiscal_w		:= (vl_base_deprec_w - vl_deprec_acum_w);
		end if;
		end;
	end if;
	end;

vl_deprec_mes_p		:= nvl(vl_deprec_bem_w,0);
vl_deprec_acum_p		:= nvl(vl_deprec_acum_w, 0);
vl_fiscal_p		:= nvl(vl_fiscal_w, 0);
end if;

end pat_calcular_deprec_fiscal;
/
