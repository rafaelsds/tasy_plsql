create or replace
procedure atualizar_controle_qualidade( nr_seq_lista_prod_p 	varchar2,
					nr_seq_qualidade_p	number,
					nm_usuario_p		Varchar2) is 

					
nr_seq_lista_w		varchar2(255);
nr_pos_virgula_w	number(10,0);	
nr_sequencia_w		number(10,0);	
nr_seq_qual_prod_w	number(10,0);
nr_seq_derivado_w	number(10,0);
nr_seq_exame_w		number(10,0);


Cursor C01 is
	SELECT 	nr_seq_exame
	FROM   	san_regra_exame_cont_item b,
		san_regra_exame_controle a
	WHERE  	a.nr_sequencia			= b.nr_seq_regra_exame
	AND	a.nr_seq_derivado		= nr_seq_derivado_w;   


begin
if	(nr_seq_lista_prod_p is not null)	then
	begin	
	nr_seq_lista_w	:= nr_seq_lista_prod_p;
	while	(nr_seq_lista_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(nr_seq_lista_w,',');
		if	(nr_pos_virgula_w > 0)	then
			begin
			nr_sequencia_w	:= to_number(substr(nr_seq_lista_w,1,nr_pos_virgula_w-1));
			nr_seq_lista_w	:= substr(nr_seq_lista_w,nr_pos_virgula_w+1,length(nr_seq_lista_w));
			if	(nr_sequencia_w > 0) then
				begin
				
				select	san_controle_qual_prod_seq.nextVal
				into	nr_seq_qual_prod_w
				from	dual;
				
				insert into	san_controle_qual_prod(
						nr_sequencia,           
						dt_atualizacao,         
						nm_usuario,             
						dt_atualizacao_nrec,
						nm_usuario_nrec,        
						nr_seq_producao,        
						nr_seq_qualidade)
					values (nr_seq_qual_prod_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_sequencia_w,
						nr_seq_qualidade_p);
						
				select	nr_seq_derivado
				into	nr_seq_derivado_w
				from	san_producao
				where	nr_sequencia = nr_sequencia_w;				
				
				open C01;
				loop
				fetch C01 into	
					nr_seq_exame_w;
				exit when C01%notfound;
					begin
					
					insert	into	san_controle_qual_exame(
							nr_sequencia,           
							dt_atualizacao,         
							nm_usuario,             
							dt_atualizacao_nrec,    
							nm_usuario_nrec,        
							nr_seq_exame,           
							nr_seq_qual_prod )
						values( 
							san_controle_qual_exame_seq.nextVal,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_exame_w,
							nr_seq_qual_prod_w);
					end;
				end loop;
				close C01;
				end;
			end if;
			end;
		else
			nr_seq_lista_w := null;
		end if;
		end;
	end loop;
	end;
end if;

commit;

end atualizar_controle_qualidade;
/