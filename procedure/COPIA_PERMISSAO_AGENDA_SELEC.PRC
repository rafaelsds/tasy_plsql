CREATE OR REPLACE
PROCEDURE Copia_Permissao_Agenda_Selec	(cd_agenda_p		number,
						nr_seq_perm_copiar_p	number,
						nm_usuario_destino_p	varchar2,
						cd_perfil_destino_p	number,
						cd_setor_destino_p	number) is

cd_pessoa_destino_w	varchar2(10);

begin
select	max(cd_pessoa_fisica)
into	cd_pessoa_destino_w
from	usuario
where	upper(nm_usuario) = upper(nm_usuario_destino_p);

if	(nr_seq_perm_copiar_p	is not null) and
	((cd_pessoa_destino_w	<> '0') or 
	(cd_perfil_destino_p	is not null) or
	(cd_setor_destino_p	is not null)) then
	insert into med_permissao	(
					nr_sequencia,
					cd_medico_prop,
					dt_atualizacao,
					nm_usuario,
					ie_paciente,
					ie_atendimento,
					ie_evolucao,
					ie_protocolo,
					ie_receita,
					ie_solic_exame,
					ie_agenda,
					ie_resultado,
					ie_consulta,
					ie_fechar_atend,
					ie_med_padrao,
					ie_exame_padrao,
					ie_permissao,
					ie_config_relat,
					ie_grupo_medico,
					ie_diagnostico,
					ie_texto_adicional,
					ie_referencia,
					ie_eis,
					ie_enderecos,
					ie_texto_padrao,
					ie_parametro,
					ie_config_agenda,
					cd_pessoa_fisica,
					cd_medico,
					nr_seq_grupo,
					cd_perfil,
					cd_setor_atendimento,
					ie_permite_excluir_agenda,
					ie_permite_bloquear_agenda,
					cd_agenda)
					(select	med_permissao_seq.nextval,
							a.cd_medico_prop,
							a.dt_atualizacao,
							a.nm_usuario,
							a.ie_paciente,
							a.ie_atendimento,
							a.ie_evolucao,
							a.ie_protocolo,
							a.ie_receita,
							a.ie_solic_exame,
							a.ie_agenda,
							a.ie_resultado,
							a.ie_consulta,
							a.ie_fechar_atend,
							a.ie_med_padrao,
							a.ie_exame_padrao,
							a.ie_permissao,
							a.ie_config_relat,
							a.ie_grupo_medico,
							a.ie_diagnostico,
							a.ie_texto_adicional,
							a.ie_referencia,
							a.ie_eis,
							a.ie_enderecos,
							a.ie_texto_padrao,
							a.ie_parametro,
							a.ie_config_agenda,
							cd_pessoa_destino_w,
							a.cd_medico,
							a.nr_seq_grupo,
							cd_perfil_destino_p,
							cd_setor_destino_p,
							ie_permite_excluir_agenda,
							ie_permite_bloquear_agenda,
							cd_agenda_p
						from	med_permissao a
						where	a.nr_sequencia = nr_seq_perm_copiar_p);
end if;
commit;
END Copia_Permissao_Agenda_Selec;
/
