create or replace
procedure finalizar_baixar_mat_barras(	nr_sequencia_p		number,
				nr_prescricao_p		number,
				cd_material_p		number,
				nr_atendimento_p		number,
				ds_observacao_p		Varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2,
				nr_seq_lote_p		number) is 



ie_baixa_compl_w		varchar2(1);
qt_total_dispensar_w	number(18,6);
qt_material_w		number(9,3);
nr_ordem_compra_w	number(10);
ds_lote_fornec_w	varchar2(255);
begin

obter_param_usuario(24, 54, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_baixa_compl_w);

if	(nvl(ie_baixa_compl_w, 'N') = 'S') and
	(nvl(nr_sequencia_p, 0) > 0) and
	(nvl(nr_prescricao_p, 0) > 0) then
	begin
	
	Select	nvl(a.qt_total_dispensar, 0),
		nvl(sum(b.qt_material), 0) qt_material
	into	qt_total_dispensar_w,
		qt_material_w
	from	material_atend_paciente b,
		prescr_material a
        	 where	a.nr_prescricao		= nr_prescricao_p
         	and	a.nr_sequencia		= nr_sequencia_p
	and	a.nr_prescricao		= b.nr_prescricao
      	and	a.nr_sequencia		= b.nr_sequencia_prescricao
	group by	a.qt_total_dispensar;
	
	if	(qt_total_dispensar_w < qt_material_w) then
		
		update	prescr_material
		set	dt_baixa		= sysdate,
			cd_motivo_baixa	= 1,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_prescricao	= nr_prescricao_p
		and	nr_sequencia	= nr_sequencia_p
		and	cd_motivo_baixa	= 0;
		
	end if;
	
	select	max(a.nr_ordem_compra)
	into	nr_ordem_compra_w
	from	ordem_compra a,
		ordem_compra_item b
	where	a.nr_atendimento	= b.nr_atendimento
	and	a.nr_atendimento	= nr_atendimento_p
	and	b.cd_material	= cd_material_p;
	
	if	(nvl(nr_ordem_compra_w, 0) > 0) then
		if (nvl(nr_seq_lote_p,0) <> 0)	then
			select	ds_lote_fornec 
			into	ds_lote_fornec_w
			from	material_lote_fornec 
			where	nr_sequencia = nr_seq_lote_p;
		end if;
		
		update	ordem_compra_item
		set	ds_observacao	= substr(ds_observacao || ds_observacao_p, 1, 255),
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
	  	where	nr_ordem_compra	= nr_ordem_compra_w
		and	cd_material	= cd_material_p;
		
	end if;
	
	end;
end if;

commit;

end finalizar_baixar_mat_barras;
/
