create or replace
procedure ajustar_vol_sol_dialise (nr_prescricao_p number,
								  nr_sequencia_p  number,
								  valor_p	  	  number) is 
															
begin

update prescr_solucao
set qt_solucao_total = valor_p
where nr_prescricao  = nr_prescricao_p
and nr_seq_solucao   = nr_sequencia_p;

commit;

end ajustar_vol_sol_dialise;
/