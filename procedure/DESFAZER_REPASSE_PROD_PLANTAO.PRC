create or replace
procedure DESFAZER_REPASSE_PROD_PLANTAO
			(nr_seq_prod_plantao_p	number,
			nm_usuario_p		varchar2) is

qt_item_w	number(10);
vl_total_w	number(15,2);
ds_ocorrencia_w	varchar2(4000);

begin

select	count(*),
	sum(a.vl_repasse)
into	qt_item_w,
	vl_total_w
from	repasse_producao_plantao a
where	a.nr_seq_prod_plantao	= nr_seq_prod_plantao_p;

delete	from repasse_producao_plantao
where	nr_seq_prod_plantao	= nr_seq_prod_plantao_p;

/*ds_ocorrencia_w	:=	'Desfeita simula��o de repasse.' || chr(13) || chr(10) ||
			'Itens exclu�dos da simula��o: ' || qt_item_w || chr(13) || chr(10) ||
			'Valor total da simula��o: ' || to_char(vl_total_w,'999999990.00');*/
			
ds_ocorrencia_w	:=	wheb_mensagem_pck.get_texto(304458,'QT_ITEM_W='||qt_item_w||';VL_TOTAL_W='||to_char(vl_total_w,'999999990.00'));

gerar_repasse_prod_plantao_log(	nr_seq_prod_plantao_p,
				'F',
				'S',
				ds_ocorrencia_w,
				nm_usuario_p);

commit;

end DESFAZER_REPASSE_PROD_PLANTAO;
/
