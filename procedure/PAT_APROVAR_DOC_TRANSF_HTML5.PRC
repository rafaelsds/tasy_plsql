create or replace
procedure pat_aprovar_doc_transf_html5(	nr_sequencia_p		number,
				cd_estabelecimento_p	number,
				ie_operacao_p		varchar2,
				nm_usuario_p		varchar2,
				dt_referencia_p date default sysdate) is

ie_gerar_transf_bem_w		varchar2(1);
ie_gerar_envio_comm_doc_w varchar2(1);
ie_transferencia_w			varchar2(1);
ie_valor_w			varchar2(1);
nr_seq_tipo_w			number(10);

begin

ie_gerar_transf_bem_w	:= nvl(obter_valor_param_usuario(937, 44, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'N');
ie_gerar_envio_comm_doc_w	:= nvl(obter_valor_param_usuario(937, 20, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p), 'N');


update	pat_doc_transferencia
set	dt_aprovacao	= decode(ie_operacao_p,'A',sysdate,'E',null),
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;


if	(ie_gerar_transf_bem_w = 'S') then

	select	max(nr_seq_tipo)
	into	nr_seq_tipo_w
	from	pat_doc_transferencia
	where	nr_sequencia	= nr_sequencia_p;

	select	nvl(max(a.ie_transferencia), 'N'),
		nvl(max(a.ie_valor), 'N')
	into	ie_transferencia_w,
		ie_valor_w
	from	pat_tipo_historico a
	where	a.nr_sequencia	= nr_seq_tipo_w;
	
	if	(ie_transferencia_w = 'S') then
		
		pat_gerar_transf_docto(nr_sequencia_p, nm_usuario_p, sysdate);	
		
	end if;
	
	if	(ie_valor_w in('B','V')) then
		
		pat_gerar_baixa_doc_bem(nr_sequencia_p, nm_usuario_p,dt_referencia_p);	
		
	end if;
	
end if;

if	(ie_gerar_envio_comm_doc_w = 'S') then
	PAT_GERAR_ENVIO_COMUNIC_DOC(nr_sequencia_p,nm_usuario_p, 2);
end if;

commit;
end pat_aprovar_doc_transf_html5;
/