Create or Replace
PROCEDURE Autorizacao_laudo (	nr_sequencia_p		Number,
				ie_tipo_p		Number,
				ie_forma_aprovacao_p	varchar2,
				qt_caracteres_p		Number,
				nm_usuario_p		varchar2) IS

ie_nao_liberado_w	Number(1);
nm_medico_w		varchar2(60);
ie_desfazer_aprov_w	varchar2(01)	:= 'S';
ie_limpar_integracao_w	varchar2(01)	:= 'N';
cd_medico_w		varchar2(10);
cd_setor_atendimento_w	number(5);
qt_regra_setor_aprov_w	number(10);
ie_desfazer_seg_aprov_w varchar2(10);
ie_exige_seg_aprov_w	varchar2(1);
dt_aprovacao_w  	date;
dt_seg_aprovacao_w 	date;
nr_seq_classif_w	number(10);
qt_regra_w		number(10);
ie_aborta_w		varchar2(1);
ie_int_centricity_w	varchar2(1);
nr_atendimento_w	number(10);

Cursor C01 is
	select  b.nr_seq_classif
	from	proc_interno b,		
		procedimento_paciente a		 
	where	a.nr_seq_proc_interno = b.nr_sequencia	
	and	a.nr_laudo = nr_sequencia_p
	and	exists( select 	1 
			from 	regra_proc_classif_laudo c
			where	b.nr_seq_classif = c.nr_seq_classif );

BEGIN

SELECT	NVL(MAX(Obter_Valor_Param_Usuario(28,74,obter_perfil_ativo,nm_usuario_p,0)) , 'S')
into	ie_desfazer_aprov_w
FROM	dual;

SELECT	NVL(MAX(Obter_Valor_Param_Usuario(28,145,obter_perfil_ativo,nm_usuario_p,0)) , 'S')
into	ie_limpar_integracao_w
FROM	dual;

select	count(*)
into	ie_nao_liberado_w
from 	laudo_paciente
where	nr_sequencia	= nr_sequencia_p
and	dt_liberacao is null;	

select	nvl(max(ie_exige_seg_aprov), 'N'),
	max(nr_atendimento)
into	ie_exige_seg_aprov_w,
	nr_atendimento_w
from	laudo_paciente
where	nr_sequencia = nr_sequencia_p;

select 	nvl(max(dt_aprovacao), null),
	nvl(max(dt_seg_aprovacao), null)	
into	dt_aprovacao_w,
        dt_seg_aprovacao_w	
from 	laudo_paciente
where	dt_aprovacao is not null
and	dt_seg_aprovacao is not null
and	nr_sequencia 	= nr_sequencia_p;

if	(ie_tipo_p = 1) then
	begin
	if	(ie_nao_liberado_w > 0) and
		((ie_forma_aprovacao_p = 'S') or 
		 (ie_forma_aprovacao_p = 'D')) then
		update	laudo_paciente
		set	dt_seg_aprovacao	= sysdate,
			dt_liberacao		= sysdate,
			qt_caracteres		= qt_caracteres_p,
			nm_usuario_seg_aprov	= nm_usuario_p,
			nm_usuario		= nm_usuario_p,
			nm_usuario_liberacao	= nm_usuario_p
		where	nr_sequencia		= nr_sequencia_p;
		gravar_log_status_laudo(nr_sequencia_p,3,nm_usuario_p);
		envia_sms_email_laudo(nr_atendimento_w,null,nm_usuario_p);
	else
		update	laudo_paciente
		set	dt_seg_aprovacao	= sysdate,
			qt_caracteres		= qt_caracteres_p,
			nm_usuario_seg_aprov	= nm_usuario_p,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_sequencia_p;
		gravar_log_status_laudo(nr_sequencia_p,3,nm_usuario_p);
	end if;
	end;
elsif	(ie_tipo_p = 0) then
	begin
	
	
	
	select	count(*)
	into	qt_regra_w
	from	regra_proc_classif_laudo;
	
	if (qt_regra_w > 0) then
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_classif_w;
		exit when C01%notfound;
			begin
			
			select  decode(count(*),0,'S','N')
			into	ie_aborta_w
			from	regra_proc_classif_laudo
			where	nr_seq_classif = nr_seq_classif_w
			and	cd_medico = ( select 	max(cd_pessoa_fisica)
					      from	usuario 
					      where	nm_usuario = nm_usuario_p);
						
			
			if (ie_aborta_w = 'S') then
				wheb_mensagem_pck.exibir_mensagem_abort(230526);				
			end if;
			
			end;
		end loop;
		close C01;	
		
	end if;
	
	if	(ie_nao_liberado_w > 0) and
		(ie_forma_aprovacao_p = 'A') or 
		((ie_forma_aprovacao_p = 'D') and 
		 (ie_exige_seg_aprov_w = 'N')) then
		update	laudo_paciente
		set	dt_aprovacao		= sysdate,
			dt_liberacao		= sysdate,
			nm_usuario_liberacao	= nm_usuario_p,
			qt_caracteres		= qt_caracteres_p,
			nm_usuario_aprovacao	= nm_usuario_p,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_sequencia_p;
		gravar_log_status_laudo(nr_sequencia_p,2,nm_usuario_p);
		envia_sms_email_laudo(nr_atendimento_w,null,nm_usuario_p);
	else
	
		select	count(*)
		into	qt_regra_setor_aprov_w
		from	regra_laudo_seg_aprov;
		
		if (qt_regra_setor_aprov_w > 0) then
			
			select	max(a.cd_setor_atendimento)
			into	cd_setor_atendimento_w
			from	prescr_procedimento a, laudo_paciente b
			where	a.nr_prescricao = b.nr_prescricao
			and	a.nr_sequencia = b.nr_seq_prescricao
			and	b.nr_sequencia = nr_sequencia_p;
		
			select	count(*)
			into	qt_regra_setor_aprov_w
			from	regra_laudo_seg_aprov
			where	cd_setor_atendimento = cd_setor_atendimento_w;
			
			if (qt_regra_setor_aprov_w > 0) then
			
				update	laudo_paciente
				set	dt_aprovacao		= sysdate,
					qt_caracteres		= qt_caracteres_p,
					nm_usuario_aprovacao	= nm_usuario_p,
					nm_usuario		= nm_usuario_p,
					dt_seg_aprovacao	= sysdate,
					nm_usuario_seg_aprov	= nm_usuario_p,
					dt_liberacao		= sysdate,
					nm_usuario_liberacao	= nm_usuario_p
				where	nr_sequencia		= nr_sequencia_p;
				gravar_log_status_laudo(nr_sequencia_p,2,nm_usuario_p);	
				envia_sms_email_laudo(nr_atendimento_w,null,nm_usuario_p);
			else
			
				update	laudo_paciente
				set	dt_aprovacao		= sysdate,
					qt_caracteres		= qt_caracteres_p,
					nm_usuario_aprovacao	= nm_usuario_p,
					nm_usuario		= nm_usuario_p
				where	nr_sequencia		= nr_sequencia_p;
				gravar_log_status_laudo(nr_sequencia_p,2,nm_usuario_p);
				
			end if;
		else
		
			update	laudo_paciente
			set	dt_aprovacao		= sysdate,
				qt_caracteres		= qt_caracteres_p,
				nm_usuario_aprovacao	= nm_usuario_p,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_sequencia_p;
			gravar_log_status_laudo(nr_sequencia_p,2,nm_usuario_p);
		end if;
	end if;
	end;
else
	begin
	
		if	(ie_desfazer_aprov_w = 'N') then

			select	nvl(max(cd_medico),'0')
			into	cd_medico_w
			from	med_laudo_cdi
			where	nr_seq_laudo = nr_sequencia_p;

			if 	(cd_medico_w <> '0')	then

				select	nvl(max(substr(obter_nome_medico(cd_medico,'N'),1,60)), '0')
				into	nm_medico_w
				from	med_laudo_cdi
				where	nr_seq_laudo = nr_sequencia_p;
				
				--O m�dico #@NM_MEDICO#@ j� inseriu uma observa��o referente a este laudo! Favor entrar em contato com o m�dico!				
				Wheb_mensagem_pck.exibir_mensagem_abort(213533, 'NM_MEDICO='|| nm_medico_w); 
			end if;
		end if;

		obter_param_usuario(28,267,Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_desfazer_seg_aprov_w);
		
		if 	(((ie_forma_aprovacao_p = 'S') or 
			  (ie_forma_aprovacao_p = 'D')) and
			 (ie_desfazer_seg_aprov_w = 'S') and
			 (dt_aprovacao_w  is not null) and
			 (dt_seg_aprovacao_w is not null)) then	
			 
			update	laudo_paciente
			set	dt_seg_aprovacao	= null,
				nm_usuario_seg_aprov	= null,
				dt_liberacao 		= null,
				nm_usuario_liberacao	= null
			where	nr_sequencia		= nr_sequencia_p;
			gravar_log_status_laudo(nr_sequencia_p,7,nm_usuario_p);
		else	
			update	laudo_paciente
			set	dt_aprovacao		= null,
				dt_integracao		= decode(ie_limpar_integracao_w,'S',null,dt_integracao),
				dt_liberacao		= null,
				nm_usuario_liberacao	= null,
				dt_envelopado		= null,
				nm_usuario_aprovacao	= null,
				dt_seg_aprovacao	= null,
				nm_usuario_seg_aprov	= null,
				dt_desaprovacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_sequencia		= nr_sequencia_p;
			gravar_log_status_laudo(nr_sequencia_p,5,nm_usuario_p);
		end if;
	end;
end if;


select	decode(nvl(max(ie_situacao),'I'),'I','N','S')
into	ie_int_centricity_w
from	cliente_integracao
where	nr_seq_inf_integracao = 585;

if (ie_int_centricity_w = 'S') then

	select 	dt_liberacao
	into	dt_aprovacao_w
	from 	laudo_paciente
	where	nr_sequencia = nr_sequencia_p;

	if (dt_aprovacao_w is not null) then
		quebra_linha_hl7(nr_sequencia_p, nm_usuario_p); 
	end if;	
end if;

commit;

END Autorizacao_laudo;
/
