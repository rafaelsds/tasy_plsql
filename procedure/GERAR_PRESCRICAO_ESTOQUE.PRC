create or replace 
procedure Gerar_Prescricao_Estoque(
			cd_estabelecimento_p	number,
			nr_atendimento_p		number,
			dt_entrada_Unidade_p	date,
			cd_material_p		number,
			dt_atendimento_p		date,
			cd_acao_p		varchar2,
			cd_local_estoque_p	number,
			qt_material_p 		number,
			cd_setor_atendimento_p	number,
			cd_unidade_medida_p	varchar2,
			nm_usuario_p		varchar2,
			ie_inserir_excluir_p		varchar2,
			nr_prescricao_p		number,
			nr_seq_prescricao_p	number,
			nr_seq_proc_princ_p	number,
			nr_sequencia_p		number,
			cd_cgc_fornecedor_p	varchar2,
			ds_observacao_p		varchar2,
			nr_seq_lote_fornec_p	number,
			nr_receita_p		number,
			nr_serie_material_p		varchar2,
			nr_lote_ap_p		number,
			ie_vago1_p		varchar2,
			ie_vago2_p		varchar2,
			ie_vago3_p		varchar2) is


/*ie_vago1_p
Ja esta sendo utilizado para tratamentos diferenciando o tipo da operacao:
	S	= Tratamento especial para baixa no PEPO - Itens contaminados	=> Busca a operacao de Perda de consumo
	X	= Tratamento para nova funcao - Producao de medicamentos	=> Busca a operacao de Consumo do setor

*/

qt_existe_w				number(10);
nr_seq_aprovacao_w			number(10);
dt_atualizacao_w			date	:= sysdate;
cd_estabelecimento_w			number(4,0);
nr_movimento_estoque_w			number(10,0);
cd_operacao_cons_Paciente_w 		number(4,0);
cd_operacao_Transf_Paciente_w		number(4,0);
cd_operacao_Cons_Consignado_w		number(4,0);
cd_operacao_Devol_Paciente_w		number(4,0);
cd_operacao_dev_consignado_w		number(4,0);
cd_oper_perda_cons_w			number(4,0);
cd_oper_perda_cons_con_w		number(4,0);
cd_oper_cons_setor_w			number(4,0);
cd_acao_w	  			varchar2(1);
qt_minimo_multiplo_solic_w 		number(15,4);
qt_material_w				number(15,4);
qt_material_ww				number(15,4);
cd_centro_custo_w			number(08,0);
ie_consignado_Mat_w			varchar2(01);
ie_gera_ordem_w				varchar2(01);
nr_ordem_compra_w			number(10,0);
nr_ordem_compra_gerada_w		number(10,0);
nr_item_oci_gerado_w			number(10,0);
nm_usuario_ww				varchar2(255)	:= 'Marcus';
nr_sequencia_w				number(10,0);
nr_seq_classif_w			number(10,0);
nr_item_oci_w				number(10,0);
cd_local_entrega_w			number(15,0);
cd_centro_custo_ww			number(08,0);
pr_desconto_w				number(15,4);
nm_usuario_dest_w			varchar2(255);
ds_material_w				varchar2(255);
nm_pessoa_fisica_w			varchar2(100);
ds_acao_w				varchar2(30);
ie_situacao_local_w			varchar2(01);
ds_observacao_movto_w			varchar2(500);
nr_seq_motivo_cancel_oc_w		number(10);
ie_cancelar_ordem_consig_w		varchar2(1);
qt_conv_compra_estoque_w		number(13,4)	:= 0;
qt_conv_estoque_consumo_w		number(13,4)	:= 0;
qt_item_oc_w				number(15)	:= 0;
ds_lote_fornec_w			varchar2(20);
dt_validade_w				date;
cd_conta_contabil_w			varchar2(20)	:= '';
ie_tipo_conta_w				number(01);
cd_pessoa_solicitante_w			varchar2(10);
ds_convenio_w				varchar2(255);
cd_convenio_w				number(10);
nm_fornecedor_w				varchar2(255);
nm_medico_prescr_w			varchar2(255);
ie_forma_operacao_estorno_w		varchar2(1);
nr_cot_compra_w				number(10);
ie_solicitante_oc_consig_w		varchar2(1);
ie_consiste_oc_aprov_w			varchar2(1);
ie_exclui_item_oc_w			varchar2(1);
dt_aprovacao_w				date;
qt_material_oc_w			number(13,4);
cd_perfil_comunic_w			varchar2(10);
ie_consiste_lote_fornec_w		varchar2(1);
ds_lote_w				varchar2(25);
nr_seq_lote_fornec_w			number(10);
ie_devolucao_w				varchar2(1) := 'N';
ie_passagem_direta_cc_w			varchar2(1) := 'N';	
ie_passagem_direta_cc_prescr_w		varchar2(1) := 'N';
ds_comunicacao_w			varchar2(2000);
lista_usuario_w				varchar2(2000);
tam_lista_w				number(10);
ie_pos_virgula_w			number(10);
ds_email_usuario_w			varchar2(255);
ds_lista_email_usuario_w		varchar2(2000);
nm_usuario_w				varchar2(2000);
ie_gera_oc_reposicao_w			varchar2(1);
ie_tipo_ordem_w				ordem_compra.ie_tipo_ordem%type;
qt_compra_w				ordem_compra_item.qt_material%type;
cd_unidade_medida_compra_w		unidade_medida.cd_unidade_medida%type;
cd_comprador_padrao_w			pessoa_fisica.cd_pessoa_fisica%type;
cd_comprador_consig_w			pessoa_fisica.cd_pessoa_fisica%type;
ie_faturamento_direto_w			varchar2(01);
ie_regra_local_usuario_w		Varchar2(1);
ie_regra_w						Varchar2(1);
cd_oper_baixa_producao_w		parametro_estoque.cd_oper_baixa_producao%type;
cd_oper_baixa_prod_consig_w		parametro_estoque.cd_oper_baixa_prod_consig%type;
cd_oper_producao_w              parametro_estoque.cd_oper_producao%type;
ie_evento_w number(10);

cursor c01 is
	select	nvl(obter_ordem_atend_consignado(cd_estabelecimento_w, cd_cgc_fornecedor_p, cd_local_estoque_p, 0, nr_atendimento_p, nvl(nr_prescricao_p,0),'G'),0), /*Ordem de consumo de consignado*/
		'G'
	from	dual
	union all
	select	nvl(obter_ordem_atend_consignado(cd_estabelecimento_w, cd_cgc_fornecedor_p, cd_local_estoque_p, 0, nr_atendimento_p, nvl(nr_prescricao_p,0),'W'),0), /* Ordem de reposicao de consignado*/
		'W'
	from	dual;


begin
if	cd_estabelecimento_p is null then
	begin
	select	cd_estabelecimento
  	into	cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	end;
else
	begin
	cd_estabelecimento_w	:= cd_estabelecimento_p;
	end;
end if;

ie_faturamento_direto_w := substr(nvl(obter_se_faturamento_direto(nvl(nr_seq_prescricao_p,0), nvl(nr_prescricao_p,0)),'N'),1,1);
/*
Se faturamento direto = S, nao gera ordem de compra
OS 1585567
*/

obter_param_usuario(24,269,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_consiste_oc_aprov_w);
obter_param_usuario(24,270,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_exclui_item_oc_w);

if	(obter_funcao_ativa = 24) then
	begin
	obter_param_usuario(24, 328, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_passagem_direta_cc_w);
	end;
elsif	(obter_funcao_ativa = 36) then	
	begin
	obter_param_usuario(36, 249, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_passagem_direta_cc_prescr_w);
	end;
end if;

select	nvl(max(cd_convenio),0),
	substr(nvl(max(obter_nome_convenio(cd_convenio)),WHEB_MENSAGEM_PCK.get_texto(301935)),1,40),
	substr(max(obter_nome_pf_pj(null, cd_cgc_fornecedor_p)),1,255)
into	cd_convenio_w,
	ds_convenio_w,
	nm_fornecedor_w
from	atendimento_paciente_v
where	nr_atendimento = nr_atendimento_p;

select	nvl(max(ie_situacao),'A')
into	ie_situacao_local_w
from	local_estoque
where	cd_local_estoque	= cd_local_estoque_p;
if	(ie_situacao_local_w = 'I') then
     	/*(-20011,'O local de estoque esta inativo');*/
	WHEB_MENSAGEM_PCK.exibir_mensagem_abort(196048);
end if;

ie_evento_w := 3;
if	(obter_funcao_ativa in (900, 24)) then	
	select	nvl(max(ie_regra_local_usuario),'N')
	into	ie_regra_local_usuario_w 
	from	parametro_estoque 
	where	cd_estabelecimento = nvl(cd_estabelecimento_p,cd_estabelecimento_w);

	if	(ie_regra_local_usuario_w  = 'S') then
    if (obter_funcao_ativa = 24) then
      ie_evento_w := 4;
    end if;
		select	obter_regra_local_usuario(nvl(cd_estabelecimento_p,cd_estabelecimento_w), cd_local_estoque_p, ie_evento_w, nm_usuario_p)
		into	ie_regra_w 	
		from	dual;

		if	(ie_regra_w = 'N') then
			WHEB_MENSAGEM_PCK.exibir_mensagem_abort(180907);
		end if;
	end if;
end if;

if	(ie_inserir_excluir_p = 'I') then
	cd_acao_w 	:= cd_acao_p;
else
	if	(cd_acao_p = '1') then
		cd_acao_w 	:= '2';
	else
		cd_acao_w	:= '1';
	end if;
end if;

qt_material_w		:= qt_material_p;
qt_material_w		:= abs(qt_material_w);

begin
select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,30) cd_unidade_medida_compra,
	qt_minimo_multiplo_solic,
	ie_consignado,
	ds_material,
	qt_conv_compra_estoque,
	qt_conv_estoque_consumo
into	cd_unidade_medida_compra_w,
	qt_minimo_multiplo_solic_w,
	ie_consignado_Mat_w,
	ds_material_w,
	qt_conv_compra_estoque_w,
	qt_conv_estoque_consumo_w
from	material
where	cd_material = cd_material_p;
exception
	when others then
     	/*(-20011,'Falta indicador baixa e/ou min. mult. solic do material');*/
	WHEB_MENSAGEM_PCK.exibir_mensagem_abort(196049);
end;

begin
select	cd_operacao_transf_paciente,
	cd_operacao_cons_paciente,
	cd_operacao_devol_paciente,
	nvl(cd_operacao_cons_consignado, cd_operacao_cons_paciente),
	cd_operacao_dev_consignado,
	nvl(cd_oper_perda_cons, cd_operacao_cons_paciente),
	nvl(cd_oper_perda_cons_con, cd_operacao_cons_consignado), --Tenho a operacao de consigando... mas ainda nao existe tratamento para este caso
	cd_operacao_consumo_setor,
	cd_oper_baixa_producao,
	cd_oper_baixa_prod_consig,
	ie_forma_operacao_estorno,
    cd_oper_producao
into	cd_operacao_transf_paciente_w,
	cd_operacao_cons_paciente_w,
	cd_operacao_devol_paciente_w,
	cd_operacao_cons_consignado_w,
	cd_operacao_dev_consignado_w,
	cd_oper_perda_cons_w,
	cd_oper_perda_cons_con_w,
	cd_oper_cons_setor_w,
	cd_oper_baixa_producao_w,
	cd_oper_baixa_prod_consig_w,
	ie_forma_operacao_estorno_w,
    cd_oper_producao_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_w;
exception
	when others then
     	/*(-20011,'Falta informar as Operacoes Transf/Consumo ou Devolucao nos Parametros de estoque');*/
	WHEB_MENSAGEM_PCK.exibir_mensagem_abort(196050);
end;


select	decode(cd_acao_p,1,WHEB_MENSAGEM_PCK.get_texto(301936),WHEB_MENSAGEM_PCK.get_texto(301937))
into	ds_acao_w
from	dual;

if	(ie_consignado_Mat_w = 1) or
	((nvl(cd_cgc_fornecedor_p, 'X') <> 'X') and (ie_consignado_mat_w = 2)) then
	cd_operacao_cons_paciente_w	:= cd_operacao_cons_consignado_w;
end if;

if	(cd_acao_w = '2') then
	ie_devolucao_w := 'S';
end if;

/*Fabio OS150544 - tratamento para gerar o movimento com operacao devolucao e acao inclusao*/
if	(ie_forma_operacao_estorno_w = 'D') and
	(cd_acao_w = '2') then
	cd_operacao_cons_paciente_w	:= cd_operacao_devol_paciente_w;
	cd_acao_w			:= '1';
	if	(ie_consignado_Mat_w = 1) or
		((nvl(cd_cgc_fornecedor_p, 'X') <> 'X') and (ie_consignado_mat_w = 2)) then
		cd_operacao_cons_paciente_w	:= cd_operacao_dev_consignado_w;
	end if;
end if;


/*Tratamento especial as rotinas com operacao diferenciada*/
if	(nvl(ie_vago1_p, 'N') = 'S') then
	cd_operacao_cons_paciente_w	:= cd_oper_perda_cons_w;
elsif	(nvl(ie_vago1_p, 'N') = 'X') then
	if	(ie_consignado_Mat_w = 1) or
		((nvl(cd_cgc_fornecedor_p, 'X') <> 'X') and (ie_consignado_mat_w = 2)) then
		cd_operacao_cons_paciente_w := cd_oper_baixa_prod_consig_w;
	else
		cd_operacao_cons_paciente_w := cd_oper_baixa_producao_w;
	end if;
elsif (nvl(ie_vago1_p, 'N') = 'P') then
    cd_operacao_cons_paciente_w := cd_oper_producao_w;
end if;


if	(nr_seq_proc_princ_p is not null) then
	begin
	select	c.cd_centro_custo
	into	cd_centro_custo_w
	from	equipamento c,
		procedimento_paciente a
	where	a.cd_equipamento = c.cd_equipamento
	and	a.nr_sequencia = nr_seq_proc_princ_p;
	exception
		when others then
			cd_centro_custo_w	:= null;
	end;
end if;

/* Tratar consumo de consignados  */

select	nvl(max(nr_seq_motivo_cancel_ordem),0),
	nvl(max(ie_cancelar_ordem_consig),'N'),
	nvl(max(ie_solicitante_oc_consig), 'R'),
	max(cd_comprador_consig), 
	max(cd_comprador_padrao)
into	nr_seq_motivo_cancel_oc_w,
	ie_cancelar_ordem_consig_w,
	ie_solicitante_oc_consig_w,
	cd_comprador_consig_w,
	cd_comprador_padrao_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_w;

if	(nvl(nr_seq_lote_fornec_p,0) > 0) then
	select	ds_lote_fornec,
		dt_validade
	into	ds_lote_fornec_w,
		dt_validade_w
	from	material_lote_fornec
	where	nr_sequencia = nr_seq_lote_fornec_p;
end if;

if	(wheb_usuario_pck.get_cd_funcao = 900) then
	obter_param_usuario(900, 474, obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w, ie_consiste_lote_fornec_w);

	if	(nvl(ie_consiste_lote_fornec_w,'N') = 'S') then 
		begin
		select 	ds_lote,
			nr_seq_lote_fornec
		into	ds_lote_w,
			nr_seq_lote_fornec_w
		from 	prescr_material 
		where 	nr_prescricao = nr_prescricao_p 
		and 	nr_sequencia = nr_seq_prescricao_p;

		obter_regra_ordem_consignado
				(cd_estabelecimento_w,
				cd_local_estoque_p,
				cd_operacao_cons_paciente_w,
				cd_material_p,
				cd_cgc_fornecedor_p,
				cd_convenio_w,
				cd_setor_atendimento_p,
				ie_gera_ordem_w,
				cd_local_entrega_w,
				cd_centro_custo_ww,
				pr_desconto_w,
				nm_usuario_dest_w,
				cd_perfil_comunic_w,
				cd_pessoa_solicitante_w,
				ie_gera_oc_reposicao_w);
		exception
		when others then
				ie_gera_ordem_w := 'N';
		end;

		if	((nvl(ie_gera_ordem_w,'N') = 'S') or 
			(nvl(ie_gera_ordem_w,'N') = 'A') or
			(nvl(ie_gera_ordem_w,'N') = 'O')) and
			(nr_seq_lote_fornec_w is null) and
			(ds_lote_w is null) then 
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(212888, 'CD_MATERIAL_W='||to_char(cd_material_p));
		end if;
	end if;
end if;


if	(ie_consignado_Mat_w = 1) or ((cd_cgc_fornecedor_p is not null) and (ie_consignado_Mat_w = 2)) then			
	if	(cd_cgc_fornecedor_p is not null) then
		if	(cd_acao_p = '1') and (ie_inserir_excluir_p   = 'I') then
			begin

			obter_regra_ordem_consignado
				(cd_estabelecimento_w,
				cd_local_estoque_p,
				cd_operacao_cons_paciente_w,
				cd_material_p,
				cd_cgc_fornecedor_p,
				cd_convenio_w,
				cd_setor_atendimento_p,
				ie_gera_ordem_w,
				cd_local_entrega_w,
				cd_centro_custo_ww,
				pr_desconto_w,
				nm_usuario_dest_w,
				cd_perfil_comunic_w,
				cd_pessoa_solicitante_w,
				ie_gera_oc_reposicao_w);

				if	(ie_solicitante_oc_consig_w = 'C') then /*Solicitante do consumo*/
					cd_pessoa_solicitante_w	:= obter_pessoa_fisica_usuario(nm_usuario_p, 'C');
				end if;

				select	substr(obter_pessoa_atendimento(nr_atendimento_p,'N'),1,100)
				into	nm_pessoa_fisica_w
				from	dual;

				if	(nvl(nr_prescricao_p,0) > 0) then
					select	substr(obter_nome_pf(cd_medico),1,255)
					into	nm_medico_prescr_w
					from	prescr_medica
					where	nr_prescricao = nr_prescricao_p;
				end if;

			ds_comunicacao_w :=	WHEB_MENSAGEM_PCK.get_texto(301938,'CD_MATERIAL_P='||cd_material_p||
									';DS_MATERIAL_W='||ds_material_w||
									';QT_MATERIAL_P='||campo_mascara_virgula(qt_material_p)||
									';NR_PRESCRICAO_P='||nr_prescricao_p||
									';DS_ACAO_W='||ds_acao_w||
									';NR_ATENDIMENTO_P='||nr_atendimento_p||
									';NM_PESSOA_FISICA_W='||nm_pessoa_fisica_w||
									';DS_CONVENIO_W='||ds_convenio_w||
									';NM_FORNECEDOR_W='||nm_fornecedor_w||
									';DS_LOTE_FORNEC_W='||ds_lote_fornec_w||
									';NM_MEDICO_PRESCR_W='||nm_medico_prescr_w||
									';CD_SERIE_W='||obter_dados_ultima_compra(cd_estabelecimento_w, cd_material_p, 'SI'));
			if	(ie_gera_ordem_w = 'N') and 
				(ie_gera_oc_reposicao_w = 'S') and
				(ie_faturamento_direto_w = 'N')	then	/*Quando somente e para gerar a ordem de resposicao, senao a ordem de resposicao e feita dentro da procedure GERAR_ORDEM_COMPRA_CONSIG*/

				qt_compra_w	:= (qt_material_w / qt_conv_compra_estoque_w / qt_conv_estoque_consumo_w);

				gerar_ordem_reposicao_consig(
					cd_estabelecimento_w,
					cd_cgc_fornecedor_p,
					cd_material_p,
					qt_material_w,
					dt_atendimento_p,
					nm_usuario_p,
					nr_prescricao_p,
					nr_seq_prescricao_p,
					nr_atendimento_p,
					cd_local_estoque_p,
					ds_observacao_p,
					cd_local_estoque_p,
					cd_centro_custo_ww,
					pr_desconto_w,
					nr_serie_material_p,
					cd_pessoa_solicitante_w,
					nr_seq_lote_fornec_p,
					cd_setor_atendimento_p,
					nr_sequencia_p,
					qt_compra_w,
					cd_unidade_medida_compra_w,
					nvl(cd_comprador_consig_w,cd_comprador_padrao_w));

			elsif	(ie_gera_ordem_w in ('S','A','O') and 
				(ie_faturamento_direto_w = 'N')) then	/*S = Gera a ordem, A = Gera a ordem + comunicacao interna, O = Ordem + E-mail*/

				gerar_ordem_compra_consig(
					cd_estabelecimento_w,
					cd_cgc_fornecedor_p,
					cd_material_p,
					qt_material_w,
					dt_atendimento_p,
					nm_usuario_p,
					nr_prescricao_p,
					nr_seq_prescricao_p,
					nr_atendimento_p,
					cd_local_estoque_p,
					ds_observacao_p,
					cd_local_entrega_w,
					cd_centro_custo_ww,
					pr_desconto_w,
					nr_serie_material_p,
					cd_pessoa_solicitante_w,
					nr_seq_lote_fornec_p,
					cd_setor_atendimento_p,
					ie_gera_oc_reposicao_w,
					nr_sequencia_p,
					nr_ordem_compra_gerada_w,
					nr_item_oci_gerado_w);

				if	(ie_gera_ordem_w = 'A') then	/* Para enviar a comunicacao interna quando a opcao 'A' - Ordem+Comunicacao*/
					Gerar_comunic_consignado(nm_usuario_p,nm_usuario_dest_w,ds_comunicacao_w,cd_perfil_comunic_w);
				end if;

				if	(ie_gera_ordem_w = 'O') then	/* Para enviar e-mail quando a opcao 'O' = Ordem + E-mail*/
					lista_usuario_w	:= substr(nm_usuario_dest_w,1,2000);

					while	(lista_usuario_w is not null) and
						(trim(lista_usuario_w) <> ',') loop
						tam_lista_w		:= length(lista_usuario_w);
						ie_pos_virgula_w	:= instr(lista_usuario_w,',');
						if	(ie_pos_virgula_w <> 0) then
							nm_usuario_w	:= substr(lista_usuario_w,1,(ie_pos_virgula_w - 1));
							lista_usuario_w	:= substr(lista_usuario_w,(ie_pos_virgula_w + 1), tam_lista_w);

							select	trim(max(ds_email))
							into	ds_email_usuario_w
							from	usuario
							where	nm_usuario = nm_usuario_w;

							if	(nvl(ds_email_usuario_w,'X') <> 'X') then
								begin
								ds_lista_email_usuario_w	:= substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
								end;
							end if;
						elsif	(tam_lista_w > 1) then
							nm_usuario_w	:= lista_usuario_w;
							lista_usuario_w	:= null;

							select	trim(max(ds_email))
							into	ds_email_usuario_w
							from	usuario
							where	nm_usuario = nm_usuario_w;

							if	(nvl(ds_email_usuario_w,'X') <> 'X') then
								begin
								ds_lista_email_usuario_w	:= substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
								end;
							end if;
						else
							lista_usuario_w	:= null;
						end if;
					end loop;

					if	(nvl(ds_lista_email_usuario_w,'X') <> 'X') then
						begin
						Enviar_Email(WHEB_MENSAGEM_PCK.get_texto(301943),ds_comunicacao_w, null, ds_lista_email_usuario_w, nm_usuario_p,'M');
						exception
						when others then
							null;
						end;
					end if;
				end if;

			elsif	(ie_gera_ordem_w = 'C') and	/*C = Comunicacao, A = Gera ordem e envia comunicacao*/
				(cd_local_estoque_p is not null) then
				Gerar_comunic_consignado(nm_usuario_p,nm_usuario_dest_w,
					WHEB_MENSAGEM_PCK.get_texto(301944,'CD_MATERIAL_P='||cd_material_p||
							';DS_MATERIAL_W='||ds_material_w||
							';QT_MATERIAL_P='||campo_mascara_virgula(qt_material_p)||
							';NR_PRESCRICAO_P='||nr_prescricao_p||
							';DS_ACAO_W='||ds_acao_w||
							';NR_ATENDIMENTO_P='||nr_atendimento_p||
							';NM_PESSOA_FISICA_W='||nm_pessoa_fisica_w||
							';CD_SERIE_W='||obter_dados_ultima_compra(cd_estabelecimento_w, cd_material_p, 'SI')),
					cd_perfil_comunic_w);
			elsif	(ie_gera_ordem_w = 'E') and	/*'E' = E-mail*/
				(cd_local_estoque_p is not null) then
				lista_usuario_w	:= substr(nm_usuario_dest_w,1,2000);

				while	(lista_usuario_w is not null) and
					(trim(lista_usuario_w) <> ',') loop
					tam_lista_w		:= length(lista_usuario_w);
					ie_pos_virgula_w	:= instr(lista_usuario_w,',');
					if	(ie_pos_virgula_w <> 0) then
						nm_usuario_w	:= substr(lista_usuario_w,1,(ie_pos_virgula_w - 1));
						lista_usuario_w	:= substr(lista_usuario_w,(ie_pos_virgula_w + 1), tam_lista_w);

						select	trim(max(ds_email))
						into	ds_email_usuario_w
						from	usuario
						where	nm_usuario = nm_usuario_w;

						if	(nvl(ds_email_usuario_w,'X') <> 'X') then
							begin
							ds_lista_email_usuario_w	:= substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
							end;
						end if;
					elsif	(tam_lista_w > 1) then
						nm_usuario_w	:= lista_usuario_w;
						lista_usuario_w	:= null;

						select	trim(max(ds_email))
						into	ds_email_usuario_w
						from	usuario
						where	nm_usuario = nm_usuario_w;

						if	(nvl(ds_email_usuario_w,'X') <> 'X') then
							begin
							ds_lista_email_usuario_w	:= substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
							end;
						end if;
					else
						lista_usuario_w	:= null;
					end if;
				end loop;

				if	(nvl(ds_lista_email_usuario_w,'X') <> 'X') then
					begin
					Enviar_Email(WHEB_MENSAGEM_PCK.get_texto(301943),ds_comunicacao_w, null, ds_lista_email_usuario_w, nm_usuario_p,'M');
					exception
					when others then
						null;
					end;
				end if;
			end if;
			end;
		else

			qt_material_ww	:= (qt_material_w / qt_conv_compra_estoque_w / qt_conv_estoque_consumo_w);

			open C01;
			loop
			fetch C01 into	
				nr_ordem_compra_w,
				ie_tipo_ordem_w;
			exit when C01%notfound;
				begin

				if	(nr_ordem_compra_w > 0) then

					select	nvl(min(b.nr_item_oci),0)
					into	nr_item_oci_w
					from	ordem_compra_item b
					where	b.nr_ordem_compra = nr_ordem_compra_w
					and	b.cd_material in (
						select	x.cd_material
						from	material x
						where	x.cd_material_estoque = cd_material_p
						union
						select	x.cd_material
						from	material x
						where	x.cd_material	= cd_material_p)
					and	b.qt_material >= qt_material_ww;
				end if;

				if	(nr_ordem_compra_w = 0) and
					(ie_tipo_ordem_w = 'G') and
					(ie_consiste_oc_aprov_w = 'S') then
					WHEB_MENSAGEM_PCK.exibir_mensagem_abort(196052,'CD_MATERIAL_P='||CD_MATERIAL_P);
					/*(-20011,	'Nao e possivel devolver o item  ' || cd_material_p || ', pois nao possui nenhuma ' ||
									'ordem de compra relacionada a este atendimento que  nao esteja aprovada. ' ||
									'Verifique o parametro [269]. Para realizar essa acao, deve ser estornado a aprovacao ' ||
									'de alguma ordem  relacionada a este atendimento');*/
				end if;

				if	(nr_ordem_compra_w > 0) and
					(nr_item_oci_w > 0) then

					if	(ie_consiste_oc_aprov_w = 'S') and
						(ie_tipo_ordem_w = 'G') and
						(qt_material_ww > 0) then

						select	dt_aprovacao
						into	dt_aprovacao_w
						from	ordem_compra
						where	nr_ordem_compra = nr_ordem_compra_w;

						if	(dt_aprovacao_w is not null) then
							WHEB_MENSAGEM_PCK.exibir_mensagem_abort(196052,'CD_MATERIAL_P='||CD_MATERIAL_P);
							/*(-20011,	'Nao e possivel devolver o item  ' || cd_material_p || ', pois nao possui nenhuma ' ||
									'ordem de compra relacionada a este atendimento que  nao esteja aprovada. ' ||
									'Verifique o parametro [269]. Para realizar essa acao, deve ser estornado a aprovacao ' ||
									'de alguma ordem  relacionada a este atendimento');*/
						end if;
					end if;

					select	qt_material
					into	qt_material_oc_w
					from	ordem_compra_item
					where	nr_ordem_compra	= nr_ordem_compra_w
					and	nr_item_oci	= nr_item_oci_w;

					if	((qt_material_oc_w - qt_material_w) = 0) and (nvl(ie_exclui_item_oc_w,'N') = 'S') then
						begin

						select	nvl(max(nr_seq_aprovacao),0)
						into	nr_seq_aprovacao_w
						from	ordem_compra_item
						where	nr_ordem_compra = nr_ordem_compra_w
						and	nr_item_oci = nr_item_oci_w;

						delete	ordem_compra_item
						where	nr_ordem_compra = nr_ordem_compra_w
						and	nr_item_oci = nr_item_oci_w;

						select	count(*)
						into	qt_existe_w
						from	ordem_compra_item
						where	nr_seq_aprovacao = nr_seq_aprovacao_w;

						if	(qt_existe_w = 0 ) then						
							delete from processo_aprov_compra
							where	nr_sequencia = nr_seq_aprovacao_w;

						end if;

						select	count(*)
						into	qt_existe_w
						from	ordem_compra_item
						where	nr_ordem_compra = nr_ordem_compra_w;

						if	(qt_existe_w = 0) then
							begin

							delete ordem_compra
							where nr_ordem_compra = nr_ordem_compra_w;

							exception
							when others then
								nr_ordem_compra_w := nr_ordem_compra_w;
							end;
						end if;

						end;
					else
						begin
						update	ordem_compra_item
						set	qt_material = qt_material - qt_material_ww,
							vl_total_item = round(( (qt_material - qt_material_ww) * vl_unitario_material),4)
						where	nr_ordem_compra	= nr_ordem_compra_w
						and	nr_item_oci = nr_item_oci_w;

						update	ordem_compra_item_entrega
						set	qt_prevista_entrega = qt_prevista_entrega - qt_material_ww
						where	nr_ordem_compra	= nr_ordem_compra_w
						and	nr_item_oci = nr_item_oci_w
						and	dt_prevista_entrega =
							(select max(dt_prevista_entrega)
							from	ordem_compra_item_entrega
							where	nr_ordem_compra	= nr_ordem_compra_w
							and	nr_item_oci = nr_item_oci_w
							and	dt_cancelamento is null);

						end;
					end if;

					/* Verificar se existe mais de um item na OC - Matheus OS 41012 - em 26/09/2006 */
					select	count(*)
					into	qt_item_oc_w
					from	ordem_compra_item
					where	nr_ordem_compra = nr_ordem_compra_w
					and	qt_material > 0;

					select	count(1)
					into	qt_existe_w
					from	ordem_compra
					where	nr_ordem_compra = nr_ordem_compra_w;

					if	(ie_cancelar_ordem_consig_w  in ('S','A')) and
						(nr_seq_motivo_cancel_oc_w <> 0) and
						(qt_item_oc_w = 0) and
						(qt_existe_w > 0) then
						cancelar_ordem_compra(nr_ordem_compra_w, nr_seq_motivo_cancel_oc_w, 'N', 'N', 'N', nm_usuario_p, '','N', nr_cot_compra_w);
					end if;				
				end if;
				end;
			end loop;
			close C01;

			obter_regra_ordem_consignado
					(cd_estabelecimento_w,
					cd_local_estoque_p,
					cd_operacao_cons_paciente_w,
					cd_material_p,
					cd_cgc_fornecedor_p,
					cd_convenio_w,
					cd_setor_atendimento_p,
					ie_gera_ordem_w,
					cd_local_entrega_w,
					cd_centro_custo_ww,
					pr_desconto_w,
					nm_usuario_dest_w,
					cd_perfil_comunic_w,
					cd_pessoa_solicitante_w,
					ie_gera_oc_reposicao_w);

			select comunic_interna_seq.nextval
			into nr_sequencia_w
			from dual;
			select obter_classif_comunic('F')
			into	nr_seq_classif_w
			from dual;

			ds_comunicacao_w := 	WHEB_MENSAGEM_PCK.get_texto(301948,'CD_MATERIAL_P='||cd_material_p||
							';DS_MATERIAL_W='||ds_material_w||
							';QT_MATERIAL_W='||qt_material_w||
							';NR_PRESCRICAO_P='||nr_prescricao_p||
							';NR_ATENDIMENTO_P='||nr_atendimento_p||
							';NM_PESSOA_FISICA_W='||nm_pessoa_fisica_w||
							';CD_CGC_FORNECEDOR_P='||cd_cgc_fornecedor_p);

			if	(ie_gera_ordem_w in ('C','A')) then
				select	max(b.nm_usuario)
				into	nm_usuario_ww
				from	usuario b,
					parametro_compras a
				where	a.cd_estabelecimento	= cd_estabelecimento_w
				and	a.cd_comprador_padrao	= b.cd_pessoa_fisica;

				if	(nm_usuario_dest_w is not null) then
					nm_usuario_ww := substr(nm_usuario_ww || ',' || nm_usuario_dest_w,1,255);
				end if;				

				insert into comunic_interna(
					dt_comunicado, ds_titulo, ds_comunicado,
					nm_usuario, dt_atualizacao, ie_geral, nm_usuario_destino,
					nr_sequencia,	ie_gerencial, nr_seq_classif, dt_liberacao)
				values(
					sysdate + 1/86400, WHEB_MENSAGEM_PCK.get_texto(301951), ds_comunicacao_w,
					nm_usuario_p, sysdate, 'N', nm_usuario_ww || ',',
					nr_sequencia_w, 'N', nr_seq_classif_w, sysdate);
			end if;

			if	(ie_gera_ordem_w in ('E','O')) then
				select	max(b.nm_usuario)
				into	nm_usuario_ww
				from	usuario b,
					parametro_compras a
				where	a.cd_estabelecimento	= cd_estabelecimento_w
				and	a.cd_comprador_padrao	= b.cd_pessoa_fisica;

				nm_usuario_dest_w := substr(nm_usuario_dest_w ||','|| nm_usuario_ww,1,2000);

				lista_usuario_w	:= substr(nm_usuario_dest_w,1,2000);

				while	(lista_usuario_w is not null) and
					(trim(lista_usuario_w) <> ',') loop
					tam_lista_w		:= length(lista_usuario_w);
					ie_pos_virgula_w	:= instr(lista_usuario_w,',');
					if	(ie_pos_virgula_w <> 0) then
						nm_usuario_w	:= substr(lista_usuario_w,1,(ie_pos_virgula_w - 1));
						lista_usuario_w	:= substr(lista_usuario_w,(ie_pos_virgula_w + 1), tam_lista_w);

						select	trim(max(ds_email))
						into	ds_email_usuario_w
						from	usuario
						where	nm_usuario = nm_usuario_w;

						if	(nvl(ds_email_usuario_w,'X') <> 'X') then
							begin
							ds_lista_email_usuario_w := substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
							end;
						end if;
					elsif	(tam_lista_w > 1) then
						nm_usuario_w	:= lista_usuario_w;
						lista_usuario_w	:= null;

						select	trim(max(ds_email))
						into	ds_email_usuario_w
						from	usuario
						where	nm_usuario = nm_usuario_w;

						if	(nvl(ds_email_usuario_w,'X') <> 'X') then
							begin
							ds_lista_email_usuario_w := substr(ds_lista_email_usuario_w||ds_email_usuario_w||',',1,2000);
							end;
						end if;
					else
						lista_usuario_w	:= null;
					end if;
				end loop;

				if	(nvl(ds_lista_email_usuario_w,'X') <> 'X') then
					begin
					Enviar_Email(WHEB_MENSAGEM_PCK.get_texto(301943),ds_comunicacao_w, null, ds_lista_email_usuario_w, nm_usuario_p,'M');
					exception
					when others then
						null;
					end;
				end if;
			end if;
		end if;
	end if;
end if;

ds_observacao_movto_w	:= 'Gerar_Prescricao_Estoque - MAP ' || nr_sequencia_p;

if	(ds_observacao_p is not null) then
	ds_observacao_movto_w	:= substr(ds_observacao_movto_w || chr(13) || chr(10) || ds_observacao_p,1,255);
end if;

/*Mesmo tratamento de acaba fazendo na trigger do movimento de estoque*/
if	(nvl(cd_setor_atendimento_p, 0) > 0) and (cd_centro_custo_w is null) then
	select	max(cd_centro_custo)
	into	cd_centro_custo_w
	from 	setor_atendimento
	where 	cd_setor_atendimento = cd_setor_atendimento_p;
end if;

if	(ie_devolucao_w = 'S') and 
	(nvl(ie_passagem_direta_cc_w,'X') <> 'S') and (nvl(ie_passagem_direta_cc_prescr_w,'X') <> 'S') then
	begin
	ie_tipo_conta_w	:= 3; 

	if	(nvl(cd_centro_custo_w,0) > 0) then
		ie_tipo_conta_w := 2;
	end if;
	end;
else
	begin
	ie_tipo_conta_w	:= 2;

	if	(nvl(cd_centro_custo_w,0) > 0) then
		ie_tipo_conta_w := 3;
	end if;
	end;
end if;

Define_Conta_Material(	
	cd_estabelecimento_w,
	cd_material_p,
	ie_tipo_conta_w,
	null,
	cd_setor_atendimento_p,
	null,
	null,
	null,
	null,
	null,
	cd_local_estoque_p,
	cd_operacao_cons_paciente_w,
	dt_atendimento_p,
	cd_conta_contabil_w,
	cd_centro_custo_w,null);

if	(nvl(cd_operacao_cons_paciente_w,0) = 0) then
	begin

	WHEB_MENSAGEM_PCK.exibir_mensagem_abort(315500);	

	end;
end if;	

select	movimento_estoque_seq.nextval
into	nr_movimento_estoque_w
from	dual;

insert into movimento_estoque(
 	nr_movimento_estoque,
 	cd_estabelecimento,
 	cd_local_estoque,
 	dt_movimento_estoque,
 	cd_operacao_estoque,
 	cd_acao,
 	cd_material,
 	dt_mesano_referencia,
 	qt_movimento,
 	dt_atualizacao,
 	nm_usuario,
 	ie_origem_documento,
 	nr_documento,
 	nr_sequencia_item_docto,
 	cd_unidade_medida_estoque,
 	cd_setor_atendimento,
 	qt_estoque,
	cd_centro_custo,
 	cd_unidade_med_mov,
	cd_fornecedor,
	ds_observacao,
	nr_seq_tab_orig,
	nr_seq_lote_fornec,
	cd_lote_fabricacao,
	dt_validade,
	nr_atendimento,
	nr_prescricao,
	nr_receita,
	cd_conta_contabil,
	nr_ordem_compra,
	nr_item_oci,
	nr_lote_ap,
	nr_lote_producao)
values(	nr_movimento_estoque_w,
 	cd_estabelecimento_w,
 	cd_local_estoque_p,
	dt_atendimento_p,
 	cd_operacao_cons_paciente_w,
 	cd_acao_w,
	cd_material_p,
	dt_atendimento_p,
 	qt_material_w,
 	sysdate,
 	nm_usuario_p,
 	'3',
 	nvl(nr_prescricao_p,nr_atendimento_p),
 	nvl(nr_seq_prescricao_p,999),
 	cd_unidade_medida_p,
 	cd_setor_atendimento_p,
 	qt_material_w,
	cd_centro_custo_w,
	cd_unidade_medida_p,
	cd_cgc_fornecedor_p,
	substr(ds_observacao_movto_w,1,255),
	nr_sequencia_p,
	decode(nr_seq_lote_fornec_p, 0, null, nr_seq_lote_fornec_p),
	ds_lote_fornec_w,
	dt_validade_w,
	nr_atendimento_p,
	nr_prescricao_p,
	decode(nr_receita_p,0,null,nr_receita_p),
	cd_conta_contabil_w,
	nr_ordem_compra_gerada_w,
	nr_item_oci_gerado_w,
	decode(nr_lote_ap_p,0,null,nr_lote_ap_p),
	decode(ie_vago1_p,'X',campo_numerico(ie_vago3_p),null));


end Gerar_Prescricao_Estoque;
/
