create or replace
procedure GPI_CONSISTIR_INATIV_PROJETO(	nm_usuario_p	Varchar2,
					nr_sequencia_p	number) is 
					
ds_consistencia_w	varchar2(4000);
qt_registro_w		number(10) := 0;

begin

select 	count(*)
into	qt_registro_w
from	solic_compra
where 	nr_seq_proj_gpi 	= nr_sequencia_p
and 	nr_seq_motivo_cancel 	is null;

if 	(qt_registro_w <> 0) then
	ds_consistencia_w 	:= substr(obter_dic_objeto_idioma(266548,WHEB_USUARIO_PCK.get_nr_seq_idioma) || chr(13) || chr(10),1,4000);
end if;

qt_registro_w 	:= 0;
select	count(*)
into	qt_registro_w
from 	nota_fiscal_item
where 	nr_seq_proj_gpi 	= nr_sequencia_p;

if 	(qt_registro_w <> 0) then
	ds_consistencia_w	:= substr(ds_consistencia_w || obter_dic_objeto_idioma(266549,WHEB_USUARIO_PCK.get_nr_seq_idioma) || chr(13) || chr(10),1,4000);
end if;

qt_registro_w	:= 0;
select	count(*)
into	qt_registro_w
from 	titulo_pagar
where 	nr_seq_proj_gpi 	= nr_sequencia_p;

if 	(qt_registro_w <> 0) then
	ds_consistencia_w	:= substr(ds_consistencia_w ||	obter_dic_objeto_idioma(266544,WHEB_USUARIO_PCK.get_nr_seq_idioma) || chr(13) || chr(10),1,4000);
end if;

qt_registro_w := 0;
select	count(*)
into	qt_registro_w 
from 	requisicao_material
where 	nr_seq_proj_gpi 	= nr_sequencia_p;

if 	(qt_registro_w <> 0) then
	ds_consistencia_w	:= substr(ds_consistencia_w || obter_dic_objeto_idioma(266545,WHEB_USUARIO_PCK.get_nr_seq_idioma) || chr(13) || chr(10),1,4000);
end if;

qt_registro_w	:= 0;
select	count(*)
into	qt_registro_w 
from	man_ordem_servico
where 	nr_seq_proj_gpi 	= nr_sequencia_p;

if	(qt_registro_w <> 0) then
	ds_consistencia_w	:= substr(ds_consistencia_w || obter_dic_objeto_idioma(266546,WHEB_USUARIO_PCK.get_nr_seq_idioma) || chr(13) || chr(10),1,4000);
end if;

if	(ds_consistencia_w is not null) then
	
	ds_consistencia_w	:= substr(ds_consistencia_w || obter_dic_objeto_idioma(266547,WHEB_USUARIO_PCK.get_nr_seq_idioma),1,4000);
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_consistencia_w);
end if;


end GPI_CONSISTIR_INATIV_PROJETO;
/