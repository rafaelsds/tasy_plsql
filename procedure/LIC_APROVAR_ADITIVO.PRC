create or replace
procedure lic_aprovar_aditivo(		nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 

pr_aditivo_w			number(15,4);
pr_aditivo_total_w			number(15,4);
qt_item_w			number(15,4);
nr_seq_reg_compra_item_w		number(10);
ds_historico_w			varchar2(4000);
cd_material_w			number(6);
ds_material_w			varchar2(255);
nr_seq_reg_compra_w		number(10);

begin

select	nr_seq_reg_compra_item,
	pr_aditivo,
	qt_item
into	nr_seq_reg_compra_item_w,
	pr_aditivo_w,
	qt_item_w
from	reg_lic_aditivo
where	nr_sequencia = nr_sequencia_p;

select	nvl(sum(pr_aditivo),0) + pr_aditivo_w
into	pr_aditivo_total_w
from	reg_lic_aditivo
where	nr_seq_reg_compra_item = nr_seq_reg_compra_item_w
and	dt_aprovacao is not null;

if	(pr_aditivo_total_w > 25) then
	wheb_mensagem_pck.exibir_mensagem_abort(266169);
	--'O valor de ativo n�o pode ultrapassar 25% sobre a quantidade do registro de pre�o.');
end if;

update	reg_compra_item
set	qt_item		= qt_item + qt_item_w
where	nr_sequencia	= nr_seq_reg_compra_item_w;

update	reg_lic_aditivo
set	dt_aprovacao	= sysdate,
	nm_usuario_aprov	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;

select	nr_seq_reg_compra,
	cd_material,
	substr(obter_desc_material(cd_material),1,255) ds_material
into	nr_seq_reg_compra_w,
	cd_material_w,
	ds_material_w
from	reg_compra_item
where	nr_sequencia	= nr_seq_reg_compra_item_w;    

ds_historico_w := 	substr(WHEB_MENSAGEM_PCK.get_texto(310364, 'NR_SEQUENCIA_P=' || nr_sequencia_p || ';PR_ADITIVO_W=' || pr_aditivo_w) || cd_material_w || ' - ' || ds_material_w || '.',1,4000); --Aprovado o aditivo n�mero ' || nr_sequencia_p || ' de ' || pr_aditivo_w  || '% do material '
			
lic_gerar_historico_reg_preco(nr_seq_reg_compra_w, ds_historico_w, 'S', nm_usuario_p);

commit;

end lic_aprovar_aditivo;
/
