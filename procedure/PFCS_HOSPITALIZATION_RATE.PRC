create or replace procedure pfcs_hospitalization_rate(
					nr_seq_indicator_p	number,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p	varchar2) is
	
cursor c01 is
	select	p.cd_pessoa_fisica id_patient,
		nvl(get_formatted_person_name(p.cd_pessoa_fisica, 'list'), obter_nome_pf(p.cd_pessoa_fisica)) nm_patient,
		pfcs_obter_lista_dados_classif(p.cd_pessoa_fisica) ds_classification,
		obter_sexo_pf(p.cd_pessoa_fisica, 'D') ds_gender,
		pf.dt_nascimento dt_birthdate,
        obter_dados_pf(p.cd_pessoa_fisica, 'I') qt_idade_paciente,
		p.nr_atendimento nr_encounter,
		p.dt_entrada dt_entrance
	from	atendimento_paciente p, pessoa_fisica pf
	where	to_date(obter_dados_desfecho_pa(p.nr_atendimento, 'UP'), 'dd/mm/yyyy hh24:mi:ss') > pfcs_get_pa_period
	and	(p.ie_tipo_atendimento = 3 or (p.ie_tipo_atendimento = 1 and (p.dt_usuario_intern is not null or p.nr_atend_origem_pa is not null)))
	and	p.dt_cancelamento is null
	and	p.cd_estabelecimento = to_number(cd_estabelecimento_p)
	and	p.cd_pessoa_fisica = pf.cd_pessoa_fisica;

cursor c02(nr_atendimento_p number) is
	select	1/*decode(nvl(count(*),0),0,'N','S') ie_hospitalized*/
	from	atendimento_alta a, parametro_medico pm
	where	a.nr_atendimento = nr_atendimento_p
	and	a.ie_desfecho = 'I'
	and     pm.cd_estabelecimento = to_number(cd_estabelecimento_p)
	and 	((a.ie_tipo_orientacao <> 'P')
	or  	(nvl(pm.ie_liberar_desfecho,'N')  = 'N')
	or  	((a.dt_liberacao is not null) and (a.dt_inativacao is null)));

nr_seq_operational_level_w		pfcs_operational_level.nr_sequencia%type;
pfcs_panel_detail_seq_w			pfcs_panel_detail.nr_sequencia%type;
nr_seq_panel_w				pfcs_panel_detail.nr_seq_panel%type;
qt_total_w				number(10) := 0;
qt_hospitalization_w			number(10) := 0;

begin

	nr_seq_operational_level_w := pfcs_get_structure_level(
									cd_establishment_p => cd_estabelecimento_p,
									ie_level_p => 'O',
									ie_info_p => 'C');

	for c01_w in c01 loop
		begin

			qt_total_w := qt_total_w + 1;

			for c02_w in c02(c01_w.nr_encounter) loop
				begin

					qt_hospitalization_w := qt_hospitalization_w + 1;

					select	pfcs_panel_detail_seq.nextval
					into	pfcs_panel_detail_seq_w
					from	dual;

					insert into pfcs_panel_detail(
						nr_sequencia,
						nm_usuario,
						dt_atualizacao,
						nm_usuario_nrec,
						dt_atualizacao_nrec,
						ie_situation,
						nr_seq_indicator,
						nr_seq_operational_level)
					values (
						pfcs_panel_detail_seq_w,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						sysdate,
						'T',
						nr_seq_indicator_p,
						nr_seq_operational_level_w);

					insert into pfcs_detail_patient(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_encounter,
						dt_entrance,
						id_patient,
						nm_patient,
						ds_gender,
						ds_classification,
						dt_birthdate,
                        ds_age_range,
						nr_seq_detail)
					values (
						pfcs_detail_patient_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						c01_w.nr_encounter,
						c01_w.dt_entrance,
						c01_w.id_patient,
						c01_w.nm_patient,
						c01_w.ds_gender,
						c01_w.ds_classification,
						c01_w.dt_birthdate,
                        c01_w.qt_idade_paciente,
						pfcs_panel_detail_seq_w);

				end;
			end loop;

			commit;

		end;
	end loop;

	pfcs_pck.pfcs_generate_results(
		vl_indicator_p => qt_hospitalization_w,
		vl_indicator_aux_p => qt_total_w,
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);

	pfcs_pck.pfcs_update_detail(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_panel_p => nr_seq_panel_w,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

	pfcs_pck.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

end pfcs_hospitalization_rate;
/