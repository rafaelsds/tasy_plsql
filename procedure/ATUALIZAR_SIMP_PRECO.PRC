create or replace
procedure atualizar_simp_preco(
			nm_usuario_p		Varchar2,
			cd_tuss_p		number,
			cd_simpro_p		number,
			dt_vigencia_p		date) is 

cd_estab_simpro_w	estabelecimento.cd_estabelecimento%type;

begin

cd_estab_simpro_w := wheb_usuario_pck.get_cd_estabelecimento;

update	simpro_preco
set	cd_tuss = cd_tuss_p,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where 	cd_simpro = cd_simpro_p
and 	dt_vigencia = dt_vigencia_p
and     nvl(cd_estabelecimento, nvl(cd_estab_simpro_w, 0)) = nvl(cd_estab_simpro_w, 0);

commit;

end atualizar_simp_preco;
/
