create or replace procedure evento_metas_medicamento(cd_convenio_p		number,
				cd_material_p			number, 
				cd_prof_solicitou_p		number,
				cd_prof_autorizou_p		number,
				nr_atendimento_p		number,
				cd_estabelecimento_p		number,
				cd_perfil_p			number default null,
				cd_setor_p			number default null,
				ds_email_p			varchar2 default null,
				nr_telefone_p			number default null) is 
			
ie_tipo_material_w		cad_metas_medicamentos.ie_tipo_material%type;
qt_estoque_maximo_w		cad_metas_medicamentos.qt_estoque_maximo%type;
qt_estoque_minimo_w		cad_metas_medicamentos.qt_estoque_minimo%type;
vl_meta_w			cad_metas_medicamentos.vl_meta%type;
vl_meta_minima_w		cad_metas_medicamentos.vl_meta_minima%type;
cd_grupo_material_w		cad_metas_medicamentos.cd_grupo_material%type;
cd_subgrupo_material_w		cad_metas_medicamentos.cd_subgrupo_material%type;
cd_classe_material_w		cad_metas_medicamentos.cd_classe_material%type;
cd_estab_w			saldo_estoque.cd_estabelecimento%type;
nr_seq_meta_w			cad_metas_medicamentos.nr_sequencia%type;
cd_material_w			material.cd_material%type;
ds_material_w			material.ds_material%type;
cd_local_estoque_w		local_estoque.cd_local_estoque%type;
ds_local_estoque_w		local_estoque.ds_local_estoque%type;
qt_material_w			prescr_material.qt_material%type;
ie_med_alto_custo_w		cad_metas_medicamentos.ie_med_alto_custo%type;
nr_prescricao_w         prescr_material.nr_prescricao%type;


Cursor C01 is
	select	b.ie_tipo_material,
		b.qt_estoque_maximo,
		b.qt_estoque_minimo,
		b.vl_meta,
		b.vl_meta_minima,
		b.cd_grupo_material,
		b.cd_subgrupo_material,
		b.cd_classe_material,
		b.nr_sequencia,
		b.cd_local_estoque,
		nvl(b.ie_med_alto_custo,'N')
	from	cad_metas_medicamentos b
	where	nvl(b.ie_situacao,'A') = 'A'
	and	b.cd_convenio = nvl(cd_convenio_p, b.cd_convenio)
	and	b.cd_material = cd_material_p
	order by b.nm_meta;
	
Cursor C02 is
	select	b.ds_material
    into ds_material_w
	from	saldo_estoque a,
		material b,
		grupo_material d,
		subgrupo_material c,
		Classe_material e
	where	b.cd_classe_material = e.cd_classe_material
	and	e.cd_subgrupo_material = c.cd_subgrupo_material
	and c.cd_grupo_material = d.cd_grupo_material
	and	a.cd_material = b.cd_material
    and ((b.cd_classe_material = cd_classe_material_w) or (nvl(cd_classe_material_w,0) = 0))
    and ((c.cd_grupo_material = cd_grupo_material_w) or (nvl(cd_grupo_material_w,0) = 0))
    and ((e.cd_subgrupo_material = cd_subgrupo_material_w) or (nvl(cd_subgrupo_material_w,0) = 0))
    and ((b.ie_tipo_material = ie_tipo_material_w) or (nvl(ie_tipo_material_w,'0') = '0'))
    and ((b.cd_material = cd_material_p) or (nvl(cd_material_p,0) = 0))
	and	not exists ( 	select	1
				from	cad_metas_medicamentos b
				where	nvl(b.ie_situacao,'A') = 'A'
				and	nvl(b.ie_excecao,'N') = 'S'
				and	b.nr_sequencia = nr_seq_meta_w	)
	and	a.cd_estabelecimento = nvl(cd_estabelecimento_p, a.cd_estabelecimento)
	and	pkg_date_utils.start_of(a.dt_mesano_referencia, 'MONTH', 0) = pkg_date_utils.start_of(sysdate, 'MONTH', 0)
    and ESTABLISHMENT_TIMEZONE_UTILS.startOfmonth(a.dt_mesano_referencia) = ESTABLISHMENT_TIMEZONE_UTILS.startOfmonth(sysdate)
	and	a.cd_local_estoque = cd_local_estoque_w
	and (ie_med_alto_custo_w = 'S')
    and	((ie_med_alto_custo_w = 'S')
		or ((nvl(qt_material_w,0) >= nvl(qt_estoque_maximo_w,0)) or (nvl(qt_material_w,0) <= nvl(qt_estoque_minimo_w,0))))
   ;
Cursor C03 is
	select	b.ds_material
	from	saldo_estoque a,
		material b,
		grupo_material d,
		subgrupo_material c,
		classe_material e
	where	b.cd_classe_material = e.cd_classe_material
	and	e.cd_subgrupo_material = c.cd_subgrupo_material
	and 	c.cd_grupo_material = d.cd_grupo_material
	and	a.cd_material = b.cd_material
    and ((b.cd_classe_material = cd_classe_material_w) or (nvl(cd_classe_material_w,0) = 0))
    and ((c.cd_grupo_material = cd_grupo_material_w) or (nvl(cd_grupo_material_w,0) = 0))
    and ((e.cd_subgrupo_material = cd_subgrupo_material_w) or (nvl(cd_subgrupo_material_w,0) = 0))
    and ((b.ie_tipo_material = ie_tipo_material_w) or (nvl(ie_tipo_material_w,'0') = '0'))
    and ((b.cd_material = cd_material_p) or (nvl(cd_material_p,0) = 0))
	and	not exists ( 	select	1
				from	cad_metas_medicamentos b
				where	nvl(b.ie_situacao,'A') = 'A'
				and	nvl(b.ie_excecao,'N') = 'S'
				and	b.nr_sequencia = nr_seq_meta_w	)
	and	a.cd_estabelecimento = nvl(cd_estabelecimento_p, a.cd_estabelecimento)
	and	pkg_date_utils.start_of(a.dt_mesano_referencia, 'MONTH', 0) = pkg_date_utils.start_of(sysdate, 'MONTH', 0)
	and	a.cd_local_estoque = cd_local_estoque_w
	and	((ie_med_alto_custo_w = 'S')
		or (((nvl(qt_material_w,0)*nvl(a.vl_custo_medio,0)) <= nvl(vl_meta_w,0)) or ((nvl(qt_material_w,0)*nvl(a.vl_custo_medio,0)) >= nvl(vl_meta_minima_w,0))));
		
begin
open C01;
loop
fetch C01 into	
	ie_tipo_material_w,
	qt_estoque_maximo_w,
	qt_estoque_minimo_w,
	vl_meta_w,
	vl_meta_minima_w,
	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w,
	nr_seq_meta_w,
	cd_local_estoque_w,
	ie_med_alto_custo_w;
exit when C01%notfound;
	begin
	select 	sum(qt_material)
	into	qt_material_w
	from 	prescr_material
	where 	cd_material = cd_material_p
	and	nvl(dt_liberacao,nvl(dt_lib_material,nvl(dt_lib_farmacia,dt_lib_enfermagem))) is not null
	and	pkg_date_utils.start_of(nvl(dt_liberacao,nvl(dt_lib_material,nvl(dt_lib_farmacia,dt_lib_enfermagem))), 'MONTH', 0) = pkg_date_utils.start_of(sysdate, 'MONTH', 0);	
    
    open C02;
	loop
	fetch C02 into	
		ds_material_w;
	exit when C02%notfound;
		begin			
		if (ds_material_w is not null) then
			ds_local_estoque_w := obter_desc_local_estoque(cd_local_estoque_w);
			Enviar_evento_medicamento(cd_convenio_p, ds_material_w, cd_prof_solicitou_p, cd_prof_autorizou_p, cd_estabelecimento_p, nr_seq_meta_w, nvl(nr_atendimento_p,0), cd_perfil_p, cd_setor_p, ds_email_p, nr_telefone_p, 'E', ds_local_estoque_w);
		end if;
		end;
	end loop;
	close C02;
	
	open C03;
	loop
	fetch C03 into	
		ds_material_w;
	exit when C03%notfound;
		begin
		if (ds_material_w is not null) then
			ds_local_estoque_w := obter_desc_local_estoque(cd_local_estoque_w);
			Enviar_evento_medicamento(cd_convenio_p, ds_material_w, cd_prof_solicitou_p, cd_prof_autorizou_p, cd_estabelecimento_p, nr_seq_meta_w, nvl(nr_atendimento_p,0), cd_perfil_p, cd_setor_p, ds_email_p, nr_telefone_p, 'V', ds_local_estoque_w);
		end if;
		end;
	end loop;
	close C03;
	
	end;
end loop;
close C01;

commit;

end evento_metas_medicamento;
/