create or replace
procedure fa_inserir_orientacao_paciente(nr_seq_entre_item_p		number,
					 cd_material_p			number,
					 ds_orientacao_p		varchar2,
					 nm_usuario_p			varchar2) is 

nr_seq_rec_item_w	number(10);
nr_seq_orientacao_w	number(10);					 
begin

select 	max(t.nr_sequencia)
into	nr_seq_rec_item_w	
from   	fa_entrega_medicacao_item i,
	fa_entrega_medicacao m,
	fa_receita_farmacia f,
	fa_receita_farmacia_item t
where 	i.nr_seq_fa_entrega = m.nr_sequencia
and   	f.nr_sequencia = m.nr_seq_receita_amb
and   	t.nr_seq_receita = f.nr_sequencia
and   	i.nr_sequencia = nr_seq_entre_item_p
and   	t.cd_material = cd_material_p;
	
select	count(*) 
into	nr_seq_orientacao_w
from	fa_receita_far_orient_item
where	nr_seq_receita_item =  nr_seq_rec_item_w;

if (nr_seq_orientacao_w > 0) then

	select	max(nr_sequencia)
	into	nr_seq_orientacao_w
	from 	fa_receita_far_orient_item
	where	nr_seq_receita_item =  nr_seq_rec_item_w;

	update 	fa_receita_far_orient_item
	set	ds_orientacao  = ds_orientacao_p,
		nm_usuario     = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia   = nr_seq_orientacao_w;
else
	insert 
	into 	fa_receita_far_orient_item
		(nr_sequencia,
		nm_usuario,		
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_receita_item,
		ds_orientacao)
	values	(fa_receita_far_orient_item_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_rec_item_w,
		ds_orientacao_p);
end if;
	
commit;

end fa_inserir_orientacao_paciente;
/
