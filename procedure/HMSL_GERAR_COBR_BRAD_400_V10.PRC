create or replace
procedure hmsl_gerar_cobr_brad_400_v10
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 
/*	Versao 06
	Data: 05/06/2009
*/

/* HEader */
ds_conteudo_w			varchar2(400);
ds_brancos_277_w		varchar2(277);
nm_empresa_w			varchar2(30);
cd_empresa_w			varchar2(20);
ds_branco_8_w			varchar2(8);
nr_seq_arquivo_w		varchar2(7);
dt_geracao_w			varchar2(6);
nr_seq_registro_w		varchar2(10);

/* Transacao */
nm_avalista_w			varchar2(60);
nm_sacado_w			varchar2(40);
ds_endereco_sacado_w		varchar2(40);
nr_controle_partic_w		varchar2(25);
id_empresa_w			varchar2(17);
nr_inscricao_w			varchar2(14);
vl_titulo_w			varchar2(13);
vl_acrescimo_w			varchar2(13);
vl_desconto_w			varchar2(13);
vl_iof_w			varchar2(13);
vl_abatimento_w			varchar2(13);
nr_nosso_numero_w		varchar2(11);
vl_desconto_dia_w		varchar2(10);
nr_documento_w			varchar2(10);
cd_cep_w			varchar2(15);
cd_conta_w			varchar2(7);
dt_vencimento_w			varchar2(6);
dt_emissao_w			varchar2(6);
dt_desconto_w			varchar2(6);
cd_agencia_debito_w		varchar2(5);
cd_agencia_bancaria_w		varchar2(5);
cd_agencia_deposito_w		varchar2(5);
pr_multa_w			varchar2(4);
cd_banco_w			varchar2(3);
cd_banco_cobranca_w		varchar2(3);
ds_brancos_2_w			varchar2(2);
ie_ocorrencia_w			varchar2(2);
ie_especie_w			varchar2(2);
ie_instrucao_1_w		varchar2(2);
ie_instrucao_2_w		varchar2(2);
ie_tipo_inscricao_w		varchar2(2);
ie_digito_agencia_w		varchar2(1);
ie_digito_conta_w		varchar2(1);
ie_multa_w			varchar2(1);
nr_dig_nosso_numero_w		varchar2(1);
cd_condicao_w			varchar2(1);
ie_emite_papeleta_w		varchar2(1);
ie_rateio_w			varchar2(1);
ie_endereco_w			varchar2(1);
ie_aceite_w			varchar2(1);
ds_brancos_10_w			varchar2(10);
ds_brancos_12_w			varchar2(12);
nr_titulo_w			number(10);
cd_carteira_w			varchar2(3);

/*Mensagens - Instrucao boleto*/
ds_mens_boleto_w        	varchar2(200);
ds_conteudo_envio_w		varchar(4000);
controle_msg_w			number(15) := 0;
controle_msg_2_w		number(15) := 0;
qt_instrucao_w			number(5);
qt_instrucao_2_w		number(5);
nr_seq_grupo_inst_w		number(10);

/* Trailler */
ds_brancos_393_w		varchar2(393);

nr_seq_apres_w			number(10)	:= 0;
qt_registros_w			number(10)	:= 1;
ie_gerar_cob_esc_prim_mens_w	varchar(1) 	:= null;
vl_multa_w				varchar2(12);		

Cursor C01 is
	--PMG - 22/01/2016 - OS 984998
	select	lpad(x.cd_agencia_bancaria,5, '0') cd_agencia_debito,
    lpad(nvl(substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DA'),1,1),'0'),1,'0') ie_digito_agencia,		
		lpad(x.cd_agencia_bancaria,5, '0') cd_agencia_bancaria,
		lpad(x.cd_conta,7,'0') cd_conta,
		nvl(x.ie_digito_conta,'0') ie_digito_conta,
		'0' || lpad(nvl(x.cd_carteira,'0'),3,'0') || lpad(nvl(substr(x.cd_agencia_bancaria,1,5),'0'),5,'0') || lpad(nvl(substr(x.cd_conta,1,7),'0'),7,'0') || nvl(x.ie_digito_conta,'0') id_empresa,
		lpad(nvl(f.cd_cgc,'0'),25,'0') nr_controle_partic,
		lpad(nvl(substr(a.cd_banco,1,3),'0'),3,'0') cd_banco,
		'2' ie_multa,
		lpad(nvl(substr((b.tx_multa*100),1,4),0),4,'0') pr_multa,
		lpad(nvl(to_char(b.nr_titulo),'0'),11,'0') nr_nosso_numero,
		decode(calcula_digito('MODULO11_BRAD',lpad(substr(x.cd_carteira,2,2),2,'0') || lpad(b.nr_titulo,11,'0')),'-1','P',
			calcula_digito('MODULO11_BRAD',lpad(substr(x.cd_carteira,2,2),2,'0') || lpad(b.nr_titulo,11,0))) nr_dig_nosso_numero,
		lpad('0',10,'0') vl_desconto_dia,
		nvl(a.IE_EMISSAO_BLOQUETO,'2') cd_condicao,
		'N' ie_emite_papeleta,
		' ' ie_rateio,
		'0' ie_endereco,
		lpad(nvl(substr(c.cd_ocorrencia,1,2),'1'),2,'0') ie_ocorrencia,
		lpad('0',10,'0') nr_documento,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyy') dt_vencimento,
		lpad(replace(to_char(nvl(b.vl_titulo,0), 'fm00000000000.00'),'.',''),13,'0') vl_titulo,
		lpad('0',3,'0') cd_banco_cobranca,
		lpad('0',5,'0') cd_agencia_deposito,
		'12' ie_especie,
		'N' ie_aceite,
		to_char(b.dt_emissao,'ddmmyy') dt_emissao,
		lpad(nvl(substr(c.cd_instrucao,1,2),'0'),2,'0') ie_instrucao1,
		'00' ie_instrucao2,
		--substr(lpad(decode(nvl(elimina_caracteres_especiais(b.tx_juros),0),0,'0',elimina_caracteres_especiais(b.tx_juros)),13,'0'),1,13) vl_acrescimo,
		--lpad(elimina_caracteres_especiais( round((b.vl_saldo_titulo/30) * dividir_sem_round(b.tx_juros, 100),2) ) ,13,'0') vl_acrescimo,	
		lpad(elimina_caracteres_especiais(to_char((b.vl_saldo_titulo * b.tx_juros)/100, 'fm00000000000.00')),13,'0') vl_acrescimo,
		decode(nvl(elimina_caracteres_especiais(c.vl_desconto),0),0,'000000',to_char(sysdate,'ddmmyy')) dt_desconto,
		substr(lpad(decode(nvl(elimina_caracteres_especiais(c.vl_desconto),0),0,'0',elimina_caracteres_especiais(c.vl_desconto)),13,'0'),1,13) vl_desconto,
		lpad('0',13,'0') vl_iof,
		lpad('0',13,'0') vl_abatimento,
		decode(b.cd_cgc, null,'01','02') ie_tipo_inscricao,
		lpad(nvl(b.cd_cgc_cpf,'0'),14,'0') nr_inscricao,
		rpad(nvl(upper(elimina_acentuacao(substr(b.nm_pessoa,1,40))),' '),40,' ') nm_sacado,
		substr(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'E')),1,24)  || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'NR'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'NR')),1,4) || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CO'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CO')),1,10),1,40) ds_endereco_sacado,
		substr(elimina_caractere_especial(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CEP'))),1,8) cd_cep,
		lpad(' ',60,' ') nm_avalista,
    b.nr_titulo,
		lpad(nvl(x.cd_carteira,'0'),3,'0'),
		substr(lpad(nvl(to_char(a.nr_sequencia),'0'),6,'0'),1,6) nr_seq_arquivo,
		lpad(elimina_caracteres_especiais(round((b.vl_saldo_titulo * b.tx_multa)/100,2)),12,'0') vl_multa
	from	pls_lote_mensalidade	z,
		banco_estabelecimento	x,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v	b,
		titulo_receber_cobr	c,
		cobranca_escritural	a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador	= f.nr_sequencia(+)
	and	d.nr_seq_lote		= z.nr_sequencia(+)
	and	((z.ie_primeira_mensalidade is null or z.ie_primeira_mensalidade = 'N') 
	or	ie_gerar_cob_esc_prim_mens_w = 'S')
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
 
/* MENSAGENS DE ATE 160 CARACTERES */
cursor c02 is 
  select	substr(obter_instrucao_boleto_tam (nr_titulo_w,cd_banco_w,a.nr_linha,'F', null, null, a.nr_sequencia),1,160) ds_mensagem
  from	banco_instrucao_boleto a
  where	nvl(a.ie_posicao,'F')			= 'F'
  and	nvl(a.ie_aplicacao_mensagem,'T')	= 'T'
  and	a.cd_banco			= cd_banco_w
  and	nvl(a.nr_seq_grupo,0)		= nvl(nr_seq_grupo_inst_w,nvl(a.nr_seq_grupo,0))
  order by	a.nr_linha;

begin
delete from w_envio_banco 
where nm_usuario = nm_usuario_p;

/* Pega o parametro para ver se considera os titulos gerados por lotes de primera mensalidade */
select	nvl(max(ie_gerar_cob_esc_prim_mens),'S')
into	ie_gerar_cob_esc_prim_mens_w
from	pls_parametros_cr
where	cd_estabelecimento = cd_estabelecimento_p;

select	lpad(' ',8,' '),
	lpad(' ',277,' '),
	lpad(' ',2,' '),
	lpad(' ',393,' '),
	lpad(' ',10,' '),
	lpad(' ',12,' ')
into	ds_branco_8_w,
	ds_brancos_277_w,
	ds_brancos_2_w,
	ds_brancos_393_w,
	ds_brancos_10_w,
	ds_brancos_12_w
from	dual;

/* Header */
select	lpad(nvl(substr(c.cd_convenio_banco,1,20),'0'),20,'0'),
	lpad(upper(elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,30))),30,' '),
	to_char(a.dt_remessa_retorno,'ddmmyy'),
	lpad(to_char(a.nr_sequencia),7,'0')
into	cd_empresa_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_seq_arquivo_w
from	estabelecimento		b,
	cobranca_escritural	a,
	banco_estabelecimento	c
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;

 ds_conteudo_w	:= 	'01' || 
			'REMESSA' || 
			'01' || 
			'COBRANCA       ' ||
			cd_empresa_w || 
			nm_empresa_w || 
			'237' || 
			'BRADESCO       ' || 
			dt_geracao_w || 
			ds_branco_8_w|| 
			'MX' || 
			nr_seq_arquivo_w || 
			ds_brancos_277_w || 
			lpad(qt_registros_w,6,'0');	
		
insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres,
			nr_seq_apres_2)
	values	(	nr_seq_registro_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w,
			nr_seq_apres_w);
			
nr_seq_apres_w	:= nr_seq_apres_w + 1;
qt_registros_w  := qt_registros_w + 1;
/* Fim Header */

/* Transacao */
--begin
open C01;
loop
fetch C01 into	
	cd_agencia_debito_w,
	ie_digito_agencia_w,
	cd_agencia_bancaria_w,
	cd_conta_w,
	ie_digito_conta_w,
	id_empresa_w,
	nr_controle_partic_w,
	cd_banco_w,
	ie_multa_w,
	pr_multa_w,
	nr_nosso_numero_w,
	nr_dig_nosso_numero_w,
	vl_desconto_dia_w,
	cd_condicao_w,
	ie_emite_papeleta_w,
	ie_rateio_w,
	ie_endereco_w,
	ie_ocorrencia_w,
	nr_documento_w,
	dt_vencimento_w,
	vl_titulo_w,
	cd_banco_cobranca_w,
	cd_agencia_deposito_w,
	ie_especie_w,
	ie_aceite_w,
	dt_emissao_w,
	ie_instrucao_1_w,
	ie_instrucao_2_w,
	vl_acrescimo_w,
	dt_desconto_w,
	vl_desconto_w,
	vl_iof_w,
	vl_abatimento_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	cd_cep_w,
	nm_avalista_w,
  nr_titulo_w,
	cd_carteira_w,
	nr_seq_arquivo_w,
	vl_multa_w;
exit when C01%notfound;
	begin
	select	w_envio_banco_seq.nextval
	into	nr_seq_registro_w
	from	dual;
    
  select max(NR_SEQUENCIA)
  into nr_seq_grupo_inst_w 
  from BANCO_GRUPO_INSTRUCAO 
  where cd_banco = cd_banco_w 
  and ie_situacao = 'A';
	
	ds_conteudo_w	:= 	'1' ||  --01
				lpad('0',5,'0') || 		--002 a 006
				'0' ||  	--007 a 007
				lpad('0',5,'0') ||  	--008 a 012
				lpad('0',7,'0') || 		  	--013 a 019
				'0' || 	  	--020 a 020
				id_empresa_w || 	  	--021 a 037
				nr_controle_partic_w ||   	--038 a 062
				--cd_banco_w || 		  	--063 a 065
				'000' || 		  	--063 a 065
				ie_multa_w || 		  	--066 a 066
				pr_multa_w || 		  	--067 a 070
				nr_nosso_numero_w ||	  	--071 a 081
				nr_dig_nosso_numero_w ||  	--082 a 082
				vl_desconto_dia_w || 		--083 a 092
				cd_condicao_w ||    		--093 a 093
				ie_emite_papeleta_w || 		--094 a 094
				ds_brancos_10_w || 		--095 a 104
				' ' ||				--105 a 105
				ie_endereco_w || 		--106 a 106
				ds_brancos_2_w || 		--107 a 108
				ie_ocorrencia_w || 		--109 a 110
				lpad(nr_titulo_w,10,'0')/* nr_documento_w */|| --111 a 120
				dt_vencimento_w || 		--121 a 126
				vl_titulo_w || 			--127 a 139
				cd_banco_cobranca_w || 		--140 a 142
				cd_agencia_deposito_w || 	--143 a 147
				ie_especie_w || 		--148 a 149
				ie_aceite_w || 			--150 a 150
				dt_emissao_w || 		--151 a 156
				ie_instrucao_1_w || 		--157 a 158
				ie_instrucao_2_w ||		--159 a 160
				vl_acrescimo_w || 		--161 a 173
				dt_desconto_w || 		--174 a 179
				vl_desconto_w || 		--180 a 192
				vl_iof_w || 			--193 a 205
				vl_abatimento_w || 		--206 a 218
				ie_tipo_inscricao_w ||		--219 a 220
				nr_inscricao_w || 		--221 a 234
				nm_sacado_w || 			--235 a 274
				rpad(ds_endereco_sacado_w,40,' ') || 	--275 a 314
				vl_multa_w || 		--315 a 326
				rpad(nvl(cd_cep_w,0),8,'0') || 	--327 a 331 e 332 a 334
				rpad(nvl(nm_avalista_w,' '),60,' ') || 	--335 a 394
				lpad(qt_registros_w,6,'0') ;		--395 a 400
				
	insert into w_envio_banco
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres,
				nr_seq_apres_2)
		values	(	nr_seq_registro_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_apres_w,
				nr_seq_apres_w);
				
	nr_seq_apres_w	:= nr_seq_apres_w + 1;
	
	qt_registros_w	:= qt_registros_w + 1;
	
	/*MENSAGENS VINDAS DO CURSOR C02*/  
  open c02;
		loop
		fetch c02 into
			ds_mens_boleto_w ;
		exit when c02%notfound;
			
		begin
		
			ds_conteudo_w	:= null;
			controle_msg_w	:= controle_msg_w + 1;
			
			select 	count(*)
			into	qt_instrucao_w
			from	banco_instrucao_boleto a
			where	a.cd_banco	= cd_banco_w
			and	a.cd_estabelecimento = cd_estabelecimento_p
			and	a.ie_posicao = 'F'
			order by a.nr_linha;
			
			qt_instrucao_2_w := qt_instrucao_w - controle_msg_w ;
			controle_msg_2_w := controle_msg_2_w + 1;
				
			if	( ds_mens_boleto_w is not null) then
				begin
				ds_conteudo_envio_w := ds_conteudo_envio_w || rpad( ds_mens_boleto_w,160,' ') ; 
				
					if ( controle_msg_2_w > 1) then
					
						ds_conteudo_w	:=	'2' || -- 001
									rpad( ds_conteudo_envio_w ,320,' ') || -- 002 a 321
									rpad(' ', 6, ' ') || -- 322 a 327
									rpad(' ', 13, ' ') || -- 328 a 340
									rpad(' ', 6, ' ') || -- 341 a 346
									rpad(' ', 13, ' ') || -- 347 a 359
									rpad(' ', 7, ' ') || -- 360 a 366
									cd_carteira_w || -- 367 a 369
									cd_agencia_debito_w || -- 370 a 374
									cd_conta_w || -- 375 a 381
									ie_digito_conta_w || -- 382 a 382
									nr_nosso_numero_w || -- 383 a 393
									nr_dig_nosso_numero_w || -- 394 a 394
									lpad( qt_registros_w,6,'0'); -- 395 a 400
                  
						qt_registros_w := qt_registros_w + 1;            
						
						insert into w_envio_banco
							(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_estabelecimento,
							ds_conteudo,
							nr_seq_apres,
							nr_seq_apres_2)
						values	(w_envio_banco_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_estabelecimento_p,
							ds_conteudo_w,
							nr_seq_apres_w,
							0);
              
						nr_seq_apres_w := nr_seq_apres_w + 1;
						controle_msg_2_w := 0;
						ds_conteudo_envio_w := null;
						
					elsif (( controle_msg_w > 1 and controle_msg_w = qt_instrucao_w )
						and ( qt_instrucao_w < 3 or qt_instrucao_2_w < 2)
						or  qt_instrucao_w < 2) then
					
						ds_conteudo_w	:=	'2' ||
									rpad(ds_conteudo_envio_w,320,' ') ||
									rpad(' ', 6, ' ') ||
									rpad(' ', 13, ' ') ||
									rpad(' ', 6, ' ') ||
									rpad(' ', 13, ' ') ||
									rpad(' ', 7, ' ') ||
									cd_carteira_w ||
									cd_agencia_debito_w ||
									cd_conta_w ||
									ie_digito_conta_w ||
									nr_nosso_numero_w ||
									nr_dig_nosso_numero_w ||
									lpad(qt_registros_w,6,'0');
                  
						qt_registros_w := qt_registros_w + 1;
						
						insert into w_envio_banco
							(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_estabelecimento,
							ds_conteudo,
							nr_seq_apres,
							nr_seq_apres_2)
						values	(w_envio_banco_seq.nextval,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							cd_estabelecimento_p,
							ds_conteudo_w,
							nr_seq_apres_w,
							0);
              
						nr_seq_apres_w := nr_seq_apres_w + 1;
						ds_conteudo_envio_w := null;
					end if;	
				end;
			end if;   
		end;
		end loop;
			controle_msg_w := 0;
			controle_msg_2_w := 0;
		close c02;
	
	if	(qt_registros_w = 500) then
		qt_registros_w	:= 1;
		commit;
	end if;
	end;
end loop;
close C01;

/* Fim Transacao */

/* Trailler */
ds_conteudo_w	:= 	'9' || 
			ds_brancos_393_w || 
			lpad(qt_registros_w,6,'0');	

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;
		
insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres,
			nr_seq_apres_2)
	values	(	nr_seq_registro_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w,
			nr_seq_apres_w);
/* Fim Trailler*/

commit;

end hmsl_gerar_cobr_brad_400_v10;
/