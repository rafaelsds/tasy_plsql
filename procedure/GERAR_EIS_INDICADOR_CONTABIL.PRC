create or replace 
procedure Gerar_EIS_Indicador_Contabil(	dt_parametro_p	date,
					nm_usuario_p	varchar2) is

cd_empresa_w		number(05,0);
cd_estabelecimento_w	number(04,0);
dt_referencia_w		date;
nr_seq_indicador_w		number(10,0);
cd_conta_w		varchar2(20);
ds_conta_base_w		varchar2(2000);
ds_conta_divisor_w		varchar2(2000);
vl_base_w		number(15,4);
vl_base_ww		number(15,4);
vl_divisor_w		number(15,4);
vl_indicador_w		number(15,4);
nr_sequencia_w		number(10,0);
ds_comando_w		varchar2(256);
ds_comando_ww		varchar2(4000);
ie_anualizar_w		varchar2(01);
ie_percentual_w		varchar2(01);
qt_mes_w		number(03,0);
Guampa			varchar2(1)		:= chr(39);
nr_seq_mes_ref_w		number(10,0);
ie_consol_empresa_w	varchar2(1);
vl_base_consol_w		number(15,4);
vl_base_consol_ww	number(15,4);
vl_divisor_consol_w		number(15,4);

cursor c01 is
select	cd_empresa,
	nr_sequencia,
	ds_conta_base,
	ds_conta_divisor,
	ie_anualizar,
	ie_percentual,
	nvl(ie_consol_empresa,'N')
from	eis_indicador_contabil
where	ie_situacao = 'A';

cursor c02 is
select	cd_estabelecimento
from	estabelecimento
where	cd_empresa = cd_empresa_w;

begin


commit;
dt_referencia_w	:= trunc(dt_parametro_p, 'month');
select campo_numerico(to_char(dt_referencia_w,'mm'))
into	qt_mes_w
from dual;

delete from eis_valor_indic_contab
where 	dt_referencia	= dt_referencia_w and nr_seq_ind_base is null;


open	c01;
loop
fetch	c01 into
	cd_empresa_w,
	nr_seq_indicador_w,
	ds_conta_base_w,
	ds_conta_divisor_w,
	ie_anualizar_w,
	ie_percentual_w,
	ie_consol_empresa_w;
exit	when c01%notfound;
	begin

	vl_base_consol_w	:= 0;
	vl_divisor_consol_w	:= 0;
	
	open c02;
	loop
	fetch c02 into
		cd_estabelecimento_w;
	exit	when c02%notfound;
		begin

		select	max(nr_sequencia)
		into	nr_seq_mes_ref_w
		from	ctb_mes_ref
		where	cd_empresa	= cd_empresa_w
		and	dt_referencia	= dt_referencia_w;

		select	ctb_Obter_Valor_Conta(nr_seq_mes_ref_w, ds_conta_base_w, cd_estabelecimento_w, null, 'S', null)
		into	vl_base_w
		from	dual;

		select	ctb_Obter_Valor_Conta(nr_seq_mes_ref_w, ds_conta_divisor_w, cd_estabelecimento_w, null,'S', null)
		into	vl_divisor_w
		from	dual;

		vl_base_ww		:= vl_base_w;
		if	(ie_anualizar_w = 'S') then
			vl_base_w	:= dividir((vl_base_w * 12), qt_mes_w);
		end if;

		vl_indicador_w	:= dividir(vl_base_w, vl_divisor_w);
		if	(ie_percentual_w = 'S') then
			vl_indicador_w	:= dividir(vl_base_w  * 100, vl_divisor_w);
		end if;

		if	(ie_consol_empresa_w = 'S') then
			vl_base_consol_w	:= vl_base_consol_w + vl_base_ww;
			vl_divisor_consol_w	:= vl_divisor_consol_w + vl_divisor_w;
		end if;
		
		select eis_valor_indic_contab_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert into eis_valor_indic_contab(
			nr_sequencia, cd_empresa, cd_estabelecimento, dt_referencia,
			nr_seq_indicador, dt_atualizacao,
			nm_usuario, vl_indicador,
			vl_base, vl_divisor)
		values(
			nr_sequencia_w, cd_empresa_w, cd_estabelecimento_w, dt_referencia_w,
			nr_seq_indicador_w, sysdate,
			nm_usuario_p, vl_indicador_w,
			vl_base_ww, vl_divisor_w);
		end;
	end loop;
	close c02;

	if	(ie_consol_empresa_w = 'S') then
		vl_base_consol_ww	:= vl_base_consol_w;
		vl_divisor_consol_w		:= vl_divisor_consol_w;
			
		if	(ie_anualizar_w = 'S') then
			vl_base_consol_w	:= dividir((vl_base_consol_w * 12), qt_mes_w);
		end if;

		vl_indicador_w			:= dividir(vl_base_consol_w, vl_divisor_consol_w);
		if	(ie_percentual_w = 'S') then
			vl_indicador_w	:= dividir(vl_base_consol_w  * 100, vl_divisor_consol_w);
		end if;
			
		insert into eis_valor_indic_contab(
			nr_sequencia,
			cd_empresa,
			cd_estabelecimento,
			dt_referencia,
			nr_seq_indicador,
			dt_atualizacao,
			nm_usuario,
			vl_indicador,
			vl_base,
			vl_divisor)
		values(	eis_valor_indic_contab_seq.nextval,
			cd_empresa_w,
			null,
			dt_referencia_w,
			nr_seq_indicador_w,
			sysdate,
			nm_usuario_p,
			vl_indicador_w,
			vl_base_consol_ww,
			vl_divisor_consol_w);
	end if;
	
	end;
end loop;
close c01;

commit;
end Gerar_EIS_Indicador_Contabil;
/