create or replace
procedure nut_cardapio_plan_todos( 	nr_sequencias_p 	varchar2,
					nr_seq_cardapio_dia_p	number,
					nm_usuario_p		Varchar2) is 


nr_sequencias_w		varchar2(2000);
nr_sequencia_w		number(10,0);
nr_pos_virgula_w	number(10,0);

begin
if	(nr_sequencias_p is not null) then
	begin
	
	delete
	FROM nut_pac_opcao_rec
	WHERE nr_seq_cardapio_dia = nr_seq_cardapio_dia_p;
	
	nr_sequencias_w	:= nr_sequencias_p;
	while (nr_sequencias_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(nr_sequencias_w,',');
		if	(nr_pos_virgula_w > 0) then
			begin
			nr_sequencia_w	:= to_number(substr(nr_sequencias_w,1,nr_pos_virgula_w-1));
			nr_sequencias_w	:= substr(nr_sequencias_w,nr_pos_virgula_w+1,length(nr_sequencias_w));
			if	(nr_sequencia_w > 0) then
				begin
				
				nut_cardapio_planejamento(nr_sequencia_w, nr_seq_cardapio_dia_p, nm_usuario_p);

				end;
			end if;
			end;
		else
			begin
			nr_sequencias_w	:= null;
			end;
		end if;
		end;
	end loop;
	end;
end if;
commit;

end nut_cardapio_plan_todos;
/