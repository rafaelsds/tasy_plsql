create or replace procedure pfcs_mod_tl_occupancy_cnt (
	nr_seq_indicator_p		  number,
	cd_estabelecimento_p	  number,
	nm_usuario_p			      varchar2
) is

-- Variables
pfcs_panel_seq_w					pfcs_panel.nr_sequencia%type;

qt_available_tl_w					number(20)    := 0; --> Available Beds
qt_total_in_use_w					number(20)    := 0; --> Occupied Beds
qt_total_brkn_pfcs_w				number(20)    := 0; --> Blocked Beds
qt_total_aux_w						number(20)    := 0; --> Total Capacity
qt_discharge_unit_w					number(20)    := 0; --> Discharge Count



begin
	qt_available_tl_w 		:= 	pfcs_func_tl_device_usage('Monitor', 'A', cd_estabelecimento_p);
   	qt_total_in_use_w 		:= 	pfcs_func_tl_device_usage('Monitor', 'U', cd_estabelecimento_p);
   	qt_total_brkn_pfcs_w 	:=  pfcs_func_tl_device_usage('Monitor', 'B_L_D', cd_estabelecimento_p);
	qt_total_aux_w 			:=  qt_available_tl_w + qt_total_in_use_w;

	select	count(*) into qt_discharge_unit_w
	from	pfcs_service_request sr,
			pfcs_encounter enc,
			pfcs_patient pat,
			pfcs_encounter_location el,
			pfcs_device pd,
			unidade_atendimento uni,
			setor_atendimento   sec
	where	sr.si_status = 'ACTIVE'
	and		sr.cd_service = 'E0403'
	and		sr.nr_seq_encounter = enc.nr_sequencia
	and		enc.si_status in ('PLANNED', 'ARRIVED')
	and		enc.nr_seq_patient = pat.nr_sequencia
	and		pat.ie_active = '1'
	and		pat.nr_sequencia = pd.nr_seq_patient
	and		pd.si_status = 'ACTIVE'
	and		pd.ds_device_type = 'Monitor'
	and 	el.nr_seq_encounter = enc.nr_sequencia
	and 	uni.nr_seq_location = el.nr_seq_location
	and   	uni.cd_setor_atendimento = sec.cd_setor_atendimento
	and 	sec.ie_situacao = 'A'
	and 	uni.ie_situacao = 'A'
	and 	sec.cd_classif_setor = '3'
	and		sec.cd_estabelecimento_base = cd_estabelecimento_p;

    pfcs_pck_v2.pfcs_generate_results(
		vl_indicator_p => qt_available_tl_w,
		vl_indicator_aux_p => qt_total_in_use_w,
		vl_indicator_help_p => qt_total_brkn_pfcs_w,
        vl_indicator_assist_p => qt_discharge_unit_w,
		vl_indicator_collab_p => qt_total_aux_w,
        nr_seq_indicator_p => nr_seq_indicator_p,
        nr_seq_operational_level_p => cd_estabelecimento_p,
        nm_usuario_p => nm_usuario_p,
        nr_seq_panel_p => pfcs_panel_seq_w);
	commit;
pfcs_pck_v2.pfcs_activate_records(
        nr_seq_indicator_p => nr_seq_indicator_p,
        nr_seq_operational_level_p => cd_estabelecimento_p,
        nm_usuario_p => nm_usuario_p);

end pfcs_mod_tl_occupancy_cnt;
/
