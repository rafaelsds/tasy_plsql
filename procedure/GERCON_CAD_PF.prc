CREATE OR REPLACE PROCEDURE gercon_cad_pf(nm_pessoa_fisica_p     VARCHAR2,
                                          nr_cartao_nac_sus_p    VARCHAR2,
                                          nr_telefone_paciente_p VARCHAR2,
                                          nr_cpf_p               VARCHAR2,
                                          dt_nascimento_p        DATE,
                                          ie_sexo_p              VARCHAR2,
                                          ie_estado_civil_p      VARCHAR2,
                                          nr_seq_cor_pele_p      NUMBER,
                                          cd_nacionalidade_p     VARCHAR2,
                                          ds_bairro_p            VARCHAR2,
                                          cd_cep_p               VARCHAR2,
                                          ds_endereco_p          VARCHAR2,
                                          nr_endereco_p          NUMBER,
                                          ds_complemento_p       VARCHAR2,
                                          ds_municipio_p         VARCHAR2,
                                          nm_contato_p           VARCHAR2,
                                          cd_pessoa_fisica_out_p OUT VARCHAR2,
                                          nm_pessoa_fisica_out_p OUT VARCHAR2,
                                          ie_novo_paciente_out_p OUT VARCHAR2,
                                          ie_erro_out_p          OUT VARCHAR2,
                                          ds_erro_out_p          OUT VARCHAR2) IS

BEGIN

    ie_erro_out_p          := 'N';
    ds_erro_out_p          := '';
    ie_novo_paciente_out_p := 'N';

    SELECT MAX(cd_pessoa_fisica),
           MAX(nm_pessoa_fisica)
      INTO cd_pessoa_fisica_out_p,
           nm_pessoa_fisica_out_p
      FROM pessoa_fisica
     WHERE nr_cpf = nr_cpf_p
        OR nr_cartao_nac_sus = nr_cartao_nac_sus_p;

    IF cd_pessoa_fisica_out_p IS NULL THEN

        nm_pessoa_fisica_out_p := nm_pessoa_fisica_p;
        ie_novo_paciente_out_p := 'S';
        SELECT pessoa_fisica_seq.nextval INTO cd_pessoa_fisica_out_p FROM dual;

        BEGIN
            INSERT INTO pessoa_fisica
                (cd_pessoa_fisica,
                 nm_pessoa_fisica,
                 dt_nascimento,
                 ie_sexo,
                 nr_ddd_celular,
                 nr_telefone_celular,
                 ie_revisar,
                 nr_cpf,
                 nr_cartao_nac_sus,
                 cd_nacionalidade,
                 nr_seq_cor_pele,
                 ie_estado_civil,
                 ie_tipo_pessoa,
                 dt_atualizacao,
                 dt_atualizacao_nrec,
                 nm_usuario,
                 nm_usuario_nrec)
            VALUES
                (cd_pessoa_fisica_out_p,
                 substr(nm_pessoa_fisica_p, 1, 60),
                 dt_nascimento_p,
                 ie_sexo_p,
                 obter_somente_numero(substr(nr_telefone_paciente_p, 1, 4)),
                 substr(nr_telefone_paciente_p, 5, 40),
                 'N',
                 nr_cpf_p,
                 nr_cartao_nac_sus_p,
                 obter_conversao_interna_int(NULL, 'NACIONALIDADE', 'CD_NACIONALIDADE', cd_nacionalidade_p, 'GERCON'),
                 obter_conversao_interna_int(NULL, 'COR_PELE', 'NR_SEQ_COR_PELE', nr_seq_cor_pele_p, 'GERCON'),
                 obter_conversao_interna_int(NULL, 'PESSOA_FISICA', 'IE_ESTADO_CIVIL', ie_estado_civil_p, 'GERCON'),
                 '2',
                 SYSDATE,
                 SYSDATE,
                 'GERCON',
                 'GERCON');
            COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
                cd_pessoa_fisica_out_p := NULL;
                nm_pessoa_fisica_out_p := NULL;
                ie_erro_out_p          := 'S';
                ds_erro_out_p          := obter_expressao_dic_objeto(1112607 ) || SQLERRM;
        END;

    END IF;

END;
/
