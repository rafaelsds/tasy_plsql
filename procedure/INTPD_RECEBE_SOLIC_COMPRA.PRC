create or replace
procedure intpd_recebe_solic_compra(	nr_sequencia_p	in	number,
					xml_p		in	xmltype) is 

ds_id_origin_w				intpd_eventos_sistema.ds_id_origin%type;
ie_conversao_w				intpd_eventos_sistema.ie_conversao%type;
nr_seq_sistema_w			intpd_eventos_sistema.nr_seq_sistema%type;
nr_seq_projeto_xml_w			intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_regra_conv_w			conversao_meio_externo.nr_seq_regra%type;
ie_sistema_externo_w			varchar2(15);
reg_integracao_w			gerar_int_padrao.reg_integracao_conv;
solic_compra_w				solic_compra%rowtype;
solic_compra_item_w			solic_compra_item%rowtype;
solic_compra_item_entrega_w		solic_compra_item_entrega%rowtype;
nr_item_solic_compra_w			solic_compra_item.nr_item_solic_compra%type;
nr_item_solic_compra_entr_w		solic_compra_item_entrega.nr_item_solic_compra_entr%type;
ie_erro_w				varchar2(1) := 'N';
nr_solic_compra_delete_w		solic_compra.nr_solic_compra%type;
qt_registros_w				number(10);	
	
cursor c01 is
select	*
from	xmltable('/STRUCTURE/PURCHASE' passing xml_p columns	
	ie_action				varchar2(15)			path 'IE_ACTION',
	DT_SOLICITACAO_COMPRA			DATE				path 'DT_SOLICITACAO_COMPRA',
	DT_BAIXA				DATE				path 'DT_BAIXA',
	DT_LIBERACAO				DATE				path 'DT_LIBERACAO',
	DT_IMPRESSAO				DATE				path 'DT_IMPRESSAO',
	DT_AUTORIZACAO				DATE				path 'DT_AUTORIZACAO',
	DT_PRE_APROVACAO			DATE				path 'DT_PRE_APROVACAO',
	DT_PRE_LIBERACAO			DATE				path 'DT_PRE_LIBERACAO',
	DT_RECEBIMENTO				DATE				path 'DT_RECEBIMENTO',
	DT_VENCIMENTO				DATE				path 'DT_VENCIMENTO',
	CD_ESTABELECIMENTO			NUMBER(4)			path 'CD_ESTABELECIMENTO',
	CD_PESSOA_SOLICITANTE			VARCHAR2(10) 			path 'CD_PESSOA_SOLICITANTE',
	CD_LOCAL_ESTOQUE			NUMBER(4)			path 'CD_LOCAL_ESTOQUE',
	CD_CENTRO_CUSTO				NUMBER(8)			path 'CD_CENTRO_CUSTO',
	CD_CONTA_CONTABIL			VARCHAR2(20)			path 'CD_CONTA_CONTABIL',
	CD_SETOR_ATENDIMENTO			NUMBER(5)			path 'CD_SETOR_ATENDIMENTO',
	DS_OBSERVACAO				VARCHAR2(4000)			path 'DS_OBSERVACAO',
	IE_AVISO_CHEGADA			VARCHAR2(1)			path 'IE_AVISO_CHEGADA',
	IE_AVISO_APROV_OC			VARCHAR2(1)			path 'IE_AVISO_APROV_OC',
	IE_URGENTE				VARCHAR2(1)			path 'IE_URGENTE',
	NR_DOC_EXTERNO_RECEB			VARCHAR2(40)			path 'NR_DOC_EXTERNO_RECEB',
	CD_COMPRADOR_RESP			VARCHAR2(10)			path 'CD_COMPRADOR_RESP',
	CD_MOTIVO_BAIXA				NUMBER(5)			path 'CD_MOTIVO_BAIXA',	
	CD_PESSOA_AUTORIZA			VARCHAR2(10)			path 'CD_PESSOA_AUTORIZA',
	NR_SEQ_ORDEM_SERV			NUMBER(10)			path 'NR_SEQ_ORDEM_SERV',
	NR_SEQ_MOTIVO_CANCEL			NUMBER(10)			path 'NR_SEQ_MOTIVO_CANCEL',
	IE_FORMA_EXPORTAR			VARCHAR2(1)			path 'IE_FORMA_EXPORTAR',
	NR_SEQ_PLANEJAMENTO			NUMBER(10) 			path 'NR_SEQ_PLANEJAMENTO',
	NR_SEQ_PROJ_GPI				NUMBER(10)			path 'NR_SEQ_PROJ_GPI',
	NR_SEQ_ETAPA_GPI			NUMBER(10)			path 'NR_SEQ_ETAPA_GPI',
	NR_SEQ_CONTA_GPI			NUMBER(10)			path 'NR_SEQ_CONTA_GPI',
	IE_TIPO_SOLICITACAO			VARCHAR2(1)			path 'IE_TIPO_SOLICITACAO',
	NR_SEQ_RESSUP_FORNEC			NUMBER(10)			path 'NR_SEQ_RESSUP_FORNEC',
	IE_COMODATO				VARCHAR2(1)			path 'IE_COMODATO',
	IE_SEMANAL				VARCHAR2(1)			path 'IE_SEMANAL',
	IE_TIPO_COMPRA				VARCHAR2(15)			path 'IE_TIPO_COMPRA',
	NR_SEQ_TIPO_COMPRA			NUMBER(10)			path 'NR_SEQ_TIPO_COMPRA',
	NR_SEQ_MOD_COMPRA			NUMBER(10)			path 'NR_SEQ_MOD_COMPRA',
	NR_SEQ_FORMA_COMPRA			NUMBER(10)			path 'NR_SEQ_FORMA_COMPRA',
	NR_SEQ_MOTIVO_SOLIC			NUMBER(10)			path 'NR_SEQ_MOTIVO_SOLIC',
	DS_JUSTIFICATIVA			VARCHAR2(2000)			path 'DS_JUSTIFICATIVA',
	NR_SOLIC_COMPRA_ORIGEM_LIC		NUMBER(10)			path 'NR_SOLIC_COMPRA_ORIGEM_LIC',
	NR_COT_COMPRA_ORIGEM_LIC		NUMBER(10)			path 'NR_COT_COMPRA_ORIGEM_LIC',
	CD_FORNEC_EXCLUSIVO			VARCHAR2(14)			path 'CD_FORNEC_EXCLUSIVO',
	CD_FORNEC_SUGERIDO			VARCHAR2(14)			path 'CD_FORNEC_SUGERIDO',
	NR_SEQ_MOTIVO_URGENTE			NUMBER(10)			path 'NR_SEQ_MOTIVO_URGENTE',
	NM_USUARIO_PRE_APROV			VARCHAR2(15)			path 'NM_USUARIO_PRE_APROV',
	NR_PRESCRICAO				NUMBER(14)			path 'NR_PRESCRICAO',
	NR_SEQ_PROT_SOLIC			NUMBER(10)			path 'NR_SEQ_PROT_SOLIC',
	NR_SEQ_ORCAMENTO_GPI			NUMBER(10)			path 'NR_SEQ_ORCAMENTO_GPI',
	NM_USUARIO_PRE_LIB			VARCHAR2(15)			path 'NM_USUARIO_PRE_LIB',
	NM_USUARIO_LIB				VARCHAR2(15)			path 'NM_USUARIO_LIB',
	NR_SEQ_GRUPO_RESP_COMPRA		NUMBER(10)			path 'NR_SEQ_GRUPO_RESP_COMPRA',
	NR_LOTE_PRODUCAO			NUMBER(10)			path 'NR_LOTE_PRODUCAO',
	IE_TIPO_SERVICO				VARCHAR2(15)			path 'IE_TIPO_SERVICO',
	IE_STATUS_ENVIO				VARCHAR2(15)			path 'IE_STATUS_ENVIO',
	NM_USUARIO_VER_PRE_LIB			VARCHAR2(15)			path 'NM_USUARIO_VER_PRE_LIB',
	CD_PERFIL				NUMBER(5)			path 'CD_PERFIL',
	CD_PESSOA_FISICA			VARCHAR2(10)			path 'CD_PESSOA_FISICA',
	NM_USUARIO_RECEB			VARCHAR2(15)			path 'NM_USUARIO_RECEB',
	CD_SETOR_ENTREGA			NUMBER(5)			path 'CD_SETOR_ENTREGA',
	CD_BANCO				NUMBER(5)			path 'CD_BANCO',
	CD_AGENCIA_BANCARIA			VARCHAR2(8)			path 'CD_AGENCIA_BANCARIA',
	NR_CONTA				VARCHAR2(8)			path 'NR_CONTA',
	NR_DIGITO_CONTA				VARCHAR2(2)			path 'NR_DIGITO_CONTA',
	NR_NOTA_FISCAL				VARCHAR2(255)			path 'NR_NOTA_FISCAL',
	IE_ORCADO				VARCHAR2(1)			path 'IE_ORCADO',
	NM_USUARIO				VARCHAR2(15)			path 'NM_USUARIO',
	xml_solic_itens				xmltype 			path 'ITENS');
			
c01_w	c01%rowtype;		
		
cursor c02 is		
select	*		
from	xmltable('/ITENS/ITEM' passing c01_w.xml_solic_itens columns		
	NR_ITEM_SOLIC_COMPRA			NUMBER(5)			path 'NR_ITEM_SOLIC_COMPRA',
	CD_MATERIAL				NUMBER(6)			path 'CD_MATERIAL',
	CD_UNIDADE_MEDIDA_COMPRA		VARCHAR2(30)			path 'CD_UNIDADE_MEDIDA_COMPRA',
	QT_MATERIAL				NUMBER(13,4)			path 'QT_MATERIAL',
	IE_SITUACAO				VARCHAR2(1)			path 'IE_SITUACAO',
	DS_MATERIAL_DIRETO			VARCHAR2(255)			path 'DS_MATERIAL_DIRETO',
	DS_OBSERVACAO				VARCHAR2(255)			path 'DS_OBSERVACAO',
	NR_COT_COMPRA				NUMBER(10)			path 'NR_COT_COMPRA',
	NR_ITEM_COT_COMPRA			NUMBER(5)			path 'NR_ITEM_COT_COMPRA',
	CD_MOTIVO_BAIXA				NUMBER(5)			path 'CD_MOTIVO_BAIXA',
	DT_BAIXA				DATE				path 'DT_BAIXA',
	DT_SOLIC_ITEM				DATE				path 'DT_SOLIC_ITEM',
	NR_SEQ_APROVACAO			NUMBER(10)			path 'NR_SEQ_APROVACAO',
	DT_AUTORIZACAO				DATE				path 'DT_AUTORIZACAO',
	VL_UNIT_PREVISTO			NUMBER(17,4)			path 'VL_UNIT_PREVISTO',
	IE_GERACAO				VARCHAR2(1)			path 'IE_GERACAO',
	NR_SEQ_PROJ_REC				NUMBER(10)			path 'NR_SEQ_PROJ_REC',
	DT_REPROVACAO				DATE				path 'DT_REPROVACAO',
	NR_SEQ_MOTIVO_CANCEL			NUMBER(10)			path 'NR_SEQ_MOTIVO_CANCEL',
	IE_FORMA_EXPORTAR			VARCHAR2(1)			path 'IE_FORMA_EXPORTAR',
	NR_SOLIC_COMPRA_REF			NUMBER(10)			path 'NR_SOLIC_COMPRA_REF',
	NR_ITEM_SOLIC_COMPRA_REF		NUMBER(5)			path 'NR_ITEM_SOLIC_COMPRA_REF',
	CD_CONTA_CONTABIL			VARCHAR2(20)			path 'CD_CONTA_CONTABIL',
	DS_OBSERVACAO_INT			VARCHAR2(255)			path 'DS_OBSERVACAO_INT',
	NR_CONTRATO				NUMBER(10)			path 'NR_CONTRATO',
	IE_BULA					VARCHAR2(1)			path 'IE_BULA',
	IE_AMOSTRA				VARCHAR2(1)			path 'IE_AMOSTRA',
	IE_CATALOGO				VARCHAR2(1)			path 'IE_CATALOGO',
	NR_SEQ_ORC_ITEM_GPI			NUMBER(10)			path 'NR_SEQ_ORC_ITEM_GPI',
	QT_CONV_COMPRA_EST_ORIG			NUMBER(13,4)			path 'QT_CONV_COMPRA_EST_ORIG',
	NR_SEQ_MARCA				NUMBER(10)			path 'NR_SEQ_MARCA',
	NR_SEQ_REGRA_CONTRATO			NUMBER(10)			path 'NR_SEQ_REGRA_CONTRATO',
	QT_SALDO_DISP_ESTOQUE			NUMBER(13,4)			path 'QT_SALDO_DISP_ESTOQUE',
	QT_CONSUMO_MENSAL			NUMBER(15,4)			path 'QT_CONSUMO_MENSAL',
	DT_DESDOBR_APROV			DATE				path 'DT_DESDOBR_APROV',
	NR_SEQ_LOTE_FORNEC			NUMBER(10)			path 'NR_SEQ_LOTE_FORNEC',
	NR_SEQ_LICITACAO			NUMBER(10)			path 'NR_SEQ_LICITACAO',
	NR_SEQ_LIC_ITEM				NUMBER(10)			path 'NR_SEQ_LIC_ITEM',
	IE_MOTIVO_REPROVACAO			VARCHAR2(15)			path 'IE_MOTIVO_REPROVACAO',
	DS_JUSTIFICATIVA_REPROV			VARCHAR2(255)			path 'DS_JUSTIFICATIVA_REPROV',
	NR_REQUISICAO				NUMBER(10)			path 'NR_REQUISICAO',
	NR_SEQ_ITEM_REQUISICAO			NUMBER(5)			path 'NR_SEQ_ITEM_REQUISICAO',
	VL_CUSTO_TOTAL				NUMBER(17,4)			path 'VL_CUSTO_TOTAL',
	VL_ESTOQUE_TOTAL			NUMBER(17,4)			path 'VL_ESTOQUE_TOTAL',
	NR_ORDEM_COMPRA_ORIG			NUMBER(10)			path 'NR_ORDEM_COMPRA_ORIG',
	CD_PERFIL_APROV				NUMBER(5)			path 'CD_PERFIL_APROV',
	NR_SEQ_CRITERIO_RATEIO			NUMBER(10) 			path 'NR_SEQ_CRITERIO_RATEIO',
	DS_JUSTIF_DIVER				VARCHAR2(4000)			path 'DS_JUSTIF_DIVER',
	QT_DIAS_ESTOQUE				NUMBER(13,4)			path 'QT_DIAS_ESTOQUE',
	CD_CNPJ					VARCHAR2(14)			path 'CD_CNPJ',
	IE_CANCELAMENTO_INTEGR			VARCHAR2(1)			path 'IE_CANCELAMENTO_INTEGR',
	NR_SEQ_NORMA_COMPRA			NUMBER(10)			path 'NR_SEQ_NORMA_COMPRA',
	NR_SEQ_PROC_APROV			NUMBER(10) 			path 'NR_SEQ_PROC_APROV',
	IE_SERVICO_REALIZADO			VARCHAR2(15)			path 'IE_SERVICO_REALIZADO',
	DS_MARCAS_ACEITAVEIS			VARCHAR2(2000) 			path 'DS_MARCAS_ACEITAVEIS',
	VL_ORCADO_GPI				NUMBER(17,4) 			path 'VL_ORCADO_GPI',
	DT_SOLIC_ITEM_ANT			DATE				path 'DT_SOLIC_ITEM_ANT',
	NR_SEQ_JUSTIF				NUMBER(10)			path 'NR_SEQ_JUSTIF',
	NR_SEQ_ORDEM_SERV			NUMBER(10)			path 'NR_SEQ_ORDEM_SERV',
	NR_SEQ_CONTA_FINANC			NUMBER(10)			path 'NR_SEQ_CONTA_FINANC',
	QT_AMOSTRA				NUMBER(13,4)			path 'QT_AMOSTRA',
	NR_SEQ_REG_PCS				NUMBER(10)			path 'NR_SEQ_REG_PCS',
	NR_SEQ_REG_COMPRA			NUMBER(10)			path 'NR_SEQ_REG_COMPRA',
	NR_SEQ_OP_COMP_OPM			NUMBER(10)			path 'NR_SEQ_OP_COMP_OPM',
	NR_ITEM_OCI_ORIG			NUMBER(5)			path 'NR_ITEM_OCI_ORIG',
	QT_SALDO_REG_PRECO			NUMBER(13,4)			path 'QT_SALDO_REG_PRECO',
	QT_MATERIAL_CANCELADO			NUMBER(13,4)			path 'QT_MATERIAL_CANCELADO',
	xml_solic_item_entrega			xmltype 			path 'ENTREGAS');	
	
c02_w	c02%rowtype;
	
cursor c03 is
select	*
from	xmltable('/ENTREGAS/ENTREGA' passing c02_w.xml_solic_item_entrega columns		
	NR_ITEM_SOLIC_COMPRA			NUMBER(5)			path 'NR_ITEM_SOLIC_COMPRA',
	NR_ITEM_SOLIC_COMPRA_ENTR		NUMBER(5)			path 'NR_ITEM_SOLIC_COMPRA_ENTR',
	QT_ENTREGA_SOLICITADA			NUMBER(13,4)			path 'QT_ENTREGA_SOLICITADA',
	DT_ENTREGA_SOLICITADA			DATE				path 'DT_ENTREGA_SOLICITADA',
	DS_OBSERVACAO				VARCHAR2(255)			path 'DS_OBSERVACAO');
	
c03_w	c03%rowtype;	
		
begin

select	id_origin
into	ds_id_origin_w
from	xmltable('/STRUCTURE' passing xml_p
	columns id_origin	varchar2(10) path 'ID_ORIGIN');

select	nvl(b.ie_conversao,'I'),
	nr_seq_sistema,
	nr_seq_projeto_xml,
	nr_seq_regra_conv,
	nvl(ds_id_origin,ds_id_origin_w)
into	ie_conversao_w,
	nr_seq_sistema_w,
	nr_seq_projeto_xml_w,
	nr_seq_regra_conv_w,
	ds_id_origin_w
from	intpd_fila_transmissao a,
	intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and	a.nr_sequencia = nr_sequencia_p;

ie_sistema_externo_w				:=	nr_seq_sistema_w;

reg_integracao_w.nr_seq_fila_transmissao	:=	nr_sequencia_p;
reg_integracao_w.ie_envio_recebe		:=	'R';
reg_integracao_w.ie_sistema_externo		:=	ie_sistema_externo_w;
reg_integracao_w.ie_conversao			:=	ie_conversao_w;
reg_integracao_w.nr_seq_projeto_xml		:=	nr_seq_projeto_xml_w;
reg_integracao_w.nr_seq_regra_conversao		:=	nr_seq_regra_conv_w;

open c01;
loop
fetch c01 into	
	c01_w;
exit when c01%notfound;
	begin	
	ie_erro_w := 'N';

	if	(c01_w.ie_action = 'DELETE') then	
		select	nvl(max(nr_solic_compra),0)
		into	nr_solic_compra_delete_w
		from	solic_compra
		where	nr_doc_externo_receb = c01_w.nr_doc_externo_receb;
	
		if	(nr_solic_compra_delete_w > 0) then
			select 	sum(qt)
			into	qt_registros_w
			from	(select	count(*) qt
				from	cot_compra_item
				where	nr_solic_compra = nr_solic_compra_delete_w
				union all
				select	count(*) qt
				from	cot_compra_solic_agrup
				where	nr_solic_compra = nr_solic_compra_delete_w
				union all
				select	count(*) qt
				from	ordem_compra_item
				where	nr_solic_compra = nr_solic_compra_delete_w);
			
			if	(qt_registros_w = 0) then			
				delete from solic_compra where nr_solic_compra = nr_solic_compra_delete_w;
			else
				ie_erro_w := 'S';
				intpd_gravar_log_recebimento(nr_sequencia_p, wheb_mensagem_pck.get_Texto(1027477), c01_w.nm_usuario);				
			end if;	
		end if;	
	else
		reg_integracao_w.nm_tabela		:=	'SOLIC_COMPRA';
		reg_integracao_w.nm_elemento		:=	'PURCHASE';
		reg_integracao_w.nr_seq_visao		:=	7303;
		
		intpd_processar_atributo(reg_integracao_w, 'DT_SOLICITACAO_COMPRA', c01_w.DT_SOLICITACAO_COMPRA, 'S', solic_compra_w.DT_SOLICITACAO_COMPRA);
		intpd_processar_atributo(reg_integracao_w, 'DT_BAIXA', c01_w.DT_BAIXA, 'S', solic_compra_w.DT_BAIXA);
		intpd_processar_atributo(reg_integracao_w, 'DT_LIBERACAO', c01_w.DT_LIBERACAO, 'S', solic_compra_w.DT_LIBERACAO);
		intpd_processar_atributo(reg_integracao_w, 'DT_IMPRESSAO', c01_w.DT_IMPRESSAO, 'S', solic_compra_w.DT_IMPRESSAO);
		intpd_processar_atributo(reg_integracao_w, 'DT_AUTORIZACAO', c01_w.DT_AUTORIZACAO, 'S', solic_compra_w.DT_AUTORIZACAO);
		intpd_processar_atributo(reg_integracao_w, 'DT_PRE_APROVACAO', c01_w.DT_PRE_APROVACAO, 'S', solic_compra_w.DT_PRE_APROVACAO);
		intpd_processar_atributo(reg_integracao_w, 'DT_PRE_LIBERACAO', c01_w.DT_PRE_LIBERACAO, 'S', solic_compra_w.DT_PRE_LIBERACAO);
		intpd_processar_atributo(reg_integracao_w, 'DT_RECEBIMENTO', c01_w.DT_RECEBIMENTO, 'S', solic_compra_w.DT_RECEBIMENTO);
		intpd_processar_atributo(reg_integracao_w, 'DT_VENCIMENTO', c01_w.DT_VENCIMENTO, 'S', solic_compra_w.DT_VENCIMENTO);
		intpd_processar_atributo(reg_integracao_w, 'CD_ESTABELECIMENTO', c01_w.CD_ESTABELECIMENTO, 'S', solic_compra_w.CD_ESTABELECIMENTO);
		intpd_processar_atributo(reg_integracao_w, 'CD_PESSOA_SOLICITANTE', c01_w.CD_PESSOA_SOLICITANTE, 'S', solic_compra_w.CD_PESSOA_SOLICITANTE);
		intpd_processar_atributo(reg_integracao_w, 'CD_LOCAL_ESTOQUE', c01_w.CD_LOCAL_ESTOQUE, 'S', solic_compra_w.CD_LOCAL_ESTOQUE);
		intpd_processar_atributo(reg_integracao_w, 'CD_CENTRO_CUSTO', c01_w.CD_CENTRO_CUSTO, 'S', solic_compra_w.CD_CENTRO_CUSTO);
		intpd_processar_atributo(reg_integracao_w, 'CD_CONTA_CONTABIL', c01_w.CD_CONTA_CONTABIL, 'S', solic_compra_w.CD_CONTA_CONTABIL);
		intpd_processar_atributo(reg_integracao_w, 'CD_SETOR_ATENDIMENTO', c01_w.CD_SETOR_ATENDIMENTO, 'S', solic_compra_w.CD_SETOR_ATENDIMENTO);
		intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c01_w.DS_OBSERVACAO, 'S', solic_compra_w.DS_OBSERVACAO);
		intpd_processar_atributo(reg_integracao_w, 'IE_AVISO_CHEGADA', c01_w.IE_AVISO_CHEGADA, 'S', solic_compra_w.IE_AVISO_CHEGADA);
		intpd_processar_atributo(reg_integracao_w, 'IE_AVISO_APROV_OC', c01_w.IE_AVISO_APROV_OC, 'S', solic_compra_w.IE_AVISO_APROV_OC);
		intpd_processar_atributo(reg_integracao_w, 'IE_URGENTE', c01_w.IE_URGENTE, 'S', solic_compra_w.IE_URGENTE);
		intpd_processar_atributo(reg_integracao_w, 'CD_COMPRADOR_RESP', c01_w.CD_COMPRADOR_RESP, 'S', solic_compra_w.CD_COMPRADOR_RESP);
		intpd_processar_atributo(reg_integracao_w, 'CD_MOTIVO_BAIXA', c01_w.CD_MOTIVO_BAIXA, 'S', solic_compra_w.CD_MOTIVO_BAIXA);
		intpd_processar_atributo(reg_integracao_w, 'CD_PESSOA_AUTORIZA', c01_w.CD_PESSOA_AUTORIZA, 'S', solic_compra_w.CD_PESSOA_AUTORIZA);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_ORDEM_SERV', c01_w.NR_SEQ_ORDEM_SERV, 'S', solic_compra_w.NR_SEQ_ORDEM_SERV);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MOTIVO_CANCEL', c01_w.NR_SEQ_MOTIVO_CANCEL, 'S', solic_compra_w.NR_SEQ_MOTIVO_CANCEL);
		intpd_processar_atributo(reg_integracao_w, 'IE_FORMA_EXPORTAR', c01_w.IE_FORMA_EXPORTAR, 'S', solic_compra_w.IE_FORMA_EXPORTAR);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_PLANEJAMENTO', c01_w.NR_SEQ_PLANEJAMENTO, 'S', solic_compra_w.NR_SEQ_PLANEJAMENTO);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_PROJ_GPI', c01_w.NR_SEQ_PROJ_GPI, 'S', solic_compra_w.NR_SEQ_PROJ_GPI);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_ETAPA_GPI', c01_w.NR_SEQ_ETAPA_GPI, 'S', solic_compra_w.NR_SEQ_ETAPA_GPI);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CONTA_GPI', c01_w.NR_SEQ_CONTA_GPI, 'S', solic_compra_w.NR_SEQ_CONTA_GPI);
		intpd_processar_atributo(reg_integracao_w, 'IE_TIPO_SOLICITACAO', c01_w.IE_TIPO_SOLICITACAO, 'S', solic_compra_w.IE_TIPO_SOLICITACAO);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_RESSUP_FORNEC', c01_w.NR_SEQ_RESSUP_FORNEC, 'S', solic_compra_w.NR_SEQ_RESSUP_FORNEC);
		intpd_processar_atributo(reg_integracao_w, 'IE_COMODATO', c01_w.IE_COMODATO, 'S', solic_compra_w.IE_COMODATO);
		intpd_processar_atributo(reg_integracao_w, 'IE_SEMANAL', c01_w.IE_SEMANAL, 'S', solic_compra_w.IE_SEMANAL);
		intpd_processar_atributo(reg_integracao_w, 'IE_TIPO_COMPRA', c01_w.IE_TIPO_COMPRA, 'S', solic_compra_w.IE_TIPO_COMPRA);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_TIPO_COMPRA', c01_w.NR_SEQ_TIPO_COMPRA, 'S', solic_compra_w.NR_SEQ_TIPO_COMPRA);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MOD_COMPRA', c01_w.NR_SEQ_MOD_COMPRA, 'S', solic_compra_w.NR_SEQ_MOD_COMPRA);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_FORMA_COMPRA', c01_w.NR_SEQ_FORMA_COMPRA, 'S', solic_compra_w.NR_SEQ_FORMA_COMPRA);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MOTIVO_SOLIC', c01_w.NR_SEQ_MOTIVO_SOLIC, 'S', solic_compra_w.NR_SEQ_MOTIVO_SOLIC);
		intpd_processar_atributo(reg_integracao_w, 'DS_JUSTIFICATIVA', c01_w.DS_JUSTIFICATIVA, 'S', solic_compra_w.DS_JUSTIFICATIVA);
		intpd_processar_atributo(reg_integracao_w, 'NR_SOLIC_COMPRA_ORIGEM_LIC', c01_w.NR_SOLIC_COMPRA_ORIGEM_LIC, 'S', solic_compra_w.NR_SOLIC_COMPRA_ORIGEM_LIC);
		intpd_processar_atributo(reg_integracao_w, 'NR_COT_COMPRA_ORIGEM_LIC', c01_w.NR_COT_COMPRA_ORIGEM_LIC, 'S', solic_compra_w.NR_COT_COMPRA_ORIGEM_LIC);
		intpd_processar_atributo(reg_integracao_w, 'CD_FORNEC_EXCLUSIVO', c01_w.CD_FORNEC_EXCLUSIVO, 'S', solic_compra_w.CD_FORNEC_EXCLUSIVO);
		intpd_processar_atributo(reg_integracao_w, 'CD_FORNEC_SUGERIDO', c01_w.CD_FORNEC_SUGERIDO, 'S', solic_compra_w.CD_FORNEC_SUGERIDO);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MOTIVO_URGENTE', c01_w.NR_SEQ_MOTIVO_URGENTE, 'S', solic_compra_w.NR_SEQ_MOTIVO_URGENTE);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO_PRE_APROV', c01_w.NM_USUARIO_PRE_APROV, 'S', solic_compra_w.NM_USUARIO_PRE_APROV);
		intpd_processar_atributo(reg_integracao_w, 'NR_PRESCRICAO', c01_w.NR_PRESCRICAO, 'S', solic_compra_w.NR_PRESCRICAO);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_PROT_SOLIC', c01_w.NR_SEQ_PROT_SOLIC, 'S', solic_compra_w.NR_SEQ_PROT_SOLIC);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_ORCAMENTO_GPI', c01_w.NR_SEQ_ORCAMENTO_GPI, 'S', solic_compra_w.NR_SEQ_ORCAMENTO_GPI);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO_PRE_LIB', c01_w.NM_USUARIO_PRE_LIB, 'S', solic_compra_w.NM_USUARIO_PRE_LIB);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO_LIB', c01_w.NM_USUARIO_LIB, 'S', solic_compra_w.NM_USUARIO_LIB);
		intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_GRUPO_RESP_COMPRA', c01_w.NR_SEQ_GRUPO_RESP_COMPRA, 'S', solic_compra_w.NR_SEQ_GRUPO_RESP_COMPRA);
		intpd_processar_atributo(reg_integracao_w, 'NR_LOTE_PRODUCAO', c01_w.NR_LOTE_PRODUCAO, 'S', solic_compra_w.NR_LOTE_PRODUCAO);
		intpd_processar_atributo(reg_integracao_w, 'IE_TIPO_SERVICO', c01_w.IE_TIPO_SERVICO, 'S', solic_compra_w.IE_TIPO_SERVICO);
		intpd_processar_atributo(reg_integracao_w, 'IE_STATUS_ENVIO', c01_w.IE_STATUS_ENVIO, 'S', solic_compra_w.IE_STATUS_ENVIO);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO_VER_PRE_LIB', c01_w.NM_USUARIO_VER_PRE_LIB, 'S', solic_compra_w.NM_USUARIO_VER_PRE_LIB);
		intpd_processar_atributo(reg_integracao_w, 'CD_PERFIL', c01_w.CD_PERFIL, 'S', solic_compra_w.CD_PERFIL);
		intpd_processar_atributo(reg_integracao_w, 'CD_PESSOA_FISICA', c01_w.CD_PESSOA_FISICA, 'S', solic_compra_w.CD_PESSOA_FISICA);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO_RECEB', c01_w.NM_USUARIO_RECEB, 'S', solic_compra_w.NM_USUARIO_RECEB);
		intpd_processar_atributo(reg_integracao_w, 'CD_SETOR_ENTREGA', c01_w.CD_SETOR_ENTREGA, 'S', solic_compra_w.CD_SETOR_ENTREGA);
		intpd_processar_atributo(reg_integracao_w, 'CD_BANCO', c01_w.CD_BANCO, 'S', solic_compra_w.CD_BANCO);
		intpd_processar_atributo(reg_integracao_w, 'CD_AGENCIA_BANCARIA', c01_w.CD_AGENCIA_BANCARIA, 'S', solic_compra_w.CD_AGENCIA_BANCARIA);
		intpd_processar_atributo(reg_integracao_w, 'NR_CONTA', c01_w.NR_CONTA, 'S', solic_compra_w.NR_CONTA);
		intpd_processar_atributo(reg_integracao_w, 'NR_DIGITO_CONTA', c01_w.NR_DIGITO_CONTA, 'S', solic_compra_w.NR_DIGITO_CONTA);
		intpd_processar_atributo(reg_integracao_w, 'NR_NOTA_FISCAL', c01_w.NR_NOTA_FISCAL, 'S', solic_compra_w.NR_NOTA_FISCAL);
		intpd_processar_atributo(reg_integracao_w, 'IE_ORCADO', c01_w.IE_ORCADO, 'S', solic_compra_w.IE_ORCADO);
		intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO', c01_w.NM_USUARIO, 'S', solic_compra_w.NM_USUARIO);
		intpd_processar_atributo(reg_integracao_w, 'NR_DOC_EXTERNO_RECEB', c01_w.NR_DOC_EXTERNO_RECEB, 'S', solic_compra_w.NR_DOC_EXTERNO_RECEB);
		solic_compra_w.dt_atualizacao := sysdate;
		solic_compra_w.dt_atualizacao_nrec := sysdate;
		solic_compra_w.nm_usuario_nrec := solic_compra_w.nm_usuario;
	
		if	(reg_integracao_w.qt_reg_log = 0) then
			select	nvl(max(nr_solic_compra),0)
			into	solic_compra_w.nr_solic_compra
			from	solic_compra
			where	nr_doc_externo_receb = solic_compra_w.nr_doc_externo_receb;
			
			if (solic_compra_w.nr_solic_compra > 0) then
				update	solic_compra
				set	row = solic_compra_w
				where	nr_solic_compra = solic_compra_w.nr_solic_compra;
			else
				select	solic_compra_seq.nextval
				into	solic_compra_w.nr_solic_compra
				from	dual;
			
				insert into solic_compra values solic_compra_w;
			end if;
		end if;
		
		open c02;
		loop
		fetch c02 into	
			c02_w;
		exit when c02%notfound;
			begin
			reg_integracao_w.nm_tabela		:=	'SOLIC_COMPRA_ITEM';
			reg_integracao_w.nm_elemento		:=	'ITEM';
			reg_integracao_w.nr_seq_visao		:=	16015;			
			intpd_processar_atributo(reg_integracao_w, 'NR_ITEM_SOLIC_COMPRA', c02_w.NR_ITEM_SOLIC_COMPRA, 'S', solic_compra_item_w.NR_ITEM_SOLIC_COMPRA);
			intpd_processar_atributo(reg_integracao_w, 'CD_MATERIAL', c02_w.CD_MATERIAL, 'S', solic_compra_item_w.CD_MATERIAL);
			intpd_processar_atributo(reg_integracao_w, 'CD_UNIDADE_MEDIDA_COMPRA', c02_w.CD_UNIDADE_MEDIDA_COMPRA, 'S', solic_compra_item_w.CD_UNIDADE_MEDIDA_COMPRA);
			intpd_processar_atributo(reg_integracao_w, 'QT_MATERIAL', c02_w.QT_MATERIAL, 'S', solic_compra_item_w.QT_MATERIAL);
			intpd_processar_atributo(reg_integracao_w, 'IE_SITUACAO', c02_w.IE_SITUACAO, 'S', solic_compra_item_w.IE_SITUACAO);
			intpd_processar_atributo(reg_integracao_w, 'DS_MATERIAL_DIRETO', c02_w.DS_MATERIAL_DIRETO, 'S', solic_compra_item_w.DS_MATERIAL_DIRETO);
			intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c02_w.DS_OBSERVACAO, 'S', solic_compra_item_w.DS_OBSERVACAO);
			intpd_processar_atributo(reg_integracao_w, 'NR_COT_COMPRA', c02_w.NR_COT_COMPRA, 'S', solic_compra_item_w.NR_COT_COMPRA);
			intpd_processar_atributo(reg_integracao_w, 'NR_ITEM_COT_COMPRA', c02_w.NR_ITEM_COT_COMPRA, 'S', solic_compra_item_w.NR_ITEM_COT_COMPRA);
			intpd_processar_atributo(reg_integracao_w, 'CD_MOTIVO_BAIXA', c02_w.CD_MOTIVO_BAIXA, 'S', solic_compra_item_w.CD_MOTIVO_BAIXA);
			intpd_processar_atributo(reg_integracao_w, 'DT_BAIXA', c02_w.DT_BAIXA, 'S', solic_compra_item_w.DT_BAIXA);
			intpd_processar_atributo(reg_integracao_w, 'DT_SOLIC_ITEM', c02_w.DT_SOLIC_ITEM, 'S', solic_compra_item_w.DT_SOLIC_ITEM);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_APROVACAO', c02_w.NR_SEQ_APROVACAO, 'S', solic_compra_item_w.NR_SEQ_APROVACAO);
			intpd_processar_atributo(reg_integracao_w, 'DT_AUTORIZACAO', c02_w.DT_AUTORIZACAO, 'S', solic_compra_item_w.DT_AUTORIZACAO);
			intpd_processar_atributo(reg_integracao_w, 'VL_UNIT_PREVISTO', c02_w.VL_UNIT_PREVISTO, 'S', solic_compra_item_w.VL_UNIT_PREVISTO);
			intpd_processar_atributo(reg_integracao_w, 'IE_GERACAO', c02_w.IE_GERACAO, 'S', solic_compra_item_w.IE_GERACAO);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_PROJ_REC', c02_w.NR_SEQ_PROJ_REC, 'S', solic_compra_item_w.NR_SEQ_PROJ_REC);
			intpd_processar_atributo(reg_integracao_w, 'DT_REPROVACAO', c02_w.DT_REPROVACAO, 'S', solic_compra_item_w.DT_REPROVACAO);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MOTIVO_CANCEL', c02_w.NR_SEQ_MOTIVO_CANCEL, 'S', solic_compra_item_w.NR_SEQ_MOTIVO_CANCEL);
			intpd_processar_atributo(reg_integracao_w, 'IE_FORMA_EXPORTAR', c02_w.IE_FORMA_EXPORTAR, 'S', solic_compra_item_w.IE_FORMA_EXPORTAR);
			intpd_processar_atributo(reg_integracao_w, 'NR_SOLIC_COMPRA_REF', c02_w.NR_SOLIC_COMPRA_REF, 'S', solic_compra_item_w.NR_SOLIC_COMPRA_REF);
			intpd_processar_atributo(reg_integracao_w, 'NR_ITEM_SOLIC_COMPRA_REF', c02_w.NR_ITEM_SOLIC_COMPRA_REF, 'S', solic_compra_item_w.NR_ITEM_SOLIC_COMPRA_REF);
			intpd_processar_atributo(reg_integracao_w, 'CD_CONTA_CONTABIL', c02_w.CD_CONTA_CONTABIL, 'S', solic_compra_item_w.CD_CONTA_CONTABIL);
			intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO_INT', c02_w.DS_OBSERVACAO_INT, 'S', solic_compra_item_w.DS_OBSERVACAO_INT);
			intpd_processar_atributo(reg_integracao_w, 'NR_CONTRATO', c02_w.NR_CONTRATO, 'S', solic_compra_item_w.NR_CONTRATO);
			intpd_processar_atributo(reg_integracao_w, 'IE_BULA', c02_w.IE_BULA, 'S', solic_compra_item_w.IE_BULA);
			intpd_processar_atributo(reg_integracao_w, 'IE_AMOSTRA', c02_w.IE_AMOSTRA, 'S', solic_compra_item_w.IE_AMOSTRA);
			intpd_processar_atributo(reg_integracao_w, 'IE_CATALOGO', c02_w.IE_CATALOGO, 'S', solic_compra_item_w.IE_CATALOGO);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_ORC_ITEM_GPI', c02_w.NR_SEQ_ORC_ITEM_GPI, 'S', solic_compra_item_w.NR_SEQ_ORC_ITEM_GPI);
			intpd_processar_atributo(reg_integracao_w, 'QT_CONV_COMPRA_EST_ORIG', c02_w.QT_CONV_COMPRA_EST_ORIG, 'S', solic_compra_item_w.QT_CONV_COMPRA_EST_ORIG);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_MARCA', c02_w.NR_SEQ_MARCA, 'S', solic_compra_item_w.NR_SEQ_MARCA);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_REGRA_CONTRATO', c02_w.NR_SEQ_REGRA_CONTRATO, 'S', solic_compra_item_w.NR_SEQ_REGRA_CONTRATO);
			intpd_processar_atributo(reg_integracao_w, 'QT_SALDO_DISP_ESTOQUE', c02_w.QT_SALDO_DISP_ESTOQUE, 'S', solic_compra_item_w.QT_SALDO_DISP_ESTOQUE);
			intpd_processar_atributo(reg_integracao_w, 'QT_CONSUMO_MENSAL', c02_w.QT_CONSUMO_MENSAL, 'S', solic_compra_item_w.QT_CONSUMO_MENSAL);
			intpd_processar_atributo(reg_integracao_w, 'DT_DESDOBR_APROV', c02_w.DT_DESDOBR_APROV, 'S', solic_compra_item_w.DT_DESDOBR_APROV);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_LOTE_FORNEC', c02_w.NR_SEQ_LOTE_FORNEC, 'S', solic_compra_item_w.NR_SEQ_LOTE_FORNEC);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_LICITACAO', c02_w.NR_SEQ_LICITACAO, 'S', solic_compra_item_w.NR_SEQ_LICITACAO);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_LIC_ITEM', c02_w.NR_SEQ_LIC_ITEM, 'S', solic_compra_item_w.NR_SEQ_LIC_ITEM);
			intpd_processar_atributo(reg_integracao_w, 'IE_MOTIVO_REPROVACAO', c02_w.IE_MOTIVO_REPROVACAO, 'S', solic_compra_item_w.IE_MOTIVO_REPROVACAO);
			intpd_processar_atributo(reg_integracao_w, 'DS_JUSTIFICATIVA_REPROV', c02_w.DS_JUSTIFICATIVA_REPROV, 'S', solic_compra_item_w.DS_JUSTIFICATIVA_REPROV);
			intpd_processar_atributo(reg_integracao_w, 'NR_REQUISICAO', c02_w.NR_REQUISICAO, 'S', solic_compra_item_w.NR_REQUISICAO);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_ITEM_REQUISICAO', c02_w.NR_SEQ_ITEM_REQUISICAO, 'S', solic_compra_item_w.NR_SEQ_ITEM_REQUISICAO);
			intpd_processar_atributo(reg_integracao_w, 'VL_CUSTO_TOTAL', c02_w.VL_CUSTO_TOTAL, 'S', solic_compra_item_w.VL_CUSTO_TOTAL);
			intpd_processar_atributo(reg_integracao_w, 'VL_ESTOQUE_TOTAL', c02_w.VL_ESTOQUE_TOTAL, 'S', solic_compra_item_w.VL_ESTOQUE_TOTAL);
			intpd_processar_atributo(reg_integracao_w, 'NR_ORDEM_COMPRA_ORIG', c02_w.NR_ORDEM_COMPRA_ORIG, 'S', solic_compra_item_w.NR_ORDEM_COMPRA_ORIG);
			intpd_processar_atributo(reg_integracao_w, 'CD_PERFIL_APROV', c02_w.CD_PERFIL_APROV, 'S', solic_compra_item_w.CD_PERFIL_APROV);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CRITERIO_RATEIO', c02_w.NR_SEQ_CRITERIO_RATEIO, 'S', solic_compra_item_w.NR_SEQ_CRITERIO_RATEIO);
			intpd_processar_atributo(reg_integracao_w, 'DS_JUSTIF_DIVER', c02_w.DS_JUSTIF_DIVER, 'S', solic_compra_item_w.DS_JUSTIF_DIVER);
			intpd_processar_atributo(reg_integracao_w, 'QT_DIAS_ESTOQUE', c02_w.QT_DIAS_ESTOQUE, 'S', solic_compra_item_w.QT_DIAS_ESTOQUE);
			intpd_processar_atributo(reg_integracao_w, 'CD_CNPJ', c02_w.CD_CNPJ, 'S', solic_compra_item_w.CD_CNPJ);
			intpd_processar_atributo(reg_integracao_w, 'IE_CANCELAMENTO_INTEGR', c02_w.IE_CANCELAMENTO_INTEGR, 'S', solic_compra_item_w.IE_CANCELAMENTO_INTEGR);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_NORMA_COMPRA', c02_w.NR_SEQ_NORMA_COMPRA, 'S', solic_compra_item_w.NR_SEQ_NORMA_COMPRA);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_PROC_APROV', c02_w.NR_SEQ_PROC_APROV, 'S', solic_compra_item_w.NR_SEQ_PROC_APROV);
			intpd_processar_atributo(reg_integracao_w, 'IE_SERVICO_REALIZADO', c02_w.IE_SERVICO_REALIZADO, 'S', solic_compra_item_w.IE_SERVICO_REALIZADO);
			intpd_processar_atributo(reg_integracao_w, 'DS_MARCAS_ACEITAVEIS', c02_w.DS_MARCAS_ACEITAVEIS, 'S', solic_compra_item_w.DS_MARCAS_ACEITAVEIS);
			intpd_processar_atributo(reg_integracao_w, 'VL_ORCADO_GPI', c02_w.VL_ORCADO_GPI, 'S', solic_compra_item_w.VL_ORCADO_GPI);
			intpd_processar_atributo(reg_integracao_w, 'DT_SOLIC_ITEM_ANT', c02_w.DT_SOLIC_ITEM_ANT, 'S', solic_compra_item_w.DT_SOLIC_ITEM_ANT);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_JUSTIF', c02_w.NR_SEQ_JUSTIF, 'S', solic_compra_item_w.NR_SEQ_JUSTIF);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_ORDEM_SERV', c02_w.NR_SEQ_ORDEM_SERV, 'S', solic_compra_item_w.NR_SEQ_ORDEM_SERV);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_CONTA_FINANC', c02_w.NR_SEQ_CONTA_FINANC, 'S', solic_compra_item_w.NR_SEQ_CONTA_FINANC);
			intpd_processar_atributo(reg_integracao_w, 'QT_AMOSTRA', c02_w.QT_AMOSTRA, 'S', solic_compra_item_w.QT_AMOSTRA);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_REG_PCS', c02_w.NR_SEQ_REG_PCS, 'S', solic_compra_item_w.NR_SEQ_REG_PCS);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_REG_COMPRA', c02_w.NR_SEQ_REG_COMPRA, 'S', solic_compra_item_w.NR_SEQ_REG_COMPRA);
			intpd_processar_atributo(reg_integracao_w, 'NR_SEQ_OP_COMP_OPM', c02_w.NR_SEQ_OP_COMP_OPM, 'S', solic_compra_item_w.NR_SEQ_OP_COMP_OPM);
			intpd_processar_atributo(reg_integracao_w, 'NR_ITEM_OCI_ORIG', c02_w.NR_ITEM_OCI_ORIG, 'S', solic_compra_item_w.NR_ITEM_OCI_ORIG);
			intpd_processar_atributo(reg_integracao_w, 'QT_SALDO_REG_PRECO', c02_w.QT_SALDO_REG_PRECO, 'S', solic_compra_item_w.QT_SALDO_REG_PRECO);
			intpd_processar_atributo(reg_integracao_w, 'QT_MATERIAL_CANCELADO', c02_w.QT_MATERIAL_CANCELADO, 'S', solic_compra_item_w.QT_MATERIAL_CANCELADO);
			solic_compra_item_w.nr_solic_compra := solic_compra_w.nr_solic_compra;
			solic_compra_item_w.dt_atualizacao := sysdate;
			solic_compra_item_w.nm_usuario := solic_compra_w.nm_usuario;
			
			if	(reg_integracao_w.qt_reg_log = 0) then
				select	nvl(max(nr_item_solic_compra),0)
				into	nr_item_solic_compra_w
				from	solic_compra_item
				where	nr_solic_compra = solic_compra_item_w.nr_solic_compra
				and	nr_item_solic_compra = solic_compra_item_w.nr_item_solic_compra;
			
				if (nr_item_solic_compra_w > 0) then
					update	solic_compra_item
					set	row = solic_compra_item_w
					where	nr_solic_compra = solic_compra_item_w.nr_solic_compra
					and	nr_item_solic_compra = nr_item_solic_compra_w;
				else
					insert into solic_compra_item values solic_compra_item_w;
				end if;
			end if;	

			open c03;
			loop
			fetch c03 into	
				c03_w;
			exit when c03%notfound;
				begin
				reg_integracao_w.nm_tabela		:=	'SOLIC_COMPRA_ITEM_ENTREGA';
				reg_integracao_w.nm_elemento		:=	'ENTREGA';
				reg_integracao_w.nr_seq_visao		:=	15848;	
				intpd_processar_atributo(reg_integracao_w, 'NR_ITEM_SOLIC_COMPRA', c03_w.NR_ITEM_SOLIC_COMPRA, 'S', solic_compra_item_entrega_w.NR_ITEM_SOLIC_COMPRA);
				intpd_processar_atributo(reg_integracao_w, 'NR_ITEM_SOLIC_COMPRA_ENTR', c03_w.NR_ITEM_SOLIC_COMPRA_ENTR, 'S', solic_compra_item_entrega_w.NR_ITEM_SOLIC_COMPRA_ENTR);
				intpd_processar_atributo(reg_integracao_w, 'QT_ENTREGA_SOLICITADA', c03_w.QT_ENTREGA_SOLICITADA, 'S', solic_compra_item_entrega_w.QT_ENTREGA_SOLICITADA);
				intpd_processar_atributo(reg_integracao_w, 'DT_ENTREGA_SOLICITADA', c03_w.DT_ENTREGA_SOLICITADA, 'S', solic_compra_item_entrega_w.DT_ENTREGA_SOLICITADA);
				intpd_processar_atributo(reg_integracao_w, 'DS_OBSERVACAO', c03_w.DS_OBSERVACAO, 'S', solic_compra_item_entrega_w.DS_OBSERVACAO);
				solic_compra_item_entrega_w.nr_solic_compra := solic_compra_w.nr_solic_compra;
				solic_compra_item_entrega_w.dt_atualizacao := sysdate;
				solic_compra_item_entrega_w.nm_usuario := solic_compra_w.nm_usuario;
				solic_compra_item_entrega_w.dt_atualizacao_nrec := solic_compra_item_entrega_w.dt_atualizacao;
				solic_compra_item_entrega_w.nm_usuario_nrec := solic_compra_w.nm_usuario;
			
				if	(reg_integracao_w.qt_reg_log = 0) then			
					select	nvl(max(nr_item_solic_compra_entr),0)
					into	nr_item_solic_compra_entr_w
					from	solic_compra_item_entrega
					where	nr_solic_compra = solic_compra_item_w.nr_solic_compra
					and	nr_item_solic_compra = solic_compra_item_w.nr_item_solic_compra
					and	nr_item_solic_compra_entr = solic_compra_item_entrega_w.nr_item_solic_compra_entr;
					
					if (nr_item_solic_compra_entr_w > 0) then
						update	solic_compra_item_entrega
						set	row = solic_compra_item_entrega_w
						where	nr_solic_compra = solic_compra_item_w.nr_solic_compra
						and	nr_item_solic_compra = solic_compra_item_w.nr_item_solic_compra
						and	nr_item_solic_compra_entr = solic_compra_item_entrega_w.nr_item_solic_compra_entr;
					else
						insert into solic_compra_item_entrega values solic_compra_item_entrega_w;
					end if;
				end if;
				end;
			end loop;
			close c03;			
			end;
		end loop;
		close c02;	
	end if;
	end;
end loop;
close C01;

if	((reg_integracao_w.qt_reg_log > 0) or (ie_erro_w = 'S')) then
	begin
	rollback;
 
	update 	intpd_fila_transmissao
	set	ie_status = 'E',
		ie_tipo_erro = 'F'
	where	nr_sequencia = nr_sequencia_p;
 
	for i in 0..reg_integracao_w.qt_reg_log-1 loop		
		INTPD_GRAVAR_LOG_RECEBIMENTO(nr_sequencia_p,reg_integracao_w.intpd_log_receb(i).ds_log,'INTPDTASY');
	end loop;
	end;
else
	update	intpd_fila_transmissao
	set	ie_status = 'S',
		nr_seq_documento = nvl(nr_solic_compra_delete_w, solic_compra_w.nr_solic_compra),
		nr_doc_externo = c01_w.nr_doc_externo_receb
	where	nr_sequencia = nr_sequencia_p;	
end if;

commit;

end intpd_recebe_solic_compra;
/
