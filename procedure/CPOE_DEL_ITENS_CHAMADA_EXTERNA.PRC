create or replace
procedure cpoe_del_itens_chamada_externa(nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
										cd_pessoa_fisica_p	pessoa_fisica.cd_pessoa_fisica%type,
										cd_funcao_origem_p	cpoe_procedimento.cd_funcao_origem%type,
										nm_usuario_p		Varchar2) is 

nr_sequencia_w		cpoe_procedimento.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia
	from	cpoe_procedimento
	where ((nr_atendimento = nr_atendimento_p) or (cd_pessoa_fisica = cd_pessoa_fisica_p and nr_atendimento is null))
	and nm_usuario = nm_usuario_p
	and nvl(cd_funcao_origem,2314) = cd_funcao_origem_p
	and dt_liberacao is null;

begin
/* 
objeto esta somente procedimento, porem pode ser colocado outros itens da cpoe aqui, seguindo o mesmo objetivo da mesma
*/

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;

	delete from cpoe_revalidation_events
	where nr_seq_exam = nr_sequencia_w;

	delete from cpoe_plan_protocol
	where nr_seq_exam_cpoe = nr_sequencia_w
	and exists (select 1 
	              from cpoe_procedimento x
				 where x.nr_sequencia = nr_sequencia_w
				   and x.dt_liberacao is null);

	delete from cpoe_procedimento
	where 	nr_seq_proc_princ = nr_sequencia_w
	and	dt_liberacao is null;			

	delete from cpoe_procedimento
	where nr_sequencia = nr_sequencia_w
	and	dt_liberacao is null;

	delete cpoe_material
	where nr_seq_procedimento = nr_sequencia_w
	and	dt_liberacao is null;

end loop;
close C01;

commit;

end cpoe_del_itens_chamada_externa;
/
