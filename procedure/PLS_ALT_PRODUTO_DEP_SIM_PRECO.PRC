create or replace
procedure pls_alt_produto_dep_sim_preco
			(	nr_seq_titular_p	number,
				nm_usuario_p		Varchar2) is 

nr_seq_dependente_w	number(15);	
nr_seq_simulacao_w	number(15);
nr_seq_produto_w		number(15);
nr_seq_tabela_w		number(15);
				
cursor c01 is
	select	nr_sequencia
	from	pls_simulpreco_individual
	where	nr_seq_simulacao = nr_seq_simulacao_w
	and	ie_tipo_benef	 = 'D';

begin
select	nr_seq_simulacao,
	nr_seq_produto,
	nr_seq_tabela
into	nr_seq_simulacao_w,
	nr_seq_produto_w,
	nr_seq_tabela_w
from	pls_simulpreco_individual
where	nr_sequencia	= nr_seq_titular_p;	

open c01;
loop
fetch C01 into
	nr_seq_dependente_w;
exit when C01%NOTFOUND;
	update	pls_simulpreco_individual
	set	nr_seq_produto	= nr_seq_produto_w,
		nr_seq_tabela	= nr_seq_tabela_w
	where	nr_sequencia	= nr_seq_dependente_w;	
end loop;
close C01;

pls_recalcular_simulacao(nr_seq_simulacao_w,nm_usuario_p);

open C01;
loop
fetch C01 into	
	nr_seq_dependente_w;
exit when C01%notfound;
	begin
	
	pls_gerar_resumo_simulacao(nr_seq_simulacao_w,nr_seq_dependente_w,'E','BS','I',wheb_usuario_pck.get_cd_estabelecimento,nm_usuario_p);
	
	end;
end loop;
close C01;

end pls_alt_produto_dep_sim_preco;
/
