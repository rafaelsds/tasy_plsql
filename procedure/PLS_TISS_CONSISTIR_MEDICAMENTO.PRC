create or replace
procedure pls_tiss_consistir_medicamento
			(	nr_sequencia_p		Number,
				ie_tipo_glosa_p		Varchar2,
				ie_evento_p		Varchar2,
				nr_seq_prestador_p	Number,
				nr_seq_ocorrencia_p	Number,
				ds_parametro_um_p	Varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is 

/* IE_TIPO_GLOSA_P
	C - Conta
	CP - Conta procedimento
	CM - Conta material
	A - Autorização
	AP - Autorização procedimento
	AM - Autorização material
*/				

qt_medicamento_imp_w		Number(12,4);
dt_atendimento_w		date;
dt_exclusao_w			date;
nr_seq_material_w		number(10);
ie_mat_data_exclusao_w		varchar2(1) := 'N';
nr_seq_conta_w			pls_conta.nr_sequencia%type;


begin

if	(ie_tipo_glosa_p = 'CM') then
	begin
		qt_medicamento_imp_w := 0;
		select	nr_seq_material,
			nvl(qt_material_imp,0),
			dt_atendimento,
			nr_seq_conta
		into	nr_seq_material_w,
			qt_medicamento_imp_w,
			dt_atendimento_w,
			nr_seq_conta_w
		from	pls_conta_mat
		where	nr_sequencia = nr_sequencia_p;
	exception
	when others then
		qt_medicamento_imp_w := null;
	end;
	
	if	(nr_seq_material_w is not null) then
		begin
		select	dt_exclusao
		into	dt_exclusao_w
		from	pls_material
		where	nr_sequencia = nr_seq_material_w;
		exception
		when others then
			dt_exclusao_w := null;
		end;
	
		if	(dt_exclusao_w is not null) and
			(dt_atendimento_w is not null) and
			(trunc(dt_atendimento_w) > trunc(dt_exclusao_w)) then
			ie_mat_data_exclusao_w := 'S';
		end if;
	end if;

end if;

if	(qt_medicamento_imp_w <= 0) then		
	pls_gravar_glosa_tiss('2105', 
		null, ie_evento_p,
		nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
		nr_seq_ocorrencia_p, '', nm_usuario_p,
		cd_estabelecimento_p, nr_seq_conta_w);
end if;

if	(ie_mat_data_exclusao_w = 'S') then
	pls_gravar_glosa_tiss('2199', 
		'Medicamento com data de exclusão '||dt_exclusao_w||'. Data do medicamento na conta: '||dt_atendimento_w, ie_evento_p,
		nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
		nr_seq_ocorrencia_p, '', nm_usuario_p,
		cd_estabelecimento_p, nr_seq_conta_w);
end if;

--commit;

end pls_tiss_consistir_medicamento;
/