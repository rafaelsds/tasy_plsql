create or replace
procedure Des_Gerar_Ordens_Plano_Meta
		(nr_seq_plano_p			number,
		nm_usuario_p			Varchar2) is 

nr_seq_gerencia_w				number(10,0);
nr_seq_ordem_w					number(10,0);
ie_60_dias_w					varchar2(10);
ie_45_dias_w					varchar2(10);
ie_30_dias_w					varchar2(10);
ie_15_dias_w					varchar2(10);
ie_7_dias_w						varchar2(10);
ie_semana_w						varchar2(10);
ie_tipo_os_meta_w				varchar2(10);
nr_seq_plano_os_w				number(10,0);
nr_seq_estagio_w				number(10,0);
		
Cursor 	C01 is
		select  a.nr_sequencia,
				a.nr_seq_estagio,
				decode(obter_se_data_periodo(trunc(a.dt_ordem_servico, 'dd'), to_date('01/01/2007','dd/mm/yyyy'), sysdate -60), 'S', 'S', '') ie_60_dias,
				decode(obter_se_data_periodo(trunc(a.dt_ordem_servico, 'dd'), sysdate - 59, sysdate -45), 'S', 'S', '') ie_45_dias,
				decode(obter_se_data_periodo(trunc(a.dt_ordem_servico, 'dd'), sysdate - 44, sysdate -30), 'S', 'S', '') ie_30_dias,
				decode(obter_se_data_periodo(trunc(a.dt_ordem_servico, 'dd'), sysdate - 29, sysdate -15), 'S', 'S', '') ie_15_dias,
				decode(obter_se_data_periodo(trunc(a.dt_ordem_servico, 'dd'), sysdate - 14, sysdate -7), 'S', 'S', '') ie_7_dias,
				decode(obter_se_data_periodo(trunc(a.dt_ordem_servico, 'dd'), sysdate - 6, sysdate), 'S', 'S', '') ie_semana
		from    gerencia_wheb c,
				grupo_desenvolvimento b,
				man_ordem_servico  a
		where  	b.nr_seq_gerencia	= c.nr_sequencia
		and		(('S' = 'N')  or (obter_se_os_pend_cliente(a.nr_sequencia) = 'N'))
		and	  	(('S' = 'N')  or (Obter_Se_OS_Desenv(a.nr_sequencia) = 'S'))
		and   	(('N' = 'N' and ie_status_ordem <> 3) or ('N' = 'S' and ie_status_ordem = 3))
		and   	a.nr_seq_grupo_des    = b.nr_sequencia
		and   	a.dt_ordem_servico < sysdate
		and   	substr(obter_se_ordem_vinc_projeto(a.nr_sequencia,'2'),1,1) = 'N'
		and 	c.nr_sequencia 	= nr_seq_gerencia_w
		order by 2,3,4,5;
		
begin

delete from desenv_plano_meta_os
where nr_seq_plano = nr_seq_plano_p;

select	max(nr_seq_gerencia)
into	nr_seq_gerencia_w
from	desenvolvimento_plano_meta
where	nr_sequencia	= nr_seq_plano_p;

if		(nr_seq_gerencia_w is not null) then
		begin
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_ordem_w,
			nr_seq_estagio_w,
			ie_60_dias_w,
			ie_45_dias_w,
			ie_30_dias_w,
			ie_15_dias_w,
			ie_7_dias_w,
			ie_semana_w;
		exit when C01%notfound;
			begin
			if	(ie_60_dias_w = 'S') then
				ie_tipo_os_meta_w	:= '0';
			elsif (ie_45_dias_w = 'S') then
				ie_tipo_os_meta_w	:= '1';
			elsif (ie_30_dias_w = 'S') then
				ie_tipo_os_meta_w	:= '2';
			elsif (ie_15_dias_w = 'S') then
				ie_tipo_os_meta_w	:= '3';
			elsif (ie_7_dias_w = 'S') then
				ie_tipo_os_meta_w	:= '4';
			elsif (ie_semana_w = 'S') then
				ie_tipo_os_meta_w	:= '5';
			end if;
			
			select	desenv_plano_meta_os_seq.nextval
			into	nr_seq_plano_os_w
			from	dual;
			
			insert 	into desenv_plano_meta_os
					(nr_sequencia,           
					nr_seq_plano,
					nr_seq_ordem_serv,      
					dt_atualizacao,         
					nm_usuario,             
					dt_atualizacao_nrec,
					nm_usuario_nrec,        
					ie_tipo_os_meta,
					nr_seq_estagio_orig)
			values
					(nr_seq_plano_os_w,
					nr_seq_plano_p,
					nr_seq_ordem_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					ie_tipo_os_meta_w,
					nr_seq_estagio_w);
			
			end;
		end loop;
		close C01;
		
		update	desenvolvimento_plano_meta
		set		dt_geracao	= sysdate
		where	nr_sequencia	= nr_seq_plano_p;
		
		end;
end if;

commit;

end Des_Gerar_Ordens_Plano_Meta;
/