create or replace
procedure controlar_processo_sorologia(	nr_seq_exame_lote_p	number,
					nr_seq_exame_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		Varchar2) is 

nr_seq_doacao_w		number(10);

begin

select	c.nr_sequencia
into	nr_seq_doacao_w
from	san_doacao c,
	san_exame_lote b,
	san_exame_realizado a
where	c.nr_sequencia		= b.nr_seq_doacao
and	b.nr_sequencia		= a.nr_seq_exame_lote
and	a.nr_seq_exame_lote	= nr_seq_exame_lote_p
and	a.nr_seq_exame		= nr_seq_exame_p;

insert into san_processo_controle_soro(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_doacao,
	dt_processo_soro)
values( san_processo_controle_soro_seq.nextVal,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_doacao_w,
	sysdate);

commit;

end controlar_processo_sorologia;
/
