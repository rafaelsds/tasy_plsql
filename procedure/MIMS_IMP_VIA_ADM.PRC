create or replace procedure MIMS_IMP_VIA_ADM( ie_via_aplicacao_p varchar2,

    ds_via_aplicacao_p varchar2,

    nm_usuario_p  varchar2,
    nr_mims_ver_p number) is



qt_reg_w  number(10);

via_aplicacao_w  imp_via_aplicacao%rowtype;



begin



select count(1)

into qt_reg_w

from imp_via_aplicacao

where ie_via_aplicacao = ie_via_aplicacao_p;



if (qt_reg_w = 0) then

 via_aplicacao_w.ie_via_aplicacao := ie_via_aplicacao_p;

 via_aplicacao_w.ds_via_aplicacao := ds_via_aplicacao_p;

 via_aplicacao_w.nm_usuario  := nm_usuario_p;

 via_aplicacao_w.nm_usuario_nrec  := nm_usuario_p;

 via_aplicacao_w.dt_atualizacao  := sysdate;

 via_aplicacao_w.dt_atualizacao_nrec := sysdate;

 via_aplicacao_w.ie_situacao  := 'A';

 via_aplicacao_w.ie_gera_diluicao := 'S';

 via_aplicacao_w.NR_SEQ_MIMS_VERSION := nr_mims_ver_p;


 insert into imp_via_aplicacao values via_aplicacao_w;

-- commit;

end if;



end mims_imp_via_adm;
/