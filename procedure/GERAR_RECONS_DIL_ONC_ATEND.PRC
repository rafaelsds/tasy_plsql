CREATE OR REPLACE
PROCEDURE Gerar_Recons_Dil_onc_atend(	nr_seq_atendimento_p	number,
					nr_seq_material_p	number,
					ie_opcao_p	Varchar2,
					nm_usuario_p		varchar2) is


cd_material_w				Number(6,0);
cd_material_sem_apresent_w		Number(6,0);
cd_material_generico_w			Number(6,0);
cd_mat_generico_sem_apresent_w		Number(6,0);
cd_intervalo_w				Varchar2(7);
cd_intervalo_cad_w			varchar2(7);
nr_agrupamento_w			Number(07,1);
cd_unidade_medida_w			Varchar2(30);
nr_ocorrencia_w				Number(15,4);
nr_seq_acum_w				Number(10,0);
nr_agrup_acum_w				Number(07,1);
nr_sequencia_w				Number(10,0);
qt_conversao_w				Number(18,6);
cd_dil_w				Number(10,0);
nm_usuario_w				Varchar2(15);
ie_bomba_infusao_w			Varchar2(15);
ie_gerar_kit_w				Varchar2(15);
ie_gerar_reconstituicao_w		Varchar2(15);
ie_tipo_material_w			Varchar2(15);
nr_seq_ficha_tecnica_w			Number(10,0);

cd_diluente_w				Number(6,0);
qt_diluicao_w				Number(18,6);
cd_unid_med_diluente_w			Varchar2(30);
ie_reconstituicao_w			Varchar2(1);
ie_gera_dil_dose_w			Varchar2(5);
ie_via_aplicacao_w			Varchar2(5);
ie_priorizar_min_w			Varchar2(5);
ie_via_aplic_order_w			Varchar2(5);
qt_minuto_aplicacao_w			number(4);
qt_hora_aplicacao_w			number(3);
qt_material_w				Number(15,3);
Qt_total_disp_w				Number(18,6);
qt_unitaria_w				Number(18,6);

ie_gera_diluicao_w			Varchar2(1) := 'S';
Controle_w				Number(1,0) := 0;
Controle_ww				Number(1,0) := 0;
ie_regra_w				Varchar2(1) := 'N';
ie_diluente_w				Varchar2(1) := 'N';
ie_atualizar_horario_w			Varchar2(5);
nr_seq_prioridade_w			Number(3);
cd_setor_atendimento_w			number(5);
cd_setor_w				number(5);
cd_estabelecimento_w			number(4);
qt_idade_w				Number(15,2);
qt_idade_min_w				Number(15,2);
qt_idade_max_w				Number(15,2);

cd_motivo_baixa_w			number(3,0); /* Rafael em 17/11/2007 OS74014 */
ie_cobra_paciente_w			varchar2(1); /* Rafael em 17/11/2007 OS74014 */
ds_dose_diferenciada_w			varchar2(50);
qt_dispensar_w				Number(18,6);
ie_regra_disp_w				varchar2(1);
ds_erro_w				varchar2(255);
cd_perfil_w				number(5);
nm_usuario_original_w			varchar2(15);
ie_atualiza_reconst_w			varchar2(1);
qt_peso_w				number(6,3);
ie_gerar_diluicao_w			varchar2(1);
qt_vol_adic_reconst_w			number(18,6);
ie_proporcao_w				varchar2(15);
qt_referencia_w				number(18,6); 
qt_referencia_aux_w			number(18,6);
qt_unitaria_medic_w			number(18,6);
qt_dose_medic_w				number(18,6);
qt_dose_aux_w				number(18,6);
cd_unidade_medida_dose_w		varchar2(30);
qt_dose_diluicao_w			number(18,6);
nr_seq_via_acesso_w			number(10);
nr_seq_restricao_w			number(10);
cd_pessoa_fisica_w			varchar2(30);
ds_lista_restricao_w			varchar2(255);
qt_solucao_w				number(15,4);
nr_seq_interno_w			number(10,0);
cd_unid_med_reconst_w			varchar2(30);
qt_dose_especial_w			number(18,6);
qt_dias_util_w				number(3,0);
ie_gerar_diluicao_ww			varchar2(1);
ie_prescr_mat_sem_lib_w			varchar2(30);
ie_calcula_volume_afterpost_w		varchar2(1);
ie_substitui_tempo_Atual_w		varchar2(1);
ie_ConsisteDoseConsumoMedic_w		varchar2(1) := 'S';
ie_gerar_lote_w				varchar2(1);
ds_dias_aplicacao_w			varchar2(4000);
nr_seq_mat_diluicao_w			number(10);
nr_seq_paciente_w			number(10);
qt_volume_medic_w			number(15,4);
ie_medicacao_paciente_w		varchar2(1);
/*
ie_opcao_p
	D Diluicao
	R Reconstituicao
	A Ambos
*/	

Cursor c01 is
select	nvl(cd_diluente,0),
	nvl(qt_diluicao,0),
	nvl(cd_unid_med_diluente, obter_unid_med_usua('ml')),
	nvl(ie_reconstituicao,'A'),
	ie_via_aplicacao,
	nvl(qt_minuto_aplicacao,0),
	cd_intervalo,
	nr_seq_prioridade,
	cd_setor_atendimento,
	qt_idade_min,
	qt_idade_max,
	nvl(cd_motivo_baixa,0),
	nvl(ie_cobra_paciente,'S'),
	nvl(ie_proporcao,'F'),
	qt_referencia,
	nr_seq_restricao,
	nr_seq_interno,
	ie_atualizar_tempo,
	IE_DILUIR_INTEIRO,
	nvl(ie_gerar_lote,'S'),
	nr_seq_interno,
	qt_volume_medic
from	material_diluicao
where	ie_gera_diluicao_w = 'S'
and	ie_opcao_p in ('A','D')
and	((cd_setor_atendimento	= cd_setor_atendimento_w) 
or	(cd_setor_atendimento	is null))
and	((cd_setor_excluir is null) or (cd_setor_excluir <> cd_setor_atendimento_w))
and	obter_se_regra_diluicao_setor(nr_seq_interno, cd_setor_atendimento_w) = 'S'
and	((nr_seq_via_acesso is null) or (nr_seq_via_acesso = nr_seq_via_acesso_w))
and	cd_estabelecimento	= cd_estabelecimento_w
and	((obter_se_contido(nr_seq_restricao, ds_lista_restricao_w) = 'S') or
	 (nr_seq_restricao	is null))
and	ie_reconstituicao	= 'N'
and	cd_material		= nvl(cd_material_sem_apresent_w, cd_material_w)
and	qt_idade_w between obter_idade_diluicao(cd_material,nr_sequencia,'MIN') and obter_idade_diluicao(cd_material,nr_sequencia,'MAX')
and	qt_peso_w  between nvl(qt_peso_min,0) and nvl(qt_peso_max,999999)
and	nvl(cd_perfil,nvl(obter_perfil_ativo,0)) = nvl(obter_perfil_ativo,0)
and	((cd_unidade_medida is null) or
	 (cd_unidade_medida	= cd_unidade_medida_dose_w))
and	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida_dose_w,qt_dose_medic_w),0) between 
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_min),0) and
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_max),9999999)
and	((ie_via_excluir	<> ie_via_aplicacao_w) or
	 (ie_via_excluir is null))
and	((ie_via_aplicacao	= ie_via_aplicacao_w) or
	(ie_via_aplicacao is null))
union
select	nvl(cd_diluente,0),
	nvl(qt_diluicao,0),
	nvl(cd_unid_med_diluente, obter_unid_med_usua('ml')),
	nvl(ie_reconstituicao,'A'),
	ie_via_aplicacao,
	nvl(qt_minuto_aplicacao,0),
	cd_intervalo,
	nr_seq_prioridade,
	cd_setor_atendimento,
	qt_idade_min,
	qt_idade_max,
	nvl(cd_motivo_baixa,0),
	nvl(ie_cobra_paciente,'S'),
	nvl(ie_proporcao,'F'),
	qt_referencia,
	nr_seq_restricao,
	nr_seq_interno,
	ie_atualizar_tempo,
	IE_DILUIR_INTEIRO,
	nvl(ie_gerar_lote,'S'),
	nr_seq_interno,
	qt_volume_medic
from	material_diluicao
where	ie_gera_diluicao_w	= 'S'
and	ie_opcao_p in ('A','D')
and	ie_reconstituicao	= 'N'
and	((cd_setor_atendimento	= cd_setor_atendimento_W) 
or	(cd_setor_atendimento	is null))
and	((cd_setor_excluir is null) or (cd_setor_excluir <> cd_setor_atendimento_w))
and	obter_se_regra_diluicao_setor(nr_seq_interno, cd_setor_atendimento_w) = 'S'
and	((nr_seq_via_acesso is null) or (nr_seq_via_acesso = nr_seq_via_acesso_w))
and	((obter_se_contido(nr_seq_restricao, ds_lista_restricao_w) = 'S') or
	 (nr_seq_restricao	is null))
and	cd_estabelecimento	= cd_estabelecimento_w
and	cd_material		= nvl(cd_mat_generico_sem_apresent_w, cd_material_generico_w)
and	qt_idade_w between obter_idade_diluicao(cd_material,nr_sequencia,'MIN') and obter_idade_diluicao(cd_material,nr_sequencia,'MAX')
and	qt_peso_w  between nvl(qt_peso_min,0) and nvl(qt_peso_max,999999)
and	nvl(cd_perfil,nvl(obter_perfil_ativo,0)) = nvl(obter_perfil_ativo,0)
and	((cd_unidade_medida	is null) or
	 (cd_unidade_medida	= cd_unidade_medida_dose_w))
and	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida_dose_w,qt_dose_medic_w),0) between 
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_min),0) and
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_max),9999999)
and	ie_regra_w		= 'G'
and	((ie_via_excluir	<> ie_via_aplicacao_w) or
	 (ie_via_excluir is null))
and	((ie_via_aplicacao	= ie_via_aplicacao_w) or
	 (ie_via_aplicacao is null))
order by 	ie_via_aplicacao desc, 
		cd_setor_atendimento desc,
		qt_idade_min desc,
		qt_idade_max desc,
		nr_seq_prioridade desc,
		nr_seq_restricao desc;

Cursor c02 is
Select	nvl(cd_diluente,0),
	nvl(qt_diluicao,0),
	nvl(cd_unid_med_diluente, obter_unid_med_usua('ml')),
	nvl(ie_reconstituicao,'A'),
	ie_via_aplicacao,
	cd_intervalo,
	nr_seq_prioridade,
	cd_setor_atendimento,
	qt_idade_min,
	qt_idade_max,
	nvl(cd_motivo_baixa,0),
	nvl(ie_cobra_paciente,'S'),
	nvl(qt_minuto_aplicacao,0),
	qt_volume_adic,
	nvl(ie_proporcao,'F'),
	qt_referencia,
	nr_seq_restricao,
	cd_unid_med_reconst,
	ie_atualizar_tempo,
	nvl(ie_priorizar_min,'N'),
	nvl(ie_gerar_lote,'S'),
	nr_seq_interno
from	material_diluicao
where	ie_opcao_p in ('A','R')
and	cd_material		= cd_material_w
and	ie_reconstituicao 	= 'S'
and	ie_gerar_reconstituicao_w	= 'S'
and	cd_estabelecimento	= cd_estabelecimento_w
and	((obter_se_contido(nr_seq_restricao, ds_lista_restricao_w) = 'S') or
	 (nr_seq_restricao	is null))
and	qt_idade_w between obter_idade_diluicao(cd_material,nr_sequencia,'MIN') and obter_idade_diluicao(cd_material,nr_sequencia,'MAX')
and	qt_peso_w  between nvl(qt_peso_min,0) and nvl(qt_peso_max,999999)
and	((cd_setor_atendimento	= cd_setor_atendimento_W) or	
	(cd_setor_atendimento	is null))
and	((cd_setor_excluir is null) or (cd_setor_excluir <> cd_setor_atendimento_w))
and	obter_se_regra_diluicao_setor(nr_seq_interno, cd_setor_atendimento_w) = 'S'
and	nvl(cd_perfil,nvl(obter_perfil_ativo,0)) = nvl(obter_perfil_ativo,0)
and	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida_dose_w, qt_dose_medic_w ),0) between 
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_min),0) and
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_max),9999999)
and	((ie_via_aplicacao 	= ie_via_aplicacao_w) or
	 (ie_via_aplicacao is null))
and	((ie_via_excluir	<> ie_via_aplicacao_w) or
	 (ie_via_excluir is null))	 
and 	((cd_unidade_medida is null) or
	 (cd_unidade_medida	= cd_unidade_medida_dose_w))
union
Select	nvl(cd_diluente,0),
	nvl(qt_diluicao,0),
	nvl(cd_unid_med_diluente, obter_unid_med_usua('ml')),
	nvl(ie_reconstituicao,'A'),
	ie_via_aplicacao,
	cd_intervalo,
	nr_seq_prioridade,
	cd_setor_atendimento,
	qt_idade_min,
	qt_idade_max,
	nvl(cd_motivo_baixa,0),
	nvl(ie_cobra_paciente,'S'),
	nvl(qt_minuto_aplicacao,0),
	qt_volume_adic,
	nvl(ie_proporcao,'F'),
	qt_referencia,
	nr_seq_restricao,
	cd_unid_med_reconst,
	ie_atualizar_tempo,
	nvl(ie_priorizar_min,'N'),
	nvl(ie_gerar_lote,'S'),
	nr_seq_interno
from	material_diluicao
where	ie_opcao_p in ('A','R')
and	cd_material 		= cd_material_generico_w
and	cd_estabelecimento	= cd_estabelecimento_w
and	((obter_se_contido(nr_seq_restricao, ds_lista_restricao_w) = 'S') or
	 (nr_seq_restricao	is null))
and	qt_idade_w between obter_idade_diluicao(cd_material,nr_sequencia,'MIN') and obter_idade_diluicao(cd_material,nr_sequencia,'MAX')
and	qt_peso_w  between nvl(qt_peso_min,0) and nvl(qt_peso_max,999999)
and	ie_reconstituicao	= 'S'
and	ie_gerar_reconstituicao_w	= 'S'
and	ie_regra_w		= 'G'
and	nvl(cd_perfil,nvl(obter_perfil_ativo,0)) = nvl(obter_perfil_ativo,0)
and	((cd_setor_atendimento	= cd_setor_atendimento_W) 
or	(cd_setor_atendimento	is null))
and	((ie_via_excluir	<> ie_via_aplicacao_w) or
	 (ie_via_excluir is null))
and	((cd_setor_excluir is null) or (cd_setor_excluir <> cd_setor_atendimento_w))
and	obter_se_regra_diluicao_setor(nr_seq_interno, cd_setor_atendimento_w) = 'S'
and	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida_dose_w,qt_dose_medic_w),0) between 
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_min),0) and
	nvl(obter_conversao_unid_med_cons(cd_material_w,cd_unidade_medida,qt_dose_max),9999999)
and	((ie_via_aplicacao	= ie_via_aplicacao_w) or
	 (ie_via_aplicacao is null))
and 	((cd_unidade_medida is null) or
	 (cd_unidade_medida	= cd_unidade_medida_dose_w))
order by	ie_via_aplicacao desc, 
		cd_setor_atendimento desc,
		qt_idade_min desc,
		qt_idade_max desc,
		nr_seq_prioridade desc,
		nr_seq_restricao desc;
		
cursor c04 is
select	nvl(cd_diluente,0),
	nvl(qt_diluicao,0),
	nvl(cd_unid_med_diluente, obter_unid_med_usua('ml')),
	nvl(ie_reconstituicao,'A'),
	ie_via_aplicacao,
	nvl(qt_minuto_aplicacao,0),
	cd_intervalo,
	nr_seq_prioridade,
	cd_setor_atendimento,
	qt_idade_min,
	qt_idade_max,
	nvl(cd_motivo_baixa,0),
	nvl(ie_cobra_paciente,'S'),
	nvl(ie_proporcao,'F'),
	qt_referencia,
	nr_seq_restricao,
	qt_volume,
	nvl(ie_gerar_lote,'S'),
	nr_seq_interno
from	material_diluicao
where	cd_material		= cd_material_w
and	cd_estabelecimento	= cd_estabelecimento_w
and	qt_idade_w between obter_idade_diluicao(cd_material,nr_sequencia,'MIN') and obter_idade_diluicao(cd_material,nr_sequencia,'MAX')
and	qt_peso_w  between nvl(qt_peso_min,0) and nvl(qt_peso_max,999)
and	ie_reconstituicao	= 'R'
and	ie_gerar_rediluente	= 'S'
and	((ie_via_excluir	<> ie_via_aplicacao_w) or
	 (ie_via_excluir is null))
and	nvl(cd_perfil,nvl(obter_perfil_ativo,0)) = nvl(obter_perfil_ativo,0)
and	qt_volume is not null
and	((ie_via_aplicacao is null) or
	 (ie_via_aplicacao = ie_via_aplicacao_w));
		
cursor c03 is
select	nr_seq_restricao
from	paciente_rep_prescricao a
where	dt_liberacao	is not null
and	cd_pessoa_fisica = cd_pessoa_fisica_w
and	dt_inativacao is null
and	((a.dt_fim is null) or (sysdate between nvl(a.dt_inicio,sysdate-1) and a.dt_fim + 86399/86400));

cursor c05 is
select	b.cd_material
FROM	material_estab c,
	material b,
	medic_ficha_tecnica a
WHERE	b.nr_seq_ficha_tecnica	= a.nr_sequencia
AND	c.cd_material		= b.cd_material
AND	c.cd_estabelecimento	= cd_estabelecimento_w
AND	a.nr_sequencia		= nr_seq_ficha_tecnica_w
AND	Obter_se_via_adm(b.cd_material, ie_via_aplicacao_w) = 'S'
AND	b.cd_material		<> cd_diluente_w
AND	NVL(obter_conversao_unid_med_onc(b.cd_material, cd_unid_med_diluente_w),0) >= qt_dose_diluicao_w
AND	b.ie_situacao		= 'A'
AND	c.ie_prescricao		= 'S'
order by NVL(obter_conversao_unid_med_onc(b.cd_material, cd_unid_med_diluente_w),0) desc;

BEGIN

select	max(nr_seq_paciente)
into	nr_seq_paciente_w
from	paciente_atendimento
where	nr_seq_atendimento = nr_seq_atendimento_p;

/* Fernando */
/* Matheus OS 50978 20/03/07 coloquei cd_estabelecimento*/
select	max(a.cd_setor_atendimento),
	max(a.cd_estabelecimento),
	max(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'DIA')),
	max(a.nm_usuario),
	nvl(max(a.qt_peso),0),
	max(b.cd_pessoa_fisica)
into	cd_setor_atendimento_w,
	cd_estabelecimento_w,
	qt_idade_w,
	nm_usuario_original_w,
	qt_peso_w,
	cd_pessoa_fisica_w
from	pessoa_fisica b,
	paciente_setor a
where	a.nr_seq_paciente		= nr_seq_paciente_w
and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica;






cd_perfil_w	:= obter_perfil_ativo;
Obter_Param_Usuario(924,78,cd_perfil_w,nm_usuario_original_w,cd_estabelecimento_w,ie_regra_w);
Obter_Param_Usuario(924,214,cd_perfil_w,nm_usuario_original_w,cd_estabelecimento_w,ie_atualiza_reconst_w);
Obter_Param_Usuario(924,347,cd_perfil_w,nm_usuario_original_w,cd_estabelecimento_w,ie_gera_dil_dose_w);
Obter_Param_Usuario(924,530,cd_perfil_w,nm_usuario_original_w,cd_estabelecimento_w,ie_prescr_mat_sem_lib_w);
Obter_Param_Usuario(924,600,cd_perfil_w,nm_usuario_original_w,cd_estabelecimento_w,ie_calcula_volume_afterpost_w);
Obter_Param_Usuario(924,702,cd_perfil_w,nm_usuario_original_w,cd_estabelecimento_w,ie_gerar_kit_w);

select	nvl(max(ie_gerar_diluicao),'S'),
	nvl(max(ie_gerar_reconstituicao),'S')
into	ie_gerar_diluicao_ww,
	ie_gerar_reconstituicao_w
from	setor_atendimento
where	cd_setor_atendimento	= cd_setor_atendimento_w;

if	(nr_seq_atendimento_p > 0) and
	(nr_seq_material_p > 0) and
	((ie_gerar_diluicao_ww = 'S') or
	 (ie_gerar_reconstituicao_w = 'S')) then
	
	select	nvl(max(nr_seq_material),0), 
		nvl(max(nr_agrupamento),0)
	into	nr_seq_acum_w, 
		nr_agrup_acum_w
	from	paciente_atend_medic
	where	nr_seq_atendimento = nr_seq_atendimento_p;
	
	
	/* Verificar aqui*/
	
	Select	nvl(cd_material,0),
		cd_intervalo,
		nvl(nr_agrupamento,0),
		nvl(cd_unid_med_dose, obter_unid_med_usua('ml')),
		1,
		nvl(nm_usuario,'Tasy'),
		ie_via_aplicacao,
		QT_DOSE_PRESCRICAO,
		QT_DOSE_PRESCRICAO,
		QT_DOSE_PRESCRICAO,		
		null,
		'S',
		null,
		null,
		qt_dose_prescricao,		
		CD_UNID_MED_DOSE,
		null,
		qt_dias_util,
		ie_bomba_infusao,
		nvl(ie_medicacao_paciente,'N')
	into	cd_material_w,
		cd_intervalo_w,
		nr_agrupamento_w,
		cd_unidade_medida_w,
		nr_ocorrencia_w,
		nm_usuario_w,
		ie_via_aplicacao_w,
		qt_material_w,
		Qt_total_disp_w,
		qt_unitaria_medic_w,		
		cd_material_sem_apresent_w,
		ie_gerar_diluicao_w,
		ds_dose_diferenciada_w,
		nr_seq_via_acesso_w,	
		qt_dose_medic_w,		
		cd_unidade_medida_dose_w,
		qt_dose_especial_w,
		qt_dias_util_w,
		ie_bomba_infusao_w,
		ie_medicacao_paciente_w
	from	paciente_atend_medic
	where	nr_seq_atendimento = nr_seq_atendimento_p
	and	nr_seq_material  = nr_seq_material_p
	and	cd_material is not null;
	
	open c03;
	loop
	fetch c03 into	
		nr_seq_restricao_w;
	exit when c03%notfound;
		ds_lista_restricao_w	:= nr_seq_restricao_w  || ',' || ds_lista_restricao_w;
	end loop;
	close c03;
	

	select	nvl(max(cd_material_generico),cd_material_w)
	into	cd_material_generico_w
	from	material
	where	cd_material	= cd_material_w;

	select	nvl(max(cd_material_generico),cd_material_sem_apresent_w)
	into	cd_mat_generico_sem_apresent_w
	from	material
	where	cd_material	= cd_material_sem_apresent_w;
	
	/* Inserir o reconstituinte */
	open C02;
	loop
	fetch c02 into
		cd_diluente_w,
		qt_diluicao_w,
		cd_unid_med_diluente_w,
		ie_reconstituicao_w,
		ie_via_aplic_order_w,
		cd_intervalo_cad_w,
		nr_seq_prioridade_w,
		cd_setor_w,
		qt_idade_min_w,
		qt_idade_max_w,
		cd_motivo_baixa_w,
		ie_cobra_paciente_w,
		qt_minuto_aplicacao_w,
		qt_vol_adic_reconst_w,
		ie_proporcao_w,
		qt_referencia_w,
		nr_seq_restricao_w,
		cd_unid_med_reconst_w,
		ie_substitui_tempo_Atual_w,
		ie_priorizar_min_w,
		ie_gerar_lote_w,
		nr_seq_mat_diluicao_w;
	exit when C02%NOTFound;
		ie_diluente_w	:= 'S';
	end loop;	
	close C02;
	
	if	(nvl(qt_referencia_w,0) > 0) then
		qt_referencia_w	:= obter_conversao_unid_med_cons(cd_diluente_w,cd_unid_med_diluente_w,qt_referencia_w);
	end if;
	

	
	if	(ie_diluente_w = 'S') and	
		(nvl(cd_diluente_w,0) > 0) then
	
		nr_sequencia_w	:= nr_seq_acum_w + 1;

		select	max(substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMS'),1,30))
		into	cd_unidade_medida_w
		from	material
		where	cd_material	= cd_diluente_w;
		
		select	nvl(max(qt_conversao),1)
		into	qt_conversao_w
		from	material_conversao_unidade
		where	cd_material		= cd_diluente_w
		and	cd_unidade_medida	= cd_unid_med_diluente_w;

		if	(ie_atualiza_reconst_w = 'N') then
			begin
			delete	paciente_atend_medic
			where	nr_seq_atendimento = nr_seq_atendimento_p
			and	ie_agrupador  = 9
			and	nr_seq_diluicao = nr_seq_material_p;
			end;
		end if;
		
		Select	count(*)
		into	Controle_w
		from	paciente_atend_medic
		where	nr_seq_atendimento = nr_seq_atendimento_p
		and	ie_agrupador  = 9
		and	nr_seq_diluicao = nr_seq_material_p;

		qt_unitaria_w	:= dividir(qt_diluicao_w,qt_conversao_w);
		
		
		/*insert into logX_tasy
		values(sysdate,nm_usuario_original_w,55878,'4 Diluicao/Reconst: '|| TO_CHAR(nr_prescricao_p) ||' '|| nr_sequencia_p ||' '||ie_opcao_p ||' '||Controle_w||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss') ||' '||TO_CHAR(obter_funcao_ativa) ||' '||TO_CHAR(obter_perfil_ativo));
		commit;*/

		if 	(Controle_w = 0) then
			begin
		
			Select	count(*)
			into	Controle_ww
			from	paciente_atend_medic
			where	nr_seq_atendimento = nr_seq_atendimento_p
			and	ie_agrupador  = 3
			and	nr_seq_diluicao = nr_seq_material_p;
			
			if	(Controle_ww = 0) then
				begin
				if	(qt_minuto_aplicacao_w > 0) then
					if	(qt_minuto_aplicacao_w < 60) then
						qt_hora_aplicacao_w	:= null;
					elsif	(qt_minuto_aplicacao_w = 60) then
						qt_hora_aplicacao_w	:= 1;
						qt_minuto_aplicacao_w	:= null;
					else
						qt_hora_aplicacao_w	:= trunc(dividir(qt_minuto_aplicacao_w,60));		
						qt_minuto_aplicacao_w	:= (qt_minuto_aplicacao_w - (qt_hora_aplicacao_w * 60));
					end if;
				end if;
				
				if	(qt_minuto_aplicacao_w = 0) then
					qt_minuto_aplicacao_w	:= null;
				end if;

				update	paciente_atend_medic
				set	qt_min_aplicacao	= qt_minuto_aplicacao_w,
					qt_hora_aplicacao	= qt_hora_aplicacao_w
				where	nr_seq_atendimento 	= nr_seq_atendimento_p
				and	nr_seq_material		= nr_seq_material_p
				and	ie_aplic_bolus		= 'N'
				and	ie_aplic_lenta		= 'N'
				and 	((nvl(ie_substitui_tempo_Atual_w,'N') = 'S') or
					 (qt_min_aplicacao is null and qt_hora_aplicacao is null));
				
				end;
			/*insert into logX_tasy
			values(sysdate,nm_usuario_original_w,55878,'5 Diluicao/Reconst: '|| TO_CHAR(nr_prescricao_p) ||' '|| nr_sequencia_p ||' '||ie_opcao_p ||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss') ||' '||TO_CHAR(obter_funcao_ativa) ||' '||TO_CHAR(obter_perfil_ativo));
			commit;*/
			end if;
			

			INSERT INTO PACIENTE_ATEND_MEDIC      ( NR_SEQ_DILUICAO, 
								NR_SEQ_MATERIAL, 
								NR_AGRUPAMENTO, 
								DS_RECOMENDACAO, 
								NR_SEQ_ATENDIMENTO, 
								CD_MATERIAL, 
								QT_DOSE, 
								CD_UNID_MED_DOSE, 
								IE_VIA_APLICACAO, 
								DT_ATUALIZACAO, 
								NM_USUARIO, 
								QT_MIN_APLICACAO, 
								IE_BOMBA_INFUSAO, 
								QT_HORA_APLICACAO, 
								CD_INTERVALO, 
								NR_SEQ_INTERNO, 
								QT_DIAS_UTIL, 
								IE_SE_NECESSARIO, 
								DS_OBSERVACAO, 
								IE_URGENCIA, 
								IE_APLIC_BOLUS, 
								IE_APLIC_LENTA, 
								CD_UNID_MED_PRESCR, 
								QT_DOSE_PRESCRICAO, 
								IE_PRE_MEDICACAO,								
								NR_SEQ_SOLUCAO, 
								IE_GERAR_SOLUCAO, 
								PR_REDUCAO, 
								NR_SEQ_RECONSTITUINTE, 
								IE_AGRUPADOR, 
								IE_APLICA_REDUCAO, 
								IE_ZERADO, 
								IE_REGRA_DILUICAO_CAD_MAT,
								NR_SEQ_MAT_DILUICAO,
								IE_CANCELADA,
								IE_MEDICACAO_PACIENTE) 
			 VALUES ( 
								nr_seq_material_p, 
								nr_sequencia_w, 
								0, 
								null, 
								nr_seq_atendimento_p, 
								cd_diluente_w, 
								qt_diluicao_w,
								nvl(cd_unid_med_diluente_w,cd_unidade_medida_w), 
								ie_via_aplicacao_w, 
								sysdate, 
								nm_usuario_p, 
								null, 
								ie_bomba_infusao_w, 
								null, 
								null, 
								PACIENTE_ATEND_MEDIC_seq.nextval, 
								null, 
								null, 
								null, 
								null, 
								null, 
								null, 
								cd_unid_med_diluente_w, 
								qt_diluicao_w, 
								'N', 
								null, 
								'N', 
								null, 
								null, 
								9, 
								'N', 
								'N', 
								'N',
								nr_seq_mat_diluicao_w,
								'N',
								ie_medicacao_paciente_w);				
			
			
			/*insert into logX_tasy
			values(sysdate,nm_usuario_original_w,55878,'6 Diluicao/Reconst: '|| TO_CHAR(nr_prescricao_p) ||' '|| nr_sequencia_p ||' '||ie_opcao_p ||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss') ||' '||TO_CHAR(obter_funcao_ativa) ||' '||TO_CHAR(obter_perfil_ativo));
			commit;*/
						
				
			end;
		end if;
	end if;

	/* Inserir o Diluente */
	if	(ie_gerar_diluicao_ww = 'S') then
		
		select	nvl(max(nr_seq_material),0), 
			nvl(max(nr_agrupamento),0)
		into	nr_seq_acum_w, 
			nr_agrup_acum_w
		from	paciente_atend_medic
		where	nr_seq_atendimento = nr_seq_atendimento_p;
	
		/*Select	nvl(cd_material,0),
			cd_intervalo,
			nvl(nr_agrupamento,0),
			nvl(cd_unidade_medida,'ml'),
			nvl(nr_ocorrencia,1),
			nvl(nm_usuario,'Tasy'),
			ie_via_aplicacao,
			ds_dose_diferenciada
		into	cd_material_w,
			cd_intervalo_w,
			nr_agrupamento_w,
			cd_unidade_medida_w,
			nr_ocorrencia_w,
			nm_usuario_w,
			ie_via_aplicacao_w,
			ds_dose_diferenciada_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_p
		and	nr_sequencia  = nr_sequencia_p
		and	cd_material is not null;
		*/
		if	(ie_via_aplicacao_w is not null) then
			select	max(ie_gera_diluicao)
			into	ie_gera_diluicao_w
			from	via_aplicacao
			where	ie_via_aplicacao = ie_via_aplicacao_w;
		end if;

		ie_diluente_w	:= 'N';
		
		open C01;
		loop
		fetch C01 into
			cd_diluente_w,
			qt_diluicao_w,
			cd_unid_med_diluente_w,
			ie_reconstituicao_w,
			ie_via_aplic_order_w,
			qt_minuto_aplicacao_w,
			cd_intervalo_cad_w,
			nr_seq_prioridade_w,
			cd_setor_w,
			qt_idade_min_w,
			qt_idade_max_w,
			cd_motivo_baixa_w,
			ie_cobra_paciente_w,
			ie_proporcao_w,
			qt_referencia_w,
			nr_seq_restricao_w,
			nr_seq_interno_w,
			ie_substitui_tempo_Atual_w,
			ie_ConsisteDoseConsumoMedic_w,
			ie_gerar_lote_w,
			nr_seq_mat_diluicao_w,
			qt_volume_medic_w;
		exit when C01%notfound;
			ie_diluente_w	:= 'S';
		end loop;	
		close C01;
		
		/*insert into logX_tasy
		values(sysdate,nm_usuario_original_w,55878,'7 Diluicao/Reconst: '|| TO_CHAR(nr_prescricao_p) ||' '|| nr_sequencia_p ||' '||ie_opcao_p|| ' '||ie_diluente_w|| ' '||qt_referencia_w||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss') ||' '||TO_CHAR(obter_funcao_ativa) ||' '||TO_CHAR(obter_perfil_ativo));
		commit;*/

		if	(ie_diluente_w = 'S') and	
			(nvl(cd_diluente_w,0) > 0) then
			
			select	max(ie_tipo_material),
				max(nr_seq_ficha_tecnica)
			into	ie_tipo_material_w,
				nr_seq_ficha_tecnica_w
			from	material
			where	cd_material	= cd_diluente_w;			
			
			qt_dose_diluicao_w	:= qt_diluicao_w;
			qt_referencia_aux_w	:= qt_referencia_w;
			if	(nvl(qt_referencia_w,0) > 0) then
				qt_referencia_w	:= obter_conversao_unid_med_cons(cd_diluente_w,cd_unid_med_diluente_w,qt_referencia_w);
			end if;
			
			if	(ie_tipo_material_w	= '6')	then
				
				open C05;
				loop
				fetch C05 into	
					cd_dil_w;
				exit when C05%notfound;
					cd_dil_w	:= cd_dil_w;
				end loop;
				close C05;
				
				if	(cd_dil_w > 0) then
					cd_diluente_w	:= cd_dil_w;
				end if;
			end if;
		
			nr_sequencia_w	:= nr_seq_acum_w + 1;

			if	(ie_proporcao_w = 'F') and
				(ie_ConsisteDoseConsumoMedic_w = 'N') and
				(nvl(qt_volume_medic_w,0)	> 0) then
				qt_diluicao_w	:= obter_conversao_unid_med_cons(cd_material_w,obter_unid_med_usua('ml'),qt_volume_medic_w);
			elsif	(nvl(qt_referencia_w,0) > 0) then
				if	(ie_proporcao_w = 'SC') then
					if	(ie_ConsisteDoseConsumoMedic_w = 'S') then
						qt_diluicao_w	:= ceil(qt_unitaria_medic_w) * qt_referencia_w; 
					else
						qt_diluicao_w	:= qt_unitaria_medic_w * qt_referencia_w; 
					end if;
				elsif	(ie_proporcao_w = 'ST') then
					if	(ie_ConsisteDoseConsumoMedic_w = 'S') then
						qt_diluicao_w		:= ceil(qt_unitaria_medic_w) * qt_referencia_w;
					else
						qt_diluicao_w		:= qt_unitaria_medic_w * qt_referencia_w;
					end if;
					qt_dose_aux_w		:= obter_conversao_unid_med_cons(cd_material_w, cd_unidade_medida_dose_w, qt_dose_medic_w);
					--qt_referencia_aux_w	:= obter_conversao_unid_med_cons(cd_material_w, cd_unid_med_diluente_w, qt_referencia_aux_w);
					qt_minuto_aplicacao_w	:= trunc(qt_minuto_aplicacao_w * qt_dose_aux_w);
				end if;
			end if;
			
			select	max(substr(obter_dados_material_estab(cd_material,cd_estabelecimento_w,'UMS'),1,30))
			into	cd_unidade_medida_w
			from	material
			where	cd_material	= cd_diluente_w;

			select	nvl(max(qt_conversao),1)
			into	qt_conversao_w
			from	material_conversao_unidade
			where	cd_material		= cd_diluente_w
			and	cd_unidade_medida	= cd_unid_med_diluente_w;

			Select	count(*)
			into	Controle_ww
			from	paciente_atend_medic
			where	nr_seq_atendimento = nr_seq_atendimento_p
			and	ie_agrupador  = 3
			and	nr_seq_diluicao = nr_seq_material_p;
			
			/*insert into logX_tasy
			values(sysdate,nm_usuario_original_w,55878,'8 Diluicao/Reconst: '|| TO_CHAR(nr_prescricao_p) ||' '|| nr_sequencia_p ||' '||ie_opcao_p|| ' '||Controle_w||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss') ||' '||TO_CHAR(obter_funcao_ativa) ||' '||TO_CHAR(obter_perfil_ativo));
			commit;*/
			
			if	(Controle_w = 0) then

				if	(qt_minuto_aplicacao_w > 0) then
					if	(qt_minuto_aplicacao_w < 60) then
						qt_hora_aplicacao_w	:= null;
					elsif	(qt_minuto_aplicacao_w = 60) then
						qt_hora_aplicacao_w	:= 1;
						qt_minuto_aplicacao_w	:= null;
					else
						qt_hora_aplicacao_w	:= trunc(dividir(qt_minuto_aplicacao_w,60));		
						qt_minuto_aplicacao_w	:= (qt_minuto_aplicacao_w - (qt_hora_aplicacao_w * 60));
					end if;
				end if;
				
				if	(qt_minuto_aplicacao_w = 0) then
					qt_minuto_aplicacao_w	:= null;
				end if;

				/* Ivan em 10/07/2007 OS62199 - 
				Inclusao das restricoes por bolus e lentamente para nao desativar os dois campos ao mesmo tempo */
				update	paciente_atend_medic
				set	qt_min_aplicacao	= qt_minuto_aplicacao_w,
					qt_hora_aplicacao	= qt_hora_aplicacao_w
				where	nr_seq_atendimento 	= nr_seq_atendimento_p
				and	nr_seq_material		= nr_seq_material_p
				and	ie_aplic_bolus		= 'N'
				and	ie_aplic_lenta		= 'N'
				and	((ie_priorizar_min_w	= 'N') or
					 (qt_min_aplicacao is null and	
					  qt_hora_aplicacao is null))
				and 	((nvl(ie_substitui_tempo_Atual_w,'N') = 'S' and
					 ie_proporcao_w <> 'ST') or
					 (qt_min_aplicacao is null and	
					  qt_hora_aplicacao is null));
					  
				
				if	(nvl(qt_referencia_w,0) > 0) and
					(ie_proporcao_w = 'ST') then
					update	paciente_atend_medic
					set	qt_min_aplicacao	= qt_minuto_aplicacao_w,
						qt_hora_aplicacao	= qt_hora_aplicacao_w
					where	nr_seq_atendimento 	= nr_seq_atendimento_p
					and	nr_seq_material		= nr_seq_material_p
					and	ie_aplic_bolus		= 'N'
					and	ie_aplic_lenta		= 'N';
				end if;
				
			
				
			INSERT INTO PACIENTE_ATEND_MEDIC      ( NR_SEQ_DILUICAO, 
								NR_SEQ_MATERIAL, 
								NR_AGRUPAMENTO, 
								DS_RECOMENDACAO, 
								NR_SEQ_ATENDIMENTO, 
								CD_MATERIAL, 
								QT_DOSE, 
								CD_UNID_MED_DOSE, 
								IE_VIA_APLICACAO, 
								DT_ATUALIZACAO, 
								NM_USUARIO, 
								QT_MIN_APLICACAO, 
								IE_BOMBA_INFUSAO, 
								QT_HORA_APLICACAO, 
								CD_INTERVALO, 
								NR_SEQ_INTERNO, 
								QT_DIAS_UTIL, 
								IE_SE_NECESSARIO, 
								DS_OBSERVACAO, 
								IE_URGENCIA, 
								IE_APLIC_BOLUS, 
								IE_APLIC_LENTA, 
								CD_UNID_MED_PRESCR, 
								QT_DOSE_PRESCRICAO, 
								IE_PRE_MEDICACAO, 
								NR_SEQ_SOLUCAO, 
								IE_GERAR_SOLUCAO, 
								PR_REDUCAO, 
								NR_SEQ_RECONSTITUINTE, 
								IE_AGRUPADOR, 
								IE_APLICA_REDUCAO, 
								IE_ZERADO, 
								IE_REGRA_DILUICAO_CAD_MAT ,
								nr_seq_mat_diluicao,
								IE_CANCELADA,
								IE_MEDICACAO_PACIENTE) 
			 VALUES ( 
								nr_seq_material_p, 
								nr_sequencia_w, 
								0, 
								null, 
								nr_seq_atendimento_p, 
								cd_diluente_w, 
								qt_diluicao_w,
								nvl(cd_unid_med_diluente_w,cd_unidade_medida_w), 
								ie_via_aplicacao_w, 
								sysdate, 
								nm_usuario_p, 
								null, 
								ie_bomba_infusao_w, 
								null, 
								null, 
								PACIENTE_ATEND_MEDIC_seq.nextval, 
								null, 
								null, 
								null, 
								null, 
								null, 
								null, 
								cd_unid_med_diluente_w, 
								qt_diluicao_w, 
								'N', 
								null, 
								'N', 
								null, 
								null, 
								3, 
								'N', 
								'N', 
								'N' ,
								nr_seq_mat_diluicao_w,
								'N',
								ie_medicacao_paciente_w);	
			/*insert into logX_tasy
			values(sysdate,nm_usuario_original_w,55878,'9 Diluicao/Reconst: '|| TO_CHAR(nr_prescricao_p) ||' '|| nr_sequencia_p ||' '||ie_opcao_p|| ' '||TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss') ||' '||TO_CHAR(obter_funcao_ativa) ||' '||TO_CHAR(obter_perfil_ativo));
			commit;*/
			
			
				

			elsif	(ie_gera_dil_dose_w	= 'S') then
				null;
			end if; 
		end if;
	end if;
	/* inserir o rediluente*/	
	
	
end if;
/*
Ajustar_Dose_Diluente (nr_prescricao_p, nr_sequencia_p);

if	(ie_calcula_volume_afterpost_w = 'S') then
	calcular_vol_dil_Prescr_mat(nr_prescricao_p, nr_agrupamento_w, nm_usuario_original_w);
end if;
*/
END Gerar_Recons_Dil_onc_atend;
/
