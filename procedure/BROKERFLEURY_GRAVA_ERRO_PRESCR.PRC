create or replace 
procedure brokerfleury_grava_erro_prescr(
				nr_prescricao_p		Number,
				nr_seq_proced_p		Number,			
				ds_regra_prescr_p	varchar2,
				ds_mensagem_p		varchar2,
				nm_usuario_p		Varchar2) is

nr_sequencia_w			number(10);
ie_libera_prescr_w		varchar2(15);
ds_mensagem_w			varchar2(4000);
ds_regra_prescr_w		varchar2(255);
ie_usuario_freq_w		varchar2(1)	:= 'N';

BEGIN

select	prescr_medica_erro_seq.nextval
into	nr_sequencia_w
from	dual;

insert into prescr_medica_erro(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_prescricao,
	ds_inconsistencia,
	ie_libera,
	ie_tipo_regra,
	ds_erro,	
	nr_seq_proced,
	ie_tipo)
values(	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_prescricao_p,
	substr(ds_regra_prescr_p,1,255),
	'N',
	'P',
	substr(ds_mensagem_p,1,4000),
	nr_seq_proced_p,
	'E');


END brokerfleury_grava_erro_prescr;
/