create or replace
procedure gerar_anexos_agenda_autor_conv(	nr_seq_autorizacao_p	number,
						nr_seq_agenda_p		number,
						nm_usuario_p		Varchar2) is 
ie_anexar_w			Varchar(1);
					
begin
if	(nr_seq_agenda_p > 0) and
        (nr_seq_autorizacao_p > 0) then
	begin
	ie_anexar_w := Obter_Regra_Atributo2('AUTORIZACAO_CIRURGIA_ARQ','IE_ANEXAR_EMAIL',0,'VLD',wheb_usuario_pck.get_cd_estabelecimento,obter_perfil_ativo,obter_funcao_ativa,nm_usuario_p);
	
	insert into autorizacao_cirurgia_arq (	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_autorizacao,
					ds_arquivo,
					ie_anexar_email)
				select	autorizacao_cirurgia_arq_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_autorizacao_p,
					ds_arquivo,
					ie_anexar_w
				from	anexo_agenda a
				where	a.nr_seq_agenda	= nr_seq_agenda_p
				and	nvl(a.ie_situacao,'A')   = 'A'
				and	not exists(	select	1
						from	autorizacao_cirurgia_arq b
						where	b.ds_arquivo		= a.ds_arquivo
						and	b.nr_seq_autorizacao 	= nr_seq_autorizacao_p);
	commit;
	end;
end if;

end gerar_anexos_agenda_autor_conv;
/