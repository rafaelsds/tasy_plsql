/* CREATE TABLE qua_tabela_traduzir (nm_tabela VARCHAR2(50), nm_atributo VARCHAR2(50)) */

/* CREATE TABLE qua_texto_traduzir (ds_texto VARCHAR2(4000), ds_traducao VARCHAR2(4000)) */

create or replace
procedure TRA_VERIFICA_EXPRESSOES_CORP is

cd_expressao_w			dic_expressao.cd_expressao%type;

vl_campo_ds_w			varchar2(4000);
vl_campo_exp_w			varchar2(4000);
nm_usuario_w			varchar2(50);
retorno_w				number(10);
qt_existe_w				number(10);


ds_sep_bv_w				varchar2(50);
ds_comando_select_w		varchar2(4000);
ds_comando_update_w		varchar2(4000);
nm_user_w 				varchar2(10);

ds_erro_w				varchar2(4000);

nm_tabela_w				corp_tabelas_traducao.nm_tabela%type;
nm_atributo_w			corp_tabelas_traducao.nm_atributo%type;
nm_atributo_exp_w		corp_tabelas_traducao.nm_atributo_exp%type;

cur_id					number;
TYPE CurType 			IS REF CURSOR;
C02						CurType;

Cursor C01 is
	SELECT	nm_tabela,
			nm_atributo
	FROM   	corp_tabelas_traducao;

begin
ds_sep_bv_w	:= obter_separador_bv;

select	substr(user,1,4)
into 	nm_user_w
from 	dual;

open C01;
loop
fetch C01 into
	nm_tabela_w,
	nm_atributo_w;
exit when C01%notfound;
	begin

	ds_comando_select_w := 'select	' || nm_atributo_w ||
							' from	' || nm_tabela_w ||
							' where ' || nm_atributo_w || ' is not null ';

	BEGIN
		cur_id := DBMS_SQL.OPEN_CURSOR;
		DBMS_SQL.PARSE(cur_id, ds_comando_select_w, DBMS_SQL.NATIVE);
		DBMS_SQL.DEFINE_COLUMN(cur_id, 1, vl_campo_ds_w, 4000);
		--DBMS_SQL.DEFINE_COLUMN(cur_id, 2, vl_campo_exp_w, 4000);
		--DBMS_SQL.DEFINE_COLUMN(cur_id, 3, nm_usuario_w, 50);

		retorno_w := DBMS_SQL.execute(cur_id);

		WHILE DBMS_SQL.FETCH_ROWS(cur_id) > 0 LOOP

			DBMS_SQL.COLUMN_VALUE(cur_id, 1, vl_campo_ds_w);
			--DBMS_SQL.COLUMN_VALUE(cur_id, 2, vl_campo_exp_w);
			--DBMS_SQL.COLUMN_VALUE(cur_id, 3, nm_usuario_w);



			/*Obter_valor_Dinamico_bv('select	max(a.cd_expressao) '||
									'from	dic_expressao@whebl02_orcl a '||
									'where	a.ds_expressao_br	= :ds_expressao_br_p','ds_expressao_br_p=' || vl_campo_ds_w, cd_expressao_w);*/

			Obter_valor_Dinamico(	'SELECT	max(a.cd_expressao) '||
									'FROM	dic_expressao@whebl02_orcl a '||
									'WHERE	a.ds_expressao_br = ' || chr(39) || vl_campo_ds_w || chr(39), cd_expressao_w);

			-- Existe na DIC_EXPRESSAO
			if	(nvl(cd_expressao_w, 0) > 0) then


				Obter_valor_Dinamico_bv('select	nvl(max(a.cd_expressao),0) '||
										'from	dic_expressao@whebl02_orcl a '||
										'where	a.ds_expressao_us is not null ' ||
										'and 	a.cd_expressao	= :cd_expressao_p','cd_expressao_p=' || cd_expressao_w, cd_expressao_w);

				-- Existe a express�o por�m n�o tem tradu��o
				if	(cd_expressao_w = 0) then

					select	count(*)
					into	qt_existe_w
					from	qua_texto_traduzir
					where	ds_texto = vl_campo_ds_w;

					if	(qt_existe_w = 0) then
						insert into qua_texto_traduzir (ds_texto) values (vl_campo_ds_w);
					end if;

				end if;

				--insert into LOG_CORP_TABELAS_TRADUCAO (nm_tabela, ds_log, dt_execucao) values (nm_tabela_p, 'ds_comando_update_w= ' || ds_comando_update_w ||
				--																						' cd_exp_retorno_w = ' || cd_exp_retorno_w ||
				--																							' vl_campo_ds_w = ' || vl_campo_ds_w, null);

			else
				select	count(*)
				into	qt_existe_w
				from	qua_texto_traduzir
				where	ds_texto = vl_campo_ds_w;

				-- N�o existe a express�o na DIC_EXPRESSAO
				if	(qt_existe_w = 0) then
					insert into qua_texto_traduzir (ds_texto) values (vl_campo_ds_w);
				end if;

			end if;


		END LOOP;
		DBMS_SQL.CLOSE_CURSOR(cur_id);

	EXCEPTION
		WHEN OTHERS THEN
		ds_erro_w := SUBSTR(SQLERRM,1,4000);
		insert into qua_texto_traduzir (ds_traducao) values (ds_erro_w);
	END;

	end;
end loop;
close C01;

commit;

end TRA_VERIFICA_EXPRESSOES_CORP;
/