create or replace
procedure atualiza_tributo_baixa_mx(nr_titulo_p	number,
				nr_seq_baixa_p	number,
				nm_usuario_p	varchar2) is

				
vl_baixa_w			titulo_pagar_baixa.vl_baixa%type;
cd_tributo_w			titulo_pagar_imposto.cd_tributo%type;	
vl_imposto_w			titulo_pagar_imposto.vl_imposto%type;	
vl_base_calculo_w		titulo_pagar_imposto.vl_base_calculo%type;
pr_aliquota_w			titulo_pagar_imposto.pr_imposto%type;
vl_imposto_baixa_w		titulo_pagar_baixa.vl_imposto%type;
pr_aliquota_inv_w		Number(15,2);

vl_baixa_consistencia_w		titulo_pagar_baixa.vl_baixa%type;
vl_saldo_multa_w		titulo_pagar.vl_saldo_multa%type;
vl_multa_w			titulo_pagar_baixa.vl_multa%type;
vl_saldo_juros_w		titulo_pagar.vl_saldo_juros%type;
vl_juros_w			titulo_pagar_baixa.vl_juros%type;
vl_saldo_titulo_w		titulo_pagar.vl_saldo_titulo%type;
dt_baixa_w			titulo_pagar_baixa.dt_baixa%type;			
vl_outras_despesas_w		titulo_pagar.vl_outras_despesas%type;
vl_outros_acrescimos_w		titulo_pagar.vl_outros_acrescimos%type;
ds_erro_w			varchar2(4000);
vl_trib_baixas_w		titulo_pagar_baixa.vl_imposto%type;
vl_dif_imposto_w		titulo_pagar_imposto.vl_imposto%type;	

begin

if (philips_param_pck.get_cd_pais = 2) then -- MX

	/*Como so tem IVA, foi retirado o cursor, e  esse select apenas pega o valor do imposto IVA do titulo
	O valor do imposto vem da tabela titulo_pagar_trib_baixa, pois o c�lculo j� foi realizado e gravado nela */
	select	nvl(max(c.vl_baixa),0)
	into	vl_imposto_w
	from	titulo_pagar_imposto a,
		tributo b,
		titulo_pagar_trib_baixa c
	where	a.cd_tributo 		= b.cd_tributo
	and	a.nr_sequencia	= c.nr_seq_tit_trib
	and	b.ie_tipo_tributo = 'IVA'
	and	a.nr_titulo 	= nr_titulo_p
	and	c.nr_seq_tit_baixa = nr_seq_baixa_p;
	
	update	titulo_pagar_baixa
	set	vl_imposto	= nvl(vl_imposto_w,0)
	where	nr_titulo	= nr_titulo_p
	and	nr_sequencia	= nr_seq_baixa_p;	
	

	/*Inicio altera��o OS 1094887 conforme sugest�o do analista no hist�rico  do dia 13/07/2016 09:34:11*/
	
	/*Mesma procedure utilizada na Atualizar_Saldo_Tit_Pagar, que verifica e define o saldo do titulo. 
	Precisamos saber o saldo do t�tulo aqui, se for 0, vamos veriifcar se a soma das baixas de tributos existentes  se equivale com o valor do imposto no titulo */
	consistir_tit_pagar_baixa(	nr_titulo_p,
					vl_baixa_consistencia_w,
					vl_saldo_multa_w,
					vl_multa_w,
					vl_saldo_juros_w,
					vl_juros_w,
					vl_saldo_titulo_w,
					dt_baixa_w,
					vl_outras_despesas_w,
					vl_outros_acrescimos_w,
					ds_erro_w );
	
	/*Se o saldo for 0, significa que o t�tulo ser� liquidado, entao temos que verificar o arredondamento desta ultima baixa*/
	if (vl_saldo_titulo_w = 0) then
	
		/*Buscar a soma de todos os impostos das baixas do titulo*/
		select	nvl(sum(a.vl_imposto),0)
		into	vl_trib_baixas_w
		from	titulo_pagar_baixa a
		where	nr_titulo = nr_titulo_p;
		
		if ( nvl(vl_trib_baixas_w,0) >  nvl(vl_imposto_w,0) ) then
		
			/*Conforme sugest�o do analista, se a diferen�a for menor que 0,05, vamos arredondar, sen�o n�o. hist�rico  do dia 13/07/2016 09:34:11*/
			vl_dif_imposto_w := nvl(vl_trib_baixas_w,0) - nvl(vl_imposto_w,0);
			if (vl_dif_imposto_w <= 0.5) then
			
				update	titulo_pagar_baixa
				set		vl_imposto		= nvl(vl_imposto,0) - nvl(vl_dif_imposto_w,0)
				where	nr_titulo		= nr_titulo_p
				and		nr_sequencia	= nr_seq_baixa_p; 
			
			end if;
		
		elsif ( nvl(vl_trib_baixas_w,0) <  nvl(vl_imposto_w,0) ) then
		
			vl_dif_imposto_w := nvl(vl_imposto_w,0) - nvl(vl_trib_baixas_w,0);
			if (vl_dif_imposto_w <= 0.5) then
			
				update	titulo_pagar_baixa
				set		vl_imposto		= nvl(vl_imposto,0) + nvl(vl_dif_imposto_w,0)
				where	nr_titulo		= nr_titulo_p
				and		nr_sequencia	= nr_seq_baixa_p; 
			
			end if;
		
		end if;
		
	end if;
		
	/*Fim altera��o OS 1094887 conforme sugest�o do analista no hist�rico  do dia 13/07/2016 09:34:11*/
   
end if;

end atualiza_tributo_baixa_mx;
/