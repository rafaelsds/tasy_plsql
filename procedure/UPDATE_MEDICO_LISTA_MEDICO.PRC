CREATE OR REPLACE PROCEDURE UPDATE_MEDICO_LISTA_MEDICO(
    nm_usuario_p              VARCHAR2,
    nr_seq_lista_p            NUMBER,
    nr_prescricao_p           NUMBER,
    nr_sequencia_prescricao_p NUMBER)

IS
  nm_usuario_w              lista_central_exame.nm_usuario%type;
  nr_seq_lista_w            lista_central_exame.nr_seq_lista_medico%type;
  nr_prescricao_w           lista_central_exame.nr_prescricao%type;
  nr_sequencia_prescricao_w lista_central_exame.nr_sequencia_prescricao%type;
  nr_ordem_apresentacao_w   lista_central_exame.nr_ordem_apresentacao%type; 
  ie_contido_mesma_lista    VARCHAR2(1);

BEGIN
  nm_usuario_w              := nm_usuario_p;
  nr_seq_lista_w            := nr_seq_lista_p;
  nr_prescricao_w           := nr_prescricao_p;
  nr_sequencia_prescricao_w := nr_sequencia_prescricao_p;

  SELECT decode(count(1), 0, 'N', 'S')
  INTO ie_contido_mesma_lista 
  FROM lista_central_exame a
  WHERE a.nr_seq_lista_medico = nr_seq_lista_w
  AND a.nr_prescricao = nr_prescricao_w
  AND a.nr_sequencia_prescricao = nr_sequencia_prescricao_w;

  IF (ie_contido_mesma_lista = 'N') THEN
  BEGIN
    SELECT NVL(MAX(a.nr_ordem_apresentacao),0)+5
    INTO nr_ordem_apresentacao_w
    FROM lista_central_exame a
    WHERE a.nr_seq_lista_medico = nr_seq_lista_w
    AND a.nr_prescricao IN (SELECT pp.nr_prescricao
                            FROM prescr_procedimento pp
                            WHERE a.nr_prescricao         = pp.nr_prescricao
                            AND a.nr_sequencia_prescricao = pp.nr_sequencia
                            AND pp.ie_status_execucao     = '20');
                            
    UPDATE lista_central_exame
    SET nr_seq_lista_medico = nr_seq_lista_w,
        nm_usuario = nm_usuario_w,
        nr_ordem_apresentacao = nr_ordem_apresentacao_w      
    WHERE nr_prescricao = nr_prescricao_w
    AND nr_sequencia_prescricao = nr_sequencia_prescricao_w; 
   
    COMMIT;
  END;
  END IF;

END UPDATE_MEDICO_LISTA_MEDICO;
/