create or replace
procedure lic_aprovar_documentacao(	nr_sequencia_p		number,
				nm_usuario_p		varchar2) is 

begin

update	reg_lic_fornec_docto
set	dt_aprovacao		= sysdate,
	ie_revisado		= 'S',
	nm_usuario_aprov		= nm_usuario_p
where	nr_sequencia		= nr_sequencia_p;

commit;

end lic_aprovar_documentacao;
/