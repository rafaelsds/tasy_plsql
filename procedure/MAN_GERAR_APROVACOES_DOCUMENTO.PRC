create or replace
procedure man_gerar_aprovacoes_documento(
			nr_documento_p	number,
			nm_usuario_p	varchar2) is 

cursor c01 is
	select	cd_pessoa_fisica
	from	pessoa_fisica
	where	cd_pessoa_fisica in (134,4933,11106);

v01	c01%rowtype;
	
begin

open c01;
loop
fetch c01 into	
	v01;
exit when c01%notfound;
	begin
	
	insert into desenv_art_doc_aprovacao(
		cd_pessoa_fisica,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_documento,
		nr_sequencia)
	values(	v01.cd_pessoa_fisica,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_documento_p,
		desenv_art_doc_aprovacao_seq.nextval);
	
	end;
end loop;
close c01;

commit;

end man_gerar_aprovacoes_documento;
/