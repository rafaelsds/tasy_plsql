create or replace
procedure pls_atual_campos_repasse_mens
		(	nr_seq_repasse_mens_p	number,
			nm_usuario_p		Varchar2) is 

nr_seq_mens_seg_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_plano_w			number(10);
nr_seq_sca_w			number(10);
nm_segurado_w			varchar2(255);
nr_parcela_contrato_w		number(10);
nm_estipulante_w		varchar2(255);
nm_pagador_w			varchar2(255);
ie_tipo_contrato_ww		varchar2(10);
vl_pre_estab_w			number(15,2);
ds_acao_contrato_w		varchar2(255);
dt_adesao_contrato_w		date;
dt_liberacao_beneficiario_w	date;
vl_mensalidade_ww		number(15,2);
nm_vendedor_vinculado_w		varchar2(255);
nr_contrato_w			number(10);
nr_seq_lote_w			number(10);
nm_produto_w			varchar2(255);
nm_sca_w			varchar2(255);
nm_subestipulante_w		varchar2(255);
nr_parcela_w			number(10);
nr_seq_contrato_w		number(10);
qt_idade_w			number(10);
nr_seq_mensalidade_w		number(10);
dt_mesano_referencia_w		date;
nr_seq_intercambio_w		number(10);
nr_seq_vendedor_benef_w		number(10);
nr_seq_repasse_w		pls_repasse_mens.nr_seq_repasse%type;	
ie_tipo_repasse_w		pls_repasse_vend.ie_tipo_repasse%type;
nr_seq_proposta_w		pls_repasse_mens.nr_seq_proposta%type;
nr_titulo_w			titulo_receber.nr_titulo%type;

begin

select	nr_seq_repasse,
	nr_seq_mens_seg,
	nr_seq_segurado,
	nr_seq_plano,
	nr_seq_plano_sca
into	nr_seq_repasse_w,
	nr_seq_mens_seg_w,
	nr_seq_segurado_w,
	nr_seq_plano_w,
	nr_seq_sca_w
from	pls_repasse_mens
where	nr_sequencia = nr_seq_repasse_mens_p;

begin
select	ie_tipo_repasse
into	ie_tipo_repasse_w
from	pls_repasse_vend
where	nr_sequencia = nr_seq_repasse_w;
exception
when others then
	ie_tipo_repasse_w := 'N';	
end;

if	(nr_seq_plano_w is not null) then
	select	substr(ds_plano,1,255)
	into	nm_produto_w
	from	pls_plano
	where	nr_sequencia = nr_seq_plano_w;
end if;

if	(nr_seq_sca_w is not null) then
	select	substr(ds_plano,1,255)
	into	nm_sca_w
	from	pls_plano
	where	nr_sequencia = nr_seq_sca_w;
end if;

select	substr(c.nm_pessoa_fisica,1,255),
	a.nr_parcela_contrato,
	substr(pls_obter_dados_pagador(b.nr_seq_pagador,'N'),1,255),
	substr(pls_obter_valor_item_mens(a.nr_sequencia,'1'),1,100),
	substr(obter_valor_dominio(2115,b.ie_acao_contrato),1,200),
	b.dt_contratacao,
	b.dt_liberacao,
	a.vl_mensalidade,
	e.nr_seq_lote,
	substr(pls_obter_dados_segurado(b.nr_sequencia,'SU'),1,255),
	substr(pls_obter_dados_segurado(b.nr_sequencia,'VPF'),1,255),
	a.nr_parcela,
	a.qt_idade,
	e.nr_sequencia,
	a.dt_mesano_referencia,
	b.nr_seq_contrato,
	b.nr_seq_intercambio,
	b.nr_seq_vendedor_canal
into	nm_segurado_w,
	nr_parcela_contrato_w,
	nm_pagador_w,
	vl_pre_estab_w,
	ds_acao_contrato_w,
	dt_adesao_contrato_w,
	dt_liberacao_beneficiario_w,
	vl_mensalidade_ww,
	nr_seq_lote_w,
	nm_subestipulante_w,
	nm_vendedor_vinculado_w,
	nr_parcela_w,
	qt_idade_w,
	nr_seq_mensalidade_w,
	dt_mesano_referencia_w,
	nr_seq_contrato_w,
	nr_seq_intercambio_w,
	nr_seq_vendedor_benef_w
from	pls_mensalidade			e,
	pessoa_fisica			c,
	pls_segurado			b,
	pls_mensalidade_segurado	a
where	b.cd_pessoa_fisica		= c.cd_pessoa_fisica
and	a.nr_seq_segurado		= b.nr_sequencia
and	a.nr_seq_mensalidade		= e.nr_sequencia
and	a.nr_sequencia			= nr_seq_mens_seg_w;

select	max(nr_titulo)
into	nr_titulo_w
from	titulo_receber
where	nr_seq_mensalidade = nr_seq_mensalidade_w;

if	(nr_seq_contrato_w is not null) then
	select	substr(obter_nome_pf_pj(cd_pf_estipulante,cd_cgc_estipulante),1,255),
		decode(cd_pf_estipulante,null,'PJ','PF'),
		nr_contrato
	into	nm_estipulante_w,
		ie_tipo_contrato_ww,
		nr_contrato_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
elsif	(nr_seq_intercambio_w is not null) then
	select	substr(obter_nome_pf_pj(cd_pessoa_fisica,cd_cgc),1,255),
		decode(cd_pessoa_fisica,null,'PJ','PF'),
		null
	into	nm_estipulante_w,
		ie_tipo_contrato_ww,
		nr_contrato_w
	from	pls_intercambio
	where	nr_sequencia	= nr_seq_intercambio_w;
end if;

if	(nr_seq_segurado_w is not null) then
	select	max(a.nr_seq_proposta)
	into	nr_seq_proposta_w
	from	pls_proposta_beneficiario a,
		pls_segurado b
	where	a.nr_sequencia            = b.nr_seq_pessoa_proposta
	and	b.nr_sequencia            = nr_seq_segurado_w;
end if;

update	pls_repasse_mens
set	nm_segurado			= nm_segurado_w,
	nr_parcela_contrato		= nr_parcela_contrato_w,
	nm_estipulante			= nm_estipulante_w,
	nm_pagador			= nm_pagador_w,
	ie_tipo_contrato			= ie_tipo_contrato_ww,
	vl_pre_estab			= vl_pre_estab_w,
	ds_acao_contrato			= ds_acao_contrato_w,
	dt_liberacao_beneficiario		= dt_liberacao_beneficiario_w,
	dt_adesao_contrato		= dt_adesao_contrato_w,
	dt_mesano_referencia		= dt_mesano_referencia_w,
	vl_mensalidade			= decode(ie_tipo_repasse_w, 'M', vl_mensalidade, vl_mensalidade_ww),
	nr_seq_mensalidade		= nr_seq_mensalidade_w,
	nr_contrato			= nr_contrato_w,
	nr_seq_lote			= nr_seq_lote_w,
	nm_subestipulante			= nm_subestipulante_w, 
	nm_vendedor_vinculado		= nm_vendedor_vinculado_w,
	nm_produto			= nm_produto_w,
	nm_sca				= nm_sca_w,
	qt_idade				= qt_idade_w,
	nr_seq_contrato			= nr_seq_contrato_w,
	nr_parcela_benef			= nr_parcela_w,
	nr_seq_vendedor_benef		= nr_seq_vendedor_benef_w,
	nr_titulo_receber			= nr_titulo_w,
	nr_seq_proposta			= nr_seq_proposta_w
where	nr_sequencia			= nr_seq_repasse_mens_p;

end pls_atual_campos_repasse_mens;
/