create or replace procedure proj_calcular_ponto(

				nr_seq_aval_p	in	number,

				nm_usuario_p	in	varchar2) is




qt_ponto_w			Number(15,2);

qt_registro_w			Number(15,0);

ie_tipo_avaliacao_w		Varchar2(03);

nr_sequencia_w			Number(15,0);

vl_hora_w			Number(15,2);

vl_unit_w			Number(15,2);

dt_avaliacao_w			Date;

nr_seq_modelo_w			Number(10,0);

nr_seq_nivel_consultor_w	Number(10,0);

ie_nivel_cliente_w		Varchar2(01);

ie_satisfacao_w			Varchar2(01);

qt_pto_nivel_w			Number(15,2);

ie_complexidade_w		Varchar2(01);

ie_funcao_exec_w		Varchar2(15);

ie_regime_contr_w		Varchar2(15);

nr_seq_quesito_w		number(10);

qt_resp_remun_var_cons_w	number(15,2);

qt_quest_remun_var_cons_w	number(15,2);

qt_resp_remun_var_coor_w	number(15,2);

qt_quest_remun_var_coor_w	number(15,2);

qt_hora_trab_total_w		number(17,2);

qt_hora_trab_w			number(17,2);

vl_hora_def_w			number(15,2);

vl_hora_faixa4_w		number(15,2);

vl_hora_faixa3_w		number(15,2);

vl_hora_faixa2_w		number(15,2);

vl_multi_bonus_w		number(15,2) := 0;



cursor c01 is

	select	b.ie_tipo_avaliacao,

		sum(a.qt_ponto)

	from	proj_avaliacao b,

		proj_consultor_aval_quesito a

	where	a.nr_seq_quesito	= b.nr_sequencia

	and	a.nr_seq_avaliacao	= nr_seq_aval_p

	group by	b.ie_tipo_avaliacao

	union all

	select	'0',

		0

	from	dual;



cursor c02 is

	select	vl_hora

	from	proj_consultor_valor

	where	ie_tipo_avaliacao	= ie_tipo_avaliacao_w

	and	nr_seq_modelo		= nr_seq_modelo_w

	and	dt_vigencia		<= dt_avaliacao_w

	and	ie_regime_contr		= ie_regime_contr_w

	and	ie_funcao_rat		= ie_funcao_exec_w

	order by dt_vigencia;



BEGIN



select	min(dt_avaliacao),

	min(nr_seq_modelo),

	min(nr_seq_nivel_consultor),

	min(ie_complexidade),

	min(ie_regime_contr),

	min(ie_funcao_exec)

into	dt_avaliacao_w,

	nr_seq_modelo_w,

	nr_seq_nivel_consultor_w,

	ie_complexidade_w,

	ie_regime_contr_w,

	ie_funcao_exec_w

from	proj_consultor_aval

where	nr_sequencia = nr_seq_aval_p;



select	nvl(max(ie_nivel_cliente),'N'),

	nvl(max(ie_satisfacao),'N'),

	max(decode(ie_regime_contr_w,'CLT', nvl(QT_PTO_NIVEL_CLT,0), nvl(qt_pto_nivel,0)))

into	ie_nivel_cliente_w,

	ie_satisfacao_w,

	qt_pto_nivel_w

from	proj_nivel_consultor

where	nr_sequencia		= nr_seq_nivel_consultor_w;



select	count(*)

into	qt_registro_w

from	proj_consultor_aval_quesito

where	nr_seq_avaliacao	= nr_seq_aval_p

and	qt_ponto		is null;



select	(	select	count(*)

		from	proj_consultor_aval a,

			proj_consultor_aval_quesito b,

			proj_aval_opcao c

		where	a.nr_sequencia = b.nr_seq_avaliacao

		and	b.nr_seq_opcao = c.nr_sequencia

		and	a.nr_sequencia = nr_seq_aval_p

		and	trunc(a.dt_avaliacao, 'month') = trunc(dt_avaliacao_w, 'month')

		and	b.ie_tipo_avaliacao = 6

		and	trim(c.ds_opcao) = 'S'	) qt_resp_remun_var_cons,

	(	select	count(*)

		from	proj_consultor_aval a,

			proj_consultor_aval_quesito b,

			proj_aval_opcao c

		where	a.nr_sequencia = b.nr_seq_avaliacao

		and	b.nr_seq_opcao = c.nr_sequencia

		and	a.nr_sequencia = nr_seq_aval_p

		and	trunc(a.dt_avaliacao, 'month') = trunc(dt_avaliacao_w, 'month')

		and	b.ie_tipo_avaliacao = 7

		and	trim(c.ds_opcao) = 'S'	) qt_resp_remun_var_coor

into	qt_resp_remun_var_cons_w,

	qt_resp_remun_var_coor_w

from	dual;



select	(	select	count(*)

		from	proj_consultor_aval a,

			proj_consultor_aval_quesito b

		where	a.nr_sequencia = b.nr_seq_avaliacao

		and	a.nr_sequencia = nr_seq_aval_p

		and	trunc(a.dt_avaliacao, 'month') = trunc(dt_avaliacao_w, 'month')

		and	b.ie_tipo_avaliacao = 6	) qt_quest_remun_var_cons,

	(	select	count(*)

		from	proj_consultor_aval a,

			proj_consultor_aval_quesito b

		where	a.nr_sequencia = b.nr_seq_avaliacao

		and	a.nr_sequencia = nr_seq_aval_p

		and	trunc(a.dt_avaliacao, 'month') = trunc(dt_avaliacao_w, 'month')

		and	b.ie_tipo_avaliacao = 7	) qt_quest_remun_var_coor

into	qt_quest_remun_var_cons_w,

	qt_quest_remun_var_coor_w

from	dual;



/*select	sum(nvl(a.qt_total_hora, 0))

into	qt_hora_trab_total_w

from	proj_rat a

where	a.cd_executor = (	select	b.CD_CONSULTOR

				from	proj_consultor_aval b

				where	b.nr_sequencia = nr_seq_aval_p	)

and	trunc(a.dt_liberacao, 'month') = trunc(dt_avaliacao_w, 'month');*/



select	Sum(c.QT_MIN_ATIV / 60)  --sum(nvl(a.qt_total_hora, 0))  Ajustado para pegar a soma das atividades

into	qt_hora_trab_total_w

from	proj_rat a,

		proj_rat_ativ c

where	a.cd_executor = (	select	b.CD_CONSULTOR

				from	proj_consultor_aval b

				where	b.nr_sequencia = nr_seq_aval_p	)

and	c.nr_seq_rat = a.nr_sequencia

and trunc(c.dt_fim_ativ, 'month') = trunc(dt_avaliacao_w, 'month')

and proj_obter_cod_classif_proj(a.nr_seq_proj) not in (73);



if	(qt_hora_trab_total_w <= 60) then

	vl_multi_bonus_w := 0;

elsif	(qt_hora_trab_total_w > 60) and

	(qt_hora_trab_total_w <= 100) then

	vl_multi_bonus_w := 0.25;

elsif	(qt_hora_trab_total_w > 100) and

	(qt_hora_trab_total_w < 143) then

	vl_multi_bonus_w := 0.5;

elsif	(qt_hora_trab_total_w >= 143) and

	(qt_hora_trab_total_w <= 170) then

	vl_multi_bonus_w := 1;

elsif	(qt_hora_trab_total_w > 170) and

	(qt_hora_trab_total_w <= 200) then

	vl_multi_bonus_w := 1.5;

elsif	(qt_hora_trab_total_w > 200) then

	vl_multi_bonus_w := 2;

end if;



delete from proj_consultor_aval_res

where nr_seq_avaliacao		= nr_seq_aval_p;



if	(nr_seq_modelo_w = 3) and

	(nr_seq_nivel_consultor_w is null) then

	wheb_mensagem_pck.exibir_mensagem_abort(190188);

	--application_error(-20011,'Para este modelo de avaliacao o nivel do consultor e obrigatorio');

end if;

if	(qt_registro_w > 0) then

	wheb_mensagem_pck.exibir_mensagem_abort(190189);

	--application_error(-20011,'Existem quesitos nao avaliados para esta avaliacao');

else

	OPEN C01;

	LOOP

	FETCH C01 into

			ie_tipo_avaliacao_w,

			qt_ponto_w;

	EXIT when C01%notfound;

		select	proj_consultor_aval_res_seq.nextval

		into	nr_sequencia_w

		from	dual;



		OPEN C02;

		LOOP

		FETCH C02 into vl_unit_w;

		EXIT when C02%notfound;

			vl_unit_w	:= vl_unit_w;

		END LOOP;

		CLOSE C02;



		if	(nr_seq_modelo_w = 1) then

			if	(ie_tipo_avaliacao_w = '0') then  -- valor base

				vl_hora_w	:= vl_unit_w;

			elsif	(ie_tipo_avaliacao_w = '3') and	-- Nivel conhecimento

				(qt_ponto_w > 0) then

				vl_hora_w	:= vl_unit_w;

			elsif	(ie_tipo_avaliacao_w = '5') then	-- Distancia da base

				vl_hora_w	:= dividir(qt_ponto_w,2)	* vl_unit_w;

			else

				vl_hora_w	:= dividir(qt_ponto_w,10)	* vl_unit_w;

			end if;

		else

			if	(ie_tipo_avaliacao_w = '0') then  -- valor base

				begin

				if	(ie_regime_contr_w = 'CLT') and

					(nr_seq_nivel_consultor_w in (1,5,6)) then

					vl_hora_w		:= vl_unit_w;

				else

					vl_hora_w		:= vl_unit_w;

				end if;

				end;

			elsif	(ie_tipo_avaliacao_w = '1') and	-- Nivel do cliente (complexidade)

				(ie_nivel_cliente_w = 'S') then

				qt_ponto_w		:= campo_numerico(ie_complexidade_w);

				vl_hora_w		:= dividir(qt_ponto_w, 5) * vl_unit_w;

			elsif	(ie_tipo_avaliacao_w = '2') and	-- Satisfacao do cliente

				(qt_ponto_w > 0) and

				(ie_satisfacao_w = 'S')then

				vl_hora_w		:= dividir(qt_ponto_w, 50)	* vl_unit_w;

			elsif	(ie_tipo_avaliacao_w = '3') then	-- Nivel conhecimento consultor

				qt_ponto_w		:= qt_pto_nivel_w;

				vl_hora_w		:= dividir(qt_ponto_w, 8) * vl_unit_w;

			elsif	(ie_tipo_avaliacao_w = '4') then	-- Cumprimento no prazo

				vl_hora_w		:=  qt_ponto_w * vl_unit_w;

			elsif	(ie_tipo_avaliacao_w = 6 or	--Remuneracao variavel - Consultor

				 ie_tipo_avaliacao_w = 7 ) then	--Remuneracao variavel - Coordenador

				begin



					vl_hora_def_w  := vl_unit_w;

					qt_hora_trab_total_w := nvl(qt_hora_trab_total_w, 1);



					select	max(vl_hora_faixa4),

						max(vl_hora_faixa3),

						max(vl_hora_faixa2)

					into	vl_hora_faixa4_w,

						vl_hora_faixa3_w,

						vl_hora_faixa2_w

					from	proj_consultor_valor

					where	ie_tipo_avaliacao	= ie_tipo_avaliacao_w

					and	nr_seq_modelo		= nr_seq_modelo_w

					and	dt_vigencia		<= dt_avaliacao_w

					and	ie_regime_contr		= ie_regime_contr_w

					and	ie_funcao_rat		= ie_funcao_exec_w;



					if 	(ie_tipo_avaliacao_w = 6) then

						if 	(qt_resp_remun_var_cons_w = qt_quest_remun_var_cons_w) then

							begin

								if 	(vl_hora_faixa4_w is null) or

									(vl_hora_faixa3_w is null) or

									(vl_hora_faixa2_w is null) or

									(vl_hora_def_w is null) then

									wheb_mensagem_pck.exibir_mensagem_abort(341882);

									--Existem valores por hora nao cadastrados para esta avaliacao. Verificar.

								end if;



								qt_hora_trab_w := nvl(qt_hora_trab_total_w, 1);



								if 	(qt_hora_trab_w > 220) then

									vl_hora_faixa4_w := ((((qt_hora_trab_w - 220) / qt_hora_trab_total_w) * 100) * vl_hora_faixa4_w) / 100;

									qt_hora_trab_w := qt_hora_trab_w - (qt_hora_trab_w - 220);

									vl_hora_w := nvl(vl_hora_w, 0) + nvl(vl_hora_faixa4_w, 0);

								end if;

								if 	(qt_hora_trab_w > 190) and

									(qt_hora_trab_w <= 220) then

									vl_hora_faixa3_w := ((((qt_hora_trab_w - 190) / qt_hora_trab_total_w) * 100) * vl_hora_faixa3_w) / 100;

									qt_hora_trab_w := qt_hora_trab_w - (qt_hora_trab_w - 190);

									vl_hora_w := nvl(vl_hora_w, 0) + nvl(vl_hora_faixa3_w, 0);

								end if;

								if 	(qt_hora_trab_w > 160) and

									(qt_hora_trab_w <= 190) then

									vl_hora_faixa2_w := ((((qt_hora_trab_w - 160) / qt_hora_trab_total_w) * 100) * vl_hora_faixa2_w) / 100;

									qt_hora_trab_w := qt_hora_trab_w - (qt_hora_trab_w - 160);

									vl_hora_w := nvl(vl_hora_w, 0) + nvl(vl_hora_faixa2_w, 0);

								end if;

								if	(qt_hora_trab_w <= 160) then

									vl_hora_def_w := (((qt_hora_trab_w / qt_hora_trab_total_w) * 100) * vl_hora_def_w) / 100;

									vl_hora_w := nvl(vl_hora_w, 0) + nvl(vl_hora_def_w, 0);

								end if;

							end;

						else

							vl_hora_w := vl_unit_w;

						end if;

					else

						if 	(qt_resp_remun_var_coor_w = qt_quest_remun_var_coor_w) then

							begin

								if 	(vl_hora_faixa4_w is null) or

									(vl_hora_faixa3_w is null) or

									(vl_hora_faixa2_w is null) or

									(vl_hora_def_w is null) then

									wheb_mensagem_pck.exibir_mensagem_abort(341882);

									--Existem valores por hora nao cadastrados para esta avaliacao. Verificar.

								end if;



								qt_hora_trab_w := nvl(qt_hora_trab_total_w, 1);



								if 	(qt_hora_trab_w > 160) then

									vl_hora_faixa4_w := ((((qt_hora_trab_w - 160) / qt_hora_trab_total_w) * 100) * vl_hora_faixa4_w) / 100;

									qt_hora_trab_w := qt_hora_trab_w - (qt_hora_trab_w - 160);

									vl_hora_w := nvl(vl_hora_w, 0) + nvl(vl_hora_faixa4_w, 0);

								end if;

								if 	(qt_hora_trab_w > 140) and

									(qt_hora_trab_w <= 160) then

									vl_hora_faixa3_w := ((((qt_hora_trab_w - 140) / qt_hora_trab_total_w) * 100) * vl_hora_faixa3_w) / 100;

									qt_hora_trab_w := qt_hora_trab_w - (qt_hora_trab_w - 140);

									vl_hora_w := nvl(vl_hora_w, 0) + nvl(vl_hora_faixa3_w, 0);

								end if;

								if 	(qt_hora_trab_w > 110) and

									(qt_hora_trab_w <= 140) then

									vl_hora_faixa2_w := ((((qt_hora_trab_w - 110) / qt_hora_trab_total_w) * 100) * vl_hora_faixa2_w) / 100;

									qt_hora_trab_w := qt_hora_trab_w - (qt_hora_trab_w - 110);

									vl_hora_w := nvl(vl_hora_w, 0) + nvl(vl_hora_faixa2_w, 0);

								end if;

								if	(qt_hora_trab_w <= 110) then

									vl_hora_def_w := (((qt_hora_trab_w / qt_hora_trab_total_w) * 100) * vl_hora_def_w) / 100;

									vl_hora_w := nvl(vl_hora_w, 0) + nvl(vl_hora_def_w, 0);

								end if;

							end;

						else

								vl_hora_w := vl_unit_w;

						end if;

					end if;

				end;

			else

				vl_hora_w		:= 0;

			end if;

		end if;

		vl_hora_w		:= nvl(vl_hora_w,0);

		qt_ponto_w		:= nvl(qt_ponto_w,0);



		if	(ie_tipo_avaliacao_w = '4') and (nr_seq_modelo_w not in (106,111)) then

			if	(qt_ponto_w <> 0) then

				vl_hora_w := vl_hora_w * vl_multi_bonus_w;

			end if;

		else

			if (nr_seq_modelo_w not in (106,111)) then            -- ajustado para nao pegar o multi_bonus_w na avaliacao de ferias

				vl_hora_w := vl_hora_w * vl_multi_bonus_w;

			else

				vl_hora_w := vl_hora_w;

			end if;

		end if;



		insert into	proj_consultor_aval_res(

			nr_sequencia, nr_seq_avaliacao, dt_atualizacao,

			nm_usuario, ie_tipo_avaliacao,  qt_ponto, vl_hora)

		Values(

			nr_sequencia_w, nr_seq_aval_p, sysdate,

			nm_usuario_p, ie_tipo_avaliacao_w,

			qt_ponto_w, vl_hora_w);

		select	sum(vl_hora),

			sum(qt_ponto)

		into	vl_hora_w,

			qt_ponto_w

		from	proj_consultor_aval_res

		where	nr_seq_avaliacao	= nr_seq_aval_p;



		update	proj_consultor_aval

		set	qt_ponto	= qt_ponto_w,

			vl_hora	= vl_hora_w

		where	nr_sequencia	= nr_seq_aval_p;



	END LOOP;

	CLOSE C01;

end if;



END Proj_Calcular_Ponto;
/
