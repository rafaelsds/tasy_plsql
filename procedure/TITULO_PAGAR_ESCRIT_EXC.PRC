create or replace
procedure inserir_tit_pagar_esc_exc(	cd_erro_p		number,
					nr_seq_escrit_p		number,
					nr_titulo_p		number,
					nm_usuario_p		varchar2) is 

begin
		insert	into titulo_pagar_escrit_exc
			(cd_erro,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario, 
			nm_usuario_nrec,
			nr_seq_escrit,
			nr_sequencia, 
			nr_titulo)
		values	(cd_erro_p, 
			sysdate,
			sysdate, 
			nm_usuario_p,
			nm_usuario_p,
			nr_seq_escrit_p,
			titulo_pagar_escrit_exc_seq.nextval,
			nr_titulo_p);
commit;

end inserir_tit_pagar_esc_exc;
/