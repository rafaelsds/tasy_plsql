create or replace
procedure pls_confirmar_cad_benef_web (	ds_hash_p		varchar2,
					nm_segurado_p	out	varchar2) is  

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Confirmar o cadastro de beneficiarios no portal web
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [X] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */	

ds_link_w 		varchar(255);
ds_mensagem_w 		varchar(1000);
ds_assinatura_w 	varchar(500);
ds_email_benef_w 	varchar(255);
ds_email_origem_w 	varchar(255);
ds_email_compl_w 	varchar(255);
ds_endereco_w 		varchar(500);
ds_observacao_w		varchar(255);
ds_local_w 		varchar(1000);
ds_senha_w 		varchar(100);
ds_senha_hash_w		varchar(64);
ie_barra_link_w 	varchar(1);
ie_assina_email_w 	varchar(1);
ie_tipo_compl_w 	varchar(5);
nm_fantasia_w	 	varchar(255);
nm_segurado_w	 	varchar(60);
nr_telefone_w	 	varchar(15);
nr_carteira_w	 	varchar(30);
vl_parametro_w 		varchar(255);
ds_salt_w			varchar2(15);
ds_tec_w			varchar2(15);

cd_estabelecimento_w	number(4);
nr_seq_segurado_w	number(10);
nr_seq_param_w		number(10);
qt_caracter_w 		number(10);

	
begin

if	(ds_hash_p is not null) then

	-- Verifica se o beneficiario esta pendente, caso nao estiver nao sera feito nada nesta rotina
	begin
		select	pls_obter_dados_segurado(nr_seq_segurado, 'N') nm_segurado,
			pls_obter_dados_segurado(nr_seq_segurado, 'C') nr_carteira,
			ds_senha,
			pls_obter_dados_segurado(nr_seq_segurado, 'EST') cd_estabelecimento,
			pls_obter_dados_segurado(nr_seq_segurado, 'E') ds_email,
			nr_seq_segurado,
			ds_tec
		into	nm_segurado_w,
			nr_carteira_w,
			ds_senha_w,
			cd_estabelecimento_w,
			ds_email_benef_w,
			nr_seq_segurado_w,
			ds_tec_w
		from   	pls_segurado_web
		where  	ds_hash = ds_hash_p
		and    	ie_situacao = 'P';
	exception
	when others then
		nm_segurado_w 			:= null;
		nr_carteira_w 			:= null;
		ds_senha_w 				:= null;
		cd_estabelecimento_w 	:= null;
		ds_email_benef_w		:= null;
		nr_seq_segurado_w		:= null;
		ds_tec_w				:= null;
	end;
	
	if 	(nvl(nr_seq_segurado_w,0) > 0) then
		--  Parametros - funcao '1246': OPSW - Portal web		
		ie_assina_email_w := pls_obter_param_padrao_funcao(3,'1246'); -- Assinar e-mails
		ie_tipo_compl_w := pls_obter_param_padrao_funcao(17,'1246'); -- Complemento de pessoa juridica utilizado no portal Web
		
		ds_link_w 		:= pls_parametro_operadora_web('LPO', cd_estabelecimento_w);
		ds_email_origem_w 	:= pls_parametro_operadora_web('EREM', cd_estabelecimento_w);
		if	(ds_email_origem_w is null or ds_email_origem_w = '') then
			--  Parametro - funcao: MENU DO SISTEMA
			ds_email_origem_w := pls_obter_param_padrao_funcao(38,'0'); -- Nome do USERID do servidor de E-mail
		end if;
		
		if	(ie_assina_email_w = 'S') then
		
			ds_assinatura_w := substr(pls_obter_assinatura_email_web(3, 'N', cd_estabelecimento_w),1,500);
		
			if	(ds_assinatura_w is null or  ds_assinatura_w = '') then
		
				-- Caso estiver informado um complemento no parametro 17, sera feito a  busca no complemento pessoa juridica, caso contrario sera buscado em Pessoa Juridica
				if	(ie_tipo_compl_w is not null and ie_tipo_compl_w <> 'X') then
					begin
						select  distinct b.nm_fantasia,
							c.ds_endereco ||', '||c.nr_endereco ||', '|| c.ds_complemento ||' - '|| c.ds_bairro ||' - '|| c.cd_cep ds_endereco,
							c.ds_municipio ||' - '|| c.sg_estado ds_local,
							c.nr_telefone,
							c.ds_email
						into	nm_fantasia_w,
							ds_endereco_w,
							ds_local_w,
							nr_telefone_w,
							ds_email_compl_w
						from    pessoa_juridica a, 
							pls_outorgante b, 
							pessoa_juridica_compl c 
						where   a.cd_cgc = b.cd_cgc_outorgante 
						and     a.cd_cgc = c.cd_cgc 
						and     b.cd_cgc_outorgante = c.cd_cgc 
						and     b.cd_estabelecimento = cd_estabelecimento_w 
						and     c.ie_tipo_complemento = ie_tipo_compl_w 
						and     c.ds_email is not null
						and	rownum = 1;
					exception
					when others then
						nm_fantasia_w 	:= null;
						ds_endereco_w 	:= null;
						ds_local_w 	:= null;
						nr_telefone_w 	:= null;
						ds_email_compl_w:= null;
					end;
					
					-- Caso nao estiver informado nenhum registro no complemento, sera feito a busca pelo Pessoa Juridica
					if	(nm_fantasia_w is null) then
						begin
							select  b.nm_fantasia,
								a.ds_endereco       ||', '   ||
								a.nr_endereco       ||', '   ||
								a.ds_complemento    ||' - '  ||
								a.ds_bairro         ||' - '  ||
								a.cd_cep ds_endereco,
								a.ds_municipio      || ' - '   ||
								a.sg_estado ds_local,
								a.nr_telefone,
								nvl(pls_obter_dados_pj_compl(b.cd_estabelecimento, a.cd_cgc, 'M'), a.ds_email) ds_email 
							into	nm_fantasia_w,
								ds_endereco_w,
								ds_local_w,
								nr_telefone_w,
								ds_email_compl_w
							from   	pessoa_juridica a,
								pls_outorgante b
							where  	a.cd_cgc = b.cd_cgc_outorgante
							and    	b.cd_estabelecimento = cd_estabelecimento_w
							and	rownum = 1;
						exception
						when others then
							nm_fantasia_w 	:= null;
							ds_endereco_w 	:= null;
							ds_local_w 	:= null;
							nr_telefone_w 	:= null;
							ds_email_compl_w:= null;
						end;
					end if;
				else	
					begin
						select  b.nm_fantasia,
							a.ds_endereco       ||', '||
							a.nr_endereco       ||', '||
							a.ds_complemento    ||' - '||
							a.ds_bairro         ||' - '||
							a.cd_cep ds_endereco,
							a.ds_municipio      ||' - '||
							a.sg_estado ds_local,
							a.nr_telefone,
							nvl(pls_obter_dados_pj_compl(b.cd_estabelecimento, a.cd_cgc, 'M'), a.ds_email) ds_email 
						into	nm_fantasia_w,
							ds_endereco_w,
							ds_local_w,
							nr_telefone_w,
							ds_email_compl_w
						from   	pessoa_juridica a,
							pls_outorgante b
						where  	a.cd_cgc = b.cd_cgc_outorgante
						and    	b.cd_estabelecimento = cd_estabelecimento_w
						and	rownum = 1;
					exception
					when others then
						nm_fantasia_w 	:= null;
						ds_endereco_w 	:= null;
						ds_local_w 	:= null;
						nr_telefone_w 	:= null;
						ds_email_compl_w:= null;
					end;
				end if;
				
				-- Monta a assinatura de email
				ds_assinatura_w := nm_fantasia_w 				|| chr(13) || chr(10);
				ds_assinatura_w := ds_assinatura_w ||ds_endereco_w 		|| chr(13) || chr(10);
				ds_assinatura_w := ds_assinatura_w ||ds_local_w 		|| chr(13) || chr(10);
				ds_assinatura_w := ds_assinatura_w ||'Fone: '|| nr_telefone_w 	|| chr(13) || chr(10);
				ds_assinatura_w := ds_assinatura_w || ds_email_compl_w;
			end if;
			
		end if;
		
		ds_mensagem_w := wheb_mensagem_pck.get_texto(1172306, 'NM_SEGURADO=' || nm_segurado_w || ';' || 'NR_CARTEIRA=' || nr_carteira_w || ';' || 'DS_LINK=' || ds_link_w);
				
		-- Caso possuir assinatura de email, sera concatenado a mensagem com a assinatura
		if 	(ie_assina_email_w = 'S' and ds_assinatura_w is not null) then
			ds_mensagem_w := ds_mensagem_w || chr(13) || chr(10)|| chr(13) || chr(10) || ds_assinatura_w;
		end if;		
		
		-- Envia o email de confirmacao do cadastro
		enviar_email(wheb_mensagem_pck.get_texto(1172307), ds_mensagem_w, ds_email_origem_w, ds_email_benef_w, '', 'A');
		
		-- Grava o log 
		ds_observacao_w := wheb_mensagem_pck.get_texto(1172308, 'NM_SEGURADO=' || nm_segurado_w);
		
		pls_gravar_log_acesso_portal(nr_seq_segurado_w, null, null, null, null, null, null, null, '3', ds_observacao_w, 'Procedure - enviar_email ', 'N');
		
		if (ds_tec_w is null) then
			-- Altera o beneficiario para ativo e gera o hash
			
			select 	replace(replace(dbms_random.string('P', 15), chr(39), ''), ';', '') 
			into	ds_salt_w
			from 	dual;
		
			ds_senha_hash_w := obter_sha2(upper(ds_senha_w) || ds_salt_w, 256);
		
			update  pls_segurado_web
			set    	ds_senha = ds_senha_hash_w, 
					ds_tec = ds_salt_w
			where  	ds_hash = ds_hash_p;
		end if;
		
		update  pls_segurado_web
		set    	ie_situacao = 'A',
				dt_atualizacao = sysdate, 
				ds_observacao = ''
		where  	ds_hash = ds_hash_p;
		
		commit;
	end if;
end if;

nm_segurado_p := nm_segurado_w;

end pls_confirmar_cad_benef_web;
/
