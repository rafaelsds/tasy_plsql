create or replace
procedure registar_apagar_buscar_pac(nr_cirurgia_p		number,
					ie_opcao_p		varchar2,
					nm_usuario_p		Varchar2) is 
/*ie_opcao_p:
	'R' = Registrar
	'A = Apagar'*/
begin
if 	(nr_cirurgia_p is not null) and
	(ie_opcao_p is not null) and
	(nm_usuario_p is not null) then
	if (ie_opcao_p = 'R') then
		update	agenda_paciente
		set	nm_usuario		=	nm_usuario_p,
			nm_buscar_paciente	=	nm_usuario_p,
			dt_atualizacao		=	sysdate,
			dt_buscar_paciente	=	sysdate
		where	nr_cirurgia		=	nr_cirurgia_p;
	
	elsif (ie_opcao_p = 'A') then
		update	agenda_paciente
		set	nm_usuario		=	nm_usuario_p,
			nm_buscar_paciente	=	null,
			dt_atualizacao		=	sysdate,
			dt_buscar_paciente	=	null
		where	nr_cirurgia		=	nr_cirurgia_p;
	end if;
end if;

commit;

end registar_apagar_buscar_pac;
/