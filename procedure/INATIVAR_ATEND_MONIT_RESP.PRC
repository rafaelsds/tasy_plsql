create or replace
procedure inativar_atend_monit_resp (
		nr_sequencia_p		number,
		ds_justificativa_p	varchar2,
		nm_usuario_p		varchar2) is
		
begin 

if	(nr_sequencia_p	> 0) and
	(ds_justificativa_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	atendimento_monit_resp
	set	ie_situacao = 'I',
		dt_inativacao = sysdate,
		nm_usuario_inativacao = nm_usuario_p,
		ds_justificativa = ds_justificativa_p
	where 	nr_sequencia = nr_sequencia_p;
	end;
end if;

commit;

end inativar_atend_monit_resp;
/