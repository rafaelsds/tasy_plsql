create or replace procedure pls_atualizar_datas_lote_notif

			(	nr_seq_lote_p		number,

				nm_usuario_p		varchar2) is



ie_acao_nao_util_w	varchar2(3);

ie_acao_notificacao_w	varchar2(3);

nr_seq_notif_regra_w	number(10);

qt_dias_w		number(5);

cd_estabelecimento_w	number(4);

dt_lote_w		date;

dt_acao_w		date;



Cursor C01 is

	select	a.ie_acao_notificacao,

		a.qt_dias,

		a.ie_acao_nao_util

	from	pls_notificacao_regra_data	a

	where	dt_lote_w between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_lote_w)

	and	nvl(a.nr_seq_notificacao_regra,nr_seq_notif_regra_w)	= nr_seq_notif_regra_w;



begin

if	(nr_seq_lote_p is not null) then

	select	a.dt_lote,

		a.nr_seq_regra,

		a.cd_estabelecimento

	into	dt_lote_w,

		nr_seq_notif_regra_w,

		cd_estabelecimento_w

	from	pls_notificacao_lote a

	where	a.nr_sequencia	= nr_seq_lote_p;



	open C01;

	loop

	fetch C01 into	

		ie_acao_notificacao_w,

		qt_dias_w,

		ie_acao_nao_util_w;

	exit when C01%notfound;

		begin

		dt_acao_w	:= trunc(dt_lote_w,'dd') + qt_dias_w;

		

		if	(ie_acao_nao_util_w = 'A') then /* Antecipar data */

			dt_acao_w	:= obter_dia_util_anterior(cd_estabelecimento_w,dt_acao_w);

		elsif	(ie_acao_nao_util_w = 'P') then /* Postergar data */

			dt_acao_w	:= obter_proximo_dia_util(cd_estabelecimento_w,dt_acao_w);

		end if;

		

		if	(ie_acao_notificacao_w = 'NT') then /* Notificação via telefone */

			update	pls_notificacao_lote

			set	dt_limite_notif_tel	= dt_acao_w

			where	nr_sequencia		= nr_seq_lote_p;

		elsif	(ie_acao_notificacao_w = 'NC') then /* Notificação via carta */

			update	pls_notificacao_lote

			set	dt_limite_notif_carta	= dt_acao_w

			where	nr_sequencia		= nr_seq_lote_p;

		elsif	(ie_acao_notificacao_w = 'S') then /* Suspensão */

			update	pls_notificacao_lote

			set	dt_limite_suspensao	= dt_acao_w

			where	nr_sequencia		= nr_seq_lote_p;

		elsif	(ie_acao_notificacao_w = 'R') then /* Rescisão */

			update	pls_notificacao_lote

			set	dt_limite_rescisao	= dt_acao_w

			where	nr_sequencia		= nr_seq_lote_p;

		elsif	(ie_acao_notificacao_w = 'RN') then /* Recebimento de notificação */

			update	pls_notificacao_lote

			set	dt_limite_recebimento	= dt_acao_w

			where	nr_sequencia		= nr_seq_lote_p;

		end if;

		end;

	end loop;

	close C01;

end if;



commit;



end pls_atualizar_datas_lote_notif;
/


 