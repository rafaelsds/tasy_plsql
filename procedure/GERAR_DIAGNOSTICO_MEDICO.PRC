create or replace
procedure gerar_diagnostico_medico	(nr_atendimento_p	number,
					dt_diagnostico_p	date,
					cd_medico_diag_p	varchar2,
					nm_usuario_p		varchar2) is

ie_gerar_diag_w	varchar2(1);

begin
if	(nr_atendimento_p is not null) and
	(dt_diagnostico_p is not null) and
	(cd_medico_diag_p is not null) and
	(nm_usuario_p is not null) then
	/* consistir diag x atend x data */
	select	decode(count(*),0,'S','N')
	into	ie_gerar_diag_w
	from	diagnostico_medico
	where	nr_atendimento	= nr_atendimento_p
	and	dt_diagnostico	= dt_diagnostico_p;

	if	(ie_gerar_diag_w = 'S') then
		insert into diagnostico_medico	(
							nr_atendimento,
							dt_diagnostico,
							ie_tipo_diagnostico,
							cd_medico,
							dt_atualizacao,
							nm_usuario,
							ds_diagnostico,
							ie_tipo_doenca,
							ie_unidade_tempo,
							qt_tempo
							)
		values					(
							nr_atendimento_p,
							dt_diagnostico_p,
							2,
							cd_medico_diag_p,
							sysdate,
							nm_usuario_p,
							null,
							null,
							null,
							null
							);
	end if;
end if;

commit;

end gerar_diagnostico_medico;
/