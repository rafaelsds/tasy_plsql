create or replace
procedure atualizar_repasse_calc(nr_interno_conta_p	number) is

cursor	c_procedimento(nr_interno_conta_pc	conta_paciente.nr_interno_conta%type) is
	select	a.nr_seq_procedimento,
		nvl(a.nr_seq_partic,0) nr_seq_partic,
		nvl(a.vl_repasse,0) vl_repasse
	from	procedimento_paciente b,
		procedimento_repasse a
	where	a.nr_seq_procedimento = b.nr_sequencia
	and	b.nr_interno_conta = nr_interno_conta_pc;

cursor	c_material(nr_interno_conta_pc	conta_paciente.nr_interno_conta%type) is
	select	a.nr_seq_material,
		nvl(a.vl_repasse,0) vl_repasse
	from	material_atend_paciente b,
		material_repasse a
	where	a.nr_seq_material = b.nr_sequencia
	and	b.nr_interno_conta = nr_interno_conta_pc;

begin

for	r_c_procedimento in c_procedimento(nr_interno_conta_p) loop
	if	(r_c_procedimento.nr_seq_partic > 0) then
		update	procedimento_participante
		set	vl_repasse_calc			= r_c_procedimento.vl_repasse
		where	nr_seq_partic			= r_c_procedimento.nr_seq_partic
		and	nr_sequencia			= r_c_procedimento.nr_seq_procedimento;
	else
		update	procedimento_paciente
		set	vl_repasse_calc			= r_c_procedimento.vl_repasse
		where	nr_sequencia			= r_c_procedimento.nr_seq_procedimento;
	end if;
end loop;

for	r_c_material in c_material(nr_interno_conta_p) loop
	update	material_atend_paciente
	set	vl_repasse_calc				= r_c_material.vl_repasse
	where	nr_sequencia				= r_c_material.nr_seq_material;
end loop;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end atualizar_repasse_calc;
/