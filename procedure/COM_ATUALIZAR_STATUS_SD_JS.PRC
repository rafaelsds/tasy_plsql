create or replace
procedure com_atualizar_status_sd_js(
					ie_status_p		varchar2,
					nr_ordem_servico_p	number,
					nr_seq_motivo_p		number,
					ds_justificativa_p	varchar2,
					nm_usuario_p		varchar2,
					ie_commit_p		varchar2) is 

begin

if	(ie_status_p = 'C') then

	update	com_solic_sd
	set	ie_status 		= ie_status_p,
		dt_cancelamento 	= sysdate,
		nm_usuario_cancel 	= nm_usuario_p,
		nr_seq_motivo_cancel	= nr_seq_motivo_p,
		ds_justif_cancel 	= ds_justificativa_p
	where	nr_ordem_servico	= nr_ordem_servico_p;

	update	agenda_tasy
	set	ie_status	= ie_status_p,
		dt_atualizacao 	= sysdate,
		nm_usuario 	= nm_usuario_p
	where	nr_ordem_servico = nr_ordem_servico_p;
	

elsif	(ie_status_p = 'R') then

	update	com_solic_sd
	set	ie_status 		= ie_status_p,
		dt_atualizacao	 	= sysdate,
		nm_usuario	 	= nm_usuario_p
	where	nr_ordem_servico	= nr_ordem_servico_p;

	update	agenda_tasy
	set	ie_status	= 'E',
		dt_atualizacao 	= sysdate,
		nm_usuario 	= nm_usuario_p
	where	nr_ordem_servico = nr_ordem_servico_p;
	
elsif	(ie_status_p = 'AG') then

	update	com_solic_sd
	set	ie_status 		= ie_status_p,
		dt_atualizacao	 	= sysdate,
		nm_usuario	 	= nm_usuario_p
	where	nr_ordem_servico	= nr_ordem_servico_p;

end if;

if	(ie_commit_p = 'S') then
	commit;
end if;

end com_atualizar_status_sd_js;
/