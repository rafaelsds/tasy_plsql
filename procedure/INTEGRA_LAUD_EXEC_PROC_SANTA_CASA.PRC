create or replace PROCEDURE Integra_Laud_exec_proc(
    nr_prescricao_p       NUMBER,
    nr_seq_proc_p         NUMBER,
    nr_seq_interno_proc_p NUMBER,
    nr_acesso_dicom_p     VARCHAR2,
    nr_crm_executor_p     VARCHAR2,
    ds_ufCRM_Executor_p   VARCHAR2,
    cd_medico_Executor_p  VARCHAR2,
    nm_usuario_p          VARCHAR2,
    ds_erro_p OUT VARCHAR2)
IS
  cd_medico_exec_w       VARCHAR2(10);
  nr_prescricao_w        NUMBER(10);
  nr_seq_prescricao_w    NUMBER(10);
  nr_seq_proc_interno_w  NUMBER(10);
  cd_procedimento_w      NUMBER(15);
  ie_origem_proced_w     NUMBER(10);
  qt_procedimento_w      NUMBER(8,3);
  cd_setor_atendimento_w NUMBER(5);
  dt_prev_execucao_w DATE;
  ie_lado_w        VARCHAR2(1);
  nr_seq_proced_w  NUMBER(10);
  nr_seq_propaci_w NUMBER(10);
  nr_atendimento_w NUMBER(10);
  dt_entrada_unidade_w DATE;
  qt_existe_w NUMBER(10);
  nr_seq_exame_w prescr_procedimento.nr_seq_exame%type;
  cd_acao_w                  NUMBER(1);
  qt_kit_w                   NUMBER(5);
  ie_n_gera_kit_w            VARCHAR2(1);
  nr_seq_agenda_w            NUMBER(10,0);
  nr_seq_agenda_pac_w        NUMBER(10,0);
  nr_seq_agenda_con_w        NUMBER(10,0);
  ie_tipo_agenda_w           VARCHAR2(1);
  cd_estabelecimento_w       NUMBER(4,0);
  qt_exames_pendentes_exec_w NUMBER(10);
  CURSOR c1
  IS
    SELECT a.cd_kit_material,
      b.dt_prev_execucao,
      b.nr_prescricao,
      b.nr_sequencia,
      'N' ie_auditoria,
      0 cd_local_estoque,
      NULL nr_seq_proc_princ,
      nvl((select max(nr_seq_interno)
            from atend_paciente_unidade e
            where d.nr_atendimento = e.nr_atendimento
            and a.cd_setor_atendimento = e.cd_setor_atendimento
            ),0) as nr_seq_atepacu,
      NULL ie_baixa_sem_estoque,
      1 qt_kit,
      NVL(b.nr_doc_convenio,0) nr_doc_convenio,
      NULL nr_seq_regra_lanc
    FROM proc_interno_kit a
    INNER JOIN proc_interno c
    ON c.nr_sequencia = a.nr_seq_proc_interno
    INNER JOIN prescr_procedimento b
    ON (((b.cd_procedimento   = c.cd_procedimento)
    AND (b.ie_origem_proced   = c.ie_origem_proced))
    OR (b.nr_seq_proc_interno = c.nr_sequencia))
    INNER JOIN prescr_medica d
    ON ((d.nr_prescricao      = b.nr_prescricao)
    AND (d.cd_estabelecimento = a.cd_estabelecimento))
    WHERE b.nr_prescricao     = nr_prescricao_w
    AND b.nr_sequencia        = nr_seq_proced_w
	and     obter_idade_kit_proced(a.nr_sequencia,'MIN') <= obter_idade_pf(d.cd_pessoa_fisica,sysdate,'DIA')
	AND     obter_idade_kit_proced(a.nr_sequencia,'MAX') >= obter_idade_pf(d.cd_pessoa_fisica,sysdate,'DIA');
BEGIN
  ds_erro_p                   := '';
  IF ((NVL(nr_prescricao_p,0)  > 0) AND (NVL(nr_seq_proc_p,0) > 0)) OR (NVL(nr_seq_interno_proc_p,0) > 0) OR (NVL(nr_acesso_dicom_p,0) > 0) THEN
    IF (NVL(nr_prescricao_p,0) > 0) AND (NVL(nr_seq_proc_p,0) > 0) THEN
      SELECT MAX(nr_prescricao),
        MAX(nr_sequencia),
        MAX(nr_seq_agenda),
        MAX(nr_seq_agenda_cons)
      INTO nr_prescricao_w,
        nr_seq_proced_w,
        nr_seq_agenda_pac_w,
        nr_seq_agenda_con_w
      FROM prescr_procedimento
      WHERE nr_prescricao               = nr_prescricao_p
      AND nr_sequencia                  = nr_seq_proc_p;
    elsif (NVL(nr_seq_interno_proc_p,0) > 0) THEN
      SELECT MAX(nr_prescricao),
        MAX(nr_sequencia),
        MAX(nr_seq_agenda),
        MAX(nr_seq_agenda_cons)
      INTO nr_prescricao_w,
        nr_seq_proced_w,
        nr_seq_agenda_pac_w,
        nr_seq_agenda_con_w
      FROM prescr_procedimento
      WHERE nr_seq_interno = nr_seq_interno_proc_p;
    ELSE
      SELECT MAX(nr_prescricao),
        MAX(nr_sequencia),
        MAX(nr_seq_agenda),
        MAX(nr_seq_agenda_cons)
      INTO nr_prescricao_w,
        nr_seq_proced_w,
        nr_seq_agenda_pac_w,
        nr_seq_agenda_con_w
      FROM prescr_procedimento
      WHERE nr_acesso_dicom = nr_acesso_dicom_p;
    END IF;
    
    IF (NVL(nr_prescricao_p,0) > 0) AND (NVL(nr_seq_proc_p,0) > 0) THEN
      SELECT MAX(nr_prescricao),
        MAX(nr_sequencia)
      INTO nr_prescricao_w,
        nr_seq_proced_w
      FROM prescr_procedimento
      WHERE nr_prescricao               = nr_prescricao_p
      AND nr_sequencia                  = nr_seq_proc_p;
    elsif (NVL(nr_seq_interno_proc_p,0) > 0) THEN
      SELECT MAX(nr_prescricao),
        MAX(nr_sequencia)
      INTO nr_prescricao_w,
        nr_seq_proced_w
      FROM prescr_procedimento
      WHERE nr_seq_interno = nr_seq_interno_proc_p;
    ELSE
      SELECT MAX(nr_prescricao),
        MAX(nr_sequencia)
      INTO nr_prescricao_w,
        nr_seq_proced_w
      FROM prescr_procedimento
      WHERE nr_acesso_dicom = nr_acesso_dicom_p;
    END IF;
    
    SELECT DECODE(NVL(MAX(b.ie_altera_status_exec),'N'),'S',1,9),
      NVL(MAX(b.ie_n_gerar_kit_material),'N'),
      max(a.cd_estabelecimento) 
    INTO cd_acao_w,
      ie_n_gera_kit_w,
      cd_estabelecimento_w
    FROM prescr_medica a,
      parametro_integracao_pacs b
    WHERE a.cd_estabelecimento = b.cd_estabelecimento
    AND a.nr_prescricao        = nr_prescricao_w;
    
    IF (nr_prescricao_w      IS NOT NULL) AND (nr_seq_proced_w IS NOT NULL) THEN
      IF ((nr_crm_executor_p IS NOT NULL) AND (ds_ufCRM_Executor_p IS NOT NULL)) THEN
        SELECT MAX(cd_pessoa_fisica)
        INTO cd_medico_exec_w
        FROM medico
        WHERE nr_crm = nr_crm_executor_p
        AND uf_crm   = ds_ufCrm_executor_p;
      ELSE
        SELECT MAX(cd_pessoa_fisica)
        INTO cd_medico_exec_w
        FROM medico
        WHERE cd_pessoa_fisica = cd_medico_Executor_p;
      END IF;
      IF (cd_medico_exec_w  IS NOT NULL) OR -- Achou o medio ou nao passou nenhum medico
        ((nr_crm_executor_p IS NULL) AND (ds_ufCRM_Executor_p IS NULL) AND (cd_medico_Executor_p IS NULL)) THEN
        SELECT nr_seq_proc_interno,
          cd_procedimento,
          ie_origem_proced,
          qt_procedimento,
          cd_setor_atendimento,
          dt_prev_execucao,
          ie_lado
        INTO nr_seq_proc_interno_w,
          cd_procedimento_w,
          ie_origem_proced_w,
          qt_procedimento_w,
          cd_setor_atendimento_w,
          dt_prev_execucao_w,
          ie_lado_w
        FROM prescr_procedimento
        WHERE nr_prescricao = nr_prescricao_w
        AND nr_sequencia    = nr_seq_proced_w;
        SELECT NVL(MAX(nr_sequencia),0),
          MAX(nr_atendimento),
          MAX(dt_entrada_unidade)
        INTO nr_seq_propaci_w,
          nr_atendimento_w,
          dt_entrada_unidade_w
        FROM procedimento_paciente
        WHERE nr_prescricao         = nr_prescricao_w
        AND nr_sequencia_prescricao = nr_seq_proced_w;
        IF (nr_seq_propaci_w        = 0) THEN -- Aqui
          BEGIN
            Gerar_Proc_Pac_item_Prescr( nr_prescricao_w, nr_seq_proced_w, NULL, NULL, nr_seq_proc_interno_w, cd_procedimento_w, ie_origem_proced_w, qt_procedimento_w, cd_setor_atendimento_w, cd_acao_w, dt_prev_execucao_w, SUBSTR(wheb_mensagem_pck.get_texto(299648),1,255), cd_medico_exec_w, NULL, ie_lado_w, NULL);
            IF (ie_n_gera_kit_w = 'N') THEN
              BEGIN
                SELECT COUNT(1)
                INTO qt_kit_w
                FROM proc_interno_kit a
                INNER JOIN prescr_procedimento b
                ON b.nr_seq_proc_interno = a.nr_seq_proc_interno
                INNER JOIN prescr_medica c
                ON ((c.nr_prescricao        = b.nr_prescricao)
                AND (c.cd_estabelecimento   = a.cd_estabelecimento))
                WHERE a.nr_seq_proc_interno = nr_seq_proc_interno_w
                AND b.nr_prescricao         = nr_prescricao_w
                AND b.nr_sequencia          = nr_seq_proced_w;
                IF NVL(qt_kit_w,0)          > 0 THEN
                  FOR r1                   IN c1
                  LOOP
                    gerar_kit_material_exec(r1.cd_kit_material, r1.dt_prev_execucao, r1.nr_prescricao, r1.ie_auditoria, r1.cd_local_estoque, r1.nr_seq_proc_princ, r1.nr_seq_atepacu, r1.ie_baixa_sem_estoque, nm_usuario_p, r1.qt_kit, ds_erro_p, r1.nr_doc_convenio, r1.nr_seq_regra_lanc);
                  END LOOP;
                END IF;
              END;
            END IF;
          END;
        ELSE
          BEGIN
            SELECT COUNT(*)
            INTO qt_existe_w
            FROM procedimento_paciente a,
              prescr_procedimento b
            WHERE a.nr_prescricao         = nr_prescricao_w
            AND a.nr_sequencia_prescricao = nr_seq_proced_w
            AND a.nr_prescricao           = b.nr_prescricao
            AND a.nr_sequencia_prescricao = b.nr_sequencia
            AND b.dt_cancelamento        IS NULL
            AND b.ie_status_execucao      < '20';
            IF (qt_existe_w               > 0) THEN
              BEGIN
                SELECT NVL(MAX(nr_seq_exame),0)
                INTO nr_seq_exame_w
                FROM prescr_procedimento
                WHERE nr_prescricao = nr_prescricao_w
                AND nr_sequencia    = nr_seq_proced_w;
                atualiza_status_proced_exec(nr_seq_proced_w, nr_prescricao_w, nr_seq_exame_w, nm_usuario_p);
              END;
            END IF;
            ds_erro_p := ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(282394,NULL);
          END;
        END IF;
      ELSE
        ds_erro_p := ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(282395,NULL);
      end if;
      
      IF nr_seq_agenda_con_w IS NOT NULL THEN
         --Consulta
         ie_tipo_agenda_w        := 'C';
         nr_seq_agenda_w         := nr_seq_agenda_con_w;
      elsif nr_seq_agenda_pac_w IS NOT NULL THEN
         --Exame
         ie_tipo_agenda_w := 'E';
         nr_seq_agenda_w  := nr_seq_agenda_pac_w;
      END IF;
      
      SELECT COUNT(*)
        INTO qt_exames_pendentes_exec_w
        FROM prescr_procedimento a,
             prescr_medica b
       WHERE a.nr_prescricao          = b.nr_prescricao
         AND ((a.nr_seq_agenda          = nr_seq_agenda_w
          OR a.nr_seq_agenda_cons        = nr_seq_agenda_w)
          OR (b.nr_seq_agenda            = nr_seq_agenda_w
          OR b.nr_seq_agecons            = nr_seq_agenda_w))
         AND a.ie_status_execucao       < '20'
         AND a.dt_cancelamento         IS NULL;
        ds_erro_p := ds_erro_p || qt_exames_pendentes_exec_w;
        if (qt_exames_pendentes_exec_w = 0 and nr_seq_agenda_w is not null) then
            executar_evento_agenda('EPG',ie_tipo_agenda_w,nr_seq_agenda_w,cd_estabelecimento_w,nm_usuario_p,NULL,NULL);
         end if;
    ELSE
      ds_erro_p := ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(282396,NULL);
    END IF;
  ELSE
    ds_erro_p := ds_erro_p || WHEB_MENSAGEM_PCK.get_texto(282397,NULL);
  END IF;
  COMMIT;
END integra_laud_exec_proc;
/
