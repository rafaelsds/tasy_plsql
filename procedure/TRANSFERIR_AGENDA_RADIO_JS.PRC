create or replace
procedure transferir_agenda_radio_js(nr_seq_origem_p               number,
			nr_seq_destino_p               number,
			ie_acao_p                      varchar2,
			cd_estabelecimento_p           number,
			nm_usuario_p                   varchar2
			) is 

ds_erro_w 	varchar2(2000) := '';			
			
begin

Rxt_Consistir_transf_agenda(nr_seq_origem_p, nr_seq_destino_p, ie_acao_p, cd_estabelecimento_p, nm_usuario_p, 0, ds_erro_w);

if	(ds_erro_w is not null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(261436, 'ERRO='|| ds_erro_w);
end if;

rxt_copiar_transferir_agenda(cd_estabelecimento_p, nr_seq_origem_p, nr_seq_destino_p, ie_acao_p, nm_usuario_p);

end transferir_agenda_radio_js;
/
