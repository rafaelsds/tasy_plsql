create or replace
procedure cme_inserir_conj_classif(	nr_seq_conj_p	number,
			ds_classif_p	varchar2,
			nm_usuario_p	varchar2) is

nr_seq_classif_w		number(10,0);
nr_seq_conjunto_w		number(10,0);

/* variaveis conjunto */
nm_conjunto_w		varchar2(200);
ie_situacao_w		varchar2(1);
nr_seq_embalagem_w		number(10,0);
ie_controle_fisico_w		varchar2(1);
ds_conjunto_w		varchar2(2000);
cd_estabelecimento_w		number(4,0);
qt_ponto_w			number(15,2);
cd_setor_atendimento_w		number(5,0);
qt_limite_esterilizacao_w	number(10,0);
qt_tempo_esterelizacao_w	number(10,0);
dt_revisao_w		date;
qt_min_intervalo_prep_w		number(8,0);
ds_observacao_w		varchar2(255);
ds_reduzida_w		varchar2(20);
cd_especialidade_w		number(5,0);
qt_consiste_agenda_w		number(5,0);
cd_medico_w			varchar2(10);
nr_agrupador_w		number(10,0);
ie_agendamento_w		varchar2(1);
ie_gerar_conj_auto_w		varchar2(1);

Cursor C01 is
	select	nr_sequencia
	from	cm_classif_conjunto
	where	obter_Se_Contido(nr_sequencia, '(' || ds_classif_p || ')') = 'S';
					
begin

update	cm_conjunto
set	nr_agrupador	= nr_sequencia
where	nr_sequencia	= nr_seq_conj_p
and	nr_agrupador is null;

commit;

/* Obter os dados do conjunto */
select	nm_conjunto,
	ie_situacao,
	nr_seq_embalagem,
	ie_controle_fisico,
	ds_conjunto,
	cd_estabelecimento,
	qt_ponto,
	cd_setor_atendimento,
	qt_limite_esterilizacao,
	qt_tempo_esterelizacao,
	dt_revisao,
	qt_min_intervalo_prep,
	ds_observacao,
	ds_reduzida,
	cd_especialidade,
	qt_consiste_agenda,
	cd_medico,
	nr_agrupador,
	ie_agendamento,
	ie_gerar_conj_auto
into	nm_conjunto_w,
	ie_situacao_w,
	nr_seq_embalagem_w,
	ie_controle_fisico_w,
	ds_conjunto_w,
	cd_estabelecimento_w,
	qt_ponto_w,
	cd_setor_atendimento_w,
	qt_limite_esterilizacao_w,
	qt_tempo_esterelizacao_w,
	dt_revisao_w,
	qt_min_intervalo_prep_w,
	ds_observacao_w,
	ds_reduzida_w,
	cd_especialidade_w,
	qt_consiste_agenda_w,
	cd_medico_w,
	nr_agrupador_w,
	ie_agendamento_w,
	ie_gerar_conj_auto_w
from	cm_conjunto
where	nr_sequencia = nr_seq_conj_p;

/* Inserir o conjunto nas classificacoes selecionados */
open C01;
loop
fetch C01 into
	nr_seq_classif_w;
exit when C01%notfound;
	begin	
	select	cm_conjunto_seq.nextval
	into	nr_seq_conjunto_w
	from	dual;	
	
	/* Conjunto */
	insert into cm_conjunto(
		nr_sequencia,
		nm_conjunto,
		nr_seq_classif,
		dt_atualizacao,
		nm_usuario,
		ie_situacao,
		nr_seq_embalagem,
		ie_controle_fisico,
		ds_conjunto,
		cd_estabelecimento,
		qt_ponto,
		cd_setor_atendimento,
		qt_limite_esterilizacao,
		qt_tempo_esterelizacao,
		dt_revisao,
		qt_min_intervalo_prep,
		ds_observacao,
		ds_reduzida,
		cd_especialidade,
		qt_consiste_agenda,
		cd_medico,
		nr_agrupador,
		ie_agendamento,
		ie_gerar_conj_auto)
	values	(nr_seq_conjunto_w,
		nm_conjunto_w,
		nr_seq_classif_w,
		sysdate,
		nm_usuario_p,
		ie_situacao_w,
		nr_seq_embalagem_w,
		ie_controle_fisico_w,
		ds_conjunto_w,
		cd_estabelecimento_w,
		qt_ponto_w,
		cd_setor_atendimento_w,
		qt_limite_esterilizacao_w,
		qt_tempo_esterelizacao_w,
		dt_revisao_w,
		qt_min_intervalo_prep_w,
		ds_observacao_w,
		ds_reduzida_w,
		cd_especialidade_w,
		qt_consiste_agenda_w,
		cd_medico_w,
		nr_agrupador_w,
		ie_agendamento_w,
		nvl(ie_gerar_conj_auto_w,'S'));

	/* Cotas */
	insert into cm_conjunto_cota(
		nr_sequencia,
		cd_estabelecimento,
		qt_conjunto,
		ie_periodo,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_conjunto,
		cd_setor_atendimento,
		ie_situacao)
	select	cm_conjunto_cota_seq.nextval,
		cd_estabelecimento,
		qt_conjunto,
		ie_periodo,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_conjunto_w,
		cd_setor_atendimento,
		ie_situacao
	from	cm_conjunto_cota
	where	nr_seq_conjunto = nr_seq_conj_p;

	/* Itens */
	insert into cm_conjunto_item(
		nr_seq_conjunto,
		nr_seq_item,
		qt_item,
		dt_atualizacao,
		nm_usuario,
		ie_indispensavel)
	select	nr_seq_conjunto_w,
		nr_seq_item,
		qt_item,
		sysdate,
		nm_usuario_p,
		ie_indispensavel
	from	cm_conjunto_item
	where	nr_seq_conjunto = nr_seq_conj_p;
	
	end;
end loop;
close C01;

commit;

end cme_inserir_conj_classif;
/