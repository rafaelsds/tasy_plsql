create or replace
procedure ctb_copiar_ctb_grupo_conta(	cd_empresa_origem_p	number,
					cd_empresa_destino_p	number,
					nm_usuario_p		Varchar2) is 

	/*Variaveis de controle*/
	cd_grupo_w	number(10);
	nr_grupo_w	number(10);

	/*Cursor com dados filtrados */
	cursor c1 is
	select	cd_grupo,
		cd_empresa_destino_p cd_empresa,
		ds_grupo,
		sysdate dt_atualizacao,
		nm_usuario_p nm_usuario,
		cd_mascara,
		ie_debito_credito,
		ie_tipo,
		sysdate dt_atualizacao_nrec,
		nm_usuario_p nm_usuario_nrec,
		cd_exp_grupo
	from	ctb_grupo_conta
	where	cd_empresa	=	cd_empresa_origem_p;

	r1 c1%rowtype;

begin

/*Obtem ultimo grupo cadastrado*/
select	max(cd_grupo)
into	cd_grupo_w
from	ctb_grupo_conta;

open c1;
loop
fetch c1 into r1;
exit when c1%notfound;

	/* Verifica se o grupo de conta contabil ja foi cobiado anteriormente pra esta empresa, usando como comparação a empresa destiuno e a cd_grupo_conta_ref*/
	select	count(*)
	into	nr_grupo_w
	from	ctb_grupo_conta
	where	cd_grupo_ref = r1.cd_grupo
	and	cd_empresa  = cd_empresa_destino_p;
	
	if (	nr_grupo_w = 0 ) then
		
		cd_grupo_w := cd_grupo_w + 1;
		
		insert into ctb_grupo_conta (
			cd_grupo,
			cd_empresa,
			ds_grupo,
			dt_atualizacao,
			nm_usuario,
			cd_mascara,
			ie_debito_credito,
			ie_tipo,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_exp_grupo,
			cd_grupo_ref) 
		values (cd_grupo_w,
			r1.cd_empresa,
			r1.ds_grupo,
			r1.dt_atualizacao,
			r1.nm_usuario,
			r1.cd_mascara,
			r1.ie_debito_credito,
			r1.ie_tipo,
			r1.dt_atualizacao_nrec,
			r1.nm_usuario_nrec,
			r1.cd_exp_grupo,
			r1.cd_grupo);
			
	end if;

end loop;
close c1;

commit;

end ctb_copiar_ctb_grupo_conta;
/