create or replace
procedure excluir_preco_amb(	dt_inicio_vigencia_p	date,
			cd_edicao_p		number) is 

begin

if	(dt_inicio_vigencia_p	is not null) and
	(cd_edicao_p		is not null) then
	begin
	delete
	from	preco_amb
	where	dt_inicio_vigencia	= dt_inicio_vigencia_p
	and	cd_edicao_amb	= cd_edicao_p;
	end;
end if;

end excluir_preco_amb;
/