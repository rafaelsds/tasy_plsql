create or replace
procedure pls_alterar_congenere_event
			(	nr_seq_segurado_p	number,
				nr_seq_congenere_p	number,
				nm_usuario_p		varchar2) is

nr_seq_congenere_ant_w	number(10);

begin

if	(nr_seq_congenere_p is not null) then
	select	max(nr_seq_ops_congenere)
	into	nr_seq_congenere_ant_w
	from	pls_segurado
	where	nr_sequencia		= nr_seq_segurado_p;
	nr_seq_congenere_ant_w	:= nvl(nr_seq_congenere_ant_w,0);
	
	if	(nr_seq_congenere_ant_w <> nvl(nr_seq_congenere_p,0)) then
		update	pls_segurado
		set	nr_seq_ops_congenere	= decode(nr_seq_congenere_p,0,null,nr_seq_congenere_p),
			dt_atualizacao		= sysdate
		where	nr_sequencia		= nr_seq_segurado_p;
		
		pls_gerar_segurado_historico(
			nr_seq_segurado_p, '81', sysdate,
			wheb_mensagem_pck.get_texto(321830, 'NR_SEQ_CONGENERE_P=' || to_char(nr_seq_congenere_ant_w) ) ,'pls_alterar_congenere_event', null,
			null, null, null,
			sysdate, null, null,
			null, null, null,
			null, nm_usuario_p, 'S');
	end if;
end if;

commit;

end pls_alterar_congenere_event;
/