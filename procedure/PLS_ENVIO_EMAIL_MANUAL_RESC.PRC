create or replace
procedure pls_envio_email_manual_resc( 
					nr_seq_solicitacao_p	number,
					nr_seq_email_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is
			
cd_prioridade_w			pls_email_parametros.cd_prioridade%type;
nr_seq_email_w			pls_email.nr_sequencia%type;
nr_seq_anexo_w			pls_email_anexo.nr_sequencia%type;
ds_mensagem_w			varchar2(4000); 
ds_assunto_w			varchar2(255);
ds_destinatario_w		varchar2(255);
vl_parametro_w			varchar2(255);

Cursor C01 is
	select 	nr_sequencia,
		ds_anexo 
	from 	w_pls_envio_email_anexo
	where 	nr_seq_w_email = nr_seq_email_p
	order by 1;
	
begin
--Origem : 2 - OPS - Gest�o de Rescis�o de Contrato
select	nvl(max(cd_prioridade),5)
into	cd_prioridade_w
from	pls_email_parametros
where	ie_origem 		= 2
and	cd_estabelecimento 	= cd_estabelecimento_p
and	ie_situacao 		= 'A';

Obter_Param_Usuario(268, 10, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);

if (nr_seq_email_p is not null) and (nr_seq_solicitacao_p is not null) then
	
	select 	ds_mensagem, 
		ds_assunto,
		ds_destinatario
	into	ds_mensagem_w, 
		ds_assunto_w,
		ds_destinatario_w
	from 	w_pls_envio_email 
	where 	nr_sequencia = nr_seq_email_p;	
	
	select	pls_email_seq.nextval	
	into	nr_seq_email_w
	from 	dual;

	insert into pls_email (	
				nr_sequencia,
				nr_seq_solic_rescisao,
				cd_estabelecimento,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nm_usuario,
				dt_atualizacao,
				ie_tipo_mensagem,
				ie_status,
				ie_origem,
				ds_remetente,
				ds_mensagem,
				ds_destinatario,
				ds_assunto,
				cd_prioridade)
	values(			nr_seq_email_w,
				nr_seq_solicitacao_p,
				cd_estabelecimento_p,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				2,	--Envio de Dados da Solicita��o de rescis�o						
				'P',	--Pendente
				2,	--OPS - Gest�o de Rescis�o de Contrato
				vl_parametro_w, 	--Remetente
				ds_mensagem_w,
				ds_destinatario_w,
				ds_assunto_w,
				cd_prioridade_w				
				);
	for r_C01_w in C01 loop
		select	pls_email_anexo_seq.nextval
		into	nr_seq_anexo_w
		from	dual;

		insert into pls_email_anexo (	
					nr_sequencia,
					nr_seq_email,
					nm_usuario,
					nm_usuario_nrec,
					dt_atualizacao,
					dt_atualizacao_nrec,
					ds_arquivo,
					ie_tipo_anexo )
		values(			nr_seq_anexo_w,
					nr_seq_email_w,
					nm_usuario_p,
					nm_usuario_p,
					sysdate,
					sysdate,				
					r_C01_w.ds_anexo,
					'A'
					);	
	
	end loop;
	
	commit;
end if;

end pls_envio_email_manual_resc;
/