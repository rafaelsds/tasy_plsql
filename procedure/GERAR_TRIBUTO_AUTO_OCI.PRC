create or replace
procedure	gerar_tributo_auto_oci(
			nr_ordem_compra_p		number,
			nr_item_oci_p			number,
			nm_usuario_p			varchar2) is

qt_existe_w			number(10);
nr_seq_regra_w			number(10);
cd_material_w			number(6);
cd_grupo_material_w		number(3);
cd_subgrupo_material_w		number(3);
cd_classe_material_w		number(5);
qt_material_w			number(13,4);
vl_unitario_material_w		number(13,4);
cd_tributo_w			number(3);
vl_tributo_w			number(15,2);
tx_tributo_w			number(15,4);
vl_ipi_w				number(15,2);
cd_estabelecimento_w		number(5);
ie_considera_ipi_w			varchar2(1);
ie_desc_nf_w           tributo.ie_desc_nf%type;
vl_desconto_w          ordem_compra_item.vl_desconto%type;
ie_frete_w             ordem_compra.ie_frete%type;
vl_frete_w             ordem_compra.vl_frete%type;
vl_frete_rateado_w     ordem_compra.vl_frete%type;
ie_tipo_tributo_w      tributo.ie_tipo_tributo%type;
qt_itens_w             number(10);

cursor	c00 is
select	a.cd_tributo
from	regra_trib_automatico_oci a,
	tributo b
where	a.cd_estabelecimento = cd_estabelecimento_w
and	a.cd_tributo = b.cd_tributo
and	a.tx_tributo > 0
and	b.ie_tipo_tributo <> 'ICMS'
group by	a.cd_tributo;

cursor	c01 is
select	a.nr_sequencia
from	regra_trib_automatico_oci a
where	a.tx_tributo > 0
and	a.cd_tributo = cd_tributo_w
and	a.cd_estabelecimento = cd_estabelecimento_w
and	nvl(a.cd_material,cd_material_w)			= cd_material_w
and	nvl(a.cd_grupo_material,cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material,cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(a.cd_classe_material,cd_classe_material_w)		= cd_classe_material_w
order by	nvl(a.cd_material,0),
	nvl(a.cd_classe_material,0),
	nvl(a.cd_subgrupo_material,0),
	nvl(a.cd_grupo_material,0);

cursor	c02 is
select	a.nr_sequencia
from	regra_trib_automatico_oci a,
	tributo b
where	a.cd_tributo = b.cd_tributo
and	a.tx_tributo > 0
and	b.ie_tipo_tributo = 'ICMS'
and	a.cd_estabelecimento = cd_estabelecimento_w
and	nvl(a.cd_material,cd_material_w)			= cd_material_w
and	nvl(a.cd_grupo_material,cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(a.cd_subgrupo_material,cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(a.cd_classe_material,cd_classe_material_w)		= cd_classe_material_w
order by	nvl(a.cd_material,0),
	nvl(a.cd_classe_material,0),
	nvl(a.cd_subgrupo_material,0),
	nvl(a.cd_grupo_material,0);

begin

select	cd_estabelecimento,
        ie_frete,
		nvl(vl_frete, 0)
into	cd_estabelecimento_w,
        ie_frete_w,
		vl_frete_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p;

select	count(*)
into	qt_existe_w
from	regra_trib_automatico_oci
where	cd_estabelecimento = cd_estabelecimento_w;

if	(qt_existe_w > 0) then
	begin

	select	a.cd_material,
		b.cd_grupo_material,
		b.cd_subgrupo_material,
		b.cd_classe_material,
		a.qt_material,
		a.vl_unitario_material
	into	cd_material_w,
		cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		qt_material_w,
		vl_unitario_material_w
	from	ordem_compra_item a,
		estrutura_material_v b
	where	a.cd_material = b.cd_material
	and	a.nr_ordem_compra = nr_ordem_compra_p
	and	a.nr_item_oci = nr_item_oci_p;

	open c00;
	loop
	fetch c00 into
		cd_tributo_w;
	exit when c00%notfound;
		begin

		nr_seq_regra_w	:= 0;

		open c01;
		loop
		fetch c01 into
			nr_seq_regra_w;
		exit when c01%notfound;
		end loop;
		close c01;

		if	(nr_seq_regra_w > 0) then
			begin

			select	tx_tributo
			into	tx_tributo_w
			from	regra_trib_automatico_oci
			where	nr_sequencia = nr_seq_regra_w;
            
			select	count(*)
			into	qt_itens_w
			from	ordem_compra_item
			where	nr_ordem_compra = nr_ordem_compra_p;
        
			select	ie_tipo_tributo
			into	ie_tipo_tributo_w
			from	tributo
			where	cd_tributo = cd_tributo_w;
    
			if (ie_frete_w = 'F' and vl_frete_w > 0 and qt_itens_w > 0 and ie_tipo_tributo_w = 'IPI') then
				vl_frete_rateado_w := vl_frete_w / qt_itens_w;
			else
				vl_frete_rateado_w := 0;
			end if;
            
			select	nvl(ie_desc_nf,'N')
			into	ie_desc_nf_w
			from	tributo
			where	cd_tributo = cd_tributo_w;
            
			if (ie_desc_nf_w = 'S') then
				select nvl(vl_desconto,0)
				into vl_desconto_w
				from ordem_compra_item
				where nr_ordem_compra = nr_ordem_compra_p
				and nr_item_oci = nr_item_oci_p;
            else
				vl_desconto_w := 0;
            end if;

            vl_tributo_w	:= ((tx_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w) + vl_frete_rateado_w - vl_desconto_w));
	
			insert into ordem_compra_item_trib(
				nr_ordem_compra,
				nr_item_oci,
				cd_tributo,
				pr_tributo,
				vl_tributo,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec) values (
					nr_ordem_compra_p,
					nr_item_oci_p,
					cd_tributo_w,
					tx_tributo_w,
					vl_tributo_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p);

			end;
		end if;

		end;
	end loop;
	close c00;	

	nr_seq_regra_w	:= 0;

	open c02;
	loop
	fetch c02 into
		nr_seq_regra_w;
	exit when c02%notfound;
	end loop;
	close c02;

	if	(nr_seq_regra_w > 0) then
		begin

		Obter_Param_Usuario(917,101,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_considera_ipi_w);

		if	(ie_considera_ipi_w = 'S') then

			select	sum(nvl(a.vl_tributo,0))
			into	vl_ipi_w
			from	ordem_compra_item_trib a,
				tributo b
			where	a.nr_ordem_compra = nr_ordem_compra_p
			and	a.nr_item_oci = nr_item_oci_p
			and	a.cd_tributo = b.cd_tributo
			and	b.ie_tipo_tributo = 'IPI';

		else
			vl_ipi_w	:= 0;
		end if;

		select	tx_tributo,
			cd_tributo
		into	tx_tributo_w,
			cd_tributo_w
		from	regra_trib_automatico_oci
		where	nr_sequencia = nr_seq_regra_w;
        
        select	nvl(ie_desc_nf,'N')
        into	ie_desc_nf_w
        from	tributo
        where	cd_tributo = cd_tributo_w;
            
        if (ie_desc_nf_w = 'S') then
            select nvl(vl_desconto,0)
            into vl_desconto_w
            from ordem_compra_item
            where nr_ordem_compra = nr_ordem_compra_p
            and nr_item_oci = nr_item_oci_p;
        else
            vl_desconto_w := 0;
        end if;

		vl_tributo_w	:= ((tx_tributo_w / 100) * ((qt_material_w * vl_unitario_material_w) + vl_ipi_w - vl_desconto_w));

		insert into ordem_compra_item_trib(
			nr_ordem_compra,
			nr_item_oci,
			cd_tributo,
			pr_tributo,
			vl_tributo,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec) values (
				nr_ordem_compra_p,
				nr_item_oci_p,
				cd_tributo_w,
				tx_tributo_w,
				vl_tributo_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p);

		end;
	end if;

	commit;

	end;
end if;

end gerar_tributo_auto_oci;
/