create or replace
procedure PLS_DELETE_INTERFACE_MENS(nr_seq_vinculo_est_p	varchar,
				    nr_seq_vinculo_est_tit_p    varchar,
				    nm_usuario_p		Varchar2) is 

begin
	
	if (nr_seq_vinculo_est_p <> null) then 
		DELETE 	FROM w_pls_interface_mens a 
		WHERE  	a.nm_usuario = nm_usuario_p 
			   AND    NVL(a.nr_seq_vinculo_est,0) NOT IN(   
			   SELECT   NVL(x.nr_seq_vinculo_est,0) 
			   FROM     w_pls_interface_mens x 
			   WHERE    x.nm_usuario = nm_usuario_p
			   AND NVL(x.NR_SEQ_VINCULO_EST,0) IN (nr_seq_vinculo_est_p));
	end if;
	
	if (nr_seq_vinculo_est_tit_p <> null) then 
		delete 	from w_pls_interface_mens a 
		where  	a.nm_usuario = nm_usuario_p
			and    nvl(a.nr_seq_vinculo_est_tit,0) not in(
			select   nvl(x.nr_seq_vinculo_est_tit,0) 
			from     w_pls_interface_mens x  where    
			x.nm_usuario = nm_usuario_p 
			AND NVL(x.NR_SEQ_VINCULO_EST,0) IN (nr_seq_vinculo_est_tit_p));
	end if;

commit;

end PLS_DELETE_INTERFACE_MENS;
/