create or replace
procedure consiste_sup_int_marca(
			nr_sequencia_p		number,
			cd_cnpj_fabricante_p	varchar2,
			nr_seq_tipo_p 		number,
			nm_usuario_p		varchar2) is
		
qt_existe_fabricante_w		number(10);		
qt_existe_tipo_marca_w		number(10);
nr_inconsistencia_w		number(5);

begin

delete 	from sup_int_marca_consist
where	nr_sequencia = nr_sequencia_p
and	ie_integracao = '2';

select 	count(*)
into	qt_existe_fabricante_w
from 	pessoa_juridica
where 	cd_sistema_ant = cd_cnpj_fabricante_p;

select 	count(*)
into	qt_existe_tipo_marca_w
from 	tipo_marca
where	cd_sistema_ant = nr_seq_tipo_p;

if 	(qt_existe_fabricante_w = 0) then

	select	nvl(max(nr_inconsistencia),0) + 1
	into	nr_inconsistencia_w
	from	sup_int_marca_consist
	where	nr_sequencia = nr_sequencia_p
	and	ie_integracao = '2';
	
	insert into sup_int_marca_consist(
		nr_sequencia,       
		nr_inconsistencia,
		ds_mensagem,        
		ie_integracao,
		dt_atualizacao,     
		nm_usuario,         
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_sequencia_p,
		nr_inconsistencia_w,
		wheb_mensagem_pck.get_texto(1043543),
		2,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p);

end if;

if 	(qt_existe_tipo_marca_w = 0) then

	select	nvl(max(nr_inconsistencia),0) + 1
	into	nr_inconsistencia_w
	from	sup_int_marca_consist
	where	nr_sequencia = nr_sequencia_p
	and	ie_integracao = '2';

	insert into sup_int_marca_consist(
		nr_sequencia,       
		nr_inconsistencia,
		ds_mensagem,        
		ie_integracao,
		dt_atualizacao,     
		nm_usuario,         
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_sequencia_p,
		nr_inconsistencia_w,
		wheb_mensagem_pck.get_texto(1043544),
		2,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p);
	
end if;

end consiste_sup_int_marca;
/