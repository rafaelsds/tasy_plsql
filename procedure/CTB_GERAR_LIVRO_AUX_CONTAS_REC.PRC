create or replace
procedure ctb_gerar_livro_aux_contas_rec(	cd_empresa_p		varchar2,
						cd_estabelecimento_p	varchar2,
						dt_inicial_p		date,
						dt_final_p		date,	
						nm_usuario_p		Varchar2) is

nr_titulo_w					w_ctb_livro_aux_contas_rec.nr_titulo%type;
dt_emissao_w					w_ctb_livro_aux_contas_rec.dt_emissao%type;
dt_vencimento_w					w_ctb_livro_aux_contas_rec.dt_vencimento%type;
nr_carteira_w					w_ctb_livro_aux_contas_rec.nr_seq_carteira%type;
nm_pessoa_fisica_w				w_ctb_livro_aux_contas_rec.nm_pessoa_fisica%type;
ds_modalidade_w					w_ctb_livro_aux_contas_rec.ds_modalidade%type;
tp_contratacao_w				w_ctb_livro_aux_contas_rec.ds_tipo_contratacao%type;
dt_inicio_cobertura_w				w_ctb_livro_aux_contas_rec.dt_inicio_cobertura%type;
dt_fim_cobertura_w				w_ctb_livro_aux_contas_rec.dt_fim_cobertura%type;
vl_titulo_w					w_ctb_livro_aux_contas_rec.vl_titulo%type;
vl_saldo_titulo_w				w_ctb_livro_aux_contas_rec.vl_saldo_titulo%type;
vl_recebido_vigencia_w				w_ctb_livro_aux_contas_rec.vl_receb_vigencia%type;
vl_receb_ate_vigencia_w				w_ctb_livro_aux_contas_rec.vl_receb_ate_vigencia%type;
vl_recebido_w					w_ctb_livro_aux_contas_rec.vl_recebido%type;
dt_recebimento_w				w_ctb_livro_aux_contas_rec.dt_recebimento%type;
dt_pagamento_previsto_w				w_ctb_livro_aux_contas_rec.dt_pagamento_previsto%type;
vl_ant_vencimento_w				w_ctb_livro_aux_contas_rec.vl_titulo%type;
qt_dias_venc_w					w_ctb_livro_aux_contas_rec.qt_dias_venc%type;
dt_inicial_w					date;
dt_final_w					date;
ds_classif_diops_w				w_ctb_livro_aux_contas_rec.ds_classif_diops%type;
qt_dias_vencido_w				number(4);
						

cursor	c01 is

select	a.nr_titulo,
	a.dt_emissao,
	a.dt_vencimento,
	ps.nr_sequencia,
	substr(obter_nome_pessoa_fisica(ps.cd_pessoa_fisica,null),1,100),
	substr(pls_obter_dados_produto(pp.nr_sequencia,'P'),1,255), 
	substr(pls_obter_dados_produto(ps.nr_sequencia,'C'),1,255),
	ms.dt_inicio_cobertura,
	ms.dt_fim_cobertura,
	a.vl_titulo,
	a.vl_saldo_titulo,
	ms.vl_pro_rata_dia,
	ms.vl_antecipacao,
	b.vl_recebido,
	a.dt_pagamento_previsto,
	b.dt_recebimento
from	estabelecimento			c,
	titulo_receber			a,
	titulo_receber_liq		b,
	pls_segurado			ps,
	pls_plano			pp,
	pls_mensalidade_segurado	ms
where	a.nr_titulo           	= b.nr_titulo
and	ms.nr_seq_mensalidade	= a.nr_seq_mensalidade
and	pp.nr_sequencia		= ms.nr_seq_plano
and	ps.nr_sequencia		= ms.nr_seq_segurado
and	a.cd_estabelecimento	= c.cd_estabelecimento
and	nvl(a.cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
and	c.cd_empresa		= cd_empresa_p
and	exists	(select	1
		from	movimento_contabil_doc		x,
			lote_contabil			l,
			movimento_contabil		w
		where	x.nm_tabela		= 'TITULO_RECEBER_LIQ'
		and	l.nr_lote_contabil	= x.nr_lote_contabil
		and	x.nr_seq_doc_compl	= b.nr_sequencia
		and	x.nr_documento		= a.nr_titulo
		and	w.nr_sequencia		= x.nr_seq_movimento
		and	w.nr_lote_contabil	= w.nr_lote_contabil
		and	substr(w.cd_classificacao,1,5)	= '1.2.3'
		and	l.dt_referencia	between dt_inicial_w	and	dt_final_w)
group by 	a.nr_titulo,
		a.dt_emissao, 
		a.dt_vencimento, 
		ps.nr_sequencia,
		ps.cd_pessoa_fisica,
		pp.nr_sequencia,
		ms.dt_inicio_cobertura,
		ms.dt_fim_cobertura,
		a.vl_titulo,
		a.vl_saldo_titulo,
		ms.vl_pro_rata_dia,
		ms.vl_antecipacao,
		b.vl_recebido,
		a.dt_pagamento_previsto,
		b.dt_recebimento;					
begin

dt_inicial_w	:= trunc(dt_inicial_p);
dt_final_w	:= fim_dia(dt_final_p);
qt_dias_venc_w	:= obter_dias_entre_datas(dt_inicial_p,dt_final_p);

delete w_ctb_livro_aux_contas_rec
where	nm_usuario	= nm_usuario_p;
commit;

open c01;
loop
fetch c01 into
	nr_titulo_w,
	dt_emissao_w,
	dt_vencimento_w,
	nr_carteira_w,
	nm_pessoa_fisica_w,
	ds_modalidade_w,
	tp_contratacao_w,
	dt_inicio_cobertura_w,
	dt_fim_cobertura_w,
	vl_titulo_w,
	vl_saldo_titulo_w,
	vl_recebido_vigencia_w,
	vl_receb_ate_vigencia_w,
	vl_recebido_w,
	dt_pagamento_previsto_w,
	dt_recebimento_w;
exit when c01%notfound;

	begin

	if	(trunc(dt_recebimento_w,'dd')	< trunc(dt_pagamento_previsto_w,'dd')) then

		vl_ant_vencimento_w	:= nvl(vl_recebido_w,0);

	else

		vl_ant_vencimento_w	:= 0;

	end if;

	
	if	(dt_vencimento_w > sysdate) then
		ds_classif_diops_w := obter_desc_expressao(330037);	
	else	
		qt_dias_vencido_w := dt_vencimento_w - sysdate;
		
		if	(qt_dias_vencido_w <= 30) then
			ds_classif_diops_w := obter_desc_expressao(490309);
		elsif	(qt_dias_vencido_w <= 60) then
			ds_classif_diops_w := obter_desc_expressao(490310);
		elsif	(qt_dias_vencido_w <= 90) then
			ds_classif_diops_w := obter_desc_expressao(490311);
		elsif	(qt_dias_vencido_w <= 120) then
			ds_classif_diops_w := obter_desc_expressao(491705);
		else	
			ds_classif_diops_w := obter_desc_expressao(492546);
		end if;	
	end if;
	
	insert into w_ctb_livro_aux_contas_rec(
		nr_titulo,
		dt_emissao,
		dt_vencimento,
		dt_atualizacao,
		nr_seq_carteira,
		nm_pessoa_fisica,
		ds_modalidade,
		ds_tipo_contratacao,
		dt_inicio_cobertura,
		dt_fim_cobertura,
		vl_titulo,
		vl_saldo_titulo,
		vl_receb_vigencia,
		vl_receb_ate_vigencia,
		vl_recebido,
		qt_dias_venc,
		ds_classif_diops,
		dt_pagamento_previsto,
		dt_recebimento,
		cd_estabelecimento,
		nm_usuario)
	values(	nr_titulo_w,
		dt_emissao_w,
		dt_vencimento_w,
		sysdate,
		nr_carteira_w,
		nm_pessoa_fisica_w,
		ds_modalidade_w,
		tp_contratacao_w,
		dt_inicio_cobertura_w,
		dt_fim_cobertura_w,
		vl_titulo_w,
		vl_saldo_titulo_w,
		vl_recebido_vigencia_w,
		vl_receb_ate_vigencia_w,
		vl_ant_vencimento_w,
		qt_dias_venc_w,
		ds_classif_diops_w,
		dt_pagamento_previsto_w,
		dt_recebimento_w,
		cd_estabelecimento_p,
		nm_usuario_p);
	end;
end loop;

commit;

end ctb_gerar_livro_aux_contas_rec;
/