create or replace
procedure pls_migrar_carencia
			(	nr_seq_segurado_p	number,
				nr_seq_segurado_novo_p	number,
				nr_seq_contrato_p	number,
				nr_seq_plano_p		number,
				nm_usuario_p		varchar2) is

nr_seq_tipo_carencia_contr_w	number(10);
dt_inicio_vigencia_contrato_w	date;
qt_dias_contrato_w		number(10);
nr_seq_tipo_carencia_plano_w	number(10);
dt_inicio_vigencia_plano_w	date;
qt_dias_plano_w			number(10);
qt_carencia_contrato_w		number(10);
qt_carencia_plano_w		number(10);
qt_carencias_w			number(10);
qt_carencia_seg_w		number(10);
dt_inclusao_operadora_w		date;
nr_seq_tipo_carencia_seg_w	number(10);
dt_inicio_vig_seg_w		date;
qt_dias_seg_w			number(10);
dt_ultima_carencia_w		date;
dt_fim_vigencia_w		date;
ie_inserir_segurado_w		varchar2(1) := 'S';
ie_inserir_contrato_w		varchar2(1) := 'S';
dt_contratacao_w		date;
dt_inicio_vigencia_w		date;
dt_adesao_ant_w			date;
nr_seq_plano_sca_w		number(10);
nr_seq_tipo_carencia_sca_w	number(10);
dt_inicio_vigencia_sca_w	date;
qt_dias_sca_w			number(10);
qt_carencia_sca_w		number(10);
dt_adesao_sca_w			date;
nr_seq_plano_ant_w		number(10);
nr_seq_contrato_ant_w		number(10);
qt_carencias_contrato_ant_w	number(10);
qt_carencias_plano_ant_w	number(10);
dt_inclusao_benef_w		date;
nr_seq_grupo_carencia_w		number(10);
qt_dias_carencias_grupo_w	number(10);
qt_dias_fora_abrang_ant_w	pls_carencia.qt_dias_fora_abrang_ant%type;
ie_abrangencia_ant_w		pls_plano.ie_abrangencia%type;
ie_abrangencia_w		pls_plano.ie_abrangencia%type;
ie_carencia_abrangencia_ant_w	pls_parametros.ie_carencia_abrangencia_ant%type;
nr_seq_carencia_w		pls_carencia.nr_sequencia%type;

Cursor C01 is /*migra carencias do contrato  novo*/
	select	nr_seq_tipo_carencia,
		nvl(dt_inicio_vigencia,dt_adesao_ant_w),
		qt_dias
	from	pls_carencia
	where	nr_seq_contrato = nr_seq_contrato_p
	and	((nr_seq_plano_contrato = nr_seq_plano_p) or (nr_seq_plano_contrato is null));

Cursor C02 is /* migra carencia plano novo */
	select	nr_seq_tipo_carencia,
		nvl(dt_inicio_vigencia,dt_adesao_ant_w),
		qt_dias
	from	pls_carencia
	where	nr_seq_plano = nr_seq_plano_p
	and	dt_inclusao_operadora_w between nvl(dt_inicio_vig_plano,dt_inclusao_operadora_w) and nvl(dt_fim_vig_plano,dt_inclusao_operadora_w);

Cursor C03 is /* migra carencia segurado anterior */
	select	nr_seq_tipo_carencia,
		nvl(dt_inicio_vigencia,dt_adesao_ant_w),
		qt_dias
	from	pls_carencia
	where	nr_seq_segurado = nr_seq_segurado_p
	and	ie_cpt = 'N'
	and	nvl(ie_origem_carencia_benef,'P') = 'P'
	union
	select	nr_seq_tipo_carencia,
		nvl(dt_inicio_vigencia,dt_adesao_ant_w),
		qt_dias
	from	pls_carencia
	where	nr_seq_contrato = nr_seq_contrato_ant_w
	and	((nr_seq_plano_contrato = nr_seq_plano_ant_w) or (nr_seq_plano_contrato is null))
	and	not exists (	select	1
				from	pls_carencia	x
				where	x.nr_seq_segurado = nr_seq_segurado_p
				and	x.ie_cpt = 'N'
				and	nvl(x.ie_origem_carencia_benef,'P') = 'P')
	union
	select	nr_seq_tipo_carencia,
		nvl(dt_inicio_vigencia,dt_adesao_ant_w),
		qt_dias
	from	pls_carencia
	where	nr_seq_plano = nr_seq_plano_ant_w
	and	dt_adesao_ant_w between nvl(dt_inicio_vig_plano,dt_adesao_ant_w) and nvl(dt_fim_vig_plano,dt_adesao_ant_w)
	and	not exists (	select	1
				from	pls_carencia	x
				where	x.nr_seq_segurado = nr_seq_segurado_p
				and	x.ie_cpt = 'N'
				and	nvl(x.ie_origem_carencia_benef,'P') = 'P')
	and	not exists (	select	1
				from	pls_carencia	x
				where	x.nr_seq_contrato = nr_seq_contrato_ant_w
				and	((x.nr_seq_plano_contrato = nr_seq_plano_ant_w) or (x.nr_seq_plano_contrato is null)));

Cursor C04 is 
	select	a.nr_seq_plano,
		a.dt_inicio_vigencia,
		a.dt_inclusao_benef
	from	pls_sca_vinculo	a,
		pls_plano	b
	where	a.nr_seq_plano	= b.nr_sequencia
	and	a.nr_seq_segurado	= nr_seq_segurado_p
	and	((a.dt_fim_vigencia is null) or (a.dt_fim_vigencia > dt_contratacao_w))
	and	exists	(	select	1
				from	pls_sca_regra_contrato	x,
					pls_plano		y
				where	y.nr_sequencia		= x.nr_seq_plano
				and	x.nr_seq_contrato	= nr_seq_contrato_p /*aaschlote 27/04/2011 OS - 310695*/
				and	y.nr_seq_classificacao	= b.nr_seq_classificacao)/*aaschlote 23/05/2011 OS - 310688*/
	union
	select	a.nr_seq_plano,
		a.dt_inicio_vigencia,
		a.dt_inclusao_benef
	from	pls_sca_vinculo	a,
		pls_plano	b
	where	a.nr_seq_plano		= b.nr_sequencia
	and	a.nr_seq_segurado	= nr_seq_segurado_p
	and	((a.dt_fim_vigencia is null) or (a.dt_fim_vigencia > dt_contratacao_w))
	and	not exists	(	select	1
					from	pls_sca_regra_contrato	x,
						pls_plano		y
					where	y.nr_sequencia		= x.nr_seq_plano
					and	x.nr_seq_contrato	= nr_seq_contrato_p
					and	y.nr_seq_classificacao	= b.nr_seq_classificacao) /*aaschlote 27/04/2011 OS - 310695*/
	and	exists	(	select	1
				from	pls_plano_servico_adic	x,
					pls_plano		y
				where	y.nr_sequencia		= x.nr_seq_plano_adic
				and	x.nr_seq_plano		= nr_seq_plano_p 
				and	y.nr_seq_classificacao	= b.nr_seq_classificacao)
	union
	select	a.nr_seq_plano,
		a.dt_inicio_vigencia,
		a.dt_inclusao_benef
	from	pls_sca_vinculo	a,
		pls_plano	b
	where	a.nr_seq_plano		= b.nr_sequencia
	and	a.nr_seq_segurado	= nr_seq_segurado_p
	and	((a.dt_fim_vigencia is null) or (a.dt_fim_vigencia > dt_contratacao_w))
	and	not exists	(	select	1
					from	pls_sca_regra_contrato	x,
						pls_plano		y
					where	y.nr_sequencia		= x.nr_seq_plano
					and	x.nr_seq_contrato	= nr_seq_contrato_p
					and	y.nr_seq_classificacao	= b.nr_seq_classificacao)
	and	not exists	(	select	1
					from	pls_plano_servico_adic	x,
						pls_plano		y
					where	y.nr_sequencia		= x.nr_seq_plano_adic
					and	x.nr_seq_plano		= nr_seq_plano_p
					and	y.nr_seq_classificacao	= b.nr_seq_classificacao);

Cursor C05 is /* migra plano sca */
	select	nr_seq_tipo_carencia,
		nvl(dt_adesao_sca_w,dt_adesao_ant_w),
		qt_dias
	from	pls_carencia
	where	nr_seq_plano = nr_seq_plano_sca_w;

begin

/*Obter dados do seguraod antigo*/
select	a.dt_inclusao_operadora,
	a.nr_seq_plano,
	a.nr_seq_contrato,
	decode(b.ie_abrangencia,'E','ES','GE','A','GM','RB','M','MU','N','NA')
into	dt_adesao_ant_w,
	nr_seq_plano_ant_w,
	nr_seq_contrato_ant_w,
	ie_abrangencia_ant_w
from	pls_segurado 	a,
	pls_plano	b
where	a.nr_seq_plano = b.nr_sequencia
and	a.nr_sequencia = nr_seq_segurado_p;

/*Obter dados do segurado novo*/
select	a.dt_inclusao_operadora,
	a.dt_contratacao,
	decode(b.ie_abrangencia,'E','ES','GE','A','GM','RB','M','MU','N','NA')
into	dt_inclusao_operadora_w,
	dt_contratacao_w,
	ie_abrangencia_w
from	pls_segurado	a,
	pls_plano	b
where	a.nr_seq_plano = b.nr_sequencia
and	a.nr_sequencia	= nr_seq_segurado_novo_p;

/* Obter par�metro  considerar abrang�ncia ao migrar de contrato OPS - Gest�o de operadoras*/
select  max(ie_carencia_abrangencia_ant)
into	ie_carencia_abrangencia_ant_w
from	pls_parametros;

open C03;
loop
fetch C03 into
	nr_seq_tipo_carencia_seg_w,
	dt_inicio_vig_seg_w,
	qt_dias_seg_w;
exit when C03%notfound;
	begin
	select	max(qt_dias)
	into	qt_dias_plano_w
	from	pls_carencia
	where	nr_seq_plano		= nr_seq_plano_p
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_seg_w;
	
	select	max(qt_dias)
	into	qt_carencia_contrato_w
	from	pls_carencia
	where	nr_seq_plano		= nr_seq_plano_p
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_seg_w;
	
	select	max(qt_dias)
	into	qt_dias_sca_w
	from	pls_sca_vinculo	b,
		pls_carencia	a
	where	a.nr_seq_plano		= b.nr_seq_plano
	and	b.nr_seq_segurado	= nr_seq_segurado_p
	and	a.nr_seq_tipo_carencia	= nr_seq_tipo_carencia_seg_w;
	
	if	(((nvl(qt_carencia_contrato_w,3000) < qt_dias_seg_w) or
		(nvl(qt_dias_plano_w,3000) < qt_dias_seg_w)) or
		(nvl(qt_dias_sca_w,3000) < qt_dias_seg_w)) then
		ie_inserir_segurado_w	:= 'N';
	else
		ie_inserir_segurado_w	:= 'S';
	end if;
	
	/*Verifica car�ncias do segurado antigo e que ainda n�o foram cumpridas*/
	select	count(1)
	into	qt_carencia_seg_w
	from	pls_carencia
	where	nr_seq_segurado		= nr_seq_segurado_novo_p
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_seg_w;	
	
	
	if	(qt_carencia_seg_w = 0) and (ie_inserir_segurado_w = 'S') and
		(pls_consistir_sexo_carencia(nr_seq_segurado_novo_p,nr_seq_tipo_carencia_seg_w) = 'S') then
		/*OS - 262495 - aaschlote 16/12/2010*/
		dt_inicio_vigencia_w	:= dt_inicio_vig_seg_w;
		dt_fim_vigencia_w	:= (dt_inicio_vigencia_w + qt_dias_seg_w);
		
		select pls_carencia_seq.nextval
		into nr_seq_carencia_w
		from dual;
		
		insert	into pls_carencia
			(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_segurado, dt_inicio_vigencia, qt_dias,
				nr_seq_tipo_carencia, ds_observacao, dt_fim_vigencia, ie_origem_carencia_benef, ie_carencia_anterior,qt_dias_fora_abrang_ant)
		values	(	nr_seq_carencia_w, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_segurado_novo_p, dt_inicio_vigencia_w, qt_dias_seg_w,
				nr_seq_tipo_carencia_seg_w, 'Origem: Benefici�rio migrado', dt_fim_vigencia_w, 'P', 'S',qt_dias_fora_abrang_ant_w);
		
		/* Verifica par�metro de abrangencia de plano e se abr�ngencia anterior � maior que atual */
		if(nvl(ie_carencia_abrangencia_ant_w,'N') = 'S') then
			/* verifica se a abrang�ncia atual � maior que a anterior ,*/
			if(ie_abrangencia_ant_w <> ie_abrangencia_w) then
			
				if(ie_abrangencia_w = 'NA' ) or  
				(ie_abrangencia_w = 'A'  and ie_abrangencia_ant_w <> 'NA') or
				(ie_abrangencia_w = 'ES' and ie_abrangencia_ant_w <> 'NA' and ie_abrangencia_ant_w <> 'A') or
				(ie_abrangencia_w = 'RB' and ie_abrangencia_ant_w = 'MU') then						
					
					select	max(qt_dias)
					into	qt_dias_fora_abrang_ant_w
					from	pls_carencia
					where	nr_seq_plano		= nr_seq_plano_p
					and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_seg_w;	
					
					pls_alterar_qt_dias_abrang_ant(nr_seq_carencia_w,qt_dias_fora_abrang_ant_w, nm_usuario_p,null );
					
				end if;
			end if;	
		end if;		
				
	end if;
	end;
end loop;
close C03;

open C01;
loop
fetch C01 into
	nr_seq_tipo_carencia_contr_w,
	dt_inicio_vigencia_contrato_w,
	qt_dias_contrato_w;
exit when C01%notfound;
	begin
	select	max(qt_dias)
	into	qt_dias_plano_w
	from	pls_carencia
	where	nr_seq_plano		= nr_seq_plano_p
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_contr_w;
	
	select	max(qt_dias)
	into	qt_dias_sca_w
	from	pls_sca_vinculo	b,
		pls_carencia	a
	where	a.nr_seq_plano		= b.nr_seq_plano
	and	b.nr_seq_segurado	= nr_seq_segurado_p
	and	a.nr_seq_tipo_carencia	= nr_seq_tipo_carencia_contr_w;
	
	qt_dias_carencias_grupo_w	:= pls_obter_dias_caren_benef_ant(nr_seq_segurado_p,nr_seq_tipo_carencia_contr_w);
	
	/*aaschlote 18/04/2012 OS - 437518*/
	if	(qt_dias_carencias_grupo_w >= 0) and
		(qt_dias_carencias_grupo_w < qt_dias_plano_w) then
		qt_dias_plano_w	:= qt_dias_carencias_grupo_w;
	end if;
	
	if	((nvl(qt_dias_plano_w,3000) < qt_dias_contrato_w) or
		(nvl(qt_dias_sca_w,3000) < qt_dias_contrato_w)) then
		ie_inserir_segurado_w	:= 'N';
	else
		ie_inserir_segurado_w	:= 'S';
	end if;
	
	/* Verifica se no novo contrato existe a car�ncia do segurado, caso sta verifica se a mesma j� foi cumprida*/
	select	count(1)
	into	qt_carencia_contrato_w
	from	pls_carencia
	where	nr_seq_contrato		= nr_seq_contrato_p
	and	(	select	count(1)
			from	pls_carencia
			where	nr_seq_segurado		= nr_seq_segurado_novo_p
			and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_contr_w ) = 0;
	
	select	count(1)
	into	qt_carencias_contrato_ant_w
	from	pls_carencia
	where	nr_seq_contrato		= nr_seq_contrato_ant_w
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_contr_w;
	
	select	count(1)
	into	qt_carencias_plano_ant_w
	from	pls_carencia
	where	nr_seq_plano	= nr_seq_plano_ant_w
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_contr_w;
	
	select	count(1)
	into	qt_carencias_w
	from	pls_carencia
	where	nr_seq_segurado = nr_seq_segurado_novo_p
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_contr_w;
	
	/*OS - 262495 - aaschlote 16/12/2010*/
	if	(qt_carencia_contrato_w > 0) and
		(ie_inserir_segurado_w	= 'S') and
		(qt_carencias_w	= 0) and
		(pls_consistir_sexo_carencia(nr_seq_segurado_novo_p,nr_seq_tipo_carencia_contr_w) = 'S') then
		/*OS - 296952 - aaschlote 01/03/2011*/
		if	(qt_carencias_contrato_ant_w	= 0) and
			(qt_carencias_plano_ant_w	= 0) then
			dt_inicio_vigencia_w	:= dt_contratacao_w;
		else
			dt_inicio_vigencia_w	:= dt_adesao_ant_w;
		end if;
		
		dt_fim_vigencia_w	:= (dt_inicio_vigencia_w + qt_dias_contrato_w);
		insert	into pls_carencia
			(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_segurado, dt_inicio_vigencia, qt_dias,
				nr_seq_tipo_carencia, ds_observacao, dt_fim_vigencia, ie_origem_carencia_benef, ie_carencia_anterior)
		values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_segurado_novo_p, dt_inicio_vigencia_w, qt_dias_contrato_w,
				nr_seq_tipo_carencia_contr_w, 'Origem: Migra��o para contrato atual', dt_fim_vigencia_w, 'P', 'S');
	end if;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into
	nr_seq_tipo_carencia_plano_w,
	dt_inicio_vigencia_plano_w,
	qt_dias_plano_w;
exit when C02%notfound;
	begin
	
	select	max(qt_dias)
	into	qt_dias_sca_w
	from	pls_sca_vinculo	b,
		pls_carencia	a
	where	a.nr_seq_plano		= b.nr_seq_plano
	and	b.nr_seq_segurado	= nr_seq_segurado_p
	and	a.nr_seq_tipo_carencia	= nr_seq_tipo_carencia_plano_w;
	
	qt_dias_carencias_grupo_w	:= pls_obter_dias_caren_benef_ant(nr_seq_segurado_p,nr_seq_tipo_carencia_plano_w);
	
	/*aaschlote 18/04/2012 OS - 437518*/
	if	(qt_dias_carencias_grupo_w >= 0) and
		(qt_dias_carencias_grupo_w < qt_dias_plano_w) then
		qt_dias_plano_w	:= qt_dias_carencias_grupo_w;
	end if;
	
	if	(nvl(qt_dias_sca_w,3000) < qt_dias_plano_w) then
		ie_inserir_segurado_w	:= 'N';
	else
		ie_inserir_segurado_w	:= 'S';
	end if;
	
	select	count(1)
	into	qt_carencias_contrato_ant_w
	from	pls_carencia
	where	nr_seq_contrato	= nr_seq_contrato_ant_w
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_plano_w;
	
	select	count(1)
	into	qt_carencias_plano_ant_w
	from	pls_carencia
	where	nr_seq_plano	= nr_seq_plano_ant_w
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_plano_w;
	
	select	count(1)
	into	qt_carencias_w
	from	pls_carencia
	where	nr_seq_segurado = nr_seq_segurado_novo_p
	and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_plano_w;
	
	/*OS - 262495 - aaschlote 16/12/2010*/
	if	(qt_carencias_w	= 0) and
		(pls_consistir_sexo_carencia(nr_seq_segurado_novo_p,nr_seq_tipo_carencia_plano_w) = 'S') then
		/*OS - 296952 - aaschlote 01/03/2011*/
		if	(qt_carencias_contrato_ant_w	= 0) and
			(qt_carencias_plano_ant_w	= 0) then
			dt_inicio_vigencia_w	:= dt_contratacao_w;
		else
			dt_inicio_vigencia_w	:= dt_adesao_ant_w;
		end if;	
		
		dt_fim_vigencia_w	:= (dt_inicio_vigencia_w + qt_dias_plano_w);
		insert	into pls_carencia
			(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				nr_seq_segurado, dt_inicio_vigencia, qt_dias,
				nr_seq_tipo_carencia, ds_observacao, dt_fim_vigencia, ie_origem_carencia_benef, ie_carencia_anterior)
		values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_segurado_novo_p, dt_inicio_vigencia_w, qt_dias_plano_w,
				nr_seq_tipo_carencia_plano_w, 'Origem: Migra��o para produto do benefici�rio atual', dt_fim_vigencia_w, 'P', 'S');
	end if;
	end;
end loop;
close C02;

open C04;
loop
fetch C04 into
	nr_seq_plano_sca_w,
	dt_adesao_sca_w,
	dt_inclusao_benef_w;
exit when C04%notfound;
	begin
	open C05;
	loop
	fetch C05 into
		nr_seq_tipo_carencia_sca_w,
		dt_inicio_vigencia_sca_w,
		qt_dias_sca_w;
	exit when C05%notfound;
		begin
		
		select	count(1)
		into	qt_carencias_contrato_ant_w
		from	pls_carencia
		where	nr_seq_contrato		= nr_seq_contrato_ant_w
		and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_sca_w;
		
		select	count(1)
		into	qt_carencias_plano_ant_w
		from	pls_carencia
		where	nr_seq_plano		= nr_seq_plano_ant_w
		and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_contr_w;
		
		select	count(1)
		into	qt_carencias_w
		from	pls_carencia
		where	nr_seq_segurado 	= nr_seq_segurado_novo_p
		and	nr_seq_tipo_carencia	= nr_seq_tipo_carencia_sca_w;
		
		/*OS - 262495 - aaschlote 16/12/2010*/
		if	(qt_carencias_w	= 0) and
			(pls_consistir_sexo_carencia(nr_seq_segurado_novo_p,nr_seq_tipo_carencia_sca_w) = 'S') then
			/*OS - 296952 - aaschlote 01/03/2011*/
			if	(qt_carencias_contrato_ant_w	= 0) and
				(qt_carencias_plano_ant_w	= 0) then
				dt_inicio_vigencia_w	:= nvl(dt_inclusao_benef_w,dt_adesao_ant_w);
			end if;
			
			dt_fim_vigencia_w	:= (dt_inicio_vigencia_w + qt_dias_sca_w);
			insert	into pls_carencia
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
					nr_seq_segurado, dt_inicio_vigencia, qt_dias,
					nr_seq_tipo_carencia, ds_observacao, dt_fim_vigencia, ie_origem_carencia_benef, ie_carencia_anterior)
			values	(	pls_carencia_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
					nr_seq_segurado_novo_p, dt_inicio_vigencia_w, qt_dias_sca_w,
					nr_seq_tipo_carencia_sca_w, 'Origem: Migra��o do SCA do benefici�rio migrado', dt_fim_vigencia_w, 'S', 'S');
		end if;
		end;
	end loop;
	close C05;
	end;
end loop;
close C04;

begin
select	max(dt_fim_vigencia)
into	dt_ultima_carencia_w
from	pls_carencia
where	nr_seq_segurado	= nr_seq_segurado_novo_p;
exception
when others then
	dt_ultima_carencia_w	:= null;
end;

if	(dt_ultima_carencia_w is not null) then
	update	pls_segurado
	set	dt_ultima_carencia	= dt_ultima_carencia_w
	where	nr_sequencia		= nr_seq_segurado_novo_p;
end if;

end pls_migrar_carencia;
/
