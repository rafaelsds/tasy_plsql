create or replace
procedure ctb_obter_doc_movto
			(	cd_tipo_lote_contabil_p		number,
				nm_atributo_p			varchar2,
				ie_opcao_p			varchar2,
				dt_vigencia_p			date,
				dt_inicio_p			date,
				dt_fim_p			date,
				ds_atributos_p			varchar2,
				nm_usuario_p			varchar2,
				ie_regra_p		out	varchar2,
				vl_atributo_p		out	varchar2,
				ie_origem_docto_p	out	number,
				nr_sequencia_p			number default 0) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	IE_OPCAO_P
		VR - Verificar a regra cadastrada no Parametros Contabilidade
		VC - Verificar se j� existe um item cadastrado com mesmo tipo de lote e atributo no Parametros Contabilidade
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

qt_reg_w		number(10);
ds_retorno_w		varchar2(4000);
ds_atributos_w		varchar2(4000);
nm_atributo_w		varchar2(255);
vl_atributo_w		varchar2(255);
nr_pos_sep_w		number(10);
nr_pos_igual_w		number(10);
ie_docto_w		varchar2(255)	:= 'N';
nr_atrib_regra_w	atributo_doc_movto.nm_atributo%type;
nr_pos_fim_w		number(10)	:= 1;
nm_atributo_contab_w	ctb_regra_doc_movto.nm_atributo_contab%type	:= nvl(nm_atributo_p,'X');


Cursor c_regras is
	select	c.nm_atributo
	from	ctb_regra_doc_movto		a,
		ctb_regra_doc_movto_atrib	b,
		atributo_doc_movto		c
	where	a.nr_sequencia		= b.nr_seq_regra_doc
	and	c.nr_sequencia		= b.nr_seq_atributo
	and	a.cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
	and	nvl(a.nm_atributo_contab, nm_atributo_contab_w) = nm_atributo_contab_w	
	and	dt_vigencia_p between a.dt_inicio_vig and nvl(a.dt_fim_vig,dt_vigencia_p)
	order by
		b.nr_seq_ordem,
		b.nr_sequencia;

begin
ie_regra_p		:= 'N';
vl_atributo_p		:= null;
ie_origem_docto_p	:= null;

if	(ie_opcao_p = 'VC') then
	ds_retorno_w	:= 'N';
	
	
	select	count(1)
	into	qt_reg_w
	from	ctb_regra_doc_movto	a
	where	a.cd_tipo_lote_contabil	= cd_tipo_lote_contabil_p
	and	((a.nm_atributo_contab	= nm_atributo_p)
	or	(nvl(nm_atributo_p,'X') = 'X'))
	and	dt_inicio_p between a.dt_inicio_vig and nvl(a.dt_fim_vig,dt_inicio_p)
	and	((dt_fim_p between a.dt_inicio_vig and nvl(a.dt_fim_vig,dt_fim_p)) or (dt_fim_p is null))
	and     a.nr_sequencia  <>  nvl(nr_sequencia_p,0)
	and	rownum			= 1;
	
	if	(qt_reg_w > 0) then
		ie_regra_p	:= 'S';
	end if;
elsif	(ie_opcao_p = 'VR') then
	ie_docto_w	:= 'N';
	qt_reg_w	:= 0;
	
	open c_regras;
	loop
	fetch c_regras into	
		nr_atrib_regra_w;
	exit when ((c_regras%notfound) or (ie_docto_w = 'S'));
		begin
		ds_atributos_w	:= substr(ds_atributos_p, 1, 4000);
		
		while	(length(ds_atributos_w) > 0) and
			(ie_docto_w = 'N') loop
			begin
			nr_pos_igual_w	:= instr(ds_atributos_w, '=');
			nr_pos_sep_w	:= instr(ds_atributos_w, ';');
			nr_pos_fim_w	:= 1;
			
			if	(nr_pos_sep_w = 0) then -- �ltimo atributo
				nr_pos_sep_w	:= length(ds_atributos_w);
				nr_pos_fim_w	:= 0;
			end if;
			
			nm_atributo_w	:= substr(ds_atributos_w, 1, nr_pos_igual_w - 1);
			
			if	(nr_atrib_regra_w = nm_atributo_w) then
				vl_atributo_p	:= substr(ds_atributos_w, nr_pos_igual_w + 1, (nr_pos_sep_w) - (nr_pos_igual_w + nr_pos_fim_w));
				vl_atributo_p	:= substr(vl_atributo_p, 1, 255);
				
				if	(nvl(vl_atributo_p,'0') != '0') then
					-- Dom�nio 6091
					if	(nr_atrib_regra_w = 'NR_NOTA_FISCAL') then
						ie_origem_docto_p	:= '1'; -- Nota fiscal
					elsif	(nr_atrib_regra_w in ('NR_SEQ_NOTA_FISCAL', 'NR_NFE_IMP')) then
						ie_origem_docto_p	:= '1'; -- Nota fiscal	
					elsif	(nr_atrib_regra_w in ('NR_TITULO_PAGAR','NR_TITULO_PAGAR_GERADO','NR_TITULO_PAGAR_BAIXADO')) then
						ie_origem_docto_p	:= '2'; -- T�tulo a pagar
					elsif	(nr_atrib_regra_w in ('NR_TITULO_RECEBER','NR_TITULO_RECEBER_GERADO','NR_TITULO_RECEBER_BAIXADO')) then
						ie_origem_docto_p	:= '3'; -- T�tulo a receber
					--ie_origem_docto_p	:= '4'; Conta Paciente
					elsif	(nr_atrib_regra_w = 'NR_SEQ_PROTOCOLO') then
						ie_origem_docto_p	:= '5'; -- Protocolo conv�nio
					elsif	(nr_atrib_regra_w = 'NR_SEQ_BEM') then
						ie_origem_docto_p	:= '6'; -- Bem (Patrim�nio)
					elsif	(nr_atrib_regra_w = 'NR_SEQ_REPASSE') then
						ie_origem_docto_p	:= '7'; --  Repasse para terceiros
					elsif	(nr_atrib_regra_w = 'NR_SEQ_RECEB_CONVENIO') then
						ie_origem_docto_p	:= '8'; -- Recebimento conv�nio
					--ie_origem_docto_p	:= '9'; Lote produ��o
					--ie_origem_docto_p	:= '10'; Ordem compra
					elsif	(nr_atrib_regra_w = 'NR_SEQ_MOVTO_CONTROLE_BANCARIO') then
						ie_origem_docto_p	:= '11';
					elsif	(nr_atrib_regra_w = 'NR_SEQ_MOVTO_TESOURARIA') then
						ie_origem_docto_p	:= '12';
					elsif	(nr_atrib_regra_w = 'NR_DOCUMENTO') then					
						ie_origem_docto_p	:= '12';
					elsif	(nr_atrib_regra_w = 'NR_BORDERO_PAGAR') then
						ie_origem_docto_p	:= '13';
					elsif	(nr_atrib_regra_w = 'NR_BORDERO_RECEBER') then
						ie_origem_docto_p	:= '14';
					elsif	(nr_atrib_regra_w = 'NR_INTERNO_CONTA') then
						ie_origem_docto_p	:= '4';
					elsif	(nr_atrib_regra_w = 'NR_SEQ_CHEQUE') then
						ie_origem_docto_p	:= '15';
					end if;
					
					ie_docto_w	:= 'S';
				end if;
			end if;
			
			ds_atributos_w	:= substr(ds_atributos_w, nr_pos_sep_w + 1, length(ds_atributos_w));
			end;
		end loop;
		end;
	end loop;
	close c_regras;	
end if;

if	(c_regras%ISOPEN) then
	close c_regras;
end if;

end ctb_obter_doc_movto;
/