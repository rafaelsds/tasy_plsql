create or replace
procedure man_gerar_ordem_servico_par(	nr_sequencia_p		out     	number,
					cd_pessoa_solicitante_p	in	varchar2,
					nr_seq_localizacao_p	in	number,
					nr_seq_equipamento_p	in	number,
					ds_dano_breve_p		in	varchar2,
					ds_dano_p		in	varchar2,
					cd_funcao_p		in	number,
					ie_classificacao_p		in	varchar2,
					ie_forma_receb_p		in	varchar2,
					dt_inicio_previsto_p		in	date,
					dt_fim_previsto_p		in	date,
					nr_seq_estagio_p		in	number,
					ie_tipo_ordem_p		in	varchar2,
					nr_grupo_planej_p		in	number,
					nr_grupo_trabalho_p	in	number,
					ie_prioridade_p		in	varchar2,
					ie_status_ordem_p		in	varchar2,
					nm_usuario_p		in	varchar2,
					nr_seq_proj_cron_etapa_p	in	number,
					nr_seq_tre_agenda_p	in	number,
					ie_origem_os_p		in	varchar2,
					nr_seq_severidade_p 	in 	varchar2 default null,
					nr_versao_cliente_p 	in 	varchar2 default null ) is

nr_sequencia_w			number(10);
nm_usuario_exec_w		varchar2(15);
qt_existe_w			number(10,0);
	
cursor c01 is
select	nm_usuario_exec
from	man_equip_usuario_exec
where	nr_seq_equipamento 				= nr_seq_equipamento_p
and	nvl(nr_seq_grupo_trabalho,nvl(nr_grupo_trabalho_p,0)) 	= nvl(nr_grupo_trabalho_p,0)
and	nvl(nr_seq_grupo_planej,nvl(nr_grupo_planej_p,0)) 	= nvl(nr_grupo_planej_p,0);


begin

select	man_ordem_servico_seq.nextval
into	nr_sequencia_w
from	dual;

nr_sequencia_p := nr_sequencia_w;

insert	into man_ordem_servico(
	nr_sequencia,
	nr_seq_localizacao,
	nr_seq_equipamento,
	cd_pessoa_solicitante,
	dt_ordem_servico,
	ie_prioridade,
	ie_parado,
	ds_dano_breve,
	dt_atualizacao,
	nm_usuario,
	dt_inicio_desejado,
	dt_conclusao_desejada,
	ds_dano,
	dt_inicio_previsto,
	dt_fim_previsto,
	dt_inicio_real,
	dt_fim_real,
	ie_tipo_ordem,
	ie_status_ordem,
	nr_grupo_planej,
	nr_grupo_trabalho,
	nr_seq_tipo_solucao,
	ds_solucao,
	nm_usuario_exec,
	qt_contador,
	nr_seq_planej,
	nr_seq_tipo_contador,
	nr_seq_estagio,
	cd_projeto,
	nr_seq_etapa_proj,
	dt_reabertura,
	cd_funcao,
	nm_tabela,
	ie_classificacao,
	nr_seq_origem,
	nr_seq_projeto,
	ie_grau_satisfacao,
	nr_seq_indicador,
	nr_seq_causa_dano,
	ie_forma_receb,
	nr_seq_cliente,
	nr_seq_grupo_des,
	nr_seq_grupo_sup,
	nr_seq_superior,
	ie_eficacia,
	dt_prev_eficacia,
	cd_pf_eficacia,
	nr_seq_nao_conform,
	nr_seq_complex,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_obriga_news,
	nr_seq_meta_pe,
	nr_seq_classif,
	nr_seq_nivel_valor,
	nm_usuario_lib_news,
	dt_libera_news,
	dt_envio_wheb,
	ds_contato_solicitante,
	ie_prioridade_desen,
	ie_prioridade_sup,
	nr_seq_proj_cron_etapa,
	nr_seq_tre_agenda,
	ie_origem_os,
	nr_seq_severidade,
	nr_versao_cliente_abertura
) values (	nr_sequencia_w,				-- nr_sequencia,
	nr_seq_localizacao_p,			-- nr_seq_localizacao,
	nr_seq_equipamento_p,			-- nr_seq_equipamento,
	cd_pessoa_solicitante_p,			-- cd_pessoa_solicitante,
	sysdate,					-- dt_ordem_servico,
	ie_prioridade_p,				-- ie_prioridade,
	'N',					-- ie_parado,
	substr(ds_dano_breve_p,1,80),		-- ds_dano_breve,
	sysdate,					-- dt_atualizacao,
	nm_usuario_p,				-- nm_usuario,
	sysdate,					-- dt_inicio_desejado,
	(sysdate + 15),				-- dt_conclusao_desejada,
	ds_dano_p,				-- ds_dano,
	nvl(dt_inicio_previsto_p,sysdate),		-- dt_inicio_previsto,
	dt_fim_previsto_p,				-- dt_fim_previsto,
	sysdate,					-- dt_inicio_real,
	null,					-- dt_fim_real,
	nvl(ie_tipo_ordem_p,'1'),			-- ie_tipo_ordem,
	nvl(ie_status_ordem_p,'1'),			-- ie_status_ordem,
	nr_grupo_planej_p,				-- nr_grupo_planej,
	nr_grupo_trabalho_p,			-- nr_grupo_trabalho,
	null,					-- nr_seq_tipo_solucao,
	null,					-- ds_solucao,
	decode(ie_status_ordem_p,'1',null,nm_usuario_p), -- nm_usuario_exec,
	null,					-- qt_contador,
	null,					-- nr_seq_planej,
	null,					-- nr_seq_tipo_contador,
	nr_seq_estagio_p,				-- nr_seq_estagio,
	null,					-- cd_projeto,
	null,					-- nr_seq_etapa_proj,
	null,					-- dt_reabertura,
	cd_funcao_p,				-- cd_funcao,
	null,					-- nm_tabela,
	ie_classificacao_p,				-- ie_classificacao,
	null,					-- nr_seq_origem,
	null,					-- nr_seq_projeto,
	null,					-- ie_grau_satisfacao,
	null,					-- nr_seq_indicador,
	null,					-- nr_seq_causa_dano,
	nvl(ie_forma_receb_p,'W'),			-- ie_forma_receb,
	null,					-- nr_seq_cliente,
	null,					-- nr_seq_grupo_des,
	null,					-- nr_seq_grupo_sup,
	null,					-- nr_seq_superior,
	null,					-- ie_eficacia,
	null,					-- dt_prev_eficacia,
	null,					-- cd_pf_eficacia,
	null,					-- nr_seq_nao_conform,
	null,					-- nr_seq_complex,
	null,					-- dt_atualizacao_nrec,
	nm_usuario_p,				-- nm_usuario_nrec,
	null,					-- ie_obriga_news,
	null,					-- nr_seq_meta_pe,
	null,					-- nr_seq_classif,
	null,					-- nr_seq_nivel_valor,
	null,					-- nm_usuario_lib_news,
	null,					-- dt_libera_news,
	null,					-- dt_envio_wheb,
	null,					-- ds_contato_solicitante,
	null,					-- ie_prioridade_desen,
	null,					-- ie_prioridade_sup
	nr_seq_proj_cron_etapa_p,			-- nr_seq_proj_cron_etapa
	nr_seq_tre_agenda_p,			-- nr_seq_tre_agenda
	ie_origem_os_p,				-- ie_origem_os
	nr_seq_severidade_p,			-- nr_seq_severidade
	nr_versao_cliente_p 			-- nr_versao_cliente_abertura
);

open C01;
loop
fetch C01 into	
	nm_usuario_exec_w;
exit when C01%notfound;
	begin
	select 	count(*)
	into	qt_existe_w
	from	man_ordem_servico_exec
	where	nr_seq_ordem 	= nr_sequencia_w
	and	nm_usuario_exec	= nm_usuario_exec_w;
	
	if	(qt_existe_w = 0) then
		insert into  man_ordem_servico_exec(
			nr_sequencia,
			nr_seq_ordem,
			dt_atualizacao,
			nm_usuario,
			nm_usuario_exec )
		values(	man_ordem_servico_exec_seq.nextval,
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			nm_usuario_exec_w);
	end if;
	end;
end loop;
close C01;

/*commit; Anderson 03/10/2011 - Solicitado na OS368686 do HSL*/

end man_gerar_ordem_servico_par;
/