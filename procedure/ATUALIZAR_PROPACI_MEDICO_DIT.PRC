create or replace
procedure Atualizar_Propaci_Medico_Dit(	nr_prescricao_p		number,
					ie_opcao_p			varchar2,
					nm_usuario_p		Varchar2) is 
/* 
EX 	= M�dico executor
*/
nr_seq_propaci_w	number(15);
cd_medico_exec_w	varchar2(10);
ie_medico_w			varchar2(1);
nr_seq_interno_w	number(10);

Cursor C01 is
	select	nr_seq_interno
	from	prescr_proc_ditado b,
		prescr_procedimento a
	where	a.nr_seq_interno	= b.nr_Seq_prescr_proc
	and	a.nr_prescricao		= nr_prescricao_p
	and	b.nm_usuario		= nm_usuario_p
	--and	to_char(b.dt_atualizacao,'dd/mm/yyyy hh24:mi') = to_char(sysdate,'dd/mm/yyyy hh24:mi')
	order by 1;

begin

open C01;
loop
fetch C01 into	
	nr_seq_interno_w;
exit when C01%notfound;
	begin
	if	(nr_seq_interno_w	is not null) then
		begin
		select 	a.nr_sequencia,
			Obter_Pessoa_Fisica_Usuario(b.nm_usuario,'C')
		into	nr_seq_propaci_w,
			cd_medico_exec_w
		from	prescr_proc_ditado b,
			procedimento_paciente a,
			prescr_procedimento c
		where	a.nr_prescricao			= c.nr_prescricao
		and	a.nr_sequencia_prescricao	= c.nr_sequencia
		and	c.nr_seq_interno		= b.nr_Seq_prescr_proc
		and	c.nr_seq_interno		= nr_seq_interno_w;
		
		ie_medico_w	:= Obter_se_medico(cd_medico_exec_w,'M');
		
		exception
		when others then
			nr_seq_propaci_w	:=0;
		end;
		
		if	(nvl(nr_seq_propaci_w,0)	<> 0) and
			(ie_medico_w = 'S')	then
							
			if	(nvl(ie_opcao_p,'EX') = 'EX') then
			
				update	procedimento_paciente
				set	cd_medico_exec_conta	= 	cd_medico_exec_w,
					dt_atualizacao = sysdate,
					nm_usuario = nm_usuario_p
				where	nr_sequencia	= 	nr_seq_propaci_w;
		
			end if;
		end if;
		
	end if;

	
	end; 
end loop;
close C01;


commit;

end Atualizar_Propaci_Medico_Dit;
/
