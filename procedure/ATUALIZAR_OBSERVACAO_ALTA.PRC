create or replace
procedure atualizar_observacao_alta (	nr_atendimento_p	number,
					ds_obs_alta_p		varchar2,
					nr_seq_obs_p		number default null) is 

begin

if	(nvl(nr_atendimento_p,0) > 0)then
	begin
	
	update 	atendimento_paciente 
	set 	ds_obs_alta 		= ds_obs_alta_p,
		nr_seq_tipo_obs_alta 	= nr_seq_obs_p
	where 	nr_atendimento 	= nr_atendimento_p;
	
	end;
end if;

commit;

end atualizar_observacao_alta;
/