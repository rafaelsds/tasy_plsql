create or replace
procedure pls_gerar_usuario_pericia
			(	nr_seq_analise_p		Varchar2,
				nm_usuario_p			Varchar2) is 
				
nr_seq_proposta_w		Number(10);
cd_pessoa_fisica_w		Varchar2(10);
nr_seq_plano_w			Number(10);
nr_seq_pagador_w		Number(10);
nr_seq_segurado_w		Number(10);
cd_estabelecimento_w		Number(4);
nr_seq_pessoa_proposta_w	Number(10);

begin

select	max(nr_seq_pessoa_proposta)
into	nr_seq_pessoa_proposta_w
from	pls_analise_adesao
where	nr_sequencia	= nr_seq_analise_p;

if	(nvl(nr_seq_pessoa_proposta_w,0) <> 0) then
	/* Obter dados dados da pessoa da proposta */
	select	a.nr_seq_proposta,
		a.cd_beneficiario,
		a.nr_seq_plano,
		a.nr_seq_pagador,
		b.cd_estabelecimento
	into	nr_seq_proposta_w,
		cd_pessoa_fisica_w,
		nr_seq_plano_w,
		nr_seq_pagador_w,
		cd_estabelecimento_w
	from	pls_proposta_adesao		b,
		pls_proposta_beneficiario	a
	where	a.nr_seq_proposta		= b.nr_sequencia
	and	a.nr_sequencia			= nr_seq_pessoa_proposta_w;

	select	max(nr_sequencia)
	into	nr_seq_segurado_w
	from	pls_segurado
	where	ie_tipo_segurado	= 'P'
	and	nr_seq_pessoa_proposta	= nr_seq_pessoa_proposta_w;
	
	--if	(nvl(nr_seq_segurado_w,0)	= 0) then 
	select	pls_segurado_seq.nextval
	into	nr_seq_segurado_w
	from	dual;

	insert into pls_segurado(
		nr_sequencia, dt_atualizacao, nm_usuario,
		dt_atualizacao_nrec, nm_usuario_nrec, cd_pessoa_fisica,
		nr_seq_titular, nr_seq_contrato, nr_seq_pagador,
		nr_seq_plano, nr_seq_tabela, nr_proposta_adesao,
		ie_tipo_segurado, nr_seq_pessoa_proposta, ie_situacao_atend,
		cd_estabelecimento)
	values(	nr_seq_segurado_w, sysdate, nm_usuario_p,
		sysdate, nm_usuario_p, cd_pessoa_fisica_w,
		null, null, null,
		nr_seq_plano_w, null, nr_seq_proposta_w,
		'P', nr_seq_pessoa_proposta_w,'A',
		cd_estabelecimento_w);
	--else
	--	wheb_mensagem_pck.exibir_mensagem_abort(265818,'');
		--Usu�rio j� est� registrado na per�cia m�dica!
	--end if;
end if;

commit;

end pls_gerar_usuario_pericia;
/
