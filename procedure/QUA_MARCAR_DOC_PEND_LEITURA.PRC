create or replace
procedure qua_marcar_doc_pend_leitura(  nr_seq_doc_p 	number,
					nm_usuario_p	Varchar2) is

cd_estabelecimento_w		number(04,0) := wheb_usuario_pck.get_cd_estabelecimento;
cd_perfil_w			number(05,0) := obter_perfil_ativo;
ie_visualiza_setor_usuario_w	varchar2(15);
ie_consid_toda_regra_w		varchar2(15);
nm_usuario_w			varchar2(15);
nr_seq_filho_w			number(10);

cursor c01 is
	select	nm_usuario
	from	usuario
	where	ie_situacao = 'A'
	and	substr(obter_acesso_docto_qual(	nr_seq_doc_p,
						nm_usuario, 
						cd_estabelecimento_w,
						ie_visualiza_setor_usuario_w, 
						ie_consid_toda_regra_w),1,1) = 'S';

cursor c02 is
	select	nr_sequencia
	from	qua_documento
	where	nr_seq_superior = nr_seq_doc_p;

begin
	obter_param_usuario(4000, 112, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_visualiza_setor_usuario_w);
	obter_param_usuario(4000, 113, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w, ie_consid_toda_regra_w);

	open C01;
	loop
	fetch C01 into	
		nm_usuario_w;
	exit when C01%notfound;
		begin
			insert  into qua_doc_log_acesso(
				dt_acesso,
				dt_atualizacao,
				nm_usuario,
				nr_seq_doc,
				nr_sequencia,
				dt_atualizacao_nrec,
				dt_leitura,
				ie_log,
				nm_usuario_nrec,
				cd_estabelecimento,
				cd_funcao,
                                dt_pendente)
			values(	sysdate,
				sysdate,
				nm_usuario_w,
				nr_seq_doc_p,
				qua_doc_log_acesso_seq.nextval,
				sysdate,
				null,
				'C',
				nm_usuario_w,
				cd_estabelecimento_w,
				wheb_usuario_pck.get_cd_funcao,
                                sysdate);

			open c02;
			loop
			fetch c02 into	
				nr_seq_filho_w;
			exit when c02%notfound;
				begin
					insert  into qua_doc_log_acesso(
						dt_acesso,
						dt_atualizacao,
						nm_usuario,
						nr_seq_doc,
						nr_sequencia,
						dt_atualizacao_nrec,
						dt_leitura,
						ie_log,
						nm_usuario_nrec,
						cd_estabelecimento,
						cd_funcao,
                                                dt_pendente)
					values(	sysdate,
						sysdate,
						nm_usuario_w,
						nr_seq_filho_w,
						qua_doc_log_acesso_seq.nextval,
						sysdate,
						null,
						'C',
						nm_usuario_w,
						cd_estabelecimento_w,
						wheb_usuario_pck.get_cd_funcao,
                                                sysdate);
				end;
			end loop;
			close c02;
		end;
	end loop;
	close C01;

commit;

end qua_marcar_doc_pend_leitura;
/
