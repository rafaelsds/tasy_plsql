Create or replace
procedure laudo_insere_laudo_pdf_serial( nr_seq_laudo_p 		number,
								  nr_acesso_dicom_p		varchar2,
								  ds_laudo_pdf_p		long,
								  nm_usuario_p			varchar2,
								  ds_erro_p				out varchar2,
								  nr_seq_laudo_pdf_p	out  number) is

nr_seq_laudo_pdf_w		laudo_paciente_pdf_serial.nr_sequencia%type;
								  
begin
								  
if (nr_seq_laudo_p is not null)	then
	
	select 	laudo_paciente_pdf_serial_seq.nextVal
	into	nr_seq_laudo_pdf_w
	from 	dual;
	
	begin
	
	insert into laudo_paciente_pdf_serial (nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_acesso_dicom,
									nr_seq_laudo,
									ds_pdf_serial)
							values (nr_seq_laudo_pdf_w,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_acesso_dicom_p,
									nr_seq_laudo_p,
									ds_laudo_pdf_p);
									

	--Atualizando o formato do resultado do laudo para buscar o laudo  em pdf serializado.									
	update	laudo_paciente
	set		ie_formato = 4
	where	nr_sequencia = nr_seq_laudo_p;
		
	exception
		when others then
			ds_erro_p := substr(sqlerrm,1,4000);
		
	end;

end if;
		
commit;								  

nr_seq_laudo_pdf_p := nr_seq_laudo_pdf_w;

end laudo_insere_laudo_pdf_serial;		


/
