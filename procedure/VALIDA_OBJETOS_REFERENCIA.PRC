CREATE OR REPLACE PROCEDURE VALIDA_OBJETOS_REFERENCIA(NR_SEQ_OBJETO_P IN NUMBER, CD_VERSAO_P IN VARCHAR2) IS

  wDtAjusteVersao      ajuste_versao.dt_atualizacao%TYPE;
  wCdVersao            aplicacao_tasy_versao.cd_versao%TYPE := cd_versao_p;
  wDtVersao            aplicacao_tasy_versao.dt_versao%TYPE;
  wNrSeqObjeto         objeto_sistema.nr_sequencia%TYPE := nr_seq_objeto_p;
  wNmObjeto            objeto_sistema.nm_objeto%TYPE;
  wMsgBloqueio         VARCHAR2(4000) := '';
  wDtDocObjeto         DATE;
  wStatus              VARCHAR2(50);

  CURSOR c_obj_ref(nmObjeto_p VARCHAR2) IS
    SELECT referenced_name, referenced_type FROM user_dependencies a WHERE a.name = nmObjeto_p AND referenced_owner = 'TASY';


  FUNCTION objIsDocAjusteVersao(nrSeqObj_p IN NUMBER, dtCriacaoObj_p IN DATE, dsVersao_p IN DATE) RETURN VARCHAR IS
    wReturn VARCHAR2(1) := 'N';
    aux     NUMBER := 0;
  BEGIN
    BEGIN

      SELECT 'N' INTO wReturn FROM ajuste_versao a, ajuste_versao_objeto b
      WHERE a.nr_sequencia = b.nr_seq_ajuste_versao
      AND   Trunc(a.dt_atualizacao) <= dtCriacaoObj_p
      AND   b.nr_seq_objeto = nrSeqObj_p                                                        
      AND   a.cd_versao = dsVersao_p;

    EXCEPTION
      WHEN No_Data_Found THEN
        wReturn := 'S';
    END;

    RETURN wReturn;
  END;

  FUNCTION getNrSeqObj(nmObjeto_p IN  VARCHAR2) RETURN VARCHAR IS
    wReturn VARCHAR2(50) := NULL;
  BEGIN
    BEGIN

      SELECT nr_sequencia INTO wReturn FROM objeto_sistema
      WHERE nm_objeto = nmObjeto_p;

    EXCEPTION
      WHEN No_Data_Found THEN
        wReturn := NULL;
    END;                                                                                                                                                          

    RETURN wReturn;
  END;

  FUNCTION tabelaOuAtributoAjusteVersao(nmTabela_p IN VARCHAR2, dtAtualizacao_p IN DATE, dsVersao_p IN VARCHAR2, nmAtributo_p IN VARCHAR DEFAULT NULL) RETURN VARCHAR IS
    wReturn VARCHAR2(1) := 'S';
  BEGIN
    BEGIN

      IF nmAtributo_p IS NULL THEN
      --tabela
        SELECT 'N' INTO wReturn FROM ajuste_versao a, ajuste_versao_tabela b
        WHERE a.nr_sequencia = b.nr_seq_ajuste_versao
        AND   Trunc(a.dt_atualizacao) <= Trunc(dtAtualizacao_p)
        AND   b.nm_tabela = nmTabela_p
        AND   a.cd_versao = dsVersao_p;

      ELSE
      --atributo
        SELECT 'N' INTO wReturn FROM ajuste_versao a, ajuste_versao_tabela b
        WHERE a.nr_sequencia = b.nr_seq_ajuste_versao
        AND   Trunc(a.dt_atualizacao) <= Trunc(dtAtualizacao_p)
        AND   b.nm_tabela = nmTabela_p
        AND   a.cd_versao = dsVersao_p
        AND   b.nm_atributo = nmAtributo_p;

      END IF;
    EXCEPTION
      WHEN No_Data_Found THEN
        wReturn := 'S';
    END;

    RETURN wReturn;
  END;

BEGIN
  BEGIN
    BEGIN

      DELETE FROM temp_objetos_referencia;

      SELECT Trunc(dt_versao)
          INTO wDtVersao
      FROM APLICACAO_TASY_VERSAO
      WHERE cd_versao = wCdVersao;

      SELECT nm_objeto INTO wNmObjeto FROM objeto_sistema WHERE nr_sequencia = wNrSeqObjeto;
    END;

    FOR r IN c_obj_ref(wNmObjeto) LOOP
      IF r.referenced_type = 'TABLE' THEN
        DECLARE
          createT NUMBER(10) := 0;
          createA NUMBER(10) := 0;
          updateT NUMBER(10) := 0;
          updateA NUMBER(10) := 0;

        BEGIN
          SELECT Count(1) INTO createT FROM tabela_sistema WHERE nm_tabela = r.referenced_name AND Trunc(dt_criacao) > wDtVersao;
          SELECT Count(1) INTO createA FROM tabela_atributo WHERE nm_tabela = r.referenced_name AND ie_tipo_atributo NOT IN ('LONG RAW','XMLTYPE','TIMESTAMP','VISUAL', 'FUNCTION') AND Trunc(dt_criacao) > wDtVersao;

          IF createT > 0 THEN
            SELECT dt_criacao INTO wDtDocObjeto FROM tabela_sistema WHERE nm_tabela = r.referenced_name AND Trunc(dt_criacao) > wDtVersao;
            SELECT Count(1) INTO createA FROM tabela_sistema WHERE nm_tabela = r.referenced_name AND Trunc(dt_atualizacao) > wDtVersao;

            wStatus := 'CRIADO';

            IF createA > 0 THEN
              wStatus := 'CRIADO - ATUALIZADO';
            END IF;


            IF tabelaOuAtributoAjusteVersao(r.referenced_name, wDtDocObjeto, wCdVersao) = 'S' THEN
              INSERT INTO temp_objetos_referencia(referenced_type, referenced_name,  status) VALUES (r.referenced_type, r.referenced_name, wStatus);
            END IF;
          END IF;
          
          wStatus := '';

          IF createA > 0 THEN
            FOR atrib IN (SELECT * FROM tabela_atributo WHERE nm_tabela = r.referenced_name AND ie_tipo_atributo NOT IN ('LONG RAW','XMLTYPE','TIMESTAMP','VISUAL', 'FUNCTION') AND dt_criacao > wDtVersao ) LOOP
            
            SELECT Count(1) INTO updateA FROM tabela_atributo WHERE nm_tabela = atrib.nm_tabela AND nm_atributo = atrib.nm_atributo AND ie_tipo_atributo NOT IN ('LONG RAW','XMLTYPE','TIMESTAMP','VISUAL', 'FUNCTION') AND dt_Atualizacao > wDtVersao;  
            
            wStatus := 'CRIADO';

            IF updateA > 0 THEN
              wStatus := 'CRIADO - ATUALIZADO';
            END IF;
   
              IF tabelaOuAtributoAjusteVersao(r.referenced_name, atrib.dt_criacao, wCdVersao, atrib.nm_atributo) = 'S' THEN
                INSERT INTO temp_objetos_referencia(referenced_type, referenced_name, status) VALUES (r.referenced_type, r.referenced_name || ' ' || obter_desc_expressao(329853) || ' ' ||atrib.nm_atributo, wStatus);
              END IF;
            END LOOP;
          END IF;
        END;

      ELSIF r.referenced_type = 'SEQUENCE' THEN
        DECLARE
        auxS NUMBER(10) := 0;
        BEGIN
          SELECT Max(cont) cont, Max(data) data INTO auxS, wDtDocObjeto FROM (
            SELECT Count(1) cont, Max(dt_criacao) data FROM objeto_sistema WHERE nm_objeto = r.referenced_name AND Trunc(dt_criacao) > wDtVersao
            UNION
            SELECT Count(1) cont, Max(dt_criacao) data FROM tabela_sistema WHERE nm_tabela =  SubStr(r.referenced_name, 1, Length(r.referenced_name) -4) AND Trunc(dt_criacao) > wDtVersao);

          IF auxS > 0 THEN
            IF tabelaOuAtributoAjusteVersao(SubStr(r.referenced_name, 1, Length(r.referenced_name) -4), wDtDocObjeto, wCdVersao) = 'S' OR objIsDocAjusteVersao(getNrSeqObj(r.referenced_name), wDtDocObjeto, wCdVersao) = 'S' THEN
              INSERT INTO temp_objetos_referencia(referenced_type, referenced_name, status) VALUES (r.referenced_type, r.referenced_name, 'CRIADO'); 
            END IF;
          END IF;

        END;
      ELSE

        DECLARE
            seq_aux NUMBER;
            updateO NUMBER;                                                             
        BEGIN
          
          BEGIN
            SELECT dt_criacao, nr_sequencia INTO wDtDocObjeto , seq_aux FROM (SELECT Trunc(dt_criacao) dt_criacao, nr_sequencia FROM objeto_sistema WHERE nm_objeto = r.referenced_name AND Trunc(dt_criacao) > wDtVersao ORDER BY 1 DESC) WHERE ROWNUM = 1;           
          EXCEPTION
            WHEN No_Data_Found THEN
              wDtDocObjeto := NULL;
          END;  
        
          IF wDtDocObjeto IS NOT NULL THEN
            SELECT Count(1) INTO updateO FROM (SELECT 1 FROM objeto_sistema WHERE nm_objeto = r.referenced_name AND Trunc(dt_atualizacao) > wDtVersao ORDER BY 1 DESC) WHERE ROWNUM = 1;

            wStatus := 'CRIADO';

            IF updateO > 0 THEN
              wStatus := 'CRIADO - ATUALIZADO';
            END IF;

            IF objIsDocAjusteVersao(seq_aux, wDtDocObjeto, wCdVersao) = 'S' THEN
              INSERT INTO temp_objetos_referencia(referenced_type, referenced_name, status) VALUES (r.referenced_type, r.referenced_name, wStatus);
            END IF;
          END IF;
        END;
      END IF;

    END LOOP;
  END;

EXCEPTION
  WHEN Others THEN
    raise_application_error(-20011, 'Erro => ' || SQLERRM);
END VALIDA_OBJETOS_REFERENCIA;
/