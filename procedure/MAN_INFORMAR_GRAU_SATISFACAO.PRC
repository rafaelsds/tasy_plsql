create or replace
procedure man_informar_grau_satisfacao (
			nr_seq_ordem_serv_p	number,
			ie_solucao_p		varchar2,
			ie_agilidade_p		varchar2,
			ie_conhecimento_p		varchar2,
			ie_cordialidade_p		varchar2,
			nm_usuario_p		varchar2,
			ie_grau_satisfacao_p	varchar2 default null,
			ds_observacao_p		varchar2 default '') is 


begin

begin
insert into man_ordem_serv_satisf
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_ordem_serv,
				ie_solucao,
				ie_agilidade,
				ie_conhecimento,
				ie_cordialidade,
				ie_grau_satisfacao_geral,
				ds_observacao)
		values	(	man_ordem_serv_satisf_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_ordem_serv_p,
				ie_solucao_p,
				ie_agilidade_p,
				ie_conhecimento_p,
				ie_cordialidade_p,
				ie_grau_satisfacao_p,
				substr(ds_observacao_p,1,4000));
exception
when dup_val_on_index then
	update	man_ordem_serv_satisf
	set	dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		ie_solucao		= ie_solucao_p,
		ie_agilidade		= ie_agilidade_p,
		ie_conhecimento		= ie_conhecimento_p,
		ie_cordialidade		= ie_cordialidade_p,
		ie_grau_satisfacao_geral	= ie_grau_satisfacao_p,
		ds_observacao		= substr(ds_observacao_p,1,4000)
	where	nr_seq_ordem_serv		= nr_seq_ordem_serv_p;	
end;

commit;

end man_informar_grau_satisfacao;
/