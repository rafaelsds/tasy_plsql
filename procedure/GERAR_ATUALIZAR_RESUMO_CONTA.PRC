CREATE OR REPLACE
PROCEDURE Gerar_Atualizar_Resumo_Conta
				(nr_interno_conta_p	number,
				 nr_atendimento_p	number) is

nr_atendimento_w	number(10,0);
nr_interno_conta_w	number(10,0);
ie_status_acerto_w	number(1);


CURSOR c01 is
	select	nr_atendimento,
		nr_interno_conta,
		ie_status_acerto
	from	conta_paciente
	where	nr_atendimento	 = nr_atendimento_p
	union
	select	nr_atendimento,
		nr_interno_conta,
		ie_status_acerto
	from	conta_paciente
	where	nr_interno_conta = nr_interno_conta_p;


BEGIN

open	c01;
loop
fetch	c01 into
	nr_atendimento_w,
	nr_interno_conta_w,
	ie_status_acerto_W;
exit	when c01%notfound;
		
	if 	(ie_status_acerto_W = 1) then
		Atualizar_Resumo_Conta(nr_interno_conta_w, 2);
	end if;

end loop;
close c01;

END Gerar_Atualizar_Resumo_Conta;
/