CREATE OR REPLACE
PROCEDURE Obter_Convenio_Particular(
                         		cd_estabelecimento_p		in	Number,
					CD_CONVENIO_P			OUT	 NUMBER,
					CD_CATEGORIA_P		OUT	 VARCHAR2) IS		

cd_categoria_w	varchar2(10);

BEGIN

begin
SELECT	nvl(cd_convenio_partic, 0),
	nvl(cd_categoria_partic, 0)
into	CD_CONVENIO_P,
	CD_CATEGORIA_P
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_p;
exception
	when others then
		CD_CONVENIO_P	:= 0;
		CD_CATEGORIA_P	:= 0;
end;

if (CD_CONVENIO_P = 0) or (CD_CATEGORIA_P = 0) then
	begin
	SELECT nvl(vl_parametro,VL_PARAMETRO_PADRAO)
	INTO   CD_CONVENIO_P
	FROM   FUNCAO_PARAMETRO
	WHERE  CD_FUNCAO = 81
	AND  	 NR_SEQUENCIA = 10;
	exception
		when others then
			cd_convenio_p	:= 0;
	end;

	begin
	SELECT nvl(vl_parametro,VL_PARAMETRO_PADRAO)
	INTO   CD_CATEGORIA_P
	FROM   FUNCAO_PARAMETRO
	WHERE  CD_FUNCAO = 81
	AND  	 NR_SEQUENCIA = 11;
	exception
		when others then
          		cd_categoria_p	:= 0;
	end;
end if;

begin
select cd_categoria
into cd_categoria_w
from categoria_convenio
where cd_convenio = cd_convenio_p
  and cd_categoria = cd_categoria_p;
exception
	when no_data_found then
		--r.aise_application_error(-20011,'Categoria do conv�nio particular n�o encontrada. Verifique par�metros do faturamento!');	
		wheb_mensagem_pck.exibir_mensagem_abort(263418);
end;

END Obter_Convenio_Particular;
/