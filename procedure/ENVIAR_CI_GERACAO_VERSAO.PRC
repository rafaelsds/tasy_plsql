create or replace
procedure enviar_ci_geracao_versao is 

begin

insert into comunic_interna 
				(nr_sequencia,
					dt_comunicado,
					ds_titulo,
					ds_comunicado,
					nm_usuario,
					nm_usuario_destino,
					ds_grupo,
					dt_atualizacao,
					ie_geral,
					ie_gerencial,
					cd_setor_destino,
					dt_liberacao)
		values(	comunic_interna_seq.nextval,
				sysdate,
				'GERA��O VERS�O!',
				'Hoje � dia de gera��o de vers�o oficial !!!' || chr(10) || chr(13) || 
				'Favor comitar suas altera��es at� �s 18:00 !!',
				'Tasy',
				null,
				'Ger�ncia Assistencial, ',
				sysdate,
				'N',
				'N',
				null,
				sysdate);

commit;

end enviar_ci_geracao_versao;
/