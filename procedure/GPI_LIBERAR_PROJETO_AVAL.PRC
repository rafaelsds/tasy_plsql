create or replace
procedure gpi_liberar_projeto_aval( 
				nm_usuario_p		Varchar2,
				nr_sequencia_p		number) is 

begin

update 	gpi_projeto_aval 
set 	nm_usuario 	= nm_usuario_p,
	dt_liberacao 	= sysdate,
	nm_usuario_lib  	= nm_usuario_p 
where 	nr_sequencia 	= nr_sequencia_p;

commit;

end gpi_liberar_projeto_aval;
/