create or replace
procedure Gerar_Sinal_Vital_ToLife	(	nr_atendimento_p	number,
						qt_peso_p		number,
						qt_altura_p		number,
						qt_dor_p		number,
						qt_glicose_p		number,
						qt_temperatura_p	number,
						qt_pa_sistolica_p	number,
						qt_pa_diastolica_p	number,
						qt_oxigenio_p		number,
						qt_pulso_p		number,
						nm_usuario_p		Varchar2,
						qt_freq_resp_p	number default null) is 

begin
	--Integracao nao implementada
	null;
end Gerar_Sinal_Vital_ToLife;
/
