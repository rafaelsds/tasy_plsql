create or replace
procedure ptu_importar_mat_unimed
			(	ds_conteudo_p		varchar2,
				nm_usuario_p		varchar2) is 
				
ds_registro_w			varchar(3);
ds_segundo_segmento_w		varchar2(255);	
nr_seq_linha_w			number(1);
ie_tipo_w			number(3);
nr_seq_max_w			number(10);
nr_tamanho_linha_w		number(10);

cd_material_ww			varchar2(8);
nr_cnpj_ww			varchar2(14);
ds_origem_ww			varchar2(1);
nr_registro_anvisa_ww		varchar2(20);
ie_tipo_situacao_ww		varchar2(1);

--'902'
cd_material_w			number(8);
ds_fracao_produto_w		varchar2(20);
nr_cnpj_w			varchar2(14);
nm_fabricante_w			varchar2(50);
nm_importador_w			varchar2(50);
ds_origem_w			number(1);
nr_registro_anvisa_w		number(20);
ie_tipo_situacao_w		number(1);
ds_motivo_ati_inat_w		varchar2(40);
vl_fabrica_w			number(11,4);
vl_max_consumidor_w		number(11,4);
dt_inicio_obrig_w		date;

--tamanhos vari�veis 902
nm_comercial_w			varchar2(255);
ds_produto_w			varchar2(255);
ds_especialidade_w		varchar(255);
ds_class_w			varchar(255);
ds_apresentacao_w		varchar(255);

--'905'
cd_medicamento_w		number(8);
dt_validade_anvisa_w		date;
vl_fator_conversao_w		number(5);
ie_ind_generico_w		varchar2(1);

--tamanhos vari�veis 905
ds_principio_ativo_w		varchar(255);
nm_apres_comercial_w		varchar(255);
ds_grau_farma_w			varchar(255);
ds_cla_farma_w			varchar(255);
ds_for_farma_w			varchar(255);

begin



/*obter o tipo de registro*/
select	length (ds_conteudo_p)
into	nr_tamanho_linha_w
from	dual;

if(nr_tamanho_linha_w <= 9) then	
	ds_registro_w	:= 'seg';
else
	ds_registro_w	:=	substr(ds_conteudo_p,9,3);
end if;

if	(ds_registro_w	= '902') then
	
	--apagar registros contidos na tabela temporaria
	delete  w_ptu_material_unimed;

	select	decode(substr(ds_conteudo_p,12,8),'0','',substr(ds_conteudo_p,12,8)),
		decode(substr(ds_conteudo_p,30,14),'0','',substr(ds_conteudo_p,30,14)),
		decode(substr(ds_conteudo_p,144,1),'0','',substr(ds_conteudo_p,144,1)),		
		decode(substr(ds_conteudo_p,145,20),'0','',substr(ds_conteudo_p,145,20)),
		decode(substr(ds_conteudo_p,173,1),'0','',substr(ds_conteudo_p,173,1))			
	into	cd_material_ww, 	
		nr_cnpj_ww,		
		ds_origem_ww,		
		nr_registro_anvisa_ww,	
		ie_tipo_situacao_ww	
	from	dual;	

	ds_fracao_produto_w 	:= trim(substr(ds_conteudo_p,20,10));
	nm_fabricante_w		:= trim(substr(ds_conteudo_p,44,50));
	nm_importador_w		:= trim(substr(ds_conteudo_p,94,50));
	ds_motivo_ati_inat_w	:= trim(substr(ds_conteudo_p,174,40));
	dt_inicio_obrig_w	:= to_date(substr(ds_conteudo_p,250,2)||substr(ds_conteudo_p,248,2)||substr(ds_conteudo_p,244,4), 'dd/mm/yyyy');
		
	select	to_number(decode(cd_material_ww,' ',0,cd_material_ww)),
		to_number(decode(nr_cnpj_ww,' ',0,nr_cnpj_ww)),
		to_number(decode(ds_origem_ww,' ',0,ds_origem_ww)),
		to_number(decode(nr_registro_anvisa_ww,' ',0,nr_registro_anvisa_ww)),
		to_number(decode(ie_tipo_situacao_ww,' ',0,ie_tipo_situacao_ww))
	into	cd_material_w,
		nr_cnpj_w,
		ds_origem_w,
		nr_registro_anvisa_w,
		ie_tipo_situacao_w
	from 	dual;	
		
	insert into w_ptu_material_unimed
		(nr_sequencia,cd_material, nr_seq_linha, ds_segundo_segmento, ie_tipo, dt_atualizacao, nm_usuario)
	values	(w_ptu_material_unimed_seq.nextval,cd_material_w, 0, ' ', ds_registro_w, sysdate, nm_usuario_p);	
	
	pls_importar_material_unimed(cd_material_w, ds_fracao_produto_w, ds_origem_w, null, 902, ie_tipo_situacao_w,
	nr_cnpj_w, nm_fabricante_w, nm_importador_w, nr_registro_anvisa_w, ds_motivo_ati_inat_w, null, null, 
	null, null, null, dt_inicio_obrig_w, null, null, null, null, null, null, null, null, null, null, null, nm_usuario_p);			
	 
elsif	(ds_registro_w = '905') then

	--apagar registros contidos na tabela temporaria
	delete  w_ptu_material_unimed;
	
	select	decode(to_number(substr(ds_conteudo_p,12,8)),0,'0',to_number(substr(ds_conteudo_p,12,8))),
		decode(to_number(substr(ds_conteudo_p,30,20)),0,'0',to_number(substr(ds_conteudo_p,30,20))),
		decode(to_number(substr(ds_conteudo_p,58,14)),0,'0',to_number(substr(ds_conteudo_p,58,14))),
		decode(to_number(substr(ds_conteudo_p,72,1)),0,'0',to_number(substr(ds_conteudo_p,72,1))),
		decode(to_number(substr(ds_conteudo_p,113,1)),0,'0',to_number(substr(ds_conteudo_p,113,1))),		
		decode(to_number(substr(ds_conteudo_p,114,5)),0,'0',to_number(substr(ds_conteudo_p,114,5)))
	into	cd_medicamento_w,		
		nr_registro_anvisa_w, 		
		nr_cnpj_w,			
		ie_tipo_situacao_w,
		ds_origem_w,			
		vl_fator_conversao_w
	from	dual;
	
	ds_fracao_produto_w		:= trim(substr(ds_conteudo_p,20,10));
	ds_motivo_ati_inat_w		:= trim(substr(ds_conteudo_p,73,40));
	ie_ind_generico_w		:= trim(substr(ds_conteudo_p,119,1));
	dt_inicio_obrig_w		:= to_date(substr(ds_conteudo_p,126,2)||substr(ds_conteudo_p,124,2)||substr(ds_conteudo_p,120,4),'dd/mm/yyyy');
	
	insert into w_ptu_material_unimed
		(nr_sequencia,cd_material, nr_seq_linha, ds_segundo_segmento, ie_tipo, dt_atualizacao, nm_usuario)
	values	(w_ptu_material_unimed_seq.nextval,cd_medicamento_w, 0, ' ',ds_registro_w , sysdate, nm_usuario_p);
	
	pls_importar_material_unimed(cd_medicamento_w, ds_fracao_produto_w, ds_origem_w, null, 905, 
	ie_tipo_situacao_w, nr_cnpj_w, null, null, nr_registro_anvisa_w, ds_motivo_ati_inat_w, 
	null, null, null, nm_apres_comercial_w, ie_ind_generico_w, dt_inicio_obrig_w, 
	null, null, null, null, null, null, null, null, null, null, null,nm_usuario_p);
	
elsif	((ds_registro_w <> '901') and (ds_registro_w <> '906') and (ds_registro_w <> '909') and (ds_registro_w <> 'FIM$')) or (ds_registro_w = 'seg') then
	
	ds_segundo_segmento_w	:= trim(substr(ds_conteudo_p,1,255));
			
	select	max(nr_sequencia)
	into	nr_seq_max_w
	from	w_ptu_material_unimed;		
	
	select	cd_material,		
		ie_tipo,
		nr_seq_linha
	into	cd_material_w,		
		ie_tipo_w,
		nr_seq_linha_w
	from	w_ptu_material_unimed
	where	nr_sequencia	=	nr_seq_max_w; 			
				
	nr_seq_linha_w := nr_seq_linha_w + 1;
	
	/*insere na tabala secundaria*/
	insert into w_ptu_material_unimed
		(nr_sequencia,cd_material, nr_seq_linha, ds_segundo_segmento, ie_tipo, dt_atualizacao, nm_usuario)
	values	(w_ptu_material_unimed_seq.nextval,cd_material_w, nr_seq_linha_w, ds_segundo_segmento_w, ie_tipo_w, sysdate, nm_usuario_p);
	
	/*verifica qual � o tipo do produto para lan�ar para os campos determinados da tabela quente.*/
	if	(ie_tipo_w = 902) then
		if	(nr_seq_linha_w = 1) then			
			update	pls_material_unimed 
			set	nm_material = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;
		elsif	(nr_seq_linha_w = 2) then
			update	pls_material_unimed 
			set	ds_material = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;
		elsif	(nr_seq_linha_w = 3) then
			update pls_material_unimed 
			set	ds_especialidade = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;
		elsif	(nr_seq_linha_w = 4) then
			update pls_material_unimed 
			set	ds_classe = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;		
		elsif	(nr_seq_linha_w = 5) then
			update pls_material_unimed 
			set	ds_apresentacao = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;
		end if;		
	elsif(ie_tipo_w = 905) then
		if	(nr_seq_linha_w = 1) then
			update pls_material_unimed 
			set	ds_principio_ativo = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;
		elsif	(nr_seq_linha_w = 2) then
			update pls_material_unimed 
			set	nm_material = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;
		elsif	(nr_seq_linha_w = 3) then
			update pls_material_unimed 
			set	ds_grupo_farmacologico = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;
		elsif	(nr_seq_linha_w = 4) then
			update pls_material_unimed 
			set	ds_classe_farmacologico = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;		
		elsif	(nr_seq_linha_w = 5) then
			update pls_material_unimed 
			set	ds_forma_farmaceutico = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;
		elsif	(nr_seq_linha_w = 6) then
			update pls_material_unimed 
			set	nm_fabricante = ds_segundo_segmento_w
			where	cd_material = cd_material_w
			and	ie_tipo = ie_tipo_w;
		end if;									
	end if;	
end if;

commit;
end ptu_importar_mat_unimed;
/