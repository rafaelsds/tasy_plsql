create or replace
procedure atualizar_hemocomponentes(
		ds_campo_p		varchar2,
		ds_valor_p		varchar2,
		ds_valores_restricao_p	varchar2) is

ds_sql_w		varchar2(2000);
ds_parametros_w	varchar2(2000);
ds_sep_bv_w	varchar2(10);


begin
ds_sep_bv_w	:= obter_separador_bv;

ds_sql_w	:=
	' update	san_producao'||
	' set	'||ds_campo_p||' = :ds_valor' ||
	' where	nr_sequencia     in '||ds_valores_restricao_p;

ds_parametros_w	:= 'ds_valor=' || ds_valor_p || ds_sep_bv_w;

exec_sql_dinamico_bv('', ds_sql_w, ds_parametros_w);

commit;

end atualizar_hemocomponentes;
/
