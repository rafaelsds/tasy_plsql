create or replace
procedure hc_agenda_hc_controle(
			nr_seq_agenda_p		number,
			ds_operacao_p		varchar2,
			nm_usuario_p		Varchar2) is 
			
/*
Valores para ds_operacao_p:
E - Exclus�o
I - Inclus�o
*/

begin

if	(ds_operacao_p is not null) then
	
	if	(ds_operacao_p = 'E') and
		(nm_usuario_p is not null) then
	
		delete 	
		from	agenda_hc_controle
		where	nm_usuario = nm_usuario_p
		and	(nr_seq_agenda = nr_seq_agenda_p
			or	nr_seq_agenda_p is null);
		
	elsif	(ds_operacao_p = 'I') and
		(nr_seq_agenda_p is not null) and
		(nr_seq_agenda_p > 0) then
	
		insert into agenda_hc_controle(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_agenda
		) values (
			agenda_hc_controle_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_agenda_p
		);
	
	end if;

end if;

commit;

end hc_agenda_hc_controle;
/