create or replace
procedure ck_desfazer_cancel_check_js (
		nr_sequencia_p	number,
		nm_usuario_p	varchar2
		) is

begin

if	(nr_sequencia_p is not null) then
	begin
	update	checkup	
	set	dt_cancelamento = null
		,nm_usuario = nm_usuario_p
		,nm_usuario_cancel = null
		,dt_confirmacao = null
		,nm_usuario_conf = null
		where nr_sequencia = nr_sequencia_p;
		
	commit;
	end;
end if;

end ck_desfazer_cancel_check_js;
/