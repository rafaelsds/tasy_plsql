create or replace procedure verifica_medic_dupl_atend(nr_seq_atendimento_p		  paciente_atend_medic.nr_seq_atendimento%type,
                                                      nr_sequencia_p		        paciente_atend_medic.nr_seq_material%type,
                                                      ds_erro_w		              out varchar2,
                                                      cd_material_p  		        paciente_atend_medic.cd_material%type) is
                                                      
  nr_seq_ficha_tecnica_w       material.nr_seq_ficha_tecnica%type;
begin
  if (cd_material_p is not null) then
    begin
      select material.nr_seq_ficha_tecnica
        into nr_seq_ficha_tecnica_w
        from material
       where material.cd_material = cd_material_p;
    exception
      when no_data_found then
        nr_seq_ficha_tecnica_w := null;
    end;
  end if;
  
  for i in (select material.nr_seq_ficha_tecnica, 
                   material.cd_material, 
                   paciente_atend_medic.nr_seq_material
              from paciente_atend_medic
             inner join material on (material.cd_material = paciente_atend_medic.cd_material)
             where paciente_atend_medic.nr_seq_mat_diluicao is null
               and nr_seq_atendimento = nr_seq_atendimento_p) LOOP
               
    if (nvl(i.nr_seq_ficha_tecnica,0) = nvl(nr_seq_ficha_tecnica_w,-1) and nvl(i.nr_seq_material,0) <> nvl(nr_sequencia_p,0)) then
      ds_erro_w := Wheb_mensagem_pck.get_texto(1175707, null);
      return;
    end if;
  end LOOP;
end verifica_medic_dupl_atend;
/
