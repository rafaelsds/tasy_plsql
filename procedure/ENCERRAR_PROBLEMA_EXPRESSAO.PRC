create or replace
procedure encerrar_problema_expressao(	cd_expressao_p		number,
					nm_usuario_p		Varchar2) is 

begin

update	dic_expressao_problema
set	dt_encerramento = sysdate,
	nm_usuario = nm_usuario_p
where	cd_expressao = cd_expressao_p;
	

commit;

end encerrar_problema_expressao;
/