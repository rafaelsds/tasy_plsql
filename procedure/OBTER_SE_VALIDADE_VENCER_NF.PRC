create or replace
procedure obter_se_validade_vencer_nf(	nr_sequencia_p				number,
					cd_estabelecimento_p			number,
					nm_usuario_p				varchar2,
					ds_lista_mat_p			out	varchar2) is

qt_dias_validade_w			varchar2(10);
ie_permite_calcular_w		varchar2(10);
nr_item_nf_w			number(5);
cd_material_w			number(6);
dt_validade_w			date;
ds_justificativa_w			varchar2(255);
ds_lista_mat_w			varchar2(255);
			
cursor c01 is			
select	cd_material,
	trunc(dt_validade,'dd'),
	ds_justificativa
from	nota_fiscal_item
where	nr_sequencia = nr_sequencia_p
and	dt_validade is not null
union all
select	a.cd_material,
	trunc(b.dt_validade,'dd'),
	a.ds_justificativa
from	nota_fiscal_item a,
	nota_fiscal_item_lote b
where	a.nr_Sequencia = b.nr_Seq_nota
and	a.nr_item_nf = b.nr_item_nf
and	a.nr_sequencia = nr_sequencia_p
and	b.dt_validade is not null;
			
begin

select	Obter_Valor_Param_Usuario(40, 230, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),
	Obter_Valor_Param_Usuario(40, 231, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)
into	qt_dias_validade_w,
	ie_permite_calcular_w
from	dual;

if	(qt_dias_validade_w is not null) then
	
	open C01;
	loop
	fetch C01 into	
		cd_material_w,
		dt_validade_w,
		ds_justificativa_w;
	exit when C01%notfound;
		begin
		if	(ie_permite_calcular_w = 'N') and
			(dt_validade_w <= (trunc(sysdate,'dd') + qt_dias_validade_w)) then
			ds_lista_mat_w	:= substr(ds_lista_mat_w || cd_material_w || ', ',1,255);
		
		elsif	(ie_permite_calcular_w = 'J') and
			(ds_justificativa_w is null) and
			(dt_validade_w <= (trunc(sysdate,'dd') + qt_dias_validade_w)) then
			ds_lista_mat_w	:= substr(ds_lista_mat_w || cd_material_w || ', ',1,255);
		end if;
		end;
	end loop;
	close C01;
	
end if;

ds_lista_mat_p			:= substr(ds_lista_mat_w,1,length(ds_lista_mat_w) -2);

end obter_se_validade_vencer_nf;
/