create or replace
procedure pls_obter_regra_honorario
			(	ie_tipo_guia_p		in	Varchar2,
				ie_conta_repasse_p	in	Varchar2,
				cd_medico_executor_p	in	Varchar2,
				nr_seq_tipo_atend_p	in	Number,
				cd_procedimento_p	in	Number,
				ie_origem_proced_p	in	Number,
				nm_usuario_p		in	Varchar2,
				cd_estabelecimento_p	in	Number,
				nr_seq_prestador_p	in	Number,
				nr_seq_prestador_atend_p in	Number,
				ie_regra_p		in	Varchar2,
				ie_tipo_conta_p		in	Varchar2,
				nr_seq_grau_partic_p	in	number,
				dt_referencia_p		in	date,
				vl_medico_ptu_p		in	Number,
				vl_co_ptu_p		in	Number,
				vl_material_ptu_p	in	Number,
				nr_seq_regra_p		out	Number,
				ie_calcula_valor_p	out	Varchar2,
				ie_repassa_medico_p	out	Varchar2,
				ie_medico_ptu_p		out	Varchar2,
				ie_co_ptu_p		out	Varchar2,
				ie_material_ptu_p	out	Varchar2,
				nr_seq_conta_proc_p	in	pls_conta_proc.nr_sequencia%type default null) is 
			
nr_seq_regra_w			Number(10);
ie_calcula_valor_w		Varchar2(1)	:= 'S';
ie_repassa_medico_w		Varchar2(1)	:= 'N';
ie_credenciado_w		Varchar2(1);
cd_grupo_w			Number(15);
cd_especialidade_w		Number(8);
cd_area_w			Number(8);
ie_estrutura_w			Varchar2(1);
cd_medico_w			Varchar2(10)	:= '0';
nr_seq_prestador_w		Number(10);
nr_seq_prest_cred_w		Number(10);
ie_prest_credenciado_w		Varchar2(2)	:= 'N';
ie_tipo_relacao_w		Varchar2(2);
cd_pessoa_fisica_w		Varchar2(20);
dt_referencia_w			date;
ie_medico_exec_cooperado_W	Varchar2(255);
nr_seq_tipo_prestador_w		Number(10);
ie_tipo_vinculo_w		Varchar2(1);
ie_cred_regra_w			Varchar2(1);
nr_seq_regra_aux_w		Number(10);
ie_ptu_vl_honorario_w		Varchar2(1);	
ie_ptu_vl_co_w			Varchar2(1);
ie_ptu_vl_filme_w		Varchar2(1);
qt_partic_w			pls_integer;

Cursor C01 is
	select	a.nr_sequencia nr_seq_regra,
		a.ie_calcula_valor,
		a.ie_repassa_medico,
		nvl(b.ie_estrutura,'S') ie_estrutura,
		b.ie_credenciado,
		b.nr_sequencia,
		b.ie_ptu_vl_honorario,
		b.ie_ptu_vl_co,
		b.ie_ptu_vl_filme,
		b.nr_seq_grau_partic
	from	pls_regra_honorario_crit	b,
		pls_regra_honorario		a
	where	a.nr_sequencia			= b.nr_seq_regra
	and	b.cd_estabelecimento 		= cd_estabelecimento_p
	and	a.ie_situacao			= 'A'
	and	((b.ie_tipo_guia 		is null) or ( b.ie_tipo_guia =  ie_tipo_guia_p))
	and	((b.ie_credenciado 		= 'T')   or (upper(b.ie_credenciado) 	= upper(ie_credenciado_w)) or (ie_conta_repasse_p = 'S'))
	and	((b.nr_seq_tipo_atendimento 	is null) or ( b.nr_seq_tipo_atendimento = nr_seq_tipo_atend_p))
	and	((cd_procedimento 		is null) or ((cd_procedimento 	= cd_procedimento_p) and (ie_origem_proced = ie_origem_proced_p)))
	and	((cd_grupo_proc 		is null) or (cd_grupo_proc 	=  cd_grupo_w))
	and	((cd_especialidade 		is null) or (cd_especialidade	= cd_especialidade_w))
	and	((cd_area_procedimento 		is null) or (cd_area_procedimento =  cd_area_w))
	and	((b.cd_medico 			is null) or ( b.cd_medico 	= cd_medico_w))
	and	((b.nr_seq_prestador 		is null) or ( b.nr_seq_prestador = nr_seq_prestador_w))
	and	((b.ie_tipo_prest_cred		= 'A')   or (b.ie_tipo_prest_cred = nvl(ie_prest_credenciado_w,b.ie_tipo_prest_cred)))
	and	((b.ie_regra 			is null) or (b.ie_regra		= 'A') or (b.ie_regra = ie_regra_p))
	and	((b.ie_tipo_relacao 		is null) or (b.ie_tipo_relacao 	= ie_tipo_relacao_w))
	and	((b.ie_tipo_conta		is null) or (b.ie_tipo_conta 	= ie_tipo_conta_p) or (b.ie_tipo_conta = 'T'))
	and	((b.ie_medico_exec_cooperado 	= 'N')   or (ie_medico_exec_cooperado_w = b.ie_medico_exec_cooperado))
	and	((b.nr_seq_tipo_prestador 	is null) or (b.nr_seq_tipo_prestador = nr_seq_tipo_prestador_w))
	and	((ie_ptu_vl_honorario 		is null) or (ie_ptu_vl_honorario = 'N') or ((ie_ptu_vl_honorario = 'S') and (vl_medico_ptu_p > 0)))
	and	((ie_ptu_vl_co 			is null) or (ie_ptu_vl_co 	= 'N') or ((ie_ptu_vl_co = 'S') and (vl_co_ptu_p > 0)))
	and	((ie_ptu_vl_filme 		is null) or (ie_ptu_vl_filme 	= 'N') or ((ie_ptu_vl_filme = 'S') and (vl_material_ptu_p > 0)))
	order by
		nvl(b.cd_procedimento,0),
		nvl(b.cd_grupo_proc,0),
		nvl(b.cd_especialidade,0),
		nvl(b.cd_area_procedimento,0),
		nvl(b.nr_seq_tipo_atendimento,0),
		b.ie_medico_exec_cooperado,
		b.ie_tipo_relacao desc,
		b.ie_tipo_prest_cred,
		b.ie_credenciado desc,
		nvl(b.ie_tipo_guia,'0'),
		nvl(b.ie_tipo_conta,'I'),
		nvl(b.nr_seq_tipo_prestador,0),
		nvl(b.nr_seq_prestador,0),
		nvl(b.cd_medico,'0'),
		b.ie_regra,
		b.ie_estrutura,
		b.ie_ptu_vl_honorario,
		b.ie_ptu_vl_co,
		b.ie_ptu_vl_filme,
		nvl(nr_seq_grau_partic,0);
		
begin
if	(dt_referencia_p is null) then
	dt_referencia_w	:= sysdate;
else
	dt_referencia_w	:= dt_referencia_p;
end if;

cd_medico_w		:= nvl(cd_medico_executor_p,'0');
nr_seq_prestador_w	:= nvl(nr_seq_prestador_p, 0);

/* Obter se o m�dico � credenciado na operadora */
ie_credenciado_w	:= pls_obter_se_cred_prestador(nr_seq_prestador_p, nr_seq_prestador_atend_p, dt_referencia_w, cd_medico_w);

ie_medico_exec_cooperado_w := pls_obter_se_cooperado_ativo(cd_medico_w,dt_referencia_w,null);

begin
select 	cd_pessoa_fisica,
	decode(ie_tipo_vinculo,'C','S','V','N'),
	ie_tipo_relacao,
	nr_seq_tipo_prestador
into	cd_pessoa_fisica_w,
	ie_prest_credenciado_w,
	ie_tipo_relacao_w,
	nr_seq_tipo_prestador_w
from	pls_prestador
where	nr_sequencia	= nr_seq_prestador_p;
exception
when others then
	ie_prest_credenciado_w	:= '';
	ie_tipo_relacao_w	:= '';
	nr_seq_tipo_prestador_w	:= '';
end;

if	(nvl(cd_pessoa_fisica_w,'0') = '0') then
	ie_prest_credenciado_w	:= null;
else

	if	(nvl(nr_seq_prestador_p,0) > 0) then
		select	max(a.ie_tipo_historico)
		into 	ie_tipo_vinculo_w
		from	pls_credenciamento_hist a	
		where	a.nr_seq_prestador = nr_seq_prestador_p
		and	a.cd_pessoa_fisica 	= cd_pessoa_fisica_w
		and	trunc(a.dt_historico,'dd') > trunc(dt_referencia_w,'dd')
		and	a.dt_historico = (	select max(x.dt_historico)
						from	pls_credenciamento_hist x	
						where	x.nr_seq_prestador = nr_seq_prestador_p
						and	x.cd_pessoa_fisica 	= cd_pessoa_fisica_w
						and	trunc(x.dt_historico,'dd') > trunc(dt_referencia_w,'dd'));
		if	(ie_tipo_vinculo_w = 'D') then
			ie_prest_credenciado_w	:= 'S';
		elsif	(ie_tipo_vinculo_w = 'C') then
			ie_prest_credenciado_w := 'N';
		end if;
	
	end if;
end if;

/* Obter Estrutura do procedimento */
begin
select	cd_grupo_proc,
	cd_especialidade,
	cd_area_procedimento
into	cd_grupo_w,
	cd_especialidade_w,
	cd_area_w
from	estrutura_procedimento_v
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p;
exception
     	when others then
	begin
	cd_grupo_w		:= 0;
	cd_especialidade_w	:= 0;
	cd_area_w		:= 0;
	end;
end;

for r_c01_w in C01() loop
	begin
	
	qt_partic_w := 1;
	if	(r_C01_w.nr_seq_grau_partic is not null) then
		select	count(1)
		into	qt_partic_w
		from	pls_proc_participante
		where	nr_seq_conta_proc	= nr_seq_conta_proc_p
		and	nr_seq_grau_partic	= r_C01_w.nr_seq_grau_partic;
	end if;
		
	if	(qt_partic_w > 0) then
		ie_ptu_vl_honorario_w	:= r_c01_w.ie_ptu_vl_honorario;
		ie_ptu_vl_co_w		:= r_c01_w.ie_ptu_vl_co;
		ie_ptu_vl_filme_w	:= r_c01_w.ie_ptu_vl_filme;
		nr_seq_regra_w		:= r_c01_w.nr_seq_regra;
		ie_calcula_valor_w	:= r_c01_w.ie_calcula_valor;
		ie_repassa_medico_w	:= r_c01_w.ie_repassa_medico;
		
		if	(r_c01_w.ie_estrutura	= 'N') then
			ie_ptu_vl_honorario_w	:= null;
			ie_ptu_vl_co_w		:= null;
			ie_ptu_vl_filme_w	:= null;
			nr_seq_regra_w		:= null;
			ie_calcula_valor_w	:= null;
			ie_repassa_medico_w	:= null;
		end if;
	end if;
	
	end;
end loop;

ie_medico_ptu_p		:= ie_ptu_vl_honorario_w;
ie_co_ptu_p		:= ie_ptu_vl_co_w;
ie_material_ptu_p	:= ie_ptu_vl_filme_w;
nr_seq_regra_p		:= nr_seq_regra_w;
ie_calcula_valor_p	:= ie_calcula_valor_w;
ie_repassa_medico_p	:= ie_repassa_medico_w;

end pls_obter_regra_honorario;
/