create or replace
procedure obter_se_excedeu_entrada_real	(
						nr_atendimento_p in number,
						nr_sequencia_p in number,
						ie_ultrapasso_limite_p out varchar2) is 

qt_consiste_entrada_real_w	number(10);			
cd_setor_atendimento_w		number(10);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);
qt_max_visitante_un		number(10);
						
begin
if (nvl(nr_atendimento_p,0) > 0) then

	select	max(cd_setor_atendimento),
		max(cd_unidade_basica),
		max(cd_unidade_compl)
	into	cd_setor_atendimento_w,
		cd_unidade_basica_w,
		cd_unidade_compl_w
	from	atend_paciente_unidade 
	where	nr_seq_interno = obter_atepacu_paciente(nr_atendimento_p,'A');

	select	max(qt_max_visitante)
	into	qt_max_visitante_un
	from	unidade_atendimento
	where	cd_setor_Atendimento = cd_setor_Atendimento_w
	and	cd_unidade_basica = cd_unidade_basica_w
	and	cd_unidade_compl = cd_unidade_compl_w;
	
	select	count(*)
	into	qt_consiste_entrada_real_w
	from	atendimento_visita
	where	nr_atendimento = nr_atendimento_p
	and	(nr_sequencia != nr_sequencia_p or nr_sequencia_p is null)
	and	dt_entrada_real is not null
	and	dt_saida is null;
	
	if ((qt_consiste_entrada_real_w + 1) > qt_max_visitante_un) then		
		ie_ultrapasso_limite_p := 'S';
	else
		ie_ultrapasso_limite_p := 'N';
	end if;
else
	ie_ultrapasso_limite_p := 'N';
end if;

end obter_se_excedeu_entrada_real;
/	