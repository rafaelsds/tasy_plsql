create or replace
procedure pls_liberar_hist_prestador
			(	nr_sequencia_p		Number,
				nm_usuario_p		Varchar2) is

begin

update	pls_prestador_historico set
	dt_liberacao	= sysdate,
	nm_usuario_liberacao = nm_usuario_p
where	nr_sequencia = nr_sequencia_p;
	
commit;

end pls_liberar_hist_prestador;
/