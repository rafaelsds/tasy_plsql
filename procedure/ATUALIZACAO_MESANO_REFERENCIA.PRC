create or replace
Procedure Atualizacao_MesAno_Referencia(	dt_parametro_p			date,
						nm_usuario_p			varchar2,
						cd_estabelecimento_p		number)	is



cd_convenio_w	number(5,0);

Cursor C01 is
	select	cd_convenio
	from	convenio
	where	ie_tipo_convenio <> 3; 
	/* OS 652363 Removido a verificação dos convênios Ativos conforme histórico do cliente do dia 23/10/2013 14:14:03. */

BEGIN

open C01;
loop
fetch C01 into	
	cd_convenio_w;
exit when C01%notfound;
	begin
	
	Atualiza_MesAno_Refer_Conta(Trunc(dt_parametro_p,'MONTH'), cd_convenio_w, nm_usuario_p,'S','S',cd_estabelecimento_p);
	
	end;
end loop;
close C01;

commit;

end Atualizacao_MesAno_Referencia;
/