create or replace
procedure mprev_alterar_status_agenda(	nr_seq_agendamento_p	number,
					dt_execucao_p		date,
					ie_status_p		varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					ie_gera_atendimento_p	varchar2,
					nr_seq_atend_mprev_p	out	number) is
					
nr_seq_atend_mprev_w		mprev_atendimento.nr_sequencia%type	:= null;
nr_seq_status_paciente_w	mprev_agendamento.nr_seq_status_pac%type;
qt_regra_atend_w		number(10);
nr_seq_partic_ciclo_item_w	mprev_agendamento.nr_seq_partic_ciclo_item%type;
ie_caracteriza_atend_w	varchar2(1);
	
Cursor C01 is
	select 	a.nr_seq_part_cic_item_ativ	
	from 	mprev_agendamento_ativ a
	where 	a.nr_seq_agendamento = nr_seq_agendamento_p
	and	a.ie_executado = 'S'
	order by a.nr_seq_part_cic_item_ativ;
	
begin	

if 	(ie_status_p is not null) and
	(nr_seq_agendamento_p is not null) then
	
	select	decode(nvl(nr_seq_participante,nvl(nr_seq_turma,0)),0,'N','S')
	into	ie_caracteriza_atend_w	
	from	mprev_agendamento
	where	nr_sequencia = nr_seq_agendamento_p;
	
	select	count(1)
	into	qt_regra_atend_w
	from	mprev_regra_ger_atend a
	where	a.cd_estabelecimento	= cd_estabelecimento_p;
	
	/* Gerar o atendimento */
	if	(qt_regra_atend_w > 0) and
		(nvl(ie_gera_atendimento_p,'S') <> 'N') and
		(ie_caracteriza_atend_w = 'S') then
		mprev_gerar_atend_agenda(nr_seq_agendamento_p,
					dt_execucao_p,
					nm_usuario_p,
					cd_estabelecimento_p,
					nr_seq_atend_mprev_w);
	end if;
				
	update	mprev_agendamento
	set	ie_status_agenda = ie_status_p,
		dt_atendido	= dt_execucao_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_seq_agendamento_p;
	
	nr_seq_status_paciente_w := mprev_obter_status_pac_ag('GA',dt_execucao_p);

	if	(nr_seq_status_paciente_w is not null)then
		update	mprev_agendamento
		set	nr_seq_status_pac	= nr_seq_status_paciente_w
		where	nr_sequencia		= nr_seq_agendamento_p
		and	ie_tipo_atendimento	<> 'C';
	end if;
	
	nr_seq_atend_mprev_p := nr_seq_atend_mprev_w;

	select 	nr_seq_partic_ciclo_item
	into	nr_seq_partic_ciclo_item_w
	from 	mprev_agendamento
	where 	nr_sequencia = nr_seq_agendamento_p;
	
	if (nr_seq_partic_ciclo_item_w is not null) then
		update	mprev_partic_ciclo_item
		set	ie_status	= 'R'
		where	nr_sequencia	= nr_seq_partic_ciclo_item_w; 
		
		/*Atualiza as atividades dos itens do ciclo de atendimento das atividades cadastradas no agendamento e marcado como executado.*/
		for r_C01 in C01 loop
			begin
				update	mprev_part_cic_item_ativ
				set	ie_executado = 'S',
					dt_executado_sistema = sysdate,
					dt_atualizacao_nrec = sysdate,
					nm_usuario_nrec = nm_usuario_p
				where	nr_sequencia = r_C01.nr_seq_part_cic_item_ativ;
			end;
		end loop;
		
	end if;
	
	commit;
end if;

end mprev_alterar_status_agenda;
/
