create or replace
procedure pls_copiar_contrato_benef
			(	cd_estab_origem_p	number,
				cd_estab_destino_p	number,
				nm_usuario_p		Varchar2) is 

qt_contratos_w			number(10);
qt_pagadores_w			number(10);
nr_seq_contrato_w		number(10);
nr_seq_contrato_novo_w		number(10);
nr_contrato_w			number(10);
nr_seq_proposta_ant_w		number(10);
nr_seq_proposta_nova_w		number(10);
nr_seq_emissor_ant_w		number(10);
nr_seq_emissor_novo_w		number(10);
nr_contrato_principal_ant_w	number(10);
nr_contrato_principal_novo_w	number(10);
nr_seq_motivo_rescisao_ant_w	number(10);
nr_seq_motivo_rescisao_novo_w	number(10);

nr_seq_contrato_plano_w		number(10);
nr_seq_contrato_plano_novo_w	number(10);
nr_seq_plano_ant_w		number(10);
nr_seq_tabela_ant_w		number(10);
nr_seq_tabela_origem_ant_w	number(10);
nr_seq_plano_novo_w		number(10);
nr_seq_tabela_novo_w		number(10);
nr_seq_tabela_origem_novo_w	number(10);

nr_seq_pagador_w		number(10);
nr_seq_pagador_novo_w		number(10);
nr_seq_mt_cancel_pag_ant_w	number(10);
nr_seq_mt_cancel_pag_novo_w	number(10);
nr_seq_pagador_info_fin_w	number(10);
nr_seq_vinculo_est_ant_w	number(10);
nr_seq_vinculo_est_novo_w	number(10);
nr_seq_pagador_historicos_w	number(10);
nr_seq_pagador_bonificacao_w	number(10);
nr_seq_pagador_rescisao_w	number(10);

nr_seq_subestipulante_w		number(10);

nr_seq_cobertura_w		number(10);
nr_seq_limitacoes_w		number(10);
nr_seq_carencia_w		number(10);
nr_seq_coparticipacao_w		number(10);
nr_seq_pos_estabelicido_w	number(10);
nr_seq_inscricao_w		number(10);
nr_seq_desconto_w		number(10);
nr_seq_desconto_novo_w		number(10);
nr_seq_desconto_criterios_w	number(10);
nr_seq_estipulante_web_w	number(10);
nr_seq_mensalidade_w		number(10);
nr_seq_bonificacao_w		number(10);
nr_seq_regras_sca_w		number(10);
nr_seq_inclusao_benef_w		number(10);
nr_seq_renovacao_w		number(10);
nr_seq_extensao_atendimento_w	number(10);

nr_seq_historico_w		number(10);
nr_seq_instrumento_juridico_w	number(10);
nr_seq_contato_w		number(10);
nr_seq_rescisao_contrato_w	number(10);
nr_seq_doc_anaexo_w		number(10);

nr_seq_mt_cancel_cont_ant_w	number(10);
nr_seq_mt_cancel_cont_novo_w	number(10);
nr_mt_cancel_pag_res_ant_w	number(10);
nr_mt_cancel_pag_res_novo_w	number(10);

nr_seq_tipo_carencia_ant_w	number(10);
nr_seq_tipo_carencia_novo_w	number(10);

/*Contrato - Contrato*/
Cursor C01 is
	select	nr_sequencia
	from	pls_contrato
	where	cd_estabelecimento	= cd_estab_origem_p;

/*Contrato - Plano*/	
Cursor C02 is
	select	nr_sequencia
	from	pls_contrato_plano
	where	nr_seq_contrato	= nr_seq_contrato_w;
	
/*Pagador - Pagador*/	
Cursor C03 is
	select	nr_sequencia	
	from	pls_contrato_pagador
	where	nr_seq_contrato	= nr_seq_contrato_w;

/*Pagador - Informacoes financeiras*/
Cursor C04 is
	select	nr_sequencia
	from	pls_contrato_pagador_fin
	where	nr_seq_pagador	= nr_seq_pagador_w;

/*Pagador - Historicos - Usurio/Sistema*/
Cursor C05 is
	select	nr_sequencia
	from	pls_pagador_historico
	where	nr_seq_pagador	= nr_seq_pagador_w;
/*Pagador - Bonificacao*/	
Cursor C06 is
	select	nr_sequencia
	from	pls_bonificacao_vinculo
	where	nr_seq_pagador	= nr_seq_pagador_w;

/*Pagador - Rescisao programada */ 	
Cursor C07 is
	select	nr_sequencia
	from	pls_rescisao_contrato
	where	nr_seq_pagador	= nr_seq_pagador_w;

/*Subestipulante - Subestipulante*/	
Cursor C08 is
	select	nr_sequencia
	from	pls_sub_estipulante
	where	nr_seq_contrato	= nr_seq_contrato_w;
	
/*Regras- Coberturas*/	
Cursor C09 is
	select	nr_sequencia
	from	pls_cobertura
	where	nr_seq_contrato	= nr_seq_contrato_w
	and 	ie_situacao = 'A'
	and 	sysdate between nvl(dt_inicio_vigencia, sysdate) and nvl(dt_fim_vigencia, sysdate);		
	
/* Regras - Limitacoes*/
Cursor C10 is
	select	nr_sequencia
	from	pls_limitacao
	where	nr_seq_contrato	= nr_seq_contrato_w;
	
/* Regras - Carencia*/
Cursor C11 is
	select	nr_sequencia
	from	pls_carencia
	where	nr_seq_contrato	= nr_seq_contrato_w;

/* Regras - Co - Participacao*/
Cursor C12 is
	select	nr_sequencia
	from	pls_regra_coparticipacao
	where	nr_seq_contrato	= nr_seq_contrato_w;

/* Regras - Ps - establecido*/
Cursor C13 is
	select	nr_sequencia
	from	pls_regra_pos_estabelecido
	where	nr_seq_contrato	= nr_seq_contrato_w;

/* Regras - Inscricao*/
Cursor C14 is
	select	nr_sequencia
	from	pls_regra_inscricao
	where	nr_seq_contrato	= nr_seq_contrato_w;
	
/* Regras - Desconto*/
Cursor C15 is
	select	nr_sequencia
	from	pls_regra_desconto
	where	nr_seq_contrato	= nr_seq_contrato_w;

/* Regras - Critrios descontos*/
Cursor C16 is
	select	nr_sequencia
	from	pls_preco_regra
	where	nr_seq_regra	= nr_seq_desconto_w;

/* Regras - Estipulante web*/
/*Cursor C17 is
	select	nr_sequencia
	from	pls_estipulante_web
	where	nr_seq_contrato	= nr_seq_contrato_w;
*/

/* Regras - Mensalidade*/
Cursor C18 is
	select	nr_sequencia
	from	pls_regra_mens_contrato
	where	nr_seq_contrato	= nr_seq_contrato_w;

/* Regras - Bonificacao*/
Cursor C19 is
	select	nr_sequencia
	from	pls_bonificacao_vinculo
	where	nr_seq_contrato	= nr_seq_contrato_w;
	
/* Regras - Programacao SCA*/
Cursor C20 is
	select	nr_sequencia
	from	pls_sca_regra_contrato
	where	nr_seq_contrato	= nr_seq_contrato_w;

/* Regras - Inclusao beneficiario*/
Cursor C21 is
	select	nr_sequencia
	from	pls_regra_inclusao_benef
	where	nr_seq_contrato	= nr_seq_contrato_w;

/* Regras - Renovacao de Carteiras*/
Cursor C22 is
	select	nr_sequencia
	from	pls_carteira_renovacao
	where	nr_seq_contrato	= nr_seq_contrato_w;

/* Regras - Extensao de atendimento*/
Cursor C23 is
	select	nr_sequencia
	from	pls_extensao_area
	where	nr_seq_contrato		= nr_seq_contrato_w;

/*Historico*/	
Cursor C24 is
	select	nr_sequencia
	from	pls_contrato_historico
	where	nr_seq_contrato	= nr_seq_contrato_w;

/*Instrumento juridico*/	
Cursor C25 is
	select	nr_sequencia
	from	pls_contrato_documento
	where	nr_seq_contrato		= nr_seq_contrato_w;

/*Contato*/	
Cursor C26 is
	select	nr_sequencia
	from	pls_contrato_contato
	where	nr_seq_contrato		= nr_seq_contrato_w;
/*Rescisao*/	
Cursor C27 is
	select	nr_sequencia
	from	pls_rescisao_contrato
	where	nr_seq_contrato		= nr_seq_contrato_w;
	
/*Documento anexo contrato*/	
Cursor C28 is
	select	nr_sequencia
	from	pls_contrato_doc_arq
	where	nr_seq_contrato		= nr_seq_contrato_w;
begin

open C01;
loop
fetch C01 into	
	nr_seq_contrato_w;
exit when C01%notfound;
	begin
	
	select	count(*)
	into	qt_contratos_w
	from	pls_contrato
	where	nr_seq_contrato_ant	= nr_seq_contrato_w;
	
	if	(qt_contratos_w	= 0) then
		select	pls_contrato_seq.NextVal
		into	nr_seq_contrato_novo_w
		from	dual;
		
		select	nr_contrato_seq.nextval
		into	nr_contrato_w
		from	dual;
		
		select	nr_seq_proposta,	
			nr_seq_emissor,
			nr_contrato_principal,
			nr_seq_motivo_rescisao
		into	nr_seq_proposta_ant_w,
			nr_seq_emissor_ant_w,
			nr_contrato_principal_ant_w,
			nr_seq_motivo_rescisao_ant_w
		from	pls_contrato
		where	nr_sequencia	= nr_seq_contrato_w;
		
		begin
		select	nr_sequencia
		into	nr_seq_proposta_nova_w
		from	pls_proposta_adesao
		where	nr_seq_proposta_ant	= nr_seq_proposta_ant_w;
		exception
		when others then
			nr_seq_proposta_nova_w := null;
		end;
		
		begin
		select	nr_sequencia
		into	nr_seq_emissor_novo_w
		from	pls_emissor_carteira
		where	nr_seq_emissor_ant	= nr_seq_emissor_ant_w;
		exception
		when others then
			nr_seq_emissor_novo_w	:= null;
		end;
		
		begin
		select	nr_sequencia
		into	nr_contrato_principal_novo_w
		from	pls_contrato
		where	nr_seq_contrato_ant	= nr_contrato_principal_ant_w;
		exception
		when others then
			nr_contrato_principal_novo_w	:= null;
		end;
		
		begin
		select	nr_sequencia
		into	nr_seq_motivo_rescisao_novo_w
		from	pls_motivo_cancelamento
		where	nr_seq_motivo_ant	= nr_seq_motivo_rescisao_ant_w;
		exception
		when others then
			nr_seq_motivo_rescisao_novo_w	:= null;
		end;
		
		insert into pls_contrato (nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, cd_cgc_estipulante, 
					cd_pf_estipulante, dt_aprovacao, qt_intervalo, ie_renovacao_automatica, ie_permite_prod_dif, 
					ie_situacao, dt_rescisao_contrato, dt_contrato, dt_reativacao, cd_contrato, ie_reajuste, 
					cd_estabelecimento, ie_geracao_valores, nr_seq_emissor, ie_tipo_beneficiario, nr_seq_congenere, qt_dias_valid_cart, 
					qt_idade_limite_reaj, qt_anos_limite_reaj, dt_cancelamento, nr_seq_motivo_rescisao, nr_seq_proposta, nr_contrato_principal, 
					ie_preco_co_operadora, ie_itens_nao_cobertos, ie_tipo_operacao, dt_limite_movimentacao, qt_interv_mes_mensalidade, 
					dt_ultima_mens_gerada, dt_limite_utilizacao, ie_controle_carteira, nr_contrato, nr_seq_assinatura, cd_operadora_empresa, 
					nr_seq_operadora, nr_seq_cooperativa, nr_seq_indice_reajuste,nr_seq_contrato_ant, ie_mes_cobranca_reaj)
				(select	nr_seq_contrato_novo_w, sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec, cd_cgc_estipulante, 
					cd_pf_estipulante, dt_aprovacao, qt_intervalo, ie_renovacao_automatica, ie_permite_prod_dif, 
					ie_situacao, dt_rescisao_contrato, dt_contrato, dt_reativacao, cd_contrato, ie_reajuste, 
					cd_estab_destino_p, ie_geracao_valores, nr_seq_emissor_novo_w, ie_tipo_beneficiario, nr_seq_congenere, qt_dias_valid_cart, 
					qt_idade_limite_reaj, qt_anos_limite_reaj, dt_cancelamento, nr_seq_motivo_rescisao_novo_w, nr_seq_proposta_nova_w, nr_contrato_principal_novo_w, 
					ie_preco_co_operadora, ie_itens_nao_cobertos, ie_tipo_operacao, dt_limite_movimentacao, qt_interv_mes_mensalidade, 
					dt_ultima_mens_gerada, dt_limite_utilizacao, ie_controle_carteira, nr_contrato_w, nr_seq_assinatura, cd_operadora_empresa, 
					nr_seq_operadora, nr_seq_cooperativa, nr_seq_indice_reajuste,nr_seq_contrato_w, ie_mes_cobranca_reaj
				from	pls_contrato
				where	nr_sequencia	= nr_seq_contrato_w);
		
		open C02;
		loop
		fetch C02 into	
			nr_seq_contrato_plano_w;
		exit when C02%notfound;
			begin
			
			select	nr_seq_plano,
				nr_seq_tabela,
				nr_seq_tabela_origem
			into	nr_seq_plano_ant_w,
				nr_seq_tabela_ant_w,
				nr_seq_tabela_origem_ant_w
			from	pls_contrato_plano
			where	nr_sequencia	= nr_seq_contrato_plano_w;
			
			begin
			select	nr_sequencia
			into	nr_seq_plano_novo_w
			from	pls_plano
			where	nr_seq_plano_ant	= nr_seq_plano_ant_w;
			exception
			when others then
				nr_seq_plano_novo_w	:= nr_seq_plano_ant_w;
			end;
			
			begin
			select	nr_sequencia
			into	nr_seq_tabela_novo_w
			from	pls_tabela_preco
			where	nr_seq_tabela_ant	= nr_seq_tabela_ant_w;
			exception
			when others then
				nr_seq_tabela_novo_w := nr_seq_tabela_ant_w;
			end;
			
			insert into pls_contrato_plano	(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_contrato, 
								nr_seq_plano, nr_seq_tabela, ie_situacao, dt_inativacao, nr_seq_tabela_origem, ie_tipo_operacao)
							(select	pls_contrato_plano_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
								nr_seq_plano_novo_w,nr_seq_tabela_novo_w,ie_situacao,dt_inativacao,nr_seq_tabela_origem,ie_tipo_operacao
						from	pls_contrato_plano
						where	nr_sequencia	= nr_seq_contrato_plano_w);
			end;
		end loop;
		close C02;
		
		open C03;
		loop
		fetch C03 into	
			nr_seq_pagador_w;
		exit when C03%notfound;
			begin
			
			select	count(*)
			into	qt_pagadores_w
			from	pls_contrato_pagador
			where	nr_seq_pagador_ant	= nr_seq_pagador_w;
			
			if	(qt_pagadores_w	= 0) then
				select	nr_seq_motivo_cancelamento
				into	nr_seq_mt_cancel_pag_ant_w
				from	pls_contrato_pagador
				where	nr_sequencia	= nr_seq_pagador_w;
				
				begin
				select	nr_sequencia
				into	nr_seq_mt_cancel_pag_novo_w
				from	pls_motivo_cancelamento
				where	nr_seq_motivo_ant	= nr_seq_mt_cancel_pag_ant_w;
				exception
				when others then
					nr_seq_mt_cancel_pag_novo_w	:= null;
				end;
				
				select	pls_contrato_pagador_seq.NextVal
				into	nr_seq_pagador_novo_w
				from	dual;
					
				insert into	pls_contrato_pagador	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
										cd_cgc,cd_pessoa_fisica,ie_tipo_pagador,ie_envia_cobranca,nr_seq_forma_cobranca,cd_banco,
										cd_agencia_bancaria,ie_digito_agencia,dt_dia_vencimento,ie_endereco_boleto,ie_calc_primeira_mens,cd_condicao_pagamento,
										dt_primeira_mensalidade,ie_calculo_proporcional,nr_seq_conta_banco,ie_pessoa_comprovante,dt_rescisao,nr_seq_motivo_cancelamento,
										ie_notificacao,dt_reativacao,ie_bonus_indicacao,ie_taxa_emissao_boleto,dt_suspensao,ie_inadimplencia_via_adic,
										cd_sistema_anterior,nr_seq_pagador_ant, nr_seq_compl_pf_tel_adic, nr_seq_tipo_compl_adic)
									(select	nr_seq_pagador_novo_w,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
										cd_cgc,cd_pessoa_fisica,ie_tipo_pagador,ie_envia_cobranca,nr_seq_forma_cobranca,cd_banco,
										cd_agencia_bancaria,ie_digito_agencia,dt_dia_vencimento,ie_endereco_boleto,ie_calc_primeira_mens,cd_condicao_pagamento,
										dt_primeira_mensalidade,ie_calculo_proporcional,nr_seq_conta_banco,ie_pessoa_comprovante,dt_rescisao,nr_seq_mt_cancel_pag_novo_w,
										ie_notificacao,dt_reativacao,ie_bonus_indicacao,ie_taxa_emissao_boleto,dt_suspensao,ie_inadimplencia_via_adic,
										cd_sistema_anterior,nr_seq_pagador_w, nr_seq_compl_pf_tel_adic, nr_seq_tipo_compl_adic
									from	pls_contrato_pagador
									where	nr_sequencia	= nr_seq_pagador_w);
									
				open C04;
				loop
				fetch C04 into	
					nr_seq_pagador_info_fin_w;
				exit when C04%notfound;
					begin
				
					select	nr_seq_vinculo_est
					into	nr_seq_vinculo_est_ant_w
					from	pls_contrato_pagador_fin
					where	nr_sequencia	= nr_seq_pagador_info_fin_w;
				
					begin
					select	nr_sequencia
					into	nr_seq_vinculo_est_novo_w
					from	pls_vinculo_estipulante
					where	nr_seq_vinculo_ant	= nr_seq_vinculo_est_ant_w;
					exception
					when others then
						nr_seq_vinculo_est_novo_w	:= null;
					end;
					
					insert into pls_contrato_pagador_fin	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_pagador,
											nr_seq_forma_cobranca,dt_inicio_vigencia,dt_fim_vigencia,cd_banco,nr_seq_conta_banco,cd_agencia_bancaria,
											ie_digito_conta,dt_dia_vencimento,nr_seq_empresa,cd_matricula,cd_condicao_pagamento,cd_profissao,
											nr_seq_vinculo_est,cd_tipo_portador,cd_portador,ie_portador_exclusivo,nr_seq_vinculo_empresa,ie_mes_vencimento,
											nr_seq_carteira_cobr, ie_gerar_cobr_escrit)
										(select	pls_contrato_pagador_fin_seq.NextVal,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_pagador_novo_w,
											nr_seq_forma_cobranca,dt_inicio_vigencia,dt_fim_vigencia,cd_banco,nr_seq_conta_banco,cd_agencia_bancaria,
											ie_digito_conta,dt_dia_vencimento,nr_seq_empresa,cd_matricula,cd_condicao_pagamento,cd_profissao,
											nr_seq_vinculo_est_novo_w,cd_tipo_portador,cd_portador,ie_portador_exclusivo,nr_seq_vinculo_empresa,ie_mes_vencimento,
											nr_seq_carteira_cobr, ie_gerar_cobr_escrit
										from	pls_contrato_pagador_fin
										where	nr_sequencia	= nr_seq_pagador_info_fin_w);
					end;	
				end loop;
				close C04;

				open C05;
				loop
				fetch C05 into	
					nr_seq_pagador_historicos_w;
				exit when C05%notfound;
					begin
					insert into pls_pagador_historico	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_pagador,
											cd_estabelecimento,ds_historico,dt_historico,ds_titulo,nm_usuario_historico,ie_tipo_historico)
										(select	pls_pagador_historico_seq.NextVal,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_pagador_novo_w,
											cd_estab_destino_p,ds_historico,dt_historico,ds_titulo,nm_usuario_historico,ie_tipo_historico
										from	pls_pagador_historico
										where	nr_sequencia	= nr_seq_pagador_historicos_w);
					
					end;
				end loop;
				close C05;	
				
				open C06;
				loop
				fetch C06 into	
					nr_seq_pagador_bonificacao_w;
				exit when C06%notfound;
					begin
					insert into pls_bonificacao_vinculo	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_pagador,
											nr_seq_bonificacao,tx_bonificacao,vl_bonificacao,dt_inicio_vigencia,dt_fim_vigencia)
										(select	pls_bonificacao_vinculo_seq.NextVal,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_pagador_novo_w,
											nr_seq_bonificacao,tx_bonificacao,vl_bonificacao,dt_inicio_vigencia,dt_fim_vigencia
										from	pls_bonificacao_vinculo
										where	nr_sequencia	= nr_seq_pagador_bonificacao_w);
					end;
				end loop;
				close C06;	
				
				open C07;
				loop
				fetch C07 into	
					nr_seq_pagador_rescisao_w;
				exit when C07%notfound;
					begin
					
					select	nr_seq_motivo_rescisao
					into	nr_mt_cancel_pag_res_ant_w
					from	pls_rescisao_contrato
					where	nr_sequencia	= nr_seq_pagador_rescisao_w;
						
					begin
					select	nr_sequencia
					into	nr_mt_cancel_pag_res_novo_w
					from	pls_motivo_cancelamento
					where	nr_seq_motivo_ant	= nr_mt_cancel_pag_res_ant_w;
					exception
					when others then
						nr_mt_cancel_pag_res_novo_w	:= null;
					end;
					
					insert into pls_rescisao_contrato	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_pagador,
											dt_rescisao,ds_observacao,ie_situacao,ie_tipo_solicitante,nm_usuario_solicitacao,dt_solicitacao,
											nr_seq_motivo_rescisao,ie_processo)
										(select	pls_rescisao_contrato_seq.NextVal,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_pagador_novo_w,
											dt_rescisao,ds_observacao,ie_situacao,ie_tipo_solicitante,nm_usuario_solicitacao,dt_solicitacao,
											nr_mt_cancel_pag_res_novo_w,ie_processo
										from	pls_rescisao_contrato
										where	nr_sequencia	= nr_seq_pagador_rescisao_w);
					end;
				end loop;
				close C07;
			end if;	
			end;
		end loop;
		close C03;
		
		open C08;
		loop
		fetch C08 into	
			nr_seq_subestipulante_w;
		exit when C08%notfound;
			begin
			insert into pls_sub_estipulante		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
									cd_pessoa_fisica,cd_cgc,dt_liberacao,dt_rescisao,cd_cod_anterior,nr_seq_motivo_cancelamento,
									dt_reativacao,dt_limite_utilizacao,nr_seq_subestipulante_ant)
								(select	pls_sub_estipulante_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
									cd_pessoa_fisica,cd_cgc,dt_liberacao,dt_rescisao,cd_cod_anterior,nr_seq_motivo_cancelamento,
									dt_reativacao,dt_limite_utilizacao,nr_seq_subestipulante_w
								from	pls_sub_estipulante
								where	nr_sequencia	 = nr_seq_subestipulante_w);
			end;
		end loop;
		close C08;
		
		/*Procedure criada para copiar a tabela pls_segrado e as tabelas referentes a elas na funo OPS - Gesto de contratos*/
		pls_copiar_segurado_estab(nr_seq_contrato_w,cd_estab_destino_p,nm_usuario_p);
		
		open C09;
		loop
		fetch C09 into
			nr_seq_cobertura_w;
		exit when C09%notfound;
			begin
			
			insert into pls_cobertura	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
								nr_seq_contrato,nr_seq_tipo_cobertura,ie_tipo_atendimento,ie_sexo,nr_seq_plano_contrato,
								ie_situacao, dt_inicio_vigencia, dt_fim_vigencia)
							(select	pls_cobertura_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,
								nr_seq_contrato_novo_w,nr_seq_tipo_cobertura,ie_tipo_atendimento,ie_sexo,nr_seq_plano_contrato,
								ie_situacao, dt_inicio_vigencia, dt_fim_vigencia
							from	pls_cobertura
							where	nr_sequencia	= nr_seq_cobertura_w);
			end;
		end loop;
		close C09;
		
		open C10;
		loop
		fetch C10 into
			nr_seq_limitacoes_w;
		exit when C10%notfound;
			begin
			
			insert into pls_limitacao	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
								nr_seq_tipo_limitacao,qt_permitida,qt_meses_intervalo,ie_periodo,qt_dia_internacao,nr_seq_plano_contrato,
								ie_tipo_periodo, ie_qt_familia)
							(select	pls_limitacao_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
								nr_seq_tipo_limitacao,qt_permitida,qt_meses_intervalo,ie_periodo,qt_dia_internacao,nr_seq_plano_contrato,
								ie_tipo_periodo, ie_qt_familia
							from	pls_limitacao
							where	nr_sequencia	= nr_seq_limitacoes_w);
			end;
		end loop;
		close C10;
		
		open C11;
		loop
		fetch C11 into
			nr_seq_carencia_w;
		exit when C11%notfound;
			begin
			
			select	nr_seq_tipo_carencia
			into	nr_seq_tipo_carencia_ant_w
			from	pls_carencia
			where	nr_sequencia	= nr_seq_carencia_w;
			
			begin
			select	nr_sequencia
			into	nr_seq_tipo_carencia_novo_w
			from	pls_tipo_carencia
			where	nr_seq_carencia_ant	= nr_seq_tipo_carencia_ant_w;
			exception
			when others then
				nr_seq_tipo_carencia_novo_w	:= nr_seq_tipo_carencia_ant_w;
			end;
			
			insert into pls_carencia	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
								nr_seq_tipo_carencia,qt_dias,ie_mes_posterior,dt_inicio_vigencia,ie_origem_carencia_benef,
								nr_seq_plano_contrato)
							(select	pls_carencia_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
								nr_seq_tipo_carencia_novo_w,qt_dias,ie_mes_posterior,dt_inicio_vigencia,'P',
								nr_seq_plano_contrato
							from	pls_carencia
							where	nr_sequencia	= nr_seq_carencia_w);
			end;
		end loop;
		close C11;
		
		open C12;
		loop
		fetch C12 into	
			nr_seq_coparticipacao_w;
		exit when C12%notfound;
			begin
			
			insert into pls_regra_coparticipacao	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
									tx_coparticipacao,nr_seq_tipo_coparticipacao,vl_maximo,ie_tipo_atendimento,ie_situacao,vl_coparticipacao,
									dt_inicio_vigencia,dt_fim_vigencia,dt_contrato_de,dt_contrato_ate, ie_reajuste, ie_inc_demais_itens,
									vl_base_min, vl_base_max, ie_incidencia_psiquiatria)
								(select	pls_regra_coparticipacao_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
									tx_coparticipacao,nr_seq_tipo_coparticipacao,vl_maximo,ie_tipo_atendimento,ie_situacao,vl_coparticipacao,
									dt_inicio_vigencia,dt_fim_vigencia,dt_contrato_de,dt_contrato_ate, ie_reajuste,ie_inc_demais_itens,
									vl_base_min, vl_base_max, ie_incidencia_psiquiatria
								from	pls_regra_coparticipacao 
								where	nr_sequencia	= nr_seq_coparticipacao_w);
			end;
		end loop;
		close C12;
		
		open C13;
		loop
		fetch C13 into	
			nr_seq_pos_estabelicido_w;
		exit when C13%notfound;
			begin
			
			insert into pls_regra_pos_estabelecido	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
									tx_administracao,vl_informado,dt_vigencia_inicio,dt_vigencia_fim,ie_cobranca,ie_autorizacao_previa)
								(select	pls_regra_pos_estabelecido_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
									tx_administracao,vl_informado,dt_vigencia_inicio,dt_vigencia_fim,ie_cobranca,ie_autorizacao_previa
								from	pls_regra_pos_estabelecido
								where	nr_sequencia	= nr_seq_pos_estabelicido_w);
			end;
		end loop;
		close C13;
		
		open C14;
		loop
		fetch C14 into	
			nr_seq_inscricao_w;
		exit when C14%notfound;
			begin
			
			insert into pls_regra_inscricao		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
									vl_inscricao,tx_inscricao,qt_parcela_inicial,qt_parcela_final,dt_inicio_vigencia,dt_fim_vigencia,
									nr_seq_subestipulante,ie_acao_contrato, ie_grau_dependencia)
								(select	pls_regra_inscricao_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
									vl_inscricao,tx_inscricao,qt_parcela_inicial,qt_parcela_final,dt_inicio_vigencia,dt_fim_vigencia,
									nr_seq_subestipulante,ie_acao_contrato, ie_grau_dependencia
								from	pls_regra_inscricao
								where	nr_sequencia	= nr_seq_inscricao_w);
			end;
		end loop;
		close C14;
		
		open C15;
		loop
		fetch C15 into	
			nr_seq_desconto_w;
		exit when C15%notfound;
			begin
			
			select	pls_regra_desconto_seq.nextval
			into	nr_seq_desconto_novo_w
			from	dual;
			
			insert into pls_regra_desconto	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,cd_estabelecimento,
								nr_seq_contrato,ds_regra,ie_situacao,dt_inicio_vigencia,dt_fim_vigencia,ie_tipo_regra,nr_seq_desconto_ant)
							(select	nr_seq_desconto_novo_w,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,cd_estab_destino_p,
								nr_seq_contrato_novo_w,ds_regra,ie_situacao,dt_inicio_vigencia,dt_fim_vigencia,ie_tipo_regra,nr_seq_desconto_w
							from	pls_regra_desconto
							where	nr_sequencia	= nr_seq_desconto_w);
			
			open C16;
			loop
			fetch C16 into	
				nr_seq_desconto_criterios_w;
			exit when C16%notfound;
				begin
				
				insert into pls_preco_regra	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_regra,
									qt_min_vidas,qt_max_vidas,tx_desconto,ie_tipo_segurado, vl_desconto)
								(select	pls_preco_regra_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_desconto_novo_w,
									qt_min_vidas,qt_max_vidas,tx_desconto,ie_tipo_segurado, vl_desconto
								from	pls_preco_regra
								where	nr_sequencia	= nr_seq_desconto_criterios_w);
				end;
			end loop;
			close C16;
			end;
		end loop;
		close C15;
		
		/*
		open C17;
		loop
		fetch C17 into	
			nr_seq_estipulante_web_w;
		exit when C17%notfound;
			begin
			
			insert into pls_estipulante_web		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
									nm_usuario_web,ds_senha,ds_email,ds_observacao,cd_pessoa_fisica,nr_seq_subestipulante,
									ie_situacao)
								(select	pls_estipulante_web_seq.nextval,dt_atualizacao, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
									nm_usuario_web,ds_senha,ds_email,ds_observacao,cd_pessoa_fisica,nr_seq_subestipulante,
									ie_situacao
								from	pls_estipulante_web
								where	nr_sequencia	= nr_seq_estipulante_web_w);
			end;
		end loop;
		close C17;*/

		open C18;
		loop
		fetch C18 into	
			nr_seq_mensalidade_w;
		exit when C18%notfound;
			begin
			
			insert into pls_regra_mens_contrato	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,cd_estabelecimento,
									nr_seq_contrato,dt_inicio_vigencia,dt_fim_vigencia,dt_limite_movimentacao,
									ie_tipo_data_limite,qt_interv_mes_mensalidade,ie_mensalidade_mes_rescisao,vl_minimo,nr_seq_intercambio,
									ie_pessoa_contrato,vl_minimo_abat_fatura,pr_aplicacao_reajuste,nr_seq_regra_origem,ie_tipo_regra)
								(select	pls_regra_mens_contrato_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,cd_estab_destino_p,
									nr_seq_contrato_novo_w,dt_inicio_vigencia,dt_fim_vigencia,dt_limite_movimentacao,
									ie_tipo_data_limite,qt_interv_mes_mensalidade,ie_mensalidade_mes_rescisao,vl_minimo,nr_seq_intercambio,
									ie_pessoa_contrato,vl_minimo_abat_fatura,pr_aplicacao_reajuste,nr_seq_regra_origem,ie_tipo_regra
								from	pls_regra_mens_contrato
								where	nr_sequencia	= nr_seq_mensalidade_w);
			end;
		end loop;
		close C18;
		
		open C19;
		loop
		fetch C19 into	
			nr_seq_bonificacao_w;
		exit when C19%notfound;
			begin
			
			insert into pls_bonificacao_vinculo	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
									tx_bonificacao,vl_bonificacao,dt_inicio_vigencia,dt_fim_vigencia,nr_seq_bonificacao)
								(select	pls_bonificacao_vinculo_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
									tx_bonificacao,vl_bonificacao,dt_inicio_vigencia,dt_fim_vigencia,nr_seq_bonificacao
								from	pls_bonificacao_vinculo
								where	nr_sequencia	= nr_seq_bonificacao_w);
			end;
		end loop;
		close C19;
		
		open C20;
		loop
		fetch C20 into	
			nr_seq_regras_sca_w;
		exit when C20%notfound;
			begin
			
			insert into pls_sca_regra_contrato	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,cd_estabelecimento,
									nr_seq_contrato,nr_seq_plano,nr_seq_tabela,dt_inicio_vigencia,dt_fim_vigencia)
								(select	pls_sca_regra_contrato_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,cd_estab_destino_p,
									nr_seq_contrato_novo_w,nr_seq_plano,nr_seq_tabela,dt_inicio_vigencia,dt_fim_vigencia
								from	pls_sca_regra_contrato
								where	nr_sequencia	= nr_seq_regras_sca_w);
			end;
		end loop;
		close C20;
		
		open C21;
		loop
		fetch C21 into	
			nr_seq_inclusao_benef_w;
		exit when C21%notfound;
			begin
			
			insert into pls_regra_inclusao_benef	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,cd_estabelecimento,
									nr_seq_contrato,dt_limite_movimentacao,dt_inicio_vigencia,dt_fim_vigencia,dt_dia_inclusao,ie_tipo_data,
									ie_acao_contrato)
								(select	pls_regra_inclusao_benef_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,cd_estab_destino_p,
									nr_seq_contrato_novo_w,dt_limite_movimentacao,dt_inicio_vigencia,dt_fim_vigencia,dt_dia_inclusao,ie_tipo_data,
									ie_acao_contrato
								from	pls_regra_inclusao_benef
								where	nr_sequencia	= nr_seq_inclusao_benef_w);
			end;
		end loop;
		close C21;
		
		open C22;
		loop
		fetch C22 into	
			nr_seq_renovacao_w;
		exit when C22%notfound;
			begin
			
			insert into pls_carteira_renovacao	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,cd_estabelecimento,
									nr_seq_contrato,qt_meses_renovacao,ie_tipo_renovacao)
								(select	pls_carteira_renovacao_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,cd_estab_destino_p,
									nr_seq_contrato_novo_w,qt_meses_renovacao,ie_tipo_renovacao
								from	pls_carteira_renovacao
								where	nr_sequencia	= nr_seq_renovacao_w);
			end;
		end loop;
		close C22;
		
		open C23;
		loop
		fetch C23 into	
			nr_seq_extensao_atendimento_w;
		exit when C23%notfound;
			begin
			
			insert into pls_extensao_area		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,cd_estabelecimento,
									nr_seq_contrato,cd_municipio_ibge,sg_estado,nr_seq_regiao,dt_inicio_vigencia,dt_fim_vigencia)
								(select	pls_extensao_area_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,cd_estab_destino_p,
									nr_seq_contrato_novo_w,cd_municipio_ibge,sg_estado,nr_seq_regiao,dt_inicio_vigencia,dt_fim_vigencia
								from	pls_extensao_area
								where	nr_sequencia	= nr_seq_extensao_atendimento_w);
			end;
		end loop;
		close C23;
		
		
		open C24;
		loop
		fetch C24 into	
			nr_seq_historico_w;
		exit when C24%notfound;
			begin
			
			insert into pls_contrato_historico	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,cd_estabelecimento,
									nr_seq_contrato,dt_historico,ie_tipo_historico,ds_historico,ds_titulo,nr_seq_tipo_historico)
								(select	pls_contrato_historico_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,cd_estab_destino_p,
									nr_seq_contrato_novo_w,dt_historico,ie_tipo_historico,ds_historico,ds_titulo,nr_seq_tipo_historico
								from	pls_contrato_historico
								where	nr_sequencia	= nr_seq_historico_w);
			end;
		end loop;
		close C24;
		
		open C25;
		loop
		fetch C25 into	
			nr_seq_instrumento_juridico_w;
		exit when C25%notfound;
			begin
			
			insert into pls_contrato_documento	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
									nr_seq_instrumento,ds_documento)
								(select	pls_contrato_documento_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
									nr_seq_instrumento,ds_documento
								from	pls_contrato_documento
								where	nr_sequencia	= nr_seq_instrumento_juridico_w);
			end;
		end loop;
		close C25;
		
		open C26;
		loop
		fetch C26 into	
			nr_seq_contato_w;
		exit when C26%notfound;
			begin
			
			insert into pls_contrato_contato	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
									cd_pessoa_fisica,nr_telefone,ds_email,cd_tipo_contato)
								(select	pls_contrato_contato_seq.nextval,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
									cd_pessoa_fisica,nr_telefone,ds_email,cd_tipo_contato
								from	pls_contrato_contato
								where	nr_sequencia	= nr_seq_contato_w);
			end;
		end loop;
		close C26;
		
		open C27;
		loop
		fetch C27 into	
			nr_seq_rescisao_contrato_w;
		exit when C27%notfound;
			begin
			
			select	nr_seq_motivo_rescisao
			into	nr_seq_mt_cancel_cont_ant_w
			from	pls_rescisao_contrato
			where	nr_sequencia	= nr_seq_rescisao_contrato_w;
				
			begin
			select	nr_sequencia
			into	nr_seq_mt_cancel_cont_novo_w
			from	pls_motivo_cancelamento
			where	nr_seq_motivo_ant	= nr_seq_mt_cancel_cont_ant_w;
			exception
			when others then
				nr_seq_mt_cancel_cont_novo_w	:= null;
			end;
			
				insert into pls_rescisao_contrato	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
										dt_rescisao,ds_observacao,ie_situacao,ie_tipo_solicitante,nm_usuario_solicitacao,dt_solicitacao,
										nr_seq_motivo_rescisao,ie_processo)
									(select	pls_rescisao_contrato_seq.NextVal,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
										dt_rescisao,ds_observacao,ie_situacao,ie_tipo_solicitante,nm_usuario_solicitacao,dt_solicitacao,
										nr_seq_mt_cancel_cont_novo_w,ie_processo
									from	pls_rescisao_contrato
									where	nr_sequencia	= nr_seq_rescisao_contrato_w);
			end;
		end loop;
		close C27;
		
		open C28;
		loop
		fetch C28 into	
			nr_seq_doc_anaexo_w;
		exit when C28%notfound;
			begin
			
			insert into  pls_contrato_doc_arq	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,nr_seq_contrato,
									nr_seq_documento,ds_arquivo)
								(select	pls_contrato_doc_arq_seq.NextVal,sysdate, nm_usuario_p, dt_atualizacao_nrec, nm_usuario_nrec,nr_seq_contrato_novo_w,
									nr_seq_documento,ds_arquivo
								from	pls_contrato_doc_arq
								where	nr_sequencia	= nr_seq_doc_anaexo_w);
			end;
		end loop;
		close C28;
	end if;	
	end;
end loop;
close C01;

commit;

end pls_copiar_contrato_benef;
/