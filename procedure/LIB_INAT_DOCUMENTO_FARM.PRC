create or replace
procedure lib_inat_documento_farm(nr_sequencia_p	number,
				  ie_acao_p		varchar,
				  nm_usuario_p		varchar,
				  cd_estabelecimento_p	number) 
				  is

begin

if	(nvl(nr_sequencia_p,0) > 0) then
	if	(ie_acao_p = 'L') then
		update	documento_farmacia
		set	dt_liberacao 		= sysdate,
			nm_usuario 		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia 		= nr_sequencia_p;
	elsif	(ie_acao_p = 'I') then
		update	documento_farmacia
		set	dt_inativacao 		= sysdate,
			nm_usuario_inativacao 	= nm_usuario_p,
			ie_situacao		= 'I',
			dt_atualizacao		= sysdate
		where	nr_sequencia 		= nr_sequencia_p;
	end if;	
	commit;
end if;	

end lib_inat_documento_farm;
/