create or replace
procedure Inserir_mat_W_Item_prescr(
			cd_pessoa_fisica_p	varchar2,
			nr_atendimento_p	number,
			nr_seq_item_p	Number,
			nm_usuario_p	varchar2,
			ie_origem_p     number)  is 
 
/* 
Utilizado no AtePac_P3

Ie_origem_p
1- Procedimento N�o-Laboratorial
2- Procedimento Laboratorial
*/

cd_procedimento_w	number(10);
ie_origem_proced_w	number(05);
cd_procedimento_ww	number(10) := null;
ie_origem_proced_ww	number(05) := null;
nr_proc_interno_w	number(10) := null;
cd_pessoa_fisica_w	number(10);
qt_idade_pac_w		number(20,8);
qt_idade_pac_ww		number(05,2);
cd_setor_atendimento_w  number(6);
cd_material_w		number(6);
cd_unidade_medida_w	varchar2(30);
ie_mostrar_item_w	varchar2(30);
qt_dose_w		number(18,6);
nr_sequencia_w		number(10);
cont_w			number(10);
ie_duplicar_w		varchar2(30);
nr_seq_item_w		number(10);
qt_inseridos_w		number(5) := 0;
cd_medico_ww		varchar2(10);
	
cursor C01 is
select	a.nr_sequencia,
	a.cd_material,
	a.cd_unidade_medida,
	a.qt_dose,
	'S'
from	procedimento_mat_prescr a
where	a.cd_procedimento	= cd_procedimento_w
and	a.ie_origem_proced	= ie_origem_proced_w
and 	not exists (	
	Select	1
	from	W_item_prescr k
	where	k.nr_prescricao is null
	and	k.ie_origem_inf in ('PI', 'P')
	and 	nm_usuario	= nm_usuario_p
	and	a.cd_material	= k.cd_material
	and 	k.cd_protocolo in (cd_procedimento_w, cd_procedimento_ww, nr_proc_interno_w))
and	((cd_setor_atendimento_w = a.cd_setor_atendimento) or
	 (a.cd_setor_atendimento is null))
and	(((qt_idade_pac_ww is null) or
	  (a.qt_idade_min is null and a.qt_idade_max is null)) or
	 ((qt_idade_pac_ww is not null) and
	  (qt_idade_pac_ww between nvl(a.qt_idade_min,qt_idade_pac_w) and nvl(a.qt_idade_max,qt_idade_pac_w))))
order by nvl(a.cd_setor_atendimento,999);

cursor C02 is
select	a.nr_sequencia,
	a.cd_material,
	a.cd_unidade_medida,
	a.qt_dose,
	nvl(a.ie_mostrar_item,'S'),
	nvl(a.ie_duplicar,'S')
from	proc_int_mat_prescr a
where	a.nr_seq_proc_interno	= nr_proc_interno_w
and	Obter_se_mat_setor(a.nr_sequencia, cd_setor_atendimento_w, obter_convenio_atendimento(nr_atendimento_p), obter_tipo_atendimento(nr_atendimento_p)) = 'S'
and	nvl(a.ie_situacao,'A')	= 'A'
and	((a.cd_medico is null) or (a.cd_medico = cd_medico_ww))
and	((a.cd_convenio_exc is null) or (a.cd_convenio_exc <> obter_convenio_atendimento(nr_atendimento_p)))
and 	not exists (	
	Select	1
	from	W_item_prescr k
	where	k.nr_prescricao is null
	and	k.ie_origem_inf in ('PI', 'P')
	and	k.cd_material 	= a.cd_material	
	and 	nm_usuario	= nm_usuario_p
	and 	k.cd_protocolo in (cd_procedimento_w, cd_procedimento_ww, nr_proc_interno_w))
and	((cd_setor_atendimento_w = a.cd_setor_atendimento) or
	 (a.cd_setor_atendimento is null))
and		((a.cd_estabelecimento is null) or
		 (a.cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento))
and	nvl(qt_idade_pac_w,1) between nvl(obter_idade_mat_int_prescr(a.nr_sequencia,'MIN'),0) and nvl(obter_idade_mat_int_prescr(a.nr_sequencia,'MAX'),9999999)
and	nvl(obter_sinal_vital(nr_atendimento_p,'PESO'),0) between nvl(a.qt_peso_min,0) and nvl(a.qt_peso_max,999)
order by nvl(a.cd_setor_atendimento,999);

begin

select	Obter_Pf_Usuario(nm_usuario_p,'C')
into		cd_medico_ww
from		dual;

if	(ie_origem_p = 1) then
	select  cd_procedimento,
		ie_origem_proced,
		nr_proc_interno
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_proc_interno_w
	from 	procedimento_rotina 
	where 	nr_sequencia = nr_seq_item_p;
else 
	select  b.cd_procedimento,
		b.ie_origem_proced,
		null nr_proc_interno
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_proc_interno_w
	from 	exame_lab_rotina a,
		exame_laboratorio b
	where 	a.nr_sequencia = nr_seq_item_p
	and 	a.nr_seq_exame = b.nr_seq_exame;
end if;

If	(nr_proc_interno_w is not null) then
	select  a.cd_procedimento,
			a.ie_origem_proced
	into 	cd_procedimento_ww,
			ie_origem_proced_ww
	from 	proc_interno a
	where 	a.nr_sequencia = nr_proc_interno_w;
end if;	

cd_pessoa_fisica_w	:= cd_pessoa_fisica_p;
cd_setor_atendimento_w	:= nvl(obter_unidade_atendimento(NR_ATENDIMENTO_P,'IA','CS'), obter_unidade_atendimento(NR_ATENDIMENTO_P,'A','CS')) ;

qt_idade_pac_ww		:= obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'A');

begin
select	max(obter_idade(b.dt_nascimento,nvl(b.dt_obito,sysdate),'DIA'))
into	qt_idade_pac_w
from	pessoa_fisica b
where	b.cd_pessoa_fisica	= cd_pessoa_fisica_w;
exception
when others then
	qt_idade_pac_w	:= 0;
end;

delete  from w_item_prescr
where	nr_prescricao is null
and	nm_usuario = nm_usuario_p
and 	cd_protocolo in (cd_procedimento_w, cd_procedimento_ww, nr_proc_interno_w)
and 	nr_seq_item in (select nr_sequencia 
			from	procedimento_mat_prescr a
			where	a.cd_procedimento	in (cd_procedimento_w, cd_procedimento_ww)
			and	a.ie_origem_proced	in (ie_origem_proced_w, ie_origem_proced_ww)
			and	((cd_setor_atendimento_w = a.cd_setor_atendimento) or
				 (a.cd_setor_atendimento is null))
			and	(((qt_idade_pac_ww is null) or
				  (a.qt_idade_min is null and a.qt_idade_max is null)) or
				 ((qt_idade_pac_ww is not null) and
				  (qt_idade_pac_ww between nvl(a.qt_idade_min,qt_idade_pac_w) and nvl(a.qt_idade_max,qt_idade_pac_w))))
			union all
			select 	a.nr_sequencia
			from	proc_int_mat_prescr a
			where	a.nr_seq_proc_interno	= nr_proc_interno_w
			and	Obter_se_mat_setor(a.nr_sequencia, cd_setor_atendimento_w, null, obter_tipo_atendimento(nr_atendimento_p)) = 'S'
			and	((cd_setor_atendimento_w = a.cd_setor_atendimento) or
				 (a.cd_setor_atendimento is null))
			and	nvl(qt_idade_pac_w,1) between nvl(obter_idade_mat_int_prescr(a.nr_sequencia,'MIN'),0) and nvl(obter_idade_mat_int_prescr(a.nr_sequencia,'MAX'),9999999)
			and	nvl(obter_sinal_vital(nr_atendimento_p,'PESO'),0) between nvl(a.qt_peso_min,0) and nvl(a.qt_peso_max,999))
and 	ie_origem_inf in ('P', 'PI');
commit;

qt_inseridos_w	:= 0;

open C02;
loop
fetch C02 into	
	nr_seq_item_w,
	cd_material_w,
	cd_unidade_medida_w,
	qt_dose_w,
	ie_mostrar_item_w,
	ie_duplicar_w;
exit when C02%notfound;
	begin	
	
	if	(ie_duplicar_w = 'N') then
		select	count(*)
		into	cont_w
		from	W_Item_Prescr
		where	cd_material	= cd_material_w
		and	nr_prescricao	is null
		and	nm_usuario = nm_usuario_p;
		
		if	(cont_w = 0) then
			ie_duplicar_w	:= 'S';
		end if;
	end if;
	
	select 	W_Item_Prescr_seq.nextval
	into	nr_sequencia_w
	from	dual;
	
	insert into W_Item_Prescr(
		nr_sequencia,
		dt_atualizacao,         
		nm_usuario,             
		nr_prescricao,          
		cd_protocolo,            
		nr_seq_item,            
		qt_dose,
		cd_material,
		ie_origem_inf,
		nr_seq_solucao,
		cd_unidade_medida,
		ie_mostrar_item,
		ie_excluido)
	Values(
		nr_sequencia_w,
		sysdate,
		nm_usuario_p,
		null,
		nr_proc_interno_w,
		nr_seq_item_w,
		qt_dose_w,
		cd_material_w,
		'PI', 
		nr_seq_item_p,
		cd_unidade_medida_w,
		ie_mostrar_item_w,
		decode(ie_duplicar_w,'S','N','S'));
		
		qt_inseridos_w	:= qt_inseridos_w + 1;
	end;
end loop;
close C02;

commit;

if	(qt_inseridos_w = 0 ) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_item_w,
		cd_material_w,
		cd_unidade_medida_w,
		qt_dose_w,
		ie_duplicar_w;
	exit when C01%notfound;
		begin	

		if	(ie_duplicar_w = 'N') then
			select	count(*)
			into	cont_w
			from	W_Item_Prescr
			where	cd_material	= cd_material_w
			and	nr_prescricao	is null
			and	nm_usuario	= nm_usuario_p;
			
			if	(cont_w = 0) then
				ie_duplicar_w	:= 'S';
			end if;
		end if;	

		select 	W_Item_Prescr_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		insert into W_Item_Prescr(
			nr_sequencia,
			dt_atualizacao,         
			nm_usuario,             
			nr_prescricao,          
			cd_protocolo,            
			nr_seq_item,            
			qt_dose,
			cd_material,
			ie_origem_inf,
			nr_seq_solucao,
			cd_unidade_medida,
			ie_excluido)
		Values(
			nr_sequencia_w,
			sysdate,
			nm_usuario_p,
			null,
			cd_procedimento_w,
			nr_seq_item_w,
			qt_dose_w,
			cd_material_w,
			'P',
			nr_seq_item_p,
			cd_unidade_medida_w,
			decode(ie_duplicar_w,'S','N','S'));
		end;
	end loop;
	close C01;

	commit;
	if	(cd_procedimento_ww is not null) then
		cd_procedimento_w	:= cd_procedimento_ww;
		ie_origem_proced_w	:= ie_origem_proced_ww;
		open C01;
		loop
		fetch C01 into	
			nr_seq_item_w,
			cd_material_w,
			cd_unidade_medida_w,
			qt_dose_w,
			ie_duplicar_w;
		exit when C01%notfound;
			begin
			if	(ie_duplicar_w = 'N') then
				select	count(*)
				into	cont_w
				from	W_Item_Prescr
				where	cd_material	= cd_material_w
				and	nr_prescricao	is null
				and	nm_usuario	= nm_usuario_p;
				
				if	(cont_w = 0) then
					ie_duplicar_w	:= 'S';
				end if;
			end if;	

			select 	W_Item_Prescr_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			insert into W_Item_Prescr(
				nr_sequencia,
				dt_atualizacao,         
				nm_usuario,             
				nr_prescricao,          
				cd_protocolo,            
				nr_seq_item,            
				qt_dose,
				cd_material,
				ie_origem_inf,
				nr_seq_solucao,
				cd_unidade_medida,
				ie_excluido)
			Values(
				nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				null,
				cd_procedimento_w,
				nr_seq_item_w,
				qt_dose_w,
				cd_material_w,
				'P',
				nr_seq_item_p,
				cd_unidade_medida_w,
				decode(ie_duplicar_w,'S','N','S'));
			
			end;
			
		end loop;
		close C01;
		
		commit;
	end if;
end if;

end Inserir_mat_W_Item_prescr;
/
