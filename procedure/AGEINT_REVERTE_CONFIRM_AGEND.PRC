create or replace
procedure Ageint_reverte_confirm_agend(		cd_agenda_p			number,
											nr_seq_agenda_p		number,
											ie_status_p			varchar2,
											cd_motivo_p			varchar2,
											ds_motivo_p			varchar2,
											ie_agenda_dia_p		varchar2,
											ie_tipo_agendamento_p	varchar2,
											nm_usuario_p		Varchar2) is 

begin

if	(nr_seq_agenda_p is not null)then
	begin
	
	Ageint_Alterar_Status_Agendas(cd_agenda_p, nr_seq_agenda_p, ie_status_p, '', '', 'N', ie_tipo_agendamento_p, nm_usuario_p);
	
	update	ageint_confirm_agendamento
	set		dt_confirmacao	= null
	where	nr_seq_agenda = nr_seq_agenda_p;
	
	commit;
	exception
	when others then
		null;
	end;
	
end if;

end Ageint_reverte_confirm_agend;
/