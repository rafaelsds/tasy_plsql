create or replace
procedure pls_gerar_resumo_comp_prop
				(nr_seq_proposta_p	number,				 				 				
				 nm_usuario_p		varchar2) is

				 nr_seq_plano_w			number(10);
nr_seq_sca_w			number(10);				 
nr_seq_beneficiario_w		number(10);
nr_seq_bonificacao_prop_w	number(10);
nr_seq_bonif_benef_w		number(10);
nr_seq_tx_insc_w		number(10);
nr_seq_tx_insc_plano_w		number(10);

cursor c01 is
	select	nr_sequencia
	from	pls_proposta_beneficiario	
	where	nr_seq_proposta = nr_seq_proposta_p;
	
Cursor C02 is
	select	nr_sequencia
	from	pls_sca_vinculo
	where	nr_seq_benef_proposta = nr_seq_beneficiario_w;
	
cursor c03 is
	select	nr_sequencia
	from	pls_bonificacao_vinculo
	where	nr_seq_proposta = nr_seq_proposta_p;
	
cursor c04 is
	select	nr_sequencia
	from	pls_bonificacao_vinculo
	where	nr_seq_segurado_prop = nr_seq_beneficiario_w;
	
Cursor C05 is
	select	nr_sequencia
	from	pls_regra_inscricao
	where	nr_seq_proposta = nr_seq_proposta_p;

Cursor C06 is
	select	nr_sequencia
	from	pls_regra_inscricao
	where	nr_seq_plano = nr_seq_plano_w;
	
begin
open C01;
loop
fetch C01 into	
	nr_seq_beneficiario_w;
exit when C01%notfound;
	begin		
	
	select	nr_seq_plano
	into	nr_seq_plano_w
	from	pls_proposta_beneficiario	
	where	nr_sequencia = nr_seq_beneficiario_w;
	
	pls_gerar_resumo_proposta(nr_seq_proposta_p, nr_seq_beneficiario_w, 'B',
				  nr_seq_plano_w, nm_usuario_p);
	
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_sca_w;
	exit when C02%notfound;
		begin
		pls_gerar_resumo_proposta(nr_seq_proposta_p, nr_seq_beneficiario_w, 'S',
					  nr_seq_sca_w, nm_usuario_p);	
		end;
	end loop;
	close C02;
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_bonificacao_prop_w;
	exit when C03%notfound;
		begin
		pls_gerar_resumo_proposta(nr_seq_proposta_p, nr_seq_beneficiario_w, 'BP',
					  nr_seq_bonificacao_prop_w, nm_usuario_p);	
		end;
	end loop;
	close C03;
	
	open C04;
	loop
	fetch C04 into	
		nr_seq_bonif_benef_w;
	exit when C04%notfound;
		begin
		pls_gerar_resumo_proposta(nr_seq_proposta_p, nr_seq_beneficiario_w, 'BO',
					  nr_seq_bonif_benef_w, nm_usuario_p);		
		end;
	end loop;
	close C04;		
	
	open C05;
	loop
	fetch C05 into	
		nr_seq_tx_insc_w;
	exit when C05%notfound;
		begin
		pls_gerar_resumo_proposta(nr_seq_proposta_p, nr_seq_beneficiario_w, 'I',
					  nr_seq_tx_insc_w, nm_usuario_p);											  
		end;
	end loop;
	close C05;		
	
	
	open C06;
	loop
	fetch C06 into	
		nr_seq_tx_insc_plano_w;
	exit when C06%notfound;
		begin
			pls_gerar_resumo_proposta(nr_seq_proposta_p, nr_seq_beneficiario_w, 'IP',
					  nr_seq_tx_insc_plano_w, nm_usuario_p);	
		end;
	end loop;
	close C06;


	end;
end loop;
close C01;

commit;

end pls_gerar_resumo_comp_prop;
/