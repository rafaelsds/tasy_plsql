create or replace PROCEDURE CPOE_SET_ORDER_UNIT_NUMBER(
                 nr_seq_cpoe_tipo_pedido_p number,
                 si_type_of_prescription_p varchar2,
                 nr_order_unit_p    out varchar2) is

nr_order_unit_w             cpoe_order_unit.nr_order_unit%type default null;
nr_seq_cpoe_rule_slip_w   cpoe_rule_slip_number.nr_sequencia%type  := null;
new_day_w                   varchar2(1) := 'N';
nr_current_seq_w      cpoe_rule_slip_number.nr_current_seq%type;
   
cursor C_rule is
  select  a.nr_sequencia
   from  cpoe_rule_slip_tipo_ped b,
         cpoe_rule_slip_number a
    where   a.nr_sequencia = b.nr_seq_cpoe_rule_slip(+)
    and   nvl(b.nr_seq_cpoe_tipo_pedido,nr_seq_cpoe_tipo_pedido_p) = nr_seq_cpoe_tipo_pedido_p
    and   nvl(b.si_type_of_prescription,nvl(si_type_of_prescription_p,'0')) = nvl(si_type_of_prescription_p,'0')
    and   sysdate Between pkg_date_utils.start_of(a.dt_vigencia_ini,'DAY') and NVL(pkg_date_utils.end_of(a.Dt_vigencia_fim,'DAY'), sysdate)
    and   b.si_event_type = 'OU'
    order by
    nvl(b.nr_seq_cpoe_tipo_pedido,'0'),
    nvl(b.si_type_of_prescription,'AAAAAAA');


cursor C_Update ( nr_sequencia_p  number) is
  select nr_sequencia,
         CD_PREFIXO_SLIP_NUMBER,
         ds_timestamp_format,
         nr_current_seq,
         qt_maximo,
         si_seq_restart_daily,
         dt_last_seq_reset,
         ds_sufixo
   from  cpoe_rule_slip_number
  where nr_sequencia = nr_sequencia_p

for update of dt_last_seq_reset, nr_current_seq;

   
begin

  for r_c_rule in c_rule loop
    nr_seq_cpoe_rule_slip_w := r_c_rule.nr_sequencia;
  end loop;


  if nr_seq_cpoe_rule_slip_w is not null then 
    for rLin in C_Update ( nr_seq_cpoe_rule_slip_w ) loop
       new_day_w := 'N';
       if  (rLin.si_seq_restart_daily = 'S') and
          ( (rLin.dt_last_seq_reset < trunc(sysdate,'dd')) or
            (rLin.dt_last_seq_reset is null) ) then
         new_day_w := 'S';
       end if;     
       if new_day_w = 'S' then
          nr_current_seq_w  := 1;
       else    
         nr_current_seq_w  := rLin.nr_current_seq + 1;
       end if;
    
       nr_order_unit_w := rLin.CD_PREFIXO_SLIP_NUMBER || TO_CHAR(SYSDATE, rLin.ds_timestamp_format)
                 || LPAD(nr_current_seq_w,LENGTH(rLin.qt_maximo), '0')
                 || rLin.ds_sufixo;

       if (new_day_w = 'S') then
          update  cpoe_rule_slip_number
              set nr_current_seq = 0,
                  dt_last_seq_reset = sysdate
          where current of C_Update;
        end if;

        update  cpoe_rule_slip_number
           set  nr_current_seq = nr_current_seq + 1
         where current of C_Update;
    end loop;
  end if;
  nr_order_unit_p := nr_order_unit_w;

end;
/
