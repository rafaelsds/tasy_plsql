create or replace
procedure Agserv_verf_agend_lista_espera(	cd_agenda_p		in number,
						cd_pessoa_fisica_p	in varchar2,
						nm_paciente_p		in varchar2,
						dt_agenda_p		in date,
						ds_consistencia_p	out varchar2) is 

qt_pac_lista_espera_w	number(10);
ds_mensagem_w		varchar2(2000);
			
begin
ds_consistencia_p := null;
-- verifica se existe paciente em Lista de Espera  no mesmo hor�rio, com status Aguardando, caso o cd_pessoa_fisica_p e o nm_paciente_p sejam diferentes;
select	count(*) 
into	qt_pac_lista_espera_w
from 	agenda_lista_espera a
where	a.cd_agenda = cd_agenda_p
and	(a.cd_pessoa_fisica <> cd_pessoa_fisica_p
or	a.nm_pessoa_lista <> nm_paciente_p)
and	a.dt_desejada = dt_agenda_p
and	a.ie_status_espera = 'A';


--se existir paciente em lista de espera para o mesmo hor�rio o agendamento do paciente ser� bloqueado para que o mesmo n�o passe na frente dos que est�o em lista de espera;
if	(qt_pac_lista_espera_w > 0) then	
	ds_mensagem_w := obter_desc_expressao(774809);	
end if;

ds_consistencia_p := ds_mensagem_w;

end Agserv_verf_agend_lista_espera;
/
