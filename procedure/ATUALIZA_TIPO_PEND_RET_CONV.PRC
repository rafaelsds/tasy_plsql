create or replace
procedure atualiza_tipo_pend_ret_conv(
			nr_sequencia_p	number,
			nr_seq_tipo_pend_p	number)
			is 

begin
	
	update	convenio_retorno_item
	set	nr_seq_tipo_pend	= nr_seq_tipo_pend_p
	where	nr_sequencia		= nr_sequencia_p;
	commit;

end atualiza_tipo_pend_ret_conv;
/