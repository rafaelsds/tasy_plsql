create or replace PROCEDURE p_integrar_exames_bb   
    (nr_seq_resultado_p EXAME_LAB_RESULTADO.nr_seq_resultado%type,
    dt_resultado_p EXAME_LAB_RESULTADO.dt_resultado%type,
    nr_atendimento_p EXAME_LAB_RESULTADO.nr_atendimento%type,
    ie_tipo_exame_p varchar2)
    
IS

    type thash IS VARRAY(17) OF VARCHAR2(32);
    labitens thash := thash(
      '53c08e0380dc489aaca8f0e27ebb5006',
      'bc18562b37cb4e96974adea6f37f66ca',
      'ecdbf83f11044e97ac07efefb897eee1',
      '6ddec8a3f67d4cbbb84259b9c090e3d2',
      '7e977aea81f74c50ab142a1f0c0bad10',
      'b86e76098b3a40a293b429d506c21992',
      'b293f206fa9b4c8992679c34a4946d13',
      '77eb037db6b545cc8855c8f8ff71e8c3',
      'db9dd2caa28a478f866760da7edbb715',
      '6888379d5fcd4cd8b8a4bddc73adb32c',
      '1cbffbac46124b00b647f281d970952e',
      'd84cf4287f2d47acb6c91cb703300e08',
      '6fa09ee62c384e6eaaf09a467a202ac8',
      '8803942a916a4dd1a54963b469f9896e',
      'ffad0223c96543d1916b2e133b441753',
      '21fe56244de94fbab33cc2a3696d9b0d',
      '492072c8c46742aeb0bcfd6063c2cca4'
    );
    
    json_smart_alert                philips_json;
    json_microbiology               philips_json;
    json_chemistry                  philips_json_list;
    json_hematology                 philips_json_list;
    json_abg                        philips_json_list;
    json_lab_result                 philips_json;
    envio_integracao_bb             clob;
    retorno_integracao_bb           clob;

    bb_min_result                   EXAME_LAB_RESULT_ITEM.QT_MINIMA%TYPE;
    bb_max_result                   EXAME_LAB_RESULT_ITEM.QT_MAXIMA%TYPE;
    bb_conversion_factor            CONVERSAO_UNID_MED_LAB.NR_FATOR_CONVERSAO%TYPE;
    bb_unit_of_measurement          LAB_UNIDADE_MEDIDA.DS_UNIDADE_MEDIDA%TYPE;
    bb_exam_result                  EXAME_LAB_RESULT_ITEM.QT_RESULTADO%TYPE;
    bb_exam_type                    ALGORITMOS_VARIAVEL_EXAME.IE_VARIAVEL%TYPE;
    bb_source_unit_of_measurement   LAB_UNIDADE_MEDIDA.NR_SEQUENCIA%TYPE;
    bb_target_unit_of_measurement   LAB_UNIDADE_MEDIDA.NR_SEQUENCIA%TYPE;
    bb_formato_resultado            EXAME_LABORATORIO.IE_FORMATO_RESULTADO%TYPE;

    CURSOR lab_result_item_c IS
    SELECT * FROM  exame_lab_result_item WHERE nr_seq_resultado = nr_seq_resultado_p;

BEGIN

  json_smart_alert := philips_json();  
  json_smart_alert.put('typeID', 'LABS');
  json_smart_alert.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'MM/DD/YYYY HH24:MI:SS.SSSSS'));
  json_smart_alert.put('patientHealthSystemStayID', LPAD(nr_atendimento_p, 32, 0));
  json_smart_alert.put('labOrderNumber', LPAD(nr_seq_resultado_p, 32, 0));
  json_smart_alert.put('objectID', LPAD(nr_seq_resultado_p, 32, 0));
  json_smart_alert.put('labResultDateGMT', TO_CHAR(f_extract_utc_bb(dt_resultado_p), 'YYYY-MM-DD"T"HH24:MI'));
  json_smart_alert.put('labResultDateGMTOffset', 0);
  
  json_chemistry := philips_json_list();
  json_hematology := philips_json_list();
  json_abg := philips_json_list();
      
  IF (ie_tipo_exame_p = 'I') THEN
      
    FOR item IN lab_result_item_c LOOP
      BEGIN
        SELECT IE_VARIAVEL INTO bb_exam_type FROM ALGORITMOS_VARIAVEL_EXAME WHERE nr_seq_exame = item.NR_SEQ_EXAME;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          bb_exam_type := NULL;
      END;
      
      BEGIN
        SELECT NR_SEQUENCIA, DS_UNIDADE_MEDIDA
        INTO bb_source_unit_of_measurement, bb_unit_of_measurement
        FROM LAB_UNIDADE_MEDIDA   
        WHERE NR_SEQUENCIA = item.NR_SEQ_UNID_MED;
      
        SELECT nr_seq_unidade_medida 
        INTO bb_target_unit_of_measurement
        FROM ALGORITMOS_VAR_EXAME_UM   
        WHERE IE_VARIAVEL = bb_exam_type;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          bb_unit_of_measurement := NULL;
          bb_source_unit_of_measurement := NULL;
          bb_target_unit_of_measurement := NULL;
      END;
      
      IF (bb_source_unit_of_measurement = bb_target_unit_of_measurement) THEN
          bb_conversion_factor := 1;
      ELSE
        BEGIN
          SELECT NR_FATOR_CONVERSAO
          INTO bb_conversion_factor
          FROM CONVERSAO_UNID_MED_LAB 
          WHERE nr_seq_unidade_medida_origem = bb_source_unit_of_measurement
          AND nr_seq_unidade_medida_destino = bb_target_unit_of_measurement;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            bb_conversion_factor := NULL;
        END;
      END IF;

      IF (bb_exam_type is not null AND item.qt_resultado is not null AND bb_conversion_factor is not null AND bb_unit_of_measurement is not null) THEN
        json_lab_result := philips_json();
        json_lab_result.put('objectID', LPAD(item.nr_sequencia, 32, 0));
        json_lab_result.put('resultSourceID', '0253220e18bc4089a4dd1ad350abf795');
        json_lab_result.put('resultStatusID', '9ef5ea20a97045fcb26dc3fcb86b6c24');
        
        json_lab_result.put('labItemNameID', labitens(bb_exam_type));
        json_lab_result.put('labMeasurementName', bb_unit_of_measurement);

        bb_exam_result := item.qt_resultado * bb_conversion_factor;
        json_lab_result.put('resultNumeric', bb_exam_result);
        json_lab_result.put('labResultText', TO_CHAR(bb_exam_result));

        IF (item.qt_minima is not null AND item.qt_maxima is not null) THEN
          bb_min_result := item.qt_minima * bb_conversion_factor; 
          bb_max_result := item.qt_maxima * bb_conversion_factor;
          json_lab_result.put('labRangeText', bb_min_result || '-' || bb_max_result);
        ELSIF (item.pr_minimo is not null AND item.pr_maximo is not null) THEN
          bb_min_result := item.pr_minimo * bb_conversion_factor;
          bb_max_result := item.pr_maximo * bb_conversion_factor;
          json_lab_result.put('labRangeText', bb_min_result || '-' || bb_max_result);
        ELSE
          json_lab_result.put('labRangeText', '');
          json_lab_result.put('abnormalFlag', 'False');
        END IF;
          
        IF (bb_min_result is not null AND bb_max_result is not null) THEN
          IF (bb_exam_result < bb_min_result OR bb_exam_result > bb_max_result) THEN
            json_lab_result.put('abnormalFlag', 'True');
          ELSE
            json_lab_result.put('abnormalFlag', 'False');
          END IF;
        END IF;

        IF (bb_exam_type in (1,3,4,5,9,16)) THEN
          json_chemistry.append(json_lab_result.to_json_value());
        ELSIF (bb_exam_type in (2,6,15)) THEN
          json_hematology.append(json_lab_result.to_json_value());
        ELSIF (bb_exam_type in (7,8,10,11,12,13,14,17)) THEN
          json_abg.append(json_lab_result.to_json_value());
        END IF;
      END IF;
      
      BEGIN
        SELECT ie_formato_resultado INTO bb_formato_resultado FROM EXAME_LABORATORIO WHERE nr_seq_exame = item.NR_SEQ_EXAME;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          bb_formato_resultado := NULL;
      END;
      
      IF (bb_formato_resultado is not null AND bb_formato_resultado = 'SM' AND
          item.ds_resultado is not null AND LOWER(item.ds_resultado) = 'positivo') THEN
        json_microbiology := philips_json();  
        json_microbiology.put('typeID', 'MICRO');
        json_microbiology.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'MM-DD-YYYY HH24:MI:SS.SSSSS'));
        json_microbiology.put('patientHealthSystemStayID', LPAD(nr_atendimento_p, 32, 0));
        json_microbiology.put('collectionDate', TO_CHAR(f_extract_utc_bb(item.dt_coleta), 'YYYY-MM-DD"T"HH24:MM'));
        json_microbiology.put('orderNumber', LPAD(item.nr_sequencia, 32, 0));
        json_microbiology.put('resultStatusID', '9ef5ea20a97045fcb26dc3fcb86b6c24');
        dbms_lob.createtemporary(envio_integracao_bb, TRUE);
        json_microbiology.to_clob(envio_integracao_bb);
        SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Microbiology',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;
      END IF;
      
    END LOOP;
  END IF;
  
  IF (ie_tipo_exame_p = 'D') THEN       
      
    FOR item IN lab_result_item_c LOOP
      json_lab_result := philips_json();
      
      BEGIN
        SELECT IE_VARIAVEL INTO bb_exam_type FROM ALGORITMOS_VARIAVEL_EXAME WHERE nr_seq_exame = item.NR_SEQ_EXAME;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          bb_exam_type := NULL;
      END;
      
      IF (bb_exam_type is not null AND item.qt_resultado is not null) THEN
        json_lab_result.put('objectID', LPAD(item.nr_sequencia, 32, 0));
        json_lab_result.put('resultStatusID', 'b2d3b6203742404aa12297c91fd40b8e');
        
        IF (bb_exam_type in (1,3,4,5,9,16)) THEN
          json_chemistry.append(json_lab_result.to_json_value());
        ELSIF (bb_exam_type in (2,6,15)) THEN
          json_hematology.append(json_lab_result.to_json_value());
        ELSIF (bb_exam_type in (7,8,10,11,12,13,14,17)) THEN
          json_abg.append(json_lab_result.to_json_value());
        END IF;
      END IF;
    END LOOP; 
  END IF;
  
  IF (json_chemistry.count > 0) THEN
    json_smart_alert.put('labTypeID', '6edbcc89dd214bb6bef55e61f1c08c8c');
    json_smart_alert.put('results', json_chemistry);
    dbms_lob.createtemporary(envio_integracao_bb, TRUE);
    json_smart_alert.to_clob(envio_integracao_bb);
    SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Lab_Result',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;
  END IF;

  IF (json_hematology.count > 0) THEN
    json_smart_alert.put('labTypeID', '925ccfcd1bad431c9edf7770a5ea45a9');
    json_smart_alert.put('results', json_hematology);
    dbms_lob.createtemporary(envio_integracao_bb, TRUE);
    json_smart_alert.to_clob(envio_integracao_bb);
    SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Lab_Result',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;
  END IF;

  IF (json_abg.count > 0) THEN
    json_smart_alert.put('labTypeID', 'c83183bfac984fa0a2dc7df5dbd6c0b9');
    json_smart_alert.put('results', json_abg);
    dbms_lob.createtemporary(envio_integracao_bb, TRUE);
    json_smart_alert.to_clob(envio_integracao_bb);
    SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Lab_Result',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;
  END IF;
    
END;
/