create or replace 
procedure Cancelar_desdobramento_titulo (nr_titulo_p	number,
					nm_usuario_p	varchar2) is

cont_w			number(5)		:= 0;
nr_lote_contabil_w		number(10)	:= 0;
nr_titulo_w		number(10);
nr_seq_alt_valor_w		number(5);
cd_moeda_w		number(10,0);
vl_saldo_titulo_w		number(15,2);
ie_situacao_w		varchar2(1);
vl_alteracao_w		number(15,2);

Cursor c01 is
select	b.nr_titulo_dest
from	titulo_receber_desdob b,
	titulo_receber a
where	a.nr_titulo	= b.nr_titulo
and	b.nr_titulo	= nr_titulo_p
and	b.nr_titulo_dest is not null
order by nr_titulo_dest;

begin

select	ie_situacao,
	nvl(nr_lote_contabil,0),
	cd_moeda,
	vl_titulo
into	ie_situacao_w,
	nr_lote_contabil_w,
	cd_moeda_w,
	vl_saldo_titulo_w
from	titulo_receber
where	nr_titulo	= nr_titulo_p;

if	(ie_situacao_w = '5') and
	(nr_lote_contabil_w = 0) then
	Open c01;
	loop
	fetch c01 into
		nr_titulo_w;
	exit when c01%notfound;
		cancelar_titulo_receber(nr_titulo_w,
					nm_usuario_p,
					'N',
					sysdate);

		delete	from titulo_receber_desdob
		where	nr_titulo_dest = nr_titulo_w;
	end loop;
	close c01;

/*	OS83712 - 21/02/2008 - Troquei pelo update e o insert abaixo.
	update	titulo_receber
	set	ie_situacao 	= '1',
		dt_liquidacao	= null,
		vl_saldo_titulo	= obter_saldo_titulo_receber(nr_titulo,sysdate),
		ds_observacao_titulo = null
	where	nr_titulo	= nr_titulo_p;
*/

	update	titulo_receber
	set	ds_observacao_titulo	= null
	where	nr_titulo		= nr_titulo_p;

	/* Obter a �ltima altera��o de valor para estornar */
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_alt_valor_w
	from	alteracao_valor
	where	nr_titulo	= nr_titulo_p;

	/* Obter o valor da �ltima altera��o */
	select	nvl(max(vl_alteracao),vl_saldo_titulo_w)
	into	vl_alteracao_w
	from	alteracao_valor
	where	nr_titulo	= nr_titulo_p
	and	nr_sequencia	= nr_seq_alt_valor_w;

	/* Obter a sequencia para a nova alteracao */
	select	nvl(max(nr_sequencia),0)+1
	into	nr_seq_alt_valor_w
	from	alteracao_valor
	where	nr_titulo	= nr_titulo_p;

	insert	into alteracao_valor	
		(cd_moeda,
		ds_observacao,
		dt_alteracao,
		dt_atualizacao,
		nm_usuario,
		nr_sequencia,
		nr_titulo,
		vl_alteracao,
		vl_anterior,
		ie_aumenta_diminui,
		cd_motivo,
		nr_lote_contabil)
	values	(cd_moeda_w,
		OBTER_DESC_EXPRESSAO(344577),
		sysdate,
		sysdate,
		nm_usuario_p,
		nr_seq_alt_valor_w,
		nr_titulo_p,
		vl_alteracao_w, /* Francisco - OS 87440 - Troquei o vl_saldo_titulo_w pelo vl_alteracao_w */
		vl_alteracao_w,
		'A',
		2,
		0);

	atualizar_saldo_tit_rec(nr_titulo_p,nm_usuario_p);
else
	wheb_mensagem_pck.exibir_mensagem_abort(265766,'');
	--O t�tulo j� possui lote cont�bil!
end if;
	
commit;

end Cancelar_desdobramento_titulo;
/
