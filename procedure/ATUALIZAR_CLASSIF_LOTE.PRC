create or replace
procedure atualizar_classif_lote(
			nr_prescricao_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is

nr_seq_classif_w	classif_lote_disp_far.nr_sequencia%type;
ie_classif_urgente_w	classif_lote_disp_far.ie_classif_urgente%type;
ie_controlado_w		classif_lote_disp_far.ie_controlado%type;
ie_padronizado_w	classif_lote_disp_far.ie_padronizado%type;
nr_seq_lote_w		ap_lote.nr_sequencia%type;

cursor c01 is
	select	nr_sequencia,
		ie_classif_urgente,
		ie_controlado,
		ie_padronizado
	from	classif_lote_disp_far
	where	cd_estabelecimento = cd_estabelecimento_p
	and	ie_situacao = 'A'
	order by ie_classif_urgente,
		ie_controlado desc,
		ie_padronizado desc;

cursor c02 is
	select	nr_seq_classif,
		nr_seq_lote
	from	prescr_mat_hor
	where	nr_prescricao = nr_prescricao_p
	and	nr_seq_lote is not null
	order by nr_seq_superior;

begin
open c01;
loop
fetch c01 into
	nr_seq_classif_w,
	ie_classif_urgente_w,
	ie_controlado_w,
	ie_padronizado_w;
exit when c01%notfound;
	begin
	if	(ie_controlado_w = 'A') and
		(ie_padronizado_w = 'A') then
		update	prescr_mat_hor
		set	nr_seq_classif		= nr_seq_classif_w
		where	nr_prescricao		= nr_prescricao_p
		and	ie_classif_urgente	= ie_classif_urgente_w;
	elsif	(ie_controlado_w = 'A') and
		(ie_padronizado_w = 'S') then
		update	prescr_mat_hor
		set	nr_seq_classif		= nr_seq_classif_w
		where	nr_prescricao		= nr_prescricao_p
		and	ie_padronizado		= 'S'
		and	ie_classif_urgente	= ie_classif_urgente_w;
	elsif	(ie_controlado_w = 'A') and
		(ie_padronizado_w = 'N') then
		update	prescr_mat_hor
		set	nr_seq_classif		= nr_seq_classif_w
		where	nr_prescricao		= nr_prescricao_p
		and	ie_padronizado		= 'N'
		and	ie_classif_urgente	= ie_classif_urgente_w;
	elsif	(ie_controlado_w = 'N') and
		(ie_padronizado_w = 'A') then
		update	prescr_mat_hor
		set	nr_seq_classif		= nr_seq_classif_w
		where	nr_prescricao		= nr_prescricao_p
		and	ie_controlado		= 'N'
		and	ie_classif_urgente	= ie_classif_urgente_w;
	elsif	(ie_controlado_w = 'N') and
		(ie_padronizado_w = 'N') then
		update	prescr_mat_hor
		set	nr_seq_classif		= nr_seq_classif_w
		where	nr_prescricao		= nr_prescricao_p
		and	ie_controlado		= 'N'
		and	ie_padronizado		= 'N'
		and	ie_classif_urgente	= ie_classif_urgente_w;
	elsif	(ie_controlado_w = 'N') and
		(ie_padronizado_w = 'S') then
		update	prescr_mat_hor
		set	nr_seq_classif		= nr_seq_classif_w
		where	nr_prescricao		= nr_prescricao_p
		and	ie_controlado		= 'N'
		and	ie_padronizado		= 'S'
		and	ie_classif_urgente	= ie_classif_urgente_w;
	elsif	(ie_controlado_w = 'S') and
		(ie_padronizado_w = 'A') then
		update	prescr_mat_hor
		set	nr_seq_classif		= nr_seq_classif_w
		where	nr_prescricao		= nr_prescricao_p
		and	ie_controlado		= 'S'
		and	ie_classif_urgente	= ie_classif_urgente_w;
	elsif	(ie_controlado_w = 'S') and
		(ie_padronizado_w = 'N') then
		update	prescr_mat_hor
		set	nr_seq_classif		= nr_seq_classif_w
		where	nr_prescricao		= nr_prescricao_p
		and	ie_controlado		= 'S'
		and	ie_padronizado		= 'N'
		and	ie_classif_urgente	= ie_classif_urgente_w;
	elsif	(ie_controlado_w = 'S') and
		(ie_padronizado_w = 'S') then
		update	prescr_mat_hor
		set	nr_seq_classif		= nr_seq_classif_w
		where	nr_prescricao		= nr_prescricao_p
		and	ie_controlado		= 'S'
		and	ie_padronizado		= 'S'
		and	ie_classif_urgente	= ie_classif_urgente_w;
	end if;
	end;
end loop;
close c01;

open c02;
loop
fetch c02 into
	nr_seq_classif_w,
	nr_seq_lote_w;
exit when c02%notfound;
	begin
	update	ap_lote
	set	nr_seq_classif = nr_seq_classif_w
	where	nr_sequencia = nr_seq_lote_w
	and	ie_status_lote = 'G';
	end;
end loop;
close c02;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end atualizar_classif_lote;
/
