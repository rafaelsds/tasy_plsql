create or replace
procedure alterar_seq_estagio_conta(	nr_interno_conta_p	number,
				   	nr_seq_estagio_p 	number,
					ie_tipo_operacao_p	varchar2,
					nm_usuario_p		varchar2) is 
begin
if 	(nr_interno_conta_p is not null) and
	(nr_seq_estagio_p is not null) and
	(ie_tipo_operacao_p = wheb_mensagem_pck.get_texto(802229)) then	
	update	conta_paciente 
	set	nr_seq_estagio_conta	= nr_seq_estagio_p,
		dt_atualizacao 		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_interno_conta 	= nr_interno_conta_p;	
	commit;
else
	if (ie_tipo_operacao_p = wheb_mensagem_pck.get_texto(802230)) then
		update	conta_paciente 
		set	nr_seq_estagio_conta	= null,
			dt_atualizacao 		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_interno_conta 	= nr_interno_conta_p;	
		commit;
	end if;
end if;

end alterar_seq_estagio_conta;
/