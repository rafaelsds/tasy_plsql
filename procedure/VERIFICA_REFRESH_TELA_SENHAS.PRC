create or replace 
procedure verifica_refresh_tela_senhas(	nr_seq_local_p				number,
					dt_ultima_visualizacao_p	in out 	date,
					qt_ultimo_count_p		in out 	number,
					ie_atualiza_p			out 	varchar2) is

ie_retorno_w		varchar2(1);
qt_count_w		number(10);
dt_atualizacao_w	date;

begin

select	count(*)
into	qt_count_w
from	regra_liberacao_fila
where	nr_seq_local_senha = nr_seq_local_p;

select	max(dt_atualizacao)
into	dt_atualizacao_w
from	regra_liberacao_fila
where	nr_seq_local_senha = nr_seq_local_p;

ie_atualiza_p := 'N';
if	(qt_count_w <> qt_ultimo_count_p) then
	ie_atualiza_p := 'S'; -- Se tem mais ou menos registros do que na �ltima verifica��o, deve atualizar a tela.
else	
	if	(nvl(dt_atualizacao_w, dt_ultima_visualizacao_p - 1) > dt_ultima_visualizacao_p) then
		ie_atualiza_p := 'S'; -- Se a data da �ltima atualiza��o da tabela for maior do que na �ltima verifica��o.
	else
		ie_atualiza_p := 'N'; -- Se n�o acontecer nenhum dos tratamentos acima, n�o deve atualizar a tela.
	end if;
end if;

qt_ultimo_count_p := qt_count_w;
dt_ultima_visualizacao_p := dt_atualizacao_w;

end verifica_refresh_tela_senhas;
/