create or replace
procedure gerar_evento_alerta_servico(cd_estabelecimento_p	number,
				      nm_usuario_p		varchar2) is 

nr_seq_evento_w			number(10);
nr_seq_unidade_w		number(10);	
qt_evento_servico_w		number(10);
dt_log_w			date;		
			
Cursor C01 is
	select	nr_seq_unidade
	from	sl_unid_atend
	where	dt_atualizacao_nrec > nvl(dt_log_w,sysdate);

Cursor C02 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= 1
	and	ie_evento_disp 		= 'GEIN'
	and	nvl(ie_situacao,'A') = 'A';

begin

select	max(dt_atualizacao)
into	dt_log_w
from	log_tasy
where	cd_log = 1504;


select	count(*)
into	qt_evento_servico_w
from	sl_unid_atend
where	dt_atualizacao_nrec > nvl(dt_log_w,sysdate);
	
if	(qt_evento_servico_w > 0) then
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_unidade_w;
	exit when C01%notfound;
		begin
		
		open c02;
		loop
		fetch c02 into	
			nr_seq_evento_w;
		exit when c02%notfound;
			begin			
			gerar_evento_paciente(nr_seq_evento_w,null,null,null,nm_usuario_p,null,null,null,null,null,null,null,null,null,null,'N',nr_seq_unidade_w);
			end;
		end loop;
		close c02;
		
		end;
	end loop;
	close C01;

/*	insert	into log_XXtasy (dt_atualizacao,
				nm_usuario,
				cd_log,
				ds_log)
			values (sysdate,
				nm_usuario_p,
				1504,
				substr('Evento: '|| nr_seq_evento_w || ' - Unidade: '|| nr_seq_unidade_w,1,100));   */
end if;

commit;
end gerar_evento_alerta_servico;
/