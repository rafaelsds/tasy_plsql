create or replace procedure LAUDO_PACIENTE_MARCAR_CIENTE(
    nr_prescricao_p  number,
    nr_seq_prescricao_p number,
    nm_usuario_p varchar,
    nr_seq_laudo_p number,
    nr_seq_ext_p        number default 0,
    nr_seq_result_ext_p number default 0)
is
ds_parameter_w      varchar2(2000);
nr_atendimento_w    number(10);
cd_pessoa_fisica_w  varchar2(10);
nr_seq_interno_w    number(10);
nr_seq_proc_hor_w   number(10);
dt_horario_w	date;
/*
    M - Marcado como ciente
    D - Desmarcado como ciente
*/
begin
    insert into laudo_paciente_ciente
    (nr_sequencia, 
    nr_prescricao,
    nr_seq_prescricao,
    nm_usuario, 
    nm_usuario_nrec, 
    dt_atualizacao,
    dt_atualizacao_nrec,
    nr_seq_laudo,
    nr_seq_ext,
    nr_seq_result_ext,
    ie_tipo_acao)
    values 
    (laudo_paciente_ciente_seq.nextval, 
    nr_prescricao_p,
    nr_seq_prescricao_p,
    nm_usuario_p, 
    nm_usuario_p, 
    sysdate,
    sysdate,
    nr_seq_laudo_p,
    nr_seq_ext_p,
    nr_seq_result_ext_p,
    'M'); 
    wl_gerar_finalizar_tarefa('EN','F',null,null,nm_usuario_p,null,'S',null,null,null,nr_prescricao_p,nr_seq_prescricao_p);
    begin
    select  a.nr_atendimento,
        a.cd_pessoa_fisica,
        b.nr_seq_interno
    into    nr_atendimento_w,
        cd_pessoa_fisica_w,
        nr_seq_interno_w
    from    prescr_medica a,
        prescr_procedimento b
    where   a.nr_prescricao = b.nr_prescricao
    and b.nr_prescricao = nr_prescricao_p
    and b.nr_sequencia = nr_seq_prescricao_p;
    exception
    when others then
        nr_atendimento_w := 0;
    end;
    if (nr_atendimento_w > 0) then 
        begin
		
		
		select  nvl(max(nr_sequencia),0),
				nvl(max(dt_horario),sysdate)
		into    nr_seq_proc_hor_w,
				dt_horario_w
		from    prescr_proc_hor
		where   nr_prescricao   = nr_prescricao_p
		and     nr_seq_procedimento = nr_seq_prescricao_p;        
					
        if (nr_seq_laudo_p is not null) then

        ds_parameter_w := '{"CD_PESSOA_FISICA" : "' || cd_pessoa_fisica_w || '"' || 
                    ' , "NR_ATENDIMENTO" : ' || nr_atendimento_w || 
                    ' , "NR_PRESCRICAO" : '  || nr_prescricao_p ||
                    ' , "NR_SEQ_PRESCR" : ' || nr_seq_prescricao_p ||
					' , "NR_SEQ_PROCEDIMENTO" : ' || nr_seq_proc_hor_w  || '}';          
							
            execute_bifrost_integration(80,ds_parameter_w);
        else
		
        ds_parameter_w := '{"CD_PESSOA_FISICA" : "' || cd_pessoa_fisica_w || '"' || 
                    ' , "NR_ATENDIMENTO" : ' || nr_atendimento_w || 
                    ' , "NR_PRESCRICAO" : '  || nr_prescricao_p ||
                    ' , "NR_SEQ_PRESCR" : ' || nr_seq_prescricao_p ||
					' , "DT_HORARIO" : ' || pkg_date_utils.get_isoformat(dt_horario_w)  || '}';          
							
            execute_bifrost_integration(120,ds_parameter_w);
        end if;
        end;
    end if;
    commit;  
end laudo_paciente_marcar_ciente;
/