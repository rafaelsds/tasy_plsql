create or replace
procedure sus_gerar_valor_porte_proced( 	nr_sequencia_p	number,
					nm_usuario_p	varchar2) as

nr_sequencia_w		number(10,0);
nr_seq_regra_porte_w	number(10,0);
dt_atualizacao_w		date;
nm_usuario_w		varchar2(15);
nr_seq_procedimento_w	number(10,0);
cd_procedimento_w	number(15,0);
ie_origem_proced_w	number(10,0);
dt_procedimento_w		date;
nr_atendimento_w		number(10,0);
cd_anestesista_w		varchar2(10);
vl_porte_anestesico_w	number(15,2);
qt_proced_porte_w		number(10) := 0;
cd_edicao_amb_w		number(6,0);
nr_porte_anestesico_w	number(2,0);
dt_inicio_vigencia_w	date;

begin

begin
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.dt_procedimento,
	a.nr_atendimento,
	b.cd_pessoa_fisica
into	cd_procedimento_w,
	ie_origem_proced_w,
	dt_procedimento_w,
	nr_atendimento_w,
	cd_anestesista_w
from 	procedimento_paciente a,
	procedimento_participante b
where	a.nr_sequencia	= nr_sequencia_p
and	b.nr_sequencia	= a.nr_sequencia
and	a.cd_motivo_exc_conta is null
and	sus_obter_indicador_equipe(b.ie_funcao) = 6
and	b.ie_participou_sus = 'S';
exception
when others then
	cd_procedimento_w	:= 0;
	ie_origem_proced_w	:= 0;
end;

if	(nvl(cd_procedimento_w,0) <> 0) then
	begin

	begin
	select 	count(*)
	into	qt_proced_porte_w
	from 	sus_valor_porte_proced
	where	nr_seq_procedimento = nr_sequencia_p;
	exception
	when others then
		--r.aise_application_error(-20011,'Problema ao veridicar se o procedimento j� esta na tabela de porte!'||'#@#@');
		wheb_mensagem_pck.exibir_mensagem_abort(263519);
	end;

	if	(qt_proced_porte_w = 0) then
		begin

		begin
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_regra_porte_w
		from	sus_regra_porte_proced
		where	cd_procedimento 		= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proced_w
		and	ie_situacao		= 'A';
		exception
		when OTHERS then
			nr_seq_regra_porte_w := 0;
		end;

		if	(nr_seq_regra_porte_w <> 0) then
			begin

			begin
			select	nvl(vl_porte_anestesico,0)
			into	vl_porte_anestesico_w
			from	porte_anestesico a,
				sus_regra_porte_proced b
			where	a.cd_edicao_amb 	= b.cd_edicao_amb
			and	a.nr_porte_anestesico 	= b.nr_porte_anestesico
			and	a.dt_inicio_vigencia	= b.dt_inicio_vigencia
			and	b.nr_sequencia		= nr_seq_regra_porte_w;
			exception
			when OTHERS then
				--r.aise_application_error(-20011,'Problema ao obter o valor do porte anest�sico para o procedimento '||cd_procedimento_w||'.'||'#@#@');
				wheb_mensagem_pck.exibir_mensagem_abort(263520,'cd_procedimento_w='||cd_procedimento_w);
			end;

			insert into sus_valor_porte_proced(
					nr_sequencia,
					nr_seq_regra_porte,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_procedimento,
					cd_procedimento,
					ie_origem_proced,
					dt_procedimento,
					nr_atendimento,
					cd_anestesista,
					vl_porte_anestesico,
					vl_total_anestesista)
			values (		sus_valor_porte_proced_seq.nextval,
					nr_seq_regra_porte_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_sequencia_p,
					cd_procedimento_w,
					ie_origem_proced_w,
					dt_procedimento_w,
					nr_atendimento_w,
					cd_anestesista_w,
					vl_porte_anestesico_w,
					vl_porte_anestesico_w);

			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

			end;
		end if;

		end;
	end if;

	end;
end if;

end sus_gerar_valor_porte_proced;
/
