create or replace
procedure pls_desfazer_lib_lote_prec_fed(nr_seq_lote_p		number,
					nm_usuario_p		varchar2) is 

nr_seq_pre_imp_w		number(10);
nr_seq_preco_w			number(10);
ie_situacao_w			varchar2(1);
ie_situacao_ant_w		varchar2(1);
					
Cursor C01 is
	select	nr_sequencia,
		ie_situacao,
		ie_situacao_ant,
		nr_seq_preco
	from	pls_mat_uni_sc_pre_imp
	where	nr_seq_lote = nr_seq_lote_p
	and	ie_inconsistente = 'N';
						
begin

open C01;
loop
fetch C01 into	
	nr_seq_pre_imp_w,
	ie_situacao_w,
	ie_situacao_ant_w,
	nr_seq_preco_w;
exit when C01%notfound;
	begin	
	if	(ie_situacao_ant_w is null) then
		update	pls_mat_uni_sc_pre_imp
		set	nr_seq_preco	= null
		where	nr_sequencia	= nr_seq_pre_imp_w;
	
		delete	pls_mat_uni_fed_sc_preco
		where	nr_sequencia	= nr_seq_preco_w;
	else
		update	pls_mat_uni_fed_sc_preco
		set	ie_situacao	= ie_situacao_ant_w
		where	nr_sequencia	= nr_seq_preco_w;	
	end if;
	end;
end loop;
close C01;

update	pls_lote_preco_unimed_sc
set	dt_liberacao	= null,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_lote_p;

commit;

end pls_desfazer_lib_lote_prec_fed;
/