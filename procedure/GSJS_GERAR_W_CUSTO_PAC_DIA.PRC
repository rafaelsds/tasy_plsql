create or replace
procedure gsjs_gerar_w_custo_pac_dia(dt_inicio_p date,
			dt_fim_p date,
			cd_estabelecimento_p	number,
			cd_convenio_p	number,
			ie_tipo_atendimento_p	number,
			nm_usuario_p		Varchar2) is 

vl_custo_pac_w	number(13,4);
vl_custo_req_w	number(13,4);
cd_setor_atendimento_w	number(5);
nr_atendimento_w	number(10);
cd_material_w	number(6);
dt_mesano_referencia_w	date;
qt_existe_w	number(13);
qt_paciente_w	number(10);

/*
Create Table GSJS_W_CUSTO_PAC_DIA(
  CD_SETOR_ATENDIMENTO NUMBER(10,0)     ,
  NR_ATENDIMENTO NUMBER(10,0)     ,
  CD_MATERIAL NUMBER(06,0)     ,
  VL_CUSTO_REQ NUMBER(13,4)     ,
  VL_CUSTO_PAC NUMBER(13,4)     ,
  DT_ATUALIZACAO DATE           not null  ,
  NM_USUARIO VARCHAR2(0015)     not null  ,
  QT_PACIENTE NUMBER(10,0) )
*/

Cursor C01 is
select	/*+CHOOSE */
	a.cd_setor_atendimento,
	sum(decode(b.ie_consumo,
		'A',	decode(	a.cd_acao,
			1,	(a.qt_estoque),
				(a.qt_estoque * -1)),
	 	'D',	decode(	a.cd_acao,
			1,	(a.qt_estoque * -1),
				(a.qt_estoque)),
			0) * obter_custo_medio_material(a.cd_estabelecimento, a.dt_mesano_referencia, a.cd_material)) vl_custo_pac,
		0 vl_custo_req
from	movimento_estoque a,
		operacao_estoque b,
		setor_atendimento c
where	a.cd_operacao_estoque = b.cd_operacao_estoque
and	a.cd_setor_atendimento = c.cd_setor_atendimento
and	c.cd_classif_setor not in (5,6,7)
and	a.dt_processo is not null
and	a.cd_estabelecimento = cd_estabelecimento_p
and	a.ie_origem_documento in (14,3)
and	(nvl(ie_tipo_atendimento_p,0) = 0 or Obter_Tipo_Atendimento(a.nr_atendimento) = 0)
and	(nvl(cd_convenio_p,0) = 0 or Obter_Convenio_Atendimento(a.nr_atendimento) = 0)
and	a.dt_movimento_estoque between dt_inicio_p and fim_dia(dt_fim_p)
group by a.cd_setor_atendimento
union all
select	/*+ CHOOSE */
		a.cd_setor_atendimento,
		0 vl_custo_pac,
		sum(decode(b.ie_consumo,
			'A',	decode(	a.cd_acao,
				1,	(a.qt_estoque),
					(a.qt_estoque * -1)),
		 	'D',	decode(	a.cd_acao,
				1,	(a.qt_estoque * -1),
					(a.qt_estoque)),
			0) * obter_custo_medio_material(a.cd_estabelecimento, a.dt_mesano_referencia, a.cd_material)) vl_custo_req
from	movimento_estoque a,
		operacao_estoque b,
		setor_atendimento c
where	a.cd_operacao_estoque = b.cd_operacao_estoque
and	a.cd_setor_atendimento = c.cd_setor_atendimento
and	c.cd_classif_setor not in (5,6,7)
and	a.dt_processo is not null
and	a.ie_origem_documento = 2
and	a.cd_estabelecimento = cd_estabelecimento_p
and	a.dt_movimento_estoque between dt_inicio_p and fim_dia(dt_fim_p)
group by a.cd_setor_atendimento;

begin
delete from gsjs_w_custo_pac_dia where nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into	
	cd_setor_atendimento_w,
	vl_custo_pac_w,
	vl_custo_req_w;
exit when C01%notfound;
	begin
	select 	count(*)
	into	qt_existe_w
	from	gsjs_w_custo_pac_dia
	where	nm_usuario = nm_usuario_p
	and	cd_setor_atendimento = cd_setor_atendimento_w;
	
	qt_paciente_w := gsjs_obter_qtd_pac_dia(dt_inicio_p,dt_fim_p,cd_setor_atendimento_w,ie_tipo_atendimento_p,cd_convenio_p);
	
	if (qt_existe_w = 0) then
		insert into	gsjs_w_custo_pac_dia(
			cd_setor_atendimento,
			nm_usuario,
			dt_atualizacao,
			vl_custo_pac,
			vl_custo_req,
			qt_paciente)
		values	(cd_setor_atendimento_w,
			nm_usuario_p,
			sysdate,
			vl_custo_pac_w,
			vl_custo_req_w,
			qt_paciente_w);
	else
		update gsjs_w_custo_pac_dia
		set	vl_custo_pac = (vl_custo_pac + vl_custo_pac_w),
			vl_custo_req = (vl_custo_req + vl_custo_req_w)
		where	cd_setor_atendimento = cd_setor_atendimento_w
		and	nm_usuario = nm_usuario_p;
	end if;
	end;
end loop;
close C01;

commit;
end gsjs_gerar_w_custo_pac_dia;
/