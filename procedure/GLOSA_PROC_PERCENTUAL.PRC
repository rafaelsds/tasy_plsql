create or replace
procedure glosa_proc_percentual(		nr_sequencia_p		number,
					pr_glosa_p		Number,
					cd_convenio_glosa_p	Number,
					cd_categoria_glosa_p	Varchar2,
					vl_proc_original_p		number) IS

vl_total_proc_w			number(15,2) := 0;
cd_estabelecimento_w		number(8);
vl_disponivel_w			Number(15,2);
vl_autorizado_w			Number(15,2);
vl_utilizado_w			Number(15,2);
vl_procedimento_w			Number(15,4);
nr_seq_autorizacao_w		varchar2(10);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10,0);
cd_convenio_w			Number(05,0);
cd_categoria_w			Varchar2(10);
nm_usuario_w			Varchar2(15);
nr_sequencia_w			Number(10,0);
vl_medico_w			Number(15,2);
vl_anestesista_w			Number(15,2);
vl_auxiliares_w			Number(15,2);
vl_custo_operacional_w		Number(15,2);
vl_materiais_w			Number(15,2);
vl_adic_plant_w			Number(15,2);
vl_medico_ww			Number(15,2);
vl_anestesista_ww			Number(15,2);
vl_auxiliares_ww			Number(15,2);
vl_custo_operacional_ww		Number(15,2);
vl_materiais_ww			Number(15,2);
vl_adic_plant_ww			Number(15,2);
vl_procedimento_ww		Number(15,2);
pr_glosa_w			Number(7,4);
cd_convenio_glosa_w		Number(05,0) := null;
cd_categoria_glosa_w		Varchar2(10) := null;
nr_atendimento_w			Number(10);

ie_regra_arredondamento_tx_w	varchar2(01):= 'N';
ie_regra_arred_IPE_w		varchar2(01):= 'N';
ie_tipo_rounded_w			varchar2(01);
ie_vl_cad_glosa_perc_ipe_w		varchar2(01):= 'N';
ie_considerar_adic_hora_ipe_w	varchar2(15) := 'N';

vl_medico_www			Number(15,4);
vl_anestesista_www		Number(15,4);
vl_auxiliares_www			Number(15,4);
vl_custo_operacional_www		Number(15,4);
vl_materiais_www			Number(15,4);
vl_adic_plant_www			Number(15,4);
vl_procedimento_www		Number(15,4);
dt_procedimento_w			date;

tx_adic_medico_w    		number(7,4)		:= 1;
tx_adic_anestesista_w 		number(7,4)		:= 1;
tx_adic_auxiliares_w 		number(7,4)		:= 1;
tx_adic_custo_operacional_w		number(7,4)		:= 1;
tx_adic_materiais_w 		number(7,4)		:= 1;
tx_adic_procedimento_w		number(7,4)		:= 1;
vl_adic_proc_w			number(15,2)		:= 0;
vl_adic_medico_w			number(15,2)		:= 0;
nr_seq_adic_w			number(10,0);
cd_setor_atendimento_w    		number(5,0)  	:= 0;
ie_tipo_atendimento_w     		number(3)    	:= 0;
ie_carater_inter_sus_w		varchar2(2);
dt_conta_w                		date		:= sysdate;
nr_cirurgia_w			number(10)   	:= 0;
ie_carater_cirurgia_w		varchar2(01);
ie_video_w			varchar2(01);
dt_final_cirurgia_w			date;
dt_inicio_cirurgia_w			date;
cd_tipo_acomodacao_w      		number(4);
cd_medico_executor_w      		varchar2(10);
cd_proced_calculo_horario_w		number(15,0);
ie_origem_proced_horario_w		number(10,0);
vl_procedimento_horario_w		number(15,4);
ds_log_w				varchar2(2000);

cd_area_proc_w			number(15);
cd_grupo_proc_w			number(15);
cd_especialidade_w		number(15);
ie_data_adic_hor_w		varchar2(1):= 'N';
dt_adic_hor_w			date;
ie_consid_vl_orig_w		varchar2(15) := 'X';
dt_entrada_w			date;
dt_alta_w				date;
dt_alta_preco_w				date;
dt_agenda_integrada_w		date;
dt_conta_definitiva_w		date;
dt_preco_w			date;
nr_interno_conta_w			number(10) := 0;
nr_prescricao_w			number(10)		:= 0;
ie_regra_data_preco_w		varchar2(1) := 'N';
ie_valor_filme_apos_adic_hor_w	varchar2(15) := 'N';
qt_procedimento_w			number(9,3);
dt_periodo_inicial_w		date;
dt_periodo_final_w		date;
ie_tipo_regra_data_preco_w	varchar2(1);
vl_materiais_tab_w		number(15,2) := 0;
vl_anestesista_tab_w		number(15,2) := 0;
vl_auxiliares_tab_w		number(15,2) := 0;
vl_custo_oper_tab_w		number(15,2) := 0;
vl_procedimento_tab_w		number(15,2) := 0;
vl_medico_tab_w			number(15,2) := 0;
cd_plano_w			varchar2(10);
ie_ajusta_resp_cred_glosa_w	varchar2(1):= 'N';
ie_clinica_w			number(5);
nr_seq_proc_interno_w		number(10);


BEGIN

pr_glosa_w			:= pr_glosa_p;
cd_convenio_glosa_w		:= nvl(cd_convenio_glosa_p,0);
cd_categoria_glosa_w		:= cd_categoria_glosa_p;

select	a.cd_convenio,
	a.cd_categoria,
	a.nr_atendimento,
	nvl(a.vl_procedimento,0),
	a.nr_seq_autorizacao,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.nm_usuario,
	nvl(a.vl_medico,0),
	nvl(a.vl_anestesista,0),
	nvl(a.vl_auxiliares,0),
	nvl(a.vl_custo_operacional,0),
	nvl(a.vl_materiais,0),
	nvl(a.vl_adic_plant,0),
	b.cd_estabelecimento,
	a.dt_procedimento,
	a.cd_setor_atendimento,
	b.ie_tipo_atendimento,
	b.ie_carater_inter_sus,
	nvl(a.dt_conta, nvl(a.dt_prescricao,a.dt_procedimento)),
	a.nr_cirurgia,
	a.ie_video,
	a.cd_medico_executor,
	a.qt_procedimento,
	obter_dado_atend_cat_conv(a.nr_atendimento, a.dt_conta, a.cd_convenio, a.cd_categoria, 'P'),
	b.ie_clinica,
	a.nr_seq_proc_interno
into	cd_convenio_w,
	cd_categoria_w,
	nr_atendimento_w,
	vl_procedimento_w,
	nr_seq_autorizacao_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	nm_usuario_w,
	vl_medico_w,
	vl_anestesista_w,
	vl_auxiliares_w,
	vl_custo_operacional_w,
	vl_materiais_w,
	vl_adic_plant_w,
	cd_estabelecimento_w,
	dt_procedimento_w,
	cd_setor_atendimento_w,
	ie_tipo_atendimento_w,
	ie_carater_inter_sus_w,
	dt_conta_w,
	nr_cirurgia_w,
	ie_video_w,
	cd_medico_executor_w,
	qt_procedimento_w,
	cd_plano_w,
	ie_clinica_w,
	nr_seq_proc_interno_w
from	atendimento_paciente b,
	Procedimento_paciente a
where	a.nr_atendimento	= b.nr_atendimento
and	a.nr_sequencia	= nr_sequencia_p;


begin
select	ie_carater_cirurgia,
	nvl(dt_fim_cirurgia,nvl(dt_termino,dt_conta_w)),
	nvl(dt_inicio_cirurgia, nvl(dt_inicio_real,dt_conta_w))
into	ie_carater_cirurgia_w,
	dt_final_cirurgia_w,
	dt_inicio_cirurgia_w
from	cirurgia
where	nr_cirurgia		= nr_cirurgia_w;
exception
	when others then
      	ie_carater_cirurgia_w 	:= null;
	dt_final_cirurgia_w	:= null;
	dt_inicio_cirurgia_w 	:= null;
end;

begin
select	cd_area_procedimento,
	cd_grupo_proc,
	cd_especialidade
into	cd_area_proc_w,
	cd_grupo_proc_w,
	cd_especialidade_w
from	estrutura_procedimento_v
where	cd_procedimento		= cd_procedimento_w
and	ie_origem_proced	= ie_origem_proced_w;
exception
	when others then
	cd_area_proc_w		:= 0;
	cd_grupo_proc_w		:= 0;
	cd_especialidade_w	:= 0;
	end;

begin
select	dt_entrada,
	nvl(dt_alta,sysdate),
	dt_alta
into	dt_entrada_w,
	dt_alta_w,
	dt_alta_preco_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_w;
exception
	when others then
	dt_entrada_w := null;
	dt_alta_w	:= null;
	dt_alta_preco_w := null;
	end;

begin
select	nr_interno_Conta,
	nvl(nr_prescricao,0)
into	nr_interno_conta_w,
	nr_prescricao_w
from 	procedimento_paciente
where 	nr_sequencia = nr_sequencia_p;
exception
    	when others then
      	nr_interno_conta_w := 0;
	nr_prescricao_w	:= 0;
	end;

begin
select	max(decode(ie_status_acerto,1,nvl(dt_conta_definitiva,sysdate),nvl(dt_conta_definitiva,dt_conta_w))),
	max(dt_periodo_inicial),
	max(dt_periodo_final)
into	dt_conta_definitiva_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w
from 	conta_paciente
where 	nr_interno_conta = nr_interno_conta_w;
exception
    	when others then
      	dt_conta_definitiva_w:= dt_conta_w;
	dt_periodo_inicial_w	:= sysdate;
	dt_periodo_final_w	:= sysdate;
end;

dt_agenda_integrada_w:= dt_conta_w;
if	(nvl(nr_prescricao_w,0) > 0) then

	select	nvl(Obter_Tipo_Regra_Data_Preco('P',cd_convenio_glosa_w),'X')
	into	ie_tipo_regra_data_preco_w
	from	dual;


	if	(ie_tipo_regra_data_preco_w = 'G') then

		begin
		Propaci_Obter_data_ageint(nr_prescricao_w, nr_atendimento_w, cd_procedimento_w, ie_origem_proced_w, dt_conta_w, dt_agenda_integrada_w);
		exception
			when others then
			dt_agenda_integrada_w:= dt_conta_w;
		end;


	end if;
end if;

/* obter o tipo de acomodacao do convenio */
begin
select	obter_tipo_acomod_proc(nr_atendimento_w,nr_sequencia_p)
into	cd_tipo_acomodacao_w
from	dual;
exception
	when others then
		cd_tipo_acomodacao_w 	:= 0;
end;

if	(cd_convenio_glosa_w = 0) then
	begin
	select	nvl(cd_convenio_glosa,0),
		nvl(cd_categoria_glosa,' ')
	into	cd_convenio_glosa_w,
		cd_categoria_glosa_w
	from	atend_categoria_convenio
	where	nr_seq_interno	=
		(select nvl(max(x.nr_seq_interno),0)
			from 	atend_categoria_convenio x
			where	x.nr_atendimento	= nr_atendimento_w
			and	x.cd_convenio	 = cd_convenio_w
			and	x.cd_categoria	 = cd_categoria_w);
	exception
		when others then
			cd_convenio_glosa_w	:= 0;
			cd_categoria_glosa_w	:= ' ';
	end;


	if	(cd_convenio_glosa_w = 0) then
		obter_convenio_particular(cd_estabelecimento_w, cd_convenio_glosa_w,cd_categoria_glosa_w);
		if	(cd_convenio_w = cd_convenio_glosa_w) and /* OS  344765  N�o duplicar o procedimento quando o conv�nio do proc j� for particular */
			(cd_categoria_w = cd_categoria_glosa_w)then
			cd_convenio_glosa_w := 0;
			cd_categoria_glosa_w := ' ';
		end if;
	end if;
end if;

/* ROTINA DE ARREDONDAMENTO, USADO PELO CONV�NIO IPE   --->>    INICIO  <<----- */
if	(cd_convenio_glosa_w <> 0) and
	(nvl(cd_categoria_glosa_w,'X') <> 'X') then
	begin
		begin
	select	nvl(max(ie_regra_arredondamento_tx),'N'),
		nvl(max(ie_vl_cad_glosa_perc_ipe),'N'),
		nvl(max(ie_considerar_adic_hora_ipe),'N'),
		nvl(max(ie_data_adic_hor),'N'),
		nvl(max(ie_ajusta_resp_cred_glosa),'N')
	into	ie_regra_arredondamento_tx_w,
		ie_vl_cad_glosa_perc_ipe_w,
		ie_considerar_adic_hora_ipe_w,
		ie_data_adic_hor_w,
		ie_ajusta_resp_cred_glosa_w
	from	parametro_faturamento
	where	cd_estabelecimento = cd_estabelecimento_W;
	exception
		when others then
			ie_regra_arredondamento_tx_w	:= 'N';
			ie_vl_cad_glosa_perc_ipe_w	:= 'N';
			ie_data_adic_hor_w		:= 'N';
			ie_ajusta_resp_cred_glosa_w	:= 'N';
	end;

	-- Convenio Glosa
	select	nvl(max(ie_valor_original_glosa),'X')
	into	ie_consid_vl_orig_w
	from	convenio_estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	cd_convenio		= cd_convenio_glosa_w ;

	--Convenio
	select	max(ie_arredondamento),
		nvl(max(ie_considera_regra_data_preco),'N'),
		nvl(max(ie_valor_filme_apos_adic_hor),'N')
	into	ie_tipo_rounded_w,
		ie_regra_data_preco_w,
		ie_valor_filme_apos_adic_hor_w
	from	convenio_estabelecimento
	where	cd_convenio	  	= cd_convenio_w
	and	cd_estabelecimento	= cd_estabelecimento_W;
	
	if	(ie_regra_arredondamento_tx_w = 'S') then
		begin

		if	(ie_vl_cad_glosa_perc_ipe_w = 'S') 	and
			(cd_area_proc_w = 3) 		then

			select	nvl(Obter_preco_procedimento2(
					cd_estabelecimento_w,
					cd_convenio_w,
					cd_categoria_w,
					dt_procedimento_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'P'),0)
				into	vl_procedimento_w
				from 	dual;
			
			select	nvl(Obter_preco_procedimento2(
					cd_estabelecimento_w,
					cd_convenio_w,
					cd_categoria_w,
					dt_procedimento_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'M'),0)
				into	vl_medico_w
				from	dual;
			
			select	nvl(Obter_preco_procedimento2(
					cd_estabelecimento_w,
					cd_convenio_w,
					cd_categoria_w,
					dt_procedimento_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'F'),0)
				into	vl_materiais_w
				from 	dual;

			
			select	nvl(Obter_preco_procedimento2(
					cd_estabelecimento_w,
					cd_convenio_w,
					cd_categoria_w,
					dt_procedimento_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'AN'),0)
				into	vl_anestesista_w
				from 	dual;
			
			select	nvl(Obter_preco_procedimento2(
					cd_estabelecimento_w,
					cd_convenio_w,
					cd_categoria_w,
					dt_procedimento_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'AX'),0)
				into	vl_auxiliares_w
				from 	dual;
			
			select	nvl(Obter_preco_procedimento2(
					cd_estabelecimento_w,
					cd_convenio_w,
					cd_categoria_w,
					dt_procedimento_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'C'),0)
			into	vl_custo_operacional_w


			from	dual;
		

			vl_procedimento_w := (qt_procedimento_w * vl_procedimento_w);
			
			/*Solicitada a inclus�o do adicional hor�rio para os procedimentos da �rea 3 - DIAGNOSE E TERAPIA  Hospital Ernesto Dornelles OS 256479*/
			if	(ie_considerar_adic_hora_ipe_w = 'S') and
				(cd_area_proc_w = 3) then
				begin
				select 	decode(ie_data_adic_hor_w, 'S', dt_procedimento_w, dt_conta_w)
				into	dt_adic_hor_w
				from 	dual;

				if (ie_regra_data_preco_w = 'S') then --OS262547
					begin
					select	obter_regra_data_preco('P',cd_convenio_w, dt_entrada_w, dt_conta_w, dt_alta_preco_w, dt_conta_definitiva_w, dt_agenda_integrada_w, dt_periodo_inicial_w, dt_periodo_final_w)
					into	dt_preco_w
					from 	dual;

					dt_adic_hor_w := dt_preco_w;
					end;
				end if;

				define_adicional_horario
					(cd_estabelecimento_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					cd_convenio_w,
					cd_categoria_w,
					cd_setor_atendimento_w,
					ie_tipo_atendimento_w,
					ie_carater_inter_sus_w,
					dt_adic_hor_w,
					ie_carater_cirurgia_w,
					ie_video_w,
					dt_inicio_cirurgia_w,
					dt_final_cirurgia_w,
					cd_tipo_acomodacao_w,
					cd_medico_executor_w,
					cd_plano_w,
					dt_entrada_w,
					tx_adic_medico_w,
					tx_adic_anestesista_w,
					tx_adic_auxiliares_w,
					tx_adic_custo_operacional_w,
					tx_adic_materiais_w,
					tx_adic_procedimento_w,
					vl_adic_proc_w,
					vl_adic_medico_w,
					cd_proced_calculo_horario_w,
					ie_origem_proced_horario_w,
					nr_seq_adic_w,
					ie_clinica_w,
					null,
					nr_seq_proc_interno_w);
					
				if	(nvl(nr_seq_adic_w,0) > 0) then

					if (OBTER_SE_DISPENSA_ADIC_HOR(nr_sequencia_p) = 'S') then

						tx_adic_medico_w 		:= 1;
						tx_adic_anestesista_w 		:= 1;
						tx_adic_auxiliares_w		:= 1;
						tx_adic_custo_operacional_w	:= 1;
						tx_adic_materiais_w		:= 1;
						tx_adic_procedimento_w		:= 1;
						vl_adic_proc_w			:= 0;
						vl_adic_medico_w		:= 0;
						cd_proced_calculo_horario_w 	:= null;
						ie_origem_proced_horario_w	:= null;
						nr_seq_adic_w := null;
						
					end if;
				end if;		

				if	(vl_adic_proc_w is null) then
					vl_adic_proc_w	:= 0;
				end if;

				if	(cd_proced_calculo_horario_w	is null) then
					begin


					if	(nvl(nr_seq_adic_w,0) > 0) then	 --OS 271320

						vl_custo_operacional_w 		:= (tx_adic_custo_operacional_w 	* vl_custo_operacional_w);
						vl_anestesista_w 		:= (tx_adic_anestesista_w 		* vl_anestesista_w);
						vl_medico_w 			:= (tx_adic_medico_w * (nvl(vl_adic_medico_w,0) + vl_medico_w));
						vl_auxiliares_w 		:= (tx_adic_auxiliares_w 		* vl_auxiliares_w);
						vl_materiais_w 			:= (tx_adic_materiais_w 		* vl_materiais_w);
						
						if	(ie_valor_filme_apos_adic_hor_w = 'S') then
							begin
							vl_procedimento_w := vl_procedimento_w - vl_materiais_w;
							vl_procedimento_w := (tx_adic_procedimento_w * (vl_adic_proc_w + vl_procedimento_w));
							vl_procedimento_w := vl_procedimento_w + vl_materiais_w;
							end;
						else
							vl_procedimento_w := (tx_adic_procedimento_w * (vl_adic_proc_w + vl_procedimento_w));
						end if;

						vl_total_proc_w	:= (vl_medico_w + vl_custo_operacional_w + vl_anestesista_w + vl_auxiliares_w + vl_materiais_w);
						if	(vl_total_proc_w > 0) then

							vl_procedimento_w := qt_procedimento_w * vl_total_proc_w;

						end if; 
					end if;
					end;
				else
					begin
					vl_procedimento_horario_w := (tx_adic_procedimento_w * (vl_adic_proc_w + vl_procedimento_w));

					vl_procedimento_w := vl_procedimento_horario_w - vl_procedimento_w;
					end;
				end if;
				end;
			end if;

		end if;

		vl_medico_www		:= ((vl_medico_w		* pr_glosa_w) / 100);
		vl_anestesista_www	:= ((vl_anestesista_w	* pr_glosa_w) / 100);
		vl_auxiliares_www		:= ((vl_auxiliares_w		* pr_glosa_w) / 100);
		vl_custo_operacional_www	:= ((vl_custo_operacional_w 	* pr_glosa_w) / 100);
		vl_materiais_www		:= ((vl_materiais_w		* pr_glosa_w) / 100);
		vl_adic_plant_www		:= ((vl_adic_plant_w		* pr_glosa_w) / 100);
		vl_procedimento_www	:= ((vl_procedimento_w	* pr_glosa_w) / 100);

		if	(ie_tipo_rounded_w = 'R') then
			begin
			select 	obter_regra_arredondamento(cd_convenio_w, cd_categoria_w, cd_procedimento_w, ie_origem_proced_w, cd_estabelecimento_w,
					dt_procedimento_w, 'P', 1)
			into	ie_tipo_rounded_w
			from 	dual;

			ie_regra_arred_IPE_w:= 'S';

			if	(ie_tipo_rounded_w is not null) and
				(ie_regra_arred_IPE_w = 'S') /*and
				(ie_valor_informado_w = 'N')*/ then

				arredondamento(vl_procedimento_www, 2, ie_tipo_rounded_w);
				arredondamento(vl_custo_operacional_www, 2, ie_tipo_rounded_w);
				arredondamento(vl_anestesista_www, 2, ie_tipo_rounded_w);
				arredondamento(vl_medico_www, 2, ie_tipo_rounded_w);
				arredondamento(vl_auxiliares_www, 2, ie_tipo_rounded_w);
				arredondamento(vl_materiais_www, 2, ie_tipo_rounded_w);
				

				vl_medico_ww		:=	vl_medico_www;
				vl_anestesista_ww		:=	vl_anestesista_www;
				vl_auxiliares_ww		:=	vl_auxiliares_www;
				vl_custo_operacional_ww	:=	vl_custo_operacional_www;
				vl_materiais_ww		:=	vl_materiais_www;
				vl_adic_plant_ww		:=	vl_adic_plant_www;
				vl_procedimento_ww	:=	vl_procedimento_www;
				
			else
				begin
				vl_medico_ww		:=	vl_medico_www;
				vl_anestesista_ww		:=	vl_anestesista_www;
				vl_auxiliares_ww		:=	vl_auxiliares_www;
				vl_custo_operacional_ww	:=	vl_custo_operacional_www;
				vl_materiais_ww		:=	vl_materiais_www;
				vl_adic_plant_ww		:=	vl_adic_plant_www;
				vl_procedimento_ww	:=	vl_procedimento_www;
				end;
			end if;
			end;
		else
			begin
			vl_medico_ww		:=	vl_medico_www;
			vl_anestesista_ww		:=	vl_anestesista_www;
			vl_auxiliares_ww		:=	vl_auxiliares_www;
			vl_custo_operacional_ww	:=	vl_custo_operacional_www;
			vl_materiais_ww		:=	vl_materiais_www;
			vl_adic_plant_ww		:=	vl_adic_plant_www;
			vl_procedimento_ww	:=	vl_procedimento_www;
			
			end;
		end if;


		duplicar_proc_paciente(nr_sequencia_p, nm_usuario_w, nr_sequencia_w);

		/*Procedimento original*/
		
		update procedimento_paciente
		set	ie_valor_informado	= 'S',
			vl_medico		= vl_medico_ww,
			vl_anestesista		= vl_anestesista_ww,
			vl_auxiliares		= vl_auxiliares_ww,
			vl_custo_operacional	= vl_custo_operacional_ww,
			vl_materiais		= vl_materiais_ww,
			vl_adic_plant		= vl_adic_plant_ww,
			vl_Procedimento		= vl_procedimento_ww
		where	nr_sequencia		= nr_sequencia_p;

		if	(ie_tipo_rounded_w is not null) and
			(ie_regra_arred_IPE_w = 'S') /*and
			(ie_valor_informado_w = 'N')*/ then
			begin
			vl_medico_www		:= vl_medico_w - vl_medico_ww;
			vl_anestesista_www	:= vl_anestesista_w - vl_anestesista_ww;
			vl_auxiliares_www		:= vl_auxiliares_w - vl_auxiliares_ww;
			vl_custo_operacional_www	:= vl_custo_operacional_w - vl_custo_operacional_ww;
			vl_materiais_www		:= vl_materiais_w - vl_materiais_ww;
			vl_adic_plant_www		:= vl_adic_plant_w - vl_adic_plant_ww;
			vl_procedimento_www	:= vl_Procedimento_w - vl_Procedimento_ww;

			arredondamento(vl_procedimento_www, 2, ie_tipo_rounded_w);
			arredondamento(vl_custo_operacional_www, 2, ie_tipo_rounded_w);
			arredondamento(vl_anestesista_www, 2, ie_tipo_rounded_w);
			arredondamento(vl_medico_www, 2, ie_tipo_rounded_w);
			arredondamento(vl_auxiliares_www, 2, ie_tipo_rounded_w);
			arredondamento(vl_materiais_www, 2, ie_tipo_rounded_w);

			vl_medico_ww			:=	vl_medico_www;
			vl_anestesista_ww		:=	vl_anestesista_www;
			vl_auxiliares_ww		:=	vl_auxiliares_www;
			vl_custo_operacional_ww		:=	vl_custo_operacional_www;
			vl_materiais_ww			:=	vl_materiais_www;
			vl_adic_plant_ww		:=	vl_adic_plant_www;
			vl_procedimento_ww		:=	vl_procedimento_www;
			end;
		else
			begin
			vl_medico_ww		:= vl_medico_w - vl_medico_ww;
			vl_anestesista_ww		:= vl_anestesista_w - vl_anestesista_ww;
			vl_auxiliares_ww		:= vl_auxiliares_w - vl_auxiliares_ww;
			vl_custo_operacional_ww	:= vl_custo_operacional_w - vl_custo_operacional_ww;
			vl_materiais_ww		:= vl_materiais_w - vl_materiais_ww;
			vl_adic_plant_ww		:= vl_adic_plant_w - vl_adic_plant_ww;
			vl_procedimento_ww	:= vl_Procedimento_w - vl_Procedimento_ww;
			end;
		end if;

		if	(ie_vl_cad_glosa_perc_ipe_w = 'S') and
			(cd_area_proc_w = 3) then
			vl_medico_ww		:=	(vl_medico_w 	* (100 - pr_glosa_w) / 100);
			vl_anestesista_ww	:=	(vl_anestesista_w	* (100 - pr_glosa_w) / 100);
			vl_auxiliares_ww	:=	(vl_auxiliares_w	* (100 - pr_glosa_w) / 100);
			vl_custo_operacional_ww	:=	(vl_custo_operacional_w * (100 - pr_glosa_w) / 100);
			vl_materiais_ww		:=	(vl_materiais_w 	* (100 - pr_glosa_w) / 100);
			vl_procedimento_ww	:=	(vl_procedimento_w 	* (100 - pr_glosa_w) / 100);
		end if;

		if	(obter_se_proc_recebe_honorario(nr_sequencia_w,ie_tipo_atendimento_w,cd_convenio_glosa_w) = 'N') then
			begin
			vl_procedimento_ww := ((vl_proc_original_p * pr_glosa_w) / 100);
			end;
		end if;

		/*Procedimento duplicado*/
		update procedimento_paciente
		set	ie_valor_informado	= 'S',
			vl_medico		= vl_medico_ww,
			vl_anestesista		= vl_anestesista_ww,
			vl_auxiliares		= vl_auxiliares_ww,
			vl_custo_operacional	= vl_custo_operacional_ww,
			vl_materiais		= vl_materiais_ww,
			vl_adic_plant		= vl_adic_plant_ww,
			vl_procedimento		= vl_procedimento_ww,
			cd_situacao_glosa	= 8,
			qt_procedimento		= 0,
			cd_convenio		= cd_convenio_glosa_w,
			cd_categoria		= cd_categoria_glosa_w,
			nr_seq_proc_princ	= nr_sequencia_p,
			nr_interno_conta	= null
		where	nr_sequencia		= nr_sequencia_w;

		end;
	/* ROTINA DE ARREDONDAMENTO, USADO PELO CONV�NIO IPE   --->>    FIM  <<----- */
	elsif	(ie_regra_arredondamento_tx_w = 'N') then
		begin
		/*N�o usa arredondamento IPE*/
		
		vl_medico_ww		:= ((vl_medico_w		* pr_glosa_w) / 100);
		vl_anestesista_ww		:= ((vl_anestesista_w	* pr_glosa_w) / 100);
		vl_auxiliares_ww		:= ((vl_auxiliares_w		* pr_glosa_w) / 100);
		vl_custo_operacional_ww	:= ((vl_custo_operacional_w 	* pr_glosa_w) / 100);
		vl_materiais_ww		:= ((vl_materiais_w		* pr_glosa_w) / 100);
		vl_adic_plant_ww		:= ((vl_adic_plant_w		* pr_glosa_w) / 100);
		vl_procedimento_ww	:= ((vl_procedimento_w	* pr_glosa_w) / 100);

		duplicar_proc_paciente(nr_sequencia_p, nm_usuario_w, nr_sequencia_w);

		update procedimento_paciente
		set	ie_valor_informado	= 'S',
			vl_medico		= vl_medico_ww,
			vl_anestesista		= vl_anestesista_ww,
			vl_auxiliares		= vl_auxiliares_ww,
			vl_custo_operacional	= vl_custo_operacional_ww,
			vl_materiais		= vl_materiais_ww,
			vl_adic_plant		= vl_adic_plant_ww,
			vl_Procedimento		= vl_procedimento_ww
		where	nr_sequencia		= nr_sequencia_p;

		update procedimento_paciente
		set	ie_valor_informado		= 'S',
			vl_medico		= vl_medico_w - vl_medico_ww,
			vl_anestesista		= vl_anestesista_w - vl_anestesista_ww,
			vl_auxiliares		= vl_auxiliares_w - vl_auxiliares_ww,
			vl_custo_operacional	= vl_custo_operacional_w - vl_custo_operacional_ww,
			vl_materiais		= vl_materiais_w - vl_materiais_ww,
			vl_adic_plant		= vl_adic_plant_w - vl_adic_plant_ww,
			vl_procedimento		= vl_Procedimento_w - vl_Procedimento_ww,
			cd_situacao_glosa	= 8,
			qt_procedimento		= 0,
			cd_convenio		= cd_convenio_glosa_w,
			cd_categoria		= cd_categoria_glosa_w,
			nr_seq_proc_princ		= nr_sequencia_p,
			nr_interno_conta		= null
		where	nr_sequencia		= nr_sequencia_w;
		end;
	end if;
	
	if	(nvl(ie_ajusta_resp_cred_glosa_w,'N') = 'S') and
		(nvl(nr_sequencia_w,0) > 0)then
		ajustar_resp_cred_proc_glosa(nr_sequencia_w);
	end if;
	
	end;
end if;

/* Elemar - 26/08/03 - Dava erro porque � chamado a partir atualiza_preco_proc_amb que � chamado na trigger da exame_lab_result_item
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
*/

end glosa_proc_percentual;
/
