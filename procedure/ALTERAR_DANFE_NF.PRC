create or replace
procedure alterar_danfe_nf(
		nr_nota_fiscal_p	number,
		nr_danfe_p		varchar2) is
		
begin
if	(nr_nota_fiscal_p is not null) and
	(nr_danfe_p is not null) then
	begin
	
	update	nota_fiscal
	set	nr_danfe	= substr(nr_danfe_p,1,60)
	where	nr_sequencia	= nr_nota_fiscal_p;

	end;
end if;
	
commit;

end alterar_danfe_nf;
/