create or replace
procedure hsl_gravar_hist_indent_pac(	nr_seq_agenda_p number,
					nm_usuario_p Varchar2) is
				
nr_atendimento_w	number(10);
nr_cirurgia_w		number(10);
nr_seq_sala_cir_w	number(10);
ie_processo_p		number(3);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(774219, null, wheb_usuario_pck.get_nr_seq_idioma);--Realizado a reimpress�o do relat�rio (CATE 3350) - "HSL - Identifica��o do Paciente Cir�rgico".
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(774220, null, wheb_usuario_pck.get_nr_seq_idioma);--Realizado a impress�o do relat�rio (CATE 3350) - "HSL - Identifica��o do Paciente Cir�rgico".

begin

if ((nr_seq_agenda_p is not null) and (nr_seq_agenda_p <> 0)) then

	select	max(nr_atendimento),
		max(nr_cirurgia),
		max(nr_seq_sala_cir)
	into	nr_atendimento_w,
		nr_cirurgia_w,
		nr_seq_sala_cir_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;
	
	if ((nr_cirurgia_w is not null) and (nr_cirurgia_w <> 0)) then
	
		select  count(*)
		into	ie_processo_p
		from	atend_hist_checagem_pda	
		where	nr_cirurgia = nr_cirurgia_w
		and	ie_processo = 'IE';
				
		if (ie_processo_p > 0) then
			gravar_historico_pda(nr_atendimento_w,'RE',expressao1_w,nr_cirurgia_w,nr_seq_sala_cir_w,nm_usuario_p);
		else 		
			gravar_historico_pda(nr_atendimento_w,'IE',expressao2_w,nr_cirurgia_w,nr_seq_sala_cir_w,nm_usuario_p);
		end if;
		
	end if;
	
end if;
	
end hsl_gravar_hist_indent_pac;
/
