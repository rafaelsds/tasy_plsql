create or replace
procedure copiar_param_funcao(
	cd_funcao_orig_p	number,
	nr_seq_orig_p	number,
	cd_funcao_dest_p	number,
	nr_seq_dest_p	number) is 

cd_perfil_w		perfil.cd_perfil%type;
dt_atualizacao_w		date;
nm_usuario_w		usuario.nm_usuario%type;
dt_atualizacao_nrec_w	date;
nm_usuario_nrec_w		usuario.nm_usuario%type; 
vl_parametro_w		funcao_param_perfil.vl_parametro%type;
ds_observacao_w		funcao_param_perfil.ds_observacao%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nr_seq_interno_w		number(10);
nm_usuario_param_w	usuario.nm_usuario%type;
ie_tipo_w			varchar2(1);
		
cursor c01 is
select	'E' ie_tipo,
	cd_estabelecimento,
	null cd_perfil,
	null nm_usuario_param,
	vl_parametro,
	ds_observacao,
	nr_sequencia nr_seq_interno,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec	
from	funcao_param_estab
where	cd_funcao = cd_funcao_orig_p
and	nr_seq_param = nr_seq_orig_p
union all
select	'P' ie_tipo,
	cd_estabelecimento,
	cd_perfil,
	null nm_usuario_param,
	vl_parametro,
	ds_observacao,
	nr_seq_interno,
	dt_atualizacao,
	nm_usuario,
	null dt_atualizacao_nrec,
	null nm_usuario_nrec
from	funcao_param_perfil
where	cd_funcao = cd_funcao_orig_p
and	nr_sequencia = nr_seq_orig_p
union all
select	'U' ie_tipo,
	null cd_estabelecimento,
	null cd_perfil,
	nm_usuario_param,
	vl_parametro,
	ds_observacao,
	nr_seq_interno,
	dt_atualizacao,
	nm_usuario,
	null dt_atualizacao_nrec,
	null nm_usuario_nrec
from	funcao_param_usuario
where	cd_funcao = cd_funcao_orig_p
and	nr_sequencia = nr_seq_orig_p;
	
begin

begin
select	vl_parametro
into	vl_parametro_w
from	funcao_parametro
where	cd_funcao = cd_funcao_orig_p
and	nr_sequencia = nr_seq_orig_p;

update	funcao_parametro
set	vl_parametro = vl_parametro_w
where	cd_funcao = cd_funcao_dest_p
and	nr_sequencia = nr_seq_dest_p;
exception
when others then
	null;
end;

delete	funcao_param_estab
where	cd_funcao = cd_funcao_dest_p
and	nr_seq_param = nr_seq_dest_p;

delete	funcao_param_perfil
where	cd_funcao = cd_funcao_dest_p
and	nr_sequencia = nr_seq_dest_p;

delete	funcao_param_usuario
where	cd_funcao = cd_funcao_dest_p
and	nr_sequencia = nr_seq_dest_p;

open c01;
loop
fetch c01 into	
	ie_tipo_w,
	cd_estabelecimento_w,
	cd_perfil_w,
	nm_usuario_param_w,
	vl_parametro_w,
	ds_observacao_w,
	nr_seq_interno_w,
	dt_atualizacao_w,
	nm_usuario_w,
	dt_atualizacao_nrec_w,
	nm_usuario_nrec_w;
exit when c01%notfound;
	begin
	if	(ie_tipo_w = 'E') then
		begin
		insert into funcao_param_estab(
			cd_funcao,
			nr_seq_param,
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			vl_parametro,
			ds_observacao,
			cd_estabelecimento)
		values (	cd_funcao_dest_p,
			nr_seq_dest_p,
			funcao_param_estab_seq.nextval,
			dt_atualizacao_w,
			nm_usuario_w,
			dt_atualizacao_nrec_w,
			nm_usuario_nrec_w,
			vl_parametro_w,
			ds_observacao_w,
			cd_estabelecimento_w);
		end;
	elsif	(ie_tipo_w = 'P') then
		begin
		insert into funcao_param_perfil(
			cd_funcao,
			nr_sequencia,
			cd_perfil,
			dt_atualizacao,
			nm_usuario,
			vl_parametro,
			ds_observacao,
			cd_estabelecimento)
		values (	cd_funcao_dest_p,
			nr_seq_dest_p,
			cd_perfil_w,
			dt_atualizacao_w,
			nm_usuario_w,
			vl_parametro_w,
			ds_observacao_w,
			cd_estabelecimento_w);
		end;
	elsif	(ie_tipo_w = 'U') then
		begin
		insert into funcao_param_usuario(
			cd_funcao,
			nr_sequencia,
			nm_usuario_param,
			dt_atualizacao,
			nm_usuario,
			vl_parametro,
			ds_observacao)
		values (	cd_funcao_dest_p,
			nr_seq_dest_p,
			nm_usuario_param_w,
			dt_atualizacao_w,
			nm_usuario_w,
			vl_parametro_w,
			ds_observacao_w);
		end;
	end if;
	end;
end loop;
close c01;

commit;

end copiar_param_funcao;
/