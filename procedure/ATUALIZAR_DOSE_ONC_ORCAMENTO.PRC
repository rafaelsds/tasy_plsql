create or replace
procedure atualizar_dose_onc_orcamento
                         	(nr_seq_orcamento_p   	number,
				nm_usuario_p		varchar,
				nr_seq_mat_orcamento_p	number default null) is

qt_mg_carboplatina_w		number(15,4); 
cd_pessoa_fisica_w		varchar2(10);
qt_peso_w			number(10,3); 
qt_superficie_corporea_w	number(15,4);
ie_calcula_preenchido_w		varchar2(3);
nr_seq_atendimento_w		number(10);
ie_atualizar_ciclos_w		varchar2(10);

nr_sequencia_w			number(10,0);
ds_dias_aplicacao_w		varchar2(4000);
ds_ciclos_aplicacao_w		orcamento_paciente_mat.ds_ciclos_aplicacao%type;
qt_dias_aplicacao_w		number(10,0);
qt_ciclos_aplicacao_w		number(10,0);
nr_ciclos_w			number(10,0);
cd_estabelecimento_w		number(4,0);
ie_dispara_kit_w		varchar2(1);
cd_material_w			number(6,0);
qt_lancamento_w			number(9,3);
cd_kit_material_w		number(5,0);
ie_regra_qtde_fatur_w		varchar2(1);
cd_convenio_w			number(5,0);
cd_categoria_w			varchar2(10);
qt_original_w			number(9,3);
qt_material_w			number(9,3);
dt_orcamento_w			date;
qt_material_regra_w		number(9,3);

ds_dias_aplic_filtro_w		varchar2(255);
ds_ciclos_aplic_filtro_w	orcamento_paciente.ds_ciclos_aplic_filtro%type;
ds_retorno_w 			varchar2(255);
qt_dias_w 			number(30);
k    				number(10);
p    				number(10);
w    				number(10);
qt_dias_w2 			number(30);
ie_regra_apresent_quimio_w	varchar2(1);

ds_m2_w 			varchar2(30);		
ds_mgc_w 			varchar2(30);
ds_kg_w 			varchar2(30);
ds_mgcar_w 			varchar2(30);

indice_w			number(10,0) := 0;
ds_valido_dias_w		varchar2(255) := '0123456789D,- ';
dia_ciclo_w			varchar2(255) := '';
type campos 			is record (ds_dias_aplicacao varchar2(255));
type vetor 			is table of campos index by binary_integer;
dias_w 				vetor;
dias_vazio_w 			vetor;
dias_filtro_w			vetor;
indice_dias_w			number(10,0) := 0;
posicao_virg_w			number(10,0) := 0;
indice_loop_w			number(10,0) := 0;

qt_dose_w			orcamento_paciente_mat.qt_dose%type;
cd_unid_med_prescr_w		orcamento_paciente_mat.cd_unid_med_prescr%type;

Cursor C01 is
	select	ds_dias_aplicacao,
		ds_ciclos_aplicacao,
		nr_sequencia,
		cd_material,
		qt_original,
		qt_dose,
		cd_unid_med_prescr
	from	orcamento_paciente_mat
	where	nr_sequencia_orcamento = nr_seq_orcamento_p
	and	((nr_seq_mat_orcamento_p is null) or (nvl(nr_sequencia, nr_seq_mat_orcamento_p) = nr_seq_mat_orcamento_p))
	and 	ds_dias_aplicacao is not null
	order by nr_sequencia;

begin

select	max(obter_superficie_corp_red_ped(qt_peso, qt_altura, nvl(qt_redutor_sc,0), cd_pessoa_fisica, nm_usuario)),
	max(b.qt_peso),
	max(qt_mg_carboplatina),
	nvl(max(nr_ciclos),1),
	max(cd_estabelecimento),
	max(cd_convenio),
	max(cd_categoria),
	max(dt_orcamento),
	max(ds_dias_aplic_filtro),
	max(ds_ciclos_aplic_filtro)
into	qt_superficie_corporea_w,
	qt_peso_w,
	qt_mg_carboplatina_w,
	nr_ciclos_w,
	cd_estabelecimento_w,
	cd_convenio_w,
	cd_categoria_w,
	dt_orcamento_w,
	ds_dias_aplic_filtro_w,
	ds_ciclos_aplic_filtro_w
from 	orcamento_paciente b  
where	b.NR_SEQUENCIA_ORCAMENTO = nr_seq_orcamento_p;
	
ie_dispara_kit_w	:= nvl(Obter_valor_param_usuario(106, 111, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');
ie_regra_qtde_fatur_w	:= nvl(Obter_valor_param_usuario(106, 78,obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_w),'N');
ie_regra_apresent_quimio_w:=  nvl(Obter_valor_param_usuario(106, 135,obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_w),'N');

/* Filtro de dias de aplicacao informado no orcamento - INICIO*/
if	(ds_dias_aplic_filtro_w is not null) then

	for indice_w in 1..length(ds_dias_aplic_filtro_w) loop
		dia_ciclo_w	:= substr(upper(ds_dias_aplic_filtro_w), indice_w, 1);
		if	(instr(ds_valido_dias_w, dia_ciclo_w) = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1084612);
		end if;
	end loop;

	while ds_dias_aplic_filtro_w is not null loop
		ds_dias_aplic_filtro_w := substr(ds_dias_aplic_filtro_w, instr(ds_dias_aplic_filtro_w,'D') + 1, length(ds_dias_aplic_filtro_w));		
		posicao_virg_w	:= instr(ds_dias_aplic_filtro_w,'D');
		indice_dias_w := indice_dias_w + 1;
		
		if (posicao_virg_w = 0) then
			dias_filtro_w(indice_dias_w).ds_dias_aplicacao	:= 'D' || somente_numero_real(substr(ds_dias_aplic_filtro_w,1,length(ds_dias_aplic_filtro_w)));
			ds_dias_aplic_filtro_w := '';
		else
			dias_filtro_w(indice_dias_w).ds_dias_aplicacao	:= 'D' || somente_numero_real(substr(ds_dias_aplic_filtro_w,1,posicao_virg_w - 2));
			
			ds_dias_aplic_filtro_w := substr(ds_dias_aplic_filtro_w,posicao_virg_w, length(ds_dias_aplic_filtro_w));
		end if;
		
		if	(instr(dias_filtro_w(indice_dias_w).ds_dias_aplicacao,'D') = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1084612);
		end if;
		indice_loop_w := indice_loop_w + 1;
		if	(indice_loop_w > 100) then
			exit;
		end if;
	end loop;

	qt_dias_w	:= dias_filtro_w.count;
	
	open C01;
	loop
	fetch C01 into	
		ds_dias_aplicacao_w,
		ds_ciclos_aplicacao_w,
		nr_sequencia_w,
		cd_material_w,
		qt_original_w,
		qt_dose_w,
		cd_unid_med_prescr_w;
	exit when C01%notfound;
		begin
		
		for indice_w in 1..length(ds_dias_aplicacao_w) loop
			dia_ciclo_w	:= substr(upper(ds_dias_aplicacao_w), indice_w, 1);
			if	(instr(ds_valido_dias_w, dia_ciclo_w) = 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(1084612);
			end if;
		end loop;
		
		indice_dias_w	:= 0;
		indice_loop_w	:= 0;
		dias_w		:= dias_vazio_w;

		while ds_dias_aplicacao_w is not null loop
			ds_dias_aplicacao_w := substr(ds_dias_aplicacao_w, instr(ds_dias_aplicacao_w,'D') + 1, length(ds_dias_aplicacao_w));		
			posicao_virg_w	:= instr(ds_dias_aplicacao_w,'D');
			indice_dias_w := indice_dias_w + 1;
			
			if (posicao_virg_w = 0) then
				dias_w(indice_dias_w).ds_dias_aplicacao	:= 'D' || somente_numero_real(substr(ds_dias_aplicacao_w,1,length(ds_dias_aplicacao_w)));
				ds_dias_aplicacao_w := '';
			else
				dias_w(indice_dias_w).ds_dias_aplicacao	:= 'D' || somente_numero_real(substr(ds_dias_aplicacao_w,1,posicao_virg_w - 2));
				
				ds_dias_aplicacao_w := substr(ds_dias_aplicacao_w,posicao_virg_w, length(ds_dias_aplicacao_w));
			end if;
			
			if	(instr(dias_w(indice_dias_w).ds_dias_aplicacao,'D')	= 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(1084612);
			end if;
			indice_loop_w := indice_loop_w + 1;
			if	(indice_loop_w > 100) then
				exit;
			end if;
		end loop;
		
		qt_dias_w2	:= dias_w.count;
		ds_retorno_w	:= '';		
				
		for k in 1.. qt_dias_w2 loop
			
			begin
 
			for p in 1.. qt_dias_w loop
				
				begin
				
				if (dias_filtro_w(p).ds_dias_aplicacao = dias_w(k).ds_dias_aplicacao) then
					if (ds_retorno_w is null) then
						ds_retorno_w := dias_filtro_w(p).ds_dias_aplicacao;
					else
						ds_retorno_w := ds_retorno_w || ' ' || dias_filtro_w(p).ds_dias_aplicacao;
					end if;
				end if;
				
				end;
			
			end loop;
			
			end;
		
		end loop;
  
		if	(ds_retorno_w is not null) then
			
			update	orcamento_paciente_mat
			set 	ds_dias_aplic_orig = ds_dias_aplicacao,
				ds_dias_aplicacao  = ds_retorno_w
			where 	nr_sequencia_orcamento = nr_seq_orcamento_p
			and 	nr_sequencia = nr_sequencia_w;
		
		else
		
			delete from orcamento_paciente_mat
			where 	nr_sequencia_orcamento = nr_seq_orcamento_p
			and 	nr_sequencia = nr_sequencia_w;
			
		end if;
		
		end;
	end loop;
	close C01;
	
	commit;
	
end if;
/* Filtro de dias de aplicacao informado no orcamento - FIM*/

if	(ds_ciclos_aplic_filtro_w is not null) and
	(ie_regra_apresent_quimio_w = 'C') then
	atualizar_ciclo_aplic_orc_onc(nr_seq_orcamento_p, nr_seq_mat_orcamento_p, nm_usuario_p);
end if;

/* atualizar todos os medicamentos com dose padrao */
/* atualizar os medicamentos com base na superficie corporal */
ds_m2_w := obter_unid_med_usua('m2');
ds_mgc_w := obter_unid_med_usua('mgc');
ds_kg_w := obter_unid_med_usua('kg');
ds_mgcar_w := obter_unid_med_usua('mgcar');

update 	ORCAMENTO_PACIENTE_MAT a  
set 	a.cd_unid_med_prescr =  
               (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida),
	a.qt_material =  	obter_conversao_unid_med_cons(	a.cd_material, 
								(select b.cd_unidade_med_princ  
								from 	unidade_medida b  
								where 	a.cd_unidade_medida = b.cd_unidade_medida), 
								a.qt_dose * qt_superficie_corporea_w)
where 	a.nr_sequencia_orcamento = nr_seq_orcamento_p
and	((nr_seq_mat_orcamento_p is null) or (nvl(a.nr_sequencia, nr_seq_mat_orcamento_p) = nr_seq_mat_orcamento_p))
and	nvl(a.ie_regra_apresent_onc,'N') <> 'S'
and exists     (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida  
                  and b.cd_unidade_medida <> b.cd_unidade_med_princ  
                  and lower(b.cd_unidade_med_sec) = ds_m2_w)
and	qt_superficie_corporea_w is not null;


/* atualizar os medicamentos com base no peso */
update ORCAMENTO_PACIENTE_MAT a  
set 	a.cd_unid_med_prescr =  
               (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida),
	a.qt_material = 	obter_conversao_unid_med_cons(	a.cd_material, 
								(select b.cd_unidade_med_princ  
								from 	unidade_medida b  
								where 	a.cd_unidade_medida = b.cd_unidade_medida),
								a.qt_dose * qt_peso_w)
where 	a.nr_sequencia_orcamento = nr_seq_orcamento_p
and	((nr_seq_mat_orcamento_p is null) or (nvl(a.nr_sequencia, nr_seq_mat_orcamento_p) = nr_seq_mat_orcamento_p))
and	nvl(a.ie_regra_apresent_onc,'N') <> 'S'
and exists      (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida  
                  and b.cd_unidade_medida <> b.cd_unidade_med_princ  
                  and lower(b.cd_unidade_med_sec) = ds_kg_w)
and	qt_peso_w is not null;


/* pegar o valor da carboplatina */


--qt_mg_carboplatina_w	:= obter_qt_mg_carboplatina(nr_seq_paciente_p);


/* atualizar os medicamentos com base na carboplatina */
update ORCAMENTO_PACIENTE_MAT a  
set 	a.qt_material =   
			obter_conversao_unid_med_cons(	a.cd_material, 
							(select b.cd_unidade_med_princ  
							from 	unidade_medida b  
							where 	a.cd_unidade_medida = b.cd_unidade_medida),
							(select a.qt_dose * nvl(qt_mg_carboplatina_w,1)
							from 	orcamento_paciente b  
							where 	a.nr_sequencia_orcamento = b.nr_sequencia_orcamento)),
        a.cd_unid_med_prescr =  
               (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida)  
where	a.nr_sequencia_orcamento = nr_seq_orcamento_p
and	((nr_seq_mat_orcamento_p is null) or (nvl(a.nr_sequencia, nr_seq_mat_orcamento_p) = nr_seq_mat_orcamento_p))
and	nvl(a.ie_regra_apresent_onc,'N') <> 'S'
and 	exists (select b.cd_unidade_med_princ  
		from unidade_medida b
		where a.cd_unidade_medida = b.cd_unidade_medida
		and b.cd_unidade_medida <> b.cd_unidade_med_princ
		and lower(b.cd_unidade_med_sec) = ds_mgc_w);
		  
		  
update ORCAMENTO_PACIENTE_MAT a  
set 	a.qt_material =   
			obter_conversao_unid_med_cons(	a.cd_material, 
							(select b.cd_unidade_med_princ  
							from 	unidade_medida b  
							where 	a.cd_unidade_medida = b.cd_unidade_medida), 
							(select nvl(qt_mg_carboplatina_w,1)
							from 	orcamento_paciente b  
							where 	a.nr_sequencia_orcamento = b.nr_sequencia_orcamento)),
        a.cd_unid_med_prescr =  
               (select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida) 
where	a.nr_sequencia_orcamento = nr_seq_orcamento_p
and	((nr_seq_mat_orcamento_p is null) or (nvl(a.nr_sequencia, nr_seq_mat_orcamento_p) = nr_seq_mat_orcamento_p))
and	nvl(a.ie_regra_apresent_onc,'N') <> 'S'
and	exists	(select b.cd_unidade_med_princ  
		from unidade_medida b
		where a.cd_unidade_medida = b.cd_unidade_medida
                and b.cd_unidade_medida <> b.cd_unidade_med_princ
                and lower(b.cd_unidade_med_sec) = ds_mgcar_w);

		  
/*update ORCAMENTO_PACIENTE_MAT a  
set 	a.cd_unid_med_prescr 	= (	select	c.cd_unidade_med_princ  
					from	unidade_medida c  
					where	a.cd_unidade_medida = c.cd_unidade_medida),
	a.qt_material	= obter_conversao_unid_med_cons(	a.cd_material, 
								(select	c.cd_unidade_med_princ  
								from	unidade_medida c  
								where	a.cd_unidade_medida = c.cd_unidade_medida), 
								a.qt_dose)
where 	a.nr_sequencia_orcamento = nr_seq_orcamento_p
and	((nr_seq_mat_orcamento_p is null) or (nvl(a.nr_sequencia, nr_seq_mat_orcamento_p) = nr_seq_mat_orcamento_p))
and	exists	(select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida  
                and b.cd_unidade_medida <> b.cd_unidade_med_princ  
                and b.cd_unidade_med_sec = a.cd_unidade_medida)
and	a.qt_dose > 0;*/

-- Retirado OS 393136


update 	ORCAMENTO_PACIENTE_MAT a  
set 	a.cd_unid_med_prescr 	= nvl(a.cd_unidade_medida, a.cd_unid_med_prescr),
	a.qt_material	= obter_conversao_unid_med_cons(a.cd_material, a.cd_unidade_medida, a.qt_dose)
where 	a.nr_sequencia_orcamento = nr_seq_orcamento_p
and	((nr_seq_mat_orcamento_p is null) or (nvl(a.nr_sequencia, nr_seq_mat_orcamento_p) = nr_seq_mat_orcamento_p))
and	nvl(a.ie_regra_apresent_onc,'N') <> 'S'
and not exists	(select b.cd_unidade_med_princ  
                from unidade_medida b  
                where a.cd_unidade_medida = b.cd_unidade_medida  
                and b.cd_unidade_medida <> b.cd_unidade_med_princ  
                and lower(b.cd_unidade_med_sec) in (ds_mgcar_w, ds_m2_w, ds_kg_w, ds_mgc_w )) -- OS 393136, incluido o mgcar
and	a.qt_dose > 0;

open C01;
loop
fetch C01 into	
	ds_dias_aplicacao_w,
	ds_ciclos_aplicacao_w,
	nr_sequencia_w,
	cd_material_w,
	qt_original_w,
	qt_dose_w,
	cd_unid_med_prescr_w;
exit when C01%notfound;
	begin
	
	qt_dias_aplicacao_w	:= obter_qt_dias_aplicacao(ds_dias_aplicacao_w);
	qt_ciclos_aplicacao_w	:= obter_qt_ciclos_aplicacao(ds_ciclos_aplicacao_w);
	
	---Arredondar a QTDE para Superior  (1.125 para 2,    1.79 para 2,     3.54 para 4)
	update	orcamento_paciente_mat
	set 	qt_material = decode(trunc(qt_material), qt_material, qt_material, trunc(qt_material) + 1),
		qt_superficie_corporea = qt_superficie_corporea_w
	where 	nr_sequencia_orcamento = nr_seq_orcamento_p
	and 	nr_sequencia = nr_sequencia_w
	and	nvl(ie_regra_apresent_onc,'N') <> 'S';
	
	update	orcamento_paciente_mat
	set	qt_material = qt_material * nvl(qt_dias_aplicacao_w, qt_material) * nr_ciclos_w
	where 	nr_sequencia_orcamento = nr_seq_orcamento_p
	and 	nr_sequencia = nr_sequencia_w
	and	nvl(ie_regra_apresent_onc,'N') <> 'S';
	
	select 	max(qt_material)
	into	qt_material_w
	from 	orcamento_paciente_mat
	where 	nr_sequencia_orcamento = nr_seq_orcamento_p	
	and 	nr_sequencia = nr_sequencia_w;
	
	if	(nvl(ie_regra_qtde_fatur_w,'N')	= 'S') and (qt_original_w is null) then
		select	Obter_qt_material_fat_orc(
				cd_estabelecimento_w,
				cd_material_w,
				cd_convenio_w,
				cd_categoria_w,
				null,
				qt_material_w,
				null,
				dt_orcamento_w,
				dt_orcamento_w,
				qt_dose_w,
				cd_unid_med_prescr_w) 
		into	qt_material_regra_w
		from	dual;
		
		if	(qt_material_w <> qt_material_regra_w) then
			update	orcamento_paciente_mat
			set 	qt_material = qt_material_regra_w,
				qt_original = qt_material_w
			where 	nr_sequencia_orcamento = nr_seq_orcamento_p
			and 	nr_sequencia = nr_sequencia_w;		
		end if;

	end if;

	if	(ie_regra_apresent_quimio_w <> 'N') then
		Gerar_Apresent_Orcamento_Onc(nr_seq_orcamento_p, nr_sequencia_w, cd_estabelecimento_w, nm_usuario_p);
	end if;
	
	if	(ie_dispara_kit_w = 'S') then
	
		select 	nvl(max(cd_kit_material),0)
		into	cd_kit_material_w
		from 	material_estab 
		where 	cd_material = cd_material_w
		and 	cd_estabelecimento = cd_estabelecimento_w;
				
		if	(cd_kit_material_w	<> 0) then
		
			select 	qt_material
			into	qt_lancamento_w
			from 	orcamento_paciente_mat
			where 	nr_sequencia_orcamento = nr_seq_orcamento_p
			and 	nr_sequencia = nr_sequencia_w;
			
			gerar_kit_material_orc(nr_seq_orcamento_p, qt_lancamento_w, cd_kit_material_w, 0 ,nm_usuario_p,'N', null);
			
		end if;
	
	end if;
		
	end;
end loop;
close C01;

if (nr_seq_mat_orcamento_p is null)  then
	calcular_orcamento_paciente(nr_seq_orcamento_p, nm_usuario_p, cd_estabelecimento_w);
end if;

commit;

end atualizar_dose_onc_orcamento;
/
