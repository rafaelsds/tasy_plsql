create or replace
procedure pls_sip_confirmar_envio(	nr_seq_periodo_p	pls_lote_sip.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

tb_rowid_w	pls_util_cta_pck.t_rowid;
ds_maquina_w	varchar2(80);

-- todos os registros que possuem regra foram enviados para a ANS
cursor c01(	nr_seq_lote_pc	pls_lote_sip.nr_sequencia%type) is
	select	a.rowid
	from	sip_nv_dados a
	where	a.nr_seq_lote_sip = nr_seq_lote_pc
	and	exists (select	1
			from	sip_nv_regra_vinc_it b
			where	b.nr_seq_sip_nv_dados = a.nr_sequencia);

begin

-- informa os registros que foram enviados para a ANS
open c01(nr_seq_periodo_p);
loop
	tb_rowid_w.delete;
	
	fetch c01 bulk collect into tb_rowid_w
	limit pls_util_pck.qt_registro_transacao_w;
	
	exit when tb_rowid_w.count = 0;
	
	forall i in tb_rowid_w.first .. tb_rowid_w.last
		update	sip_nv_dados set
			ie_conta_enviada_ans = 'S'
		where	rowid = tb_rowid_w(i);
	commit;
end loop;
close c01;

select	substr(obter_inf_sessao(0)||' - '||obter_inf_sessao(1),1,80)
into	ds_maquina_w
from	dual;

update	pls_lote_sip
set	nm_usuario_envio	= nm_usuario_p,
	ds_maquina_envio	= ds_maquina_w,
	dt_envio		= sysdate,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_sequencia		= nr_seq_periodo_p;
commit;

end pls_sip_confirmar_envio;
/