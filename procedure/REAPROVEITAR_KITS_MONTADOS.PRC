create or replace
procedure Reaproveitar_kits_montados(		nr_seq_reg_kit_p		number,
					nr_seq_reg_kit_novo_p	number,
					nm_usuario_p		Varchar2,
					cd_estabelecimento_p    	number) is
					
nr_sequencia_w		number(10);					
cd_kit_material_w		number(10);
dt_montagem_w 		date;
nm_usuario_montagem_w	varchar2(15);
nm_usuario_util_w		varchar2(15);
nr_cirurgia_w		number(10);
cd_medico_w		varchar2(10);
nr_prescricao_w		number(14);
nr_atendimento_w		number(10);
cd_local_estoque_w	number(4);
ie_status_w		varchar2(1);
nr_seq_kit_estoque_w	number(10);
cd_material_w		number(6);
qt_material_w		number(11,4);
ie_gerado_barras_w	varchar2(1);
nr_seq_lote_fornec_w	number(10);
nr_seq_motivo_exclusao_w 	number(10);	
nr_seq_item_kit_w		number(10);	
dt_exclusao_w		date;
nm_usuario_exclusao_w	varchar2(15);
cd_material_troca_w	number(6);
nm_usuario_troca_w	varchar2(15);
ds_motivo_troca_w		varchar2(255);
dt_utilizacao_w		date;
nr_seq_kit_est_comp_w	number(10);

cursor c01 is
select	nr_sequencia,
	cd_kit_material,
	dt_montagem, 
	nm_usuario_montagem, 
	nm_usuario_util,
	nr_cirurgia, 
	cd_medico, 
	nr_prescricao, 
	nr_atendimento,
	cd_local_estoque,
	ie_status,
	dt_utilizacao
from	kit_estoque
where	nr_seq_reg_kit  = nr_seq_reg_kit_p ;

cursor c02 is
select	nr_sequencia,
	cd_material,
	qt_material, 
	ie_gerado_barras,
	nr_seq_lote_fornec,
	nr_seq_motivo_exclusao, 
	nr_seq_item_kit, 
	dt_exclusao, 
	nm_usuario_exclusao, 
	cd_material_troca,
	nm_usuario_troca,
	ds_motivo_troca 
from 	kit_estoque_comp 
where 	nr_seq_kit_estoque = nr_seq_kit_estoque_w;	

begin

open C01;
loop
fetch C01 into	
	nr_seq_kit_estoque_w,
	cd_kit_material_w,
	dt_montagem_w,
	nm_usuario_montagem_w,
	nm_usuario_util_w,
	nr_cirurgia_w,
	cd_medico_w,
	nr_prescricao_w,
	nr_atendimento_w,
	cd_local_estoque_w,
	ie_status_w,
	dt_utilizacao_w;
exit when C01%notfound;
	begin
	
	select	kit_estoque_seq.nextval
	into	nr_sequencia_w
	from 	dual;

	insert	into kit_estoque(
		nr_sequencia, 
		cd_kit_material,
		dt_atualizacao, 
		nm_usuario,
		dt_montagem, 
		nm_usuario_montagem, 
		dt_utilizacao, 
		nm_usuario_util,
		nr_cirurgia, 
		cd_estabelecimento, 
		cd_medico, 
		nr_prescricao, 
		nr_atendimento,
		cd_local_estoque,
		nr_seq_reg_kit,
		ie_status)
	values	(nr_sequencia_w, 
		cd_kit_material_w,
		sysdate, 
		nm_usuario_p,
		dt_montagem_w, 
		nm_usuario_montagem_w, 
		null,
		null,
		null, 
		cd_estabelecimento_p, 
		cd_medico_w, 
		null, 
		null,
		cd_local_estoque_w,
		nr_seq_reg_kit_novo_p,
		null);
	end;

	open C02;
	loop
	fetch C02 into	
		nr_seq_kit_est_comp_w,
		cd_material_w,
		qt_material_w,
		ie_gerado_barras_w,
		nr_seq_lote_fornec_w,
		nr_seq_motivo_exclusao_w,
		nr_seq_item_kit_w,
		dt_exclusao_w,
		nm_usuario_exclusao_w,
		cd_material_troca_w,
		nm_usuario_troca_w,
		ds_motivo_troca_w;
	exit when C02%notfound;
		begin
				
		insert into kit_estoque_comp (	
			nr_seq_kit_estoque, 
			nr_sequencia, 
			cd_material,
			dt_atualizacao, 
			nm_usuario,
			qt_material, 
			ie_gerado_barras,
			nr_seq_lote_fornec,
			nr_seq_motivo_exclusao, 
			nr_seq_item_kit, 
			dt_exclusao, 
			nm_usuario_exclusao, 
			cd_material_troca,
			nm_usuario_troca,
			ds_motivo_troca) 
		values( nr_sequencia_w,
			nr_seq_kit_est_comp_w,
			cd_material_w,
			sysdate,
			nm_usuario_p,
			qt_material_w,
			ie_gerado_barras_w,
			nr_seq_lote_fornec_w,
			nr_seq_motivo_exclusao_w,
			nr_seq_item_kit_w,
			dt_exclusao_w,
			nm_usuario_exclusao_w,
			cd_material_troca_w,
			nm_usuario_troca_w,
			ds_motivo_troca_w);
		end;
	end loop;
	close C02;
	
end loop;
close C01;

	
commit;

end Reaproveitar_kits_montados;
/