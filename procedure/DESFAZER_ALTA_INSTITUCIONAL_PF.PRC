create or replace
procedure desfazer_alta_institucional_pf(	cd_pessoa_fisica_p	varchar2,
						nm_usuario_p		varchar2) is 
begin
if	(cd_pessoa_fisica_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update 	pessoa_fisica 
	set 	dt_alta_institucional = null,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
	end;
end if;
commit;

end desfazer_alta_institucional_pf;
/