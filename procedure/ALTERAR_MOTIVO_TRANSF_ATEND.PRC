create or replace
procedure alterar_motivo_transf_atend( nr_seq_motivo_transf_p	varchar2,
					nr_seq_motivo_perm_p		varchar2,
					nr_seq_interno_p	number) is 

begin

if	((nvl(nr_seq_motivo_transf_p,0) > 0) or
	(nvl(nr_seq_motivo_perm_p,0) > 0)) then
	
	update	atend_paciente_unidade
	set	nr_seq_motivo_transf = nvl(nr_seq_motivo_transf_p, nr_seq_motivo_transf),
		nr_seq_motivo_perm   = nvl(nr_seq_motivo_perm_p, nr_seq_motivo_perm)
	where	nr_seq_interno 	     = nr_seq_interno_p;
	
end if;

commit;

end Alterar_motivo_transf_atend;
/