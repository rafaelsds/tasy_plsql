create or replace
procedure deletar_orcamento_servico(	nr_atendimento_p	atendimento_paciente.nr_atendimento%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

begin

delete	
from	w_orcamento_servico 
where	nm_usuario = nm_usuario_p
and	nr_atendimento = nr_atendimento_p;

commit;

end deletar_orcamento_servico;
/