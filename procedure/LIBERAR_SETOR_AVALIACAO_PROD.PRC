create or replace
procedure liberar_setor_avaliacao_prod(
				nr_seq_mat_aval_p	number,
				nm_usuario_p		varchar2) is 

begin
update	mat_avaliacao
set	dt_liberacao_setor	= sysdate
where	nr_sequencia		= nr_seq_mat_aval_p;

commit;

end liberar_setor_avaliacao_prod;
/