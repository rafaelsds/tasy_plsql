create or replace
procedure pls_gerar_hist_solic_mat_met
			(	ds_observacao_p		varchar2,
				nr_seq_solicitacao_p	number,
				ie_tipo_historico_p	number,
				nm_usuario_p		Varchar2) is 

begin

insert	into pls_solic_lib_historico
	(nr_sequencia, nr_seq_solic_lib, dt_atualizacao,
	 nm_usuario, ds_observacao, dt_historico,
	 ie_tipo_historico)
values	(pls_solic_lib_historico_seq.NextVal, nr_seq_solicitacao_p, sysdate,
	 nm_usuario_p, ds_observacao_p, sysdate,
	 ie_tipo_historico_p);

commit;

end pls_gerar_hist_solic_mat_met;
/
