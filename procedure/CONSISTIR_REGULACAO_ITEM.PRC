create or replace
procedure Consistir_Regulacao_Item( 	nm_usuario_p			varchar2,
										nr_atendimento_p		number,
										cd_pessoa_fisica_p		varchar2,
										nr_sequencia_regra_p	number,
										ie_tipo_p				varchar2,
										ie_apresentar_p			out varchar2) is 


customSQL_w			varchar2(4000);
nr_seq_exame_w		number(10);
nr_seq_exame_lab_w 	number(10);
cd_material_exame_w	varchar2(20);
cd_procedimento_w	number(15);
nr_seq_proc_int_sus_w 	number(10);
ie_origem_proced_w	number(10);

ie_exige_justi_w	varchar2(1);
nr_seq_aval_w		number(10);
ds_mensagem_w		varchar2(255);
nr_seq_item_pos_w	number(10);
ds_sql_w			varchar2(4000);
ds_titulo_w			varchar2(150);
ds_documentacao_w	varchar2(255);
ie_resultado_w		varchar2(80);
nr_seq_regulacao_w	number(10);

nr_seq_consistencia_w	number(10);

Cursor C01 is
		select	max(a.nr_seq_exame),			
				max(a.nr_seq_exame_lab),
				max(a.cd_material_exame),
				max(a.cd_procedimento),
				max(nvl(a.nr_seq_proc_int_sus,a.nr_proc_interno)),
				max(a.ie_origem_proced)
		from	pedido_exame_externo_item a,
				pedido_exame_externo b
		where	b.nr_sequencia = a.nr_seq_pedido
		and		b.nr_sequencia = nr_sequencia_regra_p;

			
begin

Delete from consist_regulacao_itens
where   nm_usuario = nm_usuario_p
and		nr_atendimento = nr_atendimento_p;

commit;

if (ie_tipo_p = 'SE') then

	ie_apresentar_p := 'N';

	open C01;
	loop
	fetch C01 into	
		nr_seq_exame_w,		
		nr_seq_exame_lab_w, 		
		cd_material_exame_w,	
		cd_procedimento_w,	
		nr_seq_proc_int_sus_w,
		ie_origem_proced_w;
	exit when C01%notfound;
		begin
			obter_regras_regulacao_exame(nr_seq_exame_w, nr_seq_exame_lab_w, nr_seq_proc_int_sus_w, cd_procedimento_w, ie_origem_proced_w, cd_material_exame_w, nr_atendimento_p, ie_exige_justi_w, nr_seq_aval_w, ds_mensagem_w, nr_seq_item_pos_w, ds_sql_w, ds_titulo_w, ds_documentacao_w);
			
				customSQL_w :=  ' select nvl(max(''S''), ''N'') ' ||
						  ' from  pessoa_fisica a'     ||
						  ' where  a.cd_pessoa_fisica = :cd_pessoa_fisica  '   ||
						ds_sql_w;

				execute immediate customSQL_w
				into   ie_resultado_w
				using  cd_pessoa_fisica_p;
				
				if (ie_resultado_w = 'N') then
					
					ie_apresentar_p := 'S';
					
					insert into consist_regulacao_itens(
						nr_sequencia,
						dt_atualizacao, 
						nm_usuario_nrec,
						nm_usuario,
						nr_atendimento,
						nr_seq_origem,	
						ds_inconsistencia,	
						ds_documentacao)		
					values(
						consist_regulacao_itens_seq.nextval,
						sysdate, 
						nm_usuario_p,
						nm_usuario_p,
						nr_atendimento_p,
						nr_sequencia_regra_p,
						nvl(ds_titulo_w,' '),
						ds_documentacao_w);
				end if;
		end;
	end loop;
	close C01;

end if;

commit;

end Consistir_Regulacao_Item;
/