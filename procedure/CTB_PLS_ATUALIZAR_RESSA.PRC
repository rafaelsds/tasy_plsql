create or replace
procedure ctb_pls_atualizar_ressa
			(	nr_seq_processo_p		Number,
				nm_usuario_p			Varchar2,
				cd_estabelecimento_p		Number,
				nr_seq_proc_conta_p 		Number,
				nr_seq_competencia_p 		Number,
				nr_seq_conta_competencia_p	Number,
				qt_movimento_p	in out		Number,
				nr_seq_atualizacao_p		Number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
-------------------------------------------------------------------------------------------------------------------
Referencias:
	PLS_ATUALIZAR_CONTA_CONTABIL
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_classificacao_item_w		varchar2(30);
cd_conta_deferido_w		varchar2(20);
ie_tipo_contratacao_w		varchar2(15);
ie_segmentacao_w		varchar2(3);
ie_tipo_segurado_w		varchar2(3);
ie_tipo_beneficiario_w		varchar2(3);
ie_preco_w			varchar2(2);
ie_regulamentacao_w		varchar2(2);
ie_tipo_w			varchar2(2);
ie_participacao_w		varchar2(1);
cd_historico_w			number(20);
qt_movimento_w			number(10);
nr_seq_conta_w			number(10);
nr_seq_regra_w			number(10);
cd_historico_deferido_w		number(10);
nr_seq_contrato_w		number(10);
qt_esquema_ativo_w		number(10);
ie_tipo_movimento_w		number(5);
dt_referencia_w			date;
dt_referencia_ww		date;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
ie_esquema_contabil_w		pls_parametro_contabil.ie_esquema_contabil%type;
nr_seq_esquema_w		pls_esquema_contabil.nr_sequencia%type;
cd_historico_padrao_w		pls_esquema_contabil.cd_historico_padrao%type;
ie_codificacao_w		pls_esquema_contabil_seg.ie_codificacao%type;
vl_fixo_w			pls_esquema_contabil_seg.vl_fixo%type;
cd_conta_contabil_w		pls_esquema_contabil_seg.cd_conta_contabil%type;
ie_debito_credito_w		pls_esquema_contabil_seg.ie_debito_credito%type;
ds_mascara_w			pls_esquema_contabil_seg.ds_mascara%type;
cd_classificacao_credito_w	conta_contabil.cd_classificacao_atual%type;
cd_classificacao_debito_w	conta_contabil.cd_classificacao_atual%type;
cd_conta_credito_w		conta_contabil.cd_conta_contabil%type;
cd_conta_debito_w		conta_contabil.cd_conta_contabil%type;



Cursor C01 is
	select	b.nr_sequencia,
		d.ie_tipo_contratacao,
		d.ie_preco,
		d.ie_segmentacao,
		d.ie_regulamentacao,
		d.ie_participacao,
		c.nr_seq_contrato,
		--substr(pls_obter_dados_contrato(c.nr_seq_contrato,'TB'),1,3),
		trunc(a.dt_processo,'month'),
		c.ie_tipo_segurado,
		c.nr_sequencia
	from	pls_segurado		c,
		pls_plano		d,
		pls_processo_conta	b,
		pls_processo		a
	where	a.nr_sequencia		= b.nr_seq_processo
	and	b.nr_seq_segurado	= c.nr_sequencia(+)
	and	c.nr_seq_plano		= d.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_processo_p
	and	b.nr_sequencia 		= nvl(nr_seq_proc_conta_p, b.nr_sequencia);

cursor c_contas is
	select	d.nr_sequencia nr_seq_conta_comp,
		null nr_seq_processo_conta,
		f.nr_sequencia nr_seq_plano,
		c.nr_seq_contrato,
		c.ie_tipo_segurado,
		trunc(a.dt_processo,'month') dt_referencia,
		f.ie_tipo_contratacao,
		f.ie_preco,
		f.ie_segmentacao,
		f.ie_regulamentacao,
		nvl(d.vl_provisao, 0) vl_provisao,
		nvl(d.vl_ajuste, 0) vl_ajuste,
		0 vl_deferido,
		0 vl_ressarcir,
		b.ie_status_pagamento
	from	pls_plano		f,
		pls_processo_competencia e,
		pls_processo_contas_comp d,
		pls_segurado		c,
		pls_processo_conta	b,
		pls_processo		a
	where	a.nr_sequencia		= b.nr_seq_processo
	and	b.nr_seq_segurado	= c.nr_sequencia(+)
	and	d.nr_seq_conta		= b.nr_sequencia
	and	d.nr_seq_competencia	= e.nr_sequencia
	and	c.nr_seq_plano		= f.nr_sequencia(+)
	and	d.ie_tipo_movimentacao	= 'P'
	and	e.nr_sequencia 		= nr_seq_competencia_p
	and	d.nr_sequencia		= nvl(nr_seq_conta_competencia_p, d.nr_sequencia)
	union all
	select	null nr_seq_conta_comp,
		b.nr_sequencia nr_seq_processo_conta,
		f.nr_sequencia nr_seq_plano,
		c.nr_seq_contrato,
		c.ie_tipo_segurado,
		trunc(a.dt_processo,'month') dt_referencia,
		f.ie_tipo_contratacao,
		f.ie_preco,
		f.ie_segmentacao,
		f.ie_regulamentacao,
		0 vl_provisao,
		0 vl_ajuste,
		nvl(b.vl_deferido, 0),
		nvl(b.vl_ressarcir, 0),
		b.ie_status_pagamento
	from	pls_plano		f,
		pls_segurado		c,
		pls_processo_conta	b,
		pls_processo		a
	where	a.nr_sequencia		= b.nr_seq_processo
	and	b.nr_seq_segurado	= c.nr_sequencia(+)
	and	c.nr_seq_plano		= f.nr_sequencia(+)
	and	a.nr_sequencia 		= nr_seq_processo_p
	and	b.nr_sequencia 		= nvl(nr_seq_proc_conta_p, b.nr_sequencia);

vet_contas c_contas%rowtype;

cursor c_tipo_movimento is
select 	28
from	dual
where	nvl(vet_contas.nr_seq_conta_comp, 0) <> 0
union all
select	29
from	dual
where	nvl(vet_contas.nr_seq_processo_conta, 0) <> 0
union all
select	30
from 	dual
where	nvl(vet_contas.nr_seq_processo_conta, 0) <> 0
union all
select 31
from	dual
where	vet_contas.vl_ajuste >= 0
and	nvl(vet_contas.nr_seq_conta_comp, 0) <> 0
union all
select	32
from	dual
where	vet_contas.vl_ajuste < 0
and	nvl(vet_contas.nr_seq_conta_comp, 0) <> 0;

cursor c_esquema is
	select	nr_sequencia,
		cd_historico_padrao
	from	pls_esquema_contabil
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_tipo_regra		= 'R'
	and	nvl(ie_tipo_movimentacao,28) = ie_tipo_movimento_w
	and	vet_contas.dt_referencia between dt_inicio_vigencia and nvl(dt_fim_vigencia,vet_contas.dt_referencia)
	and	((ie_tipo_segurado = vet_contas.ie_tipo_segurado) or (ie_tipo_segurado is null))
	and	((nr_seq_contrato = vet_contas.nr_seq_contrato) or (nr_seq_contrato is null))
	and	((ie_status_pagamento = vet_contas.ie_status_pagamento) or (ie_status_pagamento is null))
	order by
		nvl(ie_tipo_segurado,' '),
		nvl(nr_seq_contrato,0),
		nvl(ie_status_pagamento, ' '),
		nvl(dt_inicio_vigencia,sysdate);

Cursor c_segmentacao is
	select	ie_codificacao,
		vl_fixo,
		cd_conta_contabil,
		ie_debito_credito,
		ds_mascara
	from	pls_esquema_contabil_seg
	where	nr_seq_regra_esquema	= nr_seq_esquema_w
	order by
		ie_debito_credito,
		nr_seq_apresentacao;
	
begin

select	nvl(max(ie_esquema_contabil), 'N')
into	ie_esquema_contabil_w
from	pls_parametro_contabil
where	cd_estabelecimento = cd_estabelecimento_p;

qt_movimento_w := qt_movimento_p;

qt_esquema_ativo_w := 0;
if 	(nr_seq_processo_p is not null) then
	select	a.dt_processo
	into	dt_referencia_ww
	from	pls_processo a
	where	nr_sequencia = nr_seq_processo_p;

	select	count(1)
	into	qt_esquema_ativo_w
	from	pls_esquema_contabil
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_tipo_regra		= 'R'
	and	dt_referencia_ww between dt_inicio_vigencia and nvl(dt_fim_vigencia,dt_referencia_ww);
end if;

/* 
Se o nr_seq_conta_competencia_p ou nr_seq_competencia_p nao for nulo, significa que esta atualizando um item de percentual historico.
Percentual historico so e atualizado por esquema contabil.
*/
if 	(qt_esquema_ativo_w = 0 and (nr_seq_conta_competencia_p is not null or nr_seq_competencia_p is not null)) then
	qt_esquema_ativo_w := 1;
end if;

/* 
	Se nao estiver marcada a checkbox "Utilizar esquema contabil", ou se nao houver esquema contabil ativo, utiliza regras contabeis
*/
if	((ie_esquema_contabil_w = 'N' or qt_esquema_ativo_w = 0)) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_conta_w,
		ie_tipo_contratacao_w,
		ie_preco_w,
		ie_segmentacao_w,
		ie_regulamentacao_w,
		ie_participacao_w,
		nr_seq_contrato_w,
		--ie_tipo_beneficiario_w,
		dt_referencia_w,
		ie_tipo_segurado_w,
		nr_seq_segurado_w;
	exit when C01%notfound;
		begin
		begin
		ie_tipo_beneficiario_w	:= substr(pls_obter_dados_contrato(nr_seq_contrato_w, 'TB'), 1, 3);
		exception
		when others then
			ie_tipo_beneficiario_w	:= null;
		end;
		
		if	(nvl(nr_seq_segurado_w, 0) = 0) then
			begin
			
			ie_tipo_segurado_w:= 'D';
			
			end;
		end if;
		
		ctb_pls_obter_conta_ressa(	cd_estabelecimento_p,
						dt_referencia_w,
						'C',
						ie_tipo_contratacao_w,
						ie_preco_w,
						ie_segmentacao_w,
						ie_regulamentacao_w,
						ie_participacao_w,
						ie_tipo_beneficiario_w,
						ie_tipo_segurado_w,
						nr_seq_regra_w,
						cd_historico_w,
						cd_conta_contabil_w,
						cd_historico_deferido_w,
						cd_conta_deferido_w);
			
		if	(to_number(nr_seq_regra_w) > 0) then
			update	pls_processo_conta
			set	cd_conta_cred		= cd_conta_contabil_w,
				nr_seq_regra_ctb_cred	= nr_seq_regra_w,
				cd_historico		= cd_historico_w,
				cd_historico_deferido	= cd_historico_deferido_w,
				cd_conta_deferido_cred	= cd_conta_deferido_w
			where	nr_sequencia		= nr_seq_conta_w;
		end if;
			
		ctb_pls_obter_conta_ressa(	cd_estabelecimento_p,
						dt_referencia_w,
						'D',
						ie_tipo_contratacao_w,
						ie_preco_w,
						ie_segmentacao_w,
						ie_regulamentacao_w,
						ie_participacao_w,
						ie_tipo_beneficiario_w,
						ie_tipo_segurado_w,
						nr_seq_regra_w,
						cd_historico_w,
						cd_conta_contabil_w,
						cd_historico_deferido_w,
						cd_conta_deferido_w);
			
		if	(to_number(nr_seq_regra_w) > 0) then
			update	pls_processo_conta
			set	cd_conta_deb		= cd_conta_contabil_w,
				nr_seq_regra_ctb_deb	= nr_seq_regra_w,
				cd_historico		= cd_historico_w,
				cd_historico_deferido	= cd_historico_deferido_w,
				cd_conta_deferido_deb	= cd_conta_deferido_w
			where	nr_sequencia		= nr_seq_conta_w;
		end if;
		
		end;
	end loop;
	close C01;
	/* Quando a atualizacao for de um item de percentual historico, deve entrar na atualizacao por esquema mesmo que use regras contabeis.*/
elsif 	(ie_esquema_contabil_w = 'S' or qt_esquema_ativo_w > 0) then
	open c_contas;
	loop
	fetch c_contas into
		vet_contas;
	exit when c_contas%notfound;
		begin
		open c_tipo_movimento;
		loop
		fetch c_tipo_movimento into
			ie_tipo_movimento_w;
		exit when c_tipo_movimento%notfound;
			begin
						
			cd_classificacao_credito_w	:= null;
			cd_classificacao_debito_w	:= null;
			nr_seq_esquema_w		:= null;
			cd_historico_padrao_w		:= null;
			cd_conta_credito_w	:= null;
			cd_conta_debito_w	:= null;


			open c_esquema;
			loop
			fetch c_esquema into
				nr_seq_esquema_w,
				cd_historico_padrao_w;
			exit when c_esquema%notfound;
			end loop;
			close c_esquema;

			open c_segmentacao;
			loop
			fetch c_segmentacao into	
				ie_codificacao_w,
				vl_fixo_w,
				cd_conta_contabil_w,
				ie_debito_credito_w,
				ds_mascara_w;
			exit when c_segmentacao%notfound;
				begin
				cd_classificacao_item_w	:= null;
			
				if	(ie_debito_credito_w = 'C') then /* Classificacao CREDITO */
					if	(ie_codificacao_w = 'CR') then /* Codigo reduzido */
						select	max(cd_classificacao_atual)
						into	cd_classificacao_credito_w
						from	conta_contabil
						where	cd_conta_contabil	= cd_conta_contabil_w;
						
						cd_conta_credito_w	:= cd_conta_contabil_w;
					elsif	(ie_codificacao_w = 'FX') then /* Fixo */
						cd_classificacao_item_w	:= vl_fixo_w;
					elsif	(ie_codificacao_w = 'RC') then /* Tipo de contratacao / Regulamentacao */
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(vet_contas.ie_tipo_contratacao,vet_contas.ie_regulamentacao);
					elsif	(ie_codificacao_w = 'FP') then /* Formacao de Preco */
						if	(vet_contas.ie_preco in ('1','2','3')) then
							cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_formacao_preco(vet_contas.ie_preco);
						else
							cd_classificacao_item_w	:= 'FP';
						end if;
					elsif	(ie_codificacao_w = 'TC') then /* Tipo de contratacao */
						if	(vet_contas.ie_tipo_contratacao in ('I','CE','CA')) then
							cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_contratacao(vet_contas.ie_tipo_contratacao);
						else
							cd_classificacao_item_w	:= 'TC';
						end if;
					elsif	(ie_codificacao_w = 'R') then /* Regulamentacao */
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_regulamentacao(vet_contas.ie_regulamentacao);
					elsif	(ie_codificacao_w = 'S') then /* Segmentacao */
						cd_classificacao_item_w	:= lpad(vet_contas.ie_segmentacao,2,'0');
					end if;

					if	(cd_classificacao_item_w is not null) then
						if	(ds_mascara_w = '00') then
							cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
						elsif	(ds_mascara_w = '0.0') then
							cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
						elsif	(ds_mascara_w = '0_') then
							cd_classificacao_item_w	:= cd_classificacao_item_w;
						else
							cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
						end if;
						
						cd_classificacao_credito_w	:= cd_classificacao_credito_w || cd_classificacao_item_w;
					end if;
				elsif	(ie_debito_credito_w = 'D') then /* Classificacao DEBITO */
					if	(ie_codificacao_w = 'CR') then /* Codigo reduzido */
						select	max(cd_classificacao_atual)
						into	cd_classificacao_debito_w
						from	conta_contabil
						where	cd_conta_contabil	= cd_conta_contabil_w;
						
						cd_conta_debito_w	:= cd_conta_contabil_w;
					elsif	(ie_codificacao_w = 'FX') then /* Fixo */
						cd_classificacao_item_w	:= vl_fixo_w;
					elsif	(ie_codificacao_w = 'RC') then /* Tipo de contratacao / Regulamentacao */
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_contratacao_regulamentacao(vet_contas.ie_tipo_contratacao,vet_contas.ie_regulamentacao);
					elsif	(ie_codificacao_w = 'FP') then /* Formacao de Preco */
						if	(vet_contas.ie_preco in ('1','2','3')) then
							cd_classificacao_item_w	:= pls_atualizar_codificacao_pck.get_formacao_preco(vet_contas.ie_preco);
						else
							cd_classificacao_item_w	:= 'FP';
						end if;
					elsif	(ie_codificacao_w = 'TC') then /* Tipo de contratacao */
						if	(vet_contas.ie_tipo_contratacao in ('I','CE','CA')) then
							cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_tipo_contratacao(vet_contas.ie_tipo_contratacao);
						else
							cd_classificacao_item_w	:= 'TC';
						end if;
					elsif	(ie_codificacao_w = 'R') then /* Regulamentacao */
						cd_classificacao_item_w := pls_atualizar_codificacao_pck.get_regulamentacao(vet_contas.ie_regulamentacao);
					elsif	(ie_codificacao_w = 'S') then /* Segmentacao */
						cd_classificacao_item_w	:= lpad(vet_contas.ie_segmentacao,2,'0');
					end if;

					if	(cd_classificacao_item_w is not null) then
						if	(ds_mascara_w = '00') then
							cd_classificacao_item_w	:= lpad(cd_classificacao_item_w,2,'0') || '.';
						elsif	(ds_mascara_w = '0.0') then
							cd_classificacao_item_w	:= substr(lpad(cd_classificacao_item_w,2,'0'),1,1) ||'.'||substr(lpad(cd_classificacao_item_w,2,'0'),2,1) || '.';
						elsif	(ds_mascara_w = '0_') then
							cd_classificacao_item_w	:= cd_classificacao_item_w;
						else
							cd_classificacao_item_w	:= cd_classificacao_item_w || '.';
						end if;
						
						cd_classificacao_debito_w	:= cd_classificacao_debito_w || cd_classificacao_item_w;
					end if;
				end if;
				end;
			end loop;
			close c_segmentacao;

			/* Remover o ultimo ponto da classificacao */
			if	(substr(cd_classificacao_credito_w,length(cd_classificacao_credito_w),length(cd_classificacao_credito_w)) = '.') then
				cd_classificacao_credito_w	:= substr(cd_classificacao_credito_w,1,length(cd_classificacao_credito_w)-1);
			end if;
			
			if	(substr(cd_classificacao_debito_w,length(cd_classificacao_debito_w),length(cd_classificacao_debito_w)) = '.') then
				cd_classificacao_debito_w	:= substr(cd_classificacao_debito_w,1,length(cd_classificacao_debito_w)-1);
			end if;

			if	(cd_conta_credito_w is null) then
				cd_conta_credito_w	:= ctb_obter_conta_classif(cd_classificacao_credito_w,vet_contas.dt_referencia,cd_estabelecimento_p);
			end if;

			if	(cd_conta_debito_w is null) then
				cd_conta_debito_w	:= ctb_obter_conta_classif(cd_classificacao_debito_w,vet_contas.dt_referencia,cd_estabelecimento_p);
			end if;
			
			if	(ie_tipo_movimento_w = 28) then
				ie_tipo_w	:= 'PR';

				update	pls_processo_contas_comp
				set	cd_conta_cred_prov		= cd_conta_credito_w,
					cd_conta_deb_prov		= cd_conta_debito_w,
					nr_seq_esquema_prov		= nr_seq_esquema_w,
					cd_historico_prov		= cd_historico_padrao_w,
					cd_classif_cred_prov		= cd_classificacao_credito_w,
					cd_classif_deb_prov		= cd_classificacao_debito_w
				where	nr_sequencia			= vet_contas.nr_seq_conta_comp;

				qt_movimento_w := qt_movimento_w + 1;
			elsif	(ie_tipo_movimento_w = 29) then
				ie_tipo_w	:= 'DF';

				update	pls_processo_conta
				set	cd_conta_deferido_cred		= cd_conta_credito_w,
					cd_conta_deferido_deb		= cd_conta_debito_w,
					nr_seq_esquema_def		= nr_seq_esquema_w,
					cd_historico_deferido		= cd_historico_padrao_w,
					cd_classif_deferido_cred	= cd_classificacao_credito_w,
					cd_classif_deferido_deb		= cd_classificacao_debito_w
				where	nr_sequencia			= vet_contas.nr_seq_processo_conta;

				qt_movimento_w := qt_movimento_w + 1;
			elsif	(ie_tipo_movimento_w = 30) then
				ie_tipo_w	:= 'RE';

				update	pls_processo_conta
				set	cd_conta_cred			= cd_conta_credito_w,
					cd_conta_deb			= cd_conta_debito_w,
					nr_seq_esquema_ressarc		= nr_seq_esquema_w,
					cd_historico			= cd_historico_padrao_w,
					cd_classif_cred			= cd_classificacao_credito_w,
					cd_classif_deb			= cd_classificacao_debito_w
				where	nr_sequencia			= vet_contas.nr_seq_processo_conta;

				qt_movimento_w := qt_movimento_w + 1;
			elsif	(ie_tipo_movimento_w = 31) then
				ie_tipo_w	:= 'AC';

				update	pls_processo_contas_comp
				set	cd_conta_cred_ajuste		= cd_conta_credito_w,
					cd_conta_deb_ajuste		= cd_conta_debito_w,
					nr_seq_esquema_ajuste		= nr_seq_esquema_w,
					cd_historico_ajuste		= cd_historico_padrao_w,
					cd_classif_cred_ajuste		= cd_classificacao_credito_w,
					cd_classif_deb_ajuste		= cd_classificacao_debito_w
				where	nr_sequencia			= vet_contas.nr_seq_conta_comp;

				qt_movimento_w := qt_movimento_w + 1;
			elsif	(ie_tipo_movimento_w = 32) then
				ie_tipo_w	:= 'DC';

				update	pls_processo_contas_comp
				set	cd_conta_cred_ajuste		= cd_conta_credito_w,
					cd_conta_deb_ajuste		= cd_conta_debito_w,
					nr_seq_esquema_ajuste		= nr_seq_esquema_w,
					cd_historico_ajuste		= cd_historico_padrao_w,
					cd_classif_cred_ajuste		= cd_classificacao_credito_w,
					cd_classif_deb_ajuste		= cd_classificacao_debito_w
				where	nr_sequencia			= vet_contas.nr_seq_conta_comp;

				qt_movimento_w := qt_movimento_w + 1;
			end if;


			if	(nr_seq_atualizacao_p is not null) then
				if	(nr_seq_esquema_w is null) then
					pls_gravar_mov_contabil(nr_seq_atualizacao_p,
								1,
								null,
								null,
								null,
								ie_tipo_w,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								nm_usuario_p, 
								nr_seq_esquema_w,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								vet_contas.nr_seq_processo_conta,
								vet_contas.nr_seq_conta_comp);

				elsif	((cd_conta_credito_w is null) or
					(cd_conta_debito_w is null)) then
					pls_gravar_mov_contabil(nr_seq_atualizacao_p,
								2,
								null,
								null,
								null,
								ie_tipo_w,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								nm_usuario_p, 
								nr_seq_esquema_w,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								vet_contas.nr_seq_processo_conta,
								vet_contas.nr_seq_conta_comp);
				end if;
			end if;
			end;
		end loop;
		close c_tipo_movimento;
		end;
	end loop;
	close c_contas;
end if;

qt_movimento_p := qt_movimento_w;
commit;

end ctb_pls_atualizar_ressa;
/