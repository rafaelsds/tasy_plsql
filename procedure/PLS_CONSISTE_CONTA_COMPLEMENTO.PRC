create or replace
procedure pls_consiste_conta_complemento
		(	nr_seq_conta_p		Number,
			nr_seq_prestador_p	Number,
			nr_seq_segurado_p	Number,
			cd_estabelecimento_p	Number,
			nm_usuario_p		Varchar2) is

/* Aten��o, n�o usar mais essa procedure 
A procedure agora � a PLS_CONSISTE_GUIA_COMPLEMENTO
*/			

cd_procedimento_w		Number(15);
cd_grupo_w			Number(15);
cd_especialidade_w		Number(15);
cd_area_w			Number(15);
cd_guia_w			Varchar(20);

ie_tipo_guia_w			Varchar(2);
ie_tipo_complemento_w		Varchar(2);
ie_tipo_conta_compl_w		Varchar(2);
ie_origem_proced_w		Number(15);
ie_origem_proced_ww		Number(15);
ie_preco_w			Number(10);

nr_seq_contrato_w		Number(10);
nr_seq_prestador_exec_w		Number(10);
nr_seq_regra_w			Number(10);
nr_seq_guia_w			Number(10);
nr_seq_motivo_inc_w		Number(10);
nr_seq_plano_w			Number(10);
nr_seq_intercambio_w		Number(10);

nr_seq_estrutura_mat_w		Number(10);
nr_seq_congenere_w		Number(10);
nr_seq_material_w		Number(10);
cd_material_w			Varchar2(20);
cd_prestador_w			Varchar2(30);

nr_seq_estrut_regra_w		number(10);
ie_estrut_mat_w			varchar2(1);
ie_tipo_segurado_w		pls_segurado.ie_tipo_segurado%type;

	
cursor C01 is
	select 	cd_procedimento,
		ie_origem_proced
	from 	pls_conta_proc
	where	nr_seq_conta = nr_seq_conta_p;

cursor C02 is
	select  cd_material
	from 	pls_conta_mat
	where	nr_seq_conta = nr_seq_conta_p;
	
begin

select	ie_tipo_guia,
	nr_seq_prestador_exec,
	cd_guia
into	ie_tipo_guia_w,
	nr_seq_prestador_exec_w,
	cd_guia_w
from	pls_conta
where 	nr_sequencia = nr_seq_conta_p;

select	nvl(max(nr_seq_contrato),0),
	nvl(max(nr_seq_plano),0),
	nvl(max(nr_seq_intercambio),0),
	nvl(max(nr_seq_congenere),0),
	max(ie_tipo_segurado)
into	nr_seq_contrato_w,
	nr_seq_plano_w,
	nr_seq_intercambio_w,
	nr_seq_congenere_w,
	ie_tipo_segurado_w
from	pls_segurado
where	nr_sequencia = nr_seq_segurado_p;

begin
select	ie_preco
into	ie_preco_w
from	pls_plano
where	nr_sequencia	= nr_seq_plano_w;
exception
when others then
	ie_preco_w		:= 0;
end;

begin
select	max(nr_sequencia)
into	nr_seq_guia_w
from	pls_guia_plano
where	cd_guia		= cd_guia_w
and	nr_seq_segurado	= nr_seq_segurado_p;
exception
when others then
	nr_seq_guia_w	:= 0;
end;

if	(nr_seq_guia_w	<> 0) then
	select	max(nr_seq_motivo_inclusao)
	into	nr_seq_motivo_inc_w
	from	pls_execucao_requisicao
	where	nr_seq_guia	= nr_seq_guia_w;
end if;

select	max(cd_prestador)
into	cd_prestador_w
from	pls_prestador
where	nr_sequencia	= nr_seq_prestador_p;
	
open C01;
loop
fetch C01 into	
	cd_procedimento_w,
	ie_origem_proced_w;
exit when C01%notfound;
	begin
	
	pls_obter_estrut_proc(	cd_procedimento_w, ie_origem_proced_w, cd_area_w,
				cd_especialidade_w, cd_grupo_w, ie_origem_proced_ww);
	
	select	max(nr_sequencia)
	into	nr_seq_regra_w
	from	pls_regra_conta_compl
	where	(ie_tipo_guia		is null	or decode(ie_tipo_guia,'1','5','2','4',ie_tipo_guia)	= ie_tipo_guia_w)
	and	(nr_seq_prestador	is null	or nr_seq_prestador	= nr_seq_prestador_exec_w)
	and	(nr_seq_contrato	is null	or nr_seq_contrato	= nr_seq_contrato_w)
	and	(ie_preco		is null	or ie_preco		= ie_preco_w)
	and	(nr_seq_grupo_prestador	is null	or pls_obter_se_prestador_grupo(nr_seq_grupo_prestador, nr_seq_prestador_exec_w)	= 'S')
	and	(nr_seq_intercambio	is null	or nr_seq_intercambio	= nr_seq_intercambio_w)
	and	nvl(nr_seq_congenere,nr_seq_congenere_w)		= nr_seq_congenere_w
	and     nvl(cd_procedimento,cd_procedimento_w)        		= cd_procedimento_w
        and     nvl(ie_origem_proced,ie_origem_proced_w)      		= ie_origem_proced_ww
        and     nvl(cd_grupo_proc,cd_grupo_w)                 		= cd_grupo_w
        and     nvl(cd_especialidade, cd_especialidade_w) 		= cd_especialidade_w
        and     nvl(cd_area_procedimento, cd_area_w)          		= cd_area_w
	and	((cd_prestador is null) or (cd_prestador = nvl(cd_prestador_w,'0')))
	and	(nvl(nr_seq_motivo_inclusao,nr_seq_motivo_inc_w)	= nr_seq_motivo_inc_w)
	and	(ie_tipo_segurado	is null or ie_tipo_segurado	= ie_tipo_segurado_w)
	and	ie_situacao 						= 'A'
	and	sysdate	between (dt_inicio_vigencia) and (fim_dia(nvl(dt_fim_vigencia, sysdate)));	
		
	if	(nr_seq_regra_w	is not null) then
		select	ie_tipo_complemento
		into	ie_tipo_complemento_w
		from	pls_regra_conta_compl
		where	nr_sequencia	= nr_seq_regra_w;
		
		if	(ie_tipo_complemento_w = 1) then
			ie_tipo_conta_compl_w := 1;
		else
			ie_tipo_conta_compl_w := 4;
		end if;
				
		update	pls_conta
		set	ie_estagio_complemento 	= ie_tipo_conta_compl_w,
			ie_origem_conta		= 'C',
			nm_usuario 		= nm_usuario_p,
			dt_atualizacao 		= sysdate
		where	nr_sequencia 		= nr_seq_conta_p;
	end if;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	cd_material_w;
exit when C02%notfound;
	begin
	select	max(nr_sequencia),
		nr_seq_estrut_mat
	into 	nr_seq_material_w,
		nr_seq_estrutura_mat_w
	from	pls_material
	where	cd_material = cd_material_w;
	
	select	max(nr_sequencia),
		max(nr_seq_estrutura_mat)
	into	nr_seq_regra_w,
		nr_seq_estrut_regra_w
	from	pls_regra_conta_compl
	where	(ie_tipo_guia		is null	or decode(ie_tipo_guia,'1','5','2','4',ie_tipo_guia)	= ie_tipo_guia_w)
	and	(nr_seq_prestador	is null	or nr_seq_prestador	= nr_seq_prestador_exec_w)
	and	(nr_seq_contrato	is null	or nr_seq_contrato	= nr_seq_contrato_w)
	and	(ie_preco		is null	or ie_preco		= ie_preco_w)
	and	(nr_seq_grupo_prestador	is null	or pls_obter_se_prestador_grupo(nr_seq_grupo_prestador, nr_seq_prestador_exec_w)	= 'S')
	and	(nr_seq_intercambio	is null	or nr_seq_intercambio	= nr_seq_intercambio_w)
	and	nvl(nr_seq_congenere,nr_seq_congenere_w)		= nr_seq_congenere_w
	and	nvl(nr_seq_material,nr_seq_material_w) = nr_seq_material_w
	and	((cd_prestador is null) or (cd_prestador = nvl(cd_prestador_w,'0')))
	and	(nvl(nr_seq_motivo_inclusao,nr_seq_motivo_inc_w)	= nr_seq_motivo_inc_w)
	and	ie_situacao 						= 'A'
	and	sysdate	between (dt_inicio_vigencia) and (fim_dia(nvl(dt_fim_vigencia, sysdate)));
		
	ie_estrut_mat_w		:= 'S';
	
	if	(nr_seq_estrut_regra_w is not null) then
		if	(pls_obter_se_mat_estrutura(nr_seq_material_w, nr_seq_estrut_regra_w) = 'N') then
			ie_estrut_mat_w	:= 'N';
		end if;
	end if;	
	
	if	(nr_seq_regra_w	is not null) and
		(ie_estrut_mat_w = 'S') then
		select	ie_tipo_complemento
		into	ie_tipo_complemento_w
		from	pls_regra_conta_compl
		where	nr_sequencia	= nr_seq_regra_w;
		
		if	(ie_tipo_complemento_w = 1) then
			ie_tipo_conta_compl_w := 1;
		else
			ie_tipo_conta_compl_w := 4;
		end if;
				
		update	pls_conta
		set	ie_estagio_complemento 	= ie_tipo_conta_compl_w,
			ie_origem_conta		= 'C',
			nm_usuario 		= nm_usuario_p,
			dt_atualizacao 		= sysdate
		where	nr_sequencia 		= nr_seq_conta_p;
	end if;
	end;
end loop;
close C02;

commit;			

end pls_consiste_conta_complemento;
/