create or replace
procedure usinadap_atualiza_obs_exame  (            nr_prescricao_p  varchar2,
					cd_exame_integracao_p varchar2,
					ds_observacao_p  varchar2) is 

nr_seq_prescr_w  number(6);
nr_seq_exame_w  number(10);
nr_seq_resultado_w  number(10);
ds_observacao_w  varchar2(4000);

begin
ds_observacao_w := substr(ds_observacao_p,1,4000);

select       nvl(max(a.nr_sequencia),0),
	nvl(max(a.nr_seq_exame),0)
into  	nr_seq_prescr_w,
	nr_seq_exame_w
from  	prescr_procedimento a,
	exame_laboratorio b
where      a.nr_seq_exame = b.nr_seq_exame
and 	b.ie_situacao <> 'i'
and 	a.nr_prescricao = nr_prescricao_p
and 	upper(nvl(b.cd_exame_integracao,b.cd_exame)) = upper(cd_exame_integracao_p);

if  not(nr_seq_prescr_w > 0) then
	gravar_log_tasy (8989,obter_desc_expressao(622766, cd_exame_integracao_p)||to_char(nr_prescricao_p)|| ' #@#@','TASY');
	commit;
end if;

select  MAX(nr_seq_resultado)
into  	nr_seq_resultado_w
from  	exame_lab_resultado
where  	nr_prescricao = nr_prescricao_p;

update  exame_lab_result_item
set 	ds_observacao = ds_observacao_w
where  	nr_seq_resultado = nr_seq_resultado_w
and 	nr_seq_exame  = nr_seq_exame_w
and 	nr_seq_prescr  = nr_seq_prescr_w;

commit;

end usinadap_atualiza_obs_exame;
/