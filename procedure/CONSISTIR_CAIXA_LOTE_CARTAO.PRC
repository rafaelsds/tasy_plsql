create or replace
procedure consistir_caixa_lote_cartao (nr_seq_lote_p		number,
				      nm_usuario_p		varchar2,
				      cd_estabelecimento_p	number,
				      ie_acao_p			varchar2) is 

vl_lote_w		movto_cartao_cr_parcela.vl_parcela%type;
nr_seq_caixa_movto_w	number(10);
nr_seq_tf_movto_w	number(10);
ie_saldo_negativo_w	varchar2(1);
vl_saldo_atual_w	number(15,2);
vl_saldo_avista_w	number(15,2)	:= 0;
vl_saldo_pre_w		number(15,2)	:= 0;
nr_seq_saldo_caixa_w	number(15);
dt_saldo_w		date;
ie_saldo_caixa_w	transacao_financeira.ie_saldo_caixa%type;
ie_caixa_w		transacao_financeira.ie_caixa%type;
vl_atualizado_w		number(15,2)	:= 0;
ie_saldo_neg_cheque_w			varchar2(1);
ie_saldo_dep_cheque_w			varchar2(255);

begin

if (nr_seq_lote_p is not null) then
	
	obter_param_usuario(3020,34,obter_perfil_ativo,nm_usuario_p,obter_estabelecimento_ativo,nr_seq_caixa_movto_w);
	obter_param_usuario(3020,35,obter_perfil_ativo,nm_usuario_p,obter_estabelecimento_ativo,nr_seq_tf_movto_w);

	if (ie_acao_p = 'I') then
		select	nvl(sum(a.vl_parcela),0)
		into	vl_lote_w
		from	movto_cartao_cr_parcela a
		where	a.nr_seq_lote	= nr_seq_lote_p;
	else
		select	nvl(sum(a.vl_parcela),0) *-1
		into	vl_lote_w
		from	movto_cartao_cr_parcela a
		where	a.nr_seq_lote	= nr_seq_lote_p;
	end if;
	
	begin
	select	nvl(ie_saldo_negativo,'S'),
		nvl(ie_saldo_dep_cheque,'N'),
		nvl(ie_saldo_neg_cheque,'N')
	into	ie_saldo_negativo_w,
		ie_saldo_dep_cheque_w,
		ie_saldo_neg_cheque_w
	from	parametro_tesouraria
	where	cd_estabelecimento	= cd_estabelecimento_p;
	exception
	when no_data_found then
		/*Os par�metros da tesouraria n�o est�o cadastrados!*/
		wheb_mensagem_pck.exibir_mensagem_abort(220466);
	end;
	
	if (nr_seq_caixa_movto_w is not null) then
	
		select	max(nr_sequencia)
		into	nr_seq_saldo_caixa_w
		from	caixa_saldo_diario
		where	nr_seq_caixa	= nr_seq_caixa_movto_w
		and	dt_fechamento	is null;
	
	end if;
	
	if (nr_seq_saldo_caixa_w is not null) then
		select	max(dt_saldo)
		into	dt_saldo_w
		from	caixa_saldo_diario
		where	nr_sequencia	= nr_seq_saldo_caixa_w;
	end if;
	
	if (nr_seq_caixa_movto_w is not null) and (nr_seq_tf_movto_w is not null) and (dt_saldo_w is not null)then
	
		if	( nvl(ie_saldo_negativo_w,'S') = 'N' ) then
	
			select	max(vl_saldo),
				nvl(max(vl_cheque_vista_atual),0),
				nvl(max(vl_cheque_pre_atual),0)
			into	vl_saldo_atual_w,
				vl_saldo_avista_w,
				vl_saldo_pre_w
			from	caixa_saldo_diario
			where	trunc(dt_saldo,'dd')	= trunc(dt_saldo_w,'dd')
			and	nr_seq_caixa		= nr_seq_caixa_movto_w;
			
			select 	max(ie_saldo_caixa),
				max(ie_caixa)
			into	ie_saldo_caixa_w,
				ie_caixa_w
			from	transacao_financeira
			where	nr_sequencia = nr_seq_tf_movto_w;

			if 	(ie_caixa_w	= 'D') or
				(ie_caixa_w	= 'T') or
				(ie_caixa_w	= 'A') then
				if	(ie_saldo_caixa_w	= 'S') then
					vl_atualizado_w		:= nvl(vl_atualizado_w,0) + nvl(vl_lote_w,0);
				elsif	(ie_saldo_caixa_w	= 'E') then
					vl_atualizado_w		:= nvl(vl_atualizado_w,0) + (nvl(vl_lote_w,0) * -1);
				end if;
			end if;

			if	((vl_saldo_atual_w - vl_atualizado_w) < 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(220480,'VL_SALDO_ATUAL_W='||vl_saldo_atual_w||';VL_SALDO_FECHAMENTO_W='||(vl_saldo_atual_w - vl_atualizado_w));
			end if;
			
		end if;
	
	end if;
end if;

end consistir_caixa_lote_cartao;
/
