create or replace
procedure pls_atualizar_dt_int_req_web
			(	nr_seq_guia_princ_p	Number,
				dt_internacao_p		Varchar2,
				dt_alta_p		Varchar2,
				nm_usuario_p		Varchar2) is 
	
nr_seq_atend_w		Number(10);	
				
begin

select  max(nr_sequencia)
into	nr_seq_atend_w
from	pls_guia_atendimento
where	nr_seq_guia = nr_seq_guia_princ_p;

if	(nr_seq_atend_w is not null) then

	if	(dt_internacao_p is not null) then
		update	pls_guia_atendimento
		set	dt_internacao = to_date(dt_internacao_p,'dd/mm/yyyy')
		where	nr_sequencia = nr_seq_atend_w;
	end if;
	
	if	(dt_alta_p is not null) then
		update	pls_guia_atendimento
		set	dt_alta = to_date(dt_alta_p,'dd/mm/yyyy')
		where	nr_sequencia = nr_seq_atend_w;
	end if;
	
elsif	(nr_seq_guia_princ_p is not null and (dt_alta_p is not null or dt_internacao_p is not null)) then

	insert	into pls_guia_atendimento
		    (nr_sequencia, nr_seq_guia, dt_atualizacao,
		     nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		     ds_observacao, dt_internacao, dt_alta)
	      values(pls_guia_atendimento_seq.nextval, nr_seq_guia_princ_p, sysdate,
		     nm_usuario_p, sysdate, nm_usuario_p,
		     null, to_date(dt_internacao_p,'dd/mm/yyyy'), to_date(dt_alta_p,'dd/mm/yyyy'));
	
end if;

commit;

end pls_atualizar_dt_int_req_web;
/
