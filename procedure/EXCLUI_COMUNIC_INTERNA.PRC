create or replace
procedure exclui_comunic_interna is


qt_dia_w			Number(10);
nr_seq_classif_w		Number(10);
cd_perfil_destino_w		Number(5);
cd_setor_destino_w		Number(5);
nr_seq_grupo_usu_destino_w	Number(10);
nm_usuario_destino_w	Varchar2(15);
ie_abrangencia_w		Varchar2(15);
nr_sequencia_w		Number(10);

Cursor C01 is
	select	qt_dia,
		nr_seq_classif,
		cd_perfil_destino,
		cd_setor_destino,
		nr_seq_grupo_usu_destino,
		nm_usuario_destino,
		ie_abrangencia
	from	comunic_interna_regra_excl
	where	ie_situacao  = 'A';

Cursor C02 is
	select	nr_sequencia
	from	comunic_interna
	where	((nr_seq_classif = nr_seq_classif_w) or (nvl(nr_seq_classif_w,0) = 0))
	and	((nvl(cd_perfil_destino_w,0) = 0) or 
		((obter_se_contido_char(cd_perfil_destino_w, replace(ds_perfil_adicional,' ','')) = 'S') and 
		(instr(ds_perfil_adicional,',',instr(ds_perfil_adicional,',')+1) = 0)))
	and	((nvl(cd_setor_destino_w,0) = 0) or 
		((obter_se_contido_char(cd_setor_destino_w, replace(ds_setor_adicional,' ','')) = 'S') and 
		(instr(ds_setor_adicional,',',instr(ds_setor_adicional,',')+1) = 0)))
	and	((nvl(nr_seq_grupo_usu_destino_w,0) = 0) or 
		((obter_se_contido_char(nr_seq_grupo_usu_destino_w, replace(ds_grupo,' ','')) = 'S') and 
		(instr(ds_grupo,',',instr(ds_grupo,',')+1) = 0)))
	and	((nvl(nm_usuario_destino_w,'0') = '0') or 
		((nm_usuario_destino like to_char(Chr(37)||nm_usuario_destino_w||Chr(37))) and 
		(instr(nm_usuario_destino,',',instr(nm_usuario_destino,',')+1) = 0)))
	and	(obter_dias_entre_datas(dt_comunicado,sysdate) >= qt_dia_w)
	and	ie_abrangencia_w = 'E'
	union
	select	nr_sequencia
	from	comunic_interna
	where	((nr_seq_classif = nr_seq_classif_w) or (nvl(nr_seq_classif_w,0) = 0))
	and	((nvl(cd_perfil_destino_w,0) = 0) or 
		(obter_se_contido_char(cd_perfil_destino_w, replace(ds_perfil_adicional,' ','')) = 'S'))
	and	((nvl(cd_setor_destino_w,0) = 0) or 
		(obter_se_contido_char(cd_setor_destino_w, replace(ds_setor_adicional,' ','')) = 'S'))
	and	((nvl(nr_seq_grupo_usu_destino_w,0) = 0) or 
		(obter_se_contido_char(nr_seq_grupo_usu_destino_w, replace(ds_grupo,' ','')) = 'S'))
	and	((nvl(nm_usuario_destino_w,'0') = '0') or 
		(nm_usuario_destino like to_char(Chr(37)||nm_usuario_destino_w||Chr(37))))
	and	(obter_dias_entre_datas(dt_comunicado,sysdate) >= qt_dia_w)
	and	ie_abrangencia_w = 'Q';

begin
open C01;
loop
fetch C01 into	
	qt_dia_w,
	nr_seq_classif_w,
	cd_perfil_destino_w,
	cd_setor_destino_w,
	nr_seq_grupo_usu_destino_w,
	nm_usuario_destino_w,
	ie_abrangencia_w;
exit when C01%notfound;
	begin
	open C02;
	loop
	fetch C02 into	
		nr_sequencia_w;
	exit when C02%notfound;
		begin
		delete	from alerta_tasy_ocor
		where	nr_seq_comunic = nr_sequencia_w;
		
		delete	from comunic_interna_arq
		where	nr_seq_comunic = nr_sequencia_w;
		
		delete	from comunic_interna_lida
		where	nr_sequencia = nr_sequencia_w;
		
		delete	from tasy_dica_interna_java
		where	nr_seq_comunic_interna = nr_sequencia_w;
		
		delete	from comunic_interna
		where	nr_sequencia = nr_sequencia_w;

		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

commit;

end exclui_comunic_interna;
/