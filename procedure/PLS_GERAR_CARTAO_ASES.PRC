create or replace
procedure pls_gerar_cartao_ases
		(	nr_seq_lote_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is

nr_seq_segurado_w	pls_segurado.nr_sequencia%type;
nm_beneficiario_w	pessoa_fisica.nm_pessoa_fisica%type;

cd_usuario_plano_w	pls_segurado_carteira.cd_usuario_plano%type;
dt_nascimento_w		varchar2(10);
ds_plano_w		pls_plano.ds_plano%type;
nr_prot_ans_scpa_w	varchar2(20); -- 18/08/2016 - Tanto o campo CD_SCPA como o campo NR_PROTOCOLO_ANS (da tabela PLS_PLANO) possuem o tamanho varchar2(20)
dt_adesao_w		varchar2(10);
dt_validade_carteira_w	varchar2(10);
ds_segmentacao_w	varchar2(255);
ds_acomodacao_w		varchar2(255);
ds_tipo_contratacao_w	varchar2(255);
nm_empresa_w		varchar2(255);
nr_via_carteira_w	varchar2(5);	
ds_fator_moderador_w	varchar2(5);
ds_trilha1_w		pls_segurado_carteira.ds_trilha1%type;
ds_trilha2_w		pls_segurado_carteira.ds_trilha2%type;
			
Cursor C01 is
	select	b.nr_sequencia nr_seq_segurado,
		upper(substr(e.nm_pessoa_fisica,1,32)) nm_beneficiario
	from	pls_segurado_carteira	a,
		pls_segurado		b,
		pls_carteira_emissao	c,
		pls_lote_carteira	d,
		pessoa_fisica		e
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	c.nr_seq_seg_carteira	= a.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and	b.cd_pessoa_fisica	= e.cd_pessoa_fisica
	and	d.nr_sequencia		= nr_seq_lote_p
	and	b.nr_seq_contrato is not null
	order by e.nm_pessoa_fisica;
			
begin

delete	w_pls_interface_carteira
where	nr_seq_lote = nr_seq_lote_p;

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w,
	nm_beneficiario_w;
exit when C01%notfound;
	begin

	select	pls_obter_carteira_mascara(b.nr_sequencia) cd_usuario_plano,
		to_char(a.dt_nascimento,'dd/mm/yyyy') dt_nascimento,
		substr(d.ds_plano,1,41) ds_plano,		
		substr(decode(d.ie_regulamentacao,'R',d.cd_scpa,d.nr_protocolo_ans),1,12) nr_prot_ans_scpa,
		to_char(b.dt_contratacao,'dd/mm/yyyy') dt_adesao,
		to_char(c.dt_validade_carteira,'dd/mm/yyyy') dt_validade_carteira,
		upper(substr(obter_valor_dominio(1665,d.ie_segmentacao),1,45)) ds_segmentacao,
		substr(pls_obter_dados_cart_unimed(b.nr_sequencia,d.nr_sequencia,'DA',1),1,38) ds_acomodacao, 
		substr(obter_valor_dominio(1666,d.ie_tipo_contratacao),1,24) ds_tipo_contratacao,
		upper(substr(obter_nome_pf_pj(null,e.cd_cgc_estipulante),1,45)) nm_empresa,
		decode(nvl(ie_coparticipacao,'N'),'N','N�O','SIM') ds_fator_moderador,		
		lpad(to_char(c.nr_via_solicitacao),2,'0') nr_via_carteira,
		replace(replace(c.ds_trilha1,'%',''),'?','') ds_trilha1,
		replace(replace(c.ds_trilha2,';',''),'?','') ds_trilha2		
	into	cd_usuario_plano_w,
		dt_nascimento_w,
		ds_plano_w,
		nr_prot_ans_scpa_w,
		dt_adesao_w,
		dt_validade_carteira_w,
		ds_segmentacao_w,
		ds_acomodacao_w,
		ds_tipo_contratacao_w,
		nm_empresa_w,
		ds_fator_moderador_w,
		nr_via_carteira_w,
		ds_trilha1_w,
		ds_trilha2_w
	from	pessoa_fisica		a,
		pls_segurado		b,
		pls_segurado_carteira	c,	
		pls_plano		d,
		pls_contrato		e		
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	c.nr_seq_segurado	= b.nr_sequencia
	and	b.nr_seq_plano		= d.nr_sequencia
	and	b.nr_seq_contrato	= e.nr_sequencia
	and	b.nr_sequencia		= nr_seq_segurado_w;
	
	insert into w_pls_interface_carteira
		(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, 
			nr_seq_lote, ie_tipo_reg, cd_usuario_plano, nm_beneficiario, dt_nascimento,
			nm_plano, nr_protocolo_ans, dt_adesao, dt_validade_carteira, ds_segmentacao,
			ds_acomodacao, ds_tipo_contratacao, ds_estipulante, ds_observacao_2, nr_via_cartao,
			ds_trilha_1, ds_trilha_2)
	values	(	w_pls_interface_carteira_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p, 
			nr_seq_lote_p, 1, cd_usuario_plano_w, nm_beneficiario_w, dt_nascimento_w,
			ds_plano_w, nr_prot_ans_scpa_w, dt_adesao_w, dt_validade_carteira_w, ds_segmentacao_w,
			ds_acomodacao_w, ds_tipo_contratacao_w, nm_empresa_w, ds_fator_moderador_w, nr_via_carteira_w,
			ds_trilha1_w, ds_trilha2_w);
	end;
end loop;
close C01;

commit;

end pls_gerar_cartao_ases;
/