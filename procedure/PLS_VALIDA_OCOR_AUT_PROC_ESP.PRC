create or replace
procedure pls_valida_ocor_aut_proc_esp
				(	nr_seq_ocor_combinada_p		Number,
					nr_seq_ocorrencia_p		Number,
					nr_seq_segurado_p		Number,
					nr_seq_motivo_glosa_p		Number,
					nr_seq_guia_p			Number,
					nr_seq_requisicao_p		Number,
					nr_seq_execucao_p		Number,
					ie_utiliza_filtro_p		Varchar2,
					nr_seq_param1_p			Number,
					nr_seq_param2_p			Number,
					nr_seq_param3_p			Number,
					nr_seq_param4_p			Number,
					nr_seq_param5_p			Number,
					nm_usuario_p			Varchar2,
					cd_estabelecimento_p		Number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Procedure utilizada para validar informa��es da regra de procedimento x especialidade, esta rotina �
utilizada para gera��o de ocorr�ncia na Autoriza��o / Requisi��o e Execu��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [x]  Tasy (Delphi/Java) [ x] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
 Rotina utilizada para gera��o de ocorr�ncia na Autoriza��o / Requisi��o e Execu��o
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_especialidade_w			number(5);
nr_seq_guia_proc_w			number(10);
cd_procedimento_w			procedimento.cd_procedimento%type;
ie_origem_proced_w			procedimento.ie_origem_proced%type;
ie_gerou_ocor_cabecalho_w		Varchar2(2);
ie_regra_w				Varchar2(2);
ie_tipo_ocorrencia_w			Varchar2(2);
ie_gerar_ocorrencia_w			Varchar2(2)	:= 'N';
ie_tipo_item_w				Number(10);
nr_seq_oc_benef_w			Number(10);
nr_seq_proc_espec_w			Number(10);
nr_seq_req_proc_w			Number(10);
nr_seq_exec_proc_w			Number(10);

nr_seq_proc_espec_ww			varchar2(20);
nr_seq_proc_w				varchar2(20);

Cursor C02 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p;

Cursor C03 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C04 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_execucao_req_item
	where	nr_seq_execucao	= nr_seq_execucao_p;
	
Cursor C05 is
	select	nr_seq_proc_espec
	from	pls_validacao_aut_proc_esp
	where	nr_seq_ocor_aut_combinada	= nr_seq_ocor_combinada_p;
	
begin
if	(nr_seq_guia_p is not null) then
	begin
		select 	cd_especialidade
		into	cd_especialidade_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_p;
	exception
	when others then
		cd_especialidade_w	:= null;
	end;

	open C05;
	loop
	fetch C05 into	
		nr_seq_proc_espec_w;
	exit when C05%notfound;
		begin
		if	(nr_seq_proc_espec_w is not null) and (cd_especialidade_w is not null) then
			open C02;
			loop
			fetch C02 into
				nr_seq_guia_proc_w,
				cd_procedimento_w,
				ie_origem_proced_w;
			exit when C02%notfound;
				begin
				ie_gerar_ocorrencia_w 	:= pls_obter_se_espec_solic_comb(nr_seq_guia_proc_w, null, null, nr_seq_proc_espec_w,cd_especialidade_w);

				if 	(ie_utiliza_filtro_p = 'S') then
					/* Tratamento para filtros */
					pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, null,
									null, nr_seq_guia_proc_w, null,
									null, null, null,
									cd_procedimento_w, ie_origem_proced_w, null,
									ie_gerou_ocor_cabecalho_w,null, null,
									null, null, null,
									nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

					if	(ie_regra_w	= 'S') then
						ie_gerar_ocorrencia_w	:= 'S';
					elsif	(ie_regra_w	in ('E','N')) then
						ie_gerar_ocorrencia_w	:= 'N';
						exit;
					end if;
				end if;
				end;

				if	(ie_gerar_ocorrencia_w	= 'S')	then
					if	(nr_seq_guia_p	is not null) then
						ie_tipo_item_w	:= 1;
					end if;

					pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, null,
								nr_seq_guia_p, null, nr_seq_guia_proc_w,
								null, nr_seq_ocor_combinada_p, nm_usuario_p,
								null, nr_seq_motivo_glosa_p, ie_tipo_item_w,
								cd_estabelecimento_p, 'N' ,null,
								nr_seq_oc_benef_w, null,
								null, null, null);
								
					pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
									nr_seq_guia_p, null, null,
									nr_seq_guia_proc_w, null, null,
									null, null, null,
									nm_usuario_p, cd_estabelecimento_p);							
				end if;
			end loop;
			close C02;
		end if;
		end;
	end loop;
	close C05;

elsif	(nr_seq_requisicao_p is not null) then
	begin
		select 	cd_especialidade
		into	cd_especialidade_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_p;
	exception
	when others then
		cd_especialidade_w	:= null;
	end;

	open C05;
	loop
	fetch C05 into	
		nr_seq_proc_espec_w;
	exit when C05%notfound;
		begin
		if	(nr_seq_proc_espec_w is not null) and (cd_especialidade_w is not null) then
			open C03;
			loop
			fetch C03 into
				nr_seq_req_proc_w,
				cd_procedimento_w,
				ie_origem_proced_w;
			exit when C03%notfound;
				begin
				ie_gerar_ocorrencia_w 	:= pls_obter_se_espec_solic_comb(null,nr_seq_req_proc_w,null,nr_seq_proc_espec_w,cd_especialidade_w);

				if 	(ie_utiliza_filtro_p = 'S') then

					/* Tratamento para filtros */
					pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, nr_seq_requisicao_p,
									null, null, nr_seq_req_proc_w,
									null, null, null,
									cd_procedimento_w, ie_origem_proced_w, null,
									ie_gerou_ocor_cabecalho_w,null, null,
									null, null, null,
									nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

					if	(ie_regra_w	= 'S') then
						ie_gerar_ocorrencia_w	:= 'S';
					elsif	(ie_regra_w	in ('E','N')) then
						ie_gerar_ocorrencia_w	:= 'N';
						exit;
					end if;
				end if;
				end;

				if	(ie_gerar_ocorrencia_w	= 'S')	then
					if	(nr_seq_requisicao_p	is not null) then
						ie_tipo_item_w	:= 5;
					end if;

					pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
								null, null, nr_seq_req_proc_w,
								null, nr_seq_ocor_combinada_p, nm_usuario_p,
								null, nr_seq_motivo_glosa_p, ie_tipo_item_w,
								cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
								nr_seq_oc_benef_w, null,
								null, null, null);
								
					pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
									null, nr_seq_requisicao_p, null,
									null, null, nr_seq_req_proc_w,
									null, null, null,
									nm_usuario_p, cd_estabelecimento_p);	
				end if;
			end loop;
			close C03;
		end if;
		end;
	end loop;
	close C05;
elsif	(nr_seq_execucao_p is not null) then
	begin 
		select 	b.cd_especialidade
		into	cd_especialidade_w
		from	pls_execucao_requisicao a,
			pls_requisicao b
		where	a.nr_sequencia		= nr_seq_execucao_p
		and	a.nr_seq_requisicao	= b.nr_sequencia;
	exception
	when others then
		cd_especialidade_w	:= null;
	end;
	
	open C05;
	loop
	fetch C05 into	
		nr_seq_proc_espec_w;
	exit when C05%notfound;
		begin
		if	(nr_seq_proc_espec_w is not null) and (cd_especialidade_w is not null) then
			open C04;
			loop
			fetch C04 into
				nr_seq_exec_proc_w,
				cd_procedimento_w,
				ie_origem_proced_w;
			exit when C04%notfound;
				begin
				ie_gerar_ocorrencia_w 	:= pls_obter_se_espec_solic_comb(null,null,nr_seq_exec_proc_w,nr_seq_proc_espec_w,cd_especialidade_w);

				if 	(ie_utiliza_filtro_p = 'S') then
					/* Tratamento para filtros */
					pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, nr_seq_requisicao_p,
									null, nr_seq_execucao_p, null,
									null, null, nr_seq_exec_proc_w,
									cd_procedimento_w, ie_origem_proced_w, null,
									ie_gerou_ocor_cabecalho_w,null, null,
									null, null, null,
									nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

					if	(ie_regra_w	= 'S') then
						ie_gerar_ocorrencia_w	:= 'S';
					elsif	(ie_regra_w	in ('E','N')) then
						ie_gerar_ocorrencia_w	:= 'N';
						exit;
					end if;
				end if;
				end;

				if	(ie_gerar_ocorrencia_w	= 'S')	then
					if	(nr_seq_execucao_p	is not null) then
						ie_tipo_item_w	:= 10;
					end if;

					pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, null,
								null, null, nr_seq_exec_proc_w,
								null, nr_seq_ocor_combinada_p, nm_usuario_p,
								null, nr_seq_motivo_glosa_p, ie_tipo_item_w,
								cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
								nr_seq_oc_benef_w, null,
								null, null, null);
								
					pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
									null, null, nr_seq_execucao_p,
									null, null, null,
									null, nr_seq_exec_proc_w, null,
									nm_usuario_p, cd_estabelecimento_p);
				end if;
			end loop;
			close C04;
		end if;
		end;
	end loop;
	close C05;
end if;

end pls_valida_ocor_aut_proc_esp;
/
