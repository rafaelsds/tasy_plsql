create or replace
procedure pls_consistir_documentos_prop
			(	nr_seq_proposta_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_benef_proposta_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
ie_titular_w			varchar2(10);
dt_nascimento_w			date;
qt_idade_w			number(10);
ie_tipo_proposta_w		varchar2(10);
nr_seq_parentesco_w		number(10);
ie_tipo_contratacao_w		varchar2(10);
nr_seq_regra_doc_prop_w		number(10);
qt_registros_w			number(10);
nr_seq_tipo_documento_w		number(10);
qt_registros_inco_w		number(10);
ie_tipo_processo_w		varchar2(1);

Cursor C01 is
	select	b.nr_sequencia,
		b.cd_beneficiario,
		a.dt_nascimento,
		b.nr_seq_parentesco,
		decode(nr_seq_titular,null,decode(nr_seq_titular_contrato,null,'T','D'),'D')
	from	pls_proposta_beneficiario	b,
		pessoa_fisica			a
	where	a.cd_pessoa_fisica	= b.cd_beneficiario
	and	b.nr_seq_proposta	= nr_seq_proposta_p
	and	b.dt_cancelamento is null;

Cursor C02 is
	select	nr_sequencia
	from	pls_regra_doc_proposta
	where	ie_situacao		= 'A'
	and	((ie_tipo_proposta	= ie_tipo_proposta_w) or (ie_tipo_proposta is null))
	and	((ie_tipo_contratacao	= ie_tipo_contratacao_w) or (ie_tipo_contratacao is null))
	and	((ie_titularidade	= ie_titular_w) or (ie_titularidade = 'A'))
	and	((nr_seq_parentesco	= nr_seq_parentesco_w) or (nr_seq_parentesco is null))
	and	((ie_tipo_processo	= ie_tipo_processo_w) or (ie_tipo_processo = 'A'))
	and	qt_idade_w between nvl(qt_idade_inicial,qt_idade_w) and nvl(qt_idade_final,qt_idade_w);

Cursor C03 is
	select	nr_seq_tipo_documento
	from	pls_regra_proposta_doc
	where	nr_seq_regra	= nr_seq_regra_doc_prop_w;

begin

delete	from	pls_proposta_inconsist_doc
where	nr_seq_proposta = nr_seq_proposta_p
and	dt_liberacao is null;

select	ie_tipo_proposta,
	ie_tipo_contratacao,
	ie_tipo_processo
into	ie_tipo_proposta_w,
	ie_tipo_contratacao_w,
	ie_tipo_processo_w
from	pls_proposta_adesao
where	nr_sequencia	= nr_seq_proposta_p;

open C01;
loop
fetch C01 into
	nr_seq_benef_proposta_w,
	cd_pessoa_fisica_w,
	dt_nascimento_w,
	nr_seq_parentesco_w,
	ie_titular_w;
exit when C01%notfound;
	begin
	qt_idade_w	:= trunc(months_between(sysdate, dt_nascimento_w) / 12);
	
	open C02;
	loop
	fetch C02 into
		nr_seq_regra_doc_prop_w;
	exit when C02%notfound;
		begin
		open C03;
		loop
		fetch C03 into
			nr_seq_tipo_documento_w;
		exit when C03%notfound;
			begin
			qt_registros_inco_w := 0;
			select	count(1)
			into	qt_registros_w
			from	pls_tipo_documento_pf	b,
				pls_documento_pf	a
			where	a.nr_seq_tipo_doc_pf	= b.nr_sequencia
			and	b.cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	b.nr_seq_tipo_documento	= nr_seq_tipo_documento_w;
			
			begin
			select	count(1)
			into	qt_registros_inco_w
			from	pls_proposta_inconsist_doc
			where	nr_seq_proposta		= nr_seq_proposta_p
			and	nr_seq_benef_proposta	= nr_seq_benef_proposta_w
			and	nr_seq_tipo_documento	= nr_seq_tipo_documento_w;
			exception
			when others then
				qt_registros_inco_w := 0;
			end;
			
			if	((qt_registros_w = 0) and (qt_registros_inco_w = 0)) then
				insert into pls_proposta_inconsist_doc
						(	nr_sequencia, nr_seq_proposta,
							nr_seq_benef_proposta, nr_seq_tipo_documento, dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, dt_liberacao, nm_usuario_liberacao)
					values	(	pls_proposta_inconsist_doc_seq.NextVal, nr_seq_proposta_p,
							nr_seq_benef_proposta_w, nr_seq_tipo_documento_w, sysdate, nm_usuario_p,
							sysdate, nm_usuario_p, null, null);
			end if;
			end;
		end loop;
		close C03;
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

end pls_consistir_documentos_prop;
/