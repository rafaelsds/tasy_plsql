create or replace
procedure pls_alt_prod_usuario_eventual
		(	nr_seq_segurado_p	number,
			nr_seq_plano_p		number,
			nr_seq_motivo_alt_p	number,
			dt_alteracao_p		date,
			ds_observacao_p		varchar2,
			nm_usuario_p		Varchar2) is 
			
nr_seq_plano_ant_w		number(10);
nr_seq_alteracao_produto_w	pls_segurado_alt_plano.nr_sequencia%type;

begin

select	nr_seq_plano
into	nr_seq_plano_ant_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

if	(nr_seq_plano_ant_w = nr_seq_plano_p) then
	wheb_mensagem_pck.exibir_mensagem_abort(1109071,'DS_PRODUTO='||nr_seq_plano_ant_w||'-'||substr(pls_obter_dados_produto(nr_seq_plano_ant_w, 'N'),1,255)||
							';NM_BENEFICIARIO='||pls_obter_dados_produto(nr_seq_segurado_p, 'N'));
end if;

update	pls_segurado
set	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	nr_seq_plano	= nr_seq_plano_p
where	nr_sequencia	= nr_seq_segurado_p;

if	(nr_seq_plano_ant_w is not null) then
	select	pls_segurado_alt_plano_seq.nextval
	into	nr_seq_alteracao_produto_w
	from	dual;

	insert into pls_segurado_alt_plano
		(nr_sequencia, nm_usuario, dt_atualizacao,
		nr_seq_segurado, nr_seq_plano_ant, nr_seq_plano_atual, dt_alteracao,
		nr_seq_motivo_alt, ds_observacao, ie_situacao)
	values	(nr_seq_alteracao_produto_w, nm_usuario_p, sysdate,
		nr_seq_segurado_p, nr_seq_plano_ant_w, nr_seq_plano_p, dt_alteracao_p,
		nr_seq_motivo_alt_p, ds_observacao_p, 'A');
		
	pls_inativar_alteracao_produto(nr_seq_segurado_p, nr_seq_alteracao_produto_w, dt_alteracao_p, 'N', nm_usuario_p);
end if;		
		
pls_gerar_segurado_historico(nr_seq_segurado_p, '4', sysdate,
	wheb_mensagem_pck.get_texto(1108501),
	wheb_mensagem_pck.get_texto(1108497, 'NR_SEQ_PLANO_ANT='||nr_seq_plano_ant_w||';'||'NR_SEQ_PLANO='||nr_seq_plano_p),
	null, null, null,
	null, dt_alteracao_p, null,
	null, null, null,
	null, null, nm_usuario_p, 'S');

commit;

end pls_alt_prod_usuario_eventual;
/
