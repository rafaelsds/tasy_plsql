create or replace
procedure diops_gravar_inconsistencia
			(	nr_seq_periodo_p	number,
				cd_inconsistencia_p	number,
				ds_detalhe_p		varchar2,
				cd_pessoa_fisica_p	varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				nr_seq_movimento_p	number default null) is 
				
nr_seq_inconsistencia_w		Number(10);
nr_sequencia_w			Number(10);
ds_acao_usuario_w		Varchar2(4000);

begin

select	nr_sequencia,
	nvl(ds_acao_usuario,'')
into	nr_seq_inconsistencia_w,
	ds_acao_usuario_w
from	diops_inconsistencia
where	cd_inconsistencia	= cd_inconsistencia_p;

select	nvl(max(nr_sequencia),0) + 1
into	nr_sequencia_w
from	diops_periodo_inconsist
where	nr_seq_periodo	= nr_seq_periodo_p;

insert into diops_periodo_inconsist
	(nr_seq_periodo,
	nr_sequencia,
	cd_estabelecimento,
	nr_seq_inconsistencia,
	dt_atualizacao,
	nm_usuario,
	ds_acao_usuario,
	ds_detalhe,
	cd_pessoa_fisica,
	nr_seq_movimento)
values	(nr_seq_periodo_p,
	nr_sequencia_w,
	cd_estabelecimento_p,
	nr_seq_inconsistencia_w,
	sysdate,
	nm_usuario_p,
	ds_acao_usuario_w,
	ds_detalhe_p,
	cd_pessoa_fisica_p,
	nr_seq_movimento_p);

commit;

end diops_gravar_inconsistencia;
/
