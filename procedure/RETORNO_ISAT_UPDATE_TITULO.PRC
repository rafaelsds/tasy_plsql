create or replace
procedure retorno_ISAT_update_titulo(
			ds_obs_p		varchar2,
			nr_seq_caixa_p		varchar2,
			nr_titulo_p		varchar2) is 

begin

update	titulo_receber_liq a
set	a.ds_observacao = ds_obs_p
where	a.nr_seq_caixa_rec = nr_seq_caixa_p
and	a.nr_titulo = nr_titulo_p;

commit;

end retorno_ISAT_update_titulo;
/
