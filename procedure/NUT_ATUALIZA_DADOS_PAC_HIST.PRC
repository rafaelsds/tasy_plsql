create or replace
procedure nut_atualiza_dados_pac_hist(	nr_seq_nivel_assist_p		number,
					nr_seq_nivel_assist_ant_p	number,
					nr_seq_classificacao_pac_p	number,
					nr_seq_classificacao_pac_ant_p	number,
					nm_usuario_p		Varchar2) is 

begin

insert into nut_dados_paciente_hist(	nr_sequencia,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					NR_SEQ_NIVEL_ASSISTENCIAL,
					NR_SEQ_NIVEL_ASSIST_ANT, 					
					NR_SEQ_CLASSIFICACAO_PAC,
					NR_SEQ_CLASSIF_PAC_ANT)
		values		(	nut_dados_paciente_hist_seq.NextVal,
					sysdate,
					sysdate,
					nm_usuario_p,
					nm_usuario_p,
					nr_seq_nivel_assist_p,
					nr_seq_nivel_assist_ant_p,
					nr_seq_classificacao_pac_p,
					nr_seq_classificacao_pac_ant_p);

commit;

end nut_atualiza_dados_pac_hist;
/