create or replace procedure CPOE_GET_EMERGENCY_DISP_RULE
		(dt_reference_p in date,
		cd_estabelecimento_p in number,
		cd_setor_atendimento_p in number,
		ie_via_aplicacao_p in varchar2,
		nr_seq_order_unit_p in number,
		nr_seq_cpoe_rp_p in number,
		dt_start_p out date,
		dt_end_p out date,
		dt_deadline_p out date) is
  
dt_emerg_controle_w		varchar2(1) := 'N';
dt_start_calc_normal_w	date	:= null;
dt_start_calc_emerg_w	date	:= null;
dt_end_calc_emerg_w		date	:= null;

ie_via_aplicacao_w		varchar2(5) := null;
cd_setor_atendimento_w	number(5);
dt_current_date_w		date;
current_weekday_w		number(1);
dt_limite_emerg_w		date;


cursor c04 is
	select	nr_sequencia,
			si_dispensation_day,
			hr_dispensation_time,
			nvl(si_action,'1') si_action,
			nvl(qt_dispensation_days, 0) qt_dispensation_days,
			si_limit_day,
			dt_fixed_start,
			dt_fixed_end,
			dt_fixed_limit,
			hr_limit_time,
			ie_via_aplicacao,
			cd_setor_atendimento,
			hr_business_day_start,
			hr_business_day_end,
			hr_saturday_start,
			hr_saturday_end,
			hr_sunday_start,
			hr_sunday_end,
			hr_holiday_start,
			hr_holiday_end
	from	cpoe_week_dispens_rule
	where	si_action = '4'
	and 	(cd_setor_atendimento = nvl(cd_setor_atendimento_w,cd_setor_atendimento) or cd_setor_atendimento is null)
	and		(ie_via_aplicacao = nvl(ie_via_aplicacao_w, ie_via_aplicacao) or ie_via_aplicacao is null)
	-- and ((dt_reference_pc between dt_fixed_start and dt_fixed_end) )
	order by cd_setor_atendimento, ie_via_aplicacao, dt_fixed_start;

begin
dt_current_date_w := sysdate;

if	(nr_seq_cpoe_rp_p is not null) then
	begin
	select	a.ie_via_aplicacao,
			obter_setor_atendimento(b.nr_atendimento)
	into	ie_via_aplicacao_w,
			cd_setor_atendimento_w
	from	cpoe_order_unit b,
			cpoe_rp a
	where	b.nr_sequencia = a.nr_seq_cpoe_order_unit
	and		a.nr_seq_cpoe_order_unit = nr_seq_order_unit_p
	and 	a.nr_sequencia = nr_seq_cpoe_rp_p;

	exception
		when others then
		begin
		ie_via_aplicacao_w     := null;
		cd_setor_atendimento_w := null;
		end;
	end;
else
	ie_via_aplicacao_w		 := ie_via_aplicacao_p;
	cd_setor_atendimento_w := cd_setor_atendimento_p;
end if;

-- Search for specific rule
for r_c04 in c04 loop
	dt_start_calc_normal_w	:= trunc(dt_reference_p,'dd');
	-- dt_current_date_w       := sysdate;
	-- dt_current_date_w       := to_date('01/04/2021 19:00','dd/mm/yyyy hh24:mi');    
	-- dt_current_date_w       := to_date('31/03/2021 18:30','dd/mm/yyyy hh24:mi');        
	current_weekday_w	      := to_char(dt_current_date_w,'d');

	dt_emerg_controle_w := 'N';       
	--> Dias Uteis. --> periodo 01
	if  (current_weekday_w between 2 and 6) then  --> Mon to Fri.
		dt_start_calc_emerg_w := pkg_date_utils.get_DateTime(dt_current_date_w, pkg_date_utils.get_Time(
						nvl(pkg_date_utils.extract_field('HOUR', r_c04.hr_business_day_start, 0), 0),
						nvl(pkg_date_utils.extract_field('MINUTE', r_c04.hr_business_day_start, 0), 0)));
		dt_end_calc_emerg_w := pkg_date_utils.end_of(dt_current_date_w,'DAY');

		if ( (dt_current_date_w >= dt_start_calc_emerg_w) and (dt_current_date_w <= dt_end_calc_emerg_w)) then
		  dt_emerg_controle_w := 'S';
		end if;
	end if; 
	--> periodo 02
	if  (current_weekday_w between 2 and 6) then  --> Mon to Fri.
		dt_start_calc_emerg_w := pkg_date_utils.start_of(dt_current_date_w,'DAY');
		dt_end_calc_emerg_w := pkg_date_utils.get_DateTime(dt_current_date_w, pkg_date_utils.get_Time(
						nvl(pkg_date_utils.extract_field('HOUR', r_c04.hr_business_day_end, 0), 0),
						nvl(pkg_date_utils.extract_field('MINUTE', r_c04.hr_business_day_end, 0), 0)));

		if ( (dt_current_date_w >= dt_start_calc_emerg_w) and (dt_current_date_w <= dt_end_calc_emerg_w)) then
		  dt_emerg_controle_w := 'S';       
		end if;
	end if;


	--> periodo 01  --> SATURDAY
	if  (current_weekday_w = 7) then  
		dt_start_calc_emerg_w := pkg_date_utils.get_DateTime(dt_current_date_w, pkg_date_utils.get_Time(
						nvl(pkg_date_utils.extract_field('HOUR', r_c04.hr_saturday_start, 0), 0),
						nvl(pkg_date_utils.extract_field('MINUTE', r_c04.hr_saturday_start, 0), 0)));
		dt_end_calc_emerg_w := pkg_date_utils.end_of(dt_current_date_w,'DAY');

		if ( (dt_current_date_w >= dt_start_calc_emerg_w) and (dt_current_date_w <= dt_end_calc_emerg_w)) then
		  dt_emerg_controle_w := 'S';
		end if;

	end if; 
	--> periodo 02
	if  (current_weekday_w = 7) then  
		dt_start_calc_emerg_w := pkg_date_utils.start_of(dt_current_date_w,'DAY');
		dt_end_calc_emerg_w := pkg_date_utils.get_DateTime(dt_current_date_w, pkg_date_utils.get_Time(
						nvl(pkg_date_utils.extract_field('HOUR', r_c04.hr_saturday_end, 0), 0),
						nvl(pkg_date_utils.extract_field('MINUTE', r_c04.hr_saturday_end, 0), 0)));

		if ((dt_current_date_w >= dt_start_calc_emerg_w) and (dt_current_date_w <= dt_end_calc_emerg_w)) then
		  dt_emerg_controle_w := 'S';       
		end if;
    
	end if;

	--> periodo 01 --> SUNDAY
	if  (current_weekday_w = 1) then  
		dt_start_calc_emerg_w := pkg_date_utils.get_DateTime(dt_current_date_w, pkg_date_utils.get_Time(
						nvl(pkg_date_utils.extract_field('HOUR', r_c04.hr_sunday_start, 0), 0),
						nvl(pkg_date_utils.extract_field('MINUTE', r_c04.hr_sunday_start, 0), 0)));
		dt_end_calc_emerg_w := pkg_date_utils.end_of(dt_current_date_w,'DAY');

		if ((dt_current_date_w >= dt_start_calc_emerg_w) and (dt_current_date_w <= dt_end_calc_emerg_w)) then
		  dt_emerg_controle_w := 'S';
		end if;
	end if; 
	--> periodo 02
	if  (current_weekday_w = 1) then  
		dt_start_calc_emerg_w := pkg_date_utils.start_of(dt_current_date_w,'DAY');
		dt_end_calc_emerg_w := pkg_date_utils.get_DateTime(dt_current_date_w, pkg_date_utils.get_Time(
						nvl(pkg_date_utils.extract_field('HOUR', r_c04.hr_sunday_end, 0), 0),
						nvl(pkg_date_utils.extract_field('MINUTE', r_c04.hr_sunday_end, 0), 0)));

		if ((dt_current_date_w >= dt_start_calc_emerg_w) and (dt_current_date_w <= dt_end_calc_emerg_w)) then
		  dt_emerg_controle_w := 'S';       
		end if;
	end if;

	--> periodo 01 --> HOLIDAY
	if  (obter_se_feriado (nvl(cd_estabelecimento_p,1), dt_current_date_w) > 0) then 
		dt_start_calc_emerg_w := pkg_date_utils.get_DateTime(dt_current_date_w, pkg_date_utils.get_Time(
						nvl(pkg_date_utils.extract_field('HOUR', r_c04.hr_holiday_start, 0), 0),
						nvl(pkg_date_utils.extract_field('MINUTE', r_c04.hr_holiday_start, 0), 0)));
		dt_end_calc_emerg_w := pkg_date_utils.end_of(dt_current_date_w,'DAY');

		if ( (dt_current_date_w >= dt_start_calc_emerg_w) and (dt_current_date_w <= dt_end_calc_emerg_w)) then
		  dt_emerg_controle_w := 'S';
		end if;

	end if; 
	--> periodo 02
	if  (obter_se_feriado (nvl(cd_estabelecimento_p,1), dt_current_date_w) > 0) then 
		dt_start_calc_emerg_w := pkg_date_utils.start_of(dt_current_date_w,'DAY');
		dt_end_calc_emerg_w := pkg_date_utils.get_DateTime(dt_current_date_w, pkg_date_utils.get_Time(
						nvl(pkg_date_utils.extract_field('HOUR', r_c04.hr_holiday_end, 0), 0),
						nvl(pkg_date_utils.extract_field('MINUTE', r_c04.hr_holiday_end, 0), 0)));

		if ((dt_current_date_w >= dt_start_calc_emerg_w) and (dt_current_date_w <= dt_end_calc_emerg_w)) then
		  dt_emerg_controle_w := 'S';       
		end if;

	end if;

	-- dt_limite_emerg_w   := pkg_date_utils.end_of( dt_current_date_w + r_c04.qt_dispensation_days, 'DAY' );
	dt_limite_emerg_w   := dt_current_date_w + r_c04.qt_dispensation_days;
  if (to_char(dt_limite_emerg_w,'d') >1) and  (to_char(dt_limite_emerg_w,'d') <=5) then 
     dt_limite_emerg_w   := dt_limite_emerg_w +1 ;
  end if;
  
	while ((instr('6|7|1', to_char(dt_limite_emerg_w,'d')) > 0) or
			(obter_se_feriado (cd_estabelecimento_p, dt_limite_emerg_w)>0)) loop
		dt_limite_emerg_w := dt_limite_emerg_w +1; 
	end loop;

	--> DT reference within dispensation days.
	if dt_emerg_controle_w = 'S' then
		if (dt_reference_p >= dt_current_date_w) and 
		   (dt_reference_p <= dt_limite_emerg_w) then 
			dt_start_p   	:= dt_reference_p;
      --dt_end_p	  	:= (dt_reference_p+1)-(1/(24*60));    --Funciona para TC1, TC2 e TC3   
      dt_end_p := pkg_date_utils.get_DateTime(trunc(dt_limite_emerg_w), pkg_date_utils.get_Time(
              nvl(pkg_date_utils.extract_field('HOUR'  , dt_reference_p, 0), 0),
              nvl(pkg_date_utils.extract_field('MINUTE', dt_reference_p, 0), 0)));      
			dt_end_p	  	:= dt_end_p-(1/(24*60));
      
			dt_deadline_p	:= dt_start_calc_emerg_w-(1/(24*60));
    
		end if;  
	else
		dt_emerg_controle_w := 'N';
		dt_start_p    := null;
		dt_end_p      := null;
		dt_deadline_p := null;
	end if;

	exit;
end loop;

end CPOE_GET_EMERGENCY_DISP_RULE;
/
