create or replace
procedure atualizar_com_canal_hist(
		nr_sequencia_p	number,
		ie_opcao_p	varchar2,
		nm_usuario_p	varchar2) is 


begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	if	('L' = ie_opcao_p) then	
		begin
		update	com_canal_hist
		set	dt_liberacao 	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia 	= nr_sequencia_p;
		end;
	elsif	('D' = ie_opcao_p) then
		begin
		update	com_canal_hist
		set	dt_liberacao 	= null,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia 	= nr_sequencia_p;
		end;
	end if;
	
	end;
end if;
commit;
end atualizar_com_canal_hist;
/