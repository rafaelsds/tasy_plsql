create or replace
procedure eme_log_email_boletos(nm_usuario_p 	varchar2,
			 nr_seq_faturamento_p 	number,
			 nr_seq_contrato_p 		number) is 

ds_contato_w	eme_contrato_email.ds_contato%type;
ds_email_w		eme_contrato_email.ds_email%type;

Cursor c01 is
select  a.ds_contato, a.ds_email
from eme_contrato_email a,
    eme_contrato b
where a.nr_seq_contrato = nr_seq_contrato_p
and b.nr_sequencia = a.nr_seq_contrato;

begin

open c01;
loop fetch c01 into  
	ds_contato_w,
	ds_email_w;
exit when c01%notfound;

	insert into eme_fat_boleto_log(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_faturamento,
		ds_contato,
		ds_email)
	values (
		eme_fat_boleto_log_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_faturamento_p,
		ds_contato_w,
		ds_email_w);

end loop;
close c01;

commit;

end eme_log_email_boletos;
/
