create or replace
procedure  intpd_recebe_moeda (	nr_sequencia_p	in	number,
								xml_p			in	xmltype ) is 


ie_conversao_w				intpd_eventos_sistema.ie_conversao%type;
nr_seq_sistema_w			intpd_eventos_sistema.nr_seq_sistema%type;	
nr_seq_projeto_xml_w		intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_regra_w				conversao_meio_externo.nr_seq_regra%type;
ie_sistema_externo_w		varchar2(15);
reg_integracao_w			gerar_int_padrao.reg_integracao_conv;
moeda_w						moeda%rowtype;
ds_erro_w					varchar2(4000);
qt_reg_w					number(10);				
								
cursor c01 is
select	*
from	xmltable('/STRUCTURE/CURRENCY' passing xml_p columns
		ie_acao							varchar2(15)	path	'IE_ACTION',
		ds_moeda						varchar2(30)	path	'DS_CURRRENCY',
		ds_sigla_moeda					varchar2(3)		path	'DS_CURRRENCY_ABBREVIATION',
		ie_situacao						varchar2(1)		path	'IE_STATUS',
		nm_usuario						varchar2(15)	path	'NM_USER', 	
		cd_moeda_banco					number(3)		path	'CD_BANK_CURRENCY',
		ie_conv_cot_compra				varchar2(1)		path	'IE_CONV_PURCHASE_QUOTATION',
		ie_exige_conv_oc				varchar2(1)		path	'IE_CONVERSION_PURCHASE_ORDER');
	
c01_w	c01%rowtype;								

begin

/*'Atualiza o status da fila para Em processamento'*/
update	intpd_fila_transmissao
set		ie_status 		= 'R'
where	nr_sequencia 	= nr_sequencia_p;

/*'Realiza o commit para não permite o status de processamento em casa de rollback por existir consistência. Existe tratamento de exceção abaixo para colocar o status de erro em caso de falha'*/
commit;


begin

select	nvl(max(b.ie_conversao),'I'),
		max(nr_seq_sistema),
		max(nr_seq_projeto_xml),
		max(nr_seq_regra_conv)
into	ie_conversao_w,
		nr_seq_sistema_w,
		nr_seq_projeto_xml_w,
		nr_seq_regra_w
from	intpd_fila_transmissao a,
		intpd_eventos_sistema b
where	a.nr_seq_evento_sistema = b.nr_sequencia
and		a.nr_sequencia 			= nr_sequencia_p;

ie_sistema_externo_w	:=	nr_seq_sistema_w;

/*'Alimenta as informações iniciais de controle e consistência de cada atributo do XML'*/
reg_integracao_w.nr_seq_fila_transmissao	:=	nr_sequencia_p;
reg_integracao_w.ie_envio_recebe			:=	'R';
reg_integracao_w.ie_sistema_externo			:=	ie_sistema_externo_w;
reg_integracao_w.ie_conversao				:=	ie_conversao_w;
reg_integracao_w.nr_seq_projeto_xml			:=	nr_seq_projeto_xml_w;
reg_integracao_w.nr_seq_regra_conversao		:=	nr_seq_regra_w;
reg_integracao_w.intpd_log_receb.delete;
reg_integracao_w.qt_reg_log					:=	0;

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	reg_integracao_w.nm_tabela			:=	'MOEDA';
	reg_integracao_w.nm_elemento		:=	'CURRENCY';
	reg_integracao_w.nr_seq_visao		:=	'';
	
	select	count(*)
	into	qt_reg_w
	from	moeda
	where	upper(ds_moeda) = upper(c01_w.ds_moeda);
	
	if (qt_reg_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(764166,'ds_moeda='||c01_w.ds_moeda);
	end if;
	
	intpd_processar_atributo(reg_integracao_w, 'DS_MOEDA', c01_w.ds_moeda, 'N', moeda_w.ds_moeda);
	intpd_processar_atributo(reg_integracao_w, 'DS_SIGLA_MOEDA', c01_w.ds_sigla_moeda, 'N', moeda_w.ds_sigla_moeda);
	intpd_processar_atributo(reg_integracao_w, 'IE_SITUACAO', c01_w.ie_situacao, 'N', moeda_w.ie_situacao);
	intpd_processar_atributo(reg_integracao_w, 'NM_USUARIO', c01_w.nm_usuario, 'N', moeda_w.nm_usuario);
	intpd_processar_atributo(reg_integracao_w, 'CD_MOEDA_BANCO', c01_w.cd_moeda_banco, 'N', moeda_w.cd_moeda_banco);
	intpd_processar_atributo(reg_integracao_w, 'IE_CONV_COT_COMPRA', c01_w.ie_conv_cot_compra, 'N', moeda_w.ie_conv_cot_compra);
	intpd_processar_atributo(reg_integracao_w, 'IE_EXIGE_CONV_OC', c01_w.ie_exige_conv_oc, 'N', moeda_w.ie_exige_conv_oc);

	if	(reg_integracao_w.qt_reg_log = 0) then
		begin				
		select	nvl(max(cd_moeda),0) + 1
		into	moeda_w.cd_moeda
		from	moeda;

		moeda_w.dt_atualizacao := sysdate;
		
		insert into moeda values moeda_w;			
		end;
	end if;
	
	end;
end loop;
close C01;
exception
when others then
	begin
	/*'Em caso de qualquer falha o sistema captura a mensagem de erro, efetua o rollback, atualiza o status para Erro e registra a falha ocorrida'*/
	ds_erro_w	:=	substr(sqlerrm,1,4000);
	rollback;
	update	intpd_fila_transmissao
	set		ie_status 		= 'E',
			ds_log 			= ds_erro_w
	where	nr_sequencia 	= nr_sequencia_p;
	end;
end;

if	(reg_integracao_w.qt_reg_log > 0) then
	begin
	/*'Em caso de qualquer consistência o sistema efetua rollback, atualiza o status para Erro e registra todos os logs de consistência'*/
	rollback;
	
	update	intpd_fila_transmissao
	set		ie_status 		= 'E',
			ds_log 			= ds_erro_w
	where	nr_sequencia 	= nr_sequencia_p;
	
	for i in 0..reg_integracao_w.qt_reg_log-1 loop
		intpd_gravar_log_recebimento(nr_sequencia_p,reg_integracao_w.intpd_log_receb(i).ds_log,'INTPDTASY');
	end loop;
	end;
end if;

commit;

end intpd_recebe_moeda;
/