create or replace 
procedure sup_atualizar_pedido_reg_kit(	nr_seq_registro_p	number,
					nr_seq_pedido_p		number,
					ds_registro_kit_p 	varchar2 ) is

begin

if	(nvl(nr_seq_registro_p,0) > 0) then

	update	kit_estoque_reg
	set	nr_seq_pedido_agenda = nr_seq_pedido_p,
		ds_registro_kit = substr(ds_registro_kit_p,1,255)
	where	nr_sequencia = nr_seq_registro_p;
	
	commit;

end if;

end sup_atualizar_pedido_reg_kit;
/