create or replace
procedure gerar_inv_barras_conversao(
				nr_inventario_p			number,
				cd_local_estoque_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2,
				ds_erro_p		out	varchar2) is

/* Procedure utilizada na rotina do menu item "Inventário por barras conversão" no HTML.
Menu item da função "Administração de Estoque" da aba "Inventário estoque" e "Inventário consignado". */

ds_erro_w			varchar2(2000);
ie_tipo_entrada_w		varchar2(1);
dt_mesano_referencia_w		date;
qt_estoque_w                	number(15,4) := 0;
qt_material_estoque_w		number(15,4);
qt_material_w			number(15,4);
nr_movimento_w			movimento_estoque.nr_movimento_estoque%type;
cd_operacao_estoque_w		operacao_estoque.cd_operacao_estoque%type;
cd_material_w			material.cd_material%type;
cd_fornecedor_w			pessoa_juridica.cd_cgc%type;
nr_seq_lote_fornec_w	material_lote_fornec.nr_sequencia%type;

cursor C01 is
	select	cd_material_estoque,
		sum(qt_inventario),
		cd_fornecedor,
		nr_seq_lote_fornec
	from	w_inventario_barra
	where	nm_usuario = nm_usuario_p
	and	cd_local_estoque = cd_local_estoque_p
	and	cd_estabelecimento = cd_estabelecimento_p
	group by cd_estabelecimento,
		cd_local_estoque,
		cd_material_estoque,
		cd_fornecedor,
		nr_seq_lote_fornec;

begin
dt_mesano_referencia_w := trunc(sysdate,'mm');

open c01;
loop
fetch c01 into
	cd_material_w,
	qt_material_w,
	cd_fornecedor_w,
	nr_seq_lote_fornec_w;
exit when c01%notfound;
	begin
	if	(nvl(cd_fornecedor_w,'X') = 'X') and (obter_se_mat_consignado(cd_material_w) in ('0','2')) then
		begin
		Obter_Saldo_Estoque(cd_estabelecimento_p,cd_material_w,cd_local_estoque_p,dt_mesano_referencia_w,qt_estoque_w);
		
		select	(qt_material_w - qt_estoque_w)
		into	qt_material_estoque_w
		from	dual;
		
		if	(nvl(qt_material_estoque_w,0) > 0) then
			ie_tipo_entrada_w := 'E';
		else
			ie_tipo_entrada_w := 'S';
			qt_material_estoque_w := (qt_material_estoque_w * -1);
		end if;
		
		if	(nvl(ie_tipo_entrada_w,'X') <> 'X') then
			select	min(cd_operacao_estoque)
			into	cd_operacao_estoque_w
			from	operacao_estoque
			where	ie_tipo_requisicao = 5
			and	ie_entrada_saida = ie_tipo_entrada_w
			and	ie_consignado	= '0'
			and	ie_situacao	= 'A';
		end if;
		end;
	elsif (nvl(cd_fornecedor_w,'X') <> 'X') and (obter_se_mat_consignado(cd_material_w) in ('1','2')) then
		begin
		qt_estoque_w := obter_saldo_estoque_consig(cd_estabelecimento_p,cd_fornecedor_w,cd_material_w,cd_local_estoque_p);
		
		select	(qt_material_w - qt_estoque_w)
		into	qt_material_estoque_w
		from	dual;
		
		if	(nvl(qt_material_estoque_w,0) > 0) then
			ie_tipo_entrada_w := 'E';
		else
			ie_tipo_entrada_w := 'S';
			qt_material_estoque_w := (qt_material_estoque_w * -1);
		end if;
		
		if	(nvl(ie_tipo_entrada_w,'X') <> 'X') then
			select	min(cd_operacao_estoque)
			into	cd_operacao_estoque_w
			from	operacao_estoque
			where	ie_tipo_requisicao = 5
			and	ie_entrada_saida = ie_tipo_entrada_w
			and	ie_consignado = '7'
			and	ie_situacao	= 'A';
		end if;
		end;
	else
		goto final;
	end if;
	
	if	(nvl(cd_operacao_estoque_w,0) <> 0) and (qt_material_estoque_w > 0) then
		begin
		select	movimento_estoque_seq.nextval
		into	nr_movimento_w
		from	dual;
		
		begin
		insert into movimento_estoque(
			nr_movimento_estoque,
			cd_estabelecimento,
			cd_local_estoque,
			dt_movimento_estoque,
			cd_operacao_estoque,
			cd_acao,
			cd_material,
			dt_mesano_referencia,
			qt_movimento,
			dt_atualizacao,
			nm_usuario,
			ie_origem_documento,
			ie_origem_proced,
			qt_estoque,
			dt_processo,
			cd_material_estoque,
			qt_inventario,
			ie_movto_consignado,
			nr_documento,
			nr_seq_lote_fornec,
			cd_fornecedor)
		values( nr_movimento_w,
			cd_estabelecimento_p,
			cd_local_estoque_p,
			sysdate,
			cd_operacao_estoque_w,
			1,
			cd_material_w,
			dt_mesano_referencia_w,
			qt_material_estoque_w,
			sysdate,
			nm_usuario_p,
			5,
			1,
			qt_material_estoque_w,
			null,
			cd_material_w,
			qt_material_w,
			'N',
			nr_inventario_p,
			nr_seq_lote_fornec_w,
			cd_fornecedor_w);
		exception
		when others then
			ds_erro_w := substr(sqlerrm,1,2000);
		end;
		end;
	end if;
	
	end;
end loop;
close c01;

<<final>>

ds_erro_p := ds_erro_w;

commit;

end gerar_inv_barras_conversao;
/