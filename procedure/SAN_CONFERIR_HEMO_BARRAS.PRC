create or replace
procedure SAN_CONFERIR_HEMO_BARRAS(
			nr_seq_producao_p		number,
			nm_usuario_p		Varchar2) is 

begin
if	(nr_seq_producao_p is not null) then
	update	san_producao		
	set	dt_conf_transf 		= sysdate,
		nm_usuario_conf_trans 	= nm_usuario_p
	where	nr_sequencia 		= nr_seq_producao_p;
end if;
commit;

end san_conferir_hemo_barras;
/