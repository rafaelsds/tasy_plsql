create or replace
procedure oft_insere_motilidade_ocular (	nr_seq_consulta_p			number,
														nr_seq_consulta_form_p	number,
														vListaMotilidade			strRecTypeFormOft) is

nr_sequencia_w				oft_motilidade_ocular.nr_sequencia%type;
dt_exame_w					oft_motilidade_ocular.dt_exame%type;
ds_observacao_w			oft_motilidade_ocular.ds_observacao%type;
vl_od_sup_esq_w			oft_motilidade_ocular.vl_od_sup_esq%type;
vl_od_sup_dir_w			oft_motilidade_ocular.vl_od_sup_dir%type;
vl_od_centro_esq_w		oft_motilidade_ocular.vl_od_centro_esq%type;
vl_od_centro_dir_w		oft_motilidade_ocular.vl_od_centro_dir%type;
vl_od_inferior_esq_w		oft_motilidade_ocular.vl_od_inferior_esq%type;
vl_od_inferior_dir_w		oft_motilidade_ocular.vl_od_inferior_dir%type;
vl_oe_sup_esq_w			oft_motilidade_ocular.vl_oe_sup_esq%type;
vl_oe_sup_dir_w			oft_motilidade_ocular.vl_oe_sup_dir%type;
vl_oe_centro_esq_w		oft_motilidade_ocular.vl_oe_centro_esq%type;
vl_oe_centro_dir_w		oft_motilidade_ocular.vl_oe_centro_dir%type;
vl_oe_inferior_esq_w		oft_motilidade_ocular.vl_oe_inferior_esq%type;
vl_oe_inferior_dir_w		oft_motilidade_ocular.vl_oe_inferior_dir%type;
ie_aval_mov_ocular_w		oft_motilidade_ocular.ie_aval_mov_ocular%type;
ie_mov_sc_perto_w			oft_motilidade_ocular.ie_mov_sc_perto%type;
ie_mov_sc_longe_w			oft_motilidade_ocular.ie_mov_sc_longe%type;
ie_mov_sc_infra_w			oft_motilidade_ocular.ie_mov_sc_infra%type;
ie_mov_cc_perto_w			oft_motilidade_ocular.ie_mov_cc_perto%type;
ie_mov_cc_longe_w			oft_motilidade_ocular.ie_mov_cc_longe%type;
ie_mov_cc_infra_w			oft_motilidade_ocular.ie_mov_cc_infra%type;
ds_teste_prisma_w			oft_motilidade_ocular.ds_teste_prisma%type;
ds_fixacao_binocular_w	oft_motilidade_ocular.ds_fixacao_binocular%type;
ie_med_estrab_sv_w		oft_motilidade_ocular.ie_med_estrab_sv%type;	
ie_med_estrab_di_w		oft_motilidade_ocular.ie_med_estrab_di%type;
ie_med_estrab_dv_w		oft_motilidade_ocular.ie_med_estrab_dv%type;
ie_med_estrab_in_w		oft_motilidade_ocular.ie_med_estrab_in%type;
ie_med_estrab_lv_w		oft_motilidade_ocular.ie_med_estrab_lv%type;
ie_med_estrab_li_w		oft_motilidade_ocular.ie_med_estrab_li%type;
ie_med_estrab_oh_w		oft_motilidade_ocular.ie_med_estrab_oh%type;
ds_mosca_w					oft_motilidade_ocular.ds_mosca%type;
ds_circulos_w				oft_motilidade_ocular.ds_circulos%type;
ds_bichos_w					oft_motilidade_ocular.ds_bichos%type;
ds_cores_w					oft_motilidade_ocular.ds_cores%type;
ds_contuda_w				oft_motilidade_ocular.ds_contuda%type;
ds_titmos_w					oft_motilidade_ocular.ds_titmos%type;
cd_profissional_w			oft_motilidade_ocular.cd_profissional%type;	
ie_preferencia_w        oft_motilidade_ocular.ie_preferencia%type;	
nm_usuario_w				usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
ie_registrado_w			varchar2(1) := 'N';
ds_erro_w					varchar2(4000);
											
begin
begin

if	(nvl(nr_seq_consulta_p,0) > 0) and (vListaMotilidade.count > 0) then
	for i in 1..vListaMotilidade.count loop
		begin
		if	(vListaMotilidade(i).ds_valor is not null) or 
			(vListaMotilidade(i).nr_valor is not null) then
			case upper(vListaMotilidade(i).nm_campo)
				when 'CD_PROFISSIONAL' then
					cd_profissional_w							:= vListaMotilidade(i).ds_valor;
				when 'DT_EXAME' then
					dt_exame_w 									:= pkg_date_utils.get_DateTime(vListaMotilidade(i).ds_valor);
				when 'DS_OBSERVACAO' then
					ds_observacao_w							:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OD_SUP_ESQ' then
					vl_od_sup_esq_w							:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OD_SUP_DIR' then
					vl_od_sup_dir_w							:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OD_CENTRO_ESQ' then
					vl_od_centro_esq_w						:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OD_CENTRO_DIR' then
					vl_od_centro_dir_w						:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OD_INFERIOR_ESQ' then
					vl_od_inferior_esq_w						:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OD_INFERIOR_DIR' then
					vl_od_inferior_dir_w						:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OE_SUP_ESQ' then
					vl_oe_sup_esq_w							:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OE_SUP_DIR' then
					vl_oe_sup_dir_w							:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OE_CENTRO_ESQ' then
					vl_oe_centro_esq_w						:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OE_CENTRO_DIR' then
					vl_oe_centro_dir_w						:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OE_INFERIOR_ESQ' then
					vl_oe_inferior_esq_w						:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'VL_OE_INFERIOR_DIR' then
					vl_oe_inferior_dir_w						:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'IE_AVAL_MOV_OCULAR' then
					ie_aval_mov_ocular_w						:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MOV_SC_PERTO' then
					ie_mov_sc_perto_w							:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MOV_SC_LONGE' then
					ie_mov_sc_longe_w							:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MOV_SC_INFRA' then
					ie_mov_sc_infra_w							:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MOV_CC_PERTO' then
					ie_mov_cc_perto_w							:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MOV_CC_LONGE' then
					ie_mov_cc_longe_w							:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MOV_CC_INFRA' then
					ie_mov_cc_infra_w							:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'DS_TESTE_PRISMA' then
					ds_teste_prisma_w							:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'DS_FIXACAO_BINOCULAR' then
					ds_fixacao_binocular_w					:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MED_ESTRAB_SV' then
					ie_med_estrab_sv_w						:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MED_ESTRAB_DI' then
					ie_med_estrab_di_w						:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MED_ESTRAB_DV' then
					ie_med_estrab_dv_w						:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MED_ESTRAB_IN' then
					ie_med_estrab_in_w						:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MED_ESTRAB_LV' then
					ie_med_estrab_lv_w						:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MED_ESTRAB_LI' then
					ie_med_estrab_li_w						:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'IE_MED_ESTRAB_OH' then
					ie_med_estrab_oh_w						:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'DS_MOSCA' then
					ds_mosca_w									:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'DS_CIRCULOS' then
					ds_circulos_w								:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'DS_BICHOS' then
					ds_bichos_w									:= vListaMotilidade(i).nr_valor;
					ie_registrado_w							:= 'S';
				when 'DS_CORES' then
					ds_cores_w									:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'DS_CONTUDA' then
					ds_contuda_w								:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
				when 'DS_TITMOS' then
					ds_titmos_w									:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';
            when 'IE_PREFERENCIA' then
					ie_preferencia_w							:= vListaMotilidade(i).ds_valor;
					ie_registrado_w							:= 'S';   
				else
					null;	
			end case;	
		end if;	
	end;
	end loop;
	
	select	max(nr_sequencia)
	into		nr_sequencia_w
	from		oft_motilidade_ocular
	where		nr_seq_consulta_form = nr_seq_consulta_form_p
	and		nr_seq_consulta		= nr_seq_consulta_p
	and		dt_liberacao 			is null
	and		nm_usuario				= nm_usuario_w;
	
	if	(nr_sequencia_w is not null) then
		update	oft_motilidade_ocular
		set		dt_atualizacao			=	sysdate, 
					nm_usuario				=	nm_usuario_w, 
					cd_profissional		= nvl(cd_profissional_w,cd_profissional),	
					dt_exame					=	nvl(dt_exame_w,dt_exame), 
					ds_observacao			=	ds_observacao_w,
					vl_od_sup_esq			=	vl_od_sup_esq_w, 
					vl_od_sup_dir			=	vl_od_sup_dir_w, 
					vl_od_centro_esq		=	vl_od_centro_esq_w, 
					vl_od_centro_dir		=	vl_od_centro_dir_w, 
					vl_od_inferior_esq	=	vl_od_inferior_esq_w, 
					vl_od_inferior_dir	=	vl_od_inferior_dir_w, 
					vl_oe_sup_esq			=	vl_oe_sup_esq_w, 
					vl_oe_sup_dir			=	vl_oe_sup_dir_w, 
					vl_oe_centro_esq		=	vl_oe_centro_esq_w, 
					vl_oe_centro_dir		=	vl_oe_centro_dir_w, 
					vl_oe_inferior_esq	=	vl_oe_inferior_esq_w, 
					vl_oe_inferior_dir	=	vl_oe_inferior_dir_w, 
					ie_aval_mov_ocular	=	ie_aval_mov_ocular_w, 
					ie_mov_sc_perto		=	ie_mov_sc_perto_w, 
					ie_mov_sc_longe		=	ie_mov_sc_longe_w, 
					ie_mov_sc_infra		=	ie_mov_sc_infra_w, 
					ie_mov_cc_perto		=	ie_mov_cc_perto_w, 
					ie_mov_cc_longe		=	ie_mov_cc_longe_w, 
					ie_mov_cc_infra		=	ie_mov_cc_infra_w, 
					ds_teste_prisma		=	ds_teste_prisma_w, 
					ds_fixacao_binocular	=	ds_fixacao_binocular_w, 
					ie_med_estrab_sv		=	ie_med_estrab_sv_w, 
					ie_med_estrab_di		=	ie_med_estrab_di_w, 
					ie_med_estrab_dv		=	ie_med_estrab_dv_w, 
					ie_med_estrab_in		=	ie_med_estrab_in_w, 
					ie_med_estrab_lv		=	ie_med_estrab_lv_w, 
					ie_med_estrab_li		=	ie_med_estrab_li_w, 
					ie_med_estrab_oh		=	ie_med_estrab_oh_w, 
					ds_mosca					=	ds_mosca_w, 
					ds_circulos				=	ds_circulos_w, 
					ds_bichos				=	ds_bichos_w, 
					ds_cores					=	ds_cores_w, 
					ds_contuda				=	ds_contuda_w, 
					ds_titmos				=	ds_titmos,
               ie_preferencia       =  ie_preferencia_w
		where		nr_sequencia			=	nr_sequencia_w;	
		wheb_usuario_pck.set_ie_commit('S');
	else
		if	(ie_registrado_w = 'S') then
			wheb_usuario_pck.set_ie_commit('S');
			select	oft_motilidade_ocular_seq.nextval
			into		nr_sequencia_w	
			from		dual;

			insert	into oft_motilidade_ocular
													(	nr_sequencia, 
														dt_atualizacao, 
														nm_usuario, 
														dt_atualizacao_nrec, 
														nm_usuario_nrec, 
														cd_profissional, 
														dt_exame,
														nr_seq_consulta, 
														ie_situacao,
														ds_observacao,
														vl_od_sup_esq, 
														vl_od_sup_dir, 
														vl_od_centro_esq, 
														vl_od_centro_dir, 
														vl_od_inferior_esq, 
														vl_od_inferior_dir, 
														vl_oe_sup_esq, 
														vl_oe_sup_dir, 
														vl_oe_centro_esq, 
														vl_oe_centro_dir, 
														vl_oe_inferior_esq, 
														vl_oe_inferior_dir, 
														ie_aval_mov_ocular, 
														ie_mov_sc_perto, 
														ie_mov_sc_longe, 
														ie_mov_sc_infra, 
														ie_mov_cc_perto, 
														ie_mov_cc_longe, 
														ie_mov_cc_infra, 
														ds_teste_prisma, 
														ds_fixacao_binocular, 
														ie_med_estrab_sv, 
														ie_med_estrab_di, 
														ie_med_estrab_dv, 
														ie_med_estrab_in, 
														ie_med_estrab_lv, 
														ie_med_estrab_li, 
														ie_med_estrab_oh, 
														ds_mosca, 
														ds_circulos, 
														ds_bichos, 
														ds_cores, 
														ds_contuda, 
														ds_titmos,
														nr_seq_consulta_form,
                                          ie_preferencia
														)
			values								(	nr_sequencia_w, 
														sysdate, 
														nm_usuario_w, 
														sysdate, 
														nm_usuario_w, 
														nvl(cd_profissional_w,obter_pf_usuario(nm_usuario_w,'C')), 
														nvl(dt_exame_w,sysdate), 
														nr_seq_consulta_p,
														'A',
														ds_observacao_w,
														vl_od_sup_esq_w,
														vl_od_sup_dir_w,
														vl_od_centro_esq_w,
														vl_od_centro_dir_w,
														vl_od_inferior_esq_w,
														vl_od_inferior_dir_w,
														vl_oe_sup_esq_w,
														vl_oe_sup_dir_w,
														vl_oe_centro_esq_w,
														vl_oe_centro_dir_w,
														vl_oe_inferior_esq_w,
														vl_oe_inferior_dir_w,
														ie_aval_mov_ocular_w,
														ie_mov_sc_perto_w,
														ie_mov_sc_longe_w,
														ie_mov_sc_infra_w,
														ie_mov_cc_perto_w,
														ie_mov_cc_longe_w,
														ie_mov_cc_infra_w,
														ds_teste_prisma_w,
														ds_fixacao_binocular_w,
														ie_med_estrab_sv_w,
														ie_med_estrab_di_w,
														ie_med_estrab_dv_w,
														ie_med_estrab_in_w,
														ie_med_estrab_lv_w,
														ie_med_estrab_li_w,
														ie_med_estrab_oh_w,
														ds_mosca_w,
														ds_circulos_w,
														ds_bichos_w,
														ds_cores_w,
														ds_contuda_w,
														ds_titmos_w,
														nr_seq_consulta_form_p,
                                          ie_preferencia_w);
		end if;												
	end if;											
end if;	

exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,4000);
	update	OFT_CONSULTA_FORMULARIO
	set		ds_stack			=	substr(dbms_utility.format_call_stack||ds_erro_w,1,4000)
	where		nr_sequencia	= 	nr_seq_consulta_form_p;
end;

end oft_insere_motilidade_ocular;
/