create or replace
procedure Obter_Se_consiste_SN(	cd_material_p		number,
				ie_permite_sn_p	OUT	varchar2,
				nm_usuario_p		Varchar2) is 

cd_grupo_material_w		Number(10,0);
cd_subgrupo_material_w		Number(10,0);
cd_classe_material_w		Number(10,0);
ie_permite_sn_w			varchar2(10)	:= 'S';

cursor c01 is
select	ie_permite_sn
from	regra_prescr_medic_sn
where	nvl(cd_material,cd_material_p)			= cd_material_p
and	nvl(cd_grupo_material,cd_grupo_material_w)	= cd_grupo_material_w
and	nvl(cd_subgrupo_material,cd_subgrupo_material_w) = cd_subgrupo_material_w
and	nvl(cd_classe_material,cd_classe_material_w)	= cd_classe_material_w
order by nvl(cd_grupo_material,0),
	nvl(cd_subgrupo_material,0),
	nvl(cd_classe_material,0),
	nvl(cd_material,0);

begin

select 	cd_grupo_material,
	cd_subgrupo_material,
	cd_classe_material
into	cd_grupo_material_w,
	cd_subgrupo_material_w,
	cd_classe_material_w
from 	estrutura_material_v
where 	cd_material			= cd_material_p;

open C01;
loop
fetch C01 into	
	ie_permite_sn_w;
exit when C01%notfound;
	ie_permite_sn_w	:= ie_permite_sn_w;
end loop;
close C01;

ie_permite_sn_p	:= ie_permite_sn_w;

end Obter_Se_consiste_SN;
/
