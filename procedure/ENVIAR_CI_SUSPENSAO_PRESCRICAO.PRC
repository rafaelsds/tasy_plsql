create or replace
procedure enviar_ci_suspensao_prescricao(nr_prescricao_p	number,
					 nm_usuario_p		varchar2) is
--- Declara��o das vari�veis
nr_seq_regra_w		number(10);
ds_titulo_w		varchar2(255);
ds_mensagem_w		varchar2(2000);
cd_perfil_w		number(10);
cd_perfil_destino_w	varchar2(2000);
nr_seq_classif_w	number(10);
nr_seq_comunic_w	number(10);
ie_tipo_Atendimento_w	number(10);
--- Cursor que busca as mensagens e os titulos
cursor c01 is 
select	nr_sequencia,
	ds_titulo,
	ds_mensagem
from 	rep_ci_suspensao
where	nvl(ie_tipo_Atendimento,ie_tipo_Atendimento_w)	= ie_tipo_Atendimento_w
order by
	1;
--- Cursor que busca os perfis que ir�o receber a mensagem
cursor c02 is 
select	cd_perfil
from	rep_ci_suspensao_perfil
where 	nr_seq_regra = nr_seq_regra_w
order by
	1;

begin
--- Busca a sequencia da classifica��o da CI
select	max(nr_sequencia) 
into	nr_seq_classif_w
from	comunic_interna_classif 
where	ie_tipo	= 'F'
and 	ie_situacao = 'A';

select	max(obter_tipo_atendimento(nr_atendimento))
into	ie_tipo_Atendimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;
--- Inicia o cursor de busca do titulo e da mensagem da CI
open c01;
loop
fetch c01 into
	nr_seq_regra_w,
	ds_titulo_w,
	ds_mensagem_w;
exit when c01%notfound;
	begin
	--- Inicia o cursor de busca dos perfis que ir�o receber a mensagem
	open c02;
	loop
	fetch c02 into
		cd_perfil_w;
	exit when c02%notfound;
		begin
		--- Verifica se a variavel de perfis de destino n�o � nula e adiciona uma virgula para separar o
		if	(cd_perfil_destino_w is not null) or
			(cd_perfil_destino_w <> '') and 
			(cd_perfil_w is not null)then
			cd_perfil_destino_w := cd_perfil_destino_w || ',';
		end if;
		--- Adiciona o perfil que retornou no  cursor
		cd_perfil_destino_w := substr(cd_perfil_destino_w || cd_perfil_w,1,1000);	
		end;
	end loop;
	close c02;
	--- V erifica se a mensagem n�o � null e pula uma linha.
	if	(ds_mensagem_w is not null) or
		(ds_mensagem_w <> '') then
		ds_mensagem_w := ds_mensagem_w||chr(13);
	end if;
	--- Adiciona o n�mero da prescri��o na mensagem
	ds_mensagem_w := ds_mensagem_w || ' ' || obter_desc_expressao(493955) || ': ' || nr_prescricao_p;  
	--- Verifica se o titulo n�o � nulo
	if	(ds_titulo_w is not null) and 
		(cd_perfil_destino_w is not null)then
			--- Busca a pr�xima sequencia na tabela
			select	comunic_interna_seq.nextval
			into	nr_seq_comunic_w
			from	dual;
			insert	into comunic_interna(	
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				nm_usuario_destino,
				dt_atualizacao,
				ie_geral,
				ie_gerencial,
				ds_perfil_adicional,
				nr_sequencia,
				nr_seq_classif,
				dt_liberacao)
			values(
				sysdate,
				ds_titulo_w,
				ds_mensagem_w, 
				nm_usuario_p,
				null,
				sysdate,
				'N',
				'N',
				cd_perfil_destino_w,
				nr_seq_comunic_w,
				nr_seq_classif_w,
				sysdate);
			if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
	end if;
	end;
end loop;
close c01; 
end enviar_ci_suspensao_prescricao;
/
