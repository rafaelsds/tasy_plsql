create or replace
procedure gera_alerta_gerais_mon( nr_seq_alteracao_p	integer,
				  nm_usuario_p		varchar2 ) is 

begin

	insert	into mon_cad_gerais_ciente(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_alteracao)
	values (mon_cad_gerais_ciente_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_alteracao_p);

commit;

end gera_alerta_gerais_mon;
/