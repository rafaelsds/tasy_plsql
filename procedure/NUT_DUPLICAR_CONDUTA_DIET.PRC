create or replace
procedure Nut_Duplicar_Conduta_Diet(	nr_seq_serv_dia_dieta_p	Number,
					nm_usuario_p		Varchar2) is

nr_seq_serv_dia_w	Number(10);
ie_tipo_nutricao_w	Varchar2(15);
dt_servico_w		Date;
ds_mensagem_w		Varchar2(1000);
nr_seq_produto_w	Number(10);
ds_lista_suplemento_w	Varchar2(1000);
ds_lista_enteral_w	Varchar2(1000);
nr_seq_dieta_w		Number(10);
ie_via_aplicacao_w	Varchar2(5);
cd_unidade_medida_w 	Varchar2(30);
qt_dose_w		Number(15,3);
cd_intervalo_w		Varchar2(7);
hr_prim_horario_w	Varchar2(5);
ds_horarios_w		Varchar2(2000);
ds_observacao_w		Varchar2(4000);
nr_seq_leite_deriv_w	nut_atend_serv_dia_dieta.nr_seq_leite_deriv%type;
qt_volume_oral_w	nut_atend_serv_dia_dieta.qt_volume_oral%type;
qt_vel_infusao_w	nut_atend_serv_dia_dieta.qt_vel_infusao%type;
nr_seq_disp_succao_w	nut_atend_serv_dia_dieta.nr_seq_disp_succao%type;

cursor c01 is
	select	a.nr_sequencia,
		b.cd_material,
		b.qt_porcentagem,
		b.qt_dose
	from	nutricao_leite_deriv_adic a,
		nut_atend_serv_dia_dieta b
	where	a.cd_material		= b.cd_material
	and	a.nr_seq_produto	= nvl(nr_seq_leite_deriv_w,0)
	and	b.nr_sequencia_diluicao = nr_seq_serv_dia_dieta_p;

begin
if (nr_seq_serv_dia_dieta_p is not null) then

	select	max(a.nr_sequencia),
		max(trunc(dt_servico))
	into	nr_seq_serv_dia_w,
		dt_servico_w
	from	nut_atend_serv_dia a,
		nut_atend_serv_dieta b,
		nut_atend_serv_dia_dieta c
	where	a.nr_sequencia	= b.nr_seq_servico
	and	b.nr_seq_dieta 	= c.nr_sequencia
	and	c.nr_sequencia	= nr_seq_serv_dia_dieta_p;

	select	nr_seq_produto,
		nr_seq_leite_deriv,
		ie_tipo_nutricao,
		ie_via_aplicacao,
		cd_unidade_medida_dosa,
		qt_dose,
		cd_intervalo,
		hr_prim_horario,
		ds_horarios,
		ds_observacao,
		qt_volume_oral,
		qt_vel_infusao,
		nr_seq_disp_succao
	into	nr_seq_produto_w,
		nr_seq_leite_deriv_w,
		ie_tipo_nutricao_w,
		ie_via_aplicacao_w,
		cd_unidade_medida_w,
		qt_dose_w,
		cd_intervalo_w,
		hr_prim_horario_w,
		ds_horarios_w,
		ds_observacao_w,
		qt_volume_oral_w,
		qt_vel_infusao_w,
		nr_seq_disp_succao_w
	from	nut_atend_serv_dia_dieta
	where	nr_sequencia = nr_seq_serv_dia_dieta_p;

	if (ie_tipo_nutricao_w = 'SMP') then
		ds_lista_suplemento_w 	:= nr_seq_produto_w||',';
		ds_lista_enteral_w	:= '';
	elsif (ie_tipo_nutricao_w = 'SNE') then
		ds_lista_suplemento_w 	:= '';
		ds_lista_enteral_w	:= nr_seq_produto_w||',';
	elsif (ie_tipo_nutricao_w = 'L') then
		PLT_Gerar_w_produto(0, 0, 0, 'L', nm_usuario_p, '', '', null, '');
		PLT_Gerar_w_produto(0, nvl(nr_seq_leite_deriv_w,0), 0, 'I', nm_usuario_p, '', '', null, '');
		
		for	row_C01 in C01 loop
			PLT_Gerar_w_produto(0, nvl(nr_seq_leite_deriv_w,0), row_C01.nr_sequencia, 'I', nm_usuario_p, '', '', null, '');		
			Gerar_informacoes_prod_adic(0,
					nr_seq_leite_deriv_w,
					row_C01.nr_sequencia,
					row_C01.qt_porcentagem,
					row_C01.qt_dose);
		end loop;

	end if;

	Nut_Inserir_Conduta_Dietot(
				nr_seq_serv_dia_w,
				'',
				ds_lista_suplemento_w,
				ds_lista_enteral_w,
				nm_usuario_p,
				dt_servico_w,
				dt_servico_w + 86399/86400,
				'',
				ds_mensagem_w,
				nr_seq_dieta_w
				);

	if (nr_seq_dieta_w is not null) then
		update	nut_atend_serv_dia_dieta
		set	ie_via_aplicacao	= ie_via_aplicacao_w,
			cd_unidade_medida_dosa	= cd_unidade_medida_w,
			qt_dose			= qt_dose_w,
			cd_intervalo		= cd_intervalo_w,
			hr_prim_horario		= hr_prim_horario_w,
			ds_horarios		= ds_horarios_w,
			ie_tipo_nutricao	= ie_tipo_nutricao_w,
			ds_observacao		= ds_observacao_w,
			qt_volume_oral          = qt_volume_oral_w,
			qt_vel_infusao          = qt_vel_infusao_w,
			nr_seq_disp_succao      = nr_seq_disp_succao_w
		where	nr_sequencia = nr_seq_dieta_w;
	end if;

	Nut_suspender_conduta_diet(nr_seq_serv_dia_dieta_p,nm_usuario_p);

end if;

commit;

end Nut_Duplicar_Conduta_Diet;
/