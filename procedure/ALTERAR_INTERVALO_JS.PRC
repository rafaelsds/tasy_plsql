create or replace
procedure alterar_intervalo_js(
			nr_prescricao_p		number,
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2) is 

begin

if	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) then
	begin
	
	update	prescr_material
	set	cd_intervalo 	= null
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_sequencia	= nr_sequencia_p;
	
	commit;
	
	gerar_reconst_diluicao(nr_prescricao_p, nr_sequencia_p, 'A');
	
	end;
end if;

end alterar_intervalo_js;
/