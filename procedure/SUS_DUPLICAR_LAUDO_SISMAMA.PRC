create or replace
procedure sus_duplicar_laudo_sismama(	nr_seq_sismama_p		number,
				nr_atend_laudo_p		number,
				nr_atend_novo_p		number,
				nm_usuario_p		varchar2,
				nr_laudo_novo_p	out	number)as

cd_estabelecimento_w		number(4);
cd_profissional_w			varchar2(10);
cd_cgc_unidade_saude_w		varchar2(14);
cd_pf_resp_emissao_filme_w		varchar2(10);
nr_seq_unidade_saude_w		number(10);
cd_laudo_externo_w		varchar2(20);
nr_seq_sismama_w			number(10);

begin

begin
select	cd_estabelecimento,
	cd_profissional,
	cd_cgc_unidade_saude,
	cd_pf_resp_emissao_filme,
	nr_seq_unidade_saude,
	cd_laudo_externo
into	cd_estabelecimento_w,
	cd_profissional_w,
	cd_cgc_unidade_saude_w,
	cd_pf_resp_emissao_filme_w,
	nr_seq_unidade_saude_w,
	cd_laudo_externo_w
from	sismama_atendimento
where	nr_sequencia 	= nr_seq_sismama_p
and	nr_atendimento	= nr_atend_laudo_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações do atendimento do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263443);
end;

select	sismama_atendimento_seq.nextval
into	nr_seq_sismama_w
from	dual;

insert into sismama_atendimento (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_atendimento,
	cd_estabelecimento,
	dt_emissao,
	cd_profissional,
	dt_liberacao,
	cd_cgc_unidade_saude,
	cd_pf_resp_emissao_filme,
	nr_seq_unidade_saude,
	cd_laudo_externo)
values(	nr_seq_sismama_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_atend_novo_p,
	cd_estabelecimento_w,
	sysdate,
	cd_profissional_w,
	null,
	cd_cgc_unidade_saude_w,
	cd_pf_resp_emissao_filme_w,
	nr_seq_unidade_saude_w,
	cd_laudo_externo_w);

begin
insert into sismama_mam_anamnese (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_sismama,
	ie_nodulo_caroco,
	ie_risco_elevado,
	ie_mama_exam_prof,
	ie_ultima_mamografia,
	dt_ultima_mamografia)
select	sismama_mam_anamnese_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_sismama_w,
	ie_nodulo_caroco,
	ie_risco_elevado,
	ie_mama_exam_prof,
	ie_ultima_mamografia,
	dt_ultima_mamografia
from	sismama_mam_anamnese
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações da anamnese do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263444);
end;

begin
insert into sismama_mam_ind_clinica (
	nr_sequencia,
	dt_atualizacao,		nm_usuario,		dt_atualizacao_nrec,
	nm_usuario_nrec,		nr_seq_sismama,		ie_achado_exame_clin,
	ie_lesao_pap_dir,		ie_lesao_pap_esq,		ie_descarga_pap_dir,
	ie_descarga_pap_esq,	ie_mamografia_diag,	ie_nod_qsl_dir,
	ie_esp_qsl_dir,		ie_nod_qsl_esq,		ie_esp_qsl_esq,
	ie_esp_uqlat_dir,		ie_esp_uqlat_esq,		ie_esp_uqsup_esq,
	ie_esp_uqsup_dir,		ie_esp_uqmed_dir,		ie_esp_uqinf_dir,
	ie_esp_rra_dir,		ie_esp_pa_dir,		ie_linf_axilar_dir,
	ie_rad_assim_focal_esq,	ie_rad_microcalcif_esq,	ie_rad_microcalcif_dir,
	ie_rad_nodulo_esq,		ie_rad_nodulo_dir,		ie_controle_rad,
	ie_linf_supraclavic_esq,	ie_linf_supraclavic_dir,	ie_linf_axilar_esq,
	ie_nod_uqinf_dir,		ie_nod_uqmed_esq,	ie_nod_uqmed_dir,
	ie_nod_uqsup_esq,		ie_nod_uqsup_dir,		ie_nod_uqlat_esq,
	ie_nod_uqlat_dir,		ie_nod_qil_dir,		ie_diag_area_densa_esq,
	ie_esp_qil_dir,		ie_nod_pa_esq,		ie_nod_pa_dir,
	ie_nod_rra_esq,		ie_nod_rra_dir,		ie_nod_uqinf_esq,
	ie_diag_area_densa_dir,	ie_diag_assim_difusa_esq,	ie_diag_assim_focal_dir,
	ie_diag_assim_difusa_dir,	ie_diag_assim_focal_esq,	ie_diag_microcalcif_esq,
	ie_diag_microcalcif_dir,	ie_diag_nodulo_esq,	dt_exame,
	ie_mam_rastreamento,	ie_aval_qt_neo_adjuvante,	ie_lesao_diag_cancer,
	ie_diag_distorcao_focal_esq,	ie_diag_distorcao_focal_dir,	ie_diag_nodulo_dir,
	ie_rad_distorcao_focal_esq,	ie_rad_distorcao_focal_dir,	ie_rad_area_densa_esq,
	ie_rad_area_densa_dir,	ie_rad_assim_difusa_esq,	ie_rad_assim_focal_dir,
	ie_rad_assim_difusa_dir,	ie_esp_pa_esq,		ie_esp_rra_esq,
	ie_esp_uqinf_esq,		ie_esp_uqmed_esq,		ie_nod_qil_esq,
	ie_esp_qil_esq,		ie_nod_qsm_dir,		ie_esp_qsm_dir,
	ie_esp_qsm_esq,		ie_nod_qsm_esq,		ie_esp_qim_dir,
	ie_nod_qim_dir,		ie_esp_qim_esq,		ie_nod_qim_esq)
select	sismama_mam_ind_clinica_seq.nextval,
	sysdate,			nm_usuario_p,		dt_atualizacao_nrec,
	nm_usuario_nrec,		nr_seq_sismama_w,		ie_achado_exame_clin,
	ie_lesao_pap_dir,		ie_lesao_pap_esq,		ie_descarga_pap_dir,
	ie_descarga_pap_esq,	ie_mamografia_diag,	ie_nod_qsl_dir,
	ie_esp_qsl_dir,		ie_nod_qsl_esq,		ie_esp_qsl_esq,
	ie_esp_uqlat_dir,		ie_esp_uqlat_esq,		ie_esp_uqsup_esq,
	ie_esp_uqsup_dir,		ie_esp_uqmed_dir,		ie_esp_uqinf_dir,
	ie_esp_rra_dir,		ie_esp_pa_dir,		ie_linf_axilar_dir,
	ie_rad_assim_focal_esq,	ie_rad_microcalcif_esq,	ie_rad_microcalcif_dir,
	ie_rad_nodulo_esq,		ie_rad_nodulo_dir,		ie_controle_rad,
	ie_linf_supraclavic_esq,	ie_linf_supraclavic_dir,	ie_linf_axilar_esq,
	ie_nod_uqinf_dir,		ie_nod_uqmed_esq,	ie_nod_uqmed_dir,
	ie_nod_uqsup_esq,		ie_nod_uqsup_dir,		ie_nod_uqlat_esq,
	ie_nod_uqlat_dir,		ie_nod_qil_dir,		ie_diag_area_densa_esq,
	ie_esp_qil_dir,		ie_nod_pa_esq,		ie_nod_pa_dir,
	ie_nod_rra_esq,		ie_nod_rra_dir,		ie_nod_uqinf_esq,
	ie_diag_area_densa_dir,	ie_diag_assim_difusa_esq,	ie_diag_assim_focal_dir,
	ie_diag_assim_difusa_dir,	ie_diag_assim_focal_esq,	ie_diag_microcalcif_esq,
	ie_diag_microcalcif_dir,	ie_diag_nodulo_esq,	dt_exame,
	ie_mam_rastreamento,	ie_aval_qt_neo_adjuvante,	ie_lesao_diag_cancer,
	ie_diag_distorcao_focal_esq,	ie_diag_distorcao_focal_dir,	ie_diag_nodulo_dir,
	ie_rad_distorcao_focal_esq,	ie_rad_distorcao_focal_dir,	ie_rad_area_densa_esq,
	ie_rad_area_densa_dir,	ie_rad_assim_difusa_esq,	ie_rad_assim_focal_dir,
	ie_rad_assim_difusa_dir,	ie_esp_pa_esq,		ie_esp_rra_esq,
	ie_esp_uqinf_esq,		ie_esp_uqmed_esq,		ie_nod_qil_esq,
	ie_esp_qil_esq,		ie_nod_qsm_dir,		ie_esp_qsm_dir,
	ie_esp_qsm_esq,		ie_nod_qsm_esq,		ie_esp_qim_dir,
	ie_nod_qim_dir,		ie_esp_qim_esq,		ie_nod_qim_esq
from	sismama_mam_ind_clinica
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações da indicação clínica do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263445);
end;

begin
insert into sismama_anamnese_rad (
	nr_sequencia,
	dt_atualizacao,		nm_usuario,
	dt_atualizacao_nrec,	nm_usuario_nrec,
	nr_seq_sismama,		cd_cgc_clinica,
	dt_recebimento,		nr_exame,
	ie_nao_sus,		dt_ult_mestruacao,
	ie_nunca_menstruou,	ie_nao_lembra_ult_menstr,
	nr_idade_menopausa,	ie_nao_lembra_menopausa,
	ie_remedio_menopausa,	ie_gravida,
	ie_radioterapia,		dt_radioterapia_dir,
	dt_radioterapia_esq,	ie_nao_fez_cirurgia,
	dt_tumorectomia_dir,	dt_tumorectomia_esq,
	dt_segmentectomia_dir,	dt_segmentectomia_esq,
	dt_dutectomia_dir,		dt_dutectomia_esq,
	dt_mastectomia_dir,		dt_mastectomia_esq,
	dt_mastectomia_pele_dir,	dt_mastectomia_pele_esq,
	dt_esvaziamento_dir,	dt_esvaziamento_esq,
	dt_biopsia_linf_sent_dir,	dt_biopsia_linf_sent_esq,
	dt_reconstrucao_dir,	dt_reconstrucao_esq,
	dt_plastic_redutora_dir,	dt_plastic_redutora_esq,
	dt_plastic_implante_dir,	dt_plastic_implante_esq)
select	sismama_anamnese_rad_seq.nextval,
	sysdate,			nm_usuario_p,
	dt_atualizacao_nrec,			nm_usuario_nrec,
	nr_seq_sismama_w,		cd_cgc_clinica,
	dt_recebimento,		nr_exame,
	ie_nao_sus,		dt_ult_mestruacao,
	ie_nunca_menstruou,	ie_nao_lembra_ult_menstr,
	nr_idade_menopausa,	ie_nao_lembra_menopausa,
	ie_remedio_menopausa,	ie_gravida,
	ie_radioterapia,		dt_radioterapia_dir,
	dt_radioterapia_esq,	ie_nao_fez_cirurgia,
	dt_tumorectomia_dir,	dt_tumorectomia_esq,
	dt_segmentectomia_dir,	dt_segmentectomia_esq,
	dt_dutectomia_dir,		dt_dutectomia_esq,
	dt_mastectomia_dir,		dt_mastectomia_esq,
	dt_mastectomia_pele_dir,	dt_mastectomia_pele_esq,
	dt_esvaziamento_dir,	dt_esvaziamento_esq,
	dt_biopsia_linf_sent_dir,	dt_biopsia_linf_sent_esq,
	dt_reconstrucao_dir,	dt_reconstrucao_esq,
	dt_plastic_redutora_dir,	dt_plastic_redutora_esq,
	dt_plastic_implante_dir,	dt_plastic_implante_esq
from	sismama_anamnese_rad
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações da indicação clínica do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263446);
end;

begin
insert into sismama_achado_rad (
	nr_sequencia,
	dt_atualizacao,		nm_usuario,		dt_atualizacao_nrec,
	nm_usuario_nrec,		nr_seq_sismama,		ie_micr_distrib3,
	ie_lado,			ie_pele,			ie_composicao_mama,
	ie_nodulo1,		ie_nodulo2,		ie_nodulo3,
	ie_nod_localizacao1,	ie_micr_localizacao1,	ie_micr_localizacao3,
	ie_micr_localizacao2,	ie_nod_localizacao3,	ie_nod_localizacao2,
	ie_nod_tamanho1,		ie_nod_tamanho2,		ie_nod_tamanho3,
	ie_nod_contorno1,		ie_nod_contorno2,		ie_nod_contorno3,
	ie_nod_limite1,		ie_nod_limite2,		ie_nod_limite3,
	ie_microcalcificacao1,	ie_microcalcificacao2,	ie_microcalcificacao3,
	ie_assim_focal1,		ie_assim_focal2,		ie_assim_focal_loc1,
	ie_assim_focal_loc2,	ie_assim_difusa1,		ie_assim_difusa2,
	ie_assim_difusa_loc2,	ie_assim_difusa_loc1,	ie_distor_focal1,
	ie_distor_focal2,		ie_distor_foc_loc1,		ie_distor_foc_loc2,
	ie_area_densa1,		ie_area_densa2,		ie_area_densa_loc1,
	ie_area_densa_loc2,	ie_linf_visibilizado,		ie_inf_aumentados,
	ie_linf_densos,		ie_linf_confluentes,		ie_dilatacao_ductal,
	ie_nod_dens_gordura,	ie_nod_calcificado,		ie_nod_dens_het,
	ie_calcificacao_vasc,	ie_outra_calcif,		ie_linf_intramamario,
	ie_dist_arq_cirurg,		ie_implante_integro,		ie_implante_ruptura,
	nr_filmes,			ie_micr_forma1,		ie_micr_forma2,
	ie_micr_distrib1,		ie_micr_distrib2,		ie_micr_forma3)
select	sismama_achado_rad_seq.nextval,
	sysdate,			nm_usuario_p,		dt_atualizacao_nrec,
	nm_usuario_nrec,		nr_seq_sismama_w,		ie_micr_distrib3,
	ie_lado,			ie_pele,			ie_composicao_mama,
	ie_nodulo1,		ie_nodulo2,		ie_nodulo3,
	ie_nod_localizacao1,	ie_micr_localizacao1,	ie_micr_localizacao3,
	ie_micr_localizacao2,	ie_nod_localizacao3,	ie_nod_localizacao2,
	ie_nod_tamanho1,		ie_nod_tamanho2,		ie_nod_tamanho3,
	ie_nod_contorno1,		ie_nod_contorno2,		ie_nod_contorno3,
	ie_nod_limite1,		ie_nod_limite2,		ie_nod_limite3,
	ie_microcalcificacao1,	ie_microcalcificacao2,	ie_microcalcificacao3,
	ie_assim_focal1,		ie_assim_focal2,		ie_assim_focal_loc1,
	ie_assim_focal_loc2,	ie_assim_difusa1,		ie_assim_difusa2,
	ie_assim_difusa_loc2,	ie_assim_difusa_loc1,	ie_distor_focal1,
	ie_distor_focal2,		ie_distor_foc_loc1,		ie_distor_foc_loc2,
	ie_area_densa1,		ie_area_densa2,		ie_area_densa_loc1,
	ie_area_densa_loc2,	ie_linf_visibilizado,		ie_inf_aumentados,
	ie_linf_densos,		ie_linf_confluentes,		ie_dilatacao_ductal,
	ie_nod_dens_gordura,	ie_nod_calcificado,		ie_nod_dens_het,
	ie_calcificacao_vasc,	ie_outra_calcif,		ie_linf_intramamario,
	ie_dist_arq_cirurg,		ie_implante_integro,		ie_implante_ruptura,
	nr_filmes,			ie_micr_forma1,		ie_micr_forma2,
	ie_micr_distrib1,		ie_micr_distrib2,		ie_micr_forma3
from	sismama_achado_rad
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações do achado radiológico do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263447);
end;

begin
insert into sismama_mam_conclusao (
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_responsavel,
	nr_seq_sismama,
	ie_categoria_esq,
	ie_categoria_dir,
	ie_recomendacao_dir,
	ie_recomendacao_esq,
	ds_observacao,
	dt_liberacao)
select	sismama_mam_conclusao_seq.nextval,
	sysdate,
	nm_usuario_p,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_responsavel,
	nr_seq_sismama_w,
	ie_categoria_esq,
	ie_categoria_dir,
	ie_recomendacao_dir,
	ie_recomendacao_esq,
	ds_observacao,
	dt_liberacao
from	sismama_mam_conclusao
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações da conclusão radiológica do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263448);
end;

begin
insert into sismama_his_dados_clinico (
	nr_sequencia,
	nr_seq_sismama,		dt_atualizacao,
	nm_usuario,		dt_atualizacao_nrec,
	nm_usuario_nrec,		ie_tipo_exame,
	ie_risco_elevado,		ie_gravida_amamentando,
	ie_tratamento_anterior,	ie_trat_mesma_mama,
	ie_trat_outra_mama,	ie_trat_quimioterapia,
	ie_trat_rad_mesma_mama,	ie_trat_rad_outra_mama,
	ie_trat_hormonio,		ie_deteccao_lesao,
	ie_diagnostico_imagem,	ie_caracteristica_lesao,
	ie_localizacao,		ie_tamanho,
	ie_linfonodo_axilar,		ie_material_enviado,
	dt_coleta)
select	sismama_his_dados_clinico_seq.nextval,
	nr_seq_sismama_w,	sysdate,
	nm_usuario_p,		dt_atualizacao_nrec,
	nm_usuario_nrec,		ie_tipo_exame,
	ie_risco_elevado,		ie_gravida_amamentando,
	ie_tratamento_anterior,	ie_trat_mesma_mama,
	ie_trat_outra_mama,	ie_trat_quimioterapia,
	ie_trat_rad_mesma_mama,	ie_trat_rad_outra_mama,
	ie_trat_hormonio,		ie_deteccao_lesao,
	ie_diagnostico_imagem,	ie_caracteristica_lesao,
	ie_localizacao,		ie_tamanho,
	ie_linfonodo_axilar,		ie_material_enviado,
	dt_coleta
from	sismama_his_dados_clinico
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações dos dados clínicos histopatológicos do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263449);
end;

begin
insert into sismama_his_resultado (
	nr_sequencia,
	nr_seq_sismama,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_cgc_laboratorio,
	nr_exame,
	dt_recebimento,
	ie_nao_gerar_bpa,
	ie_procedimento_cirurgico,
	ie_adequabilidade_material,
	ds_insatisfatorio,
	ie_dim_tumor_dominante,
	ie_dim_tumor_secundario)
select	sismama_his_resultado_seq.nextval,
	nr_seq_sismama_w,
	sysdate,
	nm_usuario_p,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_cgc_laboratorio,
	nr_exame,
	dt_recebimento,
	ie_nao_gerar_bpa,
	ie_procedimento_cirurgico,
	ie_adequabilidade_material,
	ds_insatisfatorio,
	ie_dim_tumor_dominante,
	ie_dim_tumor_secundario
from	sismama_his_resultado
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações dos resultados histopatológicos do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263450);
end;

begin
insert into sismama_his_exame_micro (
	nr_sequencia,
	nr_seq_sismama,		dt_atualizacao,
	nm_usuario,		dt_atualizacao_nrec,
	nm_usuario_nrec,		ie_microcalcificacoes,
	ie_hiper_ductal_sem_atipias,	ie_hiper_ductal_com_atipias,
	ie_hiper_lobular_com_atipias,	ie_adenose_soe,
	ie_lesao_esclerosante,	ie_consicao_fibrocistica,
	ie_fibroadenoma,		ie_papiloma_solitario,
	ie_papiloma_multiplo,	ie_papilomatose_florida,
	ie_mastite,		ie_outros,
	ds_outros,		ie_neoplasico_maligno,
	ds_outros_neoplasico,	ie_tipo_histopatologico,
	ds_tipo_histopatologico,	ie_multifocalidade_tumor,
	ie_multicentricidade_tumor,	ie_grau_histologico,
	ie_invasao_vascular,	ie_infiltracao_perineural,
	ie_embolizacao_linfatica,	ie_pele,
	ie_mamilo,		ie_musculo_peitoral,
	ie_fascia_peitoral,		ie_gradil_costal,
	ie_margens_cirurgicas,	ie_linfonodos,
	nr_lifonodo_avaliado,	ie_linfonodo_comprometido,
	ie_presenca_coalescencia,	ie_extravasamento,
	ie_receptor_estrogeno,	ie_receptor_progesterona,
	ie_estudo_histoquimico,	ds_outros_estudos,
	ds_observacao,		dt_liberacao)
select	sismama_his_exame_micro_seq.nextval,
	nr_seq_sismama_w,		sysdate,
	nm_usuario_p,		dt_atualizacao_nrec,
	nm_usuario_nrec,		ie_microcalcificacoes,
	ie_hiper_ductal_sem_atipias,	ie_hiper_ductal_com_atipias,
	ie_hiper_lobular_com_atipias,	ie_adenose_soe,
	ie_lesao_esclerosante,	ie_consicao_fibrocistica,
	ie_fibroadenoma,		ie_papiloma_solitario,
	ie_papiloma_multiplo,	ie_papilomatose_florida,
	ie_mastite,		ie_outros,
	ds_outros,		ie_neoplasico_maligno,
	ds_outros_neoplasico,	ie_tipo_histopatologico,
	ds_tipo_histopatologico,	ie_multifocalidade_tumor,
	ie_multicentricidade_tumor,	ie_grau_histologico,
	ie_invasao_vascular,	ie_infiltracao_perineural,
	ie_embolizacao_linfatica,	ie_pele,
	ie_mamilo,		ie_musculo_peitoral,
	ie_fascia_peitoral,		ie_gradil_costal,
	ie_margens_cirurgicas,	ie_linfonodos,
	nr_lifonodo_avaliado,	ie_linfonodo_comprometido,
	ie_presenca_coalescencia,	ie_extravasamento,
	ie_receptor_estrogeno,	ie_receptor_progesterona,
	ie_estudo_histoquimico,	ds_outros_estudos,
	ds_observacao,		dt_liberacao
from	sismama_his_exame_micro
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações dos exames microscópicos histopatológicos do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263451);
end;

begin
insert into sismama_cit_anamnese (
	nr_sequencia,
	nr_seq_sismama,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_nodulo,
	ie_gravida_amamentando,
	ie_risco_cancer)
select	sismama_cit_anamnese_seq.nextval,
	nr_seq_sismama_w,
	sysdate,
	nm_usuario_p,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_nodulo,
	ie_gravida_amamentando,
	ie_risco_cancer
from	sismama_cit_anamnese
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações da anamnese citopatológica do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263453);
end;

begin
insert into sismama_cit_exame_clinico (
	nr_sequencia,
	nr_seq_sismama,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_descarga_papilar,
	ie_nodulo,
	ie_localizacao,
	ie_material_enviado,
	ie_tipo_material_enviado,
	ie_tumor_residual,
	ie_tumor_solido,
	qt_laminas_enviadas,
	dt_coleta)
select	sismama_cit_exame_clinico_seq.nextval,
	nr_seq_sismama_w,
	sysdate,
	nm_usuario_p,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ie_descarga_papilar,
	ie_nodulo,
	ie_localizacao,
	ie_material_enviado,
	ie_tipo_material_enviado,
	ie_tumor_residual,
	ie_tumor_solido,
	qt_laminas_enviadas,
	dt_coleta
from	sismama_cit_exame_clinico
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações do exame clínico citopatológico do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263454);	
end;

begin
insert into sismama_cit_resultados (
	nr_sequencia,
	nr_seq_sismama,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_cgc_laboratorio,
	nr_exame,
	dt_recebimento,
	ie_localizacao,
	nr_material_recebido,
	ie_adequabilidade,
	ds_insatisfatorio,
	ie_processos,
	ds_outros_processos,
	ie_padrao_cito_malignidade,
	ds_outros_cito_malig,
	ie_padrao_suspeito,
	ds_outros_suspeito,
	ie_padrao_positivo,
	ds_outros_positivo,
	ie_padrao_amostra,
	ds_observacao,
	dt_liberacao)
select	sismama_cit_resultados_seq.nextval,
	nr_seq_sismama_w,
	sysdate,
	nm_usuario_p,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_cgc_laboratorio,
	nr_exame,
	dt_recebimento,
	ie_localizacao,
	nr_material_recebido,
	ie_adequabilidade,
	ds_insatisfatorio,
	ie_processos,
	ds_outros_processos,
	ie_padrao_cito_malignidade,
	ds_outros_cito_malig,
	ie_padrao_suspeito,
	ds_outros_suspeito,
	ie_padrao_positivo,
	ds_outros_positivo,
	ie_padrao_amostra,
	ds_observacao,
	dt_liberacao
from	sismama_cit_resultados
where	nr_seq_sismama = nr_seq_sismama_p;
exception
when others then
	--r.aise_application_error(-20011,'Problemas ao obter as informações dos  citopatológicos do laudo SISMAMA.'||'#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(263456);
end;

nr_laudo_novo_p	:= nr_seq_sismama_w;

commit;

end sus_duplicar_laudo_sismama;
/