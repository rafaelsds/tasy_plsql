create or replace
procedure excluir_lote_orgao_cobr(
			nr_seq_lote_p		Varchar2,
			nm_usuario_p		varchar2) is 

ds_historico_w		varchar2(255);
dt_fechamento_w		date;
qtd_w				number(10);
nr_sequencia_w		number(10);
ie_tipo_lote_w		varchar2(1);
ie_origem_w			varchar2(1);

cursor c01 is
	select	nr_sequencia
	from	cheque_cr_orgao_cobr a
	where 	(a.nr_seq_lote = nr_seq_lote_p 
	or 	a.nr_seq_lote_exc = nr_seq_lote_p)
	union
	select	nr_sequencia
	from 	titulo_receber_orgao_cobr b
	where 	(b.nr_seq_lote = nr_seq_lote_p 
	or 	b.nr_seq_lote_exc = nr_seq_lote_p)
	union
	select	nr_sequencia
	from	outros_orgao_cobr c
	where 	(c.nr_seq_lote = nr_seq_lote_p 
	or 	c.nr_seq_lote_exc = nr_seq_lote_p)
	order by 1;

begin

if	(nr_seq_lote_p is not null)then

	select	dt_fechamento,
			ie_origem,
			ie_tipo_lote
	into	dt_fechamento_w,
			ie_origem_w,
			ie_tipo_lote_w
	from	lote_orgao_cobranca
	where	nr_sequencia = nr_seq_lote_p;
	
	if (dt_fechamento_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(683443);		
	end if;	

	open c01;
	loop
	fetch c01 into	
		nr_sequencia_w;
	exit when c01%notfound;
		begin
		excluir_cobranca_lote(nr_sequencia_w,ie_origem_w,wheb_usuario_pck.get_nm_usuario,ie_tipo_lote_w);		
		end;
	end loop;
	close c01;	
	
	delete from lote_orgao_cobr_aprovacao
	where nr_seq_lote = nr_seq_lote_p;

	delete from lote_orgao_cobr_hist
	where nr_seq_lote = nr_seq_lote_p;

	delete from lote_orgao_cobranca
	where nr_sequencia = nr_seq_lote_p;
end if;

commit;

end excluir_lote_orgao_cobr;
/
