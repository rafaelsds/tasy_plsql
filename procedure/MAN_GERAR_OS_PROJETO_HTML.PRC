create or replace
procedure man_gerar_os_projeto_html(	nr_seq_projeto_p	number,
					nm_usuario_p		varchar2) is

nr_sequencia_w		number(10);
nm_usuario_exec_w		varchar2(15);
nr_seq_gerencia_w		number(10,0);
cd_estabelecimento_w	number(4);
nr_seq_grupo_des_w		number(10,0);
nr_seq_cliente_w		number(10,0);
nr_seq_localizacao_w	number(10,0);
nr_seq_equipamento_w	number(10,0);
nr_seq_proj_cron_etp_w	number(10,0);
cd_funcao_w		number(5);
ds_etapa_w		varchar2(4000);
ds_atividade_w		varchar2(255);
cd_coordenador_w		varchar2(10);
nr_seq_ordem_w		number(10);
nr_seq_projeto_w		number(10);
ie_gera_ordem_auto_w	varchar2(1);
ie_fase_w		varchar2(1);

Cursor C01 is
	select	nr_sequencia,
		p.nr_seq_cliente,
		p.cd_estabelecimento,
		p.cd_funcao,
		substr(obter_usuario_pessoa(p.cd_coordenador),1,15),
		p.cd_coordenador,
		p.nr_seq_grupo_des
	from 	proj_projeto p
	Where 	p.nr_sequencia	= nr_seq_projeto_p;


Cursor C02 is
	select	e.nr_sequencia,
		trim(nvl(e.ds_etapa,e.ds_atividade)) ds_etapa,
		trim(e.ds_atividade),
		nvl(c.ie_gera_ordem_auto,'A'),
		nvl(e.ie_fase,'N')
	from	proj_projeto p,
		proj_cronograma c,
		proj_cron_etapa e
	where	p.nr_sequencia = nr_seq_projeto_w
	and	p.nr_sequencia = c.nr_seq_proj
	and	c.nr_sequencia = e.nr_seq_cronograma
	and	c.ie_tipo_cronograma = 'E'
	and	c.ie_situacao = 'A'
	and 	nvl(e.pr_etapa,0) <> 100
	and       not exists (	select	1
				from	man_ordem_servico_v  x
				where	x.nr_seq_proj_cron_etapa  = e.nr_sequencia)
	and	not exists (	select	1
			from	proj_cron_etapa x
			where	x.nr_seq_superior = e.nr_sequencia);

begin

open C01;
loop
fetch C01 into
	nr_seq_projeto_w,
	nr_seq_cliente_w,
	cd_estabelecimento_w,
	cd_funcao_w,
	nm_usuario_exec_w,
	cd_coordenador_w,
	nr_seq_grupo_des_w;
exit when C01%notfound;
	begin
	

	nr_seq_equipamento_w:= 41;

	nr_seq_localizacao_w	:= 41;

	open C02;
	loop
	fetch C02 into	
		nr_seq_proj_cron_etp_w,
		ds_etapa_w,
		ds_atividade_w,
		ie_gera_ordem_auto_w,
		ie_fase_w;
	exit when C02%notfound;
		begin
		
		if	(ie_gera_ordem_auto_w <> 'N') and
			(ie_fase_w = 'N') then
		
			man_gerar_os_proj_etapa_html(nr_seq_ordem_w,					/*nr_sequencia_w*/
					cd_coordenador_w,					/*cd_pessoa_solicitante_p*/
					nr_seq_localizacao_w, 				/*nr_seq_localizacao_p*/
					nr_seq_equipamento_w, 				/*nr_seq_equipamento_p*/
					'Proj. ' ||nr_seq_projeto_w|| ' - ' || ds_atividade_w,	/*ds_dano_breve_p*/
					nvl(ds_etapa_w,ds_atividade_w),			/*ds_dano_p*/
					cd_funcao_w, 					/*cd_funcao_p*/
					'S', 						/*ie_classificacao_p*/
					'U', 						/*ie_forma_receb_p*/
					sysdate+1, 						/*dt_inicio_previsto_p*/
					sysdate+20, 					/*dt_fim_previsto_p*/
					4,  						/*nr_seq_estagio_p*/
					'4', 						/*ie_tipo_ordem_p*/
					'1', 						/*nr_grupo_planej_p*/
					'1', 						/*nr_grupo_trabalho_p*/
					'M', 						/*ie_prioridade_p*/
					'1', 						/*ie_status_ordem_p*/
					substr(obter_usuario_pessoa(cd_coordenador_w),1,15),	/*nm_usuario_p*/
					nr_seq_proj_cron_etp_w, 				/*nr_seq_proj_cron_etapa_p*/
					nr_seq_grupo_des_w, 				/*nr_seq_grupo_des_p*/
					nr_seq_projeto_w, 					/*nr_seq_projeto_w*/
					10,
					null,
					null,
					cd_estabelecimento_w); 						/*nr_seq_ativ_exec_p*/
		end if;	
		end;
	end loop;
	close C02;	
	
	end;
end loop;
close C01;

commit;

end man_gerar_os_projeto_html;
/
