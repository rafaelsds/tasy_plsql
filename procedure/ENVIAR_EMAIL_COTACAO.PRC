create or replace
procedure enviar_email_cotacao(
			ie_tipo_p		number,
			nr_ordem_compra_p	number,
			nm_usuario_p		varchar2) is 
	
ds_email_w			varchar2(255);
nm_contato_w			varchar2(255);
ie_envia_pessoa_deleg_w		varchar2(255);
cd_perfil_dispara_w		varchar2(255);
ds_contato_w			varchar2(2000);
ds_email_adicional_w		varchar2(2000);
ds_assunto_padrao_w		varchar2(255);
ds_mensagem_padrao_w		varchar2(4000);
ds_email_origem_w		varchar2(255);
ds_titulo_historico_w		varchar2(2000);
ds_historico_w			varchar2(2000);
nr_seq_aprovacao_w		number(10);
nr_seq_proc_aprov_w		number(10);
cursor_w			int;
ie_enviou_w			varchar2(1);
nr_seq_regra_w			number(10);
ie_envia_email_regra_w		varchar2(1);
cd_setor_atendimento_w		setor_atendimento.cd_setor_atendimento%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
	
cursor c01 is
	select	substr(obter_dados_pf_pj_estab(cd_estabelecimento_w,cd_pessoa_fisica,cd_cgc_fornecedor, 'M'),1,255) ds_email,
		substr(obter_dados_pf_pj_estab(cd_estabelecimento_w,cd_pessoa_fisica,cd_cgc_fornecedor, 'ENC'),1,254) nm_contato,
		null,
		null
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_p
	and	obter_se_envia_email_internet(ie_tipo_p,null,cd_estabelecimento_w,nr_ordem_compra_p) = 'S'
	and	obter_se_envia_email_ordem(ie_tipo_p,nr_ordem_compra_p,cd_estabelecimento_w) = 'S'
	and	ie_tipo_p <> 9
	union all
	select	u.ds_email,
		u.nm_usuario,
		a.nr_sequencia,
		a.nr_seq_proc_aprov
	from	usuario u,
		pessoa_fisica b,
		pessoas_processo_aprovacao_v a
	where	b.cd_pessoa_fisica = a.cd_pessoa_fisica
	and	b.cd_pessoa_fisica = u.cd_pessoa_fisica
	and	u.ie_situacao = 'A' 
	and	a.dt_liberacao is not null 
	and	u.ds_email is not null
	and    	obter_se_setor_usuario_aprov(cd_setor_atendimento_w,cd_estabelecimento_w,u.nm_usuario) = 'S'
	and	a.nr_sequencia in (
		select	distinct nr_seq_aprovacao
		from	ordem_compra_item
		where	nr_ordem_compra  = nr_ordem_compra_p
	and     obter_se_envia_email_internet(ie_tipo_p,null,cd_estabelecimento_w,nr_ordem_compra_p) = 'S')
	and 	not exists(
		select	1
		from	pessoa_fisica_delegacao x
		where	b.cd_pessoa_fisica = x.cd_pessoa_fisica
		and	((nvl(trunc(x.dt_inicio_limite),trunc(sysdate)) <= trunc(sysdate))
		and	(trunc(x.dt_limite) >= trunc(sysdate))))
	and	ie_envia_pessoa_deleg_w = 'S'
	and	ie_tipo_p = 9
	union all
	select	u.ds_email,
		u.nm_usuario,
		a.nr_sequencia,
		a.nr_seq_proc_aprov
	from	usuario u,
		usuario y,
		pessoa_fisica_delegacao b,
		pessoas_processo_aprovacao_v a
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica 
	and	b.cd_pessoa_substituta = u.cd_pessoa_fisica
	and	a.cd_pessoa_fisica = y.cd_pessoa_fisica
	and	((nvl(trunc(b.dt_inicio_limite),trunc(sysdate)) <= trunc(sysdate))
	and	(trunc(b.dt_limite) >= trunc(sysdate)))
	and	u.ie_situacao = 'A'
	and	a.dt_liberacao is not null and u.ds_email is not null
	and	obter_se_setor_usuario_aprov(cd_setor_atendimento_w,cd_estabelecimento_w,y.nm_usuario) = 'S'
	and	a.nr_sequencia in (
		select	distinct nr_seq_aprovacao
		from	ordem_compra_item
		where	nr_ordem_compra  = nr_ordem_compra_p
	and	obter_se_envia_email_internet(ie_tipo_p,null,cd_estabelecimento_w,nr_ordem_compra_p) = 'S')
	and	ie_envia_pessoa_deleg_w = 'S'
	and	ie_tipo_p = 9
	union all
	select	u.ds_email,
		u.nm_usuario,
		a.nr_sequencia,
		a.nr_seq_proc_aprov
	from	usuario u,
		pessoa_fisica b,
		processo_aprov_compra a
	where	(((a.cd_pessoa_fisica is null) and (b.cd_cargo = a.cd_cargo)) or
		((a.cd_pessoa_fisica is not null) and (b.cd_pessoa_fisica = a.cd_pessoa_fisica)))
	and	u.ie_situacao = 'A'
	and	a.dt_liberacao is not null
	and	u.ds_email is not null
	and	a.ie_aprov_reprov <> 'A'
	and	obter_se_setor_usuario_aprov(cd_setor_atendimento_w,cd_estabelecimento_w,u.nm_usuario) = 'S'
	and	b.cd_pessoa_fisica = u.cd_pessoa_fisica
	and	a.nr_sequencia in (
		select	distinct nr_seq_aprovacao
		from	ordem_compra_item
		where	nr_ordem_compra = nr_ordem_compra_p
		and	obter_se_envia_email_internet(ie_tipo_p,null,cd_estabelecimento_w,nr_ordem_compra_p) = 'S')
	and	ie_envia_pessoa_deleg_w = 'N'
	and	ie_tipo_p = 9;
	
begin

select	cd_setor_atendimento,
	cd_estabelecimento
into	cd_setor_atendimento_w,
	cd_estabelecimento_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p;

nr_seq_regra_w := substr(obter_dados_regra_email_compra(ie_tipo_p,cd_estabelecimento_w,nr_ordem_compra_p,'S',null,nm_usuario_p,null),1,10);
ie_envia_email_regra_w := obter_se_envia_email_regra(nr_ordem_compra_p, 'OC', ie_tipo_p, cd_estabelecimento_w);

if	(nr_seq_regra_w > 0) and
	(ie_envia_email_regra_w = 'S') then
	ie_envia_pessoa_deleg_w := substr(obter_dados_regra_email_compra(ie_tipo_p,cd_estabelecimento_w,nr_ordem_compra_p,'ED',null,nm_usuario_p,null),1,255);

	open c01;
	loop
	fetch c01 into	
		ds_email_w,
		nm_contato_w,
		nr_seq_aprovacao_w,
		nr_seq_proc_aprov_w;
	exit when c01%notfound;
		begin
		ds_assunto_padrao_w	:= substr(obter_dados_regra_email_compra(ie_tipo_p,cd_estabelecimento_w,nr_ordem_compra_p,'A',null,nm_usuario_p,null),1,255);
		ds_mensagem_padrao_w	:= substr(obter_dados_regra_email_compra(ie_tipo_p,cd_estabelecimento_w,nr_ordem_compra_p,'M',null,nm_usuario_p,null),1,4000);
		ds_email_origem_w	:= substr(obter_dados_regra_email_compra(ie_tipo_p,cd_estabelecimento_w,nr_ordem_compra_p,'O',null,nm_usuario_p,null),1,4000);
		
		if	(ds_email_w is not null) then
			ds_contato_w := nm_contato_w || '<' || ds_email_w || '>;';
			
			if	(ds_email_adicional_w is not null) then
				ds_contato_w := ds_contato_w || ds_email_adicional_w;
			end if;
			
			begin
			enviar_email(ds_assunto_padrao_w,ds_mensagem_padrao_w,ds_email_origem_w,ds_contato_w,nm_usuario_p,'M');
			ie_enviou_w := 'S';
			exception when others then
			ie_enviou_w := 'N';
			end;

			if	(ie_tipo_p = 9) and
				(ie_enviou_w <> 'S') then
				ds_titulo_historico_w := wheb_mensagem_pck.get_texto(255103);
				ds_historico_w := wheb_mensagem_pck.get_texto(255104);
				inserir_historico_ordem_compra(nr_ordem_compra_p,ie_tipo_p,ds_titulo_historico_w,ds_historico_w,nm_usuario_p);
			end if;
			
			if	(ds_contato_w is not null) then
				insere_ordem_compra_envio(nr_ordem_compra_p,nm_usuario_p,'E',ds_email_w || ';' || ds_email_adicional_w);
			end if;
		end if;
		
		end;
	end loop;
	close c01;
end if;

end enviar_email_cotacao;
/