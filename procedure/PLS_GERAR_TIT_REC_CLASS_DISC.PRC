create or replace
procedure pls_gerar_tit_rec_class_disc(	nr_titulo_p			titulo_receber.nr_titulo%type,
					nm_usuario_p			usuario.nm_usuario%type,
					ie_commit_p			varchar2 ) is

dt_atual_w			date := trunc(sysdate);
nr_seq_classif_w		titulo_receber_classif.nr_sequencia%type;
nr_seq_lote_disc_w		pls_lote_discussao.nr_sequencia%type;
nr_seq_lote_contest_w		pls_lote_contestacao.nr_sequencia%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
cd_conta_financ_w		pls_conta_financ_regra.cd_conta_financ%type;
nr_seq_camara_w			pls_camara_compensacao.nr_sequencia%type;
ie_tipo_intercambio_w		pls_conta_financ_regra.ie_tipo_intercambio%type := 'T';
nr_seq_pls_fatura_w		pls_fatura.nr_sequencia%type;
nr_seq_ptu_fatura_w		ptu_fatura.nr_sequencia%type;
nr_seq_congenere_w		pls_congenere.nr_sequencia%type;
ie_tipo_congenere_w		pls_congenere.ie_tipo_congenere%type;
cd_unimed_origem_w		ptu_fatura.cd_unimed_origem%type;
vl_classif_w			titulo_receber_classif.vl_classificacao%type := 0;
vl_titulo_w			titulo_receber.vl_titulo%type;

cursor c01 (	nr_seq_lote_disc_pc	pls_lote_discussao.nr_sequencia%type ) is
	select	sum(nvl(dp.vl_ndc,0)) vl_classif,
		dp.cd_conta_negado_deb cd_conta_debito
	from	pls_discussao_proc 		dp,
		pls_contestacao_discussao	ds
	where	ds.nr_sequencia	= dp.nr_seq_discussao
	and	ds.nr_seq_lote	= nr_seq_lote_disc_pc
	and	dp.cd_conta_negado_deb is not null
	group by dp.cd_conta_negado_deb
	union all
	select	sum(nvl(dm.vl_ndc,0)) vl_classif,
		dm.cd_conta_negado_deb cd_conta_debito
	from	pls_discussao_mat 		dm,
		pls_contestacao_discussao	ds
	where	ds.nr_sequencia	= dm.nr_seq_discussao
	and	ds.nr_seq_lote	= nr_seq_lote_disc_pc
	and	dm.cd_conta_negado_deb is not null
	group by dm.cd_conta_negado_deb;
					
begin
if	(nr_titulo_p is not null) then
	select	max(l.nr_sequencia),
		max(l.nr_seq_lote_contest),
		max(t.vl_titulo)
	into	nr_seq_lote_disc_w,
		nr_seq_lote_contest_w,
		vl_titulo_w
	from	pls_lote_discussao	l,
		titulo_receber		t
	where	t.nr_titulo		= l.nr_titulo_receber
	and	l.nr_titulo_receber	= nr_titulo_p;
	
	if	(nr_seq_lote_disc_w is null) then
		select	max(nr_sequencia),
			max(nr_seq_lote_contest)
		into	nr_seq_lote_disc_w,
			nr_seq_lote_contest_w
		from	pls_lote_discussao
		where	nr_titulo_receber_ndr	= nr_titulo_p;
	end if;
	
	-- Obter dados do lote de contesta��o
	if	(nr_seq_lote_contest_w is not null) then
		select	max(cd_estabelecimento),
			max(nr_seq_pls_fatura),
			max(nr_seq_ptu_fatura)
		into	cd_estabelecimento_w,
			nr_seq_pls_fatura_w,
			nr_seq_ptu_fatura_w
		from	pls_lote_contestacao
		where	nr_sequencia	= nr_seq_lote_contest_w;
	end if;	
	
	-- Se for contesta��o de OPS - Faturamento
	if	(nr_seq_pls_fatura_w is not null) then
		select	max(nr_seq_congenere)
		into	nr_seq_congenere_w
		from	pls_fatura
		where	nr_sequencia	= nr_seq_pls_fatura_w;
	
	-- Se for contesta��o de OPS - Contas de Interc�mbio (A500)
	elsif	(nr_seq_ptu_fatura_w is not null) then
		select	max(p.cd_unimed_origem)
		into	cd_unimed_origem_w
		from	ptu_fatura	p
		where	p.nr_sequencia		= nr_seq_ptu_fatura_w;
		
		cd_unimed_origem_w := somente_numero(cd_unimed_origem_w);
	
		select	max(c.nr_sequencia)
		into	nr_seq_congenere_w
		from	pls_congenere	c
		where	c.cd_cooperativa_number	= cd_unimed_origem_w;		
	end if;
	
	-- Obter dados da congenere
	if	(nr_seq_congenere_w is not null) then
		select	max(a.nr_seq_camara)
		into	nr_seq_camara_w
		from	pls_congenere_camara a
		where	a.nr_seq_congenere = nr_seq_congenere_w
		and	dt_atual_w between a.dt_inicio_vigencia_ref and a.dt_fim_vigencia_ref;
		
		select	max(ie_tipo_congenere)
		into	ie_tipo_congenere_w
		from	pls_congenere
		where	nr_sequencia = nr_seq_congenere_w;
		
		-- Obt�m se o tipo do congenere � operadora (funda��o)
		if	(ie_tipo_congenere_w = 'OP') then
			ie_tipo_intercambio_w := 'F'; -- Funda��o
		else
			ie_tipo_intercambio_w := pls_obter_tipo_intercambio( nr_seq_congenere_w, cd_estabelecimento_w); -- Nacional ou Estadual
		end if;
	end if;

	-- Obter conta financeira
	pls_obter_conta_financ_regra(	'ICR',		null,		cd_estabelecimento_w,
					null,		null,		nr_seq_camara_w,
					null,		null,		null,
					null,		null,		null,
					null,		null,		ie_tipo_intercambio_w,
					null,		null,		cd_conta_financ_w);	

	for r_c01_w in C01 ( nr_seq_lote_disc_w )  loop	
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_classif_w
		from	titulo_receber_classif
		where	nr_titulo	= nr_titulo_p;
		
		insert into titulo_receber_classif
			(nr_titulo,
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			cd_conta_contabil,
			vl_original,
			vl_classificacao,
			vl_desconto,
			cd_conta_financ)
		values	(nr_titulo_p,
			nr_seq_classif_w,
			nm_usuario_p,
			sysdate,
			r_c01_w.cd_conta_debito,
			0,
			r_c01_w.vl_classif,
			0,
			cd_conta_financ_w);	
			
		vl_classif_w := vl_classif_w + r_c01_w.vl_classif;
	end loop;
	
	vl_titulo_w := vl_titulo_w - vl_classif_w;
	
	if	(cd_conta_financ_w is not null) and
		(vl_titulo_w > 0) then
		
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_classif_w
		from	titulo_receber_classif
		where	nr_titulo	= nr_titulo_p;
		
		insert into titulo_receber_classif
			(nr_titulo,
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			cd_conta_contabil,
			vl_original,
			vl_classificacao,
			vl_desconto,
			cd_conta_financ)
		values	(nr_titulo_p,
			nr_seq_classif_w,
			nm_usuario_p,
			sysdate,
			null,
			0,
			vl_titulo_w,
			0,
			cd_conta_financ_w);	
	end if;
	

	if	(nvl(ie_commit_p,'N') = 'S') then
		commit;
	end if;
end if;

end pls_gerar_tit_rec_class_disc;
/