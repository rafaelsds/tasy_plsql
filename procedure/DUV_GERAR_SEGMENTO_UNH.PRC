create or replace 
procedure duv_gerar_segmento_unh(nr_seq_mensagem_p 	in duv_mensagem.nr_sequencia%type,
				nm_usuario_p      	in usuario.nm_usuario%type,
				nr_seq_episodio_p 	in episodio_paciente.nr_sequencia%type)is
                                                   
cursor c01 is
select 	null cd_destino, 
	upper(c.cd_mensagem)||':'||replace(b.cd_versao,'.',':')||':UV' ds_cabecalho, 
	null nr_seq_arquivo
from 	duv_mensagem a,
	duv_versao b,
	duv_tipo_mensagem c
where	a.nr_seq_versao = b.nr_sequencia
and	a.nr_seq_tipo_mensagem = c.nr_sequencia
and	a.nr_sequencia	= nr_seq_mensagem_p;

c01_w c01%rowtype;                                                   

begin 
c01_w := null;
open c01;
fetch c01 into c01_w;
close c01;

insert into duv_unh (nr_sequencia, 
	dt_atualizacao, 
	nm_usuario, 
	dt_atualizacao_nrec, 
	nm_usuario_nrec,
	nr_seq_mensagem, 
	cd_destino, 
	ds_cabecalho, 
	nr_seq_arquivo) 
values 	(duv_unh_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_mensagem_p,
	c01_w.cd_destino,
	c01_w.ds_cabecalho,
	c01_w.nr_seq_arquivo);
	
end duv_gerar_segmento_unh;
/
