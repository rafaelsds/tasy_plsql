create or replace
procedure GERAR_LOG_PESSOA_TERCEIRO
			(nr_seq_terceiro_p	number,
			cd_pessoa_fisica_p	varchar2,
			nm_usuario_p		varchar2) is

nm_pessoa_fisica_w	varchar2(60);
nm_terceiro_w		varchar2(255);

begin

select	max(substr(obter_nome_pf(a.cd_pessoa_fisica), 1, 255))
into	nm_pessoa_fisica_w
from	pessoa_fisica a
where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;

if	(nvl(nr_seq_terceiro_p,0) <> 0) then
	nm_terceiro_w := substr(obter_nome_terceiro(nr_seq_terceiro_p),1,255);
end if;

insert	into fin_log_repasse
	(cd_log,
	ds_log,
	dt_atualizacao,
	nm_usuario)
values	(1,
	substr(wheb_mensagem_pck.get_texto(303970,'NM_PESSOA_FISICA=' || nm_pessoa_fisica_w || ';NM_TERCEIRO=' || nm_terceiro_w),1,2000),
	sysdate,
	nm_usuario_p);

commit;

end GERAR_LOG_PESSOA_TERCEIRO;
/
