create or replace
procedure DUPLICAR_PARTE_CORPO_HUMANO(	nr_sequencia_p		number,
										nm_usuario_p		Varchar2) is 

										
										
nr_seq_parte_corpo_w	number(10);	
nr_seq_parte_corpo_old_w	number(10);			
nr_seq_articulacao_w		number(10);
nr_seq_articulacao_old_w	number(10);		
nr_seq_ARTIC_MOV_MUSCULO_w	number(10);
nr_seq_ARTIC_MOV_MUSCULO_old_w	number(10);			
										
Cursor C01 is
	select	*
	from	PARTE_CORPO_HUMANO
	where	nr_sequencia	= nr_sequencia_p;
	
cursor c02 is
	select	*
	from	ARTICULACAO
	where	NR_SEQ_PARTE = nr_seq_parte_corpo_old_w;
	
cursor c03 is
	select	*
	from	ARTIC_MOV_MUSCULO
	where	NR_SEQ_ARTICULACAO = nr_seq_articulacao_old_w;
	
	
cursor c04 is
	select	*
	from	ARTIC_MOV_MUSCULO_REGRA
	where	NR_SEQ_ART_MOV_MUSCULO = nr_seq_ARTIC_MOV_MUSCULO_old_w;
										

c01_w	c01%rowtype;		
c02_w	c02%rowtype;				
c03_w	c03%rowtype;		
c04_w	c04%rowtype;

		
begin
open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	
	select	PARTE_CORPO_HUMANO_seq.nextval
	into	nr_seq_parte_corpo_w
	from	dual;
	
	nr_seq_parte_corpo_old_w	:= c01_w.nr_sequencia;
	
	insert into PARTE_CORPO_HUMANO (
				 NR_SEQUENCIA,
				 DT_ATUALIZACAO,
				 NM_USUARIO,
				 DT_ATUALIZACAO_NREC,
				 NM_USUARIO_NREC,
				 DS_PARTE_CORPO,
				 IE_SITUACAO,
				 NR_SEQ_APRES,
				 IE_QUADRO_CLINICO)
			values (
				 nr_seq_parte_corpo_w,
				sysdate,
				 nm_usuario_p,
				 sysdate,
				 nm_usuario_p,
				 c01_w.DS_PARTE_CORPO,
				 c01_w.IE_SITUACAO,
				 c01_w.NR_SEQ_APRES,
				 c01_w.IE_QUADRO_CLINICO);
	
	
	open C02;
	loop
	fetch C02 into	
		c02_w;
	exit when C02%notfound;
		begin
		
		nr_seq_articulacao_old_w	:= c02_w.nr_sequencia;
		
		select	ARTICULACAO_seq.nextval
		into	nr_seq_articulacao_w
		from	dual;
		
		insert into ARTICULACAO (
					 NR_SEQUENCIA,
					 DS_ARTICULACAO,
					 DT_ATUALIZACAO,
					 NM_USUARIO,
					 DT_ATUALIZACAO_NREC,
					 NM_USUARIO_NREC,
					 IE_SITUACAO,
					 NR_SEQ_PARTE,
					 NR_SEQ_APRES)
				values (
					 nr_seq_articulacao_w,
					 c02_w.DS_ARTICULACAO,
					 sysdate,
					 nm_usuario_p,
					 sysdate,
					 nm_usuario_p,
					 c02_w.IE_SITUACAO,
					 nr_seq_parte_corpo_w,
					 c02_w.NR_SEQ_APRES);
					 
					 
					 
			open C03;
			loop
			fetch C03 into	
				c03_w;
			exit when C03%notfound;
				begin
				
				select	ARTIC_MOV_MUSCULO_seq.nextval
				into	nr_seq_ARTIC_MOV_MUSCULO_w
				from	dual;
				
				nr_seq_ARTIC_MOV_MUSCULO_old_w	:= c03_w.nr_sequencia;
				
				insert into ARTIC_MOV_MUSCULO (
							 NR_SEQUENCIA,
							 DT_ATUALIZACAO,
							 NM_USUARIO,
							 DT_ATUALIZACAO_NREC,
							 NM_USUARIO_NREC,
							 IE_SITUACAO,
							 NR_SEQ_ARTICULACAO,
							 NR_SEQ_MOVIMENTO,
							 NR_SEQ_MUSCULO,
							 IE_MUSCULATURA,
							 NR_SEQ_APRES)
						values (
							 nr_seq_ARTIC_MOV_MUSCULO_w,
							sysdate,
							 nm_usuario_p,
							 sysdate,
							 nm_usuario_p,
							 c03_w.IE_SITUACAO,
							 nr_seq_articulacao_w,
							 c03_w.NR_SEQ_MOVIMENTO,
							 c03_w.NR_SEQ_MUSCULO,
							 c03_w.IE_MUSCULATURA,
							 c03_w.NR_SEQ_APRES);
							 
							 
							 
							 
				open C04;
				loop
				fetch C04 into	
					c04_w;
				exit when C04%notfound;
					begin
					insert into ARTIC_MOV_MUSCULO_REGRA (
								 NR_SEQUENCIA,
								 DT_ATUALIZACAO,
								 NM_USUARIO,
								 DT_ATUALIZACAO_NREC,
								 NM_USUARIO_NREC,
								 QT_IDADE_MIN,
								 QT_IDADE_MAX,
								 QT_DOSE_MIN,
								 QT_DOSE_MAX,
								 QT_DOSE_PADRAO,
								 NR_SEQ_ART_MOV_MUSCULO,
								 QT_PONTOS_PADRAO,
								 QT_PONTOS_MIN,
								 QT_PONTOS_MAX,
								 QT_PESO_MIN,
								 QT_PESO_MAX,
								 QT_MULTIPLO,
								 IE_FORMA_CALCULO,
								 NR_SEQ_TOXINA)
							values (
								 ARTIC_MOV_MUSCULO_REGRA_seq.nextval,
								 sysdate,
								 nm_usuario_p,
								 sysdate,
								 nm_usuario_p,
								 c04_w.QT_IDADE_MIN,
								 c04_w.QT_IDADE_MAX,
								 c04_w.QT_DOSE_MIN,
								 c04_w.QT_DOSE_MAX,
								 c04_w.QT_DOSE_PADRAO,
								 nr_seq_ARTIC_MOV_MUSCULO_w,
								 c04_w.QT_PONTOS_PADRAO,
								 c04_w.QT_PONTOS_MIN,
								 c04_w.QT_PONTOS_MAX,
								 c04_w.QT_PESO_MIN,
								 c04_w.QT_PESO_MAX,
								 c04_w.QT_MULTIPLO,
								 c04_w.IE_FORMA_CALCULO,
								 c04_w.NR_SEQ_TOXINA);

					end;
				end loop;
				close C04;
							 

				end;
			end loop;
			close C03;		 
		
		
		
		end;
	end loop;
	close C02;
	
	
	end;
end loop;
close C01;



commit;

end DUPLICAR_PARTE_CORPO_HUMANO;
/