create or replace
procedure GERAR_TRANS_FINANC_GLOSA
	(nr_seq_retorno_p	number,
	 nm_usuario_p		varchar2) is


nr_seq_trans_financ_w	number(10,0);
cd_estabelecimento_w	number(10,0);
nr_seq_partic_w		number(10,0);
nr_seq_ret_glosa_w	number(10,0);

cursor c01 is
select	c.nr_sequencia,
	c.nr_seq_partic
from	convenio_retorno_glosa c,
	convenio_retorno_item b
where	b.nr_sequencia		= c.nr_seq_ret_item
and	b.nr_seq_retorno	= nr_seq_retorno_p;

begin

begin
dbms_application_info.SET_ACTION('GERAR_TRANS_FINANC_GLOSA'); 

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	convenio_retorno
where	nr_sequencia	= nr_seq_retorno_p;

open c01;
loop
fetch c01 into
	nr_seq_ret_glosa_w,
	nr_seq_partic_w;
exit when c01%notfound;

	select	max(nr_seq_trans_financ)
	into	nr_seq_trans_financ_w
	from	regra_trans_fin_ret_glosa
	where	cd_estabelecimento	= cd_estabelecimento_w
	and	(((ie_partic_glosa	= 'P') and (nr_seq_partic_w is not null)) or
		 ((ie_partic_glosa	= 'S') and (nr_seq_partic_w is null)))
	and	cd_estabelecimento	= cd_estabelecimento_w;

	if	(nr_seq_trans_financ_w is not null) then
		update	convenio_retorno_glosa
		set	nr_seq_trans_financ	= nr_seq_trans_financ_w
		where	nr_sequencia		= nr_seq_ret_glosa_w;
	end if;

end loop;
close c01;

dbms_application_info.SET_ACTION('');

exception
	when others then
	dbms_application_info.SET_ACTION('');
	-- #@DS_ERRO#@
	wheb_mensagem_pck.exibir_mensagem_abort(210215, 'DS_ERRO=' || sqlerrm);
end;


-- N�O DAR COMMIT NESTA PROCEDURE	

end GERAR_TRANS_FINANC_GLOSA;
/