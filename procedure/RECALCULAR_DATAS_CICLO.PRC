create or replace
procedure recalcular_datas_ciclo(nr_seq_paciente_p	number,
				dt_inicio_p		date,
				nr_ciclo_inicial_p	number,
				nr_ciclo_final_p	number,
				nr_dia_inicial_p	number,
				nr_dia_final_p		number,
				nm_usuario_p		varchar2,
				ie_gera_data_real_p	varchar,
				ie_consiste_dia_util_p	varchar2,
				dt_atualizacao_nrec_p	date,
				ds_retorno_quimio_p	out varchar2) is
				
nr_ciclo_w			integer;
nr_ciclo_ant_w			integer;
nr_dia_w			integer;
nr_seq_atendimento_w		number(10);
i 				integer;
r2				integer := 1;
dt_inicio_w			date;
dt_prevista_w			date;
ie_interv_ciclo_w		varchar2(1);
ie_cons_dia_ant_ciclo_w		varchar2(10);
cd_estabelecimento_w		number(4);
ds_bloqueio_agenda_qui_w	varchar2(2000);
ie_dia_util_w			varchar2(1);
ie_dia_util_dt_real_w		varchar2(1);
ie_consiste_agenda_quimio_w 	varchar2(1);
qt_dias_intervalo_w		number(3);	
cd_estab_usuario_w		number(4);
qt_nao_util_w			number(3)	:= 0;
ie_consiste_dia_prim_ciclo_w	varchar2(10);
ie_med_casa_dia_nao_util_w	varchar2(3) := 'N';
ie_dia_so_med_casa_w		varchar2(3) := 'N';
dt_real_w			date;
ie_param_1201_w			varchar2(1) := 'N';
qt_horas_dt_real_w		number(15,3);
qt_dias_intervalo_ww		number(10);
prim_dt_prevista_ciclo_w	Date;
ds_retorno_w			varchar2(2000);
ie_tipo_atendimento_w		number(3);
cd_protocolo_w			number(10);
nr_seq_medicacao_w		number(10);

Cursor C01 is
	select	nr_seq_atendimento,
		nr_ciclo,
		replace(replace(ds_dia_ciclo, 'D', ''), 'd', '')
	from	paciente_atendimento
	where	nr_seq_paciente = nr_seq_paciente_p
	and	((nr_ciclo = nr_ciclo_inicial_p and to_number(replace(replace(ds_dia_ciclo, 'D', ''), 'd', '')) >= nr_dia_inicial_p) or
		 (nr_ciclo = nr_ciclo_final_p and (nr_dia_final_p is null or to_number(replace(replace(ds_dia_ciclo, 'D', ''), 'd', '')) <= nr_dia_final_p)) or
		 (nr_ciclo > nr_ciclo_inicial_p and nr_ciclo < nr_ciclo_final_p))
	and  dt_atualizacao_nrec = dt_atualizacao_nrec_p
	order by nr_ciclo, to_number(replace(replace(ds_dia_ciclo, 'D', ''), 'd', ''));
	
begin
cd_estab_usuario_w	:= wheb_usuario_pck.get_cd_estabelecimento;
obter_param_usuario(281,617,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w,ie_consiste_dia_prim_ciclo_w);
obter_param_usuario(281,1054,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w,ie_consiste_agenda_quimio_w);
obter_param_usuario(281,1201,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w,qt_horas_dt_real_w);
if	(qt_horas_dt_real_w > 0) then
	ie_param_1201_w := 'S';
else
	ie_param_1201_w := 'N';
end if;

select	max(b.ie_tipo_atendimento)
into	ie_tipo_atendimento_w
from	paciente_atendimento a,
	atendimento_paciente b
where	a.nr_atendimento = b.nr_atendimento
and	a.nr_seq_paciente = nr_seq_paciente_p;

select	nvl(max(nvl(a.qt_dias_intervalo,b.nr_dias_intervalo)),0)
into	qt_dias_intervalo_w
from	protocolo_medicacao b,
	paciente_setor a
where	a.nr_seq_paciente	= nr_seq_paciente_p
and	a.cd_protocolo		= b.cd_protocolo (+)
and	a.nr_seq_medicacao	= b.nr_sequencia (+);

select	nvl(max(b.cd_estabelecimento),1)
into	cd_estabelecimento_w
from	paciente_setor		b, 
	paciente_atendimento	a
where	a.nr_seq_paciente	= b.nr_seq_paciente 
and	a.nr_seq_paciente	= nr_seq_paciente_p;

select 	max(cd_protocolo),
	max(nr_seq_medicacao)
into	cd_protocolo_w,
	nr_seq_medicacao_w
from	paciente_setor
where	nr_seq_paciente = nr_seq_paciente_p;

select	nvl(max( ie_interv_ciclo ),'I'),
	nvl(max(ie_cons_dia_ant_ciclo),'N'),
	nvl(max(ie_gerar_med_casa_dia_nao_util),'N')
into	ie_interv_ciclo_w,
	ie_cons_dia_ant_ciclo_w,
	ie_med_casa_dia_nao_util_w
from	parametro_medico
where	cd_estabelecimento	= cd_estabelecimento_w;

dt_inicio_w	:= dt_inicio_p;
if	(nr_dia_inicial_p	> 1 ) and
	(ie_interv_ciclo_w	= 'I')  and
	(ie_cons_dia_ant_ciclo_w = 'S') then
	
	for i in 1..nr_dia_inicial_p loop
		begin
		dt_inicio_w	:= dt_inicio_w - 1;
		
		ie_dia_util_w		:= obter_se_dia_util_oncologia(dt_inicio_w,cd_estabelecimento_w,nm_usuario_p,ie_tipo_atendimento_w,cd_protocolo_w,nr_seq_medicacao_w);
		if	(ie_consiste_agenda_quimio_w	= 'S') and 
			(ie_dia_util_w	= 'S') then
			ds_bloqueio_agenda_qui_w	:= qt_obter_se_dia_livre(dt_inicio_w, nm_usuario_p, cd_estabelecimento_w);
			if	(ds_bloqueio_agenda_qui_w is not null) then
				ie_dia_util_w	:= 'N';
				ds_retorno_w	:= ds_retorno_w || ds_bloqueio_agenda_qui_w || ', ';
			end if;
		end if;
		
		while 	(ie_dia_util_w	= 'N') loop 
			begin
			dt_inicio_w	:= dt_inicio_w -1;					 
			ie_dia_util_w	:= obter_se_dia_util_oncologia(dt_inicio_w,cd_estabelecimento_w,nm_usuario_p,ie_tipo_atendimento_w,cd_protocolo_w,nr_seq_medicacao_w);
			 
			if	(ie_consiste_agenda_quimio_w	= 'S') and 
				(ie_dia_util_w	= 'S') then
				ds_bloqueio_agenda_qui_w	:= qt_obter_se_dia_livre(dt_inicio_w, nm_usuario_p, cd_estabelecimento_w);
				if	(ds_bloqueio_agenda_qui_w is not null) then
					ie_dia_util_w	:= 'N';
				end if;
			end if;
			end;
		end loop;	
		
		end;
	end loop;
	
	dt_inicio_w	:= dt_inicio_w - (nr_dia_inicial_p-1);
elsif	(nr_dia_inicial_p	> 0) then
	dt_inicio_w	:= dt_inicio_p - nr_dia_inicial_p;

end if;

qt_nao_util_w := 0;
prim_dt_prevista_ciclo_w := null;

open C01;
loop
fetch C01 into	
	nr_seq_atendimento_w,
	nr_ciclo_w,
	nr_dia_w;
exit when C01%notfound;
	begin

	if	(nr_ciclo_w > nr_ciclo_inicial_p) and (nr_ciclo_ant_w <> nr_ciclo_w) then
		qt_dias_intervalo_ww	:= obter_dias_interv_protoc(nr_seq_paciente_p,nr_ciclo_w +1);
		
		if	(ie_interv_ciclo_w	= 'I') then
			dt_inicio_w		:= dt_inicio_w 		+ nvl(qt_dias_intervalo_ww,qt_dias_intervalo_w);
		elsif	(ie_interv_ciclo_w	= 'U') then
			dt_inicio_w		:= dt_prevista_w 	+ nvl(qt_dias_intervalo_ww,qt_dias_intervalo_w) -1;
		end if;
		
		qt_nao_util_w := 0;
		prim_dt_prevista_ciclo_w := null;
	end if;

	dt_prevista_w	:= dt_inicio_w + nr_dia_w + qt_nao_util_w;
	if (ie_consiste_dia_util_p = 'S') then
		ie_dia_util_w	:= 'N';
		while	(ie_dia_util_w = 'N') loop
			ie_dia_util_w	:= obter_se_dia_util_oncologia(dt_prevista_w, cd_estabelecimento_w, nm_usuario_p,ie_tipo_atendimento_w,cd_protocolo_w,nr_seq_medicacao_w);					
			if	(ie_consiste_agenda_quimio_w	= 'S') and 
				(ie_dia_util_w	= 'S') then
				ds_bloqueio_agenda_qui_w	:= qt_obter_se_dia_livre(dt_prevista_w, nm_usuario_p, cd_estabelecimento_w);
				if	(ds_bloqueio_agenda_qui_w is not null) then
					ie_dia_util_w	:= 'N';
					ds_retorno_w	:= ds_retorno_w || ds_bloqueio_agenda_qui_w || ', ';
				end if;
			end if;

			ie_dia_so_med_casa_w := 'N';
			if	(ie_med_casa_dia_nao_util_w = 'S') then
				select	decode(count(*),0,'S','N')
				into	ie_dia_so_med_casa_w
				from	material b,
					paciente_protocolo_medic a
				where	a.cd_material		= b.cd_material
				and	a.nr_seq_paciente	= nr_seq_paciente_p
				and	a.nr_seq_diluicao is null
				and	a.nr_seq_solucao is null
				and	a.nr_seq_procedimento is null
				and	nvl(a.ie_local_adm,'H') <> 'C'
				and	instr(','||replace(ds_dias_aplicacao,' ','')||',', ',D'||nr_dia_w||',') > 0;
			
			end if;

			ie_dia_util_w	:=	'S';
			if 	((ie_med_casa_dia_nao_util_w = 'N') or (ie_dia_so_med_casa_w = 'N')) then
				select	obter_se_dia_util_oncologia(dt_prevista_w, cd_estabelecimento_w, nm_usuario_p,ie_tipo_atendimento_w,cd_protocolo_w,nr_seq_medicacao_w)
				into	ie_dia_util_w
				from	dual;
				if	(ie_consiste_agenda_quimio_w	= 'S') and 
					(ie_dia_util_w	= 'S') then
					ds_bloqueio_agenda_qui_w	:= qt_obter_se_dia_livre(dt_prevista_w, nm_usuario_p, cd_estabelecimento_w);
					if	(ds_bloqueio_agenda_qui_w is not null) then
						ie_dia_util_w	:= 'N';
						ds_retorno_w	:= ds_retorno_w || ds_bloqueio_agenda_qui_w || ', ';
					end if;
				end if;
			end if;

			if	(ie_dia_util_w = 'N') then
				qt_nao_util_w	:= qt_nao_util_w + 1;
				dt_prevista_w	:= dt_prevista_w + 1;
			end if;
		end loop;
	end if;
	if	(dt_prevista_w < sysdate) and
		(nr_dia_w > 0) then
		dt_prevista_w	:= sysdate; /* Felipe - OS 31185 Se a data gerada do ciclo for menos que sysdate a data recebe sysdate */
	end if;	


	if	(ie_gera_data_real_p	= 'S') then
		dt_real_w	:= dt_prevista_w;

		if	(ie_param_1201_w = 'S') and (trunc(dt_inicio_p) = trunc(sysdate)) then
			dt_real_w	:= dt_prevista_w + (qt_horas_dt_real_w / 24);
			
			ie_dia_util_dt_real_w	:=	'N';
			if (ie_consiste_dia_util_p = 'S') then
				r2 := 1;
				while	(ie_dia_util_dt_real_w = 'N') loop
					select	obter_se_dia_util_oncologia(dt_real_w, cd_estabelecimento_w, nm_usuario_p,ie_tipo_atendimento_w,cd_protocolo_w,nr_seq_medicacao_w)
					into	ie_dia_util_dt_real_w
					from	dual;
					if	(ie_consiste_agenda_quimio_w	= 'S') and 
						(ie_dia_util_dt_real_w	= 'S') then
						ds_bloqueio_agenda_qui_w	:= qt_obter_se_dia_livre(dt_real_w, nm_usuario_p, cd_estabelecimento_w);
						if	(ds_bloqueio_agenda_qui_w is not null) then
							ie_dia_util_dt_real_w	:= 'N';
							ds_retorno_w	:= ds_retorno_w || ds_bloqueio_agenda_qui_w || ', ';
						end if;
					end if;
					
					if	(ie_dia_util_dt_real_w = 'N') then
						dt_real_w		:= dt_real_w + 1;
					end if;
					
					r2 := r2 + 1;
					if	(r2 > 100) then
						dt_real_w	:= dt_prevista_w;
						ie_dia_util_dt_real_w	:= 'S';
					end if;
				end loop;
			end if;
		end if;
	end if;

	if	(prim_dt_prevista_ciclo_w is null) then
		prim_dt_prevista_ciclo_w := dt_prevista_w;
	end if;
	
	update	paciente_atendimento
	set	dt_prevista = dt_prevista_w,
		dt_real = decode(ie_gera_data_real_p,'S',dt_real_w,dt_real),
		ds_dia_ciclo_real = 'D' || to_char(trunc(trunc(dt_prevista_w) -  trunc(prim_dt_prevista_ciclo_w))+1)
	where	nr_seq_atendimento = nr_seq_atendimento_w;

	nr_ciclo_ant_w := nr_ciclo_w;
	
	end;
end loop;
close C01;	

commit; 

ds_retorno_quimio_p	:= substr(substr(ds_retorno_w,1,length(ds_retorno_w) - 2),1,255);

end recalcular_datas_ciclo;
/