create or replace
procedure vincular_ordem_compra_adiant(	nr_adiant_pago_p		number,
					nr_ordem_compra_p	number,
					ie_acao_p		varchar2,
					nm_usuario_p		varchar2,
					ds_historico_p		varchar2) is

/* ie_acao_p

V	Vincular
D	Desvincular

*/

vl_vinculado_w		number(15,2)	:= 0;
dt_baixa_w		date;
ds_erro_w		varchar2(255)	:= '';
qt_ordem_w		number(10,0);

begin

select	count(*)
into	qt_ordem_w
from	ordem_compra
where	nr_ordem_compra	= nr_ordem_compra_p;

if	(qt_ordem_w = 0) then
	--(-20011,'Ordem de compra inexistente, favor verificar o n�mero digitado!');
	wheb_mensagem_pck.exibir_mensagem_abort(254821);
end if;
	

if	(nr_adiant_pago_p is not null) then

	select	dt_baixa
	into	dt_baixa_w
	from	ordem_compra
	where	nr_ordem_compra	= nr_ordem_compra_p;

	if	(dt_baixa_w is not null) then
		--(-20011,'A ordem de compra selecionada j� possui data de baixa!');
		wheb_mensagem_pck.exibir_mensagem_abort(254822);
		
	end if;

	if	(nvl(ie_acao_p,'V') = 'V') and
		(nr_ordem_compra_p is not null) then

		select	vl_saldo
		into	vl_vinculado_w
		from	adiantamento_pago
		where	nr_adiantamento	= nr_adiant_pago_p;

		consistir_ordem_compra_adiant(nr_ordem_compra_p,nr_adiant_pago_p,vl_vinculado_w,ds_erro_w, nm_usuario_p);

		if	(nvl(ds_erro_w,'X') <> 'X') then
			--(-20011,ds_erro_w);
			wheb_mensagem_pck.exibir_mensagem_abort(254823,'DS_ERRO='||ds_erro_w);
		end if;

		insert	into	ordem_compra_adiant_pago
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_ordem_compra,
			nr_adiantamento,
			vl_vinculado)
		values	(ordem_compra_adiant_pago_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_ordem_compra_p,
			nr_adiant_pago_p,
			vl_vinculado_w);

	elsif	(ie_acao_p	= 'D') then

		insert	into adiantamento_pago_hist
			(ds_historico,
			dt_atualizacao,
			nm_usuario,
			nr_adiantamento,
			nr_sequencia)
		values	(WHEB_MENSAGEM_PCK.get_texto(298005,'NR_ORDEM_COMPRA_P=' || nr_ordem_compra_p || ';' ||
							'DS_HISTORICO_P=' || ds_historico_p),
			sysdate,
			nm_usuario_p,
			nr_adiant_pago_p,
			adiantamento_pago_hist_seq.nextval);

		delete	from ordem_compra_adiant_pago
		where	nr_adiantamento	= nr_adiant_pago_p
		and	nr_ordem_compra	= nr_ordem_compra_p;

	end if;

	atualiza_adiantamento_pago(0,nr_adiant_pago_p,vl_vinculado_w,0,nm_usuario_p,'I');

	commit;

end if;

end;
/
