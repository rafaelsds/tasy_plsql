Create or replace
Procedure Gravar_movto_funcionario	(	cd_pessoa_fisica_p			varchar2,
					nm_usuario_p			varchar2,
					ie_acao_p			number,
					dt_referencia_p			date,
					cd_estabelecimento_p		number,
					cd_cargo_p			 number,
					cd_cargo_ant_p			number,
					cd_setor_atendimento_p		 number,
					cd_setor_ant_p			number,
					ds_observacao_p			varchar2,
					nm_usuario_atual_p			varchar2,
					ie_gerar_cargo_perfil_p		varchar2,
					ie_consiste_usuario_p		varchar2,
					ie_apagar_inf_demitir_p		varchar2) is

/*	IE_ACAO_P

1	- Gerar demiss�o
2	- Trocar setor
3	- Trocar cargo
4	- Re-admitir

*/

ds_observacao_w			varchar2(255);
dt_referencia_w			date;
ie_tipo_evolucao_w			varchar2(3);
nr_seq_classif_w			number(10);
qt_medico_w			number(10);
ie_medico_w			varchar2(1);
cd_cargo_w			number(10);
nr_seq_perfil_w			number(10);
cd_cargo_ww			number(10);
cd_setor_atendimento_w		number(10);
cd_setor_atendimento_ww		number(10);
qt_usuario_w			number(10);
ie_gerar_cargo_perfil_w		varchar2(10);
qt_reg_loc_trab_w		number(10);
dt_admissao_hosp_w		date;
ie_ativa_funcionario_w		varchar2(1);
cd_cbo_w			number(6);
cd_perfil_ativo_w		perfil.cd_perfil%type	:= obter_perfil_ativo;
ie_limpa_dados_usu_w		varchar2(1);
qt_setor_w			number(10);

begin
ds_observacao_w		:= nvl(substr(ds_observacao_p,1,255),'');
dt_referencia_w		:= nvl(trunc(dt_referencia_p), sysdate);

select	max(cd_cargo),
	max(dt_admissao_hosp)
into	cd_cargo_ww,
	dt_admissao_hosp_w
from	pessoa_fisica
where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

select	max(cd_setor_atendimento)
into	cd_setor_atendimento_ww
from	usuario
where	upper(nm_usuario)	= upper(nm_usuario_atual_p);

if	(nvl(cd_cargo_p,0) <= 0) then
	cd_cargo_w	:= cd_cargo_ww;
else
	cd_cargo_w	:= cd_cargo_p;
end if;

if	(nvl(cd_setor_atendimento_p,0)	<= 0)then
	cd_setor_atendimento_w	:=  cd_setor_atendimento_ww;

else
	cd_setor_atendimento_w	:= cd_setor_atendimento_p;
end if;


if	(ie_consiste_usuario_p = 'S' and nm_usuario_atual_p is null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(186208);
end if;

obter_param_usuario(230,10,cd_perfil_ativo_w,nm_usuario_p,cd_estabelecimento_p,ie_gerar_cargo_perfil_w);

obter_param_usuario(6001,161,cd_perfil_ativo_w,nm_usuario_p,cd_estabelecimento_p,ie_limpa_dados_usu_w);

if	(ie_acao_p = 1) then	/* demitir funcion�rio	*/
	begin
	if	(ie_limpa_dados_usu_w = 'S') then
		begin
		delete	from usuario_setor
		where	nm_usuario_param	= nm_usuario_atual_p;

		delete	from usuario_perfil
		where	nm_usuario		= nm_usuario_atual_p;
		end;
	end if;

	if	(ie_apagar_inf_demitir_p = 'S') then	
		update	pessoa_fisica
		set	dt_demissao_hosp	= dt_referencia_w,
			ie_funcionario		= 'N',
			cd_cargo		= null,
			cd_funcionario		= null,
			cd_sistema_ant		= null,
			dt_admissao_hosp	= null,
			nr_seq_conselho		= null,
			ds_codigo_prof		= null,
			cd_cbo_sus		= null,
			dt_primeira_admissao	= dt_admissao_hosp_w
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	else
		update	pessoa_fisica
		  set	dt_demissao_hosp	= dt_referencia_w,
			ie_funcionario		= 'N',
			dt_primeira_admissao	= dt_admissao_hosp_w
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p;	
	end if;

	update	usuario
	set	ie_situacao		= 'I',
		dt_inativacao		= sysdate,
		cd_setor_atendimento	= null,
		ie_tipo_evolucao	= null,
		nr_seq_classif		= null
	where	nm_usuario		= nm_usuario_atual_p
	and	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	  

	update	regra_pf_loc_patologia
	set	ie_situacao		= 'I'
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

	select	count(*) 
	into	qt_usuario_w
	from 	usuario 
	where 	nm_usuario	= nm_usuario_atual_p;
	
	if	(qt_usuario_w > 0) then -- afstringari 201032 11/03/2010
		--lhalves OS 196458 19/02/2010
		insert into usuario_hist(
			nr_sequencia,
			nm_usuario,
			nm_usuario_ref,
			ds_alteracao,
			dt_atualizacao)
		values	(usuario_hist_seq.nextval,
			nm_usuario_p,
			nm_usuario_atual_p,
			nvl(substr(ds_observacao_p,1,2000),wheb_mensagem_pck.get_texto(338711)),
			sysdate);
	end if;

	if	(nm_usuario_atual_p is not null) then
		insert into usuario_setor_cargo
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_referencia,
			ds_observacao,
			nm_usuario_param)
		select	usuario_setor_cargo_seq.nextval,
			sysdate,
			nm_usuario_p,
			dt_referencia_w,
			substr(ds_observacao_w,1,255),
			nm_usuario_atual_p
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	end if;
	end;
elsif	(ie_acao_p = 2) then	/* trocar setor funcion�rio */
	begin
	select 	count(*)
	into	qt_reg_loc_trab_w
	from	pessoa_fisica_loc_trab
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	
	if	(qt_reg_loc_trab_w > 0) then
		update	pessoa_fisica_loc_trab
		set	cd_setor_atendimento	= cd_setor_atendimento_p
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	end if;
	
	if 	(nm_usuario_atual_p is not null) then /* Elton OS214374 - Se a pessoa n�o possuir usu�rio, troca o setor da pasta "Local de trabalho"     */
		insert into usuario_setor_cargo
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_referencia,
			ds_observacao,
			nm_usuario_param,
			cd_setor_atendimento)
		select	usuario_setor_cargo_seq.nextval,
			sysdate,
			nm_usuario_p,
			dt_referencia_w,
			substr(ds_observacao_w,1,255),
			nm_usuario_atual_p,
			decode(cd_setor_ant_p,0,null,cd_setor_ant_p)
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

		update	usuario
		set	cd_setor_atendimento	= cd_setor_atendimento_p
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	cd_estabelecimento	= cd_estabelecimento_p;
		  
		if	(nvl(ie_gerar_cargo_perfil_w,'N') = 'S') then
			gerar_cargo_perfil_setor(	cd_cargo_w,
							cd_pessoa_fisica_p,
							'',
							cd_setor_atendimento_w,
							nm_usuario_p,
							nm_usuario_atual_p,
							cd_estabelecimento_p);
		end if;
	end if;
	end;
elsif	(ie_acao_p = 3) then	/* trocar cargo funcion�rio */
	begin	
	select	nvl(ie_medico,'N')
	into	ie_medico_w
	from	cargo
	where	cd_cargo	= cd_cargo_p;
	
	select	count(*)
	into	qt_medico_w
	from	medico
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

	if	(ie_medico_w = 'S') and
		(qt_medico_w = 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(186207);
	end if;

	if	(nm_usuario_atual_p is not null) or 
		(cd_pessoa_fisica_p is not null) then
		insert into usuario_setor_cargo
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_referencia,
			ds_observacao,
			nm_usuario_param,
			cd_cargo,
			cd_pessoa_fisica)
		select	usuario_setor_cargo_seq.nextval,
			sysdate,
			nm_usuario_p,
			dt_referencia_w,
			substr(ds_observacao_w,1,255),
			nm_usuario_atual_p,
			cd_cargo_ant_p,
			cd_pessoa_fisica
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	end if;
	
	/* afstringari OS161967 */
	select	max(nr_seq_perfil),
		max(cd_cbo_sus)
	into	nr_seq_perfil_w,
		cd_cbo_w
	from	cargo
	where	cd_cargo	= cd_cargo_p;
	
	update	pessoa_fisica
	set	cd_cargo 		= cd_cargo_p,
		nr_seq_perfil		= nr_seq_perfil_w, /* afstringari OS161967 */ 
		cd_cbo_sus		= cd_cbo_w	
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

	select	ie_tipo_evolucao,
		nr_seq_classif
	into	ie_tipo_evolucao_w,
		nr_seq_classif_w
	from	cargo
	where	cd_cargo		= cd_cargo_p;

	update	usuario
	set	ie_tipo_evolucao	= nvl(ie_tipo_evolucao_w,ie_tipo_evolucao),
		nr_seq_classif		= nr_seq_classif_w
	where	nm_usuario		= nm_usuario_atual_p
	and	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	cd_estabelecimento	= cd_estabelecimento_p;
	end;

	if	(nvl(ie_gerar_cargo_perfil_w,'N') = 'S') and
		(nm_usuario_atual_p is not null) then
		gerar_cargo_perfil_setor(	cd_cargo_w,
						cd_pessoa_fisica_p,
						'',
						cd_setor_atendimento_w,
						nm_usuario_p,
						nm_usuario_atual_p,
						cd_estabelecimento_p);
						
		select	count(1)
		into	qt_setor_w
		from	cargo_setor
		where	cd_cargo		= cd_cargo_w
		and	cd_setor_atendimento	= cd_setor_atendimento_w
		and	rownum			= 1;
		
		if	(qt_setor_w = 0) then
			update	usuario
			set	cd_setor_atendimento	= null
			where	nm_usuario		= nm_usuario_atual_p
			and	cd_pessoa_fisica	= cd_pessoa_fisica_p
			and	cd_estabelecimento	= cd_estabelecimento_p;
			
			delete from usuario_setor_cargo
			where	nm_usuario		= nm_usuario_atual_p
			and	cd_setor_atendimento	= cd_setor_atendimento_w;
		end if;
	end if;
elsif	(ie_acao_p = 4) then	/* Re-admitir funcion�rio - ahoffelder - OS 191295 - 26/01/2010 */
	update	pessoa_fisica
	set	dt_demissao_hosp	= null,
		dt_admissao_hosp	= sysdate,
		ie_funcionario		= 'S',
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_atual_p
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p;

	ie_ativa_funcionario_w	:= Obter_Valor_Param_Usuario(230,48,Obter_Perfil_Ativo,nm_usuario_p,0);	
	
	if	(ie_ativa_funcionario_w = 'S') then
		update	usuario
		set	ie_situacao		= 'A',
			dt_inativacao		= null,
			dt_atualizacao		= sysdate,
			nm_usuario_atual	= nm_usuario_atual_p
		where	cd_pessoa_fisica	= cd_pessoa_fisica_p;
	end if;
	
	update	regra_pf_loc_patologia
	set	ie_situacao = 'A'
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;	
end if;

commit;

end Gravar_movto_funcionario;
/