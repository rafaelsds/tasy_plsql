create or replace
procedure	envia_sup_int_oc(
			nr_seq_regra_integ_p		number,
			nr_ordem_compra_p		number,
			cd_estabelecimento_p		number,
			cd_cgc_fornecedor_p		varchar2,
			cd_condicao_pagamento_p		number,
			cd_comprador_p			varchar2,
			dt_ordem_compra_p		date,
			cd_moeda_p			number,
			cd_pessoa_solicitante_p		varchar2,
			cd_cgc_transportador_p		varchar2,
			ie_frete_p			varchar2,
			vl_frete_p			number,
			pr_desconto_p			number,
			pr_desc_pgto_antec_p		number,
			pr_juros_negociado_p		number,
			ds_pessoa_contato_p		varchar2,
			ds_observacao_p			varchar2,
			cd_local_entrega_p		number,
			dt_entrega_p			date,
			ie_aviso_chegada_p		varchar2,
			vl_despesa_acessoria_p		number,
			nr_seq_subgrupo_compra_p		number,
			pr_desc_financeiro_p		number,
			cd_pessoa_fisica_p		varchar2,
			ie_urgente_p			varchar2,
			nr_seq_forma_pagto_p		number,
			nr_documento_externo_p		number,
			vl_desconto_p			number,
			cd_centro_custo_p		number,
			nm_comprador_p			varchar2,
			cd_func_comprador_p		varchar2,
			ie_tipo_oc_p			varchar2,
			ie_situacao_oc_p		varchar2,			
			nm_usuario_p			varchar2) is

ie_de_para_unid_med_w		varchar2(15);
ie_de_para_material_w		varchar2(15);
qt_existe_w			number(10);
nr_sequencia_w			number(10);

nr_item_oci_w			number(5);
cd_material_w			number(6);
cd_unidade_medida_compra_w	varchar2(30);
vl_unitario_material_w		number(13,4);
qt_material_w			number(13,4);
ds_material_direto_w		varchar2(255);
ds_observacao_w			varchar2(255);
cd_centro_custo_w		number(8);
cd_conta_contabil_w		varchar2(20);
qt_existe_item_w			number(10);
ie_envia_sempre_w		varchar2(1);
vl_ipi_w			sup_int_oc_item.vl_ipi%type;
tx_ipi_w			sup_int_oc_item.tx_ipi%type;
pr_descontos_w			sup_int_oc_item.pr_descontos%type;
vl_desconto_w			sup_int_oc_item.vl_desconto%type;
nr_seq_marca_w			ordem_compra_item.nr_seq_marca%type;

cursor	c01 is
select	nr_item_oci,
	cd_material,
	cd_unidade_medida_compra,
	vl_unitario_material,
	qt_material,
	ds_material_direto,
	ds_observacao,
	cd_centro_custo,
	cd_conta_contabil,
	nr_seq_marca
from	ordem_compra_item
where	nr_ordem_compra = nr_ordem_compra_p;

begin

select	obter_ie_de_para_sup_integr('OC','E','UNIDADE_MEDIDA'),
	obter_ie_de_para_sup_integr('OC','E','MATERIAL')
into	ie_de_para_unid_med_w,
	ie_de_para_material_w
from	dual;

select	nvl(max(ie_envia_sempre),'N')
into	ie_envia_sempre_w
from	sup_int_regra_oc
where	nr_sequencia = nr_seq_regra_integ_p;

if	(ie_envia_sempre_w = 'S') then
	qt_existe_w	:= 0;	
else
	begin

	select	count(*)
	into	qt_existe_w
	from	sup_int_oc
	where	nr_ordem_compra = nr_ordem_compra_p
	and	ie_forma_integracao = 'E';

	end;
end if;

if	(qt_existe_w = 0) then
	begin

	select	SUP_INT_OC_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into sup_int_oc(
		nr_sequencia,
		ie_forma_integracao,
		nr_ordem_compra,
		cd_estabelecimento,
		cd_cgc_fornecedor,
		cd_condicao_pagamento,
		cd_comprador,
		dt_ordem_compra,
		cd_moeda,
		cd_pessoa_solicitante,
		cd_cgc_transportador,
		ie_frete,
		vl_frete,
		pr_desconto,
		pr_desc_pgto_antec,
		pr_juros_negociado,
		ds_pessoa_contato,
		ds_observacao,
		cd_local_entrega,
		dt_entrega,
		ie_aviso_chegada,
		vl_despesa_acessoria,
		nr_seq_subgrupo_compra,
		pr_desc_financeiro,
		cd_pessoa_fisica,
		ie_urgente,
		nr_seq_forma_pagto,
		nr_documento_externo,
		vl_desconto,
		cd_centro_custo,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nm_comprador,
		cd_func_comprador,
		ie_tipo_oc,
		ie_situacao_oc) values (
			nr_sequencia_w,
			'E',
			nr_ordem_compra_p,
			cd_estabelecimento_p,
			cd_cgc_fornecedor_p,
			cd_condicao_pagamento_p,
			cd_comprador_p,
			dt_ordem_compra_p,
			cd_moeda_p,
			cd_pessoa_solicitante_p,
			cd_cgc_transportador_p,
			ie_frete_p,
			vl_frete_p,
			pr_desconto_p,
			pr_desc_pgto_antec_p,
			pr_juros_negociado_p,
			ds_pessoa_contato_p,
			ds_observacao_p,
			cd_local_entrega_p,
			dt_entrega_p,
			ie_aviso_chegada_p,
			vl_despesa_acessoria_p,
			nr_seq_subgrupo_compra_p,
			pr_desc_financeiro_p,
			cd_pessoa_fisica_p,
			ie_urgente_p,
			nr_seq_forma_pagto_p,
			nr_documento_externo_p,
			vl_desconto_p,
			cd_centro_custo_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nm_comprador_p,
			cd_func_comprador_p,
			ie_tipo_oc_p,
			ie_situacao_oc_p);
	
	end;
else
	begin

	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	sup_int_oc
	where	nr_ordem_compra = nr_ordem_compra_p;
	
	end;
end if;

open c01;
loop
fetch c01 into
	nr_item_oci_w,
	cd_material_w,
	cd_unidade_medida_compra_w,
	vl_unitario_material_w,
	qt_material_w,
	ds_material_direto_w,
	ds_observacao_w,
	cd_centro_custo_w,
	cd_conta_contabil_w,
	nr_seq_marca_w;
exit when c01%notfound;
	begin
	
	select	max(vl_ipi),
		max(tx_ipi),
		max(pr_descontos)
	into	vl_ipi_w,
		tx_ipi_w,
		pr_descontos_w
	from	sup_int_oc_item
	where	nr_sequencia = nr_sequencia_w
	and	nr_item_oci = nr_item_oci_w;

	if (nvl(pr_descontos_w,0) > 0) then
		vl_desconto_w := dividir((qt_material_w * vl_unitario_material_w * pr_descontos_w),100);
	else
		vl_desconto_w := pr_descontos_w;
	end if;
	
	select	count(*)
	into	qt_existe_item_w
	from	sup_int_oc_item
	where	nr_sequencia = nr_sequencia_w
	and	nr_item_oci = nr_item_oci_w;

	if	(qt_existe_item_w > 0) then
		begin

		delete
		from	sup_int_oc_item
		where	nr_sequencia = nr_sequencia_w
		and	nr_item_oci = nr_item_oci_w;

		end;
	end if;

	/*Conversao para unidade de medida*/
	if	(ie_de_para_unid_med_w = 'C') then
		cd_unidade_medida_compra_w	:= nvl(Obter_Conversao_externa(null,'UNIDADE_MEDIDA','CD_UNIDADE_MEDIDA',cd_unidade_medida_compra_w),cd_unidade_medida_compra_w);
	elsif	(ie_de_para_unid_med_w = 'S') then		
		cd_unidade_medida_compra_w	:= nvl(obter_dados_unid_medida(cd_unidade_medida_compra_w,'SA'),cd_unidade_medida_compra_w);
	end if;

	/*Conversao para material*/
	if	(ie_de_para_material_w = 'C') then
		cd_material_w		:= nvl(Obter_Conversao_externa(null,'MATERIAL','CD_MATERIAL',cd_material_w),cd_material_w);
	elsif	(ie_de_para_material_w = 'S') then		
		cd_material_w	:= nvl(obter_dados_material_estab(cd_material_w, cd_estabelecimento_p, 'CSA'),cd_material_w);
	end if;

	insert into sup_int_oc_item(
		nr_sequencia,
		nr_item_oci,
		nr_ordem_compra,
		cd_material,
		cd_unidade_medida_compra,
		vl_unitario_material,
		qt_material,
		ds_material_direto,
		ds_observacao,
		cd_centro_custo,
		cd_conta_contabil,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		vl_ipi,
		tx_ipi,
		pr_descontos,
		vl_desconto,
		nr_seq_marca) values (
			nr_sequencia_w,
			nr_item_oci_w,
			nr_ordem_compra_p,
			cd_material_w,
			cd_unidade_medida_compra_w,
			vl_unitario_material_w,
			qt_material_w,
			ds_material_direto_w,
			ds_observacao_w,
			cd_centro_custo_w,
			cd_conta_contabil_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			vl_ipi_w,
			tx_ipi_w,
			pr_descontos_w,
			vl_desconto_w,
			nr_seq_marca_w);

	end;
end loop;
close c01;

update	sup_int_oc
set	dt_liberacao = sysdate
where	nr_sequencia = nr_sequencia_w;

/* N�o pode dar commit pois � chamado em trigger */

end envia_sup_int_oc;
/