create or replace
procedure carregar_juros_multa (
		nr_titulo_p	number,
		nr_sequencia_p 	number) is 

begin
if	(nr_titulo_p is not null) and
	(nr_sequencia_p is not null)then 
	begin 
	update 	titulo_receber_liq
	set     	vl_juros = to_number(obter_juros_multa_titulo(nr_titulo,sysdate,'R','J')),
		vl_multa = to_number(obter_juros_multa_titulo(nr_titulo,sysdate,'R','M'))
	where   	nr_titulo      = nr_titulo_p
	and     	nr_sequencia   = nr_sequencia_p;
	commit;
	end;
end if;
end carregar_juros_multa;
/