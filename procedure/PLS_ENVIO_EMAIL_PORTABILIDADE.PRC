create or replace
procedure pls_envio_email_portabilidade
			(	nr_seq_solicitacao_p	pls_portab_pessoa.nr_sequencia%type,
				ie_acao_p		pls_portab_email.ie_tipo_email%type,
				nm_usuario_p		varchar2) is

nr_seq_motivo_recusa_w		pls_portab_email.nr_seq_motivo_recusa%type;
ds_assunto_w			pls_portab_email.ds_assunto%type;
ds_mensagem_w			pls_portab_email.ds_mensagem%type;
ds_remetente_w			varchar2(255);
dt_solicitacao_w		pls_portab_pessoa.dt_solicitacao%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nm_pessoa_w			pessoa_fisica.nm_pessoa_fisica%type;
ds_motivo_recusa_w		pls_portab_motivo_recusa.ds_motivo%type;
ds_tipo_portabilidade_w		pls_tipo_portabilidade.ds_tipo_portabilidade%type;
ds_email_w			compl_pessoa_fisica.ds_email%type;
cd_estabelecimento_w		pls_portab_pessoa.cd_estabelecimento%type;
nr_protocolo_atendimento_w	pls_protocolo_atendimento.nr_protocolo%type;
nr_seq_email_w			pls_email.nr_sequencia%type;
nr_seq_anexo_w			pls_email_anexo.nr_sequencia%type;
ds_valor_parametro_w		varchar2(255);


Cursor C01 is
	select	a.nr_sequencia,
		a.ds_assunto,
		a.ds_mensagem
	from	pls_portab_email a
	where	a.ie_tipo_email = ie_acao_p
	and	(a.nr_seq_motivo_recusa = nr_seq_motivo_recusa_w or a.nr_seq_motivo_recusa is null)
	order by
		nvl(a.nr_seq_motivo_recusa, 0);

Cursor C02	(nr_seq_regra_pc pls_portab_email.nr_sequencia%type)is
	select	cd_classif_relat,
		cd_relatorio,
		ds_arquivo
	from	pls_portab_email_anexo
	where	nr_seq_regra = nr_seq_regra_pc;

Cursor C03 (	cd_classif_relat_pc relatorio.cd_classif_relat%type,
		cd_relatorio_pc relatorio.cd_relatorio%type)is
	select	b.cd_parametro,
		b.ie_tipo_atributo 
	from	relatorio a,
		relatorio_parametro b
	where	a.nr_sequencia	= b.nr_seq_relatorio
	and	a.cd_classif_relat = cd_classif_relat_pc
	and	a.cd_relatorio	= cd_relatorio_pc;
begin
ds_remetente_w	:= Obter_Valor_Param_Usuario(1242, 5, Obter_Perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);

if	(nr_seq_solicitacao_p is not null) and 
	(ie_acao_p is not null) then
	select	a.cd_pessoa_fisica,
		ds_email,
		obter_nome_pf(a.cd_pessoa_fisica),
		a.dt_solicitacao,
		(	select	max(ds_motivo)
			from	pls_portab_motivo_recusa
			where	nr_sequencia = a.nr_seq_motivo_recusa) ds_motivo,
		(	select	max(ds_tipo_portabilidade)
			from	pls_tipo_portabilidade
			where	nr_sequencia = a.ie_tipo_portabilidade) ds_tipo_portabilidade,
		a.nr_seq_motivo_recusa,
		a.cd_estabelecimento,
		pls_obter_nr_protocolo_atend(a.nr_sequencia,'10')		
	into	cd_pessoa_fisica_w,
		ds_email_w,
		nm_pessoa_w,
		dt_solicitacao_w,
		ds_motivo_recusa_w,
		ds_tipo_portabilidade_w,
		nr_seq_motivo_recusa_w,
		cd_estabelecimento_w,
		nr_protocolo_atendimento_w		
	from	pls_portab_pessoa a
	where	nr_sequencia = nr_seq_solicitacao_p;
	
	if	(ds_email_w is not null) then -- SOMENTE DEVE ENVIAR EMAIL SE EXISTIR EMAIL NA TABELA PLS_PORTAB_PESSOA.DS_EMAIL
		for r_c01_w in C01 loop
			begin
			ds_mensagem_w	:= r_c01_w.ds_mensagem;
			ds_assunto_w	:= r_c01_w.ds_assunto;
			ds_mensagem_w	:= substr(replace(ds_mensagem_w, '@DT_SOLICITACAO', to_char(dt_solicitacao_w, 'dd/mm/yyyy hh24:mi:ss')), 1, 4000);
			ds_mensagem_w	:= substr(replace(ds_mensagem_w, '@NM_PESSOA', nm_pessoa_w ), 1, 4000);
			ds_mensagem_w	:= substr(replace(ds_mensagem_w, '@DS_MOTIVO_RECUSA', ds_motivo_recusa_w ), 1, 4000);
			ds_mensagem_w	:= substr(replace(ds_mensagem_w, '@DS_TIPO_PORTABILIDADE', ds_tipo_portabilidade_w ), 1, 4000);
			ds_mensagem_w	:= substr(replace(ds_mensagem_w, '@NR_PROTOCOLO_ATENDIMENTO', nr_protocolo_atendimento_w ), 1, 4000);			
			
			insert	into	pls_email
				(	nr_sequencia, cd_estabelecimento, nm_usuario,
					dt_atualizacao, nm_usuario_nrec, dt_atualizacao_nrec,
					ie_tipo_mensagem, ie_status, ie_origem,
					ds_remetente, ds_mensagem, ds_destinatario,
					ds_assunto, cd_pessoa_fisica,nr_seq_portabilidade)
			values (	pls_email_seq.nextval, cd_estabelecimento_w, nm_usuario_p,
					sysdate, nm_usuario_p, sysdate,
					5, 'P', 5,
					ds_remetente_w, ds_mensagem_w, ds_email_w,
					ds_assunto_w, cd_pessoa_fisica_w,nr_seq_solicitacao_p)
				returning nr_sequencia into nr_seq_email_w;
			
			for r_c02_w in C02(r_c01_w.nr_sequencia) loop
				begin
				
				insert into pls_email_anexo (
							nr_sequencia,
							nr_seq_email,
							nm_usuario,
							nm_usuario_nrec,
							dt_atualizacao,
							dt_atualizacao_nrec,
							cd_classif_relat,
							cd_relatorio,
							ds_arquivo,
							ie_tipo_anexo )
				values(			pls_email_anexo_seq.nextval,
							nr_seq_email_w,
							nm_usuario_p,
							nm_usuario_p,
							sysdate,
							sysdate,
							r_C02_w.cd_classif_relat,
							r_C02_w.cd_relatorio,
							r_C02_w.ds_arquivo,
							decode(r_C02_w.ds_arquivo,null,'R','A')
							) returning nr_sequencia into nr_seq_anexo_w;
							
				if	(r_C02_w.cd_relatorio is not null) then
					for r_C03_w in C03(	r_C02_w.cd_classif_relat,
								r_C02_w.cd_relatorio) loop
						
						if	(upper(r_C03_w.cd_parametro) in ('NR_SEQ_SOLICITACAO','NR_SEQ_PORTABILIDADE')) then
							ds_valor_parametro_w	:= nr_seq_solicitacao_p;
						elsif	(upper(r_c03_w.cd_parametro) = 'CD_PESSOA_FISICA') then
							ds_valor_parametro_w	:= cd_pessoa_fisica_w;
						else
							ds_valor_parametro_w	:= null;
						end if;
						
						insert 	into	pls_email_anexo_param (
										nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										nr_seq_anexo,
										nm_parametro,
										ds_valor_parametro)
						values(				pls_email_anexo_param_seq.nextval,sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										nr_seq_anexo_w,
										r_C03_w.cd_parametro,
										ds_valor_parametro_w
										);
					end loop;
				end if;
				end;
			end loop;
			
			end;
		end loop;
	end if;
end if;

commit;

end pls_envio_email_portabilidade;
/
