create or replace procedure
hd_obter_dados_contr_html5(	nr_seq_dialise_p	number,
					qt_tempo_controle_p	number,
					qt_fluxo_sangue_p	out number, 
					qt_fluxo_dialisado_p	out number, 
					qt_pa_arterial_p	out number, 
					qt_heparina_p		out number, 
					qt_sangue_dial_p	out number,
					ie_clearence_p		out varchar2,
					ie_kvt_p		out varchar2,
					nm_paciente_p		out varchar2,
					hr_controle_p		out varchar2,
					ie_tipo_dialise_p	out varchar2,
					hr_dialise_retro_p	out date) is 

ie_cad_maquinas_w	varchar2(1) := 'N';
begin
ie_cad_maquinas_w := obter_valor_param_usuario(7009,119,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);

HD_Obter_Dados_Ultimo_Controle(nr_seq_dialise_p, qt_fluxo_sangue_p, qt_fluxo_dialisado_p, qt_pa_arterial_p, qt_heparina_p, qt_sangue_dial_p);
if (ie_cad_maquinas_w = 'N') then
	ie_clearence_p 	:= nvl(hd_obter_dados_modelo_maq(HD_Obter_Dados_Maquina(hd_obter_maquina_controle(nr_seq_dialise_p),'M'),'CL'),'N');
	ie_kvt_p	:= nvl(hd_obter_dados_modelo_maq(HD_Obter_Dados_Maquina(hd_obter_maquina_controle(nr_seq_dialise_p),'M'),'K'),'N');
else
	ie_clearence_p  := NVL(HD_Obter_Dados_Maquina(hd_obter_maquina_controle(nr_seq_dialise_p),'CLE'),'N');
	ie_kvt_p	:= NVL(HD_Obter_Dados_Maquina(hd_obter_maquina_controle(nr_seq_dialise_p),'KTV'),'N');
end if;	
hr_controle_p	:= to_char(obter_hora_controle(nr_seq_dialise_p, qt_tempo_controle_p),'hh24:mi');
nm_paciente_p	:= hd_obter_dados_dialise(nr_seq_dialise_p, 'NP');

ie_tipo_dialise_p := hd_obter_dados_dialise(nr_seq_dialise_p, 'TD');

hr_dialise_retro_p := hd_obter_dt_dia_retro_html5(nr_seq_dialise_p);
		          
end hd_obter_dados_contr_html5;
/