create or replace
procedure consiste_nova_solic_compra(	cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				ds_erro_p	out		varchar2) is 

ie_permite_nova_solic_w		varchar2(1);
cd_perfil_w			number(10);
ds_erro_w			varchar2(255) := '';
nr_solic_compra_w			number(10);

begin

cd_perfil_w	:= obter_perfil_ativo;

select	nvl(max(obter_valor_param_usuario(913, 205, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p)),'S')
into	ie_permite_nova_solic_w
from	dual;

if	(ie_permite_nova_solic_w = 'N') then
	
	select	nvl(max(nr_solic_compra),0)
	into	nr_solic_compra_w
	from	solic_compra
	where	cd_estabelecimento = cd_estabelecimento_p
	and	nm_usuario_nrec = nm_usuario_p
	and	dt_baixa is null
	and	dt_liberacao is null;
	
	if	(nr_solic_compra_w > 0) then
		ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(280687) || nr_solic_compra_w || WHEB_MENSAGEM_PCK.get_texto(280688);
	end if;	
end if;

ds_erro_p := ds_erro_w;

end consiste_nova_solic_compra;
/