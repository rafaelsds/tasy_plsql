create or replace
procedure sus_obter_doc_exec_cnes(
		cd_estabelecimento_p 	in 	number,
		cd_cbo_p 		in 	Varchar2,
		cd_medico_executor_p 	in 	varchar2,
		dt_procedimento_p 	in 	date,
		ie_doc_executor_p 	out 	number) is

cd_cnes_hospital_w 	number(7);
ie_doc_executor_w 	number(3) := null;
nr_seq_tipo_vinculo_w	cnes_profissional_vinculo.nr_seq_tipo_vinculo%type;

begin

select 	max(cd_cnes_hospital)
into 	cd_cnes_hospital_w
from 	sus_parametros_aih
where 	cd_estabelecimento = cd_estabelecimento_p;

if (cd_cnes_hospital_w is not null) then
	select  max(z.ie_doc_executor)
	into 	ie_doc_executor_w
	from    cnes_profissional y,
		cnes_identificacao x,
		cnes_profissional_vinculo z
	where   y.nr_seq_identificacao  = x.nr_sequencia
	and     x.CD_CNES               = cd_cnes_hospital_w
	and     y.cd_pessoa_fisica      = cd_medico_executor_p
	and     z.nr_seq_profissional   = y.nr_sequencia
	and     cd_cbo 			= cd_cbo_p
	and     ((z.dt_vigencia_inicial is null) or (pkg_date_utils.get_time(z.dt_vigencia_inicial,0,0,0) <= pkg_date_utils.get_time(dt_procedimento_p,0,0,0)))
	and     ((z.dt_vigencia_final is null) or (pkg_date_utils.get_time(z.dt_vigencia_final,0,0,0) >= pkg_date_utils.get_time(dt_procedimento_p,0,0,0)))
	and     z.ie_atendimento_sus 	= 'S';	
end if;

if	(ie_doc_executor_w is null) then
	begin
	select  max(z.nr_seq_tipo_vinculo)
	into 	nr_seq_tipo_vinculo_w
	from    cnes_profissional y,
		cnes_identificacao x,
		cnes_profissional_vinculo z
	where   y.nr_seq_identificacao  = x.nr_sequencia
	and     x.CD_CNES               = cd_cnes_hospital_w
	and     y.cd_pessoa_fisica      = cd_medico_executor_p
	and     z.nr_seq_profissional   = y.nr_sequencia
	and     cd_cbo 			= cd_cbo_p
	and     ((z.dt_vigencia_inicial is null) or (pkg_date_utils.get_time(z.dt_vigencia_inicial,0,0,0) <= pkg_date_utils.get_time(dt_procedimento_p,0,0,0)))
	and     ((z.dt_vigencia_final is null) or (pkg_date_utils.get_time(z.dt_vigencia_final,0,0,0) >= pkg_date_utils.get_time(dt_procedimento_p,0,0,0)))
	and     z.ie_atendimento_sus 	= 'S';	
	end;
	
	if	(nr_seq_tipo_vinculo_w is not null) then
		begin
		
		begin
		select	ie_doc_executor
		into	ie_doc_executor_w	
		from	cnes_tipo_vinculo
		where	nr_sequencia = nr_seq_tipo_vinculo_w
		and	ie_situacao = 'A';		
		exception
		when others then
			ie_doc_executor_w := null;
		end;
		
		end;
	end if;
end if;

ie_doc_executor_p := ie_doc_executor_w;

end sus_obter_doc_exec_cnes;
/
