create or replace
procedure pls_gerar_lote_pag_tit_mens(	nr_seq_lote_p		number,
					nm_usuario_p		varchar2) is
				
dt_mes_competencia_w		pls_lote_pagamento.dt_mes_competencia%type;
vl_titulo_w			titulo_receber.vl_titulo%type;
nr_seq_pag_prestador_w		pls_pagamento_prestador.nr_sequencia%type;
nr_seq_pag_conta_evento_w	pls_pagamento_item.nr_sequencia%type;
ie_natureza_w			pls_evento.ie_natureza%type;
dt_inicio_comp_w		pls_lote_pagamento.dt_inicio_comp%type;
dt_fim_comp_w			pls_lote_pagamento.dt_fim_comp%type;
nr_seq_periodo_w		pls_lote_pagamento.nr_seq_periodo%type;
ie_incide_periodo_w		pls_evento_regra.ie_incide_periodo%type;
vl_total_titulos_w		titulo_receber.vl_titulo%type;
nr_seq_lote_evento_w		pls_lote_evento.nr_sequencia%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_seq_pag_item_w		pls_pagamento_item.nr_sequencia%type;
						
Cursor C01 (	dt_mes_competencia_pc	pls_lote_pagamento.dt_mes_competencia%type) is
	select	a.nr_seq_pagador,
		a.nr_seq_prestador,
		a.nr_seq_evento,
		a.ie_tipo_mensalidade,
		nvl(b.ie_natureza,'P') ie_natureza
	from	pls_evento		b,
		pls_evento_mensalidade	a
	where	b.nr_sequencia	= a.nr_seq_evento
	and	a.ie_situacao	= 'A'
	and	trunc(dt_mes_competencia_pc) between trunc(a.dt_inicio_vigencia) and nvl(a.dt_fim_vigencia, dt_mes_competencia_pc);
	
Cursor C02 (	nr_seq_pagador_pc	pls_contrato_pagador.nr_sequencia%type,
		dt_inicio_comp_pc	pls_lote_pagamento.dt_inicio_comp%type,
		dt_fim_comp_pc		pls_lote_pagamento.dt_fim_comp%type) is
	select	nvl(b.vl_saldo_titulo,0) vl_titulo,
		trunc(a.dt_referencia, 'month') dt_referencia,
		b.nr_titulo
	from	titulo_receber	b,
		pls_mensalidade	a
	where	a.nr_sequencia		= b.nr_seq_mensalidade
	and	a.nr_seq_pagador	= nr_seq_pagador_pc
	-- and	a.dt_referencia between trunc(nvl(dt_inicio_comp_pc,a.dt_referencia)) and fim_dia(nvl(dt_fim_comp_pc,a.dt_referencia))  Edgar 23/05/2014, N�o filtrar por data, a filtragem por data �feita dentro da pls_obter_se_evento_inside_per
	and	b.dt_liquidacao is null;
	
begin
/* Obter dados do lote de pagamento */
select	trunc(dt_mes_competencia, 'month'),
	dt_inicio_comp,
	dt_fim_comp,
	nr_seq_periodo,
	cd_estabelecimento
into	dt_mes_competencia_w,
	dt_inicio_comp_w,
	dt_fim_comp_w,
	nr_seq_periodo_w,
	cd_estabelecimento_w
from	pls_lote_pagamento
where	nr_sequencia	= nr_seq_lote_p;

select	max(nr_sequencia)
into	nr_seq_lote_evento_w
from	pls_lote_evento
where	nr_seq_lote_pagamento	= nr_seq_lote_p;

/* Obt�m os eventos de apropria��o de mensalidade. */
for r_c01_w in C01(dt_mes_competencia_w) loop
	begin	
	vl_total_titulos_w := 0;
	
	/* Soma os t�tulos do pagador, que se enquadram no per�odo */
	for r_c02_w in C02(r_c01_w.nr_seq_pagador, dt_inicio_comp_w, dt_fim_comp_w) loop
		begin
		ie_incide_periodo_w	:= pls_obter_se_evento_inside_per(nr_seq_periodo_w, r_c01_w.nr_seq_evento, r_c01_w.nr_seq_prestador, dt_mes_competencia_w, r_c02_w.dt_referencia);
		
		if	(nvl(ie_incide_periodo_w, 'S') = 'S') then
			vl_total_titulos_w := vl_total_titulos_w + r_c02_w.vl_titulo;
		end if;
		end;
	end loop;		
	
	/* Gera o pagamento para o prestador,  com os t�tulos do pagador */
	if	(vl_total_titulos_w > 0) then	
		select	max(nr_sequencia)
		into	nr_seq_pag_prestador_w
		from	pls_pagamento_prestador
		where	nr_seq_lote		= nr_seq_lote_p
		and	nr_seq_prestador	= r_c01_w.nr_seq_prestador;
		
		/* Se n�o existir pagamento para este prestador, cria um pagamento novo */
		if	(nr_seq_pag_prestador_w is null) then
			select	pls_pagamento_prestador_seq.nextval
			into	nr_seq_pag_prestador_w
			from	dual;
			
			insert into pls_pagamento_prestador
				(nr_sequencia,
				nr_seq_lote,
				nr_seq_prestador,
				nm_usuario,
				nm_usuario_nrec,
				dt_atualizacao,
				dt_atualizacao_nrec,
				vl_pagamento)
			values	(nr_seq_pag_prestador_w,
				nr_seq_lote_p,
				r_c01_w.nr_seq_prestador,
				nm_usuario_p,
				nm_usuario_p,
				sysdate,
				sysdate,
				vl_total_titulos_w);
		end if;
		
		/* S� insere o item de pagamento, se houver pagamento para o prestador. */
		if	(nr_seq_pag_prestador_w is not null) then			
			if	(r_c01_w.ie_natureza	= 'D') then
				vl_total_titulos_w	:= vl_total_titulos_w * -1;
			end if;
			
			select	pls_pagamento_item_seq.nextval
			into	nr_seq_pag_conta_evento_w
			from	dual;
			
			insert into pls_pagamento_item
				(nr_sequencia,			nr_seq_pagamento,	nr_seq_evento,
				dt_atualizacao,			nm_usuario,		dt_atualizacao_nrec,
				nm_usuario_nrec,		vl_item)
			values(	nr_seq_pag_conta_evento_w,	nr_seq_pag_prestador_w,	r_c01_w.nr_seq_evento,
				sysdate,			nm_usuario_p,		sysdate,
				nm_usuario_p,			vl_total_titulos_w) returning nr_sequencia into nr_seq_pag_item_w;
				
			/* Vincula o item de pagamento aos t�tulos incidentes */	
			for r_c02_w in C02(r_c01_w.nr_seq_pagador, dt_inicio_comp_w, dt_fim_comp_w) loop
				begin
				ie_incide_periodo_w	:= pls_obter_se_evento_inside_per(nr_seq_periodo_w, r_c01_w.nr_seq_evento, r_c01_w.nr_seq_prestador, dt_mes_competencia_w, r_c02_w.dt_referencia);
			
				if	(nvl(ie_incide_periodo_w, 'S') = 'S') then
					update	titulo_receber	a
					set	a.nr_seq_pag_conta_evento	= nr_seq_pag_conta_evento_w
					where	a.nr_titulo			= r_c02_w.nr_titulo;
				end if;
				end;
			end loop;
			
			/* Somar o valor do item de pagamento, ao pagamento do prestador */		
			update	pls_pagamento_prestador
			set	vl_pagamento	= vl_pagamento + vl_total_titulos_w
			where	nr_sequencia	= nr_seq_pag_prestador_w;
		end if;
		
		-- Caso n�o tenha lote de evento movimento
		if	(nr_seq_lote_evento_w is null) then
			insert into pls_lote_evento
				(nr_sequencia,			nm_usuario,		dt_atualizacao,
				nm_usuario_nrec,		dt_atualizacao_nrec,	ie_origem,
				dt_inicio_comp,			dt_fim_comp,		dt_liberacao,
				dt_competencia,			cd_estabelecimento,	nr_seq_lote_pagamento,
				ds_observacao)
			values	(pls_lote_evento_seq.nextval,	nm_usuario_p,		sysdate,
				nm_usuario_p,			sysdate,		'A',
				dt_inicio_comp_w,		dt_fim_comp_w,		sysdate,
				dt_mes_competencia_w,		cd_estabelecimento_w,	nr_seq_lote_p,
				null) returning nr_sequencia into nr_seq_lote_evento_w;
		end if;

		if	(nr_seq_lote_evento_w is not null) then
			-- Evento movimento
			insert into pls_evento_movimento(
				nr_sequencia,				dt_atualizacao,		nm_usuario,
				dt_atualizacao_nrec,			nm_usuario_nrec,	dt_movimento,
				nr_seq_evento,				nr_seq_lote,		nr_seq_prestador,
				vl_movimento,				nr_seq_regra_fixo,	ds_observacao,
				nr_seq_periodo,				nr_seq_classe_tit_rec,	ie_titulo_pagar,
				cd_pf_titulo_pagar,			cd_cgc_titulo_pagar,	nr_seq_trans_fin_baixa,
				nr_seq_trans_fin_contab,		cd_centro_custo,	ie_forma_pagto,
				nr_seq_pagamento_item,			nr_seq_lote_pgto
			) values (
				pls_evento_movimento_seq.nextval,	sysdate,		nm_usuario_p,
				sysdate,				nm_usuario_p,		trunc(dt_mes_competencia_w,'dd'),
				r_c01_w.nr_seq_evento,			nr_seq_lote_evento_w,	r_c01_w.nr_seq_prestador,
				vl_total_titulos_w,			null,			'Apropria��o de mensalidade',
				nr_seq_periodo_w,			null,			null,
				null,					null,			null,
				null,					null,			'P',
				nr_seq_pag_item_w,			nr_seq_lote_p
			);
		end if;
	end if;	
	end;
end loop;

end pls_gerar_lote_pag_tit_mens;
/
