create or replace procedure gerar_d301_conteudo_ambo(	nr_seq_dataset_p	number,
                                     									nm_usuario_p		Varchar2) is 



/*

Gerar e armazenar o conte�do (com separadores) deste dataset no campo DS_CONTEUDO da tabela D301_DATASET_CONTEUDO

*/									

 cursor c01 is
  select nr_ident_conta,
         ds_dt_conta,
         nr_seq_301_indic_cobr,
         nr_seq_301_indic_cobr_ped,
         ds_dt_admissao,
         vl_cobranca_conta,
         nr_conta_banc_hosp,
         nr_referencia_hosp,
         nr_ik_cobranca
  from d301_segmento_rec  
  where	nr_seq_dataset	= nr_seq_dataset_p;
     
  c01_w c01%rowtype;
  
  cursor c02 is
     select vl_coparticipacao
     from d301_segmento_zlg
     where	nr_seq_dataset	= nr_seq_dataset_p;
 
  c02_w c02%rowtype;
  
  cursor c03 is
     select  nr_seq_301_depart,
             cd_medico_referencia,
             cd_estab_medico_ref,
             cd_medico_ref_dentista,
             cd_cid_transf_1,
             nr_seq_301_local_transf_1,
             nr_seq_301_confdiag_trans_1,
             cd_cid_transf_2,
             nr_seq_301_local_transf_2,
             nr_seq_301_confdiag_trans_2,
             cd_medico_atend,
             cd_medico_coop,
             nr_estab_medico,
             ds_dt_transf_asv,
             nr_seq_301_distrito_kv,
             ds_dt_versao_ebm
     from d301_segmento_rza
     where	nr_seq_dataset	= nr_seq_dataset_p; 
  c03_w c03%rowtype;
  
  cursor c04 is
      select cd_cid_trat_1,
             nr_seq_301_loc_trat_1,
             nr_seq_301_confdiag_trat_1,
             cd_cid_trat_2,
             nr_seq_301_loc_trat_2,
             nr_seq_301_confdiag_trat_2,
             cd_tipo_diagnostico,
             cd_membro_equipe
      from d301_segmento_bdg
      where	nr_seq_dataset	= nr_seq_dataset_p;  
  c04_w c04%rowtype;
   
  cursor c05 is 
      select cd_procedimento_ops,
             nr_seq_301_loc_proc,
             dt_procedimento,
             ie_doacao_orgao_vivo
      from d301_segmento_prz
      where	nr_seq_dataset	= nr_seq_dataset_p;
  c05_w c05%rowtype;
  
  cursor c06 is   
    select nr_seq_301_tipo_pag,
           nr_seq_301_indic_adic_ebm,
           ds_justif_cobranca,
           ie_taxa_pacote,
           ds_dt_tratamento,
           qt_ponto_ebm,
           nr_cobranca,
           vl_taxa,
           vl_ponto_ebm,
           ie_dupla_investigacao,
           cd_membro_equipe,
           nr_seq_301_nivel_equipe,
           cd_ebm_tipo_exame,
           qt_ebm_exame
    from d301_segmento_ena
    where	nr_seq_dataset	= nr_seq_dataset_p;
  c06_w c06%rowtype;
  
  cursor c07 is   
    select vl_material,
           nr_seq_301_remuneracao,
           ds_justificativa,
           qt_pagamento,
           dt_tratamento,
           vl_taxa
    from d301_segmento_ezv
    where	nr_seq_dataset	= nr_seq_dataset_p;  
  c07_w c07%rowtype;

  cursor c08 is   
    select nr_seq_301_tipo_servico,
          null,
          ds_dt_servico
    from d301_segmento_lei
    where	nr_seq_dataset	= nr_seq_dataset_p;        
  c08_w c08%rowtype;  
  
  ds_segmento_w		varchar2(4000);
  ds_segmento_clob_w	clob := empty_clob;

begin

select	'UNH+'||
	NR_REF_SEGMENTO||'+'||
	IE_DATASET||':'||
	IE_VERSAO_DATASET||':'||
	NR_RELEASE_DATASET||':'||
	NR_VERSAO_ORGANIZ||chr(39)
into	ds_segmento_w
from	d301_segmento_unh
where	nr_seq_dataset	= nr_seq_dataset_p;

select	concat(ds_segmento_clob_w,ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

/*
select	'FKT+'||
	NR_SEQ_301_IDENT_PROCESSO||'+'||
	NR_TRANSACAO||'+'||
	NR_IK_ORIGEM||'+'||
	NR_IK_DESTINO||chr(39)||chr(13)
into	ds_segmento_w
from	D301_SEGMENTO_FKT
where	nr_seq_dataset	= nr_seq_dataset_p;*/

OBTER_CONTEUDO_SEG_FKT_301( nr_seq_dataset_p, ds_segmento_w );

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

select	'INV+'||
	cd_usuario_convenio||'+'||
	obter_descricao_padrao('C301_12_STATUS_SEGURADO','IE_STATUS',NR_SEQ_301_STATUS_SEGURADO)||'+'||
	obter_descricao_padrao('C301_12_DOENCA_CRONICA','IE_DOENCA',NR_SEQ_301_DOENCA_CRONICA)||'+'||
	obter_descricao_padrao('C301_12_GRUPO_PESSOA','IE_GRUPO',NR_SEQ_301_GRUPO_PESSOA)||'+'||
	DS_MESANO_VALIDADE_CART||'+'||
	NR_INTERNO_SEGURADO||'+'||
	NR_EPISODIO_CONVENIO||'+'||
	NR_ARQUIVO_CONVENIO||'+'||
	DS_DT_INICIO_COBERT_CONV||'+'||
	NR_CONTRATO_CONV||chr(39)
into	ds_segmento_w
from	d301_segmento_inv
where	nr_seq_dataset	= nr_seq_dataset_p;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

select	'NAD+'||
	nm_sobrenome||'+'||
	ds_nome||'+'||
	obter_descricao_padrao('C301_21_SEXO','IE_SEXO',nr_seq_301_sexo)||'+'||
	ds_dt_nascimento||'+'||
	ds_rua_numero||'+'||
	cd_postal||'+'||
	ds_cidade||'+'||
	ds_titulacao||'+'||
	obter_descricao_padrao('C301_7_PAIS','IE_PAIS',nr_seq_301_pais)||'+'||
	ds_sufixo_nome||'+'||
	ds_prefixo_sobrenome||'+'||
	ds_compl_endereco||chr(39)
into	ds_segmento_w
from	d301_segmento_nad
where	nr_seq_dataset	= nr_seq_dataset_p;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

/*select 'CUX+' ||
        nr_seq_301_moeda
into ds_segmento_w
from d301_segmento_cux
where nr_seq_dataset	= nr_seq_dataset_p;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;*/

select 'DPV+'||
  cd_versao_cid||'+'||
  cd_versao_ops||'+'||chr(39)
into	ds_segmento_w
from d301_segmento_dpv
where	nr_seq_dataset	= nr_seq_dataset_p;
  
select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

--segmentos
open c01;
fetch c01 into c01_w;
loop
  ds_segmento_w :='REC+'||
                   c01_w.nr_ident_conta||'+'||
                   c01_w.ds_dt_conta||'+'||
                   c01_w.nr_seq_301_indic_cobr||'+'||
                   c01_w.nr_seq_301_indic_cobr_ped||'+'||
                   c01_w.ds_dt_admissao||'+'||
                   c01_w.vl_cobranca_conta||'+'||
                   c01_w.nr_conta_banc_hosp||'+'||
                   c01_w.nr_referencia_hosp||'+'||
                   c01_w.nr_ik_cobranca||'+'||chr(39);
    
fetch c01 into c01_w;
exit when c01%notfound;     
end loop;
close c01;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

open c02;
fetch c02 into c02_w;
loop
  ds_segmento_w := 'ZLG+'||
                  c02_w.vl_coparticipacao||'+'||chr(39);
        
fetch c02 into c02_w;
exit when c02%notfound;     
end loop;
close c02;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;

open c03;
fetch c03 into c03_w;
loop
   ds_segmento_w:=  'RZA+'||
                    c03_w.nr_seq_301_depart||'+'||
                    c03_w.cd_medico_referencia||'+'||
                    c03_w.cd_estab_medico_ref||'+'||
                    c03_w.cd_medico_ref_dentista||'+'||
                    c03_w.cd_cid_transf_1||c03_w.nr_seq_301_local_transf_1||c03_w.nr_seq_301_confdiag_trans_1||'+'||
                    c03_w.cd_cid_transf_2||c03_w.nr_seq_301_local_transf_2||c03_w.nr_seq_301_confdiag_trans_2||'+'||
                    c03_w.cd_medico_atend||'+'||
                    c03_w.cd_medico_coop||'+'||
                    c03_w.nr_estab_medico||'+'||
                    c03_w.ds_dt_transf_asv||'+'||
                    c03_w.nr_seq_301_distrito_kv||'+'||
                    c03_w.ds_dt_versao_ebm||'+'||chr(39);

fetch c03 into c03_w;
exit when c03%notfound;     
end loop;
close c03;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;      

open c04;
fetch c04 into c04_w;
loop
  ds_segmento_w:= 'BDG+'||
                   c04_w.cd_cid_trat_1||c04_w.nr_seq_301_loc_trat_1||c04_w.nr_seq_301_confdiag_trat_1||'+'||
                   c04_w.cd_cid_trat_2||c04_w.nr_seq_301_loc_trat_2||c04_w.nr_seq_301_confdiag_trat_2||'+'||
                   c04_w.cd_tipo_diagnostico||'+'||
                   c04_w.cd_membro_equipe||'+'||chr(39);
fetch c04 into c04_w;
exit when c04%notfound;     
end loop;
close c04;
 
select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;    

open c05;
fetch c05 into c05_w;
loop
  ds_segmento_w:='PRZ+'||
                 c05_w.cd_procedimento_ops||c05_w.nr_seq_301_loc_proc||'+'||
                 c05_w.dt_procedimento||'+'||
                 c05_w.ie_doacao_orgao_vivo||'+'||chr(39);

fetch c05 into c05_w;
exit when c05%notfound;     
end loop;
close c05;

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;  

open c06;
fetch c06 into c06_w;
loop
 ds_segmento_w:='ENA+'||
                c06_w.nr_seq_301_tipo_pag||'+'||
                c06_w.nr_seq_301_indic_adic_ebm||'+'||
                c06_w.ds_justif_cobranca||'+'||
                c06_w.ie_taxa_pacote||'+'||
                c06_w.ds_dt_tratamento||'+'||
                c06_w.qt_ponto_ebm||'+'||
                c06_w.nr_cobranca||'+'||
                c06_w.vl_taxa||'+'||
                c06_w.vl_ponto_ebm||'+'||
                c06_w.ie_dupla_investigacao||'+'||
                c06_w.cd_membro_equipe||'+'||
                c06_w.nr_seq_301_nivel_equipe||'+'||
                c06_w.cd_ebm_tipo_exame||'+'||
                c06_w.qt_ebm_exame||'+'||chr(39);

fetch c06 into c06_w;
exit when c06%notfound;     
end loop;
close c06;          

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual;


open c07;
fetch c07 into c07_w;
loop
 ds_segmento_w:= 'EZV+'||
                 c07_w.vl_material||'+'||
                 c07_w.nr_seq_301_remuneracao||'+'||
                 c07_w.ds_justificativa||'+'||
                 c07_w.qt_pagamento||'+'||
                 c07_w.dt_tratamento||'+'||
                 c07_w.vl_taxa||'+'||chr(39);

fetch c07 into c07_w;
exit when c07%notfound;     
end loop;
close c07;          

select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual; 


open c08;
fetch c08 into c08_w;
loop
   ds_segmento_w:= 'LEI+'||
                   c08_w.nr_seq_301_tipo_servico||'+'||
                   null||'+'||
                   c08_w.ds_dt_servico||'+'||chr(39);
fetch c08 into c08_w;
exit when c08%notfound;     
end loop;
close c08;
    
select	concat(ds_segmento_clob_w, ds_segmento_w)
into	ds_segmento_clob_w
from	dual; 

  --rodape
select  'UNT+'||
  qt_segmento||'+'||
  lpad(nr_ref_segmento,5,'0')||chr(39)
into  ds_segmento_w
from  d301_segmento_unt
where  nr_seq_dataset  = nr_seq_dataset_p;

select  concat(ds_segmento_clob_w, ds_segmento_w)
into  ds_segmento_clob_w
from  dual;


inserir_d301_dataset_conteudo(nm_usuario_p, nr_seq_dataset_p, ds_segmento_clob_w);



end gerar_d301_conteudo_ambo;
/