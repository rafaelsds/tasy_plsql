create or replace
procedure duplicar_sinais_vitais_cir(	nr_sequencia_p		number,
					dt_data_hora_p		date,
					nm_usuario_p		varchar2) is 
					
					
begin

insert into atendimento_sinal_vital (	
	nr_sequencia,
	cd_pessoa_fisica,
	nr_cirurgia,
	dt_sinal_vital,
	nr_atendimento,
	ie_ritmo_ecg,
	qt_segmento_st,
	qt_freq_cardiaca,
	ie_aparelho_pa,
	qt_pa_sistolica,
	qt_pam,
	qt_freq_resp,
	qt_temp,
	ie_sitio,
	qt_saturacao_o2,
	qt_pa_diastolica,
	qt_pvc,
	qt_pae,
	qt_maec,
	qt_pressao_intra_cranio,
	ie_pressao,
	qt_bcf,
	ds_observacao,
	nm_usuario,
	dt_atualizacao,
	ie_situacao,
	nr_seq_pepo,
	cd_escala_dor,
	qt_escala_dor,
	nr_seq_topografia_dor,
	ie_lado,
	ie_unid_med_peso,
	ie_unid_med_altura)
select	atendimento_sinal_vital_seq.NextVal,
	obter_pessoa_fisica_usuario(nm_usuario_p, 'C'),
	nr_cirurgia,
	dt_data_hora_p,
	nr_atendimento,
	ie_ritmo_ecg,
	qt_segmento_st,
	qt_freq_cardiaca,
	ie_aparelho_pa,
	qt_pa_sistolica,
	qt_pam,
	qt_freq_resp,
	qt_temp,
	ie_sitio,
	qt_saturacao_o2,
	qt_pa_diastolica,
	qt_pvc,
	qt_pae,
	qt_maec,
	qt_pressao_intra_cranio,
	ie_pressao,
	qt_bcf,
	ds_observacao,
	nm_usuario_p,
	sysdate,
	'A',
	nr_seq_pepo,
	cd_escala_dor,
	qt_escala_dor,
	nr_seq_topografia_dor,
	ie_lado,
	ie_unid_med_peso,
	ie_unid_med_altura
from	atendimento_sinal_vital
where	nr_sequencia = nr_sequencia_p;		


commit;

end duplicar_sinais_vitais_cir;
/