create or replace
procedure pls_atualizar_intercambio_guia
			(	nr_seq_guia_p		number,
				nm_usuario_p		Varchar2) is 
				
nr_seq_segurado_w		number(10);
ie_tipo_segurado_w		varchar2(2);
nr_seq_uni_exec_w		number(10);
ie_tipo_intercambio_w		varchar2(1);

begin

begin
	select	nr_seq_segurado,
		nr_seq_uni_exec
	into	nr_seq_segurado_w,
		nr_seq_uni_exec_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_p;
exception
when others then
	nr_seq_segurado_w := '';
end;

begin
	select	ie_tipo_segurado
	into	ie_tipo_segurado_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_w;
exception
when others then
	ie_tipo_segurado_w := '';
end;

if	(nr_seq_uni_exec_w is not null) then
	ie_tipo_intercambio_w := 'I';
elsif	(ie_tipo_segurado_w not in ('A','B','P')) then
	ie_tipo_intercambio_w := 'E';
end if;

if	(ie_tipo_intercambio_w is not null) then
	update	pls_guia_plano
	set	ie_tipo_intercambio	= ie_tipo_intercambio_w
	where	nr_sequencia		= nr_seq_guia_p;
end if;

commit;

end pls_atualizar_intercambio_guia;
/