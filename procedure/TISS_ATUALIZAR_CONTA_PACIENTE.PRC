create or replace
procedure TISS_ATUALIZAR_CONTA_PACIENTE
		(nr_interno_conta_p			in	varchar2,
		nr_seq_med_fatur_p		in	varchar2,
		ie_atualizar_todos_p		in	varchar2,
		cd_estabelecimento_p		in	varchar2,
		nm_usuario_p			in	varchar2,
		nr_atend_med_p			in	number,
		nr_seq_prot_med_p			in	number) is

cd_motivo_alta_w			number(15,0);
dt_alta_w				date;
ie_tipo_atendimento_w		varchar(20);
ie_clinica_w			varchar(20);
nr_atendimento_w			number(15,0);
ie_tipo_atend_tiss_w		varchar(20);
ie_tipo_atend_tiss_conta_w		varchar(20);
ie_tipo_atend_tiss_regra_w		varchar2(20);
ie_data_entrada_alta_w		varchar2(20);
ie_status_acerto_w			varchar(20);
cd_setor_entrada_w		number(5);
cd_convenio_w			number(5);
nr_seq_tipo_atend_w		number(10);
cd_area_procedimento_w		number(15);
cd_area_procedimento_ww		number(15);
cd_especialidade_w		number(15);
cd_especialidade_ww		number(15);
cd_grupo_proc_w			number(15);
cd_grupo_proc_ww			number(15);
cd_procedimento_w		number(15);
nr_atend_original_w		number(15);
cd_procedimento_ww		number(15);
cd_tipo_procedimento_w		number(3);
cd_tipo_procedimento_ww		number(3);
ie_origem_proced_w		number(10);
ie_origem_proced_ww		number(10);
nr_seq_classif_atend_w		number(10);
ie_tipo_consulta_tiss_w		number(10);
nr_seq_saida_int_w		number(10);
dt_periodo_inicial_w		date;
dt_periodo_final_w			date;
ie_atualiza_tipo_atend_w	varchar2(20);
nr_seq_saida_spsadt_w		number(10);
nr_seq_saida_consulta_w		number(10);
ie_tipo_atend_conta_w		varchar(20);
ie_att_tipo_atend_conta_w	varchar2(10);
nr_interno_conta_w		number(10);
ie_tipo_fatur_periodo_w		varchar2(10);
nr_seq_queixa_w			number(10);
count_param_w			number(10,0);
cd_procedencia_w		number(5);
nr_interno_conta_total_w	number(10);
ie_tipo_fat_tiss_regra_w    tiss_regra_tipo_fatur.ie_tipo_fat_tiss_regra%type;
ie_atualizar_final_w        tiss_regra_tipo_fatur.ie_atualizar_final%type;

cursor c01 is
select	nvl(ie_tipo_atend_tiss,'0')
from	tiss_tipo_atendimento
where	cd_estabelecimento							= cd_estabelecimento_p
and	nvl(ie_tipo_atendimento,nvl(ie_tipo_atendimento_w,0))			= nvl(ie_tipo_atendimento_w,0)
and	nvl(ie_clinica, ie_clinica_w)						= ie_clinica_w
and	nvl(cd_setor_entrada, cd_setor_entrada_w)				= cd_setor_entrada_w
and	nvl(cd_convenio, cd_convenio_w)						= cd_convenio_w
and	nvl(ie_tipo_atend_conta, nvl(ie_tipo_atend_conta_w,'0')) 		= nvl(ie_tipo_atend_conta_w,'0')
and	nvl(nr_seq_classif_atend, nvl(nr_seq_classif_atend_w,0)) 		= nvl(nr_seq_classif_atend_w,0)
and	nvl(nr_seq_queixa, nvl(nr_seq_queixa_w,0))				= nvl(nr_seq_queixa_w,0)
and	nvl(cd_procedencia, nvl(cd_procedencia_w,0))				= nvl(cd_procedencia_w,0)
and	nvl(TISS_OBTER_SE_TIPO_ATEND(nr_sequencia, nr_interno_conta_p), 'S')	= 'S'
order by nvl(cd_convenio,0),
	nvl(ie_tipo_atendimento,0),
	nvl(ie_clinica,0),
	nvl(cd_setor_entrada,0),	
	nvl(ie_tipo_atend_conta, '0'),
	nvl(nr_seq_classif_atend,0),
	nvl(TISS_OBTER_SE_TIPO_ATEND(nr_sequencia, nr_interno_conta_p), 'A'), -- Edgar 25/06/2009, OS 133370, ordenar pelas regras que tem regra de procedimento
	nvl(nr_seq_queixa,0),
	nvl(cd_procedencia,0);	

cursor c02 is
select	nvl(IE_TIPO_CONSULTA_TISS,0)
from	tiss_regra_tipo_consulta
where	cd_estabelecimento					= cd_estabelecimento_p
and	nvl(nr_seq_classif_atend, nr_seq_classif_atend_w)	= nr_seq_classif_atend_w
and 	nvl(cd_convenio, cd_convenio_w)				= cd_convenio_w
and	((nvl(ie_atend_retorno, 'T') = 'T') or
	 ((nr_atend_original_w is null) and (nvl(ie_atend_retorno, 'T') = 'N')) or
	 ((nr_atend_original_w is not null) and (nvl(ie_atend_retorno, 'T') = 'R')))
order 	by nvl(cd_convenio,0),
	nvl(nr_seq_classif_atend, 0);
	
cursor c03 is
select	ie_regra, ie_tipo_fat_tiss_regra, ie_atualizar_final
from	tiss_regra_tipo_fatur
where	cd_estabelecimento					= cd_estabelecimento_p
and	nvl(cd_convenio, nvl(cd_convenio_w,0))			= nvl(cd_convenio_w,0)
and	nvl(ie_tipo_atendimento, nvl(ie_tipo_atendimento_w,0))	= nvl(ie_tipo_atendimento_w,0)
and	nvl(ie_tipo_atend_conta, nvl(ie_tipo_atend_conta_w,0))	= nvl(ie_tipo_atend_conta_w,0)
order by nvl(ie_tipo_atend_conta, 0),
	nvl(ie_tipo_atendimento, 0),
	nvl(cd_convenio, 0);
	
begin

select	max(ie_status_acerto),
	max(ie_tipo_atend_conta)
into	ie_status_acerto_w,
	ie_tipo_atend_conta_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

if	(nvl(nr_interno_conta_p,0) > 0) or
	((nvl(nr_atend_med_p,0) > 0) and (nvl(nr_seq_prot_med_p,0) > 0)) then
	begin

	select	b.cd_motivo_alta,
		b.dt_alta,
		b.ie_tipo_atendimento,
		nvl(b.ie_clinica,'0'),
		b.nr_atendimento,
		b.IE_TIPO_ATEND_TISS,
		a.cd_convenio_parametro,
		b.nr_seq_classificacao,
		a.dt_periodo_inicial,
		a.dt_periodo_final,
		b.nr_atend_original,
		b.nr_seq_queixa,
		b.cd_procedencia
	into	cd_motivo_alta_w,
		dt_alta_w,
		ie_tipo_atendimento_w,
		ie_clinica_w,
		nr_atendimento_w,
		IE_TIPO_ATEND_TISS_w,
		cd_convenio_w,
		nr_seq_classif_atend_w,
		dt_periodo_inicial_w,
		dt_periodo_final_w,
		nr_atend_original_w,
		nr_seq_queixa_w,
		cd_procedencia_w
	from	atendimento_paciente b,
		conta_paciente a
	where	a.nr_atendimento	= b.nr_atendimento
	and	a.nr_interno_conta	= nr_interno_conta_p;
	exception
	when others then
		cd_motivo_alta_w		:= null;
		dt_alta_w			:= null;
		ie_tipo_atendimento_w	:= null;
		ie_clinica_w		:= null;
		ie_tipo_atend_tiss_w	:= null;
		cd_procedencia_w	:= null;
	end;
	
	if	(nvl(nr_seq_prot_med_p,0) > 0) then
		
		select	max(cd_convenio)
		into 	cd_convenio_w
		from 	med_prot_convenio
		where 	nr_sequencia = nr_seq_prot_med_p;
		
	end if;

	select	count(*),
		nvl(max(ie_data_entrada_alta), 'N'),
		max(nr_seq_saida_int),
		max(nr_seq_saida_spsadt),
		max(nr_seq_saida_consulta),
		nvl(max(ie_atual_conta), 'N'),
		nvl(max(ie_alt_tipo_atend_conta), 'N'),
		nvl(max(ie_tipo_fatur_periodo),'N')
	into	count_param_w,
		ie_data_entrada_alta_w,
		nr_seq_saida_int_w,
		nr_seq_saida_spsadt_w,
		nr_seq_saida_consulta_w,
		ie_atualiza_tipo_atend_w,
		ie_att_tipo_atend_conta_w,
		ie_tipo_fatur_periodo_w
	from	tiss_parametros_convenio
	where	cd_convenio		= cd_convenio_w
	and	cd_estabelecimento	= cd_estabelecimento_p;

	
	if	(count_param_w > 0) then
	
		begin
		cd_setor_entrada_w	:= obter_setor_atepacu(obter_atepacu_paciente(nr_atendimento_w, 'P'),0);
		exception
		when others then
			cd_setor_entrada_w	:= null;
		end;

		if	(nvl(cd_motivo_alta_w,0) > 0) then
			tiss_atualizar_alta(	nr_atendimento_w,
						nr_interno_conta_p,
						cd_motivo_alta_w,
						dt_alta_w,
						nm_usuario_p);
		end if;

		if	(nvl(nr_atendimento_w,'0') > 0)  then

			if	(nvl(IE_TIPO_ATEND_TISS_w,'0') = '0') or
				(ie_atualiza_tipo_atend_w in ('S','A')) then

				open c01;
				loop
				fetch c01 into
					ie_tipo_atend_tiss_regra_w;
				exit when c01%notfound;

					ie_tipo_atend_tiss_w	:= ie_tipo_atend_tiss_regra_w;

				end loop;
				close c01;

			end if;

			open c02;
			loop
			fetch c02 into
				ie_tipo_consulta_tiss_w;
			exit when c02%notfound;
			end loop;
			close c02;

			if	(nvl(ie_tipo_consulta_tiss_w,0) > 0) then
				update	conta_paciente
				set	ie_tipo_consulta_tiss	= nvl(ie_tipo_consulta_tiss, ie_tipo_consulta_tiss_w)
				where	nr_interno_conta		= nr_interno_conta_p;
			end if;

			select	max(ie_tipo_atend_tiss)
			into	ie_tipo_atend_tiss_conta_w
			from	conta_paciente
			where	nr_interno_conta	= nr_interno_conta_p;

			if	(nvl(ie_tipo_atend_tiss_w,'0') <> '0') and
				((nvl(ie_tipo_atend_tiss_conta_w,'0') = '0')) then
				if	(ie_atualizar_todos_p = 'S') then
					update	conta_paciente
					set	ie_tipo_atend_tiss	= ie_tipo_atend_tiss_w
					where	nr_interno_conta	= nr_interno_conta_p;
				else
					update	conta_paciente
					set	ie_tipo_atend_tiss	= nvl(ie_tipo_atend_tiss, ie_tipo_atend_tiss_w)
					where	nr_interno_conta	= nr_interno_conta_p;
				end if;
			end if;
			if	(nvl(ie_tipo_atend_tiss_w,'0') <> '0') then
				if	(ie_atualiza_tipo_atend_w = 'S') then --lhalves OS249864 em 21/09/2010 - Atualizar apenas na conta
					update	conta_paciente
					set	ie_tipo_atend_tiss	= ie_tipo_atend_tiss_w
					where	nr_interno_conta	= nr_interno_conta_p;		
				elsif	(ie_atualiza_tipo_atend_w = 'A') then -- Atualizar na conta e no atendimento
					update	conta_paciente
					set	ie_tipo_atend_tiss	= ie_tipo_atend_tiss_w
					where	nr_interno_conta	= nr_interno_conta_p;	
					
					update 	atendimento_paciente
					set 	ie_tipo_atend_tiss	= nvl(ie_tipo_atend_tiss_w,ie_tipo_atend_tiss)
					where 	nr_atendimento		= nr_atendimento_w;
				end if;				
			end if;

		end if;

		if	(nvl(ie_data_entrada_alta_w, 'N') = 'S') then
			update	conta_paciente
			set	dt_entrada_tiss		= nvl(dt_periodo_inicial_w,dt_entrada_tiss),
				dt_alta_tiss		= nvl(dt_periodo_final_w,dt_alta_tiss)
			where	nr_interno_conta	= nr_interno_conta_p;
		end if;	

		update	conta_paciente
		set	dt_geracao_tiss		= sysdate,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			nr_seq_saida_int	= decode(ie_tipo_fatur_tiss, 'P', nvl(nr_seq_saida_int_w, nr_seq_saida_int), nr_seq_saida_int),
			nr_seq_saida_spsadt	= decode(ie_tipo_fatur_tiss, 'P', nvl(nr_seq_saida_spsadt_w, nr_seq_saida_spsadt), nr_seq_saida_spsadt),
			nr_seq_saida_consulta	= decode(ie_tipo_fatur_tiss, 'P', nvl(nr_seq_saida_consulta_w, nr_seq_saida_consulta), nr_seq_saida_consulta),
			ie_tipo_atend_conta	= decode(ie_att_tipo_atend_conta_w, 'S', nvl(ie_tipo_atendimento_w, ie_tipo_atend_conta), ie_tipo_atend_conta)
		where	nr_interno_conta	= nr_interno_conta_p;
		
		if	(nvl(ie_tipo_fatur_periodo_w,'N') = 'R') then --Conforme regra Shift + F11
		
			open C03;
			loop
			fetch C03 into	
				ie_tipo_fatur_periodo_w, ie_tipo_fat_tiss_regra_w, ie_atualizar_final_w;
			exit when C03%notfound;	
				ie_tipo_fatur_periodo_w	:= ie_tipo_fatur_periodo_w;
				ie_tipo_fat_tiss_regra_w := ie_tipo_fat_tiss_regra_w;
				ie_atualizar_final_w 	:= ie_atualizar_final_w;
			end loop;
			close C03;
		end if;
		
		if	(nvl(ie_tipo_fatur_periodo_w,'N') = 'S') then /*lhalves OS344143 em 26/07/2011*/
		
		
			/*1013462 Foi apresentada a necessidade de atualizar o tipo de faturamento como complementar
			quando existir uma conta faturada TOTAL com o mesmo periodo*/
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_total_w
			from 	conta_paciente a
			where	a.nr_atendimento	= nr_atendimento_w
			and	a.cd_convenio_parametro	= cd_convenio_w
			--and	a.IE_TIPO_FATUR_TISS	= 'T'
			and	a.dt_periodo_final 	= dt_periodo_final_w
			and	a.dt_periodo_inicial 	= dt_periodo_inicial_w
			and	a.nr_interno_conta	< nr_interno_conta_p;
			

			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from 	conta_paciente a
			where	a.nr_atendimento	= nr_atendimento_w
			and	a.cd_convenio_parametro	= cd_convenio_w
			and	not exists (	select	1
						from 	conta_paciente x
						where	x.nr_atendimento	= a.nr_atendimento
						and	x.cd_convenio_parametro	= a.cd_convenio_parametro
						and	a.dt_periodo_final 	= dt_periodo_final_w
						and	a.dt_periodo_inicial 	= dt_periodo_inicial_w
						and	a.nr_interno_conta	>nr_interno_conta_p)
			and	a.dt_periodo_final 	= (	select	max(x.dt_periodo_final)
								from	conta_paciente x
								where	x.nr_atendimento	= a.nr_atendimento
								and	x.cd_convenio_parametro	= a.cd_convenio_parametro
								and	x.cd_estabelecimento	= a.cd_estabelecimento);
			/*begin						
			select 	nr_interno_conta
			into	nr_interno_conta_w
			from	(select	a.nr_interno_conta
				from	conta_paciente a
				where 	a.nr_atendimento 	= nr_atendimento_w
				and 	a.cd_convenio_parametro = cd_convenio_w
				and 	a.dt_periodo_final  =  (select 	max(x.dt_periodo_final)
								from 	conta_paciente x
								where 	x.nr_atendimento = a.nr_atendimento
								and 	x.cd_convenio_parametro = a.cd_convenio_parametro
								and 	x.cd_estabelecimento = a.cd_estabelecimento)
				order by a.ie_status_acerto desc,
					a.nr_interno_conta asc)
			where  rownum = 1;
			exception
			when others then
				nr_interno_conta_w := null;
			end;*/
			
			if	(nr_interno_conta_total_w is not null) then
				update	conta_paciente
				set	ie_tipo_fatur_tiss 	= 'C'
				where	nr_interno_conta	= nr_interno_conta_p;
			elsif	(nr_interno_conta_w = nr_interno_conta_p) then
			
				if(ie_atualizar_final_w = 'S') then
                    begin
                    select 'F' into ie_tipo_fat_tiss_regra_w
                      from conta_paciente c
                     where c.nr_atendimento = nr_atendimento_w 
                       and c.cd_convenio_parametro	= cd_convenio_w
                       and c.ie_tipo_fatur_tiss = 'P'
                       and c.nr_interno_conta <> nr_interno_conta_w 
		       and c.ie_cancelamento is null
                       and rownum = 1;
                    exception
                        when no_data_found then
                            ie_tipo_fat_tiss_regra_w := null;
                    end;
                end if;
				
				update	conta_paciente
				set	ie_tipo_fatur_tiss 	= nvl(ie_tipo_fat_tiss_regra_w,'T')  ---- 'T' Se ele tem regra cadastrada vai colocar o valor da regra, senao mantem o padrao do sistema
				where	nr_interno_conta	= nr_interno_conta_p;
			else
				update	conta_paciente
				set	ie_tipo_fatur_tiss 	= 'P'
				where	nr_interno_conta	= nr_interno_conta_p;
				
				update	conta_paciente
				set	dt_geracao_tiss		= sysdate,
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate,
					nr_seq_saida_int		= decode(ie_tipo_fatur_tiss, 'P', nvl(nr_seq_saida_int_w, nr_seq_saida_int), nr_seq_saida_int),
					nr_seq_saida_spsadt	= decode(ie_tipo_fatur_tiss, 'P', nvl(nr_seq_saida_spsadt_w, nr_seq_saida_spsadt), nr_seq_saida_spsadt),
					nr_seq_saida_consulta	= decode(ie_tipo_fatur_tiss, 'P', nvl(nr_seq_saida_consulta_w, nr_seq_saida_consulta), nr_seq_saida_consulta)
				where	nr_interno_conta		= nr_interno_conta_p;
		
			end if;
		end if;	

		TISS_ATUALIZAR_DESPESAS(nr_interno_conta_p, nm_usuario_p);

		tiss_gerar_conta(nr_interno_conta_p, nr_seq_med_fatur_p, cd_estabelecimento_p, nm_usuario_p, nr_atend_med_p, nr_seq_prot_med_p);

		if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
		
	end if;
end if;

end TISS_ATUALIZAR_CONTA_PACIENTE;
/
