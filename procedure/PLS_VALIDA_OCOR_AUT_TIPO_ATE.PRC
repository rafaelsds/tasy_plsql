create or replace
procedure pls_valida_ocor_aut_tipo_ate
					(	nr_seq_ocor_combinada_p		Number,
					nr_seq_ocorrencia_p		Number,
					nr_seq_motivo_glosa_p		Number,
					nr_seq_guia_p			Number,
					nr_seq_requisicao_p		Number,
					nr_seq_execucao_p		Number,
					ie_utiliza_filtro_p		Varchar2,
					nm_usuario_p			Varchar2,
					cd_estabelecimento_p		Number) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a valida��o 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_prestador_w		Number(10);
nr_seq_segurado_w		Number(10);
nr_seq_requisicao_w		Number(10);
nr_seq_regra_w			Number(10);
ie_regra_w			Varchar2(2);
ie_tipo_item_w			Number(15);
ie_tipo_ocorrencia_w		Varchar2(2);
ie_gerar_ocorrencia_w		Varchar2(2)	:= 'N';
ie_tipo_atendimento_w		pls_requisicao.ie_tipo_atendimento%type;
ie_tipo_guia_w			pls_requisicao.ie_tipo_guia%type;
ie_gerar_ocor_atend_w		Varchar2(2);
ie_gerou_ocor_cabecalho_w	Varchar2(2);
nr_seq_oc_benef_w		Number(10);
nr_seq_lote_exec_w		pls_execucao_requisicao.nr_seq_lote_exec%type;

Cursor C01 is
	select	ie_exige_tipo_atend
	from	pls_validacao_aut_tipo_ate
	where	nr_seq_ocor_aut_combinada	= nr_seq_ocor_combinada_p
	and	ie_situacao			= 'A';

Cursor C02 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p;

Cursor C03 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_guia_plano_mat
	where	nr_seq_guia		= nr_seq_guia_p;

Cursor C04 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C05 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C06 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_execucao_req_item
	where	nr_seq_execucao		= nr_seq_execucao_p
	and	cd_procedimento		is not null
	and	ie_origem_proced	is not null;

Cursor C07 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_execucao_req_item
	where	nr_seq_execucao		= nr_seq_execucao_p
	and	nr_seq_material		is not null;

begin
	for r_C01_w in C01 loop
		begin
			if	(nr_seq_guia_p is not null) then
				begin
					select	nr_seq_prestador,
						nr_seq_segurado,
						ie_tipo_atend_tiss,
						ie_tipo_guia
					into	nr_seq_prestador_w,
						nr_seq_segurado_w,
						ie_tipo_atendimento_w,
						ie_tipo_guia_w
					from	pls_guia_plano
					where	nr_sequencia	= nr_seq_guia_p;
				exception
				when others then
					nr_seq_prestador_w	:= null;
					nr_seq_segurado_w	:= null;
					ie_tipo_atendimento_w	:= null;
					ie_tipo_guia_w		:= null;
				end;

				ie_gerar_ocor_atend_w := 'S';

				if	(nvl(r_C01_w.ie_exige_tipo_atend, 'N') = 'S') and (ie_tipo_atendimento_w is null) and (ie_tipo_guia_w = '2') then			
					ie_gerar_ocor_atend_w := 'S';
				else 
					ie_gerar_ocor_atend_w := 'N';
				end if;

				for r_C02_w in C02 loop
					begin
						ie_gerar_ocorrencia_w	:= 'S';	

						if	(ie_utiliza_filtro_p	= 'S') then
							/* Tratamento para filtros */
							pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, null,
											null, r_C02_w.nr_sequencia, null,
											null, null, null,
											r_C02_w.cd_procedimento, r_C02_w.ie_origem_proced, null,
											null,nr_seq_prestador_w, null,
											null, null, null,
											nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

							if	(ie_regra_w	= 'S') then
								ie_gerar_ocorrencia_w	:= 'S';
							elsif	(ie_regra_w	in ('E','N')) then
								ie_gerar_ocorrencia_w	:= 'N';
							end if;							
						end if;

						if	(ie_gerar_ocorrencia_w	= 'S') then
						
							ie_gerar_ocorrencia_w	:= ie_gerar_ocor_atend_w;		

							if	(ie_gerar_ocorrencia_w	= 'S') then	
						
								pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, null,
											nr_seq_guia_p, null, null,
											null, nr_seq_ocor_combinada_p, nm_usuario_p,
											null, nr_seq_motivo_glosa_p, 7,
											cd_estabelecimento_p, 'N' ,null,
											nr_seq_oc_benef_w, null,
											null, null, null);

								pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
												nr_seq_guia_p, null, null,
												r_C02_w.nr_sequencia, null, null,
												null, null, null,
												nm_usuario_p, cd_estabelecimento_p);
							end if;
						end if;
					end;
				end loop;
				for r_C03_w in C03 loop
					begin
						ie_gerar_ocorrencia_w	:= 'S';

						if	(ie_utiliza_filtro_p	= 'S') then
						/* Tratamento para filtros */

							pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, null,
											null, null, r_C03_w.nr_sequencia,
											null, null, null,
											null, null, r_C03_w.nr_seq_material,
											null,nr_seq_prestador_w, nr_seq_ocorrencia_p,
											null, null, null,
											nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);


							if	(ie_regra_w	= 'S') then
								ie_gerar_ocorrencia_w	:= 'S';
							elsif	(ie_regra_w	in ('E','N')) then
								ie_gerar_ocorrencia_w	:= 'N';
							end if;
						end if;

						if	(ie_gerar_ocorrencia_w	= 'S') then

							ie_gerar_ocorrencia_w	:= ie_gerar_ocor_atend_w;

							if	(ie_gerar_ocorrencia_w	= 'S') then
								pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, null,
											nr_seq_guia_p, null, null,
											null, nr_seq_ocor_combinada_p, nm_usuario_p,
											null, nr_seq_motivo_glosa_p, 7,
											cd_estabelecimento_p, 'N' ,null,
											nr_seq_oc_benef_w, null,
											null, null, null);

								pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
												nr_seq_guia_p, null, null,
												null, r_C03_w.nr_sequencia, null,
												null, null, null,
												nm_usuario_p, cd_estabelecimento_p);

							end if;
						end if;
					end;
				end loop;
			elsif	(nr_seq_requisicao_p is not null) then
				begin
					select	nr_seq_prestador,
						nr_seq_segurado,
						ie_tipo_atendimento,
						ie_tipo_guia
					into	nr_seq_prestador_w,
						nr_seq_segurado_w,
						ie_tipo_atendimento_w,
						ie_tipo_guia_w
					from	pls_requisicao
					where	nr_sequencia	= nr_seq_requisicao_p;
					exception
					when others then
						nr_seq_prestador_w	:= null;
						nr_seq_segurado_w	:= null;
						ie_tipo_atendimento_w	:= null;
				end;

				ie_gerar_ocor_atend_w := 'S';

				if	(nvl(r_C01_w.ie_exige_tipo_atend, 'N') = 'S') and (ie_tipo_atendimento_w is null) and (ie_tipo_guia_w = '2') then			
					ie_gerar_ocor_atend_w := 'S';
				else 
					ie_gerar_ocor_atend_w := 'N';
				end if;

				for r_C04_w in C04 loop
					begin
						ie_gerar_ocorrencia_w	:= 'S';	

						if	(ie_utiliza_filtro_p	= 'S') then
						/* Tratamento para filtros */
							pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, nr_seq_requisicao_p,
											null, null, null,
											r_C04_w.nr_sequencia, null, null,
											r_C04_w.cd_procedimento, r_C04_w.ie_origem_proced, null,
											null,nr_seq_prestador_w, nr_seq_ocorrencia_p,
											null, null, null,
											nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

							if	(ie_regra_w	= 'S') then
								ie_gerar_ocorrencia_w	:= 'S';
							elsif	(ie_regra_w	in ('E','N')) then
								ie_gerar_ocorrencia_w	:= 'N';
							end if;							
						end if;

						if	(ie_gerar_ocorrencia_w	= 'S') then

							ie_gerar_ocorrencia_w	:= ie_gerar_ocor_atend_w;		

							if	(ie_gerar_ocorrencia_w	= 'S') then							
								pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
											null, null, null, 
											null, nr_seq_ocor_combinada_p, nm_usuario_p, 
											null, nr_seq_motivo_glosa_p, 9, 
											cd_estabelecimento_p, 'N' ,null,
											nr_seq_oc_benef_w, null,
											null, null, null);

								pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
												null, nr_seq_requisicao_p, null,
												null, null, r_C04_w.nr_sequencia,
												null, null, null,
												nm_usuario_p, cd_estabelecimento_p);
							end if;
						end if;
					end;
				end loop;
				for r_C05_w in C05 loop
					begin
						ie_gerar_ocorrencia_w	:= 'S';

						if	(ie_utiliza_filtro_p	= 'S') then
						/* Tratamento para filtros */
							pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, nr_seq_requisicao_p,
											null, null, null,
											null,r_C05_w.nr_sequencia, null,
											null, null, r_C05_w.nr_seq_material,
											null,nr_seq_prestador_w, nr_seq_ocorrencia_p,
											null, null, null,
											nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

							if	(ie_regra_w	= 'S') then
								ie_gerar_ocorrencia_w	:= 'S';
							elsif	(ie_regra_w	in ('E','N')) then
								ie_gerar_ocorrencia_w	:= 'N';
							end if;
						end if;

						if	(ie_gerar_ocorrencia_w	= 'S') then

							ie_gerar_ocorrencia_w	:= ie_gerar_ocor_atend_w;

							if	(ie_gerar_ocorrencia_w	= 'S') then
								pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
											null, null, null, 
											null, nr_seq_ocor_combinada_p, nm_usuario_p, 
											null, nr_seq_motivo_glosa_p, 9, 
											cd_estabelecimento_p, 'N' ,null,
											nr_seq_oc_benef_w, null,
											null, null, null);

								pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
												null, nr_seq_requisicao_p, null,
												null, r_C05_w.nr_sequencia, null,
												null, null, null,
												nm_usuario_p, cd_estabelecimento_p);
							end if;
						end if;
					end;
				end loop;
			elsif 	(nr_seq_execucao_p is not null) then	
				begin
					select	nr_seq_prestador,
						nr_seq_requisicao,
						nr_seq_lote_exec
					into	nr_seq_prestador_w,
						nr_seq_requisicao_w,
						nr_seq_lote_exec_w
					from	pls_execucao_requisicao
					where	nr_sequencia	= nr_seq_execucao_p;
				exception
				when others then
						nr_seq_prestador_w	:= null;
						nr_seq_requisicao_w	:= null;
				end;

				/*begin
				select	ie_tipo_atendimento,
				ie_tipo_guia
				into	ie_tipo_atendimento_w,
				ie_tipo_guia_w
				from	pls_execucao_req_item
				where	nr_seq_execucao	= nr_seq_execucao_p;
				exception
				when others then
				ie_tipo_atendimento_w	:= null;
				ie_tipo_guia_w		:= null;
				end;*/

				--Foram realizados os selects abaixo pois, ao realizar a consulta diretamente na tablea pls_execucao_req_item, as informa��es ainda n�o est�o presentes na mesma

				begin
					select	nr_seq_segurado,
						ie_tipo_guia
					into	nr_seq_segurado_w,
						ie_tipo_guia_w
					from	pls_requisicao
					where	nr_sequencia	= nr_seq_requisicao_w;
				exception
				when others then
					nr_seq_segurado_w	:= null;
					ie_tipo_guia_w		:= null;
				end;

				begin
					select	max(ie_tipo_atendimento)
					into	ie_tipo_atendimento_w
					from	pls_itens_lote_execucao
					where	nr_seq_lote_exec = nr_seq_lote_exec_w;
				exception
				when others then
					ie_tipo_atendimento_w	:= null;
				end;

				if (ie_tipo_atendimento_w is null) then
					begin
						select	ie_tipo_atendimento
						into	ie_tipo_atendimento_w
						from	pls_requisicao
						where	nr_sequencia	= nr_seq_requisicao_w;
					exception
					when others then
						ie_tipo_atendimento_w	:= null;
					end;
				end if;

				ie_gerar_ocor_atend_w	:= 'S';

				if	(nvl(r_C01_w.ie_exige_tipo_atend, 'N') = 'S') and (ie_tipo_atendimento_w is null) and (ie_tipo_guia_w = '2') then			
					ie_gerar_ocor_atend_w := 'S';
				else 
					ie_gerar_ocor_atend_w := 'N';
				end if;

				for r_C06_w in C06 loop
					begin
						ie_gerar_ocorrencia_w	:= 'S';

						if	(ie_utiliza_filtro_p	= 'S') then
							/* Tratamento para filtros */
							pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, null,
											nr_seq_execucao_p, null, null,
											null, null, r_C06_w.nr_sequencia,
											r_C06_w.cd_procedimento, r_C06_w.ie_origem_proced, null,
											null,nr_seq_prestador_w, nr_seq_ocorrencia_p,
											null, null, null,
											nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

							if	(ie_regra_w	= 'S') then
								ie_gerar_ocorrencia_w	:= 'S';
							elsif	(ie_regra_w	in ('E','N')) then
								ie_gerar_ocorrencia_w	:= 'N';
							end if;
						end if;

						if	(ie_gerar_ocorrencia_w	= 'S') then

							ie_gerar_ocorrencia_w	:= ie_gerar_ocor_atend_w;

							if	(ie_gerar_ocorrencia_w	= 'S') then
								pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, null,
											null, null, null, 
											null, nr_seq_ocor_combinada_p, nm_usuario_p, 
											null, nr_seq_motivo_glosa_p, 10, 
											cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
											nr_seq_oc_benef_w, null,
											null, null, null);

								pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
												null, null, nr_seq_execucao_p,
												null, null, null,
												null, r_C06_w.nr_sequencia, null,
												nm_usuario_p, cd_estabelecimento_p);
							end if;
						end if;
					end;
				end loop;

				for r_C07_w in C07 loop
					begin
						ie_gerar_ocorrencia_w	:= 'S';

						if	(ie_utiliza_filtro_p	= 'S') then
							/* Tratamento para filtros */
							pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, null,
											nr_seq_execucao_p, null, null,
											null, null, r_C07_w.nr_sequencia,
											null, null, r_C07_w.nr_seq_material,
											ie_gerou_ocor_cabecalho_w,nr_seq_prestador_w, nr_seq_ocorrencia_p,
											null, null, null,
											nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

							if	(ie_regra_w	= 'S') then
								ie_gerar_ocorrencia_w	:= 'S';
							elsif	(ie_regra_w	in ('E','N')) then
								ie_gerar_ocorrencia_w	:= 'N';
							end if;
						end if;

						if	(ie_gerar_ocorrencia_w	= 'S') then

							ie_gerar_ocorrencia_w	:= ie_gerar_ocor_atend_w;

							if	(ie_gerar_ocorrencia_w	= 'S') then
								pls_inserir_ocorrencia(	nr_seq_segurado_w, nr_seq_ocorrencia_p, null,
											null, null, null, 
											null, nr_seq_ocor_combinada_p, nm_usuario_p, 
											null, nr_seq_motivo_glosa_p, 11, 
											cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
											nr_seq_oc_benef_w, null,
											null, null, null);

								pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
												null, null, nr_seq_execucao_p,
												null, null, null,
												null, null, r_C07_w.nr_sequencia,
												nm_usuario_p, cd_estabelecimento_p);
							end if;
						end if;
					end;
				end loop;
			end if;
		end;
	end loop;
end pls_valida_ocor_aut_tipo_ate;
/
