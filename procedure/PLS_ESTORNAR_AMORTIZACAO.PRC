create or replace
procedure pls_estornar_amortizacao(	nr_titulo_p			number,
					nr_seq_baixa_estornada_p	number,
					nr_seq_baixa_estorno_p		number,
					nm_usuario_p			Varchar2) is

nr_sequencia_w			number(5);
nr_seq_liq_amortizacao_w	number(5);
nr_seq_amortiz_anterior_w	number(10);
nr_seq_amortizacao_w		number(10);

begin

if	(nr_titulo_p is not null) and
	(nr_seq_baixa_estornada_p is not null) and
	(nr_seq_baixa_estorno_p is not null) then
	
	select	max(nr_seq_liq_amortizacao),
		max(nr_seq_amortizacao)
	into	nr_seq_liq_amortizacao_w,
		nr_seq_amortiz_anterior_w
	from	titulo_receber_liq a
	where	a.nr_titulo	= nr_titulo_p
	and	a.nr_sequencia	= nr_seq_baixa_estornada_p;
	
	if	(nr_seq_liq_amortizacao_w is not null) or
		(nr_seq_amortiz_anterior_w is not null) then -- Caso seja estornada a pr�pria baixa de amortiza��o
	
		if	(nr_seq_amortiz_anterior_w is null) then
			select	max(nr_seq_amortizacao)
			into	nr_seq_amortiz_anterior_w
			from	titulo_receber_liq a
			where	a.nr_titulo	= nr_titulo_p
			and	a.nr_sequencia	= nr_seq_liq_amortizacao_w;
		end if;
		
		-- Gerar amortiza��o
		select	pls_pagador_amortizacao_seq.nextval
		into	nr_seq_amortizacao_w
		from	dual;

		insert	into	pls_pagador_amortizacao
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_contrato,
			nr_seq_pagador,
			nr_titulo_origem,
			dt_amortizacao,
			vl_amortizado)
		select	nr_seq_amortizacao_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_contrato,
			nr_seq_pagador,
			nr_titulo_p,
			dt_amortizacao,
			vl_amortizado * -1
		from	pls_pagador_amortizacao
		where	nr_sequencia	= nr_seq_amortiz_anterior_w;
		
		if	(nr_seq_liq_amortizacao_w is not null) then
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	titulo_receber_liq
			where	nr_titulo	= nr_titulo_p;
		
			insert into titulo_receber_liq
				(nr_titulo,
				nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				dt_recebimento,
				vl_recebido,
				vl_descontos,
				vl_juros,
				vl_multa,
				cd_moeda,
				cd_tipo_recebimento,
				vl_rec_maior,
				vl_glosa,
				ie_lib_caixa,
				ds_observacao,
				nr_seq_amortizacao,
				ie_acao,
				nr_seq_trans_fin,
				nr_lote_contab_antecip,
				nr_lote_contab_pro_rata,
				nr_lote_contabil)
			select	nr_titulo_p,
				nr_sequencia_w,
				nm_usuario_p,
				sysdate,
				dt_recebimento,
				vl_recebido * -1,
				0,
				0,
				0,
				cd_moeda,
				cd_tipo_recebimento,
				0,
				0,
				'S',
				substr(wheb_mensagem_pck.get_texto(303775),1,255),
				nr_seq_amortizacao_w,
				'I',
				nr_seq_trans_fin,
				0,
				0,
				0
			from	titulo_receber_liq a
			where	a.nr_titulo	= nr_titulo_p
			and	a.nr_sequencia	= nr_seq_liq_amortizacao_w;
			
			atualizar_saldo_tit_rec(nr_titulo_p, nm_usuario_p); -- AAMFIRMO OS 624010 atualizar saldo do titulo quando a baixa de amortiza��o for estornada. 
									--Dentro desta procedure chama outras procedures que possuem commit, mas recebem parametros como N, para n comitar.
		end if;
	end if;
end if;

/* N�o pode commitar */

end pls_estornar_amortizacao;
/