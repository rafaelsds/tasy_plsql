create or replace
procedure ptu_importar_confirmacao_v50
			(	ds_arquivo_p		Varchar2,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Rotina utilizada nas transa��es ptu via SCS homologadas com a unimed brasil.
Quando for alterar, favor verificar com o an�lista respons�vel para a realiza��o de testes.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_transacao_w			Varchar2(10);
ie_tipo_cliente_w		Varchar2(15);
cd_unimed_executora_w		Number(4);
cd_unimed_beneficiario_w	Number(4);
nr_seq_execucao_w		Number(10);
nr_seq_origem_w			Number(10);
nr_seq_origem_ww		Number(10);
nr_seq_guia_w			Number(10);
nr_seq_requisicao_w		Number(10);
ds_conteudo_w			Varchar2(4000);
ie_tipo_identifica_w		pls_integer;
nr_seq_import_w			pls_integer;
qt_reg_canelamento_w		pls_integer;
qt_reg_insistencia_w		pls_integer;
ie_tipo_resposta_w		Varchar2(2);
nr_seq_pedido_compl_w		Number(10);
nr_seq_pedido_aut_w		Number(10);
qt_reg_confir_cancel_w		pls_integer;
qt_reg_confir_insist_w		pls_integer;
qt_reg_resp_aud_w		pls_integer;
qt_reg_confir_aud_w		pls_integer;
nr_versao_w			Varchar2(3);
qt_reg_decurso_w		pls_integer;
qt_reg_confir_decurso_w		pls_integer;
qt_registros_aud_w		pls_integer;
qt_proc_aut_w			pls_integer;
qt_proc_neg_w			pls_integer;
nr_seq_res_ord_serv_w		ptu_resposta_req_ord_serv.nr_sequencia%type;
cd_unimed_solicitante_w		ptu_resposta_req_ord_serv.cd_unimed_solicitante%type;
nr_seq_solic_w			ptu_resposta_req_ord_serv.nr_seq_origem%type;
qt_reg_autoriz_os_w		pls_integer;
qt_reg_confir_autoriz_os_w	pls_integer;

nr_seq_segurado_w		pls_guia_plano.nr_seq_segurado%type;
dt_solicitacao_w		pls_guia_plano.dt_solicitacao%type;
ie_tipo_guia_w			varchar(2);
dt_valid_senha_w		Date;
cd_senha_w			Varchar2(20);
dt_solicitacao_varchar_w	Varchar2(20);
nr_seq_cancel_wsd_w		number(10);
ie_gerar_senha_interna_w	varchar2(1);

cursor c01 is
	select	nr_seq_importacao,
		ds_valores
	from	w_scs_importacao
	where	nm_usuario	= nm_usuario_p
	order by nr_seq_importacao;

	
begin
-- Verificar o par�metro na fun��o OPS - Gest�o de Operadoras / Par�metros OPS / Interc�mbio / Interc�mbio SCS
begin
	select	ie_gerar_senha_interna
	into	ie_gerar_senha_interna_w
	from	pls_param_intercambio_scs;
exception
when others then
	ie_gerar_senha_interna_w	:= null;
end;

open c01;
loop
fetch c01 into
	nr_seq_import_w,
	ds_conteudo_w;
exit when c01%notfound;
	begin
	if	(substr(ds_conteudo_w,1,4)	<> 'FIM$') then
		if	(nr_seq_import_w	= 1) then
			select  substr(ds_conteudo_w,1,5),
				trim(substr(ds_conteudo_w,6,15)),
				decode(to_number(substr(ds_conteudo_w,21,4)),0,'',to_number(substr(ds_conteudo_w,21,4))),
				decode(to_number(substr(ds_conteudo_w,25,4)),0,'',to_number(substr(ds_conteudo_w,25,4))),
				decode(to_number(substr(ds_conteudo_w,29,10)),0,'',to_number(substr(ds_conteudo_w,29,10))),
				decode(to_number(substr(ds_conteudo_w,39,10)),0,'',to_number(substr(ds_conteudo_w,39,10))),
				decode(to_number(substr(ds_conteudo_w,49,1)),0,'',to_number(substr(ds_conteudo_w,49,1))),
				trim(substr(ds_conteudo_w,50,3))
			into	cd_transacao_w,
				ie_tipo_cliente_w,
				cd_unimed_executora_w,
				cd_unimed_beneficiario_w,
				nr_seq_execucao_w,
				nr_seq_origem_w,
				ie_tipo_identifica_w,
				nr_versao_w
			from	dual;

			begin
				select	a.nr_sequencia
				into	nr_seq_pedido_compl_w
				from	ptu_pedido_compl_aut a,
					ptu_controle_execucao b
				where	b.nr_sequencia		= nr_seq_execucao_w
				and	a.nr_sequencia		= b.nr_seq_pedido_compl
				and	a.cd_unimed_executora	= cd_unimed_executora_w;			
			exception
			when others then
				begin
					select	a.nr_sequencia
					into	nr_seq_pedido_aut_w
					from	ptu_pedido_autorizacao a,
						ptu_controle_execucao b
					where	b.nr_sequencia		= nr_seq_execucao_w
					and	a.nr_sequencia		= b.nr_seq_pedido_aut
					and	a.cd_unimed_executora	= cd_unimed_executora_w;				
				exception
				when others then
					begin
						select	nvl(nr_seq_pedido_compl,0),
							nvl(nr_seq_pedido_aut,0)
						into	nr_seq_pedido_compl_w,
							nr_seq_pedido_aut_w
						from	ptu_controle_execucao
						where	nr_sequencia	= nr_seq_origem_w;
					exception
					when others then
						select 	nvl(nr_seq_origem,0)
						into	nr_seq_origem_ww
						from    ptu_resposta_autorizacao
						where   nr_seq_execucao 	= nr_seq_execucao_w
						and	cd_unimed_executora	= cd_unimed_executora_w;
						
						select	nvl(nr_seq_pedido_compl,0),
							nvl(nr_seq_pedido_aut,0)
						into	nr_seq_pedido_compl_w,
							nr_seq_pedido_aut_w
						from	ptu_controle_execucao
						where	nr_sequencia	= nr_seq_origem_ww;
					end;				
				end;				
			end;			

			if	(nr_seq_pedido_aut_w	<> 0) then
				select	max(nr_seq_guia),
					max(nr_seq_requisicao)
				into	nr_seq_guia_w,
					nr_seq_requisicao_w
				from	ptu_pedido_autorizacao
				where	nr_sequencia	= nr_seq_pedido_aut_w;
			elsif	(nr_seq_pedido_compl_w	<> 0) then
				select	max(nr_seq_guia),
					max(nr_seq_requisicao)
				into	nr_seq_guia_w,
					nr_seq_requisicao_w
				from	ptu_pedido_compl_aut
				where	nr_sequencia	= nr_seq_pedido_compl_w;
			end if;

			if	(ie_tipo_cliente_w	= 'UNIMED') then
				ie_tipo_cliente_w	:= 'U';
			elsif	(ie_tipo_cliente_w	= 'PORTAL') then
				ie_tipo_cliente_w	:= 'P';
			elsif	(ie_tipo_cliente_w	= 'PRESTADOR') then
				ie_tipo_cliente_w	:= 'R';
			end if;

			select	count(1)
			into	qt_reg_decurso_w
			from	ptu_decurso_prazo	a
			where	a.nr_seq_execucao	= nr_seq_execucao_w;

			select 	count(1)
			into	qt_reg_confir_decurso_w
			from	ptu_confirmacao	x
			where	x.nr_seq_execucao	= nr_seq_execucao_w
			and	x.ie_tipo_resposta	= 'DP';

			if	(qt_reg_decurso_w	<> qt_reg_confir_decurso_w) then
				ie_tipo_resposta_w	:= 'DP';
			end if;

			select	count(1)
			into	qt_reg_autoriz_os_w
			from	ptu_autorizacao_ordem_serv	a
			where	a.nr_seq_execucao		= nr_seq_execucao_w;

			select 	count(1)
			into	qt_reg_confir_autoriz_os_w
			from	ptu_confirmacao	x
			where	x.nr_seq_execucao	= nr_seq_execucao_w
			and	x.ie_tipo_resposta	= 'AO';

			if	(qt_reg_autoriz_os_w	<> qt_reg_confir_autoriz_os_w) then
				ie_tipo_resposta_w	:= 'AO';
			end if;

			select	count(1)
			into	qt_reg_canelamento_w
			from	ptu_cancelamento	a
			where	a.nr_seq_execucao	= nr_seq_execucao_w;

			if	(qt_reg_canelamento_w	= 0) and (nr_seq_execucao_w	= nr_seq_origem_w) then
				select	count(1)
				into	qt_reg_canelamento_w
				from	ptu_cancelamento		a
				where	a.nr_seq_origem			= nr_seq_origem_w
				and	a.cd_unimed_beneficiario	= cd_unimed_beneficiario_w;
			end if;

			select	count(1)
			into	qt_reg_confir_cancel_w
			from	ptu_confirmacao	x
			where	x.nr_seq_execucao	= nr_seq_execucao_w
			and	x.ie_tipo_resposta	= 'C';

			if	(qt_reg_confir_cancel_w	= 0) and (nr_seq_execucao_w	= nr_seq_origem_w) then
				select	count(1)
				into	qt_reg_confir_cancel_w
				from	ptu_confirmacao	x
				where	x.nr_seq_origem			= nr_seq_origem_w
				and	x.cd_unimed_beneficiario	= cd_unimed_beneficiario_w
				and	x.ie_tipo_resposta		= 'C';
			end if;

			if	(qt_reg_canelamento_w	<> qt_reg_confir_cancel_w ) then
				ie_tipo_resposta_w	:= 'C';
			end if;

			select	count(1)
			into	qt_reg_insistencia_w
			from	ptu_pedido_insistencia	a
			where	a.nr_seq_execucao	= nr_seq_execucao_w;

			select 	count(1)
			into	qt_reg_confir_insist_w
			from	ptu_confirmacao	x
			where	x.nr_seq_execucao	= nr_seq_execucao_w
			and	x.ie_tipo_resposta	= 'PI';

			if	(qt_reg_insistencia_w	<> qt_reg_confir_insist_w ) then
				ie_tipo_resposta_w	:= 'PI';
			end if;

			select	count(1)
			into	qt_reg_resp_aud_w
			from	ptu_resposta_auditoria	a
			where	a.nr_seq_execucao	= nr_seq_execucao_w;

			select	count(1)
			into	qt_reg_confir_aud_w
			from	ptu_confirmacao	x
			where	x.nr_seq_execucao	= nr_seq_execucao_w
			and	x.ie_tipo_resposta	= 'RA';

			if	(qt_reg_resp_aud_w	<> qt_reg_confir_aud_w ) then
				ie_tipo_resposta_w	:= 'RA';
			end if;

			if	(ie_tipo_resposta_w	= 'C') and (ie_tipo_identifica_w  = '1') then
				select	max(nr_sequencia)
				into 	nr_seq_cancel_wsd_w
				from 	pls_guia_motivo_cancel
				where 	ie_motivo_cancel_wsd = 'EC'
				and	ie_situacao = 'A';

				if	(nr_seq_cancel_wsd_w is null) then
					select	max(nr_sequencia)
					into 	nr_seq_cancel_wsd_w
					from 	pls_guia_motivo_cancel;
				end if;

				if	(nr_seq_requisicao_w	is not null) then
					pls_cancelar_requisicao(nr_seq_requisicao_w, nr_seq_cancel_wsd_w, '', nm_usuario_p, cd_estabelecimento_p);

				elsif	(nr_seq_guia_w	is not null) then
					pls_cancelar_autorizacao(nr_seq_guia_w, nr_seq_cancel_wsd_w, nm_usuario_p,null);
				end if;

			elsif	(ie_tipo_resposta_w	= 'PI') and (ie_tipo_identifica_w  = '1') then
				if	(nr_seq_requisicao_w	is not null) then
					update	pls_requisicao_proc
					set	ie_status		= 'A',
						ie_estagio		= 'AA',
						dt_atualizacao		= sysdate,
						nm_usuario		= nm_usuario_p
					where	nr_seq_requisicao	= nr_seq_requisicao_w;

					update	pls_requisicao_mat
					set	ie_status		= 'A',
						ie_estagio		= 'AA',
						dt_atualizacao		= sysdate,
						nm_usuario		= nm_usuario_p
					where	nr_seq_requisicao	= nr_seq_requisicao_w;

					update	pls_requisicao
					set	ie_estagio	= 5,
						ie_status	= 'P',
						dt_atualizacao	= sysdate,
						nm_usuario	= nm_usuario_p
					where	nr_sequencia	= nr_seq_requisicao_w;

					pls_gerar_auditoria_requisicao(nr_seq_requisicao_w, nm_usuario_p,'AE');
					ptu_gerar_grupo_aud_padrao(null,nr_seq_requisicao_w,'GC',nm_usuario_p);
				elsif	(nr_seq_guia_w	is not null) then
					update	pls_guia_plano_proc
					set	ie_status	= 'A',
						dt_atualizacao	= sysdate,
						nm_usuario	= nm_usuario_p
					where	nr_seq_guia	= nr_seq_guia_w;

					update	pls_guia_plano_mat
					set	ie_status	= 'A',
						dt_atualizacao	= sysdate,
						nm_usuario	= nm_usuario_p
					where	nr_seq_guia	= nr_seq_guia_w;

					update	pls_guia_plano
					set	ie_estagio	= 11,
						ie_status	= '2',
						dt_atualizacao	= sysdate,
						nm_usuario	= nm_usuario_p
					where	nr_sequencia	= nr_seq_guia_w;

					pls_gerar_auditoria_guia(nr_seq_guia_w, nm_usuario_p);
					ptu_gerar_grupo_aud_padrao(nr_seq_guia_w,null,'GC',nm_usuario_p);
				end if;	
			elsif	((ie_tipo_resposta_w	= 'DP') and (ie_tipo_identifica_w  in ('1','4'))) then

				if	(nr_seq_requisicao_w	is not null) then
					update	pls_requisicao_proc
					set	ie_status		= 'S',
						qt_procedimento		= qt_solicitado,
						dt_atualizacao		= sysdate,
						nm_usuario		= nm_usuario_p
					where	nr_seq_requisicao	= nr_seq_requisicao_w
					and	ie_status		= 'A';

					update	pls_requisicao_mat
					set	ie_status		= 'S',
						qt_material		= qt_solicitado,
						dt_atualizacao		= sysdate,
						nm_usuario		= nm_usuario_p
					where	nr_seq_requisicao	= nr_seq_requisicao_w
					and	ie_status		= 'A';

					select	nr_seq_segurado,
						dt_requisicao,
						ie_tipo_guia
					into	nr_seq_segurado_w,
						dt_solicitacao_w,
						ie_tipo_guia_w
					from	pls_requisicao
					where	nr_sequencia = nr_seq_requisicao_w;

					pls_gerar_validade_senha_req (	nr_seq_requisicao_w,
									nr_seq_segurado_w,
									dt_solicitacao_w,
									ie_tipo_guia_w,
									nm_usuario_p);

					update	pls_requisicao
					set	ie_estagio	= 2,
						dt_atualizacao	= sysdate,
						nm_usuario	= nm_usuario_p,
						cd_senha_externa = nr_seq_origem_w
					where	nr_sequencia	= nr_seq_requisicao_w;

					select	count(1)
					into	qt_registros_aud_w
					from	pls_auditoria
					where	nr_seq_requisicao	= nr_seq_requisicao_w
					and	dt_liberacao		is null;

					if	(qt_registros_aud_w > 0) then
						update	pls_auditoria
						set	ie_status		= 'F',
							dt_liberacao		= sysdate,
							nr_seq_proc_interno	= null,
							dt_atualizacao		= sysdate,
							nm_usuario		= nm_usuario_p
						where	nr_seq_requisicao	= nr_seq_requisicao_w;
					end if;

					pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L','Requisi��o autorizada por decurso de prazo',null,nm_usuario_p);
					
					update	pls_auditoria_grupo
					set	dt_liberacao		= sysdate,
						ie_status		= 'S',
						nm_usuario		= nm_usuario_p,
						dt_atualizacao		= sysdate
					where	dt_liberacao is null
					and	exists(	select	1
							from 	pls_auditoria x
							where 	x.nr_seq_requisicao	= nr_seq_requisicao_w
							and 	x.nr_sequencia 		= nr_seq_auditoria);									
					
				elsif	(nr_seq_guia_w	is not null) then
					update	pls_guia_plano_proc
					set	ie_status	= 'S',
						qt_autorizada	= qt_solicitada,
						dt_atualizacao	= sysdate,
						nm_usuario	= nm_usuario_p
					where	nr_seq_guia	= nr_seq_guia_w
					and	ie_status	= 'A';

					update	pls_guia_plano_mat
					set	ie_status	= 'S',
						qt_autorizada	= qt_solicitada,
						dt_atualizacao	= sysdate,
						nm_usuario	= nm_usuario_p
					where	nr_seq_guia	= nr_seq_guia_w
					and	ie_status	= 'A';

					select	nr_seq_segurado,
						dt_solicitacao,
						ie_tipo_guia
					into	nr_seq_segurado_w,
						dt_solicitacao_w,
						ie_tipo_guia_w
					from	pls_guia_plano
					where	nr_sequencia = nr_seq_guia_w;

					if	(nvl(ie_gerar_senha_interna_w,'S')	= 'S') then
						pls_gerar_validade_senha(nr_seq_guia_w,
									nr_seq_segurado_w,
									0/*qt_dias_val_senha_p*/,
									dt_solicitacao_w,
									ie_tipo_guia_w,
									nm_usuario_p,
									dt_solicitacao_varchar_w,
									cd_senha_w);

						dt_valid_senha_w := to_date(dt_solicitacao_varchar_w, 'dd/mm/rrrr');
					else
						dt_valid_senha_w := to_date(SYSDATE + 60, 'dd/mm/rrrr');
						cd_senha_w	:= null;
					end if;

					update	pls_guia_plano
					set	ie_estagio		= 6,
						ie_status		= '1',
						dt_atualizacao		= sysdate,
						nm_usuario		= nm_usuario_p,
						cd_senha 		= cd_senha_w,
						dt_validade_senha 	= dt_valid_senha_w,
						cd_senha_externa 	= nr_seq_origem_w
					where	nr_sequencia		= nr_seq_guia_w;

					select	count(1)
					into	qt_registros_aud_w
					from	pls_auditoria
					where	nr_seq_guia	= nr_seq_guia_w
					and	dt_liberacao	is null;

					if	(qt_registros_aud_w > 0) then
						update	pls_auditoria
						set	ie_status		= 'F',
							dt_liberacao		= sysdate,
							nr_seq_proc_interno	= null,
							dt_atualizacao		= sysdate,
							nm_usuario		= nm_usuario_p
						where	nr_seq_guia		= nr_seq_guia_w;
					end if;

					pls_guia_gravar_historico(nr_seq_guia_w,2,'Guia autorizada por decurso de prazo','',nm_usuario_p);
					
					update	pls_auditoria_grupo
					set	dt_liberacao		= sysdate,
						ie_status		= 'S',
						nm_usuario		= nm_usuario_p,
						dt_atualizacao		= sysdate
					where	dt_liberacao is null
					and	exists(	select	1
							from 	pls_auditoria x
							where 	x.nr_seq_guia 	= nr_seq_guia_w
							and 	x.nr_sequencia 	= nr_seq_auditoria);					
				end if;
			elsif	(ie_tipo_resposta_w	= 'AO') and (ie_tipo_identifica_w	= '1') then
				select	max(nr_sequencia)
				into	nr_seq_res_ord_serv_w
				from	ptu_resposta_req_ord_serv
				where	nr_seq_execucao	= nr_seq_execucao_w;

				if	(nr_seq_res_ord_serv_w	is not null) then
					select	count(1)
					into	qt_proc_aut_w
					from	ptu_resposta_req_servico
					where	nr_seq_resp_req_ord	= nr_seq_res_ord_serv_w
					and	ie_status_requisicao	= 2;

					select	count(1)
					into	qt_proc_neg_w
					from	ptu_resposta_req_servico
					where	nr_seq_resp_req_ord	= nr_seq_res_ord_serv_w
					and	ie_status_requisicao	= 1;

					select	cd_unimed_solicitante,
						nr_seq_origem
					into	cd_unimed_solicitante_w,
						nr_seq_solic_w
					from	ptu_resposta_req_ord_serv
					where	nr_sequencia	= nr_seq_res_ord_serv_w;
				end if;

				if	(qt_proc_aut_w	> 0) and (qt_proc_neg_w	= 0) then
					update	ptu_requisicao_ordem_serv
					set	ie_estagio			= 3,
						dt_atualizacao			= sysdate,
						nm_usuario			= nm_usuario_p
					where	nr_transacao_solicitante	= nr_seq_solic_w
					and	cd_unimed_solicitante		= cd_unimed_solicitante_w;
				elsif	(qt_proc_aut_w	= 0) and (qt_proc_neg_w	> 0) then
					update	ptu_requisicao_ordem_serv
					set	ie_estagio			= 4,
						dt_atualizacao			= sysdate,
						nm_usuario			= nm_usuario_p
					where	nr_transacao_solicitante	= nr_seq_solic_w
					and	cd_unimed_solicitante		= cd_unimed_solicitante_w;
				elsif	(qt_proc_aut_w	> 0) and (qt_proc_neg_w	> 0) then
					update	ptu_requisicao_ordem_serv
					set	ie_estagio			= 5,
						dt_atualizacao			= sysdate,
						nm_usuario			= nm_usuario_p
					where	nr_transacao_solicitante	= nr_seq_solic_w
					and	cd_unimed_solicitante		= cd_unimed_solicitante_w;
				end if;
			end if;

			insert	into ptu_confirmacao
				(nr_sequencia, cd_transacao, ie_tipo_cliente,
				 cd_unimed_executora, cd_unimed_beneficiario, nr_seq_execucao,
				 dt_atualizacao, nm_usuario, nr_seq_origem,
				 nr_seq_requisicao, nr_seq_guia, ie_tipo_identificador,
				 ie_tipo_resposta, ds_arquivo_pedido, nm_usuario_nrec,
				 dt_atualizacao_nrec, nr_versao)
			values	(ptu_confirmacao_seq.NextVal, cd_transacao_w, ie_tipo_cliente_w,
				 cd_unimed_executora_w, cd_unimed_beneficiario_w, nr_seq_execucao_w,
				 sysdate, nm_usuario_p, nr_seq_origem_w,
				 nr_seq_requisicao_w, nr_seq_guia_w, ie_tipo_identifica_w,
				 ie_tipo_resposta_w, ds_arquivo_p, nm_usuario_p,
				 sysdate, nr_versao_w);

			if	(ie_tipo_resposta_w	in ('PI','C','DP')) then
				if	(nr_seq_requisicao_w	is not null) then
					pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L','Recebida e processada a confirma��o de recebimento da Unimed '||cd_unimed_beneficiario_w,null,nm_usuario_p);
					-- Se for uma requisi��o recebida por webService(TISS) ent�o a guia gerada deve ser atualizada conforme a requisi��o
					ptu_atualizar_guia_proc_ws(nr_seq_requisicao_w, nm_usuario_p);
				elsif	(nr_seq_guia_w	is not null) then
					pls_guia_gravar_historico(nr_seq_guia_w,2,'Recebida e processada a confirma��o de recebimento da Unimed '||cd_unimed_beneficiario_w,'',nm_usuario_p);
				end if;
			elsif	(ie_tipo_resposta_w	= 'RA') then
				if	(nr_seq_requisicao_w	is not null) then
					pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L','Recebida e processada a confirma��o de recebimento da Unimed '||cd_unimed_executora_w,null,nm_usuario_p);
					-- Se for uma requisi��o recebida por webService(TISS) ent�o a guia gerada deve ser atualizada conforme a requisi��o
					ptu_atualizar_guia_proc_ws(nr_seq_requisicao_w, nm_usuario_p);
				elsif	(nr_seq_guia_w	is not null) then
					pls_guia_gravar_historico(nr_seq_guia_w,2,'Recebida e processada a confirma��o de recebimento da Unimed '||cd_unimed_executora_w,'',nm_usuario_p);
				end if;
			end if;
		end if;
	end if;
	end;
end loop;
close c01;

commit;

end ptu_importar_confirmacao_v50;
/