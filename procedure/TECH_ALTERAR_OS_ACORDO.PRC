create or replace
procedure tech_alterar_os_acordo(		
		nr_acordo_atual_p			number,
		nr_acordo_destino_p		number,
		nr_ordem_servico_p		number,
		nr_prioridade_p			number) is 

begin

	update	tech_agreement_so
	set 	nr_prioridade = nr_prioridade_p,
			nr_seq_tech_agreement = nr_acordo_destino_p
	where 	nr_seq_ordem_servico = nr_ordem_servico_p
	and 	nr_seq_tech_agreement = nr_acordo_atual_p;

	commit;
	
end tech_alterar_os_acordo;
/