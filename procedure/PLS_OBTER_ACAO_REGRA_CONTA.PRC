create or replace
procedure pls_obter_acao_regra_conta
			(	nr_seq_procedimento_p	in	Number,
				ie_tipo_consiste_p	in	Varchar2,
				nm_usuario_p		in	Varchar2,
				cd_estabelecimento_p	in	Number,
				nr_seq_clinica_p	in	Number,
				nr_seq_acao_regra_p	out	Number,
				ds_retorno_p		out	Varchar2) is 
			
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
cd_grupo_proc_w			Number(15);
cd_especialidade_w		Number(8);
cd_area_procedimento_w		Number(8);
nr_seq_acao_regra_w		Number(10);
ie_estrutura_w			Varchar2(1)	:= 'N';
ie_tipo_guia_w			Varchar2(2);
ie_guia_referencia_w		Varchar2(1)	:= 'N';
ie_nascidos_vivos_w		Varchar2(1)	:= 'N';
cd_guia_referencia_w		Varchar2(20)	:= '0';
cd_guia_w			Varchar2(20);
qt_nasc_vivos_imp_w		Varchar2(2)	:= '0';
ds_retorno_w			Varchar2(2000)	:= '';
ie_regime_internacao_w		Varchar2(1)	:= 'N';
ie_tipo_acomodacao_w		Varchar2(1)	:= 'N';
ie_tipo_internacao_w		Varchar2(1)	:= 'N';
ie_motivo_saida_int_w		Varchar2(1)	:= 'N';
ie_data_internacao_w		Varchar2(1)	:= 'N';
ie_regime_int_conta_w		Varchar2(10);
nr_seq_tipo_acomod_w		Number(10);
nr_seq_saida_int_w		Number(10);
nr_seq_clinica_w		Number(10);
cd_tipo_tabela_imp_w		Varchar2(10);
cd_tipo_acomocadao_imp_w	Varchar2(2);
cd_motivo_alta_imp_w		Varchar2(3);
qt_nasc_mortos_imp_w		Varchar2(2);
qt_nasc_vivos_prematuros_imp_w	Varchar2(2);
ie_parto_cesaria_imp_w		Varchar2(10)	:= '1';
dt_entrada_w			Date;
dt_alta_w			Date;
nr_seq_tipo_atendimento_w	number(10);
ie_tipo_atendimento_w		number(10);
IE_DECLARACAO_OBITO_w		Varchar2(1)	:= 'N';
IE_DECLARACAO_NASC_VIVO_w	Varchar2(1)	:= 'N';
IE_CID_PRINCIPAL_w		Varchar2(1)	:= 'N';
IE_CARATER_ATEND_w		Varchar2(1)	:= 'N';
IE_CID_OBITO_w			Varchar2(1)	:= 'N';
nr_seq_saida_spsadt_w		Number(10);
nr_declaracao_obito_w		Varchar2(20)	:= '';
cd_doenca_obito_w		Varchar2(10)	:= '';
cd_doenca_w			Varchar2(10)	:= '';
nr_seq_conta_w			Number(10);
nr_seq_protocolo_w		Number(10);
cd_versao_tiss_w		Varchar2(20);
ie_carater_internacao_w		Varchar2(1)	:= '';
nr_seq_decl_nasc_vivo_w		Varchar2(15)	:= '';
ie_data_alta_int_w		pls_acao_regra_conta.ie_data_alta_int%type;
nr_seq_saida_consulta_w		pls_conta.nr_seq_saida_consulta%type;
ie_tipo_consulta_w		pls_conta.ie_tipo_consulta%type;

Cursor C01 is
	select	b.nr_sequencia,
		a.ie_estrutura
	from	pls_filtro_regra_conta	a,
		pls_acao_regra_conta	b
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.ie_situacao	= 'A'
	and	((a.cd_procedimento	is null) or ((a.cd_procedimento	= cd_procedimento_w) and (a.ie_origem_proced	= a.ie_origem_proced)))
	and	((a.cd_grupo_proc	is null) or (a.cd_grupo_proc	= cd_grupo_proc_w))
	and	((a.cd_especialidade	is null) or (a.cd_especialidade	= cd_especialidade_w))
	and	((a.ie_tipo_guia	is null) or (a.ie_tipo_guia	= ie_tipo_guia_w))
	and	((a.nr_seq_clinica 	is null) or (a.nr_seq_clinica = nr_seq_clinica_p))
	and	(( a.nr_seq_saida_int 	is null) or ( a.nr_seq_saida_int = nr_seq_saida_int_w ))
	and	((a.ie_tipo_consulta 	is null) or (a.ie_tipo_consulta = ie_tipo_consulta_w))
	and	(( a.nr_seq_saida_spsadt 	is null) or( a.nr_seq_saida_spsadt = nr_seq_saida_spsadt_w ))
	and	((a.cd_area_procedimento 	is null) or (a.cd_area_procedimento = cd_area_procedimento_w))
	and	((a.nr_seq_tipo_atendimento 	is null) or (a.nr_seq_tipo_atendimento = nr_seq_tipo_atendimento_w))
	and	((a.nr_seq_saida_consulta 	is null) or (a.nr_seq_saida_consulta = nr_seq_saida_consulta_w))
	and	b.nr_sequencia	= a.nr_seq_acao_regra
	and	b.ie_situacao	= 'A'
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	order by
		nvl(a.nr_seq_clinica,0),
		nvl(a.cd_procedimento,0),
		nvl(a.cd_grupo_proc,0),
		nvl(a.cd_especialidade,0),
		nvl(a.cd_area_procedimento,0),
		nvl(a.nr_seq_saida_int,0),
		nvl(a.nr_seq_saida_spsadt,0),
		nvl(a.ie_tipo_consulta, 0),
		nvl(a.nr_seq_saida_consulta, 0),
		nvl(b.ie_tipo_internacao,'S'),
		nvl(b.ie_tipo_atendimento,'S'),
		nvl(b.ie_profissional_compl,'S'),
		nvl(b.ie_nascidos_vivos,'S'),
		nvl(b.ie_tipo_acomodacao,'S'),
		nvl(b.ie_motivo_saida_internacao,'S'),
		nvl(b.ie_motivo_saida_spsadt,'S'),
		nvl(b.ie_data_internacao,'S'),
		nvl(b.ie_regime_internacao,'S'),
		nvl(b.ie_guia_referencia,'S');

begin

if	(ie_tipo_consiste_p	= 'CC') then
	/* Obter dados do procedimento ao consistir a conta*/
	select	a.cd_procedimento,
		a.ie_origem_proced,
		nvl(b.ie_tipo_guia,0),
		nvl(b.cd_guia_referencia,'0'),
		nvl(b.qt_nasc_vivos_termo,'0'),
		nvl(b.qt_nasc_mortos,'0'),
		nvl(b.qt_nasc_vivos_prematuros,'0'),
		nvl(b.ie_regime_internacao,'0'),
		nvl(b.nr_seq_tipo_acomodacao,0),
		nvl(nr_seq_clinica,0),
		nvl(nr_seq_saida_int,0),		
		dt_entrada,
		dt_alta,
		nvl(ie_parto_cesaria,'N'),/*askono OS349266*/
		nvl(b.nr_seq_tipo_atendimento,0),
		b.nr_seq_saida_spsadt,
		b.nr_sequencia,
		b.ie_carater_internacao,
		b.nr_seq_saida_consulta,
		b.ie_tipo_consulta
	into	cd_procedimento_w,
		ie_origem_proced_w,
		ie_tipo_guia_w,
		cd_guia_referencia_w,
		qt_nasc_vivos_imp_w,
		qt_nasc_mortos_imp_w,
		qt_nasc_vivos_prematuros_imp_w,
		ie_regime_int_conta_w,
		nr_seq_tipo_acomod_w,
		nr_seq_clinica_w,
		nr_seq_saida_int_w,
		dt_entrada_w,
		dt_alta_w,
		ie_parto_cesaria_imp_w,
		nr_seq_tipo_atendimento_w,
		nr_seq_saida_spsadt_w,
		nr_seq_conta_w,
		ie_carater_internacao_w,
		nr_seq_saida_consulta_w,
		ie_tipo_consulta_w
	from	pls_conta	b,
		pls_conta_proc	a
	where	a.nr_sequencia	= nr_seq_procedimento_p
	and	a.nr_seq_conta	= b.nr_sequencia;
	
	begin
		select	max(cd_doenca),
			max(nr_declaracao_obito)
		into	cd_doenca_obito_w,
			nr_declaracao_obito_w
		from	pls_diagnost_conta_obito
		where	nr_seq_conta = nr_seq_conta_w;
	exception
	when others then
		cd_doenca_obito_w	:= null;
		nr_declaracao_obito_w	:= null;
	end;
	
	begin
		select	max(cd_doenca)
		into	cd_doenca_w
		from	pls_diagnostico_conta
		where	nr_seq_conta = nr_seq_conta_w;
	exception
	when others then
		cd_doenca_w	:= null;
	end;
	begin
		select	max(nr_decl_nasc_vivo)
		into	nr_seq_decl_nasc_vivo_w
		from	pls_diagnostico_nasc_vivo 
		where	nr_seq_conta	= nr_seq_conta_w;
	exception
	when others then
		nr_seq_decl_nasc_vivo_w := null;
	end;
	
elsif	(ie_tipo_consiste_p	= 'IA') then
	/* Obter dados do procedimento ao importar a conta*/
	select	a.cd_procedimento_imp,
		a.cd_tipo_tabela_imp,
		nvl(b.ie_tipo_guia,0),
		nvl(b.cd_guia_solic_imp,'0'),
		nvl(b.qt_nasc_vivos_imp,'0'),
		nvl(b.qt_nasc_mortos_imp,'0'),
		nvl(b.qt_nasc_vivos_prematuros_imp,'0'),
		nvl(b.ie_regime_internacao_imp,'0'),
		nvl(b.cd_tipo_acomodacao_imp,0),
		nvl(nr_seq_clinica_imp,0),
		nvl(cd_motivo_alta_imp,0),
		nvl(ie_parto_cesaria_imp,'N'),
		dt_entrada_imp,
		nvl(b.ie_tipo_atendimento_imp,0),
		b.nr_sequencia,
		b.nr_seq_protocolo,
		substr(b.ie_carater_internacao_imp,1,1),
		b.ie_tipo_consulta_imp,
		b.dt_alta_imp
	into	cd_procedimento_w,
		cd_tipo_tabela_imp_w,
		ie_tipo_guia_w,
		cd_guia_referencia_w,
		qt_nasc_vivos_imp_w,
		qt_nasc_mortos_imp_w,
		qt_nasc_vivos_prematuros_imp_w,
		ie_regime_int_conta_w,
		cd_tipo_acomocadao_imp_w,
		nr_seq_clinica_w,
		cd_motivo_alta_imp_w,
		ie_parto_cesaria_imp_w,
		dt_entrada_w,
		ie_tipo_atendimento_w,
		nr_seq_conta_w,
		nr_seq_protocolo_w,
		ie_carater_internacao_w,
		ie_tipo_consulta_w,
		dt_alta_w
	from	pls_conta	b,
		pls_conta_proc	a
	where	a.nr_sequencia	= nr_seq_procedimento_p
	and	a.nr_seq_conta	= b.nr_sequencia;
		
	if	(cd_tipo_tabela_imp_w in ('01','02','03','04','07','08')) then
		ie_origem_proced_w	:= 1; /* AMB */
	elsif	(cd_tipo_tabela_imp_w = '11') then
		ie_origem_proced_w	:= 2; /* SUS-AIH */
	elsif	(cd_tipo_tabela_imp_w = '06') then
		ie_origem_proced_w	:= 5; /* CBHPM */
	elsif	(cd_tipo_tabela_imp_w = '10') then
		ie_origem_proced_w	:= 3; /* SUS-BPA */
	elsif	(cd_tipo_tabela_imp_w in('94','95','96','97','98','99')) then
		ie_origem_proced_w	:= 4; /* PROPRIO */
	end if;
	
	select	max(cd_versao_tiss)
	into	cd_versao_tiss_w
	from	pls_protocolo_conta
	where	nr_sequencia = nr_seq_protocolo_w;

	select	max(nr_sequencia)
	into	nr_seq_tipo_acomod_w
	from	pls_tipo_acomodacao
	where	cd_tiss	= cd_tipo_acomocadao_imp_w;
	
	select	max(nr_sequencia)
	into	nr_seq_tipo_atendimento_w
	from	pls_tipo_atendimento
	where	cd_tiss			= ie_tipo_atendimento_w;
	
	select	max(nr_sequencia)
	into	nr_seq_saida_spsadt_w
	from	pls_motivo_saida_sadt
	where	cd_tiss	= cd_motivo_alta_imp_w
	and	cd_estabelecimento	= cd_estabelecimento_p;
	
	begin	
		nr_seq_saida_int_w	:= pls_obter_seq_mot_saida_tiss(cd_motivo_alta_imp_w, cd_versao_tiss_w, cd_estabelecimento_p);			
	exception
	when others then
		nr_seq_saida_int_w := nr_seq_saida_int_w;			
	end;
	
	begin
		select	max(cd_doenca_imp),
			max(nr_declaracao_obito_imp)
		into	cd_doenca_obito_w,
			nr_declaracao_obito_w
		from	pls_diagnost_conta_obito
		where	nr_seq_conta = nr_seq_conta_w;
	exception
	when others then
		cd_doenca_obito_w 	:= null;
		nr_declaracao_obito_w 	:= null;
	end;
	
	begin
		select	max(cd_doenca_imp)
		into	cd_doenca_w
		from	pls_diagnostico_conta
		where	nr_seq_conta = nr_seq_conta_w;
	exception
	when others then
		cd_doenca_w	:= null;
	end;
	
	begin
		select	max(nr_decl_nasc_vivo_imp)
		into	nr_seq_decl_nasc_vivo_w
		from	pls_diagnostico_nasc_vivo 
		where	nr_seq_conta	= nr_seq_conta_w;
	exception
	when others then
		nr_seq_decl_nasc_vivo_w := null;
	end;
end if;

/* Obter a estrutura do procedimento */
begin
select	cd_area_procedimento,
	cd_especialidade,
	cd_grupo_proc
into	cd_area_procedimento_w,
	cd_especialidade_w,	
	cd_grupo_proc_w
from	estrutura_procedimento_v
where	cd_procedimento		= cd_procedimento_w
and	ie_origem_proced	= ie_origem_proced_w;
exception
	when others then
	cd_area_procedimento_w	:= '';
	cd_especialidade_w	:= '';
	cd_grupo_proc_w		:= '';
end;
open C01;
loop
fetch C01 into	
	nr_seq_acao_regra_w,
	ie_estrutura_w;
exit when C01%notfound;
end loop;
close C01;
if	(ie_estrutura_w	= 'S') and
	(nr_seq_acao_regra_w	> 0) then
	select	ie_guia_referencia,
		ie_nascidos_vivos,
		ie_regime_internacao,
		ie_tipo_acomodacao,
		ie_tipo_internacao,
		ie_motivo_saida_internacao,
		nvl(ie_data_internacao, 'N'),
		ie_declaracao_obito,
		ie_declaracao_nasc_vivo,
		ie_cid_principal,
		ie_carater_atend,
		ie_cid_obito,
		ie_data_alta_int
	into	ie_guia_referencia_w,
		ie_nascidos_vivos_w,
		ie_regime_internacao_w,
		ie_tipo_acomodacao_w,
		ie_tipo_internacao_w,
		ie_motivo_saida_int_w,
		ie_data_internacao_w,
		ie_declaracao_obito_w,
		ie_declaracao_nasc_vivo_w,
		ie_cid_principal_w,
		ie_carater_atend_w,
		ie_cid_obito_w,
		ie_data_alta_int_w
	from	pls_acao_regra_conta
	where	nr_sequencia	= nr_seq_acao_regra_w;
	
	
	if	(ie_guia_referencia_w	= 'S') and
		(nvl(cd_guia_referencia_w,'0')	= '0') then
		ds_retorno_w	:= ds_retorno_w || 'Guia de refer�ncia n�o informada; ';
	end if;
	/*askono OS349266 - 23/08/11 -  Foi ajustado para que os campos de informa��es de nascimento  n�o sejam os "_IMP" quando for consist�ncia de conta.*/
	/*Ajustado ie_parto_cesaria_imp_w para que seja 'S' ou 'N' como � feito no atributo'*/
	if	(ie_nascidos_vivos_w	= 'S') then	
		if	((qt_nasc_vivos_imp_w 	= '0') and (qt_nasc_mortos_imp_w = '0') and (qt_nasc_vivos_prematuros_imp_w = '0') and
			(ie_parto_cesaria_imp_w = 'N')) then
			ds_retorno_w	:= ds_retorno_w || 'Quantidade de nascidos vivos ou morto n�o informado; ';
		elsif	((qt_nasc_vivos_imp_w 	= '0') and (qt_nasc_mortos_imp_w = '0') and (qt_nasc_vivos_prematuros_imp_w = '0') and
			(ie_parto_cesaria_imp_w = 'S')) then
			ds_retorno_w	:= ds_retorno_w || 'Quantidade de nascidos vivos ou morto n�o informado; ';	
		end if;	
	end if;
	
	if	(ie_regime_internacao_w	= 'S') and
		(ie_regime_int_conta_w	= '0') then
		ds_retorno_w	:= ds_retorno_w || 'Regime interna��o n�o informado; ';
	end if;
	if	(ie_tipo_acomodacao_w	= 'S') and
		(nr_seq_tipo_acomod_w	= 0) then
		ds_retorno_w	:= ds_retorno_w || 'Tipo acomoda��o n�o informado; ';
	end if;
	if	(ie_tipo_internacao_w	= 'S') and
		(nr_seq_clinica_w	= 0) then
		ds_retorno_w	:= ds_retorno_w || 'Tipo interna��o n�o informado; ';
	end if;
	if	(ie_motivo_saida_int_w	= 'S') and
		(nr_seq_saida_int_w	= 0) then
		ds_retorno_w	:= ds_retorno_w || 'Motivo sa�da interna��o n�o informado; ';
	end if;
	
	if	(ie_data_internacao_w	= 'S') and
		(dt_entrada_w is null) then
		ds_retorno_w	:= ds_retorno_w || 'Data/hora interna��o n�o informado; ';
	end if;

	if	(ie_data_alta_int_w	= 'S') and
		(dt_alta_w is null) then
		ds_retorno_w	:= ds_retorno_w || 'Data/hora alta interna��o n�o informado; ';
	end if;
	
	if	(ie_declaracao_obito_w = 'S') and
		(nvl(nr_declaracao_obito_w,'X') = 'X') then
		ds_retorno_w	:= ds_retorno_w || 'Declara��o de �bito n�o informado; ';
	end if;
	
	if	(ie_declaracao_nasc_vivo_w = 'S') and
		(nvl(nr_seq_decl_nasc_vivo_w,'X') = 'X') then
		ds_retorno_w	:= ds_retorno_w || 'Declara��o de nascido vivo n�o informado; ';
	end if;
	
	if	(ie_cid_principal_w 	= 'S') and
		(nvl(cd_doenca_w,'X') = 'X') then
		ds_retorno_w	:= ds_retorno_w || 'CID n�o informado; ';
	end if;
	
	if	(ie_carater_atend_w	= 'S') and
		(nvl(ie_carater_internacao_w,'X') = 'X') then
		ds_retorno_w	:= ds_retorno_w || 'Carater de atendimento n�o informado; ';
	end if;
	
	if	(ie_cid_obito_w		= 'S') and
		(nvl(cd_doenca_obito_w,'X') = 'X') then
		ds_retorno_w	:= ds_retorno_w || 'CID �bito n�o informado; ';
	end if;
	nr_seq_acao_regra_p	:= nr_seq_acao_regra_w;
else
	nr_seq_acao_regra_p	:= 0;
	
end if;

ds_retorno_p	:= ds_retorno_w;

end pls_obter_acao_regra_conta;
/