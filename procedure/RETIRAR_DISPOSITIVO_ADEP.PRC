create or replace procedure Retirar_dispositivo_adep (	nr_seq_acao_disp_p		number,
										nr_seq_dispositivo_p	number,
										ie_opcao_disp_p			Varchar2,
										dt_registro_p			date,
										nr_seq_motivo_ret_p		number,
										nr_seq_registro_p		number,
										nr_atendimento_p		number,
										nm_usuario_p			Varchar2,
										ds_retirada_p varchar2) is 

ie_acao_w				varchar2(15);
nr_seq_disp_proc_w		number(10);
ds_hint_w				varchar2(4000);
ds_dispositivo_w		varchar2(255);
cd_pessoa_fisica_w		varchar2(10);
ie_gerar_evolucao_w		varchar2(10);
cd_evolucao_w			number(10);
ds_texto_aux_w			varchar2(50);

begin

select	Obter_Dados_Usuario_Opcao(nm_usuario_p,'C')
into	cd_pessoa_fisica_w
from	dual;

ie_acao_w	:= obter_acao_procedimento(nr_seq_acao_disp_p, nr_seq_dispositivo_p,nvl(ie_opcao_disp_p,'P'));

update	atend_pac_dispositivo
set		dt_retirada			=	dt_registro_p,
		nr_seq_motivo_ret	=	nr_seq_motivo_ret_p,
		ie_acao				=	nvl(ie_acao_w, ie_acao),
		dt_atualizacao		=	sysdate,
		nm_usuario			=	nm_usuario_p
where	nr_sequencia		=	nr_seq_registro_p;

Select	atend_pac_disp_proc_seq.nextval
into	nr_seq_disp_proc_w 
from	dual;

insert into atend_pac_disp_proc(
	nr_sequencia,
	nr_seq_disp_pac,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_proc_interno,
	dt_procedimento,
	cd_pessoa_fisica,
	cd_setor_atendimento,
	ds_observacao)
values(
	nr_seq_disp_proc_w,
	nr_seq_registro_p,
	sysdate, 
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_acao_disp_p,
	dt_registro_p,
	cd_pessoa_fisica_w,
	campo_numerico(Obter_Unidade_Atendimento(nr_atendimento_p,'A','CS')),
	ds_retirada_p);
	
ADEP_exec_lancto_dispositivo(nr_atendimento_p,nr_seq_disp_proc_w ,nm_usuario_p);

ADEP_cobranca_dispositivo(nr_atendimento_p,nr_seq_dispositivo_p,nr_seq_disp_proc_w ,nm_usuario_p,'R',ie_acao_w, nr_seq_registro_p);

select	nvl(max(ie_gerar_evolucao),'S')
into	ie_gerar_evolucao_w
from	DISPOSITIVO
where	nr_sequencia	= nr_seq_dispositivo_p;

ds_texto_aux_w := wheb_mensagem_pck.get_texto(300586);

select	nvl(substr(obter_descricao_padrao('DISPOSITIVO','DS_DISPOSITIVO',max(a.nr_seq_dispositivo)),1,80), ds_texto_aux_w) ds_dispositivo,
		substr(obter_hint_graf_niss_adep(max(a.nr_sequencia)),1,4000) ds_hint
into	ds_dispositivo_w,
		ds_hint_w
from	atend_pac_dispositivo a
where	obter_se_exibe_graf_disp(a.nr_sequencia) = 'S'
and		a.nr_atendimento = nr_atendimento_p
and		a.nr_sequencia 	= nr_seq_registro_p;

ds_hint_w	:= substr(ds_hint_w || chr(13) || wheb_mensagem_pck.get_texto(300588, 'DT='||PKG_DATE_FORMATERS.TO_VARCHAR(dt_registro_p, 'timestamp', WHEB_USUARIO_PCK.GET_CD_ESTABELECIMENTO, WHEB_USUARIO_PCK.GET_NM_USUARIO)||';NOME='||substr(obter_nome_pf(cd_pessoa_fisica_w),1,80)) ,1,4000);
			
if	(ie_gerar_evolucao_w = 'S') then
	Gerar_evolPaci_automa('DISP',nm_usuario_p,nr_atendimento_p, ds_dispositivo_w, null,'D',ds_hint_w, dt_registro_p,null,null,null,null,cd_evolucao_w);
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Retirar_dispositivo_adep;
/