create or replace
procedure pat_duplicar_bem(	qt_copias_p		number,
			nr_seq_bem_p		Number,
			dt_aquisicao_p		Date,
			vl_original_p		Number,
			cd_moeda_p		Number,
			nm_usuario_p		Varchar2,
			ie_duplicar_equipe_p	Varchar2,
			nr_seq_duplicado_p	out varchar2) is

cd_imobilizado_w	pat_bem.cd_bem%type;
nr_seq_duplicado_w	varchar2(254);
pat_bem_w		pat_bem%rowtype;
begin

select	a.*
into	pat_bem_w
from	pat_bem a
where	a.nr_sequencia	= nr_seq_bem_p;

cd_imobilizado_w		:= pat_bem_w.cd_bem;

pat_bem_w.ds_bem		:= substr(Wheb_mensagem_pck.get_texto(795076) || ' ' || pat_bem_w.ds_bem,1,255);
pat_bem_w.nm_usuario	 	:= nm_usuario_p;
pat_bem_w.nm_usuario_nrec 	:= nm_usuario_p;
pat_bem_w.ie_situacao		:= 'A';
pat_bem_w.dt_aquisicao		:= nvl(dt_aquisicao_p, pat_bem_w.dt_aquisicao);
pat_bem_w.dt_inicio_uso		:= nvl(dt_aquisicao_p, pat_bem_w.dt_inicio_uso);
pat_bem_w.vl_original		:= nvl(vl_original_p, pat_bem_w.vl_original);
pat_bem_w.cd_moeda		:= nvl(cd_moeda_p, pat_bem_w.cd_moeda);
pat_bem_w.nr_lote_contabil	:= 0;

for i in 1..nvl(qt_copias_p,1) LOOP
	begin

	pat_bem_w.cd_bem := null;
	pat_bem_w.nr_sequencia := null;
	philips_patrimonio_pck.gerar_pat_bem('N', nm_usuario_p, pat_bem_w);

	if	(ie_duplicar_equipe_p = 'S') then
		begin

		insert into	man_equipamento(
				nr_sequencia,
				ds_equipamento,
				nr_seq_local,
				nr_seq_tipo_equip,
				dt_atualizacao,
				nm_usuario,
				nr_seq_planej,
				nr_seq_trab,
				ie_situacao,
				cd_imobilizado,
				cd_impacto,
				cd_estab_contabil,
				cd_centro_custo,
				ie_voltagem,
				dt_ano_fabricacao,
				ds_marca,
				ds_modelo,
				qt_peso,
				nr_seq_fabricante,
				cd_nacionalidade,
				cd_fornecedor,
				dt_aquisicao,
				vl_aquisicao,
				cd_moeda,
				dt_inicio_garantia,
				dt_fim_garantia,
				nr_doc_garantia,
				ds_observacao,
				nr_serie,
				ie_disponibilidade,
				nr_seq_contador,
				ie_rotina_seguranca,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_propriedade,
				cd_cgc_terc,
				vl_custo_sem_utilizacao,
				ie_parado,
				nr_seq_superior,
				ie_controle_setor,
				nr_seq_categoria,
				nr_seq_status,
				cd_pessoa_terceiro,
				ie_tensao_ac,
				qt_corrente_ac,
				qt_potencia_ac,
				ie_tensao_dc,
				qt_corrente_dc,
				qt_potencia_dc,
				nr_seq_marca,
				nr_seq_modelo,
				ds_padrao_os,
				qt_largura,
				qt_comprimento,
				qt_altura,
				nr_seq_bem,
				ie_tensao_dc_saida,
				ie_tensao_ac_saida,
				qt_corrente_ac_saida,
				qt_corrente_dc_saida,
				qt_potencia_ac_saida,
				qt_potencia_dc_saida,
				ds_observacao_tensao,
				ie_consiste_os_duplic,
				ie_tipo_ordem,
				nr_seq_tipo_ordem,
				ie_classificacao_os)
			select	man_equipamento_seq.NextVal,
				ds_equipamento,
				nr_seq_local,
				nr_seq_tipo_equip,
				sysdate,
				nm_usuario_p,
				nr_seq_planej,
				nr_seq_trab,
				'A',
				pat_bem_w.cd_bem,
				cd_impacto,
				cd_estab_contabil,
				cd_centro_custo,
				ie_voltagem,
				dt_ano_fabricacao,
				ds_marca,
				ds_modelo,
				qt_peso,
				nr_seq_fabricante,
				cd_nacionalidade,
				cd_fornecedor,
				dt_aquisicao,
				vl_aquisicao,
				cd_moeda,
				dt_inicio_garantia,
				dt_fim_garantia,
				nr_doc_garantia,
				ds_observacao,
				nr_serie,
				ie_disponibilidade,
				nr_seq_contador,
				ie_rotina_seguranca,
				sysdate,
				nm_usuario_p,
				ie_propriedade,
				cd_cgc_terc,
				vl_custo_sem_utilizacao,
				ie_parado,
				nr_seq_superior,
				ie_controle_setor,
				nr_seq_categoria,
				nr_seq_status,
				cd_pessoa_terceiro,
				ie_tensao_ac,
				qt_corrente_ac,
				qt_potencia_ac,
				ie_tensao_dc,
				qt_corrente_dc,
				qt_potencia_dc,
				nr_seq_marca,
				nr_seq_modelo,
				ds_padrao_os,
				qt_largura,
				qt_comprimento,
				qt_altura,
				pat_bem_w.nr_sequencia,
				ie_tensao_dc_saida,
				ie_tensao_ac_saida,
				qt_corrente_ac_saida,
				qt_corrente_dc_saida,
				qt_potencia_ac_saida,
				qt_potencia_dc_saida,
				ds_observacao_tensao,
				nvl(ie_consiste_os_duplic,'N'),
				ie_tipo_ordem,
				nr_seq_tipo_ordem,
				ie_classificacao_os
			from	man_equipamento
			where	cd_imobilizado = cd_imobilizado_w;

		end;
	end if;

	if	(i = 1) then
		nr_seq_duplicado_w	:= substr(pat_bem_w.nr_sequencia,1,254);
	else
		nr_seq_duplicado_w	:= substr(nr_seq_duplicado_w || ',' || pat_bem_w.nr_sequencia,1,254);
	end if;
	end;
	END LOOP;

commit;

nr_seq_duplicado_p := nr_seq_duplicado_w;

end pat_duplicar_bem;
/
