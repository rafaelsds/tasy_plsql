create or replace
procedure desfazer_liberacao_glosa(		nm_tabela_p		varchar2,
					nr_sequencia_p		number,
					nm_usuario_p		varchar2) is 

ds_sep_bv_w	varchar2(255);
ds_parametros_w	varchar2(255);
ds_comando_w	varchar2(4000);
begin

ds_sep_bv_w	:= obter_separador_bv;
ds_comando_w	:=	' update '|| nm_tabela_p ||
			'	set	ie_status = ''A'', '			||
			'        		vl_liberado = 0, '			||
			'		dt_liberacao = null, '			||	
			'		nr_seq_ret_glosa = null, '		||
			'		nr_seq_item_retorno = null, '		||
			'		nm_usuario = :nm_usuario, '		||
			'		dt_atualizacao = sysdate '		||
			'	where	nr_sequencia = :nr_sequencia '	||
			'	and	ie_status in(''G'') '			||
			'	and	nr_repasse_terceiro is null ';
			
ds_parametros_w		:= 	'nr_sequencia=' || nr_sequencia_p || ds_sep_bv_w ||
				'nm_usuario=' || nm_usuario_p || ds_sep_bv_w;

exec_sql_dinamico_bv(	'DESFAZER_LIBERACAO_GLOSA',
			ds_comando_w,
			ds_parametros_w);

commit;
end desfazer_liberacao_glosa;
/