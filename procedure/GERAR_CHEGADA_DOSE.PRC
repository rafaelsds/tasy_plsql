CREATE OR REPLACE
PROCEDURE Gerar_chegada_dose(
				dt_referencia_p		Date,
				nr_prescricao_p		Number,
				nm_usuario_p		Varchar2) IS

qt_contador_w	Number(10);

BEGIN

select	count(*)
into	qt_contador_w
from	can_ordem_prod
where	nr_prescricao		= nr_prescricao_p
and	trunc(dt_prevista)	= trunc(dt_referencia_p)
and	dt_entrega_setor is null;

if	(qt_contador_w = 0) then
	update	paciente_atendimento
	set	dt_dispensacao_medic	= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_prescricao		= nr_prescricao_p
	and	trunc(dt_prevista)	= trunc(dt_referencia_p);
end if;

commit;
end Gerar_chegada_dose; 
/
