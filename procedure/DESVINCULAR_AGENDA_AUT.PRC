create or replace
procedure desvincular_agenda_aut(
			nr_sequencia_p		number) is 

nr_seq_agenda_p	number(10);
ds_historico_w	varchar2(4000):= '';

begin

select 	nvl(max(nr_seq_agenda),0)
into 	nr_seq_agenda_p
from 	autorizacao_convenio
where	nr_sequencia   = nr_sequencia_p; 

if (nr_seq_agenda_p > 0)then
	update	agenda_paciente 
	set	ie_autorizacao  	= null,
		nm_usuario	= wheb_usuario_pck.get_nm_usuario,
		dt_atualizacao  	= sysdate
	where	nr_sequencia    	= nr_seq_agenda_p;  
	
	ds_historico_w := 'Campo: NR_SEQ_AGENDA - Antes: '||nr_seq_agenda_p||' Depois: NULL'||chr(13)||chr(10);
	gravar_autor_conv_log_alter(nr_sequencia_p,Wheb_mensagem_pck.get_texto(1044707),substr(ds_historico_w,1,2000),wheb_usuario_pck.get_nm_usuario);
end if;

update	autorizacao_convenio 
set	nr_seq_agenda  	= null,
	nm_usuario	= wheb_usuario_pck.get_nm_usuario,
	dt_atualizacao	= sysdate
where	nr_sequencia   	= nr_sequencia_p; 

commit;

end desvincular_agenda_aut;
/