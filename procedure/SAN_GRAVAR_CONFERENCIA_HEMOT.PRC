create or replace
procedure san_gravar_conferencia_hemot(
		nr_seq_doacao_p		number,
		nm_usuario_pri_conf_p	varchar2,
		nm_usuario_seg_conf_p	varchar2) is 

begin

if	(nm_usuario_pri_conf_p is not null) then
	san_gravar_conferencia(nr_seq_doacao_p, nm_usuario_pri_conf_p);
end if;

if	(nm_usuario_seg_conf_p is not null) then
	san_gravar_segunda_conferencia(nr_seq_doacao_p, nm_usuario_seg_conf_p);
end if;

end san_gravar_conferencia_hemot;
/