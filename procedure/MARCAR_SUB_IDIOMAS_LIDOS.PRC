create or replace
procedure marcar_sub_idiomas_lidos( ie_doc_resp_p		varchar2,
				    nr_seq_doc_p		number,
				    nm_usuario_p		varchar2 ) is 

nr_seq_doc_w		number(10);
nr_seq_idioma_w		number(10);
nr_seq_superior_w	number(10);

cursor c01 is
	select	nr_sequencia
	from	qua_documento
	where	((nr_sequencia = nr_seq_superior_w) and (nr_seq_superior is null))
	or	((nr_seq_superior = nr_seq_superior_w) or ((nr_seq_superior_w is null) and (nr_seq_superior = nr_seq_doc_p)))
	and	(nr_sequencia <> nr_seq_doc_p);

begin
	select	max(nr_seq_idioma),
		max(nr_seq_superior)
	into	nr_seq_idioma_w,
		nr_seq_superior_w
	from	qua_documento
	where	nr_sequencia = nr_seq_doc_p;

	if (nr_seq_idioma_w = philips_param_pck.get_nr_seq_idioma) then
		open c01;
		loop
		fetch c01 into	
			nr_seq_doc_w;
		exit when c01%notfound;
			begin			
				update	qua_doc_log_acesso
				set	dt_fim_leitura = sysdate,
					dt_leitura = sysdate,
					dt_atualizacao = sysdate,
					ie_doc_resp = ie_doc_resp_p
				where	nr_seq_doc = nr_seq_doc_w
				and	nm_usuario = nm_usuario_p;
			end;
		end loop;
		close c01;
	end if;

commit;

end marcar_sub_idiomas_lidos;
/
