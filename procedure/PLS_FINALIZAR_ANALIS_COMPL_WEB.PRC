create or replace
procedure pls_finalizar_analis_compl_web(nr_seq_analise_p		number,
										 nr_seq_requisicao_p	number,
										 ds_historico_p			varchar2,
										 nm_usuario_p			varchar2) is 

begin

update pls_auditoria
 set   ie_status = 'A',
	   dt_atualizacao = sysdate,
	   nm_usuario = nm_usuario_p
 where nr_sequencia = nr_seq_analise_p;
 
 --Gerar hist�rico, informando que a requisi��o n�o est� mais sendo enviada para complemento de OPME
 pls_requisicao_gravar_hist(nr_seq_requisicao_p, 'RCO', ds_historico_p,
							null,  nm_usuario_p);


commit;

end pls_finalizar_analis_compl_web;
/