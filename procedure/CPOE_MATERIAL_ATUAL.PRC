create or replace trigger cpoe_material_atual
before insert or update on cpoe_material
for each row

declare

begin
begin
    if (:new.hr_min_aplicacao is not null) and ((:new.hr_min_aplicacao <> :old.hr_min_aplicacao) or (:old.dt_min_aplicacao is null)) then
		:new.dt_min_aplicacao := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_min_aplicacao,'dd/mm/yyyy hh24:mi');
	end if;	
    if (:new.hr_min_aplic_adic is not null) and ((:new.hr_min_aplic_adic <> :old.hr_min_aplic_adic) or (:old.dt_min_aplic_adic is null)) then	
		:new.dt_min_aplic_adic := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_min_aplic_adic,'dd/mm/yyyy hh24:mi');
	end if;	
	if (:new.hr_min_aplic_ataque is not null) and ((:new.hr_min_aplic_ataque <> :old.hr_min_aplic_ataque) or (:old.dt_min_aplic_ataque is null)) then
		:new.dt_min_aplic_ataque := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_min_aplic_ataque,'dd/mm/yyyy hh24:mi');
	end if;		
    if (:new.hr_prim_horario is not null) and ((:new.hr_prim_horario <> :old.hr_prim_horario) or (:old.dt_prim_horario is null)) then
		:new.dt_prim_horario := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_horario,'dd/mm/yyyy hh24:mi');
	end if;			
exception
	when others then
	null;
end;

end;
/