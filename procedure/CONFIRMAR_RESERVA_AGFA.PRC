CREATE OR REPLACE 
procedure Confirmar_Reserva_AGFA(
			nr_seq_ageint_p	Number,
			nm_usuario_p	Varchar2,
			cd_estabelecimento_p	number) is

ind			Number(5)	:= 1;
ds_horario_w		Varchar2(255);
ds_sql_w			Varchar2(4000);
ds_sep_bv_w		Varchar2(50);
ds_parametros_w		Varchar2(4000);
cd_tipo_agenda_w	Number(10);
cd_pessoa_fisica_w	Varchar2(10);
nm_contato_w		Varchar2(80);
nr_telefone_w		Varchar2(60);
cd_convenio_w		Number(5);
cd_Categoria_w		Varchar2(10);
cd_plano_w		Varchar2(10);
nr_doc_convenio_w	Varchar2(20);
cd_usuario_convenio_w	Varchar2(30);
dt_validade_carteira_w	Date;
nr_seq_agenda_w		Number(10);
cd_agenda_w		Number(10);
nr_sequencia_w		Number(10);
nr_status_agendado_w	Number(10);
nr_seq_ageint_item_w	Number(10);
cd_medico_w		Varchar2(10);
nr_seq_proc_interno_w	Number(10);
cd_estabelecimento_w	Number(4);
cd_procedimento_w	Number(15);
ie_origem_proced_w	Number(10);
nr_minuto_duracao_w	Number(10);
ie_encaixe_w		Varchar2(1);
hr_agenda_w		Date;
nr_Seq_encaixe_w		Number(10);
cd_medico_marcacao_w	Varchar2(10);
nr_seq_proc_int_assoc_w	Number(10);
cd_proc_assoc_w		Number(15);
ie_origem_proc_assoc_w	Number(10);
nr_seq_proc_Adic_w	Number(10)	:= 0;
ie_classif_agenda_w	Varchar2(5);
ds_observacao_w		Varchar2(2000);
nr_seq_exame_adic_w	number(10);
cd_proc_adic_w		number(15);
ie_origem_proc_adic_w	number(10);
nm_paciente_w		varchar2(60);
qt_peso_w		number(10,3);
qt_altura_cm_w		number(5,2);
dt_nascimento_w		date;
ie_exige_check_list_w	varchar2(1);
ie_exige_avaliacao_w	varchar2(1);
nr_seq_tipo_aval_w	number(10,0);
qt_dias_confirm_aut_w	number(10,0);
dt_entrega_prevista_w	date;
nr_seq_classif_int_w	number(10,0);
cd_convenio_ageint_w	number(5);
cd_Categoria_ageint_w	varchar2(10);
cd_plano_ageint_w	varchar2(10);
cd_conv_item_w		number(5);
cd_categ_item_w		varchar2(10);
cd_plano_item_w		varchar2(10);
dt_inicio_agendamento_w	Date;
dt_fim_agendamento_w 	Date;
ie_tipo_atendimento_w	number(3);
ie_lado_w		varchar2(1);
ds_autorizacao_w	Varchar2(30);
ds_observacao_ww	Varchar2(400);
dt_agenda_w		date;
cd_profissional_w	varchar2(10);
cd_estab_agenda_w	number(4,0);
nm_usuario_confirm_encaixe_w 	varchar2(15);
dt_confirm_encaixe_w		date;
nr_seq_tipo_classif_pac_w	number(10,0);
nm_medico_externo_w		varchar2(60);
crm_medico_externo_w		varchar2(60);
nm_medico_externo_item_w	varchar2(60);      
crm_medico_externo_item_w	varchar2(60);
qt_diaria_prev_w		number(3,0);
cd_medico_solicitante_w		agenda_integrada.cd_medico_solicitante%type;
cd_medico_req_w			agenda_integrada_item.cd_medico_req%type;

qt_idade_w		number(3);
ds_obs_item_w		varchar2(255);	


Cursor C01 is
	select	nr_seq_agenda,
		cd_agenda,
		nr_sequencia,
		nr_seq_ageint_item,
		nr_minuto_duracao,
		nvl(ie_encaixe, 'N'),
		hr_agenda,
		cd_pessoa_fisica,
		nm_usuario_confirm_encaixe,
		dt_confirm_encaixe 
	from	ageint_marcacao_usuario
	where	nm_usuario		= nm_usuario_p
	and	nr_seq_ageint		= nr_seq_ageint_p
	and	nvl(ie_gerado,'N')	= 'S'
	and	nvl(ie_horario_auxiliar,'N') = 'N';

begin

select	cd_pessoa_fisica,
	nm_contato,
	nr_telefone,
	cd_convenio,
	cd_Categoria,
	cd_plano,
	nr_doc_convenio,
	cd_usuario_convenio,
	dt_validade_carteira,
	cd_estabelecimento,
	ds_observacao,
	nm_paciente,
	qt_peso,
	qt_altura_cm,
	dt_nascimento,
	dt_inicio_agendamento,
	dt_fim_agendamento,
	ie_tipo_atendimento,
	nr_seq_tipo_classif_pac,
	nm_medico_externo,      
	crm_medico_externo,
	cd_medico_solicitante
into	cd_pessoa_fisica_w,
	nm_contato_w,
	nr_telefone_w,
	cd_convenio_ageint_w,
	cd_Categoria_ageint_w,
	cd_plano_ageint_w,
	nr_doc_convenio_w,
	cd_usuario_convenio_w,
	dt_validade_carteira_w,
	cd_estabelecimento_w,
	ds_observacao_w,
	nm_paciente_w,
	qt_peso_w,
	qt_altura_cm_w,
	dt_nascimento_w,
	dt_inicio_agendamento_w,
	dt_fim_agendamento_w,
	ie_tipo_atendimento_w,
	nr_seq_tipo_classif_pac_w,
	nm_medico_externo_w,      
	crm_medico_externo_w,
	cd_medico_solicitante_w
from	agenda_integrada
where	nr_sequencia	= nr_seq_Ageint_p;

qt_idade_w	:= obter_idade(dt_nascimento_w, sysdate, 'A');

cd_convenio_w	:= cd_convenio_ageint_w;
cd_categoria_w	:= cd_categoria_ageint_w;
cd_plano_w	:= cd_plano_ageint_w;


open C01;
loop
fetch C01 into
	nr_seq_agenda_w,
	cd_agenda_w,
	nr_sequencia_w,
	nr_seq_ageint_item_w,
	nr_minuto_duracao_w,
	ie_encaixe_w,
	hr_agenda_w,
	cd_medico_marcacao_w,
	nm_usuario_confirm_encaixe_w,
	dt_confirm_encaixe_w;
exit when C01%notfound;
	begin
	
	select	max(cd_convenio),
		max(cd_categoria),
		max(cd_plano)
	into	cd_conv_item_w,
		cd_categ_item_w,
		cd_plano_item_w
	from	agenda_integrada_conv_item
	where	nr_seq_agenda_item	= nr_seq_ageint_item_w;

	if	(cd_conv_item_w is not null) then
		cd_convenio_w	:= cd_conv_item_w;
		cd_categoria_w	:= cd_categ_item_w;
		cd_plano_w	:= cd_plano_item_w;
	end if;

	select 	max(cd_tipo_Agenda),
		max(nr_seq_classif_int),
		max(cd_estabelecimento)
	into	cd_tipo_Agenda_w,
		nr_seq_classif_int_w,
		cd_estab_agenda_w
	from	agenda
	where	cd_agenda	= cd_Agenda_w;
	
	select	nr_Seq_proc_interno,
		cd_medico,
		ie_classif_agenda,
		ie_lado,
		crm_medico_externo,
		nm_medico_externo,
		ds_observacao,
		qt_diaria_prev,
		cd_medico_req
	into	nr_seq_proc_interno_w,
		cd_medico_w,
		ie_classif_agenda_w,
		ie_lado_w,
		crm_medico_externo_item_w,
		nm_medico_externo_item_w,
		ds_obs_item_w,
		qt_diaria_prev_w,
		cd_medico_req_w
	from	agenda_integrada_item
	where	nr_Sequencia	= nr_seq_ageint_item_w;

	if	(ie_encaixe_w	= 'N') then
		if	(nr_Seq_proc_interno_w is not null) then
			obter_proc_tab_interno_conv(
					nr_seq_proc_interno_w,
					cd_estabelecimento_w,
					cd_convenio_w,
					cd_categoria_w,
					cd_plano_w,
					null,
					cd_procedimento_w,
					ie_origem_proced_w, 
					null,
					sysdate,
					null,
					null,
					null,
					null,
					null,
					null,
					null,
					null);
		end if;
		if	(cd_tipo_agenda_w	= 2) then
			
			select  NVL(MAX(ie_autorizacao),null),
				SUBSTR(MAX(ds_observacao),1,400),
				MAX(hr_inicio)
			into	ds_autorizacao_w,
				ds_observacao_ww,
				dt_agenda_w
			from	agenda_paciente
			where	nr_sequencia = nr_seq_agenda_w;

			Gravar_Agfa_Siu_In(cd_pessoa_fisica_w,
						dt_inicio_agendamento_w,
						dt_fim_agendamento_w,
						nm_usuario_p,
						cd_estabelecimento_p,
						null,
						nr_seq_agenda_w,
						cd_medico_marcacao_w,
						nr_seq_proc_interno_w,
						sysdate,
						nr_minuto_duracao_w,
						ie_lado_w,
						cd_agenda_w,
						'S12',
						ds_autorizacao_w,
						ds_observacao_ww,
						dt_agenda_w,
						nvl(cd_medico_solicitante_w,cd_medico_req_w));

		elsif	(cd_tipo_agenda_w 	= 3) then
			
			select  NVL(MAX(ie_autorizacao),null),
				SUBSTR(MAX(ds_observacao),1,400),
				MAX(dt_agenda)
			into	ds_autorizacao_w,
				ds_observacao_ww,
				dt_agenda_w
			from	agenda_consulta
			where	nr_sequencia = nr_seq_agenda_w;

			Gravar_Agfa_Siu_In(cd_pessoa_fisica_w,
						dt_inicio_agendamento_w,
						dt_fim_agendamento_w,
						nm_usuario_p,
						cd_estabelecimento_p,
						nr_seq_agenda_w,
						null,
						cd_medico_w,
						null,
						sysdate,
						nr_minuto_duracao_w,
						ie_lado_w,
						cd_agenda_w,
						'S12',
						ds_autorizacao_w,
						ds_observacao_ww,
						dt_agenda_w,
						nvl(cd_medico_solicitante_w,cd_medico_req_w));

		elsif	(cd_tipo_Agenda_w	= 5) then
			
			select  NVL(MAX(ie_autorizacao),null),
				SUBSTR(MAX(ds_observacao),1,400),
				MAX(dt_agenda)
			into	ds_autorizacao_w,
				ds_observacao_ww,
				dt_agenda_w
			from	agenda_consulta
			where	nr_sequencia = nr_seq_agenda_w;

			Gravar_Agfa_Siu_In(cd_pessoa_fisica_w,
						dt_inicio_agendamento_w,
						dt_fim_agendamento_w,
						nm_usuario_p,
						cd_estabelecimento_p,
						nr_seq_agenda_w,
						null,
						cd_medico_w,
						nr_seq_proc_interno_w,
						sysdate,
						nr_minuto_duracao_w,
						ie_lado_w,
						cd_agenda_w,
						'S12',
						ds_autorizacao_w,
						ds_observacao_ww,
						dt_agenda_w,
						nvl(cd_medico_solicitante_w,cd_medico_req_w));
		end if;
	elsif	(ie_encaixe_w	= 'S') then
		
		if	(cd_tipo_agenda_w	= 2) then		
			select  NVL(MAX(ie_autorizacao),null),
				SUBSTR(MAX(ds_observacao),1,400),
				MAX(hr_inicio)
			into	ds_autorizacao_w,
				ds_observacao_ww,
				dt_agenda_w
			from	agenda_paciente
			where	nr_sequencia = nr_seq_encaixe_w;

			Gravar_Agfa_Siu_In(cd_pessoa_fisica_w,
						dt_inicio_agendamento_w,
						dt_fim_agendamento_w,
						nm_usuario_p,
						cd_estabelecimento_p,
						null,
						nr_seq_encaixe_w,
						cd_medico_marcacao_w,
						nr_seq_proc_interno_w,
						sysdate,
						nr_minuto_duracao_w,
						ie_lado_w,
						cd_agenda_w,
						'S12',
						ds_autorizacao_w,
						ds_observacao_ww,
						dt_Agenda_w,
						nvl(cd_medico_solicitante_w,cd_medico_req_w));
		else
			select  NVL(MAX(ie_autorizacao),null),
				SUBSTR(MAX(ds_observacao),1,400),
				MAX(dt_agenda)
			into	ds_autorizacao_w,
				ds_observacao_ww,
				dt_agenda_w
			from	agenda_consulta
			where	nr_sequencia = nr_seq_encaixe_w;

			Gravar_Agfa_Siu_In(cd_pessoa_fisica_w,
						dt_inicio_agendamento_w,
						dt_fim_agendamento_w,
						nm_usuario_p,
						cd_estabelecimento_p,
						nr_seq_encaixe_w,
						null,
						cd_medico_marcacao_w,
						null,
						sysdate,
						nr_minuto_duracao_w,
						ie_lado_w,
						cd_agenda_w,
						'S12',
						ds_autorizacao_w,
						ds_observacao_ww,
						dt_agenda_w,
						nvl(cd_medico_solicitante_w,cd_medico_req_w));
		end if;
	
	end if;

	cd_convenio_w	:= cd_convenio_ageint_w;
	cd_categoria_w	:= cd_categoria_ageint_w;
	cd_plano_w	:= cd_plano_ageint_w;
	end;
end loop;
close C01;

commit;

end Confirmar_Reserva_AGFA;
/
