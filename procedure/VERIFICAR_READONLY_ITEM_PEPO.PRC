create or replace
procedure verificar_readonly_item_pepo(nm_usuario_p		varchar2,
						cd_estabelecimento_p			number,
						ie_nivel_cirurgia_p				varchar2,
						ds_item_p						varchar2,
						cd_perfil_p						number,
						nr_cirurgia_p					number,
						nr_atendimento_p				number,
						nr_seq_pepo_p					number,
						ie_atualizar_p				out varchar2,
						nr_seq_perfil_pepo_p 		out number,
						ie_somente_template_p		out varchar2,
						dt_liberacao_p				out varchar2,
						dt_liberacao_anestesista_p	out varchar2,
						qt_cirurgia_p				out number,
						ie_dt_atendimento_p			out varchar2,
						ie_senha_liberacao_p		out varchar2,
						ie_imprimir_liberar_p		out varchar2,
						ie_forma_liberar_p			out varchar2,
						ie_quest_texto_padrao_p     out varchar2) is
					
ie_atualizar_w				varchar2(1) := 'N';
nr_seq_perfil_pepo_w		number(10) 	:= 0;
ie_somente_template_w		varchar2(1)	:= 'N';
ie_estrutura_pepo_w			varchar2(1)	:= 'N';
ie_perm_real_w				varchar2(1)	:= '';
dt_liberacao_w				date		:= null;
dt_liberacao_anestesista_w 	date		:= null;
qt_cirurgia_w				number(1)	:= 0;
ie_permite_inserir_info_w	varchar2(1)	:= '';
dt_atendimento_w			date		:= null;
qt_min_apos_alta_w			number(10)	:= 0;
ie_dt_atendimento_w			varchar2(1)	:= 'N';
ie_senha_liberacao_w		varchar2(1)	:= 'N';
ie_imprimir_liberar_w		varchar2(1)	:= 'N';
ie_forma_liberar_w			varchar2(1)	:= 'Q';
ie_quest_texto_padrao_w		varchar2(1)	:= 'N';

begin
ie_estrutura_pepo_w 		:= obter_valor_param_usuario(872, 158, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p);
ie_perm_real_w 				:= obter_valor_param_usuario(872, 119, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p);
ie_permite_inserir_info_w	:= obter_valor_param_usuario(872, 90, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p);
qt_min_apos_alta_w			:= to_number(obter_valor_param_usuario(872, 131, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p));

	select	nvl(max(b.ie_somente_template),'N'),
		max(b.nr_sequencia), 
		max(obter_se_usuario_item(b.nr_sequencia, nm_usuario_p)),
		nvl(max(b.ie_senha_liberacao),'N') ie_senha_liberacao,
		nvl(max(b.ie_forma_liberar),'Q') ie_forma_liberar,
		nvl(max(b.ie_imprimir_liberar),'N') ie_imprimir_liberar,
		NVL(MAX(b.ie_quest_texto_padrao), 'N')  ie_quest_texto_padrao
	into 	ie_somente_template_w,
		nr_seq_perfil_pepo_w,
		ie_atualizar_w,
		ie_senha_liberacao_w,
		ie_forma_liberar_w,
		ie_imprimir_liberar_w,
		ie_quest_texto_padrao_w
	from    pepo_item a,
		perfil_item_pepo b,
		perfil_item_pepo_usuario c
	where   a.nr_sequencia	= b.nr_seq_item_pepo
	and     b.nr_sequencia	= c.nr_seq_perfil_item(+)
	and     b.cd_perfil	= cd_perfil_p
	--and	a.nr_sequencia	= nr_sequencia_p
	--and	nvl(b.ds_item_instituicao,nvl(a.ds_item_instituicao,a.ds_item)) = ds_item_p
	and	upper(nvl(b.ds_item_instituicao,nvl(a.ds_item_instituicao,substr(obter_desc_expressao(cd_exp_desc_item,a.ds_item),1,255)))) = upper(ds_item_p)
	order by a.nr_sequencia;
	
	if	not ((ie_estrutura_pepo_w = 'S') and (ie_nivel_cirurgia_p = 'S')) then
		begin
		select	max(dt_liberacao),
				max(dt_liberacao_anestesista)
		into	dt_liberacao_w,
				dt_liberacao_anestesista_w
		from 	cirurgia 
		where 	nr_cirurgia = nr_cirurgia_p;
			
		if	(ie_perm_real_w = 'N') then
			begin
			select	count(*)
			into	qt_cirurgia_w
			from	cirurgia 
			where	nr_cirurgia = nr_cirurgia_p 
			and	ie_status_cirurgia in (3,4);
			end;
		end if;
		end;
	else
		select	max(dt_liberacao),
				max(dt_liberacao_anestesista)
		into	dt_liberacao_w,
				dt_liberacao_anestesista_w
		from 	pepo_cirurgia 
		where 	nr_sequencia = nr_seq_pepo_p;
			
		if	(ie_perm_real_w = 'N') then
			begin
			select	count(*)
			into	qt_cirurgia_w
			from	cirurgia 
			where	nr_seq_pepo = nr_seq_pepo_p 
			and	ie_status_cirurgia in (3,4);
			end;
		end if;
	end if;
	
	if	(ie_permite_inserir_info_w = 'N') then
		begin
		dt_atendimento_w := TO_DATE(obter_dados_atendimento(nr_atendimento_p, 'DA'), 'dd/mm/yyyy hh24:mi:ss');
		if	(dt_atendimento_w is not null) and
			(sysdate >= (dt_atendimento_w + (qt_min_apos_alta_w/1440))) then
			begin
				ie_dt_atendimento_w := 'S';
			end;
		end if;
		end;
	end if;

ie_atualizar_p 				:= ie_atualizar_w;
nr_seq_perfil_pepo_p		:= nr_seq_perfil_pepo_w;
ie_somente_template_p		:= ie_somente_template_w;	
dt_liberacao_p				:= dt_liberacao_w;
dt_liberacao_anestesista_p 	:= dt_liberacao_anestesista_w;
qt_cirurgia_p				:= qt_cirurgia_w;	
ie_dt_atendimento_p 		:= ie_dt_atendimento_w;
ie_senha_liberacao_p		:= ie_senha_liberacao_w;
ie_imprimir_liberar_p		:= ie_imprimir_liberar_w;
ie_forma_liberar_p			:= ie_forma_liberar_w;
ie_quest_texto_padrao_p     := ie_quest_texto_padrao_w;

commit;
end verificar_readonly_item_pepo;
/
