CREATE OR REPLACE PROCEDURE GENERATE_DISEASE_END ( ie_status_p varchar2,
								cd_doenca_p varchar2,
								cd_pessoa_fisica_p number,
								nr_atendimento_p number,
								nr_seq_interno_p varchar2,
								dt_disease_end_p date) is
	  ie_decease_end_w varchar2(5) := 'SYSD';

	begin

	Obter_Param_Usuario(281, 1589, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_decease_end_w);

	Insert into DIAGNOSTICO_DOENCA_HIST (NR_SEQUENCIA,
	DT_ATUALIZACAO,
	NM_USUARIO,
	IE_STATUS,
	DS_OBSERVACAO,
	CD_PESSOA_FISICA,
	CD_RESPONSAVEL,
	CD_DOENCA,
	DT_ATUALIZACAO_NREC,
	NM_USUARIO_NREC,
	DT_DIAGNOSTICO,
	NR_ATENDIMENTO,
	IE_STATUS_PROBLEMA,
	DT_DISEASE_END,
	NR_SEQ_INTERNO) values (DIAGNOSTICO_DOENCA_HIST_seq.nextval,
	sysdate,
	wheb_usuario_pck.get_nm_usuario,
	ie_status_p,
	null,
	cd_pessoa_fisica_p,
	obter_pf_usuario_ativo,
	cd_doenca_p,
	sysdate,
	wheb_usuario_pck.get_nm_usuario,
	null,
	nr_atendimento_p,
	null,
	nvl(dt_disease_end_p, decode ( ie_decease_end_w, 'SYSD', sysdate, LAST_DAY(ADD_MONTHS(sysdate,-1)))),
	nr_seq_interno_p);

	commit;

END GENERATE_DISEASE_END;
/
