create or replace
procedure pls_salvar_comunic_int_glosa( nr_seq_comunicado_p	Number,
				        cd_glosa_p		Varchar2,
				        nm_usuario_p		Varchar2) is 
	
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Salvar as glosas no comunicação de internação
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [ X ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
begin
	
insert	into	pls_comunic_int_glosa(nr_sequencia, nr_seq_comunicado_int, dt_atualizacao,
				      nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				      cd_glosa)
			       values(pls_comunic_int_glosa_seq.nextval, nr_seq_comunicado_p, sysdate,
				      nm_usuario_p, sysdate, nm_usuario_p,
				      cd_glosa_p);
	
commit;

end pls_salvar_comunic_int_glosa;
/ 