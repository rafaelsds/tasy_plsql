create or replace
procedure san_consiste_bolsa_direcionada(sql_item_p		Varchar2,
					cd_pessoa_receptor_p	Varchar2,
					nm_usuario_p		Varchar2, 
					ds_item_retorno_p out	Varchar2,
					ds_retorno_p	out 	varchar2) is 

lista_producao_w		dbms_sql.varchar2_table;
nr_seq_doacao_w			number(10);
nr_seq_tipo_doacao_w		number(10);
ds_derivado_w			Varchar2(50);
nr_seq_producao_w		number(10);
nr_seq_prod_incons_w		Varchar2(2000);
cd_pessoa_dest_w		Varchar2(15);
qt_registro_w			Number(5);

begin

ds_retorno_p := '';
lista_producao_w := obter_lista_string(sql_item_p, ',');

for	i in lista_producao_w.first..lista_producao_w.last loop
	nr_seq_producao_w := to_number(lista_producao_w(i));
	
	if	(nvl(nr_seq_producao_w,0) > 0) then
	
		select	max(nr_seq_doacao),
			max(substr(obter_desc_san_derivado(nr_seq_derivado),1,255))
		into	nr_seq_doacao_w,
			ds_derivado_w
		from	san_producao
		where	nr_sequencia = nr_seq_producao_w
		and	dt_liberacao_direc is null;

		if	(nvl(nr_seq_doacao_w, 0) > 0) then
			
			select	max(nr_seq_tipo),
				max(cd_pessoa_dest)
			into	nr_seq_tipo_doacao_w,
				cd_pessoa_dest_w
			from	san_doacao
			where	nr_sequencia = nr_seq_doacao_w;
					
			select	count(*)
			into	qt_registro_w
			from	san_tipo_doacao
			where	nr_sequencia		= nr_seq_tipo_doacao_w
			and	ie_classif_doacao 	= 'D'
			and	cd_pessoa_dest_w	<> cd_pessoa_receptor_p;
			
			if	(qt_registro_w > 0) then
			
				select	substr(obter_desc_expressao(688456),1,255)|| chr(10)||chr(13)||ds_derivado_w
				into	ds_retorno_p
				from	dual;
				
				nr_seq_prod_incons_w	:= nr_seq_prod_incons_w||nr_seq_producao_w||',';
				
			end if;		
		end if;	
	end if;	
end loop;

ds_item_retorno_p	:= nr_seq_prod_incons_w;

commit;

end san_consiste_bolsa_direcionada;
/
