create or replace
procedure gerar_cobranca_bb_240_sepaco(	nr_seq_cobr_escrit_p		number,
						cd_estabelecimento_p		number,
						nm_usuario_p			varchar2) is

-- Febraban v08.4  -  01/09/2009
						
ds_conteudo_w			varchar2(240);
ds_brancos_205_w		varchar2(205);
ds_brancos_165_w		varchar2(165);
nm_empresa_w			varchar2(80);
nm_pessoa_w			varchar2(80);
ds_brancos_54_w			varchar2(54);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(40);
ds_brancos_80_w			varchar2(80);
ds_banco_w			varchar2(30);
ds_brancos_25_w			varchar2(25);
cd_convenio_banco_w		varchar2(20);
ds_cidade_w			varchar2(20);
ds_nosso_numero_w		varchar2(20);
ds_complemento_w		varchar2(18);
vl_juros_w			varchar2(17);
ds_brancos_17_w			varchar2(17);
nr_conta_corrente_w		varchar2(15);
dt_remessa_retorno_w		varchar2(14);
dt_geracao_arquivo_w		varchar2(14);
nr_inscricao_w			varchar2(15);
vl_liquidacao_w			varchar2(15);
vl_cobranca_w			varchar2(15);
vl_desconto_w			varchar2(15);
dt_desconto_w			varchar2(8);
vl_mora_w			varchar2(15);
nr_seq_registro_w		number(10);
ds_brancos_10_w			varchar2(10);
qt_reg_lote_w			varchar2(10);
dt_vencimento_w			varchar2(8);
dt_emissao_w			varchar2(8);
dt_pagamento_w			varchar2(8);
cd_cep_w			varchar2(8);
nr_seq_lote_w			varchar2(7);
nr_seq_arquivo_w		varchar2(6);
nr_seq_envio_w			varchar2(8);
cd_agencia_bancaria_w		varchar2(5);
ie_tipo_inscricao_w		varchar2(5);
nr_endereco_w			varchar2(5);
cd_banco_w			varchar2(3);
ie_emissao_bloqueto_w		varchar2(3);
nr_digito_agencia_w		varchar2(2);
sg_uf_w				varchar2(2);
ie_protesto_w			varchar2(2);
ie_tipo_registro_w		varchar2(1);
qt_reg_arquivo_w		number(10) := 0;
ds_brancos_69_w			varchar2(69);
ds_brancos_33_w			varchar2(33);
dt_gravacao_w			varchar2(8);
nr_nosso_numero_w		varchar2(20);
nr_seq_carteira_cobr_w		varchar2(1);
nr_documento_cobr_w		varchar2(15);
nr_titulo_w			varchar2(25);
nm_pessoa_titulo_w		varchar2(40);
sg_estado_w			varchar2(15);
nr_seq_apres_w			number(10)	:= 1;
ds_brancos_9_w			varchar2(9);
ds_brancos_2_w			varchar2(2);
ds_zeros_23_w			varchar2(23);
ds_brancos_8_w			varchar2(8);
ds_brancos_55_w			varchar2(55);
ds_brancos_28_w			varchar2(28);
ie_codigo_desconto_w		varchar2(1);
cd_carteira_w			banco_carteira.cd_carteira%type;
cd_variacao_carteira_w		banco_carteira.cd_variacao_carteira%type;
ds_zeros_8_w			varchar2(8);
nr_lote_w_w			varchar2(4) := 0;
nr_lote_w			varchar2(4);
nr_seq_registro_seg_q_w		number(10);

cursor C01 is
	select	lpad(c.cd_banco,3,'0') cd_banco,
		substr(a.nr_sequencia,1,5) nr_seq_envio,
		'3' ie_tipo_registro,
		lpad(somente_numero(to_char(c.vl_liquidacao,'9999999999990.00')),15,'0') vl_liqudacao,
		rpad('Banco do Brasil',30,' ') nm_cedente,
		to_char(b.dt_pagamento_previsto,'ddmmyyyy') dt_vencimento,
		lpad(somente_numero(to_char(c.vl_cobranca,'9999999999990.00')),15,'0') vl_cobranca,
		lpad(somente_numero(to_char(c.vl_desconto,'9999999999990.00')),15,'0') vl_desconto,
		decode(nvl(c.vl_desconto,0),0,'00000000',to_char(sysdate,'ddmmyyyy')) dt_desconto,
		lpad(somente_numero(to_char(nvl(c.vl_juros,0) + nvl(c.vl_multa,0),'9999999999990.00')),15,'0') vl_mora,
		to_char(c.dt_liquidacao,'ddmmyyyy') dt_pagamento,
		rpad(nvl(b.nr_nosso_numero,' '),20,' ') ds_nosso_numero,
		lpad(substr(nvl(c.cd_agencia_bancaria,'0'),1,5),5,'0') cd_agencia_bancaria,
		rpad(nvl(calcula_digito('Modulo11',nvl(c.cd_agencia_bancaria,'0')),'0'),1,'0') nr_digito_agencia,
		lpad(substr(nvl(e.cd_conta,'0'),1,12),12,'0') || rpad(substr(nvl(e.ie_digito_conta,'0'),1,1),1,'0') nr_conta_corrente,
		rpad(nvl(b.nr_nosso_numero,' '),20,' ') nr_nosso_numero,
		lpad(nvl(b.nr_seq_carteira_cobr,0),1,'0') nr_seq_carteira_cobr,
		rpad(a.nr_sequencia,15,' ') nr_documento_cobr,
		to_char(b.dt_emissao,'ddmmyyyy') dt_emissao,
		b.nr_titulo,
		decode(b.cd_pessoa_fisica,null,'2','1') ie_tipo_inscricao,
		lpad(decode(b.cd_pessoa_fisica,null,b.cd_cgc,(	select	nvl(x.nr_cpf,'0')
								from	pessoa_fisica x
								where	x.cd_pessoa_fisica	= b.cd_pessoa_fisica)),15,'0') nr_inscricao,
		rpad(upper(nvl(substr(obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc),1,40),' ')),40,' ') nm_pessoa_titulo,
		rpad(upper(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'EN'),1,40),' ')),40,' ') ds_endereco,
		rpad(upper(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'B'),1,15),' ')),15,' ') ds_bairro,
		lpad(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CEP'),1,8),' '),8,'0') cd_cep,
		rpad(upper(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CI'),1,15),' ')),15,' ') ds_cidade,
		rpad(upper(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'UF'),1,2),' ')),2,' ') sg_estado,
		decode(nvl(c.vl_desconto,0),0,'0','1') ie_codigo_desconto
	from	titulo_receber_v b,
		titulo_receber_cobr c,
		cobranca_escritural a,
		banco_carteira d,
		banco_estabelecimento e
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= e.nr_sequencia
	and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin

delete from w_envio_banco where nm_usuario = nm_usuario_p;

select	lpad(' ',17,' '),
	lpad(' ',10,' '),
	lpad(' ',54,' '),
	lpad(' ',80,' '),
	lpad(' ',25,' '),
	lpad(' ',165,' '),
	lpad(' ',205,' '),
	lpad(' ',69,' '),
	lpad(' ',33,' '),
	lpad(' ',9,' '),
	lpad(' ',2,' '),
	lpad('0',23,'0'),
	lpad(' ',8,' '),
	lpad(' ',55,' '),
	lpad(' ',28,' '),
	lpad('0',8,'0')
into	ds_brancos_17_w,
	ds_brancos_10_w,
	ds_brancos_54_w,
	ds_brancos_80_w,
	ds_brancos_25_w,
	ds_brancos_165_w,
	ds_brancos_205_w,
	ds_brancos_69_w,
	ds_brancos_33_w,
	ds_brancos_9_w,
	ds_brancos_2_w,
	ds_zeros_23_w,
	ds_brancos_8_w,
	ds_brancos_55_w,
	ds_brancos_28_w,
	ds_zeros_8_w
from	dual;

/* Header Arquivo*/
select	'0' ie_tipo_registro,
	lpad(c.cd_banco,3,'0') cd_banco,
	to_char(sysdate,'DDMMYYYYHHMISS') dt_geracao_arquivo,
	rpad('BANCO DO BRASIL S.A.',30,' ') ds_banco,
	lpad(c.cd_agencia_bancaria,5,'0') cd_agencia_bancaria,
	rpad(calcula_digito('Modulo11',c.cd_agencia_bancaria),1,'0') nr_digito_agencia,
	lpad(c.cd_conta,12,'0') || rpad(c.ie_digito_conta,1,'0') nr_conta_corrente,
	rpad(upper(substr(obter_razao_social(b.cd_cgc),1,30)),30,' ') nm_empresa,
	lpad(b.cd_cgc,14,'0') nr_inscricao,
	substr(nvl(d.cd_convenio_banco,c.cd_convenio_banco),1,9) cd_convenio_banco,
	lpad(nvl(substr(d.cd_carteira,1,2),0),2, '0') cd_carteira,
	lpad(nvl(substr(d.cd_variacao_carteira,1,3),0),3, '0') cd_variacao_carteira,
	lpad(a.nr_sequencia,6,'0')
into	ie_tipo_registro_w,
	cd_banco_w,
	dt_geracao_arquivo_w,
	ds_banco_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nm_empresa_w,
	nr_inscricao_w,
	cd_convenio_banco_w,
	cd_carteira_w,
	cd_variacao_carteira_w,
	nr_seq_arquivo_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:=	cd_banco_w ||
			'0000' ||
			ie_tipo_registro_w ||
			rpad(' ',9,' ') ||
			'2' ||
			nr_inscricao_w ||
			lpad(nvl(cd_convenio_banco_w,'0'),9,'0') || '0014' || cd_carteira_w || cd_variacao_carteira_w || ds_brancos_2_w || --33 at�  52
			cd_agencia_bancaria_w ||
			nr_digito_agencia_w ||
			nr_conta_corrente_w ||
			' ' ||
			nm_empresa_w ||
			ds_banco_w ||
			ds_brancos_10_w ||
			'1' ||
			dt_geracao_arquivo_w ||
			nr_seq_arquivo_w ||
			'08400000' ||
			ds_brancos_69_w;

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);

qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
/* Fim Header Arquivo */

/* Header Lote */

nr_lote_w_w	:= nr_lote_w_w + 1;
nr_lote_w	:= lpad(nr_lote_w_w,4,'0');

select	'1' ie_tipo_registro,
	lpad(c.cd_banco,3,'0') cd_banco,
	lpad(a.nr_sequencia,8,'0') nr_seq_envio,
	lpad(c.cd_agencia_bancaria,5,'0') cd_agencia_bancaria,
	rpad(calcula_digito('Modulo11',c.cd_agencia_bancaria),1,'0') nr_digito_agencia,
	lpad(c.cd_conta,12,'0') || rpad(c.ie_digito_conta,1,'0') nr_conta_corrente,
	rpad(upper(substr(obter_razao_social(b.cd_cgc),1,30)),30,' ') nm_empresa,
	lpad(b.cd_cgc,15,'0') nr_inscricao,
	nvl(d.cd_convenio_banco,c.cd_convenio_banco) cd_convenio_banco,
	lpad(nvl(substr(d.cd_carteira,1,2),0),2, '0') cd_carteira,
	lpad(nvl(substr(d.cd_variacao_carteira,1,3),0),3, '0') cd_variacao_carteira,
	rpad(a.nr_sequencia,6,'0'),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'EN'),40,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'NR'),5,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'CM'),18,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'MU'),20,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'CEP'),8,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'UF'),2,' '),
	to_char(sysdate, 'ddmmyyyy') dt_geracao
into	ie_tipo_registro_w,
	cd_banco_w,
	nr_seq_envio_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nm_empresa_w,
	nr_inscricao_w,
	cd_convenio_banco_w,
	cd_carteira_w,
	cd_variacao_carteira_w,
	nr_seq_arquivo_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_cidade_w,
	cd_cep_w,
	sg_uf_w,
	dt_gravacao_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:= 	cd_banco_w || '0001' || ie_tipo_registro_w || 'R01' || ds_brancos_2_w || '043 2' || nr_inscricao_w || 
			lpad(nvl(cd_convenio_banco_w,'0'),9,'0') || '0014' || cd_carteira_w || cd_variacao_carteira_w || ds_brancos_2_w || 
			cd_agencia_bancaria_w || nr_digito_agencia_w || nr_conta_corrente_w || ' ' || nm_empresa_w || ds_brancos_80_w || 
			ds_zeros_8_w || dt_gravacao_w || ds_zeros_8_w || ds_brancos_33_w;
			--ds_zeros_23_w || ds_brancos_33_w;

nr_seq_apres_w	:= nr_seq_apres_w + 1;

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
		
qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
/* Fim Header Lote */

/* Detalhe */
open C01;
loop
fetch C01 into	
	cd_banco_w,
	nr_seq_envio_w,
	ie_tipo_registro_w,
	vl_liquidacao_w,
	ds_banco_w,
	dt_vencimento_w,
	vl_cobranca_w,
	vl_desconto_w,
	dt_desconto_w,
	vl_mora_w,
	dt_pagamento_w,
	ds_nosso_numero_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nr_nosso_numero_w,
	nr_seq_carteira_cobr_w,
	nr_documento_cobr_w,
	dt_emissao_w,
	nr_titulo_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_titulo_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	sg_estado_w,
	ie_codigo_desconto_w;
exit when C01%notfound;

	nr_seq_registro_w	:= nvl(nr_seq_registro_w,0) + 1;

	/* Segmento P */
	nr_seq_apres_w		:= nr_seq_apres_w + 1;

	if	(length(nvl(cd_convenio_banco_w,' ')) = 7) then
		ds_nosso_numero_w	:= cd_convenio_banco_w || lpad(nr_titulo_w,10,'0');
	elsif	(length(nvl(cd_convenio_banco_w,' ')) >= 5) then
		ds_nosso_numero_w	:= lpad(cd_convenio_banco_w,6,'0') || lpad(nr_titulo_w,5,'0');
		ds_nosso_numero_w	:= ds_nosso_numero_w || calcula_digito('MODULO11',ds_nosso_numero_w);
	elsif	(length(nvl(cd_convenio_banco_w,' ')) <= 4) then
		ds_nosso_numero_w	:= lpad(cd_convenio_banco_w,4,'0') || lpad(nr_titulo_w,7,'0');
		ds_nosso_numero_w	:= ds_nosso_numero_w || calcula_digito('MODULO11',ds_nosso_numero_w);
	end if;

	ds_conteudo_w	:=	cd_banco_w ||
				'0001' ||
				ie_tipo_registro_w ||
				lpad(nr_seq_registro_w,5,'0') ||
				'P' ||
				' ' ||
				'01' ||
				cd_agencia_bancaria_w ||
				nr_digito_agencia_w ||
				nr_conta_corrente_w ||
				' ' ||
				rpad(ds_nosso_numero_w,20,' ') ||
				nr_seq_carteira_cobr_w ||
				'1' ||
				'2' ||
				'2' ||
				'2' ||
				nr_documento_cobr_w ||
				dt_vencimento_w ||
				vl_cobranca_w ||
				'00000' ||
				' ' ||
				'02' ||
				'N' ||
				dt_emissao_w ||
				'1' ||
				'00000000' ||
				vl_mora_w ||
				ie_codigo_desconto_w ||
				dt_desconto_w ||
				vl_desconto_w ||
				'000000000000000' ||
				'000000000000000' ||
				rpad(nr_titulo_w,25,' ') ||
				'3' ||
				'00' ||
				'1' ||
				'000' ||
				'09' ||
				'0000000000' ||
				' ';
	
	insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w);
	/* Fim segmento P */

	/* Segmento Q */
	nr_seq_apres_w	:= nr_seq_apres_w + 1;
	nr_seq_registro_seg_q_w := nvl(nr_seq_registro_w,0) + 1;

	ds_conteudo_w	:= 	cd_banco_w || '0001' || ie_tipo_registro_w || lpad(nr_seq_registro_seg_q_w,5,'0') || 'Q' || ' ' || '01' || 
				ie_tipo_inscricao_w || nr_inscricao_w || nm_pessoa_titulo_w || ds_endereco_w || ds_bairro_w || cd_cep_w || 
				ds_cidade_w || substr(sg_estado_w,1,2) || '0' || ds_brancos_55_w || '000' || ds_brancos_28_w;

	insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w);

	/* Fim segmento Q */
				
	qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
	
end loop;
close C01;
/* Fim detalhe */

/* Trailler Lote*/
select	(count(*) * 2) + 2,
	'9' ie_tipo_registro,
	'0001' nr_seq_envio,
	somente_numero(to_char(sum(b.vl_liquidacao),'000000000000.00'))
into	qt_reg_lote_w,
	ie_tipo_registro_w,
	nr_seq_envio_w,
	vl_liquidacao_w
from	titulo_receber_cobr b,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
group by a.nr_sequencia,
	 a.dt_remessa_retorno,
	 a.dt_remessa_retorno,
	 a.cd_banco,
	a.ie_emissao_bloqueto;
	
ds_conteudo_w	:= 	'001' || nr_seq_envio_w || '5' || ds_brancos_9_w || lpad(qt_reg_lote_w,6,'0') || '000000' || lpad(vl_liquidacao_w,16,'0') ||
			'000000000000000000' || ds_brancos_165_w || '0000000000';

nr_seq_apres_w	:= nr_seq_apres_w + 1;
	
insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
		
qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
/* Fim Trailler Lote*/

/* Trailler Arquivo*/
select	(count(*) * 2) + 4,
	'9' ie_tipo_registro,
	'99999' nr_seq_envio
into	qt_reg_lote_w,
	ie_tipo_registro_w,
	nr_seq_envio_w
from	titulo_receber_cobr b,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
qt_reg_arquivo_w := qt_reg_arquivo_w + 1;

ds_conteudo_w	:=	'001' || '9999' || '9' || ds_brancos_9_w || '000001' || lpad(qt_reg_lote_w,6,'0') || '000000' || 
			ds_brancos_205_w;

nr_seq_apres_w	:= nr_seq_apres_w + 1;

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
/* Fim Trailler Arquivo*/

commit;

end gerar_cobranca_bb_240_sepaco;
/