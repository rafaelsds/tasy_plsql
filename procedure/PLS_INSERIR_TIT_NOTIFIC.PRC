create or replace 
procedure pls_inserir_tit_notific
	(nr_titulo_p			titulo_receber.nr_titulo%type,
	 nr_seq_lote_p			pls_notificacao_lote.nr_sequencia%type,
	 nr_seq_notific_pagador_p	pls_notificacao_item.nr_seq_notific_pagador%type,
	 ie_tipo_vinculacao_p		pls_notificacao_item.ie_tipo_vinculacao%type,
	 nm_usuario_p			pls_notificacao_item.nm_usuario%type
	) is

ie_rescindir_contrato_w	pls_notificacao_regra.ie_rescindir_contrato%type;
ie_titulo_perda_w	pls_notificacao_regra.ie_titulo_perda%type;
ie_suspender_atend_w	pls_notificacao_regra.ie_suspender_atend%type;
vl_juros_w		number(15,2)	:= 0;
vl_multa_w		number(15,2)	:= 0;

begin
select	b.ie_rescindir_contrato,
	b.ie_titulo_perda,
	b.ie_suspender_atend
into	ie_rescindir_contrato_w,
	ie_titulo_perda_w,
	ie_suspender_atend_w
from	pls_notificacao_lote	a,
	pls_notificacao_regra	b
where	a.nr_seq_regra	= b.nr_sequencia
and	a.nr_sequencia	= nr_seq_lote_p;

vl_juros_w	:= nvl(obter_juros_multa_titulo(nr_titulo_p, sysdate, 'R', 'J'),0);
vl_multa_w	:= nvl(obter_juros_multa_titulo(nr_titulo_p, sysdate, 'R', 'M'),0);

insert into pls_notificacao_item
	(nr_sequencia,
	nr_seq_notific_pagador,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_regra,
	nr_seq_mensalidade,
	nr_seq_contrato,
	ie_rescindir_contrato,
	nr_titulo,
	vl_saldo_titulo,
	vl_juros,
	vl_multa,
	vl_titulo,
	dt_vencimento_titulo,
	dt_liquidacao,
	vl_pagar,
	ie_suspender_contrato,
	dt_vencimento_original,
	ie_tipo_vinculacao)
select	pls_notificacao_item_seq.nextval,
	nr_seq_notific_pagador_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	null,
	a.nr_seq_mensalidade,
	b.nr_seq_contrato,
	ie_rescindir_contrato_w,
	a.nr_titulo,
	nvl(a.vl_saldo_titulo,0),
	vl_juros_w,
	vl_multa_w,
	nvl(a.vl_titulo,0),
	a.dt_pagamento_previsto,
	a.dt_liquidacao,
	(decode(ie_titulo_perda_w, 'S', nvl(a.vl_titulo,0), nvl(a.vl_saldo_titulo,0)) + vl_juros_w + vl_multa_w),
	ie_suspender_atend_w,
	a.dt_vencimento,
	ie_tipo_vinculacao_p
from	titulo_receber		a,
	pls_contrato_pagador	b,
	pls_notificacao_item	c
where	b.nr_sequencia			= a.nr_seq_pagador
and	c.nr_titulo(+)			= a.nr_titulo
and	c.nr_seq_notific_pagador(+)	= nr_seq_notific_pagador_p
and	c.nr_sequencia	is null -- para n�o inserir titulos duplicados na mesma notifica��o
and	a.nr_titulo 			= nr_titulo_p;

commit;
end pls_inserir_tit_notific;
/