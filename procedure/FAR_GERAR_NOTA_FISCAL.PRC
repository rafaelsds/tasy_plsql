create or replace
procedure	far_gerar_nota_fiscal (
			nr_seq_nf_p		out	number,
			nr_seq_pedido_p			number,
			cd_pessoa_fisica_p			varchar2,
			nm_usuario_p			varchar2,
			cd_estabelecimento_p		number) is

nr_sequencia_w		number(10);
cd_cgc_w		varchar2(14);
nr_nota_w		varchar2(255);
cd_operacao_nf_w		number(4);
cd_natureza_operacao_w	number(4);
cd_serie_nf_w		nota_fiscal.cd_serie_nf%type;

begin

select	nvl(max(cd_operacao_nf),0)
into	cd_operacao_nf_w
from	operacao_nota
where	ie_situacao = 'A'
and	ie_operacao_fiscal = 'E';

if	(cd_operacao_nf_w = 0) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(279136);
end if;

select	cd_natureza_operacao
into	cd_natureza_operacao_w
from	operacao_nota
where	cd_operacao_nf = cd_operacao_nf_w;

if	(nvl(cd_natureza_operacao_w,'0') = '0') then
	begin
	
	select	max(cd_natureza_operacao)
	into	cd_natureza_operacao_w
	from	operacao_nota_nat
	where	cd_operacao_nf = cd_operacao_nf_w;

	if	(nvl(cd_natureza_operacao_w,'0') = '0') then
		begin

		select	max(cd_natureza_operacao)
		into	cd_natureza_operacao_w
		from	natureza_operacao
		where	ie_entrada_saida = 'S'
		and	ie_situacao = 'A';

		end;
	end if;

	if	(nvl(cd_natureza_operacao_w,0) = '0') then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(279137);	
	end if;

	end;
end if;

select	max(cd_serie_nf)
into	cd_serie_nf_w
from	operacao_nota_serie
where	cd_operacao_nf = cd_operacao_nf_w;

if	(nvl(cd_serie_nf_w,'0') = '0') then
	begin
	
	select	max(cd_serie_nf)
	into	cd_serie_nf_w
	from	serie_nota_fiscal
	where	cd_estabelecimento = cd_estabelecimento_p;

	if	(cd_serie_nf_w = '') then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(279139);	
	end if;

	end;
end if;

select	cd_cgc
into	cd_cgc_w
from	estabelecimento_v
where	cd_estabelecimento = cd_estabelecimento_p;

select	nota_fiscal_seq.nextval
into	nr_sequencia_w
from	dual;

nr_nota_w	:= to_char(nr_sequencia_w);

insert into nota_fiscal(
	nr_sequencia,
	cd_estabelecimento,
	cd_cgc_emitente,
	cd_serie_nf,
	nr_sequencia_nf,
	cd_operacao_nf,
	dt_emissao,
	dt_entrada_saida,
	ie_acao_nf,
	ie_emissao_nf,
	ie_tipo_frete,
	vl_mercadoria,
	vl_total_nota,
	qt_peso_bruto,
	qt_peso_liquido,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_condicao_pagamento,
	cd_pessoa_fisica,
	vl_ipi,
	vl_descontos,
	vl_frete,
	vl_seguro,
	vl_despesa_acessoria,
	cd_natureza_operacao,
	vl_desconto_rateio,
	ie_situacao,
	nr_lote_contabil,
	ie_entregue_bloqueto,
	ie_tipo_nota,
	nr_nota_fiscal) values (
		nr_sequencia_w,
		cd_estabelecimento_p,
		cd_cgc_w,
		cd_serie_nf_w,
		1,
		cd_operacao_nf_w,
		trunc(sysdate),
		sysdate,
		'1', '0', '0', 0, 0, 0, 0,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		2,
		cd_pessoa_fisica_p,
		0, 0, 0, 0, 0,
		cd_natureza_operacao_w,
		0, '1', 0, 'N', 'SF', nr_nota_w);

commit;

nr_seq_nf_p	:= nr_sequencia_w;

end far_gerar_nota_fiscal;
/
