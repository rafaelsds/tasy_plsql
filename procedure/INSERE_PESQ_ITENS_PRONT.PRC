create or replace
procedure insere_pesq_itens_pront(	nr_seq_item_p		number,
					ds_texto_p		long,
					nm_usuario_p		varchar2,
					dt_registro_p		date,
					cd_profissional_p	varchar2,
					dt_liberacao_p		date,
					nr_atendimento_p	number,
					ds_texto_varchar_p	varchar2 default null,
					nr_seq_resultado_p	number default null,
					nr_seq_result_item_p 	number default null)
					is
		
ds_texto_w		long;

begin

if	(trim(ds_texto_varchar_p) is not null) then
	ds_texto_w := ds_texto_varchar_p;
end if;

insert	into w_pesq_itens_pront(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				ds_texto,
				nr_seq_item,
				cd_profissional,
				dt_liberacao,
				nr_atendimento,
				dt_registro,
				nr_seq_resultado,
				nr_seq_result_item)
values
				(w_pesq_itens_pront_seq.nextval,
				sysdate,
				nm_usuario_p,
				ds_texto_p,
				nr_seq_item_p,
				cd_profissional_p,
				dt_liberacao_p,
				nr_atendimento_p,
				dt_registro_p,
				nr_seq_resultado_p,
				nr_seq_result_item_p);
				
commit;				

end insere_pesq_itens_pront;				
/