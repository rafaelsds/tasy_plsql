create or replace
procedure pls_gerar_w_resumo_conta
			(	nr_seq_conta_p			number,
				nr_seq_conta_proc_p		number,
				nr_seq_conta_mat_p		number,
				nr_seq_conta_princ_p		number,
				nr_seq_analise_p		number,
				nm_usuario_p			varchar2,
				nr_seq_item_criado_p	out	number) is

nr_seq_prestador_exec_w		Number(10);
nr_seq_grau_partic_w		Number(10);
nr_seq_conta_referencia_w	number(10);
nr_seq_item_w			Number(10);
ie_tipo_item_w			Varchar2(1);
cd_item_w			Number(15);
ie_origem_proced_w		Number(10);
ie_tipo_despesa_w		Varchar2(10);
ie_via_acesso_w			Varchar2(10);
dt_item_w			Date;
tx_item_w			Number(9,3);
qt_apresentado_w		Number(12,4);
qt_liberado_w			Number(12,4);
vl_unitario_w			pls_conta_proc.vl_unitario%type;

vl_total_w			Number(15,2);
vl_taxa_servico_imp_w		Number(15,2);
vl_taxa_co_imp_w		Number(15,2);
vl_taxa_material_imp_w		Number(15,2);
vl_taxa_material_w		Number(15,2);
vl_taxa_servico_w		Number(15,2);
vl_taxa_co_w			Number(15,2);
vl_taxa_mat_material_w		Number(15,2);
vl_taxa_mat_material_imp_w	Number(15,2);
vl_taxa_intercambio_imp_w	Number(15,2);
vl_taxa_intercambio_w		Number(15,2);
cd_medico_executor_w		varchar2(10);
ie_tipo_guia_w			varchar2(10);
cd_guia_referencia_w		varchar2(20);
cd_guia_w			varchar2(20);
nr_seq_apres_w			number(10);
ds_grau_participacao_w		varchar2(255);
ds_item_w			varchar2(255);
ds_via_acesso_w			varchar2(255);
nm_prestador_w			varchar2(255);
nm_participante_w		varchar2(255);
ds_tipo_guia_w			varchar2(255);
ie_status_w			varchar2(1);
qt_glosa_w			number(5);
ie_glosa_w			varchar2(1);
qt_ocorrencia_w			number(5);
ie_ocorrencia_glosa_w		varchar2(1);
vl_total_apres_w		Number(15,2);
vl_unitario_apres_w		Number(15,2);
vl_calculado_w			Number(15,2);
qt_ocorrencia_glosa_w		number(5);
nr_seq_protocolo_w		number(10);
ie_status_conta_w		varchar2(1);
ds_status_conta_w		varchar2(120);
cd_porte_anestesico_w		varchar2(10);
nr_auxiliares_w			number(10);
nm_prestador_pag_w		varchar2(255);
nr_seq_prestador_pgto_w		number(10);
nr_seq_guia_w			number(10);
ie_autorizado_w			varchar2(1)	:= 'N';
cd_classificacao_sip_w		varchar2(15);
cd_classif_cred_w		varchar2(40);
cd_classif_deb_w		varchar2(40);
ie_carater_internacao_w		varchar2(1);
ie_exige_nf_w			varchar2(1);
nr_nota_fiscal_w		Number(20);
nr_seq_prestador_solic_w	Number(10);
nm_prestador_solic_w		varchar2(255);
vl_coparticipacao_unit_w	Number(15,2);
vl_coparticipacao_w		Number(15,2);
ie_nota_fiscal_w		varchar2(1);
nr_seq_restricao_w		Number(10);
dt_atend_ref_w			date;
nr_seq_prest_fornec_w		Number(10);
ds_fornecedor_w			varchar2(255);
nr_seq_segurado_w		Number(10);
cd_usuario_plano_w		varchar2(30);
nm_segurado_w			varchar2(255);
nm_prestador_exec_w		varchar2(255);
nr_identificador_w		Number(10);
nr_seq_item_analise_w		Number(10);
nr_seq_partic_w			Number(10);
cd_medico_w			varchar2(10);
nr_seq_cbo_saude_w		Number(10);
nr_seq_conselho_w		Number(10);
nr_seq_honorario_crit_w		Number(10);
nr_seq_prestador_w		Number(10);
vl_honorario_medico_w		Number(15,2);
vl_participante_w		Number(15,2);
nr_seq_proc_ref_w		Number(10);
vl_apresentado_w		Number(15,2);
nr_seq_grau_partic_ww		Number(10);
ds_grau_participacao_ww		varchar2(255);
nr_seq_cbo_saude_ww		Number(10);
nm_participante_ww		varchar2(255);
nr_seq_apres_ww			Number(10);
dt_inicio_item_w		date;
nr_seq_setor_atend_w		Number(10);
ds_setor_atendimento_w		varchar2(255);
cd_estabelecimento_w		Number(10);
ds_item_importacao_w		Varchar2(300);
tx_intercambio_w		Number(7,4);
tx_intercambio_imp_w		Number(7,4);
vl_glosa_w			Number(15,2);
ie_gestacao_w			Varchar2(1);
ie_aborto_w			Varchar2(1);
ie_parto_normal_w               Varchar2(1);
ie_complicacao_puerperio_w	Varchar2(1);
ie_complicacao_neonatal_w       Varchar2(1);
ie_parto_cesaria_w		Varchar2(1);
ie_baixo_peso_w                 Varchar2(1);
ie_atend_rn_sala_parto_w        Varchar2(1);
ie_transtorno_w			Varchar2(1);
ie_obito_mulher_w		Number(2);

nr_seq_conta_proc_w		Number(10);
nr_seq_conta_mat_w		Number(10);
nr_seq_glosa_oc_gerada_w	Number(10);
ie_pagamento_w			Varchar2(1);
ie_status_ww			Varchar2(1);
ie_valor_base_w			Varchar2(1);

ie_repassa_medico_w		Varchar2(1);
vl_calculado_hi_w		Number(15,2);
vl_calculado_co_w		Number(15,2);
vl_calculado_material_w		Number(15,2);
qt_partics_gerados_conta_w	Number(10);
ie_custo_operacional_w		Varchar2(1);

vl_calc_co_util_w		Number(15,2);
vl_calc_hi_util_w		Number(15,2);
vl_calc_mat_util_w		Number(15,2);
vl_calc_hi_w			Number(15,2);
qt_partics_proc_w		Number(10);
ie_conta_gerada_w		Varchar2(1) := 'N';
nm_prest_exec_w			varchar2(255);
nm_prest_solic_w		varchar2(255);

qt_registro_w			number(10);

Cursor C01 is
	select	nr_sequencia,
		'P',
		cd_procedimento,
		ie_origem_proced,
		ie_tipo_despesa,
		ie_via_acesso,
		dt_procedimento,
		tx_item,
		qt_procedimento_imp,
		qt_procedimento,
		vl_unitario,
		vl_liberado,
		vl_unitario_imp,
		vl_procedimento_imp,
		vl_procedimento,
		ie_status,
		cd_porte_anestesico,
		nr_auxiliares,
		cd_classificacao_sip,
		cd_classif_cred,
		cd_classif_deb,
		null,
		null,
		'',
		nvl(nr_seq_proc_ref,nr_sequencia),
		dt_inicio_proc,
		nr_seq_setor_atend,
		pls_obter_ds_setor_atend(nr_seq_setor_atend),
		(cd_procedimento_imp||' - '||ds_procedimento_imp) ds_item,
		vl_taxa_servico_imp,
		vl_taxa_co_imp,
		vl_taxa_material_imp,
		vl_taxa_material,
		vl_taxa_servico,
		vl_taxa_co,
		0,
		0,
		tx_intercambio_imp,
		tx_intercambio,
		vl_glosa,
		nr_sequencia,
		null,
		nvl(ie_valor_base,'1'),
		vl_total_partic,
		vl_custo_operacional,
		vl_materiais,
		ie_repassa_medico,
		vl_calc_co_util,
		vl_calc_hi_util,
		vl_calc_mat_util
	from	pls_conta_proc
	where	((nr_seq_conta	= nr_seq_conta_p) and ((nvl(nr_seq_conta_proc_p,0) = 0) and (nvl(nr_seq_conta_mat_p,0) = 0)))
		or	nr_sequencia  = nr_seq_conta_proc_p
	union
	select	nr_sequencia,
		'M',
		nr_seq_material,
		null,
		ie_tipo_despesa,
		'',
		dt_atendimento,
		tx_reducao_acrescimo,
		qt_material_imp,
		qt_material,
		vl_unitario,
		vl_liberado,
		vl_unitario_imp,
		vl_material_imp,
		vl_material,
		ie_status,
		'',
		null,
		cd_classificacao_sip,
		cd_classif_cred,
		cd_classif_deb,
		nr_nota_fiscal,
		nr_seq_prest_fornec,
		substr(pls_obter_dados_prestador(nr_seq_prest_fornec,'N'),1,255),
		null,
		dt_inicio_atend,
		nr_seq_setor_atend,
		pls_obter_ds_setor_atend(nr_seq_setor_atend),
		(cd_material_imp||' - '||ds_material_imp) ds_item,
		0,
		0,
		0,
		0,
		0,
		0,
		vl_taxa_material,
		vl_taxa_material_imp,
		tx_intercambio_imp,
		tx_intercambio,
		vl_glosa,
		null,
		nr_sequencia,
		nvl(ie_valor_base,'1'),
		null,
		null,
		vl_material,
		'N',
		0,
		0,
		vl_material
	from	pls_conta_mat
	where	((nr_seq_conta	= nr_seq_conta_p) and ((nvl(nr_seq_conta_proc_p,0) = 0) and (nvl(nr_seq_conta_mat_p,0) = 0)))
		or	nr_sequencia  = nr_seq_conta_mat_p;


Cursor C02 is
	select	a.nr_sequencia,
		a.cd_medico,
		a.nr_seq_cbo_saude,
		a.nr_seq_conselho,
		a.nr_seq_grau_partic,
		a.nr_seq_honorario_crit,
		a.nr_seq_prestador,
		a.nr_seq_prestador_pgto,
		a.vl_honorario_medico,
		a.vl_participante,
		'R',
		decode(a.nr_seq_prestador,null,nvl(Obter_nome_medico(a.cd_medico,'N'), a.nm_medico_executor_imp),pls_obter_dados_prestador(a.nr_Seq_prestador,'N')),
		pls_obter_grau_participacao(a.nr_seq_grau_partic),
		a.vl_apresentado,
		dividir_sem_round(a.vl_apresentado,nvl(qt_apresentado_w,0)),
		b.nr_seq_apres,
		a.vl_calculado,
		a.vl_calculado
	from	pls_proc_participante	a,
		pls_grau_participacao	b,
		pls_conta_proc		c,
		pls_conta		d
	where	a.nr_seq_grau_partic 	= b.nr_sequencia(+)
	and	a.nr_seq_conta_proc	= c.nr_sequencia
	and	c.nr_seq_conta		= d.nr_sequencia
	and	c.nr_sequencia 		= nr_seq_item_w
	and	d.ie_tipo_guia		in ('5','4','6')
	and	not exists	(select	x.nr_sequencia
				from	pls_conta_proc x
				where	x.nr_seq_participante_hi	= a.nr_sequencia)
	order by nr_seq_apres;

begin
/* Obter dados conta */
select	nr_seq_prestador_exec,
	nr_seq_prestador,
	nr_seq_grau_partic,
	nr_seq_conta_referencia,
	cd_medico_executor,
	ie_tipo_guia,
	cd_guia_referencia,
	cd_guia,
	nr_seq_protocolo,
	ie_status,
	(select	substr(x.ds_valor_dominio,1,255)
	from	valor_dominio x
	where	x.cd_dominio	= 1746
	and	vl_dominio	= a.ie_tipo_guia),
	--obter_valor_dominio(1746,ie_tipo_guia),
	(select	substr(x.ds_valor_dominio,1,255)
	from	valor_dominio x
	where	x.cd_dominio	= 1961
	and	vl_dominio	= a.ie_status),
	--obter_valor_dominio(1961,ie_status),
	nr_seq_guia,
	ie_carater_internacao,
	dt_atendimento_referencia,
	nr_seq_segurado,
	pls_obter_dados_segurado(nr_seq_segurado,'C'),
	pls_obter_dados_segurado(nr_seq_segurado,'N'),
	nr_seq_cbo_saude,
	cd_estabelecimento,
	ie_gestacao,
	ie_aborto,
	ie_parto_normal,
	ie_complicacao_puerperio,
	ie_complicacao_neonatal,
	ie_parto_cesaria,
	ie_baixo_peso,
	ie_atend_rn_sala_parto,
	ie_transtorno,
	ie_obito_mulher
into	nr_seq_prestador_exec_w,
	nr_seq_prestador_solic_w,
	nr_seq_grau_partic_w,
	nr_seq_conta_referencia_w,
	cd_medico_executor_w,
	ie_tipo_guia_w,
	cd_guia_referencia_w,
	cd_guia_w,
	nr_seq_protocolo_w,
	ie_status_conta_w,
	ds_tipo_guia_w,
	ds_status_conta_w,
	nr_seq_guia_w,
	ie_carater_internacao_w,
	dt_atend_ref_w,
	nr_seq_segurado_w,
	cd_usuario_plano_w,
	nm_segurado_w,
	nr_seq_cbo_saude_w,
	cd_estabelecimento_w,
	ie_gestacao_w,
	ie_aborto_w,
	ie_parto_normal_w,
	ie_complicacao_puerperio_w,
	ie_complicacao_neonatal_w,
	ie_parto_cesaria_w,
	ie_baixo_peso_w,
	ie_atend_rn_sala_parto_w,
	ie_transtorno_w,
	ie_obito_mulher_w
from	pls_conta	a
where	nr_sequencia	= nr_seq_conta_p;

begin
select	nr_seq_apres
into	nr_seq_apres_w
from	pls_grau_participacao
where	nr_sequencia = nr_seq_grau_partic_w;
exception
when others then
	nr_seq_apres_w := null;
end;

ds_grau_participacao_w 	:= pls_obter_grau_participacao(nr_seq_grau_partic_w);

if	(cd_medico_executor_w is not null) then	
	nm_participante_w	:= Obter_nome_medico(cd_medico_executor_w,'N');
else
	nm_participante_w	:= pls_obter_dados_prestador(nr_seq_prestador_exec_w, 'N');
end if;

nm_prest_exec_w		:= pls_obter_dados_prestador(nr_seq_prestador_exec_w,'N');
nm_prest_solic_w	:= pls_obter_dados_prestador(nr_seq_prestador_solic_w,'N');

open C01;
loop
fetch C01 into
	nr_seq_item_w,
	ie_tipo_item_w,
	cd_item_w,
	ie_origem_proced_w,
	ie_tipo_despesa_w,
	ie_via_acesso_w,
	dt_item_w,
	tx_item_w,
	qt_apresentado_w,
	qt_liberado_w,
	vl_unitario_w,
	vl_total_w,
	vl_unitario_apres_w,
	vl_total_apres_w,
	vl_calculado_w,
	ie_status_w,
	cd_porte_anestesico_w,
	nr_auxiliares_w,
	cd_classificacao_sip_w,
	cd_classif_cred_w,
	cd_classif_deb_w,
	nr_nota_fiscal_w,
	nr_seq_prest_fornec_w,
	ds_fornecedor_w,
	nr_seq_proc_ref_w,
	dt_inicio_item_w,
	nr_seq_setor_atend_w,
	ds_setor_atendimento_w,
	ds_item_importacao_w,
	vl_taxa_servico_imp_w,
	vl_taxa_co_imp_w,
	vl_taxa_material_imp_w,
	vl_taxa_material_w,
	vl_taxa_servico_w,
	vl_taxa_co_w,
	vl_taxa_mat_material_w,
	vl_taxa_mat_material_imp_w,
	tx_intercambio_imp_w,
	tx_intercambio_w,
	vl_glosa_w,
	nr_seq_conta_proc_w,
	nr_seq_conta_mat_w,
	ie_valor_base_w,
	vl_calculado_hi_w,
	vl_calculado_co_w,
	vl_calculado_material_w,
	ie_repassa_medico_w,
	vl_calc_co_util_w,
	vl_calc_hi_util_w,
	vl_calc_mat_util_w;
exit when C01%notfound;
	begin
	vl_taxa_intercambio_imp_w	:= nvl(vl_taxa_servico_imp_w,0) + nvl(vl_taxa_co_imp_w,0) + nvl(vl_taxa_material_imp_w,0) + nvl(vl_taxa_mat_material_imp_w,0);
	vl_taxa_intercambio_w		:= nvl(vl_taxa_mat_material_w,0) + nvl(vl_taxa_servico_w,0) + nvl(vl_taxa_material_w,0) + nvl(vl_taxa_co_w,0);
	ie_nota_fiscal_w 		:= '';
	ie_conta_gerada_w		:= 'N';
	
	/*Diego OS - 282259 - Obter o prestador que receber� pelo pagamento*/
	begin
	select	nm_prestador_pgto,
		nr_seq_prestador_pgto
	into	nm_prestador_pag_w,
		nr_seq_prestador_pgto_w
	from	pls_conta_medica_resumo
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_tipo_item	= 'P'
	and	nvl(cd_procedimento, pls_obter_seq_codigo_material(nr_seq_material, '')) = cd_item_w
	and	nvl(nr_seq_prest_fornec_w,0) = 0
	and	((ie_situacao != 'I') or (ie_situacao is null))
	union
	select	pls_obter_dados_prestador(nr_seq_prest_fornec_w, 'N'),
		nr_seq_prest_fornec_w
	from	dual
	where	nvl(nr_seq_prest_fornec_w,0) > 0;
	exception
	when others then
		nr_seq_prestador_pgto_w	:= nr_seq_prestador_exec_w;
		nm_prestador_pag_w	:= nm_prest_exec_w;--pls_obter_dados_prestador(nr_seq_prestador_exec_w,'N');
	end;

	if	(nvl(vl_unitario_apres_w,0) = 0) then
		vl_unitario_apres_w	:= dividir_sem_round(vl_total_apres_w, qt_apresentado_w);
	end if;

	select	max(nm_prestador)
	into	nm_prestador_pag_w
	from	(select	b.nr_crm || ' - ' || nm_prestador_pag_w nm_prestador
		from	pls_prestador a,
			medico b
		where	a.nr_sequencia		= nr_seq_prestador_pgto_w
		and	nvl(a.cd_cgc,'X')	= 'X'
		and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		union
		select	a.cd_prestador || ' - ' || nm_prestador_pag_w nm_prestador
		from	pls_prestador a
		where	a.nr_sequencia	= nr_seq_prestador_pgto_w
		and	nvl(a.cd_cgc,'X') <> 'X');


	/*Diego OS 313317 - Obter o prestador exec */
	select	max(nm_prestador)
	into	nm_prestador_w
	from	(select	b.nr_crm || ' - ' || nm_prest_exec_w nm_prestador --pls_obter_dados_prestador(nr_seq_prestador_exec_w, 'N') nm_prestador
		from	pls_prestador a,
			medico b
		where	a.nr_sequencia 		= nr_seq_prestador_exec_w
		and	nvl(a.cd_cgc,'X')	= 'X'
		and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		union
		select	a.cd_prestador || ' - ' || nm_prest_exec_w nm_prestador --pls_obter_dados_prestador(nr_seq_prestador_exec_w, 'N') nm_prestador
		from	pls_prestador a
		where	a.nr_sequencia	= nr_seq_prestador_exec_w
		and	nvl(a.cd_cgc,'X') <> 'X');

	/*Diego OS 324844 - Obter o prestador solic */
	select	max(nm_prestador)
	into	nm_prestador_solic_w
	from	(select	b.nr_crm || ' - ' || nm_prest_solic_w nm_prestador --pls_obter_dados_prestador(nr_seq_prestador_solic_w, 'N') nm_prestador
		from	pls_prestador a,
			medico b
		where	a.nr_sequencia		= nr_seq_prestador_solic_w
		and	nvl(a.cd_cgc,'X')	= 'X'
		and	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		union
		select	a.cd_prestador || ' - ' || nm_prest_solic_w nm_prestador  --pls_obter_dados_prestador(nr_seq_prestador_solic_w, 'N') nm_prestador
		from	pls_prestador a
		where	a.nr_sequencia	= nr_seq_prestador_solic_w
		and	nvl(a.cd_cgc,'X') <> 'X');

	if	(ie_origem_proced_w is null) then
		ds_item_w	:= pls_obter_desc_material(cd_item_w);
	else
		ds_item_w	:= obter_descricao_procedimento(cd_item_w,ie_origem_proced_w);
	end if;
	ds_via_acesso_w		:= obter_valor_dominio(1268,ie_via_acesso_w);

	select	count(1)
	into	qt_glosa_w
	from	pls_conta_glosa
	where	((nr_seq_conta_proc = nr_seq_item_w
	and 	ie_tipo_item_w = 'P')
	or	(nr_seq_conta_mat = nr_seq_item_w
	and	ie_tipo_item_w = 'M'))
	and	ie_situacao <> 'I'
	and	rownum <= 1;

	select	count(1)
	into	qt_ocorrencia_w
	from	pls_ocorrencia_benef
	where	((nr_seq_proc = nr_seq_item_w
	and	ie_tipo_item_w = 'P')
	or	(nr_seq_mat = nr_seq_item_w
	and	ie_tipo_item_w = 'M'))
	and	nr_seq_conta = nr_seq_conta_p
	and	rownum <= 1;

	qt_ocorrencia_glosa_w	:= nvl(qt_ocorrencia_w,0) + nvl(qt_glosa_w,0);

	if	(qt_ocorrencia_glosa_w > 0) then
		ie_ocorrencia_glosa_w	:= 'S';
	else
		ie_ocorrencia_glosa_w	:= null;
	end if;

	if	(nvl(nr_seq_guia_w,0) > 0) then
		select	decode(ie_status, 1, 'S', 'N')
		into	ie_autorizado_w
		from	pls_guia_plano
		where	nr_sequencia = nr_seq_guia_w;
	end if;

	/*Obter exig�ncia de materiais*/
	if	(ie_tipo_item_w = 'M') then
		if	(nvl(nr_nota_fiscal_w,0) <> 0)then
			ie_exige_nf_w	:= 'S';	--Entregue
		else
			pls_obter_mat_restricao_data(cd_item_w,nvl(dt_item_w,dt_atend_ref_w),nr_seq_restricao_w);

			ie_exige_nf_w := 'N';

			if	(nvl(nr_seq_restricao_w,0) > 0) then
				select	nvl(ie_nota_fiscal,'N')
				into	ie_nota_fiscal_w
				from	pls_material_restricao
				where	nr_sequencia	= nr_seq_restricao_w;

				if	(ie_nota_fiscal_w = 'S') then
					ie_exige_nf_w := 'E';
				end if;
			end if;
		end if;
	else
		ie_exige_nf_w := '';
	end if;

	select	(nvl(max(nr_identificador),0) + 1)
	into	nr_identificador_w
	from	w_pls_resumo_conta
	where	nr_seq_analise = nr_seq_analise_p;

	nm_prestador_exec_w	:= nm_prest_exec_w;--pls_obter_dados_prestador(nr_seq_prestador_exec_w,'N');

	select	w_pls_resumo_conta_seq.nextval
	into	nr_seq_item_analise_w
	from	dual;

	if	(ie_status_w = 'M') then
		ie_pagamento_w	:= 'L';
	else
		ie_pagamento_w	:= 'G';
	end if;

	if	(ie_tipo_item_w = 'P') then
		select	count(a.nr_sequencia)
		into	qt_partics_proc_w
		from	pls_proc_participante 	a,
			pls_conta_proc 		b
		where	a.nr_sequencia		= b.nr_seq_participante_hi
		and	a.nr_seq_conta_proc 	= nr_seq_conta_proc_w;		
		
		if	(nvl(qt_partics_proc_w,0) > 0) then
			ie_conta_gerada_w := 'S';
		end if;		
		
		if	(nvl(ie_conta_gerada_w,'S') = 'N') then			
		
			/*Obter se existe guias de HI no atendimento*/
			select	count(a.nr_sequencia)
			into	qt_partics_gerados_conta_w
			from	pls_conta_proc 	a,
				pls_conta 	b
			where	a.nr_seq_conta		= b.nr_sequencia
			and	a.nr_seq_proc_ref 	= nr_seq_conta_proc_w
			and	b.ie_tipo_guia		= '6'
			and	rownum	= 1;	

			
			if	(nvl(qt_partics_gerados_conta_w,0) = 0) then
				if	(nvl(qt_partics_gerados_conta_w,0) = 0) then
					select	count(nr_sequencia)
					into	qt_partics_gerados_conta_w
					from	pls_conta_proc
					where	nvl(nr_seq_participante_hi,0) > 0
					and	nr_sequencia = nr_seq_conta_proc_w
					and	rownum	= 1;

					if	(nvl(qt_partics_gerados_conta_w,0) = 0) then
						ie_custo_operacional_w := 'N';
					else
						ie_custo_operacional_w := 'S';
					end if;
				else
					ie_custo_operacional_w := 'N';
				end if;
			else
				ie_custo_operacional_w := 'S';
			end if;
		else
			ie_custo_operacional_w := 'S';
		end if;
	end if;
	
	/*Diego - Orientado pelo Sr� Adriano Geyer = No caso de haver participantes deve ser base somente o calculado.*/
	select	count(a.nr_sequencia)
	into	qt_partics_proc_w
	from	pls_proc_participante a
	where	a.nr_seq_conta_proc	= nr_seq_conta_proc_w;
	
	if	(nvl(vl_calculado_w,0) = 0) and	(nvl(vl_total_apres_w,0) > 0) then
		ie_valor_base_w := '1';
	elsif	((nvl(vl_total_apres_w,0) = 0) and (nvl(vl_calculado_w,0) > 0)/* or (nvl(qt_partics_proc_w,0) > 1) or (nvl(ie_conta_gerada_w,'N')= 'S')*/) then
		ie_valor_base_w := '2';	
	end if;
	
	select	count(*)
	into	qt_registro_w
	from	(select	1
		from	w_pls_resumo_conta a
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.nr_seq_conta_proc 	= nr_seq_conta_proc_w
		union all
		select	1
		from	w_pls_resumo_conta a
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.nr_seq_conta_mat	= nr_seq_conta_mat_w);
	
	if	(qt_registro_w = 0) then
		insert into w_pls_resumo_conta
			(nr_sequencia, nr_seq_conta, nr_seq_item,
			ds_tipo_despesa, ie_via_acesso, nr_seq_prestador_exec,
			dt_item, cd_item, ie_origem_proced,
			nr_seq_grau_partic, tx_item, ds_grau_participacao,
			qt_apresentado, qt_liberado, vl_unitario,
			vl_total, nr_seq_conta_referencia, cd_medico_executor,
			ie_tipo_guia, cd_guia_referencia, cd_guia,
			nr_seq_apres_prof, nm_usuario, dt_atualizacao,
			nm_usuario_nrec, dt_atualizacao_nrec, ds_item,
			nm_participante, ds_via_acesso,	nm_prestador,
			ds_tipo_guia,ie_status,vl_calculado,
			ie_ocorrencia_glosa, vl_unitario_apres, vl_total_apres,
			nr_seq_protocolo, ie_status_conta, nm_prestador_pagamento,
			ie_tipo_despesa, ie_tipo_item, ds_status_conta,
			nr_seq_analise, cd_porte_anestesico, nr_auxiliares,
			nr_seq_prestador_pgto, nr_seq_guia, ie_autorizado,
			cd_classificacao_sip, cd_classif_cred, cd_classif_deb,
			ie_carater_internacao, ie_exige_nf, nr_nota_fiscal,
			nr_seq_res_conta_princ, nr_seq_prestador_solic, nm_prestador_solic,
			nr_seq_prest_fornec, ds_fornecedor, ie_pagamento,
			nr_seq_segurado, nm_segurado, cd_usuario_plano,
			nm_prestador_exec, nr_identificador, nr_seq_cbo_saude,
			nr_seq_item_ref, dt_inicio_item, ds_setor_atendimento,
			nr_seq_setor_atend,ds_item_importacao, vl_taxa_intercambio_imp,
			vl_taxa_intercambio, tx_intercambio_imp, tx_intercambio,
			vl_glosa, ie_gestacao,ie_aborto,
			ie_parto_normal,ie_complicacao_puerperio, ie_complicacao_neonatal,
			ie_parto_cesaria, ie_baixo_peso,ie_atend_rn_sala_parto,
			ie_transtorno, ie_obito_mulher, nr_seq_conta_proc,
			nr_seq_conta_mat, ie_valor_base, ie_custo_operacional,
			vl_calculado_hi, vl_calculado_co, vl_calculado_material)
		values( nr_seq_item_analise_w, nr_seq_conta_p, nr_seq_item_w,
			ie_tipo_item_w || ie_tipo_despesa_w, ie_via_acesso_w, nr_seq_prestador_exec_w,
			dt_item_w, cd_item_w, ie_origem_proced_w,
			nr_seq_grau_partic_w, tx_item_w, ds_grau_participacao_w,
			nvl(qt_apresentado_w,1), nvl(qt_liberado_w,0), nvl(vl_unitario_w,0),
			nvl(vl_total_w,0), nr_seq_conta_referencia_w, cd_medico_executor_w,
			ie_tipo_guia_w, cd_guia_referencia_w,cd_guia_w,
			nvl(nr_seq_apres_w,0),nm_usuario_p, sysdate,
			nm_usuario_p, sysdate, ds_item_w,
			nm_participante_w, ds_via_acesso_w, nm_prestador_w,
			ds_tipo_guia_w,'L', nvl(vl_calculado_w,0),
			ie_ocorrencia_glosa_w, nvl(vl_unitario_apres_w,0), nvl(vl_total_apres_w,0),
			nr_seq_protocolo_w, ie_status_conta_w, nm_prestador_pag_w,
			ie_tipo_despesa_w, ie_tipo_item_w, ds_status_conta_w,
			nr_seq_analise_p, cd_porte_anestesico_w, nr_auxiliares_w,
			nr_seq_prestador_pgto_w, nr_seq_guia_w, ie_autorizado_w,
			cd_classificacao_sip_w, cd_classif_cred_w, cd_classif_deb_w,
			ie_carater_internacao_w, ie_exige_nf_w, nr_nota_fiscal_w,
			nr_seq_conta_princ_p, nr_seq_prestador_solic_w, nm_prestador_solic_w,
			nr_seq_prest_fornec_w, ds_fornecedor_w, ie_pagamento_w,
			nr_seq_segurado_w, nm_segurado_w, cd_usuario_plano_w,
			nm_prestador_exec_w, nr_identificador_w, nr_seq_cbo_saude_w,
			nr_seq_proc_ref_w, dt_inicio_item_w, ds_setor_atendimento_w,
			nr_seq_setor_atend_w,ds_item_importacao_w, vl_taxa_intercambio_imp_w,
			vl_taxa_intercambio_w, tx_intercambio_imp_w, tx_intercambio_w,
			vl_glosa_w, ie_gestacao_w, ie_aborto_w,
			ie_parto_normal_w,ie_complicacao_puerperio_w,ie_complicacao_neonatal_w,
			ie_parto_cesaria_w,ie_baixo_peso_w,ie_atend_rn_sala_parto_w,
			ie_transtorno_w, ie_obito_mulher_w, nr_seq_conta_proc_w,
			nr_seq_conta_mat_w, ie_valor_base_w, ie_custo_operacional_w,
			vl_calc_hi_util_w, vl_calc_co_util_w, vl_calc_mat_util_w);

		if	(nvl(nr_seq_conta_proc_p,0) > 0) or
			(nvl(nr_seq_conta_mat_p,0) > 0) then
			if	(ie_status_w <> 'M') then
				pls_analise_status_item(nr_seq_conta_p,	nr_seq_conta_mat_w, nr_seq_conta_proc_w,
							nr_seq_analise_p, cd_estabelecimento_w,	nm_usuario_p,
							null);

				pls_analise_status_pgto(nr_seq_conta_p,	nr_seq_conta_mat_w, nr_seq_conta_proc_w,
							nr_seq_analise_p, cd_estabelecimento_w,	nm_usuario_p,
							null, null, null, 
							null);
			end if;
			
			nr_seq_item_criado_p := nr_seq_item_analise_w;
		end if;

		if	(ie_tipo_item_w = 'P') then
			open C02;
			loop
			fetch C02 into
				nr_seq_partic_w,
				cd_medico_w,
				nr_seq_cbo_saude_ww,
				nr_seq_conselho_w,
				nr_seq_grau_partic_ww,
				nr_seq_honorario_crit_w,
				nr_seq_prestador_w,
				nr_seq_prestador_pgto_w,
				vl_honorario_medico_w,
				vl_participante_w,
				ie_tipo_item_w,
				nm_participante_ww,
				ds_grau_participacao_ww,
				vl_apresentado_w,
				vl_unitario_apres_w,
				nr_seq_apres_ww,
				vl_calculado_w,
				vl_calc_hi_w;
			exit when C02%notfound;
				begin
				select	w_pls_resumo_conta_seq.nextval
				into	nr_seq_item_analise_w
				from	dual;

				select	nvl(nr_identificador_w,0) + 1
				into	nr_identificador_w
				from	dual;
				
				insert into w_pls_resumo_conta
					(nr_sequencia, nr_seq_conta, nr_seq_item,
					ds_tipo_despesa, ie_via_acesso, nr_seq_prestador_exec,
					dt_item, cd_item, ie_origem_proced,
					nr_seq_grau_partic, tx_item, ds_grau_participacao,
					qt_apresentado, qt_liberado, vl_unitario,
					vl_total, nr_seq_conta_referencia, cd_medico_executor,
					ie_tipo_guia, cd_guia_referencia, cd_guia,
					nr_seq_apres_prof,nm_usuario, dt_atualizacao,
					nm_usuario_nrec, dt_atualizacao_nrec, ds_item,
					nm_participante, ds_via_acesso,	nm_prestador,
					ds_tipo_guia,ie_status,vl_calculado,
					ie_ocorrencia_glosa, vl_unitario_apres, vl_total_apres,
					nr_seq_protocolo, ie_status_conta, nm_prestador_pagamento,
					ie_tipo_despesa, ie_tipo_item, ds_status_conta,
					nr_seq_analise, cd_porte_anestesico, nr_auxiliares,
					nr_seq_prestador_pgto, nr_seq_guia, ie_autorizado,
					cd_classificacao_sip, cd_classif_cred, cd_classif_deb,
					ie_carater_internacao, ie_exige_nf, nr_nota_fiscal,
					nr_seq_res_conta_princ, nr_seq_prestador_solic, nm_prestador_solic,
					nr_seq_prest_fornec, ds_fornecedor, ie_pagamento,
					nr_seq_segurado, nm_segurado, cd_usuario_plano,
					nm_prestador_exec, nr_identificador, nr_seq_partic_proc,
					nr_seq_cbo_saude, nr_seq_item_ref, nr_seq_setor_atend,
					ds_setor_atendimento, dt_inicio_item,ie_gestacao,ie_aborto,
					ie_parto_normal,ie_complicacao_puerperio, ie_complicacao_neonatal,
					ie_parto_cesaria, ie_baixo_peso,ie_atend_rn_sala_parto,
					ie_transtorno, ie_obito_mulher, vl_glosa, 
					ie_valor_base, vl_calculado_hi)
				values( nr_seq_item_analise_w, nr_seq_conta_p, nr_seq_item_w,
					ie_tipo_item_w || ie_tipo_despesa_w, ie_via_acesso_w, nr_seq_prestador_exec_w,
					dt_item_w, cd_item_w, ie_origem_proced_w,
					nr_seq_grau_partic_ww, tx_item_w, ds_grau_participacao_ww,
					nvl(qt_apresentado_w,1), 0, 0,
					0, nr_seq_conta_referencia_w, cd_medico_w,
					ie_tipo_guia_w, cd_guia_referencia_w,cd_guia_w,
					nvl(nr_seq_apres_ww,0),nm_usuario_p, sysdate,
					nm_usuario_p, sysdate, ds_item_w,
					nm_participante_ww, ds_via_acesso_w, nm_prestador_w,
					ds_tipo_guia_w,'L', nvl(vl_calculado_w,0),
					ie_ocorrencia_glosa_w, nvl(vl_unitario_apres_w,0), nvl(vl_apresentado_w,0),
					nr_seq_protocolo_w, ie_status_conta_w, nm_prestador_pag_w,
					ie_tipo_despesa_w, ie_tipo_item_w, ds_status_conta_w,
					nr_seq_analise_p, cd_porte_anestesico_w, nr_auxiliares_w,
					nr_seq_prestador_pgto_w, nr_seq_guia_w, ie_autorizado_w,
					cd_classificacao_sip_w, cd_classif_cred_w, cd_classif_deb_w,
					ie_carater_internacao_w, ie_exige_nf_w, nr_nota_fiscal_w,
					nr_seq_conta_princ_p, nr_seq_prestador_solic_w, nm_prestador_solic_w,
					nr_seq_prest_fornec_w, ds_fornecedor_w, 'G',
					nr_seq_segurado_w, nm_segurado_w, cd_usuario_plano_w,
					nm_prestador_exec_w, nr_identificador_w, nr_seq_partic_w,
					nr_seq_cbo_saude_ww, nr_seq_proc_ref_w, nr_seq_setor_atend_w,
					ds_setor_atendimento_w, dt_inicio_item_w, ie_gestacao_w, ie_aborto_w,
					ie_parto_normal_w,ie_complicacao_puerperio_w,ie_complicacao_neonatal_w,
					ie_parto_cesaria_w,ie_baixo_peso_w,ie_atend_rn_sala_parto_w,
					ie_transtorno_w, ie_obito_mulher_w, vl_glosa_w, 
					'2', vl_calc_hi_w);

				if	(nvl(nr_seq_conta_proc_p,0) > 0) then
					pls_analise_status_item(nr_seq_conta_p,	null, null,
								nr_seq_analise_p, cd_estabelecimento_w,	nm_usuario_p,
								nr_seq_partic_w);

					pls_analise_status_pgto(nr_seq_conta_p,	null, null,
								nr_seq_analise_p, cd_estabelecimento_w,	nm_usuario_p,
								nr_seq_partic_w, null, null, 
								null);
				end if;
				end;
			end loop;
			close C02;
		end if;
	end if;

	end;
end loop;
close C01;

end pls_gerar_w_resumo_conta;
/
