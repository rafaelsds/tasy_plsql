create or replace
procedure proj_duplicar_TP(	nr_sequencia_p	number,
			nm_usuario_p	varchar2) is

nr_sequencia_w		number(10);
nr_seq_roteiro_w		number(10);
nr_seq_rot_cli_w		number(10);
nr_seq_item_w		number(10);

Cursor c01 is
	select	a.nr_seq_roteiro
	from	proj_tp_cli_rot a
	where	a.nr_seq_cliente = nr_sequencia_p;

begin

select	proj_tp_cliente_seq.nextval
into	nr_sequencia_w
from	dual;

insert into proj_tp_cliente(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nr_seq_cliente,
	dt_prev_inic,
	dt_prev_fim,
	dt_real_inic,
	dt_real_fim,
	nr_seq_proj)
select	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	nr_seq_cliente,
	dt_prev_inic,
	dt_prev_fim,
	dt_real_inic,
	dt_real_fim,
	nr_seq_proj
from	proj_tp_cliente
where	nr_sequencia = nr_sequencia_p;

open c01;
loop
fetch c01 into
	nr_seq_roteiro_w;
exit when c01%notfound;
	begin
	select	proj_tp_cli_rot_seq.nextval
	into	nr_seq_rot_cli_w
	from	dual;	

	insert into proj_tp_cli_rot(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_seq_roteiro,
		nr_seq_cliente)
	values (	nr_seq_rot_cli_w,
		sysdate,
		nm_usuario_p,
		nr_seq_roteiro_w,
		nr_sequencia_w);

	proj_gerar_rot_item(nr_seq_rot_cli_w, nr_seq_roteiro_w, nm_usuario_p);
	end;
end loop;
close c01;

commit;

end proj_duplicar_TP;
/