create or replace
procedure pls_liberar_lanc_programado
			(	nr_seq_lanc_mens_p	number,
				ie_opcao_p		varchar2,
				ie_altera_mes_p		varchar2,
				nm_usuario_p		varchar2) is
/*
ie_opcao_p
	L = Liberar
	D = Desfazer liberacao
*/
vl_apropriacao_w		pls_lancamento_mens_aprop.vl_apropriacao%type;
vl_lancamento_w			pls_lancamento_mensalidade.vl_lancamento%type;
dt_liberacao_w			pls_lancamento_mensalidade.dt_liberacao%type;
qt_aprop_w			pls_integer;
qt_apropriacao_duplicada_w	pls_integer;

begin

if	(ie_opcao_p = 'L') then
	
	select	dt_liberacao,
		vl_lancamento
	into	dt_liberacao_w,
		vl_lancamento_w
	from	pls_lancamento_mensalidade
	where	nr_sequencia = nr_seq_lanc_mens_p;
	
	if	(vl_lancamento_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(834522,'NR_SEQ_LANCAMENTO='||nr_seq_lanc_mens_p);
		/*Mensagem: Nao e permitido liberar lancamento sem valor. Favor verifique o lancamento NR_SEQ_LANCAMENTO.*/
	end if;	
	
	if	(dt_liberacao_w is null) then
		select	count(1)
		into 	qt_aprop_w
		from 	pls_lancamento_mens_aprop
		where	nr_seq_lancamento = nr_seq_lanc_mens_p;
	
		if 	(qt_aprop_w > 0) then
			select	nvl(sum(vl_apropriacao),0)
			into 	vl_apropriacao_w
			from 	pls_lancamento_mens_aprop
			where	nr_seq_lancamento = nr_seq_lanc_mens_p;
			
			if	(nvl(vl_lancamento_w,0) <> vl_apropriacao_w) then
				wheb_mensagem_pck.exibir_mensagem_abort(312702,'NR_SEQ_LANCAMENTO='||nr_seq_lanc_mens_p);
				/* Mensagem: Valor total da apropriacao precisa ser o mesmo valor do lancamento. Favor verificar lancamento NR_SEQ_LANCAMENTO. */
			end if;
		end if;
		
	select  count(*)
	into	qt_apropriacao_duplicada_w
	from    pls_lancamento_mens_aprop a
	where   a.nr_seq_lancamento = nr_seq_lanc_mens_p
	and     exists (select  1
                from    pls_lancamento_mens_aprop b
                where   b.nr_seq_lancamento = nr_seq_lanc_mens_p
                and     b.nr_seq_centro_apropriacao = a.nr_seq_centro_apropriacao
                and     b.nr_sequencia <> a.nr_sequencia);
		
	if(nvl(qt_apropriacao_duplicada_w,0) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1112833,'NR_SEQ_LANCAMENTO='||nr_seq_lanc_mens_p);
	/* Nao e permitido liberar lancamento com apropriacao repetida. Favor verificar o lancamento #@NR_SEQ_LANCAMENTO#@.*/
	end if;
		
		update	pls_lancamento_mensalidade
		set	dt_liberacao		= sysdate,
			nm_usuario_liberacao	= nm_usuario_p,
			dt_mes_competencia	= decode(ie_altera_mes_p,'S',add_months(dt_mes_competencia,1),dt_mes_competencia)
		where	nr_sequencia 		= nr_seq_lanc_mens_p;
	end if;
elsif	(ie_opcao_p = 'D') then
	update	pls_lancamento_mensalidade
	set	dt_liberacao		= null,
		nm_usuario_liberacao	= null
	where	nr_sequencia 		= nr_seq_lanc_mens_p;
end if;

end pls_liberar_lanc_programado;
/
