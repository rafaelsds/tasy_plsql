create or replace
procedure insert_w_item_reversao_glosa(	nr_seq_propaci_p	number,
					nr_seq_matpaci_p	number,
					nr_seq_item_p		number,
					nm_usuario_p		varchar2) is
begin
if	((nr_seq_propaci_p	is not null) or
	(nr_seq_matpaci_p	is not null)) and
	(nr_seq_item_p		is not null) and
	(nm_usuario_p		is not null) then
	begin
	insert	into w_item_reversao_glosa
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_propaci,
		nr_seq_matpaci,
		nr_seq_item)
	values	(w_item_reversao_glosa_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_propaci_p,
		nr_seq_matpaci_p, 
		nr_seq_item_p);	
	end;
end if;	
commit;

end insert_w_item_reversao_glosa;
/