create or replace 
procedure OBTER_DADOS_EMAIL(nr_seq_contrato_p	number,
							nm_usuario_p		varchar2,
							ds_email_retorno_p 	out varchar2) is
									

qt_comunic_w		number(10);
nr_seq_regra_w		number(10);
ds_email_w			varchar2(4000);
nr_seq_email_lib_w	number(10);
cd_pessoa_resp_w	number(10);
ds_texto_w			long;		

begin

select 	nvl(count(*),0)
into	qt_comunic_w
from 	padrao_comunic_contrato 
where 	ie_situacao = 'A';

if	(qt_comunic_w > 0) then
	select	max(nr_sequencia) 
	into	nr_seq_regra_w
	from 	padrao_comunic_contrato 
	where 	ie_situacao = 'A';
	
	converte_rtf_html(' select ds_comunicado from padrao_comunic_contrato where nr_sequencia = :nr_seq ',nr_seq_regra_w,nm_usuario_p,nr_seq_email_lib_w);
	
	select 	ds_texto ds_texto
	into	ds_texto_w
	from 	tasy_conversao_rtf 
	where 	nr_sequencia = nr_seq_email_lib_w;

	select 	cd_pessoa_resp cd_resp
	into	cd_pessoa_resp_w
	from 	contrato 
	where 	nr_sequencia = nr_seq_contrato_p;
	
	if	(cd_pessoa_resp_w <> '') then
		select 	obter_compl_pf(cd_pessoa_resp_w,1,'M') 
		into	ds_email_w
		from 	dual;		
	end if;
	
	if	(ds_email_w <> '') then
		ds_email_retorno_p := ds_email_w;
	end if;
	
end if;

end	OBTER_DADOS_EMAIL;
/