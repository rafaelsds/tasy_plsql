create or replace
procedure reg_record_revision_pdf_hash (	nr_seq_revision_p			number,
											ds_hash_pdf_p				varchar2,
											nm_usuario_p				varchar2 ) is 

begin

if (nr_seq_revision_p is not null) then

	update	reg_version_revision
	set		nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate,
			ds_hash_pdf = ds_hash_pdf_p
	where	nr_sequencia = nr_seq_revision_p;

end if;

commit;

end reg_record_revision_pdf_hash;
/