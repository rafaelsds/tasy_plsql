create or replace
procedure altera_obs_movto_banco(
			nr_seq_movto_p number,
			ds_observacao_p varchar2) is 

begin

if (nr_seq_movto_p is not null) then
	begin
	update   movto_banco_pend 
	set      ds_observacao  = substr(ds_observacao_p,1,4000)
	where    nr_sequencia   = nr_seq_movto_p;
	commit;
	end;	
end if;
end altera_obs_movto_banco;
/
