create or replace
procedure aceita_ordem_compra_web(	nr_ordem_compra_p			number,
				nm_usuario_aceite_p			varchar2,
				nm_usuario_p				Varchar2) is 

nr_seq_regra_w				number(10);
cd_estabelecimento_w			number(4);
cd_cgc_fornecedor_w			varchar2(14);
ds_razao_social_w				varchar2(100);
ds_fantasia_w				varchar2(100);
ds_fornecedor_w				varchar2(100);
ds_email_origem_w				varchar2(255);
ds_email_destino_w				varchar2(255);
ds_email_solicitante_w			varchar2(255);
ds_email_comprador_w			varchar2(255);
ds_assunto_w				varchar2(255);
ds_mensagem_w				varchar2(2000);
ie_usuario_w				varchar2(3);
ds_email_compras_w			varchar2(255);
ds_email_adicional_w			varchar2(2000);
ie_momento_envio_w			varchar2(1);
ds_email_remetente_w			varchar2(255);

begin
if (nr_ordem_compra_p is not null) then
	begin
	update  ordem_compra
	set	dt_aceite      	= sysdate,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p,
		nm_usuario_aceite 	= nm_usuario_aceite_p
	where	nr_ordem_compra 	= nr_ordem_compra_p
	and	dt_aceite is null;

	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_p;

	select	nvl(max(nr_sequencia),0),
		nvl(max(ie_usuario),'U'),
		nvl(max(ds_email_remetente),'X'),
		max(replace(ds_email_adicional,',',';')),
		nvl(max(ie_momento_envio),'I')
	into	nr_seq_regra_w,
		ie_usuario_w,
		ds_email_remetente_w,
		ds_email_adicional_w,
		ie_momento_envio_w
	from	regra_envio_email_compra
	where	ie_tipo_mensagem = 19
	and	ie_situacao = 'A'
	and	cd_estabelecimento = cd_estabelecimento_w
	and	obter_se_envia_email_regra(nr_ordem_compra_p, 'OC', 19, cd_estabelecimento_w) = 'S';
	
	if	(nr_seq_regra_w > 0) then
		select	substr(obter_dados_pf_pj(null,obter_cgc_estabelecimento(cd_estabelecimento),'N'),1,100) ds_razao_social,
			substr(obter_dados_pf_pj(null,obter_cgc_estabelecimento(cd_estabelecimento),'F'),1,100) ds_fantasia,
			substr(obter_dados_pf_pj(null,cd_cgc_fornecedor,'F'),1,100) ds_fornecedor,
			substr(obter_dados_pf_pj_estab(cd_estabelecimento,null,cd_cgc_fornecedor,'M'),1,100) ds_email_origem,
			substr(obter_dados_usuario_opcao(obter_usuario_pessoa(cd_pessoa_solicitante),'E'),1,255) ds_email_solicitante,
			substr(obter_dados_usuario_opcao(obter_usuario_pessoa(cd_comprador),'E'),1,255) ds_email_comprador
		into	ds_razao_social_w,
			ds_fantasia_w,
			ds_fornecedor_w,
			ds_email_origem_w,
			ds_email_solicitante_w,
			ds_email_comprador_w
		from	ordem_compra
		where	nr_ordem_compra = nr_ordem_compra_p;

		select	substr(
			replace(
			replace(
			replace(
			replace(
			replace(
			replace(ds_assunto,
			'@fantasia_pj',ds_fantasia_w),
			'@razao_pj',ds_razao_social_w),
			'@ordem',nr_ordem_compra_p),
			'@usuario_aceite',nm_usuario_aceite_p),
			'@data',sysdate),
			'@fornecedor',ds_fornecedor_w),1,255),
			substr(
			replace(
			replace(
			replace(
			replace(
			replace(
			replace(ds_mensagem_padrao,
			'@fantasia_pj',ds_fantasia_w),
			'@razao_pj',ds_razao_social_w),
			'@ordem',nr_ordem_compra_p),
			'@usuario_aceite',nm_usuario_aceite_p),
			'@data',sysdate),
			'@fornecedor',ds_fornecedor_w),1,2000)
		into	ds_assunto_w,
			ds_mensagem_w
		from	regra_envio_email_compra
		where	nr_sequencia = nr_seq_regra_w;
		
		ds_email_destino_w := ds_email_solicitante_w;
		
		if	(ie_usuario_w = 'C') then
			select	ds_email
			into	ds_email_compras_w
			from	parametro_compras
			where	cd_estabelecimento = cd_estabelecimento_w;
			
			if	(ds_email_compras_w is not null) then
				ds_email_origem_w := ds_email_compras_w;
			end if;
		end if;
			
		if	(nvl(ds_email_comprador_w,'X') <> nvl(ds_email_solicitante_w,'X')) then
			if	(ds_email_destino_w is not null) then
				ds_email_destino_w := ds_email_destino_w || '; ' || ds_email_comprador_w;
			else
				ds_email_destino_w := ds_email_comprador_w;
			end if;
		end if;		
		
		if	(ds_email_adicional_w is not null) then
			if	(ds_email_destino_w is not null) then
				ds_email_destino_w := ds_email_destino_w || '; ' || ds_email_adicional_w;
			else
				ds_email_destino_w := ds_email_adicional_w;
			end if;
		end if;

		if	(ds_email_remetente_w <> 'X') then
			ds_email_origem_w	:= ds_email_remetente_w;
		end if;

		if	(ie_momento_envio_w = 'A') then
			begin

			sup_grava_envio_email(
				'OC',
				'19',
				nr_ordem_compra_p,
				null,
				null,
				ds_email_destino_w,
				nm_usuario_p,
				ds_email_origem_w,
				ds_assunto_w,
				ds_mensagem_w,
				cd_estabelecimento_w,
				nm_usuario_p);

			end;
		else		
			enviar_email(ds_assunto_w,ds_mensagem_w,ds_email_origem_w,ds_email_destino_w,nm_usuario_p,'M');
		end if;	

	end if;

	commit;
	end;	
end if;
end	aceita_ordem_compra_web;
/