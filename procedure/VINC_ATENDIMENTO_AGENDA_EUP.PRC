create or replace
procedure vinc_atendimento_agenda_EUP(	nr_seq_agenda_p			number,
					nr_atendimento_p		number,
					cd_pessoa_atendimento_p		varchar2,					
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number)
					is

nr_cirurgia_w		number(10,0);
ie_atendimento_w	varchar2(15);
cd_pessoa_fisica_w	varchar2(20);
nr_sequencia_autor_w	number(10);
nr_doc_convenio_w	varchar2(20);
cd_senha_w		varchar2(20);
nr_seq_interno_w	number(10);

cd_agenda_w		number(10);
hr_inicio_w		date;
cd_medico_w		varchar2(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
dt_cancelamento_w	date;
nr_seq_evento_w		number(10);
ie_gera_hist_vinc_w	varchar2(1);
ds_historico_w		varchar2(4000);
nm_paciente_w		varchar2(240);
nr_atendimento_w	number(10);
dt_agenda_w		varchar2(30);
nr_seq_proc_interno_w	number(10);
ds_observacao_w		varchar2(255);
ie_autoriza_autor_inter_w varchar2(1);
qt_dia_autorizado_w	autorizacao_convenio.qt_dia_autorizado%type;
dt_fim_vigencia_w	date;
nr_seq_estagio_w	estagio_autorizacao.nr_sequencia%type;	
nr_seq_autor_int_w	autorizacao_convenio.nr_sequencia%type;	
Cursor C01 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	Obter_seq_servico_agenda(nr_seq_agenda_p,nr_seq_proc_servico) = 'S'
	and	obter_se_regra_envio(a.nr_sequencia,nr_atendimento_p) = 'S'
	and	a.ie_evento_disp	= 'VAAG'
	and	nvl(a.ie_situacao,'A') = 'A';

begin

select	nvl(nr_cirurgia,0)
into	nr_cirurgia_w
from	agenda_paciente
where	nr_sequencia	=	nr_seq_agenda_p;

obter_param_usuario(871,84,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_atendimento_w);
obter_param_usuario(916,502,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gera_hist_vinc_w);
obter_param_usuario(916,1122,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_autoriza_autor_inter_w);


if	(ie_gera_hist_vinc_w = 'S') then
	select	substr(decode(max(cd_pessoa_fisica),null,max(nm_paciente),obter_nome_pf(max(cd_pessoa_fisica))),1,240),
		to_char(max(dt_agenda),'dd/mm/yyyy') || ' ' || to_char(max(hr_inicio),'hh24:mi:ss'),
		max(cd_procedimento),
		max(ie_origem_proced),
		max(nr_seq_proc_interno)
	into	nm_paciente_w,
		dt_agenda_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w
	from	agenda_paciente	
	where	nr_sequencia	= nr_seq_agenda_p;
end if;	

update	agenda_paciente
set	nr_atendimento			=	nr_atendimento_p,
	cd_pessoa_fisica		=	cd_pessoa_atendimento_p,
	dt_atualizacao			=	sysdate,
	nm_usuario			=	nm_usuario_p,
	dt_vinculacao_atendimento 	= 	sysdate,
	ie_status_agenda		= 	decode(cd_pessoa_fisica,null,'N',ie_status_agenda)
where	nr_sequencia			=	nr_seq_agenda_p;

if	(ie_gera_hist_vinc_w = 'S') then
	ds_historico_w:= Wheb_mensagem_pck.get_texto(306075,'NM_PACIENTE=' || nm_paciente_w || ';'||
														'NM_PACIENTE_ATENDIMENTO=' || obter_nome_pf(cd_pessoa_atendimento_p) || ';' ||
														'NR_ATENDIMENTO=' || nr_atendimento_p || ';' ||
														'DT_AGENDA=' || dt_agenda_w || ';' ||
														'PROCEDIMENTO=' || substr(obter_exame_agenda(cd_procedimento_w, ie_origem_proced_w, nr_seq_proc_interno_w),1,240));
	grava_historico_agenda_cir(nr_seq_agenda_p,ds_historico_w,Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C'),null,null,null,null,nm_usuario_p);
end if;	

update	prescr_medica
set	nr_atendimento	=	nr_atendimento_p,
	cd_pessoa_fisica=	cd_pessoa_atendimento_p,
	nm_usuario	=	nm_usuario_p,
	dt_atualizacao	=	sysdate
where	nr_seq_agenda	=	nr_seq_agenda_p;

update	autorizacao_convenio
set	nr_atendimento   =	nr_atendimento_p
where	nr_seq_agenda    =	nr_seq_agenda_p
and	nr_atendimento is null;


/* Francisco - OS 115283 - 23/01/2009 - Mudar guia/senha do atendimento se j� foi autorizado */
select	max(a.nr_sequencia)
into	nr_sequencia_autor_w
from	estagio_autorizacao b,
	autorizacao_convenio a
where	a.nr_seq_estagio	= b.nr_sequencia
and	b.ie_interno		= '10'
and	a.nr_seq_agenda		= nr_seq_agenda_p;

if	(nr_sequencia_autor_w is not null) then
	select	max(cd_senha),
		max(cd_autorizacao)
	into	cd_senha_w,
		nr_doc_convenio_w
	from	autorizacao_convenio
	where	nr_sequencia	= nr_sequencia_autor_w;

	select	max(nr_seq_interno)
	into	nr_seq_interno_w
	from	atend_categoria_convenio
	where	nr_atendimento					= nr_atendimento_p
	and	obter_atecaco_atendimento(nr_atendimento)	= nr_seq_interno;

	if	(nr_seq_interno_w is not null) then

		update	atend_categoria_convenio
		set	nr_doc_convenio	=	nvl(nr_doc_convenio,nr_doc_convenio_w),
			cd_senha	=	nvl(cd_senha,cd_senha_w)
		where	nr_atendimento	= 	nr_atendimento_p
		and	nr_seq_interno	=	nr_seq_interno_w;
	end if;
end if;
/* Fim 23/01/2009 */

commit;

if	(nr_cirurgia_w > 0) then
	update	cirurgia
	set	cd_pessoa_fisica	=	cd_pessoa_atendimento_p,
		nr_atendimento		=	decode(ie_atendimento_w,'S',nvl(nr_atendimento,nr_atendimento_p),nr_atendimento),
		nm_usuario		=	nm_usuario_p,
		dt_atualizacao		=	sysdate
	where	nr_cirurgia		=	nr_cirurgia_w;
	commit;
end if;


if	(nvl(ie_autoriza_autor_inter_w,'N') = 'S') then
	
	begin
		select	a.cd_autorizacao,
			a.cd_senha,
			a.qt_dia_autorizado,
			a.dt_fim_vigencia,
			a.nr_seq_estagio
		into	nr_doc_convenio_w,
			cd_senha_w,
			qt_dia_autorizado_w,
			dt_fim_vigencia_w,
			nr_seq_estagio_w
		from	autorizacao_convenio a,
			estagio_autorizacao b
		where	a.nr_seq_estagio 	= b.nr_sequencia
		and	a.ie_tipo_autorizacao 	= '5'
		and	a.nr_seq_agenda		= nr_seq_agenda_p
		and	b.ie_interno		= '10'
		and	a.nr_atendimento	= nr_atendimento_p
		and	rownum = 1;
	exception
	when others then
		nr_doc_convenio_w 	:= null;
		cd_senha_w	  	:= null;
		qt_dia_autorizado_w 	:= null;
		dt_fim_vigencia_w	:= null;
		nr_seq_estagio_w	:= null;
	end;
	
	if	(nr_doc_convenio_w is not null) or
		(cd_senha_w is not null) or
		(qt_dia_autorizado_w is not null) or
		(dt_fim_vigencia_w is not null) or
		(nr_seq_estagio_w is not null) then
		
		
		select	max(x.nr_sequencia)
		into	nr_seq_autor_int_w
		from	autorizacao_convenio x,
			estagio_autorizacao y
		where	x.nr_atendimento	= nr_atendimento_p
		and	x.ie_tipo_autorizacao   = '1'
		and	y.nr_sequencia 		= x.nr_seq_estagio
		and	y.ie_interno <> '10';
		
		if	(nr_seq_autor_int_w is not null) then
		
		
			update	autorizacao_convenio a
			set	a.cd_autorizacao 	= nvl(nr_doc_convenio_w,a.cd_autorizacao),
				a.cd_senha	 	= nvl(cd_senha_w,a.cd_senha),
				a.qt_dia_autorizado 	= nvl(qt_dia_autorizado_w,a.qt_dia_autorizado),
				a.dt_fim_vigencia	= nvl(dt_fim_vigencia_w,a.dt_fim_vigencia),
				a.dt_atualizacao	= sysdate,
				a.nm_usuario		= nm_usuario_p
				--a.nr_seq_estagio	= nvl(nr_seq_estagio_w,a.nr_seq_estagio)
			where	a.nr_sequencia		= nr_seq_autor_int_w;
			
			atualizar_autorizacao_convenio(nr_seq_autor_int_w, nm_usuario_p, nr_seq_estagio_w, 'N', 'N','S');
			
		end if;
		
	end if;
		
end if;

open C01;
loop
fetch C01 into	
	nr_seq_evento_w;
exit when C01%notfound;
	begin
	
	select	max(cd_agenda),
		max(hr_inicio),
		max(cd_medico),
		max(cd_procedimento),
		max(ie_origem_proced),
		max(dt_cancelamento),
		substr(max(ds_observacao),1,255)
	into	cd_agenda_w,
		hr_inicio_w,
		cd_medico_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		dt_cancelamento_w,
		ds_observacao_w
	from	agenda_paciente
	where	nr_sequencia = nr_seq_agenda_p;
	
	gerar_evento_agenda_trigger(	nr_seq_evento_w,
					nr_atendimento_p,
					cd_pessoa_atendimento_p,
					null,
					nm_usuario_p,
					cd_agenda_w, 
					hr_inicio_w, 
					cd_medico_w, 
					cd_procedimento_w,
					ie_origem_proced_w, 
					dt_cancelamento_w,
					null,
					NULL,
					NULL,
					null,
					null,
					'N',
					nr_seq_agenda_p,
					null,
					null,
					null,
					null,
					ds_observacao_w);
	end;
	
end loop;
close C01;

commit;

end vinc_atendimento_agenda_EUP;
/