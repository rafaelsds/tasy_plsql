create or replace
procedure gerar_necesidade_vaga_eup(nr_atendimento_p	Number,
									cd_convenio_p		out Number,
									cd_categoria_p		out Number,
									cd_plano_p		out Varchar2,
									qt_vagas_p		out Number,
									ds_pergunta_p		out Varchar2,
									nm_usuario_p		Varchar2) is 

begin
select	MAX(obter_convenio_atendimento(nr_atendimento)),
		MAX(obter_dados_categ_conv(nr_atendimento,'CA')),
		MAX(obter_dados_categ_conv(nr_atendimento,'P'))
into	cd_convenio_p,
		cd_categoria_p,
		cd_plano_p
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

select 	obter_se_possui_vaga(nr_atendimento_p)
into	qt_vagas_p
from	dual;

ds_pergunta_p := wheb_mensagem_pck.get_texto(120758);

commit;

end gerar_necesidade_vaga_eup;
/
