create or replace
procedure pls_assumir_grupo_auditoria
			(	nr_seq_grupo_auditor	number,
				nm_usuario_p		Varchar2) is 

ie_status_w			Varchar2(3);
nr_seq_auditoria_w		Number(10);
nm_usuario_exec_w		Varchar2(15);

begin

begin
	select	nm_usuario_exec
	into	nm_usuario_exec_w
	from	pls_auditoria_grupo
	where	nr_sequencia	= nr_seq_grupo_auditor;
exception
when others then
	nm_usuario_exec_w	:= null;
end;

if	(nm_usuario_exec_w	is null) then
	begin
		select	nr_seq_auditoria
		into	nr_seq_auditoria_w
		from	pls_auditoria_grupo
		where	nr_sequencia	= nr_seq_grupo_auditor;
	exception
	when others then
		nr_seq_auditoria_w	:= null;
	end;

	if	(nr_seq_auditoria_w	is not null) then
		begin
			select	ie_status
			into	ie_status_w
			from	pls_auditoria
			where	nr_sequencia	= nr_seq_auditoria_w;
		exception
		when others then
			ie_status_w	:= null;
		end;
		
		if	(ie_status_w	in('F','C')) then
			wheb_mensagem_pck.exibir_mensagem_abort(230314);
		end if;
	end if;


	update	pls_auditoria_grupo
	set	nm_usuario_exec		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_inicio_auditoria    	= sysdate
	where	nr_sequencia	= nr_seq_grupo_auditor;

	commit;
end if;
	
end pls_assumir_grupo_auditoria;
/