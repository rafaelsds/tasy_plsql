create or replace
procedure consistir_dados_agenda_serv_2(
		cd_pessoa_fisica_p		varchar2,
		cd_agenda_p			number,
		dt_agenda_p			date,
		cd_setor_atendimento_p		number,
		nr_seq_unidade_p		number,
		qt_idade_p			number,
		nr_seq_agenda_p			number,
		cd_convenio_p			number,
		ie_gerar_reserva_p		varchar2,
		cd_medico_p			varchar2,
		cd_procedimento_p		number,
		ie_origem_proced_p		number,
		nr_seq_proc_interno_p		number,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p		number,
		nm_paciente_p			varchar2,
		cd_categoria_p    		varchar2,
		ie_tipo_atendimento_p  		number,
		cd_usuario_convenio_p  		varchar2,
		cd_medico_solic_p   		varchar2,
		ds_msg_liber_cidade_p out	varchar2,
		ds_msg_idade_agenda_serv_p out	varchar2,
		nr_controle_secao_p out		number,
		nr_reserva_p out		varchar2,
		ds_erro_p out			varchar2,
		ds_msg_falta_pac_p out		varchar2,
		ds_msg_usuario_categoria_p out 	varchar2,
		ie_regra_bloqueio_p out 	varchar2) is 

ie_tipo_convenio_w		number(2);
ie_perm_bloq_agend_dif_esp_w	varchar2(1);
ds_consistencia_w		varchar2(2000);
ie_alerta_usuario_categ_w	varchar2(1);
ie_usuario_categoria_w		varchar2(1);
ds_observacao_categ_w		varchar2(255);
ie_clinica_categ_w		number(2);
nr_seq_regra_w 			number(10);
begin
if	(cd_agenda_p is not null) and
	(nr_seq_agenda_p is not null) and
	(nm_usuario_p is not null) then
	begin

	regra_agenda_liber_cidade(
		cd_pessoa_fisica_p,
		cd_agenda_p,
		ds_msg_liber_cidade_p);

	if	(ds_msg_liber_cidade_p is not null) then
		ds_msg_liber_cidade_p	:= ds_msg_liber_cidade_p || substr(obter_texto_tasy(47478, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	end if;

	consiste_idade_agenda_servico(
		cd_agenda_p,
		dt_agenda_p,
		qt_idade_p,
		ds_msg_idade_agenda_serv_p,
		nm_usuario_p);

	if	(ds_msg_idade_agenda_serv_p is not null) then
		ds_msg_idade_agenda_serv_p	:= ds_msg_idade_agenda_serv_p || substr(obter_texto_tasy(47481, wheb_usuario_pck.get_nr_seq_idioma),1,255);
	end if;
	
	insere_agenda_cons_cont_secao(dt_agenda_p, nr_seq_agenda_p, nm_usuario_p, nr_controle_secao_p);
	/*
	select	max(obter_controle_secao_agserv(nr_seq_agenda_p))
	into	nr_controle_secao_p
	from	dual;   */

	select	max(ie_tipo_convenio)
	into	ie_tipo_convenio_w
	from	convenio
	where	cd_convenio	= cd_convenio_p;

	if	(ie_tipo_convenio_w = 3) and
		(cd_pessoa_fisica_p is not null) then
		verifica_regra_laudo_sus(
			cd_pessoa_fisica_p,
			cd_agenda_p,
			ds_erro_p);
	end if;

	if	(ie_gerar_reserva_p is not null) and
		(ie_gerar_reserva_p = 'S') then
		begin
		select	substr(ageint_obter_reserva_pac(nr_seq_agenda_p, dt_agenda_p, cd_pessoa_fisica_p, cd_estabelecimento_p, 'S'), 1,20)
		into	nr_reserva_p
		from	dual;
		end;
	end if;

	if	(cd_medico_p is null) then	
		begin
		consistir_regra_medico_ageserv(
			nr_seq_agenda_p,
			cd_agenda_p,
			cd_estabelecimento_p,
			cd_procedimento_p,
			ie_origem_proced_p,
			nr_seq_proc_interno_p,
			nm_usuario_p);
		end;
	end if;

	obter_param_usuario(866, 164, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_perm_bloq_agend_dif_esp_w);

	if	(ie_perm_bloq_agend_dif_esp_w = 'S') then
		begin

		Agserv_verf_agend_lista_espera(	cd_agenda_p,cd_pessoa_fisica_p,nm_paciente_p,dt_agenda_p,ds_consistencia_w);
	
		if	(ds_consistencia_w is not null) then
			begin 

			Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);

			end;
		end if;

		end;
	end if;

	--Tramento parametro [183]
	obter_param_usuario(866, 183, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_perm_bloq_agend_dif_esp_w);
	if	(ie_perm_bloq_agend_dif_esp_w = 'S') then
		begin
		ageserv_consiste_falta_pac(
			cd_pessoa_fisica_p,
			dt_agenda_p,
			cd_agenda_p,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_msg_falta_pac_p);
		end;
	end if;

	-- Verifica a exisitencia do procedimento
	if 	(cd_procedimento_p is not null) and 
		(consiste_se_proc_existe(cd_procedimento_p) = 'N')then
		begin
		Wheb_mensagem_pck.exibir_mensagem_abort(105488);
		end;
	end if;	
	
	--Tramento parametro [186]
	obter_param_usuario(866, 186, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_perm_bloq_agend_dif_esp_w);
	if	(ie_perm_bloq_agend_dif_esp_w = 'S') then
		begin
		ageserv_consistir_setor_unid(
			dt_agenda_p,
			cd_setor_atendimento_p,
			nr_seq_unidade_p,
			nr_seq_agenda_p,
			nm_usuario_p,
			ds_consistencia_w);

		if	(ds_consistencia_w is not null) then
			begin
			Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);
			end;
		end if;
		end;
	end if;
	
	Ageserv_Consistir_Medico_Solic(cd_agenda_p, cd_medico_solic_p, nm_usuario_p);
	
	Consiste_lib_conv_ageserv(cd_agenda_p, cd_convenio_p, dt_agenda_p, 'S', nr_seq_agenda_p, ds_consistencia_w);
	if	(ds_consistencia_w is not null) then
		Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||ds_consistencia_w);
	end if;
	
	--Tratamento parametro [274]
	obter_param_usuario(866, 274, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_alerta_usuario_categ_w);
	if (ie_alerta_usuario_categ_w = 'S') then
		begin
		consiste_usuario_categoria(
			cd_estabelecimento_p,
			cd_convenio_p,
			cd_categoria_p,
			ie_tipo_atendimento_p,
			cd_usuario_convenio_p,
			ie_usuario_categoria_w,
			ie_regra_bloqueio_p,
			ds_observacao_categ_w,
			ie_clinica_categ_w,
			nr_seq_regra_w);

		if (ie_usuario_categoria_w <> 'S') then
			begin
			if (ie_usuario_categoria_w = 'N') then
				ds_msg_usuario_categoria_p := substr(obter_texto_tasy(294185, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			elsif (ie_usuario_categoria_w = 'C') then
				ds_msg_usuario_categoria_p := substr(obter_texto_tasy(294186, wheb_usuario_pck.get_nr_seq_idioma),1,255);
			end if;
			end;
		end if;
		end;
	end if;
	end;
end if;
commit;
end consistir_dados_agenda_serv_2;
/
