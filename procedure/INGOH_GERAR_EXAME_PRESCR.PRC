create or replace
procedure INGOH_Gerar_exame_prescr(cd_procedimento_p 	number,
				   cd_exame_p		varchar2,
				   cd_material_exame_p 	varchar2,
				   cd_senha_p		varchar2,
				   nr_guia_origem_p	number,
				   nr_atend_origem_p	number) is 

nr_sequencia_w		number(10);
ie_existe_exame_w	varchar2(1);
ie_existe_proc_w	varchar2(1);
nr_seq_interno_w	number(10);
sql_err_w		varchar2(2000);
nr_seq_exame_w		number(10);
nr_atendimento_w	number(10);
ie_tipo_atendimento_w	number(10);
cd_estabelecimento_w	number(10);
ie_tipo_convenio_w	number(10);
nr_doc_convenio_w	varchar2(30);
cd_setor_atendimento_w	number(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
ds_erro_w		varchar2(2000);
cd_convenio_w		number(10);
cd_categoria_w		varchar2(10);
nr_prescricao_w		number(14);
			
begin

if	(cd_procedimento_p 	 is not null) and
	(cd_material_exame_p 	 is not null) and
	(cd_exame_p		 is not null) and
	(nr_guia_origem_p	 is not null) and
	(cd_senha_p		 is not null) and
	(nr_atend_origem_p	 is not null) then
	begin
	
	select	nvl(nr_atendimento,0),
		nvl(ie_tipo_atendimento,0),
		nvl(cd_estabelecimento,0),
		nvl(ie_tipo_convenio,0)
	into	nr_atendimento_w,
		ie_tipo_atendimento_w,
		cd_estabelecimento_w,
		ie_tipo_convenio_w
	from	atendimento_paciente
	where 	nr_seq_ficha = nr_atend_origem_p;
	
	
	select	nvl(cd_convenio,0),
		nvl(cd_categoria,0),
		nvl(nr_doc_convenio,0)
	into	cd_convenio_w,
		cd_categoria_w,
		nr_doc_convenio_w
	from	atend_categoria_convenio
	where	nr_atendimento = nr_atendimento_w
	and	nr_seq_interno = obter_atecaco_atendimento(nr_atendimento);
	
	select	nvl(max(nr_prescricao),0)
	into	nr_prescricao_w
	from	prescr_medica
	where 	nr_seq_externo = nr_guia_origem_p;
	
	select 	max(nr_seq_exame)
	into	nr_seq_exame_w
	from	exame_laboratorio
	where	cd_exame = cd_exame_p;
	
--	OBTER_EXAME_LAB_CONVENIO(nr_seq_referencia_p,cd_convenio_w,cd_categoria_w,ie_tipo_atendimento_w,cd_estabelecimento_w,ie_tipo_convenio_w,cd_setor_atendimento_w,cd_procedimento_w,ie_origem_proced_w,ds_erro_w);
	
	select	nvl(max(nr_sequencia),0)+1
	into	nr_sequencia_w
	from	prescr_procedimento
	where   nr_prescricao = nr_prescricao_w;
	
	select  decode(nvl(count(*),0),0,'N','S')
	into	ie_existe_proc_w
	from	procedimento 
	where   cd_procedimento = cd_procedimento_p;
	
	select  Obter_Origem_Proced(cd_estabelecimento_w,ie_tipo_atendimento_w,ie_tipo_convenio_w,cd_convenio_w)
	into	ie_origem_proced_w
	from 	dual;
	
	if	(ie_existe_proc_w = 'S') then
		begin
			insert into prescr_procedimento	(cd_procedimento,
							cd_material_exame,
							nr_doc_convenio,
							cd_senha,
							nr_prescricao,
							dt_atualizacao,
							nr_sequencia,
							qt_procedimento,
							nm_usuario,
							ie_origem_proced,
							ie_origem_inf,
							nr_seq_exame) values
							(cd_procedimento_p,
							cd_material_exame_p,
							nr_doc_convenio_w,
							cd_senha_p,
							nr_prescricao_w,
							sysdate,
							nr_sequencia_w,
							1,
							'TASY_INTEGRACAO',
							ie_origem_proced_w,
							1,
							nr_seq_exame_w);
			exception
			when others then
			begin
				sql_err_w 		:= substr(SQLERRM, 1, 1800);
				insert into log_tasy(dt_atualizacao, nm_usuario, cd_log, ds_log) values (sysdate,'TASY_INTEGRACAO',88937, sql_err_w || ' - ' || to_char(cd_procedimento_p) || ' - ' || to_char(cd_material_exame_p) || ' - ' || to_char(nr_doc_convenio_w) || ' - ' || to_char(cd_senha_p) || ' - ' || to_char(nr_prescricao_w) || to_char(nr_guia_origem_p));
				commit;
			end;							
		end;
	else
		insert into log_tasy(dt_atualizacao, nm_usuario, cd_log, ds_log) values (sysdate,'TASY_INTEGRACAO',88937, 'PROCEDIMENTO OU EXAME N�O ENCONTRADO!' || ' - ' || to_char(cd_procedimento_p));
	end if;
	end;
else
	insert into log_tasy(dt_atualizacao, nm_usuario, cd_log, ds_log) values (sysdate,'TASY_INTEGRACAO',88937, 'Um ou mais dos campos n�o foram informados! ' || ' - ' || to_char(cd_procedimento_p) || ' - ' || to_char(cd_material_exame_p) || ' - ' || to_char(nr_doc_convenio_w) || ' - ' || to_char(cd_senha_p) || ' - ' || to_char(nr_guia_origem_p));
end if;
commit;

end INGOH_Gerar_exame_prescr;
/
