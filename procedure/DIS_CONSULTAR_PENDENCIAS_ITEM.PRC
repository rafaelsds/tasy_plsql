create or replace
procedure dis_consultar_pendencias_item(
			cd_material_p	number,
			cd_estabelecimento_p	number,
			qt_dias_consulta_p	number,
			nm_usuario_p	Varchar2) is 

dt_prevista_entrega_w	date;
dt_mesano_referencia_w	date;
qt_prevista_entrega_w	number(13,4);
qt_prevista_saida_w	number(13,4);
qt_dia_anterior_w		number(13,4);
qt_final_dia_w		number(13,4);

cd_material_ww		number(6);
cd_material_estoque_w	number(6);
ds_lista_material_w		varchar2(255);

Cursor C01 is
	select	trunc(sysdate) + dia
	from 	(select	rownum -1 dia
			from	tabela_sistema)
	where	rownum < qt_dias_consulta_p
	order by 1 asc;
	
Cursor C02 is
select	a.cd_material
from	material a
where	a.cd_material_estoque = cd_material_estoque_w;
	
begin
delete w_mapa_estoque where nm_usuario = nm_usuario_p;

select	dt_mesano_vigente
into	dt_mesano_referencia_w
from	parametro_estoque
where	cd_estabelecimento = cd_estabelecimento_p;

select	a.cd_material_estoque
into	cd_material_estoque_w
from	material a
where	a.cd_material = cd_material_p;

ds_lista_material_w := cd_material_estoque_w || ',';

open C02;
loop
fetch C02 into	
	cd_material_ww;
exit when C02%notfound;
	ds_lista_material_w := substr(ds_lista_material_w || cd_material_ww || ',',1,255);
end loop;
close C02;

qt_dia_anterior_w := obter_saldo_disp_estoque(cd_estabelecimento_p,cd_material_p,null,dt_mesano_referencia_w);
open C01;
loop
fetch C01 into	
	dt_prevista_entrega_w;
exit when C01%notfound;
	begin
	select	nvl(sum(obter_quantidade_convertida(b.cd_material,nvl(qt_prevista_entrega,0),b.cd_unidade_medida_compra,'UME') - 
			obter_quantidade_convertida(b.cd_material,nvl(qt_real_entrega,0),b.cd_unidade_medida_compra,'UME')),0)
	into	qt_prevista_entrega_w
	from	ordem_compra_item_entrega a,
		ordem_compra_item b,
		ordem_compra c
	where	a.nr_ordem_compra = b.nr_ordem_compra
	and	a.nr_item_oci = b.nr_item_oci
	and	a.nr_ordem_compra = c.nr_ordem_compra 
	and	a.dt_cancelamento is null
	and	c.nr_seq_motivo_cancel is null
	and	c.dt_baixa is null
	and	b.dt_reprovacao is null
	and	substr(obter_se_contido(b.cd_material,ds_lista_material_w),1,1) = 'S'
	and	a.dt_prevista_entrega = dt_prevista_entrega_w
	and	nvl(a.qt_prevista_entrega,0) > nvl(a.qt_real_entrega,0)
	and	c.cd_estabelecimento = cd_estabelecimento_p;
	
	select	nvl(sum(obter_quantidade_convertida(b.cd_material,nvl(qt_prevista_entrega,0),b.cd_unidade_venda,'UME')),0)
	into	qt_prevista_saida_w
	from	dis_pedido_item_entrega a,
		dis_pedido_item b,
		dis_pedido c
	where	a.nr_sequencia_item = b.nr_sequencia
	and	b.nr_seq_pedido = c.nr_sequencia
	and	b.nr_seq_motivo_cancel is null 
	and	c.nr_seq_motivo_cancel is null
	and	c.dt_baixa is null
	and	substr(obter_se_contido(b.cd_material,ds_lista_material_w),1,1) = 'S'
	and	a.dt_prevista_entrega = dt_prevista_entrega_w
	and	c.cd_estabelecimento = cd_estabelecimento_p;
	
	if	(qt_prevista_entrega_w <> 0) or
		(qt_prevista_saida_w <> 0) or
		(dt_prevista_entrega_w = trunc(sysdate)) then
		begin
		qt_final_dia_w := (qt_dia_anterior_w + qt_prevista_entrega_w - qt_prevista_saida_w);
		
		insert into	w_mapa_estoque(
			nr_sequencia,
			dt_atualizacao,
			cd_codigo,
			nm_usuario,
			qt_saldo_anterior,
			qt_entrada1,
			qt_saida1,
			qt_saldo_atual,
			ds_descricao)
		values (w_mapa_estoque_seq.nextval,
			dt_prevista_entrega_w,
			cd_material_p,
			nm_usuario_p,
			qt_dia_anterior_w,
			qt_prevista_entrega_w,
			qt_prevista_saida_w,
			qt_final_dia_w,
			ds_lista_material_w);
			
		qt_dia_anterior_w := qt_final_dia_w;
		end;
	end if;
	end;
end loop;
close C01;

commit;
end dis_consultar_pendencias_item;
/
