create or replace
procedure desfazer_finalizar_meta( nr_seq_meta_atend_p number,
                             ds_justificativa_p Varchar2) is

begin

if (nr_seq_meta_atend_p > 0) then

    update	tof_meta_atend
	set	dt_finalizacao = null,
	    ds_motivo_finalizacao = null,
		ie_finalizacao = null,
	    ds_justificativa = ds_justificativa_p
	where	nr_sequencia	= nr_seq_meta_atend_p;
	
	log_finalizar_meta(Obter_Usuario_Ativo, nr_seq_meta_atend_p, null, ds_justificativa_p, 'D');
end if;

commit;

end desfazer_finalizar_meta;
/