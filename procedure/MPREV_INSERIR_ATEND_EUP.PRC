create or replace procedure mprev_inserir_atend_eup(	nr_seq_atend_mprev_p	number,
									nr_seq_regra_eup_p		number,
									cd_pessoa_fisica_p		varchar,
									nm_usuario_p			varchar,
									nr_seq_segurado_p		pls_segurado.nr_sequencia%type,
									nr_atend_eup_p		out	number) is
										
					
cd_procedencia_w		atendimento_paciente.cd_procedencia%type;
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%type;
ie_carater_inter_sus_w	atendimento_paciente.ie_carater_inter_sus%type;
nr_seq_classificacao_w	atendimento_paciente.nr_seq_classificacao%type;
ie_tipo_atend_tiss_w	atendimento_paciente.ie_tipo_atend_tiss%type;
cd_setor_atendimento_w	atend_paciente_unidade.cd_setor_atendimento%type;
cd_tipo_acomodacao_w	atend_paciente_unidade.cd_tipo_acomodacao%type;
cd_unidade_basica_w		atend_paciente_unidade.cd_unidade_basica%type;
cd_unidade_compl_w		atend_paciente_unidade.cd_unidade_compl%type;
nr_atend_eup_w			atendimento_paciente.nr_atendimento%type;
cd_profissional_w		mprev_atendimento.cd_profissional%type;
/* Dados convenio */
cd_convenio_w			atend_categoria_convenio.cd_convenio%type;
cd_categoria_w			atend_categoria_convenio.cd_categoria%type;
cd_plano_convenio_w		atend_categoria_convenio.cd_plano_convenio%type;
cd_usuario_convenio_w	atend_categoria_convenio.cd_usuario_convenio%type;
dt_validade_carteira_w	atend_categoria_convenio.dt_validade_carteira%type;
cd_municipio_convenio_w	atend_categoria_convenio.cd_municipio_convenio%type;
cd_empresa_w			atend_categoria_convenio.cd_empresa%type;
cd_convenio_titular_w			atend_categoria_convenio.cd_convenio%type;
cd_categoria_titular_w			atend_categoria_convenio.cd_categoria%type;
cd_plano_convenio_titular_w		atend_categoria_convenio.cd_plano_convenio%type;
cd_usuario_convenio_titular_w	atend_categoria_convenio.cd_usuario_convenio%type;
dt_validade_carteira_titular_w	atend_categoria_convenio.dt_validade_carteira%type;
cd_empresa_titular_w			atend_categoria_convenio.cd_empresa%type;
dt_liberacao_w			pls_segurado.dt_liberacao%type;
dt_rescisao_w			pls_segurado.dt_rescisao%type;
dt_inicio_atend_w		mprev_atendimento.dt_inicio%type; /*Data informada na execucao do agendamento ( data de execucao informada no wdlg)*/
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
nr_seq_interno_w		number(10);
nr_sequencia_w			number(10);
nr_seq_participante_w	mprev_participante.nr_sequencia%type;
nr_sequencia_convenio_w	number(10);
ds_mensagem_retorno_w	varchar2(255);
ie_tipo_mensagem_w		varchar2(3);
nr_count_pls_segurado_w number(10);
cd_perfil_w					perfil.cd_perfil%type;
ie_cat_lib_pf_w			varchar2(1);
cd_medico_resp_w	mprev_regra_ger_atend.cd_medico_resp%type;

begin
select	cd_estabelecimento,
		cd_profissional,
		nvl(dt_inicio, sysdate)
into	cd_estabelecimento_w,
		cd_profissional_w,
		dt_inicio_atend_w
from	mprev_atendimento a
where	a.nr_sequencia	= nr_seq_atend_mprev_p;

/* Obter campos da regra que foi encontrada, para gerar o atendimento */
select	cd_procedencia,
		ie_tipo_atendimento,
		ie_carater_inter_sus,
		nr_seq_classificacao,
		ie_tipo_atend_tiss,
		cd_setor_atendimento,
		cd_tipo_acomodacao,
		cd_unidade_basica,
		cd_unidade_compl,
    cd_medico_resp
into	cd_procedencia_w,
		ie_tipo_atendimento_w,
		ie_carater_inter_sus_w,
		nr_seq_classificacao_w,
		ie_tipo_atend_tiss_w,
		cd_setor_atendimento_w,
		cd_tipo_acomodacao_w,
		cd_unidade_basica_w,
		cd_unidade_compl_w,
    cd_medico_resp_w
from	mprev_regra_ger_atend
where	nr_sequencia = nr_seq_regra_eup_p;

select	atendimento_paciente_seq.nextval
into	nr_atend_eup_w
from	dual;

/* Atendimento */
insert into atendimento_paciente
	(nr_atendimento,
	cd_pessoa_fisica, 
	cd_procedencia,
	ie_tipo_atendimento,
	ie_carater_inter_sus,
	ie_tipo_atend_tiss,
	nr_seq_classificacao,
	nm_usuario,
	dt_atualizacao,
	cd_estabelecimento,
	dt_entrada,
	cd_medico_resp,
	ie_permite_visita)
values	(nr_atend_eup_w,
	cd_pessoa_fisica_p,
	cd_procedencia_w,
	ie_tipo_atendimento_w,
	ie_carater_inter_sus_w,
	ie_tipo_atend_tiss_w,
	nr_seq_classificacao_w,
	nm_usuario_p,
	sysdate,
	cd_estabelecimento_w,
	dt_inicio_atend_w,
	nvl(cd_medico_resp_w, cd_profissional_w),
	'S');
	
/* Obter beneficiario da pessoa fisica, pode ter mais de um, pegar o primeiro ativo que achar */
select	max(nr_sequencia)
into	nr_seq_participante_w
from	mprev_participante a
where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;

if	(nr_seq_participante_w is not null) then
	
	--nr_seq_segurado_w	:= mprev_obter_benef_partic(nr_seq_participante_w);
	
	if	(nr_seq_segurado_p is not null) then
		/* Dados do convenio  segurado*/
		cd_convenio_w		    := obter_dados_seg_entrada_unica(nr_seq_segurado_p,'CV');
		cd_categoria_w		    := obter_dados_seg_entrada_unica(nr_seq_segurado_p,'CA');
		cd_plano_convenio_w	    := obter_dados_seg_entrada_unica(nr_seq_segurado_p,'PLC');
		cd_usuario_convenio_w	:= obter_dados_seg_entrada_unica(nr_seq_segurado_p,'CI');
		dt_validade_carteira_w	:= obter_dados_seg_entrada_unica(nr_seq_segurado_p,'VC');
		cd_empresa_w		    := obter_dados_seg_entrada_unica(nr_seq_segurado_p,'ER');
		
		if	(cd_convenio_w is null) or
			(cd_categoria_w is null) then
			/*Nao foi possivel obter o convenio e categoria do beneficiario para gerar o atendimento na Entrada unica de Pacientes.
			Por favor verifique em OPS - Cadastro de Regras/Produtos/OPS x Convenio (Convenio/Tasy) e OPS - Cadastro de Regras/Produtos/OPS x Categ*/
			wheb_mensagem_pck.exibir_mensagem_abort(292606);
		end if;
		
	else 
        begin
			select max(nr_sequencia)
			into nr_sequencia_convenio_w
			from	pessoa_titular_convenio
            where	cd_pessoa_fisica = 	cd_pessoa_fisica_p
            and	((trunc(sysdate)	<= 	trunc(dt_fim_vigencia)) or (dt_fim_vigencia is null))
            and	((trunc(sysdate)	>= 	trunc(dt_inicio_vigencia)) or (dt_inicio_vigencia is null));
		
			if (nr_sequencia_convenio_w is not null) then
				select cd_convenio,
					cd_categoria,
					cd_plano_convenio,
					cd_usuario_convenio,
					dt_validade_carteira,
					cd_empresa_refer  
				into cd_convenio_titular_w,
					cd_categoria_titular_w,
					cd_plano_convenio_titular_w,
					cd_usuario_convenio_titular_w,
					dt_validade_carteira_titular_w,
					cd_empresa_titular_w 
				from	pessoa_titular_convenio
				where nr_sequencia = nr_sequencia_convenio_w;
			else
				cd_convenio_titular_w           := null;
				cd_categoria_titular_w          := null;
				cd_plano_convenio_titular_w     := null;
				cd_usuario_convenio_titular_w   := null;
				dt_validade_carteira_titular_w  := null;
				cd_empresa_titular_w            := null;
			end if;
        end;
    
    	if (nr_sequencia_convenio_w is not null) then
      		/* Dados do convenio  do titular*/
      		cd_convenio_w		:= cd_convenio_titular_w;
      		cd_categoria_w		:= cd_categoria_titular_w;
      		cd_plano_convenio_w	:= cd_plano_convenio_titular_w;
    		cd_usuario_convenio_w	:= cd_usuario_convenio_titular_w;
      		dt_validade_carteira_w	:= dt_validade_carteira_titular_w;
      		cd_empresa_w		:= cd_empresa_titular_w;
    	else
            if (obter_utiliza_nom(wheb_usuario_pck.get_cd_estabelecimento) = 'S') then
				select count(*)
				into nr_count_pls_segurado_w
				from pls_segurado
				where cd_pessoa_fisica = cd_pessoa_fisica_p;

				if (nr_count_pls_segurado_w = 0) then
				  wheb_mensagem_pck.exibir_mensagem_abort(1072227);
				end if;
			end if;
            
			/* Dados do convenio  particular*/
            Obter_Convenio_Particular(cd_estabelecimento_w,	cd_convenio_w, cd_categoria_w);

      		if (cd_convenio_w is null) or
        	   (cd_categoria_w is null) then
        		--A pessoa nm_pessoa_fisica nao e beneficiario, nao sendo possivel obter o convenio e categoria da mesma de forma automatica.
        		--Sistema ira considerar como convenio particular, para isso deve estar preenchido o convenio e categoria particular nos parametros do faturamento.
       			wheb_mensagem_pck.exibir_mensagem_abort(338742, 'nm_pessoa_fisica='|| obter_nome_pf(cd_pessoa_fisica_p));
      		end if;
    	end if;

	end if;
else	
	-- A pessoa selecionada nao e participante da Medicina Preventiva, por favor, selecione um participante valido.
	wheb_mensagem_pck.exibir_mensagem_abort(254982);
end if;

cd_perfil_w := wheb_usuario_pck.get_cd_perfil;

select obter_se_categoria_lib_perf(cd_convenio_w,cd_categoria_w,cd_perfil_w)
into ie_cat_lib_pf_w
from dual;

if (ie_cat_lib_pf_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(1108453);
end if;

insert into atend_categoria_convenio
	(nr_seq_interno,
	nr_atendimento,
	cd_convenio,
	cd_categoria,
	cd_plano_convenio,
	cd_usuario_convenio,
	dt_validade_carteira,
	dt_inicio_vigencia,
	cd_empresa,
	nm_usuario_original,
	cd_municipio_convenio,
	nm_usuario,
	dt_atualizacao,
  cd_tipo_acomodacao)
values	(atend_categoria_convenio_seq.nextval,
	nr_atend_eup_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_convenio_w,
	cd_usuario_convenio_w,
	dt_validade_carteira_w,
	dt_inicio_atend_w,
	cd_empresa_w,
	nm_usuario_p,
	cd_municipio_convenio_w,
	nm_usuario_p,
	sysdate,
  cd_tipo_acomodacao_w);

/* Dados setor */
select	atend_paciente_unidade_seq.nextval
into	nr_seq_interno_w
from	dual;

select 	nvl(max(nr_sequencia),0) + 1
into 	nr_sequencia_w
from 	atend_paciente_unidade
where 	nr_atendimento = nr_atend_eup_w;

insert into atend_paciente_unidade
	(nr_sequencia,
	nr_seq_interno,
	nr_atendimento,
	cd_setor_atendimento,
	cd_tipo_acomodacao,
	cd_unidade_basica,
	cd_unidade_compl,
	dt_entrada_unidade,	
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	dt_saida_interno)
values	(nr_sequencia_w,
	nr_seq_interno_w,
	nr_atend_eup_w,
	cd_setor_atendimento_w,
	cd_tipo_acomodacao_w,
	cd_unidade_basica_w,
	cd_unidade_compl_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	to_date('30/12/2999','dd/mm/yyyy'));
	
nr_atend_eup_p	:= nr_atend_eup_w;

begin
gerar_lancamento_automatico(nr_atend_eup_p, null, 26, nm_usuario_p, null, null, null, null, null, null);
exception
when others then
	null;
end;

end mprev_inserir_atend_eup;
/
