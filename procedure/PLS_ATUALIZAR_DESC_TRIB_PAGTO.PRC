create or replace
procedure pls_atualizar_desc_trib_pagto(nr_seq_pag_prestador_p	pls_pagamento_prestador.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

nr_seq_vencimento_w		pls_pag_prest_vencimento.nr_sequencia%type;
vl_impostos_regra_w		pls_pag_prest_venc_trib.vl_imposto%type;
vl_deduzir_w			pls_pag_prest_venc_trib.vl_imposto%type;
vl_item_w			pls_pagamento_item.vl_item%type;
vl_desc_trib_w			pls_pagamento_item.vl_desc_trib%type;
vl_soma_movto_w			pls_evento_movimento.vl_movimento%type;
vl_soma_desc_trib_w		pls_evento_movimento.vl_desc_trib%type;
nr_seq_evento_movto_w		pls_evento_movimento.nr_sequencia%type;
cd_estabelecimento_w		pls_lote_pagamento.cd_estabelecimento%type;
vl_liquido_w			pls_pag_prest_vencimento.vl_liquido%type;
vl_liquido_estornar_w		pls_pag_prest_vencimento.vl_liquido%type;
vl_itens_nao_desc_impost_w	pls_pag_prest_venc_trib.vl_imposto%type;
vl_total_imposto_w		pls_pag_prest_venc_trib.vl_imposto%type;
vl_total_w			number(18,4);


Cursor	c01( nr_seq_pag_prestador_pc	pls_pagamento_prestador.nr_sequencia%type) is
	select	a.nr_sequencia,
		a.vl_item,
		a.nr_seq_evento,
		b.nr_prior_desc,
		nr_prior_desc_trib
	from	pls_evento 		b,
		pls_pagamento_item 	a
	where	a.nr_seq_pagamento	= nr_seq_pag_prestador_pc
	and	a.nr_seq_evento		= b.nr_sequencia
	and	b.nr_prior_desc_trib is not null
	and	a.vl_item < 0
	and	exists(	select	1
			from	pls_pag_prest_vencimento x,
				pls_pag_prest_venc_trib y
			where	x.nr_seq_pag_prestador = a.nr_seq_pagamento
			and	y.nr_seq_vencimento = x.nr_sequencia
			and	y.vl_imposto > 0)
	order by b.nr_prior_desc_trib desc;

Cursor 	c02( nr_seq_pagamento_item_pc	pls_pagamento_item.nr_sequencia%type) is
	select	nr_sequencia,
		vl_movimento
	from	pls_evento_movimento
	where	nr_seq_pagamento_item	= nr_seq_pagamento_item_pc;

begin
-- OBTEM A SEQU�NCIA E O VALOR L�QUIDO DO PAGAMENTO EM QUEST�O
select	max(nr_sequencia),
	nvl(max(vl_liquido),0)
into	nr_seq_vencimento_w,
	vl_liquido_w
from	pls_pag_prest_vencimento
where	nr_seq_pag_prestador = nr_seq_pag_prestador_p;

-- SE O VALOR L�QUIDO DO PAGAMENTO � NEGATIVO ENT�O O SISTEMA IR� RECALCULAR OS VALORES DOS EVENTOS DE DESCONTO QUE TIVEREM REGRA DE DESCONTO DE TRIBUTOS CADASTRADAS
if	(vl_liquido_w < 0) then

	-- SOMA TODOS OS EVENTOS DE DESCONTOS QUE N�O TEM "PRIORIDADE DESC TRIBUTO" AFIM DE DESCONT�-LOS DO L�QUIDO NEGATIVO
	select	nvl(sum(b.vl_item),0)
	into	vl_itens_nao_desc_impost_w
	from	pls_pagamento_item	b,
		pls_evento		a
	where	a.nr_sequencia		= b.nr_seq_evento
	and	b.nr_seq_pagamento	= nr_seq_pag_prestador_p
	and	a.ie_natureza		= 'D'
	and	a.nr_prior_desc_trib is null;

	-- DESCONTA DO VALOR L�QUIDO DO PAGAMENTO OS EVENTOS DE DESCONTOS QUE N�O TEM "PRIORIDADE DESC TRIBUTO" ASSIM OBTENDO O VALOR REAL QUE O SISTEMA DEVE APLICAR SOBRE OS EVENTOS COM "PRIORIDADE DESC TRIBUTO"
	vl_liquido_estornar_w	:= abs(vl_liquido_w) - abs(vl_itens_nao_desc_impost_w);

	-- CONTINUAR� O PROCESSO APENAS DE HOUVER ALGUM DESCONTO A APLICAR SOBRE OS EVENTOS COM "PRIORIDADE DESC TRIBUTO"
	if	(vl_liquido_estornar_w > 0) then

		-- OBTEM A SOMA DE TODOS OS IMPOSTOS DO VENCIMENTO
		select	nvl(sum(a.vl_imposto),0)
		into	vl_total_imposto_w
		from	tributo 		b,
			pls_pag_prest_venc_trib a
		where	a.ie_pago_prev		= 'V'
		and	a.cd_tributo		= b.cd_tributo
		and	a.nr_seq_vencimento	= nr_seq_vencimento_w;

		-- s� ir� retornar caso exista algum tributo gerado para o vencimento do prestador com valor de tributo maior
		-- que zero, pois n�o faz sentido os ajustes caso n�o tenha tributo ou o valor seja zero
		for r_c01_w in c01( nr_seq_pag_prestador_p ) loop

			vl_impostos_regra_w	:= null;

			select	nvl(sum(a.vl_imposto),0)
			into	vl_impostos_regra_w 
			from	tributo 		b,
				pls_pag_prest_venc_trib a
			where	a.ie_pago_prev		= 'V'
			and	a.cd_tributo		= b.cd_tributo
			and	a.nr_seq_vencimento	= nr_seq_vencimento_w
			and	exists(	select	1
					from	pls_evento_trib_desc	z
					where	z.nr_seq_evento		= r_c01_w.nr_seq_evento
					and	z.cd_tributo 		= b.cd_tributo
					and	nvl(z.ie_situacao,'I')	= 'A');

			-- OBTEM O VALOR RESTANTE DO "L�QUIDO A ESTORAR" DESCONTANDO TODOS OS IMPOSTOS, E SOMA APENAS OS IMPOSTOS QUE EST�O CONFIGURADOS PARA DESCONTAR SOBRE O EVENTO EM QUEST�O
			if	(nvl(vl_impostos_regra_w,0) > 0) or ((vl_liquido_estornar_w - vl_total_imposto_w) > 0) then
				vl_impostos_regra_w	:= nvl(vl_impostos_regra_w,0) + (vl_liquido_estornar_w - vl_total_imposto_w);
			else
				vl_impostos_regra_w	:= 0;
			end if;

			vl_deduzir_w	:= nvl(vl_impostos_regra_w,0);
			if	((r_c01_w.vl_item * -1) < vl_impostos_regra_w) then 
				vl_deduzir_w	:= (r_c01_w.vl_item * -1);
			end if;

			update	pls_pagamento_item
			set	vl_item		= vl_item + vl_deduzir_w,
				vl_desc_trib	= nvl(vl_desc_trib, 0) + vl_deduzir_w
			where	nr_sequencia	= r_c01_w.nr_sequencia
			returning vl_item into vl_total_w;
			
			-- Ap�s aplicar o desc do tributo, tem que atualizar a base de calculo dos tributos relacionados ao evento do item de pagamento.
			update	pls_pag_prest_venc_trib	x
			set	x.vl_base_calculo	= x.vl_base_calculo + nvl(vl_deduzir_w,0)
			where	x.nr_sequencia		in (	select	distinct a.nr_sequencia
								from	pls_pag_prest_venc_trib		a,
									pls_pag_prest_vencimento	b,
									pls_pagamento_item		c,
									pls_evento			d,
									pls_evento_tributo		e
								where	b.nr_sequencia			= a.nr_seq_vencimento
								and	c.nr_seq_pagamento		= b.nr_seq_pag_prestador
								and	d.nr_sequencia			= c.nr_seq_evento
								and	e.nr_seq_evento			= d.nr_sequencia
								and	a.cd_tributo			= e.cd_tributo
								and	c.nr_sequencia			= r_c01_w.nr_sequencia
								group by a.nr_sequencia);


			for r_c02_w in c02(r_c01_w.nr_sequencia) loop
				nr_seq_evento_movto_w	:= r_c02_w.nr_sequencia;
				
				update	pls_evento_movimento
				set	vl_movimento	= dividir_sem_round(vl_movimento, r_c01_w.vl_item) * vl_total_w
				where	nr_sequencia	= r_c02_w.nr_sequencia;
				
				update	pls_evento_movimento
				set	vl_desc_trib	= r_c02_w.vl_movimento - vl_movimento
				where	nr_sequencia	= r_c02_w.nr_sequencia;
			end loop;

			/* 
			comentado, ajustei para fazer o rateio dentro do cursor c02 aqui s� estava deixando os valores bagun�ados, jogando tudo
			select	vl_item,
				vl_desc_trib
			into	vl_item_w,
				vl_desc_trib_w
			from	pls_pagamento_item
			where	nr_sequencia = r_c01_w.nr_sequencia;
			
			select	nvl(sum(vl_movimento),0),
				nvl(sum(nvl(vl_desc_trib,0)),0)
			into	vl_soma_movto_w,
				vl_soma_desc_trib_w
			from	pls_evento_movimento
			where	nr_seq_pagamento_item	= r_c01_w.nr_sequencia;
			
			update	pls_evento_movimento
			set	vl_movimento = vl_movimento + (vl_item_w - vl_soma_movto_w),
				vl_desc_trib = vl_desc_trib + (vl_desc_trib_w - vl_soma_desc_trib_w)
			where	nr_sequencia = nr_seq_evento_movto_w;
			*/
			-- DIMINUI DO "L�QUIDO A ESTORAR" A REDU��O APLICADA SOBRE O EVENTO ANTERIOR, DESCONTA DO "L�QUIDO A ESTORAR" CADA DEDU��O APLICADA(LOOP ANTERIOR DO CURSOR)
			vl_liquido_estornar_w	:= vl_liquido_estornar_w - nvl(vl_deduzir_w,0);
			
			-- SE N�O H� MAIS VALOR A ESTORNAR O SISTEMA SAI DA APLICA��O DE DESCONTO
			if	(vl_liquido_estornar_w <= 0) then
				exit;
			end if;
		end loop;
		
		
		-- Ap�s atualizar a base de calculo dos impostos, o IR ou IRRF tem uma particularidade. Em alguns casos o vl produ��o fica negativo (!!!) devido
		-- ao sistema de apropria��o. Neste caso � apurado novamente o valor de produ��o para o IRRF com vl produ��o negativo.
		-- Ainda zera o valor do tributo, pois neste caso como foi oriundo de produ��o "negativa", s� dever� existir base de calculo, para ser composto em outro pagamento
		
		update	pls_pag_prest_venc_trib	x
		set	x.vl_imposto		= 0,
			x.vl_base_producao	= (	select	sum(a.vl_item)
							from	pls_pagamento_item	a
							where	a.nr_seq_pagamento	= nr_seq_pag_prestador_p
							and	exists (select	1
									from	pls_evento		b,
										pls_evento_tributo	c,
										tributo			d
									where	b.nr_sequencia		= a.nr_seq_evento
									and	c.nr_seq_evento		= b.nr_sequencia
									and	d.cd_tributo		= c.cd_tributo
									and	d.ie_tipo_tributo	= 'IR'))
		where	x.nr_sequencia		in (	select	distinct a.nr_sequencia
							from	pls_pag_prest_venc_trib		a,
								pls_pag_prest_vencimento	b,
								pls_pagamento_item		c,
								pls_evento			d,
								pls_evento_tributo		e,
								tributo				f
							where	b.nr_sequencia			= a.nr_seq_vencimento
							and	c.nr_seq_pagamento		= b.nr_seq_pag_prestador
							and	d.nr_sequencia			= c.nr_seq_evento
							and	e.nr_seq_evento			= d.nr_sequencia
							and	a.cd_tributo			= e.cd_tributo
							and	f.cd_tributo			= e.cd_tributo
							and	f.ie_tipo_tributo		= 'IR'
							and	c.nr_seq_pagamento		= nr_seq_pag_prestador_p
							group by a.nr_sequencia)
		and	x.vl_base_producao	< 0;
		
		-- Ap�s realizar o processo no IRRF, se ainda existir o IRRF com valor de imposto negativo, ele dever� ser excluido do pagamento.
		delete	pls_pag_prest_venc_trib	x
		where	x.nr_sequencia		in (	select	distinct a.nr_sequencia
							from	pls_pag_prest_venc_trib		a,
								pls_pag_prest_vencimento	b,
								pls_pagamento_item		c,
								pls_evento			d,
								pls_evento_tributo		e,
								tributo				f
							where	b.nr_sequencia			= a.nr_seq_vencimento
							and	c.nr_seq_pagamento		= b.nr_seq_pag_prestador
							and	d.nr_sequencia			= c.nr_seq_evento
							and	e.nr_seq_evento			= d.nr_sequencia
							and	a.cd_tributo			= e.cd_tributo
							and	f.cd_tributo			= e.cd_tributo
							and	f.ie_tipo_tributo		= 'IR'
							and	c.nr_seq_pagamento		= nr_seq_pag_prestador_p
							group by a.nr_sequencia)
		and	x.vl_imposto		< 0;
		

		select	max(b.cd_estabelecimento)
		into	cd_estabelecimento_w
		from	pls_lote_pagamento b,
			pls_pagamento_prestador a
		where	a.nr_seq_lote		= b.nr_sequencia
		and	a.nr_sequencia		= nr_seq_pag_prestador_p;

		pls_atualizar_valor_pag_prest(nr_seq_pag_prestador_p, nm_usuario_p, cd_estabelecimento_w);

		select	nvl(sum(nvl(vl_desc_trib,0)),0)
		into	vl_soma_desc_trib_w
		from	pls_pagamento_item
		where	nr_seq_pagamento	= nr_seq_pag_prestador_p;

		update	pls_pag_prest_vencimento
		set	vl_liquido	= vl_liquido + vl_soma_desc_trib_w,
			vl_vencimento	= vl_vencimento + vl_soma_desc_trib_w
		where	nr_sequencia	= nr_seq_vencimento_w;
	end if;
end if;

end pls_atualizar_desc_trib_pagto;
/
