CREATE OR REPLACE
PROCEDURE Atualizar_dependente_protocolo(nr_seq_paciente_p	number,
					nr_ciclo_p		number) is

nr_prescricao_w		Number(14);

Cursor C01 is
	select	nr_prescricao
	from	paciente_atendimento
	where	nr_seq_paciente	= nr_seq_paciente_p
	and	nr_ciclo	= nr_ciclo_p
	and	nvl(dt_real,dt_prevista) > sysdate
	and	nr_prescricao is not null;

BEGIN

delete	paciente_atendimento
where	nr_seq_paciente	= nr_seq_paciente_p
and	nr_ciclo	= nr_ciclo_p
and	nr_prescricao is null;

OPEN C01;
LOOP
FETCH C01 into
	nr_prescricao_w;
exit when c01%notfound;
	update	can_ordem_prod
	set	ie_suspensa	= 'S'
	where	nr_prescricao	= nr_prescricao_w;
END LOOP;
CLOSE C01;

update	paciente_atendimento
set	ie_exige_liberacao	= 'S'
where	nr_seq_paciente	= nr_seq_paciente_p
and	nr_prescricao is not null
and	nvl(dt_real,dt_prevista) > sysdate
and	nr_ciclo	= nr_ciclo_p;

END Atualizar_dependente_protocolo;
/
