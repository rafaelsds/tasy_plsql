create or replace
procedure Gerar_Evolucao_Escala_ECA(nr_sequencia_p	number,
									nm_usuario_p	Varchar2) is 			

nr_sequencia_w			number(10);
ds_item_w				varchar2(32000);
ds_resultado_w			varchar2(32000);
ds_evolucao_w			varchar2(32000);
ds_titulo_w				varchar2(32000);
ds_itens_w				varchar2(32000);
ds_compl_w				varchar2(32000);
cd_medico_w				varchar2(10);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w		varchar2(10);		
ie_inserir_w			boolean;	
nr_seq_tipo_avaliacao_w	number(10);
dt_avaliacao_w			date;
ds_autor_w				varchar2(255);
qt_pontuacao_w			number(10);
ds_gradacao_w			varchar2(100);
IE_TIPO_EVOLUCAO_GCA_w	varchar2(100);
cd_estabelecimento_w	number(10);
IE_TIPO_EVOLUCAO_w		varchar2(10);
cd_evolucao_w			number(10);
nr_seq_apres_w			number(10);
DS_OBSERVACAO_w			varchar2(4000);



Cursor C01 is
	
	select 	'    '||REPLACE(obter_descricao_padrao('GCA_ITEM','DS_ITEM',b.NR_SEQ_ITEM),CHR(13), ' ') ds_item,
		' '||REPLACE(obter_descricao_padrao('GCA_ITEM_RESULT','DS_RESULTADO',b.NR_SEQ_RESULT),CHR(10),' ') ds_resultado,
		(select	x.nr_seq_apres
		 from	gca_item x
		 where	x.nr_sequencia = b.nr_seq_item) nr_seq_apres
	from   	gca_atend_result b,
		gca_atendimento a
	where  	b.nr_seq_avaliacao = a.nr_Sequencia
	and	a.nr_sequencia = nr_sequencia_p
	and	obter_descricao_padrao('GCA_ITEM_RESULT','DS_RESULTADO',b.NR_SEQ_RESULT) is not null
	ORDER BY nr_seq_apres, ds_item;
	
	
	pragma autonomous_transaction;
	
begin


select	obter_pessoa_atendimento(nr_atendimento,'C'),
		nr_atendimento,
		dt_avaliacao,
		obter_valor_dominio(4562,IE_AUTOR),
		QT_PONTUACAO,
		obter_descricao_padrao('GCA_GRADACAO', 'DS_GRADACAO', NR_SEQ_GRADACAO),
		cd_pessoa_fisica,
		DS_OBSERVACAO
into	cd_pessoa_fisica_w,
		nr_atendimento_w,
		dt_avaliacao_w,
		ds_autor_w,
		qt_pontuacao_w,
		ds_gradacao_w,
		cd_medico_w,
		DS_OBSERVACAO_w
from	gca_atendimento
where	nr_sequencia	= nr_sequencia_p;

cd_estabelecimento_w	:= OBTER_ESTAB_ATEND(nr_atendimento_w);

select	max(IE_TIPO_EVOLUCAO_GCA)
into	IE_TIPO_EVOLUCAO_GCA_w
from	parametro_medico
where	cd_estabelecimento = cd_estabelecimento_w;


if	(IE_TIPO_EVOLUCAO_GCA_w	is not null) then


	ds_evolucao_w	:= wheb_rtf_pck.GET_CABECALHO;



	ds_titulo_w	:= wheb_rtf_pck.get_quebra_linha||wheb_rtf_pck.get_negrito(true)||'Avalia��o Di�ria do Enfermeiro'||wheb_rtf_pck.get_negrito(false);
	--ds_compl_w	:= wheb_rtf_pck.get_quebra_linha||wheb_rtf_pck.get_quebra_linha||wheb_rtf_pck.get_negrito(true)||'Data avalia��o :'||wheb_rtf_pck.get_negrito(false)||to_char(dt_avaliacao_w,'dd/mm/yyyy');
	ds_compl_w	:= ds_compl_w||wheb_rtf_pck.get_quebra_linha||wheb_rtf_pck.get_negrito(true) || obter_desc_expressao(296046) || ': ' || wheb_rtf_pck.get_negrito(false)||qt_pontuacao_w;

	if 	(ds_gradacao_w is not null) then
		ds_compl_w	:= ds_compl_w||wheb_rtf_pck.get_quebra_linha||wheb_rtf_pck.get_negrito(true) || obter_desc_expressao(305277)/*'N�vel compl assistencial: '*/||': '|| wheb_rtf_pck.get_negrito(false)||ds_gradacao_w;
	end if;

	if 	(ds_autor_w is not null) then
		ds_compl_w	:= ds_compl_w||wheb_rtf_pck.get_quebra_linha||wheb_rtf_pck.get_negrito(true) || obter_desc_expressao(297388) || ': ' || wheb_rtf_pck.get_negrito(false)||ds_autor_w;
	end if;

	ds_compl_w	:= ds_compl_w||wheb_rtf_pck.get_quebra_linha||wheb_rtf_pck.get_negrito(true) || obter_desc_expressao(306200) || ': ' || wheb_rtf_pck.get_negrito(false);

	ds_itens_w	:= null;

	open C01;
	loop
	fetch C01 into	
		ds_item_w,
		ds_resultado_w,
		nr_seq_apres_w;
	exit when C01%notfound;
		begin
		if	(trim(ds_item_w)	is not null	) and
			(trim(ds_resultado_w)	is not null	) then
			ds_itens_w	:= ds_itens_w ||wheb_rtf_pck.get_quebra_linha||wheb_rtf_pck.get_negrito(true)||rpad(ds_item_w,40,' ')||':'||wheb_rtf_pck.get_negrito(false)||ds_resultado_w;
		end if;		
		end;
	end loop;
	close C01;

	if	(ds_itens_w	is not null) then
		ds_evolucao_w	:= substr(ds_evolucao_w||ds_titulo_w||ds_compl_w||ds_itens_w,1,32000);
		ie_inserir_w	:= true;
	end if;
	
	if	(DS_OBSERVACAO_w	is not null) then
		ds_evolucao_w	:= ds_evolucao_w||wheb_rtf_pck.get_quebra_linha||wheb_rtf_pck.get_quebra_linha || obter_desc_expressao(753406) || ' ' || DS_OBSERVACAO_w;
	end if;

	ds_evolucao_w	:= ds_evolucao_w||wheb_rtf_pck.get_rodape;

	if	(ie_inserir_w) then
	
		select	max(IE_TIPO_EVOLUCAO)
		into	IE_TIPO_EVOLUCAO_w
		from	usuario
		where	nm_usuario = nm_usuario_p;
		
		select	evolucao_paciente_seq.nextval
		into	cd_evolucao_w
		from	dual;
		
		insert into evolucao_paciente(cd_evolucao,
									  dt_evolucao,
									  ie_tipo_evolucao,
									  cd_pessoa_fisica,
									  dt_atualizacao,
									  nm_usuario,
									  nr_atendimento,
									  ds_evolucao,					
									  dt_liberacao,
									  ie_evolucao_clinica,
									  cd_medico)
				values	(cd_evolucao_w,
						 sysdate,
						 nvl(IE_TIPO_EVOLUCAO_w,1),
						 cd_pessoa_fisica_w,
						 sysdate,
						 nm_usuario_p,
						 nr_atendimento_w,
						 ds_evolucao_w,					
						 null,
						 IE_TIPO_EVOLUCAO_GCA_w,
						 cd_medico_w);
		
		--liberar_evolucao(cd_evolucao_w,nm_usuario_p);    N�o gerar liberado por causa da assinatura digital.
		
		
		
		commit;
	end if;

end if;

end Gerar_Evolucao_Escala_ECA;
/
