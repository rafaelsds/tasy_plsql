create or replace
procedure pls_valida_ocor_aut_vig_item
				(	nr_seq_ocor_combinada_p		Number,
					nr_seq_ocorrencia_p		Number,
					nr_seq_segurado_p		Number,
					nr_seq_motivo_glosa_p		Number,
					nr_seq_guia_p			Number,
					nr_seq_requisicao_p		Number,
					nr_seq_execucao_p		Number,
					ie_utiliza_filtro_p		Varchar2,
					nr_seq_param1_p			Number,
					nr_seq_param2_p			Number,
					nr_seq_param3_p			Number,
					nr_seq_param4_p			Number,
					nr_seq_param5_p			Number,
					nm_usuario_p			Varchar2,
					cd_estabelecimento_p		Number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Procedure utilizada para validar a vig�ncia dos itens.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [x]  Tasy (Delphi/Java) [x]  Portal [  ]  Relat�rios [ ]  Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
 Rotina utilizada para gera��o de ocorr�ncia na Autoriza��o / Requisi��o e Execu��o
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

ie_gerar_ocorrencia_w		varchar2(2);
ie_mat_proc_inativo_w		varchar2(2);

nr_seq_guia_proc_w		number(10);
nr_seq_guia_mat_w		number(10);
nr_seq_req_proc_w		number(10);
nr_seq_req_mat_w		number(10);
nr_seq_exec_item_w		number(10);

dt_autorizacao_w		date;
dt_requisicao_w			date;
dt_execucao_w			date;

cd_procedimento_w		number(10);
ie_origem_proced_w		number(10);
nr_seq_material_w		number(10);

ie_situacao_validacao_w		varchar2(2);
ie_regra_w			varchar2(2);
ie_tipo_ocorrencia_w		varchar2(2);
nr_seq_oc_benef_w		number(10);

Cursor C01 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia		= nr_seq_guia_p;

Cursor C02 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_guia_plano_mat
	where	nr_seq_guia		= nr_seq_guia_p;

Cursor C03 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C04 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_p;

Cursor C05 is
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_execucao_req_item
	where	nr_seq_execucao		= nr_seq_execucao_p
	and	nr_seq_material		is null;

Cursor C06 is
	select	nr_sequencia,
		nr_seq_material
	from	pls_execucao_req_item
	where	nr_seq_execucao		= nr_seq_execucao_p
	and	cd_procedimento		is null;

begin

begin
	select	ie_valida_vigencia_item
	into	ie_situacao_validacao_w
	from	pls_validacao_aut_vig_item
	where	nr_seq_ocor_aut_combinada = nr_seq_ocor_combinada_p
	and	ie_situacao = 'A';
exception
when others then
	ie_situacao_validacao_w := 'N';
end;


if	(ie_situacao_validacao_w = 'S') then
	if	(nr_seq_guia_p is not null) then
		begin
			select	dt_solicitacao
			into	dt_autorizacao_w
			from	pls_guia_plano
			where	nr_sequencia = 	nr_seq_guia_p;	
		exception
		when others then
			dt_autorizacao_w := sysdate;
		end;

		open C01;
		loop
		fetch C01 into
			nr_seq_guia_proc_w,
			cd_procedimento_w,
			ie_origem_proced_w;
		exit when C01%notfound;
			begin
			ie_mat_proc_inativo_w := pls_obter_se_proc_inativo(cd_procedimento_w,ie_origem_proced_w,dt_autorizacao_w);

			ie_gerar_ocorrencia_w	:= 'S';

			if	(ie_utiliza_filtro_p	= 'S') then
				pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
								nr_seq_execucao_p, nr_seq_guia_proc_w, null,
								null, null, null,
								cd_procedimento_w, ie_origem_proced_w, null,
								null,null, nr_seq_ocorrencia_p,
								null, null, null,
								nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

				if	(ie_regra_w	= 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				elsif	(ie_regra_w	in ('E','N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				end if;
			end if;

			if	(ie_gerar_ocorrencia_w	= 'S') and
				(ie_mat_proc_inativo_w = 'N') then
				pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, null,
							nr_seq_guia_p, null, nr_seq_guia_proc_w, 
							null, nr_seq_ocor_combinada_p, nm_usuario_p, 
							null, nr_seq_motivo_glosa_p, 1, 
							cd_estabelecimento_p, 'N' ,null,
							nr_seq_oc_benef_w, null,
							null, null, null);

				pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
								nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
								nr_seq_guia_proc_w, null, null,
								null, null, null,
								nm_usuario_p, cd_estabelecimento_p);
			end if;
			end;
		end loop;
		close C01;

		open C02;
		loop
		fetch C02 into
			nr_seq_guia_mat_w,
			nr_seq_material_w;
		exit when C02%notfound;
			begin
			ie_mat_proc_inativo_w := pls_obter_se_mat_inativo(nr_seq_material_w,dt_autorizacao_w);

			ie_gerar_ocorrencia_w	:= 'S';

			if	(ie_utiliza_filtro_p	= 'S') then
				pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
								nr_seq_execucao_p, null, nr_seq_guia_mat_w,
								null, null, null,
								null, null, nr_seq_material_w,
								null, null, nr_seq_ocorrencia_p,
								null, null, null,
								nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

				if	(ie_regra_w	= 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				elsif	(ie_regra_w	in ('E','N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				end if;
			end if;

			if	(ie_gerar_ocorrencia_w	= 'S') and
				(ie_mat_proc_inativo_w = 'N') then
				pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, null,
							nr_seq_guia_p, null, null, 
							nr_seq_guia_mat_w, nr_seq_ocor_combinada_p, nm_usuario_p, 
							null, nr_seq_motivo_glosa_p, 2, 
							cd_estabelecimento_p, 'N' ,null,
							nr_seq_oc_benef_w, null,
							null, null, null);

				pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
								nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
								null, nr_seq_guia_mat_w, null,
								null, null, null,
								nm_usuario_p, cd_estabelecimento_p);
			end if;
			end;
		end loop;
		close C02;

	elsif	(nr_seq_requisicao_p is not null) then
		begin
			select	dt_requisicao
			into	dt_requisicao_w
			from	pls_requisicao
			where	nr_sequencia = 	nr_seq_requisicao_p;
		exception
		when others then
			dt_requisicao_w := sysdate;
		end;

		open C03;
		loop
		fetch C03 into
			nr_seq_req_proc_w,
			cd_procedimento_w,
			ie_origem_proced_w;
		exit when C03%notfound;
			begin
			ie_mat_proc_inativo_w := pls_obter_se_proc_inativo(cd_procedimento_w,ie_origem_proced_w,dt_requisicao_w);

			ie_gerar_ocorrencia_w	:= 'S';

			if	(ie_utiliza_filtro_p	= 'S') then
				pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
								nr_seq_execucao_p, null, null,
								nr_seq_req_proc_w, null, null,
								cd_procedimento_w, ie_origem_proced_w, null,
								null, null, nr_seq_ocorrencia_p,
								null, null, null,
								nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

				if	(ie_regra_w	= 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				elsif	(ie_regra_w	in ('E','N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				end if;
			end if;

			if	(ie_gerar_ocorrencia_w	= 'S') and
				(ie_mat_proc_inativo_w = 'N') then
				pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
							null, null, nr_seq_req_proc_w, 
							null, nr_seq_ocor_combinada_p, nm_usuario_p, 
							null, nr_seq_motivo_glosa_p, 5, 
							cd_estabelecimento_p, 'N' ,null,
							nr_seq_oc_benef_w, null,
							null, null, null);

				pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
								nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
								null, null, nr_seq_req_proc_w,
								null, null, null,
								nm_usuario_p, cd_estabelecimento_p);
			end if;
			end;
		end loop;
		close C03;

		open C04;
		loop
		fetch C04 into
			nr_seq_req_mat_w,
			nr_seq_material_w;
		exit when C04%notfound;
			begin
			ie_mat_proc_inativo_w := pls_obter_se_mat_inativo(nr_seq_material_w,dt_requisicao_w);

			ie_gerar_ocorrencia_w	:= 'S';

			if	(ie_utiliza_filtro_p	= 'S') then
				pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
								nr_seq_execucao_p, null, null,
								null, nr_seq_req_mat_w, null,
								null, null, nr_seq_material_w,
								null, null, nr_seq_ocorrencia_p,
								null, null, null,
								nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

				if	(ie_regra_w	= 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				elsif	(ie_regra_w	in ('E','N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				end if;
			end if;

			if	(ie_gerar_ocorrencia_w	= 'S') and
				(ie_mat_proc_inativo_w = 'N') then
				pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
							null, null, null, 
							nr_seq_req_mat_w, nr_seq_ocor_combinada_p, nm_usuario_p, 
							null, nr_seq_motivo_glosa_p, 6, 
							cd_estabelecimento_p, 'N' ,null,
							nr_seq_oc_benef_w, null,
							null, null, null);

				pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
								nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
								null, null, null,
								nr_seq_req_mat_w, null, null,
								nm_usuario_p, cd_estabelecimento_p);
			end if;
			end;
		end loop;
		close C04;

	elsif	(nr_seq_execucao_p is not null) then
		begin
			select	dt_execucao
			into	dt_execucao_w
			from	pls_execucao_requisicao
			where	nr_sequencia = 	nr_seq_execucao_p;
		exception
		when others then
			dt_execucao_w := sysdate;
		end;

		open C05;
		loop
		fetch C05 into
			nr_seq_exec_item_w,
			cd_procedimento_w,
			ie_origem_proced_w;
		exit when C05%notfound;
			begin
			ie_mat_proc_inativo_w := pls_obter_se_proc_inativo(cd_procedimento_w,ie_origem_proced_w,dt_execucao_w);

			ie_gerar_ocorrencia_w	:= 'S';

			if	(ie_utiliza_filtro_p	= 'S') then
				pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
								nr_seq_execucao_p, null, null,
								null, null, nr_seq_exec_item_w,
								cd_procedimento_w, ie_origem_proced_w, null,
								null, null, nr_seq_ocorrencia_p,
								null, null, null,
								nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

				if	(ie_regra_w	= 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				elsif	(ie_regra_w	in ('E','N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				end if;
			end if;

			if	(ie_gerar_ocorrencia_w	= 'S') and
				(ie_mat_proc_inativo_w = 'N') then
				pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
							null, null, nr_seq_exec_item_w, 
							null, nr_seq_ocor_combinada_p, nm_usuario_p, 
							null, nr_seq_motivo_glosa_p, 10, 
							cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
							nr_seq_oc_benef_w, null,
							null, null, null);

				pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
								nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
								null, null, null,
								null, nr_seq_exec_item_w, null,
								nm_usuario_p, cd_estabelecimento_p);
			end if;
			end;
		end loop;
		close C05;

		open C06;
		loop
		fetch C06 into
			nr_seq_exec_item_w,
			nr_seq_material_w;
		exit when C06%notfound;
			begin
			ie_mat_proc_inativo_w := pls_obter_se_mat_inativo(nr_seq_material_w,dt_execucao_w);

			ie_gerar_ocorrencia_w	:= 'S';

			if	(ie_utiliza_filtro_p	= 'S') then
				pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, nr_seq_requisicao_p,
								nr_seq_execucao_p, null, null,
								null, null, nr_seq_exec_item_w,
								null, null, nr_seq_material_w,
								null, null, nr_seq_ocorrencia_p,
								null, null, null,
								nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

				if	(ie_regra_w	= 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				elsif	(ie_regra_w	in ('E','N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				end if;
			end if;

			if	(ie_gerar_ocorrencia_w	= 'S') and
				(ie_mat_proc_inativo_w = 'N') then
				pls_inserir_ocorrencia(	nr_seq_segurado_p, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
							null, null, null, 
							nr_seq_exec_item_w, nr_seq_ocor_combinada_p, nm_usuario_p, 
							null, nr_seq_motivo_glosa_p, 11, 
							cd_estabelecimento_p, 'N' ,nr_seq_execucao_p,
							nr_seq_oc_benef_w, null,
							null, null, null);

				pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
								nr_seq_guia_p, nr_seq_requisicao_p, nr_seq_execucao_p,
								null, null, null,
								null, null, nr_seq_exec_item_w,
								nm_usuario_p, cd_estabelecimento_p);
			end if;
			end;
		end loop;
		close C06;
	end if;
end if;

commit;

end pls_valida_ocor_aut_vig_item;
/