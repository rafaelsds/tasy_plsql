create or replace
procedure gerar_saida_acomp_barras(	nr_atendimento_p	number,
					nr_controle_p		varchar2) is
		
begin

if	(nr_atendimento_p > 0) and
	(nr_controle_p is not null) then
	begin

	update	atendimento_acompanhante
	set	dt_saida	= sysdate
	where	nr_atendimento	= nr_atendimento_p
	and	nr_controle	= nr_controle_p;
	
	commit;
	
	end;
end if;

end gerar_saida_acomp_barras;
/