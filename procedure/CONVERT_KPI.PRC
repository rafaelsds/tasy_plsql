create or replace
procedure convert_kpi(	nm_usuario_p	varchar2,
			nr_seq_indicador_p	number,
			nr_seq_ind_base_p out number) is 			

-- Cadastro base indicador
ind_data_sequence_w     ind_data.nr_sequencia%type;
ind_data_nextval_w      ind_data.nr_sequencia%type;
ds_indicador_w			ind_base.ds_indicador%type;
cd_exp_indicador_w		ind_base.cd_exp_indicador%type;
ds_tabela_visao_w		ind_base.ds_origem_inf%type;
ds_sql_where_w			ind_base.ds_sql_where%type;
nr_seq_wheb_w			ind_base.nr_seq_ind_gestao%type;
ie_qualidade_w			ind_base.ie_qualidade%type;
ie_padrao_sistema_w		ind_base.ie_padrao_sistema%type;
nr_nova_sequencia_w		ind_base.nr_sequencia%type;
qt_ind_atrib_w			number(1);

ie_tipo_data_w			indicador_gestao.ie_tipo_data%type;
nr_seq_mod_impl_w		indicador_gestao.nr_seq_mod_impl%type;
ds_orientacao_w			indicador_gestao.ds_orientacao%type;
nr_seq_apresent_w		indicador_gestao.nr_seq_apresent%type;
nr_seq_ordem_serv_w		indicador_gestao.nr_seq_ordem_serv%type;
ie_inicia_grid_w		indicador_gestao.ie_inicia_grid%type;
ie_decimal_w			indicador_gestao.ie_decimal%type;
ie_plano_saude_w		indicador_gestao.ie_plano_saude%type;
ie_mostrar_total_w		indicador_gestao.ie_mostrar_total%type;
ie_mostrar_media_w		indicador_gestao.ie_mostrar_media%type;
ie_formato_evolucao_w	indicador_gestao.ie_formato_evolucao%type;
ie_formato_tot_linha_w	indicador_gestao.ie_formato_tot_linha%type;
nr_sequencia_w			indicador_gestao.nr_sequencia%type;

ie_origem_w             VARCHAR(2);
ie_indic_painel_w       indicador_gestao.ie_indic_painel%type;


qt_convertido_w			number(1);

-- Cadastro dimensao

nm_atributo_w				ind_data.ds_atributo%type;
nm_tabela_referencia_w		ind_data.nm_tabela_referencia%type;
nm_atributo_referencia_w	ind_data.nm_atributo_referencia%type;

cd_exp_dimensao_w			ind_dimensao.cd_exp_dimensao%type;
ds_dimensao_w				ind_dimensao.ds_dimensao%type;
nm_atributo_drill_w			ind_dimensao.nm_atributo_drill%type;
ie_mostra_nulo_w			ind_dimensao.ie_mostra_nulo%type;
nr_nova_seq_dim_w			ind_dimensao.nr_sequencia%type;

nr_seq_dim_w				indicador_gestao_atrib.nr_seq_dim%type;
ds_filtro_w					indicador_gestao_atrib.ds_filtro%type;
ds_filtro_padrao_w			indicador_gestao_atrib.ds_filtro_padrao%type;
cd_exp_grid_w				indicador_gestao_atrib.cd_exp_grid%type;
nr_seq_grid_w				indicador_gestao_atrib.nr_seq_grid%type;
qt_tamanho_grid_w			indicador_gestao_atrib.qt_tamanho_grid%type;
ds_mascara_grid_w			indicador_gestao_atrib.ds_mascara_grid%type;
ie_classificacao_ordem_w	indicador_gestao_atrib.ie_classificacao_ordem%type;
ie_tipo_ordem_classif_w		indicador_gestao_atrib.ie_tipo_ordem_classif%type;
ie_situacao_w				indicador_gestao_atrib.ie_situacao%type;
nr_seq_ind_gestao_atrib_w  indicador_gestao_atrib.nr_sequencia%type;

-- Cadastro filtro
nr_nova_seq_filtro_w	ind_filtro.nr_sequencia%type;

-- Cadastro paises da dimensao
nr_sequencia_dim_pais_w 	dsb_ind_dimensao_pais.nr_sequencia%type;
qtd_registro_dim			number(10);


-- Cadastro SubIndicador
ds_subindicador_w				subindicador_gestao.ds_subindicador%type;
cd_exp_subindicador_w			subindicador_gestao.cd_exp_subindicador%type;
ds_sql_w						subindicador_gestao.ds_sql%type;
nr_seq_apresent_subindicador_w	subindicador_gestao.nr_seq_apresent%type;
nr_sequencia_subgestao_w		subindicador_gestao.nr_sequencia%type;
nr_nova_sequencia_sub_w			subindicador_gestao.nr_sequencia%type;

-- Cadastro Atributo SubIndicador
nm_atributo_sub_w				subind_dashboard_atrib.nm_atributo%type;
ie_tipo_filtro_w				subind_dashboard_atrib.ie_tipo_filtro%type;
ie_forma_passagem_filtro_w		subind_dashboard_atrib.ie_forma_passagem_filtro%type;
ds_filtro_sub_w					subind_dashboard_atrib.ds_filtro%type;
ds_filtro_padrao_sub_w			subind_dashboard_atrib.ds_filtro_padrao%type;
ds_grid_w                       subind_dashboard_atrib.ds_grid%type;
cd_exp_grid_sub_w               subind_dashboard_atrib.cd_exp_grid%type;
ds_mascara_grid_sub_w	        subind_dashboard_atrib.ds_mascara_grid%type;
nr_seq_subindicador_w	        subind_dashboard_atrib.nr_seq_subindicador%type;
qt_tamanho_grid_sub_w			subind_dashboard_atrib.qt_tamanho_grid%type;
nr_seq_grid_sub_w				subind_dashboard_atrib.nr_seq_grid%type;

-- Cadastro data
cd_exp_data_w		indicador_gestao_atrib.cd_exp_data%type;
nr_seq_data_w		indicador_gestao_atrib.nr_seq_data%type;
ds_data_w			indicador_gestao_atrib.ds_data%type;
ie_data_w			indicador_gestao_atrib.ie_data%type;


ds_documentacao_w	dsb_indicador_doc.ds_documentacao%type;

cursor c02 is
	select	nm_atributo,
			cd_exp_dimensao,
			nr_seq_dim,
			ds_dimensao,
			ds_filtro,
			ds_filtro_padrao,
			cd_exp_grid,
			nr_seq_grid,
			qt_tamanho_grid,
			ds_mascara_grid,
			nm_atributo_drill,
			ie_classificacao_ordem,
			ie_tipo_ordem_classif,
			ds_sql_where,
			ie_mostra_nulo,
			nm_tabela_referencia,
			nm_atributo_referencia,
			ie_situacao,
			nr_sequencia
	from	indicador_gestao_atrib
	where	nr_seq_ind_gestao = nr_sequencia_w
	and		ds_dimensao is not null;


cursor c_informacoes is
select	nm_atributo,
	cd_exp_informacao,
	nr_seq_inf,
	ds_informacao,
	ie_formato_tot_linha,
	ie_dividendo,
	ie_divisor,
	ie_evolucao_media,
	ds_filtro,
	ds_filtro_padrao,
	cd_exp_grid,
	nr_seq_grid,
	qt_tamanho_grid,
	ds_mascara_grid,
	nm_atributo_drill,
	ie_classificacao_ordem,
	ie_tipo_ordem_classif,
	ds_sql_where,
	ie_mostra_nulo,
	nm_tabela_referencia,
	nm_atributo_referencia,
	ie_situacao
from	indicador_gestao_atrib
where	nr_seq_ind_gestao = nr_sequencia_w
and	ds_informacao is not null
order by nr_seq_inf;
	
	
cursor c04 is
	select	nm_atributo,
			cd_exp_data,
			nr_seq_data,
			ds_data,
			ie_data,
			ds_filtro,
			ds_filtro_padrao,
			cd_exp_grid,
			nr_seq_grid,
			qt_tamanho_grid,
			ds_mascara_grid,
			nm_atributo_drill,
			ie_classificacao_ordem,
			ie_tipo_ordem_classif,
			ds_sql_where,
			ie_mostra_nulo,
			nm_tabela_referencia,
			nm_atributo_referencia,
			ie_situacao
	from	indicador_gestao_atrib
	where	nr_seq_ind_gestao = nr_sequencia_w
	and		ds_data is not null;

	
cursor c05 is
select	a.nr_sequencia,
	a.nr_seq_indicador,
	a.ie_tipo,
	a.cd_pais,
	a.ds_documento_longo
from	indicador_gestao_doc a
where	a.nr_seq_indicador	= nr_sequencia_w;

cursor c06 is
select	a.nr_sequencia,
	a.ds_subindicador,
	a.cd_exp_subindicador,
	a.ds_sql,
	a.nr_seq_apresent
from	subindicador_gestao a
where	a.nr_seq_indicador	= nr_seq_indicador_p;

cursor c07 is
select	a.nm_atributo,	
	a.ie_tipo_filtro,	
	a.ie_forma_passagem_filtro,
	a.ds_filtro,		
	a.ds_filtro_padrao,	
	a.ds_grid,                 
	a.cd_exp_grid,         	   
	a.ds_mascara_grid,	  
	a.nr_seq_subindicador,
	a.qt_tamanho_grid,
	a.nr_seq_grid
from	subindicador_gestao_atrib a
where	a.nr_seq_subindicador	= nr_sequencia_subgestao_w;

cursor c08 is
	select	nm_atributo,
			cd_exp_dimensao,
			nr_seq_dim,
			ds_dimensao,
			ds_filtro,
			ds_filtro_padrao,
			cd_exp_grid,
			nr_seq_grid,
			qt_tamanho_grid,
			ds_mascara_grid,
			nm_atributo_drill,
			ie_classificacao_ordem,
			ie_tipo_ordem_classif,
			ds_sql_where,
			ie_mostra_nulo,
			nm_tabela_referencia,
			nm_atributo_referencia,
			ie_situacao
	from	indicador_gestao_atrib
	where	nr_seq_ind_gestao = nr_sequencia_w
	and		ds_filtro is not null;

begin
	ds_sql_where_w := null;
	
    --Ajustar sequence caso esteja incorreta
    select max(nr_sequencia)
    into ind_data_sequence_w
    from ind_data;
    
    select ind_data_seq.nextval
    into   ind_data_nextval_w
    from   dual;
    
    if(ind_data_sequence_w > ind_data_nextval_w) then
        begin
            tasy_posicionar_sequence('IND_DATA','NR_SEQUENCIA','N');    
        end;
    end if;
	
    -- Feito isto, e realizado o insert dos indicadores, na nova estrutura


	select	ds_indicador,
			cd_exp_indicador,
			ds_tabela_visao,
			ie_tipo_data,
			nr_seq_mod_impl,
			ds_orientacao,
			ds_sql_where,
			nr_seq_wheb,
			nr_seq_apresent,
			nr_seq_ordem_serv,
			ie_inicia_grid,
			ie_decimal,
			ie_qualidade,
			ie_plano_saude,
			ie_mostrar_total,
			ie_mostrar_media,
			ie_formato_evolucao,
			ie_formato_tot_linha,
			nr_sequencia,
			decode(nr_seq_wheb, null, 'N', 'S'),
            ie_indic_painel
	into	ds_indicador_w,
			cd_exp_indicador_w,
			ds_tabela_visao_w,
			ie_tipo_data_w,
			nr_seq_mod_impl_w,
			ds_orientacao_w,
			ds_sql_where_w,
			nr_seq_wheb_w,
			nr_seq_apresent_w,
			nr_seq_ordem_serv_w,
			ie_inicia_grid_w,
			ie_decimal_w,
			ie_qualidade_w,
			ie_plano_saude_w,
			ie_mostrar_total_w,
			ie_mostrar_media_w,
			ie_formato_evolucao_w,
			ie_formato_tot_linha_w,
			nr_sequencia_w,
			ie_padrao_sistema_w,
            ie_indic_painel_w
	from	indicador_gestao
	where	ie_situacao = 'A'
	and		nr_sequencia = nr_seq_indicador_p;

	select 	count(*)
	into 	qt_convertido_w
	from 	ind_base
	where 	nr_seq_ind_gestao = nr_seq_wheb_w
	and	ie_situacao = 'A'
	and	ds_origem_inf != 'SUBINDICADOR'
	and 	rownum = 1;
	
	if (qt_convertido_w > 0) then			
		wheb_mensagem_pck.exibir_mensagem_abort(1073558);		
	end if;
	
	select 	count(*)
	into 	qt_ind_atrib_w
	from 	indicador_gestao_atrib
	where 	nr_seq_ind_gestao = nr_seq_wheb_w
	and 	rownum = 1;
	
	if(ie_indic_painel_w = 'P') then
        ie_origem_w := 'PA';    
    elsif(qt_ind_atrib_w = 0) then
        ie_origem_w := 'R';    
	else
        ie_origem_w := 'P';
    end if;
	
	select	ind_base_seq.nextval
	into	nr_nova_sequencia_w
	from	dual;
	
	nr_seq_ind_base_p :=	nr_nova_sequencia_w;
	
	-- TODO: Implementar insert da base do cadastro base do indicadores, na nova estrutura.
	insert into ind_base (	
		nr_sequencia,
		ds_indicador,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		cd_exp_indicador,
		ie_padrao_sistema,
		ds_origem_inf,
		ds_sql_where,
		ie_situacao,
		nr_seq_ind_gestao,
		ie_qualidade,
		ie_origem)
	values(	nr_nova_sequencia_w,
		ds_indicador_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		cd_exp_indicador_w,
		ie_padrao_sistema_w, -- Levando em consideracao que tem nr_seq_wheb, e padrao do sistema
		ds_tabela_visao_w,
		ds_sql_where_w,
		'A',
		nr_seq_wheb_w,
		ie_qualidade_w,
		ie_origem_w);

	open c02;
	loop
	fetch c02 into
		nm_atributo_w,
		cd_exp_dimensao_w,
		nr_seq_dim_w,
		ds_dimensao_w,
		ds_filtro_w,
		ds_filtro_padrao_w,
		cd_exp_grid_w,
		nr_seq_grid_w,
		qt_tamanho_grid_w,
		ds_mascara_grid_w,
		nm_atributo_drill_w,
		ie_classificacao_ordem_w,
		ie_tipo_ordem_classif_w,
		ds_sql_where_w,
		ie_mostra_nulo_w,
		nm_tabela_referencia_w,
		nm_atributo_referencia_w,
		ie_situacao_w,
		nr_seq_ind_gestao_atrib_w;
	exit when c02%notfound;
		begin
		
		select	ind_dimensao_seq.nextval
		into	nr_nova_seq_dim_w
		from	dual;

		select ind_filtro_seq.nextval
		into nr_nova_seq_filtro_w
		from dual;					
		
		-- TODO: Implementar a insercao dos registros na tabela de dimensoes, na nova estrutura.
		insert into ind_dimensao (	
			nr_sequencia,
			ds_dimensao,
			nr_seq_indicador,
			ds_atributo,
			nm_atributo_drill,
			ds_sql_where,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_exp_dimensao,
			ie_mostra_nulo,
			nm_tabela_referencia,
			ie_situacao,
			nm_atributo_referencia,
            ie_classificacao_ordem)
		values(	
			nr_nova_seq_dim_w,
			ds_dimensao_w,
			nr_nova_sequencia_w,
			nm_atributo_w,
			nm_atributo_drill_w,
			ds_sql_where_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,						
			cd_exp_dimensao_w,
			ie_mostra_nulo_w,
			nm_tabela_referencia_w,
			ie_situacao_w,
			nm_atributo_referencia_w,
            ie_classificacao_ordem_w);
		
		
		ds_sql_where_w := null;
		
		insert into ind_filtro (	
			nr_sequencia,
			nr_seq_indicador,
			nr_seq_dimensao,
			nm_atributo,
			ds_sql_filtro,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec)
		values(
			nr_nova_seq_filtro_w,
			nr_nova_sequencia_w,
			nr_nova_seq_dim_w,
			nm_atributo_w,
			ds_filtro_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate);						
		
		select count(nr_sequencia)
		into qtd_registro_dim
		from dsb_ind_dimensao_pais
		where nr_seq_dimensao = nr_seq_ind_gestao_atrib_w;
		
		if qtd_registro_dim > 0 then			
		
		select nr_sequencia
		into nr_sequencia_dim_pais_w
		from dsb_ind_dimensao_pais
		where nr_seq_dimensao = nr_seq_ind_gestao_atrib_w;
		
		update dsb_ind_dimensao_pais
		set nr_seq_filtro = nr_nova_seq_filtro_w
		where nr_sequencia = nr_sequencia_dim_pais_w;	

		end if;
			
		end;
	end loop;
	close c02;
	/*Copy the information to Dashboard structure*/
	for inf in c_informacoes loop	
		begin
		
		insert into ind_informacao (	
			nr_sequencia,
			ds_informacao,
			nr_seq_indicador,
			ds_atributo,
			ds_sql_where,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,						
			cd_exp_informacao,
			nr_seq_apres,
			nr_seq_grid,
			ds_mascara_grid,
			ie_formato_tot_linha,
			ie_divisor,
			ie_dividendo,
			ds_filtro_padrao,
			nm_tabela_referencia,
			nm_atributo_referencia,
            ie_classificacao_ordem)
		values(	ind_informacao_seq.nextval,
			inf.ds_informacao,
			nr_nova_sequencia_w,
			inf.nm_atributo,
			inf.ds_sql_where,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			inf.cd_exp_informacao,
			inf.nr_seq_inf,
			inf.nr_seq_grid,
			inf.ds_mascara_grid,
			inf.ie_formato_tot_linha,
			inf.ie_divisor,
			inf.ie_dividendo,
			inf.ds_filtro_padrao,
			nm_tabela_referencia_w,
			nm_atributo_referencia_w,
            inf.ie_classificacao_ordem);
		
		end;
	end loop;
	
	
	open c04;
	loop
	fetch c04 into
		nm_atributo_w,
		cd_exp_data_w,
		nr_seq_data_w,
		ds_data_w,
		ie_data_w,
		ds_filtro_w,
		ds_filtro_padrao_w,
		cd_exp_grid_w,
		nr_seq_grid_w,
		qt_tamanho_grid_w,
		ds_mascara_grid_w,
		nm_atributo_drill_w,
		ie_classificacao_ordem_w,
		ie_tipo_ordem_classif_w,
		ds_sql_where_w,
		ie_mostra_nulo_w,
		nm_tabela_referencia_w,
		nm_atributo_referencia_w,
		ie_situacao_w;
	exit when c04%notfound;
		begin

			-- TODO: Implementar a insercao dos registros na tabela de data, na nova estrutura.
			insert into ind_data (	
				nr_sequencia,
				ds_data,
				nr_seq_indicador,
				ds_atributo,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,						
				cd_exp_data,
				ie_tipo_data,
				nm_tabela_referencia,
				nm_atributo_referencia)
			values(	
				ind_data_seq.nextval,
				ds_data_w,
				nr_nova_sequencia_w,
				nm_atributo_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,						
				cd_exp_data_w,
				ie_data_w,
				nm_tabela_referencia_w,
				nm_atributo_referencia_w);

			ds_sql_where_w := null;
		end;
	end loop;
	close c04;			

	for vet05 in c05 loop
		begin
		ds_documentacao_w	:= to_clob(vet05.ds_documento_longo);

		insert into dsb_indicador_doc(
			nr_sequencia,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nm_usuario,
			dt_atualizacao,
			nr_seq_indicador,
			ie_tipo,
			cd_pais,
			ie_origem,
			ds_documentacao)
		values(	dsb_indicador_doc_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_nova_sequencia_w,
			vet05.ie_tipo,
			vet05.cd_pais,
			'S',
			ds_documentacao_w);
		
		end;
	
	end loop;
	
	open c06;
	loop
	fetch c06 into
		nr_sequencia_subgestao_w,
		ds_subindicador_w,
		cd_exp_subindicador_w,
		ds_sql_w,
		nr_seq_apresent_subindicador_w;
	exit when c06%notfound;
		begin		

		select	ind_base_seq.nextval
		into	nr_nova_sequencia_sub_w
		from	dual;

		insert into ind_base (	
			nr_sequencia,
			nr_seq_superior,
			ds_indicador,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_exp_indicador,
			ie_padrao_sistema,
			ds_origem_inf,
			ds_sql_where,
			ie_situacao,
			nr_seq_ind_gestao,
			nr_seq_apres,
			ie_origem)
		values(	
			nr_nova_sequencia_sub_w,
			nr_nova_sequencia_w,
			ds_subindicador_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			cd_exp_subindicador_w,						
			'S', 
			'SUBINDICADOR',
			ds_sql_w,
			'A',
			nr_seq_wheb_w,
			nr_seq_apresent_subindicador_w,
			'P');

		commit;	

			open c07;
			loop
			fetch c07 into
				nm_atributo_sub_w,	
				ie_tipo_filtro_w,	
				ie_forma_passagem_filtro_w,
				ds_filtro_sub_w,		
				ds_filtro_padrao_sub_w,	
				ds_grid_w,                 
				cd_exp_grid_sub_w,         				             
				ds_mascara_grid_sub_w,	  
				nr_seq_subindicador_w,
				qt_tamanho_grid_sub_w,
				nr_seq_grid_sub_w;
			exit when c07%notfound;
				begin		

				insert into subind_dashboard_atrib (	
					nr_sequencia,            
					nr_seq_subindicador,     
					nm_atributo,             
					dt_atualizacao,          
					nm_usuario,              
					ie_tipo_filtro,          
					ds_filtro,               
					ds_grid,                 
					ds_mascara_grid,         
					ds_filtro_padrao,        
					ie_forma_passagem_filtro,
					cd_exp_grid,             
					dt_atualizacao_nrec,     
					nm_usuario_nrec,
					qt_tamanho_grid,
					nr_seq_grid
					)
				values(	
					subind_dashboard_atrib_seq.nextval,
					nr_nova_sequencia_sub_w,
					nm_atributo_sub_w,
					sysdate,
					nm_usuario_p,
					ie_tipo_filtro_w,
					ds_filtro_sub_w,
					ds_grid_w,						
					ds_mascara_grid_sub_w, 
					ds_filtro_padrao_sub_w,
					ie_forma_passagem_filtro_w,
					cd_exp_grid_sub_w,
					sysdate,
					nm_usuario_p,
					qt_tamanho_grid_sub_w,
					nr_seq_grid_sub_w);
			
				commit;
					
					
				end;
			end loop;
			close c07;				
		end;
	end loop;
	close c06;
	
commit;

end convert_kpi;
/
