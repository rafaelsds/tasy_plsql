create or replace PROCEDURE db_doc_param_value_log(
    nm_usuario_p           VARCHAR,
    nr_seq_document_p    NUMBER,
    nr_seq_content_macro_p NUMBER,
    nr_seq_macro_param_p   NUMBER,
    ds_value_p             VARCHAR2,
    ds_content_value_p CLOB,
    ds_content_result_p CLOB,
    ds_error_p varchar2)
IS
  nr_seq_w NUMBER(10);
BEGIN
  SELECT db_document_param_value_seq.nextval INTO nr_seq_w FROM dual;
  INSERT
  INTO db_document_param_value
    (
      nr_sequencia,
      dt_atualizacao_nrec,
      nm_usuario_nrec,
      dt_atualizacao,
      nm_usuario,
      nr_seq_document,
      nr_seq_content_macro,
      nr_seq_macro_param,
      ds_value,
      ds_content_value,
      ds_content_result,
      ds_error
    )
    VALUES
    (
      nr_seq_w,
      sysdate,
      nm_usuario_p,
      sysdate,
      nm_usuario_p,
      nr_seq_document_p,
      nr_seq_content_macro_p,
      nr_seq_macro_param_p,
      ds_value_p,
      ds_content_value_p,
      ds_content_result_p,
      ds_error_p
    );
  COMMIT;
END db_doc_param_value_log;
/
