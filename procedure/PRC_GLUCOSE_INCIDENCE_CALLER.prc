CREATE OR REPLACE PROCEDURE PRC_GLUCOSE_INCIDENCE_CALLER(
  dt_inicio_p        DATE,
  dt_fim_p           DATE,
  cd_setor_p         VARCHAR2,
  visao_p            NUMBER DEFAULT 1) IS
BEGIN
  --
  PKG_GLUCOSE_INCIDENCE.RUN(dt_inicio_p, dt_fim_p, NVL(cd_setor_p, '0'), visao_p);
  --
END;
/
