create or replace procedure GERAR_FLUXO_CAIXA_LOTE_VL_REGR(
				nr_seq_lote_fluxo_p	number,
				cd_conta_financ_p	number,
                                nr_dia_fixo_p		number,
                                vl_fixo_p		number,
                                dt_inicial_p		date,
                                dt_final_p		date,
                                cd_empresa_p		number,
                                nr_seq_regra_p		number,
                                cd_estabelecimento_p	number,
                                nm_usuario_p		varchar2) is

/*--------------------------------------------------------------- ATEN��O ----------------------------------------------------------------*/
/* Cuidado ao realizar altera��es no fluxo de caixa em lote. Toda e qualquer altera��o realizada em qualquer uma */
/* das procedures do fluxo de caixa em lote deve ser cuidadosamente verificada e realizada no fluxo de caixa      */
/* convencional. Devemos garantir que os dois fluxos de caixa tragam os mesmos valores no resultado, evitando     */
/* assim que existam diferen�as entre os fluxos de caixa.                                                                                                */
/*--------------- AO ALTERAR O FLUXO DE CAIXA EM LOTE ALTERAR TAMB�M O FLUXO DE CAIXA ---------------*/

ie_tratar_fim_semana_w          varchar2(100);
ie_antecipar_dia_util_w         varchar2(100);
ie_dia_fixo_w                   varchar2(100);
ie_regra_data_w                 varchar2(3);
vl_deducao_w                    number(15,2);
dt_referencia_w                 date;
dt_mes_w                        date;
dt_previsto_w                   date;
vl_previsto_w                   number(15,2);
vl_fluxo_previsto_w             number(15,2);
vl_fluxo_maior_w		number(15,2);
qt_meses_valor_contrato_w       number(3);
vl_soma_dias_w			number(15,2)	:= 0;
dt_ref_inicial_w		date;
dt_ref_final_w			date;

begin

select  ie_tratar_fim_semana,
        nvl(qt_meses_valor_contrato,0)
into    ie_tratar_fim_semana_w,
        qt_meses_valor_contrato_w
from    parametro_fluxo_caixa
where   cd_estabelecimento = cd_estabelecimento_p;

select  nvl(ie_antecipar_dia_util, 'N'),
        nvl(ie_dia_fixo, 'N'),
        nvl(ie_regra_data,'F')
into    ie_antecipar_dia_util_w,
        ie_dia_fixo_w,
        ie_regra_data_w
from    regra_fluxo_caixa
where   nr_sequencia = nr_seq_regra_p;

if      not(nvl(nr_dia_fixo_p,0) between 1 and 31) then
        wheb_mensagem_pck.exibir_mensagem_abort(195875,'DS_CONTA='||OBTER_DESC_CONTA_FINANC(cd_conta_financ_p));
end if;

dt_mes_w	:= pkg_date_utils.start_of(dt_inicial_p,'MONTH',0);

if      (nr_dia_fixo_p > to_number(pkg_date_utils.extract_field('DAY', pkg_date_utils.end_of(dt_mes_w,'MONTH',0), 0))) then
	dt_referencia_w	:=  pkg_date_utils.get_datetime(pkg_date_utils.end_of(dt_mes_w, 'MONTH', 0), nvl(dt_mes_w, PKG_DATE_UTILS.get_Time('00:00:00')));
else
	dt_referencia_w	:= pkg_date_utils.get_date(nr_dia_fixo_p, dt_mes_w, 0);
end if;

dt_mes_w	:= dt_referencia_w;

for i in 1.. qt_meses_valor_contrato_w loop
	begin
	dt_referencia_w		:= PKG_DATE_UTILS.ADD_MONTH(dt_referencia_w, i * -1,0);
	dt_ref_inicial_w	:= pkg_date_utils.start_of(dt_referencia_w,'DD',0);
	dt_ref_final_w		:= fim_dia(dt_referencia_w);

	select  nvl(sum(vl_fluxo),0),
		max(dt_previsto)
	into	vl_previsto_w,
		dt_previsto_w
	from    (select (dividir_sem_round(nvl(c.vl_titulo, b.vl_titulo),b.vl_titulo) * d.vl_pago) vl_fluxo,
			PKG_DATE_UTILS.start_of(d.dt_baixa,'DD',0) dt_previsto,
			b.cd_estabelecimento
		from    titulo_pagar_baixa d,
			titulo_pagar_classif c,
			titulo_pagar b
		where   c.nr_seq_conta_financ   = cd_conta_financ_p
		and     b.nr_titulo             = c.nr_titulo
		and     b.nr_titulo             = d.nr_titulo
		and     b.cd_estabelecimento	= cd_estabelecimento_p
		and     not exists
			(select 1
			from    titulo_pagar_baixa_cc x
			where   x.nr_seq_baixa  = d.nr_sequencia
			and     x.nr_titulo     = d.nr_titulo)
		and     b.ie_situacao           <> 'C'
		and     d.dt_baixa              between dt_ref_inicial_w and dt_ref_final_w
		union all
		select  e.vl_pago vl_fluxo,
			PKG_DATE_UTILS.start_of(d.dt_baixa,'DD',0) dt_previsto,
			b.cd_estabelecimento
		from    titulo_pagar_baixa_cc e,
			titulo_pagar_baixa d,
			titulo_pagar b
		where   e.cd_conta_financ       = cd_conta_financ_p
		and     d.nr_sequencia          = e.nr_seq_baixa
		and     d.nr_titulo             = e.nr_titulo
		and     b.nr_titulo             = d.nr_titulo
		and     b.cd_estabelecimento	= cd_estabelecimento_p
		and     b.ie_situacao           <> 'C'
		and     d.dt_baixa              between dt_ref_inicial_w and dt_ref_final_w)
	where	obter_empresa_estab(cd_estabelecimento) = cd_empresa_p;
	
	vl_soma_dias_w	:= vl_soma_dias_w + vl_previsto_w;
	
	end;
end loop;

dt_referencia_w	:= dt_mes_w;

if      ((ie_tratar_fim_semana_w = 'S') and (ie_antecipar_dia_util_w <> 'U')) or
	((ie_dia_fixo_w = 'N') and (ie_antecipar_dia_util_w = 'S')) then
	dt_referencia_w	:= obter_proximo_dia_util(cd_estabelecimento_p, dt_referencia_w);
end if;

if      (ie_antecipar_dia_util_w = 'U') or
	((ie_antecipar_dia_util_w = 'S') and
	 ((PKG_DATE_UTILS.start_of(dt_referencia_w, 'MONTH',0) > PKG_DATE_UTILS.start_of(dt_mes_w, 'MONTH',0)) or
	  (obter_se_dia_util(dt_referencia_w, cd_estabelecimento_p) = 'N'))) then
	dt_referencia_w	:= obter_dia_anterior_util(cd_estabelecimento_p, dt_mes_w);
end if;

vl_soma_dias_w	:= vl_soma_dias_w / qt_meses_valor_contrato_w;

if	(vl_soma_dias_w > 0) then
	GERAR_FLUXO_CAIXA_LOTE(	dt_referencia_w,
				cd_conta_financ_p,
				--'[RE] Regra Especial - M�dia dos t�tulos da regra',
				wheb_mensagem_pck.get_texto(303637),
				'22-1',
				'CP',
				nm_usuario_p,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				nr_seq_lote_fluxo_p,
				null,
				null,
				null,
				null,
				nr_seq_regra_p,
				null,
				null,
				nvl(vl_soma_dias_w,0));
end if;

vl_deducao_w		:= nvl(obter_se_fluxo_caixa_regra(nr_seq_regra_p, dt_referencia_w, cd_estabelecimento_p, 'VL'), 0);
vl_fluxo_previsto_w	:= vl_fixo_p - vl_deducao_w;

if	(vl_fluxo_previsto_w > nvl(vl_soma_dias_w,0)) then
	vl_fluxo_maior_w	:= vl_fluxo_previsto_w - vl_soma_dias_w;
	
	GERAR_FLUXO_CAIXA_LOTE(	dt_referencia_w,
				cd_conta_financ_p,
				--'[RE] Regra Especial',
				wheb_mensagem_pck.get_texto(303639),
				'22-2',
				'RE',
				nm_usuario_p,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				nr_seq_lote_fluxo_p,
				null,
				null,
				null,
				null,
				nr_seq_regra_p,
				null,
				null,
				nvl(vl_fluxo_maior_w,0));
end if;

/*NAO COLOCAR COMMIT NESTA PROCEDURE*/

end GERAR_FLUXO_CAIXA_LOTE_VL_REGR;
/
