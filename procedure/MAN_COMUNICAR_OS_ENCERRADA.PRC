create or replace procedure man_comunicar_os_encerrada
			(	nr_sequencia_p		number,
				nm_usuario_p		varchar2,
				ds_solucao_p		varchar2,
				ie_anexo_alternat_p	varchar2,
				cd_estabelecimento_p	varchar2) is


ds_titulo_w			varchar2(255);
ds_comunicado_w			varchar2(32000);
ds_historico_w			varchar2(32000);
ds_dano_w			varchar2(5000);
nr_sequencia_w			number(10,0);
nr_seq_classif_w		number(10,0);
nm_usuario_w			varchar2(15);
count_w				number(10,0);
ds_dano_breve_w			varchar2(255);
cd_pessoa_solicitante_w		varchar2(10);
ds_solucao_w			varchar2(255);
ds_pos_inicio_rtf_w		number(10,0);
ds_pos_inicio_rtf_java_w	number(10,0);
nr_seq_comunic_interna_w	number(10,0);
ds_arquivo_w			varchar2(255);
ds_caminho_w			varchar2(255);
ie_gerar_dano_os_w		varchar2(01);

cursor c01 is
select	ds_arquivo
from	man_ordem_serv_arq
where	ie_anexar_email = 'S'
and	nr_seq_ordem 	= nr_sequencia_p;

begin
select	max(obter_valor_param_usuario(299, 183, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),
	nvl(max(obter_valor_param_usuario(299, 236, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),'N')
into	ds_caminho_w,
	ie_gerar_dano_os_w
from 	dual;

select	substr(wheb_mensagem_pck.get_texto(348060) || ds_dano_breve,1,255),
	cd_pessoa_solicitante,
	nvl(ds_solucao, ds_solucao_p),
	substr(decode(ie_gerar_dano_os_w,'S',ds_dano,''),1,4000)
into	ds_dano_breve_w,
	cd_pessoa_solicitante_w,
	ds_solucao_w,
	ds_dano_w
from	man_ordem_servico a
where	nr_sequencia	= nr_sequencia_p;

if	(nvl(ie_gerar_dano_os_w,'N') = 'S') then
	ds_dano_w	:= substr(wheb_mensagem_pck.get_texto(348061) || ds_dano_w,1,5000);
end if;

select	nvl(max(nm_usuario),'X')
into	nm_usuario_w
from	usuario
where	cd_pessoa_fisica	= cd_pessoa_solicitante_w;

if	(nm_usuario_w <> 'X') then
	select	count(*)
	into	count_w
	from	man_ordem_serv_tecnico
	where	nr_seq_ordem_serv	= nr_sequencia_p;

	if	(count_w > 0) then
		select	nvl(max(nr_sequencia),0)
		into	nr_sequencia_w
		from	man_ordem_serv_tecnico
		where	nr_seq_ordem_serv	= nr_sequencia_p;

    ds_historico_w := convert_long_to_string('ds_relat_tecnico','man_ordem_serv_tecnico','nr_sequencia	= '|| nr_sequencia_w);
	end if;

	select	obter_classif_comunic('F')
	into	nr_seq_classif_w
	from	dual;

	ds_titulo_w	:= substr(wheb_mensagem_pck.get_texto(348051,	'NR_SEQ_ORDEM=' || nr_sequencia_p || ';DS_DANO=' || ds_dano_breve_w),1,255);
	--ds_titulo_w	:= substr('A Ordem de servi�o n� ' || nr_sequencia_p || ' foi encerrada' || '  ' || ds_dano_breve_w,1,255);

	if	(count_w = 0) or (ds_historico_w is null) then
		ds_comunicado_w	:= substr(ds_dano_w || chr(13) || chr(10) || ds_comunicado_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(348056), 1, 32000);
		if	(ds_solucao_w is not null) then
			ds_comunicado_w := substr(ds_comunicado_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(348057) || ds_solucao_w, 1, 32000);
		end if;
	elsif	(ds_historico_w is not null) then
		/*Pega o cabecalho do RTF*/
		ds_pos_inicio_rtf_w 		:= instr(ds_historico_w,'lang1046')+8;
		ds_pos_inicio_rtf_java_w	:= instr(ds_historico_w,'JWord2')+8;

		if	(ds_pos_inicio_rtf_w <> 8) then /*se realmente existir rtf, pois quando gera por procedure, gera string pura, da� n�o precisa retirar o cabe�alho*/
			begin
			ds_comunicado_w 	:= substr(substr(ds_historico_w,1,ds_pos_inicio_rtf_w) || 'fs20 ', 1, 32000);

			ds_comunicado_w 	:= substr(ds_comunicado_w || ds_dano_w || '\par ', 1, 32000);

			/*Acrecenta solu��o da OS*/
			if	(ds_solucao_w is not null) then
				ds_comunicado_w	:= substr(ds_comunicado_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(348057) || ds_solucao_w || '\par ' || '\par ', 1, 32000);
			end if;

			/*Acrecenta resto do conteudo do RTF*/
			ds_comunicado_w	:= substr(ds_comunicado_w || '\par '|| substr(ds_historico_w,ds_pos_inicio_rtf_w,length(ds_historico_w)), 1, 32000);
			end;
		elsif	(ds_pos_inicio_rtf_java_w <> 8) then /*se n�o existir rtf, concatena normalmente a string �s outras*/
			begin
			ds_comunicado_w 	:= substr(substr(ds_historico_w,1,ds_pos_inicio_rtf_java_w), 1, 32000);

			ds_comunicado_w 	:= substr(ds_comunicado_w || ds_dano_w || '\par ', 1, 32000);

			/*Acrecenta solu��o da OS*/
			if	(ds_solucao_w is not null) then
				ds_comunicado_w := substr(ds_comunicado_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(348057) || ds_solucao_w || '\par ' || '\par ', 1, 32000);
			end if;

			/*Acrecenta resto do conteudo do RTF*/
			ds_comunicado_w := substr(ds_comunicado_w || '\par '|| substr(ds_historico_w,ds_pos_inicio_rtf_java_w,length(ds_historico_w)), 1, 32000);

			end;
		elsif	(ds_pos_inicio_rtf_w = 8) then /*se n�o existir rtf, concatena normalmente a string �s outras*/
			begin
			ds_comunicado_w 	:= substr(ds_dano_w || chr(13) || chr(10) || ds_historico_w || chr(13) || chr(10), 1, 32000);

			/*Acrecenta solu��o da OS*/
			if	(ds_solucao_w is not null) then
				ds_comunicado_w := substr(ds_comunicado_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(348057) || ds_solucao_w || chr(13) || chr(10), 1, 32000);
			end if;
			end;
		end if;
	end if;

	select	comunic_interna_seq.nextval
	into	nr_seq_comunic_interna_w
	from	dual;

	insert into comunic_interna
		(dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		ie_geral,
		nm_usuario_destino,
		nr_sequencia,
		ie_gerencial,
		nr_seq_classif,
		dt_liberacao)
	values	(sysdate,
		ds_titulo_w,
		ds_comunicado_w,
		nm_usuario_p,
		sysdate,
		'N',
		nm_usuario_w || ',',
		nr_seq_comunic_interna_w,
		'N',
		nr_seq_classif_w,
		sysdate);

	open C01;
	loop
	fetch C01 into
		ds_arquivo_w;
	exit when C01%notfound;
		begin

		if	(ie_anexo_alternat_p = 'S') then
			while (instr(ds_arquivo_w,'\') > 0) loop
				begin
				ds_arquivo_w := substr(ds_arquivo_w,instr(ds_arquivo_w,'\')+1, length(ds_arquivo_w));
				end;
			end loop;
		ds_arquivo_w := ds_caminho_w||ds_arquivo_w;
		end if;

		insert into comunic_interna_arq
			(nr_sequencia,
			nr_seq_comunic,
			dt_atualizacao,
			nm_usuario,
			ds_arquivo)
		values	(comunic_interna_arq_seq.nextval,
			nr_seq_comunic_interna_w,
			sysdate,
			nm_usuario_p,
			ds_arquivo_w);
		end;
	end loop;
	close C01;

	Insert into man_ordem_serv_envio
		(nr_sequencia,
		nr_seq_ordem,
		dt_atualizacao,
		nm_usuario,
		dt_envio,
		ie_tipo_envio,
		ds_destino,
		ds_observacao)
	values	(man_ordem_serv_envio_seq.nextval,
		nr_sequencia_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		'I',
		substr(nm_usuario_w,1,255),
		wheb_mensagem_pck.get_texto(348059));
	commit;
end if;

end man_comunicar_os_encerrada;
/