create or replace
procedure pls_finalizar_auditoria_tws (	nr_seq_auditoria_p	pls_auditoria.nr_sequencia%type,
					ds_parecer_p		varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Atualizar a auditoria, inserir historico na requisicao e gerar a resposta de auditoria SCS.
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [ x ] Portal [  ] Relatorios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de atencao:  
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/	

nr_seq_requisicao_w	pls_requisicao.nr_sequencia%type;
ie_item_pendente_w	varchar2(1);
nr_seq_auditoria_w	ptu_resposta_auditoria.nr_sequencia%type;

ds_resp_ped_auditoria_w			pls_end_webservice_scs.ds_resp_ped_auditoria%type;
ds_ip_proxy_w				pls_end_webservice_scs.ds_ip_proxy%type;
ds_porta_proxy_w			pls_end_webservice_scs.ds_porta_proxy%type;
ds_usuario_proxy_w			pls_end_webservice_scs.ds_usuario_proxy%type;
ds_senha_proxy_w			pls_end_webservice_scs.ds_senha_proxy%type;
ds_senha_certificado_w			pls_end_webservice_scs.ds_senha_certificado%type;
ds_caminho_certificado_w		pls_end_webservice_scs.ds_caminho_certificado%type;
ds_webservice_envio_w			pls_end_webservice_scs.ds_webservice_envio%type;
ds_porta_w				pls_end_webservice_scs.ds_porta%type;

req_w					utl_http.req;
res_w					utl_http.resp;
ds_resposta_w				varchar2(32647);
ds_parametros_w				varchar2(1000);
ds_retorno_w				varchar(255);

begin

select	nr_seq_requisicao
into	nr_seq_requisicao_w
from	pls_auditoria
where	nr_sequencia	= nr_seq_auditoria_p;
	
if	(nr_seq_requisicao_w is not null) then

	pls_obter_ret_auditoria_estip (	nr_seq_auditoria_p, nm_usuario_p);
	
	pls_liberar_auditoria (	nr_seq_auditoria_p, nm_usuario_p, ie_item_pendente_w);
	
	if	(ds_parecer_p is not null) then
		pls_gerar_hist_req_web ( nr_seq_requisicao_w, 'M', ds_parecer_p, nm_usuario_p);
	end if;
	
	if	(pls_verif_envia_resp_aud_web(nr_seq_requisicao_w) = 'S') then
	
		ptu_env_pck.ptu_env_00404_v70(	null, nr_seq_requisicao_w, null,
						ds_parecer_p, cd_estabelecimento_p, nm_usuario_p,
						nr_seq_auditoria_w);
						
		begin
			select	ds_resp_ped_auditoria,
				ds_ip_proxy,
				ds_porta_proxy,
				ds_usuario_proxy,
				ds_senha_proxy,
				ds_senha_certificado,
				ds_caminho_certificado,
				ds_webservice_envio,
				ds_porta
			into	ds_resp_ped_auditoria_w,
				ds_ip_proxy_w,
				ds_porta_proxy_w,
				ds_usuario_proxy_w,
				ds_senha_proxy_w,
				ds_senha_certificado_w,
				ds_caminho_certificado_w,
				ds_webservice_envio_w,
				ds_porta_w
			from	pls_end_webservice_scs
			where	ie_situacao = 'A';

			if	(ds_porta_w is not null) then			
				
				utl_http.set_transfer_timeout(30);

				ds_parametros_w	:= 	'xml='				||
							'<nrSequenciaProjeto>'		|| 102923			|| '</nrSequenciaProjeto>'||
							'<cdTransacaoWebService>'	|| 30				|| '</cdTransacaoWebService>' ||
							'<nrSequenciaTransacao>'	|| nr_seq_auditoria_w		|| '</nrSequenciaTransacao>' ||
							'<nmUsuario>'			|| nm_usuario_p			|| '</nmUsuario>' ||
							'<cdEstabelecimento>'		|| cd_estabelecimento_p		|| '</cdEstabelecimento>' ||
							'<urlWSDLServico>'		|| ds_resp_ped_auditoria_w	|| '</urlWSDLServico>' ||
							'<ipProxy>'			|| ds_ip_proxy_w		|| '</ipProxy>' ||
							'<portaProxy>'			|| ds_porta_proxy_w		|| '</portaProxy>' ||
							'<usuarioProxy>'		|| ds_usuario_proxy_w		|| '</usuarioProxy>' ||
							'<senhaProxy>'			|| ds_senha_proxy_w		|| '</senhaProxy>' ||
							'<senhaCertificado>'		|| ds_senha_certificado_w	|| '</senhaCertificado>' ||
							'<caminhoCertificado>'		|| ds_caminho_certificado_w	|| '</caminhoCertificado>';

				req_w := utl_http.begin_request('http://' || ds_webservice_envio_w || ':' || ds_porta_w || '/SCS80Server/WScsServletProcessor' , method => 'POST');
				utl_http.set_header(req_w, 'user-agent', 'mozilla/4.0'); 
				utl_http.set_header(r => req_w, name => 'Content-Type', value => 'application/x-www-form-urlencoded;charset=utf-8');
				utl_http.set_header(r => req_w, name => 'Content-Length', value => length(ds_parametros_w));
				utl_http.write_text(r => req_w, data => ds_parametros_w);

				res_w := utl_http.get_response(req_w);

				utl_http.read_line(res_w, ds_resposta_w); 

				utl_http.end_response(res_w);

				ds_retorno_w		:= substr(ds_resposta_w, 1, 5);

				if	(ds_retorno_w = 'ERRO#') then
					pls_requisicao_gravar_hist(nr_seq_requisicao_w, 'L', substr(ds_resposta_w, 5, length(ds_resposta_w)), null, nm_usuario_p);
				end if;
			end if;
		exception
		when others then
			pls_requisicao_gravar_hist(nr_seq_requisicao_w, 'L', expressao_pck.obter_desc_expressao(962803) || sqlerrm, null, nm_usuario_p);
		end;
	end if;
end if;

commit; 

end pls_finalizar_auditoria_tws;
/
