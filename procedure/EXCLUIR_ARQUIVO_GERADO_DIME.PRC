create or replace
procedure EXCLUIR_ARQUIVO_GERADO_DIME(
			nr_sequencia_p		Varchar2) is 

begin
                delete from 
	w_dime_arquivo
	where nr_seq_controle_dime = nr_sequencia_p;
	
	delete  from  	
                fis_dime_controle
	where   nr_sequencia = nr_sequencia_p;

commit;

end EXCLUIR_ARQUIVO_GERADO_DIME;
/