create or replace
procedure man_liberar_doc_erro(	nr_sequencia_p		number,
				ie_acao_p		varchar2,
				nm_usuario_p		Varchar2,
				ds_justificativa_p		varchar2,
				ie_forma_origem_p		varchar2,
				ie_libera_correcao_p	varchar2,
				nr_seq_motivo_testes_p	number,
				ds_acao_severidade_p	varchar2) is

/*
ie_acao_p
L = Libera��o
E = Estorno
*/
ie_situacao_w	varchar2(15);
qt_existe_w	number(10,0);
cd_funcao_w	number(05,0);

begin

if	(ie_acao_p = 'L') then
	begin
	select	f.ie_situacao,
		f.cd_funcao
	into	ie_situacao_w,
		cd_funcao_w
	from	funcao f,
		man_ordem_servico a,
		man_doc_erro b
	where	b.nr_seq_ordem 	= a.nr_sequencia
	and	f.cd_funcao 	= a.cd_funcao
	and	b.nr_sequencia 	= nr_sequencia_p;
	exception
	when others then
		ie_situacao_w := '';
	end;

	if	(nr_seq_motivo_testes_p = 2) then /*Fun��o fora do protocolo de testes, verifica se realmente n�o existe protocolo para a fun��o selecionada*/
		select	count(*)
		into	qt_existe_w
		from	teste_funcao
		where	cd_funcao = cd_funcao_w
		and	ie_situacao = 'A';
		
		if	(qt_existe_w <> 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(194410);
		end if;
	end if;
	
	update	man_doc_erro
	set	dt_liberacao 		= sysdate,
		nm_usuario		= nm_usuario_p,
		ds_justificativa		= ds_justificativa_p,
		ie_forma_origem		= ie_forma_origem_p,
		ie_libera_correcao		= ie_libera_correcao_p,
		nr_seq_motivo_testes	= nr_seq_motivo_testes_p,
		ie_situacao_funcao		= ie_situacao_w,
		ds_acao_severidade	= ds_acao_severidade_p
	where	nr_sequencia		= nr_sequencia_p;
elsif	(ie_acao_p = 'E') then
	update	man_doc_erro
	set	dt_liberacao 		= null,
		nm_usuario		= nm_usuario_p,
		ds_justificativa		= ds_justificativa_p,
		ie_forma_origem		= ie_forma_origem_p,
		ie_libera_correcao		= ie_libera_correcao_p,
		ie_situacao_funcao		= '',
		ds_acao_severidade	= ds_acao_severidade_p
	where	nr_sequencia		= nr_sequencia_p;
end if;

commit;

end man_liberar_doc_erro;
/