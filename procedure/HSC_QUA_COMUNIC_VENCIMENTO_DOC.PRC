create or replace
procedure hsc_qua_comunic_vencimento_doc(
			qt_dia_aviso_p		number,
			cd_estabelecimento_p	number) is

nr_seq_documento_w	varchar2(10);
cd_documento_w		varchar2(20);
nm_documento_w		varchar2(255);
nm_usuario_elab_w		varchar2(15);
nm_usuario_aprov_w	varchar2(15);
nr_seq_ult_revisao_w	number(10);
nm_usuario_rev_w		varchar2(15);
nm_usuario_aprov_rev_w	varchar2(15);
dt_vencimento_w		date;
ds_titulo_w		varchar2(255);
ds_comunicado_w		varchar2(4000);
nm_usuario_destino_w	varchar2(4000);
nr_seq_classif_w		varchar2(10) := obter_classif_comunic('F');

Cursor C01 is
	select	a.nr_sequencia,
		a.cd_documento,
		a.nm_documento,
		substr(obter_usuario_ativo_pf(a.cd_pessoa_elaboracao),1,15) nm_usuario_elab,
		substr(obter_usuario_ativo_pf(a.cd_pessoa_aprov),1,15) nm_usuario_aprov,
		nvl(a.dt_revalidacao,nvl(to_date(substr(qua_obter_dados_documento(a.nr_sequencia,'UAR'),1,20),'dd/mm/yyyy hh24:mi:ss'),
		nvl(a.dt_aprovacao,nvl(a.dt_validacao,a.dt_elaboracao)))) + a.qt_dias_revisao
	from	qua_documento a
	where	a.ie_situacao = 'A'
	and	a.ie_status not in ('R','I')
	and	nvl(a.qt_dias_revisao,0) > 0
	and	trunc(nvl(a.dt_revalidacao,nvl(to_date(substr(qua_obter_dados_documento(a.nr_sequencia,'UAR'),1,20),'dd/mm/yyyy hh24:mi:ss'),
		nvl(a.dt_aprovacao,nvl(a.dt_validacao,a.dt_elaboracao))))) + (a.qt_dias_revisao - qt_dia_aviso_p) = trunc(sysdate)
	and	(nvl(cd_estabelecimento_p,0) = 0 or a.cd_estabelecimento = cd_estabelecimento_p)
	and	qt_dia_aviso_p is not null
	union all
	select	a.nr_sequencia,
		a.cd_documento,
		a.nm_documento,
		substr(obter_usuario_ativo_pf(a.cd_pessoa_elaboracao),1,15) nm_usuario_elab,
		substr(obter_usuario_ativo_pf(a.cd_pessoa_aprov),1,15) nm_usuario_aprov,
		nvl(a.dt_revalidacao,nvl(to_date(substr(qua_obter_dados_documento(a.nr_sequencia,'UAR'),1,20),'dd/mm/yyyy hh24:mi:ss'),
		nvl(a.dt_aprovacao,nvl(a.dt_validacao,a.dt_elaboracao)))) + a.qt_dias_revisao
	from	qua_documento a
	where	a.ie_situacao = 'A'
	and	a.ie_status not in ('R','I')
	and	nvl(a.qt_dias_revisao,0) > 0
	and	trunc(nvl(a.dt_revalidacao,nvl(to_date(substr(qua_obter_dados_documento(a.nr_sequencia,'UAR'),1,20),'dd/mm/yyyy hh24:mi:ss'),
		nvl(a.dt_aprovacao,nvl(a.dt_validacao,a.dt_elaboracao)))) + a.qt_dias_revisao,'mm') = trunc(add_months(sysdate,1),'mm')
	and	trunc(sysdate) = trunc(sysdate,'mm')
	and	(nvl(cd_estabelecimento_p,0) = 0 or a.cd_estabelecimento = cd_estabelecimento_p)
	and	qt_dia_aviso_p is null;

begin

ds_titulo_w	:= substr('Alerta - Vencimento do documento da Qualidade',1,255);	

open C01;
loop
fetch C01 into
	nr_seq_documento_w,
	cd_documento_w,
	nm_documento_w,
	nm_usuario_elab_w,
	nm_usuario_aprov_w,
	dt_vencimento_w;
exit when C01%notfound;
	begin
	/* Limpar v�riavel para colocar as pessoas que ser�o comunicadas neste documento */
	nm_usuario_destino_w	:= '';

	ds_comunicado_w	:= substr('O documento ' || cd_documento_w || ' - ' || nm_documento_w || ', vence no dia ' || to_char(dt_vencimento_w,'dd/mm/yyyy') || '.',1,4000);

	if	(nvl(nm_usuario_elab_w,'0') <> '0') then
		nm_usuario_destino_w := substr(nm_usuario_destino_w || nm_usuario_elab_w || ', ',1,4000);
	end if;

	if	(nvl(nm_usuario_aprov_w,'0') <> '0') then
		nm_usuario_destino_w := substr(nm_usuario_destino_w || nm_usuario_aprov_w || ', ',1,4000);
	end if;

	select	max(nr_sequencia)
	into	nr_seq_ult_revisao_w
	from	qua_doc_revisao
	where	dt_aprovacao is not null
	and	nr_seq_doc = nr_seq_documento_w;

	if	(nvl(nr_seq_ult_revisao_w,0) > 0) then
		begin
		select	substr(obter_usuario_ativo_pf(cd_pessoa_revisao),1,15) nm_usuario_rev,
			substr(obter_usuario_ativo_pf(cd_pessoa_aprovacao),1,15) nm_usuario_aprov_rev
		into	nm_usuario_rev_w,
			nm_usuario_aprov_rev_w
		from	qua_doc_revisao
		where	nr_sequencia = nr_seq_ult_revisao_w;

		if	(nvl(nm_usuario_rev_w,'0') <> '0') then
			nm_usuario_destino_w := substr(nm_usuario_destino_w || nm_usuario_rev_w || ', ',1,4000);
		end if;

		if	(nvl(nm_usuario_aprov_rev_w,'0') <> '0') then
			nm_usuario_destino_w := substr(nm_usuario_destino_w || nm_usuario_aprov_rev_w || ', ',1,4000);
		end if;
		end;
	end if;

	if	(nvl(nm_usuario_destino_w,'0') <> '0') then
		begin
		gerar_comunic_padrao(
				sysdate,
				ds_titulo_w,
				ds_comunicado_w,
				'Tasy',
				'N',
				nm_usuario_destino_w,
				'N',
				nr_seq_classif_w,
				null,
				null,
				null,
				sysdate,
				null,
				null);
		end;
	end if;
	end;
end loop;
close C01;

commit;

end hsc_qua_comunic_vencimento_doc;
/