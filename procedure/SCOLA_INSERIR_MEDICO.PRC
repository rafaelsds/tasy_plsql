create or replace
procedure scola_inserir_medico (	cd_pessoa_fisica_p	varchar2,
					nm_guerra_p		varchar2,
					nr_crm_p		varchar2,
					uf_crm_p		varchar2,
					ie_vinculo_medico_p	varchar2,
					ie_corpo_clinico_p	varchar2, 
					ie_corpo_assist_p	varchar2,
					cd_erro_p		out number,
					ds_erro_p		out varchar2) is 

ie_existe_pf_w			varchar2(1);
ie_existe_uf_w			varchar2(1);
ie_existe_crm_uf_w		varchar2(1);
ds_erro_w			varchar2(1000);
					
/*
0 - SUCESSO
1 - Registro n�o informado
	1.1 - C�digo da pessoa f�sica n�o informada
	1.2 - Nome de guerra n�o informado
	1.3 - CRM N�o informado
	1.4 - UF n�o informado
	1.5 - V�nculo m�dico n�o informado
	1.6 - Corpo clinico n�o informado
	1.7 - Corpo assistencial n�o informado
2 - Registro n�o encontrado
	2.1 - Pessoa f�sica n�o existe
	2.2 - UF Inexistente, verificar o dom�nio 50
	2.3 - CRM j� est� cadastrado para outro m�dico
3 - Erro ao inserir o registro
*/					
					
begin
cd_erro_p	:= 0;

if	(cd_pessoa_fisica_p is null) then
	cd_erro_p	:= 1.1;
	ds_erro_p	:= wheb_mensagem_pck.get_texto(281319,null);
elsif	(nm_guerra_p is null) then
	cd_erro_p	:= 1.2;
	ds_erro_p	:= wheb_mensagem_pck.get_texto(278062,null);
elsif	(nr_crm_p is null) then
	cd_erro_p	:= 1.3;
	ds_erro_p	:= wheb_mensagem_pck.get_texto(278063,null);
elsif	(uf_crm_p is null)  then
	cd_erro_p	:= 1.4;
	ds_erro_p	:= wheb_mensagem_pck.get_texto(281311,null);
elsif	(ie_vinculo_medico_p is null) then
	cd_erro_p	:= 1.5;
	ds_erro_p	:= wheb_mensagem_pck.get_texto(278065,null);
elsif	(ie_corpo_clinico_p is null) then
	cd_erro_p	:= 1.6;
	ds_erro_p	:= wheb_mensagem_pck.get_texto(281325,null);
elsif	(ie_corpo_assist_p is null) then
	cd_erro_p	:= 1.7;
	ds_erro_p	:= wheb_mensagem_pck.get_texto(278067,null);
end if;

if	(cd_erro_p = 0) then
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_pf_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_uf_w
	from	valor_dominio
	where	cd_dominio = 50
	and	vl_dominio = uf_crm_p;
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_crm_uf_w
	from	medico
	where	nr_crm = nr_crm_p
	and	uf_crm = uf_crm_p;
	
	if	(ie_existe_pf_w = 'N') then
		cd_erro_p	:= 2.1;
		ds_erro_p	:= wheb_mensagem_pck.get_texto(281320,null);
	elsif	(ie_existe_uf_w = 'N') then
		cd_erro_p	:= 2.2;
		ds_erro_p	:= wheb_mensagem_pck.get_texto(281321,null);
	elsif	(ie_existe_crm_uf_w = 'S') then
		cd_erro_p	:= 2.3;
		ds_erro_p	:= wheb_mensagem_pck.get_texto(281323,null);
	end if;
	
	if	(cd_erro_p = 0) then
		begin
		insert into medico(	cd_pessoa_fisica,
					nm_guerra,
					nr_crm,
					uf_crm,
					ie_vinculo_medico,
					ie_corpo_clinico,
					ie_corpo_assist,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					ie_situacao)
			values	(	cd_pessoa_fisica_p,
			                nm_guerra_p,
			                nr_crm_p,
			                uf_crm_p,
			                ie_vinculo_medico_p,
			                ie_corpo_clinico_p,
			                ie_corpo_assist_p,
					sysdate,
					sysdate,
					'SCOLA',
					'SCOLA',
					'A');
		commit;
		exception
		when others then
			ds_erro_w	:= substr(sqlerrm,1,1000);
			cd_erro_p	:= 3;
			ds_erro_p	:= wheb_mensagem_pck.get_texto(281324,null) ||ds_erro_w;
		end;
	end if;
	
end if;

end scola_inserir_medico;
/