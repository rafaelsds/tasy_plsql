create or replace
procedure pls_gerar_titulo_prot_reemb(	nr_seq_protocolo_p	pls_protocolo_conta.nr_sequencia%type,
					dt_vencimento_p		date,
					dt_emissao_p		date,
					nm_usuario_p		usuario.nm_usuario%type,
					ie_commit_p		varchar2,
					dt_venc_inf_p		varchar2) is 

ds_vencimentos_w		varchar2(4000);
cd_conta_cred_w			varchar2(20);
cd_cgc_w			varchar2(14);
cd_cgc_estipulante_w		varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
cd_pessoa_fisica_ww		varchar2(10);
cd_pf_estipulante_w		varchar2(10);
ie_tipo_resp_credito_w		varchar2(2);
qt_vencimentos_w		number(10);
ie_forma_pagamento_prot_w	varchar2(2);
ie_mes_fechado_w		varchar2(1);
ie_acao_venc_nao_util_w		varchar2(1)	:= 'M';
tx_fracao_parcela_w		number(15,4)	:= 0;
tx_acrescimo_w			number(15,4)	:= 0;
vl_classif_w			number(15,2) 	:= 0;
vl_reembolso_w			number(15,2);
vl_titulo_w			number(15,2);
vl_proc_mat_w			number(15,2);
cd_conta_financ_regra_w		number(10)	:= null;
cd_tipo_taxa_juros_w		number(10);
cd_tipo_taxa_multa_w		number(10);
nr_titulo_w			number(10);
cd_condicao_pagamento_w		number(10);
nr_seq_segurado_w		number(10);
nr_seq_trans_fin_baixa_reemb_w	number(10);
nr_contrato_principal_w		number(10);
cd_conta_financ_w		number(10);
nr_seq_classif_w		number(10);
nr_seq_subestipulante_w		number(10);
nr_seq_titular_w		number(10);
nr_seq_regra_reemb_w		number(10);
nr_seq_trans_fin_baixa_regra_w	number(10);
pr_juros_padrao_w		number(7,4);
pr_multa_padrao_w		number(7,4);
cd_moeda_w			number(5);
cd_estabelecimento_w		number(4);
ie_forma_pagamento_w		number(2);
dt_referencia_w			date;
dt_vencimento_w			date;
dt_base_venc_w			date;
dt_protocolo_w			date;
dt_emissao_w			date;
qt_dia_vencimento_w		number(10);
nr_seq_lote_reemb_cred_w	pls_protocolo_conta.nr_seq_lote_reemb_cred%type;
nr_seq_protocolo_w		pls_protocolo_conta.nr_sequencia%type;
ds_observacao_titulo_w		pls_protocolo_conta.ds_observacao%type;
nr_seq_classe_w			pls_regra_tit_reembolso.nr_seq_classe%type;  --aldellandrea  os 793353
nr_seq_trans_fin_contab_w	pls_regra_tit_reembolso.nr_seq_trans_fin_contab%type; --//aldellandrea  os 793353
vl_baixas_realizadas_w		titulo_receber_liq.vl_recebido%type;
qt_tit_pag_gerados_w		pls_integer;
qt_tit_rec_gerados_w		pls_integer;
ie_impedir_tit_reemb_a500_w	pls_parametros.ie_impedir_tit_reemb_a500%type;	
ie_origem_protocolo_w		pls_protocolo_conta.ie_origem_protocolo%type;

cursor C01 is
	select	tx_fracao_parcela,	--A Prazo
		nvl(tx_acrescimo, 0),
		null
	from	parcela
	where	cd_condicao_pagamento 	= cd_condicao_pagamento_w
	and	ie_forma_pagamento_w	not in (1,10)
	union
	select	100, -- A Vista
		0,	
		dt_base_venc_w
	from	dual
	where	ie_forma_pagamento_w	= 1
	union
	select	100,
		0, -- Conforme Vencimentos
		dt_base_venc_w
	from	dual
	where	ie_forma_pagamento_w	= 10;
	
cursor C02 is
	select	sum(vl_proc_mat),
		cd_conta_cred
	from	(select	a.cd_conta_cred,
			nvl(a.vl_liberado,0) vl_proc_mat
		from	pls_conta_mat a,
			pls_conta c
		where	a.nr_seq_conta = c.nr_sequencia
		and	c.nr_seq_protocolo	= nr_seq_protocolo_p
		union all
		select	b.cd_conta_cred,
			nvl(b.vl_liberado,0) - nvl(b.vl_coparticipacao,0) vl_proc_mat
		from	pls_conta_proc b,
			pls_conta c
		where	b.nr_seq_conta = c.nr_sequencia
		and	c.nr_seq_protocolo	= nr_seq_protocolo_p)
	group by cd_conta_cred;
			
begin

dt_base_venc_w	:= dt_vencimento_p;
dt_emissao_w	:= nvl(dt_emissao_p,sysdate);

-- busca os dados do protocolo de reembolso
select	trunc(dt_mes_competencia, 'dd'),
	nvl(pls_obter_valor_protocolo(nr_sequencia,'T'),0),
	substr(pls_obter_dados_protocolo(nr_sequencia, 'PF'),1,60),
	cd_condicao_pagamento,
	ie_tipo_resp_credito,
	nr_seq_segurado,
	cd_estabelecimento,
	cd_pessoa_fisica,
	ie_forma_pagamento,
	nvl(dt_protocolo,sysdate),
	nr_seq_lote_reemb_cred,
	nr_sequencia,
	ds_observacao,
	nvl(ie_origem_protocolo,'X')
into	dt_referencia_w,
	vl_reembolso_w,
	cd_pessoa_fisica_w,
	cd_condicao_pagamento_w,
	ie_tipo_resp_credito_w,
	nr_seq_segurado_w,
	cd_estabelecimento_w,
	cd_pessoa_fisica_ww,
	ie_forma_pagamento_prot_w,
	dt_protocolo_w,
	nr_seq_lote_reemb_cred_w,
	nr_seq_protocolo_w,
	ds_observacao_titulo_w,
	ie_origem_protocolo_w
from	pls_protocolo_conta
where	nr_sequencia = nr_seq_protocolo_p;

--Desconta os valores abatido de mensalidade
select 	nvl(sum(vl_recebido),0)
into	vl_baixas_realizadas_w
from	titulo_receber_liq
where	nr_seq_reembolso = nr_seq_protocolo_p;

vl_reembolso_w := vl_reembolso_w - vl_baixas_realizadas_w;

if	(nr_seq_lote_reemb_cred_w is not null) then
	select 	a.cd_condicao_pagamento,
		a.ie_forma_pagamento
	into	cd_condicao_pagamento_w,
		ie_forma_pagamento_prot_w
	from 	pls_lote_reemb_credito a
	where	a.nr_sequencia = nr_seq_lote_reemb_cred_w;
end if;

if	(cd_condicao_pagamento_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(196696);
end if;

select	pls_obter_se_mes_fechado(dt_referencia_w, 'T', cd_estabelecimento_w)
into	ie_mes_fechado_w
from	dual;

if 	(ie_mes_fechado_w = 'S') then
	--'N?o ? poss?vel realizar esta opera??o pois o m?s de compet?ncia ou a contabilidade do m?s est? fechada!');
	wheb_mensagem_pck.exibir_mensagem_abort(196697);
end if;

begin
	select	cd_tipo_taxa_juro,
		pr_juro_padrao,
		cd_tipo_taxa_multa,
		pr_multa_padrao,
		nr_seq_trans_fin_baixa_reemb,
		cd_conta_financ_reembolso,
		ie_impedir_tit_reemb_a500
	into	cd_tipo_taxa_juros_w,
		pr_juros_padrao_w,
		cd_tipo_taxa_multa_w,
		pr_multa_padrao_w,
		nr_seq_trans_fin_baixa_reemb_w,
		cd_conta_financ_w,
		ie_impedir_tit_reemb_a500_w
	from	table(pls_parametros_pck.f_retorna_param(cd_estabelecimento_w));
exception
when others then
	--'Problema na leitura dos par?metros na fun??o OPS - Gest?o de Operadoras. [' || cd_estabelecimento_w || '] #@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(196698);
end;

if	(pr_juros_padrao_w is null) then
	--(-20011,'N?o foi informado o percentual de juros padr?o. Fun??o OPS - Gest?o de Operadoras / Par?metros OPS / Mensalidades!#@#@');
	wheb_mensagem_pck.exibir_mensagem_abort(196699);
end if;

-- Obter dados do contrato
begin
select	b.cd_cgc_estipulante,
	b.cd_pf_estipulante,
	nvl(b.nr_contrato_principal,0),
	a.nr_seq_subestipulante
into	cd_cgc_estipulante_w,
	cd_pf_estipulante_w,
	nr_contrato_principal_w,
	nr_seq_subestipulante_w
from	pls_segurado	a,
	pls_contrato	b
where	a.nr_seq_contrato	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_segurado_w;
exception
when others then
	cd_cgc_estipulante_w	:= '';
	cd_pf_estipulante_w	:= '';
end;

if	(ie_tipo_resp_credito_w = 'P') then

	select	a.cd_pessoa_fisica,
		a.cd_cgc
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	pls_contrato_pagador a,
		pls_segurado b
	where	a.nr_sequencia = b.nr_seq_pagador
	and	b.nr_sequencia = nr_seq_segurado_w;
	
elsif	(ie_tipo_resp_credito_w = 'E') then

	cd_pessoa_fisica_w	:= cd_pf_estipulante_w;
	cd_cgc_w		:= cd_cgc_estipulante_w;
	
elsif	(ie_tipo_resp_credito_w = 'CP') and 
	(nr_contrato_principal_w > 0) then
	
	select	cd_cgc_estipulante,
		cd_pf_estipulante
	into	cd_cgc_w,
		cd_pessoa_fisica_w
	from	pls_contrato
	where	nr_sequencia	= nr_contrato_principal_w;
	
elsif	(ie_tipo_resp_credito_w = 'SE') then

	begin
	select	cd_pessoa_fisica,
		cd_cgc
	into	cd_pessoa_fisica_w,
		cd_cgc_w
	from	pls_sub_estipulante
	where	nr_sequencia	= nr_seq_subestipulante_w;
	exception
	when others then
		cd_pessoa_fisica_w	:= '';
		cd_cgc_w			:= '';
	end;
	
elsif	(ie_tipo_resp_credito_w = 'T') then

	select	nvl(nr_seq_titular, 0)
	into	nr_seq_titular_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	
	if	(nr_seq_titular_w	<> 0) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_titular_w;
		
	elsif	(nr_seq_titular_w	= 0) then
		select	cd_pessoa_fisica
		into	cd_pessoa_fisica_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_w;
	end if;
	
	cd_cgc_w	:= '';
	
elsif	(ie_tipo_resp_credito_w = 'PI') then

	cd_pessoa_fisica_w	:= cd_pessoa_fisica_ww;	
	cd_cgc_w		:= '';
end if;

select	cd_moeda_padrao
into	cd_moeda_w
from	parametros_contas_pagar
where	cd_estabelecimento = cd_estabelecimento_w;

select	nvl(max(ie_forma_pagamento),3),
	nvl(max(ie_acao_nao_util),'M')
into	ie_forma_pagamento_w,
	ie_acao_venc_nao_util_w
from	condicao_pagamento
where	cd_condicao_pagamento = cd_condicao_pagamento_w
and	ie_situacao = 'A';

pls_obter_conta_financ_regra(	'RE', 			null, 		cd_estabelecimento_w,
				null, 			null, 		null,
				null, 			null, 		null,
				nr_seq_protocolo_p, 	null, 		null,
				null, 			null, 		null,
				null, 			null,		cd_conta_financ_regra_w);
				
pls_obter_regra_tit_pag_reemb(ie_forma_pagamento_prot_w, dt_protocolo_w, nr_seq_regra_reemb_w);

if	(nr_seq_regra_reemb_w is not null) then

	select	max(nr_seq_trans_fin_baixa),
		max(nr_seq_classe), --aldellandrea  os 793353
		max(nr_seq_trans_fin_contab) --aldellandrea  os 793353 -- pego os valor informado na regra para abaixo inserir no titulos
	into	nr_seq_trans_fin_baixa_regra_w,
		nr_seq_classe_w,		
		nr_seq_trans_fin_contab_w
	from	pls_regra_tit_reembolso
	where	nr_sequencia	= nr_seq_regra_reemb_w;
	
end if;

if	(cd_condicao_pagamento_w is not null) then
	if( dt_venc_inf_p is null) then --aldelandrea os 797146 criado pois no reembolso ao gerar o titulo ja calcula a data e apresenta no wdlg para o usuario ter certeza da dta entao n?o precisa calcular de novo apenas gerar
		calcular_vencimento(	cd_estabelecimento_w, cd_condicao_pagamento_w, dt_base_venc_w,
					qt_vencimentos_w, ds_vencimentos_w);
	else
		ds_vencimentos_w := to_char(dt_base_venc_w, 'dd/mm/yyyy'); -- aldellandrea os 797146
	end if;				
end if;

--nao tenta gerar titulo caso parametrizado para nao gerar em protocolos de a500 e for esse o tipo de protocolo
if  not (ie_origem_protocolo_w = 'A' and ie_impedir_tit_reemb_a500_w = 'S') then 
	open C01;
	loop
	fetch C01 into
		tx_fracao_parcela_w,
		tx_acrescimo_w,
		dt_vencimento_w;
	exit when C01%notfound;
		begin
		
		dt_vencimento_w		:= to_date(substr(ds_vencimentos_w,1,10), 'dd/mm/yyyy');
		ds_vencimentos_w	:= substr(ds_vencimentos_w, 12, length(ds_vencimentos_w));
		
		if	(dt_vencimento_w is null) then
			dt_vencimento_w	:= ds_vencimentos_w;
		end if;
		
		if	(ie_forma_pagamento_w	= 10) and
			(trunc(dt_vencimento_w)	< trunc(sysdate)) then
			--'A data vencimento n?o pode ser inferior a data de hoje. 
			wheb_mensagem_pck.exibir_mensagem_abort(196700);		
		end if;
		
		if	(nvl(tx_fracao_parcela_w,0) > 0) then
			vl_titulo_w 	:= ((vl_reembolso_w * tx_fracao_parcela_w) / 100);
		end if;
			
		if 	(tx_acrescimo_w <> 0) then
			vl_titulo_w 	:= vl_titulo_w + ((vl_titulo_w * tx_acrescimo_w) / 100);
		end if;
		
		--Pode zerar o valor devido ao desconto do reembolso no valor da mensalidade
		if	(vl_titulo_w <> 0) then
			insert into titulo_pagar (
				nr_titulo, cd_estabelecimento, dt_atualizacao,
				nm_usuario, dt_emissao, dt_vencimento_original,
				dt_vencimento_atual, vl_titulo, vl_saldo_titulo,
				vl_saldo_juros, vl_saldo_multa, cd_moeda,
				tx_juros, tx_multa, cd_tipo_taxa_juro,
				cd_tipo_taxa_multa, ie_situacao, ie_origem_titulo,
				ie_tipo_titulo, cd_pessoa_fisica, cd_cgc,
				ie_pls, nr_seq_reembolso, nr_seq_trans_fin_baixa, 
				dt_contabil, nr_documento, ds_observacao_titulo,
				nr_seq_classe, nr_seq_trans_fin_contab)
			values(	titulo_pagar_seq.nextval, cd_estabelecimento_w, sysdate,
				nm_usuario_p, dt_emissao_w, dt_vencimento_w,
				dt_vencimento_w, vl_titulo_w, vl_titulo_w,
				0, 0, cd_moeda_w,
				pr_juros_padrao_w, pr_multa_padrao_w, cd_tipo_taxa_juros_w,
				cd_tipo_taxa_multa_w, 'A', '6',
				'10', cd_pessoa_fisica_w, cd_cgc_w,
				'S', nr_seq_protocolo_p, nvl(nr_seq_trans_fin_baixa_regra_w,nr_seq_trans_fin_baixa_reemb_w),
				sysdate, nr_seq_protocolo_w, ds_observacao_titulo_w, 
				nr_seq_classe_w, nr_seq_trans_fin_contab_w  -- //aldellandrea  os 793353
				) returning nr_titulo into nr_titulo_w;
				
			atualizar_inclusao_tit_pagar(nr_titulo_w, nm_usuario_p);
			
			open C02;
			loop
			fetch C02 into	
				vl_proc_mat_w,
				cd_conta_cred_w;
			exit when C02%notfound;
				begin
				
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_seq_classif_w
				from	titulo_pagar_classif
				where	nr_titulo	= nr_titulo_w;
				
				vl_classif_w	:= vl_proc_mat_w * dividir_sem_round(vl_titulo_w,vl_reembolso_w);
				
				insert into titulo_pagar_classif(
					dt_atualizacao, nm_usuario, nr_seq_conta_financ,
					nr_sequencia, nr_titulo, vl_acrescimo,
					vl_desconto, vl_original, vl_titulo, cd_conta_contabil)
				values(	sysdate, nm_usuario_p, nvl(cd_conta_financ_regra_w,cd_conta_financ_w),
					nr_seq_classif_w, nr_titulo_w, 0,
					0, vl_classif_w,vl_classif_w, cd_conta_cred_w);
				end;
			end loop;
			close C02;
		end if;
		
		end;
	end loop;
	close C01;

	/*Se ao gerar titulos de pagto, todo o valor for em desconto de mensalidade, ent?o atualiza o reembolso para
	pago, pois sen?o o mesmo permanecer? sempre como liberado para pagamento. Se tiver ao menos um titulo a pagar
	gerado, ent?o n?o atualiza o status*/
	select	count(1)
	into	qt_tit_pag_gerados_w
	from	titulo_pagar
	where	nr_seq_reembolso = nr_seq_protocolo_p;

	if	(qt_tit_pag_gerados_w = 0) then
		select	nvl(count(1),0)
		into	qt_tit_rec_gerados_w
		from	titulo_receber_liq
		where	nr_seq_reembolso = nr_seq_protocolo_p;
	else
		-- jtonon - OS 1108463 - Envio de alerta SMS/Email para o benefici?rio
		pls_gerar_alerta_rembolso_lib(12, nr_seq_protocolo_p, null, nm_usuario_p);
	end if;

	if	(qt_tit_pag_gerados_w = 0 and qt_tit_rec_gerados_w > 0) then
		update 	pls_protocolo_conta
		set	ie_status = 6
		where 	nr_sequencia = nr_seq_protocolo_p;
	end if;
end if;

if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_gerar_titulo_prot_reemb;
/