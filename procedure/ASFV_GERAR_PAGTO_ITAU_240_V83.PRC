create or replace
procedure ASFV_GERAR_PAGTO_ITAU_240_V83
		(	nr_seq_envio_p		number,
			nm_usuario_p		varchar2) is

/* PADR�O CNAB 240 VERS�O 080 DE 07/2010 */

ds_conteudo_w		varchar2(240);
cd_estabelecimento_w	number(4);
nr_seq_apres_w		number(10)	:= 0;

/* header de arquivo */
cd_agencia_estab_w	varchar2(5);
cd_cgc_estab_w		varchar2(14);
cd_convenio_banco_w	varchar2(20);
dt_geracao_w		varchar2(14);
nm_empresa_w		varchar2(30);
nr_conta_estab_w	varchar2(12);
nr_remessa_w		number(10);
ie_digito_estab_w	varchar2(1);

/* header de lote - DOC */
nr_lote_servico_w	number(10);
ie_forma_lanc_w		varchar2(2);
ie_tipo_pagamento_w	varchar2(3);
ie_finalidade_w		varchar2(2);
ds_endereco_w		varchar2(30);
nr_endereco_w		varchar2(5);
ds_complemento_w	varchar2(15);
ds_municipio_w		varchar2(20);
nr_cep_w		varchar2(8);
sg_estado_w		varchar2(15);
ie_converte_bloq_w	varchar2(1);
ie_tipo_pagto_w		varchar2(2);
nr_versao_w		varchar2(3);
ie_forma_lanc_blq_w 	VARCHAR(2);
ie_data_pagamento_w	banco_estabelecimento.ie_data_pagamento%type;
dt_pagamento_w		date;
cd_cgc_tit_w	titulo_pagar.cd_cgc%type;

cursor	c01 is
select	distinct
	decode(b.ie_tipo_pagamento,'CC','01','DOC','03','TED','41','OP','30','BLQ',decode(b.cd_banco,341,'30','31'),'PC','13','CCP','01','03'),
	b.ie_tipo_pagamento,
	decode(b.ie_tipo_pagamento,'CC','01','CCP','01','99'),
	decode(b.ie_tipo_pagamento,'PC',decode((select count(*) from darf_titulo_pagar x where x.nr_titulo = b.nr_titulo),0,'20','22'),'20') ie_tipo_pagto,
	(select max(x.cd_cgc) from titulo_pagar x where x.nr_titulo = b.nr_titulo) cd_cgc_tit
from	titulo_pagar_escrit b,
	banco_escritural a
where	b.ie_tipo_pagamento	in ('DOC','CC','OP','TED','PC','CCP')
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* detalhe - DOC */
nr_sequencia_w		number(10);
cd_camara_compensacao_w	varchar2(3);
cd_banco_w		number(3);
cd_agencia_bancaria_w	varchar2(6);
nr_conta_w		varchar2(20);
nm_pessoa_w		varchar2(30);
nr_titulo_w		number(10);
dt_remessa_retorno_w	date;
vl_escritural_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_desconto_w		number(15,2);
vl_despesa_w		number(15,2);
ie_tipo_inscricao_w	varchar2(1);
nr_inscricao_w		varchar2(14);
dt_vencimento_w		date;
vl_juros_w		number(15,2);
vl_multa_w		number(15,2);
cd_agencia_conta_w	varchar2(20);
ie_digito_agencia_w	varchar2(2);
cd_agencia_w		varchar2(8);
nr_nosso_numero_w	varchar2(15);
ie_digito_conta_w	varchar2(2);

cursor	c02 is
select	b.cd_banco,
	lpad(substr(b.cd_agencia_bancaria,1,5) || substr(b.ie_digito_agencia,1,1),6,'0'),
	rpad(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,30),30,' '),
	c.nr_titulo,
	b.vl_escritural,
	b.vl_acrescimo,
	b.vl_desconto,
	b.vl_despesa,
	lpad(nvl(d.nr_cpf,c.cd_cgc),14,'0'),
	b.nr_conta,
	lpad(nvl(substr(b.ie_digito_agencia,1,2),' '),2,' '),
	b.cd_agencia_bancaria,
	rpad(substr(c.nr_nosso_numero,1,15),15,' ') nr_nosso_numero,
	c.dt_vencimento_atual,
	lpad(nvl(substr(b.ie_digito_conta,1,2),' '),2,' '),
	c.nr_bloqueto,
	b.vl_juros,
	b.vl_multa
from	pessoa_fisica d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	decode(b.ie_tipo_pagamento,'PC',decode((select count(*) from darf_titulo_pagar x where x.nr_titulo = b.nr_titulo),0,'20','22'),'20') = ie_tipo_pagto_w
and	c.cd_pessoa_fisica	= d.cd_pessoa_fisica(+)
and	b.nr_titulo		= c.nr_titulo
and	b.ie_tipo_pagamento	= ie_tipo_pagamento_w
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* trailer lote - DOC */
vl_total_w		number(15,2);

/* header de lote - BLQ */
ie_tipo_bloqueto_w	varchar2(1);
ie_tipo_titulo_w	varchar2(2);

cursor	c03 is
select	distinct
	decode(	b.ie_tipo_pagamento,
		'PC',
		'13',
		decode(	(select	count(*)
			from	banco x
			where	x.cd_banco	= somente_numero(substr(c.nr_bloqueto,1,3))),
			0,
			decode(c.nr_bloqueto,null,'13','91'),
			decode(substr(c.nr_bloqueto,1,3),'341','30','31'))),
	b.ie_tipo_pagamento,
	decode(	(select	count(*)
		from	banco x
		where	x.cd_banco	= somente_numero(substr(c.nr_bloqueto,1,3))),
		0,
		'I',
		'B') ie_tipo_bloqueto,
	'20'
from	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	b.nr_titulo		= c.nr_titulo
and	b.ie_tipo_pagamento	in ('BLQ','DDA')
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* detalhe - BLQ */
nr_bloqueto_w		varchar2(48);
ie_tipo_identificacao_w	varchar2(2);

cursor	c04 is
select	rpad(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,30),30,' '),
	c.nr_titulo,
	b.vl_escritural,
	b.vl_acrescimo,
	b.vl_desconto,
	b.vl_despesa,
	c.dt_vencimento_atual,
	b.vl_juros,
	b.vl_multa,
	substr(decode(ie_tipo_bloqueto_w,'I',c.nr_bloqueto,decode(nvl(ie_converte_bloq_w,'N'),'S',converte_codigo_bloqueto('Lido_Barra',c.nr_bloqueto),c.nr_bloqueto)),1,48) nr_bloqueto,
	lpad(nvl(d.nr_cpf,c.cd_cgc),14,'0'),
	decode(c.cd_cgc,null,'01','02') ie_tipo_identificacao
from	pessoa_fisica d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	c.cd_pessoa_fisica	= d.cd_pessoa_fisica(+)
and	ie_tipo_bloqueto_w	=
	decode(	(select	count(*)
		from	banco x
		where	x.cd_banco	= somente_numero(substr(c.nr_bloqueto,1,3))),
		0,
		'I',
		'B')
and	b.nr_titulo		= c.nr_titulo
and	ie_forma_lanc_w		= 
	decode(	b.ie_tipo_pagamento,
		'PC',
		'13',
		decode(	(select	count(*)
			from	banco x
			where	x.cd_banco	= somente_numero(substr(c.nr_bloqueto,1,3))),
			0,
			decode(c.nr_bloqueto,null,'13','91'),
			decode(substr(c.nr_bloqueto,1,3),'341','30','31')))
and	b.ie_tipo_pagamento	= ie_tipo_pagamento_w
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* trailer do arquivo */
qt_registro_w		number(10);

begin

delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;

/* header de arquivo */
nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;

select	lpad(b.cd_cgc,14,'0'),
	rpad(substr(c.cd_convenio_banco,1,20),20,' '),
	lpad(substr(c.cd_agencia_bancaria,1,5),5,'0'),
	lpad(somente_numero(substr(c.cd_conta,1,12)),12,'0'),
	rpad(substr(obter_nome_estabelecimento(b.cd_estabelecimento),1,30),30,' '),
	to_char(sysdate,'ddmmyyyyhh24miss'),
	nvl(a.nr_remessa,a.nr_sequencia),
	b.cd_estabelecimento,
	a.dt_remessa_retorno,
	rpad(substr(obter_dados_pf_pj(null,b.cd_cgc,'R'),1,30),30,' '),
	lpad(substr(obter_dados_pf_pj(null,b.cd_cgc,'NR'),1,5),5,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'CO'),1,15),' '),15,' '),
	rpad(substr(obter_dados_pf_pj(null,b.cd_cgc,'CI'),1,20),20,' '),
	lpad(substr(obter_dados_pf_pj(null,b.cd_cgc,'CEP'),1,8),8,'0'),
	rpad(substr(obter_dados_pf_pj(null,b.cd_cgc,'UF'),1,2),2,' '),
	nvl(c.ie_digito_conta,'0'),
	nvl(c.ie_data_pagamento,'R')
into	cd_cgc_estab_w,
	cd_convenio_banco_w,
	cd_agencia_estab_w,
	nr_conta_estab_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_remessa_w,
	cd_estabelecimento_w,
	dt_remessa_retorno_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_municipio_w,
	nr_cep_w,
	sg_estado_w,
	ie_digito_estab_w,
	ie_data_pagamento_w
from	banco_estabelecimento c,
	estabelecimento b,
	banco_escritural a
where	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_sequencia		= nr_seq_envio_p;

obter_param_usuario(857,36,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_converte_bloq_w);

ds_conteudo_w	:=	'341' ||
			'0000' ||
			'0' ||
			rpad(' ',6,' ') ||
			'080' ||
			'2' ||
			cd_cgc_estab_w ||
			rpad(' ',20,' ') ||
			cd_agencia_estab_w ||
			' ' ||
			nr_conta_estab_w ||
			' ' ||
			ie_digito_estab_w ||
			nm_empresa_w ||
			rpad('Banco Ita�',30,' ') ||
			rpad(' ',10,' ') ||
			'1' ||
			dt_geracao_w ||
			'000000000' ||
			'00000' ||
			rpad(' ',69,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

open	c01;
loop
fetch	c01 into
	ie_forma_lanc_w,
	ie_tipo_pagamento_w,
	ie_finalidade_w,
	ie_tipo_pagto_w,
	cd_cgc_tit_w;
exit	when c01%notfound;

	/* header de lote */
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
	nr_sequencia_w		:= 0;
	vl_total_w		:= 0;

	if	(ie_tipo_pagamento_w	= 'PC') then
		nr_versao_w	:= '030';
	else
		nr_versao_w	:= '040';
	end if;
	
	if (cd_cgc_estab_w = cd_cgc_tit_w) then
	
		if (ie_forma_lanc_w = '03') then
			ie_forma_lanc_w := '07';
		elsif (ie_forma_lanc_w = '41') then
			ie_forma_lanc_w := '43'; 
		end if;
 
	end if;

	ds_conteudo_w	:=	'341' ||
				lpad(nr_lote_servico_w,4,'0') ||
				'1' ||
				'C' ||
				ie_tipo_pagto_w ||
				ie_forma_lanc_w ||
				nr_versao_w ||
				' ' ||
				'2' ||
				cd_cgc_estab_w ||
				rpad(' ',20,' ') ||
				cd_agencia_estab_w ||
				' ' ||
				nr_conta_estab_w ||
				' ' ||
				ie_digito_estab_w ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				ds_endereco_w ||
				nr_endereco_w ||
				ds_complemento_w ||
				ds_municipio_w ||
				nr_cep_w ||
				substr(sg_estado_w,1,2) ||
				rpad(' ',18,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

	open	c02;
	loop
	fetch	c02 into
		cd_banco_w,
		cd_agencia_bancaria_w,
		nm_pessoa_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_despesa_w,
		nr_inscricao_w,
		nr_conta_w,
		ie_digito_agencia_w,
		cd_agencia_w,
		nr_nosso_numero_w,
		dt_vencimento_w,
		ie_digito_conta_w,
		nr_bloqueto_w,
		vl_juros_w,
		vl_multa_w;
	exit	when c02%notfound;

		/* segmento A */
		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w	:= nvl(qt_registro_w,0) + 1;
		vl_total_w	:= nvl(vl_total_w,0) + (nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0));

		if	(dt_vencimento_w	< sysdate) then
			dt_vencimento_w	:= sysdate;
		end if;

		if	(ie_tipo_pagamento_w	= 'PC') then

			ds_conteudo_w	:=	'341' ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'O' ||
						'000' ||
						rpad(nvl(nr_bloqueto_w,' '),48,' ') ||
						nm_pessoa_w ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						'REA' ||
						'000000000000000' ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0),'9999999999990.00')),15,'0') ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						rpad(' ',3,' ') ||
						'000000000' ||
						rpad(' ',3,' ') ||
						rpad(nr_titulo_w,20,' ') ||
						rpad(' ',21,' ') ||
						nr_nosso_numero_w ||
						rpad(' ',10,' ');

		else

			if	(cd_banco_w in (341,409)) then

				cd_agencia_conta_w	:=	'0' ||
								lpad(substr(cd_agencia_w,1,4),4,'0') ||
								' ' ||
								'000000' ||
								lpad(substr(nr_conta_w,1,6),6,'0') ||
								ie_digito_conta_w;

			else
			
				cd_agencia_conta_w	:=	lpad(substr(cd_agencia_w,1,5),5,'0') ||
								' ' ||
								lpad(substr(nr_conta_w,1,12),12,'0') ||
								ie_digito_conta_w;

			end if;

			ds_conteudo_w	:=	'341' ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'A' ||
						'000' ||
						'000' ||
						lpad(cd_banco_w,3,'0') ||
						cd_agencia_conta_w ||
						nm_pessoa_w ||
						rpad(nr_titulo_w,20,' ') ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						'REA' ||
						'000000000000000' ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') ||
						rpad(' ',20,' ') ||
						'00000000' || /* to_char(dt_remessa_retorno_w,'ddmmyyyy') || */
						'000000000000000' || /* lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_juros_w,0) + nvl(vl_multa_w,0),'9999999999990.00')),15,'0') || */
						rpad(' ',20,' ') ||
						'000000' ||
						nr_inscricao_w ||
						rpad(' ',23,' ');

		end if;

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

	end	loop;
	close	c02;

	/* trailer de lote */
	nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w	:=	'341' ||
				lpad(nr_lote_servico_w,4,'0') ||
				'5' ||
				rpad(' ',9,' ') ||
				lpad(nr_sequencia_w + 2,6,'0') ||
				lpad(somente_numero(to_char(nvl(vl_total_w,0),'9999999999999990.00')),18,'0') ||
				'000000000000000000' ||
				rpad(' ',181,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c01;

open	c03;
loop
fetch	c03 into
	ie_forma_lanc_w,
	ie_tipo_pagamento_w,
	ie_tipo_bloqueto_w,
	ie_tipo_pagto_w;
exit	when c03%notfound;

	ie_forma_lanc_blq_w := ie_forma_lanc_w;
	
	if	(ie_tipo_bloqueto_w	= 'I') and
		(ie_tipo_pagamento_w	<> 'PC') then
		if	(ie_tipo_titulo_w	= '16') then
			ie_forma_lanc_blq_w	:= '35';
		elsif	(ie_forma_lanc_blq_w	<> '91') then
			ie_forma_lanc_blq_w	:= '19';
		end if;
	end if;

	if	(ie_tipo_bloqueto_w	= 'I') then

		ie_tipo_pagto_w	:= '22';

	end if;

	/* header de lote - BLQ Ita� */
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
	nr_sequencia_w		:= 0;
	vl_total_w		:= 0;

	ds_conteudo_w	:=	'341' ||
				lpad(nr_lote_servico_w,4,'0') ||
				'1' ||
				'C' ||
				ie_tipo_pagto_w ||
				ie_forma_lanc_blq_w ||
				'030' ||
				' ' ||
				'2' ||
				cd_cgc_estab_w ||
				rpad(' ',20,' ') ||
				cd_agencia_estab_w ||
				' ' ||
				nr_conta_estab_w ||
				' ' ||
				ie_digito_estab_w ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				ds_endereco_w ||
				nr_endereco_w ||
				ds_complemento_w ||
				ds_municipio_w ||
				nr_cep_w ||
				substr(sg_estado_w,1,2) ||
				rpad(' ',18,' ');

	
	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

	open	c04;
	loop
	fetch	c04 into
		nm_pessoa_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_despesa_w,
		dt_vencimento_w,
		vl_juros_w,
		vl_multa_w,
		nr_bloqueto_w,
		nr_inscricao_w,
		ie_tipo_identificacao_w;
	exit	when c04%notfound;

		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w	:= nvl(qt_registro_w,0) + 1;
		vl_total_w	:= nvl(vl_total_w,0) + (nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_multa_w,0) + nvl(vl_juros_w,0));

		if	(dt_vencimento_w	< sysdate) then
			dt_vencimento_w	:= sysdate;
		end if;

		/* segmento O */
		if	(ie_tipo_bloqueto_w	= 'I') then

			ds_conteudo_w	:=	'341' ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'O' ||
						'000' ||
						rpad(nvl(nr_bloqueto_w,' '),48,' ') ||
						nm_pessoa_w ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						'REA' ||
						'000000000000000' ||
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0),'9999999999990.00')),15,'0') ||
						to_char(dt_vencimento_w,'ddmmyyyy') ||
						lpad(somente_numero(to_char(nvl(vl_multa_w,0) + nvl(vl_juros_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0),'9999999999990.00')),15,'0') ||
						rpad(' ',3,' ') ||
						rpad(' ',9,' ') ||
						rpad(' ',3,' ') ||
						rpad(nr_titulo_w,20,' ') ||
						rpad(' ',21,' ') ||
						rpad(' ',15,' ') ||
						rpad(' ',10,' ');

		/* segmento J */
		else
			if	(nvl(ie_data_pagamento_w,'R')	= 'R') then
				dt_pagamento_w	:= dt_remessa_retorno_w;
			else
				dt_pagamento_w	:= dt_vencimento_w;
			end if;

			ds_conteudo_w	:=	'341' || --001 003
						lpad(nr_lote_servico_w,4,'0') || --004 007
						'3' || --008 008
						lpad(nr_sequencia_w,5,'0') || --009 013
						'J' || --014 014
						'000' || --015 017
						lpad(nvl(substr(nr_bloqueto_w,1,44),'0'),44,'0') || --018 061
						nm_pessoa_w || --062 091
						to_char(dt_vencimento_w,'ddmmyyyy') || --092 099
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0),'9999999999990.00')),15,'0') || --100 114
						lpad(somente_numero(to_char(nvl(vl_desconto_w,0),'9999999999990.00')),15,'0') || --115 129
						lpad(somente_numero(to_char(nvl(vl_multa_w,0) + nvl(vl_juros_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0),'9999999999990.00')),15,'0') || --130 144
						to_char(dt_pagamento_w,'ddmmyyyy') || --145 152
						lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0) + nvl(vl_multa_w,0) + nvl(vl_juros_w,0),'9999999999990.00')),15,'0') || --153 167
						'000000000000000' || --168 182
						rpad(nr_titulo_w,20,' ') || --183 202
						rpad(' ',38,' '); --203 240

		end if;

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

		/* segmento J-52 */
		if	(ie_tipo_bloqueto_w	<> 'I') and
			(ie_forma_lanc_w	= '31') then

			nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
			nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
			qt_registro_w	:= nvl(qt_registro_w,0) + 1;

			ds_conteudo_w	:=	'341' ||
						lpad(nr_lote_servico_w,4,'0') ||
						'3' ||
						lpad(nr_sequencia_w,5,'0') ||
						'J' ||
						' ' ||
						'00' ||
						'52' ||
						'2' ||
						lpad(cd_cgc_estab_w,15,'0') ||
						rpad(nm_empresa_w,40,' ') ||
						substr(ie_tipo_identificacao_w,2,1) ||
						lpad(nr_inscricao_w,15,'0') ||
						rpad(nm_pessoa_w,40,' ') ||
						'2' ||
						lpad(cd_cgc_estab_w,15,'0') ||
						rpad(nm_empresa_w,40,' ') ||
						rpad(' ',52,' ');

			insert	into w_envio_banco
				(cd_estabelecimento,
				ds_conteudo,
				dt_atualizacao,
				nm_usuario,
				nr_seq_apres,
				nr_seq_apres_2,
				nr_sequencia)
			values	(cd_estabelecimento_w,
				ds_conteudo_w,
				sysdate,
				nm_usuario_p,
				nr_seq_apres_w,
				nr_seq_apres_w,
				w_envio_banco_seq.nextval);

		end if;

	end	loop;
	close	c04;

	/* trailer de lote */
	nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w	:=	'341' ||
				lpad(nr_lote_servico_w,4,'0') ||
				'5' ||
				rpad(' ',9,' ') ||
				lpad(nr_sequencia_w + 2,6,'0') ||
				lpad(somente_numero(to_char(nvl(vl_total_w,0),'9999999999999990.00')),18,'0') ||
				'000000000000000000' ||
				rpad(' ',181,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c03;

/* trailer de arquivo */
nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

ds_conteudo_w	:=	'341' ||
			'9999' ||
			'9' ||
			rpad(' ',9,' ') ||
			lpad(nr_lote_servico_w,6,'0') ||
			lpad(nvl(qt_registro_w,0) + (nvl(nr_lote_servico_w,0) * 2) + 2,6,'0') ||	/* registros detalhe + header e trailer dos lotes + header e trailer do arquivo */
			rpad(' ',211,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

commit;

end ASFV_GERAR_PAGTO_ITAU_240_V83;
/