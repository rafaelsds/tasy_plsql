create or replace
procedure odonto_inserir_proced_conta(nr_atendimento_p		number,
					cd_procedimento_p	number,
					ie_origem_proced_p	number,
					nr_seq_proc_interno_p	number default null,
					qt_procedimento_p	number,
					nr_seq_odont_proced_p	number,
					nm_usuario_p		Varchar2,
					ie_plano_tratamento_p	varchar2 default 'N') is 

cd_convenio_w		number(10,0)  	:= 0;
cd_categoria_w		number(10,0)  	:= 0;	
nr_seq_atepacu_w	number(10,0)  	:= 0;
dt_ent_unidade_w	date 		:= sysdate;
cd_local_estoque_w	number(10);
nr_seq_proc_pac_w  	number(10,0)  	:= 0;				
ds_erro_w		varchar2(255);
cd_setor_atendimento_w	number(10);
cd_medico_executor_w	procedimento_paciente.cd_medico_executor%type;
cd_medico_exec_w	procedimento_paciente.cd_medico_executor%type;
ie_tipo_atendimento_w	atendimento_paciente.ie_tipo_atendimento%type;
ie_medico_executor_w	procedimento_paciente.ie_funcao_medico%type;
cd_cgc_prest_regra_w	procedimento_paciente.cd_cgc_prestador%type;
cd_pes_fis_regra_w	procedimento_paciente.cd_pessoa_fisica%type;
nr_seq_classificacao_w	atendimento_paciente.nr_seq_classificacao%type;
cd_profissional_w	odont_procedimento.cd_profissional%type;
nr_interno_conta_w      conta_paciente.nr_interno_conta%type;
cd_estabelecimento_w    conta_paciente.cd_estabelecimento%type;

begin

if (nr_atendimento_p is not null) then

	select 	max(obter_convenio_atendimento(a.nr_atendimento)),
		max(obter_dados_categ_conv(a.nr_atendimento,'CA')),
		max(Obter_Atepacu_paciente(nr_atendimento_p,'A')),
		max(ie_tipo_atendimento),
		max(nr_seq_classificacao)
	into  	cd_convenio_w,
		cd_categoria_w,
		nr_seq_atepacu_w,
		ie_tipo_atendimento_w,
		nr_seq_classificacao_w
	from  	atendimento_paciente a
	where 	a.nr_atendimento = nr_atendimento_p;
		
	select	max(Obter_Dados_AtePacu(nr_seq_atepacu_w,0))
	into	cd_setor_atendimento_w
	from	dual;
	
	select 	max(w.dt_entrada_unidade)
	into	dt_ent_unidade_w
	from   	atend_paciente_unidade w
	where  	w.nr_atendimento 		= nr_atendimento_p
	and    	w.cd_setor_atendimento 		= cd_setor_atendimento_w;
	
	select	max(cd_local_estoque)
	into	cd_local_estoque_w
	from	setor_atendimento
	where	cd_setor_atendimento	= cd_setor_atendimento_w;
	
	consiste_medico_executor
			(wheb_usuario_pck.get_cd_estabelecimento,
			cd_convenio_w,
			cd_setor_atendimento_w,
			cd_procedimento_p,
			ie_origem_proced_p,
			ie_tipo_atendimento_w,
			null,
			nr_seq_proc_interno_p,
			ie_medico_executor_w,
			cd_cgc_prest_regra_w,		
			cd_medico_executor_w,
			cd_pes_fis_regra_w,
			null,
			sysdate,
			nr_seq_classificacao_w,
			'N',
			null,
			null);
			
	if (ie_plano_tratamento_p = 'N') then
		select	max(cd_profissional)
		into	cd_medico_exec_w
		from	odont_procedimento
		where	nr_sequencia = nr_seq_odont_proced_p;
	else
		select	max(cd_profissional)
		into	cd_medico_exec_w
		from	odont_procedimento_proc
		where	nr_sequencia = nr_seq_odont_proced_p;
	end if;

	if	(cd_medico_exec_w is null) then
		cd_medico_exec_w := cd_medico_executor_w;
	end if;
	
	select 	procedimento_paciente_seq.NEXTVAL
	into  	nr_seq_proc_pac_w
	from  	dual;

	insert into procedimento_paciente (nr_sequencia,
						nr_atendimento,
						dt_entrada_unidade,
						cd_procedimento,
						dt_procedimento,
						qt_procedimento,
						dt_atualizacao,
						nm_usuario,
						cd_setor_atendimento,
						ie_origem_proced,
						nr_seq_atepacu,
						nr_seq_proc_interno,
						cd_convenio,
						cd_categoria,
						cd_pessoa_fisica,
						cd_medico_executor,
						cd_cgc_prestador)
				values  (nr_seq_proc_pac_w,
						nr_atendimento_p,
						nvl(dt_ent_unidade_w,sysdate),
						cd_procedimento_p,
						sysdate,
						qt_procedimento_p,
						sysdate,
						nm_usuario_p,
						cd_setor_atendimento_w,
						ie_origem_proced_p,
						nr_seq_atepacu_w,
						nr_seq_proc_interno_p,
						cd_convenio_w,
						cd_categoria_w,
						cd_pes_fis_regra_w,
						cd_medico_exec_w,
						cd_cgc_prest_regra_w);

	consiste_exec_procedimento(nr_seq_proc_pac_w, ds_erro_w);
	atualiza_preco_procedimento(nr_seq_proc_pac_w, cd_convenio_w, nm_usuario_p);
	gerar_lancamento_automatico(nr_atendimento_p,cd_local_estoque_w,34,nm_usuario_p,nr_seq_proc_pac_w,null,null,null,null,null);

    select a.nr_interno_conta,
        a.cd_estabelecimento
    into nr_interno_conta_w,
        cd_estabelecimento_w
    from conta_paciente a,
        procedimento_paciente b
    where b.nr_sequencia = nr_seq_proc_pac_w
    and b.nr_interno_conta = a.nr_interno_conta;

    tiss_atualizar_conta_paciente(nr_interno_conta_w, null, 'S', cd_estabelecimento_w, nm_usuario_p, null, null);
	
	if	(ie_plano_tratamento_p = 'N') then	
		update	odont_procedimento		
		set	nr_seq_propaci = nr_seq_proc_pac_w
		where	nr_sequencia = nr_seq_odont_proced_p;
	
	else 
		update 	odont_procedimento_proc
		set	nr_seq_propaci = nr_seq_proc_pac_w
		where	nr_sequencia = nr_seq_odont_proced_p;
	
	end if;

end if;

commit;

end odonto_inserir_proced_conta;
/
