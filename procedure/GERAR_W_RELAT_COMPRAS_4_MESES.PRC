create or replace
procedure gerar_w_relat_compras_4_meses(dt_referencia_p		date,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

dt_entrada_saida_w	date;
cd_material_w		number(6);
vl_total_item_nf_w	number(13,4)	:= 0;
qt_item_nf_w		number(13,4)	:= 0;
qt_mes_w		number(10) 	:= 0;
qt_notas_w		number(10) 	:= 0;
vl_unitario_item_w	number(13,4)	:= 0;
vl_media_unitario_w	number(13,4)	:= 0;

cursor c01 is
select	distinct
	PKG_DATE_UTILS.start_of(a.dt_entrada_saida,'month',0) dt_entrada_saida
from	nota_fiscal a,
	nota_fiscal_item b,
	estrutura_material_v e	
where	(a.nr_sequencia = b.nr_sequencia)
and	(b.cd_material = e.cd_material)
and	(a.dt_atualizacao_estoque is not null)
and	(a.ie_tipo_nota in ('ES','EN'))
and	(a.ie_situacao = '1')
and	(a.dt_atualizacao_estoque is not null)
and	(a.cd_estabelecimento = cd_estabelecimento_p)
and	(b.cd_material is not null)
and	(PKG_DATE_UTILS.start_of(a.dt_entrada_saida,'month',0) between PKG_DATE_UTILS.ADD_MONTH(dt_referencia_p,-4,0) and dt_referencia_p)
order by 1 desc;

cursor c02 is
select	distinct
	b.cd_material,
	sum(vl_total_item_nf),
	sum(qt_item_nf)
from	nota_fiscal a,
	nota_fiscal_item b
where	(a.nr_sequencia = b.nr_sequencia)
and	(a.ie_tipo_nota in ('ES','EN'))
and	(a.ie_situacao = '1')
and	(a.dt_atualizacao_estoque is not null)
and	(a.cd_estabelecimento = cd_estabelecimento_p)
and	(b.cd_material is not null)
and	(PKG_DATE_UTILS.start_of(a.dt_entrada_saida,'month',0) = dt_entrada_saida_w)
group by b.cd_material
order by 1 desc;

begin

delete from w_relat_compra_ult_meses;

open C01;
loop
fetch C01 into	
	dt_entrada_saida_w;
exit when C01%notfound;
	begin
	qt_mes_w := qt_mes_w + 1;
	open C02;
	loop
	fetch C02 into	
		cd_material_w,
		vl_total_item_nf_w,
		qt_item_nf_w;
	exit when C02%notfound;
		begin
		
		select	count(distinct b.nr_sequencia),
			sum(b.vl_unitario_item_nf)
		into	qt_notas_w,
			vl_unitario_item_w
		from	nota_fiscal a,
			nota_fiscal_item b
		where	(a.nr_sequencia = b.nr_sequencia)
		and	(a.ie_tipo_nota in ('ES','EN'))
		and	(a.ie_situacao = '1')
		and	(a.dt_atualizacao_estoque is not null)
		and	(a.cd_estabelecimento = cd_estabelecimento_p)
		and	(b.cd_material is not null)
		and	(PKG_DATE_UTILS.start_of(a.dt_entrada_saida,'month',0) = dt_entrada_saida_w)
		and	(b.cd_material = cd_material_w);	

		vl_media_unitario_w := dividir(vl_unitario_item_w , qt_notas_w);
		
		insert into  w_relat_compra_ult_meses(
			dt_atualizacao,
			nm_usuario,
			qt_item_nf,
			vl_total_item_nf,
			dt_referencia,
			qt_mes,
			cd_material,
			vl_media_mes)
		values	(sysdate,
			nm_usuario_p,
			qt_item_nf_w,
			vl_total_item_nf_w,
			dt_referencia_p,
			qt_mes_w,
			cd_material_w,
			vl_media_unitario_w);
		end;
	end loop;
	close C02;	
	end;
end loop;
close C01;
commit;
end gerar_w_relat_compras_4_meses;
/