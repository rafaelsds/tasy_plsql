create or replace
procedure ctb_gerar_orc_detalhe(	nr_seq_cenario_p		number,
				nr_seq_orcamento_p	number,
				nm_usuario_p		Varchar2) is 

cd_centro_custo_w			number(10);
cd_conta_contabil_w		varchar2(20);
cd_empresa_w			number(10);
cd_estabelecimento_w		number(10);
cd_pessoa_fisica_w		varchar2(14);
cd_cnpj_w			varchar2(14);
ds_detalhe_w			varchar2(255);
ds_observacao_w			varchar2(4000);
dt_inicial_w			date;
dt_final_w			date;
dt_referencia_w			date;
nr_mes_w			number(10);
nr_seq_mes_ref_w			number(10);
vl_orcado_w			number(15,2);

cursor c02 is
select	a.cd_pessoa_fisica,
	a.cd_cnpj,
	a.ds_detalhe,
	a.ds_observacao,
	decode(nr_mes_w,1, nvl(sum(a.vl_orcado_1),0),
			2, nvl(sum(a.vl_orcado_2),0),
			3, nvl(sum(a.vl_orcado_3),0),
			4, nvl(sum(a.vl_orcado_4),0),
			5, nvl(sum(a.vl_orcado_5),0),
			6, nvl(sum(a.vl_orcado_6),0),
			7, nvl(sum(a.vl_orcado_7),0),
			8, nvl(sum(a.vl_orcado_8),0),
			9, nvl(sum(a.vl_orcado_9),0),
			10, nvl(sum(a.vl_orcado_10),0),
			11, nvl(sum(a.vl_orcado_11),0),
			12, nvl(sum(a.vl_orcado_12),0)) vl_orcado
from	ctb_orc_cen_valor_linear a
where	a.nr_seq_cenario	= nr_seq_cenario_p
and	a.cd_estabelecimento	= cd_estabelecimento_w
and	a.cd_centro_custo	= cd_centro_custo_w
and	a.cd_conta_contabil	= cd_conta_contabil_w
group by a.cd_pessoa_fisica,
	a.cd_cnpj,
	a.ds_detalhe,
	a.ds_observacao;

vet02	C02%RowType;

begin

select	nr_seq_mes_ref,
	cd_conta_contabil,
	cd_centro_custo,
	cd_estabelecimento
into	nr_seq_mes_ref_w,
	cd_conta_contabil_w,
	cd_centro_custo_w,
	cd_estabelecimento_w
from	ctb_orcamento
where	nr_sequencia	= nr_seq_orcamento_p;


select	dt_referencia
into	dt_referencia_w
from	ctb_mes_ref
where	nr_sequencia	= nr_seq_mes_ref_w;

select	cd_empresa,
	ctb_obter_mes_ref(nr_seq_mes_inicio),
	ctb_obter_mes_ref(nr_seq_mes_fim)
into	cd_empresa_w,
	dt_inicial_w,
	dt_final_w
from	ctb_orc_cenario
where	nr_sequencia	= nr_seq_cenario_p;

delete	from ctb_orcamento_detalhe
where	nr_seq_orcamento	= nr_seq_orcamento_p
and	vl_realizado	= 0;

nr_mes_w	:= nvl(trunc(months_between(dt_referencia_w,dt_inicial_w))+1,1);



open C02;
loop
fetch C02 into	
	cd_pessoa_fisica_w,
	cd_cnpj_w,
	ds_detalhe_w,
	ds_observacao_w,
	vl_orcado_w;
exit when C02%notfound;
	begin
	
	if	(vl_orcado_w <> 0) then
		insert into ctb_orcamento_detalhe(
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_orcamento,
			cd_pessoa_fisica,
			cd_cnpj,
			ds_detalhe,
			ds_observacao,
			vl_orcado,
			vl_original,
			vl_realizado)
		values(	ctb_orcamento_detalhe_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_orcamento_p,
			cd_pessoa_fisica_w,
			cd_cnpj_w,
			ds_detalhe_w,
			ds_observacao_w,
			vl_orcado_w,
			0,
			0);
		
	end if;
	
	end;
end loop;
close C02;

commit;

end ctb_gerar_orc_detalhe;
/