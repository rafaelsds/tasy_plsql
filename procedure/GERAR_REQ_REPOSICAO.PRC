create or replace
procedure gerar_req_reposicao(
			nr_ordem_compra_p	number,
			cd_local_destino_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2,
			nr_requisicao_p		out number) is

/* Procedure utilizada no CorEtqTR */

/* Cursor c01 */
nr_ordem_compra_w		ordem_compra.nr_ordem_compra%type;
nr_item_oci_w			ordem_compra_item.nr_item_oci%type;
cd_material_w			material.cd_material%type;
qt_material_w			number(15,4);
qt_estoque_w			number(15,4);

cd_grupo_material_w		grupo_material.cd_grupo_material%type;
cd_subgrupo_w			subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w		classe_material.cd_classe_material%type;

/* Cursor c02 */
cd_local_estoque_w		local_estoque.cd_local_estoque%type;
cd_operacao_estoque_w		operacao_estoque.cd_operacao_estoque%type;

cd_local_estoque_ant_w		local_estoque.cd_local_estoque%type;
cd_operacao_estoque_ant_w	operacao_estoque.cd_operacao_estoque%type;
cd_pessoa_requisicao_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_requisicao_w			requisicao_material.nr_requisicao%type;
nr_sequencia_w			item_requisicao_material.nr_sequencia%type;
ds_erro_w			varchar2(2000);
dt_liberacao_w			date;
qt_conv_estoque_cons_w		number(15,4);
cd_unidade_medida_w		unidade_medida.cd_unidade_medida%type;
cd_unidade_medida_estoque_w	unidade_medida.cd_unidade_medida%type;

cursor c01 is
select	a.nr_ordem_compra,
	b.nr_item_oci,
	b.cd_material,
	obter_quantidade_convertida(b.cd_material, sum(x.qt_prevista_entrega) - (obter_qt_oci_trans_nota(a.nr_ordem_compra, b.nr_item_oci,'S')), b.cd_unidade_medida_compra, 'UME')
from	ordem_compra_item b,
	ordem_compra a,
	ordem_compra_item_entrega x
where	a.nr_ordem_compra = b.nr_ordem_compra
and	x.nr_item_oci = b.nr_item_oci
and	x.nr_ordem_compra = b.nr_ordem_compra
and	x.dt_cancelamento is null
AND	a.ie_tipo_ordem = 'T'
and	a.cd_estab_transf = cd_estabelecimento_p
and	a.nr_ordem_compra = nr_ordem_compra_p
having sum(x.qt_prevista_entrega) - (obter_qt_oci_trans_nota(a.nr_ordem_compra, b.nr_item_oci,'S')) > 0
group by a.nr_ordem_compra,
	b.nr_item_oci,
	b.cd_material,
	b.ds_material_direto,
	b.nr_solic_compra,
	b.cd_unidade_medida_compra;

cursor c02 is
select	cd_local_estoque,
	cd_operacao_estoque
from	regra_req_transf_estab
where	cd_estabelecimento					= cd_estabelecimento_p
and	cd_local_estoque_destino				= cd_local_destino_p
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_w)		= cd_subgrupo_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and	nvl(cd_material, cd_material_w)				= cd_material_w
and	((nr_seq_estrut_int is null) or
	(nr_seq_estrut_int is not null and consistir_se_mat_contr_estrut(nr_seq_estrut_int,cd_material_w) = 'S'));

cursor c03 is
select	nr_ordem_compra,
	nr_item_oci,
	cd_material,
	qt_material,
	cd_local_estoque,
	cd_operacao_estoque
from	w_itens_req_reposicao
where	nm_usuario = nm_usuario_p
group by nr_ordem_compra,
	nr_item_oci,
	cd_material,
	qt_material,
	cd_local_estoque,
	cd_operacao_estoque;

cursor c04 is
select	nvl(substr(wheb_mensagem_pck.get_texto(314232) || cd_material || ' - ' || ds_consistencia || '.',1,2000),null)
from	requisicao_mat_consist
where	nr_requisicao = nr_requisicao_w;

begin
select	nvl(max(nr_requisicao),0)
into	nr_requisicao_w
from	item_requisicao_material
where	nr_ordem_compra = nr_ordem_compra_p;

if	(nr_requisicao_w > 0) then
	/* J� existe a requisi��o ' || nr_requisicao_w || ' gerada para essa transfer�ncia! */
	wheb_mensagem_pck.exibir_mensagem_abort(323103,'NR_REQUISICAO='||nr_requisicao_w);
end if;

delete	w_itens_req_reposicao
where	nm_usuario = nm_usuario_p;
commit;

open c01;
loop
fetch c01 into
	nr_ordem_compra_w,
	nr_item_oci_w,
	cd_material_w,
	qt_material_w;
exit when c01%notfound;
	begin
	select	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material
	into	cd_grupo_material_w,
		cd_subgrupo_w,
		cd_classe_material_w
	from	estrutura_material_v
	where 	cd_material = cd_material_w;
	
	cd_local_estoque_w	:= 0;
	cd_operacao_estoque_w	:= 0;
	
	open c02;
	loop
	fetch c02 into
		cd_local_estoque_w,
		cd_operacao_estoque_w;
	exit when c02%notfound;
		begin
		cd_local_estoque_w	:= cd_local_estoque_w;
		cd_operacao_estoque_w	:= cd_operacao_estoque_w;
		end;
	end loop;
	close c02;
	
	if	(nvl(cd_local_estoque_w,0) > 0) and (nvl(cd_operacao_estoque_w,0) > 0) then
		insert into w_itens_req_reposicao(
			nr_ordem_compra,
			nr_item_oci,
			cd_material,
			qt_material,
			cd_local_estoque,
			cd_operacao_estoque,
			nm_usuario) 
		values(	nr_ordem_compra_w,
			nr_item_oci_w,
			cd_material_w,
			qt_material_w,
			cd_local_estoque_w,
			cd_operacao_estoque_w,
			nm_usuario_p);
	end if;
	end;
end loop;
close c01;

cd_pessoa_requisicao_w	:= obter_pf_usuario(nm_usuario_p,'C');
nr_requisicao_w		:= 0;

open c03;
loop
fetch c03 into
	nr_ordem_compra_w,
	nr_item_oci_w,
	cd_material_w,
	qt_material_w,
	cd_local_estoque_w,
	cd_operacao_estoque_w;
exit when c03%notfound;
	begin
	if	(nr_requisicao_w = 0) or
		(cd_local_estoque_w <> cd_local_estoque_ant_w) or
		(cd_operacao_estoque_w <> cd_operacao_estoque_ant_w) then
		select	requisicao_seq.nextval
		into	nr_requisicao_w
		from	dual;
		
		insert into requisicao_material(
			nr_requisicao,
			cd_estabelecimento,
			cd_local_estoque,
			dt_solicitacao_requisicao,
			dt_atualizacao,
			nm_usuario,
			cd_operacao_estoque,
			cd_pessoa_requisitante,
			cd_local_estoque_destino,
			nm_usuario_lib,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_geracao,
			ie_urgente)
		values(	nr_requisicao_w,
			cd_estabelecimento_p,
			cd_local_estoque_w,
			sysdate,
			sysdate,
			nm_usuario_p,
			cd_operacao_estoque_w,
			cd_pessoa_requisicao_w,
			cd_local_destino_p,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'I',
			'N');
		
		cd_local_estoque_ant_w		:= cd_local_estoque_w;
		cd_operacao_estoque_ant_w	:= cd_operacao_estoque_w;
	end if;
	
	if	(nr_requisicao_w > 0) then
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_sequencia_w
		from	item_requisicao_material
		where	nr_requisicao = nr_requisicao_w;
		
		cd_unidade_medida_w 		:= obter_dados_material_estab(cd_material_w,cd_estabelecimento_p,'UMS');
		cd_unidade_medida_estoque_w 	:= obter_dados_material_estab(cd_material_w,cd_estabelecimento_p,'UME');
		qt_estoque_w			:= qt_material_w;
		qt_material_w			:= obter_quantidade_convertida(cd_material_w, qt_estoque_w, cd_unidade_medida_estoque_w, 'UMC');
		
		begin
		qt_conv_estoque_cons_w 		:= qt_material_w / qt_estoque_w;
		exception
		when others then
			qt_conv_estoque_cons_w 		:= obter_conversao_material(cd_material_w,'CE');
		end;
		
		insert into item_requisicao_material(
			nr_requisicao,
			nr_sequencia,
			cd_estabelecimento,
			cd_material,
			qt_material_requisitada,
			qt_estoque,
			vl_material,
			dt_atualizacao,
			nm_usuario,
			cd_unidade_medida,
			cd_unidade_medida_estoque,
			nr_ordem_compra,
			nr_item_oci)
		values( nr_requisicao_w,
			nr_sequencia_w,
			cd_estabelecimento_p,
			cd_material_w,
			qt_material_w,
			qt_estoque_w,
			0,
			sysdate,
			nm_usuario_p,
			cd_unidade_medida_w,
			cd_unidade_medida_estoque_w,
			nr_ordem_compra_w,
			nr_item_oci_w);
	end if;
	end;
end loop;
close c03;

commit;

if	(nr_requisicao_w > 0) then
	consistir_requisicao(nr_requisicao_w,nm_usuario_p,cd_local_destino_p,null,'N','N','N','N','S','N','N',cd_operacao_estoque_w,ds_erro_w);
	commit;
end if;

nr_requisicao_p := nr_requisicao_w;

end gerar_req_reposicao;
/