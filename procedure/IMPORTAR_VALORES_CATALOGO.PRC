create or replace
procedure importar_valores_catalogo(
			ds_endereco_p			varchar2,
			cd_postal_p				varchar2,
			cd_endereco_catalogo_p	varchar2,
			cd_sistema_anterior_p	varchar2,
			ds_abreviacao_p			varchar2,
			cd_endereco_superior_p	varchar2,
			nr_seq_catalogo_p		number,
			cd_tipo_lograd_p		varchar2,
			cd_tipo_bairro_p		varchar2,
	                cd_gkz_p           varchar2 default null,
                      cd_gcd_p           varchar2 default null,
	                ds_additional_info_p varchar2 default null,
                      ds_extra_info_p varchar2 default null,
			nm_usuario_p			varchar2) is 

qt_registros_w			number(10);
cd_endereco_catalogo_w	end_endereco.cd_endereco_catalogo%type;
cd_endereco_catalogo_ww end_endereco.cd_endereco_catalogo%type;
cd_postal_w end_endereco.cd_postal%type;
ds_endereco_w end_endereco.ds_endereco%type;
cd_gkz_w postal_community_code.cd_geminde_kennziffer%type;
cd_gcd_w postal_community_code.cd_gemeinde_code%type;
ds_abreviacao_w end_endereco.ds_abreviacao%type;
cd_tipo_lograd_w w_carga_end_endereco.cd_tipo_lograd%type;
ds_additional_info_w w_carga_end_endereco.ds_additional_info%type;
ds_extra_info_w w_carga_end_endereco.ds_extra_info%type;
qt_cd_endereco_catalogo_w			number(10);

begin

	if (ds_endereco_p is not null) then


	begin
		cd_endereco_catalogo_w	:= cd_endereco_catalogo_p;
		if	(substr(trim(cd_endereco_catalogo_w),1,2) = '#@') then
			cd_endereco_catalogo_w	:= trim(replace(cd_endereco_catalogo_w,'#@',''));
		end if;
		if	(substr(trim(cd_endereco_catalogo_w),1,1) = ';') then
			cd_endereco_catalogo_w	:= trim(replace(cd_endereco_catalogo_w,';',''));
		end if;
	
		select 	count(*)
		into	qt_registros_w
		from	w_carga_end_catalogo
		where	nm_usuario = nm_usuario_p;
		
		if (nvl(qt_registros_w, 0) = 0) then
				
			insert into w_carga_end_catalogo(
				dt_atualizacao,
				nm_usuario,
				ie_status_carga,
				nr_seq_catalogo)
			values 
				(sysdate,
				nm_usuario_p,
				'P',
				nr_seq_catalogo_p);
			
			commit;
		
		
		end if;
		cd_postal_w	:= trim(replace(cd_postal_p,'"',''));
              cd_endereco_catalogo_ww	:= trim(replace(cd_endereco_catalogo_w,'"',''));
              ds_endereco_w	:= trim(replace(ds_endereco_p,'"',''));
              cd_gkz_w := trim(replace(cd_gkz_p,'"',''));
              cd_gcd_w := trim(replace(cd_gcd_p,'"',''));
              ds_abreviacao_w := trim(replace(ds_abreviacao_p,'"',''));
              cd_tipo_lograd_w := trim(replace(cd_tipo_lograd_p,'"',''));
	        ds_additional_info_w :=trim(replace(ds_additional_info_p,'"',''));
              ds_additional_info_w :=trim(replace(ds_additional_info_w,'#@',','));
              ds_additional_info_w :=trim(replace(ds_additional_info_w,';',','));
              ds_additional_info_w :=trim(replace(ds_additional_info_w,',,',','));
              ds_extra_info_w :=trim(replace(ds_extra_info_p,'"',''));
              ds_extra_info_w :=trim(replace(ds_extra_info_w,'#@',','));
              ds_extra_info_w :=trim(replace(ds_extra_info_w,';',''));
              ds_extra_info_w :=trim(replace(ds_extra_info_w,',,',','));
              ds_extra_info_w :=trim(replace(ds_extra_info_w,',,',','));
						
	        insert into w_carga_end_endereco(
			dt_atualizacao,
			nm_usuario,
			ds_endereco,
			cd_postal,
			cd_endereco_catalogo,
			cd_sistema_anterior,
			ds_abreviacao,
			cd_endereco_superior,
			cd_tipo_bairro,
			cd_tipo_lograd,
		        cd_gkz,
                      cd_gcd,
                      ds_additional_info,
                      ds_extra_info)
		values 
			(sysdate,
			nm_usuario_p,
			ds_endereco_w,
			cd_postal_w,
			cd_endereco_catalogo_ww,
			cd_sistema_anterior_p,
			ds_abreviacao_w,
			cd_endereco_superior_p,
			cd_tipo_bairro_p,
			cd_tipo_lograd_w,
			cd_gkz_w,
                      cd_gcd_w,
                      ds_additional_info_w,
                      ds_extra_info_w);
		exception
			when DUP_VAL_ON_INDEX then
				null;
			when others then
				wheb_mensagem_pck.exibir_mensagem_abort(189708);
	end;
		
	end if;

end importar_valores_catalogo;
/
