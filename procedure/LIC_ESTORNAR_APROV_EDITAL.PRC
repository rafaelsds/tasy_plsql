create or replace
procedure lic_estornar_aprov_edital(	nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 

nr_seq_licitacao_w			number(10);
ie_existe_lance_w			number(10);
			
begin

select	nr_seq_licitacao
into	nr_seq_licitacao_w
from	reg_lic_edital
where	nr_sequencia	= nr_sequencia_p;

select	count(*)
into	ie_existe_lance_w
from	reg_lic_item ri, reg_lic_item_fornec rf
where	ri.nr_seq_licitacao = rf.nr_seq_licitacao
and ri.NR_SEQ_LIC_ITEM = rf.NR_SEQ_LIC_ITEM
and	nvl(ri.ie_lote,'N') = 'S'
and	ri.nr_seq_licitacao = nr_seq_licitacao_w;

if (ie_existe_lance_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(901261);
end if;

select	count(*)
into	ie_existe_lance_w
from	reg_lic_item ri, reg_lic_vencedor rv
where	ri.nr_seq_licitacao = rv.nr_seq_licitacao
and ri.NR_SEQ_LIC_ITEM = rv.NR_SEQ_LIC_ITEM
and	nvl(ri.ie_lote,'N') = 'S'
and	ri.nr_seq_licitacao = nr_seq_licitacao_w;

if (ie_existe_lance_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(901327);
end if;

update	reg_lic_edital
set	dt_aprovacao_edital	= '',
	nm_usuario_aprov		= ''
where	nr_sequencia 		= nr_sequencia_p;

insert into reg_lic_historico(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ie_tipo_historico,
	ds_observacao,
	nr_seq_licitacao)
values(	reg_lic_historico_seq.nextval,
	sysdate,
	nm_usuario_p,
	'EA',
	wheb_mensagem_pck.get_Texto(802132, 'NR_SEQUENCIA_P='|| NR_SEQUENCIA_P),
	nr_seq_licitacao_w);

update	reg_lic_item
set	nr_seq_lote = null
where	nr_seq_licitacao = nr_seq_licitacao_w;

delete from reg_lic_item
where	nr_seq_licitacao = nr_seq_licitacao_w
and	nvl(ie_lote,'N') = 'S';
	
	
commit;

end lic_estornar_aprov_edital;
/