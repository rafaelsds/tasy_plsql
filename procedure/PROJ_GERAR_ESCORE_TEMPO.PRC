create or replace
procedure proj_gerar_escore_tempo
			(	nr_seq_item_p	Number,
				nr_seq_proj_p	Number,
				qt_escore_p		Number,
				nm_usuario_p	Varchar2) is 

qt_ponto_w			Number(15,0);
qt_peso_w			Number(15,0);
qt_escore_real_w	Number(15,0);
pr_escore_real_w	Number(15,0);

begin

select	count(*)
into	qt_peso_w
from	proj_cron_etapa b,
		proj_cronograma a
where	a.nr_seq_proj = nr_seq_proj_p
and		a.nr_sequencia = b.nr_seq_cronograma
and		b.nr_seq_etapa is not null
and		nvl(b.ie_situacao,'A') = 'A'
and		nvl(a.ie_situacao, 'A') = 'A';

select	count(*)
into	qt_ponto_w
from	proj_cron_etapa b,
		proj_cronograma a
where	a.nr_seq_proj = nr_seq_proj_p
and		a.nr_sequencia = b.nr_seq_cronograma
and		(nvl(b.qt_hora_real,0) <= b.qt_hora_prev)
and		b.nr_seq_etapa is not null
and		nvl(b.ie_situacao,'A') = 'A'
and		nvl(a.ie_situacao, 'A') = 'A';


if	(qt_peso_w >0)  then
	qt_escore_real_w	:= qt_escore_p * (qt_ponto_w / qt_peso_w);
	pr_escore_real_w	:= dividir(qt_escore_real_w * 100, qt_escore_p);
else
	qt_escore_real_w	:= qt_escore_p;
	pr_escore_real_w	:= 100;
end if;

update	proj_escore_item
set		qt_nota_maxima	= qt_peso_w,
		qt_nota			= qt_ponto_w,
		qt_escore_real	= qt_escore_real_w,
		pr_escore_real	= pr_escore_real_w
where	nr_sequencia	= nr_seq_item_p;

end proj_gerar_escore_tempo;
/
