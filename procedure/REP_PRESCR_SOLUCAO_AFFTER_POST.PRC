create or replace
procedure rep_prescr_solucao_affter_post( 	nr_prescricao_p		number,
						nr_seq_solucao_p 	number,
						ie_esquema_alternado_p 	varchar2,
						nm_usuario_p		varchar2) is 

qtd_w	number(10);

begin

reordenar_solucoes(nr_prescricao_p);

--verificar se possui registros
select 	count(*)
into	qtd_w
from	prescr_solucao
where 	nr_prescricao	= nr_prescricao_p
and	nr_seq_solucao	= nr_seq_solucao_p;

if ( qtd_w > 0)	then
	ajustar_prescr_mat_solucao(nr_prescricao_p, nr_seq_solucao_p);
end if;

if ('S' = ie_esquema_alternado_p)	then
	calcular_etapas_esquema(nr_prescricao_p, nr_seq_solucao_p);
end if;
            
commit;

end rep_prescr_solucao_affter_post;
/ 