create or replace
procedure man_gerar_registro_aval_os(
				nr_seq_avaliacao_p	number,
				nm_usuario_p		varchar2) is
			
cd_paciente_w			varchar2(10);
cd_avaliador_w			varchar2(10);
nr_seq_tipo_aval_w			number(10);
nr_seq_aval_paciente_w		number(10);

begin

if	(nvl(nr_seq_avaliacao_p,0) <> 0) then
	begin
	select	a.cd_pessoa_solicitante,
		substr(obter_pf_usuario(b.nm_usuario,'C'),1,10),
		b.nr_seq_tipo_avaliacao
	into	cd_paciente_w,
		cd_avaliador_w,
		nr_seq_tipo_aval_w
	from	man_ordem_serv_avaliacao b,
		man_ordem_servico a
	where	a.nr_sequencia = b.nr_seq_ordem_servico
	and	b.nr_sequencia = nr_seq_avaliacao_p;
	
	select	med_avaliacao_paciente_seq.nextval
	into	nr_seq_aval_paciente_w
	from	dual;
	
	insert into med_avaliacao_paciente(
			nr_sequencia,
			cd_pessoa_fisica,
			cd_medico,
			dt_avaliacao,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_situacao,
			cd_estabelecimento,
			cd_perfil_ativo,
			ie_avaliador_aux,
			ie_avaliacao_parcial,
			ie_apresentar_web,
			ie_restricao_visualizacao,
			nr_seq_tipo_avaliacao)
		values(	nr_seq_aval_paciente_w,
			cd_paciente_w,
			cd_avaliador_w,
			sysdate,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'A',
			wheb_usuario_pck.get_cd_estabelecimento,
			obter_perfil_ativo,
			'N',
			'N',
			'N',
			'T',
			nr_seq_tipo_aval_w);			
			
	insert	into med_avaliacao_result(
			nr_seq_avaliacao,
			nr_seq_item,
			dt_atualizacao,
			nm_usuario,
			qt_resultado,
			ds_resultado)
		select	nr_seq_aval_paciente_w,
			b.nr_seq_item,
			sysdate,
			nm_usuario_p,
			b.qt_resultado,
			b.ds_resultado
		from	man_ordem_serv_aval_result b,
			man_ordem_serv_avaliacao a
		where	a.nr_sequencia = b.nr_seq_ordem_serv_aval
		and	a.nr_sequencia = nr_seq_avaliacao_p;

	commit;
	end;
end if;

end man_gerar_registro_aval_os;
/