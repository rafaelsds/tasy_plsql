create or replace
procedure copiar_dados_compl_estab(
			cd_estabelecimento_p number,
			cd_cgc_p varchar2,
			cd_estab_destino_p	number,
			nm_usuario_p		varchar2) is 
			
ie_existe_pj_w		varchar(1);

nr_sequencia_w			number;
CD_CGC_w			varchar2(255);
CD_ESTABELECIMENTO_w		number;
NM_PESSOA_CONTATO_w		varchar2(255);
NR_RAMAL_CONTATO_w		varchar2(255);
DS_EMAIL_w			varchar2(255);
CD_COND_PAGTO_w			number;
QT_DIA_PRAZO_ENTREGA_w		number;
VL_MINIMO_NF_w			number;
IE_QUALIDADE_w			varchar2(255);
CD_REFERENCIA_FORNEC_w		number;
IE_FORMA_PAGTO_w		varchar2(255);
IE_CONTA_DIF_NF_w		varchar2(255);
CD_INTERNO_LAB_w		number;
CD_INTERF_LAB_LOTE_ENVIO_w	number;
CD_INTERF_LAB_LOTE_RET_w	number;
NR_SEQ_XML_LAB_ENVIO_w		number;
NR_SEQ_XML_LAB_RET_w		number;
CD_PORTADOR_w			number;
CD_TIPO_PORTADOR_w		number;
CD_TIPO_BAIXA_w			number;
NR_CODIGO_SERV_PREST_w		number;
TX_DESC_ANTECIPACAO_w		number;
CD_INTERF_PLS_MENSALIDADE_w	number;
IE_RATEIO_ADIANT_w		varchar2(255);
IE_RETEM_ISS_w			varchar2(255);
TX_DESC_ORDEM_w			number;
IE_ENDERECO_CORRESPONDENCIA_w	varchar2(255);
pR_DESC_FINANCEIRO_w		number;
CD_SISTEMA_ANT_w		number;
IE_FRETE_w			varchar2(255);
IE_EXIGE_ANEXO_NF_w		varchar2(255);
IE_BAIXA_ABAT_w			varchar2(255);
			
begin


select 	count(*)
into	ie_existe_pj_w
from 	pessoa_juridica_estab
where 	cd_cgc = cd_cgc_p
and 	cd_estabelecimento = cd_estab_destino_p;


if (ie_existe_pj_w = 0) then
	begin
	insert into pessoa_juridica_estab (NR_SEQUENCIA,
						CD_CGC,
						CD_ESTABELECIMENTO,
						DT_ATUALIZACAO,
						NM_USUARIO,
						NM_PESSOA_CONTATO,
						NR_RAMAL_CONTATO,
						DS_EMAIL,
						CD_COND_PAGTO,
						QT_DIA_PRAZO_ENTREGA,
						VL_MINIMO_NF,
						IE_QUALIDADE,
						CD_REFERENCIA_FORNEC,
						IE_FORMA_PAGTO,
						DT_ATUALIZACAO_NREC,
						NM_USUARIO_NREC,
						IE_CONTA_DIF_NF,
						CD_INTERNO_LAB,
						CD_INTERF_LAB_LOTE_ENVIO,
						CD_INTERF_LAB_LOTE_RET,
						NR_SEQ_XML_LAB_ENVIO,
						NR_SEQ_XML_LAB_RET,
						CD_PORTADOR,
						CD_TIPO_PORTADOR,
						CD_TIPO_BAIXA,
						NR_CODIGO_SERV_PREST,
						TX_DESC_ANTECIPACAO,
						CD_INTERF_PLS_MENSALIDADE,
						IE_RATEIO_ADIANT,
						IE_RETEM_ISS,
						TX_DESC_ORDEM,
						IE_ENDERECO_CORRESPONDENCIA,
						PR_DESC_FINANCEIRO,
						CD_SISTEMA_ANT,
						IE_FRETE,
						IE_EXIGE_ANEXO_NF,
						IE_BAIXA_ABAT)
				(select 	pessoa_juridica_estab_seq.nextval,
						CD_CGC,
						CD_ESTab_destino_p,
						sysdate,
						NM_USUARIO_p,
						NM_PESSOA_CONTATO,
						NR_RAMAL_CONTATO,
						DS_EMAIL,
						CD_COND_PAGTO,
						QT_DIA_PRAZO_ENTREGA,
						VL_MINIMO_NF,
						IE_QUALIDADE,
						CD_REFERENCIA_FORNEC,
						IE_FORMA_PAGTO,
						sysdate,
						NM_USUARIO_p,
						IE_CONTA_DIF_NF,
						CD_INTERNO_LAB,
						CD_INTERF_LAB_LOTE_ENVIO,
						CD_INTERF_LAB_LOTE_RET,
						NR_SEQ_XML_LAB_ENVIO,
						NR_SEQ_XML_LAB_RET,
						CD_PORTADOR,
						CD_TIPO_PORTADOR,
						CD_TIPO_BAIXA,
						NR_CODIGO_SERV_PREST,
						TX_DESC_ANTECIPACAO,
						CD_INTERF_PLS_MENSALIDADE,
						IE_RATEIO_ADIANT,
						IE_RETEM_ISS,
						TX_DESC_ORDEM,
						IE_ENDERECO_CORRESPONDENCIA,
						PR_DESC_FINANCEIRO,
						CD_SISTEMA_ANT,
						IE_FRETE,
						IE_EXIGE_ANEXO_NF,
						IE_BAIXA_ABAT
				from 	pessoa_juridica_estab 
				where 	cd_cgc = cd_cgc_p
				and	cd_estabelecimento = cd_estabelecimento_p);	
	end;
end if;

if (ie_existe_pj_w > 0) then
	begin
	
	select	NR_SEQUENCIA,
		CD_CGC,
		CD_ESTABELECIMENTO,
		NM_PESSOA_CONTATO,
		NR_RAMAL_CONTATO,
		DS_EMAIL,
		CD_COND_PAGTO,
		QT_DIA_PRAZO_ENTREGA,
		VL_MINIMO_NF,
		IE_QUALIDADE,
		CD_REFERENCIA_FORNEC,
		IE_FORMA_PAGTO,
		IE_CONTA_DIF_NF,
		CD_INTERNO_LAB,
		CD_INTERF_LAB_LOTE_ENVIO,
		CD_INTERF_LAB_LOTE_RET,
		NR_SEQ_XML_LAB_ENVIO,
		NR_SEQ_XML_LAB_RET,
		CD_PORTADOR,
		CD_TIPO_PORTADOR,
		CD_TIPO_BAIXA,
		NR_CODIGO_SERV_PREST,
		TX_DESC_ANTECIPACAO,
		CD_INTERF_PLS_MENSALIDADE,
		IE_RATEIO_ADIANT,
		IE_RETEM_ISS,
		TX_DESC_ORDEM,
		IE_ENDERECO_CORRESPONDENCIA,
		PR_DESC_FINANCEIRO,
		CD_SISTEMA_ANT,
		IE_FRETE,
		IE_EXIGE_ANEXO_NF,
		IE_BAIXA_ABAT
	into	NR_SEQUENCIA_w,
		CD_CGC_w,			
		CD_ESTABELECIMENTO_w,		
		NM_PESSOA_CONTATO_w,		
		NR_RAMAL_CONTATO_w,		
		DS_EMAIL_w,		
		CD_COND_PAGTO_w,
		QT_DIA_PRAZO_ENTREGA_w,		
		VL_MINIMO_NF_w,			
		IE_QUALIDADE_w,			
		CD_REFERENCIA_FORNEC_w,		
		IE_FORMA_PAGTO_w,		
		IE_CONTA_DIF_NF_w,		
		CD_INTERNO_LAB_w,		
		CD_INTERF_LAB_LOTE_ENVIO_w,	
		CD_INTERF_LAB_LOTE_RET_w,	
		NR_SEQ_XML_LAB_ENVIO_w,		
		NR_SEQ_XML_LAB_RET_w,		
		CD_PORTADOR_w,			
		CD_TIPO_PORTADOR_w,		
		CD_TIPO_BAIXA_w,			
		NR_CODIGO_SERV_PREST_w,		
		TX_DESC_ANTECIPACAO_w,		
		CD_INTERF_PLS_MENSALIDADE_w,	
		IE_RATEIO_ADIANT_w,		
		IE_RETEM_ISS_w,
		TX_DESC_ORDEM_w,		
		IE_ENDERECO_CORRESPONDENCIA_w,	
		pR_DESC_FINANCEIRO_w,		
		CD_SISTEMA_ANT_w,		
		IE_FRETE_w,			
		IE_EXIGE_ANEXO_NF_w,		
		IE_BAIXA_ABAT_w			
	from	pessoa_juridica_estab
	where	cd_cgc = cd_cgc_p
	and	cd_estabelecimento = cd_estabelecimento_p;

	update		pessoa_juridica_estab
	set		dt_atualizacao 			= sysdate,
			nm_usuario			= nm_usuario_p,
			dt_atualizacao_nrec		= sysdate,
			nm_usuario_nrec			= nm_usuario_p,
			CD_CGC				= CD_CGC_w,
			CD_ESTABELECIMENTO		= cd_estab_destino_p,
			NM_PESSOA_CONTATO		= NM_PESSOA_CONTATO_w,
			NR_RAMAL_CONTATO		= NR_RAMAL_CONTATO_w,
			DS_EMAIL			= DS_EMAIL_w,
			CD_COND_PAGTO			= CD_COND_PAGTO_w,
			QT_DIA_PRAZO_ENTREGA		= QT_DIA_PRAZO_ENTREGA_w,
			VL_MINIMO_NF			= VL_MINIMO_NF_w,
			IE_QUALIDADE			= IE_QUALIDADE_w,
			CD_REFERENCIA_FORNEC		= CD_REFERENCIA_FORNEC_w,
			IE_FORMA_PAGTO			= IE_FORMA_PAGTO_w,
			IE_CONTA_DIF_NF			= IE_CONTA_DIF_NF_w,
			CD_INTERNO_LAB			= CD_INTERNO_LAB_w,
			CD_INTERF_LAB_LOTE_ENVIO	= CD_INTERF_LAB_LOTE_ENVIO_w,
			CD_INTERF_LAB_LOTE_RET		= CD_INTERF_LAB_LOTE_RET_w,
			NR_SEQ_XML_LAB_ENVIO		= NR_SEQ_XML_LAB_ENVIO_w,
			NR_SEQ_XML_LAB_RET		= NR_SEQ_XML_LAB_RET_W,
			CD_PORTADOR			= CD_PORTADOR_w,
			CD_TIPO_PORTADOR		= CD_TIPO_PORTADOR_w,
			CD_TIPO_BAIXA			= CD_TIPO_BAIXA_w,
			NR_CODIGO_SERV_PREST		= NR_CODIGO_SERV_PREST_w,
			TX_DESC_ANTECIPACAO		= TX_DESC_ANTECIPACAO_w,
			CD_INTERF_PLS_MENSALIDADE	= CD_INTERF_PLS_MENSALIDADE_w,
			IE_RATEIO_ADIANT		= IE_RATEIO_ADIANT_w,
			IE_RETEM_ISS			= IE_RETEM_ISS_w,
			TX_DESC_ORDEM			= TX_DESC_ORDEM_w,
			IE_ENDERECO_CORRESPONDENCIA	= IE_ENDERECO_CORRESPONDENCIA_w,
			PR_DESC_FINANCEIRO		= pR_DESC_FINANCEIRO_w,
			CD_SISTEMA_ANT			= CD_SISTEMA_ANT_w,
			IE_FRETE			= IE_FRETE_w,
			IE_EXIGE_ANEXO_NF		= IE_EXIGE_ANEXO_NF_w,
			IE_BAIXA_ABAt			= IE_BAIXA_ABAT_w		
	where		cd_cgc			=  cd_cgc_p
	and		cd_estabelecimento 	= cd_estab_destino_p;	

	end;
end if;
commit;
end copiar_dados_compl_estab;
/