create or replace
procedure ADEP_exec_lancto_dispositivo(
			nr_atendimento_p		number,
			nr_seq_disp_proc_p	number,
			nm_usuario_p		Varchar2) is 

Ie_Lancto_disp_ret_w	varchar2(1) := 'N';
cd_setor_Atendimento_w	number(10);
nr_seq_proc_interno_w	number(10);
cd_procedimento_w	number(10);
ie_origem_proc_w	number(3);
cd_categoria_w		varchar2(10);
cd_convenio_w		number(5);
nr_Atendimento_w	number(15);
cd_plano_w		varchar2(10);
cd_tipo_acomodacao_w	number(4,0);

begin
obter_param_usuario(1113, 315, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, Ie_Lancto_disp_ret_w);

if	(Ie_Lancto_disp_ret_w = 'S') then
	if	nr_atendimento_p is null then
		Select	nr_atendimento
		into	nr_Atendimento_w
		from	ATEND_PAC_DISP_PROC b,
			ATEND_PAC_DISPOSITIVO c
		where	b.nr_sequencia	= nr_seq_disp_proc_p
		and	c.nr_sequencia	= b.nr_seq_disp_pac;
	else	
		nr_Atendimento_w	:= nr_atendimento_p;
	end if;

	Select	NR_SEQ_PROC_INTERNO,
		cd_setor_atendimento
	into	nr_seq_proc_interno_w,
		cd_setor_Atendimento_w
	from	ATEND_PAC_DISP_PROC b
	where	b.nr_sequencia	= nr_seq_disp_proc_p;	
	
	begin
	select	cd_convenio,
		cd_categoria,
		cd_plano_convenio,
		cd_tipo_acomodacao
	into	cd_convenio_w,
		cd_categoria_w,
		cd_plano_w,
		cd_tipo_acomodacao_w
	from 	atend_categoria_convenio
	where	nr_seq_interno = Obter_Atecaco_atendimento(nr_Atendimento_w)
	and 	nr_Atendimento = nr_Atendimento_w;
	exception
	when no_data_found then
		-- O atendimento #@NR_ATENDIMENTO#@ n�o possui conv�nio definido na Entrada �nica!
		Wheb_mensagem_pck.exibir_mensagem_abort(192069,'NR_ATENDIMENTO=' || nr_Atendimento_w);
	end;
		
	OBTER_PROC_TAB_INTERNO_CONV(	nr_seq_proc_interno_w, wheb_usuario_pck.get_cd_estabelecimento, 
					cd_convenio_w, cd_categoria_w, cd_plano_w, cd_setor_Atendimento_w, 
					cd_procedimento_w, ie_origem_proc_w, null, sysdate, cd_tipo_acomodacao_w, null, null, null, null, null, null, null);
	ADEP_gerar_lancto_Dispositivo(nr_seq_proc_interno_w, nr_seq_disp_proc_p, nr_Atendimento_w, cd_procedimento_w, ie_origem_proc_w, 	1, nm_usuario_p);
end if;


if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end ADEP_exec_lancto_dispositivo;
/
