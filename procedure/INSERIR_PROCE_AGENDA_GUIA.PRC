CREATE OR REPLACE 
PROCEDURE INSERIR_PROCE_AGENDA_GUIA(
					nr_seq_ageint_p number default null,
					nr_seq_agepac_p number default null,
					cd_medico_p	varchar2 default null,
					cd_material_exame_p varchar2 default null,
					ds_observacao_p varchar2 default null,
					nr_atendimento_p number default null,
					nr_doc_convenio_p varchar2 default null,
					nr_seq_exame_p number default null,
					nr_seq_topografia_p number default null,
					nr_seq_proc_interno_p number default null,
					qt_procedimento_p number default null,
					nr_seq_contraste_p number default null,
					ds_ind_clinica_p varchar2 default null,
					ie_origem_informacao_p varchar2 default null,
					nr_prescricao_p	varchar2 default null,
					nm_usuario_p        varchar2 default null) IS
		
ie_lado_w		agenda_integrada_item.ie_lado%type;
vl_coparticipacao_w		agenda_integrada_item.vl_coparticipacao%type;
nr_seq_proc_interno_w		agenda_integrada_item.nr_seq_proc_interno%type;		
nr_seq_topografia_w		agenda_integrada_item.nr_seq_topografia%type;		
nr_seq_exame_w		agenda_integrada_item.nr_seq_proc_interno%type;		
nr_sequencia_w		procedimento_guia_wint.nr_sequencia%type;
nr_seq_proc_guia_w        	   procedimento_guia_wint.nr_sequencia%type := null;
cd_procedimento_tuss_w procedimento_guia_wint.cd_procedimento_tuss%type;
cd_procedimento_w      proc_interno.cd_procedimento%type;
nr_doc_convenio_w		agenda_integrada_item.nr_doc_convenio%type;
vl_item_w		agenda_integrada_item.vl_item%type;
cd_medico_w		agenda_integrada_item.cd_medico%type;
nr_crm_w	procedimento_guia_wint.nr_crm%type;
nr_sequencia_item_w	agenda_integrada_item.nr_sequencia%type;
nr_seq_prescr_proc_w	prescr_procedimento.nr_sequencia%type;
nr_seq_contraste_w		prescr_procedimento.nr_seq_contraste%type;
nr_ano_guia_w	agenda_integrada_item.nr_ano_guia%type;
nr_seq_presc_proc_adic_w prescr_proced_inf_adic.nr_sequencia%type;

CURSOR C01 IS
	select	nr_sequencia,
			null nr_presc_procedimento,
			nr_seq_proc_interno, 
			ie_lado, 
			vl_coparticipacao,
			cd_procedimento,
			nr_doc_convenio,
			nr_seq_topografia,
			null,
			null nr_seq_exame,
			vl_item,
			nvl(cd_medico,cd_medico_req),
			coalesce(crm_medico_externo, obter_crm_medico(cd_medico), '999'),
			nr_ano_guia
	from	agenda_integrada_item
	where	nr_seq_agenda_int = nr_seq_ageint_p
	and		ie_tipo_agendamento in ('C','E','S')
	and 	nr_seq_proc_interno is not null
	union
	select  null nr_seq_age_int_item,
			nr_sequencia,
			nr_seq_proc_interno, 
			ie_lado, 
			obter_valor_copart_wint(nr_prescricao, nr_sequencia),
			cd_procedimento,
			nr_doc_convenio,
			nr_seq_topografia,
			nr_seq_contraste,
			nr_seq_exame,
			(eup_obter_valor_proc_part(nr_atendimento_p, nr_prescricao, nr_sequencia, NVL(cd_medico_exec, cd_medico_solicitante))) * qt_procedimento vl_item,
			nvl(cd_medico_exec, cd_medico_solicitante),
			nvl(obter_crm_medico(nvl(cd_medico_exec,cd_medico_solicitante)), 999),
			null nr_ano_guia
	from 	prescr_procedimento 
	where 	nr_prescricao = nr_prescricao_p 	
	and 	dt_cancelamento is null
	and 	dt_suspensao is null;
	
BEGIN 
if (nvl(nr_seq_ageint_p,0) > 0) then
	delete	procedimento_guia_wint where nr_seq_ageint = nr_seq_ageint_p;
elsif (nvl(nr_seq_agepac_p,0) > 0) then
	delete	procedimento_guia_wint where nr_seq_agepac = nr_seq_agepac_p;
elsif (nvl(nr_prescricao_p,0) > 0) then
	delete	procedimento_guia_wint where nr_prescricao = nr_prescricao_p;
end if;

commit;

open C01;
loop
fetch C01 into
	nr_sequencia_item_w,
	nr_seq_prescr_proc_w,
	nr_seq_proc_interno_w,
	ie_lado_w,
	vl_coparticipacao_w,
	cd_procedimento_w,
	nr_doc_convenio_w,
	nr_seq_topografia_w,
	nr_seq_contraste_w,
	nr_seq_exame_w,
	vl_item_w,
	cd_medico_w,
	nr_crm_w,
	nr_ano_guia_w;
exit when C01%notfound;
	begin
	
	if(nr_sequencia_item_w is not null) then 
	
		select  max(nr_sequencia)
		into    nr_seq_proc_guia_w
		from    procedimento_guia_wint
		where nr_seq_ageint_item = nr_sequencia_item_w
		and nr_seq_ageint = nr_seq_ageint_p;
		
	elsif(nr_seq_prescr_proc_w is not null) then
		
		select  max(nr_sequencia)
		into    nr_seq_proc_guia_w
		from    procedimento_guia_wint
		where nr_seq_prescr_proc = nr_seq_prescr_proc_w
		and nr_prescricao = nr_prescricao_p;
		
		select 	max(nr_sequencia) 
		into 	nr_seq_presc_proc_adic_w
		from 	prescr_proced_inf_adic 
		where 	nr_seq_prescricao = nr_seq_prescr_proc_w
		and 	nr_prescricao = nr_prescricao_p;
		
		select 	max(cd_crm_medico), max(nr_ano_guia) , max(vl_coparticipacao)
		into 	nr_crm_w, nr_ano_guia_w, vl_coparticipacao_w
		from 	prescr_proced_inf_adic 
		where 	nr_sequencia = nr_seq_presc_proc_adic_w;
		
	end if;
	
	if(nr_seq_proc_guia_w is null) then
	
		select procedimento_guia_wint_seq.nextval
		into   nr_sequencia_w
		from   dual;

		wint_proc_interno_tuss(nr_seq_proc_interno_w, cd_procedimento_tuss_w);
		
		insert into PROCEDIMENTO_GUIA_WINT (
					nr_seq_ageint_item,
					nr_seq_prescr_proc,
					cd_material,
					cd_procedimento,
					cd_procedimento_tuss,
					ds_ind_clinica,
					ds_observacao,
					ds_proc_exame,
					dt_atualizacao,
					dt_atualizacao_nrec,
					ie_gerar,
					ie_lado,
					nm_usuario,
					nm_usuario_nrec,
					nr_ano_guia,
					nr_atendimento,
					nr_crm,
					nr_seq_ageint,
					nr_seq_agepac,
					nr_prescricao,
					nr_seq_doc_convenio,
					nr_seq_exame,
					nr_seq_proc_interno,
					nr_seq_topografia,
					nr_sequencia,
					qt_procedimento,
					vl_coparticipacao,
					vl_procedimento,
					nr_seq_contraste,
					ie_origem_informacao)
				 values (
					nr_sequencia_item_w,
					nr_seq_prescr_proc_w,
					cd_material_exame_p,
					cd_procedimento_w,
					cd_procedimento_tuss_w,
					ds_ind_clinica_p,
					ds_observacao_p,
					Obter_Desc_Proc_Interno(nr_seq_proc_interno_w),
					sysdate,
					sysdate,
					'S',
					ie_lado_w,
					nm_usuario_p,
					nm_usuario_p,
					nvl(nr_ano_guia_w,0000),
					nr_atendimento_p,
					nr_crm_w,
					nr_seq_ageint_p,
					nr_seq_agepac_p,
					nr_prescricao_p,
					nvl(nr_doc_convenio_p, nr_doc_convenio_w),
					nvl(nr_seq_exame_p, nr_seq_exame_w),
					nvl(nr_seq_proc_interno_p, nr_seq_proc_interno_w),
					nvl(nr_seq_topografia_p, nr_seq_topografia_w),
					nr_sequencia_w,
					nvl(qt_procedimento_p, 1),
					vl_coparticipacao_w,
					vl_item_w,
					nvl(nr_seq_contraste_w, nr_seq_contraste_p),
					ie_origem_informacao_p); 
		
			commit;
			
	end if;
	
	end;
end loop;
close C01;
		
		
END INSERIR_PROCE_AGENDA_GUIA;
/
