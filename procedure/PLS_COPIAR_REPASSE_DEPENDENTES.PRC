create or replace
procedure pls_copiar_repasse_dependentes
			(	nr_seq_seg_repasse_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
			
nr_seq_segurado_w		number(10);
nr_seq_titular_w		number(10);
nr_seq_seg_repasse_w		number(10);
			
Cursor C01 is
	select	a.nr_sequencia
	from	pls_segurado	a
	where	a.nr_seq_titular	= nr_seq_titular_w
	and	a.dt_liberacao is not null
	and	((a.dt_rescisao is null) or (a.dt_rescisao > sysdate))
	and	not exists	(	select	1
					from	pls_segurado_repasse	x
					where	x.nr_seq_segurado	= a.nr_sequencia
					and	x.dt_fim_repasse is not null);

begin

select	nr_seq_segurado
into	nr_seq_titular_w
from	pls_segurado_repasse
where	nr_sequencia	= nr_seq_seg_repasse_p;

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	
	select	pls_segurado_repasse_seq.nextval
	into	nr_seq_seg_repasse_w
	from	dual;
	
	insert into pls_segurado_repasse
	(		nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			cd_estabelecimento,nr_seq_segurado,nr_seq_congenere,dt_repasse,ie_tipo_repasse,
			nr_seq_plano,ie_cartao_provisorio,nr_seq_motivo_via_adic,nr_seq_congenere_atend,ie_origem_solicitacao,
			ie_tipo_compartilhamento)
	(	select	nr_seq_seg_repasse_w,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
			cd_estabelecimento_p,nr_seq_segurado_w,nr_seq_congenere,dt_repasse,ie_tipo_repasse,
			nr_seq_plano,ie_cartao_provisorio,nr_seq_motivo_via_adic,nr_seq_congenere_atend,'M',
			ie_tipo_compartilhamento
		from	pls_segurado_repasse
		where	nr_sequencia	= nr_seq_seg_repasse_p);
		
	pls_liberar_repasse_seg(nr_seq_seg_repasse_w,nm_usuario_p,'N');
	
	end;
end loop;
close C01;

commit;

end pls_copiar_repasse_dependentes;
/
