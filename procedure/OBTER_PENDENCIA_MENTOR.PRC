create or replace procedure obter_pendencia_mentor ( cd_pessoa_fisica_p number,
                                                    nr_atendimento_p number,
                                                    nr_seq_curativo_p number,
                                                    nr_seq_sinal_vital_p number,
                                                    nr_seq_problema_p number,
                                                    nr_seq_pend_regra_p number,
                                                    qt_registros_w out number,
                                                    qt_registros_justify_w out number) is

qt_registros number(10) := 0;
qt_registros_justify number(10) := 0;
ie_medico_w varchar2(1) := 'N';

begin

select OBTER_SE_PF_MEDICO(cd_pessoa_fisica_p)
into ie_medico_w
from dual;

select count (1), sum(decode(pep_obter_info_pend_mentor(a.nr_seq_pend_pac_acao, 'J'),'S',1,0))
into qt_registros, qt_registros_justify
from    gqa_pend_pac_acao_dest a
where ((a.cd_pessoa_fisica = cd_pessoa_fisica_p and a.IE_PROFISSIONAL_MEDICO <> 'S') or 
( ie_medico_w = 'S' and a.IE_PROFISSIONAL_MEDICO = 'S' and a.nr_seq_pend_pac_acao not in (select p.nr_seq_pend_pac_acao 
                                                                                          from GQA_PEND_PAC_ACAO_DEST p,
                                                                                          gqa_pendencia_pac m,
                                                                                          GQA_PEND_PAC_ACAO v
                                                                                          where   m.nr_sequencia = v.nr_seq_pend_pac
                                                                                          and     p.dt_ciencia is not null
                                                                                          and     v.nr_sequencia = p.nr_seq_pend_pac_acao
                                                                                          and     m.nr_atendimento = nr_atendimento_p
                                                                                          and     p.cd_pessoa_fisica = cd_pessoa_fisica_p)))
and a.dt_justificativa is null
and a.nr_sequencia in (
    select    c.nr_sequencia
    from    pessoa_fisica d,
            gqa_pendencia_pac a,
            GQA_PEND_PAC_ACAO b,
            GQA_PEND_PAC_ACAO_DEST c
    where    a.nr_sequencia = b.nr_seq_pend_pac
    and        b.nr_sequencia = c.nr_seq_pend_pac_acao
    and        d.cd_pessoa_fisica = a.cd_pessoa_fisica
    and       ((c.cd_pessoa_fisica = cd_pessoa_fisica_p) or ( ie_medico_w = 'S' and c.IE_PROFISSIONAL_MEDICO = 'S'  ))
    and        c.dt_encerramento is null
    and        b.dt_encerramento is null
    and        ((nr_atendimento_p = 0)    or (  a.nr_atendimento    = nr_atendimento_p))
    and        ((((nr_seq_curativo_p = 0)    or ( a.nr_seq_curativo    = nr_seq_curativo_p))
    and        ((nr_seq_sinal_vital_p = 0) or ( a.nr_seq_sinal_vital = nr_seq_sinal_vital_p))
    and        ((nr_seq_problema_p = 0) or
        ((a.nr_seq_ciap in (select nr_seq_registro
                            from lista_problema_pac_item
                            where nr_seq_problema = nr_seq_problema_p) or
        a.nr_seq_diag_doenca in (select nr_seq_registro
                                from lista_problema_pac_item
                                where nr_seq_problema = nr_seq_problema_p))))
    and        ((nr_seq_pend_regra_p = 0)  or ( a.nr_sequencia       = nr_seq_pend_regra_p))) or (c.dt_ciencia is null and a.ie_assistencial = 'S'))     
    and        ((Wheb_assist_pck.obterParametroFuncao(355, 2) = 0) or (a.dt_atualizacao_nrec >= sysdate - (Wheb_assist_pck.obterParametroFuncao(355, 2)/24)))
    and        ((nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP' 
                and (nvl(nr_seq_pend_regra_p,0) > 0
                or not exists (
                  select 1 
                  from GQA_PENDENCIA gp,
                  gqa_pendencia_regra gpr
                  where gpr.nr_seq_pendencia = gp.nr_sequencia
                  and a.nr_seq_pend_regra = gpr.nr_sequencia
                  and gp.ie_tipo_pendencia = 12))
                ) or nvl(pkg_i18n.get_user_locale, 'pt_BR') <> 'ja_JP')
    and        (((b.nr_seq_proc is not null or b.nr_seq_proc_saps is not null)
    and not exists (
        select    1
        from    pe_prescricao e
        where    e.nr_seq_pend_pac_acao in (
                                select nr_sequencia
                                from   gqa_pend_pac_acao
                                where  nr_seq_pend_pac = a.nr_sequencia)))
        or ((b.nr_seq_protocolo is not null)      
        and not exists (
            select    1
            from    prescr_medica f
            where    f.nr_seq_pend_pac_acao = b.nr_sequencia))
        or ((b.nr_seq_mprev_programa is not null)      
        and not exists (
            select    1
            from    atend_programa_saude f
            where    f.nr_seq_pend_pac_acao = b.nr_sequencia))
        or ((b.nr_seq_protocolo_ger is not null)      
        and not exists (
            select    1
            from    gqa_protocolo_pac f
            where    f.nr_seq_pend_pac_acao = b.nr_sequencia))
        or ((b.nr_seq_escala is not null)
        and not exists (
            select  1
            from    escala_pend_pac_acao f
                where    f.nr_seq_pend_pac_acao = b.nr_sequencia
        ))
        or ((b.nr_seq_meta is not null)
        and not exists (
            select  1
            from    tof_meta_atend f
                where    f.nr_seq_pend_pac_acao = b.nr_sequencia
        ))
        or (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP' and a.nr_seq_protocolo_int_pac_even is not null)  
    )
 );
 
qt_registros_w :=  qt_registros;
qt_registros_justify_w :=  qt_registros_justify;

end obter_pendencia_mentor;
/
