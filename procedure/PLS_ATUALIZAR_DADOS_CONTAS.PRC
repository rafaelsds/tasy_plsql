create or replace
procedure pls_atualizar_dados_contas(
		ds_lista_nr_conta_p	varchar2,
		nr_seq_prestador_exec_p	number,
		nm_usuario_p		varchar2) is 

ds_lista_nr_conta_w	varchar2(2000);
nr_pos_virgula_w	number(10,0);
nr_seq_sequencia_w	number(10);

begin
if	(ds_lista_nr_conta_p is not null) and
	(nr_seq_prestador_exec_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	ds_lista_nr_conta_w := ds_lista_nr_conta_p;
	
	while (ds_lista_nr_conta_w is not null) loop
		begin		
		nr_pos_virgula_w	:= instr(ds_lista_nr_conta_w,',');
	
		if	(nr_pos_virgula_w > 0) then
			begin
			nr_seq_sequencia_w	:= to_number(substr(ds_lista_nr_conta_w,0,nr_pos_virgula_w-1));
			ds_lista_nr_conta_w	:= substr(ds_lista_nr_conta_w,nr_pos_virgula_w+1,length(ds_lista_nr_conta_w));			
			end;
		else
			begin
			nr_seq_sequencia_w	:= to_number(ds_lista_nr_conta_w);
			ds_lista_nr_conta_w	:= null;
			end;
		end if;
		
		if	(nr_seq_sequencia_w > 0) then
			begin
			update	pls_conta
			set	nr_seq_prestador_exec 	= nr_seq_prestador_exec_p,
				nm_usuario 		= nm_usuario_p
			where   nr_sequencia 		= nr_seq_sequencia_w;
			end;
		end if;
		end;
	end loop;
	end;
end if;
commit;
end pls_atualizar_dados_contas;
/