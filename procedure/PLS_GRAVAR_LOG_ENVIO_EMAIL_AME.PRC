create or replace
procedure pls_gravar_log_envio_email_ame
		(	nr_seq_lote_rem_dest_p	pls_ame_destino_envio.nr_sequencia%type,
			ds_email_p		pls_ame_destino_envio.ds_destino%type,	
			ds_email_copia_p	pls_ame_destino_envio.ds_destino%type,	
			nm_usuario_p		varchar2 ) is

ds_destino_w		pls_ame_destino_envio.ds_destino%type;
			
begin

if	(ds_email_copia_p is null) then
	ds_destino_w := ds_email_p;
else
	ds_destino_w := substr(ds_email_p || '; ' || ds_email_copia_p,1,255);
end if;	

insert into pls_ame_destino_envio 
	      (	nr_sequencia, 
		nr_seq_lote_rem_dest, dt_envio, ds_destino, nm_usuario_envio,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec )
        values(	pls_ame_destino_envio_seq.Nextval, 
		nr_seq_lote_rem_dest_p, sysdate, ds_destino_w, nm_usuario_p,
		sysdate, nm_usuario_p, sysdate, nm_usuario_p );

commit;

end pls_gravar_log_envio_email_ame;
/