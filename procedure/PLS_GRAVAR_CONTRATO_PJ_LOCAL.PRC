create or replace
procedure pls_gravar_contrato_pj_local
			(nr_seq_localizacao_p		Number) is 

nr_contrato_w			number(10);
nr_seq_contrato_w		number(10);

begin

select	max(nr_contrato)
into	nr_contrato_w
from	pls_localizacao_pj
where	nr_sequencia	= nr_seq_localizacao_p;

if	(nvl(nr_contrato_w,0) <> 0) then
	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato	= nr_contrato_w;
	
	if	(nvl(nr_seq_contrato_w,0) <> 0) then
		update	pls_localizacao_pj
		set	nr_seq_contrato	= nr_seq_contrato_w
		where	nr_sequencia	= nr_seq_localizacao_p;
	end if;
else
	update	pls_localizacao_pj
	set	nr_seq_contrato	= null
	where	nr_sequencia	= nr_seq_localizacao_p;
end if;

commit;

end pls_gravar_contrato_pj_local;
/
