create or replace
procedure gerar_cobr_deb_conta_itau
			(	nr_seq_cobr_escrit_p	varchar2,
				nm_usuario_p		Varchar2) is 

nm_pagador_w		varchar2(80);
cd_conta_w		varchar2(15);
ds_cgc_cpf_w		varchar2(14);
cd_agencia_bancaria_w	varchar2(8);
nr_digito_agencia_w	varchar2(2);
ie_tipo_mora_w		varchar2(2);
ie_digito_conta_w	varchar2(2);
vl_tot_registros_w	number(15,2)	:= 0;
vl_titulo_w		number(15,2);
vl_juros_w		number(15,2);
nr_seq_reg_lote_w	number(15)	:= 1;
qt_reg_lote_w		number(15)	:= 0;
qt_lotes_w		number(15)	:= 0;	
qt_arquivo_w		number(15)	:= 0;
nr_titulo_w		number(10);
nr_seq_pagador_w	number(10);
ie_tipo_registro_w	number(3);
dt_vencimento_w		date;
cd_convenio_banco_w	banco_estab_interf.cd_convenio_banco%type;
	
Cursor C01 is
	select	e.cd_agencia_bancaria,
		substr(e.cd_conta,1,15),
		e.ie_digito_agencia,
		e.ie_digito_conta,
		substr(obter_dados_titulo_receber(b.nr_titulo, 'N'),1,30) nm_pagador,
		substr(b.nr_titulo,1,15) nr_titulo,
		d.dt_pagamento_previsto dt_vencimento,
		substr(b.vl_cobranca,1,15) vl_titulo,
		01 ie_tipo_mora,
		substr(d.tx_juros,1,17) vl_juros,
		substr(nvl(d.cd_cgc,obter_cpf_pessoa_fisica(d.cd_pessoa_fisica)),1,14) ds_cgc_cpf
	from	pls_mensalidade e,
		titulo_receber d,
		banco_estabelecimento c,
		titulo_receber_cobr b,
		cobranca_escritural a
	where	a.nr_seq_conta_banco	= c.nr_sequencia 
	and	b.nr_seq_cobranca	= a.nr_sequencia
	and	b.nr_titulo		= d.nr_titulo
	and	d.nr_seq_mensalidade	= e.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;	
	
begin
delete from	w_cobranca_banco;

select	lpad(substr(nvl(max(a.cd_convenio_banco),0),1,13),13,'0')
into	cd_convenio_banco_w
from	banco_estab_interf a,
		banco_estabelecimento b,
		cobranca_escritural	c
where	a.nr_seq_conta_banco 	= b.nr_sequencia
and		b.nr_sequencia			= c.nr_seq_conta_banco
and		c.nr_sequencia			= to_number(nr_seq_cobr_escrit_p)
and		upper(a.ds_objeto)		= upper('gerar_cobr_deb_conta_itau');

-- HEADER DO ARQUIVO 
insert	into w_cobranca_banco
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	nr_seq_reg_lote,
	ie_tipo_pessoa,
	ds_cgc_cpf,
	cd_convenio_banco,
	cd_agencia_bancaria,
	cd_conta,
	nr_digito_agencia, 
	nm_empresa,
	ds_banco,
	dt_geracao,
	nr_seq_envio,
	ie_tipo_registro)
select	w_cobranca_banco_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	1 nr_seq_reg_lote,
	'2' ie_tipo_pessoa, 
	substr(c.cd_cgc,1,14) ds_cgc_cpf,
	cd_convenio_banco_w, --'2119604200796' cd_convenio_banco,
	substr(d.cd_agencia_bancaria,1,4) cd_agencia_bancaria,
	substr(d.cd_conta,1,5) cd_conta,
	substr(d.ie_digito_conta,1,1) nr_digito_agencia,
	substr(c.ds_razao_social,1,30) nm_empresa,
	substr(obter_nome_banco(d.cd_banco),1,30) ds_banco,
	sysdate dt_geracao,
	a.nr_remessa nr_seq_envio,
	0 ie_tipo_registro
from	banco_estabelecimento d,
	pessoa_juridica	c,
	estabelecimento b,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	b.cd_cgc		= c.cd_cgc
and	a.nr_seq_conta_banco	= d.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

-- HEADER DO LOTE
insert	into w_cobranca_banco
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	nr_seq_reg_lote,
	ie_tipo_pessoa,	
	ds_cgc_cpf,	
	cd_convenio_banco,	
	cd_agencia_bancaria,
	nr_digito_agencia,
	nm_empresa,	
	ds_endereco,
	nr_endereco,
	ds_endereco_compl,
	ds_cidade,
	cd_cep,
	sg_estado,
	cd_conta,
	ie_tipo_registro)
select	w_cobranca_banco_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	1 nr_seq_reg_lote,
	'2' ie_tipo_pessoa,	
	substr(c.cd_cgc,1,14) nr_inscricao,	
	cd_convenio_banco_w, --'2119604200796' cd_convenio_banco,
	substr(d.cd_agencia_bancaria,1,4) cd_agencia_bancaria,
	substr(d.ie_digito_conta,1,1) nr_digito_agencia,	
	substr(c.ds_razao_social,1,30) nm_empresa,	
	substr(c.ds_endereco,1,30) ds_endereco, 
	substr(c.nr_endereco,1,30) nr_endereco,
	substr(c.ds_complemento,1,15) ds_endereco_compl,
	substr(c.ds_municipio,1,20) ds_cidade,
	substr(c.cd_cep,1,8) cd_cep,
	substr(c.sg_estado,1,2) sg_estado,
	d.cd_conta,
	1
from	banco_estabelecimento d,
	pessoa_juridica	c,
	estabelecimento b,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	b.cd_cgc		= c.cd_cgc
and	a.nr_seq_conta_banco	= d.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;	

qt_reg_lote_w	:= 1;
qt_lotes_w	:= qt_lotes_w + 1;

open C01;
loop
fetch C01 into	
	cd_agencia_bancaria_w,
	cd_conta_w,
	nr_digito_agencia_w,
	ie_digito_conta_w,
	nm_pagador_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	ie_tipo_mora_w,
	vl_juros_w,
	ds_cgc_cpf_w;
exit when C01%notfound;
	begin
	--REGISTRO DETALHE 
	insert	into w_cobranca_banco
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_reg_lote,
		cd_agencia_bancaria,
		cd_conta,
		ie_digito_conta,
		nm_pagador,
		nr_titulo,
		dt_vencimento,
		vl_titulo,
		ie_tipo_mora,
		vl_juros,
		ds_cgc_cpf,
		ie_tipo_registro)
	values	(w_cobranca_banco_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_reg_lote_w,
		cd_agencia_bancaria_w,
		cd_conta_w,
		to_number(ie_digito_conta_w),
		nm_pagador_w,
		nr_titulo_w,
		dt_vencimento_w,
		vl_titulo_w,
		ie_tipo_mora_w,
		vl_juros_w,
		ds_cgc_cpf_w,
		3);
		
	nr_seq_reg_lote_w	:= nr_seq_reg_lote_w + 1;
	qt_reg_lote_w		:= qt_reg_lote_w + 1;
	vl_tot_registros_w	:= vl_tot_registros_w + vl_titulo_w;	
	end;
end loop;
close C01;	

-- REGISTRO TRAILER DE LOTE 
qt_reg_lote_w	:= qt_reg_lote_w + 1;

insert	into w_cobranca_banco
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	nr_seq_reg_lote,
	qt_reg_lote,
	vl_tot_registros,
	ie_tipo_registro)
values	(w_cobranca_banco_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	1,
	qt_reg_lote_w,
	vl_tot_registros_w,
	5);

qt_arquivo_w	:= qt_reg_lote_w + 2;
	
--REGISTRO TRAILER DE ARQUIVO 
insert	into w_cobranca_banco
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	qt_lotes,
	qt_registros,
	ie_tipo_registro)
values	(w_cobranca_banco_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	qt_lotes_w,
	qt_arquivo_w,
	9);
		
commit;

end gerar_cobr_deb_conta_itau;
/