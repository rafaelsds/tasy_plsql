create or replace
procedure obter_atrib_lookup_wfiltro(
			nm_tabela_ref_p			varchar2,
			nm_atributo_cd_p 	out  	varchar2,
                  	nm_atributo_ds_p	out	varchar2,
			ie_restringe_estab_p	out	varchar2,
			ie_restringe_empresa_p	out	varchar2) is 

ie_restrige_estab_w	Varchar2(01);
ie_restrige_empresa_w	Varchar2(01);
ds_sql_w		Varchar2(2000)	:= '';
i			Integer;
			
begin

select	nvl(max(ie_restringe_estab),'N'),
	nvl(max(ie_restringe_empresa),'N')
into	ie_restrige_estab_w,
	ie_restrige_empresa_W
from	tabela_sistema
where	nm_tabela = nm_tabela_ref_p;

begin
select 	ds_sql_lookup
into	ds_sql_w
from 	Tabela_Sistema
where 	nm_tabela = nm_tabela_ref_p;
exception
	when others then
		ds_sql_w		:= '';
end;

--ds_sql_p				:= ds_sql_w;
ds_sql_w				:= UPPER(ds_sql_w);
ds_sql_w				:= replace(ds_sql_w, 'SELECT','');
ds_sql_w				:= replace(ds_sql_w, chr(13)||chr(10),'');
ds_sql_w				:= substr(ds_sql_w, 1, Instr(ds_sql_w, 'FROM') -1);
ds_sql_w				:= replace(ds_sql_w, ',',' ');
ds_sql_w				:= substr(ds_sql_w, 2, length(ds_sql_w) -1);
i					:= Instr(ds_sql_w, ' ');
nm_atributo_cd_p 			:= replace(substr(ds_sql_w,1, i-1),' ','');
nm_atributo_ds_p			:= ltrim(substr(ds_sql_w,i + 1, length(ds_sql_w)));
if (Instr(nm_atributo_ds_p,' ') <> 0) then
	nm_atributo_ds_p			:= replace(substr(nm_atributo_ds_p, 1, Instr(nm_atributo_ds_p,' ')),' ','');
else
	nm_atributo_ds_p			:= replace(substr(nm_atributo_ds_p, 1, length(nm_atributo_ds_p)),' ','');
end if;

ie_restringe_estab_p		:= ie_restrige_estab_w;
ie_restringe_empresa_p		:= ie_restrige_empresa_w;

end obter_atrib_lookup_wfiltro;
/