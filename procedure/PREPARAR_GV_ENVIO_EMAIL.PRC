create or replace
procedure preparar_gv_envio_email(
			nr_seq_gv_p	w_gv_envio_email.nr_sequencia%type,
			ds_conteudo_p	w_gv_envio_email.ds_conteudo%type,
			nm_usuario_p	varchar) is 

begin

delete from w_gv_envio_email 
where nr_sequencia = nr_seq_gv_p;


insert into w_gv_envio_email  (
	nr_sequencia, 
	ds_conteudo, 
	nm_usuario, 
	dt_atualizacao) 
values (
	nr_seq_gv_p, 
	ds_conteudo_p, 
	nm_usuario_p, 
	sysdate);

commit;

end preparar_gv_envio_email;
/