create or replace
procedure regra_envio_comunic_permancia (nm_usuario_p		Varchar2) is 

ie_possui_w		number(10);
nr_sequencia_w		number(10);
ds_titulo_w		varchar2(80);
ie_tipo_envio_w		varchar2(1);
nm_usuario_remetente_w	varchar(15);
ds_remetente_w		varchar2(200);
qt_tempo_perm_w		number(10);
qt_dias_ant_w		number(10);
qt_dias_post_w		number(10);
nr_atendimento_w	number(10);
nm_pessoa_fisica_w	varchar(255);
cd_setor_atendimento_w	number(10);
cd_setor_ant_w		number(10);
cd_setor_ant_comunic_w  number(10);
ds_setor_atendimento_w	varchar(255);
qt_dias_permanenecia_w	number(10);
cd_unidade_basica_w	varchar2(10);
cd_unidade_compl_w	varchar2(10);
ds_mensagem_w		varchar2(32767);
ds_mensagem_comunic_w	varchar2(32767);
nm_usuario_destino_w	varchar2(15);
nm_usuario_destino_ww	varchar2(4000);
ds_lista_email_usua_w 	varchar2(2000);
ds_lista_email_usua_ww 	varchar2(2000);
ds_email_usuario_w	varchar2(255);
ie_enviou_w 		varchar2(1) :='N';
ie_enviou_email_w	varchar2(1) :='N';
ie_valida_atendimento_w	varchar2(1) :='S';
ie_gerou_atendimento_w	ATENDIMENTO_PACIENTE.nr_atendimento%type;
ds_nome_banco_w		varchar2(255);
ie_somente_internacao_w	varchar2(1);
cd_convenio_w		number(10);
ds_convenio_w		varchar2(100);
dt_entrada_w		date;
dt_entrada_unid_w	date;
cd_estabelecimento_w	atendimento_paciente.cd_estabelecimento%type;

Cursor C01 is
	select	nr_sequencia,
		qt_tempo_perm,
		qt_dias_ant,
		qt_dias_post,
		nvl(ie_somente_internacao, 'N'),
		cd_convenio,
		ie_valida_atendimento,
		cd_estabelecimento
	from	regra_alerta_tempo_perm
	where	ie_situacao = 'A';
	
Cursor C02 is
	select	a.nr_atendimento,
		substr(obter_pessoa_atendimento(a.nr_atendimento,'N'),1,255),
		b.cd_setor_atendimento,
		substr(obter_nome_setor(b.cd_setor_atendimento),1,255),
		(obter_dias_entre_datas(a.dt_entrada_unidade, sysdate) + 1),
		b.cd_unidade_basica, 
		b.cd_unidade_compl,
		substr(obter_nome_convenio(cd_convenio), 1, 100),
		obter_data_entrada(a.nr_atendimento),
		a.dt_entrada_unidade
	from	atend_paciente_unidade a,
		unidade_atendimento b,
		atend_categoria_convenio c,
		atendimento_paciente d
	where	a.nr_atendimento = b.nr_atendimento
	and	a.dt_saida_unidade is null
	and	c.nr_atendimento = a.nr_atendimento
	and	c.nr_seq_interno = obter_atecaco_atendimento(a.nr_atendimento)
	and 	(((nvl(abs(qt_tempo_perm_w),0) - nvl(abs(qt_dias_ant_w),0))  = (obter_dias_entre_datas(a.dt_entrada_unidade, sysdate) + 1))
	or	 ((nvl(abs(qt_tempo_perm_w),0) + nvl(abs(qt_dias_post_w),0)) = (obter_dias_entre_datas(a.dt_entrada_unidade, sysdate) + 1)))
	and	((ie_somente_internacao_w = 'N') or (a.nr_seq_interno = obter_atepacu_paciente(a.nr_atendimento, 'PI')))
	and	c.cd_convenio = decode(cd_convenio_w, null, c.cd_convenio, cd_convenio_w)
	and	a.nr_atendimento = d.nr_atendimento
	and	(d.cd_estabelecimento = nvl(cd_estabelecimento_w, d.cd_estabelecimento))
	order by a.cd_setor_atendimento, 2, 1;
	
Cursor C04 is
select	nm_usuario_destino
	from	destino_alerta_tempo_perm     
    where	nr_seq_regra = nr_sequencia_w;       
	
begin
select 	count(*)
into	ie_possui_w
from	regra_alerta_tempo_perm
where	ie_situacao = 'A';

if (nvl(ie_possui_w,0) > 0) then
	
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w,
		qt_tempo_perm_w,
		qt_dias_ant_w,
		qt_dias_post_w,
		ie_somente_internacao_w,
		cd_convenio_w,
		ie_valida_atendimento_w,
		cd_estabelecimento_w;
	exit when C01%notfound;
		begin
		cd_setor_ant_w := 0;
		cd_setor_ant_comunic_w := 0;
		
		select	max(ds_alerta),
			max(ds_alerta),
			max(ds_titulo),
			max(ie_tipo_envio),
			max(qt_tempo_perm),
			max(nm_usuario_remetente),
			max(ds_remetente)
		into	ds_mensagem_w,
			ds_mensagem_comunic_w,
			ds_titulo_w,
			ie_tipo_envio_w,
			qt_tempo_perm_w,
			nm_usuario_remetente_w,
			ds_remetente_w
		from	regra_alerta_tempo_perm
		where	ie_situacao = 'A'
		and	nr_sequencia = nr_sequencia_w;

		select  substr(obter_select_concatenado_bv('select max(global_name) ds_banco from global_name','',''),1,255) ds_servico_banco
		into	ds_nome_banco_w
		from    dual;

		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@NomeBanco',ds_nome_banco_w),1,32700);					
		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_comunic_w,'@NomeBanco',ds_nome_banco_w),1,32700);	

		ie_gerou_atendimento_w := 0;
		
		open C02;
		loop
		fetch C02 into
			nr_atendimento_w,
			nm_pessoa_fisica_w,
			cd_setor_atendimento_w,
			ds_setor_atendimento_w,
			qt_dias_permanenecia_w,
			cd_unidade_basica_w,
			cd_unidade_compl_w,
			ds_convenio_w,
			dt_entrada_w,
			dt_entrada_unid_w;
		exit when C02%notfound;
			begin
			ie_gerou_atendimento_w := nr_atendimento_w;
			nm_usuario_destino_ww := '';

			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@Atendimento',		nr_atendimento_w), 1, 32700);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@Convenio',		ds_convenio_w), 1, 32700);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@Setor',		ds_setor_atendimento_w),	1, 32700);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@DtEntrada',		to_char(dt_entrada_w, 'dd/mm/yyyy hh24:mi')), 1, 32700);
			ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@DtEntradaUnid',	to_char(dt_entrada_unid_w, 'dd/mm/yyyy hh24:mi')), 1, 32700);
		
			ds_mensagem_comunic_w	:= substr(replace_macro(ds_mensagem_comunic_w,'@Atendimento',	 nr_atendimento_w), 1, 32700);
			ds_mensagem_comunic_w	:= substr(replace_macro(ds_mensagem_comunic_w,'@Convenio',	 ds_convenio_w), 1, 32700);
			ds_mensagem_comunic_w	:= substr(replace_macro(ds_mensagem_comunic_w,'@Setor',	 ds_setor_atendimento_w), 1, 32700);
			ds_mensagem_comunic_w	:= substr(replace_macro(ds_mensagem_comunic_w,'@DtEntrada',	 to_char(dt_entrada_w, 'dd/mm/yyyy hh24:mi')), 1, 32700);
			ds_mensagem_comunic_w	:= substr(replace_macro(ds_mensagem_comunic_w,'@DtEntradaUnid',to_char(dt_entrada_unid_w, 'dd/mm/yyyy hh24:mi')), 1, 32700);
		
			if	(ie_tipo_envio_w = 'E' or ie_tipo_envio_w = 'A') then
				if	(cd_setor_ant_w <> cd_setor_atendimento_w) then
					cd_setor_ant_w := cd_setor_atendimento_w;
				
					if	(ds_mensagem_w is null) then
						ds_mensagem_w := wheb_mensagem_pck.get_texto(309613) || ' ' || ds_setor_atendimento_w || chr(13) || chr(10); -- Setor
					else
						ds_mensagem_w := ds_mensagem_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(309613) || ' ' ||  ds_setor_atendimento_w || chr(13) || chr(10); -- Setor
					end if;
				
				end if;
				ds_mensagem_w := ds_mensagem_w|| wheb_mensagem_pck.get_texto(309614,	'NR_ATENDIMENTO_W=' || nr_atendimento_w || ';' ||
																						'NM_PESSOA_FISICA_W=' || nm_pessoa_fisica_w || ';' ||
																						'CD_UNIDADE_BASICA_W=' || cd_unidade_basica_w || ';' ||
																						'CD_UNIDADE_COMPL_W=' || cd_unidade_compl_w || ';' ||
																						'QT_DIAS_PERMANENECIA_W=' || qt_dias_permanenecia_w); 
												/*
													     Atend. #@NR_ATENDIMENTO_W#@
														   Paciente #@NM_PESSOA_FISICA_W#@
														   Unid B�sica/Compl. #@CD_UNIDADE_BASICA_W#@/#@CD_UNIDADE_COMPL_W#@
														   Dias perm. #@QT_DIAS_PERMANENECIA_W#@

												*/
				
				if	(Length(ds_mensagem_w) > 31000) then
					ds_lista_email_usua_w := '';
					open C04;
					loop
					fetch C04 into	
						nm_usuario_destino_w;
					exit when C04%notfound;
						begin
						
						
						select	nvl(max(ds_email),'X')
							into	ds_email_usuario_w
							from	usuario
							where	ie_situacao = 'A' and
							nm_usuario = nm_usuario_destino_w;
						
						if	(nvl(ds_email_usuario_w,'X') <> 'X') then
							ds_lista_email_usua_w := ds_lista_email_usua_w || ds_email_usuario_w || ',';
						end if;
						end;
					end loop;
					close C04;
					
					ds_lista_email_usua_ww := substr(ds_lista_email_usua_w,1,length(ds_lista_email_usua_w)-1);
					
					begin
						enviar_email(	
							ds_titulo_w,
							ds_mensagem_w,
							ds_remetente_w, -- E-mail cadastrado na regra.
							ds_lista_email_usua_ww,
							nvl(nm_usuario_remetente_w,nm_usuario_p),
							'M');	
					exception
						when	others then
							null;
					end;
					ie_enviou_w := 'S';
					ds_mensagem_w := '';
					ds_lista_email_usua_w := '';
				end if;			
			end if;	
		-- comunica��o interna	
		
		
			if (ie_tipo_envio_w = 'C' or ie_tipo_envio_w = 'A') then
				if (cd_setor_ant_comunic_w <> cd_setor_atendimento_w) then
					cd_setor_ant_comunic_w := cd_setor_atendimento_w;
				
					if (ds_mensagem_comunic_w is null) then
						ds_mensagem_comunic_w := wheb_mensagem_pck.get_texto(309613) || ' ' || ds_setor_atendimento_w || chr(13) || chr(10); -- Setor
					else
						ds_mensagem_comunic_w := ds_mensagem_comunic_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(309613) || ' ' ||  ds_setor_atendimento_w || chr(13) || chr(10); -- Setor
					end if;
				
				end if;
				ds_mensagem_comunic_w := ds_mensagem_comunic_w || wheb_mensagem_pck.get_texto(309614,	'NR_ATENDIMENTO_W=' || nr_atendimento_w || ';' ||
																										'NM_PESSOA_FISICA_W=' || nm_pessoa_fisica_w || ';' ||
																										'CD_UNIDADE_BASICA_W=' || cd_unidade_basica_w || ';' ||
																										'CD_UNIDADE_COMPL_W=' || cd_unidade_compl_w || ';' ||
																										'QT_DIAS_PERMANENECIA_W=' || qt_dias_permanenecia_w); 
												/*
													     Atend. #@NR_ATENDIMENTO_W#@
														   Paciente #@NM_PESSOA_FISICA_W#@
														   Unid B�sica/Compl. #@CD_UNIDADE_BASICA_W#@/#@CD_UNIDADE_COMPL_W#@
														   Dias perm. #@QT_DIAS_PERMANENECIA_W#@

												*/			
								 
				
				if (Length(ds_mensagem_comunic_w) > 31000) then
					nm_usuario_destino_ww := '';
					
					open C04;
					loop
					fetch C04 into	
						nm_usuario_destino_w;
					exit when C04%notfound;
						begin
						nm_usuario_destino_ww := nm_usuario_destino_ww || nm_usuario_destino_w || ',';
						end;
					end loop;
					close C04;
					
					nm_usuario_destino_ww := substr(nm_usuario_destino_ww,1,length(nm_usuario_destino_ww)-1);
					
					if (nm_usuario_destino_ww is not null) then
						iNSERT INTO comunic_interna(
								dt_comunicado,		
								ds_titulo,			
								ds_comunicado,
								nm_usuario,			
								dt_atualizacao,			
								ie_geral,
								nm_usuario_destino,		
								nr_sequencia,			
								ie_gerencial,
								dt_liberacao)
							VALUES(	SYSDATE,				
								ds_titulo_w,			
								ds_mensagem_comunic_w,
								nvl(nm_usuario_remetente_w,nm_usuario_p),			
								SYSDATE,				
								'N',
								nm_usuario_destino_ww,
								comunic_interna_seq.nextval,			
								'N',						
								SYSDATE);
						commit;
					end if;
					
					ie_enviou_email_w := 'S';
					ds_mensagem_comunic_w := '';
					nm_usuario_destino_ww := '';
				end if;			
		end if;				
			end;
		end loop;
		close C02;
		
		if	(ie_valida_atendimento_w = 'N') or
			(ie_gerou_atendimento_w > 0) then
			-- resto e-mail
			if (ds_mensagem_w is not null)  then
				ds_lista_email_usua_w := '';
				open C04;
				loop
				fetch C04 into	
					nm_usuario_destino_w;
				exit when C04%notfound;
					begin
					
				select	nvl(max(ds_email),'X')
                    into	ds_email_usuario_w
                    from	usuario
                    where	ie_situacao = 'A' and
                    nm_usuario = nm_usuario_destino_w; 
					
					if (nvl(ds_email_usuario_w,'X') <> 'X') then
						ds_lista_email_usua_w := ds_lista_email_usua_w || ds_email_usuario_w || ',';
					end if;
					end;
				end loop;
				close C04;

				ds_lista_email_usua_ww := substr(ds_lista_email_usua_w,1,length(ds_lista_email_usua_w)-1);
				
				begin
					enviar_email(	
						ds_titulo_w,
						ds_mensagem_w,
						ds_remetente_w, -- E-mail cadastrado na regra.
						ds_lista_email_usua_ww,
						nvl(nm_usuario_remetente_w,nm_usuario_p),
						'M');	
					ie_enviou_w := 'S';
					ds_mensagem_w := '';
					ds_lista_email_usua_w := '';
				exception
					when	others then
						null;
				end;
				
			end if;	
			-- resto comunic
			if (ds_mensagem_comunic_w is not null)  then
			
				nm_usuario_destino_w := '';
				open C04;
				loop
				fetch C04 into	
					nm_usuario_destino_w;
				exit when C04%notfound;
					begin
					nm_usuario_destino_ww := nm_usuario_destino_ww || nm_usuario_destino_w || ',';
					end;
				end loop;
				close C04;
				
				nm_usuario_destino_ww := substr(nm_usuario_destino_ww,1,length(nm_usuario_destino_ww)-1);
				
				if (nm_usuario_destino_ww is not null) then
					iNSERT INTO comunic_interna(
							dt_comunicado,		
							ds_titulo,			
							ds_comunicado,
							nm_usuario,			
							dt_atualizacao,			
							ie_geral,
							nm_usuario_destino,		
							nr_sequencia,			
							ie_gerencial,
							dt_liberacao)
						VALUES(	SYSDATE,				
							ds_titulo_w,			
							ds_mensagem_comunic_w,
							nvl(nm_usuario_remetente_w,nm_usuario_p),			
							SYSDATE,				
							'N',
							nm_usuario_destino_ww,
							comunic_interna_seq.nextval,			
							'N',						
							SYSDATE);
					commit;
				end if;
				
				ie_enviou_email_w := 'S';
				ds_mensagem_comunic_w := '';
				ds_lista_email_usua_w := '';	
			end if; 
		end if;
		end; 
	end loop;
	close C01;
	
end if;

commit;

end regra_envio_comunic_permancia;
/