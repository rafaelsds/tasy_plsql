create or replace
procedure usc_gerar_cobr_santander_240
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 
			
ds_conteudo_w			varchar2(240);
nr_remessa_w			varchar2(8);
qt_lote_arquivo_w			varchar2(6) := 0;
qt_reg_lote_w			varchar2(6) := 0;
qt_registro_lote_w			varchar2(6) := 0;
nr_seq_arquivo_w			varchar2(6) := 0;
nr_seq_registro_w			varchar2(5) := 0;
nm_empresa_w			varchar2(30);
cd_cgc_w			varchar2(15);
nm_banco_w			varchar2(30);
dt_geracao_w			varchar2(8);
nr_lote_w_w			varchar2(4) := 0;
nr_lote_w				varchar2(4);
ie_tipo_taxa_juro_w			varchar2(1);
ie_tipo_taxa_multa_w		varchar2(1);
cd_multa_w			varchar2(1);
cd_juro_w			varchar2(1);

/* Segmentos */
nr_nosso_numero_w		varchar2(20);
nr_nosso_num_tit_w		titulo_receber.nr_nosso_numero%type;
dt_vencimento_w			varchar2(8);
vl_titulo_w			varchar2(15);
dt_emissao_w			varchar2(8);
cd_agencia_w			varchar2(4);
ie_digito_agencia_w		varchar2(1);
nr_conta_w			varchar2(9);
ie_digito_conta_w			varchar2(1);
cd_conta_cobr_w			varchar2(9);
ie_conta_cobr_w			varchar2(1);
nr_seu_numero_w			varchar2(15);
ie_tipo_inscricao_w			varchar2(1);
nr_inscricao_w			varchar2(15);
nm_sacado_w			varchar2(40);
ds_endereco_sacado_w		varchar2(40);
ds_bairro_sacado_w		varchar2(15);
cd_cep_sacado_w			varchar2(8);
ds_municipio_sacado_w		varchar2(15);
ds_estado_sacado_w		varchar2(2);
cd_mov_remessa_w		varchar2(2);
cd_transmissao_w			varchar2(15);
nm_cedente_w			varchar2(30);
nr_seq_pagador_w			number(10);
nr_titulo_w			number(10);

ds_mensagem_3_w			varchar2(40);
ds_mensagem_4_w			varchar2(40);
vl_mora_w			varchar2(15);
ie_impressao_w			varchar2(1);
cd_banco_w			banco.cd_banco%type;
ie_judicial_w			varchar2(1);
cd_tipo_cobranca_w		varchar2(1);
cd_forma_cadastro_w		varchar2(1);

ds_conteudo_aux_w		varchar2(200);
nr_linha_instrucao_w		varchar2(2);

nr_seq_mensalidade_w		pls_mensalidade.nr_sequencia%type;
nr_linha_w			number(5);
tx_multa_w			titulo_receber.tx_multa%type;
ds_multa_w			varchar2(15);
qt_mensagem_w			number(10);
vl_juros_w			number(10);
cd_especie_titulo_banco_w		especie_titulos_remessa.cd_especie_titulo_banco%type;



/* Segmento P */
cursor C01 is
	select	substr(nvl(c.cd_ocorrencia,'01'),1,2) cd_mov_remessa, 
		substr(x.cd_agencia_bancaria,1,4) cd_agencia,
		substr(z.ie_digito,1,1) ie_digito_agencia,
		substr(x.cd_conta_bloqueto,1,9) nr_conta,
		substr(c.ie_digito_conta,1,1) ie_digito_conta, 
		substr(x.cd_conta,1,9) cd_conta_cobr,
		substr(x.ie_digito_conta,1,1) ie_conta_cobr,
		nvl(b.nr_nosso_numero,'0') nr_nosso_num_tit,
		nvl(substr(obter_nosso_numero_interf(x.cd_banco,b.nr_titulo),1,13),'0') nr_nosso_numero,		
		substr(b.nr_titulo,1,15) nr_seu_numero,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyyyy') dt_vencimento,
		lpad(somente_numero(to_char(nvl(c.vl_cobranca,nvl(b.vl_titulo,0)),'9999999999990.00')),15,'0') vl_titulo,
		to_char(b.dt_emissao,'ddmmyyyy') dt_emissao,
		
		/* Segmento Q */
		
		decode(b.cd_cgc, null,'1','2') ie_tipo_inscricao, 
		substr(b.cd_cgc_cpf,1,15) nr_inscricao,
		upper(elimina_acentuacao(substr(b.nm_pessoa,1,40))) nm_sacado,
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),pls_obter_compl_pagador(d.nr_seq_pagador,'EN')),1,40) ds_endereco_sacado,
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),pls_obter_compl_pagador(d.nr_seq_pagador,'B')),1,15) ds_bairro_sacado,
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8) cd_cep_sacado,
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),1,15) ds_municipio_sacado,
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2) ds_estado_sacado,
		f.nr_sequencia nr_seq_pagador,
		b.nr_titulo nr_titulo,
		substr(nvl(obter_instrucao_bol_estab_fin(b.nr_titulo,a.cd_banco,1,nvl(cd_estabelecimento_p,a.cd_estabelecimento)), ' '),1,40) ds_mensagem_3,
		substr(nvl(obter_instrucao_bol_estab_fin(b.nr_titulo,a.cd_banco,2,nvl(cd_estabelecimento_p,a.cd_estabelecimento)), ' '),1,40) ds_mensagem_4,
		somente_numero(to_char(nvl(nvl(c.vl_juros, obter_juros_multa_titulo(b.nr_titulo, sysdate, 'R', 'J')),0) +
		nvl(nvl(c.vl_multa, obter_juros_multa_titulo(b.nr_titulo, sysdate, 'R', 'M')),0),'9999999999990.00')) vl_mora,
		x.cd_banco,
		d.nr_sequencia,
		b.tx_multa,
		obter_juros_multa_titulo(b.nr_titulo,sysdate,'R','J') vl_juros,
		obter_especie_titulo_banco(a.cd_banco, b.ie_tipo_titulo)
	from	agencia_bancaria	z,
		banco_estabelecimento	x,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v		b,
		titulo_receber_cobr		c,
		cobranca_escritural		a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo			= b.nr_titulo
	and	x.nr_sequencia		= a.nr_seq_conta_banco
	and	z.cd_agencia_bancaria	= x.cd_agencia_bancaria
	and	z.cd_banco		= x.cd_banco
	and	d.nr_sequencia(+)		= b.nr_seq_mensalidade
	and	e.nr_sequencia(+)		= b.nr_seq_carteira_cobr
	and	f.nr_sequencia(+)		= d.nr_seq_pagador
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
Cursor C02 is
	select	replace(rpad(ds_conteudo,100,' '),CHR(10),' ') ds_conteudo_aux,
		lpad(rownum,2,0) nr_linha_instrucao
		-- Busca informa��es dos benefici�rios
	from (select distinct	rpad(rpad(cd_usuario_plano,20,' ') || 
				rpad(nm_beneficiario,35,' ') || 
				rpad(ds_item || ds_tipo_guia,30,' ') ||
				lpad(vl_item,10,' '),100,' ') ds_conteudo,
				ie_ordem,
				nm_beneficiario ds_ordem, ---campo para ordenar o beneficiario
				ds_item ds_ordem2, ---campo para ordenar o item do beneficiario
				nr_seq_item
		from (select 	1 ie_ordem,
				f.nr_sequencia nr_seq_item,
				e.nr_sequencia nr_seq_seg,
				nvl(substr(pls_obter_dados_cart_segurado(e.nr_sequencia,'C'),1,18),' ') cd_usuario_plano,
				nvl(upper(substr(pls_obter_dados_segurado(e.nr_sequencia,'N'),1,30)),' ') nm_beneficiario,
				nvl(substr(elimina_caractere_especial(decode(f.ie_tipo_item,20,l.ds_tipo_lancamento,obter_valor_dominio(1930,f.ie_tipo_item))),1,25),' ') ds_item,
				lpad(nvl(replace(to_char(f.vl_item,'fm9999990.00'),'.',','),' '),10,' ') vl_item,
				e.nr_seq_plano,
				d.nr_seq_segurado,
				d.nr_sequencia,
				(select	decode(y.ie_tipo_guia,'3',' - Consulta','4',' - SP/SADT','5',' - Interna��o','6',' - Honor�rio','11',' - Odontologia',null)
				from	pls_conta y
				where	f.nr_seq_conta	= y.nr_sequencia
				and	f.ie_tipo_item	= 3) ds_tipo_guia
			from	pls_segurado e,
				pls_mensalidade_segurado	d,
				pls_mensalidade_seg_item	f,
				pls_mensalidade		c,
				titulo_receber_cobr		b,
				titulo_receber		x,
				cobranca_escritural		a,
				pls_tipo_lanc_adic	l
			where	a.nr_sequencia    		= b.nr_seq_cobranca
			and	b.nr_titulo			= x.nr_titulo
			and	c.nr_sequencia		= x.nr_seq_mensalidade
			and	d.nr_seq_mensalidade	= c.nr_sequencia
			and	e.nr_sequencia        	= d.nr_seq_segurado
			and 	d.nr_sequencia		= f.nr_seq_mensalidade_seg
			and	f.nr_seq_tipo_lanc	=	 l.nr_sequencia(+)
			and	x.nr_titulo			= nr_titulo_w
			and	d.nr_sequencia in       (select	z.nr_sequencia
							from    pls_mensalidade_segurado z
							where   z.nr_seq_mensalidade = c.nr_sequencia
							and	rownum <= 50)
			order by nm_beneficiario, ds_item)
		union all
		-- Busca instru��es de reajuste, SCA, quita��o, portabilidade e outras instru��es gerais cadastradas
		select replace(rpad(ds_instrucao,100,' '),CHR(10),' ') ds_conteudo,
				ie_ordem,
				'XXX' ds_ordem,
				'XXX' ds_ordem2,
				null
		from (select distinct c.nr_linha ie_ordem,
					-- Retorna a instru��o at� o �ltimo espa�o em branco antes dos 100 caracteres
					substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '),' '),1,100) ds_instrucao,
					h.nr_seq_plano,
					1 ie_ordem2
			from cobranca_escritural a,
				banco_estabelecimento b,
				banco_instrucao_boleto c,
				titulo_receber_cobr d,
				titulo_receber e,
				pls_mensalidade f,
				pls_mensalidade_segurado g,
				pls_segurado h
			where a.nr_seq_conta_banco	= b.nr_sequencia
			and c.cd_estabelecimento	= a.cd_estabelecimento
			and c.cd_banco		= b.cd_banco
			and d.nr_seq_cobranca	= a.nr_sequencia
			and e.nr_titulo		= d.nr_titulo
			and e.nr_seq_mensalidade	= f.nr_sequencia
			and g.nr_seq_mensalidade	= f.nr_sequencia
			and g.nr_seq_segurado	= h.nr_sequencia
			and e.nr_titulo		= nr_titulo_w
			and c.nr_linha		> 2
			and g.nr_sequencia in 	(select z.nr_sequencia
									from pls_mensalidade_segurado z
									where z.nr_seq_mensalidade = f.nr_sequencia
									and rownum <= 50)
			union all
			select distinct c.nr_linha ie_ordem,  -- Retorna o complemento da instru��o quando esta for maior que 100 caracteres
					-- Retorna o restante da instru��o a partir do ponto onde parou a instru��o do select anterior at� atingir 100 caracteres ap�s este ponto
					/*substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '), ' '), -- String retornada
						instr(substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '),' '),1,101),' ',-1,1), --Posi��o inicial 
						instr(substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '), ' '),
							instr(substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '),' '),1,101),' ',-1,1), --Posi��o final 
							instr(substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '),' '),1,101),' ',-1,1)+100),' ',-1,1))	ds_instrucao,*/
					substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '),' '),101,200) ds_instrucao,
					h.nr_seq_plano,
					2 ie_ordem2
			from cobranca_escritural a,
				banco_estabelecimento b,
				banco_instrucao_boleto c,
				titulo_receber_cobr d,
				titulo_receber e,
				pls_mensalidade f,
				pls_mensalidade_segurado g,
				pls_segurado h
			where a.nr_seq_conta_banco	= b.nr_sequencia
			and c.cd_estabelecimento	= a.cd_estabelecimento
			and c.cd_banco				= b.cd_banco
			and d.nr_seq_cobranca		= a.nr_sequencia
			and e.nr_titulo				= d.nr_titulo
			and e.nr_seq_mensalidade	= f.nr_sequencia
			and g.nr_seq_mensalidade	= f.nr_sequencia
			and g.nr_seq_segurado		= h.nr_sequencia
			and e.nr_titulo				= nr_titulo_w
			and c.nr_linha				> 2
			and length(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '), ' ')) > 100
			and g.nr_sequencia in 	(select z.nr_sequencia
									from pls_mensalidade_segurado z
									where z.nr_seq_mensalidade = f.nr_sequencia
									and rownum <= 50)
			union all
			select distinct c.nr_linha ie_ordem,  -- Retorna o complemento da instru��o quando esta for maior que 200 caracteres
					-- Retorna o restante da instru��o a partir do ponto onde parou a instru��o do select anterior at� atingir 100 caracteres ap�s este ponto
					/*substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '), ' '), -- String retornada
						instr(substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '), ' '), 
									1,instr(substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '),' '),1,101),' ',-1,1)+101),' ',-1,1), --instr Posi��o inicial 
						instr(substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '), ' '), 
									instr(substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '),' '),1,101),' ',-1,1), --instr Posi��o final
									instr(substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '),' '),1,101),' ',-1,1)+100),' ',-1,1)+100)	ds_instrucao,*/
					substr(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '),' '),201,300) ds_instrucao,
					h.nr_seq_plano,
					3 ie_ordem2
			from cobranca_escritural a,
				banco_estabelecimento b,
				banco_instrucao_boleto c,
				titulo_receber_cobr d,
				titulo_receber e,
				pls_mensalidade f,
				pls_mensalidade_segurado g,
				pls_segurado h
			where a.nr_seq_conta_banco	= b.nr_sequencia
			and c.cd_estabelecimento	= a.cd_estabelecimento
			and c.cd_banco				= b.cd_banco
			and d.nr_seq_cobranca		= a.nr_sequencia
			and e.nr_titulo				= d.nr_titulo
			and e.nr_seq_mensalidade	= f.nr_sequencia
			and g.nr_seq_mensalidade	= f.nr_sequencia
			and g.nr_seq_segurado		= h.nr_sequencia
			and e.nr_titulo				= nr_titulo_w
			and c.nr_linha				> 2
			and length(nvl(replace(obter_instrucao_bol_estab_fin(e.nr_titulo,b.cd_banco,c.nr_linha,nvl(cd_estabelecimento_p,a.cd_estabelecimento)),CHR(13),' '), ' ')) > 200
			and g.nr_sequencia in 	(select z.nr_sequencia
									from pls_mensalidade_segurado z
									where z.nr_seq_mensalidade = f.nr_sequencia
									and rownum <= 50)
			order by 1,4)
		where nvl(ds_instrucao,' ') != ' ' -- Verifica se a instru��o n�o est� em branco
		order by 2,3,4) -- Ordena pelo campo ie_ordem, ds_ordem, ds_ordem2 para as instru��es sa�rem na ordem correta
	where rownum < 98;

begin
delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;

/* Header Arquivo*/
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;

select	elimina_acentuacao(substr(obter_nome_pf_pj(null,b.cd_cgc),1,30)) nm_empresa,
	upper(substr(obter_nome_banco(c.cd_banco),1,30)) nm_banco,
	b.cd_cgc cd_cgc,
	to_char(a.nr_sequencia) nr_seq_arquivo,
	to_char(sysdate,'ddmmyyyy') dt_geracao,
	substr(nvl(c.cd_transmissao,'0'),1,15) cd_transmissao,
	nvl(substr(e.cd_tipo_cobranca_ext,1,1),'5') cd_tipo_cobranca,
	nvl(substr(e.cd_forma_cadastro_ext,1,1),'1') cd_forma_cadastro
into	nm_empresa_w,
	nm_banco_w,
	cd_cgc_w,
	nr_seq_arquivo_w,
	dt_geracao_w,
	cd_transmissao_w,
	cd_tipo_cobranca_w,
	cd_forma_cadastro_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d,
	tipo_cobr_escrit e
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
and	a.nr_seq_tipo		= e.nr_sequencia(+);

ds_conteudo_w	:=	'033' ||
			'0000' ||
			'0' ||
			rpad(' ',8,' ') ||
			'2' ||
			lpad(nvl(cd_cgc_w,'0'),15, '0') ||
			lpad(nvl(cd_transmissao_w,'0'),15,'0') ||
			rpad(' ',25,' ') ||
			rpad(nvl(nm_empresa_w,' '),30,' ') || 
			rpad(nvl(nm_banco_w,' '),30,' ') ||
			rpad(' ',10,' ') ||
			'1' ||
			lpad(nvl(dt_geracao_w,'0'),8,'0') ||
			rpad(' ',6,' ') ||
			lpad(nvl(nr_seq_arquivo_w,'0'),6, '0') ||
			'040' ||
			rpad(' ',74,' ');

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	1,
	1);

/* Fim Header Arquivo */

/* Header Lote */
nr_lote_w_w	:= nr_lote_w_w + 1;
nr_lote_w		:= lpad(nr_lote_w_w,4,'0');
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
qt_registro_lote_w	:= qt_registro_lote_w + 1;

select	b.cd_cgc cd_cgc,
	elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,30)) nm_cedente,
	substr(nvl(a.nr_remessa,a.nr_sequencia),1,8) nr_remessa,
	to_char(sysdate,'ddmmyyyy') dt_geracao,
	substr(nvl(c.cd_transmissao,'0'), 1, 15) cd_transmissao
into	cd_cgc_w,
	nm_cedente_w,
	nr_remessa_w,
	dt_geracao_w,
	cd_transmissao_w
from	estabelecimento		b,
	banco_estabelecimento	c,
	cobranca_escritural		a,
	banco_carteira		d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:=	'033' ||
			lpad(nvl(nr_lote_w,'0'),4, '0') ||
			'1' ||
			'R' ||
			'01' ||
			rpad(' ', 2, ' ') ||
			'030' ||
			rpad(' ', 1, ' ') ||
			'2' ||
			lpad(nvl(cd_cgc_w,'0'),15, '0') ||
			rpad(' ', 20, ' ') ||
			lpad(nvl(cd_transmissao_w,'0'),15,'0') ||
			rpad(' ',5,' ') ||
			rpad(nvl(nm_cedente_w,' '),30, ' ') ||
			rpad(' ', 40, ' ') ||
			rpad(' ', 40, ' ') ||
			lpad(nvl(nr_remessa_w,'0'),8, '0') || 
			lpad(nvl(dt_geracao_w,'0'),8, '0') ||
			rpad(' ', 41, ' ');

insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	2,
	2);

qt_reg_lote_w		:= qt_reg_lote_w + 1;
/* Fim Header Lote */

/* Inicio Segmentos */
open C01;
loop
fetch C01 into	
	cd_mov_remessa_w,
	cd_agencia_w,
	ie_digito_agencia_w,
	nr_conta_w,
	ie_digito_conta_w,
	cd_conta_cobr_w,
	ie_conta_cobr_w,
	nr_nosso_num_tit_w,
	nr_nosso_numero_w,
	nr_seu_numero_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	ds_bairro_sacado_w,
	cd_cep_sacado_w,
	ds_municipio_sacado_w,
	ds_estado_sacado_w,
	nr_seq_pagador_w,
	nr_titulo_w,
	ds_mensagem_3_w,
	ds_mensagem_4_w,
	vl_mora_w,
	cd_banco_w,
	nr_seq_mensalidade_w,
	tx_multa_w,
	vl_juros_w,
	cd_especie_titulo_banco_w;
exit when C01%notfound;
	begin
	/* Segmento P */
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	qt_registro_lote_w	:= qt_registro_lote_w + 1;
	qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;

	select	max(b.ie_tipo_taxa)
	into	ie_tipo_taxa_juro_w
	from	tipo_taxa		b,
		titulo_receber		a
	where	a.cd_tipo_taxa_juro		= b.cd_tipo_taxa
	and	a.nr_titulo			= nr_titulo_w;

	if	(ie_tipo_taxa_juro_w in ('V','F')) then
		cd_juro_w		:= '1';
	elsif	(ie_tipo_taxa_juro_w = 'D') then
		cd_juro_w		:= '2';
	else
		cd_juro_w		:= '3';
	end if;

	select	max(b.ie_tipo_taxa)
	into	ie_tipo_taxa_multa_w
	from	tipo_taxa		b,
		titulo_receber		a
	where	a.cd_tipo_taxa_multa	= b.cd_tipo_taxa
	and	a.nr_titulo		= nr_titulo_w;

	if	(ie_tipo_taxa_multa_w in ('V','F')) then
		cd_multa_w	:= '1';
	else
		cd_multa_w	:= '2';
	end if;
	
	if	(nr_nosso_num_tit_w = '0') and /* Tratamento para gravar o nosso n�mero conforme parametriza��o do t�tulo e para atualizar a sequencia do nosso n�mero na regra do mesmo */
		(nr_nosso_numero_w <> '0') then
		update	titulo_receber
		set	nr_nosso_numero 		= nr_nosso_numero_w,
			ds_observacao_titulo	= substr(ds_observacao_titulo ||
						' Nosso n�mero ' || nr_nosso_numero_w || ' atualizado atrav�s da procedure gerar_cobr_santander_240 ao gerar remessa da cobran�a escritural ' || nr_seq_cobr_escrit_p,1,4000)
		where	nr_titulo 			= nr_titulo_w;
		
		update	banco_regra_numero
		set	nr_atual			= nvl(nr_atual,0) + 1
		where	cd_banco		= cd_banco_w
		and	cd_estabelecimento		= cd_estabelecimento_p
		and	ie_regra			= 'FSD';
	elsif	(nr_nosso_num_tit_w <> '0') then
		nr_nosso_numero_w		:= lpad(substr(nr_nosso_num_tit_w,1,13),13,'0');
	end if;

	ds_conteudo_w	:=	'033' || --001 - 003
				lpad(nr_lote_w, 4,'0') || --004 - 007
				'3' || --008 - 008
				lpad(nvl(nr_seq_registro_w,'0'), 5, '0') || --009 - 013
				'P' || --014 - 014
				rpad(' ', 1, ' ') || --015 - 015
				lpad(nvl(cd_mov_remessa_w,'0'), 2, '0') || --016 - 017 
				lpad(nvl(cd_agencia_w,'0'), 4, '0') || --018 �021
				lpad(nvl(ie_digito_agencia_w,'0'), 1, '0') || --022 �022
				lpad(nvl(nr_conta_w,'0'), 9, '0') || --023 - 031
				lpad(nvl(ie_conta_cobr_w,'0'), 1, '0') || --032 � 032
				lpad(nvl(cd_conta_cobr_w,'0'), 9, '0') || --033 - 041 
				lpad(nvl(ie_conta_cobr_w,'0'), 1, '0') || --042 - 042
				rpad(' ', 2, ' ') || --043 - 044
				lpad(nvl(nr_nosso_numero_w,'0'), 13, '0') || --045 �057
				'5' || --058 - 058
				'1' || -- 059 - 059
				'2' || --060 - 060
				rpad(' ', 2, ' ') || --061 �062
				lpad(nvl(nr_seu_numero_w,'0'), 15, '0') || --063 - 077
				lpad(nvl(dt_vencimento_w,'0'), 8, '0') || --078 - 085
				vl_titulo_w || --086 - 100
				'0000' || --101 - 104
				'0' || --105 � 105
				rpad(' ', 1, ' ') || --106 - 106
				lpad(nvl(cd_especie_titulo_banco_w,'2'), 2, '0') || --107 � 108
				'N' || --109 - 109
				lpad(nvl(dt_emissao_w,'0'), 8, '0') || --110 - 117
				'3' || --118 - 118
				lpad(nvl('0','0'), 8, '0') || --119 - 126
				lpad(nvl('0','0'), 15, '0') || --127 - 141
				'0' || --142 - 142
				'00000000' || --143 - 150
				lpad('0', 15, '0') || --151 - 165
				lpad('0', 15, '0') || --166 - 180
				lpad('0', 15, '0') || --181 - 195
				rpad(' ', 25, ' ') || --196 - 220
				'0' || --221 - 221
				'00' || --222 - 223
				'2' || --224 - 224
				'0' || --225 � 225
				'00' || --226 - 227
				'00' || --228 - 229
				rpad(' ', 11, ' '); --230 �240

	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		3,
		nr_seq_registro_w);
	/* Fim segmento P */

	qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
	qt_registro_lote_w	:= qt_registro_lote_w + 1;

	/* Segmento Q */
	ds_conteudo_w		:= null;
	nr_seq_registro_w	:= nr_seq_registro_w + 1;

	ds_conteudo_w	:=	'033' ||
				lpad(nvl(nr_lote_w,'0'),4,'0') ||
				'3' ||
				lpad(nvl(nr_seq_registro_w,'0'), 5, '0') ||
				'Q' ||
				rpad(' ',1, ' ') ||
				lpad(nvl(cd_mov_remessa_w,'0'),2,'0') || 
				lpad(nvl(ie_tipo_inscricao_w,'0'),1,'0') ||
				lpad(nvl(nr_inscricao_w,'0'),15,'0') ||
				rpad(nvl(nm_sacado_w,' '),40,' ') ||
				rpad(nvl(ds_endereco_sacado_w,' '),40,' ') ||
				rpad(nvl(ds_bairro_sacado_w,' '),15,' ') ||
				lpad(nvl(cd_cep_sacado_w,'0'),8,'0') ||
				rpad(nvl(ds_municipio_sacado_w,' '),15,' ') ||
				rpad(nvl(ds_estado_sacado_w,' '),2,' ') ||
				lpad('0', 16, '0') ||
				rpad(' ', 40, ' ') ||
				'000' || 
				'000' ||
				'000' ||
				'000' ||
				rpad(' ', 19, ' '); 

	insert	into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres,
		nr_seq_apres_2)
	values	(w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		3,
		nr_seq_registro_w);

	/* Fim segmento Q */

	/* Segmento R */
	--if	(somente_numero(vl_mora_w) <> 0) then

		ds_conteudo_w		:= null;
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		qt_registro_lote_w	:= qt_registro_lote_w + 1;
		qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;

		if	(cd_multa_w	= '1') then

			ds_multa_w	:= lpad(vl_mora_w, 15, '0');

		else

			ds_multa_w	:= lpad(tx_multa_w, 15, '0');

		end if;

		ds_conteudo_w	:=	'033' ||
					lpad(nvl(nr_lote_w,'0'), 4, '0') ||
					'3' ||
					lpad(nvl(nr_seq_registro_w,'0'), 5, '0') ||
					'R' ||
					rpad(' ', 1, ' ') ||
					lpad(nvl(cd_mov_remessa_w,'0'), 2, '0') ||
					'0' ||
					lpad('0', 8, '0') ||
					lpad('0', 15, '0') ||
					rpad(' ', 24, ' ') ||
					lpad(nvl(cd_multa_w,'0'), 1, '0') ||
					lpad(nvl(dt_vencimento_w,'0'), 8, '0') ||
					ds_multa_w ||
					rpad(' ', 10, ' ') ||
					rpad(nvl(ds_mensagem_3_w,' '), 40, ' ') ||
					rpad(nvl(ds_mensagem_4_w,' '), 40, ' ') ||
					rpad(' ', 61, ' ');

		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres,
			nr_seq_apres_2)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			3,
			nr_seq_registro_w);

	--end if;
	/* Fim segmento R */

	/* � obrigat�rio aparecer esse processo judicial no boleto, quando ele existir */
	select	nvl(max('S'),'N')
	into	ie_judicial_w
	from	pls_mensalidade_segurado b,
		processo_judicial_liminar a
	where	b.nr_seq_mensalidade	= nr_seq_mensalidade_w
	and	a.nr_seq_segurado	= b.nr_sequencia
	and	a.cd_juridico_liminar	= '5830020082380670';


	/* Segmento S*/
	open C02;
	loop
	fetch C02 into	
		ds_conteudo_aux_w,
		nr_linha_instrucao_w;
	exit when C02%notfound;
		begin
		
		ds_conteudo_w		:= '';
		nr_seq_registro_w	:= nvl(nr_seq_registro_w,0) + 1;
		qt_registro_lote_w	:= nvl(qt_registro_lote_w,0) + 1;
		qt_lote_arquivo_w	:= nvl(qt_lote_arquivo_w,0) + 1;
		
		ds_conteudo_w	:= '033' || -- 001 a 003
							lpad(nvl(nr_lote_w,'0'),4,0) || -- 004 a 007
							'3' || -- 008 a 008
							lpad(nvl(nr_seq_registro_w,'0'),5,'0') || -- 009 a 013
							'S' || -- 014 a 014
							rpad(' ',1,' ') || -- 015 a 015
							lpad(nvl(cd_mov_remessa_w,'0'),2,'0') || -- 016 a 017
							'1' || -- 018 a 018
							lpad(nvl(nr_linha_instrucao_w,'0'),2,'0') || -- 019 a 020
							'4' || -- 021 a 021
							rpad(substr(ds_conteudo_aux_w,1,100),100,' ') || -- 022 a 121
							rpad(' ',119,' ');
							
		insert	into w_envio_banco
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres,
			nr_seq_apres_2)
		values	(w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			3,
			nr_seq_registro_w);
			
		end;
	end loop;
	close C02;

	/* Fim segmento S */


	end;
end loop;
close C01;
/*Fim Segmentos */

/* Trailler Lote*/
begin
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;
qt_registro_lote_w	:= qt_registro_lote_w + 1;
	
ds_conteudo_w	:=	'033' ||
			lpad(nvl(nr_lote_w,'0'), 4, '0') ||
			'5' ||
			lpad(' ', 9, ' ') ||
			lpad(nvl(qt_registro_lote_w,'0'), 6, '0') ||
			rpad(' ', 217, ' ');

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	5,
	5);
end;
/* Fim Trailler Lote*/

/* Trailler Arquivo*/
begin
qt_lote_arquivo_w	:= qt_lote_arquivo_w + 1;

ds_conteudo_w	:=	'033' ||
			'9999' ||
			'9' ||
			lpad(' ', 9, ' ') ||
			lpad(nvl(qt_reg_lote_w,'0'), 6, '0') ||
			lpad(nvl(qt_lote_arquivo_w,'0'), 6, '0') ||
			rpad(' ', 211, ' ');

insert	into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres,
	nr_seq_apres_2)
values	(w_envio_banco_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	6,
	6);
end;
/* Fim Trailler Arquivo*/

commit;

end usc_gerar_cobr_santander_240;
/