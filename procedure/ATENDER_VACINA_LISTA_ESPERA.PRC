create or replace
procedure atender_vacina_lista_espera (	nr_sequencia_p	Number,
					nm_usuario_p	Varchar2) is 

begin
if	(nvl(nr_sequencia_p,0) > 0) then
	begin
	
	update	vacina_lista_espera
	set	dt_execucao	= sysdate,
		nm_usuario_exec	= nm_usuario_p,
		ie_status	= 'E'
	where	nr_sequencia	= nr_sequencia_p;
	
	end;
end if;

commit;

end atender_vacina_lista_espera;
/
