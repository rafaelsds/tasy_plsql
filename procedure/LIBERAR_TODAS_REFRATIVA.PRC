CREATE OR REPLACE PROCEDURE liberar_todas_refrativa IS
BEGIN
  UPDATE OFT_CIRURGIA_REFRATIVA
  SET dt_liberacao = sysdate,
    nm_usuario     = 'TASY',
    ie_situacao    = 'A';
  COMMIT;
END liberar_todas_refrativa;
/