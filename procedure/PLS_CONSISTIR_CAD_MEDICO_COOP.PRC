create or replace
procedure pls_consistir_cad_medico_coop
		(cd_pessoa_fisica_p	varchar2,
		cd_cgc_p		varchar2,
		dt_inclusao_p		Date,
		nr_sequencia_p		number,
		nm_usuario_p		varchar2) is 

nr_seq_cooperado_w	number(15);
					
begin
select	max(nr_sequencia)
into	nr_seq_cooperado_w
from	pls_cooperado
where	((cd_pessoa_fisica 	= cd_pessoa_fisica_p) or
	 (cd_cgc		= cd_cgc_p))
and	dt_exclusao is null
and	nr_sequencia <> nr_sequencia_p;

if	(nr_seq_cooperado_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(265472,'SEQ_COOPERADO=' ||nr_seq_cooperado_w);
	--Este cooperado ja est� cadastrado no Tasy! Seq ||nr_seq_cooperado_w
else
	nr_seq_cooperado_w := 0;
end if;

select	max(nr_sequencia)
into	nr_seq_cooperado_w
from	pls_cooperado
where	((cd_pessoa_fisica 	= cd_pessoa_fisica_p) or
	 (cd_cgc		= cd_cgc_p))
and	to_date(dt_inclusao_p) < to_date(dt_exclusao)
and	nr_sequencia <> nr_sequencia_p;

if	(nr_seq_cooperado_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(265474,'SEQ_COOPERADO=' ||nr_seq_cooperado_w);
	--N�o � permitido incluir este coooperado com a data de inclus�o menor do que a data de exclus�o ja existente! Seq ||nr_seq_cooperado_w
end if;

end pls_consistir_cad_medico_coop;
/
