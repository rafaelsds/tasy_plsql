create or replace
procedure gravar_anexo_pj(	nr_seq_doc_p		number,
				ds_arquivo_p		varchar2,
				nm_usuario_p		varchar2,
				ds_titulo_p		varchar2,
				nr_seq_tipo_anexo_p     varchar2) is

begin

if	(nr_seq_doc_p is not null) then
	insert into pessoa_juridica_doc_anexo(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_arquivo,
		ds_titulo,
		nr_seq_doc,
		nr_seq_tipo_anexo)
	values(	pessoa_juridica_doc_anexo_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		ds_arquivo_p,
		ds_titulo_p,
		nr_seq_doc_p,
		nr_seq_tipo_anexo_p);
	commit;
end if;

end gravar_anexo_pj;
/