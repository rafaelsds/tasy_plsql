create or replace
procedure sus_gerar_w_esus_procedi(	nr_seq_lote_p	esus_lote_envio.nr_sequencia%type,
					ie_tipo_lote_esus_p	esus_lote_envio.ie_tipo_lote_esus%type,
					cd_cnes_estab_p		estabelecimento.cd_cns%type,
					nm_usuario_p		usuario.nm_usuario%type) is 


nr_seq_exp_w			w_esus_ficha_proc_profis.nr_sequencia%type;
cd_uuid_original_w		w_esus_ficha_proc_profis.cd_uuid_original%type;
nr_data_atendimento_w		w_esus_ficha_proc_profis.cd_uuid_original%type;
cd_cns_profissional_w		w_esus_ficha_proc_profis.cd_cns_profissional%type;

nr_sequencia_w			esus_ficha_proc_profis.nr_sequencia%type;
cd_estabelecimento_w		esus_ficha_proc_profis.cd_estabelecimento%type;
dt_atualizacao_w			esus_ficha_proc_profis.dt_atualizacao%type;
nm_usuario_w			esus_ficha_proc_profis.nm_usuario%type;
dt_atualizacao_nrec_w		esus_ficha_proc_profis.dt_atualizacao_nrec%type;
nm_usuario_nrec_w		esus_ficha_proc_profis.nm_usuario_nrec%type;
cd_profissional_w		esus_ficha_proc_profis.cd_profissional%type;
cd_cbo_w			esus_ficha_proc_profis.cd_cbo%type;
cd_cnes_unidade_w		esus_ficha_proc_profis.cd_cnes_unidade%type;
cd_cnes_unidade_ww		esus_ficha_proc_profis.cd_cnes_unidade%type;
nr_seq_sus_equipe_w		esus_ficha_proc_profis.nr_seq_sus_equipe%type;
dt_atendimento_w		esus_ficha_proc_profis.dt_atendimento%type;
dt_liberacao_w			esus_ficha_proc_profis.dt_liberacao%type;
qt_afericao_pa_w		esus_ficha_proc_profis.qt_afericao_pa%type;
qt_afericao_temperat_w		esus_ficha_proc_profis.qt_afericao_temperat%type;
qt_curativo_simples_w		esus_ficha_proc_profis.qt_curativo_simples%type;
qt_col_mate_exam_lab_w		esus_ficha_proc_profis.qt_col_mate_exam_lab%type;
qt_glicemia_capilar_w		esus_ficha_proc_profis.qt_glicemia_capilar%type;
qt_medicao_altura_w		esus_ficha_proc_profis.qt_medicao_altura%type;
qt_medicao_peso_w		esus_ficha_proc_profis.qt_medicao_peso%type;
nr_seq_lote_envio_w 		esus_ficha_proc_profis.nr_seq_lote_envio%type;

nr_seq_ficha_proc_w		w_esus_ficha_procedimento.nr_sequencia%type;
cd_cns_paciente_w		w_esus_ficha_procedimento.cd_cns_paciente%type;
dt_nascimento_paciente_w	w_esus_ficha_procedimento.dt_nascimento_paciente%type;
nr_data_nasc_pac_w		w_esus_ficha_procedimento.nr_data_nasc_pac%type;
ie_sexo_paciente_w		w_esus_ficha_procedimento.ie_sexo_paciente%type;
nr_prontuario_w			w_esus_ficha_procedimento.nr_prontuario%type;
nr_hora_inicio_atend_w          w_esus_ficha_procedimento.nr_hora_inicio_atend%type;
nr_hora_final_atend_w           w_esus_ficha_procedimento.nr_hora_final_atend%type;
cd_cpf_paciente_w               w_esus_ficha_procedimento.cd_cpf_paciente%type;

cd_contra_chave_rem_w		w_esus_header_footer.cd_contra_chave_rem%type;
cd_uuid_instal_rem_w		w_esus_header_footer.cd_uuid_instal_rem%type;
cd_contra_chave_ori_w		w_esus_header_footer.cd_contra_chave_ori%type;
cd_uuid_instal_orig_w		w_esus_header_footer.cd_uuid_instal_orig%type;
cd_municipio_ibge_ww		w_esus_header_footer.cd_municipio_ibge%type;
nr_seq_ficha_proc_ww		esus_ficha_pro_proced.nr_seq_ficha_proc%type;

Cursor ProcProf is
	select 	nr_sequencia,           
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_profissional,
		cd_cbo,
		cd_cnes_unidade,
		nr_seq_sus_equipe,
		dt_atendimento,
		dt_liberacao,
		qt_afericao_pa,
		qt_afericao_temperat,
		qt_curativo_simples,
		qt_col_mate_exam_lab,
		qt_glicemia_capilar,
		qt_medicao_altura,
		qt_medicao_peso,
		nr_seq_lote_envio,
		cd_uuid_original           
	from	esus_ficha_proc_profis
	where	nr_seq_lote_envio =	nr_seq_lote_p
	order by nr_sequencia;
	
Cursor Proc is
	select 	nr_sequencia,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_turno_atendimento,
		cd_pessoa_fisica,
		nr_prontuario,
		ie_local_atendimento,
		ie_escuta_ini_orient,
		ie_acupuntura,
		ie_admini_vitamina_a,
		ie_cateterismo_vesic,
		ie_cauteriza_quimica,
		ie_cirurgia_unha_can,
		ie_cuidado_estomas,
		ie_curativo_especial,
		ie_drenagem_abscesso,
		ie_eletrocardiograma,
		ie_coleta_citop_colo,
		ie_exame_pe_diabetic,
		ie_exe_bio_pun_tumor,
		ie_fundoscopia_olho,
		ie_inf_cavi_sinovial,
		ie_rem_corpo_aud_nas,
		ie_rem_corpo_subcuta,
		ie_retirada_cerume,
		ie_ret_pont_cirurgia,
		ie_sutura_simples,
		ie_triagem_oftalmolo,
		ie_tamponam_epistaxe,
		ie_test_rap_gravidez,
		ie_tr_dosag_proteinu,
		ie_teste_rapido_hiv,
		ie_te_rap_hepatite_c,
		ie_teste_rap_sifilis,
		ie_admin_medica_oral,
		ie_adm_med_intramusc,
		ie_adm_me_endovenosa,
		ie_adm_med_inal_nebu,
		ie_admin_medi_topica,
		ie_penicili_trat_sif,
		ie_adim_medic_subcutanea,
		nr_seq_ficha_pro_prof,
                dt_hora_inicio_atend,
                dt_hora_final_atend
	from	esus_ficha_procedimento
	where	nr_seq_ficha_pro_prof = nr_sequencia_w;
	
Cursor ProProc is
	select	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,		
		nm_usuario_nrec,
		nr_seq_ficha_proc,
		cd_procedimento,
		ie_origem_proced
	from	esus_ficha_pro_proced
	where	nr_seq_ficha_proc = nr_seq_ficha_proc_ww;

Proc_w				Proc%rowtype;
ProProc_w			ProProc%rowtype;
	
type 		fetch_array is table of ProcProf%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_ProcProf_w			vetor;

	
begin

delete 	from w_esus_ficha_proc_profis
where	nr_seq_lote_envio = nr_seq_lote_p;

delete 	from w_esus_ficha_procedimento
where	nr_seq_lote_envio = nr_seq_lote_p;

delete 	from w_esus_ficha_pro_proced
where	nr_seq_lote_envio = nr_seq_lote_p;

open ProcProf;
loop
fetch ProcProf bulk collect into s_array limit 1000;
	vetor_ProcProf_w(i) := s_array;
	i := i + 1;
exit when ProcProf%notfound;
end loop;
close ProcProf;


for i in 1..vetor_ProcProf_w.count loop
	begin
	s_array := vetor_ProcProf_w(i);
	for z in 1..s_array.count loop
		begin
		
		nr_sequencia_w				:= s_array(z).nr_sequencia;
		cd_estabelecimento_w			:= s_array(z).cd_estabelecimento;
		dt_atualizacao_w			:= s_array(z).dt_atualizacao;
		nm_usuario_w				:= s_array(z).nm_usuario;
		dt_atualizacao_nrec_w			:= s_array(z).dt_atualizacao_nrec;
		nm_usuario_nrec_w			:= s_array(z).nm_usuario_nrec;
		cd_profissional_w			:= s_array(z).cd_profissional;
		cd_cbo_w				:= s_array(z).cd_cbo;
		cd_cnes_unidade_w			:= s_array(z).cd_cnes_unidade;
		nr_seq_sus_equipe_w			:= s_array(z).nr_seq_sus_equipe;
		dt_atendimento_w			:= s_array(z).dt_atendimento;
		dt_liberacao_w				:= s_array(z).dt_liberacao;
		qt_afericao_pa_w			:= s_array(z).qt_afericao_pa;
		qt_afericao_temperat_w			:= s_array(z).qt_afericao_temperat;
		qt_curativo_simples_w			:= s_array(z).nr_sequencia;
		qt_col_mate_exam_lab_w			:= s_array(z).qt_col_mate_exam_lab;
		qt_glicemia_capilar_w			:= s_array(z).qt_glicemia_capilar;
		qt_medicao_altura_w			:= s_array(z).qt_medicao_altura;
		qt_medicao_peso_w			:= s_array(z).qt_medicao_peso;
		nr_seq_lote_envio_w			:= s_array(z).nr_seq_lote_envio;
		cd_uuid_original_w			:= s_array(z).cd_uuid_original;
			
		select	w_esus_ficha_proc_profis_seq.nextval
		into	nr_seq_exp_w
		from	dual;
		
		begin
			cd_cns_profissional_w := substr(obter_dados_pf(cd_profissional_w,'CNS'),1,20);		
		exception
		when others then
			cd_cns_profissional_w := '';
		end;
		
		select	nvl(max(cd_cnes_unidade),'0')
		into	cd_cnes_unidade_ww
		from	w_esus_header_footer
		where	nr_seq_lote_envio = nr_seq_lote_p;
		
		if	(cd_cnes_unidade_w <> cd_cnes_unidade_ww) then
			begin
			cd_contra_chave_rem_w	:= substr(sus_gerar_uuid_esus(cd_cnes_unidade_w,'C'),1,50);
			cd_uuid_instal_rem_w	:= substr(sus_gerar_uuid_esus(cd_cnes_unidade_w,'U'),1,50);
			cd_contra_chave_ori_w	:= cd_contra_chave_rem_w;
			cd_uuid_instal_orig_w	:= cd_uuid_instal_rem_w;			
			cd_municipio_ibge_ww	:= sus_obter_dados_equipe(nr_seq_sus_equipe_w,'CM');
			cd_municipio_ibge_ww	:= substr(cd_municipio_ibge_ww||Calcula_Digito('MODULO10',cd_municipio_ibge_ww),1,7);
			
			update 	w_esus_header_footer
			set 	cd_cnes_unidade = cd_cnes_unidade_w,
				cd_cnes_serealizado = cd_cnes_unidade_w,
				cd_contra_chave_rem = cd_contra_chave_rem_w,
				cd_uuid_instal_rem = cd_uuid_instal_rem_w,
				cd_contra_chave_ori = cd_contra_chave_ori_w,
				cd_uuid_instal_orig = cd_uuid_instal_orig_w,
				cd_municipio_ibge = cd_municipio_ibge_ww
			where	nr_seq_lote_envio = nr_seq_lote_p;
			
			end;
		end if;
		
		if	(nvl(cd_uuid_original_w,'X') = 'X')  then
			cd_uuid_original_w := nvl(sus_gerar_uuid_esus(cd_cnes_unidade_w,'U'),'0');
			update esus_ficha_proc_profis set cd_uuid_original = cd_uuid_original_w where  nr_sequencia = nr_sequencia_w;
		end if;			
		
		nr_data_atendimento_w := esus_converte_data(dt_atendimento_w);
		
		insert into w_esus_ficha_proc_profis (	nr_sequencia,
							cd_estabelecimento,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							cd_profissional,
							cd_cbo,
							cd_cnes_unidade,
							nr_seq_sus_equipe,
							dt_atendimento,
							qt_afericao_pa,
							qt_afericao_temperat,
							qt_curativo_simples,
							qt_col_mate_exam_lab,
							qt_glicemia_capilar,
							qt_medicao_altura,
							qt_medicao_peso,
							nr_seq_lote_envio,
							cd_uuid_original,
							nr_seq_ficha,
							cd_cns_profissional,
							nr_data_atendimento)
		values	(	nr_seq_exp_w,
				cd_estabelecimento_w,
				dt_atualizacao_w,
				nm_usuario_w,
				dt_atualizacao_nrec_w,
				nm_usuario_nrec_w,
				cd_profissional_w,
				cd_cbo_w,
				cd_cnes_unidade_w,
				nr_seq_sus_equipe_w,
				dt_atendimento_w,
				qt_afericao_pa_w,
				qt_afericao_temperat_w,
				qt_curativo_simples_w,
				qt_col_mate_exam_lab_w,
				qt_glicemia_capilar_w,
				qt_medicao_altura_w,
				qt_medicao_peso_w,
				nr_seq_lote_envio_w,
				cd_uuid_original_w,
				nr_sequencia_w,
				cd_cns_profissional_w,
				nr_data_atendimento_w);
				
			open Proc;
			loop	
			fetch Proc into Proc_w;
			exit when Proc%notfound;
			
				select 	w_esus_ficha_procedimento_seq.nextval 
				into	nr_seq_ficha_proc_w 
				from 	dual;
				
				if	(nvl(Proc_w.cd_pessoa_fisica,'X') <> 'X') then
					begin
						cd_cns_paciente_w		:= substr(obter_dados_pf(Proc_w.cd_pessoa_fisica,'CNS'),1,20);
						dt_nascimento_paciente_w	:= to_date(substr(obter_dados_pf(Proc_w.cd_pessoa_fisica,'DN'),1,10),'dd/mm/yyyy');
						ie_sexo_paciente_w		:= substr(sus_gerar_depara_esus(obter_dados_pf(Proc_w.cd_pessoa_fisica,'SE'),'SEXO'),1,20);
                                                cd_cpf_paciente_w               := substr(obter_dados_pf(Proc_w.cd_pessoa_fisica,'CPF'),1,20);
						if	(dt_nascimento_paciente_w is not null) then
							nr_data_nasc_pac_w := esus_converte_data(dt_nascimento_paciente_w);
						else
							nr_data_nasc_pac_w := null;
						end if;
                                                if	(proc_w.dt_hora_inicio_atend is not null) then
							nr_hora_inicio_atend_w := esus_converte_data(proc_w.dt_hora_inicio_atend);
						else
							nr_hora_inicio_atend_w := null;
						end if;
                                                if	(proc_w.dt_hora_final_atend is not null) then
							nr_hora_final_atend_w := esus_converte_data(proc_w.dt_hora_final_atend);
						else
							nr_hora_final_atend_w := null;
						end if;
						nr_prontuario_w := Proc_w.nr_prontuario;
						if	(nvl(nr_prontuario_w,'0') = '0') then
							nr_prontuario_w := to_number(obter_dados_pf(Proc_w.cd_pessoa_fisica,'NP'));
						end if;
					end;
				end if;
				
				nr_seq_ficha_proc_ww	:= Proc_w.nr_sequencia;
				
				insert into	w_esus_ficha_procedimento (nr_sequencia,
									cd_estabelecimento,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									ie_turno_atendimento,
									cd_pessoa_fisica,
									cd_cns_paciente,
									dt_nascimento_paciente,
									nr_data_nasc_pac,
									ie_sexo_paciente,
									nr_prontuario,
									ie_local_atendimento,
									ie_escuta_ini_orient,
									ie_acupuntura,
									ie_admini_vitamina_a,
									ie_cateterismo_vesic,
									ie_cauteriza_quimica,
									ie_cirurgia_unha_can,
									ie_cuidado_estomas,
									ie_curativo_especial,
									ie_drenagem_abscesso,
									ie_eletrocardiograma,
									ie_coleta_citop_colo,
									ie_exame_pe_diabetic,
									ie_exe_bio_pun_tumor,
									ie_fundoscopia_olho,
									ie_inf_cavi_sinovial,
									ie_rem_corpo_aud_nas,
									ie_rem_corpo_subcuta,
									ie_retirada_cerume,
									ie_ret_pont_cirurgia,
									ie_sutura_simples,
									ie_triagem_oftalmolo,
									ie_tamponam_epistaxe,
									ie_test_rap_gravidez,
									ie_tr_dosag_proteinu,
									ie_teste_rapido_hiv,
									ie_te_rap_hepatite_c,
									ie_teste_rap_sifilis,
									ie_admin_medica_oral,
									ie_adm_med_intramusc,
									ie_adm_me_endovenosa,
									ie_adm_med_inal_nebu,
									ie_admin_medi_topica,
									ie_penicili_trat_sif,
									ie_adim_medic_subcutanea,
									nr_seq_ficha_pro_prof,
									nr_seq_w_fic_pro_prof,
									nr_seq_lote_envio,
									nr_seq_ficha_proced,
                                                                        dt_hora_inicio_atend,
                                                                        dt_hora_final_atend,
                                                                        nr_hora_inicio_atend,
                                                                        nr_hora_final_atend,
                                                                        cd_cpf_paciente)
							values	(nr_seq_ficha_proc_w,
									Proc_w.cd_estabelecimento,
									Proc_w.dt_atualizacao,
									Proc_w.nm_usuario,
									Proc_w.dt_atualizacao_nrec,
									Proc_w.nm_usuario_nrec,
									Proc_w.ie_turno_atendimento,
									Proc_w.cd_pessoa_fisica,
									cd_cns_paciente_w,
									dt_nascimento_paciente_w,
									nr_data_nasc_pac_w,
									ie_sexo_paciente_w,
									nr_prontuario_w,
									Proc_w.ie_local_atendimento,
									decode(Proc_w.ie_escuta_ini_orient,'S','true','false'),
									decode(Proc_w.ie_acupuntura,'S','ABPG001',''),
									decode(Proc_w.ie_admini_vitamina_a,'S','ABPG002',''),
									decode(Proc_w.ie_cateterismo_vesic,'S','ABPG003',''),
									decode(Proc_w.ie_cauteriza_quimica,'S','ABPG004',''),
									decode(Proc_w.ie_cirurgia_unha_can,'S','ABPG005',''),
									decode(Proc_w.ie_cuidado_estomas,'S','ABPG006',''),
									decode(Proc_w.ie_curativo_especial,'S','ABPG007',''),
									decode(Proc_w.ie_drenagem_abscesso,'S','ABPG008',''),
									decode(Proc_w.ie_eletrocardiograma,'S','ABEX004',''),
									decode(Proc_w.ie_coleta_citop_colo,'S','ABPG010',''),
									decode(Proc_w.ie_exame_pe_diabetic,'S','ABPG011',''),
									decode(Proc_w.ie_exe_bio_pun_tumor,'S','ABPG012',''),
									decode(Proc_w.ie_fundoscopia_olho,'S','ABPG013',''),
									decode(Proc_w.ie_inf_cavi_sinovial,'S','ABPG014',''),
									decode(Proc_w.ie_rem_corpo_aud_nas,'S','ABPG015',''),
									decode(Proc_w.ie_rem_corpo_subcuta,'S','ABPG016',''),
									decode(Proc_w.ie_retirada_cerume,'S','ABPG017',''),
									decode(Proc_w.ie_ret_pont_cirurgia,'S','ABPG018',''),
									decode(Proc_w.ie_sutura_simples,'S','ABPG019',''),
									decode(Proc_w.ie_triagem_oftalmolo,'S','ABPG020',''),
									decode(Proc_w.ie_tamponam_epistaxe,'S','ABPG021',''),
									decode(Proc_w.ie_test_rap_gravidez,'S','ABPG022',''),
									decode(Proc_w.ie_tr_dosag_proteinu,'S','ABPG040',''),
									decode(Proc_w.ie_teste_rapido_hiv,'S','ABPG024',''),
									decode(Proc_w.ie_te_rap_hepatite_c,'S','ABPG025',''),
									decode(Proc_w.ie_teste_rap_sifilis,'S','ABPG026',''),
									decode(Proc_w.ie_admin_medica_oral,'S','ABPG027',''),
									decode(Proc_w.ie_adm_med_intramusc,'S','ABPG028',''),
									decode(Proc_w.ie_adm_me_endovenosa,'S','ABPG029',''),
									decode(Proc_w.ie_adm_med_inal_nebu,'S','ABPG030',''),
									decode(Proc_w.ie_admin_medi_topica,'S','ABPG031',''),
									decode(Proc_w.ie_penicili_trat_sif,'S','ABPG032',''),
									decode(Proc_w.ie_adim_medic_subcutanea,'S','ABPG041',''),
									nr_sequencia_w,
									nr_seq_exp_w,
									nr_seq_lote_envio_w,
									nr_seq_ficha_proc_ww,
                                                                        proc_w.dt_hora_inicio_atend,
                                                                        proc_w.dt_hora_final_atend,
                                                                        nr_hora_inicio_atend_w,
                                                                        nr_hora_final_atend_w,
                                                                        cd_cpf_paciente_w);
										
					open ProProc;
					loop
					fetch ProProc into ProProc_w;
					exit when ProProc%notfound;
						insert into w_esus_ficha_pro_proced (nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,		
										nm_usuario_nrec,
										nr_seq_ficha_proc,
										cd_procedimento,
										ie_origem_proced,
										nr_seq_w_ficha_proc,
										nr_seq_lote_envio)
								values (w_esus_ficha_pro_proced_seq.nextval,
										ProProc_w.dt_atualizacao,
										ProProc_w.nm_usuario,
										ProProc_w.dt_atualizacao_nrec,		
										ProProc_w.nm_usuario_nrec,
										Proc_w.nr_sequencia,
										ProProc_w.cd_procedimento,
										ProProc_w.ie_origem_proced,
										nr_seq_ficha_proc_w,
										nr_seq_lote_envio_w);
					end loop;
					close ProProc;
	
			end loop;
			close Proc;
		end;
	end loop;
	end;
end loop;

commit;

end sus_gerar_w_esus_procedi;
/
