CREATE OR REPLACE
PROCEDURE Baixar_Prescr_Pendente_alta(
			qt_hora_adic_mat_p	Number,
			qt_hora_adic_proc_p	Number,
			cd_motivo_baixa_p	Number,
			dt_parametro_p		date,
			nr_horas_alta_p		number) IS

nr_prescricao_w	number(10);

Cursor C01 is
Select	b.nr_prescricao
from	prescr_medica b,
	Atendimento_paciente a
where	a.nr_atendimento = b.nr_atendimento
and	a.DT_ALTA_INTERNO < dt_parametro_p
and	a.dt_alta + nr_horas_alta_p/24 < dt_parametro_p
and	rownum <= 1000;
	
BEGIN
open C01;
loop
fetch C01 into	
	nr_prescricao_w;
exit when C01%notfound;
	begin
	update	prescr_procedimento a
	set	cd_motivo_baixa = cd_motivo_baixa_p
	where	nr_prescricao = nr_prescricao_w
	and	cd_motivo_baixa = 0
	and	nvl(ie_status_atend,1) < 20 
	/*and	exists
		(select	1
		from	Procedimento c, 
			prescr_medica b
		where	a.nr_prescricao	= b.nr_prescricao
		and	(a.dt_prev_execucao + (nvl(c.qt_hora_baixar_prescr,qt_hora_adic_proc_p) / 24) < dt_parametro_p)
		and	(b.dt_prescricao + ((b.nr_horas_validade + nvl(c.qt_hora_baixar_prescr,qt_hora_adic_proc_p)) /24) < dt_parametro_p)
		and	c.cd_procedimento = a.cd_procedimento
		and	((nr_horas_alta_p = 0) or (obter_hora_entre_datas(Obter_data_alta_Atendimento(b.nr_atendimento), sysdate) > nr_horas_alta_p))
		and	c.ie_origem_proced = a.ie_origem_proced) */
	and not exists 
		(select	1 
		from	procedimento_paciente x
		where	x.nr_prescricao = a.nr_prescricao
		and	x.nr_sequencia_prescricao = a.nr_sequencia)
	and	rownum <= 1000;
	commit;

	update	prescr_material a
	set	cd_motivo_baixa = cd_motivo_baixa_p
	where	nr_prescricao = nr_prescricao_w
	and	cd_motivo_baixa = 0
	/*and exists
		(select	1
		from 	prescr_medica b
		where	a.nr_prescricao = b.nr_prescricao
		and	((nr_horas_alta_p = 0) or (obter_hora_entre_datas(Obter_data_alta_Atendimento(b.nr_atendimento), sysdate) > nr_horas_alta_p))
		and	(b.dt_prescricao + (qt_hora_adic_mat_p/24)) < dt_parametro_p)*/
	and not exists 
		(select	1
		from	conclusao_recom_apae w
		where	w.nr_prescricao = a.nr_prescricao)
	and	rownum <= 1000;
	/*OS95251 Identificado que n�o baixava os material quando atendido no ADEP antes da Farm�cia*/
	/*and	not exists(	select	1
				from	prescr_mat_hor c
				where	c.nr_prescricao		= a.nr_prescricao
				and	c.nr_seq_material	= a.nr_sequencia
				and	c.dt_fim_horario is not null);*/
	commit;
	end;
end loop;
close C01;
commit;

end Baixar_Prescr_Pendente_alta; 
/