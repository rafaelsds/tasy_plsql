create or replace
procedure liberar_paciente_medicamento(
				nr_sequencia_p	number,
				nm_usuario_p	varchar2) is

begin

update	paciente_medic_uso
set	dt_liberacao = sysdate,
	nm_usuario_liberacao	= nm_usuario_p
where	nr_sequencia		= nr_sequencia_p;

commit;

end	liberar_paciente_medicamento;
/