create or replace
procedure ctb_gravar_clob_demonstrativo(	nr_sequencia_p	ctb_demo_externo.nr_sequencia%type) is

ds_documento_w		clob;
ds_demonstrativo_w	long;

begin

select	a.ds_demonstrativo
into	ds_demonstrativo_w
from	ctb_demo_externo a
where	a.nr_sequencia		= nr_sequencia_p;

ds_documento_w	:= to_clob(ds_demonstrativo_w);

update	ctb_demo_externo
set	ds_documento	= ds_documento_w
where	nr_sequencia	= nr_sequencia_p;

commit;

end ctb_gravar_clob_demonstrativo;
/