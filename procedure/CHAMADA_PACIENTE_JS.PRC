create or replace
procedure chamada_paciente_js(	ie_opcao_p		Varchar2,
				ie_clinica_p		Varchar2,
				ie_regra_estab_p	Varchar2,
				cd_setores_p		Varchar2,
				nm_usuario_p		Varchar2,
				cd_lista_grupo_p    varchar2 default '',
				nr_atendimento_p out 	Number,
				cd_pessoa_fisica_p out  Varchar2,
				nm_pessoa_fisica_p out  Varchar,
				nm_medico_p	   out 	Varchar2,
				ds_breve_local_p   out  Varchar2) is 
				
dt_entrada_w		date;
nr_atendimento_w	number(10);
dt_chamada_pac_w	date;
cd_estabelecimento_w	number(4);
nm_pessoa_fisica_w	varchar2(255);
nm_medico_w		varchar2(60);
ds_local_w		varchar2(255);
cd_pessoa_fisica_w	varchar2(255);

cursor c01 is
select /*+ index (a atepaci_7) */
       a.dt_entrada,
       a.nr_atendimento,
       a.dt_chamada_paciente,
       a.cd_estabelecimento,
       substr(obter_nome_pf(p.cd_pessoa_fisica),1,255),
       substr(obter_nome_medico(a.cd_medico_resp,'N'),1,60) nm_medico,
       substr((obter_desc_abrev_local_pa(nr_seq_local_pa)||obter_texto_reav_painel_cham(a.nr_atendimento)),1,200) ds_breve_local,
       a.cd_pessoa_fisica
from   pessoa_fisica p,
       atendimento_paciente a
where  a.cd_pessoa_fisica      = p.cd_pessoa_fisica
and    a.dt_entrada            >= sysdate - 30
and    ((a.dt_chamada_paciente    is not null and a.dt_atend_medico            is null) or
       (a.dt_chamada_enfermagem  is not null and a.dt_inicio_atendimento      is null) or
       (a.dt_chamada_reavaliacao is not null and a.dt_inicio_reavaliacao      is null))
and    a.ie_chamado            is null
and	   (ie_clinica_p = '0' or a.ie_clinica in (ie_clinica_p))
and    (ie_regra_estab_p = '1' or a.cd_estabelecimento in (	select 	x.cd_estabelecimento    
									                        from 	usuario_estabelecimento_v x 
									                        where 	x.nm_usuario = nm_usuario_p))
and    (cd_setores_p is null or cd_setores_p = '' or obter_se_contido_char(obter_setor_atendimento(a.nr_atendimento),cd_setores_p) = 'S')
and    (cd_lista_grupo_p is null or cd_lista_grupo_p = '' or obter_se_contido(nr_seq_local_pa,cd_lista_grupo_p) = 'S')
order by a.dt_entrada; 

begin

open C01;
loop
fetch C01 into	
	dt_entrada_w,
	nr_atendimento_w,
	dt_chamada_pac_w,
	cd_estabelecimento_w,
	nm_pessoa_fisica_w,
	nm_medico_w,
	ds_local_w,
	cd_pessoa_fisica_w;
exit when C01%notfound;
	begin
	
	nr_atendimento_p   := nr_atendimento_w;
	cd_pessoa_fisica_p := cd_pessoa_fisica_w;
	nm_pessoa_fisica_p := nm_pessoa_fisica_w;
	nm_medico_p	   := nm_medico_w;
	ds_breve_local_p   := ds_local_w;	
	
	end;
end loop;
close C01;

if (nr_atendimento_w > 0) then
	update  atendimento_paciente
	set		ie_chamado = 'S'
	where 	nr_atendimento = nr_atendimento_w;
end if;

end chamada_paciente_js;
/
