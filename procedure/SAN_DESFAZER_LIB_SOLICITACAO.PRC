create or replace
procedure San_Desfazer_Lib_Solicitacao (nr_seq_solic_p	number) is

begin

update	san_reserva
set	dt_liberacao_solicitacao = null,
	ie_status = 'R'
where	nr_sequencia = nr_seq_solic_p;

commit;

end San_Desfazer_Lib_Solicitacao;
/