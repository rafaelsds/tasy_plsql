CREATE OR REPLACE
PROCEDURE Obter_Conta_Financeira(
				IE_ENTRADA_SAIDA_P		VARCHAR2,
				CD_ESTABELECIMENTO_P   		NUMBER,
				CD_MATERIAL_P			NUMBER,
				CD_PROCEDIMENTO_P		NUMBER,
				IE_ORIGEM_PROCED_P		NUMBER,
				CD_SETOR_ATEND_P        		NUMBER,
				CD_CONVENIO_P			NUMBER,
				CD_CGC_p			VARCHAR2,
				CD_CENTRO_CUSTO_P		NUMBER,
				NR_SEQ_CONTA_FINANC_P	OUT	NUMBER,
				ie_clinica_p			number,
				cd_operacao_nf_p			number,
				ie_tipo_pessoa_p			varchar2,
				ie_tipo_atendimento_p		number,
				cd_categoria_convenio_p		varchar2,
				nr_seq_proj_recurso_p		number,
				nr_seq_produto_p			number,
				cd_pessoa_fisica_p		varchar2,
				ie_origem_tit_rec_p		varchar2,
				ie_origem_tit_pag_p		varchar2,
				nr_seq_classe_tit_rec_p		number,
				nr_seq_classe_tit_pag_p		number,
				cd_local_estoque_p		number,
				nr_seq_trans_fin_p		number,
				ie_vago2_p			varchar2,
				ie_vago3_p			varchar2,
				ie_tipo_titulo_pagar_p		varchar2,
				ie_tipo_titulo_receber_p	varchar2,
				cd_moeda_p			number) IS

nr_seq_conta_financ_w		number(10)		:= 0;
cd_grupo_proc_w			number(15)		:= 0;
cd_espec_proc_w			number(15)		:= 0;
cd_area_proc_w			number(15)		:= 0;
cd_grupo_w			Number(03)		:= 0;
cd_subgrupo_w			number(03)		:= 0;	
cd_empresa_w			number(10)		:= 0;	
cd_classe_w			Number(05)		:= 0;
nr_seq_familia_w 	number(10)		:= 0;	
cd_tipo_pessoa_w		Number(03)		:= 0;
ie_complexidade_w		varchar2(2);
ie_tipo_financiamento_w		varchar2(4);
nr_seq_forma_org_w		number(10);
nr_seq_grupo_w			number(10);
nr_seq_subgrupo_w		number(10);
ie_tipo_convenio_w		number(2);

/* Edgar OS 9997 - inclui o cd_procedimento e cd_material  */
CURSOR C01 IS
select	a.nr_seq_conta_financ
from	conta_financeira b,
	conta_financ_regra a
where	a.nr_seq_conta_financ				= b.cd_conta_financ
and	nvl(a.cd_local_estoque,nvl(cd_local_estoque_p,0))	= nvl(cd_local_estoque_p,0)
and	nvl(a.ie_tipo_titulo_pagar,nvl(ie_tipo_titulo_pagar_p,'0'))	= nvl(ie_tipo_titulo_pagar_p,'0')
and	nvl(a.ie_tipo_titulo_receber,nvl(ie_tipo_titulo_receber_p,'0'))	= nvl(ie_tipo_titulo_receber_p,'0')
and	nvl(a.nr_seq_classe_tp,nvl(nr_seq_classe_tit_pag_p,0)) = nvl(nr_seq_classe_tit_pag_p,0)
and	nvl(a.nr_seq_classe_tr,nvl(nr_seq_classe_tit_rec_p,0)) = nvl(nr_seq_classe_tit_rec_p,0)
and	nvl(a.ie_origem_tit_rec,nvl(ie_origem_tit_rec_p,'0'))	= nvl(ie_origem_tit_rec_p,'0')
and	nvl(a.ie_origem_tit_pagar,nvl(ie_origem_tit_pag_p,'0'))	= nvl(ie_origem_tit_pag_p,'0')
and	nvl(a.ie_complexidade,nvl(ie_complexidade_w,'0'))	= nvl(ie_complexidade_w,'0')
and	nvl(a.ie_tipo_financiamento, nvl(ie_tipo_financiamento_w,'0'))	= nvl(ie_tipo_financiamento_w,'0')
and	nvl(a.nr_seq_forma_org,nvl(nr_seq_forma_org_w,0))	= nvl(nr_seq_forma_org_w,0)
and	nvl(a.nr_seq_grupo,nvl(nr_seq_grupo_w,0))		= nvl(nr_seq_grupo_w,0)
and	nvl(a.nr_seq_subgrupo,nvl(nr_seq_subgrupo_w,0))	= nvl(nr_seq_subgrupo_w,0)
and	nvl(a.cd_estabelecimento, cd_estabelecimento_p)		= cd_estabelecimento_p
and	a.ie_entrada_saida					= ie_entrada_saida_p
and	nvl(a.cd_area_procedimento,cd_area_proc_w)		= NVL(cd_area_proc_w, 0)
and	nvl(a.cd_especialidade,cd_espec_proc_w)		= NVL(cd_espec_proc_w, 0)
and	nvl(a.cd_grupo_proc,cd_grupo_proc_w)			= NVL(cd_grupo_proc_w, 0)
and	nvl(a.cd_procedimento, nvl(cd_procedimento_p, 0))	= nvl(cd_procedimento_p, 0)
and	nvl(a.ie_origem_proced, nvl(ie_origem_proced_p, 0))	= nvl(ie_origem_proced_p, 0)
and	nvl(a.cd_grupo_material,cd_grupo_w) 			= nvl(cd_grupo_w, 0)
and	nvl(a.cd_subgrupo_material,cd_subgrupo_w) 		= nvl(cd_subgrupo_w, 0)
and	nvl(a.cd_classe_material,cd_classe_w) 			= nvl(cd_classe_w, 0)
and	nvl(a.nr_seq_familia,nvl(nr_seq_familia_w, 0)) 		= nvl(nr_seq_familia_w, 0)
and	nvl(a.cd_material, nvl(cd_material_p, 0))			= nvl(cd_material_p, 0)
and	nvl(a.cd_convenio, nvl(cd_convenio_p,0))		= nvl(cd_convenio_p,0)
and	nvl(a.cd_cgc, nvl(cd_cgc_p,0))			= nvl(cd_cgc_p,0)
and	nvl(a.cd_tipo_pessoa, NVL(cd_tipo_pessoa_w,0))		= NVL(cd_tipo_pessoa_w,0)
and	nvl(a.cd_setor_atendimento, nvl(cd_setor_atend_p,0))	= nvl(cd_setor_atend_p,0)
and	nvl(a.cd_centro_custo,nvl(cd_centro_custo_p,0))		= nvl(cd_centro_custo_p,0)
and	nvl(a.ie_clinica, nvl(ie_clinica_p, 0)) 			= nvl(ie_clinica_p, 0)
and	nvl(a.cd_operacao_nf, nvl(cd_operacao_nf_p, 0)) 		= nvl(cd_operacao_nf_p, 0)
and	((a.ie_faturamento = 'N') or (cd_convenio_p is not null))
and	nvl(a.ie_tipo_atendimento, nvl(ie_tipo_atendimento_p, 0)) 	= nvl(ie_tipo_atendimento_p, 0)
and	b.ie_situacao					= 'A'
and	nvl(a.cd_categoria, nvl(cd_categoria_convenio_p, 0))	= nvl(cd_categoria_convenio_p, 0)
and	nvl(a.nr_seq_proj_rec,nvl(nr_seq_proj_recurso_p,0))	= nvl(nr_seq_proj_recurso_p,0)
and	nvl(a.ie_tipo_convenio,ie_tipo_convenio_w) = ie_tipo_convenio_w /* OS 178098 - 13/11/09 */
and	b.cd_empresa					= nvl(cd_empresa_w, b.cd_empresa)
and	nvl(a.nr_seq_produto, nvl(nr_seq_produto_p,0))		= nvl(nr_seq_produto_p,0)
and	nvl(a.cd_pessoa_fisica, nvl(cd_pessoa_fisica_p, '0'))	= nvl(cd_pessoa_fisica_p, '0')
and	nvl(a.ie_tipo_pessoa,nvl(ie_tipo_pessoa_p,'0'))	= nvl(ie_tipo_pessoa_p,'0')
and	nvl(a.nr_seq_trans_fin, nvl(nr_seq_trans_fin_p, 0)) = nvl(nr_seq_trans_fin_p,0)
and	nvl(a.cd_moeda, nvl(cd_moeda_p, 0)) 			= nvl(cd_moeda_p,0)
order by
	nvl(a.cd_procedimento, 0),
	nvl(a.ie_origem_proced, 0),
	nvl(a.cd_area_procedimento,0),
	nvl(a.cd_especialidade,0),
	nvl(a.cd_grupo_proc,0),
	nvl(a.cd_material, 0),
	nvl(a.nr_seq_familia,0),
	nvl(a.cd_classe_material,0),
	nvl(a.cd_subgrupo_material,0),
	nvl(a.cd_grupo_material,0),
  	nvl(a.cd_convenio, 0),
	nvl(a.ie_tipo_convenio,0),
   	nvl(a.cd_cgc, '0'),
   	nvl(a.cd_tipo_pessoa, 0),
	nvl(a.ie_tipo_pessoa,'0'),
	nvl(a.cd_setor_atendimento,0),
	nvl(a.cd_centro_custo,0),
	nvl(a.ie_clinica,0),
	nvl(a.cd_operacao_nf,0),
	nvl(a.cd_categoria, 0),
	nvl(a.nr_seq_proj_rec,0),
	nvl(a.nr_seq_produto, 0),
	nvl(a.cd_pessoa_fisica, 0),
	nvl(a.nr_seq_trans_fin, 0),
	nvl(a.ie_tipo_titulo_pagar, '0'),
	nvl(a.ie_tipo_titulo_receber, '0');

BEGIN



select	max(cd_empresa)
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

/* Obter Estrutura do Material */
if	(cd_material_p > 0) then  
	begin
	select 	cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
    nr_seq_familia
	into	cd_grupo_w,
		cd_subgrupo_w,
		cd_classe_w,
    nr_seq_familia_w
	from 	estrutura_material_v
	where	cd_material		= cd_material_p;  
	exception
     		when others then
			cd_grupo_w			:= 0;
	end;
end if;

/* Obter Estrutura do procedimento */
if	(cd_procedimento_p > 0) then
	begin

		select 	cd_grupo_proc,
			cd_especialidade,
			cd_area_procedimento
		into	cd_grupo_proc_w,
			cd_espec_proc_w,
			cd_area_proc_w
		from	Estrutura_Procedimento_V
		where	cd_procedimento 	= cd_procedimento_p
		and	ie_origem_proced	= ie_origem_proced_p;

	exception

		when others then
			cd_grupo_proc_w			:= 0;
	end;

	if	(ie_origem_proced_p = 7) then
		select	max(a.ie_complexidade),
			max(a.ie_tipo_financiamento),
			max(a.nr_seq_forma_org),
			max(a.nr_seq_grupo),
			max(a.nr_seq_subgrupo)
		into	ie_complexidade_w,
			ie_tipo_financiamento_w,
			nr_seq_forma_org_w,
			nr_seq_grupo_w,
			nr_seq_subgrupo_w
		from	sus_estrutura_procedimento_v a
		where	a.cd_procedimento	= cd_procedimento_p
		and	a.ie_origem_proced	= ie_origem_proced_p;
	end if;

end if;

/* Obter Tipo Pessoa Juridica */
if	(cd_cgc_p is not null) then
	select	cd_tipo_pessoa
	into	cd_tipo_pessoa_w
	from	pessoa_juridica
	where	cd_cgc	= cd_cgc_p;
end if;

select	nvl(max(a.ie_tipo_convenio),0)
into	ie_tipo_convenio_w
from	convenio a
where	a.cd_convenio	= cd_convenio_p;

OPEN C01;
LOOP
	FETCH C01 into
		nr_seq_conta_financ_w;
	exit when c01%notfound;
		nr_seq_conta_financ_w	:= nr_seq_conta_financ_w;
END LOOP;
CLOSE C01;

nr_seq_conta_financ_p			:= nr_seq_conta_financ_w;

END Obter_Conta_Financeira;
/