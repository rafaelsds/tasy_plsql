create or replace procedure atualizar_laudo_todos_imp	(nr_atendimento_p		number,
						nr_prescricao_p		number,
						nr_seq_prescricao_p	number,
						ie_mapa_laudo_p		varchar2,
						ie_tipo_imp_p		varchar2,
						cd_estabelecimento_p	number,
						nm_usuario_p		varchar2) is

nr_seq_presricao_w		number(6);
nr_prescricao_w		number(14);

Cursor C02 is
	select 	 b.nr_prescricao,
		 b.nr_sequencia
	from	 prescr_procedimento b,
	 	 prescr_medica a
	where 	 a.nr_atendimento  = nr_atendimento_p
	and	 a.nr_prescricao   = b.nr_prescricao
	and	 nvl(obter_se_motivo_baixa_conta(b.cd_motivo_baixa), 'S') = 'S'
	and	 obter_ds_status_result_exame(b.nr_prescricao, b.nr_sequencia, nm_usuario_p, cd_estabelecimento_p) <> 'Processando';
begin

if (nr_seq_prescricao_p is not null) then
	atualizar_lab_prescr_proc_imp(nr_prescricao_p, nr_seq_prescricao_p, ie_mapa_laudo_p, nm_usuario_p, ie_tipo_imp_p);
elsif  (nr_atendimento_p is not null) then
	open C02;
	loop
	fetch C02 into
		nr_prescricao_w,
		nr_seq_presricao_w;
	exit when C02%notfound;
		begin
			atualizar_lab_prescr_proc_imp(nr_prescricao_w, nr_seq_presricao_w, ie_mapa_laudo_p, nm_usuario_p, ie_tipo_imp_p);
		end;
	end loop;
	close C02;
end if;
commit;

end atualizar_laudo_todos_imp;
/