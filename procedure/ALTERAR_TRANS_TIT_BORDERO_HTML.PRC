create or replace
procedure alterar_trans_tit_bordero_html(	nr_bordero_p	number,
					nr_seq_trans_financ_p	number,
					nr_titulo_p				number,
					nm_usuario_p			varchar2,
					ie_sem_transacao_p		varchar2) is

begin

if	(nr_bordero_p is not null and nr_titulo_p is not null) then
	begin
	update	bordero_tit_pagar
	set	nr_seq_trans_financ	= nr_seq_trans_financ_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_bordero		= nr_bordero_p
	and	nr_titulo = nr_titulo_p
	and	((nvl(ie_sem_transacao_p,'N')	= 'S' 
		and	nr_seq_trans_financ is null)
		or (nvl(ie_sem_transacao_p,'N') = 'N'));
	end;
end if;

commit;

end alterar_trans_tit_bordero_html;
/
