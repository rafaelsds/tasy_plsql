create or replace
procedure pls_liberar_item_req_manual
			(	nr_seq_requisicao_p	number,
				nr_seq_proc_p		number,
				nr_seq_mat_p		number,
				ds_motivo_libercao_p	varchar2,
				nm_usuario_p		Varchar2) is 
				
ds_item_w			varchar2(255);
cd_item_w			number(20);
ie_origem_proced_p		number(3);
ds_texto_w			varchar2(4000);

begin

if	(nr_seq_proc_p is not null) then
	update	pls_requisicao_proc
	set	ie_status	= 'P',
		qt_procedimento	= qt_solicitado,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia 	= nr_seq_proc_p;
	
	select	cd_procedimento,
		ie_origem_proced
	into	cd_item_w,
		ie_origem_proced_p
	from	pls_requisicao_proc
	where	nr_sequencia 	= nr_seq_proc_p;
elsif	(nr_seq_mat_p is not null) then
	update	pls_requisicao_mat
	set	ie_status	= 'P',
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_sequencia	= nr_seq_mat_p;
	select	nr_seq_material,
		0
	into	cd_item_w,
		ie_origem_proced_p
	from	pls_requisicao_mat
	where	nr_sequencia 	= nr_seq_mat_p;
end if;


if	(nvl(cd_item_w,0) > 0) then
	if	(nvl(ie_origem_proced_p,0) > 0) then
		select	Obter_Descricao_Procedimento(cd_item_w,ie_origem_proced_p)
		into	ds_item_w
		from	dual;
	else
		select	pls_obter_desc_material(cd_item_w)
		into	ds_item_w
		from	dual;
	end if;
end if; 

ds_texto_w := 'O usu�rio '||nm_usuario_p||' liberou o item '||cd_item_w||' - '||ds_item_w||', pelo seguinte motivo: '||chr(13)||chr(13)||ds_motivo_libercao_p;

insert	into pls_requisicao_historico
	(nr_sequencia, dt_atualizacao_nrec, nm_usuario_nrec,
	dt_atualizacao, nm_usuario, ie_tipo_historico,
	ie_origem_historico, ds_historico, nr_seq_requisicao,
	dt_historico)
values	(pls_requisicao_historico_seq.nextval, sysdate, nm_usuario_p,
	sysdate, nm_usuario_p, 'M',
	'M',ds_texto_w, nr_seq_requisicao_p,
	sysdate);

commit;

end pls_liberar_item_req_manual;
/