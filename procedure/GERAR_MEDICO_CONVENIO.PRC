create or replace 
procedure	gerar_medico_convenio(
					cd_convenio_p		number,
					nr_sequencia_p		number,
					nm_usuario_p		varchar2) is
					
begin

if (cd_convenio_p is not null) then

	insert into medico_convenio (
		nr_sequencia,
		cd_pessoa_fisica,
		cd_convenio,
		dt_atualizacao,
		nm_usuario,
		ie_conveniado,
		ie_auditor,
		ie_plantonista,
		cd_estabelecimento,
		ie_tipo_servico_sus) 
	select	medico_convenio_seq.nextval,
		cd_pessoa_fisica,
		cd_convenio_p,
		sysdate,
		nm_usuario_p,
		ie_conveniado,
		ie_auditor,
		ie_plantonista,
		cd_estabelecimento,
		ie_tipo_servico_sus
	from	medico_convenio
	where	nr_sequencia = nr_sequencia_p;

	commit;	

end if;
		
end gerar_medico_convenio;
/