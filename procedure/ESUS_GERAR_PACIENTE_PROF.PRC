create or replace
procedure esus_gerar_paciente_prof(	cd_profissional_p		varchar2,
				cd_cbo_p			varchar2,
				dt_atendimento_p		date,
				cd_estabelecimento_p		number,
				ie_local_atendimento_p		varchar2,
				nr_seq_ficha_pro_prof_p		number,
				nm_usuario_p			usuario.nm_usuario%type) is 
					
nr_sequencia_w			esus_ficha_procedimento.nr_sequencia%type;
nr_atendimento_w			esus_ficha_procedimento.nr_atendimento%type;
nr_prontuario_w			esus_ficha_procedimento.nr_prontuario%type;
cd_pessoa_fisica_w		esus_ficha_procedimento.cd_pessoa_fisica%type;
ie_turno_atendimento_w		esus_ficha_procedimento.ie_turno_atendimento%type;
ie_acupuntura_w			esus_ficha_procedimento.ie_acupuntura%type;
ie_admini_vitamina_a_w		esus_ficha_procedimento.ie_admini_vitamina_a%type;
ie_admin_medica_oral_w		esus_ficha_procedimento.ie_admin_medica_oral%type;
ie_admin_medi_topica_w		esus_ficha_procedimento.ie_admin_medi_topica%type;
ie_adm_med_inal_nebu_w		esus_ficha_procedimento.ie_adm_med_inal_nebu%type;
ie_adm_med_intramusc_w		esus_ficha_procedimento.ie_adm_med_intramusc%type;
ie_adm_me_endovenosa_w		esus_ficha_procedimento.ie_adm_me_endovenosa%type;
ie_cateterismo_vesic_w		esus_ficha_procedimento.ie_cateterismo_vesic%type;
ie_cauteriza_quimica_w		esus_ficha_procedimento.ie_cauteriza_quimica%type;
ie_cirurgia_unha_can_w		esus_ficha_procedimento.ie_cirurgia_unha_can%type;
ie_coleta_citop_colo_w		esus_ficha_procedimento.ie_coleta_citop_colo%type;
ie_cuidado_estomas_w		esus_ficha_procedimento.ie_cuidado_estomas%type;
ie_curativo_especial_w		esus_ficha_procedimento.ie_curativo_especial%type;
ie_drenagem_abscesso_w		esus_ficha_procedimento.ie_drenagem_abscesso%type;
ie_eletrocardiograma_w		esus_ficha_procedimento.ie_eletrocardiograma%type;
ie_escuta_ini_orient_w		esus_ficha_procedimento.ie_escuta_ini_orient%type;
ie_exame_pe_diabetic_w		esus_ficha_procedimento.ie_exame_pe_diabetic%type;
ie_exe_bio_pun_tumor_w		esus_ficha_procedimento.ie_exe_bio_pun_tumor%type;
ie_fundoscopia_olho_w		esus_ficha_procedimento.ie_fundoscopia_olho%type;
ie_inf_cavi_sinovial_w		esus_ficha_procedimento.ie_inf_cavi_sinovial%type;
ie_penicili_trat_sif_w		esus_ficha_procedimento.ie_penicili_trat_sif%type;
ie_rem_corpo_aud_nas_w		esus_ficha_procedimento.ie_rem_corpo_aud_nas%type;
ie_rem_corpo_subcuta_w		esus_ficha_procedimento.ie_rem_corpo_subcuta%type;
ie_retirada_cerume_w		esus_ficha_procedimento.ie_retirada_cerume%type;	
ie_ret_pont_cirurgia_w		esus_ficha_procedimento.ie_ret_pont_cirurgia%type;
ie_sutura_simples_w		esus_ficha_procedimento.ie_sutura_simples%type;
ie_tamponam_epistaxe_w		esus_ficha_procedimento.ie_tamponam_epistaxe%type;
ie_te_rap_hepatite_c_w		esus_ficha_procedimento.ie_te_rap_hepatite_c%type;
ie_teste_rapido_hiv_w		esus_ficha_procedimento.ie_teste_rapido_hiv%type;
ie_teste_rap_sifilis_w		esus_ficha_procedimento.ie_teste_rap_sifilis%type;
ie_test_rap_gravidez_w		esus_ficha_procedimento.ie_test_rap_gravidez%type;
ie_tr_dosag_proteinu_w		esus_ficha_procedimento.ie_tr_dosag_proteinu%type;
ie_triagem_oftalmolo_w		esus_ficha_procedimento.ie_triagem_oftalmolo%type;
dt_entrada_w			atendimento_paciente.dt_entrada%type;
ds_exame_pac_w			varchar2(2000) := '';
qt_atend_gerado_w		number(10) := 0;
					
Cursor C01 is
	select	a.nr_atendimento,
		a.dt_entrada
	from	atendimento_paciente a,
		conta_paciente c,
		procedimento_paciente b
	where	c.nr_atendimento = a.nr_atendimento
	and	b.nr_interno_conta = c.nr_interno_conta
	and	trunc(b.dt_procedimento) = trunc(dt_atendimento_p)
	and	b.cd_medico_executor = cd_profissional_p
	and	b.cd_cbo = cd_cbo_p
	and	b.cd_motivo_exc_conta is null
	group by	a.nr_atendimento,
		a.dt_entrada;

begin

open C01;
loop
fetch C01 into	
	nr_atendimento_w,
	dt_entrada_w;
exit when C01%notfound;
	begin
	
	select	count(1)
	into	qt_atend_gerado_w
	from	esus_ficha_procedimento
	where	nr_seq_ficha_pro_prof	= nr_seq_ficha_pro_prof_p
	and	nr_atendimento		= nr_atendimento_w;
	
	if	(qt_atend_gerado_w  = 0) then
		begin
		
		cd_pessoa_fisica_w 	:= obter_pessoa_atendimento(nr_atendimento_w,'C');
		nr_prontuario_w		:= obter_prontuario_atendimento(nr_atendimento_w);
		ie_turno_atendimento_w	:= substr(esus_obter_turno_data(dt_entrada_w),1,1);
		ds_exame_pac_w		:= substr(esus_obter_exames_paciente(cd_profissional_p,cd_cbo_p,nr_atendimento_w,dt_atendimento_p),1,2000);
		
		if	(instr(ds_exame_pac_w,',1,') > 0) then
			ie_acupuntura_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',2,') > 0) then
			ie_admini_vitamina_a_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',3,') > 0) then
			ie_admin_medica_oral_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',4,') > 0) then
			ie_admin_medi_topica_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',5,') > 0) then
			ie_adm_med_inal_nebu_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',6,') > 0) then
			ie_adm_med_intramusc_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',7,') > 0) then
			ie_adm_me_endovenosa_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',8,') > 0) then
			ie_cateterismo_vesic_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',9,') > 0) then
			ie_cauteriza_quimica_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',10,') > 0) then
			ie_cirurgia_unha_can_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',11,') > 0) then
			ie_coleta_citop_colo_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',12,') > 0) then
			ie_cuidado_estomas_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',13,') > 0) then
			ie_curativo_especial_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',14,') > 0) then
			ie_drenagem_abscesso_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',15,') > 0) then
			ie_eletrocardiograma_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',16,') > 0) then
			ie_escuta_ini_orient_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',17,') > 0) then
			ie_exame_pe_diabetic_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',18,') > 0) then
			ie_exe_bio_pun_tumor_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',19,') > 0) then
			ie_fundoscopia_olho_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',20,') > 0) then
			ie_inf_cavi_sinovial_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',21,') > 0) then
			ie_penicili_trat_sif_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',22,') > 0) then
			ie_rem_corpo_aud_nas_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',23,') > 0) then
			ie_rem_corpo_subcuta_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',24,') > 0) then
			ie_retirada_cerume_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',25,') > 0) then
			ie_ret_pont_cirurgia_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',26,') > 0) then
			ie_sutura_simples_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',27,') > 0) then
			ie_tamponam_epistaxe_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',28,') > 0) then
			ie_te_rap_hepatite_c_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',29,') > 0) then
			ie_teste_rapido_hiv_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',30,') > 0) then
			ie_teste_rap_sifilis_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',31,') > 0) then
			ie_test_rap_gravidez_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',32,') > 0) then
			ie_tr_dosag_proteinu_w := 'S';
		end if;	
		if	(instr(ds_exame_pac_w,',33,') > 0) then
			ie_triagem_oftalmolo_w := 'S';
		end if;
		
		select	esus_ficha_procedimento_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		insert into esus_ficha_procedimento (
				nr_sequencia,
				nr_seq_ficha_pro_prof,
				nr_atendimento,
				nr_prontuario,
				cd_pessoa_fisica,
				ie_turno_atendimento,
				cd_estabelecimento,
				nm_usuario,
				nm_usuario_nrec,
				dt_atualizacao,
				dt_atualizacao_nrec,
				ie_acupuntura,
				ie_admini_vitamina_a,
				ie_admin_medica_oral,
				ie_admin_medi_topica,
				ie_adm_med_inal_nebu,
				ie_adm_med_intramusc,
				ie_adm_me_endovenosa,
				ie_cateterismo_vesic,
				ie_cauteriza_quimica,
				ie_cirurgia_unha_can,
				ie_coleta_citop_colo,
				ie_cuidado_estomas,
				ie_curativo_especial,
				ie_drenagem_abscesso,
				ie_eletrocardiograma,
				ie_escuta_ini_orient,
				ie_exame_pe_diabetic,
				ie_exe_bio_pun_tumor,
				ie_fundoscopia_olho,
				ie_inf_cavi_sinovial,
				ie_local_atendimento,
				ie_penicili_trat_sif,
				ie_rem_corpo_aud_nas,
				ie_rem_corpo_subcuta,
				ie_retirada_cerume,
				ie_ret_pont_cirurgia,
				ie_sutura_simples,
				ie_tamponam_epistaxe,
				ie_te_rap_hepatite_c,
				ie_teste_rapido_hiv,
				ie_teste_rap_sifilis,
				ie_test_rap_gravidez,
				ie_tr_dosag_proteinu,
				ie_triagem_oftalmolo)
		values(		nr_sequencia_w,
				nr_seq_ficha_pro_prof_p,
				nr_atendimento_w,
				nr_prontuario_w,
				cd_pessoa_fisica_w,
				ie_turno_atendimento_w,
				cd_estabelecimento_p,
				nm_usuario_p,
				nm_usuario_p,
				sysdate,
				sysdate,
				ie_acupuntura_w,
				ie_admini_vitamina_a_w,
				ie_admin_medica_oral_w,
				ie_admin_medi_topica_w,
				ie_adm_med_inal_nebu_w,
				ie_adm_med_intramusc_w,
				ie_adm_me_endovenosa_w,
				ie_cateterismo_vesic_w,
				ie_cauteriza_quimica_w,
				ie_cirurgia_unha_can_w,
				ie_coleta_citop_colo_w,
				ie_cuidado_estomas_w,
				ie_curativo_especial_w,
				ie_drenagem_abscesso_w,
				ie_eletrocardiograma_w,
				ie_escuta_ini_orient_w,
				ie_exame_pe_diabetic_w,
				ie_exe_bio_pun_tumor_w,
				ie_fundoscopia_olho_w,
				ie_inf_cavi_sinovial_w,
				ie_local_atendimento_p,
				ie_penicili_trat_sif_w,
				ie_rem_corpo_aud_nas_w,
				ie_rem_corpo_subcuta_w,
				ie_retirada_cerume_w,
				ie_ret_pont_cirurgia_w,
				ie_sutura_simples_w,
				ie_tamponam_epistaxe_w,
				ie_te_rap_hepatite_c_w,
				ie_teste_rapido_hiv_w,
				ie_teste_rap_sifilis_w,
				ie_test_rap_gravidez_w,
				ie_tr_dosag_proteinu_w,
				ie_triagem_oftalmolo_w);
				
		esus_gerar_proced_pac_prof (cd_profissional_p,cd_cbo_p,nr_atendimento_w,dt_atendimento_p,nr_sequencia_w,nm_usuario_p);
		
		end;
	end if;	
	
	end;
end loop;
close C01;

commit;

end esus_gerar_paciente_prof;
/
