create or replace
procedure apae_liberar_escala(	nr_sequencia_p	number,
				ie_opcao_p	number) is
begin

if	(nr_sequencia_p is not null) then
	begin
	if	(ie_opcao_p = 6) then	
		update	escala_risco_pulmonar
		set	dt_liberacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	elsif	(ie_opcao_p = 7) then		
		update	escala_risco_insuf_renal
		set	dt_liberacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	elsif	(ie_opcao_p = 8)then
		update	escala_child_pugh
		set	dt_liberacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	elsif	(ie_opcao_p = 9)then
		update	escala_risco_delirium
		set	dt_liberacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	elsif	(ie_opcao_p = 3)then 
		update	pac_clereance_creatinina
		set	dt_liberacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	elsif	(ie_opcao_p = 2)then 
		update	ESCALA_POSSUM
		set	dt_liberacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	elsif	(ie_opcao_p = 1)then 
		update	ESCALA_LEE
		set	dt_liberacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	elsif 	(ie_opcao_p = 10) then
		update	ESCALA_EIF
		set		dt_liberacao = sysdate
		where	nr_sequencia = nr_sequencia_p;
	end if;
	
	commit;
	end;
end if;

end apae_liberar_escala;
/
