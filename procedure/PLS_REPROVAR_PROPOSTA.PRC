create or replace
procedure pls_reprovar_proposta
			(	nr_seq_proposta_p		number,
				cd_motivo_reprovacao_p		number,
				nr_seq_motivo_rej_incl_p	number default null,	
				ds_observacao_p			varchar2 default null,
				nm_usuario_p			varchar2) is

nr_seq_beneficiario_w		pls_proposta_beneficiario.nr_sequencia%type;
nr_seq_inclusao_benef_w		pls_inclusao_beneficiario.nr_sequencia%type;
ie_rejeitar_solic_incl_w	varchar2(1);
				
Cursor C01 is
	select	nr_sequencia,
		nr_seq_inclusao_benef
	from	pls_proposta_beneficiario
	where	nr_seq_proposta	= nr_seq_proposta_p;

begin

ie_rejeitar_solic_incl_w := obter_valor_param_usuario(1232, 116, Obter_Perfil_Ativo, nm_usuario_p, obter_estabelecimento_ativo);

update	pls_proposta_adesao
set	ie_status		= 'R',
	cd_motivo_reprovacao	= cd_motivo_reprovacao_p,
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate,
	dt_reprovacao		= sysdate,
	ds_observacao		= ds_observacao_p
where	nr_sequencia		= nr_seq_proposta_p;

/*aaschlote 02/05/2011 OS - 314422 - Gerar historico para o benefici�rio*/
open C01;
loop
fetch C01 into
	nr_seq_beneficiario_w,
	nr_seq_inclusao_benef_w;
exit when C01%notfound;
	begin
	
	pls_gravar_histor_prop_benef(nr_seq_beneficiario_w,sysdate,'6','Reprova��o da proposta de ades�o',nm_usuario_p);
		
	if	(nr_seq_inclusao_benef_w is not null) then
		if	(nvl(ie_rejeitar_solic_incl_w,'N') = 'S') then
			pls_reprovar_inclusao_benef(nr_seq_inclusao_benef_w, nr_seq_motivo_rej_incl_p, ds_observacao_p, 'T', null, nm_usuario_p);
			pls_gerar_comunic_externo_web(nr_seq_inclusao_benef_w, '1', nm_usuario_p);
		end if;
	end if;
	end;
end loop;
close C01;

commit;

end pls_reprovar_proposta;
/
