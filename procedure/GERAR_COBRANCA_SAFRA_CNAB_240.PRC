create or replace
procedure gerar_cobranca_safra_cnab_240(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is
						
ds_conteudo_w			varchar2(240);
ds_brancos_205_w			varchar2(205);
ds_brancos_165_w			varchar2(165);
nm_empresa_w			varchar2(80);
nm_pessoa_w			varchar2(80);
ds_brancos_54_w			varchar2(54);
ds_endereco_w			varchar2(40);
ds_bairro_w			varchar2(40);
ds_brancos_80_w			varchar2(80);
ds_banco_w			varchar2(30);
ds_brancos_25_w			varchar2(25);
cd_convenio_banco_w		varchar2(20);
ds_cidade_w			varchar2(20);
ds_nosso_numero_w		varchar2(20);
ds_complemento_w			varchar2(18);
vl_juros_w			varchar2(17);
ds_brancos_17_w			varchar2(17);
nr_conta_corrente_w		varchar2(15);
dt_remessa_retorno_w		varchar2(14);
dt_geracao_arquivo_w		varchar2(14);
nr_inscricao_w			varchar2(15);
vl_liquidacao_w			varchar2(15);
vl_cobranca_w			varchar2(15);
vl_desconto_w			varchar2(15);
dt_desconto_w			varchar2(8);
vl_mora_w			varchar2(15);
nr_seq_registro_w			number(10);
ds_brancos_10_w			varchar2(10);
qt_reg_lote_w			varchar2(10);
dt_vencimento_w			varchar2(8);
dt_emissao_w			varchar2(8);
dt_pagamento_w			varchar2(8);
cd_cep_w			varchar2(8);
nr_seq_lote_w			number(10);
nr_seq_arquivo_w			varchar2(6);
nr_seq_envio_w			varchar2(8);
cd_agencia_bancaria_w		varchar2(5);
ie_tipo_inscricao_w			varchar2(5);
nr_endereco_w			varchar2(5);
cd_banco_w			varchar2(3);
ie_emissao_bloqueto_w		varchar2(3);
nr_digito_agencia_w		varchar2(2);
sg_uf_w				varchar2(2);
ie_protesto_w			varchar2(2);
ie_tipo_registro_w			varchar2(1);
qt_reg_arquivo_w			number(10) := 1;
ds_brancos_69_w			varchar2(69);
ds_brancos_33_w			varchar2(33);
dt_gravacao_w			varchar2(8);
nr_nosso_numero_w		varchar2(20);
nr_seq_carteira_cobr_w		varchar2(1);
nr_documento_cobr_w		varchar2(15);
nr_titulo_w			varchar2(25);
nm_pessoa_titulo_w		varchar2(40);
sg_estado_w			varchar2(15);
nr_seq_apres_w			number(10)	:= 1;
ds_brancos_9_w			varchar2(9);
ds_brancos_2_w			varchar2(2);
ds_zeros_23_w			varchar2(23);
ds_brancos_8_w			varchar2(8);
ds_brancos_55_w			varchar2(55);
ds_brancos_28_w			varchar2(28);
ds_brancos_119_w			varchar2(119);
ie_codigo_desconto_w		varchar2(1);
nr_seq_contrato_w			number(10);
nr_seq_mensalidade_w		number(10);
cd_mov_remessa_w		varchar2(2);
qt_registro_lote_w			varchar2(6) := 0;
vl_juros_acobrar_w			varchar2(15);
nr_seq_total_linhas_w		number(10);

/* Mensagens */
nr_seq_mensagem_w		number(10);
pr_juros_w			varchar2(255);
pr_multa_w			varchar2(255);
dt_mensalidade_w			varchar2(255);
nr_contrato_w			varchar2(255);
ds_plano_w			varchar2(255);
nr_protocolo_ans_w		varchar2(255);
dt_proximo_reajuste_w		varchar2(255);
cd_usuario_plano_w		varchar2(255);
dt_contratacao_w			varchar2(255);
vl_mensalidade_w			varchar2(255);
vl_coparticipacao_w		varchar2(255);
cd_pessoa_fisica_w		varchar2(255);
dt_solicitacao_w			varchar2(255);
dt_resposta_w			varchar2(255);
vl_outros_n_w			number(15,2);
vl_outros_w			varchar2(255)	:= null;
vl_total_w			varchar2(255)	:= null;
ie_adiciona_linhas_w		varchar2(255)	:= 'N';
nr_seq_plano_w			number(15);
nr_seq_segurado_w		number(15);
nr_seq_segurado_mens_w		number(15);
qt_itens_w			number(15)	:= 0;
nr_seq_pagador_w			number(15);
qt_coparticipacao_w		number(15);
nr_seq_mensalidade_seg_w		number(10);
vl_item_w				number(15,2);
dt_reajuste_prox_w			date;
dt_ref_mensalidade_w		date;
dt_reajuste_w			date;
dt_remessa_retorno_w		date;
cd_cgc_estip_w			varchar2(14);
nr_linha_w			number(10) := 0;
ds_mensagem_w			varchar2(100);

cursor C01 is
	select	lpad(c.cd_banco,3,'0') cd_banco,
		substr(a.nr_sequencia,1,5) nr_seq_envio,
		'3' ie_tipo_registro,
		lpad(somente_numero(to_char(c.vl_liquidacao,'9999999999990.00')),15,'0') vl_liquidacao,
		rpad('BRADESCO',30,' ') nm_cedente,
		to_char(b.dt_pagamento_previsto,'ddmmyyyy') dt_vencimento,
		lpad(somente_numero(to_char(c.vl_cobranca,'0000000000000.00')),15,'0') vl_cobranca,
		lpad(somente_numero(to_char(c.vl_desconto,'0000000000000.00')),15,'0') vl_desconto,
		decode(nvl(c.vl_desconto,0),0,'00000000',to_char(sysdate,'ddmmyyyy')) dt_desconto,
		lpad(somente_numero(to_char(nvl(c.vl_juros,0) + nvl(c.vl_multa,0),'9999999999990.00')),15,'0') vl_mora,
		to_char(c.dt_liquidacao,'ddmmyyyy') dt_pagamento,
		rpad(nvl(b.nr_nosso_numero,' '),11,' ') ds_nosso_numero,
		lpad(substr(nvl(c.cd_agencia_bancaria,'0'),1,5),5,'0') cd_agencia_bancaria,
		rpad(nvl(calcula_digito('Modulo11',nvl(c.cd_agencia_bancaria,'0')),'0'),1,'0') nr_digito_agencia,
		lpad(substr(nvl(e.cd_conta,'0'),1,12),12,'0') || rpad(substr(nvl(e.ie_digito_conta,'0'),1,1),1,'0') nr_conta_corrente,
		rpad(nvl(b.nr_nosso_numero,' '),20,' ') nr_nosso_numero,
		lpad(nvl(b.nr_seq_carteira_cobr,0),1,'1') nr_seq_carteira_cobr,
		rpad(a.nr_sequencia,15,' ') nr_documento_cobr,
		to_char(b.dt_emissao,'ddmmyyyy') dt_emissao,
		b.nr_titulo,
		decode(b.cd_pessoa_fisica,null,'2','1') ie_tipo_inscricao,
		lpad(decode(b.cd_pessoa_fisica,null,b.cd_cgc,(	select	x.nr_cpf
								from	pessoa_fisica x
								where	x.cd_pessoa_fisica	= b.cd_pessoa_fisica)),15,'0') nr_inscricao,
		rpad(upper(nvl(substr(obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc),1,40),' ')),40,' ') nm_pessoa_titulo,
		rpad(upper(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'E'),1,40),' ')),40,' ') ds_endereco,
		rpad(upper(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'B'),1,15),' ')),15,' ') ds_bairro,
		lpad(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CEP'),1,8),' '),8,'0') cd_cep,
		rpad(upper(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'CI'),1,15),' ')),15,' ') ds_cidade,
		rpad(upper(nvl(substr(obter_dados_pf_pj(b.cd_pessoa_fisica,b.cd_cgc,'UF'),1,2),' ')),2,' ') sg_estado,
		decode(nvl(c.vl_desconto,0),0,'0','1') ie_codigo_desconto,
		k.nr_sequencia,
		f.nr_seq_contrato,
		lpad(nvl(substr(c.cd_ocorrencia,1,2),'01'),2,'0') cd_mov_remessa,
		campo_mascara_virgula_casas(obter_dados_titulo_receber(b.nr_titulo,'PDJ'),3) pr_juros_diario, 
		campo_mascara_virgula(obter_dados_titulo_receber(b.nr_titulo,'TXM')) pr_multa_diario
	from	titulo_receber_v b,
		titulo_receber_cobr c,
		cobranca_escritural a,
		banco_carteira d,
		banco_estabelecimento e,
		pls_contrato_pagador	f,
		pls_mensalidade		k
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= e.nr_sequencia
	and	b.nr_seq_mensalidade	= k.nr_sequencia(+)
	and	k.nr_seq_pagador	= f.nr_sequencia(+)
	and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
Cursor C02 is
	select	campo_mascara_virgula(nvl(a.vl_mensalidade - a.vl_coparticipacao - a.vl_outros,0)) vl_mensalidade,
		campo_mascara_virgula(nvl(a.vl_coparticipacao,0)) vl_coparticipacao,
		a.nr_seq_segurado
	from	pls_mensalidade_segurado 	a
	where	a.nr_seq_mensalidade 	= nr_seq_mensalidade_w
	and	rownum 			<= 9
	order by a.nr_sequencia;

begin

delete from w_envio_banco where nm_usuario = nm_usuario_p;

select	lpad(' ',17,' '),
	lpad(' ',10,' '),
	lpad(' ',54,' '),
	lpad(' ',80,' '),
	lpad(' ',25,' '),
	lpad(' ',165,' '),
	lpad(' ',205,' '),
	lpad(' ',69,' '),
	lpad(' ',33,' '),
	lpad(' ',9,' '),
	lpad(' ',2,' '),
	lpad('0',23,'0'),
	lpad(' ',8,' '),
	lpad(' ',55,' '),
	lpad(' ',28,' '),
	lpad(' ',119,' ')
into	ds_brancos_17_w,
	ds_brancos_10_w,
	ds_brancos_54_w,
	ds_brancos_80_w,
	ds_brancos_25_w,
	ds_brancos_165_w,
	ds_brancos_205_w,
	ds_brancos_69_w,
	ds_brancos_33_w,
	ds_brancos_9_w,
	ds_brancos_2_w,
	ds_zeros_23_w,
	ds_brancos_8_w,
	ds_brancos_55_w,
	ds_brancos_28_w,
	ds_brancos_119_w
from	dual;

/* Header Arquivo*/
select	'0' ie_tipo_registro,
	lpad(c.cd_banco,3,'0') cd_banco,
	to_char(sysdate,'DDMMYYYYHHMISS') dt_geracao_arquivo,
	rpad('BANCO SAFRA S/A',30,' ') ds_banco,
	lpad(c.cd_agencia_bancaria,5,'0') cd_agencia_bancaria,
	rpad(calcula_digito('Modulo11',c.cd_agencia_bancaria),1,'0') nr_digito_agencia,
	lpad(c.cd_conta,12,'0') || rpad(c.ie_digito_conta,1,'0') nr_conta_corrente,
	rpad(upper(substr(obter_razao_social(b.cd_cgc),1,30)),30,' ') nm_empresa,
	lpad(b.cd_cgc,14,'0') nr_inscricao,
	substr(nvl(d.cd_convenio_banco,c.cd_convenio_banco),1,20) cd_convenio_banco,
	lpad(a.nr_sequencia,6,'0')
into	ie_tipo_registro_w,
	cd_banco_w,
	dt_geracao_arquivo_w,
	ds_banco_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nm_empresa_w,
	nr_inscricao_w,
	cd_convenio_banco_w,
	nr_seq_arquivo_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

ds_conteudo_w	:=	cd_banco_w || --1 3
			'0000' || --4 7
			'0'	|| -- 8 8
			rpad(' ',9,' ') || --9 17
			'2' || --18 18
			nr_inscricao_w || --19 32
			rpad(' ',20,' ') || --33 52
			cd_agencia_bancaria_w || --53 57
			nr_digito_agencia_w || -- 58 58
			nr_conta_corrente_w || --59 71
			' ' || --72 72
			nm_empresa_w || --73 102
			ds_banco_w || --103 132
			ds_brancos_10_w || --133 142
			'1' || --143 143
			dt_geracao_arquivo_w || --144 157
			lpad(qt_reg_arquivo_w,6,'0') || --158 163
			'087' || --164 166
			'01600' || --167 171
			ds_brancos_69_w; --172 240

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);

qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
qt_registro_lote_w	:= qt_registro_lote_w + 1;
/* Fim Header Arquivo */

/* Header Lote */
select	'1' ie_tipo_registro,
	lpad(c.cd_banco,3,'0') cd_banco,
	lpad(a.nr_sequencia,8,'0') nr_seq_envio,
	lpad(c.cd_agencia_bancaria,5,'0') cd_agencia_bancaria,
	rpad(calcula_digito('Modulo11',c.cd_agencia_bancaria),1,'0') nr_digito_agencia,
	lpad(c.cd_conta,12,'0') || rpad(c.ie_digito_conta,1,'0') nr_conta_corrente,
	rpad(upper(substr(obter_razao_social(b.cd_cgc),1,30)),30,' ') nm_empresa,
	lpad(b.cd_cgc,15,'0') nr_inscricao,
	nvl(d.cd_convenio_banco,c.cd_convenio_banco) cd_convenio_banco,
	rpad(a.nr_sequencia,6,'0'),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'EN'),40,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'NR'),5,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'CM'),18,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'MU'),20,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'CEP'),8,' '),
	rpad(obter_dados_pf_pj(null, b.cd_cgc,'UF'),2,' '),
	to_char(sysdate,'DDMMYYYY') dt_gravacao
into	ie_tipo_registro_w,
	cd_banco_w,
	nr_seq_envio_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nm_empresa_w,
	nr_inscricao_w,
	cd_convenio_banco_w,
	nr_seq_arquivo_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_cidade_w,
	cd_cep_w,
	sg_uf_w,
	dt_gravacao_w
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a,
	banco_carteira d
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_seq_carteira_cobr 	= d.nr_sequencia(+)
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

nr_seq_lote_w	:= 1;
nr_seq_total_linhas_w	:= nvl(nr_seq_total_linhas_w,0) + 1;

ds_conteudo_w	:= 	cd_banco_w || --1 3
					lpad(nvl(nr_seq_lote_w,0),4,'0') || --4 7
					'1' || --8 8
					'R' || --9 9
					'01' || --10 11
					rpad(' ',2,' ') || --12 13
					'045' || --14 16
					' ' || --17 17
					'2' || --18 18
					nr_inscricao_w || --19 33	
					rpad(' ',20,' ') || --34 53
					cd_agencia_bancaria_w || --cd_agencia_bancaria_w || --54 58
					nr_digito_agencia_w || --59 59
					nr_conta_corrente_w || --60 72
					' ' || --73 73
					nm_empresa_w || --74 103
					ds_brancos_80_w || --104 183
					lpad(nvl(nr_seq_cobr_escrit_p,'0'),8,'0')|| --184 191
					substr(dt_geracao_arquivo_w,1,8) || --192 199
					rpad(' ',8,' ') || --200 207
					ds_brancos_33_w; --208 240

nr_seq_apres_w	:= nr_seq_apres_w + 1;

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
		
qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
/* Fim Header Lote */

/* Detalhe */
open C01;
loop
fetch C01 into	
	cd_banco_w,
	nr_seq_envio_w,
	ie_tipo_registro_w,
	vl_liquidacao_w,
	ds_banco_w,
	dt_vencimento_w,
	vl_cobranca_w,
	vl_desconto_w,
	dt_desconto_w,
	vl_mora_w,
	dt_pagamento_w,
	ds_nosso_numero_w,
	cd_agencia_bancaria_w,
	nr_digito_agencia_w,
	nr_conta_corrente_w,
	nr_nosso_numero_w,
	nr_seq_carteira_cobr_w,
	nr_documento_cobr_w,
	dt_emissao_w,
	nr_titulo_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_pessoa_titulo_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	sg_estado_w,
	ie_codigo_desconto_w,
	nr_seq_mensalidade_w,
	nr_seq_contrato_w,
	cd_mov_remessa_w,
	pr_multa_w,
	pr_juros_w;
exit when C01%notfound;
	begin
	nr_seq_registro_w	:= nvl(nr_seq_registro_w,0) + 1;

	/* Segmento P */
	nr_seq_apres_w		:= nr_seq_apres_w + 1;

	if	(length(nvl(cd_convenio_banco_w,' ')) = 7) then
		ds_nosso_numero_w	:= cd_convenio_banco_w || lpad(nr_titulo_w,10,'0');
	elsif	(length(nvl(cd_convenio_banco_w,' ')) >= 5) then
		ds_nosso_numero_w	:= lpad(cd_convenio_banco_w,6,'0') || lpad(nr_titulo_w,5,'0');
		ds_nosso_numero_w	:= ds_nosso_numero_w || calcula_digito('MODULO11',ds_nosso_numero_w);
	elsif	(length(nvl(cd_convenio_banco_w,' ')) <= 4) then
		ds_nosso_numero_w	:= lpad(cd_convenio_banco_w,4,'0') || lpad(nr_titulo_w,7,'0');
		ds_nosso_numero_w	:= ds_nosso_numero_w || calcula_digito('MODULO11',ds_nosso_numero_w);
	end if;

	vl_juros_acobrar_w	:= nvl(obter_juros_multa_titulo(nr_titulo_w,sysdate,'R','J'),0);
		
	ds_conteudo_w	:=	cd_banco_w || --1 3
				lpad(nvl(nr_seq_lote_w,0),4,'0') || --4 7
				'3' || --8 8
				lpad(nr_seq_registro_w,5,'0') || --9 13
				'P' || --14 14
				' ' || --15 15
				'01' || --16 17
				cd_agencia_bancaria_w || --18 22
				nr_digito_agencia_w || --23 23
				nr_conta_corrente_w || --24 36
				' ' || --37 37				
				rpad(ds_nosso_numero_w,20,' ') || --38 57
				'2' || --58 58
				'1' || --59 59
				'2' || --60 60
				'1' || --61 61
				'1' || --62 62
				rpad(nr_documento_cobr_w,15,' ') || --63 77
				dt_vencimento_w || --78 85
				vl_cobranca_w || --86 100
				'13500' || --101 105
				' ' || --106 106
				'02' || --107 108
				'N' || --109 109
				dt_emissao_w || --110 117
				'1' || --118 118
				'00000000' || --119 126
				lpad(somente_numero(to_char(nvl(vl_juros_acobrar_w,0),'0000000000000.00')),15,'0') || --127 141
				ie_codigo_desconto_w || --142 142
				dt_desconto_w || --143 150
				vl_desconto_w || --151 165
				'000000000000000' || --166 180
				'000000000000000' || --181 195
				rpad(nr_titulo_w,25,' ') || --196 220
				'3' || --221 221
				'00' || --222 223
				'1' || --224 224
				'000' || --225 227
				'09' || --228 229
				'0000000000' || --230 239
				' '; --240 240
	qt_registro_lote_w	:= qt_registro_lote_w + 1;
	nr_seq_total_linhas_w	:= nvl(nr_seq_total_linhas_w,0) + 1;
	
	insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w);
	/* Fim segmento P */

	/* Segmento Q */
	nr_seq_registro_w	:= nvl(nr_seq_registro_w,0) + 1;
	nr_seq_apres_w	:= nr_seq_apres_w + 1;

	ds_conteudo_w	:= 	cd_banco_w || --1 3
				lpad(nvl(nr_seq_lote_w,0),4,'0') || --4 7
				'3' || --8 8
				lpad(nr_seq_registro_w,5,'0') || --9 13
				'Q' || --14 14
				' ' || --15 15
				'01'|| --16 17
				ie_tipo_inscricao_w || --18 18
				nr_inscricao_w || --19 33
				nm_pessoa_titulo_w || --34 73
				ds_endereco_w || --74 113
				ds_bairro_w || --114 128
				cd_cep_w || --129 136
				ds_cidade_w || --137 151
				substr(sg_estado_w,1,2) || --152 153
				'0' || --154 154
				rpad('0',15,'0') || --155 169
				lpad(' ',40,' ') || --170 209 
				'000' || --210 212 
				ds_brancos_28_w; --213 240

	insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres)
	values	(	w_envio_banco_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w);

	nr_seq_total_linhas_w	:= nvl(nr_seq_total_linhas_w,0) + 1;
	/* Fim segmento Q */
	
	end;	

	nr_seq_lote_w	:= nvl(nr_seq_lote_w,0) + 1;
	
end loop;
close C01;
/* Fim detalhe */

/* Trailler Lote*/
select	(count(*) * 2) + 2,
	'9' ie_tipo_registro,
	'0001' nr_seq_envio,
	somente_numero(to_char(sum(b.vl_liquidacao),'000000000000.00'))
into	qt_reg_lote_w,
	ie_tipo_registro_w,
	nr_seq_envio_w,
	vl_liquidacao_w
from	titulo_receber_cobr b,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	a.nr_sequencia		= nr_seq_cobr_escrit_p
group by a.nr_sequencia,
	 a.dt_remessa_retorno,
	 a.dt_remessa_retorno,
	 a.cd_banco,
	a.ie_emissao_bloqueto;
	
ds_conteudo_w	:= 			cd_banco_w || 
					nr_seq_envio_w || 
					'5' || 
					ds_brancos_9_w || 
					lpad(nr_seq_total_linhas_w,6,'0') || 
					'000000' || 
					lpad(vl_liquidacao_w,15,'0') ||
					lpad('0',63,'0') || 
					rpad(' ',133,' ');

nr_seq_apres_w	:= nr_seq_apres_w + 1;

	
insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
		
qt_reg_arquivo_w	:= qt_reg_arquivo_w + 1;
qt_registro_lote_w	:= qt_registro_lote_w + 1;
/* Fim Trailler Lote*/

/* Trailler Arquivo*/
select	(count(*) * 2) + 4,
	'9' ie_tipo_registro,
	'99999' nr_seq_envio
into	qt_reg_lote_w,
	ie_tipo_registro_w,
	nr_seq_envio_w
from	titulo_receber_cobr b,
	cobranca_escritural a
where	a.nr_sequencia		= b.nr_seq_cobranca
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
qt_reg_arquivo_w := qt_reg_arquivo_w + 1;

ds_conteudo_w	:=	cd_banco_w || 
					'9999' || 
					'9' || 
					ds_brancos_9_w || 
					'000001' || 
					lpad(nr_seq_total_linhas_w + 1,6,'0') || 
					'000000' || 
					ds_brancos_205_w;

nr_seq_apres_w	:= nr_seq_apres_w + 1;

insert into w_envio_banco
	(	nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
values	(	w_envio_banco_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
/* Fim Trailler Arquivo*/

commit;

end gerar_cobranca_safra_cnab_240;
/
