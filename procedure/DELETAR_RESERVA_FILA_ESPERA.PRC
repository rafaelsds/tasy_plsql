create or replace
procedure deletar_reserva_fila_espera(	nr_sequencia_p		number,
										nm_usuario_p		varchar2) is 

ie_possui_reserva_w			varchar2(1);
ie_gerar_nova_reserva_w		varchar2(1);
									
begin
Obter_param_Usuario(1002, 122, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_gerar_nova_reserva_w);

if (nvl(nr_sequencia_p,0) > 0) then
	select	decode(count(*),0,'N','S')
	into	ie_possui_reserva_w
	from	fila_espera_reserva
	where	nr_seq_gv	= nr_sequencia_p;
		
	if	(ie_possui_reserva_w = 'S')	and
		(ie_gerar_nova_reserva_w = 'S')	then
		delete	fila_espera_reserva
		where	nr_seq_gv = nr_sequencia_p;
		commit;
		Reservar_leito	(nr_sequencia_p, nm_usuario_p);
	end if;
end if;
	
end deletar_reserva_fila_espera;
/