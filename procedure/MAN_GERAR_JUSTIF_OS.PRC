create or replace
procedure Man_Gerar_Justif_OS
			(	nr_seq_ordem_p			number,
				ie_tipo_justificativa_p		varchar2,
				ds_justificativa_p		varchar2,
				nm_usuario_p			Varchar2) is
				
nm_user_w			varchar2(30);
nr_seq_tipo_w			number(10);
ds_mensagem_w			varchar2(4000);

begin
select	username
into	nm_user_w
from	user_users;

if	(nm_user_w = 'CORP') then
	nr_seq_tipo_w	:= 148;
end if;

select	substr(max(obter_desc_expressao_idioma(cd_exp_informacao, ds_informacao,man_obter_idioma_os_local(nr_seq_ordem_p))) || substr(ds_justificativa_p,1,255),1,4000)
into	ds_mensagem_w
from	dic_objeto
where	nr_sequencia = 305600
and	ie_tipo_objeto = 'T';

insert into man_ordem_justificativa
	(nr_sequencia,
	nr_seq_ordem,
	ie_tipo_justificativa,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_justificativa)
values	(man_ordem_justificativa_seq.nextval,
	nr_seq_ordem_p,
	ie_tipo_justificativa_p,
	sysdate, nm_usuario_p,
	sysdate, nm_usuario_p,
	substr(ds_justificativa_p,1,255));

insert into man_ordem_serv_tecnico
	(nr_sequencia,
	nr_seq_ordem_serv,
	dt_atualizacao,
	nm_usuario,
	ds_relat_tecnico,
	dt_historico,
	ie_origem,
	nr_seq_tipo,
	dt_liberacao,
	nm_usuario_lib)
values	(man_ordem_serv_tecnico_seq.nextval,
	nr_seq_ordem_p,
	sysdate,
	nm_usuario_p,
	ds_mensagem_w,
	sysdate,
	'I',
	nr_seq_tipo_w,
	sysdate,
	nm_usuario_p);
	
commit;

end Man_Gerar_Justif_OS;
/