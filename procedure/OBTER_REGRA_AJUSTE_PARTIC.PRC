create or replace
procedure Obter_Regra_Ajuste_Partic(	cd_estabelecimento_p	number,
					cd_convenio_p		number,
					qt_porte_anestesico_p	number,
					dt_procedimento_p	date,
					cd_especialidade_p	number,
					vl_participante_p	out number,
					vl_medico_original_p	number) is

Cursor C01 is
	select	nvl(vl_participante,0),
		nvl(tx_medico_orig_tab,0)
	from	regra_valor_participante a
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_convenio		= cd_convenio_p
	and	nvl(nr_porte_anestesico,nvl(qt_porte_anestesico_p,0))	= nvl(qt_porte_anestesico_p,0)
	and 	nvl(cd_especialidade, nvl(cd_especialidade_p,0)) = nvl(cd_especialidade_p,0)
	and	dt_procedimento_p between dt_inicio_vigencia and nvl(dt_final_vigencia,dt_procedimento_p)
	order by dt_inicio_vigencia,
		nvl(cd_especialidade,0);
	
vl_participante_w		number(15,2);
tx_medico_orig_tab_w		regra_valor_participante.tx_medico_orig_tab%type;
begin

open C01;
loop
fetch C01 into	
	vl_participante_w,
	tx_medico_orig_tab_w;
exit when C01%notfound;
end loop;
close C01;

if	(vl_participante_w > 0) then
	vl_participante_p	:= vl_participante_w;
elsif	(tx_medico_orig_tab_w > 0) and
	(nvl(vl_medico_original_p,0) > 0) then
	vl_participante_p	:= (dividir(vl_medico_original_p,100) * tx_medico_orig_tab_w);
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end Obter_Regra_Ajuste_Partic;
/
