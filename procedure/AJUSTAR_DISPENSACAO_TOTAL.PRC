create or replace
procedure ajustar_dispensacao_total(	nr_sequencia_p		number,
					nm_usuario_p		varchar2,
					nr_prescricao_p		number,
					cd_estabelecimento_p	number,
					ie_opcao_p		varchar2) is 

nr_seq_lote_fornec_w		number(10,0);
cd_material_w			number(6,0);
qt_dispensacao_w		number(15,4);
qt_registro_w			number(10);
qt_material_w			number(18,6);
qt_material_ww			number(15,3);
nr_sequencia_w			number(10);
var_cd_intervalo_w		varchar2(10);
cd_unidade_medida_consumo_w	varchar2(30);
ie_via_aplicacao_w		varchar2(5);
ie_tipo_pessoa_w		number(1,0);
cd_pessoa_usuario_w		varchar2(10);
qt_total_dispensar_w		number(18,6);
ie_regra_disp_w			varchar2(1);
ds_erro_w			varchar2(2000);
ie_se_necessario_w		varchar2(1);
ie_acm_w			varchar2(1);
nr_cirurgia_w			number(10);
				
Cursor 	C01 is
	select	decode(nr_seq_lote_fornec,0,null,nr_seq_lote_fornec),
		cd_material,
		qt_dispensacao
	from	cirurgia_agente_disp
	where	nr_sequencia = nr_sequencia_p;
begin

select	max(nr_cirurgia)
into	nr_cirurgia_w
from	cirurgia
where	nr_prescricao = nr_prescricao_p;

if	(ie_opcao_p = 'I') then
	open C01;
	loop
	fetch C01 into	
		nr_seq_lote_fornec_w,
		cd_material_w,
		qt_dispensacao_w;
	exit when C01%notfound;
		select	count(*)
		into	qt_registro_w
		from	prescr_material
		where	nr_prescricao			= nr_prescricao_p
		and	cd_material			= cd_material_w
		and	nvl(nr_seq_lote_fornec,0)	= nvl(nr_seq_lote_fornec_w,0)
		and	cd_motivo_baixa			= 0;
		if	(qt_registro_w = 0) then
			gravar_mat_adic_barras(	nr_prescricao_p,
						nr_seq_lote_fornec_w,
						cd_material_w,
						null,
						qt_dispensacao_w,
						nm_usuario_p,
						cd_estabelecimento_p,
						'GI',
						null,
						null,
						null,
						null,
						null);
		else
			select	sum(qt_material)
			into	qt_material_w
			from	prescr_material
			where	nr_prescricao			= nr_prescricao_p
			and	cd_material			= cd_material_w
			and	nvl(nr_seq_lote_fornec,0)	= nvl(nr_seq_lote_fornec_w,0)
			and	cd_motivo_baixa			= 0;
			
			if	(qt_material_w = 0) then
				qt_material_w := qt_dispensacao_w;
			else
				qt_material_w := qt_material_w + qt_dispensacao_w;
			end if;
			
			select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo,
				ie_via_aplicacao
			into	cd_unidade_medida_consumo_w,
				ie_via_aplicacao_w
			from	material
			where	cd_material	= cd_material_w;
			
			select	nvl(max(cd_intervalo),1)
			into	var_cd_intervalo_w
			from	intervalo_prescricao
			where	ie_agora = 'S';
			
			select	obter_tipo_pessoa(cd_pessoa_usuario_w)
			into	ie_tipo_pessoa_w
			from	dual;
			
			select	nvl(max(ie_se_necessario),'N'),
				nvl(max(ie_acm),'N')
			into	ie_se_necessario_w,
				ie_acm_w
			from	prescr_material
			where	nr_prescricao = nr_Prescricao_p
			and	nr_sequencia = nr_sequencia_p;
			
			obter_quant_dispensar(	cd_estabelecimento_p,
						cd_material_w,
						nr_prescricao_p,
						0,
						var_cd_intervalo_w,
						ie_via_aplicacao_w,
						qt_material_w,
						null,
						1,
						null,
						nvl(ie_tipo_pessoa_w,1),
						cd_unidade_medida_consumo_w,
						null,
						qt_material_ww,
						qt_total_dispensar_w,
						ie_regra_disp_w,
						ds_erro_w,
						ie_se_necessario_w,
						ie_acm_w);
						
			update	prescr_material
			set	qt_material			= qt_material_ww,
				qt_unitaria			= qt_material_w,
				qt_total_dispensar		= qt_total_dispensar_w
			where	nr_prescricao			= nr_prescricao_p
			and	cd_material			= cd_material_w
			and	nvl(nr_seq_lote_fornec,0)	= nvl(nr_seq_lote_fornec_w,0)
			and	cd_motivo_baixa			= 0
			and	dt_baixa is null;
			
		end if;	
	end loop;
	close C01;
else
	update	prescr_material
	set	qt_material			= 0,
		qt_total_dispensar		= 0,
		qt_dose				= 0,
		qt_unitaria			= 0
	where 	nr_prescricao			= nr_prescricao_p
	and	cd_motivo_baixa			= 0
	and	dt_baixa is null;
end if;	

if	(ie_opcao_p = 'I') then
	update	cirurgia_agente_disp
	set	dt_ajuste 	= sysdate
	where	nr_sequencia 	= nr_sequencia_p;
end if;	

--atualiza_disp_cirurgia(nr_cirurgia_w);

commit;

end ajustar_dispensacao_total;
/
