create or replace
procedure cancelar_frasco_pato_amostra( nr_seq_frasco_pato_loc_p	number,
				   nm_usuario_p			varchar2) is 

begin
if (nr_seq_frasco_pato_loc_p is not null) then

	update	frasco_pato_loc
	set	ie_status      = 40,
		dt_atualizacao = sysdate,
		nm_usuario     = nm_usuario_p
	where	nr_sequencia   = nr_seq_frasco_pato_loc_p;
	
	insert into frasco_pato_loc_status (	
		nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		ie_status, 
		nr_seq_frasco_pato_loc)
	values(	frasco_pato_loc_status_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		40,
		nr_seq_frasco_pato_loc_p);
	commit;
end if;

end cancelar_frasco_pato_amostra;
/