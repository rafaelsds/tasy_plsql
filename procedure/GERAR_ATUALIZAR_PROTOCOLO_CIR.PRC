create or replace 
PROCEDURE gerar_atualizar_protocolo_cir(	nr_seq_protocolo_p	NUMBER,
						nr_seq_cirurgia_desc_p	NUMBER,
						nm_cirurgia_p		VARCHAR2,
						ie_opcao_p		VARCHAR2,
						cd_medico_p		VARCHAR2,
						nm_usuario_p		VARCHAR2,
						cd_estabelecimento_p	NUMBER,
						ds_erro_p		out varchar2) IS

/*
ie_opcao_p

I - Novo protocolo
A - Atualizar protocolo
*/

ds_diagnostico_w	VARCHAR2(2000);
ds_resumo_cirurgia_w	VARCHAR2(2000);
ds_diagnostico_pos_w	VARCHAR2(2000);
ds_exame_rad_w		VARCHAR2(2000);
ds_exame_anatomo_w	VARCHAR2(2000);
ds_achados_operat_w	VARCHAR2(2000);
ds_cirurgia_w		VARCHAR2(32000);
cd_especialidade_w	number(5,0);
ie_gravar_espec_w	varchar2(1);
ie_gravar_estab_w	varchar2(1);
cd_estabebecimento_w	number(4);

BEGIN
begin
ds_erro_p := '';

Obter_Param_Usuario(281,379,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gravar_espec_w);
Obter_Param_Usuario(872,407,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_gravar_estab_w);

if (ie_gravar_estab_w = 'S') then
	cd_estabebecimento_w	:=	cd_estabelecimento_p;
end if;

SELECT	ds_diagnostico,
	ds_resumo_cirurgia,
	ds_diagnostico_pos,
	ds_exame_rad,
	ds_exame_anatomo,
	ds_achados_operatorios,
	ds_cirurgia,
	obter_especialidade_medico(cd_medico_p,'C')
INTO	ds_diagnostico_w,
	ds_resumo_cirurgia_w,
	ds_diagnostico_pos_w,
	ds_exame_rad_w,
	ds_exame_anatomo_w,
	ds_achados_operat_w,
	ds_cirurgia_w,
	cd_especialidade_w
FROM	cirurgia_descricao
WHERE	nr_sequencia	= nr_seq_cirurgia_desc_p;

IF	(ie_opcao_p = 'A') THEN
	UPDATE	cirurgia_protocolo
	SET	ds_diagnostico		=	ds_diagnostico_w,
		ds_resumo_cirurgia	=	ds_resumo_cirurgia_w,
		ds_diagnostico_pos	=	ds_diagnostico_pos_w,
		ds_exame_rad		=	ds_exame_rad_w,
		ds_exame_anatomo	=	ds_exame_anatomo_w,
		ds_achados_operatorios	=	ds_achados_operat_w,
		ds_cirurgia		=	ds_cirurgia_w,
		cd_especialidade 	= 	decode(ie_gravar_espec_w,'S',cd_especialidade_w,null)
	WHERE	nr_sequencia		=	nr_seq_protocolo_p;
ELSE
	INSERT INTO cirurgia_protocolo(
		nr_sequencia,
		nm_cirurgia,
		dt_atualizacao,
		nm_usuario,
		ds_diagnostico,
		ds_resumo_cirurgia,
		ds_diagnostico_pos,
		ds_exame_rad,
		ds_exame_anatomo,
		ds_achados_operatorios,
		ds_cirurgia,
		cd_especialidade,
		cd_medico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ie_situacao)
	VALUES(
		cirurgia_protocolo_seq.NEXTVAL,
		nm_cirurgia_p,
		SYSDATE,
		nm_usuario_p,
		ds_diagnostico_w,
		ds_resumo_cirurgia_w,
		ds_diagnostico_pos_w,
		ds_exame_rad_w,
		ds_exame_anatomo_w,
		ds_achados_operat_w,
		ds_cirurgia_w,
		decode(ie_gravar_espec_w,'S',cd_especialidade_w,null),
		cd_medico_p,
		SYSDATE,
		nm_usuario_p,
		cd_estabebecimento_w,
		'A');
END IF;


COMMIT;

exception
	when others then
		ds_erro_p := wheb_mensagem_pck.get_texto(279499);
end;

END gerar_atualizar_protocolo_cir;
/