create or replace
procedure ptu_gerar_nota_cobr_pre_pgto
			(	nr_seq_servico_pre_p	number,
				nr_seq_congenere_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				cd_interface_p		number,
				nm_usuario_p		varchar2) is 
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Gera os dados da nota cobranca em pre-pagamento (A700)
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

nr_seq_conta_w			pls_conta.nr_sequencia%type;
nr_seq_cobranca_w		ptu_nota_cobranca.nr_sequencia%type;
nr_lote_w			number(8);
cd_cooperativa_w		pls_congenere.cd_cooperativa%type;
cd_usuario_plano_w		varchar2(30);
nm_segurado_w			pessoa_fisica.nm_pessoa_fisica%type;
dt_atendimento_referencia_w	pls_conta.dt_atendimento_referencia%type;
ie_carater_internacao_w		number(1);
cd_doenca_w			pls_diagnostico_conta.cd_doenca%type;
ie_tipo_atendimento_w		varchar2(2);
dt_internacao_w			pls_conta.dt_entrada%type;
dt_alta_w			pls_conta.dt_alta%type;
tp_nota_w			varchar2(10);
id_nota_principal_w		varchar2(10);
nr_ver_tiss_w			varchar2(10);
nr_guia_tiss_prestador_w	pls_conta.cd_guia_prestador%type;
nr_guia_tiss_principal_w	pls_conta.cd_guia_referencia%type;
cd_guia_w			pls_conta.cd_guia%type;
tp_ind_acidente_w		varchar2(10);
nr_seq_motivo_saida_int_w	pls_conta.nr_seq_saida_int%type;
motivo_encerram_w		pls_motivo_saida.cd_ptu%type;
nr_cnpj_cpf_req_w		varchar2(14);
nm_prest_req_w			varchar2(70);
sg_cons_prof_req_w		varchar2(12);
nr_cons_prof_req_w		varchar2(15);
sg_uf_cons_req_w		valor_dominio.vl_dominio%type;
cd_cbo_req_w			sus_cbo_pessoa_fisica.cd_cbo%type;
nr_cbo_req_w			number(6);
dt_protocolo_w			date;
ie_tipo_guia_w			pls_conta.ie_tipo_guia%type;
ie_tipo_consulta_w		pls_conta.ie_tipo_consulta%type;
nr_seq_tipo_atendimento_w	pls_conta.nr_seq_tipo_atendimento%type;
nr_seq_conta_referencia_w	pls_conta.nr_seq_conta_referencia%type;
cd_medico_solic_w		pls_conta.cd_medico_solicitante%type;
nr_seq_prestador_w		pls_protocolo_conta.nr_seq_prestador%type;
nr_seq_protocolo_w		pls_protocolo_conta.nr_sequencia%type;
qt_registro_w			number(10);
nr_tipo_atendimento_w		number(10);
nr_seq_saida_spsadt_w		number(10);
ie_motivo_saida_w		varchar2(2)	:= '1';
nr_seq_saida_consulta_w		number(10);
nr_seq_pagador_w		pls_segurado.nr_seq_pagador%type;
ie_tipo_contrato_w		pls_intercambio.ie_tipo_contrato%type;
nr_seq_segurado_w		number(10);
cd_cartao_intercambio_w		varchar2(30);
ie_tipo_repasse_w		pls_segurado.ie_tipo_repasse%type;
ie_tipo_segurado_w		pls_segurado.ie_tipo_segurado%type;
nr_seq_cbo_saude_rec_w		number(10);
dt_inicio_w			date;
dt_final_w			date;
nr_ver_tiss_aux_w		varchar2(10);
nr_seq_analise_w		pls_analise_conta.nr_sequencia%type;
nr_seq_prestador_princ_w	pls_analise_conta.nr_seq_prestador%type;
nr_seq_conta_princ_w		pls_conta.nr_sequencia%type;
nr_seq_prest_solic_w		pls_guia_plano.nr_seq_prest_solic%type;
sg_estado_w			pessoa_juridica.sg_estado%type;
tp_consulta_w			pls_conta.ie_tipo_consulta%type;
ie_versao_w			number(10);
tp_pessoa_w			ptu_nota_cobranca.tp_pessoa%type;
nr_cnpj_cpf_w			ptu_nota_cobranca.nr_cnpj_cpf%type;
cd_cnes_cont_exec_w		ptu_nota_cobranca.cd_cnes_cont_exec%type;
cd_munic_cont_exec_w		ptu_nota_cobranca.cd_munic_cont_exec%type;

nm_prest_exec_w			ptu_nota_cobranca.nm_prest_exec%type;
tp_prest_exec_w			ptu_nota_cobranca.tp_prest_exec%type;
cd_cid_obito_w			ptu_nota_cobranca.cd_cid_obito%type;
id_rec_proprio_w		ptu_nota_cobranca.id_rec_proprio%type;
	
Cursor C01 is
	select	a.nr_sequencia
	from	pls_conta			a,
		pls_conta_medica_resumo		d,
  		pls_segurado			b,
		pls_lote_pagamento		e,
		pls_intercambio			c
	where	d.nr_seq_conta			= a.nr_sequencia
	and	a.nr_seq_segurado		= b.nr_sequencia
	and	d.nr_seq_lote_pgto		= e.nr_sequencia
	and	b.nr_seq_intercambio		= c.nr_sequencia
	and	c.nr_seq_congenere		= nr_seq_congenere_p
	and	b.ie_tipo_repasse		= 'P'
	and	e.ie_status			= 'D'
	and	e.dt_mes_competencia		between dt_inicio_w and dt_final_w
	and	e.dt_geracao_titulos is not null
	and	nvl(d.nr_seq_serv_pre_pgto,0)	= 0
	union
	select	a.nr_sequencia
	from	pls_conta			a,
		pls_conta_medica_resumo		d,
  		pls_segurado			b,
		pls_lote_pagamento		e,
		PLS_PAGAMENTO_PRESTADOR		f,
		pls_intercambio			c,
		pls_prestador			g
	where	d.nr_seq_conta			= a.nr_sequencia
	and	a.nr_seq_segurado		= b.nr_sequencia
	and	d.nr_seq_lote_pgto		= e.nr_sequencia
	and	f.NR_SEQ_LOTE			= e.nr_sequencia
	and	b.nr_seq_intercambio		= c.nr_sequencia
	and	f.nr_seq_prestador		= g.nr_sequencia
	and	c.nr_seq_congenere		= nr_seq_congenere_p
	and	g.IE_TIPO_RELACAO		= 'P'
	and	b.ie_tipo_repasse		= 'P'
	and	e.ie_status			= 'D'
	and	e.dt_mes_competencia		between dt_inicio_w and dt_final_w
	and	e.DT_GERACAO_VENCIMENTOS is not null
	and	nvl(d.nr_seq_serv_pre_pgto,0)	= 0
	group by a.nr_sequencia;

begin
dt_inicio_w := trunc(dt_inicio_p,'month');
dt_final_w := fim_dia(last_day(dt_inicio_p));

select	max(cd_cooperativa)
into	cd_cooperativa_w
from	pls_congenere
where	nr_sequencia	= nr_seq_congenere_p;

--A versao do TISS esta definida na funcao OPS - Gestao de Operadoras / Cadastros / Versao TISS
nr_ver_tiss_aux_w := pls_obter_versao_tiss;
ie_versao_w	:= somente_numero(ptu_obter_versao_dominio('A700', cd_interface_p));

open C01;
loop
fetch C01 into	
	nr_seq_conta_w;
exit when C01%notfound;
	begin	
	motivo_encerram_w	:= '';
	ie_tipo_guia_w		:= '';
	
	select	upper(c.nm_pessoa_fisica),
		d.dt_atendimento_referencia,
		d.ie_tipo_guia,
		d.ie_tipo_consulta,
		decode(d.ie_carater_internacao,'E',0,'U',1,1),
		d.nr_seq_tipo_atendimento,
		--d.dt_entrada,
		--d.dt_alta,
		d.nr_seq_conta_referencia,
		nr_ver_tiss_aux_w,
		d.cd_guia_prestador,
		d.cd_guia_referencia,
		d.cd_guia,
		d.ie_indicacao_acidente,
		d.nr_seq_saida_int,
		d.cd_medico_solicitante,
		e.nr_seq_prestador,
		nvl(e.dt_protocolo,e.dt_recebimento),
		e.nr_sequencia,
		d.nr_seq_saida_spsadt,
		d.nr_seq_saida_consulta,
		b.nr_seq_pagador,
		a.nr_seq_segurado,
		b.cd_cartao_intercambio,
		b.ie_tipo_repasse,
		b.ie_tipo_segurado,
		d.ie_tipo_consulta
	into	nm_segurado_w,
		dt_atendimento_referencia_w,
		ie_tipo_guia_w,
		ie_tipo_consulta_w,
		ie_carater_internacao_w,
		nr_seq_tipo_atendimento_w,
		--dt_internacao_w,
		--dt_alta_w,
		nr_seq_conta_referencia_w,
		nr_ver_tiss_w,
		nr_guia_tiss_prestador_w,
		nr_guia_tiss_principal_w,
		cd_guia_w,
		tp_ind_acidente_w,
		nr_seq_motivo_saida_int_w,
		cd_medico_solic_w,
		nr_seq_prestador_w,
		dt_protocolo_w,
		nr_seq_protocolo_w,
		nr_seq_saida_spsadt_w,
		nr_seq_saida_consulta_w,
		nr_seq_pagador_w,
		nr_seq_segurado_w,
		cd_cartao_intercambio_w,
		ie_tipo_repasse_w,
		ie_tipo_segurado_w,
		tp_consulta_w
	from	pls_conta				d,
		pessoa_fisica				c,
		pls_segurado				b,
		pls_segurado_carteira			a,
		pls_protocolo_conta			e
	where	d.nr_seq_segurado			= b.nr_sequencia
	and	c.cd_pessoa_fisica			= b.cd_pessoa_fisica
	and	a.nr_seq_segurado			= b.nr_sequencia
	and	d.nr_seq_protocolo			= e.nr_sequencia
	and	d.nr_sequencia				= nr_seq_conta_w;
		
	tp_ind_acidente_w	:= ptu_obter_tp_ind_acidente(tp_ind_acidente_w);
		
	if	(nr_ver_tiss_w >= '3.00.00' ) and (tp_ind_acidente_w = '0') then
		tp_ind_acidente_w	:= '9';
	end if;
		
	cd_usuario_plano_w := nvl(substr(pls_obter_dados_segurado(nr_seq_segurado_w,'CI'),5,13),substr(cd_cartao_intercambio_w,4,13));
	
	if	(nr_seq_saida_spsadt_w is not null) then
		select	nvl(substr(max(cd_tiss), 1, 1), '1')
		into	ie_motivo_saida_w
		from	pls_motivo_saida_sadt
		where	nr_sequencia	= nr_seq_saida_spsadt_w;
	
	elsif	(nr_seq_saida_consulta_w is not null) then
		select	nvl(substr(max(cd_tiss), 1, 1), '1')
		into	ie_motivo_saida_w
		from	pls_motivo_saida_consulta
		where	nr_sequencia	= nr_seq_saida_consulta_w;
	end if;
	
	if	(ie_motivo_saida_w is null) then
		ie_motivo_saida_w	:= '1';
	end if;
	
	if	(ie_tipo_guia_w = '5') or (nr_ver_tiss_w >= '3.00.00' ) then
		ie_motivo_saida_w	:= null;
	end if;
	
	-- SP/SADT
	if	(ie_tipo_guia_w	= '4') then
		begin
		select	substr(cd_tiss,1,2)
		into	ie_tipo_atendimento_w
		from    pls_tipo_atendimento
		where	nr_sequencia	= nr_seq_tipo_atendimento_w;
		exception
		when others then
			ie_tipo_atendimento_w	:= null;
		end;
		--ie_tipo_atendimento_w		:= '0';
	else
		ie_tipo_atendimento_w		:= null;
	end if;
	
	-- Internacao
	if	(ie_tipo_guia_w = '5') and
		(ie_tipo_atendimento_w is null) then
		ie_carater_internacao_w	:= '0';
	end if;
	
	-- Consulta
	if	(ie_tipo_guia_w	= '3') then
		nr_cbo_req_w := 0; -- Regra: para o tipo de nota igual a 1 Consulta (registro 502, seq 25 ) o campo NR_CBO_REQ, seq 38 do registro 502, devera ser SEMPRE preenchido com uma sequencia de zeros.
		ie_carater_internacao_w	:= '0';
		
		if	(ie_tipo_atendimento_w is null) then
			ie_tipo_atendimento_w := null;
		end if;
	end if;
	
	-- Honorario Individual
	if	(ie_tipo_guia_w	= '6') and
		(ie_tipo_atendimento_w is null) then
		ie_tipo_atendimento_w := '4';
	end if;
	
	if	(nr_seq_motivo_saida_int_w is not null) then
		select	max(cd_ptu)
		into	motivo_encerram_w
		from	pls_motivo_saida
		where	nr_sequencia = nr_seq_motivo_saida_int_w;
	end if;
	
	if	(ie_tipo_guia_w	= '3') then
		tp_nota_w		:= '1';
		id_nota_principal_w	:= 'S';
	elsif	(ie_tipo_guia_w = '4') then
		tp_nota_w		:= '2';
		
		if	(nr_seq_conta_referencia_w is not null) then
			id_nota_principal_w	:= 'N';
		else
			id_nota_principal_w	:= 'S';	
		end if;
		
	elsif	(ie_tipo_guia_w	= '5') then
		tp_nota_w		:= '3';
		
		if	(nr_seq_conta_referencia_w is not null) then
			id_nota_principal_w	:= 'N';
		else
			id_nota_principal_w	:= 'S';	
		end if;
		
	elsif	(ie_tipo_guia_w	= '6') then
		tp_nota_w		:= '4';
		id_nota_principal_w	:= 'N';
	end if;
	
	-- Se for Consulta ou SADT insere em branco
	if	(tp_nota_w not in ('1','2')) then	
		begin
		select	max(b.nr_seq_analise),
			max(nvl(a.nr_seq_prestador, b.nr_seq_prestador))
		into	nr_seq_analise_w,
			nr_seq_prestador_princ_w
		from	pls_analise_conta	a,
			pls_conta		b
		where	b.nr_seq_analise	= a.nr_sequencia(+)
		and	b.nr_sequencia		= nr_seq_conta_w;
		exception
		when others then
			nr_seq_analise_w		:= null;
			nr_seq_prestador_princ_w	:= null;
		end;
		
		nr_seq_conta_princ_w	:= pls_obter_conta_principal( cd_guia_w , nr_seq_analise_w , nr_seq_segurado_w , nr_seq_prestador_princ_w );
			
		begin		
		select	nvl(max(dt_inicio_faturamento),max(dt_entrada)),
			nvl(max(dt_fim_faturamento),max(dt_alta))
		into	dt_internacao_w,
			dt_alta_w
		from	pls_conta
		where	nr_sequencia	= nr_seq_conta_princ_w;
		exception
		when others then
			dt_internacao_w	:= null;
			dt_alta_w	:= null;
		end;
		
		if	(dt_internacao_w is null) and
			(dt_alta_w is null) then
			begin
			select	nvl(max(dt_inicio_faturamento),max(dt_entrada)),
				nvl(max(dt_fim_faturamento),max(dt_alta))
			into	dt_internacao_w,
				dt_alta_w
			from	pls_conta
			where	nr_sequencia	= nr_seq_conta_w;
			exception
			when others then
				dt_internacao_w	:= null;
				dt_alta_w	:= null;
			end;
			
			dt_internacao_w := nvl(dt_internacao_w,dt_alta_w);
			dt_alta_w := nvl(dt_alta_w,dt_internacao_w);
		end if;
		
	else				
		dt_internacao_w		:= null;
		dt_alta_w		:= null;
	end if;
	
	begin
	select	cd_doenca
	into	cd_doenca_w
	from	pls_diagnostico_conta
	where	nr_seq_conta	= nr_seq_conta_w
	and	ie_classificacao = 'P';
	exception
	when others then
		cd_doenca_w	:= '';
	end;
	----------------------------------------------------------------------------------		DADOS DO SOLICITANTE/REQUISITANTE		----------------------------------------------------------------------------------  
	
	nr_seq_cbo_saude_rec_w := null;
	
	-- Buscar primeiro os dados do medico solicitante da conta 
	if	(cd_medico_solic_w is not null) then
		nr_cnpj_cpf_req_w	:= substr(obter_dados_pf(cd_medico_solic_w,'CPF'),1,14);
		nm_prest_req_w		:= substr(obter_nome_medico(cd_medico_solic_w,'N'),1,70);
		sg_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'SGCRM'),1,12);
		nr_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'CRM'),1,15);
		sg_uf_cons_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'UFCRM'),1,2);
		
		begin
		select	max(nr_seq_cbo_saude)
		into	nr_seq_cbo_saude_rec_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_medico_solic_w;
		exception
		when others then
			nr_seq_cbo_saude_rec_w := null;
		end;
		
		begin
		select	max(cd_cbo)
		into	cd_cbo_req_w
		from	sus_cbo_pessoa_fisica
		where	cd_pessoa_fisica = cd_medico_solic_w;
		exception
		when others then
			cd_cbo_req_w := null;
		end;
		
	-- Se nao tiver, buscar o prestador do atendimento
	elsif	(nr_seq_prestador_w is not null) then
		select	nvl(a.cd_cgc,substr(obter_dados_pf(a.cd_pessoa_fisica,'CPF'),1,14)),
			a.cd_pessoa_fisica,
			substr(pls_obter_dados_prestador(a.nr_sequencia,'N'),1,70)
		into	nr_cnpj_cpf_req_w,
			cd_medico_solic_w,
			nm_prest_req_w
		from	pls_prestador	a
		where	a.nr_sequencia	= nr_seq_prestador_w;
		
		if	(cd_medico_solic_w is null) then	-- '23-5578780'	
			begin
			select	max(nr_seq_prest_solic)
			into	nr_seq_prest_solic_w
			from 	pls_guia_plano
			where	cd_guia		= cd_guia_w;
			exception
			when others then
			nr_seq_prest_solic_w := null;
			end;
			
			if	(nr_seq_prest_solic_w is not null) then
				begin
				select	max(cd_pessoa_fisica)
				into	cd_medico_solic_w
				from	pls_prestador
				where	nr_sequencia	= nr_seq_prest_solic_w;
				exception
				when others then
				cd_medico_solic_w	:= null;
				end;
			end if;	
		end if;
		
		if	(cd_medico_solic_w is not null) then
			nr_cnpj_cpf_req_w	:= substr(obter_dados_pf(cd_medico_solic_w,'CPF'),1,14);
			nm_prest_req_w		:= substr(obter_nome_medico(cd_medico_solic_w,'N'),1,70);
			sg_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'SGCRM'),1,12);
			nr_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'CRM'),1,15);
			sg_uf_cons_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'UFCRM'),1,2);
			
			begin
			select	max(nr_seq_cbo_saude)
			into	nr_seq_cbo_saude_rec_w
			from	pessoa_fisica
			where	cd_pessoa_fisica = cd_medico_solic_w;
			exception
			when others then
				nr_seq_cbo_saude_rec_w := null;
			end;
				
			begin
			select	max(cd_cbo)
			into	cd_cbo_req_w
			from	sus_cbo_pessoa_fisica
			where	cd_pessoa_fisica = cd_medico_solic_w;
			exception
			when others then
				cd_cbo_req_w := null;
			end;
		else
			select	nvl(max(sg_estado),'X')
			into	sg_estado_w
			from	pessoa_juridica
			where	cd_cgc	=	(select	max(cd_cgc_outorgante)
						from	pls_outorgante
						where	cd_estabelecimento	= obter_estabelecimento_ativo);
		end if;
	
	elsif	(nr_guia_tiss_principal_w is not null) then
		select	count(1)
		into	qt_registro_w
		from	pls_guia_plano
		where	cd_guia	= nr_guia_tiss_principal_w;
		
		if	(qt_registro_w > 0) then
			select	max(cd_medico_solicitante),
				substr(pls_obter_dados_prestador(max(nr_seq_prestador),'N'),1,70)
			into	cd_medico_solic_w,
				nm_prest_req_w
			from	pls_guia_plano
			where	cd_guia	= nr_guia_tiss_principal_w;
			
			if	(cd_medico_solic_w is not null) then
				nr_cnpj_cpf_req_w	:= substr(obter_dados_pf(cd_medico_solic_w,'CPF'),1,14);
				nm_prest_req_w		:= nvl(substr(obter_nome_medico(cd_medico_solic_w,'N'),1,70),nm_prest_req_w);
				sg_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'SGCRM'),1,12);
				nr_cons_prof_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'CRM'),1,15);
				sg_uf_cons_req_w	:= substr(obter_dados_medico(cd_medico_solic_w,'UFCRM'),1,2);
				
				begin
				select	max(nr_seq_cbo_saude)
				into	nr_seq_cbo_saude_rec_w
				from	pessoa_fisica
				where	cd_pessoa_fisica = cd_medico_solic_w;
				exception
				when others then
					nr_seq_cbo_saude_rec_w := null;
				end;
				
				begin
				select	max(cd_cbo)
				into	cd_cbo_req_w
				from	sus_cbo_pessoa_fisica
				where	cd_pessoa_fisica = cd_medico_solic_w;
				exception
				when others then
					cd_cbo_req_w := null;
				end;
			end if;
		end if;
	end if;
	
	if	(nr_seq_cbo_saude_rec_w is not null) then		
		cd_cbo_req_w := pls_obter_cbo_saude_ptu(nr_seq_cbo_saude_rec_w, null, 'A700', cd_interface_p);
	end if;
	
	if	(cd_cbo_req_w is null) or (nr_ver_tiss_w = '2.02.03') then
		cd_cbo_req_w	:= '999999';
	end if;
		
	if	(somente_numero(nvl(cd_cbo_req_w,0)) > 0) then
		nr_cbo_req_w := somente_numero(cd_cbo_req_w);
	end if;
	
	-- Consulta
	if	(ie_tipo_guia_w	= '3') then
		nr_cbo_req_w	:= 0;
	end if;
	
	----------------------------------------------------------------------------------	FIM	DADOS DO SOLICITANTE/REQUISITANTE	FIM	----------------------------------------------------------------------------------  	
	
	select	ptu_nota_cobranca_seq.nextval
	into	nr_seq_cobranca_w
	from	dual;
	
	nr_lote_w 			:= nr_seq_conta_w + nr_seq_protocolo_w;
	nr_guia_tiss_principal_w	:= nr_guia_tiss_principal_w;
	nr_guia_tiss_prestador_w	:= nr_guia_tiss_prestador_w;
	cd_guia_w			:= cd_guia_w;
	nm_segurado_w			:= Elimina_Acentos(nm_segurado_w);
	nm_segurado_w			:= substr(nm_segurado_w,1,25);
	
	nr_tipo_atendimento_w	:= to_number(ie_tipo_atendimento_w);
	ie_tipo_atendimento_w	:= to_char(nr_tipo_atendimento_w);
	
	sg_cons_prof_req_w	:= nvl(trim(sg_cons_prof_req_w),'CRM');
	nr_cons_prof_req_w	:= nvl(trim(nr_cons_prof_req_w),'1');
	sg_uf_cons_req_w	:= nvl(trim(sg_uf_cons_req_w),sg_estado_w);
	
	-- Versao 6.1
	if	(ie_versao_w >= 61) then
		-- 1  Eletivo -- 2  Urgencia/Emergencia
		if	(ie_carater_internacao_w = 0) then
			ie_carater_internacao_w := 1;
		elsif	(ie_carater_internacao_w = 1) then
			ie_carater_internacao_w := 2;
		end if;
	
		if	(tp_nota_w = 1) then -- Consulta
			ie_carater_internacao_w := null;
			
		elsif	(tp_nota_w = 2) and -- SP/SADT
			(ie_tipo_atendimento_w <> '4') then
			tp_consulta_w := null;
			
		elsif	(tp_nota_w = 3) then -- Internacao
			tp_consulta_w := null;
			
		elsif	(tp_nota_w = 4) then -- Honorario Individual
			tp_consulta_w := null;
			ie_carater_internacao_w := null;
		end if;
	end if;
	
	-- Se for versao anterior a versao 9.0
	if	(ie_versao_w < 90) then
		nm_prest_req_w := substr(nm_prest_req_w,1,40);
	end if;	
	
	-- Se for versao igual ou maior 11.0
	if	(ie_versao_w >= 110) then
		nm_prest_req_w := substr(nm_prest_req_w,1,60);
	end if;	
	
	
	if	(sg_cons_prof_req_w is not null) then
		select	nvl(max(cd_ptu), sg_cons_prof_req_w)
		into	sg_cons_prof_req_w
		from	conselho_profissional
		where	sg_conselho = sg_cons_prof_req_w;
	end if;
	
	
	insert into ptu_nota_cobranca
		(	nr_sequencia,		dt_atualizacao,			nm_usuario,			dt_atualizacao_nrec,		nm_usuario_nrec,
			nr_seq_serv_pre_pagto,	nr_lote,			nr_nota,			cd_unimed,			cd_usuario_plano,
			nm_beneficiario,	dt_atendimento,			ie_carater_atendimento,		cd_cid,				ie_paciente,
			ie_tipo_atendimento,	nr_guia_principal,		dt_internacao,			dt_alta,			tp_nota,
			id_nota_principal,	nr_ver_tiss,			nr_guia_tiss_prestador,		nr_guia_tiss_principal,		nr_guia_tiss_operadora,
			tp_ind_acidente,	motivo_encerram,		nr_cnpj_cpf_req,		nm_prest_req,			sg_cons_prof_req,
			nr_cons_prof_req,	sg_uf_cons_req,			nr_cbo_req,			dt_protocolo,			nr_seq_conta,
			ie_tipo_saida_spdat,	tp_consulta,			tp_pessoa,			nr_cnpj_cpf,			cd_cnes_cont_exec,
			cd_munic_cont_exec)
	values	(	nr_seq_cobranca_w,	sysdate,			nm_usuario_p,			sysdate,			nm_usuario_p,
			nr_seq_servico_pre_p,	nr_lote_w,			nr_seq_conta_w,			cd_cooperativa_w,		cd_usuario_plano_w,
			nm_segurado_w,		dt_atendimento_referencia_w,	ie_carater_internacao_w,	cd_doenca_w,			'1',
			ie_tipo_atendimento_w,	nr_guia_tiss_principal_w,	dt_internacao_w,		dt_alta_w,			tp_nota_w,
			id_nota_principal_w,	nr_ver_tiss_w,			nr_guia_tiss_prestador_w,	nr_guia_tiss_principal_w,	cd_guia_w,
			tp_ind_acidente_w,	motivo_encerram_w,		nr_cnpj_cpf_req_w,		nm_prest_req_w,			sg_cons_prof_req_w,
			nr_cons_prof_req_w,	sg_uf_cons_req_w,		nr_cbo_req_w,			dt_protocolo_w, 		nr_seq_conta_w,
			ie_motivo_saida_w,	tp_consulta_w,			null,				null,				null,
			null);
	
	cd_cid_obito_w := null;
	if	(ie_tipo_guia_w = '5') or
		(ie_tipo_atendimento_w = '7') then
		ptu_gerar_nota_hospitalar700(nr_seq_cobranca_w, cd_interface_p, nm_usuario_p);
		
		-- Obter dados da internacao
		select	max(cd_cid_obito)
		into	cd_cid_obito_w
		from	ptu_nota_hospitalar
		where	nr_sequencia	= (	select	max(nr_sequencia)
						from	ptu_nota_hospitalar
						where	nr_seq_nota_cobr	= nr_seq_cobranca_w);
	end if;
	
	ptu_gerar_nota_servico700(nr_seq_cobranca_w, cd_interface_p, nm_usuario_p);
	ptu_gerar_nota_complemento700(nr_seq_cobranca_w, cd_interface_p, nm_usuario_p);

	-- Obter dados do executor dos itens
	select	max(ie_tipo_pessoa_prestador),
		max(nr_cgc_cpf),
		max(cd_cnes_prest),
		max(cd_munic),
		max(nm_prestador),
		max(ie_tipo_prestador),
		max(ie_rede_propria)
	into	tp_pessoa_w,
		nr_cnpj_cpf_w,
		cd_cnes_cont_exec_w,
		cd_munic_cont_exec_w,
		nm_prest_exec_w,
		tp_prest_exec_w,
		id_rec_proprio_w
	from	ptu_nota_servico
	where	nr_sequencia	= (	select	max(nr_sequencia)
					from	ptu_nota_servico
					where	nr_seq_nota_cobr	= nr_seq_cobranca_w);

	-- Atualizar os dados de executante
	update	ptu_nota_cobranca
	set	tp_pessoa		= tp_pessoa_w,
		nr_cnpj_cpf		= nr_cnpj_cpf_w,
		cd_cnes_cont_exec	= cd_cnes_cont_exec_w,
		cd_munic_cont_exec	= cd_munic_cont_exec_w,
		nm_prest_exec		= nm_prest_exec_w,
		tp_prest_exec		= tp_prest_exec_w,
		id_rec_proprio		= id_rec_proprio_w,
		cd_cid_obito		= cd_cid_obito_w
	where	nr_sequencia		= nr_seq_cobranca_w;
	
	update	pls_conta_medica_resumo
	set	nr_seq_serv_pre_pgto	= nr_seq_servico_pre_p
	where	nr_seq_conta		= nr_seq_conta_w;
	
	select	count(1)
	into	qt_registro_w
	from	ptu_nota_servico
	where	nr_seq_nota_cobr = nr_seq_cobranca_w;
	
	if	(qt_registro_w = 0) then
		delete	ptu_nota_complemento
		where	nr_seq_nota_cobr = nr_seq_cobranca_w;
	
		delete	ptu_nota_hosp_compl a
		where	a.nr_seq_nota_hosp = (	select	max(x.nr_sequencia)
						from	ptu_nota_hospitalar x
						where	x.nr_seq_nota_cobr = nr_seq_cobranca_w);
	
		delete	ptu_nota_hospitalar
		where	nr_seq_nota_cobr = nr_seq_cobranca_w;
	
		delete	ptu_nota_cobranca
		where	nr_sequencia = nr_seq_cobranca_w;
	end if;	
	end;
end loop;
close C01;

commit;

end ptu_gerar_nota_cobr_pre_pgto;
/