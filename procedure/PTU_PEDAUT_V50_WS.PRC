create or replace
procedure ptu_pedaut_v50_ws ( 	dt_atendimento_p			ptu_pedido_autorizacao.dt_atendimento%type, 
				ie_tipo_cliente_p		ptu_pedido_autorizacao.ie_tipo_cliente%type,--cabecalho
				cd_doenca_cid_p			ptu_pedido_autorizacao.cd_doenca_cid%type, 			
				ie_alto_custo_p			ptu_pedido_autorizacao.ie_alto_custo%type, 
				cd_unimed_prestador_req_p	ptu_pedido_autorizacao.cd_unimed_prestador_req%type,
				nr_seq_prestador_req_p		ptu_pedido_autorizacao.nr_seq_prestador_req%type, 
				cd_especialidade_p		ptu_pedido_autorizacao.cd_especialidade%type, 
				ie_urg_emerg_p			ptu_pedido_autorizacao.ie_urg_emerg%type,					
				cd_unimed_executora_p		ptu_pedido_autorizacao.cd_unimed_executora%type,--cabecalho 
				cd_unimed_beneficiario_p	ptu_pedido_autorizacao.cd_unimed_beneficiario%type, --cabecalho  
				cd_unimed_p			ptu_pedido_autorizacao.cd_unimed%type, --Verificar utilização
				nr_via_cartao_p			ptu_pedido_autorizacao.nr_via_cartao%type, 			
				nr_seq_execucao_p		ptu_pedido_autorizacao.nr_seq_execucao%type,
				cd_usuario_plano_p		ptu_pedido_autorizacao.cd_usuario_plano%type, 
				ds_biometria_p			ptu_pedido_autorizacao.ds_biometria%type,
				cd_unimed_prestador_p		ptu_pedido_autorizacao.cd_unimed_prestador%type, 
				qt_dias_doenca_p		ptu_pedido_autorizacao.qt_dias_doenca%type, 
				cd_transacao_p			ptu_pedido_autorizacao.cd_transacao%type,--cabecalho 
				nr_seq_prest_alto_custo_p	ptu_pedido_autorizacao.nr_seq_prest_alto_custo%type,
				nm_prestador_alto_custo_p	ptu_pedido_autorizacao.nm_prestador_alto_custo%type, 
				cd_versao_p			ptu_pedido_autorizacao.nr_versao%type, 
				ie_recem_nascido_p		ptu_pedido_autorizacao.ie_recem_nascido%type, 
				ie_tipo_internacao_p		ptu_pedido_autorizacao.ie_tipo_internacao%type,
				ie_tipo_acidente_p		ptu_pedido_autorizacao.ie_tipo_acidente%type, 
				nm_prof_solicitante_p		ptu_pedido_autorizacao.nm_prof_solicitante%type, --Lista do profissional	
				nr_fone_prof_solic_p		ptu_pedido_autorizacao.nr_fone_prof_solic%type,		
				ds_email_prof_solic_p		ptu_pedido_autorizacao.ds_email_prof_solic%type, 
				cd_unimed_atendimento_p		ptu_pedido_autorizacao.cd_unimed_atendimento%type, 
				ie_anexo_p			ptu_pedido_autorizacao.ie_anexo%type,
				ie_sexo_p			ptu_pedido_autorizacao.ie_sexo%type,
				nr_idade_p			ptu_pedido_autorizacao.nr_idade%type, 
				dt_sug_internacao_p		ptu_pedido_autorizacao.dt_sug_internacao%type,
				ie_ordem_servico_p		ptu_pedido_autorizacao.ie_ordem_servico%type, 
				nr_seq_ordem_p			ptu_pedido_autorizacao.nr_seq_ordem%type, 
				cd_versao_tiss_p		ptu_pedido_autorizacao.nr_versao_tiss%type,
				ds_observacao_p			ptu_pedido_autorizacao.ds_observacao%type,
				ds_ind_clinica_p		ptu_pedido_autorizacao.ds_ind_clinica%type,
				nm_usuario_p			usuario.nm_usuario%type,					
				nr_seq_pedido_aut_p  out 	ptu_pedido_autorizacao.nr_sequencia%type,	
				ie_possui_registro_p out	varchar2) is
				
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a importação do arquivo de 00600 - Pedido de Autorização do PTU 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ x] Tasy (Delphi/Java) [  x] Portal [  ]  Relatórios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

ptu_imp_scs_ws_pck.ptu_imp_cab_pedido_aut( 	dt_atendimento_p, ie_tipo_cliente_p, cd_doenca_cid_p,
						ie_alto_custo_p, cd_unimed_prestador_req_p, nr_seq_prestador_req_p,
						cd_especialidade_p, ie_urg_emerg_p, cd_unimed_executora_p,
						cd_unimed_beneficiario_p, cd_unimed_p, nr_via_cartao_p,
						nr_seq_execucao_p, cd_usuario_plano_p, ds_biometria_p,
						cd_unimed_prestador_p, qt_dias_doenca_p, cd_transacao_p,
						nr_seq_prest_alto_custo_p, nm_prestador_alto_custo_p, cd_versao_p,
						ie_recem_nascido_p, ie_tipo_internacao_p, ie_tipo_acidente_p,
						nm_prof_solicitante_p, nr_fone_prof_solic_p, ds_email_prof_solic_p,
						cd_unimed_atendimento_p, ie_anexo_p, ie_sexo_p,
						nr_idade_p, dt_sug_internacao_p,ie_ordem_servico_p,
						nr_seq_ordem_p, cd_versao_tiss_p, ds_observacao_p,
						ds_ind_clinica_p, nm_usuario_p,	nr_seq_pedido_aut_p,
						ie_possui_registro_p);

end ptu_pedaut_v50_ws;
/