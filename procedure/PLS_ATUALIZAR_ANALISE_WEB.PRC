create or replace
procedure pls_atualizar_analise_web(
			nr_seq_analise_p		Varchar2,
			ie_visualizada_estip_p   	Varchar2 ) is 
		
begin

if	(nr_seq_analise_p is not null) then
	update	pls_auditoria
	set	ie_visualizada_estip = ie_visualizada_estip_p
	where	nr_sequencia = nr_seq_analise_p;
end if;

commit;
end pls_atualizar_analise_web;
/
