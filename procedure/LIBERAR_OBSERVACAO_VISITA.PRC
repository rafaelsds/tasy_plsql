create or replace
procedure liberar_observacao_visita(	nr_sequencia_p		number) is 

begin

update	atend_observacao_visita
set	dt_liberacao 	= sysdate
where	nr_sequencia 	= nr_sequencia_p;

commit;

end liberar_observacao_visita;
/