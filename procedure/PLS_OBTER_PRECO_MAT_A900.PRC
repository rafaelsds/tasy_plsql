create or replace
procedure pls_obter_preco_mat_A900
			(	nr_seq_material_p	in	pls_material.nr_sequencia%type,
				dt_vigencia_p		in	pls_material_unimed.dt_inicio_vigencia_val%type,
				vl_material_p		out	pls_material_unimed.vl_max_consumidor%type) is

vl_max_consumidor_w		pls_material_unimed.vl_max_consumidor%type	:= null;
nr_seq_mat_unimed_w		pls_material_unimed.nr_sequencia%type		:= null;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_vigencia_informada_a900_w	pls_parametros.ie_vigencia_informada_a900%type;
dt_vig_ini_w			pls_material_unimed.dt_inicio_vigencia_val%type;
ie_priorizar_cnv_mat_w		pls_parametros.ie_priorizar_conv_mat%type;

cursor c01(nr_seq_mat_unimed_w pls_material_unimed.nr_sequencia%type) is
	select	nvl(a.vl_max_consumidor,c.vl_max_consumidor) vl_max_consumidor,
		a.dt_inicio_vigencia
	from	pls_material_unimed	c,
		pls_material_uni_val	a
	where	c.nr_sequencia	= nr_seq_mat_unimed_w
	and	c.ie_situacao	= 'A'
	and	c.nr_sequencia	= a.nr_seq_mat_unimed (+)
	order 	by nvl(a.dt_inicio_vigencia,c.dt_atualizacao); -- Edgar 11/11/2013, OS 633529, deixar dt_atualizacao enquanto n?o houver vig?ncia na TNUNN

begin
	
if (cd_estabelecimento_w is null) then
	select	nvl(max(ie_vigencia_informada_a900),'N'),
		nvl(max(ie_priorizar_conv_mat),'N')
	into	ie_vigencia_informada_a900_w,
		ie_priorizar_cnv_mat_w
	from	pls_parametros;
else
	select	nvl(max(ie_vigencia_informada_a900),'N'),
		nvl(max(ie_priorizar_conv_mat),'N')
	into	ie_vigencia_informada_a900_w,
		ie_priorizar_cnv_mat_w
	from	pls_parametros
	where	cd_estabelecimento = cd_estabelecimento_w;
end if;

--Se estiver parametrizado para nao priorizar conversao a900, entao busca diretamente do material primeiramente e so se nao achar, busca 
--via conversao
if ( ie_priorizar_cnv_mat_w = 'N') then
	select	max(nr_seq_material_unimed)
	into	nr_seq_mat_unimed_w
	from	pls_material
	where	nr_sequencia	= nr_seq_material_p;
end if;

if	(nr_seq_mat_unimed_w is null) then
	
	--Se parametrizado para vig?ncia informada A900, ent?o deve buscar maior data inicio vig?ncia da pls_material_unimed que seja
	--inferior a data vig?ncia passada por par?metro
	if (ie_vigencia_informada_a900_w = 'S') then
	
		dt_vig_ini_w := fim_dia(dt_vigencia_p);
	
		
		-- tenta buscar pelo sequencial do material unimed, necessario ap?s a Unimed Brasil enviar o cd_material duplicado
		with query_tmp as(
			select	t.nr_sequencia,
				t.dt_inicio_vigencia
			from (	select	b.nr_sequencia,
					c.dt_inicio_vigencia
				from	pls_material_a900		a,
					pls_material_unimed		b,
					pls_material_uni_val		c
				where	a.nr_seq_material		= nr_seq_material_p
				and	b.nr_sequencia			= a.nr_seq_material_unimed
				and	c.nr_seq_mat_unimed		= b.nr_sequencia
				and	c.dt_inicio_vigencia		< dt_vig_ini_w
				and	a.dt_inicio_vigencia		< dt_vig_ini_w
				and	a.dt_fim_vigencia		is null
				union all
				select	b.nr_sequencia,
					c.dt_inicio_vigencia
				from	pls_material_a900		a,
					pls_material_unimed		b,
					pls_material_uni_val		c
				where	a.nr_seq_material		= nr_seq_material_p
				and	b.nr_sequencia			= a.nr_seq_material_unimed
				and	c.nr_seq_mat_unimed		= b.nr_sequencia
				and	c.dt_inicio_vigencia		< dt_vig_ini_w
				and	a.dt_inicio_vigencia		< dt_vig_ini_w
				and	a.dt_fim_vigencia		>= trunc(dt_vig_ini_w, 'dd') -- ate a data limite
			) t
			order by t.dt_inicio_vigencia desc
		)
		select 	max(nr_sequencia)
		into	nr_seq_mat_unimed_w
		from	query_tmp
		where	rownum = 1;
						
		-- se nao conseguiu achar o sequencia, tenta pelo comportamento "padr?o" anterior a altera??o
		if	(nr_seq_mat_unimed_w is null) then
		
			with query_tmp as(
				select	t.nr_sequencia,
					t.dt_inicio_vigencia
				from (	select	b.nr_sequencia,
						c.dt_inicio_vigencia
					from	pls_material_a900		a,
						pls_material_unimed		b,
						pls_material_uni_val		c
					where	a.nr_seq_material		= nr_seq_material_p
					and	b.cd_material			= a.cd_material_a900
					and	c.nr_seq_mat_unimed		= b.nr_sequencia
					and	c.dt_inicio_vigencia		< dt_vig_ini_w
					and	a.dt_inicio_vigencia		< dt_vig_ini_w
					and	a.dt_fim_vigencia		is null
					union all
					select	b.nr_sequencia,
						c.dt_inicio_vigencia
					from	pls_material_a900		a,
						pls_material_unimed		b,
						pls_material_uni_val		c
					where	a.nr_seq_material		= nr_seq_material_p
					and	b.cd_material			= a.cd_material_a900
					and	c.nr_seq_mat_unimed		= b.nr_sequencia
					and	c.dt_inicio_vigencia		< dt_vig_ini_w
					and	a.dt_inicio_vigencia		< dt_vig_ini_w
					and	a.dt_fim_vigencia		>= trunc(dt_vig_ini_w, 'dd')
				) t
				order by t.dt_inicio_vigencia desc
			)
			select 	max(nr_sequencia)
			into	nr_seq_mat_unimed_w
			from	query_tmp
			where	rownum = 1;
		end if;
	
	else
		select	max(b.nr_sequencia)
		into	nr_seq_mat_unimed_w
		from	pls_material_a900	a,
			pls_material_unimed	b
		where	a.nr_seq_material	= nr_seq_material_p
		and	b.cd_material		= a.cd_material_a900
		AND 	a.dt_fim_vigencia is null;
		
		if	(nr_seq_mat_unimed_w	is null) then
			select	max(b.nr_sequencia)
			into	nr_seq_mat_unimed_w
			from	pls_material_a900	a,
				pls_material_unimed	b
			where	a.nr_seq_material	= nr_seq_material_p
			and	b.cd_material		= a.cd_material_a900;
		end if;
	end if;
end if;

--Se prioriza pela conversao, porem nao achou nenhuma valida, entao ainda tneta obter diretamente do material
if ( ie_priorizar_cnv_mat_w = 'S' and nr_seq_mat_unimed_w is null) then
	select	max(nr_seq_material_unimed)
	into	nr_seq_mat_unimed_w
	from	pls_material
	where	nr_sequencia	= nr_seq_material_p;
end if;

for r_c01_w in c01(nr_seq_mat_unimed_w) loop
	if	(r_c01_w.dt_inicio_vigencia is null) or
		(r_c01_w.dt_inicio_vigencia <= dt_vigencia_p) then
		vl_max_consumidor_w := r_c01_w.vl_max_consumidor;
	end if;
end loop;

vl_material_p := nvl(vl_max_consumidor_w,0);

end pls_obter_preco_mat_A900;
/