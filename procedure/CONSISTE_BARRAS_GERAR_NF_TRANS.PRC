create or replace
procedure consiste_barras_gerar_nf_trans(
			cd_estabelecimento_p	in 	number,
			nr_ordem_compra_p	in 	number,
			ie_entrada_saida_p		in 	varchar2,
			ie_item_nf_p		in	number,
			ds_retorno_p		out	varchar2) is 

qt_existe_w	number(10);
cd_material_w	number(10);
nr_item_oci_w	number(5);
ds_material_w	varchar2(255);
ds_retorno_w	varchar2(4000);
			
cursor c01 is
select	a.nr_item_oci,
	a.cd_material,
	substr(obter_desc_material(a.cd_material),1,255)
from	ordem_compra_item a,
	ordem_compra_item_entrega b
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_item_oci = b.nr_item_oci
and	substr(obter_se_leitura_barras(cd_estabelecimento_p, a.cd_material, 146),1,1) = 'S'
and	nvl(a.qt_material,0) > nvl(a.qt_material_entregue,0)
and	a.dt_reprovacao is null
and	b.dt_cancelamento is null
and	a.nr_ordem_compra = nr_ordem_compra_p
and	ie_entrada_saida_p = 'S'
and	ie_item_nf_p = 0
group by	a.nr_item_oci,
	a.cd_material
having	((sum(b.qt_prevista_entrega) - max(obter_qt_oci_trans_nota(a.nr_ordem_compra, a.nr_item_oci,'S'))) > 0)
union all
select	nr_item_oci,
	cd_material,
	substr(obter_desc_material(cd_material),1,255)
from	ordem_compra_item_cb
where	nvl(ie_status,'CB') = 'CM' 
and	substr(obter_se_leitura_barras(cd_estabelecimento_p, cd_material, 146),1,1) = 'S'
and	ie_atende_recebe = 'A'
and	nr_seq_nota is null
and	nr_ordem_compra = nr_ordem_compra_p
and	ie_item_nf_p = 1
group by nr_item_oci,
	cd_material
order by	nr_item_oci;

begin
ds_retorno_w := null;

select	count(*)
into	qt_existe_w
from	regra_leitura_barra_mat
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(qt_existe_w > 0) then
	begin
	open c01;
	loop
	fetch c01 into	
		nr_item_oci_w,
		cd_material_w,
		ds_material_w;
	exit when c01%notfound;
		begin
		ds_retorno_w := substr(ds_retorno_w || '[' || nr_item_oci_w || '] - ' || cd_material_w || ' - ' || ds_material_w || chr(13) || chr(10),1,4000);
		end;
	end loop;
	close c01;
	end;
end if;

if	(ds_retorno_w is not null) then
	-- Os itens abaixo n�o ser�o gerados na NF pois exigem leitura por barras!
	ds_retorno_w := substr(wheb_mensagem_pck.get_texto(298642,'DS_RETORNO='||ds_retorno_w),1,4000);
end if;
	
ds_retorno_p := ds_retorno_w;
end consiste_barras_gerar_nf_trans;
/