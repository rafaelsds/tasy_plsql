create or replace
procedure atualizar_nut_pac_elem_kg_dia (nr_sequencia_p		number,
				     qt_elem_kg_dia_p	number ) is

begin

if (nr_sequencia_p is not null) and
	(qt_elem_kg_dia_p > 0 ) then
	
	update	nut_pac_elemento
	set	qt_elem_kg_dia	= qt_elem_kg_dia_p
	where	nr_sequencia	= nr_sequencia_p;
	
	commit;
	
end if;

end atualizar_nut_pac_elem_kg_dia;
/