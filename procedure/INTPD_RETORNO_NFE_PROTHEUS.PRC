create or replace
procedure intpd_retorno_nfe_protheus (nr_sequencia_p in number, ds_xml_p	in clob) is 

nota_fiscal_w		nota_fiscal%rowtype;
ie_conversao_w		intpd_eventos_sistema.ie_conversao%type;
nr_seq_projeto_xml_w	intpd_eventos_sistema.nr_seq_projeto_xml%type;
nr_seq_sistema_w	intpd_eventos_sistema.nr_seq_sistema%type;
ie_sistema_externo_w	varchar2(15);
reg_integracao_w	gerar_int_padrao.reg_integracao_conv;
nr_seq_regra_w		conversao_meio_externo.nr_seq_regra%type;
ds_erro_w		varchar2(4000);
qt_reg_w		number(10);
ds_xml_w		clob;
ds_xml_type_w		xmltype;
cursor c01 is
select *
from xmltable('/STRUCTURE/INVOICE' passing ds_xml_type_w columns
	nr_sequencia		number(10)	path 	'NR_SEQUENCE',
	nr_nfe_imp		varchar2(40)	path	'NR_INVOICE');
	
c01_w	c01%rowtype;

begin

ds_xml_w := replace(replace(ds_xml_p, '<phil:integration>', ''), '</phil:integration>', '');
ds_xml_type_w := XMLTYPE.createXML(ds_xml_w);

update	intpd_fila_transmissao
set	ie_status = 'R'
where	nr_sequencia = nr_sequencia_p;

commit;
	
begin
	
intpd_reg_integracao_inicio(nr_sequencia_p, 'R', reg_integracao_w);

open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin

	reg_integracao_w.nm_tabela	:=	'NOTA_FISCAL';
	reg_integracao_w.nm_elemento	:=	'INVOICE';
	reg_integracao_w.nr_seq_visao	:=	null;
	
	intpd_processar_atributo(reg_integracao_w, 'NR_NFE_IMP', c01_w.nr_nfe_imp, 'S', nota_fiscal_w.nr_nfe_imp);
	intpd_processar_atributo(reg_integracao_w, 'NR_SEQUENCIA', c01_w.nr_sequencia, 'N', nota_fiscal_w.nr_sequencia);
	
	select	count(*)
	into	qt_reg_w
	from	nota_fiscal
	where	nr_sequencia = nota_fiscal_w.nr_sequencia;
	
	if (qt_reg_w > 0) then
	
		if	(reg_integracao_w.qt_reg_log = 0) then
		begin
			update	nota_fiscal
			set	nr_nfe_imp = nota_fiscal_w.nr_nfe_imp
			where	nr_sequencia = nota_fiscal_w.nr_sequencia;
		
			update	intpd_fila_transmissao
			set	ie_status = 'S',
				ie_response_procedure	= 'S',
				ds_xml_retorno		= ds_xml_p
			where	nr_sequencia = nr_sequencia_p;
			
		exception
		when others then
			ds_erro_w	:=	substr(sqlerrm,1,4000);	
		end;
		end if;
	else 
		ds_erro_w := wheb_mensagem_pck.get_Texto(1110267);
	end if;
	end;
end loop;
close C01;
exception
when others then
	ds_erro_w  := substr(sqlerrm, 1 , 4000);
end;

if	(nvl(ds_erro_w, 'NULL') <> 'NULL') then
	begin

	rollback;
	
	update intpd_fila_transmissao
	set	ie_status = 'E',
		ie_response_procedure	= 'S',
		ds_xml_retorno		= ds_xml_p,
		ds_log = ds_erro_w
	where	nr_sequencia = nr_sequencia_p;
	
	end;
end if;

commit;

end intpd_retorno_nfe_protheus;
/
