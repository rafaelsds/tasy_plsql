CREATE OR REPLACE
PROCEDURE marca_posic_importante(nr_posic_p NUMBER) IS


BEGIN


 UPDATE proj_posicao_coordenacao
 SET ie_importante  = 'S'
 WHERE nr_sequencia = nr_posic_p;

 COMMIT;


END marca_posic_importante;
/