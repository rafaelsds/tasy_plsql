create or replace
procedure atualizar_reg_grav_resp(
		nr_sequencia_p	number,
		nm_usuario_p	varchar2) is 

begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	eme_reg_grav_resp
	set	ie_resposta 	= decode(nvl(ie_resposta,'N'),'N','S','N'),
		dt_atualizacao 	= sysdate,
		nm_usuario 	= nm_usuario_p
	where	nr_sequencia 	= nr_sequencia_p;
	end;
end if;
commit;
end atualizar_reg_grav_resp;
/