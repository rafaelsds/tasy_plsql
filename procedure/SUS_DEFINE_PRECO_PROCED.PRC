create or replace
procedure sus_define_preco_proced
			(	dt_procedimento_p			date,
				cd_procedimento_p			number,
				ie_origem_proced_p		number,
				cd_estabelecimento_p		number,
				ie_tipo_atendimento_p		number,
				vl_sa_p			out	number,
				vl_sh_p			out	number,
				vl_sp_p			out	number,
				vl_sadt_p			out	number,
				vl_total_hospitalar_p	out	number,
				vl_total_amb_p		out	number,
				qt_pontos_ato_p		out	number,
				vl_honorario_medico_p	out	number,
				vl_matmed_p		out	number,
				vl_anestesista_p		out	number,
				dt_competencia_p		out	date) is


vl_sa_w				number(15,2)	:= 0;
vl_sh_w				number(15,2)	:= 0;
vl_sp_w				number(15,2)	:= 0;
vl_sadt_w			number(15,2)	:= 0;
vl_total_hospitalar_w		number(15,2)	:= 0;
vl_total_amb_w			number(15,2)	:= 0;
qt_pontos_ato_w			number(5)		:= 0;
vl_honorario_medico_w		number(11,2)	:= 0;
vl_matmed_w			number(11,2)	:= 0;
vl_anestesista_w			number(15,2)	:= 0;
dt_competencia_sus_w		date;
dt_competencia_proc_w		date;
ie_tipo_financiamento_w		varchar2(4);
qt_registros_w			number(5)		:= 0;
dt_competencia_aih_w		date;
dt_comp_apac_bpa_w		date;
ie_data_proc_comp_preco_w		varchar2(4) 	:= 'N';
cd_municipio_ibge_w		sus_preco_municipio.cd_municipio_ibge%type := 'X';

begin

begin
select	nvl(max(dt_competencia_sus), sysdate),
	nvl(nvl(max(dt_comp_apac_bpa), max(dt_competencia_sus)),sysdate),
	nvl(max(ie_data_proc_comp_preco),'N')
into	dt_competencia_aih_w,
	dt_comp_apac_bpa_w,
	ie_data_proc_comp_preco_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_p;
exception
	when others then
	dt_competencia_aih_w		:= sysdate;
	dt_comp_apac_bpa_w		:= sysdate;
	ie_data_proc_comp_preco_w		:= 'N';
end;

if	(ie_tipo_atendimento_p	<> 1) then
	begin
	dt_competencia_sus_w	:= dt_comp_apac_bpa_w;
	end;
else
	begin
	dt_competencia_sus_w	:= dt_competencia_aih_w;
	end;
end if;

ie_tipo_financiamento_w		:= obter_tipo_financ_proc(cd_procedimento_p, ie_origem_proced_p, 1);

begin
select	nvl(obter_dados_pf_pj(null,cd_cgc,'CDM'),'X')
into	cd_municipio_ibge_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;
exception
when others then
	cd_municipio_ibge_w := 'X';
end;

select	count(1)
into	qt_registros_w
from	sus_preco_municipio
where	cd_procedimento		= cd_procedimento_p
and	ie_origem_proced	= ie_origem_proced_p
and	nvl(cd_municipio_ibge,cd_municipio_ibge_w) 	= cd_municipio_ibge_w
and	nvl(cd_estabelecimento,cd_estabelecimento_p)	= cd_estabelecimento_p
and	rownum < 2;

if	(qt_registros_w	> 0) then
	begin	
	sus_define_preco_proced_pab(dt_competencia_sus_w, cd_procedimento_p, ie_origem_proced_p, cd_estabelecimento_p,
					vl_total_amb_w, vl_sh_w, vl_sp_w, vl_total_hospitalar_w);

	vl_honorario_medico_w	:= vl_sp_w;
	vl_matmed_w		:= vl_sh_w;
	end;
elsif	(ie_data_proc_comp_preco_w = 'S') then
	begin
	
	select	max(pkg_date_utils.get_time(dt_competencia,0,0,0))
	into	dt_competencia_proc_w
	from 	sus_preco
	where	dt_competencia 		<= dt_procedimento_p
	and	cd_procedimento 	= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p
	and	(((ie_aih = 'S') and (ie_tipo_atendimento_p = 1)) or
		((ie_apac = 'S') and (ie_tipo_atendimento_p <> 1)) or
		((ie_bpa = 'S') and (ie_tipo_atendimento_p <> 1)));
	
	begin
	select	nvl(vl_sa,0),
		nvl(vl_sh,0),
		nvl(vl_sp,0),
		nvl(vl_sadt,0),
		nvl(vl_total_hospitalar,0),
		nvl(vl_total_amb,0),
		nvl(qt_pontos_ato,0),
		nvl(vl_honorario_medico,0),
		nvl(vl_matmed,0),
		nvl(vl_anestesia,0)
	into	vl_sa_w,
		vl_sh_w,
		vl_sp_w,
		vl_sadt_w,
		vl_total_hospitalar_w,
		vl_total_amb_w,
		qt_pontos_ato_w,
		vl_honorario_medico_w,
		vl_matmed_w,
		vl_anestesista_w
	from	sus_preco
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced		= ie_origem_proced_p
	and	(((ie_aih = 'S') and (ie_tipo_atendimento_p = 1)) or
		((ie_apac = 'S') and (ie_tipo_atendimento_p <> 1)) or
		((ie_bpa = 'S') and (ie_tipo_atendimento_p <> 1)))
	and	pkg_date_utils.get_time(dt_competencia,0,0,0)	= dt_competencia_proc_w;
	exception
		when others then
	        vl_total_hospitalar_w	:= 0;
	end;
	
	end;
else
	begin
	select	nvl(vl_sa,0),
		nvl(vl_sh,0),
		nvl(vl_sp,0),
		nvl(vl_sadt,0),
		nvl(vl_total_hospitalar,0),
		nvl(vl_total_amb,0),
		nvl(qt_pontos_ato,0),
		nvl(vl_honorario_medico,0),
		nvl(vl_matmed,0),
		nvl(vl_anestesia,0)
	into	vl_sa_w,
		vl_sh_w,
		vl_sp_w,
		vl_sadt_w,
		vl_total_hospitalar_w,
		vl_total_amb_w,
		qt_pontos_ato_w,
		vl_honorario_medico_w,
		vl_matmed_w,
		vl_anestesista_w
	from	sus_preco
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced		= ie_origem_proced_p
	and	pkg_date_utils.get_time(dt_competencia,0,0,0)	=  dt_competencia_sus_w;
	exception
	when others then
		 vl_total_hospitalar_w	:= 0;
	end;
end if;

vl_sa_p			:= vl_sa_w;
vl_sh_p			:= vl_sh_w;
vl_sp_p			:= vl_sp_w;
vl_sadt_p			:= vl_sadt_w;
vl_total_hospitalar_p	:= vl_total_hospitalar_w;
vl_total_amb_p		:= vl_total_amb_w;
qt_pontos_ato_p		:= qt_pontos_ato_w;
vl_honorario_medico_p	:= vl_honorario_medico_w;
vl_matmed_p		:= vl_matmed_w;
dt_competencia_p		:= dt_competencia_sus_w;
vl_anestesista_p		:= vl_anestesista_w;

end sus_define_preco_proced;
/
