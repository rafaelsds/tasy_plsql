create or replace procedure insert_ihc_response(nr_seq_claim_p number,
												cd_transaction_p varchar2,
												cd_status_p varchar2,
												nr_account_p varchar2,
												cd_clm_asses_fund_p varchar2,
												vl_co_pay_p number,
												vl_excess_amt_p number,
												cd_facility_p  varchar2,
												cd_fund_brand_p varchar2,
												cd_fund_ref_p varchar2,
												cd_fund_status_p number,
												vl_total_benefit_p number,
												vl_charge_p number,
												nm_usuario_p varchar2) is

nm_usuario_w 		ihc_response.nm_usuario%type;
nr_sequencia_w 		ihc_response.nr_seq_claim%type;
count_w				pls_integer := 0;

begin
	select	max(nr_sequencia),
			nm_usuario
	into 	nr_sequencia_w,
			nm_usuario_w
	from	ihc_claim 
	where 	cd_transaction = cd_transaction_p
	group by nr_sequencia, nm_usuario;

	select count(1)
	into count_w
	from ihc_response
	where nr_seq_claim = nr_sequencia_w;

	if (count_w > 0) then
		delete from ihc_service_details
		where nr_seq_service in (select 	a.nr_sequencia
								from 	ihc_service_response a,
										ihc_response b
								where 	b.nr_seq_claim = nr_sequencia_w);

		delete from ihc_service_response
		where nr_seq_response in (	select nr_sequencia
									from ihc_response
									where nr_seq_claim = nr_sequencia_w);

		delete from ihc_response_details
		where nr_seq_response in (	select nr_sequencia
									from ihc_response
									where nr_seq_claim = nr_sequencia_w);

		delete from ihc_response
		where nr_seq_claim = nr_sequencia_w;
	end if;

	insert into ihc_response(nr_sequencia,
							 dt_atualizacao,
							 nm_usuario,
							 dt_atualizacao_nrec,
							 nm_usuario_nrec,
							 nr_seq_claim,
							 cd_transaction,
							 cd_status,
							 nr_account,
							 cd_clm_asses_fund,
							 vl_co_pay,
							 vl_excess_amt,
							 cd_facility,
							 cd_fund_brand,
							 cd_fund_ref,
							 cd_fund_status,
							 vl_total_benefit,
							 vl_charge)
					 values(ihc_response_seq.nextval,
							sysdate,
							nvl(nm_usuario_p, nm_usuario_w),
							sysdate,
							nvl(nm_usuario_p, nm_usuario_w),
							nvl(nr_sequencia_w, nr_seq_claim_p),
							cd_transaction_p,
							cd_status_p,
							nr_account_p,
							cd_clm_asses_fund_p,
							vl_co_pay_p,
							vl_excess_amt_p,
							cd_facility_p,
							cd_fund_brand_p,
							cd_fund_ref_p,
							cd_fund_status_p,
							vl_total_benefit_p,
							vl_charge_p);
	commit;

end insert_ihc_response;
/