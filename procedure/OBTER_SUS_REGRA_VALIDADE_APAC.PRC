create or replace
function obter_sus_regra_validade_apac(cd_procedimento_p	procedimento.cd_procedimento%type,
							ie_origem_proced_p procedimento.ie_origem_proced%type)
 		    	return number is
				
retorno_w number(5);

begin

select nvl(max(qt_meses_validade),0)
into retorno_w
from sus_regra_validade_apac
where ie_situacao = 'A'
and  cd_procedimento = cd_procedimento_p
and ie_origem_proced = ie_origem_proced_p;

if (retorno_w > 0) then
	retorno_w := retorno_w -1;
end if;

return	retorno_w;

end obter_sus_regra_validade_apac;
/