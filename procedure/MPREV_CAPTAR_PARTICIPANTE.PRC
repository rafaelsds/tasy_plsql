create or replace procedure mprev_captar_participante(	ds_list_nr_seq_capt_p	varchar2,
					nm_usuario_p		Varchar2) is
					
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Inserir as pessoas da captacao nos programas e campanhas selecionados
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionario [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_programa_w	number(10);
nr_seq_campanha_w	number(10);
nr_seq_turma_w		number(10);
nr_seq_participante_w	mprev_participante.nr_sequencia%type;
ie_incluso_w		varchar2(1);
ie_incluso_grupo_w	varchar2(1);
nm_incluso_w		varchar2(255);
nm_incluso_grupo_w	varchar2(255);
ie_origem_w         mprev_captacao.ie_origem%type;
dt_inclusao_w		mprev_captacao_sel_dest.dt_inclusao%type;
nr_seq_captacao_w       mprev_captacao.nr_sequencia%type;
nr_seq_pop_alvo_pessoa_w    mprev_captacao.nr_seq_pop_alvo_pessoa%type;
ie_momento_envio_w      mprev_regra_envio_mensagem.ie_momento_envio%type;
nr_seq_envio_mens_w     mprev_regra_cubo.nr_seq_envio_mens%type;
nm_regra_w              mprev_regra_cubo.nm_regra%type;

Cursor c01 is
	select	nr_seq_programa,
		nr_seq_campanha,
		nr_seq_turma,
		dt_inclusao
	from 	mprev_captacao_sel_dest
	where	nr_seq_captacao = nr_seq_captacao_w
	order by
		nvl(nr_seq_turma,0),
		nvl(nr_seq_campanha,0),
		nvl(nr_seq_programa,0);

Cursor C02 is
	select	nr_sequencia,
                cd_pessoa_fisica
	from	mprev_captacao
	WHERE Obter_Se_Contido(nr_sequencia, ds_list_nr_seq_capt_p) = 'S';

begin

for r_C02 in C02 loop
	begin

        select	max(a.nr_sequencia) 
        into	nr_seq_participante_w
        from	mprev_participante a
        where	a.cd_pessoa_fisica = r_C02.cd_pessoa_fisica;
        
        dbms_output.put_line('nr_seq_participante_w: ' || nr_seq_participante_w);
        
        
        nr_seq_captacao_w := r_C02.nr_sequencia;
        

        open C01;
        loop
        fetch C01 into	
                nr_seq_programa_w,
                nr_seq_campanha_w,
                nr_seq_turma_w,
                dt_inclusao_w;

        exit when C01%notfound;
                begin
                if	(nr_seq_programa_w is not null) or
                        (nr_seq_campanha_w is not null) then

                        mprev_insere_participante
                                        (nr_seq_captacao_w,
                                        null,
                                        nr_seq_programa_w,
                                        nr_seq_campanha_w,
                                        nm_usuario_p,
                                        ie_incluso_w,
                                        nm_incluso_w,
                                        dt_inclusao_w);
                end if;
                if	(nr_seq_participante_w is not null) and
                        (nr_seq_turma_w is not null) then

                        mprev_inserir_partic_turma
                                        (nr_seq_participante_w,
                                        nr_seq_turma_w,
                                        dt_inclusao_w,
                                        nm_usuario_p,
                                        ie_incluso_grupo_w,
                                        nm_incluso_grupo_w);
                end if;
                end;
        end loop;
        close C01;

        update	mprev_captacao
        set	ie_status = 'A'
        where	nr_sequencia = nr_seq_captacao_w;
        
        select nvl(ie_origem, ''), nvl(nr_seq_pop_alvo_pessoa, 0) 
        into   ie_origem_w, nr_seq_pop_alvo_pessoa_w
        from   mprev_captacao 
        where  nr_sequencia = nr_seq_captacao_w;
        
        if(ie_origem_w = 'BA' and nr_seq_pop_alvo_pessoa_w > 0) then --Busca ativa e tem pop alvo relacionada
            --Regra de envio de mensagem vinculada
            select nvl(nr_seq_envio_mens, 0), nvl(nm_regra, '')
            into   nr_seq_envio_mens_w , nm_regra_w
            from   mprev_regra_cubo 
            where  nr_sequencia = (select nr_seq_regra_cubo 
                                   from   mprev_populacao_alvo 
                                   where  nr_sequencia = (select nr_seq_populacao_alvo 
                                                          from   mprev_pop_alvo_pessoa 
                                                          where  nr_sequencia = nr_seq_pop_alvo_pessoa_w));
            
            if (nr_seq_envio_mens_w > 0) then
              select ie_momento_envio
              into   ie_momento_envio_w
              from   mprev_regra_envio_mensagem
              where  nr_sequencia = nr_seq_envio_mens_w;
              
              if (ie_momento_envio_w = 'AC') then 
                  mprev_enviar_email_sms(r_C02.cd_pessoa_fisica , nr_seq_envio_mens_w, nm_regra_w, '7813', nm_usuario_p);
              end if;
            end if;
        end if;
	end;
end loop;

commit;

end mprev_captar_participante;
/
