create or replace
procedure pls_dividir_faturas_prest
			(	nr_seq_lote_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

cd_operadora_empresa_w		varchar2(4000);
cd_cgc_w			varchar2(4000);
ie_tipo_relacao_w		varchar2(2);
ie_tipo_relacao_ant_w		varchar2(2);
ie_tipo_relacao_prest_w		varchar2(2);
vl_faturado_w			number(15,2);
nr_seq_fatura_w			number(10);
nr_seq_pagador_w		number(10);
nr_seq_congenere_w		number(10);
nr_seq_conta_w			number(10);
nr_seq_fatura_nova_w		number(10)	:= null;
nr_seq_fatura_evento_w		number(10);
nr_seq_evento_fatura_w		number(10);
nr_seq_evento_w			number(10);
nr_seq_segurado_w		number(10);
nr_seq_conta_prest_w		number(10);
nr_seq_evento_prest_w		number(10);
nr_seq_fatura_conta_w		number(10);
nr_seq_prestador_exec_w		number(10);
nr_seq_grupo_prestador_w	number(10);
nr_seq_regra_w			number(10);
dt_mes_competencia_w		date;
qt_registro_w			pls_integer;
	
Cursor C01 is
	select	a.nr_sequencia,
		a.ie_tipo_relacao,
		a.nr_seq_grupo_prestador
	from	pls_regra_divisao_prest a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	trunc(sysdate,'dd') between trunc(a.dt_inicio_vigencia,'dd') and fim_dia(a.dt_fim_vigencia)
	and	((a.ie_tipo_relacao	= ie_tipo_relacao_prest_w) or (a.ie_tipo_relacao is null))
	and	((pls_obter_se_prestador_grupo(a.nr_seq_grupo_prestador, nr_seq_prestador_exec_w) = 'S') or (a.nr_seq_grupo_prestador is null))
	order by
		nvl(a.ie_tipo_relacao, ' '),
		nvl(a.nr_seq_grupo_prestador, 0),
		nvl(a.dt_inicio_vigencia,sysdate) desc;

Cursor 	C02 is
	select	a.nr_sequencia,
		a.nr_seq_pagador,
		a.nr_seq_congenere
	from	pls_fatura		a		
	where	a.nr_seq_lote	= nr_seq_lote_p
	and	a.ie_impedimento_cobranca is null
	order by
		3;

Cursor 	C03 is
	select	b.nr_seq_conta,
		a.nr_seq_evento,
		a.nr_sequencia,
		b.nr_sequencia,
		d.ie_tipo_relacao,
		d.nr_sequencia
	from	pls_prestador		d,
		pls_conta		c,
		pls_fatura_conta 	b,
		pls_fatura_evento 	a
	where	a.nr_seq_fatura = nr_seq_fatura_w	
	and	a.nr_sequencia	= b.nr_seq_fatura_evento
	and	c.nr_sequencia	= b.nr_seq_conta
	and	d.nr_sequencia	= c.nr_seq_prestador_exec
	order by
		d.ie_tipo_relacao;

begin
if	(nr_seq_lote_p is not null) then	
	select	a.dt_mesano_referencia
	into	dt_mes_competencia_w
	from	pls_lote_faturamento a
	where	a.nr_sequencia = nr_seq_lote_p;
	
	select	count(1)
	into	qt_registro_w
	from	pls_regra_divisao_prest
	where	cd_estabelecimento	= cd_estabelecimento_p;

	if	(qt_registro_w > 0) then
		open C02;
		loop
		fetch C02 into	
			nr_seq_fatura_w,
			nr_seq_pagador_w,
			nr_seq_congenere_w;
		exit when C02%notfound;
			begin
			nr_seq_fatura_nova_w	:= null;
			ie_tipo_relacao_ant_w	:= null;
			
			open C03;
			loop
			fetch C03 into	
				nr_seq_conta_w,
				nr_seq_evento_w,
				nr_seq_fatura_evento_w,
				nr_seq_fatura_conta_w,
				ie_tipo_relacao_prest_w,
				nr_seq_prestador_exec_w;
			exit when C03%notfound;
				begin
				ie_tipo_relacao_w		:= null;
				nr_seq_grupo_prestador_w	:= null;
				nr_seq_regra_w			:= null;
				
				open C01;
				loop
				fetch C01 into	
					nr_seq_regra_w,
					ie_tipo_relacao_w,
					nr_seq_grupo_prestador_w;
				exit when C01%notfound;
				end loop;
				close C01;
				
				if	(nr_seq_regra_w is not null) then
					ie_tipo_relacao_w	:= nvl(ie_tipo_relacao_prest_w, ie_tipo_relacao_w);
					
					/*Verifica se existe uma fatura com estas informaes */
					select	max(a.nr_sequencia)
					into	nr_seq_evento_prest_w
					from	pls_fatura_conta 	b,
						pls_fatura_evento 	a,
						pls_fatura		f
					where	f.nr_sequencia			= a.nr_seq_fatura
					and	a.nr_sequencia			= b.nr_seq_fatura_evento
					and	f.nr_sequencia			<> nr_seq_fatura_w
					and	f.nr_seq_pagador 		= nr_seq_pagador_w
					and	f.ie_tipo_relacao 		= ie_tipo_relacao_w
					and	((f.nr_seq_grupo_prestador	= nr_seq_grupo_prestador_w) or (nr_seq_grupo_prestador_w is null))
					and	a.nr_seq_evento			= nr_seq_evento_w
					and	f.nr_seq_lote  			= nr_seq_lote_p;			
					
					if	(nr_seq_evento_prest_w is null) then								
						/*Cria uma nova fatura */
						select	pls_fatura_seq.nextval
						into	nr_seq_fatura_nova_w
						from	dual;
						
						insert into pls_fatura
							(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_seq_pagador,
							nr_seq_lote,
							dt_vencimento,
							nr_seq_congenere,
							dt_mes_competencia,
							vl_fatura,
							dt_vencimento_ndc,
							vl_total_ndc,
							ie_tipo_relacao,
							nr_seq_grupo_prestador,
							nr_fatura,
							nr_seq_fat_divisao,
							ie_tipo_fatura)
						select	nr_seq_fatura_nova_w,
							sysdate,
							nm_usuario_p,
							sysdate,
							nm_usuario_p,
							nr_seq_pagador,
							nr_seq_lote,
							dt_vencimento,
							nr_seq_congenere,
							dt_mes_competencia,
							0,
							dt_vencimento_ndc,
							0,
							ie_tipo_relacao_prest_w,
							nr_seq_grupo_prestador_w,
							nr_seq_fatura_nova_w,
							nr_seq_fatura_w,
							ie_tipo_fatura
						from	pls_fatura
						where	nr_sequencia = nr_seq_fatura_w;
					else
						nr_seq_fatura_nova_w	:= null;
					end if;
					
					if	(nr_seq_fatura_nova_w is not null) then
						select	max(nr_sequencia)
						into	nr_seq_evento_fatura_w
						from 	pls_fatura_evento
						where	nr_seq_fatura = nr_seq_fatura_nova_w
						and	nr_seq_evento = nr_seq_evento_w;
						
						if	(nr_seq_evento_fatura_w is null) then
							select	pls_fatura_evento_seq.nextval
							into	nr_seq_evento_fatura_w
							from	dual;
							
							insert into pls_fatura_evento
								(nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_seq_evento,
								nr_seq_fatura,
								vl_evento)
							values	(nr_seq_evento_fatura_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_seq_evento_w,
								nr_seq_fatura_nova_w,
								0);
						end if;				
						
						/* Atualizar a conta para a fatura nova */
						update	pls_fatura_conta
						set	nr_seq_fatura_evento 	= nr_seq_evento_fatura_w
						where	nr_seq_conta 		= nr_seq_conta_w
						and	nr_seq_fatura_evento 	= nr_seq_fatura_evento_w;
						
					elsif 	(nr_seq_evento_prest_w is not null) then
						update	pls_fatura_conta
						set	nr_seq_fatura_evento 	= nr_seq_evento_prest_w
						where	nr_seq_conta 		= nr_seq_conta_w
						and	nr_seq_fatura_evento 	= nr_seq_fatura_evento_w;
					end if;
				end if;
				end;
			end loop;
			close C03;
			end;
			commit;
		end loop;
		close C02;
		commit;
	end if;
		
	if	(nr_seq_fatura_nova_w is not null) then	
		pls_atualizar_vl_lote_fatura(	nr_seq_lote_p, nm_usuario_p, 'N', 'S');
	end if;
end if;

end pls_dividir_faturas_prest;
/
