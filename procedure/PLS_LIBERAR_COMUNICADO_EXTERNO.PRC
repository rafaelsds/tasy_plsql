create or replace
procedure pls_liberar_comunicado_externo
			(	nr_seq_comunic_p	number,
				nm_usuario_p		Varchar2) is 

begin

update	pls_comunic_externa_web
set	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	dt_liberacao	= sysdate
where	nr_sequencia 	= nr_seq_comunic_p;

commit;

end pls_liberar_comunicado_externo;
/