create or replace
procedure  HD_ISCMM_EXAME_MENSAL	(dt_inicial_p		date,
					dt_final_p		date,
					cd_empresa_param_p		number,
					nr_seq_unid_dialise_p	number,
					nm_usuario_p		varchar2)  is
					
nr_seq_unid_dialise_w	number(10);
cd_pessoa_fisica_w	varchar2(10);

dt_inicial_w		date;
dt_final_w		date;
dt_atual_w		date;

resultado_ktv_w			varchar2(255);
resultado_hb_w			varchar2(255);
resultado_ht_w			varchar2(255);
resultado_po4_w			varchar2(255);
resultado_caXpo4_w		varchar2(255);
resultado_pth_w			varchar2(255);
resultado_alb_w			varchar2(255);
resultado_hbs_ag_w		varchar2(255);
resultado_anti_hbs_w		varchar2(255);
resultado_anti_hcv_w		varchar2(255);
resultado_anti_hiv_w		varchar2(255);
resultado_hbs_ag_ant_w		varchar2(255);
resultado_anti_hcv_ant_w	varchar2(255);
resultado_anti_hiv_ant_w	varchar2(255);


qt_ktv1_w 		number(10);
qt_ktv2_w 		number(10);
qt_ktv3_w 		number(10);
qt_ktv4_w 		number(10);
qt_ktv5_w 		number(10);
qt_hb1_w 		number(10);
qt_hb2_w 		number(10);
qt_hb3_w 		number(10);
qt_ht1_w 		number(10);
qt_ht2_w 		number(10);
qt_ht3_w 		number(10);
qt_ht4_w 		number(10);
qt_ht5_w 		number(10);
qt_po41_w 		number(10);
qt_po42_w 		number(10);
qt_caxpo41_w 		number(10);
qt_caxpo42_w 		number(10);
qt_pth1_w 		number(10);
qt_pth2_w 		number(10);
qt_pth3_w 		number(10);
qt_pth4_w 		number(10);
qt_pth5_w 		number(10);
qt_alb1_w 		number(10);
qt_alb2_w 		number(10);
qt_hbs_ag_pos_w 	number(10);
qt_anti_hbs_pos_w 	number(10);
qt_anti_hcv_pos_w 	number(10);
qt_anti_hiv_pos_w 	number(10);
qt_conv_hbs_ag_w 	number(10);
qt_conv_hcv_w 		number(10);
qt_conv_hiv_w 		number(10);
qt_mes_atual_w		number(10);


Cursor C01 is
	select	nr_sequencia
	from	hd_unidade_dialise
	where	((nr_sequencia = nr_seq_unid_dialise_p) or (nvl(nr_seq_unid_dialise_p,0) = 0))
	and	((obter_empresa_estab(cd_estabelecimento) = cd_empresa_param_p) or (nvl(cd_empresa_param_p,0) = 0));
	
Cursor C02 is
	select	distinct 
		b.cd_pessoa_fisica
	from	hd_pac_renal_cronico a,
		paciente_tratamento b
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	b.ie_tratamento		in ('HD','PL','CH','CHD','CHDF','CHDI','PAF','SCUF','SLED','DDC','IHF','IHDF')
	and	b.dt_inicio_tratamento	is not null
	and	(trunc(b.dt_inicio_tratamento) <= trunc(dt_atual_w))
	and	((b.dt_final_tratamento	is null) or (b.dt_final_tratamento > dt_atual_w))
	and	b.nr_seq_unid_dialise = nr_seq_unid_dialise_w;

begin

dt_inicial_w	:= fim_mes(dt_inicial_p);
dt_final_w	:= fim_mes(dt_final_p);

dt_atual_w	:= dt_inicial_w;

select	trunc(months_between(dt_final_p,dt_inicial_p) + 1)
into	qt_mes_atual_w
from	dual;

delete	w_fpr_exame_mensal
where	nm_usuario = nm_usuario_p;

while dt_atual_w <= dt_final_w loop 
	begin

	open C01;
	loop
	fetch C01 into	
		nr_seq_unid_dialise_w;
	exit when C01%notfound;
		begin
		
		qt_ktv1_w 		:= 0;
		qt_ktv2_w 		:= 0;
		qt_ktv3_w 		:= 0;
		qt_ktv4_w 		:= 0;
		qt_ktv5_w 		:= 0;
		qt_hb1_w 		:= 0;
		qt_hb2_w 		:= 0;
		qt_hb3_w 		:= 0;
		qt_ht1_w 		:= 0;
		qt_ht2_w 		:= 0;
		qt_ht3_w 		:= 0;
		qt_ht4_w 		:= 0;
		qt_ht5_w 		:= 0;
		qt_po41_w 		:= 0;
		qt_po42_w 		:= 0;
		qt_caxpo41_w 		:= 0;
		qt_caxpo42_w 		:= 0;
		qt_pth1_w 		:= 0;
		qt_pth2_w 		:= 0;
		qt_pth3_w 		:= 0;
		qt_pth4_w 		:= 0;
		qt_pth5_w 		:= 0;
		qt_alb1_w 		:= 0;
		qt_alb2_w 		:= 0;
		qt_hbs_ag_pos_w 	:= 0;
		qt_anti_hbs_pos_w 	:= 0;
		qt_anti_hcv_pos_w 	:= 0;
		qt_anti_hiv_pos_w 	:= 0;
		qt_conv_hbs_ag_w 	:= 0;
		qt_conv_hcv_w 		:= 0;
		qt_conv_hiv_w 		:= 0;
	
		open C02;
		loop
		fetch C02 into	
			cd_pessoa_fisica_w;
		exit when C02%notfound;
			begin
			
			resultado_hb_w		:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,31); /*HEMOGRAMA HB*/
			resultado_ht_w		:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,31); /*HEMOGRAMA HT*/
			resultado_po4_w		:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,13); /*PO4*/
			resultado_caXpo4_w	:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,880); /*CA*/
			resultado_pth_w		:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,1476); /*PTH*/
			resultado_alb_w		:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,317); /*PTF ALBUMINA*/
			resultado_hbs_ag_w	:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,315); /*HBS ag*/
			resultado_anti_hbs_w	:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,1333); /*ANTI HBS*/
			resultado_anti_hcv_w	:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,1510); /*ANTI HCV*/
			resultado_anti_hiv_w	:= obter_resultado_mes_lab(cd_pessoa_fisica_w,dt_atual_w,1488); /*ANTI HIV*/

					
			if	(resultado_ktv_w = 'NA') then
			
				qt_ktv5_w	:= qt_ktv5_w + 1;
			
			elsif	(to_number(resultado_ktv_w) <= 1) then
			
				qt_ktv1_w	:= qt_ktv1_w + 1;
				
			elsif	(to_number(resultado_ktv_w) > 1) and
				(to_number(resultado_ktv_w) <= 1.19) then
				
				qt_ktv2_w	:= qt_ktv2_w + 1;
				
			elsif	(to_number(resultado_ktv_w) > 1.2) and
				(to_number(resultado_ktv_w) <= 2.0) then
				
				qt_ktv3_w	:= qt_ktv3_w + 1;
				
			elsif	(to_number(resultado_ktv_w) > 2.0) then
			
				qt_ktv4_w	:= qt_ktv4_w + 1;
			
			end if;
			
			if	(resultado_hb_w <> 'NA') then
			
				if	(to_number(resultado_hb_w) <= 11) then
				
					qt_hb1_w	:= qt_hb1_w + 1;

				elsif	(to_number(resultado_hb_w) > 11) and
					(to_number(resultado_hb_w) <= 12.99) then
					
					qt_hb2_w	:= qt_hb2_w + 1;
					
				elsif	(to_number(resultado_hb_w) > 13) then
				
					qt_hb3_w	:= qt_hb3_w + 1;
				
				end if;
			end if;
			
			if	(resultado_ht_w <> 'NA') then
				if	(to_number(resultado_ht_w) <= 28) then
				
					qt_ht1_w	:= qt_ht1_w + 1;
					
				elsif	(to_number(resultado_ht_w) > 28) and
					(to_number(resultado_ht_w) <= 32.9) then
					
					qt_ht2_w	:= qt_ht2_w + 1;
				
				elsif	(to_number(resultado_ht_w) > 33) and
					(to_number(resultado_ht_w) <= 36.9) then
					
					qt_ht3_w	:= qt_ht3_w + 1;
				
				elsif	(to_number(resultado_ht_w) > 37) and
					(to_number(resultado_ht_w) <= 40) then
				
					qt_ht4_w	:= qt_ht4_w + 1;
	
				elsif	(to_number(resultado_ht_w) > 40) then
				
					qt_ht5_w	:= qt_ht5_w + 1;
			
				end if;
			end if;
			
			if	(resultado_po4_w <> 'NA') then
				if	(to_number(resultado_po4_w) <= 5.5) then
				
					qt_po41_w	:= qt_po41_w + 1;
					
				elsif	(to_number(resultado_po4_w) > 5.5) then
					
					qt_po42_w	:= qt_po42_w + 1;
										
				end if;
			end if;
			
			if	(resultado_caXpo4_w <> 'NA') then
				if	(to_number(resultado_caXpo4_w) <= 55) then
				
					qt_caxpo41_w	:= qt_caxpo41_w + 1;
					
				elsif	(to_number(resultado_caXpo4_w) > 55) then
					
					qt_caxpo42_w	:= qt_caxpo42_w + 1;
										
				end if;
			end if;
			
			if	(resultado_pth_w = 'NA') then
			
				qt_pth5_w	:= qt_pth5_w + 1;
			
			elsif	(to_number(resultado_pth_w) <= 150) then
			
				qt_pth1_w	:= qt_pth1_w + 1;
				
			elsif	(to_number(resultado_pth_w) > 150) and
				(to_number(resultado_pth_w) <= 200) then
				
				qt_pth2_w	:= qt_pth2_w + 1;
				
			elsif	(to_number(resultado_pth_w) > 200) and
				(to_number(resultado_pth_w) <= 1000) then
				
				qt_pth3_w	:= qt_pth3_w + 1;
				
			elsif	(to_number(resultado_pth_w) > 1000) then
			
				qt_pth4_w	:= qt_pth4_w + 1;
			
			end if;
			
			if	(resultado_alb_w <> 'NA') then
			
				if	(to_number(resultado_alb_w) <= 3.7) then
				
					qt_alb1_w	:= qt_alb1_w + 1;
					
				elsif	(to_number(resultado_alb_w) > 3.7) then
					
					qt_alb2_w	:= qt_alb2_w + 1;
										
				end if;
			end if;
			
			if	(upper(substr(resultado_hbs_ag_w,1,3)) = 'POS') then
			
				qt_hbs_ag_pos_w	:= qt_hbs_ag_pos_w + 1;
				
				if	(upper(substr(resultado_hbs_ag_ant_w,1,3)) <> 'NA')	and
					(upper(substr(resultado_hbs_ag_ant_w,1,3)) <> 'POS') then
				
					qt_conv_hbs_ag_w	:= qt_conv_hbs_ag_w + 1;
			
				end if;
			
			end if;

			if	(upper(substr(resultado_anti_hbs_w,1,3)) = 'POS') then
			
				qt_anti_hbs_pos_w	:= qt_anti_hbs_pos_w + 1;
				
										
			end if;
			
			if	(upper(substr(resultado_anti_hcv_w,1,3)) = 'POS') then
			
				qt_anti_hcv_pos_w	:= qt_anti_hcv_pos_w + 1;
				
				if	(upper(substr(resultado_anti_hcv_ant_w,1,3)) <> 'NA')	and
					(upper(substr(resultado_anti_hcv_ant_w,1,3)) <> 'POS') then
				
					qt_conv_hcv_w	:= qt_conv_hcv_w + 1;
			
				end if;
			
			end if;
			
			if	(upper(substr(resultado_anti_hiv_w,1,3)) = 'POS') then
			
				qt_anti_hiv_pos_w	:= qt_anti_hiv_pos_w + 1;
				
				if	(upper(substr(resultado_anti_hiv_ant_w,1,3)) <> 'NA')	and
					(upper(substr(resultado_anti_hiv_ant_w,1,3)) <> 'POS') then
				
					qt_conv_hiv_w	:= qt_conv_hiv_w + 1;
			
				end if;				
				
			end if;		
			
			end;
		end loop;
		close C02;		

		insert into w_fpr_exame_mensal (nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						dt_referencia,
						nr_seq_unid_dialise,
						qt_ktv1,
						qt_ktv2,
						qt_ktv3,
						qt_ktv4,
						qt_ktv5,
						qt_hb1,
						qt_hb2,
						qt_hb3,
						qt_ht1,
						qt_ht2,
						qt_ht3,
						qt_ht4,
						qt_ht5,
						qt_po41,
						qt_po42,
						qt_caxpo41,
						qt_caxpo42,
						qt_pth1,
						qt_pth2,
						qt_pth3,
						qt_pth4,
						qt_pth5,
						qt_alb1,
						qt_alb2,
						qt_hbs_ag_pos,
						qt_anti_hbs_pos,
						qt_anti_hcv_pos,
						qt_anti_hiv_pos,
						qt_conv_hbs_ag,
						qt_conv_hcv,
						qt_conv_hiv,
						qt_mes_atual)
				values	(	w_fpr_exame_mensal_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						dt_atual_w,
						nr_seq_unid_dialise_w,
						qt_ktv1_w,
						qt_ktv2_w,
						qt_ktv3_w,
						qt_ktv4_w,
						qt_ktv5_w,
						qt_hb1_w,
						qt_hb2_w,
						qt_hb3_w,
						qt_ht1_w,
						qt_ht2_w,
						qt_ht3_w,
						qt_ht4_w,
						qt_ht5_w,
						qt_po41_w,
						qt_po42_w,
						qt_caxpo41_w,
						qt_caxpo42_w,
						qt_pth1_w,
						qt_pth2_w,
						qt_pth3_w,
						qt_pth4_w,
						qt_pth5_w,
						qt_alb1_w,
						qt_alb2_w,
						qt_hbs_ag_pos_w,
						qt_anti_hbs_pos_w,
						qt_anti_hcv_pos_w,
						qt_anti_hiv_pos_w,
						qt_conv_hbs_ag_w,
						qt_conv_hcv_w,
						qt_conv_hiv_w,
						qt_mes_atual_w
					);
		
		
		end;
	end loop;
	close C01;	
	
	dt_atual_w	:= fim_mes(dt_atual_w + 1);
	end;
end loop;

commit;

end HD_ISCMM_EXAME_MENSAL;
/
