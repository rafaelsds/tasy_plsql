create or replace procedure GERAR_TRANSF_MAPA(cd_perfil_p               number,
                                              nm_usuario_p              varchar2,
                                              cd_estabelecimento_p      number,
                                              nr_seq_interno_de_p       number,
                                              nr_seq_interno_para_p     number,
                                              ds_out_p                  out varchar2,
                                              nr_atendimento_p          number default null,        
                                              ie_transf_acomp_p         varchar2 default 'N',
                                              ie_consiste_sexo_acomp_p  varchar2 default 'N',
                                              nr_seq_interno_para_acomp_p number default null,
                                              nr_seq_gestao_vaga_p      number default null,
            ie_out_warning_p                    out varchar2) is
nr_atendimento_w            unidade_atendimento.nr_atendimento%type;
cd_setor_atendimento_para_w         unidade_atendimento.cd_setor_atendimento%type;
cd_unidade_basica_para_w        unidade_atendimento.cd_unidade_basica%type;
cd_unidade_compl_para_w         unidade_atendimento.cd_unidade_compl%type;
cd_setor_atendimento_de_w       unidade_atendimento.cd_setor_atendimento%type;
cd_unidade_basica_de_w          unidade_atendimento.cd_unidade_basica%type;
cd_unidade_compl_de_w           unidade_atendimento.cd_unidade_compl%type;
cd_tipo_acomodacao_w            unidade_atendimento.cd_tipo_acomodacao%type;
nr_acompanhante_w           atend_categoria_convenio.nr_acompanhante%type;
cd_paciente_reserva_w           atendimento_acompanhante.cd_pessoa_fisica%type;
nm_paciente_reserva_w           atendimento_acompanhante.nm_acompanhante%type;
-- Params
ie_transf_acomp_w               varchar2(1);
ie_consist_sexo_pac_w           varchar2(1);
ie_aguardando_hig_transf_w      varchar2(1);
ie_considerar_leito_rn_w        varchar2(1);
ie_new_clinical_panorama_w        varchar2(1);
ie_alojamento_w             varchar(1);
ie_sem_acomodacao_w         varchar2(1);
nr_acompanhante_www         number(15);
nr_seq_interno_w            unidade_atendimento.nr_seq_interno%type;
cd_setor_atendimento_de_ww      unidade_atendimento.cd_setor_atendimento%type;
cd_unidade_basica_de_ww         unidade_atendimento.cd_unidade_basica%type;
cursor C01(cd_setor_atendimento_p number, cd_unidade_basica_p varchar2) is
    select  nr_seq_interno
    from    unidade_atendimento
    where   cd_setor_atendimento    = cd_setor_atendimento_p
    and cd_unidade_basica   = cd_unidade_basica_p;
cursor C02(cd_setor_atendimento_p number, cd_unidade_basica_p varchar2) is
    select  cd_setor_atendimento,
        cd_unidade_basica,
        cd_unidade_compl,
        nr_seq_interno
    from UNIDADE_ATENDIMENTO
    where   cd_setor_atendimento = cd_setor_atendimento_p
    and cd_unidade_basica = cd_unidade_basica_p
    and nvl(ie_situacao, 'A') = 'A'
    and nvl(ie_bloqueio_transf,'N') = 'S'
    and ie_status_unidade = 'I';
begin
ie_out_warning_p := 'N';
obter_Param_Usuario(44, 122, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_transf_acomp_w);
obter_Param_Usuario(44, 220, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_consist_sexo_pac_w);
obter_Param_Usuario(44, 202, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_aguardando_hig_transf_w);
obter_Param_Usuario(44, 229, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_considerar_leito_rn_w);
obter_Param_Usuario(338, 12, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_new_clinical_panorama_w);
if  (nr_atendimento_p is null) then
    select  max(nr_atendimento),
            max(cd_setor_atendimento),
            max(cd_unidade_basica),
            max(cd_unidade_compl)
    into    nr_atendimento_w,
            cd_setor_atendimento_de_w,
            cd_unidade_basica_de_w,
            cd_unidade_compl_de_w
    from    unidade_atendimento
    where   nr_seq_interno = nr_seq_interno_de_p;
end if;
SELECT  nvl(MAX(nr_acompanhante), 0)
INTO    nr_acompanhante_w
FROM    atend_categoria_convenio b
WHERE   b.nr_atendimento = nvl(nr_atendimento_p, nr_atendimento_w);
select  max(cd_tipo_acomodacao),
        max(cd_setor_atendimento),
        max(cd_unidade_basica),
        max(cd_unidade_compl)
into    cd_tipo_acomodacao_w,
        cd_setor_atendimento_para_w,
        cd_unidade_basica_para_w,
        cd_unidade_compl_para_w
from    unidade_atendimento
where   nr_seq_interno = nr_seq_interno_para_p;
if  (nvl(nr_atendimento_p,nr_atendimento_w) is not null) then
    select  decode(count(*),0,'N','S')
    into    ie_alojamento_w 
    from    atendimento_acompanhante
    where   ie_alojamento = 'S'
    and     nr_atendimento = nr_atendimento_p;
    
    select  nvl(max(x.ie_sem_acomodacao),'N')
    into    ie_sem_acomodacao_w
    from    unidade_atendimento  a,
        tipo_acomodacao x
    where   a.nr_seq_interno     = nr_seq_interno_para_p
    and a.cd_tipo_acomodacao = x.cd_tipo_acomodacao;
    
    select  nvl(max(b.nr_seq_interno),0)
    into    nr_seq_interno_w
    from    unidade_atendimento b
    where   b.nr_atendimento_acomp = nvl(nr_atendimento_p, nr_atendimento_w)
    and b.ie_status_unidade    = 'M';
    
    select  max(cd_setor_atendimento),
        max(cd_unidade_basica)
    into    cd_setor_atendimento_de_ww,
        cd_unidade_basica_de_ww
    from    unidade_atendimento
    where   nr_seq_interno = nr_seq_interno_de_p;
    if ((ie_consist_sexo_pac_w = 'S') and (obter_se_leito_sexo_pac_pan(cd_setor_atendimento_para_w, cd_unidade_basica_para_w, cd_unidade_compl_para_w, nvl(nr_atendimento_p,nr_atendimento_w)) = 'N')) then
            ds_out_p:= wheb_mensagem_pck.get_texto(1052116);
            ie_out_warning_p := 'S';
            return;
    end if;
    if ((ie_transf_acomp_w = 'S') and (ie_transf_acomp_p = 'S') and
        (ie_sem_acomodacao_w = 'N') and 
        (ie_alojamento_w = 'S' and 
        Obter_se_leito_livre_acomp(nvl(nr_atendimento_p, nr_atendimento_w), cd_unidade_basica_para_w, cd_unidade_compl_para_w, cd_setor_atendimento_para_w) = 'N')) then
         ds_out_p:= wheb_mensagem_pck.get_texto(1043245);
        ie_out_warning_p := 'S';
        return;
    else
        ATUALIZAR_CASE_PLANEJADO(nr_atendimento_p,'C',nm_usuario_p,'N');
	
	update	atend_categoria_convenio
	set	dt_inicio_vigencia = sysdate
	where 	nr_atendimento = nr_atendimento_p
	and 	dt_inicio_vigencia > sysdate;
    
        GERAR_TRANSFERENCIA_PACIENTE(nvl(nr_atendimento_p,nr_atendimento_w),
                                     cd_setor_atendimento_para_w,
                                     cd_unidade_basica_para_w,
                                     cd_unidade_compl_para_w,
                                     cd_tipo_acomodacao_w,
                                     nr_acompanhante_w,
                                     null,
                                     null,
                                     nm_usuario_p,
                                     sysdate);
                                     
                                         
        
        atualizar_seq_transf_solict(nr_seq_gestao_vaga_p, nr_seq_interno_para_p, 'S');
		if (ie_new_clinical_panorama_w <> 'S') then
			begin
				panorama_leito_pck.ATUALIZAR_W_PAN_LEITO(cd_estabelecimento_p,nr_seq_interno_de_p,nm_usuario_p);
				panorama_leito_pck.ATUALIZAR_W_PAN_LEITO(cd_estabelecimento_p,nr_seq_interno_para_p,nm_usuario_p);
				
				for C01_W in C01(cd_setor_atendimento_para_w, cd_unidade_basica_para_w) loop
					panorama_leito_pck.ATUALIZAR_W_PAN_LEITO(cd_estabelecimento_p, C01_W.nr_seq_interno, nm_usuario_p);
				end loop C01_W;
				
				for C01_W in C01(cd_setor_atendimento_de_ww, cd_unidade_basica_de_ww) loop
					panorama_leito_pck.ATUALIZAR_W_PAN_LEITO(cd_estabelecimento_p, C01_W.nr_seq_interno, nm_usuario_p);
				end loop C01_W;
				
			exception
				when others then
					null;
			end;
			commit;
		end if;
    
        if  (ie_transf_acomp_p = 'S') then
            if  (nr_seq_interno_w > 0) and
                (ie_sem_acomodacao_w = 'S') then  
               
                wheb_usuario_pck.set_ie_executar_trigger('N');
        
                update unidade_atendimento
                set ie_status_unidade = 'L', 
                nr_atendimento_acomp = null, 
                cd_paciente_reserva = null, 
                nm_pac_reserva = null
                where nr_atendimento_acomp = nvl(nr_atendimento_p,nr_atendimento_w);
        
				if(ie_new_clinical_panorama_w <> 'S') then
					panorama_leito_pck.ATUALIZAR_W_PAN_LEITO(cd_estabelecimento_p,nr_seq_interno_w,nm_usuario_p);
				end if;
                
                for C02_W in C02(cd_setor_atendimento_de_ww, cd_unidade_basica_de_ww) loop
                update  unidade_atendimento
                set ie_status_unidade   = 'L',
                    ds_observacao       = null,
                    dt_atualizacao      = sysdate,
                    dt_interdicao       = null,
                    nm_usuario      = nm_usuario_p,
                    cd_motivo_interdicao    = null,
                    qt_dias_prev_interd = null
                where   cd_setor_atendimento    = C02_W.cd_setor_atendimento
                and cd_unidade_basica   = C02_W.cd_unidade_basica
                and cd_unidade_compl    = C02_W.cd_unidade_compl;
				
                if(ie_new_clinical_panorama_w <> 'S') then
					panorama_leito_pck.ATUALIZAR_W_PAN_LEITO(cd_estabelecimento_p,C02_W.nr_seq_interno,nm_usuario_p);
				end if;
               end loop c02_w;
     
               if(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
                  wheb_usuario_pck.set_ie_executar_trigger('S');
               end if;
               
               update   atendimento_acompanhante
               set      dt_saida    = sysdate
               where    nr_atendimento  = nvl(nr_atendimento_p,nr_atendimento_w);
               
               select   nvl(max(nr_acompanhante),0) + 1,
                    max(cd_pessoa_fisica),
                    max(nm_acompanhante)
               into     nr_acompanhante_www,
                    cd_paciente_reserva_w,
                    nm_paciente_reserva_w
               from     atendimento_acompanhante
               where    nr_atendimento  = nvl(nr_atendimento_p,nr_atendimento_w);
                
               insert into atendimento_acompanhante(
                                 nr_atendimento, 
                                 dt_acompanhante,
                                 nr_acompanhante,
                                 dt_atualizacao, 
                                 nm_usuario, 
                                 cd_pessoa_fisica,
                                 nr_seq_interno,
                                 dt_atualizacao_nrec,
                                 nm_usuario_nrec,
                                 nm_acompanhante,
                                 ie_alojamento)
                    values( nvl(nr_atendimento_p,nr_atendimento_w), 
                                sysdate, 
                                nr_acompanhante_www,
                                sysdate, 
                                nm_usuario_p, 
                                cd_paciente_reserva_w,
                                nr_seq_interno_para_p,
                                sysdate,
                                nm_usuario_p,
                                nm_paciente_reserva_w,
                                ie_alojamento_w);
            else 
            
               Transf_acompanhantes_paciente(nvl(nr_atendimento_p,nr_atendimento_w),
                                              cd_setor_atendimento_para_w,
                                              cd_unidade_basica_para_w,
                                              cd_unidade_compl_para_w,
                                              nm_usuario_p,
                                              ie_consiste_sexo_acomp_p,
                                              'S',
                                              nr_seq_interno_para_acomp_p);
            end if;                               
        end if;
        gerar_ajustes_ap_lote('M', nvl(nr_atendimento_p,nr_atendimento_w), nm_usuario_p);
        if(nr_atendimento_p is not null) then
            if  ((ie_aguardando_hig_transf_w = 'S') or
                ((ie_aguardando_hig_transf_w = 'D') and
                (obter_se_auto_higienizacao(cd_setor_atendimento_para_w,cd_unidade_compl_para_w,cd_unidade_compl_para_w,'S') = 'S'))) then
                alterar_status_leito_transf('G',
                                            nm_usuario_p,
                                            cd_setor_atendimento_de_w,
                                            cd_unidade_basica_de_w,
                                            cd_unidade_compl_de_w);
            end if;
        end if;
        if  (ie_considerar_leito_rn_w = 'S') then
            if  (nvl(obter_consistir_se_mae_rn(nvl(nr_atendimento_p, nr_atendimento_w)), 0) > 0) then
                gerar_transferencia_rn(nvl(nr_atendimento_p, nr_atendimento_w),
                                       cd_setor_atendimento_para_w,
                                       cd_unidade_basica_para_w,
                                       cd_unidade_compl_para_w,
                                       null,
                                       null,
                                       nm_usuario_p,
                                       sysdate);
            end if;
        end if;
        ds_out_p:= wheb_mensagem_pck.get_texto(802292);
        commit;
    end if;
end if;
end gerar_transf_mapa;
/ 
