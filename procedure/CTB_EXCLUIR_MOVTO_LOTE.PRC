create or replace
procedure ctb_excluir_movto_lote(	nr_sequencia_p	number,
				nr_lote_contabil_p	number,
				nm_usuario_p	varchar2) is

integrity_error exception;
pragma exception_init(integrity_error, -02292);
ds_erro_w varchar2(4000);
ds_log_W			varchar2(255);
qtd_delete_w		pls_integer;

begin

if	(nvl(nr_sequencia_p,0) <> 0) then
	delete	from ctb_movimento
	where	nr_lote_contabil 	= nr_lote_contabil_p
	and	nr_sequencia		= nr_sequencia_p;

	ds_log_w	:= WHEB_MENSAGEM_PCK.get_texto(336004) || ' ' || nr_sequencia_p;
else
	loop
	update CTB_MOVIMENTO 
	set nr_seq_movto_corresp = null,
	    nr_seq_movto_partida = null 
	where nr_lote_contabil = nr_lote_contabil_p
	and ((nr_seq_movto_corresp is not null) or (nr_seq_movto_partida is not null))
	and rownum < 1001;
	
	qtd_delete_w := sql%rowcount;
	commit;
	
	if(qtd_delete_w = 0) then
		exit;
	end if;
	
	end loop;

	loop 
	delete	from ctb_movimento
	where	nr_lote_contabil = nr_lote_contabil_p 
	and rownum < 1001;
	
	qtd_delete_w := sql%rowcount;
	commit;
	
	if(qtd_delete_w = 0) then
		exit;
	end if;
	
	end loop;
	
	delete	from ctb_movimento
	where	nr_lote_contabil = nr_lote_contabil_p;
end if;

ctb_gravar_log_lote(nr_lote_contabil_p,8,ds_log_w,nm_usuario_p);

commit;

exception when integrity_error then
    ds_erro_w := substr(sqlerrm(sqlcode),1,4000);
    rollback;
    /*Nao foi possivel excluir os movimentos por existirem dependencias
    Verifique se foram selecionadas todas as partidas e contrapartidas relacionadas
    Detalhes DSERRO*/
    
    wheb_mensagem_pck.exibir_mensagem_abort(1077102);

end ctb_excluir_movto_lote;
/