create or replace
procedure gerar_retorno_per_protocolo(	nr_seq_retorno_p		number,
					ie_liberar_repasse_p	varchar2,
					ie_analisada_p		varchar2,
					nr_convenio_p		number,
				    	nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					ie_periodo_conta_p	varchar2,
					ds_mensagem_p out	varchar2,
					ie_guias_considerar_p	varchar2) is

/* cd_estabelecimento_w	number(15); */
nr_interno_conta_w		number(15);
cd_autorizacao_w		varchar2(255);
vl_guia_w		number(15,2);
nr_sequencia_w		number(15);
vl_pago_w		number(15,2);
vl_glosado_w		number(15,2);
dt_inicial_w		date;
dt_final_w		date;
cd_convenio_retorno_w	number(15);
ie_tipo_convenio_w		number(15);
vl_conta_w		number(15,2);
nr_titulo_w 		number(10);
vl_titulo_w 		number(15,2);
vl_saldo_titulo_w		number(15,2);
qt_itens_retorno_w		number(5);
ie_primeiro_item_w		varchar2(255);
nr_seq_protocolo_w	number(15);
vl_glosa_grg_w		number(15,2);
vl_pago_grg_w		number(15,2);
ie_libera_repasse_w	varchar2(5);
ie_contas_canc_estornadas_w	varchar2(10);
ie_permitecontasoutroretorno_w	varchar2(1);
ie_estab_financ_w		varchar2(10);
ds_msg_conta_outro_ret_w	varchar2(2000);
ie_data_referencia_protocolo_w	varchar2(2);
ie_insere_guia_w		varchar2(1) := 'S';

cursor c01 is
select	b.nr_interno_conta,
	b.cd_autorizacao,
	b.vl_guia,
	a.vl_conta,
	obter_tipo_convenio(cd_convenio),
	c.nr_seq_protocolo,
	to_number(obter_titulo_conta_guia(a.nr_interno_conta,b.cd_autorizacao,nr_seq_retorno_p,c.nr_seq_protocolo))
from	conta_paciente_guia b,
	protocolo_convenio c,
	conta_paciente a
where	a.nr_interno_conta		= b.nr_interno_conta
and	c.nr_seq_protocolo		= a.nr_seq_protocolo
and	a.ie_status_acerto		= 2
and	a.ie_cancelamento		is null
and	b.dt_convenio		is null
and	a.cd_estabelecimento	= c.cd_estabelecimento
and	c.cd_convenio		= cd_convenio_retorno_w
and	nvl(a.nr_conta_convenio,0)	= decode(nvl(nr_convenio_p,0), 0, nvl(a.nr_conta_convenio,0), nr_convenio_p)
and	((ie_data_referencia_protocolo_w = 'R' and c.dt_mesano_referencia between nvl(dt_inicial_w, sysdate + 1) and nvl(dt_final_w, sysdate - 1))
	or (ie_data_referencia_protocolo_w = 'V' and c.dt_vencimento between nvl(dt_inicial_w, sysdate + 1) and nvl(dt_final_w, sysdate - 1)) 
	or (ie_data_referencia_protocolo_w = 'VT' and trunc(Obter_data_venc_titulo(a.nr_interno_conta,a.nr_seq_protocolo)) between trunc(nvl(dt_inicial_w, sysdate + 1)) and trunc(nvl(dt_final_w, sysdate - 1))))
and	((a.cd_estabelecimento	= cd_estabelecimento_p) or
	 ( ie_estab_financ_w	= 'S' and obter_estab_financeiro(a.cd_estabelecimento) = cd_estabelecimento_p))
and	'N'			= nvl(ie_periodo_conta_p,'N')
and	(nvl(a.ie_cancelamento,'1') not in ('C', 'E') or (nvl(ie_contas_canc_estornadas_w,'S') = 'S'))
union
select	b.nr_interno_conta,
	b.cd_autorizacao,
	b.vl_guia,
	a.vl_conta,
	obter_tipo_convenio(a.cd_convenio_parametro),
	a.nr_seq_protocolo,
	to_number(obter_titulo_conta_guia(a.nr_interno_conta,b.cd_autorizacao,nr_seq_retorno_p,a.nr_seq_protocolo))
from	conta_paciente_guia b,
	conta_paciente a
where	a.nr_interno_conta		= b.nr_interno_conta
and	a.ie_status_acerto		= 2
and	a.ie_cancelamento		is null
and	b.dt_convenio		is null
and	a.cd_convenio_parametro	= cd_convenio_retorno_w
and	nvl(a.nr_conta_convenio,0)	= decode(nvl(nr_convenio_p,0), 0, nvl(a.nr_conta_convenio,0), nr_convenio_p)
and	((a.cd_estabelecimento	= cd_estabelecimento_p) or
	 (ie_estab_financ_w	= 'S' and obter_estab_financeiro(a.cd_estabelecimento) = cd_estabelecimento_p))
and	a.dt_mesano_referencia	between nvl(dt_inicial_w, sysdate + 1) and nvl(dt_final_w, sysdate - 1)
and	'S'			= nvl(ie_periodo_conta_p,'N')
and	(nvl(a.ie_cancelamento,'-1') not in ('C', 'E') or (nvl(ie_contas_canc_estornadas_w,'S') = 'S'));

begin

select	dt_inicial,
	dt_final,
	cd_convenio
into	dt_inicial_w,
	dt_final_w,
	cd_convenio_retorno_w
from	convenio_retorno
where	nr_sequencia	= nr_seq_retorno_p;

obter_param_usuario(27, 150, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_contas_canc_estornadas_w);
obter_param_usuario(27, 100, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_permitecontasoutroretorno_w);
obter_param_usuario(27, 277, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_estab_financ_w);
obter_param_usuario(27, 288, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_data_referencia_protocolo_w);
 
ie_primeiro_item_w		:= 'S';
ds_msg_conta_outro_ret_w	:= null;

open c01;
loop
fetch c01 into
	nr_interno_conta_w,
	cd_autorizacao_w,
	vl_guia_w,
	vl_conta_w,
	ie_tipo_convenio_w,
	nr_seq_protocolo_w,
	nr_titulo_w;
exit	when c01%notfound;

	if	(ie_tipo_convenio_w = 3) and
		(vl_guia_w < vl_conta_w) then
		gerar_conta_paciente_guia(nr_interno_conta_w, 2);
		atualizar_resumo_conta(nr_interno_conta_w, 2);
	end if;

	select	nvl(sum(vl_pago),0),
		nvl(sum(vl_glosado),0) --bruna os33187 - tratar para nao incluir o valor glosado no momento da inclus�o do protocolo
	into	vl_pago_w,
		vl_glosado_w
	from	convenio_retorno_item b,
		convenio_retorno a
	where	a.nr_sequencia		= b.nr_seq_retorno
	and	a.cd_convenio		= cd_convenio_retorno_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.nr_interno_conta		= nr_interno_conta_w
	and	b.cd_autorizacao		= cd_autorizacao_w;
--	and	a.ie_status_retorno	= 'F';  Comentado Bruna, para consistir em todos os casos

	vl_pago_grg_w	:= nvl(to_number(obter_valor_guia_grg(nr_interno_conta_w, cd_autorizacao_w, 'VP')),0);
	vl_glosa_grg_w	:= nvl(to_number(obter_valor_guia_grg(nr_interno_conta_w, cd_autorizacao_w, 'VG')),0);

	select	count(*)
	into	qt_itens_retorno_w
	from	convenio_retorno_item b,
		convenio_retorno a
	where	a.nr_sequencia		= b.nr_seq_retorno
	and	a.cd_convenio		= cd_convenio_retorno_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	b.nr_interno_conta	= nr_interno_conta_w
	and	b.cd_autorizacao	= cd_autorizacao_w;
	
	if	(nvl(ie_guias_considerar_p,'0') <> '0') then
		ie_insere_guia_w	:= 'N';
	end if;
	
	if	(nvl(ie_guias_considerar_p,'0') = '1') and --Somente guias integrais (Valor Guia = Valor Saldo)
		(vl_pago_w = 0 and vl_glosado_w = 0 and vl_pago_grg_w = 0 and vl_glosa_grg_w = 0) then --Indica que a guia est� integra, ainda n�o foi paga e nem glosada em outros retornos.
		ie_insere_guia_w	:= 'S';
	elsif	(nvl(ie_guias_considerar_p,'0') = '2') and --Somente guias recursadas (Valor guia <> Valor Saldo)
		(vl_pago_w <> 0 or vl_glosado_w <> 0 or vl_pago_grg_w <> 0 or vl_glosa_grg_w <> 0) then
		ie_insere_guia_w	:= 'S';
	end if;

	if 	(qt_itens_retorno_w = 0) then
		

		select	nvl(sum(vl_saldo_titulo),0)
		into	vl_saldo_titulo_w
		from (select nvl(vl_saldo_titulo,0) vl_saldo_titulo
			from titulo_receber
			where (nr_documento is not null) 
			  and nr_documento <> nr_interno_conta_w
			  and nr_interno_conta = nr_interno_conta_w
			union
			select nvl(vl_saldo_titulo,0) vl_saldo_titulo
			from titulo_receber
			where nr_interno_conta_w is not null
			  and ((nr_documento = nr_interno_conta_w) or (nr_documento is null))
			  and nr_interno_conta = nr_interno_conta_w
			union
			select nvl(vl_saldo_titulo,0) vl_saldo_titulo
			from titulo_receber
			where nr_seq_protocolo = nr_seq_protocolo_w
		  	and nr_interno_conta is null);

		if (vl_saldo_titulo_w = 0) then

			if (ie_primeiro_item_w =  'S') then
	
				--ds_mensagem_p	:= substr('Guias n�o possuem saldo: ' || cd_autorizacao_w, 1, 255);
				ds_mensagem_p	:= substr(wheb_mensagem_pck.get_texto(304128, 'CD_AUTORIZACAO=' || cd_autorizacao_w), 1, 255);						
			else
				ds_mensagem_p	:= substr(ds_mensagem_p || ',' || cd_autorizacao_w, 1, 255);
			end if;
			if	(length(ds_mensagem_p) > 125) and 
				(instr(ds_mensagem_p, chr(13)) = 0) then
				ds_mensagem_p	:= ds_mensagem_p || chr(13);
			end if;
			ie_primeiro_item_w := 'N';
		end if;
	end if;


	/* lhalves, alterada consist�ncia para n�o abortar, para continuar o processo e apresentar mensagem no final
	if	(nvl(ie_permitecontasoutroretorno_w,'S') = 'N')	and
		(obter_conta_outro_retorno(nr_seq_retorno_p,nr_interno_conta_w,cd_autorizacao_w) <> 0) then
		--Esta conta j� est� vinculada a outro retorno em aberto. Par�metro [100]
		wheb_mensagem_pck.exibir_mensagem_abort(255816);
	end if;*/
	
	if	(nvl(ie_insere_guia_w,'S') = 'S') then		
		if	(nvl(ie_permitecontasoutroretorno_w,'S') = 'S') or
			(obter_conta_outro_retorno(nr_seq_retorno_p,nr_interno_conta_w,cd_autorizacao_w) = 0)then
		
			if	((vl_pago_w + vl_glosado_w + vl_glosa_grg_w + vl_pago_grg_w) < vl_guia_w) then

				select	max(a.ie_libera_repasse)
				into	ie_libera_repasse_w
				from	regra_campo_lib_rep a
				where	a.cd_estabelecimento	= cd_estabelecimento_p
				and	nvl(a.cd_convenio,cd_convenio_retorno_w) = cd_convenio_retorno_w
				and	a.ie_regra	= 1;
			
				select	convenio_retorno_item_seq.nextval
				into	nr_sequencia_w
				from	dual;

				insert into convenio_retorno_item
					(nr_sequencia,
					nr_seq_retorno,
					vl_pago,
					vl_glosado,
					dt_atualizacao,
					nm_usuario,
					ie_glosa,
					nr_interno_conta,
					cd_autorizacao,
					vl_adicional,
					vl_amenor,
					ie_libera_repasse,
					ie_analisada,
					nr_titulo)
				values(	nr_sequencia_w,
					nr_seq_retorno_p,
					vl_guia_w - vl_pago_w - vl_glosado_w - vl_glosa_grg_w - vl_pago_grg_w,
					0,
					sysdate,
					nm_usuario_p,
					'N',
					nr_interno_conta_w,
					cd_autorizacao_w,
					0,
					0,
					nvl(ie_libera_repasse_w,ie_liberar_repasse_p),
					ie_analisada_p,
					nr_titulo_w);
			end if;
		else
			if	(nvl(length(ds_msg_conta_outro_ret_w),0) < 1800) then
				--ds_msg_conta_outro_ret_w := ds_msg_conta_outro_ret_w || ' Conta: '||nr_interno_conta_w||' / Guia: '||cd_autorizacao_w||chr(10);
				ds_msg_conta_outro_ret_w := ds_msg_conta_outro_ret_w || wheb_mensagem_pck.get_texto(304130, 'NR_INTERNO_CONTA=' || nr_interno_conta_w || ';' ||
															    'CD_AUTORIZACAO=' || cd_autorizacao_w) ||chr(10);
			end if;
		end if;
	end if;
end loop;
close c01;

if	(ds_msg_conta_outro_ret_w is not null) then
	--ds_mensagem_p	:= 'As guias abaixo n�o foram inclu�das pois j� est�o em outro retorno: '||chr(10)||ds_msg_conta_outro_ret_w;
	ds_mensagem_p	:= wheb_mensagem_pck.get_texto(304129)||chr(10)||ds_msg_conta_outro_ret_w;	
end if;

commit;

end gerar_retorno_per_protocolo;
/