create or replace procedure pls_tiss_consistir_diaria
			(	nr_sequencia_p	Number,
				ie_tipo_glosa_p		Varchar2,
				ie_evento_p		Varchar2,
				nr_seq_prestador_p	Number,
				nr_seq_ocorrencia_p	Number,
				nr_seq_segurado_p	Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realiza a consist�ncia  de di�ria.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  x]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

/* IE_TIPO_GLOSA_P
	C - Conta
	CP - Conta procedimento
	CM - Conta material
	A - Autoriza��o
	AP - Autoriza��o procedimento
	AM - Autoriza��o material
*/

qt_registro_w			Number(10)	:= 0;
cd_guia_w			Varchar2(20);
nr_seq_segurado_w		Number(10);
dt_entrada_w			date;
dt_alta_w			date;
qt_procedimento_w		number(12,4);
qt_proc_total_diarias_w		number(12,4)	:= 0;
qt_dias_internado_w		pls_integer	:= null;
ie_tipo_guia_w			varchar2(2);
cd_tipo_acomodacao_w		varchar2(2);
nr_seq_tipo_acomodacao_w	number(10);
nr_seq_tipo_acomodacao_conta_w	number(10);
ie_acomodacao_invalida_w	varchar2(1) 	:= 'S';
ie_guia_autor_w			varchar2(1) 	:= 'S';
ie_acomodacao_nula_w		varchar2(1)	:= 'N';
ie_categoria_w			Varchar2(1)	:= 'N';
nr_seq_categoria_w		Number(10);
nr_seq_categoria_ww		Number(10);
ie_validacao_w			varchar2(5);
cd_versao_tiss_w		pls_protocolo_conta.cd_versao_tiss%type;
nr_seq_protocolo_w		pls_conta.nr_seq_protocolo%type;

Cursor C01 is
	select	nr_seq_categoria
	from	pls_regra_categoria
	where	nr_seq_tipo_acomodacao = nr_seq_tipo_acomodacao_w;

begin

if	(ie_tipo_glosa_p	= 'C') then
	/* Obter dados da conta */
	begin
	select	nvl(nvl(cd_guia_referencia,cd_guia),nvl(cd_guia_solic_imp,cd_guia_imp)),
		nr_seq_segurado,
		nvl(dt_entrada,dt_entrada_imp),
		nvl(dt_alta,dt_alta_imp),
		ie_tipo_guia,
		cd_tipo_acomodacao_imp,
		nr_seq_tipo_acomodacao,
		nr_seq_protocolo
	into	cd_guia_w,
		nr_seq_segurado_w,
		dt_entrada_w,
		dt_alta_w,
		ie_tipo_guia_w,
		cd_tipo_acomodacao_w,
		nr_seq_tipo_acomodacao_conta_w,
		nr_seq_protocolo_w
	from	pls_conta
	where	nr_sequencia	= nr_sequencia_p;
	exception
	when others then
		nr_seq_segurado_w	:= null;
	end;

	select	nvl(sum(qt_procedimento_imp),0)
	into	qt_proc_total_diarias_w
	from	pls_conta_proc
	where	nr_seq_conta = nr_sequencia_p
	and	ie_tipo_despesa = '3';


	if	(dt_alta_w is not null) and
		(dt_entrada_w is not null) then
		qt_dias_internado_w := Obter_Dias_Entre_Datas_hora(dt_entrada_w,dt_alta_w) + 1;
	end if;
	
	select 	cd_versao_tiss
	into	cd_versao_tiss_w
	from	pls_protocolo_conta
	where	nr_sequencia = nr_seq_protocolo_w;
	
	if ( cd_versao_tiss_w < '3.01.00') then
		if	(ie_evento_p = 'IA') then
			if	(ie_tipo_guia_w = '5') and
				(cd_tipo_acomodacao_w is null) then
				ie_acomodacao_nula_w	:= 'S';
			end if;
		elsif	(ie_evento_p = 'CC') then
			if	(ie_tipo_guia_w = '5') and
				(nr_seq_tipo_acomodacao_conta_w is null) then
				ie_acomodacao_nula_w	:= 'S';
			end if;
		end if;
	end if;

	if	(ie_tipo_guia_w = '5') then
		if	(cd_guia_w is not null) then
			if	(ie_evento_p = 'IA') then
				select	max(nr_sequencia)
				into	nr_seq_tipo_acomodacao_conta_w
				from	pls_tipo_acomodacao
				where	cd_tiss			= cd_tipo_acomodacao_w;

				nr_seq_segurado_w := nr_seq_segurado_p;
			end if;

			begin
			select 	ie_validacao_categoria
			into	ie_validacao_w
			from	pls_regra_glosa_1909;
			exception
			when others then
				ie_validacao_w	:= 'AC';
			end;

			begin
				select	nr_seq_tipo_acomodacao
				into	nr_seq_tipo_acomodacao_w
				from	pls_guia_plano
				where	cd_guia		= cd_guia_w
				and	nr_seq_segurado	= nr_seq_segurado_w;
			exception
			when others then
				nr_seq_tipo_acomodacao_w := null;
				ie_guia_autor_w := 'N';
			end;

			if	(nvl(cd_guia_w,'X') <> 'X')	then
				ie_guia_autor_w	:= 'S';

				begin
					select	nr_seq_tipo_acomodacao
					into	nr_seq_tipo_acomodacao_w
					from	pls_guia_plano
					where	cd_guia_pesquisa = cd_guia_w
					and	nr_seq_segurado	 = nr_seq_segurado_w;
				exception
				when others then
					nr_seq_tipo_acomodacao_w := null;
					ie_guia_autor_w := 'N';
				end;
			end if;

			if	(nr_seq_tipo_acomodacao_w <> nr_seq_tipo_acomodacao_conta_w) and
				(ie_guia_autor_w = 'S') then /*Diego OS 326796 - S� ser� considerada para guias que possuem uma autoriza��o na base.*/
				ie_acomodacao_invalida_w := 'N';
			end if;

			if	(ie_acomodacao_invalida_w 	= 'N') 	and
				(nvl(ie_validacao_w,'AC')	= 'AC')	then
				open C01;
				loop
				fetch C01 into
					nr_seq_categoria_w;
				exit when C01%notfound;
					begin
					nr_seq_categoria_ww := null;
					begin
						select	max(nr_sequencia)
						into	nr_seq_categoria_ww
						from	pls_regra_categoria
						where	nr_seq_categoria 	= nr_seq_categoria_w
						and	nr_seq_tipo_acomodacao 	= nr_seq_tipo_acomodacao_conta_w;
					exception
					when others then
						nr_seq_categoria_ww:= 0;
					end;


					if	(nvl(nr_seq_categoria_ww,0) > 0) then
						ie_acomodacao_invalida_w := 'S';
					end if;
					end;
				end loop;
				close C01;
			end if;
		end if;
	end if;
elsif	(ie_tipo_glosa_p	= 'A') then
	select	nr_seq_tipo_acomodacao,
		ie_tipo_guia
	into	nr_seq_tipo_acomodacao_w,
		ie_tipo_guia_w
	from	pls_guia_plano
	where	nr_sequencia = nr_sequencia_p;

	if	(ie_tipo_guia_w in ('1','8')) and
		(nr_seq_tipo_acomodacao_w is null) then
		ie_acomodacao_nula_w	:= 'S';
	end if;
end if;

qt_registro_w	:= 0;
/* 1913 - Cobran�a de di�ria em quantidade incompat�vel com a perman�ncia hospitalar*/
if	(qt_proc_total_diarias_w > qt_dias_internado_w) then
	pls_gravar_glosa_tiss('1914',
		'Total de di�rias: '||qt_proc_total_diarias_w ||' - Per�odo internado: '||qt_dias_internado_w||' dias.', ie_evento_p,
		nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
		nr_seq_ocorrencia_p, '', nm_usuario_p,
		cd_estabelecimento_p, null);
end if;

/* 1906 - Acomoda��o n�o informada*/
if	(ie_acomodacao_nula_w = 'S') then
	pls_gravar_glosa_tiss('1906',
		'Conta: ' || nr_sequencia_p, ie_evento_p,
		nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
		nr_seq_ocorrencia_p, '', nm_usuario_p,
		cd_estabelecimento_p, null);
end if;

/* 1908 (Agora 1909) - Acomoda��o n�o autorizada -> alterado para 1401 OS 757971*/
if	(ie_tipo_guia_w = '5') and
	(ie_acomodacao_invalida_w = 'N') then
	pls_gravar_glosa_tiss('1401',
		'Acomoda��o autorizada: '|| pls_obter_desc_tipo_acomodacao(nr_seq_tipo_acomodacao_w)||' | Acomoda��o da conta: '||pls_obter_desc_tipo_acomodacao(nr_seq_tipo_acomodacao_conta_w), ie_evento_p,
		nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
		nr_seq_ocorrencia_p, '', nm_usuario_p,
		cd_estabelecimento_p, null);
end if;

--commit;

end pls_tiss_consistir_diaria;
/