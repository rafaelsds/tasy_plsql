create or replace
procedure gerar_nova_validade_lote_forn(	nr_seq_lote_p		number,
					nm_usuario_p		Varchar2,
					pr_var_validade_p		number,
					dt_validade_nova_p     out	date) is 

qt_dias_validade_w		number(10,0);
qt_dias_aplicar_w		number(10,0);
dt_validade_nova_w	date;
dt_atual_w		date;
dt_validade_original_w	date;

begin
dt_validade_nova_p	:= null;

select	dt_validade_original
into	dt_validade_original_w
from	material_lote_fornec
where	nr_sequencia	= nr_seq_lote_p;

if	(dt_validade_original_w is null) then
	begin
	dt_atual_w	:= trunc(sysdate,'dd');

	update	material_lote_fornec
	set	dt_validade_original	= dt_validade
	where	nr_sequencia	= nr_seq_lote_p;

	select	nvl(dt_validade - dt_atual_w,0)
	into	qt_dias_validade_w
	from	material_lote_fornec
	where	nr_sequencia	= nr_seq_lote_p;

	qt_dias_aplicar_w		:= dividir((qt_dias_validade_w * pr_var_validade_p),100);
	dt_validade_nova_w	:= trunc((dt_atual_w + qt_dias_aplicar_w),'dd');

	update	material_lote_fornec
	set	dt_validade	= dt_validade_nova_w
	where	nr_sequencia	= nr_seq_lote_p;

	dt_validade_nova_p	:= dt_validade_nova_w;

	commit;
	end;
end if;

end gerar_nova_validade_lote_forn;
/