create or replace
procedure pls_consistir_acomod_diaria
				(	nr_seq_guia_proc_p	Number,					
					nm_usuario_p		Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Verificar se existe regra de di�rias X acomoda��es e gerar glosa caso a regra exista e o procedimento da guia n�o constar na regra.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_guia_w			number(10);
nr_seq_tipo_acomodacao_w	number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_reg_glosa_w		number(10);
qt_registros_w			number(10);
ie_gerar_glosa_w		varchar2(1);
nr_seq_prestador_w		number(10);	
ie_classificacao_w		varchar2(1);

Cursor C01 is
	select	nr_sequencia
	from	pls_regra_glosa_acom
	where	nr_seq_tipo_acomodacao 	= nr_seq_tipo_acomodacao_w
	and 	ie_situacao 		= 'A'
	and 	(nvl(cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento) = wheb_usuario_pck.get_cd_estabelecimento and pls_obter_se_controle_estab('RE') = 'S')
	UNION
	select	nr_sequencia
	from	pls_regra_glosa_acom
	where	nr_seq_tipo_acomodacao 	= nr_seq_tipo_acomodacao_w
	and 	ie_situacao 		= 'A';

begin
begin
	select 	nr_seq_guia
	into	nr_seq_guia_w
	from	pls_guia_plano_proc
	where	nr_sequencia = nr_seq_guia_proc_p;  
exception
when others then
	nr_seq_guia_w := null;	
end;

begin
	select 	nr_seq_tipo_acomodacao,
		nr_seq_prestador
	into	nr_seq_tipo_acomodacao_w,
		nr_seq_prestador_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_w;
exception
when others then
	nr_seq_tipo_acomodacao_w 	:= null;
	nr_seq_prestador_w 		:= null;
end;

if 	(nr_seq_tipo_acomodacao_w	is not null)	then	
	begin
		select 	cd_procedimento,
			ie_origem_proced
		into	cd_procedimento_w,
			ie_origem_proced_w
		from	pls_guia_plano_proc
		where 	nr_sequencia	= nr_seq_guia_proc_p;
	exception
	when others then
		cd_procedimento_w 	:= null;
		ie_origem_proced_w 	:= null;
	end;
	
	begin
		select 	ie_classificacao
		into	ie_classificacao_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_w
		and 	ie_origem_proced	= ie_origem_proced_w;
	exception
	when others then
		ie_classificacao_w	:= null;	
	end;
	
	if	(ie_classificacao_w = '3') then
		open C01;
		loop
		fetch C01 into	
			nr_seq_reg_glosa_w;
		exit when C01%notfound;
			begin
			ie_gerar_glosa_w := 'S';
					
			select 	count(1)
			into	qt_registros_w
			from	pls_glosa_proc_acom
			where	nr_seq_regra_acom	= nr_seq_reg_glosa_w
			and	cd_procedimento		= cd_procedimento_w
			and 	ie_origem_proced	= ie_origem_proced_w;
			
			if 	(qt_registros_w <> 0) then
				ie_gerar_glosa_w := 'N';
				exit;
			end if;
			end;
		end loop;
		close C01;
		--Alterado de 1815 para 1402 OS 757971
		if	(ie_gerar_glosa_w = 'S') then
			pls_gravar_motivo_glosa(	'1402',null,nr_seq_guia_proc_p,
							null,'Procedimento n�o autorizado',nm_usuario_p,	
							'','CG',nr_seq_prestador_w, 
							null,null);
		end if;
	end if;
end if;

commit;

end pls_consistir_acomod_diaria;
/
