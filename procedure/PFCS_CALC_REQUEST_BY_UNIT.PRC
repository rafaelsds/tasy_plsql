create or replace procedure PFCS_CALC_REQUEST_BY_UNIT(
    nr_seq_indicator_p   number,
    cd_estabelecimento_p varchar2,
    nm_usuario_p         varchar2) is

	nr_seq_panel_w 						pfcs_panel.nr_sequencia%type;
	ds_planned_status_w				varchar2(15) := 'PLANNED';
	ds_arrived_status_w				varchar2(15) := 'ARRIVED';
	ds_active_status_w				varchar2(15) := 'ACTIVE';
	ds_monitor_dev_type_w 		varchar2(10) := 'Monitor';
	ds_service_requested_w		varchar2(10) := 'E0404';
	ie_active_w							  varchar2(1) := '1';

/* Cursor to read from pfcs integration tables */
cursor c01_pfcs is
	select
		loc.ds_department ds_unit,
		count(sr.nr_sequencia) qt_patients_w
	from pfcs_service_request sr,
		pfcs_encounter enc,
		pfcs_patient pat,
		pfcs_encounter_location el,
		pfcs_location loc,
		pfcs_organization org
	where
		sr.si_status = ds_active_status_w
		and sr.cd_service = ds_service_requested_w
		and sr.nr_seq_encounter = enc.nr_sequencia
		and enc.si_status in (ds_planned_status_w, ds_arrived_status_w)
		and el.nr_seq_encounter = enc.nr_sequencia
		and el.nr_seq_location = loc.nr_sequencia
		and loc.si_status = ds_active_status_w
		and enc.nr_seq_patient = pat.nr_sequencia
		and pat.ie_active = ie_active_w
		and pat.nr_seq_organization = org.nr_sequencia
		and org.cd_estabelecimento = to_number(cd_estabelecimento_p)
		and pat.nr_sequencia not in
		(
			select dev.nr_seq_patient
			from pfcs_device dev
			where dev.si_status = ds_active_status_w
				and dev.ds_device_type = ds_monitor_dev_type_w
				and dev.nr_seq_patient is not null
		 )
	group by loc.ds_department;

begin

	for c01_w in c01_pfcs loop
	begin
	  pfcs_pck_v2.pfcs_generate_results(
			vl_indicator_p => c01_w.qt_patients_w,
			ds_reference_value_p => c01_w.ds_unit,
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => to_number(cd_estabelecimento_p),
			nm_usuario_p => nm_usuario_p,
			nr_seq_panel_p => nr_seq_panel_w);
	end;
	end loop;
	commit;

	pfcs_pck_v2.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => to_number(cd_estabelecimento_p),
		nm_usuario_p => nm_usuario_p);

end PFCS_CALC_REQUEST_BY_UNIT;
/