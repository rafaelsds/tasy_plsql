create or replace
procedure excluir_itens_nf_credito(nr_seq_nf_credito_p	number) is 

begin

if	(nr_seq_nf_credito_p is not null) then
	begin
	delete
	from	nf_credito_item
	where	nr_seq_nf_credito = nr_seq_nf_credito_p;
	commit;
	end;
end if;

end excluir_itens_nf_credito;
/