create or replace
procedure Atualizar_status_proced_pato(nr_prescricao_p		number,
					nr_seq_proced_p		number,
					nr_seq_status_p		number,
					nm_usuario_p		Varchar2) is 

begin

update	prescr_procedimento
set	nr_seq_status_pato	= nr_seq_status_p,
	dt_atualizacao		= sysdate
where	nr_prescricao		= nr_prescricao_p
and	nr_sequencia		= nr_seq_proced_p;

commit;

end Atualizar_status_proced_pato;
/
