create or replace procedure tiss_obter_guia_prest_autor (
    cd_convenio_p           number,
    cd_estabelecimento_p    number,
    ie_tipo_autorizacao_p   varchar2,
    nr_atendimento_p        number,
    nr_seq_agenda_p         number,
    nr_seq_agenda_integ_p   number,
    nr_guia_prest_p out varchar2
) is
/*
EUP - Numero da guia da EUP
IN -	Intervalo de numeracao
SAT - Senha atual do atendimento
CIP - Codigo interno do prestador
GAT - Numero de guia informado na "Agenda Integrada"
GAE - Numero de guia informado na "Agenda de Exames"
NA - Numero do atendimento
*/

nr_guia_prest_w     varchar2(20) := null;
ie_forma_w            regra_guia_prest_autor.ie_forma%type;
nr_atual_w             regra_guia_prest_autor.nr_atual%type;
nr_sequencia_w     regra_guia_prest_autor.nr_sequencia%type;
    
cursor c01 is
select ie_forma,
        nr_atual,
        nr_sequencia
from regra_guia_prest_autor
where ie_situacao = 'A'
and cd_estabelecimento = cd_estabelecimento_p
and cd_convenio = cd_convenio_p
and ie_tipo_autorizacao = ie_tipo_autorizacao_p
order by ie_prioridade nulls last;--Conforme visto com Ronaldo, caso nao tenha informacao (nr_guia_prest_w = null) deve ir para proxima linha

begin
open c01;
loop
  fetch c01 into
    ie_forma_w,
    nr_atual_w,
    nr_sequencia_w;

  if(ie_forma_w = 'EUP' and nr_guia_prest_w is null) then --Numero da guia da EUP
	  begin
		  select	b.nr_doc_convenio			
		  into	nr_guia_prest_w
		  from	atend_categoria_convenio b,
			  atendimento_paciente a			
		  where	a.nr_atendimento		= b.nr_atendimento			
		    and	b.nr_seq_interno		= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_p)
		    and	a.nr_atendimento		= nr_atendimento_p
		    and	rownum  = 1;
	  exception
	  when others then
		  nr_guia_prest_w	:= null;
	  end;
  elsif(ie_forma_w = 'IN' and nr_guia_prest_w is null)then    --Intervalo de numeracao
    if (nr_atual_w is not null) then
		  nr_guia_prest_w	:= nr_atual_w;
		
		  update	regra_guia_prest_autor
		  set	nr_atual 	= nr_atual_w + 1,				
			  dt_atualizacao	= sysdate		
		  where	nr_sequencia	= nr_sequencia_w;        
	  end if;
  elsif(ie_forma_w = 'SAT' and nr_guia_prest_w is null) then --Senha atual do atendimento
    begin
		  select	b.cd_senha			
		  into	nr_guia_prest_w
		  from	atend_categoria_convenio b,
			  atendimento_paciente a			
		  where	a.nr_atendimento		= b.nr_atendimento			
		    and	b.nr_seq_interno		= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_p)
		    and	a.nr_atendimento		= nr_atendimento_p
		    and	rownum  = 1;
	    exception
	    when others then
		    nr_guia_prest_w	:= null;
	  end;
  elsif(ie_forma_w = 'CIP' and nr_guia_prest_w is null) then --Codigo interno do prestador
    select	max(obter_valor_conv_estab(cd_convenio_p, cd_estabelecimento_p, 'CD_INTERNO'))
	  into	nr_guia_prest_w
	  from 	dual;
  elsif(ie_forma_w = 'GAT' and nr_guia_prest_w is null) then --Numero de guia informado na "Agenda Integrada"
    if(nr_seq_agenda_integ_p is not null) then
      select a.nr_doc_convenio
      into nr_guia_prest_w
      from agenda_integrada a
      where a.nr_sequencia = nr_seq_agenda_integ_p;    
    end if;
  elsif(ie_forma_w = 'GAE' and nr_guia_prest_w is null) then --Numero de guia informado na "Agenda de Exames"
    if(nr_seq_agenda_p is not null) then
      select a.nr_doc_convenio
      into nr_guia_prest_w
      from agenda_paciente a
      where a.nr_sequencia = nr_seq_agenda_p;
    end if;
  elsif(ie_forma_w = 'NA' and nr_guia_prest_w is null) then --Numero do atendimento
    nr_guia_prest_w := nr_atendimento_p;
  end if;

  exit when c01%notfound;
end loop;
close c01;

nr_guia_prest_p := nr_guia_prest_w;
end tiss_obter_guia_prest_autor;
/
