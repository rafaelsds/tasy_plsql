create or replace
procedure incluir_antib_adicional(
                        nr_seq_resultado_p      number,
                        nr_seq_result_item_p	number,
                        cd_microorganismo_p     number,
                        cd_medicamento_p        number,
                        nm_usuario_p            Varchar2,
						nr_cultura_microorg_p	number) is

nr_seq_result_antib_w           number;

begin

select 	nvl(max(nr_sequencia),0)
into   	nr_seq_result_antib_w
from   	exame_lab_result_antib
where  	nr_seq_resultado = nr_seq_resultado_p
and    	nr_seq_result_item = nr_seq_result_item_p
and    	cd_microorganismo = cd_microorganismo_p
and		nvl(nr_cultura_microorg,0) = nvl(nr_cultura_microorg_p,0);

if      (nr_seq_result_antib_w <> 0) then
        begin

        insert into exame_lab_result_antib_adi (
                nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                nr_seq_exame_lab_result_antib,
                cd_medicamento
				)
        values( exame_lab_result_antib_adi_seq.nextval,
                sysdate,
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                nr_seq_result_antib_w,
                cd_medicamento_p);
        end;
end if;

commit;
end incluir_antib_adicional;
/