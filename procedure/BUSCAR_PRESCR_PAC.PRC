create or replace
procedure Buscar_prescr_pac(nr_sequencia_p	number,
                            ie_opcao_p		varchar2) is
			
nr_sequencia_w			nut_prod_lac_item.nr_sequencia%type;
nr_atendimento_w		prescr_medica.nr_atendimento%type;
nr_seq_dispositivo_w	prescr_leite_deriv.nr_seq_disp_succao%type;
nr_prescricao_w			prescr_medica.nr_prescricao%type;
		
Cursor C01 is
	select 	d.nr_sequencia,
		a.nr_atendimento,
		(SELECT max(l.nr_seq_disp_succao)
		FROM	prescr_leite_deriv l
		WHERE l.nr_sequencia = b.nr_seq_leite_deriv) nr_seq_dispositivo,
		a.nr_prescricao
	FROM	prescr_medica     a,
		prescr_material   b,
		nut_prod_lac_item d,
		prescr_mat_hor    e
	WHERE   b.nr_prescricao     = d.nr_prescricao 
	AND	b.nr_sequencia      = d.nr_seq_material
	AND	d.nr_seq_mat_hor    = e.nr_sequencia
	AND	d.nr_seq_prod_princ = nr_sequencia_p
	AND 	a.nr_prescricao     = b.nr_prescricao;		
	
begin

	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w,
		nr_atendimento_w,
		nr_seq_dispositivo_w,
		nr_prescricao_w;
	exit when C01%notfound;
		begin
		if (nr_sequencia_w is not null and nr_atendimento_w is not null) then
			if(ie_opcao_p = 'C') then
				nut_consistir_mat_conta_pac(nr_sequencia_w, nr_atendimento_w,nr_seq_dispositivo_w, nr_prescricao_w);
			else
				Insere_mat_resumo_pac(nr_sequencia_w, nr_atendimento_w,nr_seq_dispositivo_w, nr_prescricao_w);				
			end if;
		end if;
		end;
	end loop;
	close C01;
commit;
end Buscar_prescr_pac;
/
