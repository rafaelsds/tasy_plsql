create or replace
procedure Gerar_w_gsjs_ocupacao( mes_referencia_p	date,
				 ie_quinzena_p		number,
				 cd_estabelecimento_p	number) is 
				

type Valores is record (qt_dia_ref		number(5));
type Vetor is table of Valores index by binary_integer;
vt_internacao_w		vetor;
dt_temp_w		date;
nr_pos_vet_w		integer := 1;
nr_dia_inicial_w	number(10);
qt_valor_w		number(10,2);
cd_setor_atendimento_w	number(10);
ds_setor_atendimento_W	varchar(255);

Cursor C01 is
	select	cd_setor_atendimento,
		ds_setor_atendimento
	from	setor_atendimento
	where	ie_situacao = 'A'
	and	cd_setor_atendimento not in (43,115)
	and	cd_estabelecimento = cd_estabelecimento_p
	and	cd_classif_setor in (3,4)
	order by 1;

begin

delete W_GSJS_OCUPACAO;

commit;

if ie_quinzena_p = 1 then
	nr_dia_inicial_w := 1;
else
	nr_dia_inicial_w := 16;
end if;

open C01;
loop
fetch C01 into	
	cd_setor_atendimento_w,
	ds_setor_atendimento_W;
exit when C01%notfound;
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				0,
				'#' || ds_setor_atendimento_w || '#N',
				'#' || nr_dia_inicial_w     || '#N',
				'#' || (nr_dia_inicial_w +1)  || '#N',
				'#' || (nr_dia_inicial_w +2)  || '#N',
				'#' || (nr_dia_inicial_w +3)  || '#N',
				'#' || (nr_dia_inicial_w +4)  || '#N',
				'#' || (nr_dia_inicial_w +5)  || '#N',
				'#' || (nr_dia_inicial_w +6)  || '#N',
				'#' || (nr_dia_inicial_w +7)  || '#N',
				'#' || (nr_dia_inicial_w +8)  || '#N',
				'#' || (nr_dia_inicial_w +9)  || '#N',
				'#' || (nr_dia_inicial_w +10) || '#N',
				'#' || (nr_dia_inicial_w +11) || '#N',
				'#' || (nr_dia_inicial_w +12) || '#N',
				'#' || (nr_dia_inicial_w +13) || '#N',
				'#' || (nr_dia_inicial_w +14) || '#N');
				
	nr_pos_vet_w := 1;
	
	
	
	FOR i in nr_dia_inicial_w..nr_dia_inicial_w + 15 LOOP
		begin
			dt_temp_w	:= TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy');
		exception
		when others then
			exit;
		end;
		
		SELECT	COUNT(*)
		into	qt_valor_w
		FROM	atend_paciente_unidade b,
			atendimento_paciente a
		WHERE	a.nr_atendimento = b.nr_atendimento
		AND	obter_atepacu_paciente(a.nr_atendimento,'IA') = b.nr_seq_interno
		AND	a.ie_tipo_atendimento = 1
		AND	b.cd_setor_atendimento = cd_setor_atendimento_w
		and	a.cd_estabelecimento = cd_estabelecimento_p
		AND	a.dt_entrada <= TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy'))
		AND	(a.dt_alta_interno > TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')));
		vt_internacao_w(nr_pos_vet_w).qt_dia_ref := qt_valor_w;
		nr_pos_vet_w := nr_pos_vet_w +1;
	END LOOP;
	for i in vt_internacao_w.count..15 loop
		begin
		vt_internacao_w(i).qt_dia_ref := 0;
		end;
	end loop;
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				1,
				'          Inicial',
				vt_internacao_w(1).qt_dia_ref,
				vt_internacao_w(2).qt_dia_ref,
				vt_internacao_w(3).qt_dia_ref,
				vt_internacao_w(4).qt_dia_ref,
				vt_internacao_w(5).qt_dia_ref,
				vt_internacao_w(6).qt_dia_ref,
				vt_internacao_w(7).qt_dia_ref,
				vt_internacao_w(8).qt_dia_ref,
				vt_internacao_w(9).qt_dia_ref,
				vt_internacao_w(10).qt_dia_ref,
				vt_internacao_w(11).qt_dia_ref,
				vt_internacao_w(12).qt_dia_ref,
				vt_internacao_w(13).qt_dia_ref,
				vt_internacao_w(14).qt_dia_ref,
				vt_internacao_w(15).qt_dia_ref);
	vt_internacao_w.delete;	
	nr_pos_vet_w := 1;
	
	FOR i in nr_dia_inicial_w..nr_dia_inicial_w + 15 LOOP
		begin
			dt_temp_w	:= TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy');
		exception
		when others then
			exit;
		end;
		
		SELECT	COUNT(*)
		into	qt_valor_w
		FROM	atend_paciente_unidade b,
			atendimento_paciente a
		WHERE	a.nr_atendimento = b.nr_atendimento
		AND	obter_atepacu_paciente(a.nr_atendimento,'IA') = b.nr_seq_interno
		AND	a.ie_tipo_atendimento = 1
		AND	b.cd_setor_atendimento = cd_setor_atendimento_w
		and	a.cd_estabelecimento = cd_estabelecimento_p
		AND	a.dt_entrada <= fim_dia(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy'))
		AND	(a.dt_alta_interno > fim_dia(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')));
		vt_internacao_w(nr_pos_vet_w).qt_dia_ref := qt_valor_w;
		nr_pos_vet_w := nr_pos_vet_w +1;
	END LOOP;
	for i in vt_internacao_w.count..15 loop
		begin
		vt_internacao_w(i).qt_dia_ref := 0;
		end;
	end loop;
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				2,
				'          Final Dia',
				vt_internacao_w(1).qt_dia_ref,
				vt_internacao_w(2).qt_dia_ref,
				vt_internacao_w(3).qt_dia_ref,
				vt_internacao_w(4).qt_dia_ref,
				vt_internacao_w(5).qt_dia_ref,
				vt_internacao_w(6).qt_dia_ref,
				vt_internacao_w(7).qt_dia_ref,
				vt_internacao_w(8).qt_dia_ref,
				vt_internacao_w(9).qt_dia_ref,
				vt_internacao_w(10).qt_dia_ref,
				vt_internacao_w(11).qt_dia_ref,
				vt_internacao_w(12).qt_dia_ref,
				vt_internacao_w(13).qt_dia_ref,
				vt_internacao_w(14).qt_dia_ref,
				vt_internacao_w(15).qt_dia_ref);
	vt_internacao_w.delete;	
	nr_pos_vet_w := 1;
	
	FOR i in nr_dia_inicial_w..nr_dia_inicial_w + 15 LOOP
		begin
			dt_temp_w	:= TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy');
		exception
		when others then
			exit;
		end;
		
		SELECT	COUNT(*)
		into	qt_valor_w
		FROM	atend_paciente_unidade b,
			atendimento_paciente a
		WHERE	a.nr_atendimento = b.nr_atendimento
		AND	obter_atepacu_paciente(a.nr_atendimento,'IA') = b.nr_seq_interno
		AND	a.ie_tipo_atendimento = 1
		AND	b.cd_setor_atendimento = cd_setor_atendimento_w
		and	a.cd_estabelecimento = cd_estabelecimento_p
		AND	a.dt_entrada between TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')) and
			fim_dia(TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')));
		vt_internacao_w(nr_pos_vet_w).qt_dia_ref := qt_valor_w;
		nr_pos_vet_w := nr_pos_vet_w +1;
	END LOOP;
	for i in vt_internacao_w.count..15 loop
		begin
		vt_internacao_w(i).qt_dia_ref := 0;
		end;
	end loop;
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				3,
				'          Entradas',
				vt_internacao_w(1).qt_dia_ref,
				vt_internacao_w(2).qt_dia_ref,
				vt_internacao_w(3).qt_dia_ref,
				vt_internacao_w(4).qt_dia_ref,
				vt_internacao_w(5).qt_dia_ref,
				vt_internacao_w(6).qt_dia_ref,
				vt_internacao_w(7).qt_dia_ref,
				vt_internacao_w(8).qt_dia_ref,
				vt_internacao_w(9).qt_dia_ref,
				vt_internacao_w(10).qt_dia_ref,
				vt_internacao_w(11).qt_dia_ref,
				vt_internacao_w(12).qt_dia_ref,
				vt_internacao_w(13).qt_dia_ref,
				vt_internacao_w(14).qt_dia_ref,
				vt_internacao_w(15).qt_dia_ref);
	vt_internacao_w.delete;	
	nr_pos_vet_w := 1;
	
	FOR i in nr_dia_inicial_w..nr_dia_inicial_w + 15 LOOP
		begin
			dt_temp_w	:= TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy');
		exception
		when others then
			exit;
		end;
		
		SELECT	COUNT(*)
		INTO	qt_valor_w
		FROM	atend_paciente_unidade b,
			atendimento_paciente a
		WHERE	a.nr_atendimento = b.nr_atendimento
		AND	obter_atepacu_paciente(a.nr_atendimento,'IA') = b.nr_seq_interno
		AND	a.ie_tipo_atendimento = 1
		and	a.cd_motivo_alta not in (7,19,21)
		AND	b.cd_setor_atendimento = cd_setor_atendimento_w
		and	a.cd_estabelecimento = cd_estabelecimento_p
		AND	a.dt_alta between TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')) and
			fim_dia(TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')));
		vt_internacao_w(nr_pos_vet_w).qt_dia_ref := qt_valor_w;
		nr_pos_vet_w := nr_pos_vet_w +1;
	END LOOP;
	for i in vt_internacao_w.count..15 loop
		begin
		vt_internacao_w(i).qt_dia_ref := 0;
		end;
	end loop;
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				4,
				'          Altas Normais',
				vt_internacao_w(1).qt_dia_ref,
				vt_internacao_w(2).qt_dia_ref,
				vt_internacao_w(3).qt_dia_ref,
				vt_internacao_w(4).qt_dia_ref,
				vt_internacao_w(5).qt_dia_ref,
				vt_internacao_w(6).qt_dia_ref,
				vt_internacao_w(7).qt_dia_ref,
				vt_internacao_w(8).qt_dia_ref,
				vt_internacao_w(9).qt_dia_ref,
				vt_internacao_w(10).qt_dia_ref,
				vt_internacao_w(11).qt_dia_ref,
				vt_internacao_w(12).qt_dia_ref,
				vt_internacao_w(13).qt_dia_ref,
				vt_internacao_w(14).qt_dia_ref,
				vt_internacao_w(15).qt_dia_ref);
	vt_internacao_w.delete;	
	nr_pos_vet_w := 1;
	
	FOR i in nr_dia_inicial_w..nr_dia_inicial_w + 15 LOOP
		begin
			dt_temp_w	:= TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy');
		exception
		when others then
			exit;
		end;
		
		SELECT	COUNT(*)
		INTO	qt_valor_w
		FROM	atend_paciente_unidade b,
			atendimento_paciente a
		WHERE	a.nr_atendimento = b.nr_atendimento
		AND	obter_atepacu_paciente(a.nr_atendimento,'IA') = b.nr_seq_interno
		AND	a.ie_tipo_atendimento = 1
		AND	b.cd_setor_atendimento = cd_setor_atendimento_w
		and	a.cd_estabelecimento = cd_estabelecimento_p
		AND	a.cd_motivo_alta in (7,21,19)
		AND	a.dt_alta BETWEEN TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')) AND
			fim_dia(TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')));
		vt_internacao_w(nr_pos_vet_w).qt_dia_ref := qt_valor_w;
		nr_pos_vet_w := nr_pos_vet_w +1;
	END LOOP;
	for i in vt_internacao_w.count..15 loop
		begin
		vt_internacao_w(i).qt_dia_ref := 0;
		end;
	end loop;
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				5,
				'          Altas - �bitos',
				vt_internacao_w(1).qt_dia_ref,
				vt_internacao_w(2).qt_dia_ref,
				vt_internacao_w(3).qt_dia_ref,
				vt_internacao_w(4).qt_dia_ref,
				vt_internacao_w(5).qt_dia_ref,
				vt_internacao_w(6).qt_dia_ref,
				vt_internacao_w(7).qt_dia_ref,
				vt_internacao_w(8).qt_dia_ref,
				vt_internacao_w(9).qt_dia_ref,
				vt_internacao_w(10).qt_dia_ref,
				vt_internacao_w(11).qt_dia_ref,
				vt_internacao_w(12).qt_dia_ref,
				vt_internacao_w(13).qt_dia_ref,
				vt_internacao_w(14).qt_dia_ref,
				vt_internacao_w(15).qt_dia_ref);
	vt_internacao_w.delete;	
	nr_pos_vet_w := 1;
	
	FOR i in nr_dia_inicial_w..nr_dia_inicial_w + 15 LOOP
		begin
			dt_temp_w	:= TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy');
		exception
		when others then
			exit;
		end;
		
		SELECT	COUNT(*)
		into	qt_valor_w
		FROM	atend_paciente_unidade b,
			atendimento_paciente a
		WHERE	a.nr_atendimento = b.nr_atendimento
		AND	obter_atepacu_paciente(a.nr_atendimento,'IA') = b.nr_seq_interno
		AND	a.ie_tipo_atendimento = 1
		AND	b.cd_setor_atendimento = cd_setor_atendimento_w
		and	a.cd_estabelecimento = cd_estabelecimento_p
		AND	Obter_Tipo_Unidade_Atend(b.nr_atendimento, nr_seq_interno, ie_passagem_setor) = 'N'
		AND	b.dt_saida_unidade BETWEEN TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')) AND
			fim_dia(TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')));
		vt_internacao_w(nr_pos_vet_w).qt_dia_ref := qt_valor_w;
		nr_pos_vet_w := nr_pos_vet_w +1;
	END LOOP;
	for i in vt_internacao_w.count..15 loop
		begin
		vt_internacao_w(i).qt_dia_ref := 0;
		end;
	end loop;
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				6,
				'          Altas - Transfer�ncias',
				vt_internacao_w(1).qt_dia_ref,
				vt_internacao_w(2).qt_dia_ref,
				vt_internacao_w(3).qt_dia_ref,
				vt_internacao_w(4).qt_dia_ref,
				vt_internacao_w(5).qt_dia_ref,
				vt_internacao_w(6).qt_dia_ref,
				vt_internacao_w(7).qt_dia_ref,
				vt_internacao_w(8).qt_dia_ref,
				vt_internacao_w(9).qt_dia_ref,
				vt_internacao_w(10).qt_dia_ref,
				vt_internacao_w(11).qt_dia_ref,
				vt_internacao_w(12).qt_dia_ref,
				vt_internacao_w(13).qt_dia_ref,
				vt_internacao_w(14).qt_dia_ref,
				vt_internacao_w(15).qt_dia_ref);
	vt_internacao_w.delete;	
	nr_pos_vet_w := 1;
	
	SELECT	COUNT(*)
	into	qt_valor_w
	FROM	setor_atendimento c,
		unidade_atendimento b
	WHERE 	b.cd_setor_atendimento     	= c.cd_setor_atendimento
	AND	b.ie_situacao              	= 'A'
	and	c.cd_estabelecimento = cd_estabelecimento_p
	AND	b.ie_temporario <> 'S'
	AND	c.cd_setor_atendimento 	= cd_setor_atendimento_w;
	
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				7,
				'          Leitos',
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w,
				qt_valor_w);
				
	FOR i in nr_dia_inicial_w..nr_dia_inicial_w + 15 LOOP
		begin
			dt_temp_w	:= TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy');
		exception
		when others then
			exit;
		end;
		
		SELECT	COUNT(*)
		INTO	qt_valor_w
		FROM	unidade_atendimento c,
			atend_paciente_unidade b,
			atendimento_paciente a
		WHERE	a.nr_atendimento = b.nr_atendimento
		AND	b.cd_setor_atendimento = c.cd_setor_atendimento
		AND	b.cd_unidade_basica	   = c.cd_unidade_basica
		AND	b.cd_unidade_compl	   = c.cd_unidade_compl
		AND	obter_atepacu_paciente(a.nr_atendimento,'IA') = b.nr_seq_interno
		AND	c.ie_temporario = 'S'
		AND	a.ie_tipo_atendimento = 1
		AND	b.cd_setor_atendimento = cd_setor_atendimento_w
		and	a.cd_estabelecimento = cd_estabelecimento_p
		AND	a.dt_entrada <= TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy'))
		AND	(a.dt_alta_interno > TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')));
		vt_internacao_w(nr_pos_vet_w).qt_dia_ref := qt_valor_w;
		nr_pos_vet_w := nr_pos_vet_w +1;
	END LOOP;
	for i in vt_internacao_w.count..15 loop
		begin
		vt_internacao_w(i).qt_dia_ref := 0;
		end;
	end loop;
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				8,
				'          Leitos Temp. Ocupados',
				vt_internacao_w(1).qt_dia_ref,
				vt_internacao_w(2).qt_dia_ref,
				vt_internacao_w(3).qt_dia_ref,
				vt_internacao_w(4).qt_dia_ref,
				vt_internacao_w(5).qt_dia_ref,
				vt_internacao_w(6).qt_dia_ref,
				vt_internacao_w(7).qt_dia_ref,
				vt_internacao_w(8).qt_dia_ref,
				vt_internacao_w(9).qt_dia_ref,
				vt_internacao_w(10).qt_dia_ref,
				vt_internacao_w(11).qt_dia_ref,
				vt_internacao_w(12).qt_dia_ref,
				vt_internacao_w(13).qt_dia_ref,
				vt_internacao_w(14).qt_dia_ref,
				vt_internacao_w(15).qt_dia_ref);
	vt_internacao_w.delete;	
	nr_pos_vet_w := 1;
	
	FOR i in nr_dia_inicial_w..nr_dia_inicial_w + 15 LOOP
		begin
			dt_temp_w	:= TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy');
		exception
		when others then
			exit;
		end;
		
		SELECT	nvl(max(ROUND(((COUNT(*)* 100) / z.qt_total_leito),2)),0)
		into	qt_valor_w
		FROM	atend_paciente_unidade x,
			atendimento_paciente y,
			(SELECT	SUM(qt_leito_temp_ocup + qt_leito) qt_total_leito
			 FROM	(SELECT	COUNT(*) qt_leito_temp_ocup
				FROM	unidade_atendimento c,
					atend_paciente_unidade b,
					atendimento_paciente a
				WHERE	a.nr_atendimento = b.nr_atendimento
				AND	b.cd_setor_atendimento = c.cd_setor_atendimento
				AND	b.cd_unidade_basica	   = c.cd_unidade_basica
				AND	b.cd_unidade_compl	   = c.cd_unidade_compl
				AND	obter_atepacu_paciente(a.nr_atendimento,'IA') = b.nr_seq_interno
				AND	c.ie_temporario = 'S'
				AND	a.ie_tipo_atendimento = 1
				AND	b.cd_setor_atendimento = cd_setor_atendimento_w
				AND	a.cd_estabelecimento = cd_estabelecimento_p
				AND	a.dt_entrada <= TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy'))
				AND	(a.dt_alta_interno > TRUNC(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')))) a,
				(SELECT	COUNT(*) qt_leito
				FROM	setor_atendimento c,
					unidade_atendimento b
				WHERE 	b.cd_setor_atendimento     	= c.cd_setor_atendimento
				AND	b.ie_situacao              	= 'A'
				AND	c.cd_estabelecimento = cd_estabelecimento_p
				AND	b.ie_temporario <> 'S'
				AND	c.cd_setor_atendimento 	= cd_setor_atendimento_w) b) z
			WHERE	y.nr_atendimento = x.nr_atendimento
			AND	obter_atepacu_paciente(y.nr_atendimento,'IA') = x.nr_seq_interno
			AND	y.ie_tipo_atendimento = 1
			AND	x.cd_setor_atendimento = cd_setor_atendimento_w
			AND	y.cd_estabelecimento = cd_estabelecimento_p
			AND	y.dt_entrada <= fim_dia(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy'))
			AND	(y.dt_alta_interno > fim_dia(TO_DATE(i||'/'||TO_CHAR(mes_referencia_p,'mm/yyyy'),'dd/mm/yyyy')))
			GROUP BY z.qt_total_leito;
		vt_internacao_w(nr_pos_vet_w).qt_dia_ref := qt_valor_w;
		nr_pos_vet_w := nr_pos_vet_w +1;
	END LOOP;
	for i in vt_internacao_w.count..15 loop
		begin
		vt_internacao_w(i).qt_dia_ref := 0;
		end;
	end loop;
	insert into W_GSJS_OCUPACAO(	cd_setor_atendimento,
				ie_quinzena,
				ie_ocupacao,
				ds_ocupacao,
				qt_dia_1,
				qt_dia_2,
				qt_dia_3,
				qt_dia_4,
				qt_dia_5,
				qt_dia_6,
				qt_dia_7,
				qt_dia_8,
				qt_dia_9,
				qt_dia_10,
				qt_dia_11,
				qt_dia_12,
				qt_dia_13,
				qt_dia_14,
				qt_dia_15)
			values(	cd_setor_atendimento_w,
				ie_quinzena_p,
				9,
				'          % Ocupa��o',
				vt_internacao_w(1).qt_dia_ref,
				vt_internacao_w(2).qt_dia_ref,
				vt_internacao_w(3).qt_dia_ref,
				vt_internacao_w(4).qt_dia_ref,
				vt_internacao_w(5).qt_dia_ref,
				vt_internacao_w(6).qt_dia_ref,
				vt_internacao_w(7).qt_dia_ref,
				vt_internacao_w(8).qt_dia_ref,
				vt_internacao_w(9).qt_dia_ref,
				vt_internacao_w(10).qt_dia_ref,
				vt_internacao_w(11).qt_dia_ref,
				vt_internacao_w(12).qt_dia_ref,
				vt_internacao_w(13).qt_dia_ref,
				vt_internacao_w(14).qt_dia_ref,
				vt_internacao_w(15).qt_dia_ref);
	vt_internacao_w.delete;	
	nr_pos_vet_w := 1;
end loop;
close C01;
commit;

end Gerar_w_gsjs_ocupacao;
/