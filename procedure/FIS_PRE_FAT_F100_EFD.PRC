CREATE OR REPLACE 
procedure fis_pre_fat_F100_efd(	nr_seq_controle_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p			date,
				cd_empresa_p		number,
				ds_separador_p		varchar2,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is

pr_imposto_cofins_w		number(7,4);
pr_imposto_pis_w		number(7,4);
aliquota_iss_w			number(7,4);
cd_tributo_pis_w		number(3);
cd_tributo_cofins_w		number(3);
cd_tributo_iss_w		number(3);
nr_seq_regra_efd_w		number(10);
nr_versao_efd_w			varchar2(5);
tp_registro_w			varchar2(4);
nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;
ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
sep_w				varchar2(1) := ds_separador_p;
nr_sequencia_w			number(10);
vl_material_incentivo_w		number(15,2);
vl_base_calculo_tributo_w	number(15,2);
vl_pis_w			number(15,2);
vl_cofins_w			number(15,2);
vl_iss_retido_w			number(15,2);
vl_pis_retido_w			number(15,2);
vl_cofins_retido_w		number(15,2);
ie_local_gerar_sped_w		varchar2(1);
ie_consistir_cpf_cnpj_w		varchar2(1);
dt_operacao_w			date;
vl_glosa_w			number(15,2);
vl_monofasico_w			number(15,2);
vl_base_calculo_w		number(15,2);
qt_registros_w			number(10);
vl_pre_faturamento_w		number(15,2) := 0;
vl_pre_faturamento_atu_w	number(15,2);
vl_pre_faturamento_ant_w	number(15,2) := 0;
ds_convenio_w			varchar2(255);
vl_faturamento_w		number(15,2) := 0;
dt_fim_ant_w			date;
dt_inicio_ant_w			date;
dt_fim_atual_w			date;
dt_inicio_atual_w		date;

cursor c01 is
		select	distinct 'F100'								tp_registro,
			'1'									ie_receita,
			decode(c.ie_tipo_convenio,1,a.cd_pessoa_fisica,c.cd_cgc) cd_participante,
			sum(f.vl_procedimento + f.vl_material) vl_pre_faturamento_at,
			''									cd_item,
			'01'									cd_cst_pis,
			pr_imposto_pis_w							pr_aliquota_pis,
			'01'									cd_cst_cofins,
			pr_imposto_cofins_w							pr_aliquota_cofins,
			''									cd_base_calculo,
			''									ie_origem_credito,
			''									cd_conta_contabil,
			''									cd_centro_custos,
			''									ds_documento
		from	pre_faturamento f,
				conta_paciente p,
				atendimento_paciente a,	
				convenio c
		where	f.cd_convenio = c.cd_convenio
		and	f.nr_interno_conta = p.nr_interno_conta(+)
		and	p.nr_atendimento = a.nr_atendimento(+)
		and	dt_referencia between dt_inicio_atual_w and dt_fim_atual_w
		and	f.cd_estabelecimento = cd_estabelecimento_p
		group by	decode(c.ie_tipo_convenio,1,a.cd_pessoa_fisica,c.cd_cgc),
					pr_imposto_pis_w,
					pr_imposto_cofins_w
		union
		select	distinct 'F100'								tp_registro,
			'1'									ie_receita,
			decode(c.ie_tipo_convenio,1,a.cd_pessoa_fisica,c.cd_cgc) cd_participante,
			sum(f.vl_procedimento + f.vl_material) vl_pre_faturamento_at,
			''									cd_item,
			'01'									cd_cst_pis,
			pr_imposto_pis_w							pr_aliquota_pis,
			'01'									cd_cst_cofins,
			pr_imposto_cofins_w							pr_aliquota_cofins,
			''									cd_base_calculo,
			''									ie_origem_credito,
			''									cd_conta_contabil,
			''									cd_centro_custos,
			''									ds_documento
		from	pre_faturamento f,
				conta_paciente p,
				atendimento_paciente a,	
				convenio c
		where	f.cd_convenio = c.cd_convenio
		and	f.nr_interno_conta = p.nr_interno_conta(+)
		and	p.nr_atendimento = a.nr_atendimento(+)
		and	dt_referencia between dt_inicio_ant_w and dt_fim_ant_w
		and	f.cd_estabelecimento = cd_estabelecimento_p
		group by	decode(c.ie_tipo_convenio,1,a.cd_pessoa_fisica,c.cd_cgc),
					pr_imposto_pis_w,
					pr_imposto_cofins_w;

vet01	C01%RowType;

begin

dt_inicio_atual_w	:= trunc(dt_inicio_p,'mm');
dt_fim_atual_w		:= fim_dia(last_day(dt_fim_p));

dt_inicio_ant_w		:= trunc(add_months(dt_inicio_p,-1),'mm');
dt_fim_ant_w		:= fim_dia(last_day(add_months(dt_fim_p,-1)));

select	nr_seq_regra_efd
into	nr_seq_regra_efd_w
from	fis_efd_controle
where	nr_sequencia = nr_seq_controle_p;

select	cd_tributo_pis,
	cd_tributo_cofins,
	cd_tributo_iss
into	cd_tributo_pis_w,
	cd_tributo_cofins_w,
	cd_tributo_iss_w
from	fis_regra_efd
where	nr_sequencia = nr_seq_regra_efd_w;

pr_imposto_pis_w 	:= obter_pr_imposto(cd_tributo_pis_w);
pr_imposto_cofins_w 	:= obter_pr_imposto(cd_tributo_cofins_w);

open C01;
loop
fetch C01 into
	vet01;
exit when C01%notfound;
	begin

	select	count(*)
	into	qt_registros_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = vet01.cd_participante;
	
	if (qt_registros_w > 0) then
	
		vl_faturamento_w := nvl(vet01.vl_pre_faturamento_at,0);
		
		select	sum(vl_material)
		into	vl_monofasico_w
		from	efd_monofasico
		where	cd_pessoa_fisica = vet01.cd_participante;
		
		vl_base_calculo_w := vl_faturamento_w - nvl(vl_monofasico_w,0);
		
	else
		
		select	max(x.dt_mesano_referencia)
		into	dt_operacao_w
		from 	conta_paciente_status_v x,
				pessoa_juridica d,
				convenio c
		where 	c.cd_convenio = x.cd_convenio
		and		c.cd_cgc = d.cd_cgc
		and 	exists (select 1 from dual where x.cd_estab_conta = 2)
		and		x.dt_mesano_referencia between dt_inicio_atual_w and dt_fim_atual_w
		and		c.cd_cgc = vet01.cd_participante;
		
		if (dt_operacao_w is null) then
			dt_operacao_w := dt_fim_p;
			vl_faturamento_w := 0;
		else
			select	sum(x.vl_total_receita)		vl_operacao
			into	vl_faturamento_w
			from 	conta_paciente_status_v x,
					pessoa_juridica d,
					convenio c
			where 	c.cd_convenio = x.cd_convenio
			and		c.cd_cgc = d.cd_cgc
			and 	exists (select 1 from dual where x.cd_estab_conta = 2)
			and  	x.dt_mesano_referencia between dt_inicio_atual_w and dt_fim_atual_w
			and		c.cd_cgc = vet01.cd_participante;
		end if;
		
		select	sum(nvl(a.vl_procedimento,0) + nvl(a.vl_material,0)) vl_total_receita
		into	vl_pre_faturamento_ant_w
		from	pre_faturamento a,
				convenio c
		where	a.cd_convenio = c.cd_convenio
		and		dt_referencia between dt_inicio_ant_w and dt_fim_ant_w
		and		a.cd_estabelecimento = cd_estabelecimento_p
		and		c.cd_convenio  in	( 	select	d.cd_convenio
										from	convenio d
										where	d.cd_cgc = vet01.cd_participante);

		select	sum(nvl(a.vl_procedimento,0) + nvl(a.vl_material,0)) vl_total_receita
		into	vl_pre_faturamento_atu_w
		from	pre_faturamento a,
				convenio c
		where	a.cd_convenio = c.cd_convenio
		and		dt_referencia between dt_inicio_atual_w and dt_fim_atual_w
		and		a.cd_estabelecimento = cd_estabelecimento_p
		and		c.cd_convenio  in	( 	select	d.cd_convenio
										from	convenio d
										where	d.cd_cgc = vet01.cd_participante);
		
		vl_faturamento_w 			:= nvl(vl_faturamento_w,0);
		vl_pre_faturamento_atu_w 	:= nvl(vl_pre_faturamento_atu_w,0);
		vl_pre_faturamento_ant_w	:= nvl(vl_pre_faturamento_ant_w,0);
		
		vl_pre_faturamento_w := vl_pre_faturamento_atu_w - vl_pre_faturamento_ant_w;

		vl_base_calculo_w := vl_faturamento_w + vl_pre_faturamento_w;	
		
		select	obter_nome_pf_pj('', vet01.cd_participante) into ds_convenio_w from dual;
	
	end if;
	
	ds_linha_w	:= substr(	sep_w || vet01.tp_registro	 							||
					sep_w || vet01.ie_receita	 							||
					sep_w || vet01.cd_participante	 							||
					sep_w || vet01.cd_item	 								||
					sep_w || to_Char(dt_operacao_w,'ddmmyyyy')						||
					sep_w || replace(campo_mascara(vl_base_calculo_w,2),'.',',')	||
					sep_w || vet01.cd_cst_pis	 							||
					sep_w || replace(campo_mascara(vl_base_calculo_w,2),'.',',') 				||
					sep_w || replace(campo_mascara(pr_imposto_pis_w,2),'.',',')				||
					sep_w || replace(campo_mascara(vl_base_calculo_w * pr_imposto_pis_w/100,2),'.',',')	 	||
					sep_w || vet01.cd_cst_cofins	 							||
					sep_w || replace(campo_mascara(vl_base_calculo_w,2),'.',',')  				||
					sep_w || replace(campo_mascara(pr_imposto_cofins_w,2),'.',',')				||
					sep_w || replace(campo_mascara(vl_base_calculo_w * pr_imposto_cofins_w/100,2),'.',',')	||
					sep_w || vet01.cd_base_calculo								||
					sep_w || vet01.ie_origem_credito							||
					sep_w || vet01.ie_origem_credito							||
					sep_w || vet01.cd_centro_custos								||
					sep_w || vet01.ds_documento								|| sep_w,1,8000);

	ds_arquivo_w := substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w := substr(ds_linha_w,4001,4000);
	nr_seq_registro_w := nr_seq_registro_w + 1;
	nr_linha_w := nr_linha_w + 1;

	insert into fis_efd_arquivo	(
					nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nr_seq_controle_efd,
					nr_linha,
					cd_registro,
					ds_arquivo,
					ds_arquivo_compl)
			values		(
					nr_seq_registro_w,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nr_seq_controle_p,
					nr_linha_w,
					vet01.tp_registro,
					ds_arquivo_w,
					ds_arquivo_compl_w);
	
	commit;
	
	end;
end loop;
close C01;

qt_linha_p := nr_linha_w;
nr_sequencia_p := nr_seq_registro_w;

end fis_pre_fat_F100_efd;
/