create or replace
procedure gravar_log_laudo_ditado (  	ds_arquivo_p		varchar2,
					nm_usuario_p		Varchar2) is 
					

begin
	
insert into prescr_proc_ditado_log (	
	nr_sequencia, 
	dt_atualizacao, 
	nm_usuario, 
	dt_atualizacao_nrec, 
	nm_usuario_nrec, 
	ds_arquivo )
values(	prescr_proc_ditado_log_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	ds_arquivo_p );
		
commit;

end gravar_log_laudo_ditado;
/