create or replace procedure ctb_gerar_planej_orc_gpi(   nr_seq_planej_p ctb_orc_cenario.nr_sequencia%type,
                                                        nm_usuario_p varchar2) is

nr_seq_gpi_orc_w        gpi_orcamento.nr_sequencia%type;
nr_seq_proj_gpi_ww      gpi_projeto.nr_sequencia%type;
nr_seq_gpi_orc_item_w   gpi_orc_item.nr_sequencia%type;
ex_continue             exception;

cursor c_planej_orc_material is
select  a.nr_seq_proj_gpi,
        a.dt_referencia,
        a.cd_conta_contabil,
        a.cd_material,
        a.cd_centro_custo,
        a.cd_conta_financ,
        a.cd_cnpj,
        nvl(gpi_obter_conta(a.cd_empresa,sysdate,a.cd_material), 0) nr_seq_plano,
        a.cd_empresa,
        sum(a.vl_planej) vl_planej,
        sum(a.qt_planej) qt_planej
from    ctb_planej_orc_material_v a
where   a.nr_seq_planej = nr_seq_planej_p
and     a.nr_seq_proj_gpi is not null
group by a.nr_seq_proj_gpi,
         a.dt_referencia,
         a.cd_conta_contabil,
         a.cd_material,
         a.cd_centro_custo,
         a.cd_cnpj,
         a.cd_conta_financ,
         a.cd_empresa
order by 1 desc;

type t_planej_orc_material is table of c_planej_orc_material%rowtype index by pls_integer;
v_planej_orc_material_w t_planej_orc_material;

begin

delete 
from    gpi_orc_item a
where   a.nr_seq_orcamento in (
    select  nr_sequencia
    from    gpi_orcamento b
    where   b.nr_seq_planej_orc = nr_seq_planej_p
);

open c_planej_orc_material;
loop fetch c_planej_orc_material bulk collect into v_planej_orc_material_w limit 1000;
    for i in 1 .. v_planej_orc_material_w.count loop
    begin

        if  (nvl(v_planej_orc_material_w(i).nr_seq_plano,0) = 0) then
            select  max(a.nr_sequencia)
            into    v_planej_orc_material_w(i).nr_seq_plano
            from    gpi_plano a
            where   a.cd_conta_contabil = v_planej_orc_material_w(i).cd_conta_contabil
            and     a.cd_empresa = v_planej_orc_material_w(i).cd_empresa
            and     a.ie_situacao = 'A';

            if  (nvl(v_planej_orc_material_w(i).nr_seq_plano,0) = 0) then
                raise ex_continue;
            end if;

        end if;

        if ((v_planej_orc_material_w(i).nr_seq_proj_gpi <> nr_seq_proj_gpi_ww) or (nr_seq_proj_gpi_ww is null)) then
            begin
            nr_seq_proj_gpi_ww := v_planej_orc_material_w(i).nr_seq_proj_gpi;

            begin
            select  a.nr_sequencia
            into    nr_seq_gpi_orc_w
            from    gpi_orcamento a
            where   a.nr_seq_planej_orc     = nr_seq_planej_p
            and     a.nr_seq_projeto        = nr_seq_proj_gpi_ww;
            exception
                when others then
                    nr_seq_gpi_orc_w := null;
            end;

            if  (nr_seq_gpi_orc_w is null) then

                select  gpi_orcamento_seq.nextval
                into    nr_seq_gpi_orc_w
                from    dual;

                insert into gpi_orcamento (
                    nr_sequencia,
                    nr_seq_projeto,
                    nr_seq_planej_orc,
                    dt_atualizacao,
                    nm_usuario,
                    dt_atualizacao_nrec,
                    nm_usuario_nrec,
                    ds_orcamento,
                    dt_orcamento,
                    ie_preliminar_definitivo,
                    ie_situacao,
                    ie_prioridade
                ) values (
                    nr_seq_gpi_orc_w,
                    nr_seq_proj_gpi_ww,
                    nr_seq_planej_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    wheb_mensagem_pck.get_texto(1116051), -- Planejamento Orcamentario
                    sysdate,
                    'P',
                    'A',
                    'N'
                );
            end if;
            end;
        end if;

        update  gpi_orc_item a
        set     a.vl_orcado         = a.vl_orcado + v_planej_orc_material_w(i).vl_planej,
                a.qt_item           = a.qt_item + v_planej_orc_material_w(i).qt_planej,
                a.vl_unitario       = dividir((a.vl_orcado + v_planej_orc_material_w(i).vl_planej), (a.qt_item + v_planej_orc_material_w(i).qt_planej)),
                a.dt_atualizacao    = sysdate,
                a.nm_usuario        = nm_usuario_p
        where   a.nr_seq_orcamento  = nr_seq_gpi_orc_w
        and     a.nr_seq_plano      = v_planej_orc_material_w(i).nr_seq_plano
        and     a.cd_material       = v_planej_orc_material_w(i).cd_material
        and     a.cd_centro_custo   = v_planej_orc_material_w(i).cd_centro_custo
        and     a.dt_mes_referencia = v_planej_orc_material_w(i).dt_referencia
        and     nvl(a.cd_conta_financ,0) = nvl(v_planej_orc_material_w(i).cd_conta_financ,0);

        if  (sql%notfound and v_planej_orc_material_w(i).vl_planej > 0) then

            select  gpi_orc_item_seq.nextval
            into    nr_seq_gpi_orc_item_w
            from    dual;

            insert into gpi_orc_item (
                nr_sequencia,
                nr_seq_orcamento,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                nr_seq_plano,
                vl_orcado,
                vl_realizado,
                cd_material,
                qt_item,
                vl_unitario,
                cd_centro_custo,
                ds_fornecedor,
                dt_mes_referencia,
                ie_situacao,
                cd_conta_financ
            ) values (
                nr_seq_gpi_orc_item_w,
                nr_seq_gpi_orc_w,
                sysdate,
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                v_planej_orc_material_w(i).nr_seq_plano,
                v_planej_orc_material_w(i).vl_planej,
                0,
                v_planej_orc_material_w(i).cd_material,
                v_planej_orc_material_w(i).qt_planej,
                dividir(v_planej_orc_material_w(i).vl_planej, v_planej_orc_material_w(i).qt_planej),
                v_planej_orc_material_w(i).cd_centro_custo,
                obter_nome_pj(v_planej_orc_material_w(i).cd_cnpj),
                v_planej_orc_material_w(i).dt_referencia,
                'A',
                v_planej_orc_material_w(i).cd_conta_financ
            );
        
        end if;
    exception
      when ex_continue then
        null;
    end;
    end loop;
exit when c_planej_orc_material%notfound;
end loop;
close c_planej_orc_material;

commit;

end ctb_gerar_planej_orc_gpi;
/
