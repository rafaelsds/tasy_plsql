create or replace
procedure man_liberar_historico_acordo(
			nr_seq_hist_acordo_p	number,
			nm_usuario_p		varchar2) is

begin

if	(nr_seq_hist_acordo_p is not null) then
	begin

	update	desenv_acordo_hist
	set	dt_liberacao = sysdate
	where	nr_sequencia = nr_seq_hist_acordo_p;

	commit;

	end;
end if;

end man_liberar_historico_acordo;
/