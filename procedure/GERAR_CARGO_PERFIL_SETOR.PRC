Create or replace
procedure gerar_cargo_perfil_setor (	cd_cargo_p		number,
				cd_pessoa_fisica_p		varchar2,
				ie_opcao_p		varchar2,
				cd_setor_atendimento_p	number,
				nm_usuario_p		varchar2,
				nm_usuario_atual_p		varchar2,
				cd_estabelecimento_p	number) is

cd_perfil_w			number(5);
cd_setor_atendimento_w		number(5);
NR_SEQ_REGRA_CARGO_w		number(10);
qt_registro_w			number(10);
ie_liberar_cargos_w		varchar2(1);
ie_limpar_perfil_w		varchar2(1);
ie_limpar_setor_w		varchar2(1);
ie_tipo_evolucao_w		varchar2(3);
cd_estabelecimento_w   		number(5);
cd_setor_padrao_w   		number(5);
nr_seq_perfil_w    		number(5);
cd_estabelecimento_principal_w  number(5);

CURSOR C01 is
	select	cd_perfil
	from	cargo_perfil
	where	cd_cargo = cd_cargo_p;

CURSOR C02 is
	select	cd_setor_atendimento
	from	cargo_setor
	where	cd_cargo = cd_cargo_p;
	
Cursor c03 is
	select	cd_setor_atendimento
	from	regra_cargo_setor_lib
	where	nr_seq_regra_cargo	= nr_seq_regra_cargo_w;
	
Cursor c04 is
	select	cd_perfil
	from	regra_cargo_perfil_lib
	where	nr_seq_regra_cargo	= nr_seq_regra_cargo_w;
	
Cursor c05 is
	 select cd_estabelecimento,
		cd_setor_padrao,
		nr_seq_perfil
	 from 	cargo_estabelecimento
	 where 	cd_cargo			= cd_cargo_p
	 and 	ie_estabelecimento_principal	= 'N';	

begin

if	(nm_usuario_atual_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(189463);
end if;

select	max(nr_sequencia)
into	nr_seq_regra_cargo_w
from	regra_cargo_setor
where	cd_cargo			= cd_cargo_p
and	cd_setor_atendimento	= cd_setor_atendimento_p;

obter_param_usuario(230,31,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_limpar_perfil_w);
obter_param_usuario(230,32,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_limpar_setor_w);

if	(nvl(nr_seq_regra_cargo_w,0) <> 0) or (nvl(ie_limpar_perfil_w,'S') = 'S') then
	delete	from usuario_perfil
	where	nm_usuario	= nm_usuario_atual_p;
end if;

if	(nvl(nr_seq_regra_cargo_w,0) <> 0) or (nvl(ie_limpar_setor_w,'S') = 'S') then
	delete	from usuario_setor
	where 	nm_usuario_param = nm_usuario_atual_p;
end if;

obter_param_usuario(230,29,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_liberar_cargos_w);

if	not ((nr_seq_regra_cargo_w is not null) and (ie_liberar_cargos_w = 'N')) then

	open C01;
	loop
	fetch C01 into cd_perfil_w;
	exit when C01%notfound;
		begin
	
		select  count(*)
		into	qt_registro_w
		from	usuario_perfil
		where	nm_usuario = nm_usuario_atual_p
		and	cd_perfil = cd_perfil_w; 
		
		if (qt_registro_w = 0) then
			insert into usuario_perfil
				(nm_usuario,
				cd_perfil,
				dt_atualizacao,
				nm_usuario_atual)
			values	(nm_usuario_atual_p,
				cd_perfil_w,
				sysdate,
				nm_usuario_p);
		end if;
		
		end;
	end loop;
	close C01;
	
	open C02;
	loop
	fetch C02 into cd_setor_atendimento_w;
	exit when C02%notfound;
		begin
		
		select	count(*)
		into	qt_registro_w
		from	usuario_setor
		where	nm_usuario_param = nm_usuario_atual_p
		and	cd_setor_atendimento = cd_setor_atendimento_w;
		
		if (qt_registro_w = 0) then
		
			insert into usuario_setor(	nm_usuario_param,
						cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario)
				values	(	nm_usuario_atual_p,
						cd_setor_atendimento_w,
						sysdate,
						nm_usuario_p);
			end if;
		end;
	end loop;
	close C02;

end if;

if	(nr_seq_regra_cargo_w is not null) then
	
	open C03;
	loop
	fetch C03 into	
		cd_setor_atendimento_w;
	exit when C03%notfound;
		begin
		
		select	count(*)
		into	qt_registro_w
		from	usuario_setor
		where	nm_usuario_param	= nm_usuario_atual_p
		and	cd_setor_Atendimento	= cd_setor_atendimento_w;
	
		if	(qt_registro_w	= 0) then
			insert into usuario_setor
				(nm_usuario_param,
				cd_setor_atendimento,
				dt_atualizacao,
				nm_usuario)
			values	(nm_usuario_atual_p,
				cd_setor_atendimento_w,
				sysdate,
				nm_usuario_p);
			
		end if;
		end;
	end loop;
	close C03;
	
	open C04;
	loop
	fetch C04 into	
		cd_perfil_w;
	exit when C04%notfound;
		begin
		
		select	count(*)
		into	qt_registro_w
		from	usuario_perfil
		where	cd_perfil		= cd_perfil_w
		and	nm_usuario	= nm_usuario_atual_p;
		if	(qt_registro_w	= 0) then
			insert into usuario_perfil
				(nm_usuario,
				cd_perfil,
				dt_atualizacao,
				nm_usuario_atual)
			values	(nm_usuario_atual_p,
				cd_perfil_w,
				sysdate,
				nm_usuario_p);
		end if;
		end;
	end loop;
	close C04;
	
end if;

select 	max(cd_estabelecimento)
into 	cd_estabelecimento_principal_w
from 	cargo_estabelecimento
where 	cd_cargo			= cd_cargo_p
and  	ie_estabelecimento_principal	= 'S';

if	(cd_estabelecimento_principal_w is not null) then
	UPDATE	usuario
	SET	cd_estabelecimento	= cd_estabelecimento_principal_w
	where	nm_usuario		= nm_usuario_atual_p
	and	cd_pessoa_fisica	= cd_pessoa_fisica_p;
end if;

open c05;
loop
fetch c05 into
	cd_estabelecimento_w,
	cd_setor_padrao_w,
	nr_seq_perfil_w;
exit when c05%notfound;
	begin
	select	count(*)
	into	qt_registro_w
	from	usuario_estabelecimento
	where	nm_usuario_param	= nm_usuario_atual_p
	and	cd_estabelecimento	= cd_estabelecimento_w;
	
	if	(qt_registro_w = 0) then
		insert into usuario_estabelecimento
			(	nm_usuario_param,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_setor_padrao,
				nr_seq_perfil)
		values	(	nm_usuario_atual_p,
				cd_estabelecimento_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_setor_padrao_w,
				nr_seq_perfil_w);
	end if;
	end;
end loop;
close C05;


begin
select	ie_tipo_evolucao
into	ie_tipo_evolucao_w
from	cargo
where	cd_cargo		= cd_cargo_p;
exception
when others then
	ie_tipo_evolucao_w	:= '';
end;

update	usuario
set	ie_tipo_evolucao	= nvl(ie_tipo_evolucao_w,ie_tipo_evolucao)
where	nm_usuario		= nm_usuario_atual_p
and	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	cd_estabelecimento	= cd_estabelecimento_p;

commit;

end gerar_cargo_perfil_setor;
/