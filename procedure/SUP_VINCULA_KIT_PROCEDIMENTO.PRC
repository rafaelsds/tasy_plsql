create or replace
procedure sup_vincula_kit_procedimento(
			cd_kit_material_p	Number,
			cd_procedimento_p	Number,
			ie_origem_proc_p	Number,
			nm_usuario_p		Varchar2) is 

begin

update 	procedimento 
set 	cd_kit_material 	= cd_kit_material_p
where 	cd_procedimento 	= cd_procedimento_p
and   	ie_origem_proced 	= ie_origem_proc_p;

commit;

end sup_vincula_kit_procedimento;
/