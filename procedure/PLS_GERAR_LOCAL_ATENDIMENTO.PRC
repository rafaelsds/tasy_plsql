create or replace
procedure pls_gerar_local_atendimento(	ie_acao_p		Varchar2,
					nr_seq_local_p		Number,
					cd_estabelecimento_p	Number,
					nm_usuario_p		Varchar2,
					ds_local_p              Varchar2,
					cd_empresa_p           	Number,
					cd_municipio_ibge_p	Varchar2,
					ds_endereco_p           Varchar2,
					ds_bairro_p             Varchar2,
					cd_cep_p                Varchar2,
					nr_endereco_p           Number,
					sg_estado_p             Varchar2,
					ie_situacao_p		Varchar2,
					nr_telefone_p		Varchar2,
					ds_observacao_p		Varchar2) is 
begin

/*ie_acao_p
I - Insert
U - Update
*/

if	(ie_acao_p = 'I') then 
	insert into local_atendimento_medico(
		nr_sequencia, cd_estabelecimento, dt_atualizacao,
		nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,        
		ds_local, cd_empresa, cd_municipio_ibge,
		ds_endereco, ds_bairro, cd_cep,
		nr_endereco,sg_estado, ie_situacao,
		nr_telefone, ds_observacao)
	values(local_atendimento_medico_seq.nextval,cd_estabelecimento_p, sysdate,
		nm_usuario_p, sysdate, nm_usuario_p,        
		ds_local_p, cd_empresa_p, cd_municipio_ibge_p,
		ds_endereco_p, ds_bairro_p, cd_cep_p,
		nr_endereco_p,sg_estado_p, ie_situacao_p,
		nr_telefone_p, ds_observacao_p);
elsif	(ie_acao_p = 'U' and nr_seq_local_p is not null) then
	update	local_atendimento_medico
	set	dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p,
		ds_local = ds_local_p,
		cd_empresa = cd_empresa_p,
		cd_municipio_ibge = cd_municipio_ibge_p,
		ds_endereco = ds_endereco_p,
		ds_bairro = ds_bairro_p,
		cd_cep = cd_cep_p,
		nr_endereco = nr_endereco_p,
		sg_estado = sg_estado_p,
		ie_situacao = ie_situacao_p,
		nr_telefone = nr_telefone_p,
		ds_observacao =  ds_observacao_p
	where 	nr_sequencia = nr_seq_local_p;
	
end if;

commit;

end pls_gerar_local_atendimento;
/