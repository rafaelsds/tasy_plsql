create or replace
procedure PROJ_Gerar_Tit_pagar_RAT(
		cd_estabelecimento_p	Number,
		nr_seq_rat_p			Number,
		nm_usuario_p			varchar2) is


cd_estab_financeiro_w	number(10,0);
dt_emissao_w			Date;
dt_vencimento_w			Date;
dt_inicio_w				Date;
dt_final_w				Date;
vl_rat_w				Number(15,2);
cd_consultor_w			Varchar2(10);
nr_titulo_w				Number(10,0);

cd_tipo_taxa_juro_w		Number(15,0);
cd_tipo_taxa_multa_w	Number(15,0);
cd_moeda_w				Number(15,0);
ds_cliente_w			Varchar2(80);
ds_obs_w				Varchar2(255);

BEGIN

select	titulo_pagar_seq.nextval
into	nr_titulo_w
from	dual;

select	cd_executor,
	vl_pagar,
	dt_inicio,
	dt_final,
	substr(obter_desc_cliente(nr_seq_cliente,'N'),1,80)
into	cd_consultor_w,
	vl_rat_w,
	dt_inicio_w,
	dt_final_w,
	ds_cliente_w
from	proj_rat
where	nr_sequencia		= nr_seq_rat_p;

select	cd_tipo_taxa_juro,
	cd_tipo_taxa_multa,
	cd_moeda_padrao
into	cd_tipo_taxa_juro_w,
	cd_tipo_taxa_multa_w,
	cd_moeda_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_p;

select	nvl(cd_estab_financeiro, cd_estabelecimento)
into	cd_estab_financeiro_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

dt_emissao_w		:= PKG_DATE_UTILS.start_of(sysdate,'dd', 0);
dt_vencimento_w	:= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_inicio_w,1, 0), 'MONTH', 0) + 9;
if	(dt_vencimento_w < sysdate) then
	dt_vencimento_w	:= PKG_DATE_UTILS.start_of(sysdate,'dd', 0);
end if;
select	obter_proximo_dia_util(cd_estabelecimento_p, dt_vencimento_w)
into	dt_vencimento_w
from	dual;

ds_obs_w		:= ds_cliente_w || wheb_mensagem_pck.get_texto(305845) || dt_inicio_w || 
			wheb_mensagem_pck.get_texto(305847) || dt_final_w;

/* Ricardo 05/04/2006 - A pedido do Marcus, inclu� em todos os inserts da Titulo Pagar o campo NR_LOTE_TRANSF_TRIB como 0 (zero) */

insert into titulo_pagar(
	nr_titulo, cd_estabelecimento, dt_atualizacao, nm_usuario, 
	dt_emissao, dt_vencimento_original, dt_vencimento_atual, 
	vl_titulo, vl_saldo_titulo, vl_saldo_juros, vl_saldo_multa, 
	cd_moeda, tx_juros, tx_multa, cd_tipo_taxa_juro, cd_tipo_taxa_multa,
	tx_desc_antecipacao, ie_situacao, ie_origem_titulo,ie_tipo_titulo, 
	cd_pessoa_fisica, nr_documento, nr_parcelas, nr_total_parcelas, 
	dt_contabil, nr_lote_contabil, ds_observacao_titulo, ie_status_tributo,
	NR_LOTE_TRANSF_TRIB, cd_estab_financeiro, ie_status)
values(
	nr_titulo_w, cd_estabelecimento_p, sysdate, nm_usuario_p, 
	dt_emissao_w,	dt_vencimento_w, dt_vencimento_w, 
	vl_rat_w, vl_rat_w, 0, 0,
	cd_moeda_w, 0, 0, cd_tipo_taxa_juro_w, cd_tipo_taxa_multa_w, 
	0, 'A', '3', '9', 
	cd_consultor_w, nr_seq_rat_p, 1, 1, 
	dt_emissao_w, 0, ds_obs_w, 'N', 0, cd_estab_financeiro_w,'D');
ATUALIZAR_INCLUSAO_TIT_PAGAR(nr_titulo_w, nm_usuario_p);

update	proj_rat
set	nr_titulo_pagar	= nr_titulo_w
where	nr_sequencia		= nr_seq_rat_p;
commit;

END PROJ_Gerar_Tit_pagar_RAT;
/