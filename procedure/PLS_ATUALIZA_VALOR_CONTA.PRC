create or replace
procedure pls_atualiza_valor_conta 
			(	nr_seq_conta_p		Number,
				nm_usuario_p		Varchar2) is 
			
vl_procedimentos_w		Number(15,2)	:= 0;
vl_diarias_w			Number(15,2)	:= 0;
vl_pacotes_w			Number(15,2)	:= 0;
vl_taxas_w			Number(15,2)	:= 0;
vl_materiais_w			Number(15,2)	:= 0;
vl_medicamentos_w		Number(15,2)	:= 0;
vl_gases_w			Number(15,2)	:= 0;
vl_opm_w			Number(15,2)	:= 0;
vl_cobrado_w			Number(15,2)	:= 0;
vl_total_w			Number(15,2)	:= 0;
vl_glosa_w			Number(15,2)	:= 0;
vl_saldo_w			Number(15,2)	:= 0;
vl_cobrado_proc_w		Number(15,2)	:= 0;
vl_cobrado_mat_w		Number(15,2)	:= 0;
vl_total_proc_w			Number(15,2)	:= 0;
vl_total_mat_w			Number(15,2)	:= 0;
vl_proc_apres_w			Number(15,2)	:= 0;
vl_taxas_apres_w		Number(15,2)	:= 0;
vl_diarias_apres_w		Number(15,2)	:= 0;
vl_pacotes_apres_w		Number(15,2)	:= 0;
vl_gases_apres_w		Number(15,2)	:= 0;
vl_medic_apres_w		Number(15,2)	:= 0;
vl_mat_apres_w			Number(15,2)	:= 0;
vl_opm_apres_w			Number(15,2)	:= 0;
vl_glosa_proc_w			Number(15,2)	:= 0;
vl_glosa_taxa_w			Number(15,2)	:= 0;
vl_glosa_diaria_w		Number(15,2)	:= 0;
vl_glosa_pacote_w		Number(15,2)	:= 0;
vl_glosa_medic_w		Number(15,2)	:= 0;
vl_glosa_gases_w		Number(15,2)	:= 0;
vl_glosa_mat_w			Number(15,2)	:= 0;
vl_glosa_opm_w			Number(15,2)	:= 0;
vl_saldo_proc_w			Number(15,2)	:= 0;
vl_saldo_taxa_w			Number(15,2)	:= 0;
vl_saldo_diaria_w		Number(15,2)	:= 0;
vl_saldo_pacote_w		Number(15,2)	:= 0;
vl_saldo_medic_w		Number(15,2)	:= 0;
vl_saldo_gases_w		Number(15,2)	:= 0;
vl_saldo_mat_w			Number(15,2)	:= 0;
vl_saldo_opm_w			Number(15,2)	:= 0;
vl_proc_beneciciario_w		Number(15,2)	:= 0;
vl_mat_beneciciario_w		Number(15,2)	:= 0;
vl_total_beneciciario_w		Number(15,2)	:= 0;
/*ie_preco_plano_w		Varchar2(2); */
ie_calcula_preco_benef_w	Varchar2(1);
vl_adic_materiais_w		Number(15,2);
vl_adic_co_w			Number(15,2);
vl_adic_materiais_ww		Number(15,2);
vl_adic_procedimento_w		Number(15,2);
vl_co_ptu_w			Number(15,2)	:= 0;
vl_procedimento_ptu_w		Number(15,2)	:= 0;
vl_material_ptu_w		Number(15,2)	:= 0;
vl_material_ptu_ww		Number(15,2)	:= 0;
vl_liberado_ptu_w		Number(15,2)	:= 0;
qt_item_w			Number(10);
ie_tipo_protocolo_w		pls_protocolo_conta.ie_tipo_protocolo%type;
nr_seq_protocolo_w		pls_protocolo_conta.nr_sequencia%type;

begin

select	count(1)
into	qt_item_w
from	pls_conta_proc
where	nr_seq_conta	= nr_seq_conta_p
and	ie_tipo_despesa	= '1'
and	ie_status != 'D'
and	rownum <= 1;

if	(qt_item_w > 0)then
	select	nvl(sum(vl_liberado),0),
		nvl(sum(vl_procedimento_imp),0),
		nvl(sum(vl_glosa),0),
		nvl(sum(vl_saldo),0)
	into	vl_procedimentos_w,
		vl_proc_apres_w,
		vl_glosa_proc_w,
		vl_saldo_proc_w
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_tipo_despesa	= '1'
	and	ie_status 	not in  ('M','D');
else
	vl_procedimentos_w	:= 0;
	vl_proc_apres_w		:= 0;
	vl_glosa_proc_w		:= 0;
	vl_saldo_proc_w		:= 0;
end if;

select	count(1)
into	qt_item_w
from	pls_conta_proc
where	nr_seq_conta	= nr_seq_conta_p
and	ie_tipo_despesa	= '2'
and	ie_status 	not in  ('M','D')
and	rownum <= 1;

if	(qt_item_w > 0)then
	select	nvl(sum(vl_liberado),0),
		nvl(sum(vl_procedimento_imp),0),
		nvl(sum(vl_glosa),0),
		nvl(sum(vl_saldo),0)
	into	vl_taxas_w,
		vl_taxas_apres_w,
		vl_glosa_taxa_w,
		vl_saldo_taxa_w
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_tipo_despesa	= '2'
	and	ie_status 	not in  ('M','D');
else
	vl_taxas_w	 := 0;
	vl_taxas_apres_w := 0;
	vl_glosa_taxa_w	 := 0;
	vl_saldo_taxa_w	 := 0;
end if;

select	count(1)
into	qt_item_w
from	pls_conta_proc
where	nr_seq_conta	= nr_seq_conta_p
and	ie_tipo_despesa	= '3'
and	ie_status 	not in  ('M','D')
and	rownum <= 1;

if	(qt_item_w > 0)then
	select	nvl(sum(vl_liberado),0),
		nvl(sum(vl_procedimento_imp),0),
		nvl(sum(vl_glosa),0),
		nvl(sum(vl_saldo),0)
	into	vl_diarias_w,
		vl_diarias_apres_w,
		vl_glosa_diaria_w,
		vl_saldo_diaria_w
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_tipo_despesa	= '3'
	and	ie_status 	not in  ('M','D');
else
	vl_diarias_w		:= 0;
	vl_diarias_apres_w	:= 0;
	vl_glosa_diaria_w	:= 0;
	vl_saldo_diaria_w	:= 0;
end if;

select	count(1)
into	qt_item_w
from	pls_conta_proc
where	nr_seq_conta	= nr_seq_conta_p
and	ie_tipo_despesa	= '4'
and	ie_status 	not in  ('M','D')
and	rownum <= 1;

if	(qt_item_w > 0)then

	select	nvl(sum(vl_liberado),0),
		nvl(sum(vl_procedimento_imp),0),
		nvl(sum(vl_glosa),0),
		nvl(sum(vl_saldo),0)
	into	vl_pacotes_w,
		vl_pacotes_apres_w,
		vl_glosa_pacote_w,
		vl_saldo_pacote_w
	from	pls_conta_proc
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_tipo_despesa	= '4'
	and	ie_status 	not in  ('M','D');
else
	vl_pacotes_w		:= 0;
	vl_pacotes_apres_w	:= 0;
	vl_glosa_pacote_w	:= 0;
	vl_saldo_pacote_w	:= 0;
end if;

select	count(1)
into	qt_item_w
from	pls_conta_mat
where	nr_seq_conta	= nr_seq_conta_p
and	ie_tipo_despesa	= '1'
and	ie_status 	not in  ('M','D')
and	rownum <= 1;

if	(qt_item_w > 0)then
	select	nvl(sum(vl_liberado),0),
		nvl(sum(vl_material_imp),0),
		nvl(sum(vl_glosa),0),
		nvl(sum(vl_saldo),0)
	into	vl_gases_w,
		vl_gases_apres_w,
		vl_glosa_gases_w,
		vl_saldo_gases_w
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_tipo_despesa	= '1'
	and	ie_status 	not in  ('M','D');
else
	vl_gases_w		:= 0;
	vl_gases_apres_w	:= 0;
	vl_glosa_gases_w	:= 0;
	vl_saldo_gases_w	:= 0;
end if;

select	count(1)
into	qt_item_w
from	pls_conta_mat
where	nr_seq_conta	= nr_seq_conta_p
and	ie_tipo_despesa	= '2'
and	ie_status 	not in  ('M','D')
and	rownum <= 1;

if	(qt_item_w > 0)then
	select	nvl(sum(vl_liberado),0),
		nvl(sum(vl_material_imp),0),
		nvl(sum(vl_glosa),0),
		nvl(sum(vl_saldo),0)
	into	vl_medicamentos_w,
		vl_medic_apres_w,
		vl_glosa_medic_w,
		vl_saldo_medic_w
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_tipo_despesa	= '2'
	and	ie_status 	not in  ('M','D');
else
	vl_medicamentos_w	:= 0;
	vl_medic_apres_w	:= 0;
	vl_glosa_medic_w	:= 0;
	vl_saldo_medic_w	:= 0;
end if;

select	count(1)
into	qt_item_w
from	pls_conta_mat
where	nr_seq_conta	= nr_seq_conta_p
and	ie_tipo_despesa	= '3'
and	ie_status 	not in  ('M','D')
and	rownum <= 1;

if	(qt_item_w > 0)then
	select	nvl(sum(vl_liberado),0),
		nvl(sum(vl_material_imp),0),
		nvl(sum(vl_glosa),0),
		nvl(sum(vl_saldo),0)
	into	vl_materiais_w,
		vl_mat_apres_w,
		vl_glosa_mat_w,
		vl_saldo_mat_w
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_tipo_despesa	= '3'
	and	ie_status 	not in  ('M','D');
else
	vl_materiais_w	:= 0;
	vl_mat_apres_w	:= 0;
	vl_glosa_mat_w	:= 0;
	vl_saldo_mat_w	:= 0;
end if;

select	count(1)
into	qt_item_w
from	pls_conta_mat
where	nr_seq_conta	= nr_seq_conta_p
and	ie_tipo_despesa	= '7'
and	ie_status 	not in  ('M','D')
and	rownum <= 1;

if	(qt_item_w > 0)then
	select	nvl(sum(vl_liberado),0),
		nvl(sum(vl_material_imp),0),
		nvl(sum(vl_glosa),0),
		nvl(sum(vl_saldo),0)
	into	vl_opm_w,
		vl_opm_apres_w,
		vl_glosa_opm_w,
		vl_saldo_opm_w
	from	pls_conta_mat
	where	nr_seq_conta	= nr_seq_conta_p
	and	ie_tipo_despesa	= '7'
	and	ie_status 	not in  ('M','D');
else
	vl_opm_w	:= 0;
	vl_opm_apres_w	:= 0;
	vl_glosa_opm_w	:= 0;
	vl_saldo_opm_w	:= 0;
end if;

vl_cobrado_w	:=	nvl(vl_proc_apres_w,0) + nvl(vl_taxas_apres_w,0) + nvl(vl_diarias_apres_w,0) +
			nvl(vl_pacotes_apres_w,0) + nvl(vl_gases_apres_w,0) + nvl(vl_medic_apres_w,0) +
			nvl(vl_mat_apres_w,0) + nvl(vl_opm_apres_w,0);

vl_glosa_w	:=	nvl(vl_glosa_proc_w,0) + nvl(vl_glosa_taxa_w,0) + nvl(vl_glosa_diaria_w,0) +
			nvl(vl_glosa_pacote_w,0) + nvl(vl_glosa_gases_w,0) + nvl(vl_glosa_medic_w,0) +
			nvl(vl_glosa_mat_w,0) + nvl(vl_glosa_opm_w,0);

vl_saldo_w	:=	nvl(vl_saldo_proc_w,0) + nvl(vl_saldo_taxa_w,0) + nvl(vl_saldo_diaria_w,0) +
			nvl(vl_saldo_pacote_w,0) + nvl(vl_saldo_gases_w,0) + nvl(vl_saldo_medic_w,0) +
			nvl(vl_saldo_mat_w,0) + nvl(vl_saldo_opm_w,0);
			
/* Obter os valores a serem pagos pelo beneficiario nos casos de produto p�s estabelecido por custo operacional */

select	nvl(sum(vl_beneficiario),0)
into	vl_proc_beneciciario_w
from	pls_conta_proc
where	nr_seq_conta	= nr_seq_conta_p
and	ie_status 	!= 'D';

select	nvl(sum(vl_beneficiario),0)
into	vl_mat_beneciciario_w
from	pls_conta_mat
where	nr_seq_conta	= nr_seq_conta_p
and	ie_status 	!= 'D';

select	nr_seq_protocolo
into	nr_seq_protocolo_w
from 	pls_conta
where	nr_sequencia = nr_seq_conta_p;

select	ie_tipo_protocolo
into	ie_tipo_protocolo_w
from	pls_protocolo_conta
where 	nr_sequencia = nr_seq_protocolo_w;

/* Quando for protocolo de Reembolso n�o pode pegar o vl_original pois pode se alterar o valor */
if 	(ie_tipo_protocolo_w <> 'R')then

	select	nvl(sum(vl_lib_original),0)
	into	vl_total_w
	from	pls_conta_medica_resumo
	where	nr_seq_conta = nr_seq_conta_p
	and	((ie_tipo_item <> 'I') or
		(ie_tipo_item is null))
	and	ie_situacao 	= 'A';
	
end if;
	
if	(nvl(vl_total_w,0) = 0) then
	vl_total_w	:=	nvl(vl_procedimentos_w,0) + nvl(vl_taxas_w,0) + nvl(vl_diarias_w,0) +
				nvl(vl_pacotes_w,0) + nvl(vl_gases_w,0) + nvl(vl_medicamentos_w,0) +
				nvl(vl_materiais_w,0) + nvl(vl_opm_w,0);
end if;
vl_total_beneciciario_w	:= vl_proc_beneciciario_w + vl_mat_beneciciario_w;

select	nvl(sum(vl_adic_procedimento),0),
	nvl(sum(vl_adic_co),0),
	nvl(sum(vl_adic_materiais),0),
	nvl(sum(vl_co_ptu),0),
	nvl(sum(vl_procedimento_ptu),0),
	nvl(sum(vl_material_ptu),0)
into	vl_adic_procedimento_w,
	vl_adic_co_w,
	vl_adic_materiais_w,
	vl_co_ptu_w,
	vl_procedimento_ptu_w,
	vl_material_ptu_w
from	pls_conta_proc
where	nr_seq_conta	= nr_seq_conta_p
and	ie_status 	not in  ('M','D');

select	nvl(sum(vl_adic_material),0),
	nvl(sum(vl_material_ptu),0)
into	vl_adic_materiais_ww,
	vl_material_ptu_ww
from	pls_conta_mat
where	nr_seq_conta	= nr_seq_conta_p
and	ie_status 	not in  ('M','D');

vl_adic_procedimento_w	:= (vl_adic_procedimento_w + vl_adic_materiais_ww);
vl_liberado_ptu_w	:= vl_material_ptu_ww + vl_co_ptu_w + vl_procedimento_ptu_w + vl_material_ptu_w;

update	pls_conta
set	vl_procedimentos	= nvl(vl_procedimentos_w,0),
	vl_diarias		= nvl(vl_diarias_w,0),
	vl_pacotes		= nvl(vl_pacotes_w,0),
	vl_taxas		= nvl(vl_taxas_w,0),
	vl_materiais		= nvl(vl_materiais_w,0),
	vl_medicamentos		= nvl(vl_medicamentos_w,0),
	vl_gases		= nvl(vl_gases_w,0),
	vl_opm			= nvl(vl_opm_w,0),
	vl_cobrado		= nvl(vl_cobrado_w,0),
	vl_total		= nvl(vl_total_w,0),
	vl_glosa		= nvl(vl_glosa_w,0),
	vl_saldo		= nvl(vl_saldo_w,0),
	vl_total_beneficiario	= nvl(vl_total_beneciciario_w,0),
	vl_adic_procedimento	= nvl(vl_adic_procedimento_w,0),
	vl_adic_co		= nvl(vl_adic_co_w,0),
	vl_adic_materiais	= nvl(vl_adic_materiais_w,0),
	vl_liberado_ptu		= nvl(vl_liberado_ptu_w,0)
where	nr_sequencia		= nr_seq_conta_p;

end pls_atualiza_valor_conta;
/