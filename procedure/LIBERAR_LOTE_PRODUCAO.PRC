create or replace
procedure liberar_lote_producao(	nr_lote_producao_p	number,
					nm_usuario_p		Varchar2) is 

begin

if	(nr_lote_producao_p > 0) then
	update	lote_producao
	set	dt_confirmacao		=	sysdate,
		nm_usuario_confirmacao	=	nm_usuario_p
	where	nr_lote_producao	=	nr_lote_producao_p;
	
	gerar_lote_producao_individual(nr_lote_producao_p, nm_usuario_p);
end if;

commit;

end liberar_lote_producao;
/