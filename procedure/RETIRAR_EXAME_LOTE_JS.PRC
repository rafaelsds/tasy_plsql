create or replace
procedure retirar_exame_lote_js( nr_prescricao_p		number,
			    nr_sequencia_p		number) is 
          
nr_seq_w number(15);

begin

if	(nr_prescricao_p is not null)and
	(nr_sequencia_p is not null)then
  
  begin
  
  delete from lab_lote_item
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_seq_prescr = nr_sequencia_p;
	
	update 	prescr_procedimento 
	set 	nr_seq_lote_externo = null
	where 	nr_prescricao = nr_prescricao_p
	and 	nr_sequencia = nr_sequencia_p;
	
	end;
end if;

commit;

end retirar_exame_lote_js;
/