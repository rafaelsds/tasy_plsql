create or replace
procedure pls_exportar_xml_cta_banco ( nr_seq_xml_arquivo_p pls_xml_arquivo.nr_sequencia%type ) is 

nm_arquivo_w 		pls_xml_arquivo.nm_arquivo%type;
ds_local_w		varchar2(255) := null;
ds_local_windows_w	varchar2(255) := null;
ds_erro_w		varchar2(255);
arq_texto_w		utl_file.file_type;
ds_conteudo_w		clob;
file_exist_w		boolean;
size_w			number;
block_size_w		number;
qt_tamanho_w		pls_integer;
qt_posicao_w		pls_integer;

begin

select	nm_arquivo
into	nm_arquivo_w
from	pls_xml_arquivo
where	nr_sequencia = nr_seq_xml_arquivo_p;

pls_convert_long_to_clob(	'PLS_XML_ARQUIVO',
				'DS_XML',
				'WHERE NR_SEQUENCIA = :NR_SEQUENCIA',
				'NR_SEQUENCIA='||nr_seq_xml_arquivo_p,
				ds_conteudo_w);

begin
obter_evento_utl_file(19, null, ds_local_w, ds_erro_w);
exception
when others then
	ds_local_w := null;
	ds_local_windows_w := '';
end;

utl_file.fgetattr(ds_local_w,nm_arquivo_w,file_exist_w,size_w,block_size_w);

if	(file_exist_w) then 
	utl_file.fremove(ds_local_w,nm_arquivo_w);
end if;

if	(ds_local_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(387514);
else
	begin
	arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W');
	exception
	when others then
		ds_erro_w := obter_erro_utl_open(sqlcode);
		wheb_mensagem_pck.exibir_mensagem_abort('Local: '||ds_local_w||' Nome arquivo: '||nm_arquivo_w||' '||ds_erro_w);
	end;
	
	qt_tamanho_w	:= dbms_lob.getlength(ds_conteudo_w);
	qt_posicao_w	:= 1;
	
	while (qt_posicao_w < qt_tamanho_w) loop
		utl_file.put_line(arq_texto_w,dbms_lob.substr(ds_conteudo_w, 32767, qt_posicao_w));
		utl_file.fflush(arq_texto_w);
		qt_posicao_w := qt_posicao_w + 32767;
	end loop;

	utl_file.fclose(arq_texto_w);
end if;

end pls_exportar_xml_cta_banco;
/