create or replace
procedure qua_alterar_resp_pendencia(	nm_usuario_p		varchar2,
				nr_sequencia_p		number,
				cd_pessoa_p		varchar2) is

begin
update	proj_ata_pendencia
set	cd_pessoa_resp	= cd_pessoa_p,
	nm_usuario	= nm_usuario_p,
	nm_pessoa_resp	= substr(obter_nome_pf(cd_pessoa_p),1,255)
where	nr_sequencia	= nr_sequencia_p;

commit;

end qua_alterar_resp_pendencia;
/