create or replace
procedure pls_gerar_sip_conta_assist
			(	nr_seq_conta_p		Number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number) is

nr_seq_segurado_w		Number(10);
nr_seq_protocolo_w		Number(10);
nr_seq_prestador_exec_w		Number(10);
qt_beneficiario_w		Number(10);
ie_sexo_w			Varchar2(10);
cd_doenca_cid_w			Varchar2(10);
sg_uf_sip_w			pls_prestador.sg_uf_sip%type	:= 'NC';
ie_internado_w			Varchar2(1);
ie_segmentacao_w		Varchar2(10);
nr_seq_proc_w			Number(10);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10);
nr_seq_item_assistencial_w	Number(10);
ie_tipo_guia_w			Varchar2(2);
nr_seq_cbo_saude_w		Number(10);
nr_seq_tipo_atendimento_w	Number(10);
nr_seq_clinica_w		Number(10);
ie_regime_internacao_w		Varchar2(10);
ie_tipo_contratacao_w		Varchar2(10);
cd_item_sip_w			Varchar2(10);
cd_classificacao_w		Varchar2(20);
cd_classificacao_ww		Varchar2(20);
dt_mes_competencia_w		Date;
qt_idade_w			Number(10);
qt_idade_meses_w		Number(10);
qt_idade_dia_w			Number(10);
nr_seq_mat_w			Number(10);
nr_seq_material_w		Number(10);
ie_parto_normal_w		Varchar2(1);
qt_nasc_vivos_w			Number(10);
qt_nasc_vivos_premat_w		Number(10);
ie_nasc_vivos_w			Varchar2(1)	:= 'S';
qt_evento_w			Number(12,4);
qt_evento_ww			Number(12,4);
nr_seq_conta_w			Number(10);
vl_despesa_w			Number(15,2);
nr_seq_item_assis_w		number(10);
qt__w				Number(10);
cd_grupo_proc_w			Number(15);
cd_especialidade_w		Number(15);
cd_area_procedimento_w		Number(15);
ie_materiais_w			Varchar2(1)	:= 'N';
nr_seq_prot_referencia_w	Number(10);
qt_ptocedimento_regra_w		Number(10);
nr_seq_contrato_w		Number(10);
nr_seq_congenere_contrato_w	Number(10);

cd_procedimento_ww		Number(15);
ie_origem_proced_ww		Number(10);
cd_area_procedimento_ww		Number(15);
cd_especialidade_ww		Number(15);
cd_grupo_proc_ww		Number(15);
qt_sip_mov_item_assistencial_w	Number(10);
nr_sequencia_w			Number(10);
nr_seq_mov_item_sip_w		number(10);
ie_nascido_vivo_w		Varchar2(10);
qt_idade_min_w			number(3);
qt_idade_max_w			number(3);
ie_unid_tempo_idade_w		Varchar2(10);
ie_idade_w			Varchar2(10);
cd_categoria_cid_w		Varchar2(10);
cd_guia_referencia_w		Varchar2(20);
nr_seq_item_internacao_w	Number(10);
nr_seq_item_internacao_EX_w	Number(10);
nr_seq_item_internacao_F_w	Number(10);
nr_seq_item_internacao_EY_w	Number(10);
ie_acho_regra_w			Varchar2(10)	:= 'N';
cd_cbo_saude_w			Varchar2(15);
dt_ocorrencia_w			Date;
dt_ocorrencia_item_w		Date;
qt_procedimento_resumo_w	Number(10)	:= 0;
nr_seq_lote_sip_w		Number(10);
nr_seq_lote_w			Number(10);
nr_seq_congenere_w		Number(10);
ie_tipo_protocolo_w		Varchar2(3);
ie_considerar_conta_w		Varchar2(10)	:= 'N';
ie_considerar_conta_ww		Varchar2(10)	:= 'N';
ie_considerar_guia_w		Varchar2(10)	:= 'N';
ie_considerar_guia_ww		Varchar2(10)	:= 'N';
nr_seq_regra_w			Number(10);
nr_seq_regra_ww			Number(10);
ie_verificar_atualiza_w		Varchar2(10);
ie_verificar_atualiza_ww	Varchar2(10);
cd_pessoa_fisica_w		Number(15);
dt_nascimento_w			Date;
Cursor C01 is
	select	a.nr_sequencia,
		a.cd_procedimento,
		a.ie_origem_proced,
		nvl(a.qt_procedimento_imp,0),
		nvl(a.vl_liberado,0),
		a.dt_procedimento
	from	pls_conta_proc	a
	where	a.nr_seq_conta	= nr_seq_conta_p;

Cursor C02 is
	select	a.nr_sequencia,
		a.nr_seq_material,
		nvl(a.qt_material_imp,0),
		nvl(a.vl_liberado,0),
		a.dt_atendimento
	from	pls_conta_mat	a
	where	a.nr_seq_conta	= nr_seq_conta_p;

/*Regras de procedimento*/
Cursor C03 is
	select	a.nr_seq_item_assist,
		nvl(a.ie_nascido_vivo,'N'),
		b.cd_item,
		b.cd_classificacao,
		ie_nascido_vivo,
		qt_idade_inicial,
		qt_idade_final,
		nvl(ie_unid_tempo_idade,'X'),
		ie_considerar_conta,
		ie_considerar_guia_ref,
		a.nr_sequencia
	from	sip_item_assist_regra	a,
		sip_item_assistencial	b
	where	a.nr_seq_item_assist	= b.nr_sequencia
	and	a.ie_situacao	= 'A'
	--and	(nvl(ie_parto_normal,'N')	= ie_parto_normal_p) OS - 305282
	and	((a.cd_procedimento = cd_procedimento_w) or (a.cd_procedimento is null))
	and	((a.ie_origem_proced = ie_origem_proced_w) or (ie_origem_proced is null))
	and	((a.cd_grupo_proc = cd_grupo_proc_w) or (a.cd_grupo_proc is null))
	and	((a.cd_especialidade = cd_especialidade_w) or (cd_especialidade	is null))
	and	((a.cd_area_procedimento = cd_area_procedimento_w) or (cd_area_procedimento is null))
	and	((a.nr_seq_tipo_atendimento = nr_seq_tipo_atendimento_w) or (nr_seq_tipo_atendimento is null))
	and	((a.nr_seq_clinica = nr_seq_clinica_w) or (nr_seq_clinica is null))
	and	((a.ie_regime_internacao = ie_regime_internacao_w) or (ie_regime_internacao is null))
	--and	((a.nr_seq_cbo_saude = nr_seq_cbo_saude_w) or (nr_seq_cbo_saude is null))
	and	((a.ie_tipo_guia = ie_tipo_guia_w) or (ie_tipo_guia is null))
	and	((a.cd_doenca_cid = cd_doenca_cid_w) or (cd_doenca_cid is null))
	and	((ie_sexo	= ie_sexo_w)	or (ie_sexo is null))
	and	((cd_categoria_cid = cd_categoria_cid_w) or (cd_categoria_cid is null))
	and	((substr(obter_codigo_cbo_saude(a.nr_seq_cbo_saude),1,15) = cd_cbo_saude_w) or (nr_seq_cbo_saude is null))
	and	((a.nr_seq_prestador_exec is null) or (a.nr_seq_prestador_exec = nr_seq_prestador_exec_w))
	order by
		nvl(a.cd_procedimento,0) desc,
		nvl(a.cd_grupo_proc,0) desc,
		nvl(a.cd_especialidade,0) desc,
		nvl(a.cd_area_procedimento,0) desc,
		nvl(qt_idade_inicial,0) desc,
		nvl(qt_idade_final,0) desc,
		nvl(ie_unid_tempo_idade,0) desc,
		nvl(a.ie_regime_internacao,'A') desc,
		nvl(cd_categoria_cid,'A') desc,
		nvl(a.nr_seq_tipo_atendimento,0) desc,
		nvl(a.cd_doenca_cid,0) desc,
		nvl(a.nr_seq_clinica,0) desc,
		nvl(a.nr_seq_cbo_saude,0) desc,
		nvl(a.nr_seq_prestador_exec,0) desc;

/*Regras de Material*/
Cursor C06 is
	select	a.nr_seq_item_assist,
		nvl(a.ie_nascido_vivo,'N'),
		a.cd_procedimento,
		a.ie_origem_proced,
		a.cd_area_procedimento,
		a.cd_especialidade,
		a.cd_grupo_proc,
		b.cd_item,
		b.cd_classificacao,
		ie_nascido_vivo,
		qt_idade_inicial,
		qt_idade_final,
		nvl(ie_unid_tempo_idade,'X')
	from	sip_item_assist_regra	a,
		sip_item_assistencial	b
	where	a.nr_seq_item_assist	= b.nr_sequencia
	and	a.ie_situacao	= 'A'
	and	((a.nr_seq_tipo_atendimento = nr_seq_tipo_atendimento_w) or (nr_seq_tipo_atendimento is null))
	and	((a.nr_seq_clinica = nr_seq_clinica_w) or (nr_seq_clinica is null))
	and	((a.ie_regime_internacao = ie_regime_internacao_w) or (ie_regime_internacao is null))
	--and	((a.nr_seq_cbo_saude = nr_seq_cbo_saude_w) or (nr_seq_cbo_saude is null))
	and	((a.ie_tipo_guia = ie_tipo_guia_w) or (ie_tipo_guia is null))
	and	((a.cd_doenca_cid = cd_doenca_cid_w) or (cd_doenca_cid is null))
	and	(nvl(a.ie_materiais,'N')	= ie_materiais_w)
	and	((ie_sexo	= ie_sexo_w)	or (ie_sexo is null))
	and	((cd_categoria_cid = cd_categoria_cid_w) or (cd_categoria_cid is null))
	and	((substr(obter_codigo_cbo_saude(a.nr_seq_cbo_saude),1,15) = cd_cbo_saude_w) or (nr_seq_cbo_saude is null))
	and	((a.nr_seq_prestador_exec is null) or (a.nr_seq_prestador_exec = nr_seq_prestador_exec_w))
	order by
		nvl(a.cd_procedimento,0) desc,
		nvl(a.cd_grupo_proc,0) desc,
		nvl(a.cd_especialidade,0) desc,
		nvl(a.cd_area_procedimento,0) desc,
		nvl(qt_idade_inicial,0) desc,
		nvl(qt_idade_final,0) desc,
		nvl(ie_unid_tempo_idade,0) desc,
		nvl(a.ie_regime_internacao,'A') desc,
		nvl(cd_categoria_cid,'A') desc,
		nvl(a.nr_seq_tipo_atendimento,0) desc,
		nvl(a.cd_doenca_cid,0) desc,
		nvl(a.nr_seq_clinica,0) desc,
		nvl(a.nr_seq_cbo_saude,0) desc,
		nvl(a.nr_seq_prestador_exec,0) desc;
	
Cursor C04 is
	select	nr_seq_item_sip,
		cd_item_sip,
		cd_classificacao_sip,
		ie_tipo_contratacao,
		ie_segmentacao,
		sg_uf,
		dt_item_sip,
		nvl(a.dt_ocorrencia,b.dt_emissao)
	from	sip_mov_item_assistencial	a,
		pls_conta	b
	where	a.nr_seq_conta		= b.nr_sequencia
	and	b.nr_seq_segurado	= nr_seq_segurado_w
	and	a.cd_procedimento	= cd_procedimento_w
	and	a.ie_origem_proced	= ie_origem_proced_w
	and	b.nr_seq_protocolo	= nr_seq_prot_referencia_w;

Cursor C05 is
	select	nr_seq_item_sip,
		cd_item_sip,
		cd_classificacao_sip,
		ie_tipo_contratacao,
		ie_segmentacao,
		sg_uf,
		dt_item_sip,
		nvl(a.dt_ocorrencia,b.dt_emissao)
	from	sip_mov_item_assistencial	a,
		pls_conta	b
	where	a.nr_seq_conta		= b.nr_sequencia
	and	b.nr_seq_segurado	= nr_seq_segurado_w
	and	a.nr_seq_material	= nr_seq_material_w
	and	b.nr_seq_protocolo	= nr_seq_prot_referencia_w;

Cursor C07 is
	select	cd_classificacao_sip
	from	sip_mov_item_assistencial
	where	nr_seq_mat		= nr_seq_mat_w;
	
Cursor C08 is
	select	cd_classificacao_sip
	from	sip_mov_item_assistencial
	where	nr_seq_proc		= nr_seq_proc_w;
	
Cursor C09 is
	select	a.nr_sequencia,
		a.cd_classificacao_sip
	from	sip_mov_item_assistencial	a,
		pls_conta			b
	where	a.nr_seq_conta		= b.nr_sequencia
	and	((b.cd_guia_referencia	= cd_guia_referencia_w)
	or	(b.cd_guia_referencia	is null)
	and 	(b.cd_guia		= cd_guia_referencia_w))
	and	b.ie_tipo_guia		<> 6
	and	a.nr_sequencia	in 	(select  x.nr_sequencia
					from    sip_mov_item_assistencial	x,
						pls_conta		y
					where   y.nr_sequencia		= x.nr_seq_conta
					and	((y.cd_guia_referencia	= cd_guia_referencia_w)
					or	(y.cd_guia_referencia	is null)
					and 	(y.cd_guia		= cd_guia_referencia_w))
					and	((substr(x.cd_classificacao_sip,1,1)	= 'F'
					and	substr(x.cd_classificacao_sip,1,1)	= substr(a.cd_classificacao_sip,1,1))
					or	substr(x.cd_classificacao_sip,1,2)	= substr(a.cd_classificacao_sip,1,2))
					and	x.cd_classificacao_sip			<> 'G'
					and	y.ie_tipo_guia		<> 6
					and	x.cd_procedimento is not null)
	order by length(a.cd_classificacao_sip),
		 dt_item_sip desc;

Cursor C10 is	
	select	nr_sequencia
	from	pls_conta
	where	((cd_guia_referencia	= cd_guia_referencia_w)
	or	(nr_sequencia 		= nr_seq_conta_p));

begin
/* Buscar dados da conta do beneficiários da OPS e que sejam contas principais (guia referência nula ou igual a guia principal*/
begin 
select	a.nr_seq_segurado,
	a.nr_seq_protocolo,
	a.nr_seq_prestador_exec,
	a.ie_tipo_guia,
	a.nr_seq_cbo_saude,
	a.nr_seq_tipo_atendimento,
	a.nr_seq_clinica,
	a.ie_regime_internacao,
	b.ie_tipo_contratacao,
	b.ie_segmentacao,
	a.nr_sequencia,
	nvl(a.ie_parto_normal,'N'),
	nvl(a.qt_nasc_vivos_termo,0),
	nvl(qt_nasc_vivos_prematuros,0),
	nvl(cd_guia_referencia,cd_guia),
	nr_seq_contrato
into	nr_seq_segurado_w,
	nr_seq_protocolo_w,
	nr_seq_prestador_exec_w,
	ie_tipo_guia_w,
	nr_seq_cbo_saude_w,
	nr_seq_tipo_atendimento_w,
	nr_seq_clinica_w,
	ie_regime_internacao_w,
	ie_tipo_contratacao_w,
	ie_segmentacao_w,
	nr_seq_conta_w,
	ie_parto_normal_w,
	qt_nasc_vivos_w,
	qt_nasc_vivos_premat_w,
	cd_guia_referencia_w,
	nr_seq_contrato_w
from	pls_segurado	c,
	pls_plano	b,
	pls_conta	a
where	a.nr_seq_segurado	= c.nr_sequencia 
and	c.nr_seq_plano		= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_conta_p 
and	((nvl(a.ie_tipo_segurado,'B')	= 'B')
or	(nvl(a.ie_tipo_segurado,'B')	= 'R'));
exception
when others then
	nr_seq_segurado_w	:= null;
end;

select	max(nr_seq_congenere)
into	nr_seq_congenere_contrato_w
from	pls_contrato 
where	nr_sequencia	= nr_seq_contrato_w;

if	(nvl(nr_seq_segurado_w,0) <> 0) and
	(nr_seq_congenere_contrato_w is null) then 
	if	(nr_seq_cbo_saude_w is not null) then
		cd_cbo_saude_w	:= substr(obter_codigo_cbo_saude(nr_seq_cbo_saude_w),1,15);
	end if;
	/* Obter data do protocolo */ 
	begin
	select	dt_mes_competencia,
		nr_seq_prot_referencia,
		nr_seq_congenere,
		ie_tipo_protocolo
	into	dt_mes_competencia_w,
		nr_seq_prot_referencia_w,
		nr_seq_congenere_w,
		ie_tipo_protocolo_w
	from	pls_protocolo_conta
	where	nr_sequencia	= nr_seq_protocolo_w; 
	exception 
	when others then 
		dt_mes_competencia_w		:= null;
		nr_seq_prot_referencia_w	:= null;
	end;
	
	/* William - OS 397802 - Performance, count(1), IN*/
	/* Obter a quantidade de beneficiário */ 
	begin
	select	qt_benef_sip
	into	qt_beneficiario_w
	from	w_sip_inform_aux
	where 	nm_usuario = nm_usuario_p;
	exception
	when others then
	qt_beneficiario_w:=0;	
	end;
	
	
	
	if	(nvl(qt_beneficiario_w,0)=0) then
		select	count(1)
		into	qt_beneficiario_w 
		from(	select	1
			from	pls_segurado
			where	dt_liberacao is not null
			and	dt_rescisao is null
			and	ie_tipo_segurado = 'R'
			and	cd_estabelecimento = cd_estabelecimento_p
			union all
			select	1
			from	pls_segurado
			where	dt_liberacao is not null
			and	dt_rescisao is null
			and	ie_tipo_segurado = 'B'
			and	cd_estabelecimento = cd_estabelecimento_p);
	end if;
	
	if	(qt_beneficiario_w	> 49999) then
		/* Obter a UF do prestador executor */
		begin
		select	sg_uf_sip
		into	sg_uf_sip_w
		from	pls_prestador
		where	nr_sequencia	= nr_seq_prestador_exec_w;
		exception
		when others then
			sg_uf_sip_w	:= 'NC';
		end;
		if 	(sg_uf_sip_w is null) then
			sg_uf_sip_w	:= 'NC';
		end if;
		if (nvl(ie_tipo_protocolo_w,'X') = 'I') then
			begin
			select	a.sg_estado
			into	sg_uf_sip_w
			from	pls_congenere		b,
				pls_cooperativa_area	a
			where	b.nr_sequencia	= a.nr_seq_cooperativa
			and	b.nr_sequencia  = nr_seq_congenere_w;
			exception
			when others then
			sg_uf_sip_w	:= 'NC';	
			end;
			if 	(sg_uf_sip_w is null) then
				sg_uf_sip_w	:= 'NC';
			end if;
		end if;
	
	end if; 

	/*qt_idade_w	:= pls_obter_idade_segurado(nr_seq_segurado_w, sysdate, 'A');*/
	begin
	select	ie_sexo,
		a.cd_pessoa_fisica
	into	ie_sexo_w,
		cd_pessoa_fisica_w
	from	pls_segurado	a,
		pessoa_fisica	b
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nr_sequencia		= nr_seq_segurado_w;
	exception 
	when others then 
		ie_sexo_w := '';
		cd_pessoa_fisica_w := 0;
	end; 
	
	if 	(ie_sexo_w is null) then
		ie_sexo_w := 'I';
	end if;
	
	if 	(nvl(cd_pessoa_fisica_w,0) > 0) then
		
		select 	dt_nascimento
		into	dt_nascimento_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = to_char(cd_pessoa_fisica_w);

		qt_idade_w	:= substr(obter_idade(dt_nascimento_w, sysdate, 'A'),1,10);
		qt_idade_meses_w:= substr(obter_idade(dt_nascimento_w, sysdate, 'M'),1,10);
		qt_idade_dia_w	:= substr(obter_idade(dt_nascimento_w, sysdate, 'DIA'),1,10);
	end if;
		
	/* Obter o CID principal da conta */ 
	select	max(cd_doenca)
	into	cd_doenca_cid_w 
	from	pls_diagnostico_conta
	where	nr_seq_conta		= nr_seq_conta_p
	and	ie_classificacao	= 'P'; 
	
	/*Buscar catergoria CID*/
	
	select	max(cd_categoria_cid)
	into	cd_categoria_cid_w
	from 	cid_doenca
	where 	cd_doenca_cid = cd_doenca_cid_w;

	ie_internado_w	:= pls_obter_se_internado(nr_seq_conta_p, '');
	if	((ie_segmentacao_w in ('1','8')) or
		((ie_segmentacao_w in ('5','6','7','11','12')) and (ie_internado_w = 'N'))) then
		ie_segmentacao_w	:= 1;
	/*	ERRO ANS: Erro ao processar elemento segmentacao. Trimestre de reconhecimento 01/01/2010, forma de contratação individual familiar.
	Operadoras com cobertura hospitalar E obstetrica deverão enviar as informações sobre os itens hospitalares no quadro hospitalarObstetricia. 
	Felipe - 29/10/2010 - Coloquei para receber 3 nesse caso também para ser possível enviar o arquivo para o Itamed 
	*/ 
	elsif	((ie_segmentacao_w in ('3','10')) or
		((ie_segmentacao_w in ('7','12')) and (ie_internado_w = 'S'))) then
		ie_segmentacao_w	:= 3;
	elsif	((ie_segmentacao_w in ('2','9')) or
		((ie_segmentacao_w in ('5','6','11')) and (ie_internado_w = 'S'))) then
		ie_segmentacao_w	:= 3;
	elsif	(ie_segmentacao_w = '4') then
		ie_segmentacao_w	:= 4;
	end if; 
	
	ie_verificar_atualiza_w	:= 'G';
	pls_sip_considerar_guia( nr_seq_regra_ww, nr_seq_conta_p, cd_guia_referencia_w, ie_tipo_contratacao_w, ie_segmentacao_w, 
					sg_uf_sip_w, dt_mes_competencia_w, nm_usuario_p, cd_estabelecimento_p, ie_verificar_atualiza_w);
	if	(ie_verificar_atualiza_w	= 'N') then
		open C01;
		loop
		fetch C01 into
			nr_seq_proc_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			qt_evento_w,
			vl_despesa_w,
			dt_ocorrencia_item_w;
		exit when C01%notfound;
			begin
			if	(ie_considerar_conta_ww	= 'N') and
				(ie_considerar_guia_ww	= 'N') then
				select	max(nr_seq_lote_sip)
				into	nr_seq_lote_sip_w
				from	sip_mov_item_assistencial
				where	nr_seq_proc	= nr_seq_proc_w;
				
				if	(nr_seq_lote_sip_w is not null) then
					select	max(nr_sequencia)
					into	nr_seq_lote_w
					from	pls_lote_sip
					where	nr_sequencia = nr_seq_lote_sip_w
					and	dt_envio is not null;
				end if;
				
				if	(nr_seq_lote_w is null) then
					delete	sip_mov_item_assistencial
					where	nr_seq_proc	= nr_seq_proc_w;
					
					ie_materiais_w	:= null;
					pls_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, cd_area_procedimento_w,
					cd_especialidade_w, cd_grupo_proc_w, ie_origem_proced_w);
					
					select	count(1)
					into	qt_procedimento_resumo_w
					from	sip_mov_item_assistencial	a,
						pls_conta	b
					where	a.nr_seq_conta		= b.nr_sequencia
					and	b.nr_seq_segurado	= nr_seq_segurado_w
					and	a.cd_procedimento	= cd_procedimento_w
					and	a.ie_origem_proced	= ie_origem_proced_w
					and	b.nr_seq_protocolo	= nr_seq_prot_referencia_w;
					
					if	(nr_seq_prot_referencia_w is not null) and
						(qt_procedimento_resumo_w	<> 0) then
						open C04;
						loop
						fetch C04 into	
							nr_seq_item_assistencial_w,
							cd_item_sip_w,
							cd_classificacao_w,
							ie_tipo_contratacao_w,
							ie_segmentacao_w,
							sg_uf_sip_w,
							dt_mes_competencia_w,
							dt_ocorrencia_w;
						exit when C04%notfound;
							begin
							
							select	sip_mov_item_assistencial_seq.nextval
							into	nr_seq_mov_item_sip_w
							from	dual;
							
							insert into sip_mov_item_assistencial
								(nr_sequencia, cd_estabelecimento, qt_evento,
								qt_beneficiario, vl_despesa, nr_seq_conta,
								dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
								nm_usuario_nrec, nr_seq_proc, nr_seq_mat,
								nr_seq_item_sip, ie_tipo_contratacao, ie_segmentacao,
								sg_uf, cd_item_sip, cd_classificacao_sip,
								dt_item_sip,cd_procedimento,ie_origem_proced,
								dt_ocorrencia)
							values	(nr_seq_mov_item_sip_w, cd_estabelecimento_p, 0,
								0, vl_despesa_w, nr_seq_conta_w,
								sysdate, nm_usuario_p, sysdate,
								nm_usuario_p, nr_seq_proc_w, null,
								nr_seq_item_assistencial_w, ie_tipo_contratacao_w, ie_segmentacao_w,
								sg_uf_sip_w, cd_item_sip_w, cd_classificacao_w,
								dt_mes_competencia_w,cd_procedimento_w,ie_origem_proced_w,
								dt_ocorrencia_w);
							end;
						end loop;
						close C04;
					else
						open C03;
						loop
						fetch C03 into	
							nr_seq_item_assistencial_w,
							ie_nasc_vivos_w,
							cd_item_sip_w,
							cd_classificacao_w,
							ie_nascido_vivo_w,
							qt_idade_min_w,
							qt_idade_max_w,
							ie_unid_tempo_idade_w,
							ie_considerar_conta_w,
							ie_considerar_guia_w,
							nr_seq_regra_w;
						exit when C03%notfound;
							begin
							ie_idade_w	:= 'S';
							qt_sip_mov_item_assistencial_w	:= 0;
							if	(ie_unid_tempo_idade_w = 'A') then
								ie_idade_w	:= 'N';
								if	((qt_idade_min_w is not null) and (qt_idade_min_w <= qt_idade_w)) and
									((qt_idade_max_w is not null) and (qt_idade_max_w >= qt_idade_w)) then
									ie_idade_w	:= 'S';
								end if;
							elsif	(ie_unid_tempo_idade_w = 'M') then
								ie_idade_w	:= 'N';
								if	((qt_idade_min_w is not null) and (qt_idade_min_w <= qt_idade_meses_w)) or
									((qt_idade_max_w is not null) and (qt_idade_max_w >= qt_idade_meses_w)) then
									ie_idade_w	:= 'S';
								end if;	
							elsif	(ie_unid_tempo_idade_w = 'D') then
								ie_idade_w	:= 'N';
								if	((qt_idade_min_w is not null) and (qt_idade_min_w <= qt_idade_dia_w)) or
									((qt_idade_max_w is not null) and (qt_idade_max_w >= qt_idade_dia_w)) then
									ie_idade_w	:= 'S';
								end if;
							end if;
							
							if	(ie_idade_w	= 'S') then
								open C08;
								loop
								fetch C08 into	
									cd_classificacao_ww;
								exit when C08%notfound;
									begin
									if	(cd_classificacao_w	<> 'G') then
										if	(substr(cd_classificacao_w,1,2)	= 'EX') or
											(substr(cd_classificacao_w,1,2)	= 'EY') or
											(substr(cd_classificacao_w,1,1)	= 'F')  then
											if ((substr(nvl(cd_classificacao_ww,'0'),1,2) = substr(nvl(cd_classificacao_w,'0'),1,2)) or                                                
												(substr(cd_classificacao_ww,1,1) not in ('E','F','G'))) then 
												qt_sip_mov_item_assistencial_w	:= 1;
											end if;
										else
											--if	(substr(nvl(cd_classificacao_ww,'0'),1,1) = substr(nvl(cd_classificacao_w,'0'),1,1)) then
												qt_sip_mov_item_assistencial_w	:= 1;
											--end if;
										end if;
									end if;
									end;
								end loop;
								close C08;
								
								if	(qt_sip_mov_item_assistencial_w	= 0) then
									if	(ie_nasc_vivos_w	= 'S') and
										((qt_nasc_vivos_w	<> 0) or
										(qt_nasc_vivos_premat_w <> 0)) then
										qt_evento_ww	:= qt_nasc_vivos_w + qt_nasc_vivos_premat_w;
									elsif   (ie_internado_w = 'S') then
										qt_evento_ww    := 1;
									else
										qt_evento_ww	:= qt_evento_w;
									end if;
									
									if	(nvl(ie_considerar_conta_w,'N')	= 'S') then
										ie_considerar_conta_ww		:= 'S';
										nr_seq_regra_ww			:= nr_seq_regra_w;
									end if;
									
									if	(nvl(ie_considerar_guia_w,'N')	= 'S') then
										ie_considerar_guia_ww		:= 'S';
										nr_seq_regra_ww			:= nr_seq_regra_w;
									end if;
									
									select	sip_mov_item_assistencial_seq.nextval
									into	nr_seq_mov_item_sip_w
									from	dual;
									
									insert into sip_mov_item_assistencial
										(nr_sequencia, cd_estabelecimento, qt_evento,
										qt_beneficiario, vl_despesa, nr_seq_conta,
										dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
										nm_usuario_nrec, nr_seq_proc, nr_seq_mat,
										nr_seq_item_sip, ie_tipo_contratacao, ie_segmentacao,
										sg_uf, cd_item_sip, cd_classificacao_sip,
										dt_item_sip,cd_procedimento,ie_origem_proced,
										ie_nascido_vivo, dt_ocorrencia, ie_considerar_guia)
									values	(nr_seq_mov_item_sip_w, cd_estabelecimento_p, qt_evento_ww,
										1, vl_despesa_w, nr_seq_conta_w,
										sysdate, nm_usuario_p, sysdate,
										nm_usuario_p, nr_seq_proc_w, null,
										nr_seq_item_assistencial_w, ie_tipo_contratacao_w, ie_segmentacao_w,
										sg_uf_sip_w, cd_item_sip_w, cd_classificacao_w,
										dt_mes_competencia_w,cd_procedimento_w,ie_origem_proced_w,
										ie_nascido_vivo_w, dt_ocorrencia_item_w, ie_considerar_guia_w);
								end if;
							end if;
							end;
						end loop;
						close C03;
					end if;
				end if;
				/*nr_seq_item_assistencial_w	:= sip_obter_item_assistencial(cd_procedimento_w, ie_origem_proced_w, ie_tipo_guia_w,
											nr_seq_cbo_saude_w, nr_seq_tipo_atendimento_w, nr_seq_clinica_w, 
											qt_idade_w, cd_doenca_cid_w, ie_regime_internacao_w,
											'', ie_sexo_w, ie_parto_normal_w, ie_nasc_vivos_w);*/
			end if;
			end;
		end loop;
		close C01;

		cd_procedimento_w	:= '';
		ie_origem_proced_w	:= '';
		cd_area_procedimento_w	:= '';
		cd_especialidade_w	:= '';
		cd_grupo_proc_w		:= '';
		if	(ie_considerar_conta_ww	= 'N') then
			open C02;
			loop 
			fetch C02 into
				nr_seq_mat_w,
				nr_seq_material_w,
				qt_evento_w,
				vl_despesa_w,
				dt_ocorrencia_item_w;
			exit when C02%notfound;
				begin
				select	max(nr_seq_lote_sip)
				into	nr_seq_lote_sip_w
				from	sip_mov_item_assistencial
				where	nr_seq_mat	= nr_seq_mat_w;
				
				if	(nr_seq_lote_sip_w is not null) then
					select	max(nr_sequencia)
					into	nr_seq_lote_w
					from	pls_lote_sip
					where	nr_sequencia = nr_seq_lote_sip_w
					and	dt_envio is not null;
				end if;
				
				if	(nr_seq_lote_w is null) then
					delete	sip_mov_item_assistencial
					where	nr_seq_mat	= nr_seq_mat_w;
				
					ie_materiais_w	:= 'S';
					
					select	count(1)
					into	qt_procedimento_resumo_w
					from	sip_mov_item_assistencial	a,
						pls_conta			b
					where	a.nr_seq_conta		= b.nr_sequencia
					and	b.nr_seq_segurado	= nr_seq_segurado_w
					and	a.nr_seq_material	= nr_seq_material_w
					and	b.nr_seq_protocolo	= nr_seq_prot_referencia_w;
					
					if	(nr_seq_prot_referencia_w is not null) and
						(qt_procedimento_resumo_w <> 0) then
						open C05;
						loop
						fetch C05 into	
							nr_seq_item_assistencial_w,
							cd_item_sip_w,
							cd_classificacao_w,
							ie_tipo_contratacao_w,
							ie_segmentacao_w,
							sg_uf_sip_w,
							dt_mes_competencia_w,
							dt_ocorrencia_w;
						exit when C05%notfound;
							begin
							
							select	sip_mov_item_assistencial_seq.nextval
							into	nr_seq_mov_item_sip_w
							from	dual;
							
							insert into sip_mov_item_assistencial
								(nr_sequencia, cd_estabelecimento, qt_evento,
								qt_beneficiario, vl_despesa, nr_seq_conta,
								dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
								nm_usuario_nrec, nr_seq_proc, nr_seq_mat,
								nr_seq_item_sip, ie_tipo_contratacao, ie_segmentacao,
								sg_uf, cd_item_sip, cd_classificacao_sip,
								dt_item_sip, nr_seq_material, dt_ocorrencia)
							values	(nr_seq_mov_item_sip_w, cd_estabelecimento_p, 0,
								0, vl_despesa_w, nr_seq_conta_w,
								sysdate, nm_usuario_p, sysdate,
								nm_usuario_p, null, nr_seq_mat_w,
								nr_seq_item_assistencial_w, ie_tipo_contratacao_w, ie_segmentacao_w,
								sg_uf_sip_w, cd_item_sip_w, cd_classificacao_w,
								dt_mes_competencia_w,nr_seq_material_w, dt_ocorrencia_w);
							end;
						end loop;
						close C05;
					else
					
					open C06;
					loop
					fetch C06 into	
						nr_seq_item_assistencial_w,
						ie_nasc_vivos_w,
						cd_procedimento_ww,
						ie_origem_proced_ww,
						cd_area_procedimento_ww,
						cd_especialidade_ww,
						cd_grupo_proc_ww,
						cd_item_sip_w,
						cd_classificacao_w,
						ie_nascido_vivo_w,
						qt_idade_min_w,
						qt_idade_max_w,
						ie_unid_tempo_idade_w;
					exit when C06%notfound;
						begin
						ie_idade_w	:= 'S';
						qt_sip_mov_item_assistencial_w	:= 0;
						if	(ie_unid_tempo_idade_w = 'A') then
							ie_idade_w	:= 'N';
							if	((qt_idade_min_w is not null) and (qt_idade_min_w <= qt_idade_w)) and
								((qt_idade_max_w is not null) and (qt_idade_max_w >= qt_idade_w)) then
								ie_idade_w	:= 'S';
							end if;
						elsif	(ie_unid_tempo_idade_w = 'M') then
							ie_idade_w	:= 'N';
							if	((qt_idade_min_w is not null) and (qt_idade_min_w <= qt_idade_meses_w)) and
								((qt_idade_max_w is not null) and (qt_idade_max_w >= qt_idade_meses_w)) then
								ie_idade_w	:= 'S';
							end if;	
						elsif	(ie_unid_tempo_idade_w = 'D') then
								ie_idade_w	:= 'N';
								if	((qt_idade_min_w is not null) and (qt_idade_min_w <= qt_idade_dia_w)) and
									((qt_idade_max_w is not null) and (qt_idade_max_w >= qt_idade_dia_w)) then
									ie_idade_w	:= 'S';
								end if;
						end if;
						
						if	(ie_idade_w	= 'S') then
							open C07;
							loop
							fetch C07 into	
								cd_classificacao_ww;
							exit when C07%notfound;
								begin
								if	(cd_classificacao_w	<> 'G') then
									if	(substr(cd_classificacao_w,1,2)	= 'EX') or
										(substr(cd_classificacao_w,1,2)	= 'EY') or
										(substr(cd_classificacao_w,1,1)	= 'F') then
											if ((substr(nvl(cd_classificacao_ww,'0'),1,2) = substr(nvl(cd_classificacao_w,'0'),1,2)) or                                                
												(substr(cd_classificacao_ww,1,1) not in ('E','F','G'))) then 
												qt_sip_mov_item_assistencial_w	:= 1;
											end if;
										else
											--if	(substr(nvl(cd_classificacao_ww,'0'),1,1) = substr(nvl(cd_classificacao_w,'0'),1,1)) then
												qt_sip_mov_item_assistencial_w	:= 1;
											--end if;
										end if;
								end if;
								end;
							end loop;
							close C07;
							
							if	(qt_sip_mov_item_assistencial_w	= 0) then
								qt_ptocedimento_regra_w	:= 1;
								ie_acho_regra_w	:= 'N';
								if	(ie_tipo_guia_w	= 5) then
									open C10;
									loop
									fetch C10 into	
										nr_seq_conta_w;
									exit when C10%notfound;
										begin
										if	(ie_acho_regra_w	= 'N') then
											if	(nvl(cd_procedimento_ww,0) <> 0) then
												select	count(1)
												into	qt_ptocedimento_regra_w
												from	pls_conta_proc
												where	nr_seq_conta		= nr_seq_conta_w
												and	cd_procedimento		= cd_procedimento_ww
												and	ie_origem_proced	= ie_origem_proced_ww;
											elsif	(nvl(cd_area_procedimento_ww,0) <> 0) then
												select	count(1)
												into	qt_ptocedimento_regra_w
												from	pls_conta_proc
												where	nr_seq_conta		= nr_seq_conta_w
												and	to_number(obter_dados_estrut_proc(cd_procedimento,ie_origem_proced,'C','A'))	= cd_area_procedimento_ww;
											elsif	(nvl(cd_especialidade_ww,0) <> 0) then
												select	count(1)
												into	qt_ptocedimento_regra_w
												from	pls_conta_proc
												where	nr_seq_conta		= nr_seq_conta_w
												and	to_number(obter_dados_estrut_proc(cd_procedimento,ie_origem_proced,'C','E'))	= cd_especialidade_ww;
											elsif	(nvl(cd_grupo_proc_ww,0) <> 0) then
												select	count(1)
												into	qt_ptocedimento_regra_w
												from	pls_conta_proc	b,
													procedimento	a
												where	b.cd_procedimento	= a.cd_procedimento
												and	b.ie_origem_proced	= a.ie_origem_proced
												and	b.nr_seq_conta		= nr_seq_conta_w
												and	a.cd_grupo_proc		= cd_grupo_proc_ww;
											end if;
											if	(nvl(qt_ptocedimento_regra_w,0) <> 0) then
												ie_acho_regra_w	:= 'S';
											end if;
										end if;
										end;
									end loop;
									close C10;
								else
									if	(nvl(cd_procedimento_ww,0) <> 0) then
										select	count(1)
										into	qt_ptocedimento_regra_w
										from	pls_conta_proc
										where	nr_seq_conta		= nr_seq_conta_w
										and	cd_procedimento		= cd_procedimento_ww
										and	ie_origem_proced	= ie_origem_proced_ww;
									elsif	(nvl(cd_area_procedimento_ww,0) <> 0) then
										select	count(1)
										into	qt_ptocedimento_regra_w
										from	pls_conta_proc
										where	nr_seq_conta		= nr_seq_conta_w
										and	to_number(obter_dados_estrut_proc(cd_procedimento,ie_origem_proced,'C','A'))	= cd_area_procedimento_ww;
									elsif	(nvl(cd_especialidade_ww,0) <> 0) then
										select	count(1)
										into	qt_ptocedimento_regra_w
										from	pls_conta_proc
										where	nr_seq_conta		= nr_seq_conta_w
										and	to_number(obter_dados_estrut_proc(cd_procedimento,ie_origem_proced,'C','E'))	= cd_especialidade_ww;
									elsif	(nvl(cd_grupo_proc_ww,0) <> 0) then
										select	count(1)
										into	qt_ptocedimento_regra_w
										from	pls_conta_proc	b,
											procedimento	a
										where	b.cd_procedimento	= a.cd_procedimento
										and	b.ie_origem_proced	= a.ie_origem_proced
										and	b.nr_seq_conta		= nr_seq_conta_w
										and	a.cd_grupo_proc		= cd_grupo_proc_ww;
									end if;
									if	(qt_ptocedimento_regra_w > 0) then
										ie_acho_regra_w	:= 'S';
									end if;
								end if;
								
								if	(ie_acho_regra_w = 'S') then
									select	sip_mov_item_assistencial_seq.nextval
									into	nr_seq_mov_item_sip_w
									from	dual;
							
									insert into sip_mov_item_assistencial
										(nr_sequencia, cd_estabelecimento, qt_evento,
										qt_beneficiario, vl_despesa, nr_seq_conta,
										dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
										nm_usuario_nrec, nr_seq_proc, nr_seq_mat,
										nr_seq_item_sip, ie_tipo_contratacao, ie_segmentacao,
										sg_uf, cd_item_sip, cd_classificacao_sip,
										dt_item_sip,nr_seq_material, ie_nascido_vivo,
										dt_ocorrencia)
									values	(nr_seq_mov_item_sip_w, cd_estabelecimento_p, 0,
										1, vl_despesa_w, nr_seq_conta_p,
										sysdate, nm_usuario_p, sysdate,
										nm_usuario_p, null, nr_seq_mat_w,
										nr_seq_item_assistencial_w, ie_tipo_contratacao_w, ie_segmentacao_w,
										sg_uf_sip_w, cd_item_sip_w, cd_classificacao_w,
										dt_mes_competencia_w,nr_seq_material_w, ie_nascido_vivo_w,
										dt_ocorrencia_item_w);
								end if;
							end if;
						end if;
						end;
					end loop;
					close C06;
					end if;
				end if;
				/*nr_seq_item_assistencial_w	:= sip_obter_item_assistencial(null, null, ie_tipo_guia_w,
											nr_seq_cbo_saude_w, nr_seq_tipo_atendimento_w, nr_seq_clinica_w,
											qt_idade_w, cd_doenca_cid_w, ie_regime_internacao_w,
											nr_seq_mat_w, ie_sexo_w, ie_parto_normal_w, ie_nasc_vivos_w);*/
				end;
			end loop;
			close C02;
		end if;
		
		if	(ie_considerar_guia_ww	= 'S') then
			ie_verificar_atualiza_ww	:= 'GA';
			pls_sip_considerar_guia( nr_seq_regra_ww, nr_seq_conta_p, cd_guia_referencia_w, ie_tipo_contratacao_w, ie_segmentacao_w, 
							sg_uf_sip_w, dt_mes_competencia_w, nm_usuario_p, cd_estabelecimento_p, ie_verificar_atualiza_ww);
		end if;
	end if;

	if	(ie_considerar_conta_ww	= 'S') and
		(ie_considerar_guia_ww	= 'N') then
		pls_sip_considerar_conta( nr_seq_regra_ww, nr_seq_conta_p, ie_tipo_contratacao_w, ie_segmentacao_w, 
					sg_uf_sip_w, dt_mes_competencia_w, nm_usuario_p, cd_estabelecimento_p);
	end if;
	
	if	(ie_tipo_guia_w	<> '6') then
		if      (ie_internado_w = 'S') then
			qt_evento_w	:= 1;
			if	((qt_nasc_vivos_w	<> 0) or
				(qt_nasc_vivos_premat_w	<> 0)) then
				qt_evento_w	:= qt_nasc_vivos_w + qt_nasc_vivos_premat_w;
			end if;

			update  sip_mov_item_assistencial
			set     qt_evento       = 0
			where   nr_seq_conta    = nr_seq_conta_p;
			nr_seq_item_internacao_F_w	:= 0;
			nr_seq_item_internacao_EX_w	:= 0;
			nr_seq_item_internacao_EY_w	:= 0;
			open C09;
			loop
			fetch C09 into	
				nr_seq_item_internacao_w,
				cd_classificacao_w;
			exit when C09%notfound;
				begin
				update  sip_mov_item_assistencial	a
				set     a.qt_evento	= 0
				where   a.nr_seq_conta	= nr_seq_conta_p
				and	a.nr_sequencia	= nr_seq_item_internacao_w;
				
				if	(substr(cd_classificacao_w,1,1) = 'F') then
					nr_seq_item_internacao_F_w	:= nr_seq_item_internacao_w;
				elsif	(substr(cd_classificacao_w,1,2) = 'EX') then
					nr_seq_item_internacao_EX_w	:= nr_seq_item_internacao_w;
				elsif	(substr(cd_classificacao_w,1,2) = 'EY') then
					nr_seq_item_internacao_EY_w	:= nr_seq_item_internacao_w;
				end if;
				end;
			end loop;
			close C09;

			update  sip_mov_item_assistencial	a
			set     a.qt_evento	= 1
			where   a.nr_seq_conta	= nr_seq_conta_p
			and	a.nr_sequencia	= nr_seq_item_internacao_F_w;

			update  sip_mov_item_assistencial	a
			set     a.qt_evento	= 1
			where   a.nr_seq_conta	= nr_seq_conta_p
			and	a.nr_sequencia	= nr_seq_item_internacao_EX_w;

			update  sip_mov_item_assistencial	a
			set     a.qt_evento	= 1
			where   a.nr_seq_conta	= nr_seq_conta_p
			and	a.nr_sequencia	= nr_seq_item_internacao_EY_w;
			
			update  sip_mov_item_assistencial	a
			set     a.qt_evento       = qt_evento_w
			where   a.nr_seq_conta    = nr_seq_conta_p
			and	a.nr_sequencia	in 	(select  max(x.nr_sequencia)
							from    sip_mov_item_assistencial	x
							where   x.nr_seq_conta    	= nr_seq_conta_p
							and	x.cd_classificacao_sip	= a.cd_classificacao_sip
							and	x.cd_classificacao_sip	= 'G'
							and	x.cd_procedimento is not null);

			update	sip_mov_item_assistencial
			set     qt_evento       = 0
			where   nr_seq_conta    = nr_seq_conta_p
			and	nr_seq_material is not null;
		end if;
	else
		update  sip_mov_item_assistencial
		set     qt_evento       = 0
		where   nr_seq_conta    = nr_seq_conta_p;
	end if;
	if      (ie_internado_w	= 'N') then
		update	sip_mov_item_assistencial
		set     qt_evento       = 0
		where   nr_seq_conta    = nr_seq_conta_p
		and	nr_seq_material is not null;
	end if;
end if;

end pls_gerar_sip_conta_assist;
/