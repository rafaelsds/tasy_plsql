create or replace
procedure eup_gerar_pass_conv_agenda_js(
					ie_lanc_automatico_p		varchar2,
					dt_entrada_p			date,
					nr_atendimento_p		number,
					ie_utiliza_unid_age_serv_p	varchar2,
					cd_tipo_agenda_p		number,
					nr_seq_agenda_p			number,
					nm_usuario_p			Varchar2,
					ie_atualiza_unidade_p       out varchar2) is 
cd_setor_atendimento_w	number(5);
qt_setor_w		number(10);
nr_seq_unidade_w	number(10);
nr_seq_interno_w	number(10);
					
begin

if	(nvl(nr_atendimento_p,0) > 0) then

	cd_setor_atendimento_w := nvl(obter_dados_agendas(nvl(cd_tipo_agenda_p,0), nvl(nr_seq_agenda_p,0), 'CS'),0);

	select	count(*) 
	into	qt_setor_w
	from	atend_paciente_unidade 
	where	nr_atendimento = nr_atendimento_p; 

	if	(ie_utiliza_unid_age_serv_p = 'S') and
		(nvl(cd_tipo_agenda_p,0) = 5) and
		(cd_setor_atendimento_w > 0) and
		(qt_setor_w = 0) then
		
		select  max(nr_seq_unidade)     
		into	nr_seq_unidade_w
		from    agenda_consulta                  
		where   nr_atendimento = nr_atendimento_p;
			
		Ageserv_Gerar_Pass_Setor_Atend(nr_atendimento_p, cd_setor_atendimento_w, dt_entrada_p, 'S', nr_seq_unidade_w, nm_usuario_p);
		ie_atualiza_unidade_p := 'S';
		
	elsif	(cd_setor_atendimento_w > 0) and
		(qt_setor_w = 0) then
		gerar_passagem_setor_atend(nr_atendimento_p, cd_setor_atendimento_w, dt_entrada_p, 'S', nm_usuario_p);
		
		select	nvl(max(nr_seq_interno),0) 
		into	nr_seq_interno_w
		from 	atend_paciente_unidade 
		where 	nr_atendimento = nr_atendimento_p;
		if	(nr_seq_interno_w > 0) then
			Atend_Paciente_Unid_AfterPost(nr_seq_interno_w, 'I', nm_usuario_p);
		end if;
		if	(ie_lanc_automatico_p = 'S') then
			gerar_lancamento_automatico(nr_atendimento_p, null, 26, nm_usuario_p, '', null, null, null, null, null);
		end if;
		ie_atualiza_unidade_p := 'S';
	end if;
end if;
	
commit;

end eup_gerar_pass_conv_agenda_js;
/