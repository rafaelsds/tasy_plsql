create or replace
procedure ptu_processa_resp_ordem_serv(	cd_unimed_benef_p	ptu_resposta_req_ord_serv.cd_unimed_beneficiario%type,
					cd_unimed_solic_p	ptu_resposta_req_ord_serv.cd_unimed_solicitante%type,
					nr_seq_origem_p		ptu_resposta_req_ord_serv.nr_seq_origem%type,
					nr_seq_resposta_p	ptu_resposta_req_ord_serv.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Importar resposta ordem serv_v50

Rotina utilizada nas transa��es ptu via scs homologadas com a unimed brasil.
quando for alterar, favor verificar com o an�lista respons�vel para a realiza��o de testes.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
Performance
---------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

qt_registros_neg_w	pls_integer;
qt_registros_aprov_w	pls_integer;

begin

select	count(1)
into	qt_registros_neg_w
from	ptu_resposta_req_servico
where	nr_seq_resp_req_ord	= nr_seq_resposta_p
and	ie_status_requisicao	= 1;

select	count(1)
into	qt_registros_aprov_w
from	ptu_resposta_req_servico
where	nr_seq_resp_req_ord	= nr_seq_resposta_p
and	ie_status_requisicao	= 2;

-- Se a operadora de origem do benefici�rio for diferente da operadora solicitante da ordem de servi�o, � caracterizada uma Triangula��o
if	(cd_unimed_benef_p	<> cd_unimed_solic_p) then
	if	(qt_registros_neg_w	> 0)	and (qt_registros_aprov_w	= 0) then
		-- Se a resposta da ordem de servi�o vier totalmente recusada, se encerra o processo
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 4
		where	nr_transacao_solicitante	= nr_seq_origem_p
		and	cd_unimed_solicitante		= cd_unimed_solic_p;
	else
		-- Se pelo menos um item for aceito, a ordem de servi�o fica aguardando a transa��o de autoriza��o
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 2
		where	nr_transacao_solicitante	= nr_seq_origem_p
		and	cd_unimed_solicitante		= cd_unimed_solic_p;
	end if;
-- Se a operadora de origem do benefici�rio for igual a operadora solicitante da ordem de servi�o, � caracterizada uma transa��o ponto-a-ponto
elsif	(cd_unimed_benef_p	= cd_unimed_solic_p) then
	if	(qt_registros_neg_w	> 0)	and (qt_registros_aprov_w	= 0) then
		-- Se a resposta da ordem de servi�o vier totalmente recusada, se encerra o processo
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 4
		where	nr_transacao_solicitante	= nr_seq_origem_p
		and	cd_unimed_solicitante		= cd_unimed_solic_p;
	elsif	(qt_registros_neg_w	> 0)	and (qt_registros_aprov_w	> 0) then
		-- Se a resposta da ordem de servi�o vier com servi�os recusados e aceitos, a ordem de servi�o fica Parcialmente aceita
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 5
		where	nr_transacao_solicitante	= nr_seq_origem_p
		and	cd_unimed_solicitante		= cd_unimed_solic_p;
	elsif	(qt_registros_neg_w	= 0)	and (qt_registros_aprov_w	> 0) then
		-- Se a resposta da ordem de servi�o vier totalmente aceita, o est�gio da ordem de servi�o fica Aceita e se encerra o processo
		update	ptu_requisicao_ordem_serv
		set	ie_estagio			= 3
		where	nr_transacao_solicitante	= nr_seq_origem_p
		and	cd_unimed_solicitante		= cd_unimed_solic_p;
	end if;	
end if;

commit;

end ptu_processa_resp_ordem_serv;
/