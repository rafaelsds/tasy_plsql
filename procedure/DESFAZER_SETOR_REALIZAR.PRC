create or replace
procedure desfazer_setor_realizar(
		nr_seq_atendimento_p	number,
		nm_usuario_p		varchar2) is 
        
nr_atendimento_w    	number(10);
nr_seq_interno_w    	number(10);
dt_entrada_unidade_w    date;
ds_erro_w   		varchar(255);
ie_transf_paci_acomod_w	varchar2(1);

begin

/* Quimioterapia - Par�metro [492] - Ao definir acomoda��o do paciente, controlar ocupa��o das unidades de setor. */
obter_param_usuario(3130, 492, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_transf_paci_acomod_w);

update	paciente_atendimento
set	nr_seq_local		= null,
	dt_acomodacao_paciente	= null,
	nm_usuario		= nm_usuario_p
where	nr_seq_atendimento	= nr_seq_atendimento_p;

if	(ie_transf_paci_acomod_w = 'S') then

	SELECT  max(nr_atendimento)
	INTO    nr_atendimento_w
	FROM    paciente_atendimento
	where   nr_seq_atendimento = NR_SEQ_ATENDIMENTO_P;

	nr_seq_interno_w := OBTER_ATEPACU_PACIENTE(nr_atendimento_w, 'A');

	select	max(dt_entrada_unidade)
	into	dt_entrada_unidade_w
	from    atend_paciente_unidade
	where   nr_seq_interno = nr_seq_interno_w;

	if (nr_atendimento_w is not null) then
		EXCLUIR_ATEND_PAC_UNIDADE(nr_atendimento_w, dt_entrada_unidade_w, obter_usuario_ativo(),ds_erro_w, nr_seq_interno_w);
	end if;
end if;

commit;

end desfazer_setor_realizar;
/