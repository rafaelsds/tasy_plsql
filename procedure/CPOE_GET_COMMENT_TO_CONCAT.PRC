create or replace procedure cpoe_get_comment_to_concat(
										nr_seq_item_p		number,
										nm_table_p			varchar2,
										si_state_p			varchar2,
										ds_justification_p	out varchar2,
										ds_notes_p			out varchar2
										) as
						
Cursor C01 is	
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_DIETA'
	and a.nr_seq_cpoe_nutricao = nr_seq_item_p
	and a.dt_text_concat is null 
	and upper(si_state_p) = 'UPDATING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_DIETA'
	and a.nr_seq_cpoe_nutricao_tmp = nr_seq_item_p
	and a.dt_text_concat is null 
	and upper(si_state_p) = 'INSERTING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_MATERIAL'
	and a.nr_seq_cpoe_material = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'UPDATING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_MATERIAL'
	and a.nr_seq_cpoe_material_tmp = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'INSERTING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_PROCEDIMENTO'
	and a.nr_seq_cpoe_proced = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'UPDATING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_PROCEDIMENTO'
	and a.nr_seq_cpoe_proced_tmp = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'INSERTING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_GASOTERAPIA'
	and a.nr_seq_cpoe_gasoterapia = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'UPDATING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_GASOTERAPIA'
	and a.nr_seq_cpoe_gasoterapia_tmp = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'INSERTING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_RECOMENDACAO'
	and a.nr_seq_cpoe_recomend = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'UPDATING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_RECOMENDACAO'
	and a.nr_seq_cpoe_recomend_tmp = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'INSERTING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_HEMOTERAPIA'
	and a.nr_seq_cpoe_hemoterapia = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'UPDATING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_HEMOTERAPIA'
	and a.nr_seq_cpoe_hemoterapia_tmp = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'INSERTING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_DIALISE'
	and a.nr_seq_cpoe_dialise = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'UPDATING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_DIALISE'
	and a.nr_seq_cpoe_dialise_tmp = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'INSERTING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_ANATOMIA_PATOLOGICA'
	and a.nr_seq_cpoe_anat_pat = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'UPDATING'
	union all
	select	b.ds_comment,
			b.si_add_text,
			a.nr_sequencia,
			a.ds_additional_text
	from	cpoe_std_comment b,
			cpoe_comment_linkage a
	where	a.nr_seq_std_comment = b.nr_sequencia
	and nm_table_p = 'CPOE_ANATOMIA_PATOLOGICA'
	and a.nr_seq_cpoe_anat_pat_tmp = nr_seq_item_p
	and a.dt_text_concat is null
	and upper(si_state_p) = 'INSERTING';
	
ds_notes_w			varchar2(4000)	:= null;
ds_justification_w	varchar2(4000)	:= null;
ds_additional_w		cpoe_comment_linkage.ds_additional_text%type;

begin
	
	for r_c01_w in c01 loop
		ds_additional_w	:= null;
		
		if	(r_c01_w.ds_additional_text is not null) then
			ds_additional_w	:= ' : ' || r_c01_w.ds_additional_text;
		end if;
		
		if	(r_c01_w.si_add_text	= 'AN') then
			ds_notes_w := ds_notes_w || r_c01_w.ds_comment || ds_additional_w || ', ';
		elsif	(r_c01_w.si_add_text	= 'AJ') then
			ds_justification_w := ds_justification_w || r_c01_w.ds_comment || ds_additional_w || ', ';
		end if;
		
		update	cpoe_comment_linkage
		set		dt_text_concat = sysdate
		where	nr_sequencia = r_c01_w.nr_sequencia;	
	end loop;

	ds_justification_p := SUBSTR(ds_justification_w, 1, LENGTH(ds_justification_w) - 2);
	ds_notes_p := SUBSTR(ds_notes_w, 1, LENGTH(ds_notes_w) - 2);

end cpoe_get_comment_to_concat;
/
