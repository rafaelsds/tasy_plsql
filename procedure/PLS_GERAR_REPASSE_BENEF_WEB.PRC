create or replace
procedure pls_gerar_repasse_benef_web
			(	ie_tipo_acao_p			varchar2,
				dt_repasse_p			varchar2,
				nr_seq_congenere_p		number,
				nr_seq_segurado_p		number,
				nr_seq_repasse_p		number,
				ie_consistir_regra_contrato_p	varchar2,
				ie_mes_posterior_repasse_p	varchar2,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		number,
				nr_seq_repasse_gerado_p   out	number) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:   Gerar repasse do beneficiário
----------------------------------------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [  ] Tasy (Delphi/Java) [ X ] Portal [  ] Relatórios [ ] Outros:
 ----------------------------------------------------------------------------------------------------------------------------------------------------
Pontos de atenção:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

IE_TIPO_ACAO_P
I - INSERT
U - UPADTE
*/

ie_preco_w			varchar2(2);
ie_tipo_repasse_w		varchar2(1);
nr_seq_contrato_w		number(10);
nr_seq_congenere_atend_w	number(10);
nr_seq_congenere_cobr_w		number(10);
dt_contratacao_w		date;
qt_repasse_w			number(10);
nr_seq_ordem_w			number(10);
dt_repasse_w			date;
nr_seq_repasse_gerado_w		number(10);

Cursor C01 is
	select	nr_seq_congenere_cobr,
		nr_seq_congenere,
		ie_tipo_repasse
	from	(	select	nr_seq_congenere_cobr,
				nvl(nr_seq_congenere,0) nr_seq_congenere,
				ie_titularidade,
				ie_tipo_parentesco,
				ie_tipo_repasse,
				ie_tipo_intercambio
			from	pls_contrato_regra_repasse
			where	nr_seq_contrato	= nr_seq_contrato_w
			and	((nr_seq_congenere = nr_seq_congenere_atend_w) or (nr_seq_congenere is null))
			and	pls_obter_se_depend_repasse(nr_sequencia,nr_seq_segurado_p) = 'S'
			and	pls_restring_loc_atend_repasee(nr_sequencia,nr_seq_congenere_atend_w,cd_estabelecimento_p) = 'S'
			union all
			select	nr_seq_congenere_cobr,
				nvl(nr_seq_congenere,0),
				ie_titularidade,
				ie_tipo_parentesco,
				ie_tipo_repasse,
				ie_tipo_intercambio
			from	pls_contrato_regra_repasse
			where	nr_seq_contrato	is null
			and	nr_seq_intercambio is null
			and	cd_estabelecimento	= cd_estabelecimento_p
			and	((nr_seq_congenere = nr_seq_congenere_atend_w) or (nr_seq_congenere is null))
			and	pls_obter_se_depend_repasse(nr_sequencia,nr_seq_segurado_p) = 'S'
			and	not exists (	select	1
						from	pls_contrato_regra_repasse
						where	nr_seq_contrato	= nr_seq_contrato_w))
	order by nr_seq_congenere,
		decode(nvl(ie_titularidade,'A'),'A',-1,1),
		decode(ie_tipo_parentesco,null,-1,1),
		decode(nvl(ie_tipo_intercambio,'A'),'A',-1,1);

begin

nr_seq_congenere_atend_w	:= nr_seq_congenere_p;

select	b.ie_preco,
	a.nr_seq_contrato,
	a.dt_contratacao
into	ie_preco_w,
	nr_seq_contrato_w,
	dt_contratacao_w
from	pls_segurado	a,
	pls_plano	b
where	a.nr_seq_plano	= b.nr_sequencia
and	a.nr_sequencia	= nr_seq_segurado_p;


if	(ie_consistir_regra_contrato_p = 'S') and
	(nr_seq_contrato_w is not null) then
	open C01;
	loop
	fetch C01 into	
		nr_seq_congenere_cobr_w,
		nr_seq_ordem_w,
		ie_tipo_repasse_w;
	exit when C01%notfound;
	end loop;
	close C01;
else
	nr_seq_congenere_cobr_w	:= nr_seq_congenere_atend_w;
end if;

if	(nvl(nr_seq_congenere_cobr_w,0) = 0) then
	nr_seq_congenere_cobr_w	:= nr_seq_congenere_atend_w;
end if;

if	(ie_tipo_repasse_w is null) then
	if	(ie_preco_w = '1') then
		ie_tipo_repasse_w	:= 'P';
	else
		ie_tipo_repasse_w	:= 'C';
	end if;
end if;	

if	(ie_tipo_acao_p = 'I') then
	select	count(*)
	into	qt_repasse_w
	from	pls_segurado_repasse
	where	nr_seq_segurado	= nr_seq_segurado_p
	and	dt_fim_repasse is null;
	
	
	dt_repasse_w := to_date(dt_repasse_p,'dd/mm/yyyy hh24:mi:ss');
	
	if	(ie_mes_posterior_repasse_p = 'S') then
		dt_repasse_w	:=  add_months(trunc(dt_repasse_w,'month'),1);
	end if; 
	
	/* Lepinski - OS 403172 - Finalizar repasse existente ao inserir um novo */
	if	(qt_repasse_w > 0) then
		update	pls_segurado_repasse
		set	dt_fim_repasse	= (dt_repasse_w-1),
			dt_fim_real	= sysdate
		where	nr_seq_segurado	= nr_seq_segurado_p
		and	dt_repasse is not null
		and	dt_fim_repasse is null;
	end if;
	
	select  pls_segurado_repasse_seq.nextval
	into	nr_seq_repasse_gerado_w
	from	dual;
	
	insert into pls_segurado_repasse (nr_sequencia, dt_repasse, nr_seq_congenere,
					  nm_usuario, dt_atualizacao, cd_estabelecimento,
					  nr_seq_segurado, dt_atualizacao_nrec, ie_origem_solicitacao,
					  nr_seq_congenere_atend, ie_tipo_repasse, ie_tipo_compartilhamento)
				   values(nr_seq_repasse_gerado_w, nvl(dt_repasse_w,dt_contratacao_w), nr_seq_congenere_cobr_w,
					  nm_usuario_p, sysdate, cd_estabelecimento_p,
					  nr_seq_segurado_p, sysdate, 'P',
					  nr_seq_congenere_atend_w, ie_tipo_repasse_w, 1);
					  
	nr_seq_repasse_gerado_p := nr_seq_repasse_gerado_w;
elsif	(ie_tipo_acao_p = 'U') then
	update	pls_segurado_repasse 
	set 	dt_repasse 		= nvl(to_date(dt_repasse_p,'dd/mm/yyyy hh24:mi:ss'),dt_contratacao_w),
		nr_seq_congenere 	= nr_seq_congenere_p,
		nm_usuario 		= nm_usuario_p,
		ie_origem_solicitacao 	= 'P', 
		dt_atualizacao 		= sysdate
	where	nr_sequencia = nr_seq_repasse_p;

end if;

commit;

end pls_gerar_repasse_benef_web;
/
