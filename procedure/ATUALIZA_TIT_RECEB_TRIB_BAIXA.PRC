/*Verificar chamada*/
create or replace
procedure atualiza_tit_receb_trib_baixa(	dt_recebimento_p	date,
						cd_estabelecimento_p	number,
						nr_seq_trib_baixa_p	number,
						vl_baixa_p		number,
						nr_titulo_p		number,
						nr_seq_trans_financ_p	number,
						nr_seq_baixa_p		number,
						nm_usuario_p		varchar2) is 

						
dt_recebimento_w		titulo_receber_liq.dt_recebimento%type := dt_recebimento_p;	
cd_estabelecimento_w		titulo_receber.cd_estabelecimento%type := cd_estabelecimento_p;
nr_seq_trans_financ_w		titulo_receber_trib_baixa.nr_seq_trans_financ%type := nr_seq_trans_financ_p;
vl_baixa_w			titulo_receber_trib_baixa.vl_baixa%type := vl_baixa_p;
nr_titulo_w			titulo_receber_trib_baixa.nr_titulo%type := nr_titulo_p;

cursor c01 is
select	a.nm_atributo,
        10 cd_tipo_lote_contab
from	atributo_contab a
where 	a.cd_tipo_lote_contab = 39
and 	a.nm_atributo in ( 'VL_TRIB_TIT_REC')
union all
select	a.nm_atributo,
        5 cd_tipo_lote_contab
from	atributo_contab a
where 	a.cd_tipo_lote_contab = 5
and 	a.nm_atributo in ( 'VL_TRIB_TIT_REC');

c01_w		c01%rowtype;

begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

open c01;
loop
fetch c01 into	
	c01_w;
exit when c01%notfound;
	begin
	
	if	(nvl(vl_baixa_w, 0) <> 0) then
		begin                
                
		ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
									trunc(dt_recebimento_w),
									c01_w.cd_tipo_lote_contab,
									nr_seq_trans_financ_w,
									55,
									nr_titulo_w,
									nr_seq_baixa_p,
									nr_seq_trib_baixa_p,
									vl_baixa_w,
									'TITULO_RECEBER_TRIB_BAIXA',
									c01_w.nm_atributo,
									nm_usuario_p);

		end;
	end if;
	
	end;
end loop;
close c01;


end atualiza_tit_receb_trib_baixa;
/