create or replace
procedure reconst_before_delete_js( 	nr_prescricao_p		number,
					nr_sequencia_p		number,
					cd_perfil_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 
					
ie_reg_alteracao_farm_w	varchar2(1);
cd_material_w	number(6);
qt_dose_w	number(15);
qt_solucao_w	number(15,4);
cd_unidade_medida_dose_w varchar2(30);	

begin
obter_param_usuario(7010, 48, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_reg_alteracao_farm_w);

if	(nr_prescricao_p is not null) and
	(nr_sequencia_p is not null) then
	begin
	
	select  max(cd_material), 
		max(qt_dose), 
		max(qt_solucao), 
		max(cd_unidade_medida_dose) 
	into	cd_material_w, 
		qt_dose_w, 
		qt_solucao_w, 
		cd_unidade_medida_dose_w
	from    prescr_material 
	where   nr_prescricao           = nr_prescricao_p 
	and     nr_sequencia_diluicao   = nr_sequencia_p 
	and     ie_agrupador            = 9;
	
	if	(ie_reg_alteracao_farm_w = 'S') then
		gerar_historico_dil_red_rec('N', 3, 9, nr_prescricao_p, nr_sequencia_p, cd_material_w, 0, qt_dose_w, 0, qt_solucao_w, 0, cd_unidade_medida_dose_w, '',
		cd_estabelecimento_p, nm_usuario_p);		
	end if;
	
	end;
end if;

end reconst_before_delete_js;
/