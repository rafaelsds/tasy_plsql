create or replace
procedure vincula_atend_age_cir_eup_js(
					nr_seq_agenda_p		number,
					nr_seq_agenda_vinculo_p	number,
					ds_tipos_atend_agenda_p	varchar2,
					nr_atendimento_p	number,
					ds_lista_agendas_pac_p	varchar2,
					nm_usuario_p		Varchar2) is 

begin

if	(nvl(nr_atendimento_p,0) > 0) then
	atualizar_atend_agenda_pac_js(nr_seq_agenda_p, nr_seq_agenda_vinculo_p, ds_tipos_atend_agenda_p, nr_atendimento_p, ds_lista_agendas_pac_p, nm_usuario_p);

	vinc_automaticamente_atend_EUP(nr_seq_agenda_vinculo_p, nr_atendimento_p);

end if;

commit;

end vincula_atend_age_cir_eup_js;
/