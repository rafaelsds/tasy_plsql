create or replace 
procedure qua_gerar_itens_auditar(	
					nr_sequencia_p		Number,
					nm_usuario_p		Varchar2) IS


nr_sequencia_w			Number(10);
nr_seq_evento_w			Number(10);
nr_seq_tipo_analise_w		Number(10);
nr_seq_questao_w		Number(10);
nr_seq_apres_w			Number(10);

Cursor C01 IS
	select	a.nr_sequencia,
		a.nr_seq_apres
	from	qua_questao_analise a,
		qua_grupo_analise b
	where	a.nr_seq_grupo = b.nr_sequencia
	and	b.nr_seq_tipo = nr_seq_tipo_analise_w
	and	nvl(a.ie_situacao,'A') = 'A'
	and	nvl(b.ie_situacao,'A') = 'A';

BEGIN
select	b.nr_seq_tipo_analise
into	nr_seq_tipo_analise_w
from	qua_evento b,
	qua_evento_paciente a
where	a.nr_sequencia  = nr_sequencia_p
and	a.nr_seq_evento = b.nr_sequencia;

OPEN C01;
LOOP
FETCH C01 INTO
	nr_seq_questao_w,
	nr_seq_apres_w;
EXIT WHEN C01%NOTFOUND;
	begin
	select	qua_evento_pac_questao_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert	into qua_evento_pac_questao(
		nr_sequencia,
		nr_seq_evento,
		nr_seq_questao,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres)
	values(
		nr_sequencia_w,
		nr_sequencia_p,
		nr_seq_questao_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w);
	end;
END LOOP;
CLOSE C01;

commit;
end qua_gerar_itens_auditar;
/
