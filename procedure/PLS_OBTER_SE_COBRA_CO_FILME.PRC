create or replace
procedure pls_obter_se_cobra_co_filme (	nr_seq_conta_proc_p		pls_conta_proc.nr_sequencia%type,
					dt_procedimento_ref_p		pls_conta_proc.dt_procedimento_referencia%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					ie_cobrar_custo_op_p	out	pls_regra_cobr_val_pos.ie_cobrar_custo_op%type,
					ie_cobrar_filme_p	out	pls_regra_cobr_val_pos.ie_cobrar_filme%type,
					nr_seq_regra_p		out	pls_regra_cobr_val_pos.nr_sequencia%type) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Verificar se deve ser cobrado os valores de CO e Filme do p�s-estabelecido
	conforme regra de cobran�a de custo operacional cadastrada na pasta
	"OPS - Contas M�dicas / Cobran�a custo operacional" da "OPS - Cadastro de Regras"
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Criada com base em um cursor pois futuramente pode ser necess�rio acrescentar verifica��es.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
			
ie_cobrar_custo_op_w	pls_regra_cobr_val_pos.ie_cobrar_custo_op%type := 'S';
ie_cobrar_filme_w	pls_regra_cobr_val_pos.ie_cobrar_filme%type := 'S';
nr_seq_grau_partic_w	pls_grau_participacao.nr_sequencia%type;
nr_seq_regra_w		pls_regra_cobr_val_pos.nr_sequencia%type;
qt_regra_w		pls_integer;

Cursor C01 (	nr_seq_grau_partic_pc	pls_grau_participacao.nr_sequencia%type,
		cd_estabelecimento_pc	estabelecimento.cd_estabelecimento%type,
		dt_procedimento_ref_pc	pls_conta_proc.dt_procedimento_referencia%type) is
	select	ie_cobrar_custo_op,
		ie_cobrar_filme,
		nr_sequencia
	from	pls_regra_cobr_val_pos
	where	dt_procedimento_ref_pc between dt_inicio_vigencia_ref and dt_fim_vigencia_ref
	and	cd_estabelecimento = cd_estabelecimento_pc
	and	((nr_seq_grau_partic is null) or (nr_seq_grau_partic = nr_seq_grau_partic_pc))
	order by nr_seq_grau_partic;

begin
select	max(nr_seq_grau_partic)
into	nr_seq_grau_partic_w
from	pls_proc_participante
where	nr_seq_conta_proc = nr_seq_conta_proc_p;

if	(nr_seq_grau_partic_w is null) then
	select	max(a.nr_seq_grau_partic)
	into	nr_seq_grau_partic_w
	from	pls_conta a,
		pls_conta_proc b
	where	a.nr_sequencia = b.nr_seq_conta
	and	b.nr_sequencia = nr_seq_conta_proc_p;
end if;

for r_C01_w in C01 (nr_seq_grau_partic_w, cd_estabelecimento_p, dt_procedimento_ref_p) loop
	
	ie_cobrar_custo_op_w := r_C01_w.ie_cobrar_custo_op;
	ie_cobrar_filme_w := r_C01_w.ie_cobrar_filme;
	nr_seq_regra_w := r_C01_w.nr_sequencia;
	-- Se achou uma regra sai do cursor
	exit;
end loop;

ie_cobrar_custo_op_p 	:= ie_cobrar_custo_op_w;
ie_cobrar_filme_p 	:= ie_cobrar_filme_w;
nr_seq_regra_p		:= nr_seq_regra_w;

end pls_obter_se_cobra_co_filme;
/
