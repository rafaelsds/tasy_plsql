CREATE OR REPLACE
PROCEDURE do_arrival_for_today_appts(cd_pessoa_fisica_p varchar2) IS

CURSOR c_appointments_not_arrived IS
  SELECT  v.cd_agenda, v.nr_sequencia, v.cd_pessoa_fisica, v.nr_atendimento
  FROM    scheduling_check_today_appts_v v
  WHERE   v.ie_status_agenda IN ('N', 'CN')
  AND     v.cd_pessoa_fisica = cd_pessoa_fisica_p;

BEGIN
  wheb_usuario_pck.set_ie_commit('N');

  FOR reg IN c_appointments_not_arrived LOOP
    do_arrival_for_appointment(reg.cd_agenda, reg.nr_sequencia, reg.cd_pessoa_fisica, reg.nr_atendimento);
  END LOOP;

  COMMIT;
END do_arrival_for_today_appts;
/
