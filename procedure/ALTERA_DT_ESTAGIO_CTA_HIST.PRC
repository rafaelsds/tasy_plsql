create or replace
procedure altera_dt_estagio_cta_hist(
			dt_final_estagio_p	date,
			nr_seq_pend_hist_p	number) is 

begin

if	(nr_seq_pend_hist_p is not null) then
	update  cta_pendencia_hist
	set     dt_final_estagio = dt_final_estagio_p,
		qt_horas_estagio = trunc((dt_final_estagio_p - dt_inicio_estagio) * 24),
		ie_tipo_meta = Obter_Tipo_Meta_Estagio(nr_seq_estagio, 0, trunc((dt_final_estagio_p - dt_inicio_estagio) * 24))
	where   nr_sequencia = nr_seq_pend_hist_p; 
end if;

commit;

end altera_dt_estagio_cta_hist;
/