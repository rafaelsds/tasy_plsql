create or replace
procedure altera_status_conj_cme(
				cd_status_conj_p	number,
				nr_sequencia_p		number,
				ie_expurgo_p		varchar2,
				nm_usuario_p		varchar2,
                nm_usuario_orig_p varchar2 default null) is

nr_seq_embalagem_w		number(10);
dt_validade_w			date;

begin

if	(nvl(cd_status_conj_p,0) > 0) and
	(nvl(nr_sequencia_p,0) > 0) and
	(nvl(ie_expurgo_p,'X') <> 'X') then
	
	if	(cd_status_conj_p = 1) then
		
		select	nr_seq_embalagem
		into	nr_seq_embalagem_w
		from	cm_conjunto_cont
		where	nr_sequencia = nr_sequencia_p;
		
		select	(sysdate + qt_dia_validade)
		into	dt_validade_w
		from	cm_classif_embalagem
		where	nr_sequencia = nr_seq_embalagem_w;
		
	end if;
	
	update	cm_conjunto_cont
	set	ie_status_conjunto = cd_status_conj_p,
		ie_expurgo = ie_expurgo_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		nm_usuario_origem = nvl(nm_usuario_orig_p,nm_usuario_origem),
		dt_validade = decode(dt_validade_w,null,null,dt_validade_w)
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end altera_status_conj_cme;
/
