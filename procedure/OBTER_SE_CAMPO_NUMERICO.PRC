create or replace
function obter_se_campo_numerico(ds_campo_p varchar2)
     				return varchar2 is

nr_campo_w number;
ie_campo_numerico_w varchar2(1);

begin

begin
	nr_campo_w := to_number(ds_campo_p);
	ie_campo_numerico_w := 'S';
exception when others then
	ie_campo_numerico_w := 'N';
end;

return ie_campo_numerico_w;

end obter_se_campo_numerico;
/
