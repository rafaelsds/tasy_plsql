create or replace
procedure incluir_idioma_nac(	cd_nacionalidade_p	varchar2,
				cd_pessoa_fisica_p	varchar2,
				nm_usuario_p		varchar2) is

nr_seq_idioma_w		number(10);
nr_seq_idioma_pf_w	number(10);


begin


if	(cd_pessoa_fisica_p is not null) then

	select	max(nr_seq_idioma)
	into	nr_seq_idioma_w
	from	nacionalidade
	where	cd_nacionalidade	=	cd_nacionalidade_p;

	if	(nr_seq_idioma_w is not null) then

		select	max(nr_seq_idioma)
		into	nr_seq_idioma_pf_w
		from	pessoa_fisica_idioma
		where	cd_pessoa_fisica	=	cd_pessoa_fisica_p;			
	
		if	(nr_seq_idioma_pf_w is null) then
			
			insert into pessoa_fisica_idioma
				(cd_pessoa_fisica,			
				dt_atualizacao,
				dt_atualizacao_nrec,
				ie_fluencia,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_idioma,
				nr_sequencia)
			values	(cd_pessoa_fisica_p,
				sysdate,
				sysdate,
				'F',
				nm_usuario_p,
				nm_usuario_p,
				nr_seq_idioma_w,
				pessoa_fisica_idioma_seq.nextval);		

		end if;
	end if;
end if;

commit;

end incluir_idioma_nac;
/