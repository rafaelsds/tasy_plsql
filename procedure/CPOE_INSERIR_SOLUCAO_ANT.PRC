create or replace procedure CPOE_Inserir_solucao_ant(nr_atendimento_p        prescr_medica.nr_atendimento%type,
                                                     nr_atendimento_ant_p    prescr_medica.nr_atendimento%type,
                                                     nr_prescricao_p         prescr_medica.nr_prescricao%type,
                                                     nr_sequencia_p          prescr_solucao.nr_seq_solucao%type,
                                                     nm_usuario_p            prescr_medica.nm_usuario%type,
                                                     cd_perfil_p             perfil.cd_perfil%type,
                                                     cd_estabelecimento_p    estabelecimento.cd_estabelecimento%type,
                                                     nr_seq_item_gerado_p    out number,
                                                     cd_pessoa_fisica_p      pessoa_fisica.cd_pessoa_fisica%type,
                                                     nr_seq_transcricao_p    number default null,
                                                     ie_item_alta_p          varchar2 default 'N',
                                                     ie_prescritor_aux_p     varchar2 default 'N',
                                                     cd_medico_p             number default null,
                                                     ie_retrogrado_p         varchar2 default 'N',
                                                     dt_inicio_p             date default null,
                                                     nr_seq_pepo_p           number default null,
                                                     nr_cirurgia_p           number default null,
                                                     nr_cirurgia_patologia_p number default null,
                                                     nr_seq_agenda_p         number default null,
                                                     ie_oncologia_p          varchar2 default 'N',
                                                     nr_seq_conclusao_apae_p number default null,
                 ie_futuro_p           varchar2 default 'N',
                 nr_seq_cpoe_rp_p       cpoe_rp.nr_sequencia%type default null,
                 nr_seq_cpoe_order_unit_p    cpoe_order_unit.nr_sequencia%type default null) is

  -- Duplicacao da procedure CPOE_REP_Gerar_solucao

  type cpoe_comp_solucao is record(
    nr_seq_material_w     prescr_material.nr_sequencia%type,
    cd_material_w         prescr_material.cd_material%type,
    qt_dose_w             prescr_material.qt_dose%type,
    cd_unidade_medida_w   prescr_material.cd_unidade_medida%type,
    qt_volume_corrigido_w prescr_material.qt_volume_corrigido%type);

  type vetor is table of cpoe_comp_solucao index by binary_integer;

  cpoe_componente_sol_w vetor;

  cd_intervalo_w          prescr_solucao.cd_intervalo%type;
  ds_horarios_w           prescr_solucao.ds_horarios%type;
  ds_horarios_aux_w       prescr_solucao.ds_horarios%type;
  ds_orientacao_w         prescr_solucao.ds_orientacao%type;
  ds_solucao_w            prescr_solucao.ds_solucao%type;
  hr_prim_horario_w       prescr_solucao.hr_prim_horario%type;
  ie_acm_w                prescr_solucao.ie_acm%type;
  ie_bomba_infusao_w      prescr_solucao.ie_bomba_infusao%type;
  ie_fator_correcao_w     prescr_solucao.ie_fator_correcao%type;
  ie_pca_modo_prog_w      prescr_solucao.ie_pca_modo_prog%type;
  ie_se_necessario_w      prescr_solucao.ie_se_necessario%type;
  ie_solucao_especial_w   prescr_solucao.ie_solucao_especial%type;
  ie_solucao_pca_w        prescr_solucao.ie_solucao_pca%type;
  ie_tipo_analgesia_w     prescr_solucao.ie_tipo_analgesia%type;
  ie_tipo_dosagem_w       prescr_solucao.ie_tipo_dosagem%type;
  ie_tipo_solucao_w       prescr_solucao.ie_tipo_solucao%type;
  ie_um_bolus_pca_w       prescr_solucao.ie_um_bolus_pca%type;
  ie_um_dose_inicio_pca_w prescr_solucao.ie_um_dose_inicio_pca%type;
  ie_um_fluxo_pca_w       prescr_solucao.ie_um_fluxo_pca%type;
  ie_um_limite_hora_pca_w prescr_solucao.ie_um_limite_hora_pca%type;
  ie_um_limite_pca_w      prescr_solucao.ie_um_limite_pca%type;
  ie_via_aplicacao_w      prescr_solucao.ie_via_aplicacao%type;
  nr_etapas_w             prescr_solucao.nr_etapas%type;
  nr_seq_solucao_w        prescr_solucao.nr_seq_solucao%type;
  qt_bolus_pca_w          prescr_solucao.qt_bolus_pca%type;
  qt_dosagem_w            prescr_solucao.qt_dosagem%type;
  qt_dose_ataque_w        prescr_solucao.qt_dose_ataque%type;
  qt_dose_inicial_pca_w   prescr_solucao.qt_dose_inicial_pca%type;
  qt_hora_fase_w          prescr_solucao.qt_hora_fase%type;
  qt_intervalo_bloqueio_w prescr_solucao.qt_intervalo_bloqueio%type;
  qt_limite_quatro_hora_w prescr_solucao.qt_limite_quatro_hora%type;
  qt_limite_uma_hora_w    prescr_solucao.qt_limite_uma_hora%type;
  qt_solucao_total_w      prescr_solucao.qt_solucao_total%type;
  qt_tempo_aplicacao_w    prescr_solucao.qt_tempo_aplicacao%type;
  qt_vol_infusao_pca_w    prescr_solucao.qt_vol_infusao_pca%type;
  qt_volume_w             prescr_solucao.qt_volume%type;
  cd_protocolo_w          prescr_solucao.cd_protocolo%type;
  ie_continuo_w           varchar2(1);

  nr_ocorrencia_w     cpoe_material.nr_ocorrencia%type;
  qt_dose_correcao_w  cpoe_material.qt_dose_correcao1%type;
  ie_administracao_w  cpoe_material.ie_administracao%type;
  ie_ref_calculo_w    cpoe_material.ie_ref_calculo%type;
  dt_prim_horario_w   cpoe_material.dt_inicio%type;
  qt_hora_fase_cpoe_w cpoe_material.qt_hora_fase%type;
  ie_duracao_w      cpoe_material.ie_duracao%type := 'C';
  dt_fim_w            date := null;

  i                      number(15);
  x                      number(15);
  ie_sol_continua_w      char(1);
  ie_prescr_alta_agora_w varchar2(1);
  ie_use_item_types_w    varchar2(1);
  qt_min_intervalo_w     intervalo_prescricao.qt_min_intervalo%type;
  sql_w                  varchar2(4000);
  ds_sep_bv_w            varchar2(10);
  ds_parametros_w        varchar2(2000);

  cd_material_ret_w material.cd_material%type;
  cd_material_w     prescr_material.cd_material%type;

  cd_setor_execucao_w		cpoe_order_unit.cd_setor_execucao%type default null;

  ie_via_aplicacao_rp_w             cpoe_rp.ie_via_aplicacao%type default null;
  nr_seq_grupo_interval_rp_w        cpoe_rp.nr_seq_grupo_interval%type default null;
  nr_seq_subgrupo_interval_rp_w     cpoe_rp.nr_seq_subgrupo_interval%type default null;
  cd_intervalo_rp_w                 cpoe_rp.cd_intervalo%type default null;
  dt_inicio_rp_w                    cpoe_rp.dt_inicio%type default null;
  dt_fim_rp_w                       cpoe_rp.dt_fim%type default null;
  ds_horarios_rp_w                  cpoe_rp.ds_horarios%type default null;
  ie_segunda_rp_w                   cpoe_rp.ie_segunda%type default null;
  ie_terca_rp_w                     cpoe_rp.ie_terca%type default null;
  ie_quarta_rp_w                    cpoe_rp.ie_quarta%type default null;
  ie_quinta_rp_w                    cpoe_rp.ie_quinta%type default null;
  ie_sexta_rp_w                     cpoe_rp.ie_sexta%type default null;
  ie_sabado_rp_w                    cpoe_rp.ie_sabado%type default null;
  ie_domingo_rp_w                   cpoe_rp.ie_domingo%type default null;
  ie_duracao_rp_w                 protocolo_medic_material.ie_duracao%type default null;
  
  ie_urgencia_rp_w                  cpoe_rp.ie_urgencia%type default null;
  ie_administracao_rp_w             cpoe_rp.ie_administracao%type default null;
  qt_solucao_total_rp_w             cpoe_rp.qt_solucao_total%type default null;
  qt_volume_rp_w                    cpoe_rp.qt_volume%type default null;
  ie_tipo_analgesia_rp_w            cpoe_rp.ie_tipo_analgesia%type default null;
  ie_pca_modo_prog_rp_w             cpoe_rp.ie_pca_modo_prog%type default null;
  qt_dose_inicial_pca_rp_w          cpoe_rp.qt_dose_inicial_pca%type default null;
  ie_um_dose_inicio_pca_rp_w        cpoe_rp.ie_um_dose_inicio_pca%type default null;
  ie_um_fluxo_pca_rp_w              cpoe_rp.ie_um_fluxo_pca%type default null;
  qt_vol_infusao_pca_rp_w           cpoe_rp.qt_vol_infusao_pca%type default null;
  qt_bolus_pca_rp_w                 cpoe_rp.qt_bolus_pca%type default null;
  ie_um_bolus_pca_rp_w              cpoe_rp.ie_um_bolus_pca%type default null;
  qt_intervalo_bloqueio_rp_w        cpoe_rp.qt_intervalo_bloqueio%type default null;
  qt_limite_quatro_hora_rp_w        cpoe_rp.qt_limite_quatro_hora%type default null;
  ie_um_limite_pca_rp_w             cpoe_rp.ie_um_limite_pca%type default null;
  qt_limite_uma_hora_rp_w           cpoe_rp.qt_limite_uma_hora%type default null;
  ie_um_limite_hora_pca_rp_w        cpoe_rp.ie_um_limite_hora_pca%type default null;
  si_rapid_infusion_rp_w            cpoe_rp.si_rapid_infusion%type default null;
  qt_final_concentration_rp_w       cpoe_rp.qt_final_concentration%type default null;
  ie_incrementar_rp_w               cpoe_rp.ie_incrementar%type default null;
  ie_tipo_dosagem_inc_rp_w          cpoe_rp.ie_tipo_dosagem_inc%type default null;
  ie_um_final_conc_pca_rp_w         cpoe_rp.ie_um_final_conc_pca%type default null;
  qt_dosagem_inc_rp_w               cpoe_rp.qt_dosagem_inc%type default null;
  ie_fator_correcao_rp_w            cpoe_rp.ie_fator_correcao%type default null;
  qt_dose_terapeutica_rp_w          cpoe_rp.qt_dose_terapeutica%type default null;
  

  cursor c01 is
    select cd_intervalo,
           ds_horarios,
           ds_orientacao,
           ds_solucao,
           hr_prim_horario,
           nvl(ie_acm, 'N'),
           ie_bomba_infusao,
           ie_fator_correcao,
           ie_pca_modo_prog,
           nvl(ie_se_necessario, 'N'),
           nvl(ie_solucao_especial, 'N'),
           nvl(ie_solucao_pca, 'N'),
           ie_tipo_analgesia,
           ie_tipo_dosagem,
           ie_tipo_solucao,
           ie_um_bolus_pca,
           ie_um_dose_inicio_pca,
           ie_um_fluxo_pca,
           ie_um_limite_hora_pca,
           ie_um_limite_pca,
           ie_via_aplicacao,
           nr_etapas,
           nr_seq_solucao,
           qt_bolus_pca,
           qt_dosagem,
           qt_dose_ataque,
           qt_dose_inicial_pca,
           qt_hora_fase,
           qt_intervalo_bloqueio,
           qt_limite_quatro_hora,
           qt_limite_uma_hora,
           qt_solucao_total,
           qt_tempo_aplicacao,
           qt_vol_infusao_pca,
           qt_volume,
           cd_protocolo
      from prescr_solucao
     where nr_prescricao = nr_prescricao_p
       and nr_seq_solucao = nr_sequencia_p
     order by nr_seq_solucao;

  cursor c02 is
    select nr_sequencia,
           cd_material,
           qt_dose,
           cd_unidade_medida_dose,
           qt_volume_corrigido
      from prescr_material
     where nr_prescricao = nr_prescricao_p
       and nr_sequencia_solucao = nr_seq_solucao_w
       and nr_sequencia_diluicao is null
       and rownum <= 7
     order by nvl(nr_agrupamento, 0),
              nr_sequencia,
              nvl(nr_sequencia_diluicao, 0);

begin

  ds_sep_bv_w := obter_separador_bv;
  obter_param_usuario(924,
                      409,
                      cd_perfil_p,
                      nm_usuario_p,
                      cd_estabelecimento_p,
                      ie_prescr_alta_agora_w);
  open c01;
  loop
    fetch c01
      into cd_intervalo_w, ds_horarios_w, ds_orientacao_w, ds_solucao_w, hr_prim_horario_w, ie_acm_w, ie_bomba_infusao_w, ie_fator_correcao_w, ie_pca_modo_prog_w, ie_se_necessario_w, ie_solucao_especial_w, ie_solucao_pca_w, ie_tipo_analgesia_w, ie_tipo_dosagem_w, ie_tipo_solucao_w, ie_um_bolus_pca_w, ie_um_dose_inicio_pca_w, ie_um_fluxo_pca_w, ie_um_limite_hora_pca_w, ie_um_limite_pca_w, ie_via_aplicacao_w, nr_etapas_w, nr_seq_solucao_w, qt_bolus_pca_w, qt_dosagem_w, qt_dose_ataque_w, qt_dose_inicial_pca_w, qt_hora_fase_w, qt_intervalo_bloqueio_w, qt_limite_quatro_hora_w, qt_limite_uma_hora_w, qt_solucao_total_w, qt_tempo_aplicacao_w, qt_vol_infusao_pca_w, qt_volume_w, cd_protocolo_w;
    exit when c01%notfound;
    begin

     cd_material_ret_w := OBTER_SUBST_GENERIC_MEDIC(cd_protocolo_w,
                                                   nr_seq_item_gerado_p,
                                                   null,
                                                   nr_seq_solucao_w,
                                                   nr_atendimento_ant_p);

    IF (NVL(cd_material_ret_w, 0) > 0) THEN
        cd_material_w := cd_material_ret_w;
      end if;

      if (nvl(ie_fator_correcao_w, 'N') = 'N') then
        obter_param_usuario(924,
                            1082,
                            cd_perfil_p,
                            nm_usuario_p,
                            cd_estabelecimento_p,
                            ie_fator_correcao_w);
      end if;

      ie_sol_continua_w := 'N';

      if (ie_solucao_pca_w = 'S') then
        ie_tipo_solucao_w := 'P';
      elsif (ie_solucao_especial_w = 'S') then
        ie_tipo_solucao_w := 'V';
      elsif (ie_tipo_solucao_w is null) then
        ie_tipo_solucao_w := 'C';
        ie_sol_continua_w := 'S';
      end if;

      qt_hora_fase_cpoe_w := obter_horas_minutos(qt_hora_fase_w * 60);

      ie_administracao_w := 'P';
      if (ie_acm_w = 'S') then
        ie_administracao_w := 'C';
        ds_horarios_w      := '';
        hr_prim_horario_w  := '';
      elsif (ie_se_necessario_w = 'S') then
        ie_administracao_w := 'N';
        ds_horarios_w      := '';
        hr_prim_horario_w  := '';
      end if;

      ie_ref_calculo_w := 'NE';
      if (cd_intervalo_w is not null) then
        ie_ref_calculo_w := 'I';
      end if;

      i := 0;
      For i in 0 .. 4 LOOP
        begin
          cpoe_componente_sol_w(i).nr_seq_material_w := null;
          cpoe_componente_sol_w(i).cd_material_w := null;
          cpoe_componente_sol_w(i).qt_dose_w := null;
          cpoe_componente_sol_w(i).cd_unidade_medida_w := null;
          cpoe_componente_sol_w(i).qt_volume_corrigido_w := null;
        end;
      end loop;

      i := 0;
      open c02;
      loop
        fetch c02
          into cpoe_componente_sol_w(i) .nr_seq_material_w, cpoe_componente_sol_w(i) .cd_material_w, cpoe_componente_sol_w(i) .qt_dose_w, cpoe_componente_sol_w(i) .cd_unidade_medida_w, cpoe_componente_sol_w(i) .qt_volume_corrigido_w;
        exit when c02%notfound;
        begin
          i := i + 1;
        end;
      end loop;
      close c02;

      qt_dose_correcao_w := cpoe_obter_volume_corrigido(ie_bomba_infusao_w,
                                                        nvl(ie_fator_correcao_w,
                                                            'N'),
                                                        qt_volume_w,
                                                        cpoe_componente_sol_w(0)
                                                        .cd_material_w,
                                                        cpoe_componente_sol_w(0)
                                                        .qt_dose_w,
                                                        cpoe_componente_sol_w(0)
                                                        .cd_unidade_medida_w);

      if ((nvl(ie_prescr_alta_agora_w, 'N') = 'S') and
         (nvl(ie_item_alta_p, 'N') = 'S') or (ie_administracao_w = 'P')) then

        select max(qt_min_intervalo)
          into qt_min_intervalo_w
          from intervalo_prescricao
         where cd_intervalo = cd_intervalo_w;

        if (hr_prim_horario_w is not null) then
          dt_prim_horario_w := to_date(to_char(sysdate, 'dd/mm/yyyy ') ||
                                       hr_prim_horario_w,
                                       'dd/mm/yyyy hh24:mi');
          if (dt_prim_horario_w < sysdate) then
            dt_prim_horario_w := dt_prim_horario_w + 1;
          end if;
        else
          dt_prim_horario_w := sysdate;
        end if;

        if (cd_intervalo_w is not null) then
          cpoe_calcular_horario_prescr(nr_atendimento_p,
                                       cd_intervalo_w,
                                       cpoe_componente_sol_w(0)
                                       .cd_material_w,
                                       dt_prim_horario_w,
                                       0,
                                       qt_min_intervalo_w,
                                       nr_ocorrencia_w,
                                       ds_horarios_w,
                                       ds_horarios_aux_w,
                                       nm_usuario_p,
                                       cd_estabelecimento_p,
                                       cd_perfil_p,
                                       null);
        elsif (ie_ref_calculo_w = 'NE') then

          if (ie_continuo_w = 'S') then
            ie_continuo_w := 'S';
          else
            ie_continuo_w := 'N';
          end if;

          CPOE_Calcula_horarios_etapas(nm_usuario_p,
                                       dt_prim_horario_w,
                                       'S',
                                       nr_etapas_w,
                                       cd_intervalo_w,
                                       qt_tempo_aplicacao_w,
                                       dividir(24, nvl(nr_etapas_w, 0)),
                                       0,
                                       ds_horarios_w,
                                       'N');

          ds_horarios_w := replace(padroniza_horario_prescr(ds_horarios_w,
                                                            null),
                                   'A');
        end if;

        ds_horarios_w := ds_horarios_w || ds_horarios_aux_w;

        hr_prim_horario_w := obter_prim_dshorarios(ds_horarios_w);

        dt_prim_horario_w := to_date(to_char(sysdate, 'dd/mm/yyyy ') ||
                                     hr_prim_horario_w,
                                     'dd/mm/yyyy hh24:mi');

        if (dt_prim_horario_w < sysdate) then

          dt_prim_horario_w := dt_prim_horario_w + 1;

        end if;
      end if;

      if (ie_retrogrado_p = 'S' or ie_futuro_p = 'S') then
        -- retrograde item
        dt_prim_horario_w := dt_inicio_p;
        dt_fim_w          := (dt_prim_horario_w + 1) - 1 / 1440;
        ie_duracao_w    := 'P';

        select max(qt_min_intervalo)
        into qt_min_intervalo_w
        from intervalo_prescricao
        where cd_intervalo = cd_intervalo_w;

        if (cd_intervalo_w is not null) then
              cpoe_calcular_horario_prescr(nr_atendimento_p, cd_intervalo_w, cpoe_componente_sol_w(0).cd_material_w,
                                           dt_prim_horario_w, 0, qt_min_intervalo_w, nr_ocorrencia_w, ds_horarios_w,
                                           ds_horarios_aux_w, nm_usuario_p, cd_estabelecimento_p, cd_perfil_p, null);
            elsif (ie_ref_calculo_w = 'NE') then

              if (ie_continuo_w = 'S') then
                ie_continuo_w := 'S';
              else
                ie_continuo_w := 'N';
              end if;

              CPOE_Calcula_horarios_etapas(nm_usuario_p, dt_prim_horario_w, 'S', nr_etapas_w, cd_intervalo_w,
                                           qt_tempo_aplicacao_w, dividir(24, nvl(nr_etapas_w, 0)), 0, ds_horarios_w, 'N');

              ds_horarios_w := replace(padroniza_horario_prescr(ds_horarios_w,null), 'A');
        end if;

            ds_horarios_w := ds_horarios_w || ds_horarios_aux_w;
            hr_prim_horario_w := obter_prim_dshorarios(ds_horarios_w);
      end if;

      select cpoe_material_seq.nextval
       into nr_seq_item_gerado_p
      from dual;

      obter_param_usuario(2314,46,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_use_item_types_w);
      if (nvl(ie_use_item_types_w, 'N') = 'S') then

        select  cd_setor_execucao
        into    cd_setor_execucao_w
        from    cpoe_order_unit
        where   nr_sequencia = nr_seq_cpoe_order_unit_p;

        select
          ie_via_aplicacao
          ,nr_seq_grupo_interval
          ,nr_seq_subgrupo_interval
          ,cd_intervalo
          ,dt_inicio
          ,dt_fim
          ,ds_horarios
          ,ie_segunda
          ,ie_terca
          ,ie_quarta
          ,ie_quinta
          ,ie_sexta
          ,ie_sabado
          ,ie_domingo
          ,ie_urgencia
          ,ie_administracao
          ,qt_solucao_total
          ,qt_volume
          ,ie_tipo_analgesia
          ,ie_pca_modo_prog
          ,qt_dose_inicial_pca
          ,ie_um_dose_inicio_pca
          ,ie_um_fluxo_pca
          ,qt_vol_infusao_pca
          ,qt_bolus_pca
          ,ie_um_bolus_pca
          ,qt_intervalo_bloqueio
          ,qt_limite_quatro_hora
          ,ie_um_limite_pca
          ,qt_limite_uma_hora
          ,ie_um_limite_hora_pca
          ,si_rapid_infusion
          ,qt_final_concentration
          ,ie_incrementar
          ,ie_tipo_dosagem_inc
          ,ie_um_final_conc_pca
          ,qt_dosagem_inc
          ,ie_fator_correcao
          ,qt_dose_terapeutica
        into
          ie_via_aplicacao_rp_w
          ,nr_seq_grupo_interval_rp_w
          ,nr_seq_subgrupo_interval_rp_w
          ,cd_intervalo_rp_w
          ,dt_inicio_rp_w
          ,dt_fim_rp_w
          ,ds_horarios_rp_w
          ,ie_segunda_rp_w
          ,ie_terca_rp_w
          ,ie_quarta_rp_w
          ,ie_quinta_rp_w
          ,ie_sexta_rp_w
          ,ie_sabado_rp_w
          ,ie_domingo_rp_w
          ,ie_urgencia_rp_w
          ,ie_administracao_rp_w
          ,qt_solucao_total_rp_w
          ,qt_volume_rp_w
          ,ie_tipo_analgesia_rp_w
          ,ie_pca_modo_prog_rp_w
          ,qt_dose_inicial_pca_rp_w
          ,ie_um_dose_inicio_pca_rp_w
          ,ie_um_fluxo_pca_rp_w
          ,qt_vol_infusao_pca_rp_w
          ,qt_bolus_pca_rp_w
          ,ie_um_bolus_pca_rp_w
          ,qt_intervalo_bloqueio_rp_w
          ,qt_limite_quatro_hora_rp_w
          ,ie_um_limite_pca_rp_w
          ,qt_limite_uma_hora_rp_w
          ,ie_um_limite_hora_pca_rp_w
          ,si_rapid_infusion_rp_w
          ,qt_final_concentration_rp_w
          ,ie_incrementar_rp_w
          ,ie_tipo_dosagem_inc_rp_w
          ,ie_um_final_conc_pca_rp_w
          ,qt_dosagem_inc_rp_w
          ,ie_fator_correcao_rp_w
          ,qt_dose_terapeutica_rp_w
        from cpoe_rp
        where  nr_sequencia = nr_seq_cpoe_rp_p;

        ie_duracao_rp_w := 'P';

      end if;

      insert into cpoe_material
        (nr_sequencia,
         nr_atendimento,
         ie_ref_calculo,
         ie_controle_tempo,
         ie_duracao,
         dt_inicio,
         dt_fim,
         cd_intervalo,
         ds_horarios,
         ds_orientacao_preparo,
         ds_solucao,
         hr_prim_horario,
         ie_acm,
         ie_bomba_infusao,
         ie_se_necessario,
         ie_tipo_dosagem,
         ie_tipo_solucao,
         ie_via_aplicacao,
         nr_etapas,
         qt_dosagem,
         qt_dose_ataque,
         qt_hora_fase,
         qt_tempo_aplicacao,
         cd_material,
         qt_dose,
         cd_unidade_medida,
         dt_atualizacao,
         dt_atualizacao_nrec,
         nm_usuario,
         nm_usuario_nrec,
         cd_perfil_ativo,
         cd_pessoa_fisica,
         cd_funcao_origem,
         nr_seq_transcricao,
         qt_dose_correcao1,
         ie_item_alta,
         ie_prescritor_aux,
         cd_medico,
         ie_retrogrado,
         nr_seq_pepo,
         nr_cirurgia,
         nr_cirurgia_patologia,
         nr_seq_agenda,
         ie_oncologia,
         nr_seq_conclusao_apae,
         ie_futuro,
         nr_seq_cpoe_rp,
         nr_seq_cpoe_order_unit,
         cd_setor_atendimento
         ,nr_seq_grupo_interval
         ,nr_seq_subgrupo_interval
         ,ie_segunda
         ,ie_terca
         ,ie_quarta
         ,ie_quinta
         ,ie_sexta
         ,ie_sabado
         ,ie_domingo
         ,ie_urgencia
         ,ie_administracao
         ,ie_fator_correcao
         ,ie_pca_modo_prog
         ,ie_tipo_analgesia
         ,ie_um_bolus_pca
         ,ie_um_dose_inicio_pca
         ,ie_um_fluxo_pca
         ,ie_um_limite_hora_pca
         ,ie_um_limite_pca
         ,qt_bolus_pca
         ,qt_dose_inicial_pca
         ,qt_intervalo_bloqueio
         ,qt_limite_quatro_hora
         ,qt_limite_uma_hora
         ,qt_solucao_total
         ,qt_vol_infusao_pca
         ,qt_volume
      )
      values
        (nr_seq_item_gerado_p,
         nr_atendimento_p,
         ie_ref_calculo_w,
         'S',
         nvl(ie_duracao_rp_w,ie_duracao_w),
         nvl(dt_inicio_rp_w, dt_prim_horario_w),
         nvl(dt_fim_rp_w,dt_fim_w),
         nvl(cd_intervalo_rp_w,cd_intervalo_w),
         nvl(ds_horarios_rp_w,ds_horarios_w),
         ds_orientacao_w,
         ds_solucao_w,
         hr_prim_horario_w,
         ie_acm_w,
         ie_bomba_infusao_w,
         ie_se_necessario_w,
         ie_tipo_dosagem_w,
         ie_tipo_solucao_w,
         nvl(ie_via_aplicacao_rp_w,ie_via_aplicacao_w),
         nr_etapas_w,
         qt_dosagem_w,
         qt_dose_ataque_w,
         qt_hora_fase_cpoe_w,
         qt_tempo_aplicacao_w,
         cpoe_componente_sol_w(0).cd_material_w,
         cpoe_componente_sol_w(0).qt_dose_w,
         cpoe_componente_sol_w(0).cd_unidade_medida_w,
         sysdate,
         sysdate,
         nm_usuario_p,
         nm_usuario_p,
         cd_perfil_p,
         OBTER_CD_PACIENTE_PRESCRICAO(nr_prescricao_p),
         2314,
         nr_seq_transcricao_p,
         qt_dose_correcao_w,
         ie_item_alta_p,
         ie_prescritor_aux_p,
         cd_medico_p,
         ie_retrogrado_p,
         nr_seq_pepo_p,
         nr_cirurgia_p,
         nr_cirurgia_patologia_p,
         nr_seq_agenda_p,
         ie_oncologia_p,
         nr_seq_conclusao_apae_p,
         ie_futuro_p,
         nr_seq_cpoe_rp_p,
         nr_seq_cpoe_order_unit_p,
         cd_setor_execucao_w
        ,nr_seq_grupo_interval_rp_w
        ,nr_seq_subgrupo_interval_rp_w
        ,ie_segunda_rp_w
        ,ie_terca_rp_w
        ,ie_quarta_rp_w
        ,ie_quinta_rp_w
        ,ie_sexta_rp_w
        ,ie_sabado_rp_w
        ,ie_domingo_rp_w
        ,ie_urgencia_rp_w            --> Na versao anterior era preenchido como NULL
        ,nvl(ie_administracao_rp_w, ie_administracao_w)
        ,nvl(ie_fator_correcao_rp_w, ie_fator_correcao_w)
        ,nvl(ie_pca_modo_prog_rp_w, ie_pca_modo_prog_w)
        ,nvl(ie_tipo_analgesia_rp_w, ie_tipo_analgesia_w)
        ,nvl(ie_um_bolus_pca_rp_w, ie_um_bolus_pca_w)
        ,nvl(ie_um_dose_inicio_pca_rp_w, ie_um_dose_inicio_pca_w)
        ,nvl(ie_um_fluxo_pca_rp_w, ie_um_fluxo_pca_w)
        ,nvl(ie_um_limite_hora_pca_rp_w, ie_um_limite_hora_pca_w)
        ,nvl(ie_um_limite_pca_rp_w, ie_um_limite_pca_w)
        ,nvl(qt_bolus_pca_rp_w, qt_bolus_pca_w)
        ,nvl(qt_dose_inicial_pca_rp_w, qt_dose_inicial_pca_w)
        ,nvl(qt_intervalo_bloqueio_rp_w, qt_intervalo_bloqueio_w)
        ,nvl(qt_limite_quatro_hora_rp_w, qt_limite_quatro_hora_w)
        ,nvl(qt_limite_uma_hora_rp_w, qt_limite_uma_hora_w)
        ,nvl(qt_solucao_total_rp_w, qt_solucao_total_w)
        ,nvl(qt_vol_infusao_pca_rp_w, qt_vol_infusao_pca_w)
        ,nvl(qt_volume_rp_w, qt_volume_w)
      );

      cpoe_consis_med_insert(nr_seq_item_gerado_p,nm_usuario_p);

      CPOE_REP_Gerar_dil_redil_recon(nr_prescricao_p,
                                     nr_seq_item_gerado_p,
                                     cpoe_componente_sol_w(0)
                                     .nr_seq_material_w,
                                     'N');

      x := 1;
      while (x < i) loop
        begin
          if (cpoe_componente_sol_w(x).nr_seq_material_w is not null) then

            qt_dose_correcao_w := cpoe_obter_volume_corrigido(ie_bomba_infusao_w,
                                                              nvl(ie_fator_correcao_w,
                                                                  'N'),
                                                              qt_volume_w,
                                                              cpoe_componente_sol_w(0)
                                                              .cd_material_w,
                                                              cpoe_componente_sol_w(0)
                                                              .qt_dose_w,
                                                              cpoe_componente_sol_w(0)
                                                              .cd_unidade_medida_w);

            ds_parametros_w := 'cd_material_w=' || cpoe_componente_sol_w(x)
                              .cd_material_w || ds_sep_bv_w || 'qt_dose_w=' ||
                               cpoe_componente_sol_w(x)
                              .qt_dose_w || ds_sep_bv_w ||
                               'qt_dose_correcao=' || qt_dose_correcao_w ||
                               ds_sep_bv_w || 'cd_unidade_medida_w=' ||
                               cpoe_componente_sol_w(x)
                              .cd_unidade_medida_w || ds_sep_bv_w ||
                               'nr_seq_item_gerado_p=' ||
                               nr_seq_item_gerado_p;

            sql_w := ' update  cpoe_material ' || ' set    cd_mat_comp' || x ||
                     ' = :cd_material_w, ' || '       qt_dose_comp' || x ||
                     ' = :qt_dose_w, ' || '       qt_dose_correcao' ||
                     to_char(x + 1) || ' = :qt_dose_correcao, ' ||
                     '       cd_unid_med_dose_comp' || x ||
                     ' = :cd_unidade_medida_w ' ||
                     ' where    nr_sequencia = :nr_seq_item_gerado_p ';

            exec_sql_dinamico_bv('TASY', sql_w, ds_parametros_w);

            CPOE_REP_Gerar_dil_redil_recon(nr_prescricao_p,
                                           nr_seq_item_gerado_p,
                                           cpoe_componente_sol_w(x)
                                           .nr_seq_material_w,
                                           'N');
          end if;
          x := x + 1;
        end;
      end loop;

    end;
  end loop;
  close c01;

  commit;

end CPOE_Inserir_solucao_ant;
/
