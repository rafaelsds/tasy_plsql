create or replace
procedure hsm_gerar_sinal_vital_toLife	(	nr_atendimento_p	number,
						qt_peso_p		number,
						qt_altura_p		number,
						qt_dor_p		number,
						qt_glicose_p		number,
						qt_temperatura_p	number,
						qt_pa_sistolica_p	number,
						qt_pa_diastolica_p	number,
						qt_oxigenio_p		number,
						qt_pulso_p		number,
						nm_usuario_p		Varchar2,
						qt_freq_resp_p	number default null) is 

begin



insert into atendimento_sinal_vital(	nr_sequencia,
					nr_atendimento,
					dt_sinal_vital,
					dt_atualizacao,
					nm_usuario,
					CD_PESSOA_FISICA,
					dt_liberacao,
					ie_situacao,
					nr_cirurgia,
					nr_seq_pepo,
					qt_peso,
					QT_ALTURA_CM,
					QT_ESCALA_DOR,
					QT_GLICEMIA_CAPILAR,
					QT_TEMP,
					QT_PA_SISTOLICA,
					QT_PA_DIASTOLICA,
					QT_SATURACAO_O2,
					QT_FREQ_CARDIACA,
					CD_ESCALA_DOR,
					QT_FREQ_RESP)
		values		  (	atendimento_sinal_vital_seq.nextval,
					nr_atendimento_p,
					sysdate,
					sysdate,
					nm_usuario_p,
					null,
					sysdate,
					'A',
					null,
					null,
					qt_peso_p,
					qt_altura_p,
					qt_dor_p,
					qt_glicose_p,
					qt_temperatura_p,
					qt_pa_sistolica_p,
					qt_pa_diastolica_p,
					qt_oxigenio_p,
					qt_pulso_p,
					decode(qt_dor_p,null,null,'FEVA'),
					qt_freq_resp_p);
commit;

end hsm_gerar_sinal_vital_toLife;
/
