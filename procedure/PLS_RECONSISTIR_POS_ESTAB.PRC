create or replace
procedure pls_reconsistir_pos_estab(    nr_seq_analise_p        number,
                                        cd_estabelecimento_p    number,
					ie_reconsistir_p	varchar2,
                                        nm_usuario_p            Varchar2        ) is
/*Esta rotina tem como objetivo realizar a reconsistencia das an�lises de p�s estabelecido*/

nr_seq_pos_estab_w      number(10);
nr_seq_conta_w          number(10);
ie_origem_analise_w	Number(10);
nr_seq_analise_pagto_w	Number(10);
nr_seq_analise_pos_w	Number(10);

Cursor C01 is
        select  distinct(nr_seq_conta)
        from    pls_conta_pos_estabelecido
        where   nr_seq_analise = nr_seq_analise_p
	and	((ie_situacao	= 'A') or (ie_situacao	is null));
begin
/*ie_reconsistir = 'S'  -> reconsistir p�s estabelecido
    ie_reconsistir = 'N' -> recalcular p�sestabelecido
*/

select	max(ie_origem_analise)
into	ie_origem_analise_w
from	pls_analise_conta
where	nr_sequencia = nr_seq_analise_p;

if	(ie_origem_analise_w = '2') then
	/*Se for uma an�lse de p�s.*/
	nr_seq_analise_pos_w := nr_seq_analise_p;

	select	max(nr_seq_analise_ref)
	into	nr_seq_analise_pagto_w
	from	pls_analise_conta
	where	nr_sequencia = nr_seq_analise_p;
else
	/*Se for uma an�lise de pagto.*/
	select	max(nr_sequencia)
	into	nr_seq_analise_pos_w
	from	pls_analise_conta
	where	nr_seq_analise_ref = nr_seq_analise_p;

	nr_seq_analise_pagto_w := nr_seq_analise_p;
end if;

select	max(ie_origem_analise)
into	ie_origem_analise_w
from	pls_analise_conta
where	nr_sequencia = nr_seq_analise_pagto_w;

open C01;
loop
fetch C01 into
	nr_seq_conta_w;
exit when C01%notfound;
	begin
	
	if	(ie_reconsistir_p = 'S')	then
		/*Inativar as ocorrencias do pos estabelecido*/
		pls_desfazer_oco_pos_estab(nr_seq_conta_w,nm_usuario_p);
		
		/*Recalcular novamente o pos estab*/
		pls_gerar_valor_pos_estab(nr_seq_conta_w, nm_usuario_p,'A',null,null,'C'); 
		
		/*Gerar novamente as ocorr�ncia*/
		pls_gerar_ocorrencia_pos_estab(nr_seq_analise_p, nr_seq_conta_w,null,cd_estabelecimento_p,nm_usuario_p);

		/*Atualizar dados na an�lise */				
		if	(ie_origem_analise_w = 3) then
			pls_atualizar_pos_estab_ptu(	nr_seq_conta_w,	null, null,
							nr_seq_analise_p, 'N', cd_estabelecimento_p, 
							nm_usuario_p	);
		else
			pls_atualizar_conta_pos_estab(	nr_seq_conta_w,	null, null,
							nr_seq_analise_p, 'N', cd_estabelecimento_p, 
							nm_usuario_p	);
		end if;
						
						
		/*Atualizar as glosas da an�lise*/	
		pls_atualiza_oco_analise_pos(nr_seq_conta_w, nm_usuario_p);
			
		/*Liberar valor e verificar status*/
		pls_analise_status_fat(		nr_seq_conta_w, null, null,
						nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p);
	elsif	(ie_reconsistir_p = 	'N')	then
		/*Recalcular novamente o pos estab*/
		pls_gerar_valor_pos_estab(nr_seq_conta_w, nm_usuario_p,'A',null,null,'C');

		if	(ie_origem_analise_w = 3) then
			pls_atualizar_pos_estab_ptu(	nr_seq_conta_w,	null, null,
							nr_seq_analise_p, 'N', cd_estabelecimento_p, 
							nm_usuario_p );
		else
			pls_atualizar_conta_pos_estab(	nr_seq_conta_w,	null, null,
							nr_seq_analise_p, 'N', cd_estabelecimento_p, 
							nm_usuario_p );
		end if;
						
		/*inclu�do o tratamento abaixo para que o valor liberado seja atualizado estava ficando zerado  Diogo*/
		/*Liberar valor e verificar status*/
		pls_analise_status_fat(		nr_seq_conta_w, null, null,
						nr_seq_analise_p, cd_estabelecimento_p, nm_usuario_p);
	end if;
	end;
end loop;
close C01;		

end pls_reconsistir_pos_estab;
/
