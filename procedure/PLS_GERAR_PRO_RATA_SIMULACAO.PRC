create or replace
procedure pls_gerar_pro_rata_simulacao
		(	nr_seq_simulacao_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is 

nr_seq_resumo_w			number(10);
nr_seq_segurado_simul_w		number(10);
dt_simulacao_w			date;
qt_dias_dividir_w		number(10);
qt_dias_adesao_w		number(10);
qt_dias_mes_mensalidade_w	number(10);
qt_dias_pro_rata_w		number(10);
qt_dias_antecipado_w		number(10);
nr_seq_item_resumo_w		number(10);
vl_antecipacao_w		number(15,2);
vl_pro_rata_dia_w		number(15,2);
vl_resumo_w			number(15,2);
nr_seq_ordem_w			number(10);
ie_tipo_item_w			varchar2(5);
qt_regra_pro_rata_w		number(10);
ie_data_base_proporcional_w	varchar2(1);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_segurado_simul
	from	pls_simulacao_resumo
	where	nr_seq_simulacao	= nr_seq_simulacao_p
	and	nr_seq_ordem		= 1;

Cursor C02 is
	select	nr_sequencia,
		nr_seq_ordem,
		vl_resumo,
		ie_tipo_item
	from	pls_simulacao_resumo
	where	nr_seq_segurado_simul = nr_seq_segurado_simul_w;
	
begin
select	dt_simulacao
into	dt_simulacao_w
from	pls_simulacao_preco
where	nr_sequencia	= nr_seq_simulacao_p;

select	Obter_Dias_Entre_Datas(dt_simulacao_w,add_months(dt_simulacao_w,1)),
	to_char(to_date(dt_simulacao_w),'dd'),
	to_char(trunc(last_day(dt_simulacao_w),'dd'),'dd')
into	qt_dias_dividir_w,
	qt_dias_adesao_w,
	qt_dias_mes_mensalidade_w
from	dual;

select	max(ie_data_base_proporcional)
into	ie_data_base_proporcional_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(nvl(ie_data_base_proporcional_w,'U') = 'T') then
	qt_dias_dividir_w	:= 30;
	/* tratamento fevereiro */
	if	(to_char(dt_simulacao_w,'mm') = 02 ) then
		qt_dias_dividir_w	:= to_char(last_day(dt_simulacao_w),'dd');
	end if;	
end if;

qt_dias_pro_rata_w	:= qt_dias_mes_mensalidade_w - qt_dias_adesao_w+1;
qt_dias_antecipado_w	:= qt_dias_mes_mensalidade_w - qt_dias_pro_rata_w;

open C01;
loop
fetch C01 into	
	nr_seq_resumo_w,
	nr_seq_segurado_simul_w;
exit when C01%notfound;
	begin
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_item_resumo_w,
		nr_seq_ordem_w,
		vl_resumo_w,
		ie_tipo_item_w;
	exit when C02%notfound;
		begin
		
		if	(ie_tipo_item_w is null) then
			if	(nr_seq_ordem_w = 2) then
				ie_tipo_item_w	:= '1';
			elsif	(nr_seq_ordem_w = 3) then
				ie_tipo_item_w	:= '14';
			elsif	(nr_seq_ordem_w	= 4) then
				ie_tipo_item_w	:= '15';
			end if;
		end if;
		
		select	count(*)
		into	qt_regra_pro_rata_w
		from	pls_regra_pro_rata_dia
		where	ie_tipo_item_mensalidade = ie_tipo_item_w;
		
		if	(qt_regra_pro_rata_w > 0) then
			vl_antecipacao_w 	:= dividir(vl_resumo_w,qt_dias_dividir_w) * qt_dias_antecipado_w;
			vl_pro_rata_dia_w	:= dividir(vl_resumo_w,qt_dias_dividir_w) * qt_dias_pro_rata_w;
			
			if	(vl_resumo_w <> (nvl(vl_pro_rata_dia_w,0) + nvl(vl_antecipacao_w,0))) then
				vl_antecipacao_w	:= vl_resumo_w - nvl(vl_pro_rata_dia_w,0);
			end if;
		else
			vl_antecipacao_w 	:= 0;
			vl_pro_rata_dia_w	:= 0;
		end if;
		
		update	pls_simulacao_resumo
		set	vl_pro_rata_dia	= vl_pro_rata_dia_w,
			vl_antecipacao	= vl_antecipacao_w
		where	nr_sequencia	= nr_seq_item_resumo_w;
		end;
	end loop;
	close C02;
	
	select	sum(vl_antecipacao),
		sum(vl_pro_rata_dia)
	into	vl_antecipacao_w,
		vl_pro_rata_dia_w
	from	pls_simulacao_resumo
	where	nr_seq_segurado_simul	= nr_seq_segurado_simul_w
	and	nr_seq_ordem	<> 1;
	
	update	pls_simulacao_resumo
	set	vl_antecipacao	= vl_antecipacao_w,
		vl_pro_rata_dia	= vl_pro_rata_dia_w
	where	nr_sequencia	= nr_seq_resumo_w;
	end;
end loop;
close C01;
	
commit;

end pls_gerar_pro_rata_simulacao;
/
