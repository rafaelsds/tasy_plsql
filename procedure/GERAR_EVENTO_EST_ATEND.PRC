create or replace
procedure gerar_evento_est_atend(
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				ie_opcao_p 		varchar2,
				nr_seq_unidade_p	number,
				ie_status_p		varchar2 default null) is

nr_seq_evento_w		number(10);
nr_sequencia_w		number(10);

pragma autonomous_transaction;

cursor c01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_evento_disp		= ie_opcao_p 
	and	nvl(ie_situacao,'A') = 'A';

begin

	open c01;
	loop
	fetch c01 into
		nr_seq_evento_w;
	exit when c01%notfound;
		gerar_evento_paciente(nr_seq_evento_w,null,null,null,nm_usuario_p,null,null,null,null,null,null,null,null,null,null,null,nr_seq_unidade_p);
	end loop;
	close c01;

commit;

end gerar_evento_est_atend;
/