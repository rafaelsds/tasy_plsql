create or replace
procedure ageint_resolver_pendencia 
		(nr_seq_ageint_item_p	number,
		nm_usuario_p		Varchar2) is 

begin

update	ageint_pendencia
set	ie_status_pendencia 	= 'R',
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	ie_status_pendencia 	= 'P'
and	nr_seq_ageint_item 	= nr_seq_ageint_item_p;

commit;

end ageint_resolver_pendencia;
/