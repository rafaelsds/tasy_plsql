create or replace
procedure ctb_pls_obter_conta
			(	cd_estabelecimento_p	in	Number,
				dt_referencia_p		in	Date,
				ie_debito_credito_p	in	Varchar2,
				ie_tipo_contratacao_p	in	Varchar2,
				ie_tipo_item_p		in	Varchar2,
				ie_preco_p		in	Varchar2,
				ie_segmentacao_p	in	Varchar2,
				ie_regulamentacao_p	in	Varchar2,
				ie_participacao_p	in	Varchar2,
				ie_tipo_beneficiario_p	in	Varchar2,
				ie_status_mensalidade_p	in	Varchar2,
				nr_seq_conta_p		in	Number,
				cd_procedimento_p	in 	Number,
				ie_origem_proced_p	in 	Number,
				nr_seq_clinica_p	in	Number,
				ie_tipo_segurado_p	in	Varchar2,
				nr_seq_material_p	in	Number,
				nr_seq_tipo_lanc_p	in	Varchar2,
				nr_seq_item_mensal_p	in	Varchar2,
				ds_parametro_tres_p	in	Varchar2,
				nr_seq_regra_p		out	Number,
				cd_historico_p		out	Number,
				cd_conta_contabil_p	out	Varchar2,
				cd_historico_baixa_p	out	Number) is

nr_seq_regra_w			Varchar2(20);
cd_conta_contabil_w		Varchar2(20);
cd_conta_estorno_w		Varchar2(20);
cd_historico_w			Number(10);
cd_hist_estorno_w		Number(10);
ie_tipo_guia_w			Varchar2(2);
nr_seq_tipo_atendimento_w	Number(10);
cd_area_procedimento_w		Number(15);
cd_especialidade_w		Number(15);
cd_grupo_proc_w			Number(15);
nr_seq_grupo_ans_w		Number(10);
ie_tipo_relacao_w		Varchar2(2);
cd_medico_executor_w		Varchar2(10);
ie_regime_internacao_w		Varchar2(1);
nr_seq_conselho_w		Number(10);
cd_historico_baixa_w		Number(10);
nr_seq_grupo_superior_w		Number(10);
ie_tipo_protocolo_w		Varchar2(3);
nr_seq_prestador_w		Number(10);
nr_seq_plano_w			Number(10);
nr_seq_sca_w			Number(10);
ie_tipo_vinculo_operadora_w	Varchar2(2);
nr_seq_bonificacao_w		Number(10);
nr_seq_pagador_fin_w		Number(10);
nr_seq_mensalidade_w		Number(10);
nr_seq_empresa_w		Number(10);
nr_seq_forma_cobranca_w		Number(10);

Cursor C01 is
	select	nr_sequencia,
		cd_conta_contabil,
		cd_conta_estorno,
		cd_historico,
		cd_historico_estorno,
		cd_historico_baixa
	from	pls_regra_ctb_mensal
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	dt_inicio_vigencia	<= dt_referencia_p
	and	dt_fim_vigencia		>= dt_referencia_p
	and	((ie_tipo_contratacao	= ie_tipo_contratacao_p) 	or (ie_tipo_contratacao is null))
	and	((ie_debito_credito	= ie_debito_credito_p) 		or (ie_debito_credito is null))
	and	((ie_preco		= ie_preco_p) 			or (ie_preco is null))
	and	((ie_segmentacao	= ie_segmentacao_p)		or (ie_segmentacao is null))
	and	((ie_regulamentacao	= ie_regulamentacao_p)		or (ie_regulamentacao is null))
	and	((ie_participacao	= ie_participacao_p) 		or (ie_participacao is null))
	and	((ie_tipo_beneficiario	= ie_tipo_beneficiario_p) 	or (ie_tipo_beneficiario is null))
	and	((ie_tipo_item		= ie_tipo_item_p) 		or (ie_tipo_item is null))
	and	((ie_tipo_relacao	= ie_tipo_relacao_w) 		or (ie_tipo_relacao is null))
	and	((ie_tipo_segurado	= ie_tipo_segurado_p) 		or (ie_tipo_segurado is null))
	and	((nr_seq_tipo_lancamento	= nr_seq_tipo_lanc_p) 	or (nr_seq_tipo_lancamento is null))
	and	((nr_seq_plano		= nr_seq_plano_w) 		or (nr_seq_plano is null))
	and	((nr_seq_sca		= nr_seq_sca_w) 		or (nr_seq_sca is null))
	and	((nr_seq_bonificacao	= nr_seq_bonificacao_w) 	or (nr_seq_bonificacao is null))
	and	((ie_tipo_vinculo_operadora	= ie_tipo_vinculo_operadora_w) or (ie_tipo_vinculo_operadora is null))
	and	((nr_seq_forma_cobranca	= nr_seq_forma_cobranca_w)	or (nr_seq_forma_cobranca is null))
	and	((nr_seq_empresa	= nr_seq_empresa_w)		or (nr_seq_empresa is null))
	and	(nvl(nr_seq_grupo_ans,nvl(nr_seq_grupo_superior_w,nvl(nr_seq_grupo_ans_w,0))) 	= nvl(nr_seq_grupo_superior_w,nvl(nr_seq_grupo_ans_w,0)))
	order by	nvl(nr_seq_plano,0),
			nvl(nr_seq_sca,0),
			nvl(nr_seq_bonificacao,0),
			nvl(nr_seq_empresa,0),
			nvl(ie_tipo_vinculo_operadora,' '),
			nvl(ie_tipo_item,'A'),
			nvl(ie_tipo_beneficiario,'A'),
			nvl(ie_regulamentacao,'A'),
			nvl(ie_preco,'A'),
			nvl(ie_participacao,'A'),
			nvl(ie_segmentacao,'A'),
			nvl(ie_debito_credito,'A'),
			nvl(ie_tipo_contratacao,'A'),
			nvl(nr_seq_tipo_lancamento,0),
			nvl(nr_seq_forma_cobranca,0),
			nvl(cd_conta_contabil,'A'),
			nvl(dt_inicio_vigencia,sysdate),
			nvl(dt_fim_vigencia,sysdate),
			nvl(ie_status_mensalidade,'A');

Cursor C03 is
	select	a.nr_seq_grupo_desp
	from	ans_grupo_desp_copartic a
	where	a.nr_seq_clinica	= nr_seq_clinica_p
	and	a.ie_liberado	= 'S';

begin
if 	(nvl(cd_procedimento_p,0) <> 0) then
	ie_tipo_relacao_w	:= '';
	
	select	a.ie_tipo_guia,
		a.nr_seq_tipo_atendimento,
		a.cd_medico_executor,
		a.ie_regime_internacao,
		b.ie_tipo_protocolo,
		b.nr_seq_prestador
	into	ie_tipo_guia_w,
		nr_seq_tipo_atendimento_w,
		cd_medico_executor_w,
		ie_regime_internacao_w,
		ie_tipo_protocolo_w,
		nr_seq_prestador_w
	from	pls_conta 		a,
		pls_protocolo_conta	b
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	a.nr_sequencia 		= nr_seq_conta_p;
	
	if	(ie_tipo_protocolo_w	= 'C') then
		select	max(ie_tipo_relacao)
		into	ie_tipo_relacao_w
		from	pls_prestador
		where	nr_sequencia	= nr_seq_prestador_w;
	end if;
	
	select	max(nr_seq_conselho)
	into	nr_seq_conselho_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_medico_executor_w;
	
	select	pls_obter_grupo_ans(cd_procedimento_p, ie_origem_proced_p, nr_seq_conselho_w, 
				nr_seq_tipo_atendimento_w, ie_tipo_guia_w, ie_regime_internacao_w,
				'', 'S', cd_estabelecimento_p)
	into	nr_seq_grupo_ans_w
	from	dual;
elsif 	(cd_procedimento_p = 0) and (nvl(nr_seq_clinica_p,0) <> 0) then
	open C03;
	loop
	fetch C03 into	
		nr_seq_grupo_ans_w;
	exit when C03%notfound;
	end loop;
	close C03;
end if;

if	(nvl(nr_seq_grupo_ans_w,0) > 0) then
	select	max(nr_seq_grupo_superior)
	into	nr_seq_grupo_superior_w
	from	ans_grupo_despesa
	where	nr_sequencia	= nr_seq_grupo_ans_w;
end if;

if	(ie_tipo_item_p = '1') then
	select	max(nr_seq_plano)
	into	nr_seq_plano_w
	from	pls_mensalidade_seg_item
	where	nr_sequencia	= nr_seq_item_mensal_p;
elsif	(ie_tipo_item_p = '15') then
	select	max(b.nr_seq_plano)
	into	nr_seq_sca_w
	from	pls_mensalidade_seg_item	a,
		pls_sca_vinculo			b
	where	a.nr_sequencia		= nr_seq_item_mensal_p
	and	a.nr_seq_vinculo_sca	= b.nr_sequencia;
elsif	(ie_tipo_item_p = '14') then
	select	max(b.nr_seq_bonificacao)
	into	nr_seq_bonificacao_w
	from	pls_mensalidade_seg_item	a,
		pls_bonificacao_vinculo		b
	where	a.nr_seq_bonificacao_vinculo	= b.nr_sequencia
	and	a.nr_sequencia			= nr_seq_item_mensal_p;
end if;

begin
select	c.ie_tipo_vinculo_operadora,
	b.nr_seq_mensalidade
into	ie_tipo_vinculo_operadora_w,
	nr_seq_mensalidade_w
from	pls_mensalidade_seg_item	a,
	pls_mensalidade_segurado	b,
	pls_segurado			c
where	a.nr_seq_mensalidade_seg = b.nr_sequencia
and	b.nr_seq_segurado	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_item_mensal_p;
exception
when others then
	ie_tipo_vinculo_operadora_w	:= null;
end;

begin
select	nr_seq_pagador_fin,
	nr_seq_forma_cobranca
into	nr_seq_pagador_fin_w,
	nr_seq_forma_cobranca_w
from	pls_mensalidade
where	nr_sequencia	= nr_seq_mensalidade_w;
exception
when others then
	nr_seq_pagador_fin_w	:= null;
end;

if	(nvl(nr_seq_pagador_fin_w,0) <> 0) then
	begin
	select	nr_seq_empresa,
		nvl(nr_seq_forma_cobranca_w,nr_seq_forma_cobranca)
	into	nr_seq_empresa_w,
		nr_seq_forma_cobranca_w
	from	pls_contrato_pagador_fin
	where	nr_sequencia	= nr_seq_pagador_fin_w;
	exception
	when others then
		nr_seq_empresa_w	:= null;
		nr_seq_forma_cobranca_w	:= null;
	end;
end if;

OPEN C01;
LOOP
FETCH C01 into
	nr_seq_regra_w,
	cd_conta_contabil_w,
	cd_conta_estorno_w,
	cd_historico_w,
	cd_hist_estorno_w,
	cd_historico_baixa_w;
exit when c01%notfound;
END LOOP;
CLOSE C01;

if 	(nvl(ie_status_mensalidade_p,'N') = 'E') then
	cd_conta_contabil_p	:= nvl(cd_conta_estorno_w,'0');
	nr_seq_regra_p		:= nvl(nr_seq_regra_w,0);
	cd_historico_p		:= nvl(cd_hist_estorno_w,0);
	cd_historico_baixa_p	:= cd_historico_baixa_w;
else
	cd_conta_contabil_p	:= nvl(cd_conta_contabil_w,'0');
	nr_seq_regra_p		:= nvl(nr_seq_regra_w,0);
	cd_historico_p		:= nvl(cd_historico_w,0);
	cd_historico_baixa_p	:= cd_historico_baixa_w;
end if;

--commit;

end ctb_pls_obter_conta;
/