create or replace
procedure consiste_atend_encaixe_js(ds_mensagem_p	out 	varchar2,
			cd_pessoa_fisica_p out	varchar2,
			nr_atendimento_p	number,
			nm_usuario_p		Varchar2,
			ie_consiste_paciente_p	varchar2,
			cd_agenda_p		number) is 
			
qt_registros_w	number(15);			

begin

select 	count(*) 
into	qt_registros_w
from 	atendimento_paciente 
where 	nr_atendimento = nr_atendimento_p;

if 	(qt_registros_w = 0) then
	ds_mensagem_p 	:= obter_texto_tasy(169327, wheb_usuario_pck.get_nr_seq_idioma);
elsif	(ie_consiste_paciente_p = 'S') then
	consiste_atend_agenda_paciente(cd_agenda_p,nr_atendimento_p);
end if;

if (qt_registros_w > 0) then
	cd_pessoa_fisica_p	:= substr(obter_pessoa_atendimento(nr_atendimento_p,'C'),1,10);
end if;
commit;

end consiste_atend_encaixe_js;
/
