create or replace
procedure pls_valida_formulario_benef
			(	nr_carteira_p		varchar2,
				dt_nasc_p		varchar2,
				nr_cpf_p		varchar2,
				nr_rg_p			varchar2,
				ds_email_p		varchar2,
				ds_retorno_p	out	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Validar os campos, comparando os que est�o cadastrados na base, com os dados informados pelo benefici�rio, no cadastro do login.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ x ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_retorno_w			varchar2(255) := 'S';
nr_seq_segurado_w		number(10);
dt_nascimento_w			date;
nr_cpf_w			varchar2(11);
nr_rg_w				varchar2(15);
nr_rg_estrang_w			varchar2(30);
ds_email_w			varchar2(60);
ie_brasileiro_w			varchar2(1);
nm_pessoa_fisica_w		varchar2(60);
cd_nacionalidade_w		varchar2(8);
nr_carteira_w			varchar2(30);
cd_pessoa_fisica_w		varchar(10);
nr_seq_compl_pf_w		number(3);
nr_sequencia_w			number(3);
qt_idade_w			number(3);
nm_segurado_w			varchar2(15);

/* Retorna a mensagem com o erro ocorrido ou S quando est� tudo ok.
Na mensagem de erro cont�m a mensagem e um c�digo para definir o campo que deve receber o focus.
1 - Carteira
2 - Email
3 - Data de nascimento
4 - CPF
5 - RG

Obs: Nome n�o ser� mais utilizado no portal.
*/

begin

nr_carteira_w := elimina_caractere_especial(nr_carteira_p);

nr_seq_segurado_w := pls_obter_dados_carteira(null, nr_carteira_w, 'S');

if	(nr_seq_segurado_w is null) then
	ds_retorno_w := 'Carteira n�o encontrada#1';
else
	select	a.dt_nascimento,
		obter_idade_pf(a.cd_pessoa_fisica,sysdate,'A') qt_idade,
		a.nr_cpf,
		a.nr_identidade,
		a.nr_reg_geral_estrang,
		obter_compl_pf(a.cd_pessoa_fisica, 1, 'M'),
		a.nm_pessoa_pesquisa,
		a.cd_pessoa_fisica,
		a.cd_nacionalidade,
		substr(tws_get_name_person(b.cd_pessoa_fisica, b.cd_estabelecimento),1,15) nm_segurado
	into	dt_nascimento_w,
		qt_idade_w,
		nr_cpf_w,
		nr_rg_w,
		nr_rg_estrang_w,
		ds_email_w,
		nm_pessoa_fisica_w,
		cd_pessoa_fisica_w,
		cd_nacionalidade_w,
		nm_segurado_w
	from	pessoa_fisica	a,
		pls_segurado	b
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	b.nr_sequencia		= nr_seq_segurado_w;
	
	if	(dt_nascimento_w is null and dt_nasc_p is not null) then
		ds_retorno_w	:= 'Data de nascimento n�o cadastrada. Entre em contato com a Operadora para realizar o cadastro.#3';
	elsif	(trunc(dt_nascimento_w) <> to_date(dt_nasc_p, 'dd/mm/yyyy')) then
		ds_retorno_w 	:= 'Data de nascimento n�o confere#3';
	end if;
	
	if	(nr_cpf_p is not null) then
		
		if	(nr_cpf_w is null) then
			ds_retorno_w 	:= 'CPF n�o cadastrado. Entre em contato com a Operadora para realizar o cadastro.#4';
		elsif	(nr_cpf_p <> nr_cpf_w) then
			ds_retorno_w	:= 'CPF n�o confere#4';
		end if;
	else
		select	max(ie_brasileiro)
		into	ie_brasileiro_w
		from	nacionalidade
		where	cd_nacionalidade = cd_nacionalidade_w;
		
		if	(ie_brasileiro_w = 'N') then
			if	(nr_rg_estrang_w is null) then
				ds_retorno_w 	:= 'RG Estrangeiro n�o cadastrado. Entre em contato com a Operadora para realizar o cadastro.#5';
			elsif	(nr_rg_estrang_w != nr_rg_p) then
				ds_retorno_w	:= 'RG Estrangeiro n�o confere#5';
			end if;
		elsif	(ie_brasileiro_w = 'S') then
			if	(nr_rg_w is null and qt_idade_w >=18) then
				ds_retorno_w	:= 'RG n�o cadastrado. Entre em contato com a Operadora para realizar o cadastro.#5';
			elsif	(nr_rg_w != nr_rg_p) then
				ds_retorno_w	:= 'RG n�o confere#5';
			end if;
		elsif	(nr_rg_w is not null) and
			(nr_rg_w != nr_rg_p) then
			ds_retorno_w	:= 'RG n�o confere#5';
		elsif	(nr_rg_estrang_w is not null) and
			(nr_rg_estrang_w != nr_rg_p) then
			ds_retorno_w	:= 'RG Estrangeiro n�o confere#5';
		end if;
	end if;
	
	if	(ds_retorno_w = 'S') then
		select	max(nr_sequencia)
		into	nr_seq_compl_pf_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w
		and	ie_tipo_complemento	= 1;
		
		if	((nr_seq_compl_pf_w is not null and nr_seq_compl_pf_w > 0) and (ds_email_w != ds_email_p or ds_email_w is null)) then
			update	compl_pessoa_fisica 
			set	ds_email 		= ds_email_p,
				dt_atualizacao 		= sysdate,
				nm_usuario		= nm_segurado_w
			where	cd_pessoa_fisica 	= cd_pessoa_fisica_w
			and	ie_tipo_complemento	= 1;
			commit;
		elsif	(nr_seq_compl_pf_w is null) then 
			select	max(nr_sequencia)
			into	nr_sequencia_w
			from	compl_pessoa_fisica;
			nr_sequencia_w := nr_sequencia_w + 1;
			insert	into	compl_pessoa_fisica 
					(nr_sequencia, cd_pessoa_fisica, ie_tipo_complemento,
					ds_email, dt_atualizacao, nm_usuario)
			values		(nr_sequencia_w, cd_pessoa_fisica_w, 1,
					ds_email_p, sysdate, nm_segurado_w);
			commit;
		end if;
	end if;
end if;

ds_retorno_p := ds_retorno_w;

end pls_valida_formulario_benef;
/
