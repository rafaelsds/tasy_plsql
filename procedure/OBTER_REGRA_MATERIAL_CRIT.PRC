CREATE OR REPLACE PROCEDURE OBTER_REGRA_MATERIAL_CRIT(
                         		CD_ESTABELECIMENTO_P   	NUMBER,
					CD_CONVENIO_P		NUMBER,
					CD_CATEGORIA_P		VARCHAR2,
					cd_material_P		NUMBER,
					DT_atendimento_P	DATE,
					IE_TIPO_ATENDIMENTO_P	NUMBER,
					CD_SETOR_ATENDIMENTO_P	NUMBER,
					IE_TIPO_SERVICO_SUS_P	NUMBER,
					IE_TIPO_ATO_SUS_P	NUMBER,
					CD_MEDICO_EXECUTOR_P	VARCHAR2,
					CD_PESSOA_JURIDICA_P	VARCHAR2,
					CD_CGC_FORNECEDOR_P	VARCHAR2,
					IE_VALOR_AUTORIZADO_p	VARCHAR2,
					CD_REGRA_P		OUT	VARCHAR2,
					IE_ENTRA_CONTA_P	OUT	VARCHAR2,
					IE_CALCULA_VALOR_P	OUT	VARCHAR2,
					CD_CGC_P		OUT	VARCHAR2,
					CD_PESSOA_FISICA_P	OUT	VARCHAR2,
					nr_seq_criterio_p	OUT	Number) IS

cd_edicao_amb_w				number(6)		:= 0;
cd_grupo_w				number(15)		:= 0;
cd_subgrupo_w				number(15)		:= 0;
cd_classe_w				number(15)		:= 0;
ie_credenciado_w			varchar2(1);
ie_funcionario_w			varchar2(1);
ie_corpo_clinico_w			varchar2(1);
cd_especial_medica_w			number(5)		:= 0;
cd_regra_w				varchar2(5)		:= '';
cd_regra_valida_w			varchar2(5)		:= '';
nr_sequencia_w         			number(4)		:= 0;
ie_especialidade_w			number(1)		:= 0;
ie_entra_conta_w			varchar2(1)		:= 'S';
ie_calcula_valor_w			varchar2(1)		:= 'S';
cd_cgc_w				varchar2(14);
cd_pessoa_fisica_w			varchar2(10);
cd_medico_executor_w			varchar2(10);
cd_pessoa_juridica_w			varchar2(14);
nr_seq_criterio_w			Number(10,0);
nr_seq_criterio_valido_w		Number(10,0);
ie_tipo_convenio_w			number(02,0);
cd_cgc_fornecedor_w			varchar2(14);
nr_seq_familia_w			number(10,0);

CURSOR C01 IS
	select 		a.cd_especialidade_medica,
			a.cd_regra,
			b.ie_entra_conta,
			b.ie_calcula_valor,
			b.cd_cgc,
			b.cd_pessoa_fisica,
			a.nr_sequencia
      from 		regra_material_criterio a,
			regra_honorario b
	where		a.cd_regra						= b.cd_regra
	and		b.ie_situacao						= 'A' 
        and 		a.cd_estabelecimento      				= cd_estabelecimento_p
	and		nvl(a.cd_material,cd_material_p) 			= cd_material_p
	and		nvl(a.CD_GRUPO_MATERIAL,cd_grupo_w)			= cd_grupo_w	
	and		nvl(a.CD_SUBGRUPO_MATERIAL,CD_SUBGRUPO_w)		= CD_SUBGRUPO_w
	and		nvl(a.cd_classe_material,cd_classe_w)			= cd_classe_w
	and 		nvl(a.nr_seq_familia, nvl(nr_seq_familia_w,0))		= nvl(nr_seq_familia_w,0)
	--and		nvl(a.cd_edicao_amb,cd_edicao_amb_w)			= cd_edicao_amb_w
	and		nvl(a.cd_convenio, cd_convenio_p)			= cd_convenio_p
	and		nvl(a.ie_tipo_convenio, nvl(ie_tipo_convenio_w, 0))	= nvl(ie_tipo_convenio_w, 0)
	and		nvl(a.cd_categoria, nvl(cd_categoria_p, '0'))		= nvl(cd_categoria_p, '0')
        and 		nvl(a.ie_tipo_atendimento,ie_tipo_atendimento_p)	= ie_tipo_atendimento_p
	and		nvl(a.cd_setor_atendimento, cd_setor_atendimento_p)	= cd_setor_atendimento_p
	and		nvl(a.cd_medico, cd_medico_executor_w)			= cd_medico_executor_w
	and		nvl(a.cd_cgc, cd_pessoa_juridica_w)			= cd_pessoa_juridica_w
	and		((a.ie_credenciado					= 'T') or
			 (a.ie_credenciado 					= ie_credenciado_w))
	and		((a.ie_funcionario					= 'T') or
			 (a.ie_funcionario 					= ie_funcionario_w))
	and		((a.ie_corpo_clinico					= 'T') or
			 (a.ie_corpo_clinico					= ie_corpo_clinico_w))
	and		((nvl(a.ie_valor_autorizado,'A') = 'A') or (a.ie_valor_autorizado = nvl(ie_valor_autorizado_p,'N')))
	and		nvl(a.ie_tipo_servico_sus, nvl(ie_tipo_servico_sus_p,0))= nvl(ie_tipo_servico_sus_p,0)
	and		nvl(a.ie_tipo_ato_sus, nvl(ie_tipo_ato_sus_p,0))	= nvl(ie_tipo_ato_sus_p,0)
	and		nvl(a.cd_cgc_fornecedor, cd_cgc_fornecedor_w)		= cd_cgc_fornecedor_w
	and		ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(DT_atendimento_P) between ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(nvl(dt_inicio_vigencia, DT_atendimento_P)) and
					ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(nvl(dt_final_vigencia,DT_atendimento_P))
	order by	nvl(a.cd_medico,0),
			nvl(a.cd_cgc, 0),
			nvl(a.cd_setor_atendimento,0),
			nvl(a.cd_material,0),
			nvl(a.nr_seq_familia,0),
			nvl(a.cd_classe_material,0),
			nvl(a.cd_subgrupo_material,0),
			nvl(a.cd_grupo_material,0),
			nvl(a.cd_convenio,0),
			nvl(a.ie_tipo_atendimento,0),
			a.ie_credenciado desc,
			nvl(a.cd_especialidade_medica,0),
			nvl(a.cd_edicao_amb,0),
			nvl(a.ie_tipo_servico_sus, 0),
			nvl(a.ie_tipo_ato_sus, 0),
			nvl(a.cd_cgc_fornecedor,'0'),
			nvl(a.ie_valor_autorizado,'A');
BEGIN

cd_regra_valida_w	:= '';
cd_medico_executor_w	:= nvl(cd_medico_executor_p,'0');
cd_pessoa_juridica_w	:= nvl(cd_pessoa_juridica_p,'0');
cd_cgc_fornecedor_w	:= nvl(cd_cgc_fornecedor_p,'0');

select	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio	= cd_convenio_p;

/* Obter Estrutura do procedimento */
begin
select 		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		nr_seq_familia
into		cd_grupo_w,
		cd_subgrupo_w,
		cd_classe_w,
		nr_seq_familia_w
from		Estrutura_material_V
where		cd_material	 	= cd_material_p;
exception
     		when others then
		begin
		cd_grupo_w	:= 0;
		cd_subgrupo_w	:= 0;
		cd_classe_w	:= 0;
		nr_seq_familia_w:= 0;
		end;
end;

/*      Obter edicao da AMB  */
begin
select	cd_edicao_amb
into		cd_edicao_amb_w
from		convenio_amb
where		cd_estabelecimento	= cd_estabelecimento_p
and		cd_convenio			= cd_convenio_p
and		cd_categoria		= cd_categoria_p
and 		nvl(ie_situacao,'A')	= 'A'
and		dt_inicio_vigencia	=
		(select max(dt_inicio_vigencia)
			from		convenio_amb a
			where		a.cd_estabelecimento	= cd_estabelecimento_p
			and		a.cd_convenio		= cd_convenio_p
			and		a.cd_categoria		= cd_categoria_p
		      and 		nvl(ie_situacao,'A')	= 'A'
			and		a.dt_inicio_vigencia 	<= dt_atendimento_p);
exception
		when others then
     		cd_edicao_amb_w 	:= 0;
end;

/*      Obter indicador de medico credenciado  */
begin
select		nvl(ie_conveniado,'N')
into		ie_credenciado_w
from		medico_convenio
where		cd_pessoa_fisica		= cd_medico_executor_p
and		cd_convenio			= cd_convenio_p;
exception
		when others then
     		ie_credenciado_w 	:= 'N';
end;


/*      Obter indicador de medico do corpo clinico */
select	nvl(max(ie_corpo_clinico),'N'),
	decode(nvl(max(ie_vinculo_medico),'99'),'1','S','N')
into	ie_corpo_clinico_w,
	ie_funcionario_w
from	Medico
where	cd_pessoa_fisica	= cd_medico_executor_p;

OPEN C01;
LOOP
FETCH C01 into
		cd_especial_medica_w,
		cd_regra_w,
		ie_entra_conta_w,
		ie_calcula_valor_w,
		cd_cgc_w,
		cd_pessoa_fisica_w,
		nr_seq_criterio_w;
	exit when c01%notfound;
	BEGIN
	if	(cd_especial_medica_w is null) then
		cd_regra_valida_w		:= cd_regra_w;
		nr_seq_criterio_valido_w	:= nr_seq_criterio_w;	-- Edgar 14/04/2006, os 30827, coloquei tratamento de nr_seq_criterio_valido_w
	else
		begin
		select 	cd_regra_w,
			nr_seq_criterio_w
		into	cd_regra_valida_w,
			nr_seq_criterio_valido_w
		from	medico_especialidade
		where	cd_pessoa_fisica	= cd_medico_executor_p
		and	cd_especialidade	= cd_especial_medica_w;
		exception
			when others then
    				cd_regra_valida_w		:= cd_regra_valida_w;
				nr_seq_criterio_valido_w	:= nr_seq_criterio_valido_w;
		end;
	end if;
	END;
END LOOP;
CLOSE C01;

cd_regra_p			:= cd_regra_valida_w;
nr_seq_criterio_p		:= nr_seq_criterio_valido_w;

begin
select 	nvl(ie_entra_conta,'S'),
	nvl(ie_calcula_valor,'S'),
	cd_cgc,
	cd_pessoa_fisica
into
	ie_entra_conta_w,
	ie_calcula_valor_w,
	cd_cgc_w,
	cd_pessoa_fisica_w
from	regra_honorario
where	cd_regra		= cd_regra_valida_w;
exception
	when others then
		begin
		ie_entra_conta_w	:= 'S';
		ie_calcula_valor_w	:= 'S';
		end;	
end;	

ie_entra_conta_p		:= ie_entra_conta_w;
ie_calcula_valor_p		:= ie_calcula_valor_w;
cd_cgc_p			:= cd_cgc_w;
cd_pessoa_fisica_p		:= cd_pessoa_fisica_w;
--nr_seq_criterio_p		:= nr_seq_criterio_w;	-- Edgar 14/04/2006, os 30827, tirie pois coloquei tratamento de nr_seq_criterio_valido_w

END OBTER_REGRA_MATERIAL_CRIT;
/