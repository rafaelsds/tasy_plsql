create or replace
procedure alterar_resp_credito_partic(	cd_pessoa_fisica_p		varchar2,
					nr_cirurgia_p			number,
					ie_responsavel_credito_p	varchar2,
					nm_usuario_p			Varchar2,
					cd_estabelecimento_p		number) is 


nr_seq_participante_w	number(10);
nr_seq_procedimento_w	number(10);					

Cursor C01 is
	SELECT 			nr_seq_partic,
				nr_seq_procedimento
	FROM			cirurgia_participante b,
				procedimento_participante a
	WHERE			b.nr_seq_procedimento		= a.nr_sequencia
	and			b.cd_pessoa_fisica		= cd_pessoa_fisica_p
	AND			b.nr_cirurgia			= nr_cirurgia_p;

	
begin

if 	(cd_pessoa_fisica_p is not null) and
	(nr_cirurgia_p	is not null) and 
	(ie_responsavel_credito_p is not null) then
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_participante_w,
		nr_seq_procedimento_w;
	exit when C01%notfound;
		begin	
		Atualizar_Resp_Credito_Conta(nr_seq_procedimento_w,nr_seq_participante_w,cd_estabelecimento_p,ie_responsavel_credito_p, 'S',nm_usuario_p);		
		end;
	end loop;
	close C01;
	
end if;


commit;


end Alterar_resp_credito_partic;
/