create or replace
procedure DesfazerFechamentoAlta(nr_atendimento_p	number) is 

begin

delete	atendimento_historico
where	nr_atendimento = nr_atendimento_p
and	cd_tipo_historico = 2;

commit;

end DesfazerFechamentoAlta;
/
