CREATE OR REPLACE
PROCEDURE Atualiza_referencia_convenio
                             	(DT_PARAMETRO_P		DATE) IS
dt_atualizacao_w			date		:= sysdate;
cd_convenio_w			number(5);
dt_dia_vcto_w			number(2);
dt_ref_valida_w			date;
qt_dias_tolerancia_w		number(3);
dt_virada_w				date;
qt_dia_w				number(2);
ie_calendario_w			Varchar2(1);
qt_reg_tipo_protocolo_w		number(10,0);
qt_regra_w			number(10,0);
ie_tipo_protocolo_w		varchar2(03);
ie_regra_dia_w			varchar2(01);
ie_dia_w			varchar2(01);
dt_dia_entrega_w		number(10);
dt_entrega_w			date;
cd_estabelecimento_w		number(4,0);

CURSOR C01 IS
	select	a.cd_convenio,
             	nvl(decode(nvl(a.ie_venc_ultimo_dia,'N'),'S',
		PKG_DATE_UTILS.extract_field('DAY', PKG_DATE_UTILS.END_OF(dt_parametro_p,'MONTH', 0)),a.dt_dia_vencimento), 
		PKG_DATE_UTILS.extract_field('DAY', dt_parametro_p) + 1),
             	PKG_DATE_UTILS.start_of(nvl(a.dt_ref_valida, PKG_DATE_UTILS.ADD_MONTH(sysdate,-1,0)),'dd',0),
			nvl(a.qt_dias_tolerancia,0)
	from		convenio a
	where 	a.ie_situacao	= 'A'
	and		trunc(dt_parametro_p,'mi') > PKG_DATE_UTILS.start_of((nvl(a.dt_ref_valida, PKG_DATE_UTILS.ADD_MONTH(sysdate,-1,0)) + 
			(nvl(a.qt_dias_tolerancia,0) + 1)), 'dd', 0);
			
			
CURSOR C03 IS
	select	a.cd_convenio,
             	a.dt_dia_entrega, 
             	PKG_DATE_UTILS.start_of(nvl(a.dt_entrega_prot, sysdate - 30), 'dd', 0)
	from		convenio a
	where 	a.ie_situacao	= 'A'
	and	DT_DIA_ENTREGA is not null
	and	dt_parametro_p + 1/1440 > (nvl(a.dt_entrega_prot, sysdate - 30) + nvl(a.qt_dias_tol_entrega,0)); 

cursor	c02 is
	select	vl_dominio /* Tipo de protocolo */
	from	valor_dominio
	where	cd_dominio	= 73;

BEGIN

select 	nvl(max(wheb_usuario_pck.get_cd_estabelecimento),1)
into	cd_estabelecimento_w
from 	dual;

OPEN C01;
LOOP
	FETCH C01 into 
		cd_convenio_w,
		dt_dia_vcto_w,
		dt_ref_valida_w,
		qt_dias_tolerancia_w;
      EXIT WHEN C01%NOTFOUND;
      BEGIN

	begin
	dt_virada_w := null;
	select 	min(a.dt_fechamento),
		min(a.ie_regra_dia)
	into	dt_virada_w,
		ie_regra_dia_w
	from	convenio_fechamento a
	where	a.cd_convenio		= cd_convenio_w
	and	a.dt_fechamento		> dt_parametro_p
	and 	nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
	and	a.ie_tipo_protocolo	is null;
	exception
    		when others then
      		dt_virada_w := null;
	end;

	if	(dt_virada_w	is null)	then
		begin
		begin
		select 	min(qt_dia),
			min(a.ie_regra_dia)
		into	qt_dia_w,
			ie_regra_dia_w
		from	convenio_fechamento a
		where	a.cd_convenio	= cd_convenio_w
		and	nvl(a.qt_dia,0)	> PKG_DATE_UTILS.extract_field('DAY', dt_parametro_p)
		and 	nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
		and	a.ie_tipo_protocolo	is null;
		exception
			when others then
				qt_dia_w := null;
		end;
		if 	(qt_dia_w is null) then	
			begin
			select 	min(qt_dia),
				min(ie_regra_dia)
			into	qt_dia_w,
				ie_regra_dia_w
			from	convenio_fechamento a
			where	a.cd_convenio	= cd_convenio_w
			and	a.qt_dia		is not null
			and 	nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
			and	a.ie_tipo_protocolo	is null;
			exception
				when others then
    					qt_dia_w := null;
			end;
		end if;
		dt_dia_vcto_w	:= nvl(qt_dia_w,dt_dia_vcto_w);
		if	(dt_dia_vcto_w is null) or
			(dt_dia_vcto_w < 1) or
			(dt_dia_vcto_w > 31) then
			dt_dia_vcto_w	:= 31;
		end if;
		end;
	end if;

	if	(dt_virada_w is null) then
		begin
		if	(dt_dia_vcto_w <= PKG_DATE_UTILS.extract_field('DAY', dt_parametro_p)) then
			dt_virada_w       := PKG_DATE_UTILS.ADD_MONTH(dt_parametro_p,1,0);
		else
			dt_virada_w       := dt_parametro_p;
      		end if;

		if  	(dt_dia_vcto_w = 31) then
				dt_virada_w := PKG_DATE_UTILS.END_OF(dt_virada_w, 'MONTH', 0);
      elsif  	(dt_dia_vcto_w > PKG_DATE_UTILS.extract_field('DAY', PKG_DATE_UTILS.END_OF(dt_virada_w, 'MONTH', 0))) and
					(PKG_DATE_UTILS.extract_field('MONTH', dt_virada_w) = 2) then
					dt_virada_w := PKG_DATE_UTILS.END_OF(dt_virada_w, 'MONTH', 0);
		else
		        	dt_virada_w := PKG_DATE_UTILS.get_Date(PKG_DATE_UTILS.extract_field('YEAR', dt_virada_w), PKG_DATE_UTILS.extract_field('MONTH', dt_virada_w), dt_dia_vcto_w);
		end if;
		end;
	end if;

	select	pkg_date_utils.get_WeekDay(dt_virada_w) 
	into	ie_dia_w
	from	dual;
			
			
	if	(ie_regra_dia_w	= 'A') then
		begin
		if	(ie_dia_w = '1') then
			dt_virada_w	:= dt_virada_w - 2;
		elsif (ie_dia_w = '7') then
			dt_virada_w	:= dt_virada_w - 1;
		end if;
		end;
	elsif 	(ie_regra_dia_w	= 'P') then
		begin
		if	(ie_dia_w = '1') then
			dt_virada_w	:= dt_virada_w + 1;
		elsif (ie_dia_w = '7') then
			dt_virada_w	:= dt_virada_w + 2;
		end if;
		end;
	end if;
	
	update 	convenio
	set		dt_ref_valida = PKG_DATE_UTILS.start_of(dt_virada_w, 'dd', 0)
	where		cd_convenio	= cd_convenio_w;
	commit;

	/* Inicio da rotina para tratamento de regras por tipo de protocolo */
	open	c02;
	loop
	fetch	c02 into 
		ie_tipo_protocolo_w;
		exit when c02%notfound;
		begin

		select	count(*)
		into	qt_reg_tipo_protocolo_w
		from	convenio_fechamento a
		where	a.cd_convenio		= cd_convenio_w
		and 	nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
		and	a.ie_tipo_protocolo	= ie_tipo_protocolo_w;	

		if	(qt_reg_tipo_protocolo_w > 0) then
			begin
			dt_virada_w := null;
			select 		min(a.dt_fechamento),
					min(a.ie_regra_dia)
			into		dt_virada_w,
					ie_regra_dia_w
			from		convenio_fechamento a
			where		a.cd_convenio		= cd_convenio_w
			and		a.dt_fechamento		> dt_parametro_p
			and 		nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
			and		a.ie_tipo_protocolo	= ie_tipo_protocolo_w;
			exception
    				when others then
	      			dt_virada_w := null;
			end;

			if	(dt_virada_w	is null)	then
				begin
				begin
				select 	min(qt_dia),
					min(ie_regra_dia)
				into		qt_dia_w,
						ie_regra_dia_w
				from		convenio_fechamento a
				where		a.cd_convenio	= cd_convenio_w
				and		nvl(a.qt_dia,0)	> PKG_DATE_UTILS.extract_field('DAY', dt_parametro_p)
				and 		nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
				and		a.ie_tipo_protocolo	= ie_tipo_protocolo_w;
				exception
					when others then
						qt_dia_w := null;
				end;
				if 	(qt_dia_w is null) then	
					begin
					select 	min(qt_dia),
						min(ie_regra_dia)
					into		qt_dia_w,
							ie_regra_dia_w
					from		convenio_fechamento a
					where		a.cd_convenio	= cd_convenio_w
					and		a.qt_dia		is not null
					and 		nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
					and		a.ie_tipo_protocolo	= ie_tipo_protocolo_w;
					exception
					when others then
    							qt_dia_w := null;
					end;
				end if;
				dt_dia_vcto_w	:= nvl(qt_dia_w,dt_dia_vcto_w);
				if	(dt_dia_vcto_w is null) or
					(dt_dia_vcto_w < 1) or
					(dt_dia_vcto_w > 31) then
					dt_dia_vcto_w	:= 31;
				end if;
				end;
			end if;
	
			if	(dt_virada_w is null) then
				begin
				if	(dt_dia_vcto_w <= PKG_DATE_UTILS.extract_field('DAY', dt_parametro_p)) then
					dt_virada_w       := PKG_DATE_UTILS.ADD_MONTH(dt_parametro_p,1,0);
				else
					dt_virada_w       := dt_parametro_p;
	      	end if;
	
				if  	(dt_dia_vcto_w = 31) then
						dt_virada_w := PKG_DATE_UTILS.END_OF(dt_virada_w, 'MONTH', 0);
	      	elsif  	(dt_dia_vcto_w > PKG_DATE_UTILS.extract_field('DAY', PKG_DATE_UTILS.END_OF(dt_virada_w, 'MONTH', 0))) and
							(PKG_DATE_UTILS.extract_field('MONTH', dt_virada_w) = 2) then
							dt_virada_w := PKG_DATE_UTILS.END_OF(dt_virada_w, 'MONTH', 0);
				else
				      	dt_virada_w := PKG_DATE_UTILS.get_Date(PKG_DATE_UTILS.extract_field('YEAR', dt_virada_w), PKG_DATE_UTILS.extract_field('MONTH', dt_virada_w), dt_dia_vcto_w);
				end if;	
				end;
			end if;

		select	pkg_date_utils.get_WeekDay(dt_virada_w) 
		into	ie_dia_w
		from	dual;
			
			
		if	(ie_regra_dia_w	= 'A') then
			begin
			if	(ie_dia_w = '1') then
				dt_virada_w	:= dt_virada_w - 2;
			elsif (ie_dia_w = '7') then
				dt_virada_w	:= dt_virada_w - 1;
			end if;
			end;
		elsif 	(ie_regra_dia_w	= 'P') then
			begin
			if	(ie_dia_w = '1') then
				dt_virada_w	:= dt_virada_w + 1;
			elsif (ie_dia_w = '7') then
				dt_virada_w	:= dt_virada_w + 2;
			end if;
			end;
		end if;
			
		select	count(*)
		into	qt_regra_w
		from	conv_fechamento_tipo_prot
		where	cd_convenio		= cd_convenio_w
		and	ie_tipo_protocolo	= ie_tipo_protocolo_w;

		if	(qt_regra_w > 0) then
			update 	conv_fechamento_tipo_prot
			set	dt_ref_valida		= PKG_DATE_UTILS.start_of(dt_virada_w, 'dd', 0)
			where	cd_convenio		= cd_convenio_w
			and	ie_tipo_protocolo	= ie_tipo_protocolo_w;
		else
			insert into conv_fechamento_tipo_prot
				(NR_SEQUENCIA          ,
				CD_CONVENIO            ,
				IE_TIPO_PROTOCOLO      ,
				DT_REF_VALIDA          ,
				DT_ATUALIZACAO         ,
				NM_USUARIO             ,
				DT_ATUALIZACAO_NREC    ,
				NM_USUARIO_NREC        )
			values
				(conv_fechamento_tipo_prot_seq.nextval,
				cd_convenio_w,
				ie_tipo_protocolo_w,
				PKG_DATE_UTILS.start_of(dt_virada_w, 'dd', 0),
				sysdate,
				'Job',
				sysdate,
				'Job');
		end if;


		end if;

		commit;

		end;
		end loop;
		close c02;
	end;
END LOOP;
CLOSE C01;

/*Rotina de atualiza��o da Data de Entrega dos protocolos*/

open C03;
loop
fetch C03 into	
	cd_convenio_w,
	dt_dia_entrega_w,
	dt_entrega_w;
exit when C03%notfound;
	begin
	cd_convenio_w:=cd_convenio_w;
	ie_regra_dia_w:= 'M';
		
	/* Inicio Rotina para conv�nio com mais de 1 data de entrega no m�s*/
	dt_virada_w := null;
	begin
	select 	min(a.dt_entrega),
		min(a.ie_regra_dia)
	into	dt_virada_w,
		ie_regra_dia_w
	from	convenio_regra_entrega a
	where	a.cd_convenio		= cd_convenio_w
	and 	nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
	and	a.dt_entrega		> dt_parametro_p;
	exception
    		when others then
      		dt_virada_w := null;
	end;

	if	(dt_virada_w	is null)	then
		begin
		begin
		select 	min(qt_dia),
			min(a.ie_regra_dia)
		into	qt_dia_w,
			ie_regra_dia_w
		from	convenio_regra_entrega a
		where	a.cd_convenio	= cd_convenio_w
		and 	nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
		and	nvl(a.qt_dia,0)	> PKG_DATE_UTILS.extract_field('DAY', dt_parametro_p);
		exception
			when others then
				qt_dia_w := null;
		end;
		if 	(qt_dia_w is null) then	
			begin
			select 	min(qt_dia),
				min(a.ie_regra_dia)
			into	qt_dia_w,
				ie_regra_dia_w
			from	convenio_regra_entrega a
			where	a.cd_convenio	= cd_convenio_w
			and 	nvl(a.cd_estabelecimento, cd_estabelecimento_w) = cd_estabelecimento_w
			and	a.qt_dia is not null;
			exception
				when others then
    					qt_dia_w := null;
			end;
		end if;
		dt_dia_entrega_w:= nvl(qt_dia_w,dt_dia_entrega_w);
		if	(dt_dia_entrega_w is null) or
			(dt_dia_entrega_w < 1) or
			(dt_dia_entrega_w > 31) then
			dt_dia_entrega_w:= 31;
		end if;
		end;
	end if;
	/* Fim Rotina para conv�nio com mais de 1 data de entrega no m�s*/
	

	--dt_virada_w	:=  null;
	if	(dt_virada_w is null) then
		begin
		if	(dt_dia_entrega_w <= PKG_DATE_UTILS.extract_field('DAY', dt_parametro_p)) then
			dt_virada_w       := PKG_DATE_UTILS.ADD_MONTH(dt_parametro_p,1,0);
		else
			dt_virada_w       := dt_parametro_p;
      		end if;

		if  	(dt_dia_entrega_w = 31) then
				dt_virada_w := PKG_DATE_UTILS.END_OF(dt_virada_w, 'MONTH', 0);
		elsif  	(dt_dia_entrega_w > PKG_DATE_UTILS.extract_field('DAY', PKG_DATE_UTILS.END_OF(dt_virada_w, 'MONTH', 0))) and
					(PKG_DATE_UTILS.extract_field('MONTH', dt_virada_w) = 2) then
					dt_virada_w := PKG_DATE_UTILS.END_OF(dt_virada_w, 'MONTH', 0);
		else
		        	dt_virada_w := PKG_DATE_UTILS.get_Date(PKG_DATE_UTILS.extract_field('YEAR', dt_virada_w), PKG_DATE_UTILS.extract_field('MONTH', dt_virada_w), dt_dia_entrega_w);
		end if;
		end;
	end if;
	
	select	pkg_date_utils.get_WeekDay(dt_virada_w)
	into	ie_dia_w
	from	dual;			
			
	if	(nvl(ie_regra_dia_w,'M') = 'A') then
		begin
		if	(ie_dia_w = '1') then
			dt_virada_w	:= dt_virada_w - 2;
		elsif (ie_dia_w = '7') then
			dt_virada_w	:= dt_virada_w - 1;
		end if;
		end;
	elsif 	(nvl(ie_regra_dia_w,'M') = 'P') then
		begin
		if	(ie_dia_w = '1') then
			dt_virada_w	:= dt_virada_w + 1;
		elsif (ie_dia_w = '7') then
			dt_virada_w	:= dt_virada_w + 2;
		end if;
		end;
	end if;
	
	update	convenio
	set	dt_entrega_prot	= dt_virada_w
	where	cd_convenio	= cd_convenio_w;

	
	end;
end loop;
close C03;


COMMIT;
END Atualiza_referencia_convenio;
/
