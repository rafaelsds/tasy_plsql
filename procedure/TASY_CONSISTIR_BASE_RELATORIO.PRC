create or replace 
procedure tasy_consistir_base_relatorio is

table_name_w		varchar2(30);
column_name_w		varchar2(30);
data_type_w		varchar2(106);
data_precision_w	number;
data_scale_w		number;
qt_atributo_w		number(10, 0);
ds_comando_w		varchar2(1000);
nullable_w		varchar2(10)   := '';
data_length_w		number;
  
cursor c01 is
	select	distinct table_name
	from	user_tab_columns
	where	table_name in ('W_RELATORIO', 'W_BANDA_RELATORIO', 'W_BANDA_RELAT_CAMPO', 'W_RELATORIO_PARAMETRO', 'W_RELATORIO_FUNCAO', 'W_ETIQUETA', 'W_FUNCAO_REGRA_RELAT', 'W_RELATORIO_DOCUMENTACAO', 'W_USUARIO_RELATORIO', 'W_BANDA_RELAT_CAMPO_LONGO')
	order by table_name;

cursor c02 is
	select	a.column_name, 
		a.data_type, 
		a.data_precision,
		nvl (a.data_scale, 0), 
		decode (a.nullable, 'N', ' NOT NULL '),
		data_length
	from	user_tab_columns a
	where	not exists (
			select	1
			from	user_tab_columns b
			where	b.column_name	= a.column_name
			and	b.table_name	= substr (table_name_w, 3, length (table_name_w)))
			and	a.table_name	= table_name_w;
begin
open c01;
loop
fetch c01
	into table_name_w;
exit when c01%notfound;
	begin
	
	select	count (*)
	into	qt_atributo_w
	from	user_tab_columns a
	where	not exists (
			select	1
			from	user_tab_columns b
			where	b.column_name = a.column_name
			and	b.table_name = substr (table_name_w, 3, length (table_name_w)))
			and	a.table_name = table_name_w;
	if (qt_atributo_w > 0) then
		
		open c02;
            	loop
               	fetch c02
                	into	column_name_w, 
				data_type_w, 
				data_precision_w,
                     		data_scale_w, 
				nullable_w, 
				data_length_w;
		exit when c02%notfound;
			begin
		
			if 	(data_type_w = 'DATE') or 
				(data_type_w = 'LONG') then
                     		ds_comando_w := ' alter table '
						|| substr (table_name_w, 3, length (table_name_w))
						|| ' add '
						|| column_name_w
						|| ' '
						|| data_type_w
						|| nullable_w;
			elsif	(data_scale_w > 0) then
				ds_comando_w := ' alter table '
						|| substr (table_name_w, 3, length (table_name_w))
						|| ' add '
						|| column_name_w
						|| ' '
						|| data_type_w
						|| '('
						|| data_precision_w
						|| ','
						|| data_scale_w
						|| ')'
						|| nullable_w;
			elsif	(data_scale_w = 0) and
				(data_precision_w > 0) then
				ds_comando_w := ' alter table '
						|| substr (table_name_w, 3, length (table_name_w))
						|| ' add '
						|| column_name_w
						|| ' '
						|| data_type_w
						|| '('
						|| data_precision_w
						|| ')'
						|| nullable_w;
			else
				ds_comando_w := ' alter table '
						|| substr (table_name_w, 3, length (table_name_w))
						|| ' add '
						|| column_name_w
						|| ' '
						|| data_type_w
						|| '('
						|| data_length_w
						|| ')'
						|| nullable_w;
			end if;
			exec_sql_dinamico ('TASY', ds_comando_w);
               		end;
		end loop;
		close c02;
		
	valida_objetos_sistema;
	
	end if;
	end;
end loop;
close c01;

end tasy_consistir_base_relatorio;
/
