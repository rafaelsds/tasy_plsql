create or replace
procedure pls_obter_ds_indicacao_clinica(
		dt_competencia_p		varchar2,
		ie_tipo_p			varchar2,
		nr_seq_protocolo_p		number,
		ie_indicacao_clinica_p	varchar2,
		nr_seq_atendimento_p	number,
		cd_doenca_p		number,
		cd_medico_executor_p	varchar2,
		nm_usuario_p		varchar2,
		cd_estabelecimento_p	number,
		ds_indicacao_clinica_p	out	varchar2) is 

		
ie_existe_param_w		varchar2(4000);		
ds_indicacao_clinica_w	varchar2(300);
ds_retorno_w		varchar2(255);
dt_competencia_w	date;

begin
if	(dt_competencia_p is not null) and
	(ie_tipo_p is not null) and
	(nr_seq_protocolo_p is not null) and
	(cd_estabelecimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	if	(cd_medico_executor_p is not null) then
		pls_consistir_medico(cd_medico_executor_p, nm_usuario_p, ds_retorno_w);
		if	(ds_retorno_w <> '') then
			-- #@DS_RETORNO#@
			wheb_mensagem_pck.exibir_mensagem_abort(266847, 'DS_RETORNO=' || ds_retorno_w);
		end if;
	end if;
	
	dt_competencia_w := to_date(dt_competencia_p,'dd/mm/yyyy hh24:mi:ss');
	
	pls_consiste_dt_competencia(
		dt_competencia_w,
		null,
		ie_tipo_p,
		nr_seq_protocolo_p,
		nm_usuario_p,
		cd_estabelecimento_p);
	
	if	(ie_indicacao_clinica_p is not null) and
		(nr_seq_atendimento_p is not null) then
		begin
		select	pls_obter_se_item_lista(ie_indicacao_clinica_p, lpad(pls_obter_cd_tipo_atend_tiss(nr_seq_atendimento_p, cd_estabelecimento_p),2,0))
		into	ie_existe_param_w
		from	dual;
		
		if	(ie_existe_param_w = 'S') and
			(cd_doenca_p is not null) then
			begin
			select	substr(obter_desc_cid(cd_doenca_p),1,255)
			into	ds_indicacao_clinica_w
			from	dual;
			end;
		end if;
		end;
	end if;

	ds_indicacao_clinica_p := ds_indicacao_clinica_w;
	
	end;
end if;

commit;

end pls_obter_ds_indicacao_clinica;
/
