create or replace
procedure lab_calcular_swisslab(	cd_barras_p			varchar2,
										cd_exame_p			varchar2,
										nm_usuario_aprov_p	varchar2,
										nm_usuario_p		Varchar2) is 

cd_exame_superior_w		varchar2(20);
cd_exame_w			varchar2(20);
nr_seq_resultado_w		number(20);
nr_seq_prescr_w			number(10);
nr_prescricao_w			number(10);

nr_seq_material_w		number(10);
nr_seq_exame_w			number(10);
nr_seq_exame_prescr_w	number(10);
qt_resultado_w			number(15,4);
pr_resultado_w			number(9,4);
ds_resultado_w			varchar2(2000);
ie_status_receb_w		number(2);
ie_status_novo_receb_w		number(2);
ds_consistencia_w		varchar2(4000);
ie_aprovar_todos_interf_w	varchar2(1);
qt_exames_consist_w		number(5);
ie_status_receb_lote_w		number(2);
ie_curva_w			varchar2(1);
ie_existe_w			varchar2(1);
qt_item_w			number(10);
nr_seq_formato_w	number(10);
nr_seq_exame_analito_w	number(10);
ie_status_atend_w	prescr_procedimento.ie_status_atend%type;
cd_medico_resp_w	pessoa_fisica.cd_pessoa_fisica%type;
nm_usuario_aprov_w	usuario.nm_usuario%type;


ds_erro_w			varchar2(2000);
nr_seq_log_hl7_w		number(10);
nr_seq_log_w			number(10);

cursor c02 is
	select	r.nr_seq_exame,
		nvl(r.qt_resultado,0),
		r.pr_resultado,
		r.ds_resultado
	from    exame_lab_result_item r,
			exame_laboratorio e
	where   r.nr_seq_resultado	= nr_seq_resultado_w
	and   	r.nr_seq_prescr		= nr_seq_prescr_w
	and   	((r.qt_resultado <> 0) or (r.pr_resultado <> 0) or (r.ds_resultado is not null))
	and   	r.nr_seq_exame 		= e.nr_seq_exame
	order   by 	r.nr_sequencia,
		e.nr_seq_apresent;
					
begin


begin
cd_exame_superior_w 	:= substr(cd_exame_p, 1, instr(cd_exame_p,'%')-1);
cd_exame_w		:= substr(cd_exame_p, length(cd_exame_superior_w) + 2 ,length(cd_exame_p));


select 	max(nr_prescricao)
into	nr_prescricao_w
from	prescr_proc_material
where	cd_barras = to_char(to_number(cd_barras_p));

select	max(nr_seq_exame)
into	nr_seq_exame_prescr_w
from	exame_laboratorio
where	nvl(cd_exame_integracao, cd_exame) = cd_exame_superior_w;

select	max(c.nr_sequencia)
into	nr_seq_prescr_w
from  	prescr_proc_material a,
		prescr_proc_mat_item b,
		prescr_procedimento c
where 	a.nr_sequencia 	= b.nr_seq_prescr_proc_mat
and	  	a.nr_prescricao = c.nr_prescricao
and	  	c.nr_sequencia 	= b.nr_seq_prescr
and	  	c.nr_prescricao	= nr_prescricao_w
and	  	c.nr_seq_exame 	= nr_seq_exame_prescr_w
and	  	a.cd_barras 	= to_char(to_number(cd_barras_p))
and	  	nvl(c.ie_suspenso, 'N') = 'N';
/*select	max(p.nr_sequencia)
into	nr_seq_prescr_w
from	prescr_procedimento p,
		exame_laboratorio e
where	p.nr_prescricao	 = nr_prescricao_w
and		e.nr_seq_exame	 = p.nr_seq_exame
and		nvl(e.cd_exame_integracao, e.cd_exame)	= cd_exame_superior_w;*/

select	nvl(max(ie_status_atend),0)
into	ie_status_atend_w
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_w
and		nr_sequencia = nr_seq_prescr_w;

select	max(nr_seq_resultado)
into	nr_seq_resultado_w
from	exame_lab_resultado
where	nr_prescricao = nr_prescricao_w;


if		(nr_seq_resultado_w 	is not null) and
		(nr_seq_prescr_w 	is not null) and
		(ie_status_atend_w <= 30) then

		
	select	nvl(max(b.ie_status_receb),35),
		nvl(max(b.ie_aprovar_todos_interf),'S'),
		max(b.ie_status_receb_lote_ext)
	into	ie_status_receb_w,
		ie_aprovar_todos_interf_w,
		ie_status_receb_lote_w
	from	lab_parametro b,
		prescr_medica a
	where	a.cd_estabelecimento	= b.cd_estabelecimento
	  and	a.nr_prescricao		= nr_prescricao_w;


	select	max(nr_seq_material)
	into	nr_seq_material_w
	from 	exame_lab_result_item
	where 	nr_seq_resultado	= nr_seq_resultado_w
	and 	nr_seq_prescr		= nr_seq_prescr_w
	and 	nr_seq_material 	is not null;
		
	open c02;
	loop
	fetch c02 into	nr_seq_exame_w,
			qt_resultado_w,
			pr_resultado_w,
			ds_resultado_w;
		exit when c02%notfound;

		calcular_valores_exame(nr_seq_resultado_w,
					nr_prescricao_w,
					nr_seq_prescr_w,
					nr_seq_material_w,
					nr_seq_exame_w,
					qt_resultado_w,
					pr_resultado_w,
					ds_resultado_w);


	end loop;
	close c02;
	
	
	ie_status_novo_receb_w := ie_status_receb_w;
	ds_consistencia_w := '';
	if (ie_aprovar_todos_interf_w = 'N') then	
		Consistir_Aprovacao_Exame(nr_prescricao_w, nr_seq_prescr_w, ds_consistencia_w);
		
		ds_consistencia_w	:= replace(ds_consistencia_w,chr(13)||chr(10),'');
				
		if ((nvl(ds_consistencia_w,'-1') <> '-1') and (ie_status_receb_w = 35)) then		
			ie_status_novo_receb_w := 30;
		end if;
		
	end if;

		
	if (ie_status_novo_receb_w = 35) then
	
		consitir_valores_interf_aprov(nr_seq_resultado_w, nr_seq_prescr_w, qt_exames_consist_w);
		if (qt_exames_consist_w > 0) then		
			ie_status_novo_receb_w := 30;
		end if;
	end if;

	/*update 	prescr_procedimento
	set 	ie_status_atend = ie_status_novo_receb_w, 
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where 	nr_prescricao = nr_prescricao_w
	and 	nr_sequencia = nr_seq_prescr_w
	and 	ie_status_atend = 30;*/
	
	if (cd_exame_w <> cd_exame_superior_w) then
		select	--max(nr_seq_exame),
				max(nr_seq_exame)
		into	--nr_seq_exame_w,
				nr_seq_exame_analito_w
		from	exame_laboratorio a
		where	nvl(a.cd_exame_integracao, a.cd_exame) = cd_exame_w
		and		cd_exame_superior_w IN(	SELECT	NVL(c.cd_exame_integracao, c.cd_exame)
										FROM	exame_laboratorio c
										CONNECT BY PRIOR c.nr_seq_superior = c.nr_seq_exame
										START WITH c.nr_seq_exame = a.nr_seq_exame);					
	end if;
	
	if (ie_status_novo_receb_w <> ie_status_receb_w) then
		ie_status_receb_w	:=	ie_status_novo_receb_w;
	end if;
	
	/*select	max(a.nr_ordem_amostra)
	into	ie_curva_w
	from	exame_laboratorio a
	where	nvl(a.cd_exame_integracao, a.cd_exame) = cd_exame_w;*/
	select	max(a.nr_ordem_amostra)
	into	ie_curva_w
	from	exame_laboratorio a
	where	a.nr_seq_exame = nr_seq_exame_analito_w;
	
	if	(ie_curva_w is not null) then
		
		select	decode(count(*),0,'N','S')
		into	ie_existe_w
		from	prescr_proc_mat_item
		where	nr_prescricao 	= nr_prescricao_w
		and		nr_seq_prescr 	= nr_seq_prescr_w
		and		cd_barras = to_char(to_number(cd_barras_p));
				
		--Este update n�o esta duplicado, foi feito para corrigir uma consist�ncia da prescr_procedimento_update, no quesito de altera��o de status da amostra.
		update	prescr_proc_mat_item
		set	ie_status 	= nvl(ie_status_receb_w,30)
		where	nr_prescricao 	= nr_prescricao_w
		and	nr_seq_prescr 	= nr_seq_prescr_w
		and	cd_barras = to_char(to_number(cd_barras_p));
				
		select	decode(count(*),0,'N','S')
		into	ie_existe_w
		from	prescr_proc_mat_item a
		where	a.nr_prescricao = nr_prescricao_w
		and		a.nr_seq_prescr = nr_seq_prescr_w
		and		a.ie_status < nvl(ie_status_receb_w,30);
		
		
		
		if	(ie_existe_w = 'N') then		
			update 	prescr_procedimento
			set	ie_status_atend	= nvl(ie_status_receb_w,30),
				nm_usuario 	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where 	nr_prescricao	= nr_prescricao_w
			and 	nr_sequencia	= nr_seq_prescr_w
			and		ie_status_atend < nvl(ie_status_receb_w,30);
		else
		
			update 	prescr_procedimento
			set	ie_status_atend	= obter_valor_menor(ie_status_receb_w,30),
				nm_usuario 	= nm_usuario_p,
				dt_atualizacao	= sysdate
			where 	nr_prescricao	= nr_prescricao_w
			and 	nr_sequencia	= nr_seq_prescr_w
			and		ie_status_atend < obter_valor_menor(ie_status_receb_w,30);
		end if;
				
		--Este update n�o esta duplicado, foi feito para corrigir uma consist�ncia da prescr_procedimento_update, no quesito de altera��o de status da amostra.
		update	prescr_proc_mat_item
		set	ie_status 	= nvl(ie_status_receb_w,30)
		where	nr_prescricao 	= nr_prescricao_w
		and	nr_seq_prescr 	= nr_seq_prescr_w
		and	cd_barras = to_char(to_number(cd_barras_p));

	else
		
		if	(nvl(ie_status_receb_w,30) >= 35) then		
		
			select	max(e.nr_seq_exame)
			into	nr_seq_exame_w
			from	exame_laboratorio e
			where	nvl(e.cd_exame_integracao, e.cd_exame)	= cd_exame_superior_w;
			
			select	max(nr_seq_material),
					max(nr_seq_formato)
			into	nr_seq_material_w,
					nr_seq_formato_w
			from	exame_lab_result_item
			where	nr_seq_exame = nr_seq_exame_w
			and		nr_seq_resultado = nr_seq_resultado_w
			and		nr_seq_prescr = nr_seq_prescr_w;
			
			select	count(*)
			into	qt_item_w
			from	(
					SELECT	a.nr_seq_exame
					FROM 	EXAME_LABORATORIO C,
							EXAME_LAB_MATERIAL B,
							EXAME_LAB_FORMAT_ITEM A
					WHERE 	A.NR_SEQ_FORMATO = nr_seq_formato_w
					AND 	A.NR_SEQ_EXAME IS NOT NULL
					AND 	A.NR_SEQ_EXAME = C.NR_SEQ_EXAME
					AND 	A.NR_SEQ_EXAME = B.NR_SEQ_EXAME(+)
					AND 	B.NR_SEQ_MATERIAL(+) = nr_seq_material_w
					union all
					SELECT	a.nr_seq_exame
					FROM 	EXAME_LABORATORIO C,
							EXAME_LAB_MATERIAL B,
							EXAME_LAB_FORMAT D,
							EXAME_LAB_FORMAT_ITEM A
					WHERE 	D.NR_SEQ_SUPERIOR = nr_seq_formato_w
					AND		D.NR_SEQ_FORMATO =  A.NR_SEQ_FORMATO
					AND		A.NR_SEQ_EXAME IS NOT NULL
					AND		A.NR_SEQ_EXAME = C.NR_SEQ_EXAME
					AND		A.NR_SEQ_EXAME = B.NR_SEQ_EXAME(+)
					AND		B.NR_SEQ_MATERIAL(+)= nr_seq_material_w) a
			where	a.nr_seq_exame not in(	select	a.nr_seq_exame										
											from	exame_lab_result_item a
											where	a.nr_seq_resultado = nr_seq_resultado_w
											and		a.nr_seq_prescr = nr_seq_prescr_w
											and		(a.ds_resultado is not null or a.qt_resultado is not null or a.pr_resultado is not null));
				
			if	(qt_item_w = 0) then
				update 	prescr_procedimento
				set		ie_status_atend	= ie_status_receb_w,
						nm_usuario 	= nm_usuario_p,
						dt_atualizacao	= sysdate
				where 	nr_prescricao	= nr_prescricao_w
				and 	nr_sequencia	= nr_seq_prescr_w;				
			else
				update 	prescr_procedimento
				set	ie_status_atend	= obter_valor_menor(ie_status_receb_w,30),
					nm_usuario 	= nm_usuario_p,
					dt_atualizacao	= sysdate
				where 	nr_prescricao	= nr_prescricao_w
				and 	nr_sequencia	= nr_seq_prescr_w;			
			end if;
		else
				
			update 	prescr_procedimento
			set		ie_status_atend	= nvl(ie_status_receb_w,30),
					nm_usuario 	= nm_usuario_p,
					dt_atualizacao	= sysdate
			where 	nr_prescricao	= nr_prescricao_w
			and 	nr_sequencia	= nr_seq_prescr_w;
		end if;
		
	end if;

	select	decode(count(*),0,'N','S')
	into	ie_existe_w
	from	prescr_procedimento
	where	nr_prescricao	= nr_prescricao_w
	and 	nr_sequencia	= nr_seq_prescr_w
	and		ie_status_atend >= 35;

	if	--(ie_status_novo_receb_w >= 35) and
		--((qt_item_w = 0 and nvl(ie_status_receb_w,30) >= 35) or (nvl(ie_status_receb_w,30) < 35)) then
		(ie_existe_w = 'S') then
				
		select	max(a.cd_pessoa_fisica)
		into	cd_medico_resp_w
		from	usuario a
		where	upper(a.nm_usuario) = upper(nm_usuario_aprov_p);
		
		select	max(a.nm_usuario)
		into	nm_usuario_aprov_w
		from	usuario a
		where	upper(a.nm_usuario) = upper(nm_usuario_aprov_p)
		and		nvl(cd_medico_resp_w,0) = nvl(a.cd_pessoa_fisica,0);
		
		update 	exame_lab_result_item
		set		dt_aprovacao		= sysdate,
				nm_usuario_aprovacao = nm_usuario_aprov_w,
				cd_medico_resp		= cd_medico_resp_w
		where	nr_seq_resultado	= nr_seq_resultado_w
		  and	nr_seq_prescr		= nr_seq_prescr_w
		  and	nr_seq_material	is not null;		  
	end if;
	
end if;

exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,1000);		
	gravar_log_lab(69999,
			substr('SwissLab - '||
			'Par�metros: '||cd_barras_p||'_'||cd_exame_p||' - '||
			'Resultado: '||nr_seq_resultado_w||' - '||
			'Prescri��o: '||nr_prescricao_w||' - '||
			'Seq. Prescri��o: '||nr_seq_prescr_w||' - '||
			'Material: '||nr_seq_material_w||' - '||
			'Exame: '||nr_seq_exame_w||' - '||
			'Qt. Resultado: '||qt_resultado_w||' - '||
			'Pr: Resultado: '||pr_resultado_w||' - '||
			'Erro: '||ds_erro_w,1,2000), nm_usuario_p, nr_prescricao_w);
end;

commit;

end lab_calcular_swisslab;
/
