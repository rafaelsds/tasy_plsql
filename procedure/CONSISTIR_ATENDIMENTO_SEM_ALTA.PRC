create or replace
procedure consistir_atendimento_sem_alta(	cd_pessoa_fisica_p	varchar2,
						nr_atendimento_p	number,
						cd_perfil_p         	number,
						cd_estabelecimento_p	number,
						nm_usuario_p		varchar2,
						ds_retorno_p		out varchar2) is 

vl_parametro_w		varchar2(1);
nr_atend_sem_alta_w	number(10,0);
ds_unidade_w		varchar2(255);
				
begin

obter_param_usuario(	916, 443, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, vl_parametro_w);
			
if	(vl_parametro_w = 'S') and
	(cd_pessoa_fisica_p is not null) and
	(nvl(nr_atendimento_p,0) = 0)then
	begin
	
	nr_atend_sem_alta_w	:= obter_atend_sem_alta(cd_pessoa_fisica_p);
	
	if	(nr_atend_sem_alta_w > 0)then
		begin
		
		ds_unidade_w	:= obter_unidade_atendimento(nr_atend_sem_alta_w, 'A', 'S');
		
		ds_retorno_p	:=  substr(obter_texto_dic_objeto(312617, wheb_usuario_pck.get_nr_seq_idioma, 'NR_ATEND_SEM_ALTA_W='||nr_atend_sem_alta_w || ';DS_UNIDADE_W='|| nvl(ds_unidade_w,' ')),1,255);
		--'Existe atendimento sem alta para esta pessoa f�sica. Atendimento: ' || nr_atend_sem_alta_w || ' Setor: ' || ds_unidade_w;
		
		end;
	end if;
	
	end;
elsif	(vl_parametro_w = 'Q') and
	(cd_pessoa_fisica_p is not null) and
	(nvl(nr_atendimento_p,0) = 0)then
	begin
	
	nr_atend_sem_alta_w	:= obter_atend_sem_alta(cd_pessoa_fisica_p);
	
	if	(nr_atend_sem_alta_w > 0)then
		begin
		
		ds_unidade_w	:= obter_unidade_atendimento(nr_atend_sem_alta_w, 'A', 'S');
		
		ds_retorno_p	:= substr(obter_texto_dic_objeto(312617, wheb_usuario_pck.get_nr_seq_idioma, 'NR_ATEND_SEM_ALTA_W='||nr_atend_sem_alta_w || ';DS_UNIDADE_W='|| nvl(ds_unidade_w,' ')) ||
					chr(10) || obter_texto_tasy(312665,null),1,255);
		--'Existe atendimento sem alta para esta pessoa f�sica. Atendimento: ' || nr_atend_sem_alta_w || ' Setor: ' || ' Deseja gerar um novo atendimento ?';
		
		end;
	end if;
	
	end;
end if;

end consistir_atendimento_sem_alta;
/