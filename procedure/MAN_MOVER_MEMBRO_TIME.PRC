create or replace
procedure man_mover_membro_time(
			nr_membro_p		number,
			nr_time_p		number,
			nm_usuario_p		varchar2) is 

begin

update	desenv_member
set	nr_team = nr_time_p,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_sequencia = nr_membro_p;

commit;

end man_mover_membro_time;
/