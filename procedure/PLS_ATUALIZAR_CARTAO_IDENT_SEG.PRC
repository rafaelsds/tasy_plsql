create or replace
procedure pls_atualizar_cartao_ident_seg
			(	nr_seq_segurado_p	pls_segurado_carteira.nr_seq_segurado%type,
				nr_via_p		Varchar2,
				dt_validade_p		varchar2,
				nr_seq_plano_p		varchar2,
				ie_tipo_p		varchar2,
				nm_usuario_p		Varchar2) is
/* ie_tipo_p
*     	V: Via
*	D: Data de validade
*	P: Produto
*	T: Todos
*/
nr_via_anterior_w	pls_segurado_carteira.nr_via_solicitacao%type;
dt_validade_ant_w	pls_segurado_carteira.dt_validade_carteira%type;

begin

select	max(nr_via_solicitacao),
	max(dt_validade_carteira)
into	nr_via_anterior_w,
	dt_validade_ant_w
from	pls_segurado_carteira
where	nr_seq_segurado = nr_seq_segurado_p;

if	(nr_via_p is not null) and
	((ie_tipo_p = 'V') or (ie_tipo_p = 'T')) and
	((nr_via_anterior_w is null) or (to_number(nr_via_p) <> nr_via_anterior_w)) then
	
	update	pls_segurado_carteira
	set	nr_via_solicitacao = to_number(nr_via_p)
	where	nr_seq_segurado = nr_seq_segurado_p;
	
	pls_gerar_segurado_historico(nr_seq_segurado_p, '76', sysdate,
				     wheb_mensagem_pck.get_texto(295006, 'NR_VIA_ANTERIOR=' || nr_via_anterior_w || ';NR_VIA_NOVA=' || nr_via_p),
				     null, null, null, null,
				     null, null, null, null,
				     null, null, null, null,
				     nm_usuario_p, 'N');
end if;

if	(dt_validade_p is not null) and
	((ie_tipo_p = 'D') or (ie_tipo_p = 'T')) and
	((dt_validade_ant_w is null) or (trunc(dt_validade_ant_w,'dd') <> trunc(last_day('01/'||dt_validade_p),'dd'))) then
	
	update	pls_segurado_carteira
	set	dt_validade_carteira = last_day('01/'||dt_validade_p)
	where	nr_seq_segurado = nr_seq_segurado_p;
	
	pls_gerar_segurado_historico(nr_seq_segurado_p, '76', sysdate,
				     wheb_mensagem_pck.get_texto(295007, 'DT_VALIDADE_ANT=' || to_char(dt_validade_ant_w,'dd/mm/yyyy') || ';DT_VALIDADE_NOVA=' || to_char(last_day('01/'||dt_validade_p),'dd/mm/yyyy')),
				     null, null, null, null,
				     null, null, null, null,
				     null, null, null, null,
				     nm_usuario_p, 'N');
end if;

if	(nr_seq_plano_p is not null) and ((ie_tipo_p = 'P') or (ie_tipo_p = 'T')) then
	update	pls_segurado
	set	nr_seq_plano = nr_seq_plano_p
	where	nr_sequencia = nr_seq_segurado_p;
end if;


commit;

end pls_atualizar_cartao_ident_seg;
/