create or replace
procedure ul_avisar_vencimento_contrato(	cd_estabelecimento_p	number,
					ie_comunicar_resp_p		varchar2 default 'S') as

/*
ie_comunicar_resp_p: Se habilitado para 'N' apenas as pessoas que possuem liberação receberão a comunicação interna/email
*/					
					
cd_pessoa_responsavel_w		varchar2(10);
cd_pessoa_negoc_w		varchar2(10);
qt_dias_vencer_w			number(5);
nr_sequencia_w			number(10);
nr_seq_classif_w			number(10);
cd_contrato_w			varchar2(20);
ds_objeto_contrato_w		varchar2(4000);
dt_fim_w				date;
nm_usuario_w			varchar2(255) := '';
nm_usuario_neg_w			varchar2(15);
nm_contratado_w			varchar2(80);
cd_setor_w			number(5);
cd_setor_adicional_w		varchar2(255);
cd_perfil_w				number(10);
cd_perfil_adicional_w			varchar2(4000);
nr_seq_contrato_w			number(10,0);
nm_usuario_lib_w			varchar2(15);
nm_usuario_resp_w			varchar2(15);
ie_avisa_venc_setor_w		varchar2(1);
ie_aviso_vencimento_usuario_w	varchar2(01);
ds_email_destino_w			varchar2(2000);
ds_email_w			varchar2(2000);
ds_tipo_contrato_w			varchar2(200);
dt_inicio_w			date;
ds_forma_renovacao_w		varchar2(255);
vl_total_contrato_w			number(15,2);
ds_mensagem_w			varchar2(4000);
ds_email_origem_w			varchar2(255);
ds_condicao_pagamento_w		varchar2(80);
ds_forma_pagamento_w		varchar2(255);
ds_moeda_w			varchar2(255);
vl_pagto_w			number(15,2);
qt_dias_diferenca_w		number(10);
qt_dias_interv_aviso_w		number(10);
ie_intervalo_w			number(10);
ie_gerar_w			varchar2(1);
dt_revisao_w			date;
ds_consistencia_w			varchar2(2000);
ds_email_origem_estab_w		varchar2(255);
ie_email_origem_w			varchar2(1);

cursor c01 is
select	cd_pessoa_resp,
	cd_pessoa_negoc,
	trunc(dt_fim - sysdate),
	cd_contrato,
	ds_objeto_contrato,
	dt_fim,
	substr(obter_razao_social(cd_cgc_contratado),1,80),
	cd_setor,
	nr_sequencia,
	ie_avisa_venc_setor,
	substr(obter_descricao_padrao('TIPO_CONTRATO','DS_TIPO_CONTRATO',nr_seq_tipo_contrato),1,200) ds_tipo_contrato,
	dt_inicio,
	substr(obter_valor_dominio(1061, ie_renovacao),1,255) ds_forma_renovacao,
	vl_total_contrato,
	substr(obter_desc_cond_pagto(cd_condicao_pagamento),1,80) ds_condicao_pagamento,
	nvl(qt_dias_interv_aviso,0),
	dt_revisao,
	nvl(ie_email_origem,'E')
from	contrato
where	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p
and	nvl(ie_avisa_vencimento,'N') = 'S'
and	trunc(trunc(dt_fim,'dd') - trunc(sysdate,'dd')) <= nvl(qt_dias_aviso_venc,0)
and	trunc(dt_fim,'dd') >= trunc(sysdate,'dd')
and	cd_pessoa_resp is not null;

cursor c02 is
select	distinct
	nvl(nm_usuario_lib,'X'),
	cd_perfil,
	ie_aviso_vencimento
from	contrato_usuario_lib
where	nr_seq_contrato = nr_seq_contrato_w
and	nvl(ie_aviso_vencimento, 'N') <> 'N';

begin

select	nvl(max(substr(obter_dados_pf_pj_estab(cd_estabelecimento_p, '', cd_cgc, 'M'),1,255)), 'X')
into	ds_email_origem_estab_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

open c01;
loop
fetch c01 into
	cd_pessoa_responsavel_w,
	cd_pessoa_negoc_w,
	qt_dias_vencer_w,
	cd_contrato_w,
	ds_objeto_contrato_w,
	dt_fim_w,
	nm_contratado_w,
	cd_setor_w,
	nr_seq_contrato_w,
	ie_avisa_venc_setor_w,
	ds_tipo_contrato_w,
	dt_inicio_w,
	ds_forma_renovacao_w,
	vl_total_contrato_w,
	ds_condicao_pagamento_w,
	qt_dias_interv_aviso_w,
	dt_revisao_w,
	ie_email_origem_w;
exit when c01%notfound;
	begin
	ds_email_destino_w := '';
	ds_email_origem_w	:= ds_email_origem_estab_w;

	if	(ie_email_origem_w = 'R') then
		begin

		select	substr(nvl(obter_compl_pf(cd_pessoa_responsavel_w,1, 'M'),ds_email_origem_estab_w),1,255)
		into	ds_email_origem_w
		from	dual;

		end;
	elsif	(ie_email_origem_w = 'G') then
		begin

		select	substr(nvl(obter_compl_pf(cd_responsavel_contratos,1, 'M'),ds_email_origem_estab_w),1,255)
		into	ds_email_origem_w
		from	parametro_compras
		where	cd_estabelecimento = cd_estabelecimento_p;

		end;
	end if;
	
	ie_gerar_w		:= 'S';
	qt_dias_diferenca_w	:= trunc(trunc(dt_fim_w,'dd') - trunc(sysdate,'dd'));
	ie_intervalo_w		:= trunc(mod(qt_dias_diferenca_w, qt_dias_interv_aviso_w));
	
	if	(nvl(qt_dias_interv_aviso_w, 0) <> 0)  and
		(ie_intervalo_w <> 0) then

		ie_gerar_w	:= 'N';
		
	end if;
	
	if	(ie_gerar_w = 'S') then
	
		nm_usuario_w		:= '';
		cd_setor_adicional_w	:= '';
		cd_perfil_adicional_w	:= '';
			
		select	nvl(max(substr(obter_usuario_pessoa(cd_pessoa_responsavel_w),1,15)),'X')
		into	nm_usuario_resp_w
		from	contrato_usuario_lib
		where	nr_seq_contrato = nr_seq_contrato_w
		and	nvl(ie_aviso_vencimento, 'N') in ('S','A')
		and	nm_usuario_lib = substr(obter_usuario_pessoa(cd_pessoa_responsavel_w),1,15);

		if	(nm_usuario_resp_w = 'X') and (ie_comunicar_resp_p = 'S') then
			select	nvl(max(nm_usuario),'X')
			into	nm_usuario_resp_w
			from	usuario
			where	cd_pessoa_fisica = cd_pessoa_responsavel_w;
		end if;
			
		select	nvl(max(substr(obter_usuario_pessoa(cd_pessoa_negoc_w),1,15)),'X')
		into	nm_usuario_neg_w
		from	contrato_usuario_lib
		where	nr_seq_contrato = nr_seq_contrato_w
		and	nvl(ie_aviso_vencimento, 'N') in ('S','A')
		and	nm_usuario_lib = substr(obter_usuario_pessoa(cd_pessoa_negoc_w),1,15);

		if	(nm_usuario_neg_w = 'X') and (ie_comunicar_resp_p = 'S') then
			select	nvl(max(nm_usuario),'X')
			into	nm_usuario_neg_w
			from	usuario
			where	cd_pessoa_fisica = cd_pessoa_negoc_w;
		end if;
		
		select	max(substr(obter_valor_dominio(1062, ie_forma),1,255)),
			max(substr(obter_desc_moeda(cd_moeda),1,255)),
			max(vl_pagto)
		into	ds_forma_pagamento_w,
			ds_moeda_w,
			vl_pagto_w
		from	contrato_regra_pagto
		where	nr_seq_contrato = nr_seq_contrato_w;

		select	nvl(max(nm_usuario),'X')
		into	nm_usuario_resp_w
		from	usuario
		where	nm_usuario	= nm_usuario_resp_w
		and	ie_situacao	= 'A';
		
		if	(nm_usuario_resp_w <> 'X') then
			nm_usuario_w 		:= substr(nm_usuario_w || ',' || nm_usuario_resp_w,1,255);
		end if;

		select	nvl(max(nm_usuario),'X')
		into	nm_usuario_neg_w
		from	usuario
		where	nm_usuario	= nm_usuario_neg_w
		and	ie_situacao	= 'A';
		
		if	(nm_usuario_neg_w <> 'X') then
			nm_usuario_w		:= substr(nm_usuario_w || ',' || nm_usuario_neg_w,1,255);
		end if;

		open c02;
		loop
		fetch c02 into
			nm_usuario_lib_w,
			cd_perfil_w,
			ie_aviso_vencimento_usuario_w;
			exit when c02%notfound;

			if	(nm_usuario_lib_w <> 'X') then
				select	nvl(max(nm_usuario),'X')
				into	nm_usuario_lib_w
				from	usuario
				where	nm_usuario	= nm_usuario_lib_w
				and	ie_situacao	= 'A';
			end if;
			
			if	(nm_usuario_lib_w <> 'X') and
				(instr(nvl(nm_usuario_w,'X'), nm_usuario_lib_w) = 0) then
				if	((ie_aviso_vencimento_usuario_w = 'S') or (ie_aviso_vencimento_usuario_w = 'A')) then
					nm_usuario_w := substr(nm_usuario_w || ',' || nm_usuario_lib_w,1,255);
				end if;
			end if;
			if	((ie_aviso_vencimento_usuario_w = 'E') or (ie_aviso_vencimento_usuario_w = 'A') and (nvl(nm_usuario_lib_w,'X') <> 'X')) then
				select	substr(obter_dados_usuario_opcao(nm_usuario_lib_w,'E'),1,255)
				into	ds_email_w
				from	dual;
				if	(ds_email_w is not null) then
					ds_email_destino_w := substr(ds_email_destino_w || ds_email_w || ';',1,2000);
				end if;
			end if;
			
			if	(ie_aviso_vencimento_usuario_w = 'S') and
				(cd_perfil_w is not null) then
				
				if	(nvl(cd_perfil_adicional_w, 'X') = 'X') then
					
					cd_perfil_adicional_w	:= cd_perfil_w;
				else		
					
					cd_perfil_adicional_w	:= substr(cd_perfil_adicional_w || ' ,' || cd_perfil_w, 1, 400);
				end if;
			end if;
			
		end loop;
		close c02;

		if	(cd_setor_w is not null) and
			(ie_avisa_venc_setor_w = 'S') and
			(ie_comunicar_resp_p = 'S') then
			cd_setor_adicional_w	:= cd_setor_w || ', ';
		end if;

		if	(nm_usuario_w <> 'X') then
	
			begin
			select	comunic_interna_seq.nextval
			into	nr_sequencia_w
			from	dual;

			select	obter_classif_comunic('F')
			into	nr_seq_classif_w
			from	dual;

			ds_mensagem_w	:= Substr(WHEB_MENSAGEM_PCK.get_texto(819045, 'NR_SEQUENCIA=' || cd_contrato_w || 
																		';DS_OBJETO_CONTRATO=' || ds_objeto_contrato_w || 
																		';NM_CONTRATADO=' || nm_contratado_w || 
																		';DS_TIPO_CONTRATO=' || ds_tipo_contrato_w  ||
																		';DS_OBJETO_CONTRATO=' || ds_objeto_contrato_w || 
																		';DT_INICIO=' || dt_inicio_w || 
																		';DT_FIM=' || dt_fim_w ||'      '||
																		';QT_DIAS_VENCER=' || qt_dias_vencer_w || 
																		';DS_FORMA_RENOVACAO=' || ds_forma_renovacao_w ||
																		';VL_TOTAL_CONTRATO=' || vl_total_contrato_w || 
																		';DS_CONDICAO_PAGAMENTO=' || ds_condicao_pagamento_w || 
																		';DS_FORMA_PAGAMENTO=' || ds_forma_pagamento_w || 
																		';DS_MOEDA=' || ds_moeda_w ||  
																		';DT_REVISAO=' || dt_revisao_w ||  
																		';VL_PAGTO=' || vl_pagto_w),1,4000);
			
			insert into comunic_interna(
				dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				nr_sequencia,
				ie_gerencial,
				nr_seq_classif,
				dt_liberacao,
				ds_setor_adicional,
				ds_perfil_adicional)
			values(	sysdate + 1/86400,
				WHEB_MENSAGEM_PCK.get_texto(309971), /*'Aviso de vencimento de contrato'*/
				ds_mensagem_w,
				'Tasy',
				sysdate,
				'N',
				nm_usuario_w || ',',
				nr_sequencia_w,
				'N',
				nr_seq_classif_w,
				sysdate,
				cd_setor_adicional_w,
				cd_perfil_adicional_w);
			end;
			
			if	(ds_email_destino_w is not null) then
				begin
				if	(substr(ds_email_destino_w,1,1) = ';') then
					ds_email_destino_w := substr(ds_email_destino_w,2,255);
				end if;
				
				begin
				enviar_email(	WHEB_MENSAGEM_PCK.get_texto(309971), /*'Aviso de vencimento de contrato'*/
						ds_mensagem_w,
						ds_email_origem_w,
						ds_email_destino_w,
						'Aviso_Tasy', /* Nome de usuario padrao para envio de email */
						'A');
		
				exception when others then
					
					ds_consistencia_w	:= substr(sqlerrm(sqlcode),1,2000);
					ds_consistencia_w	:= substr(nr_seq_contrato_w || ' - ' || ds_email_origem_w || ' - ' || ds_email_destino_w || ' - ' || ds_consistencia_w, 1, 2000);
					
					/*insert into logxxxxx_tasy (	
						dt_atualizacao,
						nm_usuario,
						cd_log,
						ds_log)
					values(	sysdate,
						'Aviso_Tasy',
						55877,
						ds_consistencia_w);*/
					
				end;
				end;
			end if;
		end if;
	end if;
	end;
end loop;
close c01;
commit;

end ul_avisar_vencimento_contrato;
/