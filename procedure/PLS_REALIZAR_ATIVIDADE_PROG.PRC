create or replace
procedure pls_realizar_atividade_prog
			(nr_sequencia_p		number,
			 nm_usuario_p		varchar2) is

nr_seq_seg_historico_w		number(10);
			
begin

select	nr_sequencia
into	nr_seq_seg_historico_w
from	pls_segurado_historico	
where	nr_sequencia = nr_sequencia_p;
	
if	(nr_seq_seg_historico_w is not null) then
	update	pls_segurado_historico
	set	dt_realizacao = sysdate,
		nm_usuario_realizacao = nm_usuario_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;
end if;
	
commit;
end pls_realizar_atividade_prog;
/