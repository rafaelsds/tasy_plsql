create or replace
procedure pls_alterar_titularidade
			(	nr_seq_segurado_p		number,
				nr_seq_titular_p		number,
				nr_grau_parentesco_p		number,
				ie_opcao_p			number,
				nr_seq_mtvo_rescisao_p		number,
				dt_rescisao_p			date,
				ds_observacao_p			varchar2,
				cd_estabelecimento_p		number,
				dt_geracao_sib_p		date,
				nm_usuario_p			varchar2) is

/*	ie_opcao_p
	1 - Transformar dependente em 
	2 - Transformar titular em dependente
	3 - Alterar titular do dependente
*/

nr_seq_titular_w		number(10);
nm_titular_w			varchar2(255);
nr_seq_contrato_w		number(10);
nr_seq_titular_ant_w		number(10);
ie_titular_pf_w			varchar2(1);
ds_observacao_w			varchar2(100);
ie_permite_tab_dif_w		varchar2(1);
ie_consiste_tab_contr_w		varchar2(1);
ie_geracao_valores_w		varchar2(1);
ie_titular_familia_w		varchar2(1);
ie_alterar_cart_benef_w		varchar2(10);
cd_matricula_familia_w		number(10);
nm_pessoa_fisica_w		varchar2(255);
nr_seq_seg_titular_w		number(10);
nm_titular_ant_w		varchar2(255);
cd_cgc_estipulante_w		varchar2(20);
ie_alterar_matricula_benef_w	varchar2(10);
nr_seq_seg_dependente_w		number(10);
ie_dt_adesao_w			varchar2(10);
dt_contratacao_titular_w	date;
dt_contratacao_w		date;
dt_rescisao_w			date;
dt_rescisao_tit_w		date;
ie_reativa_titular_w		varchar2(1)	:= 'N';
ie_titularidade_w		pls_segurado.ie_titularidade%type;
ie_tipo_parentesco_w		grau_parentesco.ie_tipo_parentesco%type;
nr_seq_tabela_w			pls_tabela_preco.nr_sequencia%type;
ie_preco_vidas_contrato_w	pls_tabela_preco.ie_preco_vidas_contrato%type;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
nr_seq_tabela_seg_w		pls_tabela_preco.nr_sequencia%type;
ie_calculo_vidas_tab_w		pls_tabela_preco.ie_calculo_vidas%type;
qt_vidas_w			pls_segurado_valor.qt_vidas%type	:= 0;
qt_registros_w			number(10);
qt_idade_w			number(10);
ie_estado_civil_w		pessoa_fisica.ie_estado_civil%type;
nm_beneficiario_w		varchar2(255);

Cursor C01 is
	select	nr_sequencia,
		dt_rescisao
	from	pls_segurado
	where	nr_seq_titular = nr_seq_titular_ant_w
	and	nr_seq_contrato	= nr_seq_contrato_w;
	
Cursor C02 is
	select	a.nr_sequencia
	from	pls_segurado a,
		pls_tabela_preco b
	where	a.nr_seq_tabela = b.nr_sequencia
	and	a.dt_cancelamento is null
	and	a.dt_liberacao is not null
	and	a.nr_seq_contrato = nr_seq_contrato_w;

begin
ie_permite_tab_dif_w		:= obter_valor_param_usuario(1202,9,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
ie_consiste_tab_contr_w		:= obter_valor_param_usuario(1202,10,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
ie_titular_familia_w		:= nvl(obter_valor_param_usuario(1202,82,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p),'N');
ie_dt_adesao_w			:= nvl(obter_valor_param_usuario(1202, 88, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'S');
ie_alterar_cart_benef_w		:= nvl(obter_valor_param_usuario(1202, 99, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'N');
ie_titular_pf_w			:= nvl(obter_valor_param_usuario(1202, 48, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');

if	(nvl(ie_opcao_p,0) <> 0) then
	select 	max(nr_seq_contrato),
		max(nr_seq_tabela)
	into	nr_seq_contrato_w,
		nr_seq_tabela_w
	from	pls_segurado
	where 	nr_sequencia = nr_seq_segurado_p;
	
	if	(nr_seq_tabela_w is not null) then
		select	nvl(ie_preco_vidas_contrato,'N')
		into	ie_preco_vidas_contrato_w
		from	pls_tabela_preco
		where	nr_sequencia = nr_seq_tabela_w;
	else
		ie_preco_vidas_contrato_w	:= 'N';
	end if;
	
	begin
	select	ie_geracao_valores,
		cd_cgc_estipulante
	into	ie_geracao_valores_w,
		cd_cgc_estipulante_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
	exception
	when others then
		ie_geracao_valores_w	:= 'B';
		cd_cgc_estipulante_w	:= '';
	end;

	if	(ie_titular_pf_w = 'S') and
		(cd_cgc_estipulante_w is null) then
		ie_alterar_matricula_benef_w	:= 'N';
	else
		ie_alterar_matricula_benef_w	:= 'S';
	end if;
	
	if	(nvl(ie_opcao_p,0) = 1) then
		select	nr_seq_titular,
			substr(pls_obter_dados_segurado(nr_seq_titular,'N'),1,255),
			dt_rescisao,
			ie_titularidade
		into	nr_seq_titular_ant_w,
			nm_titular_ant_w,
			dt_rescisao_w,
			ie_titularidade_w
		from	pls_segurado
		where	nr_sequencia	= nr_seq_segurado_p;
		
		update	pls_segurado
		set	nr_seq_titular		= null,
			ie_tipo_parentesco	= '',
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			nr_seq_parentesco	= null,
			cd_matricula_familia	= decode(ie_alterar_matricula_benef_w,'S',null,cd_matricula_familia)
		where	nr_sequencia	= nr_seq_segurado_p;
		
		pls_gerar_regra_titularidade(nr_seq_segurado_p,cd_estabelecimento_p,nm_usuario_p);

		if	(ie_alterar_matricula_benef_w = 'S') then
			pls_atualizar_familia_pf(nr_seq_segurado_p,cd_estabelecimento_p,nm_usuario_p);
					
			pls_gerar_segurado_historico(	nr_seq_segurado_p, '28', sysdate, wheb_mensagem_pck.get_texto(1109908),
							wheb_mensagem_pck.get_texto(1109909), null, null, null,
							null, sysdate, null, null,
							null, null, null, null,
							nm_usuario_p, 'N');					
		end if;
		
		select 	max(cd_matricula_familia)
		into	cd_matricula_familia_w
		from	pls_segurado
		where 	nr_sequencia = nr_seq_segurado_p;
		
		--Consistir o parametro [82] - Permite apenas 1 titular por familia no contrato
		if	(nvl(ie_titular_familia_w,'N') = 'S') then
			select	max(nr_sequencia)
			into	nr_seq_seg_titular_w
			from	pls_segurado
			where	nr_seq_contrato		= nr_seq_contrato_w
			and	cd_matricula_familia	= cd_matricula_familia_w
			and	nr_sequencia	<> nr_seq_segurado_p
			and	nr_seq_titular is null
			and	dt_liberacao is not null
			and	dt_rescisao is null;
			
			if	(nr_seq_seg_titular_w is not null) then
				select	max(b.nm_pessoa_fisica)
				into	nm_pessoa_fisica_w
				from	pls_segurado	a,
					pessoa_fisica	b
				where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
				and	a.nr_sequencia		= nr_seq_seg_titular_w;
				
				if	(nm_pessoa_fisica_w is not null) then
					wheb_mensagem_pck.exibir_mensagem_abort(193607,'NM_TITULAR='||nm_pessoa_fisica_w);
				end if;
			end if;
		end if;
		
		--Gerar o valor para o beneficiario, caso o mesmo
		if	(nvl(ie_geracao_valores_w,'B') <> 'B') then
			pls_gerar_valor_segurado(
					null, nr_seq_segurado_p, 'T',
					cd_estabelecimento_p, nm_usuario_p, 'S',
					sysdate, ie_permite_tab_dif_w, ie_consiste_tab_contr_w,
					'N', 'N');
		end if;
		
		--Caso o dependente esteja rescindido, entao deve ser reativado
		if	(dt_rescisao_w is not null) then
			ie_reativa_titular_w	:= 'S';
			pls_reativar_segurado(	nr_seq_segurado_p,sysdate, wheb_mensagem_pck.get_texto(1109913),
						'S', nm_usuario_p);
		end if;
		
		if	(ie_titular_pf_w = 'S') and
			(cd_cgc_estipulante_w is null) then
			open C01;
			loop
			fetch C01 into
				nr_seq_seg_dependente_w,
				dt_rescisao_w;
			exit when C01%notfound;
				begin
				
				update	pls_segurado
				set	nr_seq_titular	= nr_seq_segurado_p
				where	nr_sequencia	= nr_seq_seg_dependente_w;
				
				--Caso o dependente esteja rescindido, entao deve ser reativado
				if	((dt_rescisao_w is not null) and (ie_reativa_titular_w = 'S')) then
					pls_reativar_segurado(	nr_seq_seg_dependente_w,sysdate, wheb_mensagem_pck.get_texto(1109914),
								'S', nm_usuario_p);
				end if;
				
				pls_gerar_segurado_historico(
					nr_seq_seg_dependente_w, '12', sysdate,
					wheb_mensagem_pck.get_texto(1109915,'NM_TITULAR_ANT='|| nm_titular_ant_w), ds_observacao_p, null,
					null, null, null,
					dt_geracao_sib_p, null, null,
					null, null, null,
					null, nm_usuario_p, 'N');
				
				if	(ie_alterar_matricula_benef_w = 'S') then
					--Atualizar a familia do beneficiario
					pls_atualizar_familia_pf(nr_seq_seg_dependente_w,cd_estabelecimento_p,nm_usuario_p);				
						
					pls_gerar_segurado_historico(	nr_seq_segurado_p, '28', sysdate, wheb_mensagem_pck.get_texto(1109908),
									wheb_mensagem_pck.get_texto(1109909), null, null, null,
									null, sysdate, null, null,
									null, null, null, null,
									nm_usuario_p, 'N');							
				end if;
				
				end;
			end loop;
			close C01;
			
			select	max(dt_rescisao)
			into	dt_rescisao_tit_w
			from	pls_segurado
			where	nr_sequencia	= nr_seq_titular_ant_w;
			
			--Apenas pode rescindir o titular caso o mesmo nao esteja rescindido
			if	(dt_rescisao_tit_w is null) then
				if	(nvl(nr_seq_mtvo_rescisao_p,0) <> 0) then
					pls_rescindir_segurado(nr_seq_titular_ant_w, dt_rescisao_p, dt_rescisao_p, nr_seq_mtvo_rescisao_p, wheb_mensagem_pck.get_texto(1109924), cd_estabelecimento_p, nm_usuario_p, 'B', 'N',null, null);
				else
					update	pls_segurado
					set	dt_rescisao		= sysdate,
						nm_usuario		= nm_usuario_p,
						dt_atualizacao		= sysdate,
						ie_situacao_atend	= 'I',
						ie_tipo_rescisao	= 'B'
					where	nr_sequencia		= nr_seq_titular_ant_w;
					
					--Rescindir a carteirinha
					update	pls_segurado_carteira
					set	dt_validade_carteira	= sysdate,
						nm_usuario		= nm_usuario_p,
						dt_atualizacao		= sysdate
					where	nr_seq_segurado		= nr_seq_titular_ant_w
					and	dt_validade_carteira is null;
					
					ds_observacao_w	:= wheb_mensagem_pck.get_texto(1109924);
					
					pls_gerar_segurado_historico(	nr_seq_segurado_p, '28', sysdate, wheb_mensagem_pck.get_texto(1109911),
									ds_observacao_w, null, null, null,
									null, sysdate, null, null,
									null, null, null, null,
									nm_usuario_p, 'N');							
				end if;
			end if;
		end if;
		
		pls_gerar_segurado_historico(
			nr_seq_segurado_p, '12', sysdate,
			wheb_mensagem_pck.get_texto(1109916,'NM_TITULAR_ANT='|| nm_titular_ant_w), ds_observacao_p, null,
			null, null, null,
			sysdate, null, null,
			nr_seq_titular_ant_w, null, null,
			null, nm_usuario_p, 'N');
	elsif	(nvl(ie_opcao_p,0) = 2) then
		pls_consiste_data_sib(dt_geracao_sib_p, nm_usuario_p, cd_estabelecimento_p);
		select	decode(max(a.ie_titularidade),null,'T','D'),
			max(b.nm_pessoa_fisica),
			max(b.ie_estado_civil)
		into	ie_titularidade_w,
			nm_beneficiario_w,
			ie_estado_civil_w
		from	pls_segurado a,
			pessoa_fisica b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.nr_sequencia = nr_seq_segurado_p;	
		
		select	max(ie_tipo_parentesco)
		into	ie_tipo_parentesco_w
		from	grau_parentesco
		where	nr_sequencia	= nr_grau_parentesco_p;

		qt_idade_w := pls_obter_idade_segurado(nr_seq_segurado_p,dt_geracao_sib_p,'A');

		select	count(1)
		into	qt_registros_w
		from	pls_restricao_inclusao_seg
		where	nr_seq_contrato	= nr_seq_contrato_w
		and	dt_geracao_sib_p between trunc(nvl(dt_inicio_vigencia,dt_geracao_sib_p),'dd') and fim_dia(nvl(dt_fim_vigencia,dt_geracao_sib_p))
		and     ie_titularidade in ('D','A')
		and 	(qt_idade_w  between nvl(qt_idade_inicial,qt_idade_w) and nvl(qt_idade_final,qt_idade_w))
		and	(nr_seq_parentesco = nr_grau_parentesco_p or nr_seq_parentesco is null)
		and	(ie_estado_civil = ie_estado_civil_w or ie_estado_civil is null);

		if	(qt_registros_w	> 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1109938, 'NM_BENEFICIARIO=' || nm_beneficiario_w || ';DS_PARENTESCO='|| substr(Obter_desc_parentesco(nr_grau_parentesco_p),1,50));
			--Mensagem: Nao e permitido alterar o beneficiario #@NM_BENEFICIARIO#@ para dependente, com o grau de parentesco #@DS_PARENTESCO#@.Favor Verificar as regras de restricao de inclusao do contrato.
		end if;		
		
		update	pls_segurado
		set	nr_seq_titular		= nr_seq_titular_p,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			nr_seq_parentesco	= nr_grau_parentesco_p,
			ie_tipo_parentesco	= ie_tipo_parentesco_w
		where	nr_sequencia		= nr_seq_segurado_p;
		
		
		pls_gerar_regra_titularidade(nr_seq_segurado_p,cd_estabelecimento_p,nm_usuario_p);
		
		--Gerar o valor para o beneficiario, caso o mesmo
		if	(nvl(ie_geracao_valores_w,'B') <> 'B') then
			pls_gerar_valor_segurado(
					null, nr_seq_segurado_p, 'T',
					cd_estabelecimento_p, nm_usuario_p, 'S',
					sysdate, ie_permite_tab_dif_w, ie_consiste_tab_contr_w,
					'N', 'N');
		end if;
		
		pls_gerar_segurado_historico(
			nr_seq_segurado_p, '12', sysdate,
			wheb_mensagem_pck.get_texto(1109917), ds_observacao_p, null,
			null, null, null,
			dt_geracao_sib_p, null, null,
			null, null, null,
			null, nm_usuario_p, 'N');
		
		--Atualizar a familia do beneficiario
		if	(ie_alterar_matricula_benef_w = 'S') then
			pls_atualizar_familia_pf(nr_seq_segurado_p,cd_estabelecimento_p,nm_usuario_p);	

			pls_gerar_segurado_historico(	nr_seq_segurado_p, '28', sysdate, wheb_mensagem_pck.get_texto(1109908),
							 wheb_mensagem_pck.get_texto(1109909), null, null, null,
							null, sysdate, null, null,
							null, null, null, null,
							nm_usuario_p, 'N');
		end if;
	elsif	(nvl(ie_opcao_p,0) = 3) then
		pls_consiste_data_sib(dt_geracao_sib_p, nm_usuario_p, cd_estabelecimento_p);
		
		select	a.nr_seq_titular,
			substr(pls_obter_dados_segurado(a.nr_seq_titular,'N'),1,255),
			decode((a.ie_titularidade),null,'T','D'),
			b.nm_pessoa_fisica,
			b.ie_estado_civil
		into	nr_seq_titular_w,
			nm_titular_w,
			ie_titularidade_w,
			nm_beneficiario_w,
			ie_estado_civil_w
		from	pls_segurado a,
			pessoa_fisica b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	nr_sequencia	= nr_seq_segurado_p;

		qt_idade_w := pls_obter_idade_segurado(nr_seq_segurado_p,dt_geracao_sib_p,'A');

		select	count(1)
		into	qt_registros_w
		from	pls_restricao_inclusao_seg
		where	nr_seq_contrato	= nr_seq_contrato_w
		and	dt_geracao_sib_p between trunc(nvl(dt_inicio_vigencia,dt_geracao_sib_p),'dd') and fim_dia(nvl(dt_fim_vigencia,dt_geracao_sib_p))
		and    	ie_titularidade in ('D','A')
		and 	(qt_idade_w  between nvl(qt_idade_inicial,qt_idade_w) and nvl(qt_idade_final,qt_idade_w))
		and	(nr_seq_parentesco = nr_grau_parentesco_p or nr_seq_parentesco is null)
		and	(ie_estado_civil = ie_estado_civil_w or ie_estado_civil is null);

		if	(qt_registros_w	> 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(1109939, 'NM_BENEFICIARIO=' || nm_beneficiario_w || ';DS_PARENTESCO='|| substr(Obter_desc_parentesco(nr_grau_parentesco_p),1,50));
			--Mensagem: Nao e permitido alterar o titular do beneficiario #@NM_BENEFICIARIO#@, com o grau de parentesco #@DS_PARENTESCO#@.Favor Verificar as regras de restricao de inclusao do contrato..
		end if;		
		
		if	(nr_seq_titular_w <> nr_seq_titular_p) then
			update	pls_segurado
			set	nr_seq_titular		= nr_seq_titular_p,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				nr_seq_parentesco	= decode(nr_grau_parentesco_p,0,nr_seq_parentesco,nr_grau_parentesco_p)
			where	nr_sequencia		= nr_seq_segurado_p;
			
			--Gerar historico
			pls_gerar_segurado_historico(
				nr_seq_segurado_p, '12', sysdate,
				wheb_mensagem_pck.get_texto(1109915,'NM_TITULAR_ANT='|| nm_titular_w), ds_observacao_p, null,
				null, null, null,
				dt_geracao_sib_p, null, null,
				nr_seq_titular_w, null, null,
				null, nm_usuario_p, 'S');

			pls_atualizar_familia_pf(nr_seq_segurado_p,cd_estabelecimento_p,nm_usuario_p);			

			pls_gerar_segurado_historico(	nr_seq_segurado_p, '28', sysdate, wheb_mensagem_pck.get_texto(1109908),
							wheb_mensagem_pck.get_texto(1109909), null, null, null,
							null, sysdate, null, null,
							null, null, null, null,
							nm_usuario_p, 'N');					
		end if;
	end if;
	
	pls_preco_beneficiario_pck.atualizar_desconto_benef(nr_seq_contrato_w, sysdate, null, 'N', nm_usuario_p, cd_estabelecimento_p);
	
	select	nr_seq_titular
	into	nr_seq_titular_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_p;
	
	pls_att_classif_dependencia(nvl(nr_seq_titular_w,nr_seq_segurado_p),nm_usuario_p,'N');
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_segurado_w;
	exit when C02%notfound;
		begin
		pls_preco_beneficiario_pck.gravar_preco_benef(nr_seq_segurado_w, 'Q', 'S', sysdate, 'N', null, nm_usuario_p, cd_estabelecimento_p);
		end;
	end loop;
	close C02;
end if;

pls_gerar_valor_sca_segurado(nr_seq_segurado_p, 'T', sysdate, nm_usuario_p, cd_estabelecimento_p);

if	(((ie_alterar_cart_benef_w = 'S') or
	(ie_alterar_cart_benef_w = 'PF') and
	(cd_cgc_estipulante_w is null)) or
	(ie_alterar_cart_benef_w = 'PJ') and
	(cd_cgc_estipulante_w is not null)) then
	pls_alterar_cartao_ident_benef(nr_seq_segurado_p,dt_geracao_sib_p,cd_estabelecimento_p,nm_usuario_p);
end if;

commit;

end pls_alterar_titularidade;
/
