CREATE OR REPLACE procedure hd_teste_reuso_apc(	nr_teste_p			number,
		nr_seq_dialisador_p			number,
		cd_estabelecimento_p		number,
		dt_teste_p			date,
		ie_resultado_p			varchar2,
		nm_usuario_p			varchar2) is


nr_seq_unid_dialise_w		number(10,0);
nr_seq_hd_teste_reuso_w	hd_teste_reuso.nr_sequencia%type;
ie_parametro_90_w 	varchar2(1);
qt_reuso_w			number(2);
qt_reuso_max_w			number(10);
ie_tipo_w			varchar2(1);
begin
ie_parametro_90_w := obter_valor_param_usuario(7009, 90, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);

if	(ie_resultado_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(233282);
end if;

select	hd_teste_reuso_seq.nextval
into	nr_seq_hd_teste_reuso_w
from 	dual;
/* Insere os dados do teste */
insert into hd_teste_reuso (
	nr_sequencia,
  cd_estabelecimento,
  dt_atualizacao,
  nm_usuario,
  dt_atualizacao_nrec,
  nm_usuario_nrec,
  dt_teste,
  nr_teste,
  ie_resultado,
  cd_pf_teste,
  nr_seq_dialisador
) values (
	nr_seq_hd_teste_reuso_w,
  cd_estabelecimento_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	dt_teste_p,
  nr_teste_p,
	ie_resultado_p,
	substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10),
	nr_seq_dialisador_p
);

if	(ie_parametro_90_w = 'N') and
	(nr_teste_p = 1) and
	(ie_resultado_p = 'N') then
	/* Atualizar status do dialisador */
	update	hd_dializador
	set    	ie_status              	= 'P'
	where  	nr_sequencia		= nr_seq_dialisador_p;

	/* Verifica se chegou ao reuso m�ximo, neste caso o dialisador ser� descartado de forma autom�tica */
	select	max(qt_reuso),
		max(nr_max_reuso),
		max(ie_tipo)
	into	qt_reuso_w,
		qt_reuso_max_w,
		ie_tipo_w	
	from	hd_dializador
	where	nr_sequencia		= nr_seq_dialisador_p;	

	if	(ie_tipo_w = 'U') then /* Envia dialisador de �nico uso para descarte */
		HD_Descarte_Dialisador(nr_seq_dialisador_p, 'UU', 'U', nm_usuario_p);				
	elsif	(qt_reuso_w+1 > qt_reuso_max_w) and (qt_reuso_max_w > 0) then /* Envia dialisador com reuso m�ximo para descarte - SOMENTE PARA DIALISADORES QUE N�O S�O DE 1� USO*/
		HD_Descarte_Dialisador(nr_seq_dialisador_p, 'RE', 'U', nm_usuario_p);			
	end if;
end if;

commit;

end hd_teste_reuso_apc;
/
