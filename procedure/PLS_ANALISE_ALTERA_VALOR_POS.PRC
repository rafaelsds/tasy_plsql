create or replace
procedure pls_analise_altera_valor_pos
			(	nr_seq_analise_p	number,
				nr_seq_conta_pos_p	number,
				nr_seq_w_item_p		number,
				vl_total_p		number,
				qt_item_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

vl_administracao_w	pls_conta_pos_estabelecido.vl_administracao%type;
tx_administracao_w	pls_conta_pos_estabelecido.tx_administracao%type;
vl_medico_w		pls_conta_pos_estabelecido.vl_medico%type	:= 0;
vl_materiais_w		pls_conta_pos_estabelecido.vl_materiais%type	:= 0;
ie_tipo_item_w		varchar2(1);
begin

select	max(ie_tipo_item)
into	ie_tipo_item_w
from	(select	'P' ie_tipo_item
	from	pls_conta_pos_estabelecido
	where	nr_sequencia		= nr_seq_conta_pos_p
	and	nr_seq_conta_proc	is not null
	and	((ie_situacao		= 'A')or (ie_situacao	is null))
	union
	select	'M' ie_tipo_item
	from	pls_conta_pos_estabelecido
	where	nr_sequencia		= nr_seq_conta_pos_p
	and	nr_seq_conta_mat	is not null
	and	((ie_situacao		= 'A')or (ie_situacao	is null)));
	
select	max(tx_administracao)
into	tx_administracao_w
from	pls_conta_pos_estabelecido
where	nr_sequencia	= nr_seq_conta_pos_p
and	((ie_situacao		= 'A')or (ie_situacao	is null));

vl_administracao_w	:= dividir((vl_total_p),100 + nvl(tx_administracao_w,0)) * tx_administracao_w;

if	(ie_tipo_item_w	= 'M') then	
	vl_materiais_w	:= vl_total_p - vl_administracao_w;
	update	pls_conta_pos_estabelecido
	set	vl_beneficiario		= vl_total_p,
		vl_materiais		= vl_materiais_w,
		vl_custo_operacional	= 0,
		vl_medico		= 0,
		qt_item			= qt_item_p,
		vl_administracao	= vl_administracao_w,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_conta_pos_p
	and	((ie_situacao		= 'A')or (ie_situacao	is null));
else
	vl_medico_w		:= vl_total_p - vl_medico_w;

	update	pls_conta_pos_estabelecido
	set	vl_beneficiario		= vl_total_p,
		vl_materiais		= 0,
		vl_custo_operacional	= 0,
		vl_medico		= vl_medico_w,
		qt_item			= qt_item_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_conta_pos_p
	and	((ie_situacao		= 'A')or (ie_situacao	is null));
end if;

commit;

end pls_analise_altera_valor_pos;
/
