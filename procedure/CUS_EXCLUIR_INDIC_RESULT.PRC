create or replace
procedure cus_excluir_indic_result(	cd_estabelecimento_p	number,
				cd_tabela_custo_p		number,
				nr_seq_indicador_p		number,
				nr_seq_tabela_p		number,
				nm_usuario_p		Varchar2) is 

begin
delete	from resultado_ind_cen_cont
where	cd_estabelecimento		= cd_estabelecimento_p
and	nr_seq_tabela		= nr_seq_tabela_p
and	nr_seq_indicador		= nr_seq_indicador_p;

commit;

end cus_excluir_indic_result;
/