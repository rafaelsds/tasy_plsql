create or replace trigger escala_horario_medico_lib_atua
before insert or update on escala_horario_medico_lib
for each row

declare

begin
begin
    if (:new.hr_final_vinc is not null) and ((:new.hr_final_vinc <> :old.hr_final_vinc) or (:old.dt_final_vinc is null)) then
		:new.dt_final_vinc := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_final_vinc,'dd/mm/yyyy hh24:mi');
	end if;
    if (:new.hr_inicial_vinc is not null) and ((:new.hr_inicial_vinc <> :old.hr_inicial_vinc) or (:old.dt_inicial_vinc is null)) then	
		:new.dt_inicial_vinc := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicial_vinc,'dd/mm/yyyy hh24:mi');
	end if;	
exception
	when others then
	null;
end;

end;
/