create or replace
procedure ws_diagnostika_atual_laudo(id_diagnostica_p number,
									  id_origem_p number,
									  versao_p Varchar2,
									  nm_paciente_p Varchar2,
									  ds_procedencia_p Varchar2,
									  dt_entrada_p Varchar2,
									  dt_saida_p Varchar2,
									  ds_convenio_p Varchar2,
									  nm_medico_solic_p Varchar2,
									  crm_medico_solic_p Varchar2,
									  crmuf_medico_solic_p Varchar2,
									  nm_medico_particip_p Varchar2,
									  crm_medico_particip_p Varchar2,
									  crmuf_medico_particip_p Varchar2,
									  nm_medico1_laudo_p  Varchar2,
									  crm_medico1_laudo_p Varchar2,
									  crmuf_medico1_laudo_p Varchar2,
									  nm_medico2_laudo_p Varchar2,
									  crm_medico2_laudo_p Varchar2,
									  crmuf_medico2_laudo_p Varchar2,
									  ds_exame_procedimento_p Varchar2,
									  ds_dados_clinicos_p Varchar2,
									  ds_painel_anti_p Varchar2,
									  ds_coloracoes_p Varchar2,
									  ds_imagens_p Varchar2,
									  ds_per_operatorio_p Varchar2,
									  ds_ref_biblio_p Varchar2,
									  ds_microscopia_p Varchar2,
									  ds_macroscopia_p Varchar2,
									  ds_conclusao_p Varchar2,
									  ds_local_colheita_p Varchar2,
									  ds_avaliacao_material_p Varchar2,
									  ds_observacao_p Varchar2,
									  ds_dados_diag_descritivo_p Varchar2,
									  ds_avaliacao_horm_p Varchar2,
									  ds_avaliacao_micro_p Varchar2,
									  ds_outros_achados_p Varchar2,
									  ds_indicador_clinico_p Varchar2,
									  ds_historico_clinico_p varchar2,
									  nr_seq_laudo_principal_p Integer,
									  ds_log_p	out	varchar2,
									  ie_erro_p	out varchar2,
									  nr_seq_laudo_inserido_p out number) is 
									  
									  
ds_laudo_w				long;	
nr_seq_laudo_w			laudo_paciente.nr_sequencia%type;	

ds_log_w				varchar2(255);
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_medico_w				pessoa_fisica.cd_pessoa_fisica%type;
cd_medico_aprov1_w		pessoa_fisica.cd_pessoa_fisica%type;
cd_medico_aprov2_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_exame_w				prescr_procedimento.nr_seq_lab%type;

dt_entrada_unidade_w	date;
dt_saida_exame_w		date;			
dt_seg_aprovacao_w		date;
dt_aprovacao_w			date;

nm_usuario_aprov1_w		usuario.nm_usuario%type;
nm_usuario_aprov2_w		usuario.nm_usuario%type;

ds_laudo_principal_W	long;



begin
nr_seq_laudo_inserido_p := 0;
ds_laudo_w	:=	'';
nm_usuario_aprov1_w := '';
nm_usuario_aprov2_w := '';
--Atribuido 1 ao nr_exame pois n�o h� prescr_procedimento
nr_exame_w	:= '';
dt_entrada_unidade_w := to_date(to_char(to_date(dt_entrada_p,'yyyy-mm-dd'),'dd/mm/yyyy'),'dd/mm/yyyy');
dt_saida_exame_w	 := to_date(to_char(to_date(dt_saida_p,'yyyy-mm-dd'),'dd/mm/yyyy'),'dd/mm/yyyy');
dt_seg_aprovacao_w	 := null;
dt_aprovacao_w		 := null;
cd_medico_aprov2_w	 := null;

if 	(crm_medico_solic_p is not null) and 
	(crmuf_medico_solic_p is not null)  then
	
	select distinct 
		   MAX(cd_pessoa_fisica)
	into   cd_medico_w
	from   medico 
	where  nr_crm = crm_medico_solic_p
	and    uf_crm = crmuf_medico_solic_p;
	
	if (cd_medico_w is null) then
		ds_log_w := 'M�dico resp n�o encontrado: '||crm_medico_solic_p||' - '||crmuf_medico_solic_p||CHR(13)||CHR(10);
	end if;
	
end if;
if 	(crm_medico1_laudo_p is not null) and 
	(crmuf_medico1_laudo_p is not null)  then
	
	select MAX(nvl(b.nm_usuario,trim(substr(nm_guerra,1,15)))),
			MAX(a.cd_pessoa_fisica)
	into   nm_usuario_aprov1_w,
		   cd_medico_aprov1_w
	from   medico a,
		   usuario b
	where  a.cd_pessoa_fisica = b.cd_pessoa_fisica (+)
	and	   nvl(b.ie_situacao,'A') = 'A' 
	and	   a.nr_crm = crm_medico1_laudo_p
	and    a.uf_crm = crmuf_medico1_laudo_p;
	
	if (nm_usuario_aprov1_w is null) then
		ds_log_w := ds_log_w ||'M�dico 1 aprova��o n�o encontrado: '||crm_medico1_laudo_p||' - '||crmuf_medico1_laudo_p||CHR(13)||CHR(10);
	end if;
	
	dt_aprovacao_w := sysdate;
	
end if;

if 	(crm_medico2_laudo_p is not null) and 
	(crmuf_medico2_laudo_p is not null)  then
	
	select 	MAX(nvl(b.nm_usuario,trim(substr(nm_guerra,1,15)))),
			MAX(a.cd_pessoa_fisica)
	into   	nm_usuario_aprov2_w,
			cd_medico_aprov2_w
	from   	medico a,
			usuario b
	where 	a.cd_pessoa_fisica = b.cd_pessoa_fisica (+)
	and	  	nvl(b.ie_situacao,'A') = 'A' 
	and	  	a.nr_crm = crm_medico2_laudo_p
	and   	a.uf_crm = crmuf_medico2_laudo_p;
	
	if (nm_usuario_aprov2_w is null) then
		ds_log_w := ds_log_w ||'M�dico 2 aprova��o n�o encontrado: '||crm_medico2_laudo_p||' - '||crmuf_medico2_laudo_p||CHR(13)||CHR(10);
	end if;
	
	dt_seg_aprovacao_w := sysdate;
	
end if;

select 	MAX(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = id_origem_p;

ds_laudo_w	:= 'C�digo de identifica��o externo:'||id_diagnostica_p||CHR(13)||CHR(10);			
ds_laudo_w	:= ds_laudo_w ||'Vers�o do laudo:'||versao_p||CHR(13)||CHR(10);			
ds_laudo_w	:= ds_laudo_w ||'Paciente:'||nm_paciente_p||CHR(13)||CHR(10);			
ds_laudo_w	:= ds_laudo_w ||'Proced�ncia:'||ds_procedencia_p||CHR(13)||CHR(10);			
ds_laudo_w	:= ds_laudo_w ||'Data de entrada da requisi��o:'||dt_entrada_p||CHR(13)||CHR(10);			
ds_laudo_w	:= ds_laudo_w ||'Data de finaliza��o do exame:'||dt_saida_p||CHR(13)||CHR(10);
ds_laudo_w	:= ds_laudo_w ||'M�dico solicitante:'||nm_medico_solic_p||CHR(13)||CHR(10);			
ds_laudo_w	:= ds_laudo_w ||'M�dico participante:'||nm_medico_particip_p||CHR(13)||CHR(10)||CHR(13)||CHR(10);

if (ds_historico_clinico_p is not null) then
	ds_laudo_w := ds_laudo_w||'Hist�rico cl�nico:'||CHR(13)||CHR(10);	
	ds_laudo_w := ds_laudo_w||ds_historico_clinico_p||CHR(13)||CHR(10);
end if;
if (ds_exame_procedimento_p is not null) then
	ds_laudo_w := 'Procedimento: '||ds_exame_procedimento_p||CHR(13)||CHR(10);
end if;	
if (ds_dados_clinicos_p is not null) then
	ds_laudo_w := ds_laudo_w||'Dados cl�nicos: '||ds_exame_procedimento_p||CHR(13)||CHR(10);
end if;	
if (ds_painel_anti_p is not null) then
	ds_laudo_w := ds_laudo_w||'Painel: '||ds_painel_anti_p||CHR(13)||CHR(10);
end if;	
if (ds_coloracoes_p is not null) then
	ds_laudo_w := ds_laudo_w||'Colora��es: '||ds_coloracoes_p||CHR(13)||CHR(10);
end if;	
if (ds_imagens_p is not null) then
	ds_laudo_w := ds_laudo_w||'Imagens: '||ds_imagens_p||CHR(13)||CHR(10);
end if;	
if (ds_per_operatorio_p is not null) then
	ds_laudo_w := ds_laudo_w||'Operat�rio: '||ds_per_operatorio_p||CHR(13)||CHR(10);
end if;	
if (ds_ref_biblio_p is not null) then
	ds_laudo_w := ds_laudo_w||'Refer�ncia: '||ds_ref_biblio_p||CHR(13)||CHR(10);
end if;	
if (ds_microscopia_p is not null) then
	ds_laudo_w := ds_laudo_w||'Microscopia:'||CHR(13)||CHR(10);
	ds_laudo_w := ds_laudo_w||ds_microscopia_p||CHR(13)||CHR(10);
end if;	
if (ds_macroscopia_p is not null) then
	ds_laudo_w := ds_laudo_w||'Macroscopia:'||CHR(13)||CHR(10);
	ds_laudo_w := ds_laudo_w||ds_macroscopia_p||CHR(13)||CHR(10);
end if;	
if (ds_conclusao_p is not null) then
	ds_laudo_w := ds_laudo_w||'Conclus�o '||ds_conclusao_p||CHR(13)||CHR(10);
end if;	
if (ds_local_colheita_p is not null) then
	ds_laudo_w := ds_laudo_w||'Local da colheita: '||ds_local_colheita_p||CHR(13)||CHR(10);
end if;	
if (ds_avaliacao_material_p is not null) then
	ds_laudo_w := ds_laudo_w||'Avalia��o material: '||ds_avaliacao_material_p||CHR(13)||CHR(10);
end if;	
if (ds_observacao_p is not null) then
	ds_laudo_w := ds_laudo_w||'Observa��o: '||ds_observacao_p||CHR(13)||CHR(10);
end if;
if (ds_dados_diag_descritivo_p is not null) then
	ds_laudo_w := ds_laudo_w||'Diagn�stico descritivo: '||ds_dados_diag_descritivo_p||CHR(13)||CHR(10);
end if;	
if (ds_avaliacao_horm_p is not null) then
	ds_laudo_w := ds_laudo_w||'Avalia��o hormonal: '||ds_avaliacao_horm_p||CHR(13)||CHR(10);
end if;	
if (ds_avaliacao_micro_p is not null) then
	ds_laudo_w := ds_laudo_w||'Avalia��o micro '||ds_avaliacao_micro_p||CHR(13)||CHR(10);
end if;	
if (ds_outros_achados_p is not null) then
	ds_laudo_w := ds_laudo_w||'Outros achados: '||ds_outros_achados_p||CHR(13)||CHR(10);
end if;	
if (ds_indicador_clinico_p is not null) then
	ds_laudo_w := ds_laudo_w||'Indicador cl�nico: '||ds_indicador_clinico_p||CHR(13)||CHR(10);
end if;
if (nm_medico1_laudo_p is not null) then
	ds_laudo_w := ds_laudo_w||'Primeiro m�dico laudante: '||nm_medico1_laudo_p||CHR(13)||CHR(10);
end if;
if (nm_medico2_laudo_p is not null) then
	ds_laudo_w := ds_laudo_w||'Segundo m�dico laudante: '||nm_medico2_laudo_p||CHR(13)||CHR(10);
end if;


select laudo_paciente_seq.nextval
into   nr_seq_laudo_w
from   dual;

/*gravar_log_tasy(30,substr(ds_laudo_w,1,255),'TESTE');
commit;
*/

	if (ds_laudo_w is not null) then
	begin
		/*Se possui laudo principal, entao alterar o laudo acrescentando o laudo complementar, caso contr�rio inserir um novo laudo:*/
		if (nr_seq_laudo_principal_p > 0) then
		begin
			select	ds_laudo
			into 	ds_laudo_principal_W
			from	laudo_paciente
			where	nr_sequencia = nr_seq_laudo_principal_p;
			
			update	laudo_paciente
			set		ds_laudo = ds_laudo_principal_W ||CHR(13)||CHR(10)||'Laudo Complementar: '||CHR(13)||CHR(10)|| ds_laudo_w ||CHR(13)||CHR(10)
			where	nr_sequencia = nr_seq_laudo_principal_p;			
		end;
		else
		begin	
			insert into laudo_paciente (nr_sequencia,
										nr_laudo,
										nr_atendimento,
										dt_entrada_unidade,
										cd_pessoa_fisica,
										nm_usuario,
										dt_atualizacao,
										qt_imagem,
										nr_exame,
										cd_medico_resp,
										dt_aprovacao,
										dt_exame,
										dt_integracao,
										dt_laudo,
										dt_liberacao,
										dt_seg_aprovacao,
										ie_normal,
										nm_medico_solicitante,
										nm_usuario_aprovacao,
										nm_usuario_seg_aprov,
										ds_titulo_laudo,
										ds_laudo)
									values (nr_seq_laudo_w,
										1,
										id_origem_p,
										dt_entrada_unidade_w,
										cd_pessoa_fisica_w,
										'DiagnostikaWS',
										sysdate,
										0,
										nr_exame_w,
										nvl(cd_medico_aprov2_w,cd_medico_aprov1_w),
										dt_aprovacao_w,
										dt_saida_exame_w,
										sysdate,
										dt_saida_exame_w,
										dt_saida_exame_w,
										dt_seg_aprovacao_w,
										' ',
										nm_medico_solic_p,
										nm_usuario_aprov1_w,
										nm_usuario_aprov2_w,
										'Exame de An�tomo Diagn�stika',
										ds_laudo_w
										);
										
			nr_seq_laudo_inserido_p := nr_seq_laudo_w;
		end;
		end if;
			
		exception
		when others then
			ie_erro_p := 'S';
			ds_log_w := ds_log_w ||'Erro: '||substr(sqlerrm,1,255)||CHR(13)||CHR(10);
		end;
			
	end if;
							
commit;

ds_log_p := ds_log_w;

end ws_diagnostika_atual_laudo;
/
