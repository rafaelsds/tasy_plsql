create or replace
procedure cm_gera_recebimento(
			nr_seq_expurgo_p	number,
			cd_setor_receb_p	number,
			cd_local_entrega_p	number,
			nm_usuario_entrega_p	varchar2,
			ds_observacao_p		varchar2,
			nm_usuario_p		varchar2) is 

nr_seq_conjunto_w	number(10);
nr_conjunto_cont_w	number(10);
cd_estabelecimento_w	number(5);

begin

select	nr_conjunto_cont
into	nr_conjunto_cont_w
from	cm_expurgo_receb
where	nr_sequencia = nr_seq_expurgo_p;

update	cm_expurgo_receb
set	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	nm_usuario_receb = nm_usuario_p,
	cd_setor_receb = cd_setor_receb_p,
	nm_usuario_entrega = nm_usuario_entrega_p,
	cd_local_estoque = cd_local_entrega_p,
	dt_recebimneto = sysdate,
	ds_observacao = ds_observacao_p
where	nr_sequencia = nr_seq_expurgo_p;

update	cm_conjunto_cont
set	cd_local_estoque = cd_local_entrega_p
where	nr_sequencia = nr_conjunto_cont_w;

select	nr_seq_conjunto,
	cd_estabelecimento
into	nr_seq_conjunto_w,
	cd_estabelecimento_w
from	cm_expurgo_receb
where	nr_sequencia = nr_seq_expurgo_p;

cm_gera_expurgo_saldo(nr_seq_conjunto_w, cd_local_entrega_p, 'A', cd_estabelecimento_w, nm_usuario_p);

commit;

end cm_gera_recebimento;
/