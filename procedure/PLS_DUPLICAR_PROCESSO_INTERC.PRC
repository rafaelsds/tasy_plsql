create or replace
procedure pls_duplicar_processo_interc(	nm_usuario_p			Varchar2,
					nr_seq_processo_interc_p	number) is 

				
nr_seq_processo_evento_w		number(10);
nr_seq_processo_interc_w		number(10);
nr_seq_processo_evento_acao_w		number(10);
				
begin

if	(nr_seq_processo_interc_p is not null) then
	
	select	pls_regra_processo_interc_seq.nextval
	into	nr_seq_processo_interc_w
	from	dual;
	
	select	nr_sequencia
	into	nr_seq_processo_evento_acao_w
	from	pls_processo_interc_evento
	where	nr_seq_processo_interc = nr_seq_processo_interc_p; 
	
	insert	into pls_regra_processo_interc
		(cd_estabelecimento,      
		dt_atualizacao,          
		dt_atualizacao_nrec,     
		dt_fim_vigencia,        
		dt_inicio_vigencia,      
		dt_liberacao,            
		ie_envio_recebimento,
		ie_situacao,             
		nm_regra,                
		nm_usuario,              
		nm_usuario_nrec,         
		nr_seq_camara,           
		nr_seq_congenere,        
		nr_sequencia)            
	select	cd_estabelecimento,      
		sysdate dt_atualizacao,          
		sysdate dt_atualizacao_nrec,     
		dt_fim_vigencia,        
		dt_inicio_vigencia,      
		null dt_liberacao,            
		ie_envio_recebimento,
		'A' ie_situacao,             
		nm_regra,                
		nm_usuario_p,              
		nm_usuario_p,         
		nr_seq_camara,           
		nr_seq_congenere,        
		nr_seq_processo_interc_w
	from	pls_regra_processo_interc
	where	nr_sequencia	= nr_seq_processo_interc_p;

	select	pls_processo_interc_evento_seq.nextval
	into	nr_seq_processo_evento_w
	from 	dual;
	
	insert	into pls_processo_interc_evento
		(dt_atualizacao,          
		dt_atualizacao_nrec,     
		ie_evento,               
		nm_usuario,              
		nm_usuario_nrec,         
		nr_seq_processo_interc,
		nr_sequencia)
	select	sysdate dt_atualizacao,          
		sysdate dt_atualizacao_nrec,     
		ie_evento,               
		nm_usuario_p,              
		nm_usuario_p,         
		nr_seq_processo_interc_w,
		nr_seq_processo_evento_w            
	from	pls_processo_interc_evento
	where	nr_seq_processo_interc = nr_seq_processo_interc_p;
	
	insert	into pls_processo_interc_acao
		(dt_atualizacao,            
		dt_atualizacao_nrec,             
		ie_acao,                   
		ie_tipo_titulo_pagar,            
		ie_tipo_titulo_receber,          
		ie_tit_pagar_lib_pagamento,
		nm_usuario,                
		nm_usuario_nrec,                 
		nr_seq_processo_evento,    
		nr_seq_trans_fin_baixa,          
		nr_seq_trans_fin_cancel,         
		nr_seq_trans_fin_contab,         
		nr_sequencia,              
		qt_dias_antes_a500,              
		qt_dias_vencimento)
	select	sysdate dt_atualizacao,            
		sysdate dt_atualizacao_nrec,             
		ie_acao,                   
		ie_tipo_titulo_pagar,            
		ie_tipo_titulo_receber,          
		ie_tit_pagar_lib_pagamento,
		nm_usuario_p,                
		nm_usuario_p,                 
		nr_seq_processo_evento_w,    
		nr_seq_trans_fin_baixa,          
		nr_seq_trans_fin_cancel,         
		nr_seq_trans_fin_contab,         
		pls_processo_interc_acao_seq.nextval,              
		qt_dias_antes_a500,              
		qt_dias_vencimento
	from	pls_processo_interc_acao
	where	nr_seq_processo_evento = nr_seq_processo_evento_acao_w;	
	
	commit;
end if;

end pls_duplicar_processo_interc;
/