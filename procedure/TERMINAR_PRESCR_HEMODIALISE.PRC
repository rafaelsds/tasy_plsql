create or replace
PROCEDURE terminar_prescr_hemodialise(nr_atendimento_p number,
				     nr_prescricao_p  number,
				     dt_inicio_prescr_p date) is 
				     
				     
nr_prescr_anterior_w number(15);


begin				     
				     			    
select	max(nr_prescricao) 
into	nr_prescr_anterior_w
from	prescr_medica 
where	nr_prescricao < nr_prescricao_p
and	nr_atendimento = nr_atendimento_p
and	dt_validade_prescr is null
and	ie_hemodialise is not null;

if (nr_prescr_anterior_w is not null) then
	update	prescr_medica
	set	dt_validade_prescr 	= dt_inicio_prescr_p
	where	nr_prescricao	   	= nr_prescr_anterior_w;

end if;
	
commit;

end terminar_prescr_hemodialise;
/