CREATE OR REPLACE PROCEDURE atualizar_controle_dit_mmed(ds_lista_exames_p VARCHAR2,
                                                                                                            ie_opcao_p            VARCHAR2,
                                                                                                            nm_usuario_p         VARCHAR2) IS

nr_seq_interno_w     NUMBER(10);
ds_possib_w             VARCHAR2(6000);
qt_controle_w           NUMBER(10);
qt_pos_separador_w NUMBER(10);

BEGIN
  ds_possib_w := ds_lista_exames_p || ',';

  IF (INSTR(ds_possib_w,'(') > 0 )
  AND (INSTR(ds_possib_w,')') > 0 ) THEN
    ds_possib_w := SUBSTR(ds_lista_exames_p,(INSTR(ds_lista_exames_p,'(')+1),(INSTR(ds_lista_exames_p,')')-2));
  END IF;
  
  qt_controle_w      := 0;
  qt_pos_separador_w := INSTR(ds_possib_w,',');
  
  IF (qt_pos_separador_w = 0) THEN
    qt_pos_separador_w := -1;
  END IF;
  
  WHILE  (qt_pos_separador_w >= 0)
  AND (qt_controle_w < 1000) LOOP
  BEGIN
    IF(qt_pos_separador_w = 0) THEN
     nr_seq_interno_w   := TO_NUMBER(ds_possib_w);
     qt_pos_separador_w := -1;
    ELSE
      nr_seq_interno_w := TO_NUMBER(SUBSTR(ds_possib_w,1,qt_pos_separador_w-1));
    END IF;
    
    IF  (ie_opcao_p = 'I') THEN
      INSERT INTO controle_ditar_laudo (
        nm_usuario,
        nr_seq_interno,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        dt_atualizacao)
      VALUES(nm_usuario_p,
        nr_seq_interno_w,
        SYSDATE,
        nm_usuario_p,
        SYSDATE);
    ELSE
      DELETE
      FROM  controle_ditar_laudo
      WHERE  nr_seq_interno = nr_seq_interno_w;
    END IF;
   
    IF(qt_pos_separador_w > 0 ) THEN
      ds_possib_w        := SUBSTR(ds_possib_w,qt_pos_separador_w+1,length(ds_possib_w));
      qt_pos_separador_w := INSTR(ds_possib_w,',');
    END IF;

   qt_controle_w := qt_controle_w + 1;
    
  END;
  END LOOP;

COMMIT;

END atualizar_controle_dit_mmed;
/