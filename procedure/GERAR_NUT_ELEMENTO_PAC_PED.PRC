Create or Replace PROCEDURE 
Gerar_Nut_Elemento_Pac_Ped(	NR_SEQUENCIA_P     	NUMBER,
				NM_USUARIO_P         	VARCHAR2) IS


nr_seq_elemento_w		Number(10,0);
nr_sequencia_w		Number(10,0);
nr_sequencia_ww		Number(10,0);
nr_sequencia_www		Number(10,0);
cd_unidade_medida_w	Varchar2(30);
ie_prim_fase_w		Varchar2(1);
ie_seg_fase_w		Varchar2(1);
ie_terc_fase_w		Varchar2(1);
ie_quar_fase_w		Varchar2(1);
qt_fase_npt_w		Number(15,0);
qt_fase_w		Number(15,1);
ie_unid_med_w		varchar2(15);

cursor C01 is
	select	nr_sequencia,
		cd_unidade_medida,
		ie_prim_fase,
		ie_seg_fase,
		ie_terc_fase,
		ie_quar_fase,
		ie_unid_med
	from 	Nut_Elemento
	where	ie_situacao		= 'A'
	and	ie_npt_pediatrica	= 'S'
	and	cd_unidade_medida is not null
	Order by nr_seq_apresent, ds_elemento;

cursor C02 is
	select	nr_sequencia
	from 	Nut_elem_material
	where	ie_situacao	= 'A'
	and	ie_padrao	= 'S'
	and	nr_seq_elemento	= nr_seq_elemento_w
	and	nvl(ie_tipo,'NPT') = 'NPT'
	Order by cd_material;
		 
BEGIN

select	nvl(max(qt_fase_npt),0)
into	qt_fase_npt_w
from	nut_pac
where	nr_sequencia	= nr_sequencia_p;

open C01;
loop
	fetch C01 into 
			nr_seq_elemento_w,
			cd_unidade_medida_w,
			ie_prim_fase_w,
			ie_seg_fase_w,
			ie_terc_fase_w,
			ie_quar_fase_w,			
			ie_unid_med_w;
	exit when C01%notfound;
		begin

		select	nvl(max(nr_sequencia),0)
		into	nr_sequencia_w
		from	nut_pac_elemento
		where	nr_seq_nut_pac		= nr_sequencia_p
		  and	nr_seq_elemento		= nr_seq_elemento_w;

		if	(nr_sequencia_w = 0) then
			begin
			if	(qt_fase_npt_w = 3) then
				ie_quar_fase_w	:= 'N';
			elsif	(qt_fase_npt_w = 2) then
				ie_terc_fase_w	:= 'N';
				ie_quar_fase_w	:= 'N';				
			elsif	(qt_fase_npt_w = 1) then
				ie_quar_fase_w	:= 'N';				
				ie_terc_fase_w	:= 'N';
				ie_seg_fase_w	:= 'N';
			end if;

			select	nut_pac_elemento_seq.nextVal
			into	nr_sequencia_w
			from dual;
	
			insert into Nut_Pac_Elemento(
				nr_sequencia, nr_seq_nut_pac, nr_seq_elemento,
				dt_atualizacao, nm_usuario, cd_unidade_medida,
				qt_elem_kg_dia, qt_diaria, pr_total, qt_kcal,
				ie_prim_fase, ie_seg_fase, ie_terc_fase, ie_quar_fase, ie_npt,
				ie_unid_med)
			values(
				nr_sequencia_w, nr_sequencia_p, nr_seq_elemento_w,
				sysdate, nm_usuario_p, cd_unidade_medida_w,
				0, 0, 0, 0, ie_prim_fase_w, ie_seg_fase_w, ie_terc_fase_w, 
				ie_quar_fase_w, 'S', ie_unid_med_w);

			open C02;
			loop
				fetch C02 into 
					nr_sequencia_www;
			exit when C02%notfound;	
				select	nut_pac_elem_mat_seq.nextVal
				into	nr_sequencia_ww
				from dual;				

				insert into nut_pac_elem_mat(
					nr_sequencia, nr_seq_pac_elem, dt_atualizacao,
					nm_usuario, qt_volume, qt_vol_1_fase, 
					qt_vol_2_fase, qt_vol_3_fase, qt_vol_4_fase, nr_seq_elem_mat)
				values(	nr_sequencia_ww, nr_sequencia_w, sysdate,
					nm_usuario_p, null, null,
					null, null, null, nr_sequencia_www);
			end Loop;
			close C02;
			end;
		end if;
		end;
end Loop;
close C01;

commit;
END Gerar_Nut_Elemento_Pac_Ped;
/