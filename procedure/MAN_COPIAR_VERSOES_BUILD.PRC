create or replace
procedure man_copiar_versoes_build( nr_sequencia_ctrl_build_p	number,
			nm_usuario_p		Varchar2) is 

Cursor C01 is 
	select	nr_sequencia
	from	man_os_ctrl_build a
	where	nr_sequencia != nr_sequencia_ctrl_build_p
	and 	nr_seq_man_os_ctrl_desc = (select max(nr_seq_man_os_ctrl_desc) from man_os_ctrl_build b where b.nr_sequencia  = nr_sequencia_ctrl_build_p)
	and		not exists (select 1 from man_os_ctrl_build_alt c where a.nr_sequencia = c.nr_seq_man_os_ctrl_build);
			
Cursor C02 is
	select	nr_sequencia,
		ds_projeto,
		ds_classe,
		cd_pessoa_fisica,
		cd_ramal,
		ie_localizacao,
		substr(ds_alteracao, 1, 1999) ds_alteracao,
		ie_camada
 	from	man_os_ctrl_build_alt
	where	nr_seq_man_os_ctrl_build = nr_sequencia_ctrl_build_p;			
			
begin

FOR rs_C01 IN C01 LOOP
	FOR rs_C02 IN C02 LOOP
		insert into man_os_ctrl_build_alt ( 	nr_sequencia,
										dt_atualizacao,
										nm_usuario,
										dt_atualizacao_nrec,
										nm_usuario_nrec,
										ds_projeto,
										ds_classe,
										cd_pessoa_fisica,
										cd_ramal,
										ie_localizacao,
										ds_alteracao,
										ie_camada,
										nr_seq_man_os_ctrl_build
										) 
									values(
										man_os_ctrl_build_alt_seq.nextval,
										sysdate,
										nm_usuario_p,
										sysdate,
										nm_usuario_p,
										rs_C02.ds_projeto,
										rs_C02.ds_classe,
										rs_C02.cd_pessoa_fisica,
										rs_C02.cd_ramal,
										rs_C02.ie_localizacao,
										rs_C02.ds_alteracao,
										rs_C02.ie_camada,
										rs_C01.nr_sequencia);
	END LOOP;
END LOOP;

commit;

end man_copiar_versoes_build;
/