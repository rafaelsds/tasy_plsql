create or replace
procedure usjrp_gerar_retorno_bb_150
			(	nr_seq_cobr_escrit_p	number,
				nm_usuario_p		varchar2) is 

ds_titulo_w			varchar2(255);
cd_ocorrencia_w			varchar2(10);
dt_liquidacao_w			varchar2(8);
vl_titulo_w			number(15,2);
vl_acrescimo_w			number(15,2);
vl_desconto_w			number(15,2);
vl_abatimento_w			number(15,2);
vl_liquido_w			number(15,2);
vl_outras_despesas_w		number(15,2);
vl_cobranca_w			number(15,2);
vl_alterar_w			number(15,2);
nr_seq_reg_T_w			number(10);
nr_seq_reg_U_w			number(10);
nr_titulo_w			number(10);
nr_seq_ocorrencia_ret_w		number(10);
nr_seq_tipo_w			number(10);
ie_forma_cobranca_w		number(10);
dt_remessa_retorno_w		date;

cursor C01 is
	select	trim(substr(ds_string,70,25)),
		to_number(substr(ds_string,53,15))/100,
		0,
		0,
		0,
		to_number(substr(ds_string,53,15))/100,
		0,
		substr(ds_string,45,8),
		substr(ds_string,68,2)
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= 'F';
begin
select	max(a.nr_seq_tipo),
	max(a.nr_seq_forma_cobranca)
into	nr_seq_tipo_w,
	ie_forma_cobranca_w
from	banco_carteira b,
	cobranca_escritural a
where	a.nr_sequencia		= nr_seq_cobr_escrit_p
and	a.nr_seq_carteira_cobr	= b.nr_sequencia(+);

select	max(to_date(trim(substr(ds_string,66,8)), 'yyyymmdd'))
into	dt_remessa_retorno_w
from	w_retorno_banco
where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
and	substr(ds_string,1,1)	= 'A'
and	substr(ds_string,66,8) <> '00000000';

if	(dt_remessa_retorno_w is not null) then
	update	cobranca_escritural
	set	dt_credito_bancario		= dt_remessa_retorno_w
	where	nr_sequencia		= nr_seq_cobr_escrit_p;
end if;

open C01;
loop
fetch C01 into	
	ds_titulo_w,
	vl_cobranca_w,
	vl_acrescimo_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_liquido_w,
	vl_outras_despesas_w,
	dt_liquidacao_w,
	cd_ocorrencia_w;
exit when C01%notfound;
	begin
	vl_alterar_w	:= 0;
	
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_titulo	= somente_numero(ds_titulo_w);

	if	(nr_titulo_w is null) then -- Quando n�o h� t�tulo, busca pelo nosso numero
		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_nosso_numero	= to_char(somente_numero(ds_titulo_w));
	end if;
	
	if	(nr_titulo_w is null) then -- Quando n�o h� t�tulo, busca pelo numero externo
		select	max(nr_titulo)
		into	nr_titulo_w
		from	titulo_receber
		where	nr_titulo_ext_numerico = to_char(somente_numero(ds_titulo_w));
	end if;
	
	select 	max(a.nr_sequencia)
	into	nr_seq_ocorrencia_ret_w
	from	banco_ocorr_escrit_ret a
	where	a.cd_banco	= 001
	and	a.cd_ocorrencia = cd_ocorrencia_w
	and	nvl(a.nr_seq_tipo,nvl(nr_seq_tipo_w,0))	= nvl(nr_seq_tipo_w,0)
	and	nvl(a.ie_forma_cobranca,nvl(ie_forma_cobranca_w,0)) = nvl(ie_forma_cobranca_w,0);

	if	(nr_titulo_w is not null) then -- Se encontrou o t�tulo importa, sen�o grava no log
		select	vl_titulo
		into	vl_titulo_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

		insert into titulo_receber_cobr
			(nr_sequencia,
			nr_titulo,
			cd_banco,
			vl_cobranca,
			vl_desconto,
			vl_acrescimo,
			vl_despesa_bancaria,
			vl_liquidacao,
			dt_liquidacao,
			dt_atualizacao,
			nm_usuario,
			nr_seq_cobranca,
			nr_seq_ocorrencia_ret)
		values	(titulo_receber_cobr_seq.nextval,
			nr_titulo_w,
			001,
			vl_titulo_w,
			vl_desconto_w,
			vl_acrescimo_w,
			vl_outras_despesas_w,
			vl_liquido_w,
			nvl(to_Date(dt_liquidacao_w,'yyyymmdd'),sysdate),
			sysdate,
			nm_usuario_p,
			nr_seq_cobr_escrit_p,
			nr_seq_ocorrencia_ret_w);
	else
		insert into log_tasy
			(nm_usuario,
			dt_atualizacao,
			cd_log,
			ds_log)
		values	(nm_usuario_p,
			sysdate,
			55760,
			'N�o foi importado o t�tulo ' || ds_titulo_w || ', pois n�o foi encontrado no Tasy');
			
		insert into cobranca_escrit_log
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_cobranca,
			nr_titulo,
			dt_credito,
			vl_saldo_titulo,
			ds_log,
			nr_nosso_numero,
			nr_seq_ocorrencia_ret)
		values	(cobranca_escrit_log_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_cobr_escrit_p,
			somente_numero(substr(ds_titulo_w,1,10)),
			sysdate,
			nvl(vl_liquido_w,0),
			'N�o foi importado o t�tulo ' || somente_numero(substr(ds_titulo_w,1,10)) || ', pois o mesmo n�o foi encontrado no Tasy.',
			null,
			nr_seq_ocorrencia_ret_w);	
	end if;
	end;
end loop;
close C01;

commit;

end usjrp_gerar_retorno_bb_150;
/