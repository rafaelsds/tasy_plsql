create or replace
procedure arquivo_natureza_operacao (cd_estabelecimento_p	number,
					dt_inicio_p		date,
					dt_fim_p		date,
					nm_usuario_p		varchar2) is

nr_sequencia_w			varchar2(10);
dt_atualizacao_w		varchar2(8);
cd_natureza_operacao_w		varchar2(6);
ds_natureza_operacao_w		varchar2(45);
cd_estabelecimento_w		varchar2(10);
contador_w			number;
ds_arquivo_ww			varchar2(4000);


cursor	c01 is
select	distinct
	n.nr_sequencia							nr_sequencia,
	lpad(nvl(to_char(o.dt_atualizacao,'ddmmyyyy'),' '),8,' ')	dt_atualizacao,
	lpad(nvl(o.cd_cfop,' '),6,' ') 					cd_natureza_operacao,
	rpad(nvl(ds_natureza_operacao,' '),45,' ')			ds_natureza_operacao
from	nota_fiscal n,
	natureza_operacao o
where	o.cd_natureza_operacao = n.cd_natureza_operacao
and	n.cd_estabelecimento = cd_estabelecimento_p
and	n.dt_emissao between to_date(to_char(dt_inicio_p,'dd/mm/yyyy'),'dd/mm/yyyy') and fim_dia(to_date(to_char(dt_fim_p,'dd/mm/yyyy'),'dd/mm/yyyy'))
order by n.nr_sequencia;

begin

open c01;
loop
fetch c01 into

nr_sequencia_w,
dt_atualizacao_w,
cd_natureza_operacao_w,
ds_natureza_operacao_w;

exit when c01%notfound;

	begin

	contador_w := contador_w + 1;

	ds_arquivo_ww	:=	dt_atualizacao_w	||
				cd_natureza_operacao_w	||
				ds_natureza_operacao_w;

insert into w_inss_direp_arquivo ( nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_arquivo_w)
		values		(w_inss_direp_arquivo_seq.nextval,
				cd_estabelecimento_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_arquivo_ww);

	if (mod(contador_w,100) = 0) then
		commit;
	end if;

	end;

end loop;
close c01;

commit;

end arquivo_natureza_operacao;
/
 