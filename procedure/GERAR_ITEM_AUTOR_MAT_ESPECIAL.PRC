create or replace
procedure gerar_item_autor_mat_especial(	nr_seq_material_p		number,
					nm_usuario_p		varchar2) is

nr_sequencia_mat_w	number(10);
nr_seq_autor_cirurgia_w	number(10);
cd_material_w		number(6);
qt_solicitada_w		number(15);
pr_adicional_w		number(15);
vl_unitario_w		number(17);
ie_valor_conta_w		varchar2(1);
ds_observacao_w		varchar2(2000);
ie_reducao_acrescimo_w	varchar2(1);

begin

begin

select	a.cd_material,
	a.qt_solicitada,
	a.pr_adicional,
	a.vl_unitario,
	a.ie_valor_conta,
	a.ds_observacao,
	a.ie_reducao_acrescimo,
	b.nr_seq_autor_cirurgia
into	cd_material_w,
	qt_solicitada_w,
	pr_adicional_w,
	vl_unitario_w,
	ie_valor_conta_w,
	ds_observacao_w,
	ie_reducao_acrescimo_w,
	nr_seq_autor_cirurgia_w
from 	material_autorizado a,
	autorizacao_convenio b
where	a.nr_sequencia_autor = b.nr_sequencia
and	a.nr_sequencia = nr_seq_material_p;

exception
when others then
	nr_seq_autor_cirurgia_w := 0;
end;

if	(nvl(nr_seq_material_p,0) > 0) and	
	(nvl(nr_seq_autor_cirurgia_w, 0) > 0) then

	select	material_autor_cirurgia_seq.nextval
	into	nr_sequencia_mat_w
	from	dual;

	insert	into material_autor_cirurgia
		(nr_sequencia,
		nr_seq_autorizacao,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		cd_material,
		qt_solicitada,
		qt_material,
		vl_unitario_material,
		vl_material,
		ie_aprovacao,
		ie_valor_conta,
		ds_observacao,
		pr_adicional,
		ie_reducao_acrescimo)
	values	(nr_sequencia_mat_w,
		nr_seq_autor_cirurgia_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		cd_material_w,
		qt_solicitada_w,
		0,
		0,
		0,
		'N',
		ie_valor_conta_w,
		ds_observacao_w,
		pr_adicional_w,
		ie_reducao_acrescimo_w);

	insert into material_autor_cir_cot
		(nr_sequencia,
		cd_cgc,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_aprovacao,
		nr_orcamento,
		vl_unitario_cotado,
		vl_cotado)
	select	nr_sequencia_mat_w,
		a.cd_cgc_fabricante,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		' ',
		a.nr_orcamento,
		nvl(a.vl_cotado,0),
		(nvl(a.vl_cotado,0) * a.qt_solicitada)
	from 	material_autorizado a
	where	a.nr_sequencia	    = nr_seq_material_p
	and	a.cd_cgc_fabricante is not null
	and	not exists(	select 1 
				from 	material_autor_cir_cot x
				where	x.nr_sequencia = nr_seq_autor_cirurgia_w
				and	x.cd_cgc	= a.cd_cgc_fabricante
				and	nvl(x.nr_orcamento,nvl(a.nr_orcamento,'X')) = nvl(a.nr_orcamento,'X'));


end if;

commit;

end gerar_item_autor_mat_especial;
/