create or replace
procedure reg_approve_item (	ds_table_name 			varchar2,
				nr_seq_item			number,
				nm_user_p			varchar2,
				nm_dt_approving_field_p		varchar2 default 'dt_aprovacao',
				nm_user_column_p		varchar2 default null) as


/*
 * Utilizada para liberar e desfazer a libera��o dos itens da fun��o Documenta��o do Produto (CorQuaDP)
 * Alterna o campo dt_atualizacao da tabela passada por par�metro, no registro correspondente ao indice passado por par�metro: se o campo estiver null, � inserida a data atual, sen�o � inserido null.
 */

ds_campos_adic_w	varchar2(255);
ds_table_pk_field_w	varchar2(50);

begin
	select	max(a.nm_atributo)
	into	ds_table_pk_field_w
	from	indice_atributo a,
		indice i
	where	lower(i.nm_tabela) = lower(ds_table_name)
	and	a.nm_indice = i.nm_indice
	and	i.ie_tipo = 'PK';
	
	select	decode(nm_user_column_p, null, '', nm_user_column_p || ' = decode(' || nm_user_column_p || ', null, ''' || nm_user_p || ''', null),')
	into	ds_campos_adic_w
	from	dual;
	
	execute immediate	' update ' || ds_table_name ||
				' set	 ' || nm_dt_approving_field_p || ' = decode(' || nm_dt_approving_field_p || ', null, sysdate, null),' ||
				ds_campos_adic_w ||
				'	dt_atualizacao = sysdate,' ||
				'	nm_usuario = ''' || nm_user_p || '''' ||
				' where	' || ds_table_pk_field_w || ' = ' || to_char(nr_seq_item);

	commit;

end reg_approve_item;
/