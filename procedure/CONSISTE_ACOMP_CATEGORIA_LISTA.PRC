create or replace
procedure consiste_acomp_categoria_lista(
		nr_seq_servico_lista_p		varchar2,
		nr_atendimento_p			number,
		dt_servico_p			date,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p		number,
		ds_mensagem_p		out	varchar2) is 

ie_acomp_lib_eup_w		varchar2(1);
nr_seq_servico_lista_w	varchar2(2000);
nr_seq_servico_w		number(10);
ds_mensagem_w		varchar2(255);
ds_pergunta_w		varchar2(255);

begin
ds_mensagem_p		:= '';
nr_seq_servico_lista_w	:= nr_seq_servico_lista_p;
ds_pergunta_w		:= obter_texto_tasy(70497, wheb_usuario_pck.get_nr_seq_idioma);

/* Gest�o da Nutri��o - Par�metro [5] - Ao inserir acompanhante verificar acompanhantes liberados na EUP */
obter_param_usuario(1003, 5, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_acomp_lib_eup_w);

while nr_seq_servico_lista_w is not null loop 
	begin
	nr_seq_servico_w		:= substr(nr_seq_servico_lista_w, 1, instr(nr_seq_servico_lista_w, ',') - 1);
	nr_seq_servico_lista_w	:= substr(nr_seq_servico_lista_w, instr(nr_seq_servico_lista_w, ',') + 1, length(nr_seq_servico_lista_w));

	consiste_acomp_categoria(
		nr_seq_servico_w,
		nr_atendimento_p,
		nm_usuario_p,
		cd_estabelecimento_p,
		dt_servico_p,
		ds_mensagem_w);

	if	(ds_mensagem_w is not null) then
		if	(ie_acomp_lib_eup_w = 'Q') then
			ds_mensagem_p	:= ds_mensagem_p || '#@#@' || ds_mensagem_w || ds_pergunta_w;

		elsif	(ie_acomp_lib_eup_w = 'S') then
			begin
			nr_seq_servico_lista_w	:= '';
			ds_mensagem_p		:= ds_mensagem_w;
			end;
		end if;
	end if;
	end;
end loop;

if	(ie_acomp_lib_eup_w = 'Q') then
	ds_mensagem_p	:= substr(ds_mensagem_p, 5, length(ds_mensagem_p));
end if;

end consiste_acomp_categoria_lista;
/
