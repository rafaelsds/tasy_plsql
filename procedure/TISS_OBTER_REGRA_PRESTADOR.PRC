create or replace
procedure TISS_OBTER_REGRA_PRESTADOR (cd_convenio_p				number,
					cd_estabelecimento_p			number,
					ie_tipo_atendimento_p			number,
					ie_responsavel_credito_p		varchar2,
					ie_clinica_p				number,
					cd_cgc_prestador_p			varchar2,
					cd_setor_atendimento_p			number,
					cd_medico_executor_p			varchar2,
					cd_medico_atend_p			varchar2,
					cd_categoria_p				varchar2,
					cd_procedimento_p			number,
					ie_origem_proced_p			number,
					ie_tiss_tipo_guia_p			varchar2,
					ie_honorario_p				varchar2,
					cd_cgc_prestador_tiss_p		out	varchar2,
					cd_prestador_tiss_p		out	varchar2,
					ds_prestador_tiss_p		out	varchar2,
					cd_medico_exec_tiss_p		out	varchar2,
					cd_cgc_prestador_solic_p	out	varchar2,
					nr_seq_regra_p			out	number,
					ie_tipo_regra_p			in	varchar2,
					ie_solicitante_p		out	varchar2,
					cd_cgc_solic_prescr_p			varchar2,
					ie_medico_exec_p		out 	varchar2,
					cd_prestador_solic_p		out 	varchar2,
					ds_prestador_solic_p		out 	varchar2,
					dt_procedimento_p		in 	date,
					ie_fatur_medico_p		in 	varchar2,
					cd_medico_referido_p		in 	varchar2,
					cd_tipo_acomodacao_p		in 	number,
					cd_procedencia_p		in	number,
					cd_plano_p			in 	varchar2,
					nr_seq_classificacao_p		in 	number,
					ie_elegibilidade_p		in	varchar2,
					ie_solic_autor_p		in 	varchar2,
					ie_tipo_atend_conta_p		in	number,
					ie_tipo_protocolo_p		in	number) is

/*
ie_tipo_prestador_p
"S" - Prestador solicitante
"E" - Prestador executante
*/

cd_cgc_prestador_tiss_w		varchar2(255) := null;
cd_cgc_estab_w			varchar2(255);
cd_prestador_tiss_w		varchar2(255);
cd_cgc_prest_solic_w		varchar2(255);
cd_medico_exec_tiss_w		varchar2(255);
ds_prestador_tiss_w		varchar2(255);
ie_solicitante_w			varchar2(255);
cd_area_procedimento_w		number(15);
cd_especialidade_w		number(15);
cd_grupo_proc_w			number(15);
nr_seq_regra_w			number(15);
ie_medico_exec_w		varchar(2);
cd_prestador_solic_w		varchar2(255);
ds_prestador_solic_w		varchar2(255);
ie_vinculo_medico_w		number(3);
qt_regra_w			number(10);
nr_seq_agrup_classif_w		number(10);

cursor c01 is
select	nr_sequencia,
	cd_cgc_prestador_tiss,
	cd_interno,
	decode(ie_tiss_tipo_guia_p, '5', null, decode(ie_solicitante, 'S', cd_cgc_prestador_tiss, null)),
	cd_medico_exec_tiss,
	ds_prestador_tiss,
	nvl(ie_prestador,'N'),
	nvl(ie_med_exec,'N'),
	decode(ie_tiss_tipo_guia_p, '5', null, decode(ie_solicitante, 'S', cd_interno, null)),
	decode(ie_tiss_tipo_guia_p, '5', null, decode(ie_solicitante, 'S', ds_prestador_tiss, null))
from	tiss_regra_prestador
where	cd_convenio								= cd_convenio_p
and	cd_estabelecimento							= cd_estabelecimento_p
and	nvl(ie_tipo_atendimento, 	ie_tipo_atendimento_p)			= ie_tipo_atendimento_p
and	nvl(cd_procedimento,		nvl(cd_procedimento_p, 0))		= nvl(cd_procedimento_p, 0)
and	(nvl(ie_origem_proced,		nvl(ie_origem_proced_p,0))		= nvl(ie_origem_proced_p,0) or cd_procedimento is null) --Se n�o possui procedimento na regra, n�o precisa considerar a origem
and	nvl(ie_clinica,			nvl(ie_clinica_p, 0))			= nvl(ie_clinica_p,0)
and	nvl(cd_setor_atendimento,	nvl(cd_setor_atendimento_p,0))		= nvl(cd_setor_atendimento_p,0)
and	nvl(cd_area_procedimento, 	nvl(cd_area_procedimento_w,0))		= nvl(cd_area_procedimento_w,0)
and	nvl(cd_especialidade,		nvl(cd_especialidade_w,0))		= nvl(cd_especialidade_w,0)
and	nvl(cd_grupo_proc,		nvl(cd_grupo_proc_w,0))			= nvl(cd_grupo_proc_w,0)
and	nvl(ie_responsavel_credito,	nvl(ie_responsavel_credito_p,'X'))	= nvl(ie_responsavel_credito_p, 'X')
and	nvl(cd_cgc_prestador, 		nvl(cd_cgc_prestador_p, 'X'))		= nvl(cd_cgc_prestador_p, 'X')
and	nvl(cd_cgc_solic_prescr, 	nvl(cd_cgc_solic_prescr_p, 'X'))	= nvl(cd_cgc_solic_prescr_p, 'X')
and	nvl(cd_medico_executor,		nvl(cd_medico_executor_p, 'X'))		= nvl(cd_medico_executor_p,'X')
and	nvl(cd_medico_atend,		nvl(cd_medico_atend_p, 'X')) 		= nvl(cd_medico_atend_p, 'X')
and	nvl(cd_categoria,		nvl(cd_categoria_p, 'X'))		= nvl(cd_categoria_p, 'X')
and	nvl(cd_medico_referido,		nvl(cd_medico_referido_p, 'X'))		= nvl(cd_medico_referido_p, 'X')
and	nvl(cd_tipo_acomodacao,		nvl(cd_tipo_acomodacao_p,0))		= nvl(cd_tipo_acomodacao_p,0)
and	nvl(cd_procedencia,		nvl(cd_procedencia_p,0))		= nvl(cd_procedencia_p,0)
and	nvl(cd_plano,			nvl(cd_plano_p,'X'))			= nvl(cd_plano_p,'X')
and	nvl(nr_seq_classificacao,	nvl(nr_seq_classificacao_p,0))		= nvl(nr_seq_classificacao_p,0)
and	nvl(ie_vinculo_medico,		nvl(ie_vinculo_medico_w,0))		= nvl(ie_vinculo_medico_w,0)
and	nvl(ie_tipo_atend_conta,	nvl(ie_tipo_atend_conta_p,0))		= nvl(ie_tipo_atend_conta_p,0)
and	nvl(ie_tipo_protocolo,		nvl(ie_tipo_protocolo_p,0))		= nvl(ie_tipo_protocolo_p,0)
and	nvl(nr_seq_agrup_classif,	nvl(nr_seq_agrup_classif_w,0))		= nvl(nr_seq_agrup_classif_w,0) --OS810426
and	((ie_tiss_tipo_guia_p	<> '5') or
	 (ie_guia_RI	= 'S'))
and	(((ie_honorario_p	not in ('S','P')) or  --lhalves OS269749 em 1/12/2010 - Definir o tipo de guia de Honor (SADT ou HI) que deve se aplicar a regra.
	   (ie_guia_honorario 	= 'S')) or
	  (((ie_honorario_p 	in ('S','P')) and (ie_tiss_tipo_guia_p = '4')) and
	   (ie_guia_honorario 	= 'SADT')) or
	  (((ie_honorario_p 	in ('S','P')) and (ie_tiss_tipo_guia_p = '6')) and
	   (ie_guia_honorario 	= 'HI'))	  
	)
and	((nvl(ie_tipo_regra, 'T') 	= 'T') or (ie_tipo_regra = ie_tipo_regra_p))
and 	cd_convenio is not null
and 	(nvl(ie_escala,'N') = 'N' or (nvl(ie_escala,'N') = 'S' and nvl(tiss_obter_se_PF_escala(cd_medico_executor_p,dt_procedimento_p),'N') = 'S'))
and	(((nvl(ie_fatur_medico,'N') = 'S') and (ie_fatur_medico_p = 'S')) or
	 (ie_fatur_medico_p = 'N'))
and	(((nvl(ie_elegibilidade,'N') = 'S') and (ie_elegibilidade_p = 'S')) or
	 (ie_elegibilidade_p = 'N'))
and	(((nvl(ie_solic_autor,'N') = 'S') and (ie_solic_autor_p = 'S')) or
	 (ie_solic_autor_p = 'N'))	
and	nvl(dt_procedimento_p,sysdate)	between nvl(dt_inicio_vigencia,pkg_date_utils.get_dateTime(1900, 1, 1)) and pkg_date_utils.get_time(nvl(dt_fim_vigencia,sysdate +1),0,0,0) + 89399/89400
and 	(((hr_final is null) or (hr_inicial is null)) or  --OS 813070
	 (coalesce(dt_procedimento_p,sysdate) between 
		pkg_date_utils.Get_DateTime(coalesce(dt_procedimento_p,SYSDATE) , coalesce(hr_inicial, pkg_date_utils.get_Time(00,00,01))) and
		pkg_date_utils.Get_DateTime(coalesce(dt_procedimento_p,SYSDATE),  coalesce(hr_final, pkg_date_utils.get_Time(23,59,59))))
	)
order by	nvl(ie_prioridade,0),
	nvl(cd_procedimento, 0),
	cd_cgc_prestador,
	decode(nvl(ie_responsavel_credito, '0'), '0', 0, 1),
	nvl(cd_grupo_proc, 0),
	nvl(cd_especialidade, 0),
	nvl(cd_area_procedimento, 0),
	nvl(cd_medico_executor,0),
	nvl(cd_setor_atendimento,0),
	nvl(ie_tipo_atendimento,0),
	nvl(ie_clinica,0),
	nvl(cd_cgc_prestador,0),	
	nvl(cd_medico_atend,0),
	nvl(cd_categoria,0),
	nvl(dt_inicio_vigencia,pkg_date_utils.get_dateTime(1900, 1, 1)),
	nvl(cd_medico_referido,'X'),
	nvl(cd_tipo_acomodacao,0),
	nvl(cd_procedencia,0),
	nvl(cd_plano_p,'X'),
	nvl(nr_seq_classificacao,0),
	nvl(ie_vinculo_medico,0),
	nvl(ie_tipo_atend_conta,0),
	nvl(ie_tipo_protocolo,0),
	nvl(nr_seq_agrup_classif,0);

cursor c02 is
select	nr_sequencia,
	cd_cgc_prestador_tiss,
	cd_interno,
	decode(ie_tiss_tipo_guia_p, '5', null, decode(ie_solicitante, 'S', cd_cgc_prestador_tiss, null)),
	cd_medico_exec_tiss,
	ds_prestador_tiss,
	nvl(ie_prestador,'N'),
	nvl(ie_med_exec,'N'),
	decode(ie_tiss_tipo_guia_p, '5', null, decode(ie_solicitante, 'S', cd_interno, null)),
	decode(ie_tiss_tipo_guia_p, '5', null, decode(ie_solicitante, 'S', ds_prestador_tiss, null))
from	tiss_regra_prestador
where	cd_estabelecimento							= cd_estabelecimento_p
and	nvl(ie_tipo_atendimento, 	ie_tipo_atendimento_p)			= ie_tipo_atendimento_p
and	nvl(cd_procedimento,		nvl(cd_procedimento_p, 0))		= nvl(cd_procedimento_p, 0)
and	(nvl(ie_origem_proced,		nvl(ie_origem_proced_p,0))		= nvl(ie_origem_proced_p,0) or cd_procedimento is null) --Se n�o possui procedimento na regra, n�o precisa considerar a origem
and	nvl(ie_clinica,			nvl(ie_clinica_p, 0))			= nvl(ie_clinica_p,0)
and	nvl(cd_setor_atendimento,	nvl(cd_setor_atendimento_p,0))		= nvl(cd_setor_atendimento_p,0)
and	nvl(cd_area_procedimento, 	nvl(cd_area_procedimento_w,0))		= nvl(cd_area_procedimento_w,0)
and	nvl(cd_especialidade,		nvl(cd_especialidade_w,0))		= nvl(cd_especialidade_w,0)
and	nvl(cd_grupo_proc,		nvl(cd_grupo_proc_w,0))			= nvl(cd_grupo_proc_w,0)
and	nvl(ie_responsavel_credito,	nvl(ie_responsavel_credito_p,'X'))	= nvl(ie_responsavel_credito_p, 'X')
and	nvl(cd_cgc_prestador, 		nvl(cd_cgc_prestador_p, 'X'))		= nvl(cd_cgc_prestador_p, 'X')
and	nvl(cd_cgc_solic_prescr, 	nvl(cd_cgc_solic_prescr_p, 'X'))	= nvl(cd_cgc_solic_prescr_p, 'X')
and	nvl(cd_medico_executor,		nvl(cd_medico_executor_p, 'X'))		= nvl(cd_medico_executor_p,'X')
and	nvl(cd_medico_atend,		nvl(cd_medico_atend_p, 'X')) 		= nvl(cd_medico_atend_p, 'X')
and	nvl(cd_categoria,		nvl(cd_categoria_p, 'X'))		= nvl(cd_categoria_p, 'X')
and	nvl(cd_medico_referido,		nvl(cd_medico_referido_p, 'X'))		= nvl(cd_medico_referido_p, 'X')
and	nvl(cd_tipo_acomodacao,		nvl(cd_tipo_acomodacao_p,0))		= nvl(cd_tipo_acomodacao_p,0)
and	nvl(cd_procedencia,		nvl(cd_procedencia_p,0))		= nvl(cd_procedencia_p,0)
and	nvl(cd_plano,			nvl(cd_plano_p,0))			= nvl(cd_plano_p,0)	
and	nvl(nr_seq_classificacao,	nvl(nr_seq_classificacao_p,0))		= nvl(nr_seq_classificacao_p,0)
and	nvl(ie_vinculo_medico,		nvl(ie_vinculo_medico_w,0))		= nvl(ie_vinculo_medico_w,0)
and	nvl(ie_tipo_atend_conta,	nvl(ie_tipo_atend_conta_p,0))		= nvl(ie_tipo_atend_conta_p,0)
and	nvl(ie_tipo_protocolo,		nvl(ie_tipo_protocolo_p,0))		= nvl(ie_tipo_protocolo_p,0)
and	nvl(nr_seq_agrup_classif,	nvl(nr_seq_agrup_classif_w,0))		= nvl(nr_seq_agrup_classif_w,0) --OS810426
and	((ie_tiss_tipo_guia_p	<> '5') or
	 (ie_guia_RI	= 'S'))
and	(((ie_honorario_p	not in ('S','P')) or  --lhalves OS269749 em 1/12/2010 - Definir o tipo de guia de Honor (SADT ou HI) que deve se aplicar a regra.
	   (ie_guia_honorario 	= 'S')) or
	  (((ie_honorario_p 	in ('S','P')) and (ie_tiss_tipo_guia_p = '4')) and
	   (ie_guia_honorario 	= 'SADT')) or
	  (((ie_honorario_p 	in ('S','P')) and (ie_tiss_tipo_guia_p = '6')) and
	   (ie_guia_honorario 	= 'HI'))	  
	)  
and	((nvl(ie_tipo_regra, 'T') 	= 'T') or (ie_tipo_regra = ie_tipo_regra_p))
and 	cd_convenio is null
and 	(nvl(ie_escala,'N') = 'N' or (nvl(ie_escala,'N') = 'S' and nvl(tiss_obter_se_PF_escala(cd_medico_executor_p,dt_procedimento_p),'N') = 'S'))
and	(((nvl(ie_fatur_medico,'N') = 'S') and (ie_fatur_medico_p = 'S')) or
	 (ie_fatur_medico_p = 'N'))
and	(((nvl(ie_elegibilidade,'N') = 'S') and (ie_elegibilidade_p = 'S')) or
	 (ie_elegibilidade_p = 'N'))
and	(((nvl(ie_solic_autor,'N') = 'S') and (ie_solic_autor_p = 'S')) or
	 (ie_solic_autor_p = 'N'))
and	nvl(dt_procedimento_p,sysdate)	between nvl(dt_inicio_vigencia,pkg_date_utils.get_dateTime(1900, 1, 1)) and pkg_date_utils.get_time(nvl(dt_fim_vigencia,sysdate +1),0,0,0) + 89399/89400
and 	(((hr_final is null) or (hr_inicial is null)) or --OS 813070
	 (coalesce(dt_procedimento_p,sysdate) between 
		pkg_date_utils.Get_DateTime(coalesce(dt_procedimento_p,SYSDATE), coalesce(hr_inicial, pkg_date_utils.get_Time(00,00,01))) and
		pkg_date_utils.Get_DateTime(coalesce(dt_procedimento_p,SYSDATE), coalesce(hr_final, pkg_date_utils.get_Time(23,59,59))))
	)
order by	nvl(cd_procedimento, 0),
	cd_cgc_prestador,
	decode(nvl(ie_responsavel_credito, '0'), '0', 0, 1),
	nvl(cd_grupo_proc, 0),
	nvl(cd_especialidade, 0),
	nvl(cd_area_procedimento, 0),
	nvl(cd_medico_executor,0),
	nvl(cd_medico_atend,0),
	nvl(cd_setor_atendimento,0),
	nvl(ie_tipo_atendimento,0),
	nvl(ie_clinica,0),
	nvl(cd_cgc_prestador,0),	
	nvl(cd_categoria,0),
	nvl(dt_inicio_vigencia,pkg_date_utils.get_dateTime(1900, 1, 1)),
	nvl(cd_medico_referido,'X'),
	nvl(cd_tipo_acomodacao,0),
	nvl(cd_procedencia,0),
	nvl(cd_plano,0),
	nvl(nr_seq_classificacao,0),
	nvl(ie_vinculo_medico,0),
	nvl(ie_tipo_atend_conta,0),
	nvl(ie_tipo_protocolo,0),
	nvl(nr_seq_agrup_classif,0);

begin

begin
select	1
into	qt_regra_w
from	tiss_regra_prestador
where	cd_estabelecimento	= cd_estabelecimento_p
and	rownum			= 1;
exception
when others then
	qt_regra_w	:= 0;
end;

if	(qt_regra_w > 0) then

	select	max(cd_area_procedimento),
		max(cd_especialidade),
		max(cd_grupo_proc)
	into	cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w
	from	estrutura_procedimento_v
	where	cd_procedimento		= cd_procedimento_p
	and	ie_origem_proced	= ie_origem_proced_p;

	if	(cd_medico_executor_p is not null) then
		
		select	max(ie_vinculo_medico)
		into	ie_vinculo_medico_w
		from	medico
		where	cd_pessoa_fisica	= cd_medico_executor_p;
		
	end if;
	
	if	(nvl(cd_setor_atendimento_p,0) > 0) then
		select	max(nr_seq_agrup_classif)
		into	nr_seq_agrup_classif_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_p;
	end if;	

	open c01;
	loop
	fetch c01 into
		nr_seq_regra_w,
		cd_cgc_prestador_tiss_w,
		cd_prestador_tiss_w,
		cd_cgc_prest_solic_w,
		cd_medico_exec_tiss_w,
		ds_prestador_tiss_w,
		ie_solicitante_w,
		ie_medico_exec_w,
		cd_prestador_solic_w,
		ds_prestador_solic_w;
	exit when c01%notfound;

		cd_cgc_prestador_tiss_w	:= cd_cgc_prestador_tiss_w;
		cd_prestador_tiss_w	:= cd_prestador_tiss_w;

	end loop;
	close c01;

	/*lhalves OS 220656 em 22/06/2010 - Se n�o encontrou a regra prestador para o conv�nio, utiliza a regra pelo estabelecimento do SHIFT+F11*/
	if	(nr_seq_regra_w is null) then	
		open c02;
		loop
		fetch c02 into
			nr_seq_regra_w,
			cd_cgc_prestador_tiss_w,
			cd_prestador_tiss_w,
			cd_cgc_prest_solic_w,
			cd_medico_exec_tiss_w,
			ds_prestador_tiss_w,
			ie_solicitante_w,
			ie_medico_exec_w,
			cd_prestador_solic_w,
			ds_prestador_solic_w;
		exit when c02%notfound;

			cd_cgc_prestador_tiss_w	:= cd_cgc_prestador_tiss_w;
			cd_prestador_tiss_w	:= cd_prestador_tiss_w;

		end loop;
		close c02;
	end if;

	select	max(cd_cgc)
	into	cd_cgc_estab_w
	from	estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_p;
end if;

cd_cgc_prestador_tiss_p		:= cd_cgc_prestador_tiss_w;
cd_prestador_tiss_p		:= cd_prestador_tiss_w;
ds_prestador_tiss_p		:= ds_prestador_tiss_w;
cd_medico_exec_tiss_p		:= cd_medico_exec_tiss_w;
cd_cgc_prestador_solic_p	:= cd_cgc_prest_solic_w;
nr_seq_regra_p			:= nr_seq_regra_w;
ie_medico_exec_p		:= ie_medico_exec_w;
cd_prestador_solic_p		:= cd_prestador_solic_w;		
ds_prestador_solic_p		:= ds_prestador_solic_w;


/*dsantos em 15/09/2009. OS158199. Op��o criada para trazer o campo CNPJ solic da prescri��o da EUP nos campos 13 e 14 da guia SADT*/
ie_solicitante_p			:= ie_solicitante_w;

end TISS_OBTER_REGRA_PRESTADOR;
/