CREATE OR REPLACE procedure HSL_IMPORTAR_EXTRATO_AUTTAR (	nr_seq_extrato_p	number,
					nm_usuario_p		varchar2,
					ds_arquivo_p		varchar2) is

/* Header de arquivo */
nr_seq_header_w		number(10);
dt_processamento_w	date;
dt_inicial_w		date;
dt_final_w		date;
nr_extrato_w		varchar2(7);
nr_seq_extrato_arq_w	number(10);
vl_liquido_ratiado_w	number(15,2);

cursor	c01 is
select	a.nr_sequencia,
	to_date(substr(a.ds_conteudo,12,8),'yyyymmdd') dt_processamento,
	to_date(substr(a.ds_conteudo,20,8),'yyyymmdd') dt_inicial,
	to_date(substr(a.ds_conteudo,28,8),'yyyymmdd') dt_final,
	somente_numero(substr(a.ds_conteudo,36,7)) nr_extrato
from	w_extrato_cartao_cr a
where	substr(a.ds_conteudo,1,1)	= '0'
and	a.nr_seq_extrato		= nr_seq_extrato_p
order by	a.nr_sequencia;

/* Resumo de operacao (RO) */
nr_seq_resumo_w		number(10)	:= 0;
nr_seq_resumo_tasy_w	number(10);
nr_resumo_w		varchar2(7);
dt_prev_pagto_w		date;
vl_bruto_w		number(15,2);
vl_comissao_w		number(15,2);
vl_rejeitado_w		number(15,2);
vl_liquido_w		number(15,2);
cd_banco_w		number(4);
cd_agencia_bancaria_w	number(5);
nr_conta_w		varchar2(14);
nr_seq_conta_banco_w	number(10);
qt_cv_aceito_w		number(10);
qt_cv_rejeitado_w	number(10);
cd_bandeira_w		number(3);
nr_seq_bandeira_w	number(10);
ds_dt_prev_pagto_w	varchar2(6);
cd_produto_w		produto_bandeira_cartao_cr.cd_produto%type;
ie_tipo_transacao_w	number(10);
ie_origem_ajuste_w 	number(10);

cursor	c02 is
select	a.nr_sequencia,
	somente_numero(substr(a.ds_conteudo,12,7)) nr_resumo,
	substr(a.ds_conteudo,32,6) ds_dt_prev_pagto,
	somente_numero(substr(a.ds_conteudo,45,13)) / 100 vl_bruto,
	somente_numero(substr(a.ds_conteudo,59,13)) / 100 vl_comissao,
	somente_numero(substr(a.ds_conteudo,73,13)) / 100 vl_rejeitado,
	somente_numero(substr(a.ds_conteudo,87,13)) / 100 vl_liquido,
	somente_numero(substr(a.ds_conteudo,100,4)) cd_banco,
	somente_numero(substr(a.ds_conteudo,104,5)) cd_agencia_bancaria,
	trim(substr(a.ds_conteudo,109,14)) nr_conta,
	somente_numero(substr(a.ds_conteudo,125,6)) qt_cv_aceito,
	somente_numero(substr(a.ds_conteudo,133,6)) qt_cv_rejeitado,
	somente_numero(substr(a.ds_conteudo,185,3)) cd_bandeira,
	somente_numero(substr(a.ds_conteudo,131,2)) cd_produto,
	somente_numero(substr(a.ds_conteudo,24,2)) ie_tipo_transacao,
	somente_numero(substr(a.ds_conteudo,146,2)) ie_origem_ajuste
from	w_extrato_cartao_cr a
where	a.nr_sequencia			> nr_seq_resumo_w
and	substr(a.ds_conteudo,1,1)	= '1'
and	a.nr_seq_extrato		= nr_seq_extrato_p;

/* Comprovante de venda (CV) */
nr_seq_movto_w		number(10);
nr_cartao_w		varchar2(19);
dt_compra_w		date;
vl_parcela_w		number(15,2);
nr_parcela_w		number(2);
qt_parcelas_w		number(2);
cd_motivo_rejeicao_w	varchar2(3);
nr_autorizacao_w	varchar2(6);
ds_rejeicao_w		varchar2(255);
ds_comprovante_w	varchar2(6);
nr_documento_w		varchar2(29);

cursor	c03 is
select	a.nr_sequencia,
	substr(a.ds_conteudo,19,19) nr_cartao,
	to_date(substr(a.ds_conteudo,38,8),'yyyymmdd') dt_compra,
	somente_numero(substr(a.ds_conteudo,47,13)) / 100 vl_parcela,
	somente_numero(substr(a.ds_conteudo,60,2)) nr_parcela,
	somente_numero(substr(a.ds_conteudo,62,2)) qt_parcelas,
	substr(a.ds_conteudo,64,3) cd_motivo_rejeicao,
	trim(substr(a.ds_conteudo,67,6)) nr_autorizacao,
	trim(substr(a.ds_conteudo,189,29)) nr_documento,
	trim(substr(a.ds_conteudo,93,6)) ds_comprovante
from	w_extrato_cartao_cr a
where	not exists
	(select	1
	from	w_extrato_cartao_cr x
	where	substr(x.ds_conteudo,1,1)	= '1'
	and	x.nr_sequencia			< a.nr_sequencia
	and	x.nr_sequencia			> nr_seq_resumo_w
	and	x.nr_seq_extrato		= nr_seq_extrato_p)
and	a.nr_sequencia			> nr_seq_resumo_w
and	somente_numero(substr(a.ds_conteudo,12,7))	= nr_resumo_w
and	substr(a.ds_conteudo,1,1)	= '2'
and	a.nr_seq_extrato		= nr_seq_extrato_p;

begin

open	c01;
loop
fetch	c01 into
	nr_seq_header_w,
	dt_processamento_w,
	dt_inicial_w,
	dt_final_w,
	nr_extrato_w;
exit	when c01%notfound;

	update	extrato_cartao_cr
	set	dt_atualizacao		= sysdate,
		dt_final		= dt_final_w,
		dt_importacao		= sysdate,
		dt_inicial		= dt_inicial_w,
		dt_processamento	= dt_processamento_w,
		nm_usuario		= nm_usuario_p,
		nr_extrato		= nr_extrato_w
	where	nr_sequencia		= nr_seq_extrato_p;

	select	extrato_cartao_cr_arq_seq.nextval
	into	nr_seq_extrato_arq_w
	from	dual;

	insert	into extrato_cartao_cr_arq
		(ds_arquivo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		dt_final,
		dt_inicial,
		ie_tipo_arquivo,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_extrato,
		nr_sequencia)
	values	(ds_arquivo_p,
		sysdate,
		sysdate,
		dt_final_w,
		dt_inicial_w,
		'F',
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_extrato_p,
		nr_seq_extrato_arq_w);

	open	c02;
	loop
	fetch	c02 into
		nr_seq_resumo_w,
		nr_resumo_w,
		ds_dt_prev_pagto_w,
		vl_bruto_w,
		vl_comissao_w,
		vl_rejeitado_w,
		vl_liquido_w,
		cd_banco_w,
		cd_agencia_bancaria_w,
		nr_conta_w,
		qt_cv_aceito_w,
		qt_cv_rejeitado_w,
		cd_bandeira_w,
		cd_produto_w,
		ie_tipo_transacao_w,
		ie_origem_ajuste_w;
	exit	when c02%notfound;

		begin

		dt_prev_pagto_w	:= to_date(ds_dt_prev_pagto_w,'yymmdd');

		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(265230,'DS_DT_PREV_PAGTO_W=' || ds_dt_prev_pagto_w);
		end;

		select	max(a.nr_sequencia)
		into	nr_seq_conta_banco_w
		from	banco_estabelecimento a,
			conta_banco_tipo b
		where	a.nr_seq_tipo_conta = b.nr_sequencia(+)
		and	pls_elimina_zeros_esquerda(trim(a.cd_conta))	= pls_elimina_zeros_esquerda(trim(nr_conta_w))
		and	somente_numero(a.cd_agencia_bancaria)	= cd_agencia_bancaria_w
		and	a.cd_banco		= cd_banco_w
		and  nvl(b.ie_classif_conta,'CC') = 'CC';

		if	(nr_seq_conta_banco_w	is null) then

			select	max(a.nr_sequencia)
			into	nr_seq_conta_banco_w
			from	conta_banco_tipo b,
				banco_estabelecimento a
			where	b.ie_classif_conta	= 'CC'
			and	a.nr_seq_tipo_conta	= b.nr_sequencia
			and	lpad(trim(a.cd_conta) || trim(a.ie_digito_conta),14,0) = lpad(nr_conta_w,14,0)
			and	somente_numero(a.cd_agencia_bancaria)	= cd_agencia_bancaria_w
			and	a.cd_banco		= cd_banco_w;

		end if;

		select	max(a.nr_sequencia)
		into	nr_seq_bandeira_w
		from	bandeira_cartao_cr a
		where	exists
			(select	1
			from	produto_bandeira_cartao_cr x
			where	x.nr_seq_bandeira	= a.nr_sequencia
			and	x.cd_produto		= cd_produto_w)
		and	somente_numero(a.cd_bandeira)	= cd_bandeira_w
		AND a.nr_seq_grupo = '5';
		

		if	(nr_seq_bandeira_w	is null) then

			select	max(a.nr_sequencia)
			into	nr_seq_bandeira_w
			from	bandeira_cartao_cr a
			where	somente_numero(a.cd_bandeira)	= cd_bandeira_w
			AND a.nr_seq_grupo = '5';
			

		end if;


		if	((nvl(ie_tipo_transacao_w,'0') = '4') and (nvl( ie_origem_ajuste_w,'0') = '20')) then

				insert	into	EXTRATO_CARTAO_CR_DESP
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_extrato,
					nr_resumo,
					vl_bruto,
					vl_liquido,
					nr_seq_extrato_arq,
					dt_prev_pagto)
				values	(extrato_cartao_cr_desp_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_extrato_p,
					nr_resumo_w,
					abs( vl_bruto_w ),
					abs( vl_liquido_w ),
					nr_seq_extrato_arq_w,
					dt_prev_pagto_w);

		else

		select	extrato_cartao_cr_res_seq.nextval
		into	nr_seq_resumo_tasy_w
		from	dual;

		insert	into extrato_cartao_cr_res
			(dt_atualizacao,
			dt_atualizacao_nrec,
			dt_prev_pagto,
			nm_usuario,
			nm_usuario_nrec,
			nr_resumo,
			nr_seq_bandeira,
			nr_seq_conta_banco,
			nr_seq_extrato,
			nr_seq_extrato_arq,
			nr_sequencia,
			qt_cv_aceito,
			qt_cv_rejeitado,
			vl_bruto,
			vl_comissao,
			vl_liquido,
			vl_rejeitado)
		values	(sysdate,
			sysdate,
			dt_prev_pagto_w,
			nm_usuario_p,
			nm_usuario_p,
			nr_resumo_w,
			nr_seq_bandeira_w,
			nr_seq_conta_banco_w,
			nr_seq_extrato_p,
			nr_seq_extrato_arq_w,
			nr_seq_resumo_tasy_w,
			qt_cv_aceito_w,
			qt_cv_rejeitado_w,
			vl_bruto_w,
			vl_comissao_w,
			vl_liquido_w,
			vl_rejeitado_w);
		end if;

		open	c03;
		loop
		fetch	c03 into
			nr_seq_movto_w,
			nr_cartao_w,
			dt_compra_w,
			vl_parcela_w,
			nr_parcela_w,
			qt_parcelas_w,
			cd_motivo_rejeicao_w,
			nr_autorizacao_w,
			nr_documento_w,
			ds_comprovante_w;
		exit	when c03%notfound;

			if	(cd_motivo_rejeicao_w	= '002') then

				--ds_rejeicao_w	:= 'Cartao Invalido';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304210);

			elsif	(cd_motivo_rejeicao_w	= '023') then

				--ds_rejeicao_w	:= 'Outros';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304216);

			elsif	(cd_motivo_rejeicao_w	= '031') then

				--ds_rejeicao_w	:= 'Transacao de Saque com cartao Electron valor zerado';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304217);

			elsif	(cd_motivo_rejeicao_w	in ('039','099')) then

				--ds_rejeicao_w	:= 'Banco emissor invalido';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304219);

			elsif	(cd_motivo_rejeicao_w	= '044') then

				--ds_rejeicao_w	:= 'Data da transacao invalida';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304220);

			elsif	(cd_motivo_rejeicao_w	= '045') then

				--ds_rejeicao_w	:= 'Codigo da Autorizacao invalido';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304222);

			elsif	(cd_motivo_rejeicao_w	= '055') then

				--ds_rejeicao_w	:= 'Numero de parcelas invalido.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304224);

			elsif	(cd_motivo_rejeicao_w	= '056') then

				--ds_rejeicao_w	:= 'Transacao financiada para EC nao autorizado.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304225);

			elsif	(cd_motivo_rejeicao_w	= '057') then

				--ds_rejeicao_w	:= 'Cartao em boletim protetor.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304226);

			elsif	(cd_motivo_rejeicao_w	= '061') then

				--ds_rejeicao_w	:= 'Numero de cartao invalido.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304227);

			elsif	(cd_motivo_rejeicao_w	in ('066','067','069','070','071','072','077','078','079','080','082','083','084','086','100')) then

				--ds_rejeicao_w	:= 'Transacao nao autorizada.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304228);

			elsif	(cd_motivo_rejeicao_w	= '073') then

				--ds_rejeicao_w	:= 'Transacao invalida.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304229);

			elsif	(cd_motivo_rejeicao_w	= '074') then

				--ds_rejeicao_w	:= 'Valor da transacao invalido.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304230);

			elsif	(cd_motivo_rejeicao_w	= '075') then

				--ds_rejeicao_w	:= 'Numero de cartao invalido.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304231);

			elsif	(cd_motivo_rejeicao_w	= '081') then

				--ds_rejeicao_w	:= 'Cartao vencido.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304232);

			elsif	(cd_motivo_rejeicao_w	= '092') then

				--ds_rejeicao_w	:= 'Banco emissor sem comunicacao.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304233);

			elsif	(cd_motivo_rejeicao_w	= '093') then

				--ds_rejeicao_w	:= 'Desbalanceamento no plano parcelado.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304234);

			elsif	(cd_motivo_rejeicao_w	= '094') then

				--ds_rejeicao_w	:= 'Venda parcelada para cartao emitido no exterior.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304235);

			elsif	(cd_motivo_rejeicao_w	= '097') then

				--ds_rejeicao_w	:= 'Valor de parcela menor do que o permitido.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304236);

			elsif	(cd_motivo_rejeicao_w	in ('101','102')) then

				--ds_rejeicao_w	:= 'Transacao duplicada.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304237);

			elsif	(cd_motivo_rejeicao_w	= '124') then

				--ds_rejeicao_w	:= 'BIN nao cadastrado.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304238);

			elsif	(cd_motivo_rejeicao_w	in ('126','128','129','130','133','134')) then

				--ds_rejeicao_w	:= 'Transacao de Saque com cartao Electron invalido.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304239);

			elsif	(cd_motivo_rejeicao_w	= '145') then

				--ds_rejeicao_w	:= 'Estabelecimento invalido para distribuicao.';
				ds_rejeicao_w	:= wheb_mensagem_pck.get_texto(304240);

			end if;

			vl_liquido_ratiado_w := round(dividir((vl_liquido_w*vl_parcela_w),vl_bruto_w),2);

			insert	into extrato_cartao_cr_movto
				(ds_comprovante,
				ds_rejeicao,
				dt_atualizacao,
				dt_atualizacao_nrec,
				dt_compra,
				ie_pagto_indevido,
				nm_usuario,
				nm_usuario_nrec,
				nr_autorizacao,
				nr_cartao,
				nr_documento,
				nr_parcela,
				nr_resumo,
				nr_seq_extrato,
				nr_seq_extrato_arq,
				nr_seq_extrato_res,
				nr_sequencia,
				qt_parcelas,
				vl_aconciliar,
				vl_liquido,
				vl_parcela,
				vl_saldo_concil_fin)
			values	(ds_comprovante_w,
				ds_rejeicao_w,
				sysdate,
				sysdate,
				dt_compra_w,
				'N',
				nm_usuario_p,
				nm_usuario_p,
				nr_autorizacao_w,
				nr_cartao_w,
				nr_documento_w,
				nr_parcela_w,
				nr_resumo_w,
				nr_seq_extrato_p,
				nr_seq_extrato_arq_w,
				nr_seq_resumo_tasy_w,
				extrato_cartao_cr_movto_seq.nextval,
				qt_parcelas_w,
				vl_liquido_ratiado_w,
				vl_liquido_ratiado_w,
				vl_parcela_w,
				vl_liquido_ratiado_w);

		end	loop;
		close	c03;

	end	loop;
	close	c02;

end	loop;
close	c01;

delete	from w_extrato_cartao_cr
where	nr_seq_extrato	= nr_seq_extrato_p;

commit;

end HSL_IMPORTAR_EXTRATO_AUTTAR;
/