create or replace
procedure pls_inserir_tipo_doc_cooperado
			(	nr_seq_tipo_documento_p	Number,
				nr_seq_cooperado_p	Number,
				ds_arquivo_p		Varchar2,
				dt_inicio_vigencia_p	Date,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is
							
nr_seq_tipo_documento_w		Number(10);				
qt_doc_w			Number(10);
nr_seq_tipo_documento_ww	Number(10);				
				
begin

select	max(nr_sequencia)
into	nr_seq_tipo_documento_ww
from	pls_cooperado_tipo_doc
where	nr_seq_cooperado		= nr_seq_cooperado_p
and	nr_seq_tipo_documento		= nr_seq_tipo_documento_p
and	trunc(dt_inicio_vigencia)	= trunc(dt_inicio_vigencia_p);

if	(nr_seq_tipo_documento_ww = 0) then
	
	select	pls_cooperado_tipo_doc_seq.nextval
	into	nr_seq_tipo_documento_w
	from 	dual;
	
	insert	into pls_cooperado_tipo_doc	
		(nr_sequencia, nr_seq_cooperado, nr_seq_tipo_documento,
		 dt_inicio_vigencia, dt_atualizacao, nm_usuario)
	values	(nr_seq_tipo_documento_w, nr_seq_cooperado_p, nr_seq_tipo_documento_p,
		 sysdate, sysdate, nm_usuario_p);
		 
	insert	into pls_cooperado_documento
		(nr_sequencia, nr_seq_coop_tipo, dt_atualizacao,
		nm_usuario, ds_arquivo, dt_documento)
	values	(pls_cooperado_documento_seq.nextval, nr_seq_tipo_documento_w, sysdate,
		nm_usuario_p, ds_arquivo_p, sysdate);
else
	insert	into pls_cooperado_documento
		(nr_sequencia, nr_seq_coop_tipo, dt_atualizacao,
		nm_usuario, ds_arquivo, dt_documento)
	values	(pls_cooperado_documento_seq.nextval, nr_seq_tipo_documento_ww, sysdate,
		nm_usuario_p, ds_arquivo_p, sysdate);
end if;
	
commit;

end pls_inserir_tipo_doc_cooperado;
/