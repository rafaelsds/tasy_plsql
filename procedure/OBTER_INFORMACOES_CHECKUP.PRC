create or replace
procedure obter_informacoes_checkup(
			nm_usuario_p			varchar2,
			ds_agendamento_p	out	varchar2,
			ds_sexo_p		out	varchar2,
			ds_total_p		out	varchar2) is 

qt_checkup_w	number(10) := 0;
qt_checkup_cd_w	number(10) := 0;
qt_agendados_w	number(10) := 0;
qt_encaixe_w	number(10) := 0;
qt_masculino_w	number(10) := 0;
qt_feminino_w	number(10) := 0;
			
begin


select	count(*) 
into	qt_checkup_w
from 	checkup
where	dt_cancelamento is null 
and	cd_pessoa_fisica is not null;

select	count(*) 
into	qt_checkup_cd_w
from 	checkup
where	dt_cancelamento is null 
and	cd_pessoa_fisica is null;

select	count(*) 
into	qt_agendados_w
from	checkup
where	dt_cancelamento is null 
and	cd_pessoa_fisica is not null
and	nvl(ie_encaixe,'N') = 'N';

select	count(*) 
into	qt_encaixe_w
from	checkup
where	dt_cancelamento is null
and	cd_pessoa_fisica is not null
and	nvl(ie_encaixe,'N') = 'S';

select	count(*) 
into	qt_masculino_w
from	checkup
where	dt_cancelamento is null 
and	cd_pessoa_fisica is not null 
and	obter_sexo_pf(cd_pessoa_fisica,'C') = 'M';

select	count(*) 
into	qt_feminino_w
from	checkup
where	dt_cancelamento is null 
and	cd_pessoa_fisica is not null and Obter_Sexo_PF(cd_pessoa_fisica,'C')  = 'F';

ds_agendamento_p	:= substr(obter_texto_dic_objeto(221194,wheb_usuario_pck.get_nr_seq_idioma,'QT_AGENDADOS='|| qt_agendados_w || ';QT_ENCAIXE='|| qt_encaixe_w),1,2000);
ds_sexo_p		:= substr(obter_texto_dic_objeto(221195,wheb_usuario_pck.get_nr_seq_idioma,'QT_MASCULINO='|| qt_masculino_w || ';QT_FEMININO='|| qt_feminino_w),1,2000);
ds_total_p		:= substr(obter_texto_dic_objeto(221196,wheb_usuario_pck.get_nr_seq_idioma,'SM_TOTAL='|| (qt_agendados_w + qt_encaixe_w)),1,2000);

end obter_informacoes_checkup;
/