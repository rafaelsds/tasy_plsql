create or replace 
procedure lab_ins_lote_ent_result_hb (cd_barras_p    	 varchar2,
			nr_corrida_p   		 number,
			cd_exame_p       	 varchar2,
			nm_usuario_p   		 varchar2,
	                                cd_estabelecimento_p varchar2) is

begin

if (nr_corrida_p is not null) then

	insert into lote_ent_result_hb (nr_sequencia,
									cd_estabelecimento,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_corrida,
									cd_exame,
									cd_barras)
								values
								   (lote_ent_result_hb_seq.nextVal,
									cd_estabelecimento_p,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_corrida_p,								
									cd_exame_p,
									cd_barras_p);
end if;

commit;

end lab_ins_lote_ent_result_hb;

/
