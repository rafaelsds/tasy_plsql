create or replace
procedure dmed_mensal_entrada_unica
			(	nr_seq_dmed_mensal_p		number,
				cd_estabelecimento_p		number,
				dt_referencia_p			date,
				ie_nota_fiscal_p		varchar2,
				ie_conta_paciente_p		varchar2,
				ie_cpf_p			varchar2,
				ie_idade_p			varchar2, 
				ie_estrangeiro_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	GERAR_DMED_MENSAL_PRESTADOR
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_pessoa_responsavel_w		varchar2(10);
cd_pessoa_beneficiario_w	varchar2(10);
nr_documento_w			number(10);
vl_documento_w			number(15,2);
dt_documento_w			date;
contador_w			number(10)	:= 0;
nr_cpf_resp_w			varchar2(11);
nr_cpf_benef_w			varchar2(11);
cd_pessoa_titular_w		varchar2(14);
cd_pessoa_benef_w		varchar2(14);
vl_total_titulo_w		number(15,2);
nr_atendimento_w		number(10);
nr_nota_fiscal_w		varchar2(255);
nr_seq_nf_saida_w 		number(10);
cpf_w				varchar2(11);		
cd_convenio_parametro_w		number(5);
ie_tipo_convenio_w		number(2);
ie_convenio_part_w		varchar2(1);
nr_interno_conta_w		number(10);
dt_ref_inicial_w		date;
dt_ref_final_w			date;
qt_classe_w			number(10);

cursor c_entrada is
	select  p.cd_pessoa_fisica cd_responsavel,
		a.cd_pessoa_fisica cd_beneficiario,
		r.nr_titulo,
		nvl(l.dt_recebimento,r.dt_emissao) dt_documento,
		nvl(l.vl_recebido,0) vl_total_titulo,
		a.nr_atendimento,
		nvl(r.nr_interno_conta,0) nr_interno_conta,
		nvl(r.nr_seq_nf_saida,0) nr_seq_nf_saida,
		nvl(r.nr_nota_fiscal,0) nr_nota_fiscal
	from 	atendimento_pagador	p,
		atendimento_paciente	a,
		titulo_receber_liq	l,
		titulo_receber		r,
		pessoa_fisica		f,
		nacionalidade		n
	where	a.nr_atendimento	= r.nr_atendimento
	and	r.nr_titulo		= l.nr_titulo(+)
	and	a.nr_atendimento	= p.nr_atendimento
	and	f.cd_pessoa_fisica	= p.cd_pessoa_fisica 
	and	f.cd_nacionalidade	= n.cd_nacionalidade(+)
	--and	r.vl_saldo_titulo	= 0
	and	(r.ie_situacao not in ('3','5') or (r.ie_situacao = '5' and 
		exists	(select	1			
			from	titulo_receber_liq k
			where	k.nr_titulo = r.nr_titulo
			and 	vl_recebido > 0	
			and 	not exists (select 1 from titulo_receber_liq x where x.nr_titulo = k.nr_titulo and x.nr_seq_liq_origem = k.nr_sequencia and rownum = 1))))
	and	r.ie_origem_titulo in (2,1)
	and	nvl(l.dt_recebimento,r.dt_emissao) between dt_ref_inicial_w and dt_ref_final_w
	and	 exists	(select	 1
			from	dmed_regra_tipo_tit	w
			where	w.ie_tipo_receber	= l.cd_tipo_recebimento)
	and	((ie_cpf_p = 'AM') or
		((ie_cpf_p = 'SC') and 	 (f.nr_cpf is null) 	and 	(pkg_date_utils.add_month(f.dt_nascimento, ie_idade_p * 12,0) <= Fim_Mes(dt_ref_inicial_w))) or
		((ie_cpf_p = 'CC') and (((f.nr_cpf is not null) and 	(pkg_date_utils.add_month(f.dt_nascimento, ie_idade_p * 12,0) <= Fim_Mes(dt_ref_inicial_w))) or 
									(pkg_date_utils.add_month(f.dt_nascimento, ie_idade_p * 12,0) >= Fim_Mes(dt_ref_inicial_w)))))	
	and	(((nvl(n.ie_brasileiro,'S') = 'S') and (ie_estrangeiro_p = '2'))  or
		((nvl(ie_brasileiro,'S') = 'N') and (ie_estrangeiro_p = '1')) or
		(ie_estrangeiro_p = '3'))
	and  	((a.cd_estabelecimento = nvl( cd_estabelecimento_p ,a.cd_estabelecimento)) or 
	   	((a.cd_estabelecimento is null) and (cd_estabelecimento_p is null)))
	and 	((r.nr_seq_classe is null) or
		(exists	(select 1 
			from 	 dmed_regra_classe_tit t
			where	 t.nr_seq_classe = r.nr_seq_classe)))
	and 	((exists (select	1
			from	atend_paciente_unidade c,
				dmed_regra_setor_atend b
			where	c.cd_setor_atendimento	= b.cd_setor_atendimento
			and 	c.nr_atendimento	= a.nr_atendimento)) or
		(not exists(	select 1 
				from 	 dmed_regra_setor_atend
				where 	rownum  <= 1)))
	union all
	select  r.cd_pessoa_fisica cd_responsavel,
		a.cd_pessoa_fisica cd_beneficiario,
		r.nr_titulo,
		nvl(l.dt_recebimento,r.dt_emissao) dt_documento,
		nvl(l.vl_recebido, r.vl_titulo) vl_total_titulo,
		a.nr_atendimento,
		nvl(r.nr_interno_conta,0) nr_interno_conta,
		nvl(r.nr_seq_nf_saida,0) nr_seq_nf_saida,
		nvl(r.nr_nota_fiscal,0) nr_nota_fiscal
	from 	atendimento_paciente	a,
		titulo_receber_liq	l,
		titulo_receber		r,
		pessoa_fisica		f,
		nacionalidade		n
	where	a.cd_pessoa_fisica	= f.cd_pessoa_fisica
	and	a.nr_atendimento	= r.nr_atendimento
	and	r.nr_titulo		= l.nr_titulo(+)
	and	f.cd_nacionalidade	= n.cd_nacionalidade(+)
	--and	r.vl_saldo_titulo	= 0
	and	r.cd_pessoa_fisica is not null -- trazer apenas t�tulos de pessoas f�sicas
	and	r.ie_situacao <> '5'
	and	r.ie_situacao <> '3'
	and	r.ie_origem_titulo in (2,1)
	and	nvl(l.dt_recebimento,r.dt_emissao) between dt_ref_inicial_w and dt_ref_final_w
	and	 exists	(select	 1
			from	dmed_regra_tipo_tit	w
			where	w.ie_tipo_receber	= l.cd_tipo_recebimento)
	and	((((nvl(n.ie_brasileiro,'S') = 'S') or (f.nr_cpf is not null)) and (ie_estrangeiro_p = '4')) or
		((nvl(n.ie_brasileiro,'S') = 'S') and (ie_estrangeiro_p = '2')) or
		((nvl(ie_brasileiro,'S') = 'N') and (ie_estrangeiro_p = '1')) or
		(ie_estrangeiro_p = '3'))
	and 	not exists	(select	1
				from	atendimento_pagador	p
				where	p.nr_atendimento	= a.nr_atendimento)
	and  	((a.cd_estabelecimento = nvl( cd_estabelecimento_p ,a.cd_estabelecimento)) or
	   	((a.cd_estabelecimento is null) and (cd_estabelecimento_p is null)))
	and 	((r.nr_seq_classe is null) or
		(qt_classe_w = 0) or
		(exists	(select 1 
			from 	 dmed_regra_classe_tit t
			where	 t.nr_seq_classe = r.nr_seq_classe)))
	and 	((exists (select	1
			from	atend_paciente_unidade c,
				dmed_regra_setor_atend b
			where	c.cd_setor_atendimento	= b.cd_setor_atendimento
			and 	c.nr_atendimento	= a.nr_atendimento)) or
		(not exists(	select 1 
				from 	 dmed_regra_setor_atend
				where 	rownum  <= 1)));
		
type 		fetch_array is table of c_entrada%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_movimentacao_w	vetor;

begin
dt_ref_inicial_w	:= pkg_date_utils.start_of(dt_referencia_p,'MONTH',0);
dt_ref_final_w		:= fim_dia(fim_mes(dt_referencia_p));

select	count(1)
into	qt_classe_w
from	dmed_regra_classe_tit
where	rownum	= 1;

open c_entrada;
loop
fetch c_entrada bulk collect into s_array limit 1000;
	vetor_movimentacao_w(i)	:= s_array;
	i			:= i + 1;
exit when c_entrada%notfound;
end loop;
close c_entrada;

for i in 1..vetor_movimentacao_w.count loop
	begin
	s_array := vetor_movimentacao_w(i);
	for z in 1..s_array.count loop
		cd_pessoa_responsavel_w		:= s_array(z).cd_responsavel;
		cd_pessoa_beneficiario_w	:= s_array(z).cd_beneficiario;
		nr_documento_w			:= s_array(z).nr_titulo;
		dt_documento_w			:= s_array(z).dt_documento;
		vl_total_titulo_w		:= s_array(z).vl_total_titulo;
		nr_atendimento_w		:= s_array(z).nr_atendimento;
		nr_interno_conta_w		:= s_array(z).nr_interno_conta;
		nr_seq_nf_saida_w		:= s_array(z).nr_seq_nf_saida;
		nr_nota_fiscal_w		:= s_array(z).nr_nota_fiscal;
		
		begin
		ie_tipo_convenio_w	:= 0;
	
		if	(nvl(nr_interno_conta_w,0) <> 0)  then
			begin
			select	cd_convenio_parametro
			into	cd_convenio_parametro_w
			from	conta_paciente
			where	nr_interno_conta	= nr_interno_conta_w;
			exception when others then
				cd_convenio_parametro_w	:= null;
			end;
			
			select	max(ie_tipo_convenio)
			into 	ie_tipo_convenio_w
			from 	convenio
			where  	cd_convenio	= cd_convenio_parametro_w;
		elsif	(nvl(nr_atendimento_w,0) <> 0) then
			select	max(ie_tipo_convenio)
			into	ie_tipo_convenio_w
			from	atendimento_paciente
			where	nr_atendimento	= nr_atendimento_w;	
		end if;

		select 	max(nvl(ie_convenio_part, 'S')) 
		into 	ie_convenio_part_w
		from 	dmed_regra_geral;

		cpf_w	:= trim(obter_cpf_pessoa_fisica(cd_pessoa_responsavel_w));
		
		if cpf_w = ' ' then
			cpf_w	:= null;
		end if;	
		
		if 	((ie_convenio_part_w = 'S') and
			((ie_tipo_convenio_w = 1) or
			(ie_tipo_convenio_w = 0))) or
			(ie_convenio_part_w = 'N') then
			if	(((ie_cpf_p = 'SC') and
				(cpf_w IS NULL)) or	
				((ie_cpf_p = 'CC') and
				((cpf_w IS NOT NULL))) or	
				(ie_cpf_p = 'AM')) then
				if	(((ie_nota_fiscal_p = 'S') and
					((nr_seq_nf_saida_w <> 0) or
					(nr_nota_fiscal_w <> '0'))) or
					(ie_nota_fiscal_p = 'N')) then
					insert into dmed_titulos_mensal
						(nr_sequencia, 
						dt_atualizacao, 
						nm_usuario, 
						dt_atualizacao_nrec, 
						nm_usuario_nrec, 
						nr_seq_dmed_mensal, 
						nr_documento, 
						ie_tipo_documento, 
						cd_pessoa_titular, 
						cd_pessoa_beneficiario, 
						vl_pago, 
						dt_liquidacao,
						ie_prestadora_ops,
						nr_atendimento)
					values	(dmed_titulos_mensal_seq.nextval,
						sysdate,
						'Tasy4',
						sysdate,
						'Tasy4',
						nr_seq_dmed_mensal_p,
						nr_documento_w,
						'TI',
						cd_pessoa_responsavel_w,
						cd_pessoa_beneficiario_w,
						vl_total_titulo_w,
						dt_documento_w,
						'P',
						nr_atendimento_w);

					contador_w	:= contador_w + 1;

					if	(mod(contador_w,100) = 0) then
						commit;
					end if;
				end if;	
			end if;
		end if;
		end;
	end loop;
	end;
end loop;

end dmed_mensal_entrada_unica;
/
