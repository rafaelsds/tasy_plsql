create or replace
procedure desfazer_saida_real(	nr_atendimento_p	number) is
		
begin

if	(nr_atendimento_p is not null) then
	begin

	update	atendimento_paciente
	set	dt_saida_real = null,
		nr_seq_local_destino = null
	where	nr_atendimento = nr_atendimento_p;

	commit;

	end;
end if;

end desfazer_saida_real;
/