create or replace
procedure gravar_hist_formato_result(	nr_seq_formato_p	number,
										nm_usuario_p		varchar2) is

ds_regra_w				exame_lab_format.ds_regra%type;
dt_fim_vigencia_w 		exame_lab_format.dt_fim_vigencia%type;
dt_inicio_vigencia_w 	exame_lab_format.dt_inicio_vigencia%type;
ie_mapa_laudo_w			exame_lab_format.ie_mapa_laudo%type;
ie_padrao_w				exame_lab_format.ie_padrao%type;
nr_seq_exame_w			exame_lab_format.nr_seq_exame%type;
nr_seq_metodo_w			exame_lab_format.nr_seq_metodo%type;
nr_seq_material_w		exame_lab_format.nr_seq_material%type;
nr_seq_superior_w		exame_lab_format.nr_seq_superior%type;
nr_sequencia_w			exame_lab_format_log.nr_sequencia%type;

begin
	begin
		select	ds_regra,
				dt_fim_vigencia,
				dt_inicio_vigencia,
				ie_mapa_laudo,
				ie_padrao,
				nr_seq_exame,
				nr_seq_metodo,
				nr_seq_superior
		into	ds_regra_w,
				dt_fim_vigencia_w,
				dt_inicio_vigencia_w,
				ie_mapa_laudo_w,
				ie_padrao_w,
				nr_seq_exame_w,
				nr_seq_metodo_w,
				nr_seq_superior_w
		from 	exame_lab_format
		where	nr_seq_formato = nr_seq_formato_p;
		
		select	exame_lab_format_log_seq.nextVal
		into	nr_sequencia_w
		from 	dual;
		
		insert into	exame_lab_format_log (
			nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ie_mapa_laudo,
			ie_padrao,
			ds_regra,
			dt_inicio_vigencia,
			dt_fim_vigencia,
			nr_seq_superior,
			nr_seq_metodo,
			nr_seq_material,
			nr_seq_exame,
			nr_seq_formato)
		values (
			nr_sequencia_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			ie_mapa_laudo_w,
			ie_padrao_w,
			ds_regra_w,
			dt_inicio_vigencia_w,
			dt_fim_vigencia_w,
			nr_seq_superior_w,
			nr_seq_metodo_w,
			nr_seq_material_w,			
			nr_seq_exame_w,
			nr_seq_formato_p);

		commit;
		
		COPIA_CAMPO_LONG_DE_PARA(
			'EXAME_LAB_TEXTO', 
			'DS_FORMATO', 
			'WHERE NR_SEQ_FORMATO = :NR_SEQ_FORMATO_P', 
			'NR_SEQ_FORMATO_P=' || to_char(nr_seq_formato_p), 
			'EXAME_LAB_FORMAT_LOG', 
			'DS_FORMATO', 
			'WHERE NR_SEQUENCIA = :NR_SEQUENCIA_P',
			'NR_SEQUENCIA_P=' || to_char(nr_sequencia_w));
			
		commit;
			
	exception
		when others then
			gravar_log_lab(52, sqlerrm, nm_usuario_p);
	end;
end;
/