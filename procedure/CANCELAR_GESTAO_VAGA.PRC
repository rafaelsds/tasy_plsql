create or replace
procedure cancelar_gestao_vaga(	nr_seq_agenda_p		number,
				cd_motivo_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

nr_seq_gestao_w		number(10,0);
nr_seq_motivo_gv_w	number(10,0);
cd_pessoa_fisica_w	varchar2(10);
nm_paciente_reserva_GV_w	varchar2(255);
qt_existe_leito_w		number(10,0);
cd_unidade_basica_w	varchar2(10);
cd_unidade_compl_w	varchar2(10);
cd_setor_desejado_w	number(5,0);
nr_atendimento_w		number(10,0);



Cursor C01 is
		select	nr_sequencia
		from	gestao_vaga a
		where	a.nr_seq_agenda = nr_seq_agenda_p;
begin

if	(nr_seq_agenda_p is not null) then
	begin
	/* Obter se a agenda possui registro na gestao de vagas */
	open C01;
	loop
	fetch C01 into	
		nr_seq_gestao_w;
	exit when C01%notfound;
		begin
		if	(nr_seq_gestao_w > 0) then
			begin
			select	decode(nvl(max(nr_seq_motivo_gv),0), 0, null, nvl(max(nr_seq_motivo_gv),0))
			into	nr_seq_motivo_gv_w
			from	agenda_motivo_cancelamento
			where	cd_motivo = cd_motivo_p
			and		cd_estabelecimento = cd_estabelecimento_p
			and		ie_agenda in ('CI','T');

			select	cd_pessoa_fisica,
					cd_unidade_basica,
					cd_unidade_compl,
					cd_setor_desejado,
					nvl(nr_atendimento,0),
					nm_paciente
			into	cd_pessoa_fisica_w,
					cd_unidade_basica_w,
					cd_unidade_compl_w,
					cd_setor_desejado_w,
					nr_atendimento_w,
					nm_paciente_reserva_GV_w
			from	gestao_vaga
			where	nr_sequencia = nr_seq_gestao_w;

			select	count(*)
			into	qt_existe_leito_w
			from	unidade_atendimento
			where	(cd_paciente_reserva	= cd_pessoa_fisica_w or nr_atendimento = nr_atendimento_w or nm_pac_reserva = nm_paciente_reserva_GV_w)
			and		cd_unidade_basica		= cd_unidade_basica_w
			and		cd_unidade_compl		= cd_unidade_compl_w
			and		cd_setor_atendimento	= cd_setor_desejado_w;

			if	(qt_existe_leito_w > 0) then

				update	unidade_atendimento
				set		ie_status_unidade	= decode(ie_status_unidade, 'R', 'L', ie_status_unidade),
						nm_usuario		= nm_usuario_p,
						dt_atualizacao		= sysdate,
						cd_paciente_reserva	= null,
						nm_usuario_reserva	= null,
						nm_pac_reserva		= null
				where	cd_unidade_basica		= cd_unidade_basica_w
				and		cd_unidade_compl		= cd_unidade_compl_w
				and		cd_setor_atendimento	= cd_setor_desejado_w;

			end if;

			update	gestao_vaga
			set		ie_status			= 'C',
					nr_seq_motivo_cancel	= nr_seq_motivo_gv_w
			where	nr_sequencia		= nr_seq_gestao_w;
			end;
		end if;
		end;
	end loop;
	close C01;
	end;
end if;

commit;

end cancelar_gestao_vaga;
/