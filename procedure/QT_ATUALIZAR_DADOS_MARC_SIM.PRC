CREATE OR REPLACE procedure qt_atualizar_dados_marc_sim(
			nr_seq_pac_sim_p	number,
			dt_agenda_p		date,
			nm_usuario_p		Varchar2,
			nr_seq_local_p		number,
			cd_estabelecimento_p	number,
			ie_gerar_p	out	varchar2) is

dt_prevista_w		date;
dt_real_w		date;
qt_tempo_medic_w	number(10);
qt_marcacao_w		number(10);
qt_em_agenda_w		number(10);
qt_pendencia_marcada_w	number(10);
dt_ultimo_horario_w	date;
ie_dia_quimio_w		varchar2(1)	:= 'S';
ie_gerar_w		varchar2(1)	:= 'N';
qt_tempo_prep_medic_w	number(10);
qt_tempo_prep_pac_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
ds_result_pac_prof_w	varchar2(255);
ie_prof_marcado_w	varchar2(1)	:= 'N';
nr_seq_prof_w		number(10);
ie_tipo_agendamento_w	varchar2(15)	:= 'P';
ie_perm_mesmo_horario_w	varchar2(1)	:= 'N';
nr_min_apres_w		number(10);
nr_min_apres_local_w	number(2,0);

begin

select	nvl(max(Obter_Valor_Param_Usuario(865, 1, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),0)
into	qt_tempo_prep_medic_w
from	dual;

select	nvl(max(Obter_Valor_Param_Usuario(865, 2, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),0)
into	qt_tempo_prep_pac_w
from	dual;

select	nvl(max(Obter_Valor_Param_Usuario(865, 4, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),0)
into	ie_perm_mesmo_horario_w
from	dual;

select	nvl(max(Obter_Valor_Param_Usuario(865, 3, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)),0)
into	nr_min_apres_w
from	dual;

select	max(nr_min_apres)
into	nr_min_apres_local_w
from	qt_local
where	nr_sequencia = nr_seq_local_p;

begin
select	b.dt_prevista,
	b.dt_real,
	--a.qt_tempo_medic,
	qt_obter_dur_aplicacao_sim(b.ds_dia_ciclo,a.nr_seq_medicacao,a.cd_protocolo,a.nr_sequencia,nm_usuario_p,cd_estabelecimento_p),
	a.cd_pessoa_fisica
into	dt_prevista_w,
	dt_real_w,
	qt_tempo_medic_w,
	cd_pessoa_fisica_w
from	paciente_setor_sim a,
	paciente_atendimento_sim b
where	a.nr_sequencia	= nr_seq_pac_sim_p
and	a.nr_sequencia	= b.nr_seq_paciente
and	trunc(nvl(b.dt_real, b.dt_prevista))	= trunc(dt_agenda_p);
exception
	when others then
	ie_dia_quimio_w	:= 'N';
end;

if	(ie_dia_quimio_w	= 'S') then
	select	max(nr_seq_prof)
	into	nr_seq_prof_w
	from	qt_paciente_prof
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w;

	--qt_tempo_medic_w	:= qt_tempo_medic_w + qt_tempo_prep_medic_w + qt_tempo_prep_pac_w;

	if	(nvl(qt_tempo_medic_w,0) = 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(262417);
	end if;

	select	count(*)
	into	qt_marcacao_w
	from	agenda_quimio_marcacao
	where	dt_agenda_p between dt_Agenda and dt_Agenda + (nr_duracao - 1) / 1440
	and	nr_seq_local		= nr_seq_local_p
	and	nm_usuario		= nm_usuario_p
	and	nr_seq_paciente		= nr_seq_pac_sim_p;
	if	(qt_marcacao_w	> 0) then
		delete 	agenda_quimio_marcacao
		where	dt_agenda_p + 1 /1440 between dt_agenda and dt_agenda + (nr_duracao - 1) / 1440
		and	nr_seq_local		= nr_seq_local_p
		and	nm_usuario		= nm_usuario_p
		and	nr_seq_paciente		= nr_seq_pac_sim_p;

		ie_gerar_w	:= 'S';
	else
		Qt_Consistir_Pac_Prof(nr_seq_prof_w, nr_seq_local_p, dt_agenda_p, qt_tempo_prep_pac_w, nm_usuario_p, cd_estabelecimento_p, null, ds_result_pac_prof_w);
		if	(ds_result_pac_prof_w = 'M') then
			ie_gerar_w		:= 'E';
			ie_prof_marcado_w	:= 'S';
		elsif	(ds_result_pac_prof_w = 'L') then
			ie_gerar_w		:= 'L';
			ie_prof_marcado_w	:= 'S';
		elsif	(ds_result_pac_prof_w = 'H') then
			ie_gerar_w		:= 'H';
			ie_prof_marcado_w	:= 'S';
		end if;

		if	(ie_prof_marcado_w	= 'N') then
			select	count(*)
			into	qt_pendencia_marcada_w
			from	agenda_quimio_marcacao
			where	nr_seq_paciente		= nr_seq_pac_sim_p
			and	trunc(dt_agenda)		= trunc(dt_agenda_p);

			if	(qt_pendencia_marcada_w = 0) then
				select	count(*)
				into	qt_marcacao_w
				from	agenda_quimio_marcacao
				where	((dt_agenda_p between dt_agenda and dt_agenda + (nr_duracao - 1) / 1440)
				or	(dt_agenda_p + (qt_tempo_medic_w - 1) / 1440 between dt_agenda and dt_agenda + (nr_duracao - 1) / 1440)
				or	(dt_agenda between dt_agenda_p and dt_agenda_p + (qt_tempo_medic_w - 1) / 1440)
				or	(dt_agenda + (nr_duracao - 1) / 1440 between dt_agenda_p and dt_agenda_p + (qt_tempo_medic_w - 1) / 1440))
				and	nr_seq_local	= nr_seq_local_p
				and	nr_seq_paciente		<> nr_seq_pac_sim_p;

				if	(qt_marcacao_w	> 0) then
					ie_Gerar_w	:= 'A';
				end if;

				select	count(*)
				into	qt_em_agenda_w
				from	w_agenda_quimio
				where	nr_seq_local	= nr_seq_local_p
				and	nm_usuario	= nm_usuario_p
				and	ie_status = 'EA'
				and	trunc(dt_horario)	= trunc(dt_agenda_p)
				and	nr_seq_paciente	= nr_seq_pac_sim_p;

				select	max(dt_horario)
				into	dt_ultimo_horario_w
				from	w_agenda_quimio
				where	dt_horario between dt_agenda_p and dt_agenda_p + qt_tempo_medic_w / 1440
				and	nr_seq_local	= nr_seq_local_p
				and	nm_usuario	= nm_usuario_p
				and	nr_seq_paciente	= nr_seq_pac_sim_p;
			else
				ie_Gerar_w	:= 'M';
			end if;
		end if;

		if	(ie_perm_mesmo_horario_w = 'S') and
			(((dt_agenda_p + qt_tempo_medic_w / 1440) >= (dt_ultimo_horario_w + nvl(nr_min_apres_local_w,nr_min_apres_w) / 1440)) and
			(qt_marcacao_w		> 0) or
			(qt_em_agenda_w		> 0)) then
			ie_tipo_agendamento_w	:= 'E';
			ie_Gerar_w		:= 'X';
		end if;

		---20011,qt_marcacao_w||'-'||qt_em_Agenda_w||'-'||ie_perm_mesmo_horario_w||'-'||qt_pendencia_marcada_w||'-'||ie_prof_marcado_w||' #@#@');
		if	/*(((dt_agenda_p + qt_tempo_medic_w / 1440) <= (dt_ultimo_horario_w + nr_min_apres_w / 1440)) and
			((qt_marcacao_w		= 0) and 
			(qt_em_agenda_w		= 0)) or
			(ie_perm_mesmo_horario_w = 'S')) and*/
			(qt_pendencia_marcada_w	= 0) and
			(ie_prof_marcado_w	= 'N') then
			--r(-20011,nr_seq_pac_sim_p||' inseriu#@#@');
			insert into agenda_quimio_marcacao
				(nr_sequencia,
				dt_agenda,
				nm_usuario,
				nr_seq_local,
				nr_duracao,
				nr_seq_paciente,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_gerado,
				ie_transferencia,
				nr_seq_prof,
				ie_tipo_agendamento)
			values
				(agenda_quimio_marcacao_seq.nextval,
				dt_agenda_p,
				nm_usuario_p,
				nr_seq_local_p,
				qt_tempo_medic_w,
				nr_seq_pac_sim_p,
				sysdate,
				sysdate,
				nm_usuario_p,
				'N',
				'N',
				nr_seq_prof_w,
				ie_tipo_agendamento_w);
			if	(ie_Gerar_w	<> 'X') then
				ie_gerar_w	:= 'S';
			end if;
		end if;
	end if;
end if;

ie_gerar_p	:= ie_gerar_w;

commit;

end Qt_Atualizar_Dados_Marc_Sim;
/
