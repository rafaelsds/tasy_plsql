create or replace
procedure ev_ajustar_dt_evento is 


Cursor C01 is
	select	nr_sequencia,
		dt_evento
	from	ev_evento_paciente
	order by 1;

nr_sequencia_w	number(10);
dt_evento_w	date;
qt_reg_w	number(10);

begin

qt_reg_w := 0;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	dt_evento_w;
exit when C01%notfound;
	begin
	
	qt_reg_w := qt_reg_w + 1;
	
	UPDATE	ev_evento_pac_destino
	SET	dt_evento = dt_evento_w
	WHERE	nr_seq_ev_pac = nr_sequencia_w;
	
	IF (qt_reg_w >= 10000) THEN
		commit;
		qt_reg_w := 0;
	END IF;
	
	end;
end loop;
close C01;

commit;

end ev_ajustar_dt_evento;
/
