create or replace
procedure pls_verifica_req_item_portal( nm_usuario_p		Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Verificar requisi��es do portal que n�o possuem itens e cancelar as mesmas.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [X] Outros: JOB
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_requisicao_w		pls_requisicao.nr_sequencia%type;
ds_texto_hist_w			varchar2(1000);

Cursor C00 is
	select	a.nr_sequencia
	from	pls_requisicao a
	where	a.ie_tipo_processo	= 'P'
	and	(a.ie_origem_solic	= 'P'
	or	a.ie_origem_solic is null)
	and	a.ie_estagio <> 3
	and	not exists ( 	select	1
				from	pls_requisicao_proc x
				where   x.nr_seq_requisicao = a.nr_sequencia)
	and	not exists ( 	select	1
				from	pls_requisicao_mat y
				where   y.nr_seq_requisicao = a.nr_sequencia)
	and	a.dt_requisicao < trunc(sysdate);

begin

open C00;
loop
fetch C00 into
	nr_seq_requisicao_w;
exit when C00%notfound;
	begin
		update	pls_requisicao
		set	ie_status		= 'C',
			ie_estagio		= 3,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_sequencia		= nr_seq_requisicao_w;	

		ds_texto_hist_w := 'A requisi��o '|| nr_seq_requisicao_w || ' foi cancelada pela JOB: PLS_VERIFICA_REQ_ITEM_PORTAL por n�o possuir nenhum item informado.';
		pls_requisicao_gravar_hist(nr_seq_requisicao_w,'L',ds_texto_hist_w,null,nm_usuario_p);

		commit;
	end;
end loop;
close C00;

end pls_verifica_req_item_portal;
/