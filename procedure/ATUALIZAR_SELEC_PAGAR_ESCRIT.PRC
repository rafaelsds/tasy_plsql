create or replace
procedure atualizar_selec_pagar_escrit(
			nr_seq_escrit_p	number,
			nr_titulo_p	number) is 

begin

if	(nr_seq_escrit_p > 0) then
	begin
	update	titulo_pagar_escrit
	set	ie_selecionado = decode(nvl(ie_selecionado,'N'),'N','S','N')
	where	nr_seq_escrit	= nr_seq_escrit_p
	and	nr_titulo		= nr_titulo_p;	
	end;
end if;

commit;

end atualizar_selec_pagar_escrit;
/
