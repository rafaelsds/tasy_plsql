create or replace
procedure gravar_historico_higienizacao(
			nm_usuario_p			Varchar2,
			ie_tipo_evento_p			varchar2,
			nr_seq_regra_servico_p		number,
			nr_seq_servico_p			number,
			dt_execucao_p			date,
			cd_setor_atendimento_p		number,
			NR_SEQ_SERV_UNID_p		number) is 

			
			
nr_sequencia_w	number(10);
begin


insert into SL_HISTORICO_GERACAO(
			nr_sequencia,
			nm_usuario,
			ie_tipo_evento,
			nr_seq_regra_servico,
			nr_seq_servico,
			dt_atualizacao,
			cd_setor_atendimento,
			cd_funcao,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			NR_SEQ_SERV_UNID)
		values	(
			SL_HISTORICO_GERACAO_seq.nextval,
			nm_usuario_p,
			ie_tipo_evento_p,
			nr_seq_regra_servico_p,
			nr_seq_servico_p,
			dt_execucao_p,
			cd_setor_atendimento_p,
			Obter_Funcao_Ativa,
			sysdate,
			nm_usuario_p,
			NR_SEQ_SERV_UNID_p);




end gravar_historico_higienizacao;
/