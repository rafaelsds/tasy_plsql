CREATE OR REPLACE PROCEDURE AGEINT_INSERIR_BENEFICIARIO_PF(NM_CONTATO_P VARCHAR2,
			       										   IE_TIPO_COMPLEMENTO_P VARCHAR2,
			      										   NR_CPF_P VARCHAR2,
			      										   DT_NASCIMENTO_P VARCHAR2,
			       										   NM_USUARIO_P VARCHAR2,
			      										   IE_SEXO_P	VARCHAR2,
			      										   NM_PESSOA_FISICA_P VARCHAR2,
														   CD_USUARIO_CONVENIO_P VARCHAR2,
														   CD_ESTABELECIMENTO_P	NUMBER,
														   CD_USUARIO_CONVENIO_TIT_P VARCHAR2,
														   NR_TELEFONE_CELULAR_P VARCHAR2,
														   NR_DDD_CELULAR_P VARCHAR2) IS
			       
CD_PESSOA_FISICA_W PESSOA_FISICA.CD_PESSOA_FISICA%TYPE;
CD_CONVENIO_W PARAMETRO_AGENDA_INTEGRADA.CD_CONV_OPERADORA%TYPE;
NR_SEQUENCIA_W	COMPL_PESSOA_FISICA.NR_SEQUENCIA%TYPE;

DT_ATUALIZACAO_W DATE := SYSDATE;
			       
BEGIN 
	
SELECT	MAX(CD_CONV_OPERADORA)
INTO	CD_CONVENIO_W
FROM	PARAMETRO_AGENDA_INTEGRADA
WHERE	NVL(CD_ESTABELECIMENTO, CD_ESTABELECIMENTO_P) = CD_ESTABELECIMENTO_P;
	
IF	(CD_CONVENIO_W IS NOT NULL) THEN
	SELECT	MAX(CD_PESSOA_FISICA)
	INTO    CD_PESSOA_FISICA_W
	FROM	PESSOA_FISICA PF
	WHERE	NR_CPF = NR_CPF_P
	OR EXISTS (SELECT 1 
					 FROM PESSOA_TITULAR_CONVENIO PFC
					WHERE PFC.CD_PESSOA_FISICA = PF.CD_PESSOA_FISICA
					  AND  PFC.CD_USUARIO_CONVENIO = CD_USUARIO_CONVENIO_P
					  AND ROWNUM = 1);

	IF (CD_PESSOA_FISICA_W IS NULL) THEN  
	
		SELECT  PESSOA_FISICA_SEQ.NEXTVAL
		INTO    CD_PESSOA_FISICA_W
		FROM	PESSOA_FISICA
		WHERE   ROWNUM = 1;

		INSERT INTO PESSOA_FISICA (CD_PESSOA_FISICA,
								   NR_CPF,
								   DT_NASCIMENTO,
								   IE_SEXO,
								   NM_USUARIO,
								   NM_PESSOA_FISICA,
								   DT_ATUALIZACAO,
								   DT_ATUALIZACAO_NREC,
								   NM_USUARIO_NREC,
								   IE_TIPO_PESSOA,
								   NR_TELEFONE_CELULAR,
								   NR_DDD_CELULAR)		                          
						   VALUES (CD_PESSOA_FISICA_W,
								   NR_CPF_P,
								   TO_DATE(DT_NASCIMENTO_P, 'YYYY-MM-DD HH24:MI:SS'),
								   IE_SEXO_P,
								   NM_USUARIO_P,
								   NM_PESSOA_FISICA_P,
								   DT_ATUALIZACAO_W,
								   DT_ATUALIZACAO_W,
								   NM_USUARIO_P,
								   1,
								   NR_TELEFONE_CELULAR_P,
								   NR_DDD_CELULAR_P);
								  
		COMMIT;

		IF	(CD_USUARIO_CONVENIO_P IS NOT NULL) THEN
			INSERT INTO PESSOA_TITULAR_CONVENIO (NR_SEQUENCIA,                 
												 DT_ATUALIZACAO,
												 NM_USUARIO,
												 DT_ATUALIZACAO_NREC,
												 NM_USUARIO_NREC,
												 CD_PESSOA_FISICA,
												 CD_CONVENIO,
												 CD_USUARIO_CONVENIO,
												 CD_USUARIO_CONVENIO_TIT) 
									     VALUES (PESSOA_TITULAR_CONVENIO_SEQ.NEXTVAL,
												 DT_ATUALIZACAO_W,
												 NM_USUARIO_P,
												 DT_ATUALIZACAO_W,
												 NM_USUARIO_P,
												 CD_PESSOA_FISICA_W,
												 CD_CONVENIO_W,
												 CD_USUARIO_CONVENIO_P,
												 CD_USUARIO_CONVENIO_TIT_P);
		END IF; 
		
		IF	(NM_CONTATO_P IS NOT NULL) THEN
		
			SELECT	NVL(MAX(NR_SEQUENCIA),0) + 1
			INTO	NR_SEQUENCIA_W
			FROM	COMPL_PESSOA_FISICA
			WHERE	CD_PESSOA_FISICA = CD_PESSOA_FISICA_W;
			
			INSERT INTO COMPL_PESSOA_FISICA (NR_SEQUENCIA,
											 CD_PESSOA_FISICA,
											 IE_TIPO_COMPLEMENTO,
											 NM_CONTATO,
											 NM_USUARIO,
											 NM_USUARIO_NREC,
											 DT_ATUALIZACAO,
											 DT_ATUALIZACAO_NREC)							   		                           
									VALUES 	(NR_SEQUENCIA_W,
											 CD_PESSOA_FISICA_W,
											 1,
											 NM_CONTATO_P,
											 NM_USUARIO_P,
											 NM_USUARIO_P,
											 DT_ATUALIZACAO_W,
											 DT_ATUALIZACAO_W);
			END IF;
		COMMIT;
	END IF;	               		 	
END IF;

END AGEINT_INSERIR_BENEFICIARIO_PF;
/
