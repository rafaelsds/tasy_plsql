create or replace
procedure Liberar_Estornar_Avaliacao
		(nr_sequencia_p		number,		 
		 ie_tipo_p			varchar2,
		 nm_usuario_p		varchar2) is

begin

if	(ie_tipo_p = 'L') then

	/*
	update	med_avaliacao_paciente
	set	dt_liberacao = sysdate,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;
	*/
	Liberar_avaliacao(nr_sequencia_p,nm_usuario_p);
	
end if;

if	(ie_tipo_p = 'E') then

	update	med_avaliacao_paciente
	set	dt_liberacao = null,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;
	
end if;

commit;

end Liberar_Estornar_Avaliacao;
/