create or replace procedure CMC_GERAR_RETORNO_ITAU_400(	nr_seq_cobr_escrit_p	number,
					nm_usuario_p	varchar2) is

nr_titulo_w			number(10);
vl_juros_W			number(15,2);
vl_desconto_w			number(15,2);
vl_abatimento_w			number(15,2);
vl_liquido_w			number(15,2);
dt_liquidacao_w			date;
nr_dt_liquidacao_w			number(10);
cont_w				number(10);
ie_tipo_carteira_w			varchar2(255);
ds_titulo_w			varchar2(255);
vl_cobranca_w			number(15,2);
cd_ocorrencia_w			varchar2(255);
nr_seq_ocorrencia_ret_w		number(10);
vl_tarifa_cobranca_w		number(15,2);
vl_saldo_titulo_w			number(15,2);
cd_banco_w			number(3);
cd_banco_cobr_w			number(5);
cd_centro_custo_desc_w		number(8);
nr_seq_motivo_desc_w		number(10);
nr_seq_tit_rec_cobr_w		number(10);
cd_pessoa_fisica_desc_w		varchar2(10);
cd_cgc_desc_w			varchar2(14);
cd_carteira_w			varchar2(40);
ie_separa_carteira_w		varchar2(1);
cd_estabelecimento_w		number(4);
vl_titulo_w			number(15,2);
ie_permite_estab_w	varchar2(1);
cd_estab_ativo_w	number(4);

cursor c01 is
	select	trim(substr(ds_string,38,25)),
		to_number(substr(ds_string,153,13))/100,
		to_number(substr(ds_string,267,13))/100,
		to_number(substr(ds_string,241,13))/100,
		to_number(substr(ds_string,228,13))/100,
		to_number(substr(ds_string,254,13))/100,
		to_number(substr(ds_string,176,13))/100,
		nvl(somente_numero(substr(ds_string,296,6)),0),
		substr(ds_string,109,2)
	from	w_retorno_banco
	where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
	and	substr(ds_string,1,1)	= '1'
	and	(nvl(ie_separa_carteira_w,'N') = 'N' or cd_carteira_w is null or trim(substr(ds_string,83,3)) = cd_carteira_w);
begin

select	max(to_number(substr(ds_string,77,3)))
into	cd_banco_w
from	w_retorno_banco
where	nr_seq_cobr_escrit	= nr_seq_cobr_escrit_p
and	substr(ds_string,1,1)	= '0';

select	max(a.cd_banco),
	max(b.cd_carteira),
	max(a.cd_estabelecimento)
into	cd_banco_cobr_w,
	cd_carteira_w,
	cd_estabelecimento_w
from	banco_carteira b,
	cobranca_escritural a
where	a.nr_sequencia		= nr_seq_cobr_escrit_p
and	a.nr_seq_carteira_cobr	= b.nr_sequencia(+);

if	(cd_banco_w <> cd_banco_cobr_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(226931);
end if;

cd_estab_ativo_w := obter_estabelecimento_ativo;
obter_param_usuario(815,5,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_separa_carteira_w);
obter_param_usuario(815,47,obter_perfil_ativo,nm_usuario_p,cd_estab_ativo_w,ie_permite_estab_w);

open C01;
loop
fetch C01 into
	ds_titulo_w,
	vl_cobranca_w,
	vl_juros_w,
	vl_desconto_w,
	vl_abatimento_w,
	vl_liquido_w,
	vl_tarifa_cobranca_w,
	nr_dt_liquidacao_w,
	cd_ocorrencia_w;
exit when C01%notfound;
	begin

	select	max(nr_titulo),
		nvl(max(vl_saldo_titulo),0),
		nvl(max(vl_titulo),0)
	into	nr_titulo_w,
		vl_saldo_titulo_w,
		vl_titulo_w
	from	titulo_receber
	where	nr_titulo	= somente_numero(ds_titulo_w);

	if (nr_titulo_w is null) then

		/* Encontrar pelo titulo externo */
		select	max(nr_titulo),
				nvl(max(vl_saldo_titulo),0),
				nvl(max(vl_titulo),0)
		into	nr_titulo_w,
				vl_saldo_titulo_w,
				vl_titulo_w
		from	titulo_receber
		where	nr_titulo_externo		= ds_titulo_w
		and	nvl(nr_titulo_externo,'0')	<> '0'
		and	((ie_permite_estab_w = 'S') or (cd_estabelecimento = cd_estab_ativo_w or cd_estab_financeiro = cd_estab_ativo_w));

	end if;

	/* Se encontrou o titulo importa, senao grava no log */

	if	(nr_titulo_w is not null) then

		ie_tipo_carteira_w	:= OBTER_TIPO_CARTEIRA_REGRA(nr_titulo_w);

		update	titulo_receber
		set	ie_tipo_carteira	= ie_tipo_carteira_w
		where	nr_titulo		= nr_titulo_w
		and	ie_tipo_carteira	is null;

		select 	max(a.nr_sequencia)
		into	nr_seq_ocorrencia_ret_w
		from	banco_ocorr_escrit_ret a
		where	a.cd_banco = 341
		and	a.cd_ocorrencia = cd_ocorrencia_w;

		select	max(a.cd_centro_custo),
			max(a.nr_seq_motivo_desc),
			max(a.cd_pessoa_fisica),
			max(a.cd_cgc)
		into	cd_centro_custo_desc_w,
			nr_seq_motivo_desc_w,
			cd_pessoa_fisica_desc_w,
			cd_cgc_desc_w
		from	titulo_receber_liq_desc a
		where	a.nr_titulo	= nr_titulo_w
		and	a.nr_bordero	is null
		and	a.nr_seq_liq	is null;

		if	(nvl(trim(nr_dt_liquidacao_w),0) <> 0) then
			dt_liquidacao_w	:= to_date(to_char(nr_dt_liquidacao_w, '000000'),'ddmmyy');
		end if;

		select	count(*)
		into	cont_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

		if	(cont_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(226932,'NR_TITULO_W=' || nr_titulo_w);
		end if;

		if	(vl_titulo_w	= nvl(vl_liquido_w,0)) then

			vl_liquido_w	:= nvl(vl_liquido_w,0) - nvl(vl_juros_w,0);

		end if;

		if (((nvl(vl_liquido_w,0) - nvl(vl_juros_w,0)) + nvl(vl_tarifa_cobranca_w,0)) <= nvl(vl_saldo_titulo_w,0)) then
			vl_liquido_w := nvl(vl_liquido_w,0) +  nvl(vl_tarifa_cobranca_w,0);
		end if;

		select	titulo_receber_cobr_seq.nextval
		into	nr_seq_tit_rec_cobr_w
		from	dual;

		insert	into titulo_receber_cobr (	NR_SEQUENCIA,
							NR_TITULO,
							CD_BANCO,
							VL_COBRANCA,
							VL_DESCONTO,
							VL_ACRESCIMO,
							VL_DESPESA_BANCARIA,
							VL_LIQUIDACAO,
							VL_JUROS,
							DT_LIQUIDACAO,
							DT_ATUALIZACAO,
							NM_USUARIO,
							NR_SEQ_COBRANCA,
							nr_seq_ocorrencia_ret,
							vl_saldo_inclusao,
							VL_MULTA,
							nr_seq_motivo_desc,
							cd_centro_custo_desc)
					values	(	nr_seq_tit_rec_cobr_w,
							nr_titulo_w,
							341,
							nvl(vl_cobranca_w,0),
							nvl(vl_desconto_w,0) + nvl(vl_abatimento_w,0),
							0,
							nvl(vl_tarifa_cobranca_w,0),
							nvl(vl_liquido_w,0),
							nvl(vl_juros_W,0),
							nvl(dt_liquidacao_w,sysdate),
							sysdate,
							nm_usuario_p,
							nr_seq_cobr_escrit_p,
							nr_seq_ocorrencia_ret_w,
							nvl(vl_saldo_titulo_w,0),
							0,
							nr_seq_motivo_desc_w,
							cd_centro_custo_desc_w);

		insert	into titulo_rec_cobr_desc
			(cd_cgc,
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			nr_seq_tit_rec_cobr,
			nr_sequencia)
		values	(cd_cgc_desc_w,
			cd_pessoa_fisica_desc_w,
			sysdate,
			nm_usuario_p,
			nr_seq_tit_rec_cobr_w,
			titulo_rec_cobr_desc_seq.nextval);

	else
		fin_gerar_log_controle_banco(3, WHEB_MENSAGEM_PCK.get_texto(811232, 'DS_TITULO_W=' || ds_titulo_w) ,nm_usuario_p,'N');

	end if;

	end;
end loop;
close C01;

commit;

end CMC_GERAR_RETORNO_ITAU_400;
/