create or replace
procedure tasylab_validar_result_gerado (	nr_prescricao_p		number,
											nr_seq_prescricao_p	number) is 

nr_seq_resultado_w			number(10);
qt_item_w					number(10);
ie_status_receb_lote_ext_w 	number(2);
cd_estab_prescr_w			number(4);
nr_lote_ext_w				lab_lote_externo.nr_sequencia%type;
qt_exame_lote_nao_aprov_w	number(10);
											
begin

/*select	max(nr_seq_resultado)
into	nr_seq_resultado_w
from	exame_lab_resultado
where	nr_prescricao = nr_prescricao_p;

if	(nr_seq_resultado_w is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(241140);
end if;

select	count(*)
into	qt_item_w
from	exame_lab_result_item
where	nr_seq_resultado = nr_seq_resultado_w
and		nr_seq_resultado*/

select	count(*)
into	qt_item_w
from	result_laboratorio
where	nr_prescricao = nr_prescricao_p
and		nr_seq_prescricao = nr_seq_prescricao_p;

if		(qt_item_w >= 2) then
	
	delete	result_laboratorio
	where	nr_prescricao = nr_prescricao_p
	and		nr_seq_prescricao = nr_seq_prescricao_p;	
		
	wheb_mensagem_pck.exibir_mensagem_abort(241137);

elsif	(qt_item_w >= 2) then
	wheb_mensagem_pck.exibir_mensagem_abort(241138);
else

	select	max(cd_estabelecimento)
	into	cd_estab_prescr_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
	
	select 	nvl(max(ie_status_receb_lote_ext),35)
	into	ie_status_receb_lote_ext_w
	from	lab_parametro
	where	cd_estabelecimento = cd_estab_prescr_w;

	update 	prescr_procedimento
	set		ie_status_atend = ie_status_receb_lote_ext_w,
			nm_usuario = 'TasyLab', --nm_usuario_p,
			dt_atualizacao = sysdate
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia = nr_seq_prescricao_p
	and		ie_status_atend < ie_status_receb_lote_ext_w;
end if;

select 	max(nr_seq_lote_externo)
into	nr_lote_ext_w
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p
and	nr_sequencia = nr_seq_prescricao_p;

if (nvl(nr_lote_ext_w,0) > 0) then
	select 	count(*)
	into	qt_exame_lote_nao_aprov_w
	from 	prescr_procedimento
	where 	nr_seq_lote_externo = nr_lote_ext_w
	and 	ie_status_atend < 35;

	if (qt_exame_lote_nao_aprov_w = 0) then
		update 	lab_lote_externo
		set		dt_retorno = sysdate
		where	nr_sequencia = nr_lote_ext_w;
	end if;
	
end if;

--OS727249 - Ivan
--commit;

end tasylab_validar_result_gerado;
/