create or replace PROCEDURE LINK_SYSTEM_REQ_TO_BUSINESS(nr_seq_superior_p   number default 0,
                                                        nm_usuario_p        varchar,
                                                        nr_sequencia_p      number,
                                                        link_flag           varchar,
                                                        origin_p            varchar default 'BR')
AS

nr_seq_req_prs_w        LATAM_REQUISITO.NR_SEQ_REQ_PRS%TYPE;
ie_liberar_desenv_old_w LATAM_REQUISITO.IE_LIBERAR_DESENV%type;
BEGIN

SELECT  MAX(NR_SEQ_REQ_PRS)
INTO    nr_seq_req_prs_w
FROM    LATAM_REQUISITO x
WHERE   x.NR_SEQUENCIA = nr_seq_superior_p; 

IF (link_flag = 'LINK') THEN
    UPDATE  LATAM_REQUISITO
    SET     NR_SEQ_SUPERIOR     = nr_seq_superior_p,
            NM_USUARIO          = nm_usuario_p,
            DT_ATUALIZACAO      = sysdate,
            NR_SEQ_REQ_PRS      = nr_seq_req_prs_w
    WHERE   NR_SEQUENCIA        = nr_sequencia_p;

ELSE 

    UPDATE  LATAM_REQUISITO
    SET     NR_SEQ_SUPERIOR     = NULL,
            NM_USUARIO          = nm_usuario_p,
            DT_ATUALIZACAO      = sysdate,
            NR_SEQ_REQ_PRS      = NULL
    WHERE   NR_SEQUENCIA        = nr_sequencia_p;

END IF;

SELECT  IE_LIBERAR_DESENV
INTO    ie_liberar_desenv_old_w
FROM    LATAM_REQUISITO
WHERE   LATAM_REQUISITO.NR_SEQUENCIA = DECODE (origin_p, 'BR', nr_seq_superior_p, nr_sequencia_p);

IF origin_p = 'BR' THEN
    generate_latam_log(nr_seq_superior_p, ie_liberar_desenv_old_w, CASE WHEN link_flag = 'LINK' THEN 1061484 ELSE 1061494 END);
ELSE
    generate_latam_log(nr_sequencia_p, ie_liberar_desenv_old_w, CASE WHEN link_flag = 'LINK' THEN 1031813 ELSE 1032155 END);
END IF;

COMMIT;

END LINK_SYSTEM_REQ_TO_BUSINESS;
/
