CREATE OR REPLACE PROCEDURE gqa_justifica_atraso(nr_sequencia_p     IN NUMBER
                                                ,nr_seq_motivo_p    IN NUMBER
                                                ,ds_justificativa_p IN VARCHAR2) IS
  nm_usuario_w gqa_protocolo_etapa_pac.nm_usuario%TYPE;
BEGIN
  SELECT obter_usuario_ativo
    INTO nm_usuario_w
    FROM dual;

  UPDATE gqa_protocolo_etapa_pac
     SET nr_seq_motivo_atraso    = nr_seq_motivo_p
        ,ds_justificativa_atraso = ds_justificativa_p
        ,dt_atualizacao          = SYSDATE
        ,nm_usuario              = nm_usuario_w
   WHERE nr_sequencia = nr_sequencia_p;
END gqa_justifica_atraso;
/
