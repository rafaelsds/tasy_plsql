create or replace 
procedure consistir_ret_grg(	nr_seq_retorno_p	number,
				ds_retorno_p 		out varchar2) is

nr_titulo_w			titulo_receber.nr_titulo%type;
nr_seq_lote_audit_w		lote_audit_hist.nr_seq_lote_audit%type;
nr_seq_lote_hist_w		lote_audit_hist_guia.nr_seq_lote_hist%type;
nr_interno_conta_w		lote_audit_hist_guia.nr_interno_conta%type;
ie_conv_diferente_w		varchar2(15) := 'N';
nm_usuario_w			usuario.nm_usuario%type;
cd_estab_w			estabelecimento.cd_estabelecimento%type;
conta_w				varchar2(4000);
cd_convenio_w			convenio_retorno.cd_convenio%type;
dt_fechamento_w			convenio_retorno.dt_fechamento%type;

Cursor	C01 is
	select	a.nr_titulo,
		b.cd_convenio,
		b.dt_fechamento
	from	convenio_retorno_item a,
		convenio_retorno b
	where	a.nr_seq_retorno = b.nr_sequencia 
	and	b.nr_sequencia	= nr_seq_retorno_p
	group by a.nr_titulo,
		b.cd_convenio,
		b.dt_fechamento;

type 		fetch_array is table of c01%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_c01_w			vetor;
	
begin

begin
cd_estab_w	:= nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
exception
when others then
	cd_estab_w := 0;
end;
begin
nm_usuario_w	:= nvl(wheb_usuario_pck.get_nm_usuario,'');
exception
when others then
	nm_usuario_w := '';
end;

ie_conv_diferente_w	:= nvl(obter_valor_param_usuario(27,70,obter_perfil_ativo,nm_usuario_w,cd_estab_w),'N');

open c01;
loop
fetch c01 bulk collect into s_array limit 1000;
	vetor_c01_w(i) := s_array;
	i := i + 1;
exit when c01%notfound;
end loop;
close c01;

for i in 1..vetor_c01_w.count loop
	begin
	s_array := vetor_c01_w(i);
	for z in 1..s_array.count loop
		begin
		
		nr_titulo_w	:= s_array(z).nr_titulo;
		cd_convenio_w	:= s_array(z).cd_convenio;
		dt_fechamento_w	:= s_array(z).dt_fechamento;
		
		if	(ie_conv_diferente_w = 'S') then
			begin
						
			begin
			select	b.nr_seq_lote_audit,
				b.nr_sequencia
			into	nr_seq_lote_audit_w,
				nr_seq_lote_hist_w
			from	lote_audit_hist_guia a,
				lote_audit_hist b,
				lote_auditoria c
			where	a.nr_seq_lote_hist 	= b.nr_sequencia
			and	b.nr_seq_lote_audit	= c.nr_sequencia
			and	c.dt_atualizacao_nrec 	> dt_fechamento_w
			and	b.dt_baixa_glosa is null
			and	nvl(obter_titulo_conta_guia(a.nr_interno_conta,a.cd_autorizacao,null,null),0) = nr_titulo_w
			and      exists   (select  1
					from     lote_audit_hist_item x
					where    x.nr_seq_guia  = a.nr_sequencia
					and      x.vl_glosa     > 0)
			and	rownum = 1;		
			exception
			when others then
				nr_seq_lote_audit_w	:= null;
				nr_seq_lote_hist_w	:= null;
			end;
			
			end;
		else
			begin
			
			begin
			select	b.nr_seq_lote_audit,
				b.nr_sequencia
			into	nr_seq_lote_audit_w,
				nr_seq_lote_hist_w
			from	lote_audit_hist_guia a,
				lote_audit_hist b,
				lote_auditoria c
			where	a.nr_seq_lote_hist 	= b.nr_sequencia
			and	b.dt_baixa_glosa is null
			and	b.nr_seq_lote_audit	= c.nr_sequencia
			and	c.cd_convenio		= cd_convenio_w
			and	c.dt_atualizacao_nrec 	> dt_fechamento_w
			and	nvl(obter_titulo_conta_guia(a.nr_interno_conta,a.cd_autorizacao,null,null),0) = nr_titulo_w
			and      exists   (select  1
					from     lote_audit_hist_item x
					where    x.nr_seq_guia  = a.nr_sequencia
					and      x.vl_glosa     > 0)
			and	rownum = 1;		
			exception
			when others then
				nr_seq_lote_audit_w	:= null;
				nr_seq_lote_hist_w	:= null;
			end;
			
			end;
		end if;
		

		if 	(conta_w is null) then
			if	(nr_seq_lote_hist_w is not null) then
				conta_w	:= 	wheb_mensagem_pck.get_texto(800049,'nr_seq_lote_audit_w=' || nr_seq_lote_audit_w || ';' || 'nr_seq_lote_hist_w=' || nr_seq_lote_hist_w);
			end if;
		else
			if	(nr_seq_lote_hist_w is not null) then
				conta_w	:=	conta_w || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(800049,'nr_seq_lote_audit_w=' || nr_seq_lote_audit_w || ';' || 'nr_seq_lote_hist_w=' || nr_seq_lote_hist_w);
			end if;
		end if;
		
		end;
	end loop;
	end;
end loop;

if	(conta_w is not null) then
	conta_w := substr(wheb_mensagem_pck.get_texto(351685,'CONTA_W=' || conta_w ),1,255);
end if;

ds_retorno_p := conta_w;

end consistir_ret_grg;
/