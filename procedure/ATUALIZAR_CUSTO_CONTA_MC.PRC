create or replace
procedure atualizar_custo_conta_mc(	nr_interno_conta_p	number,
				nm_usuario_p		Varchar2) is 

cd_estabelecimento_w		number(10);
cd_empresa_w			number(10);
ds_item_w			varchar2(80);
dt_mesano_referencia_w		date;
nr_seq_item_mc_w			number(10);
pr_item_w			number(15,4);
qt_registro_w			number(10);
vl_custo_variavel_w		number(15,2);
vl_receita_w			number(15,2);

cursor c01 is
select	a.nr_sequencia,
	a.ds_item
from	cus_item_mc a
where	cd_empresa	= cd_empresa_w
and	ie_situacao	= 'A'
order by a.nr_seq_apres;

begin

delete	from cus_conta_pac_mc
where	nr_interno_conta	= nr_interno_conta_p;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

select	cd_estabelecimento,
	dt_mesano_referencia
into	cd_estabelecimento_w,
	dt_mesano_referencia_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

cd_empresa_w	:= obter_empresa_estab(cd_estabelecimento_w);

select	count(*)
into	qt_registro_w
from	cus_item_mc_regra
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(qt_registro_w > 0) then

	select	nvl(sum(vl_procedimento),0) + nvl(sum(vl_material),0)
	into	vl_receita_w
	from	conta_paciente_resumo
	where	nr_interno_conta		= nr_interno_conta_p
	and	nvl(qt_exclusao_custo,0)	= 0;
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_item_mc_w,
		ds_item_w;
	exit when C01%notfound;
		begin
		
		pr_item_w		:= cus_obter_perc_item_mc(cd_estabelecimento_w, nr_seq_item_mc_w, dt_mesano_referencia_w);
		
		vl_custo_variavel_w	:= (vl_receita_w * ( dividir(pr_item_w,100) ) );
				
		insert into cus_conta_pac_mc( 
			nr_sequencia,
			nr_interno_conta,
			dt_atualizacao, 
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_item,
			pr_aplicado,
			vl_custo_variavel)
		values(	cus_conta_pac_mc_seq.nextval,
			nr_interno_conta_p,
			sysdate, 
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_item_mc_w,
			pr_item_w,
			vl_custo_variavel_w);

		end;
	end loop;
	close C01;
end if;

end atualizar_custo_conta_mc;
/
