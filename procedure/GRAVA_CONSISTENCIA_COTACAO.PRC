create or replace
procedure grava_consistencia_cotacao(		nr_cot_compra_p		number,
					nr_item_cot_compra_p	number,
					nr_seq_fornecedor_p	number,
					ds_consistencia_p		varchar2,
					ie_bloqueia_p		varchar2,
					nm_usuario_p		Varchar2) is 

begin
insert into cot_compra_consist_calc(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_cot_compra,
	nr_item_cot_compra,
	nr_seq_fornecedor,
	ds_consistencia,
	ie_bloqueia)
values(	cot_compra_consist_calc_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_cot_compra_p,
	nr_item_cot_compra_p,
	nr_seq_fornecedor_p,
	ds_consistencia_P,
	ie_bloqueia_p);
commit;

end grava_consistencia_cotacao;
/