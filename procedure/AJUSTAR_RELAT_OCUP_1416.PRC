create or replace
procedure ajustar_relat_ocup_1416  is 

nr_seq_banda_w		number(10);
ie_atualiza_w		varchar2(10);

begin

ie_atualiza_w	:=  nvl(obter_valor_param_usuario(44,40,obter_perfil_ativo,obter_usuario_ativo,obter_estabelecimento_ativo),'N');

select 	a.nr_sequencia
into	nr_seq_banda_w
from	banda_relatorio a,
	relatorio b
where	upper(a.ds_banda) 	= upper('Dados Ocupa��o')
and 	a.nr_seq_apresentacao 	= 2
and	b.nr_sequencia 		= a.nr_seq_relatorio
and	b.cd_relatorio 		= 1416;


if	((nvl(nr_seq_banda_w,0) > 0 )  and 
	(ie_atualiza_w = 'S'))  then
	
	update	banda_relatorio
	set	qt_altura	=    36
	where	nr_sequencia	=    nr_seq_banda_w;
	
	update	banda_relat_campo
	set	qt_altura	=    34,
		qt_tamanho	=    130,
		qt_esquerda	=    60
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Paciente');
	
	commit;
	
	update	banda_relat_campo
	set	qt_altura	=    34,
		qt_tamanho	=    20
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Unid.');
	
	update	banda_relat_campo
	set	qt_esquerda	=    195
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Data Nascto');
	
	update	banda_relat_campo
	set	qt_esquerda	=    253
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Entrada Hospital');	
	
	update	banda_relat_campo
	set	qt_altura	=    17,
		qt_tamanho	=    100,
		qt_esquerda	=    340,
		qt_topo		=    1,
		qt_tam_fonte	=    8,
		ds_label	=    'Entrada Unidade'
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Entrada Unidade');
	
end if;

if	((nvl(nr_seq_banda_w,0) > 0 )  and 
	( ie_atualiza_w = 'N')) then
	
	update	banda_relatorio
	set	qt_altura	=    17
	where	nr_sequencia	=    nr_seq_banda_w;
	
	update	banda_relat_campo
	set	qt_altura	=    17,
		qt_tamanho	=    210,
		qt_esquerda	=    60
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Paciente');
	
	update	banda_relat_campo
	set	qt_altura	=    17,
		qt_tamanho	=    50
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Unid.');
	
	update	banda_relat_campo
	set	qt_esquerda	=    273
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Data Nascto');
	
		
	update	banda_relat_campo
	set	qt_esquerda	=    332
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Entrada Hospital');	
	
	update	banda_relat_campo
	set	qt_altura	=    0,
		qt_tamanho	=    0,
		qt_esquerda	=    0,
		qt_topo		=    0,
		qt_tam_fonte	=    0,
		ds_label	=    ''
	where	nr_seq_banda	=    nr_seq_banda_w
	and	upper(ds_campo) =    upper('Entrada Unidade');
	
end if;

commit;

end ajustar_relat_ocup_1416;
/