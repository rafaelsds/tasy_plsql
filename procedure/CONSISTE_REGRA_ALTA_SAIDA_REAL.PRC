create or replace 
procedure consiste_regra_alta_saida_real	(dt_saida_real_p	date,
						nr_atendimento_p	number,
						cd_setor_atendimento_p	number,
						ds_consistencia_p	out	varchar2,
						nm_usuario_p		varchar2 ) is

qt_tempo_minuto_w	number(10);
qt_tempo_minuto_regra_w	number(10);
ie_tipo_atendimento_w	number(3);
dt_alta_w		date;

cursor c01 is
	select	nvl(qt_tempo_minuto,0)
	from	regra_alta_saida_real
	where	nvl(cd_setor_atendimento,cd_setor_atendimento_p) = cd_setor_atendimento_p
	and	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w) = ie_tipo_atendimento_w
	order by nvl(ie_tipo_atendimento,0),
		nvl(cd_setor_atendimento,0);
begin

qt_tempo_minuto_w	:= 0;

select	max(dt_alta),
	max(ie_tipo_atendimento)
into	dt_alta_w,
	ie_tipo_atendimento_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

open c01;
loop
fetch c01 into
	qt_tempo_minuto_regra_w;
exit when c01%notfound;
	begin
	qt_tempo_minuto_w	:= qt_tempo_minuto_regra_w;
	end;
end loop;
close c01;

if	(qt_tempo_minuto_w > 0) then


	if	((dt_saida_real_p - dt_alta_w) * 1440 >  qt_tempo_minuto_w) then

		ds_consistencia_p	:= obter_desc_expressao(774964);

	end if;

end if;


end;
/