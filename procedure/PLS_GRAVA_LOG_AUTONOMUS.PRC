create or replace
procedure pls_grava_log_autonomus (	ds_p			pls_log_execucao_temp.ds%type,
					ds_processo_p		pls_log_execucao_temp.ds_processo%type,
					nm_usuario_p		usuario.nm_usuario%type) is
						
PRAGMA AUTONOMOUS_TRANSACTION;

ds_sql_w	varchar2(500);
nm_id_sid_w	pls_log_execucao_temp.nm_id_sid%type;
nm_id_serial_w	pls_log_execucao_temp.nm_id_serial%type;

begin

begin
	ds_sql_w := 	'select	sid,' || pls_util_pck.enter_w ||
			'	serial#' || pls_util_pck.enter_w ||
			'	from	v$session' || pls_util_pck.enter_w ||
			'where	audsid = userenv(''SESSIONID'')';

	execute immediate ds_sql_w into nm_id_sid_w, nm_id_serial_w;

exception
when others then
	nm_id_sid_w := null;
	nm_id_serial_w := null;
end;

insert into pls_log_execucao_temp (	nr_sequencia,				data,			nm_usuario,		nm_id_sid,
					nm_id_serial,				ds,			ds_processo,		ds_extra)
			values (	pls_log_execucao_temp_seq.nextval,	sysdate,		nm_usuario_p,		nm_id_sid_w,
					nm_id_serial_w,				ds_p,			ds_processo_p,		dbms_utility.format_call_stack);
commit;

end pls_grava_log_autonomus;
/