create or replace
procedure liberar_amostras_patologia(	nr_prescricao_p	number,
				nr_seq_proced_p	number,
				nr_seq_status_p	varchar2,
				nm_usuario_p	varchar2) is 

begin
/*
Esta procedure libera todas as amostras ligadas a um procedimento de patologia de  uma prescrição.
*/
update 	prescr_procedimento
set	nr_seq_status_pato = nr_seq_status_p
where	nr_sequencia = nr_seq_proced_p
and	nr_prescricao = nr_prescricao_p;

update	prescr_proc_peca
set	ie_status	=	'E'
where	nr_prescricao	=	nr_prescricao_p
and	nr_seq_prescr	= 	nr_seq_proced_p;

insert 	into	prescr_proc_lib_caso
			(nr_sequencia,
			 dt_atualizacao,
			 nm_usuario,
			 dt_atualizacao_nrec,
			 nm_usuario_nrec,
			 nr_prescricao,
			 nr_seq_prescr)
values			(prescr_proc_lib_caso_seq.nextval,
			 sysdate,
			 nm_usuario_p,
			 sysdate,
			 nm_usuario_p,
			 nr_prescricao_p,
			 nr_seq_proced_p);

commit;

end liberar_amostras_patologia;
/
