create or replace
procedure	same_gravar_sequencia(	nr_seq_local_p		number,
					cd_caixa_ini_p		varchar2,
					cd_caixa_fim_p		varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is
cd_caixa_w	varchar2(20);
nr_sequencia_w	number(10);
nr_seq_existe_w	number(10);
cd_caixa_ini_w	number;
cd_caixa_fim_w	number;
begin
	cd_caixa_ini_w := cd_caixa_ini_p;
	cd_caixa_fim_w := cd_caixa_fim_p;
	while cd_caixa_ini_w <= cd_caixa_fim_w loop
		cd_caixa_w := cd_caixa_ini_w;
		
		select max(nr_sequencia)
		into nr_seq_existe_w
		from same_caixa
		where cd_caixa = cd_caixa_w
		and nr_seq_local = nr_seq_local_p;
		
		if(nr_seq_existe_w is null) then
			begin
				select	same_caixa_seq.nextval
				into	nr_sequencia_w
				from	dual;		
				
				insert into	same_caixa
				(	nr_sequencia,
					nr_seq_local,
					cd_caixa,
					cd_estabelecimento,
					ie_situacao,
					nm_usuario,			
					dt_atualizacao
				)
				values
				(	nr_sequencia_w,
					nr_seq_local_p,
					cd_caixa_w,
					cd_estabelecimento_p,
					'A',
					nm_usuario_p,
					sysdate
				);
			end;
		else
			wheb_mensagem_pck.exibir_mensagem_abort(1075830);
		end if;
		cd_caixa_ini_w := cd_caixa_ini_w + 1;
	end loop;
if(nr_seq_existe_w is null) then
	commit;
end if;

end same_gravar_sequencia;
/