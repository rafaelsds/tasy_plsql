create or replace procedure dados_cirurgia_prot(nr_cirurgia_p number,
							nr_seq_pepo_p number,
							ds_anestesista_p out varchar2 ,
							cd_medico_anestesista_p out varchar2,
							nr_seq_proc_interno_p out number,
							ds_procedimento_p out varchar2)is
	
parametro               varchar2(1);
cd_medico_anestesista_w varchar2(10):='';
ds_anestesista_w        varchar(255) :='';
nr_seq_proc_interno_w   number      :=0;
param_estrutura_pepo_w  varchar2(1) :='N';
ds_procedimento_w       procedimento.ds_procedimento%TYPE :='';
begin
	
	if (nr_seq_pepo_p is not null and nr_seq_pepo_p > 0) then
		
		select cd_medico_anestesista,
			   substr(obter_nome_pf(cd_medico_anestesista),1,255)ds_anestesia
		into   cd_medico_anestesista_w,	
			   ds_anestesista_w
		from   pepo_cirurgia
		where  nr_sequencia = nr_seq_pepo_p ;
	
	elsif(nr_cirurgia_p is not null and nr_cirurgia_p > 0)  then
		
		select cd_medico_anestesista,
			   substr(obter_nome_pf(cd_medico_anestesista),1,255)ds_anestesia,
			   nr_seq_proc_interno,
			   substr(obter_exame_agenda(cd_procedimento_princ, ie_origem_proced, nr_seq_proc_interno),1,255)
		into   cd_medico_anestesista_w,
			   ds_anestesista_w,
			   nr_seq_proc_interno_w,
			   ds_procedimento_w
		from   cirurgia
		where  nr_cirurgia = nr_cirurgia_p;
		
    end if;
 
ds_anestesista_p        := substr(ds_anestesista_w,1,255);
cd_medico_anestesista_p := cd_medico_anestesista_w;
nr_seq_proc_interno_p   := nr_seq_proc_interno_w;
ds_procedimento_p       := substr(ds_procedimento_w,1,255);

end dados_cirurgia_prot;
/