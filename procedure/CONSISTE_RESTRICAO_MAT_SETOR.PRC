create or replace
procedure consiste_restricao_mat_setor(
		cd_estabelecimento_p	number,
		cd_material_p		number,
		ie_permite_p		varchar2,
		nr_sequencia_p		number,
		nm_usuario_p		Varchar2) is

ie_erro_restricao_w	varchar2(1);

begin
if	(cd_estabelecimento_p is not null) and
	(cd_material_p is not null) and
	(ie_permite_p is not null) and
	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	select	decode(count(*),0,'N','S')
	into	ie_erro_restricao_w
	from	material_setor_exclusivo
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_material		= cd_material_p
	and	ie_permite		<> ie_permite_p
	and	nr_sequencia		= nr_sequencia_p;
	
	if	(ie_erro_restricao_w = 'S') then
		begin
		-- Todos os setores cadastrados devem ter o mesmo tipo de permiss�o ou restri��o.
		Wheb_mensagem_pck.exibir_mensagem_abort(140859);
		end;
	end if;
	end;
end if;
commit;
end consiste_restricao_mat_setor;
/