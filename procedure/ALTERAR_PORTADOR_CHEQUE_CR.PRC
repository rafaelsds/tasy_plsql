create or replace
procedure ALTERAR_PORTADOR_CHEQUE_CR(nr_seq_cheque_p	number,
				cd_tipo_portador_p	number,
				cd_portador_p		number,
				nm_usuario_p		varchar2,
				ie_commit_p		varchar2,
				ie_opcao_p		varchar2) is

/*
'I'	- Insere portador;
'E'	- Retira(exclui) portador
*/				
				
cd_tipo_portador_ant_w	number(5);
cd_portador_ant_w	number(5);
ie_cheque_cobranca_w	portador.ie_cheque_cobranca%type;

begin

begin
cd_tipo_portador_ant_w	:= to_number(obter_portador_cheque_data(nr_seq_cheque_p,sysdate,'CT'));
cd_portador_ant_w	:= to_number(obter_portador_cheque_data(nr_seq_cheque_p,sysdate,'CP'));
exception when others then
	cd_tipo_portador_ant_w	:= null;
	cd_portador_ant_w	:= null;
end;

insert into cheque_cr_alt_portador
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	nr_seq_cheque,
	dt_alteracao,
	cd_portador_ant,
	cd_tipo_portador_ant,
	cd_portador,
	cd_tipo_portador,
	ds_observacao)
values	(cheque_cr_alt_portador_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nr_seq_cheque_p,
	sysdate,
	cd_portador_ant_w,
	cd_tipo_portador_ant_w,
	cd_portador_p,
	cd_tipo_portador_p,
	wheb_mensagem_pck.get_texto(305651));
	
if	(nvl(ie_opcao_p,'I')	= 'I') then
	update	cheque_cr
	set	dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		cd_tipo_portador 	= cd_tipo_portador_p,
		cd_portador		= cd_portador_p
	where	nr_seq_cheque		= nr_seq_cheque_p;
	
	select	nvl(max(a.ie_cheque_cobranca),'N')
	into	ie_cheque_cobranca_w
	from	portador a
	where	a.cd_portador = cd_portador_p;
	
	if	(nvl(ie_cheque_cobranca_w,'N') = 'S') then
		atualizar_cobranca_cheque(nr_seq_cheque_p,nm_usuario_p);
	end if;
	
elsif	(nvl(ie_opcao_p,'I') = 'E') then
	update	cheque_cr
	set	dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		cd_tipo_portador	= null,
		cd_portador		= null
	where	nr_seq_cheque		= nr_seq_cheque_p;
end if;

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end;
/