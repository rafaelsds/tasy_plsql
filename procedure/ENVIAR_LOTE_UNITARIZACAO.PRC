create or replace
procedure enviar_lote_unitarizacao(
			nr_seq_lote_fornec_p	number,
			cd_unidade_medida_p	varchar2,
			qt_material_p		number,
			qt_unitarizacao_p		number,
			qt_manual_p		number,
			cd_pessoa_envio_p	varchar2,
			cd_pessoa_transporte_p	varchar2,
			dt_envio_p		date,
			dt_transporte_p		date,
			cd_local_estoque_p	number,
			nm_usuario_p		varchar2,
			ie_urgente_p		varchar2,
			nr_seq_motivo_unit_p	number,
			ie_gera_recebimento_p	varchar2,
			cd_local_destino_p	number,
			nr_sequencia_p	out	number) is

nr_sequencia_w		number(10);
ie_estoque_lote_w	varchar2(1);
qt_estoque_lote_w	number(15,4);
ie_permite_unitarizar_w	varchar2(1);
cd_local_destino_w	number(4);
dt_recebimento_w	date;
cd_pessoa_recebimento_w	varchar2(10);

cd_estabelecimento_w	number(4) := wheb_usuario_pck.get_cd_estabelecimento;

cd_operacao_estoque_w	number(3);

begin

if	(ie_gera_recebimento_p = 'S') and
	(cd_local_destino_p is not null) then
	dt_recebimento_w := sysdate;
	cd_pessoa_recebimento_w := cd_pessoa_envio_p;
end if;

select	nvl(max(c.ie_estoque_lote),'N')
into	ie_estoque_lote_w
from	material_lote_fornec a,
	material b,
	material_estab c
where	a.cd_material = b.cd_material
and	c.cd_material = b.cd_material_estoque
and	c.cd_estabelecimento = cd_estabelecimento_w
and	a.nr_sequencia = nr_seq_lote_fornec_p;

ie_permite_unitarizar_w := substr(sup_obter_permite_unitarizar(cd_estabelecimento_w,nr_seq_lote_fornec_p,nm_usuario_p),1,1);

if	(ie_permite_unitarizar_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(190860);
elsif	(ie_estoque_lote_w = 'S') then
	begin
	qt_estoque_lote_w := obter_saldo_lote_fornec(nr_seq_lote_fornec_p);
	
	if	(qt_estoque_lote_w < qt_material_p) then
		wheb_mensagem_pck.exibir_mensagem_abort(189897);
	end if;
	end;
end if;

select	solic_unitarizacao_seq.nextval
into	nr_sequencia_w
from	dual;

nr_sequencia_p	:= nr_sequencia_w;

insert into solic_unitarizacao(
	nr_sequencia,
	nr_seq_lote_fornec,
	dt_envio,
	cd_pessoa_envio,
	cd_pessoa_transporte,
	dt_transporte,
	qt_material,
	qt_unitarizar,
	qt_manual,
	cd_unidade_medida,
	dt_atualizacao,
	dt_atualizacao_nrec,
	nm_usuario,
	nm_usuario_nrec,
	cd_local_estoque_origem,
	cd_local_estoque_destino,
	ie_urgente,
	nr_seq_motivo_unit,
	dt_recebimento,
	cd_pessoa_recebimento) 
values	(nr_sequencia_w,
	nr_seq_lote_fornec_p,
	dt_envio_p,
	cd_pessoa_envio_p,
	cd_pessoa_transporte_p,
	dt_transporte_p,
	qt_material_p,
	qt_unitarizacao_p,
	qt_manual_p,
	cd_unidade_medida_p,
	sysdate,
	sysdate,
	nm_usuario_p,
	nm_usuario_p,
	cd_local_estoque_p,
	cd_local_destino_p,
	ie_urgente_p,
	nr_seq_motivo_unit_p,
	dt_recebimento_w,
	cd_pessoa_recebimento_w);
	
if	(cd_local_estoque_p <> nvl(cd_local_destino_p,'0')) then
	if 	(ie_gera_recebimento_p <> 'S') then	
		cd_operacao_estoque_w := sup_obter_operacao_unit('ST');
		gerar_movto_estoque_trans_unit(nr_sequencia_w, cd_operacao_estoque_w, '1', 'O', nm_usuario_p);
	else
		gerar_movto_estoque_trans_unit(nr_sequencia_w, cd_operacao_estoque_w, '1', 'TE', nm_usuario_p);
	end if;
end if;

commit;

end enviar_lote_unitarizacao;
/