create or replace
procedure HOSP_LANCAMENTOS_CONTABEIS(	dt_inicial_p	date,
					dt_final_p	date,
					cd_banco_p	number,
					nm_usuario_p	varchar2) is

ds_conteudo_w		varchar2(332);
nr_sequencia_w		number(10);
dt_transacao_w		varchar2(5);
cd_conta_debito_w	varchar2(20);
cd_conta_credito_w	varchar2(20);
vl_transacao_w		number(15,2);
ds_complemento_w	varchar2(200);

cursor	c01 is
select	to_char(a.dt_transacao,'dd/mm') dt_transacao,
	decode(c.ie_banco,'D',a.cd_conta_contabil,null) cd_conta_debito,
	decode(c.ie_banco,'C',a.cd_conta_contabil,null) cd_conta_credito,
	nvl(a.vl_transacao,0) vl_transacao,
	c.ds_transacao || ' (' || c.cd_transacao || ')' ds_complemento
from	transacao_financeira c,
	banco_estabelecimento b,
	movto_trans_financ a
where	a.nr_seq_trans_financ	= c.nr_sequencia
and	b.cd_banco		= nvl(cd_banco_p,b.cd_banco)
and	a.nr_seq_banco		= b.nr_sequencia
and	a.dt_transacao		between dt_inicial_p and fim_dia(dt_final_p);

begin

delete	from w_interf_sefip;

open	c01;
loop
fetch	c01 into
	dt_transacao_w,
	cd_conta_debito_w,
	cd_conta_credito_w,
	vl_transacao_w,
	ds_complemento_w;
exit	when c01%notfound;

	select	w_interf_sefip_seq.nextval
	into	nr_sequencia_w
	from	dual;

	ds_conteudo_w	:=	lpad(nvl(nr_sequencia_w,0),7,'0') ||
				dt_transacao_w ||
				lpad(nvl(cd_conta_debito_w,'0'),7,'0') ||
				lpad(nvl(cd_conta_credito_w,'0'),7,'0') ||
				lpad(to_char(vl_transacao_w,'00000000000009.99'),17,'0') ||
				'  999' ||
				rpad(ds_complemento_w,200,' ') ||
				rpad(' ',42,' ') ||
				rpad(' ',42,' ');

	insert	into w_interf_sefip
		(ds_conteudo,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_sequencia)
	values	(ds_conteudo_w,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_sequencia_w);

end	loop;
close	c01;

commit;

end	HOSP_LANCAMENTOS_CONTABEIS;
/