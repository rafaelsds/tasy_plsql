create or replace
procedure Gerar_diag_atend_de_diag_tumor	(nr_seq_diag_tumor_p	number,
						nr_atendimento_p	number,
						ie_liberar_p		varchar2,
						nm_usuario_p			Varchar2,
            nr_seq_atend_cons_pepa_p number default null) is

cd_doenca_w			varchar2(10);
ds_diagnostico_w	varchar2(255);
dt_diagnostico_w	date;
cd_medico_w			varchar2(10);
ie_tipo_diagnostico_w		number(10) := null;

begin

if	(nr_atendimento_p is not null) then	
     obter_param_usuario(281, 574, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_tipo_diagnostico_w);
	
	
	select	nvl(ie_tipo_diagnostico_w,ie_tipo_diagnostico),
		cd_doenca_cid,
		ds_observacao,
		cd_medico
	into	ie_tipo_diagnostico_w,
		cd_doenca_w,
		ds_diagnostico_w,
		cd_medico_w
	from	can_loco_regional
	where	nr_sequencia = nr_seq_diag_tumor_p;

	Obter_Dados_Diagnostico(nr_atendimento_p,cd_medico_w,nm_usuario_p,dt_diagnostico_w);


	if	(ie_tipo_diagnostico_w	is null) then
		ie_tipo_diagnostico_w	:= 2;
	end if;

	insert into diagnostico_doenca(
		nr_atendimento,
		cd_doenca,
		ie_classificacao_doenca,
		ds_diagnostico,
		dt_atualizacao,
		dt_diagnostico,	
		nm_usuario,
		dt_liberacao,
		ie_tipo_diagnostico,
		ie_situacao,
		nr_seq_loco_reg,
    nr_seq_atend_cons_pepa)
	values(
		nr_atendimento_p,
		cd_doenca_w,
		'P',
		ds_diagnostico_w,
		sysdate,
		dt_diagnostico_w,
		nm_usuario_p,
		decode(ie_liberar_p,'L',sysdate),
		ie_tipo_diagnostico_w,
		'A',
		nr_seq_diag_tumor_p,
    nr_seq_atend_cons_pepa_p);

	commit;

end if;

end Gerar_diag_atend_de_diag_tumor;
/