create or replace
procedure tasylab_limpar_prescr_proc_mat (	nr_prescricao_p		number,
											nr_seq_externo_p	number,											
											cd_erro_p			out number,
											ds_erro_p			out varchar2) is 
	
ie_existe_w		varchar2(1);
	
Cursor C01 is	
	select	distinct
			b.nr_sequencia,
			a.nr_sequencia nr_seq_prescr_mat
	from	prescr_proc_material a,
			prescr_proc_mat_item b,
			prescr_procedimento c
	where	a.nr_sequencia = b.nr_seq_prescr_proc_mat(+)
	and		a.nr_prescricao = b.nr_prescricao(+)
	and		a.nr_prescricao = nr_prescricao_p
	and		c.nr_prescricao = a.nr_prescricao
	and		c.nr_sequencia = b.nr_seq_prescr
	and		c.ie_status_atend <= 30; /*tratamento para n�o excluir item caso j� esteja no status digita��o ou posterior*/
	
c01_w	c01%rowtype;
	
begin

cd_erro_p	:= 0;

tasy_atualizar_dados_sessao(nr_seq_externo_p);

begin
open c01;
loop
fetch c01 into c01_w;
exit when c01%notfound;
	begin
	
	delete 	prescr_proc_mat_item
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia = c01_w.nr_sequencia;	
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_w
	from	prescr_proc_material a
	where	a.nr_sequencia = c01_w.nr_seq_prescr_mat;
	
	if	(ie_existe_w = 'N') then
		delete 	prescr_proc_material
		where	nr_sequencia = c01_w.nr_seq_prescr_mat/*
		and		nm_usuario = 'TasyLab-LibPre'*/;
	end if;
	
	end;
end loop;
close c01;

exception
when others then
	cd_erro_p	:= 1;
	ds_erro_p	:= substr('Erro ao limpas as barras do procedimento '||sqlerrm,1,2000);
end;
--OS727249 - Ivan
--commit;

end tasylab_limpar_prescr_proc_mat;
/
