create or replace procedure gerar_d301_segmento_rbg(nr_seq_dataset_p number,
                                    								nm_usuario_p		 varchar2) is 

  cursor c01 is
    select 
      null nr_seq_301_medida_reab,
      0 nr_seq_301_sugest_trat,
      null nr_seq_301_sugest_instit,
      nr_seq_dataset_p nr_seq_dataset,
      null cd_ik_hosp_sugestao
    from dual;

  c01_w c01%rowtype;
  
begin
  open c01;
  fetch c01 into c01_w;
  close c01;
  
  insert into d301_segmento_rbg(nr_sequencia,
                                dt_atualizacao,
                                nm_usuario,
                                dt_atualizacao_nrec,
                                nm_usuario_nrec,
                                nr_seq_301_medida_reab,
                                nr_seq_301_sugest_trat,
                                nr_seq_301_sugest_instit,
                                nr_seq_dataset,
                                cd_ik_hosp_sugestao) values (d301_segmento_rbg_seq.nextval,
                                                             sysdate,
                                                             nm_usuario_p,
                                                             sysdate,
                                                             nm_usuario_p,
                                                             c01_w.nr_seq_301_medida_reab,
                                                             c01_w.nr_seq_301_sugest_trat,
                                                             c01_w.nr_seq_301_sugest_instit,
                                                             c01_w.nr_seq_dataset,
                                                             c01_w.cd_ik_hosp_sugestao);

end gerar_d301_segmento_rbg;
/