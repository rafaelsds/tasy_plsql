create or replace procedure create_pie_update_message( nr_message_p number,
							nm_usuario_p varchar2,
							cd_estabelecimento_p number,
							nr_seq_idioma_p number default 9)
is

 nr_seq_pie_rule_w		pie_message.nr_seq_pie_rule%type;
 nr_sequencia_w			pie_ret_rule_action.nr_sequencia%type;
 tp_action_w			pie_ret_rule_action.tp_action%type;
 nr_sequencia_ww		pie_practitioner.nr_sequencia%type;
 nr_profession_w		pie_practitioner.nr_profession%type;
 dt_regist_expiry_w		pie_pract_registration.dt_regist_expiry%type;
 nr_sequencia_message_new_w	pie_message.nr_sequencia%type;
 nr_seq_message_old_w		pie_message.nr_sequencia%type;
 message_w 			varchar2(4000):=null;
 final_message_w		varchar2(4000):=null;
 nm_pessoa_fisica_w		pessoa_fisica.nm_pessoa_fisica%type;
 cd_perfil_dest_w		pie_rule_action_dest.cd_perfil_dest%type;
 si_internal_com_w		pie_rule_action_dest.si_internal_com%type;
 si_mail_w			pie_rule_action_dest.si_email%type; 
 practitioner_row_changed_w	number(2,0):=0;
 nr_seq_pie_pract_new_w		pie_practitioner.nr_sequencia%type;
 nr_seq_pie_pract_old_w		pie_practitioner.nr_sequencia%type;
 ds_email_origem_w		varchar2(100);
 ds_email_dest_w		varchar2(100);
 nm_usuario_dest_w		pie_rule_action_dest.nm_usuario_dest%type;
 
 cursor c01
 is
 select nr_sequencia, tp_action
 from pie_ret_rule_action
 where nr_seq_pie_rule =nr_seq_pie_rule_w;

 cursor c02
 is
 select nr_sequencia, nr_profession
 from pie_practitioner
 where nr_seq_pie_message =nr_message_p;

 cursor c03
 is
 select max(dt_regist_expiry)
 from pie_pract_registration
 where nr_seq_pie_pract =nr_sequencia_ww;
 
 cursor c04
 is
 select cd_perfil_dest, nm_usuario_dest, si_internal_com, si_email
 from pie_rule_action_dest
 where nr_seq_pie_rule_action = nr_sequencia_w;

begin
	select nr_seq_pie_rule
	into nr_seq_pie_rule_w
	from pie_message
	where nr_sequencia=nr_message_p;    
	
	if(nr_seq_pie_rule_w is not null) then

		open c01;
		loop
		fetch c01 into nr_sequencia_w, tp_action_w;
		exit when c01%notfound;
		begin

			if( tp_action_w='U' ) then

				open c02;
				loop
				fetch c02 into nr_sequencia_ww, nr_profession_w;
				exit when c02%notfound;
					begin
						
						open c03;
						loop
						fetch c03 into dt_regist_expiry_w;
						exit when c03%notfound;
							update pessoa_fisica
							set dt_validade_conselho = dt_regist_expiry_w
							where ds_codigo_prof= nr_profession_w;
						end loop;
						close c03;
						
					end;
				end loop;
				close c02;
				
			elsif ( tp_action_w = 'SDC' ) then
				begin

					open c02;
					loop
					fetch c02 into nr_sequencia_message_new_w, nr_profession_w;
					exit when c02%notfound;

						select max(nr_seq_message)
						into nr_seq_message_old_w
						from pie_message_request
						where nr_professional_requested = nr_profession_w
						and nr_seq_message not in (
						select max(nr_seq_message)
						from pie_message_request
						where nr_professional_requested = nr_profession_w);

						
						if ( nr_seq_message_old_w is not null ) then
							/* in below piece of code, i am checking whether any field(s) changed from last request for this practitioner. 
							repeat the process for all the pie related tables.
							if yes, then append the tab name.*/
							select max(nr_sequencia)
							into  nr_seq_pie_pract_new_w
							from pie_practitioner
							where nr_profession= nr_profession_w
							and nr_seq_pie_message= nr_message_p;
							
							select max(nr_sequencia)
							into  nr_seq_pie_pract_old_w
							from pie_practitioner
							where nr_profession= nr_profession_w
							and nr_seq_pie_message=nr_seq_message_old_w;

							select count(*)
							into practitioner_row_changed_w
							from(
							select dt_name_edit,ds_name_family,ds_name_given, ds_name_middle, ds_name_title , tp_gender, tp_profession, dt_profession_start
							from pie_practitioner
							where nr_seq_pie_message = nr_message_p
							minus
							select dt_name_edit,ds_name_family,ds_name_given, ds_name_middle, ds_name_title , tp_gender, tp_profession, dt_profession_start 
							from pie_practitioner
							where nr_seq_pie_message = nr_seq_message_old_w
							);
							
							select max(nm_pessoa_fisica)
							into nm_pessoa_fisica_w
							from pessoa_fisica
							WHERE ds_codigo_prof=nr_profession_w;

							message_w:= message_w|| chr(13) || chr(10)|| nm_pessoa_fisica_w ||' : ';
							
							if( practitioner_row_changed_w > 0 ) then
								message_w := message_w || obter_desc_exp_idioma(944354,nr_seq_idioma_p)||', ';
							end if;
							
							if ( is_information_changed('pie_pract_address', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(289232,nr_seq_idioma_p)||', ';
							end if;
							
							if( is_information_changed('pie_pract_caution', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(951520,nr_seq_idioma_p)||', ';
							end if;
							
							if( is_information_changed('pie_pract_condition', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(285645,nr_seq_idioma_p)||', ';
							end if;
							
							if( is_information_changed('pie_pract_other_language', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(291654,nr_seq_idioma_p)||', ';
							end if;
							
							if( is_information_changed('pie_pract_qualification', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(488909,nr_seq_idioma_p)||', ';
							end if;
							
							if( is_information_changed('pie_pract_registration', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(297460,nr_seq_idioma_p)||', ';
							end if;
							
							if( is_information_changed('pie_pract_reprimand', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(951510,nr_seq_idioma_p)||', ';
							end if;
							
							if( is_information_changed('pie_pract_undertaking', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(951531,nr_seq_idioma_p)||', ';
							end if;
							
							if( is_information_changed('pie_pract_reg_endorsement', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(953595,nr_seq_idioma_p)||', ';
							end if;
							
							if( is_information_changed('pie_pract_reg_notation', nr_seq_pie_pract_new_w, nr_seq_pie_pract_old_w) > 0) then
								message_w := message_w || obter_desc_exp_idioma(953597,nr_seq_idioma_p)||', ';
							end if;

							if ( message_w is not null) then
								message_w:= substr(message_w,1,length(message_w)-2) || chr(13) || chr(10);
							end if;
			
						end if;
					end loop;
					close c02;
					if( message_w is not null) then
						final_message_w:= obter_desc_exp_idioma(952922,nr_seq_idioma_p)||chr(13)||chr(10)||  message_w;

						open c04;
						loop
						fetch c04 into cd_perfil_dest_w, nm_usuario_dest_w, si_internal_com_w, si_mail_w ;
						exit when c04%notfound;
							if(si_internal_com_w='S')then
								
							Gerar_Comunic_Padrao(sysdate,
										obter_desc_exp_idioma(952924,nr_seq_idioma_p),
										final_message_w,
										nm_usuario_p,
										'N',
										nm_usuario_dest_w,
										'N',
										null,--classif
										null,
										cd_estabelecimento_p,
										null,
										sysdate,
										null,
										null);
							end if;
							
							if (si_mail_w ='S') then

								select	max(ds_email)
								into	ds_email_origem_w
								from	usuario
								where	nm_usuario = nm_usuario_p;
								
								if	(ds_email_origem_w is null) then
									wheb_mensagem_pck.exibir_mensagem_abort(189137,'nm_usuario='||nm_usuario_p);
								end if;

								select	substr(obter_dados_pf_pj('',cd_cgc,'M'),1,255)
								into	ds_email_dest_w
								from	estabelecimento
								where	cd_estabelecimento = cd_estabelecimento_p;

								if	(ds_email_dest_w is null) then
									select	substr(obter_dados_pf_pj_estab(cd_estabelecimento_p, '', cd_cgc, 'M'),1,255)
									into	ds_email_dest_w
									from	estabelecimento
									where	cd_estabelecimento = cd_estabelecimento_p;
									if	(ds_email_origem_w is null) then
										wheb_mensagem_pck.exibir_mensagem_abort(189136);
									end if;
								end if;

								enviar_email( obter_desc_exp_idioma(952924,nr_seq_idioma_p), final_message_w, ds_email_origem_w,ds_email_dest_w, nm_usuario_p, 'A');
							end if;
						end loop;
						close c04;
					end if;
				end;
				
			end if;
		end;
		end loop;
		close c01;
	end if;  
end create_pie_update_message; 
/