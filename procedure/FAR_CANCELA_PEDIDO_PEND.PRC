create or replace
procedure	far_cancela_pedido_pend(
			cd_estabelecimento_p		number) is

begin

update	far_pedido
set	dt_cancelamento	= sysdate,
	ie_motivo_baixa	= 'B'
where	cd_estabelecimento = cd_estabelecimento_p
and	ie_classificacao = 'P'
and	dt_fechamento is null
and	trunc(dt_pedido) < trunc(sysdate)
and	dt_cancelamento is null;

commit;

end far_cancela_pedido_pend;
/