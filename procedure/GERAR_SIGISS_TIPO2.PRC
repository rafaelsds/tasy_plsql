create or replace
procedure gerar_sigiss_tipo2 (	dt_inicio_p		date,
				dt_fim_p		date,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

ie_tp_registro_w		number(1);
ie_identificador_sistema_w	varchar2(12);
ie_tipo_codificacao_w		varchar2(1);
cd_item_servico_w		varchar2(7);
ie_situacao_w			varchar2(1);
vl_total_nota_w			varchar2(15);
vl_bc_iss_w			varchar2(15);
cd_cgc_cpf_tomador_w		varchar2(15);
nr_incricao_municipal_tom_w	varchar2(8);
nr_incricao_estadual_tom_w	varchar2(8);
ds_tomador_w			varchar2(100);
ds_rua_tomador_w		varchar2(50);
nr_endereco_tomador_w		varchar2(10);
ds_complemento_tomador_w	varchar2(30);
ds_bairro_tomador_w		varchar2(30);
ds_municipio_tomador_w		varchar2(50);
ds_uf_tomador_w			varchar2(2);
ds_cep_tomador_w		varchar2(8);
ds_email_tomador_w		varchar2(100);
cd_estabelecimento_w		number(4);
ds_servicos_w			varchar(1000);
ds_arquivo_ww			varchar2(3000);

cursor	c01 is
select '2' 							cd_registro,
       lpad(' ','12',' ') 						ie_identificador_sistema,
       1 ie_tipo_codificacao,
       lpad(nvl(substr(decode(i.cd_material,null,obter_dados_grupo_servico_item(obter_item_servico_proced(obter_procedimento_nfse(n.nr_sequencia,'P'),obter_procedimento_nfse(n.nr_sequencia,'O')),'CD'),i.cd_material),1,20),' '),7,' ') cd_item_servico,
       	decode( n.ie_situacao,1, 
		decode(nvl((	select	count(*) 
				from	nota_fiscal_trib a, 
					tributo c 
				where	a.cd_tributo 	 = c.cd_tributo 
				and	c.ie_tipo_tributo	= 'ISS' 
				and 	a.nr_sequencia  	= n.nr_sequencia),0),0,'I','T'),
		3, decode(n.ie_status_envio,null, 
				decode(nvl((	select	count(*) 
					from	nota_fiscal_trib a, 
						tributo c 
					where	a.cd_tributo 	 = c.cd_tributo 
					and	c.ie_tipo_tributo	= 'ISS' 
					and 	a.nr_sequencia  	= n.nr_sequencia),0),0,'I','T'),'C'),'C') ie_situacao,
	lpad(nvl(elimina_caractere_especial(campo_mascara_virgula(n.vl_total_nota)),' '),'15','0') vl_total_nota,
	lpad(nvl(elimina_caractere_especial(campo_mascara_virgula(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS'))),'0'),'15','0') vl_bc_iss,
	lpad(nvl(decode(n.cd_pessoa_fisica,null,n.cd_cgc,decode(obter_se_cpf_valido(obter_dados_pf(n.cd_pessoa_fisica,'CPF')),'S',obter_dados_pf(n.cd_pessoa_fisica,'CPF'),'PFNI')),'0'),'15',' ') cd_cgc_cpf_tomador,
	lpad(nvl(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'IM'),' '),'8',' ') nr_incricao_municipal_tom,
	lpad(nvl(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'IE'),' '),'8',' ') nr_incricao_estadual_tom,
	rpad(nvl(substr(decode(cd_pessoa_fisica,null,obter_dados_pf_pj(null,cd_cgc,'N'),obter_nome_pessoa_fisica(cd_pessoa_fisica,null)),1,100),' '),'100',' ') ds_tomador,
	rpad(nvl(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'R'),1,50),' '),'50',' ') ds_rua_tomador,
	lpad(nvl(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'NR'),1,10),' '),'10',' ') nr_endereco_tomador,
	rpad(nvl(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CO'),1,30),' '),'30',' ') ds_complemento_tomador,
	rpad(nvl(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'B'),1,30),' '),'30',' ') ds_bairro_tomador,
	rpad(nvl(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CI'),1,50),' '),'50',' ') ds_municipio_tomador,
	rpad(nvl(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'UF'),1,2),' '),'2',' ') ds_uf_tomador,
	lpad(nvl(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'CEP'),1,8),' '),'8',' ') ds_cep_tomador,
	rpad(nvl(substr(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc, 'M'),1,100),' '),'100',' ') ds_email_tomador,
	nvl(substr(replace(obter_descricao_rps(n.cd_estabelecimento, n.nr_sequencia, 'DS_SERVICOS'),CHR(13),' '),1,1000),' ') ds_servicos,
	n.cd_estabelecimento			cd_estabelecimento
from    nota_fiscal n,
	nota_fiscal_item i,
	operacao_nota o
where   i.nr_sequencia = n.nr_sequencia
and	o.cd_operacao_nf = n.cd_operacao_nf
and	o.ie_servico = 'S'
and	n.cd_estabelecimento = cd_estabelecimento_p
and	n.dt_emissao between dt_inicio_p and fim_dia(dt_fim_p);
begin

open c01;
loop
fetch c01 into
	ie_tp_registro_w,
	ie_identificador_sistema_w,
	ie_tipo_codificacao_w,
	cd_item_servico_w,
	ie_situacao_w,
	vl_total_nota_w,
	vl_bc_iss_w,
	cd_cgc_cpf_tomador_w,
	nr_incricao_municipal_tom_w,
	nr_incricao_estadual_tom_w,
	ds_tomador_w,
	ds_rua_tomador_w,
	nr_endereco_tomador_w,
	ds_complemento_tomador_w,
	ds_bairro_tomador_w,
	ds_municipio_tomador_w,
	ds_uf_tomador_w,
	ds_cep_tomador_w,
	ds_email_tomador_w,
	ds_servicos_w,
	cd_estabelecimento_w;
exit when c01%notfound;

	begin	
	
	ds_arquivo_ww	:=     	ie_tp_registro_w		||ie_identificador_sistema_w	||
				ie_tipo_codificacao_w		||cd_item_servico_w		||
				ie_situacao_w			||vl_total_nota_w		||
				vl_bc_iss_w			||cd_cgc_cpf_tomador_w		||
				nr_incricao_municipal_tom_w	||nr_incricao_estadual_tom_w	||
				ds_tomador_w			||ds_rua_tomador_w		||
				nr_endereco_tomador_w		||ds_complemento_tomador_w	||
				ds_bairro_tomador_w		||ds_municipio_tomador_w	||
				ds_uf_tomador_w			||ds_cep_tomador_w		||
				ds_email_tomador_w		||ds_servicos_w;
				
insert into w_inss_direp_arquivo(nr_sequencia,
				 cd_estabelecimento,
				 dt_atualizacao,
				 nm_usuario,
				 dt_atualizacao_nrec,
				 nm_usuario_nrec,
				 ds_arquivo_w)
			values	(w_inss_direp_arquivo_seq.nextval,
				 cd_estabelecimento_w,
				 sysdate,
				 nm_usuario_p,
				 sysdate,
				 nm_usuario_p,
				 ds_arquivo_ww );

	end;

end loop;
close c01;

commit;

end gerar_sigiss_tipo2;
/