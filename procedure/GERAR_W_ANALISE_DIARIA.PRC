create or replace
procedure Gerar_W_Analise_Diaria(       dt_analise_p            date,
                                        nm_usuario_p            varchar2) is

nm_usuario_grupo_w              varchar2(15);
nr_seq_grupo_w                  number(10);
cd_pessoa_fisica_w              varchar2(10);
qt_inicio_dia_w                 number(15);
qt_recebida_w                   number(15);
qt_recebida_ant_w               number(15);
qt_receb_usuario_w				number(15);
nm_usuario_w					varchar2(15);
qt_final_dia_w                  number(15);
nr_sequencia_w                  number(10);
nr_seq_gerencia_w               number(10);
qt_total_executado_w		number(10);
dt_analise_hora_ant_w   date;
dt_analise_hora_w               date;
dt_analise_dia_w                date;

Cursor C01 is
        select  nm_usuario_grupo,
                Obter_Pessoa_Fisica_Usuario(nm_usuario_grupo,'C'),
                nr_seq_grupo
        from    usuario_grupo_des;

Cursor C02 is
        select  nr_sequencia,
                nr_seq_gerencia
        from    grupo_desenvolvimento
        where   ie_situacao     = 'A';
		
Cursor C03 is
		select  count(distinct a.nr_sequencia),
						substr(OBTER_NOME_USUARIO_EXEC_des(a.nr_sequencia, 'X'),1,30) nm_usuario
				from    man_ordem_servico a
				where   nr_seq_grupo_des	= nr_seq_grupo_w
				and		(select min(b.dt_atualizacao)
						from   	man_estagio_processo c,
								man_ordem_serv_estagio b
						where   a.nr_sequencia          = b.nr_seq_ordem
						and     b.nr_seq_estagio        = c.nr_sequencia
						and     c.ie_desenv             = 'S') between
					dt_analise_hora_w - 3/24 and  dt_analise_hora_w;
						
begin

dt_analise_hora_w               := trunc(dt_analise_p, 'hh24');
dt_analise_dia_w                := trunc(dt_analise_p);

delete from w_analise_diaria
where   dt_analise = dt_analise_dia_w;

delete from w_analise_diaria_hora
where   dt_analise = dt_analise_hora_w;

select  max(dt_analise)
into    dt_analise_hora_ant_w
from    w_analise_diaria_hora
where   dt_analise < dt_analise_hora_w
and             dt_analise between trunc(dt_analise_dia_w) and fim_dia(dt_analise_dia_w);

commit;

open C01;
loop
fetch C01 into
        nm_usuario_grupo_w,
        cd_pessoa_fisica_w,
        nr_seq_grupo_w;
exit when C01%notfound;
        begin

        select  nr_seq_gerencia
        into    nr_seq_gerencia_w
        from    grupo_desenvolvimento
        where   nr_sequencia    = nr_seq_grupo_w;

        select  nvl(max(qt_final_dia),0)
        into    qt_inicio_dia_w
        from    w_analise_diaria
        where   dt_analise              = dt_analise_dia_w - 1
        and     cd_pessoa_fisica        = cd_pessoa_fisica_w;

        select  count(*)
        into    qt_recebida_w
        from    man_ordem_servico_exec a
        where   a.dt_recebimento between trunc(dt_analise_dia_w) and fim_dia(dt_analise_dia_w)
        and     a.nm_usuario_exec       = nm_usuario_grupo_w
        and exists (    select  1
                        from    man_estagio_processo c,
                                man_ordem_serv_estagio b
                        where   a.nr_seq_ordem          = b.nr_seq_ordem
                        and     b.nr_seq_estagio        = c.nr_sequencia
                        and     c.ie_desenv             = 'S');

        select  count(*)
        into    qt_final_dia_w
        from    man_ordem_servico_exec b,
           man_estagio_processo c,
                man_ordem_servico a
        where   a.nr_seq_estagio        = c.nr_sequencia
        and     c.ie_desenv             = 'S'
		and     b.nr_seq_ordem          = a.nr_sequencia
		and     b.nm_usuario_exec       = nm_usuario_grupo_w
		and     b.nm_usuario_exec       =
				(select  max(x.nm_usuario_exec)
                 from    usuario_grupo_des y,
						 man_ordem_servico_exec x
                        where   x.nr_seq_ordem          = a.nr_sequencia
				and	x.nm_usuario_exec	= y.nm_usuario_grupo); 
				
				
		/*select	count(*)
		into    qt_final_dia_w
		from    man_ordem_servico_v a
		where   a.nr_seq_meta_pe is null
		and     a.nr_seq_obj_bsc is null
		and     (a.NR_GRUPO_TRABALHO  in (  select   a.nr_sequencia
                              from     man_grupo_trabalho a,
                                       man_grupo_trab_usuario b
                              where    a.nr_sequencia = b.nr_seq_grupo_trab
                              and      b.nm_usuario_param = nm_usuario_grupo_w))
		and     a.ie_status_ordem in ('1','2')
		and     a.nm_usuario_exec_prev = nm_usuario_grupo_w
		and     a.nr_seq_estagio in (select   nr_seq_estagio
                              from     man_estagio_usuario
                              where    nm_usuario_acao = nm_usuario_grupo_w)
		and     a.nr_seq_grupo_des = nr_seq_grupo_w;*/
				

        select  w_analise_diaria_seq.nextval
        into    nr_sequencia_w
        from    dual;

        insert into w_analise_diaria(
                nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                nr_seq_gerencia,
                nr_seq_grupo_des,
                cd_pessoa_fisica,
                dt_analise,
                qt_inicio_dia,
                qt_recebida,
                qt_final_dia)
        values( nr_sequencia_w,
                sysdate,
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                nr_seq_gerencia_w,
                nr_seq_grupo_w,
                cd_pessoa_fisica_w,
                dt_analise_dia_w,
                qt_inicio_dia_w,
                qt_recebida_w,
                qt_final_dia_w);

        if      (to_char(dt_analise_hora_w, 'hh24') in ('00', '09','12','15','18'))  then
                begin
				
				qt_recebida_w	:= 0;
				
				/*open C03;
				loop
				fetch C03 into	
						qt_receb_usuario_w,
						nm_usuario_w;
				exit when C01%notfound;
					begin
					
					if	(nm_usuario_w	= nm_usuario_grupo_w) then
						qt_recebida_w	:= qt_recebida_w + qt_receb_usuario_w;
					end if;
					end;
				end loop;
				close C03;
			*/
				if	(to_char(dt_analise_hora_w, 'hh24') in ('09'))  then
					select  count(*)
					into    qt_recebida_w
					from    man_ordem_servico_exec a
					where   a.dt_recebimento between trunc(dt_analise_hora_w, 'dd') and dt_analise_hora_w
					and     a.nm_usuario_exec       = nm_usuario_grupo_w
					and exists (    select  1
	                        from    man_estagio_processo c,
	                                man_ordem_serv_estagio b
	                        where   a.nr_seq_ordem          = b.nr_seq_ordem
	                        and     b.nr_seq_estagio        = c.nr_sequencia
	                        and     c.ie_desenv             = 'S'); 
				else
					select  count(*)
					into    qt_recebida_w
					from    man_ordem_servico_exec a
					where   a.dt_recebimento between dt_analise_hora_w - 3/24 and dt_analise_hora_w
					and     a.nm_usuario_exec       = nm_usuario_grupo_w
					and exists (    select  1
	                        from    man_estagio_processo c,
	                                man_ordem_serv_estagio b
	                        where   a.nr_seq_ordem          = b.nr_seq_ordem
	                        and     b.nr_seq_estagio        = c.nr_sequencia
	                        and     c.ie_desenv             = 'S'); 
				end if;
				
				select 	count(distinct b.nr_seq_ordem)
				into	qt_total_executado_w
				from 	man_estagio_processo d,
						usuario_grupo_des c,
						man_ordem_servico a,
						man_ordem_serv_estagio b
				where 	a.nr_sequencia = b.nr_seq_ordem
				and 	a.nr_seq_grupo_des = nr_seq_grupo_w
				and 	c.nr_seq_grupo = a.nr_seq_grupo_des
				and 	b.nr_seq_estagio = d.nr_sequencia
				and 	((d.ie_suporte = 'S') or (d.nr_sequencia in (2,811,511,1511)))
				and 	c.nm_usuario_grupo = b.nm_usuario
				and 	c.nm_usuario_grupo = nm_usuario_grupo_w
				and 	b.dt_atualizacao between dt_analise_hora_ant_w and dt_analise_hora_w;				
				
				
                select  w_analise_diaria_hora_seq.nextval
                into    nr_sequencia_w
                from    dual;

                insert into w_analise_diaria_hora(
                        nr_sequencia,
                        dt_atualizacao,
                        nm_usuario,
                        dt_atualizacao_nrec,
                        nm_usuario_nrec,
                        nr_seq_gerencia,
                        nr_seq_grupo_des,
                        cd_pessoa_fisica,
                        dt_analise,
                        qt_inicio_dia,
                        qt_recebida,
                        qt_final_dia,
						qt_total_executado)
                values( nr_sequencia_w,
                        sysdate,
                        nm_usuario_p,
                        sysdate,
                        nm_usuario_p,
                        nr_seq_gerencia_w,
                        nr_seq_grupo_w,
                        cd_pessoa_fisica_w,
                        dt_analise_hora_w,
                        qt_inicio_dia_w,
                        qt_recebida_w,
                        qt_final_dia_w,
						qt_total_executado_w);

                end;
        end if;

        end;
end loop;
close C01;

open C02;
loop
fetch C02 into
        nr_seq_grupo_w,
        nr_seq_gerencia_w;
exit when C02%notfound;
        begin
        select  nvl(max(qt_final_dia),0)
        into    qt_inicio_dia_w
        from    w_analise_diaria
        where   dt_analise              = dt_analise_dia_w - 1
        and     cd_pessoa_fisica is null
        and     nr_seq_grupo_des        = nr_seq_grupo_w;

        select  count(*)
        into    qt_recebida_w
        from    man_ordem_servico a
        where   a.nr_seq_grupo_des      = nr_seq_grupo_w
        and     trunc(dt_analise_dia_w)     = (     select  trunc(min(b.dt_atualizacao))
                                                from    man_estagio_processo c,
                                                        man_ordem_serv_estagio b
                                                where   a.nr_sequencia          = b.nr_seq_ordem
                                                and     b.nr_seq_estagio        = c.nr_sequencia
                                                and     c.ie_desenv             = 'S');

	select	sum(qt_ordem)
        into    qt_final_dia_w
	from	(
        select  sum(decode(obter_se_ordem_vinc_projeto(a.nr_sequencia,'1'), 'S', 1, 0) + 
					decode(obter_se_ordem_vinc_projeto(a.nr_sequencia,'2'), 'N', 1, 0)) qt_ordem
        from    grupo_desenvolvimento d,
		man_estagio_processo c,
                man_ordem_servico a
        where   a.nr_seq_estagio        = c.nr_sequencia
        and     c.ie_desenv             = 'S'
	and      a.nr_seq_meta_pe is null
	and      a.nr_seq_obj_bsc is null
	and      a.ie_status_ordem in ('1','2')
        and     a.nr_seq_grupo_des      = nr_seq_grupo_w
	and	a.nr_seq_grupo_des	= d.nr_sequencia
	and	d.nr_seq_gerencia	<> 7
	union
	select  count(*)
        from    grupo_desenvolvimento d,
		man_estagio_processo c,
                man_ordem_servico a
        where   a.nr_seq_estagio        = c.nr_sequencia
        and     c.ie_desenv             = 'S'
	and      a.nr_seq_meta_pe is null
	and      a.nr_seq_obj_bsc is null
	and      a.ie_status_ordem in ('1','2')
        and     a.nr_seq_grupo_des      = nr_seq_grupo_w
	and	a.nr_seq_grupo_des	= d.nr_sequencia
	and	d.nr_seq_gerencia	= 7);

        select  w_analise_diaria_seq.nextval
        into    nr_sequencia_w
        from    dual;

        insert into w_analise_diaria(
                nr_sequencia,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                nr_seq_gerencia,
                nr_seq_grupo_des,
                cd_pessoa_fisica,
                dt_analise,
                qt_inicio_dia,
                qt_recebida,
                qt_final_dia)
        values( nr_sequencia_w,
                sysdate,
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                nr_seq_gerencia_w,
                nr_seq_grupo_w,
                null,
                dt_analise_dia_w,
                qt_inicio_dia_w,
                qt_recebida_w,
                qt_final_dia_w);

        if      (to_char(dt_analise_hora_w, 'hh24') in ('00', '09','12','15','18')) then
                begin

                select  w_analise_diaria_hora_seq.nextval
                into    nr_sequencia_w
                from    dual;

                select  max(qt_recebida)
                into    qt_recebida_ant_w
                from    w_analise_diaria_hora
                where   dt_analise              = dt_analise_hora_ant_w
                and     nr_seq_grupo_des        = nr_seq_grupo_w;


		select 	count(distinct b.nr_seq_ordem)
		into	qt_total_executado_w
		from 	man_estagio_processo d,
     			usuario_grupo_des c,
     			man_ordem_servico a,
     			man_ordem_serv_estagio b
		where 	a.nr_sequencia = b.nr_seq_ordem
		and 	a.nr_seq_grupo_des = nr_seq_grupo_w
		and 	c.nr_seq_grupo = a.nr_seq_grupo_des
		and 	b.nr_seq_estagio = d.nr_sequencia
		and 	((d.ie_suporte = 'S') or (d.nr_sequencia in (2,811,511,1511)))
		and 	c.nm_usuario_grupo = b.nm_usuario
		and 	b.dt_atualizacao between dt_analise_hora_ant_w and dt_analise_hora_w;



                insert into w_analise_diaria_hora(
                        nr_sequencia,
                        dt_atualizacao,
                        nm_usuario,
                        dt_atualizacao_nrec,
                        nm_usuario_nrec,
                        nr_seq_gerencia,
                        nr_seq_grupo_des,
                        cd_pessoa_fisica,
                        dt_analise,
                        qt_inicio_dia,
                        qt_recebida,
                        qt_final_dia,
			qt_total_executado)
                values( nr_sequencia_w,
                        sysdate,
                        nm_usuario_p,
                        sysdate,
                        nm_usuario_p,
                        nr_seq_gerencia_w,
                        nr_seq_grupo_w,
                        null,
                        dt_analise_hora_w,
                        qt_inicio_dia_w,
                        qt_recebida_w - nvl(qt_recebida_ant_w,0),
                        qt_final_dia_w,
			qt_total_executado_w);
                end;
        end if;

        end;
end loop;
close C02;

commit;

end Gerar_W_Analise_Diaria;
/
