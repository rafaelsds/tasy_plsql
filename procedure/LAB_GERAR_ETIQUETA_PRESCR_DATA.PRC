create or replace
procedure LAB_GERAR_ETIQUETA_PRESCR_DATA (dt_fim_prescr_p	date,
					ie_acm_p		varchar2,
					ie_urgencia_p		varchar2,
					qt_exames_p			NUMBER,
					ie_opcao_p			NUMBER,
					nm_usuario_p		varchar2) is


nr_seq_grupo_w		NUMBER(10);
nr_seq_exame_w		NUMBER(10);
qt_exames_w		NUMBER(10);
nr_seq_material_w	NUMBER(15);
nr_etiqueta_w		NUMBER(10);
cd_exame_w		VARCHAR2(20);
qt_exame_w		NUMBER(2);
nr_sequencia_w		NUMBER(10);
nr_seq_lab_w		NUMBER(10);
ds_material_especial_w	VARCHAR(255);
dt_prev_execucao_w	date;
cd_exame_integracao_w	VARCHAR(20);
ds_exame_amostra_w	VARCHAR(20);
nr_seq_origem_w		number(6);
nr_Seq_prescr_hor_w	number(6);
nr_seq_prescr_proc_mat_w number(10);
cd_cgc_externo_w	varchar2(14);
ds_prescricao_w 	varchar2(500);
nr_prescricao_param_w	number(15,0);
nr_seq_prescricao_w	varchar2(500);

cursor c01 is
select	a.nr_prescricao
from	exame_Laboratorio c,
	prescr_Medica b,
	prescr_procedimento a,
	atendimento_paciente d
where	a.nr_prescricao	=  b.nr_prescricao
and	b.nr_atendimento	= d.nr_atendimento
and	a.nr_seq_exame	= c.nr_seq_exame
and	cd_material_exame	is not null
and	ie_status_atend	= 10
and	((substr(decode(nvl(a.ie_acm,'N'),'N', obter_se_intervalo_acm(a.cd_intervalo),a.ie_acm),1,5) = ie_acm_p) or (ie_acm_p = 'N'))
and	((a.ie_urgencia	= ie_urgencia_p) or (ie_urgencia_p = 'N')) 
and	trunc(dt_prev_execucao,'dd')	<= dt_fim_prescr_p
and	a.cd_motivo_baixa	= 0
and	d.IE_TIPO_ATENDIMENTO = 1
and	a.ie_suspenso 	<> 'S'
order by 1;

cursor c02 is
select	a.nr_sequencia
from	exame_Laboratorio c,
	prescr_Medica b,
	prescr_procedimento a,
	atendimento_paciente d
where	a.nr_prescricao	=  b.nr_prescricao
and	b.nr_atendimento	= d.nr_atendimento
and	a.nr_seq_exame	= c.nr_seq_exame
and	cd_material_exame	is not null
and	ie_status_atend	= 10
and	((substr(decode(nvl(a.ie_acm,'N'),'N', obter_se_intervalo_acm(a.cd_intervalo),a.ie_acm),1,5) = ie_acm_p) or (ie_acm_p = 'N'))
and	((a.ie_urgencia	= ie_urgencia_p) or (ie_urgencia_p = 'N')) 
and	trunc(dt_prev_execucao,'dd')	<= dt_fim_prescr_p
and	a.cd_motivo_baixa	= 0
and	d.IE_TIPO_ATENDIMENTO = 1
and	a.ie_suspenso 	<> 'S'
and	a.nr_prescricao = nr_prescricao_param_w
order by 1;

begin

DELETE w_lab_etiqueta
WHERE nm_usuario = nm_usuario_p;

open c01;
loop
fetch c01 into
	nr_prescricao_param_w;
exit when c01%notfound;
	ds_prescricao_w := '';
	open c02;
	loop
	fetch c02 into
		nr_seq_prescricao_w;
	exit when c02%notfound;
		ds_prescricao_w := ds_prescricao_w || ',' || nr_seq_prescricao_w;
	end loop;
	close c02;
	LAB_Gerar_Etiqueta(nr_prescricao_param_w, ds_prescricao_w, qt_exames_p, ie_opcao_p, nm_usuario_p);	
end loop;
close c01;
commit;

end LAB_GERAR_ETIQUETA_PRESCR_DATA;
/
