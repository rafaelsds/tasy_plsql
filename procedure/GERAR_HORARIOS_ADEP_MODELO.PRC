 create or replace
procedure gerar_horarios_adep_modelo (
		cd_estabelecimento_p	number,
		cd_perfil_p		number,
		cd_setor_usuario_p	number,
		nr_atendimento_p	number,
		nr_seq_dialise_p	number,
		qt_hora_anterior_p	number,
		qt_hora_adicional_p	number,
		qt_hora_prescricao_p	number,
		ie_horario_realizado_p	varchar2,
		ie_horario_suspenso_p	varchar2,
		ie_proc_setor_user_p	varchar2,
		ie_interv_setor_user_p	varchar2,
		dt_prescricao_p		date,
		ie_sol_continuas_p	varchar2,
		ie_sol_interrompidas_p	varchar2,
		ie_modelo_p		varchar2,
		ie_aba_p		varchar2,
		ie_palm_p		varchar2,
		nm_usuario_p		varchar2,
		ie_quimio_p		varchar2,
		ie_exibir_suspensos_p	varchar2,
		ie_ios_p		varchar2,
		ie_cpoe_p		varchar2 default 'N',
		ie_html_p  		varchar2 default 'N',
		ie_java_p       	varchar2 default 'N') is

begin
if	(ie_modelo_p = 'W_ADEP_T') then
	begin			
		gerar_horarios_adep_sessao (
			cd_estabelecimento_p,
			cd_setor_usuario_p,
			cd_perfil_p,
			ie_aba_p,
			ie_palm_p,
			nm_usuario_p,
			nr_atendimento_p,
			nr_seq_dialise_p,
			qt_hora_anterior_p,
			qt_hora_adicional_p,
			qt_hora_prescricao_p,
			dt_prescricao_p,
			ie_horario_realizado_p,
			ie_horario_suspenso_p,
			ie_proc_setor_user_p,
			ie_interv_setor_user_p,
			ie_sol_continuas_p,
			ie_sol_interrompidas_p,
			ie_quimio_p,
			ie_exibir_suspensos_p,
			ie_ios_p,
			ie_cpoe_p,
			ie_html_p,
			ie_java_p);
			
	end;
elsif	(ie_aba_p = 'L') then
	begin
	gerar_horarios_adep_lab (
		cd_estabelecimento_p,
		nr_atendimento_p,
		qt_hora_anterior_p,
		qt_hora_adicional_p,
		qt_hora_prescricao_p,
		ie_horario_realizado_p,
		ie_horario_suspenso_p,
		dt_prescricao_p,
		nm_usuario_p);
	end;
else
	begin
	gerar_horarios_adep (
		cd_estabelecimento_p,
		cd_perfil_p,
		cd_setor_usuario_p,
		nr_atendimento_p,
		nr_seq_dialise_p,
		qt_hora_anterior_p,
		qt_hora_adicional_p,
		qt_hora_prescricao_p,
		ie_horario_realizado_p,
		ie_horario_suspenso_p,
		ie_proc_setor_user_p,
		ie_interv_setor_user_p,
		dt_prescricao_p,
		ie_sol_continuas_p,
		nm_usuario_p);
	end;
end if;
end gerar_horarios_adep_modelo;
/
