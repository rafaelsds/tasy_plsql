create or replace
procedure cf_receber_remessa(
			nr_remessa_p		protocolo_documento.nr_sequencia%type,
			nm_usuario_p		protocolo_documento.nm_usuario_envio%type,
			ie_destino_remessa_p	protocolo_documento.ie_destino_remessa%type
			) is

nr_seq_status_fat_w	conta_paciente.nr_seq_status_fat%type;
nr_seq_status_mob_w	conta_paciente.nr_seq_status_mob%type;
nr_interno_conta_w	protocolo_doc_item.nr_seq_interno%type;

cursor	c01 is
	select	nr_seq_interno
	from	protocolo_doc_item
	where	nr_sequencia = nr_remessa_p;

begin

if	(nr_remessa_p is not null) then
	begin

	open C01;
	loop
	fetch C01 into	
		nr_interno_conta_w;
	exit when C01%notfound;
		begin

		select	nvl(max(nr_seq_status_fat),0)
		into	nr_seq_status_fat_w
		from	conta_paciente
		where	nr_interno_conta = nr_interno_conta_w;

		nr_seq_status_mob_w := cf_obter_status_mob(nr_seq_status_fat_w, 'K');		

		update	conta_paciente
		set	nr_seq_status_mob = nvl(nr_seq_status_mob_w, nr_seq_status_mob),
			nm_usuario = nm_usuario_p
		where	nr_interno_conta = nr_interno_conta_w;

		end;
	end loop;
	close C01;

	update	protocolo_documento
	set	ie_status_protocolo = 'R',
		nm_usuario_receb = nm_usuario_p,
		dt_rec_destino = sysdate
	where	nr_sequencia = nr_remessa_p;

	commit;

	end;

end if;

end cf_receber_remessa;
/