create or replace
procedure gerar_bordero_gps(
				nr_seq_gps_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_vencimento_p		date
				) is 

cd_identificador_w		varchar2(50);
qt_registro_w			number(10);
nr_bordero_w			number(10);
nr_titulo_w			number(10);
ie_selec_tit_outro_bordero_w	varchar2(1);
vl_multa_w			number(15,2);
vl_juros_w			number(15,2);
vl_titulo_w			number(15,2);
vl_juros_bordero_w		number(15,2);
vl_multa_bordero_w		number(15,2);
ds_retorno_w            varchar2(1000);

Cursor c01 is
	select	  a.nr_titulo,
            a.vl_juros_bordero,
            a.vl_multa_bordero
	from	    gps_titulo a, 
            titulo_pagar c
  left join classe_titulo_pagar b on c.nr_seq_classe = b.nr_sequencia
	where	 a.nr_titulo                    = c.nr_titulo
  and    nvl(b.ie_permite_bordero, 'S') = 'S'
  and 	 nr_seq_gps = nr_seq_gps_p;
  
begin

-- Verificar se o GPS ja foi inserida em algum outro bordero
select	nr_bordero
into	nr_bordero_w
from	gps
where	nr_sequencia = nr_seq_gps_p;

if (nr_bordero_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(187468);
end if;

-- Buscar o identificador do GPS
select	substr(cd_identificador,1,50)
into	cd_identificador_w
from	gps
where	nr_sequencia = nr_seq_gps_p;

-- Buscar a proxima sequencia
select	bordero_pagamento_seq.nextval
into	nr_bordero_w
from	dual;

-- Verificar se o usuario pode selecionar um titulo ja inserido em outro bordero, parametro [12] - Permite selecionar titulos que ja estejam em outro bordero, funcao "Bordero a pagar"
obter_param_usuario(855,12,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_selec_tit_outro_bordero_w);

if (ie_selec_tit_outro_bordero_w = 'N') then
	
	select	count(*)
	into	qt_registro_w
	from	titulo_pagar t,
		gps_titulo g
	where	g.nr_titulo = t.nr_titulo
	and	g.nr_seq_gps = nr_seq_gps_p
	and	t.nr_bordero is not null;

	if (qt_registro_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(187469);
	end if;

	insert into bordero_pagamento	(
					nr_bordero, 
					dt_prev_pagamento, 
					dt_atualizacao, 
					nm_usuario, 
					dt_real_pagamento, 
					cd_banco, 
					cd_agencia_bancaria, 
					nr_cheque, 
					ds_bordero, 
					nr_seq_conta_banco, 
					nr_seq_trans_financ, 
					nr_documento, 
					dt_cancelamento, 
					cd_estabelecimento, 
					cd_tipo_baixa, 
					ds_observacao, 
					ie_titulo_bordero, 
					dt_inicio_bordero, 
					dt_final_bordero, 
					dt_liberacao, 
					nm_usuario_lib, 
					qt_min_usuario_lib,
                    ie_bordero_banco)
				values	(
					nr_bordero_w,
					dt_vencimento_p,
					sysdate,
					'Tasy',
					null, 
					null, 
					null, 
					null, 
					'GPS ' || cd_identificador_w || ' - ' || dt_vencimento_p,
					null, 
					null, 
					null, 
					null, 
					cd_estabelecimento_p, 
					null, 
					null, 
					'N', 
					null, 
					null, 
					null, 
					null, 
					null,
                    'P');
else
	-- inserir na tabela do bordero
	insert into bordero_pagamento	(
					nr_bordero, 
					dt_prev_pagamento, 
					dt_atualizacao, 
					nm_usuario, 
					dt_real_pagamento, 
					cd_banco, 
					cd_agencia_bancaria, 
					nr_cheque, 
					ds_bordero, 
					nr_seq_conta_banco, 
					nr_seq_trans_financ, 
					nr_documento, 
					dt_cancelamento, 
					cd_estabelecimento, 
					cd_tipo_baixa, 
					ds_observacao, 
					ie_titulo_bordero, 
					dt_inicio_bordero, 
					dt_final_bordero, 
					dt_liberacao, 
					nm_usuario_lib, 
					qt_min_usuario_lib,
					ie_bordero_banco)
				values	(
					nr_bordero_w,
					dt_vencimento_p,
					sysdate,
					'Tasy',
					null, 
					null, 
					null, 
					null, 
					'GPS ' || cd_identificador_w || ' - ' || dt_vencimento_p,
					null, 
					null, 
					null, 
					null, 
					cd_estabelecimento_p, 
					null, 
					null, 
					'S', 
					null, 
					null, 
					null, 
					null, 
					null,
                    'P');	
end if;

-- Verificar o parmetro do usuario, se for N apenas realizar UPDATE na tabela de titulos a pagar; se for S realizar INSERT na tabela BORDERO_TIT_PAGAR.
if (ie_selec_tit_outro_bordero_w = 'N') then

	open c01;
	loop
	fetch c01 into	
		nr_titulo_w,
		vl_juros_bordero_w,
		vl_multa_bordero_w;
	exit when c01%notfound;
		begin
        
		ds_retorno_w := obter_se_inclui_tit_bordero(nr_bordero_w, nr_titulo_w, nm_usuario_p);

        if(ds_retorno_w is not null) then
            wheb_mensagem_pck.exibir_mensagem_abort(ds_retorno_w);
        else
            
		select	vl_saldo_titulo
		into	vl_titulo_w
		from	titulo_pagar
		where	nr_titulo = nr_titulo_w;
		
		vl_juros_w := nvl(vl_juros_w,0) + nvl(vl_juros_bordero_w,0);

		vl_multa_w := nvl(vl_multa_w,0) + nvl(vl_multa_bordero_w,0);
	
		update  titulo_pagar 
		set     nr_bordero = nr_bordero_w, 
				vl_bordero = vl_titulo_w,
				vl_juros_bordero	=	vl_juros_bordero_w,
				vl_multa_bordero	= 	vl_multa_bordero_w
		where 	nr_titulo = nr_titulo_w;
        end if;
		end;
	end loop;
	close c01;

elsif (ie_selec_tit_outro_bordero_w = 'S') then

	open c01;
	loop
	fetch c01 into	
		nr_titulo_w,
		vl_juros_bordero_w,
		vl_multa_bordero_w;
	exit when c01%notfound;
		begin
		select	vl_saldo_multa,
				vl_saldo_juros,
				vl_saldo_titulo
		into	vl_multa_w,
				vl_juros_w,
				vl_titulo_w
		from	titulo_pagar
		where	nr_titulo = nr_titulo_w;

		vl_juros_w := nvl(vl_juros_w,0) + nvl(vl_juros_bordero_w,0);

		vl_multa_w := nvl(vl_multa_w,0) + nvl(vl_multa_bordero_w,0);
        
        ds_retorno_w := obter_se_inclui_tit_bordero(nr_bordero_w, nr_titulo_w, nm_usuario_p);

        if(ds_retorno_w is not null) then
            wheb_mensagem_pck.exibir_mensagem_abort(ds_retorno_w);
        else

		-- inserir na tabela de titulos a pagar do bordero
		insert into bordero_tit_pagar	(
						nr_sequencia, 
						dt_atualizacao, 
						nm_usuario, 
						dt_atualizacao_nrec, 
						nm_usuario_nrec, 
						nr_bordero, 
						nr_titulo, 
						vl_bordero, 
						vl_juros_bordero, 
						vl_desconto_bordero, 
						vl_out_desp_bordero, 
						vl_multa_bordero, 
						cd_centro_custo, 
						vl_outras_deducoes
						)
					values	(
						bordero_tit_pagar_seq.NextVal,
						sysdate,
						'Tasy',
						sysdate,
						'Tasy',
						nr_bordero_w,
						nr_titulo_w,
						vl_titulo_w,
						vl_juros_w,
						0,
						0,
						vl_multa_w,
						null,
						0
						);
        end if;
		end;
	end loop;
	close c01;

end if;
	
-- Gravar na tabela GPS o numero do bordero
update gps set nr_bordero = nr_bordero_w where nr_sequencia = nr_seq_gps_p;
commit;
end gerar_bordero_gps;
/