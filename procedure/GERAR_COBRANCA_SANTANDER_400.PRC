create or replace
procedure gerar_cobranca_santander_400	(	nr_seq_cobr_escrit_p	number,
						cd_estabelecimento_p	number,
						nm_usuario_p		varchar2) is


dt_vencimento_w			date;
dt_emissao_w			date;
dt_desconto_w			date;
vl_juros_mora_w			number(15,2);
ie_tipo_inscricao_w			number(10);
nm_pessoa_w			varchar2(100);
ds_endereco_w			varchar2(100);
ds_bairro_w			varchar2(80);
cd_cep_w			varchar2(20);
ds_cidade_w			varchar2(50);
ds_uf_w				varchar2(15);
ds_cgc_cpf_w			varchar2(15);
vl_titulo_w			number(15,2);
cd_cgc_operadora_w		varchar2(14);
cd_agencia_bancaria_w		varchar2(8);
cd_conta_w			banco_estabelecimento.cd_conta%type;
tx_multa_w			number(7,4);
nr_titulo_w			number(10);
vl_juros_w			number(22);
vl_descontos_w			number(22);
ie_tipo_pessoa_w			varchar2(1);
nr_seq_arquivo_w			number(3);
vl_soma_titulos_w			number(15,2);
nr_nosso_numero_w		varchar2(255);
cd_carteira_w			varchar2(50);
ds_mensagem_w			varchar2(500);
ds_mensagem_2_w			varchar2(500);
ds_mensagem_3_w			varchar2(500);
ds_mensagem_4_w			varchar2(500);
nr_seq_mensalidade_w		number(15);
ds_plano_w			varchar2(255);
ds_observacao_titulo_w		varchar2(500);


Cursor C01 is
	select	d.cd_agencia_bancaria,
		d.cd_conta,
		b.tx_multa,
		b.nr_titulo,
		b.dt_pagamento_previsto, -- vencimento,
		b.vl_titulo,
		b.dt_emissao,
		b.vl_juros,
		b.vl_descontos,
		decode(b.cd_cgc, null, '1', '2'), --tipo inscri��o		
		nvl(b.cd_cgc, b.cd_pessoa_fisica),
		b.nm_pessoa,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'EC') ds_endereco,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B') ds_bairro,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP') cd_cep,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI') ds_cidade,
		obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF') ds_uf,
		b.nr_titulo||';'||b.nr_nota_fiscal,
		b.nr_seq_mensalidade,
		b.ds_observacao_titulo
	from	banco_estabelecimento d,
		titulo_receber_v b,
		titulo_receber_cobr c,
		cobranca_escritural a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	a.nr_seq_conta_banco	= d.nr_Sequencia
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin

delete	from w_cobranca_banco;

select	cd_cgc_outorgante
into	cd_cgc_operadora_w
from	pls_outorgante
where	cd_estabelecimento	= cd_estabelecimento_p;

nr_seq_arquivo_w	:= 1;
vl_soma_titulos_w	:= 0;

/*####	HEADER	####*/
insert into	w_cobranca_banco
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ie_tipo_registro,
	cd_banco,
	nm_empresa,
	dt_geracao)
select	w_cobranca_banco_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	'0' tp_registro,
	a.cd_banco,
	substr(obter_razao_social(b.cd_cgc),1,100) nm_empresa,
	sysdate --dt_gravacao
from	estabelecimento b,
	banco_estabelecimento c,
	cobranca_escritural a
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;


/*####	REMESSA	####*/
open C01;
loop
fetch C01 into	
	cd_agencia_bancaria_w,
	cd_conta_w,
	tx_multa_w,
	nr_titulo_w,
	dt_vencimento_w,
	vl_titulo_w,
	dt_emissao_w,
	vl_juros_w,
	vl_descontos_w,
	ie_tipo_pessoa_w,
	ds_cgc_cpf_w,
	nm_pessoa_w,
	ds_endereco_w,
	ds_bairro_w,
	cd_cep_w,
	ds_cidade_w,
	ds_uf_w,
	nr_nosso_numero_w,
	nr_seq_mensalidade_w,
	ds_observacao_titulo_w;
exit when C01%notfound;
	begin
	ds_mensagem_w	:= null;	
	ds_mensagem_2_w	:= null;
	ds_mensagem_3_w	:= null;
	
	nr_seq_arquivo_w	:= nr_seq_arquivo_w +1;
	vl_soma_titulos_w	:= vl_soma_titulos_w + vl_titulo_w;
	
	insert into	w_cobranca_banco
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_tipo_registro,
		cd_cgc_cedente,
		cd_agencia_bancaria,
		cd_conta,
		tx_multa,
		nr_titulo,
		dt_vencimento,
		vl_titulo,
		dt_emissao,
		vl_juros,
		vl_desconto,
		ie_tipo_pessoa,
		ds_cgc_cpf,
		nm_pagador,
		ds_endereco,
		ds_bairro,
		cd_cep,
		ds_cidade,
		sg_estado,
		nr_nosso_numero,
		nr_seq_reg_lote)
	values	(w_cobranca_banco_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		'1', -- Tipo registro
		cd_cgc_operadora_w,
		substr(cd_agencia_bancaria_w,1,4),
		substr(cd_conta_w,1,8),
		tx_multa_w,
		nr_titulo_w,
		dt_vencimento_w,
		vl_titulo_w,
		dt_emissao_w,
		vl_juros_w,
		vl_descontos_w,
		ie_tipo_pessoa_w,
		ds_cgc_cpf_w,
		nm_pessoa_w,
		ds_endereco_w,
		substr(ds_bairro_w,1,40),
		cd_cep_w,
		ds_cidade_w,
		substr(ds_uf_w,1,2),
		nr_nosso_numero_w,
		nr_seq_arquivo_w);

		
	select	max(substr(pls_obter_dados_segurado(nr_sequencia,'C'),1,100))
	into	cd_carteira_w
	from	pls_segurado
	where	cd_pessoa_fisica	= ds_cgc_cpf_w;
		
	
	
	/* ###	MENSAGENS      ### */
	
	nr_seq_arquivo_w	:= nr_seq_arquivo_w +1;
	ds_mensagem_w	:=	'011'|| rpad('CODIGO ANS: 33.565-7 CNPJ - 02.041.808/0001-42',90,' ')||
				'021' || rpad('Beneficiario: '|| nm_pessoa_w,65,' ')||
				rpad('Codigo: ' || cd_carteira_w ,26,' ') ||
				'031' || rpad('Existem titulos em aberto no seu contrato',90,' ') ||
				rpad('041',113,' ') || lpad(nr_seq_arquivo_w,6,0);	

	insert into	w_cobranca_banco
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_tipo_registro,
		ds_mensagem)
	values	(w_cobranca_banco_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		'2', -- Tipo registro
		ds_mensagem_w);
		
	ds_plano_w	:= null;
	select	max(b.ds_plano)
	into	ds_plano_w
	from	pls_plano	b,
		pls_mensalidade_segurado d,
		pls_mensalidade c
	where	b.nr_sequencia			= d.nr_seq_plano
	and	d.nr_seq_mensalidade		= c.nr_sequencia
	and	c.nr_sequencia			= nr_seq_mensalidade_w;
	
	ds_plano_w	:= ds_plano_w || ' ';
	
	nr_seq_arquivo_w	:= nr_seq_arquivo_w +1;
	ds_mensagem_2_w	:=	'051' || rpad('MENSALIDADE............................:   '||vl_titulo_w,90,' ') || 
				'061' || rpad(ds_plano_w,90,' ') || '071' || rpad('PREZADO(A) CLIENTE,',90,' ') ||
				'081' || rpad('CASO NAO RECEBA O SEU BOLETO ATE A PROXIMA DATA DE  VENCIMENTO, MANTER  CONTATO  TELEFONE:',111,' ') ||
				lpad(nr_seq_arquivo_w,6,0);

	insert into	w_cobranca_banco
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_tipo_registro,
		ds_mensagem)
	values	(w_cobranca_banco_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		'2', -- Tipo registro
		ds_mensagem_2_w);						
		
		
	nr_seq_arquivo_w	:= nr_seq_arquivo_w +1;
	ds_mensagem_3_w	:= 	'091' || rpad('(11)3169-7000 OU EMAIL planosdesaude@advanceplanos.com.br',90,' ') ||
				'101' || rpad('** APOS O VENCIMENTO COBRAR MULTA DE 2%, MORA DIA 0,05%',90,' ') ||
				'111' || rpad('** CONHECA NOSSO SITE: www.advanceplanos.com.br **',90,' ') ||
				rpad('121',114,' ') || lpad(nr_seq_arquivo_w,6,0);
				
	insert into	w_cobranca_banco
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_tipo_registro,
		ds_mensagem)
	values	(w_cobranca_banco_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		'2', -- Tipo registro
		ds_mensagem_3_w);

	
	nr_seq_arquivo_w	:= nr_seq_arquivo_w +1;
	ds_mensagem_4_w		:= rpad(ds_observacao_titulo_w,394, ' ') || lpad(nr_seq_arquivo_w,6,0);
				
	insert into	w_cobranca_banco
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		ie_tipo_registro,
		ds_mensagem)
	values	(w_cobranca_banco_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		'2', -- Tipo registro
		ds_mensagem_4_w);

		
	end;
end loop;
close C01;	
	

/*####	TRAILER	####*/
insert into	w_cobranca_banco
	(nr_sequencia,
	nm_usuario,
	dt_atualizacao,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ie_tipo_registro,
	vl_tot_registros,
	nr_seq_reg_lote)
values	(w_cobranca_banco_seq.nextval,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	'9',
	vl_soma_titulos_w,
	nr_seq_arquivo_w+1);

commit;

end gerar_cobranca_santander_400;
/
