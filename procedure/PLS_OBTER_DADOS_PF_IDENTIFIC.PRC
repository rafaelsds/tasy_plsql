create or replace
procedure pls_obter_dados_pf_identific
			(	cd_pessoa_fisica_p		varchar2,
				nm_pessoa_fisica_p	out 	varchar2,
				dt_nascimento_p		out 	date,
				nr_cpf_p		out 	varchar2,
				nm_mae_p		out	varchar2,
				cd_nacionalidade_p	out 	varchar2,
				ie_sexo_p		out 	varchar2,
				nr_identidade_p		out 	varchar2,
				nr_telefone_celular_p	out 	varchar2,
				cd_cep_p		out 	varchar2,
				ds_endereco_p		out 	varchar2,
				nr_endereco_p		out 	number,
				ds_complemento_p	out 	varchar2,
				cd_municipio_ibge_p	out 	varchar2,
				ds_municipio_ibge_p	out 	varchar2,
				sg_estado_p		out 	varchar2,
				nr_ddi_telefone_p	out 	varchar2,
				nr_ddd_telefone_p	out 	varchar2,
				nr_telefone_p		out 	varchar2,
				ds_email_p		out 	varchar2 ) is 

begin

select	a.nm_pessoa_fisica,
	a.dt_nascimento,
	a.nr_cpf,
	(	select	x.NM_CONTATO
		from	compl_pessoa_fisica	x
		where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
		and	x.ie_tipo_complemento	= '5') nm_mae,
	a.cd_nacionalidade,
	a.ie_sexo,
	a.nr_identidade,
	a.nr_telefone_celular
into	nm_pessoa_fisica_p,
	dt_nascimento_p,
	nr_cpf_p,
	nm_mae_p,
	cd_nacionalidade_p,
	ie_sexo_p,
	nr_identidade_p,
	nr_telefone_celular_p
from	pessoa_fisica		a
where	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;

begin
select	b.cd_cep,
	b.ds_endereco,
	b.nr_endereco,
	b.ds_complemento,
	b.cd_municipio_ibge,
	substr(obter_desc_municipio_ibge(b.cd_municipio_ibge),1,255) ds_municipio_ibge,
	b.sg_estado,
	b.nr_ddi_telefone,
	b.nr_ddd_telefone,
	b.nr_telefone,
	b.ds_email
into	cd_cep_p,
	ds_endereco_p,
	nr_endereco_p,
	ds_complemento_p,
	cd_municipio_ibge_p,
	ds_municipio_ibge_p,
	sg_estado_p,
	nr_ddi_telefone_p,
	nr_ddd_telefone_p,
	nr_telefone_p,
	ds_email_p
from	compl_pessoa_fisica b
where	b.cd_pessoa_fisica	= cd_pessoa_fisica_p
and	b.ie_tipo_complemento	= '1';
exception
when others then
	cd_cep_p		:= null;
	ds_endereco_p		:= null;
	nr_endereco_p		:= null;
	ds_complemento_p	:= null;
	cd_municipio_ibge_p	:= null;
	ds_municipio_ibge_p	:= null;
	sg_estado_p		:= null;
	nr_ddi_telefone_p	:= null;
	nr_ddd_telefone_p	:= null;
	nr_telefone_p		:= null;
	ds_email_p		:= null;
end;

end pls_obter_dados_pf_identific;
/