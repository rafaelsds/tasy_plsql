create or replace
procedure fis_gerar_arquivo_lfpd(	nr_seq_controle_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

cd_registro_w		varchar2(15);
cd_estabelecimento_w	number(4);
dt_inicio_w		date;
dt_fim_w		date;
ds_separador_w		varchar2(1);
ie_gerar_w		varchar2(1);
nr_seq_regra_w		number(10);
qt_linha_w		number(10) := 0;
nr_sequencia_w		number(10);
			
cursor	c01 is
	select	cd_registro, 
		ie_gerar
	from	fis_regra_lfpd_reg
	where	nr_seq_regra_lfpd	= nr_seq_regra_w
	order by nr_sequencia;

begin
/* Limpar a tabela */
delete 	fis_lfpd_arquivo
where	nm_usuario	= nm_usuario_p;

commit;




select	r.cd_estabelecimento,
	c.dt_inicial,
	fim_dia(c.dt_final),
	r.ds_separador,
	c.nr_seq_regra_lfpd
into	cd_estabelecimento_w,
	dt_inicio_w,
	dt_fim_w,
	ds_separador_w,
	nr_seq_regra_w
from	fis_lfpd_regra r,
	fis_lfpd_controle c
where	r.nr_sequencia		= c.nr_seq_regra_lfpd
and	r.cd_estabelecimento	= cd_estabelecimento_p
and	c.nr_sequencia		= nr_seq_controle_p;

open c01;
loop
fetch c01 into	
	cd_registro_w,
	ie_gerar_w;
exit when c01%notfound;
	begin
	
	begin
	/* Bloco Zero */
	if	((cd_registro_w = '0000') and
		(ie_gerar_w = 'S')) then
		lfpd_registro_0000(	nr_seq_controle_p,
					nm_usuario_p,
					cd_estabelecimento_w,
					dt_inicio_w,
					dt_fim_w,
					ds_separador_w,
					qt_linha_w,
					nr_sequencia_w);
	elsif	((cd_registro_w = '0001') and
		(ie_gerar_w = 'S')) then
		lfpd_gerar_reg_abertura(	nr_seq_controle_p,
						nm_usuario_p,
						cd_registro_w,
						'S',
						'',
						'',
						ds_separador_w,
						qt_linha_w,
						nr_sequencia_w);
	elsif	((cd_registro_w = '0005') and
		(ie_gerar_w = 'S')) then
		lfpd_registro_0005(	nr_seq_controle_p,
					nm_usuario_p,
					cd_estabelecimento_w,
					dt_inicio_w,
					dt_fim_w,
					ds_separador_w,
					qt_linha_w,
					nr_sequencia_w);
	elsif	((cd_registro_w = '0100') and
		(ie_gerar_w = 'S')) then
		lfpd_registro_0100(	nr_seq_controle_p,
					nm_usuario_p,
					cd_estabelecimento_w,
					dt_inicio_w,
					dt_fim_w,
					ds_separador_w,
					qt_linha_w,
					nr_sequencia_w);
	elsif	((cd_registro_w = '0125') and
		(ie_gerar_w = 'S')) then
		lfpd_registro_0125(	nr_seq_controle_p,
					nm_usuario_p,
					cd_estabelecimento_w,
					dt_inicio_w,
					dt_fim_w,
					ds_separador_w,
					qt_linha_w,
					nr_sequencia_w);
	elsif	((cd_registro_w = '0150') and
		(ie_gerar_w = 'S')) then
		lfpd_registro_0150(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = '0200') and (ie_gerar_w = 'S')) then
		lfpd_registro_0200(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = '0400') and (ie_gerar_w = 'S')) then
		lfpd_registro_0400(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = '0450') and (ie_gerar_w = 'S')) then		
		lfpd_registro_0450(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = '0990') and (ie_gerar_w = 'S')) then
		lfpd_gerar_reg_encerramento(nr_seq_controle_p, nm_usuario_p, cd_registro_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	
	/* Bloco A */
	elsif	((cd_registro_w = 'A001') and (ie_gerar_w = 'S')) then
		lfpd_gerar_reg_abertura(nr_seq_controle_p, nm_usuario_p, cd_registro_w,'S','5300108','', ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'A020') and (ie_gerar_w = 'S')) then
		lfpd_registro_A020(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'A300') and (ie_gerar_w = 'S')) then
		lfpd_registro_A300(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'A990') and (ie_gerar_w = 'S')) then	
			lfpd_gerar_reg_encerramento(nr_seq_controle_p, nm_usuario_p, cd_registro_w, ds_separador_w, qt_linha_w, nr_sequencia_w);	
	
	/* Bloco B */
	elsif	((cd_registro_w = 'B001') and (ie_gerar_w = 'S')) then
		lfpd_gerar_reg_abertura(nr_seq_controle_p, nm_usuario_p, cd_registro_w,'S','5300108', '', ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'B020') and (ie_gerar_w = 'S')) then	
		lfpd_registro_B020(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'B030') and (ie_gerar_w = 'S')) then	
		lfpd_registro_B030(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'B400') and (ie_gerar_w = 'S')) then	
		lfpd_registro_B400(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	/*elsif	((cd_registro_w = 'B410') and (ie_gerar_w = 'S')) then	
			lfpd_registro_B410(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'B440') and (ie_gerar_w = 'S')) then	
			lfpd_registro_B440(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'B450') and (ie_gerar_w = 'S')) then	
			lfpd_registro_B450(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'B470') and (ie_gerar_w = 'S')) then	
			lfpd_registro_B470(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);*/
	elsif	((cd_registro_w = 'B990') and (ie_gerar_w = 'S')) then	
			lfpd_gerar_reg_encerramento(nr_seq_controle_p, nm_usuario_p, cd_registro_w, ds_separador_w, qt_linha_w, nr_sequencia_w);	
	
	/* Bloco C */
	elsif	((cd_registro_w = 'C001') and (ie_gerar_w = 'S')) then
			lfpd_gerar_reg_abertura(nr_seq_controle_p, nm_usuario_p, cd_registro_w,'N','','', ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'C990') and (ie_gerar_w = 'S')) then	
			lfpd_gerar_reg_encerramento(nr_seq_controle_p, nm_usuario_p, cd_registro_w, ds_separador_w, qt_linha_w, nr_sequencia_w);

	/* Bloco D */
	elsif	((cd_registro_w = 'D001') and (ie_gerar_w = 'S')) then
			lfpd_gerar_reg_abertura(nr_seq_controle_p, nm_usuario_p, cd_registro_w,'N','','', ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'D990') and (ie_gerar_w = 'S')) then	
			lfpd_gerar_reg_encerramento(nr_seq_controle_p, nm_usuario_p, cd_registro_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	
	/* Bloco E */
	elsif	((cd_registro_w = 'E001') and (ie_gerar_w = 'S')) then
			lfpd_gerar_reg_abertura(nr_seq_controle_p, nm_usuario_p, cd_registro_w,'S','', '', ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'E300') and (ie_gerar_w = 'S')) then	
			lfpd_registro_E300(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'E360') and (ie_gerar_w = 'S')) then	
			lfpd_registro_E360(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'E990') and (ie_gerar_w = 'S')) then	
			lfpd_gerar_reg_encerramento(nr_seq_controle_p, nm_usuario_p, cd_registro_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
		
	/* Bloco H */
	elsif	((cd_registro_w = 'H001') and (ie_gerar_w = 'S')) then
			lfpd_gerar_reg_abertura(nr_seq_controle_p, nm_usuario_p, cd_registro_w,'S','', '', ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'H020') and (ie_gerar_w = 'S')) then	
			lfpd_registro_H020(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = 'H990') and (ie_gerar_w = 'S')) then	
			lfpd_gerar_reg_encerramento(nr_seq_controle_p, nm_usuario_p, cd_registro_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	
	/* Bloco 8 */
	elsif	((cd_registro_w = '8001') and (ie_gerar_w = 'S')) then
			lfpd_gerar_reg_abertura(nr_seq_controle_p, nm_usuario_p, cd_registro_w,'N','', 'DF', ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = '8990') and (ie_gerar_w = 'S')) then	
			lfpd_gerar_reg_encerramento(nr_seq_controle_p, nm_usuario_p, cd_registro_w, ds_separador_w, qt_linha_w, nr_sequencia_w);

	/* Bloco 9 */
	elsif	((cd_registro_w = '9001') and (ie_gerar_w = 'S')) then
			lfpd_gerar_reg_abertura(nr_seq_controle_p, nm_usuario_p, cd_registro_w,'S','', '', ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = '9900') and (ie_gerar_w = 'S')) then	
			lfpd_registro_9900(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = '9990') and (ie_gerar_w = 'S')) then
			lfpd_registro_9990(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
	elsif	((cd_registro_w = '9999') and (ie_gerar_w = 'S')) then	
			lfpd_registro_9999(nr_seq_controle_p, nm_usuario_p, cd_estabelecimento_w, dt_inicio_w, dt_fim_w, ds_separador_w, qt_linha_w, nr_sequencia_w);
		
	end if;
		
	end;
	
exception
when no_data_found then
wheb_mensagem_pck.exibir_mensagem_abort(737743);

end;		
	
end loop;
close c01;
commit;



end fis_gerar_arquivo_lfpd;
/
