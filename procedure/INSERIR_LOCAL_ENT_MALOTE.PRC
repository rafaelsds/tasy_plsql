create or replace
procedure inserir_local_ent_malote(nr_seq_malote_p			number,
								   nr_seq_local_entrega_p	number, 
								   nm_usuario_p		Varchar2) is 

begin

if	(nr_seq_malote_p is not null) then
	
	update	malote_envelope_laudo
	set		nr_seq_local_entrega	= nr_seq_local_entrega_p,
			nm_usuario		     	= nm_usuario_p,
			dt_atualizacao		 	= sysdate
	where	nr_sequencia			= nr_seq_malote_p;
	
end if;

commit;

end inserir_local_ent_malote;
/