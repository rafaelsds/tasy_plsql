create or replace
procedure Aghos_ajusta_leito(	nr_seq_solicitacao_p	number,
				cd_cod_posto_enf_p	number,
				cod_enfermaria_p	number,
				cod_leito_p		number,
				cod_digito_leito_p	varchar2,
				ie_reserva_leito_w	varchar2 default 'N') is 

cd_setor_destino_w	number(10);
nr_seq_interno_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
ie_status_unidade_w	varchar2(2);
nr_internacao_w		number(10);

begin

select	max(nr_seq_interno),
	max(ie_status_unidade)
into	nr_seq_interno_w,
	ie_status_unidade_w
from	unidade_atendimento
where	cd_setor_atendimento in (select	b.cd_setor_atendimento
				from	setor_atendimento b
				where	b.cd_setor_externo = cd_cod_posto_enf_p)
and	nm_setor_integracao = cod_enfermaria_p || ';' || cod_leito_p || ';' || trim(cod_digito_leito_p);

update	solicitacao_tasy_aghos
set	nr_seq_unidade  = nr_seq_interno_w
where	nr_sequencia = nr_seq_solicitacao_p;

select	max(cd_pessoa_fisica),
	max(nr_internacao)
into	cd_pessoa_fisica_w,
	nr_internacao_w
from	solicitacao_tasy_aghos
where	nr_sequencia = nr_seq_solicitacao_p;

begin
	if	(ie_reserva_leito_w = 'S') and
		(ie_status_unidade_w = 'L') then
		update	unidade_atendimento
		set	ie_status_unidade = 'R',
			cd_paciente_reserva = cd_pessoa_fisica_w,
			nm_usuario_reserva = 'Aghos',
			nr_internacao_aghos = nr_internacao_w
		where	nr_seq_interno = nr_seq_interno_w;
	end if;
exception
	when others then
		null;
end;
	
commit;

end Aghos_ajusta_leito;
/
