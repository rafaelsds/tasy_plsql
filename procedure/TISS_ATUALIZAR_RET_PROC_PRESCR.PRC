create or replace
procedure TISS_ATUALIZAR_RET_PROC_PRESCR
			(nm_usuario_p		in varchar2,
			nr_prescricao_p		in number,
			cd_procedimento_p	in varchar2,
			ie_status_p		in varchar2,
			nr_guia_operadora_p	in varchar2)
			is
			
cd_procedimento_w	number(10);
			
begin

cd_procedimento_w	:= somente_numero(cd_procedimento_p);

if	(ie_status_p is not null) then

	update	prescr_procedimento
	set	ie_autorizacao	= ie_status_p,
		dt_atualizacao	= sysdate,
		nm_usuario	= nm_usuario_p
	where	nr_prescricao	= nr_prescricao_p
	and	cd_procedimento	= cd_procedimento_w;
	
	if	(nr_guia_operadora_p is not null) then
	
		update	w_unipoa_envio_guia
		set	nr_guia_prestador	= nr_guia_operadora_p
		where	nr_prescricao		= nr_prescricao_p;

		update	prescr_medica
		set	nr_doc_conv		= nvl(nr_guia_operadora_p, nr_doc_conv)
		where	nr_prescricao		= nr_prescricao_p;
	end if;
end if;

commit;

end TISS_ATUALIZAR_RET_PROC_PRESCR;
/