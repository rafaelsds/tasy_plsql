create or replace procedure pfcs_db_mpl_tl_in_use
(
    nr_seq_indicator_p	number,
    cd_estabelecimento_p	number,
    nm_usuario_p	varchar2
) is

	pfcs_panel_detail_seq_w		pfcs_panel_detail.nr_sequencia%type;
	nr_seq_panel_w				pfcs_panel_detail.nr_seq_panel%type;
	qt_total_in_use_w           pfcs_panel.vl_indicator%type := 0;
    qt_total_aux_w              pfcs_panel.vl_indicator_aux%type := 0;
	qt_time_alarms_rule_w		pfcs_telemetry_config.qt_time_alarm_trend_rule%type;
    qt_available_tl_w         number(15) := 0;
    qt_total_brkn_pfcs_w        number(15) := 0;
   
	/* Cursor to read from pfcs integration tables */
	cursor c01_inuse_from_pfcs is
		select 	enc.id_encounter nr_encounter,
				pat.patient_id id_patient,
				pfcs_get_human_name(enc.nr_seq_patient, 'Patient') nm_patient,
				pfcs_get_tele_time(dev.nr_sequencia, 'S') nr_tele_time,
				enc.ds_reason diagnosis,
				pat.birthdate dob_patiente,
				trunc((months_between(nvl(pat.deceased_date, sysdate), pat.birthdate)/12)) qt_idade_paciente,
				pat.gender gender,
				enc.ds_classification ds_classification,
				enc.period_start dt_entrance,
				pfcs_get_human_name(pfcs_get_practitioner_seq(enc.nr_sequencia, '405279007'), 'Practitioner') ds_attending_physician,
				pfcs_get_code_status_tl(pat.nr_sequencia,'S')  ds_dnr_status,
				sa.ds_setor_atendimento ds_department,
				(uni.cd_unidade_basica || '-' || uni.cd_unidade_compl) ds_bed,
				dev.id_device cd_equipamento,
				dev.ds_device_name ds_equipamento,
				pfcs_get_alarms_count(pat.nr_sequencia, 'A0402', qt_time_alarms_rule_w) nr_yellow_alarm_count,
				pfcs_get_alarms_count(pat.nr_sequencia, 'A0401', qt_time_alarms_rule_w) nr_red_alarm_count,
				pfcs_get_battery_status(pat.nr_sequencia) battery_status,
				pfcs_get_discharge_status(enc.nr_sequencia, cd_estabelecimento_p) ie_discharge_status,
				pfcs_get_alarms_trends(pat.nr_sequencia, enc.nr_sequencia) ds_alarms_trends,
				pfcs_get_edi_score(enc.nr_sequencia) as nr_edi_score,
				pfcs_obs_contributor_pck.get_edi_vital_warnings(enc.nr_sequencia) ds_edi_vitals_warn,
				pfcs_obs_contributor_pck.get_edi_contributors(enc.nr_sequencia) ds_edi_contrb
		from 	pfcs_service_request sr,
				pfcs_encounter enc,
				pfcs_patient pat,
				pfcs_device dev,
				unidade_atendimento uni,
				setor_atendimento sa
		where
				( (sr.si_status = 'COMPLETED' and sr.cd_service = 'E0404') or
				(sr.si_status = 'ACTIVE' and sr.cd_service = 'E0403'))
		and 	sr.nr_seq_encounter = enc.nr_sequencia
		and 	enc.si_status in ('PLANNED', 'ARRIVED')
		and 	enc.nr_seq_patient = pat.nr_sequencia
		and 	pat.ie_active = '1'
		and 	dev.nr_seq_patient = pat.nr_sequencia
		and 	dev.si_status = 'ACTIVE'
		and 	dev.ds_device_type = 'Monitor'
		and 	dev.nr_seq_location = uni.nr_seq_location
		and		uni.ie_situacao = 'A'
		and		uni.cd_setor_atendimento = sa.cd_setor_atendimento
		and 	sa.ie_situacao = 'A'
		and 	sa.cd_classif_setor in ('1','3','4','9','11','12')
		and 	sa.cd_estabelecimento_base = cd_estabelecimento_p;

begin
	-- Get hours configurable from telemetry settings
	select	nvl(max(qt_time_alarm_trend_rule),12) into qt_time_alarms_rule_w from pfcs_telemetry_config;
  
  if qt_time_alarms_rule_w is null then 
    qt_time_alarms_rule_w := 8;
  end if;
  
qt_available_tl_w 		:= 	pfcs_func_tl_device_usage('Monitor', 'A', cd_estabelecimento_p);
qt_total_in_use_w 		:= 	pfcs_func_tl_device_usage('Monitor', 'U', cd_estabelecimento_p);
qt_total_brkn_pfcs_w 	:=  pfcs_func_tl_device_usage('Monitor', 'B_L_D', cd_estabelecimento_p);
qt_total_aux_w 			  :=  qt_available_tl_w + qt_total_in_use_w + qt_total_brkn_pfcs_w;

	for r_c01 in c01_inuse_from_pfcs loop
		select pfcs_panel_detail_seq.nextval into pfcs_panel_detail_seq_w from dual;
		
		pfcs_pck_v2.pfcs_insert_details
		(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p	=> cd_estabelecimento_p,
			nm_usuario_p => nm_usuario_p,
			nr_panel_detail_seq_p => pfcs_panel_detail_seq_w,
			nr_encounter_p => r_c01.nr_encounter,
			id_patient_p => r_c01.id_patient,
			nm_patient_p => r_c01.nm_patient,
			dt_birthdate_p => r_c01.dob_patiente,
			ds_primary_diagnosis_p => r_c01.diagnosis,
			ds_gender_p => r_c01.gender,
			ds_dnr_status_p => r_c01.ds_dnr_status,
			qt_time_telemetry_p => r_c01.nr_tele_time,
			ds_classification_p => r_c01.ds_classification,
			dt_entrance_p => r_c01.dt_entrance,
			ds_service_line_p => r_c01.ds_attending_physician,
			cd_department_p => r_c01.ds_department,
			ds_department_p => r_c01.ds_department,
			ds_bed_location_p => r_c01.ds_bed,
			qt_red_alarms_p => r_c01.nr_red_alarm_count,
			qt_yellow_alarms_p => r_c01.nr_yellow_alarm_count,
			cd_equipamento_p => r_c01.cd_equipamento,
			ds_device_p => r_c01.ds_equipamento,
			ds_battery_status_p => r_c01.battery_status,
			ie_discharge_status_p =>  r_c01.ie_discharge_status,
			ds_age_range_p => r_c01.qt_idade_paciente,
			ds_alarms_trends_p => r_c01.ds_alarms_trends,
			nr_edi_score_p =>  r_c01.nr_edi_score,
			ds_edi_vitals_warn_p =>  r_c01.ds_edi_vitals_warn,
			ds_edi_contrb_p =>  r_c01.ds_edi_contrb
		);
	end loop;


	pfcs_pck_v2.pfcs_generate_results(
		vl_indicator_p => qt_total_in_use_w,
		ds_reference_value_p => '',
		vl_indicator_aux_p => qt_total_aux_w,
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);

	pfcs_pck_v2.pfcs_update_detail(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_panel_p => nr_seq_panel_w,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p);

	commit;

	pfcs_pck_v2.pfcs_activate_records(
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => cd_estabelecimento_p,
			nm_usuario_p => nm_usuario_p);

end pfcs_db_mpl_tl_in_use;
/
