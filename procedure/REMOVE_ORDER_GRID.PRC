create or replace
procedure remove_order_grid(	ds_grid_tabela_p	Varchar2,
								nm_usuario_p		Varchar2) is 

begin

	Delete from usuario_ordem_grid 
	where nm_usuario = nm_usuario_p 
	and cd_funcao = 24 
	and ds_grid_tabela like('%' || ds_grid_tabela_p || '%');

commit;

end remove_order_grid;
/