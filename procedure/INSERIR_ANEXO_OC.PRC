create or replace
procedure inserir_anexo_oc(
				nm_usuario_p		Varchar2,
				nr_ordem_compra_p	number,
				ds_arquivo_p		varchar2,
				ds_titulo_p		varchar2 default null,
				ie_anexar_email_p	varchar2 default 'S',
				ds_observacao_p		varchar2 default null,
				ie_doc_origem_p		varchar2 default null) is 

begin

insert into ordem_compra_anexo (NR_SEQUENCIA, 
				NR_ORDEM_COMPRA, 
				DT_ATUALIZACAO, 
				NM_USUARIO,   
				DS_ARQUIVO,
				ie_anexar_email,
				ds_titulo,
				ds_observacao,
				ie_doc_origem)
			values(	ordem_compra_anexo_seq.nextval,
				nr_ordem_compra_p,
				sysdate,
				nm_usuario_p,
				ds_arquivo_p,
				nvl(ie_anexar_email_p, 'S'),
				ds_titulo_p,
				substr(ds_observacao_p,1, 255),
				ie_doc_origem_p);

commit;

end inserir_anexo_oc;
/
