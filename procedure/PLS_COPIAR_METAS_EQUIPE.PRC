create or replace
procedure pls_copiar_metas_equipe(
		ds_valores_p		varchar2,
		nr_seq_meta_p		number,
		cd_estabelecimento_p	number,
		nm_usuario_p		varchar2)	is 

ds_valores_w		varchar2(2000);
nr_pos_virgula_w	number(10,0);
nr_sequencia_w	number(10);

begin
if	(ds_valores_p is not null)  and
	(nr_seq_meta_p is not null) and
	(cd_estabelecimento_p is not null)  and
	(nm_usuario_p is not null)  then
	begin
		
	ds_valores_w	:= ds_valores_p;
		
	while (ds_valores_w is not null) loop
		begin
		
		nr_pos_virgula_w	:= instr(ds_valores_w,',');
	
		if	(nr_pos_virgula_w > 0) then
			begin
			nr_sequencia_w	:= to_number(substr(ds_valores_w,0,nr_pos_virgula_w-1));
			ds_valores_w	:= substr(ds_valores_w,nr_pos_virgula_w+1,length(ds_valores_w));			
			end;
		else
			begin
			nr_sequencia_w	:= to_number(ds_valores_w);
			ds_valores_w	:= null;
			end;
		end if;
		
		if	(nr_sequencia_w > 0) then
			begin
			pls_copiar_meta_equipe(
				nr_sequencia_w,
				nr_seq_meta_p,
				cd_estabelecimento_p,
				nm_usuario_p);
			end;
		end if;
		end;
	end loop;		
	end;
end if;
commit;
end pls_copiar_metas_equipe;
/
