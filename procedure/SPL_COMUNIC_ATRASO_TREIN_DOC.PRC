create or replace procedure spl_comunic_atraso_trein_doc(
			nm_usuario_p	varchar2) is
			
ie_comunic_w		varchar2(10);
cd_documento_w		varchar2(20);
nm_documento_w		varchar2(255);
ds_treinamento_w		varchar2(255);
nr_seq_treinamento_w	number(10);


ds_titulo_w		varchar2(255);
ds_comunicacao_w		varchar2(32000);
ds_comunicacao_aux_w	varchar2(32000);
nm_usuario_destino_w	varchar2(4000);
nm_pf_usuario_dest_w	varchar2(60);
ds_email_destino_w		varchar2(255);
ds_setor_adicional_w	varchar2(2000);

Cursor C01 is
	select	'1' ie_comunic,
		a.cd_documento,
		a.nm_documento,
		b.ds_treinamento,
		b.nr_sequencia
	from	qua_doc_treinamento b,
		qua_documento a
	where	a.nr_sequencia = b.nr_seq_documento
	and	a.ie_situacao = 'A'
	and	a.ie_status <> 'I'
	and	b.dt_liberacao is null
	and	b.dt_fim_real is null
	and	obter_dias_entre_datas(b.dt_prevista,sysdate) = 20
	and exists(	select	1
			from	qua_doc_trein_resp x
			where	x.nr_seq_treinamento = b.nr_sequencia)
	union all
	select	'2' ie_comunic,
		a.cd_documento,
		a.nm_documento,
		b.ds_treinamento,
		b.nr_sequencia
	from	qua_doc_treinamento b,
		qua_documento a
	where	a.nr_sequencia = b.nr_seq_documento
	and	a.ie_situacao = 'A'
	and	a.ie_status <> 'I'
	and	b.dt_liberacao is null
	and	b.dt_fim_real is null
	and	obter_dias_entre_datas(b.dt_prevista,sysdate) = 30
	and exists(	select	1
			from	qua_doc_trein_resp x
			where	x.nr_seq_treinamento = b.nr_sequencia);

Cursor C02 is
	select	c.nm_usuario || ', ',
		SUBSTR(OBTER_NOME_PF(b.CD_PESSOA_FISICA),0,60),
		c.ds_email,
		null cd_setor
	from	usuario c,
		pessoa_fisica b,
		qua_doc_trein_resp a
	where	b.cd_pessoa_fisica = a.cd_pessoa_resp
	and	b.cd_pessoa_fisica = c.cd_pessoa_fisica
	and	c.ie_situacao = 'A'
	and	a.nr_seq_treinamento = nr_seq_treinamento_w
	union all
	select	null,
		null,
		'qualidade@hospitalbelvedere.com.br;qualidade2@hospitalbelvedere.com.br',
		'44, '
	from	dual
	where	ie_comunic_w = '2';

begin

if	(nvl(nm_usuario_p,'0') <> '0') then
	begin
	ds_titulo_w := substr('Treinamento em atraso',1,255);
	
	open C01;
	loop
	fetch C01 into	
		ie_comunic_w,
		cd_documento_w,
		nm_documento_w,
		ds_treinamento_w,
		nr_seq_treinamento_w;
	exit when C01%notfound;
		begin
		ds_comunicacao_w := substr(	'O treinamento ''' || ds_treinamento_w || ''' referente ao documento ''' || cd_documento_w || ' - ' || nm_documento_w || ''' ' ||
						'de sua responsabilidade ainda n�o foi realizado. O prazo limite para a realiza��o do mesmo � (calcular 30 dias depois de ser gerada a lista de presen�a).' || chr(13) || chr(10) || chr(13) || chr(10) ||
						'Ap�s este prazo o treinamento ser� considerado em atraso, gerando automaticamente a abertura de um RNC.' || chr(13) || chr(10) || chr(13) || chr(10) ||
						'Caso j� tenha realizado o treinamento e entregue a lista de presen�a para o Servi�o da Qualidade, favor desconsiderar esta mensagem.',1,32000);
		open C02;
		loop
		fetch C02 into
			nm_usuario_destino_w,
			nm_pf_usuario_dest_w,
			ds_email_destino_w,
			ds_setor_adicional_w;
		exit when C02%notfound;
			begin
			ds_comunicacao_aux_w		:= substr('ATEN��O!' || chr(13) || chr(10) || chr(13) || chr(10),1,32000);			
			if	(nvl(nm_usuario_destino_w,'0') <> '0') then
				ds_comunicacao_aux_w	:= substr(ds_comunicacao_aux_w || nm_pf_usuario_dest_w || ',' || chr(13) || chr(10) || chr(13) || chr(10),1,32000);
			end if;			
			ds_comunicacao_aux_w		:= substr(ds_comunicacao_aux_w || ds_comunicacao_w,1,32000);			
			
			insert into comunic_interna(
					dt_comunicado,
					ds_titulo,
					ds_comunicado,
					nm_usuario,
					dt_atualizacao,
					ie_geral,
					nm_usuario_destino,
					ds_setor_adicional,
					nr_sequencia,
					ie_gerencial,
					nr_seq_classif,
					dt_liberacao)
				values(	sysdate,
					ds_titulo_w,
					ds_comunicacao_aux_w, 
					nm_usuario_p,
					sysdate,
					'N',
					nm_usuario_destino_w,
					ds_setor_adicional_w,
					comunic_interna_seq.nextval,
					'N',
					null,
					sysdate);
			
			if	(nvl(ds_email_destino_w,'0') <> '0') then
				begin
				enviar_email(ds_titulo_w,ds_comunicacao_aux_w,null,ds_email_destino_w,nm_usuario_p,'M');
				exception
				when others then
					null;
				end;
			end if;			
			end;
		end loop;
		close C02;
		end;
	end loop;
	close C01;
	
	commit;
	end;
end if;

end spl_comunic_atraso_trein_doc;
/