create or replace
procedure ptu_env_ped_insist_v50_ws(	nr_seq_requisicao_p		ptu_pedido_autorizacao.nr_seq_requisicao%type,
					nr_seq_guia_p			ptu_pedido_autorizacao.nr_seq_guia%type,
					ds_mensagem_p			Varchar2,
					nr_versao_ptu_p			ptu_pedido_insistencia.nr_versao%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type,
					nr_seq_pedido_novo_p	out	ptu_pedido_insistencia.nr_sequencia%type) is 

begin

ptu_envio_scs_ws_pck.ptu_envio_ped_insistencia(nr_seq_requisicao_p, nr_seq_guia_p, ds_mensagem_p, nr_versao_ptu_p,
						cd_estabelecimento_p, nm_usuario_p, nr_seq_pedido_novo_p);

end ptu_env_ped_insist_v50_ws;
/