create or replace
procedure cpoe_rep_gerar_hemodialise(	
							nr_prescricao_p			prescr_medica.nr_prescricao%type,
							nr_atendimento_p		atendimento_paciente.nr_atendimento%type,
							dt_inicio_prescr_p		date,
							dt_validade_prescr_p	date,
							dt_liberacao_p			date,
							nm_usuario_p			usuario.nm_usuario%type,
							cd_funcao_origem_p		funcao.cd_funcao%type,
							cd_setor_atendimento_p	prescr_medica.cd_setor_atendimento%type default null) is 

ie_categoria_w				hd_prescricao.ie_categoria%type;
ie_tipo_hemodialise_w		hd_prescricao.ie_tipo_hemodialise%type;
qt_fluxo_sangue_w			hd_prescricao.qt_fluxo_sangue%type;
qt_sessao_sem_w				hd_prescricao.qt_sessao_sem%type;
qt_hora_sessao_w			hd_prescricao.qt_hora_sessao%type;
qt_min_sessao_w				hd_prescricao.qt_min_sessao%type;
nr_seq_mod_dialisador_w		hd_prescricao.nr_seq_mod_dialisador%type;
ie_cateter_w				hd_prescricao.ie_cateter%type;
qt_peso_pos_w				hd_prescricao.qt_peso_pos%type;
qt_peso_atual_w				hd_prescricao.qt_peso_atual%type;
qt_peso_ideal_w				hd_prescricao.qt_peso_ideal%type;
ie_ultrafiltracao_w			hd_prescricao.ie_ultrafiltracao%type;
qt_ultrafiltracao_w			hd_prescricao.qt_ultrafiltracao%type;
nr_seq_diametro_agulha_w	hd_prescricao.nr_seq_diametro_agulha%type;
nr_seq_tipo_agulha_w		hd_prescricao.nr_seq_tipo_agulha%type;
nr_seq_tipo_agulha_art_w	hd_prescricao.nr_seq_tipo_agulha_art%type;
nr_seq_ultra_w				hd_prescricao.nr_seq_ultra%type;
nr_seq_perfil_sodio_w		hd_prescricao.nr_seq_perfil_sodio%type;
qt_zero_um_w				hd_prescricao.qt_zero_um%type;
qt_um_dois_w				hd_prescricao.qt_um_dois%type;
qt_dois_tres_w				hd_prescricao.qt_dois_tres%type;
qt_tres_quatro_w			hd_prescricao.qt_tres_quatro%type;
qt_seto_oito_w				hd_prescricao.qt_seto_oito%type;
qt_cinco_seis_w				hd_prescricao.qt_cinco_seis%type;
qt_sodio_w					hd_prescricao.qt_sodio%type;
qt_calcio_w					hd_prescricao.qt_calcio%type;
qt_bicarbonato_w			hd_prescricao.qt_bicarbonato%type;
ie_ligar_prime_w			hd_prescricao.ie_ligar_prime%type;
qt_volume_prime_w			hd_prescricao.qt_volume_prime%type;
ds_observacao_w				hd_prescricao.ds_observacao%type;
cd_perfil_ativo_w			hd_prescricao.cd_perfil_ativo%type;
dt_inicio_dialise_w			hd_prescricao.dt_inicio_dialise%type;
nr_seq_dialise_w			hd_prescricao.nr_sequencia%type;
ie_tipo_dialise_w			hd_prescricao.ie_tipo_dialise%type;
ie_apagar_visor_w			hd_prescricao.ie_apagar_visor%type;
ie_ausente_w				hd_prescricao.ie_ausente%type;
qt_hora_min_duracao_w		hd_prescricao.qt_hora_duracao%type;
qt_min_duracao_w			hd_prescricao.qt_min_duracao%type;
nr_ciclos_w					hd_prescricao.nr_ciclos%type;
qt_limite_uf_negativo_w		hd_prescricao.qt_limite_uf_negativo%type;
qt_limite_uf_positivo_w		hd_prescricao.qt_limite_uf_positivo%type;
nr_seq_maq_cicladora_w		hd_prescricao.nr_seq_maq_cicladora%type;
qt_minimo_volume_w 			hd_prescricao.qt_minimo_volume%type;
ie_modo_dialise_w			hd_prescricao.ie_modo_dialise%type;
nr_serie_w					hd_prescricao.nr_serie%type;
ie_perm_inteligente_w		hd_prescricao.ie_perm_inteligente%type;
qt_tempo_min_drenagem_w 	hd_prescricao.qt_tempo_min_drenagem%type;
ie_tipo_peritoneal_w		hd_prescricao.ie_tipo_peritoneal%type;
ie_volume_alarme_w 			hd_prescricao.ie_volume_alarme%type;
qt_volume_ciclo_w			hd_prescricao.qt_volume_ciclo%type;
qt_volume_total_di_w		hd_prescricao.qt_volume_total%type;
nr_seq_perfil_bic_w			hd_prescricao.nr_seq_perfil_bic%type;	
qt_zero_um_bic_w			hd_prescricao.qt_zero_um_bic%type;
qt_um_dois_bic_w			hd_prescricao.qt_um_dois_bic%type;
qt_dois_tres_bic_w			hd_prescricao.qt_dois_tres_bic%type;
qt_tres_quatro_bic_w		hd_prescricao.qt_tres_quatro_bic%type;
qt_quatro_cinco_bic_w		hd_prescricao.qt_quatro_cinco_bic%type;
qt_cinco_seis_bic_w			hd_prescricao.qt_cinco_seis_bic%type;

ie_bomba_infusao_w			prescr_solucao.ie_bomba_infusao%type;
ie_tipo_solucao_w			prescr_solucao.ie_tipo_solucao%type;
nr_seq_protocolo_w			prescr_solucao.nr_seq_protocolo%type;
qt_solucao_total_w			prescr_solucao.qt_solucao_total%type;
qt_dosagem_w				prescr_solucao.qt_dosagem%type;
ie_unid_vel_inf_w			prescr_solucao.ie_unid_vel_inf%type;
qt_dose_ataque_w			prescr_solucao.qt_dose_ataque%type;
qt_temp_solucao_w			prescr_solucao.qt_temp_solucao%type;
nr_seq_solucao_w			prescr_solucao.nr_seq_solucao%type;
ds_orientacao_w				prescr_solucao.ds_orientacao%type;
ie_lavar_linhas_w			prescr_solucao.ie_lavar_linhas%type;
ie_tidal_w					prescr_solucao.ie_tidal%type;
ie_ultima_bolsa_w			prescr_solucao.ie_ultima_bolsa%type;
nr_etapas_w					prescr_solucao.nr_etapas%type;
pr_concentracao_w			prescr_solucao.pr_concentracao%type;
qt_tempo_drenagem_w			prescr_solucao.qt_tempo_drenagem%type;
qt_tempo_infusao_w			prescr_solucao.qt_tempo_infusao%type;
qt_tempo_permanencia_w		prescr_solucao.qt_tempo_permanencia%type;
qt_hora_permanencia_w		prescr_solucao.qt_hora_permanencia%type;
qt_volume_w					prescr_solucao.qt_volume%type;

cd_material_w				prescr_material.cd_material%type;
cd_unidade_medida_dose_w	prescr_material.cd_unidade_medida_dose%type;
qt_dose_w					prescr_material.qt_dose%type;

nr_sequencia_w				cpoe_dialise.nr_sequencia%type;	
qt_total_ciclos_w			cpoe_dialise.qt_total_ciclos%type;

ie_retrogrado_w				prescr_medica.ie_prescr_emergencia%type;
cd_pessoa_fisica_w			prescr_medica.cd_pessoa_fisica%type;
nm_usuario_lib_enf_w		prescr_medica.nm_usuario_lib_enf%type;
dt_inicio_prescr_w			prescr_medica.dt_inicio_prescr%type;

count_solucao_w				number(10) := 0;
count_component_w			number(10) := 0;
sql_w						varchar2(4000);
ds_sep_bv_w					varchar2(10);
ds_parametros_w				varchar2(2000);
qt_hora_min_sessao_w		varchar2(255);
ds_num_atrib_w				varchar2(3);
qt_duracao_ciclo_w			varchar2(10);
qt_permanencia_w			varchar2(10);
dt_liberacao_enf_w			date;
dt_prox_geracao_w			date;

cursor hemodialysis_cursor is
	select	ie_categoria,
		ie_tipo_hemodialise,
		qt_fluxo_sangue,
		qt_sessao_sem,
		qt_hora_sessao,
		qt_min_sessao,
		nr_seq_mod_dialisador,
		ie_cateter,
		qt_peso_pos,
		qt_peso_atual,
		qt_peso_ideal,
		ie_ultrafiltracao,
		qt_ultrafiltracao,
		nr_seq_diametro_agulha,
		nr_seq_tipo_agulha,
		nr_seq_tipo_agulha_art,
		nr_seq_ultra,
		nr_seq_perfil_sodio,
		qt_zero_um,
		qt_um_dois,
		qt_dois_tres,
		qt_tres_quatro,
		qt_seto_oito,
		qt_cinco_seis,
		qt_sodio,
		qt_calcio,
		qt_bicarbonato,
		ie_ligar_prime,
		qt_volume_prime,
		ds_observacao,
		cd_perfil_ativo,
		dt_inicio_dialise,
		nr_sequencia,
		ie_tipo_dialise,
		ie_apagar_visor,
		ie_ausente,
		qt_hora_duracao,
		qt_min_duracao,
		nr_ciclos,
		qt_limite_uf_negativo,
		qt_limite_uf_positivo,
		nr_seq_maq_cicladora,
		qt_minimo_volume,
		ie_modo_dialise,
		nr_serie,
		ie_perm_inteligente,
		qt_tempo_min_drenagem,
		ie_tipo_peritoneal,
		ie_volume_alarme,
		qt_volume_ciclo,
		qt_volume_total,
		nr_seq_perfil_bic,
		qt_zero_um_bic,
		qt_um_dois_bic,
		qt_dois_tres_bic,
		qt_tres_quatro_bic,
		qt_quatro_cinco_bic,
		qt_cinco_seis_bic
	from	hd_prescricao
	where	nr_prescricao = nr_prescricao_p
	and	nvl(ie_tipo_dialise,'H') in ('H','P');		

cursor solution_cursor is
	SELECT	ie_bomba_infusao,
			ie_tipo_solucao,
			nr_seq_protocolo,
			qt_solucao_total,
			qt_dosagem,
			ie_unid_vel_inf,
			qt_dose_ataque,
			qt_temp_solucao,
			nr_seq_solucao,
			ds_orientacao,
			ie_lavar_linhas,
			ie_tidal,
			ie_ultima_bolsa,
			nr_etapas,
			pr_concentracao,
			qt_tempo_drenagem,
			qt_tempo_infusao,
			qt_tempo_permanencia,
			qt_hora_permanencia,
			qt_volume
	from	prescr_solucao
	where	nr_seq_dialise	= nr_seq_dialise_w
	and		nr_prescricao	= nr_prescricao_p
	and		rownum		< 6;

cursor component_solution_cursor is
	select	material,
			unidade,
			dose
	from	(select	cd_material material,
					cd_unidade_medida_dose unidade,
					qt_dose dose
			from	prescr_material
			where	ie_agrupador			= 13
			and		nr_sequencia_solucao	= nr_seq_solucao_w
			and		nr_prescricao			= nr_prescricao_p
			order by nr_agrupamento)
	where	rownum < 8;

begin

ds_sep_bv_w	:= obter_separador_bv;

select 	max(nvl(ie_prescr_emergencia,'N')),
		max(cd_pessoa_fisica),
		max(dt_liberacao),
		max(nm_usuario_lib_enf),
		max(dt_inicio_prescr)
into	ie_retrogrado_w,
		cd_pessoa_fisica_w,
		dt_liberacao_enf_w,
		nm_usuario_lib_enf_w,
		dt_inicio_prescr_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_p;

open hemodialysis_cursor;
loop
fetch hemodialysis_cursor into	ie_categoria_w,
				ie_tipo_hemodialise_w,
				qt_fluxo_sangue_w,
				qt_sessao_sem_w,
				qt_hora_sessao_w,
				qt_min_sessao_w,
				nr_seq_mod_dialisador_w,
				ie_cateter_w,
				qt_peso_pos_w,
				qt_peso_atual_w,
				qt_peso_ideal_w,
				ie_ultrafiltracao_w,
				qt_ultrafiltracao_w,
				nr_seq_diametro_agulha_w,
				nr_seq_tipo_agulha_w,
				nr_seq_tipo_agulha_art_w,
				nr_seq_ultra_w,
				nr_seq_perfil_sodio_w,
				qt_zero_um_w,
				qt_um_dois_w,
				qt_dois_tres_w,
				qt_tres_quatro_w,
				qt_seto_oito_w,
				qt_cinco_seis_w,
				qt_sodio_w,
				qt_calcio_w,
				qt_bicarbonato_w,
				ie_ligar_prime_w,
				qt_volume_prime_w,
				ds_observacao_w,
				cd_perfil_ativo_w,
				dt_inicio_dialise_w,
				nr_seq_dialise_w,
				ie_tipo_dialise_w,
				ie_apagar_visor_w,
				ie_ausente_w,
				qt_hora_min_duracao_w,
				qt_min_duracao_w,
				nr_ciclos_w,
				qt_limite_uf_negativo_w,
				qt_limite_uf_positivo_w,
				nr_seq_maq_cicladora_w,
				qt_minimo_volume_w,
				ie_modo_dialise_w,
				nr_serie_w,
				ie_perm_inteligente_w,
				qt_tempo_min_drenagem_w,
				ie_tipo_peritoneal_w,
				ie_volume_alarme_w,
				qt_volume_ciclo_w,
				qt_volume_total_di_w,
				nr_seq_perfil_bic_w,
				qt_zero_um_bic_w,
				qt_um_dois_bic_w,
				qt_dois_tres_bic_w,
				qt_tres_quatro_bic_w,
				qt_quatro_cinco_bic_w,
				qt_cinco_seis_bic_w;

exit when hemodialysis_cursor%notfound;
	begin
	
	dt_prox_geracao_w := nvl(dt_inicio_dialise_w, dt_inicio_prescr_w);

	select	cpoe_dialise_seq.nextval
	into	nr_sequencia_w
	from	dual;

	qt_hora_min_sessao_w := obter_horas_minutos(qt_min_sessao_w + (qt_hora_sessao_w * 60));
	qt_duracao_ciclo_w := obter_horas_minutos(qt_min_duracao_w + (qt_hora_min_duracao_w * 60));
	qt_total_ciclos_w := obter_total_ciclos(nr_prescricao_p);
	
	insert into cpoe_dialise (
				nr_sequencia,
				nr_atendimento,
				ie_hemodialise,
				ie_categoria,
				ie_tipo_hemodialise,
				qt_fluxo_sangue,
				qt_sessao_sem,
				qt_hora_min_sessao,
				nr_seq_mod_dialisador,
				ie_cateter,
				qt_peso_pos,
				qt_peso_atual,
				qt_peso_ideal,
				ie_ultrafiltracao,
				qt_ultrafiltracao,
				nr_seq_diametro_agulha,
				nr_seq_tipo_agulha,
				nr_seq_tipo_agulha_art,
				nr_seq_ultra,
				nr_seq_perfil_sodio,
				qt_zero_um,
				qt_um_dois,
				qt_dois_tres,
				qt_tres_quatro,
				qt_quatro_cinco,
				qt_cinco_seis,
				qt_sodio,
				qt_calcio,
				qt_bicarbonato,
				ie_ligar_prime,
				qt_volume_prime,
				ds_orientacao,
				nm_usuario,
				nm_usuario_nrec,
				dt_atualizacao,
				dt_atualizacao_nrec,
				cd_perfil_ativo,
				dt_liberacao,
				dt_inicio,
				dt_fim,
				dt_prox_geracao,
				ie_tipo_dialise,
				ie_apagar_visor,
				ie_ausente,
				qt_hora_min_duracao,
				nr_ciclos,
				qt_limite_uf_negativo,
				qt_limite_uf_positivo,
				nr_seq_maq_cicladora,
				qt_minimo_volume,
				ie_modo_dialise,
				nr_serie,
				ie_perm_inteligente,
				qt_tempo_min_drenagem,
				ie_tipo_peritoneal,
				ie_volume_alarme,
				qt_volume_ciclo,
				qt_volume_total_di,
				cd_funcao_origem,
				nr_seq_perfil_bic,
				qt_zero_um_bic,
				qt_um_dois_bic,
				qt_dois_tres_bic,
				qt_tres_quatro_bic,
				qt_quatro_cinco_bic,
				qt_cinco_seis_bic,
				qt_total_ciclos,
				cd_setor_atendimento,
				ie_retrogrado,
				cd_pessoa_fisica,
				dt_liberacao_enf,
				nm_usuario_lib_enf
		) values (
				nr_sequencia_w,
				nr_atendimento_p,
				'S',
				ie_categoria_w,
				ie_tipo_hemodialise_w,
				qt_fluxo_sangue_w,
				qt_sessao_sem_w,
				qt_hora_min_sessao_w,
				nr_seq_mod_dialisador_w,
				ie_cateter_w,
				qt_peso_pos_w,
				qt_peso_atual_w,
				qt_peso_ideal_w,
				ie_ultrafiltracao_w,
				qt_ultrafiltracao_w,
				nr_seq_diametro_agulha_w,
				nr_seq_tipo_agulha_w,
				nr_seq_tipo_agulha_art_w,
				nr_seq_ultra_w,
				nr_seq_perfil_sodio_w,
				qt_zero_um_w,
				qt_um_dois_w,
				qt_dois_tres_w,
				qt_tres_quatro_w,
				qt_seto_oito_w,
				qt_cinco_seis_w,
				qt_sodio_w,
				qt_calcio_w,
				qt_bicarbonato_w,
				ie_ligar_prime_w,
				qt_volume_prime_w,
				ds_observacao_w,
				nm_usuario_p,
				nm_usuario_p,
				sysdate,
				sysdate,
				cd_perfil_ativo_w,
				dt_liberacao_p,
				dt_inicio_dialise_w,
				dt_validade_prescr_p,
				dt_prox_geracao_w + 12/24,
				decode(ie_tipo_dialise_w,'H','DI','P','DP'),
				ie_apagar_visor_w,
				ie_ausente_w,
				qt_duracao_ciclo_w,
				nr_ciclos_w,
				qt_limite_uf_negativo_w,
				qt_limite_uf_positivo_w,
				nr_seq_maq_cicladora_w,
				qt_minimo_volume_w,
				ie_modo_dialise_w,
				nr_serie_w,
				ie_perm_inteligente_w,
				qt_tempo_min_drenagem_w,
				ie_tipo_peritoneal_w,
				ie_volume_alarme_w,
				qt_volume_ciclo_w,
				qt_volume_total_di_w,
				cd_funcao_origem_p,
				nr_seq_perfil_bic_w,
				qt_zero_um_bic_w,
				qt_um_dois_bic_w,
				qt_dois_tres_bic_w,
				qt_tres_quatro_bic_w,
				qt_quatro_cinco_bic_w,
				qt_cinco_seis_bic_w,
				qt_total_ciclos_w,
				cd_setor_atendimento_p,
				ie_retrogrado_w,
				cd_pessoa_fisica_w,
				dt_liberacao_enf_w,
				nm_usuario_lib_enf_w);

	count_solucao_w := 0;

	open solution_cursor;
	loop
	fetch solution_cursor into
			ie_bomba_infusao_w,
			ie_tipo_solucao_w,
			nr_seq_protocolo_w,
			qt_solucao_total_w,
			qt_dosagem_w,
			ie_unid_vel_inf_w,
			qt_dose_ataque_w,
			qt_temp_solucao_w,
			nr_seq_solucao_w,
			ds_orientacao_w,
			ie_lavar_linhas_w,
			ie_tidal_w,
			ie_ultima_bolsa_w,
			nr_etapas_w,
			pr_concentracao_w,
			qt_tempo_drenagem_w,
			qt_tempo_infusao_w,
			qt_tempo_permanencia_w,
			qt_hora_permanencia_w,
			qt_volume_w;
	exit when solution_cursor%notfound;
		begin

		update	prescr_solucao
		set	nr_seq_dialise_cpoe	= nr_sequencia_w
		where	nr_seq_solucao		= nr_seq_solucao_w
		and	nr_prescricao		= nr_prescricao_p;

		count_solucao_w := count_solucao_w + 1;

		qt_duracao_ciclo_w := obter_horas_minutos(qt_tempo_permanencia_w + (qt_hora_permanencia_w * 60));
		
		ds_num_atrib_w := '';
		if (count_solucao_w > 1) then
			ds_num_atrib_w := to_char(count_solucao_w);
		end if;

		ds_parametros_w	:= 	'nr_seq_protocolo='	|| nr_seq_protocolo_w		|| ds_sep_bv_w ||
					'ie_tipo_solucao='	|| ie_tipo_solucao_w		|| ds_sep_bv_w ||
					'ie_bomba_infusao='	|| ie_bomba_infusao_w		|| ds_sep_bv_w ||
					'qt_solucao_total='	|| qt_solucao_total_w		|| ds_sep_bv_w ||
					'qt_dosagem='		|| qt_dosagem_w			|| ds_sep_bv_w ||
					'ie_unid_vel_inf='	|| ie_unid_vel_inf_w		|| ds_sep_bv_w ||
					'qt_temp_solucao='	|| qt_temp_solucao_w		|| ds_sep_bv_w ||
					'qt_dose_ataque='	|| qt_dose_ataque_w		|| ds_sep_bv_w ||
					'ds_orientacao='	|| ds_orientacao_w		|| ds_sep_bv_w ||
					'ie_lavar_linhas='	|| ie_lavar_linhas_w		|| ds_sep_bv_w ||
					'ie_tidal='		|| ie_tidal_w			|| ds_sep_bv_w ||
					'ie_ultima_bolsa='	|| ie_ultima_bolsa_w		|| ds_sep_bv_w ||
					'nr_etapas='		|| nr_etapas_w			|| ds_sep_bv_w ||
					'pr_concentracao='	|| pr_concentracao_w		|| ds_sep_bv_w ||
					'qt_tempo_drenagem='	|| qt_tempo_drenagem_w		|| ds_sep_bv_w ||
					'qt_tempo_infusao='	|| qt_tempo_infusao_w		|| ds_sep_bv_w ||
					'qt_tempo_permanencia='	|| qt_duracao_ciclo_w	|| ds_sep_bv_w ||
					'qt_volume='		|| qt_volume_w			|| ds_sep_bv_w ||
					'nr_sequencia='		|| nr_sequencia_w		|| ds_sep_bv_w;

		sql_w := 'update	cpoe_dialise '		||
			 'set		nr_seq_protocolo'	|| ds_num_atrib_w || '	= :nr_seq_protocolo, ' ||
			 '		ie_tipo_solucao'	|| ds_num_atrib_w || '	= :ie_tipo_solucao, ' ||
			 '		ie_bomba_infusao'	|| ds_num_atrib_w || '	= :ie_bomba_infusao, ' ||
			 '		qt_solucao_total'	|| ds_num_atrib_w || '	= :qt_solucao_total, ' ||
			 '		qt_dosagem'		|| ds_num_atrib_w || '	= :qt_dosagem, ' ||
			 '		ie_unid_vel_inf'	|| ds_num_atrib_w || '	= :ie_unid_vel_inf, ' ||
			 '		qt_temp_solucao'	|| ds_num_atrib_w || '	= :qt_temp_solucao, ' ||
			 '		qt_dose_ataque'		|| ds_num_atrib_w || '	= :qt_dose_ataque, ' ||
			 '		ds_orientacao'		|| ds_num_atrib_w || '	= :ds_orientacao, ' ||
			 '		ie_lavar_linhas'	|| ds_num_atrib_w || '	= :ie_lavar_linhas, ' ||
			 '		ie_tidal'		|| ds_num_atrib_w || '	= :ie_tidal, ' ||
			 '		ie_ultima_bolsa'	|| ds_num_atrib_w || '	= :ie_ultima_bolsa, ' ||
			 '		nr_etapas'		|| ds_num_atrib_w || '	= :nr_etapas, ' ||
			 '		pr_concentracao'	|| ds_num_atrib_w || '	= :pr_concentracao, ' ||
			 '		qt_tempo_drenagem'	|| ds_num_atrib_w || '	= :qt_tempo_drenagem, ' ||
			 '		qt_tempo_infusao'	|| ds_num_atrib_w || '	= :qt_tempo_infusao, ' ||
			 '		qt_tempo_permanencia'	|| ds_num_atrib_w || '	= :qt_tempo_permanencia, ' ||
			 '		qt_volume_sol'		|| ds_num_atrib_w || '	= :qt_volume ' ||
			 'where		nr_sequencia					= :nr_sequencia';

		exec_sql_dinamico_bv('TASY', sql_w, ds_parametros_w);

		count_component_w := 0;
		
		open component_solution_cursor;
		loop
		fetch component_solution_cursor into
			cd_material_w,
			cd_unidade_medida_dose_w,
			qt_dose_w;
		exit when component_solution_cursor%notfound;
			begin

			count_component_w := count_component_w + 1;

			ds_num_atrib_w := '';
			if (count_solucao_w = 1) then
				ds_num_atrib_w := to_char(count_component_w);
			else
				ds_num_atrib_w := count_component_w || '_' || to_char(count_solucao_w);
			end if;

			ds_parametros_w	:= 	'cd_material='			|| cd_material_w		|| ds_sep_bv_w ||
						'qt_dose='			|| qt_dose_w			|| ds_sep_bv_w ||
						'cd_unidade_medida_dose='	|| cd_unidade_medida_dose_w	|| ds_sep_bv_w ||
						'nr_sequencia='			|| nr_sequencia_w		|| ds_sep_bv_w;

			sql_w :='	update	cpoe_dialise '		||
				'	set	cd_mat_soluc'		|| ds_num_atrib_w || '	= :cd_material,' ||
				'		cd_unid_med_dose_sol'	|| ds_num_atrib_w || '	= :cd_unidade_medida_dose,' ||
				'		qt_dose_soluc'		|| ds_num_atrib_w || '	= :qt_dose' ||
				'	where	nr_sequencia					= :nr_sequencia';

			exec_sql_dinamico_bv('TASY', sql_w, ds_parametros_w);

			end;
		end loop;
		close component_solution_cursor;

		end;
	end loop;
	close solution_cursor;	
	
		update	hd_prescricao
		set		nr_seq_dialise_cpoe	= nr_sequencia_w
		where	nr_sequencia		= nr_seq_dialise_w
		and		nr_prescricao		= nr_prescricao_p;
	
	end;
end loop;
close hemodialysis_cursor;

end cpoe_rep_gerar_hemodialise;
/
