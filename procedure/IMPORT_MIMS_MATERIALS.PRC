CREATE OR replace PROCEDURE Import_mims_materials (cd_material_p        VARCHAR2 
, 
nm_usuario_p         VARCHAR2, 
cd_estabelecimento_p NUMBER) 
IS 
  -- seggregated cd_material 
  CURSOR c01 ( 
    cd_material_p VARCHAR) IS 
    SELECT nr_registro 
    FROM   TABLE(lista_pck.Obter_lista(cd_material_p, ',')); 
BEGIN 
    FOR r1 IN c01(cd_material_p) LOOP 
        BEGIN 
            IF ( r1.nr_registro IS NOT NULL ) THEN 
              Import_mims_material(r1.nr_registro, nm_usuario_p, 
              cd_estabelecimento_p); 
            END IF; 
        END; 
    END LOOP; 
 
    COMMIT; 
END import_mims_materials; 
/