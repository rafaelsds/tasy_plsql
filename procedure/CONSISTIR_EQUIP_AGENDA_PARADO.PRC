create or replace
procedure Consistir_equip_agenda_parado(nr_seq_agenda_p	number,
					nm_usuario_p	Varchar2,
					cd_estabelecimento_p	number) is 

					
nr_seq_equipamento_w	number(10,0);
hr_inicio_w		date;
qt_dias_w		number(10,0);
cd_agenda_w		number(10,0);
ie_parado_w		varchar2(1);

dt_inicio_w		date;
dt_fim_w		date;
dt_agenda_w		date;
					
begin

select	max(a.nr_seq_equipamento),
	max(b.hr_inicio),
	nvl(max(a.qt_dias_post_prox_livre),0),
	max(a.cd_agenda)
into	nr_seq_equipamento_w,
	hr_inicio_w,
	qt_dias_w,
	cd_agenda_w
from	agenda a,
	agenda_paciente b
where	a.cd_agenda	= b.cd_agenda
and	b.nr_sequencia	= nr_seq_agenda_p;

if	(nr_seq_equipamento_w is not null) then

	select	decode(count(*),0,'N','S')
	into	ie_parado_w
	from	man_equip_periodo_parado
	where	nr_seq_equipamento = nr_seq_equipamento_w
	and	hr_inicio_w between dt_periodo_inicial and dt_periodo_final;

	if	(ie_parado_w = 'S') and
		(qt_dias_w <> 0) then

		dt_inicio_w	:= trunc(hr_inicio_w);
		dt_fim_w	:= dt_inicio_w + 86399/86400;
		
		select	min(hr_inicio)
		into	dt_agenda_w
		from  	agenda_paciente
		where	hr_inicio between hr_inicio_w and dt_fim_w
		and	cd_agenda		= cd_agenda_w
		and   	ie_status_agenda 	= 'L'
		and	not exists (select	1
				    from	man_equip_periodo_parado x
				    where	hr_inicio between x.dt_periodo_inicial and x.dt_periodo_final);	

		if	(dt_agenda_w is null) then
			for i in 1..qt_dias_w loop
				begin
				dt_inicio_w	:= dt_inicio_w + 1;
				dt_fim_w	:= dt_inicio_w + 86399/86400;
				if	(dt_agenda_w is null) then
									
					gerar_horario_agenda_exame(cd_estabelecimento_p, cd_agenda_w, dt_inicio_w, nm_usuario_p);
					
					select	min(hr_inicio)
					into	dt_agenda_w
					from  	agenda_paciente
					where	hr_inicio between trunc(dt_inicio_w) and dt_fim_w
					and	cd_agenda		= cd_agenda_w
					and   	ie_status_agenda 	= 'L'
					and	not exists (select	1
							    from	man_equip_periodo_parado x
							    where	hr_inicio between x.dt_periodo_inicial and x.dt_periodo_final);	
				end if;
				end;
			end loop;
		end if;

		if	(dt_agenda_w is not null) then
			Wheb_mensagem_pck.exibir_mensagem_abort( 262564 , 'DT_AGENDA='||to_char(dt_agenda_w,'dd/mm/yyyy hh24:mi:ss'));
		else
			Wheb_mensagem_pck.exibir_mensagem_abort(262565);
		end if;

	end if;
end if;
commit;

end Consistir_equip_agenda_parado;
/
