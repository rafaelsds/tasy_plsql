create or replace
procedure dankia_lancamento_material(
										cd_acao_p				varchar2,
										nr_atendimento_p		number,
										cd_local_estoque_p		number,
										cd_material_p			number,
										qt_atual_p				number,
										dt_movimento_p			date,
										cd_fornecedor_p			varchar2,
										nr_prescricao_p			number,
										nr_seq_material_p		number,
										nr_seq_lote_p			number,
										cd_operacao_estoque_p	varchar2,
										ds_erro_p				out	varchar2,
										cd_barras_p				varchar2 default null,
										nm_usuario_p				varchar2 DEFAULT null) is

qt_estoque_w			number(15,4);
qt_material_w			number(15,4);
qt_devolvida_w			number(15,4);
cd_acao_w			varchar2(1) := '1';
cd_fornec_consignado_w		varchar2(14);
cd_setor_atendimento_w		number(5);
ie_cobra_paciente_w		varchar2(1);
cd_unidade_medida_w		varchar2(30);
cd_convenio_w			number(5);
cd_categoria_w			varchar2(10);
nr_doc_convenio_w		varchar2(20);
ie_tipo_guia_w			varchar2(2);
cd_senha_w			varchar2(20);
dt_entrada_unidade_w		date;
cd_centro_custo_w		number(8);
cd_conta_contabil_w		varchar2(20);
cd_operacao_estoque_w		number(3);
nr_seq_atepacu_w		number(10);
ie_consignado_w			varchar2(1);
ie_atualizar_consig_w		varchar2(1);
ds_erro_w			varchar2(255);
nr_sequencia_w			number(10);

/* parametros converte_codigo_barras */
cd_material_w			number(6);
qt_mat_barras_w			number(13,4);
nr_seq_lote_w			number(10);
nr_seq_lote_agrup_w		number(10);
cd_kit_mat_w			number(10);
ds_validade_w			varchar2(255);
ds_material_w			varchar2(255);
cd_unid_med_w			varchar2(30);
nr_etiqueta_lp_w		varchar2(255);
/* parametros converte_codigo_barras */

nr_seq_tipo_baixa_w		number(10);
qt_total_dispensar_item_w	number(10);
qt_total_item_conta_w		number(10);
qt_existe_w			number(10);
nr_seq_lote_ap_w		number(10);
cd_motivo_baixa_int_w		number(3);
ie_atualiza_estoque_w		tipo_baixa_prescricao.ie_atualiza_estoque%type;
ie_conta_paciente_w		tipo_baixa_prescricao.ie_conta_paciente%type;
ie_gerar_transf_lote_w		parametros_farmacia.ie_gerar_transf_lote%type;
ie_transf_atend_lote_w		setor_atendimento.ie_transf_atend_lote%type;
cd_local_estoque_ent_w		local_estoque.cd_local_estoque%type;
cd_local_estoque_w		local_estoque.cd_local_estoque%type;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
qt_material_ww				number(13,4);

begin
nr_atendimento_w := nr_atendimento_p;
cd_local_estoque_w := cd_local_estoque_p;
cd_material_w := cd_material_p;


if	(nr_seq_lote_p is not null) then
	select	nvl(max(cd_tipo_baixa),0),
		nvl(max(cd_setor_atendimento),0)
	into	cd_motivo_baixa_int_w,
		cd_setor_atendimento_w
	from	ap_lote
	where	nr_sequencia = nr_seq_lote_p;
elsif	(nr_prescricao_p is not null) then
	select	nvl(max(cd_setor_atendimento),0),
		nvl(max(nr_atendimento),0),
		nvl(max(cd_estabelecimento),0)
	into	cd_setor_atendimento_w,
		nr_atendimento_w,
		cd_estabelecimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
end if;

if	(nvl(cd_estabelecimento_w,0) = 0) then
	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from 	local_estoque
	where 	cd_local_estoque = cd_local_estoque_p;
end if;

if	(nvl(cd_setor_atendimento_w,0) = 0) and (nvl(nr_atendimento_w,0) > 0) then
	cd_setor_atendimento_w := obter_setor_atendimento(nr_atendimento_w);
elsif	(nvl(cd_setor_atendimento_w,0) = 0) then
	-- 'Nao encontrado setor de atendimento para o local de estoque!';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(278817);
end if;

if	(nvl(cd_setor_atendimento_w,0) > 0) then
	begin
	select	nr_sequencia
	into	nr_sequencia_w
	from	dis_regra_setor
	where	cd_setor_atendimento = cd_setor_atendimento_w;
	
	select	max(cd_local_estoque)
	into	cd_local_estoque_w
	from	dis_regra_local_setor
	where	nr_seq_dis_regra_setor = nr_sequencia_w
	and		cd_local_estoque = cd_local_estoque_w;
	
	if	(cd_local_estoque_w is null) then
		cd_local_estoque_w := cd_local_estoque_p;
	end if;
	
	exception
	when others then
		cd_local_estoque_w := cd_local_estoque_p;
	end;
end if;

if	(nvl(cd_local_estoque_w,0) = 0) then
	select	nvl(max(cd_local_estoque),0)
	into	cd_local_estoque_w
	from	parametros_farmacia
	where	cd_estabelecimento = cd_estabelecimento_w;
	
	if	(nvl(cd_local_estoque_w,0) = 0) then
		select	nvl(max(cd_local_estoque),0)
		into	cd_local_estoque_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_w;
	end if;
end if;

qt_material_w := qt_atual_p;

if	(cd_acao_p = 'I') then
	cd_acao_w := '1';
elsif	(cd_acao_p = 'E') then
	cd_acao_w := '2';
end if;

if	(qt_material_w = 0) then
	-- 'Quantidade de contagem igual ao saldo em estoque!',1,255);
	ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(278822),1,255);
end if;

if	(cd_barras_p is not null) then
	converte_codigo_barras(	cd_barras_p,		cd_estabelecimento_w,	null,
				'N',			cd_material_w,		qt_material_ww,
				nr_seq_lote_w,		nr_seq_lote_agrup_w,	cd_kit_mat_w,
				ds_validade_w,		ds_material_w,		cd_unid_med_w,
				nr_etiqueta_lp_w,	ds_erro_w);
end if;

begin
select	cd_unidade_medida_estoque,
	ie_cobra_paciente,
	nvl(ie_consignado,'0')
into	cd_unidade_medida_w,
	ie_cobra_paciente_w,
	ie_consignado_w
from	material
where	cd_material = cd_material_w;
exception
when others then
	-- 'Nao encontrado cadastro do material no Tasy!';
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(278819);
end;

if	(ie_consignado_w <> '0') and
	(cd_fornecedor_p is not null) then
	begin
	ie_atualizar_consig_w := 'S';
	
	begin
	select	cd_cgc
	into	cd_fornec_consignado_w
	from	pessoa_juridica
	where	cd_cgc = cd_fornecedor_p;
	exception
	when others then
		-- 'Nao encontrado cadastro do fornecedor no Tasy! CGC: ' || cd_fornecedor_p,1,255);
		ds_erro_w := substr(WHEB_MENSAGEM_PCK.get_texto(278821) || cd_fornecedor_p,1,255);
	end;
	end;
else
	begin
	ie_atualizar_consig_w := 'N';
	cd_fornec_consignado_w := null;
	end;
end if;


if	(ds_erro_w is null) then
	begin
	if	(nvl(nr_atendimento_w,0) > 0) then
		begin
		
		
		obter_convenio_execucao(nr_atendimento_w, sysdate, cd_convenio_w, cd_categoria_w, nr_doc_convenio_w, ie_tipo_guia_w, cd_senha_w);
		nr_seq_atepacu_w := obter_atepacu_paciente(nr_atendimento_w,'A');
		
		select 	max(a.dt_entrada_unidade)
		into	dt_entrada_unidade_w
		from	atend_paciente_unidade a
		where	a.nr_seq_interno = nr_seq_atepacu_w;
		
		
		if	(nvl(cd_motivo_baixa_int_w,0) > 0) then
			select 	nvl(max(nr_sequencia),0),
				nvl(max(ie_atualiza_estoque),'S'),
				nvl(max(ie_conta_paciente),'S')
			into	nr_seq_tipo_baixa_w,
				ie_atualiza_estoque_w,
				ie_conta_paciente_w
			from 	tipo_baixa_prescricao
			where	ie_situacao = 'A'
			and 	cd_tipo_baixa = cd_motivo_baixa_int_w
			and 	ie_prescricao_devolucao = 'P';
		else
			select	max(nr_sequencia)
			into	nr_seq_tipo_baixa_w
			from 	tipo_baixa_prescricao
			where	ie_situacao = 'A'
			and	ie_tipo_padrao = 'S'
			and 	ie_prescricao_devolucao = 'P';
			
			select 	nvl(max(ie_atualiza_estoque),'S'),
				nvl(max(ie_conta_paciente),'S')
			into	ie_atualiza_estoque_w,
				ie_conta_paciente_w
			from 	tipo_baixa_prescricao
			where	nr_sequencia = nr_seq_tipo_baixa_w;
		end if;
		
		
		if	(cd_acao_w = 2) then
			begin
			qt_devolvida_w	:= qt_material_w;
			qt_material_w	:= (qt_material_w * -1);
			
			if	(nvl(cd_motivo_baixa_int_w,0) > 0) then
				select 	nvl(nr_sequencia,0)
				into	nr_seq_tipo_baixa_w
				from 	tipo_baixa_prescricao
				where	ie_situacao = 'A'
				and 	cd_tipo_baixa = cd_motivo_baixa_int_w
				and 	ie_prescricao_devolucao = 'D';
				
				select 	nvl(ie_atualiza_estoque,'S'),
					nvl(ie_conta_paciente,'S')
				into	ie_atualiza_estoque_w,
					ie_conta_paciente_w
				from 	tipo_baixa_prescricao
				where	nr_sequencia = nr_seq_tipo_baixa_w;
			else
				select	max(nr_sequencia)
				into	nr_seq_tipo_baixa_w
				from 	tipo_baixa_prescricao
				where	ie_situacao = 'A'
				and	ie_tipo_padrao = 'S'
				and 	ie_prescricao_devolucao = 'D';
				
				select 	nvl(max(ie_atualiza_estoque),'S'),
					nvl(max(ie_conta_paciente),'S')
				into	ie_atualiza_estoque_w,
					ie_conta_paciente_w
				from 	tipo_baixa_prescricao
				where	nr_sequencia = nr_seq_tipo_baixa_w;
			end if;
			end;
		end if;
		
		if	(ie_cobra_paciente_w = 'S') and (ie_conta_paciente_w = 'S') then
			begin

			select	material_atend_paciente_seq.nextval
			into	nr_sequencia_w
			from	dual;
						
			insert into material_atend_paciente (
				nr_sequencia,
				nr_atendimento,
				dt_entrada_unidade,
				cd_material,
				dt_atendimento,
				cd_unidade_medida,
				qt_material,
				dt_atualizacao,
				nm_usuario,
				cd_acao,
				cd_setor_atendimento,
				nr_seq_atepacu,
				cd_material_prescricao,
				cd_material_exec,
				ie_via_aplicacao,
				dt_prescricao,
				nr_prescricao,
				nr_sequencia_prescricao,
				cd_cgc_fornecedor,
				qt_executada,
				nr_cirurgia,
				cd_local_estoque,
				vl_unitario,
				qt_ajuste_conta,
				ie_valor_informado,
				ie_guia_informada,
				ie_auditoria,
				nm_usuario_original,
				cd_situacao_glosa,
				cd_convenio,
				cd_categoria,
				nr_doc_convenio,
				ie_tipo_guia,
				nr_seq_lote_fornec,
				cd_senha,
				dt_conta,
				nr_seq_kit_estoque,
				qt_devolvida,
				cd_funcao,
				nr_seq_tipo_baixa,
				nr_seq_lote_ap)
			values(	nr_sequencia_w,
				nr_atendimento_w,
				dt_entrada_unidade_w,
				cd_material_w,
				sysdate,
				cd_unidade_medida_w,
				qt_material_w,
				sysdate,
				substr(nvl(nm_usuario_p, 'DANKIA'),1,15),
				cd_acao_w,
				cd_setor_atendimento_w,
				nr_seq_atepacu_w, 
				cd_material_w,
				cd_material_w,
				null,
				null,
				decode(nvl(nr_prescricao_p,0),0,null,nr_prescricao_p),
				decode(nvl(nr_seq_material_p,0),0,null,nr_seq_material_p),
				cd_fornec_consignado_w,
				qt_material_w,
				null,
				cd_local_estoque_w,
				0,
				0,
				'N',
				'N',
				'N',
				substr(nvl(nm_usuario_p, 'DANKIA'),1,15),
				0,
				cd_convenio_w, 
				cd_categoria_w, 
				nr_doc_convenio_w, 
				ie_tipo_guia_w,
				decode(nvl(nr_seq_lote_w,0),0,null,nr_seq_lote_w),
				cd_senha_w,
				sysdate,
				null,
				qt_devolvida_w,
				147,
				nr_seq_tipo_baixa_w,
				decode(nvl(nr_seq_lote_p,0),0,null,nr_seq_lote_p));
			
			atualiza_preco_material(nr_sequencia_w,substr(nvl(nm_usuario_p, 'DANKIA'),1,15));
			
			
			
			if	(nr_prescricao_p > 0) and (nr_seq_material_p > 0) then
				select	sum(qt_total_dispensar)
				into	qt_total_dispensar_item_w
				from	prescr_material
				where	nr_prescricao = nr_prescricao_p
				and	nr_sequencia = nr_seq_material_p;
				
				select 	sum(qt_material)
				into	qt_total_item_conta_w
				from 	material_atend_paciente
				where 	nr_atendimento	= nr_atendimento_w
				and 	nr_prescricao	= nr_prescricao_p
				and 	nr_sequencia_prescricao	= nr_seq_material_p;
				
				select	count(*)
				into	qt_existe_w
				from	prescr_medica
				where	nr_prescricao = nr_prescricao_p
				and	dt_liberacao is null;
				
				-- Baixa o item na prescricao quando a quantidade da conta e maior ou igual a quantidade a ser dispensada.
				if	(qt_existe_w = 0) and (qt_total_dispensar_item_w <= qt_total_item_conta_w) then
					update 	prescr_material
					set 	cd_motivo_baixa = cd_motivo_baixa_int_w,
						dt_baixa 	= sysdate,
						nm_usuario	= substr(nvl(nm_usuario_p, 'DANKIA'),1,15)
					where 	nr_prescricao 	= nr_prescricao_p
					and 	nr_sequencia	= nr_seq_material_p;
				end if;
			end if;
			
			-- Coloca lote como atendido
			if	(cd_acao_w = 1) and (nvl(nr_seq_lote_p,0) > 0) then
				
				select	count(*)
				into	qt_existe_w
				from	ap_lote_item
				where	dt_controle is null
				and	cd_material = cd_material_w
				and	nr_seq_lote = nr_seq_lote_p;
				
				if	(qt_existe_w > 0) then
					update	ap_lote_item
					set	dt_controle = sysdate,
						dt_atualizacao = sysdate,
						nm_usuario = substr(nvl(nm_usuario_p, 'DANKIA'),1,15)
					where	nr_seq_lote = nr_seq_lote_p
					and	cd_material = cd_material_w;
				end if;
				
				select	count(*)
				into	qt_existe_w
				from	ap_lote_item
				where	nr_seq_lote = nr_seq_lote_p
				and	dt_controle is null
				and	ie_prescrito = 'S'
				AND DT_SUPENSAO IS NULL;
				
				if	(qt_existe_w = 0) then
					update	ap_lote	
					set	dt_atend_farmacia = sysdate,
						nm_usuario_atend = substr(nvl(nm_usuario_p, 'DANKIA'),1,15),
						ie_status_lote   = 'A'
					where 	nr_sequencia = nr_seq_lote_p;
				end if;
	
			end if;
			
			end;
			
		elsif	(ie_atualiza_estoque_w = 'S') then
			begin
			gerar_prescricao_estoque(
				cd_estabelecimento_w,
				nr_atendimento_w,
				dt_entrada_unidade_w,
				cd_material_w,
				sysdate,
				cd_acao_w,
				cd_local_estoque_w,
				qt_material_w,
				cd_setor_atendimento_w,
				cd_unidade_medida_w,
				substr(nvl(nm_usuario_p, 'DANKIA'),1,15),
				'I',
				nr_prescricao_p,
				null,
				null,
				0,
				cd_fornec_consignado_w,
				null,
				nr_seq_lote_w,
				null,
				null,
				nr_seq_lote_p,
				null,
				null,
				null);
			
			-- Coloca lote como atendido
			if	(cd_acao_w = 1) and (nvl(nr_seq_lote_p,0) > 0) then
				
				select	count(*)
				into	qt_existe_w
				from	ap_lote_item
				where	dt_controle is null
				and	cd_material = cd_material_w
				and	nr_seq_lote = nr_seq_lote_p;
				
				if	(qt_existe_w > 0) then
					update	ap_lote_item
					set	dt_controle = sysdate,
						dt_atualizacao = sysdate,
						nm_usuario = substr(nvl(nm_usuario_p, 'DANKIA'),1,15)
					where	nr_seq_lote = nr_seq_lote_p
					and	cd_material = cd_material_w;
				end if;
				
				select	count(*)
				into	qt_existe_w
				from	ap_lote_item
				where	nr_seq_lote = nr_seq_lote_p
				and	dt_controle is null
				and	ie_prescrito = 'S'
				AND DT_SUPENSAO IS NULL;
				
				if	(qt_existe_w = 0) then
					update	ap_lote	
					set	dt_atend_farmacia = sysdate,
						nm_usuario_atend = substr(nvl(nm_usuario_p, 'DANKIA'),1,15),
						ie_status_lote   = 'A'
					where 	nr_sequencia = nr_seq_lote_p;
				end if;
			end if;
			end;
		end if;
		end;
	else
		begin
		if	(ie_atualizar_consig_w = 'N') then
			select	max(cd_operacao_estoque)
			into	cd_operacao_estoque_w
			from	operacao_estoque
			where	ie_situacao = 'A'
			and	ie_tipo_requisicao = 5
			and	nvl(ie_consignado,0) = 0
			and	ie_atualiza_estoque = 'S'
			and	ie_entrada_saida = decode(cd_acao_w,'1','S','E');
		else
			select	max(cd_operacao_estoque)
			into	cd_operacao_estoque_w
			from	operacao_estoque
			where	ie_situacao = 'A'
			and	ie_tipo_requisicao = 5
			and	nvl(ie_consignado,0) = 7
			and	ie_entrada_saida = decode(cd_acao_w,'1','S','E');
		end if;
		
		if	(nvl(cd_operacao_estoque_p,0) > 0) then
			cd_operacao_estoque_w := to_number(substr(cd_operacao_estoque_p,1,3));
		end if;
		
		cd_acao_w := '1';
		
		select	max(cd_centro_custo)
		into	cd_centro_custo_w
		from	setor_atendimento
		where	cd_setor_atendimento = cd_setor_atendimento_w;
		
		define_conta_material(
			cd_estabelecimento_w,
			cd_material_w,
			'2',
			null,
			cd_setor_atendimento_w,
			null,
			null,
			null,
			null,
			null,
			cd_local_estoque_w,
			null,
			sysdate,
			cd_conta_contabil_w,
			cd_centro_custo_w,
			null);
			
		insert into movimento_estoque(
			nr_movimento_estoque,
			cd_estabelecimento,
			cd_local_estoque,
			dt_movimento_estoque,
			cd_operacao_estoque,
			cd_acao,
			cd_material,
			dt_mesano_referencia,
			qt_movimento,
			dt_atualizacao,
			nm_usuario,
			ie_origem_documento,
			nr_documento,
			nr_sequencia_item_docto,
			cd_unidade_medida_estoque,
			cd_setor_atendimento,
			qt_estoque,
			cd_centro_custo,
			cd_unidade_med_mov,
			cd_fornecedor,
			ds_observacao,
			nr_seq_tab_orig,
			nr_seq_lote_fornec,
			cd_lote_fabricacao,
			dt_validade,
			nr_atendimento,
			nr_prescricao,
			nr_receita,
			cd_conta_contabil,
			nr_ordem_compra,
			nr_item_oci,
			nr_lote_ap,
			nr_lote_producao,
			cd_funcao)
		values(	movimento_estoque_seq.nextval,
			cd_estabelecimento_w,
			cd_local_estoque_w,
			sysdate,
			cd_operacao_estoque_w,
			'1',
			cd_material_w,
			sysdate,
			qt_material_w,
			sysdate,
			substr(nvl(nm_usuario_p, 'DANKIA'),1,15),
			'3',
			null,
			null,
			cd_unidade_medida_w,
			cd_setor_atendimento_w,
			qt_material_w,
			cd_centro_custo_w,
			cd_unidade_medida_w,
			cd_fornec_consignado_w,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			cd_conta_contabil_w,
			null,
			null,
			null,
			null,
			147);
		end;
	end if;
	end;
	
end if;

ds_erro_p := ds_erro_w;
end dankia_lancamento_material;
/
