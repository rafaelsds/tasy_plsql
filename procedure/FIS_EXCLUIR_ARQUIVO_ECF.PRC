create or replace
procedure fis_excluir_arquivo_ecf(		
				nr_sequencia_p		number,
				nm_usuario_p		varchar2) is 

begin

delete	from fis_ecf_arquivo
where	nr_seq_controle_ecf  = nr_sequencia_p;

delete	from fis_controle_ecf
where	nr_sequencia = nr_sequencia_p;

commit;

end fis_excluir_arquivo_ecf;
/
