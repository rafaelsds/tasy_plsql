create or replace procedure TREND_UPLOAD_HL7 (
    ls_sequencies_p in varchar2,
    ds_error_msg_p out varchar2
) is 

cd_pessoa_fisica_w w_atend_vs_integr_hl7.cd_pessoa_fisica%type;
nr_atendimento_w w_atend_vs_integr_hl7.nr_atendimento%type;
ds_lista_param_w w_atend_vs_integr_hl7.ds_lista_param%type;
ds_parametros_w w_atend_vs_integr_hl7.ds_lista_param%type;
dt_hora_receb_w w_atend_vs_integr_hl7.dt_hora_receb%type;
nm_integracao_w atendimento_sinal_vital.nm_usuario%type;
ie_importado_w w_atend_vs_integr_hl7.ie_importado%type;
nm_usuario_w w_atend_vs_integr_hl7.nm_usuario%type;
nr_seq_sinal_vital_w char(1) := 'N';

TYPE campos IS RECORD ( nr_sequencia number );
TYPE Vetor IS TABLE OF campos INDEX BY BINARY_INTEGER;
vetor_w Vetor;
indice INTEGER;

nr_count_emp_hl7_w number := 0;
nr_count_emp_integ_hl7_w number := 0;

begin
    nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
    ds_parametros_w := ls_sequencies_p;
    indice := 0;
    WHILE (LENGTH(ds_parametros_w) > 0) LOOP
        BEGIN
        indice	:= indice+1;
        IF	(INSTR(ds_parametros_w,';')	>0)  THEN
            vetor_w(indice).nr_sequencia	:= SUBSTR(ds_parametros_w,1,INSTR(ds_parametros_w,';')-1 );
            ds_parametros_w	:= SUBSTR(ds_parametros_w,INSTR(ds_parametros_w,';')+1,40000);
        ELSE
            vetor_w(indice).nr_sequencia	:=SUBSTR(ds_parametros_w,1,LENGTH(ds_parametros_w) - 1);
            ds_parametros_w	:= NULL;
        END IF;
        END;
    END LOOP;


    FOR j IN 1..vetor_w.COUNT LOOP
        BEGIN
        select cd_pessoa_fisica, dt_hora_receb, ds_lista_param, nm_usuario, nr_atendimento, ie_importado
        into
            cd_pessoa_fisica_w,
            dt_hora_receb_w,
            ds_lista_param_w,
            nm_integracao_w,
            nr_atendimento_w,
            ie_importado_w
        from w_atend_vs_integr_hl7 
        where nr_sequencia = vetor_w(j).nr_sequencia;
        
        SELECT count(nr_sequencia)
        INTO nr_count_emp_hl7_w
        FROM empresa_integracao
        WHERE UPPER(nm_empresa) = UPPER(nm_integracao_w);
        
        select count(b.nr_sequencia) 
        into nr_count_emp_integ_hl7_w
        from empresa_integracao a 
        inner join emp_integracao_de_para_hl7 b on b.nr_seq_emp_integracao = a.nr_sequencia
        WHERE UPPER(a.nm_empresa) = UPPER(nm_integracao_w);
        
        if (ie_importado_w <> 'S' and nr_count_emp_hl7_w = 1 and nr_count_emp_integ_hl7_w > 0) then
            gerar_sinal_vital_hl7(cd_pessoa_fisica_w, dt_hora_receb_w, ds_lista_param_w, null, nm_integracao_w, 'S');
            update w_atend_vs_integr_hl7 set dt_atualizacao = sysdate, ie_importado = 'S' where nr_sequencia = vetor_w(j).nr_sequencia;
            commit;
        elsif (nr_count_emp_hl7_w > 1) then
            ds_error_msg_p := obter_desc_expressao(1060526);
            exit;
        elsif (nr_count_emp_hl7_w = 0 OR nr_count_emp_integ_hl7_w = 0) then
            ds_error_msg_p := obter_desc_expressao(1060522);
            exit;
        end if;
        END;
    END LOOP;
end TREND_UPLOAD_HL7;
/
