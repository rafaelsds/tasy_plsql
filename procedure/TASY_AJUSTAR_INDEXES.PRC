create or replace 
procedure tasy_ajustar_indexes is

nm_tablespace_origem_w 	varchar2(30);
nm_tablespace_destino_w varchar2(30);
nm_index_w		varchar2(30);
ds_comando_w		varchar2(1000);


/*Retorna o Nome dos Indexes errados*/
cursor c01 is
	select  index_name
	from  	user_indexes
	where	tablespace_name = nm_tablespace_origem_w ;
BEGIN


/*Retorna as TableSpace com indexes errados*/
select  distinct max(tablespace_name)
into	nm_tablespace_origem_w
from  	user_segments
where 	segment_type = 'INDEX'
and	tablespace_name like '%_DATA';

if (nm_tablespace_origem_w is not null) then

	nm_tablespace_destino_w := substr(nm_tablespace_origem_w,1,instr(nm_tablespace_origem_w,'_DATA')) || 

'INDEX';

	OPEN C01;
	LOOP
	FETCH C01 into
		nm_index_w;
	exit 	when c01%notfound;
     		BEGIN
		
		ds_comando_w := 'ALTER INDEX ' || nm_index_w || ' REBUILD TABLESPACE ' || nm_tablespace_destino_w ;
		exec_sql_dinamico('MOVENDO INDEX -> ' || nm_index_w,ds_comando_w);
		END;
	END LOOP;
	CLOSE C01;
end if;

end tasy_ajustar_indexes;
/
