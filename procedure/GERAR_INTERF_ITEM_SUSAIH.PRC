create or replace
procedure gerar_interf_item_susaih(	nr_interno_conta_p		number,
				nm_usuario_p		varchar2) is

nr_sequencia_w			number(10);
cd_codigo_executor_w		varchar2(10);
ie_doc_medico_prof_w		number(1);
nr_cpf_executor_w			varchar2(11);
cd_cbo_executor_w		varchar2(6);
ie_doc_prestador_w		number(1)		:= 0;
ie_doc_executor_w			number(1);
ie_doc_executor_ww			number(1);
cd_cns_executor_w		varchar2(15);
cd_cns_cpf_executor_w		varchar2(15);
ie_funcao_w			number(1);
cd_cgc_prestador_w		varchar2(14);
cd_cnes_w			varchar2(7);
cd_procedimento_w		number(10);
ie_origem_proced_w		number(10);
qt_procedimento_w			number(6);
dt_competencia_w			date;
dt_comp_interf_w			varchar2(6);
nr_aih_w				number(13);
nr_seq_proc_w			number(10);
ds_registro_w			varchar2(711);
cd_doc_executor_w		varchar2(15);
qt_registro_w			number(10)	:= 0;
nr_seq_reg_proc_w			number(3)	:= 1;
cd_pessoa_fisica_w		varchar2(10);
nr_seq_protocolo_w		number(10);
nr_linha_proc_w			number(10)	:= 1;
cd_estabelecimento_w		number(4);
cd_cgc_estab_w			varchar2(14);
cd_forma_organizacao_w		number(6);
cd_cnes_prestador_w		varchar2(20);
ie_ordem_w			number(5);
nr_seq_proc_princ_w		number(10);
nr_seq_ordem_w			number(10);
nr_seq_ordem_princ_w		number(10);
cd_convenio_w			number(5);
ie_tipo_servico_w			number(3);
ie_credenciamento_w		varchar2(3);
qt_registros_w			number(5)		:= 0;
cd_grupo_w			number(2);
nr_seq_regra_w			number(10);
ie_exclui_medico_w			varchar2(1)	:= 'N';
ie_exclui_cnes_w			varchar2(1)	:= 'N';
qt_reg_w				number(10,0)	:= 0;
qt_proc_interf_w			number(10,0)	:= 0;
cd_cbo_exec_exp_w		varchar2(6);
cd_exec_exp_w			varchar2(10);
ie_ignora_participou_sus_w		varchar2(1)	:= 'N';
ie_exporta_cnes_w			varchar2(1)	:= 'N';
ie_exporta_cnes_hosp_w		varchar2(1);
ie_exclui_procedimento_w		varchar2(1)	:= 'N';
cd_cgc_prestador_exp_w		varchar2(14);
ie_exporta_cnes_setor_w		varchar2(1)	:= 'N';
cd_setor_atendimento_w		number(5);
dt_inicial_w			date;
dt_final_w			date;
cd_procedimento_real_w		number(10);
dt_alta_w				date;
ie_forma_envio_data_proc_w		varchar2(15)	:= 'P';
dt_mesano_referencia_w			date;
qt_reg_cnes_proc_w		number(5)		:= 0;
cd_cnes_regra_proc_w		varchar2(20);
cd_motivo_cobranca_w		number(2);
ie_identificacao_aih_w		varchar2(2);
cd_servico_w			varchar2(3);
cd_servico_classif_w		varchar2(3);
ie_exp_cnpj_fornec_fabric_w	varchar2(1)	:= nvl(sus_obter_parametro_aih('IE_EXP_CNPJ_FORNEC_FABRIC', obter_estab_conta_paciente(nr_interno_conta_p)),'N');
cd_cgc_fabricante_w		varchar2(14);
qt_reg_prof_vinc_w		number(10) := 0;
ie_considerar_cns_ant_w		parametro_faturamento.ie_considerar_cns_ant%type;
nr_cartao_nac_sus_ant_w		cnes_profissional.nr_cartao_nac_sus_ant%type;
ie_manter_doc_executor_w	sus_parametros_aih.ie_manter_doc_executor%type;
ie_regra_agrup_proc_w		varchar2(15) := 'N';
ie_exp_cnes_exc_med_w		varchar2(15) := 'N';
nr_seq_partic_w             procedimento_participante.nr_seq_partic%type;
dt_procedimento_w 			procedimento_paciente.dt_procedimento%type;

/* Obter os procedimentos Secudarios/Especiais no maximo 10*/
cursor c01 is
select	a.nr_sequencia nr_seq_proc,
	nvl(a.cd_medico_executor, a.cd_pessoa_fisica) cd_codigo_executor,
	nvl(substr(obter_dados_pf(nvl(a.cd_medico_executor, a.cd_pessoa_fisica),'CPF'),1,11),0) nr_cpf_executor,
	nvl(substr(obter_dados_pf(nvl(a.cd_medico_executor, a.cd_pessoa_fisica),'CNS'),1,15),0) cd_cns_executor,
	1 ie_funcao_equipe,
	nvl(a.cd_cgc_prestador,0) cd_cgc_prestador,
	nvl(a.cd_procedimento,0) cd_procedimento,
	nvl(a.ie_origem_proced, 7) ie_origem_proced,
	nvl(a.qt_procedimento,1) qt_procedimento,
	nvl(a.dt_procedimento,sysdate) dt_competencia,
	nvl(a.cd_cbo, nvl(sus_obter_cbo_medico(nvl(a.cd_medico_executor,a.cd_pessoa_fisica), a.cd_procedimento, a.dt_procedimento,0 ),'0')) cd_cbo_executor,
	nvl(a.ie_doc_executor, 5) ie_doc_executor,
	a.nr_seq_proc_princ nr_seq_proc_princ,
	sus_obter_seq_ordem(a.nr_sequencia) nr_seq_ordem,
	sus_obter_seq_ordem_princ(a.nr_sequencia) nr_seq_ordem_princ,
	sus_ordenar_proc_aih(a.nr_sequencia) ie_ordem,
	a.cd_setor_atendimento cd_setor_atendimento,
	nvl(sus_obter_dados_servico(a.nr_seq_servico,'C'),'0') cd_servico,
	nvl(sus_obter_desc_classif(a.nr_seq_servico_classif,null,'C'),0) cd_servico_classif,
	null nr_seq_partic,
	a.dt_procedimento dt_procedimento
from	procedimento_paciente a
where	a.qt_procedimento	> 0
and	a.cd_procedimento <> 415020050
and	not exists (	select 1 
			from sus_regra_proc x
			where	x.nr_seq_regra in (7,11,13)
			and	x.cd_procedimento = a.cd_procedimento
			and	x.ie_origem_proced = a.ie_origem_proced)
and	a.ie_origem_proced	= 7
and	a.cd_motivo_exc_conta is null			
and	a.nr_interno_conta	= nr_interno_conta_p
union
select	a.nr_sequencia,
	b.cd_pessoa_fisica,
	nvl(substr(obter_dados_pf(b.cd_pessoa_fisica,'CPF'),1,11),0) nr_cpf_executor,
	nvl(substr(obter_dados_pf(b.cd_pessoa_fisica,'CNS'),1,15),0) cd_cns_executor,
	nvl(sus_obter_indicador_equipe(b.ie_funcao),0) ie_funcao_equipe,
	nvl(nvl(b.cd_cgc,a.cd_cgc_prestador),0),
	nvl(a.cd_procedimento,0) cd_procedimento,
	nvl(a.ie_origem_proced, 7) ie_origem_proced,
	nvl(a.qt_procedimento,1),
	nvl(a.dt_procedimento,sysdate),
	nvl(b.cd_cbo, nvl(sus_obter_cbo_medico(b.cd_pessoa_fisica, a.cd_procedimento, a.dt_procedimento,nvl(sus_obter_indicador_equipe(b.ie_funcao),0)),'0')),
	nvl(b.ie_doc_executor,nvl(a.ie_doc_executor, 5)),
	a.nr_seq_proc_princ,
	sus_obter_seq_ordem(a.nr_sequencia) nr_seq_ordem,
	sus_obter_seq_ordem_princ(a.nr_sequencia) nr_seq_ordem_princ,
	sus_ordenar_proc_aih(a.nr_sequencia) ie_ordem,
	a.cd_setor_atendimento,
	nvl(sus_obter_dados_servico(a.nr_seq_servico,'C'),'0'),
	nvl(sus_obter_desc_classif(a.nr_seq_servico_classif,null,'C'),0),
    b.nr_seq_partic,
	a.dt_procedimento dt_procedimento
from	procedimento_participante	b,
	procedimento_paciente		a
where	a.qt_procedimento	> 0
and	(((nvl(b.ie_participou_sus,'S')	= 'S') and (ie_ignora_participou_sus_w = 'N')) or (ie_ignora_participou_sus_w = 'S'))
and	a.cd_procedimento <> 415020050
and	not exists (	select 1 
			from sus_regra_proc x
			where	x.nr_seq_regra in (4,7,11,13)
			and	x.cd_procedimento = a.cd_procedimento
			and	x.ie_origem_proced = a.ie_origem_proced)
and	a.nr_sequencia		= b.nr_sequencia
and	a.ie_origem_proced	= 7
and	a.cd_motivo_exc_conta is null			
and	a.nr_interno_conta	= nr_interno_conta_p
union
select	min(a.nr_sequencia),
	max(nvl(a.cd_medico_executor, a.cd_pessoa_fisica)),
	max(nvl(substr(obter_dados_pf(nvl(a.cd_medico_executor, a.cd_pessoa_fisica),'CPF'),1,11),0)) nr_cpf_executor,
	max(nvl(substr(obter_dados_pf(nvl(a.cd_medico_executor, a.cd_pessoa_fisica),'CNS'),1,15),0)) cd_cns_executor,
	1 ie_funcao_equipe ,
	max(nvl(a.cd_cgc_prestador,0)),
	nvl(a.cd_procedimento,0) cd_procedimento,
	nvl(a.ie_origem_proced, 7) ie_origem_proced,
	sum(nvl(a.qt_procedimento,1)),
	trunc(nvl(a.dt_procedimento,sysdate),'Month'),
	max(nvl(a.cd_cbo, nvl(sus_obter_cbo_medico(nvl(a.cd_medico_executor,a.cd_pessoa_fisica), a.cd_procedimento, a.dt_procedimento,0 ),'0'))),
	max(nvl(a.ie_doc_executor, 5)),
	min(a.nr_seq_proc_princ),
	min(sus_obter_seq_ordem(a.nr_sequencia)) nr_seq_ordem,
	min(sus_obter_seq_ordem_princ(a.nr_sequencia)) nr_seq_ordem_princ,
	min(sus_ordenar_proc_aih(a.nr_sequencia)) ie_ordem,
	max(a.cd_setor_atendimento),
	max(nvl(sus_obter_dados_servico(a.nr_seq_servico,'C'),'0')),
	max(nvl(sus_obter_desc_classif(a.nr_seq_servico_classif,null,'C'),0)),
    null nr_seq_partic,
	min(a.dt_procedimento) dt_procedimento
from	procedimento_paciente a
where	a.qt_procedimento	> 0
and	a.cd_procedimento <> 415020050
and	exists (	select 1 
			from sus_regra_proc x
			where	x.nr_seq_regra in (7,13)
			and	x.cd_procedimento = a.cd_procedimento
			and	x.ie_origem_proced = a.ie_origem_proced)
and	a.ie_origem_proced	= 7
and	a.cd_motivo_exc_conta is null			
and	a.nr_interno_conta	= nr_interno_conta_p
group by a.ie_origem_proced,
	trunc(nvl(dt_procedimento,sysdate),'Month'),
	a.cd_procedimento
union
select	a.nr_sequencia,
	nvl(a.cd_medico_executor, a.cd_pessoa_fisica),
	nvl(substr(obter_dados_pf(nvl(a.cd_medico_executor, a.cd_pessoa_fisica),'CPF'),1,11),0) nr_cpf_executor,
	nvl(substr(obter_dados_pf(nvl(a.cd_medico_executor, a.cd_pessoa_fisica),'CNS'),1,15),0) cd_cns_executor,
	1 ie_funcao_equipe,
	nvl(a.cd_cgc_prestador,0),
	nvl(a.cd_procedimento,0) cd_procedimento,
	nvl(a.ie_origem_proced, 7) ie_origem_proced,
	nvl(a.qt_procedimento,1),
	nvl(a.dt_procedimento,sysdate),
	nvl(a.cd_cbo, nvl(sus_obter_cbo_medico(nvl(a.cd_medico_executor,a.cd_pessoa_fisica), a.cd_procedimento, a.dt_procedimento,0 ),'0')),
	nvl(a.ie_doc_executor, 5),
	a.nr_seq_proc_princ,
	sus_obter_seq_ordem(a.nr_sequencia) nr_seq_ordem,
	sus_obter_seq_ordem_princ(a.nr_sequencia) nr_seq_ordem_princ,
	sus_ordenar_proc_aih(a.nr_sequencia) ie_ordem,
	a.cd_setor_atendimento,
	nvl(sus_obter_dados_servico(a.nr_seq_servico,'C'),'0'),
	nvl(sus_obter_desc_classif(a.nr_seq_servico_classif,null,'C'),0),
    null nr_seq_partic,
	a.dt_procedimento dt_procedimento
from	procedimento_paciente a
where	a.qt_procedimento	= 0
and	Sus_Obter_Se_Detalhe_Proc(a.cd_procedimento, a.ie_origem_proced,'007', a.dt_procedimento) > 0
and	trunc(dt_final_w) = trunc(dt_final_w,'month')
and	cd_motivo_cobranca_w not in (21,22,23,24,25,26,27,28,31,32,41,42,43,65,66,67)
and	ie_identificacao_aih_w = '05'
and	a.cd_procedimento <> 415020050
and	not exists (	select 1 
			from sus_regra_proc x
			where	x.nr_seq_regra in (7,11,13)
			and	x.cd_procedimento = a.cd_procedimento
			and	x.ie_origem_proced = a.ie_origem_proced)
and	a.ie_origem_proced	= 7
and	a.cd_motivo_exc_conta is null			
and	a.nr_interno_conta	= nr_interno_conta_p
order by ie_ordem, nr_seq_ordem, nr_seq_ordem_princ, cd_procedimento desc, ie_funcao_equipe;

type 		fetch_array is table of c01%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_c01_w			vetor;

begin

begin
select	a.nr_aih,
	b.cd_pessoa_fisica,
	b.cd_estabelecimento,
	c.cd_convenio_parametro,
	a.dt_inicial,
	a.dt_final,
	a.CD_PROCEDIMENTO_REAL,
	b.dt_alta,
	c.dt_mesano_referencia,
	a.cd_motivo_cobranca,
	a.ie_identificacao_aih
into	nr_aih_w,
	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	cd_convenio_w,
	dt_inicial_w,
	dt_final_w,
	cd_procedimento_real_w,
	dt_alta_w,
	dt_mesano_referencia_w,
	cd_motivo_cobranca_w,
	ie_identificacao_aih_w
from	conta_paciente		c,
	atendimento_paciente	b,
	sus_aih_unif		a
where	a.nr_interno_conta	= nr_interno_conta_p
and	a.nr_atendimento	= b.nr_atendimento
and	a.nr_interno_conta	= c.nr_interno_conta;
exception when others then
	wheb_mensagem_pck.exibir_mensagem_abort(195846,'NR_INTERNO_CONTA='||nr_interno_conta_p);
	/*Existe mais de uma AIH cadastrada para a conta n ' || nr_interno_conta_p*/
end;

ie_ignora_participou_sus_w	:= nvl(sus_obter_parametro_aih('IE_IGNORA_PARTICIPOU_SUS', cd_estabelecimento_w),'N');

select	nvl(max(cd_cgc),'0')
into	cd_cgc_estab_w
from 	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_w;

/* Buscar o CNES do prestador */
if	((nvl(sus_parametros_aih_pck.get_cd_estabelecimento,0) <> cd_estabelecimento_w) or
	(nvl(sus_parametros_aih_pck.get_dt_atualizacao,sysdate) > nvl(sus_parametros_aih_pck.get_dt_geracao,sysdate))) then
	begin
	gerar_sus_parametros_aih_pck(cd_estabelecimento_w);
	
	cd_cnes_w			:= lpad(sus_parametros_aih_pck.get_cd_cnes_hospital,7,'0');
	ie_exporta_cnes_w		:= sus_parametros_aih_pck.get_ie_exporta_cnes;
	ie_exporta_cnes_hosp_w		:= sus_parametros_aih_pck.get_ie_exporta_cnes_hosp;
	ie_exporta_cnes_setor_w		:= sus_parametros_aih_pck.get_ie_exporta_cnes_setor;
	ie_forma_envio_data_proc_w	:= sus_parametros_aih_pck.get_ie_forma_envio_data_proc;
	ie_manter_doc_executor_w	:= sus_parametros_aih_pck.get_ie_manter_doc_executor;
	ie_exp_cnes_exc_med_w		:= sus_parametros_aih_pck.get_ie_exp_cnes_exc_med;	
	end;
else	
	begin
	cd_cnes_w			:= lpad(sus_parametros_aih_pck.get_cd_cnes_hospital,7,'0');
	ie_exporta_cnes_w		:= sus_parametros_aih_pck.get_ie_exporta_cnes;
	ie_exporta_cnes_hosp_w		:= sus_parametros_aih_pck.get_ie_exporta_cnes_hosp;
	ie_exporta_cnes_setor_w		:= sus_parametros_aih_pck.get_ie_exporta_cnes_setor;
	ie_forma_envio_data_proc_w	:= sus_parametros_aih_pck.get_ie_forma_envio_data_proc;
	ie_manter_doc_executor_w	:= sus_parametros_aih_pck.get_ie_manter_doc_executor;
	ie_exp_cnes_exc_med_w		:= sus_parametros_aih_pck.get_ie_exp_cnes_exc_med;
	end;
end if;

select	nvl(max(nr_seq_protocolo),0)
into	nr_seq_protocolo_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

open c01;
loop
fetch c01 bulk collect into s_array limit 1000;
	vetor_c01_w(i) := s_array;
	i := i + 1;
exit when c01%notfound;
end loop;
close c01;

for i in 1..vetor_c01_w.count loop
	begin
	s_array := vetor_c01_w(i);
	for z in 1..s_array.count loop
		begin
		
		nr_seq_proc_w		:= s_array(z).nr_seq_proc;
		cd_codigo_executor_w	:= s_array(z).cd_codigo_executor;
		nr_cpf_executor_w	:= s_array(z).nr_cpf_executor;
		cd_cns_executor_w	:= s_array(z).cd_cns_executor;
		ie_funcao_w		:= s_array(z).ie_funcao_equipe;
		cd_cgc_prestador_w	:= s_array(z).cd_cgc_prestador;
		cd_procedimento_w	:= s_array(z).cd_procedimento;
		ie_origem_proced_w	:= s_array(z).ie_origem_proced;
		qt_procedimento_w	:= s_array(z).qt_procedimento;
		dt_competencia_w	:= s_array(z).dt_competencia;
		cd_cbo_executor_w	:= s_array(z).cd_cbo_executor;
		ie_doc_executor_w	:= s_array(z).ie_doc_executor;
		nr_seq_proc_princ_w	:= s_array(z).nr_seq_proc_princ;
		nr_seq_ordem_w		:= s_array(z).nr_seq_ordem;
		nr_seq_ordem_princ_w	:= s_array(z).nr_seq_ordem_princ;
		ie_ordem_w		:= s_array(z).ie_ordem;
		cd_setor_atendimento_w	:= s_array(z).cd_setor_atendimento;
		cd_servico_w		:= s_array(z).cd_servico;
		cd_servico_classif_w	:= s_array(z).cd_servico_classif;
        nr_seq_partic_w     := s_array(z).nr_seq_partic;
		dt_procedimento_w   := s_array(z).dt_procedimento;
		if	(length(qt_procedimento_w) > 3) then
			wheb_mensagem_pck.exibir_mensagem_abort(195847,'CD_PROCED='|| cd_procedimento_w ||
									'QT_PROCED='|| qt_procedimento_w ||
									'NR_INTERNO_CONTA='|| nr_interno_conta_p);
			/*Problema na leitura da quatidade do procedimento ' || cd_procedimento_w || '(' || qt_procedimento_w ||'). Conta: ' || nr_interno_conta_p*/
		end if;

		/* Flag de controle da quantidade de procedimento */
		qt_registro_w	:= qt_registro_w + 1;
		
		sus_obter_exec_exp_aih(cd_procedimento_w, ie_origem_proced_w, cd_codigo_executor_w, cd_cbo_executor_w, cd_estabelecimento_w, 'S', 'N', 'N', cd_exec_exp_w, cd_cbo_exec_exp_w);
		sus_obter_prest_exp(cd_procedimento_w, ie_origem_proced_w,cd_cgc_prestador_w,cd_estabelecimento_w,'S','N','N',cd_cgc_prestador_exp_w);
		if	(cd_cbo_exec_exp_w is not null) and
			(cd_exec_exp_w is not null) then
			begin
			cd_codigo_executor_w	:= cd_exec_exp_w;
			cd_cbo_executor_w	:= cd_cbo_exec_exp_w;			
			
			nr_cpf_executor_w := nvl(substr(obter_dados_pf(cd_codigo_executor_w,'CPF'),1,11),0);
			cd_cns_executor_w := nvl(substr(obter_dados_pf(cd_codigo_executor_w,'CNS'),1,15),0);
			
			ie_doc_executor_ww := null;
			
			sus_obter_doc_exec_cnes( cd_estabelecimento_w,cd_cbo_executor_w,cd_codigo_executor_w,dt_competencia_w,ie_doc_executor_ww);
			
			if	(nvl(ie_doc_executor_ww,ie_doc_executor_w) <> ie_doc_executor_w) then
				ie_doc_executor_w := ie_doc_executor_ww;
			end if;		
			end;
		end if;

		if	(cd_cgc_prestador_exp_w	is not null) then
			cd_cgc_prestador_w	:= cd_cgc_prestador_exp_w;
		end if;
		
		if	(sus_validar_regra(4, cd_procedimento_w, ie_origem_proced_w,dt_procedimento_w) > 0) then
			if (ie_exp_cnpj_fornec_fabric_w = 'S') then
				
				select	max(cd_cgc_fabricante)
				into	cd_cgc_fabricante_w
				from	sus_aih_opm
				where	nr_seq_procedimento = nr_seq_proc_w;
				
				cd_cgc_prestador_w := cd_cgc_fabricante_w;
				
			end if;
		end if;
		
		if	(sus_obter_se_detalhe_proc(cd_procedimento_w, ie_origem_proced_w, '10008', dt_competencia_w) = 0) then
			ie_funcao_w		:= 0;
		end if;

		begin
		select	nvl(cd_grupo,0),
			nvl(cd_forma_organizacao,0)
		into	cd_grupo_w,
			cd_forma_organizacao_w
		from	sus_estrutura_procedimento_v
		where	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proced_w;
		exception
			when others then
			cd_grupo_w		:= 0;
			cd_forma_organizacao_w	:= 0;
		end;

		select	substr(nvl(max(cd_cnes),''),1,7)
		into	cd_cnes_prestador_w
		from	pessoa_juridica
		where	cd_cgc	= cd_cgc_prestador_w;

		if	(ie_exporta_cnes_setor_w = 'S') then
			begin

			begin
			select	substr(nvl(max(cd_cnes),cd_cnes_prestador_w),1,7)
			into	cd_cnes_prestador_w
			from	setor_atendimento
			where	cd_setor_atendimento	= cd_setor_atendimento_w;
			exception
				when others then
				cd_cnes_prestador_w := cd_cnes_prestador_w;
				end;

			end;
		end if;

		if	(cd_forma_organizacao_w	= 041701) then
			ie_funcao_w		:= 0;
		end if;

		if	(cd_cbo_executor_w	= '0') then
			cd_cbo_executor_w	:= '      ';
		end if;
		
		begin
		select  count(1)
		into 	qt_reg_prof_vinc_w
		from    cnes_profissional y,
			cnes_identificacao x,
			cnes_profissional_vinculo z
		where   y.nr_seq_identificacao  = x.nr_sequencia
		and     x.CD_CNES               = cd_cnes_w
		and     y.cd_pessoa_fisica      = cd_codigo_executor_w
		and     z.nr_seq_profissional   = y.nr_sequencia
		and     cd_cbo 			= nvl(cd_cbo_executor_w,'X')
		and     ((z.dt_vigencia_inicial is null) or (trunc(z.dt_vigencia_inicial,'month') <= trunc(dt_competencia_w)))
		and     ((z.dt_vigencia_final is null) or (trunc(z.dt_vigencia_final,'month') >= trunc(dt_competencia_w)))
		and     z.ie_atendimento_sus 	= 'S'
		and	rownum = 1;	
		exception
		when others then
			qt_reg_prof_vinc_w := 0;
		end;
		

		/* Quando o prestador for o proprio hospital e o procedimento NaO for OPM, entao deve ser exportado o CNES do hospital*/
		if	(cd_cgc_prestador_w	= cd_cgc_estab_w) and
			(cd_grupo_w		<> 7) then
			ie_doc_prestador_w	:= 5; /* CNES */
			cd_cgc_prestador_w	:= lpad(cd_cnes_w,14,' ');
		else
			/* Sempre que o procedimento for OPM deve ser exportado o CNPJ */
			ie_doc_prestador_w	:= 3; /* CNPJ */
			if	(cd_grupo_w		<> 7) and
				(cd_cnes_prestador_w is not null) then
				ie_doc_prestador_w	:= 5; /* CNES */
				cd_cgc_prestador_w	:= lpad(cd_cnes_prestador_w,14,' ');
			end if;
		end if;

		if	(ie_forma_envio_data_proc_w = 'A') then
			dt_competencia_w := dt_alta_w;
		elsif	(ie_forma_envio_data_proc_w = 'F') then
			dt_competencia_w := dt_final_w;
		end if;
		
		/* Obter a regra de alteracao no disquete para excluir ou nao o Medico ou CNES */		
		sus_obter_regra_expaih(nr_seq_proc_w, nm_usuario_p, nr_seq_regra_w, ie_exclui_medico_w, ie_exclui_cnes_w,ie_exclui_procedimento_w, nr_seq_partic_w);
		
		begin
		select	nvl(ie_considerar_cns_ant, 'N')
		into	ie_considerar_cns_ant_w
		from	parametro_faturamento
		where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
		exception
			when others then
			ie_considerar_cns_ant_w :='N';
		end;
		
		if (ie_considerar_cns_ant_w = 'S') then
			begin
				select	nvl(b.nr_cartao_nac_sus_ant, ' ')
				into	nr_cartao_nac_sus_ant_w
				from 	cnes_profissional 	b
				where	b.cd_pessoa_fisica = cd_codigo_executor_w
				and 	rownum = 1;
				exception
			when others then
				nr_cartao_nac_sus_ant_w := ' ';
			end;
			if (nr_cartao_nac_sus_ant_w <> ' ') then
				cd_cns_executor_w 		:= nr_cartao_nac_sus_ant_w;
				cd_cns_cpf_executor_w	:= nr_cartao_nac_sus_ant_w;
			end if;
		end if;
		
		if	(ie_doc_executor_w	= 1) then /* CNS/CPF */
			begin
			if	(trunc(dt_competencia_w,'month') < to_date('01/01/2012','dd/mm/yyyy')) then
				cd_doc_executor_w	:= nr_cpf_executor_w;
			elsif	(trunc(dt_competencia_w,'month') >= to_date('01/01/2012','dd/mm/yyyy')) then
				cd_doc_executor_w	:= cd_cns_executor_w;
			end if;
			if (ie_manter_doc_executor_w = 'N') then	
				if	(qt_reg_prof_vinc_w > 0) then
					begin
					ie_doc_executor_w  := 5;
					cd_doc_executor_w  := cd_cnes_w;
					if	(ie_exclui_medico_w = 'N') then
						cd_cgc_prestador_w	:= '0';
					end if;
					end;
				end if;
			end if;
			end;
		elsif	(ie_doc_executor_w	= 3) then /* CNPJ */
			cd_doc_executor_w	:= cd_cgc_prestador_w;
		elsif	(ie_doc_executor_w	= 5) then /* CNES */
			cd_doc_executor_w	:= cd_cnes_w;
		elsif	(ie_doc_executor_w	= 6) then /* CNES Terc.*/
			if	(cd_cnes_prestador_w is not null) then
				ie_doc_prestador_w	:= 5; /* CNES */
				cd_cgc_prestador_w	:= lpad(cd_cnes_prestador_w,14,' ');
			end if;
			ie_doc_executor_w	:= 5;
			cd_doc_executor_w	:= cd_cnes_prestador_w;

			if	(ie_exporta_cnes_hosp_w	= 'S')	and
				(ie_exporta_cnes_setor_w = 'N')	then
				cd_doc_executor_w	:= cd_cnes_w;
			end if;
		end if;

		/*Geliard - Tratamento seguinte foi adicionado para atender a portaria sas/ms n 203 de 04 de maio de 2011 - OS 322199*/
		if	(dt_mesano_referencia_w < to_date('01/01/2012','dd/mm/yyyy')) then
			begin
			if	(dt_competencia_w	< to_date('01/05/2011','dd/mm/yyyy')) then
				begin
				if	(cd_procedimento_real_w = cd_procedimento_w) and
					(dt_inicial_w	>= to_date('01/05/2011','dd/mm/yyyy')) then
					dt_comp_interf_w	:= to_char(dt_inicial_w,'yyyymm');
				else
					dt_comp_interf_w	:= '      ';
				end if;
				end;
			else
				begin
				if	(nvl(dt_final_w,dt_alta_w) < to_date('01/05/2011','dd/mm/yyyy')) then
					dt_comp_interf_w	:= '      ';
				else
					dt_comp_interf_w	:= to_char(dt_competencia_w, 'yyyymm');
				end if;
				end;
			end if;
			end;
		else
			dt_comp_interf_w	:= to_char(dt_competencia_w, 'yyyymm');
		end if;	
		
		/*Geliard - Tratamento seguinte foi adicionado para atender a portaria n 763 de 20 de Julho de 2011 - OS 401210*/
		if	(cd_cns_executor_w = '0') and
			(nr_cpf_executor_w = '0') then
			ie_doc_medico_prof_w	:= 0;
			cd_cns_cpf_executor_w	:= '0';
		elsif	(trunc(dt_competencia_w,'month') < to_date('01/01/2012','dd/mm/yyyy')) then
			ie_doc_medico_prof_w	:= 1;
			cd_cns_cpf_executor_w	:= nr_cpf_executor_w;
		elsif	(trunc(dt_competencia_w,'month') >= to_date('01/01/2012','dd/mm/yyyy')) then
			ie_doc_medico_prof_w	:= 2;
			cd_cns_cpf_executor_w	:= cd_cns_executor_w;
		end if;

		if	(sus_validar_regra(7, cd_procedimento_w, ie_origem_proced_w,dt_procedimento_w) > 0) or
			(sus_validar_regra(13,cd_procedimento_w, ie_origem_proced_w,dt_procedimento_w) > 0) then
			dt_comp_interf_w	:= to_char(dt_competencia_w, 'yyyymm');
		end if;
		
		if	(ie_exporta_cnes_w	= 'S') then
			begin
			select	nvl(ie_tipo_servico_sus,0)
			into	ie_tipo_servico_w
			from	medico_convenio
			where	cd_pessoa_fisica	= cd_codigo_executor_w
			and	cd_convenio		= cd_convenio_w;
			exception
			when others then
				ie_tipo_servico_w	:= 30;
			end;
		else
			
			select	nvl(max(ie_tipo_servico_sus),0)
			into	ie_tipo_servico_w
			from	medico_convenio
			where	cd_pessoa_fisica	= cd_codigo_executor_w
			and	cd_convenio		= cd_convenio_w;
		end if;
		
		
		select	count(*)
		into	qt_reg_cnes_proc_w
		from	sus_regra_cnes_proc_aih
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	ie_situacao		= 'A'
		and	rownum = 1;
		
		if	(qt_reg_cnes_proc_w > 0) then
			begin
			
			cd_cnes_regra_proc_w := sus_obter_cnes_proc_aih (cd_procedimento_w, ie_origem_proced_w,cd_estabelecimento_w);	
			
			if	(nvl(cd_cnes_regra_proc_w,'X') <> 'X') then
				ie_doc_prestador_w	:= 5; 
				cd_cnes_regra_proc_w	:= lpad(cd_cnes_regra_proc_w,7,'0');
				cd_cgc_prestador_w	:= lpad(cd_cnes_regra_proc_w,14,' ');
			end if;
			
			end;
		end if;
		
		/* De acordo com a regra do SUS: Medico credenciado deve exportar CPF + CBO + CNES. Profissional autonomo deve exportar CPF + CBO */		
		begin
		select	nvl(max(ie_credenciamento),ie_tipo_servico_w)
		into	ie_credenciamento_w
		from	sus_medico_credenciamento
		where	cd_medico				= cd_codigo_executor_w
		and	nvl(cd_cbo,nvl(cd_cbo_executor_w,'X'))	= nvl(cd_cbo_executor_w,'X')
		and	nvl(ie_situacao,'A') = 'A'
		and	nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w;	
		exception
		when others then
			ie_credenciamento_w := ie_tipo_servico_w;
		end;	

		if	(sus_validar_regra(12, cd_procedimento_w, ie_origem_proced_w,dt_procedimento_w) > 0) and
			(ie_doc_prestador_w = 5) and
			(ie_credenciamento_w not in ('4','30')) and
			(cd_procedimento_w not in (0802020011, 0309010047, 0309010071)) and
			(qt_reg_prof_vinc_w = 0) then
			cd_cgc_prestador_w	:= '0';
		end if;

		if	(ie_exclui_medico_w = 'S') then
			cd_cns_cpf_executor_w	:= ' ';
			cd_cbo_executor_w	:= '      ';
			ie_doc_medico_prof_w	:= 0;
			
			if	(ie_exp_cnes_exc_med_w = 'S') then
				ie_doc_executor_w	:= 5;
				cd_doc_executor_w	:= cd_cnes_w;
			end if;
		end if;
		if	(ie_exclui_cnes_w = 'S') then
			cd_cgc_prestador_w	:= '0';
		end if;

		/* Alteracao para tratamento de mais de 150 linhas (agrupar procedimentos nao OPME) */
		ie_regra_agrup_proc_w	:= nvl(sus_obter_se_regra_agrup_proc(cd_estabelecimento_w, cd_procedimento_w,7),'N');
		
		select	count(*)
		into	qt_reg_w
		from	sus_estrutura_procedimento_v
		where	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced	= 7
		and	((cd_grupo		= 2) or (ie_regra_agrup_proc_w = 'S'))
		and	rownum = 1;

		if	(cd_doc_executor_w is null) then
			cd_doc_executor_w	:= ' ';
		end if;
		
		if	(qt_reg_w	> 0) then
			begin

			select	nvl(sum(qt_procedimento),0),
				max(nr_sequencia),
				count(*)
			into	qt_proc_interf_w,
				nr_sequencia_w,
				qt_reg_w
			from (
			select	qt_procedimento,
				nr_sequencia
			from	w_susaih_interf_item
			where	nr_interno_conta		= nr_interno_conta_p
			and	cd_procedimento			= cd_procedimento_w
			and	nr_aih				= nr_aih_w
			and	nvl(ie_doc_medico_exec,0)	= nvl(ie_doc_medico_prof_w, 0)
			and	nvl(nr_cpf_executor, '0')	= nvl(nr_cpf_executor_w, '0')
			and	nvl(cd_cbo_executor, '0')	= nvl(cd_cbo_executor_w, '0')
			and	nvl(ie_funcao, 0)		= nvl(ie_funcao_w, 0)
			and	nvl(ie_doc_prestador, 0)	= nvl(ie_doc_prestador_w, 0)
			and	nvl(cd_cgc_prestador,'0')	= nvl(cd_cgc_prestador_w, '0')
			and	nvl(ie_doc_executor, 0)		= nvl(ie_doc_executor_w, 0)
			and	dt_competencia			< to_date('01/05/2011','dd/mm/yyyy')
			and	dt_competencia_w		< to_date('01/05/2011','dd/mm/yyyy')
			union
			select	qt_procedimento,
				nr_sequencia
			from	w_susaih_interf_item
			where	nr_interno_conta		= nr_interno_conta_p
			and	cd_procedimento			= cd_procedimento_w
			and	nr_aih				= nr_aih_w
			and	nvl(ie_doc_medico_exec,0)	= nvl(ie_doc_medico_prof_w, 0)
			and	nvl(nr_cpf_executor, '0')	= nvl(nr_cpf_executor_w, '0')
			and	nvl(cd_cbo_executor, '0')	= nvl(cd_cbo_executor_w, '0')
			and	nvl(ie_funcao, 0)		= nvl(ie_funcao_w, 0)
			and	nvl(ie_doc_prestador, 0)	= nvl(ie_doc_prestador_w, 0)
			and	nvl(cd_cgc_prestador,'0')	= nvl(cd_cgc_prestador_w, '0')
			and	nvl(ie_doc_executor, 0)		= nvl(ie_doc_executor_w, 0)
			and	dt_competencia_w		>= to_date('01/05/2011','dd/mm/yyyy')
			and	trunc(dt_competencia,'month')	= trunc(dt_competencia_w,'month'));

			if	(qt_reg_w > 0) then

				qt_procedimento_w	:= qt_procedimento_w + qt_proc_interf_w;

				ds_registro_w		:= ie_doc_medico_prof_w || lpad(cd_cns_cpf_executor_w,15,' ') || lpad(cd_cbo_executor_w,6,' ') || ie_funcao_w ||
							   ie_doc_prestador_w || rpad(cd_cgc_prestador_w,14,'0') || ie_doc_executor_w || lpad(cd_doc_executor_w,15,' ') ||
							   lpad(cd_procedimento_w,10,0) || lpad(qt_procedimento_w,3,0) || nvl(dt_comp_interf_w,'      ') || lpad(cd_servico_w,3,'0') ||
							   lpad(cd_servico_classif_w,3,'0') ;

				begin			
					update	w_susaih_interf_item
					set	qt_procedimento	= qt_procedimento_w,
						ds_registro	= ds_registro_w
					where	nr_sequencia	= nr_sequencia_w;
				exception
				when others then
					/*Erro ao atualizar quantidade do procedimento!
					Conta: nr_interno_conta_p
					AIH: nr_aih_w
					Procedimento: cd_procedimento_w
					Quantidade: qt_procedimento_w
					Erro: sqlerrm*/
					wheb_mensagem_pck.exibir_mensagem_abort(330029,
									'nr_interno_conta_p='||nr_interno_conta_p||
									';nr_aih_w='||nr_aih_w||
									';cd_procedimento_w='||cd_procedimento_w||
									';qt_procedimento_w='||qt_procedimento_w||
									';sqlerrm='||sqlerrm);				
				end;

			else
				begin

				ds_registro_w		:= ie_doc_medico_prof_w || lpad(cd_cns_cpf_executor_w,15,' ') || lpad(cd_cbo_executor_w,6,' ') || ie_funcao_w ||
							   ie_doc_prestador_w || rpad(cd_cgc_prestador_w,14,'0') || ie_doc_executor_w || lpad(cd_doc_executor_w,15,' ') ||
							   lpad(cd_procedimento_w,10,0) || lpad(qt_procedimento_w,3,0) || nvl(dt_comp_interf_w,'      ') || lpad(cd_servico_w,3,'0') ||
							   lpad(cd_servico_classif_w,3,'0') ;


				if	(ie_exclui_procedimento_w	<> 'S') then

					/* Obter a sequence da tabela*/
					select	w_susaih_interf_item_seq.nextval
					into	nr_sequencia_w
					from	dual;

					insert into w_susaih_interf_item(
						nr_sequencia,
						ie_doc_medico_exec,
						nr_cpf_executor,
						cd_cns_executor,
						cd_cbo_executor,
						ie_funcao,
						cd_cgc_prestador,
						cd_cnes,
						cd_procedimento,
						ie_origem_proced,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						qt_procedimento,
						dt_competencia,
						nr_aih,
						nr_interno_conta,
						nr_seq_proc,
						nr_seq_reg_proc,
						nr_seq_protocolo,
						nr_linha_proc,
						ds_registro,
						nr_seq_proc_princ,
						ie_ordem,
						ie_doc_prestador,
						ie_doc_executor,
						cd_doc_executor,
						dt_comp_proc)
					values(	nr_sequencia_w,
						ie_doc_medico_prof_w,
						nr_cpf_executor_w,
						cd_cns_executor_w,
						cd_cbo_executor_w,
						ie_funcao_w,
						cd_cgc_prestador_w,
						cd_cnes_w,
						cd_procedimento_w,
						ie_origem_proced_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						qt_procedimento_w,
						dt_competencia_w,
						nr_aih_w,
						nr_interno_conta_p,
						nr_seq_proc_w,
						nr_seq_reg_proc_w,
						nr_seq_protocolo_w,
						nr_linha_proc_w,
						ds_registro_w,
						nr_seq_proc_princ_w,
						ie_ordem_w,
						ie_doc_prestador_w,
						ie_doc_executor_w,
						cd_doc_executor_w,
						dt_competencia_w);

					nr_linha_proc_w	:= nr_linha_proc_w + 1;

					if	(qt_registro_w	= 9) then
						qt_registro_w		:= 0;
						ds_registro_w		:= '';
						nr_seq_reg_proc_w	:= nr_seq_reg_proc_w + 1;
					end if;
				end if;
				end;
			end if;
			end;
		else
			begin

			ds_registro_w	:= 	ie_doc_medico_prof_w || lpad(cd_cns_cpf_executor_w,15,' ') || lpad(cd_cbo_executor_w,6,' ') || ie_funcao_w ||
						ie_doc_prestador_w || rpad(cd_cgc_prestador_w,14,'0') || ie_doc_executor_w || lpad(cd_doc_executor_w,15,' ') ||
						lpad(cd_procedimento_w,10,0) || lpad(qt_procedimento_w,3,0) || nvl(dt_comp_interf_w,'      ') || lpad(cd_servico_w,3,'0') ||
						lpad(cd_servico_classif_w,3,'0') ;


			if	(ie_exclui_procedimento_w		<> 'S')	then
				/* Obter a sequence da tabela*/
				select	w_susaih_interf_item_seq.nextval
				into	nr_sequencia_w
				from	dual;

				insert into w_susaih_interf_item(
					nr_sequencia,
					ie_doc_medico_exec,
					nr_cpf_executor,
					cd_cns_executor,
					cd_cbo_executor,
					ie_funcao,
					cd_cgc_prestador,
					cd_cnes,
					cd_procedimento,
					ie_origem_proced,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					qt_procedimento,
					dt_competencia,
					nr_aih,
					nr_interno_conta,
					nr_seq_proc,
					nr_seq_reg_proc,
					nr_seq_protocolo,
					nr_linha_proc,
					ds_registro,
					nr_seq_proc_princ,
					ie_ordem,
					ie_doc_prestador,
					ie_doc_executor,
					cd_doc_executor,
					dt_comp_proc)
				values(	nr_sequencia_w,
					ie_doc_medico_prof_w,
					nr_cpf_executor_w,
					cd_cns_executor_w,
					cd_cbo_executor_w,
					ie_funcao_w,
					cd_cgc_prestador_w,
					cd_cnes_w,
					cd_procedimento_w,
					ie_origem_proced_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					qt_procedimento_w,
					dt_competencia_w,
					nr_aih_w,
					nr_interno_conta_p,
					nr_seq_proc_w,
					nr_seq_reg_proc_w,
					nr_seq_protocolo_w,
					nr_linha_proc_w,
					ds_registro_w,
					nr_seq_proc_princ_w,
					ie_ordem_w,
					ie_doc_prestador_w,
					ie_doc_executor_w,
					cd_doc_executor_w,
					dt_competencia_w);

				nr_linha_proc_w	:= nr_linha_proc_w + 1;

				if	(qt_registro_w	= 9) then
					qt_registro_w	:= 0;
					ds_registro_w	:= '';
					nr_seq_reg_proc_w	:= nr_seq_reg_proc_w + 1;
				end if;
			end if;
			end;
		end if;
	
		end;
	end loop;
	end;
end loop;

end gerar_interf_item_susaih;
/
