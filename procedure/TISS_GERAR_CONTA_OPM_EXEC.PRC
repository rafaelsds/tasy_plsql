create or replace
procedure TISS_Gerar_Conta_OPM_Exec(	nr_interno_conta_p		in	number,
					nm_usuario_p		in	varchar2) is


cd_cgc_estab_w		varchar2(20);
cd_opm_w		varchar2(20);
qt_opm_w		number(15,4);
cd_tabela_w		varchar2(20);
vl_unitario_w		number(15,2);
vl_opm_w		number(15,2);
cd_autorizacao_w		varchar2(30);
cd_cgc_prestador_w	varchar2(20);
cd_cgc_prest_solic_tiss_w	varchar2(20);
ds_opm_w		varchar2(255);
ds_prestador_tiss_w	varchar2(255);
ie_tiss_tipo_guia_desp_w	varchar2(255);
ie_valor_unitario_opm_w	varchar2(255);
ie_tipo_atendimento_w	number(3);
cd_convenio_w		number(5);
cd_estabelecimento_w	number(4);
ie_emite_opm_zerado_w	varchar2(3);
cd_prestador_tiss_w	varchar2(100);
ie_pacote_w		varchar2(255);
cd_senha_w		varchar2(255);
ie_senha_guia_w		varchar2(255);
nr_atendimento_w		number(15);
cont_w			number(15);
cd_medico_exec_tiss_w	varchar2(20);
cd_categoria_w		varchar2(10);	
nr_seq_classificacao_w	number(10);
cd_setor_atendimento_w	number(5);
ie_clinica_w		number(5);
ie_tipo_atend_tiss_w	varchar2(2);
cd_barra_material_w	varchar2(255);
nr_seq_mat_paci_w	number(10);
ie_agrupar_opme_w	varchar2(1);
ie_senha_guia_regra_w	varchar2(255);
ie_tipo_atend_tiss_conta_w	varchar2(255);
ie_conversao_qtde_mat_w		varchar2(10);
ie_qtd_material_w		varchar2(10);
ie_tiss_tipo_despesa_w		varchar2(10);
ie_digitos_desp_w		varchar2(10);
ie_procedimento_w		Varchar2(2);
dt_mesano_referencia_w		date;
ds_versao_w			varchar2(20);


cursor c01 is
select	nvl(a.cd_material_tiss,
		TISS_OBTER_COD_MAT(a.cd_material_convenio, b.cd_estabelecimento,  a.cd_material, a.dt_conta,
					tiss_obter_origem_preco_mat(	a.ie_origem_preco,
									a.cd_material,
									b.cd_estabelecimento,
									b.cd_convenio_parametro,
									b.cd_categoria_parametro, a.cd_tab_preco_material, 
									a.dt_conta, a.cd_setor_atendimento, 'X',a.nr_seq_proc_pacote,a.nr_sequencia, a.cd_material_tuss), 
									b.cd_convenio_parametro)) cd_opm,
	sum(decode(nvl(TISS_OBTER_REGRA_QTDE_MAT(a.nr_sequencia),ie_conversao_qtde_mat_w), 
			'M',(TISS_OBTER_QTD_MAT(a.nr_sequencia,ie_qtd_material_w) * decode(nvl(h.tx_conversao_qtde,0),0,1,h.tx_conversao_qtde)),
			'R',(round(TISS_OBTER_QTD_MAT(a.nr_sequencia,ie_qtd_material_w) * decode(nvl(h.tx_conversao_qtde,0),0,1,h.tx_conversao_qtde))),
			'D',(dividir_sem_round(TISS_OBTER_QTD_MAT(a.nr_sequencia,ie_qtd_material_w), decode(nvl(h.tx_conversao_qtde,0),0,1,h.tx_conversao_qtde))),
			'S',(ceil(TISS_OBTER_QTD_MAT(a.nr_sequencia,ie_qtd_material_w) * decode(nvl(h.tx_conversao_qtde,0),0,1,h.tx_conversao_qtde))))) qt_opm,
	TISS_OBTER_ORIGEM_PRECO_MAT(	a.ie_origem_preco, 
					a.cd_material, 
					b.cd_estabelecimento, 
					b.cd_convenio_parametro, 
					b.cd_categoria_parametro, 
					a.cd_tab_preco_material,
					a.dt_conta,
					a.cd_setor_atendimento,
					'X',
					a.nr_seq_proc_pacote,
					a.nr_sequencia,
					a.cd_material_tuss) ct_tabela,
	sum(a.vl_material) vl_opm,
	nvl(a.nr_doc_convenio, obter_desc_expressao(778770)) cd_autorizacao,
	nvl(a.cd_cgc_prestador_tiss, a.cd_cgc_prestador),
	substr(nvl(a.ds_material_tiss, obter_desc_material(a.cd_material)),1,60),
	ie_tiss_tipo_guia_desp,
	a.cd_prestador_tiss,
	a.cd_cgc_prest_solic_tiss,
	dividir(sum(a.vl_material), sum(a.qt_material)) vl_unitario,
	a.ds_prestador_tiss,
	tiss_obter_regra_senha(b.cd_estabelecimento, b.cd_convenio_parametro, b.ie_tipo_atend_tiss, ie_tiss_tipo_guia_desp,
		ie_senha_guia_w, a.cd_senha) cd_senha,
	a.cd_medico_exec_tiss,
	decode(ie_agrupar_opme_w,'N',a.cd_setor_atendimento,null) cd_setor_atendimento,
	substr(obter_barras_material(a.cd_material,null),1,255) cd_barra_material,
	decode(ie_agrupar_opme_w,'N',a.nr_sequencia,null) nr_sequencia,
	b.ie_tipo_atend_tiss,
	a.ie_tiss_tipo_despesa,
	'N' ie_procedimento
from	subgrupo_material g,
	classe_material f,
	material e,
	mat_atend_pac_convenio h,
	material_atend_paciente a,
	conta_paciente b
where	b.nr_interno_conta			= a.nr_interno_conta
and	a.cd_motivo_exc_conta			is null
and	a.cd_material				= e.cd_material
and	f.cd_subgrupo_material			= g.cd_subgrupo_material
and	e.cd_classe_material			= f.cd_classe_material
and	a.nr_sequencia 				= h.nr_seq_material(+)
--and	((f.ie_tipo_classe			in ('O','P','M')) or Edgar 16/01/2009, OS 123077, troquei o tratamento de tipo de classe por ie_tiss_tipo_despesa
and	((a.ie_tiss_tipo_despesa		in ('7','8','9')) or 
	 (TISS_OBTER_REGRA_MAT_OPM(b.cd_convenio_parametro, b.cd_estabelecimento, e.cd_material, f.cd_classe_material, 
					  f.cd_subgrupo_material, g.cd_grupo_material, obter_tipo_atendimento(b.nr_atendimento),a.vl_unitario) = 'S'))
and	b.nr_interno_conta				= nr_interno_conta_p
and	((a.nr_seq_proc_pacote is null) or
 	 ((ie_pacote_w = 'I') and (a.nr_seq_proc_pacote is not null)) or
	 (ie_pacote_w = 'A'))
group 	by nvl(a.cd_material_tiss,
		TISS_OBTER_COD_MAT(a.cd_material_convenio, b.cd_estabelecimento,  a.cd_material, a.dt_conta,
					tiss_obter_origem_preco_mat(	a.ie_origem_preco,
									a.cd_material,
									b.cd_estabelecimento,
									b.cd_convenio_parametro,
									b.cd_categoria_parametro, a.cd_tab_preco_material, 
									a.dt_conta, a.cd_setor_atendimento, 'X',a.nr_seq_proc_pacote,a.nr_sequencia, a.cd_material_tuss), 
									b.cd_convenio_parametro)),
	TISS_OBTER_ORIGEM_PRECO_MAT(	a.ie_origem_preco, 
					a.cd_material, 
					b.cd_estabelecimento, 
					b.cd_convenio_parametro, 
					b.cd_categoria_parametro, 
					a.cd_tab_preco_material,
					a.dt_conta, 
					a.cd_setor_atendimento,
					'X',
					a.nr_seq_proc_pacote,
					a.nr_sequencia,
					a.cd_material_tuss),
	nvl(a.nr_doc_convenio, obter_desc_expressao(778770)),
	nvl(a.cd_cgc_prestador_tiss, a.cd_cgc_prestador),
	substr(nvl(a.ds_material_tiss, obter_desc_material(a.cd_material)),1,60),
	ie_tiss_tipo_guia_desp,
	a.cd_prestador_tiss,	
	a.cd_cgc_prest_solic_tiss,
	a.ds_prestador_tiss,
	tiss_obter_regra_senha(b.cd_estabelecimento, b.cd_convenio_parametro, b.ie_tipo_atend_tiss, ie_tiss_tipo_guia_desp,
		ie_senha_guia_w, a.cd_senha),
	a.cd_medico_exec_tiss,
	decode(ie_agrupar_opme_w,'N',a.cd_setor_atendimento,null),
	substr(obter_barras_material(a.cd_material,null),1,255),
	decode(ie_agrupar_opme_w,'N',a.nr_sequencia,null),
	b.ie_tipo_atend_tiss,
	a.ie_tiss_tipo_despesa
union
select	nvl(a.cd_procedimento_convenio,
		to_char(cd_procedimento)) cd_opm,
	sum(a.qt_procedimento) qt_opm,
	nvl(substr(tiss_obter_tabela(a.cd_edicao_amb,
					b.cd_estabelecimento,
					b.cd_convenio_parametro,
					b.cd_categoria_parametro,
					a.dt_conta,
					'X',
					null,
					a.cd_procedimento,
					a.ie_origem_proced,
					a.nr_sequencia,
					a.nr_seq_proc_pacote,
					b.nr_atendimento,
					a.nr_seq_proc_interno,
					a.nr_seq_exame,
					a.cd_procedimento_tuss),1,20),'00') ct_tabela,
	sum(a.vl_procedimento) vl_opm,
	nvl(a.nr_doc_convenio, obter_desc_expressao(778770)) cd_autorizacao,
	nvl(a.cd_cgc_prestador_tiss, a.cd_cgc_prestador),
	substr(nvl(a.ds_proc_tiss, obter_descricao_procedimento(a.cd_procedimento, a.ie_origem_proced)),1,60),
	a.ie_tiss_tipo_guia_desp,
	a.cd_prestador_tiss,
	a.cd_cgc_prest_solic_tiss,
	dividir(sum(a.vl_procedimento),sum(a.qt_procedimento)) vl_unitario,
	a.ds_prestador_tiss,
	tiss_obter_regra_senha(b.cd_estabelecimento, b.cd_convenio_parametro, b.ie_tipo_atend_tiss, ie_tiss_tipo_guia_desp,
		ie_senha_guia_w, a.cd_senha) cd_senha,
	a.cd_medico_exec_tiss,
	decode(ie_agrupar_opme_w,'N',a.cd_setor_atendimento,null) cd_setor_atendimento,
	null cd_barra_material,
	decode(ie_agrupar_opme_w,'N',a.nr_sequencia,null) nr_sequencia,
	b.ie_tipo_atend_tiss,
	a.ie_tiss_tipo_despesa,
	'S' ie_procedimento
from	procedimento_paciente a,
	conta_paciente b
where	b.nr_interno_conta			= a.nr_interno_conta
and	a.cd_motivo_exc_conta			is null
and	a.ie_tiss_tipo_despesa			in ('7','8','9')
and	b.nr_interno_conta				= nr_interno_conta_p
and	((a.nr_seq_proc_pacote is null) or
 	 ((ie_pacote_w = 'I') and (a.nr_seq_proc_pacote is not null)) or
	 (ie_pacote_w = 'A'))
group 	by nvl(a.cd_procedimento_convenio,to_char(cd_procedimento)),
	nvl(substr(tiss_obter_tabela(a.cd_edicao_amb,
					b.cd_estabelecimento,
					b.cd_convenio_parametro,
					b.cd_categoria_parametro,
					a.dt_conta,
					'X',
					null,
					a.cd_procedimento,
					a.ie_origem_proced,
					a.nr_sequencia,
					a.nr_seq_proc_pacote,
					b.nr_atendimento,
					a.nr_seq_proc_interno,
					a.nr_seq_exame,
					a.cd_procedimento_tuss),1,20),'00'),
	nvl(a.nr_doc_convenio, obter_desc_expressao(778770)),
	nvl(a.cd_cgc_prestador_tiss, a.cd_cgc_prestador),
	substr(nvl(a.ds_proc_tiss, obter_descricao_procedimento(a.cd_procedimento, a.ie_origem_proced)),1,60),
	a.ie_tiss_tipo_guia_desp,
	a.cd_prestador_tiss,
	a.cd_cgc_prest_solic_tiss,
	a.ds_prestador_tiss,
	tiss_obter_regra_senha(b.cd_estabelecimento, b.cd_convenio_parametro, b.ie_tipo_atend_tiss, ie_tiss_tipo_guia_desp,
		ie_senha_guia_w, a.cd_senha),
	a.cd_medico_exec_tiss,
	decode(ie_agrupar_opme_w,'N',a.cd_setor_atendimento,null),
	decode(ie_agrupar_opme_w,'N',a.nr_sequencia,null),
	b.ie_tipo_atend_tiss,
	a.ie_tiss_tipo_despesa;
	
cursor c02 is
select	nvl(ie_senha_guia,'N')
from	tiss_regra_senha
where	cd_estabelecimento						= cd_estabelecimento_w
and	nvl(cd_convenio, nvl(cd_convenio_w,0))				= nvl(cd_convenio_w,0)
and	nvl(ie_tipo_atend_tiss, nvl(ie_tipo_atend_tiss_conta_w,'X'))	= nvl(ie_tipo_atend_tiss_conta_w,'X')
and	nvl(ie_tipo_guia, nvl(ie_tiss_tipo_guia_desp_w,'X'))			= nvl(ie_tiss_tipo_guia_desp_w,'X')
order by	nvl(cd_convenio, 0),
	nvl(ie_tipo_atend_tiss,'X'),
	nvl(ie_tipo_guia,'X');

BEGIN

select	max(b.cd_cgc),
	max(obter_tipo_atendimento(a.nr_atendimento)),
	max(a.cd_convenio_parametro),	
	max(b.cd_estabelecimento),
	max(a.cd_categoria_parametro),
	max(c.nr_seq_classificacao),
	max(c.ie_clinica),
	max(c.ie_tipo_atend_tiss),
	max(a.dt_mesano_referencia)
into	cd_cgc_estab_w,
	ie_tipo_atendimento_w,
	cd_convenio_w,	
	cd_estabelecimento_w,
	cd_categoria_w,	
	nr_seq_classificacao_w,
	ie_clinica_w,	
	ie_tipo_atend_tiss_w,
	dt_mesano_referencia_w
from	atendimento_paciente c,
	estabelecimento b,
	conta_paciente a
where	a.nr_interno_conta 	= nr_interno_conta_p
and	a.nr_atendimento	= c.nr_atendimento
and	a.cd_estabelecimento	= b.cd_estabelecimento;

select	nvl(max(ie_emite_opm_zerado), 'S'),
	nvl(max(ie_valor_unitario_opm),'U'),
	nvl(max(ie_pacote),'P'),
	nvl(max(ie_senha_guia),'ACA'),
	nvl(max(ie_agrupar_opme),'S'),
	nvl(max(ie_conversao_qtde_mat),'M'),
	nvl(max(ie_qtd_material),'M'),
	nvl(max(ie_digitos_desp),'N')
into	ie_emite_opm_zerado_w,
	ie_valor_unitario_opm_w,
	ie_pacote_w,
	ie_senha_guia_w,
	ie_agrupar_opme_w,
	ie_conversao_qtde_mat_w,
	ie_qtd_material_w,
	ie_digitos_desp_w
from	tiss_parametros_convenio
where	cd_convenio		= cd_convenio_w
and	cd_estabelecimento	= cd_estabelecimento_w;

select	max(tiss_obter_versao(cd_convenio_w, cd_estabelecimento_w,dt_mesano_referencia_w))
into	ds_versao_w
from	dual;

--Para o TISS 3, as OPME sao geradas como despesas, entao nao deve gerar na TISS_CONTA_OPM_EXEC
if	(obter_se_projeto_versao(0,12,ds_versao_w,0) = 'N') then

	open c01;
	loop
	fetch c01 into
		cd_opm_w,
		qt_opm_w,
		cd_tabela_w,
		vl_opm_w,
		cd_autorizacao_w,
		cd_cgc_prestador_w,
		ds_opm_w,
		ie_tiss_tipo_guia_desp_w,
		cd_prestador_tiss_w,
		cd_cgc_prest_solic_tiss_w,
		vl_unitario_w,
		ds_prestador_tiss_w,
		cd_senha_w,
		cd_medico_exec_tiss_w,
		cd_setor_atendimento_w,
		cd_barra_material_w,
		nr_seq_mat_paci_w,
		ie_tipo_atend_tiss_conta_w,
		ie_tiss_tipo_despesa_w,
		ie_procedimento_w;
	exit when c01%notfound;

		open C02;
		loop
		fetch C02 into	
			ie_senha_guia_regra_w;
		exit when C02%notfound;
			begin
			ie_senha_guia_regra_w	:= ie_senha_guia_regra_w;
			end;
		end loop;
		close C02;

		ie_senha_guia_w	:= nvl(ie_senha_guia_regra_w, ie_senha_guia_w);

		if	(ie_senha_guia_w		<> 'P') and
			(nvl(nr_interno_conta_p,0)	> 0) then

			select	max(a.nr_atendimento)
			into	nr_atendimento_w
			from	conta_paciente a
			where	a.nr_interno_conta	= nr_interno_conta_p;

			select	count(*)
			into	cont_w
			from	autorizacao_convenio a
			where	a.nr_atendimento	= nr_atendimento_w
			and	a.cd_autorizacao	= cd_autorizacao_w;

			if	(cont_w > 0) then
				select	max(a.cd_senha)
				into	cd_senha_w
				from	autorizacao_convenio a
				where	a.nr_atendimento	= nr_atendimento_w
				and	a.cd_autorizacao	= cd_autorizacao_w
				and	a.nr_sequencia		=
					(select		max(x.nr_sequencia)
					from		autorizacao_convenio x
					where		x.nr_atendimento	= a.nr_atendimento
					and		x.cd_autorizacao	= a.cd_autorizacao);
			end if;

		end if;

		if	(ie_valor_unitario_opm_w = 'TQ') then
			vl_unitario_w	:= dividir_sem_round(vl_opm_w, qt_opm_w);
		end if;	
		
		if	(ie_digitos_desp_w	= 'N') then
			if	(length(trim(cd_opm_w)) < 8) then
				cd_opm_w	:= lpad(trim(cd_opm_w), 8, '0');
			end if;
		elsif	(ie_digitos_desp_w	= 'S') then
			if	(length(trim(cd_opm_w)) < 10) then
				cd_opm_w	:= lpad(trim(cd_opm_w), 10, '0');
			end if;
		elsif	(ie_digitos_desp_w	= 'M') then
			if	(length(trim(cd_opm_w)) < 10) then
				if	(ie_procedimento_w	= 'N') then
					cd_opm_w	:= lpad(trim(cd_opm_w), 10, '0');
				else
					cd_opm_w	:= lpad(trim(cd_opm_w), 8, '0');
				end if;
			end if;
		elsif	(ie_digitos_desp_w	= 'R') then
			if	(length(trim(cd_opm_w)) < 8) then
				cd_opm_w	:= substr(lpad(trim(cd_opm_w), 8, '0'),1,8);
			elsif	(length(trim(cd_opm_w)) = 9) then
				cd_opm_w	:= substr(trim(cd_opm_w),2,8);
			elsif	(length(trim(cd_opm_w)) > 9) then
				cd_opm_w	:= substr(trim(cd_opm_w),3,8);
			end if;
		elsif	(ie_digitos_desp_w	= 'D') then /*lhalves OS258181*/
			if	(length(trim(cd_opm_w)) < 10) then
				if	(ie_procedimento_w	= 'S') then
					cd_opm_w	:= lpad(trim(cd_opm_w), 8, ' ');
				else
					cd_opm_w	:= lpad(trim(cd_opm_w), 8, '0');
				end if;
			end if;	
		elsif	(ie_digitos_desp_w	= 'I') then --Conforme Regra		
			cd_opm_w	:= TISS_OBTER_REGRA_CODIGO_DESP(cd_estabelecimento_w,cd_convenio_w,ie_tiss_tipo_despesa_w,cd_opm_w,null,null,null);
		end if;

		if	(cd_cgc_prestador_w is null) then
			cd_cgc_prestador_w		:= cd_cgc_estab_w;
		end if;	
		
		if	(cd_medico_exec_tiss_w is not null) then --lhalves OS 200142 18/03/2010 - Aplicar regra prestador de PF sobre a OPM
			cd_cgc_prestador_w		:= null;
		end if;

		if	(nvl(qt_opm_w,0) > 0) and
			((ie_emite_opm_zerado_w	= 'S') or
			 (nvl(vl_opm_w, 0) > 0)) then
		
			insert into tiss_conta_opm_exec
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_interno_conta,
				cd_autorizacao,
				nr_seq_guia,
				cd_opm,
				cd_barras,
				cd_tabela,
				ds_opm,
				qt_opm,
				vl_unitario_opm,
				vl_opm,
				cd_cgc_prestador,
				ie_tiss_tipo_guia_desp,
				cd_prestador_tiss,
				cd_cgc_prest_solic_tiss,
				cd_medico_exec_tiss,
				ds_prestador_tiss,
				cd_senha)
			values
				(tiss_conta_opm_exec_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_interno_conta_p,
				cd_autorizacao_w,
				null,
				cd_opm_w,
				substr(cd_barra_material_w,1,20),
				cd_tabela_w,
				tiss_eliminar_caractere(ds_opm_w),
				qt_opm_w,
				tiss_obter_regra_campo(ie_tiss_tipo_guia_desp_w,'VL_UNITARIO_OPM',cd_convenio_w,vl_unitario_w,ie_tipo_atendimento_w,cd_categoria_w,'N',0,cd_estabelecimento_w,ie_clinica_w,nr_seq_classificacao_w,cd_setor_atendimento_w,ie_tipo_atend_tiss_w||'#@',cd_setor_atendimento_w),
				vl_opm_w,
				cd_cgc_prestador_w,
				ie_tiss_tipo_guia_desp_w,
				cd_prestador_tiss_w,
				cd_cgc_prest_solic_tiss_w,
				cd_medico_exec_tiss_w,
				ds_prestador_tiss_w,
				cd_senha_w);
		end if;

	end loop;
	close c01;

	if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;
end if;

end TISS_Gerar_Conta_OPM_Exec;
/
