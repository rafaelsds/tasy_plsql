create or replace
procedure excluir_evento_cirurgia_pac(nr_sequencia_p	number) is 

begin
if	(nr_sequencia_p is not null) then
	begin
	update	evento_cirurgia_paciente
	set	nm_usuario = 'Tasy'
	where 	nr_sequencia = nr_sequencia_p;
	commit;
	delete
	from	evento_cirurgia_paciente
	where	nr_sequencia = nr_sequencia_p;
	end;
end if;
commit;
end excluir_evento_cirurgia_pac;
/