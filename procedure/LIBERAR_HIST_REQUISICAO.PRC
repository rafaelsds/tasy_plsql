create or replace
procedure liberar_hist_requisicao(
									nr_sequencia_p		number,
									nm_usuario_p		varchar2) is 

begin

update	cm_requisicao_hist
set		dt_liberacao = sysdate,
		nm_usuario_lib = nm_usuario_p
where	nr_sequencia = nr_sequencia_p;

commit;

end liberar_hist_requisicao;
/