create or replace
procedure med_excluir_resultado_data(	nr_seq_cliente_p	number,					
					dt_resultado_p		date) is

begin
if	(nr_seq_cliente_p is not null) and
	(dt_resultado_p is not null) then
	
	delete	from med_result_exame
        where	dt_exame	= dt_resultado_p
        and	nr_seq_cliente	= nr_seq_cliente_p;
	
end if;

commit;

end med_excluir_resultado_data;
/