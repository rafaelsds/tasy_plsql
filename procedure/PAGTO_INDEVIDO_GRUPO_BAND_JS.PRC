create or replace
procedure pagto_indevido_grupo_band_js(
		nr_sequencia_p		number,
		ie_pag_indevido_p	varchar2) is 
begin

/* Se a op��o de menu for Marcar como pagamento indevido*/
if	(ie_pag_indevido_p = 'S') then
	begin
	update  extrato_cartao_cr_movto
	set     ie_pagto_indevido   	= 'S'
	where   nr_sequencia 		= nr_sequencia_p;
	
	desconciliar_movto_redecard(nr_sequencia_p);
	end;
/* Sen�o a op��o de menu for Desmarcar pagamento indevido*/
else	
	begin
	update  extrato_cartao_cr_movto
	set     ie_pagto_indevido   	= 'N'
	where   nr_sequencia 		= nr_sequencia_p;
	end;
end if;

commit;

end pagto_indevido_grupo_band_js;
/