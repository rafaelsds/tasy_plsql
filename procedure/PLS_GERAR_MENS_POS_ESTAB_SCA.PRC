create or replace
procedure pls_gerar_mens_pos_estab_sca
			(	nr_seq_mensalidade_seg_p	Number,
				nr_seq_conta_p			Number,
				nm_usuario_p			Varchar2) is 

ie_preco_pos_w			Varchar2(2);
vl_total_liberado_w		Number(15,2)	:= 0;
nr_seq_contrato_w		Number(10);
nr_seq_plano_w			Number(10);
nr_seq_protocolo_w		Number(10);
tx_administracao_w		Number(7,4);
vl_informado_w			Number(15,2);
vl_taxa_pos_w			Number(15,2);
ie_tipo_taxa_w			Varchar2(5);
dt_mesano_referencia_w		Date;
nr_seq_mensalidade_w		Number(10);
qt_beneficiarios_w		Number(5)	:= 0;
vl_mensalidade_seg_w		Number(15,2)	:= 0;
nr_seq_seg_contrato_w		Number(10);
qt_idade_w			Number(3);
dt_adesao_seg_w			Date;
nr_seq_mensalidade_seg_w	Number(10);
nr_parcela_segurado_w		Number(10);
nr_parcela_contrato_w		Number(10);
dt_rescisao_w			Date;
dt_reativacao_w			Date;
dt_referencia_w			Date;
nr_seq_segurado_w		Number(10);
dt_alteracao_w			Date;
qt_conta_pos_estab_w		Number(10);
nr_seq_intercambio_w		Number(10);
nr_seq_sca_w			Number(10);
nr_seq_vinculo_sca_w		Number(10);
ie_tipo_segurado_w		pls_segurado.ie_tipo_segurado%type;
ie_pos_estab_faturamento_w	varchar2(255);
ie_pos_estab_fat_resc_w		varchar2(255);
ie_forma_cobr_pos_estab_w	varchar2(255);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
vl_taxa_manutencao_w		pls_conta_pos_estab_taxa.vl_taxa_manutencao%type;
ie_tipo_taxa_ww			pls_conta_pos_estab_taxa.ie_tipo_taxa%type;
nr_seq_item_mensalidade_w	pls_mensalidade_seg_item.nr_sequencia%type;

Cursor C01 is
	select	a.nr_seq_sca,
		c.ie_preco,
		sum(a.vl_beneficiario)
	from	pls_conta_pos_estabelecido	a,
		pls_conta 			b,
		pls_plano 			c
	where	a.nr_seq_conta		= b.nr_sequencia
	and	a.nr_seq_sca		= c.nr_sequencia
	and	a.nr_seq_mensalidade_seg is null
	and	a.ie_cobrar_mensalidade	= 'S'
	and	a.nr_seq_cooperativa is null
	and	b.nr_sequencia		= nr_seq_conta_p
	and	c.ie_preco in ('1','2','3')
	and	nvl(a.ie_situacao,'A') = 'A'
	and	not exists (	select	1
				from	pls_mensalidade_seg_item x,
					pls_mensalidade_segurado y,
					pls_mensalidade w
				where	x.nr_seq_mensalidade_seg = y.nr_sequencia
				and	y.nr_seq_mensalidade	= w.nr_sequencia
				and	x.nr_seq_conta		= b.nr_sequencia
				and	w.ie_cancelamento is null
				and	x.ie_tipo_item in ('4','6','7'))
	and	(((dt_alteracao_w is not null) and (dt_atendimento_referencia >= dt_alteracao_w)) or (dt_alteracao_w is null))
	group by a.nr_seq_sca,
		c.ie_preco;

Cursor C02 is
	select	b.nr_sequencia,
		trunc(months_between(sysdate, a.dt_nascimento) / 12),
		b.dt_contratacao
	from	pessoa_fisica	a,
		pls_segurado	b,
		pls_contrato	c
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	b.nr_seq_contrato	= c.nr_sequencia
	and	c.nr_sequencia		= nr_seq_contrato_w
	union all
	select	b.nr_sequencia,
		trunc(months_between(sysdate, a.dt_nascimento) / 12),
		b.dt_contratacao
	from	pessoa_fisica	a,
		pls_segurado	b,
		pls_intercambio	c
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	b.nr_seq_intercambio	= c.nr_sequencia
	and	c.nr_sequencia		= nr_seq_intercambio_w;

Cursor C03(	nr_seq_conta_pc		pls_conta.nr_sequencia%type) is
	select	vl_taxa_manutencao,
		decode(a.ie_tipo_taxa, '1', '10', '2', '9', null) ie_tipo_taxa
	from	pls_conta_pos_estabelecido 	b,
		pls_conta_pos_estab_taxa 	a
	where	b.nr_seq_conta = nr_seq_conta_pc
	and	b.nr_sequencia = nr_seq_conta_pos_estab;

begin
select	count(*)
into	qt_conta_pos_estab_w
from	pls_conta_pos_estabelecido	a,
	pls_conta			b
where	a.nr_seq_conta	= b.nr_sequencia
and	a.nr_seq_sca is not null
and	a.nr_seq_cooperativa is null
and	b.nr_sequencia	= nr_seq_conta_p;

if	(qt_conta_pos_estab_w > 0) then
	/* Obter dados do segurado */
	select	c.nr_seq_mensalidade,
		d.nr_seq_contrato,
		d.nr_seq_plano,
		d.dt_rescisao,
		d.dt_reativacao,
		c.dt_mesano_referencia,
		d.nr_sequencia,
		d.nr_seq_intercambio,
		trunc(a.dt_mesano_referencia,'month'),
		d.ie_tipo_segurado,
		d.cd_estabelecimento
	into	nr_seq_mensalidade_w,
		nr_seq_contrato_w,
		nr_seq_plano_w,
		dt_rescisao_w,
		dt_reativacao_w,
		dt_referencia_w,
		nr_seq_segurado_w,
		nr_seq_intercambio_w,
		dt_mesano_referencia_w,
		ie_tipo_segurado_w,
		cd_estabelecimento_w
	from	pls_segurado			d,
		pls_mensalidade_segurado	c,
		pls_mensalidade			b,
		pls_lote_mensalidade		a
	where	c.nr_seq_segurado		= d.nr_sequencia
	and	c.nr_seq_mensalidade		= b.nr_sequencia
	and	b.nr_seq_lote			= a.nr_sequencia
	and	c.nr_sequencia			= nr_seq_mensalidade_seg_p;

	select	max(ie_pos_estab_faturamento),
		max(ie_pos_estab_fat_resc)
	into	ie_pos_estab_faturamento_w,
		ie_pos_estab_fat_resc_w
	from	pls_parametros
	where	cd_estabelecimento	= cd_estabelecimento_w;
	
	/* Caso a forma de geracao de valores pos estabelecido depender de regra, o sistema ira verificar a regra atraves da function pls_obter_forma_cobr_pos_estab */
	if	(ie_pos_estab_faturamento_w = 'R') then
		ie_forma_cobr_pos_estab_w	:= pls_obter_forma_cobr_pos_estab(cd_estabelecimento_w,ie_tipo_segurado_w);
		/* Se o resultado da regra para o tipo de beneficiario for F (Faturamento), e alterada a variavel de geracao do pos-estabelecido para "N" (Nao gerar na mensalidade) */
		if	(ie_forma_cobr_pos_estab_w = 'F') then
			ie_pos_estab_faturamento_w	:= 'S';
		else
			ie_pos_estab_faturamento_w	:= 'N';
		end if;
	end if;
	
	if	(nvl(ie_pos_estab_faturamento_w,'N') = 'N') or
		((dt_rescisao_w is not null) and (dt_mesano_referencia_w > dt_rescisao_w) and (nvl(ie_pos_estab_fat_resc_w,'M') = 'M')) then
	
		select	max(a.dt_alteracao)
		into	dt_alteracao_w
		from	pls_segurado_alt_plano	a,
			pls_segurado		b
		where	a.nr_seq_segurado	= b.nr_sequencia
		and	a.ie_situacao 		= 'A'
		and	b.nr_sequencia		= nr_seq_segurado_w;

		/* Obter o protocolo da conta */
		select	nvl(max(nr_seq_protocolo),0)
		into	nr_seq_protocolo_w
		from	pls_conta
		where	nr_sequencia	= nr_seq_conta_p;

		open C01;
		loop
		fetch C01 into	
			nr_seq_sca_w,
			ie_preco_pos_w,
			vl_total_liberado_w;
		exit when C01%notfound;
			begin
			select	max(nr_sequencia)
			into	nr_seq_vinculo_sca_w
			from	pls_sca_vinculo
			where	nr_seq_segurado	= nr_seq_segurado_w
			and	nr_seq_plano	= nr_seq_sca_w;
			
			if 	(ie_preco_pos_w = '2') then /* 7 - Preco pos-estabelecido por rateio */
				if	(nvl(nr_seq_intercambio_w,0) <> 0) then
					select	count(*)
					into	qt_conta_pos_estab_w
					from	pls_conta	a,
						pls_segurado	b,
						pls_intercambio	c
					where	a.nr_seq_segurado 	= b.nr_sequencia
					and	b.nr_seq_intercambio 	= c.nr_sequencia
					and	c.nr_sequencia		= nr_seq_intercambio_w;
				else
					select	count(*)
					into	qt_conta_pos_estab_w
					from	pls_conta	a,
						pls_segurado	b,
						pls_contrato	c
					where	a.nr_seq_segurado 	= b.nr_sequencia
					and	b.nr_seq_contrato 	= c.nr_sequencia
					and	c.nr_sequencia		= nr_seq_contrato_w;
				end if;
				
				if	(qt_conta_pos_estab_w > 0) then
					if	(nvl(nr_seq_intercambio_w,0) <> 0) then
						select	sum(d.vl_beneficiario)
						into	vl_total_liberado_w
						from	pls_conta			a,
							pls_segurado			b,
							pls_intercambio			c,
							pls_conta_pos_estabelecido	d
						where	a.nr_seq_segurado 	= b.nr_sequencia
						and	b.nr_seq_intercambio 	= c.nr_sequencia
						and	d.nr_seq_conta		= a.nr_sequencia
						and	d.nr_seq_cooperativa is null
						and	d.nr_seq_mensalidade_seg is null
						and	d.nr_seq_sca is not null
						and	d.ie_cobrar_mensalidade	= 'S'
						and	nvl(d.ie_situacao,'A') = 'A'
						and	c.nr_sequencia		= nr_seq_intercambio_w
						and	a.nr_sequencia		= nr_seq_conta_p;
					else
						select	sum(d.vl_beneficiario)
						into	vl_total_liberado_w
						from	pls_conta			a,
							pls_segurado			b,
							pls_contrato			c,
							pls_conta_pos_estabelecido	d
						where	a.nr_seq_segurado 	= b.nr_sequencia
						and	b.nr_seq_contrato 	= c.nr_sequencia
						and	d.nr_seq_conta		= a.nr_sequencia
						and	d.nr_seq_cooperativa is null
						and	d.nr_seq_mensalidade_seg is null
						and	d.nr_seq_sca is not null
						and	d.ie_cobrar_mensalidade	= 'S'
						and	nvl(d.ie_situacao,'A') = 'A'
						and	c.nr_sequencia		= nr_seq_contrato_w
						and	a.nr_sequencia		= nr_seq_conta_p;
					end if;
				else
					if	(nvl(nr_seq_intercambio_w,0) <> 0) then
						select	sum(vl_total)
						into	vl_total_liberado_w
						from	pls_conta	a,
							pls_segurado	b,
							pls_intercambio	c
						where	a.nr_seq_segurado 	= b.nr_sequencia
						and	b.nr_seq_intercambio	= c.nr_sequencia
						and	c.nr_sequencia		= nr_seq_intercambio_w;
					else
						select	sum(vl_total)
						into	vl_total_liberado_w
						from	pls_conta	a,
							pls_segurado	b,
							pls_contrato	c
						where	a.nr_seq_segurado 	= b.nr_sequencia
						and	b.nr_seq_contrato 	= c.nr_sequencia
						and	c.nr_sequencia		= nr_seq_contrato_w;
					end if;
				end if;
				
				if	(nvl(nr_seq_intercambio_w,0) <> 0) then
					select	count(*)
					into	qt_beneficiarios_w
					from	pls_segurado	a,
						pls_intercambio	b
					where	a.nr_seq_intercambio	= b.nr_sequencia
					and	b.nr_sequencia		= nr_seq_intercambio_w
					and	a.dt_rescisao is null;
				else
					select	count(*)
					into	qt_beneficiarios_w
					from	pls_segurado	a,
						pls_contrato	b
					where	a.nr_seq_contrato	= b.nr_sequencia
					and	b.nr_sequencia		= nr_seq_contrato_w
					and	a.dt_rescisao is null;
				end if;
				
				vl_mensalidade_seg_w := dividir(vl_total_liberado_w,qt_beneficiarios_w);
				
				open C02;
				loop
				fetch C02 into	
					nr_seq_seg_contrato_w,
					qt_idade_w,
					dt_adesao_seg_w;
				exit when C02%notfound;
					begin
					select	pls_mensalidade_segurado_seq.nextval
					into	nr_seq_mensalidade_seg_w
					from	dual;
					
					select	(trunc(months_between(trunc(dt_mesano_referencia_w,'month'),trunc(dt_adesao_seg_w,'month'))) + 1),
						pls_obter_parcela_contrato(nr_seq_contrato_w,dt_mesano_referencia_w)
					into	nr_parcela_segurado_w,
						nr_parcela_contrato_w
					from	dual;
					
					insert into pls_mensalidade_segurado
						(nr_sequencia, dt_atualizacao, nm_usuario,
						nr_seq_segurado, vl_mensalidade, nr_seq_mensalidade,
						qt_idade, dt_mesano_referencia, nr_parcela,
						nr_seq_plano, nr_seq_contrato, nr_parcela_contrato,
						nr_seq_intercambio) 
					values	(nr_seq_mensalidade_seg_w, sysdate, nm_usuario_p,
						nr_seq_seg_contrato_w, 0, nr_seq_mensalidade_w, qt_idade_w,
						dt_mesano_referencia_w, nr_parcela_segurado_w, 
						nr_seq_plano_w, nr_seq_contrato_w, nr_parcela_contrato_w,
						nr_seq_intercambio_w);
					
					if	(nvl(vl_mensalidade_seg_w,0) > 0) then
					
						nr_seq_item_mensalidade_w := null;
					
						pls_insert_mens_seg_item('7',	nm_usuario_p,	null,
									null,	null,	null,
									null,	null,	null,	
									'N',	null,	null,		
									null,	null,	nr_seq_conta_p,			
									null,	null,	nr_seq_mensalidade_seg_w,	
									null,	null,	null,			
									nr_seq_protocolo_w,	null,	null,	
									null,	null,	null,		
									nr_seq_vinculo_sca_w,	null,	null,		
									null,	vl_mensalidade_seg_w,	nr_seq_item_mensalidade_w);

						if	((qt_conta_pos_estab_w > 0) and
							(nr_seq_item_mensalidade_w is not null)) then
							update	pls_conta_pos_estabelecido
							set	nr_seq_mensalidade_seg	= nr_seq_mensalidade_seg_w,
								dt_atualizacao		= sysdate,
								nm_usuario		= nm_usuario_p
							where	nr_seq_conta		= nr_seq_conta_p
							and	nr_seq_sca is not null
							and	nr_seq_cooperativa is null;
							
							pls_atualizar_item_faturado(nr_seq_conta_p, 1, nm_usuario_p);
						end if;	
					end if;
					end;
				end loop;
				close C02;
			elsif	(ie_preco_pos_w = '3') then /* 6 - Preco pos-estabelecido por custo operacional */
				/*if	(dt_rescisao_w is null) and*/
				if	(nvl(vl_total_liberado_w,0) <> 0) then /*aaschlote  - gerei uma mensalidade, na qual um benefiiciario do pagadr nao possui conta medica, assim jogando nulo o valor da mensalidade*/
					nr_seq_item_mensalidade_w := null;
					
					pls_insert_mens_seg_item('6',	nm_usuario_p,	null,
								null,	null,	null,
								null,	null,	null,	
								'N',	null,	null,		
								null,	null,	nr_seq_conta_p,			
								null,	null,	nr_seq_mensalidade_seg_p,	
								null,	null,	null,			
								nr_seq_protocolo_w,	null,	null,	
								null,	null,	null,		
								nr_seq_vinculo_sca_w,	null,	null,		
								null,	vl_total_liberado_w,	nr_seq_item_mensalidade_w);
				end if;
			elsif	(ie_preco_pos_w = '1') and
				(dt_rescisao_w is not null)then /*Tratado com o Diogo, pois existe a regra para gerar pos estabelecido para benefiicario rescindido */
				if	(nvl(vl_total_liberado_w,0) <> 0) then
					nr_seq_item_mensalidade_w := null;
					
					pls_insert_mens_seg_item('6',	nm_usuario_p,	null,
								null,	null,	null,
								null,	null,	null,	
								'N',	null,	null,		
								null,	null,	nr_seq_conta_p,			
								null,	null,	nr_seq_mensalidade_seg_p,	
								null,	null,	null,			
								nr_seq_protocolo_w,	null,	null,	
								null,	null,	null,		
								nr_seq_vinculo_sca_w,	null,	null,		
								null,	vl_total_liberado_w,	nr_seq_item_mensalidade_w);
				end if;
			end if;
			end;
		end loop;
		close C01;
		
		if	(ie_preco_pos_w in ('2','3')) then
			open C03(nr_seq_conta_p);
			loop
			fetch C03 into	
				vl_taxa_manutencao_w,
				ie_tipo_taxa_ww;
			exit when C03%notfound;
				begin
					nr_seq_item_mensalidade_w := null;
				
					pls_insert_mens_seg_item(ie_tipo_taxa_ww,	nm_usuario_p,	null,
								null,	null,	null,
								null,	null,	null,	
								'N',	null,	null,		
								null,	null,	nr_seq_conta_p,			
								null,	null,	nr_seq_mensalidade_seg_p,	
								null,	null,	null,			
								nr_seq_protocolo_w,	null,	null,	
								null,	null,	null,		
								nr_seq_vinculo_sca_w,	null,	null,		
								null,	vl_taxa_manutencao_w,	nr_seq_item_mensalidade_w);	
				end;
			end loop;
			close C03;
		end if;
	end if;
end if;

/* Nao pode dar commit nesta procedure */
--commit;

end pls_gerar_mens_pos_estab_sca;
/
