create or replace
procedure man_os_inserir_entrega	(	nr_seq_ordem_serv_p		number,
						cd_versao_p			varchar2,
						dt_lib_versao_p			date,
						nm_usuario_p			varchar2	) is

begin

if	(nr_seq_ordem_serv_p is not null) and
	(cd_versao_p is not null) and
	(dt_lib_versao_p is not null) and
	(nm_usuario_p is not null) then

	insert into man_ordem_serv_entrega (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_ordem_serv,
		cd_versao,
		dt_lib_versao)
	values (man_ordem_serv_entrega_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_ordem_serv_p,
		cd_versao_p,
		dt_lib_versao_p);

end if;

commit;

end man_os_inserir_entrega;
/