create or replace
procedure sus_atualizar_mot_glosa_conta(
			nr_seq_mot_glosa_p	sus_protocolo_envio_conta.nr_seq_mot_glosa%type,
			nr_interno_conta_p		sus_protocolo_envio_conta.nr_interno_conta%type,
			nm_usuario_p		usuario.nm_usuario%type) is 

begin

update	sus_protocolo_envio_conta
set	nr_seq_mot_glosa = nr_seq_mot_glosa_p,
	ie_status_ajuste = 'P',
	ie_situacao_retorno = 'G',
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where	nr_interno_conta = nr_interno_conta_p;

commit;

end sus_atualizar_mot_glosa_conta;
/
