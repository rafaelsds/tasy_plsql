create or replace
procedure lab_gerar_rotina_exame_ext 	(nr_seq_resultado_p	number,
					lista_informacao_p		varchar2,
					nm_usuario_p		varchar2) is

nr_seq_exame_w		number(10);
nr_seq_exame_analito_w	number(10);
nr_seq_exame_princ_w	number(10);
nm_exame_analito_w	varchar2(255);
nr_seq_apresent_w		number(10);
nr_seq_apresent_ww	number(10);
nm_exame_princ_w		varchar2(255);
nr_sequencia_w		number(10)		:= 0;
ie_contador_w		number(10,0)		:= 0;
tam_lista_w		number(10,0);
ie_pos_virgula_w	number(3,0);
lista_informacao_w	varchar2(2000);
nr_seq_unid_medida_w 	number(10);
nr_seq_metodo_w		number(10);
nr_seq_material_w	number(10);
ie_gera_analitos_exame_w varchar2(1) := 'N';

Cursor C01 is
	select	distinct nr_seq_exame, nm_exame, nr_seq_apresent
	from	exame_laboratorio 
	where   nvl(ie_situacao,'A') = 'A'
	and	nr_seq_superior = nr_seq_exame_princ_w
	start   with nr_seq_superior = nr_seq_exame_w
	connect by prior nr_seq_exame = nr_seq_superior
	order by 3;

Cursor C02 is
	select	distinct nr_seq_exame, nm_exame, nr_seq_apresent
	from	exame_laboratorio
	where	nvl(ie_situacao,'A') = 'A'
	and	nr_seq_superior = nr_seq_exame_w
	order by 3; 		
	
begin

lista_informacao_w	:= lista_informacao_p;

obter_param_usuario(7012, 7, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gera_analitos_exame_w);

select 	nvl(max(nr_sequencia),0)
into 	nr_sequencia_w
from 	exame_lab_result_item
where	nr_seq_resultado = nr_seq_resultado_p;

while	lista_informacao_w is not null or
	ie_contador_w > 200 loop
	begin
		tam_lista_w		:= length(lista_informacao_w);
		ie_pos_virgula_w	:= instr(lista_informacao_w,',');

		if	(ie_pos_virgula_w <> 0) then
			nr_seq_exame_w		:= substr(lista_informacao_w,1,(ie_pos_virgula_w - 1));
			lista_informacao_w	:= substr(lista_informacao_w,(ie_pos_virgula_w + 1),tam_lista_w);
		end if;
		
		select 	nvl(max(nr_sequencia),null)
		into	nr_seq_metodo_w
		from(	select 	a.nr_sequencia
			from	metodo_exame_lab a,
				exame_lab_metodo b
			where 	a.nr_sequencia = b.nr_seq_metodo
			and 		b.nr_seq_exame = nr_seq_exame_w
			order by ie_prioridade)
		where	rownum = 1;
		
		select	nvl(max(nr_sequencia),null)
		into	nr_seq_material_w
		from	(select	a.nr_sequencia
				from		material_exame_lab a,
							exame_lab_material b
				where		a.nr_sequencia = b.nr_seq_material
				and		b.nr_seq_exame = nr_seq_exame_w
				order by ie_prioridade)
		where	rownum = 1;
		
		select	nvl(max(a.nr_sequencia),null)
		into	nr_seq_unid_medida_w
		from	lab_unidade_medida a,
			exame_laboratorio b
		where	a.nr_sequencia = b.nr_seq_unid_med
		and	b.nr_seq_exame = nr_seq_exame_w;

		nr_sequencia_w := nr_sequencia_w + 1;

		if (ie_gera_analitos_exame_w <> 'S') and
		   (ie_gera_analitos_exame_w <> 'A') then
			insert into exame_lab_result_item
					(nr_seq_resultado,
					 nr_sequencia, 
					 nr_seq_exame, 
					 dt_atualizacao,
					 nm_usuario, 
					 qt_resultado, 
					 ds_resultado, 
					 nr_seq_material, 
					 pr_resultado, 
					 ie_status, 
					 dt_aprovacao,
					 nm_usuario_aprovacao, 
					 ie_consiste,
					 nr_seq_metodo,
					 nr_seq_unid_med)
					values
					(nr_seq_resultado_p,
						nr_sequencia_w,
						nr_seq_exame_w,
						sysdate,
						nm_usuario_p,
						null,
						null,
						nr_seq_material_w, 
						null,
						'',
						null, 
						'',
						'S',
						nr_seq_metodo_w,
						nr_seq_unid_medida_w);
		else
				insert into exame_lab_result_item
						(nr_seq_resultado,
						 nr_sequencia, 
						 nr_seq_exame, 
						 dt_atualizacao,
						 nm_usuario, 
						 qt_resultado, 
						 ds_resultado, 
						 nr_seq_material, 
						 pr_resultado, 
						 ie_status, 
						 dt_aprovacao,
						 nm_usuario_aprovacao, 
						 ie_consiste,
						 nr_seq_metodo,
						 nr_seq_unid_med)
						values
						(nr_seq_resultado_p,
							nr_sequencia_w,
							nr_seq_exame_w,
							sysdate,
							nm_usuario_p,
							null,
							null,
							nr_seq_material_w, 
							null,
							'',
							null, 
							'',
							'S',
							nr_seq_metodo_w,
							nr_seq_unid_medida_w);
			open C02;
			loop
			fetch C02 into	
				nr_seq_exame_princ_w,
				nm_exame_princ_w,
				nr_seq_apresent_w;
			exit when C02%notfound;
				begin
				nr_sequencia_w := nr_sequencia_w + 1;
				insert into exame_lab_result_item
								 (nr_seq_resultado,
								 nr_sequencia, 
								 nr_seq_exame, 
								 dt_atualizacao,
								 nm_usuario, 
								 qt_resultado, 
								 ds_resultado, 
								 nr_seq_material, 
								 pr_resultado, 
								 ie_status, 
								 dt_aprovacao,
								 nm_usuario_aprovacao, 
								 ie_consiste,
								 nr_seq_metodo,
								 nr_seq_unid_med)
								values
								(nr_seq_resultado_p,
								nr_sequencia_w,
								nr_seq_exame_princ_w,
								sysdate,
								nm_usuario_p,
								null,
								null,
								null, 
								null,
								'',
								null, 
								'',
								'S',
								null,
								null);
				open C01;
				loop
				fetch C01 into	
					nr_seq_exame_analito_w,
					nm_exame_analito_w,
					nr_seq_apresent_w;
				exit when C01%notfound;
						begin
						nr_sequencia_w := nr_sequencia_w + 1;
						insert into exame_lab_result_item
								 (nr_seq_resultado,
								 nr_sequencia, 
								 nr_seq_exame, 
								 dt_atualizacao,
								 nm_usuario, 
								 qt_resultado, 
								 ds_resultado, 
								 nr_seq_material, 
								 pr_resultado, 
								 ie_status, 
								 dt_aprovacao,
								 nm_usuario_aprovacao, 
								 ie_consiste,
								 nr_seq_metodo,
								 nr_seq_unid_med)
								values
								(nr_seq_resultado_p,
								nr_sequencia_w,
								nr_seq_exame_analito_w,
								sysdate,
								nm_usuario_p,
								null,
								null,
								null, 
								null,
								'',
								null, 
								'',
								'S',
								null,
								null);
						end;
				end loop;
				close C01;	
				end;
			end loop;
			close C02; 			
			
		end if;
		ie_contador_w	:= ie_contador_w + 1;
		end;
end loop;
	
commit;
end lab_gerar_rotina_exame_ext;
/
