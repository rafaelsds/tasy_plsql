CREATE OR REPLACE PROCEDURE DELETE_PROJ_CRON_ETAPA (nr_sequencia_p number) AS 
BEGIN

	delete from proj_cron_predec
	where NR_SEQ_ETAPA_ATUAL = nr_sequencia_p
	or NR_SEQ_ETAPA_PREDEC = nr_sequencia_p;

	delete from proj_cron_etapa 
	where nr_sequencia = nr_sequencia_p;
    
END DELETE_PROJ_CRON_ETAPA;
/