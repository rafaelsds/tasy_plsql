create or replace
procedure oncotech_importar_tit_rec (nm_usuario_p	varchar2) is

/*
ie_tipo_emissao_titulo		1 - sem emiss�o bloqueto
			2 - emiss�o bloqueto  origem
			3 - emiss�o bloqueto banco
			4 - nota promiss�ria
			5 - duplicata
cd_tipo_portador	
			0 - carteira
			1 - banco
			2 - empresa de cobran�a
			3 - advogado/jur�dico
			4 - cart�rio
			5 - cart�o de cr�dito
			6 - adm financeira
ie_origem_titulo
			1 - nota fiscal
			2 - conta paciente
			3 - plano de sa�de
			9 - outros
ie_situacao_p
			1 - aberto
			2 - liquidado
			3 - cancelado
			4 - liquidado com cheque
			5 - titulo transferido
			6 - liquidado como perda
ie_tipo_titulo_p
			1 - bloqueto
			2 - duplicata
			3 - nota promiss�ria
			4 - cheque
			5 - guia
			6 - nota fiscal
			7 - dep�sito banc�rio
			8 - contas de terceiros
ie_tipo_inclusao_p
			1 - manual
			2 - autom�tica
*/

dt_emissao_w			date;
dt_vencimento_w			date;
vl_titulo_w			number(15,2);
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			varchar2(14);
nr_titulo_externo_w		varchar2(255);
nr_documento_w			number(22);
ds_observacao_titulo_w		varchar2(255);
nr_seq_trans_fin_contab_w	number(10);
nr_seq_trans_fin_baixa_w	number(10);
cd_conta_contabil_w		varchar2(20);
cd_conta_financ_w		number(10);
ie_pls_w			varchar2(1);
nr_titulo_w			number(10);
nr_seq_classif_w		number(5);
ie_gravou_w			varchar2(1);
nr_seq_trans_fin_contab_ww	number(10);
nr_seq_trans_fin_baixa_ww	number(10);

Cursor C01 is
select	to_date(substr(ds_conteudo,1,8),'dd/mm/yyyy') dt_emissao,
	to_date(substr(ds_conteudo,9,8),'dd/mm/yyyy') dt_vencimento,
	somente_numero(substr(ds_conteudo,17,15)) vl_titulo,
	trim(substr(ds_conteudo,32,10)) cd_pessoa_fisica,
	trim(substr(ds_conteudo,42,14)) cd_cgc,
	somente_numero(substr(ds_conteudo,56,22)) nr_documento,
	trim(substr(ds_conteudo,78,1)) ie_pls,
	somente_numero(substr(ds_conteudo,79,10)) nr_seq_trans_fin_contab,
	somente_numero(substr(ds_conteudo,89,10)) nr_seq_trans_fin_baixa,
	trim(substr(ds_conteudo,99,20)) cd_conta_contabil,
	somente_numero(substr(ds_conteudo,119,10)) cd_conta_financ,
	trim(substr(ds_conteudo,129,255)) nr_titulo_externo,
	trim(substr(ds_conteudo,384,255)) ds_observacao_titulo
from	w_interf_sefip
where	nm_usuario = nm_usuario_p;

begin

open C01;
loop
fetch C01 into	
	dt_emissao_w,
	dt_vencimento_w,
	vl_titulo_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	nr_documento_w,
	ie_pls_w,
	nr_seq_trans_fin_contab_w,
	nr_seq_trans_fin_baixa_w,
	cd_conta_contabil_w,
	cd_conta_financ_w,
	nr_titulo_externo_w,
	ds_observacao_titulo_w;
exit when C01%notfound;
	begin
	
	ie_gravou_w := 'N';
	
	vl_titulo_w := dividir(vl_titulo_w,100);
	
	select	max(nr_titulo)
	into	nr_titulo_w
	from	titulo_receber
	where	nr_titulo_externo	= nr_titulo_externo_w;
	
	if	(nr_titulo_w is null) then
		begin
		
		select	titulo_seq.nextval
		into	nr_titulo_w
		from	dual;
		
		if (nr_seq_trans_fin_contab_w = 0) then
			nr_seq_trans_fin_contab_ww := null;
		else
			nr_seq_trans_fin_contab_ww := nr_seq_trans_fin_contab_w;
		end if;
		
		if (nr_seq_trans_fin_baixa_w = 0) then
			nr_seq_trans_fin_baixa_ww := null;
		else
			nr_seq_trans_fin_baixa_ww := nr_seq_trans_fin_baixa_w;
		end if;
		
		begin
		insert into titulo_receber(
			nr_titulo,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			dt_emissao,
			dt_vencimento,
			dt_pagamento_previsto,
			vl_titulo,
			vl_saldo_titulo,
			vl_saldo_juros,
			vl_saldo_multa,
			cd_moeda,
			cd_portador,
			cd_tipo_portador,
			tx_juros,
			tx_multa,
			cd_tipo_taxa_juro,
			cd_tipo_taxa_multa,
			tx_desc_antecipacao,
			ie_situacao,
			ie_tipo_emissao_titulo,
			ie_origem_titulo,
			ie_tipo_titulo,
			ie_tipo_inclusao,
			nr_lote_contabil,
			cd_pessoa_fisica,
			cd_cgc,
			dt_inclusao,
			nm_usuario_orig,
			ie_pls,
			nr_titulo_externo,
			nr_documento,
			ds_observacao_titulo,
			nr_seq_trans_fin_contab,
			nr_seq_trans_fin_baixa)
		values( nr_titulo_w,
			21,
			sysdate,
			nm_usuario_p,
			trunc(dt_emissao_w,'dd'),
			trunc(dt_vencimento_w+45,'dd'),
			trunc(dt_vencimento_w+45,'dd'),
			vl_titulo_w,
			vl_titulo_w,
			0,
			0,
			1,
			1,
			7,
			0,
			0,
			1,
			1,
			0,
			'1',
			1,
			'2',
			'7',
			'1',
			0,
			cd_pessoa_fisica_w,
			cd_cgc_w,
			sysdate,
			nm_usuario_p,
			ie_pls_w,
			nr_titulo_externo_w,
			nr_documento_w,
			ds_observacao_titulo_w,
			nr_seq_trans_fin_contab_ww,
			nr_seq_trans_fin_baixa_ww);
		
		ie_gravou_w := 'S';
		
		exception
		when others then
			begin
			gravar_log_tasy(605,substr('TITULO_RECEBER - t�tulo externo='||nr_titulo_externo_w|| chr(13) || chr(10) || sqlerrm(sqlcode),1,4000),nm_usuario_p);
			ie_gravou_w := 'N';
			end;
		end;
		end;
	end if;
	
	if	((cd_conta_contabil_w is not null) or (cd_conta_financ_w is not null)) and
		(ie_gravou_w = 'S') then
		begin
		
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_classif_w
		from	titulo_receber_classif
		where	nr_titulo = nr_titulo_w;
		
		begin
		insert into titulo_receber_classif(
			nr_titulo,
			nr_sequencia,
			vl_classificacao,
			dt_atualizacao,
			nm_usuario,
			cd_conta_contabil,
			cd_conta_financ)
		values( nr_titulo_w,
			nr_seq_classif_w,
			vl_titulo_w,
			sysdate,
			nm_usuario_p,
			cd_conta_contabil_w,
			cd_conta_financ_w);
		exception
		when others then
			gravar_log_tasy(605,substr('TITULO_RECEBER_CLASSIF - t�tulo externo='||nr_titulo_externo_w|| chr(13) || chr(10) || sqlerrm(sqlcode),1,4000),nm_usuario_p);
		end;
		end;
	end if;
	end;
end loop;
close C01;

commit;

end oncotech_importar_tit_rec;
/
