create or replace PROCEDURE PFCS_PA_CALC_POTENTIAL_ADMITS(
    cd_estabelecimento_p   NUMBER,
    nm_usuario_p           VARCHAR2
) IS

nr_seq_indicator_w           pfcs_panel.nr_seq_indicator%TYPE := 116;
pfcs_panel_detail_seq_w      pfcs_panel_detail.nr_sequencia%type;
pfcs_panel_seq_w             pfcs_panel.nr_sequencia%TYPE;

qt_potencial_admits_w   NUMBER(20) := 0;
qt_avg_time_w           NUMBER(20) := 0;

CURSOR c01 IS
SELECT enc.id_encounter nr_encounter_varchar,
    pat.patient_id id_patient,
    pfcs_get_human_name(pat.nr_sequencia, 'Patient') nm_patient,
    PFCS_GET_PATIENT_GENDER(pat.gender) gender,
    pat.birthdate,
    to_char(round(months_between(nvl(pat.deceased_date, sysdate), pat.birthdate))) qt_patient_age,
    enc.period_start,
	  ((SYSDATE - enc.period_start) * 24 * 60 ) total_time,
    pfcs_get_patient_diagnosis(enc.nr_sequencia) diagnosis,
	  enc.si_classif,
    uni.cd_unidade_compl ds_bed,
	  pfcs_get_code_status(pat.nr_sequencia, enc.nr_sequencia, 'S') code_status,
	nvl(pfcs_get_checklist(enc.nr_sequencia, pat.nr_sequencia, 'MPL'),'') ds_care_status,
	nvl(pfcs_get_checklist(enc.nr_sequencia, pat.nr_sequencia, 'TOOLTIP'),'') ds_checklist,
    pfcs_get_lace_plus(enc.nr_sequencia, enc.nr_seq_organization) readmission_risk,
    pfcs_get_edi_score(enc.nr_sequencia) nr_edi_score,
	  pfcs_get_frequent_flyer(enc.nr_sequencia) flag_recur_pat,
	  pfcs_get_recurring_patient(pat.nr_sequencia) ds_recur_pat,
    enc.nr_esi esi_level,
    sec.ds_setor_atendimento ds_department,
    substr(obter_valor_dominio(82, uni.ie_status_unidade), 1, 255) ds_status,
    decode(sec.ds_setor_atendimento, null, uni.cd_unidade_basica || '-' || uni.cd_unidade_compl, sec.ds_setor_atendimento || '-' || uni.cd_unidade_basica || '-' || uni.cd_unidade_compl) ds_location,
    uni.ie_temporario ie_temporary,
	pfcs_obs_contributor_pck.get_laceplus(enc.nr_sequencia) ds_readm_risk_contrb,
	pfcs_obs_contributor_pck.get_freqflyer_visits(enc.nr_sequencia) ds_recur_pat_adm_data,
	pfcs_obs_contributor_pck.get_freqflyer_comorbidities(enc.nr_sequencia) ds_recur_pat_comorbd,
	pfcs_obs_contributor_pck.get_freqflyer_visit_reason(enc.nr_sequencia) ds_recur_pat_reasons,
	pfcs_obs_contributor_pck.get_edi_vital_warnings(enc.nr_sequencia) ds_edi_vitals_warn,
	pfcs_obs_contributor_pck.get_edi_contributors(enc.nr_sequencia) ds_edi_contrb
FROM pfcs_encounter enc,
    pfcs_patient pat,
    pfcs_service_request psr,
    unidade_atendimento uni,
    setor_atendimento   sec
WHERE psr.cd_service <> 'E0405'
    AND enc.nr_seq_patient  = pat.nr_sequencia
    AND enc.nr_sequencia = psr.nr_seq_encounter
    AND uni.cd_setor_atendimento = sec.cd_setor_atendimento
    AND uni.nr_seq_location = pfcs_get_pat_location(pat.nr_sequencia, enc.nr_sequencia)
    AND pat.ie_active = '1'
    AND enc.period_start IS NOT NULL
    AND enc.period_end IS NULL
    AND enc.nr_esi IS NOT NULL
    AND enc.nr_esi <= 5
    AND pfcs_get_esi_rule(enc.nr_esi) = 'S'
    AND sec.ie_situacao = 'A'
    AND sec.cd_classif_setor = '1'
    AND sec.cd_estabelecimento_base = cd_estabelecimento_p;

BEGIN
FOR c01_w IN c01 LOOP
    qt_potencial_admits_w := qt_potencial_admits_w + 1;
    qt_avg_time_w := qt_avg_time_w + c01_w.total_time;

    SELECT pfcs_panel_detail_seq.NEXTVAL
    INTO pfcs_panel_detail_seq_w
    FROM dual;

    INSERT INTO pfcs_panel_detail (
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        ie_situation,
        nr_seq_indicator,
        nr_seq_operational_level
    ) VALUES (
        pfcs_panel_detail_seq_w,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        'T',
        nr_seq_indicator_w,
        cd_estabelecimento_p
    );

    INSERT INTO pfcs_detail_bed (
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        nr_seq_detail,
        ds_location,
        ds_department,
        ds_status,
        ie_temporary
    ) VALUES (
        pfcs_detail_bed_seq.nextval,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        pfcs_panel_detail_seq_w,
        c01_w.ds_location,
        c01_w.ds_department,
        c01_w.ds_status,
        c01_w.ie_temporary);

    INSERT INTO pfcs_detail_patient (
        nr_sequencia,
        nm_usuario,
        dt_atualizacao,
        nm_usuario_nrec,
        dt_atualizacao_nrec,
        nr_seq_detail,
        nr_encounter_varchar,
        dt_entrance,
        id_patient,
        nm_patient,
        ds_gender,
        dt_birthdate,
        ds_age_range,
        ds_symptoms,
        ds_dnr_status,
        qt_time_total_pa,
        ds_care_status,
        ds_checklist,
        ds_readmission_risk,
        qt_edi_score,
        ie_freq_flyer,
        ds_recurring_patient,
        nr_esi,
		ds_readm_risk_contrb,
		ds_rec_pat_adm_data,
		ds_rec_pat_comorbd,
		ds_rec_pat_reasons,
		ds_edi_vitals_warn,
		ds_edi_contrb
    ) VALUES (
        pfcs_detail_patient_seq.NEXTVAL,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        sysdate,
        pfcs_panel_detail_seq_w,
        c01_w.nr_encounter_varchar,
        c01_w.period_start,
        c01_w.id_patient,
        c01_w.nm_patient,
        c01_w.gender,
        c01_w.birthdate,
        c01_w.qt_patient_age,
        c01_w.diagnosis,
        c01_w.code_status,
        c01_w.total_time,
        c01_w.ds_care_status,
        c01_w.ds_checklist,
        c01_w.readmission_risk,
        c01_w.nr_edi_score,
        c01_w.flag_recur_pat,
        c01_w.ds_recur_pat,
        c01_w.esi_level,
		c01_w.ds_readm_risk_contrb,
		c01_w.ds_recur_pat_adm_data,
		c01_w.ds_recur_pat_comorbd,
		c01_w.ds_recur_pat_reasons,
		c01_w.ds_edi_vitals_warn,
		c01_w.ds_edi_contrb
    );

END LOOP;

pfcs_pck.pfcs_generate_results(
    vl_indicator_p              => qt_potencial_admits_w,
    vl_indicator_aux_p          => qt_avg_time_w,
    nr_seq_indicator_p          => nr_seq_indicator_w,
    nr_seq_operational_level_p  => cd_estabelecimento_p,
    nm_usuario_p                => nm_usuario_p,
    nr_seq_panel_p              => pfcs_panel_seq_w);

pfcs_pck.pfcs_update_detail(
    nr_seq_indicator_p          => nr_seq_indicator_w,
    nr_seq_panel_p              => pfcs_panel_seq_w,
    nr_seq_operational_level_p  => cd_estabelecimento_p,
    nm_usuario_p                => nm_usuario_p);

pfcs_pck.pfcs_activate_records(
    nr_seq_indicator_p          => nr_seq_indicator_w,
    nr_seq_operational_level_p  => cd_estabelecimento_p,
    nm_usuario_p                => nm_usuario_p);

END PFCS_PA_CALC_POTENTIAL_ADMITS;
/
