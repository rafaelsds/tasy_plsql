create or replace
procedure pls_inserir_faixas_etarias
		(	nr_seq_tabela_p		number,
			nr_seq_faixa_etaria_p	number,
			nm_usuario_p		Varchar2) is 

			
qt_reg_w		number(10);
			
begin

select	count(*)
into	qt_reg_w
from	pls_plano_preco
where	nr_seq_tabela	= nr_seq_tabela_p
and	nvl(vl_preco_atual,0)	<> 0;

if	(qt_reg_w = 0)  and
	(nr_seq_faixa_etaria_p is not null) then
	delete	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_p;
	
	pls_importar_faixa_etaria(nr_seq_tabela_p,nr_seq_faixa_etaria_p,nm_usuario_p);
end if;

end pls_inserir_faixas_etarias;
/
