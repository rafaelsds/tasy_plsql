create or replace
procedure pls_agt_consistir_idade_sexo
			(	nr_seq_lote_guia_aut_p	number,
				nr_seq_lote_proc_aut_p	number,
				nr_seq_guia_plano_p	number,
				nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Consistir se a 'Idade do Beneficiário incompatível com o Procedimento'.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção: Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
				
cd_pessoa_fisica_w		varchar2(10);
ie_tipo_segurado_w		varchar2(5);
ie_sexo_w			varchar2(1);
cd_procedimento_w		number(15);
nr_seq_guia_plano_proc_w	number(10);
nr_seq_guia_proc_w		number(10);
ie_origem_proced_w		number(10);
nr_seq_prestador_w		number(10);
nr_seq_segurado_w		number(10);
qt_idade_w			number(5);
qt_idade_permitida_w		number(3);
qt_sexo_permitido_w		number(3);
	
begin
	
select	max(cd_procedimento)
into	cd_procedimento_w
from	pls_lote_anexo_proc_aut
where	nr_sequencia	= nr_seq_lote_proc_aut_p;

select	max(ie_origem_proced)
into	ie_origem_proced_w
from	pls_guia_plano_proc
where	cd_procedimento	= cd_procedimento_w
and	nr_seq_guia 	= nr_seq_guia_plano_p;

select	max(nr_seq_segurado)
into	nr_seq_segurado_w
from	pls_guia_plano
where	nr_sequencia	= nr_seq_guia_plano_p;

begin
	select	b.cd_pessoa_fisica,
		b.ie_sexo,
		a.ie_tipo_segurado
	into	cd_pessoa_fisica_w,
		ie_sexo_w,
		ie_tipo_segurado_w
	from	pls_segurado 		a,
		pessoa_fisica 		b
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	a.nr_sequencia 		= nr_seq_segurado_w;
exception
when others then
	cd_pessoa_fisica_w	:= '';
	ie_sexo_w		:= '';
end;

qt_idade_w := pls_obter_idade_segurado(nr_seq_segurado_w, sysdate, 'A');

if	(nvl(nr_seq_guia_plano_p,0) <> 0) then
	select	nr_seq_prestador
	into	nr_seq_prestador_w
	from	pls_guia_plano
	where	nr_sequencia = nr_seq_guia_plano_p;	
		
		select	count(1)
		into	qt_sexo_permitido_w
		from	procedimento
		where	cd_procedimento	= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proced_w
		and	((nvl(ie_sexo_sus,ie_sexo_w) = ie_sexo_w) or (ie_sexo_sus = 'I'));
		
		if 	(qt_sexo_permitido_w = 0) and
			(ie_tipo_segurado_w not in ('I','H')) then
			pls_inserir_glosa_anexo_guia('1802', nr_seq_lote_guia_aut_p, nr_seq_lote_proc_aut_p, null, nm_usuario_p);
		end if;
		
		select	count(1)
		into	qt_idade_permitida_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proced_w
		and	qt_idade_w between nvl(qt_idade_minima_sus,qt_idade_w) and nvl(qt_idade_maxima_sus,qt_idade_w);
		
		if 	(qt_idade_permitida_w = 0) and
			(ie_tipo_segurado_w not in ('I','H')) then
			pls_inserir_glosa_anexo_guia('1803', nr_seq_lote_guia_aut_p, nr_seq_lote_proc_aut_p, null, nm_usuario_p);
		end if;
end if;

end pls_agt_consistir_idade_sexo;
/