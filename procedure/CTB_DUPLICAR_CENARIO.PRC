create or replace
procedure ctb_duplicar_cenario(	cd_estabelecimento_p		number,
				nr_seq_cenario_origem_p		number,
				nr_seq_cenario_destino_p		number,
				dt_mes_inic_p			date,
				dt_mes_fim_p			date,
				ie_copiar_metrica_p			varchar2,
				ie_copiar_ticket_medio_p		varchar2,
				ie_copiar_regra_valor_p		varchar2,
				ie_copiar_historico_p		varchar2,
				ie_copiar_linear_detalhado_p		varchar2,
				nm_usuario_p			varchar2) is


ie_situacao_w			varchar2(2);
nr_seq_criterio_rateio_w	number(10);
nr_sequencia_w			number(10);
nr_seq_metrica_w			number(10);
nr_seq_regra_w			number(15);
nr_seq_mes_ref_w			number(10);
nr_seq_mes_ref_ini_novo_w		number(10);
cd_centro_custo_w			number(15);
cd_centro_origem_w		number(15);
cd_classif_centro_w		varchar2(40);
cd_classif_conta_w			varchar2(40);
cd_conta_contabil_w		varchar2(20);
cd_conta_origem_w		varchar2(20);
cd_empresa_w			number(5);
dt_inicial_novo_w			date;
dt_final_novo_w			date;
dt_referencia_w			date;
dt_mes_inic_w			date;
dt_mes_inic_ww			date;
dt_mes_fim_w			date;
dt_mes_fim_ww			date;
ie_regra_w			varchar2(15);
pr_aplicar_w			number(15,4);
ie_sobrepor_w			varchar2(1);
qt_fixa_w				number(15,2);
qt_leito_w			number(10);
ie_copia_regra_w			varchar2(1)	:= 'S';
ie_ano_diferente_w			varchar2(1)	:= 'N';
ds_titulo_w			varchar2(100);
ds_historico_w			long;
ds_observacao_w			varchar2(255);
cd_estabelecimento_w		number(10);
tx_ocupacao_w			number(15,4);
nr_seq_origem_w			number(10);
nr_seq_grupo_centro_w		number(10);
nr_seq_grupo_conta_w		number(10);
qt_registro_w			number(10);

cursor c01 is
select	nr_seq_regra,
	nr_seq_metrica,
	nr_seq_mes_ref,
	cd_centro_custo,
	dt_mes_inic,
	dt_mes_fim,
	ie_regra,
	pr_aplicar,
	ie_sobrepor,
	qt_fixa,
	cd_estabelecimento,
	qt_leito,
	tx_ocupacao
from	ctb_regra_metrica
where	nr_seq_cenario = nr_seq_cenario_origem_p;

cursor c02 is
select	nr_sequencia,
	nr_seq_regra,
	nr_seq_metrica,
	nr_seq_mes_ref,
	cd_centro_custo,
	cd_conta_contabil,
	dt_mes_inic,
	dt_mes_fim,
	ie_regra,
	pr_aplicar,
	ie_sobrepor,
	vl_fixo,
	cd_estabelecimento,
	nr_seq_grupo_centro,
	nr_seq_grupo_conta
from	ctb_regra_ticket_medio
where	nr_seq_cenario = nr_seq_cenario_origem_p;

cursor c03 is
select	nr_seq_regra,
	nr_seq_mes_ref_orig,
	cd_centro_custo,
	cd_conta_contabil,
	cd_classif_conta,
	cd_classif_centro,
	nr_seq_criterio_rateio,
	dt_mes_inic,
	dt_mes_fim,
	cd_centro_origem,
	cd_conta_origem,
	ie_regra_valor,
	pr_aplicar,
	ie_sobrepor,
	vl_fixo,
	cd_estabelecimento,
	ds_observacao
from	ctb_orc_cen_regra
where	nr_seq_cenario = nr_seq_cenario_origem_p;

cursor c04 is
select	ds_titulo,
	ds_historico
from	ctb_cen_historico
where	nr_seq_cenario = nr_seq_cenario_origem_p;

begin

cd_empresa_w	:= obter_empresa_estab(cd_estabelecimento_p);

if	(nr_seq_cenario_origem_p is null) or
	(nr_seq_cenario_destino_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(204405);
end if;

if	(nr_seq_cenario_origem_p = nr_seq_cenario_destino_p) then
	wheb_mensagem_pck.exibir_mensagem_abort(204409);
end if;

/*Obter Datas do cenario de origem */
select	ctb_obter_mes_ref(a.nr_seq_mes_inicio),
	ctb_obter_mes_ref(a.nr_seq_mes_fim)
into	dt_mes_inic_ww,
	dt_mes_fim_ww
from	ctb_orc_cenario a
where	a.nr_sequencia = nr_seq_cenario_origem_p;

/*Obter o periodo do Novo Cenario */
select	ctb_obter_mes_ref(a.nr_seq_mes_inicio),
	ctb_obter_mes_ref(a.nr_seq_mes_fim),
	a.nr_seq_mes_inicio
into	dt_inicial_novo_w,
	dt_final_novo_w,
	nr_seq_mes_ref_ini_novo_w
from	ctb_orc_cenario a
where	a.nr_sequencia = nr_seq_cenario_destino_p;

if	(trunc(dt_inicial_novo_w,'year') <> trunc(dt_mes_inic_ww,'year')) then
	ie_ano_diferente_w	:= 'S';
end if;

if	(dt_mes_inic_p is not null) or
	(dt_mes_fim_p is not null) then
	ie_copia_regra_w:= 'N';
	dt_mes_inic_ww	:= dt_mes_inic_p;
	dt_mes_fim_ww	:= dt_mes_fim_p;
end if;

if	(ie_copiar_metrica_p = 'S') then

	open c01;
	loop
	fetch c01 into
		nr_seq_regra_w,
		nr_seq_metrica_w,
		nr_seq_mes_ref_w,
		cd_centro_custo_w,
		dt_mes_inic_w,
		dt_mes_fim_w,
		ie_regra_w,
		pr_aplicar_w,
		ie_sobrepor_w,
		qt_fixa_w,
		cd_estabelecimento_w,
		qt_leito_w,
		tx_ocupacao_w;
	exit when c01%notfound;
		begin

		select	ctb_regra_metrica_seq.nextval
		into	nr_sequencia_w
		from	dual;

		if	(dt_mes_inic_w < dt_mes_inic_ww) or
			(dt_mes_inic_w > dt_mes_fim_ww) then
			ie_copia_regra_w	:= 'N';
		end if;

		if	(dt_mes_fim_w > dt_mes_inic_ww) and
			(dt_mes_inic_w < dt_mes_inic_ww) then
			ie_copia_regra_w	:= 'S';
			dt_mes_inic_w		:= dt_mes_inic_ww;
		end if;
		
		if	(nvl(nr_seq_mes_ref_w ,0) <> 0) then

			dt_referencia_w		:= ctb_obter_mes_ref(nr_seq_mes_ref_w);
			if	(dt_referencia_w < dt_inicial_novo_w) then
				nr_seq_mes_ref_w	:= nr_seq_mes_ref_ini_novo_w;
			end if;
		end if;
		
		/* Se o ano for diferente, gerar o mesmo mes porem com o novo ano*/
		if	(ie_ano_diferente_w = 'S') then
			dt_mes_inic_w		:= to_date(to_char(dt_mes_inic_w,'dd/mm') || '/' || to_char(dt_inicial_novo_w,'yyyy'),'dd/mm/yyyy');
			dt_mes_fim_w		:= to_date(to_char(dt_mes_fim_w,'dd/mm') || '/' || to_char(dt_inicial_novo_w,'yyyy'),'dd/mm/yyyy');
			/* Mes de origem*/
			if	(nvl(nr_seq_mes_ref_w ,0) <> 0) then
				dt_referencia_w	:= ctb_obter_mes_ref(nr_seq_mes_ref_w);
				dt_referencia_w	:= to_date(to_char(dt_referencia_w,'dd/mm') || '/' || to_char(dt_inicial_novo_w,'yyyy'),'dd/mm/yyyy');
				
				select	max(nr_sequencia)
				into	nr_seq_mes_ref_w
				from	ctb_mes_ref
				where	cd_empresa	= cd_empresa_w
				and	dt_referencia	= dt_referencia_w;
				
				if	(nr_seq_mes_ref_w is null) then
				
					wheb_mensagem_pck.exibir_mensagem_abort(204410,'DS_MES='||dt_referencia_w);
					--(-20011,'O mes: ' || dt_referencia_w  || ' nao esta cadastrado na empresa!');
				end if;
				
			end if;
		end if;
		
		if	(ie_copia_regra_w = 'S') then
			insert into ctb_regra_metrica(
				nr_sequencia,
				cd_estabelecimento,
				nr_seq_cenario,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_regra,
				nr_seq_metrica,
				nr_seq_mes_ref,
				cd_centro_custo,
				dt_mes_inic,
				dt_mes_fim,
				ie_regra,
				pr_aplicar,
				ie_sobrepor,
				qt_fixa,
				qt_leito,
				tx_ocupacao)
			values(	nr_sequencia_w,
				cd_estabelecimento_w,
				nr_seq_cenario_destino_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_regra_w,
				nr_seq_metrica_w,
				nr_seq_mes_ref_w,
				cd_centro_custo_w,
				dt_mes_inic_w,
				dt_mes_fim_w,
				ie_regra_w,
				pr_aplicar_w,
				ie_sobrepor_w,
				qt_fixa_w,
				qt_leito_w,
				tx_ocupacao_w);
		end if;
		ie_copia_regra_w	:= 'S';
		end;
		end loop;
		close c01;

end if;

if	(dt_mes_inic_p is not null) or
	(dt_mes_fim_p	is not null) then
	ie_copia_regra_w:= 'N';
end if;

if	(ie_copiar_ticket_medio_p = 'S') then

	open c02;
	loop
	fetch c02 into
		nr_seq_origem_w,
		nr_seq_regra_w,
		nr_seq_metrica_w,
		nr_seq_mes_ref_w,
		cd_centro_custo_w,
		cd_conta_contabil_w,
		dt_mes_inic_w,
		dt_mes_fim_w,
		ie_regra_w,
		pr_aplicar_w,
		ie_sobrepor_w,
		qt_fixa_w,
		cd_estabelecimento_w,
		nr_seq_grupo_centro_w,
		nr_seq_grupo_conta_w;
	exit when c02%notfound;
		begin

		select	ctb_regra_ticket_medio_seq.nextval
		into	nr_sequencia_w
		from	dual;

		if	(dt_mes_inic_w < dt_mes_inic_ww) or
			(dt_mes_inic_w > dt_mes_fim_ww) then
			ie_copia_regra_w	:= 'N';
		end if;

		if	(dt_mes_fim_w >= dt_mes_inic_ww) and
			(dt_mes_inic_w <= dt_mes_inic_ww) then
			ie_copia_regra_w	:= 'S';
			dt_mes_inic_w		:= dt_mes_inic_ww;
		end if;
		
		if	(nvl(nr_seq_mes_ref_w ,0) <> 0) then

			dt_referencia_w		:= ctb_obter_mes_ref(nr_seq_mes_ref_w);
			if	(dt_referencia_w < dt_inicial_novo_w) then
				nr_seq_mes_ref_w	:= nr_seq_mes_ref_ini_novo_w;
			end if;
		end if;
		begin
			/* Se o ano for diferente, gerar o mesmo mes porem com o novo ano*/
			if	(ie_ano_diferente_w = 'S') then
				dt_mes_inic_w		:= to_date(to_char(dt_mes_inic_w,'dd/mm') || '/' || to_char(dt_inicial_novo_w,'yyyy'),'dd/mm/yyyy');
				dt_mes_fim_w		:= to_date(to_char(dt_mes_fim_w,'dd/mm') || '/' || to_char(dt_inicial_novo_w,'yyyy'),'dd/mm/yyyy');
				/* Mes de origem*/
				if	(nvl(nr_seq_mes_ref_w ,0) <> 0) then
					dt_referencia_w	:= ctb_obter_mes_ref(nr_seq_mes_ref_w);
					dt_referencia_w	:= to_date(to_char(dt_referencia_w,'dd/mm') || '/' || to_char(dt_inicial_novo_w,'yyyy'),'dd/mm/yyyy');
					
					select	max(nr_sequencia)
					into	nr_seq_mes_ref_w
					from	ctb_mes_ref
					where	cd_empresa	= cd_empresa_w
					and	dt_referencia	= dt_referencia_w;
					
					if	(nr_seq_mes_ref_w is null) then
						wheb_mensagem_pck.exibir_mensagem_abort(204410,'DS_MES='||dt_referencia_w);
						--(-20011,'O mes: ' || dt_referencia_w  || ' nao esta cadastrado na empresa!');
					end if;
					
				end if;
			end if;
		exception
		when others then
			if	(dt_mes_inic_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(118406);
			end if;
			if	(dt_mes_fim_w is null) then
				wheb_mensagem_pck.exibir_mensagem_abort(118406);
			end if;
		end;

		if	(ie_copia_regra_w	= 'S') then
			insert into ctb_regra_ticket_medio(
				nr_sequencia,
				cd_estabelecimento,
				nr_seq_cenario,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_regra,
				nr_seq_metrica,
				nr_seq_mes_ref,
				cd_centro_custo,
				cd_conta_contabil,
				nr_seq_grupo_conta,
				nr_seq_grupo_centro,
				dt_mes_inic,
				dt_mes_fim,
				ie_regra,
				pr_aplicar,
				ie_sobrepor,
				vl_fixo)
			values(	nr_sequencia_w,
				cd_estabelecimento_w,
				nr_seq_cenario_destino_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_regra_w,
				nr_seq_metrica_w,
				nr_seq_mes_ref_w,
				cd_centro_custo_w,
				cd_conta_contabil_w,
				nr_seq_grupo_conta_w,
				nr_seq_grupo_centro_w,
				dt_mes_inic_w,
				dt_mes_fim_w,
				ie_regra_w,
				pr_aplicar_w,
				ie_sobrepor_w,
				qt_fixa_w);
				
				
			if	(nr_seq_grupo_conta_w is not null) then
				begin
			
				select	count(*)
				into	qt_registro_w
				from	ctb_regra_tm_distrib
				where	nr_seq_regra_ticket	= nr_seq_origem_w;
				
				if	(qt_registro_w > 0) then
					
					insert into ctb_regra_tm_distrib(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_regra_ticket,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						cd_conta_contabil,
						pr_valor)
					select	ctb_regra_tm_distrib_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_sequencia_w,
						sysdate,
						nm_usuario_p,
						cd_conta_contabil,
						pr_valor
					from	ctb_regra_tm_distrib
					where	nr_seq_regra_ticket	= nr_seq_origem_w;
				end if;

				end;
			end if;
		end if;
		ie_copia_regra_w	:= 'S';
		end;
		end loop;
		close c02;

end if;

if	(dt_mes_inic_p is not null) or
	(dt_mes_fim_p	is not null) then
	ie_copia_regra_w:= 'N';
end if;

if	(ie_copiar_regra_valor_p = 'S') then
	open c03;
	loop
	fetch c03 into
		nr_seq_regra_w,
		nr_seq_mes_ref_w,
		cd_centro_custo_w,
		cd_conta_contabil_w,
		cd_classif_conta_w,
		cd_classif_centro_w,
		nr_seq_criterio_rateio_w,
		dt_mes_inic_w,
		dt_mes_fim_w,
		cd_centro_origem_w,
		cd_conta_origem_w,
		ie_regra_w,
		pr_aplicar_w,
		ie_sobrepor_w,
		qt_fixa_w,
		cd_estabelecimento_w,
		ds_observacao_w;
	exit when c03%notfound;
		begin
		
		select	ctb_orc_cen_regra_seq.nextval
		into	nr_sequencia_w
		from	dual;

		if	(dt_mes_inic_w < dt_mes_inic_ww) or
			(dt_mes_inic_w > dt_mes_fim_ww) then
			ie_copia_regra_w	:= 'N';
		end if;
		
		if	(dt_mes_fim_w >= dt_mes_inic_ww) and
			(dt_mes_inic_w <= dt_mes_inic_ww) then
			ie_copia_regra_w	:= 'S';
			dt_mes_inic_w		:= dt_mes_inic_ww;
		end if;
		
		if	(nvl(nr_seq_mes_ref_w ,0) <> 0) then

			dt_referencia_w		:= ctb_obter_mes_ref(nr_seq_mes_ref_w);
			if	(dt_referencia_w < dt_inicial_novo_w) then
				nr_seq_mes_ref_w	:= nr_seq_mes_ref_ini_novo_w;
			end if;
		end if;
		
		/* Se o ano for diferente, gerar o mesmo mes porem com o novo ano*/
		if	(ie_ano_diferente_w = 'S') then
			dt_mes_inic_w		:= to_date(to_char(dt_mes_inic_w,'dd/mm') || '/' || to_char(dt_inicial_novo_w,'yyyy'),'dd/mm/yyyy');
			dt_mes_fim_w		:= to_date(to_char(dt_mes_fim_w,'dd/mm') || '/' || to_char(dt_inicial_novo_w,'yyyy'),'dd/mm/yyyy');
			/* Mes de origem*/
			if	(nvl(nr_seq_mes_ref_w ,0) <> 0) then
				dt_referencia_w	:= ctb_obter_mes_ref(nr_seq_mes_ref_w);
				dt_referencia_w	:= to_date(to_char(dt_referencia_w,'dd/mm') || '/' || to_char(dt_inicial_novo_w,'yyyy'),'dd/mm/yyyy');
				
				select	max(nr_sequencia)
				into	nr_seq_mes_ref_w
				from	ctb_mes_ref
				where	cd_empresa	= cd_empresa_w
				and	dt_referencia	= dt_referencia_w;
				
				if	(nr_seq_mes_ref_w is null) then
					wheb_mensagem_pck.exibir_mensagem_abort(204410,'DS_MES='||dt_referencia_w);
					--(-20011,'O mes: ' || dt_referencia_w  || ' nao esta cadastrado na empresa!');
				end if;
				
			end if;
		end if;

		if	(ie_copia_regra_w	= 'S') then
			ie_situacao_w := 'A';
			if cd_centro_custo_w is not null then
				select 	max(nvl(ie_situacao,'I'))
				into	ie_situacao_w
				from	centro_custo
				where 	cd_centro_custo = cd_Centro_custo_w;
			end if;
			
			if (ie_situacao_w <> 'I') then
				insert into ctb_orc_cen_regra(
					nr_sequencia,
					cd_estabelecimento,
					nr_seq_cenario,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_regra,
					nr_seq_mes_ref_orig,
					cd_centro_custo,
					cd_conta_contabil,
					cd_classif_conta,
					cd_classif_centro,
					nr_seq_criterio_rateio,
					dt_mes_inic,
					dt_mes_fim,
					cd_centro_origem,
					cd_conta_origem,
					ie_regra_valor,
					pr_aplicar,
					ie_sobrepor,
					vl_fixo,
					ds_observacao)
				values(	nr_sequencia_w,
					cd_estabelecimento_w,
					nr_seq_cenario_destino_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_regra_w,
					nr_seq_mes_ref_w,
					cd_centro_custo_w,
					cd_conta_contabil_w,
					cd_classif_conta_w,
					cd_classif_centro_w,
					nr_seq_criterio_rateio_w,
					dt_mes_inic_w,
					dt_mes_fim_w,
					cd_centro_origem_w,
					cd_conta_origem_w,
					ie_regra_w,
					pr_aplicar_w,
					ie_sobrepor_w,
					qt_fixa_w,
					ds_observacao_w);
			end if;
		end if;
		ie_copia_regra_w	:= 'S';
		end;
	end loop;
	close c03;
end if;

if	(ie_copiar_historico_p = 'S') then
	
	open C04;
	loop
	fetch C04 into	
		ds_titulo_w,
		ds_historico_w;
	exit when C04%notfound;
		begin
		insert into ctb_cen_historico(
			nr_sequencia,
			nr_seq_cenario,
			ds_titulo,
			ds_historico,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec)
		values(	ctb_cen_historico_seq.nextval,
			nr_seq_cenario_destino_p,
			ds_titulo_w,
			ds_historico_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p);
		end;
	end loop;
	close C04;
end if;

if	(nvl(ie_copiar_linear_detalhado_p, 'N') = 'S') then
	
	ctb_duplicar_linear_detalhado(	cd_estabelecimento_p,
					nr_seq_cenario_origem_p, 
					nr_seq_cenario_destino_p,
					nm_usuario_p);
end if;

commit;
end ctb_duplicar_cenario;
/