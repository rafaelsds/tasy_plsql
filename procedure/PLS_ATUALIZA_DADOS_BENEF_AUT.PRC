create or replace
procedure pls_atualiza_dados_benef_aut
				(	nr_seq_analise_p		Number,
					nr_seq_segurado_p		Number,
					nr_telef_secretaria_p		Varchar2,
					nm_secretaria_p			Varchar2,
					nr_telef_beneficiario_p		Varchar2,
					nr_telef_celular_benef_p	Varchar2,
					ds_email_beneficiario_p		Varchar2,
					ie_parametro_regra_p		Varchar2,        /* Valor do par�metro [62] da fun��o OPS - Gest�o de An�lise de autoriza��es  */
					cd_estabelecimento_p		Number,
					nm_usuario_p			Varchar2
					) is
					

nr_seq_regra_atualizacao_w	varchar2(10);
ie_funcao_validacao_w		varchar2(2);
cd_pessoa_fisica_w		varchar2(10);
nr_ddd_celular_w		varchar2(2);
nr_ddd_telef_benef_w		varchar2(2);
nr_ddd_telef_secretaria_w	varchar2(2);
nr_telef_celular_benef_w	varchar2(4000);
nr_telef_beneficiario_w		varchar2(4000);
nr_telef_secretaria_w		varchar2(4000);


begin

pls_verifica_atualizacao_cad( nr_seq_segurado_p, 'A', nr_seq_regra_atualizacao_w, ie_funcao_validacao_w, cd_pessoa_fisica_w);

nr_telef_celular_benef_w 	:= SubStr(nr_telef_celular_benef_p, InStr(nr_telef_celular_benef_p,')') + 1 ,length(nr_telef_celular_benef_p) + 1);
nr_telef_beneficiario_w 	:= SubStr(nr_telef_beneficiario_p, InStr(nr_telef_beneficiario_p,')') + 1 ,length(nr_telef_beneficiario_p) + 1);
nr_telef_secretaria_w 		:= SubStr(nr_telef_secretaria_p, InStr(nr_telef_secretaria_p,')') + 1 ,length(nr_telef_secretaria_p) + 1);


if	(InStr(nr_telef_celular_benef_p,'()',1) > 0) and (nr_telef_celular_benef_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(444259); -- DDD do celular do benefici�rio n�o informado! Favor verificar!
	
elsif	(nr_telef_celular_benef_p = '()') then
	nr_ddd_celular_w		:= '';
	
else
	nr_ddd_celular_w 		:= SubStr(nr_telef_celular_benef_p, InStr(nr_telef_celular_benef_p,'(',1)+1 ,2);
end if;

if	(InStr(nr_telef_beneficiario_p,'()',1) > 0)  and (nr_telef_beneficiario_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(444260); -- DDD do telefone do benefici�rio n�o informado! Favor verificar!
	
elsif	(nr_telef_beneficiario_p = '()') then	
	nr_ddd_telef_benef_w		:= '';
	
else
	nr_ddd_telef_benef_w 		:= SubStr(nr_telef_beneficiario_p, InStr(nr_telef_beneficiario_p,'(',1)+1 ,2);
end if;

if	(InStr(nr_telef_secretaria_p,'()',1) > 0)  and (nr_telef_secretaria_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(449072); -- DDD do telefone da secret�ria n�o informado! Favor verificar!
end if;

nr_ddd_celular_w		:= elimina_caractere_especial(nr_ddd_celular_w);
nr_telef_celular_benef_w	:= elimina_caractere_especial(nr_telef_celular_benef_w);
nr_ddd_telef_benef_w		:= elimina_caractere_especial(nr_ddd_telef_benef_w);
nr_telef_beneficiario_w		:= elimina_caractere_especial(nr_telef_beneficiario_w);

nr_ddd_celular_w		:= pls_elimina_caracteres(nr_ddd_celular_w);
nr_telef_celular_benef_w	:= pls_elimina_caracteres(nr_telef_celular_benef_w);
nr_ddd_telef_benef_w		:= pls_elimina_caracteres(nr_ddd_telef_benef_w);
nr_telef_beneficiario_w		:= pls_elimina_caracteres(nr_telef_beneficiario_w);

update	pls_auditoria
set	nr_telef_secretaria		= nr_telef_secretaria_p,
	nm_secretaria			= nm_secretaria_p,
	nr_telef_beneficiario		= nr_telef_beneficiario_p,
	nr_telef_celular_benef		= nr_telef_celular_benef_p,
	ds_email_beneficiario		= ds_email_beneficiario_p
where 	nr_sequencia	= nr_seq_analise_p;

if 	(ie_parametro_regra_p = 'CR') then
	if	(nvl(nr_seq_regra_atualizacao_w, 0) 	= 0) then	
		update	pessoa_fisica
		set	nr_ddd_celular		= nr_ddd_celular_w, 
			nr_telefone_celular	= nr_telef_celular_benef_w,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
		update	compl_pessoa_fisica
		set	ds_email		= ds_email_beneficiario_p, 
			nr_ddd_telefone		= nr_ddd_telef_benef_w, 
			nr_telefone		= nr_telef_beneficiario_w,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	cd_pessoa_fisica = cd_pessoa_fisica_w
		and	ie_tipo_complemento = 1;
		
	else 	
		pls_solic_alteracao_aut_benef(	cd_pessoa_fisica_w, nr_ddd_celular_w, nr_telef_celular_benef_w, 
						nr_ddd_telef_benef_w, nr_telef_beneficiario_w, ds_email_beneficiario_p,
						cd_estabelecimento_p, nm_usuario_p);
	end if;

else
	pls_solic_alteracao_aut_benef(	cd_pessoa_fisica_w, nr_ddd_celular_w, nr_telef_celular_benef_w, 
					nr_ddd_telef_benef_w, nr_telef_beneficiario_w, ds_email_beneficiario_p,
					cd_estabelecimento_p, nm_usuario_p);
end if;

commit;

end pls_atualiza_dados_benef_aut;
/
