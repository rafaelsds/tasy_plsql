create or replace
procedure GERAR_GLOSA_AUTOR_PLS
			(cd_estabelecimento_p		in number,
			nm_usuario_p			in varchar2,
			cd_estab_pls_p			in number,
			nr_sequencia_autor_p		in number,
			nr_seq_guia_plano_p		in number) is

nr_seq_motivo_glosa_w		number(10);
ds_observacao_w			varchar2(4000);
nr_seq_guia_proc_w		number(10);
nr_seq_guia_mat_w		number(10);
cd_procedimento_imp_w		number(15);
cd_material_imp_w		number(15);
nr_seq_proc_autor_w		number(15);
nr_seq_mat_autor_w		number(15);
ie_status_w			varchar2(2);
nr_seq_estagio_w		number(10);
cd_guia_w			varchar2(20);
cd_senha_w			varchar2(20);
nr_seq_procecimento_w		number(10);
nr_seq_material_w		number(10);
nr_seq_guia_plano_w		number(10);
ie_atualiza_guia_negada_w	varchar2(15) := 'N';
nr_seq_agenda_consulta_w	number(10,0);

cursor c01 is
select	nr_seq_motivo_glosa,
	ds_observacao
from	pls_guia_glosa
where	nr_seq_guia 	= nr_seq_guia_plano_w;

cursor c02 is
select	nr_sequencia,
	cd_procedimento_imp
from	pls_guia_plano_proc
where	nr_seq_guia	= nr_seq_guia_plano_w;

cursor c03 is
select	nr_seq_motivo_glosa,
	ds_observacao
from	pls_guia_glosa
where	nr_seq_guia_proc 	= nr_seq_guia_proc_w;

cursor c04 is
select	nr_sequencia
from	procedimento_autorizado
where	nr_sequencia_autor	= nr_sequencia_autor_p;

cursor c05 is
select	nr_sequencia
from	material_autorizado
where	nr_sequencia_autor	= nr_sequencia_autor_p;

begin

obter_param_usuario(3004,126,obter_perfil_Ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_atualiza_guia_negada_w);

GERAR_AUTORIZACAO_PLS(cd_estabelecimento_p, nr_sequencia_autor_p, nm_usuario_p);

open C04;
loop
fetch C04 into
	nr_seq_procecimento_w;
exit when C04%notfound;
	begin
	GERAR_AUTORIZACAO_PROC_PLS(cd_estabelecimento_p, nm_usuario_p, nr_sequencia_autor_p, nr_seq_procecimento_w);
	end;
end loop;
close C04;

open C05;
loop
fetch C05 into
	nr_seq_material_w;
exit when C05%notfound;
	begin
	GERAR_AUTORIZACAO_MAT_PLS(cd_estabelecimento_p, nm_usuario_p, nr_sequencia_autor_p, nr_seq_material_w);
	end;
end loop;
close C05;

select	max(nr_seq_guia_plano),
	max(nr_seq_agenda_consulta)
into	nr_seq_guia_plano_w,
	nr_seq_agenda_consulta_w
from	autorizacao_convenio
where	nr_sequencia	= nr_sequencia_autor_p;

nr_seq_guia_plano_w	:= nvl(nr_seq_guia_plano_p,nr_seq_guia_plano_w);

if	(nr_seq_guia_plano_w is not null) then

	pls_consistir_guia_importacao( nr_seq_guia_plano_w, cd_estab_pls_p, nm_usuario_p);

	select	max(ie_status),
		max(cd_guia),
		max(cd_senha)
	into	ie_status_w,
		cd_guia_w,
		cd_senha_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_plano_w;

	select	max(nr_sequencia)
	into	nr_seq_estagio_w
	from	estagio_autorizacao
	where	ie_situacao	= 'A'
	and	ie_interno	= decode(ie_status_w,'1','10','2','5','3','90')
	and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;


	if	(ie_status_w  <> '3') or
		(nvl(ie_atualiza_guia_negada_w,'N') = 'S') then

		update	autorizacao_convenio
		set	cd_autorizacao	= nvl(cd_guia_w,cd_autorizacao),
			cd_senha	= nvl(cd_senha_w,cd_senha)
		where	nr_sequencia	= nr_sequencia_autor_p;
		
		if	(nvl(nr_seq_agenda_consulta_w,0) <> 0) and
			(ie_atualiza_guia_negada_w = 'S')then
		
		update	agenda_consulta
		set	nr_doc_convenio	= nvl(cd_guia_w,nr_doc_convenio),
			cd_senha	= nvl(cd_senha_w,cd_senha)
		where	nr_sequencia	= nr_seq_agenda_consulta_w;
		
		end if;

	end if;

	if	(nr_seq_estagio_w is not null) then
		atualizar_autorizacao_convenio(nr_sequencia_autor_p,nm_usuario_p,nr_seq_estagio_w,'N','N','S');
	end if;

	open c01;
	loop
	fetch c01 into
		nr_seq_motivo_glosa_w,
		ds_observacao_w;
	exit when c01%notfound;

		insert into tiss_retorno_autorizacao
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_autorizacao,
			dt_evento,
			nr_seq_motivo_glosa,
			ds_erro_retorno,
			nr_seq_proc_autor)
		values	(tiss_retorno_autorizacao_seq.nextval,
			sysdate,
			nm_usuario_p,
			nr_sequencia_autor_p,
			sysdate,
			nr_seq_motivo_glosa_w,
			ds_observacao_w,
			null);

	end loop;
	close c01;

	open c02;
	loop
	fetch c02 into
		nr_seq_guia_proc_w,
		cd_procedimento_imp_w;
	exit when c02%notfound;

		select	max(nr_seq_proc_autor)
		into	nr_seq_proc_autor_w
		from	tiss_proc_solicitado_autor_v
		where	nr_sequencia_autor				= nr_sequencia_autor_p
		and	nvl(cd_procedimento_convenio, cd_procedimento)	= cd_procedimento_imp_w;

		open c03;
		loop
		fetch c03 into
			nr_seq_motivo_glosa_w,
			ds_observacao_w;
		exit when c03%notfound;
			insert into tiss_retorno_autorizacao
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_autorizacao,
				dt_evento,
				nr_seq_motivo_glosa,
				ds_erro_retorno,
				nr_seq_proc_autor)
			values	(tiss_retorno_autorizacao_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_sequencia_autor_p,
				sysdate,
				nr_seq_motivo_glosa_w,
				ds_observacao_w,
				nr_seq_proc_autor_w);
		end loop;
		close c03;

	end loop;
	close c02;
end if;

commit;

end GERAR_GLOSA_AUTOR_PLS;
/
