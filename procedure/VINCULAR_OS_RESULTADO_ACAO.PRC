create or replace
procedure Vincular_OS_Resultado_Acao(
			nm_usuario_p		Varchar2, 
			nr_seq_ordem_p		number,
			nr_sequencia_p		number) is 

begin
update	teste_soft_exec_acao_res               
set     	nr_seq_ordem_servico = nr_seq_ordem_p,  
	dt_atualizacao = sysdate,              
	nm_usuario = nm_usuario_p           
where	nr_sequencia = nr_sequencia_p;
commit;

end Vincular_OS_Resultado_Acao;
/