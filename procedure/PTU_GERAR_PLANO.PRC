create or replace
procedure ptu_gerar_plano
			(	nr_seq_empresa_p	number,
				nr_seq_intercambio_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2,
				ie_gerarcao_p		varchar2) is 

/*
ie_gerarcao_p
S - Gerar todos os beneficiários da empresa
N - Não gerar os beneficiários da empresa
*/				
				
nr_sequencia_w			number(10);
cd_plano_origem_w		varchar2(6);
nr_ind_reembolso_w		number(10);
nr_seq_plano_w			number(10);
cd_estabelecimento_w		number(10);
ie_situacao_w			varchar2(2) := 'A';
nr_seq_plano_ww			number(10);
qt_plano_w			number(10);
ie_repasse_w			varchar2(10);

Cursor C01 is
	select	nr_seq_plano
	from	ptu_intercambio_benef
	where	nr_seq_empresa	= nr_seq_empresa_p
	and	nr_seq_plano is not null
	and	ie_status = 'A'
	group by nr_seq_plano;

begin

select	max(ie_repasse)
into	ie_repasse_w
from	ptu_intercambio_benef
where	nr_seq_empresa	= nr_seq_empresa_p;

open C01;
loop
fetch C01 into
	nr_seq_plano_w;
exit when C01%notfound;
	begin
	
	select	count(*)
	into	qt_plano_w
	from	pls_intercambio_plano
	where	nr_seq_intercambio	= nr_seq_intercambio_p
	and	nr_seq_plano		= nr_seq_plano_w;
	
	if	(qt_plano_w = 0) then
		select	pls_intercambio_plano_seq.nextval
		into	nr_seq_plano_ww
		from	dual;
		
		insert into pls_intercambio_plano
			(nr_sequencia, nr_seq_intercambio, nr_seq_plano, 
			cd_plano_origem, dt_atualizacao, nm_usuario, 
			dt_atualizacao_nrec, nm_usuario_nrec)
		values	(nr_seq_plano_ww, nr_seq_intercambio_p, nr_seq_plano_w,
			cd_plano_origem_w, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p);

		if	(ie_repasse_w = 'P') then
			ptu_gerar_tabelas_intercambio(nr_seq_plano_ww,nr_seq_empresa_p,cd_estabelecimento_p,nm_usuario_p);
		end if;
	end if;
		
	if	(ie_gerarcao_p = 'S') then
		ptu_gerar_benef(nr_seq_empresa_p, nr_seq_plano_w, nr_seq_intercambio_p, nm_usuario_p,cd_estabelecimento_p);
	end if;
	end;
end loop;
close C01;

end ptu_gerar_plano;
/
