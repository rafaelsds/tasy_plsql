create or replace
procedure ajustar_cirurgia_partic(
			nr_sequencia_p		number,
			nr_cirurgia_p		number) is

qt_cir_partic_w		number(10);

pragma autonomous_transaction;			

begin

select	count(*)
into	qt_cir_partic_w
from	cirurgia_participante
where	nr_seq_procedimento = nr_sequencia_p
and 	nr_cirurgia	= nr_cirurgia_p;
	
if	(qt_cir_partic_w > 0) then
	wheb_usuario_pck.set_ie_executar_trigger('N');

	update	cirurgia_participante
	set	nr_seq_procedimento = null
	where 	nr_seq_procedimento = nr_sequencia_p
	and 	nr_cirurgia	= nr_cirurgia_p;
	
	wheb_usuario_pck.set_ie_executar_trigger('S');
end if;

commit;

end ajustar_cirurgia_partic;
/
