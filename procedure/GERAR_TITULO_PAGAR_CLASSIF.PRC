create or replace 
procedure gerar_titulo_pagar_classif(
		ds_lista_titulos_p	in	varchar2,
		nr_titulo_destino_p	in	number,
		nm_usuario_p	in	varchar2) is

nr_sequencia_w			number(10,0);
vl_titulo_w				number(15,2);
cd_conta_contabil_w		varchar2(20);
cd_centro_custo_w		number(10,0);
nr_seq_conta_financ_w	number(10,0);
nr_seq_trans_fin_w		number(10,0);
nr_contrato_w			number(10,0);
vl_desconto_w			number(15,2);
nr_titulo_w				number(10);
nr_seq_produto_w		number(10);
ds_mensagem_w			varchar2(255);

cursor c01 is
select	cd_conta_contabil,
	cd_centro_custo,
	nr_seq_conta_financ,
	vl_titulo,
	nr_seq_trans_fin,
	nr_contrato,
	vl_desconto,
	nr_seq_produto
from	titulo_pagar_classif
where	(' ' || replace(ds_lista_titulos_p, ',' , ' ') || ' ')  like ('% ' || to_char(nr_titulo) || ' %');

cursor c02 is
select	nr_titulo
from	titulo_pagar
where	(' ' || replace(ds_lista_titulos_p, ',' , ' ') || ' ')  like ('% ' || to_char(nr_titulo) || ' %');

begin

open c01;
loop
fetch c01 into
	cd_conta_contabil_w,
	cd_centro_custo_w,
	nr_seq_conta_financ_w,
	vl_titulo_w,
	nr_seq_trans_fin_w,
	nr_contrato_w,
	vl_desconto_w,
	nr_seq_produto_w;
exit when c01%notfound;
	begin
	update	titulo_pagar_classif
	set	vl_desconto = vl_desconto + vl_desconto_w,
		vl_titulo = vl_titulo + vl_titulo_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_titulo = nr_titulo_destino_p
	and	nvl(cd_conta_contabil,0) = nvl(cd_conta_contabil_w,0)
	and	nvl(cd_centro_custo,0) = nvl(cd_centro_custo_w,0)
	and	nvl(nr_seq_conta_financ,0) = nvl(nr_seq_conta_financ_w,0)
	and	nvl(nr_seq_trans_fin,0) = nvl(nr_seq_trans_fin_w,0)
	and	nvl(nr_contrato,0) = nvl(nr_contrato_w,0)
	and	nvl(nr_seq_produto,0) = nvl(nr_seq_produto_w,0);
	
	if	(sql%notfound) then
		begin
		select	nvl(max(nr_sequencia), 0) + 1
		into	nr_sequencia_w
		from	titulo_pagar_classif
		where	nr_titulo	= nr_titulo_destino_p;

		insert into titulo_pagar_classif(
			nr_titulo,
			nr_sequencia,
			vl_titulo,
			dt_atualizacao,
			nm_usuario,
			cd_conta_contabil,
			cd_centro_custo,
			nr_seq_conta_financ,
			nr_seq_trans_fin,
			nr_contrato,
			nr_seq_produto,
			vl_desconto,
			vl_original)
		values(	nr_titulo_destino_p,
			nr_sequencia_w,
			vl_titulo_w,
			sysdate,
			nm_usuario_p,
			cd_conta_contabil_w,
			cd_centro_custo_w,
			nr_seq_conta_financ_w,
			nr_seq_trans_fin_w,
			nr_contrato_w,
			nr_seq_produto_w,
			vl_desconto_w,
			0);
		end;
	end if;
	end;	
end loop;
close c01;

/* francisco - 142333 - troquei pelo cursor para atualizar o saldo e trocar a situacao */
open c02;
loop
fetch c02 into
	nr_titulo_w;
exit when c02%notfound;

	ds_mensagem_w := substr (wheb_mensagem_pck.get_texto(304597),1,255);
  
  
	update	titulo_pagar
	set	ds_observacao_titulo	= substr(substr(ds_observacao_titulo, 1, 3733) || ' ' || ds_mensagem_w || ' ' || to_char(nr_titulo_destino_p), 1,4000), --substr de 3733 pois ser�o 4000 - 255 da ds_mensagem_W - 10 do t�tulo (maximo) - 2 espa�os em branco
		nr_titulo_transf		= nr_titulo_destino_p
	where	nr_titulo			= nr_titulo_w;

	atualizar_saldo_tit_pagar(nr_titulo_w,nm_usuario_p);
	gerar_w_tit_pag_imposto(nr_titulo_w, nm_usuario_p);
end loop;
close c02;	

--commit;

end gerar_titulo_pagar_classif;
/
