create or replace procedure  pfcs_mpl_lhs_tl_trans_review(
   nr_seq_indicator_p   number,
    cd_estabelecimento_p number,
    nm_usuario_p         varchar2
) is

qt_total_w						number(15):= 0;
pfcs_panel_detail_seq_w			pfcs_panel_detail.nr_sequencia%type;
nr_seq_panel_w					pfcs_panel.nr_sequencia%type;
qt_time_alarms_rule_w			pfcs_telemetry_config.qt_time_alarm_trend_rule%type;

--Cursor to fetch location
cursor 	cur_get_unit_for_pfcs is
		select  sa.ds_setor_atendimento ds_department
		from    pfcs_encounter enc,
				pfcs_patient pat,
				pfcs_encounter_location el,
				unidade_atendimento uni,
				setor_atendimento sa
		where   enc.si_status in ('PLANNED', 'ARRIVED')
		and     enc.nr_seq_patient = pat.nr_sequencia
		and     pat.ie_active = '1'
		and     el.nr_seq_encounter = enc.nr_sequencia
		and     el.nr_seq_location = uni.nr_seq_location
		and     uni.ie_situacao = 'A'
		and     uni.cd_setor_atendimento = sa.cd_setor_atendimento
		and     sa.ie_situacao = 'A'
		and     sa.cd_classif_setor in ('1','3','4','9','11','12')
		and     sa.cd_estabelecimento_base = cd_estabelecimento_p		
		group by sa.ds_setor_atendimento;

cursor 	cur_get_tot_trans_pfcs(location_p varchar)  is
		select 	enc.id_encounter nr_encounter,
				pat.patient_id id_patient,
				pfcs_get_human_name(enc.nr_seq_patient, 'Patient') nm_patient,
				pfcs_get_tele_time(pd.nr_sequencia, 'S') dt_recommendation_time,
				enc.ds_reason ds_diagnosis,
				pat.birthdate dt_dob,
				trunc((months_between(nvl(pat.deceased_date, sysdate), pat.birthdate)/12)) qt_idade_paciente,
				pat.gender ds_gender,
				enc.ds_classification ds_classification,
				enc.period_start dt_entrance,
				pfcs_get_human_name(pfcs_get_practitioner_seq(enc.nr_sequencia, '405279007'), 'Practitioner') ds_attending_physician,
				pfcs_get_code_status_tl(pat.nr_sequencia,'S')   ds_dnr_status,
			    sa.ds_setor_atendimento ds_department,
                decode(sa.ds_setor_atendimento, null, uni.cd_unidade_basica || '-' || uni.cd_unidade_compl, sa.ds_setor_atendimento || '-' || uni.cd_unidade_basica || '-' || uni.cd_unidade_compl) ds_bed,
				pd.ds_device_name ds_equipamento,
				pd.id_device cd_equipamento,
				pfcs_get_alarms_count(pat.nr_sequencia, 'A0402', qt_time_alarms_rule_w) nr_yellow_alarm_count,
				pfcs_get_alarms_count(pat.nr_sequencia, 'A0401', qt_time_alarms_rule_w) nr_red_alarm_count,
				pfcs_get_battery_status(pat.nr_sequencia) battery_status,
				pfcs_get_edi_score(enc.nr_sequencia) as nr_edi_score,
				pfcs_get_alarms_trends(pat.nr_sequencia, enc.nr_sequencia) ds_alarms_trends,
				pfcs_obs_contributor_pck.get_edi_vital_warnings(enc.nr_sequencia) ds_edi_vitals_warn,
				pfcs_obs_contributor_pck.get_edi_contributors(enc.nr_sequencia) ds_edi_contrb
		from	pfcs_service_request sr,
				pfcs_encounter enc,
				pfcs_patient pat,
				pfcs_device pd,
				pfcs_patient_flag pf,
				pfcs_encounter_location encloc,
				unidade_atendimento uni,
				setor_atendimento sa
		where	sr.si_status = 'COMPLETED'
		and 	sr.cd_service = 'E0404'
		and 	sr.nr_seq_encounter = enc.nr_sequencia
		and 	enc.si_status in ('PLANNED', 'ARRIVED')
		and 	enc.nr_seq_patient = pat.nr_sequencia
		and 	pat.ie_active = '1'
		and 	pat.nr_sequencia = pd.nr_seq_patient
		and 	pd.si_status = 'ACTIVE'
		and 	pd.ds_device_type = 'Monitor'
		and 	pfcs_get_tele_time(pd.nr_sequencia, 'S') > (24 * 60)
		and 	pf.nr_seq_patient = pat.nr_sequencia
		and 	pf.nr_seq_device = pd.nr_sequencia
		and 	pf.nr_seq_encounter = enc.nr_sequencia
		and 	pf.cd_flag in ('A0401', 'A0402')
		and 	enc.nr_sequencia = encloc.nr_seq_encounter
		and 	encloc.nr_seq_location =uni.nr_seq_location
		and 	uni.ie_situacao = 'A'
		and 	uni.cd_setor_atendimento = sa.cd_setor_atendimento
		and 	sa.ie_situacao = 'A'
		and 	sa.cd_classif_setor in ('1','3','4','9','11','12')
		and 	sa.cd_estabelecimento_base = cd_estabelecimento_p
		and 	sa.ds_setor_atendimento = location_p;

begin
-- Get hours configurable from telemetry settings
	select	nvl(max(qt_time_alarm_trend_rule),12) into qt_time_alarms_rule_w from pfcs_telemetry_config;

	for r_c02 in cur_get_unit_for_pfcs loop
	begin
		qt_total_w := 0;
		for r_c01 in cur_get_tot_trans_pfcs(r_c02.ds_department) loop
		begin
			qt_total_w := qt_total_w + 1;

			select pfcs_panel_detail_seq.nextval into pfcs_panel_detail_seq_w from dual;

			pfcs_pck_v2.pfcs_insert_details(
					nr_seq_indicator_p => nr_seq_indicator_p,
					nr_seq_operational_level_p	=> cd_estabelecimento_p,
					nm_usuario_p => nm_usuario_p,
					nr_panel_detail_seq_p => pfcs_panel_detail_seq_w,
					nr_encounter_p => r_c01.nr_encounter,
					id_patient_p => r_c01.id_patient,
					nm_patient_p => r_c01.nm_patient,
					dt_birthdate_p => r_c01.dt_dob,
					ds_primary_diagnosis_p => r_c01.ds_diagnosis,
					ds_gender_p => r_c01.ds_gender,
					ds_dnr_status_p => r_c01.ds_dnr_status,
					qt_time_telemetry_p => r_c01.dt_recommendation_time,
					ds_classification_p => r_c01.ds_classification,
					dt_entrance_p => r_c01.dt_entrance,
					ds_service_line_p => r_c01.ds_attending_physician,
					cd_department_p => r_c01.ds_department,
					ds_department_p => r_c01.ds_department,
					ds_bed_location_p => r_c01.ds_bed,
					qt_red_alarms_p => r_c01.nr_red_alarm_count,
					qt_yellow_alarms_p => r_c01.nr_yellow_alarm_count,
					cd_equipamento_p => r_c01.cd_equipamento,
					ds_device_p => r_c01.ds_equipamento,
					ds_battery_status_p => r_c01.battery_status,
					ds_age_range_p => r_c01.qt_idade_paciente,
					ds_alarms_trends_p => r_c01.ds_alarms_trends,
					nr_edi_score_p => r_c01.nr_edi_score,
					ds_edi_vitals_warn_p =>  r_c01.ds_edi_vitals_warn,
					ds_edi_contrb_p =>  r_c01.ds_edi_contrb);
		end;
		end loop;

		pfcs_pck_v2.pfcs_generate_results(
			vl_indicator_p => qt_total_w,
			ds_reference_value_p => r_c02.ds_department,
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => cd_estabelecimento_p,
			nm_usuario_p => nm_usuario_p,
			nr_seq_panel_p => nr_seq_panel_w);
	end;
	end loop;

	pfcs_pck_v2.pfcs_update_detail (
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_panel_p => nr_seq_panel_w,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p );

	pfcs_pck_v2.pfcs_activate_records (
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => cd_estabelecimento_p,
		nm_usuario_p => nm_usuario_p );

end pfcs_mpl_lhs_tl_trans_review;
/
