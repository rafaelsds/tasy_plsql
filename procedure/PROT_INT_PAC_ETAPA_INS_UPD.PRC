CREATE OR REPLACE PROCEDURE prot_int_pac_etapa_ins_upd (
    nr_seq_prot_int_pac_etapa_p protocolo_int_pac_etapa.nr_sequencia%TYPE,
    ds_etapa_p protocolo_int_pac_etapa.ds_etapa%TYPE,
    nr_dias_previsto_p protocolo_int_pac_etapa.nr_dias_previsto%TYPE,
    nm_usuario_p protocolo_int_pac_etapa.nm_usuario%TYPE,
    nr_seq_protocolo_int_pac_p protocolo_int_paciente.nr_sequencia%TYPE,
    ds_etapa_paciente_p protocolo_int_pac_etapa.ds_etapa_paciente%TYPE,
    ie_ocultar_visao_paciente_p	protocolo_int_pac_etapa.ie_ocultar_visao_paciente%type,
    ie_evento_sumario_p protocolo_int_pac_etapa.ie_evento_sumario%type default null,
    nr_dia_inicial_p protocolo_int_pac_etapa.nr_dia_inicial%type default null,
    nr_dia_final_p protocolo_int_pac_etapa.nr_dia_final%type default null,
    nr_ordem_inicial_p protocolo_int_pac_etapa.nr_ordem_inicial%type default null,
    nr_ordem_final_p protocolo_int_pac_etapa.nr_ordem_final%type default null
) IS

nr_seq_protocolo_int_pac_w protocolo_int_paciente.nr_sequencia%TYPE;
min_nr_seq_etapa_w protocolo_int_pac_etapa.nr_sequencia%TYPE;
dt_inicial_previsto_w protocolo_int_pac_etapa.dt_inicial_previsto%TYPE;
dt_final_previsto_w protocolo_int_pac_etapa.dt_final_previsto%TYPE;
nr_seq_prot_int_pac_etapa_w protocolo_int_pac_etapa.nr_sequencia%TYPE;

BEGIN

-- Protocolo da etapa
SELECT
    dt_inicial_previsto
INTO
    dt_inicial_previsto_w
FROM
    protocolo_int_paciente
WHERE
    nr_sequencia = nr_seq_protocolo_int_pac_p;

if	(nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP') then

  if (ie_evento_sumario_p = 'S') then

    dt_final_previsto_w := dt_inicial_previsto_w;

  else 

    dt_final_previsto_w := dt_inicial_previsto_w + nvl(nr_dia_final_p,1) - 1;
    dt_inicial_previsto_w := dt_inicial_previsto_w + nvl(nr_dia_inicial_p,1) - 1;

  end if;

else

  -- Primeira etapa do protocolo
  SELECT MIN(nr_sequencia)
  INTO min_nr_seq_etapa_w
  FROM protocolo_int_pac_etapa
  WHERE nr_seq_protocolo_int_pac = nr_seq_protocolo_int_pac_p;

  -- NAO e a primeira etapa do protocolo
  IF (min_nr_seq_etapa_w IS NOT NULL AND (min_nr_seq_etapa_w <> nr_seq_prot_int_pac_etapa_p OR nr_seq_prot_int_pac_etapa_p IS NULL)) THEN 
      SELECT dt_final_previsto + 1
      INTO dt_inicial_previsto_w
      FROM protocolo_int_pac_etapa
      WHERE nr_sequencia = (
          SELECT MAX(nr_sequencia)
          FROM protocolo_int_pac_etapa
          WHERE (nr_seq_prot_int_pac_etapa_p IS NOT NULL AND nr_sequencia < nr_seq_prot_int_pac_etapa_p)
          OR (nr_seq_prot_int_pac_etapa_p IS NULL AND nr_seq_protocolo_int_pac = nr_seq_protocolo_int_pac_p)
      );
  END IF;

  dt_final_previsto_w := dt_inicial_previsto_w + nvl(nr_dias_previsto_p,1) - 1;

end if;

IF (nr_seq_prot_int_pac_etapa_p IS NULL) THEN -- Insert
    SELECT protocolo_int_pac_etapa_seq.nextval
    INTO nr_seq_prot_int_pac_etapa_w
    FROM dual;

    INSERT INTO protocolo_int_pac_etapa (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_seq_protocolo_int_pac,
        ds_etapa,
        nr_dias_previsto,
        dt_inicial_previsto,
        dt_final_previsto,
        ie_ocultar_visao_paciente,
        ie_evento_sumario,
        nr_dia_inicial,
        nr_dia_final,
        nr_ordem_inicial,
        nr_ordem_final
    ) VALUES (
        nr_seq_prot_int_pac_etapa_w,
        SYSDATE,
        nm_usuario_p,
        SYSDATE,
        nm_usuario_p,
        nr_seq_protocolo_int_pac_p,
        ds_etapa_p,
        nr_dias_previsto_p,
        dt_inicial_previsto_w,
        dt_final_previsto_w,
		    ie_ocultar_visao_paciente_p,
        ie_evento_sumario_p,
        nr_dia_inicial_p,
        nr_dia_final_p,
        nr_ordem_inicial_p,
        nr_ordem_final_p
    );
    if	(nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP') then

      if (nvl(ie_evento_sumario_p,'N') = 'N') then

        execute immediate   'begin link_event_from_sum_pac(:1, :2, :3, :4, :5, :6, :7); end;'
        using   nr_seq_prot_int_pac_etapa_w,
                nr_dia_inicial_p,
                nr_dia_final_p,
                nr_ordem_inicial_p,
                nr_ordem_final_p,
                nr_seq_protocolo_int_pac_p,
                0;

      end if;

    end if;
ELSE -- Update
    UPDATE protocolo_int_pac_etapa
    SET dt_inicial_previsto = dt_inicial_previsto_w,
        dt_final_previsto = dt_final_previsto_w,
        nr_dias_previsto = nr_dias_previsto_p,
        ds_etapa = ds_etapa_p,
        nm_usuario = nm_usuario_p,
        dt_atualizacao = SYSDATE,
        ds_etapa_paciente = ds_etapa_paciente_p,
		    ie_ocultar_visao_paciente	=	ie_ocultar_visao_paciente_p,
        ie_evento_sumario = ie_evento_sumario_p,
        nr_dia_inicial = nr_dia_inicial_p,
        nr_dia_final = nr_dia_final_p,
        nr_ordem_inicial = nr_ordem_inicial_p,
        nr_ordem_final = nr_ordem_final_p
    WHERE nr_sequencia = nr_seq_prot_int_pac_etapa_p;

    if	(nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP') then

      if (nvl(ie_evento_sumario_p,'N') = 'N') then

      execute immediate   'begin link_event_from_sum_pac(:1, :2, :3, :4, :5, :6, :7); end;'
        using   nr_seq_prot_int_pac_etapa_p,
                nr_dia_inicial_p,
                nr_dia_final_p,
                nr_ordem_inicial_p,
                nr_ordem_final_p,
                nr_seq_protocolo_int_pac_p,
                1;

      end if;

    end if;

END IF;

COMMIT;

END prot_int_pac_etapa_ins_upd;
/