create or replace
procedure gerar_info_curva_abc_xyz(	cd_estab_p		number,
				cd_local_estoque_p		number,
				nr_seq_marca_p		number,
				cd_fabricante_p		number,
				dt_inicial_p		date,
				dt_final_p		date,
				cd_m_grupo_material_p varchar2,
				cd_m_subgrupo_material_p varchar2,
				cd_m_classe_material_p varchar2,
				cd_m_local_estoque_p varchar2,
				cd_m_centro_custo_p varchar2,
				ie_nome_comercial_p		varchar2,
				ie_media_valores_p		varchar2,
				ie_saldo_atual_p		varchar2,
				ie_compra_comercial_p	varchar2,
				ie_agrupa_meses_p		varchar2,
				ie_saldo_zero_p		varchar2,
				ie_imprime_local_p		varchar2,
				ie_fornec_ult_compra_p	varchar2,
				ie_saldo_contabil_p		varchar2,
				ie_imprime_generico_p	varchar2,
				ie_curva_p		number,
				ie_local_estoque_p		number,
				ie_consignado_p		number,
				ie_padronizado_p		number,
				ie_situacao_p		number,
				ie_valor_comercial_p	number,
				vl_a_x_p			number,
				vl_b_y_p			number,
				vl_c_z_p			number,
				ie_imprime_a_x_p		varchar2,
				ie_imprime_b_y_p		varchar2,
				ie_imprime_c_z_p		varchar2,
				ie_performace_p		varchar2 default 'S',  -- VERIFICAR, PARAMETRO SERVE PARA SUBSTITUIUR MENSAGEM DE AVISO NO TASYREL
				nm_usuario_p		varchar2) is 

ds_quebra_w		varchar2(15) := chr(10) || chr(13);
ds_comando_w		varchar(10000);
c01_w			integer;
retorno_w			number(10);

dt_inicial_w		date;
dt_final_w		date;

nr_meses_w		number(10);
vl_movimento_total_w	number(20,4);
qt_movimento_total_w	number(20,4);

nr_ordem_campo_w		number(5) := 15;
nr_ordem_campo_01_w		number(5); -- CD_LOCAL_ESTOQUE
nr_ordem_campo_02_w		number(5); -- DS_LOCAL_ESTOQUE
nr_ordem_campo_03_w		number(5); -- NR_SEQ_MARCA
nr_ordem_campo_04_w		number(5); -- DS_MARCA
nr_ordem_campo_05_w		number(5); -- DS_MATERIAL_GENERICO
nr_ordem_campo_06_w		number(5); -- CD_MATERIAL_GENERICO
nr_ordem_campo_07_w		number(5); -- VL_SALDO
nr_ordem_campo_08_w		number(5); -- VL_SALDO_CONTABIL
nr_ordem_campo_09_w		number(5); -- DS_LOCAL
nr_ordem_campo_10_w		number(5); -- DS_FORNECEDOR

nr_ordem_w		number(10) := 0;

cd_grupo_material_w		number(3);
ds_grupo_material_w		varchar2(255);
cd_subgrupo_material_w	number(3);
ds_subgrupo_material_w	varchar2(255);
cd_classe_material_w	number(5);
ds_classe_material_w	varchar2(255);
cd_centro_custo_w		number(8);
ds_material_w		varchar2(255);
cd_material_w		number(6);
ie_tipo_material_w		varchar2(3);
vl_ultima_compra_w		number(13,4);
cd_unidade_medida_estoque_w	varchar2(30);
ie_situacao_w		varchar2(1);
qt_movimento_w		number(20,4);
vl_movimento_w		number(20,4);
cd_local_estoque_w		number(4);
ds_local_estoque_w		varchar2(40);
nr_seq_marca_w		number(10);
ds_marca_w		varchar2(80);
ds_material_generico_w	varchar2(255);
cd_material_generico_w	number(6);
vl_saldo_w		number(15,4);
vl_saldo_contabil_w		number(15,4);
ds_local_w		varchar2(255);
ds_fornecedor_w		varchar2(100);
ds_curva_w		varchar2(1);

qt_acumulado_w		number(20,4) := 0;

nr_ordem_ww		number(10);
vl_movimento_ww		number(20,4);
qt_movimento_ww		number(20,4);
qt_acumulado_ww		number(20,4);
pr_valor_w		number(10,4);
pr_item_w		number(10,4);
pr_total_w		number(10);
ie_imprime_w		varchar2(1);

cd_grupo_mat_filtro_w	varchar2(4000);
ie_grupo_mat_contido_w  varchar2(1) := 'S';

cd_subgrupo_mat_filtro_w	varchar2(4000);
ie_subgrupo_mat_contido_w  varchar2(1) := 'S';

cd_classe_mat_filtro_w	varchar2(4000);
ie_classe_mat_contido_w  varchar2(1) := 'S';

cd_local_mat_filtro_w	varchar2(4000);
ie_local_mat_contido_w  varchar2(1) := 'S';

cd_centro_custo_mat_filtro_w	varchar2(4000);
ie_centro_custo_mat_contido_w  varchar2(1) := 'S';

Cursor C01 is
	select	a.nr_ordem,
		a.vl_movimento,
		a.qt_movimento,
		a.qt_acumulado
	from w_curva_abc_xyz a
	where a.nm_usuario = nm_usuario_p
	order by	a.nr_ordem;
	
-- retorno := TO_DATE('01/01/2016 12:00:00','DD/MM/YYYY HH24:MI:SS')
	function converte_data_varchar (dt_data_p	date) return varchar2 is
	
	ds_retorno_w	varchar2(60) := '';
	
	begin
	
	if (dt_data_p is not null) then
		ds_retorno_w := 'to_date(''' || to_char(dt_data_p,'dd/mm/yyyy hh24:mi:ss') || ''',''dd/mm/yyyy hh24:mi:ss'')';
	end if;
	
	return ds_retorno_w;
	
	end converte_data_varchar;
	
	
	function replaces_parametro_filtro(cd_param_w varchar2) return varchar2 is
	
	ds_retorno_w varchar2(4000) := '';
	begin
		ds_retorno_w := cd_param_w;
		ds_retorno_w := replace(ds_retorno_w,'-','');
		ds_retorno_w := replace(ds_retorno_w,'(','');
		ds_retorno_w := replace(ds_retorno_w,')','');
		ds_retorno_w := nvl(ds_retorno_w,'X');

	return ds_retorno_w;
	end replaces_parametro_filtro;

begin

pr_total_w := vl_a_x_p + vl_b_y_p + vl_c_z_p;

if	(pr_total_w <> 100) then
	if	(ie_curva_p = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(440349);
	elsif	(ie_curva_p = 1) then
		wheb_mensagem_pck.exibir_mensagem_abort(440366);
	end if;
end if;

if	(dt_inicial_p is not null) then
	begin
	
	dt_inicial_w := inicio_dia(dt_inicial_p);
	
	if	(dt_final_p is not null) then
		dt_final_w := fim_dia(last_day(dt_final_p));
	else
		dt_final_w := inicio_dia(dt_inicial_p);
	end if;
	
	end;
else
	dt_inicial_w := trunc(sysdate,'mm');
	dt_final_w := trunc(sysdate,'mm');
end if;

nr_meses_w := round((nvl(dt_final_p,dt_final_w) - nvl(dt_inicial_p,dt_final_w)) / 30) + 1;

cd_grupo_mat_filtro_w    := replace(cd_m_grupo_material_p,' ','');
cd_subgrupo_mat_filtro_w := replace(cd_m_subgrupo_material_p,' ','');
cd_classe_mat_filtro_w 	 := replace(cd_m_classe_material_p,' ','');
cd_local_mat_filtro_w 	 := replace(cd_m_local_estoque_p,' ','');
cd_centro_custo_mat_filtro_w   := replace(cd_m_centro_custo_p,' ','');

if	(substr(cd_grupo_mat_filtro_w,1,1) = '-') then
	ie_grupo_mat_contido_w := 'N';
end if;

if	(substr(cd_subgrupo_mat_filtro_w,1,1) = '-') then
	ie_subgrupo_mat_contido_w := 'N';
end if;

if	(substr(cd_classe_mat_filtro_w,1,1) = '-') then
	ie_classe_mat_contido_w := 'N';
end if;

if	(substr(cd_local_mat_filtro_w,1,1) = '-') then
	ie_local_mat_contido_w := 'N';
end if;

if	(substr(cd_centro_custo_mat_filtro_w,1,1) = '-') then
	ie_centro_custo_mat_contido_w := 'N';
end if;

cd_grupo_mat_filtro_w    := replaces_parametro_filtro(cd_grupo_mat_filtro_w);
cd_subgrupo_mat_filtro_w := replaces_parametro_filtro(cd_subgrupo_mat_filtro_w);
cd_classe_mat_filtro_w := replaces_parametro_filtro(cd_classe_mat_filtro_w);
cd_local_mat_filtro_w  := replaces_parametro_filtro(cd_local_mat_filtro_w);
cd_centro_custo_mat_filtro_w := replaces_parametro_filtro(cd_centro_custo_mat_filtro_w);

ds_comando_w := 'select	' || ds_quebra_w;

ds_comando_w := ds_comando_w || '	b.cd_grupo_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	b.ds_grupo_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	g.cd_subgrupo_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	g.ds_subgrupo_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	a.cd_classe_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	a.ds_classe_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	max(e.cd_centro_custo), ' || ds_quebra_w;

if (nvl(ie_nome_comercial_p,'N') = 'S') then
	ds_comando_w := ds_comando_w || '	substr(obter_desc_material(e.cd_material_comercial),1,255) ds_material, ' || ds_quebra_w;
else
	ds_comando_w := ds_comando_w || '	m.ds_material, ' || ds_quebra_w;
end if;

ds_comando_w := ds_comando_w || '	m.cd_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	m.ie_tipo_material, ' || ds_quebra_w;

if (ie_valor_comercial_p = 1) then
	ds_comando_w := ds_comando_w || '	avg(obter_dados_ultima_compra(e.cd_estabelecimento, e.cd_material, ''VU'')) vl_ultima_compra, ' || ds_quebra_w;
elsif	(ie_valor_comercial_p = 0) then
	if (nvl(ie_compra_comercial_p,'N') = 'N') then
		ds_comando_w := ds_comando_w || '	avg(obter_dados_ultima_compra(e.cd_estabelecimento, e.cd_material, ''VEC'')) vl_ultima_compra, ' || ds_quebra_w;
	else
		ds_comando_w := ds_comando_w || '	avg(obter_dados_ultima_compra(e.cd_estabelecimento, e.cd_material_comercial, ''VEC'')) vl_ultima_compra, ' || ds_quebra_w;
	end if;
else 
	ds_comando_w := ds_comando_w || '	0 vl_ultima_compra, ' || ds_quebra_w;
end if;

ds_comando_w := ds_comando_w || '	substr(obter_dados_material_estab(m.cd_material,c.cd_estabelecimento,''UME''),1,30) cd_unidade_medida_estoque, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	m.ie_situacao, ' || ds_quebra_w;

if (nvl(ie_media_valores_p,'N') = 'S') then
	ds_comando_w := ds_comando_w || '	dividir(sum(e.qt_estoque),avg(' || nr_meses_w || ')) qt_movimento, ' || ds_quebra_w;
	ds_comando_w := ds_comando_w || '	dividir(sum(e.vl_estoque),avg(' || nr_meses_w || ')) vl_movimento ' || ds_quebra_w;
else
	ds_comando_w := ds_comando_w || '	sum(e.qt_estoque) qt_movimento, ' || ds_quebra_w;
	ds_comando_w := ds_comando_w || '	sum(e.vl_estoque) vl_movimento ' || ds_quebra_w;
end if;

if (cd_local_estoque_p is not null or cd_m_local_estoque_p is not null) then
	ds_comando_w := ds_comando_w || '	,c.cd_local_estoque ' || ds_quebra_w;
	nr_ordem_campo_w := nr_ordem_campo_w + 1;
	nr_ordem_campo_01_w := nr_ordem_campo_w;

	ds_comando_w := ds_comando_w || '	,c.ds_local_estoque ' || ds_quebra_w;
	nr_ordem_campo_w := nr_ordem_campo_w + 1;
	nr_ordem_campo_02_w := nr_ordem_campo_w;
end if;

if (nr_seq_marca_p is not null) then
	ds_comando_w := ds_comando_w || '	,t.nr_sequencia nr_seq_marca ' || ds_quebra_w;
	nr_ordem_campo_w := nr_ordem_campo_w + 1;
	nr_ordem_campo_03_w := nr_ordem_campo_w;
	
	ds_comando_w := ds_comando_w || '	,s.ds_marca ' || ds_quebra_w;
	nr_ordem_campo_w := nr_ordem_campo_w + 1;
	nr_ordem_campo_04_w := nr_ordem_campo_w;
end if;

if (nvl(ie_imprime_generico_p,'N') = 'S') then
	ds_comando_w := ds_comando_w || '	,substr(obter_desc_mat_generico(m.cd_material),1,255) ds_material_generico ' || ds_quebra_w;
	nr_ordem_campo_w := nr_ordem_campo_w + 1;
	nr_ordem_campo_05_w := nr_ordem_campo_w;
	
	ds_comando_w := ds_comando_w || '	,m.cd_material_generico ' || ds_quebra_w;
	nr_ordem_campo_w := nr_ordem_campo_w + 1;
	nr_ordem_campo_06_w := nr_ordem_campo_w;
end if;

if ((nvl(ie_saldo_atual_p,'N') = 'S') or (nvl(ie_saldo_contabil_p,'N') = 'S')) then
	if (nvl(ie_saldo_atual_p,'N') = 'S') then
		case ie_consignado_p
			when 0 then 
				if (cd_local_estoque_p is not null) then
					if (nvl(ie_agrupa_meses_p,'N') = 'S') then
						ds_comando_w := ds_comando_w || '	,obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, e.cd_local_estoque, '
										|| converte_data_varchar(fim_dia(dt_final_w)) || ') vl_saldo ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_07_w := nr_ordem_campo_w;
					else
						ds_comando_w := ds_comando_w || '	,obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, e.cd_local_estoque, e.dt_mesano_referencia) vl_saldo ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_07_w := nr_ordem_campo_w;
					end if;
				else 
					if (nvl(ie_agrupa_meses_p,'N') = 'S') then
						ds_comando_w := ds_comando_w || '	,obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, null, '
										|| converte_data_varchar(fim_dia(dt_final_w)) || ') vl_saldo ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_07_w := nr_ordem_campo_w;
					else
						ds_comando_w := ds_comando_w || '	,obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, null, e.dt_mesano_referencia) vl_saldo ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_07_w := nr_ordem_campo_w;
					end if;
				end if;
			when 1 then
				if (cd_local_estoque_p is not null) then
					ds_comando_w := ds_comando_w || '	,obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque) vl_saldo ' || ds_quebra_w;
					nr_ordem_campo_w := nr_ordem_campo_w + 1;
					nr_ordem_campo_07_w := nr_ordem_campo_w;
				else
					ds_comando_w := ds_comando_w || '	,obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, null) vl_saldo ' || ds_quebra_w;
					nr_ordem_campo_w := nr_ordem_campo_w + 1;
					nr_ordem_campo_07_w := nr_ordem_campo_w;
				end if;
			when 2 then
				if (cd_local_estoque_p is not null) then
					if (nvl(ie_agrupa_meses_p,'N') = 'S') then
						ds_comando_w := ds_comando_w ||
							'	,decode(m.ie_consignado, '
							|| '''0'', obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, e.cd_local_estoque, '
								|| converte_data_varchar(fim_dia(dt_final_w)) || '), '
							|| '''1'', obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque), '
							|| '''2'', obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, e.cd_local_estoque, '
								|| converte_data_varchar(fim_dia(dt_final_w)) || '), '
							|| 'obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque)) vl_saldo ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_07_w := nr_ordem_campo_w;
					else
						ds_comando_w := ds_comando_w ||
							'	,decode(m.ie_consignado, '
							|| '''0'', obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, e.cd_local_estoque, e.dt_mesano_referencia), '
							|| '''1'', obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque), '
							|| '''2'', obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, e.cd_local_estoque, e.dt_mesano_referencia), '
							|| 'obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque)) vl_saldo ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_07_w := nr_ordem_campo_w;
					end if;
				else
					if (nvl(ie_agrupa_meses_p,'N') = 'S') then
						ds_comando_w := ds_comando_w ||
							'	,decode(m.ie_consignado, '
							|| '''0'', Obter_Saldo_Disp_Estoque(E.CD_ESTABELECIMENTO, E.CD_MATERIAL, null, '
								|| converte_data_varchar(fim_dia(dt_final_w)) || '), '
							|| '''1'', Obter_Saldo_Estoque_Consig(E.CD_ESTABELECIMENTO, null, E.CD_MATERIAL, null), '
							|| '''2'', Obter_Saldo_Disp_Estoque(E.CD_ESTABELECIMENTO, E.CD_MATERIAL, null, '
								|| converte_data_varchar(fim_dia(dt_final_w)) || '), '
							|| 'Obter_Saldo_Estoque_Consig(E.CD_ESTABELECIMENTO, null, E.CD_MATERIAL, null)) vl_saldo ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_07_w := nr_ordem_campo_w;
					else
						ds_comando_w := ds_comando_w ||
							'	,decode(m.ie_consignado, '
							|| '''0'', obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, null, e.dt_mesano_referencia), '
							|| '''1'', obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, null), '
							|| '''2'', obter_saldo_disp_estoque(e.cd_estabelecimento, e.cd_material, null, e.dt_mesano_referencia), '
							|| 'obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, null)) vl_saldo ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_07_w := nr_ordem_campo_w;
					end if;
				end if;
		end case;
	end if;
	
	if (nvl(ie_saldo_contabil_p,'N') = 'S') then
		case ie_consignado_p
			when 0 then
				if (cd_local_estoque_p is not null) then
					if (nvl(ie_agrupa_meses_p,'N') = 'S') then
						ds_comando_w := ds_comando_w || '	,sup_obter_saldo_estoque(e.cd_estabelecimento,' || converte_data_varchar(fim_dia(dt_final_w))
											|| ',e.cd_local_estoque,e.cd_material) vl_saldo_contabil ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_08_w := nr_ordem_campo_w;
					else
						ds_comando_w := ds_comando_w || '	,sup_obter_saldo_estoque(e.cd_estabelecimento,e.dt_mesano_referencia,e.cd_local_estoque,e.cd_material) vl_saldo_contabil ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_08_w := nr_ordem_campo_w;
					end if;
				else 
					if (nvl(ie_agrupa_meses_p,'N') = 'S') then
						ds_comando_w := ds_comando_w || '	,sup_obter_saldo_estoque(e.cd_estabelecimento,' || converte_data_varchar(fim_dia(dt_final_w))
											|| ',null,e.cd_material) vl_saldo_contabil ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_08_w := nr_ordem_campo_w;
					else
						ds_comando_w := ds_comando_w || '	,sup_obter_saldo_estoque(e.cd_estabelecimento,e.dt_mesano_referencia,null,e.cd_material) vl_saldo_contabil ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_08_w := nr_ordem_campo_w;
					end if;
				end if;
			when 1 then
				if (cd_local_estoque_p is not null) then
					ds_comando_w := ds_comando_w || '	,obter_saldo_estoque_consig(e.cd_estabelecimento,null,e.cd_material,e.cd_local_estoque) vl_saldo_contabil ' || ds_quebra_w;
					nr_ordem_campo_w := nr_ordem_campo_w + 1;
					nr_ordem_campo_08_w := nr_ordem_campo_w;
				else 
					ds_comando_w := ds_comando_w || '	,obter_saldo_estoque_consig(e.cd_estabelecimento,null,e.cd_material,null) vl_saldo_contabil ' || ds_quebra_w;
					nr_ordem_campo_w := nr_ordem_campo_w + 1;
					nr_ordem_campo_08_w := nr_ordem_campo_w;
				end if;
			when 2 then
				if (cd_local_estoque_p is not null) then
					if (nvl(ie_agrupa_meses_p,'N') = 'S') then
						ds_comando_w := ds_comando_w ||
							'	,decode(m.ie_consignado, '
							|| '''0'', sup_obter_saldo_estoque(e.cd_estabelecimento,' || converte_data_varchar(fim_dia(dt_final_w))
								|| ',e.cd_local_estoque,e.cd_material), '
							|| '''1'', obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque), '
							|| '''2'', sup_obter_saldo_estoque(e.cd_estabelecimento,' || converte_data_varchar(fim_dia(dt_final_w))
								||  ',e.cd_local_estoque,e.cd_material), '
							|| 'obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque)) vl_saldo_contabil ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_08_w := nr_ordem_campo_w;
					else
						ds_comando_w := ds_comando_w ||
							'	,decode(m.ie_consignado, '
							|| '''0'', sup_obter_saldo_estoque(e.cd_estabelecimento,e.dt_mesano_referencia,e.cd_local_estoque,e.cd_material), '
							|| '''1'', obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque), '
							|| '''2'', sup_obter_saldo_estoque(e.cd_estabelecimento,e.dt_mesano_referencia,e.cd_local_estoque,e.cd_material), '
							|| 'obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque)) vl_saldo_contabil ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_08_w := nr_ordem_campo_w;
					end if;
				else 
					if (nvl(ie_agrupa_meses_p,'N') = 'S') then
						ds_comando_w := ds_comando_w ||
							'	,decode(m.ie_consignado, '
							|| '''0'', sup_obter_saldo_estoque(e.cd_estabelecimento,' || converte_data_varchar(fim_dia(dt_final_w))
								|| ',null,e.cd_material), '
							|| '''1'', obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, null), '
							|| '''2'', sup_obter_saldo_estoque(e.cd_estabelecimento,' || converte_data_varchar(fim_dia(dt_final_w))
								|| ',null,e.cd_material), '
							|| 'obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, null)) vl_saldo_contabil ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_08_w := nr_ordem_campo_w;
					else
						ds_comando_w := ds_comando_w ||
							'	,decode(m.ie_consignado, '
							|| '''0'', sup_obter_saldo_estoque(e.cd_estabelecimento,e.dt_mesano_referencia,null,e.cd_material), '
							|| '''1'', obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, null), '
							|| '''2'', sup_obter_saldo_estoque(e.cd_estabelecimento,e.dt_mesano_referencia,null,e.cd_material), '
							|| 'obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, null)) vl_saldo_contabil ' || ds_quebra_w;
						nr_ordem_campo_w := nr_ordem_campo_w + 1;
						nr_ordem_campo_08_w := nr_ordem_campo_w;
					end if;
				end if;
		end case;
	else
		ds_comando_w := ds_comando_w || '	,0 vl_saldo_contabil ' || ds_quebra_w;
		nr_ordem_campo_w := nr_ordem_campo_w + 1;
		nr_ordem_campo_08_w := nr_ordem_campo_w;
	end if;
end if;

if (nvl(ie_imprime_local_p,'N') = 'S') then
	ds_comando_w := ds_comando_w || '	,substr(obter_localizacao_material(m.cd_material,c.cd_local_estoque),1,255) ds_local ' || ds_quebra_w;
	nr_ordem_campo_w := nr_ordem_campo_w + 1;
	nr_ordem_campo_09_w := nr_ordem_campo_w;
end if;

if (nvl(ie_fornec_ult_compra_p,'N') = 'S') then
	if (cd_estab_p is null) then
		ds_comando_w := ds_comando_w || '	,substr(obter_razao_social(obter_fornecedor_ultima_compra(m.cd_material, max(e.cd_estabelecimento))),1,100) ds_fornecedor ' || ds_quebra_w;
		nr_ordem_campo_w := nr_ordem_campo_w + 1;
		nr_ordem_campo_10_w := nr_ordem_campo_w;
	else
		ds_comando_w := ds_comando_w || '	,substr(obter_razao_social(obter_fornecedor_ultima_compra(m.cd_material, null)),1,100) ds_fornecedor ' || ds_quebra_w;
		nr_ordem_campo_w := nr_ordem_campo_w + 1;
		nr_ordem_campo_10_w := nr_ordem_campo_w;
	end if;
end if;

ds_comando_w := ds_comando_w || 'from	' || ds_quebra_w;

if (nr_seq_marca_p is not null) then
	ds_comando_w := ds_comando_w || '	marca s, ' || ds_quebra_w;
	ds_comando_w := ds_comando_w || '	material_marca t, ' || ds_quebra_w;
end if;

ds_comando_w := ds_comando_w || '	material m, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	classe_material a, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	subgrupo_material g, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	grupo_material b, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	local_estoque c, ' || ds_quebra_w;


if ((nvl(ie_nome_comercial_p,'N') = 'S') or
	 (nvl(ie_compra_comercial_p,'N') = 'S')) then
	ds_comando_w := ds_comando_w || '	curva_abc_v2 e ' || ds_quebra_w;
else
	ds_comando_w := ds_comando_w || '	curva_abc_v e ' || ds_quebra_w;
end if;

ds_comando_w := ds_comando_w || 'where	m.cd_classe_material = a.cd_classe_material ' || ds_quebra_w;
ds_comando_w := ds_comando_w || 'and	a.cd_subgrupo_material = g.cd_subgrupo_material ' || ds_quebra_w;
ds_comando_w := ds_comando_w || 'and	g.cd_grupo_material = b.cd_grupo_material ' || ds_quebra_w;
ds_comando_w := ds_comando_w || 'and	c.cd_estabelecimento = e.cd_estabelecimento ' || ds_quebra_w;
ds_comando_w := ds_comando_w || 'and	c.cd_local_estoque = e.cd_local_estoque ' || ds_quebra_w;
ds_comando_w := ds_comando_w || 'and	m.cd_material = e.cd_material ' || ds_quebra_w;
ds_comando_w := ds_comando_w || 'and	e.dt_mesano_referencia between ' || converte_data_varchar(dt_inicial_w) || ' and ' || converte_data_varchar(dt_final_w) || ' ' || ds_quebra_w;

ds_comando_w := ds_comando_w || 'and ((''' || cd_grupo_mat_filtro_w || ''' = ''X'') or (obter_se_contido_char(b.cd_grupo_material,'''|| cd_grupo_mat_filtro_w || ''') = '''||ie_grupo_mat_contido_w ||'''))'|| ds_quebra_w;
ds_comando_w := ds_comando_w || 'and ((''' || cd_subgrupo_mat_filtro_w || ''' = ''X'') or (obter_se_contido_char(g.cd_subgrupo_material,'''|| cd_subgrupo_mat_filtro_w || ''') = '''||ie_subgrupo_mat_contido_w ||'''))'|| ds_quebra_w;
ds_comando_w := ds_comando_w || 'and ((''' || cd_classe_mat_filtro_w || ''' = ''X'') or (obter_se_contido_char(a.cd_classe_material,'''|| cd_classe_mat_filtro_w || ''') = '''||ie_classe_mat_contido_w ||'''))'|| ds_quebra_w;
ds_comando_w := ds_comando_w || 'and ((''' || cd_local_mat_filtro_w || ''' = ''X'') or (obter_se_contido_char(c.cd_local_estoque,'''|| cd_local_mat_filtro_w || ''') = '''||ie_local_mat_contido_w ||'''))'|| ds_quebra_w;	
ds_comando_w := ds_comando_w || 'and ((''' || cd_centro_custo_mat_filtro_w || ''' = ''X'') or (obter_se_contido_char(e.cd_centro_custo,'''|| cd_centro_custo_mat_filtro_w || ''') = '''||ie_centro_custo_mat_contido_w ||'''))'|| ds_quebra_w;

if (nvl(ie_saldo_zero_p,'N') = 'S') then
	case ie_consignado_p
		when 0 then
			if (nvl(ie_agrupa_meses_p,'N') = 'S') then
				ds_comando_w := ds_comando_w || 'and	obter_saldo_disp_estoque(e.cd_estabelecimento,e.cd_material,e.cd_local_estoque,'
								|| converte_data_varchar(fim_dia(dt_final_w)) || ') <= 0 ' || ds_quebra_w;
			else
				ds_comando_w := ds_comando_w || 'and	obter_saldo_disp_estoque(e.cd_estabelecimento,e.cd_material,e.cd_local_estoque,e.dt_mesano_referencia) <= 0 ' || ds_quebra_w;
			end if;
		when 1 then
				ds_comando_w := ds_comando_w || 'and	obter_saldo_estoque_consig(e.cd_estabelecimento, null, e.cd_material, e.cd_local_estoque) <= 0 ' || ds_quebra_w;
		when 2 then
			if (nvl(ie_agrupa_meses_p,'N') = 'S') then
				ds_comando_w := ds_comando_w ||
					'and	decode(m.ie_consignado,'
					|| '''0'',obter_saldo_disp_estoque(e.cd_estabelecimento,e.cd_material,e.cd_local_estoque,' || converte_data_varchar(fim_dia(dt_final_w)) || '),'
					|| '''1'',obter_saldo_estoque_consig(e.cd_estabelecimento,null,e.cd_material,e.cd_local_estoque),'
					|| '''2'',obter_saldo_disp_estoque(e.cd_estabelecimento,e.cd_material,e.cd_local_estoque,' || converte_data_varchar(fim_dia(dt_final_w)) || '),'
					|| 'obter_saldo_estoque_consig(e.cd_estabelecimento,null,e.cd_material,e.cd_local_estoque)) <= 0 ' || ds_quebra_w;
			else
				ds_comando_w := ds_comando_w ||
					'and	decode(m.ie_consignado, '
					|| '''0'', obter_saldo_disp_estoque(e.cd_estabelecimento,e.cd_material,e.cd_local_estoque,e.dt_mesano_referencia),'
					|| '''1'', obter_saldo_estoque_consig(e.cd_estabelecimento,null,e.cd_material,e.cd_local_estoque),'
					|| '''2'', obter_saldo_disp_estoque(e.cd_estabelecimento,e.cd_material,e.cd_local_estoque,e.dt_mesano_referencia),'
					|| 'obter_saldo_estoque_consig(e.cd_estabelecimento,null,e.cd_material,e.cd_local_estoque)) <= 0 ' || ds_quebra_w;
			end if;
	end case;	
end if;

if (cd_estab_p is not null) then
	ds_comando_w := ds_comando_w || 'and	e.cd_estabelecimento = ' || cd_estab_p || ' ' || ds_quebra_w;
else
	ds_comando_w := ds_comando_w || 'and	e.cd_estabelecimento in (select x.cd_estabelecimento from estabelecimento x where x.cd_empresa = 1) ' || ds_quebra_w;
end if;

case ie_local_estoque_p
	when 1 then 	
		ds_comando_w := ds_comando_w || 'and	c.ie_proprio = ''S''' || ds_quebra_w;
	when 2 then
		ds_comando_w := ds_comando_w || 'and	c.ie_proprio = ''N''' || ds_quebra_w;
	else null;
end case;

case ie_consignado_p
	when 0 then
		ds_comando_w := ds_comando_w || 'and	e.ie_consignado = ''N''' || ds_quebra_w;
	when 1 then
		ds_comando_w := ds_comando_w || 'and	e.ie_consignado = ''S''' || ds_quebra_w;
	else null;
end case;

if (cd_local_estoque_p is not null) then
	ds_comando_w := ds_comando_w || 'and	c.cd_local_estoque = ' || cd_local_estoque_p || ' ' || ds_quebra_w;
end if;

if (nr_seq_marca_p is not null) then
	ds_comando_w := ds_comando_w || 'and	e.cd_material = t.cd_material(+) ' || ds_quebra_w;
	ds_comando_w := ds_comando_w || 'and	s.nr_sequencia(+) = t.nr_sequencia ' || ds_quebra_w;
	ds_comando_w := ds_comando_w || 'and	nvl(t.ie_situacao,''A'') = ''A''' || ds_quebra_w;
	ds_comando_w := ds_comando_w || 'and	s.nr_sequencia = ' || nr_seq_marca_p || ' ' || ds_quebra_w;
end if;

if (cd_fabricante_p is not null) then
	ds_comando_w := ds_comando_w || 'and	m.nr_seq_fabric = ' || cd_fabricante_p || ' ' || ds_quebra_w;
end if;

case ie_padronizado_p
	when 1 then
		ds_comando_w := ds_comando_w || 'and	obter_se_material_padronizado(' || wheb_usuario_pck.get_cd_estabelecimento || ',m.cd_material) = ''S'' ' || ds_quebra_w;
	when 2 then
		ds_comando_w := ds_comando_w || 'and	obter_se_material_padronizado(' || wheb_usuario_pck.get_cd_estabelecimento || ',m.cd_material) = ''N'' ' || ds_quebra_w;
	else null;
end case;

case ie_situacao_p
	when 1 then
		ds_comando_w := ds_comando_w || 'and	m.ie_situacao = ''A'' ' || ds_quebra_w;
	when 2 then
		ds_comando_w := ds_comando_w || 'and	m.ie_situacao = ''I'' ' || ds_quebra_w;
	else null;
end case;


ds_comando_w := ds_comando_w || 'group by	' || ds_quebra_w;

if (cd_local_estoque_p is not null or cd_m_local_estoque_p is not null) then
	ds_comando_w := ds_comando_w || '	c.cd_local_estoque, ' || ds_quebra_w;
	ds_comando_w := ds_comando_w || '	c.ds_local_estoque, ' || ds_quebra_w;
end if;

ds_comando_w := ds_comando_w || '	b.cd_grupo_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	b.ds_grupo_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	g.cd_subgrupo_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	g.ds_subgrupo_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	a.cd_classe_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	a.ds_classe_material, ' || ds_quebra_w;

if (nr_seq_marca_p is not null) then
	ds_comando_w := ds_comando_w || '	t.nr_sequencia, ' || ds_quebra_w;
	ds_comando_w := ds_comando_w || '	s.ds_marca, ' || ds_quebra_w;
end if;

if ((nvl(ie_nome_comercial_p,'N') = 'S') or
	 (nvl(ie_compra_comercial_p,'N') = 'S')) then
	ds_comando_w := ds_comando_w || '	e.cd_material_comercial, ' || ds_quebra_w;
end if;

if (nvl(ie_nome_comercial_p,'N') <> 'S') then
	ds_comando_w := ds_comando_w || '	m.ds_material, ' || ds_quebra_w;
end if;

ds_comando_w := ds_comando_w || '	m.cd_material, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	m.ie_situacao, ' || ds_quebra_w;
ds_comando_w := ds_comando_w || '	m.ie_tipo_material, ' || ds_quebra_w;

if (nvl(ie_imprime_generico_p,'N') = 'S') then
	ds_comando_w := ds_comando_w || '	m.cd_material_generico, ' || ds_quebra_w;
end if;

if (nvl(ie_saldo_atual_p,'N') = 'S' or nvl(ie_saldo_contabil_p,'N') = 'S') then
	if (cd_local_estoque_p is not null) then
		if (nvl(ie_agrupa_meses_p,'N') = 'S') then
			ds_comando_w := ds_comando_w || '	e.cd_estabelecimento, ' || ds_quebra_w;
			ds_comando_w := ds_comando_w || '	e.cd_material, ' || ds_quebra_w;
			ds_comando_w := ds_comando_w || '	e.cd_local_estoque, ' || ds_quebra_w;
		else
			ds_comando_w := ds_comando_w || '	e.cd_estabelecimento, ' || ds_quebra_w;
			ds_comando_w := ds_comando_w || '	e.cd_material, ' || ds_quebra_w;
			ds_comando_w := ds_comando_w || '	e.cd_local_estoque, ' || ds_quebra_w;
			ds_comando_w := ds_comando_w || '	e.dt_mesano_referencia, ' || ds_quebra_w;
		end if;
	else 
		if (nvl(ie_agrupa_meses_p,'N') = 'S') then
			ds_comando_w := ds_comando_w || '	e.cd_estabelecimento, ' || ds_quebra_w;
			ds_comando_w := ds_comando_w || '	e.cd_material, ' || ds_quebra_w;
		else
			ds_comando_w := ds_comando_w || '	e.cd_estabelecimento, ' || ds_quebra_w;
			ds_comando_w := ds_comando_w || '	e.cd_material, ' || ds_quebra_w;
			ds_comando_w := ds_comando_w || '	e.dt_mesano_referencia, ' || ds_quebra_w;
		end if;
	end if;
	
	if (ie_consignado_p = 2) then
		ds_comando_w := ds_comando_w || '	m.ie_consignado, ' || ds_quebra_w;
	end if;
else
	ds_comando_w := ds_comando_w || '	0, ' || ds_quebra_w;
end if;

ds_comando_w := ds_comando_w || '	substr(obter_dados_material_estab(m.cd_material,c.cd_estabelecimento,''UME''),1,30) ' || ds_quebra_w;

if (nvl(ie_imprime_local_p,'N') = 'S') then
	ds_comando_w := ds_comando_w || '	,substr(obter_localizacao_material(m.cd_material,c.cd_local_estoque),1,255) ' || ds_quebra_w;
end if;

ds_comando_w := ds_comando_w || 'order by	' || ds_quebra_w;

if (ie_curva_p = 0) then
	ds_comando_w := ds_comando_w || '	sum(e.vl_estoque) desc ';
else
	ds_comando_w := ds_comando_w || '	sum(e.qt_estoque) desc ';
end if;

/* FIM MONTAGEM SQL */

delete w_curva_abc_xyz where nm_usuario = nm_usuario_p;
commit;

c01_w := dbms_sql.open_cursor;
dbms_sql.parse(c01_w, ds_comando_w, dbms_sql.native);

dbms_sql.define_column(c01_w, 1, cd_grupo_material_w);
dbms_sql.define_column(c01_w, 2, ds_grupo_material_w, 255);
dbms_sql.define_column(c01_w, 3, cd_subgrupo_material_w);
dbms_sql.define_column(c01_w, 4, ds_subgrupo_material_w, 255);
dbms_sql.define_column(c01_w, 5, cd_classe_material_w);
dbms_sql.define_column(c01_w, 6, ds_classe_material_w, 255);
dbms_sql.define_column(c01_w, 7, cd_centro_custo_w);
dbms_sql.define_column(c01_w, 8, ds_material_w, 255);
dbms_sql.define_column(c01_w, 9, cd_material_w);
dbms_sql.define_column(c01_w, 10, ie_tipo_material_w, 3);
dbms_sql.define_column(c01_w, 11, vl_ultima_compra_w);
dbms_sql.define_column(c01_w, 12, cd_unidade_medida_estoque_w, 30);
dbms_sql.define_column(c01_w, 13, ie_situacao_w, 1);
dbms_sql.define_column(c01_w, 14, qt_movimento_w);
dbms_sql.define_column(c01_w, 15, vl_movimento_w);
if (nr_ordem_campo_01_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_01_w, cd_local_estoque_w);
end if;
if (nr_ordem_campo_02_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_02_w, ds_local_estoque_w, 40);
end if;
if (nr_ordem_campo_03_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_03_w, nr_seq_marca_w);
end if;
if (nr_ordem_campo_04_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_04_w, ds_marca_w, 80);
end if;
if (nr_ordem_campo_05_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_05_w, ds_material_generico_w, 255);
end if;
if (nr_ordem_campo_06_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_06_w, cd_material_generico_w);
end if;
if (nr_ordem_campo_07_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_07_w, vl_saldo_w);
end if;
if (nr_ordem_campo_08_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_08_w, vl_saldo_contabil_w);
end if;
if (nr_ordem_campo_09_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_09_w, ds_local_w, 255);
end if;
if (nr_ordem_campo_10_w is not null) then
	dbms_sql.define_column(c01_w, nr_ordem_campo_10_w, ds_fornecedor_w, 100);
end if;

retorno_w := dbms_sql.execute(c01_w);

while (dbms_sql.fetch_rows(c01_w) > 0) loop
	begin
	
	dbms_sql.column_value(c01_w, 1, cd_grupo_material_w);
	dbms_sql.column_value(c01_w, 2, ds_grupo_material_w);
	dbms_sql.column_value(c01_w, 3, cd_subgrupo_material_w);
	dbms_sql.column_value(c01_w, 4, ds_subgrupo_material_w);
	dbms_sql.column_value(c01_w, 5, cd_classe_material_w);
	dbms_sql.column_value(c01_w, 6, ds_classe_material_w);
	dbms_sql.column_value(c01_w, 7, cd_centro_custo_w);
	dbms_sql.column_value(c01_w, 8, ds_material_w);
	dbms_sql.column_value(c01_w, 9, cd_material_w);
	dbms_sql.column_value(c01_w, 10, ie_tipo_material_w);
	dbms_sql.column_value(c01_w, 11, vl_ultima_compra_w);
	dbms_sql.column_value(c01_w, 12, cd_unidade_medida_estoque_w);
	dbms_sql.column_value(c01_w, 13, ie_situacao_w);
	dbms_sql.column_value(c01_w, 14, qt_movimento_w);
	dbms_sql.column_value(c01_w, 15, vl_movimento_w);
	

	if (nr_ordem_campo_01_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_01_w, cd_local_estoque_w);
	end if;
	if (nr_ordem_campo_02_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_02_w, ds_local_estoque_w);
	end if;
	if (nr_ordem_campo_03_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_03_w, nr_seq_marca_w);
	end if;
	if (nr_ordem_campo_04_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_04_w, ds_marca_w);
	end if;
	if (nr_ordem_campo_05_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_05_w, ds_material_generico_w);
	end if;
	if (nr_ordem_campo_06_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_06_w, cd_material_generico_w);
	end if;
	if (nr_ordem_campo_07_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_07_w, vl_saldo_w);
	end if;
	if (nr_ordem_campo_08_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_08_w, vl_saldo_contabil_w);
	end if;
	if (nr_ordem_campo_09_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_09_w, ds_local_w);
	end if;
	if (nr_ordem_campo_10_w is not null) then
		dbms_sql.column_value(c01_w, nr_ordem_campo_10_w, ds_fornecedor_w);
	end if;
	
	nr_ordem_w := nr_ordem_w + 1;
	
	if (ie_curva_p = 0) then
		qt_acumulado_w := qt_acumulado_w + vl_movimento_w;
	else
		qt_acumulado_w := qt_acumulado_w + qt_movimento_w;
	end if;
	
	insert into w_curva_abc_xyz values (
		nr_ordem_w,
		'S',
		cd_grupo_material_w,
		ds_grupo_material_w,
		cd_subgrupo_material_w,
		ds_subgrupo_material_w,
		cd_classe_material_w,
		ds_classe_material_w,
		ds_material_w,
		cd_material_w,
		ie_tipo_material_w,
		vl_ultima_compra_w,
		cd_unidade_medida_estoque_w,
		ie_situacao_w,
		qt_movimento_w,
		vl_movimento_w,
		cd_local_estoque_w,
		ds_local_estoque_w,
		nr_seq_marca_w,
		ds_marca_w,
		ds_material_generico_w,
		cd_material_generico_w,
		vl_saldo_w,
		vl_saldo_contabil_w,
		ds_local_w,
		ds_fornecedor_w,
		qt_acumulado_w,
		null,
		null,
		null,
		cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		cd_local_estoque_w,
		cd_centro_custo_w,
		nm_usuario_p);
	
	commit;
	
	end;
end loop;

select	sum(a.vl_movimento),
	sum(a.qt_movimento)
into	vl_movimento_total_w,
	qt_movimento_total_w
from	w_curva_abc_xyz a
where	a.nm_usuario = nm_usuario_p;

open C01;
loop
fetch C01 into	
	nr_ordem_ww,
	vl_movimento_ww,
	qt_movimento_ww,
	qt_acumulado_ww;
exit when C01%notfound;
	begin
	pr_valor_w := 0;
	pr_item_w := 0;
	if (ie_curva_p = 0) then		--***se o tipo de curva for 'ABC'***
		if (vl_movimento_total_w > 0) then --7775,1233
			pr_valor_w := trunc((qt_acumulado_ww * 100) / vl_movimento_total_w,4);
			pr_item_w := trunc((vl_movimento_ww * 100) / vl_movimento_total_w,4);
		end if;	
			if (pr_valor_w <= vl_a_x_p) then
				ds_curva_w := 'A';
			elsif (pr_valor_w <= (vl_b_y_p + vl_a_x_p)) then
				ds_curva_w := 'B';
			else
				ds_curva_w := 'C';
			end if;
		/*end if;*/
	else
		if (qt_movimento_total_w > 0) then
			pr_valor_w := trunc((qt_acumulado_ww * 100) / qt_movimento_total_w,4);
			pr_item_w := trunc((qt_movimento_ww * 100) / qt_movimento_total_w,4);
		end if;	
			if (pr_valor_w <= vl_a_x_p) then
				ds_curva_w := 'X';
			elsif (pr_valor_w <= (vl_b_y_p + vl_a_x_p)) then
				ds_curva_w := 'Y';
			else
				ds_curva_w := 'Z';
			end if;
		/*end if;*/
	end if;

	if (qt_movimento_ww is not null and
		vl_movimento_ww is not null and
		((ie_imprime_a_x_p = 'S' and ds_curva_w in ('A','X')) or
		 (ie_imprime_b_y_p = 'S' and ds_curva_w in ('B','Y')) or
		 (ie_imprime_c_z_p = 'S' and ds_curva_w in ('C','Z')))) then
		ie_imprime_w := 'S';
	else
		ie_imprime_w := 'N';
	end if;
		
	update	w_curva_abc_xyz
	set	ds_curva = ds_curva_w,
		pr_item = pr_item_w,
		pr_valor = pr_valor_w,
		ie_imprimir = ie_imprime_w
	where	nm_usuario = nm_usuario_p
	and	nr_ordem = nr_ordem_ww;
	
	commit;
	
	end;
end loop;
close C01;

end gerar_info_curva_abc_xyz;
/