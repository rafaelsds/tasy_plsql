create or replace 
procedure ageint_gerar_consulta_agecons(cd_estabelecimento_p	number,
					cd_agenda_p     	number,
					dt_agenda_p	date,
					nm_usuario_p	varchar2) is 

ie_feriado_w		Varchar2(0001)	:= 'N';
ie_dia_semana_w		Number(003,0);
hr_inicial_w		Date;
hr_final_w		Date;
hr_fim_w			Date;
hr_atual_w		Date;
hr_nada_w		Date;
nr_minuto_intervalo_w	Number(010,0);
ie_classif_Agenda_w	Varchar2(0005);
qt_existe_agenda_w	Number(003,0); 
qt_horario_w		Number(005,0)    := 0;
nr_sequencia_w		Number(0010,0)    := 0;
ds_observacao_w		Varchar2(255);
qt_min_minimo_w		Number(005,0);
ds_horario_w		Varchar2(255);
qt_turno_w		Number(05,0);
qt_turno_diario_w		Number(05,0);
nr_seq_esp_w		Number(10,0)	:= 0;
nr_seq_classif_med_w	number(10,0);
nr_seq_sala_w		number(10,0);
cd_convenio_padrao_w	number(10,0);
ie_excluir_livres_w		varchar2(01) 	:= 'N';
ie_excluir_livres_novo_w	varchar2(02) 	:= 'N';
ie_excluir_livres_fut_w	varchar2(01) 	:= 'S';
cd_turno_w		varchar2(01)	:= 0;
HR_QUEBRA_TURNO_W		varchar2(05);
qt_min_QUEBRA_TURNO_W	varchar2(05);
ie_horario_adicional_w 		varchar2(01) := 'S';
nr_seq_hora_w			number(10,0);
ie_bloqueio_w			varchar2(1);
ie_turno_w			number(1,0);
nr_seq_turno_w			number(10,0);
nr_seq_turno_esp_w		number(10,0);
ie_gerar_nome_w			varchar2(1);
cd_setor_agenda_w		number(10,0);
nr_seq_hor_bloq_w		Number(0010,0);
ie_gerar_hor_bloq_w	Varchar2(1)	:= 'N';
cd_medico_req_w		varchar2(10);
nr_seq_motivo_transf_w	number(10);
ie_status_autor_w		varchar2(15);
ie_gerar_hor_falta_w	varchar2(1);
nr_seq_preferencia_w		number(10,0);
ie_pref_lib_w			varchar2(1);
ie_gera_hor_w			varchar2(1);
qt_dias_caducar_pref_w		number(10,0);
cd_pessoa_usuario_w		varchar2(10);

cd_medico_exec_w	varchar2(10);
cd_pessoa_fisica_w	varchar2(10);
nm_paciente_w		varchar2(60);
ie_status_Agenda_w	varchar2(3);
nr_minuto_duracao_w	number(10);
dt_agenda_w		date;
nr_seq_agenda_w		number(10,0);
nr_seq_status_pac_w	number(10,0);
nr_atendimento_w		number(10);
cd_convenio_w		number(5);
hr_inicial_intervalo_w	date;
hr_final_intervalo_w		date;
cd_turno_ww		varchar2(1);
ie_encaixe_w		varchar2(1);
cd_especialidade_w		number(10);
ds_motivo_w			Varchar2(255);

CURSOR C01 IS
	select 	1 ie_turno,
			0 nr_seq_turno,
			nr_sequencia nr_seq_turno_esp,
			to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
			to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
			to_char(hr_final,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			nr_minuto_intervalo,
			ie_classif_agenda,
			0,
			ds_observacao,
			nr_seq_sala,
			0 cd_convenio_padrao,
			null cd_medico_req,
			null hr_inicial_intervalo,
			null hr_final_intervalo
	from 	agenda_Turno_Esp a
	where 	cd_agenda     	= cd_agenda_p
	and 	ie_dia_semana			= ie_dia_Semana_w
	--and	dt_agenda		= trunc(dt_agenda_p,'dd')
	and		obter_se_turno_esp_agecons_vig(dt_agenda,dt_agenda_fim,dt_agenda_p) = 'S'
	--and 	hr_inicial 		< hr_final
	and		to_char(hr_inicial,'hh24:mi:ss') < to_char(hr_final,'hh24:mi:ss')
	and		nr_minuto_intervalo	> 0
	and		nr_seq_esp_w		> 0
	union
	select 	2 ie_turno,
			nr_sequencia nr_seq_turno,
			0 nr_seq_turno_esp,
			to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
			to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
		 	to_char(hr_final,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			nr_minuto_intervalo,
			ie_classif_agenda,
			nr_seq_classif_med,
			ds_observacao,
			nr_seq_sala,
			cd_convenio_padrao,
			cd_medico_req,
			hr_inicial_intervalo,
			hr_final_intervalo
	from 	agenda_Turno a
	where 	cd_agenda     		= cd_agenda_p
	and 	ie_dia_semana			= ie_dia_Semana_w
	--and 	hr_inicial 			< hr_final
	and		to_char(hr_inicial,'hh24:mi:ss') < to_char(hr_final,'hh24:mi:ss')
	and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(dt_agenda_p)))
	and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(dt_agenda_p)))
	and	nvl(nr_minuto_intervalo,0) > 0
	and	((((ie_feriado_w <> 'S') and (nvl(ie_feriado,'X') <> 'F')) or 
		(nvl(ie_feriado,obter_se_agenda_feriado(cd_agenda_p)) = 'S')) or ((ie_feriado = 'F') and (ie_feriado_w = 'S')))
	and	(nr_seq_esp_w			= 0 or ie_horario_adicional_w = 'S')
	and	obter_se_gerar_turno_agecons(dt_inicio_vigencia,ie_frequencia,dt_agenda_p) = 'S'
	and	((Obter_Semana_Dia_Agecons(dt_agenda_p,ie_dia_semana) = nvl(ie_semana,0)) or (nvl(ie_semana,0) = 0))
	union
	select 	2 ie_turno,
			nr_sequencia nr_seq_turno,
			0 nr_seq_turno_esp,
			to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
			to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
			to_char(hr_final,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			nr_minuto_intervalo,
			ie_classif_agenda,
			nr_seq_classif_med,
			ds_observacao,
			nr_seq_sala,
			cd_convenio_padrao,
			cd_medico_req,
			hr_inicial_intervalo,
			hr_final_intervalo
	from 	agenda_Turno
	where 	cd_agenda     	= cd_agenda_p
	and 	ie_dia_semana		= 9
	--and 	hr_inicial 		< hr_final
	and		to_char(hr_inicial,'hh24:mi:ss') < to_char(hr_final,'hh24:mi:ss')
	and	nvl(nr_minuto_intervalo,0) > 0
	and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(dt_agenda_p)))
	and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(dt_agenda_p)))
	and	ie_dia_semana_w between 2 and 6
	and	((((ie_feriado_w <> 'S') and (nvl(ie_feriado,'X') <> 'F')) or 
		(nvl(ie_feriado,obter_se_agenda_feriado(cd_agenda_p)) = 'S')) or ((ie_feriado = 'F') and (ie_feriado_w = 'S')))
  	and	(nr_seq_esp_w			= 0 or ie_horario_adicional_w = 'S')
	and	obter_se_gerar_turno_agecons(dt_inicio_vigencia,ie_frequencia,dt_agenda_p) = 'S'
	and	((Obter_Semana_Dia_Agecons(dt_agenda_p,ie_dia_semana) = nvl(ie_semana,0)) or (nvl(ie_semana,0) = 0))
	and	not exists(	select	1
					from	agenda_Turno
					where 	cd_agenda     	= cd_agenda_p
					and 	ie_dia_semana	= ie_dia_Semana_w)
	order by 1,2,3,4;

CURSOR C02 IS
	select	a.dt_agenda,
			(a.dt_agenda + (a.nr_minuto_duracao / 1440)) dt_final
	from 	agenda b,
			ageint_consulta_horarios a
	where 	a.cd_agenda	= b.cd_agenda
	and		a.cd_agenda	= cd_agenda_p
	and   	a.dt_agenda between trunc(dt_agenda_p,'dd') and (trunc(dt_agenda_p,'dd') + (86399/86400))
	and		a.dt_agenda >= sysdate
	and		b.ie_gerar_sobra_horario = 'S'
	order by 1;
	
Cursor C03 is
	select	cd_pessoa_fisica,
			substr(nm_paciente,1,60),
			cd_medico_req,
			ie_status_agenda,
			nr_minuto_duracao,
			dt_Agenda,
			nr_sequencia,
			nr_seq_status_pac,
			nr_atendimento,
			cd_convenio,
			cd_turno,
			nvl(ie_encaixe,'N'),
			ie_classif_agenda,
			SUBSTR(obter_motivo_agecons(nr_sequencia,ie_status_agenda, NVL(nr_seq_motivo_transf,somente_numero(cd_motivo_cancelamento))),1,255)
	from	agenda_consulta
	where	cd_agenda		= cd_agenda_p
	and		ie_status_agenda	<> 'L'
	and		trunc(dt_agenda)	= trunc(dt_agenda_p)
	order by dt_agenda;
	
BEGIN

delete	ageint_consulta_horarios
where	cd_agenda	= cd_agenda_p
and	nm_usuario	= nm_usuario_p
and	dt_agenda	between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 83699/86400;

select	max(cd_pessoa_fisica)
into	cd_pessoa_usuario_w
from	usuario
where	nm_usuario = nm_usuario_p;

select	nvl(max(ie_gerar_hor_bloq),'N'),
		max(cd_especialidade)
into	ie_gerar_hor_bloq_w,
		cd_especialidade_w
from	agenda
where	cd_agenda	= cd_agenda_p;

select	nvl(max(HR_QUEBRA_TURNO), '12'),
	nvl(max(qt_min_quebra_turno), '00')
into	HR_QUEBRA_TURNO_W,
	qt_min_QUEBRA_TURNO_W
from	agenda
where 	cd_agenda	= cd_agenda_p;


hr_nada_w	:= trunc(dt_agenda_p,'year');

obter_param_usuario(898, 35, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_excluir_livres_w);

obter_param_usuario(821, 126, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_excluir_livres_novo_w);

obter_param_usuario(898, 52, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_excluir_livres_fut_w);

obter_param_usuario(821, 44, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_gerar_nome_w);

obter_param_usuario(821, 156, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_status_autor_w);

obter_param_usuario(821, 189, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p,ie_gerar_hor_falta_w);

Obter_Param_Usuario(869,377,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,qt_dias_caducar_pref_w);

/* verificar feriado */
select	decode(count(*),0,'N','S')
into	ie_feriado_w
from 	feriado a, 
	agenda b
where 	a.cd_estabelecimento = cd_estabelecimento_p
and	a.dt_feriado = dt_agenda_p
and 	b.cd_agenda = cd_agenda_p;

select		nvl(max(nr_sequencia),0),
		nvl(max(ie_horario_adicional), 'N')
into		nr_seq_esp_w,
		ie_horario_adicional_w
from		agenda_turno_esp
where		cd_agenda	= cd_agenda_p
--and		dt_agenda	= trunc(dt_agenda_p,'dd');
and		obter_se_turno_esp_agecons_vig(dt_agenda,dt_agenda_fim,dt_agenda_p) = 'S';

IF	(nr_sequencia_w = 0) then
	select pkg_date_utils.get_WeekDay(dt_agenda_p)
	into	ie_dia_semana_w
	from	dual;
END IF;
COMMIT;

open C03;
loop
fetch C03 into	
	cd_pessoa_fisica_w,
	nm_paciente_w,
	cd_medico_exec_w,
	ie_status_agenda_w,
	nr_minuto_duracao_w,
	dt_agenda_w,
	nr_seq_agenda_w,
	nr_seq_status_pac_w,
	nr_atendimento_w,
	cd_convenio_w,
	cd_turno_ww,
	ie_encaixe_w,
	ie_classif_agenda_w,
	ds_motivo_w;
exit when C03%notfound;
	begin
	
	insert into ageint_consulta_horarios
		(dt_Atualizacao,
		nm_usuario,
		cd_agenda,
		cd_medico_exec,
		cd_paciente,
		nm_paciente,
		ie_status_agenda,
		nr_minuto_duracao,
		dt_agenda,
		nr_seq_agenda,
		nr_seq_status_pac,
		nr_atendimento,
		cd_convenio,
		cd_turno,
		ie_encaixe,
		cd_especialidade,
		ie_classif_agenda,
		ds_motivo)
	values
		(sysdate,
		nm_usuario_p,
		cd_Agenda_p,
		cd_medico_exec_w,
		cd_pessoa_fisica_w,
		nm_paciente_w,
		ie_status_Agenda_w,
		nr_minuto_duracao_w,
		dt_agenda_w,
		nr_seq_agenda_w,
		nr_seq_status_pac_w,
		nr_atendimento_w,
		cd_convenio_w,
		cd_turno_ww,
		ie_encaixe_w,
		cd_especialidade_w,
		ie_classif_agenda_w,
		ds_motivo_w);
	
	end;
end loop;
close C03;

qt_min_minimo_w		:= 5;
ds_horario_w		:= '';
OPEN C01;
LOOP
FETCH C01 into
	ie_turno_w,
	nr_seq_turno_w,
	nr_seq_turno_esp_w,
	hr_inicial_w,
	hr_final_w,
	nr_minuto_Intervalo_w,
	ie_classif_agenda_w,
	nr_seq_classif_med_w,
	ds_observacao_w,
	nr_seq_sala_w,
	cd_convenio_padrao_w,
	cd_medico_req_w,
	hr_inicial_intervalo_w,
	hr_final_intervalo_w;
	
EXIT 	WHEN c01%notfound;
     	BEGIN

	IF	(nr_minuto_intervalo_w < qt_min_minimo_w) then
		qt_min_minimo_w	:= nr_minuto_intervalo_w;
	END IF;

	hr_atual_w					:= hr_inicial_w;
	hr_inicial_intervalo_w 		:= to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_inicial_intervalo_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
	hr_final_intervalo_w 		:= to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || to_char(hr_final_intervalo_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
	
	WHILE (hr_atual_w	< hr_final_w) LOOP
		BEGIN
		hr_fim_w		:= hr_atual_w + (nr_minuto_Intervalo_w - 1) /1440;
		
		if	(((hr_atual_w < hr_inicial_intervalo_w) or
			(hr_atual_w >= hr_final_intervalo_w)) or
			((hr_inicial_intervalo_w is null) and
			(hr_final_intervalo_w is null))) and
			(hr_atual_w >= sysdate) then
			BEGIN
			
			if	(ie_gerar_hor_falta_w = 'S') then
				select 	/*+ index (a AGECONS_UK) */
					count(*)
				into 	qt_existe_agenda_w
				from 	agenda_consulta a
				where 	a.cd_agenda	= cd_agenda_p
				and   	a.dt_agenda between trunc(hr_atual_w,'dd') and (trunc(hr_atual_w,'dd') + (86399/86400))
				and	(hr_atual_w between	a.dt_agenda  and (a.dt_agenda + (a.nr_minuto_duracao - 1) /1440))
				and 	not((a.ie_status_agenda = 'L') or (a.ie_status_agenda = 'C'));
			else
				select 	/*+ index (a AGECONS_UK) */
					count(*)
				into 	qt_existe_agenda_w
				from 	agenda_consulta a
				where 	a.cd_agenda	= cd_agenda_p
				and   	a.dt_agenda between trunc(hr_atual_w,'dd') and (trunc(hr_atual_w,'dd') + (86399/86400))
				and	(hr_atual_w between	a.dt_agenda  and (a.dt_agenda + (a.nr_minuto_duracao - 1) /1440))
				and 	not((a.ie_status_agenda = 'L') or (a.ie_status_agenda = 'C'));
			end if;

			IF	(qt_existe_agenda_w = 0) then
				BEGIN

				Obter_motivo_bloqueio_agenda(cd_agenda_p, hr_atual_w, ie_dia_semana_w, nr_seq_motivo_transf_w);
				consistir_bloqueio_agenda(cd_agenda_p, hr_atual_w, ie_dia_semana_w, ie_bloqueio_w);

				IF	(ie_bloqueio_w 		= 'N') or
					(ie_gerar_hor_bloq_w	= 'S') then
						BEGIN

						cd_turno_w		:= 0;

						IF	(To_Number(to_char(hr_atual_w,'hh24')) > somente_numero(HR_QUEBRA_TURNO_W)) or
							((To_Number(to_char(hr_atual_w,'hh24')) = somente_numero(HR_QUEBRA_TURNO_W)) and
							(To_Number(to_char(hr_atual_w,'mi')) >= somente_numero(qt_min_QUEBRA_TURNO_W))) then
							cd_turno_w	:= 1;
						END IF;
						
						nr_seq_preferencia_w := obter_pref_horario(hr_atual_w, null, obter_perfil_ativo, nr_seq_turno_w);
						
						ie_pref_lib_w := 'S';
						ie_gera_hor_w := 'S';
				
						if	(nr_seq_preferencia_w is not null) and
							(dt_agenda_p > trunc(sysdate + nvl(qt_dias_caducar_pref_w,0))) then
							ie_pref_lib_w := obter_se_pref_lib_agenda(nr_seq_preferencia_w, cd_agenda_p, obter_perfil_ativo, cd_pessoa_usuario_w);
						end if;					
						
						if	(ie_pref_lib_w = 'N') then
							ie_gera_hor_w := 'N';
						end if;

						if	(ie_gera_hor_w = 'S') then
						
							insert into ageint_consulta_horarios
								(dt_Atualizacao,
								nm_usuario,
								cd_agenda,
								cd_medico_exec,
								cd_paciente,
								nm_paciente,
								ie_status_agenda,
								nr_minuto_duracao,
								dt_Agenda,
								cd_turno,
								ie_encaixe,
								cd_especialidade,
								ie_classif_agenda)
							values
								(sysdate,
								nm_usuario_p,
								cd_Agenda_p,
								cd_medico_req_w,
								null,
								null,
								decode(ie_bloqueio_w, 'S', 'B', 'L'),
								nr_minuto_Intervalo_w,
								hr_atual_w,
								cd_turno_w,
								'N',
								cd_especialidade_w,
								ie_classif_agenda_w);
						end if;
						EXCEPTION
							WHEN others then
							Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||SQLERRM);

						END;
				END IF;
				END;
			END IF;
			END;
		END IF;

		hr_atual_w		:= hr_atual_w + (nr_minuto_intervalo_w / 1440);
		qt_horario_w 	:= qt_horario_w + 1;
/*		Elemar - 06/01/03 - permitir gerar qualquer qtde de horários
		if	(qt_horario_w > 51 ) then
			hr_atual_w	:= hr_final_w + 1;
		end if;
*/		END;
	END LOOP;
	END;
END LOOP;
CLOSE C01;

hr_fim_w		:= hr_nada_w;
ds_observacao_w		:= '';
ie_classif_agenda_w	:= 'L';


OPEN C02;
LOOP
FETCH C02 into
	hr_inicial_w,
	hr_final_w;
EXIT 	WHEN c02%notfound;

	IF	(hr_fim_w <> hr_nada_w) and
		(hr_inicial_w > hr_fim_w) then
		BEGIN
		/* abreiter -> tamanho do campo nr_minuto_intervalo_w estourava em algumas raras situaçoes.*/
		nr_minuto_intervalo_w	:= (hr_inicial_w - hr_fim_w) * 1440;
		hr_atual_w		:= hr_fim_w;

		IF	(nr_minuto_intervalo_w >= qt_min_minimo_w) then
			BEGIN

			select	count(*)
			into	qt_turno_w
			from 	agenda_Turno
			where 	cd_agenda     	= cd_agenda_p
	  		  and 	ie_dia_semana	= ie_dia_Semana_w
			  and	hr_atual_w	between
						to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
						to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi')
						and
						to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
						to_char(hr_final - 1/1440,'hh24:mi'),'dd/mm/yyyy hh24:mi');

			select	count(*)
			into	qt_turno_diario_w
			from 	agenda_Turno
			where 	cd_agenda     	= cd_agenda_p
	  		  and 	ie_dia_semana	= 9
			  and	hr_atual_w	between
						to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
						to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi')
						and
						to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
						to_char(hr_final  - 1/1440,'hh24:mi'),'dd/mm/yyyy hh24:mi')
			  and	qt_turno_w	= 0;

			--Obter classificação do turno
			select	max(ie_classif_agenda)
			into	ie_classif_agenda_w
			from 	agenda_Turno
			where 	cd_agenda     	= cd_agenda_p
	  		  and 	ie_dia_semana	= 9
			  and	hr_atual_w	between
						to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
						to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi')
						and
						to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
						to_char(hr_final  - 1/1440,'hh24:mi'),'dd/mm/yyyy hh24:mi');
			  


			IF	((qt_turno_w > 0) OR (qt_turno_diario_w > 0)) then
				BEGIN
				BEGIN
				cd_turno_w		:= 0;

				IF	(To_Number(to_char(hr_atual_w,'hh24')) > somente_numero(HR_QUEBRA_TURNO_W)) or
					((To_Number(to_char(hr_atual_w,'hh24')) = somente_numero(HR_QUEBRA_TURNO_W)) and
					(To_Number(to_char(hr_atual_w,'mi')) >= somente_numero(qt_min_QUEBRA_TURNO_W))) then
					cd_turno_w	:= 1;
				END IF;

				insert into ageint_consulta_horarios
						(dt_Atualizacao,
						nm_usuario,
						cd_agenda,
						cd_medico_exec,
						cd_paciente,
						nm_paciente,
						ie_status_agenda,
						nr_minuto_duracao,
						dt_Agenda,
						ie_encaixe,
						cd_especialidade,
						ie_classif_agenda)
					values
						(sysdate,
						nm_usuario_p,
						cd_Agenda_p,
						'',
						null,
						null,
						'L',
						nr_minuto_Intervalo_w,
						hr_atual_w,
						'N',
						cd_especialidade_w,
						ie_classif_agenda_w);
				EXCEPTION
					WHEN others then
					--nr_sequencia_w	:= nr_sequencia_w;
					Wheb_mensagem_pck.exibir_mensagem_abort( 262328 , 'DS_MENSAGEM='||SQLERRM);
				END;
				END;
			END IF;
			END;
		END IF;
		END;
	END IF;
	IF	(hr_final_w > hr_fim_w) then
		hr_fim_w	:= hr_final_w;
	END IF;
END LOOP;
CLOSE C02;

commit;

end ageint_gerar_consulta_agecons;
/
