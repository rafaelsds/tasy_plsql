create or replace
procedure pls_resposta_historico_prot(	cd_operadora_p				ptu_resp_cons_historico_pa.cd_operadora%type,
					cd_operadora_destino_p			ptu_resp_cons_historico_pa.cd_operadora_destino%type,
					cd_operadora_origem_p                   ptu_resp_cons_historico_pa.cd_operadora_origem%type,
					cd_transacao_p                          ptu_resp_cons_historico_pa.cd_transacao%type,
					nr_registro_ans_p                       ptu_resp_cons_historico_pa.nr_registro_ans%type,
					ie_tipo_cliente_p                       ptu_resp_cons_historico_pa.ie_tipo_cliente%type,
					cd_usuario_plano_p                      ptu_resp_cons_historico_pa.cd_usuario_plano%type,
					dt_geracao_p                            varchar2,
					nr_protocolo_p                          ptu_resp_cons_historico_pa.nr_protocolo%type,
					nr_seq_execucao_p                       ptu_resp_cons_historico_pa.nr_seq_execucao%type,
					nr_seq_protocolo_p                      ptu_resp_cons_historico_pa.nr_seq_protocolo%type,
					ie_origem_resposta_p                    ptu_resp_cons_historico_pa.ie_origem_resposta%type,
					dt_inicio_periodo_p                     varchar2,
					dt_fim_periodo_p                        varchar2,
					nr_trans_interc_orig_p			ptu_resp_cons_historico_pa.nr_trans_interc_orig%type,
					nr_trans_intercambio_p			ptu_resp_cons_historico_pa.nr_trans_intercambio%type,
					ie_resposta_p				ptu_resp_cons_historico_pa.ie_resposta%type,
					ie_tipo_manifestacao_p			ptu_resp_cons_historico_pa.ie_tipo_manifestacao%type,
					ie_tipo_categoria_p			ptu_resp_cons_historico_pa.ie_tipo_categoria%type,
					ie_tipo_sentimento_p			ptu_resp_cons_historico_pa.ie_tipo_sentimento%type,
					nm_beneficiario_p			ptu_resp_cons_historico_pa.nm_beneficiario%type,
					nm_usuario_p                            usuario.nm_usuario%type,
					cd_estabelecimento_p                    estabelecimento.cd_estabelecimento%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Procedure ir� apenas importar a resposta que foi recebida do WebService
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
pls_imp_xml_integracao_ws_pck.pls_resposta_historico_prot(	cd_operadora_p, cd_operadora_destino_p, cd_operadora_origem_p,
								cd_transacao_p, nr_registro_ans_p, ie_tipo_cliente_p,
								cd_usuario_plano_p, dt_geracao_p, nr_protocolo_p,
								nr_seq_execucao_p, nr_seq_protocolo_p, ie_origem_resposta_p,
								dt_inicio_periodo_p, dt_fim_periodo_p, nr_trans_interc_orig_p,
								nr_trans_intercambio_p, ie_resposta_p, ie_tipo_manifestacao_p,
								ie_tipo_categoria_p, ie_tipo_sentimento_p, nm_beneficiario_p,
								nm_usuario_p, cd_estabelecimento_p);

end pls_resposta_historico_prot;
/