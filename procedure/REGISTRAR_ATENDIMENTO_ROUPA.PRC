create or replace
procedure registrar_atendimento_roupa(		nr_sequencia_p		number,
					nm_usuario_p		varchar,
					nr_seq_inventario_p	number) is


qt_pendente_w  number(10);

begin

update	rop_inv_reposicao_item
set	dt_atendimento = sysdate,
	nm_usuario_atend = nm_usuario_p
where	nr_sequencia = nr_sequencia_p;

select	count (*)
into	qt_pendente_w
from	rop_inv_reposicao_item
where	nr_seq_inventario = nr_seq_inventario_p
and	dt_atendimento is null;

if	(qt_pendente_w = 0) then
	update	rop_inv_reposicao
	set	dt_atendimento = sysdate,
		nm_usuario_atend = nm_usuario_p
	where	nr_sequencia = nr_seq_inventario_p;
end if;

commit;

end registrar_atendimento_roupa;
/
