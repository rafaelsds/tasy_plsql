create or replace
procedure fa_motivo_alteracao_qtde_disp(nr_seq_item_p		number,
					nr_seq_motivo_p		number,
					qtd_dispensada_new_p	number,
					qtd_dispensada_old_p	number,
					ds_justificativa_p	varchar2,
					nm_usuario_p		varchar2) is 

begin

insert	
into 	fa_entrega_medic_item_disp
	(nr_sequencia, 
	 dt_atualizacao, 
	 nm_usuario, 
	 dt_atualizacao_nrec, 
	 nm_usuario_nrec, 
	 nr_seq_entrega_item, 
	 qt_disp_anterior, 
	 qt_disp_atual, 
	 nr_seq_motivo, 
	 ds_justificativa)
values	(fa_entrega_medic_item_disp_seq.nextval,
	 sysdate,
	 nm_usuario_p,
	 sysdate,
	 nm_usuario_p,
	 nr_seq_item_p,
	 qtd_dispensada_old_p,
	 qtd_dispensada_new_p,
	 nr_seq_motivo_p,
	 ds_justificativa_p);
	 
commit;

end fa_motivo_alteracao_qtde_disp;
/