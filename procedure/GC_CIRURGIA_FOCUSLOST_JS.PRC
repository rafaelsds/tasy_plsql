create or replace 
procedure	gc_cirurgia_focusLost_js(
					nm_atributo_p			varchar2,
					cd_tipo_cirurgia_p	in out	number,
					nr_seq_proc_interno_p		number,
					cd_medico_cirurgiao_p		varchar2,
					dt_inicio_real_p		date,
					nr_prescricao_p			number,
					dt_termino_p			date,
					cd_medico_anestesista_p	in out	varchar2,
					cd_procedimento_princ_p 	number,
					ie_origem_proced_p	 	number,
					dt_inicio_prevista_p		date,
					cd_tipo_anestesia_p	out	varchar2,
					nr_min_duracao_real_p	out	number,
					nr_minuto_dur_prev_p 	out	number,					
					nm_usuario_p			varchar2,
					cd_estabelecimento_p		number
					) is
					

ie_atualiza_duracao_w		varchar2(1);
ie_atualiza_tipo_anestesia_w	varchar2(1);
ie_atualiza_potencial_cont_w	varchar2(1);
ie_atua_data_inicio_real_w	varchar2(1);
ie_especialidade_w		varchar2(1);
ie_consiste_habilitacao_w	varchar2(1);
ie_consiste_hab_proc_w		varchar2(1);
nr_minuto_duracao_prev_w	number(10);
cd_tipo_cirurgia_w		number(10);
cd_tipo_anestesia_w		varchar2(10);
cod_espec_anestesista_w		varchar2(10);
cd_medico_anestesista_w		varchar2(10);
					
begin

obter_param_usuario(900, 91, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atua_data_inicio_real_w);
obter_param_usuario(900, 255, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, cod_espec_anestesista_w);
obter_param_usuario(900, 273, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_habilitacao_w);

if	(nm_atributo_p = 'NR_SEQ_PROC_INTERNO') and	(nvl(nr_seq_proc_interno_p,0) > 0) then
	begin
	
	if	(cd_tipo_cirurgia_p is null) then
		begin
		cd_tipo_cirurgia_w := obter_dados_proc_interno(nr_seq_proc_interno_p,'TC');
		end;
	end if;
	
	if	(nr_seq_proc_interno_p is not null) then
		begin
		
		obter_param_usuario(900, 162, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_duracao_w);
		obter_param_usuario(900, 163, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_tipo_anestesia_w);
		obter_param_usuario(900, 164, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_potencial_cont_w);
		
		if	(ie_atualiza_duracao_w = 'S') then
			begin
			
			nr_minuto_duracao_prev_w := obter_tempo_total_proced(nr_seq_proc_interno_p,cd_medico_cirurgiao_p);
			
			if	(nr_minuto_duracao_prev_w > 0) then
				begin
				nr_minuto_dur_prev_p := nr_minuto_duracao_prev_w;
				end;
			end if;
			end;
		end if;
		
		if	(ie_atualiza_tipo_anestesia_w = 'S') then
			begin
			
			select	max(cd_tipo_anestesia)
			into 	cd_tipo_anestesia_w
			from	proc_interno
			where	nr_sequencia = nr_seq_proc_interno_p;
			
			end;
		end if;
		
		if	(ie_atualiza_potencial_cont_w = 'S') then
			begin
			
			select	max(cd_tipo_cirurgia)
			into	cd_tipo_cirurgia_w
			from	proc_interno
			where	nr_sequencia = nr_seq_proc_interno_p;
			
			end;
		end if;
		end;
	end if;	
	end;
end if;

if	(nm_atributo_p = 'DT_INICIO_REAL') and
	(ie_atua_data_inicio_real_w = 'S') then
	begin
	
	update prescr_medica
	set    dt_prescricao   = dt_inicio_real_p
	where  nr_prescricao   = nr_prescricao_p;
	
	end;
end if;

if	(nm_atributo_p = 'DT_TERMINO') then
	begin
	
	nr_min_duracao_real_p := obter_minutos_espera(dt_inicio_real_p,dt_termino_p);
	
	end;
end if;

if	(nm_atributo_p = 'CD_MEDICO_ANESTESISTA') and
	(cd_medico_anestesista_p is not null) and
	(cod_espec_anestesista_w <> '') then
	begin
	
	ie_especialidade_w := obter_se_especialidade_medico(cd_medico_anestesista_p,cod_espec_anestesista_w);
	
	if	(ie_especialidade_w = 'N') then
		begin
		cd_medico_anestesista_w := '';
		Wheb_mensagem_pck.exibir_mensagem_abort(262186, 'MENSAGEM='||substr(obter_texto_tasy (49966, wheb_usuario_pck.get_nr_seq_idioma),1,255));
		end;
	end if;
	end;
end if;

ie_consiste_hab_proc_w := sus_consiste_habilitacao_proc(cd_procedimento_princ_p,ie_origem_proced_p,dt_inicio_prevista_p,cd_estabelecimento_p);	

if	((nm_atributo_p = 'NR_SEQ_PROC_INTERNO') or
	(nm_atributo_p = 'CD_PROCEDIMENTO_PRINC')) and
	(ie_consiste_hab_proc_w = 'N') and
	(ie_consiste_habilitacao_w = 'S') then
	Wheb_mensagem_pck.exibir_mensagem_abort(262186, 'MENSAGEM='||substr(obter_texto_tasy (49967, wheb_usuario_pck.get_nr_seq_idioma),1,255));
end if;

cd_tipo_cirurgia_p 	:= cd_tipo_cirurgia_w;
cd_tipo_anestesia_p 	:= cd_tipo_anestesia_w;

end gc_cirurgia_focusLost_js;
/