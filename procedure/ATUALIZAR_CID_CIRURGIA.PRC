create or replace
procedure atualizar_cid_cirurgia(
			nr_seq_descricao_p	number,
			nm_usuario_p		varchar2) is 

cd_doenca_cid_w		varchar2(10);	
cd_medico_w		varchar2(10);
nr_atendimento_w	number(10);
qt_reg_w		number(10);		
			
begin

select 	max(b.cd_doenca_cid),
	max(a.nr_atendimento),
	max(nvl(b.cd_responsavel,a.cd_medico_cirurgiao))
into   	cd_doenca_cid_w,
	nr_atendimento_w,
	cd_medico_w
from   	cirurgia a,
	cirurgia_descricao b
where  	a.nr_cirurgia = b.nr_cirurgia
and    	b.nr_sequencia = nr_seq_descricao_p;

if	(cd_doenca_cid_w is not null) and 
	(nr_atendimento_w > 0) then
	
	Gerar_diagnostico_Atend(nr_atendimento_w,cd_doenca_cid_w,cd_medico_w,nm_usuario_p,2,'P',sysdate);
	
end if;


commit;

end atualizar_cid_cirurgia;
/
