create or replace
PROCEDURE gerar_equipamento_gases_fanep(nr_seq_pepo_p		NUMBER,
				  nr_cirurgia_p		NUMBER,
				  nm_usuario_p		VARCHAR2) IS

cd_equipamento_w 	NUMBER(10);
cd_profissional_w	VARCHAR2(10);
dt_fim_w		DATE;
dt_inicio_w		DATE;
nr_seq_equipamento_w	NUMBER(10);

CURSOR c01 IS
	SELECT	b.cd_equipamento,
		a.cd_profissional,
		c.dt_inicio_adm,
		c.dt_final_adm
	FROM	agente_anestesico b,
		cirurgia_agente_anestesico a,
		cirurgia_agente_anest_ocor c
	WHERE 	c.nr_seq_cirur_agente = a.nr_sequencia
	AND	a.nr_seq_agente	= b.nr_sequencia
	AND	((a.nr_seq_pepo = nr_seq_pepo_p) OR (a.nr_cirurgia = nr_cirurgia_p))
	AND 	b.cd_equipamento IS NOT NULL
	and	nvl(a.ie_situacao,'A') = 'A'
	and	nvl(c.ie_situacao,'A') = 'A';

BEGIN

DELETE equipamento_cirurgia
WHERE	((nr_seq_pepo = nr_seq_pepo_p) OR
	 (nr_cirurgia = nr_cirurgia_p))
AND 	ie_gerado_sistema = 'S';
COMMIT;

OPEN c01;
LOOP
FETCH c01 INTO
	cd_equipamento_w,
	cd_profissional_w,
	dt_inicio_w,
	dt_fim_w;
EXIT WHEN c01%NOTFOUND;
	BEGIN
	SELECT 	equipamento_cirurgia_seq.NEXTVAL
	INTO	nr_seq_equipamento_w
	FROM 	dual;

	INSERT INTO equipamento_cirurgia(
		nr_sequencia,
		cd_equipamento,
		dt_lancamento_automatico,
		nr_seq_pepo,
		nr_cirurgia,
		cd_profissional,
		dt_atualizacao,
		nm_usuario,
		qt_equipamento,
		dt_fim,
		dt_inicio,
		ie_gerado_sistema,
		ie_situacao)
	VALUES	(nr_seq_equipamento_w,
		cd_equipamento_w,
		SYSDATE,
		DECODE(nr_seq_pepo_p,0,NULL,nr_seq_pepo_p),
		nr_cirurgia_p,
		cd_profissional_w,
		SYSDATE,
		nm_usuario_p,
		1,
		dt_fim_w,
		dt_inicio_w,
		'S',
		'A');
	COMMIT;
	gerar_taxa_equipamento_fanep(wheb_usuario_pck.get_cd_estabelecimento,nr_seq_equipamento_w,nm_usuario_p);
	END;
END LOOP;
CLOSE c01;

COMMIT;

END gerar_equipamento_gases_fanep;
/