create or replace
procedure pls_oc_cta_tratar_val_19_imp(	nr_seq_combinada_p		in pls_oc_cta_combinada.nr_sequencia%type,
					ie_regra_excecao_p		in pls_oc_cta_combinada.ie_excecao%type,
					nr_id_transacao_p		in pls_oc_cta_selecao_imp.nr_id_transacao%type) is 

tb_seq_selecao_w			pls_util_cta_pck.t_number_table;
tb_valido_w				pls_util_cta_pck.t_varchar2_table_1;
tb_observacao_w				pls_util_cta_pck.t_varchar2_table_4000;

--Cursor das validacoes cadastradas para regra combinada
cursor C01 (	nr_seq_oc_cta_comb_pc	pls_oc_cta_combinada.nr_sequencia%type) is
	select	a.ie_val_profs_exec_comp,
			nvl(a.ie_tipo_profissional, 'PE' ) ie_tipo_profissional,
			nvl(a.ie_valida_cpf, 'N') ie_valida_cpf,
			nvl(a.ie_profissional_inativo, 'N') ie_profissional_inativo
	from	pls_oc_cta_val_profis a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_pc;

--Cursor das contas selecionadas que gerarao a ocorrencia
Cursor C02(	nr_id_transacao_pc	pls_oc_cta_selecao_imp.nr_id_transacao%type) is
	select	conta.nr_sequencia,
		'S' ie_valido,
		null ds_observacao
	from	pls_conta_imp		conta
	where	exists (	select 1
				from	pls_oc_cta_selecao_imp sel
				where	sel.nr_id_transacao = nr_id_transacao_pc
				and  	sel.ie_valido = 'S'
				and  	sel.nr_seq_conta = conta.nr_sequencia)
	and	conta.cd_profissional_exec_conv is null;

Cursor C03(	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
		ie_tipo_profissional_pc	pls_oc_cta_val_profis.ie_tipo_profissional%type
			) is

	select	nr_seq_selecao,
			ie_valido,
			ds_observacao
	from (
		select	conta.nr_sequencia nr_seq_selecao,
			'S' ie_valido,
			decode(ie_tipo_profissional_pc , 'PE', pls_obter_dados_medico(conta.cd_profissional_exec_conv, 'CPF') ,
					pls_obter_dados_medico(conta.cd_profissional_solic_conv, 'CPF')) nr_cpf,
			decode( ie_tipo_profissional_pc , 'PE', conta.cd_profissional_exec_conv, conta.cd_profissional_solic_conv) cd_medico,
			null ds_observacao
		from	pls_conta_imp		conta
		where	exists (	select 1
					from	pls_oc_cta_selecao_imp sel
					where	sel.nr_id_transacao = nr_id_transacao_pc
					and  	sel.ie_valido = 'S'
					and  	sel.nr_seq_conta = conta.nr_sequencia)
	) v
	where	v.nr_cpf is null
	and 	v.cd_medico is not null; --Apenas aplica validao do CPF, caso o profissional do tipo adequado estiver presente.

--Cursor de participantes com CPF nulo no cadastro do medico ou sem a informacao do medico apontada no registro do participante	
Cursor C04(	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type
			) is

	select		x.nr_sequencia	nr_seq_selecao,
			'S' ie_valido,
			null ds_observacao
	from	pls_oc_cta_selecao_imp	x,
			pls_conta_ocor_v		a
	where	x.ie_valido		= 'S'
	and		x.nr_id_transacao	= nr_id_transacao_pc
	and		a.nr_sequencia 		= x.nr_seq_conta
	and   exists (select   1 
				  from  pls_conta_proc p,
						pls_proc_participante x,
						pessoa_fisica y
				  where   p.nr_seq_conta = a.nr_sequencia
				  and   p.nr_sequencia = x.nr_seq_conta_proc 
				  and   x.cd_medico_imp = y.cd_pessoa_fisica    
				  and   y.nr_cpf is null);

Cursor C05(	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
		ie_tipo_profissional_pc	pls_oc_cta_val_profis.ie_tipo_profissional%type
			) is

	select	nr_seq_selecao,
			ie_valido,
			ds_observacao
	from (
		select	conta.nr_sequencia nr_seq_selecao,
			'S' ie_valido,
			decode(ie_tipo_profissional_pc , 'PE', pls_obter_dados_medico(conta.cd_profissional_exec_conv, 'IN') ,
					pls_obter_dados_medico(conta.cd_profissional_solic_conv, 'IN')) ie_profissional_ativo,
			decode( ie_tipo_profissional_pc , 'PE', conta.cd_profissional_exec_conv, conta.cd_profissional_solic_conv) cd_medico,
			null ds_observacao
		from	pls_conta_imp		conta
		where	exists (	select 1
					from	pls_oc_cta_selecao_imp sel
					where	sel.nr_id_transacao = nr_id_transacao_pc
					and  	sel.ie_valido = 'S'
					and  	sel.nr_seq_conta = conta.nr_sequencia)
	) v
	where	v.ie_profissional_ativo is null
	and 	v.cd_medico is not null; --Apenas aplica a validacao de profissional ativo, caso o profissional do tipo adequado estiver presente.

--Cursor de participantes com CPF nulo no cadastro do medico ou sem a informacao do medico apontada no registro do participante	
Cursor C06(	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type
			) is

	select		x.nr_sequencia	nr_seq_selecao,
			'S' ie_valido,
			null ds_observacao
	from	pls_oc_cta_selecao_imp	x,
			pls_conta_ocor_v		a
	where	x.ie_valido		= 'S'
	and		x.nr_id_transacao	= nr_id_transacao_pc
	and		a.nr_sequencia 		= x.nr_seq_conta
	and   exists (select   1 
				  from  pls_conta_proc p,
						pls_proc_participante x,
						medico y
				  where   p.nr_seq_conta = a.nr_sequencia
				  and   p.nr_sequencia = x.nr_seq_conta_proc 
				  and   x.cd_medico_imp = y.cd_pessoa_fisica    
				  and   y.ie_situacao = 'I');
				  
begin

-- Deve existir a informacao da regra e transacaopara aplicar a validacao
if	(nr_seq_combinada_p is not null) and
	(nr_id_transacao_p is not null)  then
	
	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_ocor_imp_pck.atualiza_campo_auxiliar ('V', 'N', nr_id_transacao_p, null);
	
	--limpa as variaveis
	pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
						tb_valido_w,
						tb_observacao_w);
	
	for r_C01_w in C01(nr_seq_combinada_p) loop
		
		if	(r_C01_w.ie_val_profs_exec_comp = 'S')	then
			begin			
				open C02(nr_id_transacao_p);
				loop
					fetch C02 bulk collect into tb_seq_selecao_w, tb_valido_w, tb_observacao_w 
					limit pls_util_cta_pck.qt_registro_transacao_w;
					exit when tb_seq_selecao_w.count = 0;
					--Grava as informacoes na tabela de selecao
					pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
											tb_observacao_w, nr_id_transacao_p, 
											'SEQ_CONTA');
					--limpa as variaveis
					pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
										tb_valido_w,
										tb_observacao_w);
				end loop;
				close C02;
			exception
			when others then
				--Fecha cursor
				if	(C02%isopen) then
				
					close C02;
				end if;	
			end;
		end if;
		
		if (r_C01_w.ie_valida_cpf = 'S' ) then
		
			--Verificacao por tipo de profissional executor ou solicitante.
			if (r_C01_w.ie_tipo_profissional in ('PE', 'PS')) then
			
				begin
					open C03(nr_id_transacao_p, r_C01_w.ie_tipo_profissional);					
						loop
						fetch C03 bulk collect into tb_seq_selecao_w, tb_valido_w, tb_observacao_w 
						limit pls_util_cta_pck.qt_registro_transacao_w;
						exit when tb_seq_selecao_w.count = 0;
						--Grava as informacoes na tabela de selecao
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
												tb_observacao_w, nr_id_transacao_p, 
												'SEQ_CONTA');
						--limpa as variaveis
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
											tb_valido_w,
											tb_observacao_w);
					end loop;
					close C03;
				exception
				when others then
					--Fecha cursor
					if	(C03%isopen) then
					
						close C03;
					end if;	
				end;
			
			--Verificacao por tipo de profissional participante(PP). Verifica se algum medico participante nao tem seu CPF informado.
			else
				begin 
				open C04(nr_id_transacao_p);
						loop
						fetch C04 bulk collect into tb_seq_selecao_w, tb_valido_w, tb_observacao_w 
						limit pls_util_cta_pck.qt_registro_transacao_w;
						exit when tb_seq_selecao_w.count = 0;
						--Grava as informacoes na tabela de selecao
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
												tb_observacao_w, nr_id_transacao_p, 
												'SEQ_CONTA');
						--limpa as variaveis
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
											tb_valido_w,
											tb_observacao_w);
					end loop;
					
					close C04;
				exception
				when others then
					--Fecha cursor
					if	(C04%isopen) then
					
						close C04;
					end if;	
				end;
			end if;
		
		end if;		
		
		if (r_C01_w.ie_profissional_inativo = 'S' ) then
		
			--Verificacao por tipo de profissional executor ou solicitante.
			if (r_C01_w.ie_tipo_profissional in ('PE', 'PS')) then
			
				begin
					open C05(nr_id_transacao_p, r_C01_w.ie_tipo_profissional);					
						loop
						fetch C05 bulk collect into tb_seq_selecao_w, tb_valido_w, tb_observacao_w 
						limit pls_util_cta_pck.qt_registro_transacao_w;
						exit when tb_seq_selecao_w.count = 0;
						--Grava as informacoes na tabela de selecao
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
												tb_observacao_w, nr_id_transacao_p, 
												'SEQ_CONTA');
						--limpa as variaveis
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
											tb_valido_w,
											tb_observacao_w);
					end loop;
					close C05;
				exception
				when others then
					--Fecha cursor
					if	(C05%isopen) then
					
						close C05;
					end if;	
				end;
			
			--Verificacao por tipo de profissional participante(PP) para verificar se esta inativo
			else
				begin 
				open C04(nr_id_transacao_p);
						loop
						fetch C06 bulk collect into tb_seq_selecao_w, tb_valido_w, tb_observacao_w 
						limit pls_util_cta_pck.qt_registro_transacao_w;
						exit when tb_seq_selecao_w.count = 0;
						--Grava as informacoes na tabela de selecao
						pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
												tb_observacao_w, nr_id_transacao_p, 
												'SEQ_CONTA');
						--limpa as variaveis
						pls_ocor_imp_pck.limpar_nested_tables(	tb_seq_selecao_w,
											tb_valido_w,
											tb_observacao_w);
					end loop;
					
					close C06;
				exception
				when others then
					--Fecha cursor
					if	(C06%isopen) then
					
						close C06;
					end if;	
				end;
			end if;		
		end if;				
	end loop;
	--Grava o que restar nas variaveis na tabela
	pls_ocor_imp_pck.gerencia_selecao_validacao(	tb_seq_selecao_w, tb_valido_w,
							tb_observacao_w, nr_id_transacao_p, 
							'SEQ_CONTA');
	
	-- seta os registros que serao validos ou invalidos apos o processamento 
	pls_ocor_imp_pck.atualiza_campo_valido ('V', 'N', 
						ie_regra_excecao_p, null,
						nr_id_transacao_p, null);
end if;
	
end pls_oc_cta_tratar_val_19_imp;
/