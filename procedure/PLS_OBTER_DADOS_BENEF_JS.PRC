create or replace
procedure pls_obter_dados_benef_js(
						nr_seq_segurado_p		number,
						ie_opcao_p			varchar2,
						cd_pessoa_fisica_p		out	varchar2,
						nr_carteira_p			out	varchar2,
						nr_seq_contrato_p		out	varchar2,
						nr_contrato_p			out	varchar2,
						ds_estipulante_p		out	varchar2,
						dt_adesao_p			out	varchar2,
						dt_liberacao_p			out	varchar2,
						dt_migracao_p			out	varchar2,
						dt_reativacao_p			out	varchar2,
						ds_pagador_p			out	varchar2,
						ds_vendedor_p			out	varchar2,
						ds_subestipulante_p		out	varchar2,
						ds_titular_p			out	varchar2,
						dt_inclusao_p			out	varchar2,
						dt_rescisao_p			out	varchar2,
						dt_limite_p			out	varchar2,
						ds_produto_p			out	varchar2,
						nm_fantasia_p			out	varchar2,
						nr_protocolo_ans_p		out	varchar2,
						ie_contratacao_p		out	varchar2,
						ds_seg_assist_p			out	varchar2,
						ds_formacao_preco_p		out	varchar2,
						ds_abrang_geo_p			out	varchar2,
						cd_anterior_p			out	varchar2,
						ie_regulamentacao_p		out	varchar2,
						ds_acomodacao_p			out	varchar2,
						ie_situacao_p			out	varchar2,
						ds_fator_p			out	varchar2,
						ds_origem_cob_p			out	varchar2,
						nr_seq_plano_p			out	number,
						nr_seq_rol_p			out	number,
						ds_origem_carencia_p		out	varchar2,
						ds_origem_limitacao_p		out	varchar2,
						ds_canal_venda_p		out	varchar2,
						dt_validade_carteira_p		out	varchar2,
						ds_beneficiario_p		out	varchar2,
						ds_proc_contrat_p		out	varchar2,
						dt_suspensao_p			out	varchar2,
						ds_suspensao_p			out	varchar2,
						dt_devolucao_p			out	varchar2,
						ie_nascido_plano_p		out	varchar2,
						ds_parentesco_p			out	varchar2,
						cd_cartao_intercambio_p		out	varchar2,
						ds_motivo_rescisao_p		out	varchar2,
						ds_tipo_segurado_p		out	varchar2,
						ie_situacao_trab_p		out	varchar2,
						dt_mes_reajuste_p		out	varchar2,
						ie_tipo_vinculo_operadora_p	out	varchar2,
						dt_nascimento_p			out	varchar2,
						nr_cartao_nac_sus_p		out	varchar2) is

/*	ie_opcao_p -> OPS - Dossie Beneficiario (Java)
	D - Dados Beneficiário
*/
ds_link_w		varchar2(255);
cd_cco_w		pls_segurado.cd_cco%type;
cd_scpa_w		pls_plano.cd_scpa%type;
ds_classif_benef_w	varchar2(255);
cd_operadora_empresa_w	number;

begin

pls_obter_dados_beneficiario(	nr_seq_segurado_p, nr_carteira_p, nr_seq_contrato_p,
				nr_contrato_p, ds_estipulante_p, dt_adesao_p,
				dt_liberacao_p, dt_migracao_p, dt_reativacao_p,
				ds_pagador_p, ds_vendedor_p, ds_subestipulante_p,
				ds_titular_p, dt_inclusao_p, dt_rescisao_p,
				dt_limite_p, ds_produto_p, nm_fantasia_p,
				nr_protocolo_ans_p, ie_contratacao_p, ds_seg_assist_p,
				ds_formacao_preco_p, ds_abrang_geo_p, cd_anterior_p,
				ie_regulamentacao_p, ds_acomodacao_p, ie_situacao_p,
				ds_fator_p, ds_origem_cob_p, nr_seq_plano_p,
				nr_seq_rol_p, ds_origem_carencia_p, ds_origem_limitacao_p,
				ds_canal_venda_p, dt_validade_carteira_p, dt_suspensao_p,
				ds_suspensao_p,	dt_devolucao_p,	ie_nascido_plano_p,
				ds_parentesco_p, cd_cartao_intercambio_p, ds_tipo_segurado_p,
				ie_situacao_trab_p, dt_mes_reajuste_p, ie_tipo_vinculo_operadora_p,
				ds_link_w, cd_cco_w, cd_scpa_w, dt_nascimento_p,
				nr_cartao_nac_sus_p,ds_classif_benef_w,cd_operadora_empresa_w);

select	decode(nr_seq_plano_p, null, substr(obter_texto_tasy(55174, wheb_usuario_pck.get_nr_seq_idioma),1,255), substr(pls_obter_acomodacoes_plano(nr_seq_plano_p),1,80))
into	ds_acomodacao_p
from	dual;

ds_motivo_rescisao_p := substr(pls_obter_dados_segurado(nr_seq_segurado_p,'DMR'),1,255);

if	(ie_opcao_p is not null) then
	begin
	if	(ie_opcao_p = 'D') then
		begin
		select	substr(pls_obter_dados_segurado(nr_seq_segurado_p, 'PF'), 1,10)
		into	cd_pessoa_fisica_p
		from	dual;
		
		select	'Beneficiário: ' || substr(pls_obter_dados_segurado(nr_seq_segurado_p, 'N'), 1, 60),
				'Produto Contratado: ' || ds_produto_p || ' (' || nr_seq_plano_p || ')'
		into	ds_beneficiario_p,
				ds_proc_contrat_p
		from	dual;
		end;
	end if;
	end;
end if;

end pls_obter_dados_benef_js;
/