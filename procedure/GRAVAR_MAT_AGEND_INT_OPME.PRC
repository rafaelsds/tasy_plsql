create or replace
procedure gravar_mat_agend_int_opme(	cd_material_p		number,
					qt_unitaria_p		number,
					cd_fornecedor_p		varchar2,
					nr_prescricao_p		number,	
					nr_seq_prescricao_p	number,	
					ie_acao_p		number,
					nm_usuario_p		Varchar2) is 

nr_sequencia_w		number(10);
nr_seq_agenda_w		number(10);
qt_existe_w		varchar2(1);	
nr_seq_opme_w		number(10);					
					
begin

select 	nr_seq_agenda
into	nr_seq_agenda_w
from	prescr_medica
where 	nr_prescricao =	nr_prescricao_p;	

if	(ie_acao_p = 1) then
	begin
	select 	agenda_pac_opme_seq.nextval
	into	nr_sequencia_w
	from 	dual;
	
	insert into agenda_pac_opme(
		cd_cgc, 
		cd_cond_pagto, 
		cd_material, 
		ds_observacao, 
		dt_atualizacao, 
		dt_atualizacao_nrec, 
		dt_exclusao, 
		ie_autorizado, 
		ie_gerar_autor, 
		ie_integracao, 
		ie_integracao_util, 
		ie_origem_inf, 
		ie_padrao, 
		nm_usuario, 
		nm_usuario_exclusao, 
		nm_usuario_nrec, 
		nr_seq_agenda, 
		nr_seq_apres, 
		nr_seq_motivo_exclusao, 
		nr_seq_proc_interno, 
		nr_sequencia, 
		qt_material, 
		vl_desconto, 
		vl_unitario_item,
		nr_seq_prescricao,
		nr_prescricao)
	values	(cd_fornecedor_p,
		'',
		cd_material_p,
		'',
		sysdate,
		sysdate,
		'',
		'P',
		'N',
		'S',
		'',
		'I',
		'S',
		nm_usuario_p,
		'',
		nm_usuario_P,
		nr_seq_agenda_w,
		500,
		null,
		null,
		nr_sequencia_w,
		qt_unitaria_p,
		'',
		'',
		nr_seq_prescricao_p,
		nr_prescricao_p);
	end;
end if;

if	(ie_acao_p = 2) then
	begin
	
	update	agenda_pac_opme
	set	qt_material = qt_unitaria_p,
		cd_material = cd_material_p,
		cd_cgc = cd_fornecedor_p
	where	nr_seq_agenda	= nr_seq_agenda_w
	and	nr_seq_prescricao = nr_seq_prescricao_p
	and	nvl(nr_prescricao, nr_prescricao_p) = nr_prescricao_p;
	
	end;
end if;

if	(ie_acao_p = 3) then
	begin

	delete	from agenda_pac_opme
	where	cd_material = cd_material_p
	and	nr_seq_prescricao = nr_seq_prescricao_p
	and   	nr_seq_agenda = nr_seq_agenda_w
	and	nvl(nr_prescricao, nr_prescricao_p) = nr_prescricao_p;
	
	end;
end if;

commit;

end gravar_mat_agend_int_opme;
/