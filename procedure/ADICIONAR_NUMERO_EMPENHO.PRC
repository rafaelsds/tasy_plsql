create or replace
procedure adicionar_numero_empenho(	nr_ordem_compra_p		number,
				cd_conta_contabil_p		varchar2,
				nr_empenho_p			varchar2,
				nm_usuario_p			varchar2) is 

begin
if	(cd_conta_contabil_p is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265564);
	--'N�o � poss�vel inserir n�mero de empenho se n�o possuir conta cont�bil.'
end if;

update	ordem_compra_item
set	nr_empenho	= nr_empenho_p,
	dt_empenho	= sysdate
where	nr_ordem_compra	= nr_ordem_compra_p
and	cd_conta_contabil	= cd_conta_contabil_p;

commit;

end adicionar_numero_empenho;
/
