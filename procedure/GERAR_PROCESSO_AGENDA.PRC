CREATE OR REPLACE
PROCEDURE Gerar_Processo_Agenda
		(nr_seq_agenda_p	Number,
		cd_processo_p		number,
		cd_setor_exclusivo_p	number,
		nm_usuario_p		Varchar2) IS 


nr_etapas_w				Number(5);
nr_seq_etapa_w				Number(10,0);
ie_responsavel_w			Varchar2(3);
nr_minutos_prev_w			Number(9,3);
cd_setor_resp_w				Number(5);
cd_pessoa_fisica_resp_w			Varchar2(10);
cd_cgc_resp_w				Varchar2(14);
nm_usuario_resp_w			Varchar2(15);
dt_fim_prev_w				Date;
nr_seq_processo_w			number(10,0);

CURSOR C000 IS
	select 	nr_sequencia,
		ie_responsavel,
		nr_minutos_prev,
		cd_setor_resp,
		cd_pessoa_fisica_resp,
		cd_cgc_resp,
		nm_usuario_resp
	from 	Processo_etapa
	where 	cd_processo 	= cd_processo_p;

BEGIN

begin
	select 	count(*)
	into 	nr_etapas_w
	from 	agenda_pac_processo
	where 	nr_seq_agenda	= nr_seq_agenda_p
	  and 	cd_processo	= cd_processo_p;
	exception
		when others then
			nr_etapas_w := 0;
end;

if  	(nr_etapas_w 	= 0) then
	begin
	OPEN C000;
	LOOP
	FETCH C000 INTO
		 	nr_seq_etapa_w,
			ie_responsavel_w,
			nr_minutos_prev_w,
			cd_setor_resp_w,
			cd_pessoa_fisica_resp_w,
			cd_cgc_resp_w,
			nm_usuario_resp_w;
	exit when c000%notfound;
		begin
---------------------	Usu�rio
		if	(ie_responsavel_w = '2') then
			nm_usuario_resp_w	:= nm_usuario_p;
		end if;
---------------------	M�dico
		if	(ie_responsavel_w = '3') then
			select	cd_medico
			into	cd_pessoa_fisica_resp_w
			from	agenda_paciente
			where	nr_sequencia = nr_seq_agenda_p;
		end if;
---------------------	Paciente
		if	(ie_responsavel_w = '4') then
			select	cd_pessoa_fisica
			into 	cd_pessoa_fisica_resp_w
			from	agenda_paciente
			where	nr_sequencia = nr_seq_agenda_p;
		end if;
---------------------	Setor de Atendimento
		if	(ie_responsavel_w = '5') then
			cd_setor_resp_w := cd_setor_exclusivo_p; 
		end if;

		dt_fim_prev_w := sysdate + (nr_minutos_prev_w	/ 1440);

		select	agenda_pac_processo_seq.nextval
		into	nr_seq_processo_w
		from	dual;

		insert into agenda_pac_processo
			(nr_sequencia,
			nr_seq_agenda,
			cd_processo,
			nr_seq_etapa,                
			dt_atualizacao,
			nm_usuario,
			ie_status,
			nr_min_prev,
			dt_fim_prev,                
			nm_usuario_resp,   
			cd_pessoa_fisica_resp,
			cd_setor_resp)
		Values
			(nr_seq_processo_w,
			nr_seq_agenda_p,
			cd_processo_p,
			nr_seq_etapa_w,
			sysdate,
			nm_usuario_p,
			'P',
			nr_minutos_prev_w,
			dt_fim_prev_w,
			nm_usuario_resp_w,
			cd_pessoa_fisica_resp_w,
			cd_setor_resp_w);                
		end;
	END LOOP;
	CLOSE C000;

	end;
end if;

END Gerar_Processo_Agenda;
/