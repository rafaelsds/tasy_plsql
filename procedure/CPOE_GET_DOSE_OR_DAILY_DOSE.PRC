create or replace procedure cpoe_get_dose_or_daily_dose (
  ie_via_aplicacao_p in varchar2,
  cd_intervalo_p in varchar2, 
  qt_dose_p in out number, 
  qt_dose_dia_p in out number) is
  
  ie_formato_aplicacao_w  via_aplicacao.ie_formato_administracao%type := null;
  qt_operacao_w           intervalo_prescricao.qt_operacao%type := null;
  qt_dose_calc_w          cpoe_material.qt_dose%type := null;
  qt_dose_dia_calc_w      cpoe_material.qt_dose_dia%type := null;
begin
  select  max(ie_formato_administracao)
  into    ie_formato_aplicacao_w
  from    via_aplicacao
  where   ie_via_aplicacao = ie_via_aplicacao_p;
  
  select  max(qt_operacao)
  into    qt_operacao_w
  from    intervalo_prescricao
  where   cd_intervalo = cd_intervalo_p;
  
  if (ie_formato_aplicacao_w is not null and qt_operacao_w > 0) then  
    if (qt_dose_p is not null and qt_dose_dia_p is null) then
      qt_dose_dia_calc_w := qt_dose_p * qt_operacao_w;
    end if;  
    if (qt_dose_p is null and qt_dose_dia_p is not null) then
      qt_dose_calc_w := qt_dose_dia_p / qt_operacao_w;
    end if;    
  end if;
  
  qt_dose_p := qt_dose_calc_w;
  qt_dose_dia_p := qt_dose_dia_calc_w;
end cpoe_get_dose_or_daily_dose;
/
