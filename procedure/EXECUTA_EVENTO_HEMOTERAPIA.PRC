create or replace
procedure executa_evento_hemoterapia (
				nm_usuario_p	varchar2,
				nr_seq_doacao_p	number,
				ie_opcao_p	number	) is

ie_opcao_w	evento_tasy.ie_taxa_servico%type;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;
nr_seq_evento_w	evento_tasy.nr_sequencia%type;
ie_tipo_coleta_w SAN_DOACAO.ie_tipo_coleta%type;

begin

if (nr_seq_doacao_p is not null) and (nr_seq_doacao_p > 0) then

	select	max(nr_atendimento), 
			max(ie_tipo_coleta)
	into	nr_atendimento_w, 
			ie_tipo_coleta_w
	from 	san_doacao
	where	nr_sequencia = nr_seq_doacao_p ;

	if (ie_opcao_p = 1) then --san - liberar exames

		nr_seq_evento_w := 567;

	elsif (ie_opcao_p = 2) then --san - liberar para a coleta

		nr_seq_evento_w := 565;

	elsif (ie_opcao_p = 3) then --san - confirmar coleta

		nr_seq_evento_w := 566;

	elsif (ie_opcao_p = 4) then --san - liberar sorologia

		nr_seq_evento_w := 568;

	elsif (ie_opcao_p = 5) then --san - finalizar produ��o da doa��o

		nr_seq_evento_w := 569;

	elsif (ie_opcao_p = 6) then --san - inaptid�o do doador

		nr_seq_evento_w := 570;

	end if;

	select	decode(count(1),0,'N','S')
	into	ie_opcao_w
	from	regra_lanc_automatico
	where	nr_seq_evento = nr_seq_evento_w
	and	ie_situacao = 'A';

	if (ie_opcao_w = 'S') then
		if (nvl(nr_atendimento_w,0) = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(294763);

		else
			gerar_lancamento_automatico( nr_atendimento_w ,null, nr_seq_evento_w , nm_usuario_p , null, ie_tipo_coleta_w, null, null, null, null);
		end if;
	end if;

end if;

commit;

end executa_evento_hemoterapia;
/