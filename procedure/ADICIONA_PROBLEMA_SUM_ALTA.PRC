create or replace
procedure adiciona_problema_sum_alta(	nr_seq_atend_sumario_p	number,
										nr_seq_problema_p 		number,
										nm_usuario_p			varchar2) is 
begin
insert into atend_sumario_alta_item (
			 nr_sequencia,
			 cd_doenca,
			 dt_atualizacao,
			 nm_usuario,
			 dt_atualizacao_nrec,
			 nm_usuario_nrec,
			 nr_seq_exame,
			 ie_tipo_item,
			 cd_recomendacao,
			 cd_procedimento,
			 ie_origem_proced,
			 nr_proc_interno,
			 nr_seq_atend_sumario,
			 nr_seq_problema)
		values (
			 atend_sumario_alta_item_seq.nextval,
			 null,
			 sysdate,
			 nm_usuario_p,
			 sysdate,
			 nm_usuario_p,
			 null,
			 'R',
			 null,
			 null,
			 null,
			 null,
			 nr_seq_atend_sumario_p,
			 nr_seq_problema_p);
commit;

end adiciona_problema_sum_alta;
/