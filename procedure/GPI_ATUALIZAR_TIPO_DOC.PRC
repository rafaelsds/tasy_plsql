create or replace
procedure gpi_atualizar_tipo_doc(	nr_sequencia_p		number,
					nr_seq_tipo_doc_p	number,
					nm_usuario_p		Varchar2) is 

begin
	update	gpi_projeto_doc
	set	nr_seq_tipo 	= nr_seq_tipo_doc_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;

commit;

end gpi_atualizar_tipo_doc;
/
