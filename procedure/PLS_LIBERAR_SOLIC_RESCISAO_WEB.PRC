create or replace 
procedure pls_liberar_solic_rescisao_web (	nr_seq_solicitacao_p		number,
						nm_usuario_p			Varchar2,
						cd_estabelecimento_p		number,
						nr_protocolo_atend_p 	out	varchar2) is 

nr_seq_protocolo_atend_w	pls_protocolo_atendimento.nr_sequencia%type;
						
begin	

pls_liberar_solic_rescisao ( nr_seq_solicitacao_p , 'L' ,nm_usuario_p, cd_estabelecimento_p ); 

begin
	select 	nr_seq_protocolo_atend
	into	nr_seq_protocolo_atend_w
	from	pls_solicitacao_rescisao
	where	nr_sequencia	= nr_seq_solicitacao_p;
exception
when others then
	nr_seq_protocolo_atend_w	:= null;
end;
 
if (nr_seq_protocolo_atend_w is not null) then
	select	substr(pls_obter_nr_protocolo_atend(nr_seq_protocolo_atend_w,5),1,40)
	into	nr_protocolo_atend_p
	from 	dual;
end if;
 
end pls_liberar_solic_rescisao_web;
/

