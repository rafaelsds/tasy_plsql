CREATE OR REPLACE 
procedure HAOC_Atendimento_Paciente
		(nr_atendimento_p	in 	number,
		cd_pessoa_fisica_p	in	varchar2,
		dt_entrada_p		in 	date,
		ie_tipo_atendimento_p	in 	varchar2,
		ie_clinica_p		in	number,
		cd_medico_p		in	number,
		cd_convenio_p		in	number,
		cd_categoria_p		in	varchar2,
		cd_usuario_convenio_p	in	varchar2,
		nr_doc_convenio_p	in	varchar2,
		cd_tipo_acomodacao_p	in	number,
		dt_validade_p		in	date,
		ie_carater_atend_p	in	number,
		cd_cid_internacao_p	in	varchar2,
		ie_erro_p		out	number,
		ds_mensagem_p		out	varchar2) is
		

ds_erro_w		varchar2(2000);
qt_reg_w		number(10,0);
nr_seq_interno_w	number(10,0);
cd_procedencia_w	number(10,0);
cd_cid_w		varchar2(10);

begin

ie_erro_p	:= 0;
ds_mensagem_p	:= null;

cd_cid_w	:= replace(cd_cid_internacao_p, '.', '');

if	(ie_erro_p	= 0) then
	begin

	select	count(*)
	into	qt_reg_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_p;

	if	(qt_reg_w	= 0) then
		begin

		insert	into atendimento_paciente
			(NR_ATENDIMENTO            ,                     
			CD_PESSOA_FISICA           ,                   
			CD_ESTABELECIMENTO         ,                    
			CD_PROCEDENCIA             ,                    
			DT_ENTRADA                 ,                    
			IE_TIPO_ATENDIMENTO        ,                    
			DT_ATUALIZACAO             ,                    
			NM_USUARIO                 ,                    
			CD_MEDICO_RESP             ,                    
			IE_CLINICA                 ,                    
			NM_USUARIO_ATEND           ,                    
			ie_permite_visita,
			IE_CARATER_INTER_SUS)
		values
			(NR_ATENDIMENTO_p          ,                     
			CD_PESSOA_FISICA_p         ,                   
			1,                    
			1,                    
			DT_ENTRADA_p,                    
			IE_TIPO_ATENDIMENTO_p,                    
			sysdate,                    
			'INTEGRACAO',                    
			cd_medico_p,                    
			IE_CLINICA_p,                    
			'INTEGRACAO',                    
			'S',
			ie_carater_atend_p);    

		select	atend_categoria_convenio_seq.nextval
		into	nr_seq_interno_w
		from	dual;

		insert	into atend_categoria_convenio
			(NR_SEQ_INTERNO      ,           
			NR_ATENDIMENTO       ,          
			CD_CONVENIO          ,          
			CD_CATEGORIA         ,          
			DT_INICIO_VIGENCIA   ,          
			DT_ATUALIZACAO       ,          
			CD_USUARIO_CONVENIO  ,          
			NM_USUARIO           ,          
			NR_DOC_CONVENIO      ,          
			CD_TIPO_ACOMODACAO   ,          
			DT_VALIDADE_CARTEIRA,
			ie_tipo_guia)
		values
			(nr_seq_interno_w,
			nr_atendimento_p,
			cd_convenio_p,
			cd_categoria_p,
			dt_entrada_p,
			sysdate,
			cd_usuario_convenio_p,
			'INTEGRACAO',
			nr_doc_convenio_p,
			cd_tipo_acomodacao_p,
			dt_validade_p,
			'I');
		exception
			when others then
				ie_erro_p	:= -1;
				ds_erro_w	:= SQLERRM(sqlcode);
				ds_mensagem_p	:= substr('Erro na inclus�o do atendimento ' || nr_atendimento_p || chr(10) || ds_erro_w,1,255);		

		end;
	else
		begin
	
		update	atendimento_paciente
		set	dt_entrada		= dt_entrada_p,
			dt_atualizacao		= sysdate,
			ie_tipo_atendimento	= ie_tipo_atendimento_p,
			ie_clinica		= ie_clinica_p,
			IE_CARATER_INTER_SUS	= ie_carater_atend_p,
			cd_medico_resp		= cd_medico_p
		where	nr_atendimento		= nr_atendimento_p;


		update	atend_categoria_convenio
		set	CD_CONVENIO          	= cd_convenio_p,          
			CD_CATEGORIA         	= cd_categoria_p,          
			DT_INICIO_VIGENCIA   	= dt_entrada_p,          
			DT_ATUALIZACAO       	= sysdate,          
			CD_USUARIO_CONVENIO  	= cd_usuario_convenio_p,          
			NR_DOC_CONVENIO      	= nr_doc_convenio_p,          
			CD_TIPO_ACOMODACAO   	= cd_tipo_acomodacao_p,          
			DT_VALIDADE_CARTEIRA	= dt_validade_p,
			ie_tipo_guia		= 'I'
		where	nr_atendimento		= nr_atendimento_p;
		exception
			when others then
				ie_erro_p	:= -1;
				ds_erro_w	:= SQLERRM(sqlcode);
				ds_mensagem_p	:= substr('Erro na altera��o do atendimento ' || nr_atendimento_p || chr(10) || ds_erro_w,1,255);		

		end;

	end if;

	if	(cd_cid_internacao_p is not null) then
		begin

		select	count(*)
		into	qt_reg_w
		from	diagnostico_medico
		where	nr_atendimento		= nr_atendimento_p
		and	dt_diagnostico		= dt_entrada_p;
	
		if	(qt_reg_w	= 0) then	
			begin
			insert	into diagnostico_medico
				(NR_ATENDIMENTO        ,
				DT_DIAGNOSTICO         ,
				IE_TIPO_DIAGNOSTICO    ,
				CD_MEDICO              ,
				DT_ATUALIZACAO         ,
				NM_USUARIO)
			values
				(nr_atendimento_p,
				dt_entrada_p,
				1,
				cd_medico_p,
				sysdate,
				'INTEGRACAO');
			exception
				when others then
					ie_erro_p	:= -1;
					ds_erro_w	:= SQLERRM(sqlcode);
					ds_mensagem_p	:= substr('Erro na inclus�o do diagnostico ' || nr_atendimento_p || chr(10) || ds_erro_w,1,255);		

			end;
		end if;

		select	count(*)
		into	qt_reg_w
		from	diagnostico_doenca
		where	nr_atendimento		= nr_atendimento_p
		and	dt_diagnostico		= dt_entrada_p;
	
		if	(qt_reg_w	= 0) then	
			begin
			insert	into diagnostico_doenca
				(NR_ATENDIMENTO        ,
				DT_DIAGNOSTICO         ,
				CD_DOENCA              ,
				DT_ATUALIZACAO         ,
				NM_USUARIO             ,
				IE_CLASSIFICACAO_DOENCA)
			values
				(NR_ATENDIMENTO_p      ,
				DT_entrada_p           ,
				cd_cid_w   ,
				sysdate                ,
				'INTEGRACAO'           ,
				'P');
			exception
				when others then
					ie_erro_p	:= -1;
					ds_erro_w	:= SQLERRM(sqlcode);
					ds_mensagem_p	:= substr('Erro na inclus�o do CID ' || nr_atendimento_p || chr(10) || ds_erro_w,1,255);		

			end;
		else
			begin

			update	diagnostico_doenca
			set	CD_DOENCA 	= cd_cid_w,
				dt_atualizacao	= sysdate
			where	nr_atendimento	= nr_atendimento_p
			and	dt_diagnostico	= dt_entrada_p;

			exception
				when others then
					ie_erro_p	:= -1;
					ds_erro_w	:= SQLERRM(sqlcode);
					ds_mensagem_p	:= substr('Erro na altera��o do CID ' || nr_atendimento_p || chr(10) || ds_erro_w,1,255);		

			end;
		end if;
		end;
	end if;

	end;
end if; 

end HAOC_Atendimento_Paciente;
/