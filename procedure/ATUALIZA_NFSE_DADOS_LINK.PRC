create or replace
procedure atualiza_nfse_dados_link(	nr_nfe_imp_p		varchar2,
				cd_verificacao_nfse_p	varchar2,
				dt_emissao_nfe_p	varchar2,
				cd_serie_nf_p		varchar2,
				nr_nota_fiscal_p	varchar2,
				cd_cgc_emitente_p	varchar2,
				dt_emissao_rps_p	varchar2,
				ds_link_rps_p		varchar2) is

dt_emissao_nfe_aux_w	varchar2(50);
dt_emissao_nfe_w	date;
ie_situacao_w		varchar2(1);
dt_emissao_rps_w	date;
cd_interno_w		nota_fiscal.cd_serie_nf%type;

begin


if (dt_emissao_rps_p <> 'X') then
	begin
	dt_emissao_rps_w := replace(dt_emissao_rps_p, 'T', ' ');

	exception when others then
		begin
		dt_emissao_rps_w		:= to_date(replace(dt_emissao_rps_p, 'T', ' '), 'yyyy-mm-dd hh24:mi:ss');
		exception when others then
			begin
			dt_emissao_rps_w	:= to_date(replace(dt_emissao_rps_p, 'T', ' '), 'dd-mm-yyyy hh24:mi:ss');
			exception when others then
			--N�o foi poss�vel converter a data de emiss�o enviada no arquivo:
				Wheb_mensagem_pck.exibir_mensagem_abort(186114,'DT_EMISSAO_NFE=' || dt_emissao_rps_p); 
			end;
		end;
	end;
end if;

cd_interno_w	:= substr(obter_conversao_interna(cd_cgc_emitente_p,'NOTA_FISCAL','CD_SERIE_NF',nvl(trim(cd_serie_nf_p),0)),1,40);

if	(cd_interno_w is null) then
	begin
	
	select	ie_situacao
	into	ie_situacao_w
	from 	nota_fiscal
	where 	nr_nota_fiscal  = nr_nota_fiscal_p
	and	cd_serie_nf	= cd_serie_nf_p
	and 	ie_tipo_nota in ('SE','SD','SF','ST')
	and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'))
	and	((cd_cgc_emitente	= cd_cgc_emitente_p) or (cd_cgc_emitente_p = 'X'))
	and	not ie_situacao in('2','3')
	and (((dt_atualizacao_estoque is not null) and (ie_situacao = '1')) or ((dt_atualizacao_estoque is null) and (ie_situacao = '8')));

	end;
else	
	begin
	
	select	ie_situacao
	into	ie_situacao_w
	from 	nota_fiscal
	where 	nr_nota_fiscal  	= nr_nota_fiscal_p
	and	cd_serie_nf		= trim(cd_interno_w)
	and 	ie_tipo_nota in ('SE','SD','SF','ST')
	and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'))
	and	((cd_cgc_emitente	= cd_cgc_emitente_p) or (cd_cgc_emitente_p = 'X'))
	and	not ie_situacao in('2','3')
	and (((dt_atualizacao_estoque is not null) and (ie_situacao = '1')) or ((dt_atualizacao_estoque is null) and (ie_situacao = '8')));
	
	end;
end if;


if (dt_emissao_nfe_p is not null) then
	begin
	dt_emissao_nfe_aux_w := dt_emissao_nfe_p;
	dt_emissao_nfe_w     := replace(dt_emissao_nfe_aux_w, 'T', ' ');

	exception when others then
		begin
		
		if	(length(trim(dt_emissao_nfe_aux_w)) > 19) then
			begin
			dt_emissao_nfe_aux_w := substr(dt_emissao_nfe_aux_w,1,19);
			end;
		end if;	
		
		dt_emissao_nfe_w		:= to_date(replace(dt_emissao_nfe_aux_w, 'T', ' '), 'yyyy-mm-dd hh24:mi:ss');
		exception when others then
			begin
			dt_emissao_nfe_w	:= to_date(replace(dt_emissao_nfe_aux_w, 'T', ' '), 'dd-mm-yyyy hh24:mi:ss');
			exception when others then
			--N�o foi poss�vel converter a data de emiss�o enviada no arquivo:
				Wheb_mensagem_pck.exibir_mensagem_abort(186114,'DT_EMISSAO_NFE=' || dt_emissao_nfe_aux_w); 
			end;
		end;


	end;
end if;


if	(ie_situacao_w <> '8') then

	if	(cd_interno_w is null) then
		begin
		
		update	nota_fiscal
		set 	nr_nfe_imp		=  nr_nfe_imp_p,
			cd_verificacao_nfse 	= cd_verificacao_nfse_p,
			dt_emissao_nfe		= dt_emissao_nfe_w
		where	nr_nota_fiscal		= nr_nota_fiscal_p
		and	cd_serie_nf		= cd_serie_nf_p
		and 	ie_tipo_nota in ('SE','SD','SF','ST')
		and	((cd_cgc_emitente	= cd_cgc_emitente_p) or (cd_cgc_emitente_p = 'X'))
		and	ie_situacao in ('1')
		and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'));
		
		end;
	else
		begin
		
		update	nota_fiscal
		set 	nr_nfe_imp		=  nr_nfe_imp_p,
			cd_verificacao_nfse 	= cd_verificacao_nfse_p,
			dt_emissao_nfe		= dt_emissao_nfe_w
		where	nr_nota_fiscal		= nr_nota_fiscal_p
		and	cd_serie_nf		= trim(cd_interno_w)
		and 	ie_tipo_nota in ('SE','SD','SF','ST')
		and	((cd_cgc_emitente	= cd_cgc_emitente_p) or (cd_cgc_emitente_p = 'X'))
		and	ie_situacao in ('1')
		and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'));
	
		end;
	end if;
	
else
	
	if	(cd_interno_w is null) then
		begin
		
		update	nota_fiscal
		set 	nr_nfe_imp		= nr_nfe_imp_p,
			cd_verificacao_nfse 	= cd_verificacao_nfse_p,
			dt_emissao_nfe		= dt_emissao_nfe_w,
			dt_atualizacao_estoque	= sysdate,
			ie_situacao		= '1'
		where	nr_nota_fiscal		= nr_nota_fiscal_p
		and	cd_serie_nf		= cd_serie_nf_p
		and 	ie_tipo_nota in ('SE','SD','SF','ST')
		and	((cd_cgc_emitente	= cd_cgc_emitente_p) or (cd_cgc_emitente_p = 'X'))
		and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'));
	
		end;
	else
		begin
		
		update	nota_fiscal
		set 	nr_nfe_imp		= nr_nfe_imp_p,
			cd_verificacao_nfse 	= cd_verificacao_nfse_p,
			dt_emissao_nfe		= dt_emissao_nfe_w,
			dt_atualizacao_estoque	= sysdate,
			ie_situacao		= '1'
		where	nr_nota_fiscal		= nr_nota_fiscal_p
		and	cd_serie_nf		= trim(cd_interno_w)
		and 	ie_tipo_nota in ('SE','SD','SF','ST')
		and	((cd_cgc_emitente	= cd_cgc_emitente_p) or (cd_cgc_emitente_p = 'X'))
		and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'));
		
		end;
	end if;

end if;

if 	(ds_link_rps_p <> 'X') then
	
	update	nota_fiscal
	set 	ds_link_rps		= ds_link_rps_p
	where	nr_nota_fiscal		= nr_nota_fiscal_p
	and	cd_serie_nf		= cd_serie_nf_p
	and 	ie_tipo_nota in ('SE','SD','SF','ST')
	and	((cd_cgc_emitente	= cd_cgc_emitente_p) or (cd_cgc_emitente_p = 'X'))
	and	((to_char(dt_emissao,'dd/mm/yyyy') 	= to_char(dt_emissao_rps_w,'dd/mm/yyyy')) or (dt_emissao_rps_p = 'X'));

end if;

commit;


end atualiza_nfse_dados_link;
/