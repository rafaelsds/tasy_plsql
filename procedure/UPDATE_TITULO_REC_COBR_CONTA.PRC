create or replace
procedure update_titulo_rec_cobr_conta(
		nr_conta_p		varchar2,
		ie_digito_conta_p	varchar2,	
		nr_titulo_p		number,		
		nm_usuario_p		Varchar2) is 

begin

	update   titulo_receber_cobr 
	set      nr_conta          = nr_conta_p,
		 ie_digito_conta   = ie_digito_conta_p,
		 nm_usuario	   = nm_usuario_p
	where    nr_titulo         = nr_titulo_p;

commit;

end update_titulo_rec_cobr_conta;
/