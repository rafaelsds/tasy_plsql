create or replace
procedure proj_vincular_os_projeto
			(	nr_seq_ordem_p		number,
				nr_seq_projeto_p	number,
				ie_acao_p		varchar2,
				nm_usuario_p		Varchar2) is 

nr_seq_ordem_serv_pai_w		man_ordem_servico.nr_seq_ordem_serv_pai%type;
ie_possui_proj_vinc_w		varchar2(1);
begin

select	max(mos.nr_seq_ordem_serv_pai)
into	nr_seq_ordem_serv_pai_w
from	man_ordem_servico mos
where	mos.nr_sequencia = nr_seq_ordem_p;

if	(nr_seq_ordem_serv_pai_w is not null) then
	begin
		wheb_mensagem_pck.exibir_mensagem_abort(1085000);
	end;
end if;

verifica_permite_add_os_proj(nr_seq_projeto_p, nr_seq_ordem_p);

if	(ie_acao_p = 'V') then

	select	decode(count(1),0,'N','S')
	into	ie_possui_proj_vinc_w
	from	proj_ordem_servico
	where	nr_seq_ordem = nr_seq_ordem_p
	and		nr_seq_proj = nr_seq_projeto_p;
	
	if(ie_possui_proj_vinc_w = 'S') then
		begin
			wheb_mensagem_pck.exibir_mensagem_abort(1141357);
		end;
	end if;	
	
	insert into proj_ordem_servico 
		(nr_sequencia,
		nr_seq_proj,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_ordem,
		nr_seq_cron_etapa,
		ie_origem_ordem)
	values(	proj_ordem_servico_seq.nextval,
		nr_seq_projeto_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_ordem_p, 
		null,
		'P');
		
elsif 	(ie_acao_p = 'D') then
	delete	from proj_ordem_servico
	where	nr_seq_ordem 	= nr_seq_ordem_p
	and	nr_seq_proj	= nr_seq_projeto_p;
end if;

commit;

end proj_vincular_os_projeto;
/
