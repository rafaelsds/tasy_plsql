create or replace
function get_user_calendar(	nm_usuario_p			varchar2,
							cd_estabelecimento_p	number default null,
							ie_expected_result_p	varchar2 default 'C') return varchar2 as 

/*ie_expected_result_p accepted values:
P - Primary calendar
S - Secondary calendar
C - Combined calendar identifier, separated by '/'*/							
							
ds_calendar_w		varchar2(255);
ds_secondary_calendar_w	calendar.ds_calendar%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;

begin
select	max(ds_calendar),
		max(ds_secondary_calendar)
into	ds_calendar_w,
		ds_secondary_calendar_w
from	user_locale
where	nm_user = nm_usuario_p;

if	(ds_calendar_w is null) then
	if	(cd_estabelecimento_p is null) then
		select	max(nvl(wheb_usuario_pck.get_cd_estabelecimento,cd_estabelecimento))
		into	cd_estabelecimento_w
		from	usuario
		where	nm_usuario = nm_usuario_p;
	end if;
	
	select	nvl(max(ds_calendar),'iso8601'),
			max(ds_secondary_calendar)
	into	ds_calendar_w,
			ds_secondary_calendar_w
	from	establishment_locale
	where	cd_estabelecimento = nvl(cd_estabelecimento_p,cd_estabelecimento_w);
end if;

if	(nvl(ie_expected_result_p,'C') = 'C') and
	(ds_secondary_calendar_w is not null) then
	ds_calendar_w := ds_calendar_w || '/' || ds_secondary_calendar_w;
elsif (nvl(ie_expected_result_p,'C') = 'S') then
	ds_calendar_w := ds_secondary_calendar_w;
end if;

return ds_calendar_w;

end get_user_calendar;
/
