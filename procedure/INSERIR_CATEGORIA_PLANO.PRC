create or replace
procedure inserir_categoria_plano(	
				cd_convenio_p	number,
				cd_categoria_p	varchar2,
				cd_plano_p	varchar2,
				cd_estab_p	number,
				nm_usuario_p	varchar2) is
				
cd_estabelecimento_w	number(4);	
qt_categoria_plano_w 	number(10);			

begin

cd_estabelecimento_w := cd_estab_p;
if	(cd_estabelecimento_w = 0) then
	cd_estabelecimento_w := null;
end if;

if (cd_categoria_p is not null) and (cd_plano_p is not null) then
	
	select 	count(*)
	into 	qt_categoria_plano_w
	from 	categoria_plano
	where	cd_categoria = cd_categoria_p
	and		cd_plano = cd_plano_p
	and     cd_convenio = cd_convenio_p
	and		nvl(cd_estabelecimento,0) = nvl(cd_estabelecimento_w,0)
	and		ie_situacao = 'A';
	
	if (qt_categoria_plano_w = 0) then

		insert into categoria_plano (
			nr_sequencia,
			cd_convenio,
			cd_categoria,
			cd_plano,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento, 
			ie_situacao,
			dt_inicio_vigencia,
			dt_final_vigencia)
		 values(	categoria_plano_seq.nextval, 
			cd_convenio_p,
			cd_categoria_p,
			cd_plano_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_w,
			'A',
			null,
			null);
	
	end if;
	
end if;

commit;

end inserir_categoria_plano;
/