create or replace
procedure pls_gerar_valor_ato_cooperado
			(	nr_seq_mensalidade_p	number,
				nr_seq_item_p		number) is 

nr_seq_item_w			number(10);
ie_tipo_item_w			varchar2(2);
vl_item_w			number(15,2);
tx_valor_ato_w			number(7,4);
ie_tipo_ato_cooperado_w		varchar2(2);
vl_ato_cooperado_princ_w	number(15,2);
vl_ato_cooperado_aux_w		number(15,2);
vl_ato_nao_cooperado_w		number(15,2);
vl_diferenca_w			number(15,2);
dt_referencia_w			date;
ie_ato_w			varchar2(1);
nr_seq_tipo_lanc_w		number(10);
qt_registros_w			number(10);
nm_usuario_w			varchar2(255);

nr_seq_ato_item_w		number(10);
nr_seq_sca_w			number(10);
vl_sca_embutido_w		number(15,2);
vl_sca_descontar_w		number(15,2);
vl_item_ato_w			number(15,2);

qt_itens_w			number(10);
nr_vetor_w			number(10);
i				number(10);
vl_total_w			number(15,2);
vl_total_ato_princ_w		number(15,2);
vl_total_ato_aux_w		number(15,2);
vl_total_ato_nao_w		number(15,2);
nr_seq_segurado_w		number(15);
vl_total_ato_w			number(15,2);
vl_ato_cooperado_princ_cr_w	number(15,2);
vl_ato_cooperado_aux_cr_w	number(15,2);
vl_ato_nao_cooperado_cr_w	number(15,2);
qt_itens_cr_w			number(10);
tx_ato_cooperado_princ_cr_w	number(7,4);
tx_ato_cooperado_aux_cr_w	number(7,4);
tx_ato_nao_cooperado_cr_w	number(7,4);

type reg_itens_mensalidade is record ( 	vl_item			number(15,2),
					ie_tipo_item		varchar2(10),
					nr_seq_tipo_lanc	number(10),
					qt_itens		number(10),
					vl_ato_cooperado_princ	number(15,2),
					vl_ato_cooperado_aux	number(15,2),
					vl_ato_nao_cooperado	number(15,2),
					tx_ato_cooperado_princ	number(7,4),
					tx_ato_cooperado_aux	number(7,4),
					tx_ato_nao_cooperado	number(7,4));
					
type vetor_itens_mens is table of reg_itens_mensalidade index by binary_integer;
vetor_itens_mens_w	vetor_itens_mens;

Cursor C01 is
	select	count(1),
		sum(d.vl_item),
		d.ie_tipo_item,
		d.nr_seq_tipo_lanc
	from	pls_mensalidade_seg_item	d,
		pls_mensalidade_segurado	c,
		pls_mensalidade			b,
		pls_lote_mensalidade		g
	where	c.nr_sequencia	= d.nr_seq_mensalidade_seg
	and	b.nr_sequencia	= c.nr_seq_mensalidade
	and	g.nr_sequencia	= b.nr_seq_lote
	and	b.nr_sequencia	= nr_seq_mensalidade_p
	group by
		d.ie_tipo_item,
		d.nr_seq_tipo_lanc
	order by
		d.ie_tipo_item;

Cursor C02 is
	select	c.nr_sequencia,
		a.tx_valor_ato,
		a.ie_tipo_ato_cooperado
	from	pls_contab_ato_regra		a,
		pls_contab_ato_cooperado	b,
		pls_contab_ato_item		c
	where	b.nr_sequencia	= a.nr_seq_ato_cooperado
	and	b.nr_sequencia	= c.nr_seq_ato_cooperado
	and	c.ie_tipo_item_mensalidade = ie_tipo_item_w
	and	((c.nr_seq_tipo_lanc = nr_seq_tipo_lanc_w) or (c.nr_seq_tipo_lanc is null))
	and	dt_referencia_w	between	nvl(a.dt_inicio_vigencia, dt_referencia_w) and nvl(a.dt_fim_vigencia, dt_referencia_w);

Cursor C03 is
	select	nr_seq_sca
	from	pls_restricao_ato_item
	where	nr_seq_ato_item	= nr_seq_ato_item_w;
	
Cursor C04 is
	select	c.vl_item,
		c.nr_sequencia,
		b.nr_seq_segurado
	from	pls_mensalidade_seg_item	c,
		pls_mensalidade_segurado	b,
		pls_mensalidade			a
	where	b.nr_sequencia	= c.nr_seq_mensalidade_seg
	and	a.nr_sequencia	= b.nr_seq_mensalidade
	and	a.nr_sequencia	= nr_seq_mensalidade_p
	and	c.ie_tipo_item	= ie_tipo_item_w
	and	((c.nr_seq_tipo_lanc = nr_seq_tipo_lanc_w) or (c.nr_seq_tipo_lanc is null));

begin
select	count(1)
into	qt_registros_w
from	pls_contab_ato_cooperado
where	rownum	<= 1;

if	(qt_registros_w	> 0) then
	nr_vetor_w	:= 0;
	
	select	dt_referencia,
		nm_usuario
	into	dt_referencia_w,
		nm_usuario_w
	from	pls_mensalidade
	where	nr_sequencia	= nr_seq_mensalidade_p;

	/*Colocar no vetor os itens de mensalidades agrupados*/
	open C01;
	loop
	fetch C01 into
		qt_itens_w,
		vl_item_w,
		ie_tipo_item_w,
		nr_seq_tipo_lanc_w;
	exit when C01%notfound;
		begin
		nr_vetor_w	:= vetor_itens_mens_w.count + 1;
		
		vetor_itens_mens_w(nr_vetor_w).qt_itens		:= qt_itens_w;
		vetor_itens_mens_w(nr_vetor_w).vl_item		:= vl_item_w;
		vetor_itens_mens_w(nr_vetor_w).ie_tipo_item	:= ie_tipo_item_w;
		vetor_itens_mens_w(nr_vetor_w).nr_seq_tipo_lanc	:= nr_seq_tipo_lanc_w;
		end;
	end loop;
	close C01;	
	
	i	:= 0;
	
	/*Abrir o vetor dos itens das mensalidades*/
	for i in 1..vetor_itens_mens_w.count loop
		
		vl_ato_cooperado_princ_w	:= 0;
		vl_ato_cooperado_aux_w		:= 0;
		vl_ato_nao_cooperado_w		:= 0;
		
		ie_tipo_item_w			:= vetor_itens_mens_w(i).ie_tipo_item;
		nr_seq_tipo_lanc_w		:= vetor_itens_mens_w(i).nr_seq_tipo_lanc;
		vl_item_ato_w			:= vetor_itens_mens_w(i).vl_item;

		/*Realizar o calculo dos atos cooperados com o total dos itens*/
		if	(ie_tipo_item_w in ('3','6','7','13')) then
			select	nvl(sum(b.vl_ato_cooperado),0),
				nvl(sum(b.vl_ato_auxiliar),0),
				nvl(sum(b.vl_ato_nao_cooperado),0)
			into	vl_ato_cooperado_princ_w,
				vl_ato_cooperado_aux_w,
				vl_ato_nao_cooperado_w
			from	pls_mensalidade_seg_item	b,
				pls_mensalidade_segurado	a
			where	a.nr_seq_mensalidade		= nr_seq_mensalidade_p
			and	b.nr_seq_mensalidade_seg	= a.nr_sequencia
			and	b.ie_tipo_item			= ie_tipo_item_w;
		else
			open C02;
			loop
			fetch C02 into	
				nr_seq_ato_item_w,
				tx_valor_ato_w,
				ie_tipo_ato_cooperado_w;
			exit when C02%notfound;
				begin
				vl_sca_descontar_w	:= 0;
				
				open C03;
				loop
				fetch C03 into	
					nr_seq_sca_w;
				exit when C03%notfound;
					begin
					select	max(vl_parcela)
					into	vl_sca_embutido_w
					from	pls_mensalidade_sca	a,
						pls_sca_vinculo		b
					where	a.nr_seq_vinculo_sca	= b.nr_sequencia
					and	a.nr_seq_item_mens	= nr_seq_item_w
					and	b.nr_seq_plano		= nr_seq_sca_w;
					
					vl_sca_descontar_w	:= vl_sca_descontar_w + nvl(vl_sca_embutido_w,0);
					end;
				end loop;
				close C03;
				
				vl_item_ato_w	:= vetor_itens_mens_w(i).vl_item - vl_sca_descontar_w;
				
				if	(ie_tipo_ato_cooperado_w = '1') then
					vl_ato_cooperado_princ_w			:= ((vl_item_ato_w / 100) * tx_valor_ato_w);
					vetor_itens_mens_w(i).tx_ato_cooperado_princ	:= tx_valor_ato_w;
				elsif	(ie_tipo_ato_cooperado_w = '2') then
					vl_ato_cooperado_aux_w				:= ((vl_item_ato_w / 100) * tx_valor_ato_w);
					vetor_itens_mens_w(i).tx_ato_cooperado_aux	:= tx_valor_ato_w;
				elsif	(ie_tipo_ato_cooperado_w = '3') then
					vl_ato_nao_cooperado_w				:= ((vl_item_ato_w / 100) * tx_valor_ato_w);
					vetor_itens_mens_w(i).tx_ato_nao_cooperado	:= tx_valor_ato_w;
				end if;
				end;
			end loop;
			close C02;
		end if;
		
		vl_total_ato_w	:= (vl_ato_cooperado_princ_w + vl_ato_cooperado_aux_w + vl_ato_nao_cooperado_w);
		
		if	(vl_item_ato_w <> vl_total_ato_w) then
			if	(vl_ato_cooperado_princ_w <> 0) then
				vl_ato_cooperado_princ_w := vl_ato_cooperado_princ_w + (vl_item_ato_w - vl_total_ato_w);
			elsif	(vl_ato_cooperado_aux_w <> 0) then
				vl_ato_cooperado_aux_w := vl_ato_cooperado_aux_w + (vl_item_ato_w - vl_total_ato_w);
			elsif	(vl_ato_nao_cooperado_w <> 0) then								
				vl_ato_nao_cooperado_w := vl_ato_nao_cooperado_w + (vl_item_ato_w - vl_total_ato_w);
			end if;
		end if;
		
		vl_total_w	:= vl_ato_cooperado_princ_w + vl_ato_cooperado_aux_w + vl_ato_nao_cooperado_w;
		
		vetor_itens_mens_w(i).vl_ato_cooperado_princ	:= vl_ato_cooperado_princ_w;
		vetor_itens_mens_w(i).vl_ato_cooperado_aux	:= vl_ato_cooperado_aux_w;
		vetor_itens_mens_w(i).vl_ato_nao_cooperado	:= vl_ato_nao_cooperado_w;
		
		/*Gravar o calculo dos atos cooperados com o total dos itens*/
		insert into pls_mensalidade_ato_coop
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_mensalidade,
			ie_tipo_item,
			nr_seq_tipo_lanc,
			vl_total,
			vl_ato_cooperado,
			vl_ato_auxiliar,
			vl_ato_nao_cooperado)
		values	(pls_mensalidade_ato_coop_seq.nextval,
			sysdate,
			nm_usuario_w,
			sysdate,
			nm_usuario_w,
			nr_seq_mensalidade_p,
			vetor_itens_mens_w(i).ie_tipo_item,
			vetor_itens_mens_w(i).nr_seq_tipo_lanc,
			vl_total_w,
			vl_ato_cooperado_princ_w,
			vl_ato_cooperado_aux_w,
			vl_ato_nao_cooperado_w);
		
		vl_total_ato_princ_w		:= 0;
		vl_total_ato_aux_w		:= 0;
		vl_total_ato_nao_w		:= 0;
		vl_item_ato_w			:= 0;
		
		vl_ato_cooperado_princ_cr_w	:= vetor_itens_mens_w(i).vl_ato_cooperado_princ;
		vl_ato_cooperado_aux_cr_w	:= vetor_itens_mens_w(i).vl_ato_cooperado_aux;
		vl_ato_nao_cooperado_cr_w	:= vetor_itens_mens_w(i).vl_ato_nao_cooperado;
		qt_itens_cr_w			:= vetor_itens_mens_w(i).qt_itens;
		tx_ato_cooperado_princ_cr_w	:= vetor_itens_mens_w(i).tx_ato_cooperado_princ;
		tx_ato_cooperado_aux_cr_w	:= vetor_itens_mens_w(i).tx_ato_cooperado_aux;
		tx_ato_nao_cooperado_cr_w	:= vetor_itens_mens_w(i).tx_ato_nao_cooperado;

		/*aaschlote 17/09/2013 - Na geracao das coparticipacoes, o sistema ja coloca os itens do ato cooperado no item*/
		if	(ie_tipo_item_w not in ('3','6','7','13')) then
			/*Gravar nos itens da mensalidade conforme o item do vetor atual*/
			open C04;
			loop
			fetch C04 into	
				vl_item_w,
				nr_seq_item_w,
				nr_seq_segurado_w;
			exit when C04%notfound;
				begin
				/*Realizar o calculo do ato cooperado principal*/
				if	(vl_ato_cooperado_princ_cr_w <> 0) then
					vl_ato_cooperado_princ_w	:= ((vl_item_w / 100) * tx_ato_cooperado_princ_cr_w);
					vl_total_ato_princ_w		:= vl_total_ato_princ_w + vl_ato_cooperado_princ_w;
				
					/*Caso seja a ultima linha do cursor entao validar o valor do item do ato cooperado ja somado no item da mensalidade*/
					if	(c04%rowCount = qt_itens_cr_w) then
						if	(vl_total_ato_princ_w > vl_ato_cooperado_princ_cr_w) then
							vl_ato_cooperado_princ_w	:= vl_ato_cooperado_princ_w - (vl_ato_cooperado_princ_cr_w - vl_total_ato_princ_w) * -1;
						elsif	(vl_total_ato_princ_w < vl_ato_cooperado_princ_cr_w) then
							vl_ato_cooperado_princ_w	:= vl_ato_cooperado_princ_w + (vl_ato_cooperado_princ_cr_w - vl_total_ato_princ_w);
						end if;
					end if;
				end if;
				
				/*Realizar o calculo do ato cooperado auxiliar*/
				if	(vl_ato_cooperado_aux_cr_w <> 0) then
					vl_ato_cooperado_aux_w		:= ((vl_item_w / 100) * tx_ato_cooperado_aux_cr_w);
					vl_total_ato_aux_w		:= vl_total_ato_aux_w + vl_ato_cooperado_aux_w;
					
					/*Caso seja a ultima linha do cursor entao validar o valor do item do ato cooperado ja somado no item da mensalidade*/
					if	(c04%rowCount = qt_itens_cr_w) then
						if	(vl_total_ato_aux_w	> vl_ato_cooperado_aux_cr_w) then
							vl_ato_cooperado_aux_w	:= vl_ato_cooperado_aux_w - (vl_ato_cooperado_aux_cr_w - vl_total_ato_aux_w) * -1;
						elsif	(vl_total_ato_aux_w	< vl_ato_cooperado_aux_cr_w) then
							vl_ato_cooperado_aux_w	:= vl_ato_cooperado_aux_w + (vl_ato_cooperado_aux_cr_w - vl_total_ato_aux_w);
						end if;
					end if;
				end if;
				
				/*Realizar o calculo do ato nao cooperado cooperado*/
				if	(vl_ato_nao_cooperado_cr_w <> 0) then
					vl_ato_nao_cooperado_w	:= ((vl_item_w / 100) * tx_ato_nao_cooperado_cr_w);
					vl_total_ato_nao_w		:= vl_total_ato_nao_w + vl_ato_nao_cooperado_w;

					/*Caso seja a ultima linha do cursor entao validar o valor do item do ato cooperado ja somado no item da mensalidade*/
					if	(c04%rowCount = qt_itens_cr_w) then
						if	(vl_total_ato_nao_w > vl_ato_nao_cooperado_cr_w) then
							vl_ato_nao_cooperado_w	:= vl_ato_nao_cooperado_w - (vl_ato_nao_cooperado_cr_w - vl_total_ato_nao_w) * -1;
						elsif	(vl_total_ato_nao_w < vl_ato_nao_cooperado_cr_w) then
							vl_ato_nao_cooperado_w	:= vl_ato_nao_cooperado_w + (vl_ato_nao_cooperado_cr_w - vl_total_ato_nao_w);
						end if;
					end if;
				end if;
				
				vl_total_ato_w	:= (vl_ato_cooperado_princ_w + vl_ato_cooperado_aux_w + vl_ato_nao_cooperado_w);
				
				--Verifica se a soma dos atos de item e diferente do valor do item devido a arredondamentos durante a geracao dos valores, caso sim aplica a diferenca  em um dos atos.
				if	(tx_ato_cooperado_princ_cr_w <> 0) then
					if	(vl_item_w <> vl_total_ato_w) then
						vl_ato_cooperado_princ_w := vl_ato_cooperado_princ_w + (vl_item_w - vl_total_ato_w);
					end if;
				elsif	(tx_ato_cooperado_aux_cr_w <> 0) then
					if	(vl_item_w <> vl_total_ato_w) then
						vl_ato_cooperado_aux_w := vl_ato_cooperado_aux_w + (vl_item_w - vl_total_ato_w);
					end if;
				elsif	(tx_ato_nao_cooperado_cr_w <> 0) then								
					if	(vl_item_w <> vl_total_ato_w) then
						vl_ato_nao_cooperado_w := vl_ato_nao_cooperado_w + (vl_item_w - vl_total_ato_w);
					end if;
				end if;
				
				update	pls_mensalidade_seg_item
				set	vl_ato_cooperado	= vl_ato_cooperado_princ_w,
					vl_ato_auxiliar		= vl_ato_cooperado_aux_w,
					vl_ato_nao_cooperado	= vl_ato_nao_cooperado_w
				where	nr_sequencia		= nr_seq_item_w;
				end;
			end loop;
			close C04;
		end if;
	end loop;
end if;

end pls_gerar_valor_ato_cooperado;
/
