CREATE OR REPLACE
PROCEDURE chamada_paciente_pa(	
			nr_atendimento_p	number,
			nr_seq_local_p		number,
			cd_medico_p		varchar2,
			ie_opcao_p		varchar2,
			qt_hora_p		number,
			nm_usuario_p		varchar2,
			nr_seq_depar_to_life_p	number default 0,
			ie_html_p varchar2 default null) is

/*
ie_opcao_p:
CP	= Chamar Paciente
CCP	= Cancelar Chamada do Paciente
CPR	= Chamar Paciente para Reavaliacao
CCPR	= Cancelar Chamada do Paciente em Reavaliacao
CCPC	= Cancelar Chamada do Paciente por nao Comparecimento
CPMP	= Chamar Paciente para Medicacao/Procedimento
CCPMP	= Cancelar Chamada do Paciente para Medicacao/Procedimento
FCPMP	= Finalizar Chamada do Paciente para Medicacao/Procedimento
CPENF	= Chamada Paciente Enfermagem
TL              = Trocar local do paciente sem assumir
*/

ie_cancelar_chamadas_w	varchar2(20);
ie_finaliza_consulta_w	varchar2(5);
qt_chamados_minimos_w	number(10);
qt_chamados_w		number(10);
ie_pac_chamado_w	varchar2(5);
ds_log_w		varchar2(255);
nr_seq_local_w		number(10);
nr_seq_local_orig_w		number(10);
nr_seq_depar_to_life_w	number(10);
qt_agenda_w				number(10);
cd_estabelecimento_w	number(5);

begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

Obter_param_Usuario(935, 87, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_cancelar_chamadas_w);
Obter_param_Usuario(935, 155, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_finaliza_consulta_w);

ds_log_w := 'ie_opcao_p='||ie_opcao_p||';nr_atendimento_p='||nr_atendimento_p||';cd_medico_p='||cd_medico_p;

nr_seq_local_orig_w := nr_seq_local_p;

if (nr_seq_depar_to_life_p > 0) then


	select  max(IE_LOCA_SETOR)
	into	nr_seq_depar_to_life_w
	from    TO_LIFE_CHAMADA
	where   NR_SEQ_DEPARTAMENTO = nr_seq_depar_to_life_p
	and	nvl(ie_situacao,'A') = 'A';


	if ( nvl(nr_seq_depar_to_life_w,0) > 0) then
	
		nr_seq_local_orig_w := nr_seq_depar_to_life_w;
			
	end if;

end if;


if	(ie_opcao_p = 'CPMP') then
	update	atendimento_paciente
	set	dt_chamada_medic	= sysdate,
		nr_seq_local_pa		= nr_seq_local_orig_w,
		cd_medico_chamado	= cd_medico_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_p;
elsif	(ie_opcao_p = 'CCPMP') then
	update	atendimento_paciente
	set	dt_chamada_medic	= null,
		dt_chegada_medic	= null,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_p;
elsif	(ie_opcao_p = 'FCPMP') then
	update	atendimento_paciente
	set	dt_chegada_medic	= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_atendimento		= nr_atendimento_p;
elsif	(ie_opcao_p = 'CP') or (ie_opcao_p = 'CPR')
        and(ie_opcao_p <> 'TL') then
	begin
	if	(ie_opcao_p = 'CPR') then
		update	atendimento_paciente
		set	dt_chamada_reavaliacao	= sysdate,
			dt_inicio_reavaliacao	= null,
			dt_fim_reavaliacao	= null,
			nr_seq_local_pa		= nr_seq_local_orig_w,
			cd_medico_chamado	= cd_medico_p,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_atendimento		= nr_atendimento_p;
	else
		update	atendimento_paciente
		set	dt_chamada_paciente	= sysdate,
			nr_seq_local_pa		= nr_seq_local_orig_w,
			cd_medico_chamado	= cd_medico_p,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			ie_chamado 		= 'S'
		where	nr_atendimento		= nr_atendimento_p;
		
		if	(nvl(ie_html_p,'N') = 'S') then
		
			select	count(*)
			into	qt_agenda_w
			from	agenda_consulta_v2
			where	nr_atendimento = nr_atendimento_p;
			
			if	(qt_agenda_w > 0) then
				update	agenda_Consulta
				set		ie_status_agenda = 'PH'
				where	nr_atendimento = nr_atendimento_p;
			end if;
		
		end if;
		
		/*
		if	(obter_funcao_ativa	= 281) then
			gerar_log_chamada_pa(	nr_atendimento_p,
						nr_seq_local_orig_w,
						nm_usuario_p);
		end if;
		
		*/
		
		

		
		
	end if;
	
	if	(nr_seq_local_orig_w	is not null) then
		Gerar_Transferencia_local_pa(	nr_atendimento_p,
										nr_seq_local_orig_w,
										nm_usuario_p);
	end if;
	
	if	(ie_cancelar_chamadas_w	= 'S') then
		begin
		update	atendimento_paciente
		set	dt_chamada_paciente	= null,
			cd_medico_chamado	= null,
			dt_chamada_reavaliacao  = null,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	cd_medico_chamado	= cd_medico_p
		and	((dt_chamada_paciente is not null) or (dt_chamada_reavaliacao is not null))
		--and	dt_atend_medico	is null
		and	nr_atendimento	<>	nr_atendimento_p
		and	dt_entrada >= (Sysdate - (nvl(qt_hora_p,24) / 24))
		and	dt_alta is null;
		end;
	end if;

  if	(ie_cancelar_chamadas_w	= 'G') then
		begin
		update	atendimento_paciente
		set	dt_chamada_paciente	= null,
			nr_seq_local_pa		= null,
			cd_medico_chamado	= null,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			ie_chamado		= null
		where	cd_medico_chamado	= cd_medico_p
		and	((dt_chamada_paciente is not null) or (dt_chamada_reavaliacao is not null))
		and	nr_atendimento	<>	nr_atendimento_p
		and	dt_entrada >= (Sysdate - (nvl(qt_hora_p,24) / 24))
		and	dt_alta is null;
		end;
	end if;
	
	end;
elsif	(ie_opcao_p = 'CCP') or (ie_opcao_p = 'CCPR') 
	or (ie_opcao_p = 'CCPC') then  
	begin
		
	if	(ie_opcao_p = 'CCPR') then
		update	atendimento_paciente
		set	dt_chamada_reavaliacao	= null,
			nr_seq_local_pa		= null,
			cd_medico_chamado	= null,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			ie_chamado		= null
		where	nr_atendimento		= nr_atendimento_p;
	elsif	(ie_opcao_p = 'CCPC') then
	
	
		
		update	atendimento_paciente
		set	dt_chamada_reavaliacao	= null,
			dt_chamada_paciente	= null,
			dt_chamada_enfermagem	= null,
			nr_seq_local_pa		= null,
			cd_medico_chamado	= null,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			ie_chamado		= 'C'
		where	nr_atendimento		= nr_atendimento_p;
		
		
		
		
	else
		if (ie_opcao_p = 'CCP') then
		
		select	nr_seq_local_pa
		into	nr_seq_local_w
		from	atendimento_paciente
		where	nr_atendimento = nr_atendimento_p;
		
		update	historico_localizacao_pa
		set	dt_saida_local = sysdate
		where	nr_seq_local_destino_pa	= nr_seq_local_w
		and	nr_atendimento		= nr_atendimento_p
		and	dt_saida_local is null;
		
		end if;
		
		update	atendimento_paciente
		set	dt_chamada_paciente	= null,
			nr_seq_local_pa		= null,
			cd_medico_chamado	= null,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			ie_chamado		= null
		where	nr_atendimento		= nr_atendimento_p;
		
		if	(nvl(ie_html_p,'N') = 'S') then
		
			select	count(*)
			into	qt_agenda_w
			from	agenda_consulta_v2
			where	nr_atendimento = nr_atendimento_p;
			
			if	(qt_agenda_w > 0) then
				update	agenda_Consulta
				set		ie_status_agenda = 'AT'
				where	nr_atendimento = nr_atendimento_p;
			end if;
			
		end if;
		
	end if;
	
	
	if	(ie_opcao_p	= 'CCPC') or
		(ie_opcao_p	= 'CCP') then
	
		Obter_param_Usuario(935, 136, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, qt_chamados_minimos_w);
		select	count(*)
		into	qt_chamados_w
		from	log_chamada_pa
		where	nr_atendimento	= nr_atendimento_p
		and	nvl(IE_CHAMADO,'N')	= 'N';
		if	((qt_chamados_minimos_w	= 0) or
			(qt_chamados_w	>= qt_chamados_minimos_w)) then
			update	atendimento_paciente
			set	ie_chamado		= 'X'
			where	nr_atendimento		= nr_atendimento_p;
		end if;
			
	end if;
	
	end;
elsif	(ie_opcao_p = 'CPENF') then
	begin
	update	atendimento_paciente
	set	dt_chamada_paciente = sysdate
	where	nr_atendimento = nr_atendimento_p;
	
	select	nr_seq_local_pa
	into	nr_seq_local_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	gerar_log_chamada_pa(	nr_atendimento_p,
				nr_seq_local_w,
				nm_usuario_p);
	end;
elsif	(ie_opcao_p = 'TL') then
	begin                
	update	atendimento_paciente
	set	nr_seq_local_pa = nr_seq_local_orig_w
	where	nr_atendimento = nr_atendimento_p;
	
	if	(nr_seq_local_orig_w	is not null) then
	
	Gerar_Transferencia_local_pa(	nr_atendimento_p,
					nr_seq_local_orig_w,
					nm_usuario_p	);
	end if;        
	
	select	nr_seq_local_pa
	into	nr_seq_local_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;
	
	gerar_log_chamada_pa(	nr_atendimento_p,
				nr_seq_local_w,
				nm_usuario_p);
	end;        
end if;

select	decode(max(dt_chamada_paciente),null,'N','S')
into	ie_pac_chamado_w
from	atendimento_paciente
where	nr_seq_local_pa		= nr_seq_local_orig_w
and	cd_medico_chamado	= cd_medico_p
and	nr_atendimento		= nr_atendimento_p;

if	(ie_finaliza_consulta_w = 'F') and
	(ie_pac_chamado_w = 'S') then
	gerar_fim_consulta_medico(cd_medico_p,qt_hora_p);
end if;

commit;

end Chamada_paciente_pa;
/