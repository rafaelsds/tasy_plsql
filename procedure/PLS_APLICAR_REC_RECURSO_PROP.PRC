create or replace 
procedure pls_aplicar_rec_recurso_prop(	nr_seq_lote_p      		Number,
								ie_apenas_fins_contabeis_p	varchar2,
								nm_usuario_p      		Varchar2,
								cd_estabelecimento_p    	Number) is

vl_item_w			Number(15,2)  := 0;
vl_dif_partic_w         	Number(15,2);
vl_tot_partic_w         	Number(15,2);
aux_w              		Number(10);
qt_conta_w          		Number(10);
ie_atualizou_data_lote_w    	boolean := false;
vl_lib_anterior_w        	pls_conta_proc.vl_liberado%type  := 0;
dt_mes_competencia_w      	pls_lote_recalculo.dt_mes_competencia%type;
ie_tipo_regra_w          	pls_regra_lote_recalculo.ie_tipo_regra%type;
ie_recalculo_item_w       	pls_lote_recalculo.ie_recalcular_item%type;
nr_seq_prestador_pgto_w     	pls_criterio_recalculo.nr_seq_prestador_pag%type;
vl_calculado_w          	pls_conta_proc.vl_procedimento%type;
vl_liberado_w          		pls_conta_medica_resumo.vl_lib_original%type;
vl_calc_hi_util_w        	pls_conta_proc.vl_calc_hi_util%type;
vl_calc_co_util_w        	pls_conta_proc.vl_calc_co_util%type;
vl_calc_mat_util_w        	pls_conta_proc.vl_calc_mat_util%type;
vl_liberado_hi_w        	pls_conta_proc.vl_liberado_hi%type;
vl_liberado_co_w        	pls_conta_proc.vl_liberado_co%type;
vl_liberado_material_w      	pls_conta_proc.vl_liberado_material%type;
qt_apresentado_w        	pls_conta_proc.qt_procedimento_imp%type;
vl_pagamento_w          	pls_conta_medica_resumo.vl_lib_original%type;
vl_total_protocolo_w      	pls_conta.vl_total%type;
nr_seq_protocolo_anterior_w 	pls_protocolo_conta.nr_sequencia%type := -1; --Apenas para fazer um controle de loop
ie_tipo_protocolo_w    		pls_protocolo_conta.ie_tipo_protocolo%type;
tot_registros_w			pls_integer;
vl_liberado_ant_res_w		pls_conta_medica_resumo.vl_liberado_ant%type;
vl_liberado_ant_dif_w		pls_conta_medica_resumo.vl_liberado_ant%type;
qt_contas_abertas_w		pls_integer;
qt_crit_prest_pagto_w		pls_integer := 0;
vl_item_recalculo_w		pls_item_recalculo.vl_item%type;
qt_item_dif_prest_w		pls_integer := 0;
qt_item_prest_w			pls_integer := 0;
nr_seq_restante_w		pls_conta_medica_resumo.nr_sequencia%type;
sum_vl_liberado_ant_w		pls_conta_medica_resumo.vl_liberado_ant%type;
sum_vl_liberado_w		pls_conta_medica_resumo.vl_liberado%type;
sum_vl_lib_original_w		pls_conta_medica_resumo.vl_lib_original%type;
sum_vl_unitario_w		pls_conta_medica_resumo.vl_unitario%type;
sum_vl_apres_ind_w		pls_conta_medica_resumo.vl_apres_ind%type;
nr_seq_prestador_pagamento_w	pls_criterio_recalculo.nr_seq_prestador_pag%type;
nr_seq_mat_regra_w		pls_conta_mat_regra.nr_sequencia%type;
nr_seq_proc_regra_w		pls_conta_proc_regra.nr_sequencia%type;
nr_iteracao_cursor_w		pls_integer := 0;

Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_conta,
		a.cd_pessoa_fisica,
		a.nr_seq_prestador,
		a.nr_seq_protocolo,
		count(1) over() total_registros
	from    pls_conta_recalculo  a
	where   a.nr_seq_lote  = nr_seq_lote_p
	order by   nr_seq_protocolo;

Cursor C02 (nr_seq_conta_recalculo_pc  pls_item_recalculo.nr_seq_conta%type) is
	select 	a.nr_seq_procedimento,
		a.nr_seq_material,
		a.vl_item,
		a.vl_item_atual vl_atual,
		a.nr_seq_regra_preco,
		a.nr_regra_recalculo,
		a.nr_seq_conta_resumo,
		a.nr_sequencia
	from   	pls_item_recalculo  a
	where  	a.nr_seq_conta    = nr_seq_conta_recalculo_pc;

Cursor C03 (nr_seq_protocolo_pc pls_protocolo_conta.nr_sequencia%type) is
	select 	nr_sequencia
	from   	pls_conta
	where  	nr_seq_protocolo = nr_seq_protocolo_pc;

procedure atualiza_resumo_rateio_prop(	nr_seq_conta_proc_p	pls_conta_proc.nr_sequencia%type,
					vl_item_p			pls_item_recalculo.vl_item%type,
					vl_lib_anterior_p	pls_conta_proc.vl_liberado%type,
					nr_seq_regra_p pls_item_recalculo.nr_regra_recalculo%type) is

vl_ajuste_w		pls_conta_proc.vl_liberado%type;
nr_seq_prestador_pag_w	pls_criterio_recalculo.nr_seq_prestador_pag%type;

begin

	select 	nr_seq_prestador_pag
	into	nr_seq_prestador_pag_w
	from 	pls_criterio_recalculo a
	where	nr_sequencia	= nr_seq_regra_p;

	--vl_lib_original	= vl_lib_original - (vl_lib_anterior_p - vl_item_p) --atualiza com a diferen�a do liberado antigo com o atual
	update	pls_conta_medica_resumo
	set 	vl_liberado_ant	= vl_lib_anterior_p,
		vl_liberado 	= vl_item_p,
		vl_lib_original	= vl_item_p --atualiza com a diferen�a do liberado antigo com o atual
	where	nr_seq_conta_proc = nr_seq_conta_proc_p
	and	nr_seq_prestador_pgto = nr_seq_prestador_pag_w
	and	ie_situacao = 'A';

	update  pls_conta_medica_resumo
	set	vl_unitario 		= dividir(vl_liberado, qt_item),
		vl_apres_ind 		= vl_lib_original - vl_glosa
	where	nr_seq_conta_proc 	= nr_seq_conta_proc_p
	and	nr_seq_prestador_pgto 	= nr_seq_prestador_pag_w
	and	ie_situacao = 'A';

end;

begin

ie_tipo_regra_w := 1;
select  dt_mes_competencia,
	nvl(ie_recalcular_item,'N')
into    dt_mes_competencia_w,
	ie_recalculo_item_w
from    pls_lote_recalculo
where   nr_sequencia = nr_seq_lote_p;

for r_c01_w in C01() loop
	begin --*1

	nr_iteracao_cursor_w := nr_iteracao_cursor_w + 1;
	qt_conta_w  := 0;
	select	count(1)
	into  	aux_w
	from  	sip_nv_dados a
	where 	a.ie_conta_enviada_ans = 'S'
	and  	a.nr_seq_conta = r_c01_w.nr_seq_conta
	and  	exists(	select  1
			from  	pls_lote_sip b
			where  	b.nr_sequencia = a.nr_seq_lote_sip
			and  	b.dt_envio is not null);

	if  (aux_w > 0) then
		update	pls_lote_recalculo
		set  	dt_aplicacao  	= null,
			nm_usuario  	= nm_usuario_p,
			dt_atualizacao  = sysdate
		where   nr_sequencia  	= nr_seq_lote_p;

		wheb_mensagem_pck.exibir_mensagem_abort(338028);
	end if;

	if   (not ie_atualizou_data_lote_w) then
		update  pls_lote_recalculo
		set  	dt_aplicacao   	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao  = sysdate,
			ie_somente_contabil = nvl(ie_apenas_fins_contabeis_p,'N')
		where   nr_sequencia   	= nr_seq_lote_p;
		ie_atualizou_data_lote_w := true;
	end if;
	
	if (nvl(ie_apenas_fins_contabeis_p,'N') = 'S') then
		for r_c02_w in C02(r_c01_w.nr_sequencia) loop
			if  (nvl(r_c02_w.vl_item,0) > 0) then
				if  (r_c02_w.nr_seq_procedimento is not null) then
				
					select	max(nr_sequencia)
					into	nr_seq_proc_regra_w
					from	pls_conta_proc_regra
					where	nr_sequencia = r_c02_w.nr_seq_procedimento;
					
					if	(nr_seq_proc_regra_w is null) then
					
						insert into pls_conta_proc_regra 
							( nr_sequencia, dt_atualizacao, nm_usuario, vl_recalculo)
						 values 
							( r_c02_w.nr_seq_procedimento, sysdate, nm_usuario_p, r_c02_w.vl_item);
					else
						update	pls_conta_proc_regra
						set	vl_recalculo =  r_c02_w.vl_item
						where	nr_sequencia = nr_seq_proc_regra_w;
					end if;
					
				elsif  (nvl(r_c02_w.nr_seq_material,0) > 0) then
				
					select	max(nr_sequencia)
					into	nr_seq_mat_regra_w
					from	pls_conta_mat_regra
					where	nr_sequencia = r_c02_w.nr_seq_material;
					
					if	(nr_seq_mat_regra_w is null) then
					
						insert into pls_conta_mat_regra 
							( nr_sequencia, dt_atualizacao, nm_usuario, vl_recalculo)
						 values 
							( r_c02_w.nr_seq_material, sysdate, nm_usuario_p, r_c02_w.vl_item);
					else
						update	pls_conta_mat_regra
						set	vl_recalculo =  r_c02_w.vl_item
						where	nr_sequencia = nr_seq_mat_regra_w;
					end if;
				end if;
			end if;
		end loop;
	end if;

	--Aplicar ajuste de recurso pr�prio apenas para fins cont�beis(Aplicar ajuste de recurso pr�prio apenas para fins cont�beis - OS 928106)
	if (ie_apenas_fins_contabeis_p = 'N') then
		for r_c02_w in C02(r_c01_w.nr_sequencia) loop
			begin
			nr_seq_restante_w := null;
			if  (nvl(r_c02_w.vl_item,0) > 0) then
				if  (r_c02_w.nr_seq_procedimento is not null) then
					select  vl_liberado,
						vl_procedimento,
						qt_procedimento_imp
					into  	vl_lib_anterior_w,
						vl_calculado_w,
						qt_apresentado_w
					from  	pls_conta_proc
					where  	nr_sequencia  = r_c02_w.nr_seq_procedimento;

					if	(vl_lib_anterior_w <> 0) then
						select count(1)
						into	qt_crit_prest_pagto_w
						from	pls_criterio_recalculo
						where	nr_sequencia = r_c02_w.nr_regra_recalculo
						and	nr_seq_prestador_pag is not null;


						/*Para o recurso pr�prio(quando tiver regras por prestador de pgto),  est� sendo dado um tratamento
						diferente para ajuste dos valores, pois dependendo dos casos, nem todas as linhas do resumo s�o
						atualizadas, devido a isso, a atualiza��o dos valores dos procedimentos e participantes, tamb�m
						precisam ser atualizadas de maneira diferente, pois sen�o vai atualizar o vl_liberado do procedimento
						com o vl_item da linha do rec�lculo, que nesse caso, ser� apenas o valor do prestador de pagto espec�fico
						*/
						vl_item_recalculo_w := r_c02_w.vl_item;
						if (ie_tipo_regra_w = 1 and qt_crit_prest_pagto_w > 0) then
							vl_item_recalculo_w := vl_lib_anterior_w - (r_c02_w.vl_atual - r_c02_w.vl_item);

						end if;

						vl_liberado_hi_w    	:= vl_item_recalculo_w  * dividir_sem_round(vl_calc_hi_util_w,vl_calculado_w);
						vl_liberado_co_w    	:= vl_item_recalculo_w * dividir_sem_round(vl_calc_co_util_w,vl_calculado_w);
						vl_liberado_material_w  := vl_item_recalculo_w  * dividir_sem_round(vl_calc_mat_util_w,vl_calculado_w);

						update  pls_conta_proc
						set  	vl_liberado		= vl_item_recalculo_w,
							vl_unitario     	= dividir(vl_item_recalculo_w,qt_procedimento),
							vl_pag_medico_conta 	= 0,
							vl_total_partic 	= 0,
							vl_prestador    	= vl_item_recalculo_w,
							vl_liberado_hi  	= vl_liberado_hi_w,
							vl_liberado_co  	= vl_liberado_co_w,
							vl_liberado_material	= vl_liberado_material_w
						where  	nr_sequencia 		= r_c02_w.nr_seq_procedimento;

						update  pls_proc_participante
						set  	vl_participante = dividir((vl_item_recalculo_w * vl_calculado),vl_calculado_w),
							qt_liberada     = qt_apresentado_w,
							vl_glosa      	= 0,
							ie_status_pagamento 	= 'L',
							ie_status      		= 'L'
						where  nr_seq_conta_proc    = r_c02_w.nr_seq_procedimento
						and  ((ie_status is null)   or (ie_status != 'C'))
						and  ((ie_gerada_cta_honorario is null) or (ie_gerada_cta_honorario <> 'S'));

						select  nvl(sum(vl_participante),0)
						into  	vl_tot_partic_w
						from  	pls_proc_participante
						where  	nr_seq_conta_proc    = r_c02_w.nr_seq_procedimento
						and  ((	ie_status is null)   or (ie_status != 'C'))
						and  ((	ie_gerada_cta_honorario is null) or (ie_gerada_cta_honorario <> 'S'));

						if  (	vl_tot_partic_w > r_c02_w.vl_item ) then
							vl_dif_partic_w  := vl_tot_partic_w - r_c02_w.vl_item;

							update  pls_proc_participante  a
							set  	a.vl_participante  	= a.vl_participante - vl_dif_partic_w
							where  	a.nr_sequencia  	= (  select  max(x.nr_sequencia)
							from  	pls_proc_participante  x
							where 	x.nr_seq_conta_proc    = r_c02_w.nr_seq_procedimento
							and  	((x.ie_status is null) or (x.ie_status != 'C'))
							and  	((x.ie_gerada_cta_honorario is null) or (x.ie_gerada_cta_honorario <> 'S'))
							and  	x.vl_participante >= vl_dif_partic_w);

						end if;

						insert into pls_conta_proc_hist
								   (	nr_sequencia, nr_seq_proc, nr_seq_lote,
									dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
									nm_usuario_nrec, vl_liberado, vl_lib_anterior)
							values	   ( 	pls_conta_proc_hist_seq.nextval, r_c02_w.nr_seq_procedimento, nr_seq_lote_p,
									sysdate, nm_usuario_p, sysdate,
									nm_usuario_p, r_c02_w.vl_item, vl_lib_anterior_w);

						/*Se tiver crit�rios por prest_pgto, ent�o deve atualizar o resumo a n�vel
						de registro na pls_conta_medica_resumo, caso contr�rio, atualiza todos os
						registros da pls_conta_medica_resumo daquele procedimento
						*/
						if (  qt_crit_prest_pagto_w > 0) then

							atualiza_resumo_rateio_prop(r_c02_w.nr_seq_procedimento, r_c02_w.vl_item, r_c02_w.vl_atual, r_c02_w.nr_regra_recalculo);
						else
							pls_atualiza_conta_resumo_item(r_c02_w.nr_seq_procedimento, 'P', nm_usuario_p,'N');

							select	count(1) tot_registros
							into	tot_registros_w
							from	pls_conta_medica_resumo	a
							where	a.nr_seq_conta		= r_c01_w.nr_seq_conta
							and	a.nr_seq_conta_proc	= r_c02_w.nr_seq_procedimento
							and	a.ie_tipo_item		!= 'I'
							and	a.ie_situacao 		= 'A';

							--Divide proporcionalmente o valor apresentado entre os registros  na pls_conta_medica_resumo de cada item da conta
							if	(tot_registros_w > 1) then
								update	pls_conta_medica_resumo
								set	vl_liberado_ant	 	= vl_lib_anterior_w * (dividir_sem_round(vl_lib_original, vl_item_w))
								where	nr_seq_conta		= r_c01_w.nr_seq_conta
								and	nr_seq_conta_proc	= r_c02_w.nr_seq_procedimento
								and	ie_tipo_item		!= 'I'
								and	ie_situacao 		= 'A';

								select	sum(vl_liberado_ant)
								into	vl_liberado_ant_res_w
								from	pls_conta_medica_resumo
								where	nr_seq_conta		= r_c01_w.nr_seq_conta
								and	nr_seq_conta_proc	= r_c02_w.nr_seq_procedimento
								and	ie_tipo_item		!= 'I'
								and	ie_situacao 		= 'A';

								--Diferen�a entre a soma dos valores apresentados dos registros na pls_conta_medica_resumo do procedimento em quest�o, em rela��o ao valor apresentado para o procedimento.
								vl_liberado_ant_dif_w := vl_liberado_ant_res_w - vl_lib_anterior_w;

								--Na divis�o do valor apresentado, pode ocorrer uma diverg�ncia de valor entre a soma dos registros e o total apresentado do item,
								--ent�o aqui � feito um ajuste e o ajuste apenas ocorre em um �nico item cujo valor apresentado j� seja maior que o valor de ajuste(evita ficar negativo, no caso de ajuste com valor negativo)
								if	(vl_liberado_ant_dif_w <> 0) then
									update	pls_conta_medica_resumo
									set	vl_liberado_ant = vl_liberado_ant + vl_liberado_ant_dif_w
									where	nr_sequencia   =  (	select	max(nr_sequencia)
													from 	pls_conta_medica_resumo
													where	nr_seq_conta		= r_c01_w.nr_seq_conta
													and	nr_seq_conta_proc	= r_c02_w.nr_seq_procedimento
													and	vl_liberado_ant		> vl_liberado_ant_dif_w
													and	ie_tipo_item		!= 'I'
													and	ie_situacao 		= 'A')
									and	nr_seq_conta  	= r_c01_w.nr_seq_conta;
								end if;
							else
								update	pls_conta_medica_resumo
								set	vl_liberado_ant	 	= vl_lib_anterior_w
								where	nr_seq_conta		= r_c01_w.nr_seq_conta
								and	nr_seq_conta_proc	= r_c02_w.nr_seq_procedimento
								and	ie_tipo_item		!= 'I'
								and	ie_situacao 		= 'A';
							end if;
						end if;
					end if;
				elsif  (nvl(r_c02_w.nr_seq_material,0) > 0) then

					if  (ie_tipo_regra_w <> 4) then

						select	vl_liberado
						into	vl_lib_anterior_w
						from	pls_conta_mat
						where	nr_sequencia = r_c02_w.nr_seq_material;

						if	(vl_lib_anterior_w <> 0) then
							nr_seq_prestador_pgto_w := null;
							vl_item_w := r_c02_w.vl_item;
							if  (ie_tipo_regra_w = 3) then
								begin
									select  nr_seq_prestador_pag
									into  nr_seq_prestador_pgto_w
									from  pls_criterio_recalculo
									where  nr_sequencia  = r_c02_w.nr_regra_recalculo;
								exception
								when others then
									nr_seq_prestador_pgto_w := null;
								end;

								if  ( nr_seq_prestador_pgto_w is not null ) then
									select  sum(vl_lib_original)
									into  vl_liberado_w
									from  pls_conta_medica_resumo
									where  nr_seq_conta_mat  	= r_c02_w.nr_seq_material
									and  nr_seq_conta    		= r_c01_w.nr_seq_conta
									and  nr_seq_prestador_pgto   	!= nr_seq_prestador_pgto_w
									and  ie_tipo_item     	!= 'I'
									and  vl_lib_original  	> 0
									and  ((ie_situacao    	!= 'I') or (ie_situacao is null));
									vl_item_w := r_c02_w.vl_item + nvl(vl_liberado_w,0);
								end if;

							end if;

							update	pls_conta_mat
							set  	vl_liberado      = vl_item_w,
								nr_seq_regra_recalculo     = null,
								nr_seq_regra_preco_recalc  = null,
								vl_unitario     = dividir(vl_item_w,qt_material)
							where  	nr_sequencia    = r_c02_w.nr_seq_material;
						end if;

					end if;

					if (vl_lib_anterior_w <> 0 )then
						insert into pls_conta_mat_hist
						      ( nr_sequencia, nr_seq_material, nr_seq_lote,
							dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
							nm_usuario_nrec, vl_liberado, vl_lib_anterior)
						values( pls_conta_mat_hist_seq.nextval, r_c02_w.nr_seq_material, nr_seq_lote_p,
							sysdate, nm_usuario_p, sysdate, nm_usuario_p, vl_item_w, vl_lib_anterior_w);

						if  	(ie_tipo_regra_w <> 4) or (ie_recalculo_item_w = 'S') then
							pls_atualiza_conta_resumo_item(r_c02_w.nr_seq_material, 'M', nm_usuario_p,'N');

							update	pls_conta_medica_resumo
							set	vl_liberado_ant	 	= vl_lib_anterior_w
							where	nr_seq_conta		= r_c01_w.nr_seq_conta
							and	nr_seq_conta_mat	= r_c02_w.nr_seq_material
							and	ie_tipo_item		!= 'I'
							and	ie_situacao 		= 'A';

						end if;
					end if;
				end if;

			end if;
			--Inicio OS 1126582
			if (r_c02_w.nr_seq_conta_resumo <> 0) then

				--Se for raterio proporcioal do recurso pr�prio e regra por prestador de pagto, ent�o caso existir
				--mais de uma linha para o mesmo nr_seq_conta_proc e prestador_pgto(prestador e m�dico por exemplo)
				--ent�o mant�m apenas a linha do prestador e acumula nessa os valores. 	 Conf OS1284217
				if ( ie_tipo_regra_w = 1 and qt_crit_prest_pagto_w > 0) then

					select 	nr_seq_prestador_pag
					into	nr_seq_prestador_pagamento_w
					from 	pls_criterio_recalculo a
					where	nr_sequencia	= r_c02_w.nr_regra_recalculo;

					select 	count(1)
					into	qt_item_prest_w
					from 	pls_conta_medica_resumo
					where 	nr_seq_conta = r_c01_w.nr_seq_conta
					and 	nr_seq_conta_proc = r_c02_w.nr_seq_procedimento
					and	nr_seq_prestador_pgto = nr_seq_prestador_pagamento_w
					and 	ie_situacao = 'A'
					and 	ie_tipo_item = 'P';

					select 	count(1)
					into	qt_item_dif_prest_w
					from 	pls_conta_medica_resumo
					where 	nr_seq_conta = r_c01_w.nr_seq_conta
					and 	nr_seq_conta_proc = r_c02_w.nr_seq_procedimento
					and	nr_seq_prestador_pgto = nr_seq_prestador_pagamento_w
					and 	ie_situacao = 'A'
					and 	ie_tipo_item <> 'P';

					if	(qt_item_prest_w = 1 and qt_item_dif_prest_w > 0) then

						delete from pls_conta_medica_resumo
						where	nr_seq_conta_proc = r_c02_w.nr_seq_procedimento
						and	nr_seq_conta	 = r_c01_w.nr_seq_conta
						and	nr_seq_prestador_pgto = nr_seq_prestador_pagamento_w
						and	ie_situacao = 'A'
						and	ie_tipo_item <> 'P';

						select	max(nr_sequencia)
						into	nr_seq_restante_w
						from	pls_conta_medica_resumo
						where	nr_seq_conta_proc 	= r_c02_w.nr_seq_procedimento
						and	nr_seq_conta	 = r_c01_w.nr_seq_conta
						and	nr_seq_prestador_pgto 	= nr_seq_prestador_pagamento_w
						and	ie_situacao = 'A';

						--Como ocorreu exclus�o de algum registro de resumo do mesma conta proc e prestador_pagto, tem que ver se esse estava apontado no
						--item de recalculo e caso sim, ent�o substitui pelo resgistro que sobrou(Importante para quando desfazer a aplica��o dos valores)
						if	(r_c02_w.nr_seq_conta_resumo <> nr_seq_restante_w) then

							update 	pls_item_recalculo
							set 	nr_seq_conta_resumo = nr_seq_restante_w
							where 	nr_sequencia = r_c02_w.nr_sequencia;

						end if;
					elsif	(qt_item_prest_w = 0 and qt_item_dif_prest_w > 1) then
						delete  from pls_conta_medica_resumo
						where	nr_seq_conta_proc = r_c02_w.nr_seq_procedimento
						and	nr_seq_conta	 = r_c01_w.nr_seq_conta
						and	nr_seq_prestador_pgto = nr_seq_prestador_pagamento_w
						and	ie_situacao = 'A'
						and	ie_tipo_item <> 'P'
						and	nr_sequencia  = (select min(nr_sequencia)
									 from 	pls_conta_medica_resumo
									where	nr_seq_conta_proc 	= r_c02_w.nr_seq_procedimento
									and	nr_seq_conta	 	= r_c01_w.nr_seq_conta
									and	nr_seq_prestador_pgto 	= nr_seq_prestador_pagamento_w
									and	ie_situacao = 'A'
									and	ie_tipo_item <> 'P');

						select	max(nr_sequencia)
						into	nr_seq_restante_w
						from	pls_conta_medica_resumo
						where	nr_seq_conta_proc 	= r_c02_w.nr_seq_procedimento
						and	nr_seq_conta	 = r_c01_w.nr_seq_conta
						and	nr_seq_prestador_pgto 	= nr_seq_prestador_pagamento_w
						and	ie_situacao = 'A';

						--Como ocorreu exclus�o de algum registro de resumo do mesma conta proc e prestador_pagto, tem que ver se esse estava apontado no
						--item de recalculo e caso sim, ent�o substitui pelo resgistro que sobrou(Importante para quando desfazer a aplica��o dos valores)
						if	(r_c02_w.nr_seq_conta_resumo <> nr_seq_restante_w) then

							update 	pls_item_recalculo
							set 	nr_seq_conta_resumo = nr_seq_restante_w
							where 	nr_sequencia = r_c02_w.nr_sequencia;

						end if;
					end if;


				end if;

				if	(nr_seq_restante_w is null) then
					select 	count(1)
					into	aux_w
					from	pls_conta_medica_resumo
					where	nr_sequencia = r_c02_w.nr_seq_conta_resumo
					and	nr_seq_conta = r_c01_w.nr_seq_conta;

					if (aux_w = 0) then

						select 	max(nr_sequencia)
						into 	aux_w
						from
						(	select 	nr_sequencia
							from 	pls_conta_medica_resumo
							where 	nr_seq_conta = r_c01_w.nr_seq_conta
							and 	nr_seq_conta_proc = r_c02_w.nr_seq_procedimento
							and 	ie_situacao = 'A'
							and 	ie_tipo_item != 'I'
							union all
							select 	nr_sequencia
							from 	pls_conta_medica_resumo
							where 	nr_seq_conta = r_c01_w.nr_seq_conta
							and 	nr_seq_conta_mat = r_c02_w.nr_seq_material
							and 	ie_situacao = 'A'
							and 	ie_tipo_item != 'I'
						);

						update 	pls_item_recalculo
						set 	nr_seq_conta_resumo = aux_w
						where 	nr_sequencia = r_c02_w.nr_sequencia;
					end if;
				end if;
			end if;

			end;
		end loop;
	end if;

	--Aplicar ajuste de recurso pr�prio apenas para fins cont�beis(Aplicar ajuste de recurso pr�prio apenas para fins cont�beis - OS 928106)
	if (nvl(ie_apenas_fins_contabeis_p,'N') = 'N') then
		--Como o cursor 1 esta ordenado por protocolos, quando o protocolo atual for diferente do anterior, ent�o pode atualizar os valores das contas e tamb�m
		--gerar os valores de protocolo e gerar resumo. Na primeira itera��o do loop certamente o protocolo anterior ser� diferente do atual, por esse motivo
		--verifica ainda se o protocolo anterior � diferente do valor de inicializa��o. Verificado tamb�m se est� na �ltima itera��o, pois nesse caso, mesmo o 
		--protocolo atual sendo igual ao anterior, � necess�rio processar a atualiza��o do valor das contas para que as contas do �ltimo protocolo da ordena��o 
		--n�o fiquem sem a atualiza��o de valores.
		if  (( r_c01_w.total_registros = nr_iteracao_cursor_w) or 
		     (nr_seq_protocolo_anterior_w <> r_c01_w.nr_seq_protocolo and nr_seq_protocolo_anterior_w <> -1))then

			for r_c03_w in C03(nr_seq_protocolo_anterior_w) loop
				begin
				pls_cta_consistir_pck.gerar_resumo_conta(null, null, null, r_c03_w.nr_sequencia, nm_usuario_p, cd_estabelecimento_p);
				pls_atualiza_valor_conta(r_c03_w.nr_sequencia, nm_usuario_p);
				
				
				--Atualiza evento de pagamento(Pagamento novo).					
				pls_filtro_regra_event_cta_pck.gerencia_regra_filtro(	null, r_c03_w.nr_sequencia, cd_estabelecimento_p, nm_usuario_p);
				end;
			end loop;

			pls_gerar_valores_protocolo(nr_seq_protocolo_anterior_w, nm_usuario_p);
			--Realiza a verifica��o se todas as contas do protocolo est�o fechadas
			select  count(1)
			into  qt_conta_w
			from  pls_conta
			where  nr_seq_protocolo = nr_seq_protocolo_anterior_w
			and  ie_status  != 'F';

			if  (qt_conta_w     = 0) and ((ie_tipo_regra_w   != 4) or (ie_recalculo_item_w   = 'S')) then
			      /*Tartamento para refazer o lote pagamento com os novos valores. Diego OPS - OS 236018*/
			      pls_desfazer_resumo_conta_prot(nr_seq_protocolo_anterior_w, nm_usuario_p);
			      pls_gerar_resumo_conta_prot(nr_seq_protocolo_anterior_w, nm_usuario_p, cd_estabelecimento_p);
			end if;

			select	count(1)
			into	qt_contas_abertas_w
			from	pls_conta
			where	nr_seq_protocolo = nr_seq_protocolo_anterior_w
			and	ie_status <> 'F';

			--Somente far� a consist�ncia de valores caso todas as contas do protocolo estiverem fechadas, pois pode ter alguma conta
			--em an�lise com pend�ncias(talvez nem esteja no lote de rec�lculo, mas esteja em um protocolo que tenha alguma conta no lote) e nesse caso,
			--Emitiria a mensagem desnecessariamente
			if	(qt_contas_abertas_w = 0) then
				--Consiste valores liberados nas contas do protocolo com os valores da conta resumo
				select  sum(vl_total)
				into  	vl_total_protocolo_w
				from  	pls_conta
				where  	nr_seq_protocolo  = nr_seq_protocolo_anterior_w
				and  	vl_total is not null;

				select	nvl(sum(a.vl_lib_original),0)
				into   	vl_pagamento_w
				from   	pls_conta_medica_resumo  a
				where  	a.ie_tipo_item <> 'I' --retira itens de interc�mbio da contagem
				and	a.ie_situacao <> 'I'  --retira itens inativos da contagem
				and	a.nr_seq_conta in  ( 	select   b.nr_sequencia
								from  	pls_conta b
								where 	b.nr_seq_protocolo = nr_seq_protocolo_anterior_w);

				select	max(ie_tipo_protocolo)
				into	ie_tipo_protocolo_w
				from	pls_protocolo_conta
				where	nr_sequencia	= nr_seq_protocolo_anterior_w;

				if  	(nvl(vl_total_protocolo_w,0) <> nvl(vl_pagamento_w,0)) and (ie_tipo_protocolo_w   = 'C') then
					wheb_mensagem_pck.exibir_mensagem_abort(225847);--N�o � poss�vel liberar o protocolo para pagamento, pois existe diverg�ncia de valores
				end if;
			end if;

		end if;
		nr_seq_protocolo_anterior_w := r_c01_w.nr_seq_protocolo;
	end if;

	commit;

	end; --*1
end loop; 	--fim loop cursor 1

--Para efetivar a data de conclus�o da aplica��o do recalculo
update	pls_lote_recalculo
set  	dt_fim_recalculo  	= sysdate,
		dt_atualizacao		= sysdate,
		nm_usuario			= nm_usuario_p
where   nr_sequencia  		= nr_seq_lote_p;

commit;

end pls_aplicar_rec_recurso_prop;
/
