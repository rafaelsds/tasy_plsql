create or replace 
procedure sus_pessoa_fisica_colmeia (	cd_sistema_ant_p		varchar2,
					nr_cpf_p			varchar2,
					nm_pessoa_fisica_p	varchar2,
					ie_sexo_p		varchar2,
					dt_nascimento_p		varchar2,
					nr_identidade_p		varchar2,
					ds_orgao_emissor_ci_p	varchar2,
					ie_estado_civil_p		varchar2,
					nr_seq_cor_pele_p		number,
					nr_seq_etnia_p		number,
					cd_munic_nasc_p		number,
					nr_cert_casamento_p	varchar2,
					nr_cert_nasc_p		varchar2,
					nr_titulo_eleitor_p		varchar2,
					nr_cartao_nac_sus_p	varchar2,
					nr_ddd_celular_p		varchar2,
					nr_telefone_celular_p	varchar2,
					ie_grau_instrucao_p	number,
					sg_emissora_ci_p		varchar2,
					ds_observacao_p		varchar2,
					nm_contato_p		varchar2,
					cd_cep_p		varchar2,
					cd_tipo_logradouro_p	varchar2,
					ds_endereco_p		varchar2,
					nr_endereco_p		number,
					ds_complemento_p		varchar2,
					ds_bairro_p		varchar2,
					ds_municipio_p		varchar2,
					cd_municipio_ibge_p	varchar2,
					sg_estado_p		varchar2,
					ds_email_p		varchar2,
					nr_ddd_telefone_p		varchar2,
					nr_telefone_p		varchar2,
					nm_mae_p		varchar2,
					nm_usuario_p		varchar2,
					cd_pessoa_fisica_p	out varchar2) is

qt_registro_w		number(10);
nr_seq_endereco_w	number(3);
nr_seq_mae_w		number(3);
dt_nascimento_w		date;
ds_erro_w		sus_erro_imp_colmeia.ds_erro%type;
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
ie_commit_w		varchar2(1) := 'N';

begin

if	(dt_nascimento_p is not null) then
	begin
	dt_nascimento_w := to_date(dt_nascimento_p,'dd/mm/yyyy hh24:mi:ss');
	end;
end if;

begin
select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	pessoa_fisica
where	nr_cpf = nr_cpf_p
and 	rownum = 1;
exception
when others then
	cd_pessoa_fisica_w := 'X';
end;

if	(nvl(cd_pessoa_fisica_w,'X') = 'X') then
	begin
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	pessoa_fisica
	where	cd_sistema_ant = cd_sistema_ant_p
	and	rownum = 1;
	exception
	when others then
		cd_pessoa_fisica_w := 'X';
	end;
end if;

if	(nvl(cd_pessoa_fisica_w,'X') = 'X') then
	begin

	select	pessoa_fisica_seq.nextval
	into	cd_pessoa_fisica_w
	from	dual;

	begin
	insert into pessoa_fisica(
		cd_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo_pessoa,
		cd_sistema_ant,
		nr_cpf,
		nm_pessoa_fisica,
		ie_sexo,
		dt_nascimento,
		nr_identidade,
		ds_orgao_emissor_ci,
		ie_estado_civil,
		nr_seq_cor_pele,
		nr_seq_etnia,
		cd_municipio_ibge,
		nr_cert_casamento,
		nr_cert_nasc,
		nr_titulo_eleitor,
		nr_cartao_nac_sus,
		nr_ddd_celular,
		nr_telefone_celular,
		ie_grau_instrucao,
		sg_emissora_ci,
		ds_observacao)
	values(	cd_pessoa_fisica_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		2,
		cd_sistema_ant_p,
		nr_cpf_p,
		nm_pessoa_fisica_p,
		ie_sexo_p,
		dt_nascimento_w,
		nr_identidade_p,
		ds_orgao_emissor_ci_p,
		ie_estado_civil_p,
		nr_seq_cor_pele_p,
		nr_seq_etnia_p,
		cd_munic_nasc_p,
		nr_cert_casamento_p,
		nr_cert_nasc_p,
		nr_titulo_eleitor_p,
		nr_cartao_nac_sus_p,
		nr_ddd_celular_p,
		nr_telefone_celular_p,
		ie_grau_instrucao_p,
		sg_emissora_ci_p,
		ds_observacao_p);
	exception
	when others then
		ds_erro_w := substr(ds_erro_w || 'cd_sistema_ant_p:'|| cd_sistema_ant_p || ';cd_munic_nasc_p:'|| cd_munic_nasc_p || wheb_mensagem_pck.get_texto(282485) || sqlerrm || ';', 2000);
	end;

	select	count(1)
	into	qt_registro_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w
	and	rownum = 1;

	if	(qt_registro_w > 0) then
		begin

		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_endereco_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;

		insert into compl_pessoa_fisica(
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_tipo_complemento,
			nr_sequencia,
			nm_contato,
			cd_cep,
			cd_tipo_logradouro,
			ds_endereco,
			nr_endereco,
			ds_complemento,
			ds_bairro,
			ds_municipio,
			cd_municipio_ibge,
			sg_estado,
			ds_email,
			nr_ddd_telefone,
			nr_telefone)
		values(	cd_pessoa_fisica_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			1,
			nr_seq_endereco_w,
			nm_contato_p,
			cd_cep_p,
			cd_tipo_logradouro_p,
			ds_endereco_p,
			nr_endereco_p,
			ds_complemento_p,
			ds_bairro_p,
			ds_municipio_p,
			cd_municipio_ibge_p,
			sg_estado_p,
			ds_email_p,
			nr_ddd_telefone_p,
			nr_telefone_p);

		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_mae_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;

		insert into compl_pessoa_fisica (
			cd_pessoa_fisica,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			ie_tipo_complemento,
			nr_sequencia,
			nm_contato)
		values(	cd_pessoa_fisica_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			5,
			nr_seq_mae_w,
			nm_mae_p);
		end;
	end if;
	ie_commit_w := 'S';
	end;
end if;

if	(nvl(ds_erro_w, 'X') <> 'X') then
	begin

	insert into sus_erro_imp_colmeia (
		ds_erro,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_atendimento,
		nr_seq_interno,
		nr_sequencia)
	values(	ds_erro_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		null,
		null,
		sus_erro_imp_colmeia_seq.nextval);
	ie_commit_w := 'S';
	end;
end if;

if	(ie_commit_w = 'S') then
	begin
	commit;
	end;
end if;

if	(nvl(cd_pessoa_fisica_w,'X') <> 'X') then
	cd_pessoa_fisica_p	:= cd_pessoa_fisica_w;
else
	cd_pessoa_fisica_p	:= '';
end if;

end sus_pessoa_fisica_colmeia;
/