CREATE OR REPLACE
PROCEDURE Glosa_Material(
		CD_ESTABELECIMENTO_P			NUMBER,
		NR_ATENDIMENTO_P			NUMBER,
		DT_ATENDIMENTO_P    			DATE,
		CD_MATERIAL_P				NUMBER,
		QT_MATERIAL_P				NUMBER,
		CD_TIPO_ACOMODACAO_P			NUMBER,
		IE_TIPO_ATENDIMENTO_P			NUMBER,
		CD_SETOR_ATENDIMENTO_P			NUMBER,
		QT_IDADE_P				NUMBER,
		cd_proc_referencia_p			number,
		ie_origem_proced_p			number,
		nr_sequencia_p				number,
		nr_seq_proc_interno_p			number,
		CD_CONVENIO_P     		IN	OUT	NUMBER,
		CD_CATEGORIA_P    		IN	OUT	VARCHAR2,
		IE_TIPO_CONVENIO_P		OUT	NUMBER,
		IE_CLASSIF_CONVENIO_P		OUT	VARCHAR2,
		CD_AUTORIZACAO_P    		OUT	VARCHAR2,
		NR_SEQ_AUTORIZACAO_P    	OUT	NUMBER,
		QT_AUTORIZADA_P    		OUT	NUMBER,
		CD_SENHA_P    			OUT	VARCHAR2,
		NM_RESPONSAVEL_P    		OUT	VARCHAR2,
		IE_GLOSA_P			OUT	VARCHAR2,
		CD_SITUACAO_GLOSA_P		OUT	NUMBER,
		PR_GLOSA_P			OUT	NUMBER,
		vl_glosa_p			out	number,
		cd_motivo_exc_conta_p		OUT	Number,
		ie_autor_particular_p		out	varchar2,
		cd_convenio_glosa_p		out	number,
		cd_categoria_glosa_p		out	varchar2,
		nr_seq_regra_ajuste_p		out	number,
		nr_seq_orcamento_p		in	number) IS

IE_GLOSA_W			VARCHAR2(1);
CD_AUTORIZACAO_W		VARCHAR2(20)	:= '';
QT_AUTORIZADA_W			NUMBER(11,3)	:= 0;
NR_SEQ_AUTORIZACAO_W		NUMBER(10);
QT_UTILIZADA_W          	NUMBER(11,3)	:= 0;
CD_SITUACAO_GLOSA_W		NUMBER(2)		:= 0;
DT_INICIO_VIGENCIA_W		DATE;
DT_FINAL_VIGENCIA_W		DATE;
ie_classif_convenio_w		Varchar2(03);
IE_TIPO_CONVENIO_W		NUMBER(2)    := 0;
pr_glosa_w			number(7,4);
vl_glosa_w			number(15,4)	:= 0;
cd_motivo_exc_conta_w		Number(15,0);
cd_pessoa_fisica_w		varchar2(10);
CD_CONVENIO_GLOSA_w		number(10,0);
ie_autor_particular_w		varchar2(1);

cd_convenio_glosa_ww		number(5,0) := 0;
cd_categoria_glosa_ww		varchar2(10):= ' ';
nr_seq_regra_ajuste_ww		number(10,0):= 0;
cd_plano_w			varchar2(10);
dt_entrada_w			date;
vl_material_w			number(15,2);
nr_seq_origem_w			number(10,0);
nr_seq_cobertura_w		number(10,0);
qt_dias_internacao_w		number(10,0);
ie_data_mat_dias_int_w		varchar2(1);
cd_usuario_convenio_w		varchar2(30);

BEGIN

select	max(cd_pessoa_fisica),
	max(obter_dado_atend_cat_conv(nr_atendimento_p, DT_ATENDIMENTO_P, CD_CONVENIO_P, CD_CATEGORIA_P, 'P')),
	max(dt_entrada),
	nvl(max(to_number(obter_dados_categ_conv(nr_atendimento_p, 'OC'))),0),
	nvl(max(to_number(obter_dados_categ_conv(nr_atendimento_p, 'COB'))),0),
	nvl(max(trunc(nvl(dt_alta, sysdate) - dt_entrada)),0),
	max(obter_dados_categ_conv(nr_atendimento_p, 'U'))
into	cd_pessoa_fisica_w,
	cd_plano_w,
	dt_entrada_w,
	nr_seq_origem_w,
	nr_seq_cobertura_w,
	qt_dias_internacao_w,
	cd_usuario_convenio_w
from	atendimento_paciente
where	nr_atendimento		= nr_atendimento_p;



if	(nvl(nr_seq_orcamento_p,0) > 0) then
select	max(cd_pessoa_fisica),
	max(cd_plano),
	max(dt_orcamento)
into	cd_pessoa_fisica_w,
	cd_plano_w,
	dt_entrada_w
from	orcamento_paciente
where	nr_sequencia_orcamento		= nr_seq_orcamento_p; 	
end if;

/* Define glosa do material */
begin
cd_situacao_glosa_w := 0;
/*      Obter o tipo do convenio 1-Particular 2-Proprio 3-Sus */
SELECT 	IE_TIPO_CONVENIO,
		ie_classif_contabil,
		cd_convenio_glosa
INTO 		IE_TIPO_CONVENIO_W,
		ie_classif_convenio_w,
		CD_CONVENIO_GLOSA_w
FROM 		CONVENIO
WHERE 	CD_CONVENIO = CD_CONVENIO_P;
exception
	when others then
     		ie_tipo_convenio_w := 2;
end;

select	nvl(max(ie_data_mat_dias_int),'N')
into	ie_data_mat_dias_int_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_p;

if	(ie_data_mat_dias_int_w = 'S') then
	qt_dias_internacao_w	:= nvl(trunc(nvl(dt_atendimento_p, sysdate) - dt_entrada_w),0);
end if;

cd_situacao_glosa_w := 0;
if  	(ie_tipo_convenio_w = 1)  and
	(CD_CONVENIO_GLOSA_w is null) then
	ie_glosa_w	:= 'L';
else
	Define_Glosa_Material
		(CD_ESTABELECIMENTO_P,
		CD_MATERIAL_P,
		DT_ATENDIMENTO_P,
		CD_CONVENIO_P,
		CD_CATEGORIA_P,
		CD_TIPO_ACOMODACAO_P,
		IE_TIPO_ATENDIMENTO_P,
		CD_SETOR_ATENDIMENTO_P,
		QT_IDADE_P, cd_proc_referencia_p, 
		ie_origem_proced_p, 
		nr_sequencia_p,
		nr_seq_proc_interno_p,
		cd_plano_w,
		dt_entrada_w,
		nr_seq_origem_w,
		nr_seq_cobertura_w,
		qt_dias_internacao_w,
		cd_usuario_convenio_w,
		IE_GLOSA_W,
		pr_glosa_w,
		vl_glosa_w,
		cd_motivo_exc_conta_w,
		ie_autor_particular_w,
		cd_convenio_glosa_ww,
		cd_categoria_glosa_ww,
		nr_seq_regra_ajuste_ww);
end if;

dbms_output.put_line(ie_glosa_w);
if	(ie_glosa_w = 'G') then
	cd_situacao_glosa_w := 1;
end if;

if	ie_glosa_w = 'L' then
	begin
	select 	/*+ index (a  ATECACO_I2) */ a.nr_doc_convenio
	into		cd_autorizacao_w
	from		atend_categoria_convenio a
	where		a.nr_atendimento 		= nr_atendimento_p
	and		a.cd_convenio			= cd_convenio_p
	and		a.cd_categoria		= cd_categoria_p
	and		dt_atendimento_p between a.dt_inicio_vigencia and
			nvl(a.dt_final_vigencia,sysdate + 365);
	exception
     		when others then
			cd_autorizacao_w := '';
	end;
end if;

/* Validar autorizacao do convenio */
if	ie_glosa_w = 'A' then
BEGIN
	begin
	select 	b.cd_autorizacao,
			a.qt_autorizada,
			b.cd_senha,
			b.nm_responsavel,
			b.dt_inicio_vigencia,
			nvl(b.dt_fim_vigencia,sysdate),
			b.nr_seq_autorizacao
	into		cd_autorizacao_w,
			qt_autorizada_w,
			cd_senha_p,
			nm_responsavel_p,
			dt_inicio_vigencia_w,
			dt_final_vigencia_w,
			nr_seq_autorizacao_w
	from		material_autorizado a,
			autorizacao_convenio b
	where		b.nr_atendimento 		= nr_atendimento_p
	and		b.cd_convenio		= cd_convenio_p
	and		dt_atendimento_p between trunc(b.dt_inicio_vigencia) and trunc(nvl(b.dt_fim_vigencia,sysdate))
	and    (a.nr_atendimento     = b.nr_atendimento or b.nr_sequencia = a.nr_sequencia_autor)
	and		nvl(a.nr_seq_autorizacao,0) 	= nvl(b.nr_seq_autorizacao,0)
	and		a.cd_material	 	= cd_material_p;
	exception
     		when others then
			cd_situacao_glosa_w := 2;
	end;

if	(ie_glosa_w = 'A') 		and
	(cd_situacao_glosa_w = 0) 	then
	begin
	select 	nvl(sum(qt_material),0)
	into		qt_utilizada_w
	from		material_atend_paciente
	where		nr_atendimento 		= nr_atendimento_p
	and		cd_convenio			= cd_convenio_p
	and		cd_material 		= cd_material_p
	and		nr_seq_autorizacao	= nr_seq_autorizacao_w	
	and		cd_motivo_exc_conta	is null;
	exception
     		when others then
			qt_utilizada_w := 0;
	end;
end if;

if	(ie_glosa_w = 'A') 		and
	(cd_situacao_glosa_w = 0) 	and
	(qt_autorizada_w < (qt_utilizada_w + qt_material_p)) then 
	begin
	cd_autorizacao_w	   := '';
	nr_seq_autorizacao_w := null;	 
	cd_situacao_glosa_w  := 3;
	end;
end if;

END;
end if;

if	ie_glosa_w = 'T' then
	begin
	select 	cd_convenio_glosa,
			cd_categoria_glosa
	into 		cd_convenio_p,
			cd_categoria_p	
	from 		atend_categoria_convenio 
	where		nr_atendimento 		= nr_atendimento_p
	and		dt_atendimento_p between dt_inicio_vigencia and
			nvl(dt_final_vigencia,sysdate + 365)
	and 		cd_convenio_glosa is not null;
	exception
     		when others then
			cd_situacao_glosa_w 	:= 1;
	end;
end if;

if	ie_glosa_w = 'F' then
	cd_situacao_glosa_w := 1;
end if;

if	ie_glosa_w = 'D' then

	vl_material_w:= Obter_preco_material(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, dt_atendimento_p,
					     cd_material_p, cd_tipo_acomodacao_p, ie_tipo_atendimento_p, cd_setor_atendimento_p, null, qt_idade_p,
					     nr_sequencia_p);
	
	if	(vl_material_w = 0) then
		cd_situacao_glosa_w := 11;		
	else
		nr_seq_regra_ajuste_ww	:= 0;
		cd_situacao_glosa_w	:= 0;
	end if;
	
end if;

if	(cd_situacao_glosa_w > 0) then

	/* Ricardo 04/11/2006
				Obter_convenio_particular pela Obter_Convenio_Particular_PF) 
		obter_convenio_particular(CD_ESTABELECIMENTO_P, CD_CONVENIO_p,CD_CATEGORIA_p); */
	obter_convenio_particular_pf(CD_ESTABELECIMENTO_P, CD_CONVENIO_p, cd_pessoa_fisica_w, dt_atendimento_p, 

CD_CONVENIO_p,CD_CATEGORIA_p);
end if;

/*if	(ie_glosa_w = 'V') then
	cd_situacao_glosa_w := 1;
end if;*/

if	(ie_glosa_w = 'P') then
	cd_situacao_glosa_w := 8;
end if;

if	ie_glosa_w = 'T' then
	cd_situacao_glosa_w := 1;
end if;

if	ie_glosa_w = 'B' then
	cd_situacao_glosa_w := 13;	
	begin	
	select 		cd_convenio_glosa,
			cd_categoria_glosa
	into 		cd_convenio_p,
			cd_categoria_p	
	from 		atend_categoria_convenio 
	where		nr_atendimento 		= nr_atendimento_p
	and		dt_atendimento_p between dt_inicio_vigencia and
			nvl(dt_final_vigencia,sysdate + 365)
	and 		cd_convenio_glosa is not null;
	exception
     		when others then
			begin
			select 	cd_convenio_glosa_ww,
				cd_categoria_glosa_ww
			into	cd_convenio_p,
				cd_categoria_p
			from 	dual;
			if	(cd_convenio_p is null) or (cd_categoria_p is null) then
				cd_situacao_glosa_w 	:= 1;
			end if;
			end;
	end;
end if;


pr_glosa_p			:= pr_glosa_w;
vl_glosa_p			:= vl_glosa_w;
ie_tipo_convenio_p		:= ie_tipo_convenio_w;
ie_classif_convenio_p	:= ie_classif_convenio_w;
ie_glosa_p 			:= ie_glosa_w;
cd_situacao_glosa_p		:= cd_situacao_glosa_w; 
cd_autorizacao_p		:= cd_autorizacao_w;
nr_seq_autorizacao_p		:= nr_seq_autorizacao_w; 
cd_motivo_exc_conta_p	:= cd_motivo_exc_conta_w;
ie_autor_particular_p	:= ie_autor_particular_w;
cd_convenio_glosa_p		:= cd_convenio_glosa_ww; 
cd_categoria_glosa_p		:= cd_categoria_glosa_ww;
nr_seq_regra_ajuste_p		:= nr_seq_regra_ajuste_ww;

END Glosa_Material;
/
