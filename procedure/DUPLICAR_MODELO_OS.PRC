create or replace
procedure	duplicar_modelo_os(
			nr_seq_modelo_p			number,
			nm_usuario_p			varchar2) is 

nr_seq_modelo_w		number(10);
			
begin	

select	modelo_os_seq.nextval
into	nr_seq_modelo_w
from	dual;

insert into modelo_os(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	qt_intervalo_campo,
	qt_pos_esquerda,
	qt_topo_campo,
	ds_modelo,
	ie_situacao)
	(select	nr_seq_modelo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		qt_intervalo_campo,
		qt_pos_esquerda,
		qt_topo_campo,
		substr('C�pia ' || ds_modelo,1,60),
		ie_situacao
	from	modelo_os
	where	nr_sequencia = nr_seq_modelo_p);
		
insert into modelo_conteudo_os(
	nr_sequencia,
	nr_seq_modelo,
	nr_seq_perg_visual,     
	nr_seq_pergunta,
	dt_atualizacao,
	nm_usuario,             
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_apres,           
	qt_tamanho,
	ds_label,
	qt_tam_grid,            
	qt_altura,
	ds_label_grid,
	ds_mascara,             
	ie_readonly,
	ie_obrigatorio,
	ie_tabstop,
	ds_sql,
	qt_desloc_dir,
	qt_pos_esquerda,
	ie_italico,
	ie_negrito,
	ie_sublinhado,
	ds_cor,
	qt_fonte,
	ds_formula,
	vl_padrao,
	ie_apres_cad_pre,
	qt_altura_perg_visual,
	qt_tam_label,
	ie_alterar_label)
	(select	modelo_conteudo_os_seq.nextval,
		nr_seq_modelo_w,
		nr_seq_perg_visual,
		nr_seq_pergunta,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_apres,
		qt_tamanho,
		ds_label,
		qt_tam_grid,
		qt_altura,
		ds_label_grid,
		ds_mascara,
		ie_readonly,
		ie_obrigatorio,
		ie_tabstop,
		ds_sql,
		qt_desloc_dir,
		qt_pos_esquerda,
		ie_italico,
		ie_negrito,
		ie_sublinhado,
		ds_cor,
		qt_fonte,
		ds_formula,
		vl_padrao,
		ie_apres_cad_pre,
		qt_altura_perg_visual,
		qt_tam_label,
		ie_alterar_label
	from	modelo_conteudo_os
	where	nr_seq_modelo	= nr_seq_modelo_p);


insert into modelo_desenho_os(
	nr_sequencia,
	qt_topo,
	qt_esquerda,
	qt_altura,
	qt_tamanho,
	nr_seq_modelo,
	nm_usuario,
	dt_atualizacao,
	nr_seq_apres)
	(select	modelo_desenho_os_seq.nextval,
		qt_topo,
		qt_esquerda,
		qt_altura,
		qt_tamanho,
		nr_seq_modelo_w, 
		nm_usuario_p,
		sysdate,
		nr_seq_apres
	from	modelo_desenho_os
	where	nr_seq_modelo	= nr_seq_modelo_p);
	
commit;

end duplicar_modelo_os;
/