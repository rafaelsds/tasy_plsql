create or replace 
procedure importar_tasy_contas_receb_bp (nm_usuario_p	varchar2) is

ds_ativacao_w			varchar2(200);
nr_titulo_w			number(10);
nr_titulo_ww			number(10);
nr_titulo_int_w			number(10);
cd_estabelecimento_w		number(4);
ds_estabelecimento_w		varchar2(255);
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			varchar2(14);
nr_telefone_celular_w		varchar2(255);
vl_titulo_w			number(15,2);
cd_tipo_portador_w			number(5);
cd_portador_w			number(10);
vl_saldo_titulo_w			number(15,2);
dt_vencimento_w			date;
vl_saldo_juros_w			number(15,2);
vl_saldo_multa_w			number(15,2);
dt_emissao_w			date;
vl_liquido_w			number(15,2);
dt_pagamento_previsto_w		date;
cd_moeda_w			number(5);
ie_origem_titulo_w			varchar2(10);
nr_interno_conta_w			number(10);
nr_guia_w			varchar2(20);
nr_atendimento_w			number(10);
ie_tipo_titulo_w			varchar2(2);
cd_tipo_taxa_juro_w		number(10);
tx_juros_w			number(7,4);
ie_tipo_emissao_titulo_w		number(5);
ie_tipo_inclusao_w			varchar2(1);
cd_tipo_taxa_multa_w		number(10);
tx_multa_w			number(7,4);
ie_situacao_w			varchar2(1);
tx_desc_antecipacao_w		number(7,4);
vl_desc_previsto_w			number(15,2);
nr_documento_w			number(22);
cd_serie_w			varchar2(5);
vl_outras_despesas_w		number(15,2);
cd_tipo_recebimento_w		number(5);
nr_titulo_externo_w			varchar2(255);
nr_seq_conta_banco_w		number(10);
nr_seq_carteira_cobr_w		number(10);
nr_seq_classe_w			number(10);
dt_emissao_bloqueto_w		date;
nr_nota_fiscal_w			varchar2(255);
nr_bloqueto_w			varchar2(44);
nr_nfe_imp_w			varchar2(200);
ds_observacao_titulo_w		varchar2(4000);

nr_sequencia_liq_w			number(5);
nr_sequencia_liq_int_w		number(5);
nr_adiantamento_liq_w		number(10);
nr_bordero_liq_w			number(10);
nr_seq_cobranca_liq_w		number(10);
nr_seq_movto_pend_liq_w		varchar2(200);
dt_recebimento_liq_w		date;
ie_acao_liq_w			varchar2(1);
cd_tipo_recebimento_liq_w		number(5);
vl_recebido_liq_w			number(15,2);
vl_descontos_liq_w			number(15,2);
vl_rec_maior_liq_w			number(15,2);
vl_juros_liq_w			number(15,2);
vl_glosa_liq_w			number(15,2);
vl_multa_liq_w			number(15,2);
vl_adequado_liq_w			number(15,2);
vl_despesa_bancaria_liq_w		number(15,2);
vl_perdas_liq_w			number(15,2);
vl_outros_acrescimos_liq_w		number(15,2);
vl_nota_credito_liq_w		number(15,2);
vl_cambial_passivo_liq_w		number(15,2);	
vl_cambial_ativo_liq_w		number(15,2);
vl_recurso_liq_w			number(15,2);
cd_moeda_liq_w			number(5);
cd_centro_custo_desc_liq_w		number(8);
nr_seq_motivo_desc_liq_w		number(10);
ds_observacao_liq_w		varchar2(255);
nr_seq_mot_glosa_liq_w		number(10);
ds_erro_imp_w			varchar2(4000);
ie_lib_caixa_w			varchar2(1);


CURSOR c01 IS

Select 	nr_titulo,
	cd_pessoa_fisica,
	cd_cgc,
	vl_titulo,
	cd_tipo_portador,
	cd_portador,
	vl_saldo_titulo,
	dt_vencimento,
	vl_saldo_juros,
	vl_saldo_multa,
	dt_emissao,
	dt_pagamento_previsto,
	cd_moeda,
	ie_origem_titulo,
	nr_interno_conta,
	nr_guia,
	nr_atendimento,
	ie_tipo_titulo,
	cd_tipo_taxa_juro,
	tx_juros,
	ie_tipo_emissao_titulo,
	ie_tipo_inclusao,
	cd_tipo_taxa_multa,
	tx_multa,
	ie_situacao,
	tx_desc_antecipacao,
	vl_desc_previsto,
	nr_documento,
	cd_serie,
	vl_outras_despesas,
	cd_tipo_recebimento,
	nr_titulo_externo,
	nr_seq_conta_banco,
	nr_seq_carteira_cobr,
	nr_seq_classe,
	dt_emissao_bloqueto,
	nr_nota_fiscal,
	nr_bloqueto,
	ds_observacao_titulo,
	cd_estabelecimento
from	tasy_int.tasy_titulo_receber_bp
where 	ie_situacao_integracao = '1';

CURSOR c02 IS

Select 	nr_sequencia,
	nr_adiantamento,
	nr_bordero,
	nr_seq_cobranca,
	dt_recebimento,
	ie_acao,
	cd_tipo_recebimento,
	vl_recebido,
	vl_descontos,
	vl_rec_maior,
	vl_juros,
	vl_glosa,
	vl_multa,
	vl_adequado,
	vl_despesa_bancaria,
	vl_perdas,
	vl_outros_acrescimos,
	vl_nota_credito,
	vl_cambial_passivo,
	vl_cambial_ativo,
	vl_recurso,
	cd_moeda,
	cd_centro_custo_desc,
	nr_seq_motivo_desc,
	ds_observacao,
	nr_seq_mot_glosa,
	nr_titulo,
	ie_lib_caixa
from	tasy_int.tasy_titulo_receber_liq_bp
where 	ie_situacao_integracao = '1';

begin

open c01;
loop
fetch c01 into
			nr_titulo_w,
			cd_pessoa_fisica_w,
			cd_cgc_w,
			vl_titulo_w,
			cd_tipo_portador_w,
			cd_portador_w,
			vl_saldo_titulo_w,
			dt_vencimento_w,
			vl_saldo_juros_w,
			vl_saldo_multa_w,
			dt_emissao_w,
			dt_pagamento_previsto_w,
			cd_moeda_w,
			ie_origem_titulo_w,
			nr_interno_conta_w,
			nr_guia_w,
			nr_atendimento_w,
			ie_tipo_titulo_w,
			cd_tipo_taxa_juro_w,
			tx_juros_w,
			ie_tipo_emissao_titulo_w,
			ie_tipo_inclusao_w,
			cd_tipo_taxa_multa_w,
			tx_multa_w,
			ie_situacao_w,
			tx_desc_antecipacao_w,
			vl_desc_previsto_w,
			nr_documento_w,
			cd_serie_w,
			vl_outras_despesas_w,
			cd_tipo_recebimento_w,
			nr_titulo_externo_w,
			nr_seq_conta_banco_w,
			nr_seq_carteira_cobr_w,
			nr_seq_classe_w,
			dt_emissao_bloqueto_w,
			nr_nota_fiscal_w,
			nr_bloqueto_w,
			ds_observacao_titulo_w,
			cd_estabelecimento_w;
exit when c01%notfound;
	begin	
	
		select	titulo_seq.NextVal
		into	nr_titulo_int_w
		from	dual;

		insert into titulo_receber(
			nr_titulo,
			cd_pessoa_fisica,
			cd_cgc,
			vl_titulo,
			cd_tipo_portador,
			cd_portador,
			vl_saldo_titulo,
			dt_vencimento,
			vl_saldo_juros,
			vl_saldo_multa,
			dt_emissao,
			dt_pagamento_previsto,
			cd_moeda,
			ie_origem_titulo,
			nr_interno_conta,
			nr_guia,
			nr_atendimento,
			ie_tipo_titulo,
			cd_tipo_taxa_juro,
			tx_juros,
			ie_tipo_emissao_titulo,
			ie_tipo_inclusao,
			cd_tipo_taxa_multa,
			tx_multa,
			ie_situacao,
			tx_desc_antecipacao,
			vl_desc_previsto,
			nr_documento,
			cd_serie,
			vl_outras_despesas,
			cd_tipo_recebimento,
			nr_titulo_externo,
			nr_seq_conta_banco,
			nr_seq_carteira_cobr,
			nr_seq_classe,
			dt_emissao_bloqueto,
			nr_nota_fiscal,
			nr_bloqueto,
			ds_observacao_titulo,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario)
		values	(nr_titulo_int_w,
			cd_pessoa_fisica_w,
			cd_cgc_w,
			vl_titulo_w,
			cd_tipo_portador_w,
			cd_portador_w,
			vl_saldo_titulo_w,
			dt_vencimento_w,
			vl_saldo_juros_w,
			vl_saldo_multa_w,
			dt_emissao_w,
			dt_pagamento_previsto_w,
			cd_moeda_w,
			ie_origem_titulo_w,
			nr_interno_conta_w,
			nr_guia_w,
			nr_atendimento_w,
			ie_tipo_titulo_w,
			cd_tipo_taxa_juro_w,
			tx_juros_w,
			ie_tipo_emissao_titulo_w,
			ie_tipo_inclusao_w,
			cd_tipo_taxa_multa_w,
			tx_multa_w,
			ie_situacao_w,
			tx_desc_antecipacao_w,
			vl_desc_previsto_w,
			nr_documento_w,
			cd_serie_w,
			vl_outras_despesas_w,
			cd_tipo_recebimento_w,
			nr_titulo_externo_w,
			nr_seq_conta_banco_w,
			nr_seq_carteira_cobr_w,
			nr_seq_classe_w,
			dt_emissao_bloqueto_w,
			nr_nota_fiscal_w,
			nr_bloqueto_w,
			ds_observacao_titulo_w,
			cd_estabelecimento_w,
			sysdate,
			nm_usuario_p);
		
		update	tasy_int.tasy_titulo_receber_bp
		set	ie_situacao_integracao 	= '2'
		where	nr_titulo		= nr_titulo_w;
		
		update	tasy_int.tasy_titulo_receber_bp
		set	ds_erro_imp 	= 'T�tulo ' || nr_titulo_int_w ||' importado com sucesso.'
		where	nr_titulo		= nr_titulo_w;		
		
		update	tasy_int.tasy_titulo_receber_bp
		set	nr_titulo_tasy 	= nr_titulo_int_w
		where	nr_titulo	= nr_titulo_w;	
		
		exception
		when others then
			ds_erro_imp_w := sqlerrm;
			
			update	tasy_int.tasy_titulo_receber_bp
				set	ds_erro_imp = 'ERRO AO IMPORTAR TITULO ' || nr_titulo_int_w ||'. COMANDO: ' || ds_erro_imp_w
				where	nr_titulo	= nr_titulo_w;
	end;
end loop;
close c01;

open c02;
loop
fetch c02 into
	nr_sequencia_liq_w,
	nr_adiantamento_liq_w,
	nr_bordero_liq_w,
	nr_seq_cobranca_liq_w,
	dt_recebimento_liq_w,
	ie_acao_liq_w,
	cd_tipo_recebimento_liq_w,
	vl_recebido_liq_w,
	vl_descontos_liq_w,
	vl_rec_maior_liq_w,
	vl_juros_liq_w,
	vl_glosa_liq_w,
	vl_multa_liq_w,
	vl_adequado_liq_w,
	vl_despesa_bancaria_liq_w,
	vl_perdas_liq_w,
	vl_outros_acrescimos_liq_w,
	vl_nota_credito_liq_w,
	vl_cambial_passivo_liq_w,
	vl_cambial_ativo_liq_w,
	vl_recurso_liq_w,
	cd_moeda_liq_w,
	cd_centro_custo_desc_liq_w,
	nr_seq_motivo_desc_liq_w,
	ds_observacao_liq_w,
	nr_seq_mot_glosa_liq_w,
	nr_titulo_w,
	ie_lib_caixa_w;
exit when c02%notfound;
begin

	select	max(nr_titulo_tasy)
	into	nr_titulo_ww
	from	tasy_int.tasy_titulo_receber_bp	a
	where	a.nr_titulo		= nr_titulo_w;	
	
	select	nvl(max(a.nr_sequencia),0) + 1
	into	nr_sequencia_liq_int_w
	from	titulo_receber_liq	a
	where	a.nr_titulo		= nr_titulo_ww;

	insert into titulo_receber_liq(nr_sequencia,
	nr_adiantamento,
	nr_bordero,
	nr_seq_cobranca,
	dt_recebimento,
	ie_acao,
	cd_tipo_recebimento,
	vl_recebido,
	vl_descontos,
	vl_rec_maior,
	vl_juros,
	vl_glosa,
	vl_multa,
	vl_adequado,
	vl_despesa_bancaria,
	vl_perdas,
	vl_outros_acrescimos,
	vl_nota_credito,
	vl_cambial_passivo,
	vl_cambial_ativo,
	vl_recurso,
	cd_moeda,
	cd_centro_custo_desc,
	nr_seq_motivo_desc,
	ds_observacao,
	nr_seq_mot_glosa,
	dt_atualizacao,
	ie_lib_caixa,
	nm_usuario,
	nr_titulo)
	values	(nr_sequencia_liq_int_w,
	nr_adiantamento_liq_w,
	nr_bordero_liq_w,
	nr_seq_cobranca_liq_w,
	dt_recebimento_liq_w,
	ie_acao_liq_w,
	cd_tipo_recebimento_liq_w,
	vl_recebido_liq_w,
	vl_descontos_liq_w,
	vl_rec_maior_liq_w,
	vl_juros_liq_w,
	vl_glosa_liq_w,
	vl_multa_liq_w,
	vl_adequado_liq_w,
	vl_despesa_bancaria_liq_w,
	vl_perdas_liq_w,
	vl_outros_acrescimos_liq_w,
	vl_nota_credito_liq_w,
	vl_cambial_passivo_liq_w,
	vl_cambial_ativo_liq_w,
	vl_recurso_liq_w,
	cd_moeda_liq_w,
	cd_centro_custo_desc_liq_w,
	nr_seq_motivo_desc_liq_w,
	ds_observacao_liq_w,
	nr_seq_mot_glosa_liq_w,
	sysdate,
	ie_lib_caixa_w,
	nm_usuario_p,
	nr_titulo_ww);

	update	tasy_int.tasy_titulo_receber_liq_bp
	set		ie_situacao_integracao 	= '2'
	where	nr_titulo		= nr_titulo_w;
	
	update	tasy_int.tasy_titulo_receber_liq_bp
	set	ds_erro_imp 	= 'T�tulo ' || nr_titulo_ww ||' importado com sucesso.'
	where	nr_titulo		= nr_titulo_w;	

	Atualizar_Saldo_Tit_Rec(nr_titulo_ww, nm_usuario_p);
	
	exception
	when others then
		ds_erro_imp_w := sqlerrm;
		
		update	tasy_int.tasy_titulo_receber_liq_bp
			set	ds_erro_imp = 'ERRO AO IMPORTAR TITULO ' || nr_titulo_ww ||'. COMANDO: ' || ds_erro_imp_w
			where	nr_titulo	= nr_titulo_w;				
			
end;		
end loop;
close c02;

commit;

end importar_tasy_contas_receb_bp;
/
