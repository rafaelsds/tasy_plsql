create or replace procedure gerar_nota_fiscal_inspecao(
		cd_estabelecimento_p	number,
		nr_nota_fiscal_p		number,
		nr_seq_inspecao_p		number,
		cd_serie_nf_p		varchar2,
		nr_ordem_compra_p	number,
		nm_usuario_p		varchar2,
		cd_operacao_nota_p	number,
		cd_natureza_operacao_p	number,
		cd_local_estoque_p	number,
		cd_centro_custo_p	number,
		cd_condicao_pagamento_p	varchar2,
		vl_cotacao_p		number,
		dt_emissao_p		date,
		ie_retorna_frete_p		varchar2,
		nr_sequencia_p	out	number,
		ie_venc_ordem_p		varchar2,
		cd_setor_digitacao_p	number,
		nr_nota_frete_p		varchar2,
		nr_danfe_p		varchar2,
		dt_entrega_ordem_p	date) is

nr_sequencia_nf_w			number(10);
nr_sequencia_w			number(10,0);
nr_seq_inspecao_ww		number(10,0);
dt_atualizacao_w           		date 		:= sysdate;
vl_descontos_w			number(13,2)	:= 0;
vl_despesa_acessoria_nota_w	number(13,2)	:= 0;
vl_unitario_item_nf_w		number(13,4)	:= 0;
vl_total_item_nf_w			number(13,2)	:= 0;
vl_frete_w			number(13,2)	:= 0;
vl_despesa_doc_w		nota_fiscal.vl_despesa_doc%type := 0;
vl_desconto_w			number(13,2)	:= 0;
vl_liquido_w			number(13,2)	:= 0;
qt_item_estoque_w			number(13,4)	:= 0;
cd_unidade_medida_estoque_w	varchar2(30);
qt_conv_compra_estoque_w		number(13,4);
qt_prevista_entrega_w		number(13,4);
qt_lote_w				number(13,4);
dt_prevista_entrega_w		date;
nr_item_oci_w			number(5,0);
cd_cgc_emitente_w		varchar2(14);
cd_cgc_estabelecimento_w		varchar2(14);
cd_material_w			number(6,0);
cd_local_direto_w			number(6,0);
nr_ordem_compra_w		number(10,0);
nr_item_nf_w			number(5,0);
cd_natureza_operacao_ne_w		number(4,0);
cd_natureza_operacao_fe_w		number(4,0);
cd_condicao_pagamento_padrao_w	number(10);
cd_natureza_operacao_w		number(4,0);
cd_natureza_operacao_cad_w	number(4,0);
cd_natureza_op_nf_w		number(4,0);
cd_conta_contabil_w		varchar2(20)	:= null;
cd_conta_contabil_ww		varchar2(20)	:= null;
cd_conta_ordem_w			varchar2(20)	:= null;
cd_material_estoque_w		number(6,0)	:= null;
ie_tipo_conta_w			number(5,0)	:= 2;
nr_seq_conta_financeira_w		number(10,0)	:= null;
ds_lista_inspecao_w		varchar2(255);
cd_centro_conta_w			number(08,0);
sg_estado_fornec_w		pessoa_juridica.sg_estado%type;
sg_estado_estab_w			pessoa_juridica.sg_estado%type;
cd_moeda_w			number(05,0);
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;
nr_solic_compra_w			number(10,0);
ie_com_ordem_w			varchar2(1) := 'N';
nr_atendimento_w			number(15,0);
ie_busca_qt_lote_w			varchar2(1) := 'S';
ie_busca_vl_ordem_w		varchar2(1) := 'S';
cd_perfil_w			number(10);

/*	inspecao recebimento 	*/
qt_material_lote_w			number(15,4);
nr_seq_inspecao_w			number(10);
qt_registro_lote_w			number(5);
ds_lote_fornecedor_w		varchar(20);
dt_validade_lote_w			date;
qt_inspecao_w			number(15,4);
vl_inspecao_w			number(13,4);
ie_indeterminada_w			varchar2(1);
dt_fabricacao_w			date;

/*	ordem_compra 	*/
cd_estab_transf_w			number(4);
cd_pessoa_fisica_w		varchar2(10);
cd_condicao_pagamento_w		number(10,0);
ie_frete_w			varchar2(1);
ie_forma_pagamento_w		number(2);
ie_tipo_ordem_w			varchar2(1);
qt_conversao_w			number(13,4);
nr_seq_unidade_adic_w		number(10);
nr_seq_nota_transf_w		number(10);

/*	ordem compra item 	*/
cd_unidade_medida_compra_w	varchar2(30);
vl_unitario_material_w		number(13,4);
pr_descontos_w			number(13,4);
vl_desconto_oci_w			number(13,2);
cd_local_estoque_w		number(5,0);
ds_material_direto_w		varchar2(255);
ds_observacao_item_w		varchar2(255);
ds_observacao_w			varchar2(4000);
cd_centro_custo_w			number(8,0);
pr_desc_financ_w			number(7,4);
nr_seq_criterio_rateio_w		number(10);
nr_seq_ordem_serv_w		number(10,0);
nr_seq_proj_gpi_w			number(10,0);
nr_seq_etapa_gpi_w		number(10,0);
nr_seq_conta_gpi_w		number(10,0);
nr_contrato_w			number(10,0);
dt_inicio_garantia_w		date;
dt_fim_garantia_w			date;
nr_seq_marca_w			number(10);
nr_seq_marca_ww			number(10);

/*	tributos		*/
cd_tributo_w			number(5,0);
pr_tributo_w			number(7,4);
vl_tributo_w			number(15,2);
nr_seq_proj_rec_w			number(10,0);


/*	vencimentos		*/
dt_vencimento_w			date;
vl_vencimento_w			number(13,2);
qt_nota_ordem_w			number(5);
vl_vencto_w			number(13,2);
vl_liquido_ordem_w			number(15,4);
vl_total_item_ordem_w		number(13,4);
ds_observacao_venc_w		varchar2(255);
ds_erro_w			varchar2(500);

vl_desc_financ_w			number(15,2);
vl_desc_fin_venc_w		number(15,2);
vl_tot_venc_w			number(15,2);
cd_barra_material_w		varchar2(40);
ds_barras_w			varchar2(4000);
ie_conta_contabil_w		varchar2(15);
nr_item_solic_compra_w		solic_compra_item.nr_item_solic_compra%type;
nr_ordem_compra_adiant_w		ordem_compra.nr_ordem_compra%type;
nr_adiant_oc_w			ordem_compra_adiant_pago.nr_adiantamento%type;
vl_vinculado_oc_w			ordem_compra_adiant_pago.vl_vinculado%type;
ie_gerar_adiant_oc_w		varchar2(1) := 'N';
cd_operacao_nf_w			operacao_nota.cd_operacao_nf%type;
qt_registros_w			number(10);
cd_oper_compra_nf_w		parametro_compras.cd_oper_compra_nf%type;
qt_dias_garantia_w			number(10);
nr_seq_regra_contrato_w			inspecao_recebimento.nr_seq_regra_contrato%type;
cd_operacao_estoque_w			operacao_estoque.cd_operacao_estoque%type;

/* Rateio item */
nr_sequencia_item_rateio_w number(10);
nr_seq_orc_item_gpi_rateio_w      number(10);  


cursor c01 is
select	cd_cgc_fornecedor,
	cd_pessoa_fisica,
	cd_condicao_pagamento,
	nvl(ie_frete,'F'),
	cd_moeda,
	ds_observacao,
	decode(ie_retorna_frete_p,'S',vl_frete,0) vl_frete,
	nvl(vl_despesa_acessoria, 0),
	nvl(ie_tipo_ordem,'N'),
	cd_estab_transf,
	nr_atendimento
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_p
and	ie_com_ordem_w = 'S'
union
select	cd_cgc,
	cd_pessoa_fisica,
	to_number(obter_dados_pf_pj_estab(cd_estabelecimento_p ,null,cd_cgc,'ECP')) cd_condicao_pagamento,
	'F',
	to_number(obter_dados_parametro_compras(cd_estabelecimento_p,1)),   
	'',
	0,
	0,
	'N',
	0,
	0
from	inspecao_recebimento a
where	nr_sequencia = nr_seq_inspecao_p
and	((a.nr_seq_tipo_nao_conf is null) or 
	((a.nr_seq_tipo_nao_conf is not null) and (obter_se_nao_conf_gera_nf(a.nr_seq_tipo_nao_conf) = 'S')))
and	a.nr_seq_registro is null
and	ie_com_ordem_w = 'N';

cursor c02 is
select	b.nr_sequencia,
	a.nr_ordem_compra,
	a.nr_item_oci,	
	b.cd_material,
	a.cd_unidade_medida_compra,
	b.vl_unitario_material,
	b.qt_inspecao,
	nvl(b.pr_desconto,0),
	decode(cd_local_estoque_p,0,nvl(a.cd_local_estoque,cd_local_direto_w),cd_local_estoque_p),
	substr(a.ds_material_direto,1,255),
	nvl(decode(cd_centro_custo_p, 0, null, cd_centro_custo_p), a.cd_centro_custo),
	a.cd_conta_contabil,
	a.nr_seq_proj_rec,
	nvl(a.pr_desc_financ,0),
	a.nr_solic_compra,
	a.nr_item_solic_compra,
	a.nr_seq_unidade_adic,
	a.nr_seq_criterio_rateio,
	nvl(b.vl_desconto,0),
	a.nr_seq_ordem_serv,
	a.nr_seq_proj_gpi,
	a.nr_seq_etapa_gpi,
	a.nr_seq_conta_gpi,
	a.nr_contrato,
	b.nr_seq_regra_contrato,
	a.ds_observacao,
	obter_dt_prev_oci_inspecao(b.nr_sequencia),
	a.dt_inicio_garantia,
	a.dt_fim_garantia,
	nvl(b.nr_seq_marca, a.nr_seq_marca),
	a.qt_dias_garantia
from 	inspecao_recebimento b,
	ordem_compra_item a
where	a.nr_ordem_compra = b.nr_ordem_compra
and	a.nr_item_oci = b.nr_item_oci
and	a.nr_ordem_compra = nr_ordem_compra_p
and	((b.nr_seq_tipo_nao_conf is null) or 
	((b.nr_seq_tipo_nao_conf is not null) and (obter_se_nao_conf_gera_nf(b.nr_seq_tipo_nao_conf) = 'S')))
and	b.nr_seq_registro is null
and	a.dt_reprovacao is null
and	b.nr_seq_nota_fiscal is null
and	ie_com_ordem_w = 'S'
and	((dt_entrega_ordem_p is null) or
	((obter_dt_prev_oci_inspecao(b.nr_sequencia) is not null) and (obter_dt_prev_oci_inspecao(b.nr_sequencia) = dt_entrega_ordem_p)))
and	((b.nr_nota_fiscal is null) or
	((b.nr_nota_fiscal is not null) and (b.nr_nota_fiscal = nr_nota_fiscal_p)))
union
select	a.nr_sequencia,
	a.nr_ordem_compra,
	a.nr_item_oci,	
	a.cd_material,
	obter_dados_material(a.cd_material,'UMP'),
	nvl(a.vl_unitario_material,0),
	nvl(a.qt_inspecao,0),
	nvl(a.pr_desconto,0),
	cd_local_estoque_p,
	'',
	nvl(decode(cd_centro_custo_p, 0, null, cd_centro_custo_p), null),
	'',
	null,
	0,
	null,
	null,
	null,
	null,
	nvl(a.vl_desconto,0),
	null,
	null,
	null,
	null,
	null,
	a.nr_seq_regra_contrato,
	'',
	null,
	null,
	null,
	a.nr_seq_marca,
	null
from 	inspecao_recebimento a
where	a.nr_sequencia = nr_seq_inspecao_p
and	((a.nr_seq_tipo_nao_conf is null) or 
	((a.nr_seq_tipo_nao_conf is not null) and (obter_se_nao_conf_gera_nf(a.nr_seq_tipo_nao_conf) = 'S')))
and	a.nr_seq_registro is null
and	ie_com_ordem_w = 'N'
and	((a.nr_nota_fiscal is null) or
	((a.nr_nota_fiscal is not null) and (a.nr_nota_fiscal = nr_nota_fiscal_p)));

cursor c03 is
select	cd_tributo,
	pr_tributo,
	vl_tributo
from	ordem_compra_item_trib
where	nr_ordem_compra	= nr_ordem_compra_p
and	nr_item_oci = nr_item_oci_w;

cursor c04 is
select	a.dt_vencimento,
	a.vl_vencimento,
	a.ds_observacao
from	ordem_compra_venc a
where	a.nr_ordem_compra = nr_ordem_compra_p;

cursor c06 is
select	b.dt_validade,
	b.dt_fabricacao,
	b.cd_lote_fabricacao,
	b.qt_material,
	ie_indeterminada,
	b.cd_barra_material,
	b.ds_barras,
	b.nr_seq_marca
from	inspecao_recebimento_lote b,
	inspecao_recebimento a
where	a.nr_sequencia = b.nr_seq_inspecao
and	nr_seq_inspecao = nr_seq_inspecao_w;

cursor c07 is
select	distinct nr_ordem_compra
from	nota_fiscal_item a
where	a.nr_sequencia = nr_sequencia_w
and	a.nr_ordem_compra > 0;

Cursor C08 is
select	nr_adiantamento,
	vl_vinculado
from	ordem_compra_adiant_pago
where	nr_ordem_compra = nr_ordem_compra_adiant_w;

cursor c09 is
select a.cd_centro_custo,
       a.cd_conta_contabil,
       a.cd_conta_financ,
       a.vl_rateio,
       a.qt_rateio,
       a.vl_frete,
       a.vl_desconto,
       a.vl_seguro,
       a.vl_despesa_acessoria,
       a.nr_seq_criterio_rateio,
       a.ie_situacao
 from ordem_compra_item_rateio a
where nr_ordem_compra = nr_ordem_compra_w
  and nr_item_oci     = nr_item_oci_w;
  
Cursor c10(nr_seq_inspecao_c number) is
select ds_arquivo 
from inspecao_recebimento_anexo
where nr_seq_inspecao = nr_seq_inspecao_c;

begin

cd_perfil_w		:= obter_perfil_ativo;
cd_operacao_nf_w	:= cd_operacao_nota_p;

/*parametros da inspecao*/
select	Obter_Valor_Param_Usuario(270, 31, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p),
	Obter_Valor_Param_Usuario(270, 32, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p)
into	ie_busca_qt_lote_w,
	ie_busca_vl_ordem_w
from	dual;

/*parametros da NF*/
select	Obter_Valor_Param_Usuario(40, 41, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p),
	Obter_Valor_Param_Usuario(40, 441, cd_perfil_w, nm_usuario_p, cd_estabelecimento_p)
into	ie_conta_contabil_w,
	ie_gerar_adiant_oc_w
from	dual;

if	(nr_ordem_compra_p > 0) then
	ie_com_ordem_w := 'S';
end if;

/*consistencia para nao gerar a nota fiscal sem itens*/
select	nvl(max(nr_sequencia),0)
into	nr_seq_inspecao_ww
from	(
	select	max(b.nr_sequencia) nr_sequencia
	from 	inspecao_recebimento b,
		ordem_compra_item a
	where	a.nr_ordem_compra = b.nr_ordem_compra
	and	a.nr_item_oci = b.nr_item_oci
	and	a.nr_ordem_compra = nr_ordem_compra_p
	and	((b.nr_seq_tipo_nao_conf is null) or 
		((b.nr_seq_tipo_nao_conf is not null) and (obter_se_nao_conf_gera_nf(b.nr_seq_tipo_nao_conf) = 'S')))
	and	b.nr_seq_registro is null
	and	a.dt_reprovacao is null
	and	b.nr_seq_nota_fiscal is null
	and	ie_com_ordem_w = 'S'
	and	((dt_entrega_ordem_p is null) or
		((obter_dt_prev_oci_inspecao(b.nr_sequencia) is not null) and (obter_dt_prev_oci_inspecao(b.nr_sequencia) = dt_entrega_ordem_p)))
	and	((b.nr_nota_fiscal is null) or
		((b.nr_nota_fiscal is not null) and (b.nr_nota_fiscal = nr_nota_fiscal_p)))
	union
	select	max(a.nr_sequencia)
	from 	inspecao_recebimento a
	where	a.nr_sequencia = nr_seq_inspecao_p
	and	((a.nr_seq_tipo_nao_conf is null) or 
		((a.nr_seq_tipo_nao_conf is not null) and (obter_se_nao_conf_gera_nf(a.nr_seq_tipo_nao_conf) = 'S')))
	and	a.nr_seq_registro is null
	and	ie_com_ordem_w = 'N'
	and	((a.nr_nota_fiscal is null) or
		((a.nr_nota_fiscal is not null) and (a.nr_nota_fiscal = nr_nota_fiscal_p))));

if	(nr_seq_inspecao_ww = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(199167);
end if;
/*Fim*/
		
select	nvl(max(cd_nat_oper_compra_ne),1102),
	nvl(max(cd_nat_oper_compra_fe),2102),
	max(cd_condicao_pagamento_padrao),
	max(cd_oper_compra_nf)
into	cd_natureza_operacao_ne_w,
	cd_natureza_operacao_fe_w,
	cd_condicao_pagamento_padrao_w,
	cd_oper_compra_nf_w
from	parametro_compras
where	cd_estabelecimento	= cd_estabelecimento_p;

select	count(*)
into	qt_registros_w
from	operacao_nota
where	cd_operacao_nf = cd_operacao_nf_w;

if	(qt_registros_w = 0) then

	if	(cd_oper_compra_nf_w > 0) then
		cd_operacao_nf_w := cd_oper_compra_nf_w;
	end if;
	
	if	(nvl(cd_operacao_nf_w,0) = 0) then
	
		select	min(a.cd_operacao_nf)
		into	cd_operacao_nf_w
		from	operacao_nota a,
			operacao_estoque b
		where	a.cd_operacao_estoque = b.cd_operacao_estoque
		and	a.ie_situacao = 'A'
		and	b.ie_tipo_requisicao = 6
		and	a.ie_operacao_fiscal = 'E';	
	end if;	
end if;

select	nvl(max(vl_cotacao_p),1)
into	vl_cotacao_w
from	dual;

select	a.cd_cgc,
	b.sg_estado
into	cd_cgc_estabelecimento_w,
	sg_estado_estab_w
from	estabelecimento a,
	pessoa_juridica b
where	a.cd_estabelecimento 	= cd_estabelecimento_p
and	a.cd_cgc		= b.cd_cgc;

begin
select	min(cd_local_estoque)
into	cd_local_direto_w
from	local_estoque
where	ie_tipo_local = 8
and	cd_estabelecimento = cd_estabelecimento_p; -- rfoliveira 05/05/2011 OS 314583
exception
	when others then
		cd_local_direto_w := 1;
end;

select	nvl(max(cd_operacao_estoque),0)
into	cd_operacao_estoque_w
from	operacao_nota
where	cd_operacao_nf = cd_operacao_nf_w;

open c01;
loop
fetch	c01 into
	cd_cgc_emitente_w,
	cd_pessoa_fisica_w,
	cd_condicao_pagamento_w,
	ie_frete_w,
	cd_moeda_w,
	ds_observacao_w,
	vl_frete_w,
	vl_despesa_acessoria_nota_w,
	ie_tipo_ordem_w,
	cd_estab_transf_w,
	nr_atendimento_w;
exit when c01%notfound;
	begin
	
	cd_natureza_operacao_w := cd_natureza_operacao_ne_w;

	if	(ie_com_ordem_w = 'S') then
		begin

		select	nvl(cd_cgc,cd_cgc_emitente_w)
		into	cd_cgc_emitente_w
		from	inspecao_recebimento
		where	nr_sequencia = nr_seq_inspecao_p;

		end;
	end if;
	
	if	(cd_condicao_pagamento_p is not null) and (ie_venc_ordem_p = 'N') then
		cd_condicao_pagamento_w := cd_condicao_pagamento_p;
	end if;
	
	if	(cd_condicao_pagamento_w is null) then
		cd_condicao_pagamento_w := cd_condicao_pagamento_padrao_w;
	end if;
	
	if	(cd_condicao_pagamento_w is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(199172);
	end if;
	
	if	(cd_pessoa_fisica_w is not null) then
		cd_cgc_estabelecimento_w := '';
	end if;
	
	if	(cd_cgc_emitente_w is not null) then
		select	sg_estado
		into	sg_estado_fornec_w
		from	pessoa_juridica
		where	cd_cgc = cd_cgc_emitente_w;
		if	(sg_estado_fornec_w <> sg_estado_estab_w) then
			cd_natureza_operacao_w := cd_natureza_operacao_fe_w;
		end if;
	end if;

	select	nvl(max(cd_natureza_operacao),0)
	into	cd_natureza_operacao_cad_w
	from	natureza_operacao
	where	cd_natureza_operacao = cd_natureza_operacao_w;

	if	(cd_natureza_operacao_cad_w = 0) then
		begin
		if	(ie_com_ordem_w = 'S') then
			gravar_log_processo(926, sysdate, nm_usuario_p,  WHEB_MENSAGEM_PCK.get_texto(301567,'CD_NATUREZA_OPERACAO_W='||cd_natureza_operacao_w||
												';NR_NOTA_FISCAL_P='||nr_nota_fiscal_p||
												';NR_ORDEM_COMPRA_W='||nr_ordem_compra_p));
		else
			gravar_log_processo(926, sysdate, nm_usuario_p, WHEB_MENSAGEM_PCK.get_texto(301574,'CD_NATUREZA_OPERACAO_W='||cd_natureza_operacao_w||
												';NR_NOTA_FISCAL_P='||nr_nota_fiscal_p));
		end if;
		cd_natureza_operacao_w	:= 111;
		end;
	end if;

	select	nvl(max(cd_natureza_operacao), 0)
	into	cd_natureza_op_nf_w
	from	operacao_nota
	where	cd_operacao_nf = cd_operacao_nf_w;
	if	(cd_natureza_op_nf_w > 0) then
		cd_natureza_operacao_w	:= cd_natureza_op_nf_w;
	end if;

	if	(nvl(cd_cgc_emitente_w,cd_cgc_estabelecimento_w) is null) then
		begin
		
		select	(nvl(max(nr_sequencia_nf),0)+1)
		into	nr_sequencia_nf_w
		from	nota_fiscal
		where	cd_estabelecimento = cd_estabelecimento_p
		and	cd_cgc_emitente is null
		and	nr_nota_fiscal = to_char(nr_nota_fiscal_p)
		and	cd_serie_nf = cd_serie_nf_p;
	
		end;
	else
		begin
		
		select	(nvl(max(nr_sequencia_nf),0)+1)
		into	nr_sequencia_nf_w
		from	nota_fiscal
		where	cd_estabelecimento = cd_estabelecimento_p
		and	cd_cgc_emitente = nvl(cd_cgc_emitente_w,cd_cgc_estabelecimento_w)
		and	nr_nota_fiscal = to_char(nr_nota_fiscal_p)
		and	cd_serie_nf = cd_serie_nf_p;
	
		end;
	end if;
	
    if	(nr_seq_inspecao_p > 0) then
        select	nvl(sum(a.vl_despesa_doc),0)
		into	vl_despesa_doc_w
		from	ordem_compra a
		where	nr_ordem_compra in (	select distinct x.nr_ordem_compra
						from	inspecao_recebimento x
						where	x.nr_ordem_compra = a.nr_ordem_compra
						and	x.nr_sequencia = nr_seq_inspecao_p
						and	((x.nr_seq_tipo_nao_conf is null) or 
							((x.nr_seq_tipo_nao_conf is not null) and (obter_se_nao_conf_gera_nf(x.nr_seq_tipo_nao_conf) = 'S'))));
    end if;

	/* buscar o valor do desconto para lancar na nota fiscal */
	if	(ie_com_ordem_w = 'S') then
		select	nvl(obter_valor_desconto_ordem(nr_ordem_compra_p),0)
		into	vl_descontos_w
		from	dual;

		if	(ie_tipo_ordem_w = 'T') and
			(cd_estab_transf_w is not null) then
			select	max(nr_sequencia)
			into	nr_seq_nota_transf_w
			from	nota_fiscal
			where	cd_estabelecimento	= cd_estab_transf_w
			and	nr_ordem_compra		= nr_ordem_compra_p;
		end if;
	end if;
	
	consiste_nota_mesmo_numero(cd_estabelecimento_p, nvl(cd_cgc_emitente_w, cd_cgc_estabelecimento_w),cd_pessoa_fisica_w,cd_serie_nf_p,nr_nota_fiscal_p,nm_usuario_p);
		
	select	nota_fiscal_seq.nextval
	into	nr_sequencia_w
	from	dual;	
  
	insert into nota_fiscal (
		cd_estabelecimento,
		cd_cgc_emitente,
		cd_serie_nf,
		nr_nota_fiscal,
		nr_sequencia_nf,
		cd_operacao_nf,
		dt_emissao,
		dt_entrada_saida,
		ie_acao_nf,
		ie_emissao_nf,
		ie_tipo_frete,
		vl_mercadoria,
		vl_total_nota,
		qt_peso_bruto,
		qt_peso_liquido,
		dt_atualizacao,
		nm_usuario,
		cd_condicao_pagamento,
		dt_contabil,
		cd_cgc,
		cd_pessoa_fisica,
		vl_ipi,
		vl_descontos,
		vl_frete,
		vl_seguro,
		vl_despesa_acessoria,
		ds_observacao,
		nr_nota_referencia,
		cd_serie_referencia,
		cd_natureza_operacao,
		dt_atualizacao_estoque,
		vl_desconto_rateio,
		ie_situacao,
		nr_ordem_compra,
		nr_lote_contabil,
		nr_sequencia,
		nr_sequencia_ref,
		cd_moeda,
		vl_conv_moeda,
		ie_entregue_bloqueto,
		ie_tipo_nota,
		cd_setor_digitacao,
		nr_nota_frete,
		nr_danfe,
        	vl_despesa_doc)
	values(	cd_estabelecimento_p,
	 	nvl(cd_cgc_emitente_w, cd_cgc_estabelecimento_w),
	 	cd_serie_nf_p,
	 	nr_nota_fiscal_p,
		nr_sequencia_nf_w,
	 	cd_operacao_nf_w,
	 	nvl(dt_emissao_p, trunc(sysdate)),
	 	dt_atualizacao_w,
	 	'1',
	 	'0',
	 	ie_frete_w,
	 	0,
	 	0,
	 	0,
	 	0,
	 	dt_atualizacao_w,
	 	nm_usuario_p,
	 	cd_condicao_pagamento_w,
	 	null,
	 	cd_cgc_emitente_w,
	 	cd_pessoa_fisica_w,
	 	0,
	 	vl_descontos_w,
		vl_frete_w,
	 	0,
	 	nvl(vl_despesa_acessoria_nota_w, 0),
	 	ds_observacao_w,
	 	null,
	 	null,
		decode(cd_natureza_operacao_p, 0, cd_natureza_operacao_w, cd_natureza_operacao_p),
	 	null,
	 	0,
	 	'1',
	 	nr_ordem_compra_p,
	 	0,
	 	nr_sequencia_w,
		null,
		cd_moeda_w,
		vl_cotacao_w,
		'N',
		decode(cd_pessoa_fisica_w, null, 'EN', 'EF'),
		cd_setor_digitacao_p,
		nr_nota_frete_p,
		nr_danfe_p,
        	vl_despesa_doc_w); 
	begin
	open c02;
	loop
	fetch c02 into
		nr_seq_inspecao_w,
		nr_ordem_compra_w,
		nr_item_oci_w,
		cd_material_w,
		cd_unidade_medida_compra_w,
		vl_unitario_item_nf_w,
		qt_prevista_entrega_w,
		pr_descontos_w,
		cd_local_estoque_w,
		ds_material_direto_w,
		cd_centro_custo_w,
		cd_conta_ordem_w,
		nr_seq_proj_rec_w,
		pr_desc_financ_w,
		nr_solic_compra_w,
		nr_item_solic_compra_w,
		nr_seq_unidade_adic_w,
		nr_seq_criterio_rateio_w,
		vl_desconto_oci_w,
		nr_seq_ordem_serv_w,
		nr_seq_proj_gpi_w,
		nr_seq_etapa_gpi_w,
		nr_seq_conta_gpi_w,
		nr_contrato_w,
		nr_seq_regra_contrato_w,
		ds_observacao_item_w,
		dt_prevista_entrega_w,
		dt_inicio_garantia_w,
		dt_fim_garantia_w,
		nr_seq_marca_w,
		qt_dias_garantia_w;
	exit when c02%notfound;
		select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque,
			qt_conv_compra_estoque,
			cd_material_estoque
		into	cd_unidade_medida_estoque_w,
			qt_conv_compra_estoque_w,
			cd_material_estoque_w
		from 	material
		where	cd_material = cd_material_w;

		if	(ie_busca_qt_lote_w = 'S') then
			select	sum(qt_material)
			into	qt_lote_w
			from	inspecao_recebimento_lote
			where	nr_seq_inspecao = nr_seq_inspecao_w;
			
			if	(qt_lote_w > 0) then
				qt_prevista_entrega_w := qt_lote_w;
			end if;	
			
		end if;
				
		if	(cd_unidade_medida_compra_w = cd_unidade_medida_estoque_w) then
			qt_item_estoque_w	:= qt_prevista_entrega_w;
		else
			qt_item_estoque_w	:= qt_prevista_entrega_w * qt_conv_compra_estoque_w;
		end if;

		if	(nvl(nr_seq_unidade_adic_w, 0) > 0) then
			select	qt_conversao
			into	qt_conversao_w
			from	unidade_medida_adic_compra
			where	nr_sequencia = nr_seq_unidade_adic_w;
			qt_item_estoque_w	:= qt_prevista_entrega_w * qt_conversao_w;
		end if;

		if	(ie_busca_vl_ordem_w = 'S') and
			(nr_ordem_compra_w > 0) and
			(nr_item_oci_w > 0) then
			select	nvl(vl_unitario_material,0)
			into	vl_unitario_item_nf_w
			from	ordem_compra_item
			where	nr_ordem_compra = nr_ordem_compra_w
			and	nr_item_oci = nr_item_oci_w;
		end if;		
		
		if	(nr_seq_nota_transf_w is not null) then /*Matheus OS 100334 04/08/2008*/
			select	nvl(max(vl_unitario_item_nf),0)
			into	vl_unitario_item_nf_w
			from	nota_fiscal_item
			where	nr_sequencia	= nr_seq_nota_transf_w
			and	nr_item_oci	= nr_item_oci_w;
		end if;
		
		vl_unitario_item_nf_w	:= (vl_unitario_item_nf_w * vl_cotacao_w);
		vl_total_item_nf_w	:= (qt_prevista_entrega_w * vl_unitario_item_nf_w);
		
		if	(vl_desconto_oci_w > 0) then
			vl_desconto_w		:= vl_desconto_oci_w;
		else
			vl_desconto_w		:= (vl_total_item_nf_w * pr_descontos_w) / 100 + nvl(vl_desconto_oci_w,0);
		end if;
		
		vl_liquido_w		:=  vl_total_item_nf_w - vl_desconto_w;

		select	(nvl(max(nr_item_nf),0)+1)
		into	nr_item_nf_w
		from	nota_fiscal_item
		where nr_sequencia = nr_sequencia_w;

		
		obter_conta_financeira('S',
					cd_estabelecimento_p,
					cd_material_w,
					null,
					null,
					null,
					null,
					cd_cgc_emitente_w,
					cd_centro_custo_w,
					nr_seq_conta_financeira_w,
					null,
					cd_operacao_nf_w,
					null,
					null,
					null,
					nr_seq_proj_rec_w,
					null,
					null,
					null,
					null,
					null,
					null,
					cd_local_estoque_w,
					null,
					null,
					null,
					null,
					null,
					null);

		if	(nr_seq_conta_financeira_w = 0) then
			nr_seq_conta_financeira_w := null;
		end if;
		
		ie_tipo_conta_w	:= 3;
		if	(cd_centro_custo_w is null) then
			ie_tipo_conta_w	:= 2;
		end if;
		
		define_conta_material(
				cd_estabelecimento_p,
				cd_material_w,
				ie_tipo_conta_w,
				0,
				0,
				0,
				0,
				0,
				0,
				0,
				cd_local_estoque_w,
				cd_operacao_estoque_w,
				trunc(dt_atualizacao_w),
				cd_conta_contabil_ww,
				cd_centro_conta_w,
				null);
				
		if	(ie_conta_contabil_w = 'N') then /*N - nao atualiza conta contabil*/
			cd_conta_contabil_w := '';
			
		elsif	(ie_conta_contabil_w = 'OP') then /* OP - Atualiza c/ conta da OC, se nao tiver busca dos parametros contabil*/
			
			cd_conta_contabil_w := cd_conta_ordem_w;
			if	(nvl(cd_conta_contabil_w,'X') = 'X') then
				cd_conta_contabil_w := cd_conta_contabil_ww;
			end if;
			
		elsif	(ie_conta_contabil_w = 'O') then /*O - Atualiza c/ conta da Ordem de Compra (somente)*/
			cd_conta_contabil_w := cd_conta_ordem_w;
			
		elsif	(ie_conta_contabil_w = 'S') then /*S - Atualiza c/ conta dos parametros contabil, se nao tiver busca da OC*/
			
			cd_conta_contabil_w := cd_conta_contabil_ww;
			if	(nvl(cd_conta_contabil_w,'X') = 'X') then
				cd_conta_contabil_w := cd_conta_ordem_w;
			end if;
		
		elsif	(ie_conta_contabil_w = 'P') then /*P - Atualiza c/ conta dos parametros contabil (somente)*/
			cd_conta_contabil_w := cd_conta_contabil_ww;
		end if;
		
		
		if	(nr_solic_compra_w is not null) and
			(nr_seq_ordem_serv_w is null) then
			select	nr_seq_ordem_serv
			into	nr_seq_ordem_serv_w
			from	solic_compra
			where	nr_solic_compra	= nr_solic_compra_w;
		end if;
				
		if	(qt_dias_garantia_w is not null) then
			dt_inicio_garantia_w	:= nvl(dt_emissao_p, trunc(sysdate));
			dt_fim_garantia_w 	:= (nvl(dt_emissao_p, trunc(sysdate)) + qt_dias_garantia_w);
		end if;
		
		ds_lista_inspecao_w := substr(ds_lista_inspecao_w || nr_seq_inspecao_w || ', ',1,255);
		
		if	(nr_seq_regra_contrato_w > 0) and
			(nvl(nr_seq_proj_rec_w,0) = 0) then		
			select	nr_seq_proj_rec
			into	nr_seq_proj_rec_w
			from	contrato_regra_nf
			where	nr_sequencia = nr_seq_regra_contrato_w;		
		end if;

		insert into nota_fiscal_item(
			cd_estabelecimento,
			cd_cgc_emitente,
			cd_serie_nf,
			nr_nota_fiscal,
			nr_sequencia_nf,
			nr_item_nf,
			cd_natureza_operacao,
			qt_item_nf,
			vl_unitario_item_nf,
			vl_total_item_nf,
			dt_atualizacao,
			nm_usuario,
			vl_frete,
			vl_desconto,
			vl_despesa_acessoria,
			cd_material,
			cd_procedimento,
			cd_setor_atendimento,
			cd_conta,
			cd_local_estoque,
			ds_observacao,
			ds_complemento,
			qt_peso_bruto,
			qt_peso_liquido,
			cd_unidade_medida_compra,
			qt_item_estoque,
			cd_unidade_medida_estoque,
			cd_lote_fabricacao,
			dt_validade,
			dt_atualizacao_estoque,
			cd_conta_contabil,
			vl_desconto_rateio,
			vl_seguro,
			cd_centro_custo,
			cd_material_estoque,
			ie_origem_proced,
			nr_ordem_compra,
			nr_sequencia,
			vl_liquido,
			pr_desconto,
			nr_item_oci,
			dt_entrega_ordem,
			nr_seq_conta_financ,
			nr_seq_proj_rec,
			pr_desc_financ,
			nr_seq_ordem_serv,
			nr_atendimento,
			nr_seq_unidade_adic,
			nr_seq_proj_gpi,
			nr_seq_etapa_gpi,
			nr_seq_conta_gpi,
			nr_contrato,
			nr_seq_inspecao,
			dt_inicio_garantia,
			dt_fim_garantia,
			nr_seq_marca,
			nr_solic_compra,
			nr_item_solic_compra,
			cd_sequencia_parametro)
		values(	cd_estabelecimento_p,
			nvl(cd_cgc_emitente_w,cd_cgc_estabelecimento_w),
			cd_serie_nf_p,
			nr_nota_fiscal_p,
			nr_sequencia_nf_w,
			nr_item_nf_w,
			decode(cd_natureza_operacao_p, 0, cd_natureza_operacao_w, cd_natureza_operacao_p),
			qt_prevista_entrega_w,
			vl_unitario_item_nf_w,
			vl_total_item_nf_w,
			dt_atualizacao_w,
			nm_usuario_p,
			vl_frete_w,
			nvl(vl_desconto_w,0),
			0,
			cd_material_w,
			null,
			null,
			null,
			cd_local_estoque_w,
			ds_observacao_item_w,
			ds_material_direto_w,
			null,
			null,
			cd_unidade_medida_compra_w,
			qt_item_estoque_w,
			cd_unidade_medida_estoque_w,
			'',
			null,
			null,
			cd_conta_contabil_w,
			0,
			0,
			cd_centro_custo_w,
			cd_material_estoque_w,
			null,
			nr_ordem_compra_p,
			nr_sequencia_w,
			vl_liquido_w,
			pr_descontos_w,
			nr_item_oci_w,
			dt_prevista_entrega_w,
			nr_seq_conta_financeira_w,
			nr_seq_proj_rec_w,
			pr_desc_financ_w,
			nr_seq_ordem_serv_w,
			decode(nr_atendimento_w, 0, null, nr_atendimento_w),
			nr_seq_unidade_adic_w,
			nr_seq_proj_gpi_w,
			nr_seq_etapa_gpi_w,
			nr_seq_conta_gpi_w,
			nr_contrato_w,
			nr_seq_inspecao_w,
			dt_inicio_garantia_w,
			dt_fim_garantia_w,
			nr_seq_marca_w,
			nr_solic_compra_w,
			nr_item_solic_compra_w,
			philips_contabil_pck.get_parametro_conta_contabil);
		commit;

		update	inspecao_recebimento
		set	nr_nota_fiscal 	   = nr_nota_fiscal_p,
			nr_seq_nota_fiscal = nr_sequencia_w,
			nr_seq_item_nf	   = nr_item_nf_w
		where	nr_sequencia = nr_seq_inspecao_w;

		if	(nr_seq_criterio_rateio_w is not null) then
			ratear_item_nf(nr_sequencia_w, nr_item_nf_w, nr_seq_criterio_rateio_w, nm_usuario_p, trunc(dt_atualizacao_w));
		end if;

		select	count(*)
		into	qt_registro_lote_w
		from	inspecao_recebimento_lote
		where	nr_seq_inspecao = nr_seq_inspecao_w;

		open c06;
		loop
		fetch c06 into
			dt_validade_lote_w,
			dt_fabricacao_w,
			ds_lote_fornecedor_w,
			qt_material_lote_w,
			ie_indeterminada_w,
			cd_barra_material_w,
			ds_barras_w,
			nr_seq_marca_ww;
		exit when c06%notfound;
			begin
				
			if	(qt_registro_lote_w > 1) then
				insert into nota_fiscal_item_lote(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_nota,
					nr_item_nf,
					dt_validade,
					qt_material,
					cd_lote_fabricacao,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_marca,
					nr_seq_lote_fornec,
					ie_indeterminado,
					cd_barra_material,
					ds_barras,
					dt_fabricacao)
				values	(nota_fiscal_item_lote_seq.nextval,
					sysdate,
					nm_usuario_p,
					nr_sequencia_w,
					nr_item_nf_w,
					dt_validade_lote_w,
					qt_material_lote_w,
					ds_lote_fornecedor_w,
					sysdate,
					nm_usuario_p,
					nr_seq_marca_ww,
					null,
					ie_indeterminada_w,
					cd_barra_material_w,
					ds_barras_w,
					dt_fabricacao_w);
					
				/*tem que gravar a marca que esta no lote, senao fica com a marca que esta na ordem de compra, mas isso esta errado. Tem que ser aquilo que esta inspecionado*/
				if	(nr_seq_marca_ww > 0) then
					update	nota_fiscal_item
					set	nr_seq_marca	= nr_seq_marca_ww
					where	nr_sequencia	= nr_sequencia_w
					and	nr_item_nf	= nr_item_nf_w;
				end if;
			else
				update	nota_fiscal_item
				set	dt_validade		= dt_validade_lote_w,
					cd_lote_fabricacao	= ds_lote_fornecedor_w,
					ie_indeterminado	= ie_indeterminada_w,
					cd_barra_material	= cd_barra_material_w,
					ds_barras			= ds_barras_w,
					dt_fabricacao		= dt_fabricacao_w,
					nr_seq_marca		= nr_seq_marca_ww
				where	nr_sequencia = nr_sequencia_w
				and	nr_item_nf = nr_item_nf_w;
			end if;
			end;
		end loop;
		close c06;

		if	(ie_com_ordem_w = 'S') then
		
			open c03;
			loop
			fetch c03 into
				cd_tributo_w,
				pr_tributo_w,
				vl_tributo_w;
			exit when c03%notfound;
				begin
				insert into nota_fiscal_item_trib(
					cd_estabelecimento,
					cd_cgc_emitente,
					cd_serie_nf,
					nr_nota_fiscal,
					nr_sequencia_nf,
					nr_item_nf,
					cd_tributo,
					vl_tributo,
					dt_atualizacao,
					nm_usuario,
					vl_base_calculo,
					tx_tributo,
					vl_reducao_base,
					nr_sequencia,
					ie_rateio,
					vl_trib_nao_retido,
					vl_base_nao_retido,
					vl_trib_adic,
					vl_base_adic)
				values(	cd_estabelecimento_p,
					nvl(cd_cgc_emitente_w,cd_cgc_estabelecimento_w),
					cd_serie_nf_p,
					nr_nota_fiscal_p,
					nr_sequencia_nf_w,
					nr_item_nf_w,
					cd_tributo_w,
					vl_tributo_w,
					dt_atualizacao_w,
					nm_usuario_p,
					vl_liquido_w,
					pr_tributo_w,
					0,
					nr_sequencia_w,
					'N',
					0,
					0,
					0,
					0);
				end;
			end loop;
			close c03;
		end if;

		begin
      select b.nr_seq_orc_item_gpi
        into nr_seq_orc_item_gpi_rateio_w
        from ordem_compra_item a,
           solic_compra_item b
       where a.NR_SOLIC_COMPRA = b.NR_SOLIC_COMPRA
         and a.NR_ITEM_SOLIC_COMPRA = b.NR_ITEM_SOLIC_COMPRA
         and a.NR_ORDEM_COMPRA = nr_ordem_compra_w
         and a.NR_ITEM_OCI  = nr_item_oci_w;
     exception when no_data_found then
        nr_seq_orc_item_gpi_rateio_w := null;
     end;
		
		/* Iniciar c09 para inserir na tabela NOTA_FISCAL_ITEM_RATEIO */
		for r09 in c09 loop
    
    select	nota_fiscal_item_rateio_seq.nextval
		  into	nr_sequencia_item_rateio_w
		  from	dual;
      
			begin
				insert into NOTA_FISCAL_ITEM_RATEIO (
					nr_sequencia,
					nr_seq_nota,
					nr_item_nf,
					cd_centro_custo, 
					cd_conta_contabil, 
					cd_conta_financ,
					vl_rateio,
					qt_rateio,
					vl_frete,
					vl_desconto,
					vl_seguro,
					vl_despesa_acessoria,
					vl_tributo,
					nr_seq_criterio,
					ie_situacao,
					nr_seq_orc_item_gpi,
					ds_observacao,
          dt_atualizacao,
          nm_usuario)
					values (
					nr_sequencia_item_rateio_w,
					nr_sequencia_w,
					nr_item_nf_w,
					r09.cd_centro_custo,
					r09.cd_conta_contabil, 
					r09.cd_conta_financ,
					r09.vl_rateio,
					r09.qt_rateio,
					r09.vl_frete,
					r09.vl_desconto,
					r09.vl_seguro,
					r09.vl_despesa_acessoria,
					0, -- vl_tributo
					r09.nr_seq_criterio_rateio,
					r09.ie_situacao,
					nr_seq_orc_item_gpi_rateio_w,
					'', -- ds_observacao,
          dt_atualizacao_w,
					nm_usuario_p
					);
			end;
		end loop;

	end loop;
	close c02;
	end;

	if	(ie_com_ordem_w = 'S') then
	
		/* para gerar os vencimentos na nota */
		select	ie_forma_pagamento
		into	ie_forma_pagamento_w
		from	condicao_pagamento
		where	cd_condicao_pagamento = cd_condicao_pagamento_w;

		/* francisco - os 87540 - 28/03/2008 - calcular desconto financeiro */
		select	nvl(sum(vl_desc_financ),0)
		into	vl_desc_financ_w
		from	nota_fiscal_item
		where	nr_sequencia	= nr_sequencia_w;

		select	nvl(sum(a.vl_vencimento),0)
		into	vl_tot_venc_w
		from	ordem_compra_venc a
		where	a.nr_ordem_compra = nr_ordem_compra_p;
		/* francisco - os 87540 - 28/03/2008 - fim alteracao */

		if	(ie_forma_pagamento_w <> 1) and
			(ie_venc_ordem_p = 'S') then
			open c04;
			loop
			fetch	c04 into
				dt_vencimento_w,
				vl_vencimento_w,
				ds_observacao_venc_w;
			exit when c04%notfound;
				begin
				select	nvl(sum(obter_valor_liquido_ordem(nr_ordem_compra)),0)
				into	vl_liquido_ordem_w
				from	ordem_compra
				where	nr_ordem_compra = nr_ordem_compra_p;

				select	nvl(sum(vl_item_liquido),0)
				into	vl_total_item_ordem_w
				from 	ordem_compra_item
				where	nr_ordem_compra	= nr_ordem_compra_p
			  	and	nvl(qt_material,0) > nvl(qt_material_entregue,0);


				vl_vencto_w	:= ((vl_vencimento_w * vl_total_item_ordem_w) / vl_liquido_ordem_w);
				
				
				/* francisco - os 87540 - 28/03/2008 - calcular desconto financeiro */
				vl_desc_fin_venc_w	:=  dividir_sem_round(vl_vencto_w,vl_tot_venc_w) * vl_desc_financ_w;
				/* francisco - os 87540 - 28/03/2008 - fim alteracao */

				begin
				insert into nota_fiscal_venc(
					nr_sequencia,
					cd_estabelecimento,
					cd_cgc_emitente,
					cd_serie_nf,
					nr_nota_fiscal,
					nr_sequencia_nf,
					dt_vencimento,
					vl_vencimento,
					dt_atualizacao,
					nm_usuario,
					vl_desc_financ,
					ds_observacao,
					ie_origem)
				values( nr_sequencia_w,
					cd_estabelecimento_p,
					nvl(cd_cgc_emitente_w, cd_cgc_estabelecimento_w),
					cd_serie_nf_p,
					nr_nota_fiscal_p,
					nr_sequencia_nf_w,
					dt_vencimento_w,
					vl_vencto_w,
					sysdate,
					nm_usuario_p,
					vl_desc_fin_venc_w,
					ds_observacao_venc_w,
					'N');
				exception when others then
					ds_erro_w	:= sqlerrm(sqlcode);
				
					wheb_mensagem_pck.exibir_mensagem_abort(199174,	'NR_ORDEM_COMPRA_P='||NR_ORDEM_COMPRA_P||';'||
											'DT_VENCIMENTO_W='||DT_VENCIMENTO_W||';'||
											'VL_VENCIMENTO_W='||VL_VENCIMENTO_W||';'||
											'DS_ERRO_W='||DS_ERRO_W);
				end;
				end;
			end loop;
			close c04;
		end if;
	end if;
	
	
	if	(nvl(ie_gerar_adiant_oc_w,'N') = 'S' ) then
		
		open C07;
		loop
		fetch C07 into	
			nr_ordem_compra_adiant_w;
		exit when C07%notfound;
			begin
			
			open C08;
			loop
			fetch C08 into
				nr_adiant_oc_w,
				vl_vinculado_oc_w;
			exit when C08%notfound;
				begin

				if (nr_adiant_oc_w is not null) and (vl_vinculado_oc_w is not null) then

					insert into nota_fiscal_adiant_pago (
						nr_sequencia,
						dt_atualizacao,
						dt_atualizacao_nrec,
						nm_usuario,
						nm_usuario_nrec,
						nr_adiantamento,
						nr_seq_trans_financ,
						nr_sequencia_nf,
						vl_vinculado)
					values(	nota_fiscal_adiant_pago_seq.nextval,
						sysdate,
						sysdate,
						nm_usuario_p,
						nm_usuario_p,
						nr_adiant_oc_w,
						null,
						nr_sequencia_w,
						vl_vinculado_oc_w);
				end if;
				end;
			end loop;
			close C08;
			end;
		end loop;
		close C07;
	end if;	

end;
end loop;
close c01;

gerar_historico_nota_fiscal(nr_sequencia_w, nm_usuario_p, '19', WHEB_MENSAGEM_PCK.get_texto(301663,'DS_LISTA_INSPECAO_W='||ds_lista_inspecao_w));

nr_sequencia_p := nr_sequencia_w;

if (Obter_Param_Usuario_padrao(270,112,wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento) = 'S') then
		For r01 in c10(nr_seq_inspecao_p) loop
            INSERT INTO NOTA_FISCAL_ANEXO (
                                            nr_sequencia,
                                            dt_atualizacao,
                                            nm_usuario, 
                                            dt_atualizacao_nrec, 
                                            nm_usuario_nrec,
                                            nr_seq_nota,
                                            ds_arquivo,
                                            ds_arquivo_html
                                           )
                                    VALUES 
                                           (
                                            nota_fiscal_anexo_seq.NextVal,
                                            sysdate, 
                                            nm_usuario_p,
                                            sysdate,
                                            nm_usuario_p,
                                            nr_sequencia_p,
                                            r01.ds_arquivo,
                                            r01.ds_arquivo
                                           );
            end loop;
    end if;
commit;
END gerar_nota_fiscal_inspecao;
/
