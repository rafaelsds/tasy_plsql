create or replace
procedure Ageint_Duplicar_Agenda_Estab(
			nr_seq_ageint_p			number,
			cd_estabelecimento_p		number,
			ie_sem_grupo_p			varchar2,
			nr_seq_ageint_nova_p	out	number,
			ds_consistencia_p	out	varchar2,
			nm_usuario_p			Varchar2) is 

nr_seq_status_w		number(10);
nr_seq_ageint_item_w	number(10);
nr_seq_ageint_w		number(10);
nr_seq_status_cancel_w	number(10);
ie_status_tasy_w	varchar2(15);
nr_seq_check_list_w	number(10,0);
nr_seq_check_list_novo_w number(10,0);
ds_resultado_w		varchar2(1);
nr_seq_item_check_list_w number(10,0);
nr_seq_check_list_pac_item_w	number(10,0);
qt_convenio_w		number(10,0);
cd_convenio_w		number(5,0);
cd_categoria_w		varchar2(10);
cd_plano_w		varchar2(10);
nr_seq_item_grupo_w	number(10,0);
nr_minuto_duracao_w	number(10,0);
nr_seq_grupo_w		number(10,0);
nr_seq_item_selec_w	number(10,0);
ie_inserir_w		varchar2(1);
nr_seq_proc_interno_w	number(10,0);
nr_seq_item_proc_w	number(10,0);

Cursor C01 is
	select	b.nr_sequencia,
		b.nr_seq_item_selec,
		b.nr_seq_proc_interno
	from	agenda_integrada_item b
	where	b.nr_seq_agenda_int	= nr_seq_ageint_p
	and	b.nr_seq_proc_pacote is null
	and 	not exists (
				select	1
				from	ageint_exame_associado a
				where	a.nr_seq_ageint_item	= b.nr_sequencia)
	and	not exists (
				select	1
				from	ageint_itens_adicionais c 
				where	c.nr_seq_ageint_item	= b.nr_sequencia)
	order by 1;
	
Cursor C02 is
	select	nr_sequencia
	from	ageint_check_list_paciente
	where	nr_seq_ageint = nr_seq_ageint_p;
	
	
Cursor C03 is
	select	ds_resultado,
		nr_seq_item_check_list
	from	ageint_check_list_pac_item
	where	nr_seq_check_list = nr_seq_check_list_w;
			
begin

select	max(ie_status_tasy)
into	ie_status_tasy_w
from	agenda_integrada a,
	agenda_integrada_status b
where	a.nr_seq_status = b.nr_sequencia
and	a.nr_sequencia = nr_seq_ageint_p;

select	max(cd_convenio),
	max(cd_categoria),
	max(cd_plano)
into	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w
from	agenda_integrada
where	nr_sequencia = nr_seq_ageint_p;

begin
select	1
into	qt_convenio_w
from	convenio a,
	convenio_estabelecimento b
where	a.cd_convenio = b.cd_convenio
and	b.cd_estabelecimento = cd_estabelecimento_p
and	a.cd_convenio = cd_convenio_w
and	rownum = 1;
exception
when no_data_found then
	qt_convenio_w := 0;
end;

if	(qt_convenio_w = 0) then
	
	cd_convenio_w	:= null;
	cd_categoria_w	:= null;
	cd_plano_w	:= null;
	
	ds_consistencia_p := obter_texto_tasy(262547, wheb_usuario_pck.get_nr_seq_idioma);
end if;


if	(ie_status_tasy_w = 'AG') then
	Wheb_mensagem_pck.exibir_mensagem_abort(262545);
end if;

select	min(nr_sequencia)
into	nr_seq_status_w
from 	agenda_integrada_status 
where 	ie_situacao = 'A' 
and 	ie_Status_tasy = 'EA';

select	agenda_integrada_seq.nextval
into	nr_seq_ageint_w
from	dual;

insert into agenda_integrada
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	dt_inicio_agendamento,
	nr_seq_status,
	cd_pessoa_fisica,
	cd_convenio,
	cd_categoria,
	cd_plano,
	nr_doc_convenio,
	dt_validade_carteira,
	cd_usuario_convenio,
	nm_contato,
	nr_telefone,
	cd_profissional,
	cd_estabelecimento,
	cd_agenda_externa,
	dt_prevista,
	qt_peso,
	qt_altura_cm,
	ie_turno,
	ds_observacao,
	ie_tipo_atendimento,
	nm_paciente,
	dt_nascimento,
	ds_obs_final,
	nr_seq_mot_cancel,
	cd_empresa,
	cd_medico_solicitante,
	ds_indicacao,
	nr_seq_tipo_classif_pac,
	nr_seq_cobertura,
	nm_medico_externo,
	crm_medico_externo)
select	nr_seq_ageint_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nr_seq_status_w,
	cd_pessoa_fisica,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w,
	nr_doc_convenio,
	dt_validade_carteira,
	cd_usuario_convenio,
	nm_contato,
	nr_telefone,
	cd_profissional,
	cd_estabelecimento_p,
	cd_agenda_externa,
	dt_prevista,
	qt_peso,
	qt_altura_cm,
	ie_turno,
	ds_observacao,
	ie_tipo_atendimento,
	nm_paciente,
	dt_nascimento,
	ds_obs_final,
	nr_seq_mot_cancel,
	cd_empresa,
	cd_medico_solicitante,
	ds_indicacao,
	nr_seq_tipo_classif_pac,
	nr_seq_cobertura,
	nm_medico_externo,
	crm_medico_externo
from	agenda_integrada
where	nr_sequencia	= nr_seq_ageint_p;		

open C01;
loop
fetch C01 into	
	nr_seq_ageint_item_w,
	nr_seq_item_selec_w,
	nr_seq_proc_interno_w;
exit when C01%notfound;
	begin
	
	nr_seq_item_grupo_w	:= 0;
	
	begin
	select	a.nr_sequencia
	into	nr_seq_item_grupo_w
	from	agenda_int_grupo_item a,
		agenda_int_grupo b,
		agenda_int_area c
	where	a.nr_seq_grupo = b.nr_sequencia
	and	c.nr_sequencia = b.nr_seq_area
	and	a.nr_seq_proc_interno = nr_seq_proc_interno_w
	and	nvl(c.cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p;
	exception
	when no_data_found then
		nr_seq_item_proc_w := 0;
	when others then
		nr_seq_item_proc_w := 2;
	end;
	
	--20011,cd_estabelecimento_p||' - '||nr_seq_item_grupo_w||' - '||nr_seq_item_proc_w||'#@#@');
	
	if	(nr_seq_item_proc_w = 2)  then
		Ageint_Obter_Item_De_Para(nr_seq_item_selec_w,
				  cd_estabelecimento_p,
				  cd_estabelecimento_p,
				  nm_usuario_p,
				  nr_seq_item_grupo_w);
	end if;
				  
	select	max(a.nr_minuto_duracao),
		max(b.nr_sequencia)
	into	nr_minuto_duracao_w,
		nr_seq_grupo_w
	from	agenda_int_grupo_item a,
		agenda_int_grupo b
	where	a.nr_seq_grupo = b.nr_sequencia
	and	a.nr_sequencia = nr_seq_item_grupo_w;
	
	if	(ie_sem_grupo_p = 'N') and
		(nr_seq_item_grupo_w = 0) then
		ie_inserir_w := 'N';
	else
		ie_inserir_w := 'S';
	end if;
	
	if	(ie_inserir_w = 'S') then
		insert into agenda_integrada_item
			(nr_sequencia,
			nr_seq_agenda_int,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_proc_interno,
			ie_tipo_agendamento,
			cd_medico,
			cd_especialidade,
			cd_profissional_exame,
			nr_minuto_duracao,
			ie_classif_agenda,
			nr_classificacao_agend,
			cd_medico_req,
			dt_entrega_prevista,
			ie_lado,
			ds_observacao,
			nr_seq_regra,
			ie_tipo_exame_proc,
			nr_seq_grupo_proc,
			cd_procedimento,
			ie_origem_proced,
			nr_seq_grupo_selec,
			ie_anestesia,
			cd_anestesista,
			nr_seq_sala,
			nr_seq_transporte,
			ie_autorizacao,
			nr_seq_regra_ex_adic,
			dt_transferencia,
			nr_seq_item_selec,
			cd_estabelecimento,
			ie_duplicado_estab,
			nr_seq_classif_agenda)
		select	agenda_integrada_item_seq.nextval,
			nr_seq_ageint_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_proc_interno,
			ie_tipo_agendamento,
			cd_medico,
			cd_especialidade,
			cd_profissional_exame,
			nvl(nr_minuto_duracao_w,nr_minuto_duracao),
			ie_classif_agenda,
			nr_classificacao_agend,
			cd_medico_req,
			dt_entrega_prevista,
			ie_lado,
			ds_observacao,
			nr_seq_regra,
			ie_tipo_exame_proc,
			nr_seq_grupo_proc,
			cd_procedimento,
			ie_origem_proced,
			nvl(nr_seq_grupo_w,nr_seq_grupo_selec),
			ie_anestesia,
			cd_anestesista,
			nr_seq_sala,
			nr_seq_transporte,
			ie_autorizacao,
			nr_seq_regra_ex_adic,
			dt_transferencia, 
			nvl(nr_seq_item_grupo_w,nr_seq_item_selec),
			cd_estabelecimento_p,
			'S',
			nr_seq_classif_agenda
		from	agenda_integrada_item
		where	nr_sequencia	= nr_seq_ageint_item_w;
	end if;
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_seq_check_list_w;
exit when C02%notfound;
	begin
	
	select	ageint_check_list_paciente_seq.nextval
	into	nr_seq_check_list_novo_w
	from	dual;
	
	insert into ageint_check_list_paciente(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_ageint,
		dt_liberacao,
		ie_tipo_check_list)
	select	nr_seq_check_list_novo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_ageint_w,
		dt_liberacao,
		ie_tipo_check_list
	from	ageint_check_list_paciente
	where	nr_sequencia = nr_seq_check_list_w;
		
	open C03;
	loop
	fetch C03 into	
		ds_resultado_w,
		nr_seq_item_check_list_w;
	exit when C03%notfound;
		begin
		
		select	ageint_check_list_pac_item_seq.nextval
		into	nr_seq_check_list_pac_item_w
		from	dual;
		
		insert into ageint_check_list_pac_item(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_check_list,
			ds_resultado,
			nr_seq_item_check_list)
		values	(nr_seq_check_list_pac_item_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_check_list_novo_w,
			ds_resultado_w,
			nr_seq_item_check_list_w);
		end;
	end loop;
	close C03;
	end;
end loop;
close C02;

insert into ageint_exame_lab(nr_sequencia,
		dt_atualizacao,
		nm_usuario,        
		dt_atualizacao_nrec,
		nm_usuario_nrec,   
		nr_seq_proc_interno,
		nr_seq_ageint,   
		cd_procedimento,
		ie_origem_proced,
		nr_seq_exame)
select  ageint_exame_lab_seq.nextval,
		sysdate,
		nm_usuario_p,        
		sysdate,
		nm_usuario_p,   
		nr_seq_proc_interno,
		nr_seq_ageint_w,   
		cd_procedimento,
		ie_origem_proced,
		nr_seq_exame
from	ageint_exame_lab
where	nr_seq_ageint = nr_seq_ageint_p;
 
select	min(nr_sequencia)
into	nr_seq_status_cancel_w
from	agenda_integrada_status 
where 	ie_situacao = 'A' 
and 	ie_Status_tasy = 'CA';

ageint_alterar_status(nr_seq_status_cancel_w, nr_seq_ageint_p, wheb_mensagem_pck.get_texto(791005), nm_usuario_p, cd_estabelecimento_p);

nr_seq_ageint_nova_p	:= nr_seq_ageint_w;

commit;

end Ageint_Duplicar_Agenda_Estab;
/
