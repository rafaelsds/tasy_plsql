create or replace
procedure hd_gerar_dialise_lib_prescr( nr_atendimento_p	number,
			nr_prescricao_p		number,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number,
			nr_erro_p	out	number,
			ds_erro_p	out	varchar2) is 

nr_atendimento_w	number(10);
ie_perm_sem_prescr_w	varchar2(1);
cd_pessoa_fisica_w	varchar2(10);
nr_dialise_atual_w	varchar2(255);
obter_se_possui_prescr_w varchar2(1);
ie_dialise_w		varchar2(1);
ie_dial_liberado_w	varchar2(1);
ds_erro_w		varchar2(255);
cd_unidade_w		number(10);
ie_consiste_dialisador_w	varchar2(1);

begin
nr_erro_p := 0;
if	(nr_atendimento_p is not null) then
	nr_atendimento_w := nr_atendimento_p;
else
	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_p;
end if;

if	(nr_atendimento_w is null) then
	nr_erro_p := 203594;
else
	obter_param_usuario(7009,198,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_perm_sem_prescr_w);	
	obter_param_usuario(7009,269,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_consiste_dialisador_w);	
	
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;
	
	select	max(substr(hd_obter_dados_dialise(hd_obter_hemodialise_atual(cd_pessoa_fisica, 'U'),'SP'),1,200)),
		max(substr(hd_obter_se_em_dialise(cd_pessoa_fisica,'E'),1,1)),
		max(substr(hd_obter_se_dial_liberado(cd_pessoa_fisica),1,1)) 
	into	nr_dialise_atual_w,
		ie_dialise_w,
		ie_dial_liberado_w
	from	hd_pac_renal_cronico
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	
	select 	max(hd_obter_unidade_prc(cd_pessoa_fisica_w, 'C')),
		max(substr(hd_obter_se_possui_prescr(nr_atendimento_w),1,1))
	into	cd_unidade_w,
		obter_se_possui_prescr_w
	from 	dual;		 
	
	
	if	(ie_perm_sem_prescr_w = 'N') and	
		(obter_somente_numero(nr_dialise_atual_w) <> 0) and
		(obter_se_possui_prescr_w = 'N') then
		nr_erro_p := 191163;
	elsif	(cd_pessoa_fisica_w is null) then
		nr_erro_p := 203596;
	elsif	(ie_dialise_w = 'S') then
		nr_erro_p := 203597;
	elsif	(ie_dial_liberado_w = 'N') and
			(ie_consiste_dialisador_w = 'S') then
		nr_erro_p := 203598;
	end if;
	
	if	(nvl(nr_erro_p,0) = 0) then
		HD_Gerar_Dialise(sysdate,cd_pessoa_fisica_w,nm_usuario_p,cd_unidade_w,cd_estabelecimento_p,nr_atendimento_w,null,null,ds_erro_w);
	end if;
	
	ds_erro_p := ds_erro_w;
end if;	

commit;

end hd_gerar_dialise_lib_prescr;
/
