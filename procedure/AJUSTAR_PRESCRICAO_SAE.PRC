create or replace
procedure Ajustar_prescricao_SAE(   nr_prescricao_p		number, 
                                    nr_atendimento_p 	number,
                                    nm_usuario_p        varchar2) is

ie_gerar_prescricao_w		varchar2(10);
cd_funcao_origem_w			prescr_medica.cd_funcao_origem%type;
nr_seq_material_w           number(10);
nr_intervencao_w            number(10);
nr_intervalo_w              number(10);
cd_material_w               number(10);
nr_seq_presc_mat_hora_w     number(10);

--Materiais prescritos com mais de 24 horas de intervalo, que sejam de origem da SAE
cursor c01 is
    select  a.cd_funcao_origem, 
            b.nr_seq_pe_proc, 
            b.cd_material,
            obter_ocorrencia_intervalo(b.cd_intervalo, nvl(a.nr_horas_validade, 24), 'H') intervalo
    from    prescr_medica      a,
            prescr_material    b 
    where   a.nr_prescricao = nr_prescricao_p
    and     b.nr_prescricao = a.nr_prescricao
    and     obter_se_interv_superior_24h(b.cd_intervalo, null) = 'S'
    and     b.nr_seq_pe_proc is not null;
    
    
cursor c02 is
    select  a.nr_sequencia, 
            a.nr_seq_material
    from    prescr_mat_hor a
    where   a.nr_atendimento = nr_atendimento_p
    and     a.nr_prescricao  = nr_prescricao_p
    and     a.dt_suspensao is null
    --and     coalesce(a.ie_horario_especial,'N') <> 'S'    
	and     exists  (
            select  x.nr_sequencia
            from    prescr_mat_hor  x,
                    prescr_material z
            where   z.nr_prescricao  = x.nr_prescricao
            and     z.nr_sequencia   = x.nr_seq_material
            and     x.nr_atendimento = a.nr_atendimento
            and     x.nr_prescricao <> a.nr_prescricao
			and     x.nr_seq_material = a.nr_seq_material
            and     x.dt_suspensao is null
			and		rownum = 1
			and		a.cd_material = x.cd_material
            and     nvl(z.ie_administrar, 'S') <> 'N'
            and     x.dt_horario > (a.dt_horario - nr_intervalo_w/24));

    
begin
    Obter_Param_Usuario(281, 806, obter_perfil_ativo, nm_usuario_p, nvl(obter_estab_atend(nr_atendimento_p),0), ie_gerar_prescricao_w);
    
    if (ie_gerar_prescricao_w = 'C') then
    
        open c01;
        loop
        fetch c01 into
            cd_funcao_origem_w,
            nr_intervencao_w,
            cd_material_w,
            nr_intervalo_w;
        exit when c01%notfound;
        begin
        
            open c02;
            loop
            fetch c02 into  
                nr_seq_presc_mat_hora_w,
                nr_seq_material_w;
            exit when c02%notfound;
            begin
                update  prescr_material
                set     ie_administrar  = 'N'
                where   nr_prescricao   = nr_prescricao_p
                and     nr_sequencia    = nr_seq_material_w;
                
                update  prescr_mat_hor
                set     ie_administrar  = 'N'
                where   nr_prescricao   = nr_prescricao_p
                and     nr_sequencia    = nr_seq_presc_mat_hora_w;                
                
                commit;
                
            end;
            end loop;
            close c02;
            
        end;
        end loop;
        close c01;
        
    end if;
    
end Ajustar_prescricao_SAE;
/