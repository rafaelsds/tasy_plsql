create or replace
procedure cm_gravar_anexo_teste(ds_arquivo_p		varchar2,
				nm_usuario_p		varchar2,
				nr_seq_ciclo_teste_p 	number ) is

begin


if	(ds_arquivo_p is not null) then
	insert into cm_teste_ciclo_anexo(
		nr_sequencia,		
		dt_atualizacao,
		nm_usuario,
		nr_seq_ciclo_teste,
		ds_arquivo,			
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	cm_teste_ciclo_anexo_seq.nextval,	
		sysdate,
		nm_usuario_p,
		nr_seq_ciclo_teste_p,
		ds_arquivo_p,
		sysdate,
		nm_usuario_p);
	commit;
end if;

end cm_gravar_anexo_teste;
/