create or replace
procedure med_excluir_resultado_exame(	nr_sequencia_p	number) is

begin
if	(nr_sequencia_p is not null) then
	
	delete	from med_result_exame
	where	nr_sequencia = nr_sequencia_p;
	
end if;

commit;

end med_excluir_resultado_exame;
/