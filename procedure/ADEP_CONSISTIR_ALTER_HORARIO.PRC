create or replace
procedure adep_consistir_alter_horario(	nr_seq_horario_p	number,
										ie_tipo_horario_p	varchar) is

nr_prescricao_w		prescr_medica.nr_prescricao%type;
nr_sequencia_w		number(10,0);
dt_horario_w		date;
ie_ok_w				char(1) := 'S';
ie_gas_separada_w	varchar2(2 char);
nr_seq_gas_cpoe_w	cpoe_gasoterapia.nr_sequencia%type;

begin
if	(nr_seq_horario_p is not null) and
	(ie_tipo_horario_p is not null) then

	if	(ie_tipo_horario_p = 'D') then

		select	max(nr_prescricao),
				max(dt_horario),
				max(nr_seq_dieta)
		into	nr_prescricao_w,
				dt_horario_w,
				nr_sequencia_w
		from	prescr_dieta_hor
		where	nr_sequencia = nr_seq_horario_p
		and		dt_lib_horario is not null;

		select	coalesce(max('N'),'S')
		into	ie_ok_w
		from	prescr_dieta_hor
		where	rownum = 1
		and		nr_prescricao 	= nr_prescricao_w
		and		nr_seq_dieta	= nr_sequencia_w
		and		coalesce(dt_fim_horario, dt_suspensao) is null
		and		coalesce(ie_situacao,'A') = 'A'
		and		dt_horario < dt_horario_w
		and		dt_lib_horario is not null;


	elsif	(ie_tipo_horario_p in ('M','S', 'LD', 'IAH')) then

		select	max(nr_prescricao),
				max(nr_seq_material),
				max(dt_horario)
		into	nr_prescricao_w,
				nr_sequencia_w,
				dt_horario_w
		from	prescr_mat_hor
		where	nr_sequencia = nr_seq_horario_p
		and		dt_lib_horario is not null;

		select	coalesce(max('N'),'S')
		into	ie_ok_w
		from	prescr_mat_hor
		where	rownum = 1
		and		nr_prescricao 		= nr_prescricao_w
		and		nr_seq_material		= nr_sequencia_w
		and		coalesce(dt_fim_horario, coalesce(dt_suspensao, dt_recusa)) is null
		and		coalesce(ie_adep,'S')	= 'S'
		and		coalesce(ie_horario_especial,'N') = 'N'
		and		dt_horario < dt_horario_w
		and		dt_lib_horario is not null;


	elsif	(ie_tipo_horario_p in ('P','G','C')) then

		select	max(nr_prescricao),
				max(nr_seq_procedimento),
				max(dt_horario)
		into	nr_prescricao_w,
				nr_sequencia_w,
				dt_horario_w
		from	prescr_proc_hor
		where	nr_sequencia = nr_seq_horario_p
		and		dt_lib_horario is not null;

		select	coalesce(max('N'),'S')
		into	ie_ok_w
		from	prescr_proc_hor
		where	rownum = 1
		and		nr_prescricao 		= nr_prescricao_w
		and		nr_seq_procedimento	= nr_sequencia_w
		and		coalesce(dt_fim_horario, dt_suspensao) is null
		and		coalesce(ie_situacao,'A')		= 'A'
		and		coalesce(ie_horario_especial,'N') <> 'S'
		and		dt_horario < dt_horario_w
		and		dt_lib_horario is not null;


	elsif	(ie_tipo_horario_p = 'R') then

		select	max(nr_prescricao),
				max(nr_seq_recomendacao),
				max(dt_horario)
		into	nr_prescricao_w,
				nr_sequencia_w,
				dt_horario_w
		from	prescr_rec_hor
		where	nr_sequencia = nr_seq_horario_p
		and		dt_lib_horario is not null;

		select	coalesce(max('N'),'S')
		into	ie_ok_w
		from	prescr_rec_hor
		where	rownum = 1
		and		nr_prescricao 		= nr_prescricao_w
		and		coalesce(ie_horario_especial,'N') <> 'S'
		and		nr_seq_recomendacao	= nr_sequencia_w
		and		coalesce(dt_fim_horario, dt_suspensao) is null
		and		dt_horario < dt_horario_w
		and		dt_lib_horario is not null;

	elsif	(ie_tipo_horario_p = 'E') then

		select	max(nr_seq_pe_prescr),
				max(nr_seq_pe_proc),
				max(dt_horario)
		into	nr_prescricao_w,
				nr_sequencia_w,
				dt_horario_w
		from	pe_prescr_proc_hor
		where	nr_sequencia = nr_seq_horario_p;

		select	coalesce(max('N'),'S')
		into	ie_ok_w
		from	pe_prescr_proc_hor
		where	rownum = 1
		and		nr_seq_pe_prescr	= nr_prescricao_w
		and		nr_seq_pe_proc		= nr_sequencia_w
		and		coalesce(dt_fim_horario, dt_suspensao) is null
		and		dt_horario < dt_horario_w;

	elsif	(ie_tipo_horario_p = 'O') then

		ie_gas_separada_w := wheb_assist_pck.obterParametroFuncao(1113,704);

		select	max(a.nr_prescricao),
				max(a.nr_seq_gasoterapia),
				max(a.dt_horario),
				max(b.nr_seq_gas_cpoe)
		into	nr_prescricao_w,
				nr_sequencia_w,
				dt_horario_w,
				nr_seq_gas_cpoe_w
		from	prescr_gasoterapia_hor		a,
				prescr_gasoterapia			b
		where	b.nr_sequencia		= a.nr_seq_gasoterapia
		and		a.nr_sequencia 		= nr_seq_horario_p
		and		a.dt_lib_horario is not null;

		if	(ie_gas_separada_w = 'S' or nr_seq_gas_cpoe_w is null) then
			select	coalesce(max('N'),'S')
			into	ie_ok_w
			from	prescr_gasoterapia_hor
			where	rownum = 1
			and		nr_prescricao	= nr_prescricao_w
			and		coalesce(ie_horario_especial,'N') <> 'S'
			and		nr_seq_gasoterapia = nr_sequencia_w
			and		coalesce(dt_fim_horario, dt_suspensao) is null
			and		dt_horario < dt_horario_w
			and		dt_lib_horario is not null;
		else
			select	coalesce(max('N'),'S')
			into	ie_ok_w
			from	prescr_gasoterapia_hor	a,
					prescr_gasoterapia		b
			where	b.nr_sequencia		= a.nr_seq_gasoterapia
			and		b.nr_seq_gas_cpoe 	= nr_seq_gas_cpoe_w
			and		a.dt_horario < dt_horario_w
			and		a.dt_lib_horario is not null
			and		coalesce(a.dt_fim_horario, a.dt_suspensao) is null
			and		coalesce(a.ie_horario_especial,'N') <> 'S'
			and		rownum = 1;
		end if;
	end if;

	if	(ie_ok_w = 'N') then
		-- Existem hor�rios pendentes anteriores ao atual, favor verificar os mesmos primeiramente!
		Wheb_mensagem_pck.exibir_mensagem_abort(224908);
	end if;
end if;

end adep_consistir_alter_horario;
/
