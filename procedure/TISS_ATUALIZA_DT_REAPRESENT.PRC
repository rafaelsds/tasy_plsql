create or replace
procedure TISS_ATUALIZA_DT_REAPRESENT(	nr_seq_reap_lote_p	number,
					nm_usuario_p		Varchar2) is 
begin
if	(nr_seq_reap_lote_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	tiss_reap_lote
	set	dt_reapresentacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_seq_reap_lote_p;
	end;
end if;	

commit;

end TISS_ATUALIZA_DT_REAPRESENT;
/