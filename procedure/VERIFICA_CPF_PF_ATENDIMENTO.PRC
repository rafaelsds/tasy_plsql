create or replace
procedure VERIFICA_CPF_PF_ATENDIMENTO(
				nr_atendimento_p			number,
				cd_convenio_p			number,
				ie_tipo_atendimento_p		number,
				ds_erro_p out 			varchar2) is 
											
cd_pessoa_fisica_w		varchar2(10);
cd_pessoa_resp_w		varchar2(10);
cd_nacionalidade_w		nacionalidade.cd_nacionalidade%type;
nr_cpf_w				number;
nr_cartao_estrangeiro_w 		number;
nr_reg_geral_estrang_w		pessoa_fisica.nr_reg_geral_estrang%type;
qt_itens_regra_w			number(5);
NR_PASSAPORTE_w		varchar2(255);
ie_tipo_convenio_w			number(2);
ie_obriga_cpf_estrangeiro_w	varchar2(1);
ie_consiste_resp_w	varchar2(1);
ie_consiste_responsavel_w	varchar2(1) := 'N';
ie_passaporte_pac_conv_estab_w	varchar2(1) := 'N';
ie_consiste_cpf_w		varchar2(1) := 'N';
ie_brasileiro_w		nacionalidade.ie_brasileiro%type;

quebra_w				varchar2(10)	:= chr(13)||chr(10);

begin	
	ie_consiste_cpf_w 		:= obter_valor_param_usuario(916, 501, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);	
	ie_obriga_cpf_estrangeiro_w 	:= obter_valor_param_usuario(916, 665, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	ie_consiste_resp_w 		:= obter_valor_param_usuario(916, 718, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);	

	ie_passaporte_pac_conv_estab_w  := Obter_Valor_Conv_Estab(cd_convenio_p, wheb_usuario_pck.get_cd_estabelecimento, 'IE_EXIGE_PASSAPORTE_PACIENTE');
	
	select 	max(a.cd_pessoa_responsavel),
		max(a.cd_pessoa_fisica),
		max(ie_tipo_convenio)
	into	cd_pessoa_resp_w,
		cd_pessoa_fisica_w,
		ie_tipo_convenio_w
	from	atendimento_paciente a
	where 	nr_atendimento = nr_atendimento_p;
	
	if	(ie_tipo_convenio_w is null) then
		select	max(ie_tipo_convenio)
		into	ie_tipo_convenio_w
		from	convenio
		where	cd_convenio	= cd_convenio_p;
	end if;
	
	select 	count(*)
	into	qt_itens_regra_w
	from	regra_consiste_pac_conv
	where	nvl(ie_tipo_atendimento,ie_tipo_atendimento_p) 	= ie_tipo_atendimento_p
	and	nvl(cd_convenio,cd_convenio_p) 			= cd_convenio_p
	and	nvl(ie_tipo_convenio,ie_tipo_convenio_w)	= ie_tipo_convenio_w
	and	ie_resp_atendimento = 'S';
	
	If	(ie_consiste_resp_w = 'N') then
		cd_pessoa_fisica_w := nvl(cd_pessoa_resp_w, cd_pessoa_fisica_w);
	End if;		
	
	If	(ie_obriga_cpf_estrangeiro_w = 'S')
	Or	((qt_itens_regra_w > 0) and
		 (cd_pessoa_fisica_w is not null)) 
	And	(ie_passaporte_pac_conv_estab_w <> 'N') then		

		select	max(a.cd_nacionalidade),
			max(a.nr_cpf),
			max(a.nr_cartao_estrangeiro),
			max(a.nr_reg_geral_estrang),
			max(a.NR_PASSAPORTE)
		into	cd_nacionalidade_w,
			nr_cpf_w,
			nr_cartao_estrangeiro_w,
			nr_reg_geral_estrang_w,
			NR_PASSAPORTE_w
		from	pessoa_fisica a
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
		
		If	(ie_consiste_resp_w = 'S') then
		
			select	nvl(max(ie_brasileiro), 'N')
			into	ie_brasileiro_w
			from 	nacionalidade
			where	cd_nacionalidade = cd_nacionalidade_w;
		
			ie_consiste_responsavel_w := 'N';
			if	(ie_brasileiro_w = 'S' or cd_nacionalidade_w = '10') then
				if	(nr_cpf_w is null) 
				or	(obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'A') < 18) then
					ie_consiste_responsavel_w := 'S';					
				end if;
			elsif	((NR_PASSAPORTE_w is null) and 
				 (nr_reg_geral_estrang_w is null)) 
			or	(obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'A') < 18) then
				ie_consiste_responsavel_w := 'S';
			end if;
			
			If	(ie_consiste_responsavel_w = 'S') then			
				cd_pessoa_fisica_w := nvl(cd_pessoa_resp_w, cd_pessoa_fisica_w);				
				select	max(a.cd_nacionalidade),
					max(a.nr_cpf),
					max(a.nr_cartao_estrangeiro),
					max(a.nr_reg_geral_estrang),
					max(a.NR_PASSAPORTE)
				into	cd_nacionalidade_w,
					nr_cpf_w,
					nr_cartao_estrangeiro_w,
					nr_reg_geral_estrang_w,
					NR_PASSAPORTE_w
				from	pessoa_fisica a
				where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;				
			end if;
		End if;		
		
		select	nvl(max(ie_brasileiro), 'N')
		into	ie_brasileiro_w
		from 	nacionalidade
		where	cd_nacionalidade = cd_nacionalidade_w;
		
		if	(ie_brasileiro_w = 'S' or cd_nacionalidade_w = '10') then
			if	(nr_cpf_w is null) then
				--, o CPF do paciente ou responsável deve ser informado.
				ds_erro_p := ds_erro_p || wheb_mensagem_pck.get_texto(279661);
			end if;
		elsif	(NR_PASSAPORTE_w is null) and 
			(nr_reg_geral_estrang_w is null) then
			if	(cd_pessoa_resp_w is not null)then
				-- Passaporte ou Rg estrangeiro do(a) responsável #@CD_PESSOA_FISICA_P#@ deve ser informado
				ds_erro_p := ds_erro_p || ' ' || wheb_mensagem_pck.get_texto(279663, 'CD_PESSOA_FISICA_P=' || obter_nome_pf(cd_pessoa_fisica_w));		
			else
				-- Passaporte ou Rg estrangeiro do(a) #@CD_PESSOA_FISICA_P#@ deve ser informado
				ds_erro_p := ds_erro_p || ' ' || wheb_mensagem_pck.get_texto(286466, 'CD_PESSOA_FISICA_P=' || obter_nome_pf(cd_pessoa_fisica_w));		
			end if;
		end if;
	elsif (ie_consiste_cpf_w = 'S') then
		select	max(a.nr_cpf)
		into	nr_cpf_w
		from	pessoa_fisica a
		where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
	
		if	(nr_cpf_w is null) then
			--, o CPF do paciente ou responsável deve ser informado.
			ds_erro_p := ds_erro_p || wheb_mensagem_pck.get_texto(279661);
		end if;
	end if;
	ds_erro_p := replace(substr(ds_erro_p,2,254),',',quebra_w);	
	
end verifica_cpf_pf_atendimento;
/
