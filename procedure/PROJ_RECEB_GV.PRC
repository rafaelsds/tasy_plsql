create or replace
procedure proj_receb_gv(	nr_sequencia_p		number,
				nm_usuario_p		varchar2) is

begin
update	via_viagem
set	dt_receb_proj	= sysdate,
	nm_usuario_receb_proj = nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;

update	via_relat_desp
set	dt_recebimento = sysdate
where	nr_seq_viagem = nr_sequencia_p;

commit;

end proj_receb_gv;
/