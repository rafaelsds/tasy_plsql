create or replace
procedure gerar_lote_processo_gedipa(
				nr_seq_horario_p	number,
				nr_seq_motivo_p		number,
				nm_usuario_p		Varchar2,
				nr_seq_processo_p		number) is

nr_sequencia_w			number(10);
nr_prescricao_w			number(15);
cd_material_w			number(10);
nr_seq_mat_hor_w		number(10);
cd_unidade_medida_w		varchar2(30);
qt_dispensar_w			number(18,6);
nr_seq_turno_w			number(10);
cd_setor_atendimento_w		number(5);
cd_setor_regra_w		number(5);
dt_horario_w			date;
hr_inicio_turno_w		varchar2(5);
hr_hora_w			varchar2(5);
dt_inicio_turno_w		date;
cd_estabelecimento_w		number(4);
ie_urgente_w			varchar2(1);
ds_maq_user_w 			varchar2(80);
cd_perfil_ativo_w		number(5);
nr_seq_classif_w		number(10);
qt_min_antes_atend_w		number(10);
qt_min_receb_setor_w		number(10);
qt_min_entr_setor_w		number(10);
qt_min_disp_farm_w		number(10);
qt_min_atend_farm_w		number(10);
qt_min_inicio_atend_w		number(10);
cd_tipo_baixa_w			number(3);
ie_conta_paciente_w		varchar2(1);
ie_atualiza_estoque_w		varchar2(1);
cd_local_estoque_w		number(4);
cd_local_estoque_ant_w		number(4) := 0;
ie_hora_antes_w			varchar2(1);
qt_prioridade_w			number(3);
ie_gera_lote_orig_w		varchar2(1);
ie_gera_lote_medic_pac_w	varchar2(1);
ie_gerar_lote_area_w		varchar2(1);
ie_termolabil_w			varchar2(1);
nr_atendimento_w		number(15);
qt_existe_regra_w		number(10);
ie_gerar_lote_unico_w		number(10) := 0;
nr_seq_prescr_mat_w		number(10);
ie_alto_risco_w			varchar2(1);
ds_motivo_w			varchar2(255);
ds_erro_w			varchar2(2000);
ie_motivo_prescricao_w		varchar2(5);
ie_regra_disp_w			varchar2(1);
qt_total_dispensar_w		number(18,6);
qt_dispensar_hor_w		number(15,4);
nr_ocorrencia_w			number(15,4);
ie_gerar_lote_w			varchar2(2);
nr_seq_processo_w		varchar2(20);
nr_seq_empresa_w		empresa_integracao.nr_sequencia%type;
ie_existe_w			varchar2(1) := 'N';
ie_quimio_w			varchar2(1);
ds_param_integ_hl7_w		varchar2(4000) := '';

cursor c01 is
	select  b.cd_material,
		b.nr_sequencia,
		b.nr_seq_material,
		b.cd_unidade_medida,
		b.qt_dispensar,
		b.qt_dispensar_hor,
		nvl(b.nr_seq_turno,-1),
		b.dt_horario,
		to_char(b.dt_horario,'hh24:mi'),
		nvl(a.cd_estabelecimento,1),
		b.ie_urgente,
		b.nr_seq_classif,
		nvl(b.cd_local_estoque,nvl(c.cd_local_estoque,a.cd_local_estoque)) cd_local,
		c.ie_regra_disp,
		b.nr_seq_processo 
	from	material x,
		prescr_mat_hor  b,
		prescr_material c,
		prescr_medica   a
	where	a.nr_prescricao = b.nr_prescricao
	and	((ie_gerar_lote_w = 'SI' and b.nr_sequencia = nr_seq_horario_p) 
	or	(ie_gerar_lote_w = 'ST' and b.nr_seq_processo = nr_seq_processo_p))
	and	x.cd_material	= c.cd_material
	and	a.nr_prescricao	= c.nr_prescricao
	and	c.nr_sequencia	= b.nr_seq_material
	and	nvl(x.ie_gerar_lote,'S') = 'S'
	and	nvl(b.ie_situacao,'A') <> 'I'
	and	nvl(c.ie_suspenso,'N') <> 'S'
	and	nvl(b.ie_padronizado,'S') = 'S'	
	and	a.dt_suspensao is null
	and	c.cd_motivo_baixa = 0
	and	c.dt_baixa is null
	and	b.nr_seq_turno is not null
	and	((c.ie_regra_disp = 'E') or (nvl(b.qt_dispensar_hor,0) > 0))
	and	((c.ie_regra_disp = 'E') or (nvl(b.ie_gerar_lote,'S') = 'S'))
	and	nvl(c.ie_gerar_lote,'S') = 'S'
	and	b.nr_seq_lote is null
	and	obter_se_setor_lote(a.cd_setor_atendimento) = 'S'
	and	obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S'
	and	((ie_gera_lote_orig_w = 'N') or (nvl(b.ie_aprazado,'N') = 'S'))
	and	((ie_gera_lote_medic_pac_w = 'S') or ((ie_gera_lote_medic_pac_w = 'N') and (nvl(ie_medicacao_paciente,'N') = 'N')))
	and	((ie_gerar_lote_area_w = 'S') or (b.nr_seq_area_prep is null))
	and	((nvl(ie_termolabil_w,'N') = 'N') or ((nvl(ie_termolabil_w,'N') = 'S') and (nvl(x.ie_termolabil,'N') = 'N')))
	and	((nvl(ie_alto_risco_w,'N') = 'N') or ((nvl(ie_alto_risco_w,'N') = 'S') and (obter_se_material_risco(a.cd_setor_atendimento,b.cd_material) = 'N')))
	order by cd_local,
		dt_horario,
		nr_seq_classif;

cursor c02 is
	select	to_char(b.hr_inicial,'hh24:mi')
	from	regra_turno_disp_param b,
		regra_turno_disp a
	where	a.nr_sequencia = b.nr_seq_turno
	and	a.cd_estabelecimento = cd_estabelecimento_w
	and	a.nr_sequencia = nr_seq_turno_w
	and	(nvl(b.cd_setor_atendimento,cd_setor_atendimento_w) = cd_setor_atendimento_w)
	order by nvl(b.cd_setor_atendimento,0),
		to_char(b.hr_inicial,'hh24:mi');

cursor c03 is
	select	cd_setor_atendimento,
		nr_seq_turno,
		nr_seq_classif,
		qt_min_antes_atend,    
		qt_min_receb_setor,
		qt_min_entr_setor,
		qt_min_disp_farm,
		qt_min_atend_farm,
		qt_min_inicio_atend,
		nvl(ie_hora_antes,'H')
	from	regra_tempo_disp
	where	cd_estabelecimento = cd_estabelecimento_w
	and	nvl(ie_situacao, 'A') = 'A'
	order by nvl(nr_seq_classif,0),
		nvl(nr_seq_turno,0),
		nvl(cd_setor_atendimento,0);

cursor c04 is
	select	cd_tipo_baixa
	from	regra_disp_lote_farm
	where	((ie_motivo_prescricao = ie_motivo_prescricao_w) or (ie_motivo_prescricao is null))
	and		sysdate between to_date(to_char(sysdate,'dd/mm/yyyy')||' '||to_char(nvl(dt_hora_inicio,sysdate),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') and
							to_date(to_char(sysdate,'dd/mm/yyyy')||' '||to_char(nvl(dt_hora_fim,sysdate),'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
	and		nvl(cd_setor_atendimento,cd_setor_atendimento_w) = cd_setor_atendimento_w
	and		trunc(sysdate) between trunc(dt_inicio_vigencia) and trunc(nvl(dt_fim_vigencia,sysdate))
	and		ie_situacao = 'A'
	and 	((ie_quimio_w <> 'S' and nvl(ie_quimioterapicas,'N') <> 'S') or (ie_quimio_w = 'S'))
	order by decode(ie_quimioterapicas,'S','Z',decode(ie_quimioterapicas,'N','C','B')),
		nvl(cd_setor_atendimento,0), NVL(ie_motivo_prescricao,0);
	

begin

ie_gerar_lote_w := Obter_Valor_Param_Usuario(3112,54,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento);

begin
select	substr(obter_inf_sessao(0) ||' - ' || obter_inf_sessao(1),1,80)
into	ds_maq_user_w	
from	dual;

cd_perfil_ativo_w := obter_perfil_ativo;

select	nvl(max(nr_prescricao),0)
into	nr_prescricao_w
from	prescr_mat_hor
where	nr_sequencia = nr_seq_horario_p;

select 	nvl(max('S'),'N') 
into	ie_quimio_w
from	paciente_atendimento
where	nr_prescricao = nr_prescricao_w;

select	max(cd_setor_atendimento),
		max(cd_estabelecimento),
		max(nr_atendimento),
		max(ie_motivo_prescricao)
into	cd_setor_atendimento_w,
		cd_estabelecimento_w,
		nr_atendimento_w,
		ie_motivo_prescricao_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_w;

select	nvl(max(ie_gerar_lote_area),'S'),
	nvl(max(ie_gerar_termo_separado),'S'),
	nvl(max(ie_gerar_alto_risco_sep),'S')
into	ie_gerar_lote_area_w,
	ie_termolabil_w,
	ie_alto_risco_w
from	parametros_farmacia
where	cd_estabelecimento = cd_estabelecimento_w;

ie_gera_lote_orig_w      := nvl(obter_valor_param_usuario(1113, 138, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');
ie_gera_lote_medic_pac_w := nvl(obter_valor_param_usuario(924, 389, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N');

open c04;
loop
fetch c04 into
	cd_tipo_baixa_w;
exit when c04%notfound;
	begin
	cd_tipo_baixa_w := cd_tipo_baixa_w;
	end;
end loop;
close c04;

if	(cd_tipo_baixa_w is not null) then
	select	max(ie_conta_paciente),
		max(ie_atualiza_estoque)
	into	ie_conta_paciente_w,
		ie_atualiza_estoque_w
	from	tipo_baixa_prescricao
	where	cd_tipo_baixa = cd_tipo_baixa_w
	and	ie_prescricao_devolucao = 'P';
end if;

open c01;
loop
fetch c01 into
	cd_material_w,
	nr_seq_mat_hor_w,
	nr_seq_prescr_mat_w,
	cd_unidade_medida_w,
	qt_dispensar_w,
	qt_dispensar_hor_w,
	nr_seq_turno_w,
	dt_horario_w,
	hr_hora_w,
	cd_estabelecimento_w,
	ie_urgente_w,
	nr_seq_classif_w,
	cd_local_estoque_w,
	ie_regra_disp_w,
	nr_seq_processo_w;
exit when c01%notfound;
	begin
	/*
	insert into log_xxxtasy (dt_atualizacao,nm_usuario,cd_log,ds_log) values (sysdate,nm_usuario_p,55821,
			' gerar_lote_processo_gedipa ' ||
			' nr_seq_turno_w = '|| nr_seq_turno_w||
			' nr_seq_classif_w = '|| nr_seq_classif_w||
			' trunc(dt_horario_w,''dd'') = '|| trunc(dt_horario_w,'dd')||
			' cd_local_estoque_w = '|| cd_local_estoque_w||
			' cd_local_estoque_ant_w = '|| cd_local_estoque_ant_w||
			' nr_seq_mat_hor_w = '|| nr_seq_mat_hor_w||
			' nr_prescricao_w = '|| nr_prescricao_w||
			' nr_seq_lote =	'|| nr_sequencia_w||
			' ie_gerar_lote_unico_w = '|| ie_gerar_lote_unico_w);
	commit;
	*/
	
	if	(nvl(ie_regra_disp_w,'S') = 'E') then
		
		update	prescr_mat_hor
		set	ie_gerar_lote = 'S'
		where	nr_sequencia = nr_seq_mat_hor_w;
		
		select	nvl(max(qt_total_dispensar),0),
			nvl(max(nr_ocorrencia),0)
		into	qt_total_dispensar_w,
			nr_ocorrencia_w
		from	prescr_material
		where	nr_prescricao = nr_prescricao_w
		and	nr_sequencia = nr_seq_prescr_mat_w;
		
		if	(qt_total_dispensar_w > 0) and
			(nr_ocorrencia_w is not null) then		
			qt_dispensar_hor_w := nvl(dividir(qt_total_dispensar_w, nr_ocorrencia_w),0);
			
			if	(qt_dispensar_hor_w > 0) then				
				update	prescr_mat_hor
				set	qt_dispensar_hor = qt_dispensar_hor_w
				where	nr_sequencia = nr_seq_mat_hor_w;
			end if;
		end if;
		
	end if;
	
	if	(ie_gerar_lote_unico_w = 0) or
		((cd_local_estoque_w <> cd_local_estoque_ant_w) and (cd_local_estoque_ant_w <> 0)) then
		begin

		open c02;
		loop
		fetch c02 into
			hr_inicio_turno_w;
		exit when c02%notfound;
			begin
			hr_inicio_turno_w	:= hr_inicio_turno_w;
			end;
		end loop;
		close c02;

		select	nvl(max(qt_prioridade),999)
		into	qt_prioridade_w
		from	classif_lote_disp_far
		where	nr_sequencia	= nr_seq_classif_w;
		
		if	(hr_hora_w < hr_inicio_turno_w) then
			dt_inicio_turno_w 	:= to_date(to_char(dt_horario_w - 1,'dd/mm/yyyy')||' '||replace(hr_inicio_turno_w,'24:','00:') ||':00','dd/mm/yyyy hh24:mi:ss');
		else
			dt_inicio_turno_w	:= to_date(to_char(dt_horario_w,'dd/mm/yyyy')||' '||replace(hr_inicio_turno_w,'24:','00:') ||':00','dd/mm/yyyy hh24:mi:ss');
		end if;
		
		select	ap_lote_seq.nextval
		into	nr_sequencia_w
		from 	dual;
		
		insert into ap_lote
			(nr_sequencia,
			dt_inicio_turno,
			dt_prim_horario,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_geracao_lote,
			nr_prescricao,
			nr_seq_turno,
			ie_status_lote,
			cd_setor_atendimento,
			nr_seq_classif,
			nm_usuario_geracao,
			ds_maquina_geracao,
			cd_perfil_geracao,
			qt_min_atraso_inicio_atend,    
			qt_min_atraso_atend,
			qt_min_atraso_disp,
			qt_min_atraso_entrega,
			qt_min_atraso_receb,
			cd_tipo_baixa,
			ie_conta_paciente,
			ie_atualiza_estoque,
			cd_local_estoque,
			qt_prioridade,
			ie_origem_lote)
		values	(nr_sequencia_w,
			dt_inicio_turno_w,
			dt_horario_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_prescricao_w,
			nr_seq_turno_w,
			'G',
			cd_setor_atendimento_w,
			nr_seq_classif_w,
			nm_usuario_p,
			ds_maq_user_w,
			cd_perfil_ativo_w,
			0,
			0,
			0,
			0,
			0,
			cd_tipo_baixa_w,
			ie_conta_paciente_w,
			ie_atualiza_estoque_w,
			cd_local_estoque_w,
			qt_prioridade_w,
			'GLPG');
		
		select	substr(ds_motivo,1,250)
		into	ds_motivo_w
		from	motivo_geracao_lote_gedipa
		where	nr_sequencia = nr_seq_motivo_p;
		
		insert into ap_lote_historico(
			nr_sequencia,				dt_atualizacao,
			nm_usuario,				nr_seq_lote,
			ds_evento,				ds_log)
		values(	ap_lote_historico_seq.nextval,		sysdate,
			nm_usuario_p,				nr_sequencia_w,
			wheb_mensagem_pck.get_Texto(311470),	ds_motivo_w);
		
		end;
	end if;

	ie_gerar_lote_unico_w	:= nr_sequencia_w;
	cd_local_estoque_ant_w	:= cd_local_estoque_w;

	insert into ap_lote_item(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_lote,
		nr_seq_mat_hor,
		ie_prescrito,
		qt_dispensar,
		qt_total_dispensar,
		cd_material,
		cd_unidade_medida,
		ie_urgente)
	values (ap_lote_item_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_w,
		nr_seq_mat_hor_w,
		'S',
		nvl(qt_dispensar_hor_w,0),
		qt_dispensar_w,
		cd_material_w,
		cd_unidade_medida_w,
		ie_urgente_w);
		
	update	prescr_mat_hor
	set	nr_seq_lote = nr_sequencia_w
	where	nr_sequencia = nr_seq_mat_hor_w;

	end;
end loop;
close c01;

open c03;
loop
fetch c03 into
	cd_setor_regra_w,
	nr_seq_turno_w,
	nr_seq_classif_w,
	qt_min_antes_atend_w,
	qt_min_receb_setor_w,
	qt_min_entr_setor_w,
	qt_min_disp_farm_w,
	qt_min_atend_farm_w,
	qt_min_inicio_atend_w,
	ie_hora_antes_w;
exit when c03%notfound;
	begin
	if	(ie_hora_antes_w = 'H') then
		begin
		update	ap_lote
		set	dt_atend_lote		= round(dt_prim_horario - dividir(qt_min_antes_atend_w,1440),'mi'),
			dt_limite_inicio_atend	= round(dt_prim_horario - dividir(qt_min_inicio_atend_w,1440),'mi'),
			dt_limite_atend		= round(dt_prim_horario - dividir(qt_min_atend_farm_w,1440),'mi'),
			dt_limite_disp_farm	= round(dt_prim_horario - dividir(qt_min_disp_farm_w,1440),'mi'),
			dt_limite_entrega_setor	= round(dt_prim_horario - dividir(qt_min_entr_setor_w,1440),'mi'),
			dt_limite_receb_setor	= round(dt_prim_horario - dividir(qt_min_receb_setor_w,1440),'mi')
		where	nr_prescricao		= nr_prescricao_w
		and	((cd_setor_regra_w is null) or (cd_setor_regra_w = cd_setor_atendimento))
		and	((nr_seq_turno_w is null) or (nr_seq_turno_w = nr_seq_turno))
		and	((nr_seq_classif_w is null) or (nr_seq_classif_w = nr_seq_classif));
		end;
	elsif	(ie_hora_antes_w = 'I') then
		begin
		update	ap_lote
		set	dt_atend_lote		= round(dt_inicio_turno - dividir(qt_min_antes_atend_w,1440),'mi'),
			dt_limite_inicio_atend	= round(dt_prim_horario - dividir(qt_min_inicio_atend_w,1440),'mi'),
			dt_limite_atend		= round(dt_prim_horario - dividir(qt_min_atend_farm_w,1440),'mi'),
			dt_limite_disp_farm	= round(dt_prim_horario - dividir(qt_min_disp_farm_w,1440),'mi'),
			dt_limite_entrega_setor	= round(dt_prim_horario - dividir(qt_min_entr_setor_w,1440),'mi'),
			dt_limite_receb_setor	= round(dt_prim_horario - dividir(qt_min_receb_setor_w,1440),'mi')
		where	nr_prescricao		= nr_prescricao_w
		and	((cd_setor_regra_w is null) or (cd_setor_regra_w = cd_setor_atendimento))
		and	((nr_seq_turno_w is null) or (nr_seq_turno_w = nr_seq_turno))
		and	((nr_seq_classif_w is null) or (nr_seq_classif_w = nr_seq_classif));
		end;
	elsif	(ie_hora_antes_w = 'T') then
		begin
		update	ap_lote
		set	dt_atend_lote		= decode(to_char(round(dt_inicio_turno - dividir(qt_min_antes_atend_w,1440),'mi'), 'hh24:mi:ss'), '00:00:00', --os186690
			round(dt_inicio_turno - dividir(qt_min_antes_atend_w,1440),'mi') + 1/86400, round(dt_inicio_turno - dividir(qt_min_antes_atend_w,1440),'mi')),
			dt_limite_inicio_atend	= decode(to_char(round(dt_inicio_turno - dividir(qt_min_inicio_atend_w,1440),'mi'), 'hh24:mi:ss'), '00:00:00',
			round(dt_inicio_turno - dividir(qt_min_inicio_atend_w,1440),'mi') + 1/86400, round(dt_inicio_turno - dividir(qt_min_inicio_atend_w,1440),'mi')), 
			dt_limite_atend		= round(dt_inicio_turno - dividir(qt_min_atend_farm_w,1440),'mi'),
			dt_limite_disp_farm	= round(dt_inicio_turno - dividir(qt_min_disp_farm_w,1440),'mi'),
			dt_limite_entrega_setor	= round(dt_inicio_turno - dividir(qt_min_entr_setor_w,1440),'mi'),
			dt_limite_receb_setor	= round(dt_inicio_turno - dividir(qt_min_receb_setor_w,1440),'mi')
		where	nr_prescricao		= nr_prescricao_w
		and	((cd_setor_regra_w is null) or (cd_setor_regra_w = cd_setor_atendimento))
		and	((nr_seq_turno_w is null) or (nr_seq_turno_w = nr_seq_turno))
		and	((nr_seq_classif_w is null) or (nr_seq_classif_w = nr_seq_classif));
		end;
	end if;
	end;
end loop;
close c03;

-- Rotina para envio da prescricao para integracao
begin
select	nr_sequencia
into	nr_seq_empresa_w
from	empresa_integracao
where	upper(nm_empresa) = 'SWISSLOG';

select	'S'
into	ie_existe_w
from	far_setores_integracao
where	nr_seq_empresa_int = nr_seq_empresa_w
and	cd_setor_atendimento = cd_setor_atendimento_w;
exception
when others then
	nr_seq_empresa_w := 0;
	ie_existe_w := 'N';
end;

if	(ie_existe_w = 'S') then
	ds_param_integ_hl7_w := 'nr_prescricao=' || nr_prescricao_w || obter_separador_bv || 
				'nr_atendimento=' || nr_atendimento_w || obter_separador_bv ||
				'ie_aprazado=' || 'GPMH' || nr_seq_horario_p || obter_separador_bv ||
				'ie_origem=' || 'GLAP' || obter_separador_bv ||
				'nr_seq_horario_p=' || null || obter_separador_bv;
	
	-- Envio das informacoes do paciente internado para a integracao
	swisslog_gerar_integracao(434, ds_param_integ_hl7_w);
	
	update	ap_lote 
	set 	ie_integracao = 'W'
	where 	nr_prescricao = nr_prescricao_w;
	
end if;
-- Rotina para envio da prescricao para integracao

exception
when others then
	ds_erro_w := substr(sqlerrm(sqlcode),1,2000);
	/* insert into log_XXtasy(
		dt_atualizacao,
		nm_usuario,
		cd_log,
		ds_log)
	values(	sysdate,
		nm_usuario_p,
		4141,
		ds_erro_w);
	commit;
	*/
end;

commit;

end gerar_lote_processo_gedipa;
/
