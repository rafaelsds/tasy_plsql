create or replace
procedure gerar_agenda_proced_kit(	nr_seq_agenda_p 		number,
                                    cd_pessoa_fisica_p 	Varchar2,
                                    nm_usuario_p		   varchar2,
                                    ds_erro_p 	out 	   varchar2) is 

ie_ordem_w		      varchar2(1);
cd_procedimento_w	   number(15);
ie_origem_proced_w	number(10);
cd_kit_material_w		number(5);
nr_sequencia_w			number(10);
nr_seq_kit_w			number(10);	
ie_gerado_w			   varchar2(1) := 'N';
ds_erro_w			   varchar2(255);
ds_erro_ww			   varchar2(255);
ie_commit_w			   varchar2(1) := 'N';
ie_estab_agenda_w	   varchar2(1);	
cd_estabelecimento_w	number(4);
qt_idade_w			   varchar(255);
				
Cursor C01 is
	select  	1 ie_ordem,
		a.cd_procedimento,
		a.ie_origem_proced,
		b.cd_kit_material
	from    	kit_material k,
		procedimento b,
		agenda_paciente a
	where    	a.cd_procedimento    = b.cd_procedimento
	and      	a.ie_origem_proced   = b.ie_origem_proced
	and      	a.nr_sequencia	      = nr_seq_agenda_p
	and      	b.cd_kit_material    = k.cd_kit_material
	and      	k.ie_situacao        = 'A'
	and      	a.nr_seq_proc_interno is null
	union
	select  	2 ie_ordem,
		a.cd_procedimento,
		a.ie_origem_proced,
		b.cd_kit_material
	from     	kit_material k,
		procedimento b,
		agenda_paciente_proc a
	where    	a.cd_procedimento    = b.cd_procedimento
	and      	a.ie_origem_proced   = b.ie_origem_proced
	and      	a.nr_sequencia	      = nr_seq_agenda_p
	and      	b.cd_kit_material    = k.cd_kit_material
	and      	k.ie_situacao        = 'A'
	and      	a.nr_seq_proc_interno is null
	union
	select  	1 ie_ordem,
		a.cd_procedimento,
		a.ie_origem_proced,
		b.cd_kit_material
	from     	kit_material k,
		proc_interno b,
		agenda_paciente a
	where    	a.nr_seq_proc_interno= b.nr_sequencia
	and      	a.nr_sequencia	      = nr_seq_agenda_p
	and      	b.cd_kit_material    = k.cd_kit_material
	and      	k.ie_situacao        = 'A'
	union
	select  	2 ie_ordem,
		a.cd_procedimento,
		a.ie_origem_proced,
		b.cd_kit_material
	from     	kit_material k,
		proc_interno b,
		agenda_paciente_proc a
	where    	a.nr_seq_proc_interno= b.nr_sequencia
	and      	a.nr_sequencia	      = nr_seq_agenda_p
	and      	b.cd_kit_material    = k.cd_kit_material
	and      	k.ie_situacao        = 'A'
	union
	select  	1 ie_ordem,
            a.cd_procedimento,
            a.ie_origem_proced,
            c.cd_kit_material
	from     kit_material k,
            proc_interno b,
            proc_interno_kit c,
            agenda_paciente a
	where   a.nr_seq_proc_interno= b.nr_sequencia
	and     c.nr_seq_proc_interno= b.nr_sequencia
	and     a.nr_sequencia	     = nr_seq_agenda_p
	and     ((c.cd_medico        = a.cd_medico) or (c.cd_medico is null) or (a.cd_medico is null))
	and     c.cd_kit_material    = k.cd_kit_material
	and     k.ie_situacao        = 'A'
	and	  c.cd_estabelecimento = cd_estabelecimento_w
   and     qt_idade_w between obter_idade_kit_proced(c.nr_sequencia,'MIN') 
                          and obter_idade_kit_proced(c.nr_sequencia,'MAX')
	union
	select  	2 ie_ordem,
            a.cd_procedimento,
            a.ie_origem_proced,
            c.cd_kit_material
	from     kit_material k,
            proc_interno b,
            proc_interno_kit c,
            agenda_paciente_proc a
	where   a.nr_seq_proc_interno = b.nr_sequencia
	and     c.nr_seq_proc_interno = b.nr_sequencia
	and     ((c.cd_medico         = a.cd_medico) or (c.cd_medico is null) or (a.cd_medico is null))
	and     a.nr_sequencia	      = nr_seq_agenda_p
	and     c.cd_kit_material     = k.cd_kit_material
	and     k.ie_situacao         = 'A'
	and	  c.cd_estabelecimento  = cd_estabelecimento_w
   and     qt_idade_w            between obter_idade_kit_proced(c.nr_sequencia,'MIN') 
                                     and obter_idade_kit_proced(c.nr_sequencia,'MAX')
	order by 1;

	
begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
Obter_Param_Usuario(871, 531, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w,ie_estab_agenda_w);


if	(ie_estab_agenda_w = 'S') then
	cd_estabelecimento_w := obter_estab_agenda_paciente(nr_seq_agenda_p);
end if;	

begin
select 	max(nvl(obter_idade_pf(a.cd_pessoa_fisica, sysdate, 'DIA'),0))
into     qt_idade_w
from     agenda_paciente a 
where    a.nr_sequencia = nr_seq_agenda_p;
exception
   when others then
   qt_idade_w := 0;
end;

open C01;
loop
fetch C01 into	
	ie_ordem_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_kit_material_w ;
exit when C01%notfound;
	begin	
	ds_erro_ww := null;
	if	(nvl(cd_kit_material_w,0) > 0) then
		consistir_pedido_kit(cd_kit_material_w,nm_usuario_p,nr_seq_agenda_p,ds_erro_ww);
	end if;	
	
	if	(ds_erro_ww is null) and
		(nvl(cd_kit_material_w,0) > 0) and (ie_gerado_w = 'N')	then	
		begin	
		
		select	agenda_pac_pedido_seq.nextval
		into	nr_sequencia_w
		from	dual;

		insert 	into agenda_pac_pedido(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_agenda,
					dt_pedido,
					dt_liberacao,
					cd_pessoa_resp)
			values	(	nr_sequencia_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_agenda_p,
					sysdate,
					null,
					cd_pessoa_fisica_p );
					
		ie_gerado_w := 'S';
		ie_commit_w := 'S';
		end;
	end if;
	
	
		
	if (ds_erro_ww is null) then
	
		select	agenda_pac_pedido_kit_seq.nextval
		into	nr_seq_kit_w
		from	dual;
	
		insert 	into agenda_pac_pedido_kit(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_pedido,
				cd_kit_material,
				cd_procedimento,
				ie_origem_proced)
		values	(	nr_seq_kit_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_sequencia_w,
				cd_kit_material_w,
				cd_procedimento_w,
				ie_origem_proced_w);
				
		ie_commit_w := 'S';
	end if;
	end;
end loop;
close C01;

if	(nvl(cd_kit_material_w,0) = 0) then
	ds_erro_w := wheb_mensagem_pck.get_texto(278634); 
end if;

ds_erro_p :=  ds_erro_w;

if	(ie_commit_w = 'S') then
	commit;
end if;
end gerar_agenda_proced_kit;
/
