create or replace
procedure obter_dados_eis_etapa_conta(
			nr_interno_conta_p			number,
			nr_seq_etapa_p		out	number,
			ds_observacao_p		out	varchar2,
			ie_muda_status_conta_p	out	varchar2,
			cd_setor_atendimento_p	out	number,
			cd_pessoa_fisica_p		out	varchar2,
			ds_etapa_p		out	varchar2,
			nr_seq_motivo_dev_p	out	number,
			dt_ultima_etapa_p		out	date,
			nm_ultimo_usuario_p	out	varchar2,
			nm_ultimo_usu_receb_p	out	varchar2,
			nr_seq_classif_p	out	number) is 
			
ds_etapa_w			varchar2(80)	:= null;
nr_seq_etapa_w			number(10,0)	:= null;
ds_retorno_w			varchar2(255)	:= null;
ds_observacao_w			varchar2(2000);
ie_muda_status_conta_w		varchar2(01);
cd_setor_atendimento_w		number(5);
cd_pessoa_fisica_w		varchar2(10);
nr_seq_motivo_dev_w		varchar2(80);	
dt_etapa_w			date;	
nm_usuario_w			varchar2(15);
nm_usuario_recebimento_w		varchar2(15);
nr_seq_classif_w		fatur_etapa.nr_seq_classif%type;
			
cursor	c01 is
	select	a.nr_seq_etapa,
		b.ds_etapa,
		substr(a.ds_observacao,1,80),
		nvl(ie_muda_status_conta, 'S'),
		a.cd_setor_atendimento,
		a.cd_pessoa_fisica,
		a.nr_seq_motivo_dev,
		a.dt_etapa,
		a.nm_usuario,
		a.nm_usuario_recebimento,
		b.nr_seq_classif
	from	fatur_etapa b,
		conta_paciente_etapa a
	where	a.nr_seq_etapa		= b.nr_sequencia
	and	a.nr_interno_conta		= nr_interno_conta_p
	and	nvl(b.ie_situacao,'A')	= 'A'
	and	a.dt_etapa		=	(select	/*+ index(x conpaet_conpaci_fk_i) */
							max(x.dt_etapa)
   						 from	conta_paciente_etapa x
						 where   x.nr_interno_conta = nr_interno_conta_p)
	order by a.dt_etapa,
		a.dt_fim_etapa; --lhalves OS 867686 em 21/05/2015 -0 incluido o orde by tamb�m pelo fim da etapa, para que caso exista 2 etapas com mesma data, considere a sem data fim por �ltimo, pois esta ser� a etapa atual da conta
begin

open	c01;
loop
fetch	c01 into
	nr_seq_etapa_w,
	ds_etapa_w,
	ds_observacao_w,
	ie_muda_status_conta_w,
	cd_setor_atendimento_w,
	cd_pessoa_fisica_w,
	nr_seq_motivo_dev_w,
	dt_etapa_w,
	nm_usuario_w,
	nm_usuario_recebimento_w,
	nr_seq_classif_w;
exit when c01%notfound;
end loop;
close c01;

nr_seq_etapa_p		:= nr_seq_etapa_w;
ds_observacao_p		:= ds_observacao_w;
ie_muda_status_conta_p	:= ie_muda_status_conta_w;
cd_setor_atendimento_p	:= cd_setor_atendimento_w;
cd_pessoa_fisica_p		:= cd_pessoa_fisica_w;
ds_etapa_p		:= ds_etapa_w;
nr_seq_motivo_dev_p	:= nr_seq_motivo_dev_w;
dt_ultima_etapa_p		:= dt_etapa_w;
nm_ultimo_usuario_p	:= nm_usuario_w;
nm_ultimo_usu_receb_p	:= nm_usuario_recebimento_w;
nr_seq_classif_p	:= nr_seq_classif_w;

end obter_dados_eis_etapa_conta;
/