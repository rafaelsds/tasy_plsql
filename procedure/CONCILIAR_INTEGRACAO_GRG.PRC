create or replace procedure conciliar_integracao_grg(nr_seq_item_p in number,
                                                        ie_opcao_p in varchar2) is

nr_seq_lote_w		imp_resp_recurso.nr_sequencia%type;
ie_status_w varchar2(2) default 'P';

begin

    if(ie_opcao_p = 'V') then  --Se clicado na opcao "Validar", ira buscar o item caso ele esteja com o status Pendente.
        ie_status_w := 'P';
    else if(ie_opcao_p = 'C') then --Se clicado na opcao "Conciliar", ira buscar o item caso ele esteja com o status Validado.
        ie_status_w := 'V';
        end if;
    end if;
    
	if (nr_seq_item_p is not null) and (nr_seq_item_p > 0) then

		select	max(d.nr_sequencia)
		into	nr_seq_lote_w
		from	imp_resp_recurso_prot a,
			imp_resp_recurso_guia b,
			imp_resp_recurso_item c,
			imp_resp_recurso d
		where 	c.nr_sequencia 		= nr_seq_item_p
		and	b.nr_sequencia 		= c.nr_seq_res_rec_guia
		and	a.nr_sequencia 		= b.nr_seq_res_rec_prot
		and	a.nr_seq_resp_recurso	= d.nr_sequencia
		and	nvl(a.ie_status, 'P')	= ie_status_w;

		if (nvl(nr_seq_lote_w,0) > 0) then
			conciliar_integr_resp_rec_pck.executar(nr_seq_lote_w, ie_opcao_p);
		end if;

	end if;
end conciliar_integracao_grg;
/