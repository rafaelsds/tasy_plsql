create or replace
procedure gerar_retorno_spc_sirc(
			ie_tipo_p		varchar2,
			nr_seq_lote_p		varchar2,
			nm_usuario_p		varchar2) is 

			

qt_w			number(10);
ie_tipo_lote_w		lote_orgao_cobranca.ie_tipo_lote%type;
nr_seq_orgao_cobr_w	number(10);
nr_seq_corbanca_w	number(10);
ds_mensagem_w		varchar2(255);
tipo_lote_arq_w		varchar2(30);

Cursor C01 is
	select	somente_numero(substr(ds_conteudo,10,20)) nr_sequencia,
		somente_numero(substr(ds_conteudo,54,62)) dt_vencimento,
		substr(ds_conteudo,92,93) ie_tipo_operacao
	from	w_retorno_orgao_cobr
	where	nm_usuario = nm_usuario_p
	and 	nr_seq_lote = nr_seq_lote_p;

vet01	C01%RowType;
begin

select	max(ie_tipo_lote)
into 	ie_tipo_lote_w
from	lote_orgao_cobranca
where	nr_sequencia = nr_seq_lote_p;

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	
	select decode(vet01.ie_tipo_operacao,'I','inclus�o','exclus�o')
	into tipo_lote_arq_w
	from dual;
	
	if	(ie_tipo_lote_w <> vet01.ie_tipo_operacao) then
		ds_mensagem_w := wheb_mensagem_pck.get_texto(338796, 'ie_tipo_operacao=' || tipo_lote_arq_w);
		wheb_mensagem_pck.exibir_mensagem_abort(ds_mensagem_w);
	end if;

	if	(ie_tipo_p = 'C') then --cheque

		if	(ie_tipo_lote_w = 'E') then
			
			select	count(*),
				max(nr_seq_cobranca),
				max(nr_seq_orgao_cobr)
			into	qt_w,
				nr_seq_corbanca_w,
				nr_seq_orgao_cobr_w
			from	cheque_cr_orgao_cobr
			where	nr_seq_cheque = vet01.nr_sequencia
			and 	nr_seq_lote_exc = nr_seq_lote_p;
		
			if	(qt_w > 0) then
			
				update	cheque_cr_orgao_cobr 
				set	dt_exclusao 	= to_date(vet01.dt_vencimento,'dd/MM/yyyy')
				where	nr_seq_cheque 	= vet01.nr_sequencia
				and 	nr_seq_orgao_cobr =  nr_seq_orgao_cobr_w;
			
				update	cobranca_orgao 
				set	dt_retirada = sysdate 
				where	nr_seq_cobranca = nr_seq_corbanca_w
				and 	nr_seq_orgao = nr_seq_orgao_cobr_w;
			
			end if;
		
		elsif	(ie_tipo_lote_w = 'I') then
			
			select	count(*),
				max(nr_seq_cobranca),
				max(nr_seq_orgao_cobr)
			into	qt_w,
				nr_seq_corbanca_w,
				nr_seq_orgao_cobr_w
			from	cheque_cr_orgao_cobr
			where	nr_seq_cheque = vet01.nr_sequencia
			and 	nr_seq_lote = nr_seq_lote_p;
			
			if	(qt_w > 0) then
			
				update	cheque_cr_orgao_cobr 
				set	dt_envio 	= to_date(vet01.dt_vencimento,'dd/MM/yyyy')
				where	nr_seq_cheque 	= vet01.nr_sequencia
				and 	nr_seq_orgao_cobr =  nr_seq_orgao_cobr_w;

				update	cobranca_orgao 
				set	dt_inclusao = sysdate
				where	nr_seq_cobranca = nr_seq_corbanca_w
				and 	nr_seq_orgao = nr_seq_orgao_cobr_w;
			
			end if;
		end if;
		
		
	elsif	(ie_tipo_p = 'T') then --t�ttulo
	
		if	(ie_tipo_lote_w = 'E') then
			
			select	count(*),
				max(nr_seq_cobranca),
				max(nr_seq_orgao_cobr)
			into	qt_w,
				nr_seq_corbanca_w,
				nr_seq_orgao_cobr_w
			from	titulo_receber_orgao_cobr
			where	nr_titulo = vet01.nr_sequencia
			and 	nr_seq_lote_exc = nr_seq_lote_p;
		
			if	(qt_w > 0) then
			
				update	titulo_receber_orgao_cobr 
				set	dt_exclusao 	= to_date(vet01.dt_vencimento,'dd/MM/yyyy')
				where	nr_titulo 	= vet01.nr_sequencia
				and 	nr_seq_orgao_cobr =  nr_seq_orgao_cobr_w;
			
				update	cobranca_orgao 
				set	dt_retirada = sysdate 
				where	nr_seq_cobranca = nr_seq_corbanca_w
				and 	nr_seq_orgao = nr_seq_orgao_cobr_w;
			
			end if;
		
		elsif	(ie_tipo_lote_w = 'I') then
			
			select	count(*),
				max(nr_seq_cobranca),
				max(nr_seq_orgao_cobr)
			into	qt_w,
				nr_seq_corbanca_w,
				nr_seq_orgao_cobr_w
			from	titulo_receber_orgao_cobr
			where	nr_titulo = vet01.nr_sequencia
			and 	nr_seq_lote = nr_seq_lote_p;
			
			if	(qt_w > 0) then
			
				update	titulo_receber_orgao_cobr 
				set	dt_envio 	= to_date(vet01.dt_vencimento,'dd/MM/yyyy')
				where	nr_titulo 	= vet01.nr_sequencia
				and 	nr_seq_orgao_cobr =  nr_seq_orgao_cobr_w;

				update	cobranca_orgao 
				set	dt_inclusao = sysdate
				where	nr_seq_cobranca = nr_seq_corbanca_w
				and 	nr_seq_orgao = nr_seq_orgao_cobr_w;
			
			end if;
		end if;
				
	elsif	(ie_tipo_p = 'O') then --outros

		if	(ie_tipo_lote_w = 'E') then
			
			select	count(*),
				max(nr_seq_cobranca),
				max(nr_seq_orgao_cobr)
			into	qt_w,
				nr_seq_corbanca_w,
				nr_seq_orgao_cobr_w
			from	outros_orgao_cobr
			where	nr_sequencia = vet01.nr_sequencia
			and 	nr_seq_lote_exc = nr_seq_lote_p;
		
			if	(qt_w > 0) then
			
				update	outros_orgao_cobr 
				set	dt_exclusao 	= to_date(vet01.dt_vencimento,'dd/MM/yyyy')
				where	nr_sequencia 	= vet01.nr_sequencia
				and 	nr_seq_orgao_cobr =  nr_seq_orgao_cobr_w;
			
				update	cobranca_orgao 
				set	dt_retirada = sysdate 
				where	nr_seq_cobranca = nr_seq_corbanca_w
				and 	nr_seq_orgao = nr_seq_orgao_cobr_w;
			
			end if;
		
		elsif	(ie_tipo_lote_w = 'I') then
			
			select	count(*),
				max(nr_seq_cobranca),
				max(nr_seq_orgao_cobr)
			into	qt_w,
				nr_seq_corbanca_w,
				nr_seq_orgao_cobr_w
			from	outros_orgao_cobr
			where	nr_sequencia = vet01.nr_sequencia
			and 	nr_seq_lote = nr_seq_lote_p;
			
			if	(qt_w > 0) then
			
				update	outros_orgao_cobr 
				set	dt_envio 	= to_date(vet01.dt_vencimento,'dd/MM/yyyy')
				where	nr_sequencia 	= vet01.nr_sequencia
				and 	nr_seq_orgao_cobr =  nr_seq_orgao_cobr_w;

				update	cobranca_orgao 
				set	dt_inclusao = sysdate
				where	nr_seq_cobranca = nr_seq_corbanca_w
				and 	nr_seq_orgao = nr_seq_orgao_cobr_w;
			
			end if;
		end if;	
	end if;
	
	end;
end loop;
close C01;

commit;

end gerar_retorno_spc_sirc;
/