create or replace
procedure pls_excluir_titulo_cobran_terc(nr_seq_lote_p		number,
					nr_titulo_p		number,
					ie_opcao_p		varchar2) is					
					
begin
/* Excluir todos os t�tulos do lote */
if	(upper(ie_opcao_p) = 'T') and
	(nr_seq_lote_p is not null) then
	delete	
	from	pls_lote_cob_terceiro_item
	where	nr_seq_lote	= nr_seq_lote_p;
end if;

/* Excluir os t�tulos selecionados */
if	(upper(ie_opcao_p) = 'U') and
	(nr_seq_lote_p is not null) and
	(nr_titulo_p is not null) then
	delete	
	from	pls_lote_cob_terceiro_item
	where	nr_seq_lote	= nr_seq_lote_p
	and	nr_titulo	= nr_titulo_p;	
end if;

commit;

end pls_excluir_titulo_cobran_terc;
/