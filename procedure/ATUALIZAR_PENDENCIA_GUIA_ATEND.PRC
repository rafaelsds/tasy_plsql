create or replace 
procedure Atualizar_Pendencia_Guia_Atend(	nr_atendimento_p	number,
							cd_convenio_p	number,
							cd_categoria_p	varchar2,
							nm_usuario_p	varchar2) is

ie_proc_mat_w		number(1);
nr_sequencia_w		number(10);

cursor C01 is
	select ie_proc_mat,
		 nr_sequencia
	from  conta_paciente_v
	where nr_atendimento = nr_atendimento_p
	  and cd_situacao_glosa <> 0
	  and cd_motivo_exc_conta is null
	  and ie_status_acerto = 1
	order by ie_proc_mat, ds_item;

begin

open C01;
loop
	fetch C01 into 	ie_proc_mat_w,
				nr_sequencia_w;
	exit when C01%notfound;

	if	(ie_proc_mat_w = 1) then
		update procedimento_paciente
		set	nr_interno_conta	= null,
			dt_acerto_conta	= null,
			cd_situacao_glosa = 0,
			cd_convenio		= cd_convenio_p,
			cd_categoria	= cd_categoria_p
		where nr_sequencia 	= nr_sequencia_w;
		atualiza_preco_procedimento(nr_sequencia_w, cd_convenio_p, nm_usuario_p);
	else
		update material_atend_paciente
		set	nr_interno_conta	= null,
			dt_acerto_conta	= null,
			cd_situacao_glosa = 0,
			cd_convenio		= cd_convenio_p,
			cd_categoria	= cd_categoria_p
		where nr_sequencia 	= nr_sequencia_w;
		atualiza_preco_material(nr_sequencia_w, nm_usuario_p);
	end if;
end loop;
close C01;

commit;

end Atualizar_Pendencia_Guia_Atend;
/