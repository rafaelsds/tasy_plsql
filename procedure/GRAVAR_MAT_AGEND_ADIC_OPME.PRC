create or replace
procedure gravar_mat_agend_adic_opme(	cd_material_p		number,
					qt_unitaria_p		number,
					nr_seq_lote_fornec_p	varchar2,
					nr_seq_agenda_P		number,
					nm_usuario_p		Varchar2,
					cd_estabelecimento_p	Number,
					cd_cgc_fornec_p		varchar2,
					ie_integracao_util_p	Varchar2,
					nr_seq_opme_p	out	number) is 

nr_sequencia_w		Number(10);
cd_cgc_fornec_w		Varchar2(14);
cd_tabela_preco_w	Varchar2(80);
vl_preco_venda_w	Number(13,4);
vl_unitario_item_w	Number(13,4) := '';
					
begin

if	(nvl(cd_cgc_fornec_p,0) = 0) then

	if	(nvl(nr_seq_lote_fornec_p, 0) > 0) then                                     
		select	cd_cgc_fornec                                                          
		into	cd_cgc_fornec_w                                                          
		from	material_lote_fornec                                                     
		where	nr_sequencia = nr_seq_lote_fornec_p;                                    
	else                                                                           
		cd_cgc_fornec_w	:= obter_fornecedor_regra_consig(cd_estabelecimento_p, cd_material_p, '1');                                                                      
	end if;                    
else
	cd_cgc_fornec_w := cd_cgc_fornec_p;
end if;
begin
	select	substr(nvl(cd_tabela_preco,0),1,80)
	into	cd_tabela_preco_w
	from	parametros_opme
	where	cd_estabelecimento = cd_estabelecimento_p;
exception
when others then
	cd_tabela_preco_w := '0';
end;

if	(cd_tabela_preco_w <> '0') then

	select	nvl(max(b.vl_preco_venda),0)
	into	vl_preco_venda_w
	from	preco_material b,
		tabela_preco_material a
	where	a.cd_tab_preco_mat = b.cd_tab_preco_mat
	and	a.ie_situacao = 'A'
	and	b.ie_situacao = 'A'
	and	trunc(b.dt_inicio_vigencia,'mm') = trunc(sysdate,'mm')
	and	a.cd_tab_preco_mat = cd_tabela_preco_w
	and	b.cd_material = cd_material_p
	and	a.cd_estabelecimento = cd_estabelecimento_p;
	
	if	(vl_preco_venda_w > 0) then
		vl_unitario_item_w := vl_preco_venda_w;
	end if;
	
end if;

if	(nvl(nr_seq_agenda_p,0) <> 0) then
	begin
	select 	agenda_pac_opme_seq.nextval
	into	nr_sequencia_w
	from 	dual;
	
	insert into agenda_pac_opme(
		cd_cgc, 
		cd_cond_pagto, 
		cd_material, 
		ds_observacao, 
		dt_atualizacao, 
		dt_atualizacao_nrec, 
		dt_exclusao, 
		ie_autorizado, 
		ie_gerar_autor, 
		ie_integracao, 
		ie_integracao_util, 
		ie_origem_inf, 
		ie_padrao, 
		nm_usuario, 
		nm_usuario_exclusao, 
		nm_usuario_nrec, 
		nr_seq_agenda, 
		nr_seq_apres, 
		nr_seq_motivo_exclusao, 
		nr_seq_proc_interno, 
		nr_sequencia, 
		qt_material, 
		vl_desconto, 
		vl_unitario_item,
		nr_seq_lote)
	values	(cd_cgc_fornec_w,
		'',
		cd_material_p,
		'',
		sysdate,
		sysdate,
		'',
		'P',
		'S',
		'S',
		ie_integracao_util_p,
		'I',
		'S',
		nm_usuario_p,
		'',
		nm_usuario_P,
		nr_seq_agenda_p,
		500,
		null,
		null,
		nr_sequencia_w,
		qt_unitaria_p,
		'',
		vl_unitario_item_w,
		nr_seq_lote_fornec_p);
	end;
end if;

nr_seq_opme_p := nr_sequencia_w;	
commit;

end gravar_mat_agend_adic_opme;
/
