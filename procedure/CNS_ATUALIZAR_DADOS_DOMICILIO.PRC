CREATE OR REPLACE
PROCEDURE Cns_Atualizar_Dados_Domicilio
			(	cd_pessoa_fisica_p		Varchar2,
				cd_municipio_ibge_p	out	Varchar2,
				cd_cep_p		out	varchar2,
				ds_endereco_p		out	varchar2,
				nr_endereco_p		out	varchar2,
				ds_bairro_p		out	Varchar2) is


cd_municipio_ibge_w	Varchar2(6);
cd_cep_w		varchar2(15);
ds_endereco_w		varchar2(100);
nr_endereco_w		varchar2(7);
ds_bairro_w		Varchar2(30);

BEGIN

/* OBTER OS DADOS DO PACIENTE*/
begin
select	b.cd_municipio_ibge,
	b.cd_cep,
	b.ds_endereco,
	b.nr_endereco,
	substr(b.ds_bairro,1,30)
into	cd_municipio_ibge_w,
	cd_cep_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_bairro_w
from	compl_pessoa_fisica	b,
	pessoa_fisica		a
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	b.ie_tipo_complemento	= 1
and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p;
exception
	when others then
		cd_municipio_ibge_w	:= null;
		cd_cep_w		:= null;
		ds_endereco_w		:= null;
		nr_endereco_w		:= null;
		ds_bairro_w		:= null;
end;

cd_municipio_ibge_p	:= cd_municipio_ibge_w;
cd_cep_p		:= cd_cep_w;
ds_endereco_p		:= ds_endereco_w;
nr_endereco_p		:= nr_endereco_w;
ds_bairro_p		:= ds_bairro_w;

END Cns_Atualizar_Dados_Domicilio;
/
