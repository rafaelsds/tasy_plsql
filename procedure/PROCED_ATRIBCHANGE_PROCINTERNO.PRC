create or replace
procedure proced_atribChange_procInterno(
		nr_seq_proc_interno_p 		number,
		nr_sequencia_p			number,
		nr_seq_exame_p			number,
		cd_setor_atendimento_p		number,
		cd_procedimento_p		number,
		ie_origem_proced_p		number,
		nm_usuario_p			varchar2,
		ie_proc_cgc_p		out	varchar2,
		ie_proc_cig_p		out	varchar2,
		qt_proc_p		out	varchar2,
		cd_interno_padrao_p 	out	varchar2,
		ds_horarios_p		out	varchar2,
		ie_exige_lado_p		out	varchar2,
		nr_atendimento_p		number) is 

proc_w			varchar2(3);
proc_cig_w		varchar2(3);
ie_proc_cgc_w		varchar2(1)	:= 'N';
ie_proc_cig_w		varchar2(1)	:= 'N';
qt_proc_w		varchar2(50);
cd_interno_padrao_w	varchar2(50);
ds_horarios_w		varchar2(50);
ie_exige_lado_w		varchar2(1) 	:= 'N';

begin

proc_w	:= obter_ctrl_glic_proc(nr_seq_proc_interno_p);

if	(proc_w = 'CCG') then
	begin
	ie_proc_cgc_w	:= 'S';
	end;
end if;

if	(proc_w = 'CIG') then
	begin
	ie_proc_cig_w	:= 'S';
	end;
end if;

select	max(ie_exige_lado)
into	ie_exige_lado_w
from	proc_interno
where	nr_sequencia = nr_seq_proc_interno_p;

qt_proc_w		:= obter_param_prescr_proc_int(nr_seq_proc_interno_p, cd_setor_atendimento_p, 'Q', nr_atendimento_p);
cd_interno_padrao_w	:= obter_param_prescr_proc_int(nr_seq_proc_interno_p, cd_setor_atendimento_p, 'I', nr_atendimento_p);
ds_horarios_w		:= obter_param_prescr_proc_int(nr_seq_proc_interno_p, cd_setor_atendimento_p, 'H', nr_atendimento_p);

if	(cd_interno_padrao_w is null) then
	begin
	cd_interno_padrao_w	:= obter_param_proced_prescr(
					cd_procedimento_p,
					ie_origem_proced_p,
					cd_setor_atendimento_p,
					wheb_usuario_pck.get_cd_perfil,
					'I',
					nr_seq_proc_interno_p,
					nr_seq_exame_p);
	end;
end if;

ie_proc_cgc_p		:= ie_proc_cgc_w;
ie_proc_cig_p		:= ie_proc_cig_w;
qt_proc_p		:= qt_proc_w;
cd_interno_padrao_p	:= cd_interno_padrao_w;
ds_horarios_p		:= ds_horarios_w;
ie_exige_lado_p		:= ie_exige_lado_w;

commit;
end proced_atribChange_procInterno;
/