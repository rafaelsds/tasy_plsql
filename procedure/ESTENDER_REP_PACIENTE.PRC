create or replace
procedure estender_rep_paciente(	nr_prescricao_p		number,
			nr_lista_atendimento_p	varchar2,
			cd_perfil_p		number,
			cd_estabelecimento_p	number,
			gerar_liberada_p	varchar2,
			dt_prim_horario_p	varchar2,
			nm_usuario_p		Varchar2) is 


nr_atendimento_w		number(10);
ie_recalcular_horario_w		varchar(1);
recalcularHorariosCopia_w	varchar2(1);
cd_medico_w			varchar2(10);
nr_lista_atendimento_w		varchar2(255);
nr_pos_virgula_w		number(10);
nr_prescricao_nova_w		number(10);
eh_medico_w			varchar2(1);
ds_erro_w			varchar2(255);
cd_medic_posicionar_w	number(15);

begin

select 	cd_medico,
	obter_se_medico(cd_medico, 'M')
into	cd_medico_w,
	eh_medico_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

obter_param_usuario(924,37, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, recalcularHorariosCopia_w);

if	(recalcularHorariosCopia_w = 'S') then
	ie_recalcular_horario_w := 1;
elsif	(recalcularHorariosCopia_w = 'I') then
	ie_recalcular_horario_w := 2;
elsif	(recalcularHorariosCopia_w = 'H') then
	ie_recalcular_horario_w := 3;
elsif	(recalcularHorariosCopia_w = 'V') then
	ie_recalcular_horario_w := 4;
else
	ie_recalcular_horario_w := 0;
end if;


if (nr_lista_atendimento_p is not null)	then
	begin	
	nr_lista_atendimento_w	:= nr_lista_atendimento_p;
	while	(nr_lista_atendimento_w is not null) loop
		begin
		nr_pos_virgula_w	:= instr(nr_lista_atendimento_w,',');
		if (nr_pos_virgula_w > 0)	then
			begin
			nr_atendimento_w	:= to_number(substr(nr_lista_atendimento_w,1,nr_pos_virgula_w-1));
			nr_lista_atendimento_w	:= substr(nr_lista_atendimento_w,nr_pos_virgula_w+1,length(nr_lista_atendimento_w));
			if (nr_atendimento_w > 0) then
				begin
				Copia_Prescricao(nr_prescricao_p, nr_atendimento_w, nm_usuario_p, cd_medico_w, sysdate, 'S', 'S', '', 0, ie_recalcular_horario_w, '', dt_prim_horario_p, sysdate, '','N', '', '', null, cd_perfil_p, nr_prescricao_nova_w,
				null,
				null,
				null,
				'N',
				cd_medic_posicionar_w);
				if 	(gerar_liberada_p = 'S') then
					 Liberar_Prescricao(nr_prescricao_nova_w, nr_atendimento_w, eh_medico_w, cd_perfil_p, nm_usuario_p, 'S',ds_erro_w);
				end if;
				end;
			end if;
			end;
		end if;
		end;
	end loop;
	end;
end if;

commit;

end estender_rep_paciente;
/