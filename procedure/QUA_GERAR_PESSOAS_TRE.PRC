create or replace
procedure Qua_Gerar_Pessoas_tre(
			nr_seq_treinamento_p	number,
			nm_usuario_p		Varchar2) is 

nr_seq_documento_w		number(10);
cd_setor_atendimento_w		number(10);
cd_setor_usuario_w			number(10);
cd_perfil_w			number(10);
cd_cargo_w			number(10);
nm_usuario_w			varchar2(25);
cd_pessoa_fisica_w		varchar2(10);
cd_estabelecimento_w		number(04,0);
ie_somente_funcionario_w		varchar2(01);
qt_regra_lib_w			number(10,0);
nr_seq_grupo_cargo_w		number(10,0);
nr_seq_grupo_usuario_w		number(10,0);
cd_classif_setor_w			varchar2(02);
nr_seq_grupo_gerencia_w		qua_doc_lib.nr_seq_grupo_gerencia%Type;


Cursor C01 is
	select	a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	usuario a
	where	a.ie_situacao		= 'A'
	and	a.cd_pessoa_fisica is not null
	and	a.cd_setor_Atendimento	= cd_setor_Atendimento_w
	and	((nvl(substr(obter_dados_pf(a.cd_pessoa_fisica,'F'),1,1),'S') = ie_somente_funcionario_w) or
		(ie_somente_funcionario_w = 'N'))
	and	not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica		= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);
	
Cursor C02 is
	select	distinct
		a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	usuario a,
		usuario_perfil b
	where	a.ie_situacao	= 'A'
	and	b.nm_usuario	= a.nm_usuario
	and	a.cd_pessoa_fisica is not null
	and	b.cd_perfil	= cd_perfil_w
	and	((nvl(substr(obter_dados_pf(a.cd_pessoa_fisica,'F'),1,1),'S') = ie_somente_funcionario_w) or
		(ie_somente_funcionario_w = 'N'))
	and	not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica		= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);

cursor c03 is
	select	cd_perfil,
		nm_usuario_lib,
		cd_setor_Atendimento,
		cd_cargo,
		nr_seq_grupo_cargo,
		nr_seq_grupo_usuario,
		cd_classif_setor,
		nr_seq_grupo_gerencia
	from	qua_doc_lib
	where	nr_seq_doc	= nr_seq_documento_w;
	
	
Cursor C04 is
	select	a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	usuario a
	where	a.ie_situacao	= 'A'
	and	a.cd_pessoa_fisica is not null
	and	((nvl(substr(obter_dados_pf(a.cd_pessoa_fisica,'F'),1,1),'S')	= ie_somente_funcionario_w) or
		(ie_somente_funcionario_w = 'N'))
	and	a.nm_usuario	= nm_usuario_w
	and	not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica		= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);

Cursor C05 is
	select	cd_pessoa_fisica,
		cd_setor_atendimento
	from	usuario
	where	ie_situacao = 'A'
	and	cd_pessoa_fisica is not null;
	
Cursor C06 is
	select	a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	pessoa_fisica b,
		usuario a
	where	a.ie_situacao	= 'A'
	and	a.cd_pessoa_fisica is not null
	and	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	b.cd_cargo = cd_cargo_w
	and	not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica		= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);

Cursor C07 is
	select	distinct
		b.cd_pessoa_fisica,
		b.cd_setor_atendimento
	from    usuario b,
		usuario_grupo a
	where   a.nm_usuario_grupo= b.nm_usuario
	and     b.ie_situacao     = 'A'
	and     a.ie_situacao     = 'A'
	and	b.cd_pessoa_fisica is not null
	and	a.nr_seq_grupo = nr_seq_grupo_usuario_w
	and not exists(	select	1
			from	qua_doc_trein_pessoa x
			where	x.cd_pessoa_fisica		= b.cd_pessoa_fisica
			and	x.nr_seq_treinamento	= nr_seq_treinamento_p);
			
cursor c08 is
select	cd_cargo
from	qua_cargo_agrup
where	nr_seq_agrup = nr_seq_grupo_cargo_w;

cursor c09 is
select	cd_setor_atendimento
from	setor_atendimento
where	cd_classif_setor = cd_classif_setor_w;

cursor	c10 is
	select	a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	usuario a,
		gerencia_wheb e,
		gerencia_wheb_grupo b,
		gerencia_wheb_grupo_usu c
	where	b.nr_sequencia = nr_seq_grupo_gerencia_w
	and	e.nr_sequencia = b.nr_seq_gerencia
	and	b.nr_sequencia = c.nr_seq_grupo
	and	c.nm_usuario_grupo = a.nm_usuario
	and	e.ie_situacao = 'A'
	and	b.ie_situacao = 'A'
	and	not exists	(	select	1
					from	qua_doc_trein_pessoa x
					where	x.cd_pessoa_fisica		= a.cd_pessoa_fisica
					and	x.nr_seq_treinamento	= nr_seq_treinamento_p);


begin

select	nr_seq_documento
into	nr_seq_documento_w
from	qua_doc_treinamento
where	nr_sequencia	= nr_seq_treinamento_p;

select	count(*)
into	qt_regra_lib_w
from	qua_doc_lib
where	nr_seq_doc 	= nr_seq_documento_w;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	qua_documento
where	nr_sequencia 	= nr_seq_documento_w;

select	substr(nvl(obter_valor_param_usuario(4000, 75, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N'),1,1)
into	ie_somente_funcionario_w
from	dual;

dbms_output.put_line(nr_seq_documento_w);
open C03;
loop
fetch C03 into	
	cd_perfil_w,
	nm_usuario_w,
	cd_setor_atendimento_w,
	cd_cargo_w,
	nr_seq_grupo_cargo_w,
	nr_seq_grupo_usuario_w,
	cd_classif_setor_w,
	nr_seq_grupo_gerencia_w;
exit when C03%notfound;
	begin
	
	if	(cd_setor_atendimento_w	is not null) then
		open C01;
		loop
		fetch C01 into	
			cd_pessoa_fisica_w,
			cd_setor_usuario_w;
		exit when C01%notfound;
			begin
			insert 	into 	qua_doc_trein_pessoa (	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							nr_seq_treinamento,
							cd_pessoa_fisica,
							ie_faltou,
							cd_setor_atendimento)
					values	(	qua_doc_trein_pessoa_seq.nextval,
							sysdate,
							nm_usuario_p,
							nr_seq_treinamento_p,
							cd_pessoa_fisica_w,
							'N',
							cd_setor_usuario_w);
			end;
		end loop;
		close C01;
	end if;
	
	if	(cd_classif_setor_w	is not null) then
		open C09;
		loop
		fetch C09 into	
			cd_setor_atendimento_w;
		exit when C09%notfound;
			begin
			open C01;
			loop
			fetch C01 into	
				cd_pessoa_fisica_w,
				cd_setor_usuario_w;
			exit when C01%notfound;
				begin
				insert into qua_doc_trein_pessoa (	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								nr_seq_treinamento,
								cd_pessoa_fisica,
								ie_faltou,
								cd_setor_atendimento)
						values	(	qua_doc_trein_pessoa_seq.nextval,
								sysdate,
								nm_usuario_p,
								nr_seq_treinamento_p,
								cd_pessoa_fisica_w,
								'N',
								cd_setor_usuario_w);
				end;
			end loop;
			close C01;
			end;
		end loop;
		close C09;
	end if;
	if	(cd_perfil_w	is not null) then
		open C02;
		loop
		fetch C02 into	
			cd_pessoa_fisica_w,
			cd_setor_usuario_w;
		exit when C02%notfound;
			begin
			insert into qua_doc_trein_pessoa (	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							nr_seq_treinamento,
							cd_pessoa_fisica,
							ie_faltou,
							cd_setor_atendimento)
					values	(	qua_doc_trein_pessoa_seq.nextval,
							sysdate,
							nm_usuario_p,
							nr_seq_treinamento_p,
							cd_pessoa_fisica_w,
							'N',
							cd_setor_usuario_w);
			end;
		end loop;
		close C02;
	end if;
	if	(nm_usuario_w	is not null) then
		open C04;
		loop
		fetch C04 into	
			cd_pessoa_fisica_w,
			cd_setor_usuario_w;
		exit when C04%notfound;
			begin
			insert into qua_doc_trein_pessoa (	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							nr_seq_treinamento,
							cd_pessoa_fisica,
							ie_faltou,
							cd_setor_atendimento)
					values	(	qua_doc_trein_pessoa_seq.nextval,
							sysdate,
							nm_usuario_p,
							nr_seq_treinamento_p,
							cd_pessoa_fisica_w,
							'N',
							cd_setor_usuario_w);
			end;
		end loop;
		close C04;
	end if;
	if	(cd_cargo_w	is not null) then
		open C06;
		loop
		fetch C06 into	
			cd_pessoa_fisica_w,
			cd_setor_usuario_w;
		exit when C06%notfound;
			begin
			insert into qua_doc_trein_pessoa (	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							nr_seq_treinamento,
							cd_pessoa_fisica,
							ie_faltou,
							cd_setor_atendimento)
					values	(	qua_doc_trein_pessoa_seq.nextval,
							sysdate,
							nm_usuario_p,
							nr_seq_treinamento_p,
							cd_pessoa_fisica_w,
							'N',
							cd_setor_usuario_w);
			end;
		end loop;
		close C06;
	end if;	
	
	if	(nr_seq_grupo_cargo_w is not null) then
		open C08;
		loop
		fetch C08 into	
			cd_cargo_w;
		exit when C08%notfound;
			begin
			open C06;
			loop
			fetch C06 into	
				cd_pessoa_fisica_w,
				cd_setor_usuario_w;
			exit when C06%notfound;
				begin
				insert into qua_doc_trein_pessoa (	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								nr_seq_treinamento,
								cd_pessoa_fisica,
								ie_faltou,
								cd_setor_atendimento)
						values	(	qua_doc_trein_pessoa_seq.nextval,
								sysdate,
								nm_usuario_p,
								nr_seq_treinamento_p,
								cd_pessoa_fisica_w,
								'N',
								cd_setor_usuario_w);
				end;
			end loop;
			close C06;
			end;
		end loop;
		close C08;
	end if;
	
	if	(nr_seq_grupo_usuario_w is not null) then
		open C07;
		loop
		fetch C07 into	
			cd_pessoa_fisica_w,
			cd_setor_usuario_w;
		exit when C07%notfound;
			begin
			insert into qua_doc_trein_pessoa (	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							nr_seq_treinamento,
							cd_pessoa_fisica,
							ie_faltou,
							cd_setor_atendimento)
					values	(	qua_doc_trein_pessoa_seq.nextval,
							sysdate,
							nm_usuario_p,
							nr_seq_treinamento_p,
							cd_pessoa_fisica_w,
							'N',
							cd_setor_usuario_w);
			end;
		end loop;
		close C07;
	end if;
	
	if	(nr_seq_grupo_gerencia_w is not null) then
	
		open	c10;
		loop	
		fetch 	c10 into	
			cd_pessoa_fisica_w,
			cd_setor_usuario_w;
		exit 	when C10%notfound;
			begin
			insert 	into 	qua_doc_trein_pessoa 
					(	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_treinamento,
						cd_pessoa_fisica,
						ie_faltou,
						cd_setor_atendimento)
				values	(	qua_doc_trein_pessoa_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_treinamento_p,
						cd_pessoa_fisica_w,
						'N',
						cd_setor_usuario_w);
			
			end;
		end loop;
		close c10;
	
	end if;
	end;
end loop;
close C03;

if	(qt_regra_lib_w = 0) then
	begin
	open C05;
	loop
	fetch C05 into	
		cd_pessoa_fisica_w,
		cd_setor_usuario_w;
	exit when C05%notfound;
		begin
		insert into qua_doc_trein_pessoa (	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_treinamento,
						cd_pessoa_fisica,
						ie_faltou,
						cd_setor_atendimento)
				values	(	qua_doc_trein_pessoa_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_treinamento_p,
						cd_pessoa_fisica_w,
						'N',
						cd_setor_usuario_w);
		end;
	end loop;
	close C05;
	end;
end if;

commit;

end Qua_Gerar_Pessoas_tre;
/