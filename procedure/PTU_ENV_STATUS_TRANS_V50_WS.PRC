create or replace
procedure ptu_env_status_trans_v50_ws(	nr_seq_guia_p			ptu_pedido_autorizacao.nr_seq_guia%type,
					nr_seq_requisicao_p		ptu_pedido_autorizacao.nr_seq_requisicao%type,
					nr_versao_ptu_p			ptu_pedido_status.nr_versao%type,
					nm_usuario_p			usuario.nm_usuario%type,					
					nr_seq_pedido_novo_p	out	ptu_pedido_status.nr_sequencia%type) is 

begin

ptu_envio_scs_ws_pck.ptu_envio_status_transacao(nr_seq_guia_p, nr_seq_requisicao_p, nr_versao_ptu_p, nm_usuario_p,
						nr_seq_pedido_novo_p);

end ptu_env_status_trans_v50_ws;
/