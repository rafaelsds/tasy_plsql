create or replace
procedure gerar_w_relat_etapa_barras(	nr_atendimento_p	number, 
					nr_interno_conta_p	number,                       
					nr_prontuario_p         number,        
					nm_paciente_p           varchar2,
					nr_seq_etapa_p          number,       
					cd_setor_atendimento_p  number,               
					ds_setor_atendimento_p  varchar2,               
					ds_etapa_p              varchar2,
					cd_motivo_dev_etapa_p   number,      
					ds_motivo_dev_etapa_p   varchar2,               
					cd_pessoa_etapa_p       varchar2,               
					nm_pessoa_etapa_p       varchar2,               
					ds_observacao_p         varchar2,
					nm_usuario_p		varchar2) is 

nr_seq_protocolo_w	number(10,0);
vl_conta_w		number(15,2);

begin

if (nr_interno_conta_p > 0) then

	select	max(nr_seq_protocolo),
		max(obter_valor_conta(nr_interno_conta_p,0))
	into	nr_seq_protocolo_w,
		vl_conta_w
	from 	conta_paciente
	where	nr_interno_conta = nr_interno_conta_p;

end if;

insert 	into w_relat_etapa_barras(	nr_atendimento,                 
					nr_interno_conta,               
					nr_prontuario,                  
					nm_paciente,                    
					nr_seq_etapa,                   
					ds_setor_atendimento,           
					ds_etapa,                       
					dt_atualizacao,                 
					nm_usuario,                     
					dt_atualizacao_nrec,            
					nm_usuario_nrec,                
					cd_motivo_dev_etapa,            
					ds_motivo_dev_etapa,            
					cd_pessoa_etapa,                
					nm_pessoa_etapa,                
					ds_observacao,
					nr_seq_protocolo,
					vl_conta) 
	values(	nr_atendimento_p,
		nr_interno_conta_p,
		nr_prontuario_p,
		nm_paciente_p,
		nr_seq_etapa_p,
		ds_setor_atendimento_p,
		ds_etapa_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_motivo_dev_etapa_p,
		ds_motivo_dev_etapa_p,
		cd_pessoa_etapa_p,
		nm_pessoa_etapa_p,
		ds_observacao_p,
		nr_seq_protocolo_w,
		vl_conta_w);


commit;

end gerar_w_relat_etapa_barras;
/
