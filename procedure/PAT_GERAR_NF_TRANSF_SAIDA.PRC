create or replace
procedure pat_gerar_nf_transf_saida(
    nm_usuario_p usuario.nm_usuario%type
  , nr_seq_historico_p pat_historico_bem.nr_sequencia%type
  , cd_material_p pat_bem.cd_material%type
  , cd_serie_nf_p nota_fiscal.cd_serie_nf%type
  , nr_nota_fiscal_p nota_fiscal.nr_nota_fiscal%type
  , cd_operacao_nf_p nota_fiscal.cd_operacao_nf%type
  , cd_natureza_operacao_p nota_fiscal.cd_natureza_operacao%type
  , cd_condicao_pagamento_p nota_fiscal.cd_condicao_pagamento%type
  , ds_observacao_p nota_fiscal.ds_observacao%type
  , ds_retorno_p out nocopy varchar2) is

pat_historico_bem_w pat_historico_bem%rowtype;
nota_fiscal_w nota_fiscal%rowtype;
nota_fiscal_item_w nota_fiscal_item%rowtype;
begin
/*
===============================================================================
INICIO - Obter dados do patrim�nio � ser transferido
===============================================================================
/* Obter dados do hist�rico do bem */
begin
select *
  into pat_historico_bem_w
  from pat_historico_bem a
 where a.nr_sequencia = nr_seq_historico_p;
exception
when too_many_rows then pat_historico_bem_w := null;
when no_data_found then pat_historico_bem_w := null;
end;

/*
===============================================================================
FINAL - Obter dados do patrim�nio � ser transferido
===============================================================================
===============================================================================
INICIO - Gerar nota fiscal - Insert na NOTA_FISCAL
===============================================================================
/* Limpar a variavel de nota fiscal */
nota_fiscal_w := null;

/* Obter a sequencia da nota */
select  nota_fiscal_seq.nextval
into    nota_fiscal_w.nr_sequencia
from    dual;

nota_fiscal_w.nm_usuario            := nm_usuario_p;
nota_fiscal_w.ds_observacao         := ds_observacao_p;

nota_fiscal_w.nr_nota_fiscal        := nr_nota_fiscal_p;
nota_fiscal_w.nr_sequencia_nf       := 1;

nota_fiscal_w.dt_emissao            := pat_historico_bem_w.dt_historico;
nota_fiscal_w.dt_entrada_saida      := pat_historico_bem_w.dt_historico;
nota_fiscal_w.dt_atualizacao        := sysdate;

nota_fiscal_w.cd_estabelecimento    := pat_historico_bem_w.cd_estab_origem;
nota_fiscal_w.cd_cgc_emitente       := substr(obter_cgc_estabelecimento(pat_historico_bem_w.cd_estab_origem),1,14);
nota_fiscal_w.cd_serie_nf           := cd_serie_nf_p;
nota_fiscal_w.cd_operacao_nf        := cd_operacao_nf_p;
nota_fiscal_w.cd_natureza_operacao  := cd_natureza_operacao_p;
nota_fiscal_w.cd_cgc                := substr(obter_cgc_estabelecimento(pat_historico_bem_w.cd_estab_destino),1,14);

nota_fiscal_w.cd_condicao_pagamento := cd_condicao_pagamento_p;
nota_fiscal_w.ie_tipo_nota          := 'SD'; -- Dom 1661 | Saida por digita��o 
nota_fiscal_w.ie_entregue_bloqueto  := 'N';
nota_fiscal_w.ie_situacao           := '1'; -- Dom 1056 | Ativa OK
nota_fiscal_w.ie_acao_nf            := '1';
nota_fiscal_w.ie_emissao_nf         := '0';
nota_fiscal_w.ie_tipo_frete         := '0';

nota_fiscal_w.qt_peso_bruto         := 0;
nota_fiscal_w.qt_peso_liquido       := 0;

nota_fiscal_w.vl_despesa_acessoria  := 0;
nota_fiscal_w.vl_desconto_rateio    := 0;
nota_fiscal_w.vl_descontos          := 0;
nota_fiscal_w.vl_seguro             := 0;
nota_fiscal_w.vl_frete              := 0;
nota_fiscal_w.vl_ipi                := 0;

-- Busca o valor base de deprecia��o para compor o total da nota
begin
select vl_base_deprec 
into nota_fiscal_w.vl_total_nota
from pat_valor_bem 
where nr_seq_bem = pat_historico_bem_w.nr_seq_bem
and dt_valor = (select pkg_date_utils.start_of(pkg_date_utils.end_of(ADD_MONTHS(dt_historico, -1), 'MONTH',0), 'DAY', 0) 
				    from pat_historico_bem
				    where nr_sequencia = pat_historico_bem_w.nr_sequencia);
exception
when too_many_rows then wheb_mensagem_pck.exibir_mensagem_abort(1096869);
when no_data_found then wheb_mensagem_pck.exibir_mensagem_abort(1096869);
end;                    
				    
nota_fiscal_w.vl_mercadoria         := nota_fiscal_w.vl_total_nota;

insert into nota_fiscal values nota_fiscal_w;
/*
===============================================================================
FINAL - Gerar nota fiscal - Insert na NOTA_FISCAL
===============================================================================
===============================================================================
INICIO - Gerar item da nota - Insert na NOTA_FISCAL_ITEM
===============================================================================
*/
nota_fiscal_item_w := null;
nota_fiscal_item_w.nm_usuario                := nota_fiscal_w.nm_usuario;
nota_fiscal_item_w.ds_observacao             := nota_fiscal_w.ds_observacao;

nota_fiscal_item_w.nr_sequencia              := nota_fiscal_w.nr_sequencia;
nota_fiscal_item_w.nr_nota_fiscal            := nota_fiscal_w.nr_nota_fiscal;
nota_fiscal_item_w.nr_sequencia_nf           := nota_fiscal_w.nr_sequencia_nf;
nota_fiscal_item_w.nr_item_nf                := 1;

nota_fiscal_item_w.dt_atualizacao            := nota_fiscal_w.dt_atualizacao;

nota_fiscal_item_w.cd_estabelecimento        := nota_fiscal_w.cd_estabelecimento;
nota_fiscal_item_w.cd_cgc_emitente           := nota_fiscal_w.cd_cgc_emitente;
nota_fiscal_item_w.cd_serie_nf               := nota_fiscal_w.cd_serie_nf;
nota_fiscal_item_w.cd_natureza_operacao      := nota_fiscal_w.cd_natureza_operacao;
nota_fiscal_item_w.cd_material               := cd_material_p;
nota_fiscal_item_w.cd_local_estoque          := null;
nota_fiscal_item_w.cd_unidade_medida_compra  := null;
nota_fiscal_item_w.cd_material_estoque       := cd_material_p;
nota_fiscal_item_w.cd_unidade_medida_estoque := null;
nota_fiscal_item_w.cd_conta_contabil         := null;
nota_fiscal_item_w.cd_sequencia_parametro    := null;

nota_fiscal_item_w.qt_item_nf                := 1;
nota_fiscal_item_w.qt_item_estoque           := null;

nota_fiscal_item_w.pr_desconto               := 0;
nota_fiscal_item_w.pr_desc_financ            := 0;

nota_fiscal_item_w.vl_frete                  := nota_fiscal_w.vl_frete;
nota_fiscal_item_w.vl_desconto               := nota_fiscal_w.vl_descontos;
nota_fiscal_item_w.vl_despesa_acessoria      := nota_fiscal_w.vl_despesa_acessoria;
nota_fiscal_item_w.vl_desconto_rateio        := nota_fiscal_w.vl_desconto_rateio;
nota_fiscal_item_w.vl_seguro                 := nota_fiscal_w.vl_seguro;
nota_fiscal_item_w.nr_ordem_compra           := nota_fiscal_w.nr_ordem_compra;
nota_fiscal_item_w.vl_unitario_item_nf       := nota_fiscal_w.vl_total_nota;
nota_fiscal_item_w.vl_total_item_nf          := nota_fiscal_w.vl_total_nota;
nota_fiscal_item_w.vl_liquido                := nota_fiscal_w.vl_total_nota;

insert into nota_fiscal_item values nota_fiscal_item_w;
/*
===============================================================================
FINAL - Gerar item da nota - Insert na NOTA_FISCAL_ITEM
===============================================================================
===============================================================================
Vincular a nota de sa�da com o hist�rico de transfer�ncia do bem
===============================================================================
*/
update 	pat_historico_bem
set	nr_seq_nf_transf_saida 	= nota_fiscal_w.nr_sequencia
where 	nr_sequencia 		= nr_seq_historico_p;
/*
===============================================================================
Consistir/Atualizar nota fiscal, caso necess�rio
===============================================================================
*/
/* Atualizar o total da nota */
atualiza_total_nota_fiscal(nota_fiscal_w.nr_sequencia,nota_fiscal_w.nm_usuario);

gerar_vencimento_nota_fiscal(nota_fiscal_w.nr_sequencia,nota_fiscal_w.nm_usuario);
 
commit;
end pat_gerar_nf_transf_saida;
/