CREATE OR REPLACE
PROCEDURE Consiste_Mat_Esp_AIH(
					nr_atendimento_p		Number,
					NR_INTERNO_CONTA_P	NUMBER,
					ds_erro_p		OUT	Varchar2) IS

ds_erro_w			Varchar2(80);
cd_estabelecimento_w	Number(4);
qt_material_w		Number(5);
qt_proc_w			Number(5);
cd_procedimento_w		Number(15);
cd_proc_solic_w		Number(15);
ie_origem_proced_w	Number(10);
cd_material_w		Number(6);
qt_max_procedimento_w	Number(5);
qt_reg_ortese_protese_w	Number(10);
qt_proc_realizado_w	Number(10);

Cursor C01 is
select a.cd_material,
	 a.cd_procedimento,
	 a.ie_origem_proced,
	 a.cd_estabelecimento,
	 sum(b.qt_material)
from	 material_atend_paciente b,
	 material_esp_sus a
where	 b.nr_atendimento		= nr_atendimento_p
and	 b.nr_interno_conta	= nr_interno_conta_p
and	 b.cd_material		= a.cd_material
group by
	 a.cd_material,
	 a.cd_procedimento,
	 a.ie_origem_proced,
	 a.cd_estabelecimento;

BEGIN
ds_erro_w		:= '';
OPEN c01;
LOOP
FETCH c01 into
	cd_material_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_estabelecimento_w,
	qt_material_w;
EXIT when c01%notfound;
	BEGIN
	if	(qt_material_w > 0) then
		select count(*)
		into	 qt_proc_w
		from	 procedimento_paciente
		where	 nr_atendimento	= nr_atendimento_p
		and	 nr_interno_conta	= nr_interno_conta_p
		and	 cd_procedimento	= cd_procedimento_w
		and	 ie_origem_proced	= ie_origem_proced_w
		and	 cd_motivo_exc_conta	is null;
		if	 (qt_proc_w 	= 0) then
			 ds_erro_w	:= ds_erro_w ||'915'||'('||to_char(cd_procedimento_w)||') ';
		end if;
	end if;

	select count(*)
	into	 qt_reg_ortese_protese_w
	from	 sus_ortese_protese;

	if	(qt_reg_ortese_protese_w	> 0)	and
		(qt_material_w 			> 0)	and
		(qt_proc_w				> 0)	then
		begin
		select sum(nvl(qt_procedimento,0))
		into	 qt_proc_realizado_w
		from	 procedimento_paciente
		where	 nr_atendimento	= nr_atendimento_p
		and	 nr_interno_conta	= nr_interno_conta_p
		and	 cd_procedimento	= cd_procedimento_w
		and	 ie_origem_proced	= ie_origem_proced_w
		and	 cd_motivo_exc_conta	is null;

		select nvl(max(cd_procedimento_solic),0)
		into	 cd_proc_solic_w
		from	 sus_laudo_paciente
		where	 nr_atendimento	= nr_atendimento_p
		and	 nr_interno_conta	= nr_interno_conta_p
		and	 ie_tipo_laudo_sus	=
			 (select max(x.ie_tipo_laudo_sus)
			 	from	sus_laudo_paciente x
				where x.nr_atendimento = nr_atendimento_p
				and	x.nr_interno_conta	= nr_interno_conta_p
				and	x.ie_tipo_laudo_sus in(0,1));	

		select nvl(max(qt_max_procedimento),0)
		into	 qt_max_procedimento_w
		from	 sus_ortese_protese
		where	 cd_estabelecimento	= cd_estabelecimento_w
		and	 cd_proc_principal	= cd_proc_solic_w
		and	 ie_origem_proc_princ	= ie_origem_proced_w
		and	 cd_procedimento		= cd_procedimento_w
		and	 ie_origem_proced		= ie_origem_proced_w;

		if	(qt_max_procedimento_w	= 0)					or
			(qt_proc_realizado_w	> qt_max_procedimento_w)	then
			 ds_erro_w	:= ds_erro_w ||'2116'||'('||to_char(cd_procedimento_w)||') ';
		end if;
		end;
	end if;
	END;
END LOOP;
CLOSE C01;

ds_erro_p	:= ds_erro_w;

END Consiste_Mat_Esp_AIH;
/