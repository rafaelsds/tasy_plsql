create or replace
procedure GERAR_FLUXO_CONSOLIDADO
		(DT_REFERENCIA_1_P	in	varchar2,
		 DT_REFERENCIA_2_P	in	varchar2,
		 DT_REFERENCIA_3_P	in	varchar2,
		 CD_EMPRESA_P	in	varchar2) is


ds_conta_estrut_w	varchar2(255);
cd_conta_apres_w	varchar2(255);
vl_fluxo_1_w		number(17,4);
vl_fluxo_2_w		number(17,4);
vl_fluxo_3_w		number(17,4);
vl_total_w			number(17,4);
ds_comando_w		varchar2(3200);
cd_sqlw_empresa_w		varchar2(4000);
ie_empresa_est_contido_w 		varchar2(1) := 'S';
DT_REFERENCIA_1_W varchar2(20);
DT_REFERENCIA_2_W varchar2(20);
DT_REFERENCIA_3_W varchar2(20);

cursor c01 is
select cd_conta_apres, ds_conta_estrut,
	sum(decode(vl_fluxo_01,0,vl_fluxo_1,vl_fluxo_01)) vl_fluxo_1,
	sum(decode(vl_fluxo_02,0,vl_fluxo_2,vl_fluxo_02)) vl_fluxo_2,
	sum(decode(vl_fluxo_03,0,vl_fluxo_3,vl_fluxo_03)) vl_fluxo_3,
	sum(decode(vl_fluxo_01 + vl_fluxo_02+vl_fluxo_03, 0, vl_fluxo_1 + vl_fluxo_2+vl_fluxo_3, vl_fluxo_01 + vl_fluxo_02+vl_fluxo_03)) vl_total
from(
	SELECT c.cd_conta_apres, c.ds_conta_estrut,
		decode(obter_se_totaliza_cf(c.cd_conta_financ), 'S', decode(c.cd_conta_financ, obter_conta_financ_param(a.CD_ESTABELECIMENTO, 2), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_1_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_1_W,'dd/mm/yyyy')), decode(DT_REFERENCIA_1_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_1_W,'dd/mm/yyyy')) , 2), obter_conta_financ_param(a.CD_ESTABELECIMENTO, 1), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_1_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_1_W,'dd/mm/yyyy')) , decode(DT_REFERENCIA_1_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_1_W,'dd/mm/yyyy')) , 1),
			0),  0) vl_fluxo_01,
		SUM(DECODE(TO_CHAR(a.dt_referencia,'mm/yyyy'),decode(DT_REFERENCIA_1_W,'/',null,DT_REFERENCIA_1_W),a.vl_fluxo, 0)) vl_fluxo_1,
		decode(obter_se_totaliza_cf(c.cd_conta_financ), 'S', decode(c.cd_conta_financ, obter_conta_financ_param(a.CD_ESTABELECIMENTO, 2), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_2_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_2_W,'dd/mm/yyyy')), decode(DT_REFERENCIA_2_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_2_W,'dd/mm/yyyy')) , 2), obter_conta_financ_param(a.CD_ESTABELECIMENTO, 1), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_2_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_2_W,'dd/mm/yyyy')) , decode(DT_REFERENCIA_2_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_2_W,'dd/mm/yyyy')) , 1),
			0),  0) vl_fluxo_02,
		SUM(DECODE(TO_CHAR(a.dt_referencia,'mm/yyyy'),decode(DT_REFERENCIA_2_W,'/',null,DT_REFERENCIA_2_W),a.vl_fluxo, 0)) vl_fluxo_2,
		decode(obter_se_totaliza_cf(c.cd_conta_financ), 'S', decode(c.cd_conta_financ, obter_conta_financ_param(a.CD_ESTABELECIMENTO, 2), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_3_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_3_W,'dd/mm/yyyy')), decode(DT_REFERENCIA_3_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_3_W,'dd/mm/yyyy')) , 2), obter_conta_financ_param(a.CD_ESTABELECIMENTO, 1), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_3_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_3_W,'dd/mm/yyyy')) , decode(DT_REFERENCIA_3_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_3_W,'dd/mm/yyyy')) , 1),
			0),  0) vl_fluxo_03,
		SUM(DECODE(TO_CHAR(a.dt_referencia,'mm/yyyy'),decode(DT_REFERENCIA_3_W,'/',null,DT_REFERENCIA_3_W),a.vl_fluxo, 0)) vl_fluxo_3
	FROM	conta_financeira_v c, 	fluxo_caixa_lote b, 	fluxo_caixa_data a  
	WHERE	a.vl_fluxo	<> 0 
	AND	c.ie_oper_fluxo = NVL('',c.ie_oper_fluxo) 
	AND	NVL(c.ie_situacao,'A') = NVL('A',NVL(c.ie_situacao,'A')) 
	AND	a.cd_conta_financ = c.cd_conta_financ 
	AND	b.ie_classif_fluxo = 'P'
	and ((cd_sqlw_empresa_w = 'X') or (obter_se_contido(b.cd_empresa, cd_sqlw_empresa_w) = ie_empresa_est_contido_w)) 
	AND	a.nr_seq_lote_fluxo = b.nr_sequencia 
	AND	NVL(a.ie_origem,'X') = NVL('',NVL(a.ie_origem,'X')) 
	AND	NVL(a.ie_nivel,1) <= NVL('4',1) 
	AND	((DT_REFERENCIA_1_W <> '/' AND TO_CHAR(a.dt_referencia,'mm/yyyy') = DT_REFERENCIA_1_W) 
		OR (DT_REFERENCIA_2_W <> '/' AND TO_CHAR(a.dt_referencia,'mm/yyyy') = DT_REFERENCIA_2_W) 
			OR(DT_REFERENCIA_3_W <> '/' AND TO_CHAR(a.dt_referencia,'mm/yyyy') = DT_REFERENCIA_3_W))
	GROUP BY c.cd_conta_apres, c.ds_conta_estrut,
		decode(obter_se_totaliza_cf(c.cd_conta_financ), 'S', decode(c.cd_conta_financ, obter_conta_financ_param(a.CD_ESTABELECIMENTO, 2), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_1_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_1_W,'dd/mm/yyyy')), decode(DT_REFERENCIA_1_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_1_W,'dd/mm/yyyy')) , 2), obter_conta_financ_param(a.CD_ESTABELECIMENTO, 1), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_1_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_1_W,'dd/mm/yyyy')) , decode(DT_REFERENCIA_1_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_1_W,'dd/mm/yyyy')) , 1),
			0),  0),
		decode(obter_se_totaliza_cf(c.cd_conta_financ), 'S', decode(c.cd_conta_financ, obter_conta_financ_param(a.CD_ESTABELECIMENTO, 2), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_2_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_2_W,'dd/mm/yyyy')), decode(DT_REFERENCIA_2_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_2_W,'dd/mm/yyyy')) , 2), obter_conta_financ_param(a.CD_ESTABELECIMENTO, 1), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_2_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_2_W,'dd/mm/yyyy')) , decode(DT_REFERENCIA_2_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_2_W,'dd/mm/yyyy')) , 1),
			0),  0),
		decode(obter_se_totaliza_cf(c.cd_conta_financ), 'S', decode(c.cd_conta_financ, obter_conta_financ_param(a.CD_ESTABELECIMENTO, 2), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_3_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_3_W,'dd/mm/yyyy')), decode(DT_REFERENCIA_3_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_3_W,'dd/mm/yyyy')) , 2), obter_conta_financ_param(a.CD_ESTABELECIMENTO, 1), obter_valor_fluxo_saldo_lote(a.CD_ESTABELECIMENTO, 'P', decode(DT_REFERENCIA_3_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_3_W,'dd/mm/yyyy')) , decode(DT_REFERENCIA_3_W,'/',null,TO_DATE('01/' || DT_REFERENCIA_3_W,'dd/mm/yyyy')) , 1),
			0),  0))
GROUP BY cd_conta_apres, ds_conta_estrut
ORDER BY cd_conta_apres;

 
begin

DT_REFERENCIA_1_W	:= replace(DT_REFERENCIA_1_P,' ','');
DT_REFERENCIA_2_W	:= replace(DT_REFERENCIA_2_P,' ','');
DT_REFERENCIA_3_W	:= replace(DT_REFERENCIA_3_P,' ','');


cd_sqlw_empresa_w := replace(CD_EMPRESA_P,' ','');
cd_sqlw_empresa_w := replace(cd_sqlw_empresa_w,'(','');
cd_sqlw_empresa_w := replace(cd_sqlw_empresa_w,')','');
cd_sqlw_empresa_w := nvl(cd_sqlw_empresa_w,'X');

Exec_sql_Dinamico('Tasy', 'drop table w_fluxo_caixa_consolidado');

-- criar tabela

ds_comando_w		:= 'create table w_fluxo_caixa_consolidado (cd_conta_apres varchar2(255), ds_conta_estrut varchar2(255), vl_fluxo_1 NUMBER(17,4), vl_fluxo_2 NUMBER(17,4), vl_fluxo_3 NUMBER(17,4), vl_total NUMBER(17,4))';

Exec_sql_Dinamico('Tasy', ds_comando_w);


-- carregar contas financeiras
open c01;
loop
fetch c01 into
	cd_conta_apres_w,
	ds_conta_estrut_w,
	vl_fluxo_1_w,
	vl_fluxo_2_w,
	vl_fluxo_3_w,
	vl_total_w;
exit when c01%notfound;

	Exec_sql_Dinamico('Tasy', 'insert into w_fluxo_caixa_consolidado (cd_conta_apres, ds_conta_estrut, vl_fluxo_1, vl_fluxo_2, vl_fluxo_3, vl_total) values (' || chr(39) || cd_conta_apres_w || chr(39) || ',' || chr(39) || ds_conta_estrut_w || chr(39) || ', ' || chr(39) || vl_fluxo_1_w || chr(39) || ', ' || chr(39) || vl_fluxo_2_w || chr(39) || ', ' || chr(39) || vl_fluxo_3_w || chr(39) || ', ' || chr(39) || vl_total_w || chr(39) ||')');
	
	end loop;
close c01;

commit;

end GERAR_FLUXO_CONSOLIDADO;
/

