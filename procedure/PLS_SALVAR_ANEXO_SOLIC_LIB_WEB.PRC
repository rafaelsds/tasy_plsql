create or replace
procedure pls_salvar_anexo_solic_lib_web
			(	nr_seq_solic_lib_p		Number,	
				ds_caminho_p			Varchar2,
				ds_caminho2_p			Varchar2,
				ds_caminho3_p			Varchar2,
				nm_usuario_p			Varchar2) is 

begin

if	(ds_caminho_p is not null) then
	insert into pls_solic_lib_anexo(nr_sequencia, nr_seq_solic_lib, dt_anexo,           
				        ds_anexo,dt_atualizacao, nm_usuario,         
				        dt_atualizacao_nrec,nm_usuario_nrec)     
				 values(pls_solic_lib_anexo_seq.nextval, nr_seq_solic_lib_p, sysdate,            
					ds_caminho_p, sysdate, nm_usuario_p,        
					sysdate, nm_usuario_p);
end if;

if	(ds_caminho2_p is not null) then
	insert into pls_solic_lib_anexo(nr_sequencia, nr_seq_solic_lib, dt_anexo,           
				        ds_anexo,dt_atualizacao, nm_usuario,         
				        dt_atualizacao_nrec,nm_usuario_nrec)     
				 values(pls_solic_lib_anexo_seq.nextval, nr_seq_solic_lib_p, sysdate,            
					ds_caminho2_p, sysdate, nm_usuario_p,        
					sysdate, nm_usuario_p);
end if;

if	(ds_caminho3_p is not null) then
	insert into pls_solic_lib_anexo(nr_sequencia, nr_seq_solic_lib, dt_anexo,           
				        ds_anexo,dt_atualizacao, nm_usuario,         
				        dt_atualizacao_nrec,nm_usuario_nrec)     
				 values(pls_solic_lib_anexo_seq.nextval, nr_seq_solic_lib_p, sysdate,            
					ds_caminho3_p, sysdate, nm_usuario_p,        
					sysdate, nm_usuario_p);
end if;

commit;

end pls_salvar_anexo_solic_lib_web;
/