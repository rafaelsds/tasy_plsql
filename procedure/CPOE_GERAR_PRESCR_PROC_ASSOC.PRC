create or replace procedure cpoe_gerar_prescr_proc_assoc(	nr_sequencia_p			number,
										nm_usuario_p			varchar2,
										cd_estabelecimento_p	number) is
									
nr_sequencia_w				cpoe_procedimento.nr_sequencia%type;
cd_estabelecimento_w		number(4,0);
qt_idade_pac_w				number(10,2);
ie_sexo_prescr_w			varchar2(1);
ds_mat_adic_proc_w			varchar(20) := 'X';			
qt_idade_mes_w				number(10,2);
qt_idade_dia_w				number(10,2);
nr_seq_proc_int_adic_w 		proc_int_proc_prescr.nr_seq_proc_int_adic%type;
ie_origen_proced_w			number(15,0);
qt_proc_adic_w				number(15,4);
nm_usuario_w				varchar2(15);
ie_agenda_integrada_w		varchar2(7);
cd_intervalo_w				varchar2(7);
cd_intervalo_padr_w			varchar2(7);
ds_dado_clinico_w			cpoe_procedimento.ds_dado_clinico%type;
cd_material_exame_w			cpoe_procedimento.cd_material_exame%type;
cd_plano_w					varchar2(20);
ie_urgencia_w				cpoe_procedimento.ie_urgencia%type;
cd_edicao_amb_w				number(10,0);
ds_horarios_w				cpoe_procedimento.ds_horarios%type;
nr_ocorrencia_w				cpoe_procedimento.nr_ocorrencia%type;
nr_atendimento_w			cpoe_procedimento.nr_atendimento%type;
dt_prev_execucao_w			cpoe_procedimento.dt_prev_execucao%type;
cd_convenio_w				number(5,0);
ie_origem_proc_filtro_w		number(10);
ie_tipo_atendimento_w		number(3);
ie_tipo_convenio_w			number(2);
cd_categoria_w				varchar2(10);
cd_medico_w					cpoe_procedimento.cd_medico%type;
cd_setor_prescr_proc_w		number(5,0);
ie_proced_vinculado_w		varchar2(1);
ie_lado_w					cpoe_procedimento.ie_lado%type;
ds_observacao_w				cpoe_procedimento.ds_observacao%type;
ie_cobra_paciente_w			varchar2(1);
nr_seq_contraste_w			cpoe_procedimento.nr_seq_contraste%type;
ds_justificativa_w			cpoe_procedimento.ds_justificativa%type;
ie_anestesia_w				cpoe_procedimento.ie_anestesia%type;
cd_pessoa_fisica_w 			cpoe_procedimento.cd_pessoa_fisica%type;	

dt_inicio_w					cpoe_procedimento.dt_inicio%type;
ie_duracao_w				cpoe_procedimento.ie_duracao%type;
ie_se_necessario_w			cpoe_procedimento.ie_se_necessario%type;
ie_acm_w					cpoe_procedimento.ie_acm%type;
hr_prim_horario_w			cpoe_procedimento.hr_prim_horario%type;
cd_perfil_ativo_w			cpoe_procedimento.cd_perfil_ativo%type;
dt_atualizacao_nrec_w		cpoe_procedimento.dt_atualizacao_nrec%type;
nm_usuario_nrec_w			cpoe_procedimento.nm_usuario_nrec%type;
ie_futuro_w					cpoe_procedimento.ie_futuro%type;
ie_nivel_atencao_w			cpoe_procedimento.ie_nivel_atencao%type;	
ie_oncologia_w				cpoe_procedimento.ie_oncologia%type;
ie_item_alta_w				cpoe_procedimento.ie_item_alta%type;
cd_funcao_origem_w			cpoe_procedimento.cd_funcao_origem%type;
ie_retrogrado_w				cpoe_procedimento.ie_retrogrado%type;
dt_fim_w					cpoe_procedimento.dt_fim%type;
ie_administracao_w    		cpoe_procedimento.ie_administracao%type;
ie_evento_unico_w     		cpoe_procedimento.ie_evento_unico%type;
nr_seq_cpoe_order_unit_w	cpoe_procedimento.nr_seq_cpoe_order_unit%type;
ie_segunda_w				cpoe_procedimento.ie_segunda%type;
ie_terca_w					cpoe_procedimento.ie_terca%type;
ie_quarta_w					cpoe_procedimento.ie_quarta%type;
ie_quinta_w					cpoe_procedimento.ie_quinta%type;
ie_sexta_w					cpoe_procedimento.ie_sexta%type;
ie_sabado_w					cpoe_procedimento.ie_sabado%type;
ie_domingo_w				cpoe_procedimento.ie_domingo%type;

Cursor C01 is
	Select	
			a.nr_seq_proc_int_adic,
			nvl(a.qt_proc_adic, p.qt_procedimento),
			p.nm_usuario,
			p.cd_intervalo,
			p.ds_dado_clinico, 
			m.cd_material_exame,
			p.ds_horarios,
			p.nr_ocorrencia,
			nvl(a.ie_cobra_paciente,'S'),
			p.dt_prev_execucao,
			a.ie_origem_proced,
			p.cd_medico,
			p.cd_setor_atendimento,
			a.ie_proced_vinculado,
			p.ie_lado,
			substr(a.ds_observacao || '  ' || p.ds_observacao,1,255),
			a.cd_intervalo,
			p.ie_urgencia,
			a.ie_agenda_integrada,
			p.nr_seq_contraste,
			p.ds_justificativa,
			p.ie_anestesia,
			p.ie_duracao
	from	material_exame_lab m,
			proc_int_proc_prescr a,
			cpoe_procedimento p
	where	1 = 1
	and		a.nr_seq_material	= m.nr_sequencia (+) 
	and		((a.cd_estabelecimento is null) or (cd_estabelecimento = cd_estabelecimento_w))
	and 	((nvl(a.ie_permite_duplicado,'N')	= 'S') or 
			 (not exists(	select	1
							from	cpoe_procedimento b
							where	1 = 1
							and		((nvl(a.nr_seq_proc_int_adic,0) = 0) or (b.nr_seq_proc_interno = a.nr_seq_proc_int_adic))	
							and		p.nm_usuario =  nm_usuario_p					   
							and		b.dt_liberacao is null
							and		b.nr_atendimento =  nr_atendimento_w)))

	and		(((qt_idade_pac_w is null) or (a.qt_idade_min is null and a.qt_idade_max is null)) or
			 ((qt_idade_pac_w is not null) and (qt_idade_pac_w between nvl(a.qt_idade_min,qt_idade_pac_w) and nvl(a.qt_idade_max,qt_idade_pac_w))))
    and             ( ((qt_idade_mes_w is null) or (a.qt_idade_mes_min is null and a.qt_idade_mes_max is null)) or
                        (qt_idade_mes_w between nvl(a.qt_idade_min,qt_idade_pac_w) * 12 + nvl(a.qt_idade_mes_min,qt_idade_mes_w) and nvl(a.qt_idade_max,qt_idade_pac_w) * 12 + nvl(a.qt_idade_mes_max,qt_idade_mes_w)))                        
	and		((ie_sexo = ie_sexo_prescr_w) or (ie_sexo is null))
	and 	nvl(a.cd_convenio,cd_convenio_w) = cd_convenio_w
	and		((a.cd_edicao_amb is null) or (a.cd_edicao_amb = cd_edicao_amb_w))
	and		((a.cd_convenio_excluir is null) or (a.cd_convenio_excluir <> cd_convenio_w))
	and		((a.cd_categoria_convenio is null) or (a.cd_categoria_convenio = cd_categoria_w))
	and		((a.cd_plano_convenio	= cd_plano_w) or (a.cd_plano_convenio is null))
	and		((a.cd_medico_prescritor is null) or (a.cd_medico_prescritor = p.cd_medico))
	and		((a.cd_medico_excluir is null) or (a.cd_medico_excluir <> p.cd_medico))
	and 	((a.ie_origem_proc_filtro = ie_origem_proc_filtro_w) or (a.ie_origem_proc_filtro is null))
	and		obter_conv_excluir_proc_assoc(a.nr_sequencia, cd_convenio_w, cd_edicao_amb_w, ie_origem_proc_filtro_w) = 'S'
	and		((cd_perfil is null) or (cd_perfil = obter_perfil_ativo))
	and		nvl(a.ie_situacao,'A')	= 'A'
	and		nvl(a.ie_somente_agenda_int,'N') = 'N'
	and		nvl(a.ie_somente_agenda_cir,'N') = 'N'
	and		a.nr_seq_proc_interno	= p.nr_seq_proc_interno
	and		p.nr_sequencia		= nr_sequencia_p
	and		nvl(a.ie_tipo_convenio,ie_tipo_convenio_w)      = ie_tipo_convenio_w
        and             allow_generate_assoc_amount(p.nr_atendimento,a.NR_SEQ_PROC_INT_ADIC,a.QT_PRESCRICAO_MAX)='S';
	
	
	begin
	
		cd_estabelecimento_w := cd_estabelecimento_p;
		
		
		select	nr_atendimento,
				dt_inicio,
				ie_se_necessario,
				ie_acm,
				hr_prim_horario,
				cd_perfil_ativo,
				cd_pessoa_fisica,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_futuro,
				ie_nivel_atencao,
				ie_oncologia,
				ie_item_alta,
				cd_funcao_origem,
				ie_retrogrado,
				dt_fim,
				ie_administracao,
				ie_evento_unico,
				nr_seq_cpoe_order_unit,
				ie_segunda,
				ie_terca,
				ie_quarta,
				ie_quinta,
				ie_sexta,
				ie_sabado,
				ie_domingo
		into	nr_atendimento_w,
				dt_inicio_w,
				ie_se_necessario_w,
				ie_acm_w,
				hr_prim_horario_w,
				cd_perfil_ativo_w,
				cd_pessoa_fisica_w,
				dt_atualizacao_nrec_w,
				nm_usuario_nrec_w,
				ie_futuro_w,
				ie_nivel_atencao_w,
				ie_oncologia_w,
				ie_item_alta_w,
				cd_funcao_origem_w,
				ie_retrogrado_w,
				dt_fim_w,
				ie_administracao_w,
				ie_evento_unico_w,
				nr_seq_cpoe_order_unit_w,
				ie_segunda_w,
				ie_terca_w,	
				ie_quarta_w,	
				ie_quinta_w,	
				ie_sexta_w,	
				ie_sabado_w,	
				ie_domingo_w
		from	cpoe_procedimento
		where	nr_sequencia = nr_sequencia_p;

		
		select	nvl(to_number(obter_idade(dt_nascimento, sysdate, 'A')),0) qt_idade_ano,
				nvl(to_number(obter_idade(dt_nascimento, sysdate, 'M')),0) qt_idade_mes,
				nvl(to_number(obter_idade(dt_nascimento, sysdate, 'DIA')),0) qt_idade_dia
		into	qt_idade_pac_w,
				qt_idade_mes_w,
				qt_idade_dia_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w;
		
		select 	nvl(max(obter_convenio_atendimento(nr_atendimento_w)),0),
				nvl(max(obter_categoria_atendimento(nr_atendimento_w)),0)
		into	cd_convenio_w,
				cd_categoria_w
		from 	dual;

		select	max(Obter_Plano_Atendimento(nr_atendimento_w,'C'))
		into	cd_plano_w
		from	dual;

		select 	max(ie_tipo_convenio)
		into	ie_tipo_convenio_w
		from 	convenio
		where 	cd_convenio = cd_convenio_w;
		
		select 	max(ie_tipo_atendimento)
		into	ie_tipo_atendimento_w
		from 	atendimento_paciente
		where 	nr_atendimento = nr_atendimento_w;

		ie_origem_proc_filtro_w:= Obter_Origem_Proced_Cat(cd_estabelecimento_w, ie_tipo_atendimento_w, ie_tipo_convenio_w, cd_convenio_w, cd_categoria_w);
		
		select	nvl(max(cd_edicao_amb),0)
		into	cd_edicao_amb_w
		from 	convenio_amb
		where 	(cd_estabelecimento     = cd_estabelecimento_w)
		and 	(cd_convenio            = cd_convenio_w)
		and 	(nvl(ie_situacao,'A')   = 'A')
		and		((nvl(cd_categoria_w,'0') = '0') or (cd_categoria = cd_categoria_w))
		and 	(dt_inicio_vigencia     = 
				(select	max(dt_inicio_vigencia)
				from 	convenio_amb a	
				where 	(a.cd_estabelecimento  	= cd_estabelecimento_w)
				and 	(a.cd_convenio         	= cd_convenio_w)
				and 	(nvl(a.ie_situacao,'A')	= 'A')
				and	((nvl(cd_categoria_w,'0') = '0') or (a.cd_categoria = cd_categoria_w))
				and 	(a.dt_inicio_vigencia 	<=  sysdate)));
		
		open C01;
		loop
		fetch C01 into		
			nr_seq_proc_int_adic_w,
			qt_proc_adic_w,
			nm_usuario_w,
			cd_intervalo_w,
			ds_dado_clinico_w,
			cd_material_exame_w,
			ds_horarios_w,
			nr_ocorrencia_w,
			ie_cobra_paciente_w,
			dt_prev_execucao_w,
			ie_origen_proced_w,
			cd_medico_w,
			cd_setor_prescr_proc_w,
			ie_proced_vinculado_w,
			ie_lado_w,
			ds_observacao_w,
			cd_intervalo_padr_w,
			ie_urgencia_w,
			ie_agenda_integrada_w,
			nr_seq_contraste_w,
			ds_justificativa_w,
			ie_anestesia_w,
			ie_duracao_w;
		exit when C01%notfound;
		
		begin
		
		
		
			select	cpoe_procedimento_seq.nextval
			into	nr_sequencia_w
			from	dual;
																	
			insert	into cpoe_procedimento(
					nr_sequencia,
					nr_seq_proc_interno,
					qt_procedimento,
					dt_atualizacao,
					nm_usuario,
					dt_prev_execucao,
					ie_se_necessario,
					ie_acm,
					nr_ocorrencia,
					cd_medico,
					ie_lado,
					ds_observacao,
					ds_justificativa,
					ie_duracao,
					ds_horarios,
					nr_atendimento,
					dt_inicio,
					hr_prim_horario,
					cd_perfil_ativo,
					cd_pessoa_fisica,
					ds_dado_clinico,
					nr_seq_contraste,
					cd_intervalo,
					ie_urgencia,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					ie_anestesia,
					cd_material_exame,
					cd_setor_atendimento,
					nr_seq_proc_princ,
					ie_exame_assoc,
					ie_cobra_paciente,
					ie_futuro,
					ie_nivel_atencao,
					ie_oncologia,
					ie_item_alta,
					cd_funcao_origem,
					ie_retrogrado,
					dt_fim,
					ie_administracao,
					ie_evento_unico,
					nr_seq_cpoe_order_unit,
					ie_segunda,
					ie_terca,
					ie_quarta,
					ie_quinta,
					ie_sexta,
					ie_sabado,
					ie_domingo
					)
				values	(
					nr_sequencia_w,
					nr_seq_proc_int_adic_w,
					qt_proc_adic_w,
					sysdate,
					nm_usuario_w,
					dt_prev_execucao_w,
					ie_se_necessario_w,
					ie_acm_w,
					nr_ocorrencia_w,
					cd_medico_w,
					ie_lado_w,
					ds_observacao_w,
					ds_justificativa_w,
					ie_duracao_w,
					ds_horarios_w,
					nr_atendimento_w,
					dt_inicio_w,
					hr_prim_horario_w,
					cd_perfil_ativo_w,
					cd_pessoa_fisica_w,
					ds_dado_clinico_w,
					nr_seq_contraste_w,
					nvl(cd_intervalo_padr_w, cd_intervalo_w),
					ie_urgencia_w,
					nm_usuario_nrec_w,
					dt_atualizacao_nrec_w,
					ie_anestesia_w,
					cd_material_exame_w,
					cd_setor_prescr_proc_w,
					decode (ie_proced_vinculado_w, 'S', nr_sequencia_p, null),
					decode(ie_agenda_integrada_w, 'S', 'A', 'S'),
					ie_cobra_paciente_w,
					ie_futuro_w,
					ie_nivel_atencao_w,
					ie_oncologia_w,
					ie_item_alta_w,
					cd_funcao_origem_w,
					ie_retrogrado_w,
					dt_fim_w,
					ie_administracao_w,
					ie_evento_unico_w,
					nr_seq_cpoe_order_unit_w,
					ie_segunda_w,
					ie_terca_w,
					ie_quarta_w,
					ie_quinta_w,
					ie_sexta_w,
					ie_sabado_w,
					ie_domingo_w
					);
									
				cpoe_gerar_med_mat_assoc(	nr_atendimento_w, 
											nr_sequencia_w, 
											nr_seq_proc_int_adic_w, 
											ie_origen_proced_w,
											nr_seq_proc_int_adic_w, 
											cd_estabelecimento_w, 
											cd_perfil_ativo_w, 
											nm_usuario_p,
											cd_pessoa_fisica_w, 
											qt_idade_dia_w, 
											qt_idade_mes_w, 
											qt_idade_pac_w,
											cd_setor_prescr_proc_w,
											ie_tipo_atendimento_w,
											cd_convenio_w,
											cd_medico_w, 
											dt_prev_execucao_w, 
											nvl(cd_intervalo_padr_w, cd_intervalo_w),
											ie_urgencia_w,
											0, 
											0, 
											ds_horarios_w, 
											nr_ocorrencia_w,
											ds_mat_adic_proc_w,
											0);
			
			

		end;
		end loop;
		close C01;	
	commit;
	
end cpoe_gerar_prescr_proc_assoc;
/
