CREATE OR REPLACE
procedure atualiza_ordem_compra_integ(	nr_sequencia_p				number,
					nm_usuario_p				varchar2,
					cd_estab_p				number,
					cd_cgc_fornecedor_p			varchar2,
					cd_condicao_pagamento_p			number,
					cd_comprador_p				varchar2,
					cd_moeda_p				number,
					cd_pessoa_solicitante_p			varchar2,
					cd_cgc_transportador_p			varchar2,
					ie_frete_p				varchar2,
					vl_frete_p				number,
					pr_desconto_p				number,
					pr_desc_pgto_antec_p			number,
					pr_juros_negociado_p			number,
					ds_pessoa_contato_p			varchar2,
					ds_observacao_p 				varchar2,
					cd_local_entrega_p			number,
					dt_entrega_p				date,
					ie_aviso_chegada_p			varchar2,
					vl_despesa_acessoria_p			number,
					nr_seq_subgrupo_compra_p			number,
					pr_desc_financeiro_p			number,
					cd_pessoa_fisica_p			varchar2,
					ie_urgente_p				varchar2,
					nr_seq_forma_pagto_p			number,
					nr_documento_externo_p			number,
					vl_desconto_p				number,
					cd_centro_custo_p			number,
					nr_seq_motivo_cancel_p			number,
					nr_ordem_compra_p		out	number) is



cd_centro_custo_w				number(10);
nr_ordem_compra_w			number(10);
nr_ordem_compra_atual_w			number(10);
nr_item_oci_w				number(5);
cd_material_w				number(6);
cd_unidade_medida_compra_w		varchar2(30);
vl_unitario_material_w			number(13,4);
qt_material_w				number(13,4);
ds_material_direto_w			varchar2(255);
ds_observacao_w				varchar2(255);
ds_observacao_ordem_w			varchar2(4000);
ds_observacao_cancel_w			varchar2(255);
cd_conta_contabil_w			varchar2(20);
ie_de_para_unid_med_w			varchar2(15);
ie_de_para_material_w			varchar2(15);
nr_solic_compra_w				number(10);
nr_item_solic_compra_w			number(5);
nr_cot_compra_w				number(10);
nr_item_cot_compra_w			number(5);
qt_existe_w				number(10);
dt_liberacao_w				date;
nr_ordem_existente_w			number(10);
nr_ano_ordem_w				number(4);
vl_item_liquido_w				number(15,4);
vl_ipi_w					number(15,2);
tx_ipi_w					number(7,4);
cd_ipi_w					number(10);
ds_motivo_cancel_w			varchar2(255);
pr_descontos_w				number(13,4);
vl_desconto_w				number(13,2);
nr_serie_oc_w				number(2);

Cursor C01 is
select	nr_item_oci,
	cd_material,
	cd_unidade_medida_compra,
	vl_unitario_material,
	qt_material,
	ds_material_direto,
	ds_observacao,
	cd_centro_custo,
	cd_conta_contabil,
	nr_solic_compra,
	nr_item_solic_compra,
	nr_cot_compra,
	nr_item_cot_compra,
	nvl(vl_ipi,0),
	nvl(tx_ipi,0),
	nvl(pr_descontos,0),
	nvl(vl_desconto,0)
from	sup_int_oc_item
where	nr_sequencia = nr_sequencia_p;

begin

select	obter_ie_de_para_sup_integr('OC','R','UNIDADE_MEDIDA'),
	obter_ie_de_para_sup_integr('OC','R','MATERIAL')
into	ie_de_para_unid_med_w,
	ie_de_para_material_w
from	dual;

select	to_number(substr(to_char(nr_documento_externo_p),1,length(nr_documento_externo_p)-5)),
	to_number(substr(to_char(nr_documento_externo_p),length(nr_documento_externo_p)-4,1)),
	to_number(substr(to_char(nr_documento_externo_p),length(nr_documento_externo_p)-3,4))
into	nr_ordem_existente_w,
	nr_serie_oc_w,
	nr_ano_ordem_w
from	dual;

select	max(nr_ordem_compra)
into	nr_ordem_compra_w
from	ordem_compra
where	to_number(substr(to_char(nr_documento_externo),1,length(nr_documento_externo)-5)) = nr_ordem_existente_w
and  	to_number(substr(to_char(nr_documento_externo),length(to_char(nr_documento_externo))-4,1)) = nr_serie_oc_w
and	to_number(substr(to_char(nr_documento_externo),length(nr_documento_externo)-3,4)) = nr_ano_ordem_w;

select	dt_liberacao
into	dt_liberacao_w
from	ordem_compra
where	nr_ordem_compra = nr_ordem_compra_w;

nr_ordem_compra_atual_w	:= 0;

if	(dt_liberacao_w is null) and
	(nvl(nr_ordem_compra_w,0) <> 0) then
	begin

	update	ordem_compra
	set	cd_cgc_fornecedor		= cd_cgc_fornecedor_p,
		cd_condicao_pagamento	= cd_condicao_pagamento_p,
		cd_comprador		= cd_comprador_p,
		cd_moeda		= cd_moeda_p,
		cd_pessoa_solicitante	= cd_pessoa_solicitante_p,
		cd_cgc_transportador	= cd_cgc_transportador_p,
		ie_frete			= ie_frete_p,
		vl_frete			= vl_frete_p,
		pr_desconto		= pr_desconto_p,
		pr_desc_pgto_antec	= pr_desc_pgto_antec_p,
		pr_juros_negociado	= pr_juros_negociado_p,
		ds_pessoa_contato	= ds_pessoa_contato_p,
		ds_observacao		= ds_observacao_p,
		cd_local_entrega		= cd_local_entrega_p,
		dt_entrega		= dt_entrega_p,
		ie_aviso_chegada		= ie_aviso_chegada_p,
		vl_despesa_acessoria	= vl_despesa_acessoria_p,
		nr_seq_subgrupo_compra	= nr_seq_subgrupo_compra_p,
		pr_desc_financeiro		= pr_desc_financeiro_p,
		cd_pessoa_fisica		= cd_pessoa_fisica_p,
		ie_urgente		= ie_urgente_p,
		nr_seq_forma_pagto	= nr_seq_forma_pagto_p,
		vl_desconto		= vl_desconto_p,
		cd_centro_custo		= cd_centro_custo_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_ordem_compra		= nr_ordem_compra_w;

	open C01;
	loop
	fetch C01 into
		nr_item_oci_w,
		cd_material_w,
		cd_unidade_medida_compra_w,
		vl_unitario_material_w,
		qt_material_w,
		ds_material_direto_w,
		ds_observacao_w,
		cd_centro_custo_w,
		cd_conta_contabil_w,
		nr_solic_compra_w,
		nr_item_solic_compra_w,
		nr_cot_compra_w,
		nr_item_cot_compra_w,
		vl_ipi_w,
		tx_ipi_w,
		pr_descontos_w,
		vl_desconto_w;
	exit when C01%notfound;
		begin

		/*Conversao para unidade de medida*/
		if	(ie_de_para_unid_med_w = 'C') then
			cd_unidade_medida_compra_w	:= nvl(Obter_Conversao_interna(null,'UNIDADE_MEDIDA','CD_UNIDADE_MEDIDA',cd_unidade_medida_compra_w),cd_unidade_medida_compra_w);
		elsif	(ie_de_para_unid_med_w = 'S') then
			cd_unidade_medida_compra_w	:= nvl(obter_unid_med_sist_ant(cd_unidade_medida_compra_w),cd_unidade_medida_compra_w);
		end if;
		/*Fim*/

		/*Conversao para material*/
		if	(ie_de_para_material_w = 'C') then
			cd_material_w	:= nvl(Obter_Conversao_interna(null,'MATERIAL','CD_MATERIAL',cd_material_w),cd_material_w);
		elsif	(ie_de_para_material_w = 'S') then
			cd_material_w	:= nvl(obter_material_sistema_ant(cd_material_w),cd_material_w);
		end if;
		/*Fim*/

		select	count(*)
		into	qt_existe_w
		from	ordem_compra_item
		where	nr_ordem_compra = nr_ordem_compra_w
		and	nr_item_oci = nr_item_oci_w;

		vl_item_liquido_w	:= round((qt_material_w * vl_unitario_material_w),4);

		if	(qt_existe_w = 0) then
			begin

			insert into ordem_compra_item(
				nr_ordem_compra,
				nr_item_oci,
				cd_material,
				cd_unidade_medida_compra,
				vl_unitario_material,
				qt_material,
				dt_atualizacao,
				nm_usuario,
				ie_situacao,
				cd_pessoa_solicitante,
				pr_descontos,
				cd_local_estoque,
				ds_material_direto,
				ds_observacao,
				vl_item_liquido,
				cd_centro_custo,
				cd_conta_contabil,
				ie_geracao_solic,
				pr_desc_financ,
				qt_original,
				vl_desconto,
				nr_solic_compra,
				nr_item_solic_compra,
				nr_cot_compra,
				nr_item_cot_compra,
				vl_total_item)
			values(	nr_ordem_compra_w,
				nr_item_oci_w,
				cd_material_w,
				cd_unidade_medida_compra_w,
				vl_unitario_material_w,
				qt_material_w,
				sysdate,
				nm_usuario_p,
				'A',
				cd_pessoa_solicitante_p,
				pr_descontos_w,
				cd_local_entrega_p,
				ds_material_direto_w,
				ds_observacao_w,
				round((qt_material_w * vl_unitario_material_w),4),
				cd_centro_custo_w,
				cd_conta_contabil_w,
				'S',
				0,
				qt_material_w,
				vl_desconto_w,
				nr_solic_compra_w,
				nr_item_solic_compra_w,
				nr_cot_compra_w,
				nr_item_cot_compra_w,
				round((qt_material_w * vl_unitario_material_w),4));

			insert into ordem_compra_item_entrega(
				nr_sequencia,
				nr_ordem_compra,
				nr_item_oci,
				dt_prevista_entrega,
				qt_prevista_entrega,
				dt_atualizacao,
				nm_usuario,
				dt_entrega_original,
				dt_entrega_limite)
			values(	ordem_compra_item_entrega_seq.nextval,
				nr_ordem_compra_w,
				nr_item_oci_w,
				dt_entrega_p,
				qt_material_w,
				sysdate,
				nm_usuario_p,
				dt_entrega_p,
				dt_entrega_p);

			end;
		else
			begin

			update	ordem_compra_item
			set	cd_unidade_medida_compra	= cd_unidade_medida_compra_w,
				vl_unitario_material	= vl_unitario_material_w,
				vl_item_liquido		= vl_item_liquido_w,
				qt_material		= qt_material_w,
				ds_material_direto		= ds_material_direto_w,
				ds_observacao		= ds_observacao_w,
				cd_centro_custo		= cd_centro_custo_w,
				cd_conta_contabil		= cd_conta_contabil_w,
				vl_desconto		= vl_desconto_w,
				pr_descontos		= pr_descontos_w,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p,
				vl_total_item = round((qt_material_w * vl_unitario_material_w),4)
			where	nr_ordem_compra		= nr_ordem_compra_w
			and	nr_item_oci		= nr_item_oci_w;

			select	count(*)
			into	qt_existe_w
			from	ordem_compra_item_entrega
			where	nr_ordem_compra	= nr_ordem_compra_w
			and	nr_item_oci	= nr_item_oci_w;

			if	(qt_existe_w = 1) then
				begin

				update	ordem_compra_item_entrega
				set	dt_prevista_entrega	= trunc(dt_entrega_p)
				where	nr_ordem_compra		= nr_ordem_compra_w
				and	nr_item_oci		= nr_item_oci_w;

				end;
			end if;

			end;
		end if;

		/*Tem de ser feito antes de atualizar os tributos, mesmo que o procedimento atualize estes dados*/
		/*pois o percentual ou valor tamb�m pode ter sido alterado*/
		calcular_liquido_ordem_compra(nr_ordem_compra_w,nm_usuario_p);

		if	(vl_ipi_w <> 0) or
			(tx_ipi_w <> 0) then
			begin

			select	vl_item_liquido
			into	vl_item_liquido_w
			from	ordem_compra_item
			where	nr_ordem_compra = nr_ordem_compra_w
			and	nr_item_oci = nr_item_oci_w;

			delete
			from	ordem_compra_item_trib a
			where	a.nr_ordem_compra = nr_ordem_compra_w
			and	a.nr_item_oci = nr_item_oci_w
			and	a.cd_tributo in	(select	x.cd_tributo
						from	tributo x
						where	x.ie_tipo_tributo = 'IPI');

			if	(vl_ipi_w <> 0) and
				(tx_ipi_w <> 0) then
				vl_ipi_w	:= vl_ipi_w;
				tx_ipi_w	:= tx_ipi_w;
			elsif	(vl_ipi_w <> 0) then
				vl_ipi_w	:= vl_ipi_w;
				tx_ipi_w	:= (dividir((vl_ipi_w * 100),vl_item_liquido_w));
			elsif	(tx_ipi_w <> 0) then
				vl_ipi_w	:= (dividir(tx_ipi_w,100) * vl_item_liquido_w);
				tx_ipi_w	:= tx_ipi_w;
			end if;

			select	nvl(max(cd_tributo),0)
			into	cd_ipi_w
			from	tributo
			where	ie_corpo_item = 'I'
			and	ie_situacao = 'A'
			and	(nvl(cd_estabelecimento, cd_estab_p) = cd_estab_p)
			and	ie_tipo_tributo = 'IPI';

			if	(cd_ipi_w <> 0) then
				begin

				insert into ordem_compra_item_trib(
					nr_ordem_compra,
					nr_item_oci,
					cd_tributo,
					pr_tributo,
					vl_tributo,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec) values (
						nr_ordem_compra_w,
						nr_item_oci_w,
						cd_ipi_w,
						tx_ipi_w,
						vl_ipi_w,
						sysdate,
						'INTEGRACAO',
						sysdate,
						'INTEGRACAO');

				end;
			end if;

			end;
		end if;

		end;
	end loop;
	close C01;

	nr_ordem_compra_atual_w	:= nr_ordem_compra_w;

	end;
end if;

if	(nvl(nr_ordem_compra_w,0) <> 0) and
	(nr_seq_motivo_cancel_p is not null) then
	begin

	select	count(*)
	into	qt_existe_w
	from	motivo_cancel_sc_oc
	where	nr_sequencia = nr_seq_motivo_cancel_p;

	if	(qt_existe_w > 0) then
		begin

		select	ds_motivo
		into	ds_motivo_cancel_w
		from	motivo_cancel_sc_oc
		where	nr_sequencia = nr_seq_motivo_cancel_p;

		ds_observacao_cancel_w  :=	WHEB_MENSAGEM_PCK.get_texto(297532,'DT_CANCELAMENTO_W=' || to_char(sysdate,'dd/mm/yyyy hh24:mi:ss') || ';' ||
										'DS_MOTIVO_CANCEL_W=' || ds_motivo_cancel_w);

		select	ds_observacao
		into	ds_observacao_ordem_w
		from	ordem_compra
		where	nr_ordem_compra = nr_ordem_compra_w;

		if	(ds_observacao_ordem_w is not null) then
			ds_observacao_ordem_w	:= substr(ds_observacao_ordem_w || chr(13) || ds_observacao_cancel_w,1,4000);
		end if;

		update	ordem_compra
		set	nr_seq_motivo_cancel	= nr_seq_motivo_cancel_p,
			nm_usuario		= 'INTEGRACAO',
			dt_atualizacao		= sysdate,
			ds_observacao		= ds_observacao_ordem_w
		where	nr_ordem_compra		= nr_ordem_compra_w;

		Baixar_ordem_compra(nr_ordem_compra_w, null, null, 'INTEGRACAO');

		nr_ordem_compra_atual_w	:= nr_ordem_compra_w;

		end;
	else
		begin

		inserir_historico_ordem_compra(
			nr_ordem_compra_w,
			'S',
			WHEB_MENSAGEM_PCK.get_texto(297533),
			WHEB_MENSAGEM_PCK.get_texto(297534,'NR_SEQ_MOTIVO_CANCEL_P=' || to_char(nr_seq_motivo_cancel_p)),
			'INTEGRACAO');

		end;
	end if;

	end;
end if;

nr_ordem_compra_p	:= nr_ordem_compra_atual_w;

end atualiza_ordem_compra_integ;
/
