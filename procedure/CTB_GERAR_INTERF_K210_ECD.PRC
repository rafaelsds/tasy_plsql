CREATE OR REPLACE procedure ctb_gerar_interf_k210_ecd(
			nr_seq_controle_p			number,
			nm_usuario_p			Varchar2,
			cd_estabelecimento_p		number,
			dt_inicio_p			date,
			dt_fim_p				date,
			cd_empresa_p			number,
			cd_conta_contabil_p		varchar2,
			qt_linha_p		in out	number,
			nr_sequencia_p		in out	number) is

ie_ind_movimento_w	varchar2(2);
ds_arquivo_w		varchar2(4000);
ds_compl_arquivo_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_linha_w		number(10) 	:= qt_linha_p;
nr_seq_registro_w		number(10) 	:= nr_sequencia_p;
sep_w			varchar(1) 	:= '|';
tp_registro_w		varchar(15) 	:= 'K210';
cd_empresa_w		grupo_emp_estrutura.cd_empresa%type;
cd_empresa_ww		conta_contabil.cd_empresa%type;
cd_conta_contabil_w	varchar2(40); 
cd_classificacao_w		conta_contabil.cd_classificacao%type;
ie_apres_conta_ctb_w	ctb_regra_sped.ie_apres_conta_ctb%type;

cursor c01 is
	select	a.cd_empresa
	from	grupo_emp_estrutura a
	where	a.nr_seq_grupo	= holding_pck.get_grupo_emp_estrut_vigente(cd_empresa_p)
	and     a.cd_empresa <> cd_empresa_p
	order by cd_empresa;

cursor c02 is
	select	cc1.cd_empresa,
		cc1.cd_conta_contabil,
		substr(ctb_obter_classif_conta(cc1.cd_conta_contabil, cc1.cd_classificacao, dt_fim_p),1,40) cd_classificacao --- apenas analíticas  'A,1.01.01' 
	from	conta_contabil cc1
	where	cc1.cd_empresa = cd_empresa_w --(filha)
	and	cc1.cd_conta_referencia = cd_conta_contabil_p -- Conta Contábil da Controladora
	and	cc1.ie_tipo = 'A'
	and cc1.ie_situacao = 'A';

begin

select	nvl(max(a.ie_apres_conta_ctb),'CD')
into	ie_apres_conta_ctb_w
from 	ctb_regra_sped a,
		ctb_sped_controle b
where	b.nr_seq_regra_sped 	= a.nr_sequencia
and	b.nr_sequencia		= nr_seq_controle_p;

open C01;
loop
fetch C01 into
	cd_empresa_w;
exit when C01%notfound;
	begin

	open c02;
	loop
	fetch c02 into	
		cd_empresa_ww,
		cd_conta_contabil_w,
		cd_classificacao_w;
	exit when c02%notfound;
		begin
		
		if	(ie_apres_conta_ctb_w = 'CL') then
			begin
			cd_conta_contabil_w	:= cd_classificacao_w;
			end;
		elsif	(ie_apres_conta_ctb_w = 'CP') then
			begin
			cd_conta_contabil_w	:= substr(replace(cd_classificacao_w,'.',''),1,40);
			end;
		end if;

		ds_linha_w	:= substr(sep_w || tp_registro_w 	|| sep_w || 
					cd_empresa_ww 		|| sep_w ||
					cd_conta_contabil_w 	|| sep_w,1,8000);

		ds_arquivo_w		:= substr(ds_linha_w,1,4000);
		ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
		nr_seq_registro_w		:= nr_seq_registro_w + 1;
		nr_linha_w		:= nr_linha_w + 1;

		insert into ctb_sped_registro(
				nr_sequencia,
				ds_arquivo,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_controle_sped,
				ds_arquivo_compl,
				cd_registro,
				nr_linha)
			values(
				nr_seq_registro_w,
				ds_arquivo_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_controle_p,
				ds_compl_arquivo_w,
				tp_registro_w,
				nr_linha_w);

		end;
	end loop;
	close c02;

	end;
end loop;
close C01;

commit;
qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;
end ctb_gerar_interf_k210_ecd;
/