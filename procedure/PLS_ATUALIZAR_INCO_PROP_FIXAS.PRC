create or replace
procedure pls_atualizar_inco_prop_fixas
			(	nr_seq_proposta_p	pls_proposta_adesao.nr_sequencia%type,
				nm_usuario_p		varchar2 ) is

/* Campos do cursor 1 */
nr_seq_benef_prop_w		pls_proposta_beneficiario.nr_sequencia%type;
nr_seq_titular_w		pls_proposta_beneficiario.nr_seq_titular%type;
nr_seq_titular_contrato_w	pls_proposta_beneficiario.nr_seq_titular_contrato%type;
dt_admissao_w			pls_proposta_beneficiario.dt_admissao%type;
cd_beneficiario_w		pls_proposta_beneficiario.cd_beneficiario%type;
nr_seq_parentesco_w		pls_proposta_beneficiario.nr_seq_parentesco%type;
dt_cancelamento_w		pls_proposta_beneficiario.dt_cancelamento%type;
nr_seq_motivo_cancelamento_w	pls_proposta_beneficiario.nr_seq_motivo_cancelamento%type;
nr_seq_plano_w			pls_proposta_beneficiario.nr_seq_plano%type;
dt_contratacao_w		pls_proposta_beneficiario.dt_contratacao%type;
nr_seq_pagador_w		pls_proposta_beneficiario.nr_seq_pagador%type;
ie_grau_parentesco_w		grau_parentesco.ie_grau_parentesco%type;
nr_seq_subestipulante_w		pls_proposta_beneficiario.nr_seq_subestipulante%type;

/* Campos da tabela PESSOA_FISICA */
dt_nascimento_w			pessoa_fisica.dt_nascimento%type;
ie_sexo_w			pessoa_fisica.ie_sexo%type;
nr_telefone_celular_w		pessoa_fisica.nr_telefone_celular%type;
nr_cpf_w			pessoa_fisica.nr_cpf%type;
qt_idade_w			pls_integer;
ie_brasileiro_w			varchar2(1);
nm_pessoa_fisica_w		pessoa_fisica.nm_pessoa_fisica%type;
cd_declaracao_nasc_vivo_w	pessoa_fisica.cd_declaracao_nasc_vivo%type;
nr_cartao_nac_sus_w		pessoa_fisica.nr_cartao_nac_sus%type;
nr_identidade_w			pessoa_fisica.nr_identidade%type;
nr_reg_geral_estrang_w		pessoa_fisica.nr_reg_geral_estrang%type;
ds_orgao_emissor_ci_w		pessoa_fisica.ds_orgao_emissor_ci%type;
dt_emissao_ci_w			pessoa_fisica.dt_emissao_ci%type;
sg_emissora_ci_w		pessoa_fisica.sg_emissora_ci%type;
nr_seq_pais_w			pessoa_fisica.nr_seq_pais%type;
nr_pis_pasep_w			pessoa_fisica.nr_pis_pasep%type;
cd_municipio_ibge_nasc_w	pessoa_fisica.cd_municipio_ibge%type;
ie_estado_civil_w		pessoa_fisica.ie_estado_civil%type;
cd_estabelecimento_w		pessoa_fisica.cd_estabelecimento%type;
nr_cpf_contrato_grupo_w		pessoa_fisica.nr_cpf%type;
dt_emissao_cert_casamento_w	pessoa_fisica.dt_emissao_cert_casamento%type;

/* Campos da tabela COMPL_PESSOA_FISICA */
nr_telefone_w			compl_pessoa_fisica.nr_telefone%type;
ds_endereco_w			compl_pessoa_fisica.ds_endereco%type;
sg_estado_w			compl_pessoa_fisica.sg_estado%type;
cd_cep_w			compl_pessoa_fisica.cd_cep%type;
nr_endereco_w			compl_pessoa_fisica.nr_endereco%type;
cd_municipio_ibge_w		compl_pessoa_fisica.cd_municipio_ibge%type;

/* Campos do titular */
nm_titular_w			pessoa_fisica.nm_pessoa_fisica%type;
nm_titular_contrato_w		pessoa_fisica.nm_pessoa_fisica%type;
nr_seq_plano_titular_w		pls_proposta_beneficiario.nr_seq_plano%type;
nr_seq_plano_titular_contr_w	pls_segurado.nr_seq_plano%type;
dt_contratacao_titular_w	pls_proposta_beneficiario.dt_contratacao%type;
dt_contratacao_titular_contr_w	pls_segurado.dt_contratacao%type;
nr_cpf_titular_w		pessoa_fisica.nr_cpf%type;
nr_cpf_titular_contrato_w	pessoa_fisica.nr_cpf%type;
nr_seq_subestipulante_tit_w	pls_proposta_beneficiario.nr_seq_subestipulante%type;
nr_seq_subestipulante_tit_co_w	pls_segurado.nr_seq_subestipulante%type;

/* Outros campos */
ds_email_w			varchar2(255);
qt_beneficiarios_w		pls_integer;
qt_inconsist_ativas_w		pls_integer;
nr_contrato_w			pls_proposta_adesao.nr_seq_contrato%type;
ie_titularidade_w		varchar2(1);
qt_contrato_pj_w		pls_integer;
nm_mae_w			varchar2(255);
nm_pai_w			varchar2(255);
ie_uf_existe_w			varchar2(1);
ie_nome_benef_invalido_w	varchar2(10);
ie_nome_mae_invalido_w		varchar2(10);
cd_cgc_estipulante_w		pls_contrato.cd_cgc_estipulante%type;
cd_cep_estip_w			pessoa_juridica.cd_cep%type;
nr_endereco_estip_w		pessoa_juridica.nr_endereco%type;
ds_erro_w			varchar2(255);
qt_caracteres_pis_w		number(15);
nr_digito_w			varchar2(2);
cd_pis_pasep_w			varchar2(2);
qt_inco_vidas_w			number(10);
qt_idade_proposta_cpf_w		pls_parametros.qt_idade_proposta_cpf%type;
nr_seq_pagador_titular_w	pls_proposta_beneficiario.nr_seq_pagador%type;
cd_cgc_pagador_w		pls_proposta_pagador.cd_cgc_pagador%type;
cd_pagador_w			pls_proposta_pagador.cd_pagador%type;
nr_seq_contrato_pagador_w	pls_proposta_pagador.nr_seq_contrato_pagador%type;
ie_pagador_contrato_princ_w	pls_parametros.ie_pagador_contrato_princ%type;
nr_contrato_principal_w		pls_contrato.nr_contrato_principal%type;
nr_seq_pagador_benef_w		pls_contrato_pagador.nr_sequencia%type;
nr_seq_contrato_w		pls_contrato.nr_sequencia%type;
qt_plano_contrato_w		number(10);
nr_seq_subestipulante_ww	pls_proposta_beneficiario.nr_seq_subestipulante%type;
ie_possui_sca_w			number(3);
nr_seq_plano_sca_w		number(10);
ie_inclusao_portabilidade_w	varchar2(1);
qt_anexos_w			pls_integer;
qt_celular_w			pls_integer;
ie_permite_inc_fora_atuacao_w	pls_regra_benef_contrato.ie_permite_inc_fora_atuacao%type;				
nr_seq_sca_vinculo_w		pls_sca_vinculo.nr_sequencia%type;
qt_titular_ativo_w  number(10);

Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_titular,
		a.nr_seq_titular_contrato,
		a.dt_admissao,
		a.cd_beneficiario,
		a.nr_seq_parentesco,
		a.dt_cancelamento,
		a.nr_seq_motivo_cancelamento,
		a.nr_seq_plano,
		a.dt_contratacao,
		a.nr_seq_pagador,
		a.nr_seq_subestipulante,
		nvl((	select	max(x.ie_portabilidade)
			from	pls_motivo_inclusao_seg x
			where	x.nr_sequencia	= a.nr_seq_motivo_inclusao),'N') ie_inclusao_portabilidade
	from	pls_proposta_beneficiario a
	where	a.nr_seq_proposta = nr_seq_proposta_p;

Cursor C02 is
	select	nr_sequencia,
		nr_seq_plano
	from	pls_sca_vinculo
	where	nr_seq_benef_proposta	= nr_seq_benef_prop_w;

begin

delete	from pls_proposta_validacao
where	nr_seq_proposta = nr_seq_proposta_p
and	nr_seq_inconsist_fixa is not null
and	dt_liberacao is null;

select	count(1)
into	qt_inconsist_ativas_w
from	pls_inconsistencia_inc_seg
where	ie_utilizacao in ('A','P')
and	ie_situacao = 'A'
and	cd_inconsistencia in (3,8,9,10,11,12,13,14,15,16,17,18,21,22,24,25,27,28,30,31,33,35,37,39,43,44,46,49,50,51,52,53,54,58,59,60,61,62,63,64,65,66,68,69,70,82,83,84,85,86,87);

if	(qt_inconsist_ativas_w > 0) then
	select	nvl(max(qt_idade_proposta_cpf),0)
	into	qt_idade_proposta_cpf_w
	from	pls_parametros
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
	
	select	max(nr_seq_contrato)
	into	nr_contrato_w
	from	pls_proposta_adesao
	where	nr_sequencia = nr_seq_proposta_p;
	
	if	(nr_contrato_w is not null) then
		select	max(nr_sequencia)
		into	nr_seq_contrato_w
		from	pls_contrato
		where	nr_contrato = nr_contrato_w;
	end if;
	
	select	nvl(max(a.ie_permite_inc_fora_atuacao),'N')
	into	ie_permite_inc_fora_atuacao_w
	from	pls_regra_benef_contrato a
	where	a.nr_seq_contrato	= nr_seq_contrato_w;
	
	open C01;
	loop
	fetch C01 into
		nr_seq_benef_prop_w,
		nr_seq_titular_w,
		nr_seq_titular_contrato_w,
		dt_admissao_w,
		cd_beneficiario_w,
		nr_seq_parentesco_w,
		dt_cancelamento_w,
		nr_seq_motivo_cancelamento_w,
		nr_seq_plano_w,
		dt_contratacao_w,
		nr_seq_pagador_w,
		nr_seq_subestipulante_w,
		ie_inclusao_portabilidade_w;
	exit when C01%notfound;
		begin
		
		if	(nr_seq_titular_w is null and nr_seq_titular_contrato_w is null) then
			ie_titularidade_w := 'T';
		else
			ie_titularidade_w := 'D';
		end if;
		
		begin
		select	dt_nascimento,
			ie_sexo,
			nr_telefone_celular,
			nr_cpf,
			substr(obter_idade_pf(cd_pessoa_fisica, sysdate, 'A'),1,100) qt_idade,
			obter_se_brasileiro(cd_pessoa_fisica) ie_brasileiro,
			nm_pessoa_fisica,
			cd_declaracao_nasc_vivo,
			nr_cartao_nac_sus,
			nr_identidade,
			nr_reg_geral_estrang,
			ds_orgao_emissor_ci,
			dt_emissao_ci,
			sg_emissora_ci,
			nr_seq_pais,
			nr_pis_pasep,
			cd_municipio_ibge,
			ie_estado_civil,
			cd_estabelecimento,
			dt_emissao_cert_casamento
		into	dt_nascimento_w,
			ie_sexo_w,
			nr_telefone_celular_w,
			nr_cpf_w,
			qt_idade_w,
			ie_brasileiro_w,
			nm_pessoa_fisica_w,
			cd_declaracao_nasc_vivo_w,
			nr_cartao_nac_sus_w,
			nr_identidade_w,
			nr_reg_geral_estrang_w,
			ds_orgao_emissor_ci_w,
			dt_emissao_ci_w,
			sg_emissora_ci_w,
			nr_seq_pais_w,
			nr_pis_pasep_w,
			cd_municipio_ibge_nasc_w,
			ie_estado_civil_w,
			cd_estabelecimento_w,
			dt_emissao_cert_casamento_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_beneficiario_w;
		exception
		when others then
			dt_nascimento_w			:= null;
			ie_sexo_w			:= null;
			nr_telefone_celular_w		:= null;
			nr_cpf_w			:= null;
			qt_idade_w			:= null;
			ie_brasileiro_w			:= null;
			nm_pessoa_fisica_w		:= null;
			cd_declaracao_nasc_vivo_w	:= null;
			nr_cartao_nac_sus_w		:= null;
			nr_identidade_w			:= null;
			nr_reg_geral_estrang_w		:= null;
			ds_orgao_emissor_ci_w		:= null;
			dt_emissao_ci_w			:= null;
			sg_emissora_ci_w		:= null;
			nr_seq_pais_w			:= null;
			nr_pis_pasep_w			:= null;
			cd_municipio_ibge_nasc_w	:= null;
			ie_estado_civil_w		:= null;
			cd_estabelecimento_w		:= null;
			dt_emissao_cert_casamento_w	:= null;
		end;
		
		begin
		select	ds_email,
			nr_telefone,
			ds_endereco,
			sg_estado,
			cd_cep,
			nvl(nr_endereco, ds_compl_end) nr_endereco,
			cd_municipio_ibge
		into	ds_email_w,
			nr_telefone_w,
			ds_endereco_w,
			sg_estado_w,
			cd_cep_w,
			nr_endereco_w,
			cd_municipio_ibge_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica = cd_beneficiario_w
		and	ie_tipo_complemento = 1;
		exception
		when others then
			ds_email_w		:= null;
			nr_telefone_w		:= null;
			ds_endereco_w		:= null;
			sg_estado_w		:= null;
			cd_cep_w		:= null;
			nr_endereco_w		:= null;
			cd_municipio_ibge_w	:= null;
		end;
		
		begin
		select	nvl(obter_compl_pf(cd_beneficiario_w, 5, 'NPR'), obter_nome_contato(cd_beneficiario_w, 5)),
			nvl(obter_compl_pf(cd_beneficiario_w, 4, 'NPR'), obter_nome_contato(cd_beneficiario_w, 4))
		into	nm_mae_w,
			nm_pai_w
		from	dual;
		exception
		when others then
			nm_mae_w := null;
			nm_pai_w := null;
		end;
		
		begin
		select	substr(a.nm_pessoa_fisica,1,255),
			b.nr_seq_plano,
			b.dt_contratacao,
			a.nr_cpf,
			b.nr_seq_subestipulante
		into	nm_titular_w,
			nr_seq_plano_titular_w,
			dt_contratacao_titular_w,
			nr_cpf_titular_w,
			nr_seq_subestipulante_tit_w
		from	pessoa_fisica			a,
			pls_proposta_beneficiario	b
		where	a.cd_pessoa_fisica = b.cd_beneficiario
		and	b.nr_sequencia = nr_seq_titular_w;
		exception
		when others then
			nm_titular_w			:= null;
			nr_seq_plano_titular_w		:= null;
			dt_contratacao_titular_w	:= null;
			nr_cpf_titular_w		:= null;
			nr_seq_subestipulante_tit_w	:= null;
		end;
		
		begin
		select	a.nm_pessoa_fisica,
			b.nr_seq_plano,
			b.dt_contratacao,
			a.nr_cpf,
			b.nr_seq_subestipulante
		into	nm_titular_contrato_w,
			nr_seq_plano_titular_contr_w,
			dt_contratacao_titular_contr_w,
			nr_cpf_titular_contrato_w,
			nr_seq_subestipulante_tit_co_w
		from	pessoa_fisica	a,
			pls_segurado	b
		where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
		and	b.nr_sequencia = nr_seq_titular_contrato_w;
		exception
		when others then
			nm_titular_contrato_w		:= null;
			nr_seq_plano_titular_contr_w	:= null;
			dt_contratacao_titular_contr_w	:= null;
			nr_cpf_titular_contrato_w	:= null;
			nr_seq_subestipulante_tit_co_w	:= null;
		end;
		
		-- 3 - Motivo de rescisao do beneficiario nao informado 
		if	(dt_cancelamento_w is not null) and
			(nr_seq_motivo_cancelamento_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 3, null, nm_usuario_p);
		end if;
		
		-- 8 - Data de nascimento nao informada ou invalida
		if	((dt_nascimento_w is null) or (dt_nascimento_w < to_date('01/01/1890', 'dd/mm/rrrr')) or (dt_nascimento_w > sysdate)) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 8, null,nm_usuario_p);
		end if;
		
		-- 9 - Nome da mae nao informado
		if	(nm_mae_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 9, null,nm_usuario_p);
		end if;
		
		-- 10 - Grau de parentesco nao informado
		if	(nr_seq_parentesco_w is null) and 
			(ie_titularidade_w = 'D') then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 10, null,nm_usuario_p);
		end if;
		
		-- 11 - Titular nao informado
		if	(nr_seq_parentesco_w is not null) and
			(ie_titularidade_w = 'T') then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 11, null, nm_usuario_p);
		end if;
		
		-- 12 - Sexo nao informado
		if	(ie_sexo_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 12, null, nm_usuario_p);
		end if;
		
		-- 13 - Telefone nao informado
		if	(nvl(nr_telefone_w, nr_telefone_celular_w) is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 13, null, nm_usuario_p);
		end if;
		
		-- 14 - CPF nao informado (Titular e dependente acima de 16 anos)
		if	(nr_cpf_w is null and (qt_idade_w >= 16 or (ie_titularidade_w = 'T'))) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 14, null, nm_usuario_p);
		end if;
		
		-- 15 - Endereco nao informado
		if	(ds_endereco_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 15, null, nm_usuario_p);
		end if;
		
		ie_uf_existe_w := 'N';
		
		if	(sg_estado_w is not null) then
			select	pls_obter_se_uf_existe(sg_estado_w, '1')
			into	ie_uf_existe_w
			from	dual;
		end if;
		
		-- 16 - UF do Beneficiario nao informado ou invalido
		if	(sg_estado_w is null) or (ie_uf_existe_w = 'N')then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 16, null,nm_usuario_p);
		end if;
		
		-- 17 - Nome do beneficiario ou da mae invalido - Apenas para brasileiros (nao inserir com abreviacoes) 
		if	(nvl(ie_brasileiro_w,'S') = 'S') then
			ie_nome_benef_invalido_w	:= pls_consistir_letra_unica_pf(nm_pessoa_fisica_w);
			ie_nome_mae_invalido_w		:= pls_consistir_letra_unica_pf(nm_mae_w);
			
			if	(ie_nome_mae_invalido_w = 'S') or
				(ie_nome_benef_invalido_w = 'S') then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 17, null,nm_usuario_p);
			end if;
		end if;
		
		-- 21 - Numero da declaracao de nascido vivo nao informado
		if	(cd_declaracao_nasc_vivo_w is null) and (dt_nascimento_w >= to_date('01/01/2010', 'dd/mm/rrrr')) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 21, null, nm_usuario_p);
		end if;
		
		-- 22 - Grau de parentesco do beneficiario ultrapassou o limite permitido por familia
		pls_consistir_grau_paren_max(nr_seq_proposta_p,'P',cd_estabelecimento_w,nm_usuario_p,ds_erro_w);
		if	(ds_erro_w is not null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 22, null, nm_usuario_p);
		end if;
		
		-- 24 - CPF informado invalido 
		if	(trim(nr_cpf_w) is not null) and
			(obter_se_cpf_valido(nr_cpf_w) = 'N') then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 24, null, nm_usuario_p);
		end if;
		
		-- 25 - Cartao do SUS informado invalido
		if	(trim(nr_cartao_nac_sus_w) is not null) and
			(sus_obter_se_cns_inconsistente(nr_cartao_nac_sus_w) = 'S') then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 25, null,nm_usuario_p);
		end if;
		
		-- 27 - Nome do titular informado igual ao nome do proprio beneficiario
		if	(upper(trim(nm_pessoa_fisica_w)) = upper(trim(nm_titular_w))) or
			(upper(trim(nm_pessoa_fisica_w)) = upper(trim(nm_titular_contrato_w))) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 27, null,nm_usuario_p);
		end if;
		
		-- 28 - Codigo do municipio IBGE nao informado
		if	(cd_municipio_ibge_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 28, null,nm_usuario_p);
		end if;
		
		-- 30 - Documento de identificacao nao informado (Somente acima de 18 anos)
		if	(trim(nr_identidade_w) is null) and
			(trim(nr_reg_geral_estrang_w) is null) and
			(qt_idade_w >= 18) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 30, null,nm_usuario_p);
		end if;
		
		-- 31 - Orgao emissor do documento de identificacao nao informado (Sera consistido somente quando o RG estiver informado)
		if	(trim(nr_identidade_w) is not null) and
			(trim(ds_orgao_emissor_ci_w) is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 31, null,nm_usuario_p);
		end if;
		
		-- 33 - CPF nao informado (Dependente menor de 16 anos)
		if	(nr_cpf_w is null) and 
			(qt_idade_w < 16) and 
			(ie_titularidade_w = 'D') then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 33, null,nm_usuario_p);
		end if;
		
		-- 35 - Nao foram informados todos os dados referente ao RG (Sera consistido somente quando um dos campos for informado)
		if	(trim(nr_identidade_w) is not null) or
			(trim(ds_orgao_emissor_ci_w) is not null) or
			(trim(dt_emissao_ci_w) is not null) or
			(trim(sg_emissora_ci_w) is not null) or
			(trim(nr_seq_pais_w) is not null) then
			if	(trim(nr_identidade_w) is null) or
				(trim(ds_orgao_emissor_ci_w) is null) or
				(trim(dt_emissao_ci_w) is null) or
				(trim(sg_emissora_ci_w) is null) or
				(trim(nr_seq_pais_w) is null) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 35, null,nm_usuario_p);
			end if;
		end if;
		
		-- 37 - Digito do PIS informado incorreto
		if	(nr_pis_pasep_w is not null) then
			qt_caracteres_pis_w := length(nr_pis_pasep_w);
			
			select	calcula_digito('Modulo11',substr(nr_pis_pasep_w, 1, qt_caracteres_pis_w - 1))
			into	nr_digito_w
			from	dual;
			
			cd_pis_pasep_w	:= substr(nr_pis_pasep_w, qt_caracteres_pis_w, qt_caracteres_pis_w);
			
			if	(nr_digito_w <> cd_pis_pasep_w) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 37, null,nm_usuario_p);
			end if;
		end if;
		
		-- 39 - Beneficiario titular com grau de parentesco informado
		if	(ie_titularidade_w = 'T') and
			(nr_seq_parentesco_w is not null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 39, null, nm_usuario_p);
		end if;
		
		-- 43 - Nome do pai invalido
		if	(pls_consistir_letra_unica_pf(nm_pai_w) = 'S') then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 43, null, nm_usuario_p);
		end if;
		
		-- 44 - Municipio de nascimento nao encontrado
		if	(ie_brasileiro_w = 'S') and
			(cd_municipio_ibge_nasc_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 44, null, nm_usuario_p);
		end if;
		
		-- 46 - Produto informado para o beneficiario e diferente do produto do titular
		if	(ie_titularidade_w = 'D') then
			nr_seq_plano_titular_w := nvl(nr_seq_plano_titular_w, nr_seq_plano_titular_contr_w);
			if	(nr_seq_plano_titular_w is not null) and
				(nr_seq_plano_w <> nr_seq_plano_titular_w) then 
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 46, null,nm_usuario_p);
			end if;
		end if;
		
		-- 49 - Cartao do SUS nao informado
		if	(nr_cartao_nac_sus_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 49, null,nm_usuario_p);
		end if;
		
		-- 50 - Estado civil nao informado
		if	(ie_estado_civil_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 50, null,nm_usuario_p);
		end if;
		
		-- 51 - Numero da declaracao de nascido vivo invalido
		if	((cd_declaracao_nasc_vivo_w is not null) and
			((obter_se_somente_numero(cd_declaracao_nasc_vivo_w) = 'N') or (length(cd_declaracao_nasc_vivo_w) <> 11))) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 51, null,nm_usuario_p);
		end if;
		
		-- 52 - Data de adesao anterior a data de adesao do titular
		if	(dt_contratacao_w is not null) and
			(ie_titularidade_w = 'D') then
			dt_contratacao_titular_w := nvl(dt_contratacao_titular_w, dt_contratacao_titular_contr_w);
			if	(dt_contratacao_titular_w is not null) and
				(trunc(dt_contratacao_w,'dd') < trunc(dt_contratacao_titular_w,'dd')) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 52, null, nm_usuario_p);
			end if;
		end if;
		
		select	count(1)
		into	qt_contrato_pj_w
		from	pls_contrato
		where	nr_contrato = nr_contrato_w
		and	cd_cgc_estipulante is not null;
		
		if	(qt_contrato_pj_w > 0) then
			-- 18 - Beneficiario com o mesmo endereco da empresa
			if	(cd_cep_w is not null) and (nr_endereco_w is not null) then
				select	cd_cgc_estipulante
				into	cd_cgc_estipulante_w
				from	pls_contrato
				where	nr_contrato = nr_contrato_w;
				
				begin
				select	nvl(cd_cep,'X'),
					nvl(nr_endereco,'X')
				into	cd_cep_estip_w,
					nr_endereco_estip_w
				from	pessoa_juridica
				where	cd_cgc	= cd_cgc_estipulante_w;
				exception
				when others then
					cd_cep_estip_w		:= 'X';
					nr_endereco_estip_w	:= 'X';
				end;
				
				if	(cd_cep_w = cd_cep_estip_w) and (to_char(nr_endereco_w)	= nr_endereco_estip_w) then
					pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 18, null, nm_usuario_p);
				end if;
			end if;
			
			-- 53 - Data de admissao do titular nao informada ou invalida
			if	(ie_titularidade_w = 'T') then
				if	(dt_admissao_w is null) or (dt_admissao_w < dt_nascimento_w) then 
					pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 53, null, nm_usuario_p);
				end if;
			end if;
		end if;
		
		-- 54 - CPF informado e o mesmo do titular
		if	(nr_cpf_w is not null) then
			nr_cpf_titular_w := nvl(nr_cpf_titular_w, nr_cpf_titular_contrato_w);
			if	(nr_cpf_titular_w is not null) and
				(nr_cpf_titular_w = nr_cpf_w) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 54, null,nm_usuario_p);
			end if;
		end if;
		
		--58 Beneficiario com CPF informado ja ativo dentro do grupo contratual (Valido apenas para contratos com grupos contratuais)
		if	(nr_cpf_w is not null) then
			if	(pls_consiste_contrato_grupo(nr_seq_contrato_w, nr_cpf_w, dt_nascimento_w) = 'S') then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 58, null, nm_usuario_p);
			end if;
		end if;
		
		--59 Quantidade de vidas da proposta incompativel com quantidade definida em regra
		select	count(*)
		into	qt_inco_vidas_w
		from	pls_proposta_validacao a,
			pls_inconsistencia_inc_seg b
		where	a.nr_seq_inconsist_fixa = b.nr_sequencia
		and	a.nr_seq_proposta = nr_seq_proposta_p 
		and	b.cd_inconsistencia = 59;
		
		if	(qt_inco_vidas_w = 0) then
			pls_consistir_regra_qt_vidas(nr_seq_proposta_p, nm_usuario_p);
		end if;
		
		--60 CPF nao informado (Dependentes menor de @IDADE anos)
		if	(ie_titularidade_w = 'D') then
			if	(nr_cpf_w is null) and
				(qt_idade_w >= qt_idade_proposta_cpf_w) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 60, null,nm_usuario_p);
			end if;
		end if;
		
		--61 - Produto nao informado
		if	(nr_seq_plano_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 61, null,nm_usuario_p);
		end if;
		
		--62 - Data de casamento nao informada
		if	(ie_estado_civil_w = '2') and
			(dt_emissao_cert_casamento_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 62, null,nm_usuario_p);
		end if;
		
		--63 - Pagador informado para o beneficiario e diferente do pagador do titular
		if	(ie_titularidade_w = 'D') then
			if (nr_seq_titular_contrato_w is not null) then
				select	max(nr_seq_pagador)
				into	nr_seq_pagador_titular_w
				from	pls_segurado
				where	nr_sequencia = nr_seq_titular_contrato_w;
				
				if	(nr_seq_pagador_w is not null) then
					select	cd_cgc_pagador,
						cd_pagador,
						nr_seq_contrato_pagador
					into	cd_cgc_pagador_w,
						cd_pagador_w,
						nr_seq_contrato_pagador_w
					from	pls_proposta_pagador
					where	nr_sequencia	= nr_seq_pagador_w;
					
					if	(nr_seq_contrato_pagador_w is not null) then
						nr_seq_pagador_w := nr_seq_contrato_pagador_w;
					else
						select	max(ie_pagador_contrato_princ)
						into	ie_pagador_contrato_princ_w
						from	pls_parametros
						where	cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento;
						
						ie_pagador_contrato_princ_w	:= nvl(ie_pagador_contrato_princ_w,'S');
						
						select	max(nr_contrato_principal)
						into	nr_contrato_principal_w
						from	pls_contrato
						where	nr_sequencia	= nr_seq_contrato_w;
						
						if	(ie_pagador_contrato_princ_w = 'S') then
							if	(nr_contrato_principal_w is not null) then
								select	max(b.nr_sequencia)
								into	nr_seq_pagador_benef_w
								from	pls_contrato_pagador	b,
									pls_contrato		a
								where	a.nr_sequencia		= b.nr_seq_contrato
								and	((cd_pessoa_fisica = cd_pagador_w) or (cd_cgc = cd_cgc_pagador_w))
								and	a.nr_sequencia		= nr_contrato_principal_w
								and	b.ie_tipo_pagador = 'P';

								if	(nr_seq_pagador_benef_w is null) then
									select	max(b.nr_sequencia)
									into	nr_seq_pagador_benef_w
									from	pls_contrato_pagador	b,
										pls_contrato		a
									where	a.nr_sequencia		= b.nr_seq_contrato
									and	((cd_pessoa_fisica = cd_pagador_w) or (cd_cgc = cd_cgc_pagador_w))
									and	a.nr_sequencia		= nr_contrato_principal_w
									and	b.ie_tipo_pagador = 'S';
								end if;
							end if;
						end if;
						
						if	(nr_seq_pagador_benef_w is null) then
							select	max(b.nr_sequencia)
							into	nr_seq_pagador_benef_w
							from	pls_contrato_pagador	b,
								pls_contrato		a
							where	a.nr_sequencia		= b.nr_seq_contrato
							and	((cd_pessoa_fisica = cd_pagador_w) or (cd_cgc = cd_cgc_pagador_w))
							and	a.nr_sequencia		= nr_seq_contrato_w
							and	b.ie_tipo_pagador = 'P';
							
							if	(nr_seq_pagador_benef_w is null) then
								select	max(b.nr_sequencia)
								into	nr_seq_pagador_benef_w
								from	pls_contrato_pagador	b,
									pls_contrato		a
								where	a.nr_sequencia		= b.nr_seq_contrato
								and	((cd_pessoa_fisica = cd_pagador_w) or (cd_cgc = cd_cgc_pagador_w))
								and	a.nr_sequencia		= nr_seq_contrato_w
								and	b.ie_tipo_pagador = 'S';
							end if;
						end if;
					end if;
					
					if	(nr_seq_pagador_benef_w is not null) then
						nr_seq_pagador_w := nr_seq_pagador_benef_w;
					end if;
				end if;
				
			else	
				select	max(nr_seq_pagador)
				into	nr_seq_pagador_titular_w
				from	pls_proposta_beneficiario
				where	nr_sequencia = nr_seq_titular_w;
			end if;

			if	(nr_seq_pagador_titular_w <> nr_seq_pagador_w) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 63, null,nm_usuario_p);
			end if;
		end if;
		end;
		
		--64 - Data de casamento nao informada (Apenas dependentes)
		begin
		select	ie_grau_parentesco
		into	ie_grau_parentesco_w
		from	grau_parentesco
		where	nr_sequencia = nr_seq_parentesco_w;
		exception
		when others then
			ie_grau_parentesco_w := null;
		end;
		
		if	(ie_estado_civil_w = '2') and 
			(dt_emissao_cert_casamento_w is null) and
			(ie_titularidade_w = 'D') and
			(ie_grau_parentesco_w = 4) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 64, null,nm_usuario_p);
		end if;
		
		--65 - Produto invalido
		if	((nr_seq_contrato_w is not null) and
			(nr_seq_plano_w is not null)) then
			select	count(1)
			into	qt_plano_contrato_w
			from	pls_contrato_plano a
			where	a.nr_seq_contrato = nr_seq_contrato_w
			and	a.nr_seq_plano	= nr_seq_plano_w;
			
			if	(qt_plano_contrato_w = 0) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 65, null, nm_usuario_p);
			end if;
		end if;
	
		--66 - Data de adesao interior a data atual.
		if	(trunc(dt_contratacao_w,'dd') < trunc(sysdate,'dd')) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 66, null,nm_usuario_p);
		end if;

		--68 - Subestipulante informado para o beneficiario e diferente do subestipulante do titular
		if	((nr_seq_subestipulante_w is not null) and 
			(ie_titularidade_w = 'D')) then
			
			nr_seq_subestipulante_ww	:= nvl(nvl(nr_seq_subestipulante_tit_w, nr_seq_subestipulante_tit_co_w), 0);

			if	(nr_seq_subestipulante_w <> nr_seq_subestipulante_ww) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 68, null, nm_usuario_p);
			end if;
		end if;
		
		--69 - CEP nao informado
		if	(cd_cep_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 69, null, nm_usuario_p);
		elsif	(pls_verificar_cep_valido(cd_cep_w) = 'N') then
		--70 - CEP invalido
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 70, null, nm_usuario_p);
		end if;
		
		
		open c02;
		loop
		fetch c02 into
			nr_seq_sca_vinculo_w,
			nr_seq_plano_sca_w;
		exit when c02%notfound;
		begin
			--71 - SCA nao pode ser incluido ao beneficiario, pois nao esta vinculado ao titular
			if	(ie_titularidade_w = 'D') then
				select	count(1)
				into	ie_possui_sca_w
				from	pls_sca_vinculo
				where	((nr_seq_titular_contrato_w is not null and nr_seq_segurado = nr_seq_titular_contrato_w)
				or	(nr_seq_titular_w is not null and nr_seq_benef_proposta = nr_seq_titular_w) )
				and	nr_seq_plano = nr_seq_plano_sca_w;
				
				if	(ie_possui_sca_w = 0) then
					pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 71, nr_seq_sca_vinculo_w, nm_usuario_p);
				end if;
			end if;
			
			if	((ie_permite_inc_fora_atuacao_w = 'N') and
				(pls_obter_se_area_atuacao_prod(nr_seq_plano_sca_w,cd_municipio_ibge_w) = 'N')) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 86,nr_seq_sca_vinculo_w, nm_usuario_p);
			end if;
		end;
		end loop;
		close c02;
		
		--72 - Nao foi enviado anexo de portabilidade
		if	(ie_inclusao_portabilidade_w = 'S') then
			select	count(1)
			into	qt_anexos_w
			from	pls_proposta_benef_anexo a,
				tipo_documentacao b
			where	b.nr_sequencia	= a.nr_seq_tipo_documento
			and	a.nr_seq_beneficiario = nr_seq_benef_prop_w
			and	nvl(b.ie_portabilidade_ops,'N') = 'S';
			
			if	(qt_anexos_w = 0) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 72, null, nm_usuario_p);
			end if;
		end if;
		
		-- 82 - Celular nao informado
		if	(nr_telefone_celular_w is null) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 82, null, nm_usuario_p);
		else
			select	sum(qt_cel)
			into	qt_celular_w
			from(	select	count(a.nr_sequencia) qt_cel
				from	pls_segurado a,
					pessoa_fisica b
				where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
				and	somente_numero(b.nr_telefone_celular) = somente_numero(nr_telefone_celular_w)
				and	a.nr_seq_contrato	= nr_seq_contrato_w
				and	a.cd_pessoa_fisica <> cd_beneficiario_w
				and	a.dt_cancelamento is null
				union
				select	count(d.cd_cgc) qt_cel
				from	pls_proposta_adesao c,
					pessoa_juridica d
				where	c.cd_cgc_estipulante	= d.cd_cgc
				and	somente_numero(d.nr_telefone)	= somente_numero(nr_telefone_celular_w)
				and	c.nr_sequencia		= nr_seq_contrato_w
				union
				select	count(g.cd_beneficiario) qt_cel
				from	pls_proposta_adesao e,
					pessoa_fisica f,
					pls_proposta_beneficiario g
				where	e.nr_sequencia		= g.nr_seq_proposta
				and	g.cd_beneficiario	= f.cd_pessoa_fisica
				and	somente_numero(f.nr_telefone_celular) = somente_numero(nr_telefone_celular_w)
				and	e.nr_sequencia		= nr_seq_proposta_p
				and	g.nr_sequencia <> nr_seq_benef_prop_w
				and	g.dt_cancelamento is null);
			
			--83 - Celular ja informado para empresa ou outro beneficiario da empresa
			if	(qt_celular_w > 0) then
				pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 83, null, nm_usuario_p);
			end if;
		end if;
		

		select	count(1)
		into 		qt_titular_ativo_w
		from 		pls_segurado
		where   nr_sequencia = nr_seq_titular_contrato_w
    and 		dt_liberacao is not null
		and 		(dt_rescisao is null or dt_rescisao > dt_contratacao_w)
		and 		dt_cancelamento is null;

		-- 84 - Municipio de residencia fora da area de atuacao do produto
		if	((ie_permite_inc_fora_atuacao_w = 'N') and
			(pls_obter_se_area_atuacao_prod(nr_seq_plano_w,cd_municipio_ibge_w) = 'N') and qt_titular_ativo_w = 0) then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 84, null,nm_usuario_p);
		end if;
		
		-- 85 - Celular nao informado (Titular)
		if	(nr_telefone_celular_w is null) and
			(ie_titularidade_w = 'T') then
			pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 85, null,nm_usuario_p);
		end if;
		
		--87-- E-MAIL nao informado
	if	(ds_email_w is null) then
		pls_gravar_inconsist_proposta(nr_seq_proposta_p, nr_seq_benef_prop_w, 87, null, nm_usuario_p);
	end if;
	
	end loop;
	close C01;
end if;

commit;

end pls_atualizar_inco_prop_fixas;
/