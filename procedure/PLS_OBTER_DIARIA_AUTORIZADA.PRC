create or replace
procedure pls_obter_diaria_autorizada	
			(	nr_seq_guia_p			number,
				cd_estabelecimento_p		number,
				cd_procedimento_p		Number,
				ie_origem_proced_p		Number,
				nm_usuario_p			Varchar2,
				qt_dia_autorizado_p	out	Number) is

qt_dia_autorizado_w		number(10)	:= 0;
qt_dia_prorrogacao_w		number(10)	:= 0;
nr_seq_guia_princ_w		number(10);

begin

if	(nvl(nr_seq_guia_p,0) > 0) then
	begin
		select	nvl(qt_dia_autorizado,0)
		into	qt_dia_autorizado_w
		from	pls_guia_plano
		where	nr_sequencia		= nr_seq_guia_p
		and	cd_estabelecimento		= cd_estabelecimento_p;
 
	exception
	when others then
		qt_dia_autorizado_w := 0;
	end;
	
	--nr_seq_guia_princ_w	:= pls_obter_guia_aut_principal(nr_seq_guia_p); Essa parte da rotina foi comentada , pois na conta � lan�ada a guia principal e a guia principal nao tem nenhuma guia referenciada. 

	begin
		select	nvl(sum(qt_dia_autorizado),0)
		into	qt_dia_prorrogacao_w
		from	pls_guia_plano
		where	nr_seq_guia_principal	= nr_seq_guia_p
		and	cd_estabelecimento	= cd_estabelecimento_p;
	exception
	when others then
		qt_dia_prorrogacao_w := 0;
	end;
	
	qt_dia_autorizado_w	:= nvl(qt_dia_autorizado_w,0) + nvl(qt_dia_prorrogacao_w,0);
end if;

if	(qt_dia_autorizado_w = 0) and
	(nvl(cd_procedimento_p,0) > 0) and
	(nvl(nr_seq_guia_p,0) > 0) then
	begin
		select	sum(qt_autorizada)
		into	qt_dia_autorizado_w
		from	pls_guia_plano_proc
		where	nr_seq_guia		= nr_seq_guia_p
		and	cd_procedimento		= cd_procedimento_p
		and	ie_origem_proced	= ie_origem_proced_p
		and	qt_autorizada		> 0;
	exception
	when others then
		qt_dia_autorizado_w := 0;
	end;

	begin
		select	sum(a.qt_autorizada)
		into	qt_dia_prorrogacao_w
		from	PLS_GUIA_PLANO_PROC	a
		where	a.cd_procedimento	= cd_procedimento_p
		and	a.ie_origem_proced	= ie_origem_proced_p
		and	a.qt_autorizada		> 0
		and	a.nr_seq_guia		in (	select 	x.nr_sequencia
							from	pls_guia_plano	x
							where	x.nr_seq_guia_principal	= nr_seq_guia_p
							and	x.cd_estabelecimento	= cd_estabelecimento_p);
	exception
	when others then
		qt_dia_prorrogacao_w := 0;
	end;
	qt_dia_autorizado_w	:= nvl(qt_dia_autorizado_w,0) + nvl(qt_dia_prorrogacao_w,0);
end if;
qt_dia_autorizado_p	:= nvl(qt_dia_autorizado_w,0);

end pls_obter_diaria_autorizada;
/