create or replace
procedure ctb_integracao_mxm(	nr_lote_contabil_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 

cd_empresa_w		varchar2(20);
dt_referencia_w		date;
nr_contador_w		number(10) := 0;
qt_registros_w		number(10);
tgpr_sqprocesso_w	number(10);
cd_centro_custo_w	number(10);
cd_classif_centro_w	varchar2(255);

Cursor c01 is
	select	somente_numero(c.cd_classificacao) cd_conta_contabil,
		somente_numero(e.cd_classificacao) cd_centro_custo,
		m.ie_debito_credito,
		substr(m.ds_compl_historico,1,200) ds_compl_historico,
		m.vl_movimento,
		'' vl_segunda_moeda,
		'' cd_segunda_moeda,
		to_char(m.dt_movimento,'ddmmyyyy') dt_movimento,
		'' cd_projeto,
		'' ds_hist_padrao,
		'' ds_compl_hist_1,
		'' ds_compl_hist_2,
		'' ds_compl_hist_3,
		m.cd_cgc,
		m.cd_pessoa_fisica,
		m.cd_historico,
		m.ds_compl_historico ds_historico_orig,
		m.cd_centro_custo	cd_centro_orig
	from	movimento_contabil_v m,
			conta_contabil c,
			centro_custo e
	where	m.cd_conta_contabil = c.cd_conta_contabil
	and		m.cd_centro_custo = e.cd_centro_custo(+)
	and	m.nr_lote_contabil = nr_lote_contabil_p
	order by m.nr_sequencia;

vet01	C01%RowType;
begin

-- Verifica se o lote possui movimento
select	count(*)
into	qt_registros_w
from	movimento_contabil_v a
where	a.nr_lote_contabil = nr_lote_contabil_p;

if (qt_registros_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(206410);
end if;		

-- Buscar os dados do lote
select	e.cd_empresa,
	l.dt_referencia
into	cd_empresa_w,
	dt_referencia_w
from	lote_contabil l,
	estabelecimento e
where	l.cd_estabelecimento = e.cd_estabelecimento
and	l.nr_lote_contabil = nr_lote_contabil_p;

select	substr(max(a.cd_externo),1,20)
into	cd_empresa_w
from	conversao_meio_externo a
where	a.cd_interno	= cd_empresa_w
and	a.nm_atributo	= 'CD_EMPRESA'
and	a.nm_tabela	= 'EMPRESA';

-- Gravar os dados relativos ao processo (Tabela: W_MXM_TI_GERALANCCTB_TGLC)
select	max(tglc_sqprocesso) + 1
into	tgpr_sqprocesso_w
from	ti_geralancctb_tglc@mxmhomol;

insert into w_mxm_ti_geralancctb_tglc(	tglc_sqprocesso,
					tglc_nuregprocessados,
					tglc_dtgeracao,
					tglc_usuariogeracao)
				values(	tgpr_sqprocesso_w,
					0,
					sysdate,
					'TASY');

-- Gravar os dados relativos ao lote (Tabela: W_MXM_ARQCONTABIL_TADC)
insert into W_MXM_ARQCONTABIL_TADC (	tadc_sqprocesso,
                                                tadc_sqregistro,
                                                tadc_stoperacao,
                                                tadc_cdempresa,
                                                tadc_lote,
                                                tadc_dtlancamento,
                                                tadc_nodocumento)
				values 	(	tgpr_sqprocesso_w, -- mudar para buscar o valor da rotina
						1, -- mudar para pegar uma valor sequencial
						0,
						cd_empresa_w,
						nr_lote_contabil_p,
						to_char(dt_referencia_w,'ddmmyyyy'),
						'');

-- Gravar os dados relativos aos movimentos do lote (Tabela; W_MXM_TI_ARQLANCCTB_TALC)

open c01;
loop
fetch c01 into	
	vet01;
exit when c01%notfound;
	begin
	nr_contador_w := nr_contador_w + 1;
	cd_classif_centro_w	:= vet01.cd_centro_custo;

	if	(nvl(vet01.cd_centro_orig,0) = 0) then
		select	max(cd_centro_custo)
		into	cd_centro_custo_w
		from	movimento_contabil_v
		where	nr_lote_contabil	= nr_lote_contabil_p
		and	ie_debito_credito	<> vet01.ie_debito_credito
		and	ds_compl_historico	= vet01.ds_historico_orig
		and	cd_historico		= vet01.cd_historico
		and	cd_centro_custo is not null;
		
		select	nvl(max(somente_numero(cd_classificacao)),'')
		into	cd_classif_centro_w
		from	centro_custo
		where	cd_centro_custo	= cd_centro_custo_w;

	end if;

	insert into w_mxm_arqlancctb_talc	(	talc_sqprocesso,
							talc_sqregistro,
							talc_sqlancamento,
							talc_noconta,
							talc_noccusto,
							talc_indicador,
							talc_historico,
							talc_valor,
							talc_valor2moeda,
							talc_cd2moeda,
							talc_dtdigitacao,
							talc_cdprojeto,
							talc_histpadrao,
							talc_comphist1,
							talc_comphist2,
							talc_comphist3,
							talc_cdfor,
							talc_cdcliente,
							dt_envio,
							nr_lote_contabil,
							nm_usuario)
					values	(	tgpr_sqprocesso_w,
							1,
							nr_contador_w,
							vet01.cd_conta_contabil,
							cd_classif_centro_w,
							vet01.ie_debito_credito,
							vet01.ds_compl_historico,
							vet01.vl_movimento,
							vet01.vl_segunda_moeda,
							vet01.cd_segunda_moeda,
							vet01.dt_movimento,
							vet01.cd_projeto,
							vet01.ds_hist_padrao,
							vet01.ds_compl_hist_1,
							vet01.ds_compl_hist_2,
							vet01.ds_compl_hist_3,
							vet01.cd_cgc,
							vet01.cd_pessoa_fisica,
							sysdate,
							nr_lote_contabil_p,
							nm_usuario_p);
	if (mod(nr_contador_w,0) = 0) then
		commit;
	end if;
	
	end;
end loop;
close c01;

commit;

end ctb_integracao_mxm;
/