create or replace
procedure HLC_GERAR_GLASGOW_TOLIFE	(	nr_atendimento_p	number,
						nm_usuario_p		Varchar2,
						ie_resposta_motora_p number,
						ie_resposta_verbal_p number,
						ie_abertura_ocular_p number,
						qt_pontuacao_p number) is 

qt_idade_adulto_w 		number(10);
ie_classif_etaria_w		varchar2(1);
qt_idade_paciente_w		number(10);
cd_pessoa_fisica_w		number(10);

begin
	
	qt_idade_adulto_w := OBTER_PARAM_USUARIO_LOGADO(281,331);
	cd_pessoa_fisica_w := OBTER_PESSOA_ATENDIMENTO(nr_atendimento_p, 'C');
	qt_idade_paciente_w := OBTER_IDADE_PF(cd_pessoa_fisica_w, sysdate, 'A');

	if (qt_idade_paciente_w < 1) then
		ie_classif_etaria_w := 'N';
	elsif (qt_idade_paciente_w < qt_idade_adulto_w) then
		ie_classif_etaria_w := 'P';
	else
		ie_classif_etaria_w := 'A';
	end if;
	
	insert into ATEND_ESCALA_INDICE (	nr_sequencia,
								nr_atendimento,
								dt_atualizacao,
								nm_usuario,
								cd_pessoa_fisica,
								qt_glasgow,
								ie_resposta_motora,
								ie_resposta_verbal,
								ie_abertura_ocular,
								dt_avaliacao,
								dt_liberacao,
								ie_classif_etaria,
								ie_situacao)
					values			(ATEND_ESCALA_INDICE_seq.nextval,
								nr_atendimento_p,
								sysdate,
								nm_usuario_p,
								cd_pessoa_fisica_w,
								qt_pontuacao_p,
								ie_resposta_motora_p,
								ie_resposta_verbal_p,
								ie_abertura_ocular_p,
								sysdate,
								sysdate,
								ie_classif_etaria_w,
								'A');
	commit;

end HLC_GERAR_GLASGOW_TOLIFE;
/
