CREATE OR REPLACE
PROCEDURE INSERIR_PACIENTE_VACINA_RN (
        cd_pessoa_fisica_p      varchar2,
        nr_atendimento_p        number,
        nr_seq_nascimento_p     number,
        nr_seq_teste_vacina_p   number,
        nr_seq_vacina_p         number,
        dt_registro_p           date,
        nm_usuario_p            varchar2,
        ie_rn_p                 varchar2 default 'N'
) IS
/*
TIPOS ie_rn_p
S = SIM. Obtera os dados do RN para entao adicionar na tabela paciente_vacina
N = NAO. Adicionara na tabela paciente_vacina dados passados pelos parametros
ST = SIM TUDO. Obtera os dados do RN para entao adicionar na tabela paciente_vacina
*/


nr_seq_vacina_w	           vacina_calendario.nr_seq_vacina%type;
qt_prox_mes_w	             vacina_calendario.qt_prox_mes%type; 
nr_atendimento_w           paciente_vacina.nr_atendimento%type;
cd_pessoa_fisica_w         paciente_vacina.cd_pessoa_fisica%type;
pac_vac_seq                paciente_vacina.nr_sequencia%type;
ie_rn_w                    varchar2(10);

begin
  ie_rn_w := nvl(ie_rn_p, 'N');

  if (ie_rn_w = 'S' or ie_rn_w = 'ST') then
    begin
      select	max(nr_atend_rn), max(cd_pessoa_rn)
      into	nr_atendimento_w, cd_pessoa_fisica_w
      from	nascimento
      where	nr_atendimento = nr_atendimento_p
      and	nr_sequencia = nr_seq_nascimento_p
      and rownum = 1;
    exception when others then
      nr_atendimento_w := null;
      cd_pessoa_fisica_w := null;
    end;
    
    if (cd_pessoa_fisica_w is null and nr_atendimento_w is not null) then
        cd_pessoa_fisica_w := obter_pessoa_atendimento(nr_atendimento_w, 'C');
    end if;
  else 
    cd_pessoa_fisica_w := cd_pessoa_fisica_p;
    nr_atendimento_w := nr_atendimento_p;
    
    if (cd_pessoa_fisica_w is null and nr_atendimento_w is not null) then
        cd_pessoa_fisica_w := obter_pessoa_atendimento(nr_atendimento_w, 'C');
    end if;
  end if;

  if (cd_pessoa_fisica_w is not null and (ie_rn_w = 'S' or ie_rn_w = 'N')) then 
      begin
        if (nr_seq_teste_vacina_p is not null) then
          begin
            select nr_seq_vacina, qt_prox_mes
            into nr_seq_vacina_w, qt_prox_mes_w
            from vacina_calendario 
            where nr_seq_vacina = (
                                    select nr_seq_vacina 
                                    from vacina_teste_rn 
                                    where nr_sequencia = nr_seq_teste_vacina_p
                                    and nvl(ie_vacina, 'N') = 'S'
                                    and rownum = 1
            )  and ie_dose = '1D' and rownum = 1;
          exception when others then
            nr_seq_vacina_w := null;
            qt_prox_mes_w := null;
          end;
        elsif (nr_seq_vacina_p is not null) then
          begin
            select nr_seq_vacina, qt_prox_mes
            into nr_seq_vacina_w, qt_prox_mes_w
            from vacina_calendario 
            where nr_seq_vacina = nr_seq_vacina_p
            and ie_dose = '1D' and rownum = 1;
          exception when others then
            nr_seq_vacina_w := null;
            qt_prox_mes_w := null;
          end;
        end if;

        if (nr_seq_vacina_w is not null) then

          select paciente_vacina_seq.nextval
          into pac_vac_seq
          from dual;

          insert into PACIENTE_VACINA (
                NR_SEQUENCIA, 
                NR_ATENDIMENTO, 
                IE_DOSE, 
                DT_VACINA, 
                NR_SEQ_VACINA, 
                DT_ATUALIZACAO, 
                NM_USUARIO, 
                IE_ORIGEM_PROCED, 
                DT_PROXIMA_DOSE, 
                CD_PESSOA_FISICA, 
                IE_EXECUTADO, 
                IE_LOCALIDADE_INF, 
                CD_PROFISSIONAL, 
                IE_SITUACAO, 
                IE_CONFIRMADO, 
                IE_GESTANTE,
                DT_REGISTRO,
				DT_LIBERACAO,
				DT_PREVISTA_EXECUCAO
          ) values (
                /*NR_SEQUENCIA*/      pac_vac_seq, 
                /*NR_ATENDIMENTO*/    nr_atendimento_w, 
                /*IE_DOSE*/           '1D', 
                /*DT_VACINA*/         dt_registro_p, 
                /*NR_SEQ_VACINA*/     nr_seq_vacina_w, 
                /*DT_ATUALIZACAO*/    sysdate, 
                /*NM_USUARIO*/        nm_usuario_p, 
                /*IE_ORIGEM_PROCED*/  1, 
                /*DT_PROXIMA_DOSE*/   add_months(dt_registro_p, nvl(qt_prox_mes_w, 0)), 
                /*CD_PESSOA_FISICA*/  cd_pessoa_fisica_w, 
                /*IE_EXECUTADO*/      'S', 
                /*IE_LOCALIDADE_INF*/ 'N', 
                /*CD_PROFISSIONAL*/   obter_pf_usuario(nm_usuario_p, 'C'), 
                /*IE_SITUACAO*/       'A', 
                /*IE_CONFIRMADO*/     'N', 
                /*IE_GESTANTE*/       'N',
                sysdate,
				sysdate,
				sysdate
          );
        
          commit;
        end if;

      exception when others then
        null;
      end;
  elsif (ie_rn_w = 'ST' and nr_atendimento_p is not null and nr_seq_nascimento_p is not null) then
  
    if (cd_pessoa_fisica_w is not null or nr_atendimento_w is not null ) then
      for vacina in (
          select nr_seq_teste_vacina,dt_registro from atend_vacina_teste
          where nr_atendimento = nr_atendimento_p
          and nr_seq_nascimento = nr_seq_nascimento_p
          and dt_liberacao is not null
          and dt_inativacao is null
          and nr_seq_teste_vacina is not null
      ) loop

          INSERIR_PACIENTE_VACINA_RN(cd_pessoa_fisica_w, nr_atendimento_w, null, vacina.nr_seq_teste_vacina, null, vacina.dt_registro, nm_usuario_p, 'N');

      end loop;
    end if;
  end if;

END INSERIR_PACIENTE_VACINA_RN;
/
