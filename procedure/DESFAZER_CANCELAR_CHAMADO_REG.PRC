create or replace
procedure desfazer_cancelar_chamado_reg(nr_sequencia_p		number,
					nr_atendimento_p	number,
					
					nm_usuario_p	Varchar2) is 

ds_erro_w	varchar2(100);					
					
			
begin

if 	(nr_sequencia_p > 0) then
	update	eme_regulacao
	set	nm_usuario_cancelamento = null,
		dt_cancelamento = null
	where	nr_sequencia = nr_sequencia_p;
end if;

If	(nr_atendimento_p > 0) then
	gerar_estornar_alta(nr_atendimento_p, 'E', 0, 0, null, nm_usuario_p, ds_erro_w, 0, 0, null); 
end if;

commit;

end desfazer_cancelar_chamado_reg;
/