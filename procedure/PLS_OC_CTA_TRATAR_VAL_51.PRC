create or replace
procedure pls_oc_cta_tratar_val_51(
			dados_regra_p		pls_tipos_ocor_pck.dados_regra,
			nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
			nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar valida��o referente a exist�ncia de liminar judicial na requisi��o ou na autoriza��o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Altera��es:

------------------------------------------------------------------------------------------------------------------
dlehmkuhl OS 688483 - 14/04/2014 -

Altera��o:	Modificada a forma de trabalho em rela��o a atualiza��o dos campos de controle
	que basicamente decidem se a ocorr�ncia ser� ou n�o gerada. Foi feita tamb�m a 
	substitui��o da rotina obterX_seX_geraX.

Motivo:	Necess�rio realizar essas altera��es para corrigir bugs principalmente no que se
	refere a quest�o de aplica��o de filtros (passo anterior ao da valida��o). Tamb�m
	tivemos um foco especial em performance, visto que a mesma precisou ser melhorada
	para n�o inviabilizar a nova solicita��o que diz que a exce��o deve verificar todo
	o atendimento.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_selecao_w	pls_oc_cta_selecao_ocor_v.nr_sequencia%type;
qt_liminar_guia_w	Number(5);
qt_liminar_req_w	Number(5);
ie_gera_ocorrencia_w	varchar2(1);
ds_sql_w		varchar2(4000); 
tb_seq_selecao_w	dbms_sql.number_table;
tb_observacao_w		dbms_sql.varchar2_table;
tb_valido_w		dbms_sql.varchar2_table;
v_cur			pls_util_pck.t_cursor;
qt_iteracoes_w		pls_integer;


cursor C01 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.nr_sequencia	nr_seq_validacao,
		a.ie_valida_liminar
	from	pls_oc_cta_val_liminar_jud a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

begin

if	(dados_regra_p.nr_sequencia is not null) then
	
	--Percorre a lista da sele��o, criando select conforme restri��es definidas na regra
	for	r_C01_w in C01 (dados_regra_p.nr_sequencia) loop
		
		--Zera contador de registros nas listas
		qt_iteracoes_w 	:= 0; 
		
		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
		
		if 	( r_C01_w.ie_valida_liminar = 'S' ) then
		
			--Select com 2 count para buscar a quantidade de liminar judicial vinculada na requisi��o ou na guia
			ds_sql_w :=	'select	sel.nr_sequencia nr_seq_selecao, ' || pls_tipos_ocor_pck.enter_w || 
				'		(select	count(1) ' || pls_tipos_ocor_pck.enter_w ||
				'		from	processo_judicial_liminar procj, ' || pls_tipos_ocor_pck.enter_w ||
				'			pls_guia_liminar_judicial  liminar ' || pls_tipos_ocor_pck.enter_w ||
				'		where	procj.nr_sequencia 	= liminar.nr_seq_processo ' || pls_tipos_ocor_pck.enter_w ||							
				'		and 	liminar.nr_Seq_guia 	= conta.nr_seq_guia) qt_liminar_guia, ' || pls_tipos_ocor_pck.enter_w ||
				'		(select	count(1) ' || pls_tipos_ocor_pck.enter_w ||
				'		from	processo_judicial_liminar procj,  ' || pls_tipos_ocor_pck.enter_w ||
				'			pls_req_liminar_judicial  liminar,  ' || pls_tipos_ocor_pck.enter_w ||
				'			pls_execucao_requisicao exec ' || pls_tipos_ocor_pck.enter_w ||
				'		where	procj.nr_sequencia 	= liminar.nr_seq_processo  ' || pls_tipos_ocor_pck.enter_w ||
				'		and	exec.nr_seq_requisicao  = liminar.nr_seq_requisicao ' || pls_tipos_ocor_pck.enter_w ||
				'		and 	exec.nr_Seq_guia 	= conta.nr_seq_guia) qt_liminar_req ' || pls_tipos_ocor_pck.enter_w ||
				'	from	pls_oc_cta_selecao_ocor_v sel, ' || pls_tipos_ocor_pck.enter_w ||				
				'		pls_conta_ocor_v conta ' || pls_tipos_ocor_pck.enter_w || 				
				'	where	sel.nr_id_transacao = :nr_id_transacao ' || pls_tipos_ocor_pck.enter_w || 
				'	and	sel.ie_valido = ''S'' ' || pls_tipos_ocor_pck.enter_w || 				
				'	and	conta.nr_sequencia = sel.nr_seq_conta ';					
		--Limpa as listas.
		tb_seq_selecao_w.delete;
		tb_observacao_w.delete;
		tb_valido_w.delete;	
		begin
			open v_cur for ds_sql_w using 	nr_id_transacao_p;		
			loop	
				fetch v_cur
				into  nr_seq_selecao_w, qt_liminar_guia_w, qt_liminar_req_w;
					
				exit when v_cur%notfound;
					
					ie_gera_ocorrencia_w	:= 'N';
					
					--Verificar se existe liminar judicial na requisi��o ou na guia, se existir em qualquer uma delas ir� gerar a ocorr�ncia
					if ( (qt_liminar_guia_w + qt_liminar_req_w) > 0 ) then
						ie_gera_ocorrencia_w	:= 'S';
					end if;					
						
					if	(ie_gera_ocorrencia_w = 'S') then
						qt_iteracoes_w := qt_iteracoes_w + 1;
						
						--Alimenta as listas com a sequencia da sele��o...
						tb_seq_selecao_w(qt_iteracoes_w):= nr_seq_selecao_w;
						tb_observacao_w(qt_iteracoes_w) := null;	
						tb_valido_w(qt_iteracoes_w)	:= 'S';
						
						if	(qt_iteracoes_w = pls_util_cta_pck.qt_registro_transacao_w) then
							
							pls_tipos_ocor_pck.gerencia_selecao_validacao( tb_seq_selecao_w, pls_util_cta_pck.clob_table_vazia_w, 
													'SEQ', tb_observacao_w, tb_valido_w, nm_usuario_p); 
							qt_iteracoes_w := 0;
							tb_seq_selecao_w.delete;
							tb_observacao_w.delete;
							tb_valido_w.delete;
						end if;	
					end if;
			end loop;
			--Se sobrar registros nas listas, os mesmos devem ser atualizados.
			if	(qt_iteracoes_w > 0) then
				
				pls_tipos_ocor_pck.gerencia_selecao_validacao( tb_seq_selecao_w, pls_util_cta_pck.clob_table_vazia_w, 
										'SEQ', tb_observacao_w, tb_valido_w, nm_usuario_p); 
				qt_iteracoes_w := 0;
				tb_seq_selecao_w.delete;
				tb_observacao_w.delete;
				tb_valido_w.delete;
			end if;	
			close v_cur;
			
		exception			
		when others then
			--Fecha cursor
			close v_cur;
			-- Insere o log na tabela e aborta a opera��o
			pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p,ds_sql_w,nr_id_transacao_p,nm_usuario_p);
		end;
		end if;	
		
	end loop; -- C01
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
	
end if;

end pls_oc_cta_tratar_val_51;
/