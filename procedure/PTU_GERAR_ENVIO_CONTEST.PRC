create or replace
procedure ptu_gerar_envio_contest(	nr_seq_camara_p		number,
					cd_interface_p		varchar2,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar o arquivo A550 conforme a vers�o selecionada.
-------------------------------------------------------------------------------------------------------------------
OPS - Controle de Contesta��es
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X]  Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: Vers�es para serem utilizadas pelo PTU A550
2188 - V4.0		2349 - V4.1		2435 - V4.1b	2480 - V5.0
2575 - V5.1		2588 - V6.0		2699 - V6.3		2746 - V7.0
2790 - V8.0		2834 - V9.0 	2978 - V11.0

N�O PODE TER COMMIT
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/ 
	
begin

if	(cd_interface_p = '2188') then 
	ptu_gerar_envio_contest_v40(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);

elsif	(cd_interface_p = '2349') then
	ptu_gerar_envio_contest_v41(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);

elsif	(cd_interface_p = '2435') then
	ptu_gerar_envio_contest_v41b(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '2480') then
	ptu_gerar_envio_contest_v50(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '2575') then
	ptu_gerar_envio_contest_v50a(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '2588') then
	ptu_gerar_envio_contest_v60(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '2699') then
	ptu_gerar_envio_contest_v63(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '2746') then
	ptu_gerar_envio_contest_v70(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '2790') then
	ptu_gerar_envio_contest_v80(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '2834') then
	ptu_gerar_envio_contest_v90(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '2908') then
	ptu_gerar_envio_contest_v91(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '2978') then
	ptu_gerar_envio_contest_v110(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
elsif	(cd_interface_p = '3092') then
	ptu_gerar_envio_contest_v113(nr_seq_camara_p,cd_estabelecimento_p,nm_usuario_p);
	
end if;

end ptu_gerar_envio_contest;
/