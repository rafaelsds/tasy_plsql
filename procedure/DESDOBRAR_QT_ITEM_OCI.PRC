create or replace
procedure desdobrar_qt_item_oci(		nr_ordem_compra_p		number,
					nr_item_oci_p			number,
					qt_material_p			number,
					ie_update_insert_p			varchar2,
					nm_usuario_p			varchar2) is 

dt_prevista_entrega_w		date;
nr_item_oci_w			number(10);

begin

select	min(dt_prevista_entrega)
into	dt_prevista_entrega_w
from	ordem_compra_item_entrega
where	nr_ordem_compra	= nr_ordem_compra_p
and	nr_item_oci	= nr_item_oci_p;

if	(ie_update_insert_p = 'U') then
	update	ordem_compra_item
	set	qt_material	= qt_material_p,
		vl_total_item = round(( qt_material_p * vl_unitario_material),4)
	where	nr_ordem_compra	= nr_ordem_compra_p
	and	nr_item_oci	= nr_item_oci_p;
	
	delete from ordem_compra_item_entrega
	where	nr_ordem_compra	= nr_ordem_compra_p
	and	nr_item_oci	= nr_item_oci_p;
	
	insert into ordem_compra_item_entrega(
		nr_sequencia,
		nr_ordem_compra,
		nr_item_oci,
		dt_prevista_entrega,
		qt_prevista_entrega,
		dt_atualizacao,
		nm_usuario)
	values(	ordem_compra_item_entrega_seq.nextval,
		nr_ordem_compra_p,
		nr_item_oci_p,
		dt_prevista_entrega_w,
		qt_material_p,
		sysdate,
		nm_usuario_p);
		
elsif	(ie_update_insert_p = 'I') then

	select	nvl(max(nr_item_oci),0) +1
	into	nr_item_oci_w
	from	ordem_compra_item
	where	nr_ordem_compra = nr_ordem_compra_p;
	
	insert into ordem_compra_item(
		nr_ordem_compra,
		nr_item_oci,
		cd_material,
		cd_unidade_medida_compra,
		vl_unitario_material,
		qt_material,
		dt_atualizacao,
		nm_usuario,
		ie_situacao,
		cd_pessoa_solicitante,
		pr_descontos,
		cd_local_estoque,
		ds_material_direto,
		ds_observacao,
		nr_cot_compra,
		nr_item_cot_compra,
		ds_marca,
		dt_aprovacao,
		cd_centro_custo,
		cd_conta_contabil,
		nr_solic_compra,
		nr_item_solic_compra,
		qt_conv_unid_fornec,
		ie_geracao_solic,
		nr_seq_proj_rec,
		pr_desc_financ,
		nr_seq_lic_item,
		nr_seq_conta_financ,
		dt_validade,
		nr_seq_marca,
		dt_reprovacao,
		nr_seq_ordem_serv,
		vl_ultima_compra,
		vl_dif_ultima_compra,
		pr_dif_ultima_compra,
		nr_seq_unidade_adic,
		nr_seq_criterio_rateio,
		nr_serie_material,
		nr_solic_compra_cancel,
		vl_desconto,
		nr_seq_lote_fornec,
		nr_seq_proj_gpi,
		nr_seq_etapa_gpi,
		nr_seq_conta_gpi,
		dt_aprovacao_orig,
		dt_reprovacao_orig,
		nr_contrato,
		dt_inicio_garantia,
		dt_fim_garantia,
		nr_id_integracao,
		nr_seq_reg_lic_item,
		nr_seq_conta_bco,
		nr_seq_prescr_item,
		nr_empenho,
		dt_empenho,
		nr_ordem_agrup,
		nr_seq_orc_item_gpi,
		vl_unit_mat_original,
		nr_atendimento,
		qt_dias_garantia,
		dt_desdobr_aprov,
		dt_lib_estrut_pj,
		ds_justificativa_lib,
		nm_usuario_lib,
		cd_operacao_nf,
		cd_natureza_operacao,
		qt_original,
		nr_seq_aprovacao,
		vl_total_item)
	(select	nr_ordem_compra,
		nr_item_oci_w,
		cd_material,
		cd_unidade_medida_compra,
		vl_unitario_material,
		qt_material_p,
		dt_atualizacao,
		nm_usuario,
		ie_situacao,
		cd_pessoa_solicitante,
		pr_descontos,
		cd_local_estoque,
		ds_material_direto,
		ds_observacao,
		nr_cot_compra,
		nr_item_cot_compra,
		ds_marca,
		dt_aprovacao,
		cd_centro_custo,
		cd_conta_contabil,
		nr_solic_compra,
		nr_item_solic_compra,
		qt_conv_unid_fornec,
		ie_geracao_solic,
		nr_seq_proj_rec,
		pr_desc_financ,
		nr_seq_lic_item,
		nr_seq_conta_financ,
		dt_validade,
		nr_seq_marca,
		dt_reprovacao,
		nr_seq_ordem_serv,
		vl_ultima_compra,
		vl_dif_ultima_compra,
		pr_dif_ultima_compra,
		nr_seq_unidade_adic,
		nr_seq_criterio_rateio,
		nr_serie_material,
		nr_solic_compra_cancel,
		vl_desconto,
		nr_seq_lote_fornec,
		nr_seq_proj_gpi,
		nr_seq_etapa_gpi,
		nr_seq_conta_gpi,
		dt_aprovacao_orig,
		dt_reprovacao_orig,
		nr_contrato,
		dt_inicio_garantia,
		dt_fim_garantia,
		nr_id_integracao,
		nr_seq_reg_lic_item,
		nr_seq_conta_bco,
		nr_seq_prescr_item,
		nr_empenho,
		dt_empenho,
		nr_ordem_agrup,
		nr_seq_orc_item_gpi,
		vl_unit_mat_original,
		nr_atendimento,
		qt_dias_garantia,
		dt_desdobr_aprov,
		dt_lib_estrut_pj,
		ds_justificativa_lib,
		nm_usuario_lib,
		cd_operacao_nf,
		cd_natureza_operacao,
		qt_material_p,
		nr_seq_aprovacao,
		round((qt_material_p * vl_unitario_material),4)
	from	ordem_compra_item
	where	nr_ordem_compra	= nr_ordem_compra_p
	and	nr_item_oci = nr_item_oci_p);
	
	insert into ordem_compra_item_entrega(
		nr_sequencia,
		nr_ordem_compra,
		nr_item_oci,
		dt_prevista_entrega,
		qt_prevista_entrega,
		dt_atualizacao,
		nm_usuario)
	values(	ordem_compra_item_entrega_seq.nextval,
		nr_ordem_compra_p,
		nr_item_oci_w,
		dt_prevista_entrega_w,
		qt_material_p,
		sysdate,
		nm_usuario_p);
end if;


commit;

end desdobrar_qt_item_oci;
/
