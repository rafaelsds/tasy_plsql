create or replace
procedure	avisar_encaminhar_aprov_cot(
			nr_seq_aprovacao_p	number,
			nr_seq_proc_aprov_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is

nr_cot_compra_w			number(10);
nr_seq_regra_w			number(10);
ds_email_adicional_w		varchar2(2000);
cd_perfil_dispara_w		number(5);
ie_momento_envio_w		varchar2(1);

ds_titulo_w			varchar2(255);
dt_cot_compra_w			date;
cd_comprador_w			varchar2(10);
nm_comprador_w			varchar2(255);
cd_pessoa_solicitante_w		varchar2(10);
nm_pessoa_solicitante_w		varchar2(255);
nm_pessoa_enc_w			varchar2(255);

ds_assunto_w			varchar2(255);
ds_mensagem_w			varchar2(4000);

ds_email_origem_w			varchar2(255);
nm_usuario_origem_w		varchar2(255);
ie_usuario_w			varchar2(1);
cd_pessoa_enc_w			varchar2(10);
cd_cargo_enc_w			number(10);
ds_destinatarios_w			varchar2(4000);
ds_email_w			varchar2(255);
ds_observacao_w		varchar2(255);

cursor	c01 is
select	distinct
	b.ds_email
from	pessoas_processo_aprovacao_v a,
	usuario b
where	a.nm_usuario = b.nm_usuario
and	a.nr_sequencia = nr_seq_aprovacao_p
and	a.nr_seq_proc_aprov = nr_seq_proc_aprov_p;

begin

select	nvl(max(nr_cot_compra),0)
into	nr_cot_compra_w
from	cot_compra_item
where	nr_seq_aprovacao = nr_seq_aprovacao_p;

select	nvl(max(nr_sequencia),0),
	max(replace(ds_email_adicional,',',';')),
	max(cd_perfil_disparar),
	nvl(max(ie_momento_envio),'I')
into	nr_seq_regra_w,
	ds_email_adicional_w,
	cd_perfil_dispara_w,
	ie_momento_envio_w
from	regra_envio_email_compra
where	ie_tipo_mensagem = 79
and	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p
and	obter_se_envia_email_regra(nr_cot_compra_w, 'CC', ie_tipo_mensagem, cd_estabelecimento) = 'S';

if	(nvl(nr_cot_compra_w,0) > 0) and
	(nr_seq_regra_w > 0) and
	((cd_perfil_dispara_w is null) or
	((cd_perfil_dispara_w is not null) and (cd_perfil_dispara_w = obter_perfil_ativo))) then
	begin

	select	nvl(ds_titulo,WHEB_MENSAGEM_PCK.get_texto(297573)),
		dt_cot_compra,
		cd_comprador,
		substr(sup_obter_nome_comprador(cd_estabelecimento,cd_comprador),1,255),
		cd_pessoa_solicitante,
		substr(nvl(obter_nome_pf(cd_pessoa_solicitante),WHEB_MENSAGEM_PCK.get_texto(297678)),1,255),
		substr(obter_nome_usuario(nm_usuario_p),1,255)
	into	ds_titulo_w,
		dt_cot_compra_w,
		cd_comprador_w,
		nm_comprador_w,
		cd_pessoa_solicitante_w,
		nm_pessoa_solicitante_w,
		nm_pessoa_enc_w
	from	cot_compra
	where	nr_cot_compra = nr_cot_compra_w;
	
	select	substr(ds_observacao,1,255)
	into	ds_observacao_w
	from	processo_aprov_compra
	where	nr_sequencia = nr_seq_aprovacao_p
	and		nr_seq_proc_aprov = nr_seq_proc_aprov_p;

	select	ds_assunto,
		ds_mensagem_padrao
	into	ds_assunto_w,
		ds_mensagem_w
	from	regra_envio_email_compra
	where	nr_sequencia = nr_seq_regra_w;
	
	ds_assunto_w	:= substr(replace_macro(ds_assunto_w,'@nr_cotacao',nr_cot_compra_w),1,255);
	ds_assunto_w	:= substr(replace_macro(ds_assunto_w,'@nm_solicitante',nm_pessoa_solicitante_w),1,255);
	ds_assunto_w	:= substr(replace_macro(ds_assunto_w,'@seq_aprovacao',nr_seq_aprovacao_p),1,255);
	ds_assunto_w	:= substr(replace_macro(ds_assunto_w,'@ds_titulo',ds_titulo_w),1,255);
	ds_assunto_w	:= substr(replace_macro(ds_assunto_w,'@dt_cotacao',dt_cot_compra_w),1,255);
	ds_assunto_w	:= substr(replace_macro(ds_assunto_w,'@nm_comprador',nm_comprador_w),1,255);
	ds_assunto_w	:= substr(replace_macro(ds_assunto_w,'@nm_pessoa_enc',nm_pessoa_enc_w),1,255);
	ds_assunto_w	:= substr(replace_macro(ds_assunto_w,'@ds_observacao',ds_observacao_w),1,255);
	ds_assunto_w	:= substr(replace_macro(ds_assunto_w,'@nm_usuario_enc',nm_usuario_p),1,255);
	
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@nr_cotacao',nr_cot_compra_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@nm_solicitante',nm_pessoa_solicitante_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@seq_aprovacao',nr_seq_aprovacao_p),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@ds_titulo',ds_titulo_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@dt_cotacao',dt_cot_compra_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@nm_comprador',nm_comprador_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@nm_pessoa_enc',nm_pessoa_enc_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@ds_observacao',ds_observacao_w),1,4000);
	ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@nm_usuario_enc',nm_usuario_p),1,4000);

	select	nvl(max(ie_usuario),'U')
	into	ie_usuario_w
	from	regra_envio_email_compra
	where	nr_sequencia = nr_seq_regra_w;

	if	(ie_usuario_w = 'U') or
		((nvl(cd_comprador_w,'0') <> 0) and (ie_usuario_w = 'O')) then --Usuario
		begin

		select	ds_email,
			nm_usuario
		into	ds_email_origem_w,
			nm_usuario_origem_w
		from	usuario
		where	nm_usuario = nm_usuario_p;

		end;
	elsif	(ie_usuario_w = 'C') then --Setor compras
		begin

		select	ds_email
		into	ds_email_origem_w
		from	parametro_compras
		where	cd_estabelecimento = cd_estabelecimento_p;

		select	nvl(ds_fantasia,ds_razao_social)
		into	nm_usuario_origem_w
		from	estabelecimento_v
		where	cd_estabelecimento = cd_estabelecimento_p;

		end;
	elsif	(ie_usuario_w = 'O') then --Comprador
		begin

		select	max(ds_email),
			max(nm_guerra)
		into	ds_email_origem_w,
			nm_usuario_origem_w
		from	comprador
		where	cd_pessoa_fisica = cd_comprador_w
		and	cd_estabelecimento = cd_estabelecimento_p;

		end;
	end if;

	select	nvl(cd_pessoa_fisica,'X'),
		nvl(cd_cargo,0)
	into	cd_pessoa_enc_w,
		cd_cargo_enc_w
	from	processo_aprov_compra
	where	nr_sequencia = nr_seq_aprovacao_p
	and	nr_seq_proc_aprov = nr_seq_proc_aprov_p;

	if	(cd_pessoa_enc_w <> 'X') then
		
		select	max(ds_email) || ';'
		into	ds_destinatarios_w
		from	usuario
		where	cd_pessoa_fisica = cd_pessoa_enc_w;

	else
		
		open c01;
		loop
		fetch c01 into
			ds_email_w;
		exit when c01%notfound;
			begin

			ds_destinatarios_w	:= ds_destinatarios_w || ds_email_w || ';';

			end;
		end loop;
		close c01;

	end if;

	if	(ds_email_adicional_w is not null) then
		ds_destinatarios_w	:= ds_destinatarios_w || ds_email_adicional_w;
	end if;
	
	if	(ie_momento_envio_w = 'A') then
		begin

		sup_grava_envio_email(
			'CC',
			'79',
			nr_cot_compra_w,
			nr_seq_aprovacao_p,
			nr_seq_proc_aprov_p,
			ds_destinatarios_w,
			nm_usuario_origem_w,
			ds_email_origem_w,
			ds_assunto_w,
			ds_mensagem_w,
			cd_estabelecimento_p,
			nm_usuario_p);

		end;
	else
		begin
		enviar_email(ds_assunto_w,ds_mensagem_w,ds_email_origem_w,ds_destinatarios_w,nm_usuario_origem_w,'M');
		exception
		when others then
			/*gravar__log__tasy(91301,'Falha ao enviar e-mail compras - Evento: 79 - Seq. Regra: ' || nr_seq_regra_w,nm_usuario_p);*/
			gerar_historico_cotacao(
				nr_cot_compra_w,
				WHEB_MENSAGEM_PCK.get_texto(297578),
				WHEB_MENSAGEM_PCK.get_texto(297679,'NR_SEQ_REGRA_W=' || nr_seq_regra_w),
				'S',
				nm_usuario_p);
		end;
	end if;

	end;
end if;

end avisar_encaminhar_aprov_cot;
/