create or replace
procedure confirmar_agendamento_part_js(
					NM_USUARIO_P		VARCHAR2,
					NR_SEQUENCIA_P		NUMBER,
					DS_OBSERVACAO_P		varchar2) is 

nr_ordem_servico_w	number(10);
nr_sequencia_solic_w	number(10);
nr_seq_tipo_dem_w	number(10);
ie_ead_w		varchar2(1);
nr_seq_regra_w		number(10);

nr_seq_tipo_w           number(10);
ds_historico_w          varchar2(4000);
ie_enviar_email_w       varchar2(1);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_tipo_dem
	from	com_solic_sd
	where	nr_ordem_servico = nr_ordem_servico_w;					
					
begin

if	(nvl(nr_sequencia_p,0) > 0) then

	update	agenda_tasy
	set	ie_status 		= 'F',
		dt_confirmacao        	= sysdate,
		nm_usuario_confirmacao  = nm_usuario_p,
		ds_obs_confirmacao      = ds_observacao_p
	where	nr_sequencia 		= nr_sequencia_p;
	
	select	nvl(max(nr_ordem_servico),0)
	into	nr_ordem_servico_w
	from	agenda_tasy
	where	nr_sequencia = nr_sequencia_p;
	
	if	(nr_ordem_servico_w > 0) then
	
		open C01;
		loop
		fetch C01 into	
			nr_sequencia_solic_w,
			nr_seq_tipo_dem_w;
		exit when C01%notfound;
			begin
			
			update	com_solic_sd
			set	ie_status = 'CO'
			where	nr_sequencia = nr_sequencia_solic_w;
			
			select	nvl(max(ie_ead),'N')
			into	ie_ead_w
			from	com_tipo_demonstracao
			where	nr_sequencia = nr_seq_tipo_dem_w;
			
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_regra_w
			from	regra_comunic_conf_age
			where	ie_ead = ie_ead_w;
			
			if	(nr_seq_regra_w > 0) then
			
				select	nr_seq_tipo,    
					ds_historico,   
					ie_enviar_email
				into	nr_seq_tipo_w,
					ds_historico_w,
					ie_enviar_email_w
				from	regra_comunic_conf_age
				where	nr_sequencia = nr_seq_regra_w;
				
				insert into man_ordem_serv_tecnico (
							nr_sequencia,
							nr_seq_ordem_serv,
							dt_atualizacao,
							nm_usuario,
							ds_relat_tecnico,
							cd_versao_tasy,
							dt_historico,
							ie_origem,
							nr_seq_tipo,
							nm_usuario_lib,
							dt_liberacao,
							ie_grau_satisfacao)
				values (
					man_ordem_serv_tecnico_seq.nextval,
					nr_ordem_servico_w,
					sysdate,
					nm_usuario_p,
					ds_historico_w,
					null,
					sysdate,
					'I',
					nr_seq_tipo_w,
					nm_usuario_p,
					sysdate,
					null);	
				if	(nvl(ie_enviar_email_w,'N') = 'S') then
					
					com_enviar_email_conf_age_js(nm_usuario_p, nr_seq_regra_w, nr_ordem_servico_w,nr_sequencia_solic_w);
				end if;
			end if;
			
			end;
		end loop;
		close C01;
		
	end if;
end if;

commit;

end confirmar_agendamento_part_js;
/