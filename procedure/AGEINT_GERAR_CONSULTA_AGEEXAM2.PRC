create or replace
procedure ageint_gerar_consulta_ageexam2(cd_estabelecimento_p	number,
					cd_agenda_p     	number,
					dt_agenda_p	date,
					nm_usuario_p	varchar2) is 
			
/* váriaveis de registro */
nr_seq_agenda_w			number(10,0);
nr_minuto_duracao_w		number(10,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
nr_seq_proc_interno_w	number(10,0);
ie_encaixe_w			varchar2(1);
cd_turno_ww				varchar2(1);
cd_medico_exec_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nm_paciente_w			varchar2(60);
ie_status_Agenda_w		varchar2(3);
hr_inicio_w				date;
nr_seq_medico_regra_w	number(10,0);
nr_seq_status_pac_w		number(10,0);
nr_atendimento_w		number(10);
cd_convenio_ww			number(5);
qt_horarios_w			number;
ds_motivo_w			Varchar2(255);
	
cursor C06 is 
	select	cd_pessoa_fisica,
		nm_paciente,
		cd_medico_exec,
		ie_status_agenda,
		nr_minuto_duracao,
		hr_inicio,
		nr_seq_status_pac,
		cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno,
		nr_sequencia,
		nr_atendimento,
		cd_convenio,
		cd_turno,
		nvl(ie_encaixe,'N'),
		SUBSTR(obter_motivo_agenda(nr_sequencia),1,255)
	from	agenda_paciente
	where	cd_agenda		= cd_agenda_p
	and	trunc(dt_agenda)	= trunc(dt_agenda_p)
	order by hr_inicio;

begin

delete	ageint_consulta_horarios
where	cd_agenda	= cd_agenda_p
and	nm_usuario	= nm_usuario_p
and	dt_agenda	between trunc(dt_agenda_p) and trunc(dt_agenda_p) + 83699/86400;

gerar_horario_agenda_exame(cd_estabelecimento_p,
	cd_agenda_p,
	dt_agenda_p,
	nm_usuario_p);

open C06;
loop
fetch C06 into	
	cd_pessoa_fisica_w,
	nm_paciente_w,
	cd_medico_exec_w,
	ie_status_agenda_w,
	nr_minuto_duracao_w,
	hr_inicio_w,
	nr_seq_status_pac_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_proc_interno_w,
	nr_seq_agenda_w,
	nr_atendimento_w,
	cd_convenio_ww,
	cd_turno_ww,
	ie_encaixe_w,
	ds_motivo_w;
exit when C06%notfound;
	begin
	
	select	max(nr_seq_medico_regra)
	into	nr_seq_medico_regra_w
	from	agenda_medico
	where	cd_agenda	= cd_agenda_p
	and	cd_medico	= cd_medico_exec_w;
	
	insert into ageint_consulta_horarios
		(dt_Atualizacao,
		nm_usuario,
		cd_agenda,
		cd_medico_exec,
		cd_paciente,
		nm_paciente,
		ie_status_agenda,
		nr_minuto_duracao,
		dt_agenda,
		nr_seq_medico_regra,
		nr_seq_status_pac,
		cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno,
		nr_seq_agenda,
		nr_atendimento,
		cd_convenio,
		cd_turno,
		ie_encaixe,
		ds_motivo)
	values
		(sysdate,
		nm_usuario_p,
		cd_Agenda_p,
		cd_medico_exec_w,
		cd_pessoa_fisica_w,
		nm_paciente_w,
		ie_status_Agenda_w,
		nr_minuto_duracao_w,
		hr_inicio_w,
		nr_seq_medico_regra_w,
		nr_seq_status_pac_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		nr_seq_agenda_w,
		nr_atendimento_w,
		cd_convenio_ww,
		cd_turno_ww,
		ie_encaixe_w,
		ds_motivo_w);
	end;
end loop;
close C06;

commit;

end ageint_gerar_consulta_ageexam2;
/
