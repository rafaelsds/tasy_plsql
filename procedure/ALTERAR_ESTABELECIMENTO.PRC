create or replace
procedure alterar_estabelecimento(
	nr_seq_cliente_p number,
	cd_estabelecimento_p number ) is
	
begin
	if(nr_seq_cliente_p is not null) then
		update	com_cliente
		set 	cd_estabelecimento = cd_estabelecimento_p
		where 	nr_sequencia = nr_seq_cliente_p;
		
		update contrato
		set cd_estabelecimento = cd_estabelecimento_p
		where cd_estabelecimento <> cd_estabelecimento_p 
		and nr_seq_cliente = nr_seq_cliente_p;
		
	end if;
	commit;
end;
/
