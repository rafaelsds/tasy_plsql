CREATE OR REPLACE 
PROCEDURE GERAR_RELAT_EIS_CIRURGIA_HI (dt_referencia_p date) IS


ds_comando_w		varchar2(2000);
nr_count_w		number;
nr_sequencia_w 		number(5)		:= 0;
nr_interno_conta_w	number(10);
cd_convenio_w		number(10);
ds_convenio_w		varchar2(255);
qt_cirurgias_w		number(10,4)		:= 0;
qt_total_min_w		number(10,4)		:= 0;
vl_faturamento_w	number(15,4);
vl_material_W		number(15,4);
vl_procedimento_w	number(15,4);
vl_custo_W 		number(15,4);
nr_cirurgia_w		number(10);
vl_taxa_w		number(15,4);
vl_diaria_w		number(15,4);

Cursor C01 is
	select 	nvl(a.cd_convenio,0) cd_convenio,
		nvl(substr(obter_nome_convenio(a.cd_convenio),1,30), obter_desc_expressao(327119)/*'N�o Informado'*/) ds_convenio,		
		sum(nvl(a.nr_min_duracao_real,0)) qt_total_min,
		nvl(a.nr_cirurgia,0)
        from 	atend_paciente_unidade b,
		Pessoa_fisica p,
		atendimento_paciente c,
		cirurgia a   
        where  	a.dt_inicio_real between trunc(trunc(dt_referencia_p,'month'), 'dd') and (trunc(last_day(dt_referencia_p), 'dd') + 1) - (1/86400)
	and	a.dt_termino is not null
	and	a.cd_procedimento_princ is not null
    	and	a.nr_atendimento 		= c.nr_atendimento(+)
    	and 	a.nr_atendimento 		= b.nr_atendimento(+)
    	and	a.dt_entrada_unidade		= b.dt_entrada_unidade(+)
	and	a.cd_pessoa_fisica		= p.cd_pessoa_fisica
	group by a.cd_CONVENIO, a.nr_cirurgia
	order by 1 desc;

BEGIN

/* Verifica a exist�ncia da tabela */

begin
Select	count(table_name)
into	nr_count_w
from	user_tables
where	upper(table_name) = 'EIS_CIRURGIA_INTERMED_W';

if	(nr_count_w = 0) then
	EXEC_SQL_DINAMICO('TASY','create table EIS_CIRURGIA_INTERMED_W
						(nr_sequencia 		number(5),
						nr_cirurgia		number(10),						
						cd_convenio		number(10),
						ds_convenio		varchar2(60),
						qt_cirurgias		number(10,4),
						qt_total_min		number(10,4),
						vl_faturamento		number(15,4),
						vl_material		number(15,4),
						vl_procedimento		number(15,4),
						vl_taxa			number(15,4),
						vl_diaria		number(15,4),
						vl_custo 		number(15,4))');
else
	EXEC_SQL_DINAMICO('TASY','drop table EIS_CIRURGIA_INTERMED_W');
	EXEC_SQL_DINAMICO('TASY','create table EIS_CIRURGIA_INTERMED_W 
						(nr_sequencia 		number(5),
						nr_cirurgia		number(10),						
						cd_convenio		number(10),
						ds_convenio		varchar2(60),
						qt_cirurgias		number(10,4),
						qt_total_min		number(10,4),
						vl_faturamento		number(15,4),
						vl_material		number(15,4),
						vl_procedimento		number(15,4),
						vl_taxa			number(15,4),
						vl_diaria		number(15,4),
						vl_custo 		number(15,4))');
end if;

commit;
end;

Open C01;
LOOP
	Fetch C01 into	cd_convenio_w,
			ds_convenio_w,
			qt_total_min_w,			
			nr_cirurgia_w;
	exit when C01%notfound;
	
	nr_sequencia_w:= nr_sequencia_w + 1;
		
	
	select	count(*)
	into	nr_count_w
	from	conta_paciente_resumo
	where	nr_cirugia 		= nr_cirurgia_w;

	vl_taxa_w	:= 0;
	vl_diaria_w	:= 0;

	if	(nr_count_w = 0) then
		vl_material_w		:= 0;
		vl_procedimento_w	:= 0;
		vl_custo_w		:= 0;
	else
		
		select	max(nvl(nr_interno_conta,0))
		into	nr_interno_conta_w
		from	conta_paciente_resumo
		where	nr_cirugia 		= nr_cirurgia_w;
	
		select	sum(vl_material), 
			sum(vl_procedimento),
			sum(vl_custo)
		into	vl_material_w,
			vl_procedimento_w,
			vl_custo_w
		from	conta_paciente_resumo
		where	nr_interno_conta = nr_interno_conta_w;
	
		/* Taxas */
		begin
		select	nvl(sum(vl_procedimento),0)
		into	vl_taxa_w
		from	conta_paciente_resumo
		where	nr_interno_conta = nr_interno_conta_w
		and 	cd_estrutura_conta 	= '73';
		exception
			when	others then
				vl_taxa_w:= 0;
		end;
		
		/* Di�rias */
		begin
		select	nvl(sum(vl_procedimento),0)
		into	vl_diaria_w
		from	conta_paciente_resumo
		where	nr_interno_conta = nr_interno_conta_w
		and 	cd_estrutura_conta 	= '72';
		exception
			when	others then
				vl_diaria_w:= 0;
		end;

	
	end if;


	qt_cirurgias_w	:= 1;
	vl_faturamento_w:=  vl_material_w + vl_procedimento_w;  
		
	ds_comando_w	:= 	'insert	into EIS_CIRURGIA_INTERMED_W values 
						(' || nr_sequencia_w || ', ' || nr_cirurgia_w || ', ' || cd_convenio_w || 
						', ' || chr(39) || ds_convenio_w || chr(39) || ', ' || qt_cirurgias_w  || 
						', ' || qt_total_min_w || ', ' || replace(to_char(vl_faturamento_w),',','.') 
						|| ', ' || replace(to_char(vl_material_w),',','.') || ', ' || 
						replace(to_char(vl_procedimento_W),',','.') || ', ' ||  replace(to_char(vl_taxa_W),',','.')
						|| ', ' || replace(to_char(vl_diaria_W),',','.') || ', ' || replace(to_char(vl_custo_w),',','.') ||')';
	
	
	EXEC_SQL_DINAMICO('TASY',ds_comando_w);
		
end loop;
close C01;

commit;

END GERAR_RELAT_EIS_CIRURGIA_HI;
/