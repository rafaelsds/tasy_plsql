create or replace
procedure alterar_dest_gasto_item(	nr_sequencia_p		number,
									ie_trat_conta_rn_p	varchar2,
									ie_tipo_item_p		number,
									nm_usuario_p		varchar2) is 

ie_trat_conta_rn_w			varchar2(15) :='0';
cd_convenio_w				number(5);									
begin

if	(ie_tipo_item_p = 1) then

	select	nvl(max(ie_trat_conta_rn),'0'),
		nvl(max(cd_convenio),'0')
	into	ie_trat_conta_rn_w,
		cd_convenio_w
	from	procedimento_paciente
	where	nr_sequencia = nr_sequencia_p;

	if		(ie_trat_conta_rn_w <> '0') then
		update	procedimento_paciente
		set		ie_trat_conta_rn = ie_trat_conta_rn_p
		where	nr_sequencia 	 = nr_sequencia_p;
	end if;
	
	atualiza_preco_procedimento(nr_sequencia_p, cd_convenio_w,nm_usuario_p);
	
else
	select	nvl(max(ie_trat_conta_rn),'0')
	into	ie_trat_conta_rn_w
	from	material_atend_paciente
	where	nr_sequencia = nr_sequencia_p;

	if	(ie_trat_conta_rn_w <> '0') then
		update	material_atend_paciente
		set		ie_trat_conta_rn = ie_trat_conta_rn_p
		where	nr_sequencia 	 = nr_sequencia_p;
	end if;
	
	atualiza_preco_material(nr_sequencia_p, nm_usuario_P);
	
	
end if;

commit;

end alterar_dest_gasto_item;
/