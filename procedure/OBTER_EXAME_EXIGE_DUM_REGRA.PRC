create or replace
procedure obter_exame_exige_dum_regra(	nr_prescricao_p		number,
					cd_pessao_fisica_p	varchar2,
					ds_exames_mest_p	out	varchar2) is 

ds_exame_exige_mestruacao_w	varchar2(255);
ds_retorno_w			varchar2(4000);
qt_idade_w			number(3);
ie_obriga_dum_w			varchar2(1);

Cursor C01 is
	select	obter_desc_exame(a.nr_seq_exame)
	from 	prescr_procedimento b,
		exame_laboratorio a
	where 	nr_prescricao 		= nr_prescricao_p
	and   	b.nr_seq_exame 		= a.nr_seq_exame
	and   	a.ie_exige_menstruacao 	= 'S'
	order	by 1;
	
Cursor C02 is
		select	SUBSTR(obter_desc_exame(a.nr_seq_exame),1,255)
		from 	prescr_procedimento b,
				regra_exame_lab_dum a
		where 	nr_prescricao 		= nr_prescricao_p
		and   	b.nr_seq_exame 		= a.nr_seq_exame
		and   	((a.qt_idade_min < qt_idade_w) and (qt_idade_w < a.qt_idade_max))
	union all
		select	SUBSTR(obter_desc_proc_interno(b.nr_seq_proc_interno),1,255)
		from 	prescr_procedimento b,
				regra_exame_lab_dum a
		where 	nr_prescricao 		= nr_prescricao_p
		and   	b.nr_seq_proc_interno = a.nr_seq_proc_interno
		and   	((a.qt_idade_min < qt_idade_w) and (qt_idade_w < a.qt_idade_max))
	union all
		select	SUBSTR(Obter_Descricao_Procedimento(b.cd_procedimento,b.ie_origem_proced),1,255)
		from 	prescr_procedimento b,
				regra_exame_lab_dum a
		where 	nr_prescricao 		= nr_prescricao_p
		and   	b.cd_procedimento 		= a.cd_procedimento
		and		b.ie_origem_proced		= nvl(a.ie_origem_proced,b.ie_origem_proced)
		and   	((a.qt_idade_min < qt_idade_w) and (qt_idade_w < a.qt_idade_max))
		order	by 1;

begin

Obter_param_usuario(916, 911, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_obriga_dum_w);

select	Obter_Idade_PF(cd_pessao_fisica_p,sysdate,'A')
into	qt_idade_w
from 	dual;

if	(Obter_Sexo_PF(cd_pessao_fisica_p,'C') = 'F') then
	begin
	
	if	(ie_obriga_dum_w = 'R') or (ie_obriga_dum_w = 'C') then
		begin
		open C02;
		loop
		fetch C02 into	
			ds_exame_exige_mestruacao_w;
		exit when C02%notfound;
			begin
			if (ds_retorno_w is null) then
				ds_retorno_w := ds_exame_exige_mestruacao_w;
			else
				ds_retorno_w := substr(ds_retorno_w || ' - '|| ds_exame_exige_mestruacao_w,1,4000);
			end if;
			end;
		end loop;
		close C02;
		end;
	else
		begin
		open C01;
		loop
		fetch C01 into	
			ds_exame_exige_mestruacao_w;
		exit when C01%notfound;
			begin
			if (ds_retorno_w is null) then
				ds_retorno_w := ds_exame_exige_mestruacao_w;
			else
				ds_retorno_w := substr(ds_retorno_w || ' - '|| ds_exame_exige_mestruacao_w,1,4000);
			end if;
			end;
		end loop;
		close C01;
		end;
	end if;
	end;
end if;
ds_exames_mest_p	 := ds_retorno_w;
end obter_exame_exige_dum_regra;
/
