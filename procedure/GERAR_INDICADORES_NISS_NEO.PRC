create or replace procedure gerar_indicadores_niss_neo(	dt_referencia_p		date,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number ) is



cd_setor_atendimento_w	number(5,0);
dt_inicial_w		date;
dt_final_w		date;
nr_atendimento_w	number(10);
qt_dias_perma_atend_w	number(15,0);
qt_dias_perma_total_w	number(15,0);
qt_total_pacientes_w	number(15,0);
ie_infeccao_w		varchar2(1) := 'N';
qt_ih_pac_w		number(15,0);
qt_ih_w			number(15,0);
qt_ih_svd_w		number(15,0);
qt_ih_resp_w		number(15,0);
qt_ih_cvc_w		number(15,0);
tx_ih_w			number(15,3);
Tx_densidade_ih_w	number(15,3);
Tx_pac_ih_w		number(15,3);
qt_svd_w		number(15,0);
qt_cvc_w		number(15,0);
qt_respirador_w		number(15,0);
qt_paciente_w		number(15,0);
qt_permanencia_w	number(15,3);
qt_invasividade_w	number(15,3);
ie_exige_infec_disp_w	varchar2(1);
qt_invasividade_cvc_w	number(15,3);
qt_invasividade_svd_w	number(15,3);
qt_invasividade_resp_w	number(15,3);
ie_classif_peso_w	varchar2(15);
qt_ih_cvu_w		number(15,0);
qt_invasividade_cvu_w	number(15,3);
qt_cvu_w		number(15,0);
qt_dias_neo_w		varchar2(10) := null;
cd_pessoa_fisica_w	varchar2(10);
qt_dias_pf_w		number(15,4);

cursor c01 is
	select	cd_setor_atendimento
	from	niss_classif_setor b,
		setor_atendimento a
	where	a.nr_seq_classif_niss	= b.nr_sequencia
	and	a.nr_seq_classif_niss is not null
	and	ie_neonatal = 'S';

cursor c02 is
	select	a.nr_atendimento
	from	atendimento_paciente b,
		atend_paciente_unidade a
	where	a.nr_atendimento = b.nr_atendimento
	and	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	trunc(nvl(b.dt_alta,sysdate)) >= trunc(b.dt_entrada)
	and	a.dt_entrada_unidade between dt_inicial_w and dt_final_w
--	and	obter_classif_peso_neo(obter_primeiro_sinal_vital(b.nr_atendimento,'Peso')) = ie_classif_peso_w
   AND obter_classif_peso_neo(obter_dados_pf(b.cd_pessoa_fisica, 'KGN')) = ie_classif_peso_w
	group by a.nr_atendimento
	union
	select	a.nr_atendimento
	from	atendimento_paciente b,
		atend_paciente_unidade a
	where	a.nr_atendimento = b.nr_atendimento
	and	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	trunc(nvl(b.dt_alta,sysdate)) >= trunc(b.dt_entrada)
	and	a.dt_saida_interno between dt_inicial_w and dt_final_w
--	and	obter_classif_peso_neo(obter_primeiro_sinal_vital(b.nr_atendimento,'Peso')) = ie_classif_peso_w
   AND obter_classif_peso_neo(obter_dados_pf(b.cd_pessoa_fisica, 'KGN')) = ie_classif_peso_w
	union
	select	a.nr_atendimento
	from	atendimento_paciente b,
		atend_paciente_unidade a
	where	a.nr_atendimento = b.nr_atendimento
	and	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	trunc(nvl(b.dt_alta,sysdate)) >= trunc(b.dt_entrada)
	and	a.dt_entrada_unidade < dt_inicial_w
	and	a.dt_saida_interno > dt_final_w
--	and	obter_classif_peso_neo(obter_primeiro_sinal_vital(b.nr_atendimento,'Peso')) = ie_classif_peso_w
	AND obter_classif_peso_neo(obter_dados_pf(b.cd_pessoa_fisica, 'KGN')) = ie_classif_peso_w
	order by nr_atendimento;

cursor c03 is
	select	(nvl(dt_saida_unidade,sysdate) - dt_entrada_unidade)
	from	atend_paciente_unidade
	where	nr_atendimento = nr_atendimento_w
	and	cd_setor_atendimento = cd_setor_atendimento_w;

Cursor C04 is
	select 	vl_dominio
	from 	valor_dominio_v
	where 	cd_dominio = 1983;


begin

Obter_Param_Usuario(7022,16,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,qt_dias_neo_w);

select	nvl(max(ie_exige_infec_disp),'N')
into	ie_exige_infec_disp_w
from	parametro_medico
where	cd_estabelecimento	=	cd_estabelecimento_p;

dt_inicial_w	:= trunc(dt_referencia_p,'month');
dt_final_w	:= last_day(dt_referencia_p) + 86399/86400;

delete from niss_setor_indice_neo
where dt_mes_ano_referencia = trunc(dt_referencia_p,'month');
commit;


OPEN C01;
LOOP
FETCH C01 into
	cd_setor_atendimento_w;
exit when c01%notfound;
	begin

	open C04;
	loop
	fetch C04 into
		ie_classif_peso_w;
	exit when C04%notfound;
		begin

		qt_dias_perma_total_w	:= 0;
		qt_total_pacientes_w	:= 0;
		qt_ih_pac_w		:= 0;

		OPEN C02;
		LOOP
		FETCH C02 into
			nr_atendimento_w;
		exit when c02%notfound;
			begin

			select	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	atendimento_paciente
			where	nr_atendimento	=	nr_atendimento_w;

			qt_dias_pf_w	:= obter_idade_pf(cd_pessoa_fisica_w,sysdate,'DIA');

			if	(qt_dias_neo_w is null) or
				(qt_dias_pf_w <= campo_numerico(qt_dias_neo_w)) then

				/* Totaliza os pacientes */
				qt_total_pacientes_w	:= qt_total_pacientes_w + 1;

				/* Total de pacientes com infec��o */

				if	(ie_exige_infec_disp_w = 'S') then
					select	nvl(max('S'),'N')
					into	ie_infeccao_w
					from	qua_evento c,
						qua_evento_paciente b,
						atend_pac_dispositivo a
					where	dt_evento between dt_inicial_w and dt_final_w
					and	a.nr_atendimento	=	nr_atendimento_w
					and	b.nr_seq_disp_pac	=	a.nr_sequencia
					and	b.nr_seq_evento		=	c.nr_sequencia
					and	c.ie_infeccao		=	'S'
					and	nvl(b.ie_tipo_evento,'E') = 'E'
					and	obter_dados_dispositivo(a.nr_sequencia,'CS') = cd_setor_atendimento_w;
				else

					select	nvl(max('S'),'N')
					into	ie_infeccao_w
					from	cih_local_infeccao a,
						cih_ficha_ocorrencia b
					where	a.dt_infeccao between dt_inicial_w and dt_final_w
					and	a.nr_ficha_ocorrencia	=	b.nr_ficha_ocorrencia
					and	b.nr_atendimento	=	nr_atendimento_w
					and	cih_obter_tipo_infeccao(a.cd_caso_infeccao) not in ('2')
					and	nvl(ie_contabiliza_niss,'S') = 'S'
					and	campo_numerico(cih_obter_setor_infeccao_niss(a.nr_ficha_ocorrencia, a.dt_infeccao,'C')) = cd_setor_atendimento_w;
				end if;

				if	(ie_infeccao_w = 'S') then
					qt_ih_pac_w := qt_ih_pac_w + 1;
				end if;

				OPEN C03;
				LOOP
				FETCH C03 into
					qt_dias_perma_atend_w;
				exit when c03%notfound;
					begin
					qt_dias_perma_total_w	:= qt_dias_perma_total_w + qt_dias_perma_atend_w;
					end;
				END LOOP;
				CLOSE C03;

			end if;

			end;
		END LOOP;
		CLOSE C02;

		/* Totaliza os epis�dios de IH */
		if	(ie_exige_infec_disp_w = 'S') then
			select	nvl(sum(1),0),
				nvl(sum(decode(obter_classif_dispositivo(nr_seq_dispositivo),'SVD',1,0)),0),
				nvl(sum(decode(obter_classif_dispositivo(nr_seq_dispositivo),'Resp',1,0)),0),
				nvl(sum(decode(obter_classif_dispositivo(nr_seq_dispositivo),'CVC',1,0)),0),
				nvl(sum(decode(obter_classif_dispositivo(nr_seq_dispositivo),'CVU',1,0)),0)
			into	qt_ih_w,
				qt_ih_svd_w,
				qt_ih_resp_w,
				qt_ih_cvc_w,
				qt_ih_cvu_w
			from	qua_evento c,
				qua_evento_paciente b,
				atend_pac_dispositivo a
			where	dt_evento between dt_inicial_w and dt_final_w
			and	b.nr_seq_disp_pac	=	a.nr_sequencia
			and	b.nr_seq_evento		=	c.nr_sequencia
			and	nvl(b.ie_tipo_evento,'E') = 'E'
			and	c.ie_infeccao		=	'S'
			and	obter_classif_peso_neo(obter_primeiro_sinal_vital(b.nr_atendimento,'PesoPfRn')) = ie_classif_peso_w
			and	exists	(	select	1
						from	atend_paciente_unidade x
						where	x.nr_atendimento	= a.nr_atendimento
						and	x.cd_setor_atendimento	= cd_setor_atendimento_w
						and	obter_dados_dispositivo(a.nr_sequencia,'CS') = cd_setor_atendimento_w
						and	((x.dt_entrada_unidade between dt_inicial_w and dt_final_w) or
							 (x.dt_saida_interno between dt_inicial_w and dt_final_w) or
							 ((x.dt_entrada_unidade < dt_inicial_w) and (x.dt_saida_interno > dt_final_w))));
		else
			select	nvl(sum(1),0),
				nvl(sum(decode(obter_dispositivo_sitio(cd_sitio_especifico),'SVD',1,0)),0),
				nvl(sum(decode(obter_dispositivo_sitio(cd_sitio_especifico),'Resp',1,0)),0),
				nvl(sum(decode(obter_dispositivo_sitio(cd_sitio_especifico),'CVC',1,0)),0),
				nvl(sum(decode(obter_dispositivo_sitio(cd_sitio_especifico),'CVU',1,0)),0)
			into	qt_ih_w,
				qt_ih_svd_w,
				qt_ih_resp_w,
				qt_ih_cvc_w,
				qt_ih_cvu_w
			from	niss_sitio_especifico c,
				cih_local_infeccao a,
				cih_ficha_ocorrencia b
			where	a.dt_infeccao between dt_inicial_w and dt_final_w
			and	a.nr_ficha_ocorrencia	=	b.nr_ficha_ocorrencia
			and	a.nr_seq_sitio_espec 	= 	c.nr_sequencia
			and	c.cd_sitio_especifico in ('LCBI','CSEP','PNEU','SUTI','ASB')
			and	nvl(ie_contabiliza_niss,'S') = 'S'
			and	cih_obter_tipo_infeccao(a.cd_caso_infeccao) not in ('2')
			and	obter_classif_peso_neo(obter_primeiro_sinal_vital(b.nr_atendimento,'PesoPfRn')) = ie_classif_peso_w
			and	exists	(	select	1
						from	atend_paciente_unidade x
						where	x.nr_atendimento	= b.nr_atendimento
						and	x.cd_setor_atendimento	= cd_setor_atendimento_w
						and	campo_numerico(cih_obter_setor_infeccao_niss(a.nr_ficha_ocorrencia, a.dt_infeccao,'C')) = cd_setor_atendimento_w
						and	((x.dt_entrada_unidade between dt_inicial_w and dt_final_w) or
							 (x.dt_saida_interno between dt_inicial_w and dt_final_w) or
							 ((x.dt_entrada_unidade < dt_inicial_w) and (x.dt_saida_interno > dt_final_w))));
		end if;



		/* Tx_ih Taxa de infec��o */
		tx_ih_w	:= dividir(qt_ih_w,qt_total_pacientes_w) * 100;

		/* Tx_densidade_ih Densidade de infec��o */
		Tx_densidade_ih_w := dividir(qt_ih_w,qt_dias_perma_total_w) * 100;

		/* Tx_pac_ih Taxa de paciente com IH */
		Tx_pac_ih_w := dividir(qt_ih_pac_w,qt_total_pacientes_w) * 100;

		/* Procedimentos dia / diarias */
		select	sum(qt_svd),
			sum(qt_cvc),
			sum(qt_respirador),
			sum(qt_paciente),
			sum(qt_cvu)
		into	qt_svd_w,
			qt_cvc_w,
			qt_respirador_w,
			qt_paciente_w,
			qt_cvu_w
		from	niss_invasividade
		where	dt_referencia between dt_inicial_w and dt_final_w
		and	cd_setor_atendimento 	= cd_setor_atendimento_w
		and	cd_estabelecimento 	= cd_estabelecimento_p
		and	ie_classif_peso		= ie_classif_peso_w;

		/* Invasividade */
		qt_invasividade_w 	:= dividir((qt_svd_w + qt_cvc_w + qt_respirador_w),qt_paciente_w);
		qt_invasividade_cvc_w	:= dividir(qt_cvc_w,qt_paciente_w);
		qt_invasividade_svd_w	:= dividir(qt_svd_w,qt_paciente_w);
		qt_invasividade_resp_w	:= dividir(qt_respirador_w,qt_paciente_w);
		qt_invasividade_cvu_w	:= dividir(qt_cvu_w,qt_paciente_w);

		/* Perman�ncia */
		qt_permanencia_w := dividir(qt_dias_perma_total_w,qt_total_pacientes_w) ;

		insert into niss_setor_indice_neo(
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_mes_ano_referencia,
			cd_setor_atendimento,
			qt_ih,
			qt_ih_pac,
			qt_ih_svd,
			qt_ih_cvc,
			qt_ih_resp,
			qt_invasividade,
			qt_permanencia,
			tx_ih,
			tx_pac_ih,
			tx_densidade_ih,
			qt_invasividade_cvc,
			qt_invasividade_svd,
			qt_invasividade_resp,
			ie_peso_neo,
			qt_ih_cvu,
			qt_invasividade_cvu)
		values(
			niss_setor_indice_seq.nextval,
			cd_estabelecimento_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			trunc(dt_referencia_p,'month'),
			cd_setor_atendimento_w,
			qt_ih_w,
			qt_ih_pac_w,
			qt_ih_svd_w,
			qt_ih_cvc_w,
			qt_ih_resp_w,
			qt_invasividade_w,
			qt_permanencia_w,
			tx_ih_w,
			Tx_pac_ih_w,
			Tx_densidade_ih_w,
			qt_invasividade_cvc_w,
			qt_invasividade_svd_w,
			qt_invasividade_resp_w,
			ie_classif_peso_w,
			qt_ih_cvu_w,
			qt_invasividade_cvu_w);
		commit;

		end;
	end loop;
	close C04;

	end;
END LOOP;
CLOSE C01;

end gerar_indicadores_niss_neo;
/