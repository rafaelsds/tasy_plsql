create or replace procedure rxt_vincular_atend_hor (
  nr_atendimento_p number,
  cd_pessoa_fisica_p varchar2) is

nr_sequencia_w number;
dia_atual_w date;
dia_posterior_w date;

begin

dia_atual_w := INICIO_DIA(sysdate);
dia_posterior_w := FIM_DIA(sysdate);

if(nr_atendimento_p is not null and cd_pessoa_fisica_p is not null) then
  select  nr_sequencia
  into    nr_sequencia_w
  from    rxt_agenda
  where   dt_agenda between dia_atual_w and dia_posterior_w
  and     nvl(rxt_obter_pf_tratamento(nr_seq_tratamento,null),cd_pessoa_fisica) = cd_pessoa_fisica_p;

  update  rxt_agenda
  set     nr_atendimento = nr_atendimento_p
  where   nr_sequencia = nr_sequencia_w;

  commit;
end if;

exception 
when no_data_found then
  wheb_mensagem_pck.exibir_mensagem_abort(1079652, 'NM_PACIENTE=' || obter_nome_pf(cd_pessoa_fisica_p));
when too_many_rows then
  wheb_mensagem_pck.exibir_mensagem_abort(1079651, 'NM_PACIENTE=' || obter_nome_pf(cd_pessoa_fisica_p));
end rxt_vincular_atend_hor;
/