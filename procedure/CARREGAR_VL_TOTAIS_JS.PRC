create or replace
procedure carregar_vl_totais_js(		cd_estabelecimento_p	number,
					cd_empresa_p		number,
					dt_inicio_p		date,
					dt_final_p		date,
					cd_conta_contabil_p	number,
					cd_tipo_lote_p		number, 
					vl_total_debito_p	out varchar2,
					vl_total_creditp_p	out varchar2,
					vl_total_saldo_atual_p	out varchar2,
					vl_saldo_ant_p		out varchar2) is

vl_total_debito_w		varchar2(30);
vl_total_credito_w		varchar2(30);
vl_total_saldo_atual_w	varchar2(30);
vl_saldo_ant_w		varchar2(30);
				
begin

	if(cd_conta_contabil_p is not null) then
		begin
		
			select	nvl(sum(a.vl_saldo),0) vl_saldo
			into 	vl_saldo_ant_w
			from	ctb_mes_ref b,
				ctb_saldo a
			where	a.nr_seq_mes_ref = b.nr_sequencia
			and	b.cd_empresa		= 	cd_empresa_p
			and	a.cd_estabelecimento	= 	cd_estabelecimento_p
			and	a.cd_conta_contabil	= 	cd_conta_contabil_p
			and	PKG_DATE_UTILS.start_of(b.dt_referencia, 'month', 0)	= PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.ADD_MONTH(dt_inicio_p, -1, 0), 'month', 0);	
			
		
			select  campo_mascara_virgula(nvl(sum(a.vl_debito),0)) 
			into	vl_total_debito_w
			from 	lote_contabil b, 
				ctb_movimento_v a
                     		where	a.nr_lote_contabil = b.nr_lote_contabil 
                      		and	a.cd_estabelecimento =  cd_estabelecimento_p
                      		 and	dt_movimento 
			between dt_inicio_p  and  dt_final_p
                       		and	cd_conta_contabil =  cd_conta_contabil_p 
                        		and 	a.cd_tipo_lote_contabil = cd_tipo_lote_p;
			
			
			select 	campo_mascara_virgula(nvl(sum(a.vl_credito),0)) 
			into	vl_total_credito_w
			from 	lote_contabil b, 
				ctb_movimento_v a 
                        		where	a.nr_lote_contabil = b.nr_lote_contabil
                        		and	a.cd_estabelecimento =  cd_estabelecimento_p
                        		and	dt_movimento 
			between dt_inicio_p  and  dt_final_p
                        		and	cd_conta_contabil = cd_conta_contabil_p 
                        		and 	a.cd_tipo_lote_contabil =  cd_tipo_lote_p;
			
		
			select  campo_mascara_virgula(nvl(sum(vl_saldo),0)) 
			into	vl_total_saldo_atual_w
			from 	ctb_saldo 
			where 	cd_conta_contabil =  cd_conta_contabil_p
                        		and 	cd_estabelecimento = cd_estabelecimento_p 
			and	nr_seq_mes_ref = (	select  nr_sequencia 
							from	ctb_mes_ref  
							where	cd_empresa = cd_empresa_p
							and 	dt_referencia = dt_inicio_p);
		
			vl_total_debito_p 	:=	vl_total_debito_w	;
			vl_total_creditp_p 	:=	vl_total_credito_w	;
			vl_total_saldo_atual_p	:=	vl_total_saldo_atual_w	;
			vl_saldo_ant_p		:=	vl_saldo_ant_w		;	
				
		end;
	end if;
	
commit;

end carregar_vl_totais_js;
/