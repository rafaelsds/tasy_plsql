create or replace
procedure pls_dados_medico_web(cd_medico_p			Number,
			       ie_tipo_medico_p			Varchar2,
			       uf_medico_p			Varchar2,
			       cd_pessoa_fisica_p     	out	Varchar2,
			       nm_pessoa_fisica_p	out	Varchar2) is 			
/*
	ie_tipo_medico_p:   'CRO'  ,   'CRM'
*/
cd_pessoa_fisica_w		Varchar2(10) := '';
nm_pessoa_fisica_w		Varchar2(60) := '';
nr_seq_conselho_w 		Number(10);

begin

nr_seq_conselho_w := pls_obter_nr_conselho_web(ie_tipo_medico_p);

if 	(ie_tipo_medico_p = 'CRO') then
	select 		max(a.cd_pessoa_fisica),
			max(a.nm_pessoa_fisica)
	into		cd_pessoa_fisica_w,
			nm_pessoa_fisica_w
	from		pessoa_fisica a,
			medico	b
	where		b.ie_situacao = 'A' 
	and		a.nr_seq_conselho  = nr_seq_conselho_w
	and		a.cd_pessoa_fisica = b.cd_pessoa_fisica	
	and		a.ds_codigo_prof   = to_char(cd_medico_p)
	and		((b.uf_crm is not null and b.uf_crm = upper(uf_medico_p)) or (uf_medico_p is null));
	
	if	(nm_pessoa_fisica_w is null) then		
		select 		max(a.cd_pessoa_fisica),
				max(a.nm_pessoa_fisica)
		into		cd_pessoa_fisica_w,
				nm_pessoa_fisica_w
		from		pessoa_fisica a,
				medico b
		where		b.ie_situacao = 'A'
		and		a.nr_seq_conselho = nr_seq_conselho_w
		and		a.cd_pessoa_fisica = b.cd_pessoa_fisica
		and		b.nr_crm = cd_medico_p
		and		((b.uf_crm is not null and b.uf_crm = upper(uf_medico_p)) or (uf_medico_p is null));
	end if;	
end if;	

cd_pessoa_fisica_p := cd_pessoa_fisica_w;
nm_pessoa_fisica_p := nm_pessoa_fisica_w;

commit;

end pls_dados_medico_web;
/