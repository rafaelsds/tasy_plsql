create or replace
procedure cf_gravar_dados_guia(
			cd_estabelecimento_p		number,
			cd_convenio_p			number,
			nr_interno_conta_p		number,
			nr_seq_proc_interno_p		number,
			nr_seq_exame_p			number,
			dt_procedimento_p		date,
			ie_opcao_p			varchar2,
			ds_erro_p		out 	varchar2,
			nm_usuario_p			varchar2) is

/* ie_opcao_p - 1: Utilizado dentra da procedure Atualiza_Preco_AMB ; 2: Utilizado na pasta inconsistências da função Central de Faturamento */

cd_empresa_w		number(4,0);
nr_seq_regra_fluxo_w	number(10,0);
nr_seq_status_fat_w	number(10,0);
nr_seq_status_mob_w	number(10,0);
ds_erro_w		varchar2(255);

cursor	c01 is
	select	nvl(nr_sequencia, 0)
	from	cf_regra
	where	nvl(cd_empresa, cd_empresa_w) = cd_empresa_w
	and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
	and	nvl(cd_convenio, cd_convenio_p) = cd_convenio_p
	and	nvl(nr_seq_proc_interno, nvl(nr_seq_proc_interno_p,0)) = nvl(nr_seq_proc_interno_p,0)
	and	nvl(nr_seq_exame, nvl(nr_seq_exame_p,0)) = nvl(nr_seq_exame_p,0)
	and	dt_procedimento_p between dt_vigencia_inicial and nvl(dt_vigencia_final, dt_procedimento_p)
	and	ie_situacao = 'A'
	order by nvl(cd_empresa, 0),
		 nvl(cd_estabelecimento, 0),
		 nvl(cd_convenio, 0),
		 nvl(nr_seq_proc_interno, 0),
		 nvl(nr_seq_exame, 0),
		 dt_vigencia_inicial;

cursor	c02 is
	select	nvl(nr_seq_status_fat, 0)
	from	cf_regra_estagio
	where	nr_seq_regra = nr_seq_regra_fluxo_w
	order by nr_ordem desc;

cursor	c03 is
	select	nvl(nr_seq_status_mob, 0)
	from	cf_regra_status_mob
	where	nvl(nr_seq_status_fat, nr_seq_status_fat_w) = nr_seq_status_fat_w
	and	ie_evento = 'A'
	and	nr_seq_classif_pend is null
	order by nvl(nr_seq_status_fat,0);

begin

ds_erro_w:= '';

select	nvl(max(nr_seq_regra_fluxo),0)
into	nr_seq_regra_fluxo_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

if 	((ie_opcao_p = '1' and nr_seq_regra_fluxo_w = 0) or
	(ie_opcao_p = '2')) then

	select	max(cd_empresa)
	into	cd_empresa_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_p;

	open c01;
	loop
	fetch c01 into
		nr_seq_regra_fluxo_w;
	exit when C01%notfound;
		begin
		nr_seq_regra_fluxo_w:= nr_seq_regra_fluxo_w;
		end;
	end loop;
	close c01;

	if	(nr_seq_regra_fluxo_w > 0) then

		nr_seq_status_fat_w:= 0;
		open c02;
		loop
		fetch c02 into
			nr_seq_status_fat_w;
		exit when C02%notfound;
			begin
			nr_seq_status_fat_w:= nr_seq_status_fat_w;
			end;
		end loop;
		close c02;

		if	(nr_seq_status_fat_w > 0) then

			nr_seq_status_mob_w:= 0;
			open c03;
			loop
			fetch c03 into
				nr_seq_status_mob_w;
			exit when C03%notfound;
				begin
				nr_seq_status_mob_w:= nr_seq_status_mob_w;
				end;
			end loop;
			close c03;

		end if;

		begin
		update	conta_paciente
		set	nr_seq_regra_fluxo = decode(nr_seq_regra_fluxo_w, 0, null, nr_seq_regra_fluxo_w),
			nr_seq_status_fat  = decode(nr_seq_status_fat_w, 0, null, nr_seq_status_fat_w),
			nr_seq_status_mob  = decode(nr_seq_status_mob_w, 0, null, nr_seq_status_mob_w),
			nm_usuario = nm_usuario_p
		where	nr_interno_conta   = nr_interno_conta_p;
		exception
		when others then
			ds_erro_w:= substr(sqlerrm, 1, 255);
		end;

	end if;

end if;

ds_erro_p:= ds_erro_w;

end cf_gravar_dados_guia;
/
