create or replace
procedure hsj_ctb_gerar_aux_event_conhec(	nm_usuario_p		Varchar2,
					cd_empresa_p		varchar2,
					cd_estabelecimento_p	varchar2,
					dt_inicial_p		date,
					dt_final_p		date,
					ie_gerar_arquivo_p	varchar2) is 
					
nr_contador_w 					number(10) := 0;
nr_nota_fiscal_w				nota_fiscal.nr_nota_fiscal%type;
cd_conta_contabil_w				conta_contabil.cd_conta_contabil%type;
dt_vencimento_w					date;
dt_contabil_w					date;
nr_titulo_w					number(10);

ds_erro_w					varchar2(255);
ds_local_w					varchar2(255);
arq_texto_w					utl_file.file_type;
nm_arquivo_w					varchar2(255);
dt_final_w					date;

ds_cabecalho_w 					varchar2(4000) :=  'Nr Evento;Nr Contrato;Dt Emiss�o;Vencimento;' ||
								'Natureza;Cobertura;Dt Conhec Event;Nm Usu�rio Princ;CPF/CNPJ Usu�rio Prin;' ||
								'Nm Usu�rio Event;Dt Ocorr Event;Nm Prest Serv;Tipo Documento;Nr Documento;' ||
								'Nr Titulo;Dt Contabiliza��o;Vl Evento;Conta Cont�bil;Classif Cont�bil;Grupo ANS;';

cursor c_conta is
	select 	cd_conta_contabil
	from	conta_contabil
	where	substr(ctb_obter_classif_conta(cd_conta_contabil, cd_classificacao, dt_inicio_vigencia),1,5) = '4.1.1'
	and	ie_tipo = 'A';
								
cursor c_linha is
	select	a.nr_evento											|| ';' ||
		a.nr_contrato											|| ';' ||
		a.dt_emissao											|| ';' ||
		a.dt_vencimento											|| ';' ||
		obter_valor_dominio(1666,a.ie_tipo_contratacao)							|| ';' ||
		obter_valor_dominio(1665,a.ie_segmentacao)							|| ';' ||
		a.dt_conhecimento										|| ';' ||
		a.nm_usuario_princ										|| ';' ||
		a.nr_cpf_cnpj											|| ';' ||
		a.nm_usuario_evento										|| ';' ||
		a.dt_ocorrencia											|| ';' ||
		a.nm_prestador											|| ';' ||
		a.ds_tipo_documento										|| ';' ||
		a.nr_documento											|| ';' ||
		a.nr_titulo											|| ';' ||
		a.dt_contabilizacao										|| ';' ||
		a.vl_pagar											|| ';' ||
		a.cd_conta_contabil										|| ';' ||
		substr(ctb_obter_classif_conta(a.cd_conta_contabil, null, dt_final_p),1,255)			|| ';' ||
		ds_grupo_ans											ds_linha
	from	w_ctb_livro_aux_event_liq a
	where	a.nm_usuario	= nm_usuario_p
	and 	a.ie_tipo_livro = 3
	and	dt_referencia = dt_final_p;
							
Cursor c_medica is
	/*PROTOCOLO PROCEDIMENTO*/
	select 	c.nr_sequencia nr_evento,
		t.nr_contrato nr_contrato,
		c.dt_emissao dt_emissao,
		t.dt_rescisao_contrato dt_rescisao,
		y.ie_tipo_contratacao ie_tipo_contratacao,
		y.ie_segmentacao ie_segmentacao,
		e.dt_lib_pagamento dt_conhecimento,
		substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
		nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')) nr_cpf_cnpj,
		substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
		c.dt_atendimento dt_ocorrencia,
		substr(pls_obter_dados_prestador(c.nr_seq_prestador_exec,'N'),1,255) nm_prestador,
		decode(nvl(c.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
		p.cd_conta_deb cd_conta_contabil,
		e.nr_sequencia nr_seq_protocolo,
		e.ie_tipo_protocolo,
		c.nr_documento,
		substr(pls_obter_dados_grupo_ans(p.nr_seq_grupo_ans,'D'),1,255) ds_grupo_ans,
		sum(r.vl_liberado) vl_pagar
	from	pls_conta c,
		pls_conta_proc p,
		pls_conta_medica_resumo r,
		pls_protocolo_conta e,
		pls_plano y,
		pls_segurado s,
		pls_contrato t
	where	c.nr_sequencia = p.nr_seq_conta
	and	e.nr_sequencia = c.nr_seq_protocolo
	and	s.nr_sequencia = c.nr_seq_segurado
	and	t.nr_sequencia = s.nr_seq_contrato
	and	y.nr_sequencia = s.nr_seq_plano
	and	p.nr_seq_conta = r.nr_seq_conta
	and	p.nr_sequencia = r.nr_seq_conta_proc
	and	p.cd_conta_deb = cd_conta_contabil_w
	and 	e.ie_tipo_protocolo = 'C'
	and	e.dt_lib_pagamento between dt_inicial_p and dt_final_w
	and	p.cd_conta_deb is not null
	group by c.nr_sequencia,
		t.nr_contrato,
		c.dt_emissao,
		t.dt_rescisao_contrato,
		y.ie_tipo_contratacao,
		y.ie_segmentacao,
		e.dt_lib_pagamento,
		substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
		nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')),
		substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
		c.dt_atendimento,
		substr(pls_obter_dados_prestador(c.nr_seq_prestador_exec,'N'),1,255),
		decode(nvl(c.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
		p.cd_conta_deb,
		e.nr_sequencia,
		e.ie_tipo_protocolo,
		c.nr_documento,
		substr(pls_obter_dados_grupo_ans(p.nr_seq_grupo_ans,'D'),1,255)
	union all
	/*PROTOCOLO MATERIAL*/
	select 	c.nr_sequencia nr_evento,
		t.nr_contrato nr_contrato,
		c.dt_emissao dt_emissao,
		t.dt_rescisao_contrato dt_rescisao,
		y.ie_tipo_contratacao ie_tipo_contratacao,
		y.ie_segmentacao ie_segmentacao,
		e.dt_lib_pagamento dt_conhecimento,
		substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
		nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')) nr_cpf_cnpj,
		substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
		c.dt_atendimento dt_ocorrencia,
		substr(pls_obter_dados_prestador(c.nr_seq_prestador_exec,'N'),1,255) nm_prestador,
		decode(nvl(c.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
		m.cd_conta_deb cd_conta_contabil,
		e.nr_sequencia nr_seq_protocolo,
		e.ie_tipo_protocolo,
		c.nr_documento,
		substr(pls_obter_dados_grupo_ans(m.nr_seq_grupo_ans,'D'),1,255) ds_grupo_ans,
		sum(r.vl_liberado) vl_pagar
	from	pls_conta c,
		pls_conta_mat m,
		pls_conta_medica_resumo r,
		pls_protocolo_conta e,
		pls_plano y,
		pls_contrato t,
		pls_segurado s
	where	c.nr_sequencia = m.nr_seq_conta
	and	e.nr_sequencia = c.nr_seq_protocolo
	and	s.nr_sequencia = c.nr_seq_segurado
	and	t.nr_sequencia = s.nr_seq_contrato
	and	y.nr_sequencia = s.nr_seq_plano
	and	m.nr_seq_conta = r.nr_seq_conta
	and	m.nr_sequencia = r.nr_seq_conta_mat
	and	m.cd_conta_deb = cd_conta_contabil_w
	and 	e.ie_tipo_protocolo = 'C'
	and	e.dt_lib_pagamento between dt_inicial_p and dt_final_w
	and	m.cd_conta_deb is not null
	group by c.nr_sequencia,
		t.nr_contrato,
		c.dt_emissao,
		t.dt_rescisao_contrato,
		y.ie_tipo_contratacao,
		y.ie_segmentacao,
		e.dt_lib_pagamento,
		substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
		nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')),
		substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
		c.dt_atendimento,
		substr(pls_obter_dados_prestador(c.nr_seq_prestador_exec,'N'),1,255),
		decode(nvl(c.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
		m.cd_conta_deb,
		e.nr_sequencia,
		e.ie_tipo_protocolo,
		c.nr_documento,
		substr(pls_obter_dados_grupo_ans(m.nr_seq_grupo_ans,'D'),1,255)
	union all
	/*PROTOCOLO PROCEDIMENTO REEMBOLSO*/
	select 	c.nr_sequencia nr_evento,
		t.nr_contrato nr_contrato,
		c.dt_emissao dt_emissao,
		t.dt_rescisao_contrato dt_rescisao,
		y.ie_tipo_contratacao ie_tipo_contratacao,
		y.ie_segmentacao ie_segmentacao,
		nvl(e.dt_lib_pagamento,e.dt_mes_competencia) dt_conhecimento,
		substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
		nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')) nr_cpf_cnpj,
		substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
		c.dt_atendimento dt_ocorrencia,
		substr(pls_obter_dados_prestador(c.nr_seq_prestador_exec,'N'),1,255) nm_prestador,
		decode(nvl(c.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
		p.cd_conta_deb cd_conta_contabil,
		e.nr_sequencia nr_seq_protocolo,
		e.ie_tipo_protocolo,
		c.nr_documento,
		substr(pls_obter_dados_grupo_ans(p.nr_seq_grupo_ans,'D'),1,255) ds_grupo_ans,
		sum(p.vl_liberado) vl_pagar
	from	pls_conta c,
		pls_conta_proc p,
		pls_protocolo_conta e,
		pls_plano y,
		pls_segurado s,
		pls_contrato t
	where	c.nr_sequencia = p.nr_seq_conta
	and	e.nr_sequencia = c.nr_seq_protocolo
	and	s.nr_sequencia = c.nr_seq_segurado
	and	t.nr_sequencia = s.nr_seq_contrato
	and	y.nr_sequencia = s.nr_seq_plano
	and	p.cd_conta_deb = cd_conta_contabil_w
	and 	e.ie_tipo_protocolo = 'R'
	and	nvl(e.dt_lib_pagamento,e.dt_mes_competencia) between dt_inicial_p and dt_final_w
	and	p.cd_conta_deb is not null
	group by c.nr_sequencia,
		t.nr_contrato,
		c.dt_emissao,
		t.dt_rescisao_contrato,
		y.ie_tipo_contratacao,
		y.ie_segmentacao,
		nvl(e.dt_lib_pagamento,e.dt_mes_competencia),
		substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
		nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')),
		substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
		c.dt_atendimento,
		substr(pls_obter_dados_prestador(c.nr_seq_prestador_exec,'N'),1,255),
		decode(nvl(c.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
		p.cd_conta_deb,
		e.nr_sequencia,
		e.ie_tipo_protocolo,
		c.nr_documento,
		substr(pls_obter_dados_grupo_ans(p.nr_seq_grupo_ans,'D'),1,255)
	union all
	/*PROTOCOLO MATERIAL REEMBOLSO*/
	select	c.nr_sequencia nr_evento,
		t.nr_contrato nr_contrato,
		c.dt_emissao dt_emissao,
		t.dt_rescisao_contrato dt_rescisao,
		y.ie_tipo_contratacao ie_tipo_contratacao,
		y.ie_segmentacao ie_segmentacao,
		nvl(e.dt_lib_pagamento,e.dt_mes_competencia) dt_conhecimento,
		substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255) nm_usuario_princ,
		nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')) nr_cpf_cnpj,
		substr(obter_nome_pf(s.cd_pessoa_fisica),1,100) nm_usuario_evento,
		c.dt_atendimento dt_ocorrencia,
		substr(pls_obter_dados_prestador(c.nr_seq_prestador_exec,'N'),1,255) nm_prestador,
		decode(nvl(c.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA') ds_tipo_documento,
		m.cd_conta_deb cd_conta_contabil,
		e.nr_sequencia nr_seq_protocolo,
		e.ie_tipo_protocolo,
		c.nr_documento,
		substr(pls_obter_dados_grupo_ans(m.nr_seq_grupo_ans,'D'),1,255) ds_grupo_ans,
		sum(m.vl_liberado) vl_pagar
	from	pls_conta c,
		pls_conta_mat m,
		pls_protocolo_conta e,
		pls_plano y,
		pls_contrato t,
		pls_segurado s
	where	c.nr_sequencia = m.nr_seq_conta
	and	e.nr_sequencia = c.nr_seq_protocolo
	and	s.nr_sequencia = c.nr_seq_segurado
	and	t.nr_sequencia = s.nr_seq_contrato
	and	y.nr_sequencia = s.nr_seq_plano
	and	m.cd_conta_deb = cd_conta_contabil_w
	and 	e.ie_tipo_protocolo = 'R'
	and	nvl(e.dt_lib_pagamento,e.dt_mes_competencia) between dt_inicial_p and dt_final_w
	and	m.cd_conta_deb is not null
	group by c.nr_sequencia,
		t.nr_contrato,
		c.dt_emissao,
		t.dt_rescisao_contrato,
		y.ie_tipo_contratacao,
		y.ie_segmentacao,
		nvl(e.dt_lib_pagamento,e.dt_mes_competencia),
		substr(pls_obter_nomes_contrato(s.nr_seq_pagador,'P'),1,255),
		nvl(pls_obter_dados_pagador(s.nr_seq_pagador,'CPF'),pls_obter_dados_pagador(s.nr_seq_pagador,'CGC')),
		substr(obter_nome_pf(s.cd_pessoa_fisica),1,100),
		c.dt_atendimento,
		substr(pls_obter_dados_prestador(c.nr_seq_prestador_exec,'N'),1,255),
		decode(nvl(c.cd_pessoa_fisica,'X'),'X','NOTA FISCAL','RPA/FATURA'),
		m.cd_conta_deb,
		e.nr_sequencia,
		e.ie_tipo_protocolo,
		c.nr_documento,
		substr(pls_obter_dados_grupo_ans(m.nr_seq_grupo_ans,'D'),1,255);
	
vet_medica c_medica%rowtype;
			
begin

dt_final_w	:= fim_dia(dt_final_p);

delete
from	w_ctb_livro_aux_event_liq a
where	a.nm_usuario	= nm_usuario_p
and	a.ie_tipo_livro = 3
and 	dt_referencia = dt_final_p;

commit;

open c_conta;
loop
fetch c_conta into	
	cd_conta_contabil_w;
exit when c_conta%notfound;
	begin
	
	open c_medica;
	loop
	fetch c_medica into	
		vet_medica;
	exit when c_medica%notfound;
		begin
		nr_contador_w := nr_contador_w + 1;
		
		nr_nota_fiscal_w	:= null;
		nr_titulo_w		:= null;

		if (vet_medica.ie_tipo_protocolo = 'C') then
			select	max(substr(obter_dados_nota_fiscal(n.nr_seq_nota_fiscal,'0'),1,255))
			into	nr_nota_fiscal_w
			from	pls_pagamento_nota n
			where	n.nr_seq_pagamento in (	select	p.nr_sequencia
							from	pls_conta c,
								pls_conta_medica_resumo r,
								pls_lote_pagamento l,
								pls_pagamento_prestador p
							where	c.nr_sequencia = r.nr_seq_conta
							and 	l.nr_sequencia = p.nr_seq_lote 
							and	l.nr_sequencia = r.nr_seq_lote_pgto
							and	c.nr_sequencia = vet_medica.nr_evento);
			
			select	max(v.nr_titulo)
			into	nr_titulo_w
			from	pls_pag_prest_vencimento v
			where	v.nr_seq_pag_prestador in (	select	p.nr_sequencia
								from	pls_conta c,
									pls_conta_medica_resumo r,
									pls_lote_pagamento l,
									pls_pagamento_prestador p
								where	c.nr_sequencia = r.nr_seq_conta
								and 	l.nr_sequencia = p.nr_seq_lote 
								and	l.nr_sequencia = r.nr_seq_lote_pgto
								and	c.nr_sequencia = vet_medica.nr_evento);
		
			if (nr_titulo_w > 0) then
				select	dt_vencimento
				into 	dt_vencimento_w
				from	pls_pag_prest_vencimento
				where	nr_titulo = nr_titulo_w;
			end if;
			
		elsif (vet_medica.ie_tipo_protocolo = 'R') then
			nr_nota_fiscal_w	:= vet_medica.nr_documento;
		
			select 	nvl(max(nr_titulo),0)
			into 	nr_titulo_w
			from 	(
				select	t.nr_titulo
				from	pls_protocolo_conta	p,
					titulo_pagar 		t
				where	p.nr_sequencia = t.nr_seq_reembolso
				and	p.nr_sequencia = vet_medica.nr_seq_protocolo)
			where 	rownum = 1;
			
			if (nr_titulo_w > 0) then
				select	dt_vencimento_atual
				into 	dt_vencimento_w
				from	titulo_pagar
				where	nr_titulo = nr_titulo_w;
			end if;
		end if;
		
		if (nr_titulo_w > 0) then
			select 	t.dt_contabil
			into	dt_contabil_w
			from	titulo_pagar t
			where 	t.nr_titulo = nr_titulo_w;
		end if;
		
		insert into w_ctb_livro_aux_event_liq(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nm_prestador,
				ds_tipo_documento,
				nr_documento,
				nr_titulo,
				nr_evento,
				nr_contrato,
				dt_emissao,
				dt_rescisao,
				ie_tipo_contratacao,
				ie_segmentacao,
				nm_usuario_princ,
				nr_cpf_cnpj,
				nm_usuario_evento,
				dt_ocorrencia,
				dt_conhecimento,
				ds_grupo_ans,
				dt_contabilizacao,
				dt_vencimento,
				dt_pagamento,
				vl_evento,
				vl_pagar,
				vl_glosa,
				ie_tipo_livro,
				dt_referencia,
				cd_conta_contabil)
			values(	w_ctb_livro_aux_event_liq_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				vet_medica.nm_prestador,
				vet_medica.ds_tipo_documento,
				nr_nota_fiscal_w,
				nr_titulo_w,
				vet_medica.nr_evento,
				vet_medica.nr_contrato,
				vet_medica.dt_emissao,
				vet_medica.dt_rescisao,
				vet_medica.ie_tipo_contratacao,
				vet_medica.ie_segmentacao,
				vet_medica.nm_usuario_princ,
				vet_medica.nr_cpf_cnpj,
				vet_medica.nm_usuario_evento,
				vet_medica.dt_ocorrencia,
				vet_medica.dt_conhecimento,
				vet_medica.ds_grupo_ans,
				dt_contabil_w,
				dt_vencimento_w,
				null,
				null,
				vet_medica.vl_pagar,
				null,
				3,
				dt_final_p,
				vet_medica.cd_conta_contabil);
				
			if (mod(nr_contador_w, 1000) = 0) then
				commit;
			end if;
		
		end;
	end loop;
	close c_medica;
	end;
end loop;
close c_conta;

commit;

if	(ie_gerar_arquivo_p = 'S') then
	begin
	nm_arquivo_w	:= 'LivroEventConhec' || to_char(sysdate,'ddmmyyyyhh24miss') || nm_usuario_p || '.csv';
	
	obter_evento_utl_file(1, null, ds_local_w, ds_erro_w);

	arq_texto_w := utl_file.fopen(ds_local_w,nm_arquivo_w,'W'); --arq_texto_w := utl_file.fopen('/srvfs03/FINANCEIRO/TASY/',nm_arquivo_w,'W');

	utl_file.put_line(arq_texto_w, ds_cabecalho_w);
	utl_file.fflush(arq_texto_w);
	for vetl in c_linha loop
		begin
		utl_file.put_line(arq_texto_w,vetl.ds_linha);
		utl_file.fflush(arq_texto_w);
		end;
	end loop;
	end;
end if;

end hsj_ctb_gerar_aux_event_conhec;
/
