create or replace
procedure inserir_codigo_fornecedor( nr_sequencia_p 		number,
				     nm_usuario_p		varchar2,
				     cd_fornecedor_p            varchar2,
				     nr_item_nf_p               number) is 

begin

	update nota_fiscal_item
	set    cd_mat_proc_fornec = cd_fornecedor_p,
	         dt_atualizacao = sysdate
	where  nr_sequencia = nr_sequencia_p
	and    nr_item_nf = nr_item_nf_p;


commit;

end inserir_codigo_fornecedor;
/