create or replace
procedure alterar_ativ_prev_man_os(
			dt_fim_p		date,
			nr_sequencia_p		number) is 

begin

update	man_ordem_servico 
set	dt_fim_previsto = dt_fim_p 
where	nr_sequencia = nr_sequencia_p; 

commit;

end alterar_ativ_prev_man_os;
/
