create or replace
procedure ptu_gerar_a560_41(	nr_seq_nota_deb_p	number,
				nm_usuario_p		Varchar2) is 

ds_conteudo_w			varchar2(4000);	
cd_unimed_origem_w		varchar2(4);
cd_unimed_destino_w		varchar2(4);
nr_fatura_w			varchar2(11);
vl_total_fatura_w		varchar2(14) := null;
nr_nota_debito_w		varchar2(11);
dt_ven_nota_w			varchar2(8);	
nr_titulo_w			number(10);
nr_seq_conta_banco_w		number(10);
nr_bloqueto_w			varchar2(44);
nr_seq_carteira_cobr_w		number(10);
cd_banco_w			varchar2(5);
cd_agencia_bancaria_w		varchar2(25);
cd_carteira_w			varchar2(10);
nr_nosso_numero_w		varchar2(20);
vl_titulo_w			varchar2(14);
dt_documento_w			date;
dt_processo_w			date;
nr_seq_ptu_fatura_w		number(10);
nr_nota_credito_debito_a500_w	varchar2(30);
vl_total_ndc_a500_w		varchar2(14);
ie_conclusao_ndc_w		varchar2(1);
ds_uso_banco_w			varchar2(15);
ds_especie_w			varchar2(4);
ds_especie_doc_w		varchar2(5);
ds_aceite_w			varchar2(2);
ds_local_pagto_w		varchar2(60);
ds_obs_local_pagto_w		varchar2(60);
nr_seq_nota_deb_bol_w		number(10);
ds_instrucao_w			varchar2(60);
ds_observacao_w			varchar2(60);
nr_linha_digitavel_w		varchar2(60);
cd_barras_w			varchar2(44);
qt_registro_w			number(10) := 0;
tp_boleto_w			varchar2(1);
			
begin
if	(nr_seq_nota_deb_p is not null) then
	delete from w_ptu_envio_arq 
	where  nm_usuario = nm_usuario_p;
	
	begin
	select	nr_seq_ptu_fatura
	into	nr_seq_ptu_fatura_w
	from	pls_lote_contestacao	c,
		ptu_camara_contestacao	b,
		ptu_nota_debito	a
	where	b.nr_sequencia	= a.nr_seq_camara_contest
	and	c.nr_sequencia	= b.nr_seq_lote_contest
	and	a.nr_sequencia	= nr_seq_nota_deb_p;
	exception
	when others then
		nr_seq_ptu_fatura_w := null;
	end;
	
	if	(nr_seq_ptu_fatura_w is not null) then
		select	lpad(replace(replace(campo_mascara(pls_obter_dados_ptu_fatura(nr_seq_ptu_fatura_w,'VT'),2),',',''),'.',''),14,'0')
		into	vl_total_fatura_w
		from	dual;
	end if;	
	
	-- HEADER
	begin
	select	lpad(a.cd_unimed_origem,4,'0'),
		lpad(a.cd_unimed_destino,4,'0'),
		lpad(a.nr_fatura,11,'0'),
		decode(vl_total_fatura_w,null,lpad(replace(replace(campo_mascara(a.vl_total_fatura,2),',',''),'.',''),14,'0'),vl_total_fatura_w),
		lpad(nvl(a.nr_nota_debito,'0'),11,'0'),
		lpad(nvl(trim(to_char(a.dt_ven_nota,'YYYYMMDD')),' '),8,' '),
		a.nr_nota_debito,
		lpad(nvl(a.nr_nota_credito_debito_a500,0),11,'0'),
		decode(a.vl_total_ndc_a500,null,lpad('0',14,'0'),lpad(replace(replace(campo_mascara(nvl(a.vl_total_ndc_a500,0),2),',',''),'.',''),14,'0')),
		nvl(a.ie_conclusao_ndc,' ')
	into	cd_unimed_origem_w,
		cd_unimed_destino_w,
		nr_fatura_w,
		vl_total_fatura_w,
		nr_nota_debito_w,
		dt_ven_nota_w,
		nr_titulo_w,
		nr_nota_credito_debito_a500_w,
		vl_total_ndc_a500_w,
		ie_conclusao_ndc_w
	from	ptu_nota_debito a
	where	a.nr_sequencia = nr_seq_nota_deb_p;	
	exception
	when others then
		null;
	end;
	
	ds_conteudo_w :=	'00000001' || '561' || cd_unimed_destino_w || cd_unimed_origem_w || nr_fatura_w || vl_total_fatura_w || '03' ||
				nr_nota_credito_debito_a500_w||vl_total_ndc_a500_w;
	
	insert into w_ptu_envio_arq
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ds_conteudo)
	values	(w_ptu_envio_arq_seq.nextval,
		sysdate,
		nm_usuario_p,
		ds_conteudo_w);
	
	-- DADOS DA NOTA DE D�BITO DA CONCLUS�O
	ds_conteudo_w := '00000002' || '562' || nr_nota_debito_w || dt_ven_nota_w || ie_conclusao_ndc_w;
	
	insert into w_ptu_envio_arq
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ds_conteudo)
	values	(w_ptu_envio_arq_seq.nextval,
		sysdate,
		nm_usuario_p,
		ds_conteudo_w);	
		
	select	max(nr_bloqueto)
	into	nr_bloqueto_w
	from	titulo_receber
	where	nr_titulo	= nr_titulo_w;
	
	if	(nr_bloqueto_w is not null) then
		select	count(1)
		into	qt_registro_w
		from	ptu_nota_deb_bol	x
		where	x.nr_seq_nota_debito	= nr_seq_nota_deb_p;	
		
		if	(qt_registro_w > 0) then
			pls_obter_dados_arq_ndc( nr_seq_nota_deb_p, cd_banco_w, cd_agencia_bancaria_w, nr_nosso_numero_w, ds_uso_banco_w, cd_carteira_w, ds_especie_w,
						 vl_titulo_w, dt_documento_w, ds_especie_doc_w, ds_aceite_w, dt_processo_w, ds_local_pagto_w, ds_obs_local_pagto_w,
						 ds_instrucao_w, ds_observacao_w, nr_linha_digitavel_w, cd_barras_w, tp_boleto_w);
		else
			pls_obter_dados_arq_ndc_ant( nr_seq_nota_deb_p, cd_banco_w, cd_agencia_bancaria_w, nr_nosso_numero_w, ds_uso_banco_w, cd_carteira_w,
							ds_especie_w, vl_titulo_w, dt_documento_w, ds_especie_doc_w, ds_aceite_w, dt_processo_w, ds_local_pagto_w,
							ds_obs_local_pagto_w, ds_instrucao_w, ds_observacao_w, nr_linha_digitavel_w, cd_barras_w, tp_boleto_w);
		end if;
		
		-- BOLETO			
		ds_conteudo_w :=	'00000003' || 
					'563' || 
					rpad(nvl(cd_banco_w,' '),5,' ') || 
					rpad(nvl(cd_agencia_bancaria_w,' '),25,' ') ||
					rpad(nvl(nr_nosso_numero_w,' '),20,' ') || 
					lpad(nvl(ds_uso_banco_w,' '),15,' ') || 
					lpad(nvl(cd_carteira_w,' '),10,' ') ||
					rpad(nvl(ds_especie_w,' '),4,' ') || 
					lpad(replace(replace(campo_mascara_virgula(nvl(vl_titulo_w,0)),'.',''),',',''),14,'0') ||
					rpad(nvl(to_char(dt_documento_w,'YYYYMMDD'),' '),8,' ') ||
					rpad(nvl(ds_especie_doc_w,' '),5,' ') || 
					rpad(nvl(ds_aceite_w,' '),2,' ') || 
					rpad(nvl(to_char(dt_processo_w,'YYYYMMDD'),' '),8,' ') || 
					rpad(ds_local_pagto_w,60,' ') || 
					rpad(ds_obs_local_pagto_w,60,' ');
		
		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w);	
		
		-- INSTRU��ES		
		ds_conteudo_w := '00000004' || '564' || lpad(nvl(ds_instrucao_w,' '),60,' ');
		
		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w);	
		
		-- OBSERVA��ES	
		ds_conteudo_w := '00000005' || '565' || lpad(nvl(ds_observacao_w,' '),60,' ');
		
		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w);	
		
		-- LINHA DIGITAVEL			
		ds_conteudo_w := '00000006' || '566' || rpad(nvl(nr_linha_digitavel_w,' '),60,' ') || rpad(nvl(cd_barras_w,' '),44,' ');
		
		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w);	
	end if;
		
	commit;
end if;

end ptu_gerar_a560_41;
/