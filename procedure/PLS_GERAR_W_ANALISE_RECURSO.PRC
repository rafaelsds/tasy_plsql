create or replace
procedure pls_gerar_w_analise_recurso(	nr_seq_analise_p		pls_analise_conta.nr_sequencia%type,
					nr_id_transacao_p	in out	pls_analise_conta_rec.nr_id_transacao%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
					nm_usuario_p			usuario.nm_usuario%type,
					ie_opcao_p			varchar2) is 
					
-- ie_opcao_p
-- N - NOVO		E - EXCLUIR

-- NR_ORDEM
-- '1'	'PROCEDIMENTO'	 '2'	'TAXA'	 	'3'	'DI�RIA'
-- '4'	'PACOTE'		 '5'	'MEDICAMENTO'	'6'	'GASE'
-- '7'	'MATERIAL'	'8'	'OPM'

-- IE_SITUACAO
-- N	Negado		A	Acatado		P	Parcial
				
nr_id_transacao_w		pls_analise_conta_rec.nr_id_transacao%type;
ds_item_w			pls_analise_conta_rec.ds_item%type;
nr_ordem_w			pls_analise_conta_rec.nr_ordem%type;

Cursor C00 (nr_seq_analise_pc		pls_analise_conta_rec.nr_sequencia%type) is
	select	b.nr_sequencia nr_seq_conta,
		a.nr_sequencia nr_seq_conta_rec,
		b.ie_tipo_guia,
		b.nr_seq_protocolo,
		b.cd_guia_ok,
		a.nr_seq_protocolo nr_seq_protocolo_rec
	from	pls_rec_glosa_conta	a,
		pls_conta		b
	where	b.nr_sequencia		= a.nr_seq_conta
	and	a.nr_seq_analise	= nr_seq_analise_pc
	and	a.ie_status		!= '3';

Cursor C01 (nr_seq_conta_rec_pc		pls_rec_glosa_conta.nr_sequencia%type) is
	select	b.nr_sequencia nr_seq_conta_proc,
		null nr_seq_conta_mat,
		a.nr_sequencia nr_seq_proc_rec,
		null nr_seq_mat_rec,
		b.cd_procedimento,
		b.ie_origem_proced,
		null cd_material,
		decode(nvl(a.vl_acatado,0),0,'N',decode(nvl(a.vl_acatado,0),nvl(a.vl_recursado,0),'A','P')) ie_situacao,
		b.ie_tipo_despesa,
		'P' ie_tipo_item,
		b.dt_procedimento dt_item,
		b.ie_via_acesso,
		b.ds_procedimento_imp ds_item,
		null nr_seq_material,
		b.nr_seq_pacote,
		nvl(a.vl_recursado,0) vl_recursado,
		nvl(a.vl_acatado,0) vl_acatado,
		nvl(b.vl_glosa,0) vl_glosa
	from	pls_conta_proc		b,
		pls_rec_glosa_proc	a
	where	a.nr_seq_conta_proc	= b.nr_sequencia
	and	a.nr_seq_conta_rec	= nr_seq_conta_rec_pc
	union all
	select	null nr_seq_conta_proc,
		b.nr_sequencia nr_seq_conta_mat,
		null nr_seq_proc_rec,
		a.nr_sequencia nr_seq_mat_rec,
		null cd_procedimento,
		null ie_origem_proced,
		substr(pls_obter_dados_conta_mat(nr_seq_conta_mat, 'O'),1,255) cd_material,
		decode(nvl(a.vl_acatado,0),0,'N',decode(nvl(a.vl_acatado,0),nvl(a.vl_recursado,0),'A','P')) ie_situacao,
		b.ie_tipo_despesa,
		'M' ie_tipo_item,
		b.dt_atendimento dt_item,
		null ie_via_acesso,
		b.ds_material_imp ds_item,
		b.nr_seq_material,
		null nr_seq_pacote,
		nvl(a.vl_recursado,0) vl_recursado,
		nvl(a.vl_acatado,0) vl_acatado,
		nvl(b.vl_glosa,0) vl_glosa
	from	pls_conta_mat		b,
		pls_rec_glosa_mat	a
	where	a.nr_seq_conta_mat	= b.nr_sequencia
	and	a.nr_seq_conta_rec	= nr_seq_conta_rec_pc;

begin
delete	pls_analise_conta_rec
where	dt_atualizacao_nrec	<= (sysdate - 4);
commit;

if	(nr_seq_analise_p is not null) and
	(ie_opcao_p = 'E') then
	delete	pls_analise_conta_rec
	where	nr_id_transacao	= nr_id_transacao_p;
	commit;
	
elsif	(nr_seq_analise_p is not null) and
	(ie_opcao_p = 'N') then
	
	select	pls_id_transacao_anali_rec_seq.nextval
	into	nr_id_transacao_w
	from 	dual;
	
	for r_C00_w in C00 (nr_seq_analise_p) loop

		for c01_w in c01 (r_C00_w.nr_seq_conta_rec) loop
		
			if	(c01_w.cd_procedimento is not null) and
				(c01_w.ie_origem_proced is not null) then
				select	nvl(max(ds_procedimento),c01_w.ds_item)
				into	ds_item_w
				from	procedimento
				where	cd_procedimento	= c01_w.cd_procedimento
				and	ie_origem_proced= c01_w.ie_origem_proced;
				
			elsif	(c01_w.nr_seq_conta_mat is not null) then
				select	nvl(max(ds_material),c01_w.ds_item)
				into	ds_item_w
				from	pls_material
				where	nr_sequencia	= c01_w.nr_seq_material;
				
			end if;
			
			if	(c01_w.nr_seq_conta_proc is not null) then
				nr_ordem_w := c01_w.ie_tipo_despesa;
				
				if	(c01_w.nr_seq_pacote is not null) then
					nr_ordem_w := '4';
				end if;
				
			elsif	(c01_w.nr_seq_conta_mat is not null) then
				select	decode(c01_w.ie_tipo_despesa,'1','6','7','8','2','5','7')
				into	nr_ordem_w 
				from	dual;
			end if;
		
			insert into pls_analise_conta_rec (	
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_analise,
				nr_id_transacao,
				nr_seq_conta,
				nr_seq_conta_proc,
				nr_seq_conta_mat,
				nr_seq_conta_rec,
				nr_seq_proc_rec,
				nr_seq_mat_rec,
				nr_ordem,
				cd_procedimento,
				ie_origem_proced,
				cd_material_ops,
				ie_situacao,
				ie_tipo_despesa,
				ie_tipo_guia,
				ie_tipo_item,
				dt_item,
				ie_via_acesso,
				nr_seq_protocolo,
				cd_guia,
				ds_item,
				vl_glosa,
				vl_recursado,
				vl_acatado,
				nr_seq_protocolo_rec)
			values(	pls_analise_conta_rec_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_analise_p,
				nr_id_transacao_w,
				r_C00_w.nr_seq_conta,
				c01_w.nr_seq_conta_proc,
				c01_w.nr_seq_conta_mat,
				r_C00_w.nr_seq_conta_rec,
				c01_w.nr_seq_proc_rec,
				c01_w.nr_seq_mat_rec,
				nr_ordem_w,
				c01_w.cd_procedimento,
				c01_w.ie_origem_proced,
				c01_w.cd_material,
				c01_w.ie_situacao,
				c01_w.ie_tipo_despesa,
				r_C00_w.ie_tipo_guia,
				c01_w.ie_tipo_item,
				c01_w.dt_item,
				c01_w.ie_via_acesso,
				r_C00_w.nr_seq_protocolo,
				r_C00_w.cd_guia_ok,
				ds_item_w,
				c01_w.vl_glosa,
				c01_w.vl_recursado,
				c01_w.vl_acatado,
				r_C00_w.nr_seq_protocolo_rec) returning nr_id_transacao into nr_id_transacao_p;
				
			if	(c01_w.nr_seq_proc_rec is not null) then
				update	pls_rec_glosa_proc
				set	ie_status 	= decode(c01_w.ie_situacao,'N','4','A','3','P','2'),
					vl_acatado	= nvl(vl_acatado,0)
				where	nr_sequencia	= c01_w.nr_seq_proc_rec;
				
			elsif	(c01_w.nr_seq_mat_rec is not null) then
				update	pls_rec_glosa_mat
				set	ie_status 	= decode(c01_w.ie_situacao,'N','4','A','3','P','2'),
					vl_acatado	= nvl(vl_acatado,0)
				where	nr_sequencia	= c01_w.nr_seq_mat_rec;
			end if;
				
		end loop;
	end loop;
	
	commit;
end if;

end pls_gerar_w_analise_recurso;
/