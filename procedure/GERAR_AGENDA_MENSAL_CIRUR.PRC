create or replace 
procedure gerar_agenda_mensal_cirur (  nm_usuario_p			      varchar2,
                                       cd_estabelecimento_p		   number,
                                       cd_agenda_p			         number,
                                       dt_referencia_p			   date,
                                       cd_departamento_medico_p   number default null,
                                       ds_status_mes_p            out	varchar2) is
                                                         
ds_status_mes_w         varchar2(2000);
ds_status_janeiro_w     varchar2(2000);
ds_status_fevereiro_w   varchar2(2000);
ds_status_marco_w       varchar2(2000);
ds_status_abril_w       varchar2(2000);
ds_status_maio_w        varchar2(2000);
ds_status_junho_w       varchar2(2000);
ds_status_julho_w       varchar2(2000);
ds_status_agosto_w      varchar2(2000);   
ds_status_setembro_w    varchar2(2000);
ds_status_outubro_w     varchar2(2000);
ds_status_novembro_w    varchar2(2000);
ds_status_dezembro_w    varchar2(2000);
ds_status_periodo_w     varchar2(2000);  
ds_lista_total_livre_w  varchar2(2000);                                                     
		
begin

Obter_Horarios_Livres_calendar(cd_agenda_p,dt_referencia_p);
gerar_agenda_mensal( nm_usuario_p,
                     cd_estabelecimento_p,
                     cd_agenda_p,
                     dt_referencia_p,
                     null,
                     'M',
                     null,
                     null,
                     2,
                     cd_departamento_medico_p,
                     ds_status_mes_w,
                     ds_status_janeiro_w,
                     ds_status_fevereiro_w,
                     ds_status_marco_w,
                     ds_status_abril_w,
                     ds_status_maio_w,
                     ds_status_junho_w,
                     ds_status_julho_w,
                     ds_status_agosto_w,
                     ds_status_setembro_w,
                     ds_status_outubro_w,
                     ds_status_novembro_w,
                     ds_status_dezembro_w,
                     ds_status_periodo_w,
                     null,
                     null,
                     ds_lista_total_livre_w);
                     
ds_status_mes_p         := ds_status_mes_w;

end gerar_agenda_mensal_cirur;
/