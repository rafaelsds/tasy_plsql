create or replace
PROCEDURE marcar_registros_integrados(
			nm_usuario_p		VARCHAR2,
			cd_interface_p		NUMBER,
			ds_sql_p 			VARCHAR2,
			dt_emissao_de_p 		DATE,
			dt_emissao_ate_p		 DATE) IS

ds_sql_w		VARCHAR2(6000);

BEGIN

ds_sql_w := SUBSTR(ds_sql_p, 1, 4000);

 IF (cd_interface_p = 1308) THEN

UPDATE  titulo_pagar
SET     dt_integracao_externa =  TO_DATE(SYSDATE, 'dd/mm/yyyy')
WHERE  obter_se_contido(nr_titulo,ds_sql_w) = 'S';


ELSIF ((cd_interface_p = 1309) OR (cd_interface_p = 1491)) THEN

UPDATE  titulo_receber
SET     dt_integracao_externa = SYSDATE
WHERE  obter_se_contido(nr_titulo,ds_sql_w) = 'S';

ELSIF (cd_interface_p = 1326) THEN

UPDATE  titulo_pagar_baixa
SET     dt_integracao_externa = SYSDATE
WHERE  OBTER_SE_CONTIDO(nr_titulo,ds_sql_w) = 'S'
AND    TO_DATE( dt_baixa , 'dd/mm/yyyy') BETWEEN TO_DATE(dt_emissao_de_p, 'dd/mm/yyyy') AND TO_DATE(dt_emissao_ate_p,'dd/mm/yyyy');

ELSIF (cd_interface_p = 1327) THEN

UPDATE  titulo_receber_liq
SET     dt_integracao_externa = SYSDATE
WHERE  OBTER_SE_CONTIDO(nr_titulo,ds_sql_w) = 'S'
AND    TO_DATE( dt_recebimento , 'dd/mm/yyyy')  BETWEEN  TO_DATE(dt_emissao_de_p, 'dd/mm/yyyy') AND TO_DATE(dt_emissao_ate_p,'dd/mm/yyyy');

ELSIF ((cd_interface_p = 1333) OR (cd_interface_p = 1334)) THEN

UPDATE  pessoa_fisica
SET     dt_integracao_externa = SYSDATE
WHERE   OBTER_SE_CONTIDO(cd_pessoa_fisica,ds_sql_w) = 'S';

UPDATE  pessoa_juridica
SET     dt_integracao_externa = SYSDATE
WHERE   OBTER_SE_CONTIDO(cd_cgc,ds_sql_w) = 'S';

ELSIF  (cd_interface_p = 2254) THEN

UPDATE  titulo_pagar SET     dt_integracao_externa = SYSDATE WHERE  OBTER_SE_CONTIDO(nr_titulo,ds_sql_w) = 'S';

ELSIF (cd_interface_p = 1667 OR cd_interface_p = 2755) THEN

UPDATE  titulo_pagar
SET     dt_integracao_externa = SYSDATE
WHERE  OBTER_SE_CONTIDO(nr_titulo,ds_sql_w) = 'S';

END IF;

COMMIT;

END marcar_registros_integrados;
/
