create or replace
procedure desfazer_encerramento_contrato(
			nr_sequencia_p		number,
			nm_usuario_p		varchar2) is

nr_seq_historico_w			number(10);
ds_historico_w			varchar2(4000);
ds_titulo_w			varchar2(255);

begin

update	contrato
set	ie_estagio 	= 'C',
	dt_encerramento	= null,
	nm_usuario_encer	= null,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	ie_situacao	= 'A'
where	nr_sequencia	= nr_sequencia_p;

ds_titulo_w :=	wheb_mensagem_pck.get_Texto(311434);/* 'Desfeito encerramento do contrato';*/

ds_historico_w :=	wheb_mensagem_pck.get_Texto(311435, 'NM_USUARIO_P='|| NM_USUARIO_P); /*'O usu�rio ' || nm_usuario_p || ' desfez o encerramento do contrato.';*/


select	contrato_historico_seq.nextval
into	nr_seq_historico_w
from	dual;

insert into contrato_historico(
	nr_sequencia,
	nr_seq_contrato,
	dt_historico,
	ds_historico,
	dt_atualizacao,
	nm_usuario,
	ds_titulo,
	dt_liberacao,
	nm_usuario_lib,
	ie_efetivado) values (
		nr_seq_historico_w,
		nr_sequencia_p,
		sysdate,
		ds_historico_w,
		sysdate,
		nm_usuario_p,
		ds_titulo_w,
		sysdate,
		nm_usuario_p,
		'N');

commit;

end desfazer_encerramento_contrato;
/