create or replace
PROCEDURE Obter_Horarios_Livres(
				cd_estabelecimento_p	Number,
				cd_agenda_p     		number,
				dt_agenda_p			Date,
				dt_final_p			date,
				qt_minutos_p		Number,
				hr_inicial_p		Varchar2,
				hr_final_p			Varchar2,
				ds_restricao_p		Varchar2,
				nm_usuario_p		Varchar2,
				ie_grava_Livre_p		Varchar2,
				ds_horarios_p	out	Varchar2) is

ie_feriado_w			Varchar2(0001)	:= 'N';
ie_dia_semana_w			Number(001,0);
hr_inicial_w			Date;
hr_final_w			Date;
hr_inicial_intervalo_w		Date;
hr_final_intervalo_w		Date;
hr_atual_w			Date;
nr_minuto_intervalo_w		agenda_horario.nr_minuto_intervalo%type;
ie_existe_agenda_w		Varchar2(0001);
ie_bloqueio_w			Varchar2(0001);
ie_bloqueio_hora_w		Varchar2(0001);
ie_bloqueio_dia_w		Varchar2(0001);
qt_horario_w			Number(0004,0)    := 0;
nr_sequencia_w			Number(0010,0)    := 0;
dt_agenda_w			Date;
ds_horarios_w			Varchar2(0255)	:= '';
Virgula_w			Varchar2(1)		:= '';
qt_min_duracao_w		Number(10,0)	:= 0;
hr_inicio_w			Date;
hr_fim_w			Date;
hr_inicial_par_w		Date;
hr_final_par_w			Date;
cd_turno_w			Number(01,0)	:= 0;
nr_seq_esp_w			Number(10,0)	:= 0;
cd_medico_w			varchar2(10);
nr_seq_agenda_medico_w		number(10,0);
cd_medico_exec_w		varchar2(10);

qt_min_minimo_w			number(10,0);
qt_turno_w			number(10,0);
qt_intervalo_w			number(10,0);
HR_NADA_W			date;
ie_sobra_horario_w		varchar2(01);
ie_bloqueio_dia_hora_w		varchar2(01);
ds_observacao_horario_w		varchar2(255);
ie_gerar_obs_horario_w		varchar2(01);
ie_autorizacao_w		varchar2(05);
ie_excluir_livres_w		varchar2(01) 	:= 'N';
qt_agenda_w			number(10,0);
HR_QUEBRA_TURNO_W		varchar2(05);
qt_min_QUEBRA_TURNO_W		varchar2(05);
nr_seq_sala_w			number(10,0);
ie_anestesista_w		varchar2(01)	:= 'N';
ie_gerar_forcado_w		varchar2(01)	:= 'N';
ie_gerar_autorizacao_livres_w	varchar2(01)	:= 'S';

dt_forcado_w			date;
nr_min_forcado_w		number(10,0);
cd_agenda_forcado_w		number(10,0);

ie_tipo_atendimento_w		number(3,0); /* Rafael em 16/10/2006 OS 42479 */
ie_horario_adicional_w		varchar2(1); /* Rafael em 09/11/2006 OS 43852 */
NR_SEQ_CLASSIF_AGENDA_w		number(10,0);
cd_funcao_w				number(10,0);
ie_manter_livres_w		varchar2(1);
qt_gera_horario_w		number(10);
qt_dia_filtro_ini_w		number(1);
qt_dia_filtro_fim_w		number(1);

CURSOR C01 IS
	select 	to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
			to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
		 	to_char(hr_final,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
		 	to_char(nvl(hr_Inicial_Intervalo,hr_final),'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
		 	to_char(nvl(hr_final_Intervalo,hr_final),'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			decode(nvl(qt_minutos_p,0),0, nr_minuto_intervalo, qt_minutos_p),
			a.cd_medico,
			a.nr_seq_medico_exec,
			substr(a.ds_observacao,1,60),
			a.nr_seq_sala,
			a.NR_SEQ_CLASSIF_AGENDA
	from 		agenda_Horario a
	where 	cd_agenda     	= cd_agenda_p
	  and	((dt_dia_semana = ie_dia_semana_w) or ((dt_dia_semana = 9) and (ie_dia_Semana_w not in  (qt_dia_filtro_ini_w,qt_dia_filtro_fim_w))))
	  and	((nr_seq_esp_w 	= 0) or (ie_horario_adicional_w = 'S'))
	  and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(dt_agenda_p)))
	  and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(dt_agenda_p)))
	  and	to_char(hr_inicial,'hh24:mi:ss') < to_char(hr_final,'hh24:mi:ss')
	  and		nvl(nr_minuto_intervalo,0) > 0
	  and	((ie_feriado_w <> 'S') or (obter_se_agenda_feriado(cd_agenda_p) = 'S'))
	  and	(obter_se_gerar_turno_agecons(dt_inicio_vigencia,ie_frequencia,dt_agenda_p) = 'S')
	  and 	((Obter_Semana_Dia_Agecons(dt_agenda_p,ie_semana) = nvl(ie_semana,0)) or (nvl(ie_semana,0) = 0))
	union
	select 	to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
			to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
		 	to_char(hr_final,'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
		 	to_char(nvl(hr_Inicial_Intervalo,hr_final),'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
		 	to_char(nvl(hr_final_Intervalo,hr_final),'hh24:mi'),'dd/mm/yyyy hh24:mi'),
			nr_minuto_intervalo,
			a.cd_medico, a.nr_seq_medico_exec,
			a.ds_observacao,
			a.nr_seq_sala,
			a.NR_SEQ_CLASSIF_AGENDA
	from 		agenda_Horario_esp a
	where 	dt_agenda	= trunc(dt_agenda_p,'dd')
	  and 	hr_inicial < hr_final
	  and	nvl(nr_minuto_intervalo,0) > 0
	  and   cd_agenda	= cd_agenda_p
	order by 1,2,5;

CURSOR C02 IS
	select	hr_inicio,
		(hr_inicio + (nr_minuto_duracao / 1440)) dt_final
	from 	agenda_paciente
	where 	cd_agenda	= cd_agenda_p
        and	hr_inicio between trunc(dt_agenda_p,'dd') and (trunc(dt_agenda_p,'dd') + (86399/86400))
	and	hr_inicio >= sysdate
	and	ie_sobra_horario_w	= 'S'
	order by 1;

cursor	c03 is
	select	a.dt_agenda,
		a.nr_minuto_duracao,
		a.cd_agenda
	from	agenda_livre_forcado a
	where	a.cd_agenda	= cd_agenda_p
	and	a.dt_agenda	between trunc(dt_agenda_p,'dd') and (trunc(dt_agenda_p,'dd') + (86399/86400))
	and	a.dt_agenda > trunc(sysdate - 1, 'dd')
	and	not exists
		(select	1
		from	agenda_paciente x
		where	x.cd_agenda		= a.cd_agenda
		and	x.hr_inicio		= a.dt_agenda
		and	x.ie_status_agenda	in ('LF', 'N', 'E'))
	group by a.dt_agenda,
		 a.nr_minuto_duracao,
		 a.cd_agenda;
BEGIN

qt_dia_filtro_ini_w	:= 7;
qt_dia_filtro_fim_w	:= 1;

select	nvl(max(ie_forma_excluir_exame),'N')
into	ie_manter_livres_w
from	parametro_agenda
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_manter_livres_w	in ('S','E')) then
	if	(trunc(dt_Agenda_p)	< trunc(sysdate)) then
		qt_gera_horario_w	:= 1;
	else
		begin
		select 	1
		into 	qt_gera_horario_w
		from 	agenda_controle_horario
		where 	cd_agenda = cd_agenda_p
		and		dt_agenda = trunc(dt_Agenda_p)
		and 	rownum	= 1;
		exception
		when others then
			qt_gera_horario_w	:= 0;
		end;
	end if;
	if	(qt_gera_horario_w	> 0) and (ie_manter_livres_w = 'E') then
		delete	from	agenda_paciente a
		where	a.cd_agenda = cd_agenda_p
		and		a.hr_inicio < sysdate
		and		a.dt_Agenda between trunc(dt_agenda_p,'dd') and (trunc(dt_agenda_p,'dd') + (86399/86400))
		and		((a.ie_status_agenda = 'L')
		or		((a.ie_status_agenda = 'B')
		and		(a.dt_bloqueio is null)));

		commit;
	end if;
else
	qt_gera_horario_w	:= 0;
end if;

if	(qt_gera_horario_w	= 0) then
	if	(ie_manter_livres_w	in ('S','E')) and
		(ie_grava_Livre_p	= 'S') then
		begin
		insert into agenda_controle_horario
						(nr_sequencia,
						cd_agenda,
						dt_agenda,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec)
				values
						(agenda_controle_horario_seq.nextval,
						cd_agenda_p,
						trunc(dt_agenda_p),
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p);
		commit;
		exception
		when others then
			qt_gera_horario_w	:= 1;
		end;
	end if;
	if	(qt_gera_horario_w	= 0) then
		select	nvl(max(obter_valor_param_usuario(39, 60, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'N')
		into	ie_excluir_livres_w
		from	dual;

		select	nvl(max(obter_valor_param_usuario(39, 79, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'N')
		into	ie_gerar_forcado_w
		from	dual;


		select	nvl(max(ie_gerar_sobra_horario), 'N'),
			nvl(max(HR_QUEBRA_TURNO), '12'),
			nvl(max(qt_min_quebra_turno), '00'),
			nvl(max(ie_anestesista), 'N'),
			nvl(max(ie_tipo_atendimento), 0)
		into	ie_sobra_horario_w,
			HR_QUEBRA_TURNO_W,
			qt_min_QUEBRA_TURNO_W,
			ie_anestesista_w,
			ie_tipo_atendimento_w
		from	agenda
		where	cd_agenda	= cd_agenda_p;


		select		nvl(max(nr_sequencia),0),
				nvl(max(ie_horario_adicional),'N')
		into		nr_seq_esp_w,
				ie_horario_adicional_w
		from		agenda_horario_esp
		where		cd_agenda	= cd_agenda_p
		  and		dt_agenda	= trunc(dt_agenda_p,'dd');

		if	(hr_inicial_p is null) then
			hr_inicial_par_w		:= trunc(dt_agenda_p,'dd');
		else
			hr_inicial_par_w		:= to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
							hr_inicial_p,'dd/mm/yyyy hh24:mi');
		end if;
		if	(hr_final_p is null) then
			hr_final_par_w			:= trunc(dt_agenda_p,'dd') + 86399/86400;
		else
			hr_final_par_w			:= to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
							hr_final_p,'dd/mm/yyyy hh24:mi');
		end if;
		dt_agenda_w				:= trunc(dt_agenda_p,'dd');
		begin
		select 'S'
		into	ie_feriado_w
		from feriado a, agenda b
		where a.cd_estabelecimento 	= cd_estabelecimento_p
		  and a.dt_feriado		= dt_agenda_w
		  and b.cd_agenda			= cd_agenda_p
		  and ie_feriado			= 'N';
		exception
			when others then
				ie_feriado_w := 'N';
		end;

		select	nvl(max(Obter_Valor_Param_Usuario(39, 56, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'N')
		into	ie_gerar_obs_horario_w
		from	dual;


		select	nvl(max(Obter_Valor_Param_Usuario(39, 112, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'S')
		into	ie_gerar_autorizacao_livres_w
		from	dual;

		select	nvl(max(Obter_Valor_Param_Usuario(39, 57, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'L')
		into	ie_autorizacao_w
		from	dual;

		select 	obter_cod_dia_semana(dt_agenda_w)
		into 	ie_dia_semana_w
		from 	dual;
		

		cd_funcao_w	:= obter_funcao_ativa;

		if	(ie_manter_livres_w	= 'N') then
			if	((ie_excluir_livres_w = 'N') or
				(hr_inicial_p		is not null)) and
				(cd_funcao_w <> 820) then
				begin

				delete /*+ INDEX(A AGEPACI_UK) */
				from agenda_paciente a
				where cd_agenda 	= cd_agenda_p
				  and	dt_agenda 	= trunc(dt_agenda_p,'dd')
				  and ie_status_agenda		= 'L';

				commit;

				delete /*+ INDEX(A AGEPACI_UK) */
				from agenda_paciente a
				where 	cd_agenda 	= cd_agenda_p
				and 	dt_agenda 	<= sysdate
				and 	ie_status_agenda	= 'L';

				commit;

				end;
			else
				begin

				delete /*+ INDEX(A AGEPACI_UK) */
				from agenda_paciente a
				where cd_agenda 	= cd_agenda_p
				  and	dt_agenda 	= trunc(dt_agenda_p,'dd')
				  and ie_status_agenda		= 'L'
				  and hr_inicio >= sysdate;

				commit;

				end;
			end if;
		else
			if	(ie_grava_livre_p = 'S') then
				if (ie_manter_livres_w in ('N', 'E')) then
					delete	from	agenda_paciente a
					where	a.cd_agenda = cd_agenda_p
					and		a.dt_agenda between trunc(dt_agenda_p,'dd') and (trunc(dt_agenda_p,'dd') + (86399/86400))
					and		((a.ie_status_agenda = 'L')
					or		((a.ie_status_agenda = 'B')
					and		(a.dt_bloqueio is null)));
				elsif (ie_manter_livres_w = 'S') then
					delete	from	agenda_paciente a
					where	a.cd_agenda = cd_agenda_p
					and		a.dt_agenda between trunc(dt_agenda_p,'dd') and (trunc(dt_agenda_p,'dd') + (86399/86400))
					and 	a.hr_inicio > sysdate
					and		((a.ie_status_agenda = 'L')
					or		((a.ie_status_agenda = 'B')
					and		(a.dt_bloqueio is null)));					
				end if;
				
				commit;
			end if;
		end if;

		OPEN C01;
		LOOP
		FETCH C01 into
			hr_inicial_w,
			hr_final_w,
			hr_inicial_Intervalo_w,
			hr_final_Intervalo_w,
			nr_minuto_Intervalo_w,
			cd_medico_w,
			nr_seq_agenda_medico_w,
			ds_observacao_horario_w,
			nr_seq_sala_w,
			NR_SEQ_CLASSIF_AGENDA_w;
		exit 	when c01%notfound;
		     	begin
			if	(NR_SEQ_CLASSIF_AGENDA_w = 0) then
				NR_SEQ_CLASSIF_AGENDA_w := null;
			end if;
			hr_atual_w			:= hr_inicial_w;
			while 	(hr_atual_w	< hr_final_w) and
				(hr_atual_w	< hr_final_par_w)  loop
				BEGIN
				begin
				select	/*+ INDEX(A AGEPACI_UK) */
						nvl(max(nr_minuto_duracao),0)
				into		qt_min_duracao_w
				from 	agenda_Paciente a
				where cd_agenda			= cd_agenda_p
				  and ie_status_agenda		<> 'C'
				  and	dt_agenda			= trunc(hr_atual_w,'dd')

				  and to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
					to_char(hr_inicio,'hh24:mi'),'dd/mm/yyyy hh24:mi') <
					(hr_atual_w + (nr_minuto_intervalo_w + 1 / 1440))
			 	  and to_date(to_char(dt_agenda_w,'dd/mm/yyyy') || ' ' ||
					to_char(hr_inicio + ((nr_minuto_duracao -1) / 1440),'hh24:mi'),
						'dd/mm/yyyy hh24:mi') >=	hr_atual_w;
				exception
					when others then
						qt_min_duracao_w		:= 0;
				end;

				begin
				/* bloqueio por per�odo */
				select 'S'
				into	ie_bloqueio_w
				from	agenda_bloqueio
				where	cd_agenda	= cd_agenda_p
				and	trunc(hr_atual_w) between trunc(dt_inicial) and fim_dia(dt_final)
				and	ie_dia_semana is null
				and	HR_INICIO_BLOQUEIO is null
				and	HR_FINAL_BLOQUEIO is null;
				exception
					when others then
						ie_bloqueio_w := 'N';
				end;


				begin
				/* bloqueio por hor�rio */
				select 'S'
				into	ie_bloqueio_hora_w
				from	agenda_bloqueio
				where	cd_agenda	= cd_agenda_p
				and	trunc(hr_atual_w) between trunc(dt_inicial) and fim_dia(dt_final)
				and	hr_atual_w between to_date(to_char(hr_atual_w,'dd/mm/yyyy') ||' '|| to_char(hr_inicio_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')

		and to_date(to_char(hr_atual_w,'dd/mm/yyyy') ||' '|| to_char(hr_final_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
				and	ie_dia_semana is null
				and	HR_INICIO_BLOQUEIO is not null
				and	HR_FINAL_BLOQUEIO is not null
				and	hr_inicio_bloqueio < hr_final_bloqueio;
				exception
					when others then
						ie_bloqueio_hora_w := 'N';
				end;

				begin
				/* bloqueio por dia */
				select 'S'
				into	ie_bloqueio_dia_w
				from	agenda_bloqueio
				where	cd_agenda	= cd_agenda_p
				and	trunc(hr_atual_w) between trunc(dt_inicial) and fim_dia(dt_final)
				and	((ie_dia_semana = ie_dia_semana_w) or (ie_dia_semana = 9))
				and	ie_dia_semana is not null
				and	HR_INICIO_BLOQUEIO is null
				and	HR_FINAL_BLOQUEIO is null;
				exception
					when others then
						ie_bloqueio_dia_w := 'N';
				end;



				begin
				/* bloqueio dia e hora */
				select 'S'
				into	ie_bloqueio_dia_hora_w
				from	agenda_bloqueio
				where	cd_agenda	= cd_agenda_p
				and	trunc(hr_atual_w) between trunc(dt_inicial) and fim_dia(dt_final)
				and	hr_atual_w between to_date(to_char(hr_atual_w,'dd/mm/yyyy') ||' '|| to_char(hr_inicio_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')

		and to_date(to_char(hr_atual_w,'dd/mm/yyyy') ||' '|| to_char(hr_final_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
				and	ie_dia_semana is not null
				and	((ie_dia_semana = ie_dia_semana_w) or (ie_dia_semana = 9))
				and	HR_INICIO_BLOQUEIO is not null
				and	HR_FINAL_BLOQUEIO is not null
				and	hr_inicio_bloqueio < hr_final_bloqueio;
				exception
					when others then
						ie_bloqueio_dia_hora_w := 'N';
				end;


				if	(qt_min_duracao_w = 0) and
					(hr_atual_w >= sysdate) and
					(hr_atual_w >= hr_inicial_par_w) and
					(ie_bloqueio_w = 'N') and
					(ie_bloqueio_hora_w = 'N') and
					(ie_bloqueio_dia_w = 'N') and
					(ie_bloqueio_dia_hora_w = 'N') then
					begin
					if	(nvl(length(ds_horarios_w),0) < 249) then
						ds_horarios_w	:= ds_horarios_w || virgula_w ||
								to_char(hr_atual_w,'hh24:mi');
					end if;
					virgula_w		:= ',';
					if	(ie_grava_livre_p = 'S') then
						begin
						cd_turno_w		:= 0;

						if	(To_Number(to_char(hr_atual_w,'hh24')) > somente_numero(HR_QUEBRA_TURNO_W)) or
							((To_Number(to_char(hr_atual_w,'hh24')) = somente_numero(HR_QUEBRA_TURNO_W)) and
							(To_Number(to_char(hr_atual_w,'mi')) >= somente_numero(qt_min_QUEBRA_TURNO_W))) then
							cd_turno_w	:= 1;
						end if;
						select agenda_paciente_seq.nextval
						into nr_sequencia_w
						from dual;

						select	max(cd_medico)
						into	cd_medico_exec_w
						from	agenda_medico
						where	nr_sequencia	= nr_seq_agenda_medico_w;

						select	count(*)
						into	qt_agenda_w
						from	agenda_paciente
						where	cd_agenda		= cd_agenda_p
						and	dt_agenda		= trunc(dt_agenda_p,'dd')
						and	hr_inicio		= hr_atual_w
						and	ie_status_agenda	= 'L';

						if	(qt_agenda_w = 0) then
							begin

							insert into agenda_paciente
								(cd_agenda, dt_agenda, hr_inicio,
								nr_minuto_duracao,nm_usuario, dt_atualizacao,
								ie_status_agenda,	ie_ortese_protese, ie_cdi,
								ie_uti,ie_banco_sangue,	ie_serv_especial,
								ie_leito, ie_anestesia, nr_sequencia, cd_turno,
								ie_equipamento, ie_autorizacao, ie_video, ie_uc,
								cd_medico, cd_medico_exec, nm_paciente, ie_biopsia, ie_congelacao, nr_seq_sala,
								IE_CONSULTA_ANESTESICA, IE_PRE_INTERNACAO, ie_tipo_atendimento, ie_arco_c, NR_SEQ_CLASSIF_AGENDA)
							values(
								cd_agenda_p, trunc(dt_agenda_p,'dd'), hr_atual_w,
								nr_minuto_Intervalo_w, nm_usuario_p, sysdate,
								'L', 'N', 'N', 'N', 'N', 'N', 'S', ie_anestesista_w,
								nr_sequencia_w, cd_turno_w, 'N', decode(ie_gerar_autorizacao_livres_w, 'S', ie_autorizacao_w, null), 'N',

		'N', cd_medico_w, cd_medico_exec_w, substr(decode(ie_gerar_obs_horario_w, 'S', ds_observacao_horario_w, null),1,60),
								'N', 'N', nr_seq_sala_w, 'N', 'N', decode(ie_tipo_atendimento_w, 0, null, ie_tipo_atendimento_w),'N',
								NR_SEQ_CLASSIF_AGENDA_w);
							end;
						end if;
						end;
						end if;
					end;
				end if;
				hr_atual_w		:= hr_atual_w + (nr_minuto_intervalo_w / 1440);
				qt_horario_w 	:= qt_horario_w + 1;
				if	(hr_atual_w >= hr_inicial_intervalo_w) and
					(hr_atual_w < hr_final_intervalo_w) then
					hr_atual_w	:= hr_final_intervalo_w;
				end if;
				if	(qt_horario_w > 201 ) then
					hr_atual_w	:= hr_final_w + 1;
				end if;
				END;
			END LOOP;
			end;
		END LOOP;
		CLOSE C01;

		hr_nada_w	:= trunc(dt_agenda_p,'year');
		hr_fim_w	:= hr_nada_w;
		qt_min_minimo_w	:= 0;

		OPEN C02;
		LOOP
		FETCH C02 into
			hr_inicial_w,
			hr_final_w;
		exit 	when c02%notfound;
			if	(hr_fim_w <> hr_nada_w) and
				(hr_inicial_w > hr_fim_w) then
				BEGIN

				nr_minuto_intervalo_w	:= (hr_inicial_w - hr_fim_w) * 1440;
				hr_atual_w		:= hr_fim_w;
				if	(nr_minuto_intervalo_w >= qt_min_minimo_w) then
					begin
					select	count(*)
					into	qt_turno_w
					from 	agenda_horario
					where 	cd_agenda     	= cd_agenda_p
			  		  and 	dt_dia_semana	= ie_dia_Semana_w
					  and	hr_atual_w	between
								to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
								to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi')
								and
								to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
								to_char(hr_final - 1/1440,'hh24:mi'),'dd/mm/yyyy hh24:mi');

					select	count(*)
					into	qt_intervalo_w
					from 	agenda_horario
					where 	cd_agenda     	= cd_agenda_p
			  		  and 	dt_dia_semana	= ie_dia_Semana_w
					  and   to_char(hr_inicial_intervalo,'hh24') <> '00'
					  and	hr_atual_w	between
								to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
								to_char(hr_inicial_intervalo,'hh24:mi'),'dd/mm/yyyy hh24:mi')
								and
								to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
								to_char(hr_final_intervalo - 1/1440,'hh24:mi'),'dd/mm/yyyy hh24:mi');


					if	(qt_turno_w	= 0) then
						begin

						select	count(*)
						into	qt_turno_w
						from 	agenda_horario
						where 	cd_agenda     	= cd_agenda_p
				  		  and 	dt_dia_semana	= 9
						  and	hr_atual_w	between
									to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
									to_char(hr_inicial,'hh24:mi'),'dd/mm/yyyy hh24:mi')
									and
									to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
									to_char(hr_final  - 1/1440,'hh24:mi'),'dd/mm/yyyy hh24:mi');

						select	count(*)
						into	qt_intervalo_w
						from 	agenda_horario
						where 	cd_agenda     	= cd_agenda_p
				  		  and 	dt_dia_semana	= 9
						  and   to_char(hr_inicial_intervalo,'hh24') <> '00'
						  and	hr_atual_w	between
									to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
									to_char(hr_inicial_intervalo,'hh24:mi'),'dd/mm/yyyy hh24:mi')
									and
									to_date(to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' ||
									to_char(hr_final_intervalo - 1/1440,'hh24:mi'),'dd/mm/yyyy hh24:mi');


						end;
					end if;

					begin
					/* bloqueio por dia */
					select 'S'
					into	ie_bloqueio_dia_w
					from	agenda_bloqueio
					where	cd_agenda	= cd_agenda_p
					and	trunc(hr_atual_w) between dt_inicial and dt_final
					and	((ie_dia_semana = ie_dia_semana_w) or (ie_dia_semana = 9))
					and	ie_dia_semana is not null
					and	HR_INICIO_BLOQUEIO is null
					and	HR_FINAL_BLOQUEIO is null;
					exception
						when others then
							ie_bloqueio_dia_w := 'N';
					end;



					begin
					/* bloqueio dia e hora */
					select 'S'
					into	ie_bloqueio_dia_hora_w
					from	agenda_bloqueio
					where	cd_agenda	= cd_agenda_p
					and	trunc(hr_atual_w) between dt_inicial and dt_final
					and	hr_atual_w between to_date(to_char(hr_atual_w,'dd/mm/yyyy') ||' '|| to_char(hr_inicio_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy

		hh24:mi:ss') and to_date(to_char(hr_atual_w,'dd/mm/yyyy') ||' '|| to_char(hr_final_bloqueio,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss')
					and	ie_dia_semana is not null
					and	((ie_dia_semana = ie_dia_semana_w) or (ie_dia_semana = 9))
					and	HR_INICIO_BLOQUEIO is not null
					and	HR_FINAL_BLOQUEIO is not null
					and	hr_inicio_bloqueio < hr_final_bloqueio;
					exception
						when others then
							ie_bloqueio_dia_hora_w := 'N';
					end;


					if	(ie_bloqueio_dia_w = 'N') and
						(ie_bloqueio_dia_hora_w = 'N') and
						(qt_turno_w > 0) and
						(qt_intervalo_w = 0) then
						begin
						cd_turno_w		:= 0;

						if	(To_Number(to_char(hr_atual_w,'hh24')) > somente_numero(HR_QUEBRA_TURNO_W)) or
							((To_Number(to_char(hr_atual_w,'hh24')) = somente_numero(HR_QUEBRA_TURNO_W)) and
							(To_Number(to_char(hr_atual_w,'mi')) >= somente_numero(qt_min_QUEBRA_TURNO_W))) then
							cd_turno_w	:= 1;
						end if;

						select 	agenda_paciente_seq.nextval
						into 	nr_sequencia_w
						from 	dual;

						insert into agenda_paciente
							(cd_agenda, dt_agenda, hr_inicio,
							nr_minuto_duracao,nm_usuario, dt_atualizacao,
							ie_status_agenda,	ie_ortese_protese, ie_cdi,
							ie_uti,ie_banco_sangue,	ie_serv_especial,
							ie_leito, ie_anestesia, nr_sequencia, cd_turno,
							ie_equipamento, ie_autorizacao, ie_video, ie_uc, cd_medico, cd_medico_exec,
							nm_paciente, ie_biopsia, ie_congelacao, IE_CONSULTA_ANESTESICA,
							IE_PRE_INTERNACAO, ie_tipo_atendimento, ie_arco_c,NR_SEQ_CLASSIF_AGENDA)
						values(
							cd_agenda_p, trunc(dt_agenda_p,'dd'), hr_atual_w,
							nr_minuto_Intervalo_w, nm_usuario_p, sysdate,
							'L', 'N', 'N', 'N', 'N', 'N', 'S', ie_anestesista_w,
							nr_sequencia_w, cd_turno_w, 'N', decode(ie_gerar_autorizacao_livres_w, 'S', ie_autorizacao_w, null),
		'N', 'N', cd_medico_w, cd_medico_exec_w, 	substr(decode(ie_gerar_obs_horario_w, 'S', ds_observacao_horario_w, null),1,60), 'N', 'N', 'N', 'N',
		decode(ie_tipo_atendimento_w, 0, null, ie_tipo_atendimento_w), 'N',NR_SEQ_CLASSIF_AGENDA_w);

						end;
					end if;
					end;
				end if;
				END;
			end if;
			if	(hr_final_w > hr_fim_w) then
				hr_fim_w	:= hr_final_w;
			end if;
		END LOOP;
		CLOSE C02;

		if	(ie_gerar_forcado_w = 'S') then
			begin

			open	c03;
			loop
			fetch	c03 into
				dt_forcado_w,
				nr_min_forcado_w,
				cd_agenda_forcado_w;
			exit 	when c03%notfound;
				begin

				select	count(*)
				into	qt_horario_w
				from	agenda_paciente
				where	cd_agenda		= cd_agenda_forcado_w
				and	dt_agenda		= trunc(dt_forcado_w, 'dd')
				and	hr_inicio		= dt_forcado_w
				and	ie_status_agenda	in ('LF', 'N', 'E');


				if	(qt_horario_w = 0) then
					begin

					select	agenda_paciente_seq.nextval
					into	nr_sequencia_w
					from	dual;

					insert	into agenda_paciente
						(cd_agenda,
						dt_agenda,
						hr_inicio,
						nr_minuto_duracao,
						nm_usuario,
						dt_atualizacao,
						nr_sequencia,
						ie_equipamento,
						ie_status_agenda,
						nm_paciente,
						IE_CONSULTA_ANESTESICA,IE_PRE_INTERNACAO, ie_tipo_atendimento,
						NR_SEQ_CLASSIF_AGENDA)
					values
						(cd_agenda_forcado_w,
						trunc(dt_forcado_w, 'dd'),
						dt_forcado_w,
						nr_min_forcado_w,
						nm_usuario_p,
						sysdate,
						nr_sequencia_w,
						'N', 'LF', null, 'N', 'N', decode(ie_tipo_atendimento_w, 0, null, ie_tipo_atendimento_w),
						NR_SEQ_CLASSIF_AGENDA_w);
					delete	from agenda_livre_forcado
					where	dt_agenda	= dt_forcado_w
					and	cd_agenda	= cd_agenda_forcado_w;

					commit;
					end;
				end if;

				end;
			end loop;
			close c03;
			end;
		end if;
	end if;
end if;

commit;

ds_horarios_p	:= ds_horarios_w;
END Obter_Horarios_Livres;
/
