create or replace
PROCEDURE valida_escala_loinc(scale_Type_p varchar2,                              
                              nm_usuario_p varchar2,
                              cd_versao_release_p varchar2,
                              cd_versao_ult_alt_p varchar2,
                              ie_tipo_importacao_p varchar2,
                              nr_seq_loinc_dados_p number,
                              nr_seq_escala_p out number) is
nr_seq_escala_w number(10);
mensagem_w varchar2(255);
BEGIN
  select max(nr_sequencia)
    into nr_seq_escala_w
    from lab_loinc_escala
   where cd_escala = scale_Type_p;
    
  if (nr_seq_escala_w is null) then
    select lab_loinc_escala_seq.nextval 
      into nr_seq_escala_w
      from dual;    
       
    insert into lab_loinc_escala (nr_sequencia,
                                  cd_escala,
                                  ie_situacao,
                                  dt_atualizacao,
                                  dt_atualizacao_nrec,
                                  nm_usuario,
                                  nm_usuario_nrec)
                          values (nr_seq_escala_w,
                                  scale_Type_p,
                                  'A',
                                  sysdate,
                                  sysdate,
                                  nm_usuario_p,
                                  nm_usuario_p);
                         
    mensagem_w := wheb_mensagem_pck.get_texto(1028601, 'DS_TIPO_CADASTRO=' || wheb_mensagem_pck.get_texto(1028612) || ';CD_CODIGO=' || scale_Type_p);
    GRAVA_LOG_LOINC_IMP(mensagem_w, nm_usuario_p, cd_versao_release_p, cd_versao_ult_alt_p, ie_tipo_importacao_p, nr_seq_loinc_dados_p);
  end if;
  commit;
  nr_seq_escala_p := nr_seq_escala_w;
END valida_escala_loinc;
/