create or replace
procedure pls_obter_centro_custo
		(	ie_tipo_centro_custo_p		Varchar2,
			nr_seq_plano_p			Number,
			cd_estabelecimento_p		Number,
			cd_municipio_ibge_p		Varchar2,
			ie_tipo_contratacao_p		Varchar2,
			ie_regulamentacao_p		Varchar2,
			nr_seq_segurado_p		Number,
			ie_tipo_item_p			Varchar2,
			cd_centro_custo_p	out	Number,
			nr_seq_regra_p		out	Number,
			nr_seq_tipo_lanc_p		number default null,
			nr_seq_bonificacao_p		number default null,
			nr_seq_sca_p			number default null,
			nr_seq_prestador_pag_p		number default null,
			nr_seq_prestador_exec_p		number default null,
			nr_seq_prestador_atend_p	number default null,
			nr_seq_prestador_solic_p	number default null,
			nr_seq_evento_p			number default null) is

cd_centro_custo_w		pls_regra_centro_custo.cd_centro_custo%type;
nr_sequencia_w			pls_regra_centro_custo.nr_sequencia%type;
ie_centro_custo_w		pls_regra_centro_custo.ie_centro_custo%type;
ie_tipo_vinculo_operadora_w	pls_segurado.ie_tipo_vinculo_operadora%type;
nr_seq_contrato_w		pls_segurado.nr_seq_contrato%type;
ie_tipo_segurado_w		pls_segurado.ie_tipo_segurado%type;
ie_status_prov_pagto_w		pls_parametro_contabil.ie_status_prov_pagto%type;

Cursor C01 is
	select	cd_centro_custo,
		ie_centro_custo,
		nr_sequencia
	from	pls_regra_centro_custo a
	where	a.ie_tipo_centro_custo		= ie_tipo_centro_custo_p
	and	a.cd_estabelecimento		= cd_estabelecimento_p
	and	a.ie_situacao			= 'A'	
	and	((a.nr_seq_plano 		= nr_seq_plano_p) or (a.nr_seq_plano is null))
	and	((a.nr_seq_plano_sca 		= nr_seq_sca_p) or (a.nr_seq_plano_sca is null))
	and	((a.cd_municipio_ibge		= cd_municipio_ibge_p) or (a.cd_municipio_ibge is null))
	and	((a.ie_tipo_contratacao 	= ie_tipo_contratacao_p) or (a.ie_tipo_contratacao is null))
	and	((a.ie_regulamentacao		= ie_regulamentacao_p) or (a.ie_regulamentacao is null))
	and	((a.ie_tipo_vinculo_operadora 	= ie_tipo_vinculo_operadora_w) or (a.ie_tipo_vinculo_operadora is null))
	and	((a.ie_tipo_item 		= ie_tipo_item_p) or (a.ie_tipo_item is null))
	and	((a.nr_seq_tipo_lanc 		= nr_seq_tipo_lanc_p) or (a.nr_seq_tipo_lanc is null))
	and	((a.nr_seq_bonificacao 		= nr_seq_bonificacao_p) or (a.nr_seq_bonificacao is null))
	and	((a.ie_tipo_segurado		= ie_tipo_segurado_w) or (a.ie_tipo_segurado is null))
	and	((a.nr_seq_contrato		= nr_seq_contrato_w) or (a.nr_seq_contrato is null))
	and	((a.nr_seq_evento		= nr_seq_evento_p) or (a.nr_seq_evento is null))
	and	exists	(	select	1
				from	dual
				where	a.ie_prestador_codificacao 	= 'P'
				and	((a.nr_seq_prestador		= nr_seq_prestador_pag_p) 	or a.nr_seq_prestador is null)
				union all
				select	1
				from	dual
				where	a.ie_prestador_codificacao	= 'E'
				and	((a.nr_seq_prestador		= nr_seq_prestador_exec_p) 	or a.nr_seq_prestador is null)
				union all
				select	1
				from	dual
				where	a.ie_prestador_codificacao 	= 'A'
				and	((a.nr_seq_prestador		= nr_seq_prestador_atend_p) 	or a.nr_seq_prestador is null)
				union all
				select	1
				from	dual
				where	a.ie_prestador_codificacao	= 'S'
				and	((a.nr_seq_prestador		= nr_seq_prestador_solic_p) 	or a.nr_seq_prestador is null)
				union all
				select	1
				from	dual
				where	a.ie_prestador_codificacao 	is null
				and	a.nr_seq_prestador 		is null
				union all
				select	1
				from	dual
				where	a.ie_prestador_codificacao 	is null
				and	a.nr_seq_prestador 		is not null
				and	(((a.nr_seq_prestador		= nr_seq_prestador_pag_p) and (ie_status_prov_pagto_w = 'F'))
				or	((a.nr_seq_prestador		= nr_seq_prestador_exec_p) and (ie_status_prov_pagto_w = 'NC'))))
	order by	nvl(a.nr_seq_plano_sca,0),
			nvl(a.nr_seq_plano,0),
			nvl(a.ie_tipo_vinculo_operadora,'X') desc,
			nvl(a.cd_municipio_ibge,' '),
			nvl(a.ie_tipo_contratacao,' '),
			nvl(a.ie_regulamentacao,' '),
			nvl(a.ie_tipo_item,' '),
			nvl(a.nr_seq_tipo_lanc,0),
			nvl(a.nr_seq_bonificacao,0),
			nvl(a.ie_tipo_segurado, ' '),
			nvl(a.nr_seq_contrato, 0),
			nvl(a.nr_seq_evento, 0),
			nvl(a.ie_prestador_codificacao, 'E'),
			nvl(a.nr_seq_prestador,0);

begin

begin
select	ie_tipo_vinculo_operadora,
	nr_seq_contrato,
	ie_tipo_segurado
into	ie_tipo_vinculo_operadora_w,
	nr_seq_contrato_w,
	ie_tipo_segurado_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;
exception
when others then
	ie_tipo_vinculo_operadora_w	:= null;
end;

begin
select	max(nvl(ie_status_prov_pagto,'NC'))
into	ie_status_prov_pagto_w
from	pls_parametro_contabil
where	cd_estabelecimento = cd_estabelecimento_p;
exception when others then
	ie_status_prov_pagto_w := 'NC';
end;

open C01;
loop
fetch C01 into	
	cd_centro_custo_w,
	ie_centro_custo_w,
	nr_sequencia_w;
exit when C01%notfound;
end loop;
close C01;

if	(nvl(ie_centro_custo_w,'R') = 'B') then
	select	max(cd_centro_custo)
	into	cd_centro_custo_w
	from	pls_segurado_compl
	where	nr_seq_segurado	= nr_seq_segurado_p;
end if;

cd_centro_custo_p	:= cd_centro_custo_w;
nr_seq_regra_p		:= nr_sequencia_w;

end pls_obter_centro_custo;
/
