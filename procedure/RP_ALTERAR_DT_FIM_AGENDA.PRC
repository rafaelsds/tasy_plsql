create or replace
procedure rp_alterar_dt_fim_agenda( 	nr_sequencia_p	number,
					dt_fim_agenda_p	date,
					ie_opcao_p	varchar2) is 

begin
if	(dt_fim_agenda_p is not null) then
	if	(ie_opcao_p = 'I') and
		(nr_sequencia_p is not null) then
		update	rp_pac_agend_individual
		set	dt_fim_agendamento = dt_fim_agenda_p
		where	nr_sequencia = nr_sequencia_p;
	elsif	(ie_opcao_p = 'M') and
		(nr_sequencia_p is not null) then
		update	rp_pac_modelo_agendamento
		set	dt_fim_agendamento = dt_fim_agenda_p
		where	nr_sequencia = nr_sequencia_p;
	end if;
end if;
commit;

end rp_alterar_dt_fim_agenda;
/