create or replace procedure update_diagnosis_recurrence(nr_seq_prescr_p	    number,
                                                        nr_seq_diag_p       number,
                                                        ie_recorrencia_p   pe_prescr_diag.ie_recorrencia%type) is
begin
    update  pe_prescr_diag
    set     ie_recorrencia = ie_recorrencia_p
    where   nr_seq_prescr = nr_seq_prescr_p
    and     nr_seq_diag = nr_seq_diag_p;

    commit;
end update_diagnosis_recurrence;
/