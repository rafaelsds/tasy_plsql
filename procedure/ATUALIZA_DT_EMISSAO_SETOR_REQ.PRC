create or replace
procedure	atualiza_dt_emissao_setor_req	(nr_requisicao_p		number) is

begin

update	requisicao_material
set	dt_emissao_setor = sysdate
where	nr_requisicao = nr_requisicao_p;

commit;

end atualiza_dt_emissao_setor_req;
/