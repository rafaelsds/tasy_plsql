create or replace
procedure Executar_Vacina (	nr_sequencia_p		Number,
				nm_usuario_p		Varchar2) is 

begin

if (nr_sequencia_p is not null) then
	update	paciente_vacina
	set	ie_executado = 'S',
		dt_vacina = nvl(dt_vacina,sysdate),
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_sequencia_p;
end if;

commit;

end Executar_Vacina;
/