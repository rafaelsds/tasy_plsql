create or replace
procedure gerar_nom_rc_prognostico(nr_seq_cabecalho_p	number,
									nm_usuario_p		Varchar2) is 


/* Impresión diagnóstica */

ds_prognostico_w		nom_rc_prognostico.ds_prognostico%type	:= null;
ds_problema_w			varchar2(32000);

nr_atendimento_w			atendimento_paciente.nr_atendimento%type;
nr_seq_episodio_w			episodio_paciente.nr_sequencia%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

Cursor c_problema is
	select	b.ds_prognostico ds_prognostico
	from    lista_problema_pac a,
			lista_problema_pac_prog b
	where 	a.nr_sequencia = b.nr_seq_problema
	and		a.dt_liberacao is not null
	and		a.dt_inativacao is null
	and		a.nr_atendimento in (select		x.nr_atendimento
							from	nom_rc_cabecalho x
							where	x.nr_sequencia = nr_seq_cabecalho_p
							and		x.nr_atendimento is not null
							union
							select	y.nr_atendimento
							from	atendimento_paciente y,
									nom_rc_cabecalho x
							where	x.nr_seq_episodio = y.nr_seq_episodio
							and		x.nr_sequencia = nr_seq_cabecalho_p);
									
begin
delete from nom_rc_prognostico
where nr_seq_cabecalho = nr_seq_cabecalho_p;

select	a.nr_atendimento,
		a.nr_seq_episodio
into	nr_atendimento_w,
		nr_seq_episodio_w
from	nom_rc_cabecalho a
where	a.nr_sequencia	= nr_seq_cabecalho_p;


if	(nr_atendimento_w is not null) then
	null;
elsif	(nr_seq_episodio_w is not null) then
	select	min(nr_atendimento)
	into	nr_atendimento_w
	from	atendimento_paciente a
	where	a.nr_seq_episodio = nr_seq_episodio_w;
end if;

if	(nr_atendimento_w is not null) then
	for r_c_problema in c_problema loop
		if	(ds_problema_w is null) then
			ds_problema_w	:= substr(r_c_problema.ds_prognostico,1,32000);
		else
			ds_problema_w	:= substr(ds_problema_w || chr(13) || chr(10) || r_c_problema.ds_prognostico,1,32000);
		end if;
	end loop;
	
	ds_prognostico_w	:= ds_problema_w;

	if	(ds_prognostico_w is not null) then
		insert into nom_rc_prognostico
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ds_prognostico,
			nr_seq_cabecalho)
		values
			(nom_rc_prognostico_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			ds_prognostico_w,
			nr_seq_cabecalho_p);
	end if;
end if;

end gerar_nom_rc_prognostico;
/
