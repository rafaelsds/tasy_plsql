create or replace
procedure vipe_obter_horarios_proced (
		cd_estabelecimento_p		number,
		cd_setor_usuario_p		number,
		cd_perfil_p			number,
		nm_usuario_p			varchar2,
		nr_atendimento_p		number,
		dt_inicial_horarios_p		date,
		dt_final_horarios_p		date,					
		dt_validade_limite_p		date,					
		ie_exibir_hor_realizados_p	varchar2,
		ie_exibir_hor_suspensos_p	varchar2,
		ie_regra_inclusao_p		varchar2,
		ie_data_lib_prescr_p		varchar2,
		ie_exibir_suspensos_p		varchar2,
		ie_data_lib_proced_p		varchar2,
		ie_so_proc_setor_usuario_p	varchar2,
		ie_prescr_setor_p		varchar2,
		cd_setor_paciente_p		number) is
					
dt_horario_w	date;
					
cursor c01 is
select	c.dt_horario
from	prescr_procedimento x,
	prescr_proc_hor c,
	prescr_medica a
where	x.nr_prescricao = c.nr_prescricao
and	x.nr_sequencia = c.nr_seq_procedimento
and	x.nr_prescricao = a.nr_prescricao
and	c.nr_prescricao = a.nr_prescricao
and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	obter_data_lib_proc_adep(a.dt_liberacao, a.dt_liberacao_medico, ie_data_lib_proced_p) is not null
and	x.nr_seq_proc_interno is null
and	x.nr_seq_exame is null
and	x.nr_seq_solic_sangue is null
and	x.nr_seq_derivado is null
and	x.nr_seq_exame_sangue is null
and	((ie_exibir_suspensos_p = 'S') or (x.dt_suspensao is null))
and	((ie_so_proc_setor_usuario_p = 'N') or (adep_obter_se_setor_proc_user(nm_usuario_p, x.cd_setor_atendimento) = 'S'))
and	nvl(c.ie_situacao,'A') = 'A'
and	c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p
and	((nvl(c.ie_horario_especial,'N') = 'N') or (c.dt_fim_horario is not null))
and	((ie_exibir_hor_realizados_p = 'S') or (c.dt_fim_horario is null))
and	((ie_exibir_hor_suspensos_p = 'S') or (c.dt_suspensao is null))
and	((ie_regra_inclusao_p = 'S') or
	 ((ie_regra_inclusao_p = 'R') and (adep_obter_regra_inclusao(	'PROC', 
																	cd_estabelecimento_p, 
																	cd_setor_usuario_p, 
																	cd_perfil_p, 
																	null, 
																	c.cd_procedimento, 
																	c.ie_origem_proced, 
																	c.nr_seq_proc_interno,
																	a.cd_setor_Atendimento,
																	x.cd_setor_Atendimento,
																	null, -- nr_prescricao_p. Passei nulo porque criaram o param na adep_obter_regra_inclusao como default null, e n�o haviam passado nada
																	x.nr_seq_exame) = 'S'))) -- nr_seq_exame_p
and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = Obter_Unidade_Atendimento(nr_atendimento_p, 'IA', 'CS'))))
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
group by
	c.dt_horario	 
union all
select	c.dt_horario
from	proc_interno w,
	prescr_procedimento x,
	prescr_proc_hor c,
	prescr_medica a
where	w.nr_sequencia = x.nr_seq_proc_interno
and	w.nr_sequencia = c.nr_seq_proc_interno
and	x.nr_prescricao = c.nr_prescricao
and	x.nr_sequencia = c.nr_seq_procedimento
and	x.nr_prescricao = a.nr_prescricao
and	c.nr_prescricao = a.nr_prescricao
and	obter_se_exibir_rep_adep_setor(cd_setor_paciente_p,a.cd_setor_atendimento,nvl(a.ie_adep,'S')) = 'S'
and	a.nr_atendimento = nr_atendimento_p
and	a.dt_validade_prescr > dt_validade_limite_p
and	obter_data_lib_proc_adep(a.dt_liberacao, a.dt_liberacao_medico, ie_data_lib_proced_p) is not null
and	w.ie_tipo <> 'G'
and	w.ie_tipo <> 'BS'
and	nvl(w.ie_ivc,'N') <> 'S'
and	nvl(w.ie_ctrl_glic,'NC') = 'NC'
and	x.nr_seq_proc_interno is not null
and	x.nr_seq_prot_glic is null
and	x.nr_seq_exame is null
and	x.nr_seq_solic_sangue is null
and	x.nr_seq_derivado is null
and	x.nr_seq_exame_sangue is null
and	((ie_exibir_suspensos_p = 'S') or (x.dt_suspensao is null))
and	((ie_so_proc_setor_usuario_p = 'N') or (adep_obter_se_setor_proc_user(nm_usuario_p, x.cd_setor_atendimento) = 'S'))
and	nvl(c.ie_situacao,'A') = 'A'
and	c.dt_horario between dt_inicial_horarios_p and dt_final_horarios_p
and	((nvl(c.ie_horario_especial,'N') = 'N') or (c.dt_fim_horario is not null))
and	((ie_exibir_hor_realizados_p = 'S') or (c.dt_fim_horario is null))
and	((ie_exibir_hor_suspensos_p = 'S') or (c.dt_suspensao is null))
and	((ie_regra_inclusao_p = 'S') or
	 ((ie_regra_inclusao_p = 'R') and (adep_obter_regra_inclusao(	'PROC', 
																	cd_estabelecimento_p, 
																	cd_setor_usuario_p, 
																	cd_perfil_p, 
																	null, 
																	c.cd_procedimento, 
																	c.ie_origem_proced, 
																	c.nr_seq_proc_interno,
																	a.cd_setor_Atendimento,
																	x.cd_setor_Atendimento,
																	null, -- nr_prescricao_p. Passei nulo porque criaram o param na adep_obter_regra_inclusao como default null, e n�o haviam passado nada
																	x.nr_seq_exame) = 'S'))) -- nr_seq_exame_p
and	((ie_prescr_setor_p = 'N') or ((ie_prescr_setor_p = 'S') and (a.cd_setor_atendimento = Obter_Unidade_Atendimento(nr_atendimento_p, 'IA', 'CS'))))
and	Obter_se_horario_liberado(c.dt_lib_horario, c.dt_horario) = 'S'
group by
	c.dt_horario;	
	
begin
open c01;
loop
fetch c01 into dt_horario_w;
exit when c01%notfound;
	begin
	insert into w_vipe_horarios_t (
		nm_usuario,
		dt_horario)
	values (
		nm_usuario_p,
		dt_horario_w);
	end;
end loop;
close c01;
end vipe_obter_horarios_proced;
/
