create or replace procedure gerar_ctg_seq_prescription(	nr_sequencia_p	number,
                                                        nm_usuario_p fa_receita_farmacia.nm_usuario%type) is						

nr_presc_ctg_w			number;
cd_presc_ctg_w varchar(2);
dt_prescription_w date;
ds_prescr_ctg_w varchar(6) ;
cd_pessoa_fisica_w pessoa_fisica.cd_pessoa_fisica%type;
nr_seq_etnia_w number;

begin

if (pkg_i18n.get_user_locale()='en_AU') then

    if	((nvl(nr_sequencia_p,0)) > 0) then
    
          begin
    
                select  dt_receita,cd_pessoa_fisica into dt_prescription_w,cd_pessoa_fisica_w 
                from fa_receita_farmacia 
                where nr_sequencia = nr_sequencia_p;
                
                select nr_seq_etnia into nr_seq_etnia_w
                from pessoa_fisica
                where cd_pessoa_fisica = cd_pessoa_fisica_w;
                
                if (nr_seq_etnia_w is not null) then
                      begin
                              select mod(count(*)+1,99) into nr_presc_ctg_w 
                              from  fa_receita_farmacia 
                              where trunc(dt_receita) = trunc(dt_prescription_w) 
                              and cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
                              and dt_liberacao is not null
                              and dt_cancelamento is null
                              and cd_ctg_prescr is not null;
                              
                              
                              if ( nr_presc_ctg_w < 10) then
                                cd_presc_ctg_w := '0' ||to_char(nr_presc_ctg_w);
                              else
                                cd_presc_ctg_w := to_char(nr_presc_ctg_w);
                              end if;
                              
              
                              select ('CTG' || cd_presc_ctg_w || decode(mod(to_number(cd_presc_ctg_w || to_char(dt_prescription_w,'ddmmyy')),19),0,'B',1,'C',2,'D',3,'E',4,'F',5,'G',6,'H',7,'J',8,'K',9,'L',10,'M',11,'N',12,'P',13,'Q',14,'R',15,'T',16,'W',17,'X','Y'))
                              into ds_prescr_ctg_w from dual;
                              
              
              
                              update fa_receita_farmacia 
                              set nr_prescr_seq_ctg = nr_presc_ctg_w , 
                              cd_ctg_prescr =  ds_prescr_ctg_w,
                              nm_usuario = nm_usuario_p, 
                              dt_atualizacao = sysdate,
                              dt_liberacao    = sysdate,
                              nm_usuario_lib	= nm_usuario_p
                              where nr_sequencia = nr_sequencia_p;
                              commit;
                              
                    
                     end;
                    end if;
          end;
    
    end if;

end if;
exception 
when others then 
null;
end gerar_ctg_seq_prescription;
/
