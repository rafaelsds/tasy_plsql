create or replace
procedure sup_receb_solic_unitarizacao(
			nr_sequencia_p		number,
			cd_pessoa_recebimento_p	varchar2,
			dt_recebimento_p		date,
			cd_local_estoque_p	number,
			nr_seq_motivo_unit_p	number,
			nm_usuario_p		varchar2) is

--SOLIC_UNITARIZACAO
cd_local_origem_w	solic_unitarizacao.cd_local_estoque_origem%type;
dt_recebimento_w	date;
qt_registros_w		number(10);

cd_operacao_estoque_w	number(3);

begin

dt_recebimento_w := dt_recebimento_p;
if	(dt_recebimento_w is null) then
	dt_recebimento_w := sysdate;
end if;

select	cd_local_estoque_origem
into	cd_local_origem_w
from	solic_unitarizacao
where	nr_sequencia = nr_sequencia_p;

select	count(*)
into	qt_registros_w
from	solic_unitarizacao
where	nr_sequencia = nr_sequencia_p
and	dt_recebimento is null;

if	(nvl(qt_registros_w, 0) > 0) then
	update	solic_unitarizacao
	set	cd_pessoa_recebimento 	= cd_pessoa_recebimento_p,
		dt_recebimento 		= dt_recebimento_w,
		cd_local_estoque_destino = cd_local_estoque_p,
		dt_atualizacao 		= sysdate,
		nm_usuario 		= nm_usuario_p
	where	nr_sequencia 		= nr_sequencia_p;

	if 	(nr_seq_motivo_unit_p <> 0) then
		update	solic_unitarizacao
		set	nr_seq_motivo_unit	= nr_seq_motivo_unit_p
		where	nr_sequencia 		= nr_sequencia_p;
	end if;

	cd_operacao_estoque_w := sup_obter_operacao_unit('ET');

	if	(cd_local_estoque_p <> cd_local_origem_w) then
		gerar_movto_estoque_trans_unit(nr_sequencia_p, cd_operacao_estoque_w, '1', 'D', nm_usuario_p);
	else
		--N�o � permitido receber a solicita��o de unitariza��o no local de estoque solicitante!
		wheb_mensagem_pck.exibir_mensagem_abort(289222);
	end if;

	commit;
else
	-- Solicita��o de unitariza��o j� recebida!
	wheb_mensagem_pck.exibir_mensagem_abort(957403);
end if;

end sup_receb_solic_unitarizacao;
/