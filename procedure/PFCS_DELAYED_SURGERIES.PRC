create or replace procedure pfcs_delayed_surgeries( 	nr_seq_indicator_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p	varchar2) is

cursor c01 is
select
     a.cd_pessoa_fisica,
     nvl(get_formatted_person_name(a.cd_pessoa_fisica, 'list'), obter_nome_pf(a.cd_pessoa_fisica)) nm_patient,
     substr(nvl(obter_nome_sala_cirur(nvl(b.nr_seq_sala_cir,c.nr_seq_sala_cir), ''),obter_unid_setor_cirurgia(a.nr_cirurgia,'U')), 1, 255) ds_room,
     a.dt_inicio_real,
     a.dt_inicio_prevista,
     a.cd_procedimento_princ cd_procedure,
     substr(obter_exame_agenda(a.cd_procedimento_princ, a.ie_origem_proced, a.nr_seq_proc_interno),1,240) ds_procedure,
     b.dt_agendamento,
     date_as_varchar2(to_date(to_char(b.dt_agenda, 'dd/mm/yyyy') || to_char(b.hr_inicio, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'), 'shortDate') dt_agenda,
     a.nr_min_duracao_real nr_minutos_duracao_real,
     a.nr_min_duracao_prev nr_minutos_duracao_prev,
     a.cd_medico_cirurgiao cd_medico,
     nvl(get_formatted_person_name(a.cd_medico_cirurgiao, 'list'), obter_nome_pf(a.cd_medico_cirurgiao)) nm_medico,
     a.ie_status_cirurgia,
     b.ie_status_agenda status_agenda,
     obter_dados_pf(b.cd_pessoa_fisica, 'I') qt_idade_paciente,
     a.dt_termino_prevista,
     a.dt_termino,
     a.nr_atendimento  nr_atendimento,
     substr(obter_Sexo_pf(b.cd_pessoa_fisica,'D'),1,60) ds_gender,
     obter_desc_classif_pac(b.nr_seq_tipo_classif_pac) ds_classification,
     b.dt_nascimento_pac
FROM   cirurgia  a, agenda_paciente b, agenda c
WHERE b.nr_cirurgia = a.nr_cirurgia
AND b.cd_agenda = c.cd_agenda
AND c.cd_estabelecimento = cd_estabelecimento_p
AND (nvl(a.dt_inicio_real,SYSDATE) > a.dt_inicio_prevista)
AND a.ie_status_cirurgia IN ('1','2')
AND b.ie_status_agenda <> 'PA'
AND a.dt_inicio_prevista >= sysdate - 1;


cursor c02 is
SELECT
    max(ap2.IE_STATUS_AGENDA) status_agenda,
    max(obter_desc_agenda(ap2.cd_agenda)) agenda,
    max(ap2.hr_inicio) inicio_potencial_atraso,
    max(ap2.nr_atendimento) nr_atendimento,
    max(c2.cd_pessoa_fisica) cd_patient,
    max(nvl(get_formatted_person_name(c2.cd_pessoa_fisica, 'list'), obter_nome_pf(c2.cd_pessoa_fisica))) nm_patient,
    max(substr(nvl(obter_nome_sala_cirur(nvl(ap2.nr_seq_sala_cir,ag.nr_seq_sala_cir), ''),obter_unid_setor_cirurgia(c2.nr_cirurgia,'U')), 1, 255)) ds_room,
    max(c2.cd_procedimento_princ) cd_procedure,
    substr(max(obter_exame_agenda(c2.cd_procedimento_princ, c2.ie_origem_proced, c2.nr_seq_proc_interno)),1,240) ds_procedure,
    max(ap2.dt_agendamento) dt_agendamento,
    date_as_varchar2(to_date(to_char(max(ap2.dt_agenda), 'dd/mm/yyyy') || to_char(max(ap2.hr_inicio), 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss'), 'shortDate') dt_agenda,
    max(c2.nr_min_duracao_real) tempo_real,
    max(c2.nr_min_duracao_prev) tempo_previsto,
    max(c2.dt_inicio_prevista) dt_inicio_previsto,
    max(c2.dt_termino_prevista) dt_termino_previsto,
    max(substr(obter_Sexo_pf(c2.cd_pessoa_fisica,'D'),1,60)) ds_gender,
    max(obter_desc_classif_pac(ap2.nr_seq_tipo_classif_pac)) ds_classification,
    max(ap2.dt_nascimento_pac) dt_nascimento,
    max(ap2.hr_inicio) inicio_cirurgia_atrasada,
    max(c2.cd_medico_cirurgiao) cd_medico,
    max(nvl(get_formatted_person_name(c2.cd_medico_cirurgiao, 'list'), obter_nome_pf(c2.cd_medico_cirurgiao))) nm_medico,
    c2.nr_cirurgia nr_surgey,
    max(ap1.nr_cirurgia) delayed_surgery,
    max(c2.dt_inicio_real)  dt_inicio_real,
	max(obter_dados_pf(ap2.cd_pessoa_fisica, 'I')) qt_idade_paciente
FROM
    agenda_paciente ap1
    INNER JOIN cirurgia c1 ON c1.nr_cirurgia = ap1.nr_cirurgia
    INNER JOIN agenda ag ON ag.cd_agenda = ap1.cd_agenda
    INNER JOIN agenda_paciente ap2 ON ap2.cd_agenda = ap1.cd_agenda
    INNER JOIN cirurgia c2 ON c2.nr_cirurgia = ap2.nr_cirurgia
WHERE
    ag.cd_estabelecimento = cd_estabelecimento_p
    AND TRUNC(ap1.hr_inicio) >= TRUNC(SYSDATE)
    AND c1.dt_inicio_real is not null
    AND c1.dt_inicio_real > c1.dt_inicio_prevista
    AND c2.dt_inicio_real is null
    AND c2.ie_status_cirurgia IN ('1','2')
    AND ap2.hr_inicio > ap1.hr_inicio
	AND ap1.ie_status_agenda <> 'PA' 
	AND ap2.ie_status_agenda <> 'PA'
    AND c2.dt_inicio_prevista >= SYSDATE
GROUP BY c2.nr_cirurgia
ORDER BY  inicio_potencial_atraso desc;


pfcs_panel_detail_seq_w         pfcs_panel_detail.nr_sequencia%type;
pfcs_detail_bed_seq_w           pfcs_detail_bed.nr_sequencia%type;
pfcs_panel_seq_w                pfcs_panel.nr_sequencia%type;
nr_seq_operational_level_w      pfcs_operational_level.nr_sequencia%type;

qt_delayed_w        number(20) := 0;
qt_would_delayed_w  number(20) := 0;

begin

    nr_seq_operational_level_w := pfcs_get_structure_level(
            cd_establishment_p => cd_estabelecimento_p,
            ie_level_p => 'O',
            ie_info_p => 'C');

    for c01_w in c01 loop
        begin

        qt_delayed_w := qt_delayed_w+1;

        select  pfcs_panel_detail_seq.nextval
        into  pfcs_panel_detail_seq_w
        from  dual;


                insert into pfcs_panel_detail(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    ie_situation,
                    nr_seq_indicator,
                    nr_seq_operational_level)
                values (
                    pfcs_panel_detail_seq_w,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    'T',
                    nr_seq_indicator_p,
                    nr_seq_operational_level_w);

                insert into pfcs_detail_patient(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    nr_seq_detail,
                    nr_encounter,
                    id_patient,
                    nm_patient,
                    ds_gender,
                    ds_classification,
                    dt_birthdate,
                    ds_age_range)
                values (
                    pfcs_detail_patient_seq.nextval,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    pfcs_panel_detail_seq_w,
                    c01_w.nr_atendimento,
                    c01_w.cd_pessoa_fisica,
                    c01_w.nm_patient,
                    c01_w.ds_gender,
                    c01_w.ds_classification,
                    c01_w.dt_nascimento_pac,
                    c01_w.qt_idade_paciente);

                insert into pfcs_detail_schedule(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    nr_seq_detail,
                    cd_executing_physician,
                    nm_physician,
                    cd_procedure,
                    ds_procedure,
                    ds_room,
                    dt_schedule,
                    dt_scheduling,
                    hr_start,
                    qt_duration_minutes,
                    ie_status_schedule)
                values (
                    pfcs_detail_schedule_seq.nextval,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    pfcs_panel_detail_seq_w,
                    c01_w.cd_medico,
                    c01_w.nm_medico,
                    c01_w.cd_procedure,
                    c01_w.ds_procedure,
                    c01_w.ds_room,
                    c01_w.dt_agenda,
                    c01_w.dt_agendamento,
                    c01_w.dt_inicio_prevista,
                    c01_w.nr_minutos_duracao_prev,
                    c01_w.status_agenda);

                insert into pfcs_detail_surgery(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    nr_seq_detail,
                    cd_procedure,
                    ds_procedure,
                    dt_actual_start,
                    DT_ACTUAL_END,
                    dt_expected_final,
                    dt_expected_start)
                values (
                    pfcs_detail_schedule_seq.nextval,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    pfcs_panel_detail_seq_w,
                    c01_w.cd_procedure,
                    c01_w.ds_procedure,
                   c01_w.dt_inicio_real,
                    c01_w.dt_termino,
                    c01_w.dt_termino_prevista,
                    c01_w.dt_inicio_prevista);
        end;
    end loop;
    for c02_w in c02 loop
        begin

        qt_would_delayed_w :=qt_would_delayed_w+1;
        select  pfcs_panel_detail_seq.nextval
            into  pfcs_panel_detail_seq_w
            from  dual;
        insert into pfcs_panel_detail(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    ie_situation,
                    nr_seq_indicator,
                    nr_seq_operational_level)
                values (
                    pfcs_panel_detail_seq_w,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    'T',
                    nr_seq_indicator_p,
                    nr_seq_operational_level_w);

                insert into pfcs_detail_patient(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    nr_seq_detail,
                    nr_encounter,
                    id_patient,
                    nm_patient,
                    ds_gender,
                    ds_classification,
                    dt_birthdate,
                    ds_age_range)
                values (
                    pfcs_detail_patient_seq.nextval,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    pfcs_panel_detail_seq_w,
                    c02_w.nr_atendimento,
                    c02_w.cd_patient,
                    c02_w.nm_patient,
                    c02_w.ds_gender,
                    c02_w.ds_classification,
                    c02_w.dt_nascimento,
                    c02_w.qt_idade_paciente);

                insert into pfcs_detail_schedule(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    nr_seq_detail,
                    cd_executing_physician,
                    nm_physician,
                    cd_procedure,
                    ds_procedure,
                    ds_room,
                    dt_schedule,
                    dt_scheduling,
                    hr_start,
                    qt_duration_minutes,
                    ie_status_schedule)
                values (
                    pfcs_detail_schedule_seq.nextval,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    pfcs_panel_detail_seq_w,
                    c02_w.cd_medico,
                    c02_w.nm_medico,
                    c02_w.cd_procedure,
                    c02_w.ds_procedure,
                    c02_w.ds_room,
                    c02_w.dt_agenda,
                    c02_w.dt_agendamento,
                    c02_w.dt_inicio_real,
                    c02_w.tempo_previsto,
                    c02_w.status_agenda);

                insert into pfcs_detail_surgery(
                    nr_sequencia,
                    nm_usuario,
                    dt_atualizacao,
                    nm_usuario_nrec,
                    dt_atualizacao_nrec,
                    nr_seq_detail,
                    cd_procedure,
                    ds_procedure,
                    dt_actual_start,
                    dt_expected_final,
                    dt_expected_start)
                values (
                    pfcs_detail_schedule_seq.nextval,
                    nm_usuario_p,
                    sysdate,
                    nm_usuario_p,
                    sysdate,
                    pfcs_panel_detail_seq_w,
                    c02_w.cd_procedure,
                    c02_w.ds_procedure,
                    c02_w.dt_inicio_real,
                    c02_w.dt_termino_previsto,
                    c02_w.dt_inicio_previsto);

        end;
    end loop;
    commit;

    pfcs_pck.pfcs_generate_results(
            vl_indicator_p => qt_delayed_w,
            vl_indicator_aux_p => qt_would_delayed_w,
            nr_seq_indicator_p => nr_seq_indicator_p,
            nr_seq_operational_level_p => nr_seq_operational_level_w,
            nm_usuario_p => nm_usuario_p,
            nr_seq_panel_p => pfcs_panel_seq_w);

    pfcs_pck.pfcs_update_detail(
            nr_seq_indicator_p => nr_seq_indicator_p,
            nr_seq_panel_p => pfcs_panel_seq_w,
            nr_seq_operational_level_p => nr_seq_operational_level_w,
            nm_usuario_p => nm_usuario_p);

    pfcs_pck.pfcs_activate_records(
            nr_seq_indicator_p => nr_seq_indicator_p,
            nr_seq_operational_level_p => nr_seq_operational_level_w,
            nm_usuario_p => nm_usuario_p);

end pfcs_delayed_surgeries;
/
