create or replace
procedure gerar_fluxo_caixa_doc_real(cd_estabelecimento_p	number,
					dt_inicio_p		date,
					dt_final_p		date,    
					nm_usuario_p		varchar2,
					cd_empresa_p		number,
					ie_restringe_estab_p	varchar2,
					vl_saldo_anterior_p	number,
					ie_periodo_p		varchar2,
					ie_todas_contas_p	varchar2,
					ie_passado_real_p	varchar2,
					ie_operacao_p		varchar2,
					ie_somente_ativa_p	varchar2,
					ie_tit_rec_p		varchar2,
					ie_tit_pagar_p		varchar2,
					ie_cheque_p		varchar2,
					ie_cartao_p		varchar2,
					ie_conv_receb_p		varchar2,
					ie_protocolo_p		varchar2,
					ie_ordem_compra_p	varchar2,
					ie_contrato_p		varchar2,
					ie_fluxo_especial_p	varchar2,
					ie_agenda_p		varchar2,
					ie_bordero_pagar_p	varchar2,
					ie_pagto_escrit_p	varchar2,
					ie_tipo_fluxo_p		varchar2 default 'R',
					nr_seq_regra_p		number default null) is

/*--------------------------------------------------------------- ATEN��O ----------------------------------------------------------------*/
/* Cuidado ao realizar altera��es no fluxo de caixa. Toda e qualquer altera��o realizada em qualquer uma das       */
/* procedures do fluxo de caixa deve ser cuidadosamente verificada e realizada no fluxo de caixa em lote.           */
/* Devemos garantir que os dois fluxos de caixa tragam os mesmos valores no resultado, evitando assim que           */
/* existam diferen�as entre os fluxos de caixa.                                                                                                                */
/*--------------- AO ALTERAR O FLUXO DE CAIXA ALTERAR TAMB�M O FLUXO DE CAIXA EM LOTE ---------------*/

dt_inicio_w			date;
dt_final_w			date;
dt_inicio_mes_w			date;
dt_final_fim_mes_w		date;
dt_referencia_w			date;
ie_filtro_fluxo_w		varchar2(1);
ie_filtro_fluxo_proj_w		varchar2(1);
qtd_dias_fluxo_oc_w		number(10);
ie_cheque_pago_pend_w		parametro_fluxo_caixa.ie_cheque_pago_pend%type	:= 'N';
ie_operacao_w			conta_financeira.ie_oper_fluxo%type;
ie_conta_financ_ativa_w		conta_financeira.ie_situacao%type;
ie_somente_futuro_w		parametro_fluxo_caixa.ie_somente_futuro%type;
ie_acumular_vencidos_w		parametro_fluxo_caixa.ie_acumular_vencidos%type;
ie_oc_aprovada_w		parametro_fluxo_caixa.ie_oc_aprovada%type;
ie_forma_calculo_contrato_w	parametro_fluxo_caixa.ie_forma_calculo_contrato%type;
/* Projeto Multimoeda - Vari�veis */
cd_moeda_empresa_w		moeda.cd_moeda%type;

vl_fluxo_w fluxo_caixa.vl_fluxo%type;

cursor c01 is
	select	a.ie_integracao,
		a.nr_titulo_receber,
		a.nr_titulo_pagar,
		a.dt_referencia,
		a.cd_conta_financ,
		sum(a.vl_fluxo) vl_fluxo,
		a.vl_saldo_estrangeiro,
		a.cd_moeda,
		a.ie_classif_fluxo,
		a.cd_estabelecimento,
		a.cd_empresa,
		obter_se_contrato_titulo(a.nr_titulo_pagar,a.nr_titulo_receber) ie_contrato
	from	fluxo_caixa_docto a
	where	a.ie_classif_fluxo = 'R'
	and a.ie_integracao <> 'OC'
	and	a.dt_referencia <= dt_final_w
	and	((a.dt_referencia >= dt_inicio_w)
		or (a.ie_integracao in ('CR','RC') and a.dt_referencia >= decode(ie_acumular_vencidos_w,'N',dt_inicio_w,a.dt_referencia)))
	and	a.cd_empresa = cd_empresa_p
	and	(ie_restringe_estab_p = 'N'
		or (ie_restringe_estab_p = 'E' and a.cd_estabelecimento = cd_estabelecimento_p)
		or (ie_restringe_estab_p = 'S' and (a.cd_estabelecimento = cd_estabelecimento_p or a.cd_estab_financeiro = cd_estabelecimento_p)))
	and	(ie_restringe_estab_p = 'N' or substr(obter_se_conta_financ_estab(a.cd_conta_financ, cd_estabelecimento_p,ie_restringe_estab_p),1,1) = 'S')
	and	(ie_filtro_fluxo_w = 'N' or substr(obter_se_filtro_fluxo_estab(a.nr_seq_conta_banco, a.nr_seq_cheque_cr, null, nm_usuario_p, a.nr_seq_caixa, cd_estabelecimento_p),1,1) = 'S')
	and	(ie_filtro_fluxo_proj_w = 'N' or substr(obter_se_proj_filtro_fluxo(a.nr_seq_proj_recurso, nm_usuario_p),1,1) = 'S')
	and	(ie_tit_rec_p = 'S' or a.nr_titulo_receber is null)
	and	(ie_tit_pagar_p = 'S' or (a.nr_titulo_pagar is null or a.ie_integracao <> 'CP'))
	and	(ie_cheque_p = 'S' or a.nr_seq_cheque_cr is null)
	and	(ie_cartao_p = 'S' or a.nr_seq_movto_cartao is null)
	and	(ie_conv_receb_p = 'S' or a.nr_seq_conv_receb is null)
	and	(ie_protocolo_p = 'S' or a.nr_seq_protocolo is null)
	and	(ie_bordero_pagar_p = 'S' or (a.nr_titulo_pagar is null or a.ie_integracao <> 'BP'))
	and	(ie_pagto_escrit_p = 'S' or (a.nr_titulo_pagar is null or a.ie_integracao <> 'PE'))
	and	(a.nr_titulo_receber is null
		or (a.nr_titulo_receber is not null
		and not exists	(select	1
				from	fluxo_caixa_excecao x
				where	x.ie_integracao		= 'TR'
				and	x.ie_origem_titulo_rec	= (select b.ie_origem_titulo from titulo_receber b where b.nr_titulo = a.nr_titulo_receber)
				and	x.ie_tipo_fluxo		= 'R')))
	group by a.ie_integracao,
		a.nr_titulo_receber,
		a.nr_titulo_pagar,
		a.dt_referencia,
		a.cd_conta_financ,
		a.vl_saldo_estrangeiro,
		a.cd_moeda,
		a.ie_classif_fluxo,
		a.cd_estabelecimento,
		a.cd_empresa,
		obter_se_contrato_titulo(a.nr_titulo_pagar,a.nr_titulo_receber)
	union all	/* acumular cheques pagos � compensar no primeiro dia do fluxo */
	select	a.ie_integracao,
		a.nr_titulo_receber,
		a.nr_titulo_pagar,
		dt_inicio_p,
		a.cd_conta_financ,
		sum(a.vl_fluxo) vl_fluxo,
		a.vl_saldo_estrangeiro,
		a.cd_moeda,
		a.ie_classif_fluxo,
		a.cd_estabelecimento,
		a.cd_empresa,
		'N' ie_contrato
	from	fluxo_caixa_docto a,
		cheque b
	where	a.ie_classif_fluxo = 'R'
	and a.ie_integracao <> 'OC'
	and	a.nr_seq_cheque_cp is not null
	and	a.nr_seq_cheque_cp = b.nr_sequencia
	and	a.dt_referencia < dt_inicio_w 
	and 	a.dt_referencia > dt_final_w
	and	b.dt_emissao < dt_inicio_w
	and	(b.dt_compensacao is null or b.dt_compensacao > dt_final_w)
	and	ie_cheque_pago_pend_w = 'S'
	and	a.cd_empresa = cd_empresa_p
	and	(ie_restringe_estab_p = 'N'
		or (ie_restringe_estab_p = 'E' and a.cd_estabelecimento = cd_estabelecimento_p)
		or (ie_restringe_estab_p = 'S' and (a.cd_estabelecimento = cd_estabelecimento_p or a.cd_estab_financeiro = cd_estabelecimento_p)))
	and	(ie_restringe_estab_p = 'N' or substr(obter_se_conta_financ_estab(a.cd_conta_financ, cd_estabelecimento_p,ie_restringe_estab_p),1,1) = 'S')
	and	(ie_filtro_fluxo_w = 'N' or substr(obter_se_filtro_fluxo_estab(a.nr_seq_conta_banco, a.nr_seq_cheque_cr, null, nm_usuario_p, a.nr_seq_caixa, cd_estabelecimento_p),1,1) = 'S')
	group by a.ie_integracao,
		a.nr_titulo_receber,
		a.nr_titulo_pagar,
		a.dt_referencia,
		a.cd_conta_financ,
		a.vl_saldo_estrangeiro,
		a.cd_moeda,
		a.ie_classif_fluxo,
		a.cd_estabelecimento,
		a.cd_empresa
	union all
	select	'CP' ie_integracao,
		a.nr_titulo_receber,
		a.nr_titulo_pagar,
		OBTER_VENC_TITULO(a.nr_titulo_pagar, null),
		a.cd_conta_financ,
		sum(a.vl_fluxo) vl_fluxo,
		a.vl_saldo_estrangeiro,
		a.cd_moeda,
		a.ie_classif_fluxo,
		a.cd_estabelecimento,
		a.cd_empresa,
		obter_se_contrato_titulo(a.nr_titulo_pagar,a.nr_titulo_receber) ie_contrato
		from	fluxo_caixa_docto a
		where	a.ie_classif_fluxo = 'R'
				and ie_integracao <> 'OC'
				and a.nr_titulo_pagar is not null
				and ((a.ie_integracao = 'PE' and ie_pagto_escrit_p = 'N') or (a.ie_integracao = 'BP' and ie_bordero_pagar_p = 'N'))
				and	((a.dt_referencia <= dt_final_w)
				or (a.ie_integracao in ('PE','BP') and OBTER_VENC_TITULO(a.nr_titulo_pagar, null) <= dt_final_w))
				and	((a.dt_referencia >= dt_inicio_w)
					or (a.ie_integracao in ('PE','BP') and OBTER_VENC_TITULO(a.nr_titulo_pagar, null) >= dt_inicio_w))
				and	a.cd_empresa = cd_empresa_p
				and	(ie_restringe_estab_p = 'N'
					or (ie_restringe_estab_p = 'E' and a.cd_estabelecimento = cd_estabelecimento_p)
					or (ie_restringe_estab_p = 'S' and (a.cd_estabelecimento = cd_estabelecimento_p or a.cd_estab_financeiro = cd_estabelecimento_p)))
				and	(ie_restringe_estab_p = 'N' or substr(obter_se_conta_financ_estab(a.cd_conta_financ, cd_estabelecimento_p,ie_restringe_estab_p),1,1) = 'S')
				and	(ie_filtro_fluxo_w = 'N' or substr(obter_se_filtro_fluxo_estab(a.nr_seq_conta_banco, a.nr_seq_cheque_cr, null, nm_usuario_p, a.nr_seq_caixa, cd_estabelecimento_p),1,1) = 'S')
				and	(ie_filtro_fluxo_proj_w = 'N' or substr(obter_se_proj_filtro_fluxo(a.nr_seq_proj_recurso, nm_usuario_p),1,1) = 'S')
				and	(ie_tit_rec_p = 'S' or a.nr_titulo_receber is null)
				and	(ie_tit_pagar_p = 'S' or (a.nr_titulo_pagar is null or a.ie_integracao <> 'CP'))
				and	(ie_cheque_p = 'S' or a.nr_seq_cheque_cr is null)
				and	(ie_cartao_p = 'S' or a.nr_seq_movto_cartao is null)
				and	(ie_conv_receb_p = 'S' or a.nr_seq_conv_receb is null)
				and	(ie_protocolo_p = 'S' or a.nr_seq_protocolo is null)
		group by 	a.nr_titulo_receber,
					a.nr_titulo_pagar,
					OBTER_VENC_TITULO(a.nr_titulo_pagar, null),
					a.cd_conta_financ,
					a.vl_saldo_estrangeiro,
					a.cd_moeda,
					a.ie_classif_fluxo,
					a.cd_estabelecimento,
					a.cd_empresa,
					obter_se_contrato_titulo(a.nr_titulo_pagar,a.nr_titulo_receber);

vet01		c01%rowtype;

cursor c02 is
	select	cd_conta_financ
	from	conta_financeira
	where	decode(ie_somente_ativa_p,'S',ie_situacao,'A') = 'A'
	and	(cd_empresa		= cd_empresa_p or cd_estabelecimento = cd_estabelecimento_p)
	and	((ie_restringe_estab_p = 'N') or (obter_se_conta_financ_estab(cd_conta_financ, cd_estabelecimento_p,ie_restringe_estab_p) = 'S'))
	and	nvl(ie_todas_contas_p,'N') = 'S';
	
vet02		c02%rowtype;


cursor c03 is
	select	a.ie_integracao,
		a.nr_ordem_compra,
		a.dt_referencia,
		a.cd_conta_financ,
		sum(a.vl_fluxo) vl_fluxo,
		a.vl_saldo_estrangeiro,
		a.cd_moeda,
		a.ie_classif_fluxo,
		a.cd_estabelecimento,
		a.cd_empresa,
		'N' ie_contrato
	from	fluxo_caixa_docto a,
		ORDEM_COMPRA b
	where	a.ie_classif_fluxo = 'R'
	and a.ie_integracao = 'OC'
	and a.ie_integracao <> 'CP'
	and a.ie_integracao <> 'CR'
	and	a.nr_ordem_compra is not null
	and	a.nr_ordem_compra = b.NR_ORDEM_COMPRA
	and	a.dt_referencia >= dt_inicio_w 
	and a.dt_referencia <= dt_final_w
	and	a.cd_empresa = cd_empresa_p
	and	(ie_restringe_estab_p = 'N'
		or (ie_restringe_estab_p = 'E' and a.cd_estabelecimento = cd_estabelecimento_p)
		or (ie_restringe_estab_p = 'S' and (a.cd_estabelecimento = cd_estabelecimento_p or a.cd_estab_financeiro = cd_estabelecimento_p)))
	and	(ie_restringe_estab_p = 'N' or substr(obter_se_conta_financ_estab(a.cd_conta_financ, cd_estabelecimento_p,ie_restringe_estab_p),1,1) = 'S')
	and	(ie_filtro_fluxo_w = 'N' or substr(obter_se_filtro_fluxo_estab(a.nr_seq_conta_banco, a.nr_seq_cheque_cr, null, nm_usuario_p, a.nr_seq_caixa, cd_estabelecimento_p),1,1) = 'S')
	group by a.ie_integracao,
		a.nr_ordem_compra,
		a.dt_referencia,
		a.cd_conta_financ,
		a.vl_saldo_estrangeiro,
		a.cd_moeda,
		a.ie_classif_fluxo,
		a.cd_estabelecimento,
		a.cd_empresa;
vet03		c03%rowtype;


begin

/* Projeto Multimoeda - Busca a moeda padr�o da empresa para gravar no fluxo. */
select	obter_moeda_padrao_empresa(cd_estabelecimento_p,'E')
into	cd_moeda_empresa_w
from 	dual;

select	nvl(max(ie_cheque_pago_pend),'N'),
	nvl(max(ie_somente_futuro),'N'),
	nvl(max(ie_acumular_vencidos),'N'),
	nvl(max(ie_oc_aprovada),'S'),
	nvl(max(ie_forma_calculo_contrato),'R')
into 	ie_cheque_pago_pend_w,
	ie_somente_futuro_w,
	ie_acumular_vencidos_w,
	ie_oc_aprovada_w,
	ie_forma_calculo_contrato_w
from	parametro_fluxo_caixa
where	cd_estabelecimento	= cd_estabelecimento_p;

dt_inicio_w		:= pkg_date_utils.start_of(dt_inicio_p,'DD',0);
dt_final_w		:= fim_dia(dt_final_p);
dt_inicio_mes_w		:= pkg_date_utils.start_of(dt_inicio_p,'MONTH',0);
dt_final_fim_mes_w	:= fim_mes(dt_final_p);
if	(ie_somente_futuro_w = 'S') then
	dt_inicio_w	:= pkg_date_utils.start_of(sysdate,'DD',0);
end if;

select	nvl(max('S'),'N')
into	ie_filtro_fluxo_w
from	w_filtro_fluxo
where	nm_usuario	= nm_usuario_p
and	nr_seq_proj_rec	is null;

select	nvl(max('S'),'N')
into	ie_filtro_fluxo_proj_w
from	w_filtro_fluxo
where	nm_usuario	= nm_usuario_p
and	nr_seq_proj_rec	is not null;


delete	from fluxo_caixa
where	dt_referencia 		>= dt_inicio_w
and	ie_classif_fluxo 	in ('R', 'V')
and	ie_origem 		<> 'D'
and	cd_empresa		= cd_empresa_p
and	(ie_restringe_estab_p = 'N' or
	(ie_restringe_estab_p = 'E' and cd_estabelecimento = cd_estabelecimento_p) or
	(ie_restringe_estab_p = 'S' and (cd_estabelecimento = cd_estabelecimento_p or obter_estab_financeiro(cd_estabelecimento) = cd_estabelecimento_p)));

commit;

open c01;
loop
fetch c01 into 
	vet01;
exit when c01%notfound;
	
	dt_referencia_w := vet01.dt_referencia;
	if	(vet01.dt_referencia < dt_inicio_w) then
		if	(ie_acumular_vencidos_w = 'S') then
			dt_referencia_w	:= dt_inicio_w;
		end if;
	end if;
		
	if	(vet01.cd_conta_financ <> 0) then
		
		select	max(ie_oper_fluxo),
			decode(ie_somente_ativa_p,'S',max(ie_situacao),'A')
		into	ie_operacao_w,
			ie_conta_financ_ativa_w
		from	conta_financeira
		where	cd_conta_financ	= vet01.cd_conta_financ;
		
		if	(ie_operacao_p = 'A') or
			(ie_operacao_p = 'D' and ie_operacao_w <> 'S') or
			(ie_operacao_p = 'S' and ie_operacao_w <> 'D') then

			if	(ie_conta_financ_ativa_w = 'A') then
				begin

				insert	into fluxo_caixa
					(cd_estabelecimento,
					dt_referencia,
					cd_conta_financ,
					ie_classif_fluxo,
					dt_atualizacao, 
					nm_usuario, 
					vl_fluxo, 
					ie_origem, 
					ie_periodo,
					ie_integracao,
					cd_empresa,
					ie_contrato,
					cd_moeda)
				values	(cd_estabelecimento_p,
					dt_referencia_w,
					vet01.cd_conta_financ,
					vet01.ie_classif_fluxo,
					sysdate, 
					nm_usuario_p, 
					nvl(decode (vet01.cd_moeda, cd_moeda_empresa_w, nvl(vet01.vl_fluxo,0), nvl(OBTER_COTACAO_MOEDA_TITULO(vet01.cd_moeda, nvl(vet01.nr_titulo_receber, vet01.nr_titulo_pagar), vet01.ie_integracao),0) * vet01.vl_saldo_estrangeiro), 0), 
					'I', 
					'D',
					vet01.ie_integracao,
					cd_empresa_p,
					vet01.ie_contrato,
					cd_moeda_empresa_w);
				exception
					when dup_val_on_index then
						update	fluxo_caixa 
						set	vl_fluxo		= vl_fluxo +  nvl(decode (vet01.cd_moeda, cd_moeda_empresa_w, nvl(vet01.vl_fluxo,0), nvl(OBTER_COTACAO_MOEDA_TITULO(vet01.cd_moeda, nvl(vet01.nr_titulo_receber, vet01.nr_titulo_pagar), vet01.ie_integracao),0) * vet01.vl_saldo_estrangeiro),0)
						where	cd_estabelecimento	= cd_estabelecimento_p
						and	cd_conta_financ		= vet01.cd_conta_financ
						and	dt_referencia		= dt_referencia_w
						and	ie_periodo		= 'D'
						and	ie_classif_fluxo	= vet01.ie_classif_fluxo
						and	ie_integracao		= vet01.ie_integracao
						and	cd_empresa		= cd_empresa_p;
					when others then
						wheb_mensagem_pck.exibir_mensagem_abort(176731,'CONTA_FINANC_W=' || vet01.cd_conta_financ || ';' ||
												'SQLERRM=' || sqlerrm);
				end;

			end if;
		end if;
	end if;
end loop;
close c01;

/* francisco - os 61295 - trazer todas contas financeiras */
open c02;
loop
fetch c02 into
	vet02;
exit when c02%notfound;
	
	insert into fluxo_caixa
		(cd_estabelecimento,
		dt_referencia, 
		cd_conta_financ, 
		ie_classif_fluxo,
		dt_atualizacao, 
		nm_usuario, 
		vl_fluxo, 
		ie_origem, 
		ie_periodo, 
		ie_integracao,
		cd_empresa,
		cd_moeda)
	values	(cd_estabelecimento_p,
		dt_inicio_w,
		vet02.cd_conta_financ,
		'R',
		sysdate,
		nm_usuario_p,
		0,
		'I',
		ie_periodo_p,
		'X',
		cd_empresa_p,
		cd_moeda_empresa_w);
end loop;
close c02;


open c03;
loop
fetch c03 into
	vet03;
exit when c03%notfound;

		dt_referencia_w := vet03.dt_referencia;
	if	(vet03.dt_referencia < dt_inicio_w) then
		if	(ie_acumular_vencidos_w = 'S') then
			dt_referencia_w	:= dt_inicio_w;
		end if;
	end if;
		
	if	(vet03.cd_conta_financ <> 0) then
		
		select	max(ie_oper_fluxo),
			decode(ie_somente_ativa_p,'S',max(ie_situacao),'A')
		into	ie_operacao_w,
			ie_conta_financ_ativa_w
		from	conta_financeira
		where	cd_conta_financ	= vet03.cd_conta_financ;
		
		if	(ie_operacao_p = 'A') or
			(ie_operacao_p = 'D' and ie_operacao_w <> 'S') or
			(ie_operacao_p = 'S' and ie_operacao_w <> 'D') then

			if	(ie_conta_financ_ativa_w = 'A') then
				begin
				
				insert	into fluxo_caixa
					(cd_estabelecimento,
					dt_referencia,
					cd_conta_financ,
					ie_classif_fluxo,
					dt_atualizacao, 
					nm_usuario, 
					vl_fluxo, 
					ie_origem, 
					ie_periodo,
					ie_integracao,
					cd_empresa,
					ie_contrato,
					cd_moeda)
				values	(cd_estabelecimento_p,
					dt_referencia_w,
					vet03.cd_conta_financ,
					vet03.ie_classif_fluxo,
					sysdate, 
					nm_usuario_p, 
					nvl(decode (vet03.cd_moeda, cd_moeda_empresa_w, nvl(vet03.vl_fluxo,0), nvl(OBTER_COTACAO_MOEDA_OC(vet03.cd_moeda, vet03.nr_ordem_compra),0) * vet03.vl_saldo_estrangeiro), 0),  
					'I', 
					'D',
					vet03.ie_integracao,
					cd_empresa_p,
					vet03.ie_contrato,
					cd_moeda_empresa_w);
				exception
					when dup_val_on_index then
						update	fluxo_caixa 
						set	vl_fluxo		= vl_fluxo + nvl(decode (vet03.cd_moeda, cd_moeda_empresa_w, nvl(vet03.vl_fluxo,0), nvl(OBTER_COTACAO_MOEDA_OC(vet03.cd_moeda, vet03.nr_ordem_compra),0) * vet03.vl_saldo_estrangeiro),0)
						where	cd_estabelecimento	= cd_estabelecimento_p
						and	cd_conta_financ		= vet03.cd_conta_financ
						and	dt_referencia		= dt_referencia_w
						and	ie_periodo		= 'D'
						and	ie_classif_fluxo	= vet03.ie_classif_fluxo
						and	ie_integracao		= vet03.ie_integracao
						and	cd_empresa		= cd_empresa_p;
					when others then
						wheb_mensagem_pck.exibir_mensagem_abort(176731,'CONTA_FINANC_W=' || vet03.cd_conta_financ || ';' ||
												'SQLERRM=' || sqlerrm);
				end;

			end if;
		end if;
	end if;
end loop;
close c03;

if	(ie_contrato_p = 'S') then

	if	(nvl(ie_forma_calculo_contrato_w,'R') = 'M') and
		(nvl(ie_tipo_fluxo_p,'R') = 'V') then

		gerar_fluxo_previsto_contrato(	cd_estabelecimento_p,
						nm_usuario_p,
						cd_empresa_p,
						ie_restringe_estab_p,
						dt_inicio_p,
						dt_final_p,
						ie_operacao_p,
						ie_somente_ativa_p);

	else

		gerar_fluxo_caixa_contrato(	cd_estabelecimento_p,
						nm_usuario_p,
						cd_empresa_p,
						ie_restringe_estab_p,
						dt_inicio_p,
						dt_final_p,
						ie_operacao_p,
						ie_somente_ativa_p);

	end if;

	commit;
end if;

if	(ie_fluxo_especial_p = 'S') then
	gerar_fluxo_caixa_esp(	cd_estabelecimento_p,
				dt_inicio_w,
				dt_final_p,
				nm_usuario_p,
				cd_empresa_p,
				ie_restringe_estab_p,
				ie_operacao_p,
				ie_somente_ativa_p,
				'R');
	commit;
end if;

if	(ie_agenda_p = 'S') then
	gerar_fluxo_caixa_agenda(cd_empresa_p,
				cd_estabelecimento_p,
				ie_periodo_p,
				Dt_inicio_p,
				dt_final_p,
				nm_usuario_p,
				ie_operacao_p,
				ie_somente_ativa_p);
	commit;
end if;


/* acumular os valores di�rios para o mes */
delete from fluxo_caixa
where 	dt_referencia		between dt_inicio_mes_w and dt_final_fim_mes_w
and 	ie_classif_fluxo 	= 'R'
and	cd_empresa		= cd_empresa_p
and	(ie_restringe_estab_p = 'N' or
	(ie_restringe_estab_p = 'E' and cd_estabelecimento = cd_estabelecimento_p) or
	(ie_restringe_estab_p = 'S' and (cd_estabelecimento = cd_estabelecimento_p or obter_estab_financeiro(cd_estabelecimento) = cd_estabelecimento_p)))
and	ie_origem 		<> 'D'
and	ie_periodo 		= 'M';



begin
insert into fluxo_caixa(cd_estabelecimento,
			dt_referencia,
			cd_conta_financ,
			ie_classif_fluxo,
			dt_atualizacao,
			nm_usuario,
			vl_fluxo,
			ie_origem,
			ie_periodo,
			ie_integracao,
			cd_empresa,
			cd_moeda)
select			cd_estabelecimento,
			last_day(trunc(dt_referencia,'month')),
			cd_conta_financ,
			'R',
			sysdate,
			nm_usuario_p,
			sum(vl_fluxo),
			'X',
			'M',
			ie_integracao,
			cd_empresa,
			nvl(cd_moeda,cd_moeda_empresa_w)
from			Fluxo_Caixa
where 			dt_referencia between trunc(dt_inicio_w, 'dd') and last_day(dt_final_p)
and 			ie_classif_fluxo	= 'R'
and			cd_estabelecimento	= cd_estabelecimento_p
and			cd_empresa		= cd_empresa_p
group by 		cd_estabelecimento,
			last_day(trunc(dt_referencia,'month')),
			cd_conta_financ,
			ie_classif_fluxo,
			ie_periodo,
			cd_empresa,
			ie_integracao,
			nvl(cd_moeda,cd_moeda_empresa_w);
exception
when dup_val_on_index then
	wheb_mensagem_pck.exibir_mensagem_abort(176734,	'DT_INICIO_P=' || dt_inicio_p || ';' ||
							'DT_FINAL_P=' || dt_final_p || ';' ||
							'CD_ESTABELECIMENTO_P=' || cd_estabelecimento_p || ';' ||
							'CD_EMPRESA_P=' || cd_empresa_p);
end;

calcular_fluxo_caixa(	cd_estabelecimento_p,
			vl_saldo_anterior_p,
			dt_inicio_w,
			dt_final_p,
			ie_periodo_p,
			'R',
			nm_usuario_p,
			cd_empresa_p,
			ie_restringe_estab_p,
			ie_passado_real_p,
			ie_somente_ativa_p,
			nr_seq_regra_p);

commit;

gerar_fluxo_caixa_agrup(cd_estabelecimento_p,
			dt_inicio_w,
			dt_final_p,
			ie_periodo_p,
			'R',
			nm_usuario_p,
			cd_empresa_p,
			ie_operacao_p,
			ie_restringe_estab_p,
			ie_somente_ativa_p);

commit;

gerar_fluxo_caixa_externo(dt_inicio_w,
			dt_final_p,
			cd_estabelecimento_p,
			cd_empresa_p,
			ie_periodo_p,
			'R',
			ie_restringe_estab_p,
			nm_usuario_p,
			ie_somente_ativa_p);

commit;

end gerar_fluxo_caixa_doc_real;
/
