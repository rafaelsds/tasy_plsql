create or replace
procedure pls_vincular_grupo_operador
			(	nr_seq_operador_p		number,
				nr_seq_grupo_p			number,
				nm_usuario_p			varchar2) is

nr_seq_grupo_atend_w		number(3);
ds_historico_w			varchar2(4000);
nm_operador_w			varchar2(255);
ds_grupo_w			varchar2(255);
ds_grupo_novo_w			varchar2(255);
ie_controle_w			number(1);

begin
begin
	select	nr_seq_grupo_atend,
		substr(obter_nome_pf(cd_pessoa_fisica),1,255)
	into	nr_seq_grupo_atend_w,
		nm_operador_w
	from	pls_operador
	where	nr_sequencia	= nr_seq_operador_p;
exception
when others then
	ie_controle_w := 1;
end;

if	(ie_controle_w is null) then
	if 	(nr_seq_grupo_atend_w is not null) then
		begin
			select  substr(ds_grupo,1,255)
			into 	ds_grupo_w
			from    pls_grupo_atendimento
			where 	nr_sequencia 	= nr_seq_grupo_atend_w;
		exception
		when others then
			ds_grupo_w 	:= '';
		end;

		begin
			select  substr(ds_grupo,1,255)
			into 	ds_grupo_novo_w
			from    pls_grupo_atendimento
			where 	nr_sequencia 	= nr_seq_grupo_p;
		exception
		when others then
			ds_grupo_novo_w 	:= '';
		end;

		ds_historico_w := substr('O usu�rio '||nm_usuario_p||' alterou o grupo do operador '||nm_operador_w||' de '||upper(ds_grupo_w)||' para '||upper(ds_grupo_novo_w),1,4000);

		insert into pls_atend_hist_operador
			(nr_sequencia, nr_seq_operador, ie_tipo_historico,
			 ds_historico, dt_atualizacao, nm_usuario,
			 dt_atualizacao_nrec, nm_usuario_nrec, dt_historico)
		values	(pls_atend_hist_operador_seq.nextval, nr_seq_operador_p, 2,
			 ds_historico_w, sysdate, nm_usuario_p,
			 sysdate, nm_usuario_p, sysdate);

	else
		begin
			select  substr(ds_grupo,1,255)
			into 	ds_grupo_novo_w
			from    pls_grupo_atendimento
			where 	nr_sequencia 	= nr_seq_grupo_p;
		exception
		when others then
			ds_grupo_novo_w 	:= '';
		end;

		ds_historico_w := substr('O usu�rio '||nm_usuario_p||' cadastrou o grupo '||upper(ds_grupo_novo_w)||' para o operador '||nm_operador_w,1,4000);

		insert into pls_atend_hist_operador
			(nr_sequencia, nr_seq_operador, ie_tipo_historico,
			 ds_historico, dt_atualizacao, nm_usuario,
			 dt_atualizacao_nrec, nm_usuario_nrec, dt_historico)
		values	(pls_atend_hist_operador_seq.nextval, nr_seq_operador_p, 1,
			 ds_historico_w, sysdate, nm_usuario_p,
			 sysdate, nm_usuario_p, sysdate);
	end if;
	

	update	pls_operador
	set	nr_seq_grupo_atend	= nr_seq_grupo_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where 	nr_sequencia		= nr_seq_operador_p;
	
	commit;
end if;

end pls_vincular_grupo_operador;
/