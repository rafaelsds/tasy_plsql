create or replace
procedure alterar_quantidade_comp_kit(	nr_seq_kit_estoque_p	number,
					nr_sequencia_p		number,
					qt_material_p		number,
					nm_usuario_p		Varchar2) is

cd_material_w	number(6);
qt_material_w	number(13,4);
nr_sequencia_w	number(5);
ds_historico_w	varchar2(2000);
ds_titulo_w	varchar2(1000);

begin

select	cd_material,
	qt_material
into	cd_material_w,
	qt_material_w
from	kit_estoque_comp
where	nr_seq_kit_estoque = nr_seq_kit_estoque_p
and	nr_sequencia = nr_sequencia_p;

update	kit_estoque_comp
set	qt_material = qt_material_p,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p
where	nr_seq_kit_estoque = nr_seq_kit_estoque_p
and	nr_sequencia = nr_sequencia_p;

select	nvl(max(nr_sequencia), 0) + 1
into	nr_sequencia_w
from	kit_estoque_comp
where	nr_seq_kit_estoque = nr_seq_kit_estoque_p;

insert into kit_estoque_comp(
	nr_seq_kit_estoque,
	nr_sequencia,
	cd_material,
	dt_atualizacao,
	nm_usuario,
	qt_material,
	ie_gerado_barras)
values(
	nr_seq_kit_estoque_p,
	nr_sequencia_w,
	cd_material_w,
	sysdate,
	nm_usuario_p,
	qt_material_w - qt_material_p,
	'N');
	
ds_titulo_w	:=	wheb_mensagem_pck.get_texto(315076);
ds_historico_w	:=	substr(wheb_mensagem_pck.get_texto(315077)||' ' || cd_material_w || ' - ' || obter_desc_material(cd_material_w) ||
			' '|| wheb_mensagem_pck.get_texto(310560)|| ' ' || qt_material_w ||' ' || wheb_mensagem_pck.get_texto(280393)||' ' || qt_material_p || ' '||wheb_mensagem_pck.get_texto(315075),1,1000);
			
gravar_kit_estoque_hist(nr_seq_kit_estoque_p, ds_titulo_w, ds_historico_w, 'S', nm_usuario_p);

commit;

end alterar_quantidade_comp_kit;
/
