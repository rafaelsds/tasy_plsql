create or replace
procedure pls_atualiza_valor_mat_aut
			(	nr_seq_material_p	Number,
				ie_tipo_p		Varchar2,
				nm_usuario_p		Varchar2) is

/*
ie_tipo_p	
   COMPL - 	Complemento de pedido de autoriza��o
   A -	Autoriza��o
   R - 	Requisi��o
   AA- 	An�lise de Autoriza��o
   AR-	An�lise de Requisi��a
   AG-  Anexo Guia
*/				

cd_estabelecimento_w		Number(10);
nr_seq_prestador_w		Number(10);
dt_solicitacao_w		Date;
nr_seq_material_w		Number(10);
ie_tipo_despesa_w		Varchar2(1);
ie_origem_preco_w		Number(2);
ie_tipo_tabela_w		Varchar2(1)	:= '';
nr_seq_outorgante_w		Number(10);
nr_seq_segurado_w		Number(10);
vl_material_w			Number(15,4);
dt_ult_vigencia_w		Date;
nr_seq_material_preco_w		Number(10);
vl_material_simpro_w		Number(15,4);
vl_material_brasindice_w	Number(15,2);
vl_material_tabela_w		Number(15,2);
nr_seq_regra_w			Number(10);
nr_seq_guia_w			Number(10);
nr_seq_plano_w			Number(10);
nr_seq_prest_fornec_w		Number(10);
cd_material_w			Number(10);
vl_servico_w			Varchar2(14);
nr_seq_requisicao_w		Number(10);
nr_seq_tipo_acomodacao_w	Number(10);
nr_seq_categoria_w		Number(10);
nr_seq_tipo_atendimento_w	Number(10);
ie_tipo_atend_tiss_w		Varchar2(2);
nr_seq_clinica_w		Number(10);
ie_tipo_guia_w			Varchar2(2); 
qt_autorizacao_espec_w		Number(10);
ie_autorizacao_espec_w		Varchar2(10)	:= 'N';
nr_seq_plano_espec_w		Number(10);
cd_guia_referencia_w		Varchar2(20);
vl_item_w			number(15,2);
dados_regra_preco_material_w	pls_cta_valorizacao_pck.dados_regra_preco_material;
ie_prestador_preco_w		pls_param_requisicao.ie_prestador_preco%type;
nr_seq_uni_exec_w		pls_guia_plano.nr_seq_uni_exec%type;
				
begin
/*Gerar pre�o para os servi�os do  PTU - Pedido de Complemento de Autoriza��o [00505] */
if	(ie_tipo_p	= 'COMPL') then
	/*Obter dados do complemento*/
	select	a.nr_seq_guia,
		b.cd_servico,
		b.nr_seq_prest_fornec
	into	nr_seq_guia_w,
		cd_material_w,
		nr_seq_prest_fornec_w
	from	ptu_pedido_compl_aut_serv	b,
		ptu_pedido_compl_aut		a
	where	b.nr_sequencia	= nr_seq_material_p
	and	b.nr_seq_pedido	= a.nr_sequencia;
	
	/*Obter dados guia*/
	select	cd_estabelecimento,
		nr_seq_prestador,
		dt_solicitacao,
		nr_seq_segurado,
		nr_seq_plano,
		nr_seq_tipo_acomodacao,
		ie_tipo_atend_tiss,
		nr_seq_clinica,
		ie_tipo_guia,
		nr_seq_uni_exec
	into	cd_estabelecimento_w,
		nr_seq_prestador_w,
		dt_solicitacao_w,
		nr_seq_segurado_w,
		nr_seq_plano_w,
		nr_seq_tipo_acomodacao_w,
		ie_tipo_atend_tiss_w,
		nr_seq_clinica_w,
		ie_tipo_guia_w,
		nr_seq_uni_exec_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_w;
	
	/*Obter sequencia da autoriza��o especial*/
	select	max(nr_sequencia)
	into	nr_seq_plano_espec_w
	from	pls_guia_plano
	where	cd_guia	= cd_guia_referencia_w;
	/*Obter se existe material especial*/
	select	count(*)
	into	qt_autorizacao_espec_w
	from	pls_solic_lib_mat_med	a,
		pls_guia_plano		b
	where	a.nr_seq_guia	= b.nr_sequencia
	and	b.nr_sequencia	= nr_seq_plano_espec_w;

	if	(qt_autorizacao_espec_w	> 0) then
		ie_autorizacao_espec_w	:= 'S';
	end if;

	/* Obter a categoria do tipo de acomoda��o */ 
	if	(nr_seq_tipo_acomodacao_w is not null) then
		select	max(nr_seq_categoria) 
		into	nr_seq_categoria_w
		from	pls_regra_categoria 
		where	nr_seq_tipo_acomodacao	= nr_seq_tipo_acomodacao_w;
	end if; 
	
	begin
	select	nr_sequencia
	into	nr_seq_tipo_atendimento_w
	from	pls_tipo_atendimento
	where	somente_numero(cd_tiss)	= ie_tipo_atend_tiss_w;
	exception
	when others then
		nr_seq_tipo_atendimento_w	:= null;
	end;

	/*Obter dados do plano*/
	begin
	select	nr_seq_outorgante
	into	nr_seq_outorgante_w
	from	pls_plano
	where	nr_sequencia	= nr_seq_plano_w;
	exception
		when others then
		nr_seq_outorgante_w	:= 0;
	end;
	
	/*Obter dados do material*/
	select	ie_tipo_despesa,
		nr_sequencia
	into	ie_tipo_despesa_w,
		nr_seq_material_w
	from	pls_material
	where	cd_material_ops	= cd_material_w;
	
	pls_define_preco_material(	cd_estabelecimento_w, nr_seq_prestador_w, dt_solicitacao_w,
					nr_seq_material_w, 4, ie_tipo_despesa_w,
					null, 'A', nr_seq_outorgante_w,
					nr_seq_segurado_w, '', nr_seq_prest_fornec_w, 
					nr_seq_categoria_w , nr_seq_tipo_acomodacao_w, nr_seq_tipo_atendimento_w,
					nr_seq_clinica_w,ie_tipo_guia_w, ie_autorizacao_espec_w,
					nr_seq_plano_espec_w, 'S', '', '', '',
					null,null,null,null,null,
					null,null,null,null,null,
					null,null,null,null,null,
					vl_material_w, dt_ult_vigencia_w, nr_seq_material_preco_w, 
					vl_material_simpro_w, vl_material_brasindice_w, vl_material_tabela_w, 
					dados_regra_preco_material_w, nr_seq_uni_exec_w);
	nr_seq_regra_w	:= dados_regra_preco_material_w.nr_sequencia;
	
	update	ptu_pedido_compl_aut_serv
	set	vl_servico		= vl_material_w
	where	nr_sequencia		= nr_seq_material_p;
elsif	(ie_tipo_p	in ('A','AA','AG')) then
	begin
	if	(ie_tipo_p = 'A') then
		select	nr_seq_guia,
			nr_seq_material,
			nr_seq_prest_fornec
		into	nr_seq_guia_w,
			nr_seq_material_w,
			nr_seq_prest_fornec_w
		from	pls_guia_plano_mat
		where	nr_sequencia	= nr_seq_material_p;
	elsif	(ie_tipo_p = 'AA') then
		select	a.nr_seq_guia,
			b.nr_seq_material,
			b.nr_seq_prest_fornec
		into	nr_seq_guia_w,
			nr_seq_material_w,
			nr_seq_prest_fornec_w
		from	pls_auditoria_item	b,
			pls_auditoria		a			
		where	b.nr_sequencia	= nr_seq_material_p
		and	a.nr_sequencia	= b.nr_seq_auditoria;
	elsif	(ie_tipo_p = 'AG') then
		select	a.nr_seq_guia,
			b.nr_seq_material
		into	nr_seq_guia_w,
			nr_seq_material_w
		from	pls_lote_anexo_guias_aut a,
			pls_lote_anexo_mat_aut b
		where	a.nr_sequencia	= b.nr_seq_lote_anexo_guia
		and	b.nr_sequencia	= nr_seq_material_p;	
	end if;	
	exception
	when others then
		nr_seq_guia_w := null;
	end;
	
	if	(nr_seq_guia_w is not null) then
		select	cd_estabelecimento,
			nr_seq_prestador,
			dt_solicitacao,
			nr_seq_segurado,
			nr_seq_plano,
			nr_seq_tipo_acomodacao,
			ie_tipo_atend_tiss,
			nr_seq_clinica,
			ie_tipo_guia,
			nr_seq_uni_exec
		into	cd_estabelecimento_w,
			nr_seq_prestador_w,
			dt_solicitacao_w,
			nr_seq_segurado_w,
			nr_seq_plano_w,
			nr_seq_tipo_acomodacao_w,
			ie_tipo_atend_tiss_w,
			nr_seq_clinica_w,
			ie_tipo_guia_w,
			nr_seq_uni_exec_w
		from	pls_guia_plano
		where	nr_sequencia	= nr_seq_guia_w;
		
		/*Obter sequencia da autoriza��o especial*/
		select	max(nr_sequencia)
		into	nr_seq_plano_espec_w
		from	pls_guia_plano
		where	cd_guia	= cd_guia_referencia_w;
		/*Obter se existe material especial*/
		select	count(*)
		into	qt_autorizacao_espec_w
		from	pls_solic_lib_mat_med	a,
			pls_guia_plano		b
		where	a.nr_seq_guia	= b.nr_sequencia
		and	b.nr_sequencia	= nr_seq_plano_espec_w;

		if	(qt_autorizacao_espec_w	> 0) then
			ie_autorizacao_espec_w	:= 'S';
		end if;
		
		/* Obter a categoria do tipo de acomoda��o */ 
		if	(nr_seq_tipo_acomodacao_w is not null) then
			select	max(nr_seq_categoria) 
			into	nr_seq_categoria_w
			from	pls_regra_categoria 
			where	nr_seq_tipo_acomodacao	= nr_seq_tipo_acomodacao_w; 
		end if; 
		
		begin
		select	nr_sequencia
		into	nr_seq_tipo_atendimento_w
		from	pls_tipo_atendimento
		where	somente_numero(cd_tiss)	= ie_tipo_atend_tiss_w;
		exception
		when others then
			nr_seq_tipo_atendimento_w	:= null;
		end;


		begin
		select	nr_seq_outorgante
		into	nr_seq_outorgante_w
		from	pls_plano
		where	nr_sequencia	= nr_seq_plano_w;
		exception
			when others then
			nr_seq_outorgante_w	:= 0;
		end;

		select	ie_tipo_despesa
		into	ie_tipo_despesa_w
		from	pls_material
		where	nr_sequencia	= nr_seq_material_w;

		pls_define_preco_material(	cd_estabelecimento_w, nr_seq_prestador_w, dt_solicitacao_w,
						nr_seq_material_w, 4, ie_tipo_despesa_w,
						null, 'A', nr_seq_outorgante_w,
						nr_seq_segurado_w, '', nr_seq_prest_fornec_w, 
						nr_seq_categoria_w , nr_seq_tipo_acomodacao_w, nr_seq_tipo_atendimento_w,
						nr_seq_clinica_w,ie_tipo_guia_w, ie_autorizacao_espec_w,
						nr_seq_plano_espec_w, 'S', '', '', '',
						null,null,null,null,null,
						null,null,null,null,null,
						null,null,null,null,null,
						vl_material_w, dt_ult_vigencia_w, nr_seq_material_preco_w, 
						vl_material_simpro_w, vl_material_brasindice_w, vl_material_tabela_w, 
						dados_regra_preco_material_w, nr_seq_uni_exec_w);
		nr_seq_regra_w	:= dados_regra_preco_material_w.nr_sequencia;
		if	(ie_tipo_p = 'A') then
			update	pls_guia_plano_mat
			set	vl_material	= vl_material_w,
				nr_seq_regra	= nr_seq_regra_w
			where	nr_sequencia	= nr_seq_material_p;
		elsif	(ie_tipo_p = 'AA') then
			update	pls_auditoria_item
			set	vl_item		= vl_material_w,
				nr_seq_regra_vl_mat = nr_seq_regra_w
			where	nr_sequencia	= nr_seq_material_p;
		elsif	(ie_tipo_p = 'AG') then
			update	pls_lote_anexo_mat_aut
			set	vl_unit_material_solic	= vl_material_w,
				vl_unit_material_aut	= vl_material_w
			where	nr_sequencia	= nr_seq_material_p;
		end if;
	end if;
elsif	(ie_tipo_p	in ('R','AR')) then
	
	begin
	if	(ie_tipo_p = 'R') then
		select	nr_seq_requisicao,
			nr_seq_material,
			nr_seq_prest_fornec
		into	nr_seq_requisicao_w,
			nr_seq_material_w,
			nr_seq_prest_fornec_w
		from	pls_requisicao_mat
		where	nr_sequencia	= nr_seq_material_p;
	elsif	(ie_tipo_p = 'AR') then
		select	a.nr_seq_requisicao,
			b.nr_seq_material,
			b.nr_seq_prest_fornec,
			b.vl_item
		into	nr_seq_requisicao_w,
			nr_seq_material_w,
			nr_seq_prest_fornec_w,
			vl_item_w
		from	pls_auditoria_item	b,
			pls_auditoria		a			
		where	b.nr_sequencia	= nr_seq_material_p
		and	a.nr_sequencia	= b.nr_seq_auditoria;
	end if;	
	exception
	when others then
		nr_seq_requisicao_w := null;
	end;

	if	(nr_seq_requisicao_w is not null) then
		select	cd_estabelecimento,
			nr_seq_prestador,
			dt_requisicao,
			nr_seq_segurado,
			nr_seq_plano,
			nr_seq_tipo_acomodacao,
			ie_tipo_atendimento,
			nr_seq_clinica,
			ie_tipo_guia,
			nr_seq_uni_exec
		into	cd_estabelecimento_w,
			nr_seq_prestador_w,
			dt_solicitacao_w,
			nr_seq_segurado_w,
			nr_seq_plano_w,
			nr_seq_tipo_acomodacao_w,
			ie_tipo_atend_tiss_w,
			nr_seq_clinica_w,
			ie_tipo_guia_w,
			nr_seq_uni_exec_w
		from	pls_requisicao
		where	nr_sequencia	= nr_seq_requisicao_w;
		
			/* Obter a categoria do tipo de acomoda��o */ 
		if	(nr_seq_tipo_acomodacao_w is not null) then
			select	max(nr_seq_categoria) 
			into	nr_seq_categoria_w
			from	pls_regra_categoria 
			where	nr_seq_tipo_acomodacao	= nr_seq_tipo_acomodacao_w; 
		end if; 
		
		begin
		select	nr_sequencia
		into	nr_seq_tipo_atendimento_w
		from	pls_tipo_atendimento
		where	somente_numero(cd_tiss)	= ie_tipo_atend_tiss_w;
		exception
		when others then
			nr_seq_tipo_atendimento_w	:= null;
		end;

		begin
		select	nr_seq_outorgante
		into	nr_seq_outorgante_w
		from	pls_plano
		where	nr_sequencia	= nr_seq_plano_w;
		exception
			when others then
			nr_seq_outorgante_w	:= 0;
		end;

		select	ie_tipo_despesa
		into	ie_tipo_despesa_w
		from	pls_material
		where	nr_sequencia	= nr_seq_material_w;

		select	nvl(max(ie_prestador_preco),'N')
		into	ie_prestador_preco_w
		from	pls_param_requisicao
		where	cd_estabelecimento = cd_estabelecimento_w;
		
		if (ie_prestador_preco_w = 'S') then
			select	nvl(nr_seq_prestador_exec, nr_seq_prestador)
			into	nr_seq_prestador_w
			from	pls_requisicao
			where	nr_sequencia = nr_seq_requisicao_w;
		end if;
		
		pls_define_preco_material(	cd_estabelecimento_w, nr_seq_prestador_w, dt_solicitacao_w,
						nr_seq_material_w, 4, ie_tipo_despesa_w,
						null, 'A', nr_seq_outorgante_w,
						nr_seq_segurado_w, '', nr_seq_prest_fornec_w, 
						nr_seq_categoria_w , nr_seq_tipo_acomodacao_w, nr_seq_tipo_atendimento_w,
						nr_seq_clinica_w,ie_tipo_guia_w, null,
						null, 'S', '', '', '',
						null,null,null,null,null,
						null,null,null,null,null,
						null,null,null,null,null,
						vl_material_w, dt_ult_vigencia_w, nr_seq_material_preco_w, 
						vl_material_simpro_w, vl_material_brasindice_w, vl_material_tabela_w, 
						dados_regra_preco_material_w, nr_seq_uni_exec_w);
		nr_seq_regra_w	:= dados_regra_preco_material_w.nr_sequencia;				
		if	(ie_tipo_p = 'R') then
			update	pls_requisicao_mat
			set	vl_material	= vl_material_w,
				nr_seq_regra	= nr_seq_regra_w
			where	nr_sequencia	= nr_seq_material_p;
		elsif	(ie_tipo_p = 'AR') and (nvl(vl_item_w,0) = 0) then
			update	pls_auditoria_item
			set	vl_item		= vl_material_w,
				nr_seq_regra_vl_mat = nr_seq_regra_w
			where	nr_sequencia	= nr_seq_material_p;
		end if;
	end if;
end if;
			
commit;

end pls_atualiza_valor_mat_aut;
/
