create or replace
procedure pls_finalizar_requisicao_web (nr_seq_requisicao_p	pls_requisicao.nr_sequencia%Type,
					nm_usuario_p		varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: 	Setar o campo ie_req_web_finalizada para "S" quando a guia for finalizada no portal
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [ X ] Portal [ ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin
	update pls_requisicao
	set ie_req_web_finalizada = 'S'
	where nr_sequencia = nr_seq_requisicao_p;

	commit;

	pls_gerar_alerta_evento(8,
				null,
				nr_seq_requisicao_p,
				null,
				nm_usuario_p);

end pls_finalizar_requisicao_web;
/
