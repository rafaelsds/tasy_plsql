create or replace
procedure grava_log_prepedido_me(
				nm_usuario_p			varchar2,
				nr_prepedido_me_p		Number,
				id_fornecedor_me_p		Varchar2,
				ds_observacao_p		Varchar2,
				nr_solic_compra_p		Number,
				nr_ordem_compra_p		Number,
				ds_tag_p		varchar2,
				nr_sequencia_p	out	Number,
				ie_status_p		out	varchar2) as

ie_status_w			Varchar2(3);
ds_observacao_w		Varchar2(255);
nr_ordem_compra_w		Number(10);
nr_sequencia_w		Number(10);

begin
nr_ordem_compra_w	:= nr_ordem_compra_p;
ds_observacao_w	:= ds_observacao_p;
ie_status_w		:= 1;

if	(ds_observacao_p is not null) then
	ie_status_w		:= 104;
	ds_observacao_w	:= ds_observacao_p;
	nr_ordem_compra_w	:= 0;
end if;

select	log_prepedido_me_seq.nextval
into	nr_sequencia_w
from	dual;

insert into log_prepedido_me(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_solic_compra,
	nr_prepedido,
	ds_tag,
	id_fornecedor,
	nr_ordem_compra,
	ie_status,
	ds_observacao)
values(nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_solic_compra_p,
	nr_prepedido_me_p,
	ds_tag_p,
	id_fornecedor_me_p,
	decode(nr_ordem_compra_w, 0, null, nr_ordem_compra_w),
	ie_status_w,
	ds_observacao_p);

nr_sequencia_p	:= nr_sequencia_w;
ie_status_p		:= ie_status_w;
commit;
end grava_log_prepedido_me;
/