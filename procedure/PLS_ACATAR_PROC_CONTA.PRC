create or replace
procedure pls_acatar_proc_conta 
		(nr_seq_proc_conta_p	in	pls_processo_conta.nr_sequencia%type,
		 nm_usuario_p		in	usuario.nm_usuario%type) is
		 
vl_conta_w	number(15,2);
		 
begin

vl_conta_w	:= nvl(pls_conta_processo_obter_valor(nr_seq_proc_conta_p),0);
	
update	pls_processo_conta
set	ie_status_conta		= 'L',
	ie_status_pagamento	= 'P',
	vl_pendente		= 0,
	vl_ressarcir		= vl_conta_w
where	nr_sequencia	= nr_seq_proc_conta_p;

commit;

end pls_acatar_proc_conta;
/
