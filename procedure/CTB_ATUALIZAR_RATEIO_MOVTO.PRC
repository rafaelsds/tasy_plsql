create or replace
procedure ctb_atualizar_rateio_movto(	nr_sequencia_p	number,	
					nm_usuario_p	Varchar2,
                    ie_commit_p varchar2 default 'S') is 

nr_sequencia_w		number(10);
qt_movto_w		number(10);
pr_diferenca_w		number(15,4);
pr_rateio_w		number(15,4);
pr_total_rateio_w	number(15,4)	:= 0;
vl_movto_w		number(15,2);
vl_movimento_w		number(15,2);

cursor c01 is
select	a.nr_sequencia,
	a.vl_movimento
from	ctb_movto_centro_custo a
where	a.nr_seq_movimento	= nr_sequencia_p
order by 1;

begin

/* Verifica se existem rateios por centro de custo deste movimento */
select	count(*)
into	qt_movto_w
from	ctb_movto_centro_custo
where	nr_seq_movimento	= nr_sequencia_p;

/* Valor total do movimento contabil */
select	nvl(max(vl_movimento),0)
into	vl_movimento_w
from	ctb_movimento
where	nr_sequencia	= nr_sequencia_p;

if	(qt_movto_w > 0) and
	(vl_movimento_w <> 0) then
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w,
		vl_movto_w;
	exit when C01%notfound;
		begin
		pr_rateio_w		:= dividir((vl_movto_w * 100),vl_movimento_w);
		
		update	ctb_movto_centro_custo
		set	pr_rateio 	= pr_rateio_w
		where	nr_sequencia	= nr_sequencia_w;
		
		pr_total_rateio_w	:= pr_total_rateio_w + pr_rateio_w;
		
		end;
	end loop;
	close C01;
	pr_diferenca_w		:= abs(pr_total_rateio_w - 100);
	
	if	(pr_total_rateio_w > 100) then
		update	ctb_movto_centro_custo
		set	pr_rateio	= pr_rateio - pr_diferenca_w
		where	nr_sequencia	= nr_sequencia_w;
	elsif	(pr_total_rateio_w < 100) then
		update	ctb_movto_centro_custo
		set	pr_rateio	= pr_rateio + pr_diferenca_w
		where	nr_sequencia	= nr_sequencia_w;
	end if;
	
end if;

if (ie_commit_p = 'S') then
commit;
end if;

end ctb_atualizar_rateio_movto;
/
