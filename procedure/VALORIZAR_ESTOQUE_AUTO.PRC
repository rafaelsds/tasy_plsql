create or replace
procedure valorizar_estoque_auto(dt_mesano_referencia_p		date,
								 cd_estabelecimento_p		number) IS
		
dt_mesano_referencia_w		date;
dt_referencia_w			date;
dt_mesano_vigente_w		date;
ds_log_w			varchar2(4000);
ie_virada_w			varchar2(1);
erro_w				varchar2(255);
cd_estabelecimento_w 		estabelecimento.cd_estabelecimento%type;
qt_itens_negativos_w		number(10);

cursor C00 is
select	cd_estabelecimento
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p
union all
select	a.cd_estabelecimento
from	estabelecimento a
where	a.ie_situacao = 'A'
and	cd_estabelecimento_p = 0
and	exists	(select 1
		from	parametro_estoque x
		where	x.cd_estabelecimento = a.cd_estabelecimento
		and	x.ie_situacao = 'A');

BEGIN
dt_mesano_referencia_w 	:= trunc(dt_mesano_referencia_p,'mm');
dt_referencia_w		:= trunc(dt_mesano_referencia_p,'dd');
ie_virada_w		:= 'N';

open C00;
loop
fetch C00 into
	cd_estabelecimento_w;
exit when C00%notfound;
	begin
	select	max(dt_mesano_vigente)
	into	dt_mesano_vigente_w
	from 	parametro_estoque
	where	cd_estabelecimento = cd_estabelecimento_w;

	if	(dt_mesano_referencia_w = dt_referencia_w) and
		(dt_mesano_vigente_w < dt_mesano_referencia_w) then
		begin
		ie_virada_w := 'S';
		
		select	count(*)
		into	qt_itens_negativos_w
		from	saldo_estoque a
		where	a.dt_mesano_referencia = dt_mesano_vigente_w
		and		a.cd_estabelecimento = cd_estabelecimento_w
		and		a.qt_estoque < 0;
		
		if	(qt_itens_negativos_w > 0) then		
			begin
			sup_ajusta_saldo_negativo(
				cd_estabelecimento_p	=> cd_estabelecimento_w,
				dt_mesano_referencia_p	=> dt_mesano_vigente_w,
				nm_usuario_p			=> 'JOB Val_Auto');
			exception
			when others then
				erro_w := substr(sqlerrm,1,255);
			end;

			select	count(*)
			into	qt_itens_negativos_w
			from	saldo_estoque a
			where	a.dt_mesano_referencia = dt_mesano_vigente_w
			and		a.cd_estabelecimento = cd_estabelecimento_w
			and		a.qt_estoque < 0;
		end if;
		
		if	(qt_itens_negativos_w = 0) then
			begin
			valorizar_estoque(
				dt_mesano_vigente_w,
				cd_estabelecimento_w,
				'S',
				'JOB Val_Auto');	
			exception
			when others then
				erro_w := substr(sqlerrm,1,255);
			end;
		end if;
		end;
	end if;
	
	if	(qt_itens_negativos_w > 0) then
		-- 169253 - Existem itens com estoque negativo. Verifique.
		erro_w := WHEB_MENSAGEM_PCK.get_texto(169253);
	end if;

	ds_log_w := substr(
			'cd_estabelecimento_w='||cd_estabelecimento_w || chr(13) || chr(10) ||
			'dt_mesano_referencia_w='|| to_char(dt_mesano_referencia_w,'dd/mm/yyyy') || chr(13) || chr(10) ||
			'dt_referencia_w='||to_char(dt_referencia_w,'dd/mm/yyyy') || chr(13) || chr(10) ||
			'dt_mesano_vigente_w='||to_char(dt_mesano_vigente_w,'dd/mm/yyyy') || chr(13) || chr(10) ||
			WHEB_MENSAGEM_PCK.get_texto(817877) /*'Virada = '*/ || '= ' || ie_virada_w || chr(13) || chr(10) || erro_w,1,4000);

	insert into log_val_estoque (cd_log, ds_log, dt_atualizacao, nm_usuario, cd_estabelecimento)
				values(1, ds_log_w, sysdate, 'JOB Val_Auto', cd_estabelecimento_w);

	commit;
	end;
end loop;
close C00;

END valorizar_estoque_auto;
/
