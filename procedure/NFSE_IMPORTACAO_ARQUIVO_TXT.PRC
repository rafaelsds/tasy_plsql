create or replace
procedure nfse_importacao_arquivo_txt(
			cd_estabelecimento_p	Number,
			nr_nota_fiscal_p		Number,
			cd_serie_nf_p			varchar2,
			cd_cpf_cnpj_prest_p		varchar2,
			dt_emissao_nfe_p		varchar2,
			nr_nfe_imp_p			varchar2,
			ds_observacao_p			varchar2,
			cd_verificacao_nfse_p	varchar2,
			ie_situacao_p			varchar2 default '') is

nr_sequencia_w	Number(10);
cd_serie_nr_w	varchar2(5);
dt_emissao_w	date;
cd_cgc_emit_w	varchar2(15);

begin

/*conversao de formato de data*/
begin

dt_emissao_w	:= dt_emissao_nfe_p;

exception when others then
	begin
	dt_emissao_w		:= to_date(dt_emissao_nfe_p,'yyyy-mm-dd');
	exception when others then
		begin
		dt_emissao_w	:= to_date(dt_emissao_nfe_p,'dd-mm-yyyy');
		exception when others then
			Wheb_mensagem_pck.exibir_mensagem_abort(229750,'DT_EMISSAO=' || dt_emissao_nfe_p);
		end;
	end;
end;

begin

select	cd_cgc
into	cd_cgc_emit_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

select	max(replace(cd_serie_nf_p,' ',''))
into	cd_serie_nr_w
from	dual;

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	nota_fiscal
where	cd_estabelecimento	= cd_estabelecimento_p
and	nr_nota_fiscal		= to_char(nr_nota_fiscal_p)
and	cd_serie_nf			= cd_serie_nf_p
and	cd_cgc_emitente		= nvl(cd_cpf_cnpj_prest_p,cd_cgc_emit_w)
and	nvl(nr_nfe_imp,0)	<> 	nr_nfe_imp_p;

if	(nr_sequencia_w > 0) then

	if	(ie_situacao_p = 'C') then
		update	nota_fiscal
		set	nr_nfe_imp 			= nr_nfe_imp_p,
			cd_verificacao_nfse = cd_verificacao_nfse_p,
			dt_emissao_nfe		= dt_emissao_w,
			ie_status_envio		= ie_situacao_p
		where	nr_sequencia 		= nr_sequencia_w;
	else
		update	nota_fiscal
		set	nr_nfe_imp 			= nr_nfe_imp_p,
			cd_verificacao_nfse = cd_verificacao_nfse_p,
			dt_emissao_nfe		= dt_emissao_w,
			ie_status_envio		= ie_situacao_p
		where	nr_sequencia 		= nr_sequencia_w;
	end if;
	update	nota_fiscal_item
	set	nr_nfe_imp 	= nr_nfe_imp_p
	where	nr_sequencia 	= nr_sequencia_w;
	
	insert into nota_fiscal_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_nota,
		cd_evento,
		ds_historico)
	values(	nota_fiscal_hist_seq.nextval,
		sysdate,
		'Tasy',
		sysdate,
		'Tasy',
		nr_sequencia_w,
		'27',
		substr(WHEB_MENSAGEM_PCK.get_texto(312266) || nr_nfe_imp_p || ' ' || ds_observacao_p,1,255)); --NF-e importada: 
end if;


exception when others then
	Wheb_mensagem_pck.exibir_mensagem_abort(229751,'NR_NOTA_FISCAL=' || nr_nota_fiscal_p ||';'||
												'CD_ESTABELECIMENTO=' || cd_serie_nf_p ||';'|| 
												'CD_SERIE_NF=' || cd_serie_nf_p ||';'|| 
												'DT_EMISSAO=' || dt_emissao_nfe_p ||';'|| 
												'CD_CNPJ_EMITENTE=' || nvl(cd_cpf_cnpj_prest_p,cd_cgc_emit_w) ||';'||
												'NR_NFE_IMP=' || nr_nfe_imp_p );
end;

commit;

end nfse_importacao_arquivo_txt;
/
