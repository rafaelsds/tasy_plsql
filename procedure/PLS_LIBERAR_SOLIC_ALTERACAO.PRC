create or replace
procedure pls_liberar_solic_alteracao
			(	nr_seq_solic_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 

begin
update	tasy_solic_alteracao
set	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	dt_analise 	= sysdate,	
	ie_status	= 'L'
where	nr_sequencia	= nr_seq_solic_p;

commit;

end pls_liberar_solic_alteracao;
/