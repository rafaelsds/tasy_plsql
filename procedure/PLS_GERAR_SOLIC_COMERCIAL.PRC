create or replace 
procedure  pls_gerar_solic_comercial
			(	nm_pessoa_p			in	varchar2,
				dt_nasc_p			in	varchar2,
				nr_telefone_p			in	varchar2,
				ds_email_p			in	varchar2,
				nr_ddi_p			in	varchar2,
				nr_ddd_p			in	varchar2,
				sg_estado_p			in	varchar2,
				cd_municipio_ibge_p		in	varchar2,
				ie_tipo_contratacao_p   	in 	varchar2,
				cd_cgc_p			in	varchar2,
				nr_seq_simulacao_p		in 	number,
				nm_contato_p			in	varchar2,
				nr_cpf_p			in	varchar2,
				nr_seq_agente_motivador_p 	in 	number,
				cd_estabelecimento_p		in	estabelecimento.cd_estabelecimento%type,
				ds_erro_p			out	varchar2) is

nr_sequencia_w			number(10);
qt_idade_w			number(10);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ds_erro_w			varchar2(4000);
qt_pessoas_w			number;
cd_exp_consistencia_w		pls_cad_duplicidade_lead.cd_exp_consistencia%type;

begin    


/*aaschlote 30/03/2011 OS - 300982*/
if	(dt_nasc_p is not null) then
	qt_idade_w	:= 	obter_idade(to_date(dt_nasc_p), SYSDATE,'A');
end if;	

cd_estabelecimento_w	:= nvl(cd_estabelecimento_p, pls_parametro_operadora_web('EST'));

select	pls_solicitacao_comercial_seq.nextval
into	nr_sequencia_w
from	dual;
		
pls_consistir_solicitacao_lead (ie_tipo_contratacao_p,nr_sequencia_w,nr_telefone_p,null,null,nr_ddi_p,dt_nasc_p,ds_email_p,cd_cgc_p,nr_cpf_p,null,cd_estabelecimento_w,qt_pessoas_w,ds_erro_w); 
		
ds_erro_w := substr(replace(replace(ds_erro_w,chr(10),''),chr(13),''),1,255);	

if(ds_erro_w is null or ds_erro_w = '') then	
	
	insert into pls_solicitacao_comercial(	nr_sequencia, cd_estabelecimento, ie_status,
						nm_pessoa_fisica, dt_nascimento, nr_telefone,
						dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
						nm_usuario_nrec, ds_email, nr_seq_simul_preco,
						dt_solicitacao, ie_origem_solicitacao, ie_tipo_contratacao,
						nr_cpf, cd_cgc, nr_ddi,
						nr_ddd, cd_nacionalidade, sg_uf_municipio,
						cd_municipio_ibge,nm_contato,ie_etapa_solicitacao,
						nr_seq_agente_motivador) 
				values (	nr_sequencia_w, cd_estabelecimento_w, 'PE',
						nm_pessoa_p, dt_nasc_p, nr_telefone_p,
						sysdate, 'OPS - Portal', sysdate,
						'OPS - Portal', ds_email_p, nr_seq_simulacao_p,
						sysdate, 'W', ie_tipo_contratacao_p,
						nr_cpf_p, cd_cgc_p, nr_ddi_p,
						nr_ddd_p, '10', sg_estado_p,
						cd_municipio_ibge_p,nm_contato_p,'T',
						nr_seq_agente_motivador_p);	

end if;
ds_erro_p := ds_erro_w;

commit;

end pls_gerar_solic_comercial; 
/
