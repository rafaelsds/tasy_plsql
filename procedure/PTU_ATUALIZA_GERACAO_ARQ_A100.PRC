create or replace
procedure ptu_atualiza_geracao_arq_a100
		(	nr_sequencial_p		number,
			ie_tipo_p		number,
			nm_usuario_p		Varchar2) is 

/*
ie_tipo_p
1 - Lote
2 - Item
3 - Item - Importa��o do retorno
*/		

qt_registros_w		number(10);	
			
begin

if	(ie_tipo_p	= 1) then
	select	count(*)
	into	qt_registros_w
	from	ptu_intercambio
	where	nr_seq_lote_envio	= nr_sequencial_p
	and	dt_geracao_arquivo is null;
	/*Atualizar a data de gera��o do arquivo,. caso todos os arquivos foram gerados*/
	if	(qt_registros_w = 0) then
		update	ptu_intercambio_lote_envio
		set	dt_geracao_arquivo	= sysdate,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia		= nr_sequencial_p;
	end if;
elsif	(ie_tipo_p	= 2) then
	update	ptu_intercambio
	set	dt_geracao_arquivo	= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_sequencial_p;
elsif	(ie_tipo_p	= 3) then
	update	ptu_intercambio
	set	DT_IMPORTACAO_RETORNO	= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_sequencial_p;
	
end if;

commit;

end ptu_atualiza_geracao_arq_a100;
/
