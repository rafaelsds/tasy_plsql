create or replace
procedure gerar_interf_conta_susaih (	nr_interno_conta_p		Number,
				nm_usuario_p		varchar2) is


nr_sequencia_w			Number(10);
cd_modalidade_w			Number(2);
nr_seq_aih_w			Number(10);
nr_proxima_aih_w			Number(13);
nr_anterior_aih_w			Number(13);
dt_emissao_w			Date; 
dt_entrada_w			Date;
dt_saida_w			Date;
cd_procedimento_solic_w		Number(10);
ie_origem_proc_solic_w		Number(10);
ie_mudanca_proc_w		Varchar2(1);
cd_procedimento_real_w		Number(10);
ie_origem_proc_real_w		Number(10);
cd_carater_internacao_w		Varchar(2);
ie_motivo_saida_w			Varchar2(2);
ie_doc_medico_solic_w		Number(1);
nr_cpf_medico_solic_w		Varchar2(11);
cd_cns_medico_solic_w		Number(15);
ie_doc_medico_resp_w		Number(1);
nr_cpf_medico_resp_w		Varchar2(11);
cd_cns_medico_resp_w		Number(15);
ie_doc_diretor_cli_w		Number(1);
nr_cpf_diretor_cli_w			Varchar2(11);
cd_cns_diretor_cli_w		Number(15);
ie_doc_medico_aut_w		Number(1);
nr_cpf_medico_aut_w		Varchar2(11);
cd_cns_medico_aut_w		Number(15);
cd_cid_principal_w			Varchar2(4);
cd_cid_secundario_w		Varchar2(4);
cd_cid_causa_compl_w		Varchar2(4);
cd_cid_causa_morte_w		Varchar2(4);
cd_solic_liberacao_w		Varchar2(3);
cd_pessoa_fisica_w		Varchar2(10);
nm_paciente_w			varchar2(70);
dt_nascimento_w			Date;
ie_sexo_w			Varchar2(1);
nm_mae_w			Varchar2(70);
nm_responsavel_w			Varchar2(70);
nr_pis_pasep_w			Varchar2(11);
nr_identidade_w			Varchar2(11);
nr_cert_nasc_w			Varchar2(11);
nr_cpf_w				Varchar2(11);
nr_cns_w				varchar2(20);
cd_nacionalidade_w		Varchar2(3);
cd_nacionalidade_ww		Varchar2(3);
tp_logradouro_w			varchar2(3);
ds_logradouro_w			Varchar2(50);
nr_logradouro_w			Number(7);
ds_compl_logradouro_w		Varchar2(15);
ds_bairro_w			Varchar2(30);
cd_municipio_ibge_w		Varchar2(6);
sg_estado_w			compl_pessoa_fisica.sg_estado%type;
cd_cep_w			Varchar2(8);
cd_municipio_origem_w		Varchar2(6)	:= '';
nr_prontuario_w			Number(15);
nr_enfermaria_ww		varchar2(10);
nr_enfermaria_w			varchar2(4);
nr_enfermaria_exp_w		varchar2(10);
nr_leito_ww			varchar2(10);/*Alterado para 10 por causa da regra de leito*/
nr_leito_w			varchar2(4);
nr_leito_exp_w			varchar2(10);
ie_saida_utineo_w			Number(1);
qt_peso_utineo_w			Number(4);
qt_mes_gestacao_w		Number(1);
cd_cgc_empregador_w		varchar2(14);
cd_cbor_w			Varchar2(6);
cd_cnaer_w			Varchar2(3);
ie_vinculo_prev_w			Number(1);
qt_nascido_morto_w		Number(1)	:= 0;
qt_nascido_vivo_w			Number(1)	:= 0;
qt_saida_alta_w			Number(1)	:= 0;
qt_saida_transferencia_w		Number(1)	:= 0;
qt_saida_obito_w			Number(1)	:= 0;
nr_gestante_prenatal_w		Number(12);
qt_filho_w			Number(2);
ie_grau_instrucao_w		Number(1)	:= 0;
cd_cid_indicacao_w		Varchar2(4);
cd_cid_associado_w		Varchar2(4);
ie_metodo_contracep_w		Number(2);
ie_metodo_contracep_um_w		Number(2);
ie_metodo_contracep_dois_w		Number(2);
ie_alto_risco_w			Number(1);
nr_aih_w				Number(13);
cd_medico_solic_w			Varchar2(10);
cd_medico_responsavel_w		Varchar2(10);
cd_diretor_clinico_w		Varchar2(10);
cd_medico_autorizador_w		Varchar2(10);
nr_seq_laq_vasec_w		Number(10);
qt_reg_metodo_con_w		Number(2)	:= 0;
nr_atendimento_w			Number(10);
cd_estabelecimento_w		Number(4);
ds_registro_proc_w			Varchar2(711);
ds_registro_conta_w		Varchar2(711)	:= '';
ie_doc_paciente_w			Number(1)	:= 5;
nr_doc_paciente_w			Varchar2(32);
nr_seq_cor_pele_w			Number(10);
cd_etnia_w			varchar2(4) := '0000';
ie_identificacao_aih_w		Varchar2(2)	:= '01';
qt_registros_w			Number(5)	:= 1;
ie_ordem_w			Number(5);
nm_resp_atend_w			Varchar2(60);
ie_exporta_resp_w			Varchar2(1);
nr_seq_procedimento_w		number(15);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_uti_neo_w			Number(10);
nr_matricula_nasc_w             		varchar2(32);
qt_setor_exp_aih_w			number(10) := 0;
nr_telefone_pac_w			varchar2(11);
cd_convenio_parametro_w		number(5);
cd_cnpj_w			varchar2(14);
qt_regra_leito_w		number(10) := 0;
ds_justificativa_cns_w		varchar2(255);
cd_setor_atendimento_w		number(5);
ie_gerar_ds_lograd_w		varchar2(15) := 'N';
ie_classif_diag_seq_w		sus_aih_unif.ie_classif_diag_seq%type;
cd_cid_secundario2_w		sus_aih_unif.cd_cid_secundario2%type;
ie_classif_diag_seq2_w		sus_aih_unif.ie_classif_diag_seq2%type;
cd_cid_secundario3_w		sus_aih_unif.cd_cid_secundario3%type;
ie_classif_diag_seq3_w		sus_aih_unif.ie_classif_diag_seq3%type;
cd_cid_secundario4_w		sus_aih_unif.cd_cid_secundario4%type;
ie_classif_diag_seq4_w		sus_aih_unif.ie_classif_diag_seq4%type;
cd_cid_secundario5_w		sus_aih_unif.cd_cid_secundario5%type;
ie_classif_diag_seq5_w		sus_aih_unif.ie_classif_diag_seq5%type;
cd_cid_secundario6_w		sus_aih_unif.cd_cid_secundario6%type;
ie_classif_diag_seq6_w		sus_aih_unif.ie_classif_diag_seq6%type;
cd_cid_secundario7_w		sus_aih_unif.cd_cid_secundario7%type;
ie_classif_diag_seq7_w		sus_aih_unif.ie_classif_diag_seq7%type;
cd_cid_secundario8_w		sus_aih_unif.cd_cid_secundario8%type;
ie_classif_diag_seq8_w		sus_aih_unif.ie_classif_diag_seq8%type;
cd_cid_secundario9_w		sus_aih_unif.cd_cid_secundario9%type;
ie_classif_diag_seq9_w		sus_aih_unif.ie_classif_diag_seq9%type;
ie_considerar_cns_ant_w	parametro_faturamento.ie_considerar_cns_ant%type;
nr_cartao_nac_sus_ant_w cnes_profissional.nr_cartao_nac_sus_ant%type;	

/* Obter os metodos contraceptivos*/
CURSOR C01 IS
select	ie_metodo_contraceptivo
from	sus_metodo_contraceptivo
where	nr_seq_reg	= nr_seq_laq_vasec_w;

/* Obter dados dos procedimentos, maximo 10 por registro (730 bytes)*/
CURSOR C02 IS
select	ds_registro,
	ie_ordem,
	nr_seq_proc
from	w_susaih_interf_item
where	nr_interno_conta	= nr_interno_conta_p
order by nr_linha_proc;

type 		fetch_array is table of c02%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_c02_w			vetor;

BEGIN


/* Obter dados do paciente*/ 
begin
select	elimina_caractere_esp_asc(elimina_acentos(upper(c.nm_pessoa_fisica)),'S','N'),
	c.dt_nascimento,
	c.ie_sexo, 
	elimina_caractere_esp_asc(elimina_acentos(upper(nvl(substr(obter_compl_pf(c.cd_pessoa_fisica, 5, 'N'),1,70),substr(obter_nome_pf(c.cd_pessoa_mae),1,70)))),'S','N') nm_mae,
	elimina_caractere_esp_asc(elimina_acentos(upper(substr(nvl(obter_compl_pf(c.cd_pessoa_fisica, 3, 'N'),c.nm_pessoa_fisica),1,70))),'S','N') nm_responsavel,
	c.nr_pis_pasep,
	substr(somente_numero(c.nr_identidade),1,11),
	substr(c.NR_CERT_NASC,1,11) NR_CERT_NASC,
	substr(c.nr_matricula_nasc,1,32) nr_matricula_nasc,
	c.nr_cpf,
	substr(c.nr_cartao_nac_sus,1,15),
	elimina_caractere_esp_asc(elimina_acentos(upper(substr(replace(obter_compl_pf(c.cd_pessoa_fisica, 1, 'EN'),'-',' '),1,50))),'S','N') ds_logradouro,
	upper(substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'NR'),1,7)) nr_logradouro,
	elimina_caractere_esp_asc(elimina_acentos(upper(substr(replace(replace(obter_compl_pf(c.cd_pessoa_fisica, 1, 'CO'),chr(10),' '),chr(13),' '),1,15))),'S','N') ds_compl_logradouro,
	elimina_caractere_esp_asc(elimina_acentos(upper(substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'B'),1,30))),'S','N') ds_bairro,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'CDM'),1,6) cd_municipio_ibge,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'UF'),1,15) sg_estado,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'CEP'),1,8) cd_cep,
	nvl(to_number(obter_prontuario_pf(b.cd_estabelecimento,c.cd_pessoa_fisica)), c.nr_prontuario),
	decode(c.ie_grau_instrucao,1,1,10,1,11,1,2,2,7,2,14,2,3,3,8,3,15,3,4,4,5,4,6,4,9,4,12,4,13,4),
	b.nr_atendimento,
	b.dt_entrada,
	nvl(b.dt_alta,sysdate),
	c.cd_pessoa_fisica,
	b.cd_estabelecimento,
	Sus_Obter_Cor_Pele(c.cd_pessoa_fisica, 'C'),
	lpad(substr(decode(Sus_Obter_Cor_Pele(c.cd_pessoa_fisica, 'C'),'5',sus_obter_etnia(c.cd_pessoa_fisica, 'C'),'0000'),1,4),4,'0'),
	substr(c.cd_nacionalidade,1,3) cd_nacionalidade,
	substr(obter_compl_pf(c.cd_pessoa_fisica, 1, 'TLS'),1,3) tp_logradouro,
	elimina_caractere_esp_asc(elimina_acentos(upper(substr(obter_nome_pf(b.cd_pessoa_responsavel),1,60))),'S','N') nm_resp_atend,
	substr(sus_obter_telefone_export(c.cd_pessoa_fisica),1,11) nr_telefone_pac,
	a.cd_convenio_parametro
into	nm_paciente_w,
	dt_nascimento_w,
	ie_sexo_w,
	nm_mae_w,
	nm_responsavel_w,
	nr_pis_pasep_w,
	nr_identidade_w,
	nr_cert_nasc_w,
	nr_matricula_nasc_w,
	nr_cpf_w,
	nr_cns_w, 
	ds_logradouro_w,
	nr_logradouro_w,
	ds_compl_logradouro_w,
	ds_bairro_w,
	cd_municipio_ibge_w,
	sg_estado_w,
	cd_cep_w,
	nr_prontuario_w,
	ie_grau_instrucao_w,
	nr_atendimento_w,
	dt_entrada_w,
	dt_saida_w,
	cd_pessoa_fisica_w,
	cd_estabelecimento_w,
	nr_seq_cor_pele_w,
	cd_etnia_w,
	cd_nacionalidade_ww,
	tp_logradouro_w,
	nm_resp_atend_w,
	nr_telefone_pac_w,
	cd_convenio_parametro_w
from	pessoa_fisica		c,
	atendimento_paciente	b,
	conta_paciente		a
where	a.nr_interno_conta	= nr_interno_conta_p
and	a.nr_atendimento	= b.nr_atendimento
and	b.cd_pessoa_fisica	= c.cd_pessoa_fisica;
exception
	when others then
	wheb_mensagem_pck.exibir_mensagem_abort(173752,'NR_INTERNO_CONTA_P='|| nr_interno_conta_p ||';SQLERRM='||SQLERRM(sqlcode)||';');
	/* Conta: #@NR_INTERNO_CONTA_P#@ - #@SQLERRM#@ */
end;

ie_gerar_ds_lograd_w	:= nvl(obter_valor_param_usuario(1123,203,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w),'N');

if	(ie_gerar_ds_lograd_w = 'S') and	
	(tp_logradouro_w not in ('081','008')) then
	begin
	ds_logradouro_w	:= substr('('||sus_obter_desc_tipolog(tp_logradouro_w)||') '|| ds_logradouro_w ,1,50);
	end;
end if;

if	(tp_logradouro_w	is not null) and
	(somente_numero(tp_logradouro_w)	= 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(173753, 'NR_ATENDIMENTO_P='|| nr_atendimento_w ||';NR_INTERNO_CONTA_P='|| nr_interno_conta_p ||';');
	/* Tipo de Logradouro invalido no Cadastro do Paciente. Atend.: #@NR_ATENDIMENTO_P#@  - Conta: #@NR_INTERNO_CONTA_P#@ */
end if;

/* Obter dados dos parametros da AIH*/
if	((nvl(sus_parametros_aih_pck.get_cd_estabelecimento,0) <> cd_estabelecimento_w) or
	(nvl(sus_parametros_aih_pck.get_dt_atualizacao,sysdate) > nvl(sus_parametros_aih_pck.get_dt_geracao,sysdate))) then
	begin
	gerar_sus_parametros_aih_pck(cd_estabelecimento_w);
	
	cd_diretor_clinico_w		:= sus_parametros_aih_pck.get_cd_diretor_clinico;
	ie_exporta_resp_w		:= sus_parametros_aih_pck.get_ie_exporta_resp;
	nr_cpf_diretor_cli_w		:= substr(obter_dados_pf(cd_diretor_clinico_w,'CPF'),1,11);
	cd_cns_diretor_cli_w		:= substr(obter_dados_pf(cd_diretor_clinico_w,'CNS'),1,15);
	
	end;
else	
	begin
	cd_diretor_clinico_w		:= sus_parametros_aih_pck.get_cd_diretor_clinico;
	ie_exporta_resp_w		:= sus_parametros_aih_pck.get_ie_exporta_resp;
	nr_cpf_diretor_cli_w		:= substr(obter_dados_pf(cd_diretor_clinico_w,'CPF'),1,11);
	cd_cns_diretor_cli_w		:= substr(obter_dados_pf(cd_diretor_clinico_w,'CNS'),1,15);	
	end;
end if;


begin
select	nvl(ie_considerar_cns_ant, 'N')
into	ie_considerar_cns_ant_w
from	parametro_faturamento
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
exception
	when others then
	ie_considerar_cns_ant_w :='N';
end;

if (ie_considerar_cns_ant_w = 'S') then
	begin
		select	nvl(b.nr_cartao_nac_sus_ant, ' ')
		into	nr_cartao_nac_sus_ant_w
		from 	cnes_profissional 	b
		where	b.cd_pessoa_fisica = cd_diretor_clinico_w
		and 	rownum = 1;
		exception
	when others then
		nr_cartao_nac_sus_ant_w := ' ';
	end;
	if (nr_cartao_nac_sus_ant_w <> ' ') then
		cd_cns_diretor_cli_w := nr_cartao_nac_sus_ant_w;
	end if;
end if;

if	(ie_exporta_resp_w = 'A') and
	(nm_resp_atend_w is not null) then
	nm_responsavel_w	:= nm_resp_atend_w;
end if;

cd_medico_autorizador_w	:= sus_obter_autorizador_aih(nr_interno_conta_p, 'PF');

/* Obter dados da AIH*/
begin
select	nr_aih,
	cd_modalidade,
	nr_sequencia,
	nr_proxima_aih,
	nr_anterior_aih,
	dt_emissao,
	cd_procedimento_solic,
	ie_origem_proc_solic,
	decode(ie_mudanca_proc,'S',1,2),
	cd_procedimento_real,
	ie_origem_proc_real,
	cd_carater_internacao,
	cd_medico_solic,
	substr(obter_dados_pf(cd_medico_solic,'CPF'),1,11) nr_cpf_medico_solic,
	substr(obter_dados_pf(cd_medico_solic,'CNS'),1,15) cd_cns_medico_solic,
	cd_medico_responsavel,
	substr(obter_dados_pf(cd_medico_responsavel,'CPF'),1,11) nr_cpf_medico_resp,
	substr(obter_dados_pf(cd_medico_responsavel,'CNS'),1,15) cd_cns_medico_resp,
	nvl(cd_medico_autorizador, cd_medico_autorizador_w),
	substr(obter_dados_pf(nvl(cd_medico_autorizador, cd_medico_autorizador_w),'CPF'),1,11) nr_cpf_medic_autoriz,
	substr(obter_dados_pf(nvl(cd_medico_autorizador, cd_medico_autorizador_w),'CNS'),1,15) cd_cns_medic_autoriz,	
	cd_cid_principal,
	cd_cid_causa_compl,
	cd_cid_causa_morte,
	qt_nascido_vivo,
	qt_nascido_morto,
	qt_saida_alta,
	qt_saida_transferencia,
	qt_saida_obito,
	substr(nr_gestante_prenatal,1,12),
	cd_motivo_cobranca,
	lpad(ie_codigo_autorizacao, 3, 0),
	nvl(dt_inicial,dt_entrada_w),
	nvl(dt_final,dt_saida_w),
	ie_identificacao_aih,
	ds_justificativa_cns,
	cd_cid_secundario,
	nvl(ie_classif_diag_seq,'1'),
	cd_cid_secundario2,
	nvl(ie_classif_diag_seq2,'1'),
	cd_cid_secundario3,
	nvl(ie_classif_diag_seq3,'1'),
	cd_cid_secundario4,
	nvl(ie_classif_diag_seq4,'1'),
	cd_cid_secundario5,
	nvl(ie_classif_diag_seq5,'1'),
	cd_cid_secundario6,
	nvl(ie_classif_diag_seq6,'1'),
	cd_cid_secundario7,
	nvl(ie_classif_diag_seq7,'1'),
	cd_cid_secundario8,
	nvl(ie_classif_diag_seq8,'1'),
	cd_cid_secundario9,
	nvl(ie_classif_diag_seq9,'1')
into	nr_aih_w,
	cd_modalidade_w,
	nr_seq_aih_w,
	nr_proxima_aih_w,
	nr_anterior_aih_w,
	dt_emissao_w,
	cd_procedimento_solic_w,
	ie_origem_proc_solic_w,
	ie_mudanca_proc_w,
	cd_procedimento_real_w,
	ie_origem_proc_real_w,
	cd_carater_internacao_w,
	cd_medico_solic_w,
	nr_cpf_medico_solic_w,
	cd_cns_medico_solic_w,
	cd_medico_responsavel_w,
	nr_cpf_medico_resp_w,
	cd_cns_medico_resp_w,
	cd_medico_autorizador_w,
	nr_cpf_medico_aut_w,
	cd_cns_medico_aut_w,
	cd_cid_principal_w,
	cd_cid_causa_compl_w,
	cd_cid_causa_morte_w,
	qt_nascido_vivo_w,
	qt_nascido_morto_w,
	qt_saida_alta_w,
	qt_saida_transferencia_w,
	qt_saida_obito_w,
	nr_gestante_prenatal_w,
	ie_motivo_saida_w,
	cd_solic_liberacao_w,
	dt_entrada_w,
	dt_saida_w,
	ie_identificacao_aih_w,
	ds_justificativa_cns_w,
	cd_cid_secundario_w,
	ie_classif_diag_seq_w,
	cd_cid_secundario2_w,
	ie_classif_diag_seq2_w,
	cd_cid_secundario3_w,
	ie_classif_diag_seq3_w,
	cd_cid_secundario4_w,
	ie_classif_diag_seq4_w,
	cd_cid_secundario5_w,
	ie_classif_diag_seq5_w,
	cd_cid_secundario6_w,
	ie_classif_diag_seq6_w,
	cd_cid_secundario7_w,
	ie_classif_diag_seq7_w,
	cd_cid_secundario8_w,
	ie_classif_diag_seq8_w,
	cd_cid_secundario9_w,
	ie_classif_diag_seq9_w
from	sus_aih_unif
where	nr_interno_conta	= nr_interno_conta_p;
exception
	when no_data_found then
	wheb_mensagem_pck.exibir_mensagem_abort(173756, 'NR_ATENDIMENTO_P='|| NR_ATENDIMENTO_W ||';NR_INTERNO_CONTA_P='|| NR_INTERNO_CONTA_P ||';');
	/* Conta sem AIH vinculada, favor verificar! Atend: #@NR_ATENDIMENTO_P#@ - Conta: #@NR_INTERNO_CONTA_P#@ */
end;

if (ie_considerar_cns_ant_w = 'S') then

	begin
	select	nvl(b.nr_cartao_nac_sus_ant, ' ')
	into	nr_cartao_nac_sus_ant_w
	from 	cnes_profissional 	b
	where	b.cd_pessoa_fisica = cd_medico_solic_w
	and 	rownum = 1;
	exception
	when others then
		nr_cartao_nac_sus_ant_w := ' ';
	end;
	if (nr_cartao_nac_sus_ant_w <> ' ') then
		cd_cns_medico_solic_w := nr_cartao_nac_sus_ant_w;
	end if;
	
	begin
	select	nvl(b.nr_cartao_nac_sus_ant, ' ')
	into	nr_cartao_nac_sus_ant_w
	from 	cnes_profissional 	b
	where	b.cd_pessoa_fisica = cd_medico_responsavel_w
	and 	rownum = 1;
	exception
	when others then
		nr_cartao_nac_sus_ant_w := ' ';
	end;
	
	if	(nr_cartao_nac_sus_ant_w <> ' ') then
		cd_cns_medico_resp_w := nr_cartao_nac_sus_ant_w;
	end if;
	
	begin
	select	nvl(b.nr_cartao_nac_sus_ant, ' ')
	into	nr_cartao_nac_sus_ant_w
	from 	cnes_profissional 	b
	where	b.cd_pessoa_fisica = cd_medico_autorizador_w
	and 	rownum = 1;
	exception
	when others then
		nr_cartao_nac_sus_ant_w := ' ';
	end;
	if 	(nr_cartao_nac_sus_ant_w <> ' ') then
		cd_cns_medico_aut_w := nr_cartao_nac_sus_ant_w;
	end if;	
	
	begin
	select	nvl(b.nr_cartao_nac_sus_ant, ' ')
	into	nr_cartao_nac_sus_ant_w
	from 	cnes_profissional 	b
	where	b.cd_pessoa_fisica = cd_pessoa_fisica_w
	and 	rownum = 1;
	exception
	when others then
		nr_cartao_nac_sus_ant_w := ' ';
	end;
	if (nr_cartao_nac_sus_ant_w <> ' ') then
		nr_cns_w := nr_cartao_nac_sus_ant_w;
	end if;
end if;

if	(ie_identificacao_aih_w <> '05') then
	ie_identificacao_aih_w	:= '01';
end if;

select 	count(*)
into	qt_setor_exp_aih_w
from	sus_regra_setor_exp_aih
where	rownum = 1;


/* Obter enfermaria e leito de acordo com o UEXPSUS*/
if	(nvl(qt_setor_exp_aih_w,0) > 0) then
	begin
	
	begin
	select	substr(b.cd_unidade_basica,1,10),
		substr(b.cd_unidade_compl,1,10),
		b.cd_setor_atendimento
	into	nr_enfermaria_ww,
		nr_leito_ww,
		cd_setor_atendimento_w
	from   	atendimento_paciente a,
		atend_paciente_unidade b,
		atend_categoria_convenio c,
		empresa_referencia d,
		sus_regra_setor_exp_aih e
	where 	a.nr_atendimento 	= nr_atendimento_w
	and	a.nr_atendimento 	= b.nr_atendimento
	and	c.cd_empresa 		= d.cd_empresa(+)
	and	b.cd_setor_atendimento  = e.cd_setor_atendimento
	and	(b.dt_entrada_unidade 	= (	select	/*+ index(x atepacu_i2) */
							max(x.dt_entrada_unidade)
						from 	atend_paciente_unidade x
						where 	x.nr_atendimento 	= a.nr_atendimento
						and 	x.dt_entrada_unidade 	>= a.dt_entrada))
	and 	(a.nr_atendimento 	= c.nr_atendimento)
	and 	(c.dt_inicio_vigencia 	= (	select 	max(w.dt_inicio_vigencia) dt_primeira_vigencia
						from 	atend_categoria_convenio w
						where 	w.nr_atendimento  = a.nr_atendimento));
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(173760, 'NR_ATENDIMENTO_P='|| nr_atendimento_w ||';NR_INTERNO_CONTA_P='|| nr_interno_conta_p ||';SQLERRM='|| SQLERRM(sqlcode) ||';');
		/* Leito ou enfermaria nao encontrado ou convenio esta duplicado no atendimento. Atend.: #@NR_ATENDIMENTO_P#@ - Conta: #@NR_INTERNO_CONTA_P#@  #@SQLERRM#@  */
	end;
						
	end;
else
	begin
	
	begin
	select	substr(a.cd_unidade_basica,1,10),
		substr(a.cd_unidade_compl,1,10),
		a.cd_setor_atendimento
	into	nr_enfermaria_ww,
		nr_leito_ww,
		cd_setor_atendimento_w
	from 	paciente_internado_v	a,
		empresa_referencia	b
	where 	a.cd_empresa 		= b.cd_empresa(+)
	and 	a.nr_atendimento	= nr_atendimento_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(173760, 'NR_ATENDIMENTO_P='|| nr_atendimento_w ||';NR_INTERNO_CONTA_P='|| nr_interno_conta_p ||';SQLERRM='|| SQLERRM(sqlcode) ||';');
		/* Leito ou enfermaria nao encontrado ou convenio esta duplicado no atendimento. Atend.: #@NR_ATENDIMENTO_P#@ - Conta: #@NR_INTERNO_CONTA_P#@  #@SQLERRM#@  */
	end;
	
	end;
end if;

select	count(*)
into	qt_regra_leito_w
from	sus_regra_exporta_leito
where	cd_estabelecimento = cd_estabelecimento_w
and	ie_situacao = 'A'
and	rownum = 1;


if	(qt_regra_leito_w > 0) then
	begin

	begin
	select	nvl(max(nr_enfermaria_exporta),nr_enfermaria_ww),
		nvl(max(nr_leito_exporta),nr_leito_ww)
	into	nr_enfermaria_exp_w,
		nr_leito_exp_w
	from	sus_regra_exporta_leito
	where	cd_estabelecimento 				= cd_estabelecimento_w
	and	nvl(nr_enfermaria_regra,nr_enfermaria_ww)	= nr_enfermaria_ww
        and	nvl(nr_leito_regra,nr_leito_ww)			= nr_leito_ww
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_w) = cd_setor_atendimento_w
	and	ie_situacao = 'A';
	exception
	when others then
		nr_enfermaria_exp_w	:= nr_enfermaria_ww;
		nr_leito_exp_w		:= nr_leito_ww;
	end;

	nr_enfermaria_ww	:= nr_enfermaria_exp_w;
	nr_leito_ww		:= nr_leito_exp_w;	
	
	end;
end if;

nr_enfermaria_w := lpad(substr(nvl(nr_enfermaria_ww,'1'),1,4),4,'0');
nr_leito_w	:= lpad(substr(replace(nr_leito_ww,' ', '1'),1,4),4,'0');

/* Obter dados do acidente de trabalho*/
begin
select	nvl(nvl(cd_cnpj_empregador_exp,cd_cgc_empregador),'00000000000000'),
	cd_cbo_sus,
	cd_atividade_cnaer,
	ie_vinculo_sus
into	cd_cgc_empregador_w,
	cd_cbor_w,
	cd_cnaer_w,
	ie_vinculo_prev_w
from	sus_acidente_trabalho
where	nr_aih		= nr_aih_w		
and	nr_seq_aih	= nr_seq_aih_w;
exception
when others then
	cd_cgc_empregador_w	:= '00000000000000';
	cd_cbor_w		:= '';
	cd_cnaer_w		:= '';
	ie_vinculo_prev_w	:= '';
end;

/* Felpe - 27/01/2008 - provisoriamente estou passando zeros pois no SISAIH nao importa o arquivo quando tem essa informacao */
/*cd_cgc_empregador_w	:= '00000000000000';
cd_cbor_w		:= '      ';*/

/* Obter dados da laqueadura ou vasectomia */
begin
select	nr_sequencia,
	qt_filho,
	substr(nvl(cd_cid_indicacao,'0000'),1,4) cd_cid_indicacao,
	substr(cd_cid_associado,1,4) cd_cid_associado,
	decode(ie_alto_risco,'S',0,1)
into	nr_seq_laq_vasec_w,
	qt_filho_w,
	cd_cid_indicacao_w,
	cd_cid_associado_w,
	ie_alto_risco_w
from	sus_laqueadura_vasectomia
where	nr_aih		= nr_aih_w
and	nr_seq_aih	= nr_seq_aih_w;
exception
when others then
	nr_seq_laq_vasec_w	:= '';
	qt_filho_w		:= '';
	cd_cid_indicacao_w	:= '0000';
	cd_cid_associado_w	:= '';
	ie_alto_risco_w		:= '';
end;

begin
select	nvl(max(cd_externo),cd_nacionalidade_ww)
into	cd_nacionalidade_ww
from	nacionalidade
where	cd_nacionalidade = cd_nacionalidade_ww
and	cd_nacionalidade <> '10';
exception
when others then
	cd_nacionalidade_ww := cd_nacionalidade_ww;
end;

cd_nacionalidade_w := lpad(cd_nacionalidade_ww, 3, 0);

if	(nvl(cd_cid_secundario_w,'0') = '0') then
	ie_classif_diag_seq_w := '0';
end if;
if	(nvl(cd_cid_secundario2_w,'0') = '0') then
	ie_classif_diag_seq2_w := '0';
end if;
if	(nvl(cd_cid_secundario3_w,'0') = '0') then
	ie_classif_diag_seq3_w := '0';
end if;
if	(nvl(cd_cid_secundario4_w,'0') = '0') then
	ie_classif_diag_seq4_w := '0';
end if;
if	(nvl(cd_cid_secundario5_w,'0') = '0') then
	ie_classif_diag_seq5_w := '0';
end if;
if	(nvl(cd_cid_secundario6_w,'0') = '0') then
	ie_classif_diag_seq6_w := '0';
end if;
if	(nvl(cd_cid_secundario7_w,'0') = '0') then
	ie_classif_diag_seq7_w := '0';
end if;
if	(nvl(cd_cid_secundario8_w,'0') = '0') then
	ie_classif_diag_seq8_w := '0';
end if;
if	(nvl(cd_cid_secundario9_w,'0') = '0') then
	ie_classif_diag_seq9_w := '0';
end if;

/* Obter os metodos contraceptivos*/
OPEN C01;
LOOP
FETCH C01 into
	ie_metodo_contracep_w;
EXIT WHEN C01%NOTFOUND;
	BEGIN
	qt_reg_metodo_con_w	:= qt_reg_metodo_con_w + 1;
	if	(qt_reg_metodo_con_w	= 1) then
		ie_metodo_contracep_um_w	:= ie_metodo_contracep_w;
	elsif	(qt_reg_metodo_con_w	= 2) then
		ie_metodo_contracep_dois_w	:= ie_metodo_contracep_w;
	end if;
	END;
END LOOP;
close C01;

/* Identificar qual tipo de documento*/
ie_doc_medico_solic_w	:= 2;
ie_doc_medico_resp_w	:= 2;
ie_doc_medico_aut_w	:= 2;
ie_doc_diretor_cli_w	:= 2;

/* Retorna o PIS-PASEP/Identidade/RG/CPF*/
if	(nr_doc_paciente_w is null) and
        (nr_pis_pasep_w is not null) then
        nr_doc_paciente_w	:= nr_pis_pasep_w;
        ie_doc_paciente_w   	:= 1;
elsif   (nr_doc_paciente_w is null) and
        (nr_identidade_w is not null) and
	(nr_identidade_w <> 0) then
        nr_doc_paciente_w  	:= nr_identidade_w;
        ie_doc_paciente_w   	:= 2;
elsif   (nr_doc_paciente_w is null) and
        (nr_cert_nasc_w is not null) then
        nr_doc_paciente_w  	:= nr_cert_nasc_w;
        ie_doc_paciente_w   	:= 3;
elsif   (nr_doc_paciente_w is null) and
        (nr_cpf_w is not null) then
        nr_doc_paciente_w  	:= nr_cpf_w;
        ie_doc_paciente_w   	:= 4;
elsif	(nr_doc_paciente_w is null) and
	(nr_matricula_nasc_w is not null) then
	nr_doc_paciente_w  	:= nr_matricula_nasc_w;
        ie_doc_paciente_w   	:= 6;
else
        nr_doc_paciente_w  	:= ' ';
        ie_doc_paciente_w   	:= 5;
end if;

select nvl(max(nr_seq_proc),0)
into	nr_uti_neo_w
from	w_susaih_interf_item
where	nr_interno_conta	= nr_interno_conta_p
and	Sus_Validar_Regra(7,cd_procedimento,7,dt_competencia) > 0;

if	(nr_uti_neo_w <> 0) then
	begin
	select	max(nr_meses_gestacional),
		max(ie_motivo_saida_neo),
		max(nr_peso)
	into	qt_mes_gestacao_w,
		ie_saida_utineo_w,
		qt_peso_utineo_w
	from	sus_aih_uti_neo
	where	nr_seq_procedimento		= nr_uti_neo_w;
			exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(173765,'NR_ATENDIMENTO_P='|| nr_atendimento_w ||';NR_INTERNO_CONTA_P='|| nr_interno_conta_p ||';');
		/* Favor verificar os procedimentos de Uti Neo Natal, pois o campo Peso ao nascer esta com um tamanho superior ao definido pelo SUS. Atend: #@NR_ATENDIMENTO_P#@ Conta: #@NR_INTERNO_CONTA_P#@ */
	end;
end if;

if	(nvl(ie_motivo_saida_w,'X') = 'X') then
	begin
	ie_motivo_saida_w := nvl(sus_obter_motiv_cobr_conta(nr_interno_conta_p),'');
	end;
end if;

/* Obter dados dos procedimentos, maximo 10 por registro (570 bytes)*/
open c02;
loop
fetch c02 bulk collect into s_array limit 1000;
	vetor_c02_w(i) := s_array;
	i := i + 1;
exit when c02%notfound;
end loop;
close c02;

for i in 1..vetor_c02_w.count loop
	begin
	s_array := vetor_c02_w(i);
	for z in 1..s_array.count loop
		begin
			
		ds_registro_proc_w	:= s_array(z).ds_registro;
		ie_ordem_w		:= s_array(z).ie_ordem;
		nr_seq_procedimento_w	:= s_array(z).nr_seq_proc;
		
		ds_registro_conta_w	:= ds_registro_conta_w || ds_registro_proc_w;
	
		select	max(cd_procedimento),
			max(ie_origem_proced)
		into	cd_procedimento_w,
			ie_origem_proced_w
		from	procedimento_paciente
		where	nr_sequencia		= nr_seq_procedimento_w;
		
		if	(mod(qt_registros_w,9) = 0) then
			select	w_susaih_interf_conta_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			begin
			insert into w_susaih_interf_conta(
				nr_sequencia,
				cd_modalidade,
				nr_seq_aih,
				nr_proxima_aih,
				nr_anterior_aih,
				dt_emissao,
				dt_internacao,
				dt_saida,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_procedimento_solic,
				ie_origem_proc_solic,
				ie_mudanca_proc,
				cd_procedimento_real,
				ie_origem_proc_real,
				ie_carater_inter_sus,
				ie_motivo_saida,
				ie_doc_medico_solic,
				nr_cpf_medico_solic,
				cd_cns_medico_solic,
				ie_doc_medico_resp,
				nr_cpf_medico_resp,
				cd_cns_medico_resp,
				ie_doc_diretor_cli,
				nr_cpf_diretor_cli,
				cd_cns_diretor_cli,
				ie_doc_medico_aut,
				nr_cpf_medico_aut,
				cd_cns_medico_aut,
				cd_cid_principal,
				cd_cid_causa_compl,
				cd_cid_causa_morte,
				cd_solic_liberacao,
				cd_pessoa_fisica,
				nm_paciente,
				dt_nascimento,
				ie_sexo,
				nm_mae,
				nm_responsavel,
				nr_pis_pasep,
				nr_identidade,
				nr_cert_nasc,
				nr_cpf,
				nr_cns,
				cd_nacionalidade,
				tp_logradouro,
				ds_logradouro,
				nr_logradouro,
				ds_compl_logradouro,
				ds_bairro,
				cd_municipio_ibge,
				sg_estado,
				cd_cep,
				cd_municipio_origem,
				nr_prontuario,
				nr_enfermaria,
				nr_leito,
				ie_saida_utineo,
				qt_peso_utineo,
				qt_mes_gestacao,
				cd_cgc_empregador,
				cd_cbor,
				cd_cnaer,
				ie_vinculo_prev,
				qt_nascido_vivo,
				qt_nascido_morto,
				qt_saida_alta,
				qt_saida_transferencia,
				qt_saida_obito,
				nr_gestante_prenatal,
				qt_filho,
				ie_grau_instrucao,
				cd_cid_indicacao,
				cd_cid_associado,
				ie_metodo_contracep_um,
				ie_metodo_contracep_dois,
				ie_alto_risco,
				cd_medico_solic,
				cd_medico_responsavel,
				cd_diretor_clinico,
				cd_medico_autorizador,
				nr_aih,
				nr_interno_conta,
				ds_registro_proc,
				ie_doc_paciente,
				nr_doc_paciente,
				nr_seq_cor_pele,
				cd_etnia,
				ie_identificacao_aih,
				nr_matricula_nasc,
				nr_telefone_pac,
				ds_justificativa_cns,
				cd_cid_secundario,
				ie_classif_diag_seq,
				cd_cid_secundario2,
				ie_classif_diag_seq2,
				cd_cid_secundario3,
				ie_classif_diag_seq3,
				cd_cid_secundario4,
				ie_classif_diag_seq4,
				cd_cid_secundario5,
				ie_classif_diag_seq5,
				cd_cid_secundario6,
				ie_classif_diag_seq6,
				cd_cid_secundario7,
				ie_classif_diag_seq7,
				cd_cid_secundario8,
				ie_classif_diag_seq8,
				cd_cid_secundario9,
				ie_classif_diag_seq9)
			values(	nr_sequencia_w,
				cd_modalidade_w,
				nr_seq_aih_w,
				nr_proxima_aih_w,
				nr_anterior_aih_w,
				dt_emissao_w,
				dt_entrada_w,
				dt_saida_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_procedimento_solic_w,
				ie_origem_proc_solic_w,
				ie_mudanca_proc_w,
				cd_procedimento_real_w,
				ie_origem_proc_real_w,
				cd_carater_internacao_w,
				ie_motivo_saida_w,
				ie_doc_medico_solic_w,
				nr_cpf_medico_solic_w,
				cd_cns_medico_solic_w,
				ie_doc_medico_resp_w,
				nr_cpf_medico_resp_w,
				cd_cns_medico_resp_w,
				ie_doc_diretor_cli_w,
				nr_cpf_diretor_cli_w,
				cd_cns_diretor_cli_w,
				ie_doc_medico_aut_w,
				nr_cpf_medico_aut_w,
				cd_cns_medico_aut_w,
				cd_cid_principal_w,
				cd_cid_causa_compl_w,
				cd_cid_causa_morte_w,
				cd_solic_liberacao_w,
				cd_pessoa_fisica_w,
				nm_paciente_w,
				dt_nascimento_w,
				ie_sexo_w,
				nm_mae_w,
				nm_responsavel_w,
				nr_pis_pasep_w,
				nr_identidade_w,
				nr_cert_nasc_w,
				nr_cpf_w,
				nr_cns_w,
				cd_nacionalidade_w,
				tp_logradouro_w,
				ds_logradouro_w,
				nr_logradouro_w,
				ds_compl_logradouro_w,
				ds_bairro_w,
				cd_municipio_ibge_w,
				sg_estado_w,
				cd_cep_w,
				cd_municipio_origem_w,
				nr_prontuario_w,
				nr_enfermaria_w,
				nr_leito_w,
				ie_saida_utineo_w,
				qt_peso_utineo_w,
				qt_mes_gestacao_w,
				cd_cgc_empregador_w,
				cd_cbor_w,
				cd_cnaer_w,
				ie_vinculo_prev_w,
				qt_nascido_vivo_w,
				qt_nascido_morto_w,
				qt_saida_alta_w,
				qt_saida_transferencia_w,
				qt_saida_obito_w,
				nr_gestante_prenatal_w,
				qt_filho_w,
				ie_grau_instrucao_w,
				cd_cid_indicacao_w,
				cd_cid_associado_w,
				ie_metodo_contracep_um_w,
				ie_metodo_contracep_dois_w,
				ie_alto_risco_w,
				cd_medico_solic_w,
				cd_medico_responsavel_w,
				cd_diretor_clinico_w,
				cd_medico_autorizador_w,
				nr_aih_w,
				nr_interno_conta_p,
				rpad(ds_registro_conta_w,711,'0'),
				ie_doc_paciente_w,
				nr_doc_paciente_w,
				nr_seq_cor_pele_w,
				cd_etnia_w,
				ie_identificacao_aih_w,
				nr_matricula_nasc_w,
				nr_telefone_pac_w,
				substr(upper(Elimina_Acentuacao(elimina_caracteres_especiais(substr(ds_justificativa_cns_w,1,200)))),1,50),
				cd_cid_secundario_w,
				ie_classif_diag_seq_w,
				cd_cid_secundario2_w,
				ie_classif_diag_seq2_w,
				cd_cid_secundario3_w,
				ie_classif_diag_seq3_w,
				cd_cid_secundario4_w,
				ie_classif_diag_seq4_w,
				cd_cid_secundario5_w,
				ie_classif_diag_seq5_w,
				cd_cid_secundario6_w,
				ie_classif_diag_seq6_w,
				cd_cid_secundario7_w,
				ie_classif_diag_seq7_w,
				cd_cid_secundario8_w,
				ie_classif_diag_seq8_w,
				cd_cid_secundario9_w,
				ie_classif_diag_seq9_w);
			
			end;	
			ie_identificacao_aih_w	:= '03';
			ds_registro_conta_w	:= '';
			
			qt_mes_gestacao_w	:= null;
			ie_saida_utineo_w	:= null;
			qt_peso_utineo_w	:= null;
		end if;
		qt_registros_w	:= qt_registros_w + 1;
	
		end;
	end loop;
	end;
end loop;



select	w_susaih_interf_conta_seq.nextval
		into	nr_sequencia_w
		from	dual;

insert into w_susaih_interf_conta(
		nr_sequencia,
		cd_modalidade,
		nr_seq_aih,
		nr_proxima_aih,
		nr_anterior_aih,
		dt_emissao,
		dt_internacao,
		dt_saida,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_procedimento_solic,
		ie_origem_proc_solic,
		ie_mudanca_proc,
		cd_procedimento_real,
		ie_origem_proc_real,
		ie_carater_inter_sus,
		ie_motivo_saida,
		ie_doc_medico_solic,
		nr_cpf_medico_solic,
		cd_cns_medico_solic,
		ie_doc_medico_resp,
		nr_cpf_medico_resp,
		cd_cns_medico_resp,
		ie_doc_diretor_cli,
		nr_cpf_diretor_cli,
		cd_cns_diretor_cli,
		ie_doc_medico_aut,
		nr_cpf_medico_aut,
		cd_cns_medico_aut,
		cd_cid_principal,
		cd_cid_causa_compl,
		cd_cid_causa_morte,
		cd_solic_liberacao,
		cd_pessoa_fisica,
		nm_paciente,
		dt_nascimento,
		ie_sexo,
		nm_mae,
		nm_responsavel,
		nr_pis_pasep,
		nr_identidade,
		nr_cert_nasc,
		nr_cpf,
		nr_cns,
		cd_nacionalidade,
		tp_logradouro,
		ds_logradouro,
		nr_logradouro,
		ds_compl_logradouro,
		ds_bairro,
		cd_municipio_ibge,
		sg_estado,
		cd_cep,
		cd_municipio_origem,
		nr_prontuario,
		nr_enfermaria,
		nr_leito,
		ie_saida_utineo,
		qt_peso_utineo,
		qt_mes_gestacao,
		cd_cgc_empregador,
		cd_cbor,
		cd_cnaer,
		ie_vinculo_prev,
		qt_nascido_vivo,
		qt_nascido_morto,
		qt_saida_alta,
		qt_saida_transferencia,
		qt_saida_obito,
		nr_gestante_prenatal,
		qt_filho,
		ie_grau_instrucao,
		cd_cid_indicacao,
		cd_cid_associado,
		ie_metodo_contracep_um,
		ie_metodo_contracep_dois,
		ie_alto_risco,
		cd_medico_solic,
		cd_medico_responsavel,
		cd_diretor_clinico,
		cd_medico_autorizador,
		nr_aih,
		nr_interno_conta,
		ds_registro_proc,
		ie_doc_paciente,
		nr_doc_paciente,
		nr_seq_cor_pele,
		cd_etnia,
		ie_identificacao_aih,
		nr_matricula_nasc,
		nr_telefone_pac,
		ds_justificativa_cns,
		cd_cid_secundario,
		ie_classif_diag_seq,
		cd_cid_secundario2,
		ie_classif_diag_seq2,
		cd_cid_secundario3,
		ie_classif_diag_seq3,
		cd_cid_secundario4,
		ie_classif_diag_seq4,
		cd_cid_secundario5,
		ie_classif_diag_seq5,
		cd_cid_secundario6,
		ie_classif_diag_seq6,
		cd_cid_secundario7,
		ie_classif_diag_seq7,
		cd_cid_secundario8,
		ie_classif_diag_seq8,
		cd_cid_secundario9,
		ie_classif_diag_seq9)
	values(	nr_sequencia_w,
		cd_modalidade_w,
		nr_seq_aih_w,
		nr_proxima_aih_w,
		nr_anterior_aih_w,
		dt_emissao_w,
		dt_entrada_w,
		dt_saida_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_procedimento_solic_w,
		ie_origem_proc_solic_w,
		ie_mudanca_proc_w,
		cd_procedimento_real_w,
		ie_origem_proc_real_w,
		cd_carater_internacao_w,
		ie_motivo_saida_w,
		ie_doc_medico_solic_w,
		nr_cpf_medico_solic_w,
		cd_cns_medico_solic_w,
		ie_doc_medico_resp_w,
		nr_cpf_medico_resp_w,
		cd_cns_medico_resp_w,
		ie_doc_diretor_cli_w,
		nr_cpf_diretor_cli_w,
		cd_cns_diretor_cli_w,
		ie_doc_medico_aut_w,
		nr_cpf_medico_aut_w,
		cd_cns_medico_aut_w,
		cd_cid_principal_w,
		cd_cid_causa_compl_w,
		cd_cid_causa_morte_w,
		cd_solic_liberacao_w,
		cd_pessoa_fisica_w,
		nm_paciente_w,
		dt_nascimento_w,
		ie_sexo_w,
		nm_mae_w,
		nm_responsavel_w,
		nr_pis_pasep_w,
		nr_identidade_w,
		nr_cert_nasc_w,
		nr_cpf_w,
		nr_cns_w,
		cd_nacionalidade_w,
		tp_logradouro_w,
		ds_logradouro_w,
		nr_logradouro_w,
		ds_compl_logradouro_w,
		ds_bairro_w,
		cd_municipio_ibge_w,
		sg_estado_w,
		cd_cep_w,
		cd_municipio_origem_w,
		nr_prontuario_w,
		nr_enfermaria_w,
		nr_leito_w,
		ie_saida_utineo_w,
		qt_peso_utineo_w,
		qt_mes_gestacao_w,
		cd_cgc_empregador_w,
		cd_cbor_w,
		cd_cnaer_w,
		ie_vinculo_prev_w,
		qt_nascido_vivo_w,
		qt_nascido_morto_w,
		qt_saida_alta_w,
		qt_saida_transferencia_w,
		qt_saida_obito_w,
		nr_gestante_prenatal_w,
		qt_filho_w,
		ie_grau_instrucao_w,
		cd_cid_indicacao_w,
		cd_cid_associado_w,
		ie_metodo_contracep_um_w,
		ie_metodo_contracep_dois_w,
		ie_alto_risco_w,
		cd_medico_solic_w,
		cd_medico_responsavel_w,
		cd_diretor_clinico_w,
		cd_medico_autorizador_w,
		nr_aih_w,
		nr_interno_conta_p,
		rpad(ds_registro_conta_w,711,'0'),
		ie_doc_paciente_w,
		nr_doc_paciente_w,
		nr_seq_cor_pele_w,
		cd_etnia_w,
		ie_identificacao_aih_w,
		nr_matricula_nasc_w,
		nr_telefone_pac_w,
		substr(upper(Elimina_Acentuacao(elimina_caracteres_especiais(substr(ds_justificativa_cns_w,1,200)))),1,50),
		cd_cid_secundario_w,
		ie_classif_diag_seq_w,
		cd_cid_secundario2_w,
		ie_classif_diag_seq2_w,
		cd_cid_secundario3_w,
		ie_classif_diag_seq3_w,
		cd_cid_secundario4_w,
		ie_classif_diag_seq4_w,
		cd_cid_secundario5_w,
		ie_classif_diag_seq5_w,
		cd_cid_secundario6_w,
		ie_classif_diag_seq6_w,
		cd_cid_secundario7_w,
		ie_classif_diag_seq7_w,
		cd_cid_secundario8_w,
		ie_classif_diag_seq8_w,
		cd_cid_secundario9_w,
		ie_classif_diag_seq9_w);

commit;
	
end gerar_interf_conta_susaih;
/
