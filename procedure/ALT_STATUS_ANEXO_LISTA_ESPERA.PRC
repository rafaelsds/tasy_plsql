create or replace
procedure alt_status_anexo_lista_espera (	nr_seq_lista_espera_p number,
						nr_seq_status_p number ,
						nm_usuario_p 	varchar2) is

begin

if	((nr_seq_lista_espera_p > 0) and (nr_seq_status_p > 0)) then
	update	anexo_agenda
	set	nr_seq_status = nr_seq_status_p,
		nm_usuario = nm_usuario_p
	where	nr_seq_lista_espera = nr_seq_lista_espera_p;
	commit;
end if;

end alt_status_anexo_lista_espera;
/