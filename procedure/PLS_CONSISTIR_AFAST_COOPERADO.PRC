create or replace
procedure pls_consistir_afast_cooperado(nr_seq_cooperado_p	number,
					nr_seq_ausencia_p	number,
					nr_seq_motivo_p		number,
					dt_inicio_p		date,
					dt_fim_p		date) is

qt_dias_afast_w		number(10);
ie_periodo_w		varchar(3);
qt_limite_regra_w	number(10);
qt_dias_afast_ano_w	number(10);
qt_ausencia_w		number(10);

Cursor c01 is
select	a.ie_periodo,
	a.qt_limite
from	pls_regra_afast_coop a
where	dt_inicio_p between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia,dt_inicio_p)
and	nvl(a.nr_seq_cooperado,nr_seq_cooperado_p)	= nr_seq_cooperado_p
and	nvl(a.nr_seq_motivo,nr_seq_motivo_p)		= nr_seq_motivo_p
order by
	nvl(a.nr_seq_cooperado,0);

begin
	
if	(nr_seq_cooperado_p is not null) then
	select	count(*)
	into	qt_ausencia_w
	from	pls_cooperado_ausencia	a
	where	a.nr_sequencia	<> nr_seq_ausencia_p
	and	a.nr_seq_cooperado = nr_seq_cooperado_p
	and	(a.dt_inicio between dt_inicio_p and dt_fim_p
		or nvl(a.dt_fim, sysdate) between dt_inicio_p and dt_fim_p)
	and	rownum = 1;
		
	if	(qt_ausencia_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(199351);
	end if;
	
	qt_dias_afast_w	:= obter_dias_entre_datas(trunc(dt_inicio_p,'dd'),fim_dia(nvl(dt_fim_p,to_date('31/12/' || to_char(sysdate,'yyyy'),'dd/mm/yyyy')))) + 1;

	open c01;
	loop
	fetch c01 into	
		ie_periodo_w,
		qt_limite_regra_w;
	exit when c01%notfound;
		begin					
		if	(ie_periodo_w = 'A') then
			select	nvl(sum(pls_obter_qt_dias_afast_coop(a.nr_sequencia)),0)
			into	qt_dias_afast_ano_w
			from	pls_cooperado_ausencia a
			where	a.nr_seq_cooperado 	= nr_seq_cooperado_p
			and	a.nr_seq_motivo 	= nr_seq_motivo_p
			and	a.dt_inicio between trunc(sysdate,'year') and to_date('31/12/' || to_char(sysdate,'yyyy'),'dd/mm/yyyy')
			and	a.nr_sequencia	<> nr_seq_ausencia_p;

			if	(qt_dias_afast_ano_w + qt_dias_afast_w > qt_limite_regra_w) then
				wheb_mensagem_pck.exibir_mensagem_abort(199352,	'QT_LIMITE_REGRA_W='	|| to_char(qt_limite_regra_w) ||
										';QT_DIAS_AFASTADOS_W='	|| to_char(qt_dias_afast_ano_w + qt_dias_afast_w));
			end if;
		elsif	(ie_periodo_w = 'S') then
			if	(dt_inicio_p > last_day(add_months(trunc(sysdate,'year'),6)) ) then
				select 	nvl(sum(pls_obter_qt_dias_afast_coop(a.nr_sequencia)),0)
				into	qt_dias_afast_ano_w
				from	pls_cooperado_ausencia a
				where	a.nr_seq_cooperado	= nr_seq_cooperado_p
				and	a.nr_seq_motivo 	= nr_seq_motivo_p
				and	a.dt_inicio between to_date('01/07/' || to_char(sysdate,'yyyy'),'dd/mm/yyyy') 
						and to_date('31/12/' || to_char(sysdate,'yyyy'),'dd/mm/yyyy')
				and	a.nr_sequencia <> nr_seq_ausencia_p;
			else
				select	nvl(sum(pls_obter_qt_dias_afast_coop(a.nr_sequencia)),0)
				into	qt_dias_afast_ano_w
				from	pls_cooperado_ausencia a
				where	a.nr_seq_cooperado	= nr_seq_cooperado_p
				and	a.nr_seq_motivo 	= nr_seq_motivo_p
				and	a.dt_inicio between trunc(sysdate,'year') and 
						last_day(to_date('01/06/' || to_char(sysdate,'yyyy'),'dd/mm/yyyy'))
				and	a.nr_sequencia <> nr_seq_ausencia_p;
			end if;

			if	(qt_dias_afast_ano_w + qt_dias_afast_w > qt_limite_regra_w) then
				wheb_mensagem_pck.exibir_mensagem_abort(199354,	'QT_LIMITE_REGRA_W='	|| to_char(qt_limite_regra_w) ||
										';QT_DIAS_AFASTADOS_W='	|| to_char(qt_dias_afast_ano_w + qt_dias_afast_w));
			end if;
		end if;
		end;
	end loop;
	close c01;
end if;

end pls_consistir_afast_cooperado;
/