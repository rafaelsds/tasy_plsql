create or replace
procedure GERAR_DESCONTO_TERC_ITEM
                                (nr_repasse_terceiro_p  in number,
                                nr_seq_terceiro_item_p  in number,
                                vl_desconto_p           in number,
                                ds_observacao_p         in varchar2,
                                nm_usuario_p            in varchar2) is

nr_sequencia_item_w     number(10);
qt_venc_w               number(10);

begin

select  count(*)
into    qt_venc_w
from    repasse_terceiro_venc
where   nr_repasse_terceiro     = nr_repasse_terceiro_p;

if      (qt_venc_w <> 0) then
        wheb_mensagem_pck.exibir_mensagem_abort(340065);
end if;

if      (vl_desconto_p is not null) then

        select  max(nr_sequencia_item) + 1
        into    nr_sequencia_item_w
        from    repasse_terceiro_item
        where   nr_repasse_terceiro     = nr_repasse_terceiro_p;

        insert into repasse_terceiro_item
                (nr_repasse_terceiro,
                nr_sequencia_item,
                vl_repasse,
                dt_atualizacao,
                nm_usuario,
                nr_lote_contabil,
                ds_observacao,
                cd_conta_contabil,
                nr_seq_trans_fin,
                nr_seq_regra,
                cd_medico,
                nr_seq_regra_esp,
                dt_lancamento,
                cd_centro_custo,
                nr_seq_terceiro,
                dt_liberacao,
                nr_seq_trans_fin_prov,
                cd_conta_contabil_prov,
                cd_centro_custo_prov,
                nr_lote_contabil_prov,
                nr_sequencia,
                cd_conta_financ,
                cd_convenio,
                nr_seq_ret_glosa,
                nr_seq_terc_rep,
                ds_complemento,
                dt_contabil,
                qt_minuto,
                nr_seq_tipo,
                nr_atendimento,
                cd_procedimento,
                ie_origem_proced,
                ie_partic_tributo,
                nr_adiant_pago,
                cd_material,
                nr_seq_tipo_valor,
                nr_seq_terc_regra_esp,
                nr_seq_terc_regra_item)
        select  nr_repasse_terceiro,
                nr_sequencia_item_w,
                (vl_desconto_p * -1),
                sysdate,
                nm_usuario_p,
                nr_lote_contabil,
                substr(ds_observacao_p,1,4000),
                cd_conta_contabil,
                nr_seq_trans_fin,
                nr_seq_regra,
                cd_medico,
                nr_seq_regra_esp,
                dt_lancamento,
                cd_centro_custo,
                nr_seq_terceiro,
                null,
                nr_seq_trans_fin_prov,
                cd_conta_contabil_prov,
                cd_centro_custo_prov,
                nr_lote_contabil_prov,
                repasse_terceiro_item_seq.nextval,
                cd_conta_financ,
                cd_convenio,
                nr_seq_ret_glosa,
                nr_seq_terc_rep,
                null,
                dt_contabil,
                qt_minuto,
                nr_seq_tipo,
                nr_atendimento,
                cd_procedimento,
                ie_origem_proced,
                ie_partic_tributo,
                nr_adiant_pago,
                cd_material,
                nr_seq_tipo_valor,
                nr_seq_terc_regra_esp,
                nr_seq_terc_regra_item
        from    repasse_terceiro_item
        where   nr_repasse_terceiro     = nr_repasse_terceiro_p
        and     nr_sequencia_item       = nr_seq_terceiro_item_p;

        commit;
end if;

end GERAR_DESCONTO_TERC_ITEM;
/