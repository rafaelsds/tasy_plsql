create or replace
procedure excluir_rediluicao_GPE(	nr_prescricao_p 	number, 
					nr_sequencia_p	 	number,
					cd_perfil_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2,
					ds_retorno_p out 	varchar2) is 
					
qt_diluente_w	number(15);					

begin

select 	count(*)  
into	qt_diluente_w        
from 	prescr_material     
where 	nr_prescricao = nr_prescricao_p
and 	nr_sequencia_diluicao = nr_sequencia_p
and 	ie_agrupador = 7;

excluir_rediluicao(nr_prescricao_p,nr_sequencia_p,cd_perfil_p,cd_estabelecimento_p,nm_usuario_p);

if (qt_diluente_w > 0) then
	--Rediluente excluido com sucesso!
	ds_retorno_p := obter_texto_dic_objeto(110205,wheb_usuario_pck.get_nr_seq_idioma,null);
else
	--N�o existe rediluente para e o medicamento!
	ds_retorno_p := obter_texto_dic_objeto(110206,wheb_usuario_pck.get_nr_seq_idioma,null);
end if;	


commit;

end excluir_rediluicao_GPE;
/