create or replace
procedure inserir_rxt_agenda_integracao(
				cd_pessoa_fisica_p varchar2,
				cd_agendamento_externo_p varchar2,
				dt_agenda_p date,
				nr_minuto_duracao_p int,
				cd_equip_externo_p varchar2,
				ds_integracao_p varchar2,
				tipo_mensagem_p varchar2,
        				codigo_mensagem_p varchar2
				) is

nr_seq_tratamento_w varchar2(10);
nr_seq_equipamento_w number;
nr_seq_tumor_w number;
nr_seq_rxt_tipo_w number;
ie_tipo_equip_w varchar2(20);

begin

  if('SIU' = tipo_mensagem_p and 'S12' = codigo_mensagem_p) then

	select max(nr_sequencia),
  	nvl(max(ie_tipo),'A')
	into nr_seq_equipamento_w,
  	ie_tipo_equip_w
	from rxt_equipamento
	where cd_equip_externo = cd_equip_externo_p;

	select max(a.nr_sequencia)
	into nr_seq_rxt_tipo_w
	from rxt_tipo a
	where exists (select 1 from rxt_equip_tipo b
	where b.nr_seq_equipamento = nr_seq_equipamento_w
	and nr_seq_tipo = a.nr_sequencia)
	and a.ie_situacao = 'A';

	select max(nr_sequencia)
	into nr_seq_tumor_w
	from rxt_tumor
	where cd_pessoa_fisica = cd_pessoa_fisica_p
	and nr_seq_tipo = nr_seq_rxt_tipo_w;

	select max(nr_sequencia)
	into nr_seq_tratamento_w
	from rxt_tratamento
	where nr_seq_tumor = nr_seq_tumor_w;
     
 	 if (nr_seq_tratamento_w is not null and nr_seq_equipamento_w is not null) then
  
		insert into rxt_agenda (
		nr_sequencia,
		dt_atualizacao_nrec,     
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario,  
		nr_seq_tratamento,  
		nr_seq_equipamento,
		dt_agenda,      
		nr_minuto_duracao,
		ie_status_agenda,
		dt_agendamento,
  		ie_tipo_agenda,
  		cd_agendamento_externo
		) values (
		rxt_agenda_seq.nextval,
		sysdate,
		ds_integracao_p,
		sysdate,
		ds_integracao_p,
		nr_seq_tratamento_w,
		nr_seq_equipamento_w,
		dt_agenda_p,
		nr_minuto_duracao_p,
		'M',
		sysdate,
  		decode(ie_tipo_equip_w, 'A', 'T','S'),
 		cd_agendamento_externo_p
		);
  	end if;
  
  else
  	update rxt_agenda
  	set dt_agenda = dt_agenda_p
  	where cd_agendamento_externo = cd_agendamento_externo_p;
  end if;
                         
end inserir_rxt_agenda_integracao;
/