create or replace
procedure	gc_cirurgia_beforepost_js(
					cd_pessoa_fisica_p	varchar2,
					dt_inicio_prevista_p	date,
					cd_convenio_p		number,
					cd_medico_cirurgiao_p	varchar2,
					cd_tipo_anestesia_p	varchar2,
					cd_medico_anestesista_p	varchar2,
					dt_inicio_real_p	date,
					dt_termino_p		date,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					ds_aviso_p                        out varchar2,
					ds_abort_p                        out varchar2
					)is

ie_perm_agendar_classif_w	varchar2(1);
ie_consiste_conveniado_w	varchar2(1);
ie_consiste_medico_w		varchar2(1);
ie_consistir_tipo_anestesia_w	varchar2(1);
ie_obriga_anestesia_w		varchar2(1);
ds_valor_dominio_w		varchar2(255);
qt_medico_convenio_w		number(10);
ds_aviso_w         	                               varchar2(255);
ds_abort_w				varchar2(255);
					
begin

obter_param_usuario(900, 61, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_medico_w);
obter_param_usuario(900, 98, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consistir_tipo_anestesia_w);
ie_perm_agendar_classif_w := obter_se_perm_pf_classif(900, 0, cd_pessoa_fisica_p, dt_inicio_prevista_p);

select	max(ie_medico_cooperado)
into	ie_consiste_conveniado_w
from	convenio_estabelecimento
where	cd_convenio = cd_convenio_p
and	cd_estabelecimento = cd_estabelecimento_p;


if	(ie_perm_agendar_classif_w = 'N') then
	begin
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(49940);
	end;
end if;

if	(ie_consiste_medico_w = 'S') and
	((ie_consiste_conveniado_w = 'S') or
	(ie_consiste_conveniado_w = 'A'))	then
	begin
	
	select	count(*) 
	into	qt_medico_convenio_w
	from	medico_convenio
	where	cd_convenio = cd_convenio_p
	and	cd_pessoa_fisica = cd_pessoa_fisica_p
	and	ie_conveniado    = 'S';
	
	if	(cd_medico_cirurgiao_p is not null) and
		(qt_medico_convenio_w = '0') then
		begin	
			if	(ie_consiste_conveniado_w = 'S') then
				ds_abort_w	:=  WHEB_MENSAGEM_PCK.get_texto(49944); 
			else
				ds_aviso_w	:=  WHEB_MENSAGEM_PCK.get_texto(49944); 
			end if;	
		end;
	end if;
	
	end;
end if;


ie_obriga_anestesia_w := obter_se_obriga_anestesia(cd_tipo_anestesia_p);

if	(ie_consistir_tipo_anestesia_w = 'S') and
	(ie_obriga_anestesia_w = 'S') and
	(cd_medico_anestesista_p is null) then
	begin
	
	ds_valor_dominio_w := obter_valor_dominio(36, cd_tipo_anestesia_p);
	
	wheb_mensagem_pck.exibir_mensagem_abort(49951,'DS_VALOR_DOMINIO_W='||ds_valor_dominio_w);
	end;
end if;

if	(dt_inicio_real_p is null) and
	(dt_termino_p is not null) then
	begin
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(49950);
	end;
end if;

ds_aviso_p := ds_aviso_w;
ds_abort_p := ds_abort_w;

end gc_cirurgia_beforePost_js;
/
