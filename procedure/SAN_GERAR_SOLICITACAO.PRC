create or replace
procedure san_gerar_solicitacao(	nr_seq_entidade_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_P	number,
				nr_seq_solicitacao_p	out varchar2) is 
nr_seq_solicitacao_w	number(10);
cd_pf_usuario_w		varchar2(100);
begin
if	(nr_seq_entidade_p is not null) then
	
	select	san_solicitacao_seq.nextval
	into	nr_seq_solicitacao_w
	from	dual;
	
	select	substr(Obter_Pf_Usuario(nm_usuario_p,'C'),1,100)
	into	cd_pf_usuario_w
	from	dual;
	
	
	insert into san_solicitacao (	
		nr_sequencia,
		nr_seq_entidade,
		cd_pf_solic,
		dt_solicitacao,
		cd_estabelecimento,
		dt_atualizacao,
		nm_usuario,
		ie_tipo_solicitacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec) 
	values(	nr_seq_solicitacao_w,
		nr_seq_entidade_p,
		cd_pf_usuario_w,
		sysdate,
		cd_estabelecimento_p,
		sysdate,
		nm_usuario_p,
		'E',
		nm_usuario_p,
		sysdate);
	
	nr_seq_solicitacao_p := nr_seq_solicitacao_w;
	
end if;	

commit;

end san_gerar_solicitacao;
/