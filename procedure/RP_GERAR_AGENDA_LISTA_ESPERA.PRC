create or replace
procedure rp_gerar_agenda_lista_espera(nr_seq_lista_espera_p	number,
			cd_pessoa_fisica_p	varchar2,
			dt_inicial_p		date,
			dt_final_p		date,
			ie_dia_semana_p		varchar2,
			cd_setor_atendimento_p	number,
			dt_horario_p		varchar2,
			cd_estabelecimento_p	number,			
			nm_usuario_p		Varchar2,
			ds_erro_p	out	varchar2) is 

cd_agenda_w		number(10,0);
cd_medico_exec_w	varchar2(10);
--ie_dia_semana_w		number(1,0);
dt_atual_w		date;
dt_atual_ww		date;
nr_sequencia_w		varchar2(10);
nr_seq_agenda_w		number(10,0);
cd_perfil_w		number(5,0);
nm_pessoa_fisica_w	varchar2(100);
ds_agenda_w		varchar2(100);
dt_nascimento_w		date;
qt_idade_w		number(10,0);
ds_enter_w		varchar2(20)	:= chr(10)||chr(13);
nr_seq_agend_item_w	number(10,0);
ie_classif_agenda_w	varchar2(5);

ie_status_agenda_w	varchar2(2);
ds_motivo_status_w	varchar2(255);
qt_agendado_w		number(10,0);
ie_feriado_w		varchar2(1);
nr_seq_status_pac_w	number(10,0);
nr_seq_status_agendamento_w	number(10,0);
ie_modelo_w		varchar2(1);

nr_seq_agendado_w	number(10,0);
cd_medico_agendado_w	varchar2(10);

ds_motivo_licenca_w	varchar2(255);
nr_seq_licenca_w	number(10,0);
cd_classificacao_w	varchar2(05)	:= 'E';
nr_seq_hora_w		NUMBER(0010,0)  := 0;
cd_convenio_w		number(5);
ds_observacao_w		varchar2(255);
--cd_setor_Atendimento_w	number(10);

ie_data_trat_w		varchar2(1);

nr_seq_ausencia_prof_w	number(10,0);
cd_motivo_cancel_prof_aus_w	varchar2(3);
cd_motivo_cancelamento_w	varchar2(3);
ie_status_agenda_prof_aus_w	varchar2(3);
ds_motivo_licenca_prof_w 	varchar2(255);
nr_seq_status_trat_w		number(10,0);
nr_seq_tratamento_w		number(10,0);

nr_seq_pac_reab_bloqueio_w 	number(10,0);
nr_seq_status_reab_w		number(10,0);
ie_status_agenda_susp_w		varchar2(10);
qt_intervalo_w			number(5);
ie_invervalo_nulo_w		varchar2(1);

cd_categoria_w			varchar2(10);
cd_plano_w			varchar2(10);
cd_tipo_acomodacao_w		number(4);
dt_validade_w			date;
cd_usuario_convenio_w		varchar2(30);
nr_doc_convenio_w		varchar2(20);

ie_atualiza_conv_atend_w	varchar2(1);
nr_seq_pac_reab_w 		number(10);

			
Cursor C01 is
	select	cd_agenda,
		cd_medico_resp		
	from	rp_lista_espera_modelo
	where	nr_sequencia = nr_seq_lista_espera_p;
	
Cursor C02 is
	select	nr_sequencia,
			substr(DS_OBSERVACAO,1,255),
			substr(rp_obter_desc_mot_licenca(nr_seq_motivo_licenca),1,255)
	from	rp_licenca
	where	nr_seq_pac_reab = nr_seq_pac_reab_w
	and		dt_atual_w between dt_periodo_inicial and dt_periodo_final
	and		nvl(cd_estabelecimento,cd_estabelecimento_p) = cd_estabelecimento_p
	and		dt_liberacao is not null
	order by dt_periodo_inicial;	
	
	
Cursor C03 is
	select 	 nr_sequencia,
		 substr(rp_obter_desc_mot_licenca(a.nr_seq_motivo_licenca),1,255)
	from   	 rp_licenca_profissional a
	where  	 a.cd_pessoa_fisica = cd_medico_exec_w
	and	 dt_atual_w between dt_periodo_inicial and dt_periodo_final
	and	 a.dt_liberacao is not null;	
	
Cursor C04 is
select 	nr_sequencia,
	nr_seq_status
from   	rp_tratamento
where	nr_seq_pac_reav = nr_seq_pac_reab_w
and	dt_fim_tratamento is null;	
			
begin
ds_erro_p	:= 	'';
dt_atual_w	:=	trunc(dt_inicial_p,'dd');
ie_status_agenda_susp_w := null;
obter_param_usuario(866,15,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_atualiza_conv_atend_w);

select	max(nr_sequencia)
into	nr_seq_pac_reab_w
from	rp_paciente_reabilitacao
where	cd_pessoa_fisica = cd_pessoa_fisica_p;

if	(ie_atualiza_conv_atend_w = 'S') then
	define_convenio_atend_agenda(cd_pessoa_fisica_p,nm_usuario_p,cd_convenio_w,cd_categoria_w,dt_validade_w,cd_usuario_convenio_w,nr_doc_convenio_w,cd_tipo_acomodacao_w,cd_plano_w);
end if;	

select	max(nr_seq_pac_reab_agenda),
	max(ie_status_agenda_prof_aus),
	max(cd_motivo_cancelamento)
into	nr_seq_status_agendamento_w,
	ie_status_agenda_prof_aus_w,
	cd_motivo_cancel_prof_aus_w
from	rp_parametros
where	cd_estabelecimento = cd_estabelecimento_p;

dt_atual_ww	:= dt_atual_w;
while	(dt_atual_w <= dt_final_p) loop
	begin
	if (dt_atual_ww = dt_atual_w) then
		open C01;
		loop
		fetch C01 into	
			cd_agenda_w,
			cd_medico_exec_w;
		exit when C01%notfound;
			begin
			
			open C02;
			loop
			fetch C02 into
				nr_seq_licenca_w,
				ds_observacao_w,
				ds_motivo_licenca_w;
			exit when C02%notfound;
			end loop;
			close C02;

			if	(nr_seq_licenca_w > 0) then
				begin
				ie_status_agenda_w	:= 'F';
				ds_motivo_status_w	:= ds_motivo_licenca_w;
				end;
			else
				begin
				ie_status_agenda_w	:= 'N';
				ds_motivo_status_w	:= '';
				cd_motivo_cancelamento_w	:= '';
				end;
			end if;
			
			
			if	(ie_status_agenda_prof_aus_w is not null) then
					
				nr_seq_ausencia_prof_w	:= 0;
				
				open C03;
				loop
				fetch C03 into	
					nr_seq_ausencia_prof_w,
					ds_motivo_licenca_prof_w;
				exit when C03%notfound;
				end loop;
				close C03;
									
				if	(nr_seq_ausencia_prof_w > 0) then
					ie_status_agenda_w	:= ie_status_agenda_prof_aus_w;
					ds_motivo_status_w	:= ds_motivo_licenca_prof_w;
					if	(ie_status_agenda_w	= 'C') then
						cd_motivo_cancelamento_w	:= cd_motivo_cancel_prof_aus_w;
					else
						cd_motivo_cancelamento_w	:= '';
					end if;
				else
					ie_status_agenda_w	:= 'N';
					ds_motivo_status_w	:= '';
					cd_motivo_cancelamento_w := '';
				end if;		
			
			end if;
			
			select	substr(obter_nome_pf(cd_pessoa_fisica_p),1,100),
				substr(obter_nome_agenda(cd_agenda_w),1,100),				
				to_number(obter_idade_pf(cd_pessoa_fisica_p,sysdate,'A'))
			into	nm_pessoa_fisica_w,
				ds_agenda_w,				
				qt_idade_w
			from	dual;	
			
			select 	max(dt_nascimento)
			into 	dt_nascimento_w
			from 	pessoa_fisica
			where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
			
			/* Consist�ncia se o paciente j� possui agendamento no dia */
			
			select	count(*)
			into	qt_agendado_w
			from	agenda_consulta
			where	trunc(dt_agenda,'dd')   = trunc(dt_atual_w)
			and	cd_pessoa_fisica	= cd_pessoa_fisica_p
			and	ie_status_agenda 	<> 'C'
			and	to_char(dt_agenda,'hh24:mi') = dt_horario_p
			and	nr_seq_rp_item_ind is not null;
			
			if	(qt_agendado_w > 0) then
				Wheb_mensagem_pck.exibir_mensagem_abort(220031);
			end if;
			
			/* Verificando se o dia � feriado */
			select	decode(count(*),0,'N','S')
			into	ie_feriado_w
			from 	feriado a,
				agenda b
			where 	a.cd_estabelecimento = obter_estab_agenda(cd_agenda_w)
			and	trunc(a.dt_feriado) = trunc(dt_atual_w)
			and 	b.cd_agenda = cd_agenda_w
			and	nvl(b.ie_feriado,'S') = 'N';
			
			
			if	(obter_cod_dia_semana(dt_atual_w) = ie_dia_semana_p) and
				(qt_agendado_w = 0) then
				begin
				if	((ie_feriado_w <> 'S') or (obter_se_agenda_feriado(cd_agenda_w) = 'S')) then
					begin
					
					Gerar_Horario_Agenda_Servico(cd_estabelecimento_p,cd_agenda_w,dt_atual_w,nm_usuario_p);

					select	nvl(max(nr_sequencia),0)
					into	nr_sequencia_w
					from	agenda_consulta
					where	cd_agenda = cd_agenda_w
					and	trunc(dt_agenda,'dd') = dt_atual_w
					and	to_char(dt_agenda,'hh24:mi') = dt_horario_p
					and	nvl(ie_classif_agenda,'X') = nvl(nvl(ie_classif_agenda_w,ie_classif_agenda),'X')
					and	ie_status_agenda = 'L';
					
					if	(nr_sequencia_w > 0) then
						begin
						update	agenda_consulta
						set	cd_pessoa_fisica	= cd_pessoa_fisica_p,
							cd_medico		= cd_medico_exec_w,
							dt_atualizacao		= sysdate,
							nm_usuario		= nm_usuario_p,
							nr_seq_rp_item_ind 	= nr_seq_agend_item_w,
							qt_idade_pac		= qt_idade_w,
							dt_nascimento_pac	= dt_nascimento_w,
							ie_status_agenda	= decode(ie_status_agenda_susp_w,null,ie_status_agenda_w,ie_status_agenda_susp_w),
							ds_motivo_status	= ds_motivo_status_w,
							cd_convenio		= cd_convenio_w,
							cd_Setor_atendimento	= cd_setor_Atendimento_p,
							cd_motivo_cancelamento	= cd_motivo_cancelamento_w,
							cd_categoria		= nvl(cd_categoria_w,cd_categoria),
							cd_plano		= nvl(cd_plano_w,cd_plano),
							cd_tipo_acomodacao      = nvl(cd_tipo_acomodacao_w,cd_tipo_acomodacao),
							cd_usuario_convenio	= nvl(cd_usuario_convenio_w,cd_usuario_convenio),
							DS_OBSERVACAO = nvl(ds_observacao_w, DS_OBSERVACAO),
							dt_validade_carteira    = nvl(dt_validade_w,dt_validade_carteira)
						where	nr_sequencia		= nr_sequencia_w;

						end;
					else
						begin

						select nvl(max(cd_classificacao), 'E')
						into	cd_classificacao_w
						from 	agenda_turno_classif;

						select	agenda_consulta_seq.nextval
						into	nr_seq_agenda_w
						from	dual;

						select	nvl(max(nr_seq_hora),0) + 1
						into	nr_seq_hora_w
						from	agenda_consulta
						where	cd_agenda	= cd_agenda_w
						and	dt_agenda	= to_date(to_char(dt_atual_w,'dd/mm/yyyy')||' '||dt_horario_p,'dd/mm/yyyy hh24:mi:ss');

						insert into agenda_consulta(
							nr_sequencia,
							cd_agenda,
							dt_agenda,
							nr_minuto_duracao,
							cd_pessoa_fisica,							
							ie_status_agenda,
							ie_classif_agenda,
							dt_atualizacao,
							nm_usuario,
							cd_medico,
							ie_encaixe,
							dt_agendamento,
							nm_usuario_origem,
							nr_seq_rp_item_ind,
							ds_motivo_status,
							nr_seq_hora,
							nm_paciente,
							cd_convenio,
							cd_setor_atendimento,
							cd_motivo_cancelamento,
							cd_categoria,	
							cd_plano,	
							cd_tipo_acomodacao,
							cd_usuario_convenio,
							ds_observacao,
							dt_validade_carteira)
						values (
							nr_seq_agenda_w,
							cd_agenda_w,
							to_date(to_char(dt_atual_w,'dd/mm/yyyy')||' '||dt_horario_p,'dd/mm/yyyy hh24:mi:ss'),
							0,
							cd_pessoa_fisica_p,							
							decode(ie_status_agenda_susp_w,null,ie_status_agenda_w,ie_status_agenda_susp_w),
							nvl(ie_classif_agenda_w,cd_classificacao_w),
							sysdate,
							nm_usuario_p,
							cd_medico_exec_w,
							'S',
							sysdate,
							nm_usuario_p,
							nr_seq_agend_item_w,
							ds_motivo_status_w,
							nr_seq_hora_w,
							substr(obter_nome_pf(cd_pessoa_fisica_p),1,100),
							cd_convenio_w,
							cd_setor_Atendimento_p,
							cd_motivo_cancelamento_w,
							cd_categoria_w,	
							cd_plano_w,	
							cd_tipo_acomodacao_w,
							cd_usuario_convenio_w,
							ds_observacao_w,
							dt_validade_w);
							
						if	(dt_nascimento_w is not null) then
							update	agenda_consulta
							set	dt_nascimento_pac = dt_nascimento_w,
								qt_idade_pac = obter_idade(dt_nascimento_w,sysdate,'A')
							where	nr_sequencia = nr_seq_agenda_w;									
						end if;		

						end;
					end if;					
					
					if	(nr_seq_status_agendamento_w is not null) and
						(nr_seq_status_pac_w <> nr_seq_status_agendamento_w) and
						(ie_status_agenda_susp_w is null) then

						update	rp_paciente_reabilitacao
						set	nr_seq_status	= nr_seq_status_agendamento_w
						where	nr_sequencia	= nr_seq_pac_reab_w;

					end if;
					
					if	(nr_seq_status_agendamento_w is not null) and (ie_status_agenda_susp_w is null)  then
						open C04;
						loop
						fetch C04 into	
							nr_seq_tratamento_w,
							nr_seq_status_trat_w;
						exit when C04%notfound;
							begin
							if	((nr_seq_status_trat_w is null) or (nr_seq_status_trat_w <> nr_seq_status_agendamento_w)) then
								update	rp_tratamento
								set	nr_seq_status	= nr_seq_status_agendamento_w
								where	nr_sequencia	= nr_seq_tratamento_w;
							end if;
							
							end;
						end loop;
						close C04;
					end if;
					
					end;
	
				end if;	
											
				nr_seq_licenca_w   := 0;
				nr_seq_ausencia_prof_w	:= 0;
				
				
				end;
			end if;	
			
			end;
			end loop;
			close C01;
	
	end if;
	
	nr_seq_licenca_w   := 0;
	nr_seq_ausencia_prof_w	:= 0;
	if (dt_atual_ww = dt_atual_w) then
		dt_atual_ww	:= dt_atual_w + nvl(qt_intervalo_w,1);
	end if;
	dt_atual_w	:= dt_atual_w + 1;
	
	end;
	
end loop;

commit;

end rp_gerar_agenda_lista_espera;
/
