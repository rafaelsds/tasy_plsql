create or replace procedure REG_INSERT_URS_ANALISE_IMPACTO(nr_sequencia_p         number,
                                                           nr_seq_impacto_p       number,
                                                           nm_usuario_p           varchar2,
                                                           ie_impacto_requisito_p varchar2) is

  nr_seq_modelo_w reg_modelo_analise_impacto.nr_seq_modelo%type;

begin

  select max(nr_seq_modelo)
    into nr_seq_modelo_w
    from reg_modelo_analise_impacto
   where ie_documento = 'URS'
     and nr_seq_intencao_uso = (select imp.nr_seq_intencao_uso
                                  from man_ordem_serv_impacto imp
                                 where imp.nr_sequencia = nr_seq_impacto_p);

  insert into MAN_ORDEM_SERV_IMP_CR
    (NR_SEQUENCIA,
     DT_ATUALIZACAO,
     NM_USUARIO,
     DT_ATUALIZACAO_NREC,
     NM_USUARIO_NREC,
     NR_SEQ_IMPACTO,
     NR_CUSTOMER_REQUIREMENT,
     IE_IMPACTO_REQUISITO,
     DS_NEW_TITLE,
     NR_SEQ_MODELO)
  values
    (man_ordem_serv_imp_cr_seq.nextval,
     sysdate,
     nm_usuario_p,
     sysdate,
     nm_usuario_p,
     nr_seq_impacto_p,
     nr_sequencia_p,
     ie_impacto_requisito_p,
     null,
     nr_seq_modelo_w);

  commit;

end REG_INSERT_URS_ANALISE_IMPACTO;
/
