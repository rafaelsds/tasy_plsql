CREATE OR REPLACE
PROCEDURE Hem_Gerar_Diametro_Medio	(qt_diametroa_p		Number,
					qt_diametrob_p		Number,
					qt_resultado_p out	Number) is

BEGIN

qt_resultado_p := (nvl(qt_diametroa_p,0) + nvl(qt_diametrob_p,0)) / 2;

END	Hem_Gerar_Diametro_Medio;
/