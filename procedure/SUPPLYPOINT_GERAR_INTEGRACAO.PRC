create or replace
procedure supplypoint_gerar_integracao(	nr_seq_evento_p		number,
					ds_parametros_p		varchar2) is

jobno number;
nm_usuario_w		varchar2(30);
ds_comando_w		varchar2(2000);

begin

nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_comando_w := ' supplypoint_job_gerar_int('|| to_char(nr_seq_evento_p) || ', ''' || ds_parametros_p || ''','''|| nm_usuario_w ||''');';

dbms_job.submit(jobno, ds_comando_w);

end supplypoint_gerar_integracao;
/