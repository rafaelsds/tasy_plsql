create or replace
procedure gerar_med_result_exame (
		nm_usuario_p		varchar2,
		nr_seq_cliente_p		number) is

ds_sep_bv_w		varchar2(50);
dt_resultado_w		date;
nr_seq_exame_w		number(10,0);
nm_exame_w		varchar2(255);
ds_unidade_w		varchar2(30);
ie_formato_resultado_w	varchar2(1);
nr_seq_apresent_w		number(10,0);
ds_analise_result_w		varchar2(4000);
vl_exame_w		number(15,4);
vl_exame_mascara_w	varchar2(255);
ds_valor_exame_w		varchar2(255);
vl_resultado_w		varchar2(255);

nr_seq_wgrid_w		number(10,0);

ds_comando_update_w	varchar2(4000);

nr_seq_result_w		number(10,0);

type horarios is record (horario_w varchar2(255));
type vetor is table of horarios index by binary_integer;
vetor_w			vetor;
i			integer	:= 0;
j			integer	:= 0;
k			integer	:= 0;

cursor c01 is
select	dt_exame
from	med_result_exame
where	nr_seq_cliente = nr_seq_cliente_p
group by
	dt_exame;
	
cursor c02 is
select	dt_resultado
from	w_med_result_exame_hor
where	nm_usuario = nm_usuario_p
group by
	dt_resultado
order by
	dt_resultado desc;

cursor c03 is	
select	b.nr_sequencia,
	b.ds_exame,
	b.ds_unidade,
	b.ie_formato_resultado,
	b.nr_seq_apresent
from	med_exame_padrao b,
	med_result_exame a 
where	b.nr_sequencia = a.nr_seq_exame
and	a.nr_seq_aval is null
and	(a.vl_exame is not null or a.ds_valor_exame is not null)
and	a.nr_seq_cliente = nr_seq_cliente_p
group by
	b.nr_sequencia,
	b.ds_exame,
	b.ds_unidade,
	b.ie_formato_resultado,
	b.nr_seq_apresent;
	
cursor c04 is	
select	b.nr_sequencia,
	--replace(campo_mascara(a.vl_exame,nvl(b.qt_decimais,0)),'.',','),
	campo_mascara(a.vl_exame,nvl(b.qt_decimais,0)),
	a.ds_valor_exame
from	med_exame_padrao b,
	med_result_exame a 
where	b.nr_sequencia = a.nr_seq_exame
and	a.nr_seq_aval is null
and	a.nr_seq_cliente = nr_seq_cliente_p
and	a.dt_exame = dt_resultado_w
group by
	b.nr_sequencia,
	campo_mascara(a.vl_exame,nvl(b.qt_decimais,0)),
	a.ds_valor_exame;
	
cursor c05 is
select	a.nr_sequencia,
	a.nr_seq_exame,
	a.vl_exame
from	med_result_exame a 
where	a.nr_seq_cliente = nr_seq_cliente_p
and	a.dt_exame = dt_resultado_w
and	a.nr_seq_aval is null
and	a.vl_exame is not null
and	nvl(a.ie_tipo_result_alt,'N') = 'N'
group by
	a.nr_sequencia,
	a.nr_seq_exame,
	a.vl_exame;

begin


if	(nm_usuario_p is not null) and
	(nr_seq_cliente_p is not null) then
	begin
	
	delete 	w_med_result_exame_hor
	where	nm_usuario = nm_usuario_p;

	delete 	w_med_result_exame_grid
	where	nm_usuario = nm_usuario_p;
	
	ds_sep_bv_w	:= obter_separador_bv;
	
	open c01;
	loop
	fetch c01 into dt_resultado_w;
	exit when c01%notfound;
		begin
		insert into w_med_result_exame_hor (
			nm_usuario,
			dt_resultado)
		values (
			nm_usuario_p,
			dt_resultado_w);
		end;
	end loop;
	close c01;
	
	open c02;
	loop
	fetch c02 into dt_resultado_w;
	exit when c02%notfound;
		begin
		if	(i < 40) then
			begin
			i			:= i + 1;
			vetor_w(i).horario_w	:= to_char(dt_resultado_w,'dd/mm/yyyy hh24:mi:ss');
			end;
		end if;
		
		/* OS 300253 para atualizar o resultado quando alterado o cadastro */
		open c05;
		loop
		fetch c05 into	nr_seq_result_w,
				nr_seq_exame_w,
				vl_exame_w;
		exit when c05%notfound;
			begin
			med_altera_resultado_exame(nr_seq_result_w, vl_exame_w, nr_seq_cliente_p, nr_seq_exame_w);
			end;
		end loop;
		close c05;
		
		end;
	end loop;
	close c02;

	j	:= i;
	while	(j < 40) loop
		begin
		j			:= j + 1;
		vetor_w(j).horario_w	:= null;
		end;
	end loop;
	
	open c03;
	loop
	fetch c03 into	nr_seq_exame_w,
			nm_exame_w,
			ds_unidade_w,
			ie_formato_resultado_w,
			nr_seq_apresent_w;
	exit when c03%notfound;
		begin
		select	w_med_result_exame_grid_seq.nextval
		into	nr_seq_wgrid_w
		from	dual;
		
		ds_analise_result_w	:= substr(obter_tipo_result_exame(nr_seq_exame_w,nr_seq_cliente_p),1,4000);
		
		insert into w_med_result_exame_grid (
			nr_sequencia,
			nm_usuario,
			nr_seq_exame,
			nm_exame,
			ds_unidade,
			ie_formato_resultado,
			nr_seq_apresent,
			ds_analise_result)
		values (
			nr_seq_wgrid_w,
			nm_usuario_p,
			nr_seq_exame_w,
			nm_exame_w,
			ds_unidade_w,
			ie_formato_resultado_w,
			nr_seq_apresent_w,
			ds_analise_result_w);
		end;
	end loop;
	close c03;	
	
	while (k < 40) loop
		begin
		k		:= k + 1;
		dt_resultado_w	:= to_date(vetor_w(k).horario_w,'dd/mm/yyyy hh24:mi:ss');
		
		open c04;
		loop
		fetch c04 into	nr_seq_exame_w,
				vl_exame_mascara_w,
				ds_valor_exame_w;
		exit when c04%notfound;
			begin
			ds_comando_update_w	:=	' update w_med_result_exame_grid ' ||
							' set ds_result' || to_char(k) || ' = :vl_result ' ||
							' where nm_usuario = :nm_usuario ' ||
							' and nr_seq_exame = :nr_seq_exame ';
							
			vl_resultado_w		:= null;
							
			if	(vl_exame_mascara_w is not null) then
				begin
				vl_resultado_w	:= replace(vl_exame_mascara_w,'.',',');
				end;
			elsif	(ds_valor_exame_w is not null) then
				begin
				vl_resultado_w	:= ds_valor_exame_w;
				end;
			end if;
			
			if	(vl_resultado_w is not null) then
				begin
				exec_sql_dinamico_bv('TASYMED', ds_comando_update_w,	'vl_result=' || vl_resultado_w || ds_sep_bv_w ||
											'nm_usuario=' || nm_usuario_p || ds_sep_bv_w || 
											'nr_seq_exame=' || nr_seq_exame_w || ds_sep_bv_w);
				end;
			end if;
			end;
		end loop;
		close c04;		
		end;
	end loop;		
	
	commit;
	end;
end if;
end gerar_med_result_exame;
/