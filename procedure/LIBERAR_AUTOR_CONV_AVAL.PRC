create or replace
procedure liberar_autor_conv_aval(
			nr_sequencia_p		number,
			nm_usuario_p		varchar2) is 

begin

update	autorizacao_convenio_aval 
set	dt_atualizacao	= sysdate,      
	nm_usuario	= nm_usuario_p, 
	dt_liberacao	= sysdate,     
	nm_usuario_lib	= nm_usuario_p  
where	nr_sequencia	= nr_sequencia_p;

commit;

end liberar_autor_conv_aval;
/