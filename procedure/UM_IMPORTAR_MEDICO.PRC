create or replace
procedure um_importar_medico is 

cd_pessoa_fisica_w	varchar2(10);
nr_ddd_telefone_w		varchar2(4);
nr_telefone_w		varchar2(40);
nr_sequencia_w		number(10);
qt_registro_w		number(10);

cursor c01 is
select	cd_pessoa,
	nm_pessoa,
	dt_nasc, 
	ie_sexo,
	nr_seq_cor,
	nr_ident, 
	nr_cpf,
	ds_bairro,
	cd_cep, 
	nm_guerra,
	ds_enderec,
	sg_estado, 
	nr_telefon,
	dt_admissa,
	ie_vinc_me
from	um_medico;

vet01	C01%RowType;

begin

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	
	cd_pessoa_fisica_w	:= vet01.cd_pessoa;
	
	select	count(*)
	into	qt_registro_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
	
	if	(qt_registro_w = 0) then
	
		select	pessoa_fisica_seq.nextval
		into	cd_pessoa_fisica_w
		from	dual;
	
		insert into pessoa_fisica(
			cd_pessoa_fisica,
			nm_pessoa_fisica,
			ie_tipo_pessoa,
			dt_nascimento,
			ie_sexo,
			nr_seq_cor_pele,
			nr_identidade,
			nr_cpf,
			dt_admissao_hosp,
			nm_usuario,
			dt_atualizacao,
			cd_sistema_ant,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nm_pessoa_fisica_sem_acento,
			nm_pessoa_pesquisa)
		values(	cd_pessoa_fisica_w,
			vet01.nm_pessoa,
			2,
			vet01.dt_nasc,
			vet01.ie_sexo,
			vet01.nr_seq_cor,
			vet01.nr_ident,
			vet01.nr_cpf,
			vet01.dt_admissa,
			'Importacao',
			sysdate,
			vet01.cd_pessoa,
			'Importacao',
			sysdate,
			elimina_acentuacao(vet01.nm_pessoa),
			padronizar_nome(vet01.nm_pessoa));
			
	end if;
	
	select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
	
	nr_sequencia_w	:= nr_sequencia_w + 1;
	
	nr_ddd_telefone_w	:= substr(vet01.nr_telefon,1,4);
	nr_ddd_telefone_w	:= replace(nr_ddd_telefone_w,'(','');
	nr_ddd_telefone_w	:= replace(nr_ddd_telefone_w,')','');
	nr_ddd_telefone_w	:= substr('0' || nr_ddd_telefone_w,1,3);
	nr_telefone_w		:= ltrim(substr(vet01.nr_telefon,5,15));
	
	insert into compl_pessoa_fisica(
		cd_pessoa_fisica,
		ie_tipo_complemento,
		nr_sequencia,
		nm_contato,
		ds_bairro,
		ds_endereco,
		nr_ddd_telefone,
		nr_telefone,
		cd_cep,
		sg_estado,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec)
	values(	cd_pessoa_fisica_w,
		1,
		nr_sequencia_w,
		'',
		vet01.ds_bairro,
		vet01.ds_enderec,
		nr_ddd_telefone_w,
		nr_telefone_w,
		vet01.cd_cep,
		vet01.sg_estado,
		'Importacao',
		sysdate,
		'Importacao',
		sysdate);
	
	insert into medico(
		cd_pessoa_fisica,
		nr_crm,
		nm_guerra,
		ie_vinculo_medico,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_situacao,
		ie_corpo_clinico,
		ie_corpo_assist,
		dt_admissao)
	values(	cd_pessoa_fisica_w,
		'0',
		vet01.nm_guerra,
		vet01.ie_vinc_me,
		sysdate,
		'Importacao',
		sysdate,
		'Importacao',
		'A',
		'N',
		'N',
		vet01.dt_admissa);
	
	end;
end loop;
close C01;


commit;

end um_importar_medico;
/