create or replace
procedure inserir_hist_titulo_receb_hist(
		nr_titulo_p		number,
		ds_historico_p		varchar2,
		nm_usuario_p		varchar2) is 

begin

	if 	(nr_titulo_p is not null) and
		(ds_historico_p is not null) then
		
		insert into 	titulo_receber_hist (
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_titulo, 
				dt_historico,
				ds_historico,
				dt_liberacao) 
		values (	
				titulo_receber_hist_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_titulo_p,
				sysdate,
				ds_historico_p,
				sysdate);			
	commit;
			
	end if;

end inserir_hist_titulo_receb_hist;
/