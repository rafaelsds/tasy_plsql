create or replace
procedure confirmar_usuario_justific_js (
			nr_seq_item_dev_mat_pac_p	number,
			nr_devolucao_p			number,
			nm_usuario_p			varchar2 ) is 

begin

if	(nr_seq_item_dev_mat_pac_p is not null
	and	nr_devolucao_p is not null
	and	nm_usuario_p is not null ) then

	update	item_devolucao_material_pac
	set	nm_usuario_justif	= nm_usuario_p
	where	nr_sequencia		= nr_seq_item_dev_mat_pac_p
	and	nr_devolucao		= nr_devolucao_p;

end if;

commit;

end confirmar_usuario_justific_js;
/