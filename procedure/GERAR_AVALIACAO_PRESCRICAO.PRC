create or replace
procedure Gerar_Avaliacao_Prescricao
		(nr_prescricao_p	in  number,
		nm_usuario_p		in  varchar2,
		nr_seq_avaliacao_p	out number) is

cd_procedimento_w	number(15);
cd_especialidade_w	number(15);	
cd_area_procedimento_w	number(15);	
cd_grupo_proc_w		number(15);	
ie_origem_proced_w	number(10,0);
nr_seq_proc_interno_w	number(10,0);
nr_seq_exame_w		number(10,0);
nr_atendimento_w	number(10,0);
cd_pessoa_fisica_w	varchar2(10);
cd_medico_w		varchar2(10);
nr_seq_tipo_aval_w	number(10,0);
nr_seq_avaliacao_w	number(10,0) := 0;
nr_seq_grupo_w		number(10,0);
nr_seq_subgrupo_w	number(10,0);
nr_seq_forma_org_w	number(10,0);
nr_sequencia_w		number(10);
cd_material_exame_w	varchar2(20);


cursor  c02 is
	select nr_sequencia from prescr_procedimento where nr_prescricao = nr_prescricao_p;

cursor	c01 is
	select  nr_seq_tipo_aval
	from	med_tipo_aval_proc
	where	(nvl(cd_procedimento,cd_procedimento_w) 		= cd_procedimento_w or cd_procedimento_w is null)			
	  and 	(nvl(cd_area_procedimento,cd_area_procedimento_w) 	= cd_area_procedimento_w or cd_area_procedimento_w is null)
	  and 	(nvl(cd_especialidade,cd_especialidade_w) 		= cd_especialidade_w or cd_especialidade_w is null)
	  and 	(nvl(cd_grupo_proc,cd_grupo_proc_w) 			= cd_grupo_proc_w or cd_grupo_proc_w is null)
	  and 	(nvl(nr_seq_exame,nr_seq_exame_w) 			= nr_seq_exame_w or nr_seq_exame_w is null)	
	  and 	(nvl(nr_seq_proc_interno,nr_seq_proc_interno_w) 	= nr_seq_proc_interno_w or nr_seq_proc_interno_w is null)
	  and 	(nvl(nr_seq_forma_org, nr_seq_forma_org_w)		= nr_seq_forma_org_w or nr_seq_forma_org_w is null)	
	  and 	(nvl(nr_seq_grupo, nr_seq_grupo_w)			= nr_seq_grupo_w or nr_seq_grupo_w is null)	
	  and 	(nvl(nr_seq_subgrupo, nr_seq_subgrupo_w)		= nr_seq_subgrupo_w or nr_seq_subgrupo_w is null)
	  and	(nvl(nvl(nr_seq_proc_interno,nr_seq_proc_interno_w),0) 	= nvl(nr_seq_proc_interno_w,0))
	  and 	(nvl(Obter_Material_Exame_Lab(nr_seq_material,null,2),cd_material_exame_w) = cd_material_exame_w or cd_material_exame_w is null)
	  and	exists (select 1 from med_tipo_avaliacao n where n.nr_sequencia = nr_seq_tipo_aval and nvl(ie_situacao,'A') = 'A')
          and 	(nr_seq_tipo_aval not in (select nr_seq_tipo_avaliacao from med_avaliacao_paciente where nr_prescricao = nr_prescricao_p))
	order by 	
		nvl(cd_procedimento,0),
		nvl(cd_grupo_proc,0),
		nvl(cd_especialidade,0),
		nvl(cd_area_procedimento,0),
		nvl(nr_seq_exame,0),
		nvl(nr_seq_proc_interno,0),
		nvl(nr_seq_grupo,0),
		nvl(nr_seq_subgrupo,0),
		nvl(nr_seq_forma_org,0);


begin

open c02;
loop
fetch c02 into nr_sequencia_w;
exit when c02%notfound;
	begin
	select	a.nr_atendimento,
		a.cd_pessoa_fisica,
		a.cd_medico,
		b.cd_procedimento,
		b.ie_origem_proced,
		b.nr_seq_proc_interno,
		b.nr_seq_exame,
		b.cd_material_exame,
		obter_Especialidade_Proced(b.cd_procedimento,b.ie_origem_proced) cd_especialidade,	
		obter_area_procedimento(b.cd_procedimento,b.ie_origem_proced) cd_area_procedimento,
		obter_grupo_procedimento(b.cd_procedimento,b.ie_origem_proced,'C') cd_grupo_proc,
		nvl(Sus_Obter_seq_Estrut_Proc(Sus_Obter_Estrut_Proc(b.cd_procedimento,b.ie_origem_proced,'C','G'),'G'),0) nr_seq_grupo,
		nvl(Sus_Obter_seq_Estrut_Proc(Sus_Obter_Estrut_Proc(b.cd_procedimento,b.ie_origem_proced,'C','S'),'S'),0) nr_seq_subgrupo,
		nvl(Sus_Obter_seq_Estrut_Proc(Sus_Obter_Estrut_Proc(b.cd_procedimento,b.ie_origem_proced,'C','F'),'F'),0) nr_seq_forma_org
	into	nr_atendimento_w,
		cd_pessoa_fisica_w,
		cd_medico_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		nr_seq_exame_w,
		cd_material_exame_w,
		cd_especialidade_w,
		cd_area_procedimento_w,
		cd_grupo_proc_w,
		nr_seq_grupo_w,
		nr_seq_subgrupo_w,
		nr_seq_forma_org_w
	from	prescr_medica a,
		prescr_procedimento b,
		procedimento c
	where	a.nr_prescricao		= b.nr_prescricao
	and	b.cd_procedimento	= c.cd_procedimento
	and 	b.ie_origem_proced	= c.ie_origem_proced
	and	b.nr_prescricao		= nr_prescricao_p
	and	b.nr_sequencia		= nr_sequencia_w;
	open c01;
	loop
	fetch c01 into nr_seq_tipo_aval_w;
	exit when c01%notfound;
		begin

		select	med_avaliacao_paciente_seq.nextval
		into	nr_seq_avaliacao_w
		from	dual;		

		insert	into med_avaliacao_paciente 
			(nr_sequencia,
			cd_pessoa_fisica,
			cd_medico,
			dt_avaliacao,
			dt_atualizacao,
			nm_usuario,
			ds_observacao,
			nr_seq_tipo_avaliacao,
			ie_avaliacao_parcial,
			nr_atendimento,
			nr_prescricao
		) values (
			nr_seq_avaliacao_w,
			cd_pessoa_fisica_w,
			cd_medico_w,
			sysdate,
			sysdate,
			nm_usuario_p,
			null,
			nr_seq_tipo_aval_w,
			'N', 
			nr_atendimento_w,
			nr_prescricao_p
		);

		end;	
	end loop;
	close c01;
	end;
end loop;
close c02;

nr_seq_avaliacao_p	:= nr_seq_avaliacao_w;

commit;

end Gerar_Avaliacao_Prescricao;
/
