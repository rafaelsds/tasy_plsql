create or replace
procedure sbc_gerar_cobr_brad_400_de_v10
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 
/*	Vers�o 06
	Data: 05/06/2009
*/

/* HEader */
ds_conteudo_w			varchar2(400);
ds_brancos_277_w		varchar2(277);
nm_empresa_w			varchar2(30);
cd_empresa_w			varchar2(20);
ds_branco_8_w			varchar2(8);
nr_seq_arquivo_w		varchar2(7);
dt_geracao_w			varchar2(6);
nr_seq_registro_w		varchar2(10);

/* Transa��o */
nm_avalista_w			varchar2(60);
nm_sacado_w			varchar2(40);
ds_endereco_sacado_w		varchar2(40);
nr_controle_partic_w		varchar2(25);
id_empresa_w			varchar2(17);
nr_inscricao_w			varchar2(14);
vl_titulo_w			varchar2(13);
vl_acrescimo_w			varchar2(13);
vl_desconto_w			varchar2(13);
vl_iof_w			varchar2(13);
vl_abatimento_w			varchar2(13);
nr_nosso_numero_w		varchar2(11);
vl_desconto_dia_w		varchar2(10);
nr_documento_w			varchar2(10);
cd_cep_w			varchar2(15);
cd_conta_w			varchar2(7);
dt_vencimento_w			varchar2(6);
dt_emissao_w			varchar2(6);
dt_desconto_w			varchar2(6);
cd_agencia_debito_w		varchar2(5);
cd_agencia_bancaria_w		varchar2(5);
cd_agencia_deposito_w		varchar2(5);
pr_multa_w			varchar2(4);
cd_banco_w			varchar2(3);
cd_banco_cobranca_w		varchar2(3);
ds_brancos_2_w			varchar2(2);
ie_ocorrencia_w			varchar2(2);
ie_especie_w			varchar2(2);
ie_instrucao_1_w		varchar2(2);
ie_instrucao_2_w		varchar2(2);
ie_tipo_inscricao_w		varchar2(2);
ie_digito_agencia_w		varchar2(1);
ie_digito_conta_w		varchar2(1);
ie_multa_w			varchar2(1);
nr_dig_nosso_numero_w		varchar2(1);
cd_condicao_w			varchar2(1);
ie_emite_papeleta_w		varchar2(1);
ie_rateio_w			varchar2(1);
ie_endereco_w			varchar2(1);
ie_aceite_w			varchar2(1);
ds_brancos_10_w			varchar2(10);
ds_brancos_12_w			varchar2(12);
ds_mensagem_1_w			varchar2(255);
ds_mensagem_2_w			varchar2(255);
ds_mensagem_3_w			varchar2(255);
ds_mensagem_4_w			varchar2(255);
dt_limite_con_w                 number(6);
cd_carteira_w    		varchar2(40);

/* Trailler */
ds_brancos_393_w		varchar2(393);

nr_seq_apres_w			number(10)	:= 0;
qt_registros_w			number(10)	:= 1;
ie_gerar_cob_esc_prim_mens_w	varchar(1) 	:= null;

Cursor C01 is
	--PMG - 22/01/2016 - OS 984998
	select	lpad('0',5,'0') cd_agencia_debito,
		lpad(nvl(substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DA'),1,1),'0'),1,'0') ie_digito_agencia,
		lpad(x.CD_AGENCIA_BANCARIA,5,'0') cd_agencia_bancaria,
		lpad(x.cd_conta,7,'0') cd_conta,
		nvl(x.ie_digito_conta,'0') ie_digito_conta,
		'0' || lpad(nvl(x.cd_carteira,'0'),3,'0') || lpad(nvl(substr(x.cd_agencia_bancaria,1,5),'0'),5,'0') || lpad(nvl(substr(x.cd_conta,1,7),'0'),7,'0') || nvl(x.ie_digito_conta,'0') id_empresa,
		lpad(nvl(f.cd_cgc,'0'),25,'0') nr_controle_partic,
		'000' cd_banco,
		'2' ie_multa,
		lpad(nvl(elimina_caracteres_especiais(b.tx_multa),0),4,'0') pr_multa,
		lpad(nvl(to_char(b.nr_titulo),'0'),11,'0') nr_nosso_numero,
		decode(calcula_digito('MODULO11_BRAD',lpad(substr(x.cd_carteira,2,2),2,'0') || lpad(b.nr_titulo,11,'0')),'-1','P',
			calcula_digito('MODULO11_BRAD',lpad(substr(x.cd_carteira,2,2),2,'0') || lpad(b.nr_titulo,11,0))) nr_dig_nosso_numero,
		lpad('0',10,'0') vl_desconto_dia,
		nvl(a.IE_EMISSAO_BLOQUETO,'2') cd_condicao,
		'N' ie_emite_papeleta,
		' ' ie_rateio,
		'1' ie_endereco,
		lpad(nvl(substr(c.cd_ocorrencia,1,2),'1'),2,'0') ie_ocorrencia,
		lpad('0',10,'0') nr_documento,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyy') dt_vencimento,
		lpad(replace(to_char(nvl(b.vl_titulo,0), 'fm00000000000.00'),'.',''),13,'0') vl_titulo,
		lpad('0',3,'0') cd_banco_cobranca,
		lpad('0',5,'0') cd_agencia_deposito,
		'12' ie_especie,
		'N' ie_aceite,
		to_char(b.dt_emissao,'ddmmyy') dt_emissao,
		lpad(nvl(substr(c.cd_instrucao,1,2),'0'),2,'0') ie_instrucao1,
		'00' ie_instrucao2,
		substr(lpad(decode(nvl(elimina_caracteres_especiais(c.vl_acrescimo),0),0,'0',elimina_caracteres_especiais(c.vl_acrescimo)),13,'0'),1,13) vl_acrescimo,
		decode(nvl(elimina_caracteres_especiais(c.vl_desconto),0),0,'000000',to_char(sysdate,'ddmmyy')) dt_desconto,
		substr(lpad(decode(nvl(elimina_caracteres_especiais(c.vl_desconto),0),0,'0',elimina_caracteres_especiais(c.vl_desconto)),13,'0'),1,13) vl_desconto,
		lpad('0',13,'0') vl_iof,
		lpad('0',13,'0') vl_abatimento,
		decode(b.cd_cgc, null,'01','02') ie_tipo_inscricao,
		lpad(nvl(b.cd_cgc_cpf,'0'),14,'0') nr_inscricao,
		rpad(nvl(upper(elimina_acentuacao(substr(b.nm_pessoa,1,40))),' '),40,' ') nm_sacado,
		substr(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'E')),1,24)  || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'NR'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'NR')),1,4) || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CO'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CO')),1,7) || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2),1,40) ds_endereco_sacado,
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8) cd_cep,
		lpad(' ',60,' ') nm_avalista,
		substr(lpad(nvl(to_char(a.nr_sequencia),'0'),6,'0'),1,6) nr_seq_arquivo,
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,1),' '),80, ' ') ds_mensagem_1,
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,2),' '),80, ' ') ds_mensagem_2,
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,3),' '),80, ' ') ds_mensagem_3,
		rpad(nvl(obter_instrucao_boleto(b.nr_titulo,a.cd_banco,4),' '),80, ' ') ds_mensagem_4,
		e.cd_carteira cd_carteira
	from	pls_lote_mensalidade	z,
		banco_estabelecimento	x,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v	b,
		titulo_receber_cobr	c,
		cobranca_escritural	a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador	= f.nr_sequencia(+)
	and	d.nr_seq_lote		= z.nr_sequencia(+)
	and	((z.ie_primeira_mensalidade is null or z.ie_primeira_mensalidade = 'N') 
	or	ie_gerar_cob_esc_prim_mens_w = 'S')
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;
	
begin

delete from w_envio_banco 
where nm_usuario = nm_usuario_p;

/* Pega o par�metro para ver se considera os t�tulos gerados por lotes de primera mensalidade */
select	nvl(max(ie_gerar_cob_esc_prim_mens),'S')
into	ie_gerar_cob_esc_prim_mens_w
from	pls_parametros_cr
where	cd_estabelecimento = cd_estabelecimento_p;

select	lpad(' ',8,' '),
	lpad(' ',277,' '),
	lpad(' ',2,' '),
	lpad(' ',393,' '),
	lpad(' ',10,' '),
	lpad(' ',12,' ')
into	ds_branco_8_w,
	ds_brancos_277_w,
	ds_brancos_2_w,
	ds_brancos_393_w,
	ds_brancos_10_w,
	ds_brancos_12_w
from	dual;

/* Header */
select	lpad(nvl(substr(c.cd_convenio_banco,1,20),'0'),20,'0'),
	lpad(upper(elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,30))),30,' '),
	to_char(a.dt_remessa_retorno,'ddmmyy'),
	lpad(to_char(a.nr_sequencia),7,'0')
into	cd_empresa_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_seq_arquivo_w
from	estabelecimento		b,
	cobranca_escritural	a,
	banco_estabelecimento	c
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;

 ds_conteudo_w	:= 	'01' || 
			'REMESSA' || 
			'01' || 
			'COBRANCA       ' ||
			cd_empresa_w || 
			nm_empresa_w || 
			'237' || 
			'BRADESCO       ' ||  --ajustado
			dt_geracao_w || 
			ds_branco_8_w|| 
			'MX' || 
			nr_seq_arquivo_w || 
			ds_brancos_277_w || 
			lpad(qt_registros_w,6,'0');	
		
insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres,
			nr_seq_apres_2)
	values	(	nr_seq_registro_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w,
			nr_seq_apres_w);
			
nr_seq_apres_w	:= nr_seq_apres_w + 1;
qt_registros_w  := qt_registros_w + 1;
/* Fim Header */

/* Transa��o */
--begin
open C01;
loop
fetch C01 into	
	cd_agencia_debito_w,
	ie_digito_agencia_w,
	cd_agencia_bancaria_w,
	cd_conta_w,
	ie_digito_conta_w,
	id_empresa_w,
	nr_controle_partic_w,
	cd_banco_w,
	ie_multa_w,
	pr_multa_w,
	nr_nosso_numero_w,
	nr_dig_nosso_numero_w,
	vl_desconto_dia_w,
	cd_condicao_w,
	ie_emite_papeleta_w,
	ie_rateio_w,
	ie_endereco_w,
	ie_ocorrencia_w,
	nr_documento_w,
	dt_vencimento_w,
	vl_titulo_w,
	cd_banco_cobranca_w,
	cd_agencia_deposito_w,
	ie_especie_w,
	ie_aceite_w,
	dt_emissao_w,
	ie_instrucao_1_w,
	ie_instrucao_2_w,
	vl_acrescimo_w,
	dt_desconto_w,
	vl_desconto_w,
	vl_iof_w,
	vl_abatimento_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	cd_cep_w,
	nm_avalista_w,
	nr_seq_arquivo_w,
	ds_mensagem_1_w,
	ds_mensagem_2_w,
	ds_mensagem_3_w,
	ds_mensagem_4_w,
        cd_carteira_w
	;	
	
exit when C01%notfound;
	begin
	select	w_envio_banco_seq.nextval
	into	nr_seq_registro_w
	from	dual;

	ds_conteudo_w	:= 	'1' ||  --01
				cd_agencia_debito_w || 		--002 a 006
				ie_digito_agencia_w ||  	--007 a 007
				cd_agencia_bancaria_w ||  	--008 a 012
				cd_conta_w || 		  	--013 a 019
				ie_digito_conta_w || 	  	--020 a 020
				id_empresa_w || 	  	--021 a 037
				nr_controle_partic_w ||   	--038 a 062
				cd_banco_w || 		  	--063 a 065
				ie_multa_w || 		  	--066 a 066
				pr_multa_w || 		  	--067 a 070
				nr_nosso_numero_w ||	  	--071 a 081
				nr_dig_nosso_numero_w ||  	--082 a 082
				vl_desconto_dia_w || 		--083 a 092
				cd_condicao_w ||    		--093 a 093
				ie_emite_papeleta_w || 		--094 a 094
				ds_brancos_10_w || 		--095 a 104
				' ' ||				--105 a 105
				ie_endereco_w || 		--106 a 106
				ds_brancos_2_w || 		--107 a 108
				ie_ocorrencia_w || 		--109 a 110
				nr_documento_w || 		--111 a 120
				dt_vencimento_w || 		--121 a 126
				vl_titulo_w || 			--127 a 139
				cd_banco_cobranca_w || 		--140 a 142
				cd_agencia_deposito_w || 	--143 a 147
				ie_especie_w || 		--148 a 149
				ie_aceite_w || 			--150 a 150
				dt_emissao_w || 		--151 a 156
				ie_instrucao_1_w || 		--157 a 158
				ie_instrucao_2_w ||		--159 a 160
				vl_acrescimo_w || 		--161 a 173
				dt_desconto_w || 		--174 a 179
				vl_desconto_w || 		--180 a 192
				vl_iof_w || 			--193 a 205
				vl_abatimento_w || 		--206 a 218
				ie_tipo_inscricao_w ||		--219 a 220
				nr_inscricao_w || 		--221 a 234
				nm_sacado_w || 			--235 a 274
				rpad(ds_endereco_sacado_w,40,' ') || 	--275 a 314
				ds_brancos_12_w || 		--315 a 326
				nvl(cd_cep_w,'        ') || 	--327 a 331 e 332 a 334
				rpad(nvl(nm_avalista_w,' '),60,' ') || 	--335 a 394
				lpad(qt_registros_w,6,'0') ;		--395 a 400
												
	insert into w_envio_banco
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres,
				nr_seq_apres_2)
		values	(	nr_seq_registro_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_apres_w,
				nr_seq_apres_w);
															
nr_seq_apres_w	:= nr_seq_apres_w + 1;
qt_registros_w  := qt_registros_w + 1;

	
	ds_conteudo_w   :=	'2' ||  --01
				ds_mensagem_1_w 				|| --002 a 081
				ds_mensagem_2_w 				|| --082 a 161
				ds_mensagem_3_w					|| --162 a 241
				ds_mensagem_4_w					|| --242 a 321
				lpad('0' ,6,'0')    				|| --322 a 327
				lpad('0' ,13,'0')  				|| --328 a 340
				lpad('0' ,6,'0')  				|| --341 a 346
				lpad('0' ,13,'0')  				|| --347 a 359
				lpad(' ' ,7,' ')  				|| --360 a 366
				lpad(nvl(cd_carteira_w,0),3,'0')       		|| --367 a 369 
				lpad(nvl(cd_agencia_bancaria_w,'0'),5,'0')	|| --370 a 374 
				lpad(nvl(cd_conta_w,'0'),7,'0')			|| --375 a 381  
				lpad(nvl(ie_digito_conta_w,'0'),1,'0')          || --382
				lpad(nvl(nr_nosso_numero_w,'0'),11,'0')	  	|| --383 a 393
				lpad(nvl(nr_dig_nosso_numero_w,'0'),1,'0')	|| --384	
				lpad(qt_registros_w,6,'0');      	  	   --395 a 400
				
				
				
				
/*
lpad(nvl(cd_agencia_bancaria_w,'0'),5,'0')			|| Pos 370 a 374
lpad(nvl(cd_conta_w,'0'),7,'0')				|| Pos 375 a 381
lpad(nvl(ie_digito_conta_w,'0'),1,'0')			||    Pos 382
lpad(nvl(nr_nosso_numero_w,'0'),12,'0')			||    Pos 383 a 394
lpad(qt_registros_w,6,'0');				 --395 a 400


*/				
				
		
select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;
			
				
	insert into w_envio_banco
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				cd_estabelecimento,
				ds_conteudo,
				nr_seq_apres,
				nr_seq_apres_2)
		values	(	nr_seq_registro_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				cd_estabelecimento_p,
				ds_conteudo_w,
				nr_seq_apres_w,
				nr_seq_apres_w);                         
				
nr_seq_apres_w	:= nr_seq_apres_w + 1;
qt_registros_w  := qt_registros_w + 1;
	
	if	(qt_registros_w = 500) then
		qt_registros_w	:= 1;
		commit;
	end if;						
				
				
end;	
/* Fim Registro Opcional */
	
end loop;
close C01;

/* Fim Transa��o */

/* Trailler */
ds_conteudo_w	:= 	'9' || 
			ds_brancos_393_w || 
			lpad(qt_registros_w,6,'0');	

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;
		
insert into w_envio_banco
		(	nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_estabelecimento,
			ds_conteudo,
			nr_seq_apres,
			nr_seq_apres_2)
	values	(	nr_seq_registro_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_estabelecimento_p,
			ds_conteudo_w,
			nr_seq_apres_w,
			nr_seq_apres_w);
/* Fim Trailler*/

commit;

end sbc_gerar_cobr_brad_400_de_v10;
/