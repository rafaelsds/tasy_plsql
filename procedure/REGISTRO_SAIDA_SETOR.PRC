create or replace
procedure Registro_Saida_Setor(	nr_seq_interno_p		Number,
								dt_saida_p				Date,
								nm_usuario_p			Varchar2) IS

qt_reg_w				Number(05,0);
cd_estabelecimento_w	Number(4);
qt_reg_saida_setor_w	Number(4);
qt_regra_w				Number(4);
nr_atendimento_w		Number(10);
nr_seq_interno_w		Number(10);
cd_setor_atendimento_w	Number(10);
ie_saida_internacao_w	varchar2(1);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);
dt_saida_w				date;

BEGIN

dt_saida_w := nvl(dt_saida_p,sysdate);

select 	MAX(cd_estabelecimento),
		MAX(a.cd_setor_atendimento),
		MAX(a.cd_unidade_basica),
		MAX(a.cd_unidade_compl)
into	cd_estabelecimento_w,
		cd_setor_atendimento_w,
		cd_unidade_basica_w,
		cd_unidade_compl_w
from	setor_atendimento b, 
		atend_paciente_unidade a
where	a.cd_setor_atendimento = b.cd_setor_atendimento
and		a.nr_seq_interno	= nr_seq_interno_p;


Obter_Param_Usuario(3111,66,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_saida_internacao_w);


select count(*)
into	qt_reg_w
from	setor_atendimento b, 
		atend_paciente_unidade a
where	a.cd_setor_atendimento = b.cd_setor_atendimento
and		b.cd_classif_setor in (2,5)
and		a.nr_seq_interno	= nr_seq_interno_p;

if	(ie_saida_internacao_w = 'S') and
	(qt_reg_w = 0)	 then

	select 	max(nr_atendimento)
	into	nr_atendimento_w
	from	atend_paciente_unidade
	where	nr_seq_interno	= nr_seq_interno_p;

	select 	count(*)
	into	qt_reg_saida_setor_w
	from	setor_atendimento b, 
			atend_paciente_unidade a
	where	a.cd_setor_atendimento = b.cd_setor_atendimento
	and		dt_saida_unidade 	is null
	and		a.nr_seq_interno	<> nr_seq_interno_p
	and		a.nr_atendimento	= nr_atendimento_w;

	if	(qt_reg_saida_setor_w > 0) then

		select 	count(*) 
		into    qt_regra_w
		from	regra_saida_setor_int;

		if	(qt_regra_w > 0) then

			select 	count(*)
			into	qt_reg_w
			from	regra_saida_setor_int c,
					setor_atendimento b, 
					atend_paciente_unidade a
			where	a.cd_setor_atendimento = b.cd_setor_atendimento
			and		c.cd_setor_atendimento = b.cd_setor_atendimento
			and		b.cd_classif_setor in (4,3,1)
			and		a.nr_seq_interno	= nr_seq_interno_p;	

		else
			select 	count(*)
			into	qt_reg_w
			from	setor_atendimento b, 
					atend_paciente_unidade a
			where	a.cd_setor_atendimento = b.cd_setor_atendimento
			and		b.cd_classif_setor in (4,3,1)
			and		a.nr_seq_interno	= nr_seq_interno_p;	
		end if;
	else
		Wheb_mensagem_pck.exibir_mensagem_abort(213742);
	end if;
end if;

if	(qt_reg_w > 0) then
							
	update	atend_paciente_unidade
	set		dt_saida_unidade	= dt_saida_w,
			nm_usuario			= nm_usuario_p,
			dt_atualizacao		= sysdate
	where	nr_seq_interno		= nr_seq_interno_p;
	
	select	max(nr_seq_interno)
	into	nr_seq_interno_w
	from	unidade_atendimento
	where	cd_unidade_basica		= cd_unidade_basica_w
	and		cd_unidade_compl		= cd_unidade_compl_w
	and		cd_setor_atendimento	= cd_setor_atendimento_w;
	
	if (nr_seq_interno_w is not null) then
		gerar_higienizacao_leito_unid(dt_saida_w, nm_usuario_p, cd_estabelecimento_w, 'RSS', nr_seq_interno_w, null, null);
	end if;
	
	if	(nvl(cd_estabelecimento_w,0) > 0) and (nvl(nr_atendimento_w,0) > 0) then
		update	ap_lote
		set		cd_setor_ant = cd_setor_atendimento_w
		where	ie_status_lote = 'G'
		and		obter_atendimento_prescr(nr_prescricao) = nr_atendimento_w
		and		dt_geracao_lote > (sysdate -30);
	end if;
	
	commit;
	
	call_bifrost_content('patient.transfer','encounter_json_pck.get_transf_message_clob('||nr_seq_interno_p||')', nm_usuario_p);
	susp_itens_prescr_transfer(nr_seq_interno_p, nm_usuario_p);
else
	Wheb_mensagem_pck.exibir_mensagem_abort(213743);
end if;

END Registro_Saida_Setor;
/