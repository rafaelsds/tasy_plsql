create or replace 
procedure pls_gerar_ocorrencia_pos_estab(	nr_seq_analise_p		pls_analise_conta.nr_sequencia%type,
											nr_seq_conta_p			pls_conta.nr_sequencia%type,
											nr_seq_ocorrencia_p		pls_ocorrencia.nr_sequencia%type,
											cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
											nm_usuario_p			usuario.nm_usuario%type) is
						
dt_atend_referencia_w		pls_conta.dt_atendimento_referencia%type;
ie_tipo_guia_w			pls_conta.ie_tipo_guia%type;
nr_seq_segurado_w		pls_conta.nr_seq_segurado%type;
cd_senha_externa_w		pls_conta.cd_senha_externa%type;
cd_guia_w			pls_conta.cd_guia%type;
cd_guia_referencia_w		pls_conta.cd_guia_referencia%type;
nr_seq_protocolo_w		pls_conta.nr_seq_protocolo%type;
ie_tipo_conta_w			pls_conta.ie_tipo_conta%type;
ie_tipo_segurado_w		pls_segurado.ie_tipo_segurado%type;
nr_seq_contrato_w		pls_segurado.nr_seq_contrato%type;
nr_seq_intercambio_w		pls_segurado.nr_seq_intercambio%type;
cd_matri_estipulante_w		pls_segurado.cd_matricula_estipulante%type;
nr_seq_congenere_w		pls_segurado.nr_seq_congenere%type;
nr_seq_grau_partic_w		pls_grau_participacao.nr_sequencia%type;
nr_seq_prestador_exec_w		pls_prestador.nr_sequencia%type;
nr_seq_plano_w			pls_plano.nr_sequencia%type;
ie_internacao_w			pls_tipo_atendimento.ie_internado%type;
qt_idade_w			varchar2(10);
qt_idade_meses_w		varchar2(10);
nr_seq_prest_prot_w		pls_prestador.nr_sequencia%type;
cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
dt_nascimento_w			pessoa_fisica.dt_nascimento%type;
ie_pcmso_w			pls_segurado.ie_pcmso%type;
nr_seq_clinica_w		pls_conta.nr_seq_clinica%type;
nr_seq_tipo_acomod_conta_w	pls_conta.nr_seq_tipo_acomodacao%type;
ie_carater_internacao_w		pls_conta.ie_carater_internacao%type;
nr_seq_tipo_acomodacao_w	pls_plano_acomodacao.nr_seq_tipo_acomodacao%type;
ie_origem_protocolo_w		pls_protocolo_conta.ie_origem_protocolo%type;
cd_prestador_exec_w		pls_prestador.cd_prestador%type;
ie_excecao_w			varchar2(1) := 'N';

qt_partic_w			pls_integer;
qt_senha_externa_w		pls_integer;
qt_matricula_estip_w		pls_integer;
ie_filme_fat_w			varchar2(1);
ie_co_fat_w			varchar2(1);
ie_hm_fat_w			varchar2(1);
ie_valor_pos_zerado_w		varchar2(1);
ie_filme_w			varchar2(1);
ie_co_w				varchar2(1);
ie_hm_w				varchar2(1);
ie_faturamento_w		varchar2(1);
ie_valor_pos_zer_proc_w		varchar2(1);
ie_material_a900_w		varchar2(1);
nr_seq_grupo_rec_w		procedimento.nr_seq_grupo_rec%type;
nr_seq_estrutura_mat_w		pls_material.nr_seq_estrut_mat%type;
cd_material_w			pls_material_unimed.cd_material%type;
ie_gera_ocor_w			varchar2(1);
nr_contador_w			pls_integer;
nr_seq_oc_benef_w		pls_ocorrencia_benef.nr_sequencia%type;
nr_seq_mot_liberacao_w		Number(10);
nr_seq_tipo_atend_w		pls_tipo_atendimento.nr_sequencia%type;
qt_glosa_w			pls_integer;
ie_ocorrencia_w			pls_controle_estab.ie_ocorrencia%type := pls_obter_se_controle_estab('GO');
ie_origem_pos_estab_w	varchar2(2);

-- Cursor que busca as informa��es da conta p�s estabelecido e dos procedimentos/materiais
cursor C01(	nr_seq_conta_pc		pls_conta.nr_sequencia%type) is
	select	a.nr_sequencia,
		a.nr_seq_conta_proc,
		null nr_seq_conta_mat,
		nvl(a.vl_materiais, 0) vl_materiais,
		nvl(a.vl_custo_operacional, 0) vl_custo_operacional,
		nvl(a.vl_medico, 0) vl_medico,
		nvl(a.nr_seq_pos_estab_interc, a.nr_seq_regra_pos_estab) nr_seq_regra_preco,
		nvl(a.vl_calculado, 0) vl_calculado,
		a.ie_preco_plano,
		a.qt_item,
		b.cd_procedimento,
		b.vl_materiais vl_materiais_proc,
		b.vl_custo_operacional vl_custo_operacional_proc,
		b.vl_medico vl_medico_proc,
		b.nr_seq_evento nr_seq_evento_proc,
		b.ie_origem_proced,
		b.vl_beneficiario vl_beneficiario_proc,
		nvl(b.vl_liberado, 0) vl_liberado_proc,
		null nr_seq_material,
		null nr_seq_evento_mat,
		null cd_material,
		null vl_beneficiario_mat,
		a.nr_seq_analise,
		a.ie_vl_pag_prestador,
		3 ie_tipo_item, -- Tipo de item para leitura da exce��o
		b.dt_procedimento_referencia dt_referencia
	from	pls_conta_pos_estabelecido a,
			pls_conta_proc b
	where	a.nr_seq_conta = nr_seq_conta_pc
	and		a.nr_seq_analise = nr_seq_analise_p
	and	a.ie_situacao = 'A'
	and	b.nr_sequencia = a.nr_seq_conta_proc
	and	(a.ie_tipo_liberacao is null or a.ie_tipo_liberacao <> 'U')
	and	a.ie_status_faturamento	!= 'A'
	and	a.ie_cobrar_mensalidade	!= 'A'
	union all
	select	a.nr_sequencia,
		null nr_seq_conta_proc,
		a.nr_seq_conta_mat,
		nvl(a.vl_materiais, 0) vl_materiais,
		nvl(a.vl_custo_operacional, 0) vl_custo_operacional,
		nvl(a.vl_medico, 0) vl_medico,
		nvl(a.nr_seq_pos_estab_interc, a.nr_seq_regra_pos_estab) nr_seq_regra_preco,
		nvl(a.vl_calculado, 0) vl_calculado,
		a.ie_preco_plano,
		a.qt_item,
		null cd_procedimento,
		null vl_materiais_proc,
		null vl_custo_operacional_proc,
		null vl_medico_proc,
		null nr_seq_evento_proc,
		null ie_origem_proced,
		null vl_beneficiario_proc,
		0 vl_liberado_proc,
		b.nr_seq_material,
		b.nr_seq_evento nr_seq_evento_mat,
		b.cd_material,
		b.vl_beneficiario vl_beneficiario_mat,
		a.nr_seq_analise,
		a.ie_vl_pag_prestador,
		4 ie_tipo_item, -- Tipo de item para leitura da exce��o
		b.dt_atendimento_referencia dt_referencia
	from	pls_conta_pos_estabelecido a,
		pls_conta_mat b
	where	a.nr_seq_conta = nr_seq_conta_pc
	and		a.nr_seq_analise = nr_seq_analise_p
	and	a.ie_situacao = 'A'
	and	b.nr_sequencia = a.nr_seq_conta_mat
	and	(a.ie_tipo_liberacao is null or a.ie_tipo_liberacao <> 'U')
	and	a.ie_status_faturamento	!= 'A'
	and	a.ie_cobrar_mensalidade	!= 'A';

cursor C02(	ie_preco_pc			pls_conta_pos_estabelecido.ie_preco_plano%type,
		nr_seq_regra_preco_pc		number,
		nr_seq_material_pc		pls_conta_mat.nr_seq_material%type,
		cd_procedimento_pc		procedimento.cd_procedimento%type,
		ie_origem_proced_pc		procedimento.ie_origem_proced%type,
		nr_seq_tipo_atendimento_pc	pls_tipo_atendimento.nr_sequencia%type,
		nr_seq_grau_partic_pc		pls_grau_participacao.nr_sequencia%type,
		ie_ocorrencia_pc		pls_controle_estab.ie_ocorrencia%type,
		cd_estabelecimento_pc		estabelecimento.cd_estabelecimento%type) is
	select	a.nr_sequencia nr_seq_regra_pos,
		b.nr_sequencia nr_seq_ocorrencia,
		a.nr_seq_grupo_serv,
		a.nr_seq_grupo_material,
		a.nr_seq_grupo_contrato,
		a.nr_seq_estrutura_mat,
		a.ie_internado,
		a.ie_exige_qtd_lib_pag,
		b.ie_glosar_faturamento,
		nvl(a.ie_ocor_princ,'S') ie_ocor_princ,
		(select	count(1)
		 from	pls_excecao_ocorrencia x
		 where	x.nr_seq_regra_pos = a.nr_sequencia
		 and	x.ie_situacao = 'A') qt_excecao
	from	pls_ocorrencia		b,
		pls_oc_regra_pos_estab	a
	where	b.nr_sequencia 		= a.nr_seq_ocorrencia
	and	b.ie_situacao		= 'A'
	and	dt_atend_referencia_w 	between  a.dt_inicio_vigencia_ref and a.dt_fim_vigencia_ref
	and	((a.ie_tipo_guia	is null) or (a.ie_tipo_guia = ie_tipo_guia_w))
	and	((a.ie_preco		is null) or (a.ie_preco = ie_preco_pc))
	and	((a.ie_tipo_segurado	is null) or (a.ie_tipo_segurado = ie_tipo_segurado_w))
	and	((nr_seq_ocorrencia_p	is null) or (nr_seq_ocorrencia_p = b.nr_sequencia))
	and	((a.ie_sem_evento_faturamento = 'N') or (a.ie_sem_evento_faturamento = ie_faturamento_w))
	/* Por valores de procedimento */
	and 	((a.ie_valor_hm_zerado = 'N') or (a.ie_valor_hm_zerado = ie_hm_w) or a.ie_origem_valores = 'PRO')
	and 	((a.ie_valor_co_zerado = 'N') or (a.ie_valor_co_zerado = ie_co_w) or a.ie_origem_valores = 'PRO')
	and 	((a.ie_valor_filme_zerado = 'N') or (a.ie_valor_filme_zerado = ie_filme_w) or a.ie_origem_valores = 'PRO')
	/* Fim por procedimento */
	/* Por valores de p�s */
	and 	((a.ie_valor_hm_zerado = 'N') or (a.ie_valor_hm_zerado = ie_hm_fat_w) or a.ie_origem_valores = 'POS')
	and 	((a.ie_valor_co_zerado = 'N') or (a.ie_valor_co_zerado = ie_co_fat_w) or a.ie_origem_valores = 'POS')
	and 	((a.ie_valor_pos_zerado= 'N') or (a.ie_valor_pos_zerado is null) or (ie_valor_pos_zerado_w = 'S' and a.ie_origem_valores = 'POS') 
			or (ie_valor_pos_zer_proc_w = 'S' and a.ie_origem_valores = 'PRO') )
	and 	((a.ie_valor_filme_zerado = 'N') or (a.ie_valor_filme_zerado = ie_filme_fat_w) or a.ie_origem_valores = 'POS')
	/* Fim por p�s */
	/* Regra de pre�o */
	and	((a.ie_regra_preco = 'SR' and nr_seq_regra_preco_pc is null) or
		(a.ie_regra_preco = 'CR' and nr_seq_regra_preco_pc is not null) or
		a.ie_regra_preco = 'NC' or a.ie_regra_preco is null)
	/* Fim tratamento regra de pre�o*/
	and 	((a.qt_min_senha_externa is null) or (a.qt_min_senha_externa <= qt_senha_externa_w))
	and 	((a.qt_max_senha_externa is null) or (a.qt_max_senha_externa >= qt_senha_externa_w))
	and 	((a.qt_min_matricula_estip is null) or (a.qt_min_matricula_estip <= qt_matricula_estip_w))
	and 	((a.qt_max_matricula_estip is null) or (a.qt_max_matricula_estip >= qt_matricula_estip_w))
	and 	((a.nr_seq_congenere is null) or (a.nr_seq_congenere = nr_seq_congenere_w))
	and 	((a.nr_seq_intercambio is null) or (a.nr_seq_intercambio = nr_seq_intercambio_w))
	and 	((a.nr_seq_material is null) or (a.nr_seq_material = nr_seq_material_pc))
	and 	((a.ie_material_a900 = 'N') or (a.ie_material_a900 = ie_material_a900_w))
	and	((a.cd_procedimento is null) or ((a.cd_procedimento = cd_procedimento_pc) and (a.ie_origem_proced = ie_origem_proced_pc)))
	and	((a.nr_seq_grupo_rec is null) or (a.nr_seq_grupo_rec = nr_seq_grupo_rec_w))
	and	((a.nr_seq_tipo_atendimento is null) or (a.nr_seq_tipo_atendimento = nr_seq_tipo_atendimento_pc))
	and	((a.nr_seq_grau_partic is null) or (a.nr_seq_grau_partic = nr_seq_grau_partic_pc))
	and	((ie_ocorrencia_pc = 'N') or (ie_ocorrencia_pc = 'S' and cd_estabelecimento = cd_estabelecimento_pc))
	and ((a.ie_origem_pos_estab is null) or (a.ie_origem_pos_estab = 'T') or (a.ie_origem_pos_estab = ie_origem_pos_estab_w))
	order by 2, 1;

Cursor C03 (nr_seq_ocorrencia_pc	pls_ocorrencia_benef.nr_sequencia%type,
	    nr_seq_conta_pos_pc		pls_conta_pos_estabelecido.nr_sequencia%type)is
	select  b.nr_sequencia
	from	pls_analise_glo_ocor_grupo	b
	where 	b.nr_seq_ocor_benef in (select 	a.nr_sequencia
					from	pls_ocorrencia_benef	a
					where	nr_seq_conta_pos_estab	=  nr_seq_conta_pos_pc
					and	nr_seq_ocorrencia	= nr_seq_ocorrencia_pc
					and	ie_situacao		= 'I'
					and	(ie_forma_inativacao 	is null or ie_forma_inativacao = 'S'));	
begin

-- inicia as vari�veis
qt_senha_externa_w := 0;
qt_matricula_estip_w := 0;

select 	decode(max(ie_origem_analise),7,'D',8,'R',2,'P','T')
into	ie_origem_pos_estab_w
from	pls_analise_conta
where	nr_sequencia = nr_seq_analise_p;

-- busca os dados da conta
if	(nr_seq_conta_p is not null) then
	select	dt_atendimento_referencia,
		ie_tipo_guia,
		nr_seq_segurado,
		cd_senha_externa,
		nvl(cd_guia, cd_guia_referencia),
		nr_seq_protocolo,
		ie_tipo_conta,
		nr_seq_tipo_atendimento,
		nr_seq_prestador_exec,
		nr_seq_clinica,
		nr_seq_tipo_acomodacao,
		cd_guia_referencia,
		ie_carater_internacao,
		ie_tipo_segurado
	into	dt_atend_referencia_w,
		ie_tipo_guia_w,
		nr_seq_segurado_w,
		cd_senha_externa_w,
		cd_guia_w,
		nr_seq_protocolo_w,
		ie_tipo_conta_w,
		nr_seq_tipo_atend_w,
		nr_seq_prestador_exec_w,
		nr_seq_clinica_w,
		nr_seq_tipo_acomod_conta_w,
		cd_guia_referencia_w,
		ie_carater_internacao_w,
		ie_tipo_segurado_w
	from	pls_conta
	where	nr_sequencia = nr_seq_conta_p;
end if;

ie_internacao_w := pls_obter_se_internado(nr_seq_conta_p, 'C');

if	(nr_seq_protocolo_w is not null) then
	select	nr_seq_prestador,
		ie_origem_protocolo
	into	nr_seq_prest_prot_w,
		ie_origem_protocolo_w
	from	pls_protocolo_conta
	where	nr_sequencia = nr_seq_protocolo_w;
end if;

-- se tem segurado busca os dados do segurado
if	(nr_seq_segurado_w is not null) then

	select	decode(ie_tipo_segurado_w, null, ie_tipo_segurado, ie_tipo_segurado_w),
		nr_seq_contrato,
		nr_seq_intercambio,
		cd_matricula_estipulante,
		nr_seq_congenere,
		nvl(pls_obter_produto_benef(nr_sequencia,dt_atend_referencia_w),0),
		cd_pessoa_fisica,
		ie_pcmso
	into	ie_tipo_segurado_w,
		nr_seq_contrato_w,
		nr_seq_intercambio_w,
		cd_matri_estipulante_w,
		nr_seq_congenere_w,
		nr_seq_plano_w,
		cd_pessoa_fisica_w,
		ie_pcmso_w
	from	pls_segurado
	where	nr_sequencia = nr_seq_segurado_w;
end if;

if	(cd_pessoa_fisica_w is not null) then
	select	dt_nascimento
	into	dt_nascimento_w
	from	pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	qt_idade_w		:= substr(obter_idade(dt_nascimento_w, sysdate, 'A'),1,10);
	qt_idade_meses_w	:= substr(obter_idade(dt_nascimento_w, sysdate, 'M'),1,10);
end if;

if	(nr_seq_plano_w is not null) then
	select	max(nr_seq_tipo_acomodacao)
	into	nr_seq_tipo_acomodacao_w
	from 	pls_plano_acomodacao
	where	nr_seq_plano = nr_seq_plano_w;
end if;

if	(nr_seq_prestador_exec_w is not null) then
	select	cd_prestador
	into	cd_prestador_exec_w
	from	pls_prestador
	where	nr_sequencia = nr_seq_prestador_exec_w;
end if;

-- se tem senha externa verifica o tamanho da mesma, sen�o fica zero
if	(cd_senha_externa_w is not null) then
	qt_senha_externa_w := length(cd_senha_externa_w);
end if;

-- se tem c�digo da matricula do estipulante verifica o tamanho, sen�o fica zero
if	(cd_matri_estipulante_w is not null) then
	qt_matricula_estip_w := length(cd_matri_estipulante_w);
end if;

for r_c01_w in C01(nr_seq_conta_p) loop
	
	ie_valor_pos_zer_proc_w := 'N';
	ie_valor_pos_zerado_w 	:= 'N';
	ie_hm_fat_w		:= 'N';
	ie_co_fat_w		:= 'N';
	ie_filme_fat_w		:= 'N';
	ie_filme_w		:= 'N';
	ie_hm_w			:= 'N';
	ie_co_w			:= 'N';
	-- verifica��o na conta
	if	(r_c01_w.vl_materiais = 0) then
		ie_filme_fat_w := 'S';
	end if;
	
	if	(r_c01_w.vl_custo_operacional = 0) then
		ie_co_fat_w := 'S';
	end if;
	
	if	(r_c01_w.vl_medico = 0) then
		ie_hm_fat_w := 'S';
	end if;
	
	if	(r_c01_w.vl_calculado = 0) then
		ie_valor_pos_zerado_w := 'S';
	end if;
	-- fim verifica��o na conta
	
	-- verifica��o dos procedimentos
	if	(r_c01_w.nr_seq_conta_proc is not null) then
		
		if	(r_c01_w.vl_materiais_proc = 0) then
			ie_filme_w := 'S';
		end if;
		
		if	(r_c01_w.vl_custo_operacional_proc = 0) then
			ie_co_w := 'S';
		end if;
		
		if	(r_c01_w.vl_medico_proc = 0) then
			ie_hm_w := 'S';
		end if;
		
		if	(r_c01_w.nr_seq_evento_proc is null) then
			ie_faturamento_w := 'N';
		end if;

		if	(r_c01_w.vl_liberado_proc = 0) then
			ie_valor_pos_zer_proc_w := 'S';
		end if;
		
		select	max(nr_seq_grupo_rec)
		into	nr_seq_grupo_rec_w
		from	procedimento
		where	cd_procedimento = r_c01_w.cd_procedimento
		and	ie_origem_proced = r_c01_w.ie_origem_proced;
		
		select	count(1)
		into	qt_partic_w
		from	pls_proc_participante
		where	nr_seq_conta_proc = r_c01_w.nr_seq_conta_proc;
		
		if	(qt_partic_w < 2) then
			select	max(nr_seq_grau_partic)
			into	nr_seq_grau_partic_w
			from	pls_proc_participante
			where	nr_seq_conta_proc = r_c01_w.nr_seq_conta_proc;
			
			if	(nr_seq_grau_partic_w is null) then
				select	max(nr_seq_grau_partic)
				into	nr_seq_grau_partic_w
				from	pls_conta
				where	nr_sequencia = nr_seq_conta_p;
			end if;
		end if;
	end if;	
	-- fim verifica��o procedimentos

	-- verifica��o dos materiais
	if	(r_c01_w.nr_seq_conta_mat is not null) then

		select 	max(cd_material)
		into	cd_material_w
		from	pls_material_unimed
		where 	cd_material = r_c01_w.cd_material
		and	ie_situacao = 'A';
		
		if	(cd_material_w is null) then
			ie_material_a900_w := 'N';
		end if;
		
		if	(r_c01_w.nr_seq_evento_mat is null) then
			ie_faturamento_w := 'N';
		end if;
	end if;
	-- fim verifica��o materiais
	
	for r_c02_w in C02(	r_c01_w.ie_preco_plano, r_c01_w.nr_seq_regra_preco, 
				r_c01_w.nr_seq_material, r_c01_w.cd_procedimento,
				r_c01_w.ie_origem_proced, nr_seq_tipo_atend_w,
				nr_seq_grau_partic_w, ie_ocorrencia_w, cd_estabelecimento_p) loop
		-- se retornou no cursor ent�o come�a sendo v�lida a ocorr�ncia
		ie_gera_ocor_w := 'S';

		-- se possui filtro de grupo de material
		if	(r_c02_w.nr_seq_grupo_material is not null) then

			-- verifica se o material pertence ao grupo de materiais
			if	(pls_se_grupo_preco_material(r_c02_w.nr_seq_grupo_material, r_c01_w.nr_seq_material) = 'N') then
			
				ie_gera_ocor_w := 'N';
			end if;
		end if;

		-- estrutura de materiais
		if	(r_c02_w.nr_seq_estrutura_mat is not null) then
		
			if	(pls_obter_se_estruturas_mat(r_c02_w.nr_seq_estrutura_mat, r_c01_w.nr_seq_material) = 'N') then
			
				ie_gera_ocor_w := 'N';
			end if;
		end if;
		
		-- filtro por grupo de contrato, feito dois counts se n�o retornar algo ent�o n�o gera
		if	(r_c02_w.nr_seq_grupo_contrato is not null) then

			select	sum(qtde)
			into 	nr_contador_w
			from	(select	count(1) qtde
				from	pls_preco_contrato
				where	nr_seq_grupo = r_c02_w.nr_seq_grupo_contrato
				and	nr_seq_contrato	= nr_seq_contrato_w
				and 	rownum = 1
				union all
				select	count(1) qtde
				from	pls_preco_contrato
				where	nr_seq_grupo = r_c02_w.nr_seq_grupo_contrato
				and	nr_seq_intercambio = nr_seq_intercambio_w
				and 	rownum = 1);

			if	(nr_contador_w = 0) then

				ie_gera_ocor_w := 'N';
			end if;
		end if;
		
		-- se tem grupo de servi�o informado na regra
		if	(r_c02_w.nr_seq_grupo_serv is not null) then

			-- precisa ser do grupo para gerar a ocorr�ncia
			if	(pls_se_grupo_preco_servico(r_c02_w.nr_seq_grupo_serv, r_c01_w.cd_procedimento, 
								r_c01_w.ie_origem_proced) = 'N') then
				
				ie_gera_ocor_w := 'N';
			end if;
		end if;
				
		 -- se a regra exige quantidade liberada e n�o tem quantidade ent�o n�o deve gerar
		if	(r_c02_w.ie_exige_qtd_lib_pag = 'S') and
			(r_c01_w.qt_item = 0) then
			
			ie_gera_ocor_w := 'N';
		end if;

		-- Consist�ncia para contas que estiverem em uma guia de interna��o, conforme solicitado por Roni - USJRP.
		if	(r_c02_w.ie_internado = 'S') then
		
			-- verifica se trata-se de uma interna��o
			if	(pls_obter_se_internado(nr_seq_conta_p, null, null, null, null, 
								cd_estabelecimento_p, 'S') = 'N') then
				
				ie_gera_ocor_w := 'N';
			end if;
		end if;
		
		-- Caso tenha ocorr�ncia oriunda da analise de prestador, n�o gera a ocorr�ncia.
		if	(r_c02_w.ie_ocor_princ = 'N') then
			select	count(1)
			into	qt_glosa_w
			from	pls_ocorrencia_benef a
			where	a.nr_seq_conta_pos_estab = r_c01_w.nr_sequencia
			and	exists(	select	1
					from	pls_ocorrencia_benef b
					where	b.nr_seq_proc = r_c01_w.nr_seq_conta_proc
					and	b.nr_seq_ocorrencia = a.nr_seq_ocorrencia);
					
			if	(qt_glosa_w > 0) then
				ie_gera_ocor_w := 'N';
			else
				select	count(1)
				into	qt_glosa_w
				from	pls_ocorrencia_benef a
				where	a.nr_seq_conta_pos_estab = r_c01_w.nr_sequencia
				and	exists(	select	1
						from	pls_ocorrencia_benef b
						where	b.nr_seq_mat = r_c01_w.nr_seq_conta_mat
						and	b.nr_seq_ocorrencia = a.nr_seq_ocorrencia);
						
				if	(qt_glosa_w > 0) then
					ie_gera_ocor_w := 'N';
				end if;
			end if;
		end if;
		
		if	(ie_gera_ocor_w = 'S') and
			(r_c02_w.qt_excecao > 0) then
			ie_excecao_w := pls_obter_se_regra_excecao_con(	r_c02_w.nr_seq_ocorrencia, nr_seq_conta_p, r_c01_w.nr_seq_conta_proc, 
									r_c01_w.nr_seq_conta_mat, r_c01_w.dt_referencia, r_c01_w.ie_tipo_item, 
									nr_seq_prestador_exec_w, r_c01_w.cd_procedimento, r_c01_w.ie_origem_proced, 
									r_c01_w.nr_seq_material, nr_seq_segurado_w, nm_usuario_p, cd_estabelecimento_p, nr_seq_plano_w, ie_internacao_w,
									qt_idade_w, qt_idade_meses_w, nr_seq_prestador_exec_w, nr_seq_prest_prot_w, cd_pessoa_fisica_w,
									nr_seq_contrato_w, nr_seq_intercambio_w, ie_tipo_segurado_w, dt_nascimento_w, ie_pcmso_w,
									nr_seq_clinica_w, nr_seq_tipo_acomod_conta_w, cd_guia_w, cd_guia_referencia_w, ie_tipo_guia_w, 
									nr_seq_tipo_atend_w, ie_carater_internacao_w, nr_seq_protocolo_w, nr_seq_tipo_acomodacao_w,
									ie_origem_protocolo_w, cd_prestador_exec_w, r_c02_w.nr_seq_regra_pos);			
			if	(ie_excecao_w = 'S') then
				ie_gera_ocor_w := 'N';
			end if;
		end if;
		
		-- se � v�lido ent�o temos que gerar a ocorr�ncia
		if	(ie_gera_ocor_w = 'S') then
			
			-- verifica se j� existe a ocorrencia
			select	count(1)
			into	nr_contador_w
			from	pls_ocorrencia_benef a
			where	a.nr_seq_conta_pos_estab = r_c01_w.nr_sequencia
			and	a.nr_seq_ocorrencia = r_c02_w.nr_seq_ocorrencia
			and	rownum	= 1;

			-- se existe s� faz update, sen�o insere
			if	(nr_contador_w = 0) then

				pls_inserir_ocorrencia(	nr_seq_segurado_w, r_c02_w.nr_seq_ocorrencia, null,
							null, null, null,
							null, r_c02_w.nr_seq_regra_pos, nm_usuario_p,
							'', null, 0,
							cd_estabelecimento_p, 'N', null,
							nr_seq_oc_benef_w, r_c01_w.nr_sequencia,
							null, null, null);
			else
				for r_c03_w in C03(r_c02_w.nr_seq_ocorrencia,r_c01_w.nr_sequencia ) loop
					begin
					update	pls_analise_glo_ocor_grupo
					set	ie_status	= 'P'
					where 	nr_sequencia	= r_c03_w.nr_sequencia
					and	ie_status	= 'L';
					end;
				end loop;
				update	pls_ocorrencia_benef
				set	ie_situacao		= 'A',
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate,
					ie_forma_inativacao 	= null
				where	nr_seq_conta_pos_estab	= r_c01_w.nr_sequencia
				and	nr_seq_ocorrencia	= r_c02_w.nr_seq_ocorrencia
				and	(ie_forma_inativacao 	is null or ie_forma_inativacao = 'S');

				update	pls_ocorrencia_benef
				set	ie_situacao		= 'I',
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate,
					ie_forma_inativacao 	= 'U'
				where	nr_seq_conta_pos_estab	= r_c01_w.nr_sequencia
				and	nr_seq_ocorrencia	= r_c02_w.nr_seq_ocorrencia
				and	ie_forma_inativacao 	= 'US';
			end if;
			
			
			if	(nvl(r_c02_w.ie_glosar_faturamento,'N') = 'S') then
			
				select	max(nr_sequencia)
				into	nr_seq_mot_liberacao_w
				from	pls_mot_lib_analise_conta
				where	ie_tipo_motivo 		= 'N'
				and	ie_situacao		= 'A'
				and	nvl(ie_analise_conta_med,'S') = 'S'
				and	cd_estabelecimento = cd_estabelecimento_p
				and     not (	ie_pre_analise	 = 'S'
					or	ie_reconsistencia	 = 'S'
					or 	ie_substitui_item	 = 'S'
					or	ie_documento_fisico = 'S'
					or	ie_glosa_manual	 = 'S'
					or	ie_liberacao_especial	 = 'S');
					
				--Caso tfor glosar o faturamento setar a variavel de valor pago ao prestador para 'N' - OS 986125 - aedemuth	
				if	(nvl(r_c01_w.ie_vl_pag_prestador,'X') = 'S') then
					update	pls_conta_pos_estabelecido
					set	ie_vl_pag_prestador = 'N'
					where	nr_sequencia = r_c01_w.nr_sequencia;
				end if;
				
					pls_analise_glosar_item_pos(	r_c01_w.nr_seq_analise, nr_seq_conta_p, r_c01_w.nr_seq_conta_proc,
									r_c01_w.nr_seq_conta_mat, r_c01_w.nr_sequencia, 0,
									nr_seq_mot_liberacao_w, null,cd_estabelecimento_p,
									null, null,null,
									nm_usuario_p,'S');
			end if;
		end if;
	end loop; -- C02
end loop; -- C01

end pls_gerar_ocorrencia_pos_estab;
/
