create or replace function get_if_sensitive_field_pan (nm_table_p       varchar2,
						       nm_field_p	varchar2)
						return varchar2 is
is_sensitive_field_w	varchar2(1) := 'N';
cd_perfil_w		perfil.cd_perfil%type;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
cd_funcao_w		funcao.cd_funcao%type;
cd_setor_w		setor_atendimento.cd_setor_atendimento%type;
nm_usuario_w		usuario.nm_usuario%type;

begin

if (nm_table_p is not null) and
   (nm_field_p is not null) then
   
    cd_perfil_w		 := nvl(obter_perfil_ativo,0);
    cd_estabelecimento_w := nvl(obter_estabelecimento_ativo,1);
    cd_funcao_w		 := nvl(obter_funcao_ativa,0);
    cd_setor_w		 := nvl(obter_setor_ativo,0);
    nm_usuario_w	 := nvl(obter_usuario_ativo, 'Tasy');
    
    if (upper(nm_table_p) = 'W_PAN_PACIENTE') then
	begin
        select nvl(max(ie_informacao_sensivel),'N')
        into  is_sensitive_field_w
        from  tabela_atributo
        where nm_tabela = upper(nm_table_p)
        and   nm_atributo = upper(nm_field_p);
	
	select	nvl(max(ie_sensitive), is_sensitive_field_w)
	into	is_sensitive_field_w
	from	tabela_atrib_regra
	where 	nm_tabela  = upper(nm_table_p)
	and	nm_atributo = upper(nm_field_p)
	and  	nvl(cd_estabelecimento,cd_estabelecimento_w) = cd_estabelecimento_w
	and	nvl(cd_perfil, cd_perfil_w) = cd_perfil_w
	and	nvl(cd_setor_atendimento, cd_setor_w) = cd_setor_w
	and	nvl(cd_funcao, cd_funcao_w) = cd_funcao_w
	and	nvl(nm_usuario_param, nm_usuario_w) = nm_usuario_w;
	
	exception
	when others then
	   is_sensitive_field_w := 'N';
	end;
	
	
	if (nvl(is_sensitive_field_w, 'N') = 'Y') then
	    is_sensitive_field_w := 'S';
	end if;

    end if;
end if;

return	is_sensitive_field_w;

end get_if_sensitive_field_pan;
/
