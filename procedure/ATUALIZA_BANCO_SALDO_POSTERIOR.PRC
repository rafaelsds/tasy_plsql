create or replace
procedure atualiza_banco_saldo_posterior
		(nr_seq_conta_p		number,
		dt_referencia_p		date) is

vl_atualizacao_w		number(15,2);
vl_saldo_inicial_w		number(15,2);
vl_transacao_w			number(15,2);
NR_SEQ_SALDO_BANCO_W		number(10,0);
dt_referencia_w			date;
ie_banco_w			varchar2(1);
dt_referencia_anterior_w	date;
dt_referencia_dia_w		date;
/* Projeto Multimoeda - Variáveis */
vl_atualizacao_estrang_w	number(15,2);
vl_transacao_estrang_w		number(15,2);
vl_saldo_ini_estrang_w		number(15,2);
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;

cursor c02 is
select	nr_sequencia,
	dt_referencia,
	vl_cotacao
from	banco_saldo	
where	nr_seq_conta	= nr_seq_conta_p
and	dt_referencia	>= dt_referencia_dia_w
order by	dt_referencia;

cursor c01 is
select	nvl(a.vl_transacao,0) vl_transacao,
	nvl(a.vl_transacao_estrang,0) vl_transacao_estrang,
	b.ie_banco
from 	transacao_financeira b,
	movto_trans_financ a
where	b.nr_sequencia 		= a.nr_seq_trans_financ
and	a.nr_seq_saldo_banco	= nr_seq_saldo_banco_w;

begin
/* Colocado esse tratamento de data a pedido do Edgar por haver clientes com saldos muito antigos abertos */
if	(trunc(dt_referencia_p,'month') >= to_date('01/02/2007','dd/mm/yyyy')) then

	dt_referencia_dia_w	:= trunc(dt_referencia_p,'dd');

	open	c02;
	loop
	fetch	c02 into
		nr_seq_saldo_banco_w,
		dt_referencia_w,
		vl_cotacao_w;
	exit	when c02%notfound;

		select	max(b.dt_referencia)
		into	dt_referencia_anterior_w
		from	banco_saldo b
		where	b.nr_seq_conta	= nr_seq_conta_p
		and	b.dt_referencia	< dt_referencia_w;

		select	nvl(max(a.vl_saldo),0)
		into	vl_saldo_inicial_w
		from	banco_saldo a
		where	a.nr_seq_conta	= nr_seq_conta_p
		and	a.dt_referencia	= dt_referencia_anterior_w;
		
		select	nvl(max(a.vl_saldo_estrang),0)
		into	vl_saldo_ini_estrang_w
		from	banco_saldo a
		where	a.nr_seq_conta	= nr_seq_conta_p
		and	a.dt_referencia	= dt_referencia_anterior_w;

		update	banco_saldo
		set	vl_saldo	= vl_saldo_inicial_w
		where	nr_sequencia	= nr_seq_saldo_banco_w;
		
		if (nvl(vl_cotacao_w,0) <> 0) then
			update	banco_saldo
			set	vl_saldo_estrang = vl_saldo_ini_estrang_w
			where	nr_sequencia = nr_seq_saldo_banco_w;
		end if;

		vl_atualizacao_w := 0;
		vl_atualizacao_estrang_w := 0;

		open	c01;
		loop
		fetch	c01 into
			vl_transacao_w,
			vl_transacao_estrang_w,
			ie_banco_w;
		exit	when c01%notfound;

			if	(ie_banco_w = 'C') then
				vl_atualizacao_w := vl_atualizacao_w + (vl_transacao_w * -1);
				if (nvl(vl_cotacao_w,0) <> 0) then
					vl_atualizacao_estrang_w := vl_atualizacao_estrang_w + (vl_transacao_estrang_w * -1);
				end if;
			elsif	(ie_banco_w = 'D') then
				vl_atualizacao_w := vl_atualizacao_w + vl_transacao_w;
				if (nvl(vl_cotacao_w,0) <> 0) then
					vl_atualizacao_estrang_w := vl_atualizacao_estrang_w + vl_transacao_estrang_w;
				end if;
			end if;

		end	loop;
		close	c01;

		update	banco_saldo
		set	vl_saldo	= vl_saldo + vl_atualizacao_w
		where	nr_sequencia	= nr_seq_saldo_banco_w;
		
		if (nvl(vl_cotacao_w,0) <> 0) then
			update banco_saldo
			set	vl_saldo_estrang = vl_saldo_estrang + vl_atualizacao_estrang_w
			where 	nr_sequencia = nr_seq_saldo_banco_w;
		end if;

	end	loop;
	close	c02;

	commit;
end if;

end atualiza_banco_saldo_posterior;
/
