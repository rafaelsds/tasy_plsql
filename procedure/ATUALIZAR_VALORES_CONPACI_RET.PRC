create or replace 
procedure Atualizar_Valores_ConPaci_Ret (	nr_sequencia_p		number,
					vl_pendente_p		number,
					vl_glosa_devida_p	number,
					vl_glosa_indevida_p	number,
					vl_reapresentacao_p	number,
					vl_nao_auditado_p	number,
					nm_usuario_p		varchar2) is

nr_interno_conta_w	number(10);
cd_autorizacao_w		varchar2(20);
vl_inicial_w		number(15,2);
vl_pendente_w		number(15,2);
vl_glosa_devida_w		number(15,2);
vl_glosa_indevida_w	number(15,2);
vl_reapresentacao_w	number(15,2);
vl_nao_auditado_w		number(15,2);

nr_seq_ret_item_w		number(10);
nr_seq_retorno_w		number(10);

nr_seq_hist_audit_w	number(10);

begin

select	nr_interno_conta,
		cd_autorizacao,
		vl_inicial,
		vl_pendente,
		vl_glosa_devida,
		vl_glosa_indevida,
		vl_reapresentacao,
		vl_nao_auditado
into		nr_interno_conta_w,
		cd_autorizacao_w,
		vl_inicial_w,
		vl_pendente_w,
		vl_glosa_devida_w,
		vl_glosa_indevida_w,
		vl_reapresentacao_w,
		vl_nao_auditado_w
from	conta_paciente_retorno a
where a.nr_sequencia = nr_sequencia_p;

select max(a.nr_sequencia),
	max(a.nr_seq_retorno)
into	nr_seq_ret_item_w,
	nr_seq_retorno_w
from	convenio_retorno b,
	convenio_retorno_item a
where b.nr_sequencia = a.nr_seq_retorno
  and b.ie_status_retorno = 'F'
  and a.nr_interno_conta = nr_interno_conta_w
  and a.cd_autorizacao = cd_autorizacao_w
  and b.dt_fechamento <= (select nvl(max(dt_historico), max(dt_inicial))
				from	conta_paciente_ret_hist y,
					conta_paciente_retorno x
				where y.nr_seq_conpaci_ret = x.nr_sequencia(+)
				  and x.nr_sequencia = nr_sequencia_p);

if	(vl_pendente_w > 0) and
	((vl_pendente_p + vl_glosa_devida_p + vl_glosa_indevida_p + vl_reapresentacao_p + vl_nao_auditado_p) <> vl_pendente_w) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(277610);
end if;

if	(vl_pendente_w = 0) and
	(vl_glosa_indevida_w > 0) and
	((vl_glosa_devida_p + vl_reapresentacao_p + vl_glosa_indevida_p) <> vl_glosa_indevida_w) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(277611);
end if;

if	(vl_pendente_w = 0) and
	(vl_glosa_indevida_w = 0) and
	(vl_reapresentacao_w > 0) and
	((vl_glosa_devida_p + vl_reapresentacao_p) <> vl_reapresentacao_w) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(277612);
end if;

if	(vl_glosa_devida_p < 0) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(277613);
end if;

if	(vl_nao_auditado_p < 0) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(277614);
end if;

if	(vl_nao_auditado_p <> 0) then
	select nvl(max(nr_sequencia),0)
	into nr_seq_hist_audit_w
	from hist_audit_conta_paciente
	where ie_acao = 0;

	insert into conta_paciente_ret_hist
	(	nr_sequencia, nr_seq_conpaci_ret, dt_historico, 
		vl_historico, nr_seq_hist_audit, dt_atualizacao, 
		nm_usuario, nr_seq_ret_item, ds_observacao, nm_usuario_resp)
	values
	(	conta_paciente_ret_hist_seq.NextVal, nr_sequencia_p, sysdate,
		vl_nao_auditado_p, nr_seq_hist_audit_w, sysdate,
		nm_usuario_p, nr_seq_ret_item_w, null,nm_usuario_p);
end if;

if	(vl_reapresentacao_p <> 0) and
	((vl_pendente_w <> 0) or
	 (vl_glosa_indevida_w <> 0)) then
	select nvl(max(nr_sequencia),0)
	into nr_seq_hist_audit_w
	from hist_audit_conta_paciente
	where ie_acao = 2;

	insert into conta_paciente_ret_hist
	(	nr_sequencia, nr_seq_conpaci_ret, dt_historico, 
		vl_historico, nr_seq_hist_audit, dt_atualizacao, 
		nm_usuario, nr_seq_ret_item, ds_observacao, nm_usuario_resp)
	values
	(	conta_paciente_ret_hist_seq.NextVal, nr_sequencia_p, sysdate,
		vl_reapresentacao_p, nr_seq_hist_audit_w, sysdate,
		nm_usuario_p, nr_seq_ret_item_w, null,nm_usuario_p);

end if;

if	(vl_glosa_devida_p <> 0) then
	select nvl(max(nr_sequencia),0)
	into nr_seq_hist_audit_w
	from hist_audit_conta_paciente
	where ie_acao = 3;

	insert into conta_paciente_ret_hist
	(	nr_sequencia, nr_seq_conpaci_ret, dt_historico, 
		vl_historico, nr_seq_hist_audit, dt_atualizacao, 
		nm_usuario, nr_seq_ret_item, ds_observacao, nm_usuario_resp)
	values
	(	conta_paciente_ret_hist_seq.NextVal, nr_sequencia_p, sysdate,
		vl_glosa_devida_p, nr_seq_hist_audit_w, sysdate,
		nm_usuario_p, nr_seq_ret_item_w, null,nm_usuario_p);
end if;

if	(vl_glosa_indevida_p <> 0) and
	(vl_pendente_w <> 0) then
	select nvl(max(nr_sequencia),0)
	into nr_seq_hist_audit_w
	from hist_audit_conta_paciente
	where ie_acao = 4;

	insert into conta_paciente_ret_hist
	(	nr_sequencia, nr_seq_conpaci_ret, dt_historico, 
		vl_historico, nr_seq_hist_audit, dt_atualizacao, 
		nm_usuario, nr_seq_ret_item, ds_observacao, nm_usuario_resp)
	values
	(	conta_paciente_ret_hist_seq.NextVal, nr_sequencia_p, sysdate,
		vl_glosa_indevida_p, nr_seq_hist_audit_w, sysdate,
		nm_usuario_p, nr_seq_ret_item_w, null,nm_usuario_p);
end if;

commit;

end;
/