create or replace
procedure Gerar_eis_saude_mulher(	dt_parametro_p		date,
									nm_usuario_p		Varchar2) is

cd_pessoa_fisica_w			varchar2(10);
ie_pac_gravida_w			varchar2(1);
ie_sexo_w					varchar2(1);
nr_atendimento_w			number(10);
nr_seq_area_w				number(10);
nr_seq_microarea_w			number(10);
qt_vacina_w					number(10);
qt_atend_pre_natal_w		number(10);
dt_parametro_w				date;
dt_parametro_fim_w			date;

Cursor C01 is
	select	a.cd_pessoa_fisica,
			a.nr_atendimento,
			b.ie_sexo
	from	atendimento_paciente a,
			pessoa_fisica b
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and		a.ie_nivel_atencao = 'P'
	and		b.ie_sexo = 'F'
	and		a.dt_entrada between dt_parametro_w and dt_parametro_fim_w
	order by a.nr_atendimento;

begin

dt_parametro_fim_w                    :=
		       (to_date((to_char(dt_parametro_p,'dd/mm/yy') ||
			' 23:59:59'),'dd/mm/yy hh24:mi:ss'));
dt_parametro_w                        :=
       		(to_date((to_char(dt_parametro_p,'dd/mm/yy') ||
			' 00:00:00'),'dd/mm/yy hh24:mi:ss'));


delete from eis_saude_mulher
where dt_referencia = dt_parametro_w;


open C01;
loop
fetch C01 into
		cd_pessoa_fisica_w,
		nr_atendimento_w,
		ie_sexo_w;
exit when C01%notfound;
	begin

	select	max(a.nr_seq_area),
			max(a.nr_seq_microarea)
	into	nr_seq_area_w,
			nr_seq_microarea_w
	from	domicilio_familia a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;
	
	select	nvl(max(a.ie_pac_gravida),'N')
	into	ie_pac_gravida_w
	from	atendimento_gravidez a
	where	nr_atendimento = nr_atendimento_w;
	
	select	count(*)
	into	qt_vacina_w
	from  	vacina b,
		vacina_calendario c
	where	b.nr_sequencia	= c.nr_seq_vacina
	and	((sysdate-nvl(to_Date(obter_dados_pf(cd_pessoa_fisica_w,'DN'),'dd/mm/yyyy'),sysdate)) between Obter_idade_min_max_vacina(c.nr_sequencia,'MIN')  and  Obter_idade_min_max_vacina(c.nr_sequencia,'MAX'))
	and  Obter_idade_min_max_vacina(c.nr_sequencia,'MIN') is not null
	and  Obter_idade_min_max_vacina(c.nr_sequencia,'MAX') is not null
	and   not exists (select 1 from paciente_vacina e where e.nr_seq_vacina = b.nr_sequencia and e.cd_pessoa_fisica = cd_pessoa_fisica_w and c.ie_dose = e.ie_dose);
	
	select	count(*)
	into	qt_atend_pre_natal_w
	from	med_pac_pre_natal a
	where	a.cd_pessoa_fisica = cd_pessoa_fisica_w;

	insert into eis_saude_mulher (
							nr_sequencia,
							dt_atualizacao,
							dt_atualizacao_nrec,
							nm_usuario,
							nm_usuario_nrec,
							nr_seq_area,
							nr_seq_microarea,
							cd_pessoa_fisica,
							nr_atendimento,
							ie_pac_gravida,
							qt_vacina,
							ie_sexo,
							dt_referencia,
							qt_atend_pre_natal)
			values(			eis_saude_mulher_seq.nextval,
							sysdate,
							sysdate,
							nm_usuario_p,
							nm_usuario_p,
							nr_seq_area_w,
							nr_seq_microarea_w,
							cd_pessoa_fisica_w,
							nr_atendimento_w,
							ie_pac_gravida_w,
							qt_vacina_w,
							ie_sexo_w,
							dt_parametro_w,
							qt_atend_pre_natal_w);


	end;
end loop;
close C01;

commit;

end Gerar_eis_saude_mulher;
/