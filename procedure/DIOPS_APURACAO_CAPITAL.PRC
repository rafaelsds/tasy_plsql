create or replace
procedure diops_apuracao_capital(	nm_usuario_p		Varchar2,
					nr_seq_periodo_p	Number,
					cd_estabelecimento_p	Number,
					ie_opcao_p		Number) is

/* -------------------------------------------------------------------------------------

ie_opcao_p
	1 -> Credito/Debito
	2 -> Detalhamento
------------------------------------------------------------------------------------- */
begin


if	(ie_opcao_p = 1) then
	insert into diops_cred_deb_apur_capit(
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_periodo,
		nr_registro_ans,
		vl_campo_2,
		vl_campo_3,
		vl_campo_4,
		vl_campo_5,
		vl_campo_6,
		vl_campo_7,
		vl_campo_8,
		vl_campo_9,
		vl_campo_10
	) values (
		diops_cred_deb_apur_capit_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_periodo_p,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	);
elsif (ie_opcao_p = 2) then
	insert into diops_apur_capital_det(
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_periodo,
		vl_campo_1,
		vl_campo_2,
		vl_campo_3,
		vl_campo_4,
		vl_campo_5
	) values (
		diops_apur_capital_det_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_seq_periodo_p,
		0,
		0,
		0,
		0,
		0
	);
end if;
commit;

end diops_apuracao_capital;
/