create or replace
procedure SCHEMATIC_GERAR_PARAM_BO(nr_seq_objeto_p	number,
				nm_usuario_p		varchar2) is

nr_seq_bo_w	number(10);
				
begin

select	nr_seq_bo_schematic
into	nr_seq_bo_w
from	objeto_schematic
where	nr_sequencia = nr_seq_objeto_p;

if	(nr_seq_bo_w is not null) then

	insert into OBJ_SCHEMATIC_ATIV_PARAM (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_ativacao,
		nr_seq_objeto,
		nm_parametro,
		nr_seq_apres,
		nr_seq_schematic_relatorio,
		nr_seq_obj_evt_acao)
	select	obj_schematic_ativ_param_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		null,
		nr_seq_objeto_p,
		nm_parameter,
		nr_order,
		null,
		null
	from	business_object_parameter a
	where	nr_seq_bo = nr_seq_bo_w
	and	tp_parameter = 'IN'
	and	not exists (select 1 from obj_schematic_ativ_param x
				where	x.nr_seq_objeto = nr_seq_objeto_p
				and	x.nm_parametro = a.nm_parameter);
	commit;
end if;

end SCHEMATIC_GERAR_PARAM_BO;
/