create or replace
procedure gerar_caixa_opme(	nr_seq_caixa_p	number,
				nm_usuario_p	varchar2 )
				is


nr_sequencia_w	number(10,0);
nr_seq_tipo_w	number(10,0);

begin

select	opme_caixa_item_seq.nextval
into	nr_sequencia_w
from	dual;

select	nr_seq_tipo
into	nr_seq_tipo_w
from	opme_caixa
where	nr_sequencia	= nr_seq_caixa_p;	

insert into opme_caixa_item(
	nr_sequencia,
	nr_seq_caixa,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_material,
	qt_inicial,
	qt_saldo,
	nr_seq_marca)
select	opme_caixa_item_seq.nextval,
	nr_seq_caixa_p,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	a.cd_material,
	a.qt_item,
	a.qt_item,
	a.nr_seq_marca
from	opme_tipo_caixa_item a
where	a.nr_seq_tipo	=	nr_seq_tipo_w
and	a.dt_exclusao is null
and	not exists(	select	1
			from	opme_caixa_item x
			where	x.nr_seq_caixa	=	nr_seq_caixa_p
			and	x.cd_material	=	a.cd_material );		

commit;

end gerar_caixa_opme;
/