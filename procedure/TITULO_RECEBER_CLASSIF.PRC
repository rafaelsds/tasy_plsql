create or replace
procedure inserir_classif_tit_rec(nr_titulo_p		titulo_receber.nr_titulo%type,
				cd_conta_financ_p	conta_financeira.cd_conta_financ%type,
				cd_conta_contabil_p	conta_contabil.cd_conta_contabil%type,
				cd_centro_custo_p	centro_custo.cd_centro_custo%type,
				cd_historico_p		historico_padrao.cd_historico%type,
				vl_classificacao_p	titulo_receber_classif.vl_classificacao%type,
				nm_usuario_p		varchar2) as 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Insere na tabela titulo_receber_classif
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:
------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
nr_seq_classif_w		titulo_receber_classif.nr_sequencia%type;

begin
if	((cd_conta_financ_p is not null) or
	(cd_conta_contabil_p is not null)) and
	(vl_classificacao_p is not null) then
	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_classif_w
	from	titulo_receber_classif
	where	nr_titulo	= nr_titulo_p;

	insert into titulo_receber_classif
		(nr_sequencia,
		nr_titulo,
		dt_atualizacao,
		nm_usuario,
		cd_conta_financ,
		cd_centro_custo,
		cd_conta_contabil,
		nr_seq_contrato,
		nr_seq_produto,
		cd_historico,
		vl_classificacao,
		vl_desconto)
	values	(nr_seq_classif_w,
		nr_titulo_p,
		sysdate,
		nm_usuario_p,
		cd_conta_financ_p,
		cd_centro_custo_p,
		cd_conta_contabil_p,
		null,
		null,
		cd_historico_p,
		vl_classificacao_p,
		0);

end if;
end ;
/