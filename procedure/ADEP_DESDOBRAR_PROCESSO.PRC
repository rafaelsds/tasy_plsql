create or replace
procedure adep_desdobrar_processo (
                nr_processo_p               number,
                nr_etiquetas_desdobrar_p    varchar2,
                nr_seq_area_prep_p          number,
                nm_usuario_p                varchar2,
                nr_processo_desdobrado_p    out number) is

nr_processo_w               number(10,0);
nr_etiquetas_desdobrar_w    varchar2(1000);
tam_lista_w                 number(10,0);
ie_pos_virgula_w            number(5,0);
nr_seq_etiqueta_w           number(10,0);
nr_prescricao_w             number(14,0);
nr_seq_material_w           number(6,0);
nr_seq_etiq_w               number(10,0);
nr_seq_processo_desd_w      number(10,0);
nr_atendimento_proc_w       prescr_medica.nr_atendimento%type;
nr_atendimento_etiq_w       prescr_medica.nr_atendimento%type;
qt_itens_processo_w         Number(5);
qt_itens_processo_susp_w    Number(5);
dt_horario_w                prescr_mat_hor.dt_horario%type;
dt_horario_proc_w           adep_processo.dt_horario_processo%type;
ie_agrupador_w				prescr_mat_hor.ie_agrupador%type;

begin

if  (nr_processo_p is not null) and
    (nr_etiquetas_desdobrar_p is not null) and
    (nm_usuario_p is not null) then
    begin

    select  adep_processo_seq.nextval
    into    nr_processo_w
    from    dual;

    insert into adep_processo (
        nr_sequencia,
        dt_atualizacao,
        nm_usuario,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        nr_seq_processo,
        ie_tipo_processo,
        nr_atendimento,
        dt_horario_processo,
        ie_status_processo,
        cd_setor_atendimento,
        cd_local_estoque,
        dt_processo,
        nm_usuario_processo,
        dt_dispensacao,
        nm_usuario_dispensacao,
        dt_retirada,
        nm_usuario_retirada,
        cd_pessoa_retirada,
        dt_leitura,
        nm_usuario_leitura,
        dt_preparo,
        nm_usuario_preparo,
        dt_paciente,
        nm_usuario_paciente,
        cd_funcao_origem,
        ie_origem_processo,
        ie_gedipa,
        ie_urgente,
        cd_intervalo,
        ie_acm,
        ie_se_necessario,
        ie_dose_especial)
    select  nr_processo_w,
        sysdate,
        nm_usuario_p,
        sysdate,
        nm_usuario_p,
        nr_processo_p,
        ie_tipo_processo,
        nr_atendimento,
        dt_horario_processo,
        ie_status_processo,
        cd_setor_atendimento,
        cd_local_estoque,
        sysdate,
        nm_usuario_p,
        dt_dispensacao,
        nm_usuario_dispensacao,
        dt_retirada,
        nm_usuario_retirada,
        cd_pessoa_retirada,
        dt_leitura,
        nm_usuario_leitura,
        dt_preparo,
        nm_usuario_preparo,
        dt_paciente,
        nm_usuario_paciente,
        cd_funcao_origem,
        ie_origem_processo,
        ie_gedipa,
        nvl(ie_urgente, 'N'),
        cd_intervalo,
        ie_acm,
        ie_se_necessario,
        ie_dose_especial
    from    adep_processo
    where   nr_sequencia = nr_processo_p;

    select  nvl(max(nr_sequencia),0)
    into    nr_seq_etiq_w
    from    lp_individual
    where   nr_seq_processo     =   nr_processo_p;

    if  (nr_seq_etiq_w is not null) and
        (nr_seq_etiq_w > 0) then
        select  lp_processo_desdobrado_seq.nextval
        into    nr_seq_processo_desd_w
        from    dual;

        insert  into lp_processo_desdobrado (nr_sequencia,
                                             dt_atualizacao,
                                             nm_usuario,
                                             dt_atualizacao_nrec,
                                             nm_usuario_nrec,
                                             nr_seq_lp_individual,
                                             nr_seq_processo)
                values                      (nr_seq_processo_desd_w,
                                             sysdate,
                                             nm_usuario_p,
                                             sysdate,
                                             nm_usuario_p,
                                             nr_seq_etiq_w,
                                             nr_processo_w);
    end if;

    nr_etiquetas_desdobrar_w    := nr_etiquetas_desdobrar_p;

    while   (nr_etiquetas_desdobrar_w is not null) loop
        begin

        tam_lista_w := length(nr_etiquetas_desdobrar_w);
        ie_pos_virgula_w    := instr(nr_etiquetas_desdobrar_w,',');

        if  (ie_pos_virgula_w <> 0) then
            nr_seq_etiqueta_w       := to_number(substr(nr_etiquetas_desdobrar_w,1,(ie_pos_virgula_w - 1)));
            nr_etiquetas_desdobrar_w    := substr(nr_etiquetas_desdobrar_w,(ie_pos_virgula_w + 1),tam_lista_w);
        else
            nr_seq_etiqueta_w       := to_number(nr_etiquetas_desdobrar_w);
            nr_etiquetas_desdobrar_w    := null;
        end if;

        if  (nr_seq_etiqueta_w > 0) then

            select  max(nr_atendimento)
            into    nr_atendimento_proc_w
            from    adep_processo
            where   nr_sequencia    = nr_processo_p;

            select  max(a.nr_atendimento)
            into    nr_atendimento_etiq_w
            from    prescr_medica a,
                    prescr_mat_hor b
            where   a.nr_prescricao = b.nr_prescricao
            and     b.nr_sequencia = nr_seq_etiqueta_w;

            if (nvl(nr_atendimento_proc_w, 0) <> nvl(nr_atendimento_etiq_w, 0)) then
                rollback;
                gravar_log_tasy(2311,
                                ' nr_seq_horario: '         || nr_seq_etiqueta_w ||
                                ' nr_seq_processo: '        || nr_processo_p ||
                                ' nr_atendimento_etiq_w: '  || nr_atendimento_etiq_w ||
                                ' nr_atendimento_proc_w: '  || nr_atendimento_proc_w
                                , 'OS_1197521');

                --Ocorreu um erro ao desdobrar o processo: o atendimento do processo desdobrado e dos horarios e diferente. Reabra a funcao GEDIPA / ADEP e tente novamente.
                wheb_mensagem_pck.exibir_mensagem_abort(714585);
            end if;

            select  max(a.nr_prescricao),
                    max(a.nr_seq_material),
                    max(a.dt_horario),
					max(ie_agrupador)
            into    nr_prescricao_w,
                    nr_seq_material_w,
                    dt_horario_w,
					ie_agrupador_w
            from    prescr_mat_hor a
            where   a.nr_sequencia = nr_seq_etiqueta_w
            and     obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S';

            select  max(a.dt_horario_processo)
            into    dt_horario_proc_w
            from    adep_processo a
            where   a.nr_sequencia = nr_processo_w;

            if (dt_horario_w = dt_horario_proc_w) then
                update  prescr_mat_hor
                set     nr_seq_processo = nr_processo_w
                where   nr_sequencia    = nr_seq_etiqueta_w;

                update  prescr_mat_hor
                set     nr_seq_processo = nr_processo_w
                where   nr_seq_processo = nr_processo_p
                and     nr_prescricao   = nr_prescricao_w
                and     nr_seq_superior = nr_seq_material_w
                and     obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S';

                update  adep_processo_frac a
                set     a.nr_seq_processo   = nr_processo_w
                where   a.nr_seq_processo   = nr_processo_p
                and exists (
                        select  1
                        from    prescr_mat_hor b
                        where   b.nr_seq_processo   = nr_processo_w
                        and b.nr_seq_etiqueta   = a.nr_sequencia);

                update  adep_processo_item a
                set a.nr_seq_processo   = nr_processo_w
                where   a.nr_seq_processo   = nr_processo_p
                and exists (
                        select  1
                        from    prescr_mat_hor b
                        where   b.nr_sequencia      = a.nr_seq_horario
                        and b.nr_seq_processo   = nr_processo_w);

				update	adep_processo a
				set		a.nr_prescricao = nr_prescricao_w,
						a.nr_seq_material = decode(ie_agrupador_w, 11, nr_seq_material_w, null)
				where	a.nr_sequencia = nr_processo_w;
            end if;
        end if;

        end;
    end loop;

    select  count(1)
    into    qt_itens_processo_w
    from    prescr_mat_hor
    where   nr_seq_processo = nr_processo_w
    and     exists  (   select  1
                        from    adep_processo
                        where   nr_sequencia = nr_processo_w
                        and     dt_paciente is not null);

    select  count(1)
    into    qt_itens_processo_susp_w
    from    prescr_mat_hor
    where   nr_seq_processo = nr_processo_w
    and     exists  (   select  1
                        from    adep_processo
                        where   nr_sequencia = nr_processo_w
                        and     dt_paciente is not null)
    and     dt_suspensao is not null;

    if  (qt_itens_processo_w = qt_itens_processo_susp_w) then
        update  adep_processo
        set     dt_paciente = null
        where   nr_sequencia = nr_processo_w;
    end if;

    atual_adep_proc_area_desdobr(nr_processo_p, nr_processo_w, nm_usuario_p);

    exception
        when others then
        rollback;
        Wheb_mensagem_pck.exibir_mensagem_abort(235172,
                        'PROCESSO=' || to_char(nr_processo_p) ||
                        'ETIQUETAS=' || nr_etiquetas_desdobrar_p);


    end;
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

nr_processo_desdobrado_p    := nr_processo_w;

end adep_desdobrar_processo;
/
