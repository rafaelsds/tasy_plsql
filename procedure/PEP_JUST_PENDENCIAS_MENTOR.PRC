create or replace
procedure pep_just_pendencias_mentor(nr_seq_pend_pac_acao_dest_p number,
                                     acao_executada              number,
                                     ds_justificativa_p          varchar2 default null,
                                     nr_seq_equipe_p             number default null,
									 nr_seq_justificativa_p      number default 0,
									 ie_justificativa_origem_p   varchar2 default 'S') is

  nr_prescricao_w        number(14,0)   := -1;
  nr_sae_w               number(14,0)   := -1;
  nr_seq_origem_w        number(10,0)   := -1;
  nr_seq_pend_pac_w      number(10,0)   := -1;
  nr_seq_pend_regra_w    number(10,0)   := -1;
  nr_seq_resultado_w     number(10,0)   := -1;
  nr_seq_result_item_w   number(10,0)   := -1;
  nr_seq_prendencia_w    number(10,0)   := -1;
  nr_seq_equipe_w        number(10,0)   := -1;
  nr_seq_pend_pac_acao_w number(10,0)   := -1;
  nr_seq_dest_acao_w	 number(10,0)   := -1;
  ie_continuar_protocolo_w varchar2(1)  := 'N';

  ie_acoes_opcionais_w   char           := 'N';
  dt_justificativa_w     date;
  ds_justificativa_w     varchar(200);
  ds_regra_w             varchar(200);
  ds_acao_w              varchar(200);
  nm_usuario_w           varchar(15);
  cd_pessoa_fisica_w     varchar(10);
  ds_expressao_w         varchar(200);
  nr_seq_justificativa_w number(10);

  const_acao_liberada     	constant number := 1;
  const_acao_suspensa     	constant number := 2;
  const_acao_justificada  	constant number := 3;
  const_acao_liberada_equipe    constant number := 4;
  const_acao_justificada_equipe constant number := 5;

  ie_profissional_medico_w gqa_pend_pac_acao_dest.ie_profissional_medico%TYPE := 'N';
  nr_sequencia_acao_w gqa_pend_pac_acao_dest.nr_sequencia%TYPE;

  SQL_acao_liberada varchar(2000) := '          update gqa_pend_pac_acao_dest
						set    dt_justificativa    = sysdate,
						       dt_encerramento     = sysdate,
						       ds_justificativa    = :ds_justificativa_w,
						       ie_justificado_auto = ''S'',
							   ie_justificativa_origem = ''S''
						where  nr_sequencia in
						(
						       select  c.nr_sequencia
						       from    gqa_pendencia_pac       a,
							       gqa_pend_pac_acao       b,
							       gqa_pend_pac_acao_dest  c 
						       where   a.nr_sequencia          = b.nr_seq_pend_pac
						       and     b.nr_sequencia          = c.nr_seq_pend_pac_acao
						       and     a.nr_seq_origem         = :nr_seq_origem_w
						       and     a.nr_seq_pend_regra     = :nr_seq_pend_regra_w
						       <criterio1>
						)
						and    nr_seq_pend_pac_acao  <> :nr_seq_pend_pac_acao_dest_p
						<criterio2>
						and    dt_justificativa  is null
						and    ds_justificativa  is null';

  SQL_acao_liberada_exames varchar(2000) := '   update gqa_pend_pac_acao_dest
						set    dt_justificativa    = sysdate,
						       dt_encerramento     = sysdate,
						       ds_justificativa    = :ds_justificativa_w,
						       ie_justificado_auto = ''S'',
							   ie_justificativa_origem = ''S''
						where  nr_sequencia in
						(
						       select  c.nr_sequencia
						       from    gqa_pendencia_pac       a,
							       gqa_pend_pac_acao       b,
							       gqa_pend_pac_acao_dest  c 
						       where   a.nr_sequencia          = b.nr_seq_pend_pac
						       and     a.nr_seq_result_item    = :nr_seq_result_item_w
						       and     a.nr_seq_resultado      = :nr_seq_resultado_w
						       and     a.nr_seq_pend_regra     = :nr_seq_pend_regra_w
						       and     b.nr_sequencia          = c.nr_seq_pend_pac_acao
						       <criterio1>
						)
						and    nr_seq_pend_pac_acao  <> :nr_seq_pend_pac_acao_dest_p
						<criterio2>
						and    dt_justificativa  is null
						and    ds_justificativa  is null';

  SQL_acao_justificada varchar(2000) := '       update gqa_pend_pac_acao_dest 
						set    dt_justificativa    = :dt_justificativa_w, 
						       dt_encerramento     = :dt_justificativa_w, 
						       ds_justificativa    = :ds_justificativa_w, 
						       ie_justificado_auto = ''N'',
							   nr_seq_justificativa = :nr_seq_justificativa_w,
							   ie_justificativa_origem = :ie_justificativa_origem_w
						where  nr_sequencia in 
						( 
						       select  c.nr_sequencia
						       from    gqa_pendencia_pac       a,
							       gqa_pend_pac_acao       b,
							       gqa_pend_pac_acao_dest  c 
						       where   a.nr_sequencia          = b.nr_seq_pend_pac
						       and     b.nr_sequencia          = c.nr_seq_pend_pac_acao
						       and     a.nr_seq_origem         = :nr_seq_origem_w
						       and     a.nr_seq_pend_regra     = :nr_seq_pend_regra_w
						       <criterio1>
						) 
						<criterio2>
						and    dt_justificativa  is null
						and    ds_justificativa  is null';


  SQL_acao_justificada_exames varchar(2000) := 'update gqa_pend_pac_acao_dest
						set    dt_justificativa    = :dt_justificativa_w,
						       dt_encerramento     = :dt_justificativa_w,
						       ds_justificativa    = :ds_justificativa_w,
						       ie_justificado_auto = ''N'',
							   nr_seq_justificativa = :nr_seq_justificativa_w,
							   ie_justificativa_origem = :ie_justificativa_origem_w
						where  nr_sequencia in
						(
						       select  c.nr_sequencia
						       from    gqa_pendencia_pac       a,
							       gqa_pend_pac_acao       b,
							       gqa_pend_pac_acao_dest  c 
						       where   a.nr_sequencia          = b.nr_seq_pend_pac
						       and     a.nr_seq_result_item    = :nr_seq_result_item_w
						       and     a.nr_seq_resultado      = :nr_seq_resultado_w
						       and     a.nr_seq_pend_regra     = :nr_seq_pend_regra_w
						       and     b.nr_sequencia          = c.nr_seq_pend_pac_acao
						       <criterio1>
						)
						<criterio2>
						and    dt_justificativa  is null
						and    ds_justificativa  is null';
						
  SQL_acao_justificada_dest varchar(2000) := '       update gqa_pend_pac_acao_dest 
						set    dt_justificativa    = :dt_justificativa_w, 
						       dt_encerramento     = :dt_justificativa_w, 
						       ds_justificativa    = :ds_justificativa_w, 
						       ie_justificado_auto = ''N'',
							   nr_seq_justificativa = :nr_seq_justificativa_w	,
							   ie_justificativa_origem = ''S''
						where  nr_sequencia in 
						( 
						       select  c.nr_sequencia
						       from    gqa_pendencia_pac       a,
							       gqa_pend_pac_acao       b,
							       gqa_pend_pac_acao_dest  c 
						       where   a.nr_sequencia          = b.nr_seq_pend_pac
						       and     b.nr_sequencia          = c.nr_seq_pend_pac_acao
						       and     c.nr_sequencia          = :nr_seq_dest_acao_w
						       and     a.nr_seq_pend_regra     = :nr_seq_pend_regra_w
						       <criterio1>
						) 
						<criterio2>
						and    dt_justificativa  is null
						and    ds_justificativa  is null';

  SQL_executar varchar(2000) := '';
  
CURSOR c01 IS
    SELECT  (f.nr_seq_equipe) nr_seq_equipe
    FROM    gqa_acao                e,
            gqa_acao_regra_prof     f 
    WHERE   e.nr_seq_pend_regra = nr_seq_pend_regra_w
    AND	    e.nr_sequencia          = f.nr_seq_acao
    AND     obter_se_medico_equipe(f.nr_seq_equipe, cd_pessoa_fisica_w) = 'S'
    AND     f.nr_seq_equipe IS NOT NULL;  

begin
nm_usuario_w       := wheb_usuario_pck.get_nm_usuario;
cd_pessoa_fisica_w := Obter_Pessoa_Fisica_Usuario(nm_usuario_w, 'C');

if (nr_seq_justificativa_p > 0) then
	nr_seq_justificativa_w := nr_seq_justificativa_p;
end if;

select nvl(max(ie_continuar_protocolo),'N')
into   ie_continuar_protocolo_w
from   gqa_pend_justificativa
where  nr_sequencia = nr_seq_justificativa_w;


if (nvl(nr_seq_pend_pac_acao_dest_p, 0) > 0)  then

    select  max(d.ds_regra),
            max(b.ds_acao),
            max(b.nr_seq_pend_pac),
            max(nvl(d.ie_acoes_opcionais, 'N')),
            max(a.nr_seq_origem),
            max(a.nr_seq_pend_regra),
            max(a.nr_seq_resultado),
            max(a.nr_seq_result_item),
            max(a.nr_sequencia),
            max(nvl(c.ds_justificativa, '')),
            max(c.dt_justificativa),
			max(c.nr_sequencia)
    into    ds_regra_w,
            ds_acao_w,
            nr_seq_pend_pac_w,
            ie_acoes_opcionais_w,
            nr_seq_origem_w,
            nr_seq_pend_regra_w,
            nr_seq_resultado_w,
            nr_seq_result_item_w,
            nr_seq_prendencia_w,
            ds_justificativa_w,
            dt_justificativa_w,
			nr_seq_dest_acao_w
    from    gqa_pendencia_pac       a,
            gqa_pend_pac_acao       b,
            gqa_pend_pac_acao_dest  c,
            gqa_pendencia_regra     d
    where   a.nr_sequencia          = b.nr_seq_pend_pac
    and     b.nr_sequencia          = c.nr_seq_pend_pac_acao
    and     d.nr_sequencia          = a.nr_seq_pend_regra
    and     c.nr_seq_pend_pac_acao  = nr_seq_pend_pac_acao_dest_p
    and     (c.cd_pessoa_fisica      = cd_pessoa_fisica_w or c.ie_profissional_medico = 'S');
    
end if;

nr_seq_equipe_w := nr_seq_equipe_p;

select nvl(max(ie_continuar_protocolo),'N')
into   ie_continuar_protocolo_w
from   gqa_pend_justificativa
where  nr_sequencia = nr_seq_justificativa_w;

select min(nr_sequencia)
into nr_sequencia_acao_w
from gqa_pend_pac_acao_dest
where NR_SEQ_PEND_PAC_ACAO = nr_seq_pend_pac_acao_dest_p;

select nvl(ie_profissional_medico,'N')
into ie_profissional_medico_w
from gqa_pend_pac_acao_dest
where nr_sequencia = nr_sequencia_acao_w;

if (ie_continuar_protocolo_w = 'N' or ie_profissional_medico_w <> 'S') then
case acao_executada

    when const_acao_liberada then

        if (nvl(nr_seq_pend_pac_acao_dest_p, 0) > 0)  then

            select   nvl(max(nr_prescricao), -1)
            into     nr_prescricao_w
            from     prescr_medica
            where    nr_seq_pend_pac_acao = nr_seq_pend_pac_acao_dest_p;

            if (nr_prescricao_w < 0) then

                select   nvl(max(nr_prescricao), -1),
                        nvl(max(nr_seq_pend_pac_acao), 0)
                into     nr_sae_w,
                        nr_seq_pend_pac_acao_w
                from     pe_prescricao
                where    nr_seq_pend_pac_acao = nr_seq_pend_pac_acao_dest_p
                and      ie_situacao <> 'I';

            end if;

            if ((nr_prescricao_w > 0) or (nr_sae_w > 0) or (nr_seq_pend_pac_acao_w > 0)) then

                if (ie_acoes_opcionais_w = 'S') then

                    if nr_prescricao_w > 0 then
                        ds_justificativa_w := substr(Wheb_mensagem_pck.get_texto(472090, 'REGRA=' || ds_regra_w || ';ACAO=' || ds_acao_w || ';PRESCRICAO=' || nr_prescricao_w), 1, 255);
                    end if;

                    if nr_sae_w > 0 then
                        ds_justificativa_w := substr(Wheb_mensagem_pck.get_texto(472090, 'REGRA=' || ds_regra_w || ';ACAO=' || ds_acao_w || ';PRESCRICAO=' || nr_sae_w), 1, 255);
                    elsif nr_seq_pend_pac_acao_w > 0 then
                        ds_justificativa_w := substr(Wheb_mensagem_pck.get_texto(472090, 'REGRA=' || ds_regra_w || ';ACAO=' || ds_acao_w || ';PRESCRICAO=' || '--'), 1, 255);
                    end if;
		    
                    if ((nr_seq_pend_regra_w is not null) and (nr_seq_origem_w is not null)) then
                        SQL_executar := SQL_acao_liberada;

                         if (ie_continuar_protocolo_w = 'N') then
                          SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w or c.ie_profissional_medico = ''S'')');
                          SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico = ''S'')');

                        else 
                          SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w and c.ie_profissional_medico <> ''S'')');
                          SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico <> ''S'')');
                        end if;

                        execute immediate SQL_executar
                        using ds_justificativa_w, nr_seq_origem_w, nr_seq_pend_regra_w, cd_pessoa_fisica_w, nr_seq_pend_pac_acao_dest_p, cd_pessoa_fisica_w;

                    elsif ((nr_seq_resultado_w is not null) and (nr_seq_result_item_w is not null)) then -- exames laboratoriais

                        SQL_executar := SQL_acao_liberada_exames;

                         if (ie_continuar_protocolo_w = 'N') then
                          SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w or c.ie_profissional_medico = ''S'')');
                          SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico = ''S'')');

                        else 
                          SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w and c.ie_profissional_medico <> ''S'')');
                          SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico <> ''S'')');
                        end if;

                        execute immediate SQL_executar
                        using ds_justificativa_w, nr_seq_result_item_w, nr_seq_resultado_w, nr_seq_pend_regra_w, cd_pessoa_fisica_w, nr_seq_pend_pac_acao_dest_p, cd_pessoa_fisica_w;

                    end if;
		    
                    OPEN c01;
                    LOOP
                    FETCH c01 INTO
                        nr_seq_equipe_w;		
                    EXIT WHEN c01%NOTFOUND;
                        BEGIN
                            pep_just_pendencias_mentor(nr_seq_pend_pac_acao_dest_p, const_acao_liberada_equipe, null, nr_seq_equipe_w);
                        END;
                    END LOOP;
                    CLOSE c01;
		    
                    commit;

                end if;
            end if;
        end if;

    when const_acao_liberada_equipe then

        if (nvl(nr_seq_pend_pac_acao_dest_p, 0) > 0)  then

            select   nvl(max(nr_prescricao), -1)
            into     nr_prescricao_w
            from     prescr_medica
            where    nr_seq_pend_pac_acao = nr_seq_pend_pac_acao_dest_p;

            if (nr_prescricao_w < 0) then

                select   nvl(max(nr_prescricao), -1),
                        nvl(max(nr_seq_pend_pac_acao), 0)
                into     nr_sae_w,
                        nr_seq_pend_pac_acao_w
                from     pe_prescricao
                where    nr_seq_pend_pac_acao = nr_seq_pend_pac_acao_dest_p
                and      ie_situacao <> 'I';

            end if;

            if ((nr_prescricao_w > 0) or (nr_sae_w > 0) or (nr_seq_pend_pac_acao_w > 0)) then
	    
                select obter_desc_expressao(620512, '') 
                into ds_expressao_w
                from dual;
        
                ds_expressao_w := ds_expressao_w || ' ' || nm_usuario_w;
	    
                if nr_prescricao_w > 0 then
                    ds_justificativa_w := substr(Wheb_mensagem_pck.get_texto(472090, 'REGRA=' || ds_regra_w || ';ACAO=' || ds_acao_w || ', ' || ds_expressao_w || ';PRESCRICAO=' || nr_prescricao_w), 1, 255);
                end if;

                if nr_sae_w > 0 then
                    ds_justificativa_w := substr(Wheb_mensagem_pck.get_texto(472090, 'REGRA=' || ds_regra_w || ';ACAO=' || ds_acao_w || ', ' || ds_expressao_w || ';PRESCRICAO=' || nr_sae_w), 1, 255);
                elsif nr_seq_pend_pac_acao_w > 0 then
                    ds_justificativa_w := substr(Wheb_mensagem_pck.get_texto(472090, 'REGRA=' || ds_regra_w || ';ACAO=' || ds_acao_w || ', ' || ds_expressao_w || ';PRESCRICAO=' || '--'), 1, 255);
                end if;

                if ((nr_seq_pend_regra_w is not null) and (nr_seq_origem_w is not null)) then
                    SQL_executar := SQL_acao_liberada;

                    SQL_executar := replace(SQL_executar, '<criterio1>', 'AND obter_se_medico_equipe(' || nr_seq_equipe_w || ', c.cd_pessoa_fisica) = ''S''');
                    SQL_executar := replace(SQL_executar, '<criterio2>', ' ');
    
                    execute immediate SQL_executar
                    using ds_justificativa_w, nr_seq_origem_w, nr_seq_pend_regra_w, nr_seq_pend_pac_acao_dest_p;

                elsif ((nr_seq_resultado_w is not null) and (nr_seq_result_item_w is not null)) then -- exames laboratoriais

                    SQL_executar := SQL_acao_liberada_exames;

                    SQL_executar := replace(SQL_executar, '<criterio1>', 'AND obter_se_medico_equipe(' || nr_seq_equipe_w || ', c.cd_pessoa_fisica) = ''S''');
                    SQL_executar := replace(SQL_executar, '<criterio2>', ' ');

                    execute immediate SQL_executar
                    using ds_justificativa_w, nr_seq_result_item_w, nr_seq_resultado_w, nr_seq_pend_regra_w, nr_seq_pend_pac_acao_dest_p;
			
                end if;

                commit;

            end if;
        end if;

    when const_acao_justificada then
    
        if (nvl(nr_seq_pend_pac_acao_dest_p, 0) > 0)  then
                
            if (ds_justificativa_w is null) then
                ds_justificativa_w := ds_justificativa_p;
                dt_justificativa_w := sysdate;
            end if;
                
            if ((nr_seq_pend_regra_w is not null) and (nr_seq_origem_w is not null)) then
                
                SQL_executar := SQL_acao_justificada;
                if (ie_continuar_protocolo_w = 'N') then
                  SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w or c.ie_profissional_medico = ''S'')');
                  SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico = ''S'')');

                else 
                  SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w and c.ie_profissional_medico <> ''S'')');
                  SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico <> ''S'')');
                end if;

               execute immediate SQL_executar
                using dt_justificativa_w, dt_justificativa_w, ds_justificativa_w, nr_seq_justificativa_w, 'S', nr_seq_origem_w, nr_seq_pend_regra_w, cd_pessoa_fisica_w, cd_pessoa_fisica_w;
				
			elsif  ((nr_seq_pend_regra_w is not null) and (nr_seq_dest_acao_w is not null) and (nr_seq_dest_acao_w > 0)) then
			
				SQL_executar := SQL_acao_justificada_dest;

                if (ie_continuar_protocolo_w = 'N') then
                  SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w or c.ie_profissional_medico = ''S'')');
                  SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico = ''S'')');

                else 
                  SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w and c.ie_profissional_medico <> ''S'')');
                  SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico <> ''S'')');
                end if;

                execute immediate SQL_executar
                using dt_justificativa_w, dt_justificativa_w, ds_justificativa_w, nr_seq_justificativa_w, nr_seq_dest_acao_w, nr_seq_pend_regra_w, cd_pessoa_fisica_w, cd_pessoa_fisica_w;
		
            elsif ((nr_seq_resultado_w is not null) and (nr_seq_result_item_w is not null)) then -- exames laboratoriais
		
                SQL_executar := SQL_acao_justificada_exames;

                if (ie_continuar_protocolo_w = 'N') then
                  SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w or c.ie_profissional_medico = ''S'')');
                  SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico = ''S'')');

                else 
                  SQL_executar := replace(SQL_executar, '<criterio1>', 'and    (c.cd_pessoa_fisica = :cd_pessoa_fisica_w and c.ie_profissional_medico <> ''S'')');
                  SQL_executar := replace(SQL_executar, '<criterio2>', 'and    (cd_pessoa_fisica  = :cd_pessoa_fisica_w or ie_profissional_medico <> ''S'')');
                end if;

                execute immediate SQL_executar
                using dt_justificativa_w, dt_justificativa_w, ds_justificativa_w, nr_seq_justificativa_w, 'S', nr_seq_result_item_w, nr_seq_resultado_w, nr_seq_pend_regra_w, cd_pessoa_fisica_w, cd_pessoa_fisica_w;

            end if;
			
			if (nr_seq_justificativa_w > 0) then
				select nvl(max(ie_continuar_protocolo),'N')
				into   ie_continuar_protocolo_w
				from   gqa_pend_justificativa
				where  nr_sequencia = nr_seq_justificativa_w;
			end if;
			
			if (ie_continuar_protocolo_w <> 'S') then
				for equipe in c01 loop
					pep_just_pendencias_mentor(nr_seq_pend_pac_acao_dest_p, const_acao_justificada_equipe, null, equipe.nr_seq_equipe, nr_seq_justificativa_w, 'N');
				end loop;
			end if;
	    
            commit;
	    
        end if;
	
    when const_acao_justificada_equipe then
    
        if (nvl(nr_seq_pend_pac_acao_dest_p, 0) > 0)  then

            if not (nr_seq_justificativa_w > 0) and (ds_justificativa_w is null) then
                ds_justificativa_w := ds_justificativa_p;
                dt_justificativa_w := sysdate;
            end if;
            
            select obter_desc_expressao(636628, '') 
            into ds_expressao_w
            from dual;
    
            ds_expressao_w := ds_expressao_w || ' ' || nm_usuario_w;
	
            if ((nr_seq_pend_regra_w is not null) and (nr_seq_origem_w is not null)) then
	    
                SQL_executar := SQL_acao_justificada;

                SQL_executar := replace(SQL_executar, '<criterio1>', 'AND obter_se_medico_equipe(' || nr_seq_equipe_w || ', c.cd_pessoa_fisica) = ''S''');
                SQL_executar := replace(SQL_executar, '<criterio2>', ' ');

                ds_justificativa_w := ds_justificativa_w || ', ' || ds_expressao_w;
                execute immediate SQL_executar
                using dt_justificativa_w, dt_justificativa_w, ds_justificativa_w, nr_seq_justificativa_w, ie_justificativa_origem_p, nr_seq_origem_w, nr_seq_pend_regra_w;
		
            elsif ((nr_seq_resultado_w is not null) and (nr_seq_result_item_w is not null)) then -- exames laboratoriais

                SQL_executar := SQL_acao_justificada_exames;

                SQL_executar := replace(SQL_executar, '<criterio1>', 'AND obter_se_medico_equipe(' || nr_seq_equipe_w || ', c.cd_pessoa_fisica) = ''S''');
                SQL_executar := replace(SQL_executar, '<criterio2>', ' ');

                ds_justificativa_w := ds_justificativa_w || ', ' || ds_expressao_w;

                execute immediate SQL_executar
                using dt_justificativa_w, dt_justificativa_w, ds_justificativa_w, nr_seq_justificativa_w, ie_justificativa_origem_p, nr_seq_result_item_w, nr_seq_resultado_w, nr_seq_pend_regra_w;

            end if;

            commit;
        end if;

    end case;

elsif (ie_profissional_medico_w = 'S' and ie_continuar_protocolo_w = 'S') then

    insert into gqa_pend_pac_acao_dest (CD_PESSOA_FISICA,
                                        DS_JUSTIFICATIVA,
                                        DT_ATUALIZACAO,
                                        DT_ATUALIZACAO_NREC,
                                        DT_CIENCIA,
                                        DT_JUSTIFICATIVA,
                                        IE_JUSTIFICADO_AUTO,
                                        IE_JUSTIFICATIVA_ORIGEM,
                                        IE_PROFISSIONAL_MEDICO,
                                        NM_USUARIO,
                                        NM_USUARIO_NREC,
                                        NR_SEQ_JUSTIFICATIVA,
                                        NR_SEQ_PEND_PAC_ACAO,
                                        NR_SEQUENCIA)
                                values (cd_pessoa_fisica_w,
                                        ds_justificativa_p,
                                        sysdate,
                                        sysdate,
                                        sysdate,
                                        sysdate,
                                        'N',
                                        ie_justificativa_origem_p,
                                        'N',
                                        nm_usuario_w,
                                        nm_usuario_w,
                                        nr_seq_justificativa_p,
                                        nr_seq_pend_pac_acao_dest_p,
                                        gqa_pend_pac_acao_dest_seq.nextval);
    commit;

end if;

end pep_just_pendencias_mentor;
/
