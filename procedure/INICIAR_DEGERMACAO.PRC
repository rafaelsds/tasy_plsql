create or replace
procedure iniciar_degermacao( 	nr_sequencia_p	number,
				nm_usuario_p		Varchar2) is 

begin

if (nr_sequencia_p is not null) then

	update 	san_doacao
	set	dt_inicio_degermacao 		= sysdate,
		nm_usuario_ini_degermacao	= nm_usuario_p,
		dt_atualizacao			= sysdate,
		nm_usuario			= nm_usuario_p
	where	nr_sequencia 			= nr_sequencia_p;
end if;

commit;

end iniciar_degermacao;
/