create or replace
procedure qt_alterar_medico_resp(   cd_medico_resp_p    varchar2,
                                    nr_seq_paciente_p   number,
                                    nm_usuario_p		Varchar2) is 

nr_sequencia_w		varchar2(10);
cd_medico_resp_w	varchar2(10);
				 
begin

if  (nr_seq_paciente_p is not null) and
    (nm_usuario_p is not null) and
    (cd_medico_resp_p is not null)then
    
    select	max(cd_medico_resp)
    into	cd_medico_resp_w
    from	paciente_setor
    where	nr_seq_paciente =  nr_seq_paciente_p;
    
    update  	PACIENTE_SETOR
    set     	nm_usuario      =   nm_usuario_p,
		dt_atualizacao  =   sysdate,
		cd_medico_resp  =  cd_medico_resp_p
    where   	nr_seq_paciente =  nr_seq_paciente_p; 
    
    select	max(nr_sequencia)
    into	nr_sequencia_w
    from	motivo_alteracao_prot_onc
    where	ie_medic_resp_quimio = 'S';
    
    if	(nr_sequencia_w is not null) then
	Gravar_Motivo_Alt_Prot_Onc(	nr_seq_paciente_p,null,null,nr_sequencia_w,wheb_mensagem_pck.get_texto(455963),
					Obter_Pf_Usuario(nm_usuario_p,'C'),'A', nm_usuario_p,'PACIENTE_SETOR', 'CD_MEDICO_RESP',
					cd_medico_resp_w,cd_medico_resp_p,null) ;	
    end if;
    
end if;
commit;

end qt_alterar_medico_resp;
/