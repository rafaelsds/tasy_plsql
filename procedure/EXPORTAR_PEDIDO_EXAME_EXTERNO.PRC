create or replace
procedure exportar_pedido_exame_externo(nr_seq_exame_externo_item_p	number,
					ie_opcao_p			varchar2,
					nm_usuario_p			varchar2) is 

nr_seq_resultado_w		number(10);
nr_seq_pac_prot_externo_w	number(10);
nr_atendimento_w		number(10);
nr_laudo_w			number(10);
cd_pessoa_fisica_w		varchar2(20);

-- ie_opcao_p  (L / P)	

begin


if	(nr_seq_exame_externo_item_p is not null) then
	
	
	select	e.nr_atendimento,
		a.cd_pessoa_fisica
	into	nr_atendimento_w,
		cd_pessoa_fisica_w
	from	pedido_exame_externo_item i,
		pedido_exame_externo e,
		atendimento_paciente a
	where	i.nr_sequencia 	= nr_seq_exame_externo_item_p
	and	i.nr_seq_pedido = e.nr_sequencia
	and	e.nr_atendimento = a.nr_atendimento;
	
	if	(ie_opcao_p = 'L') then --Exame laborat�rio

	
		select	nvl(max(nr_seq_resultado),0)+1
		into	nr_seq_resultado_w
		from	exame_lab_resultado;
		

		insert	into exame_lab_resultado 
			(
			nr_seq_resultado,
			dt_resultado,
			dt_atualizacao,
			nm_usuario,
			nr_prescricao,
			nr_atendimento,
			cd_medico,
			nr_seq_protocolo,
			dt_liberacao,
			cd_pessoa_fisica,
			nr_seq_pedido_exam_ext_item
			)
			select	nr_seq_resultado_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				null,
				e.nr_atendimento,
				e.cd_profissional,
				null,
				null,
				cd_pessoa_fisica_w,
				nr_seq_exame_externo_item_p
			from	pedido_exame_externo_item i,
				pedido_exame_externo e
			where	i.nr_sequencia 	= nr_seq_exame_externo_item_p
			and	i.nr_seq_pedido = e.nr_sequencia;
			


		insert 	into exame_lab_result_item
			(
			nr_seq_resultado,
			nr_sequencia,
			nr_seq_exame,
			dt_atualizacao,
			nm_usuario
			)
			select	nr_seq_resultado_w,
				1,
				i.nr_seq_exame_lab,
				sysdate,
				nm_usuario_p
			from	pedido_exame_externo_item i,
				pedido_exame_externo e
			where	i.nr_sequencia 	= nr_seq_exame_externo_item_p
			and	i.nr_seq_pedido = e.nr_sequencia
			and	i.nr_seq_exame_lab is not null;	
	
	
	elsif	(ie_opcao_p = 'P') then --Exame n�o laborat�rial
	
			
		select	imagem_pac_prot_externo_seq.nextVal
		into	nr_seq_pac_prot_externo_w
		from	dual;
		
		
	
		insert 	into  imagem_pac_prot_externo
			(
			nr_sequencia,
			nr_atendimento,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_exame,
			cd_pessoa_fisica,
			cd_medico,
			nr_seq_pedido_exam_ext_item
			)
			select	nr_seq_pac_prot_externo_w,
				e.nr_atendimento,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				cd_pessoa_fisica_w,
				e.cd_profissional,
				nr_seq_exame_externo_item_p
			from	pedido_exame_externo_item i,
				pedido_exame_externo e
			where	i.nr_sequencia 	= nr_seq_exame_externo_item_p
			and	i.nr_seq_pedido = e.nr_sequencia;
		
		
		select	(nvl(max(nr_laudo),0) + 1)
		into	nr_laudo_w
		from	laudo_paciente
		where	nr_atendimento = nr_atendimento_w;
		
		
		insert 	into laudo_paciente
			(
			nr_sequencia,
			nr_atendimento,
			dt_entrada_unidade,
			nr_laudo,
			nm_usuario,
			dt_atualizacao,
			cd_medico_resp,
			qt_imagem,
			cd_pessoa_fisica,
			ds_titulo_laudo,
			dt_laudo
			)
			select	laudo_paciente_seq.nextVal,
				e.nr_atendimento,
				sysdate,
				nr_laudo_w,
				nm_usuario_p,
				sysdate,
				e.cd_profissional,
				0,
				cd_pessoa_fisica_w,
				Obter_Desc_Proc_Interno(i.nr_proc_interno),
				sysdate
			from	pedido_exame_externo_item i,
				pedido_exame_externo e
			where	i.nr_sequencia 	= nr_seq_exame_externo_item_p
			and	i.nr_seq_pedido = e.nr_sequencia;
		
	end if;
		
	commit;
	
end if;	

end exportar_pedido_exame_externo;
/
