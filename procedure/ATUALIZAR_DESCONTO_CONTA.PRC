Create or Replace
PROCEDURE Atualizar_Desconto_Conta(nr_interno_conta_p     Number) IS

cd_material_w			Number(06,0);
nr_sequencia_w		Number(10,0);
qt_resumo_w			Number(15,4);
nr_prescricao_w		Number(14,0);
cd_cgc_fornec_w		Varchar2(14);
vl_desconto_w			Number(15,2);
pr_desconto_w			Number(15,4);
vl_material_w			Number(15,2);
nr_atendimento_w		Number(15,0);
ie_desc_financ_resumo_w		varchar2(1);
vl_liquido_w			number(15,2);
cd_estabelecimento_w		number(4,0);

CURSOR C01 IS
	select	nr_sequencia,
		cd_material,
		nvl(qt_resumo,0),
		nr_prescricao,
		cd_cgc_fornec,
		nvl(vl_material,0)
	from 	conta_paciente_resumo
	where	nr_interno_conta		= nr_interno_conta_p
	and	cd_material			is not null
	and	cd_cgc_fornec			is not null
	and	nr_atendimento_w		is not null
	Order 	By cd_material;
	
Cursor C02 is
	select	a.vl_liquido,
		a.pr_desc_financ
	from	nota_fiscal_item a
	where	nr_atendimento	= nr_atendimento_w
	and	cd_material	= cd_material_w
	and	exists
		(select 1
		from	nota_fiscal b
		where	a.nr_sequencia	= b.nr_sequencia
		and	nvl(cd_cgc,cd_cgc_emitente) = cd_cgc_fornec_w)
	order by nvl(pr_desc_financ,0) asc;
	
	
BEGIN

select	max(nr_atendimento),
	max(cd_estabelecimento)
into	nr_atendimento_w,
	cd_estabelecimento_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

select 	nvl(max(ie_desc_financ_resumo),'N')
into	ie_desc_financ_resumo_w
from 	parametro_faturamento
where 	cd_estabelecimento = cd_estabelecimento_w;

OPEN C01;
LOOP
FETCH C01 into 	
	nr_sequencia_w,
	cd_material_w,
	qt_resumo_w,
	nr_prescricao_w,
	cd_cgc_fornec_w,
	vl_material_w;
EXIT WHEN C01%NOTFOUND;
	begin
	
	if	(ie_desc_financ_resumo_w = 'N') then
	
		select	nvl(max(pr_desc_financ),0)
		into	pr_desconto_w		
		from	nota_fiscal_item a
		where	nr_atendimento	= nr_atendimento_w
		and	cd_material		= cd_material_w
		and	exists
			(select 1
			from	nota_fiscal b
			where	a.nr_sequencia	= b.nr_sequencia
			and	nvl(cd_cgc,cd_cgc_emitente) = cd_cgc_fornec_w);
	
	elsif	(ie_desc_financ_resumo_w = 'S') then
	
		open C02;
		loop
		fetch C02 into	
			vl_liquido_w,
			pr_desconto_w;
		exit when C02%notfound;
			begin
			vl_liquido_w	:= vl_liquido_w;
			pr_desconto_w	:= pr_desconto_w;
			end;
		end loop;
		close C02;
		
		vl_material_w:= vl_liquido_w;	
		
	end if;

	vl_desconto_w			:= vl_material_w * pr_desconto_w / 100;
	if	(pr_desconto_w > 0) then
		update Conta_paciente_Resumo
		set 	vl_desc_financ	= vl_desconto_w
		where	nr_interno_conta	= nr_interno_conta_p
		  and	nr_sequencia		= nr_sequencia_w;
	end if;
	end;
END LOOP;
CLOSE C01;
END Atualizar_Desconto_Conta;
/