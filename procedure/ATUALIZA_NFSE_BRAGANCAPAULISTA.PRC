create or replace
procedure atualiza_nfse_bragancaPaulista(
			nm_usuario_p		varchar2,
			cd_verificacao_nfse_p	varchar2,
			nr_nfse_p		varchar2,
			cd_estabelecimento_p	number,
			nr_sequencia_p		number) is 
			
			
begin

if	(nr_sequencia_p > 0) then

	update	nota_fiscal
	set	nr_nfe_imp 		= nr_nfse_p,
		cd_verificacao_nfse	= cd_verificacao_nfse_p
	where	nr_sequencia 		= nr_sequencia_p;

	update	nota_fiscal_item
	set	nr_nfe_imp 	= nr_nfse_p
	where	nr_sequencia 	= nr_sequencia_p;

	insert into nota_fiscal_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_nota,
		cd_evento,
		ds_historico)
	values(	nota_fiscal_hist_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_sequencia_p,
		'27',
		'NFS-e: ' || nr_nfse_p);
		
end if;

commit;

end atualiza_nfse_bragancaPaulista;
/