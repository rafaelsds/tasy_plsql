create or replace
procedure excluir_material_autorizado (	nr_atendimento_p		number,
				nr_seq_mat_autor_p	number,
				nr_seq_material_p		number,
				nm_usuario_p		varchar2) is

qt_material_w		number(9,3);
qt_solicitada_w		number(9,3);
qt_autor_conv_w		number(10,0);
nr_seq_autorizacao_w	number(10,0);
ie_atual_qt_solic_autor_w	varchar2(5);
cd_convenio_w		number(5,0);
ds_observacao_w		autorizacao_convenio.ds_observacao%type;

begin

select	nvl(qt_material,0)
into	qt_material_w
from	material_atend_paciente
where	nr_sequencia	= nr_seq_material_p;

begin
select	nvl(qt_solicitada,0),
	a.nr_sequencia,
	a.cd_convenio
into	qt_solicitada_w,
	nr_seq_autorizacao_w,
	cd_convenio_w
from	autorizacao_convenio a,
	material_autorizado b
where	b.nr_sequencia		= nr_seq_mat_autor_p
and	b.nr_sequencia_autor	= a.nr_sequencia
and	a.nr_atendimento		= nr_atendimento_p;
exception
when others then
	qt_solicitada_w := 0;
end;

if	((qt_solicitada_w - qt_material_w) > 0) then

	select	nvl(max(ie_atual_qt_solic_autor),'N')
	into	ie_atual_qt_solic_autor_w
	from	convenio_estabelecimento
	where	cd_convenio		= cd_convenio_w
	and	cd_estabelecimento	= Wheb_usuario_pck.Get_cd_estabelecimento;

	update	material_autorizado a
	set	a.qt_solicitada		= a.qt_solicitada - qt_material_w,
		a.nm_usuario		= nm_usuario_p,
		a.dt_atualizacao		= sysdate
	where	a.nr_sequencia		= nr_seq_mat_autor_p
	and	(((ie_atual_qt_solic_autor_w = 'S') and
		(a.qt_autorizada > a.qt_solicitada))
		or(exists (	select	1
				from 	estagio_autorizacao y,
					autorizacao_convenio x
				where	x.nr_seq_estagio	= y.nr_sequencia
				and	x.nr_sequencia	= a.nr_sequencia_autor
				and	y.ie_interno	= '1'))); --Necessidade

else

	update	material_atend_paciente m
	set	m.nr_seq_mat_autor	= null
	where	m.nr_seq_mat_autor in (	select	a.nr_sequencia
					from material_autorizado a
					where	a.nr_sequencia	= nr_seq_mat_autor_p
					and	exists 	(select	1
							from 	estagio_autorizacao y,
								autorizacao_convenio x
							where	x.nr_seq_estagio	= y.nr_sequencia
							and	x.nr_sequencia	= a.nr_sequencia_autor
							and	y.ie_interno	= '1'));

	delete	from material_autorizado a
	where	a.nr_sequencia	= nr_seq_mat_autor_p
	and	exists 	(select	1
			from 	estagio_autorizacao y,
				autorizacao_convenio x
			where	x.nr_seq_estagio	= y.nr_sequencia
			and	x.nr_sequencia	= a.nr_sequencia_autor
			and	y.ie_interno	= '1'); --Necessidade
end if;

if	(nvl(nr_seq_autorizacao_w,0) <> 0) then
	
	select	count(1)
	into	qt_autor_conv_w
	from	material_autorizado
	where	rownum = 1
	and	nr_sequencia_autor = nr_seq_autorizacao_w;
	
	if	(qt_autor_conv_w = 0) then
		select	count(1)
		into	qt_autor_conv_w
		from	procedimento_autorizado
		where	rownum = 1
		and	nr_sequencia_autor = nr_seq_autorizacao_w;
	end if;

	if	(qt_autor_conv_w = 0) then
		--Cancelado pela exclus�o do item Conta / Execu��o prescri��o
		ds_observacao_w	:= substr(wheb_mensagem_pck.get_texto(305291),1,2000);
		
		update	autorizacao_convenio a
		set	a.nr_seq_estagio = (	select	max(x.nr_sequencia)
						from	estagio_autorizacao x
						where	x.ie_interno = '70'),
			a.dt_atualizacao = sysdate,
			a.ds_observacao	 = ds_observacao_w, 
			a.nm_usuario	 = nm_usuario_p
		where	a.nr_sequencia   = nr_seq_autorizacao_w;
			
	end if;
	
end if;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end excluir_material_autorizado;
/
