create or replace
procedure gerar_encaixe_agexame_sel_js(	nr_seq_agenda_sel_p		number,
					cd_agenda_p			varchar2,
					cd_agenda_usa_p			varchar2,
					dt_agenda_p			date,
					dt_encaixe_p			varchar2,
					ie_transferir_p			varchar2,
					qt_duracao_p			number,
					cd_motivo_p			varchar2,
					ds_motivo_p			varchar2,
					cd_estabelecimento_p		number,
					cd_perfil_p         		number,
					nm_usuario_p        		varchar2,
					nr_seq_encaixe_p 	    out number,
					ds_carteiras_selec_p 	    out varchar2,
					cd_convenio_p			number) is

ie_se_perm_pf_classif_w		varchar2(80);					
dt_encaixe_w			varchar2(255);
dt_referencia_w			varchar2(255);
dt_encaixe_cons_w		varchar2(255);
cd_pessoa_fisica_w		varchar2(10);
nr_seq_encaixe_w		number(10,0);
ie_consiste_med_executor_w	varchar2(255);
ds_carteiras_selec_w		varchar2(255);
ie_atualiza_plano_w		varchar2(255);
ds_texto_w			varchar2(255);
ie_permite_w			varchar2(1) := 'S';
dt_teste_w				date;

begin
dt_encaixe_w		:= dt_encaixe_p || ':00';
dt_encaixe_cons_w	:= '31/12/1899 ' || dt_encaixe_w;
dt_referencia_w		:= to_char(dt_agenda_p,'dd/mm/yyyy') || ' ' || dt_encaixe_w;

if	(nr_seq_agenda_sel_p is not null) then
	begin
	
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	agenda_paciente 
	where	nr_sequencia = nr_seq_agenda_sel_p;
	
	if	(cd_pessoa_fisica_w is not null) then
		begin
		ie_permite_w	:= Consiste_Agenda_Sexo(cd_pessoa_fisica_w, cd_agenda_p);

		if	(ie_permite_w = 'N') then
			Wheb_mensagem_pck.exibir_mensagem_abort(268471);
		end if;
	
		ie_se_perm_pf_classif_w	:= substr(Obter_Se_Perm_PF_Classif(820, cd_agenda_p, cd_pessoa_fisica_w, to_date(dt_referencia_w,'dd/mm/yyyy hh24:mi:ss'), 'DS'),1,80);

		if	(ie_se_perm_pf_classif_w is not null) then	
			begin
			Wheb_mensagem_pck.exibir_mensagem_abort(43226,'IE_CLASSIFICACAO='||ie_se_perm_pf_classif_w);
			end;
		end if;
		end;
	end if;
	
	consistir_encaixe_agenda_exame(cd_agenda_p, nr_seq_agenda_sel_p, trunc(dt_agenda_p), ie_transferir_p, cd_estabelecimento_p, nm_usuario_p, to_date(dt_encaixe_cons_w,'dd/mm/yyyy hh24:mi:ss'), cd_convenio_p);

	gerar_encaixe_agenda_exame_sel(	cd_estabelecimento_p, cd_agenda_usa_p, trunc(dt_agenda_p), dt_encaixe_p, qt_duracao_p, nr_seq_agenda_sel_p,
					ie_transferir_p, cd_motivo_p, ds_motivo_p, nm_usuario_p, nr_seq_encaixe_w);

	obter_Param_Usuario(820, 117, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_consiste_med_executor_w);

	if	(nr_seq_encaixe_w is not null) and
		(ie_consiste_med_executor_w = 'S') then
		begin
		Gera_usu_conv_OPS_Agepac(nr_seq_encaixe_w, cd_estabelecimento_p, ds_carteiras_selec_w);
		
		obter_Param_Usuario(820, 116, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_plano_w);
		
		if	(ds_carteiras_selec_w is null) and
			(ie_atualiza_plano_w = 'S') then
			Gera_plano_OPS_Agepac(nr_seq_encaixe_w);		
		end if;
		end;
	end if;
	end;
end if;

nr_seq_encaixe_p	:= nr_seq_encaixe_w;
ds_carteiras_selec_p	:= ds_carteiras_selec_w;

end gerar_encaixe_agexame_sel_js;
/
