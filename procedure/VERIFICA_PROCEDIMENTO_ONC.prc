create or replace procedure VERIFICA_PROCEDIMENTO_ONC (
          nr_atendimento_p  in number,
          nr_seq_paciente_p  in number) is

  cd_protocolo_w           paciente_setor.cd_protocolo%type;
  nr_seq_medicacao_w       paciente_setor.nr_seq_medicacao%type;
  cd_convenio_w            atend_categoria_convenio.cd_convenio%type;
  nr_seq_proc_w            number;
  nr_agrupamento_proced_w  number;

  cursor cur_procedimentonc is
    select regra_procedimento_onc.cd_procedimento,
           regra_procedimento_onc.ds_dias_aplicacao,
           regra_procedimento_onc.ds_ciclos_aplicacao
      from regra_procedimento_onc
     where ((regra_procedimento_onc.cd_estabelecimento =
           wheb_usuario_pck.get_cd_estabelecimento) or
           (regra_procedimento_onc.cd_estabelecimento is null))
       and nvl(regra_procedimento_onc.cd_protocolo, nvl(cd_protocolo_w, 0)) =
           nvl(cd_protocolo_w, 0)
       and nvl(regra_procedimento_onc.cd_convenio, nvl(cd_convenio_w, 0)) =
           nvl(cd_convenio_w, 0)
       and nvl(regra_procedimento_onc.nr_seq_medicacao,
               nvl(nr_seq_medicacao_w, 0)) = nvl(nr_seq_medicacao_w, 0)
       and NVL(regra_procedimento_onc.ie_situacao,'A') = 'A';
begin

  begin
    select atend_categoria_convenio.cd_convenio
      into cd_convenio_w
      from atend_categoria_convenio
     where atend_categoria_convenio.nr_atendimento = nr_atendimento_p;
  exception
    when others then
      cd_convenio_w := null;
  end;

  begin
    select cd_protocolo,
           nr_seq_medicacao
      into cd_protocolo_w,
           nr_seq_medicacao_w
      from paciente_setor
     where nr_seq_paciente = nr_seq_paciente_p;
  exception
    when others then
      cd_protocolo_w := null;
      nr_seq_medicacao_w := null;
  end;

  for i in cur_procedimentonc loop

    begin
      delete from paciente_protocolo_proc
       where paciente_protocolo_proc.nr_seq_paciente = nr_seq_paciente_p;
    exception
      when others then
        null;
    end;

    begin
      select nvl(max(NR_SEQ_PROCEDIMENTO), 0), nvl(max(nr_agrupamento), 0)
        into nr_seq_proc_w, nr_agrupamento_proced_w
        from PACIENTE_PROTOCOLO_PROC
       where nr_seq_paciente = nr_seq_paciente_p;
    exception
      when others then
        nr_seq_proc_w           := 0;
        nr_agrupamento_proced_w := 0;
    end;

    begin
      insert into PACIENTE_PROTOCOLO_PROC (
              NR_SEQ_PACIENTE,
              NR_SEQ_PROCEDIMENTO,
              CD_PROCEDIMENTO,
              QT_PROCEDIMENTO,
              DS_DIAS_APLICACAO,
              DT_ATUALIZACAO,
              NM_USUARIO,
              NR_AGRUPAMENTO,
              DS_CICLOS_APLICACAO)
      values (nr_seq_paciente_p,
              nr_seq_proc_w + 1,
              i.cd_procedimento,
              1,
              i.ds_dias_aplicacao,
              sysdate,
              wheb_usuario_pck.get_nm_usuario,
              nr_agrupamento_proced_w + 1,
              i.ds_ciclos_aplicacao);
     exception
       when others then
        debug_marlon(sqlerrm);
     end;
  end LOOP;

  COMMIT;

end verifica_procedimento_onc;
/
