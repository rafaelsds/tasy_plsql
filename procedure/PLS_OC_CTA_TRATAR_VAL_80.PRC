create or replace
procedure pls_oc_cta_tratar_val_80(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_selecao_ocor_cta.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Aplicar a valida��o de suspens�o de atendimento da cooperativa
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dados_tb_selecao_w		pls_tipos_ocor_pck.dados_table_selecao_ocor;
ie_gera_ocorrencia_w		varchar2(1);
nr_idx_w			pls_integer;
qt_registro_w			number(1);

-- Cursor das regras
cursor C01 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.ie_valida
	from	pls_oc_cta_val_sus_cop a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;

-- Cursor das contas, busca tamb�m data de inicio e fim da suspens�o para ser validado depois se a conta est� ou n�o no per�odo de suspens�o
cursor C02 (	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_sequencia%type)	is
	select 	b.dt_atendimento,
		b.nr_seq_congenere_seg,
		b.nr_sequencia nr_seq_conta
	from 	pls_conta_ocor_v b
	where 	exists (select	1
			from	pls_selecao_ocor_cta a
			where	a.nr_id_transacao = nr_id_transacao_pc
			and	a.ie_valido = 'S'
			and	a.nr_seq_conta = b.nr_sequencia);

begin
if	(dados_regra_p.nr_sequencia is not null)  then

	pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
	pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
	nr_idx_w:=	0;
	
	for r_C01_w in C01(dados_regra_p.nr_sequencia) loop
		
		if	(r_C01_w.ie_valida = 'S')	then

			for r_C02_w in C02(nr_id_transacao_p) loop

				ie_gera_ocorrencia_w := 'N';
				
				select 	max(qt_registro)
				into	qt_registro_w
				from 	(select 1 qt_registro
					from   pls_segurado_suspensao
					where  nr_seq_congenere = r_C02_w.nr_seq_congenere_seg
					and    r_C02_w.dt_atendimento between dt_inicio_suspensao and dt_fim_suspensao
					union all
					select 1 qt_registro
					from   pls_segurado_suspensao
					where  nr_seq_congenere = r_C02_w.nr_seq_congenere_seg
					and    dt_inicio_suspensao <= r_C02_w.dt_atendimento
					and    dt_fim_suspensao is null);
				
				if	( qt_registro_w > 0) then
					ie_gera_ocorrencia_w := 'S';
				end if;
				
				if	(ie_gera_ocorrencia_w = 'S') then
					--Passa nr_seq_conta ao inv�z do nr_seq_selecao, pois ser� feito valida��o a n�vel de conta
					dados_tb_selecao_w.nr_seq_selecao(nr_idx_w) := r_C02_w.nr_seq_conta;
					dados_tb_selecao_w.ds_observacao(nr_idx_w) := 'Operadora est� em per�odo de suspens�o de atendimento. ';
					dados_tb_selecao_w.ie_valido(nr_idx_w) := 'S';					
					
					if	(nr_idx_w >= pls_util_cta_pck.qt_registro_transacao_w) then
						pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
												'SEQ_CONTA', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, 
												nm_usuario_p, nr_id_transacao_p);
						
						pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
						nr_idx_w := 0;
					else
						nr_idx_w := nr_idx_w + 1;
					end if;
				end if;
			end loop; --C02
		end if;
	end loop; --C01

	pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
							'SEQ_CONTA', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, 
							nm_usuario_p, nr_id_transacao_p);
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;
end pls_oc_cta_tratar_val_80;
/