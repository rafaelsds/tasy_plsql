create or replace
procedure bsc_calcular_ctb(
			nr_seq_indicador_p		number,
			nr_seq_ind_inf_p		number,
			nr_seq_regra_calc_p	number,
			qt_real_p	out		number) is 


cd_estabelecimento_w	number(4);			
cd_ano_w		number(4);
cd_periodo_w		number(2);
cd_empresa_w		number(4);
dt_referencia_w		date;
ie_origem_valor_w		varchar2(15);
ds_origem_w		varchar2(2000);
nr_seq_mes_ref_w		number(10);
vl_referencia_w		number(17,4);


begin

select	cd_ano,
	cd_periodo,
	cd_empresa,
	cd_estabelecimento
into	cd_ano_w,
	cd_periodo_w,
	cd_empresa_w,
	cd_estabelecimento_w
from	bsc_ind_inf
where	nr_sequencia	= nr_seq_ind_inf_p;

dt_referencia_w	:= to_date('01/' || cd_periodo_w || '/' || cd_ano_w,'dd/mm/yyyy');

select	nvl(max(nr_sequencia), 0)
into	nr_seq_mes_ref_w
from	ctb_mes_ref
where	cd_empresa	= cd_empresa_w
and	trunc(dt_referencia, 'mm') = trunc(dt_referencia_w, 'mm');


begin
select	ie_origem_ctb,
	ds_origem
into	ie_origem_valor_w,
	ds_origem_w
from	bsc_regra_calc_ind
where	nr_sequencia		= nr_seq_regra_calc_p;
exception
	when no_data_found then
	Wheb_mensagem_pck.exibir_mensagem_abort(280310); -- Falta informar as origens dos valores da contabilidade para as regras
end;



if	(ie_origem_valor_w = 'I') then
	vl_referencia_w	:= ds_origem_w;
elsif	(ie_origem_valor_w in ('S','SC','M','MA','MD','MC','MAD','MAC','MASE','MCSE')) then
	select	ctb_obter_valor_conta(nr_seq_mes_ref_w, ds_origem_w, cd_estabelecimento_w, null, ie_origem_valor_w, null)
	into	vl_referencia_w
	from	dual;
elsif	(ie_origem_valor_w in ('OM','OA')) then
	select	ctb_obter_valor_conta_orc(nr_seq_mes_ref_w, ds_origem_w, cd_estabelecimento_w, null, ie_origem_valor_w)
	into	vl_referencia_w
	from	dual;
elsif	(ie_origem_valor_w = 'SV') then
	vl_referencia_w	:= null;
end if;

qt_real_p	:= nvl(vl_referencia_w,0);

end bsc_calcular_ctb;
/