create or replace
procedure pls_gravar_carga_preco
			(	cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 

dt_inicio_vigencia_w		Date;
nm_tabela_w			Varchar2(255);
dt_fim_vigencia_w		Date;
nr_protocolo_w			Varchar2(20);
cd_codigo_scpa_w		Varchar2(20);
nr_seq_plano_w			Number(10);
cd_codigo_ant_w			Varchar2(20);
ie_tabela_base_w		Varchar2(1);
ie_tabela_contrato_w		Varchar2(1);
ie_proposta_adesao_w		Varchar2(1);
ie_simulacao_preco_w		Varchar2(1);
nr_seq_tabela_w			Number(10);
qt_idade_inicial_w		Number(5);
qt_idade_final_w		Number(5);
vl_preco_inicial_w		Number(15,2);
vl_preco_atual_w		Number(15,2);
tx_acrescimo_w			Number(7,4);
vl_preco_nao_subsidiado_w	Number(15,2);
vl_preco_nao_subsid_atual_w	Number(15,2);
nr_seq_preco_w			Number(15,2);

Cursor C01 is
	select	dt_inicio_vigencia,
		nm_tabela,
		dt_fim_vigencia,
		nr_protocolo,
		cd_codigo_scpa,
		nr_seq_plano,
		cd_codigo_ant,
		nvl(ie_tabela_base,'N'),
		ie_tabela_contrato,
		ie_proposta_adesao,
		ie_simulacao_preco,
		nr_seq_tabela,
		qt_idade_inicial,
		qt_idade_final,
		vl_preco_inicial,
		vl_preco_atual,
		tx_acrescimo,
		vl_preco_nao_subsidiado,
		vl_preco_nao_subsid_atual
	from	w_pls_carga_preco;

begin

open C01;
loop
fetch C01 into	
	dt_inicio_vigencia_w,
	nm_tabela_w,
	dt_fim_vigencia_w,
	nr_protocolo_w,
	cd_codigo_scpa_w,
	nr_seq_plano_w,
	cd_codigo_ant_w,
	ie_tabela_base_w,
	ie_tabela_contrato_w,
	ie_proposta_adesao_w,
	ie_simulacao_preco_w,
	nr_seq_tabela_w,
	qt_idade_inicial_w,
	qt_idade_final_w,
	vl_preco_inicial_w,
	vl_preco_atual_w,
	tx_acrescimo_w,
	vl_preco_nao_subsidiado_w,
	vl_preco_nao_subsid_atual_w;
exit when C01%notfound;
	begin
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_preco_w
	from	pls_tabela_preco 
	where	cd_codigo_ant	= cd_codigo_ant_w;
	
	if	(nr_seq_preco_w	= 0) then
		select	pls_tabela_preco_seq.nextval
		into	nr_seq_preco_w
		from	dual;
		
		insert into pls_tabela_preco
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
			dt_inicio_vigencia, dt_fim_vigencia, nr_seq_plano,
			dt_liberacao, cd_codigo_ant, ie_tabela_base,
			nr_contrato, ie_tabela_contrato, nr_segurado,
			nr_seq_tabela_origem, ie_proposta_adesao, ie_simulacao_preco)
		values(	nr_seq_preco_w, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, nm_tabela_w,
			dt_inicio_vigencia_w, dt_fim_vigencia_w, nr_seq_plano_w,
			null, cd_codigo_ant_w, ie_tabela_base_w,
			null, ie_tabela_contrato_w, null,
			null, ie_proposta_adesao_w, ie_simulacao_preco_w);
	end if;
	if	(nr_seq_plano_w	> 0) then
		insert into pls_plano_preco 
			(nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_tabela,
			qt_idade_inicial, qt_idade_final, vl_preco_inicial,
			vl_preco_atual, tx_acrescimo, vl_preco_nao_subsidiado,
			vl_preco_nao_subsid_atual)
		values(	pls_plano_preco_seq.nextval, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, nr_seq_preco_w,
			qt_idade_inicial_w, qt_idade_final_w, vl_preco_inicial_w,
			vl_preco_atual_w, tx_acrescimo_w, vl_preco_nao_subsidiado_w,
			vl_preco_nao_subsid_atual_w);
	end if;
	end;
end loop;
close C01;

commit;

end pls_gravar_carga_preco;
/
