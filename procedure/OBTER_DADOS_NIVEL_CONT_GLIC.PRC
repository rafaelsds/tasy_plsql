create or replace
procedure obter_dados_nivel_cont_glic(	nr_seq_horario_p	number,
					nr_seq_glicemia_p	out number,
					nr_seq_prot_glic_p	out number) is

nr_prescricao_w		number(10,0)	:= 0;
nr_seq_glicemia_w	number(10,0);
nr_seq_prot_glic_w	number(10,0);
						
begin

if	(nr_seq_horario_p > 0) then
	begin
	
	select	a.nr_prescricao
	into	nr_prescricao_w
	from	prescr_mat_hor a
	where	a.nr_sequencia	= nr_seq_horario_p;
	
	if	(nr_prescricao_w is not null) then
		begin
		
		select	nvl(max(a.nr_sequencia),0)
		into	nr_seq_glicemia_w
		from	atend_glicemia a
		where	a.nr_prescricao	= nr_prescricao_w;
		
		select	nvl(max(a.nr_seq_prot_glic),0)
		into	nr_seq_prot_glic_w
		from	atend_glicemia a
		where	a.nr_prescricao = nr_prescricao_w;
		
		end;
	end if;
	end;
end if;

nr_seq_glicemia_p	:= nr_seq_glicemia_w;
nr_seq_prot_glic_p	:= nr_seq_prot_glic_w;

end obter_dados_nivel_cont_glic;
/