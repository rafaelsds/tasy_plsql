create or replace
procedure Sus_definir_apac_conta_unif(
			nr_seq_apac_p		Number,
			nr_interno_conta_p	Number,
			nm_usuario_p		Varchar2) is 
			
dt_competencia_w		sus_apac_unif.dt_competencia%type;
dt_entrada_w			atendimento_paciente.dt_entrada%type;
ie_gera_conta_mes_w		varchar2(15) := 'N';
dt_periodo_inicial_w		conta_paciente.dt_periodo_inicial%type;
dt_periodo_final_w		conta_paciente.dt_periodo_final%type;
cd_estab_usuario_w		estabelecimento.cd_estabelecimento%type;

begin

begin
cd_estab_usuario_w := nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
exception
when others then
	cd_estab_usuario_w := 0;	
end;

ie_gera_conta_mes_w	:= nvl(obter_valor_param_usuario(1124,135,obter_perfil_ativo,nm_usuario_p,cd_estab_usuario_w),'N');

update	sus_apac_unif
set	nr_interno_conta	= nr_interno_conta_p,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_apac_p;

begin
select	a.dt_competencia,
	c.dt_entrada
into	dt_competencia_w,
	dt_entrada_w
from	sus_apac_unif a,
	conta_paciente b,
	atendimento_paciente c
where	a.nr_sequencia	= nr_seq_apac_p
and	a.nr_interno_conta = b.nr_interno_conta
and	b.nr_atendimento = c.nr_atendimento;
exception
when others then
	dt_competencia_w := null;
	dt_entrada_w := null;
end;

if	(dt_competencia_w is not null) and
	(ie_gera_conta_mes_w = 'N') then
	begin
	
	begin
	update	conta_paciente
	set 	dt_mesano_referencia 	= dt_competencia_w
	where 	nr_interno_conta	= nr_interno_conta_p;
	exception
	when others then
		dt_competencia_w := null;
	end;
	
	end;
elsif	(dt_competencia_w is not null) and
	(ie_gera_conta_mes_w = 'S') then
	begin
	
	begin
	select	c.dt_periodo_inicial,
		c.dt_periodo_final
	into	dt_periodo_inicial_w,
		dt_periodo_final_w
	from	conta_paciente c
	where	c.nr_interno_conta	= nr_interno_conta_p
	and	establishment_timezone_utils.startOfMonth(c.dt_periodo_final) <> establishment_timezone_utils.startOfMonth(dt_competencia_w);	
	exception
	when others then
		dt_periodo_inicial_w	:= null;
		dt_periodo_final_w	:= null;
	end;
		
	if	((dt_periodo_inicial_w is not null) or
		(dt_periodo_final_w is not null)) and
		(dt_competencia_w is not null) then
		begin
		
		if (establishment_timezone_utils.startOfMonth(dt_periodo_inicial_w) <> establishment_timezone_utils.startOfMonth(dt_competencia_w)) then
			begin
			if	(dt_competencia_w < dt_entrada_w) then
				dt_periodo_inicial_w := dt_entrada_w;				
			else
				dt_periodo_inicial_w := (establishment_timezone_utils.startofday(dt_competencia_w) + 1/86400);
			end if;
			
			begin
			update	conta_paciente
			set 	dt_mesano_referencia 	= dt_competencia_w,
				dt_periodo_inicial	= dt_periodo_inicial_w,
				dt_periodo_final	= (last_day(establishment_timezone_utils.startofday(dt_periodo_inicial_w))+0.99999)
			where 	nr_interno_conta	= nr_interno_conta_p;
			exception
			when others then
				dt_competencia_w := null;
			end;
		
			end;
		else			
			begin
			
			update	conta_paciente
			set 	dt_mesano_referencia 	= dt_competencia_w,
				dt_periodo_final	= (last_day(establishment_timezone_utils.startofday(dt_competencia_w))+0.99999)
			where 	nr_interno_conta	= nr_interno_conta_p;
			exception
			when others then
				dt_competencia_w := null;
			end;		
		end if;		
		
		end;
	elsif	(dt_competencia_w is not null) then
		begin
		
		begin
		update	conta_paciente
		set 	dt_mesano_referencia 	= dt_competencia_w
		where 	nr_interno_conta	= nr_interno_conta_p;
		exception
		when others then
			dt_competencia_w := null;
		end;
		
		end;
	end if;
	
	end;
end if;

commit;

end Sus_definir_apac_conta_unif;
/
