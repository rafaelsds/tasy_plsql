create or replace
procedure Susp_Medic_Atend_ciclo_onc(	nr_seq_atendimento_p	number,
					nr_seq_material_p	number,
					nm_usuario_p		Varchar2) is 


nr_seq_atendimento_w		number(10);
nr_seq_paciente_w		number(10);
nr_seq_material_w		number(10);
cd_material_w			number(10);
nr_seq_interno_w		number(10);

cursor c01 is
	select	a.nr_seq_interno
	from	paciente_atend_medic a,
		paciente_atendimento b
	where	b.nr_seq_paciente	= nr_seq_paciente_w
	and	b.nr_seq_atendimento = a.nr_seq_atendimento
	and	b.nr_seq_atendimento	>= nr_seq_atendimento_p
	and	a.cd_material = cd_material_w
	and	a.nr_seq_material = nr_seq_material_p
	and	b.dt_liberacao_reg is null
	and	a.dt_cancelamento is null
	and	b.dt_cancelamento is null
	and	Obter_Se_Dia_Ciclo_Autorizado(b.nr_seq_paciente,b.nr_ciclo) = 'N';
					
begin

select	max(nr_seq_paciente)
into	nr_seq_paciente_w
from	paciente_atendimento
where	nr_seq_atendimento	= nr_seq_atendimento_p;

select	max(cd_material)
into	cd_material_w
from	paciente_atend_medic
where	nr_seq_atendimento	= nr_seq_atendimento_p
and	nr_seq_material		= NR_SEQ_MATERIAL_p;


open C01;
loop
fetch C01 into	
	nr_seq_interno_w;
exit when C01%notfound;
	begin	
	suspender_item_oncologia(nr_seq_interno_w,nm_usuario_p);
	end;
end loop;
close C01;	
					
commit;

end Susp_Medic_Atend_ciclo_onc;
/