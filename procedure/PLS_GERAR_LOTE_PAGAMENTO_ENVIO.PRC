create or replace
procedure pls_gerar_lote_pagamento_envio
			(	nr_seq_lote_p			number,
				nr_seq_pagamento_p		number,
				ds_destino_p			varchar2,
				nm_usuario_p			varchar2) is 

begin
insert into pls_lote_pagamento_envio
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_lote,
		nr_seq_pagamento,
		dt_envio,
		ds_destino)
	values	(pls_lote_pagamento_envio_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_lote_p,
		nr_seq_pagamento_p,
		sysdate,
		substr(ds_destino_p,1,255));

commit;

end pls_gerar_lote_pagamento_envio;
/