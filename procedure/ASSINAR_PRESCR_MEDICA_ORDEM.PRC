create or replace
procedure assinar_prescr_medica_ordem(
		nr_seq_assinatura_p	number,
		nr_sequencia_p		number,
		nm_usuario_p		varchar2) is
begin
if	(nr_seq_assinatura_p is not null) and
	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	update	prescr_medica_ordem
	set	nr_seq_assinatura	= nr_seq_assinatura_p
	where	nr_sequencia		= nr_sequencia_p;
	end;
end if;
commit;
end assinar_prescr_medica_ordem;
/