create or replace procedure definir_dt_fim_alergia(
		  				nr_seq_pac_alergia_p	number,
						dt_fim_p				date,
						ds_observacao_p			varchar2) is

ds_observacao_w		varchar2(8000);
ds_observacao_ww	varchar2(4000);

begin

select	max(ds_observacao)
into    ds_observacao_w
from	  paciente_alergia
where   nr_sequencia 	= nr_seq_pac_alergia_p;

if (ds_observacao_w is not null) then
  ds_observacao_w := ds_observacao_w || chr(13) || chr(10);
end if;

ds_observacao_ww := substr(ds_observacao_w || ds_observacao_p, 1, 4000);

update	paciente_alergia
set	dt_fim		= dt_fim_p,
	ds_observacao	= ds_observacao_ww
where	nr_sequencia	= nr_seq_pac_alergia_p;

commit;

end definir_dt_fim_alergia;
/