create or replace
procedure reg_atualiza_transf_ext(	nr_seq_reg_p		number,
					ie_tipo_leito_ext_p 	varchar,
					ie_esp_leito_ext_p 	varchar,
					ie_motivo_transf_p 	varchar,
					ds_motivo_transf_p 	varchar 
					) is 
begin

update	regulacao_atendimento
set	dt_atualizacao = sysdate,
	ie_tipo_leito_ext = ie_tipo_leito_ext_p,
	ie_esp_leito_ext = ie_esp_leito_ext_p,
	ie_motivo_transf = ie_motivo_transf_p,
	ds_motivo_transf = ds_motivo_transf_p
where	nr_sequencia = nr_seq_reg_p;

commit;

end reg_atualiza_transf_ext;
/
