create or replace PROCEDURE UPDATE_TASY_EUDAMED(version_number_p varchar2) is

actual_version_w varchar2(5);
actual_sequence_number_w number(10);
update_the_table_w boolean := false;
new_version_w varchar2(10);

ds_title_w        varchar2(500);
ds_mail_destino_w varchar2(2000);
ds_mensagem_w     varchar2(2000);
ds_mail_orig      CONSTANT varchar2(50) := 'support.informatics@philips.com';

BEGIN
    select max(nr_sequencia)
    into actual_sequence_number_w
    from tasy_eudamed;
    
    select nvl(cd_prefix_version, '0')
    into actual_version_w
    from tasy_eudamed
    where nr_sequencia = actual_sequence_number_w;
    
    new_version_w := REGEXP_SUBSTR(version_number_p, '.+\.([^\.]+)\.');
    
    if(version_number_p is not null and actual_version_w <> '0') then
        if (somente_numero(REGEXP_SUBSTR(version_number_p, '[^\.]+')) > somente_numero(REGEXP_SUBSTR(actual_version_w, '[^\.]+'))) then
            update_the_table_w := true;
        else
            if (somente_numero(REGEXP_SUBSTR(version_number_p, '[^\.]+')) = somente_numero(REGEXP_SUBSTR(actual_version_w, '[^\.]+'))) then
                if (somente_numero(new_version_w) > somente_numero(actual_version_w)) then
                    update_the_table_w := true;
                end if;
            end if;
        end if;
    end if;
    
    if (update_the_table_w) then
        update tasy_eudamed x
        set x.ie_situacao = 'I',
            x.dt_inativacao = sysdate,
            x.nm_usuario_inativacao = 'jenkins'
        where nr_sequencia = actual_sequence_number_w;

        insert into tasy_eudamed (nr_sequencia,
                                  ds_commercial,
                                  dt_atualizacao,
                                  nm_usuario,
                                  dt_atualizacao_nrec,
                                  nm_usuario_nrec,
                                  cd_prefix_version,
                                  cd_trade_item_id,
                                  nr_catalog,
                                  cd_gtin,
                                  ie_situacao,
                                  dt_inativacao,
                                  nm_usuario_inativacao)
                    values       (tasy_eudamed_seq.nextval,
                                 ('Tasy EMR ' || substr(new_version_w, 1, (length(new_version_w) - 1))),
                                 sysdate,
                                 'jenkins',
                                 sysdate,
                                 'jenkins',
                                 substr(new_version_w, 1, (length(new_version_w) - 1)),
                                 837002,
                                 837002,
                                 '00884838093287',
                                 'A',
                                 null,
                                 null);

        commit;
        
        ds_title_w := Wheb_Mensagem_Pck.Get_Texto(1127707);
        ds_mail_destino_w := 'lucassandrorotermel.franco@philips.com;Johnny.Fusinato@philips.com;rafael.correa@philips.com;Joao.Weiss@philips.com';
                                        
        ds_mensagem_w := Wheb_Mensagem_Pck.Get_Texto(1127708);
        ds_mensagem_w := REPLACE(ds_mensagem_w, '#@CD_VERSAO#@', version_number_p);

        enviar_email(ds_title_w, ds_mensagem_w, ds_mail_orig, ds_mail_destino_w, 'Tasy', null);
    end if;                     
END UPDATE_TASY_EUDAMED;
/