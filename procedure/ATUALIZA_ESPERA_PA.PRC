create or replace
procedure atualiza_espera_pa(
			ie_clinica_p		number,
			qt_tempo_p		number,
			nm_usuario_p		varchar2) is 

begin

delete 	from tempo_adic_espera_pa
where	ie_clinica = ie_clinica_p;

commit;


insert into	tempo_adic_espera_pa 
		(nr_sequencia,
		ie_clinica,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		qt_tempo)
		values
		(tempo_adic_espera_pa_seq.nextval,
		ie_clinica_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		qt_tempo_p);


commit;

end atualiza_espera_PA;
/