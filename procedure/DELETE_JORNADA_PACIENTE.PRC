CREATE OR REPLACE PROCEDURE delete_jornada_paciente(cd_pessoa_fisica_p w_jornada_pac.cd_pessoa_fisica%TYPE,
                                                    nm_usuario_p       w_jornada_pac.nm_usuario%TYPE) IS
BEGIN
    DELETE FROM w_jornada_pac
     WHERE cd_pessoa_fisica = cd_pessoa_fisica_p
       AND nm_usuario = nm_usuario_p;
    COMMIT;
END delete_jornada_paciente;
/
