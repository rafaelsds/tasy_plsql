create or replace
procedure pls_gerar_lote_ret_trib(	nr_seq_lote_p		number,
					ie_acao_p		varchar2,
					nm_usuario_p		varchar2) is

cd_pessoa_fisica_w		varchar2(10);
vl_liquido_w			number(15,2);
dt_mes_referencia_w		date;

Cursor C01 is
	select	e.cd_pessoa_fisica,
		sum(b.vl_liquido)
	from	pls_prestador			e,
		pls_lote_pagamento		d,
		pls_pagamento_prestador		c,
		pls_pag_prest_vencimento	b,
		pls_pag_prest_venc_trib		a,
		tributo				z
	where	a.nr_seq_vencimento	= b.nr_sequencia
	and	b.nr_seq_pag_prestador	= c.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and	c.nr_seq_prestador	= e.nr_sequencia
	and	z.cd_tributo		= a.cd_tributo
	and	z.ie_situacao		= 'A'
	and	z.ie_tipo_tributo in ('INSS','IR','IRPF')
	and	e.cd_pessoa_fisica is not null
	and	d.dt_geracao_titulos is not null
	and	(((z.ie_venc_pls_pag_prod is not null) and (decode(ie_venc_pls_pag_prod,'V',b.dt_vencimento,d.dt_mes_competencia) between trunc(dt_mes_referencia_w,'month') and fim_dia(last_day(dt_mes_referencia_w)))) or
		decode(z.ie_vencimento,'V',b.dt_vencimento,'C',b.dt_vencimento,d.dt_mes_competencia) between trunc(dt_mes_referencia_w,'month') and fim_dia(last_day(dt_mes_referencia_w)))
	group by
		e.cd_pessoa_fisica;

begin

if	(nr_seq_lote_p is not null) then
	select	a.dt_mes_referencia
	into	dt_mes_referencia_w
	from	pls_lote_retencao_trib a
	where	a.nr_sequencia	= nr_seq_lote_p;
	
	if	(ie_acao_p = 'G') then
		open C01;
		loop
		fetch C01 into	
			cd_pessoa_fisica_w,
			vl_liquido_w;
		exit when C01%notfound;
			begin
			insert into pls_lote_ret_trib_prest
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_lote,
				nr_seq_prestador,
				cd_pessoa_fisica)
			values	(pls_lote_ret_trib_prest_seq.nextval,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_lote_p,
				null,
				cd_pessoa_fisica_w);
			end;
		end loop;
		close C01;
	else
		update	pls_pag_prest_venc_trib a
		set	nr_seq_lote_ret_trib_valor 	= null,
			nm_usuario			= nm_usuario_p,
			dt_atualizacao			= sysdate
		where	exists 	(select	1
				from	pls_lote_ret_trib_valor x,
					pls_lote_ret_trib_prest y
				where	y.nr_sequencia 	= x.nr_seq_trib_prest
				and	x.nr_sequencia 	= a.nr_seq_lote_ret_trib_valor
				and	y.nr_seq_lote 	= nr_seq_lote_p);
		
		delete from pls_lote_ret_trib_val_dif a
		where	exists (select	1
				from	pls_lote_ret_trib_prest x
				where	x.nr_seq_lote	= nr_seq_lote_p
				and	x.nr_sequencia	= a.nr_seq_trib_prest);
		
		delete from pls_lote_ret_trib_valor a
		where	exists (select	1
				from	pls_lote_ret_trib_prest x
				where	x.nr_seq_lote	= nr_seq_lote_p
				and	x.nr_sequencia	= a.nr_seq_trib_prest);
				
		delete from pls_lote_ret_trib_prest
		where nr_seq_lote	= nr_seq_lote_p;
	end if;	
		
	update	pls_lote_retencao_trib
	set	dt_geracao	= decode(ie_acao_p,'G',sysdate,null)
	where	nr_sequencia	= nr_seq_lote_p;
	
	commit;
end if;

end pls_gerar_lote_ret_trib;
/