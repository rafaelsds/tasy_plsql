create or replace
procedure pat_atualizar_transf_bem(
		cd_estabelecimento_p	number,
		dt_mes_referencia_p	date,
		nm_usuario_p		varchar2) is


vl_deprec_acum_w		pat_valor_bem.vl_deprec_acum%type;
vl_base_deprec_w		pat_valor_bem.vl_base_deprec%type;
vl_deprec_mes_w			pat_valor_bem.vl_deprec_mes%type;
vl_residual_w			pat_bem.vl_residual%type;
dt_inicial_w			date;
dt_final_w			date;
vl_rateio_deprec_w		number(22,4);
nr_seq_bem_w			pat_bem.nr_sequencia%type;

cursor c01 is
	select	a.nr_sequencia,
		a.nr_seq_bem,
		a.nr_seq_local_ant,
		a.nr_seq_local_atual,
		a.cd_estab_origem,
		a.cd_estab_destino,
		a.dt_historico
	from	pat_tipo_historico b,
		pat_historico_bem a
	where	a.nr_seq_tipo		= b.nr_sequencia
	and	b.ie_transferencia	= 'S'
	and	a.cd_estab_origem	<> a.cd_estab_destino
	and	a.cd_estab_destino	= cd_estabelecimento_p
	and	a.dt_historico between dt_inicial_w and dt_final_w
	order by a.nr_seq_bem,a.dt_historico;

c01_w		c01%rowtype;

begin

dt_inicial_w	:= trunc(dt_mes_referencia_p,'mm');
dt_final_w	:= fim_mes(dt_mes_referencia_p);
/* Pegar todos os bens que sofreram transfer�ncia entre estabelecimentos no m�s de refer�ncia */
open c01;
loop
fetch c01 into
	c01_w;
exit when c01%notfound;
	begin
	nr_seq_bem_w := c01_w.nr_seq_bem;
	/* Pegar o valor residual com base na regra e no �ltimo dia �til do m�s, caso a regra for alterada dentro do m�s, deve considerar o novo valor */
	begin
		/* Pegar o valor de base de depreci��o, que � o valor Original - valor residual - baixas realizadas */
		select	vl_base_deprec,
			vl_deprec_acum,
			vl_deprec_mes,
			vl_residual
		into	vl_base_deprec_w,
			vl_deprec_acum_w,
			vl_deprec_mes_w,
			vl_residual_w
		from	pat_valor_bem
		where	nr_seq_bem	= c01_w.nr_seq_bem
		and	dt_valor	= trunc(dt_mes_referencia_p,'dd')
		and	rownum = 1;

		select	nvl(sum(vl_rateio_deprec),0)
		into	vl_rateio_deprec_w
		from	pat_historico_bem b,
			pat_valor_bem_rateio a
		where	a.nr_seq_hist_dest	= b.nr_sequencia
		and	b.nr_seq_bem		= c01_w.nr_seq_bem
		and	b.dt_historico		< c01_w.dt_historico
		and	trunc(b.dt_historico,'mm') = trunc(dt_mes_referencia_p,'mm');

		vl_base_deprec_w := nvl(vl_base_deprec_w,0);
		vl_deprec_acum_w := nvl(vl_deprec_acum_w,0) - vl_deprec_mes_w + vl_rateio_deprec_w;
	exception
	when NO_DATA_FOUND then
		vl_base_deprec_w := 0;
		vl_deprec_acum_w := 0;
	end;
	vl_residual_w := philips_patrimonio_pck.obter_vl_residual(c01_w.nr_seq_bem,fim_mes(dt_mes_referencia_p),vl_residual_w);
	/* Adicionar o valor residual ao base de deprecia��o para voltar o valor do bem ao valor original ou ao valor original descontando as baixas */
	vl_base_deprec_w := vl_base_deprec_w + vl_residual_w;

	/* Atualizar os campos utilizados pela rotina de contabiliza��o de patrim�nio. CONTABILIZA_PATRIMONIO */
	update	pat_historico_bem
	set	vl_transf_deprec_mes	= vl_base_deprec_w,
		vl_transf_deprec_acum	= vl_deprec_acum_w
	where	nr_sequencia		= c01_w.nr_sequencia;

	end;
end loop;
close c01;

commit;

end pat_atualizar_transf_bem;
/
