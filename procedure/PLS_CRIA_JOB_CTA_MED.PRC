create or replace
procedure pls_cria_job_cta_med is

jobno 			number;
qt_reg_w 		pls_integer;
ds_comando_job_w	varchar2(2000);

begin

-- Essa procedure tem como objetivo rodar os jobs referentes a manutencao de tabelas de log de contas medicas

select	count(1)
into	qt_reg_w
from 	pls_outorgante;

-- executa somente se for uma operadora de plano de saude
if	(qt_reg_w > 0) then
	
	-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
	pls_obter_qt_job_ativa('PLS_DELETE_PLSPRCO_CTA', qt_reg_w);
	
	-- log de alteracoes nas contas medicas
	if	(qt_reg_w = 0) then
		begin
			DBMS_JOB.SUBMIT(JOBNO,
					'PLS_DELETE_PLSPRCO_CTA(''Tasy'');',
					TRUNC(SYSDATE + 1) + ((1/24) * 1),
					'TRUNC(SYSDATE + 1)');
			commit;
		exception
		when others then
			null;
		end;
	end if;
	
	-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
	pls_obter_qt_job_ativa('PLS_GERAR_AUTOGERADO', qt_reg_w);
	
	-- log de alteracoes nas contas medicas
	if	(qt_reg_w = 0) then
		begin
			  DBMS_JOB.SUBMIT(JOBNO
					 ,'PLS_GERAR_AUTOGERADO;'
					 , add_months(trunc(sysdate, 'MONTH'),1)
					 ,'add_months(trunc(sysdate, ''MONTH''),1)' );
			commit;
		exception
		when others then
			null;
		end;
	end if;
		
	-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
	pls_obter_qt_job_ativa('PLS_DELETE_LOG_OCOR', qt_reg_w);
	
	if	(qt_reg_w = 0) then
		begin
			DBMS_JOB.SUBMIT(JOBNO,
					'PLS_DELETE_LOG_OCOR;',
					TRUNC(SYSDATE + 1) + ((1/24) * 1.5),
					'TRUNC(SYSDATE + 5) + ((1/24) * 2)');
			commit;
		exception
		when others then
			null;
		end;
	end if;
	
	-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
	pls_obter_qt_job_ativa('PLS_DELETE_LOG_PROCESSO', qt_reg_w);
		
	if	(qt_reg_w = 0) then
		begin
			DBMS_JOB.SUBMIT(JOBNO,
					'PLS_DELETE_LOG_PROCESSO;',
					TRUNC(SYSDATE + 1) + ((1/24) * 2),
					'TRUNC(SYSDATE + 5) + ((1/24) * 1)');
			commit;
		exception
		when others then
			null;
		end;
	end if;
	
	-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
	pls_obter_qt_job_ativa('PLS_DELETE_ANAL_CTA_TEMP', qt_reg_w);
	
	if	(qt_reg_w = 0) then
		begin
			DBMS_JOB.SUBMIT(JOBNO,
					'PLS_DELETE_ANAL_CTA_TEMP;',
					TRUNC(SYSDATE + 1) + ((1/24) * 2.5),
					'TRUNC(SYSDATE + 3) + ((1/24) * 4)');
			commit;
		exception
		when others then
			null;
		end;
	end if;
	
	-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
	pls_obter_qt_job_ativa('PLS_DELETE_CTRL_UPD_OBJ', qt_reg_w);
	
	if	(qt_reg_w = 0) then
		begin
			DBMS_JOB.SUBMIT(JOBNO,
					'PLS_DELETE_CTRL_UPD_OBJ;',
					TRUNC(SYSDATE + 1) + ((1/24) * 3),
					'TRUNC(SYSDATE + 4) + ((1/24) * 4)');
			commit;
		exception
		when others then
			null;
		end;
	end if;
	
	-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
	pls_obter_qt_job_ativa('PLS_PROCESSAR_ANALISE_CTA_PEND', qt_reg_w);
	
	if	(qt_reg_w = 0) then
		begin
			ds_comando_job_w := 'PLS_PROCESSAR_ANALISE_CTA_PEND(' || pls_util_pck.aspas_w  || 'TASY'|| pls_util_pck.aspas_w|| ',null);';
			DBMS_JOB.SUBMIT(JOBNO,
					ds_comando_job_w,
					sysdate + 5 * (1/24/60/60),
					'(SYSDATE) + 0.1/24');
			commit;
		exception
		when others then
			null;
		end;
	end if;

	-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
	pls_obter_qt_job_ativa('PLS_DELETE_LOG_CP_CTA_TEMPO', qt_reg_w);
	
	if	(qt_reg_w = 0) then
		begin
			DBMS_JOB.SUBMIT(JOBNO,
					'PLS_DELETE_LOG_CP_CTA_TEMPO;',
					TRUNC(SYSDATE + 1) + ((1/24) * 3.5),
					'TRUNC(SYSDATE + 4) + ((1/24) * 4)');
			commit;
		exception
		when others then
			null;
		end;
	end if;
	
	-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
	pls_obter_qt_job_ativa('PLS_DELETE_CP_CTA_LOG', qt_reg_w);
	
	if	(qt_reg_w = 0) then
		begin
			DBMS_JOB.SUBMIT(JOBNO,
					'PLS_DELETE_CP_CTA_LOG;',
					TRUNC(SYSDATE + 1) + ((1/24) * 4),
					'TRUNC(SYSDATE + 4) + ((1/24) * 4)');
			commit;
		exception
		when others then
			null;
		end;
	end if;

	-- verifica se e uma UNIMED, caso retorne nulo trata-se de UNIMED
	if	(pls_consistir_funcionalidade(pls_obter_cd_estab_ops, '3', 'U', null) is null) then
	
		-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
		pls_obter_qt_job_ativa('PTU_PROCESSO_A500', qt_reg_w);
		
		if	(qt_reg_w = 0) then
			begin
				DBMS_JOB.SUBMIT(JOBNO,
						'PTU_PROCESSO_A500;',
						SYSDATE,
						'SYSDATE + (5 / 1440)');
				commit;
			exception
			when others then
				null;
			end;
		end if;
		
		-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
		pls_obter_qt_job_ativa('PLS_A500_PCK.GERENCIA_CONTA_PEND_A500', qt_reg_w);
		
		if	(qt_reg_w = 0) then
			begin
				DBMS_JOB.SUBMIT(JOBNO,
						'PLS_A500_PCK.GERENCIA_CONTA_PEND_A500(''Tasy'');',
						SYSDATE + (4 / 1440),
						'SYSDATE + (4 / 1440)');
				commit;
			exception
			when others then
				null;
			end;
		end if;	
		
		
		-- verifica se existem jobs ativas, se tiver alguma job parada ja faz a limpeza da mesma
		pls_obter_qt_job_ativa('PLS_A500_PCK.GERENCIA_ANALISE_PEND_A500', qt_reg_w);
		
		if	(qt_reg_w = 0) then
			begin
				DBMS_JOB.SUBMIT(JOBNO,
						'PLS_A500_PCK.GERENCIA_ANALISE_PEND_A500(''Tasy'');',
						SYSDATE + (5 / 1440),
						'SYSDATE + (5 / 1440)');
				commit;
			exception
			when others then
				null;
			end;
		end if;	
	end if;
	-- Job de processo de geracao de lote de analise de complemento de contas
	pls_cria_job_analise_compl;
	
	pls_obter_qt_job_ativa('PLS_DEL_CONTA_POS_ESTAB_LOG', qt_reg_w);
	
	if	(qt_reg_w = 0) then

		begin
			dbms_job.submit(jobno,
					'PLS_DEL_CONTA_POS_ESTAB_LOG;',
					TRUNC(SYSDATE + 1) + ((1/24) * 1.25),
					'TRUNC(SYSDATE + 1)');
			commit;
		exception
		when others then
			null;
		end;
	end if;
	
	commit;
end if;

end pls_cria_job_cta_med;
/
