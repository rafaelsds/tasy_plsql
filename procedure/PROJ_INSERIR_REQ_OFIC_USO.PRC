create or replace
procedure proj_inserir_req_ofic_uso(	nr_seq_ofic_uso_p	number,
					nm_usuario_p		varchar2) is

nr_seq_des_req_item_w		number(10);
nr_seq_projeto_w		number(10);

Cursor c01 is
select	b.nr_sequencia
from	des_requisito_item b,
	des_requisito a
where	b.nr_seq_requisito	= a.nr_sequencia
and	a.nr_seq_projeto	= nr_seq_projeto_w
and	b.ie_status		in ('A','C','T')
and	not exists	(select	1
			from	proj_des_ofic_uso y,
				proj_ofic_requisito x
			where	x.nr_seq_oficializacao	= y.nr_sequencia
			and	y.nr_seq_proj	= a.nr_seq_projeto
			and	x.nr_seq_des_req_item = b.nr_sequencia);

begin

if	(nr_seq_ofic_uso_p is not null) then
	select	a.nr_seq_proj
	into	nr_seq_projeto_w
	from	proj_des_ofic_uso a
	where	a.nr_sequencia	= nr_seq_ofic_uso_p;

	open c01;
	loop
	fetch c01 into	
		nr_seq_des_req_item_w;
	exit when c01%notfound;
		begin
		insert into proj_ofic_requisito
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nr_seq_oficializacao,
			nr_seq_des_req_item)
		values	(proj_ofic_requisito_seq.nextval,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nr_seq_ofic_uso_p,
			nr_seq_des_req_item_w);
		end;
	end loop;
	close c01;

	commit;
end if;

end proj_inserir_req_ofic_uso;
/