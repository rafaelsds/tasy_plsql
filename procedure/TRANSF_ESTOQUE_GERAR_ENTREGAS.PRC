create or replace
procedure transf_estoque_gerar_entregas(nr_ordem_compra_p		number,
 					nm_usuario_p		varchar2) is 


nr_item_oci_w				number(5);
qt_material_w				number(13,4);
dt_entrega_w				date;

cursor c01 is
select	a.nr_item_oci,
	a.qt_material,
	b.dt_entrega
from	ordem_compra_item a,
	ordem_compra b
where	a.nr_ordem_compra = b.nr_ordem_compra
and	b.nr_ordem_compra = nr_ordem_compra_p;

begin

open c01;
loop
fetch c01 into
	nr_item_oci_w,
	qt_material_w,
	dt_entrega_w;
exit when c01%notfound;
	begin
	
	insert into ordem_compra_item_entrega(nr_sequencia, nr_item_oci, nr_ordem_compra, qt_prevista_entrega,
							dt_atualizacao, nm_usuario, dt_prevista_entrega)
	values	(ordem_compra_item_entrega_seq.nextval, nr_item_oci_w, nr_ordem_compra_p, qt_material_w,
							sysdate, nm_usuario_p, dt_entrega_w);

	end;
end loop;
close C01;
commit;

end transf_estoque_gerar_entregas;
/
