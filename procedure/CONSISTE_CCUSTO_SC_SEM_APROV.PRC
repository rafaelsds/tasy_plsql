create or replace
procedure consiste_ccusto_sc_sem_aprov(		cd_centro_custo_p			number,
						cd_estabelecimento_p		number,
						cd_setor_atendimento_p		number,
						ie_urgente_p			varchar2,
						ds_erro_p	out			varchar2) as


cd_processo_Aprov_w	number(10);
ds_erro_w		varchar2(255) := '';


begin

obter_processo_aprov_sem_mat(
		cd_centro_custo_p,
		cd_setor_atendimento_p,
		null,
		'S',
		ie_urgente_p,
		cd_estabelecimento_p,
		null,
		null,
		null,
		cd_processo_Aprov_w);

if	(nvl(cd_processo_aprov_w,0) = 0) then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(280464);
end if;

ds_erro_p := ds_erro_w;

end consiste_ccusto_sc_sem_aprov;
/
