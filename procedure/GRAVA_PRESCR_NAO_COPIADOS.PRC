create or replace
procedure grava_prescr_nao_copiados(	nr_prescricao_p			number,
										nr_prescricao_orig_p	number,
										nr_sequencia_orig_p		number,
										cd_material_p			number,
										ie_situacao_p			varchar2,
										ie_suspenso_p			varchar2,
										ie_prescricao_p			varchar2,
										ie_disponivel_mercado_p	varchar2 default 'S',
										ie_copiar_p		varchar2 default 'S') is

ie_motivo_w			varchar2(255);

begin

if	(nr_prescricao_p is not null) and
	(nr_prescricao_orig_p is not null) and
	(cd_material_p is not null) then
	
	-- Dom�nio 5534
	ie_motivo_w		:= wheb_mensagem_pck.get_texto(307949); -- Outros
	
	if	(ie_situacao_p = 'I') then
		ie_motivo_w		:= wheb_mensagem_pck.get_texto(307950); -- Inativado
	elsif	(ie_prescricao_p = 'N') then
		ie_motivo_w		:= wheb_mensagem_pck.get_texto(307951); -- N�o pode ser prescrito
	elsif	(ie_suspenso_p = 'S') then
		ie_motivo_w		:= wheb_mensagem_pck.get_texto(307952); -- Suspenso
	elsif	(ie_disponivel_mercado_p = 'N') then
		ie_motivo_w		:= wheb_mensagem_pck.get_texto(150666); -- Este material n�o est� dispon�vel no mercado.
    	elsif	(ie_copiar_p = 'N') then
		ie_motivo_w		:= wheb_mensagem_pck.get_texto(407316); -- Regra c�pia prescri��o.
	end if;
	
	insert into w_prescr_mat_nao_copia(
			nr_prescricao,
			nr_sequencia,
			nr_prescricao_origem,
			nr_sequencia_origem,
			cd_material,
			ie_motivo)
	values(
			nr_prescricao_p,
			null,
			nr_prescricao_orig_p,
			nr_sequencia_orig_p,
			cd_material_p,
			ie_motivo_w);

end if;

commit;

end grava_prescr_nao_copiados;
/