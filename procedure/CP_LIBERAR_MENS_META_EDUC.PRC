create or replace 
procedure cp_liberar_mens_meta_educ(
					nr_sequencia_p 	number,
					nm_usuario_p	varchar2) is
	
begin

if (nr_sequencia_p is not null) then

	update	pat_cp_ind_measure_eg
	set		dt_liberacao = sysdate,
			nm_usuario_lib = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;

end if;

end cp_liberar_mens_meta_educ;
/