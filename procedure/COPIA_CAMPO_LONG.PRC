create or replace
PROCEDURE COPIA_CAMPO_LONG(	nm_tabela_p VARCHAR2,nm_coluna_p VARCHAR2,ds_restricao_where_p VARCHAR2,ds_param_select_p VARCHAR2,ds_param_update_p VARCHAR2) IS
/*
 Esta procedure realiza a copia de um campo long, por�m ser� copiado no m�ximo 65528 caracteres.
 N�o ir� apresentar erro caso o conte�do a ser copiado seja superior ao tamanho m�ximo

*/
ds_param_w		VARCHAR2(2000);
ds_campo_clob_w		clob;
ds_sql_w		VARCHAR2(2000);
ds_parametro_w		VARCHAR2(255);
nr_sequencia_w		NUMBER(10);
qt_registro_w		NUMBER(10);
c001			INTEGER;
retorno_w		NUMBER(5);
dt_aux_w	DATE;


ds_conteudo_1_w		VARCHAR2(32764);
ds_conteudo_2_w		VARCHAR2(32764);

TYPE lista IS RECORD (
	nm VARCHAR2(50),
	vl LONG );
TYPE myArray IS TABLE OF lista INDEX BY BINARY_INTEGER;

/*Contem os parametros do SQL*/
ar_parametros_w myArray;

ds_param_atual_w 	VARCHAR2(512);
ds_parametros_w 	VARCHAR2(2000);
nr_pos_separador_w	NUMBER(10);
qt_parametros_w		NUMBER(10);
qt_contador_w		NUMBER(10);

ds_sep_bv_w		VARCHAR2(10);
qt_tam_seq_w		NUMBER(2);

BEGIN

/*INICIO - TRATAMENTO PARAMETROS BIND VARIABLE*/
ds_sep_bv_w := obter_separador_bv;
IF	(INSTR(ds_param_update_p,ds_sep_bv_w) = 0 ) THEN
	ds_sep_bv_w := ';';
END IF;
qt_tam_seq_w := LENGTH(ds_sep_bv_w);
ds_parametros_w    := ds_param_update_p;
nr_pos_separador_w := INSTR(ds_parametros_w,ds_sep_bv_w);
qt_parametros_w	   := 0;
WHILE	(nr_pos_separador_w > 0 ) LOOP
	BEGIN
	qt_parametros_w := qt_parametros_w + 1;
	ds_param_atual_w  := SUBSTR(ds_parametros_w,1,nr_pos_separador_w-1);
	ds_parametros_w   := SUBSTR(ds_parametros_w,nr_pos_separador_w+qt_tam_seq_w,LENGTH(ds_parametros_w));
	nr_pos_separador_w := INSTR(ds_param_atual_w,'=');
	ar_parametros_w(qt_parametros_w).nm := UPPER(SUBSTR(ds_param_atual_w,1,nr_pos_separador_w-1));
	ar_parametros_w(qt_parametros_w).vl := SUBSTR(ds_param_atual_w,nr_pos_separador_w+1,LENGTH(ds_param_atual_w));
	nr_pos_separador_w := INSTR(ds_parametros_w,ds_sep_bv_w);
	IF	(qt_parametros_w > 1000) THEN
		nr_pos_separador_w := 0;
	END IF;
	END;
END LOOP;
	nr_pos_separador_w := INSTR(ds_parametros_w,'=');
IF	( nr_pos_separador_w > 0 ) THEN
	qt_parametros_w := qt_parametros_w +1;
	ds_param_atual_w := ds_parametros_w;
	ar_parametros_w(qt_parametros_w).nm := UPPER(SUBSTR(ds_param_atual_w,1,nr_pos_separador_w-1));
	ar_parametros_w(qt_parametros_w).vl := SUBSTR(ds_param_atual_w,nr_pos_separador_w+1,LENGTH(ds_param_atual_w));
END IF;
/*FIM - TRATAMENTO PARAMETRO BIND VARIABLE*/


/*INICIO - VERIFICAR SE TABELA TEMPOR�RIO EXISTE SE N�O EXISTIR CRIA*/
SELECT 	COUNT(*)
INTO	qt_registro_w
FROM	user_tables
WHERE	table_name = 'W_COPIA_CAMPO_LONG';

IF	( qt_registro_w = 0 ) THEN
	exec_sql_dinamico('','create table w_copia_campo_long (nr_sequencia number(10), ds_texto clob)');
END IF;
/*FIM - VERIFICAR SE TABELA TEMPOR�RIO EXISTE SE N�O EXISTIR CRIA*/


/*INICIO - TRANSFERE CONTEUDO DO CAMPO LONG DA TABELA DE ORIGEM PARA O CAMPO CLOB DA TABELA TEMPORARIO*/

obter_valor_dinamico ('select	(nvl(max(nr_sequencia),0) + 1) from w_copia_campo_long',nr_sequencia_w);
ds_sql_w   := 'insert into w_copia_campo_long select :SEQUENCE, to_lob('|| nm_coluna_p || ') from ' || nm_tabela_p || ' ' || ds_restricao_where_p;
ds_param_w := 'SEQUENCE='|| TO_CHAR(NR_SEQUENCIA_W)||';' || ds_param_select_p;
exec_sql_dinamico_bv('',ds_sql_w,ds_param_w);

/*FIM - TRANSFERE CONTEUDO DO CAMPO LONG DA TABELA DE ORIGEM PARA O CAMPO CLOB DA TABELA TEMPORARIO*/


/*INICIO - RECUPERA O VALOR DO CAMPO CLOB PARA A VARIAVEL DA PROCEDURE*/
ds_sql_w	:= ' select ds_texto from w_copia_campo_long where nr_sequencia = :sequence ';

C001 := DBMS_SQL.OPEN_CURSOR;
DBMS_SQL.PARSE(C001, ds_sql_w, dbms_sql.Native);
DBMS_SQL.DEFINE_COLUMN(C001, 1, ds_campo_clob_w);
DBMS_SQL.BIND_VARIABLE(C001, 'SEQUENCE', nr_sequencia_w);
retorno_w := DBMS_SQL.EXECUTE(c001);
retorno_w := DBMS_SQL.fetch_rows(c001);
DBMS_SQL.COLUMN_VALUE(C001, 1, ds_campo_clob_w );
DBMS_SQL.CLOSE_CURSOR(C001);

/*FIM - RECUPERA O VALOR DO CAMPO CLOB PARA A VARIAVEL DA PROCEDURE*/


/*INICIO - QUEBRA O VALOR DO CONTEUDO CLOB EM VARIOS VARCHAR PARA PODER INSERIR NA TABELA DE ORIGEM*/
ds_conteudo_1_w := dbms_lob.SUBSTR(ds_campo_clob_w,32764,1);
ds_conteudo_2_w := dbms_lob.SUBSTR(ds_campo_clob_w,32764,32765);
/*FIM QUEBRA O VALOR DO CONTEUDO CLOB EM VARIOS VARCHAR PARA PODER INSERIR NA TABELA DE ORIGEM*/


/*INICIO - TRANSFERE CONTEUDO DAS VARIAVIES VARCHAR PARA O  CAMPO LONG DA TABELA DE ORIGEM*/

ds_sql_w	:= ' update ' || nm_tabela_p || ' set ' || nm_coluna_p || '= :ds_texto ' || ds_restricao_where_p;
C001 := DBMS_SQL.OPEN_CURSOR;
DBMS_SQL.PARSE(C001, ds_sql_w, dbms_sql.Native);
DBMS_SQL.BIND_VARIABLE(C001, 'DS_TEXTO', ds_conteudo_1_w || ds_conteudo_2_w);

FOR contador_w IN 1..ar_parametros_w.COUNT LOOP
	IF	(ar_parametros_w(contador_w).nm LIKE 'DT_%') THEN
		dt_aux_w := TO_DATE(ar_parametros_w(contador_w).vl,'dd/mm/yyyy hh24:mi:ss');
		DBMS_SQL.BIND_VARIABLE(C001, ar_parametros_w(contador_w).nm, dt_aux_w);
	ELSE
		DBMS_SQL.BIND_VARIABLE(C001, ar_parametros_w(contador_w).nm, ar_parametros_w(contador_w).vl,32764);
	END IF;
END LOOP;

retorno_w := DBMS_SQL.EXECUTE(c001);
DBMS_SQL.CLOSE_CURSOR(C001);

/*FIM TRANSFERE CONTEUDO DAS VARIAVIES VARCHAR PARA O  CAMPO LONG DA TABELA DE ORIGEM*/


/*INICIO - DELETA O REGISTRO DA TABELA TEMPOR�RIA*/

ds_sql_w := 'delete from w_copia_campo_long where nr_sequencia = :nr_sequencia';
ds_param_w := 'NR_SEQUENCIA='|| TO_CHAR(nr_sequencia_w);
exec_sql_dinamico_bv('',ds_sql_w,ds_param_w);

/*FIM - DELETA O REGISTRO DA TABELA TEMPOR�RIA*/

END COPIA_CAMPO_LONG;
/