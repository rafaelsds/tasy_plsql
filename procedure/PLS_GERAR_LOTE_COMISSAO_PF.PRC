create or replace
procedure pls_gerar_lote_comissao_pf
			(	nr_seq_lote_p		Number,
				ie_opcao_p		varchar2,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 
/*
ie_opcao_p
	G = Gerar lote
	D = Desfazer lote
	L = Liberar lote
*/

dt_referencia_w			Date;
ie_benef_proposta_w		Varchar2(1);
cd_pessoa_fisica_w		Varchar2(10);
cd_cgc_w			Varchar2(100);
nr_seq_bonificacao_w		Number(10);
nr_seq_indicacao_venda_w	Number(10);
nr_seq_proposta_w		Number(10);
vl_proposta_benef_w		Number(15,2);
vl_proposta_w			Number(15,2);
vl_benef_w			Number(15,2);
vl_total_indicacao_w		Number(15,2);
nr_seq_segurado_prop_w		Number(10);
vl_comissao_benef_w		Number(15,2);
tx_bonificacao_w		Number(7,4);
vl_bonificacao_w		Number(15,2);
ds_observacao_w			Varchar2(255);
qt_titulos_w			Number(10);
ds_beneficiario_proposta_w	Varchar2(4000);

Cursor C01 is
	select	'B',
		b.cd_pessoa_fisica,
		b.cd_cgc,
		b.nr_seq_bonificacao,
		c.nr_sequencia
	from	pls_proposta_beneficiario	a,
		pls_indicacao_venda		b,
		pls_proposta_adesao		c
	where	b.nr_seq_segurado_prop = a.nr_sequencia
	and	a.nr_seq_proposta = c.nr_sequencia
	and	(b.cd_pessoa_fisica is not null or b.cd_cgc is not null)
	and	b.nr_seq_lote_comissao is null
	and	trunc(c.dt_inicio_proposta,'month') = dt_referencia_w
	group by	b.nr_seq_bonificacao,
			b.cd_pessoa_fisica,
			b.cd_cgc,
			c.nr_sequencia
	union all
	select	'P',
		b.cd_pessoa_fisica,
		b.cd_cgc,
		b.nr_seq_bonificacao,
		c.nr_sequencia
	from	pls_proposta_beneficiario	a,
		pls_indicacao_venda		b,
		pls_proposta_adesao		c
	where	b.nr_seq_proposta = c.nr_sequencia
	and	a.nr_seq_proposta = c.nr_sequencia
	and	(b.cd_pessoa_fisica is not null or b.cd_cgc is not null)
	and	b.nr_seq_lote_comissao is null
	and	trunc(c.dt_inicio_proposta,'month') = dt_referencia_w
	and	not exists     (select	1
				from	pls_proposta_beneficiario	x,
					pls_indicacao_venda		y,
					pls_proposta_adesao		z
				where	y.nr_seq_segurado_prop	= x.nr_sequencia
				and	x.nr_seq_proposta = z.nr_sequencia
				and	trunc(z.dt_inicio_proposta,'month') = dt_referencia_w
				and	x.nr_sequencia = a.nr_sequencia)
	group by	b.nr_seq_bonificacao,
			b.cd_pessoa_fisica,
			b.cd_cgc,
			c.nr_sequencia;

Cursor C02 is
	select	'B',
		b.nr_sequencia,
		b.nr_seq_proposta,
		b.nr_seq_segurado_prop
	from	pls_proposta_beneficiario	a,
		pls_indicacao_venda		b,
		pls_proposta_adesao		c
	where	b.nr_seq_segurado_prop = a.nr_sequencia
	and	a.nr_seq_proposta = c.nr_sequencia
	and	(b.cd_pessoa_fisica is not null or b.cd_cgc is not null)
	and	b.nr_seq_lote_comissao is null
	and	(b.cd_pessoa_fisica = cd_pessoa_fisica_w or b.cd_cgc = cd_cgc_w)
	and	b.nr_seq_bonificacao = nr_seq_bonificacao_w
	and	c.nr_sequencia = nr_seq_proposta_w
	and	trunc(c.dt_inicio_proposta,'month') = dt_referencia_w
	group by	b.nr_sequencia,
			b.nr_seq_proposta,
			b.nr_seq_segurado_prop
	union all
	select	'P',
		b.nr_sequencia,	
		b.nr_seq_proposta,
		b.nr_seq_segurado_prop
	from	pls_proposta_beneficiario	a,
		pls_indicacao_venda		b,
		pls_proposta_adesao		c
	where	b.nr_seq_proposta = c.nr_sequencia
	and	a.nr_seq_proposta = c.nr_sequencia
	and	(b.cd_pessoa_fisica is not null or b.cd_cgc is not null)
	and	b.nr_seq_lote_comissao is null
	and	(b.cd_pessoa_fisica = cd_pessoa_fisica_w or b.cd_cgc = cd_cgc_w)
	and	b.nr_seq_bonificacao = nr_seq_bonificacao_w
	and	c.nr_sequencia = nr_seq_proposta_w
	and	trunc(c.dt_inicio_proposta,'month') = dt_referencia_w
	and	not exists	(select	1
				from	pls_proposta_beneficiario	x,
					pls_indicacao_venda		y,
					pls_proposta_adesao		z
				where	y.nr_seq_segurado_prop = x.nr_sequencia
				and	x.nr_seq_proposta = z.nr_sequencia
				and	trunc(z.dt_inicio_proposta,'month') = dt_referencia_w
				and	x.nr_sequencia = a.nr_sequencia)
	group by	b.nr_sequencia,
			b.nr_seq_proposta,
			b.nr_seq_segurado_prop;

begin

select	trunc(dt_referencia,'month')
into	dt_referencia_w
from	pls_lote_comissao_pf
where	nr_sequencia	= nr_seq_lote_p;

if	(ie_opcao_p = 'D') then
	select	count(*)
	into	qt_titulos_w
	from	pls_comissao_pf
	where	nr_seq_lote	= nr_seq_lote_p
	and	nr_titulo is not null;
	
	if	(qt_titulos_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(224788);
		/* Mensagem: N�o � poss�vel desfazer o lote de comiss�o pois j� foram gerados t�tulos para o mesmo! */
	else
		update	pls_indicacao_venda
		set	nr_seq_lote_comissao	= null
		where	nr_seq_lote_comissao	= nr_seq_lote_p;
		
		delete	from	pls_comissao_pf
		where	nr_seq_lote = nr_seq_lote_p;
	end if;
elsif	(ie_opcao_p = 'L') then
	update	pls_lote_comissao_pf
	set	nm_usuario_liberacao	= nm_usuario_p,
		dt_liberacao		= sysdate
	where	nr_sequencia		= nr_seq_lote_p;
elsif	(ie_opcao_p = 'G') then
	open C01;
	loop
	fetch C01 into	
		ie_benef_proposta_w,
		cd_pessoa_fisica_w,
		cd_cgc_w,
		nr_seq_bonificacao_w,
		nr_seq_proposta_w;
	exit when C01%notfound;
		begin
		ds_beneficiario_proposta_w	:= '';
		
		select	max(a.tx_bonificacao),
			max(a.vl_bonificacao)
		into	tx_bonificacao_w,
			vl_bonificacao_w
		from	pls_bonificacao_regra	a,
			pls_bonificacao		b
		where	a.nr_seq_bonificacao	= b.nr_sequencia
		and	b.nr_sequencia		= nr_seq_bonificacao_w;
		
		open C02;
		loop
		fetch C02 into	
			ie_benef_proposta_w,
			nr_seq_indicacao_venda_w,
			nr_seq_proposta_w,
			nr_seq_segurado_prop_w;
		exit when C02%notfound;
			begin
			
			vl_benef_w		:= 0;
			vl_comissao_benef_w	:= 0;
			if	(ds_beneficiario_proposta_w is null) then
				ds_beneficiario_proposta_w	:= nr_seq_segurado_prop_w;
			else
				ds_beneficiario_proposta_w	:= ds_beneficiario_proposta_w || ', '|| nr_seq_segurado_prop_w;
			end if;
			
			if	(ie_benef_proposta_w = 'B') then
				select	pls_obter_total_proposta_benef(nr_seq_segurado_prop_w)
				into	vl_benef_w
				from	dual;
				
				vl_comissao_benef_w	:= ((nvl(tx_bonificacao_w,0)/100) * nvl(vl_benef_w,0)) + nvl(vl_bonificacao_w,0); /* Calcula o vlaor a ser pago pela indica��o */
				ds_observacao_w	:= 'Comiss�o gerada pela indica��o do(s) benefici�rio(s): '||ds_beneficiario_proposta_w;
			elsif	(ie_benef_proposta_w = 'P') then
				pls_calcular_propos_benef(nr_seq_proposta_w, vl_proposta_benef_w, vl_proposta_w);
				
				vl_comissao_benef_w	:= ((nvl(tx_bonificacao_w,0)/100) * nvl(vl_proposta_w,0)) + nvl(vl_bonificacao_w,0); /* Calcula o vlaor a ser pago pela indica��o */
				ds_observacao_w	:= 'Comiss�o gerada pela indica��o da proposta '||nr_seq_proposta_w;
			end if;
			
			update	pls_indicacao_venda
			set	nr_seq_lote_comissao	= nr_seq_lote_p
			where	nr_sequencia		= nr_seq_indicacao_venda_w;
			
			vl_total_indicacao_w	:= nvl(vl_total_indicacao_w,0) + nvl(vl_comissao_benef_w,0);
			end;
		end loop;
		close C02;
		
		insert	into	pls_comissao_pf
			(	nr_sequencia, nr_seq_lote, dt_atualizacao,
				nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				cd_pessoa_fisica, cd_cgc, vl_indicacao, ds_observacao)
			values
			(	pls_comissao_pf_seq.nextval, nr_seq_lote_p, sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				cd_pessoa_fisica_w, cd_cgc_w, vl_total_indicacao_w, ds_observacao_w);
		vl_total_indicacao_w	:= 0;
		end;
	end loop;
	close C01;
end if;

commit;

end pls_gerar_lote_comissao_pf;
/