create or replace
procedure gerar_solic_kit_mat_comp (nr_sequencia_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is

cd_material_w		number(6);
nr_seq_item_w		number(5);
qt_solicitada_w		number(13,4);
qt_material_w		number(13,4);
qt_disponivel_w		number(13,4);
qt_kit_solic_w		number(13,4);
cd_kit_material_w	number(5);
cd_local_estoque_w	number(4);

Cursor C01 is
select	a.cd_material,
	a.qt_material,
	a.nr_sequencia
from	componente_kit a
where	cd_kit_material = cd_kit_material_w
and	ie_situacao = 'A'
and	((cd_estab_regra is null) or (cd_estab_regra = cd_estabelecimento_p))
and	not exists (	select	1
			from	solic_kit_mat_comp x
			where	x.nr_seq_solic_kit = nr_sequencia_p
			and	x.nr_seq_item_kit = a.nr_sequencia);

begin

select	qt_solicitada,
	cd_kit_material,
	cd_local_estoque
into	qt_kit_solic_w,
	cd_kit_material_w,
	cd_local_estoque_w
from	solic_kit_material
where	nr_sequencia = nr_sequencia_p;

open C01;
loop
fetch C01 into	
	cd_material_w,
	qt_material_w,
	nr_seq_item_w;
exit when C01%notfound;
	begin
	qt_solicitada_w := qt_material_w * qt_kit_solic_w;
	
	qt_disponivel_w := null;

	insert into solic_kit_mat_comp(
		nr_sequencia,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_seq_solic_kit,
		nr_seq_item_kit,
		cd_material,
		qt_solicitada,
		qt_disponivel)
	values(	solic_kit_mat_comp_seq.nextval,
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_sequencia_p,
		nr_seq_item_w,
		cd_material_w,
		qt_solicitada_w,
		qt_disponivel_w);	
	end;
end loop;
close C01;

commit;

end gerar_solic_kit_mat_comp;
/