create or replace 
procedure verifica_se_prescr_atend (nr_prescricao_p	number) is 

ie_prescr_atend_w varchar2(1);

begin

select	nvl(max('S'),'N')
into	ie_prescr_atend_w
from	prescr_material
where	nr_prescricao = nr_prescricao_p
and		cd_motivo_baixa > 0;

if (ie_prescr_atend_w = 'S') then
	-- Esta prescri��o j� foi atendida e n�o ser� poss�vel realizar esta a��o!
	Wheb_mensagem_pck.exibir_mensagem_abort( 261352 );
end if;

end verifica_se_prescr_atend;
/