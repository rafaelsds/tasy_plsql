create or replace
procedure pls_oc_cta_tratar_val_40(
			dados_regra_p		pls_tipos_ocor_pck.dados_regra,
			nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
			nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o de m�dico executor da conta ou participante ativo ou inativo.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Altera��es:	
------------------------------------------------------------------------------------------------------------------
dlehmkuhl OS 688483 - 14/04/2014 -

Altera��o:	Modificada a forma de trabalho em rela��o a atualiza��o dos campos de controle
	que basicamente decidem se a ocorr�ncia ser� ou n�o gerada. Foi feita tamb�m a 
	substitui��o da rotina obterX_seX_geraX.

Motivo:	Necess�rio realizar essas altera��es para corrigir bugs principalmente no que se
	refere a quest�o de aplica��o de filtros (passo anterior ao da valida��o). Tamb�m
	tivemos um foco especial em performance, visto que a mesma precisou ser melhorada
	para n�o inviabilizar a nova solicita��o que diz que a exce��o deve verificar todo
	o atendimento.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_sql_w		varchar2(4000); 
nr_seq_selecao_w	dbms_sql.number_table;
ds_observacao_w		dbms_sql.varchar2_table;
ie_valido_w		dbms_sql.varchar2_table;
v_cur			pls_util_pck.t_cursor;

-- Informa��es da valida��o de n�o-utiliza��o de item autorizado
cursor C01 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.nr_sequencia	nr_seq_validacao,
		a.nr_seq_proc_espec,
		a.nr_seq_proc_espec_solic
	from	pls_oc_cta_val_proc_espec a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;	
begin

-- Deve ter a informa��o da regra para que seja aplicada a valida��o.
if	((dados_regra_p.nr_sequencia is not null))  then
		
	for	r_C01_w in C01( dados_regra_p.nr_sequencia) loop
		
		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
	
		--Regra proc x especialidade m�dico executor--
		if	(r_C01_w.nr_seq_proc_espec is not null) then
	 		ds_sql_w := ds_sql_w || 'select	sel.nr_sequencia nr_seq_selecao, ' || pls_tipos_ocor_pck.enter_w ||
						' ''S'' ie_registro_valido, ' ||pls_tipos_ocor_pck.enter_w||
						' null ds_obervacao ' ||pls_tipos_ocor_pck.enter_w||
						'from	pls_oc_cta_selecao_ocor_v sel, ' || pls_tipos_ocor_pck.enter_w ||
						'	pls_conta_ocor_v conta, ' || pls_tipos_ocor_pck.enter_w ||
						'	pls_conta_proc_ocor_v proc '||pls_tipos_ocor_pck.enter_w||
						'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_tipos_ocor_pck.enter_w ||
						'and	sel.ie_valido = ''S'' ' || pls_tipos_ocor_pck.enter_w ||
						'and	conta.nr_sequencia = proc.nr_seq_conta'||pls_tipos_ocor_pck.enter_w||
						'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
						'and	sel.nr_seq_conta_proc = proc.nr_sequencia' || pls_tipos_ocor_pck.enter_w ||
						'and	sel.ie_tipo_registro = ''P'' '|| pls_tipos_ocor_pck.enter_w ||
						'and	pls_obter_se_espec_solic_oc(proc.nr_sequencia, :nr_seq_proc_espec ,conta.cd_medico_executor) = ''N'' '|| pls_tipos_ocor_pck.enter_w ||
						'and	exists ( ' || pls_tipos_ocor_pck.enter_w ||
						'		select	1 '|| pls_tipos_ocor_pck.enter_w ||
						'		from	medico med' || pls_tipos_ocor_pck.enter_w ||
						'		where	med.cd_pessoa_fisica = conta.cd_medico_executor )' || pls_tipos_ocor_pck.enter_w ||						
						
						'and 	not exists ('|| pls_tipos_ocor_pck.enter_w ||
						'		select 1 '|| pls_tipos_ocor_pck.enter_w ||
						'		from pls_proc_participante part'|| pls_tipos_ocor_pck.enter_w ||
						'		where proc.nr_sequencia = part.nr_seq_conta_proc'|| pls_tipos_ocor_pck.enter_w ||
						'	) '|| pls_tipos_ocor_pck.enter_w ||
						'union '|| pls_tipos_ocor_pck.enter_w ||
						'select	sel.nr_sequencia nr_seq_selecao, ' || pls_tipos_ocor_pck.enter_w ||
						' ''S'' ie_registro_valido, ' ||pls_tipos_ocor_pck.enter_w||
						' null ds_obervacao ' ||pls_tipos_ocor_pck.enter_w||
						'from	pls_oc_cta_selecao_ocor_v sel, ' || pls_tipos_ocor_pck.enter_w ||
						'	pls_conta_ocor_v conta, ' || pls_tipos_ocor_pck.enter_w ||
						'	pls_conta_proc_ocor_v proc, '||pls_tipos_ocor_pck.enter_w||
						'	pls_proc_participante part '||pls_tipos_ocor_pck.enter_w||
						'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_tipos_ocor_pck.enter_w ||
						'and	sel.ie_valido = ''S'' ' || pls_tipos_ocor_pck.enter_w ||
						'and	conta.nr_sequencia = proc.nr_seq_conta'||pls_tipos_ocor_pck.enter_w||
						'and	part.nr_seq_conta_proc = proc.nr_sequencia '||pls_tipos_ocor_pck.enter_w||
						'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
						'and	sel.nr_seq_conta_proc = proc.nr_sequencia' || pls_tipos_ocor_pck.enter_w ||
						'and	sel.ie_tipo_registro = ''P'' '|| pls_tipos_ocor_pck.enter_w ||
						'and	pls_obter_se_espec_solic_oc(proc.nr_sequencia, :nr_seq_proc_espec, part.cd_medico) = ''N'' '|| pls_tipos_ocor_pck.enter_w ||
						'and	exists ( ' || pls_tipos_ocor_pck.enter_w ||
						'		select	1 '|| pls_tipos_ocor_pck.enter_w ||
						'		from	medico med' || pls_tipos_ocor_pck.enter_w ||
						'		where	med.cd_pessoa_fisica = part.cd_medico ' || pls_tipos_ocor_pck.enter_w ||
						'		and	part.ie_status <> ''C'''|| pls_tipos_ocor_pck.enter_w ||					
						'	) ';
		--Regra proc x especialidade m�dico solicitante
		else
			ds_sql_w := ds_sql_w || 'select	sel.nr_sequencia nr_seq_selecao, ' || pls_tipos_ocor_pck.enter_w ||
						' ''S'' ie_registro_valido, ' ||pls_tipos_ocor_pck.enter_w||
						' null ds_obervacao ' ||pls_tipos_ocor_pck.enter_w||
						'from	pls_oc_cta_selecao_ocor_v sel, ' || pls_tipos_ocor_pck.enter_w ||
						'	pls_conta_ocor_v conta, ' || pls_tipos_ocor_pck.enter_w ||
						'	pls_conta_proc_ocor_v proc '||pls_tipos_ocor_pck.enter_w||
						'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_tipos_ocor_pck.enter_w ||
						'and	sel.ie_valido = ''S'' ' || pls_tipos_ocor_pck.enter_w ||
						'and	conta.nr_sequencia = proc.nr_seq_conta'||pls_tipos_ocor_pck.enter_w||
						'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_tipos_ocor_pck.enter_w ||
						'and	sel.nr_seq_conta_proc = proc.nr_sequencia' || pls_tipos_ocor_pck.enter_w ||
						'and	sel.ie_tipo_registro = ''P'' '|| pls_tipos_ocor_pck.enter_w ||
						'and	pls_obter_se_espec_solic_oc(proc.nr_sequencia, :nr_seq_proc_espec ,conta.cd_medico_solicitante) = ''N'' '|| pls_tipos_ocor_pck.enter_w ||
						'and	exists ( ' || pls_tipos_ocor_pck.enter_w ||
						'		select	1 '|| pls_tipos_ocor_pck.enter_w ||
						'		from	medico med' || pls_tipos_ocor_pck.enter_w ||
						'		where	med.cd_pessoa_fisica = conta.cd_medico_executor )';	
		end if;
		
		begin				
			if	(r_C01_w.nr_seq_proc_espec is not null) then
				open v_cur for ds_sql_w using 	nr_id_transacao_p, r_C01_w.nr_seq_proc_espec, nr_id_transacao_p, r_C01_w.nr_seq_proc_espec;
			else
				open v_cur for ds_sql_w using 	nr_id_transacao_p, r_C01_w.nr_seq_proc_espec_solic;
			end if;
			
			loop
				nr_seq_selecao_w	:= pls_util_cta_pck.num_table_vazia_w;
				ie_valido_w		:= pls_util_cta_pck.vchr2_table_vazia_w;	
				ds_observacao_w		:= pls_util_cta_pck.vchr2_table_vazia_w;
				fetch v_cur bulk collect
				into  nr_seq_selecao_w, ie_valido_w, ds_observacao_w
				limit pls_util_cta_pck.qt_registro_transacao_w;
					exit when nr_seq_selecao_w.count = 0;	
					
					pls_tipos_ocor_pck.gerencia_selecao_validacao( nr_seq_selecao_w, pls_util_cta_pck.clob_table_vazia_w, 'SEQ', ds_observacao_w, 
										ie_valido_w, nm_usuario_p); 
					
			end loop;
			close v_cur;
		exception			
		when others then
			--Fecha cursor
			close v_cur;
			-- Insere o log na tabela e aborta a opera��o
			pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p,ds_sql_w,nr_id_transacao_p,nm_usuario_p);
		end;

		-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
		pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
		
	end loop; -- C01
end if;

end pls_oc_cta_tratar_val_40;
/