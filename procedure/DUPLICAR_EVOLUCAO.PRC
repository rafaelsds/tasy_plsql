CREATE OR REPLACE
PROCEDURE Duplicar_evolucao(
			nr_seq_evolucao_p	Number,
			nr_atendimento_p	Number,
			nm_usuario_p		Varchar2,
			cd_evolucao_criada_p out number,
			ie_copia_texto_p	varchar2 default 'N') IS 

ie_tipo_evolucao_w	Varchar2(10);
cd_medico_w		Varchar2(10);
dt_fim_conta_w		Date;
nr_atendimento_w	Number(10);
ie_lib_tipo_w		Varchar2(1);
cd_estabelecimento_w	number(4);
ie_evolucao_clinica_w	varchar2(10);	
ie_tipo_padrao_w	varchar2(10);
ie_evolucao_clinica_padrao_w	varchar2(10);
cd_evolucao_w		number(10);
ie_situacao_w		varchar2(1);
qt_reg_w		number(1);
ie_lib_tipo_regra_w	Varchar2(1);
ie_tipo_regra_w		Varchar2(10);
ie_lib_tipo_padrao_w    Varchar2(1);
ie_tipo_registro_w 		Varchar(10);
ie_nivel_atencao_w		varchar2(1);

BEGIN

nr_atendimento_w	:= nr_atendimento_p;
ie_nivel_atencao_w := wheb_assist_pck.get_nivel_atencao_perfil;	


if	(nr_atendimento_p > 0) then
	if	(Obter_Funcao_Ativa <> 281) then
		begin
		select	dt_fim_conta,
				cd_estabelecimento
		into	dt_fim_conta_w,
				cd_estabelecimento_w
		from	atendimento_paciente 
		where	nr_atendimento = nr_atendimento_p;
		
		if	(dt_fim_conta_w is not null) then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(201165);
		end if;
		end;
	end if;
else
	nr_atendimento_w	:= null;
end if;

Obter_Param_Usuario(281,226,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_evolucao_clinica_w);
Obter_Param_Usuario(281,492,obter_perfil_ativo,nm_usuario_p,wheb_usuario_pck.get_cd_estabelecimento,ie_tipo_padrao_w);


if	(ie_evolucao_clinica_w is null) then
	ie_evolucao_clinica_w	:= 'E';
end if;

select	nvl(max(ie_tipo_evolucao),1),
	max(cd_pessoa_fisica)
into	ie_tipo_evolucao_w,
	cd_medico_w
from	usuario
where	nm_usuario	= nm_usuario_p;

select	max(ie_evolucao_clinica)
into	ie_evolucao_clinica_padrao_w
from	evolucao_paciente
where	cd_evolucao	= nr_seq_evolucao_p;

select	count(*)
into	qt_reg_w
from	tipo_evolucao
where	cd_tipo_evolucao = ie_evolucao_clinica_padrao_w
and		ie_situacao = 'A';

if	(ie_tipo_padrao_w	= 'S') and
	(ie_evolucao_clinica_padrao_w is not null) and
	(qt_reg_w > 0) then
	ie_evolucao_clinica_w	:= ie_evolucao_clinica_padrao_w;
end if;

if (nvl(nr_atendimento_p,0) > 0) then
	ie_tipo_regra_w := Obter_tipo_evolucao_regra(nr_atendimento_p); --Verificando a Regra 
	
	if (ie_tipo_regra_w is not null) then  -- Caso n�o vier nulo, e trouxer resultado, verificar se o tipo da evoluca��o esta liberado para o ousu�rio 
		ie_lib_tipo_regra_w := consiste_se_exibe_evolucao(ie_tipo_regra_w,cd_medico_w,nr_atendimento_p);
	end if;
	
end if;

ie_lib_tipo_w	:= consiste_se_exibe_evolucao(ie_evolucao_clinica_w,cd_medico_w,nr_atendimento_p);
ie_lib_tipo_padrao_w := consiste_se_exibe_evolucao('E',cd_medico_w,nr_atendimento_p); --verificando se o padrao esta liberado

select decode(ie_lib_tipo_w,'S',ie_evolucao_clinica_w, decode(nvl(ie_lib_tipo_regra_w,'N'),'S',ie_tipo_regra_w, decode(ie_lib_tipo_padrao_w,'S','E',null))) 
into   ie_tipo_registro_w
from   dual;

if (ie_tipo_registro_w  is not null) then 
	select	  nvl(max(CD_TIPO_EVOLUCAO), null) --Verificar se o tipo de evolu��o esta ativo ou n�o. 
	into	  ie_tipo_registro_w
	from	  tipo_evolucao  a
	where	a.cd_tipo_evolucao	  = ie_tipo_registro_w
	and 	a.ie_situacao = 'A';
end if;

select	evolucao_paciente_seq.nextval
into	cd_evolucao_w
from	dual;

select	max(ie_situacao)
into	ie_situacao_w
from	tipo_evolucao
where	cd_tipo_evolucao = ie_evolucao_clinica_w;

if	(ie_situacao_w = 'I') then
	ie_evolucao_clinica_w := null;
end if;



insert into evolucao_paciente(
	cd_evolucao,
	dt_evolucao,
	ie_tipo_evolucao,
	cd_pessoa_fisica,
	dt_atualizacao,
	nm_usuario,
	nr_atendimento,
	ds_evolucao,
	cd_medico,
	dt_liberacao,
	ie_evolucao_clinica,
	cd_setor_atendimento,
	cd_especialidade,
	cd_medico_parecer,
	qt_peso,
	qt_altura,
	qt_superf_corporia,
	cd_topografia,
	cd_tumor_prim_pat,
	cd_linfonodo_reg_pat,
	cd_metastase_dist_pat,
	cd_estadio_outro,
	dt_inicio_trat_proposto,
	dt_final_trat_proposto,
	cd_estado_doenca,
	ie_estado_pac_fim_trat,
	ie_recem_nato,
	nr_cirurgia,
	ie_situacao,
	cd_especialidade_medico,
	nr_seq_pepo,
	ie_avaliador_aux,
	ds_lista_problemas,
	nm_usuario_nrec,
	dt_atualizacao_nrec,
	ie_relev_resumo_alta,
	ie_nivel_atencao,
	nr_seq_evol_grav,
	NR_SEQ_ATEND_CONS_PEPA)
select	cd_evolucao_w,
	sysdate,
	ie_tipo_evolucao_w,
	cd_pessoa_fisica,
	sysdate,
	nm_usuario_p,
	nr_atendimento_w,
	' ',
	cd_medico_w,
	null,
	ie_tipo_registro_w,
	obter_setor_atendimento(nr_atendimento_p),
	null,
	null,
	qt_peso,
	qt_altura,
	qt_superf_corporia,
	cd_topografia,
	cd_tumor_prim_pat,
	cd_linfonodo_reg_pat,
	cd_metastase_dist_pat,
	cd_estadio_outro,
	dt_inicio_trat_proposto,
	dt_final_trat_proposto,
	cd_estado_doenca,
	ie_estado_pac_fim_trat,
	ie_recem_nato,
	nr_cirurgia,
	'A',
	obter_especialidade_medico2(cd_medico_w),
	nr_seq_pepo,
	'N',
	ds_lista_problemas,
	nm_usuario_p,
	sysdate,
	ie_relev_resumo_alta,
	ie_nivel_atencao_w,
	nr_seq_evol_grav,
	NR_SEQ_ATEND_CONS_PEPA
from	evolucao_paciente
where	cd_evolucao	= nr_seq_evolucao_p;

cd_evolucao_criada_p	:= cd_evolucao_w;
	
commit;
	
if	(ie_copia_texto_p	= 'S') then

        begin

	copia_campo_long_de_para_Java(	'EVOLUCAO_PACIENTE',
				'DS_EVOLUCAO',
				' where cd_evolucao = :cd_evolucao',
				'cd_evolucao='||nr_seq_evolucao_p,
				'EVOLUCAO_PACIENTE',
				'DS_EVOLUCAO', 
				' where cd_evolucao = :cd_evolucao',
				'cd_evolucao='||cd_evolucao_w,
				'L');	
				commit;
                                
        exception
                when others then
                
                copia_campo_long_de_para(	'EVOLUCAO_PACIENTE',
				'DS_EVOLUCAO',
				' where cd_evolucao = :cd_evolucao',
				'cd_evolucao='||nr_seq_evolucao_p,
				'EVOLUCAO_PACIENTE',
				'DS_EVOLUCAO', 
				' where cd_evolucao = :cd_evolucao',
				'cd_evolucao='||cd_evolucao_w);	
                
        end;

end if;

gerar_evolucao_vinculo(nr_seq_evolucao_p, cd_evolucao_w,'DUP');


END Duplicar_evolucao;
/
