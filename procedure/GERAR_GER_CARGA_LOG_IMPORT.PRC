create or replace
procedure gerar_ger_carga_log_import ( 
		nm_usuario_p		ger_carga_log_import.nm_usuario%type,
		nr_seq_carga_p		ger_carga_log_import.nr_seq_carga%type,
		ie_tipo_p			ger_carga_log_import.ie_tipo%type,
		ds_log_p			ger_carga_log_import.ds_log%type,
		nr_linha_arq_p		ger_carga_log_import.nr_linha_arq%type,
		nr_seq_carga_arq_p	ger_carga_log_import.nr_seq_carga_arq%type,
		nr_seq_tipo_carga_arq_p	ger_carga_log_import.nr_seq_tipo_carga_arq%type,
		nr_seq_reg_import_p	ger_carga_log_import.nr_seq_reg_import%type) is

begin
insert into ger_carga_log_import(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	nr_seq_carga,
	ie_tipo,
	ds_log,
	nr_linha_arq,
	nr_seq_carga_arq,
	nr_seq_tipo_carga_arq,
	nr_seq_reg_import)
values(	ger_carga_log_import_seq.nextval,
	sysdate,
	nm_usuario_p,
	nr_seq_carga_p,
	ie_tipo_p,
	ds_log_p,
	nr_linha_arq_p,
	nr_seq_carga_arq_p,
	nr_seq_tipo_carga_arq_p,
	nr_seq_reg_import_p);

commit;
end gerar_ger_carga_log_import;
/