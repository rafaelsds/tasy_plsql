create or replace
procedure HD_EXCLUIR_DIALISE_ARMAZENAR(cd_pessoa_fisica_p	varchar2) is 

nr_seq_dialise_w	number(10);

begin

nr_seq_dialise_w := hd_obter_dialise_atual(cd_pessoa_fisica_p,'NI');


if (nr_seq_dialise_w is not null) then
	delete  hd_dialise
	where	nr_sequencia = nr_seq_dialise_w;
end if;	

commit;

end hd_excluir_dialise_armazenar;
/



