Create or replace
procedure HVC_GERAR_W_CONV_RET_MOVTO(nr_seq_retorno_p	number) is

ds_data_evento_w		varchar2(255);
ds_nome_w		varchar2(255);
cd_procedimento_w	varchar2(255);
ds_procedimento_w	varchar2(255);
nr_fatura_w		varchar2(255);
nr_guia_w		varchar2(255);
vl_pago_w		varchar2(255);
ds_justificativa_w		varchar2(255);
sqlerrm_w		varchar2(255);
qt_aspas_w		number(15,0);
ds_senha_w		varchar2(255);
nr_doc_convenio_w	varchar2(255);

cursor c01 is

Select	substr(obter_valor_campo_separador(ds_conteudo,1, ';'), 1, 255),
	substr(obter_valor_campo_separador(ds_conteudo,4, ';'), 1, 230),
	somente_numero(substr(obter_valor_campo_separador(ds_conteudo,6, ';'), 1, 255)),
	somente_numero(substr(obter_valor_campo_separador(ds_conteudo,7, ';'), 1, 255)),
	substr(obter_valor_campo_separador(ds_conteudo,2, ';'), 1, 250),
	substr(obter_valor_campo_separador(ds_conteudo,5, ';'), 1, 255) ds_senha
from	w_conv_ret_movto
where	nr_seq_retorno = nr_seq_retorno_p;

begin

/*
select	max(instr(ds_conteudo, '"'))
into	qt_aspas_w
from	w_conv_ret_movto
where	nr_seq_retorno = nr_seq_retorno_p;

if	(qt_aspas_w > 0) then
	raise_application_error(-20011, 'Este arquivo possui o caractere " (aspas), aparentemente, o mesmo n�o est� no padr�o se separa��o de ; (ponto e v�rgula)');
end if;
*/

open c01;
loop
fetch c01 into
	ds_data_evento_w,
	ds_procedimento_w,
	nr_guia_w,
	vl_pago_w,
	ds_justificativa_w,
	ds_senha_w;
exit when c01%notfound;
	begin

	if	(nr_guia_w	<> 0) then
		nr_doc_convenio_w	:= to_char(nr_guia_w);
	elsif	(ds_senha_w	is not null) and
		(trim(ds_senha_w) = elimina_caracteres_especiais(ds_senha_w)) then
		nr_doc_convenio_w	:= trim(ds_senha_w);
	else
		nr_doc_convenio_w	:= '0';
	end if;

	if	(nvl(nr_doc_convenio_w,'0')	<> '0') then

		insert	into convenio_retorno_movto (
			nr_sequencia,
			nr_seq_retorno,
			dt_atualizacao,
			nm_usuario,
			dt_execucao,
			ds_item_retorno,
			cd_autorizacao,
			nr_doc_convenio,
			vl_pago,
			ds_complemento,
			nr_conta )
		Values	(convenio_retorno_movto_seq.nextval,
			nr_seq_retorno_p,
			sysdate,
			'TASY',
			sysdate,
			ds_procedimento_w,
			nr_guia_w,
			nr_doc_convenio_w,
			somente_numero(vl_pago_w) /100,
			ds_justificativa_w,
			nr_guia_w);

	end if;
	end;
	
end loop;
close c01;

delete from w_conv_ret_movto 
where nr_seq_retorno  = nr_seq_retorno_p;

commit;

end HVC_GERAR_W_CONV_RET_MOVTO;
/