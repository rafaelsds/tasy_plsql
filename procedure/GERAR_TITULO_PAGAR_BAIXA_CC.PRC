create or replace
procedure gerar_titulo_pagar_baixa_cc(	nr_titulo_p	number,
				nr_sequencia_p	number,
				ie_commit_p	varchar2,
				nm_usuario_p	Varchar2) is 

cd_centro_custo_w			number(10);
cd_conta_contabil_w		varchar2(20);
cd_estabelecimento_w		number(10);
ie_gerar_classif_baixa_w		varchar2(1);
nr_sequencia_w			number(10);
nr_seq_classif_w			number(10);
cd_conta_financ_w			number(10);
pr_baixa_w			number(15,2);
vl_baixa_w			number(15,2);
vl_titulo_classif_w			number(15,2);
vl_titulo_w			number(15,2);
vl_total_cc_w			number(15,2);
vl_desconto_w			number(15,2);
vl_total_juros_cc_w		number(15,2);
vl_total_multa_cc_w		number(15,2);
vl_total_desconto_cc_w		number(15,2);
vl_total_outros_acresc_cc_w	number(15,2);
vl_total_outras_despesas_cc_w titulo_pagar_baixa_cc.vl_outras_despesas%type;
vl_juros_w			number(15,2);
vl_multa_w			number(15,2);
vl_outros_acrescimos_w		number(15,2);
VL_OUTRAS_DESPESAS_W titulo_pagar_baixa_cc.vl_outras_despesas%type;
vl_baixa_cc_w			number(15,2);
vl_juros_cc_w			number(15,2);
vl_multa_cc_w			number(15,2);
vl_desconto_cc_w		number(15,2);
vl_outros_acrescimos_cc_w	number(15,2);
vl_outras_despesas_cc_w	titulo_pagar_baixa_cc.vl_outras_despesas%type;
vl_tit_desconto_w		number(15,2);
vl_pago_cc_w			number(15,2);
vl_pago_w			number(15,2);
vl_total_pago_cc_w		number(15,2);
qtd_w				number(5);
/* Projeto Multimoeda - Vari�veis */
vl_baixa_estrang_w		number(15,2);
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;
cd_moeda_w			number(5);
vl_baixa_estrang_cc_w		number(15,2);
vl_complemento_cc_w		number(15,2);
vl_total_estrang_cc_w		number(15,2);

cursor c01 is
select	a.nr_sequencia nr_seq_classif,
	a.cd_centro_custo,
	a.cd_conta_contabil,
	a.vl_titulo,
	a.vl_desconto,
	a.nr_seq_conta_financ
from	titulo_pagar_classif a
where	a.nr_titulo	= nr_titulo_p
and	not exists(	select	1
			from	titulo_pagar_baixa_cc y
			where	y.nr_titulo	= nr_titulo_p
			and	y.nr_seq_baixa	= nr_sequencia_p)
order by 1;

begin

delete	from titulo_pagar_baixa_cc
where	nr_titulo		= nr_titulo_p
and	nr_seq_baixa	= nr_sequencia_p;

select	nvl(max(vl_titulo),0),
	max(cd_estabelecimento)
into	vl_titulo_w,
	cd_estabelecimento_w
from	titulo_pagar
where	nr_titulo	= nr_titulo_p;

select count(*)
into 	qtd_w
from	titulo_pagar_baixa
where	nr_titulo	= nr_titulo_p
and	nr_sequencia	= nr_sequencia_p;

if (qtd_w > 0) 	then
	select	vl_baixa,
		vl_juros,
		vl_multa,
		vl_descontos,
		vl_outros_acrescimos,
		VL_OUTRAS_DESPESAS,
		vl_pago,
		vl_baixa_estrang,
		vl_cotacao,
		cd_moeda
	into	vl_baixa_w,
		vl_juros_w,
		vl_multa_w,
		vl_tit_desconto_w,
		vl_outros_acrescimos_w,
		VL_OUTRAS_DESPESAS_W,
		vl_pago_w,
		vl_baixa_estrang_w,
		vl_cotacao_w,
		cd_moeda_w
	from	titulo_pagar_baixa
	where	nr_titulo	= nr_titulo_p
	and	nr_sequencia	= nr_sequencia_p;
end if;


select	nvl(max(ie_gerar_classif_baixa),'N')
into	ie_gerar_classif_baixa_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_gerar_classif_baixa_w = 'S') then
	begin
	open C01;
	loop
	fetch C01 into	
		nr_seq_classif_w,
		cd_centro_custo_w,
		cd_conta_contabil_w,
		vl_titulo_classif_w,
		vl_desconto_w,
		cd_conta_financ_w;
	exit when C01%notfound;
		begin

		select	titulo_pagar_baixa_cc_seq.nextval
		into	nr_sequencia_w
		from	dual;

		vl_baixa_cc_w			:= dividir_sem_round(vl_baixa_w , vl_titulo_w) * vl_titulo_classif_w;
		vl_juros_cc_w			:= dividir_sem_round(vl_juros_w , vl_titulo_w) * vl_titulo_classif_w;
		vl_multa_cc_w			:= dividir_sem_round(vl_multa_w , vl_titulo_w) * vl_titulo_classif_w;
		vl_desconto_cc_w		:= dividir_sem_round(vl_tit_desconto_w , vl_titulo_w) * vl_titulo_classif_w;
		vl_outros_acrescimos_cc_w	:= dividir_sem_round(vl_outros_acrescimos_w , vl_titulo_w) * vl_titulo_classif_w;
		vl_outras_despesas_cc_w	:= dividir_sem_round(VL_OUTRAS_DESPESAS_W , vl_titulo_w) * vl_titulo_classif_w;
		vl_pago_cc_w			:= dividir_sem_round(vl_pago_w , vl_titulo_w) * vl_titulo_classif_w;
		/* Projeto Multimoeda - realiza o rateio do valor da baixa em moeda estrangeira, caso exista*/
		if (nvl(vl_baixa_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
			vl_baixa_estrang_cc_w	:= dividir_sem_round(vl_baixa_estrang_w, (vl_titulo_w / vl_cotacao_w)) * (vl_titulo_classif_w / vl_cotacao_w);
			vl_complemento_cc_w	:= vl_baixa_cc_w - vl_baixa_estrang_cc_w;
		else
			vl_baixa_estrang_cc_w	:= null;
			vl_complemento_cc_w	:= null;
			vl_cotacao_w		:= null;
			cd_moeda_w		:= null;
		end if;
		
		insert into titulo_pagar_baixa_cc( 
			nr_sequencia,
			nr_titulo,
			nr_seq_baixa, 
			cd_centro_custo,
			cd_conta_contabil,
			vl_baixa, 
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec, 
			nm_usuario_nrec,
			cd_conta_financ,
			vl_juros,
			vl_multa,
			vl_desconto,
			vl_outros_acrescimos,
			VL_OUTRAS_DESPESAS,
			vl_pago,
			nr_seq_classif,
			vl_baixa_estrang,
			vl_complemento,
			vl_cotacao,
			cd_moeda)
		values(	nr_sequencia_w,
			nr_titulo_p,
			nr_sequencia_p, 
			cd_centro_custo_w,
			cd_conta_contabil_w,
			vl_baixa_cc_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_conta_financ_w,
			vl_juros_cc_w,
			vl_multa_cc_w,
			vl_desconto_cc_w,
			vl_outros_acrescimos_cc_w,
			vl_outras_despesas_cc_w,
			vl_pago_cc_w,
			nr_seq_classif_w,
			vl_baixa_estrang_cc_w,
			vl_complemento_cc_w,
			vl_cotacao_w,
			cd_moeda_w);
		end;
	end loop;
	close C01;

	select	nvl(sum(vl_baixa), 0),
		nvl(sum(vl_juros), 0),
		nvl(sum(vl_multa), 0),
		nvl(sum(vl_desconto), 0),
		nvl(sum(vl_outros_acrescimos), 0),
		nvl(sum(VL_OUTRAS_DESPESAS), 0),
		nvl(sum(vl_pago), 0),
		nvl(sum(vl_baixa_estrang), 0)
	into	vl_total_cc_w,
		vl_total_juros_cc_w,
		vl_total_multa_cc_w,
		vl_total_desconto_cc_w,
		vl_total_outros_acresc_cc_w,
		vl_total_outras_despesas_cc_w,
		vl_total_pago_cc_w,
		vl_total_estrang_cc_w
	from	titulo_pagar_baixa_cc
	where	nr_titulo	= nr_titulo_p
	and	nr_seq_baixa	= nr_sequencia_p;

	if	(vl_total_cc_w <> vl_baixa_w) or
		(vl_total_juros_cc_w <> vl_juros_w) or
		(vl_total_multa_cc_w <> vl_multa_w) or
		(vl_total_desconto_cc_w <> vl_tit_desconto_w) or
		(vl_total_outros_acresc_cc_w <> vl_outros_acrescimos_w) or
		(vl_total_outras_despesas_cc_w <> VL_OUTRAS_DESPESAS_W) or
		(vl_total_pago_cc_w <> vl_pago_w) then
		update	titulo_pagar_baixa_cc
		set	vl_baixa		= vl_baixa + vl_baixa_w - vl_total_cc_w,
			vl_juros		= vl_juros + vl_juros_w - vl_total_juros_cc_w,
			vl_multa		= vl_multa + vl_multa_w - vl_total_multa_cc_w,
			vl_desconto		= vl_desconto + vl_tit_desconto_w - vl_total_desconto_cc_w,
			vl_outros_acrescimos	= vl_outros_acrescimos + vl_outros_acrescimos_w - vl_total_outros_acresc_cc_w,
			VL_OUTRAS_DESPESAS	= VL_OUTRAS_DESPESAS + VL_OUTRAS_DESPESAS_W - vl_total_outras_despesas_cc_w,
			vl_pago			= vl_pago + vl_pago_w - vl_total_pago_cc_w
		where	nr_sequencia	= nr_sequencia_w;
	end if;
	
	/* Projeto Multimoeda - verifica diferen�a entre a soma dos centros de custo e o valor da baixa do t�tulo em moeda estrangeira - mesmo processo executado em moeda nacional*/
	if (vl_total_estrang_cc_w <> vl_baixa_estrang_w) then
		update	titulo_pagar_baixa_cc
		set	vl_baixa_estrang	= vl_baixa_estrang + vl_baixa_estrang_w - vl_total_estrang_cc_w,
			vl_complemento		= vl_baixa - (vl_baixa_estrang + vl_baixa_estrang_w - vl_total_estrang_cc_w)
		where	nr_sequencia	= nr_sequencia_w;
	end if;

	if	(ie_commit_p = 'S') then
		commit;
	end if;
	end;
	
	gerar_titulo_pagar_baixa_ops(	nr_titulo_p,
					nr_sequencia_p,
					wheb_usuario_pck.get_cd_estabelecimento,
					nm_usuario_p);
end if;

end gerar_titulo_pagar_baixa_cc;
/
