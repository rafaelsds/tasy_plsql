create or replace
procedure pls_obter_eventos_lote_fat(	nr_seq_lote_p		pls_lote_faturamento.nr_sequencia%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
					dt_geracao_p		date,
					nm_usuario_p		usuario.nm_usuario%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_tipo_conta_w				pls_conta.ie_tipo_conta%type;
ie_impedimento_cobranca_w		varchar2(20);
ie_contas_faturar_w			pls_parametro_faturamento.ie_contas_faturar%type;
ie_data_fat_ref_w			pls_parametro_faturamento.ie_data_fat_ref%type;
ie_pos_estab_faturamento_w		pls_parametros.ie_pos_estab_faturamento%type;
dt_geracao_lote_w			pls_lote_faturamento.dt_geracao%type;
nr_seq_segurado_w			pls_conta.nr_seq_segurado%type;
nr_seq_conta_w				pls_conta.nr_sequencia%type;
nr_seq_congenere_w			pls_segurado.nr_seq_congenere%type;
dt_comp_inicial_lote_w			pls_lote_faturamento.dt_inicio_referencia%type;
dt_comp_final_lote_w			pls_lote_faturamento.dt_fim_referencia%type;
dt_fechamento_inicial_lote_w		pls_lote_faturamento.dt_inicial_fechamento%type;
dt_fechamento_final_lote_w		pls_lote_faturamento.dt_final_fechamento%type;
ie_apresentacao_prot_w			pls_lote_faturamento.ie_apresentacao_prot%type;
dt_emissao_w				pls_conta.dt_emissao%type;
ie_tipo_guia_w				pls_conta.ie_tipo_guia%type;
cd_guia_ok_w				pls_conta.cd_guia_ok%type;
dt_alta_w				pls_conta.dt_alta%type;
nr_seq_protocolo_w			pls_lote_faturamento.nr_seq_protocolo%type;
nr_seq_contrato_w			pls_segurado.nr_seq_contrato%type;
nr_seq_intercambio_w			pls_segurado.nr_seq_intercambio%type;
qt_registros_w				pls_integer;
--------------------------------------------------------------------------------------------------------------------------------------------------------------
qt_registros_conta_filtro_w		pls_integer;
qt_registros_contr_filtro_w		pls_integer;
qt_registros_congen_filtro_w		pls_integer;
qt_registros_inter_filtro_w		pls_integer;
qt_registros_prot_filtro_w		pls_integer;
qt_registros_guia_ref_w			pls_integer;
--------------------------------------------------------------------------------------------------------------------------------------------------------------
ds_sql_contas_faturamento_w		varchar2(30000);
ds_filtro_contas_faturamento_w		varchar2(10000);
ds_filtro_contas_fat_sus_w		varchar2(2000);
var_cur_w				pls_integer;
var_exec_w				pls_integer;
var_retorno_w				pls_integer;
qt_regra_lib_fat_w			pls_integer;
ie_exige_med_aud_w			pls_regra_lib_fat.ie_exige_med_aud%type;
ie_exige_enf_aud_w			pls_regra_lib_fat.ie_exige_enf_aud%type;
dt_atendimento_w			pls_conta.dt_atendimento%type;
ie_conta_fechada_w			pls_regra_faturamento.ie_conta_fechada%type;
dt_internacao_w				pls_conta.dt_entrada%type;
cursor_w				sql_pck.t_cursor;
valor_bind_w				sql_pck.t_dado_bind;
ie_prestador_a400_w			varchar2(1);
ie_fatura_encerrada_w			pls_parametro_faturamento.ie_fatura_encerrada%type;
ie_origem_conta_w			pls_conta.ie_origem_conta%type;
ie_gerar_fat_w				varchar2(1) := 'S';
ie_status_fatura_w			ptu_fatura.ie_status%type;
ie_tipo_protocolo_w			pls_protocolo_conta.ie_tipo_protocolo%type;
nr_seq_congenere_prot_w			pls_protocolo_conta.nr_seq_congenere%type;
ie_tipo_congenere_ops_w			pls_congenere.ie_tipo_congenere%type;
ie_tipo_congenere_w			pls_congenere.ie_tipo_congenere%type;
ie_tipo_congenere_prot_w		pls_congenere.ie_tipo_congenere%type;
ie_tipo_congenere_ref_w			pls_congenere.ie_tipo_congenere%type;
ie_origem_protocolo_w			pls_protocolo_conta.ie_origem_protocolo%type;
qt_reg_conta_sus_filtro_w		pls_processo_conta.nr_sequencia%type;
ie_tipo_lote_w				pls_lote_faturamento.ie_tipo_lote%type;

begin

if	(nr_seq_lote_p is not null) then

	select	nvl(max(ie_pos_estab_faturamento), 'N')
	into	ie_pos_estab_faturamento_w
	from	pls_parametros	a
	where	a.cd_estabelecimento = cd_estabelecimento_p;

	select	count(1)
	into	qt_regra_lib_fat_w
	from	pls_regra_lib_fat a
	where	a.cd_estabelecimento = cd_estabelecimento_p;

	select	nvl(max(a.ie_contas_faturar), 'PF'),
		nvl(max(a.ie_data_fat_ref),'DA'),
		nvl(max(a.ie_fatura_encerrada),'N')
	into	ie_contas_faturar_w,
		ie_data_fat_ref_w,
		ie_fatura_encerrada_w
	from	pls_parametro_faturamento a
	where	a.cd_estabelecimento = cd_estabelecimento_p;

	-- Limpar tabela temporaria
	delete	w_pls_lote_fat_item
	where	nr_seq_lote = nr_seq_lote_p
	and	nm_usuario = nm_usuario_p;

	select	trunc(a.dt_inicio_referencia, 'mm'),
		trunc(last_day(a.dt_fim_referencia), 'dd') + 86399/86400,
		trunc(a.dt_inicial_fechamento),
		fim_dia(a.dt_final_fechamento),
		trunc(sysdate, 'dd'),
		a.nr_seq_protocolo,
		nvl(a.ie_apresentacao_prot, 'T'),
		nvl(b.ie_conta_fechada,'N'),
		nvl(a.ie_tipo_lote, 'C')
	into	dt_comp_inicial_lote_w,
		dt_comp_final_lote_w,
		dt_fechamento_inicial_lote_w,
		dt_fechamento_final_lote_w,
		dt_geracao_lote_w,
		nr_seq_protocolo_w,
		ie_apresentacao_prot_w,
		ie_conta_fechada_w,
		ie_tipo_lote_w
	from	pls_lote_faturamento a,
		pls_regra_faturamento b
	where	b.nr_sequencia	= a.nr_seq_regra_fat
	and	a.nr_sequencia	= nr_seq_lote_p;
	
	select	count(1)
	into	qt_registros_w
	from	pls_lote_fat_lib
	where	nr_seq_lote_fat = nr_seq_lote_p;
	
	--Busca as regras do adicionais do lote de faturamento
	select	count(nr_seq_conta),
		count(nr_seq_contrato),
		count(nr_seq_cooperativa),
		count(nr_seq_intercambio),
		count(nr_seq_protocolo),
		count(cd_guia_referencia),
		count(nr_seq_conta_sus)
	into	qt_registros_conta_filtro_w,
		qt_registros_contr_filtro_w,
		qt_registros_congen_filtro_w,
		qt_registros_inter_filtro_w,
		qt_registros_prot_filtro_w,
		qt_registros_guia_ref_w,
		qt_reg_conta_sus_filtro_w
	from	pls_lote_fat_adic
	where	nr_seq_lote = nr_seq_lote_p;
	
	-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Faturamento ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	--Faz as restricoes para o sql_dinamico
	if	(ie_pos_estab_faturamento_w = 'S') then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and ((b.ie_tipo_conta in (''O'', ''C'', ''I'')) or 
		(a.ie_tipo_protocolo = ''R'') or (pls_obter_se_benef_remido(c.nr_sequencia, b.dt_atendimento_referencia) = ''S'')) ' || pls_util_pck.enter_w;
		
	elsif	(ie_pos_estab_faturamento_w = 'N') then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and ((b.ie_tipo_conta = ''C'') or 
		(a.ie_tipo_protocolo = ''R'') or (pls_obter_se_benef_remido(c.nr_sequencia, b.dt_atendimento_referencia) = ''S'')) ' || pls_util_pck.enter_w;
		
	elsif	(ie_pos_estab_faturamento_w = 'R') then
		-- o estabelecimento sempre e passado como parametro logo abaixo onde o select e montado
		if	(ie_tipo_lote_w != 'A') then
			ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and ((pls_obter_forma_cobr_pos_estab(:cd_estabelecimento,nvl(b.ie_tipo_segurado_cta,c.ie_tipo_segurado)) = ''F'') or 
				(a.ie_tipo_protocolo = ''R'') or (pls_obter_se_benef_remido(c.nr_sequencia, b.dt_atendimento_referencia) = ''S'')) ' || pls_util_pck.enter_w;
		else
			ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and ((pls_obter_forma_cobr_pos_estab(:cd_estabelecimento,nvl(b.ie_tipo_segurado,c.ie_tipo_segurado)) = ''F'') or 
				(a.ie_tipo_protocolo = ''R'') or (pls_obter_se_benef_remido(c.nr_sequencia, b.dt_atendimento_referencia) = ''S'')) ' || pls_util_pck.enter_w;
		end if;
	end if;
	
	if	(dt_comp_inicial_lote_w is not null) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and a.dt_mes_competencia >= :dt_comp_inicial_lote ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':dt_comp_inicial_lote', dt_comp_inicial_lote_w, valor_bind_w);
	-- Melhora de performance
	elsif	(dt_comp_inicial_lote_w is null) and (qt_registros_conta_filtro_w = 0 ) and (qt_registros_contr_filtro_w = 0 ) and (qt_registros_congen_filtro_w = 0) and (qt_registros_inter_filtro_w = 0) and
		(qt_registros_prot_filtro_w = 0) and (qt_registros_guia_ref_w = 0) then
		dt_comp_inicial_lote_w	:= sysdate - 1095;
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and a.dt_mes_competencia >= :dt_comp_inicial_lote ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':dt_comp_inicial_lote', dt_comp_inicial_lote_w, valor_bind_w);
	end if;
	
	if	(dt_comp_final_lote_w is not null) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and a.dt_mes_competencia <= :dt_comp_final_lote ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':dt_comp_final_lote', dt_comp_final_lote_w, valor_bind_w, sql_pck.b_data_hora);
	-- Melhora de performance
	elsif	(dt_comp_final_lote_w is null) and (qt_registros_conta_filtro_w = 0 ) and (qt_registros_contr_filtro_w = 0 ) and (qt_registros_congen_filtro_w = 0) and (qt_registros_inter_filtro_w = 0) and
		(qt_registros_prot_filtro_w = 0) and (qt_registros_guia_ref_w = 0) then
		dt_comp_final_lote_w	:= sysdate;
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and a.dt_mes_competencia <= :dt_comp_final_lote ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':dt_comp_final_lote', dt_comp_final_lote_w, valor_bind_w, sql_pck.b_data_hora);
	end if;
	
	if	(ie_data_fat_ref_w = 'DA') then
		if	(dt_fechamento_inicial_lote_w is not null) then
			ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and b.dt_atualizacao >= :dt_fechamento_inicial_lote ' || pls_util_pck.enter_w;
			sql_pck.bind_variable(':dt_fechamento_inicial_lote', dt_fechamento_inicial_lote_w, valor_bind_w);
		end if;
		
		if	(dt_fechamento_final_lote_w is not null) then
			ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and b.dt_atualizacao <= :dt_fechamento_final_lote ' || pls_util_pck.enter_w;
			sql_pck.bind_variable(':dt_fechamento_final_lote', dt_fechamento_final_lote_w, valor_bind_w, sql_pck.b_data_hora);
		end if;
		
	elsif	(ie_data_fat_ref_w = 'DF') then
		if	(dt_fechamento_inicial_lote_w is not null) then
			ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and b.dt_fechamento_conta >= :dt_fechamento_inicial_lote ' || pls_util_pck.enter_w;
			sql_pck.bind_variable(':dt_fechamento_inicial_lote', dt_fechamento_inicial_lote_w, valor_bind_w);
		end if;
		
		if	(dt_fechamento_final_lote_w is not null) then
			ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and b.dt_fechamento_conta <= :dt_fechamento_final_lote ' || pls_util_pck.enter_w;
			sql_pck.bind_variable(':dt_fechamento_final_lote', dt_fechamento_final_lote_w, valor_bind_w, sql_pck.b_data_hora);
		end if;
		
	elsif	(ie_data_fat_ref_w = 'DFA') then
		if	(dt_fechamento_inicial_lote_w is not null) then
			ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and exists(	select	1 ' || pls_util_pck.enter_w ||
												'		from	pls_conta_pos_estabelecido pos ' || pls_util_pck.enter_w ||
												'		where	pos.nr_seq_conta = b.nr_sequencia ' || pls_util_pck.enter_w ||
												'		and	exists(	select	1 ' || pls_util_pck.enter_w ||
												'				from	pls_analise_conta analise ' || pls_util_pck.enter_w ||
												'				where	analise.nr_sequencia = pos.nr_seq_analise ' || pls_util_pck.enter_w ||
												'				and	fim_dia(analise.dt_final_analise) >= :dt_fechamento_inicial_lote ' || pls_util_pck.enter_w ||
												'				union all ' || pls_util_pck.enter_w ||
												'				select	1 ' || pls_util_pck.enter_w ||
												'				from	pls_conta		x, ' || pls_util_pck.enter_w ||
												'					pls_protocolo_conta	y ' || pls_util_pck.enter_w ||
												'				where	y.nr_sequencia		= x.nr_seq_protocolo ' || pls_util_pck.enter_w ||
												'				and	x.nr_sequencia		= pos.nr_seq_conta ' || pls_util_pck.enter_w ||
												'				and	Y.ie_tipo_protocolo	= ''R'' ' || pls_util_pck.enter_w ||
												'				and	fim_dia(x.dt_fechamento_conta)	>= :dt_fechamento_inicial_lote)) ' || pls_util_pck.enter_w;
			sql_pck.bind_variable(':dt_fechamento_inicial_lote', dt_fechamento_inicial_lote_w, valor_bind_w);
		end if;
		
		if	(dt_fechamento_final_lote_w is not null) then
			ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and exists(	select	1' || pls_util_pck.enter_w ||
												'		from	pls_conta_pos_estabelecido pos' || pls_util_pck.enter_w ||
												'		where	pos.nr_seq_conta = b.nr_sequencia' || pls_util_pck.enter_w ||
												'		and	exists(	select	1' || pls_util_pck.enter_w ||
												'				from	pls_analise_conta analise' || pls_util_pck.enter_w ||
												'				where	analise.nr_sequencia = pos.nr_seq_analise' || pls_util_pck.enter_w ||
												'				and	trunc(analise.dt_final_analise) <= :dt_fechamento_final_lote' || pls_util_pck.enter_w ||
												'				union all ' || pls_util_pck.enter_w ||
												'				select	1 ' || pls_util_pck.enter_w ||
												'				from	pls_conta		x, ' || pls_util_pck.enter_w ||
												'					pls_protocolo_conta	y ' || pls_util_pck.enter_w ||
												'				where	y.nr_sequencia		= x.nr_seq_protocolo ' || pls_util_pck.enter_w ||
												'				and	x.nr_sequencia		= pos.nr_seq_conta ' || pls_util_pck.enter_w ||
												'				and	Y.ie_tipo_protocolo	= ''R'' ' || pls_util_pck.enter_w ||
												'				and	fim_dia(x.dt_fechamento_conta)	 <= :dt_fechamento_final_lote)) ' || pls_util_pck.enter_w;
			sql_pck.bind_variable(':dt_fechamento_final_lote', dt_fechamento_final_lote_w, valor_bind_w, sql_pck.b_data_hora);
		end if;
	end if;

	-- 3 Liberado para pagamento / 4 Encerrado sem pagamento / 6 Pago / 7 Encerrado A700
	if	(ie_contas_faturar_w <> 'CF') and (ie_tipo_lote_w != 'A') then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and a.ie_status in (''3'', ''4'', ''6'', ''7'') ' || pls_util_pck.enter_w;
	end if;
	
	if	(nr_seq_protocolo_w is not null) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and a.nr_sequencia = :nr_seq_protocolo ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':nr_seq_protocolo', nr_seq_protocolo_w, valor_bind_w);
	end if;
	
	if	(ie_apresentacao_prot_w <> 'T') then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || ' and a.ie_apresentacao = :ie_apresentacao_prot ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':ie_apresentacao_prot', ie_apresentacao_prot_w, valor_bind_w);
	end if;

	if	(qt_registros_w	> 0) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w ||	' and  exists(	select 	 1' || pls_util_pck.enter_w ||
											'		from	pls_lib_fat_conta y, ' || pls_util_pck.enter_w ||
											'			pls_lib_faturamento x, ' || pls_util_pck.enter_w ||
											'			pls_lote_fat_lib z ' || pls_util_pck.enter_w ||
											'		where	y.nr_seq_conta = b.nr_sequencia' || pls_util_pck.enter_w ||
											'		and	x.nr_sequencia	= y.nr_seq_lib_fat ' || pls_util_pck.enter_w ||
											'		and	z.nr_seq_lib_fat = x.nr_sequencia ' || pls_util_pck.enter_w ||
											'		and	z.nr_seq_lote_fat = :nr_seq_lote ' || pls_util_pck.enter_w ||
											'		and	y.ie_status  in (''LS'',''LU'')) ' || pls_util_pck.enter_w;
	end if;
	
	if	(qt_registros_conta_filtro_w > 0) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || 	'  and  exists(	select 	1 ' || pls_util_pck.enter_w ||
											'		from	pls_lote_fat_adic y ' || pls_util_pck.enter_w ||
											'		where	y.nr_seq_conta = b.nr_sequencia' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_lote = :nr_seq_lote ' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_conta is not null) ' || pls_util_pck.enter_w;
	end if;
	
	if	(qt_registros_contr_filtro_w > 0) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || 	'  and  exists(	select	1' || pls_util_pck.enter_w ||
											'		from	pls_lote_fat_adic y ' || pls_util_pck.enter_w ||
											'		where	y.nr_seq_lote = :nr_seq_lote ' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_contrato = c.nr_seq_contrato ' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_contrato is not null) ' || pls_util_pck.enter_w;
	end if;

	if	(qt_registros_congen_filtro_w > 0) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || 	'  and	exists(	select 	1 ' || pls_util_pck.enter_w ||
											'		from	pls_lote_fat_adic y ' || pls_util_pck.enter_w ||
											'		where	y.nr_seq_lote = :nr_seq_lote ' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_cooperativa = c.nr_seq_ops_congenere ' || pls_util_pck.enter_w ||
											'		union all ' || pls_util_pck.enter_w ||
											'		select	1 ' || pls_util_pck.enter_w ||
											'		from	pls_lote_fat_adic y ' || pls_util_pck.enter_w ||
											'		where	y.nr_seq_lote = :nr_seq_lote ' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_cooperativa = c.nr_seq_congenere) ' || pls_util_pck.enter_w;
	end if;
	
	if	(qt_registros_inter_filtro_w > 0) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || 	'  and  exists(	select	1' || pls_util_pck.enter_w ||
											'		from	pls_lote_fat_adic y ' || pls_util_pck.enter_w ||
											'		where	y.nr_seq_lote = :nr_seq_lote ' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_intercambio = c.nr_seq_intercambio' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_intercambio is not null) ' || pls_util_pck.enter_w;
	end if;
	
	if	(qt_registros_prot_filtro_w > 0) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || 	'  and	exists(	select	1' || pls_util_pck.enter_w ||
											'		from	pls_lote_fat_adic y ' || pls_util_pck.enter_w ||
											'		where	y.nr_seq_lote = :nr_seq_lote ' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_protocolo = a.nr_sequencia ' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_protocolo is not null) ' || pls_util_pck.enter_w;
	end if;

	if	(qt_registros_guia_ref_w > 0) then
		ds_filtro_contas_faturamento_w	:= ds_filtro_contas_faturamento_w || 	'  and	exists(	select	1' || pls_util_pck.enter_w ||
											'		from	pls_lote_fat_adic y ' || pls_util_pck.enter_w ||
											'		where	y.nr_seq_lote = :nr_seq_lote ' || pls_util_pck.enter_w ||
											'		and	y.cd_guia_referencia = b.cd_guia_referencia ' || pls_util_pck.enter_w ||
											'		and	y.cd_guia_referencia is not null) ' || pls_util_pck.enter_w;
	end if;
	
	-- ***************************************************************************** RESSARCIMENTO SUS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	if	(dt_comp_inicial_lote_w is not null) then
		ds_filtro_contas_fat_sus_w	:= ds_filtro_contas_fat_sus_w || 'and a.dt_internacao >= :dt_comp_inicial_lote ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':dt_comp_inicial_lote', dt_comp_inicial_lote_w, valor_bind_w);
	-- Melhora de performance
	elsif	(dt_comp_inicial_lote_w is null) and (qt_registros_conta_filtro_w = 0 ) and (qt_registros_contr_filtro_w = 0 ) and (qt_registros_congen_filtro_w = 0) and (qt_registros_inter_filtro_w = 0) and
		(qt_registros_prot_filtro_w = 0) and (qt_registros_guia_ref_w = 0) and (qt_reg_conta_sus_filtro_w = 0) then
		dt_comp_inicial_lote_w	:= sysdate - 1095;
		ds_filtro_contas_fat_sus_w	:= ds_filtro_contas_fat_sus_w || 'and a.dt_internacao >= :dt_comp_inicial_lote ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':dt_comp_inicial_lote', dt_comp_inicial_lote_w, valor_bind_w);
	end if;
	
	if	(dt_comp_final_lote_w is not null) then
		ds_filtro_contas_fat_sus_w	:= ds_filtro_contas_fat_sus_w || ' and a.dt_internacao <= :dt_comp_final_lote ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':dt_comp_final_lote', dt_comp_final_lote_w, valor_bind_w, sql_pck.b_data_hora);
	-- Melhora de performance
	elsif	(dt_comp_final_lote_w is null) and (qt_registros_conta_filtro_w = 0 ) and (qt_registros_contr_filtro_w = 0 ) and (qt_registros_congen_filtro_w = 0) and (qt_registros_inter_filtro_w = 0) and
		(qt_registros_prot_filtro_w = 0) and (qt_registros_guia_ref_w = 0) and (qt_reg_conta_sus_filtro_w = 0) then
		dt_comp_final_lote_w	:= sysdate;
		ds_filtro_contas_fat_sus_w	:= ds_filtro_contas_fat_sus_w || ' and a.dt_internacao <= :dt_comp_final_lote ' || pls_util_pck.enter_w;
		sql_pck.bind_variable(':dt_comp_final_lote', dt_comp_final_lote_w, valor_bind_w, sql_pck.b_data_hora);
	end if;
	
	
	if	(qt_registros_w > 0) or
		(qt_registros_conta_filtro_w > 0) or 
		(qt_registros_contr_filtro_w > 0) or
		(qt_registros_congen_filtro_w > 0) or
		(qt_registros_inter_filtro_w > 0) or
		(qt_registros_prot_filtro_w > 0) or 
		(qt_registros_guia_ref_w > 0) or
		(qt_reg_conta_sus_filtro_w > 0)  then
		ds_filtro_contas_fat_sus_w	:= ds_filtro_contas_fat_sus_w || 	'  and  exists(	select 	1 ' || pls_util_pck.enter_w ||
											'		from	pls_lote_fat_adic y ' || pls_util_pck.enter_w ||
											'		where	y.nr_seq_conta_sus	= a.nr_sequencia' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_lote 		= :nr_seq_lote ' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_conta_sus is not null' || pls_util_pck.enter_w ||
											'		and	y.nr_seq_conta is null) ' || pls_util_pck.enter_w;
	end if;
	
	if	(qt_registros_w > 0) or
		(qt_registros_conta_filtro_w > 0) or 
		(qt_registros_contr_filtro_w > 0) or
		(qt_registros_congen_filtro_w > 0) or
		(qt_registros_inter_filtro_w > 0) or
		(qt_registros_prot_filtro_w > 0) or 
		(qt_registros_guia_ref_w > 0) or
		(qt_reg_conta_sus_filtro_w > 0) then

		sql_pck.bind_variable(':nr_seq_lote', nr_seq_lote_p, valor_bind_w);
	end if;

	ds_sql_contas_faturamento_w :=	'select * ' || pls_util_pck.enter_w ||
					'from ( ' || pls_util_pck.enter_w;

					if	(ie_tipo_lote_w != 'A') then

						ds_sql_contas_faturamento_w := ds_sql_contas_faturamento_w||
						'select	b.nr_sequencia, ' || pls_util_pck.enter_w ||
						'	b.nr_seq_segurado, ' || pls_util_pck.enter_w ||
						'	b.dt_emissao, ' || pls_util_pck.enter_w ||
						'	nvl(c.nr_seq_ops_congenere, c.nr_seq_congenere), ' || pls_util_pck.enter_w ||
						'	b.ie_tipo_conta, ' || pls_util_pck.enter_w ||
						'	nvl(b.dt_atendimento,b.dt_atendimento_referencia), ' || pls_util_pck.enter_w ||-- Utilizar a data de atendimento da PLS_CONTA e nao da CONTA_CABECALHO - Conversa em conexao OS 744647
						'	b.ie_tipo_guia, ' || pls_util_pck.enter_w ||
						'	nvl(b.dt_inicio_faturamento,b.dt_entrada), ' || pls_util_pck.enter_w ||
						'	b.cd_guia_ok, ' || pls_util_pck.enter_w ||
						'	pls_obter_se_prest_a400(b.nr_sequencia, :cd_estabelecimento) ie_prestador_a400, ' || pls_util_pck.enter_w ||
						'	b.ie_origem_conta, ' || pls_util_pck.enter_w ||
						'	a.ie_tipo_protocolo, ' || pls_util_pck.enter_w ||
						'	a.nr_seq_congenere, ' || pls_util_pck.enter_w ||
						'	(select max(x.ie_tipo_congenere) from pls_congenere x where x.nr_sequencia = c.nr_seq_ops_congenere) ie_tipo_congenere_ops, ' || pls_util_pck.enter_w ||
						'	(select max(x.ie_tipo_congenere) from pls_congenere x where x.nr_sequencia = c.nr_seq_congenere) ie_tipo_congenere, ' || pls_util_pck.enter_w ||
						'	(select max(x.ie_tipo_congenere) from pls_congenere x where x.nr_sequencia = a.nr_seq_congenere) ie_tipo_congenere_prot, ' || pls_util_pck.enter_w ||
						'	a.ie_origem_protocolo ' || pls_util_pck.enter_w ||
						'from	pls_segurado c, ' || pls_util_pck.enter_w ||
						'	pls_conta_pos_cab_v b, ' || pls_util_pck.enter_w ||
						'	pls_protocolo_conta a ' || pls_util_pck.enter_w ||
						'where	a.nr_sequencia = b.nr_seq_protocolo ' || pls_util_pck.enter_w ||
						'and	c.nr_sequencia = b.nr_seq_segurado ' || pls_util_pck.enter_w ||
						'and	b.ie_status_fat	= ''L'' ' || pls_util_pck.enter_w ||
						'and	b.ie_status = ''F'' ' || pls_util_pck.enter_w ||
						'and	a.ie_situacao in (''D'', ''T'') ' || pls_util_pck.enter_w ||
						'and	exists(	select	1 ' || pls_util_pck.enter_w ||
						'		from	pls_conta_pos_estabelecido x ' || pls_util_pck.enter_w ||
						'		where	b.nr_sequencia = x.nr_seq_conta '|| pls_util_pck.enter_w ||
						'		and	x.ie_status_faturamento	= ''L'' ' || pls_util_pck.enter_w ||
						'		and	x.ie_situacao = ''A'' ' || pls_util_pck.enter_w ||
						'		and	x.nr_seq_evento_fat is null ' || pls_util_pck.enter_w ||
						'		and	x.nr_seq_lote_fat is null ' || pls_util_pck.enter_w ||
						'		and not exists(	select	1 '|| pls_util_pck.enter_w ||
						'				from	pls_analise_conta y '|| pls_util_pck.enter_w ||
						'				where	y.ie_status in (''A'', ''D'', ''G'', ''J'', ''L'', ''P'', ''X'')'|| pls_util_pck.enter_w ||
						'				and	y.nr_sequencia = x.nr_seq_analise ))'|| pls_util_pck.enter_w ||
						'and not exists(	select	1 ' || pls_util_pck.enter_w ||
						'			from	pls_conta x ' || pls_util_pck.enter_w ||
						'			where	x.nr_seq_conta_referencia = b.nr_sequencia ' || pls_util_pck.enter_w ||
						'			and	x.nr_seq_ajuste_fat is not null) ' || pls_util_pck.enter_w ||
						'and not exists(	select	1 ' || pls_util_pck.enter_w ||
						'			from	pls_lote_protocolo_conta x ' || pls_util_pck.enter_w ||
						'			where	x.nr_sequencia = a.nr_seq_lote_conta ' || pls_util_pck.enter_w ||
						'			and	x.ie_status = ''X'') ' || pls_util_pck.enter_w;
					else	-- aviso
						ds_sql_contas_faturamento_w := ds_sql_contas_faturamento_w||
						'select	b.nr_sequencia, ' || pls_util_pck.enter_w ||
						'	b.nr_seq_segurado, ' || pls_util_pck.enter_w ||
						'	b.dt_emissao, ' || pls_util_pck.enter_w ||
						'	nvl(c.nr_seq_ops_congenere, c.nr_seq_congenere), ' || pls_util_pck.enter_w ||
						'	b.ie_tipo_conta, ' || pls_util_pck.enter_w ||
						'	b.dt_atendimento, ' || pls_util_pck.enter_w ||-- Utilizar a data de atendimento da PLS_CONTA e nao da CONTA_CABECALHO - Conversa em conexao OS 744647
						'	b.ie_tipo_guia, ' || pls_util_pck.enter_w ||
						'	nvl(b.dt_inicio_faturamento,b.dt_entrada), ' || pls_util_pck.enter_w ||
						'	b.cd_guia_referencia cd_guia_ok, ' || pls_util_pck.enter_w ||
						'	pls_obter_se_prest_a400(b.nr_sequencia, :cd_estabelecimento) ie_prestador_a400, ' || pls_util_pck.enter_w ||
						'	b.ie_origem_conta, ' || pls_util_pck.enter_w ||
						'	a.ie_tipo_protocolo, ' || pls_util_pck.enter_w ||
						'	a.nr_seq_congenere, ' || pls_util_pck.enter_w ||
						'	(select max(x.ie_tipo_congenere) from pls_congenere x where x.nr_sequencia = c.nr_seq_ops_congenere) ie_tipo_congenere_ops, ' || pls_util_pck.enter_w ||
						'	(select max(x.ie_tipo_congenere) from pls_congenere x where x.nr_sequencia = c.nr_seq_congenere) ie_tipo_congenere, ' || pls_util_pck.enter_w ||
						'	(select max(x.ie_tipo_congenere) from pls_congenere x where x.nr_sequencia = a.nr_seq_congenere) ie_tipo_congenere_prot, ' || pls_util_pck.enter_w ||
						'	a.ie_origem_protocolo ' || pls_util_pck.enter_w ||
						'from	pls_segurado c, ' || pls_util_pck.enter_w ||
						'	pls_conta b, ' || pls_util_pck.enter_w ||
						'	pls_protocolo_conta a ' || pls_util_pck.enter_w ||
						'where	a.nr_sequencia = b.nr_seq_protocolo ' || pls_util_pck.enter_w ||
						'and	c.nr_sequencia = b.nr_seq_segurado ' || pls_util_pck.enter_w ||						
						'and	a.ie_situacao in (''D'', ''T'') ' || pls_util_pck.enter_w ||
						'and	exists(	select	1 ' || pls_util_pck.enter_w ||
						'		from	pls_conta_pos_estabelecido x ' || pls_util_pck.enter_w ||
						'		where	b.nr_sequencia = x.nr_seq_conta '|| pls_util_pck.enter_w ||
						'		and	x.ie_status_faturamento	= ''A'' ' || pls_util_pck.enter_w ||
						'		and	x.ie_situacao = ''A'' ' || pls_util_pck.enter_w ||
						'		and	x.nr_seq_evento_fat is null ' || pls_util_pck.enter_w ||
						'		and	x.nr_seq_lote_fat is null ' || pls_util_pck.enter_w ||
						'		and not exists(	select	1 '|| pls_util_pck.enter_w ||
						'				from	pls_analise_conta y '|| pls_util_pck.enter_w ||
						'				where	y.ie_status in (''D'', ''C'', ''X'')'|| pls_util_pck.enter_w ||
						'				and	y.nr_sequencia = x.nr_seq_analise ))'|| pls_util_pck.enter_w ||
						'and not exists(	select	1 ' || pls_util_pck.enter_w ||
						'			from	pls_conta x ' || pls_util_pck.enter_w ||
						'			where	x.nr_seq_conta_referencia = b.nr_sequencia ' || pls_util_pck.enter_w ||
						'			and	x.nr_seq_ajuste_fat is not null) ' || pls_util_pck.enter_w ||
						'and not exists(	select	1 ' || pls_util_pck.enter_w ||
						'			from	pls_lote_protocolo_conta x ' || pls_util_pck.enter_w ||
						'			where	x.nr_sequencia = a.nr_seq_lote_conta ' || pls_util_pck.enter_w ||
						'			and	x.ie_status = ''X'') ' || pls_util_pck.enter_w ||
						'and not exists(	select	1 ' || pls_util_pck.enter_w ||
						'			from	pls_conta_pos_estabelecido	x ' || pls_util_pck.enter_w ||
						'			where	b.nr_sequencia = x.nr_seq_conta ' || pls_util_pck.enter_w ||
						'			and	x.ie_status_faturamento	= ''L'' ' || pls_util_pck.enter_w ||
						'			and	x.nr_seq_evento_fat is not null ' || pls_util_pck.enter_w ||
						'			and	x.nr_seq_lote_fat is not null )' || pls_util_pck.enter_w ||
						'and not exists(	select	1 ' || pls_util_pck.enter_w ||
						'			from	ptu_aviso_proc_item		x, ' || pls_util_pck.enter_w ||
						'				pls_conta_pos_estabelecido	y '  || pls_util_pck.enter_w ||
						'			where	x.nr_seq_conta_pos_estab	= y.nr_sequencia ' || pls_util_pck.enter_w ||
						'			and	y.nr_seq_conta			= b.nr_sequencia) ' || pls_util_pck.enter_w;
					end if;
					
					ds_sql_contas_faturamento_w:= ds_sql_contas_faturamento_w||					
					ds_filtro_contas_faturamento_w ||
					'order by b.nr_seq_segurado, b.nr_sequencia) a ' || pls_util_pck.enter_w ||
					'union all ' || pls_util_pck.enter_w ||
					'select	a.nr_sequencia, ' || pls_util_pck.enter_w ||
					'	a.nr_seq_segurado, ' || pls_util_pck.enter_w ||
					'	null dt_emissao, ' || pls_util_pck.enter_w ||
					'	nvl(b.nr_seq_ops_congenere, b.nr_seq_congenere), ' || pls_util_pck.enter_w ||
					'	null ie_tipo_conta, ' || pls_util_pck.enter_w ||
					'	null dt_atendimento, ' || pls_util_pck.enter_w ||
					'	null ie_tipo_guia, ' || pls_util_pck.enter_w ||
					'	a.dt_internacao, ' || pls_util_pck.enter_w ||
					'	null cd_guia_ok, ' || pls_util_pck.enter_w ||
					'	pls_obter_se_prest_a400(a.nr_sequencia, :cd_estabelecimento) ie_prestador_a400, ' || pls_util_pck.enter_w ||
					'	null ie_origem_conta, ' || pls_util_pck.enter_w ||
					'	''S'' ie_tipo_protocolo, ' || pls_util_pck.enter_w ||
					'	null nr_seq_congenere, ' || pls_util_pck.enter_w ||
					'	(select max(x.ie_tipo_congenere) from pls_congenere x where x.nr_sequencia = b.nr_seq_ops_congenere) ie_tipo_congenere_ops, ' || pls_util_pck.enter_w ||
					'	(select max(x.ie_tipo_congenere) from pls_congenere x where x.nr_sequencia = b.nr_seq_congenere) ie_tipo_congenere, ' || pls_util_pck.enter_w ||
					'	null ie_tipo_congenere_prot, ' || pls_util_pck.enter_w ||
					'	null ie_origem_protocolo ' || pls_util_pck.enter_w ||
					'from	pls_processo_conta		a, ' || pls_util_pck.enter_w ||
					'	pls_segurado			b, ' || pls_util_pck.enter_w ||
					'	pls_segurado_mensalidade	c  ' || pls_util_pck.enter_w ||
					'where	b.nr_sequencia	= a.nr_seq_segurado ' || pls_util_pck.enter_w ||
					'and	a.nr_sequencia  = c.nr_seq_processo_conta ' || pls_util_pck.enter_w ||
					'and	c.nr_seq_lote_fat is null ' || pls_util_pck.enter_w ||
					'and	c.nr_seq_evento_fat is null ' || pls_util_pck.enter_w ||
					'and	pls_obter_se_benef_remido(b.nr_sequencia, a.dt_internacao) = ''S'' ' || pls_util_pck.enter_w ||
					ds_filtro_contas_fat_sus_w ||
					'and	c.ie_tipo_item	= ''39'' ' || pls_util_pck.enter_w;
					
	sql_pck.bind_variable(':cd_estabelecimento', cd_estabelecimento_p, valor_bind_w);

	gravar_processo_longo('Gerando lote de faturamento' ,'PLS_GERAR_LOTE_FATURAMENTO', -1);

	cursor_w := sql_pck.executa_sql_cursor(ds_sql_contas_faturamento_w, valor_bind_w);
	
	loop
	-- Mudar os eventos dos procedimentos e materiais de pos-estabelecido
	fetch cursor_w into 	nr_seq_conta_w, nr_seq_segurado_w, dt_emissao_w,
				nr_seq_congenere_w, ie_tipo_conta_w, dt_atendimento_w,
				ie_tipo_guia_w, dt_internacao_w, cd_guia_ok_w,
				ie_prestador_a400_w, ie_origem_conta_w, ie_tipo_protocolo_w,
				nr_seq_congenere_prot_w, ie_tipo_congenere_ops_w, ie_tipo_congenere_w, ie_tipo_congenere_prot_w,
				ie_origem_protocolo_w;
	exit when cursor_w%notfound;
		
		ie_impedimento_cobranca_w := null;
		dt_alta_w := null;
		dt_atendimento_w := nvl(dt_atendimento_w,dt_emissao_w);
		
		if	(ie_tipo_protocolo_w = 'R') or
			(ie_origem_protocolo_w = 'G') then
			nr_seq_congenere_w := nvl(nr_seq_congenere_prot_w,nr_seq_congenere_w);
		end if;

		-- Quando for de INTERNACAO, deve ser utilizada a DATA DE ALTA
		if	(ie_conta_fechada_w = 'S') then
			if	(cd_guia_ok_w is not null) then
				select	max(dt_alta)
				into	dt_alta_w
				from	pls_conta
				where	nr_seq_segurado	= nr_seq_segurado_w
				and	cd_guia_ok	= cd_guia_ok_w
				and	ie_tipo_guia	= '5';
				
				if	(dt_alta_w is null) and
					(ie_tipo_guia_w not in ('3','5')) then -- Nao for consulta (3) e nem internacao (5)
					select	nvl(max(dt_atendimento_referencia),dt_atendimento_w)
					into	dt_atendimento_w
					from	pls_conta
					where	nr_seq_segurado	= nr_seq_segurado_w
					and	cd_guia_ok	= cd_guia_ok_w;
				end if;
			end if;
		else -- OS 802541 - Manual do PTU - Regra: Para notas de internacao, adotar a data de inicio de faturamento, conforme regra do Envio de Dados da ANS
			if	(ie_tipo_guia_w = '5') then
				dt_atendimento_w := nvl(dt_internacao_w,dt_atendimento_w);
			end if;
		end if;
		
		if	(ie_tipo_protocolo_w = 'S') then
			dt_atendimento_w	:= dt_internacao_w;
		end if;
		
		if	(qt_regra_lib_fat_w > 0) then

			pls_obter_regra_lib_fat(	nr_seq_conta_w, dt_geracao_lote_w, cd_estabelecimento_p, 
							nm_usuario_p, ie_exige_med_aud_w, ie_exige_enf_aud_w);

			if	(nvl(ie_exige_med_aud_w,'N') = 'S') then

				ie_impedimento_cobranca_w := 'A';
				pls_inserir_mot_imp_cob_fat(	nr_seq_conta_w, nr_seq_lote_p, '3', nm_usuario_p);
			end if;
			
			if	(nvl(ie_exige_enf_aud_w,'N') = 'S') then

				ie_impedimento_cobranca_w := 'A';
				pls_inserir_mot_imp_cob_fat(	nr_seq_conta_w, nr_seq_lote_p, '4', nm_usuario_p);
			end if;
		end if;
		
		-- levanta o tipo de congenere.
		-- se for reembolso, prioriza o congenere do protocolo
		if	(ie_tipo_protocolo_w = 'R') or
			(ie_origem_protocolo_w = 'G') then
		
			ie_tipo_congenere_ref_w := nvl(ie_tipo_congenere_prot_w, nvl(ie_tipo_congenere_w, 'CO'));
			
		else -- senao, prioriza o congenere OPS e depois do segurado
		
			ie_tipo_congenere_ref_w := nvl(ie_tipo_congenere_ops_w, nvl(ie_tipo_congenere_w, 'CO'));
		end if;		
		
		-- so valida a data fora de prazo, se nao for congenere de empresa.
		if	(ie_tipo_congenere_ref_w = 'CO') then
		
			-- Se a data de atendimento estiver fora do prazo
			if	(pls_obter_se_envia_conta(	dt_atendimento_w, dt_geracao_lote_w, nr_seq_congenere_w, 
								nr_seq_segurado_w, nr_seq_conta_w) = 'N') then

				-- Verifica se a data da alta esta fora do prazo
				if	(dt_alta_w is not null) then
					if	(pls_obter_se_envia_conta(	dt_alta_w, dt_geracao_lote_w, nr_seq_congenere_w,
										nr_seq_segurado_w, nr_seq_conta_w) = 'N') then
						ie_impedimento_cobranca_w := 'P';
						
						if	(ie_tipo_protocolo_w <> 'S') then
							pls_inserir_mot_imp_cob_fat(	nr_seq_conta_w, nr_seq_lote_p, '1', nm_usuario_p, null);
						elsif	(ie_tipo_protocolo_w = 'S') then
							pls_inserir_mot_imp_cob_fat(	null, nr_seq_lote_p, '1', nm_usuario_p, nr_seq_conta_w);
						end if;
					end if;
				else
					-- Caso nao tenha data da alta, a conta esta realmente fora do prazo
					ie_impedimento_cobranca_w := 'P';
					if	(ie_tipo_protocolo_w <> 'S') then
						pls_inserir_mot_imp_cob_fat(	nr_seq_conta_w, nr_seq_lote_p, '1', nm_usuario_p, null);
					elsif	(ie_tipo_protocolo_w = 'S') then
						pls_inserir_mot_imp_cob_fat(	null, nr_seq_lote_p, '1', nm_usuario_p, nr_seq_conta_w);
					end if;
				end if;
			end if;
		end if;

		-- Nao se aplica para reembolso
		if 	(ie_prestador_a400_w = 'N') and (ie_tipo_protocolo_w != 'R') and (ie_origem_protocolo_w != 'G') then

			if	(ie_tipo_protocolo_w <> 'S') then
				pls_inserir_mot_imp_cob_fat(	nr_seq_conta_w, nr_seq_lote_p, '2', nm_usuario_p, null);
			end if;
			
			ie_impedimento_cobranca_w := 'P';
		end if;

		if	(ie_tipo_conta_w = 'IC') then

			ptu_atualiza_valor_proc_mat(	nr_seq_conta_w, dt_geracao_p, nm_usuario_p,
							cd_estabelecimento_p);
			commit;
		end if;
		
		ie_gerar_fat_w := 'S';
		
		if	(ie_origem_conta_w = 'A') and
			(ie_fatura_encerrada_w = 'S') then
			
			ie_gerar_fat_w := 'N';
			
			select	max(a.ie_status)
			into	ie_status_fatura_w
			from	ptu_fatura a,
				pls_conta b
			where	a.nr_sequencia = b.nr_seq_fatura
			and	b.nr_sequencia = nr_seq_conta_w;
			
			if	(ie_status_fatura_w = 'E') then
				ie_gerar_fat_w := 'S';
			end if;
		end if;
		
		if	(ie_gerar_fat_w = 'S') then
			pls_obter_evento_fat_pos_estab(	nr_seq_lote_p, nr_seq_conta_w, nr_seq_segurado_w, 
							case when ie_tipo_lote_w = 'A' then 'CA' else 'A' end, ie_impedimento_cobranca_w, nm_usuario_p, ie_tipo_protocolo_w);
			commit;
		end if;
	end loop;

	if (cursor_w%isopen) then
		close cursor_w;
	end if;
end if;

-- Commit na procedure externa

end pls_obter_eventos_lote_fat;
/