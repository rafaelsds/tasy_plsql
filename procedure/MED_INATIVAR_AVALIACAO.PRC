CREATE OR REPLACE
PROCEDURE med_inativar_avaliacao(nr_sequencia_p  NUMBER,
    nm_usuario_p  VARCHAR2) IS

BEGIN

IF (nm_usuario_p IS NOT NULL) AND (nr_sequencia_p IS NOT NULL) THEN
	BEGIN
	UPDATE	med_avaliacao_paciente
	SET 	dt_inativacao 			= SYSDATE,
			nm_usuario_inativacao 	= nm_usuario_p,
			nm_usuario 				= nm_usuario_p,
			ie_situacao				= 'I'
	WHERE 	nr_sequencia = nr_sequencia_p;
	END;
END IF;

COMMIT;

END med_inativar_avaliacao;
/