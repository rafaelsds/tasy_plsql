create or replace
procedure  inserir_prescr_mat_lib_aut( nr_prescricao_p	number,
									   nm_usuario_p	varchar) is 



									  
nr_sequencia_w			prescr_material.nr_sequencia%type;
qt_total_dias_lib_w		prescr_material.qt_total_dias_lib%type;
									  
cursor c01 is
select	b.nr_sequencia,
	b.qt_total_dias_lib
from	prescr_material b,
	prescr_medica a,
	material c
where	a.nr_prescricao     = b.nr_prescricao
and		a.nr_prescricao 	= nr_prescricao_p
and		b.cd_material		= c.cd_material 
and		obter_se_lib_aut_antimic(c.cd_material, a.cd_perfil_ativo, a.cd_estabelecimento, nm_usuario_p, b.cd_intervalo, a.cd_setor_atendimento, b.ie_objetivo) = 'S'
and 	b.nr_sequencia_diluicao is null
and		b.nr_sequencia_proc 	is null
and		b.nr_sequencia_dieta 	is null
and		b.nr_sequencia_solucao  is null
and		b.nr_seq_gasoterapia 	is null
and		b.nr_seq_leite_deriv 	is null
and		b.nr_seq_kit 			is null
and		b.ie_suspenso 			<> 'S'
and		c.ie_dias_util_medic 	<> 'N'
and		nvl(b.qt_total_dias_lib,0)  > 0
and		nvl(b.qt_dias_liberado,0)   > 0
and		nvl(b.qt_dias_solicitado,0) > 0
and		b.ie_agrupador 			= 1;

begin

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	qt_total_dias_lib_w;
exit when c01%notfound;
	begin

	insert into prescr_material_lib_cih
				(nr_sequencia,
				 dt_atualizacao,
				 nm_usuario,
				 dt_atualizacao_nrec,
				 nm_usuario_nrec,
				 nr_prescricao,
				 nr_seq_material,
				 ie_tipo_liberacao,
				 qt_liberacao
				)
	values
				(prescr_material_lib_cih_seq.nextval,
				 sysdate,
				 nm_usuario_p,
				 sysdate,
				 nm_usuario_p,
				 nr_prescricao_p,
				 nr_sequencia_w,
				 'T',
				 qt_total_dias_lib_w
				); 		
	end;
end loop;
close c01;

commit;

end inserir_prescr_mat_lib_aut;
/
