create or replace
procedure TRANSFERIR_VALORES_GUIA_RET
			(nr_seq_ret_item_p	number,
			nm_usuario_p		varchar2,
			ie_campo_origem_p	number,
			ie_campo_destino_p	number) is

vl_pago_w	number(15,2);
vl_adicional_w	number(15,2);
vl_glosado_w	number(15,2);
vl_amenor_w	number(15,2);
vl_desconto_w	number(15,2);
vl_perdas_w	number(15,2);

vl_novo_campo_w	number(15,2);
			
begin

select	vl_pago,
	vl_adicional,
	vl_glosado,
	vl_amenor,	
	vl_desconto,
	vl_perdas
into	vl_pago_w,
	vl_adicional_w,
	vl_glosado_w,
	vl_amenor_w,	
	vl_desconto_w,
	vl_perdas_w
from	convenio_retorno_item
where	nr_sequencia	= nr_seq_ret_item_p;

if	(ie_campo_origem_p = '1') then
	vl_novo_campo_w	:= vl_pago_w;
	vl_pago_w	:= 0;
elsif	(ie_campo_origem_p = '2') then
	vl_novo_campo_w	:= vl_adicional_w;
	vl_adicional_w	:= 0;
elsif	(ie_campo_origem_p = '3') then	
	vl_novo_campo_w	:= vl_glosado_w;
	vl_glosado_w	:= 0;
elsif	(ie_campo_origem_p = '4') then	
	vl_novo_campo_w	:= vl_amenor_w;
	vl_amenor_w	:= 0;
elsif	(ie_campo_origem_p = '5') then
	vl_novo_campo_w	:= vl_desconto_w;
	vl_desconto_w	:= 0;
elsif	(ie_campo_origem_p = '6') then
	vl_novo_campo_w	:= vl_perdas_w;
	vl_perdas_w	:= 0;
end if;

if	(ie_campo_destino_p = '1') then
	vl_pago_w	:= vl_novo_campo_w;	
elsif	(ie_campo_destino_p = '2') then
	vl_adicional_w	:= vl_novo_campo_w;	
elsif	(ie_campo_destino_p = '3') then
	vl_glosado_w	:= vl_novo_campo_w;	
elsif	(ie_campo_destino_p = '4') then	
	vl_amenor_w	:= vl_novo_campo_w;	
elsif	(ie_campo_destino_p = '5') then
	vl_desconto_w	:= vl_novo_campo_w;
elsif	(ie_campo_destino_p = '6') then
	vl_perdas_w	:= vl_novo_campo_w;	
end if;


update	convenio_retorno_item
set	vl_pago		= vl_pago_w,
	vl_adicional	= vl_adicional_w,
	vl_glosado	= vl_glosado_w,
	vl_amenor	= vl_amenor_w,	
	vl_desconto	= vl_desconto_w,
	vl_perdas	= vl_perdas_w,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_ret_item_p;

commit;

end TRANSFERIR_VALORES_GUIA_RET;
/