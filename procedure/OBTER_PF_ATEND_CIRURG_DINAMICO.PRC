CREATE OR REPLACE
Procedure Obter_pf_atend_cirurg_dinamico(
					nm_tabela_p			Varchar2,
					cd_chave_p			Varchar2,
					cd_pessoa_fisica_p 	out Varchar2,
					nr_atendimento_p	out number) IS

ds_comando_w			varchar2(2000);


	function obter_valor_obj_interno(nm_campo_retorno_p 	varchar2,
						nm_tabela_origem_p 	varchar2) return varchar2 is
	
	vl_retorno_w			Varchar2(255) := '0'; 
	c001				  	Integer;		
	retorno_w				Number(5);
	ds_campo_chave_w		varchar2(30) := 'NR_SEQUENCIA';
	
	BEGIN
	if (nm_tabela_p = 'CIRURGIA_AGENTE_ANEST_OCOR') then
		begin
		ds_comando_w	:= ' select a.'|| nm_campo_retorno_p ||' from '|| nm_tabela_origem_p ||' a,  CIRURGIA_AGENTE_ANESTESICO b, '|| nm_tabela_p ||' c  ' 
						 || ' where b.nr_cirurgia = a.nr_cirurgia and b.nr_sequencia = c.NR_SEQ_CIRUR_AGENTE and c.NR_SEQUENCIA = :CD_CHAVE ';
		exception
		when others then
			ds_comando_w := 'XPTO';
		end;
	elsif (nm_tabela_p = 'CIRURGIA_PARTICIPANTE') then
		begin
		ds_comando_w	:= ' select a.'|| nm_campo_retorno_p ||' from '|| nm_tabela_origem_p ||' a, '|| nm_tabela_p ||' b  where b.nr_cirurgia = a.nr_cirurgia and b.nr_Seq_interno = :CD_CHAVE ';
		exception
		when others then
			ds_comando_w := 'XPTO';
		end;
	else
		begin
		ds_comando_w	:= ' select a.'|| nm_campo_retorno_p ||' from '|| nm_tabela_origem_p ||' a, '|| nm_tabela_p ||' b  where b.nr_cirurgia = a.nr_cirurgia and '|| ds_campo_chave_w ||' = :CD_CHAVE ';
		exception
		when others then
			ds_comando_w := 'XPTO';
		end;
	end if;

	if (nvl(ds_comando_w, 'XPTO') <> 'XPTO') then
		begin
		C001 := DBMS_SQL.OPEN_CURSOR;
		DBMS_SQL.PARSE(C001, ds_comando_w, dbms_sql.Native);
		dbms_sql.define_column(C001,1,cd_chave_p,255);
		DBMS_SQL.BIND_VARIABLE(C001, 'CD_CHAVE', cd_chave_p,255);
		retorno_w := DBMS_SQL.execute(c001); 
		retorno_w := DBMS_SQL.fetch_rows(c001);
		DBMS_SQL.COLUMN_VALUE(C001, 1, vl_retorno_w );
		DBMS_SQL.CLOSE_CURSOR(C001);
		exception	
		when others then
			DBMS_SQL.CLOSE_CURSOR(C001);
		end;
	end if;
	
	return vl_retorno_w;
	
	END obter_valor_obj_interno;


BEGIN
if	(nm_tabela_p	is not null) and
	(cd_chave_p		is not null) then
	begin
	
	cd_pessoa_fisica_p := obter_valor_obj_interno('CD_PESSOA_FISICA', 'CIRURGIA');
	nr_atendimento_p   := obter_valor_obj_interno('NR_ATENDIMENTO',   'CIRURGIA');
	
	if (nvl(nr_atendimento_p, 0) = 0 and nvl(cd_pessoa_fisica_p, 0) = 0) then
	
		cd_pessoa_fisica_p := obter_valor_obj_interno('CD_PESSOA_FISICA', 'PEPO_CIRURGIA');
		nr_atendimento_p   := obter_valor_obj_interno('NR_ATENDIMENTO',   'PEPO_CIRURGIA');
	
	end if;

	end;
end if;


END Obter_pf_atend_cirurg_dinamico;
/
