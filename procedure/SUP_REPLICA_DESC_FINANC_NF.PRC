create or replace
procedure	sup_replica_desc_financ_nf(
			nr_seq_nf_p		number,
			pr_desconto_p		number,
			nm_usuario_p		varchar2) is

nr_item_nf_w			number(10);
qt_nf_calculada_w		number(10);
vl_total_item_nf_w		number(13,2);
vl_desc_financ_w		number(15,2);

cursor	c01 is
select	nr_item_nf,
	vl_total_item_nf
from	nota_fiscal_item
where	nr_sequencia = nr_seq_nf_p
and	nvl(vl_total_item_nf,0) > 0
order by	nr_item_nf;

begin

select	count(*)
into	qt_nf_calculada_w
from	nota_fiscal
where	nr_sequencia = nr_seq_nf_p
and	dt_atualizacao_estoque is not null;

if	(pr_desconto_p = 0) and
	(qt_nf_calculada_w = 0) then
	begin

	update	nota_fiscal_item
	set	pr_desc_financ = 0,
		vl_desc_financ = 0
	where	nr_sequencia = nr_seq_nf_p;

	end;
elsif	(pr_desconto_p <> 0) and
	(qt_nf_calculada_w = 0) then
	begin

	open c01;
	loop
	fetch c01 into
		nr_item_nf_w,
		vl_total_item_nf_w;
	exit when c01%notfound;
		begin

		vl_desc_financ_w	:= dividir((pr_desconto_p * vl_total_item_nf_w),100);

		update	nota_fiscal_item
		set	pr_desc_financ = pr_desconto_p,
			vl_desc_financ = vl_desc_financ_w
		where	nr_sequencia = nr_seq_nf_p
		and	nr_item_nf = nr_item_nf_w;

		end;
	end loop;
	close c01;

	end;
end if;

commit;

end sup_replica_desc_financ_nf;
/