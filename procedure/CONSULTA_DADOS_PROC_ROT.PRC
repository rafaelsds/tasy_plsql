create or replace
procedure consulta_dados_proc_rot(
							nr_sequencia_p					number,
							ie_contraste_p				out varchar2,
							ie_ctrl_glic_p              out varchar2,
							ie_exige_condicao_p         out varchar2,
							ie_exige_indicacao_p        out varchar2,
							ie_exige_justificativa_p    out varchar2,
							ie_frase_p                  out varchar2,
							ie_intervalo_p              out varchar2,
							ie_lado_p                   out varchar2,
							ie_medico_exec_p            out varchar2,
							ie_observacao_p             out varchar2,
							ie_orientacao_p             out varchar2,
							ie_outro_exame_p            out varchar2,
							ie_questionar_forma_p       out varchar2,
							ie_questionar_resumo_p      out varchar2,
							ie_rotina_uti_p             out varchar2,
							ie_setor_p                  out varchar2,
							ie_tela_unica_p             out varchar2,
							ie_topografia_p             out varchar2,
							ie_tipo_p					out number,
							cd_procedimento_p			out number,
							ie_origem_proced_p			out number,
							cd_especialidade_p			out varchar2,
							cd_grupo_p					out varchar2,
							cd_area_p					out varchar2,
							nr_seq_exame_p				out number,
							nr_seq_proc_interno_p		out number,
							cd_intervalo_p				out varchar2,
							ie_questiona_agora_p		out varchar2,
							cd_kit_material_p			out number,
							nr_seq_contraste_p			out number,
							nr_seq_rotina_p				out number,
							nr_seq_prot_glic_p			out number,
							cd_material_exame_p			out varchar2,
							ie_material_especial_p		out varchar2,
							ie_mat_especial_p			out varchar2,
							ds_cor_p					out varchar2) is 

ie_tipo_w				number(15);
nr_seq_proc_rotina_w	number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
cd_especialidade_w		varchar2(15);
cd_grupo_w				varchar2(15);
cd_area_w				varchar2(15);
nr_seq_proc_interno_w	number(10);
nr_seq_exame_w			number(10);
qt_w					number(10);
ie_mat_especial_w		varchar2(1);
cd_material_exame_w		varchar2(20);
ie_material_especial_w	varchar2(1) := 'N';
ie_quest_agora_w		varchar2(1) := 'N';			
begin					
		
select	nvl(max(ie_contraste),'N'),
		nvl(max(ie_ctrl_glic),'N'),
		nvl(max(ie_exige_condicao),'N'),
		nvl(max(ie_exige_indicacao),'N'),
		nvl(max(ie_exige_justificativa),'N'),
		nvl(max(ie_frase),'N'),
		nvl(max(ie_intervalo),'N'),
		nvl(max(ie_exige_lado),'N'),
		nvl(max(ie_medico_exec),'N'),
		nvl(max(ie_observacao),'N'),
		nvl(max(ie_orientacao),'N'),
		nvl(max(ie_outro_exame),'N'),
		nvl(max(ie_questionar_forma),'N'),
		nvl(max(ie_questionar_resumo),'N'),
		nvl(max(ie_rotina_uti),'N'),
		nvl(max(ie_setor),'N'),
		nvl(max(ie_tela_unica),'N'),
		nvl(max(ie_topografia),'N'),
		max(ds_cor),
		max(ie_tipo),
		max(nr_seq_proc_rotina),
		max(cd_intervalo),
		nvl(max(nr_seq_prot_glic),0),
		nvl(max(cd_material_exame),'0'),
		nvl(max(ie_quest_agora),'N')
into	ie_contraste_p,
		ie_ctrl_glic_p,
		ie_exige_condicao_p,
		ie_exige_indicacao_p,
		ie_exige_justificativa_p,
		ie_frase_p,
		ie_intervalo_p,
		ie_lado_p,
		ie_medico_exec_p,
		ie_observacao_p,
		ie_orientacao_p,
		ie_outro_exame_p,
		ie_questionar_forma_p,
		ie_questionar_resumo_p,
		ie_rotina_uti_p,
		ie_setor_p,
		ie_tela_unica_p,
		ie_topografia_p,
		ds_cor_p,
		ie_tipo_w,
		nr_seq_proc_rotina_w,
		cd_intervalo_p,
		nr_seq_prot_glic_p,
		cd_material_exame_w,
		ie_quest_agora_w
from	w_proc_rotina_esp_t
where	nr_sequencia = nr_sequencia_p;

ie_questiona_agora_p := 'N';
if (ie_tipo_w = 0) then

	select	nvl(max(b.cd_procedimento),0),
			nvl(max(b.ie_origem_proced),0),
			nvl(max(c.cd_especialidade),0),
			nvl(max(c.cd_grupo_proc),0),
			nvl(max(c.cd_area_procedimento),0),
			nvl(max(a.ie_questiona_agora),'N')
	into	cd_procedimento_w,
			ie_origem_proced_w,
			cd_especialidade_w,
			cd_grupo_w,
			cd_area_w,
			ie_questiona_agora_p
	from	exame_lab_rotina  a, 
			exame_laboratorio b,
			estrutura_procedimento_v c
	where	a.nr_seq_exame = b.nr_seq_exame
	and		b.cd_procedimento = c.cd_procedimento
	and		b.ie_origem_proced = c.ie_origem_proced
	and		a.nr_sequencia = nr_seq_proc_rotina_w;
	
	select  nvl(max(m.ie_material_especial),'N')
	into	ie_material_especial_w
    from    exame_lab_rotina e,
			material_exame_lab m
    where   e.nr_seq_material = m.nr_sequencia
	and     e.nr_sequencia    = nr_seq_proc_rotina_w;
	
	select	nvl(max(a.nr_sequencia),0)
	into	nr_seq_proc_interno_w
	from	proc_interno a
	where	a.cd_procedimento = cd_procedimento_w;

elsif (ie_tipo_w = 1) then

	select	nvl(max(a.nr_proc_interno),0),
			nvl(max(a.cd_procedimento),0),
			nvl(max(a.ie_origem_proced),0),
			nvl(max(a.ie_questiona_agora),'N'),
			max(a.cd_kit_material)
	into	nr_seq_proc_interno_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			ie_questiona_agora_p,	
			cd_kit_material_p
	from	procedimento_rotina a
	where	a.nr_sequencia = nr_seq_proc_rotina_w;
	
	select	nvl(max(b.cd_especialidade),0),
			nvl(max(b.cd_grupo_proc),0),
			nvl(max(b.cd_area_procedimento),0)
	into	cd_especialidade_w,
			cd_grupo_w,
			cd_area_w
	from	estrutura_procedimento_v b
	where	b.cd_procedimento = cd_procedimento_w
	and		b.ie_origem_proced = ie_origem_proced_w;
	
	if	(nr_seq_proc_interno_w > 0) and
		(cd_procedimento_w = 0) and
		(ie_origem_proced_w = 0) then
		select	nvl(max(a.cd_procedimento),0),
				nvl(max(a.ie_origem_proced),0)
		into	cd_procedimento_w,
				ie_origem_proced_w
		from	proc_interno a
		where	a.nr_sequencia = nr_seq_proc_interno_w;
	end if;
elsif (ie_tipo_w = 2) then

	select	nvl(max(a.nr_seq_exame),0),
			nvl(max(a.cd_procedimento),0),
			nvl(max(a.ie_origem_proced),0),
			nvl(max(b.cd_especialidade),0),
			nvl(max(b.cd_grupo_proc),0),
			nvl(max(b.cd_area_procedimento),0),
			nvl(max(a.ie_questionar_agora),'N')
	into	nr_seq_exame_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			cd_especialidade_w,
			cd_grupo_w,
			cd_area_w,
			ie_questiona_agora_p
	from	exame_laboratorio  a,
			estrutura_procedimento_v b
	where	a.nr_seq_exame = nr_seq_proc_rotina_w
	and		a.cd_procedimento = b.cd_procedimento
	and		a.ie_origem_proced = b.ie_origem_proced;
	
	select	nvl(max(b.ie_material_especial),'N')
	into	ie_material_especial_w
	from	material_exame_lab b,
			exame_lab_material a
	where	a.nr_seq_material 	= b.nr_sequencia
	and		a.nr_seq_exame 		= nr_seq_exame_w
	and		a.ie_situacao		= 'A'
	and		ie_prioridade		= (	select	min(ie_prioridade)
									from	exame_lab_material x
									where	nr_seq_exame	= nr_seq_exame_w
									and		x.ie_situacao	= 'A');
	
	if	(cd_material_exame_w <> '') and
		(cd_material_exame_w <> '0') then
		select	nvl(max(ie_material_especial),'N')
		into	ie_mat_especial_w
		from	material_exame_lab b
		where	b.cd_material_exame = cd_material_exame_w;
	end if;

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_proc_interno_w
	from	proc_interno
	where	cd_procedimento = cd_procedimento_w;
elsif (ie_tipo_w = 4) then
	ie_questiona_agora_p := ie_quest_agora_w;
end if;

select 	nvl(max(nr_seq_contraste),0)
into	nr_seq_contraste_p 
from 	procedimento_rotina 
where 	nr_sequencia = nr_seq_proc_rotina_w;

ie_tipo_p 				:= ie_tipo_w;
cd_procedimento_p 		:= cd_procedimento_w;
ie_origem_proced_p		:= ie_origem_proced_w;
cd_especialidade_p		:= cd_especialidade_w;
cd_grupo_p		    	:= cd_grupo_p;
cd_area_p		    	:= cd_area_w;
nr_seq_exame_p	    	:= nvl(nr_seq_exame_w,0);
nr_seq_proc_interno_p	:= nvl(nr_seq_proc_interno_w,0);
nr_seq_rotina_p			:= nvl(nr_seq_proc_rotina_w,0);
ie_material_especial_p	:= nvl(ie_material_especial_w,'N');
cd_material_exame_p		:= nvl(cd_material_exame_w,'0');
ie_mat_especial_p		:= nvl(ie_mat_especial_w,'N');

end consulta_dados_proc_rot;
/