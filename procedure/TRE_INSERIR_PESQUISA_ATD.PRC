create or replace
procedure tre_inserir_pesquisa_atd(
			ds_campo1 number,
			ds_campo4 number,
			ds_campo5 number,
			ds_campo6 number, 
			ds_campo7 number,
			ds_campo9 number,
			ds_campo10 varchar2,
			ds_campo11 varchar2,
			nr_seq_pesquisa_p number,
			nm_usuario_p varchar2) is 

ds_base_w varchar2(20);			
			
begin

select user
into ds_base_w
from dual;


if(upper(ds_base_w) = 'CORP') then
begin

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								16506,
								nr_seq_pesquisa_p,
								ds_campo1);

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								16508,
								nr_seq_pesquisa_p,
								ds_campo4);
								
insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								16509,
								nr_seq_pesquisa_p,
								ds_campo5);
								
insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								16510,
								nr_seq_pesquisa_p,
								ds_campo6);	

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								16511,
								nr_seq_pesquisa_p,
								ds_campo7);	

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								16515,
								nr_seq_pesquisa_p,
								ds_campo9);

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(ds_campo10,
								sysdate,
								nm_usuario_p,
								16518,
								nr_seq_pesquisa_p,
								0);	
								
insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(ds_campo11,
								sysdate,
								nm_usuario_p,
								16519,
								nr_seq_pesquisa_p,
								0);	
								
update tre_pesquisa
set dt_liberacao = sysdate
where nr_sequencia = nr_seq_pesquisa_p;

end;
end if;



if(upper(ds_base_w) = 'TASY') then 
begin

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								161783,
								nr_seq_pesquisa_p,
								ds_campo1);

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								161794,
								nr_seq_pesquisa_p,
								ds_campo4);
								
insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								161796,
								nr_seq_pesquisa_p,
								ds_campo5);
								
insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								161797,
								nr_seq_pesquisa_p,
								ds_campo6);	

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								161799,
								nr_seq_pesquisa_p,
								ds_campo7);	

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(null,
								sysdate,
								nm_usuario_p,
								162436,
								nr_seq_pesquisa_p,
								ds_campo9);

insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(ds_campo10,
								sysdate,
								nm_usuario_p,
								161809,
								nr_seq_pesquisa_p,
								0);	
								
insert into tre_pesquisa_result (ds_resultado,
								dt_atualizacao,
								nm_usuario,
								nr_seq_item,
								nr_seq_pesquisa,
								qt_resultado)
						values
								(ds_campo11,
								sysdate,
								nm_usuario_p,
								161816,
								nr_seq_pesquisa_p,
								0);	
					

end;
end if;



					
commit;

end tre_inserir_pesquisa_atd;
/
