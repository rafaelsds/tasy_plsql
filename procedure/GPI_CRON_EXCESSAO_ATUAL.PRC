create or replace trigger gpi_cron_excessao_atual
before insert or update on gpi_cron_excessao
for each row

declare

begin
begin
	if (:new.hr_final is not null) and ((:new.hr_final <> :old.hr_final) or (:old.dt_hr_final is null)) then
		:new.dt_hr_final := to_date(to_char(nvl(:new.dt_final, sysdate),'dd/mm/yyyy') || ' ' || :new.hr_final,'dd/mm/yyyy hh24:mi');
	end if;	
	if (:new.hr_inicio is not null) and ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_hr_inicio is null)) then
		:new.dt_hr_inicio := to_date(to_char(nvl(:new.dt_inicial, sysdate),'dd/mm/yyyy') || ' ' || :new.hr_inicio,'dd/mm/yyyy hh24:mi');
	end if;	
exception
	when others then
	null;
end;

end;
/