create or replace
procedure alterar_cond_pagto_ordem(	nr_ordem_compra_p		number,
					cd_condicao_pagamento_p		varchar2,
					cd_estabelecimento_p		number,
					nm_usuario_p			varchar2) is 

qt_registros_w			number(5);
ds_cond_pagto_original_w	varchar2(80);
ds_cond_pagto_alterado_w	varchar2(80);
ds_historico_w			varchar2(4000);
begin

select	count(*)
into	qt_registros_w
from	nota_fiscal_item
where	nr_ordem_compra = nr_ordem_compra_p;

if	(qt_registros_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(240174);
end if;

if	(cd_condicao_pagamento_p <> '0') then
	select	obter_desc_cond_pagto(cd_condicao_pagamento),
		obter_desc_cond_pagto(cd_condicao_pagamento_p)
	into	ds_cond_pagto_original_w,
		ds_cond_pagto_alterado_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_p;

	update	ordem_compra
	set	cd_condicao_pagamento = cd_condicao_pagamento_p
	where	nr_ordem_compra = nr_ordem_compra_p;

	delete from ordem_compra_venc where nr_ordem_compra = nr_ordem_compra_p;

	gerar_ordem_compra_venc(nr_ordem_compra_p,nm_usuario_p);
	
	
	/*Alterado o valor do campo Cond. pagamento, atrav�s da op��o Alterar condi��o de pagamento.
	De: #@DS_VALOR_OLD#@.
	Para: #@DS_VALOR_NEW#@.*/
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(297085, 'DS_VALOR_OLD=' || ds_cond_pagto_original_w || ';DS_VALOR_NEW=' || ds_cond_pagto_alterado_w),1,4000);

	inserir_historico_ordem_compra(	nr_ordem_compra_p,
					'S',
					wheb_mensagem_pck.get_texto(297086)/*Altera��o de Condi��o de pagamento.*/,
					ds_historico_w,
					nm_usuario_p);
end if;

commit;

end alterar_cond_pagto_ordem;
/