create or replace 
procedure CONSISTIR_REGRA_ATEND_MEDIC(	nr_seq_atendimento_p	number,
					nr_seq_material_p		number,
					nm_usuario_p		Varchar2,
					nr_seq_regra_p 		number default 0,
					cd_material_p 		number default null,
					cd_unid_med_prescr_p 	varchar2 default null,
					qt_dose_prescricao_p 	number default null,
					ie_via_aplicacao_p 		varchar2 default null,
					ds_observacao_p 		varchar2 default null,
					is_paciente_medic_update 	varchar2 default 'N',
					ie_uso_continuo_p 		varchar2 default null,
					ie_local_adm_p 		varchar2 default null,
					qt_dias_util_p		number default null,
					nr_seq_interno_p 		number default null,
					cd_intervalo_p 		varchar2 default null,
					nr_seq_diluicao_p 		number default null,
          					ds_dose_diferenciada_p varchar2 default null,
					qt_dose_p		number default null,
					cd_unid_med_dose_p 	varchar2 default null
					) is 
						
						
ds_erro_w			varchar2(300);
ds_erro2_w			varchar2(300);
cd_perfil_w			number(10);
nr_seq_erro_w			number(10);
IE_FORMA_CONSISTENCIA_W	VARCHAR2(1);
ds_obs_w			varchar2(4000);
cd_material_w			number(10);
ie_padronizado_w			varchar2(2);
nr_seq_paciente_w			number(10);
cd_protocolo_w			number(10);
cd_pessoa_fisica_w		varchar2(10);
qt_reg_w				number(10);
dt_real_w				date;
ie_uso_continuo_w			varchar2(1);
ie_local_adm_w			varchar2(1);
qt_dias_util_w			number(3);
qt_dose_prescricao_w  		number(15,4);
ie_padronizado_lib_w 		varchar	(2);
cd_pessoa_fisica_ww	  	varchar2(10);
ie_regra_2_w 			varchar2(2);
ie_regra_8_w    			varchar2(2);
ie_regra_10_w 			varchar2(2);
ie_regra_12_w 			varchar2(2);
nr_atendimento_w 			number(10);
cd_convenio_w  			number(15);
cd_plano_w 			varchar2(10);	
ie_regra_w			varchar2(10);
nr_seq_regra_plano_w		number(10,0);                
cd_setor_atendimento_w 		Number(10);
ds_justificativa_w			varchar2(1);
nr_seq_interno_w			number(10);	
ie_medic_padronizado_w		varchar2(2);
qt_creatinina_w			number(5,2);
ie_paciente_alergico_w        		varchar2(1);
ds_item_w                     		varchar2(100);
ds_mensagem_w 			varchar2(200) default null;
cd_intervalo_w			varchar2(7);
cd_intervalo_solucao_w		varchar2(7);
nr_seq_diluicao_w          		number(6,0);
qt_saldo_disponivel_estoque_w 	number(15,4);
ie_material_disponivel_w  		varchar2(1);

begin


cd_perfil_w		:= obter_perfil_ativo;
cd_setor_atendimento_w 	:= wheb_usuario_pck.get_cd_setor_atendimento;

delete from paciente_atendimento_erro
where	nr_seq_atendimento	= nr_seq_atendimento_p
and	nr_seq_material		= nr_seq_material_p;

select 	nvl(max(nr_seq_paciente), 0)
into	nr_seq_paciente_w
from 	paciente_atendimento 
where  	nr_seq_atendimento	= nr_seq_atendimento_p;

delete 	from paciente_atendimento_erro
where	nr_seq_paciente	= nr_seq_paciente_w
and	nr_seq_material	= nr_seq_material_p;

select	max(ds_observacao),
	max(cd_material),
	max(ie_uso_continuo),
	max(ie_local_adm),
	max(qt_dias_util),
	max(qt_dose_prescricao),
	max(nr_seq_interno),
	max(cd_intervalo),
	max(nr_seq_diluicao)
into	ds_obs_w,
	cd_material_w,
	ie_uso_continuo_w,
	ie_local_adm_w,
	qt_dias_util_w,
	qt_dose_prescricao_w,
	nr_seq_interno_w,
	cd_intervalo_w,
	nr_seq_diluicao_w
from (
select	a.ds_observacao,
	a.cd_material,
	a.ie_uso_continuo,
	a.ie_local_adm,
	a.qt_dias_util,
	a.qt_dose_prescricao,
	a.nr_seq_interno,
	a.cd_intervalo,
	a.nr_seq_diluicao,
	a.nr_seq_solucao
from	paciente_atend_medic a
where 	a.nr_seq_material = nr_seq_material_p
and 	a.nr_seq_atendimento = nr_seq_atendimento_p
and   	a.nr_seq_solucao is null
and 	a.dt_cancelamento is null
union 
select	a.ds_observacao,
	a.cd_material,
	a.ie_uso_continuo,
	a.ie_local_adm,
	a.qt_dias_util,
	a.qt_dose_prescricao,
	a.nr_seq_interno,
	b.cd_intervalo,
	a.nr_seq_diluicao,
	a.nr_seq_solucao
from 	paciente_atend_medic a,
	paciente_atend_soluc b
where 	a.nr_seq_material = nr_seq_material_p
and 	a.nr_seq_atendimento = nr_seq_atendimento_p
and 	a.nr_seq_solucao = b.nr_seq_solucao
and 	a.nr_seq_atendimento = b.nr_seq_atendimento
and 	a.nr_seq_solucao is not null) x;

if(is_paciente_medic_update = 'S') then

	ds_obs_w := ds_observacao_p;
	cd_material_w := cd_material_p;
	ie_uso_continuo_w := ie_uso_continuo_p;
	ie_local_adm_w := ie_local_adm_p;
	qt_dias_util_w := qt_dias_util_p;
	qt_dose_prescricao_w := qt_dose_prescricao_p;
	nr_seq_interno_w := nr_seq_interno_p;
	cd_intervalo_w := cd_intervalo_p;
	nr_seq_diluicao_w := nr_seq_diluicao_p;

end if;

select max(nr_atendimento) 
into nr_atendimento_w
from paciente_atendimento
where nr_seq_atendimento = nr_seq_atendimento_p;

if (nr_atendimento_w is not null)then
	select obter_convenio_atendimento(nr_atendimento_w),
		obter_plano_atendimento(nr_atendimento_w,'C')
	into cd_convenio_w,cd_plano_w
	from dual;
end if; 

select  max(a.cd_pessoa_fisica)
into	cd_pessoa_fisica_ww
from	paciente_Setor a,
	paciente_atendimento b,
	paciente_atend_medic c
where	a.nr_seq_paciente = b.nr_seq_paciente
and	b.nr_seq_atendimento = c.nr_seq_atendimento
and	c.nr_seq_atendimento = nr_seq_atendimento_p;

 
if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 1)) and
   (obter_se_consistir_regra_onc(1,cd_perfil_w) = 'S') then   

	Verifica_dose_maxima_onc(nr_seq_atendimento_p,
				nr_seq_material_p,
				'N',
				ds_erro_w,
				ds_erro2_w,
				IE_FORMA_CONSISTENCIA_W, 
				ds_mensagem_w,
				cd_material_p,
				cd_unid_med_prescr_p,
				qt_dose_prescricao_p,
				ie_via_aplicacao_p,
				ds_observacao_p,
				is_paciente_medic_update,
				nr_seq_material_p,
				qt_dose_p,
        cd_unid_med_dose_p,
        cd_intervalo_p
				);
	
	if (ds_erro_w is not null)  or
		(ds_erro2_w is not null )then		
		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,1,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W, ds_mensagem_w);
	end if;
		
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 2)) then
	ie_regra_2_w := obter_se_consistir_regra_onc(2,cd_perfil_w);
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 10)) then	
	ie_regra_10_w := obter_se_consistir_regra_onc(10,cd_perfil_w);
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 12)) then
	ie_regra_12_w := obter_se_consistir_regra_onc(12,cd_perfil_w);
end if;

if	(ie_regra_2_w = 'S') or (ie_regra_10_w = 'S') or (ie_regra_12_w = 'S') then	
	
	ie_padronizado_w	:=  substr(obter_se_material_padronizado(wheb_usuario_pck.get_cd_estabelecimento,cd_material_w),1,2);	
		
	if (cd_pessoa_fisica_ww > 0) then
		ie_padronizado_lib_w := substr(Obter_se_medic_lib_pac_onc(cd_pessoa_fisica_ww,cd_material_w,nr_seq_atendimento_p,nr_seq_material_p,'A'),1,2);
		ie_medic_padronizado_w := substr(obter_se_medic_pac_onc(cd_pessoa_fisica_ww,cd_material_w),1,2); --function nova
	end if;

	if ((ie_regra_10_w = 'S') and (ie_padronizado_w = 'N') and (nvl(ie_medic_padronizado_w,'S') = 'N')) then
		
		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,10,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);
	
	elsif (ie_regra_12_w = 'S') and (ie_padronizado_w = 'N') and (nvl(ie_padronizado_lib_w,'S') = 'N') then
		
		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,12,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);
	
	elsif (ie_regra_2_w	= 'S') and	(ds_obs_w is null) and (ie_padronizado_w = 'N') then
	
		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,2,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);
	end if;
end if;


if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 3)) and
   (obter_se_consistir_regra_onc(3,cd_perfil_w) = 'S') then

	select 	max(nr_seq_paciente),
		max(dt_real)
	into	nr_seq_paciente_w,
		dt_real_w
	from	paciente_atendimento
	where	nr_seq_atendimento = nr_seq_atendimento_p;
		
	select	max(cd_protocolo),
		max(cd_pessoa_fisica)
	into	cd_protocolo_w,
		cd_pessoa_fisica_w
	from	paciente_setor
	where	nr_seq_paciente = nr_seq_paciente_w;
	
	if (cd_protocolo_w > 0) then

		select 	count(*)
		into	qt_reg_w
		from	paciente_setor a,
			paciente_atendimento b
		where	a.nr_seq_paciente =	b.nr_seq_paciente
		and	a.cd_protocolo <> cd_protocolo_w
		and	a.cd_pessoa_fisica = cd_pessoa_fisica_w
		and	trunc(b.dt_real) = trunc(dt_real_w)
		and	b.nr_prescricao is not null;
			
		if (qt_reg_w > 0) then
			gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,3,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);
		end if;
			
	end if;
	
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 4)) and (obter_se_consistir_regra_onc(4,cd_perfil_w) = 'S') then
	Verifica_dose_minima_onc(
			nr_seq_atendimento_p,nr_seq_material_p,
			'N',
			ds_erro_w,
			ds_erro2_w,
			IE_FORMA_CONSISTENCIA_W
			);
	
	if (ds_erro_w is not null)  or
		(ds_erro2_w is not null )then
		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,4,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);
	end if;
		
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 7)) and (obter_se_consistir_regra_onc(7,cd_perfil_w) = 'S') then
   
	if (ie_uso_continuo_w = 'S') and (ie_local_adm_w = 'C') and (qt_dias_util_w is null) then
		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,7,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);
	end if;
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 8)) and (obter_se_consistir_regra_onc(8,cd_perfil_w) = 'S' ) and (cd_convenio_w is not null) then
	consiste_mat_plano_convenio(	nvl(cd_convenio_w,0),         
					nvl(cd_plano_w,'0'),                     
					cd_material_w,
					nr_atendimento_w,
					cd_setor_atendimento_w,
					ds_erro_w,                   
					ds_erro2_w,
					ie_regra_w,
					nr_seq_regra_plano_w,
					qt_dose_prescricao_w, --  qt_material_p               
					Sysdate,
					null,                
					wheb_usuario_pck.get_cd_estabelecimento,
					null,
					null);
					
	if (ie_regra_w = 6) then
		if (ds_erro_w is not null)  or (ds_erro2_w is not null )then
			gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,8,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);
		end if;
		
	end if;
	
end if;



if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 9)) and  (obter_se_consistir_regra_onc(9,cd_perfil_w) = 'S') then
	
	if (Obter_se_dose_onc_autor_conv(nr_seq_atendimento_p,cd_material_w,qt_dose_prescricao_w,nr_seq_interno_w,cd_convenio_w) = 'N') then
	
		ds_erro_w := ' ';
		ds_erro2_w := ' ';
		
		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,9,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);

	end if;
		
end if;


if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 11)) and  (obter_se_consistir_regra_onc(11,cd_perfil_w)	= 'S') then

	obter_se_regra_just_atend_onc(11,nr_seq_atendimento_p,nr_seq_material_p,ds_justificativa_w);
	
	if (ds_justificativa_w = 'N') then
	
		ds_erro_w := obter_concent_medic_onc(nr_seq_atendimento_p, nr_seq_material_p,'M');

		if (ds_erro_w is not null) then
			gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,11,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);
		end if;
	
	end if;

end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 13)) and  (obter_se_consistir_regra_onc(13,cd_perfil_w) = 'S') then

	Select 	max(QT_CREATININA)
	into	qt_creatinina_w
	from 	paciente_atendimento
	where	nr_seq_atendimento = nr_seq_atendimento_p;	
	
	if (qt_creatinina_w is null) then
	
		obter_se_regra_just_atend_onc(13,nr_seq_atendimento_p,nr_seq_material_p,ds_justificativa_w);
		
		if (ds_justificativa_w = 'N') then

			ds_erro_w := obter_consiste_creatinina(nr_seq_atendimento_p,nr_seq_material_p,'A');

			if (ds_erro_w = 'S') then
				gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,13,ds_erro_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w);
			end if;
		
		end if;
	
	end if;
		
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 14)) and (obter_se_consistir_regra_onc(14,cd_perfil_w) = 'S') then
	
	select	max(verificar_dose_dif_onc(0,nr_seq_atendimento_p,nr_seq_material_p,ds_dose_diferenciada_p, cd_intervalo_w))
	into	ds_erro_w
	from	dual;
	
	if (trim(ds_erro_w) is not null) then
		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,14,ds_erro_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w);
	end if;
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 15)) and (obter_se_consistir_regra_onc(15,cd_perfil_w) = 'S')  then
     
      select  max(a.cd_pessoa_fisica)
      into	cd_pessoa_fisica_ww
        from	paciente_Setor a,
      paciente_atendimento b,
      paciente_atend_medic c
      where	a.nr_seq_paciente = b.nr_seq_paciente
      and	b.nr_seq_atendimento = c.nr_seq_atendimento
      and	c.nr_seq_atendimento = nr_seq_atendimento_p;

     cd_pessoa_fisica_w := obter_pessoa_atendimento(nr_atendimento_w,'C');
     
     if(cd_pessoa_fisica_w is null ) then
      
      select  max(a.cd_pessoa_fisica)
      into	cd_pessoa_fisica_w
      from	paciente_Setor a,
      paciente_atendimento b,
      paciente_atend_medic c
      where	a.nr_seq_paciente = b.nr_seq_paciente
      and	b.nr_seq_atendimento = c.nr_seq_atendimento
      and	c.nr_seq_atendimento = nr_seq_atendimento_p;
      
      end if;
     
     Verifica_Paciente_Alergico (cd_pessoa_fisica_w,cd_material_w,ds_item_w,ie_paciente_alergico_w);
     

     if (ie_paciente_alergico_w = 'S') then
          gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,15,ds_erro_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w);
     end if;     
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 16)) and (obter_se_consistir_regra_onc(16,cd_perfil_w) = 'S') then
	 
    consistir_regra_prescr_onc(nr_seq_atendimento_p, nr_seq_material_p, 'A', 16);     
     
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 18)) and (obter_se_consistir_regra_onc(18,cd_perfil_w)	= 'S') then

	if ((cd_intervalo_w is null) or (obter_se_intervalo_ativo(cd_intervalo_w) = 'I')) and 	(nr_seq_diluicao_w = 0 or nr_seq_diluicao_w is null) then
		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,18,ds_erro_w||ds_erro2_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w,IE_FORMA_CONSISTENCIA_W);
	end if;
end if;

if ((nr_seq_regra_p = 0) or (nr_seq_regra_p = 19)) and   (obter_se_consistir_regra_onc(19,cd_perfil_w)	= 'S') then

  select coalesce(ie_disponivel_mercado,'N')
  into ie_material_disponivel_w
  FROM material
  WHERE cd_material = cd_material_w;

  qt_saldo_disponivel_estoque_w := 0;
	if (ie_material_disponivel_w = 'E') then
	  	qt_saldo_disponivel_estoque_w	:= obter_saldo_disp_estoque(wheb_usuario_pck.get_cd_estabelecimento, cd_material_w, 0, SYSDATE);
	end if;

	if (ie_material_disponivel_w <> 'S') AND (qt_saldo_disponivel_estoque_w <= 0) THEN
      		gerar_erro_pac_atend(nr_seq_atendimento_p,nr_seq_material_p,19,ds_erro_w,cd_perfil_w,nm_usuario_p,nr_seq_erro_w);
	end if;
end if;

if (((nr_seq_regra_p = 0) or (nr_seq_regra_p = 20)) and (obter_se_consistir_regra_onc(20, cd_perfil_w) = 'S')) then
		
	verifica_decimal_dose_aten_onc(nr_seq_atendimento_p, nr_seq_material_p, ds_mensagem_w);	
		
	if (ds_mensagem_w is not null) then
		Gerar_Erro_Pac_Atend(nr_seq_atendimento_p, nr_seq_material_p, 20, ds_erro_w, cd_perfil_w, nm_usuario_p, nr_seq_erro_w);			
	end if;			
end if;

if (((nr_seq_regra_p = 0) or (nr_seq_regra_p = 21)) and
	   (obter_se_consistir_regra_onc(21, cd_perfil_w) = 'S')) then
	verifica_medic_dupl_atend(nr_seq_atendimento_p, nr_seq_material_p, ds_erro_w, cd_material_w);
	if (ds_erro_w is not null) then
		Gerar_Erro_Pac_Atend(nr_seq_atendimento_p, nr_seq_material_p, 21, ds_erro_w, cd_perfil_w, nm_usuario_p, nr_seq_erro_w, null, ds_erro_w);	
	end if;	
end if;


commit;

end CONSISTIR_REGRA_ATEND_MEDIC;
/
