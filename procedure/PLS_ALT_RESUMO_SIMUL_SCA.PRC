create or replace
procedure pls_alt_resumo_simul_sca
		(	nr_seq_simulacao_p	number,
			nr_seq_segurado_simul_p	number,
			ie_opcao_p		varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p		Varchar2) is

/*
ie_opcao_p
I - Individual/Familiar
C - Coletivos
*/

nr_seq_sca_vinc_w	number(10);
vl_sca_w		number(15,2) := 0;
qt_benef_simul_pj_w	number(10);
ds_resumo_sca_pj_w	varchar2(255);
qt_registros_w		number(10);
nm_sca_w		varchar2(255);
ds_resumo_sca_w		varchar2(4000);

Cursor C01 is
	select	nr_sequencia
	from	pls_sca_vinculo
	where	nr_seq_segurado_simul	= nr_seq_segurado_simul_p
	and	nr_seq_plano is not null
	union all
	select	nr_sequencia
	from	pls_sca_vinculo
	where	nr_seq_simulacao	= nr_seq_simulacao_p
	and	nr_seq_plano is not null;

Cursor C02 is
	select	nr_sequencia
	from	pls_sca_vinculo
	where	nr_seq_simulacao	= nr_seq_simulacao_p
	and	nr_seq_plano is not null;

begin
select	nvl(sum(qt_beneficiario),0)
into	qt_benef_simul_pj_w
from	pls_simulpreco_coletivo
where	nr_seq_simulacao	= nr_seq_simulacao_p;
if	(ie_opcao_p	= 'I') then
	open C01;
	loop
	fetch C01 into
		nr_seq_sca_vinc_w;
	exit when C01%notfound;
		begin
		pls_gerar_resumo_itens_adic(nr_seq_segurado_simul_p,nr_seq_sca_vinc_w,'S',vl_sca_w);
		vl_sca_w	:= nvl(vl_sca_w,0);
		
		select	count(*)
		into	qt_registros_w
		from	pls_simulacao_resumo
		where	nr_seq_simulacao	= nr_seq_simulacao_p
		and	nr_seq_segurado_simul	= nr_seq_segurado_simul_p
		and	nr_seq_vinc_sca		= nr_seq_sca_vinc_w
		and	ie_tipo_pessoa		= 'PF';
		
		select	a.ds_plano
		into	nm_sca_w
		from	pls_sca_vinculo	b,
			pls_plano		a
		where	b.nr_seq_plano		= a.nr_sequencia
		and	b.nr_sequencia		= nr_seq_sca_vinc_w;
		
		ds_resumo_sca_w	:= '                SCA - ' || nm_sca_w;
		
		if	(qt_registros_w	> 0) then
			update	pls_simulacao_resumo
			set	vl_resumo		= vl_sca_w
			where	nr_seq_simulacao	= nr_seq_simulacao_p
			and	nr_seq_vinc_sca		= nr_seq_sca_vinc_w
			and	nr_seq_ordem		= 4
			and	nr_seq_segurado_simul	= nr_seq_segurado_simul_p
			and	ie_tipo_pessoa		= 'PF';
		else
			insert into	pls_simulacao_resumo
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					cd_estabelecimento,nr_seq_simulacao,nr_seq_ordem,nr_seq_segurado_simul,nr_seq_vinc_sca,
					ds_item_resumo,vl_resumo,ie_tipo_pessoa,vl_pro_rata_dia,vl_antecipacao)
			values	(	pls_simulacao_resumo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					cd_estabelecimento_p,nr_seq_simulacao_p,4,nr_seq_segurado_simul_p,nr_seq_sca_vinc_w,
					ds_resumo_sca_w,vl_sca_w,'PF',0,0);
		end if;
		end;
	end loop;
	close C01;
elsif	(ie_opcao_p	= 'C') then
	open C02;
	loop
	fetch C02 into
		nr_seq_sca_vinc_w;
	exit when C02%notfound;
		begin
		
		vl_sca_w		:= nvl(pls_obter_valores_simul_pj(nr_seq_simulacao_p,null,nr_seq_sca_vinc_w,'S'),0);
		ds_resumo_sca_pj_w	:= 'SCA - ' || substr(pls_obter_dados_vinc_sca(nr_seq_sca_vinc_w,'N'),1,150) || ' - Qt. = ' || qt_benef_simul_pj_w;
		
		select	count(*)
		into	qt_registros_w
		from	pls_simulacao_resumo
		where	nr_seq_simulacao	= nr_seq_simulacao_p
		and	nr_seq_vinc_sca		= nr_seq_sca_vinc_w
		and	nr_seq_ordem		= 4
		and	ie_tipo_pessoa		= 'PJ';
		
		if	(qt_registros_w	= 0) then
			insert into	pls_simulacao_resumo
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					cd_estabelecimento,nr_seq_simulacao,nr_seq_ordem,nr_seq_plano,nr_seq_vinc_sca,
					ds_item_resumo,vl_resumo,ie_tipo_pessoa,vl_pro_rata_dia,vl_antecipacao)
			values	(	pls_simulacao_resumo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					cd_estabelecimento_p,nr_seq_simulacao_p,4,0,nr_seq_sca_vinc_w,
					ds_resumo_sca_pj_w,vl_sca_w,'PJ',0,0);
		else
			update	pls_simulacao_resumo
			set	vl_resumo		= vl_sca_w,
				ds_item_resumo		= ds_resumo_sca_pj_w
			where	nr_seq_simulacao	= nr_seq_simulacao_p
			and	nr_seq_vinc_sca		= nr_seq_sca_vinc_w
			and	nr_seq_ordem		= 4
			and	ie_tipo_pessoa		= 'PJ';
		end if;	
		
		end;
	end loop;
	close C02;
end if;

end pls_alt_resumo_simul_sca;
/