create or replace procedure INSERIR_ALERTAS_ANESTESIA (
		CD_PESSOA_FISICA_P varchar2,	
        CD_PROFISSIONAL_P varchar2,
        IE_TIPO_ALERTA_P varchar2,
		NM_USUARIO_P varchar2,	
		DS_LISTA_P	varchar2) is

ds_lista_w 		varchar2(10000) := '';
posicao_virgula_w 	integer := 0;
nr_max_loop_w 		integer := 10000;
nr_sequencia_w   	number(10,0) := 0;
ds_observacao_w    varchar2(2000);
tamanho_lista_w		number(10);

			
begin
ds_lista_w := DS_LISTA_P;

while 	ds_lista_w is not null and
	nr_max_loop_w > 0 loop
	begin
	tamanho_lista_w := length(ds_lista_w);
	posicao_virgula_w := instr(ds_lista_w, ',');
	
	if	(posicao_virgula_w > 1) and
		(substr(ds_lista_w, 1, posicao_virgula_w - 1) is not null) then
		begin
		nr_sequencia_w := to_number(substr(ds_lista_w, 1, posicao_virgula_w -1));
		ds_lista_w := substr(ds_lista_w, (posicao_virgula_w +1), tamanho_lista_w);
		end;
	elsif	(ds_lista_w is not null) then
		nr_sequencia_w := to_number(replace(ds_lista_w, ',', ''));
		ds_lista_w := '';
	end if;
	
	if	(nr_sequencia_w is not null) and
		(nr_sequencia_w > 0) then
		begin	
            if (IE_TIPO_ALERTA_P = '0') then
                Select ds_observacao 
                into ds_observacao_w 
                from alerta_anestesia 
                where nr_sequencia  =  nr_sequencia_w;                                    

                INSERT INTO ALERTA_ANESTESIA_APAE 
                (NR_SEQUENCIA, CD_PESSOA_FISICA, CD_PROFISSIONAL, DT_ALERTA, DT_ATUALIZACAO, IE_TIPO_ALERTA, IE_SITUACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NR_SEQ_ALERTA, DS_OBSERVACAO)
                VALUES (ALERTA_ANESTESIA_APAE_SEQ.NEXTVAL, CD_PESSOA_FISICA_P, CD_PROFISSIONAL_P, SYSDATE, SYSDATE, nvl(IE_TIPO_ALERTA_P, 0), 'A', NM_USUARIO_P, sysdate, NM_USUARIO_P, nr_sequencia_w, ds_observacao_w);
                commit;
            else
                INSERT INTO ALERTA_ANESTESIA_APAE 
                (NR_SEQUENCIA, CD_PESSOA_FISICA, CD_PROFISSIONAL, DT_ALERTA, DT_ATUALIZACAO, IE_TIPO_ALERTA, IE_SITUACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NR_SEQ_ALERTA, NR_SEQ_DIAG,DS_OBSERVACAO)
                VALUES (ALERTA_ANESTESIA_APAE_SEQ.NEXTVAL, CD_PESSOA_FISICA_P, CD_PROFISSIONAL_P, SYSDATE, SYSDATE, nvl(IE_TIPO_ALERTA_P, 0), 'A', NM_USUARIO_P, sysdate, NM_USUARIO_P, null, nr_sequencia_w, null);
                commit;
            end if;
                        
		end;
	end if;
	nr_max_loop_w := nr_max_loop_w -1;
	end;
end loop;

commit;

end INSERIR_ALERTAS_ANESTESIA;
/