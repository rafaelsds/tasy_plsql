create or replace procedure dc_receive_remittance(
    cd_estabelecimento_p number,
    nm_usuario_p         varchar2)
is
  cd_convenio_w convenio.cd_convenio%type;
  nr_patient_account_w conta_paciente.nr_interno_conta%type;
  nr_sequencia_w number(10);
  cursor c01
  is
    select * from dc_era_report where ie_status = 1;
  c01_w c01%rowtype;
begin
  open c01;
  loop
    fetch c01 into c01_w;
    exit
  when c01%notfound;
    begin
      select obter_conversao_interna_int(null, 'CONTA_PACIENTE','INVOICE_ID',c01_w.nr_invoice_id,'DC')
      into nr_patient_account_w
      from dual;
      if (nr_patient_account_w > 0) then
        select cd_convenio_parametro
        into cd_convenio_w
        from conta_paciente
        where nr_interno_conta = nr_patient_account_w;
        select nvl(max(nr_sequencia),0)
        into nr_sequencia_w
        from convenio_receb
        where cd_convenio  = cd_convenio_w
        and ie_status     in ('N', 'P');
        if (nr_sequencia_w > 0) then
          update convenio_receb
          set vl_recebimento = vl_recebimento + c01_w.vl_deposit_amount_payment
          where nr_sequencia = nr_sequencia_w;
        else
          begin
            select convenio_receb_seq.nextval into nr_sequencia_w from dual;
            insert
            into convenio_receb
              (
                nr_sequencia,
                cd_convenio,
                dt_recebimento,
                vl_recebimento,
                dt_atualizacao,
                nm_usuario,
                ie_status, --N
                cd_estabelecimento,
                ie_integrar_cb_fluxo, --N
                ds_observacao,
                dt_liberacao
              )
              values
              (
                nr_sequencia_w,
                cd_convenio_w,
                nvl(c01_w.dt_payment_run_date,sysdate),
                c01_w.vl_deposit_amount_payment,
                sysdate,
                nm_usuario_p,
                'N',
                cd_estabelecimento_p,
                'N',
                null,
                sysdate
              );
          end;
        end if;
        update dc_era_report
        set ie_status      = 2
        where nr_sequencia = c01_w.nr_sequencia;
        commit;
      end if;
    end;
  end loop;
  close c01;
  commit;
end dc_receive_remittance;
/
