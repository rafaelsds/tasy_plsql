Create or Replace 
procedure Recalcular_Conta_Repasse	(
				nr_interno_conta_p		number,
				nr_seq_procedimento_p	number,
				nr_seq_material_p		number,
				nm_usuario_p		varchar2,
				nr_seq_partic_p		number default null) is



ie_repasse_mat_w		varchar2(1);
ie_repasse_proc_w		varchar2(1);
nr_seq_procedimento_w	number(10,0);
nr_seq_material_w		number(10,0);
nr_lote_repasse_w		number(10,0);
nr_atendimento_w		number(10,0);
vl_material_w			number(15,2);
cd_estabelecimento_w		Number(04,0);
cd_medico_resp_w		Varchar2(255);
ie_contabilizado_w		Varchar2(255);
cd_convenio_w			Number(05,0);
nr_seq_protocolo_w		Number(10,0);
cd_edicao_amb_w		Number(06,0);
ie_tipo_atendimento_w	Number(03,0);
cd_categoria_w		Varchar2(10);
ie_status_protocolo_w	Varchar2(10);
dt_entrada_w			date;
ie_recalcular_conta_prov_w	varchar2(10);
ie_recalcular_protocolo_prov_w	varchar2(10);
ie_retorno_w			varchar2(255);
ie_status_conta_w		number(15);
nr_seq_retorno_w		number(15);
nr_titulo_w			varchar2(255);
nr_nota_fiscal_w		varchar2(255);
ie_gerar_w			varchar2(1)	:= 'S';
ie_repasse_proc_conv_w		varchar2(1);
ie_repasse_mat_conv_w		varchar2(1);
nr_seq_partic_w			number(10);
ie_atual_res_conta_rec_rep_w	parametro_faturamento.ie_atual_res_conta_rec_rep%type;
ie_recalc_repasse_liberado_w	varchar2(10);
qt_repasses_liberados_w number(15);

--Retorna os procedimentos que sofrerao recalculo
cursor	c_procedimento(	nr_interno_conta_pc	conta_paciente.nr_interno_conta%type,
			nr_seq_procedimento_pc	procedimento_paciente.nr_sequencia%type,
			nr_seq_partic_pc	procedimento_participante.nr_seq_partic%type,
			ie_repasse_proc_pc	varchar2) is
	select	nr_sequencia,
		decode(nr_seq_partic_pc,0,0,null) nr_seq_partic
	from	procedimento_paciente
	where	nr_interno_conta	= nr_interno_conta_pc
	and	nr_sequencia		= nr_seq_procedimento_pc
	and	nr_seq_procedimento_pc	is not null
	and 	cd_motivo_exc_conta	is null
	and	nvl(nr_seq_partic_pc,0)	= 0
	and	ie_repasse_proc_pc	<> 'N'
	union
	select	nr_sequencia,
		decode(nr_seq_partic_pc,0,0,null) nr_seq_partic
	from	procedimento_paciente
	where	nr_interno_conta	= nr_interno_conta_pc
	and	nr_seq_procedimento_pc	is null
	and 	cd_motivo_exc_conta	is null
	and	nvl(nr_seq_partic_pc,0)	= 0
	and	ie_repasse_proc_pc	<> 'N'
	union
	select	b.nr_sequencia,
		b.nr_seq_partic
	from	procedimento_participante b,
		procedimento_paciente a
	where	a.nr_sequencia		= b.nr_sequencia
	and	a.nr_interno_conta	= nr_interno_conta_pc
	and	b.nr_sequencia		= nr_seq_procedimento_pc
	and	a.cd_motivo_exc_conta 	is null
	and	b.nr_seq_partic		= nr_seq_partic_pc
	and	ie_repasse_proc_pc	<> 'N';

--Retorna os materiais que sofrerao recalculo
cursor	c_material(	nr_interno_conta_pc	conta_paciente.nr_interno_conta%type,
			nr_seq_material_pc	material_atend_paciente.nr_sequencia%type,
			ie_repasse_proc_pc	varchar2) is
	select	nr_sequencia
	from	material_atend_paciente A
	where	nr_interno_conta	= nr_interno_conta_pc
	and	nr_sequencia		= nr_seq_material_pc
	and	nr_seq_material_pc	is not null
	and 	cd_motivo_exc_conta	is null
	and	ie_repasse_proc_pc	<> 'N'
	union
	select	nr_sequencia
	from	material_atend_paciente A
	where	nr_interno_conta	= nr_interno_conta_pc
	and	nr_seq_material_pc	is null
	and 	cd_motivo_exc_conta	is null
	and	ie_repasse_proc_pc	<> 'N';

begin

obter_Param_Usuario(89, 79, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo, ie_recalc_repasse_liberado_w);

if (nvl(ie_recalc_repasse_liberado_w, 'S') = 'N') then

  select max(a.ie_repasse_proc) ie_repasse_proc, 
         max(a.ie_repasse_mat) ie_repasse_mat
    into ie_repasse_proc_w,
         ie_repasse_mat_w
    from parametro_faturamento a 
   where a.cd_estabelecimento = obter_estabelecimento_ativo;

  select count(*)
    into qt_repasses_liberados_w
    from (select 1 
            from procedimento_repasse y, 
                 procedimento_paciente x 
           where x.nr_interno_conta        = nr_interno_conta_p 
             and x.nr_sequencia            = nvl(nr_seq_procedimento_p,x.nr_sequencia)
             and x.cd_motivo_exc_conta     is null
             and nvl(ie_repasse_proc_w,'N') <> 'N'
             and x.nr_sequencia            = y.nr_seq_procedimento
             and y.ie_status               in ('L','R','S')               
             union                
             select 1 
               from material_repasse y,
                    material_atend_paciente x 
              where x.nr_interno_conta       = nr_interno_conta_p
                and x.nr_sequencia           = nvl(nr_seq_material_p,x.nr_sequencia)
                and x.cd_motivo_exc_conta    is null
                and nvl(ie_repasse_mat_w,'N') <> 'N'
                and x.nr_sequencia           = y.nr_seq_material
                and y.ie_status              in ('L','R','S'));
                
  if (qt_repasses_liberados_w > 0) then
    wheb_mensagem_pck.exibir_mensagem_abort(126920);
  end if;
end if;

ie_gerar_w	:= 'S';
--Verifica parametro que permite se pode recalcular conta paciente com status em provisorio.
obter_Param_Usuario(89, 80, obter_perfil_ativo, nm_usuario_p, 0, ie_recalcular_conta_prov_w);

select	nvl(max(nr_lote_repasse),0),
	max(nr_atendimento),
	max(ie_status_acerto),
	max(nr_seq_protocolo)
into	nr_lote_repasse_w,
	nr_atendimento_w,
	ie_status_conta_w,
	nr_seq_protocolo_w
from	conta_paciente
where	nr_interno_conta	= nr_interno_conta_p;

--verifica se a conta ainda esta em provisorio
if	(ie_status_conta_w = 1) then
	begin
	if	(nvl(ie_recalcular_conta_prov_w, 'S') = 'N') then
		/* Nao e permitido o recalculo de contas provisorias! */
		wheb_mensagem_pck.exibir_mensagem_abort(191623);
	elsif	(nvl(ie_recalcular_conta_prov_w, 'S') = 'C') then
		ie_gerar_w	:= 'N';
	end if;
	end;
end if;

if	(ie_gerar_w	= 'S') then
	begin
	--Parametro para permitir recalcular protocolo provisorio
	obter_Param_Usuario(89, 97, obter_perfil_ativo, nm_usuario_p, 0, ie_recalcular_protocolo_prov_w);
	if	(nr_seq_protocolo_w is not null) then
		select	nvl(max(ie_status_protocolo), '1'),
			max(substr(obter_titulo_conta_protocolo(nr_seq_protocolo,0),1,254)),
			max(substr(obter_nota_conta_protocolo(nr_seq_protocolo,0),1,254))
		into	ie_status_protocolo_w,
			nr_titulo_w,
			nr_nota_fiscal_w
		from	protocolo_convenio
		where	nr_seq_protocolo		= nr_seq_protocolo_w;
	end if;	

	if	(ie_status_protocolo_w	= '1') and
		(nvl(ie_recalcular_protocolo_prov_w, 'S') = 'N') then
		/* Nao e permitido o recalculo de contas em protocolos abertos ou que nao estao em protocolo! */
		wheb_mensagem_pck.exibir_mensagem_abort(191624);
	end if;

	insert into FIN_LOG_REPASSE
		(DT_ATUALIZACAO,
		NM_USUARIO,
		CD_LOG,
		DS_LOG)
	values	(sysdate,
		nm_usuario_p,
		6040,
		wheb_mensagem_pck.get_texto(800060,'nr_interno_conta_p=' || nr_interno_conta_p));
	
	--obtem as informacoes da funcao Parametros Faturamento
	select nvl(max(c.ie_repasse_mat),'N'),
		nvl(max(c.ie_repasse_proc),'N'),
		max(a.cd_estabelecimento),
		max(b.cd_medico_resp),
		max(ie_tipo_atendimento),
		max(cd_convenio_parametro),
		max(cd_categoria_parametro),
		max(dt_entrada),
		nvl(max(ie_atual_res_conta_rec_rep),'N')
	into	ie_repasse_mat_w,
		ie_repasse_proc_w,
		cd_estabelecimento_w,
		cd_medico_resp_w,
		ie_tipo_atendimento_w,
		cd_convenio_w,
		cd_categoria_w,
		dt_entrada_w,
		ie_atual_res_conta_rec_rep_w
	from	parametro_faturamento c,
		atendimento_paciente b,
		conta_paciente a
	where	a.nr_atendimento		= b.nr_atendimento
	and	a.cd_estabelecimento	= c.cd_estabelecimento
	and	a.nr_interno_conta	= nr_interno_conta_p;
	
	--verifica o cadastro do convenio, pasta Relacionamento para saber se deve ou nao gerar o repasse
	select	Obter_Valor_Conv_Estab(cd_convenio_w,cd_estabelecimento_w,'IE_REPASSE_PROC'),
		Obter_Valor_Conv_Estab(cd_convenio_w,cd_estabelecimento_w,'IE_REPASSE_MAT')
	into	ie_repasse_proc_conv_w,
		ie_repasse_mat_conv_w
	from	dual;
	
	--Se encontra informacao no convenio, sobrescreve o que esta no Parametro Faturamento
	if	(ie_repasse_proc_conv_w is not null) then
		ie_repasse_proc_w := ie_repasse_proc_conv_w;
	end if;
	
	--Se encontra informacao no convenio, sobrescreve o que esta no Parametro Faturamento
	if	(ie_repasse_mat_conv_w is not null) then
		ie_repasse_mat_w := ie_repasse_mat_conv_w;
	end if;
		
	if	(nr_lote_repasse_w > 0) then
		obter_param_usuario(89, 59, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_contabilizado_w);
		if	(nvl(ie_contabilizado_w, 'N') = 'N') then
			wheb_mensagem_pck.exibir_mensagem_abort(168653,	'NR_ATENDIMENTO='||to_char(nr_atendimento_w)||';'||
									'NR_INTERNO_CONTA='||to_char(nr_interno_conta_p)||';'||
									'NR_LOTE_REPASSE='||to_char(nr_lote_repasse_w));	
			/* Nao e possivel recalcular o repasse de uma conta que ja esteja contabilizada em um lote de repasse!
							'Atendimento: ' || nr_atendimento_w || chr(13) ||
							'Conta paciente: ' || nr_interno_conta_p || chr(13) ||
							'Lote contabil: ' || nr_lote_repasse_w);*/
		end if;
	end if;
	
	--Parametro para proibir caso ja esteja em retorno convenio
	obter_param_usuario(89, 111, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_retorno_w);
	if	(ie_retorno_w = 'N') then
		select	max(nr_seq_retorno)
		into	nr_seq_retorno_w
		from	convenio_retorno_item
		where	nr_interno_conta	= nr_interno_conta_p;
		if	(nr_seq_retorno_w is not null) then
			/* Esta conta ja esta em retorno convenio! Nao e possivel o recalculo do mesmo. Parametro [111] */
			wheb_mensagem_pck.exibir_mensagem_abort(191625);
		end if;
	end if;
	
	--obtem a edicao AMB para o calculo do valor do repasse
	begin
	select	obter_edicao (cd_estabelecimento_w, cd_convenio_w, cd_categoria_w, dt_entrada_w, null)
	into	cd_edicao_amb_w
	from	dual;
	exception
		when others then
			select	max(cd_edicao_amb)
			into	cd_edicao_amb_w
			from	convenio_amb
			where	cd_estabelecimento	= cd_estabelecimento_w
			  and	cd_convenio		= cd_convenio_w
			  and	cd_categoria		= cd_categoria_w
			  and	(nvl(ie_situacao,'A')	= 'A')
			  and	dt_inicio_vigencia	=
				(select	max(dt_inicio_vigencia)
				from	convenio_amb a
				where	a.cd_estabelecimento  = cd_estabelecimento_w
				and	a.cd_convenio         = cd_convenio_w
				and	a.cd_categoria        = cd_categoria_w
				and	(nvl(a.ie_situacao,'A')= 'A')
				and	a.dt_inicio_vigencia <=  dt_entrada_w);
	end; 
	--Verifica se gera repasse com base nas informacoes do cadastro do convenio > pasta relacionamento
	--se nao encontrar nada no convenio, usa o que esta setado no parametros faturamento
	if	((ie_repasse_proc_w = 'C') and (ie_status_conta_w  = 2) or 
		(ie_repasse_proc_w = 'P') and (ie_status_protocolo_w = 2) or
		(ie_repasse_proc_w = 'G') and (ie_status_protocolo_w = 2) or
		((ie_repasse_proc_w = 'T') and (ie_status_protocolo_w = 2) and
		(nr_nota_fiscal_w is not null) and (nr_titulo_w is not null)) or
		((ie_recalcular_conta_prov_w	= 'S') and (ie_status_conta_w = 1))) then
		begin
		--abre o cursor de procedimentos
		for	r_c_procedimento in c_procedimento(nr_interno_conta_p, nr_seq_procedimento_p, nr_seq_partic_p, ie_repasse_proc_w) loop
			gerar_procedimento_repasse(
				r_c_procedimento.nr_sequencia,
				cd_estabelecimento_w,
				cd_medico_resp_w,
				nm_usuario_p,
				cd_edicao_amb_w,
				cd_convenio_w,
				ie_tipo_atendimento_w,
				null,
				null,
				r_c_procedimento.nr_seq_partic);
		end loop;
		end;
	end if;
	--Verifica se gera repasse com base nas informacoes do cadastro do convenio > pasta relacionamento
	--se nao encontrar nada no convenio, usa o que esta setado no parametros faturamento
	if	((ie_repasse_mat_w = 'C') and (ie_status_conta_w  = 2) or 
		(ie_repasse_mat_w = 'P') and (ie_status_protocolo_w = 2) or
		(ie_repasse_mat_w = 'G') and (ie_status_protocolo_w = 2) or
		((ie_repasse_mat_w = 'T') and (ie_status_protocolo_w = 2) and
		(nr_nota_fiscal_w is not null) and (nr_titulo_w is not null)) or
		((ie_recalcular_conta_prov_w	= 'S') and (ie_status_conta_w = 1))) then	
		begin
		--Abre o cursor de materiais
		for	r_c_material in c_material(nr_interno_conta_p, nr_seq_material_p, ie_repasse_proc_w) loop
			gerar_material_repasse	(
				r_c_material.nr_sequencia,
				cd_estabelecimento_w,
				cd_medico_resp_w,
				nm_usuario_p,
				cd_edicao_amb_w,
				cd_convenio_w,
				ie_tipo_atendimento_w,
				null,
				null);
		      
		end loop;
		end;
	end if;	
	
	--Verifica se atualiza o resumo da conta com base nas informacoes do cadastro do convenio > pasta relacionamento
	--se nao encontrar nada no convenio, usa o que esta setado no parametros faturamento
	if	(((ie_repasse_proc_w = 'C') and (ie_status_conta_w  = 2) or 
		(ie_repasse_proc_w = 'P') and (ie_status_protocolo_w = 2) or
		(ie_repasse_proc_w = 'G') and (ie_status_protocolo_w = 2) or
		((ie_repasse_proc_w = 'T') and (ie_status_protocolo_w = 2) and
		(nr_nota_fiscal_w is not null) and (nr_titulo_w is not null)) or
		((ie_recalcular_conta_prov_w	= 'S') and (ie_status_conta_w = 1))) or		
		((ie_repasse_mat_w = 'C') and (ie_status_conta_w  = 2) or 
		(ie_repasse_mat_w = 'P') and (ie_status_protocolo_w = 2) or
		(ie_repasse_mat_w = 'G') and (ie_status_protocolo_w = 2) or
		((ie_repasse_mat_w = 'T') and (ie_status_protocolo_w = 2) and
		(nr_nota_fiscal_w is not null) and (nr_titulo_w is not null)) or
		((ie_recalcular_conta_prov_w	= 'S') and (ie_status_conta_w = 1)))) then
		begin
		if	(nr_interno_conta_p is not null) then
			gerar_procmat_repasse_nf(nr_interno_conta_p, nm_usuario_p, 'S');
			
			if	(ie_atual_res_conta_rec_rep_w = 'S') then
				atualizar_resumo_conta(nr_interno_conta_p,ie_status_conta_w);
			end if;			
		end if;
		end;
	end if;	
	end;	
end if;

end Recalcular_Conta_Repasse;
/