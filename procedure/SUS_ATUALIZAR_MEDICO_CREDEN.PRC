Create or Replace
procedure sus_atualizar_medico_creden(	nm_usuario_p	in	varchar2) is


cd_pessoa_fisica_w	varchar2(20);

cursor	c01 is
Select	b.cd_pessoa_fisica
from	medico b 
where	ie_corpo_clinico = 'S'
and 	not exists(	select	x.cd_medico 
			from 	sus_medico_credenciamento x	
			where 	x.cd_medico = b.cd_pessoa_fisica);
BEGIN

open 	c01;
loop
fetch c01 	into
		cd_pessoa_fisica_w;
	exit when c01%notfound;
		begin
		insert into sus_medico_credenciamento(	nr_sequencia,
						cd_medico,
						dt_atualizacao,
						ie_auditor,
						ie_conveniado,
						ie_credenciamento,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						ie_situacao)
		select	sus_medico_credenciamento_seq.nextval,
			b.cd_pessoa_fisica,
                        sysdate,
			'N',
			'S',
			'30',
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			'A'
		from 	medico b
		where 	ie_corpo_clinico = 'S'
		and 	not exists(	select x.cd_medico 
					from 	sus_medico_credenciamento x 	
					where 	x.cd_medico = b.cd_pessoa_fisica);
		end;
end loop;
close c01;
commit;

END sus_atualizar_medico_creden;
/
