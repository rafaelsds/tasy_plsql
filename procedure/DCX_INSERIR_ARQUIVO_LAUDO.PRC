create or replace
procedure dcx_inserir_arquivo_laudo (	nr_acesso_dicom_p	varchar2,
										ds_arquivo_p		varchar2) is 

nr_sequencia_w	number(10);										
										
begin

select	max(d.nr_sequencia) 
into	nr_sequencia_w
from   	prescr_medica a,
		prescr_procedimento b,
		procedimento_paciente c,
		laudo_paciente d
where  	a.nr_prescricao = b.nr_prescricao
and	   	b.nr_acesso_dicom = nr_acesso_dicom_p
and	   	c.nr_prescricao = b.nr_prescricao
and	   	c.nr_sequencia_prescricao = b.nr_sequencia
and	   	d.nr_sequencia = c.nr_laudo;

update	laudo_paciente
set		ds_arquivo = ds_arquivo_p
where	nr_sequencia = nr_sequencia_w;

commit;

end dcx_inserir_arquivo_laudo;
/