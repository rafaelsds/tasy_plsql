create or replace
procedure CONSISTE_DIAS_CONV_QUIMIO
			(cd_estabelecimento_p	in number,
			nr_seq_paciente_p	in number,			
			dt_prevista_p		in date) is
	
cd_convenio_w		paciente_setor_convenio.cd_convenio%type;
qt_dias_autor_quimio_w	convenio_estabelecimento.qt_dias_autor_quimio%type;
	
begin

begin
select	cd_convenio
into	cd_convenio_w
from	paciente_setor_convenio
where	nr_seq_paciente	= nr_seq_paciente_p
and	rownum = 1;
exception
when others then
	cd_convenio_w	:= null;	
end;

if	(cd_convenio_w is not null) then

	select	max(qt_dias_autor_quimio)
	into	qt_dias_autor_quimio_w
	from	convenio_estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	cd_convenio		= cd_convenio_w;
	
	if	(nvl(qt_dias_autor_quimio_w,0) > 0) then
	
		if	(trunc(dt_prevista_p) < trunc(sysdate + qt_dias_autor_quimio_w)) then
			wheb_mensagem_pck.exibir_mensagem_abort(267536,	'QT_DIAS_AUTOR_QUIMIO_W='||qt_dias_autor_quimio_w||';'||
									'DT_PREVISTA_CONV_W='||to_char(trunc(sysdate + qt_dias_autor_quimio_w),'dd/mm/yyyy')||';'||
									'DT_PREVISTA_W='||to_char(dt_prevista_p,'dd/mm/yyyy')||';');
		end if;
	end if;
end if;

end CONSISTE_DIAS_CONV_QUIMIO;
/