create or replace
procedure com_reprovar_duplic(	nr_seq_cliente_p		number,
				nm_usuario_p			varchar2) is 
				
nr_seq_gestor_w		number(10);
nr_seq_cliente_w	number(10);
nr_seq_canal_w		number(10);
nr_seq_canal_ww		number(10);
cd_pessoa_fisica_w	number(10);
ds_email_w		varchar(255);
				
Cursor C01 is
	select	nr_sequencia, cd_pessoa_fisica
	from	com_cliente_gestor
	where	nr_seq_cliente = nr_seq_cliente_p
	and	dt_final is null
	order by 1;
	
Cursor C02 is
	select	nr_sequencia
	from	com_canal_cliente
	where	nr_seq_cliente = nr_seq_cliente_p
	and	dt_fim_atuacao is null
	and	ie_situacao = 'A'
	order by 1;	
begin
if	(nr_seq_cliente_p is not null) then
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_gestor_w,
		cd_pessoa_fisica_w;
	exit when C01%notfound;
		begin
		delete com_cliente_gestor
		where	nr_sequencia = nr_seq_gestor_w;
		end;
	end loop;
	close C01;
	
	open C02;
	loop
	fetch C02 into	
		nr_seq_canal_w;
	exit when C02%notfound;
		begin
		delete com_canal_cliente
		where	nr_sequencia = nr_seq_canal_w;
		end;
	end loop;
	close C02;
	
	select	max(nr_seq_canal)
	into	nr_seq_canal_ww
	from	com_canal_cliente
	where	nr_seq_cliente = nr_seq_cliente_w
	and	nr_sequencia = (select	max(nr_sequencia)
				from	com_canal_cliente
				where	nr_seq_cliente = nr_seq_cliente_w);
				
	delete 	com_cliente_hist 
	where	nr_seq_cliente = nr_seq_cliente_p;
	
	select 	substr(obter_dados_pf_pj(cd_pessoa_fisica_w, null, 'M'),1,255)
	into	ds_email_w
	from	dual;
	
	delete com_cliente 
	where	nr_sequencia = nr_seq_cliente_p;

enviar_email(wheb_mensagem_pck.get_texto(802674), wheb_mensagem_pck.get_texto(802675,'nr_seq_cliente_p=' || nr_seq_cliente_p) , 'comercial@wheb.com.br', 'comercial@wheb.com.br;' || ds_email_w, nm_usuario_p, 'A');							

end if;

commit;

end com_reprovar_duplic;
/