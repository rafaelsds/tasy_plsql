CREATE OR REPLACE
PROCEDURE Tasy_Atualizar_Base_versao(nm_owner_origem_p Varchar2) IS 
nm_owner_origem_w varchar2(15);

BEGIN

nm_owner_origem_w := nm_owner_origem_p;

if	( nm_owner_origem_p is null ) or
	( nm_owner_origem_p = '') then
	nm_owner_origem_w := 'TASY_VERSAO';
end if;

gravar_processo_longo('','TASY_ATUALIZAR_BASE_VERSAO',0);
begin
tasy_consistir_dados_versao;
exception
when others then
	null;
end;	
/*OS 763053*/
Tasy_Copiar_Tabela(nm_owner_origem_p, 'DIC_EXPRESSAO', 'N', 'S', 'where not exists (select 1 from DIC_EXPRESSAO b where a.cd_expressao = b.cd_expressao)');		
Tasy_Atualizar_Tabela(nm_owner_origem_p, 'DIC_EXPRESSAO', 'DIC_EXPRESSAO', 'where a.dt_atualizacao > (select b.dt_atualizacao from DIC_EXPRESSAO b where a.cd_expressao = b.cd_expressao)');

Tasy_Atualizar_Tabela(nm_owner_origem_p,'DIC_EXPRESSAO_IDIOMA','DIC_EXPRESSAO_IDIOMA','where a.dt_atualizacao > (select b.dt_atualizacao from DIC_EXPRESSAO_IDIOMA b where a.nr_Sequencia = b.nr_Sequencia)');
Tasy_Copiar_tabela(nm_owner_origem_p,'DIC_EXPRESSAO_IDIOMA','N','S','where not exists (select b.dt_atualizacao from DIC_EXPRESSAO_IDIOMA b where a.nr_Sequencia = b.nr_sequencia)');

/* dicionario principal */
tasy_copiar_tabela(nm_owner_origem_p,'tabela_sistema','N','S',
	'where not exists (select 1 from tabela_sistema b where a.nm_tabela = b.nm_tabela)');
Tasy_Atualizar_tabela(nm_owner_origem_p,'tabela_sistema','tabela_sistema',
	'WHERE a.nm_tabela = b.nm_tabela and A.DT_ATUALIZACAO Between b.dt_atualizacao +1/86400 and sysdate', 'S', 'S', 'S');

tasy_copiar_tabela(nm_owner_origem_p,'tabela_atributo','N','S',
	'where not exists (select 1 from tabela_atributo b where a.nm_tabela = b.nm_tabela and a.nm_atributo = b.nm_atributo)');
Tasy_Atualizar_tabela(nm_owner_origem_p,'tabela_atributo','tabela_atributo',
	'where a.nm_tabela = b.nm_tabela and a.nm_atributo = b.nm_atributo and a.dt_atualizacao between b.dt_atualizacao +1/86400 and sysdate', 'S', 'S', 'S');

tasy_copiar_tabela(nm_owner_origem_p,'indice','N','S',
	'where not exists (select 1 from indice b where a.nm_tabela = b.nm_tabela and a.nm_indice = b.nm_indice)');
Tasy_Atualizar_tabela(nm_owner_origem_p,'indice','indice',
	'where a.nm_tabela = b.nm_tabela and a.nm_indice = b.nm_indice and a.dt_Atualizacao between b.dt_Atualizacao +1/86400 and sysdate', 'S', 'S', 'S');

tasy_copiar_tabela(nm_owner_origem_p,'indice_atributo','N','S',
	'where not exists (select 1 from indice_atributo b where a.nm_tabela = b.nm_tabela and a.nm_indice = b.nm_indice and a.nr_sequencia = b.nr_sequencia)');
Tasy_Atualizar_tabela(nm_owner_origem_p,'indice_atributo','indice_atributo',
	'WHERE a.nm_tabela = b.nm_tabela and a.nm_indice = b.nm_indice and a.nr_sequencia = b.nr_sequencia and a.dt_Atualizacao between b.dt_Atualizacao +1/86400 and sysdate', 'S', 'S', 'S');	
	
tasy_copiar_tabela(nm_owner_origem_p,'integridade_referencial','N','S',
	'where not exists (select 1 from integridade_referencial b where a.nm_tabela = b.nm_tabela and a.nm_integridade_referencial = b.nm_integridade_referencial)');
Tasy_Atualizar_tabela(nm_owner_origem_p,'integridade_referencial','integridade_referencial',
	'where a.nm_tabela = b.nm_tabela and a.nm_integridade_referencial = b.nm_integridade_referencial and a.dt_atualizacao between b.dt_atualizacao +1/86400 and sysdate', 'S', 'S', 'S');
	
tasy_copiar_tabela(nm_owner_origem_p,'integridade_atributo','N','S',
	'where not exists (select 1 from integridade_atributo b where a.nm_tabela = b.nm_tabela and a.nm_integridade_referencial = b.nm_integridade_referencial and a.nm_atributo = b.nm_atributo)');
Tasy_Atualizar_tabela(nm_owner_origem_p,'integridade_atributo','integridade_atributo',
	'Where a.nm_tabela = b.nm_tabela and a.nm_integridade_referencial = b.nm_integridade_referencial and a.nm_atributo = b.nm_atributo and a.dt_Atualizacao between b.dt_atualizacao  +1/86400 and sysdate', 'S', 'S', 'S');

tasy_copiar_tabela(nm_owner_origem_p,'tabela_visao','N','S',
	'where not exists (select 1 from tabela_visao b where a.nr_sequencia = b.nr_sequencia)');
Tasy_Atualizar_tabela(nm_owner_origem_p,'tabela_visao','tabela_visao',
	'where a.nr_sequencia = b.nr_sequencia and a.dt_Atualizacao between b.dt_atualizacao  +1/86400 and sysdate', 'S', 'S', 'S');

tasy_copiar_tabela(nm_owner_origem_p,'tabela_visao_atributo','N','S',
	'where not exists (select 1 from tabela_visao_atributo b where a.nr_sequencia = b.nr_sequencia and a.nm_atributo = b.nm_atributo)');
Tasy_Atualizar_tabela(nm_owner_origem_p,'tabela_visao_atributo','tabela_visao_atributo',
	'WHERE a.nr_sequencia = b.nr_sequencia and a.nm_atributo = b.nm_atributo and a.dt_Atualizacao between b.dt_atualizacao  +1/86400 and sysdate', 'S', 'S', 'S');

begin
Tasy_Atualizar_Tabela(nm_owner_origem_p,'OBJETO_SISTEMA','OBJETO_SISTEMA',
	' where a.dt_atualizacao > (select b.dt_atualizacao from OBJETO_SISTEMA b where a.nm_objeto = b.nm_objeto)');
Tasy_Copiar_tabela(nm_owner_origem_p,'OBJETO_SISTEMA','N','S',' where not exists (select 1 from OBJETO_SISTEMA b '||
	' where a.nm_objeto = b.nm_objeto)');
exception
when others then
	null;
end;

begin
Tasy_Atualizar_Tabela(nm_owner_origem_p,'OBJETO_SISTEMA_PARAM','OBJETO_SISTEMA_PARAM',
	' where a.dt_atualizacao > (select b.dt_atualizacao from OBJETO_SISTEMA_PARAM b where a.nr_sequencia = b.nr_sequencia)');
Tasy_Copiar_tabela(nm_owner_origem_p,'OBJETO_SISTEMA_PARAM','N','S',' where not exists (select 1 from OBJETO_SISTEMA_PARAM b '||
	' where a.nr_sequencia = b.nr_sequencia)');
exception
when others then
	null;
end;	

/* Habilitar constraints */
tasy_enable_constraint;
tasy_enable_constraint;

END Tasy_Atualizar_Base_versao;
/
