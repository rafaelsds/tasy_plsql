create or replace
procedure lic_gerar_itens_fornecedor(		cd_estabelecimento_p	number,
					nr_sequencia_p		number,
					ie_opcao_p		number,
					nr_seq_lic_item_p		number,
					nm_usuario_p		Varchar2) is
					
/* ie_opcao_p
0 = Somente os itens que foram selecionados na lista
1 = Todos os itens da licita��o
2 = Somente os itens da regra do fornecedor*/

nr_seq_licitacao_w			number(10);
nr_seq_lic_item_w			number(5);
nr_seq_lic_item_ww			number(5);
cd_material_w			number(6);
qt_item_w			number(15,4);
cd_material_ww			number(6);
qt_item_ww			number(15,4);
qt_existe_w			number(10);
nr_seq_parecer_w			number(10);
nr_seq_reg_lic_fornec_w		number(10);
cd_cgc_fornec_w			varchar2(14);
nr_lote_compra_w			varchar2(80);
					
cursor c01 is
select	nr_seq_lic_item,
	cd_material,
	qt_item,
	nr_lote_compra
from	reg_lic_item
where	nr_seq_licitacao	= nr_seq_licitacao_w
and	ie_opcao_p	= 1
and	nr_seq_lote is null
union all
select	nr_seq_lic_item,
	cd_material,
	qt_item,
	nr_lote_compra
from	reg_lic_item
where	nr_seq_licitacao	= nr_seq_licitacao_w
and	ie_opcao_p	= 2
and	nr_seq_lote is null
and	obter_se_item_regra_fornec(cd_estabelecimento_p, cd_cgc_fornec_w, cd_material) = 'S';

cursor c02 is
select	cd_material,
	qt_item,
	nr_seq_lic_item
from	reg_lic_item
where	nr_seq_licitacao = nr_seq_licitacao_w
and	nr_lote_compra = nr_lote_compra_w
and	cd_material is not null;

begin

select	nr_seq_licitacao,
	cd_cgc_fornec
into	nr_seq_licitacao_w,
	cd_cgc_fornec_w
from	reg_lic_fornec
where	nr_sequencia = nr_sequencia_p;

if	(ie_opcao_p = 0) then
	begin

	select	max(cd_material),
		max(qt_item),
		max(nr_lote_compra)
	into	cd_material_w,
		qt_item_w,
		nr_lote_compra_w
	from	reg_lic_item
	where	nr_seq_licitacao	= nr_seq_licitacao_w
	and	nr_seq_lic_item	= nr_seq_lic_item_p;

	select	reg_lic_item_fornec_seq.nextval
	into	nr_seq_reg_lic_fornec_w		
	from	dual;
	
	insert into reg_lic_item_fornec(	
		nr_sequencia,				dt_atualizacao,			nm_usuario,
		nr_seq_fornec,				nr_seq_lic_item,			nr_seq_parecer,
		ie_vencedor,				vl_item,				vl_original,
		dt_atualizacao_nrec,			nm_usuario_nrec,			cd_material,
		nr_seq_licitacao,				ie_qualificado,			ie_qualif_lance_fornec,
		qt_item)
	values(	nr_seq_reg_lic_fornec_w,			sysdate,				nm_usuario_p,
		nr_sequencia_p,				nr_seq_lic_item_p,			'',
		'N',					0,				0,
		sysdate,					nm_usuario_p,			cd_material_w,
		nr_seq_licitacao_w,				'S',				'S',
		qt_item_w);

	if	(cd_material_w is null) and
		(nr_lote_compra_w is not null) then
	
		open C02;
		loop
		fetch C02 into	
			cd_material_ww,
			qt_item_ww,
			nr_seq_lic_item_ww;
		exit when C02%notfound;
			begin
			
			insert into reg_lic_item_fornec_lote(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_lic_item_fornec,
				nr_lote_compra,
				cd_material,
				qt_item,
				vl_item,
				nr_seq_licitacao,
				nr_seq_lic_item)
			values(	reg_lic_item_fornec_lote_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_reg_lic_fornec_w,
				nr_lote_compra_w,
				cd_material_ww,
				qt_item_ww,
				0,
				nr_seq_licitacao_w,
				nr_seq_lic_item_ww);
			
			end;
		end loop;
		close C02;
	
	end if;		
		
	end;
elsif	(ie_opcao_p in (1,2)) then
	begin
	open C01;
	loop
	fetch C01 into	
		nr_seq_lic_item_w,
		cd_material_w,
		qt_item_w,
		nr_lote_compra_w;
	exit when C01%notfound;
		begin
		
		select	count(*)
		into	qt_existe_w
		from	reg_lic_item_fornec
		where	nr_seq_licitacao	= nr_seq_licitacao_w
		and	nr_seq_lic_item	= nr_seq_lic_item_w
		and	nr_seq_fornec	= nr_sequencia_p;
		
		if	(qt_existe_w = 0) then
			
			select	reg_lic_item_fornec_seq.nextval
			into	nr_seq_reg_lic_fornec_w
			from	dual;
			
			insert into reg_lic_item_fornec(	
				nr_sequencia,				dt_atualizacao,			nm_usuario,
				nr_seq_fornec,				nr_seq_lic_item,			nr_seq_parecer,
				ie_vencedor,				vl_item,				vl_original,
				dt_atualizacao_nrec,			nm_usuario_nrec,			cd_material,
				nr_seq_licitacao,				ie_qualificado,			ie_qualif_lance_fornec,
				qt_item)
			values(	nr_seq_reg_lic_fornec_w,			sysdate,				nm_usuario_p,
				nr_sequencia_p,				nr_seq_lic_item_w,			'',
				'N',					0,				0,
				sysdate,					nm_usuario_p,			cd_material_w,
				nr_seq_licitacao_w,				'S',				'S',
				qt_item_w);
			
			if	(cd_material_w is null) and
				(nr_lote_compra_w is not null) then
	
				open C02;
				loop
				fetch C02 into	
					cd_material_ww,
					qt_item_ww,
					nr_seq_lic_item_ww;
				exit when C02%notfound;
					begin
					
					insert into reg_lic_item_fornec_lote(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_lic_item_fornec,
						nr_lote_compra,
						cd_material,
						qt_item,
						vl_item,
						nr_seq_licitacao,
						nr_seq_lic_item)
					values(	reg_lic_item_fornec_lote_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_reg_lic_fornec_w,
						nr_lote_compra_w,
						cd_material_ww,
						qt_item_ww,
						0,
						nr_seq_licitacao_w,
						nr_seq_lic_item_ww);
					
					end;
				end loop;
				close C02;
			end if;				
		end if;
		end;
	end loop;
	close C01;
	end;
end if;

commit;

end lic_gerar_itens_fornecedor;
/
