create or replace
procedure auto_atend_atualizar_cad(cd_pessoa_fisica_p varchar2,
                                    nr_telefone_celular_p varchar2,
                                    cd_cep_p varchar2,
                                    ds_email_p varchar2,
                                    nr_endereco_p varchar2,
                                    nr_telefone_p varchar2,
                                    ds_complemento_p varchar2) is
                                    
qt_acomp_w              number(2);
ie_cep_internet_w       varchar2(2 char);
ds_tipo_logradouro_w	  varchar2(125 char);
nm_logradouro_w         compl_pessoa_fisica.ds_endereco%type;
nm_localidade_w         compl_pessoa_fisica.ds_municipio%type;
nm_bairro_w             compl_pessoa_fisica.ds_bairro%type;
cd_unidade_federacao_w  compl_pessoa_fisica.sg_estado%type;
cd_tipo_logradouro_w    compl_pessoa_fisica.cd_tipo_logradouro%type;
cd_municipio_ibge_w     compl_pessoa_fisica.cd_municipio_ibge%type;
nr_seq_compl_w		compl_pessoa_fisica.nr_sequencia%type;
begin
  
  if (cd_cep_p is not null) and (cd_cep_p <> 'LIMPAR') then
    Obter_Param_Usuario(0, 25, obter_perfil_ativo, obter_usuario_ativo, obter_estabelecimento_ativo, ie_cep_internet_w);

    if	(ie_cep_internet_w = 'S') then
    
      select	max(a.nm_logradouro),
          max(a.nm_localidade),
          max(a.nm_bairro),
          max(a.cd_unidade_federacao)
      into	nm_logradouro_w,
            nm_localidade_w,
            nm_bairro_w,
            cd_unidade_federacao_w
      from	cep_logradouro a
      where	a.cd_logradouro = cd_cep_p;
      
    else
    
      select max(a.nm_logradouro),
            max(b.nm_localidade),
            max(c.ds_bairro),
            max(a.ds_uf)
      into	nm_logradouro_w,
            nm_localidade_w,
            nm_bairro_w,
            cd_unidade_federacao_w
      from	cep_loc b,          
            cep_bairro c,       
            cep_log a           
      where	b.nr_sequencia = c.nr_seq_loc  
      and	a.cd_bairro_inicial = c.nr_sequencia
      and	b.nr_sequencia = a.nr_seq_loc 
      and	a.cd_cep = cd_cep_p;
      
    end if;
    
    cd_municipio_ibge_w := obter_municipio_ibge(ELIMINA_CARACTERE_ESPECIAL(cd_cep_p));
    if cd_municipio_ibge_w = 0 then
      cd_municipio_ibge_w := null;
    end if;

    select	max(ds_tipo_logradouro)
    into	ds_tipo_logradouro_w
    from	cep_logradouro_v
    where	cd_cep = cd_cep_p;

    select	max(cd_tipo_logradouro)
    into	cd_tipo_logradouro_w
    from	sus_tipo_logradouro
    where	upper(ds_tipo_logradouro) = upper(nvl(ds_tipo_logradouro_w, ' '));
  
  end if;

  select count(1)
  into qt_acomp_w
  from compl_pessoa_fisica
  where ie_tipo_complemento = 1
  and cd_pessoa_fisica  = cd_pessoa_fisica_p;
  
  if qt_acomp_w > 0 then
  
    update compl_pessoa_fisica
    set ds_email =    decode(ds_email_p, 'LIMPAR', null, nvl(ds_email_p,ds_email)),
        nr_endereco = decode(nr_endereco_p, 'LIMPAR', null, nvl(nr_endereco_p,nr_endereco)),
        nr_telefone = decode(nr_telefone_p, 'LIMPAR', null, nvl(nr_telefone_p,nr_telefone)),
        ds_complemento = decode(ds_complemento_p, 'LIMPAR', null, nvl(ds_complemento_p,ds_complemento)),
        cd_cep =      decode(cd_cep_p, 'LIMPAR', null, nvl(cd_cep_p, cd_cep)),
        ds_endereco = decode(cd_cep_p, 'LIMPAR', null, nvl(nm_logradouro_w, ds_endereco)),
        ds_bairro =   decode(cd_cep_p, 'LIMPAR', null, nvl(nm_bairro_w, ds_bairro)),
        cd_tipo_logradouro = decode(cd_cep_p, 'LIMPAR', null, nvl(cd_tipo_logradouro_w, cd_tipo_logradouro)),
        ds_municipio =       decode(cd_cep_p, 'LIMPAR', null, nvl(nm_localidade_w, ds_municipio)),
        cd_municipio_ibge = decode(cd_cep_p, 'LIMPAR', null, nvl(cd_municipio_ibge_w, cd_municipio_ibge)),
        sg_estado =         decode(cd_cep_p, 'LIMPAR', null, nvl(cd_unidade_federacao_w, sg_estado)),
        nm_usuario = 'autoAtend',
        dt_atualizacao = sysdate
    where	cd_pessoa_fisica = cd_pessoa_fisica_p
		and	ie_tipo_complemento = 1;
  
  else

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_compl_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
    insert into compl_pessoa_fisica(
      nr_sequencia,
      ds_email,
      nr_endereco,
      nr_telefone,
      ds_complemento,
      cd_cep,
      ds_endereco,
      ds_bairro,
      cd_tipo_logradouro,
      ds_municipio,
      cd_municipio_ibge,
      sg_estado,
      nm_usuario,
      nm_usuario_nrec,
      dt_atualizacao,
      dt_atualizacao_nrec,
      cd_pessoa_fisica,
      ie_tipo_complemento
    ) values (
      nr_seq_compl_w,
      decode(ds_email_p, 'LIMPAR', null, ds_email_p),
      decode(nr_endereco_p, 'LIMPAR', null, nr_endereco_p),
      decode(nr_telefone_p, 'LIMPAR', null, nr_telefone_p),
      decode(ds_complemento_p, 'LIMPAR', null, ds_complemento_p),
      decode(cd_cep_p, 'LIMPAR', null, cd_cep_p),
      nm_logradouro_w,
      nm_bairro_w,
      cd_tipo_logradouro_w,
      nm_localidade_w,
      cd_municipio_ibge_w,
      cd_unidade_federacao_w,
      'autoAtend',
      'autoAtend',
      sysdate,
      sysdate,
      cd_pessoa_fisica_p,
      1
    );
  end if;
  
  if nr_telefone_celular_p is not null then
  
    update pessoa_fisica
    set nr_telefone_celular = decode(nr_telefone_celular_p, 'LIMPAR', null, nr_telefone_celular_p),
        nm_usuario = 'autoAtend',
        dt_atualizacao = sysdate
    where cd_pessoa_fisica = cd_pessoa_fisica_p;
  
  end if;
  
  commit;

end auto_atend_atualizar_cad;
/
