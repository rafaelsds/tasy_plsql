create or replace
procedure rxt_atualizar_dt_inicio_trat(	nr_seq_agenda_p		number,
				nr_seq_tratamento_p	number,
				nm_usuario_p		varchar2) is

dt_agenda_w		date;
nr_seq_tratamento_w	number(10,0);
begin

if	(nr_seq_agenda_p is not null) and
	(nr_seq_tratamento_p is not null) and
	(nm_usuario_p is not null) then
	begin	
	/* Obter data da agenda*/
	select	a.dt_agenda
	into	dt_agenda_w
	from	agenda_consulta a
	where	a.nr_sequencia = nr_seq_agenda_p;
	
	if	(dt_agenda_w is not null) then
		begin		
		/* Atualiza a data de in�cio de tratamento */
		update	rxt_tratamento
		set	dt_inicio_trat	= dt_agenda_w,
			dt_atualizacao	= sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_tratamento_p;
		end;
	end if;	
	end;
end if;

commit;

end rxt_atualizar_dt_inicio_trat;
/