create or replace
procedure labweb_recupera_senha( ds_email_p		varchar2,
				dt_nascimento_p		Varchar2,
				ie_tipo_login_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	varchar2,
				ds_mensagem_p		out varchar2 ) is 

cd_pessoa_fisica_w	varchar2(255);
nm_usuario_email_w	varchar2(255);
nr_atendimento_w	varchar2(25);
ds_senha_w		varchar2(20);
nr_crm_w		varchar2(20);
uf_crm_w		varchar2(2);
ds_email_origem_w	usuario.ds_email%type;
	
begin	

obter_param_usuario(80, 29, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, nm_usuario_email_w);
	
if	(ds_email_p is not null) and 
	(dt_nascimento_p is not null) then
	
	select  MAX(a.cd_pessoa_fisica)
	into 	cd_pessoa_fisica_w
	from  	pessoa_fisica a,
		compl_pessoa_fisica b
	where 	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	b.ie_tipo_complemento = 1
	and	b.ds_email is not null
	and	a.dt_nascimento = to_date (dt_nascimento_p,'dd/mm/yyyy')
	and	upper(trim(b.ds_email)) = upper(trim(ds_email_p));
	
	
	if	(cd_pessoa_fisica_w is null) then
	
		select  MAX(a.cd_pessoa_fisica)
		into 	cd_pessoa_fisica_w
		from  	pessoa_fisica a,
			compl_pessoa_fisica b
		where 	a.cd_pessoa_fisica = b.cd_pessoa_fisica
		and	b.ie_tipo_complemento = 3
		and	b.ds_email is not null
		and	a.dt_nascimento = to_date (dt_nascimento_p,'dd/mm/yyyy')
		and	upper(trim(b.ds_email)) = upper(trim(ds_email_p));
	end if;
	
	if (cd_pessoa_fisica_w is not null) then
	
		select 	nvl(MAX(ds_email),ds_email_p)
		into	ds_email_origem_w
		from	usuario 
		where  	nm_usuario = nm_usuario_email_w;
		
		
		if (ie_tipo_login_p = 'A') then
			select 	MAX(nr_atendimento)
			into 	nr_atendimento_w
			from 	atendimento_paciente
			where 	cd_pessoa_fisica = cd_pessoa_fisica_w;
			
			select 	MAX(ds_senha)
			into 	ds_senha_w
			from 	atendimento_paciente
			where 	nr_atendimento = nr_atendimento_w;
			
			if 	(nr_atendimento_w is not null) and
				(ds_senha_w is not null) then
				enviar_email('Laborat�rio Web - Recupera��o de senhas', '=========================================='||CHR(10)||
				CHR(13)||'Recupera��o de senha do laborat�rio web'||CHR(10)||CHR(13)||'=========================================='||CHR(10)||
				CHR(13)||'Senha recuperada com sucesso!'||CHR(10)||
				CHR(13)||'O seu atendimento atual �: '||nr_atendimento_w||' e senha: '||ds_senha_w, ds_email_origem_w, ds_email_p, nm_usuario_email_w, 'A');
				ds_mensagem_p := 'E-mail enviado!';
			end if;
			
			
		elsif (ie_tipo_login_p = 'P') then
			select 	MAX(ds_senha)				
			into	ds_senha_w
			from	pessoa_fisica
			where	cd_pessoa_fisica = cd_pessoa_fisica_w;
			
			if 	(ds_senha_w is not null) then
				enviar_email('Laborat�rio Web - Recupera��o de senhas', '=========================================='||CHR(10)||
				CHR(13)||'Recupera��o de senha do laborat�rio web'||CHR(10)||CHR(13)||'=========================================='||CHR(10)||
				CHR(13)||'Senha recuperada com sucesso!'||CHR(10)||
				CHR(13)||'O seu c�digo �: '||cd_pessoa_fisica_w||' e senha: '||ds_senha_w, ds_email_origem_w, ds_email_p, nm_usuario_email_w, 'A');
				ds_mensagem_p := 'E-mail enviado!';
			end if;
			
		elsif (ie_tipo_login_p = 'M') then
			select 	MAX(ds_senha),
				MAX(nr_crm),
				MAX(uf_crm)
			into	ds_senha_w,
				nr_crm_w,
				uf_crm_w
			from	medico
			where	cd_pessoa_fisica = cd_pessoa_fisica_w;
			
			if 	(ds_senha_w is not null) then
				enviar_email('Laborat�rio Web - Recupera��o de senhas', '=========================================='||CHR(10)||
				CHR(13)||'Recupera��o de senha do laborat�rio web'||CHR(10)||CHR(13)||'=========================================='||CHR(10)||
				CHR(13)||'Senha recuperada com sucesso!'||CHR(10)||
				CHR(13)||'O seu c�digo CRM-UF �: '||nr_crm_w||'-'||uf_crm_w||' e senha: '||ds_senha_w, ds_email_origem_w, ds_email_p, nm_usuario_email_w, 'A');
				ds_mensagem_p := 'E-mail enviado!';
			end if;
		end if;
			
	else
		ds_mensagem_p := 'E-mail e/ou data de nascimento incorreta!';
	end if;
		
end if;

commit;

end labweb_recupera_senha;
/