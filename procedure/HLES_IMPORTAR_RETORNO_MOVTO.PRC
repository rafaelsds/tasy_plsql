create or replace
procedure HLES_IMPORTAR_RETORNO_MOVTO(nr_seq_retorno_p	number) is

nr_sequencia_w		number(10);
nr_sequencia_min_w	number(10);
nr_protocolo_w		varchar2(255);
dt_competencia_w	varchar2(20);
ds_doc_fiscal_w		varchar2(255);
nr_doc_convenio_w	varchar2(255);
dt_atendimento_w	date;
nm_paciente_w		varchar2(255);
vl_lib_conta_w		number(17,4);
vl_pago_w		number(17,4);
vl_glosa_conta_w	number(17,4);
vl_cobrado_w		number(17,4);
nm_usuario_w		varchar2(15) := wheb_usuario_pck.get_nm_usuario;

cursor C01 is
	select	nr_sequencia,
		substr(obter_valor_campo_separador(ds_conteudo,2,';'),1,255) nr_protocolo,
		substr(obter_valor_campo_separador(ds_conteudo,7,';'),1,255) nr_doc_convenio,
		substr(obter_valor_campo_separador(ds_conteudo,9,';'),1,255) nm_paciente,
		replace(obter_valor_campo_separador(ds_conteudo,12,';'),'.','') vl_lib_conta,
		replace(obter_valor_campo_separador(ds_conteudo,13,';'),'.','') vl_glosa_conta
	from	w_conv_ret_movto
	where	nr_seq_retorno = nr_seq_retorno_p
	and	nr_sequencia > (nr_sequencia_min_w +2)
	order by nr_sequencia;

begin

select	min(nr_sequencia)
into	nr_sequencia_min_w
from	w_conv_ret_movto
where	nr_seq_retorno = nr_seq_retorno_p;

begin
open C01;
loop
fetch C01 into
	nr_sequencia_w,
	nr_protocolo_w,
	nr_doc_convenio_w,
	nm_paciente_w,
	vl_lib_conta_w,
	vl_glosa_conta_w;
exit when C01%notfound;
	begin

	begin
	vl_pago_w 	:= to_number(vl_lib_conta_w);
	exception
	when others then
		vl_pago_w	:= 0;
	end;
	
	insert 	into convenio_retorno_movto
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_Seq_retorno,
		nr_doc_convenio,
		vl_pago,
		vl_total_pago,
		ds_complemento,
		vl_glosa,
		vl_cobrado)
	values	(convenio_retorno_movto_seq.nextval,
		sysdate,
		nm_usuario_w,
		nr_seq_retorno_p,
		nr_doc_convenio_w,
		vl_pago_w,
		vl_pago_w,
		nm_paciente_w,
		vl_glosa_conta_w,
		(vl_pago_w + vl_glosa_conta_w));
			
	end;
end loop;
close C01;
exception
when others then
	delete 	from w_conv_ret_movto
	where	nr_Seq_retorno	= nr_seq_retorno_p; 
	commit;

	raise_application_error(-20011,	' GUIA:'||nr_doc_convenio_w||chr(13)||
						' PAGO:'||vl_pago_w||chr(13)||
						' PROTOCOLO:'||nr_protocolo_w||chr(13));
end;

delete 	from w_conv_ret_movto
where	nr_seq_retorno = nr_seq_retorno_p; 
commit;

end HLES_IMPORTAR_RETORNO_MOVTO;
/