create or replace
procedure ctb_gerar_relat_base_ecd(	cd_empresa_p		number,
					cd_estab_p		number,
					dt_inicial_p		date,
					dt_final_p		date,
					nm_usuario_p		varchar2) is 

/*variaveis gerais */
cd_empresa_w				empresa.cd_empresa%type;
cd_estabelecimento_w			estabelecimento.cd_estabelecimento%type;
cd_consistencia_w			w_ctb_sped_consistencia.cd_consistencia%type;
ds_consistencia_w			w_ctb_sped_consistencia.ds_consistencia%type;
i					integer	:= 0;
type registros is table of w_ctb_sped_consistencia%rowtype index by pls_integer;
consistencia_w				registros;


/*vari�veis que obtem os dados para consist�ncia */
cd_contabilista_w			empresa.cd_contabilista%type;
cd_titular_w				empresa.cd_titular%type;
qt_registro_null_w			number;
qt_cd_versao_w				number;
qt_classif_ecd_w			number;
qt_demonstrativo_J100_w		number;	
qt_demonstrativo_J150_w		number;
qt_mes_aberto_w				number;
qt_coluna_demos_mes_w		number;
cd_encerramento_exercicio_w	number;
qt_lote_encerramento_w		number;
qt_registro_sped_w			number;
qt_conta_contabil_w			number;

nr_seq_controle_sped_w		ctb_sped_controle.nr_sequencia%type;
nr_seq_demo_bp_w			ctb_sped_controle.nr_seq_demo_bp%type;
nr_crc_w					estabelecimento.nr_crc%type;	
cd_pf_auditor_w				estabelecimento.cd_pf_auditor%type;
cd_cnpj_auditor_w			estabelecimento.cd_cnpj_auditor%type;	
dt_ref_inicial_w			ctb_sped_controle.dt_ref_inicial%type;
dt_ref_final_w				ctb_sped_controle.dt_ref_final%type;
nr_cpf_w					pessoa_fisica.nr_cpf%type;
ds_email_w					compl_pessoa_fisica.ds_email%type;
nr_telefone_w				compl_pessoa_fisica.nr_telefone%type;
ds_codigo_prof_w			pessoa_fisica.ds_codigo_prof%type;
nr_reg_junta_comercial_w	estabelecimento.nr_reg_junta_comercial%type;
cd_conta_contabil_w			conta_contabil.cd_conta_contabil%type;
cd_localidade_w				estabelecimento.cd_localidade%type;
cd_tipo_lote_w				tipo_lote_contabil.cd_tipo_lote_contabil%type;
nr_lote_contabil_existe_w	lote_contabil.nr_lote_contabil%type;
mes_ref_final_w				date;
nr_seq_mes_ref_w	  		ctb_mes_ref.nr_sequencia%type;
qt_registro_w				number(10);
qt_movimento_periodo_w		number(10);
pr_mov_sem_agrup_w			number(15,2);


begin
delete	w_ctb_sped_consistencia
where	nr_seq_controle_sped is null
and	nm_usuario		= nm_usuario_p;
commit;

cd_empresa_w		:= cd_empresa_p;
cd_estabelecimento_w	:= cd_estab_p;
dt_ref_inicial_w	:= dt_inicial_p;
dt_ref_final_w		:= dt_final_p;
mes_ref_final_w 	:= TRUNC(dt_ref_inicial_w, 'MM');

select	count(*) 
into	qt_mes_aberto_w
from	ctb_mes_ref
where	dt_referencia between dt_inicial_p and dt_final_p
and	dt_fechamento is null;

begin

select	nr_sequencia 
into	nr_seq_mes_ref_w
from	ctb_mes_ref
where	cd_empresa = cd_empresa_w
and	dt_referencia = mes_ref_final_w;
exception when others then
	nr_seq_mes_ref_w	:= null;
end;

select	count(*)
into	qt_demonstrativo_J100_w
from	ctb_demonstrativo a,
	ctb_modelo_relat b
where 	a.nr_seq_tipo = b.nr_sequencia	
and	a.nr_seq_mes_ref = nr_seq_mes_ref_w
and 	b.cd_registro_sped = 'J100';

select	count(*)
into	qt_demonstrativo_J150_w
from	ctb_demonstrativo a,
	ctb_modelo_relat b
where 	a.nr_seq_tipo = b.nr_sequencia	
and	a.nr_seq_mes_ref = nr_seq_mes_ref_w
and 	b.cd_registro_sped = 'J150';

--#################################

/* 1 - falta informar o contabilista no cadastro da empresa */

select	cd_contabilista,
	cd_titular, 
	nr_crc, 
	cd_pf_auditor, 
	cd_cnpj_auditor
into	cd_contabilista_w,
	cd_titular_w,
	nr_crc_w,
	cd_pf_auditor_w,	
	cd_cnpj_auditor_w
from	empresa
where	cd_empresa =	 cd_empresa_w;


if	(nvl(cd_estabelecimento_w, 0) != 0) then
	select 	nvl(nr_crc,nr_crc_w),
		nvl(cd_pf_auditor, cd_pf_auditor_w),
		nvl(cd_cnpj_auditor, cd_cnpj_auditor_w),
		nvl(cd_contabilista, cd_contabilista_w),
		nr_reg_junta_comercial,
		cd_localidade
	into	nr_crc_w,
		cd_pf_auditor_w,
		cd_cnpj_auditor_w,
		cd_contabilista_w,
		nr_reg_junta_comercial_w,
		cd_localidade_w 	
	from	estabelecimento 
	where	cd_estabelecimento =	cd_estabelecimento_w;
end if;




if	(nvl(cd_contabilista_w,'0') <> '0') then
	select 	max(ds_codigo_prof)
	into	ds_codigo_prof_w
	from	pessoa_fisica	
	where	cd_pessoa_fisica = 	cd_contabilista_w;
end if;

select  count(*)
into	qt_cd_versao_w
from 	conta_contabil_referencial
where	cd_versao = '3.0';

select	count(*)
into 	qt_classif_ecd_w 
from	conta_contabil_classif_ecd a,
	conta_contabil b
where	a.cd_conta_contabil	= b.cd_conta_contabil
and	b.cd_empresa		= cd_empresa_w;

if	(nvl(cd_contabilista_w,'X') = 'X')  then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 1;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o contabilista no cadastro da empresa ou estabelecimento';
	consistencia_w(i).ds_acao		:= 'Cadastrar o contabilista na fun��o Empresa/Estabelecimento/Conta/CC';
	consistencia_w(i).qt_tempo		:= 1;
end if;

if	(nvl(cd_titular_w,'X')  = 'X')then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 2;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o titular da empresa ou estabelecimento';
	consistencia_w(i).ds_acao		:= 'Cadastrar o titular na fun��o Empresa/Estabelecimento/Conta/CC';
	consistencia_w(i).qt_tempo		:= 1;
end if;

select	count(*)
into	qt_registro_null_w
from	ctb_mes_ref
where	cd_empresa = cd_empresa_w
and  	dt_referencia between	dt_ref_inicial_w and dt_ref_final_w
and	dt_reorg_lancto is null;

if	(qt_registro_null_w > 0) then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 3;
	consistencia_w(i).ds_consistencia	:= 'Falta reorganizar o lan�amento cont�bil do per�odo';
	consistencia_w(i).ds_acao		:= 'Executar a funcionalidade Reorganizar lancamento cont�bil na fun��o Contabilidade pasta Par�metros';
	consistencia_w(i).qt_tempo		:= 4;
end if;


if	(nvl(nr_crc_w,'X') = 'X') then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 4;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o CRC do Contador no cadastro da Empresa ou Estabelecimento';
	consistencia_w(i).ds_acao		:= 'Cadastrar o CRC do contador respons�vel na fun��o Empresa/Estabelecimento/Conta/CC';
	consistencia_w(i).qt_tempo		:= 1;
end if;

if	(nvl(cd_pf_auditor_w,'X') = 'X') and 
	(nvl(cd_cnpj_auditor_w, 'X') = 'X') then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 5;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o auditor da empresa/estabelecimento';
	consistencia_w(i).ds_acao		:= 'Cadastrar o auditor na fun��o Empresa/Estabelecimento/Conta/CC';
	consistencia_w(i).qt_tempo		:= 1;
end if;

select	max(nr_cpf)
into	nr_cpf_w 	
from	pessoa_fisica
where	cd_pessoa_fisica = cd_titular_w;
	
if	(nvl(nr_cpf_w,'X') = 'X') then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 6;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o CPF no cadastro do Titular';
	consistencia_w(i).ds_acao		:= 'Cadastrar o CPF do titular na fun��o Cadastro Completo de Pessoas';
	consistencia_w(i).qt_tempo		:= 1;
end if;

select	max(nr_cpf)
into	nr_cpf_w 	
from	pessoa_fisica
where	cd_pessoa_fisica = cd_contabilista_w;

if	(nvl(cd_contabilista_w,'X') <> 'X') and
	(nvl(nr_cpf_w,'X') = 'X') then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 7;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o CPF no cadastro do Contador';
	consistencia_w(i).ds_acao		:= 'Cadastrar o CPF do contador na fun��o Cadastro Completo de Pessoas';
	consistencia_w(i).qt_tempo		:= 1;
end if;


if	(nvl(cd_titular_w,'X') <> 'X') then
	select	max(ds_email),
		max(nr_telefone)
	into	ds_email_w, 
		nr_telefone_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica	= cd_titular_w
	and	ie_tipo_complemento	= 2;

end if;

if	(nvl(ds_email_w,'X') = 'X') or
	(nvl(nr_telefone_w,'X') = 'X')then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 8;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o Email/telefone no cadastro do Titular';
	consistencia_w(i).ds_acao		:= 'Cadastrar o email/telefone do titular na fun��o Cadastro Completo de Pessoas, Tipo de complemento Comercial';
	consistencia_w(i).qt_tempo		:= 1;
end if;

if	(nvl(cd_contabilista_w, 'X') <> 'X') then
	begin
	select	ds_email,
		nr_telefone
	into	ds_email_w, 
		nr_telefone_w
	from	compl_pessoa_fisica
	where	cd_pessoa_fisica = cd_contabilista_w
	and	ie_tipo_complemento = 2;
	exception when others then
		ds_email_w	:= '';
		nr_telefone_w	:= '';
	end;
end if;
	
if	(nvl(cd_contabilista_w, 'X') <> 'X') and
	((nvl(ds_email_w,'X') = 'X') or
	(nvl(nr_telefone_w,'X') = 'X')) then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 9;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o Email/telefone no cadastro do Contador';
	consistencia_w(i).ds_acao		:= 'Cadastrar o email/telefone do titular na fun��o Cadastro Completo de Pessoas, Tipo de complemento Comercial';
	consistencia_w(i).qt_tempo		:= 1;
end if;

if	(nvl(ds_codigo_prof_w,'X') = 'X')then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 10;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o c�digo do profissional (Contador) no cadastro de Pessoa f�sica';
	consistencia_w(i).ds_acao		:= 'Cadastrar o c�digo do profissional (contador) na fun��o Cadastro Completo de Pessoas';
	consistencia_w(i).qt_tempo		:= 1;
end if;


if	(nvl(nr_reg_junta_comercial_w,'X') = 'X')then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 11;
	consistencia_w(i).ds_consistencia	:= 'Falta informar o NIRE no cadastro do Estabelecimento';
	consistencia_w(i).ds_acao		:= 'Cadastrar o Registro junta comercial na fun��o Empresa/Estabelecimento/Conta/CC pasta Estabelecimento';
	consistencia_w(i).qt_tempo		:= 1;
end if;

if	(qt_cd_versao_w = 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 12;
	consistencia_w(i).ds_consistencia	:= 'N�o foi importado o Plano de Contas Referencial Vers�o 3.0 ';
	consistencia_w(i).ds_acao		:= 'Executar a funcionalidade Importar plano de contas referencial na fun��o Empresa/Estabelecimento/Conta/CC pasta Conta Referencial';
	consistencia_w(i).qt_tempo		:= 2;
end if;


if	(qt_classif_ecd_w = 0)then
	i					:= i + 1;
	select	count(*)
	into 	qt_classif_ecd_w 
	from	conta_contabil 
	where 	ie_tipo		= 'A'
	and   	ie_situacao	= 'A'
	and	  	cd_empresa	= cd_empresa_w;	
	
	consistencia_w(i).cd_consistencia	:= 13;
	consistencia_w(i).ds_consistencia	:= 'O Plano de contas n�o foi vinculado ao plano de contas referencial. Total(' || Trim(TO_CHAR(qt_classif_ecd_w, '999G9999G999')) || ')';
	consistencia_w(i).ds_acao		:= 'Vincular o plano de contas na fun��o Empresa/Estabelecimento/Conta/CC pasta Conta Cont�bil subpasta Conta cont�bil referencial';
	consistencia_w(i).qt_tempo		:= 1;
		
end if;

if	(nvl(cd_localidade_w,0) = 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 14;
	consistencia_w(i).ds_consistencia	:= 'N�o foi informada a Localidade no Cadastro do Estabelecimento';
	consistencia_w(i).ds_acao		:= 'Cadastrar a localidade fun��o Empresa/Estabelecimento/Conta/CC pasta Estabelecimento';
	consistencia_w(i).qt_tempo		:= 1;
end if;

select	count(*)
into	qt_registro_w
from	ctb_modelo_relat a
where	a.cd_registro_sped	= 'J100'
and	a.cd_empresa		= cd_empresa_w
and	a.ie_situacao		= 'A';

if	(qt_registro_w = 0) then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 15;
	consistencia_w(i).ds_consistencia	:= 'Falta cadastrar modelo de demonstrativo para o Registro J100 - Balan�o Patrimonial';
	consistencia_w(i).ds_acao		:= 'Cadastrar um modelo de demonstrativo para o Registro J100 - Balan�o Patrimonial';
	consistencia_w(i).qt_tempo		:= 2;
end if;

select	count(*)
into	qt_registro_w
from	ctb_modelo_relat a
where	a.cd_registro_sped	= 'J150'
and	a.cd_empresa		= cd_empresa_w
and	a.ie_situacao		= 'A';

if	(qt_registro_w = 0) then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 16;
	consistencia_w(i).ds_consistencia	:= 'Falta cadastrar modelo de demonstrativo para o Registro J150 - DRE';
	consistencia_w(i).ds_acao		:= 'Cadastrar um modelo de demonstrativo para o Registro J150 - DRE';
	consistencia_w(i).qt_tempo		:= 2;
end if;


if	(qt_demonstrativo_J100_w = 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 17;
	consistencia_w(i).ds_consistencia	:= 'Falta cadastrar o Demonstrativo de Balan�o Patrimonial no m�s de encerramento';
	consistencia_w(i).ds_acao		:= 'Cadastrar e calcular o demonstrativo na fun��o Contabilidade, pasta Demonstrativos, no m�s de encerramento do per�odo';
	consistencia_w(i).qt_tempo		:= 1;
end if;

if	(qt_demonstrativo_J150_w = 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 18;
	consistencia_w(i).ds_consistencia	:= 'Falta cadastrar o Demonstrativo de Resultado do Exerc�cio no m�s de encerramento';
	consistencia_w(i).ds_acao		:= 'Cadastrar e calcular o demonstrativo na fun��o Contabilidade, pasta Demonstrativos, no m�s de encerramento do per�odo';
	consistencia_w(i).qt_tempo		:= 1;
end if;

select	count(b.nr_sequencia)
into	qt_registro_w
from	ctb_mes_ref a,
	ctb_movimento b
where	b.nr_seq_mes_ref	= a.nr_sequencia
and	a.cd_empresa		= cd_empresa_w
and	b.cd_estabelecimento	= nvl(cd_estabelecimento_w, b.cd_estabelecimento)
and	a.dt_referencia between dt_ref_inicial_w and dt_ref_final_w;

qt_movimento_periodo_w	:= qt_registro_w;

if	(qt_registro_w > 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 19;
	consistencia_w(i).ds_consistencia	:= 'Quantidade de movimentos do per�odo: ' || Trim(TO_CHAR(qt_registro_w, '999G9999G999'));
		consistencia_w(i).ds_acao		:= 'Verificar movimentos';
	
	if	(qt_registro_w < 50000) then
		consistencia_w(i).qt_tempo		:= 1;
		
	elsif 	(qt_registro_w > 50000) and
		(qt_registro_w < 200000) then
		consistencia_w(i).qt_tempo		:= 4;
	elsif	(qt_registro_w > 200000) then
		consistencia_w(i).qt_tempo		:= 8;	
	end if;	
	
end if;

select	count(b.nr_sequencia)
into	qt_registro_w
from	ctb_mes_ref a,
	ctb_movimento b
where	b.nr_seq_mes_ref	= a.nr_sequencia
and	a.cd_empresa		= cd_empresa_w
and	b.cd_estabelecimento	= nvl(cd_estabelecimento_w, b.cd_estabelecimento)
and	a.dt_referencia between dt_ref_inicial_w and dt_ref_final_w
and	nvl(b.nr_seq_agrupamento,0) = 0;

if	(qt_registro_w > 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 20;
	consistencia_w(i).ds_consistencia	:= 'Quantidade de movimentos s/agrupador: ' || Trim(TO_CHAR(qt_registro_w, '999G9999G999'));
	consistencia_w(i).ds_acao		:= substr('A falta do agrupador nos movimentos cont�beis pode gerar problemas na identifica��o dos lan�amentos cont�beis.'
										|| 'Execute na Contabilidade a funcionalidade Reorganizar lan�amento cont�bil. Se ap�s a execu��o da funcionalidade '
										|| 'ainda tiverem movimentos com problemas de identifica��o deve ser feito o vinculo das contrapartidas MANUALMENTE '
										|| 'conforme orienta��es dispon�veis no Manual da Escritura��o Cont�bil Digital dispon�vel na extranet',1,4000);
	
	pr_mov_sem_agrup_w := dividir_sem_round(qt_registro_w,qt_movimento_periodo_w) * 100;
	
	if	(pr_mov_sem_agrup_w >= 50) then
		consistencia_w(i).qt_tempo		:= 16;
	elsif	(pr_mov_sem_agrup_w > 25) and
		(pr_mov_sem_agrup_w < 50) then
		consistencia_w(i).qt_tempo		:= 12;
	elsif	(pr_mov_sem_agrup_w > 10) and
		(pr_mov_sem_agrup_w < 25) then
		consistencia_w(i).qt_tempo		:= 10;
	elsif	(pr_mov_sem_agrup_w <= 10) then
		consistencia_w(i).qt_tempo		:= 8;
	end if;	
		

end if;

if	(qt_mes_aberto_w > 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 21;
	consistencia_w(i).ds_consistencia	:= 'Quantidade de meses abertos na Contabilidade: ' || Trim(TO_CHAR(qt_mes_aberto_w, '999G9999G999'));
	consistencia_w(i).ds_acao		:= 'Fechar o m�s na Contabilidade';
	consistencia_w(i).qt_tempo		:= 2;
end if;


select	max(nr_sequencia)
into	nr_seq_controle_sped_w
from	ctb_sped_controle
where	cd_empresa = cd_empresa_w
and		cd_estabelecimento = cd_estabelecimento_w
and		dt_ref_inicial = dt_ref_inicial_w
and		dt_ref_final = mes_ref_final_w;

select	max(nr_seq_demo_bp)
into	nr_seq_demo_bp_w
from	ctb_sped_controle
where   nr_sequencia = nr_seq_controle_sped_w;

select	count(*)
into	qt_coluna_demos_mes_w
from	ctb_demonstrativo a,
		ctb_demo_mes b
where	a.nr_sequencia = b.nr_seq_demo
and		a.nr_sequencia = nr_seq_demo_bp_w;

if	(qt_coluna_demos_mes_w < 2)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 22;
	consistencia_w(i).ds_consistencia	:= 'A estrutura do demonstrativo de Balan�o est� incorreta. Verifique se foram adicionadas todas as colunas necess�rias';
	consistencia_w(i).ds_acao		:= 'Cadastrar conforme modelo explicado no Manual de Escritura��o Cont�bil Digital dispon�vel na Extranet';
	consistencia_w(i).qt_tempo		:= 2;
end if;

cd_encerramento_exercicio_w := nvl(obter_valor_param_usuario(923, 6, obter_perfil_ativo,  nm_usuario_p, cd_estabelecimento_w),13);

select count(*)
into	qt_lote_encerramento_w
from	lote_contabil a,
	estabelecimento b
where	b.cd_estabelecimento		= a.cd_estabelecimento
and	b.cd_empresa			= cd_empresa_w
and	a.cd_estabelecimento		= nvl(cd_estabelecimento_w, a.cd_estabelecimento)
and	trunc(dt_referencia, 'MM')	= trunc(mes_ref_final_w,'mm')
and	a.cd_tipo_lote_contabil		= cd_encerramento_exercicio_w;

if	(qt_lote_encerramento_w = 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 23;
	consistencia_w(i).ds_consistencia	:= 'N�o existe lote de encerramento de exerc�cio';
	consistencia_w(i).ds_acao		:= 'Na fun��o Contabilidade, no m�s de encerramento deve ser executada a funcionalidade Encerramento Exerc�cio';
	consistencia_w(i).qt_tempo		:= 2;
end if;

/*Consiste diferen�a de saldo entre as contas do balancete no per�odo */

select	count(*)
into	qt_registro_w
from	estabelecimento c,
	lote_contabil b,
	ctb_movimento a
where	c.cd_estabelecimento	= b.cd_estabelecimento
and	b.nr_lote_contabil	= a.nr_lote_contabil
and	c.cd_empresa		= cd_empresa_w
and	b.cd_estabelecimento	= nvl(cd_estabelecimento_w, b.cd_estabelecimento)
and	a.dt_movimento between dt_ref_inicial_w and dt_ref_final_w
and	a.vl_movimento < 0;

if	(qt_registro_w > 0) then
	begin
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 24;
	consistencia_w(i).ds_consistencia	:= 'Existem movimentos negativos na contabilidade: ' || qt_registro_w;
	consistencia_w(i).ds_acao		:= substr('Fun��o Contabilidade - pasta Consulta movimento para encontrar o movimento negativo. Depois deve ser ajustado o movimento no lote',1,255);
	consistencia_w(i).qt_tempo		:= 2;
	end;
else
	begin
	select	count(*)
	into	qt_registro_w
	from	ctb_movto_centro_custo d,
		estabelecimento c,
		lote_contabil b,
		ctb_movimento a
	where	c.cd_estabelecimento	= b.cd_estabelecimento
	and	b.nr_lote_contabil	= a.nr_lote_contabil
	and	d.nr_seq_movimento	= a.nr_sequencia
	and	c.cd_empresa		= cd_empresa_w
	and	b.cd_estabelecimento	= nvl(cd_estabelecimento_w, b.cd_estabelecimento)
	and	a.dt_movimento between dt_ref_inicial_w and dt_ref_final_w
	and	d.vl_movimento < 0;
	
	if	(qt_registro_w > 0) then
		begin
		i					:= i + 1;
		consistencia_w(i).cd_consistencia	:= 24;
		consistencia_w(i).ds_consistencia	:= 'Existem movimentos negativos na contabilidade: ' || qt_registro_w;
		consistencia_w(i).ds_acao		:= substr('Fun��o Contabilidade - pasta Consulta movimento para encontrar o movimento negativo. Depois deve ser ajustado o movimento no lote',1,255);
		consistencia_w(i).qt_tempo		:= 2;
		end;
	end if;
	
	end;
end if;

/*Fim consist�ncia de diferen�a de saldo */


select	count(*)
into	qt_registro_sped_w
from	ctb_regra_sped;

if	(qt_registro_sped_w = 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 25;
	consistencia_w(i).ds_consistencia	:= 'N�o existe regra de gera��o do ECD';
	consistencia_w(i).ds_acao		:= 'Verificar regra de gera��o do ECD';
	consistencia_w(i).qt_tempo		:= 4;
end if;

select	count(*)
into	qt_conta_contabil_w
from	conta_contabil a
where 	ie_tipo		= 'A'
and   	ie_situacao	= 'A'
and		cd_empresa	= cd_empresa_w
and		obter_se_conta_vigente2(a.cd_conta_contabil, a.dt_inicio_vigencia, a.dt_fim_vigencia, dt_ref_final_w) = 'S'
and		ctb_obter_nivel_conta(a.cd_conta_contabil) < 4;

if	(qt_conta_contabil_w > 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 26;
	consistencia_w(i).ds_consistencia	:= 'Existem contas cont�beis anal�ticas que n�o est�o no quarto n�vel';
	consistencia_w(i).ds_acao		:= substr('Fun��o Empresa/Estabelecimento - ajustar a classifica��o das contas anal�ticas que n�o est�o no 4� n�vel',1,255);
	consistencia_w(i).qt_tempo		:= ((qt_conta_contabil_w * 5) / 60);
end if;


obter_valor_dinamico('select count(*) from v$parameter where name = ''utl_file_dir'' and value = ''*'' ', qt_registro_w);

if	(qt_registro_w = 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 27;
	consistencia_w(i).ds_consistencia	:= 'Falta parametrizar o recurso de UTL_FILE no banco de dados';
	consistencia_w(i).ds_acao		:= 'Contate o DBA imediatamente para configurar o par�metro UTL_FILE_DIR para *';
	consistencia_w(i).qt_tempo		:= 4;
end if;

for i in 1.. consistencia_w.count  loop   
   
insert into w_ctb_sped_consistencia(  
	nr_sequencia,         
	dt_atualizacao,
	nm_usuario,          
	cd_consistencia,    
	ds_consistencia,      
	nr_seq_controle_sped,
	ie_tipo,                      
	ds_acao,
	qt_tempo)
values(	w_ctb_sped_consistencia_seq.nextval,
	sysdate,
	nm_usuario_p,
	consistencia_w(i).cd_consistencia,
	consistencia_w(i).ds_consistencia,
	null,   
	consistencia_w(i).ie_tipo,
	consistencia_w(i).ds_acao,
	consistencia_w(i).qt_tempo);  
end loop;

commit;

end ctb_gerar_relat_base_ecd;
/