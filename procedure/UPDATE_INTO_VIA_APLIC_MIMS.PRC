create or replace 
procedure	update_into_via_aplic_mims(	cd_imp_material_p in  imp_mat_via_aplic.cd_material%type,
					cd_material_w in  mat_via_aplic.cd_material%type,
					IE_VIA_APLICACAO_P imp_mat_via_aplic.IE_VIA_APLICACAO%type) as
					imp_material_w  imp_mat_via_aplic%rowtype;

cursor c_imp_app_mims_unit_w is  
	select *
	from	imp_mat_via_aplic a 
	where  	a.cd_material = cd_imp_material_p and a.IE_VIA_APLICACAO = IE_VIA_APLICACAO_P
	and 	a.ie_dirty_check = 1; 
begin

for r_imp_app_mims_unit_w in c_imp_app_mims_unit_w loop
	if ( r_imp_app_mims_unit_w.cd_material is not null ) then
	
		update mat_via_aplic a 
		set    	a.dt_atualizacao = r_imp_app_mims_unit_w.dt_atualizacao, 
			a.nm_usuario = r_imp_app_mims_unit_w.nm_usuario, 
			a.ie_via_aplicacao = r_imp_app_mims_unit_w.ie_via_aplicacao,
			a.ds_recomendacao = r_imp_app_mims_unit_w.ds_recomendacao, 
			a.ie_diluicao = r_imp_app_mims_unit_w.ie_diluicao, 
			a.cd_setor_atendimento = r_imp_app_mims_unit_w.cd_setor_atendimento, 
			a.cd_setor_excluir = r_imp_app_mims_unit_w.cd_setor_excluir,
			a.ie_recomendada = r_imp_app_mims_unit_w.ie_recomendada, 
			a.nr_seq_prioridade = r_imp_app_mims_unit_w.nr_seq_prioridade, 
			a.cd_estabelecimento = r_imp_app_mims_unit_w.cd_estabelecimento
		where  	a.cd_material = cd_material_w and IE_VIA_APLICACAO = IE_VIA_APLICACAO_P;
	
	end if;
end loop;

update	imp_mat_via_aplic a 
set    	a.ie_dirty_check = 0 
where  	a.cd_material = cd_imp_material_p and IE_VIA_APLICACAO = IE_VIA_APLICACAO_P;

end;
/
