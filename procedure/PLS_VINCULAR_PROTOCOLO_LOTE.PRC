create or replace
procedure pls_vincular_protocolo_lote
			(	nr_seq_lote_p		Number,
				nr_seq_protocolo_p	Number,
				nm_usuario_p		Varchar2) is 

nr_seq_prestador_w		Number(10);

begin

select	nvl(max(nr_seq_prestador),0)
into	nr_seq_prestador_w
from	pls_lote_protocolo
where	nr_sequencia	= nr_seq_lote_p;

update	pls_prot_conta_titulo
set	nr_seq_lote		= nr_seq_lote_p
where	nr_seq_protocolo	= nr_seq_protocolo_p
and	nr_seq_prestador	= nr_seq_prestador_w;

update	pls_protocolo_conta
set	nr_seq_lote	= nr_seq_lote_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_protocolo_p;
				
commit;

end pls_vincular_protocolo_lote;
/
