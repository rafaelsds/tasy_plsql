create or replace
procedure pls_desfazer_prog_contr
			(	nr_seq_lote_p	number,
				nm_usuario_p	Varchar2) is 
			
nr_seq_prog_reajuste_w		number(10);

Cursor C01 is
	select	nr_sequencia
	from	pls_prog_reaj_coletivo
	where	nr_seq_lote	= nr_seq_lote_p;
			
begin

open C01;
loop
fetch C01 into	
	nr_seq_prog_reajuste_w;
exit when C01%notfound;
	begin
	
	delete	pls_prog_reaj_coletivo
	where	nr_sequencia	= nr_seq_prog_reajuste_w;
	
	end;
end loop;
close C01;

update	pls_prog_reaj_colet_lote
set	dt_geracao_lote	= null,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_lote_p;

commit;

end pls_desfazer_prog_contr;
/
