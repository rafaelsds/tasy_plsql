create or replace
procedure suspender_horarios_pendentes( ie_tipo_item_p 	varchar2,
										nr_atendimento_p 	number,
										nr_prescricao_p 	number,
										nr_seq_proced_p 	varchar2,
										nm_usuario_p 	varchar2) is 

nr_seq_proc_hor_w	number(10,0);
dt_horario_proc_w	date;
cd_procedimento_w	number(15,0);
ie_ctrl_glic_w		varchar2(15);
ie_tipo_item_w		varchar2(10);

cursor	c01 is
select	a.nr_sequencia,
		a.dt_horario,
		a.cd_procedimento,
		obter_ctrl_glic_proc(nr_seq_proc_interno)
from	prescr_proc_hor a
where	a.nr_prescricao		= nr_prescricao_p
and		a.nr_seq_procedimento 	= nr_seq_proced_p
and		a.dt_suspensao		is null
and 	a.dt_fim_horario 	is null;

begin
if(ie_tipo_item_p = 'G') then
	open C01;
	loop
	fetch C01 into	
		nr_seq_proc_hor_w,
		dt_horario_proc_w,
		cd_procedimento_w,
		ie_ctrl_glic_w;
	exit when C01%notfound;
		begin
		if	(ie_ctrl_glic_w = 'CCG') then
			ie_tipo_item_w	:= 'G';
		else	
			ie_tipo_item_w	:= 'C';
		end if;	
				
		if	(nr_seq_proc_hor_w is not null) then
			suspender_prescr_proc_hor(nr_seq_proc_hor_w, nm_usuario_p);
			Gerar_Alter_Hor_Prescr_Adep(nr_atendimento_p, ie_tipo_item_w, cd_procedimento_w, nr_prescricao_p, nr_seq_proced_p, nr_seq_proc_hor_w,dt_horario_proc_w, 12, null, obter_desc_expressao(729667), null, nm_usuario_p);
		end if;
		end;
	end loop;
	close C01;	
end if;
end suspender_horarios_pendentes;
/
