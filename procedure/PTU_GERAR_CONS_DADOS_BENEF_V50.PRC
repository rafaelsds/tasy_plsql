create or replace
procedure ptu_gerar_cons_dados_benef_v50
			(	cd_unimed_benef_p		Number,
				cd_usuario_plano_p		Varchar2,
				nm_beneficiario_p		Varchar2,
				sobrenome_benef_p		Varchar2,
				dt_nascimento_p			Varchar2,
				nr_versao_ptu_p			Varchar2,
				cd_estabelecimento_p		Number,
				nm_usuario_p			Varchar2,
				nr_seq_retorno_p	out	Number) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Gerar a transa��o do PTU que realiza a consulta de dados do benefici�rio, atrav�s do SCS
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_cooperativa_w		varchar2(10);
cd_unimed_w			Number(4);
dt_nascimento_w			Date;
cd_usuario_plano_w		Varchar2(13);

begin
if	(dt_nascimento_p	= '  /  /    ') then
	dt_nascimento_w	:= null;
else
	dt_nascimento_w	:= to_date(dt_nascimento_p, 'dd/mm/yyyy');
end if;

if	((length(cd_usuario_plano_p) < 17) or (length(cd_usuario_plano_p) > 17)) then
	wheb_mensagem_pck.exibir_mensagem_abort(190565);
	--A carteirinha deve conter 17 d�gitos !
end if;

cd_cooperativa_w	:= pls_obter_unimed_estab(cd_estabelecimento_p);
cd_unimed_w		:= substr(cd_usuario_plano_p,1,4);
cd_usuario_plano_w	:= substr(cd_usuario_plano_p,5,17);

select	ptu_consulta_beneficiario_seq.NextVal
into	nr_seq_retorno_p
from	dual;

insert	into ptu_consulta_beneficiario
	(nr_sequencia, cd_transacao, ie_tipo_cliente,
	 cd_unimed_executora, cd_unimed_beneficiario, nr_seq_execucao,
	 dt_atualizacao, nm_usuario, cd_unimed,
	 cd_usuario_plano, dt_nascimento, nm_beneficiario,
	 sobrenome_beneficiario, nr_seq_guia, nr_seq_requisicao,
	 nm_usuario_nrec, dt_atualizacao_nrec, nr_versao)
values	(nr_seq_retorno_p, '00412', 'U',
	 cd_cooperativa_w, cd_unimed_benef_p, nr_seq_retorno_p,
	 sysdate, nm_usuario_p, cd_unimed_w,
	 cd_usuario_plano_w, dt_nascimento_w, substr(nm_beneficiario_p,1,25),
	 substr(sobrenome_benef_p,1,10), nr_seq_retorno_p, null,
	 nm_usuario_p, sysdate, nr_versao_ptu_p);

commit;

end ptu_gerar_cons_dados_benef_v50;
/