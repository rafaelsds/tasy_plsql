create or replace
procedure desfazer_fechamento_fluxo(	nm_usuario_p		varchar2,
					nr_sequencia_p		number) is 

begin
if	(nm_usuario_p is not null) and
	(nr_sequencia_p is not null) then
	begin
	delete from	fluxo_caixa_fechamento
	where		nr_sequencia = nr_sequencia_p;
	end;
end if;	
commit;

end desfazer_fechamento_fluxo;
/