create or replace
procedure desvincula_cirurgia_agenda	(
					nr_sequencia_p		Number,
					ie_opcao_p		varchar2,
					nr_cirurgia_p		number,
					nm_usuario_p		Varchar2,
					cd_estabelecimento_p	number
					) IS 
					
/*
ie_opcao_p
C - Cirurgia
P - Prescrição
*/					

ie_atualiza_data_w     varchar2(1);	
nr_cirurgia_w	       number(10,0);
dt_prevista_w          date;

begin
Obter_Param_Usuario(871, 563, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_atualiza_data_w);

if	(ie_opcao_p = 'C') then
	if	(nvl(nr_cirurgia_p,0) = 0) then
		select	max(nr_cirurgia)
		into	nr_cirurgia_w
		from	agenda_paciente
		where	nr_sequencia	=	nr_sequencia_p;	
		
		if	(nr_cirurgia_w > 0) then
			update	cirurgia
			set	nr_seq_agenda	=	null
			where	nr_cirurgia	=	nr_cirurgia_w;		
		end if;
		
		update	prescr_medica
		set	nr_seq_agenda 	= null
		where	nr_seq_agenda 	= nr_sequencia_p;
	end if;
	
	if (ie_atualiza_data_w = 'S') then
	
		if      (nvl(nr_sequencia_p,0) > 0) then
			select  max(hr_inicio)
			into	dt_prevista_w
			from	agenda_paciente
			where	nr_sequencia	=	nr_sequencia_p;
		end if;
		
		
		if      (nvl(nr_cirurgia_p,0) > 0) then
			update cirurgia
			set dt_inicio_prevista  =     dt_prevista_w
			where nr_cirurgia	   =	nr_cirurgia_p;
		end if;
	end if;
 
	update	agenda_paciente
	set	nr_cirurgia 	= decode(nvl(nr_cirurgia_p,0),0,null,nr_cirurgia_p)
	where	nr_sequencia 	= nr_sequencia_p;
	
	
	
elsif	(ie_opcao_p = 'P') then
	update	prescr_medica
	set	nr_seq_agenda 	= null
	where	nr_seq_agenda 	= nr_sequencia_p;
end if;	
	

commit;

end desvincula_cirurgia_agenda;
/
