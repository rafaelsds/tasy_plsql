/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Obter se o cart�o e a cooperativa � de regra de PCMSO, caso for retorna o emissor para gerar um novo cart�o
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_obter_regra_usuario_pcmso
			(	cd_usuario_plano_p		varchar2,
				nr_seq_congenere_p		number,
				nr_seq_emissor_p	out	number) is 
			
NR_SEQ_EMISSOR_w		number(10);
nr_seq_congenere_w		number(10);
cd_cooperativa_w		varchar2(10);

begin

if	(nr_seq_congenere_p is not null) then
	nr_seq_congenere_w	:= nr_seq_congenere_p;
else
	cd_cooperativa_w	:= substr(cd_usuario_plano_p,2,3);
	
	begin
		select	max(nr_sequencia)
		into	nr_seq_congenere_w
		from	pls_congenere
		where	to_number(nvl(cd_cooperativa,0)) = to_number(cd_cooperativa_w);
	exception
	when others then
		nr_seq_congenere_w := 0;
	end;
end if;

select	max(nr_seq_emissor)
into	nr_seq_emissor_w
from	pls_congenere_cartao_pcmso
where	nr_seq_congenere	= nr_seq_congenere_w
and	((cd_usuario_plano	= cd_usuario_plano_p) or (cd_usuario_plano is null))
and	rownum			<= 1;

nr_seq_emissor_p	:= nr_seq_emissor_w;

end pls_obter_regra_usuario_pcmso;
/