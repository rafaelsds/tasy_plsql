create or replace
procedure recebe_nota_rps_nfe_trat(
			cd_estabelecimento_p	Number,
			nr_nota_fiscal_p	Number,
			cd_serie_nf_p		varchar2,
			cd_cpf_cnpj_prest_p	varchar2,
			dt_emissao_p		varchar2,
			nr_nfe_imp_p		varchar2,
			ds_observacao_p		varchar2,
			nr_seq_lote_p		number,
			ie_situacao_p		varchar2 default '',
			cd_interface_p		number) is

nr_sequencia_w	Number(10);
cd_serie_nr_w	varchar2(5);
dt_emissao_w	varchar2(20);
cd_interface_w	interface.cd_interface%type;

begin

/*conversao de formato de data	Criada por RKORZ*/
begin

dt_emissao_w	:= dt_emissao_p;

end;

begin

select	nvl(max(replace(pls_elimina_zeros_esquerda(cd_serie_nf_p),' ','')),'X')
into	cd_serie_nr_w
from	dual;

select	nvl(cd_interface_p,0)
into	cd_interface_w
from	dual;

SELECT	NVL(MAX(nr_sequencia),0)
into 	nr_Sequencia_w
FROM	nota_fiscal
WHERE	cd_estabelecimento	= cd_estabelecimento_p
AND	nr_nota_fiscal		= TO_NUMBER(nr_nota_fiscal_p)
AND	(cd_serie_nf		= pls_elimina_zeros_esquerda(cd_serie_nf_p) OR pls_elimina_zeros_esquerda(cd_serie_nr_w) = 'X')
AND	cd_cgc_emitente		= cd_cpf_cnpj_prest_p
AND	to_date(to_char(dt_emissao, 'dd/mm/yyyy'), 'dd/mm/yyyy') <= to_date(dt_emissao_p,'dd/mm/yyyy')
AND	NVL(nr_nfe_imp,0)	<> pls_elimina_zeros_esquerda(nr_nfe_imp_p);

if	(nr_sequencia_w > 0) then

	if	(ie_situacao_p = 'C') then
		begin
		update	nota_fiscal
		set	nr_nfe_imp 		= pls_elimina_zeros_esquerda(nr_nfe_imp_p),
			nr_seq_importada	= nr_seq_lote_p,
			ie_status_envio		= ie_situacao_p
		where	nr_sequencia 		= nr_sequencia_w;
		end;
	else
		begin
		if	(cd_interface_w in(2252,2176)) then
			begin
			update	nota_fiscal
			set	nr_nfe_imp 		= pls_elimina_zeros_esquerda(nr_nfe_imp_p),
				nr_seq_importada	= nr_seq_lote_p,
				ie_status_envio		= 'E'
			where	nr_sequencia 		= nr_sequencia_w;
			end;
		else
			begin
			update	nota_fiscal
			set	nr_nfe_imp 		= pls_elimina_zeros_esquerda(nr_nfe_imp_p),
				nr_seq_importada	= nr_seq_lote_p
			where	nr_sequencia 		= nr_sequencia_w;
			end;
		end if;
		end;
	end if;
	
	update	nota_fiscal_item
	set	nr_nfe_imp 	= pls_elimina_zeros_esquerda(nr_nfe_imp_p)
	where	nr_sequencia 	= nr_sequencia_w;
	
	insert into nota_fiscal_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_nota,
		cd_evento,
		ds_historico)
	values(	nota_fiscal_hist_seq.nextval,
		sysdate,
		'Tasy',
		sysdate,
		'Tasy',
		nr_sequencia_w,
		'27',
		--substr('NF-e importada: ' || pls_elimina_zeros_esquerda(nr_nfe_imp_p) || ' ' || ds_observacao_p,1,255));
		substr(wheb_mensagem_pck.get_texto(311869, 'NR_NFE_IMP=' || pls_elimina_zeros_esquerda(nr_nfe_imp_p)) || ' ' || ds_observacao_p,1,255));
end if;


exception when others then
	wheb_mensagem_pck.exibir_mensagem_abort(266358,'NR_NOTA_FISCAL=' || to_number(nr_nota_fiscal_p) || ';' ||
							'CD_ESTABELECIMENTO=' || cd_estabelecimento_p || ';' ||
							'CD_SERIE_NF=' || pls_elimina_zeros_esquerda(cd_serie_nf_p) || ';' ||
							'DT_EMISSAO=' || dt_emissao_w || ';' ||
							'CD_CPF_CNPJ=' || cd_cpf_cnpj_prest_p || ';' ||
							'NR_NFE_IMP=' || pls_elimina_zeros_esquerda(nr_nfe_imp_p));
	--'Erro na atualiza��o da nf: ' 	|| to_number(nr_nota_fiscal_p)		|| chr(13) || chr(10) ||
	--'Estab: '		|| cd_estabelecimento_p 	|| chr(13) || chr(10) ||
	--'S�rie: ' 		|| pls_elimina_zeros_esquerda(cd_serie_nf_p)		|| chr(13) || chr(10) ||
	--'Emiss�o: ' 		|| dt_emissao_w		|| chr(13) || chr(10) ||
	--'CNPJ: '		|| cd_cpf_cnpj_prest_p 	|| chr(13) || chr(10) ||
	--'Nr NF-e: '		|| pls_elimina_zeros_esquerda(nr_nfe_imp_p));
end;

commit;

end recebe_nota_rps_nfe_trat;
/
