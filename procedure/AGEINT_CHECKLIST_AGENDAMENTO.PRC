create or replace
procedure Ageint_checklist_agendamento(nr_seq_ageint_p		number,
				       nm_usuario_p		Varchar2,
				       cd_estabelecimento_p	number,
				       ie_gerado_p		out varchar2) is 


cd_convenio_w		number(5,0);	
cd_categoria_w		varchar2(10);
cd_plano_w		varchar2(10);
ie_existe_regra_w	varchar2(1);
				       
nr_seq_ageint_check_list_w	number(10,0);

nr_seq_item_w		number(10,0);	
ie_gerado_w		varchar2(1) := 'N';
ds_grupo_chec_w	ageint_grupo_check_list.ds_grupo%type;


Cursor C01 is
	select	b.nr_sequencia
	from	ageint_grupo_check_list a,
		ageint_item_check_list b
	where	a.ie_situacao = 'A'
	and	b.ie_situacao = 'A'
	and	b.nr_seq_grupo_check_list = a.nr_sequencia
	and	a.nr_sequencia in (select	nr_seq_grupo_check_list
				   from		ageint_regra_checklist
				   where	((cd_convenio = cd_convenio_w) or (cd_convenio is null)) 
				   and		((ie_tipo_convenio = Obter_Tipo_Convenio(cd_convenio_w)) or (ie_tipo_convenio is null)) 
				   and		((cd_plano_convenio = cd_plano_w) or (cd_plano_convenio is null)) 
				   and		((cd_categoria = cd_categoria_w) or (cd_categoria is null)) 
				   and		(cd_procedimento is null) 
				   and		(nr_seq_proc_interno is null) 
				   and		(cd_area_procedimento is null) 
				   and		(cd_especialidade is null) 
				   and		(cd_grupo_proc is null)
				   and		nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
				   and		nvl(ie_apres_agendamento,'N') = 'S');
				       
				       
begin

if	(nr_seq_ageint_p is not null) then

	select	max(cd_convenio),
		max(cd_categoria),
		max(cd_plano)
	into	cd_convenio_w,
		cd_categoria_w,
		cd_plano_w
	from	agenda_integrada
	where	nr_sequencia = nr_seq_ageint_p;

	select	decode(count(*),0,'N','S')
	into	ie_existe_regra_w
	from	ageint_regra_checklist
	where	((cd_convenio = cd_convenio_w) or (cd_convenio is null)) 
	and		((ie_tipo_convenio = Obter_Tipo_Convenio(cd_convenio_w)) or (ie_tipo_convenio is null)) 
	and	((cd_plano_convenio = cd_plano_w) or (cd_plano_convenio is null)) 
	and	((cd_categoria = cd_categoria_w) or (cd_categoria is null)) 
	and	(cd_procedimento is null) 
	and	(nr_seq_proc_interno is null) 
	and	(cd_area_procedimento is null) 
	and	(cd_especialidade is null) 
	and	(cd_grupo_proc is null)
	and	nvl(cd_estabelecimento, cd_estabelecimento_p)	= cd_estabelecimento_p
	and	nvl(ie_apres_agendamento,'N') = 'S'; 

	if	(ie_existe_regra_w = 'S') then
		
		select	ageint_check_list_paciente_seq.nextval
		into	nr_seq_ageint_check_list_w
		from	dual;
				
		insert into ageint_check_list_paciente (nr_sequencia,
							nr_seq_ageint,
							dt_atualizacao,
							nm_usuario,
							ie_tipo_check_list)
						values (nr_seq_ageint_check_list_w,
							nr_seq_ageint_p,
							sysdate,	
							nm_usuario_p,
							'A');
							
		ie_gerado_w	:= 'S';

		commit;
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_item_w;
		exit when C01%notfound;
			begin
			select	substr(max(a.ds_grupo),1,255)
			into	ds_grupo_chec_w
			from	ageint_grupo_check_list a,
					ageint_item_check_list b
			where	b.nr_seq_grupo_check_list = a.nr_sequencia
			and		b.nr_sequencia = nr_seq_item_w;						
			
			insert into ageint_check_list_pac_item(	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								nr_seq_check_list,						
								nr_seq_item_check_list,
								ds_resultado)
							values (ageint_check_list_pac_item_seq.nextval,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								nr_seq_ageint_check_list_w,
								nr_seq_item_w,
								'N');
			
			end;
		end loop;
		close C01;

	end if;	
	
	update	ageint_check_list_paciente
	set		ds_grupo = ds_grupo_chec_w
	where	nr_sequencia = nr_seq_ageint_check_list_w;
	
	commit;

end if;	

ie_gerado_p	:= ie_gerado_w;
	
end Ageint_checklist_agendamento;
/