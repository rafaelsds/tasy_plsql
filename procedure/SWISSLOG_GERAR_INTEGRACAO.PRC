create or replace
procedure swisslog_gerar_integracao(	nr_seq_evento_p		number,
					ds_parametros_p		varchar2) is

jobno number;
nm_usuario_w		varchar2(30);
ds_comando_w		varchar2(2000);

begin

nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

if(nm_usuario_w is null)then
	nm_usuario_w := 'Tasy';
end if;

ds_comando_w := ' swisslog_job_gerar_integracao('|| to_char(nr_seq_evento_p) || ', ''' || ds_parametros_p || ''','''|| nm_usuario_w ||''');';

dbms_job.submit(jobno, ds_comando_w);

end swisslog_gerar_integracao;
/