create or replace
procedure delete_medida_laudo_peca(nr_seq_laudo_medida_peca_p	number) is 

begin

delete 
from 	laudo_paciente_medida
where	nr_seq_peca_medida = nr_seq_laudo_medida_peca_p;

commit;

end delete_medida_laudo_peca;
/