create or replace
procedure Ajustar_itens_auditoria
		(nr_sequencia_p			number,		 
		 qt_ajuste_p	 		number,
		 nr_seq_motivo_p		number,
		 ds_justificativa_p		varchar2,
		 ie_tipo_p			varchar,
		 nm_usuario_p			varchar2) is

begin

if	(ie_tipo_p = 'M') then
	update	auditoria_matpaci
	set	nr_seq_motivo = decode(nr_seq_motivo_p,0,NULL,nr_seq_motivo_p),
		qt_ajuste = qt_ajuste_p,
		ds_justificativa = decode(ds_justificativa_p, null, ds_justificativa, substr(ds_justificativa || ' ' || ds_justificativa_p,1,255)),
		nm_usuario = nm_usuario_p,
		dt_atualizacao  = sysdate
	where 	nr_sequencia = nr_sequencia_p;	
elsif	(ie_tipo_p = 'P') then		
	update	auditoria_propaci
	set	nr_seq_motivo = decode(nr_seq_motivo_p,0,NULL,nr_seq_motivo_p),
		qt_ajuste = qt_ajuste_p,
		ds_justificativa = decode(ds_justificativa_p, null, ds_justificativa, substr(ds_justificativa || ' ' || ds_justificativa_p,1,255)),
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where 	nr_sequencia = nr_sequencia_p;	
end if;

commit;

end Ajustar_itens_auditoria;
/
