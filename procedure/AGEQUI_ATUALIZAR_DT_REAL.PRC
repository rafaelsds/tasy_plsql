create or replace
procedure agequi_atualizar_dt_real	(	nr_seq_pend_agenda_p	number,
						dt_agenda_real_p	date,
						nm_usuario_p		Varchar2) is 

dt_menor_agenda_w	date;
nr_sequencia_w		Number(10);
qt_dif_entre_data_w	Number(10);
Cursor C01 is
	select	b.nr_sequencia
	from	agenda_quimio a,
		w_gerar_consulta_quimio b
	where	a.nr_seq_pend_agenda		= b.nr_seq_pend_agenda
	and	b.nr_seq_pend_agenda 		= nr_seq_pend_agenda_p
	and	nvl(b.ie_gerar_consulta,'N')	= 'S'
	order by 1;
	
begin

if (nvl(nr_seq_pend_agenda_p,0) > 0) and 
   (dt_agenda_real_p is not null) then

	select	min(nvl(b.dt_agenda_real, b.dt_agenda))
	into	dt_menor_agenda_w
	from	agenda_quimio a,
		w_gerar_consulta_quimio b
	where	a.nr_seq_pend_agenda		= b.nr_seq_pend_agenda
	and	b.nr_seq_pend_agenda 		= nr_seq_pend_agenda_p
	and	nvl(b.ie_gerar_consulta,'N')	= 'S';
	
	
	if (dt_menor_agenda_w is not null) then
	
	qt_dif_entre_data_w := OBTER_DIAS_ENTRE_DATAS(dt_menor_agenda_w,dt_agenda_real_p);
	
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w;
	exit when C01%notfound;
		begin
		
		update	w_gerar_consulta_quimio
		set	dt_agenda_real = (dt_agenda + qt_dif_entre_data_w)
		where	nr_sequencia   = nr_sequencia_w;
		
		end;
	end loop;
	close C01;
	
	end if;
end if;

commit;

end agequi_atualizar_dt_real;
/