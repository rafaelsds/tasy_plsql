create or replace PROCEDURE Gerar_diagnostico_atend(	nr_atendimento_p			number,
									cd_doenca_p					varchar2,
									cd_medico_p					varchar2,
									nm_usuario_p				varchar2,
									ie_tipo_diagnostico_p		number default 2,
									ie_classificacao_doenca_p	varchar2 default 'S',
									dt_liberacao_p				date default null,
                                    ie_tipo_doenca_p            varchar2 default null,
                                    ie_lado_p                   varchar2 default null,
                                    ds_diagnostico_p            varchar2 default null) is

nr_seq_interno_w	diagnostico_doenca.nr_seq_interno%type;

begin
Gerar_diagnostico_atend_Ret(nr_atendimento_p,
							cd_doenca_p,
                            cd_medico_p,
                            nm_usuario_p,
                            ie_tipo_diagnostico_p,
                            ie_classificacao_doenca_p,
                            dt_liberacao_p,
                            ie_tipo_doenca_p,
                            ie_lado_p,
                            ds_diagnostico_p,
							nr_seq_interno_w);
end Gerar_diagnostico_atend;
/
