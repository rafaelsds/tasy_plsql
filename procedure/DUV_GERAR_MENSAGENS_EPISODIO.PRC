create or replace procedure DUV_GERAR_MENSAGENS_EPISODIO
				(nr_seq_episodio_p		episodio_paciente.nr_sequencia%type,
				nm_usuario_p			usuario.nm_usuario%type,
				ie_acao_p			varchar2 default 'FC') is

/*ie_acao_p
FC - Fechar conta paciente
NC - Adicionar novo case
*/

nr_seq_mensagem_w			duv_mensagem.nr_sequencia%type;

nr_seq_versao_mens_w		duv_versao_mensagem.nr_sequencia%type;

nr_seq_regra_versao_w		duv_regra_versao.nr_sequencia%type;

nr_seq_versao_w				duv_versao.nr_sequencia%type;

nr_seq_nodo_w				duv_mensagem_segmento.nr_seq_nodo%type;

nr_seq_mens_seg_w			duv_mensagem_segmento.nr_sequencia%type;

cont_w						number(10,0);



cursor 	c01 is

select	*

from	atendimento_paciente

where	nr_seq_episodio		= nr_seq_episodio_p;



cursor 	c02 (nr_atendimento_cp 	atendimento_paciente.nr_atendimento%type)is

select	*

from	atend_categoria_convenio

where	nr_atendimento		= nr_atendimento_cp

order 	by dt_inicio_vigencia;



cursor c03 (cd_convenio_cp			duv_regra_mensagem.cd_convenio%type,

			nr_seq_tipo_acidente_cp	duv_regra_mensagem.nr_seq_tipo_acidente%type) is

select	*

from	duv_regra_mensagem

where	cd_convenio		= cd_convenio_cp

and		nvl(nr_seq_tipo_acidente, 0)	= nvl(nr_seq_tipo_acidente_cp, 0)

order	by nvl(nr_seq_tipo_acidente, 0);



-- identificar os tipos de mensagens a serem geradas

cursor 	c04 (nr_seq_regra_versao_cp	duv_regra_mens_item.nr_seq_regra_versao%type) is

select	a.nr_seq_tipo_mensagem

from	duv_regra_mens_item a

where	a.nr_seq_regra_versao			= nr_seq_regra_versao_cp;



-- identificar os segmentos das 

cursor 	c05 (nr_seq_versao_mens_cp	duv_tipo_mens_segmento.nr_seq_versao_mens%type) is

select	a.*

from	duv_tipo_mens_segmento a

where	a.nr_seq_versao_mens			= nr_seq_versao_mens_cp;



begin



for	r_c01_w in c01 loop



	for	r_c02_w in c02(r_c01_w.nr_atendimento) loop

	

		for	r_c03_w in c03(r_c02_w.cd_convenio, r_c01_w.nr_seq_tipo_acidente) loop

		

			select	max(nr_sequencia),

					max(nr_seq_versao)

			into	nr_seq_regra_versao_w,

					nr_seq_versao_w

			from	duv_regra_versao

			where	nr_seq_regra_mens		= r_c03_w.nr_sequencia;

		

		end loop;

	

	end loop;



end loop;

/*
delete	from duv_mensagem_segmento
where	nr_seq_mensagem	in
		(select	nr_sequencia
		 from	duv_mensagem
		 where	nr_seq_episodio	 = nr_seq_episodio_p);*/

delete	from	duv_mensagem
where	nr_seq_episodio	 = nr_seq_episodio_p;		 

-- cursor dos tipos de mensagens

for	r_c04_w in c04(nr_seq_regra_versao_w) loop

	select	max(nr_sequencia)
	into	nr_seq_mensagem_w
	from	duv_mensagem
	where	nr_seq_episodio		= nr_seq_episodio_p
	and	nr_seq_tipo_mensagem	= r_c04_w.nr_seq_tipo_mensagem;	

	if	(nr_seq_mensagem_w is null) then
		duv_gerar_mensagem(nr_seq_episodio_p, r_c04_w.nr_seq_tipo_mensagem, nr_seq_versao_w, nr_seq_mensagem_w, nm_usuario_p);
	end if;	

	-- obter a versao da mensaagem

	select	max(nr_sequencia)

	into	nr_seq_versao_mens_w

	from	duv_versao_mensagem

	where	nr_seq_tipo_mensagem		= r_c04_w.nr_seq_tipo_mensagem

	and	nr_seq_versao				= nr_seq_versao_w;

	

	-- cursor dos segmentos

	for	r_c05_w in c05(nr_seq_versao_mens_w) loop



		select	count(*)

		into	cont_w

		from	duv_mensagem_segmento

		where	nr_seq_mensagem		= nr_seq_mensagem_w

		and		ie_tipo_segmento	= r_c05_w.ie_tipo_segmento;



		if	(cont_w = 0) then

			insert	into duv_mensagem_segmento

				(nr_sequencia,

				dt_atualizacao,

				nm_usuario,

				dt_atualizacao_nrec,

				nm_usuario_nrec,

				nr_seq_mensagem,

				ie_tipo_segmento,

				nr_seq_apres)

			values

				(duv_mensagem_segmento_seq.nextval,

				sysdate,

				nm_usuario_p,

				sysdate,

				nm_usuario_p,

				nr_seq_mensagem_w,

				r_c05_w.ie_tipo_segmento,

				r_c05_w.nr_seq_apres) returning nr_sequencia into nr_seq_mens_seg_w;

		end if;



		if	(r_c05_w.ie_tipo_segmento	= 'aba') then					

			duv_gerar_segmento_aba(nr_seq_mensagem_w, nm_usuario_p, nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042366;

		elsif	(r_c05_w.ie_tipo_segmento	= 'abs') then 

			duv_gerar_segmento_abs(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042266;

		elsif	(r_c05_w.ie_tipo_segmento	= 'adb') then 
    
      duv_gerar_segmento_adb(nr_seq_mensagem_w,nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042271;

		elsif	(r_c05_w.ie_tipo_segmento	= 'afb') then 

			duv_gerar_segmento_afb(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042281;

		elsif	(r_c05_w.ie_tipo_segmento	= 'anl') then 

      duv_gerar_segmento_anl(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
			
			nr_seq_nodo_w	:= 1042286;

		elsif	(r_c05_w.ie_tipo_segmento	= 'ass') then 

			duv_gerar_segmento_ass(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042276;

		elsif	(r_c05_w.ie_tipo_segmento	= 'atm') then 

			duv_gerar_segmento_atm(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
    
			nr_seq_nodo_w	:= 1042159;

		elsif	(r_c05_w.ie_tipo_segmento	= 'auf') then 

			duv_gerar_segmento_auf(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042416;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bbv') then 

      duv_gerar_segmento_bbv(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
			
			nr_seq_nodo_w	:= 1042411;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bed') then 

			duv_gerar_segmento_bed(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042441;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bef') then 

      duv_gerar_segmento_bef(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
			
			nr_seq_nodo_w	:= 1042431;

		elsif	(r_c05_w.ie_tipo_segmento	= 'beh') then 

			duv_gerar_segmento_beh(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042426;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bek') then 
      
      duv_gerar_segmento_bek(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
		
			nr_seq_nodo_w	:= 1042396;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bem') then 

			duv_gerar_segmento_bem(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042421;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bes') then 

			duv_gerar_segmento_bes(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042467;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bfb') then 

			duv_gerar_segmento_bfb(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042447;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bhi') then 

			duv_gerar_segmento_bhi(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042452;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bnd') then 

			duv_gerar_segmento_bnd(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042406;

		elsif	(r_c05_w.ie_tipo_segmento	= 'bns') then 

      duv_gerar_segmento_bns(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
			
			nr_seq_nodo_w	:= 1042462;

		elsif	(r_c05_w.ie_tipo_segmento	= 'dis') then 

			duv_gerar_segmento_dis(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042436;

		elsif	(r_c05_w.ie_tipo_segmento	= 'ebh') then 
      
      duv_gerar_segmento_ebh(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
		
			nr_seq_nodo_w	:= 1042457;

		elsif	(r_c05_w.ie_tipo_segmento	= 'eti') then 

			duv_gerar_segmento_eti(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042477;

		elsif	(r_c05_w.ie_tipo_segmento	= 'gck') then 

      duv_gerar_segmento_gck(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
			
			nr_seq_nodo_w	:= 1042472;

		elsif	(r_c05_w.ie_tipo_segmento	= 'gew') then 

			duv_gerar_segmento_gew(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042391;

		elsif	(r_c05_w.ie_tipo_segmento	= 'kdi') then 

			duv_gerar_segmento_kdi(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
    
			nr_seq_nodo_w	:= 1042494;

		elsif	(r_c05_w.ie_tipo_segmento	= 'kon') then 

			duv_gerar_segmento_kon(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042386;

		elsif	(r_c05_w.ie_tipo_segmento	= 'ksd') then 

			duv_gerar_segmento_ksd(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
   
			nr_seq_nodo_w	:= 1042489;

		elsif	(r_c05_w.ie_tipo_segmento	= 'kto') then 

			duv_gerar_segmento_kto(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042401;

		elsif	(r_c05_w.ie_tipo_segmento	= 'nah') then 

	    duv_gerar_segmento_nah(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);
  		
			nr_seq_nodo_w	:= 1042509;

		elsif	(r_c05_w.ie_tipo_segmento	= 'nbh') then 

			duv_gerar_segmento_nbh(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042499;

		elsif	(r_c05_w.ie_tipo_segmento	= 'not') then 

			duv_gerar_segmento_not(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042504;

		elsif	(r_c05_w.ie_tipo_segmento	= 'rel') then 

			duv_gerar_segmento_rel(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042483;

		elsif	(r_c05_w.ie_tipo_segmento	= 'rma') then 

			duv_gerar_segmento_rma(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042529;

		elsif	(r_c05_w.ie_tipo_segmento	= 'son') then 

			duv_gerar_segmento_son(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042524;

		elsif	(r_c05_w.ie_tipo_segmento	= 'spb') then 

			duv_gerar_segmento_spb(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042376;

		elsif	(r_c05_w.ie_tipo_segmento	= 'sri') then 

			duv_gerar_segmento_sri(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042514;

		elsif	(r_c05_w.ie_tipo_segmento	= 'svb') then 

			duv_gerar_segmento_svb(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042519;

		elsif	(r_c05_w.ie_tipo_segmento	= 'swh') then 

			duv_gerar_segmento_swh(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042539;

		elsif	(r_c05_w.ie_tipo_segmento	= 'swi') then 

			duv_gerar_segmento_swi(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042381;

		elsif	(r_c05_w.ie_tipo_segmento	= 'tab') then 

			duv_gerar_segmento_tab(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042534;

		elsif	(r_c05_w.ie_tipo_segmento	= 'tdh') then 

			duv_gerar_segmento_tdh(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042554;

		elsif	(r_c05_w.ie_tipo_segmento	= 'tkn') then 

			duv_gerar_segmento_tkn(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042544;

		elsif	(r_c05_w.ie_tipo_segmento	= 'tko') then 

			duv_gerar_segmento_tko(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042371;

		elsif	(r_c05_w.ie_tipo_segmento	= 'tre') then 

			duv_gerar_segmento_tre(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042549;

		elsif	(r_c05_w.ie_tipo_segmento	= 'tsu') then 

			duv_gerar_segmento_tsu(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042291;

		elsif	(r_c05_w.ie_tipo_segmento	= 'tve') then 

			duv_gerar_segmento_tve(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042316;

		elsif	(r_c05_w.ie_tipo_segmento	= 'ufb') then 

			duv_gerar_segmento_ufb(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042311;

		elsif	(r_c05_w.ie_tipo_segmento	= 'ufd') then 

			duv_gerar_segmento_ufd(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042361;

		elsif	(r_c05_w.ie_tipo_segmento	= 'unb') then 

			duv_gerar_segmento_unb(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042321;

		elsif	(r_c05_w.ie_tipo_segmento	= 'unh') then 

			duv_gerar_segmento_unh(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042326;

		elsif	(r_c05_w.ie_tipo_segmento	= 'uuk') then 

			duv_gerar_segmento_uuk(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042331;

		elsif	(r_c05_w.ie_tipo_segmento	= 'uvt') then 

			duv_gerar_segmento_uvt(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042301;

		elsif	(r_c05_w.ie_tipo_segmento	= 'vav') then 

			duv_gerar_segmento_vav(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042341;

		elsif	(r_c05_w.ie_tipo_segmento	= 'ver') then 

			duv_gerar_segmento_ver(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042351;

		elsif	(r_c05_w.ie_tipo_segmento	= 'vin') then 

			duv_gerar_segmento_vin(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042336;

		elsif	(r_c05_w.ie_tipo_segmento	= 'vne') then 

			duv_gerar_segmento_vne(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042306;

		elsif	(r_c05_w.ie_tipo_segmento	= 'wbe') then 

			duv_gerar_segmento_wbe(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042296;

		elsif	(r_c05_w.ie_tipo_segmento	= 'wme') then 

			duv_gerar_segmento_wme(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042346;

		elsif	(r_c05_w.ie_tipo_segmento	= 'wub') then 

			duv_gerar_segmento_wub(nr_seq_mensagem_w, nm_usuario_p,nr_seq_episodio_p);

			nr_seq_nodo_w	:= 1042356;

		end if;

		

		update	duv_mensagem_segmento
		set	nr_seq_nodo	= nr_seq_nodo_w
		where	nr_sequencia	= nr_seq_mens_seg_w;
		
				

	end loop;
	
	DUV_CONSISTE_MENSAGEM(nr_seq_mensagem_w,null,null,nm_usuario_p);

end loop;

/*'Efetuado essa chamada para libera��o das mensagens de envio da troca do tipo do case, que s�o conformadas apenas no fim do breadcrumb'*/
liberar_envio_troca_tipo_case(nr_seq_episodio_p);

commit;

end DUV_GERAR_MENSAGENS_EPISODIO;
/