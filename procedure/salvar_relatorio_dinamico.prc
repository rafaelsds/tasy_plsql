create or replace procedure salvar_relatorio_dinamico(
	nr_sequencia_p number,
	eixo_x_p varchar2,
	eixo_y_p varchar2) is

begin
	update relatorio_dinamico
	set nr_eixo_x = eixo_x_p,
	nr_eixo_y = eixo_y_p
	where nr_sequencia = nr_sequencia_p;

end salvar_relatorio_dinamico;
/
