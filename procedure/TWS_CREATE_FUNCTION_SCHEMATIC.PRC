create or replace
procedure tws_create_function_schematic(cd_funcao_p		funcao.cd_funcao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Criar a fun��o no schematic pela fun��o Gerenciador do schematic
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ x] Outros: Tasy HTML5
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_exp_schematics_w		dic_expressao.cd_expressao%type;
ds_expressao_w			dic_expressao.ds_expressao_br%type;
nr_seq_funcao_schematic_w	funcao_schematic.nr_sequencia%type;
nr_seq_obj_schematic_w          objeto_schematic.nr_seq_obj_sup%type;
qt_registro_w			number(5);
		
begin


if	( cd_funcao_p is not null ) then

	select 	count(1)
	into	qt_registro_w
	from	funcao_schematic
	where	cd_funcao = cd_funcao_p;
	

	if	( qt_registro_w = 0 ) then
		
		select	cd_exp_funcao,
			obter_desc_expressao( cd_exp_funcao, null) ds_expressao
		into	cd_exp_schematics_w,
			ds_expressao_w
		from	funcao
		where	cd_funcao = cd_funcao_p;


		insert into funcao_schematic( 	
			nr_sequencia, dt_atualizacao, nm_usuario,
			dt_atualizacao_nrec, nm_usuario_nrec, cd_funcao,
			cd_exp_schematic, ie_situacao_funcao, ds_schematic)
		values(	funcao_schematic_seq.nextval, sysdate, nm_usuario_p,
			sysdate, nm_usuario_p, cd_funcao_p, 
			cd_exp_schematics_w, 'A', ds_expressao_w) returning nr_sequencia into nr_seq_funcao_schematic_w;	
		commit;
		
	end if;
	
end if;

end tws_create_function_schematic;
/