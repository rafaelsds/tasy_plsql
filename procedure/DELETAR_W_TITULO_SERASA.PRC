create or replace
procedure deletar_w_titulo_serasa(nr_titulo_p	number,
				nm_usuario_p	varchar2) is

begin

delete	from w_titulo_serasa
where	nr_titulo		= nr_titulo_p
and	nm_usuario		= nm_usuario_p;

commit;

end deletar_w_titulo_serasa;
/