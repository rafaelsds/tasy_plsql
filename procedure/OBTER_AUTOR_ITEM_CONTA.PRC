create or replace
procedure OBTER_AUTOR_ITEM_CONTA(nr_seq_propaci_p	in	number,
				nr_seq_matpaci_p	in	number,
				nr_sequencia_autor_p	out	number,
				nr_seq_proc_autor_p	out	number,
				nr_seq_mat_autor_p	out	number,
				cd_autorizacao_p	out	varchar2,
				cd_senha_p		out	varchar2,
				ie_autorizado_p		out	varchar2,
				ie_doc_autorizacao_p	in 	varchar2 default null,
				ie_autor_qtde_p 	in 	varchar2 default 'N') is

nr_sequencia_autor_w		number(10)	:= null;
cd_autorizacao_w		varchar2(20)	:= null;
cd_senha_w			varchar2(20)	:= null;
nr_seq_estagio_w		number(10)	:= null;
ie_autorizado_w			varchar2(1)	:= null;
nr_seq_proc_autor_w		number(10)	:= null;
nr_seq_mat_autor_w		number(10)	:= null;
nr_cirurgia_w			number(10)	:= null;
nr_interno_conta_w		number(10,0)	:= null;
cd_mat_conta_w			number(10,0)	:= null;
nr_prescricao_w			number(10)	:= null;
nr_sequencia_prescricao_w	number(10)	:= null;
cd_material_kit_w		number(10,0)	:= null;
ie_doc_autorizacao_w		varchar2(10);


Cursor C01 is
select  x.ie_prioridade, x.nr_seq_proced, x.nr_sequencia_autor, x.nr_seq_proc_conta, x.cd_senha, x.cd_autorizacao, x.cd_senha_item, x.cd_autorizacao_item,x.nr_prescricao
	from	(select	d.nr_sequencia nr_seq_proced,
			c.nr_sequencia nr_sequencia_autor,
			1 ie_prioridade, /*Para os casos onde a prescr e seq sao iguais na conta e autorizacao*/
			b.nr_sequencia nr_seq_proc_conta,
			nvl(c.cd_senha,	'0') cd_senha,
			nvl(c.cd_autorizacao, '0') cd_autorizacao,
			b.cd_senha cd_senha_item, 
			b.nr_doc_convenio cd_autorizacao_item,
			b.nr_prescricao,
			c.nr_seq_autorizacao
		from	procedimento_autorizado d,
			procedimento_paciente b,
			autorizacao_convenio c,
			estagio_autorizacao e
		where	b.cd_procedimento		= d.cd_procedimento
		and	b.ie_origem_proced		= d.ie_origem_proced
		and	nvl(d.nr_seq_proc_interno,0)	= nvl(b.nr_seq_proc_interno,0)
		and	b.nr_prescricao			= d.nr_prescricao
		and	b.nr_sequencia_prescricao 	= d.nr_seq_prescricao
		and	c.nr_sequencia			= d.nr_sequencia_autor
		and	e.nr_sequencia			= c.nr_seq_estagio
		and	b.nr_sequencia			= nr_seq_propaci_p
		and	b.dt_procedimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),b.dt_procedimento) and Nvl(c.dt_fim_vigencia,b.dt_procedimento)
		and	e.ie_interno not in  ('70','90')
		and 	((ie_autor_qtde_p = 'N') or (nvl(d.qt_autorizada,0) > 0))
		union
		select	d.nr_sequencia nr_seq_proced,
			c.nr_sequencia nr_sequencia_autor,
			2 ie_prioridade, /*Para os casos onde a prescr ao iguais na conta e autorizacao, porem sem seq prescricao em ambas*/
			b.nr_sequencia nr_seq_proc_conta,
			nvl(c.cd_senha,	'0') cd_senha,
			nvl(c.cd_autorizacao, '0') cd_autorizacao,
			b.cd_senha cd_senha_item, 
			b.nr_doc_convenio cd_autorizacao_item,
			nvl(b.nr_prescricao,0) nr_prescricao,
			c.nr_seq_autorizacao
		from	procedimento_autorizado d, /* Ocorre quando na conta e gerado por RLA e autorizacao quando por itens adic da regra plano*/
			procedimento_paciente b,
			autorizacao_convenio c,
			estagio_autorizacao e
		where	b.cd_procedimento		= d.cd_procedimento
		and	b.ie_origem_proced		= d.ie_origem_proced
		and	nvl(d.nr_seq_proc_interno,0)	= nvl(b.nr_seq_proc_interno,0)
		and	b.nr_prescricao			= d.nr_prescricao
		and	b.nr_sequencia_prescricao	is null
		and 	d.nr_seq_prescricao		is null
		and	c.nr_sequencia			= d.nr_sequencia_autor
		and	e.nr_sequencia			= c.nr_seq_estagio
		and	b.nr_sequencia			= nr_seq_propaci_p
		and	b.dt_procedimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),b.dt_procedimento) and Nvl(c.dt_fim_vigencia,b.dt_procedimento)
		and	e.ie_interno not in  ('70','90')
		and	c.nr_sequencia 	= (	select	max(w.nr_sequencia)
						from	autorizacao_convenio w
						where	w.nr_prescricao = c.nr_prescricao
						and	w.nr_atendimento = c.nr_atendimento
						and	w.cd_senha is not null and w.cd_autorizacao is not null)
		and 	((ie_autor_qtde_p = 'N') or (nvl(d.qt_autorizada,0) > 0))
		union
		select	b.nr_sequencia nr_seq_proced,
			c.nr_sequencia nr_sequencia_autor,
			3 ie_prioridade, /*Para os casos onde a comparacao e apenas pelo codigo do proced*/
			d.nr_sequencia nr_seq_proc_conta,
			nvl(c.cd_senha,	'0') cd_senha,
			nvl(c.cd_autorizacao, '0') cd_autorizacao,
			d.cd_senha cd_senha_item, 
			d.nr_doc_convenio cd_autorizacao_item,
			nvl(d.nr_prescricao,0) nr_prescricao,
			c.nr_seq_autorizacao
		from	autorizacao_convenio c,
			procedimento_autorizado b,
			procedimento_paciente d,
			estagio_autorizacao e
		where	b.nr_sequencia_autor	= c.nr_sequencia
		and	d.nr_atendimento	= c.nr_atendimento
		and	d.cd_procedimento	= b.cd_procedimento
		and	d.ie_origem_proced   	= b.ie_origem_proced
		and	nvl(d.nr_seq_proc_interno,0)	= nvl(b.nr_seq_proc_interno,0)
		and	e.nr_sequencia		= c.nr_seq_estagio
		and	e.ie_interno not in ('70','90')
		and	d.nr_sequencia			= nr_seq_propaci_p
		and	d.dt_procedimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),d.dt_procedimento) and Nvl(c.dt_fim_vigencia,d.dt_procedimento)
		and	b.nr_prescricao is null
		and	d.nr_prescricao is not null
		and	not exists(select	1
				   from		procedimento_paciente x
				   where	x.nr_sequencia <> nr_seq_propaci_p
				   and		x.nr_prescricao 	= d.nr_prescricao
				   and		x.cd_procedimento	= d.cd_procedimento
				   and		x.ie_origem_proced	= d.ie_origem_proced
				   and		x.nr_atendimento	= c.nr_atendimento
				   and		nvl(x.nr_seq_proc_interno,0)	= nvl(b.nr_seq_proc_interno,0)
				   and		x.cd_senha		= c.cd_senha
				   and		x.nr_doc_convenio	= c.cd_autorizacao)
		and 	((ie_autor_qtde_p = 'N') or (nvl(b.qt_autorizada,0) > 0))
		union
		select	b.nr_sequencia nr_seq_proced,
			c.nr_sequencia nr_sequencia_autor,
			4 ie_prioridade, /*Para os casos onde a comparacao e apenas pelo codigo do proced*/
			d.nr_sequencia nr_seq_proc_conta,
			nvl(c.cd_senha,	'0') cd_senha,
			nvl(c.cd_autorizacao, '0') cd_autorizacao,
			d.cd_senha cd_senha_item, 
			d.nr_doc_convenio cd_autorizacao_item,
			nvl(d.nr_prescricao,0) nr_prescricao,
			c.nr_seq_autorizacao
		from	autorizacao_convenio c,
			procedimento_autorizado b,
			procedimento_paciente d,
			estagio_autorizacao e
		where	b.nr_sequencia_autor	= c.nr_sequencia
		and	d.nr_atendimento	= c.nr_atendimento
		and	d.cd_procedimento	= b.cd_procedimento
		and	d.ie_origem_proced   	= b.ie_origem_proced
		and	nvl(d.nr_seq_proc_interno,0)	= nvl(b.nr_seq_proc_interno,0)
		and	e.nr_sequencia		= c.nr_seq_estagio
		and	e.ie_interno not in ('70','90')
		and	d.nr_sequencia			= nr_seq_propaci_p
		and	d.dt_procedimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),d.dt_procedimento) and Nvl(c.dt_fim_vigencia,d.dt_procedimento)
		and	b.nr_prescricao is null
		and	d.nr_prescricao is null
		and	not exists(select	1
				   from		procedimento_paciente x
				   where	x.nr_sequencia <> nr_seq_propaci_p
				   and		x.nr_prescricao is null
				   and		x.cd_procedimento	= d.cd_procedimento
				   and		x.nr_atendimento	= c.nr_atendimento
				   and		x.ie_origem_proced	= d.ie_origem_proced
				   and		nvl(x.nr_seq_proc_interno,0)	= nvl(b.nr_seq_proc_interno,0)
				   and		x.cd_senha		= c.cd_senha
				   and		x.nr_doc_convenio	= c.cd_autorizacao)
		and 	((ie_autor_qtde_p = 'N') or (nvl(b.qt_autorizada,0) > 0))
		union
		select	b.nr_sequencia nr_seq_proced,
			c.nr_sequencia nr_sequencia_autor,
			5 ie_prioridade, /*Para os casos onde e o proprio procedimento na conta, na conta nao tem prescricao mas na autorizacao tem*/
			d.nr_sequencia nr_seq_proc_conta,
			nvl(c.cd_senha,	'0') cd_senha,
			nvl(c.cd_autorizacao, '0') cd_autorizacao,
			d.cd_senha cd_senha_item, 
			d.nr_doc_convenio cd_autorizacao_item,
			nvl(d.nr_prescricao,0) nr_prescricao,
			c.nr_seq_autorizacao
		from	autorizacao_convenio c,
			procedimento_autorizado b,
			procedimento_paciente d,
			estagio_autorizacao e
		where	b.nr_sequencia_autor	= c.nr_sequencia
		and	d.nr_atendimento	= c.nr_atendimento
		and	d.cd_procedimento	= b.cd_procedimento
		and	d.ie_origem_proced   	= b.ie_origem_proced
		and	nvl(d.nr_seq_proc_interno,0)	= nvl(b.nr_seq_proc_interno,0)
		and	e.nr_sequencia		= c.nr_seq_estagio
		and	e.ie_interno not in ('70','90')
		and	d.nr_sequencia			= nr_seq_propaci_p
		and	d.dt_procedimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),d.dt_procedimento) and Nvl(c.dt_fim_vigencia,d.dt_procedimento)
		and	d.nr_prescricao is null
		AND	EXISTS(	SELECT  x.nr_prescricao
				FROM 	procedimento_paciente x,
					atendimento_pacote y
				WHERE	x.nr_sequencia 		  = y.nr_seq_proc_origem
				AND	x.nr_seq_proc_pacote 	  = d.nr_seq_proc_pacote
				and	x.nr_atendimento	  = c.nr_atendimento
				AND	x.nr_prescricao		  = b.nr_prescricao
				AND	x.nr_sequencia_prescricao = b.nr_seq_prescricao)
		and 	((ie_autor_qtde_p = 'N') or (nvl(b.qt_autorizada,0) > 0))
		union
		select	b.nr_sequencia nr_seq_proced,
			c.nr_sequencia nr_sequencia_autor,
			6 ie_prioridade, /*Para os casos onde e o proprio procedimento na conta, na conta nao tem prescricao mas na autorizacao tem*/
			d.nr_sequencia nr_seq_proc_conta,
			nvl(c.cd_senha,	'0') cd_senha,
			nvl(c.cd_autorizacao, '0') cd_autorizacao,
			d.cd_senha cd_senha_item, 
			d.nr_doc_convenio cd_autorizacao_item,
			nvl(d.nr_prescricao,0) nr_prescricao,
			c.nr_seq_autorizacao
		from	autorizacao_convenio c,
			procedimento_autorizado b,
			procedimento_paciente d,
			estagio_autorizacao e
		where	b.nr_sequencia_autor	= c.nr_sequencia
		and	d.nr_atendimento	= c.nr_atendimento
		and	d.cd_procedimento	= b.cd_procedimento
		and	d.ie_origem_proced   	= b.ie_origem_proced
		and	nvl(d.nr_seq_proc_interno,0)	= nvl(b.nr_seq_proc_interno,0)
		and	e.nr_sequencia		= c.nr_seq_estagio
		and	e.ie_interno not in ('70','90')
		and	d.nr_sequencia			= nr_seq_propaci_p
		and	d.dt_procedimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),d.dt_procedimento) and Nvl(c.dt_fim_vigencia,d.dt_procedimento)
		and	d.nr_prescricao is null
		and	d.nr_seq_proc_pacote is null
		and 	((ie_autor_qtde_p = 'N') or (nvl(b.qt_autorizada,0) > 0))
		union
		select	b.nr_sequencia nr_seq_proced,
			c.nr_sequencia nr_sequencia_autor,
			7 ie_prioridade, /*Para os casos onde e o proprio procedimento na conta, na conta tem prescricao mas na autorizacao nao tem*/
			d.nr_sequencia nr_seq_proc_conta,
			nvl(c.cd_senha,	'0') cd_senha,
			nvl(c.cd_autorizacao, '0') cd_autorizacao,
			d.cd_senha cd_senha_item, 
			d.nr_doc_convenio cd_autorizacao_item,
			nvl(d.nr_prescricao,0) nr_prescricao,
			c.nr_seq_autorizacao
		from	autorizacao_convenio c,
			procedimento_autorizado b,
			procedimento_paciente d,
			estagio_autorizacao e
		where	b.nr_sequencia_autor	= c.nr_sequencia
		and	d.nr_atendimento	= c.nr_atendimento
		and	d.cd_procedimento	= b.cd_procedimento
		and	d.ie_origem_proced   	= b.ie_origem_proced
		and	nvl(d.nr_seq_proc_interno,0)	= nvl(b.nr_seq_proc_interno,0)
		and	e.nr_sequencia		= c.nr_seq_estagio
		and	e.ie_interno not in ('70','90')
		and	d.nr_sequencia			= nr_seq_propaci_p
		and	d.dt_procedimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),d.dt_procedimento) and Nvl(c.dt_fim_vigencia,d.dt_procedimento)
		and	b.nr_prescricao is null
		and 	((ie_autor_qtde_p = 'N') or (nvl(b.qt_autorizada,0) > 0))
		order by ie_prioridade desc, cd_senha, cd_autorizacao, nr_seq_autorizacao, nr_prescricao,nr_seq_proc_conta, nr_seq_proced)x
	where	1 = 1;
c01_w	c01%rowtype;
begin

if	(nr_seq_propaci_p is not null) then

	select	nvl(ie_doc_autorizacao_p,nvl(max(obter_valor_conv_estab(a.cd_convenio_parametro, a.cd_estabelecimento, 'IE_DOC_AUTORIZACAO')),'N'))
	into	ie_doc_autorizacao_w
	from	conta_paciente a,
		procedimento_paciente b
	where	a.nr_interno_conta	= b.nr_interno_conta
	and	b.nr_sequencia		= nr_seq_propaci_p;
	
	if	(nvl(ie_doc_autorizacao_w,'N') = 'S') then

		select	max(d.nr_sequencia_autor),
			max(d.nr_sequencia)
		into	nr_sequencia_autor_w,
			nr_seq_proc_autor_w
		from	procedimento_autorizado d,
			procedimento_paciente b,
			autorizacao_convenio c
		where	b.cd_procedimento		= d.cd_procedimento
		and	b.ie_origem_proced		= d.ie_origem_proced
		and	b.nr_prescricao			= d.nr_prescricao
		and	b.nr_sequencia_prescricao	= d.nr_seq_prescricao
		and	c.nr_sequencia			= d.nr_sequencia_autor
		and	b.nr_sequencia			= nr_seq_propaci_p
		and	b.dt_procedimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),b.dt_procedimento) and Nvl(c.dt_fim_vigencia,b.dt_procedimento)
		and 	((ie_autor_qtde_p = 'N') or (nvl(d.qt_autorizada,0) > 0));

		if	(nr_seq_proc_autor_w is null) then

			select	max(nr_cirurgia)
			into	nr_cirurgia_w
			from	procedimento_paciente
			where	nr_sequencia	= nr_seq_propaci_p;

			if	(nr_cirurgia_w is not null) then
				select	max(b.nr_sequencia_autor),
					max(b.nr_sequencia)
				into	nr_sequencia_autor_w,
					nr_seq_proc_autor_w
				from	agenda_paciente d,
					autorizacao_convenio c,
					procedimento_autorizado b,
					procedimento_paciente a
				where	a.cd_procedimento	= b.cd_procedimento
				and	a.ie_origem_proced	= b.ie_origem_proced
				and	b.nr_sequencia_autor	= c.nr_sequencia
				and	c.nr_seq_agenda		= d.nr_sequencia
				and	d.nr_cirurgia		= a.nr_cirurgia
				and	a.nr_sequencia		= nr_seq_propaci_p
				and	a.dt_procedimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),a.dt_procedimento) and Nvl(c.dt_fim_vigencia,a.dt_procedimento)
				and 	((ie_autor_qtde_p = 'N') or (nvl(b.qt_autorizada,0) > 0));
			end if;
			
			
		end if;
	else
		
		open C01;
		loop
		fetch C01 into	
			c01_w;
		exit when C01%notfound;
			begin
			nr_seq_proc_autor_w 	:= c01_w.nr_seq_proced;
			nr_sequencia_autor_w 	:= c01_w.nr_sequencia_autor;
			end;
		end loop;
		close C01;
			

	end if;

elsif	(nr_seq_matpaci_p is not null) then

	select	nvl(ie_doc_autorizacao_p,nvl(max(obter_valor_conv_estab(a.cd_convenio_parametro, a.cd_estabelecimento, 'IE_DOC_AUTORIZACAO')),'N')),
		max(b.nr_prescricao),
		max(b.nr_sequencia_prescricao)
	into	ie_doc_autorizacao_w,
		nr_prescricao_w,
		nr_sequencia_prescricao_w
	from	conta_paciente a,
		material_atend_paciente b
	where	a.nr_interno_conta	= b.nr_interno_conta
	and	b.nr_sequencia		= nr_seq_matpaci_p;

	begin
	select	nvl(obter_material_conta(c.cd_estabelecimento, b.cd_convenio,b.cd_categoria, b.cd_material, b.cd_material, b.cd_material, null,
							b.cd_setor_atendimento, sysdate, b.cd_local_estoque, b.nr_prescricao),0)
	into	cd_mat_conta_w
	from	material_atend_paciente b,
		conta_paciente c
	where	c.nr_interno_conta = b.nr_interno_conta
	and	b.nr_sequencia	   = nr_seq_matpaci_p;
	exception
	when others then
		cd_mat_conta_w := 0;
	end;

	if	(nvl(ie_doc_autorizacao_w,'N') = 'S') then

		select	max(d.nr_sequencia_autor),
			max(d.nr_sequencia)
		into	nr_sequencia_autor_w,
			nr_seq_mat_autor_w
		from	material_autorizado d,
			material_atend_paciente b,
			autorizacao_convenio c
		where	((b.cd_material			= d.cd_material) or
			(b.cd_material = cd_mat_conta_w))
		and	b.nr_prescricao			= d.nr_prescricao
		and	b.nr_sequencia_prescricao	= d.nr_seq_prescricao
		and	c.nr_sequencia			= d.nr_sequencia_autor
		and	b.nr_sequencia			= nr_seq_matpaci_p
		and	b.dt_atendimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),b.dt_atendimento) and Nvl(c.dt_fim_vigencia,b.dt_atendimento)
		and 	((ie_autor_qtde_p = 'N') or (nvl(d.qt_autorizada,0) > 0));
	else
	
		if	(nr_prescricao_w is not null) then
			
			begin
			select	a.cd_material
			into	cd_material_kit_w
			from	prescr_material a
			where	a.nr_prescricao		= nr_prescricao_w
			and	a.cd_kit_material 	is null
			and	a.nr_sequencia		= 
					(select	max(b.nr_seq_kit)
					from	prescr_material b
					where	b.nr_prescricao		= nr_prescricao_w
					and	b.cd_kit_material 	is not null
					and	b.nr_sequencia		= nr_sequencia_prescricao_w)
			and	rownum 			= 1;			
			exception
			when others then
				cd_material_kit_w	:= null;
			end;
			
		end if;

		select	max(x.nr_sequencia_autor),
			max(x.nr_sequencia)
		into	nr_sequencia_autor_w,
			nr_seq_mat_autor_w
		from	(select	max(d.nr_sequencia_autor) nr_sequencia_autor, /* Consistencia somente pelo codigo onde nao ha vinculo com a prescr*/
				max(d.nr_sequencia) nr_sequencia
			from	autorizacao_convenio c,
				material_autorizado d,
				material_atend_paciente b,
				estagio_autorizacao e
			where	d.nr_sequencia_autor	= c.nr_sequencia
			and	c.nr_atendimento	= b.nr_atendimento
			and	b.cd_material		= d.cd_material
			and	e.nr_sequencia		= c.nr_seq_estagio
			and	e.ie_interno 		not in ('70','90')
			and	b.nr_sequencia		= nr_seq_matpaci_p
			and	b.dt_atendimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),b.dt_atendimento) and Nvl(c.dt_fim_vigencia,b.dt_atendimento)
			and	d.nr_prescricao is null
			and 	((ie_autor_qtde_p = 'N') or (nvl(d.qt_autorizada,0) > 0))
			union
			select	max(d.nr_sequencia_autor) nr_sequencia_autor,  /* Consistencia somente pelo codigo material exec onde nao ha vinculo */
				max(d.nr_sequencia) nr_sequencia
			from	autorizacao_convenio c,
				material_autorizado d,
				material_atend_paciente b,
				estagio_autorizacao e
			where	c.nr_sequencia		= d.nr_sequencia_autor
			and	c.nr_atendimento	= b.nr_atendimento
			and	e.nr_sequencia		= c.nr_seq_estagio
			and	e.ie_interno 		not in ('70','90')
			and	d.cd_material		= b.cd_material_exec			
			and	b.nr_sequencia		= nr_seq_matpaci_p
			and	b.dt_atendimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),b.dt_atendimento) and Nvl(c.dt_fim_vigencia,b.dt_atendimento)
			and	d.nr_prescricao is null
			and 	((ie_autor_qtde_p = 'N') or (nvl(d.qt_autorizada,0) > 0))
			union
			select	max(d.nr_sequencia_autor) nr_sequencia_autor, --Se o KIT possui autorizacao, os itens gerados deste KIT devem assumir o mesmo numero de guia
				max(d.nr_sequencia) nr_sequencia
			from	autorizacao_convenio c,
				material_autorizado d,
				material_atend_paciente b,
				estagio_autorizacao e
			where	c.nr_sequencia		= d.nr_sequencia_autor
			and	c.nr_atendimento	= b.nr_atendimento
			and	e.nr_sequencia		= c.nr_seq_estagio
			and	e.ie_interno 		not in ('70','90')
			and	d.cd_material		= cd_material_kit_w
			and	cd_material_kit_w	is not null
			and	b.nr_sequencia		= nr_seq_matpaci_p
			and	b.dt_atendimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),b.dt_atendimento) and Nvl(c.dt_fim_vigencia,b.dt_atendimento)
			and 	((ie_autor_qtde_p = 'N') or (nvl(d.qt_autorizada,0) > 0))
			union
			select	max(d.nr_sequencia_autor), /* Consistencia somente pelo codigo e prescr e seq prescri*/
				max(d.nr_sequencia)
			from	material_autorizado d,
				material_atend_paciente b,
				autorizacao_convenio c
			where	((b.cd_material			= d.cd_material) or
				(b.cd_material 			= cd_mat_conta_w) or
				(d.cd_material			= b.cd_material_exec))
			and	b.nr_prescricao			= d.nr_prescricao
			and	b.nr_sequencia_prescricao	= d.nr_seq_prescricao
			and	b.nr_sequencia			= nr_seq_matpaci_p
			and	c.nr_sequencia			= d.nr_sequencia_autor
			and	b.dt_atendimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),b.dt_atendimento) and Nvl(c.dt_fim_vigencia,b.dt_atendimento)
			and 	((ie_autor_qtde_p = 'N') or (nvl(d.qt_autorizada,0) > 0))
			union
			select	max(d.nr_sequencia_autor), /* Consistencia somente pelo codigo e prescricao Ocorre quando ha RLA ou itens adicionais da autorizacao*/
				max(d.nr_sequencia)
			from	material_autorizado d,
				material_atend_paciente b,
				autorizacao_convenio c
			where	((b.cd_material			= d.cd_material) or
				(b.cd_material 			= cd_mat_conta_w) or
				(d.cd_material			= b.cd_material_exec))
			and	b.nr_prescricao			= d.nr_prescricao
			and	b.nr_sequencia_prescricao	is null
			and	d.nr_seq_prescricao		is null
			and	b.nr_sequencia			= nr_seq_matpaci_p
			and	c.nr_sequencia			= d.nr_sequencia_autor
			and	b.dt_atendimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),b.dt_atendimento) and Nvl(c.dt_fim_vigencia,b.dt_atendimento)
			and 	((ie_autor_qtde_p = 'N') or (nvl(d.qt_autorizada,0) > 0)))x;
	end if;
end if;

if	(nr_sequencia_autor_w is not null) then
	select	cd_autorizacao,
		cd_senha,
		nr_seq_estagio
	into	cd_autorizacao_w,
		cd_senha_w,
		nr_seq_estagio_w
	from	autorizacao_convenio
	where	nr_sequencia	= nr_sequencia_autor_w;

	if	(nr_sequencia_autor_w is null) then

		select	max(b.nr_sequencia_autor),
			max(b.nr_sequencia)
		into	nr_sequencia_autor_w,
			nr_seq_proc_autor_w
		from	autorizacao_convenio c,
			material_autorizado b,
			material_atend_paciente d
		where	b.nr_sequencia_autor	= c.nr_sequencia
		and	d.nr_sequencia		= nr_seq_matpaci_p
		and	d.cd_material	= b.cd_material
		and	d.dt_atendimento	between Nvl(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(c.dt_inicio_vigencia),d.dt_atendimento) and Nvl(c.dt_fim_vigencia,d.dt_atendimento)
		and 	((ie_autor_qtde_p = 'N') or (nvl(b.qt_autorizada,0) > 0));

	end if;

	if	(nr_seq_estagio_w is not null) then
		select	decode(ie_interno,'10','S','N')
		into	ie_autorizado_w
		from	estagio_autorizacao
		where	nr_sequencia	= nr_seq_estagio_w;
	end if;

end if;

nr_sequencia_autor_p	:= nr_sequencia_autor_w;
nr_seq_proc_autor_p	:= nr_seq_proc_autor_w;
nr_seq_mat_autor_p	:= nr_seq_mat_autor_w;
cd_autorizacao_p	:= cd_autorizacao_w;
cd_senha_p		:= cd_senha_w;
ie_autorizado_p		:= ie_autorizado_w;

end;
/
