create or replace
procedure hd_obter_dados_instalar_pac(	cd_pessoa_fisica_p		Varchar2,
					cd_tipo_evolucao_p		varchar2,
					nr_seq_dialise_p		number,
					cd_estabelecimento_p		number,
					ie_peso_ideal_prescr_p		varchar2,
					ds_evolucao_p			out varchar2,
					dt_evolucao_p			out varchar2,
					nr_seq_exame_hematocrito_p	out number,
					qt_altura_cm_p			out varchar2,
					qt_idade_p			out varchar2,
					ds_sexo_p			out varchar2,
					ds_tempo_p			out varchar2,
					ds_ult_resultado_p		out varchar2,
					qt_pa_sist_pre_pe_p		out number,
					qt_pa_diast_pre_pe_p		out number,
					qt_pa_sist_pre_deitado_p	out number,
					qt_pa_diast_pre_deitado_p	out number,
					qt_peso_pre_p			out number,
					qt_soro_reposicao_p		out number,
					qt_soro_devolucao_p	 	out number,
					qt_peso_ideal_p			out number,
					qt_fluxo_sangue_p		out varchar2,
					ds_duracao_p			out varchar2,
					ds_ultrafiltracao_p		out varchar2,
					ds_heparina_retorno_p		out varchar2,
					ds_protocolo_p			out varchar2,
					ds_agulha_p			out varchar2,
					ie_pode_pesar_p			out varchar2,
					nr_seq_motivo_peso_pac_p	out number,
					nr_seq_motivo_pa_pre_sentado_p	out number,
					nr_seq_motivo_pa_pre_pe_p	out number,
					dif_peso_ideal_atual_p		out number,
					qt_perc_gpid_p			out number,
					dt_dialise_retro_p		out varchar2,
					ie_tipo_dialise_p		out varchar2,
                    qt_ultrafiltracao_p		out number,
					nr_seq_ultra_p			out varchar2,
					nr_seq_mod_dialisador_p out number,
					ie_tipo_hemodialise_p   out varchar2,  
					qt_sodio_p				out number,
					qt_bicarbonato_p		out number,
					qt_dose_p				out varchar2,
					unidade_medida_p		out varchar2) is 

ds_evolucao_w			long;
dt_evolucao_w			date;
nr_seq_exame_hematocrito_w	number(10);
qt_altura_cm_w			varchar2(255);
qt_idade_w			varchar2(255);
ds_sexo_w			varchar2(100);
ds_tempo_w			varchar2(255);
ds_ult_resultado_w		varchar2(255);
qt_pa_sist_pre_pe_w		number(3);
qt_pa_diast_pre_pe_w		number(3);
qt_pa_sist_pre_deitado_w	number(3);
qt_pa_diast_pre_deitado_w	number(3);
qt_peso_pre_w			number(6,3);
qt_soro_reposicao_w		number(10);
qt_soro_devolucao_w	 	number(10);
qt_peso_ideal_w			number(6,3);
qt_fluxo_sangue_w		varchar2(255);
ds_duracao_w			varchar2(255);
ds_ultrafiltracao_w		varchar2(255);
ds_heparina_retorno_w		varchar2(255);
ds_protocolo_w			varchar2(60);
ds_agulha_w			varchar2(60);
ie_pode_pesar_w			varchar2(1);
nr_seq_motivo_peso_pac_w	number(10);
nr_seq_motivo_pa_pre_sentado_w	number(10);
nr_seq_motivo_pa_pre_pe_w	number(10);
dif_peso_ideal_atual_w		number(15,7);
qt_perc_gpid_w			number(15,7);
ie_tipo_dialise_w		varchar2(10);
cd_evolucao_w			number(10);
ie_tipo_hemodialise_w    varchar(10);  
qt_sodio_w               number(10,0);
qt_bicarbonato_w         number(10,0);
nr_seq_mod_dialisador_w		number(10);
qt_dose_w				varchar(10);
unidade_medida_w		varchar(20);

begin
if	(cd_tipo_evolucao_p is not null) then

	select	nvl(max(cd_evolucao),0) 
	into	cd_evolucao_w
	from	evolucao_paciente 
	where	cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	ie_evolucao_clinica	= cd_tipo_evolucao_p
	and	dt_liberacao		is not null;
	
	if	(cd_evolucao_w > 0) then
		select	ds_evolucao,
			dt_evolucao
		into	ds_evolucao_w,
			dt_evolucao_w
		from 	evolucao_paciente 
		where cd_evolucao	= cd_evolucao_w;
	end if;

end if;

select	max(nr_seq_exame_hematocrito)
into	nr_seq_exame_hematocrito_w
from 	hd_parametro 
where 	cd_estabelecimento = cd_estabelecimento_p;

qt_altura_cm_w := substr(obter_dados_pf(cd_pessoa_fisica_p,'AL'),1,255);
                                                
qt_idade_w := substr(obter_dados_pf(cd_pessoa_fisica_p, 'I'),1,255);
                                                
ds_sexo_w := substr(Obter_Sexo_PF(cd_pessoa_fisica_p, 'D'),1,100);
                                                
select	max(substr(hd_obter_dados_maquina(nr_seq_maquina, 'T'),1,255))
into	ds_tempo_w
from 	hd_dialise_dialisador 
where 	nr_seq_dialise = nr_seq_dialise_p;
        
ds_ult_resultado_w := substr(obter_ult_resultado_lab(cd_pessoa_fisica_p, nr_seq_exame_hematocrito_w),1,255);

select 	max(qt_pa_sist_pre_pe),
	max(qt_pa_diast_pre_pe), 
	max(qt_pa_sist_pre_deitado), 
	max(qt_pa_diast_pre_deitado), 
	max(qt_peso_pre), 
	max(qt_soro_reposicao), 
	max(qt_soro_devolucao),
	max(ie_tipo_dialise)
into	qt_pa_sist_pre_pe_w,
	qt_pa_diast_pre_pe_w, 
	qt_pa_sist_pre_deitado_w, 
	qt_pa_diast_pre_deitado_w, 
	qt_peso_pre_w, 
	qt_soro_reposicao_w, 
	qt_soro_devolucao_w,
	ie_tipo_dialise_w
from  	hd_dialise
where 	nr_sequencia = nr_Seq_dialise_p;

if	(ie_peso_ideal_prescr_p = 'S') then

	qt_peso_ideal_w := hd_obter_peso_ideal_prescr(cd_pessoa_fisica_p);
else
	select 	max(qt_peso_ideal)
into	qt_peso_ideal_w
from 	hd_pac_renal_cronico 
where 	cd_pessoa_fisica = cd_pessoa_fisica_p;
end if;

qt_fluxo_sangue_w := hd_obter_dados_prescr_dialise(cd_pessoa_fisica_p, 'FS');

ds_duracao_w := hd_obter_dados_prescr_dialise(cd_pessoa_fisica_p, 'DR');

ds_ultrafiltracao_w := hd_obter_dados_prescr_dialise(cd_pessoa_fisica_p, 'UF');

ds_heparina_retorno_w := hd_obter_dados_prescr_dialise(cd_pessoa_fisica_p, 'HP');

ds_protocolo_w := substr(hd_obter_protocolo_tipo_sol(cd_pessoa_fisica_p, 'A'),1,60);
                                                                    
ds_agulha_w := substr(hd_obter_dados_prescr_dialise(cd_pessoa_fisica_p, 'DA'),1,60);

ie_tipo_hemodialise_w := hd_obter_dados_prescr_dialise(cd_pessoa_fisica_p, 'HE');

nr_seq_mod_dialisador_w := hd_obter_dados_prescr_dialise(cd_pessoa_fisica_p, 'MD');

qt_sodio_w := hd_obter_dados_prescr_dialise(cd_pessoa_fisica_p, 'SO');

qt_bicarbonato_w := hd_obter_dados_prescr_dialise(cd_pessoa_fisica_p, 'BI');

qt_dose_w := HD_obter_dose_unidade_medida(cd_pessoa_fisica_p, 'A', 'D');

unidade_medida_w := HD_obter_dose_unidade_medida(cd_pessoa_fisica_p, 'A', 'UN');

ie_pode_pesar_w := substr(hd_obter_se_pode_pesar(cd_pessoa_fisica_p),1,1);
        
select 	max(nr_seq_motivo_peso_pac) 
into	nr_seq_motivo_peso_pac_w
from 	hd_parametro 
where  	cd_estabelecimento = cd_estabelecimento_p;

select	max(nr_seq_motivo_pa_pre_sentado) nr_seq_motivo_pa_pre_sentado
into	nr_seq_motivo_pa_pre_sentado_w
from 	hd_parametro;

select	max(nr_seq_motivo_pa_pre_pe) nr_seq_motivo_pa_pre_pe
into	nr_seq_motivo_pa_pre_pe_w
from 	hd_parametro;

dif_peso_ideal_atual_w := (nvl(qt_peso_pre_w,0) - nvl(qt_peso_ideal_w,0));

qt_perc_gpid_w :=  hd_obter_gpid(nr_seq_dialise_p, cd_pessoa_fisica_p, nvl(qt_peso_pre_w,0));

ds_evolucao_p 			:= ds_evolucao_w;
dt_evolucao_p 			:= to_char(dt_evolucao_w,'dd/mm/yyyy hh24:mi:ss');
nr_seq_exame_hematocrito_p 	:= nr_seq_exame_hematocrito_w;
qt_altura_cm_p			:= qt_altura_cm_w;
qt_idade_p 			:= qt_idade_w;
ds_sexo_p 			:= ds_sexo_w;
ds_tempo_p 			:= ds_tempo_w;
ds_ult_resultado_p 		:= ds_ult_resultado_w;
qt_pa_sist_pre_pe_p 		:= qt_pa_sist_pre_pe_w;
qt_pa_diast_pre_pe_p 		:= qt_pa_diast_pre_pe_w;
qt_pa_sist_pre_deitado_p 	:= qt_pa_sist_pre_deitado_w;
qt_pa_diast_pre_deitado_p 	:= qt_pa_diast_pre_deitado_w;
qt_peso_pre_p 			:= qt_peso_pre_w;
qt_soro_reposicao_p 		:= qt_soro_reposicao_w;
qt_soro_devolucao_p 		:= qt_soro_devolucao_w;
qt_peso_ideal_p 		:= qt_peso_ideal_w;
qt_fluxo_sangue_p 		:= qt_fluxo_sangue_w;
ds_duracao_p 			:= ds_duracao_w;
ds_ultrafiltracao_p 		:= ds_ultrafiltracao_w;
ds_heparina_retorno_p 		:= ds_heparina_retorno_w;
ds_protocolo_p 			:= ds_protocolo_w;
ds_agulha_p  			:= ds_agulha_w;
ie_pode_pesar_p 		:= ie_pode_pesar_w;
nr_seq_motivo_peso_pac_p 	:= nr_seq_motivo_peso_pac_w;
nr_seq_motivo_pa_pre_sentado_p 	:= nr_seq_motivo_pa_pre_sentado_w;
nr_seq_motivo_pa_pre_pe_p 	:= nr_seq_motivo_pa_pre_pe_w;
dif_peso_ideal_atual_p 		:= dif_peso_ideal_atual_w;
qt_perc_gpid_p 			:= qt_perc_gpid_w;
dt_dialise_retro_p		:= hd_obter_dt_dialise_retro(nr_seq_dialise_p);
ie_tipo_dialise_p		:= ie_tipo_dialise_w;
nr_seq_mod_dialisador_p := nr_seq_mod_dialisador_w;
ie_tipo_hemodialise_p   := ie_tipo_hemodialise_w;  
qt_sodio_p				:= qt_sodio_w;
qt_bicarbonato_p		:= qt_bicarbonato_w;
qt_dose_p				:= qt_dose_w;
unidade_medida_p		:= unidade_medida_w;

end hd_obter_dados_instalar_pac;
/
