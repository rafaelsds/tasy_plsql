Create or replace
procedure pls_gerar_reajuste_preco
			(	nr_seq_reajuste_p	number,
				ie_opcao_p		varchar2,
				ie_commit_p		varchar2,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

qt_idade_inicial_w		number(5);
qt_idade_final_w		number(5);
qt_regras_w			number(5);
qt_registros_w			number(10);
nr_seq_preco_w			number(10);
nr_seq_reajuste_w		number(10);
nr_seq_reaj_periodo_w		number(10);
nr_contrato_w			number(10);
nr_seq_contrato_w		number(10);
nr_sequencia_w			number(10);
nr_seq_tabela_w			number(10);
vl_reajustado_w			number(15,2);
vl_reajuste_w			number(15,2);
vl_preco_w			number(15,2);
vl_base_w			number(15,2);
pr_reajuste_w			number(11,8);
tx_reajuste_lote_w		pls_reajuste.tx_reajuste%type;
tx_reajuste_tabela_w		pls_reajuste_tabela.tx_reajuste%type;
dt_reajuste_ant_w		date;
dt_reajuste_w			date;
dt_ult_reajuste_w		date;
dt_periodo_inicial_w		date;
dt_periodo_final_w		date;
ie_truncar_valor_reajuste_w	varchar2(1);
ie_tabela_contrato_w		varchar2(1);
ie_tipo_contratacao_w		varchar2(2);
ds_contrato_w			varchar2(255);
ds_erro_w			varchar2(4000) := '';
ds_inconsistencia_w		varchar2(4000) := '';
dt_autorizacao_ans_w		date;
pr_deflator_w			pls_reajuste.tx_deflator%type;
vl_reajuste_nao_subsidiado_w	number(15,2)	:= 0;
vl_preco_nao_subsid_base_w	number(15,2)	:= 0;
qt_contrato_tabela_w		number(10);
vl_minimo_w			number(15,2);
vl_minimo_atual_w		number(15,2);
nr_seq_intercambio_w		number(10);
nr_seq_contrato_inter_w		number(10);
vl_adaptacao_base_w		number(15,2);
vl_adaptacao_w			number(15,2);
ie_aplicacao_reajuste_w		varchar2(2);
nr_seq_reaj_preco_w		number(10);
nr_contrato_reaj_w		number(10);

cursor c01 is
	select	a.nr_sequencia,
		b.nr_sequencia,
		b.nr_contrato,
		c.ie_tipo_contratacao,
		b.nr_seq_contrato_inter,
		a.tx_reajuste
	from	pls_reajuste_tabela a,
		pls_tabela_preco b,
		pls_plano c
	where	a.nr_seq_tabela		= b.nr_sequencia
	and	c.nr_sequencia		= b.nr_seq_plano
	and	a.nr_seq_reajuste	= nr_seq_reajuste_p
	and	a.dt_liberacao is null;

cursor c02 is
	select	a.nr_sequencia,
		a.qt_idade_inicial,
		a.qt_idade_final,
		nvl(a.vl_preco_atual,nvl(a.vl_preco_inicial,0)),
		nvl(a.vl_preco_nao_subsid_atual,0),
		nvl(a.vl_minimo,0),
		nvl(a.vl_adaptacao,0)
	from	pls_plano_preco a
	where	a.nr_seq_tabela	= nr_seq_tabela_w
	and	not exists	(select	1
				from	pls_reajuste_preco x
				where	x.nr_seq_preco		= a.nr_sequencia
				and	x.nr_seq_reajuste	= nr_seq_reajuste_p);

begin

select	nvl(tx_reajuste,0),
	trunc(dt_reajuste),
	nvl(dt_periodo_inicial,trunc(dt_reajuste,'month')),
	nvl(dt_periodo_final,last_day(dt_reajuste)),
	nr_seq_contrato,
	dt_autorizacao_ans,
	nvl(tx_deflator,0),
	nr_seq_intercambio,
	ie_aplicacao_reajuste
into	tx_reajuste_lote_w,
	dt_reajuste_w,
	dt_periodo_inicial_w,
	dt_periodo_final_w,
	nr_seq_contrato_w,
	dt_autorizacao_ans_w,
	pr_deflator_w,
	nr_seq_intercambio_w,
	ie_aplicacao_reajuste_w
from	pls_reajuste
where	nr_sequencia	= nr_seq_reajuste_p;

select	count(1)
into	qt_registros_w
from	pls_reajuste_tabela
where	nr_seq_reajuste	= nr_seq_reajuste_p;

if	(qt_registros_w = 0) then
	pls_inserir_tabela_reajuste(nr_seq_reajuste_p, null, nm_usuario_p);
end if;

if	(ie_opcao_p = 'C') then
	pls_consistir_tabela_preco(nr_seq_reajuste_p, ds_inconsistencia_w, ds_erro_w);
	
	if	(nvl(length(ds_erro_w),0) > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort( 186811, 'DS_TABELAS='||ds_erro_w);
		--Tabelas j� calculadas e n�o liberadas em outro reajuste. Verifique! Tabela(s): ' || ds_erro_w
	end if;
else
	tx_reajuste_lote_w	:= pr_deflator_w;
end if;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	nr_seq_tabela_w,
	nr_contrato_w,
	ie_tipo_contratacao_w,
	nr_seq_contrato_inter_w,
	tx_reajuste_tabela_w;
exit when c01%notfound;
	begin
	pr_reajuste_w	:= nvl(tx_reajuste_tabela_w,tx_reajuste_lote_w);
	
	select	count(1)
	into	qt_contrato_tabela_w
	from	pls_tabela_preco a
	where	a.nr_sequencia	= nr_seq_tabela_w
	and	a.nr_contrato	= nr_contrato_w;
	
	if	(ie_tipo_contratacao_w <> 'I') and (qt_contrato_tabela_w > 1) then
		wheb_mensagem_pck.exibir_mensagem_abort( 186809, null);
		--Existem contratos com a mesma tabela de pre�o. Reajuste n�o pode ser calculado!
	end if;
	
	select	max(nr_contrato)
	into	nr_contrato_reaj_w
	from	pls_contrato
	where	nr_sequencia	= nr_seq_contrato_w;
	
	if	(ie_tipo_contratacao_w <> 'I') and
		(((nr_contrato_w is null) and (nr_seq_contrato_inter_w is null)) or
		(((nr_seq_contrato_w is not null) and (nr_seq_contrato_w <> nr_contrato_w)) or
		((nr_seq_intercambio_w is not null) and (nr_seq_intercambio_w <> nr_seq_contrato_inter_w)))) then
		wheb_mensagem_pck.exibir_mensagem_abort( 186812, 'NR_SEQ_TABELA='||nr_seq_tabela_w||';NR_CONTRATO='||nr_contrato_reaj_w);
		--Existem tabelas que n�o s�o do contrato. Verifique! Tabela: ||nr_seq_tabela_w
	end if;
	
	select	count(1)
	into	qt_regras_w
	from	pls_plano_preco
	where	nr_seq_tabela	= nr_seq_tabela_w;
	
	if	(qt_regras_w > 0) then
		open c02;
		loop
		fetch c02 into
			nr_seq_preco_w,
			qt_idade_inicial_w,
			qt_idade_final_w,
			vl_preco_w,
			vl_preco_nao_subsid_base_w,
			vl_minimo_atual_w,
			vl_adaptacao_base_w;
		exit when c02%notfound;
			begin
			
			select	max(nr_sequencia)
			into	nr_seq_reajuste_w
			from	pls_reajuste_preco
			where	nr_seq_preco	= nr_seq_preco_w
			and	dt_reajuste	= (	select	max(dt_reajuste)
							from	pls_reajuste_preco
							where	nr_seq_preco	= nr_seq_preco_w
							and	dt_reajuste	<= dt_reajuste_w);
			
			select	nvl(ie_truncar_valor_reajuste,'N')
			into	ie_truncar_valor_reajuste_w
			from	pls_parametros
			where	cd_estabelecimento	= cd_estabelecimento_p;
			
			if	(nvl(nr_seq_reajuste_w,0) > 0) then
				if (ie_truncar_valor_reajuste_w = 'N') then
					select	(nvl(vl_reajustado,0) / 100) * pr_reajuste_w,
						vl_reajustado,
						dt_reajuste,
						(nvl(vl_preco_nao_subsidiado,0) / 100) * pr_reajuste_w,
						(nvl(vl_minimo,0) / 100) * pr_reajuste_w,
						(vl_adaptacao / 100) * pr_reajuste_w
					into	vl_reajuste_w,
						vl_reajustado_w,
						dt_ult_reajuste_w,
						vl_reajuste_nao_subsidiado_w,
						vl_minimo_w,
						vl_adaptacao_w
					from	pls_reajuste_preco
					where	nr_sequencia	= nr_seq_reajuste_w;
				else
					select	trunc((nvl(vl_reajustado,0) / 100) * pr_reajuste_w,2),
						vl_reajustado,
						dt_reajuste,
						trunc((nvl(vl_preco_nao_subsidiado,0) / 100) * pr_reajuste_w,2),
						trunc((nvl(vl_minimo,0) / 100) * pr_reajuste_w,2),
						trunc((vl_adaptacao / 100) * pr_reajuste_w,2)
					into	vl_reajuste_w,
						vl_reajustado_w,
						dt_ult_reajuste_w,
						vl_reajuste_nao_subsidiado_w,
						vl_minimo_w,
						vl_adaptacao_w
					from	pls_reajuste_preco
					where	nr_sequencia	= nr_seq_reajuste_w;
				end if;
				
				vl_base_w	:= vl_reajustado_w;
			else
				if	(ie_truncar_valor_reajuste_w = 'N') then
					select	(vl_preco_w / 100) * pr_reajuste_w,
						(vl_preco_nao_subsid_base_w / 100) * pr_reajuste_w,
						(vl_minimo_atual_w / 100) * pr_reajuste_w,
						(vl_adaptacao_base_w / 100) * pr_reajuste_w
					into	vl_reajuste_w,
						vl_reajuste_nao_subsidiado_w,
						vl_minimo_w,
						vl_adaptacao_w
					from	dual;
				else
					select	trunc((vl_preco_w / 100) * pr_reajuste_w,2),
						trunc((vl_preco_nao_subsid_base_w / 100) * pr_reajuste_w,2),
						trunc((vl_minimo_atual_w / 100) * pr_reajuste_w,2),
						trunc((vl_adaptacao_base_w / 100) * pr_reajuste_w,2)
					into	vl_reajuste_w,
						vl_reajuste_nao_subsidiado_w,
						vl_minimo_w,
						vl_adaptacao_w
					from	dual;
				end if;
				
				vl_base_w	:= vl_preco_w;
			end if;
			
			vl_minimo_w	:= nvl(vl_minimo_w,0);
			vl_adaptacao_w	:= nvl(vl_adaptacao_w,0);
			
			if	(nvl(vl_adaptacao_w,0) = 0) then
				vl_adaptacao_w	:= trunc((vl_adaptacao_base_w / 100) * pr_reajuste_w,2);
			end if;
			
			select	pls_reajuste_preco_seq.nextval
			into	nr_seq_reaj_preco_w
			from	dual;
			
			insert	into	pls_reajuste_preco
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_reajuste,
				nr_seq_preco,
				nr_seq_reajuste,
				vl_base,
				pr_reajustado,
				vl_reajustado,
				nr_seq_tabela,
				dt_periodo_inicial,
				dt_periodo_final,
				dt_autorizacao_ans,
				vl_preco_nao_subsidiado,
				vl_preco_nao_subsid_base,
				vl_minimo,
				vl_minimo_base,
				vl_adaptacao_base,
				vl_adaptacao)
			values	(nr_seq_reaj_preco_w,
				sysdate,
				nm_usuario_p,
				dt_reajuste_w,
				nr_seq_preco_w,
				nr_seq_reajuste_p,
				vl_base_w,
				pr_reajuste_w,
				vl_base_w + vl_reajuste_w,
				nr_sequencia_w,
				dt_periodo_inicial_w,
				dt_periodo_final_w,
				dt_autorizacao_ans_w,
				vl_preco_nao_subsid_base_w + vl_reajuste_nao_subsidiado_w,
				vl_preco_nao_subsid_base_w,
				vl_minimo_atual_w + vl_minimo_w,
				vl_minimo_atual_w,
				vl_adaptacao_base_w,
				vl_adaptacao_base_w + vl_adaptacao_w);
			end;
		end loop;
		close c02;
	end if;
	end;
end loop;
close c01;

if	(ie_aplicacao_reajuste_w in ('G','S')) then
	pls_gerar_reajuste_sca(nm_usuario_p, nr_seq_reajuste_p, 'N', cd_estabelecimento_p);
end if;

update	pls_reajuste
set	ie_status	= '3',
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_reajuste_p;

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end pls_gerar_reajuste_preco;
/
