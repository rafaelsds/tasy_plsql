create or replace
procedure UMRS_GERAR_W_CONTA_RET_MOVTO
			(nr_seq_retorno_p	number) is
			
/*
Procedure desenvolvida na OS 682431.
Tratamento para somente importar se CNPJ 87701249000374 - Unimed Miss�es/ RS - Cooperativa M�dica Ltda.
*/			
			
nr_doc_convenio_arq_w	varchar2(255);
nr_doc_convenio_w	varchar2(255);
cd_usuario_conv_w	varchar2(255);
cd_usuario_conv_arq_w	varchar2(255);
cd_item_w		number(15);
vl_total_pago_w		number(15,2);
vl_glosado_w		number(15,2);
vl_cobrado_w		number(15,2);
ie_tipo_w		varchar2(255);
nm_beneficiario_w	varchar2(255);	
nr_nota_arq_w		varchar2(255);
nr_nota_w		varchar2(255);	
nr_linha_w		varchar2(10);
cd_convenio_w		number(10);
nr_interno_conta_w	number(10);	
nr_seq_protocolo_w	number(10);
nm_paciente_w		varchar2(255);
nr_seq_item_conta_w	number(10);
nr_sequencia_w		number(10);
cd_cgc_w		varchar2(255);
cd_brasindice_w		varchar2(255);
cd_motivo_glosa_arq_w	number(10);
cd_motivo_glosa_w	number(10);
cd_setor_atendimento_w	number(5);

cursor c01 is
select	substr(ds_conteudo,14,2) nr_linha,
	substr(ds_conteudo,14,4) ie_tipo,
	substr(ds_conteudo,62,6) nr_doc_convenio,
	substr(ds_conteudo,68,5) nr_nota,
	substr(ds_conteudo,20,13) cd_usuario_convenio,	
	to_number(substr(ds_conteudo,39,8)) cd_item,
	trim(substr(ds_conteudo,70,13)) vl_total_pago,
	trim(substr(ds_conteudo,83,9)) vl_glosado,
	trim(substr(ds_conteudo,59,11)) vl_cobrado,
	null nm_paciente,
	trim(substr(ds_conteudo,24,14)) cd_cgc,
	somente_numero(substr(ds_conteudo,92,4)) cd_motivo_glosa
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,16,2)	= '75'
and	substr(ds_conteudo,1,3) 	<> 'SMH'
order by nr_sequencia;

cursor 	c02 is
select	substr(ds_conteudo,1,2) nr_linha,
	substr(ds_conteudo,1,4) ie_tipo,
	null nr_doc_convenio,
	substr(ds_conteudo,49,5) nr_nota,
	substr(ds_conteudo,3,13) cd_usuario_convenio,	
	to_number(substr(ds_conteudo,26,08)) cd_item,
	trim(substr(ds_conteudo,145,13)) vl_total_pago,
	trim(substr(ds_conteudo,158,13)) vl_glosado,
	trim(substr(ds_conteudo,132,13)) vl_cobrado,
	trim(substr(ds_conteudo,39,43)) nm_paciente,
	nr_sequencia
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,1,3)		<> 'SMH'
and	exists
	(select	1
	from	w_conv_ret_movto x
	where	x.nr_seq_retorno	= nr_seq_retorno_p
	and	substr(x.ds_conteudo,1,4) = '0035')
union all
select	substr(ds_conteudo,1,2) nr_linha,
	substr(ds_conteudo,1,4) ie_tipo,
	null nr_doc_convenio,
	substr(ds_conteudo,47,5) nr_nota,
	substr(ds_conteudo,3,13) cd_usuario_convenio,	
	to_number(substr(ds_conteudo,18,08)) cd_item,
	trim(substr(ds_conteudo,113,13)) vl_total_pago,
	trim(substr(ds_conteudo,126,9)) vl_glosado,
	trim(substr(ds_conteudo,100,13)) vl_cobrado,
	trim(substr(ds_conteudo,55,45)) nm_paciente,
	nr_sequencia
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,1,3)		<> 'SMH'
and	exists
	(select	1
	from	w_conv_ret_movto x
	where	x.nr_seq_retorno	= nr_seq_retorno_p
	and	substr(x.ds_conteudo,1,4) = '0055')
order by nr_sequencia;

cursor c03 is
select	substr(ds_conteudo,14,2) nr_linha,
	substr(ds_conteudo,14,4) ie_tipo,
	null nr_doc_convenio,
	substr(ds_conteudo,68,5) nr_nota,
	substr(ds_conteudo,20,13) cd_usuario_convenio,	
	to_number(substr(ds_conteudo,38,08)) cd_item,
	trim(substr(ds_conteudo,82,13)) vl_total_pago,
	trim(substr(ds_conteudo,95,9)) vl_glosado,
	null vl_cobrado,
	null nm_paciente,
	trim(substr(ds_conteudo,24,14)),
	substr(ds_conteudo,70,12) cd_brasindice,
	somente_numero(substr(ds_conteudo,104,6)) cd_motivo_glosa
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,16,2)	= '76'
and	substr(ds_conteudo,1,3) 	<> 'SMH'
order by nr_sequencia;

cursor c04 is
select	substr(ds_conteudo,14,2) nr_linha,
	substr(ds_conteudo,14,4) ie_tipo,
	substr(ds_conteudo,62,6) nr_doc_convenio,
	substr(ds_conteudo,68,5) nr_nota,
	substr(ds_conteudo,20,13) cd_usuario_convenio,	
	to_number(substr(ds_conteudo,39,8)) cd_item,
	trim(substr(ds_conteudo,70,13)) vl_total_pago,
	somente_numero(trim(substr(ds_conteudo,83,9))) vl_glosado,
	trim(substr(ds_conteudo,59,11)) vl_cobrado,
	null nm_paciente,
	trim(substr(ds_conteudo,24,14)),
	somente_numero(substr(ds_conteudo,92,4)) cd_motivo_glosa
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,16,2)	= '85'
and	substr(ds_conteudo,1,3) 	<> 'SMH'
order by nr_sequencia;

cursor c05 is
select	substr(ds_conteudo,14,2) nr_linha,
	substr(ds_conteudo,14,4) ie_tipo,
	null nr_doc_convenio,
	substr(ds_conteudo,68,5) nr_nota,
	substr(ds_conteudo,20,13) cd_usuario_convenio,	
	to_number(substr(ds_conteudo,38,08)) cd_item,
	trim(substr(ds_conteudo,82,13)) vl_total_pago,
	trim(substr(ds_conteudo,95,9)) vl_glosado,
	trim(substr(ds_conteudo,57,13)) vl_cobrado,
	null nm_paciente,
	trim(substr(ds_conteudo,24,14)),
	substr(ds_conteudo,70,12) cd_brasindice,
	somente_numero(substr(ds_conteudo,104,6)) cd_motivo_glosa
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,16,2)	= '86'
and	substr(ds_conteudo,1,3) 	<> 'SMH'
order by nr_sequencia;

cursor c06 is
select	substr(ds_conteudo,14,2) nr_linha,
	substr(ds_conteudo,14,4) ie_tipo,
	null nr_doc_convenio,
	substr(ds_conteudo,68,5) nr_nota,
	substr(ds_conteudo,20,13) cd_usuario_convenio,	
	to_number(substr(ds_conteudo,38,08)) cd_item,
	trim(substr(ds_conteudo,70,13)) vl_total_pago,
	trim(substr(ds_conteudo,83,9)) vl_glosado,
	trim(substr(ds_conteudo,59,11)) vl_cobrado,
	null nm_paciente,
	trim(substr(ds_conteudo,24,14))
from	w_conv_ret_movto
where	nr_seq_retorno			= nr_seq_retorno_p
and	substr(ds_conteudo,16,2)	= '77'
and	substr(ds_conteudo,1,3) 	<> 'SMH'
order by nr_sequencia;

begin

delete 	from convenio_ret_movto_hist
where	nr_seq_retorno	= nr_seq_retorno_p;

select	max(cd_convenio)
into	cd_convenio_w
from	convenio_retorno
where	nr_sequencia	= nr_seq_retorno_p;

open C01;
loop
fetch C01 into	
	nr_linha_w,
	ie_tipo_w,
	nr_doc_convenio_arq_w,
	nr_nota_arq_w,
	cd_usuario_conv_arq_w,	
	cd_item_w,
	vl_total_pago_w,
	vl_glosado_w,
	vl_cobrado_w,
	nm_paciente_w,
	cd_cgc_w,
	cd_motivo_glosa_arq_w;
exit when C01%notfound;

	if	(substr(ie_tipo_w,1,2) = '00') then
		
		nr_nota_w		:= ltrim(nr_nota_arq_w,'0');
		cd_usuario_conv_w	:= cd_usuario_conv_arq_w;
		
		select	max(a.nr_seq_protocolo)
		into	nr_seq_protocolo_w
		from	protocolo_convenio a
		where	a.cd_convenio		= cd_convenio_w
		and	a.ie_status_protocolo	= 2
		and	exists
			(select	1
			from	conta_paciente x
			where	x.nr_seq_protocolo	= a.nr_seq_protocolo
			and	x.nr_conta_convenio	= nr_nota_w);
			
		if	(nr_seq_protocolo_w is not null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	a.nr_seq_protocolo	= nr_seq_protocolo_w
			and	a.nr_conta_convenio	= nr_nota_w			
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;
		end if;
		
		if	(nr_seq_protocolo_w is null) or
			(nr_interno_conta_w is null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	a.ie_status_acerto	= 2
			and	a.nr_seq_protocolo	is not null
			and	a.cd_convenio_parametro	= cd_convenio_w
			and	a.nr_conta_convenio	= nr_nota_w				
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;

		end if;		
		
	else			

		vl_total_pago_w	:= dividir(nvl(vl_total_pago_w,0),100);		
		vl_glosado_w	:= dividir(nvl(vl_glosado_w,0),100);
		vl_cobrado_w	:= dividir(nvl(vl_cobrado_w,0),100);
		
		if	(substr(ie_tipo_w,3,2) = '76') then /*TR 76 n�o tem valor cobrado*/
			vl_cobrado_w	:= null;
		end if;
		
		if	(nvl(cd_motivo_glosa_arq_w,0) > 0) then
		
			begin
				select	cd_motivo_glosa
				into	cd_motivo_glosa_w
				from	convenio_motivo_glosa
				where	cd_convenio		= cd_convenio_w
				and	cd_glosa_convenio	= to_char(cd_motivo_glosa_arq_w);
			exception
			when others then
				Raise_application_error(-20011,	'N�o foi encontrato motivo de glosa no Tasy para o c�digo do arquivo!'||chr(10)||
								'Motivo glosa arquivo: '||cd_motivo_glosa_arq_w);
			end;
		
		end if;
		
		if	(nr_interno_conta_w is not null) then

			select	max(nr_sequencia),
				nvl(max(vl_procedimento),0),
				max(cd_setor_atendimento)				
			into	nr_seq_item_conta_w,
				vl_cobrado_w,
				cd_setor_atendimento_w
			from	procedimento_paciente
			where	nr_interno_conta		= nr_interno_conta_w
			and	cd_motivo_exc_conta		is null
			and	(cd_procedimento		= cd_item_w or
				cd_procedimento_convenio	= cd_item_w or
				cd_procedimento_tuss		= cd_item_w);
			
			if	(nr_seq_item_conta_w is null) then
			
				select	max(nr_sequencia),
					nvl(max(vl_material),0),
					max(cd_setor_atendimento)
				into	nr_seq_item_conta_w,
					vl_cobrado_w,
					cd_setor_atendimento_w
				from	material_atend_paciente
				where	nr_interno_conta			= nr_interno_conta_w
				and	cd_motivo_exc_conta			is null
				and	(cd_material				= cd_item_w or
					cd_material_convenio			= cd_item_w or
					somente_numero(cd_material_tiss)	= cd_item_w);			
			end if;
		end if;
		
		if	(cd_cgc_w = '87701249000374') then
		
			insert into convenio_retorno_movto
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_retorno,
				nr_doc_convenio,
				cd_usuario_convenio,
				cd_item,
				vl_total_pago,
				vl_glosa,
				ds_complemento,
				vl_pago,
				vl_cobrado,			
				nr_conta,
				cd_motivo,
				cd_setor_atendimento)
			values	(convenio_retorno_movto_seq.nextval,
				sysdate,
				'Tasy_Imp',
				nr_seq_retorno_p,
				nr_doc_convenio_w,
				cd_usuario_conv_w,
				cd_item_w,
				vl_total_pago_w,
				vl_glosado_w,
				'TR '||ie_tipo_w||' '||nr_nota_w||' - '||nm_paciente_w,
				vl_total_pago_w,
				vl_cobrado_w,			
				nr_interno_conta_w,
				cd_motivo_glosa_w,
				cd_setor_atendimento_w);
		end if;

	end if;
	
end loop;
close C01;

open C02;
loop
fetch C02 into	
	nr_linha_w,
	ie_tipo_w,
	nr_doc_convenio_arq_w,
	nr_nota_arq_w,
	cd_usuario_conv_arq_w,	
	cd_item_w,
	vl_total_pago_w,
	vl_glosado_w,
	vl_cobrado_w,
	nm_paciente_w,
	nr_sequencia_w;
exit when C02%notfound;

	if	(substr(ie_tipo_w,1,2) = '00') then		
		
		nr_nota_w		:= ltrim(nr_nota_arq_w,'0');		
		
		select	max(a.nr_seq_protocolo)
		into	nr_seq_protocolo_w
		from	protocolo_convenio a
		where	a.cd_convenio		= cd_convenio_w
		and	a.ie_status_protocolo	= 2
		and	exists
			(select	1
			from	conta_paciente x
			where	x.nr_seq_protocolo	= a.nr_seq_protocolo
			and	x.nr_conta_convenio	= nr_nota_w);
	else
		begin
		
		if	(nr_seq_protocolo_w is not null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	a.nr_seq_protocolo	= nr_seq_protocolo_w
			and	a.nr_conta_convenio	= nr_nota_w			
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;
			
		end if;
		
		if	(nr_seq_protocolo_w is null) or
			(nr_interno_conta_w is null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	a.ie_status_acerto	= 2
			and	a.nr_seq_protocolo	is not null
			and	a.cd_convenio_parametro	= cd_convenio_w
			and	a.nr_conta_convenio	= nr_nota_w				
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;

		end if;	
		
		if	(nr_interno_conta_w is not null) then

			select	max(nr_sequencia),
				nvl(max(vl_procedimento),0),
				max(cd_setor_atendimento)				
			into	nr_seq_item_conta_w,
				vl_cobrado_w,
				cd_setor_atendimento_w
			from	procedimento_paciente
			where	nr_interno_conta		= nr_interno_conta_w
			and	cd_motivo_exc_conta		is null
			and	(cd_procedimento		= cd_item_w or
				cd_procedimento_convenio	= cd_item_w or
				cd_procedimento_tuss		= cd_item_w);
			
			if	(nr_seq_item_conta_w is null) then
			
				select	max(nr_sequencia),
					nvl(max(vl_material),0),
					max(cd_setor_atendimento)
				into	nr_seq_item_conta_w,
					vl_cobrado_w,
					cd_setor_atendimento_w
				from	material_atend_paciente
				where	nr_interno_conta			= nr_interno_conta_w
				and	cd_motivo_exc_conta			is null
				and	(cd_material				= cd_item_w or
					cd_material_convenio			= cd_item_w or
					somente_numero(cd_material_tiss)	= cd_item_w);			
			end if;
		end if;

		vl_total_pago_w	:= dividir(nvl(vl_total_pago_w,0),100);		
		vl_glosado_w	:= dividir(nvl(vl_glosado_w,0),100);
		vl_cobrado_w	:= dividir(nvl(vl_cobrado_w,0),100);
		
		if	(substr(ie_tipo_w,3,2) = '76') then /*TR 76 n�o tem valor cobrado*/
			vl_cobrado_w	:= null;
		end if;
		
		insert into convenio_retorno_movto
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_retorno,
			nr_doc_convenio,
			cd_usuario_convenio,
			cd_item,
			vl_total_pago,
			vl_glosa,
			ds_complemento,
			vl_pago,
			vl_cobrado,			
			nr_conta,
			cd_setor_atendimento,
			nr_seq_item_conta)
		values	(convenio_retorno_movto_seq.nextval,
			sysdate,
			'Tasy_Imp',
			nr_seq_retorno_p,
			nr_doc_convenio_w,
			cd_usuario_conv_arq_w,
			cd_item_w,
			vl_total_pago_w,
			vl_glosado_w,
			'TR '||ie_tipo_w||' '||nr_nota_w||' - '||nm_paciente_w,
			vl_total_pago_w,
			vl_cobrado_w,			
			nr_interno_conta_w,
			cd_setor_atendimento_w,
			nr_seq_item_conta_w);
		end;

	end if;

end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_linha_w,
	ie_tipo_w,
	nr_doc_convenio_arq_w,
	nr_nota_arq_w,
	cd_usuario_conv_arq_w,	
	cd_item_w,
	vl_total_pago_w,
	vl_glosado_w,
	vl_cobrado_w,
	nm_paciente_w,
	cd_cgc_w,
	cd_brasindice_w,
	cd_motivo_glosa_arq_w;
exit when C03%notfound;

	if	(substr(ie_tipo_w,1,2) = '00') then
		
		nr_nota_w		:= ltrim(nr_nota_arq_w,'0');
		cd_usuario_conv_w	:= cd_usuario_conv_arq_w;
		
		select	max(a.nr_seq_protocolo)
		into	nr_seq_protocolo_w
		from	protocolo_convenio a
		where	a.cd_convenio		= cd_convenio_w
		and	a.ie_status_protocolo	= 2
		and	exists
			(select	1
			from	conta_paciente x
			where	x.nr_seq_protocolo	= a.nr_seq_protocolo
			and	x.nr_conta_convenio	= nr_nota_w);
			
		if	(nr_seq_protocolo_w is not null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	a.nr_seq_protocolo	= nr_seq_protocolo_w
			and	a.nr_conta_convenio	= nr_nota_w			
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;
			
		end if;
		
		if	(nr_seq_protocolo_w is null) or
			(nr_interno_conta_w is null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	a.ie_status_acerto	= 2
			and	a.nr_seq_protocolo	is not null
			and	a.cd_convenio_parametro	= cd_convenio_w
			and	a.nr_conta_convenio	= nr_nota_w				
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;

		end if;		
		
	else			

		vl_total_pago_w	:= dividir(nvl(vl_total_pago_w,0),100);		
		vl_glosado_w	:= dividir(nvl(vl_glosado_w,0),100);		
		vl_cobrado_w	:= 0;

		if	(cd_cgc_w = '87701249000374') then

			if	(nr_interno_conta_w is not null) then
			
				if	(cd_item_w = '701') then
				
					select	max(cd_material),
						nvl(max(vl_material),0)
					into	cd_item_w,
						vl_cobrado_w
					from	material_atend_paciente a,
						conta_paciente b
					where	a.nr_interno_conta	= b.nr_interno_conta
					and	b.nr_interno_conta	= nr_interno_conta_w
					and	a.cd_motivo_exc_conta	is null
					and	cd_brasindice_w		= (Obter_Dados_Brasindice(b.cd_estabelecimento,a.cd_material,sysdate,b.cd_convenio_parametro,'CMED')||
									Obter_Dados_Brasindice(b.cd_estabelecimento,a.cd_material,sysdate,b.cd_convenio_parametro,'CLAB')||
									Obter_Dados_Brasindice(b.cd_estabelecimento,a.cd_material,sysdate,b.cd_convenio_parametro,'CAPR'));
				
				end if;
				
				select	max(nr_sequencia),
					nvl(max(vl_procedimento),0)
				into	nr_seq_item_conta_w,
					vl_cobrado_w	
				from	procedimento_paciente
				where	nr_interno_conta		= nr_interno_conta_w
				and	cd_motivo_exc_conta		is null
				and	(cd_procedimento		= cd_item_w or
					cd_procedimento_convenio	= cd_item_w or
					cd_procedimento_tuss		= cd_item_w);
				
				if	(nr_seq_item_conta_w is null) then
				
					select	max(nr_sequencia),
						nvl(max(vl_material),0)
					into	nr_seq_item_conta_w,
						vl_cobrado_w
					from	material_atend_paciente
					where	nr_interno_conta			= nr_interno_conta_w
					and	cd_motivo_exc_conta			is null
					and	(cd_material				= cd_item_w or
						cd_material_convenio			= cd_item_w or
						somente_numero(cd_material_tiss)	= cd_item_w);			
				end if;	
				
				select	max(cd_setor_atendimento)
				into	cd_setor_atendimento_w
				from	(select	max(cd_setor_atendimento) cd_setor_atendimento
					from	procedimento_paciente
					where	nr_sequencia	= nr_seq_item_conta_w
					union
					select	max(cd_setor_atendimento) cd_setor_atendimento
					from	material_atend_paciente
					where	nr_sequencia	= nr_seq_item_conta_w);
					
				
			end if;
						
			
			if	(nvl(cd_motivo_glosa_arq_w,0) > 0) then		
				begin
					select	cd_motivo_glosa
					into	cd_motivo_glosa_w
					from	convenio_motivo_glosa
					where	cd_convenio		= cd_convenio_w
					and	cd_glosa_convenio	= to_char(cd_motivo_glosa_arq_w);
				exception
				when others then
					Raise_application_error(-20011,	'N�o foi encontrato motivo de glosa no Tasy para o c�digo do arquivo!'||chr(10)||
									'Motivo glosa arquivo: '||cd_motivo_glosa_arq_w);
				end;		
			end if;		
			
			insert into convenio_retorno_movto
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_retorno,
				nr_doc_convenio,
				cd_usuario_convenio,
				cd_item,
				vl_total_pago,
				vl_glosa,
				ds_complemento,
				vl_pago,
				vl_cobrado,			
				nr_conta,
				nr_seq_item_conta,
				cd_motivo,
				cd_setor_atendimento)
			values	(convenio_retorno_movto_seq.nextval,
				sysdate,
				'Tasy_Imp',
				nr_seq_retorno_p,
				nr_doc_convenio_w,
				cd_usuario_conv_w,
				cd_item_w,
				vl_total_pago_w,
				vl_glosado_w,
				'TR '||ie_tipo_w||' '||nr_nota_w||' - '||nm_paciente_w,
				vl_total_pago_w,
				vl_cobrado_w,			
				nr_interno_conta_w,
				nr_seq_item_conta_w,
				cd_motivo_glosa_w,
				cd_setor_atendimento_w);
		end if;

	end if;
	
end loop;
close C03;

open C04;
loop
fetch C04 into	
	nr_linha_w,
	ie_tipo_w,
	nr_doc_convenio_arq_w,
	nr_nota_arq_w,
	cd_usuario_conv_arq_w,	
	cd_item_w,
	vl_total_pago_w,
	vl_glosado_w,
	vl_cobrado_w,
	nm_paciente_w,
	cd_cgc_w,
	cd_motivo_glosa_arq_w;
exit when C04%notfound;

	if	(substr(ie_tipo_w,1,2) = '00') then
		
		nr_nota_w		:= ltrim(nr_nota_arq_w,'0');
		cd_usuario_conv_w	:= cd_usuario_conv_arq_w;
		
		select	max(a.nr_seq_protocolo)
		into	nr_seq_protocolo_w
		from	protocolo_convenio a
		where	a.cd_convenio		= cd_convenio_w
		and	a.ie_status_protocolo	= 2
		and	exists
			(select	1
			from	conta_paciente x
			where	x.nr_seq_protocolo	= a.nr_seq_protocolo
			and	x.nr_conta_convenio	= nr_nota_w);
			
		if	(nr_seq_protocolo_w is not null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	a.nr_seq_protocolo	= nr_seq_protocolo_w
			and	a.nr_conta_convenio	= nr_nota_w			
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;
			
		end if;
		
		if	(nr_seq_protocolo_w is null) or
			(nr_interno_conta_w is null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	a.ie_status_acerto	= 2
			and	a.nr_seq_protocolo	is not null
			and	a.cd_convenio_parametro	= cd_convenio_w
			and	a.nr_conta_convenio	= nr_nota_w				
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;

		end if;		
		
	else			

		vl_total_pago_w	:= dividir(nvl(vl_total_pago_w,0),100);		
		vl_glosado_w	:= dividir(nvl(vl_glosado_w,0),100);
		vl_cobrado_w	:= dividir(nvl(vl_cobrado_w,0),100);		
		
		if	(cd_cgc_w = '87701249000374') then
		
			if	(nvl(cd_motivo_glosa_arq_w,0) > 0) then		
				begin
					select	cd_motivo_glosa
					into	cd_motivo_glosa_w
					from	convenio_motivo_glosa
					where	cd_convenio		= cd_convenio_w
					and	cd_glosa_convenio	= to_char(cd_motivo_glosa_arq_w);
				exception
				when others then
					Raise_application_error(-20011,	'N�o foi encontrato motivo de glosa no Tasy para o c�digo do arquivo!'||chr(10)||
									'Motivo glosa arquivo: '||cd_motivo_glosa_arq_w);
				end;		
			end if;
			
			if	(nr_interno_conta_w is not null) then

				select	max(nr_sequencia),
					nvl(max(vl_procedimento),0),
					max(cd_setor_atendimento)				
				into	nr_seq_item_conta_w,
					vl_cobrado_w,
					cd_setor_atendimento_w
				from	procedimento_paciente
				where	nr_interno_conta		= nr_interno_conta_w
				and	cd_motivo_exc_conta		is null
				and	(cd_procedimento		= cd_item_w or
					cd_procedimento_convenio	= cd_item_w or
					cd_procedimento_tuss		= cd_item_w);
				
				if	(nr_seq_item_conta_w is null) then
				
					select	max(nr_sequencia),
						nvl(max(vl_material),0),
						max(cd_setor_atendimento)
					into	nr_seq_item_conta_w,
						vl_cobrado_w,
						cd_setor_atendimento_w
					from	material_atend_paciente
					where	nr_interno_conta			= nr_interno_conta_w
					and	cd_motivo_exc_conta			is null
					and	(cd_material				= cd_item_w or
						cd_material_convenio			= cd_item_w or
						somente_numero(cd_material_tiss)	= cd_item_w);			
				end if;
			end if;
		
			insert into convenio_retorno_movto
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_retorno,
				nr_doc_convenio,
				cd_usuario_convenio,
				cd_item,
				vl_total_pago,
				vl_glosa,
				ds_complemento,
				vl_pago,
				vl_cobrado,			
				nr_conta,
				cd_motivo,
				cd_setor_atendimento,
				nr_seq_item_conta)
			values	(convenio_retorno_movto_seq.nextval,
				sysdate,
				'Tasy_Imp',
				nr_seq_retorno_p,
				nr_doc_convenio_w,
				cd_usuario_conv_w,
				cd_item_w,
				vl_total_pago_w,
				vl_glosado_w,
				'TR '||ie_tipo_w||' '||nr_nota_w||' - '||nm_paciente_w,
				vl_total_pago_w,
				vl_cobrado_w,			
				nr_interno_conta_w,
				cd_motivo_glosa_w,
				cd_setor_atendimento_w,
				nr_seq_item_conta_w);
		end if;

	end if;
	
end loop;
close C04;

open C05;
loop
fetch C05 into	
	nr_linha_w,
	ie_tipo_w,
	nr_doc_convenio_arq_w,
	nr_nota_arq_w,
	cd_usuario_conv_arq_w,	
	cd_item_w,
	vl_total_pago_w,
	vl_glosado_w,
	vl_cobrado_w,
	nm_paciente_w,
	cd_cgc_w,
	cd_brasindice_w,
	cd_motivo_glosa_arq_w;
exit when C05%notfound;

	if	(ie_tipo_w = '0086') then
		
		nr_nota_w		:= ltrim(nr_nota_arq_w,'0');
		cd_usuario_conv_w	:= cd_usuario_conv_arq_w;
		
		select	max(a.nr_seq_protocolo)
		into	nr_seq_protocolo_w
		from	protocolo_convenio a
		where	a.cd_convenio		= cd_convenio_w
		and	a.ie_status_protocolo	= 2
		and	exists
			(select	1
			from	conta_paciente x
			where	x.nr_seq_protocolo	= a.nr_seq_protocolo
			and	x.nr_conta_convenio	= nr_nota_w);
			
		if	(nr_seq_protocolo_w is not null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	a.nr_seq_protocolo	= nr_seq_protocolo_w
			and	a.nr_conta_convenio	= nr_nota_w			
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;
			
		end if;
		
		if	(nr_seq_protocolo_w is null) or
			(nr_interno_conta_w is null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	a.ie_status_acerto	= 2
			and	a.nr_seq_protocolo	is not null
			and	a.cd_convenio_parametro	= cd_convenio_w
			and	a.nr_conta_convenio	= nr_nota_w				
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;

		end if;		
		
	else			

		vl_total_pago_w	:= dividir(nvl(vl_total_pago_w,0),100);		
		vl_glosado_w	:= dividir(nvl(vl_glosado_w,0),100);
		vl_cobrado_w	:= dividir(nvl(vl_cobrado_w,0),100);

		if	(cd_cgc_w = '87701249000374') then
		
			if	(cd_item_w = '701') then
				
				select	max(cd_material)
				into	cd_item_w
				from	material_atend_paciente a,
					conta_paciente b
				where	a.nr_interno_conta	= b.nr_interno_conta
				and	b.nr_interno_conta	= nr_interno_conta_w
				and	a.cd_motivo_exc_conta	is null
				and	cd_brasindice_w		= (Obter_Dados_Brasindice(b.cd_estabelecimento,a.cd_material,sysdate,b.cd_convenio_parametro,'CMED')||
									Obter_Dados_Brasindice(b.cd_estabelecimento,a.cd_material,sysdate,b.cd_convenio_parametro,'CLAB')||
									Obter_Dados_Brasindice(b.cd_estabelecimento,a.cd_material,sysdate,b.cd_convenio_parametro,'CAPR'));
			
			end if;
			
			if	(nvl(cd_motivo_glosa_arq_w,0) > 0) then		
				begin
					select	cd_motivo_glosa
					into	cd_motivo_glosa_w
					from	convenio_motivo_glosa
					where	cd_convenio		= cd_convenio_w
					and	cd_glosa_convenio	= to_char(cd_motivo_glosa_arq_w);
				exception
				when others then
					Raise_application_error(-20011,	'N�o foi encontrato motivo de glosa no Tasy para o c�digo do arquivo!'||chr(10)||
									'Motivo glosa arquivo: '||cd_motivo_glosa_arq_w);
				end;		
			end if;
			
			if	(nr_interno_conta_w is not null) then

				select	max(nr_sequencia),
					nvl(max(vl_procedimento),0),
					max(cd_setor_atendimento)				
				into	nr_seq_item_conta_w,
					vl_cobrado_w,
					cd_setor_atendimento_w
				from	procedimento_paciente
				where	nr_interno_conta		= nr_interno_conta_w
				and	cd_motivo_exc_conta		is null
				and	(cd_procedimento		= cd_item_w or
					cd_procedimento_convenio	= cd_item_w or
					cd_procedimento_tuss		= cd_item_w);
				
				if	(nr_seq_item_conta_w is null) then
				
					select	max(nr_sequencia),
						nvl(max(vl_material),0),
						max(cd_setor_atendimento)
					into	nr_seq_item_conta_w,
						vl_cobrado_w,
						cd_setor_atendimento_w
					from	material_atend_paciente
					where	nr_interno_conta			= nr_interno_conta_w
					and	cd_motivo_exc_conta			is null
					and	(cd_material				= cd_item_w or
						cd_material_convenio			= cd_item_w or
						somente_numero(cd_material_tiss)	= cd_item_w);			
				end if;
			end if;
		
			insert into convenio_retorno_movto
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_retorno,
				nr_doc_convenio,
				cd_usuario_convenio,
				cd_item,
				vl_total_pago,
				vl_glosa,
				ds_complemento,
				vl_pago,
				vl_cobrado,			
				nr_conta,
				cd_motivo,
				cd_setor_atendimento,
				nr_seq_item_conta)
			values	(convenio_retorno_movto_seq.nextval,
				sysdate,
				'Tasy_Imp',
				nr_seq_retorno_p,
				nr_doc_convenio_w,
				cd_usuario_conv_w,
				cd_item_w,
				vl_total_pago_w,
				vl_glosado_w,
				'TR '||ie_tipo_w||' '||nr_nota_w||' - '||nm_paciente_w,
				vl_total_pago_w,
				vl_cobrado_w,			
				nr_interno_conta_w,
				cd_motivo_glosa_w,
				cd_setor_atendimento_w,
				nr_seq_item_conta_w);
		end if;

	end if;
	
end loop;
close C05;

open C06;
loop
fetch C06 into	
	nr_linha_w,
	ie_tipo_w,
	nr_doc_convenio_arq_w,
	nr_nota_arq_w,
	cd_usuario_conv_arq_w,	
	cd_item_w,
	vl_total_pago_w,
	vl_glosado_w,
	vl_cobrado_w,
	nm_paciente_w,
	cd_cgc_w;
exit when C06%notfound;

	if	(substr(ie_tipo_w,1,2) = '00') then
		
		nr_nota_w		:= ltrim(nr_nota_arq_w,'0');
		cd_usuario_conv_w	:= cd_usuario_conv_arq_w;
		
		select	max(a.nr_seq_protocolo)
		into	nr_seq_protocolo_w
		from	protocolo_convenio a
		where	a.cd_convenio		= cd_convenio_w
		and	a.ie_status_protocolo	= 2
		and	exists
			(select	1
			from	conta_paciente x
			where	x.nr_seq_protocolo	= a.nr_seq_protocolo
			and	x.nr_conta_convenio	= nr_nota_w);
			
		if	(nr_seq_protocolo_w is not null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	a.nr_seq_protocolo	= nr_seq_protocolo_w
			and	a.nr_conta_convenio	= nr_nota_w			
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;
			
		end if;
		
		if	(nr_seq_protocolo_w is null) or
			(nr_interno_conta_w is null) then
		
			select	max(a.nr_interno_conta)
			into	nr_interno_conta_w
			from	conta_paciente a,
				atend_categoria_convenio b
			where	a.nr_atendimento	= b.nr_atendimento
			and	b.nr_seq_interno	= obter_atecaco_atend_conv(a.nr_atendimento,cd_convenio_w)
			and	a.ie_status_acerto	= 2
			and	a.nr_seq_protocolo	is not null
			and	a.cd_convenio_parametro	= cd_convenio_w
			and	a.nr_conta_convenio	= nr_nota_w				
			and	b.cd_usuario_convenio	= cd_usuario_conv_arq_w;

		end if;		
		
	else			

		vl_total_pago_w	:= dividir(nvl(vl_total_pago_w,0),100);		
		vl_glosado_w	:= dividir(nvl(vl_glosado_w,0),100);
		vl_cobrado_w	:= dividir(nvl(vl_cobrado_w,0),100);

		if	(cd_cgc_w = '87701249000374') then

			if	(nr_interno_conta_w is not null) then

				select	max(nr_sequencia),
					nvl(max(vl_procedimento),0),
					max(cd_setor_atendimento)				
				into	nr_seq_item_conta_w,
					vl_cobrado_w,
					cd_setor_atendimento_w
				from	procedimento_paciente
				where	nr_interno_conta		= nr_interno_conta_w
				and	cd_motivo_exc_conta		is null
				and	(cd_procedimento		= cd_item_w or
					cd_procedimento_convenio	= cd_item_w or
					cd_procedimento_tuss		= cd_item_w);
				
				if	(nr_seq_item_conta_w is null) then
				
					select	max(nr_sequencia),
						nvl(max(vl_material),0),
						max(cd_setor_atendimento)
					into	nr_seq_item_conta_w,
						vl_cobrado_w,
						cd_setor_atendimento_w
					from	material_atend_paciente
					where	nr_interno_conta			= nr_interno_conta_w
					and	cd_motivo_exc_conta			is null
					and	(cd_material				= cd_item_w or
						cd_material_convenio			= cd_item_w or
						somente_numero(cd_material_tiss)	= cd_item_w);			
				end if;
			end if;		
		
			insert into convenio_retorno_movto
				(nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_retorno,
				nr_doc_convenio,
				cd_usuario_convenio,
				cd_item,
				vl_total_pago,
				vl_glosa,
				ds_complemento,
				vl_pago,
				vl_cobrado,			
				nr_conta,
				cd_setor_atendimento,
				nr_seq_item_conta)
			values	(convenio_retorno_movto_seq.nextval,
				sysdate,
				'Tasy_Imp',
				nr_seq_retorno_p,
				nr_doc_convenio_w,
				cd_usuario_conv_w,
				cd_item_w,
				vl_total_pago_w,
				vl_glosado_w,
				'TR '||ie_tipo_w||' '||nr_nota_w||' - '||nm_paciente_w,
				vl_total_pago_w,
				vl_cobrado_w,			
				nr_interno_conta_w,
				cd_setor_atendimento_w,
				nr_seq_item_conta_w);
		end if;

	end if;
	
end loop;
close C06;

delete	from	w_conv_ret_movto
where	nr_seq_retorno	= nr_seq_retorno_p;

commit;

end UMRS_GERAR_W_CONTA_RET_MOVTO;
/