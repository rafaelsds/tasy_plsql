create or replace
procedure mprev_duplicar_busca_emp(	nr_seq_busca_emp_p	number,
					ie_opcao_p		varchar2,
					ie_liberar_p		varchar2,
					nm_usuario_p		varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Gerar os registros de benefici�rios a serem copiados na busca empresarial
ou 
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
IE_OPCAO_P
	'S' - Selecionar
		* Gera todos os benefici�rios do contrato na tabela w_mprev_selecao_busca_emp
	'C' - Copiar
		* Copia para a busca empresarial
		
IE_LIBERAR_P
	'S' - Indica que as copias para a busca empresarial j� ser�o liberadas para a capta��o
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dt_atualizacao_w		w_mprev_selecao_busca_emp.dt_atualizacao%type;
nr_seq_segurado_w		w_mprev_selecao_busca_emp.nr_seq_segurado%type;
cd_usuario_plano_w		w_mprev_selecao_busca_emp.cd_usuario_plano%type;
nm_pessoa_fisica_w		w_mprev_selecao_busca_emp.nm_pessoa_fisica%type;
nr_seq_busca_emp_ref_w		w_mprev_selecao_busca_emp.nr_seq_busca_emp_ref%type;
nr_seq_contrato_w		mprev_busca_empresarial.nr_seq_contrato%type;
nr_sequencia_w			mprev_busca_empresarial.nr_sequencia%type;
nr_seq_cap_destino_w		mprev_captacao_destino.nr_sequencia%type;
nr_seq_cap_diagnostico_w	mprev_captacao_diagnostico.nr_sequencia%type;
cd_pessoa_fisica_w		mprev_busca_empresarial.cd_pessoa_fisica%type;
dt_busca_w			mprev_busca_empresarial.dt_busca%type;
ds_observacao_w			mprev_busca_empresarial.ds_observacao%type;
dt_confirmacao_w		mprev_busca_empresarial.dt_confirmacao%type;
ie_origem_w			mprev_busca_empresarial.ie_origem%type;
nr_contrato_w			mprev_busca_empresarial.nr_contrato%type;
nr_seq_captacao_w		mprev_captacao.nr_sequencia%type;
nr_seq_programa_w		mprev_captacao_destino.nr_seq_programa%type;
nr_seq_campanha_w		mprev_captacao_destino.nr_seq_campanha%type;
ie_patologia_fator_risco_w	mprev_captacao_diagnostico.ie_patologia_fator_risco%type;
nr_seq_diagnostico_int_w	mprev_captacao_diagnostico.nr_seq_diagnostico_int%type;
				
cursor c01 is
	select 	a.dt_atualizacao,
		a.nr_sequencia,
		substr(pls_obter_dados_segurado(a.nr_sequencia, 'C'),1,255) cd_usuario_plano,
		substr(obter_nome_pf(a.cd_pessoa_fisica),1,100) nm_pessoa_fisica
	from 	pls_segurado a,
		mprev_busca_empresarial b
	where	a.nr_sequencia = b.nr_seq_segurado(+) 
	and 	a.nr_seq_contrato = nr_seq_contrato_w
	and 	b.nr_sequencia is null
	and	dt_liberacao is not null
	and	(dt_rescisao >= sysdate or dt_rescisao is null);	
	
cursor c02 is
	select	a.nr_seq_segurado,
		a.cd_usuario_plano,
		b.nr_seq_contrato,
		b.dt_busca,
		b.ds_observacao,
		b.ie_origem,
		b.nr_contrato
	from	w_mprev_selecao_busca_emp a,
		mprev_busca_empresarial b		
	where	a.nr_seq_busca_emp_ref = b.nr_sequencia
	and	a.nm_usuario = nm_usuario_p
	and	nvl(a.ie_selecionado, 'N') = 'S';
	
cursor c03 is
	select	a.nr_seq_programa,
		a.nr_seq_campanha
	from	mprev_captacao_destino a,
		mprev_captacao b
	where	a.nr_seq_captacao = b.nr_sequencia
	and	b.nr_seq_busca_emp = nr_seq_busca_emp_p;

cursor c04 is
	select	a.ie_patologia_fator_risco,
		a.nr_seq_diagnostico_int
	from	mprev_captacao_diagnostico a,
		mprev_captacao b
	where	a.nr_seq_captacao = b.nr_sequencia
	and	b.nr_seq_busca_emp = nr_seq_busca_emp_p;
begin

/*Se for op��o Selecionar, deve-se abrir um cursor buscando todos os registros da PLS_SEGURADO que possuem
o mesmo NR_SEQ_CONTRATO da busca empresarial que est� sendo copiada (par�metro)*/

/* Dentro deste cursor deve-se gravar um registro na tabela w_mprev_selecao_busca_emp para
cada benefici�rio, com o nome e carteirinha e o campo selecionado como 'N' */

if (ie_opcao_p is not null) and (ie_opcao_p = 'S') then

	select 	max(nr_seq_contrato)
	into	nr_seq_contrato_w
	from 	mprev_busca_empresarial 
	where 	nr_sequencia = nr_seq_busca_emp_p;
	
	if (nr_seq_contrato_w is not null) then
		
		delete from w_mprev_selecao_busca_emp
		where nm_usuario = nm_usuario_p;
		commit;
		
		open c01;
		loop
		fetch c01 into 
			dt_atualizacao_w,
			nr_seq_segurado_w,
			cd_usuario_plano_w,
			nm_pessoa_fisica_w;
		exit when c01%notfound;
			begin
		
				select 	w_mprev_selecao_busca_emp_seq.nextval
				into	nr_sequencia_w
				from 	dual;
			
				insert into w_mprev_selecao_busca_emp(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_busca_emp_ref,
					nr_seq_segurado,
					ie_selecionado,
					cd_usuario_plano,
					nm_pessoa_fisica)
				values 
					(nr_sequencia_w,
					dt_atualizacao_w,
					nm_usuario_p,
					nr_seq_busca_emp_p,
					nr_seq_segurado_w,
					'N',
					nvl(cd_usuario_plano_w,' '),
					nm_pessoa_fisica_w);				
					
			end;
		end loop;
		close c01;

	end if;
/* Se a op��o for Copiar, deve-se abrir um cursor com todos os registros da tabela w_mprev_selecao_busca_emp
que estavam selecionados */

/* Dentro deste cursor deve-se gerar um novo registro de busca empresarial para esse benefici�rio do cursor
copiando as demais informa��es do registro de busca empresarial que est� sendo copiado (inclusive as duas tabelas filhas) */

/* Aqui ainda deve ser verificado o par�metro liberar, se o mesmo estiver como 'S' j� deve ser ainda dentro deste
chamada a procedure mprev_confirmar_busca_emp (ser� necess�rio incluir um par�metro na mesma para n�o realizar commit ) */

elsif (ie_opcao_p is not null) and (ie_opcao_p = 'C') then

	open c02;
	loop
	fetch c02 into 
		nr_seq_segurado_w,
		cd_usuario_plano_w,
		nr_seq_contrato_w,
		dt_busca_w,
		ds_observacao_w,
		ie_origem_w,
		nr_contrato_w;
	exit when c02%notfound;
		begin
		
			select	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	pls_segurado
			where	nr_sequencia = nr_seq_segurado_w;
		
			select 	mprev_busca_empresarial_seq.nextval
			into	nr_sequencia_w
			from 	dual;
			
			insert into mprev_busca_empresarial(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_contrato,
				nr_seq_segurado,
				cd_pessoa_fisica,
				dt_busca,
				ds_observacao,
				ie_origem,
				nr_contrato)
			values
				(nr_sequencia_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_contrato_w,
				nr_seq_segurado_w,
				cd_pessoa_fisica_w,
				dt_busca_w,
				ds_observacao_w,
				ie_origem_w,
				nr_contrato_w);
				
			mprev_gerar_captacao_emp(nr_sequencia_w, nm_usuario_p);
			
			SELECT	nr_sequencia
			into	nr_seq_captacao_w
			FROM	mprev_captacao
			WHERE 	nr_seq_busca_emp = nr_sequencia_w;
				
			open c03;
			loop
			fetch c03 into 
				nr_seq_programa_w,
				nr_seq_campanha_w;
			exit when c03%notfound;
				begin			
					select 	mprev_captacao_destino_seq.nextval
					into	nr_seq_cap_destino_w
					from 	dual;
			
					insert into mprev_captacao_destino(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_captacao,
						nr_seq_programa,
						nr_seq_campanha)
					values
						(nr_seq_cap_destino_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_captacao_w,
						nr_seq_programa_w,
						nr_seq_campanha_w
						);
				end;
			end loop;
			close c03;

			open c04;
			loop
			fetch c04 into
				ie_patologia_fator_risco_w,
				nr_seq_diagnostico_int_w;
			exit when c04%notfound;
				begin
					select 	mprev_captacao_diagnostico_seq.nextval
					into	nr_seq_cap_diagnostico_w
					from 	dual;
					
					insert into mprev_captacao_diagnostico(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_seq_captacao,
						ie_patologia_fator_risco,
						nr_seq_diagnostico_int)
					values
						(nr_seq_cap_diagnostico_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_captacao_w,
						ie_patologia_fator_risco_w,
						nr_seq_diagnostico_int_w);
				end;
			end loop;
			close c04;
					
				
			
			if (ie_liberar_p is not null) and (ie_liberar_p = 'S') then
				mprev_confirmar_busca_emp(nr_sequencia_w, 'N', nm_usuario_p);
			end if;
		end;
	end loop;
	close c02;
	
end if;
commit;

end  mprev_duplicar_busca_emp;
/