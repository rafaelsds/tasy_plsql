create or replace
procedure arquivo_plano_conta (cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				nm_usuario_p		varchar2,
				cd_empresa_p		number) is

dt_atualizacao_w		varchar2(8);
cd_conta_contabil_w		varchar2(28);
ie_tipo_w			varchar2(1);
cd_conta_sup_w			varchar2(28);
ds_conta_contabil_w		varchar2(45);
cd_estabelecimento_w		varchar2(10);
contador_w			number;
ds_arquivo_ww			varchar2(4000);


cursor	c01 is
select	lpad(nvl(to_char(dt_atualizacao,'ddmmyyyy'),' '),8,' ')           dt_atualizacao,
	lpad(nvl(to_char(a.cd_conta_contabil),' '),28,' ')		cd_conta_contabil,
	lpad(nvl(decode(a.ie_tipo,'T','S','A'),' '),1,' ')		ie_tipo,
	lpad(nvl(to_char(substr(ctb_obter_codigo_classif_vig(cd_empresa,ctb_obter_classif_conta_sup(a.cd_classificacao, trunc(dt_inicio_p,'year'), cd_empresa_p),dt_inicio_p),1,28)),' '),28,' ') cd_conta_sup,
	rpad(nvl(a.ds_conta_contabil,' '),45,' ')		ds_conta_contabil
from	conta_contabil a
where	a.cd_empresa = cd_empresa_p
and	substr(obter_se_conta_vigente2(cd_conta_contabil, dt_inicio_vigencia, dt_fim_vigencia, dt_inicio_p),1,1) = 'S';

begin

open c01;
loop
fetch c01 into

dt_atualizacao_w,
cd_conta_contabil_w,
ie_tipo_w,
cd_conta_sup_w,
ds_conta_contabil_w;

exit when c01%notfound;

	begin

	contador_w := contador_w + 1;

	ds_arquivo_ww	:=	dt_atualizacao_w	||
				cd_conta_contabil_w	||
				ie_tipo_w		||
				cd_conta_sup_w		||
				ds_conta_contabil_w;

insert into w_inss_direp_arquivo ( nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ds_arquivo_w)
		values		(w_inss_direp_arquivo_seq.nextval,
				cd_estabelecimento_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				ds_arquivo_ww);

	if (mod(contador_w,100) = 0) then
		commit;
	end if;

	end;

end loop;
close c01;

commit;

end arquivo_plano_conta;
/
 
