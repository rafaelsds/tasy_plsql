create or replace
procedure valida_etapa_iniciada ( 	nr_seq_checkup_etapa_p 	number,
					nm_usuario_p		varchar2) is 

nr_seq_etapa_w		number(10);
nr_seq_ck_etapa_w 	number(10);
nr_seq_checkup_w 	number(10);
qt_max_paciente_w 	number(10);
qt_paciente_etapa_w 	number(10);
dt_ini_real_w		date;
dt_fim_etapa_w		date;
dt_cancel_etapa_w	date;
nm_paciente_w		varchar2(255);
ds_etapa_w		varchar2(255);

begin

if ( nr_seq_checkup_etapa_p is not null ) then
	begin
	/*busca o nr_seq_etapa para verificar se existe alguma etapa iniciada */
	select 	max(a.nr_seq_etapa),
		max(a.nr_seq_checkup)
	into 	nr_seq_etapa_w,
		nr_seq_ck_etapa_w
	from 	checkup_etapa a
	where 	a.nr_sequencia = nr_seq_checkup_etapa_p;

	select 	nvl(a.qt_maxima_paciente,0)
	into	qt_max_paciente_w
	from  	etapa_checkup a
	where 	a.nr_sequencia = nr_seq_etapa_w;
	
	select 	count(*)
	into	qt_paciente_etapa_w
	from    checkup_etapa a
	where   a.nr_seq_etapa = nr_seq_etapa_w
	and     a.dt_inicio_real is not null
	and     trunc(a.dt_prevista) between trunc(sysdate) and fim_dia(sysdate);	

	select  max(a.nr_seq_checkup),
		max(a.dt_inicio_real),
		max(a.dt_fim_etapa),
		max(a.dt_cancelamento)
	into	nr_seq_checkup_w,
		dt_ini_real_w,
		dt_fim_etapa_w,
		dt_cancel_etapa_w
	from    checkup_etapa a
	where   a.nr_seq_etapa = nr_seq_etapa_w
	and     a.dt_inicio_real is not null
	and     trunc(a.dt_prevista) between trunc(sysdate) and fim_dia(sysdate);

	if ( qt_max_paciente_w > 0 ) and
	   ( qt_paciente_etapa_w >= qt_max_paciente_w ) and
	   ( nr_seq_checkup_w 	is not null ) and
	   ( dt_ini_real_w 	is not null ) and
	   ( dt_fim_etapa_w 	is null ) and
	   ( dt_cancel_etapa_w  is null ) then
		begin
		select 	max(obter_nome_pf(a.cd_pessoa_fisica))
		into	nm_paciente_w
		from 	checkup a
		where 	a.nr_sequencia = nr_seq_checkup_w;
		-- Essa etapa esta sendo ocupada pela pelo paciente '|| nm_paciente_w ||'#@#@'
		Wheb_mensagem_pck.exibir_mensagem_abort(264554,'NM_PACIENTE_W=' || NM_PACIENTE_W);
		end;
	end if;
	end;
	
/* verificar se existe alguma etapa para o paciente em aberto*/
	select  max(a.dt_inicio_real),
		max(a.dt_fim_etapa),
		max(a.dt_cancelamento),
		max(substr(Obter_desc_etapa_checkup(a.nr_seq_etapa),1,255)),
		max(obter_nome_pf(b.cd_pessoa_fisica))
	into	dt_ini_real_w,
		dt_fim_etapa_w,
		dt_cancel_etapa_w,
		ds_etapa_w,
		nm_paciente_w
	from    checkup_etapa a,
		checkup b
	where   a.nr_seq_checkup = nr_seq_ck_etapa_w
	and	b.nr_sequencia	= a.nr_seq_checkup
	and     a.dt_inicio_real is not null
	and     a.dt_fim_etapa is null;
	
	if ( dt_ini_real_w 	is not null ) and
	   ( dt_fim_etapa_w 	is null ) and
	   ( dt_cancel_etapa_w 	is null) then
	   -- Paciente '|| nm_paciente_w || ' esta em atendimento na etapa: '|| ds_etapa_w || '#@#@'
		Wheb_mensagem_pck.exibir_mensagem_abort(264556,	'NM_PACIENTE_W='||NM_PACIENTE_W || ';' ||'DS_ETAPA_W=' ||DS_ETAPA_W);
	end if;
	   
end if;

commit;

end valida_etapa_iniciada;
/