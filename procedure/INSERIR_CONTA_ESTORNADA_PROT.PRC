create or replace
procedure Inserir_Conta_Estornada_Prot(		nr_seq_prot_dest_p	number,
						nr_interno_conta_p	number,
						nm_usuario_p		Varchar2) is 

nr_interno_conta_est_w		number(10,0);
nr_protocolo_w			varchar2(40) := '';
		
begin

begin
select	nr_protocolo
into	nr_protocolo_w
from	protocolo_convenio
where	nr_seq_protocolo = nr_seq_prot_dest_p;
exception
when others then
	nr_protocolo_w := '';
end;

select	nvl(max(nr_interno_conta),0)
into	nr_interno_conta_est_w
from	conta_paciente
where	nr_seq_conta_origem = nr_interno_conta_p
and	ie_cancelamento = 'E';

if	(nr_interno_conta_est_w <> 0) and
	(nvl(nr_protocolo_w,'X') <> 'X')then 

	update 	conta_paciente
	set 	nr_seq_protocolo	= nr_seq_prot_dest_p,
		nr_protocolo		= nr_protocolo_w,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where 	nr_interno_conta 	= nr_interno_conta_est_w;	
				
end if;

commit;

end Inserir_Conta_Estornada_Prot;
/