create or replace
procedure paf_atualizar_cupom_fiscal(
			nr_seq_cupom_fiscal_p	number,
			nm_usuario_p		varchar2,			
			cd_pessoa_fisica_p	varchar2,
			cd_cnpj_p		varchar2,
			ie_situcao_p		varchar2,		
			ds_assinatura_p		varchar2,
			nr_senha_p		varchar2,
			nr_seq_caixa_p		varchar2,
			nr_coo_p		varchar2,
			ie_ordem_desconto_p	varchar2,
			ie_tipo_desconto_p	varchar2,
			ie_tipo_acrescimo_p	varchar2,
			vl_acrescimo_p		number,
			vl_desconto_p		number) is 

begin

update	paf_cupom_fiscal
set	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	cd_pessoa_fisica = cd_pessoa_fisica_p,
	cd_cnpj = cd_cnpj_p,	
	ie_situacao = ie_situcao_p,
	ds_assinatura = ds_assinatura_p,
	nr_senha = nr_senha_p,
	nr_seq_caixa = nr_seq_caixa_p,
	nr_coo = nr_coo_p,
	ie_ordem_desconto = ie_ordem_desconto_p,
	ie_tipo_desconto = ie_tipo_desconto_p,
	ie_tipo_acrescimo = ie_tipo_acrescimo_p,
	vl_acrescimo = vl_acrescimo_p,
	vl_desconto = vl_desconto_p
where	nr_sequencia = nr_seq_cupom_fiscal_p;

commit;

end paf_atualizar_cupom_fiscal;
/
