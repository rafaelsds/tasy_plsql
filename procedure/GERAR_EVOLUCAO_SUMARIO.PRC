create or replace
procedure gerar_evolucao_sumario(	nr_seq_sumario_p	number,
									ds_texto_p		long,
									nm_usuario_p		Varchar2) is 
									
ie_gera_evolucao_w		varchar2(1);
cd_medico_w				varchar2(10);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
cd_evolucao_w			number(10);

begin
obter_param_usuario(281,1444,obter_perfil_ativo,nm_usuario_p,obter_estabelecimento_ativo,ie_gera_evolucao_w);

if	(ie_gera_evolucao_w = 'S') then
	select	max(nr_atendimento),
			max(cd_medico_resp_alta)
	into	nr_atendimento_w,
			cd_medico_w
	from	atend_sumario_alta
	where	nr_sequencia = nr_seq_sumario_p;
	
	select	max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;
	
	select	evolucao_paciente_seq.nextval
	into	cd_evolucao_w
	from	dual;
	
	insert into evolucao_paciente(	cd_evolucao,
					dt_evolucao,
					ie_tipo_evolucao,
					cd_pessoa_fisica,
					dt_atualizacao,
					nm_usuario,
					nr_atendimento,
					cd_medico,
					ie_evolucao_clinica,
					ie_situacao)
			values	(	cd_evolucao_w,
					sysdate,
					'1',
					cd_pessoa_fisica_w,
					sysdate,
					nm_usuario_p,
					nr_atendimento_w,
					cd_medico_w,
					'ESA',
					'A');
	
	commit;

	copia_campo_long_de_para_novo('ATEND_SUMARIO_ALTA_RESUMO',
				 'DS_RESUMO',
				 'where nr_seq_atend_sumario = :nr_sequencia',
				 'nr_sequencia='||nr_seq_sumario_p,
				 'EVOLUCAO_PACIENTE',
				 'DS_EVOLUCAO',
				 'where cd_evolucao = :cd_evolucao',
				 'cd_evolucao='||cd_evolucao_w,
				 'L');
				 

	commit;
	
	liberar_evolucao(cd_evolucao_w,nm_usuario_p);
	
	gerar_evolucao_vinculo(nr_seq_sumario_p,cd_evolucao_w,'SU');
	
end if;

commit;

end gerar_evolucao_sumario;
/
