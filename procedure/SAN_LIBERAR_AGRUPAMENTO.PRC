create or replace
procedure San_Liberar_Agrupamento (	nr_seq_agrupamento_p	Number,
					nr_seq_inutilizacao_p	Number,
					nm_usuario_p		Varchar2) is 

nr_seq_producao_w	Number(10);
qt_volume_w		Number(4) := 0;
nr_seq_derivado_w	Number(10);
nr_sangue_w		Varchar2(20);
dt_vencimento_w		Date;
ie_tipo_sangue_w	Varchar2(2);
ie_fator_rh_w		Varchar2(1);
					
Cursor C01 is
	select	nr_seq_producao
	from	san_agrupamento_derivado
	where	nr_seq_agrupamento = nr_seq_agrupamento_p
	order by 1;
					
begin

if (nr_seq_agrupamento_p is not null) then

	open C01;
	loop
	fetch C01 into	
		nr_seq_producao_w;
	exit when C01%notfound;
		begin
		
		select	nvl(qt_volume,0) + qt_volume_w
		into	qt_volume_w
		from	san_producao
		where	nr_sequencia = nr_seq_producao_w;
		
		-- Inutilizar os hemocomponentes agrupados
		inutilizar_hemocomp_producao(nr_seq_producao_w, nr_seq_inutilizacao_p, 'AGRUPAMENTO_TS', nm_usuario_p);
		
		end;
	end loop;
	close C01;

	select	nr_seq_derivado,
		nr_sangue,
		dt_vencimento,
		ie_tipo_sangue,
		ie_fator_rh
	into	nr_seq_derivado_w,
		nr_sangue_w,
		dt_vencimento_w,
		ie_tipo_sangue_w,
		ie_fator_rh_w
	from	san_agrupamento
	where	nr_sequencia = nr_seq_agrupamento_p;
	
	--Criar uma nova bolsa
	insert into san_producao (	
		nr_sequencia,
		nr_seq_derivado,
		dt_producao,
		cd_pf_realizou,
		dt_vencimento,
		nr_sangue,
		ie_irradiado,
		ie_lavado,
		ie_filtrado,
		ie_aliquotado,
		ie_aferese,
		dt_atualizacao,
		nm_usuario,
		qt_volume,
		nr_seq_agrupamento,
		dt_liberacao,
		nm_usuario_lib,
		ie_tipo_sangue,
		ie_fator_rh,
		cd_estabelecimento)
	values(	san_producao_seq.nextval,
		nr_seq_derivado_w,
		sysdate,
		obter_pf_usuario(nm_usuario_p,'C'),
		dt_vencimento_w,
		nr_sangue_w,
		'N',
		'N',
		'N',
		'N',
		'N',
		sysdate,
		nm_usuario_p,
		qt_volume_w,
		nr_seq_agrupamento_p,
		sysdate,
		nm_usuario_p,
		ie_tipo_sangue_w,
		ie_fator_rh_w,
		wheb_usuario_pck.get_cd_estabelecimento);
		
	update	san_agrupamento
	set	dt_liberacao = sysdate
	where	nr_sequencia = nr_seq_agrupamento_p;

end if;

commit;

end San_Liberar_Agrupamento;
/