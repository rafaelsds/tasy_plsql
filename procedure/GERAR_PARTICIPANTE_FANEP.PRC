create or replace
procedure gerar_participante_fanep(	cd_participante_p	varchar2,
					ie_funcao_p		varchar2,
					ie_opcao_funcao_p	varchar2,
					nr_seq_pepo_p		number,
					ie_commit_p		varchar2 default 'S',
					nm_usuario_p		varchar2)
				as

/*
ie_opcao_funcao_p	
'A' - Anestesista
*/

nr_sequencia_w		number(10,0);
qt_registro_w		number(5,0) := 0;

begin
if	(ie_funcao_p is not null) and
	(nvl(nr_seq_pepo_p,0) > 0) then
	select 	count(*)
	into	qt_registro_w
	from	pepo_participante
	where	nr_seq_pepo		=	nr_seq_pepo_p
	and	cd_pessoa_fisica	=	cd_participante_p
	and	ie_funcao		=	ie_funcao_p;

	if	(qt_registro_w = 0) then
		insert into pepo_participante(
			nr_seq_pepo,
			nr_sequencia,
			ie_funcao,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_pessoa_fisica)
		values(
			nr_seq_pepo_p,
			pepo_participante_seq.nextval,
			ie_funcao_p,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			cd_participante_p);
	end if;

	if 	(ie_commit_p = 'S') then
		commit;
	end if;
end if;	

end gerar_participante_fanep;
/
