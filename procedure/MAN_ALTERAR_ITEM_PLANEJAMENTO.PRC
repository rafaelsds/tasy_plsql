create or replace
procedure man_alterar_item_planejamento(nr_sequencia_p	number,
					ds_item_p	varchar2,
					nr_story_p	number,
					nr_feature_p	number,
					nr_epic_p	number,
					nm_usuario_p	varchar2) is 

begin

update	desenv_item_planejamento
set	ds_item = ds_item_p,
	nr_story = nr_story_p,
	nr_feature = nr_feature_p,
	nr_epic = nr_epic_p
where	nr_sequencia = nr_sequencia_p;

commit;

end man_alterar_item_planejamento;
/
