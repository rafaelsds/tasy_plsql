create or replace 
procedure SIMULACAO_PROC_WINT (
        nr_sequencia_p		varchar2,
        qt_servico_p		number,
        vl_coparticipacao_p		number,
        ds_erro_p		varchar2) is

begin
if (ds_erro_p is not null) then
    begin
	update	procedimento_guia_wint
	set		ds_erro = ds_erro_p
	where	nr_sequencia = nr_sequencia_p;
    end;
else
    begin	
	update  procedimento_guia_wint
	set     qt_procedimento = nvl(qt_servico_p, 0),
            vl_coparticipacao = nvl(vl_coparticipacao_p, 0),
            ds_erro = null
    where	nr_sequencia = nr_sequencia_p;
    end;
end if;

commit;

end SIMULACAO_PROC_WINT;
/
