create or replace
procedure Qua_Nao_Conform_Causa_Update(
			nm_usuario_p		Varchar2,
			nr_seq_nao_conform_p number,
			dt_prevista_p			date,
			ie_abrangencia_p		Varchar2,
			nr_sequencia_p 			number) is 

nm_user_w			varchar2(50);			
old_value_dt_prevista_w			date;			
			
begin

select	username
into	nm_user_w
from	user_users;

select	max(dt_conclusao_desejada)
into	old_value_dt_prevista_w
from	man_ordem_servico
where 	nr_seq_causa_rnc	= nr_sequencia_p 
and		nr_seq_nao_conform = nr_seq_nao_conform_p;


if	(nm_user_w = 'CORP') then 
		
	if	(nvl(nr_seq_nao_conform_p, '0') > '0') then
		begin
			if	(nvl(ie_abrangencia_p, 'N') = 'S') then
				if	(dt_prevista_p <> old_value_dt_prevista_w) then
					update	man_ordem_servico
					set		dt_conclusao_desejada 	= dt_prevista_p,
							dt_atualizacao	= sysdate,
							nm_usuario	= nm_usuario_p
					where	nr_seq_causa_rnc	= nr_sequencia_p 
					and		nr_seq_nao_conform = nr_seq_nao_conform_p;
				end if;
			end if;
		end;
	end if;
end if;



commit;

end Qua_Nao_Conform_Causa_Update;
/
