create or replace
procedure freeze_surgical_schedules(	shedules_sequencies_p varchar2,
					ie_inconsistencies_p out nocopy varchar2) is

ie_frozen_status_w 		varchar2(4000);
ds_query_w 			varchar2(255);
id 				number(10);
nr_schedule_sequence_w		varchar2(10);
qt_inconsistencies_w		number(10);

cursor sequencies is
select level as id, regexp_substr(shedules_sequencies_p, '[^,]+', 1, level) as data
from dual
connect by regexp_substr(shedules_sequencies_p, '[^,]+', 1, level) is not null;

begin

if (shedules_sequencies_p is not null) then

	ie_inconsistencies_p := 'N';

	open sequencies;
	loop
	fetch sequencies into	
		id, 
		nr_schedule_sequence_w;
	exit when sequencies%notfound;
		
		gerar_cirurgia_agenda_html5(nr_schedule_sequence_w);
	
	end loop;
	close sequencies;
	
	select 	max(ie_status_agenda)
	into 	ie_frozen_status_w
	from 	regra_agenda_fechada;

	ds_query_w := '	update agenda_paciente 
			set	ie_status_agenda = ' || chr(39) || ie_frozen_status_w || chr(39) ||
		'	where nr_sequencia in(' || shedules_sequencies_p || ')';
		
	execute immediate ds_query_w;
	
	commit;
	
	ds_query_w := ' select count(1) 
			from w_agenda_cirurgica_consist 
			where nr_seq_agenda in(' || shedules_sequencies_p || ')
			and ie_tipo <> ' || chr(39) || 'R' || chr(39);
			
	execute immediate ds_query_w into qt_inconsistencies_w;
	
	if (qt_inconsistencies_w > 0) then
		ie_inconsistencies_p := 'S';
	end if;

end if;

end freeze_surgical_schedules;
/
