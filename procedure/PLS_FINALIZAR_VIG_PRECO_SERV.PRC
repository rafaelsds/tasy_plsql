create or replace
procedure pls_finalizar_vig_preco_serv(	cd_tabela_servico_p	in preco_servico.cd_tabela_servico%type,
					dt_inicio_vigencia_p	in preco_servico.dt_inicio_vigencia%type,
					dt_vigencia_final_p	in preco_servico.dt_vigencia_final%type,
					cd_procedimento_p	in preco_servico.cd_procedimento%type,
					cd_estabelecimento_p	in preco_servico.cd_estabelecimento%type,
					nm_usuario_p		in usuario.nm_usuario%type) is
begin

	update	preco_servico
	set	dt_vigencia_final	= dt_vigencia_final_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	cd_tabela_servico	= cd_tabela_servico_p
	and	dt_inicio_vigencia	= dt_inicio_vigencia_p
	and	cd_procedimento		= cd_procedimento_p
	and	cd_estabelecimento_p	= cd_estabelecimento_p
	and	dt_vigencia_final	is null;

end pls_finalizar_vig_preco_serv;
/