create or replace
procedure gerar_arquivo_ipasgo(
			dt_mesano_referencia_p	date,
			cd_convenio_p		number,
			nm_usuario_p		Varchar2,
			ie_tipo_protocolo_p 	protocolo_convenio.ie_tipo_protocolo%type,
			ie_tipo_atendimento_p	atendimento_paciente.ie_tipo_atendimento%type) is 


dt_mesano_referencia_w		date 		:= trunc(dt_mesano_referencia_p,'mm');
cd_estabelecimento_w		number(04,0)	:= 0;
cd_estab_atual_w		number(04,0) 	:= Wheb_usuario_pck.get_cd_estabelecimento;
qt_guia_w			number(15,0);
qt_linha_arq_w			number(10,0) := 0;
qt_linha_atend_w		number(10,0) := 0;
ie_so_estab_atual_w		varchar2(01)	:= 'N';
cd_cgc_w			estabelecimento.cd_cgc%type;
ie_regra_ipasgo_w		parametro_faturamento.ie_regra_ipasgo%type;
cd_prestador_convenio_w		convenio_prestador.cd_prestador_convenio%type;
vl_atendimento_w		w_ipasgo_dados_gerais_at.vl_atendimento%type;
vl_apresentado_w		w_ipasgo_dados_gerais_fat.vl_apresentado%type;
ds_list_estab_w			varchar2(255);
nr_seq_prot_vig_w		protocolo_convenio.nr_seq_protocolo%type;

cursor c00(     cd_estabelecimento_pp	number,
                ds_list_estab_pp        varchar2) is
	select	min(nr_sequencia) nr_seq_tipo_fatura,
		cd_tipo_fatura,
		cd_estabelecimento
	from	fatur_tipo_fatura
	where	ie_situacao = 'A'
	and	((cd_estabelecimento = cd_estabelecimento_pp) or
                (nvl(ds_list_estab_pp,'X') <> 'X' and obter_se_contido(nvl(cd_estabelecimento,ds_list_estab_pp), ds_list_estab_pp) = 'S') or 
                (cd_estabelecimento_pp = 0 AND nvl(ds_list_estab_pp,'X') = 'X'))
	group by	cd_tipo_fatura,
			cd_estabelecimento
	order by	2;

cursor c01(	dt_mesano_referencia_pp	date,
			cd_convenio_pp			number,
			nr_seq_tipo_fatura_pp	number,
			cd_estabelecimento_pp	number,
			ie_tipo_protocolo_pp	protocolo_convenio.ie_tipo_protocolo%type,
			ie_tipo_atendimento_pp	atendimento_paciente.ie_tipo_atendimento%type,
			cd_tipo_fatura_pp		number,
			ds_list_estab_pp		varchar2) is
select	a.nr_seq_protocolo,
	b.nr_interno_conta
from	atendimento_paciente c,
	conta_paciente b,
	protocolo_convenio a
where	a.dt_mesano_referencia 	between dt_mesano_referencia_pp and fim_mes(dt_mesano_referencia_pp)
and	a.cd_convenio		= cd_convenio_pp
and	c.nr_atendimento		= b.nr_atendimento
and	a.ie_status_protocolo	= 2
and	b.nr_seq_protocolo		= a.nr_seq_protocolo
and	b.nr_seq_tipo_fatura	= nr_seq_tipo_fatura_pp
and	a.cd_estabelecimento	= cd_estabelecimento_pp
and	cd_estabelecimento_pp	<> 0
and	((a.ie_tipo_protocolo = nvl(ie_tipo_protocolo_pp,0)) or (nvl(ie_tipo_protocolo_pp,0) = 0))
and	((c.ie_tipo_atendimento = nvl(ie_tipo_atendimento_pp,0)) or (nvl(ie_tipo_atendimento_pp,0) = 0))
and	ds_list_estab_pp is null
union all
select	a.nr_seq_protocolo,
	b.nr_interno_conta
from	fatur_tipo_fatura f,
	atendimento_paciente c,
	conta_paciente b,
	protocolo_convenio a
where	a.dt_mesano_referencia 	between dt_mesano_referencia_pp and fim_mes(dt_mesano_referencia_pp)
and	a.cd_convenio		= cd_convenio_pp
and	c.nr_atendimento		= b.nr_atendimento
and	a.ie_status_protocolo	= 2
and	b.nr_seq_protocolo		= a.nr_seq_protocolo
and	b.nr_seq_tipo_fatura	= f.nr_sequencia
and	f.cd_tipo_fatura		= cd_tipo_fatura_pp
and	cd_estabelecimento_pp 	= 0
and	((a.ie_tipo_protocolo = nvl(ie_tipo_protocolo_pp,0)) or (nvl(ie_tipo_protocolo_pp,0) = 0))
and	((c.ie_tipo_atendimento = nvl(ie_tipo_atendimento_pp,0)) or (nvl(ie_tipo_atendimento_pp,0) = 0))
and	ds_list_estab_pp is null
union all
select	a.nr_seq_protocolo,
	b.nr_interno_conta
from	fatur_tipo_fatura f,
	atendimento_paciente c,
	conta_paciente b,
	protocolo_convenio a
where	a.dt_mesano_referencia 	between dt_mesano_referencia_pp and fim_mes(dt_mesano_referencia_pp)
and	a.cd_convenio		= cd_convenio_pp
and	c.nr_atendimento		= b.nr_atendimento
and	a.ie_status_protocolo	= 2
and	b.nr_seq_protocolo		= a.nr_seq_protocolo
and	b.nr_seq_tipo_fatura	= f.nr_sequencia
and     f.nr_sequencia		= nr_seq_tipo_fatura_pp
and obter_se_contido(nvl(a.cd_estabelecimento,ds_list_estab_pp), ds_list_estab_pp) = 'S'
and	((a.ie_tipo_protocolo = nvl(ie_tipo_protocolo_pp,0)) or (nvl(ie_tipo_protocolo_pp,0) = 0))
and	((c.ie_tipo_atendimento = nvl(ie_tipo_atendimento_pp,0)) or (nvl(ie_tipo_atendimento_pp,0) = 0))
order by 1, 2;

c00_w      c00%rowtype;
c01_w      c01%rowtype;

begin

obter_param_usuario(999, 62, obter_perfil_Ativo, nm_usuario_p, cd_estab_atual_w, ie_so_estab_atual_w);
obter_param_usuario(999, 103, obter_perfil_Ativo, nm_usuario_p, cd_estab_atual_w, ds_list_estab_w);

delete from w_ipasgo_dados_gerais_pr 	where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_gerais_fat 	where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_gerais_at 	where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_guia_sec 	where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_trat 	where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_ato_trat 	where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_proc_trat 	where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_prof_trat 	where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_desp_trat 	where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_matmed_trat where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;
delete from w_ipasgo_dados_exame_trat where nm_usuario = nm_usuario_p and dt_mesano_referencia = dt_mesano_referencia_w;

if	(nvl(ie_so_estab_atual_w,'S') = 'S') then
	cd_estabelecimento_w := cd_estab_atual_w;
end if;

select	obter_cgc_estabelecimento(cd_estab_atual_w)
into	cd_cgc_w
from 	dual;

select	nvl(max(cd_prestador_convenio),'00000000')
into	cd_prestador_convenio_w
from	convenio_prestador
where	cd_convenio = cd_convenio_p
and	cd_cgc = cd_cgc_w;

select	nvl(max(ie_regra_ipasgo),'N')
into	ie_regra_ipasgo_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estab_atual_w; 	

gerar_w_ipasgo_dad_gerais_pr(dt_mesano_referencia_w, cd_convenio_p, nm_usuario_p, cd_estab_atual_w, 0, qt_linha_arq_w);	--Registro 0

for  	c00_w in c00(   cd_estabelecimento_w,
                        ds_list_estab_w) loop
	begin
	select	count(*)
	into	qt_guia_w
	from	fatur_tipo_fatura f,
		atendimento_paciente c,
		conta_paciente b,
		protocolo_convenio a
	where	a.dt_mesano_referencia 	between dt_mesano_referencia_w and fim_mes(dt_mesano_referencia_w)
	and	a.cd_convenio		= cd_convenio_p
	and	c.nr_atendimento		= b.nr_atendimento
	and	a.ie_status_protocolo	= 2
	and	b.nr_seq_protocolo		= a.nr_seq_protocolo
	and	b.nr_seq_tipo_fatura	= f.nr_sequencia
	and	f.cd_tipo_fatura		= c00_w.cd_tipo_fatura
	and	(a.cd_estabelecimento	= c00_w.cd_estabelecimento or c00_w.cd_estabelecimento = 0)
	and	((a.ie_tipo_protocolo = nvl(ie_tipo_protocolo_p,0)) or (nvl(ie_tipo_protocolo_p,0) = 0))
	and	((c.ie_tipo_atendimento = nvl(ie_tipo_atendimento_p,0)) or (nvl(ie_tipo_atendimento_p,0) = 0));
	
	if	(qt_guia_w > 0) then
		begin
		gerar_w_ipasgo_dad_gerais_fat(dt_mesano_referencia_w, cd_convenio_p, c00_w.nr_seq_tipo_fatura, c00_w.cd_estabelecimento, nm_usuario_p, qt_linha_arq_w);	--Registro 1

		for  	c01_w in c01(	dt_mesano_referencia_w,
								cd_convenio_p,
								c00_w.nr_seq_tipo_fatura,
								c00_w.cd_estabelecimento,
								ie_tipo_protocolo_p,
								ie_tipo_atendimento_p,
								c00_w.cd_tipo_fatura,
								ds_list_estab_w) loop
			begin
			
	/*Registro 2*/	gerar_w_ipasgo_dados_gerais_at(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);

			if	(c00_w.cd_tipo_fatura in (4,17)) then
	/*Registro 3*/		gerar_w_ipasgo_dados_guia_sec(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
			end if;
			if	(c00_w.cd_tipo_fatura in (3,4,5,6,8,9,17)) then
	/*Registro 4*/		gerar_w_ipasgo_dados_trat(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
			end if;
			if	(c00_w.cd_tipo_fatura in (3,4,5,6,8,9,17)) then
	/*Registro 5*/		gerar_w_ipasgo_dados_ato_tr(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
			end if;
			if	(c00_w.cd_tipo_fatura in (3,4,5,6,8,17)) then
				if (ie_regra_ipasgo_w = 'S') then
	/*Registro 6*/			gerar_ipasgo_dados_proc_trat_w(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
				else
	/*Registro 6*/			gerar_w_ipasgo_dados_proc_trat(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
				end if;
			end if;
			if	(c00_w.cd_tipo_fatura in (3,4,5,6,17)) then
				if (ie_regra_ipasgo_w = 'S') then
	/*Registro 7*/			gerar_ipasgo_dados_prof_trat_w(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
				else
	/*Registro 7*/			gerar_w_ipasgo_dados_prof_trat(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
				end if;
			end if;
			if	(c00_w.cd_tipo_fatura in (3,4,6,8,9,17)) then
				if (ie_regra_ipasgo_w = 'S') then
	/*Registro 8*/			gerar_ipasgo_dados_desp_trat_w(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
				else
	/*Registro 8*/			gerar_w_ipasgo_dados_desp_trat(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
				end if;
			end if;
			if	(c00_w.cd_tipo_fatura in (3,4,6,8,9,17)) then
	/*Registro 9*/		gerar_w_ipasgo_dados_matmed_tr(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w); -- Este tambem vai no pacote de objetos
			end if;
			if	(c00_w.cd_tipo_fatura in (4,9,17)) then
	/*Registro 10*/		gerar_w_ipasgo_dados_exame_tr(c01_w.nr_interno_conta, dt_mesano_referencia_w, nm_usuario_p, c00_w.nr_seq_tipo_fatura, qt_linha_arq_w, qt_linha_atend_w);
			end if;
			
			if	(c00_w.cd_tipo_fatura <> 2) then
				begin
			
				select  sum(x.vl_item)
				into	vl_atendimento_w
				from (	select  sum(a.vl_servico) vl_item
					from    w_ipasgo_dados_desp_trat a
					where	a.nr_interno_conta = c01_w.nr_interno_conta
					and	a.nr_matricula_prestador = cd_prestador_convenio_w
					and	a.nm_usuario = nm_usuario_p
					and	a.dt_mesano_referencia = dt_mesano_referencia_w
					union
					select  sum(b.vl_honorario)
					from    w_ipasgo_dados_prof_trat b
					where	b.nr_interno_conta = c01_w.nr_interno_conta
					and	b.nr_matricula_prestador = cd_prestador_convenio_w 
					and	b.nm_usuario = nm_usuario_p
					and	b.dt_mesano_referencia = dt_mesano_referencia_w
					union 
					select  sum(c.vl_matmed)
					from    w_ipasgo_dados_matmed_trat c
					where	c.nr_interno_conta = c01_w.nr_interno_conta
					and	c.nr_matricula_prestador = cd_prestador_convenio_w 
					and	c.nm_usuario = nm_usuario_p
					and	c.dt_mesano_referencia = dt_mesano_referencia_w
					union 
					select  sum(d.vl_exame)
					from	w_ipasgo_dados_exame_trat d
					where	d.nr_interno_conta = c01_w.nr_interno_conta
					and	d.nr_matricula_prestador = cd_prestador_convenio_w
					and	d.nm_usuario = nm_usuario_p
					and	d.dt_mesano_referencia = dt_mesano_referencia_w) x;
					
				update	w_ipasgo_dados_gerais_at
				set	vl_atendimento 	= vl_atendimento_w,
					vl_atendimento_gravado = replace(replace(Campo_Mascara_virgula_casas(vl_atendimento_w,4),'.',''),',','.'),
					ds_linha = 	nr_linha || '|' ||
							'2' || '|' || 
							nr_linha_atend || '|' ||	
							cd_tipo_fatura || '|' || 
							nr_guia || '|' || 
							to_char(dt_atendimento,'YYYY-MM-DD') || '|' ||
							replace(replace(Campo_Mascara_virgula_casas(vl_atendimento_w,4),'.',''),',','.') || '|' ||
							cd_cid_principal || '|' || 
							cd_cid_2 || '|' ||
							'||||'
				where	nr_interno_conta = c01_w.nr_interno_conta
				and	nm_usuario = nm_usuario_p 
				and	dt_mesano_referencia = dt_mesano_referencia_w;
				
				if (nr_seq_prot_vig_w	<>	c01_w.nr_seq_protocolo) then
					update	protocolo_convenio	
					set	dt_envio = sysdate,
						nm_usuario_envio = nm_usuario_p,
						dt_atualizacao = sysdate,
						nm_usuario = nm_usuario_p
					where	nr_seq_protocolo = c01_w.nr_seq_protocolo;
					
					nr_seq_prot_vig_w	:=	c01_w.nr_seq_protocolo;
				end if;
				
				end;
			end if;
			
			end;
		end loop;
		
                if(nvl(ds_list_estab_w,'X') = 'X') then
                        select	sum(vl_atendimento)
                        into	vl_apresentado_w
                        from	w_ipasgo_dados_gerais_at
                        where	nm_usuario = nm_usuario_p 
                        and	dt_mesano_referencia = dt_mesano_referencia_w
                        and	nr_seq_tipo_fatura = c00_w.nr_seq_tipo_fatura;
                        
                        update	w_ipasgo_dados_gerais_fat 
                        set	vl_apresentado	= vl_apresentado_w,
                                ds_linha =	nr_linha || '|' ||
                                                '1' || '|' ||
                                                tp_fatura || '|' || 
                                                qt_guia || '|' || 
                                                replace(replace(Campo_Mascara_virgula_casas(vl_apresentado_w,4),'.',''),',','.') || '|||||||||'
                        where	nm_usuario = nm_usuario_p 
                        and	dt_mesano_referencia = dt_mesano_referencia_w
                        and	nr_seq_tipo_fatura = c00_w.nr_seq_tipo_fatura;
                else
                        select	sum(vl_atendimento)
                        into	vl_apresentado_w
                        from	w_ipasgo_dados_gerais_at
                        where	nm_usuario = nm_usuario_p 
                        and	dt_mesano_referencia = dt_mesano_referencia_w
                        and	cd_tipo_fatura = c00_w.cd_tipo_fatura;
                        
                        update	w_ipasgo_dados_gerais_fat 
                        set	vl_apresentado	= vl_apresentado_w,
                                ds_linha =	nr_linha || '|' ||
                                                '1' || '|' ||
                                                tp_fatura || '|' || 
                                                qt_guia || '|' || 
                                                replace(replace(Campo_Mascara_virgula_casas(vl_apresentado_w,4),'.',''),',','.') || '|||||||||'
                        where	nm_usuario = nm_usuario_p 
                        and	dt_mesano_referencia = dt_mesano_referencia_w
                        and	nr_seq_tipo_fatura in ( select   nr_sequencia
                                                        from    fatur_tipo_fatura
                                                        where   cd_tipo_fatura = c00_w.cd_tipo_fatura
                                                        and     ie_situacao = 'A'
                                                        and     obter_se_contido(nvl(cd_estabelecimento,ds_list_estab_w), ds_list_estab_w) = 'S');
                end if;
		end;
	end if;
	end;
end loop;

  -- OS 645845 - RF 03 - Adicionar a chamada para a rotina de consist?ncia
  consistir_arquivo_ipasgo(     dt_mesano_referencia_p,
                                cd_convenio_p,
                                ie_tipo_protocolo_p,
                                ie_tipo_atendimento_p,
                                cd_estab_atual_w,
                                nm_usuario_p );

commit;

end gerar_arquivo_ipasgo;
/