CREATE OR REPLACE
PROCEDURE CALCULO_VALOR_PONTO_SUS
                         	(NR_SEQ_PROTOCOLO_P	NUMBER) IS

/* A parte comentada foi tirada em 13/04/2000 por Elemar a pedido do Marcus,
   a chamada da procedure "Atualiza_Valor_Ponto_Sp" passa a partir deste dia
   a ser chamada na procedure "Recalcular_Conta_Paciente".

nr_atendimento_w			number(10)		:= 0;
nr_interno_conta_w		number(10)		:= 0;

CURSOR C01 IS
	select 	a.nr_atendimento,
			a.nr_interno_conta
	from		conta_paciente a
	where		a.nr_seq_protocolo 	= nr_seq_protocolo_p;

OPEN C01;
LOOP
FETCH C01 into
		nr_atendimento_w,
		nr_interno_conta_w;
	exit when c01%notfound;
	BEGIN
	ATUALIZAR_VALOR_PONTO_SP(nr_atendimento_w,nr_interno_conta_w);
	END;
END LOOP;
CLOSE C01;
*/

BEGIN

ATUALIZAR_VALOR_PONTO_SADT(NR_SEQ_PROTOCOLO_P);
VALIDA_DIARIA_UTI_SUS(NR_SEQ_PROTOCOLO_P,'Tasy');
/*Gerar_controle_proc_aih(NR_SEQ_PROTOCOLO_P,'Tasy'); Felipe - Comentei pois o processo do SUS tambem foi cancelado, 
o mesmo n�o disponibiliza mais os arquivos .TXT no site*/ 

END CALCULO_VALOR_PONTO_SUS;
/
