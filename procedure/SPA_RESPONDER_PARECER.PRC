create or replace
Procedure SPA_RESPONDER_PARECER( NR_SEQ_SPA_P	        NUMBER,
				 DS_RESPONDER_PARECER_P	VARCHAR2,
				 nm_usuario_p		varchar2,
				 nr_seq_parecer_p	 number default null) IS
			
nr_sequencia_w		number(10);
ie_status_w		spa.ie_status%type;

begin	

select	max(nr_sequencia)
into	nr_sequencia_w
from	SPA_PARECER
where	DS_RESPOSTA_PARECER is null
and	nr_seq_spa	    = NR_SEQ_SPA_P
and 	nr_sequencia 	    = nvl(nr_seq_parecer_p,nr_sequencia);

update	SPA_PARECER
set	DT_RESP_PARECER		= sysdate,
	DS_RESPOSTA_PARECER	= substr(DS_RESPONDER_PARECER_P,1,254),
	DT_ATUALIZACAO		= sysdate
where	NR_SEQ_SPA = NR_SEQ_SPA_P
and	nr_sequencia = nr_sequencia_w;

select 	ie_status
into 	ie_status_w
from 	spa
where 	nr_sequencia = nr_seq_spa_p;

spa_gerar_historico(nr_seq_spa_p,nm_usuario_p,ie_status_w,ie_status_w,6,substr(ds_responder_parecer_p,1,255),'N');
	
commit;

end SPA_RESPONDER_PARECER;
/
