create or replace procedure liberar_relatorio_dinamico(nm_usuario_p varchar2,
							nr_seq_relatorio_p number,
							tipo_componente_p varchar2 default null,
							nr_seq_documento_p number default null) is

nr_seq_relatorio_w	number(10);
cd_relatorio_w		number(10);
cd_funcao_w number(6);
ds_funcao_w varchar(80);
qt_registros_w  number(6);

begin

if ('WDBPANEL' = tipo_componente_p) then
    
	select cd_funcao
	into cd_funcao_w
	from objeto_schematic
	where nr_sequencia = nr_seq_documento_p;
else
	select cd_funcao
	into cd_funcao_w
	from objeto_schematic o
	where o.nr_sequencia = nr_seq_documento_p;
    
end if;

select max(ds_funcao)
into ds_funcao_w
from funcao
where cd_funcao = cd_funcao_w;

select max(cd_funcao)
into qt_registros_w
from relatorio_funcao
where cd_funcao = cd_funcao_w
and nr_seq_relatorio = nr_seq_relatorio_p;

if (qt_registros_w < 1) then

	insert into relatorio_funcao(
	cd_funcao,  nr_seq_relatorio,  dt_atualizacao,  nm_usuario,
	ie_exige_perfil,  qt_copia,  ie_logo,  ie_imprimir,  ie_configurar)
	values(
	cd_funcao_w,  nr_seq_relatorio_p,  sysdate,  nm_usuario_p,
	'S',  '1',  'N',  'S',  'S');

end if;

select max(nm_usuario)
into qt_registros_w
from usuario_relatorio
where nm_usuario = nm_usuario_p
and nr_seq_relatorio = nr_seq_relatorio_p;

if (qt_registros_w < 1) then

	insert	into usuario_relatorio (nm_usuario_ref, nr_seq_relatorio, dt_atualizacao, nm_usuario, qt_copia, ie_logo, ie_configurar, ie_imprimir, ds_impressora)
	values (nm_usuario_p, nr_seq_relatorio_p, sysdate, sysdate, '1', 'N', 'S', 'S', null);

end if;

select max(nr_sequencia)
into qt_registros_w
from relatorio_perfil
where nm_usuario = nm_usuario_p
and nr_seq_relatorio = nr_seq_relatorio_p;

if (qt_registros_w < 1) then

	insert	into relatorio_perfil (nr_sequencia, cd_perfil, nr_seq_relatorio, dt_atualizacao, nm_usuario, qt_copia, ie_logo, ie_configurar, ie_imprimir, ie_outputbin, ds_impressora,     
	nr_seq_apresent,ie_gravar_log, ie_visualizar, ie_apresenta,ie_forma_impressao)
	values (relatorio_perfil_seq.nextval, Obter_perfil_Ativo, nr_seq_relatorio_p, sysdate, nm_usuario_p, '1', 'S', 'S', null, null, null,null,'N', 'S', null,null);

end if;

end liberar_relatorio_dinamico;
/
