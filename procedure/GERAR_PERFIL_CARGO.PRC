create or replace
procedure gerar_perfil_cargo(
			cd_cargo_p		number,
			cd_cargo_destino_p	number,
			nm_usuario_p		varchar2) is 
		
begin
if (cd_cargo_p > 0) and
   (cd_cargo_destino_p > 0) then
	begin
	insert into cargo_perfil(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_cargo,
		cd_perfil)
	select 	cargo_perfil_seq.nextval,
		sysdate,
		nm_usuario_p,
		cd_cargo_destino_p,
		a.cd_perfil
	from	cargo_perfil a
	where	a.cd_cargo = cd_cargo_p
	and	not exists (	select  1
				from	cargo_perfil x
				where 	x.cd_cargo = cd_cargo_destino_p
				and	x.cd_perfil = a.cd_perfil);
	
	
	end;
end if;	
commit;

end gerar_perfil_cargo;
/