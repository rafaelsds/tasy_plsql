create or replace
procedure man_inativar_cadastro_build(
		nr_info_build_p	number,
		nm_usuario_p	varchar2) is 

begin

if	(nr_info_build_p is not null)	then
	
	update	MAN_OS_CTRL_DESC
	set	DT_INATIVACAO = sysdate,
		nm_usuario_inativ = nm_usuario_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_info_build_p;
	
	commit;

end	if;

end	man_inativar_cadastro_build;
/