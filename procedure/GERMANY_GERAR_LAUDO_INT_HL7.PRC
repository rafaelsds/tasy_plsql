create or replace procedure germany_gerar_laudo_int_hl7
			(	nr_seg_reg_p		number,
				nm_usuario_p		varchar2,
				nr_seq_laudo_p	out	number,
                nr_controle_ext_p   varchar2 default null) is 
				
nr_prescricao_w			number(14);
nr_seq_prescricao_w		number(10);
nr_seq_propaci_w		number(10);
nr_seq_proc_interno_w		number(15);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
qt_procedimento_w		number(15);
cd_setor_atendimento_w		number(10);
cd_medico_exec_w		varchar2(10);
nr_seq_exame_w			number(15);
ie_lado_w			varchar2(15);
dt_prev_execucao_w		date;
nr_atendimento_w		number(10);
dt_entrada_unidade_w		date;
nr_laudo_w        		number(10);
cd_medico_resp_w		varchar2(10);
qt_existe_medico_w		number(10);
ds_laudo_w			long;
ds_laudo_copia_w		long;
nr_seq_laudo_w			number(10);
nr_seq_laudo_ant_w		number(10);
nr_seq_laudo_atual_w		number(10);
nr_seq_copia_w			number(10);

cd_laudo_externo_w		number(10);
cd_medico_laudante_w		varchar2(10);
ds_titulo_laudo_w		varchar2(255);
dt_liberacao_laudo_w		date;
ie_existe_laudo_w		varchar2(1);
ie_tipo_ordem_w			varchar2(5);
nm_usuario_w			varchar2(30);
nr_acesso_dicom_w		varchar2(30);
nr_seq_prescr_w			number(10);
nr_crm_w			varchar2(20);
uf_crm_w			w_integracao_laudo_hl7.UF_CRM%TYPE;
nm_medico_solicitante_w		varchar2(200);
ie_insere_medico_solic_w	varchar2(1);
dt_procedimento_w		date;
ie_status_execucao_w		varchar2(3);
cd_estabelecimento_w		number(4);
ie_alterar_medico_conta_w 	varchar2(2);
ie_alterar_medico_exec_conta_w	varchar2(2);
ie_cancelar_laudo_w   		varchar2(2);
nr_sequencia_ww			varchar2(10);
nr_seq_laudo_imagem_w   laudo_paciente_imagem.nr_sequencia%type;

begin
nr_seq_laudo_p	:= 0;

select 	max(vl_parametro)
into	ie_insere_medico_solic_w
from 	funcao_parametro
where  	cd_funcao	= 28
and 	nr_sequencia	= 78;

begin	
select	nr_acc_number,
	cd_medico_laudante,
	ie_tipo_ordem,
	cd_laudo_externo,
	dt_liberacao_laudo,
	nr_crm,
	uf_crm
into	nr_acesso_dicom_w,
	cd_medico_laudante_w,
	ie_tipo_ordem_w,
	cd_laudo_externo_w,
	dt_liberacao_laudo_w,
	nr_crm_w,
	uf_crm_w
from	w_integracao_laudo_hl7
where	nr_sequencia	= nr_seg_reg_p;
exception

when no_data_found then
	Wheb_mensagem_pck.exibir_mensagem_abort(192823,	'NR_SEG_REG_P='|| nr_seg_reg_p);
end;

if	(nr_acesso_dicom_w is not null) then
	begin
	select	nr_prescricao,
		nr_sequencia,
		nr_seq_proc_interno,
		cd_procedimento,
		ie_origem_proced,
		qt_procedimento,
		cd_setor_atendimento,
		cd_medico_exec,
		nr_seq_exame,
		nvl(ie_lado,'A'),
		dt_prev_execucao
	into	nr_prescricao_w,
		nr_seq_prescricao_w,
		nr_seq_proc_interno_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		qt_procedimento_w,
		cd_setor_atendimento_w,
		cd_medico_exec_w,
		nr_seq_exame_w,
		ie_lado_w,
		dt_prev_execucao_w
	from	prescr_procedimento
	where	nr_acesso_dicom	= nr_acesso_dicom_w
	and	dt_atualizacao_nrec > (sysdate - 60)
	and rownum = 1
	order by nr_prescricao asc;
	exception
	when no_data_found then
		begin
		
		select	nr_prescricao,
			nr_sequencia,
			nr_seq_proc_interno,
			cd_procedimento,
			ie_origem_proced,
			qt_procedimento,
			cd_setor_atendimento,
			cd_medico_exec,
			nr_seq_exame,
			nvl(ie_lado,'A'),
			dt_prev_execucao
		into	nr_prescricao_w,
			nr_seq_prescricao_w,
			nr_seq_proc_interno_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			qt_procedimento_w,
			cd_setor_atendimento_w,
			cd_medico_exec_w,
			nr_seq_exame_w,
			ie_lado_w,
			dt_prev_execucao_w
		from	prescr_procedimento
		where	nr_acesso_dicom	= nr_acesso_dicom_w;		
		exception
		when no_data_found then
			Wheb_mensagem_pck.exibir_mensagem_abort(192827,'NR_ACESSO_DICOM_P='|| nr_acesso_dicom_w);
		end;
	end;
end if;

if	(nvl(nr_prescricao_w,0) > 0) and
	(nvl(nr_seq_prescricao_w,0) > 0) then
	begin
	select	nvl(max(nr_sequencia),0),
		max(nr_atendimento),
		max(dt_entrada_unidade),
		max(dt_procedimento)
	into	nr_seq_propaci_w,
		nr_atendimento_w,
		dt_entrada_unidade_w,
		dt_procedimento_w
	from	procedimento_paciente
	where	nr_prescricao		= nr_prescricao_w
	and	nr_sequencia_prescricao	= nr_seq_prescricao_w
	and	nr_seq_proc_princ is null;
	
	if	(nr_seq_propaci_w = 0) then
		begin
		gerar_proc_pac_item_prescr(	nr_prescricao_w, 
						nr_seq_prescricao_w, 
						null, 
						null,
						nr_seq_proc_interno_w,
						cd_procedimento_w, 
						ie_origem_proced_w,
						qt_procedimento_w, 
						cd_setor_atendimento_w,
						9, 
						dt_prev_execucao_w,
						nm_usuario_p, 
						cd_medico_exec_w, 
						null,
						ie_lado_w, 
						null);
						
		select	max(nr_sequencia),
			max(nr_atendimento),
			max(dt_entrada_unidade),
			max(dt_procedimento)
		into	nr_seq_propaci_w,
			nr_atendimento_w,
			dt_entrada_unidade_w,
			dt_procedimento_w
		from	procedimento_paciente
		where	nr_prescricao		= nr_prescricao_w
		and	nr_sequencia_prescricao	= nr_seq_prescricao_w
		and	nr_seq_proc_princ is null;
		end;
	end if;
	
	select	nvl(max(nr_laudo),0) + 1
	into	nr_laudo_w
	from	laudo_paciente
	where	nr_atendimento	= nr_atendimento_w;

	select	count(*)
	into	qt_existe_medico_w
	from	medico
	where	cd_pessoa_fisica	= cd_medico_laudante_w;
	
	if 	(nvl(ie_insere_medico_solic_w,'N') = 'S') then
		select 	max(substr(obter_nome_pf(cd_medico),1,60))
		into 	nm_medico_solicitante_w
		from 	prescr_medica
		where 	nr_prescricao	= nr_prescricao_w;
	else
		nm_medico_solicitante_w	:= '';	
	end if;
	
	if	(qt_existe_medico_w = 0) then
		SELECT	NVL(MAX(cd_pessoa_fisica),'0')
		INTO	cd_medico_resp_w
		FROM	medico
		WHERE	nr_rqe = cd_medico_laudante_w;
		/*temporariamente utilizado o campo nr_rqe pois o terceiro enviado o nome do usu�rio ao inv�s do c�digo*/
	else
		cd_medico_resp_w	:= cd_medico_laudante_w;
	end if;
	
	if	(nvl(cd_medico_resp_w,'0') = '0') then
		gravar_log_cdi(	88877,
				'N�o encontrou o m�dico',
				nm_usuario_p);
				
		Wheb_mensagem_pck.exibir_mensagem_abort(192829, 'CD_MEDICO_LAUDANTE='|| cd_medico_laudante_w ||';NR_CRM='|| nr_crm_w ||';UF_CRM='|| uf_crm_w);
	else
		
		select 	nvl(max(nr_sequencia),0)
		into	nr_seq_laudo_ant_w
		from	laudo_paciente
		where	cd_laudo_externo	= cd_laudo_externo_w
		and	nr_seq_proc <> nr_seq_propaci_w;
	
		if	(nr_seq_laudo_ant_w > 0) then
			update	procedimento_paciente
			set	nr_laudo 	= nr_seq_laudo_ant_w
			where	nr_sequencia	= nr_seq_propaci_w;
		else
			select	max(cd_estabelecimento)
			into	cd_estabelecimento_w
			from	prescr_medica
			where	nr_prescricao	= nr_prescricao_w;

			select	nvl(max(ie_cancelar_laudo),'N')
			into	ie_cancelar_laudo_w
			from	parametro_integracao_pacs
			where	cd_estabelecimento	= cd_estabelecimento_w;	

			if	(ie_cancelar_laudo_w = 'S') then
				begin
				select	max(nr_sequencia)
				into	nr_sequencia_ww
				from	laudo_paciente
				where	nr_prescricao = nr_prescricao_w
				and	nr_seq_prescricao = nr_seq_prescricao_w;

				if	(nvl(nr_sequencia_ww,0)<>0) then
					begin
					cancelar_laudo_paciente(nr_sequencia_ww,
								'C',
								nm_usuario_p,
								'');
					end;
				end if;
			 end;
			end if;
		
			select	laudo_paciente_seq.nextval
			into	nr_seq_laudo_w
			from	dual;
			
			select 	nvl(max(nr_sequencia),0)
			into	nr_seq_laudo_atual_w
			from	laudo_paciente
			where	cd_laudo_externo	= cd_laudo_externo_w
			and	nr_seq_proc		= nr_seq_propaci_w;			
			
			select	substr(obter_desc_prescr_proc_laudo(p.cd_procedimento, 
														p.ie_origem_proced, 
														p.nr_seq_proc_interno, 
														p.ie_lado, 
														nr_seq_propaci_w),1,255)
			into	ds_titulo_laudo_w
			from	prescr_procedimento p
			where	nr_prescricao	= nr_prescricao_w
			and 	nr_sequencia	= nr_seq_prescricao_w;
		
			insert into laudo_paciente
				(	nr_sequencia,
					nr_atendimento,
					dt_entrada_unidade,
					nr_laudo,
					nm_usuario,
					dt_atualizacao,
					cd_medico_resp,
					ds_titulo_laudo,
					dt_laudo,
					nr_prescricao,
					nr_seq_proc,
					nr_seq_prescricao,
					dt_liberacao,
					qt_imagem,
					nm_medico_solicitante,
					ie_status_laudo,
					cd_laudo_externo,
					dt_exame,
					dt_aprovacao)
			values	(	nr_seq_laudo_w,
					nr_atendimento_w,
					dt_entrada_unidade_w,
					nr_laudo_w,
					nm_usuario_p,
					sysdate,
					cd_medico_resp_w,
					ds_titulo_laudo_w,
					dt_liberacao_laudo_w,
					nr_prescricao_w,
					nr_seq_propaci_w,
					nr_seq_prescricao_w,
					sysdate,--dt_liberacao_laudo_w,
					0,
					nm_medico_solicitante_w,
					'LL',
					cd_laudo_externo_w,
					dt_procedimento_w,
					dt_liberacao_laudo_w);
				
			nr_seq_laudo_p	:= nr_seq_laudo_w;

			select	max(cd_estabelecimento)
			into	cd_estabelecimento_w
			from	prescr_medica
			where	nr_prescricao	= nr_prescricao_w;

			ie_alterar_medico_conta_w 	:= Obter_Valor_Param_Usuario(28 ,101 , obter_perfil_ativo ,null,cd_estabelecimento_w); 
			ie_alterar_medico_exec_conta_w 	:= Obter_Valor_Param_Usuario(28 ,112 , obter_perfil_ativo ,null,cd_estabelecimento_w); 

			if	(ie_alterar_medico_conta_w = 'S') then
				Atualizar_Propaci_Medico_Laudo(	nr_seq_laudo_w,
								'EX',
								nm_usuario_p);
			end if;
			
			if	(ie_alterar_medico_exec_conta_w = 'S') then
				Atualizar_Propaci_Medico_Laudo(	nr_seq_laudo_w,
								'EXC',
								nm_usuario_p);
			end if;
	
			update	procedimento_paciente
			set	nr_laudo	= nr_seq_laudo_w
			where	nr_sequencia	= nr_seq_propaci_w;
			
			if	(nr_seq_laudo_atual_w > 0) then
				gerar_copia_laudo_padrao(	nr_seq_laudo_atual_w,
								nr_seq_laudo_w,
								null,
								nm_usuario_p,
								'Excluido na integra��o');
				
				update	laudo_paciente_copia
				set	nr_seq_laudo	= nr_seq_laudo_w
				where	nr_seq_laudo	= nr_seq_laudo_atual_w;

				update	laudo_paciente_medico
				set	nr_seq_laudo	= nr_seq_laudo_w
				where	nr_seq_laudo	= nr_seq_laudo_atual_w; 
				
				delete	laudo_paciente_pdf
				where	nr_seq_laudo	= nr_seq_laudo_atual_w;
				
				delete 	laudo_paciente
				where	nr_sequencia	= nr_seq_laudo_atual_w;
			end if;
		
			/* *****  Atualiza status execu��o na prescri��o ***** */
			update	prescr_procedimento a
			set	a.ie_status_execucao	= '40',
				a.nm_usuario		= nm_usuario_p
			where	a.nr_prescricao		= nr_prescricao_w 
			and	a.nr_acesso_dicom  in (nr_acesso_dicom_w)
			and	exists	(select 1
					from	procedimento_paciente	b
					where	b.nr_prescricao = a.nr_prescricao
					and	a.nr_sequencia	= b.nr_sequencia_prescricao);
		
			select	max(ie_status_execucao)
			into	ie_status_execucao_w
			from	prescr_procedimento a
			where	a.nr_prescricao	= nr_prescricao_w
			and	a.nr_sequencia	= nr_seq_prescricao_w;
			
			if 	(ie_status_execucao_w <> '40') then			
				update prescr_procedimento a
				set a.ie_status_execucao = '40', 
					a.nm_usuario  = nm_usuario_p
				where	a.nr_prescricao = nr_prescricao_w
				and	a.nr_acesso_dicom  in (nr_acesso_dicom_w)
				and	exists	(select	1
						from	procedimento_paciente b
						where	b.nr_prescricao = a.nr_prescricao
						and	b.nr_laudo = nr_seq_prescricao_w);
			end if;
		end if;
	end if;
	end;
else
	gravar_log_cdi(	88877,
			'N�o encontrou prescr_procedimento',
			nm_usuario_p);
			
	Wheb_mensagem_pck.exibir_mensagem_abort(192838);
end if;

select  max(nr_sequencia)
into    nr_seq_laudo_imagem_w
from    laudo_paciente_imagem
where   nr_controle_ext = nr_controle_ext_p;

if(nr_seq_laudo_imagem_w is not null) then 

    update  laudo_paciente_imagem
    set     nr_sequencia = nr_seq_laudo_w
    where   nr_sequencia = nr_seq_laudo_imagem_w;
    
    delete laudo_paciente
    where   nr_sequencia = nr_seq_laudo_imagem_w;
    
    commit;
end if;

END germany_gerar_laudo_int_hl7;
/