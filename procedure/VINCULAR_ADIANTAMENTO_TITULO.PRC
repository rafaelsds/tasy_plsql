create or replace
procedure vincular_adiantamento_titulo(
			nr_atendimento_p	number,
			nr_adiantamento_p	number) is 

begin

if(nr_adiantamento_p is not null) then
update  adiantamento
set     nr_atendimento    = nr_atendimento_p
where   nr_adiantamento   = nr_adiantamento_p;
end if;

commit;

end vincular_adiantamento_titulo;
/