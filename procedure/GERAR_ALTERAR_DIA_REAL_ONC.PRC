create or replace
procedure Gerar_alterar_dia_real_onc(		nr_seq_atendimento_p	number,
						nm_usuario_p		Varchar2,
						ie_commit_p		Varchar2 default 'S') is 

nr_ciclo_w			number(10);
nr_seq_paciente_w		number(10);
prim_dt_prevista_ciclo_w	Date;
dt_prevista_w			Date;
ds_dia_w			varchar2(10);
qt_dias_w			number(10);
nr_seq_atendimento_w		number(10);

cursor c01 is
	select	trunc(dt_prevista),
		nr_seq_atendimento
	from	paciente_atendimento
	where	nr_seq_paciente = nr_seq_paciente_w
	and	nr_ciclo = nr_ciclo_w;
	
c01_w	c01%rowtype;		

begin

if	(nr_seq_atendimento_p is not null) then
	select	nr_ciclo,
		nr_seq_paciente
	into	nr_ciclo_w,
		nr_seq_paciente_w
	from	paciente_atendimento
	where	nr_seq_atendimento = nr_seq_atendimento_p;

	select	min(trunc(dt_prevista))
	into	prim_dt_prevista_ciclo_w
	from	paciente_atendimento
	where	nr_seq_paciente = nr_seq_paciente_w
	and	nr_ciclo = nr_ciclo_w;

	open C01;
	loop
	fetch C01 into	
		dt_prevista_w,
		nr_seq_atendimento_w;
	exit when C01%notfound;
		begin
		if	(dt_prevista_w = prim_dt_prevista_ciclo_w) then
			ds_dia_w	:= 'D1';
		else
			ds_dia_w	:= 'D' || to_char(trunc( dt_prevista_w -  prim_dt_prevista_ciclo_w )+1);
		end if;
		
		update	paciente_atendimento
		set	ds_dia_ciclo_real = ds_dia_w
		where	nr_seq_atendimento = nr_seq_atendimento_w;
		end;
	end loop;
	close C01;
	
	if	(nvl(ie_commit_p,'S') = 'S') then
		commit;
	end if;
end if;

end Gerar_alterar_dia_real_onc;
/