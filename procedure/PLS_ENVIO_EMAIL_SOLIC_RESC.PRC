create or replace
procedure pls_envio_email_solic_resc
			(	nr_seq_solicitacao_p		number,
				ie_acao_p			varchar2,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar,
				ie_commit_p			varchar2 default 'S') is

ds_assunto_w			pls_regra_solic_resc_email.ds_assunto%type;
ds_conteudo_w			pls_regra_solic_resc_email.ds_email%type;
nr_seq_email_w			pls_email.nr_sequencia%type;
cd_prioridade_w			pls_email_parametros.cd_prioridade%type;
nr_seq_anexo_w			pls_email_anexo.nr_sequencia%type;
ds_valor_paramentro_w		pls_email_anexo_param.ds_valor_parametro%type;
ds_remetente_w			varchar2(255);
nr_protocolo_atendimento_w	pls_protocolo_atendimento.nr_protocolo%type;
ds_beneficiarios_w  varchar2(4000);
qt_benef_w  number(10);

Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_tipo_contato,
		a.cd_pessoa_fisica,
		b.dt_solicitacao,
		b.nr_seq_protocolo_atend,
		b.nr_sequencia nr_seq_solic_rescisao,
		a.ds_email,
		b.nr_seq_motivo_rejeicao,
		b.nr_seq_atendimento,
		b.dt_rescisao
	from	pls_solic_resc_contato a,
		pls_solicitacao_rescisao b
	where	a.nr_seq_solicitacao = b.nr_sequencia 
	and	a.nr_seq_solicitacao = nr_seq_solicitacao_p
	and	a.ie_forma_envio = '1' --E-mail
	and	a.ds_email is not null;

Cursor C02 (	nr_seq_tipo_contato_pc pls_solic_resc_contato.nr_seq_tipo_contato%type,
		nr_seq_motivo_rejeicao_pc pls_solicitacao_rescisao.nr_seq_motivo_rejeicao%type) is
	select	a.nr_sequencia,
		a.ds_assunto,
		a.ds_email
	from	pls_regra_solic_resc_email a
	where	(a.ie_tipo_email is null or a.ie_tipo_email = ie_acao_p)
	and	(a.nr_seq_motivo_rejeicao is null or a.nr_seq_motivo_rejeicao = nr_seq_motivo_rejeicao_pc)
	and	(a.nr_seq_tipo_contato is null or a.nr_seq_tipo_contato = nr_seq_tipo_contato_pc)
	and	(a.nr_seq_motivo_rescisao is null or exists (	select	1
								from	pls_solic_rescisao_benef x
								where	x.nr_seq_solicitacao = nr_seq_solicitacao_p
								and	x.nr_seq_motivo_rescisao = a.nr_seq_motivo_rescisao))
	and	((nvl(a.ie_iniciativa_beneficiario,'A') = 'A') or
		 (nvl(a.ie_iniciativa_beneficiario,'A') = 'S' and exists(select	1
									from	pls_solic_rescisao_benef x,
										pls_motivo_cancelamento y
									where	y.nr_sequencia = x.nr_seq_motivo_rescisao
									and	nvl(y.ie_iniciativa_beneficiario,'N') = 'S'
									and	x.nr_seq_solicitacao = nr_seq_solicitacao_p)) or
		 (nvl(a.ie_iniciativa_beneficiario,'A') = 'N' and exists(select	1
									from	pls_solic_rescisao_benef x,
										pls_motivo_cancelamento y
									where	y.nr_sequencia = x.nr_seq_motivo_rescisao
									and	nvl(y.ie_iniciativa_beneficiario,'N') = 'N'
									and	x.nr_seq_solicitacao = nr_seq_solicitacao_p)))
	and	((nvl(a.ie_direito_portabilidade,'A') = 'A') or
		 (nvl(a.ie_direito_portabilidade,'A') = 'S' and exists(	select	1
									from	pls_solic_rescisao_benef x,
										pls_motivo_cancelamento y
									where	y.nr_sequencia = x.nr_seq_motivo_rescisao
									and	nvl(y.ie_direito_portabilidade,'N') = 'S'
									and	x.nr_seq_solicitacao = nr_seq_solicitacao_p)) or
		 (nvl(a.ie_direito_portabilidade,'A') = 'N' and exists(	select	1
									from	pls_solic_rescisao_benef x,
										pls_motivo_cancelamento y
									where	y.nr_sequencia = x.nr_seq_motivo_rescisao
									and	nvl(y.ie_direito_portabilidade,'N') = 'N'
									and	x.nr_seq_solicitacao = nr_seq_solicitacao_p)));

Cursor C03	(nr_seq_regra_pc pls_regra_solic_resc_email.nr_sequencia%type)is
	select	cd_classif_relat,
		cd_relatorio,
		ds_arquivo
	from	pls_solic_resc_email_anexo
	where	nr_seq_regra = nr_seq_regra_pc;

Cursor C04 (	cd_classif_relat_pc relatorio.cd_classif_relat%type,
		cd_relatorio_pc relatorio.cd_relatorio%type)is
	select	b.cd_parametro,
		b.ie_tipo_atributo 
	from	relatorio a,
		relatorio_parametro b
	where	a.nr_sequencia	= b.nr_seq_relatorio
	and	a.cd_classif_relat = cd_classif_relat_pc
	and	a.cd_relatorio	= cd_relatorio_pc;

Cursor C05 ( nr_seq_solicitacao_pc pls_solicitacao_rescisao.nr_sequencia%type ) is
	select  substr(pls_obter_dados_segurado(nr_seq_segurado, 'N'),1, 255) nm_segurado,
					rownum nr_linha
	from    PLS_SOLIC_RESCISAO_BENEF 
	where 	nr_seq_solicitacao = nr_seq_solicitacao_pc;

begin
ds_remetente_w	:= obter_valor_param_usuario(268, 10, Obter_Perfil_Ativo, nm_usuario_p, 0);

select	nvl(max(cd_prioridade),5)
into	cd_prioridade_w
from	pls_email_parametros
where	ie_origem		= 2 --OPS - Gestao de Rescisao de Contrato
and	cd_estabelecimento	= cd_estabelecimento_p
and	ie_situacao		= 'A';

for r_C01_w in C01 loop
	ds_beneficiarios_w := '';
	nr_protocolo_atendimento_w	:= pls_obter_nr_protocolo_atend(r_C01_w.nr_seq_protocolo_atend,5);
	if	(nr_protocolo_atendimento_w is null) and
		(r_c01_w.nr_seq_atendimento is not null) then
		select	max(nr_protocolo_atendimento)
		into	nr_protocolo_atendimento_w
		from	pls_atendimento
		where	nr_sequencia	= r_c01_w.nr_seq_atendimento;
	end if;
	
	select count(1)
	into   qt_benef_w
	from 	PLS_SOLIC_RESCISAO_BENEF 
	where 	nr_seq_solicitacao = r_C01_w.nr_seq_solic_rescisao;

	for r_C05_w in C05(r_C01_w.nr_seq_solic_rescisao) loop
		if (qt_benef_w = 1) then
			ds_beneficiarios_w := ds_beneficiarios_w ||  r_C05_w.nm_segurado;
		elsif (qt_benef_w = r_C05_w.nr_linha) then 
			if (substr(ds_beneficiarios_w, length(ds_beneficiarios_w)-1, length(ds_beneficiarios_w))  = ', ') then
				ds_beneficiarios_w := substr(ds_beneficiarios_w, 1, length(ds_beneficiarios_w) -2);
			end if;
			ds_beneficiarios_w := ds_beneficiarios_w || ' e ' || r_C05_w.nm_segurado;
		else 
		  ds_beneficiarios_w := ds_beneficiarios_w ||  r_C05_w.nm_segurado || ', ';
		end if;
	end loop;


	for r_C02_w in C02(r_C01_w.nr_seq_tipo_contato, r_C01_w.nr_seq_motivo_rejeicao) loop
		ds_conteudo_w := substr(r_C02_w.ds_email,1,4000);
		ds_conteudo_w := substr(replace(ds_conteudo_w,'@DTSOLICITACAO',to_char(r_C01_w.dt_solicitacao, 'dd/mm/yyyy hh24:mi:ss')),1,4000);
		ds_conteudo_w := substr(replace(ds_conteudo_w,'@NMCONTATO',obter_nome_pf(r_C01_w.cd_pessoa_fisica)),1,4000);
		ds_conteudo_w := substr(replace(ds_conteudo_w,'@NRPROTOCOLOATEND',nr_protocolo_atendimento_w),1,4000);
		ds_conteudo_w := substr(replace(ds_conteudo_w,'@DTRESCISAO',to_char(r_c01_w.dt_rescisao, 'dd/mm/yyyy hh24:mi:ss')),1,4000);
		ds_conteudo_w := substr(replace(ds_conteudo_w,'@NRSOLICITACAO', r_C01_w.nr_seq_solic_rescisao),1,4000);
		ds_conteudo_w := substr(replace(ds_conteudo_w,'@BENEFICIARIOS', ds_beneficiarios_w),1,4000);
		
		ds_assunto_w := r_C02_w.ds_assunto;
		ds_assunto_w := replace(ds_assunto_w,'@DTSOLICITACAO',to_char(r_C01_w.dt_solicitacao, 'dd/mm/yyyy hh24:mi:ss'));
		ds_assunto_w := replace(ds_assunto_w,'@NMCONTATO',obter_nome_pf(r_C01_w.cd_pessoa_fisica));
		ds_assunto_w := replace(ds_assunto_w,'@NRPROTOCOLOATEND',nr_protocolo_atendimento_w);
		ds_assunto_w := replace(ds_assunto_w,'@DTRESCISAO',to_char(r_c01_w.dt_rescisao, 'dd/mm/yyyy hh24:mi:ss'));
		ds_assunto_w := replace(ds_assunto_w,'@NRSOLICITACAO', r_C01_w.nr_seq_solic_rescisao);
		ds_assunto_w := replace(ds_assunto_w,'@BENEFICIARIOS', ds_beneficiarios_w);

		select	pls_email_seq.nextval
		into	nr_seq_email_w
		from	dual;
		
		insert into pls_email (
					nr_sequencia,
					nr_seq_solic_rescisao,
					cd_estabelecimento,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					ie_tipo_mensagem,
					ie_status,
					ie_origem,
					ds_remetente,
					ds_mensagem,
					ds_destinatario,
					ds_assunto,
					cd_prioridade,
					cd_pessoa_fisica)
		values(			nr_seq_email_w,
					r_C01_w.nr_seq_solic_rescisao,
					cd_estabelecimento_p,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					2,	--Envio de Dados da Solicitacao de rescisao
					'P',	--Pendente
					2,	--OPS - Gestao de Rescisao de Contrato
					ds_remetente_w,
					ds_conteudo_w,
					r_C01_w.ds_email,
					ds_assunto_w,
					cd_prioridade_w,
					r_C01_w.cd_pessoa_fisica
					);

		for r_C03_w in C03(r_C02_w.nr_sequencia) loop
			select	pls_email_anexo_seq.nextval
			into	nr_seq_anexo_w
			from	dual;
			
			insert into pls_email_anexo (
						nr_sequencia,
						nr_seq_email,
						nm_usuario,
						nm_usuario_nrec,
						dt_atualizacao,
						dt_atualizacao_nrec,
						cd_classif_relat,
						cd_relatorio,
						ds_arquivo,
						ie_tipo_anexo )
			values(			nr_seq_anexo_w,
						nr_seq_email_w,
						nm_usuario_p,
						nm_usuario_p,
						sysdate,
						sysdate,
						r_C03_w.cd_classif_relat,
						r_C03_w.cd_relatorio,
						r_C03_w.ds_arquivo,
						decode(r_C03_w.ds_arquivo,null,'R','A')
						);
			
			if 	(r_C03_w.cd_relatorio is not null) then
				for r_C04_w in C04(	r_C03_w.cd_classif_relat,
							r_C03_w.cd_relatorio) loop
					
					if	(r_C04_w.cd_parametro = 'NR_SEQ_SOLICITACAO') then
						ds_valor_paramentro_w := nr_seq_solicitacao_p;
					elsif	(r_C04_w.cd_parametro = 'NR_SEQ_CONTATO') then
						ds_valor_paramentro_w := r_c01_w.nr_sequencia;
					end if;
					
					insert 	into	pls_email_anexo_param (
									nr_sequencia,
									dt_atualizacao,
									nm_usuario,
									dt_atualizacao_nrec,
									nm_usuario_nrec,
									nr_seq_anexo,
									nm_parametro,
									ds_valor_parametro)
					values(				pls_email_anexo_param_seq.nextval,sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									nr_seq_anexo_w,
									r_C04_w.cd_parametro,
									ds_valor_paramentro_w
									);
				end loop;
			end if;
		end loop;
	end loop;
end loop;

if	(nvl(ie_commit_p,'S') = 'S') then
	commit;
end if;

end pls_envio_email_solic_resc;
/