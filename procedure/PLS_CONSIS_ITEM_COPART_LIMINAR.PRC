create or replace
procedure pls_consis_item_copart_liminar
			(	nr_seq_conta_proc_p		in	number,
				nr_seq_conta_mat_p		in	number,
				nr_seq_segurado_p		in	number,
				dt_referencia_p			in	date,
				ie_valor_zerado_p		out	varchar2,
				nr_seq_processo_copartic_p 	out	number) is

nr_seq_material_w		number(10);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
cd_area_w			number(15);
cd_especialidade_w		number(15);
cd_grupo_w			number(15);
nr_seq_processo_jud_w		number(10);
nr_seq_processo_jud_copar_w	number(10);
qt_registros_w			number(10);
ie_tipo_despesa_w		varchar2(10);
ie_valor_zerado_w		varchar2(10);
nr_seq_contrato_w		number(10);

begin

ie_valor_zerado_w	:= 'N';

select	max(nr_seq_contrato)
into	nr_seq_contrato_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

select	count(1)
into	qt_registros_w
from	processo_judicial_liminar
where	((nr_seq_segurado	= nr_seq_segurado_p) or (nr_seq_segurado is null))
and	((nr_seq_contrato	= nr_seq_contrato_w) or (nr_seq_contrato is null))
and	dt_referencia_p between nvl(dt_inicio_validade,dt_referencia_p) and nvl(dt_fim_validade,dt_referencia_p)
and	ie_estagio			= '2'
and	ie_impacto_coparticipacao	= 'S';

if	(qt_registros_w > 0) then
	select	max(nr_sequencia)
	into	nr_seq_processo_jud_w
	from	processo_judicial_liminar
	where	((nr_seq_segurado	= nr_seq_segurado_p) or (nr_seq_segurado is null))
	and	((nr_seq_contrato	= nr_seq_contrato_w) or (nr_seq_contrato is null))
	and	dt_referencia_p between nvl(dt_inicio_validade,dt_referencia_p) and nvl(dt_fim_validade,dt_referencia_p)
	and	ie_estagio			= '2'
	and	ie_impacto_coparticipacao	= 'S';
	
	select	max(nr_sequencia)
	into	nr_seq_processo_jud_copar_w
	from	pls_processo_jud_copartic
	where	nr_seq_processo	= nr_seq_processo_jud_w
	and	ie_isentar_coparticipacao	= 'S';
	
	if	(nr_seq_processo_jud_copar_w is not null) then		
		if	(nvl(nr_seq_conta_mat_p,0) <> 0) then
			select	nr_seq_material			
			into	nr_seq_material_w
			from	pls_conta_mat
			where	nr_sequencia	= nr_seq_conta_mat_p;
			
			select	max(ie_tipo_despesa)
			into	ie_tipo_despesa_w
			from	pls_material
			where	nr_sequencia = nr_seq_material_w;
			
			select	count(1)
			into	qt_registros_w
			from	pls_processo_jud_mat
			where	nr_seq_processo_copartic	= nr_seq_processo_jud_copar_w
			and	((ie_tipo_despesa	= ie_tipo_despesa_w) or (ie_tipo_despesa is null))
			and	((nr_seq_material	= nr_seq_material_w) or (nr_seq_material is null));
			
			if	(qt_registros_w > 0) then
				ie_valor_zerado_w	:= 'S';
			end if;
			
		elsif	(nvl(nr_seq_conta_proc_p, 0) <> 0) then
			select	cd_procedimento,
				ie_origem_proced
			into	cd_procedimento_w,
				ie_origem_proced_w
			from	pls_conta_proc
			where	nr_sequencia	= nr_seq_conta_proc_p;
			
			pls_obter_estrut_proc(cd_procedimento_w, ie_origem_proced_w, cd_area_w,
				cd_especialidade_w, cd_grupo_w, ie_origem_proced_w);
				
			select	count(1)
			into	qt_registros_w
			from	pls_processo_jud_proc
			where	nr_seq_processo_copartic			= nr_seq_processo_jud_copar_w
			and	nvl(cd_procedimento,cd_procedimento_w)		= cd_procedimento_w
			and	nvl(ie_origem_proced,ie_origem_proced_w) 	= ie_origem_proced_w
			and	nvl(cd_grupo_proc,cd_grupo_w)			= cd_grupo_w
			and	nvl(cd_especialidade, cd_especialidade_w)	= cd_especialidade_w
			and	nvl(cd_area_procedimento, cd_area_w) 		= cd_area_w;
			
			if	(qt_registros_w > 0) then
				ie_valor_zerado_w	:= 'S';
			end if;
		end if;
	end if;
end if;

if	(ie_valor_zerado_w = 'S') then
	nr_seq_processo_copartic_p	:= nr_seq_processo_jud_copar_w;
end if;

ie_valor_zerado_p := nvl(ie_valor_zerado_w, 'N');

end pls_consis_item_copart_liminar;
/