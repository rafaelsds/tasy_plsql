create or replace
procedure gerar_tabela_w_nfe(
				dt_inicio_p		date,
				dt_final_p		date,
				nm_usuario_p		varchar2,
				ie_setor_usuario_p	varchar2,
				nr_nota_fiscal_p	number,
				cd_estabelecimento_p	number,
				ds_filtro_p		varchar2,
				nr_seq_lote_p		number) is


nr_seq_nota_fiscal_w		number(10);
nm_usuario_w			varchar2(15);
cd_setor_nota_w			number(5);
cd_setor_usuario_w		number(5);
ie_grava_w			varchar2(1);
ds_comando_w			varchar2(2000)	:= '';
vl_retorno_w			number(15,4);
ds_parametros_w			varchar2(2000);
ds_sep_bv_w			varchar2(20);
ie_nota_cancelada_w		varchar2(01);
ie_nota_estornada_w		varchar2(01);
ie_somente_nota_calculada_w	varchar2(01);
ie_tratamento_estorno_w		varchar2(01);
qt_notas_rps_w			number(10);
ie_primeira_vez_w		varchar(1) := 'S';

cursor c01 is
	select	a.nr_sequencia,
		a.nm_usuario
	from	nota_fiscal a,
		operacao_nota o
	where	a.cd_operacao_nf = o.cd_operacao_nf
	and	((o.ie_operacao_fiscal in ('S','E')) or
		 (o.ie_devolucao = 'S'))
	and	o.ie_nf_eletronica = 'V'
	and	dt_emissao between trunc(dt_inicio_p,'dd') and fim_dia(dt_final_p)
	and	nvl(ie_status_envio,'N') = 'N'
	and	((ie_status_nfe <> '100')
	or	(ie_status_nfe is null))
	and	a.ie_situacao = 1;

begin
delete from w_nota_fiscal;
ds_sep_bv_w			:= obter_separador_bv;
ie_nota_cancelada_w		:= substr(nvl(obter_valor_param_usuario(40, 110, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'S'),1,1);
ie_nota_estornada_w		:= substr(nvl(obter_valor_param_usuario(40, 142, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'S'),1,1);
ie_somente_nota_calculada_w	:= substr(nvl(obter_valor_param_usuario(40, 124, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'S'),1,1);
ie_tratamento_estorno_w		:= substr(nvl(obter_valor_param_usuario(40, 181, Obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'S'),1,1);


OPEN C01;
LOOP
FETCH C01 into
	nr_seq_nota_fiscal_w,
	nm_usuario_w;
exit when c01%notfound;
	begin
	ds_comando_w	:= 'select count(*) from nota_fiscal where nr_sequencia = :nr_sequencia ' || 
			   ' and nvl(ie_status_envio, :ie_status_envio) = :ie_status_envio ' ||
			   ' and dt_emissao between :dt_inicio and fim_dia(:dt_final) ' || 
			   ' and cd_estabelecimento = :cd_estabelecimento ' || ' ' || ds_filtro_p;

	ds_parametros_w	:= 'nr_sequencia='||nr_seq_nota_fiscal_w|| ds_sep_bv_w ||
			   'ie_status_envio='||'N'|| ds_sep_bv_w|| 'dt_inicio='|| dt_inicio_p || ds_sep_bv_w || 
			   'dt_final='||dt_final_p|| ds_sep_bv_w|| 'cd_estabelecimento=' || cd_estabelecimento_p;

	if	(nvl(nr_nota_fiscal_p,0) > 0) then
		ds_comando_w	:= ds_comando_w || ' and nr_nota_fiscal = :nr_nota_fiscal';
		ds_parametros_w 	:= ds_parametros_w || ds_sep_bv_w || 'nr_nota_fiscal=' || nr_nota_fiscal_p;
	end if;

	
	if	(ie_somente_nota_calculada_w = 'S') then
		ds_comando_w	:= ds_comando_w || ' and dt_atualizacao_estoque is not null';
	end if;
	
	obter_valor_dinamico_bv(ds_comando_w,ds_parametros_w,vl_retorno_w);
	
	if	(vl_retorno_w = 1) then
		begin
		ie_grava_w	:= 'S';

		select	nvl(max(obter_setor_usuario(nm_usuario_w)),0)
		into	cd_setor_nota_w
		from	dual;

		/*Se quem estiver exportando tiver algum setor que o usuario da nota tambem tenha*/
		if	(ie_setor_usuario_p = 'S') then
			select	nvl(max(obter_setor_usuario(nm_usuario_p)),0)
			into	cd_setor_usuario_w
			from	dual;
			/*Se o usuario da nota, ou usuario da exportação nao tiver o setor, nao exporta. Nesse caso ie_setor_usuario_p deve ser N*/
			if	(cd_setor_usuario_w <> cd_setor_nota_w) then
				ie_grava_w	:= 'N';
			end if;
		end if;

		if	(ie_grava_w = 'S') then
			begin
					
			if (ie_primeira_vez_w = 'S') then
				insert into nota_fiscal_lote_nfe 
								(nr_sequencia,
								 cd_estabelecimento,
								 dt_atualizacao,
								 nm_usuario,
								 dt_atualizacao_nrec,
								 nm_usuario_nrec,
								 dt_geracao,
								 dt_envio)
				values				(nr_seq_lote_p,
								 cd_estabelecimento_p,
								 sysdate,
								 nm_usuario_p,
								 sysdate,
								 nm_usuario_p,
								 sysdate,
								 sysdate);
			end if;
			ie_primeira_vez_w := 'N';
			insert into w_nota_fiscal(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_nota_fiscal,
				nr_seq_lote)
			values(	w_nota_fiscal_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_nota_fiscal_w,
				nr_seq_lote_p);
				
			update nota_fiscal set nr_seq_nf_lote_nfe = nr_seq_lote_p
			where nr_sequencia = nr_seq_nota_fiscal_w;
			end;
		end if;
		end;
	end if;
	end;
end loop;
close c01;


commit;

end gerar_tabela_w_nfe;
/
