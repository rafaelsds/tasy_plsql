create or replace
procedure HD_exclusao_dialisador(nr_seq_dialisador_p	number,
				nm_usuario_p		Varchar2) is 
				
nr_seq_reproc_dia_w		number(10);
				
Cursor C01 is
select	nr_sequencia
from	hd_dialisador_reproc
where	nr_seq_dialisador = nr_seq_dialisador_p
order by nr_sequencia;

begin

open C01;
loop
fetch C01 into	
	nr_seq_reproc_dia_w;
exit when C01%notfound;
	begin
	
	delete hd_dialisador_reproc
	where nr_sequencia = nr_seq_reproc_dia_w;
	
	end;
end loop;
close C01;


commit;

end HD_exclusao_dialisador;
/