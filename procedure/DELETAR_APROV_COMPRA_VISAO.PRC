create or replace
procedure deletar_aprov_compra_visao(	nr_sequencia_p		number) is 

begin
if	(nr_sequencia_p is not null) then
	begin	
	delete	from aprov_compra_visao
	where	nr_sequencia = nr_sequencia_p;	
	end;
end if;
commit;

end deletar_aprov_compra_visao;
/