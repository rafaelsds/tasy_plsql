create or replace
procedure SPINAL_RETYR_SINGLE_MESSAGE(message_id_p	varchar2,
						option_p	varchar2) is
						
/* option_p
ALL - retry all messages
SINGLE - retry a single message
*/						

begin

if	(option_p = 'SINGLE') and
	(nvl(message_id_p,'XPTO') <> 'XPTO') then
	
	SPINAL_PCK.RETYR_SINGLE_MESSAGE(message_id_p);
	
elsif	(option_p = 'ALL') then

	SPINAL_PCK.RETRY_MESSAGES_ALL;
	
end if;

end SPINAL_RETYR_SINGLE_MESSAGE;
/