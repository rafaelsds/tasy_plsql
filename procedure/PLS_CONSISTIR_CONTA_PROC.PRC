create or replace
procedure pls_consistir_conta_proc(	nr_seq_conta_proc_p	pls_conta_proc.nr_sequencia%type,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				ie_consistindo_conta_p	varchar2,
				nm_usuario_p		usuario.nm_usuario%type,
				ie_processo_novo_p 	varchar2 default 'N') is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Realizar a consist�ncia de informa��es relacionadas ao procedimento apresentado
	pelo prestador.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es
-------------------------------------------------------------------------------------------------------------------
jjung OS 621943 - 22/06/2013 - 

Altera��o:	Removido a chamada para a procedure PLS_OC_CTA_GERAR_COMBINADA.

Motivo:	N�o � necess�rio chamar para cada item pois na chamada para a conta j� � feita 
	a gera��o para todos os itens.
-------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_conta_proc_w			pls_conta_proc.nr_sequencia%type;
cd_procedimento_w			procedimento.cd_procedimento%type;
ie_origem_proced_w			procedimento.ie_origem_proced%type;
qt_procedimento_w			number(12,4); 
dt_procedimento_w			date; 
vl_calculado_w				number(15,2);
vl_repasse_glosa_w			number(15,2);
vl_repasse_medico_glosa_w		number(15,2);
vl_total_procedimento_w			number(15,2);
vl_materiais_w				number(15,2);
vl_custo_operacional_w			number(15,2);
ie_tipo_despesa_w			varchar2(1); 
ie_liberar_item_autor_w			varchar2(1);
nr_seq_guia_w				pls_guia_plano.nr_sequencia%type;
cd_guia_w				varchar2(20);
cd_guia_referencia_w			varchar2(20); /*Somente a guia referencia*/
cd_guia_princ_w				varchar2(20); /*Somente a guia principal*/
nr_seq_tipo_acomodacao_w		number(10); 
nr_seq_protocolo_w			number(10); 
nr_seq_segurado_w			number(10);
ie_tipo_guia_w				varchar2(2); 
ie_classificacao_w			varchar2(1); 
qt_utilizada_w				number(12,4); 
qt_autorizada_w				number(9,3);
qt_diferenca_proc_w			number(9,3)	:= 0;
nr_seq_prestador_w			number(10); 
ds_contas_autor_w			varchar2(255);
nr_seq_plano_w				number(10); 
qt_glosa_w				number(10); 
ie_segmentacao_w			varchar2(3);
nr_seq_guia_conta_w			number(10);
qt_solicitada_w				number(12,4);
ie_existe_regra_w			varchar2(1); 
nr_seq_tipo_atendimento_w		number(10);
nr_seq_conta_w				number(10);
ie_tipo_conta_w				varchar2(1);
qt_idade_maxima_sus_w			number(10); 
dt_emissao_w				date;
qt_idade_segurado_w			number(10);
ie_sexo_sus_w				varchar2(1);
ie_sexo_w				varchar2(1); 
cd_pessoa_fisica_w			number(10); 
nr_seq_regra_valor_w			number(10);
vl_total_partic_w			number(15,2);
ie_conta_intercambio_w			varchar2(10)	:= 'N';
nr_seq_congenere_w			number(10);
qt_dia_envio_w				number(10);
nr_seq_regra_w				number(10);
ie_permite_w				varchar2(1);
ie_nascido_plano_w			varchar2(1);
nr_seq_prestador_retorno_w		number(15);
nr_seq_regra_retorno_w			number(15);
nr_seq_motivo_glosa_w			number(10);
ie_origem_conta_w			varchar2(10);
nr_seq_evento_w				number(10);
nr_seq_evento_prod_w			number(10);
nr_seq_preco_pacote_w			number(10);
nr_seq_analise_w			number(10);
ie_reconsistencia_w 			varchar2(1) := 'N';
nr_seq_proc_ref_w			number(10);
ie_evento_w				varchar2(2);
ie_existe_item_analise_w		number(10);
nr_seq_regra_preco_w			number(10);
nr_seq_honorario_crit_w			number(10);
ie_gerar_glosa_valor_zerado_w		varchar2(10);
ie_repassa_medico_w			varchar2(10);
ie_repassa_medico_ww			varchar2(15);
ie_regra_liberacao_w			number(10);
qt_procedimento_conta_w			number(10);
nr_seq_hon_crit_medico_w		number(10);
ie_tipo_pagador_w			varchar2(10);
ie_taxa_coleta_w			varchar2(1)	:= 'N';
qt_registro_w				number(10);
qt_partic_conta_w			Number(10);
qt_partic_cancelado_w			Number(10);
ie_partic_zero_w			Varchar2(1);
ie_carencia_abrangencia_ant_w		Varchar2(10);
ie_status_atual_w			varchar2(3);
qt_negativo_w				number(10);
ie_limitacao_w				varchar2(1);
ie_carencia_w				varchar2(1);
ie_cpt_w				varchar2(1);
ie_proc_ativo_w				varchar2(1);
nr_seq_regra_vinculo_w			Number(10);
qt_saldo_w				Number(15,4);
qt_conta_dif_w				pls_integer;
dt_atendimento_conta_w			pls_conta_v.dt_atendimento%type;
qt_autorizada_item_w			pls_integer;
ie_estagio_complemento_w		pls_conta_proc.ie_estagio_complemento%type;
qt_glosa_ativa_w			pls_integer;
dt_procedimento_ref_w			pls_conta_proc.dt_procedimento_referencia%type;
nr_seq_congenere_seg_w			pls_segurado.nr_seq_congenere%type;
nr_seq_prest_inter_w			pls_conta.nr_seq_prest_inter%type;
dados_prestador_exec_w			pls_cta_valorizacao_pck.dados_prestador_exec;
ie_internado_w				pls_conta_v.ie_internado%type;
nr_seq_intercambio_w			pls_segurado.nr_seq_intercambio%type;
ie_tipo_intercambio_w			pls_conta_v.ie_tipo_intercambio%type;
nr_seq_regra_autor_w			pls_regra_autorizacao.nr_sequencia%type;
qt_proc_conta_aut_w			pls_integer;
ie_cobertura_taxa_diaria_w		pls_parametros.ie_cobertura_taxa_diaria%type;
ds_obs_data_w				pls_conta_glosa.ds_observacao%type;

begin

ie_evento_w := 'CC'; /*CONSITENCIA DE CONTA*/

select	nr_seq_conta,
	nr_sequencia,
	cd_procedimento,
	ie_origem_proced, 
	nvl(qt_procedimento_imp,0), 
	dt_procedimento,
	ie_tipo_despesa,
	qt_procedimento_imp,
	vl_total_procedimento,
	vl_procedimento,
	nr_seq_preco_pacote,
	nr_seq_proc_ref,
	nr_seq_regra,
	nr_seq_honorario_crit,
	vl_total_partic,
	nr_seq_regra_valor,
	vl_materiais,
	vl_custo_operacional,
	nr_seq_hon_crit_medico,
	decode(nr_seq_exame_coleta,null,'N','S'),
	ie_status,
	nr_seq_regra_vinculo,
	ie_estagio_complemento,
	dt_procedimento_referencia
into	nr_seq_conta_w,
	nr_seq_conta_proc_w, 
	cd_procedimento_w,
	ie_origem_proced_w,
	qt_procedimento_w,
	dt_procedimento_w,
	ie_tipo_despesa_w,
	qt_solicitada_w,
	vl_total_procedimento_w,
	vl_calculado_w,
	nr_seq_preco_pacote_w,
	nr_seq_proc_ref_w,
	nr_seq_regra_preco_w,
	nr_seq_honorario_crit_w,
	vl_total_partic_w,
	nr_seq_regra_valor_w,
	vl_materiais_w,
	vl_custo_operacional_w,
	nr_seq_hon_crit_medico_w,
	ie_taxa_coleta_w,
	ie_status_atual_w,
	nr_seq_regra_vinculo_w,
	ie_estagio_complemento_w,
	dt_procedimento_ref_w
from	pls_conta_proc
where	nr_sequencia = nr_seq_conta_proc_p;

-- Obter dados da conta 
select	nvl(cd_guia_referencia, cd_guia),/* Felipe - OS 276997 - Coloquei o NVL cd_guia_referencia, pois � esse o campo de integridade com a autoriza��o */
	cd_guia,
	cd_guia_referencia,
	nr_seq_tipo_acomodacao,
	nr_seq_protocolo, 
	nr_seq_segurado,
	ie_tipo_guia,
	nr_seq_tipo_atendimento,
	dt_emissao,
	nvl(ie_tipo_conta,'O'),
	ie_origem_conta,
	nr_seq_analise,
	dt_atendimento,
	nr_seq_guia,
	nr_seq_prestador_prot,
	nr_seq_congenere_prot,
	nr_seq_prest_inter,
	ie_internado,
	ie_tipo_intercambio,
	nr_seq_congenere_seg
into	cd_guia_w,
	cd_guia_princ_w,
	cd_guia_referencia_w,
	nr_seq_tipo_acomodacao_w,
	nr_seq_protocolo_w,
	nr_seq_segurado_w,
	ie_tipo_guia_w, 
	nr_seq_tipo_atendimento_w,
	dt_emissao_w,
	ie_tipo_conta_w,
	ie_origem_conta_w,
	nr_seq_analise_w,
	dt_atendimento_conta_w,
	nr_seq_guia_conta_w,
	nr_seq_prestador_w,
	nr_seq_congenere_w,
	nr_seq_prest_inter_w,
	ie_internado_w,
	ie_tipo_intercambio_w,
	nr_seq_congenere_seg_w
from	pls_conta_v
where	nr_sequencia	= nr_seq_conta_w; 

begin
	select	decode(b.cd_cgc, '','PF','PJ')
	into	ie_tipo_pagador_w
	from	pls_contrato_pagador	b,
		pls_segurado		a
	where	a.nr_sequencia		= nr_seq_segurado_w
	and	a.nr_seq_pagador	= b.nr_sequencia;
exception
	when others then
	ie_tipo_pagador_w	:= 'A';
end;

/*Obter conta intercambio para ocorrencia*/
if	(ie_tipo_conta_w = 'I') then
	ie_conta_intercambio_w	:= 'I';
	ie_evento_w		:= 'I5';
end if;

select	nvl(ie_gerar_glosa_valor_zerado,'S'),
	nvl(ie_carencia_abrangencia_ant,'N'),
	nvl(ie_cobertura_taxa_diaria,'N')
into	ie_gerar_glosa_valor_zerado_w,
	ie_carencia_abrangencia_ant_w,
	ie_cobertura_taxa_diaria_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

if	(ie_conta_intercambio_w	!= 'I') then
	pls_obter_tipo_prest_consist(nr_seq_conta_w, nm_usuario_p, nr_seq_prestador_retorno_w, nr_seq_regra_retorno_w);
end if;

if	(nr_seq_prestador_retorno_w is not null) then
	nr_seq_prestador_w	:= nr_seq_prestador_retorno_w;
end if;

select	count(1)
into	qt_registro_w
from	procedimento
where	cd_procedimento		= cd_procedimento_w
and	ie_origem_proced	= ie_origem_proced_w;

if	(nvl(qt_registro_w,0) = 0) then
	pls_gravar_conta_glosa('1801',null,nr_seq_conta_proc_p,
				null,'N', 'Procedimento inv�lido ou inexistente.', 
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);
end if;

/*Obter idade segurado em anos*/
qt_idade_segurado_w := pls_obter_idade_segurado(nr_seq_segurado_w, dt_emissao_w, 'A'); 	

begin
	--select	nr_seq_plano,
	select	pls_obter_produto_benef(nr_sequencia,dt_atendimento_conta_w),
		nr_seq_intercambio
	into	nr_seq_plano_w,
		nr_seq_intercambio_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
exception
	when others then
	nr_seq_plano_w	:= null;
	pls_gravar_conta_glosa('1013',nr_seq_conta_w,null,
				null,'N','Benefici�rio n�o informado',
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null); 
end;

ds_obs_data_w := pls_valida_regra_data_conta(dt_procedimento_ref_w);

if	(ds_obs_data_w is not null) then
	pls_gravar_conta_glosa('1701',nr_seq_conta_w,null,
				null,'N',ds_obs_data_w || '. Seq item: ' || nr_seq_conta_proc_p,
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null); 
end if;
		
if	(ie_tipo_conta_w = 'I') then 
	
	/*5002 - Problemas no processamento (Quando a data de envio  + a regra da operadora for menor que a data de hoje Glosa */
	pls_obter_data_envio_operadora(dt_procedimento_w, nr_seq_congenere_w, nr_seq_segurado_w, cd_estabelecimento_p, nm_usuario_p, qt_dia_envio_w);
	if	(nvl(qt_dia_envio_w,0) <> 0) then		
		if	(trunc(dt_procedimento_w + qt_dia_envio_w) < trunc(sysdate)) then 		
			ptu_inserir_inconsistencia(null, null, 5002,
						'Cobran�a de interc�mbio indevida. '||to_char(dt_procedimento_w,'DD/MM/YYYY')||' '||to_char(sysdate,'DD/MM/YYYY') ||' fun��o OPS - Cooperativas M�dicas - Cooperativas - Regras - Inter�mbio ', 
						cd_estabelecimento_p, nr_seq_conta_w,
						'CM', 'A500', nr_seq_conta_proc_w,
						null, 4, nm_usuario_p);
		end if; 
	end if;
	
	if	(nvl(cd_procedimento_w, 0) = 0) then 
		/*2031 - Servi�o n�o informado*/ 
		ptu_inserir_inconsistencia(null, null, 2031, 
					 '', cd_estabelecimento_p, nr_seq_conta_w,
					 'CM', 'A500', nr_seq_conta_proc_p,
					 null, 4, nm_usuario_p); 
	else

		/*2003 - Quant. servi�o deve ser maior que 0*/ 
		if	(nvl(qt_solicitada_w, 0) = 0)then
			ptu_inserir_inconsistencia(null, null, 2003,
						 '', cd_estabelecimento_p, nr_seq_conta_w,
						 'CM', 'A500', nr_seq_conta_proc_p,
						 null, 4, nm_usuario_p);
		end if; 

		if	(nvl(nr_seq_segurado_w,0) <> 0) then 

			/*2006 - Idade do benefici�rio acima da idade limite*/
			select	qt_idade_maxima_sus,
				ie_sexo_sus
			into	qt_idade_maxima_sus_w,
				ie_sexo_sus_w 
			from	procedimento
			where	cd_procedimento		= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced_w;

			if	(qt_idade_maxima_sus_w < qt_idade_segurado_w) then				
				ptu_inserir_inconsistencia(null, null, 2006, 
							 '', cd_estabelecimento_p, nr_seq_conta_w, 
							 'CM', 'A500', nr_seq_conta_proc_p, 
							 null, 4, nm_usuario_p); 
			end if;

			/*2011 - Servi�o invalido para o sexo do funcion�rio*/
			select	b.cd_pessoa_fisica, 
				b.ie_sexo
			into	cd_pessoa_fisica_w,
				ie_sexo_w
			from	pls_segurado a,
				pessoa_fisica b 
			where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica 
			and	a.nr_sequencia 		= nr_seq_segurado_w; 

			if	((ie_sexo_w <> ie_sexo_sus_w) or (ie_sexo_sus_w = 'I')) then
				ptu_inserir_inconsistencia(null, null, 2011, 
							 '', cd_estabelecimento_p, nr_seq_conta_w, 
							 'CM', 'A500', nr_seq_conta_proc_p, 
							 null, 4, nm_usuario_p); 
			end if;
		end if; 
	end if;
	
	-- se for pelo processo novo, n�o deve chamar esta rotina
	if	(ie_processo_novo_p = 'N') then
		pls_gerar_ocorrencia(	nr_seq_segurado_w, null, null, 
					nr_seq_conta_w, nr_seq_conta_proc_w, null, 
					cd_procedimento_w, ie_origem_proced_w, null, 
					ie_tipo_guia_w, nr_seq_plano_w, 'C',
					null, ie_tipo_pagador_w, nr_seq_prestador_w,
					nr_seq_tipo_atendimento_w,'I', dt_procedimento_w,
					ie_origem_conta_w,nm_usuario_p, cd_estabelecimento_p,
					nr_seq_congenere_w,ie_conta_intercambio_w, null,null,null);
	end if;
end if; 

if	(ie_tipo_despesa_w = '2') then
	pls_tiss_consistir_taxa(nr_seq_conta_proc_w, 'CP', ie_evento_w, 
		null, null, '', 
		nm_usuario_p, cd_estabelecimento_p,null);
end if;

if	(ie_tipo_despesa_w = '1') then
	pls_tiss_consistir_proc(nr_seq_conta_proc_w, 'CP', ie_evento_w, 
		null, null, '', 
		nm_usuario_p, cd_estabelecimento_p,null);
end if;
	
if	(ie_tipo_guia_w in ('6','5')) then
	pls_tiss_consistir_honorario(nr_seq_conta_proc_w, 'CP', ie_evento_w, 
		null, null, '', 
		nm_usuario_p, cd_estabelecimento_p,null);
end if;	

pls_consistir_elegibilidade(	nr_seq_segurado_w, ie_evento_w, nr_seq_conta_proc_w,
				'CP', null, null,
				'', nm_usuario_p, cd_estabelecimento_p);

pls_tiss_consistir_autorizacao(nr_seq_conta_proc_w, 'CP', ie_evento_w, 
	nr_seq_prestador_w, null, '', 
	nm_usuario_p, cd_estabelecimento_p,null);

select	count(1)
into	qt_glosa_ativa_w
from	tiss_motivo_glosa	tiss
where	cd_motivo_tiss	in ('1308','1312','1321','1317','1304')
and	pls_obter_se_evento_glosa(tiss.nr_sequencia, null, cd_estabelecimento_p) = 'S';

if	(qt_glosa_ativa_w > 0) then

	pls_tiss_consistir_guia(nr_seq_conta_proc_w, 'CP', ie_evento_w, 
				nr_seq_prestador_w, null, '', 
				nm_usuario_p, cd_estabelecimento_p);
end if;

if	(nr_seq_guia_conta_w is not null) then

	nr_seq_guia_w := nr_seq_guia_conta_w;
else
	/* Obter dados da guia */ 
	select	nvl(max(a.nr_sequencia),0)
	into	nr_seq_guia_w 
	from	pls_guia_plano	a
	where	a.cd_guia		= cd_guia_referencia_w
	and	a.ie_status <>	'3'
	and	a.nr_seq_segurado	= nr_seq_segurado_w
	and	((ie_tipo_conta_w <> 'I') or
		((ie_tipo_conta_w = 'I') and
		(a.nr_seq_prestador not in (	select 	x.nr_sequencia 
						from	pls_prestador x)
		or	a.nr_seq_prestador is null)));
	/*Se n�o houver dados da guia principal busca na referencia*/
	if	(nr_seq_guia_w = 0)	then
		select	nvl(max(a.nr_sequencia),0)
		into	nr_seq_guia_w 
		from	pls_guia_plano	a
		where	a.cd_guia		= cd_guia_princ_w
		and	a.ie_status <>	'3'
		and	a.nr_seq_segurado	= nr_seq_segurado_w
		and	((ie_tipo_conta_w <> 'I') or
			((ie_tipo_conta_w = 'I') and
			(not exists (	select 	x.nr_sequencia 
					from	pls_prestador x
					where	x.nr_sequencia = a.nr_seq_prestador)/*Prestador n�o deve estar cadastrado na base , se estiver n�o � conta de interc�mbio*/
			or	a.nr_seq_prestador is null)));
			
		if	(nr_seq_guia_w = 0) and
			(ie_tipo_conta_w = 'I') then
			
			select	nvl(max(e.nr_seq_guia),0)
			into	nr_seq_guia_w
			from	pls_execucao_requisicao  e,
				ptu_resposta_autorizacao d,
				ptu_nota_servico	 c
			where	c.nr_seq_conta_proc  	= nr_seq_conta_proc_p
			and	d.nr_seq_origem		= c.nr_autorizacao
			and	d.nr_seq_requisicao	= e.nr_seq_requisicao
			and	exists	(	select	1
						from	pls_guia_plano_proc x
						where	x.nr_seq_guia 		= e.nr_seq_guia
						and	x.cd_procedimento	= cd_procedimento_w
						and	x.ie_origem_proced	= ie_origem_proced_w);
			if	(nr_seq_guia_w = 0) then
				select	nvl(max(e.nr_seq_guia),0)
				into	nr_seq_guia_w
				from	pls_execucao_requisicao  e,
					ptu_resposta_autorizacao d,
					ptu_nota_servico	 c
				where	c.nr_seq_conta_proc  	= nr_seq_conta_proc_p
				and	d.nr_seq_origem		= c.nr_autorizacao
				and	d.nr_seq_requisicao	= e.nr_seq_requisicao;
			end if;
		end if;
		/*Se ainda assim n�o for encontrado ir� verificar na nota PTU enviada*/	
	end if;
end if;
/*Tratamento para verificar a guia tamb�m pois se n�o houver nada autorizado para a guia referencia tem que buscar da guia*/
ie_liberar_item_autor_w	:= 'N';

/*Procedure que verifica as regras de Procedimentos que necessitam de autoriza��o pr�via (OPS - Cadastro de Regras > Contas M�dicas > Regras de Autoriza��es > Contas M�dicas x Autoriza��es)
    Retorna	ie_liberar_item_autor_w = 'S' quando o procedimento exige a utiliza��o de guia autorizada
	ie_liberar_item_autor_w = 'N' quando n�o exige autoriza��o
		
	ie_existe_regra_w = 'S' quando existe regra para o procedimento
	ie_existe_regra_w = 'N' quando n�o existe regra para o procedimento*/
pls_consiste_proced_autor(nr_seq_conta_w, nr_seq_conta_proc_w, nm_usuario_p, 
			cd_estabelecimento_p, nr_seq_prestador_w, ie_evento_w,
			null, ie_liberar_item_autor_w, ie_existe_regra_w,
			nr_seq_motivo_glosa_w, nr_seq_regra_autor_w);

if	(nr_seq_guia_w <> 0) then /*Se for uma guia existente na OPS - Autoriza��o */
	
	/*TIPO DESPESA QUE NAO FOR  DIARIAS*/
	if	(ie_tipo_despesa_w <> '3') and
		(ie_liberar_item_autor_w = 'S' )then /*Quando N�O for di�rias*/
		/*Procedure que realiza uma verifica��o na guia da conta e retorna se esta autorizada a quantia liberadada e a quantia utilizada*/

		select 	nvl(max(qt_saldo),0),
			nvl(max(qt_utilizada),0),
			nvl(max(qt_autorizada),0)
		into	qt_saldo_w,
			qt_utilizada_w,
			qt_autorizada_w
		from 	table(pls_conta_autor_pck.obter_dados(nr_seq_guia_w,'P', cd_estabelecimento_p, ie_origem_proced_w, cd_procedimento_w)); 
		
		ie_classificacao_w  := ie_tipo_despesa_w;
		
		-- jjung Removido o tratamento abaixo pois esta situa��o j� est� corretamente tratada na view pls_conta_autor_v e 
		-- este c�lculo acabava duplicando as quantidades dos itens.

		if	(qt_autorizada_w > 0) and
			(qt_saldo_w < 0) 	and
			(ie_existe_regra_w = 'S') then/*Verifica se o item realmente esta autorizada*/
			
			select	count(1)
			into	qt_conta_dif_w
			from	pls_conta_autor_utilizada_v
			where	nr_seq_guia		=  nr_seq_guia_w
			and	nr_seq_conta		!= nr_seq_conta_w
			and	cd_procedimento 	= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced_w;
			
			if	( qt_conta_dif_w = 0 )	then -- Se a n�o em outras contas
				
				/*askono OS 349281 - Considerei o tratamento da glosa para taxas quando se exige autorizacao*/
				if	(ie_classificacao_w = '2') then
				
					/*'[1] - Quantidade apresentada do procedimento ('||nvl(qt_procedimento_w,0)||') excede quantidade autorizada ('|| nvl(qt_autorizada_w,0)||').'*/					
					pls_gravar_conta_glosa('2421',null,nr_seq_conta_proc_w,
								null,'N','[1] - Quantidade apresentada do procedimento ('||nvl(qt_procedimento_w,0)||') excede quantidade autorizada ('|| nvl(qt_autorizada_w,0)||'). Regra de autoriza��o: ' || nr_seq_regra_autor_w,
								nm_usuario_p,'A',ie_evento_w, 
								nr_seq_prestador_w, cd_estabelecimento_p, '', null); 											
				else
				
					pls_gravar_conta_glosa('1820',null, nr_seq_conta_proc_w, 
								null,'N',
								'[1] - A quantidade de procedimentos autorizados e ainda n�o utilizados � menor que a quantidade apresentada de procedimentos da conta. Procedimentos utilizados: ' || qt_utilizada_w || 
								' / Procedimento autorizados : ' || qt_autorizada_w || '. Regra de autoriza��o: ' || nr_seq_regra_autor_w,
								nm_usuario_p,'A',ie_evento_w, 
								nr_seq_prestador_w, cd_estabelecimento_p, '', null);
				end if;				
			
			else 	

				select substr(pls_admin_cursor.obter_desc_cursor
						(cursor (  	select	nr_seq_conta ds
								from	pls_conta_autor_utilizada_v
								where	nr_seq_guia	=  nr_seq_guia_w
								and	nr_seq_conta	!= nr_seq_conta_w
								and	cd_procedimento 	= cd_procedimento_w
								and	ie_origem_proced	= ie_origem_proced_w), ', '),1,255) x
				into	ds_contas_autor_w	
				from	dual;
				/*askono OS 349281 - Considerei o tratamento da glosa para taxas quando se exige autorizacao*/
				if	(ie_classificacao_w = '2') then

					pls_gravar_conta_glosa('2421',null,nr_seq_conta_proc_w,
								null,'N','[3] - Quantidade utilizada excede a quantidade autorizada.   Autorizado: '||nvl(qt_autorizada_w,0)||'    Qtde apresentada nesta conta: '||qt_procedimento_conta_w||
								'    (Qtde que j� foi utilizada: '||(nvl(qt_autorizada_w,0) - nvl(qt_diferenca_proc_w,0))||' Contas que utilizaram: '|| ds_contas_autor_w||'). Regra de autoriza��o: ' || nr_seq_regra_autor_w,
								nm_usuario_p,'A',ie_evento_w, 
								nr_seq_prestador_w, cd_estabelecimento_p, '', null);
				else						
					select	instr(qt_saldo_w,'-')
					into	qt_negativo_w
					from	dual;
					
					if	(nvl(qt_negativo_w,0) > 0) then

						pls_gravar_conta_glosa('1820',null, nr_seq_conta_proc_w,
									null,'N', '[3] - Esta guia foi executada em outras contas ('|| ds_contas_autor_w ||'). Quantidade cobrada acima da autorizada : '|| replace(qt_saldo_w,'-','') ||
									' Regra de autoriza��o: ' || nr_seq_regra_autor_w, 
									nm_usuario_p,'A',ie_evento_w,
									nr_seq_prestador_w, cd_estabelecimento_p, '', null);

					elsif	(qt_saldo_w < 0) then

						pls_gravar_conta_glosa('1820',null, nr_seq_conta_proc_w,
									null,'N', '[3] - Esta guia foi executada em outras contas ('|| ds_contas_autor_w ||') e possui saldo de '||qt_saldo_w || '. Regra de autoriza��o: ' || nr_seq_regra_autor_w, 
									nm_usuario_p,'A',ie_evento_w,
									nr_seq_prestador_w, cd_estabelecimento_p, '', null);
					end if;
				end if;					
			end if;					
		end if;
		
	/*TIPO DESPESA DIARIAS E GUIA INTERNACAO*/	
	elsif	(ie_tipo_despesa_w	= '3') and	-- Quando for di�rias e
		(ie_tipo_guia_w	= '5') then		-- Guia de Resumo de Interna��o	

		select 	nvl(max(qt_saldo),0),
			nvl(max(qt_utilizada),0),
			nvl(max(qt_autorizada),0)
		into	qt_saldo_w,
			qt_utilizada_w,
			qt_autorizada_w
		from 	table(pls_conta_autor_pck.obter_dados(nr_seq_guia_w,'D', cd_estabelecimento_p, ie_origem_proced_w, cd_procedimento_w)); 

		
		if	( qt_utilizada_w = 0) then
			qt_utilizada_w := qt_solicitada_w;
		end if;
		
		if	((qt_saldo_w < 0) and
			(qt_autorizada_w > 0)) or
			(qt_autorizada_w = 0) then
			
			select	count(1)
			into	qt_conta_dif_w
			from	pls_conta_autor_utilizada_v
			where	nr_seq_guia		=  nr_seq_guia_w
			and	nr_seq_conta		!= nr_seq_conta_w
			and	cd_procedimento 	= cd_procedimento_w
			and	ie_origem_proced	= ie_origem_proced_w;
			
			if	(qt_conta_dif_w = 0) then 
				pls_gravar_conta_glosa('1999',null,nr_seq_conta_proc_w,
							null,'N', '[1] Quantidade de di�rias utilizadas excede a quantidade de dias autorizados. '||
							'Autorizada: '||qt_autorizada_w||' Utilizada: '||qt_utilizada_w , 
							nm_usuario_p,'A',ie_evento_w,
							nr_seq_prestador_w, cd_estabelecimento_p, '', null);

			else
				select substr(pls_admin_cursor.obter_desc_cursor
						(cursor (  	select	nr_seq_conta ds
								from	pls_conta_autor_utilizada_v
								where	nr_seq_guia	=  nr_seq_guia_w
								and	nr_seq_conta	!= nr_seq_conta_w
								and	cd_procedimento 	= cd_procedimento_w
								and	ie_origem_proced	= ie_origem_proced_w), ', '),1,255) x
				into	ds_contas_autor_w	
				from	dual;
				
				pls_gravar_conta_glosa('1999',null,nr_seq_conta_proc_w,
							null,'N', '[3] Quantidade de di�rias utilizadas excede a quantidade de dias autorizados. Esta guia foi executada em outras contas ('|| ds_contas_autor_w ||') e possui saldo de '|| qt_saldo_w||'. '||
							'Autorizada: '||qt_autorizada_w||' Utilizada: '||qt_utilizada_w,
							nm_usuario_p,'A',ie_evento_w,
							nr_seq_prestador_w, cd_estabelecimento_p, '', null);
			end if;
		end if;
	end if;
end if;

/*Consiste vigencia proc*/
ie_proc_ativo_w	:= pls_obter_se_proc_ativo(cd_procedimento_w, ie_origem_proced_w, dt_procedimento_w);
	

if	(ie_proc_ativo_w = 'N') then
	pls_gravar_conta_glosa('9920', null, nr_seq_conta_proc_p, null, 'N', 'O procedimento n�o est� ativo.', nm_usuario_p, 'A', ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);
elsif	(ie_proc_ativo_w = 'D') then
	pls_gravar_conta_glosa('9920', null, nr_seq_conta_proc_p, null, 'N', 'O procedimento est� fora da data de vig�ncia.', nm_usuario_p, 'A', ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);
end if;

/*TIPO DESPESA PROCEDIMENTOS*/	
if 	(ie_tipo_despesa_w in ('1','4') or
	 (ie_tipo_despesa_w in ('2','3')  and ie_cobertura_taxa_diaria_w = 'S')) then 
	
	select 	nvl(max(qt_autorizada),0),
		nvl(max(qt_saldo),0)
	into	qt_autorizada_item_w,
		qt_saldo_w
	from 	table(pls_conta_autor_pck.obter_dados(nr_seq_guia_w,'P', cd_estabelecimento_p, ie_origem_proced_w, cd_procedimento_w)); 
	
	select	nvl(max(ie_segmentacao),0) 
	into	ie_segmentacao_w 
	from	pls_plano 
	where	nr_sequencia	= nr_seq_plano_w;
	
	pls_consistir_cobertura_proc(	nr_seq_segurado_w, null, nr_seq_conta_proc_w, 
					null, cd_procedimento_w, ie_origem_proced_w, 
					dt_procedimento_w, ie_segmentacao_w, cd_estabelecimento_p, 
					null, nm_usuario_p, qt_glosa_w, 'C'); 

	if 	(nvl(qt_glosa_w,0) = 0) then
		ie_limitacao_w	:= pls_obter_se_gera_glosa_conta('1423',null,'CC',cd_estabelecimento_p);
	
		if	(ie_limitacao_w = 'S') and
			(nvl(qt_autorizada_item_w,0) != nvl(qt_solicitada_w,0))then
			pls_consistir_limitacao_proc(nr_seq_segurado_w, null, nr_seq_conta_proc_w, 
							null, null, cd_procedimento_w, 
							ie_origem_proced_w, qt_solicitada_w, null, 
							cd_estabelecimento_p, nm_usuario_p); 
		end if;	
		
		select	count(1)
		into	qt_proc_conta_aut_w
		from	pls_guia_plano_proc
		where	nr_seq_guia		= nr_seq_guia_w
		and	cd_procedimento		= cd_procedimento_w
		and	ie_origem_proced	= ie_origem_proced_w
		and	rownum			= 1;
		
		if	((ie_liberar_item_autor_w = 'S') and (qt_saldo_w < 0)) or
			((ie_liberar_item_autor_w = 'N') and (qt_proc_conta_aut_w = 0)) then
			ie_carencia_w	:= pls_obter_se_gera_glosa_conta('1410',null,'CC',cd_estabelecimento_p);
		
			if	(ie_carencia_w = 'S') then
				/*aaschlote 21/12/2010 - OS - 271679 - Verificar se o benefici�rio � recem-nascido, caso for, n�o ser� preciso consitir as car�ncias do benefici�rio*/
				pls_obter_segurado_nasc_plano(nr_seq_segurado_w,cd_estabelecimento_p,ie_nascido_plano_w,nm_usuario_p);
				if	(nvl(ie_nascido_plano_w,'N') = 'N') then
					pls_consistir_carencia_proc(nr_seq_segurado_w, null, null, 
									nr_seq_conta_proc_w, null, cd_procedimento_w, 
									ie_origem_proced_w, dt_procedimento_w, cd_estabelecimento_p, 
									nm_usuario_p, null,ie_carencia_abrangencia_ant_w);
				end if;
			end if;
		end if;
		
		ie_cpt_w	:= pls_obter_se_gera_glosa_conta('1421',null,'CC',cd_estabelecimento_p);
	
		if	(ie_cpt_w = 'S') then
			pls_consistir_cpt(	nr_seq_segurado_w, null, nr_seq_conta_proc_w, 
						null, null, cd_procedimento_w, 
						ie_origem_proced_w, dt_procedimento_w, cd_estabelecimento_p, 
						nm_usuario_p);
		end if;
	end if;
end if;

/*TIPO DESPESAS = TAXAS */

select	max(ie_classificacao) 
into	ie_classificacao_w 
from	procedimento
where	cd_procedimento	= cd_procedimento_w
and	ie_origem_proced = ie_origem_proced_w;
/* incluido o tratamento para que quando for pacote seja verificado se existe regra de pacote ou regra de crit�rio de pre�o*/
/*
Regra de libera��o de valores da  conta - OPS- CAdastro de regras\ OPS - Contas m�dicas\ Libera��o de valores na conta
	REGRA 1 - VL APRESENTADO MENOR QUE VL CALCULADO
	REGRA2- VL CALCULADO MENOR QUE  VL APRESENTADO
	REGRA3- VL CALCULADO ZERADO
*/

/*OS 406815 - Quando for regra de "vl calculado zerado" na regra de libera��o de valores da conta, ent�o n�o gera glosa - Solicita��o do Felipe da UNIMED RIO PRETO*/
/* ESTE TRATAMENTO INFLUI NAS GLOSAS 1707 E 9919*/
begin
	select	a.ie_regra_liberacao	
	into	ie_regra_liberacao_w
	from 	pls_regra_valor_conta 	a	
	where	a.nr_Sequencia = nr_seq_regra_valor_w;
exception
when others then
	ie_regra_liberacao_w	:= 0; 
end;

select	count(1)
into	qt_partic_conta_w
from	pls_proc_participante
where	nr_seq_conta_proc = nr_seq_conta_proc_w;

select	count(1)
into	qt_partic_cancelado_w
from	pls_proc_participante
where	nr_seq_conta_proc = nr_seq_conta_proc_w
and	nvl(ie_status,'X') = 'C';

/*Criado para caso for conta de sp/sadt ou resumo de interna��o e houver participante, caso tenha sido gerada conta de honor�rio para este n�o sera gerada a  glosa 1707 e 9919 Diogo OS 433078*/
if	(nvl(qt_partic_cancelado_w,0) = nvl(qt_partic_conta_w,0)) and
	(nvl(qt_partic_conta_w,0) > 0) then
	ie_partic_zero_w := 'S';
else
	ie_partic_zero_w := 'N';
end if;

begin
	select	ie_repassa_medico
	into	ie_repassa_medico_w
	from	pls_regra_honorario
	where	nr_sequencia	= nr_seq_honorario_crit_w;
exception
when others then
	ie_repassa_medico_w	:= 'S';
end;

if	(ie_repassa_medico_w	= 'S') then
	vl_repasse_glosa_w		:= vl_calculado_w;
elsif	(ie_repassa_medico_w	= 'H') then
	vl_repasse_glosa_w		:= vl_total_partic_w;
elsif	(ie_repassa_medico_w	= 'N') then
	vl_repasse_glosa_w		:= vl_materiais_w + vl_custo_operacional_w;
end if;

-- verifica se � para utilizar o campo da regra de pre�o combinada ou campo nr_seq_regra
if 	(pls_filtro_regra_preco_cta_pck.usar_nova_regra_crit_preco(cd_estabelecimento_p) = 'S') then

	select 	max(nr_seq_cp_comb_filtro)
	into	nr_seq_regra_preco_w
	from	pls_conta_proc_regra
	where	nr_sequencia = nr_seq_conta_proc_w;
end if;

if	(nvl(nr_seq_regra_preco_w,0) = 0) and
	(nvl(nr_seq_preco_pacote_w,0) = 0) and 
	(ie_regra_liberacao_w <> 3) and
	((nr_seq_regra_vinculo_w is null) ) and
	(nvl(ie_partic_zero_w,'N') = 'N') then
	pls_gravar_conta_glosa('1707',null,nr_seq_conta_proc_w,
				null,'N','O sistema n�o pode calcular um pre�o para este item.'||chr(13)||chr(10)||
				'Poss�veis motivos: '||chr(13)||chr(10)||
				'1)N�o existem regras de pre�o adequadas ao item, cadastradas na fun��o OPS - Regras e Crit�rios de Pre�o.',
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);
/*Caso exista regra de pre�o, mas o valor calculado est� zerado  e n�o tenha se encaixado na REGRA DE LIBERA��O DE VALORES DA CONTA com regra de "valor calculado zerado" , gera a glos espec�fica 9919 */	
elsif	(nvl(vl_repasse_glosa_w,0)	= 0) and 
	(ie_regra_liberacao_w <> 3) and
	(nvl(ie_partic_zero_w,'N') = 'N') then /*OS 406815 - Quando for regra de vl calculado zerado na libera��o de valores da conta, ent�o n�o gera glosa*/
	if	(ie_repassa_medico_w	= 'S') then
		pls_gravar_conta_glosa('9919',null, nr_seq_conta_proc_w,
				null,'N', 'O sistema obteve uma regra de valoriza��o adequada, mas n�o foi poss�vel realizar o c�lculo do item.'||chr(13)||chr(10)||
				'Poss�veis motivos: '||chr(13)||chr(10)||				
				'1)N�o existe o procedimento cadastrado na tabela de pre�os apontada pela regra.'||chr(13)||chr(10)||
				'2)N�o existem regras de honor�rio m�dico adequadas ao item, cadastradas na fun��o OPS - Cadastro de Regras > OPS - Contas m�dicas > Regra honor�rio.',
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);	
	elsif	(ie_repassa_medico_w	= 'H') then
		pls_gravar_conta_glosa('9919',null, nr_seq_conta_proc_w,
				null,'N', 'O sistema obteve uma regra de valoriza��o adequada, mas n�o foi poss�vel realizar o c�lculo do item.'||chr(13)||chr(10)||
				'Poss�veis motivos: '||chr(13)||chr(10)||				
				'1)N�o existe o procedimento cadastrado na tabela de pre�os apontada pela regra.'||chr(13)||chr(10)||
				'2)N�o existem regras de honor�rio m�dico adequadas ao item, cadastradas na fun��o OPS - Cadastro de Regras > OPS - Contas m�dicas > Regra honor�rio.'||chr(13)||chr(10)||
				'3)Regra de honor�rio paga ao prestador os honor�rio m�dicos, por�m o mesmo n�o foi calculado.',
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);	
	elsif	(ie_repassa_medico_w	= 'N') then
		pls_gravar_conta_glosa('9919',null, nr_seq_conta_proc_w,
				null,'N', 'O sistema obteve uma regra de valoriza��o adequada, mas n�o foi poss�vel realizar o c�lculo do item.'||chr(13)||chr(10)||
				'Poss�veis motivos: '||chr(13)||chr(10)||				
				'1)N�o existe o procedimento cadastrado na tabela de pre�os apontada pela regra.'||chr(13)||chr(10)||
				'2)N�o existem regras de honor�rio m�dico adequadas ao item, cadastradas na fun��o OPS - Cadastro de Regras > OPS - Contas m�dicas > Regra honor�rio.'||chr(13)||chr(10)||
				'3)Regra de honor�rio paga ao prestador o CO e Filme, por�m o mesmo n�o foi calculado.',
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);
	end if;
end if;

begin
select	ie_repassa_medico
into	ie_repassa_medico_ww
from	pls_regra_honorario
where	nr_sequencia	= nr_seq_hon_crit_medico_w;
exception
when others then
	ie_repassa_medico_ww	:= 'S';
end;

if	(ie_repassa_medico_ww	= 'S') then
	vl_repasse_medico_glosa_w	:= vl_calculado_w;
elsif	(ie_repassa_medico_ww	= 'H') then
	vl_repasse_medico_glosa_w	:= vl_total_partic_w;
elsif	(ie_repassa_medico_ww	= 'N') then
	vl_repasse_medico_glosa_w	:= vl_materiais_w + vl_custo_operacional_w;
end if;

if	(nvl(vl_repasse_medico_glosa_w,0)	= 0) and
	(nvl(nr_seq_regra_preco_w,0) > 0) then
	if	(ie_repassa_medico_ww	= 'S') then
		pls_gravar_conta_glosa('9919',null, nr_seq_conta_proc_w,
				null,'N', 'O sistema obteve uma regra de valoriza��o adequada, mas n�o foi poss�vel realizar o c�lculo do item.'||chr(13)||chr(10)||
				'Poss�veis motivos: '||chr(13)||chr(10)||				
				'1)N�o existe o procedimento cadastrado na tabela de pre�os apontada pela regra.'||chr(13)||chr(10)||
				'2)N�o existem regras de honor�rio m�dico adequadas ao item, cadastradas na fun��o OPS - Cadastro de Regras > OPS - Contas m�dicas > Regra honor�rio.',
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);	
	elsif	(ie_repassa_medico_ww	= 'H') then
		pls_gravar_conta_glosa('9919',null, nr_seq_conta_proc_w,
				null,'N', 'O sistema obteve uma regra de valoriza��o adequada, mas n�o foi poss�vel realizar o c�lculo do item.'||chr(13)||chr(10)||
				'Poss�veis motivos: '||chr(13)||chr(10)||				
				'1)N�o existe o procedimento cadastrado na tabela de pre�os apontada pela regra.'||chr(13)||chr(10)||
				'2)N�o existem regras de honor�rio m�dico adequadas ao item, cadastradas na fun��o OPS - Cadastro de Regras > OPS - Contas m�dicas > Regra honor�rio.'||chr(13)||chr(10)||
				'3)Regra de honor�rio paga ao m�dico executor os honor�rio m�dicos, por�m o mesmo n�o foi calculado.',
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);	
	elsif	(ie_repassa_medico_ww	= 'N') then
		pls_gravar_conta_glosa('9919',null, nr_seq_conta_proc_w,
				null,'N', 'O sistema obteve uma regra de valoriza��o adequada, mas n�o foi poss�vel realizar o c�lculo do item.'||chr(13)||chr(10)||
				'Poss�veis motivos: '||chr(13)||chr(10)||				
				'1)N�o existe o procedimento cadastrado na tabela de pre�os apontada pela regra.'||chr(13)||chr(10)||
				'2)N�o existem regras de honor�rio m�dico adequadas ao item, cadastradas na fun��o OPS - Cadastro de Regras > OPS - Contas m�dicas > Regra honor�rio.'||chr(13)||chr(10)||
				'3)Regra de honor�rio paga ao m�dico executor o CO e Filme, por�m o mesmo n�o foi calculado.',
				nm_usuario_p,'A',ie_evento_w,
				nr_seq_prestador_w, cd_estabelecimento_p, '', null);
	end if;
end if;

if	(ie_tipo_conta_w	<> 'I') then
	pls_consistir_proc_prestador(nr_seq_prestador_w, nr_seq_conta_proc_w, null, null, null, cd_estabelecimento_p, nm_usuario_p,ie_evento_w);
end if;

/*Diego - OS's 257673 e 257674 - Verifica��o da idade e sexo . Gera��o das glosas 1803 e 1802.*/ 
pls_consistir_benef_proc(nr_seq_segurado_w, null, nr_seq_conta_proc_w, null, ie_evento_w, nm_usuario_p, cd_estabelecimento_p, qt_idade_segurado_w);

/*Diether - OS - 266284 */
pls_consiste_rede_atend(nr_seq_conta_w,null,null,null,nr_seq_conta_proc_w,null,null,null,nm_usuario_p,cd_estabelecimento_p, nr_seq_regra_w,ie_permite_w);

pls_obter_evento_item(cd_procedimento_w, ie_origem_proced_w, null,
	cd_estabelecimento_p, ie_tipo_guia_w, ie_origem_conta_w,
	nr_seq_tipo_atendimento_w, ie_tipo_despesa_w, nr_seq_segurado_w,
	nr_seq_conta_w, 'N', null, nr_seq_evento_w, nr_seq_evento_prod_w, 'C');
	
-- removida a rotina pls_obter_regra_valor_glosa a mesma n�o � mais utilizada
	
/*Diego - 21/06/2011 - OS  313903 - Ao final da consist�ncia do procedimento este ser� deixado em status "Pendente de libera��o" para se ter uma diferencia��o entre procedimentos consitidos e que faltam consistir.*/
/* Francisco - 17/10/2012 - OS 469604 - Se o item estiver na an�lise na pode mudar o status 
N�o pode estar  "En an�lise" , "Liberado pelo usu�rio" e "Cancelado"*/

-- se for pelo processo novo, n�o deve chamar esta rotina
if	(ie_processo_novo_p = 'N') then
		update	pls_conta_proc
		set	ie_status 		= decode(ie_status,'A','A','D','D','L','L','M','M','P'),
			nr_seq_evento		= nr_seq_evento_w,
			nr_seq_evento_prod	= nr_seq_evento_prod_w
		where	nr_sequencia		= nr_seq_conta_proc_w;
end if;

if	(nvl(nr_seq_analise_w,0) > 0) then
	
	ie_reconsistencia_w := 'S';
	/*VERIFICA SE O ITEM JA EXISTE NA TABELA DE RESUMO DA ANALISE*/
	/*SE NAO EXISTIR, EH POR QUE EST� SENDO INCLUIDO UM NOVO ITEM AO INVES DE RECONSISTIR*/
	select	count(1)
	into	ie_existe_item_analise_w
	from 	w_pls_resumo_conta
	where	nr_seq_item 		= nr_seq_conta_proc_w
	and	ie_origem_proced 	= ie_origem_proced_w
	and	cd_item 		= cd_procedimento_w
	and	nr_Seq_analise 		= nr_seq_analise_w;
	
	if	(ie_existe_item_analise_w = 0) then
		ie_reconsistencia_w := 'N';		
	end if;	
end if;

/* Alexandre - OS 248576 - Gerar ocorr�ncias para o procedimento da conta*/
/* Francisco - 20/05/2012 - OS 418741 - Quando estiver consistindo toda a conta,  vai passar s� uma vez na PLS_GERAR_OCORRENCIA */

-- se for pelo processo novo, n�o deve chamar esta rotina
if	(ie_processo_novo_p = 'N') then
	if	(nvl(ie_consistindo_conta_p,'N') = 'N') then
	
		pls_gerar_ocorrencia(	nr_seq_segurado_w, 'P', null, 
					nr_seq_conta_w, nr_seq_conta_proc_w, null, 
					cd_procedimento_w, ie_origem_proced_w, null, 
					ie_tipo_guia_w, nr_seq_plano_w, 'C',
					null, ie_tipo_pagador_w, nr_seq_prestador_w,
					nr_seq_tipo_atendimento_w, 'I', dt_procedimento_w,
					ie_origem_conta_w,nm_usuario_p, cd_estabelecimento_p,
					nr_seq_congenere_w, ie_conta_intercambio_w, null,
					null, ie_reconsistencia_w);
	end if;
end if;
	
-- se for pelo processo novo, n�o deve chamar esta rotina
if	(ie_processo_novo_p = 'N') then
	if	(ie_tipo_despesa_w = '4')	and
		(nr_seq_preco_pacote_w is not null) then
		-- Gerar a estrutura definida na composi��o do pacote dentro da conta respons�vel pelo procedimento em quest�o. 
		-- Ser�o criados registros de procedimentos e materiais para a conta caso a vig�ncia da composi��o selecionada
		-- esteja valendo para a data de atendimento da conta.
		pls_abrir_proc_pacote(
				nr_seq_preco_pacote_w, nr_seq_conta_w, null,
				null, ie_evento_w, nr_seq_prestador_w,
				ie_tipo_guia_w, dt_atendimento_conta_w, nm_usuario_p, nr_seq_conta_proc_p);
	end if;
end if;

end pls_consistir_conta_proc; 
/