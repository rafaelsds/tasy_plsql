create or replace
procedure Ageint_Gerar_Consulta_Espec(
			cd_agenda_p				number,
			cd_pessoa_fisica_p		varchar2,
			dt_agenda_p				date,
			ie_forma_apres_p		varchar2,
			ie_tipo_agendamento_p	varchar2,
			cd_setor_exclusivo_p	number,
			cd_estabelecimento_p	number,
			nr_seq_area_p			number,
			nr_seq_grupo_p			number,
			nm_usuario_p			varchar2,
			cd_especialidade_p		number,
			ie_espec_adic_agenda_p  varchar2 default 'N',
			ie_clear_agenda_p  		varchar2 default 'S') is 
			
cd_tipo_agenda_w		number(5);
cd_agenda_w				number(10);
cd_estabelecimento_w	number(10);
			
Cursor C01 is
	select	a.cd_agenda
	from	agenda a
	where	a.cd_tipo_agenda = cd_tipo_agenda_w
	and 	((a.cd_especialidade = cd_especialidade_p) or (ie_espec_adic_agenda_p = 'S' and cd_especialidade_p in (select b.cd_especialidade 
																												   from agenda_cons_especialidade b
																												   where a.cd_agenda = b.cd_agenda)))
	and 	a.ie_situacao = 'A';
	
Cursor C02 is
	select	cd_agenda
	from	agenda
	where	cd_tipo_agenda = cd_tipo_agenda_w
	and 	ie_situacao = 'A';

begin

if	(ie_tipo_agendamento_p = 'E') then
	cd_tipo_agenda_w	:= 2;
elsif	(ie_tipo_agendamento_p = 'C') then
	cd_tipo_agenda_w	:= 3;
elsif	(ie_tipo_agendamento_p = 'S') then
	cd_tipo_agenda_w	:= 5;
end if;	

if (nvl(ie_clear_agenda_p,'S') = 'S') then
	delete	ageint_consulta_hor_usu
	where	nm_usuario	= nm_usuario_p; 
end if;

cd_estabelecimento_w := Nvl(cd_estabelecimento_p,obter_estabelecimento_ativo);

if ((cd_agenda_p = 0) and (cd_especialidade_p is not null)) then
	open C01;
		loop
		fetch C01 into	
			cd_agenda_w;
		exit when C01%notfound;
			begin
			Ageint_Gerar_Cons_Hora_html(cd_agenda_w, cd_pessoa_fisica_p, dt_agenda_p, ie_forma_apres_p, ie_tipo_agendamento_p, cd_setor_exclusivo_p, cd_estabelecimento_w, nr_seq_area_p, nr_seq_grupo_p, nm_usuario_p, cd_especialidade_p);
			end;
		end loop;
		close C01;
elsif (nvl(cd_agenda_p,0) <> 0) or (nvl(cd_especialidade_p,0) <> 0) or (nvl(cd_Setor_exclusivo_p,0) <> 0) then
	Ageint_Gerar_Cons_Hora_html(cd_agenda_p, cd_pessoa_fisica_p, dt_agenda_p, ie_forma_apres_p, ie_tipo_agendamento_p, cd_setor_exclusivo_p, cd_estabelecimento_w, nr_seq_area_p, nr_seq_grupo_p, nm_usuario_p, cd_especialidade_p);
elsif (nvl(cd_agenda_p,0) = 0) and (nvl(cd_especialidade_p,0) = 0) and (nvl(cd_Setor_exclusivo_p,0) = 0) then
	for c02_w in c02 loop
		Ageint_Gerar_Cons_Hora_html(c02_w.cd_agenda, cd_pessoa_fisica_p, dt_agenda_p, ie_forma_apres_p, ie_tipo_agendamento_p, cd_setor_exclusivo_p, cd_estabelecimento_w, nr_seq_area_p, nr_seq_grupo_p, nm_usuario_p, cd_especialidade_p);
	end loop;
end if;

end Ageint_Gerar_Consulta_Espec;
/
