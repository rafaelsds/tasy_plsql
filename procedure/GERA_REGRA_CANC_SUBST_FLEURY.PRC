create or replace
procedure gera_regra_canc_subst_fleury(
					nr_prescricao_p			number,
					nr_seq_prescr_p			number,
					nr_seq_prescr_subst_p	number,
					nm_usuario_p			varchar2
					) is 

nm_exame_w		varchar2(2000);
					
begin

insert	into regra_canc_subst_fleury
	(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_prescricao,
	nr_seq_prescr,
	nr_seq_prescr_subst
	)
values
	(
	regra_canc_subst_fleury_seq.nextVal,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_prescricao_p,
	nr_seq_prescr_p,
	nr_seq_prescr_subst_p
	);

if 	(nvl(nr_seq_prescr_subst_p,0) <> 0) and
	(nvl(nr_seq_prescr_subst_p,0) <> nr_seq_prescr_p) then
	
	UPDATE	result_laboratorio
	SET		ie_status = 'I',
			dt_atualizacao = SYSDATE,
			nm_usuario = nm_usuario_p
	WHERE	nr_prescricao = nr_prescricao_p
	AND 	nr_seq_prescricao = nr_seq_prescr_p	
	AND		NVL(ie_status,'A') <> 'I';
	
	select	substr(obter_desc_exame(a.nr_seq_exame),1,255)
	into	nm_exame_w
	from	prescr_procedimento a
	where	a.nr_prescricao = nr_prescricao_p
	and		a.nr_sequencia = nr_seq_prescr_subst_p;
	
	Gravar_Result_Laboratorio( nr_prescricao_p, nr_seq_prescr_p, 'Exame ser� realizado e resultado incorporado ao resultado do exame "'||nm_exame_w||'"', nm_usuario_p, 'S');
	
	update 	result_laboratorio
	set		cd_exame_integracao = 'PADRAO_'|| nr_seq_prescr_p
	where	nr_prescricao = nr_prescricao_p
	and		nr_seq_prescricao = nr_seq_prescr_p;
	
end if;
	
commit;

end gera_regra_canc_subst_fleury;
/