create or replace
procedure pls_tiss_consistir_taxa
			(	nr_sequencia_p		Number,
				ie_tipo_glosa_p		Varchar2,
				ie_evento_p		Varchar2,
				nr_seq_prestador_p	Number,
				nr_seq_ocorrencia_p	Number,
				ds_parametro_um_p	Varchar2,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number,
				ie_origem_proced_p	number) is 
				
/* IE_TIPO_GLOSA_P
	C - Conta
	CP - Conta procedimento
	CM - Conta material
	A - Autorização
	AP - Autorização procedimento
	AM - Autorização material
*/

cd_procedimento_w		number(15);
ie_origem_proced_w		number(5);
qt_proc_w			number(3);
nr_seq_conta_w			pls_conta.nr_sequencia%type;

begin

if	(ie_tipo_glosa_p	= 'CP') then
	begin
		select	nvl(cd_procedimento,cd_procedimento_imp),
			nvl(ie_origem_proced,ie_origem_proced_p),
			nr_seq_conta
		into	cd_procedimento_w,
			ie_origem_proced_w,
			nr_seq_conta_w
		from	pls_conta_proc
		where	nr_sequencia = nr_sequencia_p;
	exception
	when others then
		cd_procedimento_w := null;
		ie_origem_proced_w := null;
	end;	

	select	count(1)
	into	qt_proc_w
	from	procedimento
	where	cd_procedimento	= cd_procedimento_w
	and	ie_origem_proced = ie_origem_proced_w;	
end if;


/* 2401 - Taxa / Aluguel inválido*/
if	(qt_proc_w = 0) then
	pls_gravar_glosa_tiss('2401', 
		null, ie_evento_p,
		nr_sequencia_p, ie_tipo_glosa_p, nr_seq_prestador_p,
		nr_seq_ocorrencia_p, '', nm_usuario_p,
		cd_estabelecimento_p, nr_seq_conta_w);
end if;
/*
commit;
*/
end pls_tiss_consistir_taxa;
/