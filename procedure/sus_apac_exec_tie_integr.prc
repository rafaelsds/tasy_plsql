create or replace procedure sus_apac_exec_tie_integr(
                                    cd_estabelecimento_p 	in estabelecimento.cd_estabelecimento%type,
                                    nm_usuario_p 		in usuario.nm_usuario%type
				    ) is

cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type not null := cd_estabelecimento_p;
nm_usuario_w		usuario.nm_usuario%type	not null := nm_usuario_p;
ds_json_w		varchar2(2000 char);
dt_data_format_w	varchar2(100 char);

YEAR_W 		constant varchar2(4 char) := 'YEAR';
MONTH_W 	constant varchar2(5 char) := 'MONTH';
DAY_W 		constant varchar2(3 char) := 'DAY';
HIPHEN_W 	constant varchar2(1 char) := '-';
TZ_HR_W 	constant varchar2(19 char) := 'T00:00:00.000-03:00';

OPEN_JSON_W 	constant varchar2(1 char) := '{';
DT_INIT_W 	constant varchar2(24 char) := '"DT_INICIO_CADASTRO" : ';
NR_GERPAC_W 	constant varchar2(23 char) := '"DS_PROTOCOLO_SOLIC" : ';
COMMA_W 	constant varchar2(1 char) := ',';
QUOT_MARK_W 	constant varchar2(1 char) := '"';
CLOSE_JSON_W 	constant varchar2(1 char) := '}';
TRUNC_DAY_W 	constant varchar2(2 char) := 'dd';

INTEGRATION_CODE_W constant pls_integer := 1002;

cursor laudos is 
select 	nr_protocolo_gerpac
from	sus_laudo_paciente
where	dt_envio_gerpac is not null
and	nr_protocolo_gerpac is not null
and	trunc(dt_envio_gerpac, TRUNC_DAY_W) = trunc(sysdate, TRUNC_DAY_W);

begin

	if wheb_usuario_pck.get_cd_estabelecimento is null then
		wheb_usuario_pck.set_cd_estabelecimento(cd_estabelecimento_w);
	end if;
    
	if wheb_usuario_pck.get_nm_usuario is null then
		wheb_usuario_pck.set_nm_usuario(nm_usuario_w);
	end if;

	/* Convert current date to ISO-8601. */
	dt_data_format_w := pkg_date_utils.extract_field(field => YEAR_W, baseDate => sysdate)
	|| HIPHEN_W || pkg_date_utils.extract_field(field => MONTH_W, baseDate => sysdate)
	|| HIPHEN_W || pkg_date_utils.extract_field(field => DAY_W, baseDate => sysdate)
	|| TZ_HR_W;

	for laudo in laudos loop 
	
		/* Create JSON. */
		ds_json_w := OPEN_JSON_W
			|| DT_INIT_W   || QUOT_MARK_W || dt_data_format_w || QUOT_MARK_W || COMMA_W
			|| NR_GERPAC_W || QUOT_MARK_W || laudo.nr_protocolo_gerpac || QUOT_MARK_W
			|| CLOSE_JSON_W;
		
		/* 
			Execute integration request.
			1002 => "API - GET - APAC Executor" flow code.
		*/
		
		execute_bifrost_integration(cd_integration_p => INTEGRATION_CODE_W, 
			ds_parameters_p => ds_json_w);
			
		
	end loop;

	

end sus_apac_exec_tie_integr;
/
