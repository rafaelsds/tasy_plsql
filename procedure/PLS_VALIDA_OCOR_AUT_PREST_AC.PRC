create or replace
procedure pls_valida_ocor_aut_prest_ac(	nr_seq_ocor_combinada_p			pls_ocor_aut_combinada.nr_sequencia%type,
					nr_seq_ocorrencia_p			pls_ocorrencia.nr_sequencia%type,
					nr_seq_segurado_p			pls_segurado.nr_sequencia%type,
					nr_seq_motivo_glosa_p			tiss_motivo_glosa.nr_sequencia%type,
					nr_seq_guia_p				pls_guia_plano.nr_sequencia%type,
					nr_seq_requisicao_p			pls_requisicao.nr_sequencia%type,
					nr_seq_execucao_p			pls_execucao_requisicao.nr_sequencia%type,
					ie_utiliza_filtro_p			varchar2,
					nr_seq_param1_p				number,
					nr_seq_param2_p				number,
					nr_seq_param3_p				number,
					nr_seq_param4_p				number,
					nr_seq_param5_p				number,
					nm_usuario_p				usuario.nm_usuario%type,
					cd_estabelecimento_p			estabelecimento.cd_estabelecimento%type ) is 

ie_valida_alto_custo_w			pls_validacao_aut_prest_ac.ie_valida_alto_custo%type;
ie_tipo_rede_min_w				ptu_pedido_autorizacao.ie_tipo_rede_min%type;
ie_tipo_documento_w			varchar2(1);	
ie_regra_w				varchar2(1);
ie_tipo_ocorrencia_w			varchar2(1);
nr_seq_oc_benef_w			pls_ocorrencia_benef.nr_sequencia%type;

ie_excecao_w				varchar2(1) := 'N';
ie_ocorrencia_w				varchar2(1) := 'N';
ie_gerar_ocorrencia_w			varchar2(1);
nr_seq_prestador_w			pls_requisicao.nr_seq_prestador%type;

nr_seq_guia_proc_w			pls_guia_plano_proc.nr_sequencia%type;
nr_seq_guia_mat_w			pls_guia_plano_mat.nr_sequencia%type;

cd_procedimento_w			procedimento.cd_procedimento%type;
ie_origem_proced_w			procedimento.ie_origem_proced%type;
nr_seq_material_w			pls_material.nr_sequencia%type;

nr_seq_req_proc_w			pls_requisicao_proc.nr_sequencia%type;
nr_seq_req_mat_w			pls_requisicao_mat.nr_sequencia%type;

ie_tipo_item_w				number(10);

Cursor c01 is 
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_guia_plano_proc
	where	nr_seq_guia = nr_seq_guia_p;

Cursor c02 is 
	select 	nr_sequencia,
		nr_seq_material
	from	pls_guia_plano_mat
	where	nr_seq_guia = nr_seq_guia_p;

Cursor c03 is 
	select	nr_sequencia,
		cd_procedimento,
		ie_origem_proced
	from	pls_requisicao_proc
	where	nr_seq_requisicao = nr_seq_requisicao_p;

Cursor c04 is 
	select	nr_sequencia,
		nr_seq_material
	from	pls_requisicao_mat
	where	nr_seq_requisicao = nr_seq_requisicao_p;

begin

begin
	select	ie_valida_alto_custo
	into	ie_valida_alto_custo_w
	from	pls_validacao_aut_prest_ac
	where	nr_sequencia	=	(select	max(nr_sequencia)
					from	pls_validacao_aut_prest_ac
					where	nr_seq_ocor_aut_combinada	= nr_seq_ocor_combinada_p
					and	ie_situacao			= 'A');
exception
when others then
	ie_valida_alto_custo_w := 'N';
end;

if	((ie_valida_alto_custo_w is not null) and (ie_valida_alto_custo_w = 'S')) then

	ie_gerar_ocorrencia_w := 'S';

	if (nr_seq_guia_p is not null) then
		
		--Autorização
		ie_tipo_documento_w	:= 'A';

		begin
			select	ie_tipo_rede_min
			into	ie_tipo_rede_min_w
			from	ptu_pedido_autorizacao
			where 	nr_seq_guia = nr_seq_guia_p;
		exception
		when others then
			ie_tipo_rede_min_w := null;
		end;

		begin
			select	nr_seq_prestador
			into	nr_seq_prestador_w
			from	pls_guia_plano
			where	nr_sequencia = nr_seq_guia_p;
		exception
		when others then
			nr_seq_prestador_w := null;
		end;

	elsif (nr_seq_requisicao_p is not null) then

		--Requisição
		ie_tipo_documento_w	:= 'R';

		begin
			select	ie_tipo_rede_min
			into	ie_tipo_rede_min_w
			from	ptu_pedido_autorizacao
			where 	nr_seq_requisicao = nr_seq_requisicao_p;
		exception
		when others then
			ie_tipo_rede_min_w := null;
		end;

		begin
			select	nr_seq_prestador
			into	nr_seq_prestador_w
			from	pls_requisicao
			where	nr_sequencia = nr_seq_requisicao_p;
		exception
		when others then
			nr_seq_prestador_w := null;
		end;
		
	end if;
	
	--	2 - Especial (Tabela Própria)
	--	3 - Master (Alto Custo)

	if	(ie_tipo_rede_min_w in (2,3)) then		

		if (ie_tipo_documento_w = 'A') then -- Autorização
			if	( ie_utiliza_filtro_p	= 'S' ) then
			/* Tratamento para filtros */
				open c01;
				loop
				fetch c01 into	
					nr_seq_guia_proc_w,
					cd_procedimento_w,
					ie_origem_proced_w;
				exit when c01%notfound;
					begin
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, null,
										null, nr_seq_guia_proc_w, null,
										null, null, null,
										cd_procedimento_w, ie_origem_proced_w, null,
										null, nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

						if	( ie_regra_w	= 'S' ) then
							ie_ocorrencia_w		:= 'S';
						elsif	( ie_regra_w	= 'E' ) then
							ie_excecao_w		:= 'S';
						end if;
					end;
				end loop;
				close c01;

				open c02;
				loop
				fetch c02 into	
					nr_seq_guia_mat_w,
					nr_seq_material_w;
				exit when c02%notfound;
					begin
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, nr_seq_guia_p, null,
										null, null, nr_seq_guia_mat_w,
										null, null, null,
										null, null, nr_seq_material_w,
										null, nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

						if	( ie_regra_w	= 'S' ) then
							ie_ocorrencia_w		:= 'S';
						elsif	( ie_regra_w	= 'E' ) then
							ie_excecao_w		:= 'S';
						end if;
					end;
				end loop;
				close c02;

				if	(ie_excecao_w = 'S' or (ie_excecao_w = 'N' and ie_ocorrencia_w = 'N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				elsif	(ie_ocorrencia_w = 'S' and ie_excecao_w <> 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				end if;
			end if;
		elsif (ie_tipo_documento_w = 'R') then --Requisição
			if	( ie_utiliza_filtro_p	= 'S' ) then
				/* Tratamento para filtros */
				open c03;
				loop
				fetch c03 into	
					nr_seq_req_proc_w,
					cd_procedimento_w,
					ie_origem_proced_w;
				exit when c03%notfound;
					begin
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, nr_seq_requisicao_p,
										null, null, null,
										nr_seq_req_proc_w, null, null,
										cd_procedimento_w, ie_origem_proced_w, null,
										null, nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

						if	( ie_regra_w	= 'S' ) then
							ie_ocorrencia_w		:= 'S';
						elsif	( ie_regra_w	= 'E' ) then
							ie_excecao_w		:= 'S';
						end if;
					end;
				end loop;
				close c03;

				open c04;
				loop
				fetch c04 into	
					nr_seq_req_mat_w,
					nr_seq_material_w;
				exit when c04%notfound;
					begin
						pls_gerar_ocor_aut_filtro(	nr_seq_ocor_combinada_p, null, nr_seq_requisicao_p,
										null, null, null,
										null, nr_seq_guia_mat_w, null,
										null, null, nr_seq_material_w,
										null, nr_seq_prestador_w, nr_seq_ocorrencia_p,
										null, null, null,
										nm_usuario_p, ie_regra_w, ie_tipo_ocorrencia_w);

						if	( ie_regra_w	= 'S' ) then
							ie_ocorrencia_w		:= 'S';
						elsif	( ie_regra_w	= 'E' ) then
							ie_excecao_w		:= 'S';
						end if;
					end;
				end loop;
				close c04;

				if	(ie_excecao_w = 'S' or (ie_excecao_w = 'N' and ie_ocorrencia_w = 'N')) then
					ie_gerar_ocorrencia_w	:= 'N';
				elsif	(ie_ocorrencia_w = 'S' and ie_excecao_w <> 'S') then
					ie_gerar_ocorrencia_w	:= 'S';
				end if;
			end if;
		end if;	

		if	( ie_gerar_ocorrencia_w	= 'S' ) then		
			if	(nr_seq_guia_p	is not null) then
				ie_tipo_item_w	:= 7;
			elsif	(nr_seq_requisicao_p	is not null) then
				ie_tipo_item_w	:= 9;
			end if;

			pls_inserir_ocorrencia(		nr_seq_segurado_p, nr_seq_ocorrencia_p, nr_seq_requisicao_p,
							nr_seq_guia_p, null, null, 
							null, nr_seq_ocor_combinada_p, nm_usuario_p, 
							null, nr_seq_motivo_glosa_p, ie_tipo_item_w, 
							cd_estabelecimento_p, 'N' ,null,
							nr_seq_oc_benef_w, null,
							null, null, null);

			pls_atualizar_status_ocor_comb(	nr_seq_ocorrencia_p, nr_seq_ocor_combinada_p, nr_seq_motivo_glosa_p,
							nr_seq_guia_p, nr_seq_requisicao_p, null,
							null, null, null,
							null, null, null,
							nm_usuario_p, cd_estabelecimento_p);
		end if;
	end if;
end if;

commit;

end pls_valida_ocor_aut_prest_ac;
/