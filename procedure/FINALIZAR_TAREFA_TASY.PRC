create or replace
procedure finalizar_tarefa_tasy(	nr_sequencia_p	number,
				nm_usuario_p	varchar ) is

dt_final_w	date;

begin

begin
select		max(dt_final)
into		dt_final_w
from		tarefa_tasy
where		nr_sequencia = nr_sequencia_p;
exception
	when no_data_found then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266279);
end;


if (dt_final_w is null) then
	update		tarefa_tasy
	set		dt_final		=	sysdate,
			ie_status		=	'S',
			nm_usuario	=	nm_usuario_p
	where		nr_sequencia = nr_sequencia_p;
else
	update		tarefa_tasy
	set		ie_status		=	'S',
			nm_usuario	=	nm_usuario_p
	where		nr_sequencia = nr_sequencia_p;
end if;

commit;		

end finalizar_tarefa_tasy;
/