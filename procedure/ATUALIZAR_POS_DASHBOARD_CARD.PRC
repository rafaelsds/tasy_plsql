create or replace
procedure atualizar_pos_dashboard_card(
			nm_usuario_p		Varchar2,
			nr_seq_dashboard_p number,	
			qt_x_p	number,
			qt_y_p	number) is 

begin

	update	dashboard_cards
	set		nm_usuario = nm_usuario_p,
			dt_atualizacao = sysdate,
			qt_x = qt_x_p,
			qt_y = qt_y_p
	where	nr_sequencia = nr_seq_dashboard_p;

commit;

end atualizar_pos_dashboard_card;
/