create or replace
procedure gerar_tabela_sistema_idioma(
		cd_funcao_p	number,
		nr_seq_idioma_p	number,
		nm_usuario_p	varchar2) is
		
nm_tabela_w		varchar2(50);
nm_atributo_w		varchar2(50);
nr_seq_visao_w		number(10,0);
ds_texto_w		varchar2(255);
cd_funcao_w		number(5,0);
nr_seq_traducao_w	number(10,0);

cursor c01 is
select	a.nm_tabela,
	'DS_TITULO',
	a.ds_titulo,
	a.cd_funcao
from	tabela_sistema a
where	a.cd_funcao = cd_funcao_p
and	a.ds_titulo is not null
and	not exists (
		select	1
		from	tab_sistema_idioma x
		where	x.nm_tabela = a.nm_tabela
		and	x.nm_atributo = 'DS_TITULO'
		and	x.nr_seq_visao is null
		and	x.nr_seq_idioma = nr_seq_idioma_p);
		
cursor c02 is
select	a.nm_tabela,
	'DS_TITULO',
	a.nr_sequencia,
	a.ds_titulo,
	a.cd_funcao
from	tabela_visao a
where	a.cd_funcao = cd_funcao_p
and	a.ds_titulo is not null
and	not exists (
		select	1
		from	tab_sistema_idioma x
		where	x.nm_tabela = a.nm_tabela
		and	x.nm_atributo = 'DS_TITULO'
		and	x.nr_seq_visao = a.nr_sequencia
		and	x.nr_seq_idioma = nr_seq_idioma_p);
		
begin
if	(cd_funcao_p is not null) and
	(nr_seq_idioma_p is not null) and
	(nm_usuario_p is not null) then
	begin
	open c01;
	loop
	fetch c01 into	nm_tabela_w,
			nm_atributo_w,
			ds_texto_w,
			cd_funcao_w;
	exit when c01%notfound;
		begin
		if	(trim(ds_texto_w) is not null) then
			begin
			select	tab_sistema_idioma_seq.nextval
			into	nr_seq_traducao_w
			from	dual;
		
			insert into tab_sistema_idioma (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				nm_tabela,
				nm_atributo,
				nr_seq_visao,
				ds_descricao,
				cd_funcao,
				nr_seq_idioma,			
				ds_traducao,
				ie_necessita_revisao)
			values (
				nr_seq_traducao_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nm_tabela_w,
				nm_atributo_w,
				null,
				ds_texto_w,
				cd_funcao_w,
				nr_seq_idioma_p,
				' ',
				'T');
			end;
		end if;
		end;
	end loop;
	close c01;	
	
	open c02;
	loop
	fetch c02 into	nm_tabela_w,
			nm_atributo_w,
			nr_seq_visao_w,
			ds_texto_w,
			cd_funcao_w;
	exit when c02%notfound;
		begin
		if	(trim(ds_texto_w) is not null) then
			begin
			select	tab_sistema_idioma_seq.nextval
			into	nr_seq_traducao_w
			from	dual;
			
			insert into tab_sistema_idioma (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				nm_tabela,
				nm_atributo,
				nr_seq_visao,
				ds_descricao,
				cd_funcao,
				nr_seq_idioma,			
				ds_traducao,
				ie_necessita_revisao)
			values (
				nr_seq_traducao_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nm_tabela_w,
				nm_atributo_w,
				nr_seq_visao_w,
				ds_texto_w,
				cd_funcao_w,
				nr_seq_idioma_p,			
				' ',
				'T');
			end;
		end if;
		end;
	end loop;
	close c02;	
	end;
end if;
commit;
end gerar_tabela_sistema_idioma;
/