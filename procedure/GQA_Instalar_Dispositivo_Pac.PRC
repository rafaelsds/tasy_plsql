create or replace
procedure GQA_Instalar_Dispositivo_Pac (nr_sequencia_p		    number,
                                        nm_usuario_p		    Varchar2,
                                        nr_regras_atendidas_p   out varchar2,
                                        nr_atendimento_p        number default null,
                                        nr_restrigir_regras_p   varchar2 default null,
                                        qt_horas_retroativa_p   number default 0,
										ie_acao_p				varchar2) is
										
nr_seq_pend_regra_w	        number(10);
vl_minimo_w                 	number(15,4);
vl_maximo_w                 	number(15,4);
nr_seq_disp_w        			number(10);
nr_seq_dispositivo_w			number(10);	
nr_atendimento_w            	number(10);
nm_atributo_w               	varchar2(50);
nr_sequencia_w              	number(10);
vl_atributo_w               	number(15,4);
cd_pessoa_fisica_w          	varchar2(10);
qt_idade_w                  	number(10);
cd_setor_atendimento_w      	number(10) := null;
cd_escala_dor_w             	varchar2(10);
nr_seq_result_dor_w         	number(10);
qt_idade_dias_w             	number(6);
qt_peso_gramas_w            	number(12,3);
ie_restricao_complementar_w 	varchar2(80);
ie_protocolo_assistencial_w 	boolean;
ie_informacao_w			gqa_pendencia_regra.ie_informacao%type;
ie_opcao_informacao_w		gqa_pendencia_regra.ie_opcao_informacao%type;
ds_sql_w			gqa_pendencia_regra.ds_sql%type;
ie_atende_regra_w		boolean;
ds_retorno_sql_w		varchar2(4000);			
nr_seq_pend_pac_w		number(10);	

Cursor C01 is
		select	a.nr_sequencia,
		nvl(a.vl_minimo,0),
		nvl(a.vl_maximo,999),
		a.nr_seq_sinal_vital,
		c.nr_seq_dispositivo,
		null ie_informacao,
		null ds_sql
	from    gqa_pendencia_regra a,
		gqa_pendencia b,
		atend_pac_dispositivo c
	where   b.nr_sequencia	= a.nr_seq_pendencia
	and	a.nr_seq_dispositivo = c.nr_seq_dispositivo
	and	b.ie_tipo_pendencia	= 10
	and	b.ie_situacao 		= 'A'
	and	a.ie_situacao		= 'A'
	and a.ie_acao = ie_acao_p
	and	c.nr_sequencia = nr_sequencia_p
	and	obter_se_gqa_regra_liberada(a.nr_sequencia) = 'S' 
	and	qt_idade_w       between nvl(a.qt_idade_min,0) and nvl(a.qt_idade_max,999)
	and	qt_idade_dias_w  between nvl(a.qt_dias_min ,0) and nvl(a.qt_dias_max ,99999)
	and	qt_peso_gramas_w between nvl(a.qt_peso_gramas_min,0) and nvl(a.qt_peso_gramas_max,999999999)
	and	nvl(a.cd_setor_atendimento,nvl(cd_setor_atendimento_w,0))	= nvl(cd_setor_atendimento_w,0)
	and	(nr_restrigir_regras_p is null or instr(nr_restrigir_regras_p, a.nr_sequencia) > 0)
	union
	select	a.nr_sequencia,
		nvl(a.vl_minimo,0),
		nvl(a.vl_maximo,999),
		null,
		null,
		a.ie_informacao,
		a.ds_sql
	from    gqa_pendencia_regra a,
		gqa_pendencia b
	where   b.nr_sequencia	= a.nr_seq_pendencia
	and	b.ie_tipo_pendencia	= 10
	and	b.ie_situacao 		= 'A'
	and	a.ie_situacao		= 'A'
	and a.ie_acao = ie_acao_p
	and	nvl(a.ie_opcao_informacao,'SV') = 'SQL'
	and	a.ds_sql is not null
	and	a.ie_informacao is not null
	and	obter_se_gqa_regra_liberada(a.nr_sequencia) = 'S'
	and	qt_idade_w       between nvl(a.qt_idade_min,0) and nvl(a.qt_idade_max,999)
	and	qt_idade_dias_w  between nvl(a.qt_dias_min ,0) and nvl(a.qt_dias_max ,99999)
	and	qt_peso_gramas_w between nvl(a.qt_peso_gramas_min,0) and nvl(a.qt_peso_gramas_max,999999999)
	and	nvl(a.cd_setor_atendimento,nvl(cd_setor_atendimento_w,0))	= nvl(cd_setor_atendimento_w,0)
	and	(nr_restrigir_regras_p is null or instr(nr_restrigir_regras_p, a.nr_sequencia) > 0);

	
	CursorDispositivo				SYS_REFCURSOR;

begin
	ie_protocolo_assistencial_w := nr_restrigir_regras_p is not null;
	if ie_protocolo_assistencial_w then
		open CursorDispositivo for select max(nr_atendimento), max(cd_profissional), nr_seq_dispositivo
								   from   atend_pac_dispositivo
								   where  nr_atendimento = nr_atendimento_p
								   group by nr_seq_dispositivo;
	else
		open CursorDispositivo for select max(nr_atendimento), max(cd_profissional), nr_seq_dispositivo
								   from   atend_pac_dispositivo
								   where  nr_sequencia = nr_sequencia_p
								   group by nr_seq_dispositivo;
	end if;
	
	loop
	fetch CursorDispositivo into
			nr_atendimento_w,
			cd_pessoa_fisica_w,
			nr_seq_disp_w;
	exit when CursorDispositivo%notfound;
	begin	
		if 	(cd_setor_atendimento_w is null) then
			cd_setor_atendimento_w		:= obter_setor_atendimento(nr_atendimento_w);
			qt_idade_w		        := obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A');
			qt_idade_dias_w		    	:= obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'DIA');
			qt_peso_gramas_w		:= (nvl(obter_peso_pf(cd_pessoa_fisica_w),0) * 1000);
		end if;
		open C01;
		loop
		fetch C01 into	
			nr_seq_pend_regra_w,
			vl_minimo_w,
			vl_maximo_w,
			nm_atributo_w,
			nr_seq_dispositivo_w,
			ie_informacao_w,
			ds_sql_w;
		exit when C01%notfound;
			begin		
					
			    select GQA_regra_complementar(nm_usuario_p, nr_atendimento_w, nr_seq_pend_regra_w) 
				into ie_restricao_complementar_w
				from dual;
			
							
				if 	(ie_restricao_complementar_w = 'X') then

					if (ie_protocolo_assistencial_w) then 
						if 	(nr_regras_atendidas_p is not null) then
							nr_regras_atendidas_p := nr_regras_atendidas_p || ',';
						end if;
						
						nr_regras_atendidas_p := nr_regras_atendidas_p || nr_seq_pend_regra_w;
						
					else
						select	gqa_pendencia_pac_seq.nextval
						into	nr_sequencia_w
						from	dual;
								
						insert into gqa_pendencia_pac (	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								cd_pessoa_fisica,
								nr_atendimento,
								nr_seq_pend_regra,
								nr_seq_dispositivo)
						values	(    nr_sequencia_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								cd_pessoa_fisica_w,
								nr_atendimento_w,
								nr_seq_pend_regra_w,
								nr_sequencia_p);
						commit;
											
						gerar_consulta_reg_mentor (nm_usuario_p, 'ATEND_PAC_DISPOSITIVO', nr_seq_pend_regra_w, nr_sequencia_p, nr_sequencia_w, nr_seq_dispositivo_w, nm_atributo_w);
						gqa_gerar_acao_regra(nr_seq_pend_regra_w,nr_sequencia_w,nr_atendimento_w,cd_pessoa_fisica_w,nm_usuario_p);
						
					end if;
				end if;
			end;
		end loop;
		close C01;
	end;
	end loop;
	close CursorDispositivo;
end GQA_Instalar_Dispositivo_Pac;
/

