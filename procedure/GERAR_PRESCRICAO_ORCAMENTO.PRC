create or replace 
procedure gerar_prescricao_orcamento(	nr_seq_orcamento_p	number,
					nr_prescricao_p		number,
					nm_usuario_p		varchar) is

nr_seq_exame_w			number(10,0);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
qt_procedimento_w		number(9,3);
cd_setor_atendimento_w		number(5);
cd_setor_atend_usuario_w	number(5);
cont_w				number(5);
nr_atendimento_w		number(10,0);
cd_setor_prescr_w		number(5);
nr_seq_prescr_w			number(10);
qt_min_atraso_w		    	number(10);
ie_atualizar_result_w		varchar2(1);
ie_atualizar_entrega_w		varchar2(1);
dt_resultado_w			date;
cd_material_exame_w		varchar2(20);
ie_autorizacao_w		varchar2(3);
nr_seq_proc_interno_w		number(10);
cd_estabelecimento_w		number(10);
dt_coleta_w			date;
cd_medico_w			varchar2(10);
cd_setor_atend_w		varchar2(255);
cd_setor_coleta_w		varchar2(255);
cd_setor_entrega_w		varchar2(255);

qt_dia_entrega_w		number(20);
qt_min_entrega_w		number(10);
ie_emite_mapa_w			varchar2(255);
ds_hora_fixa_w			varchar2(255);
ie_data_resultado_w		varchar2(255);
ie_atualizar_recoleta_w		varchar2(255);
ie_urgencia_w			varchar2(1);
ie_dia_semana_final_w 		number(10);

ie_medico_executor_w		varchar2(1);
cd_cgc_w				varchar2(14);
cd_medico_executor_w		varchar2(10);
cd_medico_exec_examLab_w	varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
ie_forma_atual_dt_result_w	exame_lab_regra_setor.ie_atul_data_result%type;
cd_setor_proc_w			setor_atendimento.cd_setor_atendimento%type;

cursor c01 is
	select	a.nr_seq_exame,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.qt_procedimento,
		b.cd_setor_exclusivo,
		a.ie_autorizacao,
		a.nr_seq_proc_interno,
		a.cd_medico,
		a.cd_setor_atendimento
	from	procedimento b,
		orcamento_paciente_proc a
	where 	a.cd_procedimento	= b.cd_procedimento
	  and 	a.ie_origem_proced	= b.ie_origem_proced
	  and 	a.nr_sequencia_orcamento = nr_seq_orcamento_p;
begin
cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
obter_param_usuario(916, 931, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_atualizar_result_w);
obter_param_usuario(916, 932, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_atualizar_entrega_w);

select	nvl(nr_atendimento,0),
	nvl(cd_setor_entrega,0)
into	nr_atendimento_w,
	cd_setor_prescr_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

select	count(*) 
into	cont_w
from	atend_paciente_unidade
where	nr_atendimento	= nvl(nr_atendimento_w,0);

if	(cont_w = 0) and 
	(cd_setor_prescr_w > 0) and 
	(nr_atendimento_w > 0) then
	Gerar_Passagem_Setor_Atend(nr_atendimento_w, 
				cd_setor_prescr_w, 
				sysdate, 
				'S',
				nm_usuario_p);
end if;

open c01;
loop
fetch c01 into	
	nr_seq_exame_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	qt_procedimento_w,
	cd_setor_atendimento_w,
	ie_autorizacao_w,
	nr_seq_proc_interno_w,
	cd_medico_w,
	cd_setor_proc_w;
exit when c01%notfound;

	select	nvl(max(nr_sequencia),0) + 1
	into	nr_seq_prescr_w
	from 	prescr_procedimento
	where 	nr_prescricao = nr_prescricao_p;

	if	(nr_seq_exame_w is not null) then
		select	max(b.cd_material_exame)
		into	cd_material_exame_w
		from	material_exame_lab b,
			exame_lab_material a
		where	a.nr_seq_material = b.nr_sequencia
		and	a.nr_seq_exame	= nr_seq_exame_w
		and	a.ie_situacao	= 'A'
		and	a.ie_prioridade	= (	select	min(c.ie_prioridade)
						from	exame_lab_material c
						where	c.nr_seq_material = b.nr_sequencia
						and	a.nr_seq_exame	= nr_seq_exame_w
						and	a.ie_situacao	= 'A');
						
		
	end if;

	If	(ie_atualizar_entrega_w = 'S') then
		cd_setor_atendimento_w := cd_setor_prescr_w;
	End if;

	if (cd_setor_atendimento_w is null) then
	    cd_setor_atendimento_w := cd_setor_proc_w;
	end if;
	
	if	(cd_setor_atendimento_w is null) then
		cd_setor_atendimento_w := obter_setor_atend_proc(	cd_estabelecimento_w, 
									cd_procedimento_w, 
									ie_origem_proced_w, 
									null, 
									null,
									null,
									nr_seq_proc_interno_w,
									nr_atendimento_w);
	end if;

	if	(cd_setor_atendimento_w is null) and
		(nr_seq_exame_w is not null) then

		cd_setor_atendimento_w := obter_setor_atend_proc_lab(	cd_estabelecimento_w,
									cd_procedimento_w,
									ie_origem_proced_w, 
									null,
									null,
									null,
									nr_seq_exame_w);
	end if;
	
	
	
	if	(cd_medico_w is null) then
		consiste_medico_executor(cd_estabelecimento_w,
					 obter_convenio_atendimento(nr_atendimento_w),
					 cd_setor_atendimento_w,
					 cd_procedimento_w,
					 ie_origem_proced_w,
					 Obter_Tipo_Atendimento(nr_atendimento_w),
					 nr_seq_exame_w,
					 nr_seq_proc_interno_w,
					 ie_medico_executor_w,
					 cd_cgc_w,
					 cd_medico_executor_w,
					 cd_pessoa_fisica_w,
					 '',
					 sysdate,
					 Obter_Classificacao_Atend(nr_atendimento_w,'C'),
					 'N',
					 null,
					 null);
					 
		If	(cd_medico_executor_w is not null) then
			cd_medico_w := cd_medico_executor_w;
		end if;
	end if;
	
	insert into prescr_procedimento (
		nr_prescricao,
		nr_sequencia,
		cd_procedimento,
		qt_procedimento,
		dt_atualizacao,
		nm_usuario,
		ds_horarios,
		ds_observacao,
		cd_motivo_baixa,
		dt_baixa,
		cd_procedimento_aih,
		ie_origem_proced,
		cd_intervalo,
		ie_urgencia,
		cd_setor_atendimento,
		dt_emissao_setor_atend,
		ds_dado_clinico,
		dt_prev_execucao,
		ie_suspenso,
		cd_material_exame,
		nr_seq_exame,
		ds_material_especial,
		ie_amostra,
		ie_status_atend,
		ie_origem_inf,
		cd_setor_coleta,
		cd_setor_entrega,
		ie_emite_mapa,
		dt_integracao,
		dt_resultado,
		ds_integracao,
		ie_executar_leito,
		cd_medico_exec,
		nr_agrupamento,
		ie_se_necessario,
		ie_acm,
		nr_ocorrencia,
		ie_externo,
		nr_seq_lote_externo,
		ds_rotina,
		nr_seq_interno,
		ie_avisar_result,
		ie_autorizacao,
		nr_seq_proc_interno)
	values (nr_prescricao_p,
		nr_seq_prescr_w,
		cd_procedimento_w,
		qt_procedimento_w,
		sysdate,
		nm_usuario_p,
		null,
		null,
		0,
		null,
		null,
		ie_origem_proced_w,
		null,
		'N',
		cd_setor_atendimento_w,
		null,
		null,
		decode(ie_atualizar_result_w, 'S', sysdate, null),
		'N',
		cd_material_exame_w,
		nr_seq_exame_w,
		null,
		'N',
		'10', /*Bruna, Alterei para 10*/		
		'1',
		cd_setor_atendimento_w, 
		cd_setor_atendimento_w,
		'N',
		null,
		null,
		null,
		'N',
		cd_medico_w,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		prescr_procedimento_seq.NextVal,
		'N',
		ie_autorizacao_w,
		nr_seq_proc_interno_w);		
		
	if	(ie_atualizar_result_w = 'S') then
		begin
			dt_resultado_w := Obter_data_resultado_exame(nr_seq_prescr_w, nr_prescricao_p);
		exception
			when 	no_data_found then
				dt_resultado_w := null;
		end;
		
		if	(dt_resultado_w is not null) then
			update	prescr_procedimento
			set	dt_resultado = dt_resultado_w
			where	nr_prescricao = nr_prescricao_p
			and	nr_sequencia = nr_seq_prescr_w;
		end if;		
	End if;
	
	cd_setor_coleta_w := null;
	cd_setor_entrega_w := null;
	cd_setor_atend_w := null;
	if	(nr_seq_exame_w is not null) then
	
		cd_setor_atend_usuario_w := obter_setor_usuario(nm_usuario_p);
	
		obter_setor_exame_lab(	nr_prescricao_p, 
					nr_seq_exame_w, 
					cd_setor_atend_usuario_w, 
					cd_material_exame_w,
					null,
					'S', 
					cd_setor_atend_w, 
					cd_setor_coleta_w, 
					cd_setor_entrega_w, 
					qt_dia_entrega_w, 
					ie_emite_mapa_w, 
					ds_hora_fixa_w, 
					ie_data_resultado_w, 
					qt_min_entrega_w, 
					ie_atualizar_recoleta_w, 
					'N',
					ie_dia_semana_final_w,
					ie_forma_atual_dt_result_w,
					qt_min_atraso_w);
		
		if	(cd_setor_coleta_w is not null) or
			(cd_setor_entrega_w is not null) or 
			(cd_setor_atend_w is not null) then
			update	prescr_procedimento
			set	cd_setor_coleta = nvl(to_number(replace(replace(cd_setor_coleta_w,'(',''), ')','')), cd_setor_coleta),
				cd_setor_entrega = nvl(to_number(replace(replace(cd_setor_entrega_w,'(',''), ')','')), cd_setor_entrega)/*,
				cd_setor_atendimento = nvl(to_number(replace(replace(cd_setor_atend_w,'(',''), ')','')), cd_setor_atendimento)*/
			where	nr_prescricao = nr_prescricao_p
			and	nr_sequencia = nr_seq_prescr_w;
		end if;
	end if;
	
end loop;
close c01;

if	(ie_atualizar_result_w = 'S') then	
	Ajustar_Data_Entrega_Maior(nr_prescricao_p, nm_usuario_p);
end if;

commit;

end gerar_prescricao_orcamento;
/ 
