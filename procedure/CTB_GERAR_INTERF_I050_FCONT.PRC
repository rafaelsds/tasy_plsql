create or replace
procedure ctb_gerar_interf_I050_fcont(	nr_seq_controle_p		number,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	Number,
				dt_inicio_p		date,
				dt_fim_p			date,
				cd_empresa_p		number,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is 

		
ds_arquivo_w			varchar2(4000);
ds_compl_arquivo_w		varchar2(4000);
ds_conta_contabil_w		varchar2(255);
ds_linha_w			varchar2(8000);
ds_identificador_w			varchar2(5);
cd_aglutinacao_w			varchar2(40);
cd_centro_custo_w			number(10);
cd_conta_contabil_w		varchar2(40);
cd_conta_contabil_ww		varchar2(20);
cd_conta_sup_w			varchar2(40);
qt_registros_w			number(10);
nr_sequencia_w			number(10) := 0;
nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w			number(10) := nr_sequencia_p;
cd_empresa_resp_w		number(5);
sep_w				varchar2(1) := '|';
tp_registro_w			varchar2(15);
ie_apres_conta_ctb_w		varchar2(02);
ie_gerar_I051_w			varchar2(01);
ie_gerar_I052_w			varchar2(01);
nr_doc_origem_w			varchar2(20);
nr_nivel_fcont_w		number(10);	
ie_conta_movto_w		varchar2(1);
ind				integer	:= 0;
ie_consolida_empresa_w		ctb_regra_sped.ie_consolida_empresa%type;	
	
cursor c01 is
	select	a.cd_grupo_ecd,
		a.ie_tipo_ecd,
		ctb_obter_nivel_classif_conta(substr(ctb_obter_classif_conta(a.cd_conta_contabil, a.cd_classificacao, dt_inicio_p),1,40)) nr_nivel,
		a.cd_conta_contabil,
		substr(ctb_obter_classif_conta_sup(substr(ctb_obter_classif_conta(a.cd_conta_contabil, a.cd_classificacao, dt_inicio_p),1,40), dt_inicio_p, a.cd_empresa),1,40) cd_classif_sup, 
		a.ds_conta_contabil,
		substr(ctb_obter_classif_conta(a.cd_conta_contabil, a.cd_classificacao, dt_inicio_p),1,40) cd_classificacao,
		1 nr_nivel_fcont
	from	ecd_plano_conta_v a
	where	a.tp_registro	= 1
	and	a.cd_empresa	= cd_empresa_p
	and	a.ie_situacao = 'A'
	and	((substr(OBTER_SE_CONTA_VIGENTE2(a.cd_conta_contabil, a.dt_inicio_vigencia, a.dt_fim_vigencia,dt_inicio_p),1,1) = 'S')
	or	(substr(OBTER_SE_CONTA_VIGENTE2(a.cd_conta_contabil, a.dt_inicio_vigencia, a.dt_fim_vigencia,dt_fim_p),1,1) = 'S'))
	and	((ie_conta_movto_w = 'N')
	or	(exists(	select	1
			from	ctb_mes_ref x,
				ctb_saldo y
			where	x.nr_sequencia		= y.nr_seq_mes_ref
			and	x.cd_empresa		= a.cd_empresa
			and	y.cd_estabelecimento	= cd_estabelecimento_p
			and	a.cd_conta_contabil	= y.cd_conta_contabil
			and	x.dt_referencia between dt_inicio_p and dt_fim_p
			and ie_conta_movto_w = 'S')))
	and	ie_consolida_empresa_w	= 'N'
	union
	select	a.cd_grupo_ecd,
		a.ie_tipo_ecd,
		ctb_obter_nivel_classif_conta(substr(ctb_obter_classif_conta(a.cd_conta_contabil, a.cd_classificacao, dt_inicio_p),1,40)) nr_nivel,
		a.cd_conta_contabil,
		substr(ctb_obter_classif_conta_sup(substr(ctb_obter_classif_conta(a.cd_conta_contabil, a.cd_classificacao, dt_inicio_p),1,40), dt_inicio_p, a.cd_empresa),1,40) cd_classif_sup, 
		a.ds_conta_contabil,
		substr(ctb_obter_classif_conta(a.cd_conta_contabil, a.cd_classificacao, dt_inicio_p),1,40) cd_classificacao,
		1 nr_nivel_fcont
	from	ecd_plano_conta_v a
	where	a.tp_registro	= 1
	and	a.cd_empresa	= cd_empresa_p
	and	a.ie_situacao = 'A'
	and	((substr(OBTER_SE_CONTA_VIGENTE2(a.cd_conta_contabil, a.dt_inicio_vigencia, a.dt_fim_vigencia,dt_inicio_p),1,1) = 'S')
	or	(substr(OBTER_SE_CONTA_VIGENTE2(a.cd_conta_contabil, a.dt_inicio_vigencia, a.dt_fim_vigencia,dt_fim_p),1,1) = 'S'))
	and	((ie_conta_movto_w = 'N')
	or	(exists(	select	1
			from	ctb_mes_ref x,
				ctb_saldo y,
				estabelecimento z
			where	x.nr_sequencia		= y.nr_seq_mes_ref
			and	x.cd_empresa		= a.cd_empresa--and	y.cd_estabelecimento	= cd_estabelecimento_p
			and	z.cd_estabelecimento	= y.cd_estabelecimento
			and	nvl(z.ie_gerar_sped,'S') = 'S'
			and	a.cd_conta_contabil	= y.cd_conta_contabil
			and	x.dt_referencia between dt_inicio_p and dt_fim_p
			and ie_conta_movto_w = 'S')))
	and	ie_consolida_empresa_w	= 'S'
	order by cd_classificacao;

vet01	C01%RowType;

type reg_contas is table of C01%RowType index by binary_integer;

contas_w	reg_contas;

cursor c02 is
select	a.cd_centro_custo	cd_centro_custo,
	a.cd_classif_ecd
from	ecd_plano_conta_v a
where	a.tp_registro		= 2
and	a.cd_empresa		= cd_empresa_p
and	a.cd_conta_contabil	= cd_conta_contabil_w
and	a.cd_classif_ecd is not null
and	ie_gerar_I051_w		= 'S'
and	substr(obter_se_periodo_vigente(a.dt_inicio_validade, a.dt_fim_validade, dt_inicio_p),1,1) = 'S'
and 	cd_versao is null
order by cd_classificacao;

vet02	C02%RowType;


begin
select	nvl(max(a.cd_empresa_resp),0),
	nvl(max(a.ie_apres_conta_ctb),'CD'),
	nvl(max(a.ie_conta_movto),'N'),
	nvl(max(a.ie_consolida_empresa), 'N')
into	cd_empresa_resp_w,
	ie_apres_conta_ctb_w,
	ie_conta_movto_w,
	ie_consolida_empresa_w
from	ctb_regra_sped a,
	ctb_sped_controle b
where	b.nr_seq_regra_sped 	= a.nr_sequencia
and	b.nr_sequencia		= nr_seq_controle_p;

select	nvl(max(ie_gerar),'N')
into	ie_gerar_I051_w
from	ctb_regra_sped_registro a,
	ctb_sped_controle b
where	a.nr_seq_regra_sped	= b.nr_seq_regra_sped
and	b.nr_sequencia		= nr_seq_controle_p
and	a.cd_registro		= 'I051';


open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	ind	:= ind + 1;
	nr_nivel_fcont_w		:= 1;
	contas_w(ind).cd_grupo_ecd	:= vet01.cd_grupo_ecd;
	contas_w(ind).ie_tipo_ecd	:= vet01.ie_tipo_ecd;
	contas_w(ind).nr_nivel		:= vet01.nr_nivel;
	contas_w(ind).cd_conta_contabil	:= vet01.cd_conta_contabil;
	contas_w(ind).cd_classif_sup	:= vet01.cd_classif_sup;
	contas_w(ind).ds_conta_contabil	:= vet01.ds_conta_contabil;
	contas_w(ind).cd_classificacao	:= vet01.cd_classificacao;
	contas_w(ind).nr_nivel_fcont	:= 0;
	
	if	(contas_w(ind).nr_nivel = 1) then
		contas_w(ind).nr_nivel_fcont	:= 1;
	end if;
	
	end;
end loop;
close C01;	

for i in 1..contas_w.Count loop
	begin
	tp_registro_w		:= 'I050';
	cd_conta_contabil_w	:= contas_w(i).cd_conta_contabil;
	cd_conta_contabil_ww	:= contas_w(i).cd_conta_contabil;
	cd_conta_sup_w		:= '';
	nr_doc_origem_w		:= contas_w(i).cd_conta_contabil;

	nr_nivel_fcont_w := 1;
	
	if	(contas_w(i).nr_nivel > 1) then
		
		for ind in 1..contas_w.Count loop
			begin
			if	(contas_w(ind).cd_classificacao = contas_w(i).cd_classif_sup) then
				nr_nivel_fcont_w	:= contas_w(ind).nr_nivel_fcont + 1;
				exit;
			else
				nr_nivel_fcont_w	:= contas_w(ind).nr_nivel_fcont;
			end if;
			end;
		end loop;
		
		contas_w(i).nr_nivel_fcont	:= nr_nivel_fcont_w;
	end if;
	
	if	(ie_apres_conta_ctb_w = 'CD') then
		begin
		if	(contas_w(i).nr_nivel > 1) then
			cd_conta_sup_w	:= ctb_obter_codigo_classif_vig(cd_empresa_p, contas_w(i).cd_classif_sup,dt_inicio_p);
		end if;
		end;
	elsif	(ie_apres_conta_ctb_w = 'CL') then
		begin
		cd_conta_contabil_w	:= contas_w(i).cd_classificacao;
		cd_conta_sup_w		:= contas_w(i).cd_classif_sup;
		end;
	elsif	(ie_apres_conta_ctb_w = 'CP') then
		begin
		cd_conta_contabil_w	:= substr(replace(contas_w(i).cd_classificacao,'.',''),1,40);
		cd_conta_sup_w		:= substr(replace(contas_w(i).cd_classif_sup,'.',''),1,40);
		end;
	end if;
	/*
	if (ctb_obter_nivel_conta(cd_conta_contabil_w) = 1) then
		nr_nivel_fcont_w	:= ctb_obter_nivel_conta(cd_conta_contabil_w);
	end if;	
	*/
	ds_conta_contabil_w := substr(contas_w(i).ds_conta_contabil,1,255);
	
	select	count(*)
	into	qt_registros_w
	from	conta_contabil_ans
	where	cd_conta_contabil = contas_w(i).cd_conta_contabil;

	if (qt_registros_w > 0) then
   	  	
		select	max(nr_sequencia)
		into	nr_sequencia_w
		from	conta_contabil_ans
		where	cd_conta_contabil = contas_w(i).cd_conta_contabil;

		select	nvl(substr(obter_desc_plano_versao_ans(cd_plano, cd_empresa, ie_versao),1,255),ds_conta_contabil_w)
		into	ds_conta_contabil_w
		from 	conta_contabil_ans 
		where 	nr_sequencia = nr_sequencia_w;
	end if;	
	
	ds_linha_w	:= substr(	sep_w || 	tp_registro_w				||
				sep_w || 	substr(to_char(dt_inicio_p,'ddmmyyyy'),1,8)	||
				sep_w || 	contas_w(i).cd_grupo_ecd			||
				sep_w || 	contas_w(i).ie_tipo_ecd				||
				sep_w || 	nr_nivel_fcont_w				||
				sep_w || 	cd_conta_contabil_w			||
				sep_w || 	cd_conta_sup_w				||
				sep_w || 	ds_conta_contabil_w			|| sep_w, 1, 8000);
	
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;
	
	insert into ctb_sped_registro(
		nr_sequencia,                   
		ds_arquivo,                     
		dt_atualizacao,                 
		nm_usuario,                     
		dt_atualizacao_nrec,            
		nm_usuario_nrec,                
		nr_seq_controle_sped,           
		ds_arquivo_compl,               
		cd_registro,
		nr_linha,
		nr_doc_origem)
	values(	nr_seq_registro_w,
		ds_arquivo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,	
		ds_compl_arquivo_w,
		tp_registro_w,
		nr_linha_w,
		nr_doc_origem_w);
		
	tp_registro_w :=	'I051';
	cd_conta_contabil_w	:= contas_w(i).cd_conta_contabil;
	open C02;
	loop
	fetch C02 into
		vet02;
	exit when C02%notfound;
		begin
		
		ds_linha_w	:= substr(	sep_w || tp_registro_w 		|| 
						sep_w || cd_empresa_resp_w	 	|| 
						sep_w || vet02.cd_centro_custo	|| 
						sep_w || vet02.cd_classif_ecd 	|| sep_w,1,8000);
	
		ds_arquivo_w		:= substr(ds_linha_w,1,4000);
		ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		nr_linha_w		:= nr_linha_w + 1;
		
		insert into ctb_sped_registro(
			nr_sequencia,                   
			ds_arquivo,                     
			dt_atualizacao,                 
			nm_usuario,                     
			dt_atualizacao_nrec,            
			nm_usuario_nrec,                
			nr_seq_controle_sped,           
			ds_arquivo_compl,               
			cd_registro,
			nr_linha,
			nr_doc_origem)
		values(	nr_seq_registro_w,
			ds_arquivo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_controle_p,
			ds_compl_arquivo_w,
			tp_registro_w,
			nr_linha_w,
			nr_doc_origem_w);
		end;
	end loop;
	close C02;
	end;
end loop;

commit;
qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end ctb_gerar_interf_I050_fcont;
/