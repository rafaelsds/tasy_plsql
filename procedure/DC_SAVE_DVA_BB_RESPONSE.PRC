create or replace procedure dc_save_dva_bb_response(
						  ie_type_report_p            		varchar2,
						  nr_seq_transaction_p        		number,
						  vl_charge_amount_p            	number,
						  vl_claim_benefit_paid_p        	number,
						  vl_service_benefit_amount_p    	number,
						  nr_invoice_p                		number,
						  dt_servide_p                		date,
						  ie_status_p                    	number,
						  nm_usuario_p        				varchar2,
						  cd_claim_p 						varchar2,
						  ds_explanation_text_p 			varchar2,
						  ds_message_p 						clob,
						  nr_item_p 						number)  is



cd_claim_w 				varchar2(10);
nr_invoice_w 			number(10) := 0;
nr_item_w 				number(5) := 0 ;

begin

select	nvl(max(cd_claim),'NULL'),
		nvl(max(nr_dc_invoice),0),
		nvl(max(nr_item),0)
into	cd_claim_w,
		nr_invoice_w,
		nr_item_w
from	dc_reports
where	nr_item			= nr_item_p
and 	nr_dc_invoice	= nr_invoice_p
and		cd_claim		= cd_claim_p;

if	(cd_claim_p <> cd_claim_w) and
	(nr_invoice_p <> nr_invoice_w) and
	(nr_item_p <> nr_item_w) then

	begin

	insert into dc_reports (
							cd_claim,
							ds_explanation_text,
							ds_message,
							dt_atualizacao,
							dt_atualizacao_nrec,
							dt_servide,
							ie_status_claim,
							ie_type_report,
							nm_usuario,
							nm_usuario_nrec,
							nr_dc_invoice,
							nr_item,
							nr_sequencia,
							nr_seq_transaction,
							vl_charge_amount,
							vl_claim_benefit_paid,
							vl_service_benefit_amount,
							ie_status)
	values      (
							 cd_claim_p,
							 ds_explanation_text_p,
							 ds_message_p,
							 sysdate,
							 sysdate,
							 dt_servide_p,
							 ie_status_p,
							 ie_type_report_p,
							 nm_usuario_p,
							 nm_usuario_p,
							 nr_invoice_p,
							 nr_item_p,
							 dc_reports_seq.nextval,
							 nr_seq_transaction_p,
							 vl_charge_amount_p,
							 vl_claim_benefit_paid_p,
							 vl_service_benefit_amount_p,
							 'P');

	commit;

	end;
else
	begin

	update	dc_reports
	set		ds_explanation_text		    =   ds_explanation_text_p,
			ds_message 					=   ds_message_p,
			dt_atualizacao 				=   sysdate,
			dt_servide 					=   dt_servide_p,
			ie_status_claim 			=   ie_status_p,
			ie_type_report 				=   ie_type_report_p,
			nm_usuario 			        =   nm_usuario_p,
			nr_seq_transaction 			=   nr_seq_transaction_p,
			vl_charge_amount 			=   vl_charge_amount_p,
			vl_claim_benefit_paid 	    =   vl_claim_benefit_paid_p,
			vl_service_benefit_amount 	=   vl_service_benefit_amount_p
	where	nr_item 					=   nr_item_p
	and 	nr_dc_invoice 				=   nr_invoice_p
	and		cd_claim 					=   cd_claim_p;

	commit;

	end;
end if;


commit;
end dc_save_dva_bb_response;
/