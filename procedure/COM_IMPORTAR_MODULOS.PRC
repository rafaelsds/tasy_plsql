create or replace
procedure com_importar_modulos(	nr_seq_cliente_p	number,
				nm_usuario_p		Varchar2) is 

nr_seq_modulo_w		number(10);

Cursor C01 is
	select	a.nr_sequencia
	from	modulo_implantacao a
	where	a.ie_comercial = 'S'
	and	not exists (	select	1
				from	com_cliente_mod_impl x
				where	x.nr_seq_modulo = a.nr_sequencia
				and	x.nr_seq_cliente	= nr_seq_cliente_p)
	order by nm_modulo;
	
begin

open C01;
loop
fetch C01 into	
	nr_seq_modulo_w;
exit when C01%notfound;
	begin
	
	insert into com_cliente_mod_impl (	NR_SEQUENCIA,
						NR_SEQ_CLIENTE,
						DT_ATUALIZACAO,
						NM_USUARIO,
						NR_SEQ_MODULO,
						PR_UTILIZACAO)
					values( com_cliente_mod_impl_seq.nextVal,
						nr_seq_cliente_p,
						sysdate,
						nm_usuario_p,
						nr_seq_modulo_w,
						0);
	
	end;
end loop;
close C01;

commit;

end com_importar_modulos;
/