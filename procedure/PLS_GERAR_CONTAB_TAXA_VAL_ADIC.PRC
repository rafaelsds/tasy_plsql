create or replace
procedure pls_gerar_contab_taxa_val_adic(	nr_seq_conta_p		pls_conta.nr_sequencia%type,
						nm_usuario_p		usuario.nm_usuario%type) is 
				
	
pr_pagamento_liberado_w		number(15,10);
vl_contab_taxa_w			number(15,2);
vl_diferenca_w				Number(15,2);
nr_seq_contab_w				pls_conta_copartic_contab.nr_sequencia%type;
vl_validacao_w				Number(15,2);
nr_seq_conta_contab_taxa_w	pls_conta_pos_taxa_contab.nr_sequencia%type;
vl_taxa_manutencao_w		pls_conta_pos_estab_taxa.vl_taxa_manutencao%type;

Cursor C01 (	nr_seq_conta_pc		pls_conta_medica_resumo.nr_seq_conta%type) is
	select	max(a.nr_sequencia) nr_seq_conta_resumo,
		a.nr_seq_conta,
		sum(a.vl_lib_original) vl_lib_pagamento,
		a.nr_seq_conta_mat,        
		a.nr_seq_conta_proc,
		a.nr_seq_prestador_pgto,
		max(a.nr_seq_participante) nr_seq_participante,
		sum(vl_hm) vl_hm,
		sum(vl_materiais_pag) vl_materiais_pag,
		sum(vl_co_pag) vl_co_pag,
		max(ie_tipo_item) ie_tipo_item
	from	pls_conta_medica_resumo a
	where	a.nr_seq_conta	= nr_seq_conta_pc 
	and	a.ie_tipo_item 	!= 'I'
	and	a.ie_situacao 	= 'A'
	--and	a.nr_seq_exame_coleta is null
	group by a.nr_seq_conta,
		 a.nr_seq_conta_mat,
		 a.nr_seq_conta_proc,
		 a.nr_seq_prestador_pgto;

Cursor C02 (	nr_seq_conta_proc_pc	pls_conta_medica_resumo.nr_seq_conta_proc%type,
		nr_seq_conta_mat_pc	pls_conta_medica_resumo.nr_seq_conta_mat%type) is
	select	a.nr_sequencia nr_seq_conta_estab_taxa,
		a.vl_taxa_manutencao,
		c.vl_liberado
	from	pls_conta_pos_estab_taxa	a,
		pls_conta_pos_estabelecido	b,
		pls_conta_proc			c
	where	a.nr_seq_conta_pos_estab	= b.nr_sequencia
	and	b.nr_seq_conta_proc		= c.nr_sequencia
	and	c.nr_sequencia			= nr_seq_conta_proc_pc
	and	b.ie_status_faturamento		!= 'A'
	UNION ALL
	select	a.nr_sequencia nr_seq_conta_estab_taxa,
		a.vl_taxa_manutencao,
		c.vl_liberado
	from	pls_conta_pos_estab_taxa	a,
		pls_conta_pos_estabelecido	b,
		pls_conta_mat			c
	where	a.nr_seq_conta_pos_estab	= b.nr_sequencia
	and	b.nr_seq_conta_mat		= c.nr_sequencia
	and	c.nr_sequencia			= nr_seq_conta_mat_pc
	and	b.ie_status_faturamento		!= 'A';

Cursor C03 (nr_seq_conta_pc		pls_conta_medica_resumo.nr_seq_conta%type) is
	select	nr_seq_pos_estab_taxa,
		sum(vl_taxa) vl_contab
	from	pls_conta_pos_taxa_contab
	where	nr_seq_conta	= nr_seq_conta_pc
	group by nr_seq_pos_estab_taxa;

Cursor C04 (	nr_seq_conta_pc		pls_conta_medica_resumo.nr_seq_conta%type) is
	select	null nr_seq_conta_resumo,
		a.nr_seq_conta,
		a.vl_liberado vl_lib_pagamento,
		null nr_seq_conta_mat,        
		a.nr_sequencia nr_seq_conta_proc,
		null nr_seq_prestador_pgto
	from	pls_conta_proc_v a
	where	a.nr_seq_conta	= nr_seq_conta_pc
	and	a.ie_tipo_conta	= 'I'
	and	a.ie_status	!= 'M'
	union all
	select	null nr_seq_conta_resumo,
		a.nr_seq_conta,
		a.vl_liberado vl_lib_pagamento,
		a.nr_sequencia nr_seq_conta_mat,        
		null nr_seq_conta_proc,
		null nr_seq_prestador_pgto
	from	pls_conta_mat_v a
	where	a.nr_seq_conta	= nr_seq_conta_pc
	and	a.ie_tipo_conta	= 'I'
	and	a.ie_status	!= 'M'
	union all
	select	null nr_seq_conta_resumo,
		a.nr_seq_conta,
		a.vl_liberado vl_lib_pagamento,
		a.nr_sequencia nr_seq_conta_mat,        
		null nr_seq_conta_proc,
		null nr_seq_prestador_pgto
	from	pls_conta_mat_v a
	where	a.nr_seq_conta	= nr_seq_conta_pc
	and	a.ie_status	= 'M'
	and	not exists (select	1
			    from	pls_conta_medica_resumo r
			    where	r.nr_seq_conta 		= a.nr_seq_conta
			    and		r.nr_seq_conta_mat 	= a.nr_sequencia)
	union all
	select	null nr_seq_conta_resumo,
		a.nr_seq_conta,
		a.vl_liberado vl_lib_pagamento,
		null nr_seq_conta_mat,        
		a.nr_sequencia nr_seq_conta_proc,
		null nr_seq_prestador_pgto
	from	pls_conta_proc_v a
	where	a.nr_seq_conta	= nr_seq_conta_pc
	and	a.ie_status	= 'M'
	and	not exists (select	1
			    from	pls_conta_medica_resumo r
			    where	r.nr_seq_conta 		= a.nr_seq_conta
			    and		r.nr_seq_conta_proc 	= a.nr_sequencia)
	union all
	select	null nr_seq_conta_resumo,
		a.nr_seq_conta,
		a.vl_liberado vl_lib_pagamento,
		a.nr_sequencia nr_seq_conta_mat,        
		null nr_seq_conta_proc,
		null nr_seq_prestador_pgto
	from	pls_conta_mat_v a
	where	a.nr_seq_conta	= nr_seq_conta_pc
	and	a.ie_tipo_protocolo	= 'R'
	union all
	select	null nr_seq_conta_resumo,
		a.nr_seq_conta,
		a.vl_liberado vl_lib_pagamento,
		null nr_seq_conta_mat,        
		a.nr_sequencia nr_seq_conta_proc,
		null nr_seq_prestador_pgto
	from	pls_conta_proc_v a
	where	a.nr_seq_conta	= nr_seq_conta_pc
	and	a.ie_tipo_protocolo	= 'R'
	union all
	select	null nr_seq_conta_resumo,
		a.nr_seq_conta,
		a.vl_liberado vl_lib_pagamento,
		null nr_seq_conta_mat,        
		a.nr_sequencia nr_seq_conta_proc,
		null nr_seq_prestador_pgto
	from	pls_conta_proc_v a
	where	a.nr_seq_conta	= nr_seq_conta_pc
	and	 exists (select	1
			    from	pls_conta_rec_resumo_item r,
						pls_rec_glosa_proc x
			    where	a.nr_sequencia		= x.nr_seq_conta_proc
			    and		x.nr_sequencia 	= r.nr_seq_proc_rec)
	union all
	select	null nr_seq_conta_resumo,
		a.nr_seq_conta,
		a.vl_liberado vl_lib_pagamento,
		a.nr_sequencia nr_seq_conta_mat,        
		null nr_seq_conta_proc,
		null nr_seq_prestador_pgto
	from	pls_conta_mat_v a
	where	a.nr_seq_conta	= nr_seq_conta_pc
	and	 exists (select	1
			    from	pls_conta_rec_resumo_item r,
						pls_rec_glosa_mat x
			    where	a.nr_sequencia		= x.nr_seq_conta_mat
			    and		x.nr_sequencia 	= r.nr_seq_mat_rec);
				
begin
for r_c01_w in C01(nr_seq_conta_p) loop
	begin
	
	for r_c02_w in C02(r_c01_w.nr_seq_conta_proc,r_c01_w.nr_seq_conta_mat) loop
		begin
		
		--Existem casos que n�o tem valor liberado, por�m tem coparticipa��o, ent�o o percentual de pagamento � 1
		if	(r_c01_w.vl_lib_pagamento <> 0) then
			/* Obter o percentual do valor pago em rela��o ao liberado do item da conta m�dica */
			pr_pagamento_liberado_w	:= round(dividir_sem_round( r_c01_w.vl_lib_pagamento, r_c02_w.vl_liberado ),10);
		else
			pr_pagamento_liberado_w	:= 1;
		end if;
		
		/* Aplicar o percentual obtido sobre o valor de coparticipa��o */
		vl_contab_taxa_w	:= round(r_c02_w.vl_taxa_manutencao * pr_pagamento_liberado_w,2);
		
		nr_seq_conta_contab_taxa_w	:= null;
		
		select	max(nr_sequencia)
		into	nr_seq_conta_contab_taxa_w
		from	pls_conta_pos_taxa_contab
		where	nr_seq_conta		= nr_seq_conta_p
		and	nr_seq_conta_resumo 	= r_c01_w.nr_seq_conta_resumo;
			
		if	(nr_seq_conta_contab_taxa_w is null) and
			(r_c01_w.nr_seq_conta_resumo is null) then -- N�o vai ter conta m�dica resumo apenas se a conta for de origem interc�mbio
			select	max(nr_sequencia)
			into	nr_seq_conta_contab_taxa_w
			from	pls_conta_pos_taxa_contab
			where	nr_seq_pos_estab_taxa	= r_c02_w.nr_seq_conta_estab_taxa;
		end if;
		
		if	(nr_seq_conta_contab_taxa_w	is null) then
			insert into pls_conta_pos_taxa_contab
				(	nr_sequencia, dt_atualizacao, nm_usuario, 
					dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_conta_resumo, 
					nr_seq_pos_estab_taxa,nr_seq_prestador_pgto,vl_taxa,nr_seq_conta,
					dt_competencia, vl_provisao)
			values	(	pls_conta_pos_taxa_contab_seq.NextVal, sysdate, nm_usuario_p,
						sysdate, nm_usuario_p, r_c01_w.nr_seq_conta_resumo, 
						r_c02_w.nr_seq_conta_estab_taxa, r_c01_w.nr_seq_prestador_pgto, vl_contab_taxa_w,nr_seq_conta_p,
						trunc(sysdate,'MM'), vl_contab_taxa_w);
					
		else
			update	pls_conta_pos_taxa_contab
			set	nr_seq_conta_resumo	= r_c01_w.nr_seq_conta_resumo, 
				vl_taxa		= vl_contab_taxa_w,
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate,
				dt_competencia = trunc(sysdate,'MM')
			where	nr_sequencia		= nr_seq_conta_contab_taxa_w;
			
		end if;
				
		end;
	end loop;
		
	end;
end loop;

for r_c04_w in C04(nr_seq_conta_p) loop
	begin

	for r_c02_w in C02(r_c04_w.nr_seq_conta_proc,r_c04_w.nr_seq_conta_mat) loop
		begin
		
		--Existem casos que n�o tem valor liberado, por�m tem coparticipa��o, ent�o o percentual de pagamento � 1
		if	(r_c04_w.vl_lib_pagamento <> 0) then
			/* Obter o percentual do valor pago em rela��o ao liberado do item da conta m�dica */
			pr_pagamento_liberado_w	:= round(dividir_sem_round(r_c04_w.vl_lib_pagamento,r_c02_w.vl_liberado),10);
		else
			pr_pagamento_liberado_w	:= 1;
		end if;
		
		/* Aplicar o percentual obtido sobre o valor de coparticipa��o */
		vl_contab_taxa_w	:= round(r_c02_w.vl_taxa_manutencao * pr_pagamento_liberado_w,2);
		
		select	max(nr_sequencia)
		into	nr_seq_conta_contab_taxa_w
		from	pls_conta_pos_taxa_contab
		where	nr_seq_conta		= nr_seq_conta_p
		and	nr_seq_pos_estab_taxa	= r_c02_w.nr_seq_conta_estab_taxa;
		
		if	(nr_seq_conta_contab_taxa_w	is null) then
			insert into pls_conta_pos_taxa_contab
				(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, 
					nr_seq_pos_estab_taxa,vl_taxa,nr_seq_conta, 
					dt_competencia, vl_provisao)
			values	(	pls_conta_pos_taxa_contab_seq.NextVal, sysdate, nm_usuario_p, sysdate, nm_usuario_p, 
					r_c02_w.nr_seq_conta_estab_taxa, vl_contab_taxa_w,nr_seq_conta_p,
					trunc(sysdate, 'MM'), vl_contab_taxa_w);
					
		else
			update	pls_conta_pos_taxa_contab
			set	vl_taxa		= vl_contab_taxa_w,
				nm_usuario	= nm_usuario_p,
				dt_atualizacao	= sysdate,
				dt_competencia = trunc(sysdate, 'MM')
			where	nr_sequencia	= nr_seq_conta_contab_taxa_w;
		
		end if;
				
		end;
	end loop;
		
	end;
end loop;

for r_c03_w in C03(nr_seq_conta_p) loop
	begin
	select	sum(vl_taxa_manutencao)
	into	vl_taxa_manutencao_w
	from	pls_conta_pos_estab_taxa
	where	nr_sequencia	= r_c03_w.nr_seq_pos_estab_taxa;
	
	if	(nvl(vl_taxa_manutencao_w,0)	!= nvl(r_c03_w.vl_contab,0)) then
		vl_diferenca_w	:= nvl(vl_taxa_manutencao_w,0) - nvl(r_c03_w.vl_contab,0);
		
		if	(vl_diferenca_w	> 0) then
			select	max(nr_sequencia)
			into	nr_seq_conta_contab_taxa_w
			from	pls_conta_pos_taxa_contab
			where	nr_seq_pos_estab_taxa	= r_c03_w.nr_seq_pos_estab_taxa
			and	vl_taxa			> vl_diferenca_w;
			
			if	(nr_seq_conta_contab_taxa_w is null) then
				select	max(nr_sequencia)
				into	nr_seq_conta_contab_taxa_w
				from	pls_conta_pos_taxa_contab
				where	nr_seq_pos_estab_taxa	= r_c03_w.nr_seq_pos_estab_taxa;
			end if;
			
			update	pls_conta_pos_taxa_contab
			set 	vl_taxa		= nvl(vl_taxa,0) + vl_diferenca_w,
					dt_competencia = trunc(sysdate, 'MM')
			where	nr_sequencia 	= nr_seq_conta_contab_taxa_w;
			
			select	sum(vl_taxa)
			into	vl_validacao_w
			from	pls_conta_pos_taxa_contab
			where	nr_seq_pos_estab_taxa	= r_c03_w.nr_seq_pos_estab_taxa;
			
			if	(nvl(vl_taxa_manutencao_w,0)	!= nvl(vl_validacao_w,0)) then
				wheb_mensagem_pck.exibir_mensagem_abort(378414,	';VL_ADIC=' || vl_taxa_manutencao_w ||';' || 'VL_CONTABIL=' ||vl_validacao_w||
										';NR_SEQ_CONTA='||nr_seq_conta_p||';NR_SEQ_POS_ESTAB_TAXA='||r_c03_w.nr_seq_pos_estab_taxa);

			end if;
		else
			
			select	max(nr_sequencia)
			into	nr_seq_conta_contab_taxa_w
			from	pls_conta_pos_taxa_contab
			where	nr_seq_pos_estab_taxa	= r_c03_w.nr_seq_pos_estab_taxa
			and	vl_taxa	> 0;
			
			if	(nr_seq_conta_contab_taxa_w is null) then
				select	max(nr_sequencia)
				into	nr_seq_conta_contab_taxa_w
				from	pls_conta_pos_taxa_contab
				where	nr_seq_pos_estab_taxa	= r_c03_w.nr_seq_pos_estab_taxa;
			end if;
			
			if	(vl_diferenca_w	> 0) THEN
				update	pls_conta_pos_taxa_contab
				set 	vl_taxa		= nvl(vl_taxa,0) - vl_diferenca_w,
						dt_competencia = trunc(sysdate, 'MM')
				where	nr_sequencia 	= nr_seq_conta_contab_taxa_w;
								
			else
				update	pls_conta_pos_taxa_contab
				set 	vl_taxa		= nvl(vl_taxa,0) + vl_diferenca_w,
						dt_competencia = trunc(sysdate, 'MM')
				where	nr_sequencia 	= nr_seq_conta_contab_taxa_w;
								
			end if;
				
			select	sum(vl_taxa)
			into	vl_validacao_w
			from	pls_conta_pos_taxa_contab
			where	nr_seq_pos_estab_taxa	= r_c03_w.nr_seq_pos_estab_taxa;
			
			if	(nvl(vl_taxa_manutencao_w,0)	!= nvl(vl_validacao_w,0)) then
				wheb_mensagem_pck.exibir_mensagem_abort(378414,	';VL_ADIC=' || vl_taxa_manutencao_w ||';' || 'VL_CONTABIL=' ||vl_validacao_w||
									';NR_SEQ_CONTA='||nr_seq_conta_p||';NR_SEQ_POS_ESTAB_TAXA='||r_c03_w.nr_seq_pos_estab_taxa);

			end if;
		end if;
	end if;
	
	end;
end loop;

end pls_gerar_contab_taxa_val_adic;
/