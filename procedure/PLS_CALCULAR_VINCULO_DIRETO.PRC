create or replace
procedure pls_calcular_vinculo_direto
                        (       nr_seq_conta_p          in      Number,
                                nr_seq_conta_proc_p     in      number,
                                ie_gravar_log_p         in      varchar2,
                                nm_usuario_p            in      Varchar2,
                                vl_medico_p             out     number,
                                vl_anestesista_p        out     number,
                                vl_auxiliares_p         out     number,
                                vl_custo_operacional_p  out     number,
                                vl_materiais_p          out     number,
                                vl_procedimento_p       out     number,
                                vl_total_procedimento_p out     number) is

/* Rotina utilizada para o cadastro de vinculo direto localizada na fun��o OPS - Cadastro de Regras */

nr_seq_proc_w                   Number(10);
cd_procedimento_w               Number(15);
ie_origem_proced_w              Number(10);
nr_seq_vinculo_w                Number(10);
pr_medico_w                     Number(7,4);
pr_anestesista_w                Number(7,4);
pr_auxiliares_w                 Number(7,4);
pr_custo_operacional_w          Number(7,4);
pr_materiais_w                  Number(7,4);
vl_medico_w                     Number(15,4)    := 0;
vl_anestesista_w                Number(15,4)    := 0;
vl_auxiliares_w                 Number(15,4)    := 0;
vl_custo_operacional_w          Number(15,4)    := 0;
vl_materiais_w                  Number(15,4)    := 0;
vl_medico_ww                    Number(15,4)    := 0;
vl_anestesista_ww               Number(15,4)    := 0;
vl_auxiliares_ww                Number(15,4)    := 0;
vl_custo_operacional_ww         Number(15,4)    := 0;
vl_materiais_ww                 Number(15,4)    := 0;
vl_total_procedimento_w         Number(15,4)    := 0;
pr_medico_ww                    Number(7,4);
pr_anestesista_ww               Number(7,4);
pr_auxiliares_ww                Number(7,4);
pr_custo_operacional_ww         Number(7,4);
pr_materiais_ww                 Number(7,4);
nr_seq_vinculo_regra_w          Number(10);
ie_tipo_despesa_w               Varchar2(10);
ie_tipo_despesa_mat_w           Varchar2(10);
cd_estabelecimento_w            Number(10);
vl_procedimento_w               number(15,2);
ie_gravar_log_w                 varchar2(1)     := 'N';
ds_observacao_log_w             varchar2(2000);
cd_proc_vinculo_w               number(15);
ie_origem_proc_vinculo_w        number(10);
ie_tipo_conta_w                 Varchar2(10);
qt_regra_vinculo_w		pls_integer;

Cursor C01 is
        select  a.nr_sequencia,
                a.cd_procedimento,
                a.ie_origem_proced,
                b.nr_sequencia,
                b.pr_medico,
                b.pr_anestesista,
                b.pr_auxiliares,
                b.pr_custo_operacional,
                b.pr_materiais
        from    pls_vinculo_direto      b,
                pls_conta_proc          a
        where   a.nr_seq_conta          = nr_seq_conta_p
        and     b.ie_situacao           = 'A'
        and     a.cd_procedimento       = b.cd_procedimento
        and     a.ie_origem_proced      = b.ie_origem_proced
        and     b.cd_estabelecimento    = cd_estabelecimento_w
        and     a.nr_sequencia          = nr_seq_conta_proc_p
        and     ((b.ie_tipo_conta is null) or (b.ie_tipo_conta = 'A') or
                ((b.ie_tipo_conta = ie_tipo_conta_w) or ((b.ie_tipo_conta = 'O') and (ie_tipo_conta_w = 'C'))));

Cursor C02 is
        select  a.nr_sequencia,
                nvl(a.pr_medico, pr_medico_w),
                nvl(a.pr_anestesista, pr_anestesista_w),
                nvl(a.pr_auxiliares, pr_auxiliares_w),
                nvl(a.pr_custo_operacional, pr_custo_operacional_w),
                nvl(a.pr_materiais, pr_materiais_w)
        from    pls_vinculo_direto_regra        a
        where   a.nr_seq_vinculo        = nr_seq_vinculo_w
        and     a.ie_situacao           = 'A'
        and     ((a.cd_procedimento 	is null)	or (a.cd_procedimento     = cd_proc_vinculo_w and a.ie_origem_proced    = ie_origem_proc_vinculo_w))
        and     ((a.ie_tipo_despesa     = ie_tipo_despesa_w) or (a.ie_tipo_despesa is null))
        and     ((a.ie_tipo_despesa_mat = ie_tipo_despesa_mat_w) or (a.ie_tipo_despesa_mat is null))
        and     vl_total_procedimento_w between nvl(vl_minimo,0) and nvl(vl_maximo,999999999999);

Cursor C03 is
        select  a.cd_procedimento,
                a.ie_origem_proced,
                a.ie_tipo_despesa,
                nvl(sum(a.vl_medico),0),
                nvl(sum(a.vl_anestesista),0),
                nvl(sum(a.vl_auxiliares),0),
                nvl(sum(a.vl_custo_operacional),0),
                nvl(sum(a.vl_materiais),0),
                nvl(sum(a.vl_total_procedimento),0),
                null
        from    pls_conta_proc  a
        where   a.nr_seq_conta  = nr_seq_conta_p
        and     a.nr_sequencia  != nr_seq_conta_proc_p
        group by a.cd_procedimento,
                 a.ie_origem_proced,
                 a.ie_tipo_despesa
        union all
        select  null,
                null,
                null,
                nvl(sum(0),0),
                nvl(sum(0),0),
                nvl(sum(0),0),
                nvl((sum(a.vl_material) - sum(nvl(a.vl_taxa_material,0))),0) ,
                nvl((sum(a.vl_material) - sum(nvl(a.vl_taxa_material,0))),0) ,
                nvl((sum(a.vl_material) - sum(nvl(a.vl_taxa_material,0))),0) ,
                a.ie_tipo_despesa
        from    pls_conta_mat   a
        where   a.nr_seq_conta  = nr_seq_conta_p
        group by a.ie_tipo_despesa;

begin

select	count(1)
into	qt_regra_vinculo_w
from	pls_vinculo_direto
where	ie_situacao	= 'A';

if	(qt_regra_vinculo_w > 0) then
	ie_gravar_log_w := ie_gravar_log_p;

	vl_medico_p             := null;
	vl_anestesista_p        := null;
	vl_auxiliares_p         := null;
	vl_custo_operacional_p  := null;
	vl_materiais_p          := null;
	vl_procedimento_p       := null;
	vl_total_procedimento_p := null;

	select  max(cd_estabelecimento),
		max(ie_tipo_conta)
	into    cd_estabelecimento_w,
		ie_tipo_conta_w
	from    pls_conta
	where   nr_sequencia    = nr_seq_conta_p;

	open C01;
	loop
	fetch C01 into
		nr_seq_proc_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_vinculo_w,
		pr_medico_w,
		pr_anestesista_w,
		pr_auxiliares_w,
		pr_custo_operacional_w,
		pr_materiais_w;
	exit when C01%notfound;
		begin
		
		open C03;
		loop
		fetch C03 into
			cd_proc_vinculo_w,
			ie_origem_proc_vinculo_w,
			ie_tipo_despesa_w,
			vl_medico_w,
			vl_anestesista_w,
			vl_auxiliares_w,
			vl_custo_operacional_w,
			vl_materiais_w,
			vl_total_procedimento_w,
			ie_tipo_despesa_mat_w;
		exit when C03%notfound;
			begin

			open C02;
			loop
			fetch C02 into
				nr_seq_vinculo_regra_w,
				pr_medico_ww,
				pr_anestesista_ww,
				pr_auxiliares_ww,
				pr_custo_operacional_ww,
				pr_materiais_ww;
			exit when C02%notfound;
				begin
				vl_medico_w             := dividir(vl_medico_w * pr_medico_ww,100);
				vl_anestesista_w        := dividir(vl_anestesista_w * pr_anestesista_ww,100);
				vl_auxiliares_w         := dividir(vl_auxiliares_w * pr_auxiliares_ww,100);
				vl_custo_operacional_w  := dividir(vl_custo_operacional_w * pr_custo_operacional_ww,100);
				vl_materiais_w          := dividir(vl_materiais_w * pr_materiais_ww,100);

				vl_medico_ww            := vl_medico_ww + vl_medico_w;
				vl_anestesista_ww       := vl_anestesista_ww + vl_anestesista_w;
				vl_auxiliares_ww        := vl_auxiliares_ww + vl_auxiliares_w;
				vl_custo_operacional_ww := vl_custo_operacional_ww + vl_custo_operacional_w;
				vl_materiais_ww         := vl_materiais_ww + vl_materiais_w;
				end;
			end loop;
			close C02;
			end;
		end loop;
		close C03;

		if      (nr_seq_vinculo_regra_w > 0) then
			vl_procedimento_w       := vl_medico_ww + vl_anestesista_ww + vl_auxiliares_ww +  vl_custo_operacional_ww + vl_materiais_ww;

			ds_observacao_log_w     :=      '% m�dico= ' || pr_medico_ww ||
									' % anestesista= ' || pr_anestesista_ww ||
									' % auxiliares= ' || pr_auxiliares_ww ||
									' % custo operacional= ' || pr_custo_operacional_ww ||
									' % materiais= ' || pr_materiais_ww;

			pls_gravar_log_calculo_proc(ie_gravar_log_w, nr_seq_conta_proc_p, cd_estabelecimento_w,
							'Ajuste v�nculo Direto', ds_observacao_log_w, 'pls_calcular_vinculo_direto',
							vl_procedimento_w, vl_materiais_ww, vl_custo_operacional_ww,
							vl_medico_ww, vl_auxiliares_ww, vl_anestesista_ww,
							7, null, null,
							null, null, null, nm_usuario_p);

			vl_medico_p                     := vl_medico_ww;
			vl_anestesista_p        := vl_anestesista_ww;
			vl_auxiliares_p         := vl_auxiliares_ww;
			vl_custo_operacional_p  := vl_custo_operacional_ww;
			vl_materiais_p          := vl_materiais_ww;
			vl_procedimento_p       := vl_procedimento_w;
			vl_total_procedimento_p	:= vl_medico_ww + vl_anestesista_ww + vl_auxiliares_ww + vl_custo_operacional_ww + vl_materiais_ww;

			update  pls_conta_proc
			set     nr_seq_regra_vinculo    = nr_seq_vinculo_w
			where   nr_sequencia            = nr_seq_proc_w;

			/* Francisco 09/05/2012 - Agora s� ir� retornar os valores pra pls_atualiza_valor_proc */
		end if;
		end;
	end loop;
	close C01;
end if;

end pls_calcular_vinculo_direto;
/