create or replace
procedure pls_prorrogar_venc_carteira
			(	nr_seq_lote_p		Number,
				ie_contrato_p		Varchar2,
				qt_dias_p		Number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 

nr_seq_segurado_w		number(10);
nr_contrato_w			number(10);
dt_validade_carteira_w		date;
dt_validade_cart_nova_w		date;
nr_seq_seg_carteira_w		number(10);
nr_seq_carteira_venc_w		number(10);
qt_dias_valid_cart_w		number(10);
ie_venc_cart_fim_mes_w		varchar2(10);
dt_rescisao_programada_w	date;
dt_rescisao_w			date;
ie_venc_cartao_rescisao_w	pls_parametros.ie_venc_cartao_rescisao%type;

Cursor C01 is
	select	b.nr_seq_segurado,
		to_number(substr(pls_obter_dados_segurado(b.nr_seq_segurado,'CC'),1,50)),
		a.nr_seq_seg_carteira,
		a.nr_sequencia,
		a.DT_VALIDADE_PRORROGADA,
		c.dt_rescisao
	from	pls_carteira_vencimento a,
		pls_segurado_carteira	b,
		pls_segurado		c
	where	a.nr_seq_seg_carteira	= b.nr_sequencia
	and	b.nr_seq_segurado	= c.nr_sequencia
	and	nr_seq_lote		= nr_seq_lote_p;
	
begin

qt_dias_valid_cart_w	:= qt_dias_p;

select	ie_venc_cart_fim_mes,
	ie_venc_cartao_rescisao
into	ie_venc_cart_fim_mes_w,
	ie_venc_cartao_rescisao_w
from	pls_parametros
where	cd_estabelecimento	= cd_estabelecimento_p;

open C01;
loop
fetch C01 into	
	nr_seq_segurado_w,
	nr_contrato_w,
	nr_seq_seg_carteira_w,
	nr_seq_carteira_venc_w,
	dt_validade_carteira_w,
	dt_rescisao_w;
exit when C01%notfound;
	begin
	
	dt_rescisao_programada_w	:= null;
	
	if (ie_contrato_p	= 'S') then
		begin
		qt_dias_valid_cart_w	:= pls_obter_meses_renovacao(nr_seq_segurado_w);
		exception
			when no_data_found then
			wheb_mensagem_pck.exibir_mensagem_abort(187963,'nr_contrato='||nr_contrato_w);
		end;
	end if;
	
	if	(dt_validade_carteira_w is null) then
		select	dt_validade_carteira
		into	dt_validade_carteira_w
		from	pls_segurado_carteira
		where	nr_sequencia	= nr_seq_seg_carteira_w;
	end if;
	
	dt_validade_cart_nova_w := (dt_validade_carteira_w + qt_dias_valid_cart_w);
	
	/*aaschlote 08/08/2012 OS - 475468*/
	if	(ie_venc_cart_fim_mes_w  = 'S') then
		dt_validade_cart_nova_w	:= last_day(dt_validade_cart_nova_w);
	end if;
	
	begin
	dt_rescisao_programada_w	:= pls_obter_data_rescisao_seg(nr_seq_segurado_w, ie_venc_cartao_rescisao_w);
	exception
	when others then
		dt_rescisao_programada_w	:= null;
	end;
	
	if	(dt_rescisao_programada_w is null) then
		dt_rescisao_programada_w := dt_rescisao_w;
	end if;
	
	/*aaschlote 14/05/2012 OS - 445193*/
	/*aaschlote 28/01/2013 OS - 537786 - Caso a data da validade prorrogada seja maior que a rescis�o programada, ent�o a validade ser� a rescis�o programada*/
	if	(dt_rescisao_programada_w is not null) and
		(dt_rescisao_programada_w	< dt_validade_cart_nova_w) then
		dt_validade_cart_nova_w	:= dt_rescisao_programada_w;
	end if;
	
	/*aaschlote 30/04/2012 OS - 423598 - Ir� alterar a validade no cart�o quando colocar o lote de vencimento para definitivo*/
	update	pls_carteira_vencimento
	set	DT_VALIDADE_PRORROGADA	= dt_validade_cart_nova_w,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_carteira_venc_w;
	
	end;
end loop;
close C01;

commit;

end pls_prorrogar_venc_carteira;
/
