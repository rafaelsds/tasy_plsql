create or replace
procedure vincular_nf_venc_contrato(	nr_seq_vencimento_p	number,
				nr_seq_nf_p		number,
				nm_usuario_p		varchar2) is

nr_seq_nota_w	number(10,0);

begin

select	nvl(max(nr_seq_nota),0)
into	nr_seq_nota_w
from	contrato_venc
where	nr_sequencia = nr_seq_vencimento_p;

if	(nr_seq_nota_w = 0) then
	update	contrato_venc
	set	dt_atualizacao 	= sysdate,
		nm_usuario	= nm_usuario_p,
		nr_seq_nota	= nr_seq_nf_p
	where	nr_sequencia	= nr_seq_vencimento_p;
elsif	(nr_seq_nota_w > 0) then
	update	contrato_venc
	set	dt_atualizacao 	= sysdate,
		nm_usuario	= nm_usuario_p,
		nr_seq_nota	= null
	where	nr_sequencia	= nr_seq_vencimento_p;
end if;

commit;

end vincular_nf_venc_contrato;
/