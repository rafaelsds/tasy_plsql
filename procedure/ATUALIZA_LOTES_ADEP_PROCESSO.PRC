create or replace
procedure atualiza_lotes_adep_processo (nr_lote_devolvido_p	varchar2,
					nr_lote_preparacao_p 	varchar2,
					nr_seq_frac_p	     	number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is

ie_permite_registrar_w	varchar2(15);
ie_permite_w		varchar2(1);
					
begin

obter_param_usuario(3112,22,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p,ie_permite_registrar_w);

if	(ie_permite_registrar_w = 'N') then     
	select	nvl(max('S'),'N')
	into	ie_permite_w
	from	material_lote_fornec
	where	ds_lote_fornec = to_char(nr_lote_preparacao_p);
	
	if	(ie_permite_w = 'N') then
		--Lote de prepara��o inv�lido!#@#@');
		Wheb_mensagem_pck.exibir_mensagem_abort(261403);
	end if;
end if;

if (nr_seq_frac_p is not null) then
   
	update 	adep_processo_item
	set	nr_lote_devolvido = nr_lote_devolvido_p,
		nr_lote_preparacao = nr_lote_preparacao_p
	where	nr_sequencia = nr_seq_frac_p;
	
	commit;
end if;

end atualiza_lotes_adep_processo;
/
