create or replace
procedure HRF_INTEGRACAO_MOVTO_CONTABIL(	nr_id_lote_p		number,
						dt_referencia_p		date,
						cd_tipo_lote_p		number,
						cd_empresa_p		number,
						cd_estabelecimento_p	number,
						dt_movimento_p		date,
						vl_movimento_p		number,
						cd_historico_p		number,
						cd_conta_debito_p		varchar2,
						cd_conta_credito_p		varchar2,
						ds_compl_historico_p	varchar2,
						nr_seq_agrupamento_p	number,
						cd_centro_custo_p		number,
						ds_erro_p		out	varchar2) as


nr_lote_contabil_w					number(10);
nr_seq_mes_ref_w					number(10);
nr_sequencia_movto_w				number(10);
nm_usuario_w					varchar2(15)	:= 'Int_contabil';
ds_erro_w					varchar2(255)	:= '';
dt_movimento_w					date;
qt_existe_hist_w					number(5);
qt_existe_conta_w					number(5);
qt_existe_centro_w					number(5);
cd_conta_credito_w				varchar2(20);
cd_conta_debito_w					varchar2(20);
qt_movimento_w					number(10);

/*
INFORMA��ES SOBRE OS PARAMETROS
nr_id_lote_p		= Identifica o numero do lote no outro sistema, e faz o relacionamento com o lote do Tasy
dt_referencia_p		= Identifica o mes do lote contabil e do mes de referencia a ser gerado
cd_tipo_lote_p		= Identifica o tipo de lote a ser gerado no Tasy
cd_empresa_p		= Identifica a empresa que corresponde a este mes no Tasy
cd_estabelecimento_p	= Identifica o estabelecimento que corresponde a este lote no Tasy
dt_movimento_p		= � a data do movimento a ser gerado
vl_movimento_p		= � o valor do movimento a ser gerado
cd_historico_p		= � o historico contabil.
cd_conta_debito_p		= � a conta de d�bito
cd_conta_credito_p		= � a conta de Cr�dito
ds_compl_historico_p	= � o complemento do historico
nr_seq_agrupamento_p	= � o agrupamento do movimento(se existir)
cd_centro_custo_p		= � o centro de custo do movimento(Se existir)
*/


begin

dt_movimento_w	:= nvl(dt_movimento_p, sysdate);

/*Verifica se foi informado o numero de identificacao do lote, que � o NR_IDLOTE do movimento*/
if	(nr_id_lote_p is null) then
	ds_erro_w	:= 'Falta informar o n�mero de identifica��o do lote.';
end if;

/*Verifica se o mes existe no Tasy*/
select	nvl(max(nr_sequencia), 0)
into	nr_seq_mes_ref_w
from	ctb_mes_ref
where	cd_empresa = cd_empresa_p
and	trunc(dt_referencia, 'mm') = trunc(dt_referencia_p, 'mm');

if	(nr_seq_mes_ref_w = 0) then
	ds_erro_w	:= substr('N�o existe o m�s ' || trunc(dt_referencia_p,'mm') || ' cadastrado no Tasy.',1,255);
end if;

/*verifica se o historico existe no Tasy*/
select	count(*)
into	qt_existe_hist_w
from	historico_padrao
where	cd_empresa = cd_empresa_p
and	cd_historico = cd_historico_p;

if	(qt_existe_hist_w = 0) then
	ds_erro_w	:= substr('O Hist�rico=' || cd_historico_p || ' n�o existe no Tasy.',1,255);
end if;


/*verifica se a conta contabil existe no Tasy*/

if	(nvl(cd_conta_debito_p,'0') <> '0') then
	begin

	select	nvl(min(cd_conta_contabil),'')
	into	cd_conta_debito_w
	from	conta_contabil
	where	replace(cd_classificacao,'.','') = cd_conta_debito_p
	and	ie_situacao	= 'A';
	exception when no_data_found then
		ds_erro_w	:= substr('A conta cont�bil=' || cd_conta_debito_p || ' n�o existe no Tasy.',1,255);
		
	end;
	
	select	nvl(max(cd_conta_destino),cd_conta_debito_w)
	into	cd_conta_debito_w
	from	ctb_troca_plano_conta
	where	cd_conta_origem	= cd_conta_debito_w;

	if	(nvl(cd_conta_debito_w,'X') = 'X') then
		ds_erro_w	:= substr('A conta cont�bil=' || cd_conta_debito_p || ' n�o existe no Tasy.',1,255);
	end if;		
end if;

/*verifica se a conta contabil existe no Tasy*/
if	(nvl(cd_conta_credito_p,'0') <> '0') then
	begin
	
	select	nvl(min(cd_conta_contabil),'')
	into	cd_conta_credito_w
	from	conta_contabil
	where	replace(cd_classificacao,'.','') = cd_conta_credito_p
	and	ie_situacao	= 'A';
	exception when no_data_found then
		ds_erro_w	:= substr('A conta cont�bil=' || cd_conta_credito_p || ' n�o existe no Tasy.',1,255);
	end;

	select	nvl(max(cd_conta_destino),cd_conta_credito_w)
	into	cd_conta_credito_w
	from	ctb_troca_plano_conta
	where	cd_conta_origem	= cd_conta_credito_w;
	
	if	(nvl(cd_conta_credito_w,'X') = 'X') then
		ds_erro_w	:= substr('A conta cont�bil=' || cd_conta_credito_p || ' n�o existe no Tasy.',1,255);
	end if;
end if;

/*verifica se o centro de custo existe no Tasy*/
if	(nvl(cd_centro_custo_p, 0) > 0) then
	select	count(*)
	into	qt_existe_centro_w
	from	centro_custo
	where	cd_centro_custo = cd_centro_custo_p;
	if	(qt_existe_centro_w = 0) then
		ds_erro_w	:= substr('O Centro de custo=' || cd_centro_custo_p || ' n�o existe no Tasy.',1,255);
	end if;
end if;

/*Consistir movimento negativo*/
if	(nvl(vl_movimento_p,0) < 0) then
	ds_erro_w	:= substr('O movimento da conta cont�bil=' || cd_conta_credito_p || '/' || cd_conta_debito_p || ' est� negativo. ' || vl_movimento_p,1,255);
end if;

if	(nvl(ds_erro_w,'X') <> 'X') then
	insert into hrf_log_imp_lote values(nm_usuario_w,sysdate,nr_id_lote_p,ds_erro_w);

	select	nvl(max(nr_lote_contabil), 0)
	into	nr_lote_contabil_w
	from	lote_contabil
	where	nr_lote_externo 	= nr_id_lote_p
	and	nr_seq_mes_ref		= nr_seq_mes_ref_w
	and	cd_tipo_lote_contabil	= cd_tipo_lote_p
	and	dt_atualizacao_saldo is null;
	
	if	(nr_lote_contabil_w <> 0) then
	
		select	count(*)
		into	qt_movimento_w
		from	ctb_movimento
		where	nr_lote_contabil	= nr_lote_contabil_w;

		if	(qt_movimento_w > 0) then
			delete from ctb_movimento
			where nr_lote_contabil		= nr_lote_contabil_w;
		end if;

		delete	lote_contabil
		where	nr_lote_contabil = nr_lote_contabil_w;
	end if;

	commit;
end if;

/*Vai entrar na rotina para inserir o movimento somente se n�o teve nenhum erro*/
if	(nvl(ds_erro_w,'X') = 'X') then
	begin
	/*Identifica se j� existe um lote com o n�mero do lote de integra��o*/
	select	nvl(max(nr_lote_contabil), 0)
	into	nr_lote_contabil_w
	from	lote_contabil
	where	nr_lote_externo 	= nr_id_lote_p
	and	nr_seq_mes_ref		= nr_seq_mes_ref_w
	and	cd_tipo_lote_contabil	= cd_tipo_lote_p
	and	dt_atualizacao_saldo is null;
	
	if	(nr_lote_contabil_w = 0) then

		/*Se n�o existir o lote, cria-o*/
		select	nvl(max(nr_lote_contabil), 0) + 1
		into	nr_lote_contabil_w
		from	lote_contabil;
		
		begin		
		insert into lote_contabil(
			nr_lote_contabil,
			dt_referencia,
			cd_tipo_lote_contabil,
			nr_seq_mes_ref,
			dt_atualizacao,
			nm_usuario,
			cd_estabelecimento,
			ie_situacao,
			ie_encerramento,
			nr_lote_externo)
		values( nr_lote_contabil_w,
			dt_referencia_p,
			cd_tipo_lote_p,
			nr_seq_mes_ref_w,
			sysdate,
			nm_usuario_w,
			cd_estabelecimento_p,
			'A',
			'N',
			nr_id_lote_p);
		exception when others then
			ds_erro_w	:= substr('Erro ao criar o lote no Tasy: ' || sqlerrm(sqlcode),1,255);
		end;
	end if;

/*	if	(nr_lote_contabil_w = 0) then
		Raise_application_error(-20011,	'Lote cont�bil n�o encontrado! ' || chr(13) || chr(10) || 
						'Lote origem: ' || nr_id_lote_p);
	end if;*/
	
	select	ctb_movimento_seq.nextval
	into	nr_sequencia_movto_w
	from	dual;

	begin
	insert into ctb_movimento(
		nr_sequencia,
		nr_lote_contabil,
		nr_seq_mes_ref,
		dt_movimento,
		vl_movimento,
		dt_atualizacao,
		nm_usuario,
		cd_historico,
		cd_conta_debito,
		cd_conta_credito,
		ds_compl_historico,
		nr_seq_agrupamento,
		ie_revisado,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values( nr_sequencia_movto_w,
		nr_lote_contabil_w,
		nr_seq_mes_ref_w,
		dt_movimento_p,
		vl_movimento_p,
		sysdate,
		nm_usuario_w,
		cd_historico_p,
		cd_conta_debito_w,
		cd_conta_credito_w,
		ds_compl_historico_p,
		nr_seq_agrupamento_p,
		'N',
		sysdate,
		nm_usuario_w);
	exception when others then
		ds_erro_w	:= substr('Erro ao gerar o movimento. ' || sqlerrm(sqlcode),1,255);
	end;

	/*Se tiver centro de custo informado, gera-o no Tasy*/
	if	(nvl(cd_centro_custo_p, 0) > 0) then
		begin
		insert into ctb_movto_centro_custo(
			nr_sequencia,
			nr_seq_movimento,
			cd_centro_custo,
			dt_atualizacao,
			nm_usuario,
			vl_movimento)
		values( ctb_movto_centro_custo_seq.nextval,
			nr_sequencia_movto_w,
			cd_centro_custo_p,
			sysdate,
			nm_usuario_w,
			vl_movimento_p);
		exception when others then
			ds_erro_w	:= substr('Erro ao gerar o centro custo do movimento. ' || sqlerrm(sqlcode),1,255);
		end;
	end if;
	end;
end if;

/*Retorno do erro - Se tiver informa��o � porque gerou algum erro e o movimento n�o foi inserido*/

ds_erro_p	:= substr(ds_erro_w,1,255);
/*
if	(nvl(ds_erro_w,'X') = 'X') then
	commit;
end if;*/
end HRF_INTEGRACAO_MOVTO_CONTABIL;
/