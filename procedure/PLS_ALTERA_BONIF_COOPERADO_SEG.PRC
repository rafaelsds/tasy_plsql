create or replace
procedure pls_altera_bonif_cooperado_seg
			(	nr_seq_segurado_p	number,
				nm_usuario_p		varchar2) is 

nr_seq_titular_w	number(10);
qt_dependente_w		number(10);
nr_seq_dependente_w	number(10);
nm_pessoa_dependente_w	varchar2(255);
ie_bonific_cooperado_w	varchar2(1);
ds_historico_w		pls_segurado_historico.ds_historico%type;

begin

select	max(nr_seq_titular),
	max(ie_bonific_cooperado)
into	nr_seq_titular_w,
	ie_bonific_cooperado_w
from	pls_segurado
where	nr_sequencia	= nr_seq_segurado_p;

if	(nvl(ie_bonific_cooperado_w,'N') = 'N') then
	select	count(*)
	into	qt_dependente_w
	from	pls_segurado
	where	nr_seq_titular	= nr_seq_titular_w
	and	nr_sequencia	<> nr_seq_segurado_p
	and	ie_bonific_cooperado	= 'S';

	if	(qt_dependente_w <> 0) then
		select	max(a.nr_sequencia),
			max(b.nm_pessoa_fisica)
		into	nr_seq_dependente_w,
			nm_pessoa_dependente_w
		from	pls_segurado	a,
			pessoa_fisica	b
		where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
		and	a.nr_seq_titular	= nr_seq_titular_w
		and	a.nr_sequencia		<> nr_seq_segurado_p
		and	ie_bonific_cooperado	= 'S';
		
		wheb_mensagem_pck.exibir_mensagem_abort(233340, 'NR_SEQ_DEPENDENTE=' || nr_seq_dependente_w || ';NM_PESSOA_DEPENDENTE=' || nm_pessoa_dependente_w);
		/* Mensagem: N�o � poss�vel habilitar a bonifica��o pois o benefici�rio #@NR_SEQ_DEPENDENTE#@ - #@NM_PESSOA_DEPENDENTE#@ est� habilitado para receber a mesma! */
	else
		update	pls_segurado
		set	ie_bonific_cooperado	= 'S'
		where	nr_sequencia	= nr_seq_segurado_p;
	end if;
else
	update	pls_segurado
	set	ie_bonific_cooperado	= 'N'
	where	nr_sequencia	= nr_seq_segurado_p;
end if;

select	'De: ' || decode(nvl(ie_bonific_cooperado_w,'N'),'N','N�o','Sim')  || ' Para: ' || decode(nvl(ie_bonific_cooperado_w,'N'),'S','N�o','Sim')
into	ds_historico_w
from 	dual;

pls_gerar_segurado_historico(	nr_seq_segurado_p, '68', sysdate, ds_historico_w,
				'pls_altera_bonif_cooperado_seg', null, null, null,
				null, null, null, null,
				null, null, null, null,
				nm_usuario_p, 'N');	

commit;

end pls_altera_bonif_cooperado_seg;
/