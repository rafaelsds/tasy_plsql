create or replace
procedure atualizar_info_export301(
			dt_ultima_exec_p	date,
			qt_tempo_interval_p 	number) is 

nr_sequencia_w		c301_regra_scheduler.nr_sequencia%type;
dt_proxima_exec_w 	c301_regra_scheduler.dt_proxima_exec_geracao%type;

total_minutos_dia_w number;

begin

select	(24 * 60) minutos_dia
into 	total_minutos_dia_w
from	dual;

select	nr_sequencia 
into	nr_sequencia_w
from	c301_regra_scheduler;

select  	sysdate + (qt_tempo_interval_p / total_minutos_dia_w) dt_proxima_exec
into	dt_proxima_exec_w
from	dual;

update	c301_regra_scheduler
set	dt_ultima_exec_geracao = dt_ultima_exec_p,
	dt_proxima_exec_geracao = dt_proxima_exec_w
where	nr_sequencia = nr_sequencia_w;
commit;

end atualizar_info_export301;
/