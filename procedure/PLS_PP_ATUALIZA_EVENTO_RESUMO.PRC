create or replace
procedure pls_pp_atualiza_evento_resumo(	dt_inicio_p		date,
						dt_fim_p		date,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
						nm_usuario_p		usuario.nm_usuario%type) is

-- em testes na base da USJRP n�o estava utilizando o indice pela data e sim pelo cmapo nr_seq_lote_pgto + ie_situacao
-- caso de algum problema de performance l� tentar utilizar o hint pelo indice /*+ INDEX(a plscomr_i8) */
cursor c01(	dt_inicio_pc	date,
		dt_fim_pc	date) is
	select	distinct a.nr_seq_conta nr_seq_conta
	from	pls_conta_medica_resumo a
	where	a.dt_competencia_pgto between dt_inicio_pc and dt_fim_pc
	and	a.ie_situacao	= 'A'
	and	a.ie_status	= 'F'
	and	a.nr_seq_pp_lote is null
	and	a.nr_seq_lote_pgto is null;

begin
-- verifica se tem algo para atualizar nas tabelas
pls_gerencia_upd_obj_pck.atualizar_objetos('Tasy', 'PLS_FILTRO_REGRA_EVENT_CTA_PCK.GERENCIA_REGRA_FILTRO', 'PLS_GRUPO_SERVICO_TM');
pls_gerencia_upd_obj_pck.atualizar_objetos('Tasy', 'PLS_FILTRO_REGRA_EVENT_CTA_PCK.GERENCIA_REGRA_FILTRO', 'PLS_ESTRUTURA_MATERIAL_TM');

for r_c01_w in c01(dt_inicio_p, dt_fim_p) loop

	pls_filtro_regra_event_cta_pck.gerencia_regra_filtro(	null, r_c01_w.nr_seq_conta, cd_estabelecimento_p,
								nm_usuario_p);
end loop;

end pls_pp_atualiza_evento_resumo;
/