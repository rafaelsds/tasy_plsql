create or replace 
procedure dispositivo_atribfocuslost(	nr_seq_dispositivo_p 	number,
				nr_atendimento_p		number,
				cd_estabelecimento_p	number,
				ie_curativo_disp_p		out varchar2,
				qt_horas_prev_curativo_p	out number,
				cd_setor_atendimento_p	out number,
				ie_permanencia_p		out varchar2,
				qt_horas_prev_disp_p	out number,
				ie_topografia_p		out varchar2) is

ie_curativo_disp_w		varchar2(15)	:= '';
qt_horas_prev_curativo_w	number(15,0)	:= 0;
cd_setor_atendimento_w	number(5,0)	:= 0;
ie_permanencia_w		varchar2(15)	:= '';
qt_horas_prev_disp_w	number(15,0)	:= 0;
ie_topografia_w		varchar2(5)	:= '';
	
begin

if	(nr_seq_dispositivo_p is not null) then
	begin
		
	select 	nvl(ie_curativo_disp,'N'),
		ie_permanencia
	into	ie_curativo_disp_w,
		ie_permanencia_w
	from 	dispositivo 
	where 	nr_sequencia = nr_seq_dispositivo_p;
		
	qt_horas_prev_curativo_w		:= obter_horas_prev_curativo(nr_seq_dispositivo_p, null, cd_estabelecimento_p);		
	cd_setor_atendimento_w	 	:= obter_setor_atendimento(nr_atendimento_p);		
	qt_horas_prev_disp_w		:= obter_horas_prev_disp(nr_seq_dispositivo_p, cd_setor_atendimento_w, cd_estabelecimento_p);
	ie_topografia_w			:= substr(obter_se_topografia(nr_seq_dispositivo_p),1,5);
		
	end;
end if;

ie_curativo_disp_p		:= ie_curativo_disp_w;
qt_horas_prev_curativo_p	:= qt_horas_prev_curativo_w;
cd_setor_atendimento_p	:= cd_setor_atendimento_w;
ie_permanencia_p		:= ie_permanencia_w;
qt_horas_prev_disp_p	:= qt_horas_prev_disp_w;
ie_topografia_p		:= ie_topografia_w;
	
end dispositivo_atribfocuslost;
/