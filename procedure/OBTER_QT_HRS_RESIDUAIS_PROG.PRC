create or replace
function Obter_Qt_Hrs_Residuais_Prog(nr_seq_prog_p number,
							dt_ref_p date)
 		    	return number is

qt_hr_residuais_retorno_w	number(10,5);
qt_total_hrs_crono_w		number(10,5);
pr_realizacao_w				number(10,5);

begin


select  max(p.pr_realizacao)
into 	pr_realizacao_w
from    proj_programa pr,
		proj_projeto p,
		proj_cronograma c, 
		proj_cron_etapa e
where	pr.nr_sequencia = p.nr_seq_programa
and		p.nr_sequencia = c.nr_seq_proj
and     c.nr_sequencia = e.nr_seq_cronograma
and     c.ie_situacao  = 'A'
and 	pr.nr_sequencia = nr_seq_prog_p
and	c.dt_aprovacao is not null
and  	not exists (select 1 from proj_cron_etapa xx where xx.nr_seq_superior =  e.nr_sequencia);

if(nvl(pr_realizacao_w,0) < 100 
	and dt_ref_p > trunc(sysdate))then
	goto final;
end if;


/*Pega o total de horas previstas de todos os cronogramas deste programa*/
select	sum(e.qt_hora_prev)
into 	qt_total_hrs_crono_w
from 	proj_programa pr, 
	proj_projeto p,
	proj_cronograma c,
	proj_cron_etapa e
where pr.nr_sequencia = p.nr_seq_programa
	and	p.nr_sequencia = c.nr_seq_proj
	and c.nr_sequencia = e.nr_seq_cronograma
	and c.ie_situacao = 'A'
	and c.ie_classificacao = 'D'
	and	c.dt_aprovacao is not null
	and  not exists (select 1 from proj_cron_etapa xx where xx.nr_seq_superior =  e.nr_sequencia)
	and pr.nr_sequencia = nr_seq_prog_p;

--horas residuais = (total de hrs  previstas do cronograma) - (Soma das hrs previstas de todas atividades realizadas que cont�m a dt fim real at� a data espec�fica)	
select	decode(max(e.qt_hora_real), 0, qt_total_hrs_crono_w,  qt_total_hrs_crono_w - sum(e.qt_hora_prev))
into 	qt_hr_residuais_retorno_w
from	proj_programa pr,  
	proj_projeto p,
	proj_cronograma c,
	proj_cron_etapa e
where pr.nr_sequencia = p.nr_seq_programa
	and p.nr_sequencia = c.nr_seq_proj
	and c.nr_sequencia = e.nr_seq_cronograma
	and c.ie_situacao = 'A'
	and c.ie_classificacao = 'D'
	and	c.dt_aprovacao is not null
	and  not exists (select 1 from proj_cron_etapa xx where xx.nr_seq_superior =  e.nr_sequencia)
	and e.dt_fim_real is not null
	and e.dt_fim_real <= pkg_date_utils.end_of(dt_ref_p	, 'DAY')
	and pr.nr_sequencia = nr_seq_prog_p;


/*Se o valor estiver nulo neste ponto, provavelmente n�o houve atividades realizadas ainda. Neste caso deve retornar o valor total das horas do cronograma*/	
if(qt_hr_residuais_retorno_w is null)then
	qt_hr_residuais_retorno_w := nvl(qt_total_hrs_crono_w, 0);
end if;	

<<final>>

return	qt_hr_residuais_retorno_w;

end Obter_Qt_Hrs_Residuais_Prog;
/
