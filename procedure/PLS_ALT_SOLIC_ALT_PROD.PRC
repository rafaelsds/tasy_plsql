create or replace
procedure pls_alt_solic_alt_prod
			(	nr_seq_solicitacao_p	number,	
				nr_seq_alteracao_p	varchar2,
				dt_alteracao_p		date,
				ds_observacao_p		varchar2,
				nm_usuario_p		varchar2) is

nr_seq_plano_w			pls_plano.nr_sequencia%type			:= null;

begin
begin
	nr_seq_plano_w		:= to_number(nr_seq_alteracao_p);
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(453777,';NM_CAMPO_P=' || 'Produto' || ';DS_VALOR_P=' || nr_seq_alteracao_p);
end;	

update 	pls_segurado_solic_alt
set	nr_seq_alt_plano = nr_seq_plano_w,
	dt_atualizacao = sysdate,
	nm_usuario = nm_usuario_p,
	ds_observacao = ds_observacao_p, 
	dt_alteracao = dt_alteracao_p
where	nr_sequencia = 	nr_seq_solicitacao_p;


commit;

end pls_alt_solic_alt_prod;
/