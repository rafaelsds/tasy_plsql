create or replace
procedure man_aprovar_orcamento(
			nr_sequencia_p		number,
			nm_usuario_p		Varchar2) is 

begin
if	(nr_sequencia_p > 0) then
	begin
	update	man_ordem_servico_orc
	set	dt_aprovacao 		= sysdate,
		nm_usuario_aprovacao	= nm_usuario_p,
		nm_pessoa_aprovacao	= substr(obter_pf_usuario(nm_usuario_p,'N'),1,40)
	where	nr_sequencia 		= nr_sequencia_p;

	commit;
	end;
end if;

end man_aprovar_orcamento;
/