create or replace
procedure copia_mat_via_aplic(	cd_estabelecimento_p	number,
				cd_material_p      		number,
				cd_material_novo_p	number,
				nm_usuario_p		varchar2) is

/* Campos da tabela LOOP */
ie_via_aplicacao_w		varchar2(5);
ds_recomendacao_w	varchar2(2000);
ie_diluicao_w		varchar2(1);
cd_setor_atendimento_w	number(5);
cd_setor_excluir_w		number(5);
cd_estabelecimento_w    mat_via_aplic.cd_estabelecimento%type;
ie_recomendada_w        mat_via_aplic.ie_recomendada%type;
nr_seq_prioridade_w     mat_via_aplic.nr_seq_prioridade%type;

/*  Sequencia da tabela */
nr_sequencia_w		number(10);

Cursor C01 is
	select	ie_via_aplicacao,
		ds_recomendacao,
		ie_diluicao,
        cd_estabelecimento,
		cd_setor_atendimento,
		cd_setor_excluir,
        nr_seq_prioridade,
        ie_recomendada
	from	mat_via_aplic
	where	cd_material = cd_material_p;

begin

open C01;
loop
fetch C01 into	
	ie_via_aplicacao_w,
	ds_recomendacao_w,
	ie_diluicao_w,
    cd_estabelecimento_w,
	cd_setor_atendimento_w,
	cd_setor_excluir_w,
    nr_seq_prioridade_w,
    ie_recomendada_w;
exit when C01%notfound;
	begin

	select	mat_via_aplic_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into mat_via_aplic (		nr_sequencia,
					cd_material,
					dt_atualizacao,
					nm_usuario,
					ie_via_aplicacao,
					ds_recomendacao,
					ie_diluicao,
                    cd_estabelecimento,
					cd_setor_atendimento,
					cd_setor_excluir,
                    nr_seq_prioridade,
                    ie_recomendada)
	values			(	nr_sequencia_w,
					cd_material_novo_p,
					sysdate,
					nm_usuario_p,
					ie_via_aplicacao_w,
					ds_recomendacao_w,
					ie_diluicao_w,
                    cd_estabelecimento_w,
					cd_setor_atendimento_w,
					cd_setor_excluir_w,
                    nr_seq_prioridade_w,
                    ie_recomendada_w
					);


	end;
end loop;
close C01;

commit;

end copia_mat_via_aplic;
/
