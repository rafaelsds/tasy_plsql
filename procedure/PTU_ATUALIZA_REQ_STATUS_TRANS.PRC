create or replace
procedure ptu_atualiza_req_status_trans
			(	nr_seq_pedido_p		number,
				nr_seq_requisicao_p	number,
				nm_usuario_p		varchar2) is

ie_autorizado_w			number(1);
ie_estagio_requisicao_w		number(3);
ie_tipo_tabela_w		varchar2(3);
qt_analise_proc_w		number(10);
qt_analise_mat_w		number(10);
qt_anali_proc_nega_w		number(10);
qt_anali_proc_apro_w		number(10);
qt_anali_proc_canc_w		number(10);
qt_anali_mat_nega_w		number(10);
qt_anali_mat_apro_w		number(10);
qt_anali_mat_canc_w		number(10);
ie_resultado_w			number(10);
qt_analise_negada_w		number(10);
qt_analise_aprovada_w		number(10);
qt_analise_cancelada_w		number(10);
cd_servico_w			number(15);
ds_servico_w			varchar2(80);
nr_seq_req_mat_w		number(10);
nr_seq_req_proc_w		number(10);
dt_validade_w			Date;
nr_seq_origem_w			number(10);
qt_autorizado_w			number(8);
qt_autorizado_ww		number(4);
qt_registros_aud_w		number(5);
ie_origem_solic_w		pls_requisicao.ie_origem_solic%type;
nr_seq_guia_w			pls_guia_plano.nr_sequencia%type;
nr_seq_mat_w			pls_requisicao_mat.nr_seq_material%type;
cd_procedimento_w		procedimento.cd_procedimento%type;
ie_origem_proc_w		procedimento.ie_origem_proced%type;
nr_seq_guia_proc_w		pls_guia_plano_proc.nr_sequencia%type;
nr_seq_guia_mat_w		pls_guia_plano_mat.nr_sequencia%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ie_tipo_intercambio_w		pls_requisicao.ie_tipo_intercambio%type;

Cursor C01 is
	select	nr_seq_req_mat,
		nr_seq_req_proc,
		ie_autorizado,
		ie_tipo_tabela,
		qt_autorizado
	from	ptu_resp_servico_status
	where	ie_autorizado in (1,2,3,4,5)
	and	nr_seq_resp_ped_status	= nr_seq_pedido_p;
	
begin

cd_estabelecimento_w := ptu_obter_estab_padrao;

select	nvl(ie_estagio, 0),
	nvl(ie_origem_solic, 'M'),
	ie_tipo_intercambio
into	ie_estagio_requisicao_w,
	ie_origem_solic_w,
	ie_tipo_intercambio_w
from	pls_requisicao
where	nr_sequencia	= nr_seq_requisicao_p;

if	(ie_origem_solic_w = 'E') then
	select	max(a.nr_seq_guia)
	into	nr_seq_guia_w
	from	pls_requisicao c,
		pls_execucao_requisicao a
	where	a.nr_seq_requisicao	= c.nr_sequencia
	and	c.nr_sequencia		= nr_seq_requisicao_p;
end if;

begin
	select	dt_validade,
		nr_seq_origem
	into	dt_validade_w,
		nr_seq_origem_w
	from	ptu_resp_pedido_status
	where	nr_sequencia	= nr_seq_pedido_p;
exception
when others then
	dt_validade_w	:= null;
	nr_seq_origem_w	:= null;
end;

/* Atualiza apenas se o est�gio da requisi��o � igual a 'AUDITORIA INTERC�MBIO' */
if	(ie_estagio_requisicao_w = 5) then
	open C01;
	loop
	fetch C01 into
		nr_seq_req_mat_w,
		nr_seq_req_proc_w,
		ie_autorizado_w,
		ie_tipo_tabela_w,
		qt_autorizado_w;
	exit when C01%notfound;
		begin
		qt_autorizado_ww   :=   substr(to_char(qt_autorizado_w),1,4);

		/* Atualiza o est�gio dos procedimentos e materiais apenas se os status forem igual a 1 (Negado), 2 (Autorizado) o 3 (Pendente para autoriza��o da empresa), 4 (Pendente para auditoria), 5 (Cancelado)*/
		if	(ie_autorizado_w in (1,2,3,4,5)) then
			if	(ie_tipo_tabela_w in ('0', '1', '4')) and (nr_seq_req_proc_w	is not null) then

				update	pls_requisicao_proc
				set	ie_status	= decode(ie_autorizado_w,1,'N',2,'S',3,'A',4,'A',5,'C'),
					qt_procedimento	= decode(ie_autorizado_w,2,qt_autorizado_ww,0)
				where	nr_sequencia	= nr_seq_req_proc_w;
				
				if	(ie_origem_solic_w = 'E') and
					(nr_seq_guia_w is not null) then
					select	cd_procedimento,
						ie_origem_proced
					into	cd_procedimento_w,
						ie_origem_proc_w
					from	pls_requisicao_proc
					where	nr_sequencia		= nr_seq_req_proc_w;
					
					select	max(nr_sequencia)
					into	nr_seq_guia_proc_w
					from	pls_guia_plano_proc
					where	cd_procedimento		= cd_procedimento_w
					and	ie_origem_proced	= ie_origem_proc_w
					and	ie_status		= 'I'
					and	nr_seq_guia		= nr_seq_guia_w;
					
					update	pls_guia_plano_proc
					set	ie_status		= decode(ie_autorizado_w,1,'N',2,'S',3,'A',4,'A',5,'C'),
						qt_autorizada		= decode(ie_autorizado_w,2,qt_autorizado_ww,0)
					where	nr_sequencia		= nr_seq_guia_proc_w;
				end if;
			elsif	(ie_tipo_tabela_w in ('2', '3')) and (nr_seq_req_mat_w	is not null) then

				update	pls_requisicao_mat
				set	ie_status	= decode(ie_autorizado_w,1,'N',2,'S',3,'A',4,'A',5,'C'),
					qt_material	= decode(ie_autorizado_w,2,qt_autorizado_ww,0)
				where	nr_sequencia	= nr_seq_req_mat_w;
				
				if	(ie_origem_solic_w = 'E') and
					(nr_seq_guia_w is not null) then
					select	nr_seq_material
					into	nr_seq_mat_w
					from	pls_requisicao_mat
					where	nr_sequencia		= nr_seq_req_mat_w;
					
					select	max(nr_sequencia)
					into	nr_seq_guia_mat_w
					from	pls_guia_plano_mat
					where	nr_seq_material		= nr_seq_mat_w
					and	ie_status		= 'I'
					and	nr_seq_guia		= nr_seq_guia_w;
					
					update	pls_guia_plano_mat
					set	ie_status		= decode(ie_autorizado_w,1,'N',2,'S',3,'A',4,'A',5,'C'),
						qt_autorizada		= decode(ie_autorizado_w,2,qt_autorizado_ww,0)
					where	nr_sequencia		= nr_seq_guia_mat_w;
				end if;
			end if;
		end if;	
		end;
	end loop;
	close C01;

	select	count(1)
	into	qt_analise_proc_w
	from	pls_requisicao_proc
	where	ie_status		= 'A'
	and	nr_seq_requisicao	= nr_seq_requisicao_p;

	select	count(1)
	into	qt_analise_mat_w
	from	pls_requisicao_mat
	where	ie_status		= 'A'
	and	nr_seq_requisicao	= nr_seq_requisicao_p;

	if	((qt_analise_proc_w = 0) and (qt_analise_mat_w = 0)) then
		/* Quantidade de procedimentos 'Negados' */
		select	count(1)
		into	qt_anali_proc_nega_w
		from	pls_requisicao_proc
		where	ie_status		= 'N'
		and	nr_seq_requisicao	= nr_seq_requisicao_p;

		/* Quantidade de procedimentos 'Aprovados' */
		select	count(1)
		into	qt_anali_proc_apro_w
		from	pls_requisicao_proc
		where	ie_status		= 'S'
		and	nr_seq_requisicao	= nr_seq_requisicao_p;

		/* Quantidade de procedimentos 'Cancelados' */
		select	count(1)
		into	qt_anali_proc_canc_w
		from	pls_requisicao_proc
		where	ie_status		= 'C'
		and	nr_seq_requisicao	= nr_seq_requisicao_p;

		/* Quantidade de materiais 'Negados' */
		select	count(1)
		into	qt_anali_mat_nega_w
		from	pls_requisicao_mat
		where	ie_status		= 'N'
		and	nr_seq_requisicao	= nr_seq_requisicao_p;

		/* Quantidade de materiais 'Aprovados' */
		select	count(1)
		into	qt_anali_mat_apro_w
		from	pls_requisicao_mat
		where	ie_status		= 'S'
		and	nr_seq_requisicao	= nr_seq_requisicao_p;

		/* Quantidade de materiais 'Cancelados' */
		select	count(1)
		into	qt_anali_mat_canc_w
		from	pls_requisicao_mat
		where	ie_status		= 'C'
		and	nr_seq_requisicao	= nr_seq_requisicao_p;

		qt_analise_negada_w	:= qt_anali_proc_nega_w + qt_anali_mat_nega_w;
		qt_analise_aprovada_w	:= qt_anali_proc_apro_w + qt_anali_mat_apro_w;
		qt_analise_cancelada_w	:= qt_anali_proc_canc_w + qt_anali_mat_canc_w;

		if	((qt_analise_cancelada_w > 0) and (qt_analise_negada_w = 0) and (qt_analise_aprovada_w = 0)) then
			ie_resultado_w	:= 3; -- Cancelada
		elsif	((qt_analise_aprovada_w > 0) and (qt_analise_negada_w = 0)) then
			ie_resultado_w	:= 2; -- Aprovada
		elsif	((qt_analise_negada_w > 0) and (qt_analise_aprovada_w = 0)) then
			ie_resultado_w	:= 7; -- Reprovada
		elsif	((qt_analise_negada_w > 0) and (qt_analise_aprovada_w > 0)) then
			ie_resultado_w	:= 6; -- Parcialmente aprovada
		end if;

		if	(ie_resultado_w is not null) then
			update	pls_requisicao
			set	ie_estagio		= ie_resultado_w,
				dt_fim_processo_req	= sysdate,
				dt_valid_senha_ext	= decode(ie_resultado_w,3,null,7,null,dt_validade_w),
				dt_validade_senha	= decode(ie_resultado_w,3,null,7,null,dt_validade_w),
				cd_senha_externa	= decode(ie_resultado_w,3,null,7,null,nr_seq_origem_w)
			where	nr_sequencia		= nr_seq_requisicao_p;
						
			select	count(1)
			into	qt_registros_aud_w
			from	pls_auditoria
			where	nr_seq_requisicao	= nr_seq_requisicao_p
			and	dt_liberacao	is null;

			if	(qt_registros_aud_w > 0) then
				update	pls_auditoria
				set	ie_status		= decode(ie_resultado_w,8,'C','F'),
					dt_liberacao		= sysdate,
					nr_seq_proc_interno	= null,
					dt_atualizacao		= sysdate,
					nm_usuario		= nm_usuario_p
				where	nr_seq_requisicao	= nr_seq_requisicao_p;
				
				pls_requisicao_gravar_hist(	nr_seq_requisicao_p,'L','An�lise da requisi��o finalizada na fun��o OPS - Gest�o de An�lise de Autoriza��es, pela importa��o de pedido de status de autoriza��o',
								null,nm_usuario_p);
								
				update	pls_auditoria_grupo
				set	dt_liberacao		= sysdate,
					ie_status		= 'S',
					nm_usuario		= nm_usuario_p,
					dt_atualizacao		= sysdate
				where	dt_liberacao is null
				and	exists(	select	1
						from 	pls_auditoria x
						where 	x.nr_seq_requisicao	= nr_seq_requisicao_p
						and 	x.nr_sequencia 		= nr_seq_auditoria);
			end if;

			if	(ie_resultado_w in (2,6)) and (ie_tipo_intercambio_w = 'E') then
				pls_executar_req_interc_aprov(nr_seq_requisicao_p, cd_estabelecimento_w, nm_usuario_p);
			end if;

		end if;
	end if;
end if;

commit;

end ptu_atualiza_req_status_trans;
/