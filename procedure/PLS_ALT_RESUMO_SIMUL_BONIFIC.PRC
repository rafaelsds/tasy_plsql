create or replace
procedure pls_alt_resumo_simul_bonific
		(	nr_seq_simulacao_p	number,
			nr_seq_segurado_simul_p	number,
			ie_opcao_p		varchar2,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is

/*
ie_opcao_p
I - Individual/Familiar
C - Coletivos
*/

nr_seq_bonific_vinc_w	number(10);
vl_bonificacao_w	number(15,2) := 0;
ds_resumo_bonific_pj_w	varchar2(255);
qt_benef_simul_pj_w	number(10);
qt_registros_w		number(10);
nm_bonificacao_w	varchar2(255);
ds_resumo_bonificacao_w	varchar2(4000);

Cursor C01 is
	select	nr_sequencia
	from	pls_bonificacao_vinculo
	where	nr_seq_segurado_simul	= nr_seq_segurado_simul_p
	union all
	select	nr_sequencia
	from	pls_bonificacao_vinculo
	where	nr_seq_simulacao	= nr_seq_simulacao_p;

Cursor C02 is
	select	nr_sequencia
	from	pls_bonificacao_vinculo
	where	nr_seq_simulacao	= nr_seq_simulacao_p;

begin

select	nvl(sum(qt_beneficiario),0)
into	qt_benef_simul_pj_w
from	pls_simulpreco_coletivo
where	nr_seq_simulacao	= nr_seq_simulacao_p;

if	(ie_opcao_p	= 'I') then
	open C01;
	loop
	fetch C01 into
		nr_seq_bonific_vinc_w;
	exit when C01%notfound;
		begin
		pls_gerar_resumo_itens_adic(nr_seq_segurado_simul_p,nr_seq_bonific_vinc_w,'B',vl_bonificacao_w);
		
		select	count(*)
		into	qt_registros_w
		from	pls_simulacao_resumo
		where	nr_seq_simulacao	= nr_seq_simulacao_p
		and	nr_seq_segurado_simul	= nr_seq_segurado_simul_p
		and	nr_seq_vinc_bonificacao	= nr_seq_bonific_vinc_w
		and	ie_tipo_pessoa		= 'PF';
		
		select	a.nm_bonificacao
		into	nm_bonificacao_w
		from	pls_bonificacao_vinculo	b,
			pls_bonificacao		a
		where	b.nr_seq_bonificacao	= a.nr_sequencia
		and	b.nr_sequencia		= nr_seq_bonific_vinc_w;
		
		ds_resumo_bonificacao_w	:= '                Bonificação - ' || nm_bonificacao_w;
		
		if	(qt_registros_w	> 0) then
			update	pls_simulacao_resumo
			set	vl_resumo		= vl_bonificacao_w
			where	nr_seq_simulacao	= nr_seq_simulacao_p
			and	nr_seq_vinc_bonificacao	= nr_seq_bonific_vinc_w
			and	nr_seq_ordem		= 3
			and	nr_seq_segurado_simul	= nr_seq_segurado_simul_p
			and	ie_tipo_pessoa		= 'PF';
		else
			insert into	pls_simulacao_resumo
				(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
					cd_estabelecimento,nr_seq_simulacao,nr_seq_ordem,nr_seq_segurado_simul,nr_seq_vinc_bonificacao,
					ds_item_resumo,vl_resumo,ie_tipo_pessoa,vl_pro_rata_dia,vl_antecipacao)
			values	(	pls_simulacao_resumo_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
					cd_estabelecimento_p,nr_seq_simulacao_p,3,nr_seq_segurado_simul_p,nr_seq_bonific_vinc_w,
					ds_resumo_bonificacao_w,vl_bonificacao_w,'PF',0,0);
		end if;
		
		end;
	end loop;
	close C01;
elsif	(ie_opcao_p	= 'C') then
	open C02;
	loop
	fetch C02 into
		nr_seq_bonific_vinc_w;
	exit when C02%notfound;
		begin
		ds_resumo_bonific_pj_w	:= 'Bonificação - ' || substr(pls_obter_dados_bonificacao(nr_seq_bonific_vinc_w,'D'),1,150) || ' - Qt. = ' || qt_benef_simul_pj_w;
		vl_bonificacao_w	:= nvl(pls_obter_valores_simul_pj(nr_seq_simulacao_p,null,nr_seq_bonific_vinc_w,'B'),0);
		
		update	pls_simulacao_resumo
		set	vl_resumo		= vl_bonificacao_w,
			ds_item_resumo		= ds_resumo_bonific_pj_w
		where	nr_seq_simulacao	= nr_seq_simulacao_p
		and	nr_seq_vinc_bonificacao	= nr_seq_bonific_vinc_w
		and	nr_seq_ordem		= 3
		and	ie_tipo_pessoa		= 'PJ';
		
		end;
	end loop;
	close C02;
end if;

end pls_alt_resumo_simul_bonific;
/