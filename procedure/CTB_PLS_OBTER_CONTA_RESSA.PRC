create or replace
procedure ctb_pls_obter_conta_ressa
			(	cd_estabelecimento_p		in	number,
				dt_referencia_p			in	date,
				ie_debito_credito_p		in	varchar2,
				ie_tipo_contratacao_p		in	varchar2,
				ie_preco_p			in	varchar2,
				ie_segmentacao_p		in	varchar2,
				ie_regulamentacao_p		in	varchar2,
				ie_participacao_p		in	varchar2,
				ie_tipo_beneficiario_p		in	varchar2,
				ie_tipo_segurado_p		in	varchar2,
				nr_seq_regra_p			out	number,
				cd_historico_p			out	number,
				cd_conta_contabil_p		out	varchar2,
				cd_historico_deferido_p		out	number,
				cd_conta_deferido_p		out	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Obter a "Regra cont�bil de despesas com ressarcimento", fuun��o OPS - Crit�rios
Cont�beis
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	CTB_PLS_ATUALIZAR_RESSA
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_regra_w			varchar2(20);
cd_conta_contabil_w		varchar2(20);
cd_conta_deferido_w		varchar2(20);
cd_historico_w			number(10);
cd_historico_deferido_w		number(10);

Cursor c_regra is
	select	nr_sequencia,
		cd_conta_contabil,
		cd_historico,
		cd_conta_deferido,
		cd_historico_deferido
	from	pls_regra_ctb_mensal_ressa
	where	cd_estabelecimento										= cd_estabelecimento_p
	and	dt_inicio_vigencia											<= dt_referencia_p
	and	nvl(dt_fim_vigencia,dt_referencia_p)						>= dt_referencia_p
	and	(nvl(ie_tipo_contratacao,nvl(ie_tipo_contratacao_p, 'X'))	= nvl(ie_tipo_contratacao_p, 'X'))	
	and	(nvl(ie_debito_credito,ie_debito_credito_p)					= ie_debito_credito_p)	
	and	(nvl(ie_preco,nvl(ie_preco_p, 'X')) 						= nvl(ie_preco_p, 'X'))	
	and	(nvl(ie_segmentacao,nvl(ie_segmentacao_p, 'X'))				= nvl(ie_segmentacao_p, 'X'))	
	and	(nvl(ie_regulamentacao,nvl(ie_regulamentacao_p, 'X'))		= nvl(ie_regulamentacao_p, 'X'))	
	and	(nvl(ie_participacao,nvl(ie_participacao_p, 'X')) 			= nvl(ie_participacao_p, 'X'))	
	and	(nvl(ie_tipo_beneficiario,nvl(ie_tipo_beneficiario_p, 'X')) = nvl(ie_tipo_beneficiario_p, 'X'))
	and	(nvl(ie_tipo_segurado,nvl(ie_tipo_segurado_p,'X')) 			= nvl(ie_tipo_segurado_p,'X'))
	order by
		nvl(ie_regulamentacao, 'A'),
		nvl(ie_preco, 'A'),
		nvl(ie_tipo_contratacao, 'A'),
		nvl(ie_participacao, 'A'),
		nvl(ie_tipo_beneficiario, 'A'),
		nvl(ie_segmentacao, 'A'),
		nvl(ie_debito_credito, 'A'),
		nvl(ie_tipo_contratacao, 'A'),
		nvl(cd_conta_contabil,' A'),
		nvl(dt_inicio_vigencia, sysdate),
		nvl(dt_fim_vigencia, sysdate),
		nvl(nr_sequencia, 0);

begin

open c_regra;
loop
fetch c_regra into
	nr_seq_regra_w,
	cd_conta_contabil_w,
	cd_historico_w,
	cd_conta_deferido_w,
	cd_historico_deferido_w;
exit when c_regra%notfound;
end loop;
close c_regra;

cd_conta_contabil_p		:= nvl(cd_conta_contabil_w, '0');
nr_seq_regra_p			:= nvl(nr_seq_regra_w, 0);
cd_historico_p			:= nvl(cd_historico_w, 0);
cd_conta_deferido_p		:= nvl(cd_conta_deferido_w, '0');
cd_historico_deferido_p		:= nvl(cd_historico_deferido_w, 0);

commit;

end ctb_pls_obter_conta_ressa;
/