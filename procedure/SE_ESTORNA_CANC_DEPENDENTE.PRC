create or replace
procedure se_estorna_canc_dependente(
		nr_seq_contrato_p	number,
		cd_pessoa_fisica_p	varchar2,
		nm_usuario_p		varchar2) is		
begin 
	if 	(nr_seq_contrato_p is not null) and
		(cd_pessoa_fisica_p is not null) then		
		begin
		
		update	eme_pf_contrato
		set 	dt_cancelamento 	= null,
			nm_usuario		= nm_usuario_p
		where	nr_seq_contrato 	= nr_seq_contrato_p
		and	cd_pessoa_fisica 	= cd_pessoa_fisica_p;
		
		end;
	end if;
commit;
end se_estorna_canc_dependente;
/