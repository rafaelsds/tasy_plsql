create or replace
procedure scola_Estornar_Prescr_Proc(	nr_prescricao_p		number,
					nr_seq_prescr_p		number,
					ds_observacao_p		varchar2,
					nm_usuario_p		varchar2 default 'SCOLA') is

ie_canc_proc_prescr_w		varchar2(1);

nr_sequencia_w			number(10,0);
cd_convenio_w			number(5,0);
nr_seq_gerada_w			number(10,0);
nr_interno_conta_w		number(10,0);
ie_recalcular_conta_w		varchar2(1):= 'N';
ie_status_acerto_w		varchar2(1);

cursor	c01 is
	select	nr_sequencia,
		cd_convenio,
		nr_interno_conta
	from	procedimento_paciente
	where 	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia_prescricao	= nr_seq_prescr_p
	and	qt_procedimento > 0;

begin

select 	nvl(max(ie_canc_proc_prescr),'N'),
	nvl(max(ie_recalcular_conta),'N')
into	ie_canc_proc_prescr_w,
	ie_recalcular_conta_w
from 	parametro_faturamento c,
	Atendimento_paciente b,
	prescr_medica a
where	a.nr_prescricao		= nr_prescricao_p
  and	a.nr_atendimento	= b.nr_atendimento
  and	b.cd_estabelecimento	= c.cd_estabelecimento;

  
select	max(y.nr_interno_conta)
into	nr_interno_conta_w
from	procedimento_paciente y
where	y.nr_prescricao 	  = nr_prescricao_p
and	y.nr_Sequencia_prescricao = nr_seq_prescr_p;  


select	max(ie_status_acerto)
into	ie_status_acerto_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_w;

if	(ie_status_acerto_w = '2') then
		
	wheb_mensagem_pck.Exibir_Mensagem_Abort(232847);
	
end if;

  
if	(ie_canc_proc_prescr_w = 'S') then
	open c01;
	loop
	fetch c01 into	nr_sequencia_w,
			cd_convenio_w,
			nr_interno_conta_w;
		exit when c01%notfound;

		duplicar_proc_paciente(nr_sequencia_w, nm_usuario_p, nr_seq_gerada_w);

		if	(nr_seq_gerada_w is not null) then
			update procedimento_paciente
			set	nr_interno_conta = null,
				qt_procedimento	 = qt_procedimento * -1,
				ds_observacao	 = ds_observacao || ' Estorno gerado a partir do cancelamento da prescrição'
			where nr_sequencia = nr_seq_gerada_w;

			atualiza_preco_procedimento(nr_seq_gerada_w, cd_convenio_w, nm_usuario_p);
		end if;
	end loop;
	close c01;
else
	open c01;
	loop
	fetch c01 into	nr_sequencia_w,
			cd_convenio_w,
			nr_interno_conta_w;
		exit when c01%notfound;
		
		delete procedimento_paciente
		where  nr_seq_proc_princ = nr_sequencia_w; 
		
	end loop;
	close c01;
	
	delete procedimento_paciente
	where nr_prescricao = nr_prescricao_p
	and nr_sequencia_prescricao = nr_seq_prescr_p;
	
	if	(nvl(ie_recalcular_conta_w,'N') = 'S') then
		
		select 	nvl(max(ie_status_acerto),2)
		into	ie_status_acerto_w
		from 	conta_paciente
		where 	nr_interno_conta = nr_interno_conta_w;
		
		if	(ie_status_acerto_w = 1) then
			Recalcular_itens_conta(nr_interno_conta_w, nm_usuario_p);
		end if;
	end if;
end if;

if	(ds_observacao_p is not null) then
	update	prescr_procedimento
	set	ds_observacao 	= ds_observacao_p
	where	nr_prescricao 	= nr_prescricao_p
	and	nr_sequencia	= nr_seq_prescr_p;
end if;

end;
/