create or replace
procedure vincular_problema(nr_sequencia_p	        number,
		              nr_seq_problema_sup_p   number,
		              ie_opcao_p	        varchar2) is

dt_inicio_atual_w date;
dt_inicio_sup_w date;						  
vinc_count_v number;
nr_seq_vinc_pai_v number;

begin

if	(ie_opcao_p = 'V') and
	(nr_sequencia_p is not null) and
	(nr_seq_problema_sup_p is not null) then
		select count(*) into vinc_count_v from lista_problema_pac where nr_seq_vinc_pai = nr_sequencia_p;
		select nr_seq_vinc_pai into nr_seq_vinc_pai_v from lista_problema_pac where nr_sequencia = nr_seq_problema_sup_p;
		
		if (vinc_count_v <> 0) then
			update	lista_problema_pac
			set	    nr_seq_vinc_pai     = nr_seq_problema_sup_p
			where	nr_seq_vinc_pai	    = nr_sequencia_p;
			  
			update	lista_problema_pac
			set	    nr_seq_problema_sup     = nr_seq_problema_sup_p,
					nr_seq_vinc_pai 	    = nvl(nr_seq_vinc_pai_v, nr_seq_problema_sup_p)
			where	nr_sequencia	        = nr_sequencia_p;
			commit;
			  
		else
			update  lista_problema_pac
			set	    nr_seq_problema_sup     = nr_seq_problema_sup_p,
					nr_seq_vinc_pai 	    = nvl(nr_seq_vinc_pai_v, nr_seq_problema_sup_p)
			where	nr_sequencia	        = nr_sequencia_p;
			commit;
		end if;
			
elsif (ie_opcao_p = 'D') then
	if	(nr_sequencia_p is not null) then

		update	lista_problema_pac
		set	nr_seq_problema_sup = null
		where	nr_sequencia	    = nr_sequencia_p;
		commit;
	end if;

elsif (ie_opcao_p = 'A') then
	if	(nr_sequencia_p is not null) and
        (nr_seq_problema_sup_p is not null)	then

		select dt_inicio
		into dt_inicio_atual_w
		from lista_problema_pac
		where nr_sequencia = nr_sequencia_p;
		
		select dt_inicio
		into dt_inicio_sup_w
		from lista_problema_pac
		where nr_sequencia = nr_seq_problema_sup_p;
		
		if (dt_inicio_atual_w < dt_inicio_sup_w) then
			
			update	lista_problema_pac
			set	nr_seq_problema_sup = nr_sequencia_p
			where	nr_sequencia	    = nr_seq_problema_sup_p;
			commit;
			
		elsif (dt_inicio_atual_w >= dt_inicio_sup_w) then
		
			update	lista_problema_pac
			set	nr_seq_problema_sup = nr_seq_problema_sup_p
			where	nr_sequencia	    = nr_sequencia_p;
			commit;			
			
		end if;
	end if;
end if;

end vincular_problema;
/
