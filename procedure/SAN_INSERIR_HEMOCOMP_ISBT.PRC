create or replace
procedure san_inserir_hemocomp_isbt(	ds_codigo_p		varchar2,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number,
					nr_seq_emp_ent_p out	number) is

ds_codigo_w		varchar2(2000);
ds_structure_w		varchar2(255);
ie_contador_w		number(10,0)	:= 0;
ie_pos_igual_w		number(3,0);
ie_pos_ecom_w		number(3,0);
ie_pos_menor_w		number(3,0);
ds_referencia_w		varchar2(255);
tam_lista_w		number(10,0);
ds_001_w		varchar2(255);
ds_002_w		varchar2(255);
ds_003_w		varchar2(255);
ds_004_w		varchar2(255);
ds_005_w		varchar2(255);
ds_006_w		varchar2(255);
ds_007_w		varchar2(255);
ds_008_w		varchar2(255);
ds_009_w		varchar2(255);
ds_019_w		varchar2(255);
cd_local_isbt_w		varchar2(5);
nr_seq_isbt_w		number(6);
ie_tipo_sangue_w	varchar2(2);
ie_fator_rh_w		varchar2(1);
nr_seq_tipo_doacao_w	number(10);
nr_seq_derivado_w	number(10);
nr_seq_entidade_w	number(10);
nr_seq_emp_ent_w	number(10);
nr_seq_conservante_w	number(10);
ie_lavado_w		varchar2(1);
ie_irradiado_w		varchar2(1);
ie_filtrado_w		varchar2(1);
ie_aliquotado_w		varchar2(1);
cd_isbt_tipo_doa_w	varchar2(1);
dt_vencimento_w		date;
dt_coleta_w		date;
dt_producao_w		date;
ds_inconsistencia_w	varchar2(255);
qt_registro_grupo_w	number(3);
ie_liberar_w		varchar2(1);
dt_liberacao_w		date;
nm_usuario_lib_w	varchar2(15);
ie_tipo_doa_cadast_w	varchar2(1);
ds_tipo_doacao_w	varchar2(255);
nr_seq_tipo_doa_deri_w	number(10);
ie_ja_existe_w		varchar2(1);
nr_seq_producao_exist_w	number(10);
cd_divisao_isbt_w	varchar2(2);
VarHoraVencimentoComponente Varchar2(255);
qt_dias_validade_w	Number(4);

begin
obter_param_usuario(450, 457, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_liberar_w);
obter_param_usuario(450, 262, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, VarHoraVencimentoComponente);


if	(ds_codigo_p is not null) then

	ds_codigo_w	:= ds_codigo_p;

	while	ds_codigo_w is not null and
		ie_contador_w < 200 loop
		begin
		tam_lista_w		:= length(ds_codigo_w);
		ie_pos_igual_w		:= instr(' '||substr(ds_codigo_w,2,tam_lista_w),chr(61));
		ie_pos_ecom_w		:= instr(' '||substr(ds_codigo_w,2,tam_lista_w),chr(38));

		if	((ie_pos_igual_w > ie_pos_ecom_w) and (ie_pos_ecom_w <> 0)) or
			(ie_pos_igual_w = 0) then
			ie_pos_menor_w	:= ie_pos_ecom_w;
		else
			ie_pos_menor_w	:= ie_pos_igual_w;
		end if;

		if	(ie_pos_menor_w <> 0) then

			ds_structure_w	:= substr(ds_codigo_w,1,(ie_pos_menor_w)-1);
			ds_codigo_w	:= substr(ds_codigo_w,(ie_pos_menor_w),tam_lista_w);
		else
			ds_structure_w	:= ds_codigo_w;
			ds_codigo_w	:= '';
		end if;


		select	max(substr(obter_ref_isbt_codigo(ds_structure_w),1,3))
		into	ds_referencia_w
		from	dual;
			
		if	(ds_referencia_w = '001') then
			ds_001_w := ds_structure_w;
		end if;
		if	(ds_referencia_w = '002') then
			ds_002_w := ds_structure_w;
		end if;
		if	(ds_referencia_w = '003') then
			ds_003_w := ds_structure_w;
		end if;
		if	(ds_referencia_w = '004') then
			ds_004_w := ds_structure_w;
		end if;
		if	(ds_referencia_w = '005') then
			ds_005_w := ds_structure_w;
		end if;
		if	(ds_referencia_w = '006') then
			ds_006_w := ds_structure_w;
		end if;
		if	(ds_referencia_w = '007') then
			ds_007_w := ds_structure_w;
		end if;
		if	(ds_referencia_w = '008') then
			ds_008_w := ds_structure_w;
		end if;
		if	(ds_referencia_w = '009') then
			ds_009_w := ds_structure_w;
		end if;
		if	(ds_referencia_w = '019') then
			ds_019_w := ds_structure_w;
		end if;


		ie_contador_w	:= ie_contador_w + 1;

		end;
	end loop;
end if;

if	(ds_001_w is not null) then
	cd_local_isbt_w	:= substr(ds_001_w,2,5);
	nr_seq_isbt_w	:= substr(ds_001_w,9,6);

	select	max(nr_sequencia)
	into	nr_seq_entidade_w
	from	san_entidade
	where	cd_local_isbt = cd_local_isbt_w;

end if;

if	(ds_002_w is not null) then
	select	count(*)
	into	qt_registro_grupo_w
	from	san_grupo_sanguineo_isbt
	where	cd_sangue_isbt = substr(ds_002_w,3,2);

	if	(nvl(qt_registro_grupo_w,0) > 0) then

		select	max(ie_tipo_sangue),
			max(ie_fator_rh),
			max(nr_seq_tipo_doacao)
		into	ie_tipo_sangue_w,
			ie_fator_rh_w,
			nr_seq_tipo_doacao_w
		from	san_grupo_sanguineo_isbt
		where	cd_sangue_isbt = substr(ds_002_w,3,2);
	else
		ds_inconsistencia_w	:= obter_desc_expressao(291139)||chr(13)||chr(10)||ds_inconsistencia_w;
	end if;
end if;

if	(ds_003_w is not null) then

	select	max(nr_seq_derivado),
		nvl(max(ie_lavado),'N'),
		nvl(max(ie_irradiado),'N'),
		nvl(max(ie_filtrado),'N'),
		nvl(max(ie_aliquotado),'N'),
		max(nr_seq_conservante)
	into	nr_seq_derivado_w,
		ie_lavado_w,
		ie_irradiado_w,
		ie_filtrado_w,
		ie_aliquotado_w,
		nr_seq_conservante_w
	from	san_derivado_bolsa
	where	nr_bolsa = substr(ds_003_w,3,5);

	cd_divisao_isbt_w	:= substr(ds_003_w,9,2);
	cd_isbt_tipo_doa_w	:= substr(ds_003_w,8,1);
end if;

if	(ds_004_w is not null) then
	begin
		select	to_date(to_char(to_date(substr( ds_004_w,6,3),'ddd'),'dd/mm')||'/'||to_char(to_date(substr( ds_004_w,1,3),'yyy'),'yyyy'))
		into	dt_vencimento_w
		from	dual;
	exception
	when others then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(288941)||chr(13)||chr(10);
	end;
end if;

if	(ds_005_w is not null) then
	begin
		select	to_date(to_char(to_date(substr( ds_005_w,6,3),'ddd'),'dd/mm')||'/'||to_char(to_date(substr( ds_005_w,3,3),'yyy'),'yyyy')
			||' '||to_char(to_date(substr( ds_005_w,9,4),'hh24mi'),'hh24:mi'),'dd/mm/yyyy hh24:mi:ss')
		into	dt_vencimento_w
		from	dual;
	exception
	when others then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(288941)||chr(13)||chr(10);
	end;

end if;

if	(ds_006_w is not null) then
	begin
		select	to_date(to_char(to_date(substr( ds_006_w,6,3),'ddd'),'dd/mm')||'/'||to_char(to_date(substr( ds_006_w,3,3),'yyy'),'yyyy'))
		into	dt_coleta_w
		from	dual;
	exception
	when others then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(288414)||chr(13)||chr(10);
	end;
	
end if;

if	(ds_007_w is not null) then
	begin
		select	to_date(to_char(to_date(substr( ds_007_w,6,3),'ddd'),'dd/mm')||'/'||to_char(to_date(substr( ds_007_w,3,3),'yyy'),'yyyy')
			||' '||to_char(to_date(substr( ds_007_w,9,4),'hh24mi'),'hh24:mi'),'dd/mm/yyyy hh24:mi:ss')
		into	dt_coleta_w
		from	dual;
	exception
	when others then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(288414)||chr(13)||chr(10);
	end;

end if;

if	(ds_008_w is not null) then
	begin
		select	to_date(to_char(to_date(substr( ds_008_w,6,3),'ddd'),'dd/mm')||'/'||to_char(to_date(substr( ds_008_w,3,3),'yyy'),'yyyy'))
		into	dt_producao_w
		from	dual;
	exception
	when others then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(612843)||chr(13)||chr(10);
	end;

end if;

if	(ds_009_w is not null) then
	begin
		select	to_date(to_char(to_date(substr( ds_009_w,6,3),'ddd'),'dd/mm')||'/'||to_char(to_date(substr( ds_009_w,3,3),'yyy'),'yyyy')
			||' '||to_char(to_date(substr( ds_009_w,9,4),'hh24mi'),'hh24:mi'),'dd/mm/yyyy hh24:mi:ss')
		into	dt_producao_w
		from	dual;
	exception
	when others then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(612843)||chr(13)||chr(10);
	end;

end if;

select	max(qt_dias_validade)
into	qt_dias_validade_w
from	san_derivado
where	nr_sequencia = nr_seq_derivado_w;

if	(dt_vencimento_w is null) then
	dt_vencimento_w := to_date( to_char(nvl(dt_producao_w,sysdate) + nvl(qt_dias_validade_w,0),'dd/mm/yyyy')|| ' '||VarHoraVencimentoComponente, 'dd/mm/yyyy hh24:mi:ss');
end if;

-- consistências

select	decode(count(*),0,'N','S')
into	ie_tipo_doa_cadast_w
from	san_tipo_doacao
where	upper(cd_isbt) = upper(cd_isbt_tipo_doa_w);

if	(ie_tipo_doa_cadast_w = 'N') then

	if	(ds_003_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(293660, 'CD_TIPO_P='|| to_char(cd_isbt_tipo_doa_w));
	end if;
end if;

select	max(nr_sequencia)
into	nr_seq_tipo_doa_deri_w
from	san_tipo_doacao
where	upper(cd_isbt) = upper(cd_isbt_tipo_doa_w);

if	(nr_seq_tipo_doacao_w <> nr_seq_tipo_doa_deri_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(293661);
end if;

if	( nr_seq_derivado_w is null) then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(291244)||chr(13)||chr(10);
end if;
if	( nr_seq_entidade_w is null) then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(289262)||chr(13)||chr(10);
end if;
if	( cd_local_isbt_w is null) then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(303984)||chr(13)||chr(10);
end if;

if	((ie_tipo_sangue_w is null) and
		(ie_fator_rh_w is null) and
		(nr_seq_tipo_doacao_w is null)) then
	ds_inconsistencia_w	:= ds_inconsistencia_w||obter_desc_expressao(291139)||chr(13)||chr(10);
end if;

select	decode(count(*),0,'N','S')
into	ie_ja_existe_w
from	san_producao
where	cd_identificacao_isbt		= substr(ds_001_w,2,13)
and	cd_produto_isbt			= substr(ds_003_w,3,5)
and	nvl(cd_divisao_isbt,'00')	= cd_divisao_isbt_w;

if	(ie_ja_existe_w = 'S') then

	select	max(nr_sequencia)
	into	nr_seq_producao_exist_w
	from	san_producao
	where	cd_identificacao_isbt		= substr(ds_001_w,2,13)
	and	cd_produto_isbt			= substr(ds_003_w,3,5)
	and	nvl(cd_divisao_isbt,'00')	= cd_divisao_isbt_w;
	
	wheb_mensagem_pck.exibir_mensagem_abort(294585,'NR_SEQ_PRODUCAO=' || nr_seq_producao_exist_w);
end if;

if	(ds_inconsistencia_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(294226,'DS_INCONSISTENCIA=' || ds_inconsistencia_w);
end if;

-- inserção
select	san_emprestimo_seq.nextval
into	nr_seq_emp_ent_w
from	dual;
	insert into san_emprestimo (
				nr_sequencia,
				nr_seq_entidade,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_entrada_saida,
				dt_emprestimo,
				cd_pf_realizou,
				cd_estabelecimento
			) values (
				nr_seq_emp_ent_w,
				nr_seq_entidade_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				'E',
				sysdate,
				substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10),
				cd_estabelecimento_p);

	if	(ie_liberar_w = 'N') then
		dt_liberacao_w	:= sysdate;
		nm_usuario_lib_w := nm_usuario_p;
	end if;

	insert into san_producao (
				nr_sequencia,
				nr_seq_derivado,
				nr_seq_emp_ent,
				dt_producao,
				cd_pf_realizou,
				dt_atualizacao,
				nm_usuario,
				dt_vencimento,
				nr_sangue,
				ie_irradiado,
				ie_lavado,
				ie_filtrado,
				ie_aliquotado,
				cd_estabelecimento,
				ie_tipo_sangue,
				ie_fator_rh,
				nr_seq_isbt,
				ie_aferese,
				ie_realiza_nat,
				ie_pai_reproduzido,
				ie_reproduzido,
				ie_bolsa_filha,
				ie_pool,
				ie_tipo_bloqueio,
				ie_em_reproducao,
				dt_liberacao,
				nm_usuario_lib,
				cd_identificacao_isbt,
				dt_coleta_isbt,
				nr_seq_tipo_doacao_isbt,
				cd_produto_isbt,
				cd_divisao_isbt,
				nr_seq_conservante
			) values (
				san_producao_seq.nextval,
				nr_seq_derivado_w,
				nr_seq_emp_ent_w,
				nvl(dt_producao_w,sysdate),
				substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10),
				sysdate,
				nm_usuario_p,
				dt_vencimento_w,
				nr_seq_isbt_w,
				ie_irradiado_w,
				ie_lavado_w,
				ie_filtrado_w,
				ie_aliquotado_w,
				cd_estabelecimento_p,
				ie_tipo_sangue_w,
				ie_fator_rh_w,
				nr_seq_isbt_w,
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				dt_liberacao_w,
				nm_usuario_lib_w,
				substr(ds_001_w,2,13),
				dt_coleta_w,
				nr_seq_tipo_doa_deri_w,
				substr(ds_003_w,3,5),
				cd_divisao_isbt_w,
				nr_seq_conservante_w);

commit;

nr_seq_emp_ent_p	:= nr_seq_emp_ent_w;

end san_inserir_hemocomp_isbt;
/
