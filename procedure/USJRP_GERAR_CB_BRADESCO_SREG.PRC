create or replace
procedure USJRP_GERAR_CB_BRADESCO_SREG
			(	nr_seq_cobr_escrit_p		number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 
/*	Vers�o 09
	Data: 11/03/2010
	------------------- Sem registro ----------------------
*/

/* Header */
ds_conteudo_w			varchar2(400);
cd_empresa_w			varchar2(20);
nm_empresa_w			varchar2(30);
dt_geracao_w			varchar2(6);
ds_branco_8_w			varchar2(8);
nr_seq_arquivo_w		varchar2(7);
ds_brancos_277_w		varchar2(277);
nr_seq_registro_w		varchar2(10);

/* Transa��o */
ie_digito_agencia_w		varchar2(1);
cd_agencia_debito_w		varchar2(5);
cd_agencia_bancaria_w		varchar2(5);
cd_conta_w			varchar2(7);
ie_digito_conta_w		varchar2(1);
id_empresa_w			varchar2(17);
nr_controle_partic_w		varchar2(25);
cd_banco_w			varchar2(3);
ie_multa_w			varchar2(1);
pr_multa_w			varchar2(4);
nr_dig_nosso_numero_w		varchar2(1);
vl_desconto_dia_w		varchar2(10);
cd_condicao_w			varchar2(1);
ie_emite_papeleta_w		varchar2(1);
ie_rateio_w			varchar2(1);
ie_endereco_w			varchar2(1);
ds_brancos_2_w			varchar2(2);
ie_ocorrencia_w			varchar2(2);
nr_documento_w			varchar2(10);
dt_vencimento_w			varchar2(6);
vl_titulo_w			varchar2(13);
cd_banco_cobranca_w		varchar2(3);
cd_agencia_deposito_w		varchar2(5);
ie_especie_w			varchar2(2);
ie_aceite_w			varchar2(1);
dt_emissao_w			varchar2(6);
ie_instrucao_1_w		varchar2(2);
ie_instrucao_2_w		varchar2(2);
vl_acrescimo_w			varchar2(13);
dt_desconto_w			varchar2(6);
vl_desconto_w			varchar2(13);
vl_iof_w			varchar2(13);
vl_abatimento_w			varchar2(13);
ie_tipo_inscricao_w		varchar2(2);
nr_inscricao_w			varchar2(14);
nm_sacado_w			varchar2(40);
ds_endereco_sacado_w		varchar2(40);
nm_avalista_w			varchar2(60);
cd_cep_w			varchar2(8);
nr_nosso_numero_w		varchar2(12);
ds_brancos_10_w			varchar2(10);
ds_branco_12_w			varchar2(12);
cd_carteira_w			varchar2(3);
nr_seq_reg_arquivo_w		number(10)	:= 1;
ds_mensagem_1_w			varchar2(80);
ds_mensagem_2_w			varchar2(80);
ds_mensagem_3_w			varchar2(80);
ds_mensagem_4_w			varchar2(80);
ie_origem_titulo_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
cd_cgc_w			varchar2(14);
ds_observacao_w			varchar2(80);	
nr_titulo_w			number(10);

/* Trailler */
ds_brancos_393_w		varchar2(393);

/*Contador*/
nr_seq_apres_w			number(10)	:= 1;
ie_gerar_cob_esc_prim_mens_w	varchar(1) 	:= null;


Cursor C01 is
	select	lpad(x.cd_agencia_bancaria,5, '0') cd_agencia_debito,
		lpad(substr(pls_obter_dados_pagador_fin(d.nr_seq_pagador,'DA'),1,1),1,' ') ie_digito_agencia,
		lpad(x.cd_agencia_bancaria,5, '0') cd_agencia_bancaria,
		lpad(x.cd_conta,7,'0') cd_conta,
		nvl(x.ie_digito_conta,'0') ie_digito_conta,
		'0' || lpad(nvl(pls_obter_dados_carteira_cob(b.nr_seq_carteira_cobr,'C'),'0'),3,'0') || lpad(x.cd_agencia_bancaria,5,'0') || lpad(x.cd_conta,7,'0') || 
															nvl(x.ie_digito_conta,'0') id_empresa,
		lpad(nvl(b.nr_titulo,'0'),25,'0') nr_controle_partic,
		--lpad(a.cd_banco,3,'0') cd_banco,
		'000' cd_banco,
		'0' ie_multa,
		lpad('0',4,'0') pr_multa,
		--lpad(to_char(b.nr_titulo),11,'0') nr_nosso_numero,
		lpad(replace(b.nr_nosso_numero,'-',''),12,'0') nr_nosso_numero,		
		--decode(calcula_digito('MODULO11_BRAD',lpad(substr(pls_obter_dados_carteira_cob(b.nr_seq_carteira_cobr,'C'),2,2),2,'0') || 
		--					lpad(b.nr_titulo,11,'0')),'-1','P',
		--	calcula_digito('MODULO11_BRAD',lpad(substr(pls_obter_dados_carteira_cob(b.nr_seq_carteira_cobr,'C'),2,2),2,'0') || 
		--					lpad(b.nr_titulo,11,0))) nr_dig_nosso_numero,
		lpad('0',10,'0') vl_desconto_dia,
		a.ie_emissao_bloqueto cd_condicao,
		'N' ie_emite_papeleta,
		' ' ie_rateio,
		'2' ie_endereco,
		lpad(nvl(c.cd_ocorrencia,'1'),2,'0') ie_ocorrencia,
		lpad(nvl(b.nr_documento,'0'),10,'0') nr_documento,
		to_char(nvl(b.dt_pagamento_previsto, b.dt_vencimento),'ddmmyy') dt_vencimento,
		replace(to_char(b.vl_titulo, 'fm00000000000.00'),'.','') vl_titulo,
		lpad('0',3,'0') cd_banco_cobranca,
		lpad('0',5,'0') cd_agencia_deposito,
		'12' ie_especie,
		'N' ie_aceite,
		to_char(b.dt_emissao,'ddmmyy') dt_emissao,
		lpad(nvl(c.cd_instrucao, '0'),2,'0') ie_instrucao1,
		'00' ie_instrucao2,
		replace(to_char(nvl(c.vl_acrescimo,0), 'fm00000000000.00'),'.','') vl_acrescimo,
		'000000' dt_desconto,
		replace(to_char(nvl(c.vl_desconto,0), 'fm00000000000.00'),'.','') vl_desconto,
		lpad('0',13,'0') vl_iof,
		lpad('0',13,'0') vl_abatimento,
		decode(b.cd_cgc, null,'01','02') ie_tipo_inscricao,
		lpad(nvl(b.cd_cgc_cpf,'0'),14, '0') nr_inscricao,
		rpad(upper(elimina_acentuacao(substr(b.nm_pessoa,1,40))),40, ' ') nm_sacado,
		rpad(substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'E'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'E')),1,10)  || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'NR'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'NR')),1,5) || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CO'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CO')),1,5) || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'B'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'B')),1,10) || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CI'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CI')),1,10) || ' ' ||
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'UF'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'UF')),1,2),40, ' ') ds_endereco_sacado,
		substr(decode(d.nr_sequencia,null,
			obter_dados_pf_pj(b.cd_pessoa_fisica, b.cd_cgc, 'CEP'),
			pls_obter_compl_pagador(d.nr_seq_pagador,'CEP')),1,8) cd_cep,
		lpad(' ',60,' ') nm_avalista,
		lpad(substr(nvl(a.nr_sequencia,''),1,6),6,'0') nr_seq_arquivo,
		lpad(nvl(substr(pls_obter_dados_carteira_cob(b.nr_seq_carteira_cobr,'C'),1,3),'0'),3,'0'),
		rpad(obter_linha_texto(g.ds_mensagem,1),80, ' ') ds_mensagem_1,
		rpad(obter_linha_texto(g.ds_mensagem,2),80, ' ') ds_mensagem_2,
		rpad(obter_linha_texto(g.ds_mensagem,3),80, ' ') ds_mensagem_3,
		rpad(obter_linha_texto(g.ds_mensagem,4),80, ' ') ds_mensagem_4,
		b.ie_origem_titulo,
		b.cd_pessoa_fisica,
		b.cd_cgc,
		b.nr_titulo
	from	pls_lote_mensalidade	z,
		titulo_receber_mensagem g,
		banco_estabelecimento	x,
		pls_contrato_pagador	f,
		banco_carteira		e,
		pls_mensalidade		d,
		titulo_receber_v	b,
		titulo_receber_cobr	c,
		cobranca_escritural	a
	where	a.nr_sequencia		= c.nr_seq_cobranca
	and	c.nr_titulo		= b.nr_titulo
	and	a.nr_seq_conta_banco	= x.nr_sequencia
	and	b.nr_seq_mensalidade	= d.nr_sequencia(+)
	and	b.nr_seq_carteira_cobr	= e.nr_sequencia(+)
	and	d.nr_seq_pagador	= f.nr_sequencia(+)
	and	c.nr_titulo		= g.nr_titulo(+)
	and	d.nr_seq_lote		= z.nr_sequencia(+)
	and	((z.ie_primeira_mensalidade is null or z.ie_primeira_mensalidade = 'N') 
	or	ie_gerar_cob_esc_prim_mens_w = 'S')
	and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

begin

delete from w_envio_banco where nm_usuario = nm_usuario_p;

/* Pega o par�metro para ver se considera os t�tulos gerados por lotes de primera mensalidade */
select	nvl(max(ie_gerar_cob_esc_prim_mens),'S')
into	ie_gerar_cob_esc_prim_mens_w
from	pls_parametros_cr
where	cd_estabelecimento = cd_estabelecimento_p;

select	lpad(' ',8,' '),
	lpad(' ',277,' '),
	lpad(' ',2,' '),
	lpad(' ',393,' '),
	lpad(' ',10,' '),
	lpad(' ',12,' ')
into	ds_branco_8_w,
	ds_brancos_277_w,
	ds_brancos_2_w,
	ds_brancos_393_w,
	ds_brancos_10_w,
	ds_branco_12_w
from	dual;


/* Header */
select	lpad(substr(c.cd_convenio_banco,1,20), 20,'0'),
	rpad(upper(elimina_acentuacao(substr(obter_nome_pf_pj(null, b.cd_cgc),1,30))),30, ' '),
	to_char(a.dt_remessa_retorno,'ddmmyy'),
	lpad(nvl(to_char(a.nr_remessa),'0'),7,'0')
into	cd_empresa_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_seq_arquivo_w
from	estabelecimento		b,
	cobranca_escritural	a,
	banco_estabelecimento	c
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.nr_sequencia		= nr_seq_cobr_escrit_p;

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;

ds_conteudo_w	:= 	'01' || 
			'REMESSA' || 
			'01' || 
			'COBRANCA       ' ||
			cd_empresa_w || 
			nm_empresa_w ||
			'237' || 
			'Bradesco       ' || 
			dt_geracao_w || 
			ds_branco_8_w || 
			'MX' || 
			nr_seq_arquivo_w ||
			ds_brancos_277_w || 
			lpad(nr_seq_reg_arquivo_w,6,'0');	

nr_seq_apres_w 	:= nr_seq_apres_w + 1;			
insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(nr_seq_registro_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_apres_w);
/* Fim Header */

/* Transa��o */
--begin
open C01;
loop
fetch C01 into	
	cd_agencia_debito_w,
	ie_digito_agencia_w,
	cd_agencia_bancaria_w,
	cd_conta_w,
	ie_digito_conta_w,
	id_empresa_w,
	nr_controle_partic_w,
	cd_banco_w,
	ie_multa_w,
	pr_multa_w,
	nr_nosso_numero_w,
	--nr_dig_nosso_numero_w,
	vl_desconto_dia_w,
	cd_condicao_w,
	ie_emite_papeleta_w,
	ie_rateio_w,
	ie_endereco_w,
	ie_ocorrencia_w,
	nr_documento_w,
	dt_vencimento_w,
	vl_titulo_w,
	cd_banco_cobranca_w,
	cd_agencia_deposito_w,
	ie_especie_w,
	ie_aceite_w,
	dt_emissao_w,
	ie_instrucao_1_w,
	ie_instrucao_2_w,
	vl_acrescimo_w,
	dt_desconto_w,
	vl_desconto_w,
	vl_iof_w,
	vl_abatimento_w,
	ie_tipo_inscricao_w,
	nr_inscricao_w,
	nm_sacado_w,
	ds_endereco_sacado_w,
	cd_cep_w,
	nm_avalista_w,
	nr_seq_arquivo_w,
	cd_carteira_w,
	ds_mensagem_1_w,
	ds_mensagem_2_w,
	ds_mensagem_3_w,
	ds_mensagem_4_w,
	ie_origem_titulo_w,
	cd_pessoa_fisica_w,
	cd_cgc_w,
	nr_titulo_w;
exit when C01%notfound;
	begin
	/*Transa��o Tipo 1*/
	select	w_envio_banco_seq.nextval
	into	nr_seq_registro_w
	from	dual;
	
	nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
	
	if	(ie_origem_titulo_w = '10') then
		if	(cd_pessoa_fisica_w is not null) then
			select	upper(rpad(regexp_replace(trim(substr(obter_compl_pf(cd_pessoa_fisica_w,2,'EN'),1, 35 - length(substr(obter_compl_pf(cd_pessoa_fisica_w,2,'NR'),1,5) ||  ' ' || substr(obter_compl_pf(cd_pessoa_fisica_w,2,'CO'),1,15) )   )), ' {2,40}', ' ')  || ' ' ||
				regexp_replace(substr(obter_compl_pf(cd_pessoa_fisica_w,2,'NR'),1,5) || ' ' ||
				substr(obter_compl_pf(cd_pessoa_fisica_w,2,'CO'),1,15) || ' ' ||
				substr(obter_compl_pf(cd_pessoa_fisica_w,2,'B'),1,30), ' {2,40}', ' ') ,40,' ')),
				substr(obter_compl_pf(cd_pessoa_fisica_w,2,'CEP'),1,8)
			into	ds_endereco_sacado_w,
				cd_cep_w
			from	dual;
		
		elsif	(cd_cgc_w is not null) then
			select	upper(rpad(regexp_replace(trim(substr(obter_compl_pj(cd_cgc_w,2,'EN'),1, 35 - length(substr(obter_compl_pj(cd_cgc_w,2,'NR'),1,5) ||  ' ' || substr(obter_compl_pj(cd_cgc_w,2,'CO'),1,15) )   )), ' {2,40}', ' ')  || ' ' ||
				regexp_replace(substr(obter_compl_pj(cd_cgc_w,2,'NR'),1,5) || ' ' ||
				substr(obter_compl_pj(cd_cgc_w,2,'CO'),1,15) || ' ' ||
				substr(obter_compl_pj(cd_cgc_w,2,'B'),1,30), ' {2,40}', ' ') ,40,' ')),
				substr(obter_compl_pj(cd_cgc_w,2,'CEP'),1,8)
			into	ds_endereco_sacado_w,
				cd_cep_w
			from	dual;
		end if;		
	end if;
	
	ds_conteudo_w	:=	'1' || 
				'00000' || 
				' ' || 
				'00000' || 
				'0000000' || 
				' ' || 
				id_empresa_w || 
				nr_controle_partic_w || 
				cd_banco_w || 
				ie_multa_w || 
				pr_multa_w || 
				nr_nosso_numero_w || 

				--nr_dig_nosso_numero_w || 
				vl_desconto_dia_w || 
				cd_condicao_w ||
				ie_emite_papeleta_w || 
				ds_brancos_10_w || 
				ie_rateio_w || 
				ie_endereco_w || 
				ds_brancos_2_w || 
				ie_ocorrencia_w || 
				nr_documento_w || 
				dt_vencimento_w ||
				vl_titulo_w || 
				cd_banco_cobranca_w || 
				cd_agencia_deposito_w || 
				ie_especie_w || 
				ie_aceite_w || 
				dt_emissao_w || 
				ie_instrucao_1_w ||
				ie_instrucao_2_w || 
				vl_acrescimo_w || 
				dt_desconto_w || 
				vl_desconto_w || 
				vl_iof_w || 
				vl_abatimento_w || 
				ie_tipo_inscricao_w ||
				nr_inscricao_w || 
				nm_sacado_w ||
				ds_endereco_sacado_w ||
				rpad(' ',12, ' ') ||
				nvl(cd_cep_w, '        ') ||
				nm_avalista_w ||
				lpad(nr_seq_reg_arquivo_w,6,'0');
				
	nr_seq_apres_w 	:= nr_seq_apres_w + 1;
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);		
	/*Fim Transa��o Tipo 1*/

	/*Transa��o Tipo 2*/			
	select	w_envio_banco_seq.nextval
	into	nr_seq_registro_w
	from	dual;
	
	nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;
	
	select	substr(max(a.ds_observacao),1,80)
	into	ds_observacao_w
	from	pls_evento_movimento	a,
		pls_lote_evento	c
	where	c.nr_sequencia		= a.nr_seq_lote
	and	a.nr_titulo_receber	= nr_titulo_w;
	
	if	(ds_mensagem_1_w is null) then
		ds_mensagem_1_w := ds_observacao_w;
		
	elsif	(ds_mensagem_2_w is null) then
		ds_mensagem_2_w := ds_observacao_w;
		
	elsif	(ds_mensagem_3_w is null) then
		ds_mensagem_3_w := ds_observacao_w;
		
	elsif	(ds_mensagem_4_w is null) then
		ds_mensagem_4_w := ds_observacao_w;
	end if;

	ds_conteudo_w	:=	'2' ||
				rpad(nvl(ds_mensagem_1_w, ' '), 80, ' ') ||
				rpad(nvl(ds_mensagem_2_w, ' '), 80, ' ') ||
				rpad(nvl(ds_mensagem_3_w, ' '), 80, ' ') ||
				rpad(nvl(ds_mensagem_4_w, ' '), 80, ' ') ||
				rpad(' ', 6, ' ') ||
				rpad(' ', 13, ' ') ||
				rpad(' ', 6, ' ') ||
				rpad(' ', 13, ' ') ||
				rpad(' ', 7, ' ') ||
				cd_carteira_w ||
				cd_agencia_debito_w ||
				cd_conta_w ||
				ie_digito_conta_w ||
				nr_nosso_numero_w ||
				--nr_dig_nosso_numero_w ||
				lpad(nr_seq_reg_arquivo_w,6,'0');
				
	nr_seq_apres_w	:= nr_seq_apres_w + 1;
	insert into w_envio_banco
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_estabelecimento,
		ds_conteudo,
		nr_seq_apres)
	values	(nr_seq_registro_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_estabelecimento_p,
		ds_conteudo_w,
		nr_seq_apres_w);
	/*Fim Transa��o Tipo 2*/		
	end;
end loop;
close C01;
/* Fim Transa��o */

/* Trailler */
nr_seq_reg_arquivo_w	:= nr_seq_reg_arquivo_w + 1;

ds_conteudo_w	:= '9' ||
		   ds_brancos_393_w || 
		   lpad(nr_seq_reg_arquivo_w,6,'0');	

select	w_envio_banco_seq.nextval
into	nr_seq_registro_w
from	dual;


nr_seq_apres_w	:= nr_seq_apres_w + 1;		
insert into w_envio_banco
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	cd_estabelecimento,
	ds_conteudo,
	nr_seq_apres)
values	(nr_seq_registro_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	cd_estabelecimento_p,
	ds_conteudo_w,
	nr_seq_apres_w);


/* Fim Trailler*/


commit;

end USJRP_GERAR_CB_BRADESCO_SREG;
/
