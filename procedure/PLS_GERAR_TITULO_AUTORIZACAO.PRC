create or replace
procedure PLS_gerar_titulo_autorizacao(	cd_estabelecimento_p	number,
					nr_seq_segurado_p	number,
					nr_seq_guia_p		number,
					vl_autorizacao_p	number,
					nr_titulo_p	OUT	number,
					nm_usuario_p		Varchar2) is 

cd_moeda_padrao_w		number(10,0);
cd_pessoa_fisica_w		varchar2(10);
nr_titulo_w			number(10);
cd_portador_w			number(10);
cd_tipo_portador_w		number(10);
cd_estab_financeiro_w		number(10);

cd_tipo_taxa_juros_w		number(10)	:= null;
tx_juros_w			number(7,4)	:= null;
cd_tipo_taxa_multa_w		number(10)	:= null;
tx_multa_w			number(7,4)	:= null;
ie_origem_titulo_w		varchar2(10)	:= null;
nr_seq_conta_banco_w		number(10)	:= null;

nr_seq_trans_fin_baixa_w	number(10,0);
nr_seq_carteira_cobr_w		number(10);
			
begin

select	cd_moeda_padrao,
	nr_seq_trans_fin_baixa,
	cd_tipo_portador,
	cd_portador
into	cd_moeda_padrao_w,
	nr_seq_trans_fin_baixa_w,
	cd_tipo_portador_w,
	cd_portador_w
from	parametro_contas_receber
where	cd_estabelecimento = cd_estabelecimento_p;

select	max(cd_pessoa_fisica)
into	cd_pessoa_fisica_w
from	pls_segurado
where	nr_sequencia = nr_seq_segurado_p;

select	nvl(cd_estab_financeiro, cd_estabelecimento)
into	cd_estab_financeiro_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

begin
select	cd_tipo_taxa_juro,
	pr_juro_padrao,
	cd_tipo_taxa_multa,
	pr_multa_padrao,
	nvl(ie_origem_titulo,3),
	nvl(nr_seq_conta_banco_w,nr_seq_conta_banco),
	nvl(cd_tipo_portador_w,cd_tipo_portador),
	nvl(cd_portador_w,cd_portador)
into	cd_tipo_taxa_juros_w,
	tx_juros_w,
	cd_tipo_taxa_multa_w,
	tx_multa_w,
	ie_origem_titulo_w,
	nr_seq_conta_banco_w,
	cd_tipo_portador_w,
	cd_portador_w
from	pls_parametros
where	cd_estabelecimento = cd_estabelecimento_p;
exception
	when no_data_found then
	wheb_mensagem_pck.exibir_mensagem_abort(265260,'');
	--Cadastro de juros e multa n�o informados nos par�metros do Plano de Sa�de!
end;

select	titulo_seq.nextval
into	nr_titulo_w
from	dual;

insert into titulo_receber(
	nr_titulo, 
	cd_estabelecimento, 
	dt_atualizacao,
	nm_usuario, 
	dt_emissao, 
	dt_vencimento,
	dt_pagamento_previsto, 
	vl_titulo, 
	vl_saldo_titulo,
	cd_moeda, 
	cd_portador, 
	cd_tipo_portador,
	ie_situacao, 
	ie_tipo_emissao_titulo, 
	ie_origem_titulo,
	ie_tipo_titulo, 
	ie_tipo_inclusao, 
	cd_pessoa_fisica,
	cd_cgc, 
	tx_juros, 
	cd_tipo_taxa_juro,
	tx_multa, 
	cd_tipo_taxa_multa, 
	nr_seq_conta_banco,
	cd_estab_financeiro, 
	ds_observacao_titulo, 
	nr_seq_guia,
	ie_pls,	
	nr_seq_trans_fin_contab, 
	nr_seq_trans_fin_baixa,
	nm_usuario_orig, 
	dt_inclusao, 
	vl_saldo_juros,
	vl_saldo_multa, 
	tx_desc_antecipacao, 
	nr_seq_carteira_cobr)
values (nr_titulo_w, 
	cd_estabelecimento_p, 
	sysdate,
	nm_usuario_p, 
	sysdate, 
	add_months(trunc(sysdate,'dd'),1),
	add_months(trunc(sysdate,'dd'),1), 
	vl_autorizacao_p,
	vl_autorizacao_p,
	cd_moeda_padrao_w,
	cd_portador_w, 
	cd_tipo_portador_w,
	1, 
	1, 
	9,
	1, 
	'2', 
	cd_pessoa_fisica_w,
	null, 
	nvl(tx_juros_w,0), 
	nvl(cd_tipo_taxa_juros_w,0),
	nvl(tx_multa_w,0), 
	nvl(cd_tipo_taxa_multa_w,0), 
	null,
	cd_estab_financeiro_w, 
	null, 
	nr_seq_guia_p,
	'S', 
	nr_seq_trans_fin_baixa_w, 
	nr_seq_trans_fin_baixa_w,
	nm_usuario_p, 
	sysdate, 
	0,
	0, 
	0, 
	nr_seq_carteira_cobr_w);

commit;

nr_titulo_p := nr_titulo_w;

end PLS_gerar_titulo_autorizacao;
/
