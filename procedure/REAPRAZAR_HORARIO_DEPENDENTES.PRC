create or replace
procedure reaprazar_horario_dependentes (
                    nr_atendimento_p        number,
                    nr_prescricao_p         number,
                    nr_seq_item_p           number,
                    nr_agrupamento_p        number,
                    dt_horario_p            date,
                    dt_horario_reaprazar_p  date,
                    qt_hor_reaprazar_p      number,
                    nm_usuario_p            varchar2,
                    nr_seq_solucao_p        number default null,
                    ie_tipo_item_p          varchar2 default null ) is
					
nr_seq_horario_w	number(10,0);
nr_seq_turno_w		number(10,0);
nr_agrupamento_w	prescr_material.nr_agrupamento%type;
nr_seq_dil_reconst_w	prescr_material.nr_sequencia_diluicao%type;

cursor c02 is
select	a.nr_sequencia nr_seq_horario,
		a.ie_agrupador ie_agrupador,
		nvl(b.nr_sequencia_diluicao,0) nr_sequencia_diluicao,
		a.nr_seq_material nr_seq_dil_reconst
from	prescr_mat_hor a,
		prescr_material b
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_material = b.nr_sequencia
and		a.nr_prescricao	= nr_prescricao_p
and		a.nr_seq_superior	= nr_seq_item_p
and		a.dt_horario		= dt_horario_p
and		a.ie_agrupador	in (2,3,7,9)
and     a.nr_sequencia not in (select x.cd_registro from table(lista_pck.obter_lista_char(wheb_assist_pck.get_nr_seq_hor_reapraz())) x)
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
order by
	a.nr_sequencia;
	
cursor c03 is
select	nr_sequencia
from	prescr_mat_hor
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_material	> nr_seq_item_p
and	nr_agrupamento	= nr_agrupamento_p
and	dt_horario		= dt_horario_p
and	ie_agrupador	= 1
and nr_sequencia not in (select x.cd_registro from table(lista_pck.obter_lista_char(wheb_assist_pck.get_nr_seq_hor_reapraz())) x)
and	Obter_se_horario_liberado(dt_lib_horario, dt_horario) = 'S'
order by
	nr_sequencia;
	
cursor c04 is
select	a.nr_sequencia
from	prescr_material b,
	prescr_mat_hor a
where	b.nr_sequencia	= a.nr_seq_material
and	b.nr_prescricao	= a.nr_prescricao
and	a.nr_prescricao	= nr_prescricao_p
and	b.nr_seq_kit in (	select	nr_sequencia
						from	prescr_material
						where	nr_prescricao = nr_prescricao_p
						and		nr_agrupamento = nr_agrupamento_w)
and	a.dt_horario	= dt_horario_p
and a.nr_sequencia not in (select x.cd_registro from table(lista_pck.obter_lista_char(wheb_assist_pck.get_nr_seq_hor_reapraz())) x)
and	Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
order by
	a.nr_sequencia;					
	
cursor c05 is
select	a.nr_sequencia
from	prescr_material b,
		prescr_mat_hor a
where	b.nr_sequencia	= a.nr_seq_material
and		b.nr_prescricao	= a.nr_prescricao
and		a.nr_prescricao	= nr_prescricao_p
and		a.dt_horario	= dt_horario_p
and     a.nr_sequencia not in (select x.cd_registro from table(lista_pck.obter_lista_char(wheb_assist_pck.get_nr_seq_hor_reapraz())) x)
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
and		a.nr_seq_superior in 	(	select	c.nr_seq_material
									from	prescr_mat_hor c
									where	c.nr_prescricao  = nr_prescricao_p
									and		c.nr_seq_solucao = nr_seq_solucao_p
									and		c.ie_agrupador = 4)
and		a.nr_seq_superior is not null
and		nr_seq_solucao_p is not null
order by
	a.nr_sequencia;			
    
-- itens dependentes gasoterapia
cursor c06 is
select  c.rowid id,
        c.nr_sequencia nr_seq_horario
from    prescr_material	a,
        intervalo_prescricao b,
        prescr_mat_hor c
where   a.cd_intervalo          = b.cd_intervalo
and     a.nr_prescricao         = c.nr_prescricao
and     a.nr_sequencia          = c.nr_seq_material
and     a.nr_prescricao         = nr_prescricao_p
and     a.nr_seq_gasoterapia    = nr_seq_item_p
and     c.dt_horario            = dt_horario_p
and     a.ie_agrupador          = 15
and     c.nr_sequencia not in (select x.cd_registro from table(lista_pck.obter_lista_char(wheb_assist_pck.get_nr_seq_hor_reapraz())) x)
order by c.nr_sequencia;

-- itens associados da diluicao/reconstituinte
cursor cAssocDilReconst is
select	a.nr_sequencia nr_seq_horario
from	prescr_mat_hor a
where	a.nr_prescricao	= nr_prescricao_p
and		a.nr_seq_superior	= nr_seq_dil_reconst_w
and		a.dt_horario		= dt_horario_p
and		a.ie_agrupador	in (1,2,3,7,9)
and     a.nr_sequencia not in (select x.cd_registro from table(lista_pck.obter_lista_char(wheb_assist_pck.get_nr_seq_hor_reapraz())) x)
and		Obter_se_horario_liberado(a.dt_lib_horario, a.dt_horario) = 'S'
order by
	nr_sequencia;

begin

if	(nr_atendimento_p is not null) and
	(nr_prescricao_p is not null) and
	(nr_seq_item_p is not null) and
	((nr_agrupamento_p is not null) or
        (ie_tipo_item_p is not null and
            (ie_tipo_item_p = 'O'))) and
	(dt_horario_p is not null) and
	(dt_horario_reaprazar_p is not null) and
	(qt_hor_reaprazar_p is not null) and
	(nm_usuario_p is not null) then
						
	select 	max(nr_seq_turno)
	into	nr_seq_turno_w
	from 	prescr_mat_hor
	where 	nr_prescricao = nr_prescricao_p
	and		nr_seq_material = nr_seq_item_p
	and		dt_horario = dt_horario_reaprazar_p;
	
	select	max(nr_agrupamento)
	into	nr_agrupamento_w
	from	prescr_material
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia = nr_seq_item_p;
	
	for c02_w in c02 loop
		begin
			if (c02_w.nr_sequencia_diluicao > 0) then
				nr_seq_dil_reconst_w := c02_w.nr_seq_dil_reconst;
				for cAssocDilReconst_w in cAssocDilReconst loop
					begin
						update	prescr_mat_hor
						set		dt_horario					= dt_horario_reaprazar_p,
								nm_usuario					= nm_usuario_p,
								qt_hor_reaprazamento		= qt_hor_reaprazar_p,
								nm_usuario_reaprazamento	= nm_usuario_p,
								nr_seq_turno				= nr_seq_turno_w
						where	nr_sequencia				= cAssocDilReconst_w.nr_seq_horario;
                        
                        wheb_assist_pck.set_nr_seq_hor_reapraz(cAssocDilReconst_w.nr_seq_horario);
					end;
				end loop;
			end if;
			
			if (c02_w.ie_agrupador <> 2) then
				update	prescr_mat_hor
				set		dt_horario					= dt_horario_reaprazar_p,
						nm_usuario					= nm_usuario_p,
						qt_hor_reaprazamento		= qt_hor_reaprazar_p,
						nm_usuario_reaprazamento	= nm_usuario_p,
						nr_seq_turno				= nr_seq_turno_w
				where	nr_sequencia				= c02_w.nr_seq_horario;
                
                wheb_assist_pck.set_nr_seq_hor_reapraz(c02_w.nr_seq_horario);
			end if;
		end;
	end loop;
	
	open c03;
	loop
	fetch c03 into	nr_seq_horario_w;
	exit when c03%notfound;
		begin
		
		update	prescr_mat_hor
		set	dt_horario					= dt_horario_reaprazar_p,
			nm_usuario					= nm_usuario_p,
			qt_hor_reaprazamento		= qt_hor_reaprazar_p,
			nm_usuario_reaprazamento	= nm_usuario_p,
			nr_seq_turno				= nr_seq_turno_w
		where	nr_sequencia			= nr_seq_horario_w;
	
        wheb_assist_pck.set_nr_seq_hor_reapraz(nr_seq_horario_w);
		end;
	end loop;
	close c03;
	
	open c04;
	loop
	fetch c04 into	nr_seq_horario_w;
	exit when c04%notfound;
		begin
		
		update	prescr_mat_hor
		set		dt_horario					= dt_horario_reaprazar_p,
				nm_usuario					= nm_usuario_p,
				qt_hor_reaprazamento		= qt_hor_reaprazar_p,
				nm_usuario_reaprazamento	= nm_usuario_p,
				nr_seq_turno				= nr_seq_turno_w
		where	nr_sequencia				= nr_seq_horario_w;
	
        wheb_assist_pck.set_nr_seq_hor_reapraz(nr_seq_horario_w);
		end;
	end loop;
	close c04;
	
	open C05;
	loop
	fetch C05 into	
		nr_seq_horario_w;
	exit when C05%notfound;
		begin
		
		update	prescr_mat_hor
		set		dt_horario					= dt_horario_reaprazar_p,
				nm_usuario					= nm_usuario_p,
				qt_hor_reaprazamento		= qt_hor_reaprazar_p,
				nm_usuario_reaprazamento	= nm_usuario_p				
		where	nr_sequencia				= nr_seq_horario_w;
		
        wheb_assist_pck.set_nr_seq_hor_reapraz(nr_seq_horario_w);
		end;
	end loop;
	close C05;
    
    for c06_w in c06 loop
        begin

        update  prescr_mat_hor
        set     dt_horario                  = dt_horario_reaprazar_p,
                nm_usuario                  = nm_usuario_p,
                qt_hor_reaprazamento        = qt_hor_reaprazar_p,
                nm_usuario_reaprazamento    = nm_usuario_p
        where   rowid                       = c06_w.id;

        wheb_assist_pck.set_nr_seq_hor_reapraz(c06_w.nr_seq_horario);
        end;
    end loop;
	
end if;

commit;

end reaprazar_horario_dependentes;
/