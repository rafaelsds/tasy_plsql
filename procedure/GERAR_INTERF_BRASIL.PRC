Create or replace
procedure Gerar_interf_brasil	(nr_seq_envio_p		number,
				nm_usuario_p		varchar2) is

tp_registro_w			number(3);
nr_inscricao_w			varchar(14);
cd_agencia_w			number(8);
ie_dig_agencia_w			varchar(5);
cd_conta_w			varchar(15);
ie_dig_conta_w			varchar(5);
ds_banco_w			varchar(40);
dt_arquivo_w			date;
nr_seq_envio_w			number(10);
ie_tipo_fornecedor_w		varchar(1);
cd_cgc_fornecedor_w		varchar(14);
nm_hospital_w			varchar(80);
ds_endereco_w			varchar(100);
nr_endereco_w			varchar(10);
ds_cidade_w			varchar(40);
cd_cep_w			varchar(10);
ds_estado_w			varchar2(15);
cd_banco_fornecedor_w		number(5);
cd_agencia_conta_w		varchar(25);
nm_fornecedor_w			varchar(80);
nr_documento_w			number(10);
dt_vencimento_w			date;
vl_saldo_titulo_w			number(15,2);
ie_cod_barras_w			varchar(100);
vl_titulo_w			number(15,2);
vl_desconto_w			number(15,2);
vl_acrescimo_w			number(15,2);
dt_pagto_w			date;
vl_pagto_w			number(15,2);
vl_total_pagto_w			number(15,2);
ie_forma_pagto_w			varchar(50);

nr_seq_apres_w			number(10)	:= 0;

nr_lote_w				number(4)	:= 1;
nr_registro_w			number(5)	:= 1;
qt_lote_arquivo_w			number(10)	:= 0;
qt_registros_w			number(10)	:= 0;
nr_Seq_reg_lote_w			number(10)	:= 3;

cursor	c01 is
SELECT	tp_registro,
	nr_inscricao,
	cd_agencia,
	ie_dig_agencia,
	cd_conta,
	ie_dig_conta,
	ds_banco,
	dt_arquivo,
	nr_seq_envio,
	ie_tipo_fornecedor,
	cd_cgc_fornecedor,
	nm_hospital,
	ds_endereco,	
	nr_endereco,
	ds_cidade,
	cd_cep,
	ds_estado,
	cd_banco_fornecedor,
	cd_agencia_conta,
	nm_fornecedor,
	nr_documento,
	dt_vencimento,
	vl_saldo_titulo,
	ie_cod_barras,
	vl_titulo,
	vl_desconto,
	vl_acrescimo,
	dt_pagto,
	vl_pagto,
	vl_total_pagto,
	ie_forma_pagto
FROM	(
/* Header do Arquivo*/
SELECT	0 tp_registro,
	substr(a.cd_cgc,1,14) nr_inscricao,
	somente_numero(g.cd_agencia_bancaria) cd_agencia,
	TO_CHAR(calcula_digito('Modulo11',g.cd_agencia_bancaria)) ie_dig_agencia,
	substr(g.cd_conta,1,15) cd_conta,
	SUBSTR(g.ie_digito_conta,1,1) ie_dig_conta,
	substr(UPPER(g.ds_banco),1,40) ds_banco,
	sysdate dt_arquivo,
	e.nr_sequencia nr_seq_envio,
	'0' ie_tipo_fornecedor,
	'0' cd_cgc_fornecedor,
	substr(UPPER(p.ds_razao_social),1,80) nm_hospital,
	'' ds_endereco,	
	'0' nr_endereco,
	'' ds_cidade,
	'0' cd_cep,
	'' ds_estado,
	0 cd_banco_fornecedor,
	'' cd_agencia_conta,
	'' nm_fornecedor,
	0 nr_documento,
	SYSDATE dt_vencimento,
	0 vl_saldo_titulo,
	'' ie_cod_barras,
	0 vl_titulo,
	0 vl_desconto,
	0 vl_acrescimo,
	SYSDATE dt_pagto,
	0 vl_pagto,
	0 vl_total_pagto,
	'0' ie_forma_pagto
FROM	estabelecimento a,
	banco_estabelecimento_v g,
	pessoa_juridica p,
	banco_escritural e
WHERE	e.cd_estabelecimento   	= a.cd_estabelecimento
AND	a.cd_cgc		= p.cd_cgc
AND	g.nr_sequencia		= e.nr_seq_conta_banco
AND	g.ie_tipo_relacao      	IN ('EP','ECC')
AND	e.nr_sequencia		= nr_seq_envio_p
UNION
/* Header do Lote Documento */
SELECT	DISTINCT 1 tp_registro,
	'0' nr_inscricao,
	somente_numero(b.cd_agencia_bancaria) cd_agencia,
	TO_CHAR(calcula_digito('Modulo11',b.cd_agencia_bancaria)) ie_dig_agencia,
	substr(b.cd_conta,1,15) cd_conta,
	SUBSTR(b.ie_digito_conta,1,1) ie_dig_conta,
	'' ds_banco,
	sysdate dt_arquivo,
	a.nr_sequencia nr_seq_envio,
	'2' ie_tipo_fornecedor,
	substr(e.cd_cgc,1,14) cd_cgc_fornecedor,
	substr(UPPER(j.ds_razao_social),1,80) nm_hospital,
	substr(UPPER(j.ds_endereco),1,40) ds_endereco,
	substr(j.nr_endereco,1,10) nr_endereco,
	substr(j.ds_municipio,1,40) ds_municipio,
	substr(j.cd_cep,1,10) cd_cep,
	substr(j.sg_estado,1,2) ds_estado,
	DECODE(c.cd_banco,001,1,2) cd_banco_fornecedor,
	'' cd_agencia_conta,
	UPPER(j.ds_razao_social) nm_fornecedor,
	0 nr_documento,
	SYSDATE dt_vencimento,
	0 vl_saldo_titulo,
	'' ie_cod_barras,
	0 vl_titulo,
	0 vl_desconto,
	0 vl_acrescimo,
	SYSDATE dt_pagto,
	0 vl_pagto,
	0 vl_total_pagto,
	DECODE(c.cd_banco,001,'30','31') ie_forma_pagto
FROM	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento b,
	banco_escritural a,
	titulo_pagar_v2 d,
	titulo_pagar_escrit c
WHERE	c.nr_titulo             = d.nr_titulo
AND	e.cd_cgc		= j.cd_cgc
AND	a.nr_sequencia          = c.nr_seq_escrit
AND	a.nr_seq_conta_banco	= b.nr_sequencia
AND	a.cd_estabelecimento    = e.cd_estabelecimento
AND	b.ie_tipo_relacao       IN ('EP','ECC')
AND 	c.ie_tipo_pagamento	= 'DOC'
AND	c.nr_seq_escrit		= nr_seq_envio_p
UNION
/* Detalhe Documento */
SELECT	2 tp_registro,
	'0' nr_inscricao,
	somente_numero(substr(c.cd_agencia_bancaria,1,8)) cd_agencia,
	'' ie_dig_agencia,
	'0' cd_conta,
	'' ie_dig_conta,
	'' ds_banco,
	sysdate dt_arquivo,
	a.nr_sequencia  nr_seq_envio,
	'0' ie_tipo_fornecedor,
	substr(d.cd_favorecido,1,14) cd_cgc_fornecedor,
	'' nm_hospital,
	'' ds_endereco,
	'0' nr_endereco,
	'' ds_cidade,
	'0' cd_cep,
	'' ds_estado,
	c.cd_banco cd_banco_fornecedor,
	'0' || c.cd_agencia_bancaria || '0000000' || c.nr_conta  cd_agencia_conta,
	substr(d.nm_favorecido,1,80) nm_fornecedor,
	d.nr_titulo nr_documento,
	d.dt_vencimento_atual dt_vencimento,
	d.vl_saldo_titulo,
	'' ie_cod_barras,
	0 vl_titulo,
	0 vl_desconto,
	0 vl_acrescimo,
	SYSDATE dt_pagto,
	0 vl_pagto,
	0 vl_total_pagto,
	DECODE(c.cd_banco,001,'30','31') ie_forma_pagto
FROM	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento b,
	banco_escritural a,
	titulo_pagar_v2 d,
	titulo_pagar_escrit c
WHERE	c.nr_titulo             = d.nr_titulo
AND	e.cd_cgc		= j.cd_cgc
AND	a.nr_sequencia          = c.nr_seq_escrit
AND	a.nr_seq_conta_banco	= b.nr_sequencia
AND	a.cd_estabelecimento    = e.cd_estabelecimento
AND	b.ie_tipo_relacao       IN ('EP','ECC')
AND 	c.ie_tipo_pagamento	= 'DOC'
AND	c.nr_seq_escrit		= nr_seq_envio_p
UNION
/* Trailler do Lote Documento */
SELECT	3 tp_registro,
	'0' nr_inscricao,
	0 cd_agencia,
	'' ie_dig_agencia,
	'0' cd_conta,
	'' ie_dig_conta,
	'' ds_banco,
	sysdate dt_arquivo,
	e.nr_sequencia  nr_seq_envio,
	'0' ie_tipo_fornecedor,
	'0' cd_cgc_fornecedor,
	'' nm_hospital,
	'' ds_endereco,
	'0' nr_endereco,
	'' ds_cidade,
	'0' cd_cep,
	'' ds_estado,
	0 cd_banco_fornecedor,
	'' cd_agencia_conta,
	'' nm_fornecedor,
	0 nr_documento,
	SYSDATE dt_vencimento,
	0 vl_saldo_titulo,
	'' ie_cod_barras,
	0 vl_titulo,
	0 vl_desconto,
	0 vl_acrescimo,
	SYSDATE dt_pagto,
	0 vl_pagto,
	SUM(c.vl_escritural - c.vl_desconto + c.vl_acrescimo) vl_total_pagto,
	DECODE(c.cd_banco,001,'30','31') ie_forma_pagto
FROM	banco_estabelecimento b,
	estabelecimento a,      
	banco_escritural e,
	titulo_pagar_escrit c
WHERE	e.nr_sequencia		= c.nr_seq_escrit
AND	e.nr_seq_conta_banco	= b.nr_sequencia
AND	e.cd_estabelecimento    = a.cd_estabelecimento
AND	b.ie_tipo_relacao      	IN ('EP','ECC')
AND 	c.ie_tipo_pagamento	= 'DOC'
AND	c.nr_seq_escrit		= nr_seq_envio_p
GROUP BY e.nr_sequencia,
			DECODE(c.cd_banco,001,'30','31')
UNION
/* Header do Lote Bloquetos*/
SELECT	DISTINCT 4 tp_registro,
	'0' nr_inscricao,
	somente_numero(substr(b.cd_agencia_bancaria,1,8)) cd_agencia,
	TO_CHAR(calcula_digito('Modulo11',b.cd_agencia_bancaria)) ie_dig_agencia,
	substr(b.cd_conta,1,15) cd_conta,
	SUBSTR(b.ie_digito_conta,1,1) ie_dig_conta,
	'' ds_banco,
	sysdate dt_arquivo,
	a.nr_sequencia nr_seq_envio,
	'2' ie_tipo_fornecedor,
	substr(e.cd_cgc,1,14) cd_cgc_fornecedor,
	substr(UPPER(j.ds_razao_social),1,80) nm_hospital,
	substr(UPPER(j.ds_endereco),1,40) ds_endereco,
	substr(j.nr_endereco,1,10) nr_endereco,
	substr(j.ds_municipio,1,40),
	substr(j.cd_cep,1,10),
	substr(j.sg_estado,1,2) ds_estado,
	0 cd_banco_fornecedor,
	'' cd_agencia_conta,
	substr(UPPER(j.ds_razao_social),1,80) nm_fornecedor,
	0 nr_documento,
	SYSDATE dt_vencimento,
	0 vl_saldo_titulo,
	'' ie_cod_barras,
	0 vl_titulo,
	0 vl_desconto,
	0 vl_acrescimo,
	SYSDATE dt_pagto,
	0 vl_pagto,
	0 vl_total_pagto,
	DECODE(c.cd_banco,001,'30','31') ie_forma_pagto
FROM	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento b,
	banco_escritural a,
	titulo_pagar_escrit c
WHERE	e.cd_cgc		= j.cd_cgc
AND	a.nr_sequencia          = c.nr_seq_escrit
AND	a.nr_seq_conta_banco	= b.nr_sequencia
AND	a.cd_estabelecimento    = e.cd_estabelecimento
AND	b.ie_tipo_relacao       IN ('EP','ECC')
AND 	c.ie_tipo_pagamento	= 'BLQ'
AND	c.nr_seq_escrit		= nr_seq_envio_p
UNION
/* Detalhe Bloquetos */
SELECT	5 tp_registro,
	'0' nr_inscricao,
	somente_numero(substr(c.cd_agencia_bancaria,1,8)) cd_agencia,
	'' ie_dig_agencia,
	c.nr_conta cd_conta,
	'0' ie_dig_conta,
	'' ds_banco,
	sysdate dt_arquivo,
	a.nr_sequencia nr_seq_envio,
	substr(d.ie_tipo_favorecido,1,1) ie_tipo_fornecedor,
	substr(d.cd_favorecido,1,14) cd_cgc_fornecedor,
	'' nm_hospital,
	'' ds_endereco,
	'0' nr_endereco,
	'' ds_cidade,
	'0' cd_cep,
	'' ds_estado,
	c.cd_banco cd_banco_fornecedor,
	'0' || c.cd_agencia_bancaria || '0000000' || c.nr_conta  cd_agencia_conta,
	substr(d.nm_favorecido,1,80) nm_fornecedor,
	d.nr_titulo nr_documento,
	d.dt_vencimento_atual dt_vencimento,
	d.vl_saldo_titulo vl_saldo_titulo,
	d.nr_bloqueto ie_cod_barras,
	d.vl_titulo,
	c.vl_desconto,
	c.vl_acrescimo,
	SYSDATE dt_pagto,
	d.vl_saldo_titulo vl_pagto,
	0 vl_total_pagto,
	DECODE(c.cd_banco,001,'30','31') ie_forma_pagto
FROM	Estabelecimento e,
	banco_estabelecimento b,
	banco_escritural a,
	titulo_pagar_v2 d,	
	titulo_pagar_escrit c
WHERE	c.nr_titulo		= d.nr_titulo
AND	a.nr_sequencia		= c.nr_seq_escrit
AND	a.nr_seq_conta_banco	= b.nr_sequencia
AND	a.cd_estabelecimento	= e.cd_estabelecimento
AND 	b.ie_tipo_relacao	IN ('EP','ECC')
AND 	c.ie_tipo_pagamento	= 'BLQ'
AND	c.nr_seq_escrit		= nr_seq_envio_p
UNION
/* Trailler do Lote Bloquetos*/
SELECT	6 tp_registro,
	'0' nr_inscricao,
	0 cd_agencia,
	'' ie_dig_agencia,
	'0' cd_conta,
	'0' ie_dig_conta,
	'' ds_banco,
	sysdate dt_arquivo,
	e.nr_sequencia nr_seq_envio,
	'0' ie_tipo_fornecedor,
	'0' cd_cgc_fornecedor,
	'' nm_hospital,
	'' ds_endereco,
	'0' nr_endereco,
	'' ds_cidade,
	'0' cd_cep,
	'' ds_estado,
	0 cd_banco_fornecedor,
	'' cd_agencia_conta,
	'' nm_fornecedor,
	0 nr_documento,
	SYSDATE dt_vencimento,
	0 vl_saldo_titulo,
	'' ie_cod_barras,
	SUM(d.vl_titulo) vl_titulo,
	SUM(c.vl_desconto) vl_desconto,
	SUM(c.vl_acrescimo) vl_acrescimo,
	SYSDATE dt_pagto,
	SUM(c.vl_liquidacao) vl_pagto,
	SUM(c.vl_escritural - c.vl_desconto + c.vl_acrescimo) vl_total_pagto,
	DECODE(c.cd_banco,001,'30','31') ie_forma_pagto
FROM	banco_estabelecimento b,
	titulo_pagar_v2 d,	
	estabelecimento a,      
	banco_escritural e,
	titulo_pagar_escrit c
WHERE	e.nr_sequencia			= c.nr_seq_escrit
AND	c.nr_titulo			= d.nr_titulo
AND	e.nr_seq_conta_banco		= b.nr_sequencia
AND	e.cd_estabelecimento    	= a.cd_estabelecimento
AND	b.ie_tipo_relacao       	IN ('EP','ECC')
AND 	c.ie_tipo_pagamento		= 'BLQ'
AND	c.nr_seq_escrit			= nr_seq_envio_p
GROUP BY e.nr_sequencia,
		DECODE(c.cd_banco,001,'30','31')
UNION
/* Trailler do Arquivo*/
SELECT	7 tp_registro,
	'0' nr_inscricao,
	0 cd_agencia,
	'' ie_dig_agencia,
	'0' cd_conta,
	'0' ie_dig_conta,
	'' ds_banco,
	sysdate dt_arquivo,
	e.nr_sequencia nr_seq_envio,
	'0' ie_tipo_fornecedor,
	'0' cd_cgc_fornecedor,
	'' nm_hospital,
	'' ds_endereco,
	'0' nr_endereco,
	'' ds_cidade,
	'0' cd_cep,
	'' ds_estado,
	0 cd_banco_fornecedor,
	'' cd_agencia_conta,
	'' nm_fornecedor,
	0 nr_documento,
	SYSDATE dt_vencimento,
	0 vl_saldo_titulo,
	'' ie_cod_barras,
	0 vl_titulo,
	0 vl_desconto,
	0 vl_acrescimo,
	SYSDATE dt_pagto,
	0 vl_pagto,
	0 vl_total_pagto,
	'99' ie_forma_pagto
FROM	banco_estabelecimento b,
	estabelecimento a,      
	banco_escritural e,
	titulo_pagar_escrit c
WHERE	e.nr_sequencia			= c.nr_seq_escrit
AND	e.cd_banco              	= b.cd_banco
AND	e.cd_estabelecimento    	= b.cd_estabelecimento
AND	e.cd_estabelecimento    	= a.cd_estabelecimento
AND	b.ie_tipo_relacao       	IN ('EP','ECC')
AND	c.nr_seq_escrit			= nr_seq_envio_p
GROUP BY e.nr_sequencia,
	DECODE(c.cd_banco,001,'30','31')
)
order by ie_forma_pagto,
	 tp_registro;

begin

delete	from w_interf_itau;
commit;

open c01;
loop
fetch c01 into
	tp_registro_w,
	nr_inscricao_w,
	cd_agencia_w,
	ie_dig_agencia_w,
	cd_conta_w,
	ie_dig_conta_w,
	ds_banco_w,
	dt_arquivo_w,
	nr_seq_envio_w,
	ie_tipo_fornecedor_w,
	cd_cgc_fornecedor_w,
	nm_hospital_w,
	ds_endereco_w,	
	nr_endereco_w,
	ds_cidade_w,
	cd_cep_w,
	ds_estado_w,
	cd_banco_fornecedor_w,
	cd_agencia_conta_w,
	nm_fornecedor_w,
	nr_documento_w,
	dt_vencimento_w,
	vl_saldo_titulo_w,
	ie_cod_barras_w,
	vl_titulo_w,
	vl_desconto_w,
	vl_acrescimo_w,
	dt_pagto_w,
	vl_pagto_w,
	vl_total_pagto_w,
	ie_forma_pagto_w;
exit when c01%notfound;

qt_registros_w	:= qt_registros_w + 1;
	
insert	into	w_interf_itau
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_envio,
		ie_tipo_registro,
		nr_inscricao,
		cd_agencia_bancaria,
		ds_dac_agencia,
		cd_conta,
		ie_digito_conta,
		ds_banco,
		dt_geracao,
		ie_tipo_inscricao,
		nr_inscricao_fornec,
		nm_hospital,		
		ds_endereco,
		nr_endereco,
		ds_cidade,
		cd_cep,
		sg_estado,
		cd_banco_fornec,
		cd_convenio_banco,
		nm_fornecedor,
		nr_documento,
		dt_vencimento,
		vl_saldo_titulo,
		cd_barras,
		vl_titulo,
		vl_Desconto,
		vl_acrescimo,
		dt_pagto,
		vl_pagto,
		vl_total_pagto,
		ie_forma_pagto,
		nr_lote,
		qt_lote_arquivo,
		nr_registro,
		qt_registros,
		nr_Seq_reg_lote)
	values	(w_interf_itau_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_envio_w,
		tp_registro_w,
		nr_inscricao_w,
		cd_agencia_w,
		ie_dig_agencia_w,
		cd_conta_w,
		ie_dig_conta_w,
		ds_banco_w,
		dt_arquivo_w,
		ie_tipo_fornecedor_w,
		cd_cgc_fornecedor_w,
		nm_hospital_w,
		ds_endereco_w,	
		nr_endereco_w,
		ds_cidade_w,
		cd_cep_w,
		substr(ds_estado_w,1,2),
		cd_banco_fornecedor_w,
		cd_agencia_conta_w,
		nm_fornecedor_w,
		nr_documento_w,
		dt_vencimento_w,
		vl_saldo_titulo_w,
		ie_cod_barras_w,
		vl_titulo_w,
		vl_desconto_w,
		vl_acrescimo_w,
		dt_pagto_w,
		vl_pagto_w,
		vl_total_pagto_w,
		ie_forma_pagto_w,
		nr_lote_w,
		qt_lote_arquivo_w,
		nr_registro_w,
		qt_registros_w,
		nr_Seq_reg_lote_w);

	/* So incrementar o numero do registro quando detalhe */
	if	(tp_registro_w in ('2','5')) then
		nr_registro_w	:= nr_registro_w + 1;
		nr_Seq_reg_lote_w := nr_registro_w + 1;
	end if;

	/* Quando passou do Trailler muda o numero do lote */
	if	(tp_registro_w in ('3','6')) then
		nr_lote_w	:= nr_lote_w + 1;
		nr_registro_w	:= 1;
	end if;	
	
	/* Quando passou do Trailler muda o numero do lote */
	if	(tp_registro_w in ('1','4')) then
		qt_lote_arquivo_w := qt_lote_arquivo_w + 1;
	end if;	

	


end loop;
close c01;

commit;

end Gerar_interf_brasil;
/
