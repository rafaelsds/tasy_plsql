create or replace
procedure Desfazer_Desfecho(nr_atendimento_p	number,
							nm_usuario_p		Varchar2,
							ie_inativar_p		varchar2 default 'N',
							ds_justificativa_p  varchar2 default '') is 

				
nr_seq_gestao_w		number(10);
nr_seq_estagio_w	number(10);		
nr_sequencia_w		number(10);
ie_tipo_w		varchar2(10);
nr_interno_conta_w	number(10);
cd_motivo_exc_conta_w	number(10);
cd_estabelecimento_w	number(10);

dt_inicio_atendimento_w		date;
dt_atend_medico_w		date;
dt_chamada_paciente_w		date;
dt_chamada_reavaliacao_w	date;
dt_reavaliacao_medica_w		date;
dt_chamada_enfermagem_w		date;
dt_fim_consulta_w		date;
ie_status_pa_w			varchar2(15);
nr_seq_atend_pac_cham_w		number(15);
ie_param_atend_w		varchar(1);
qt_pacote_w		number(10,0);
nr_seq_proc_pacote_w	number(10,0);
ie_param_227_w			varchar(1);
dt_alta_w		date;
ds_erro_w		varchar2(100);

Cursor C01 is
	select	nr_sequencia
	from	gestao_vaga
	where	nr_atendimento	= nr_atendimento_p;	

cursor	c02 is
	select	'P' ie_tipo,
		a.nr_sequencia,
		a.nr_interno_conta,
		a.nr_seq_proc_pacote
	from	procedimento_paciente a,
		conta_paciente b,
		regra_lanc_automatico c
	where	a.nr_interno_conta 	= b.nr_interno_conta
	and	b.nr_Atendimento	= nr_atendimento_p
	and	c.nr_sequencia		= a.nr_seq_regra_lanc
	and	b.ie_status_acerto	= 1
	and	c.NR_SEQ_EVENTO		= 251
	and	a.cd_motivo_exc_conta is null
	union all
	select	'M' ie_tipo,
		a.nr_sequencia,
		a.nr_interno_conta,
		a.nr_seq_proc_pacote
	from	material_Atend_paciente a,
		conta_paciente b,
		regra_lanc_automatico c
	where	a.nr_interno_conta 	= b.nr_interno_conta
	and	b.nr_Atendimento	= nr_atendimento_p
	and	c.nr_sequencia		= a.nr_seq_regra_lanc
	and	b.ie_status_acerto	= 1
	and	c.NR_SEQ_EVENTO		= 251
	and	a.cd_motivo_exc_conta is null;
	
	

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	atendimento_paciente
where	nr_atendimento	= nr_atendimento_p;

obter_param_usuario(935, 194, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_param_atend_w);
obter_param_usuario(935, 227, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_param_227_w);

begin
if	(ie_param_atend_w = 'S') then
	update	atendimento_paciente
	set	dt_lib_medico = null
	where	nr_Atendimento = nr_atendimento_p;
end if;

	select	max(nr_sequencia)
	into	nr_seq_atend_pac_cham_w
	from	atendimento_pac_chamado
	where	nr_atendimento = nr_atendimento_p;

	if	(nr_seq_atend_pac_cham_w is not null) then
		
		select	dt_inicio_atendimento,
			dt_atend_medico,
			dt_chamada_paciente,
			dt_chamada_reavaliacao,
			dt_reavaliacao_medica,
			dt_chamada_enfermagem,
			dt_fim_consulta,
			ie_status_pa
		into	dt_inicio_atendimento_w,
			dt_atend_medico_w,
			dt_chamada_paciente_w,
			dt_chamada_reavaliacao_w,
			dt_reavaliacao_medica_w,
			dt_chamada_enfermagem_w,
			dt_fim_consulta_w,
			ie_status_pa_w
		from	atendimento_pac_chamado
		where	nr_sequencia = nr_seq_atend_pac_cham_w;

		update	atendimento_paciente set
			dt_inicio_atendimento	= nvl(dt_inicio_atendimento_w,	dt_inicio_atendimento),
			dt_atend_medico		= nvl(dt_atend_medico_w, dt_atend_medico),
			dt_chamada_paciente	= nvl(dt_chamada_paciente_w, dt_chamada_paciente),
			dt_chamada_reavaliacao	= nvl(dt_chamada_reavaliacao_w, dt_chamada_reavaliacao),
			dt_reavaliacao_medica	= nvl(dt_reavaliacao_medica_w, dt_reavaliacao_medica),
			dt_chamada_enfermagem	= nvl(dt_chamada_enfermagem_w, dt_chamada_enfermagem),
			dt_fim_consulta		= nvl(dt_fim_consulta_w, dt_fim_consulta),
			ie_status_pa		= nvl(ie_status_pa_w, ie_status_pa)
		where	nr_atendimento = nr_atendimento_p;
		
		delete	from atendimento_pac_chamado
		where	nr_sequencia = nr_seq_atend_pac_cham_w;
		
	end if;
exception
when others then
null;	
end;

if (ie_inativar_p ='S') then
	update	atendimento_alta
	set		ds_justificativa = ds_justificativa_p,
			dt_inativacao = sysdate,
			nm_usuario_inativacao = nm_usuario_p,
			ie_situacao = 'I'
	where	nr_atendimento	= nr_atendimento_p
	and		ie_tipo_orientacao = 'P'
	and     dt_liberacao is not null
	and     dt_inativacao is null; 
else
	delete	atendimento_alta
	where	nr_atendimento	= nr_atendimento_p
	and		ie_tipo_orientacao = 'P'; -- Deletar apenas registro de desfecho e n�o de Orienta��es de alta
end if;
	
select	max(nr_sequencia)
into	nr_seq_estagio_w
from	estagio_autorizacao
where	ie_interno = '70'
and		OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;

select	max(cd_motivo_exc_conta)
into	cd_motivo_exc_conta_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

if (ie_param_227_w = 'S') then
	open C01;
	loop
	fetch C01 into	
		nr_seq_gestao_w;
	exit when C01%notfound;
		begin
	
		update	gestao_vaga
		set	ie_status = 'C',
			dt_atualizacao = sysdate,
			nm_usuario	= nm_usuario_p
		where	nr_sequencia	= nr_seq_gestao_w;
	
		if	(nr_seq_estagio_w	is not null) then
			update	autorizacao_convenio
			set	nr_seq_estagio	= nr_seq_estagio_w,
				dt_atualizacao = sysdate,
				nm_usuario	= nm_usuario_p
			where	nr_seq_gestao	= nr_seq_gestao_w;
		end if;
	
		end;
	end loop;
	close C01;
end if;

open C02;
loop
fetch C02 into	
	ie_tipo_w,
	nr_sequencia_w,
	nr_interno_conta_w,
	nr_seq_proc_pacote_w;
exit when C02%notfound;
	begin
	
	if	(ie_tipo_w = 'P') then
		select	count(*)
		into	qt_pacote_w
		from	atendimento_pacote
		where	nr_seq_proc_origem = nr_sequencia_w
		and	nr_atendimento = nr_atendimento_p;
		
		if	(qt_pacote_w > 0) then
			if	(nvl(nr_seq_proc_pacote_w,0) = 0) then
				select	max(nr_seq_procedimento)
				into	nr_seq_proc_pacote_w
				from	atendimento_pacote
				where	nr_seq_proc_origem = nr_sequencia_w
				and	nr_atendimento = nr_atendimento_p;
			end if;
		
			desfazer_pacote(nr_seq_proc_pacote_w, 'S', nm_usuario_p);
			
		elsif	(nvl(nr_seq_proc_pacote_w,0) > 0) then
			select	count(*)
			into	qt_pacote_w
			from	atendimento_pacote
			where	nr_seq_procedimento = nr_seq_proc_pacote_w
			and	nr_atendimento = nr_atendimento_p;
			
			if	(qt_pacote_w > 0) then
				desfazer_pacote(nr_seq_proc_pacote_w, 'S', nm_usuario_p);
			end if;
			
		end if;
	
	elsif	(nvl(nr_seq_proc_pacote_w,0) > 0) then
		select	count(*)
		into	qt_pacote_w
		from	atendimento_pacote
		where	nr_seq_procedimento = nr_seq_proc_pacote_w
		and	nr_atendimento = nr_atendimento_p;
		
		if	(qt_pacote_w > 0) then
			desfazer_pacote(nr_seq_proc_pacote_w, 'S', nm_usuario_p);
		end if;
	end if;
	
	excluir_matproc_conta(nr_sequencia_w,nr_interno_conta_w,cd_motivo_exc_conta_w,obter_desc_expressao(320459),ie_tipo_w,nm_usuario_p);
	end;
end loop;
close C02;

if (nr_atendimento_p is not null) then
	select	max(dt_alta)
	into	dt_alta_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_p;

	if (dt_alta_w is not null) then
		begin
		gerar_estornar_alta(nr_atendimento_p,'E',0,0,null,nm_usuario_p,ds_erro_w,0,0,null);
		exception
			when others then
			ds_erro_w := '';
		end;
	end if;
end if;

Cancelar_Atend_Alta_PA(	nr_atendimento_p,
			null,
			Obter_Dados_Usuario_Opcao(nm_usuario_p,'C'),
			null,
			nm_usuario_p);

commit;

end Desfazer_Desfecho;
/
