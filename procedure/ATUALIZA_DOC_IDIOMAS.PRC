create or replace
procedure atualiza_doc_idiomas( nr_sequencia_p		number	) is 

dt_emissao_w			date;
qt_dias_revisao_w			number(10);
ie_status_w			varchar2(10);
ie_situacao_w			varchar2(10);
cd_setor_atendimento_w		varchar(10);
nr_seq_tipo_w			number(10);
nr_seq_loc_w			number(10);
cd_pessoa_elaboracao_w		varchar2(10);
dt_elaboracao_w			date;
cd_documento_ext_w		varchar2(10);
cd_pessoa_validacao_w		varchar2(10);
dt_validacao_w			date;
qt_dias_validacao_w		number(10);
cd_pessoa_aprov_w		varchar(10);
dt_aprovacao_w			date;
qt_dias_aprovacao_w		number(10);
nr_seq_tipo_ref_doc_w		number(10);
dt_revalidacao_w			date;
nm_usuario_revalidacao_w		varchar2(50);
ie_externo_w			varchar2(10);
ie_permite_email_w			varchar2(10);
ie_permite_imprimir_w		varchar2(10);
ie_permite_salvar_w		varchar2(10);
ie_pergunta_w			varchar2(10);
dt_reprovacao_w			date;
cd_estabelecimento_w		varchar2(10);

begin

	select	dt_emissao,
		qt_dias_revisao,
		ie_status,
		ie_situacao,
		cd_setor_atendimento,
		nr_seq_tipo,
		nr_seq_loc,
		cd_pessoa_elaboracao,
		dt_elaboracao,
		cd_documento_ext,
		cd_pessoa_validacao,
		dt_validacao,
		qt_dias_validacao,
		cd_pessoa_aprov,
		dt_aprovacao,
		qt_dias_aprovacao,
		nr_seq_tipo_ref_doc,
		dt_revalidacao,
		nm_usuario_revalidacao,
		ie_externo,
		ie_permite_email,
		ie_permite_imprimir,
		ie_permite_salvar,
		ie_pergunta,
		dt_reprovacao,
		cd_estabelecimento	
	into	dt_emissao_w,
		qt_dias_revisao_w,
		ie_status_w,
		ie_situacao_w,
		cd_setor_atendimento_w,
		nr_seq_tipo_w,
		nr_seq_loc_w,
		cd_pessoa_elaboracao_w,
		dt_elaboracao_w,
		cd_documento_ext_w,
		cd_pessoa_validacao_w,
		dt_validacao_w,
		qt_dias_validacao_w,
		cd_pessoa_aprov_w,
		dt_aprovacao_w,
		qt_dias_aprovacao_w,
		nr_seq_tipo_ref_doc_w,
		dt_revalidacao_w,
		nm_usuario_revalidacao_w,
		ie_externo_w,
		ie_permite_email_w,
		ie_permite_imprimir_w,
		ie_permite_salvar_w,
		ie_pergunta_w,
		dt_reprovacao_w,
		cd_estabelecimento_w
	from	qua_documento
	where	nr_sequencia = nr_sequencia_p;
		
	update	qua_documento
	set	dt_emissao = dt_emissao_w,
		qt_dias_revisao = qt_dias_revisao_w,
		ie_status = ie_status_w,
		ie_situacao = ie_situacao_w,
		cd_setor_atendimento = cd_setor_atendimento_w,
		nr_seq_tipo = nr_seq_tipo_w,
		nr_seq_loc = nr_seq_loc_w,
		cd_pessoa_elaboracao = cd_pessoa_elaboracao_w,
		dt_elaboracao = dt_elaboracao_w,
		cd_documento_ext = cd_documento_ext_w,
		cd_pessoa_validacao = cd_pessoa_validacao_w,
		dt_validacao = dt_validacao_w,
		qt_dias_validacao = qt_dias_validacao_w,
		cd_pessoa_aprov = cd_pessoa_aprov_w,
		dt_aprovacao = dt_aprovacao_w,
		qt_dias_aprovacao = qt_dias_aprovacao_w,
		nr_seq_tipo_ref_doc = nr_seq_tipo_ref_doc_w,
		dt_revalidacao = dt_revalidacao_w,
		nm_usuario_revalidacao = nm_usuario_revalidacao_w,
		ie_externo = ie_externo_w,
		ie_permite_email = ie_permite_email_w,
		ie_permite_imprimir = ie_permite_imprimir_w,
		ie_permite_salvar = ie_permite_salvar_w,
		ie_pergunta = ie_pergunta_w,
		dt_reprovacao = dt_reprovacao_w,
		cd_estabelecimento = cd_estabelecimento_w
	where	nr_seq_superior = nr_sequencia_p;

commit;

end atualiza_doc_idiomas;
/