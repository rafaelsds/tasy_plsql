create or replace
procedure alterar_obs_medica(	ds_obs_medica_p		Varchar2,
								nm_usuario_p		Varchar2,
								nr_sequencia_p		number) is 

begin

	if	(ds_obs_medica_p is not null) then
		
		UPDATE SAN_TRANS_REACAO
		SET ds_obs_medico = DS_OBS_MEDICA_P,
			dt_atualizacao = sysdate,
			nm_usuario = nm_usuario_p
		WHERE nr_sequencia = nr_sequencia_p;

		commit;

	end if;
	
end Alterar_obs_medica;
/