create or replace
procedure ptu_gerar_incon_resp_aud_v60(	nr_seq_resp_aud_p  	ptu_resposta_auditoria.nr_sequencia%type,
					nm_usuario_p		usuario.nm_usuario%type,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is

cursor C01(nr_seq_resp_aud_pc  	ptu_resposta_auditoria.nr_sequencia%type) is
	select	c.cd_mensagem,
		c.ds_mensagem,
		nvl(b.nr_seq_guia_proc, b.nr_seq_req_proc) nr_seq_procedimento,
		nvl(b.nr_seq_guia_mat, b.nr_seq_req_mat) nr_seq_material,
		nvl(a.nr_seq_guia, a.nr_seq_requisicao) nr_seq_transacao,
		a.cd_transacao,
		decode(a.nr_seq_guia, null, 'R','G') ie_tipo_transacao
	from	ptu_resposta_auditoria a,
		ptu_resp_auditoria_servico b,
		ptu_resp_aud_inconsist c
	where	b.nr_seq_auditoria 	= a.nr_sequencia
	and	c.nr_seq_aud_servico 	= b.nr_sequencia
	and	a.nr_sequencia 		= nr_seq_resp_aud_pc;

begin

--Carrega os dados importados no XML para as tabelas quentes
for c01_w in C01( nr_seq_resp_aud_p ) loop
	ptu_inserir_inconsistencia(	null, null, c01_w.cd_mensagem, c01_w.ds_mensagem, cd_estabelecimento_p, c01_w.nr_seq_transacao,
					c01_w.ie_tipo_transacao, c01_w.cd_transacao, c01_w.nr_seq_procedimento, c01_w.nr_seq_material, null, nm_usuario_p);

end loop;

end ptu_gerar_incon_resp_aud_v60;
/