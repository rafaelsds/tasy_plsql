create or replace
procedure gerar_itens_can_ficha(nr_seq_ficha_p	number,
			nm_usuario_p		Varchar2) is 

begin

insert	into can_ficha_compl (	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_ficha_admissao,
				nr_seq_dado_compl)
			(select	can_ficha_compl_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_ficha_p,
				nr_sequencia
			from	can_dados_compl);

commit;

end gerar_itens_can_ficha;
/