create or replace procedure PFCS_CALC_DEVICE_AVAILABLE (
					nr_seq_indicator_p	number,
					cd_estabelecimento_p	varchar2,
					nm_usuario_p	varchar2) is

	qt_available_beds_w				number(15) := 0;
	nr_seq_operational_level_w  	number(15) := 0;
	pfcs_panel_detail_seq_w			pfcs_panel_detail.nr_sequencia%type;
	nr_seq_panel_w					pfcs_panel_detail.nr_seq_panel%type;

	ds_dev_unknown_status			    varchar2(15) := 'UNKNOWN';
	ds_dev_inactive_status_w			  varchar2(15) := 'INACTIVE';
	ds_device_type_w 			    varchar2(10) := 'Monitor';
    qt_total_aux                	pfcs_panel.vl_indicator_aux%type := 0;
    qt_total_brkn_pfcs_w              number(15) := 0;
    qt_isUse_device_w                 number(15) := 0;

    cursor c01_fhir_status is
        select 'U' cd_status_fhir from dual
        union select 'I' cd_status_fhir from dual
        union select 'C' cd_status_fhir from dual
        union select 'K' cd_status_fhir from dual
        union select 'H' cd_status_fhir from dual;

	cursor c01_from_pfcs is
			select	count(1) total_devices,
                loc.ds_department ds_department
        		from	pfcs_device dev,
        				pfcs_location loc,
        				pfcs_organization org
        		where	(dev.si_status in (ds_dev_inactive_status_w) or dev.si_status is null)
        		and		dev.ds_device_type = ds_device_type_w
        		and     dev.nr_seq_location = loc.nr_sequencia
        		and		dev.nr_seq_organization = org.nr_sequencia
        		and		org.cd_estabelecimento = to_number(cd_estabelecimento_p)
				and 	loc.si_status  in ('ACTIVE')
        		and     pfcs_get_bed_status(loc.cd_operational_status, 'C', cd_estabelecimento_p, 'Y') = 'A'
                group by loc.ds_department
        	union all
        		select	count(1) total_devices,
        			Obter_desc_expressao(344145) ds_department
        		from	pfcs_device dev,
        			pfcs_organization org
        		where	(dev.si_status in (ds_dev_inactive_status_w) or dev.si_status is null)
        				and		dev.ds_device_type = ds_device_type_w
        				and		dev.nr_seq_organization = org.nr_sequencia
        				and		dev.nr_seq_location is null
        				and		org.cd_estabelecimento = to_number(cd_estabelecimento_p);

    begin

	nr_seq_operational_level_w := to_number(cd_estabelecimento_p);

	for r_c01 in c01_from_pfcs loop
	    pfcs_pck_v2.pfcs_generate_results(
		    vl_indicator_p => r_c01.total_devices,
		    ds_reference_value_p => r_c01.ds_department,
            vl_indicator_aux_p => 0,
		    nr_seq_indicator_p => 160,
		    nr_seq_operational_level_p => nr_seq_operational_level_w,
		    nm_usuario_p => nm_usuario_p,
		nr_seq_panel_p => nr_seq_panel_w);
    end loop;

    for c01_w in c01_fhir_status loop
        if (pfcs_get_bed_status(c01_w.cd_status_fhir, 'C', cd_estabelecimento_p, 'Y') = 'A') then
            qt_available_beds_w := qt_available_beds_w + pfcs_get_tele_device_intg_v2(ds_device_type_w, replace(c01_w.cd_status_fhir, 'U', 'Unoccupied'), cd_estabelecimento_p);
        end if;
    end loop;

   	qt_isUse_device_w := pfcs_get_tele_device_intg_v2(ds_device_type_w, 'U', cd_estabelecimento_p);
   	qt_total_brkn_pfcs_w :=  pfcs_get_tele_device_intg_v2(ds_device_type_w, 'BorkenLostDevice', cd_estabelecimento_p);
   	qt_total_aux :=  qt_available_beds_w + qt_isUse_device_w + qt_total_brkn_pfcs_w;

    	    pfcs_pck_v2.pfcs_generate_results(
    		    vl_indicator_p => qt_available_beds_w,
    		    ds_reference_value_p => '',
    		    vl_indicator_aux_p => qt_total_aux,
    		    nr_seq_indicator_p => nr_seq_indicator_p,
    		    nr_seq_operational_level_p => nr_seq_operational_level_w,
    		    nm_usuario_p => nm_usuario_p,
    		nr_seq_panel_p => nr_seq_panel_w);
    commit;

	pfcs_pck_v2.pfcs_update_detail(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_panel_p => nr_seq_panel_w,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

	   pfcs_pck_v2.pfcs_update_detail(
            nr_seq_indicator_p => 160,
        	nr_seq_panel_p => nr_seq_panel_w,
        	nr_seq_operational_level_p => nr_seq_operational_level_w,
        	nm_usuario_p => nm_usuario_p);

	pfcs_pck_v2.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => nr_seq_operational_level_w,
		nm_usuario_p => nm_usuario_p);

       pfcs_pck_v2.pfcs_activate_records(
        		nr_seq_indicator_p => 160,
        		nr_seq_operational_level_p => nr_seq_operational_level_w,
        		nm_usuario_p => nm_usuario_p);

end PFCS_CALC_DEVICE_AVAILABLE;
/
