create or replace
procedure alt_status_cot_compra_forn(
			nr_sequencia_p		varchar2) is

begin

update 	cot_compra_forn
set	ie_status = 'LH',
	ie_liberada_internet = 'S'
where	nr_sequencia = nr_sequencia_p;

commit;

end alt_status_cot_compra_forn;
/