create or replace
procedure obter_itens_lote_disp (   cd_mat_barra_p      in ap_lote.nr_sequencia%type,
                                    nr_seq_processo_p   in adep_processo.nr_sequencia%type,
                                    cd_material_p       out varchar2,
                                    cd_barras_p         out varchar2,
                                    cd_unid_med_p       out varchar2   ) is

cd_material_w           varchar2(4000 char);
cd_barras_w             varchar2(4000 char);
cd_unid_med_w           varchar2(4000 char);
nr_seq_lote_fornec_w    material_atend_paciente.nr_seq_lote_fornec%type;
cd_barra_material_lf_w  material_lote_fornec.cd_barra_material%type;

cursor c01 is
select  b.cd_material cd_material,
        nvl(b.cd_unidade_medida_dose, 'XPTO') cd_unid_med,
        d.nr_atendimento,
        d.nr_prescricao,
        b.nr_seq_material,
        b.nr_seq_lote_fornec
from    prescr_mat_hor b,
        ap_lote a,
        ap_lote_item c,
        adep_processo d
where   d.nr_sequencia = b.nr_seq_processo
and     a.nr_sequencia = c.nr_seq_lote
and     b.nr_sequencia = c.nr_seq_mat_hor
and     a.nr_sequencia = b.nr_seq_lote
and     a.nr_sequencia = cd_mat_barra_p
and     d.nr_sequencia = nr_seq_processo_p
and	    obter_se_horario_liberado(b.dt_lib_horario, b.dt_horario) = 'S';

begin

if (cd_mat_barra_p is not null and
    nr_seq_processo_p is not null) then

    for c01_w in c01
    loop
        if (c01_w.cd_material is not null) then        
            if (cd_material_w is not null) then
                cd_material_w := cd_material_w || ',' || c01_w.cd_material;
            else
                cd_material_w := c01_w.cd_material;
            end if;
            if (cd_unid_med_w is not null) then
                cd_unid_med_w := cd_unid_med_w || ',' || c01_w.cd_unid_med;
            else
                cd_unid_med_w := c01_w.cd_unid_med;
            end if;
        end if;

        select  nvl(max(a.nr_seq_lote_fornec), c01_w.nr_seq_lote_fornec)
        into    nr_seq_lote_fornec_w
        from    material_atend_paciente a
        where   a.nr_atendimento = c01_w.nr_atendimento
        and     a.nr_prescricao = c01_w.nr_prescricao
        and     a.nr_sequencia_prescricao = c01_w.nr_seq_material
        and     a.nr_seq_lote_ap = cd_mat_barra_p;

        if (c01_w.nr_atendimento is not null and
                c01_w.nr_prescricao is not null and
                c01_w.nr_seq_material is not null) then

            select  max(a.cd_barra_material)
            into    cd_barra_material_lf_w
            from    material_lote_fornec a
            where   a.nr_sequencia = nr_seq_lote_fornec_w;

            if (cd_barra_material_lf_w is not null) then
                if (cd_barras_w is not null) then
                    cd_barras_w := cd_barras_w || ',' || cd_barra_material_lf_w;
                else
                    cd_barras_w := cd_barra_material_lf_w;
                end if;
            elsif (nr_seq_lote_fornec_w is not null) then
                if (length(nr_seq_lote_fornec_w) < 10) then
                    if (cd_barras_w is not null) then
                        cd_barras_w := cd_barras_w || ',' || lpad(nr_seq_lote_fornec_w, 10, '0');
                    else
                        cd_barras_w := lpad(nr_seq_lote_fornec_w, 10, '0');
                    end if;
                else
                    if (cd_barras_w is not null) then
                        cd_barras_w := cd_barras_w || ',' || nr_seq_lote_fornec_w;
                    else
                        cd_barras_w := nr_seq_lote_fornec_w;
                    end if;
                end if;
            else
                if (cd_barras_w is not null) then
                    cd_barras_w := cd_barras_w || ',' || c01_w.cd_material;
                else
                    cd_barras_w := c01_w.cd_material;
                end if;
            end if;
        end if;
    end loop;

    cd_material_p := cd_material_w;
    cd_barras_p := cd_barras_w;
    cd_unid_med_p := cd_unid_med_w;
    
end if;

end obter_itens_lote_disp;
/
