create or replace procedure gerar_tabela_sline_laudo_oper(nr_prescricao_p number
                                                         ,cd_medico_p varchar
                                                         ,ds_oper_regans_p varchar
                                                         ,ds_oper_nmcarteira_p varchar
                                                         ,ds_oper_numguia_p varchar
                                                         ,nm_usuario_p varchar
                                                         ,nr_seq_laudo_oper_p out number) as
nr_seq_sline_xml_laudo_w number(10);
begin

select nr_sequencia
  into nr_seq_sline_xml_laudo_w
  from sline_xml_laudo s 
 where s.cd_medico = cd_medico_p 
   and s.nr_prescricao = nr_prescricao_p;
   
select sline_xml_laudo_oper_seq.nextVal
  into nr_seq_laudo_oper_p
  from dual;

Insert 
  into sline_xml_laudo_oper(NR_SEQUENCIA
                           ,DT_ATUALIZACAO
                           ,NM_USUARIO
                           ,DT_ATUALIZACAO_NREC
                           ,NM_USUARIO_NREC
                           ,NR_SEQ_XML_LAUDO
                           ,DS_OPER_REGANS
                           ,DS_OPER_NMCARTEIRA
                           ,DS_OPER_NUMGUIA
                           ,NR_PRESCRICAO
                           ,CD_MEDICO)
                    values (nr_seq_laudo_oper_p
                           ,sysdate
                           ,nm_usuario_p
                           ,sysdate
                           ,nm_usuario_p
                           ,nr_seq_sline_xml_laudo_w
                           ,ds_oper_regans_p
                           ,ds_oper_nmcarteira_p
                           ,ds_oper_numguia_p
                           ,nr_prescricao_p
                           ,cd_medico_p);
commit;                           
  
end gerar_tabela_sline_laudo_oper;
/
