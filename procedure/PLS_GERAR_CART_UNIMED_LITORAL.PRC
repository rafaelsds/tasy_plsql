create or replace procedure pls_gerar_cart_unimed_litoral
		(	nr_seq_lote_p		number,
			cd_interface_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is

nr_seq_segurado_w		number(10);
ds_header_w			varchar2(4000);
ds_sub_header_w			varchar2(4000);
cd_usuario_plano_w		varchar2(30);
nm_beneficiario_w		varchar2(250);
tp_contratacao_w		varchar2(255);
dt_nascimento_w			varchar2(20);
dt_validade_carteira_w		varchar2(20);
cd_local_cobranca_w		varchar2(20);
ds_produto_w			varchar2(50);
cd_abrangencia_w		varchar2(10);
ds_abrangencia_w		varchar2(50);
ds_registro_ans_w		varchar2(50);
ds_acomodacao_w			varchar2(50);
cod_empresa_w			number(10);
ds_mensagem_cpt_w		varchar2(15);
nm_empresa_w			varchar2(50);
ds_rede_atendimento_w		varchar2(50);
ds_carencia_ww			varchar2(4000);
ds_carencia_w			varchar2(4000);
ds_observacao_1_w		varchar2(100);
ds_observacao_2_w		varchar2(100);
tp_contrato_w			varchar2(10);
nr_via_solicitacao_w		number(10);
ds_regiao_w			varchar2(4000);
ds_trilha_1_w			pls_segurado_carteira.ds_trilha1%type;
ds_trilha_2_w			pls_segurado_carteira.ds_trilha2%type;
ds_trilha_3_w			pls_segurado_carteira.ds_trilha3%type;
nr_protocolo_ans_w		varchar2(100);
dt_contratacao_w		date;
--ds_mensagem_carteira_w		varchar2(4000);
dt_inclusao_operadora_w		date;
qt_carencias_w			number(10);
nm_abreviado_w			varchar2(255);
nr_seq_contrato_w		number(10);
ds_mensagem_cart_w		varchar2(255);
ds_mensagem_carencia_w		varchar2(255);
----------------------------------------------------------------------
cd_cgc_estipulante_w		varchar2(20);
cd_pf_estipulante_w		varchar2(20);
i				number(10);
qt_registros_w			number(10);
nr_seq_plano_w			number(10);
nr_seq_regiao_w			number(10);
ds_municipio_w			varchar2(255);
ds_regiao_1_w			varchar2(255);
ds_regiao_2_w			varchar2(255);
ds_regiao_3_w			varchar2(255);
ie_tipo_contrato_w		varchar2(10);
ie_tipo_segurado_w		varchar2(10);
ds_congenere_w			varchar2(255);
nr_cartao_nac_sus_w		pessoa_fisica.nr_cartao_nac_sus%type;
ds_segmentacao_w   		varchar2(255);
dt_inicio_vigencia_w		varchar2(20);
ie_carencia_abrangencia_ant_w	pls_parametros.ie_carencia_abrangencia_ant%type;
nr_seq_segurado_ant_w		pls_segurado.nr_seq_segurado_ant%type;
ie_carencia_abrang_nova_w	varchar(1);
cd_abrangencia_ant_w		varchar2(10);
ds_plano_carteira_w		pls_plano.ds_plano_carteira%type;
nm_plano_w			pls_plano.ds_plano%type;
ie_gerou_carencia_w		varchar2(1);
qt_carencia_gerada_w		number(10);
nr_seq_subestipulante_w		pls_sub_estipulante.nr_sequencia%type;
cd_cgc_subestipulante_w		pessoa_juridica.cd_cgc%type;

Cursor C01 is
	select	b.nr_sequencia,
		'C'
	from	pls_segurado_carteira	a,
		pls_segurado		b,
		pls_carteira_emissao	c,
		pls_lote_carteira	d,
		pls_contrato		e
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	c.nr_seq_seg_carteira	= a.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and	b.nr_seq_contrato	= e.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_p
	union all
	select	b.nr_sequencia,
		'I'
	from	pls_segurado_carteira	a,
		pls_segurado		b,
		pls_carteira_emissao	c,
		pls_lote_carteira	d,
		pls_intercambio		e
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	c.nr_seq_seg_carteira	= a.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and	b.nr_seq_intercambio	= e.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_p;

cursor c02 is
	select	b.nr_sequencia
	from	pls_plano_area	a,
		pls_regiao	b
	where	a.nr_seq_regiao		= b.nr_sequencia
	and	a.nr_seq_plano		= nr_seq_plano_w
	order by a.nr_sequencia;

cursor c03 is
	select	b.ds_municipio
	from	pls_regiao_local	a,
		sus_municipio		b
	where	a.cd_municipio_ibge	= b.cd_municipio_ibge
	and	a.nr_seq_regiao		= nr_seq_regiao_w
	order by b.ds_municipio;

Cursor C04 is
	select	ds_mensagem
	from	pls_contrato_mensagem_cart
	where	nr_seq_contrato = nr_seq_contrato_w
	and	ie_situacao	= 'A'
	and	sysdate between nvl(dt_inicio_vigencia,sysdate) and fim_dia(dt_fim_vigencia);

begin
if 	(cd_interface_p = 2205) then
	ds_header_w	:= 'COD_CLIENTE;DT_NASCIMENTO;TP_CONTRATACAO;DT_VAL_CARTEIRA;NM_USUARIO;LOCAL_COBRANCA;DS_PRODUTO;DS_ABRANGENCIA;DS_DESC_ABRANG;DS_REGISTRO_ANS;DS_ACOMODACAO;COD_EMPRESA;DT_FIM_CPT;NM_EMPRESA;'||
			'COBERTURA_1;DT_FIM_1;COBERTURA_2;DT_FIM_2;COBERTURA_3;DT_FIM_3;COBERTURA_4;DT_FIM_4;COBERTURA_5;DT_FIM_5;COBERTURA_6;DT_FIM_6;COBERTURA_7;DT_FIM_7;COBERTURA_8;'||
			'DT_FIM_8;COBERTURA_9;DT_FIM_9;COBERTURA_10;DT_FIM_10;COBERTURA_11;DT_FIM_11;COBERTURA_12;DT_FIM_12;COBERTURA_13;DT_FIM_13;COBERTURA_14;DT_FIM_14;'||
			'MSG_SELO_CARNE;OBS_1;OBS_2;TP_CONTRATO;TI_GERACAO;DR_REGISTRO;DS_REGIAO_1;DS_REGIAO_2;DS_REGIAO_3;TRILHA1;TRILHA2;TRILHA3;DS_MENSAGEM;CART�O_NACIONAL_DE_SAUDE';

elsif	(cd_interface_p = 2683)then
	ds_header_w	:= 'COD_CLIENTE;DT_NASCIMENTO;TP_CONTRATACAO;DT_VAL_CARTEIRA;NM_USUARIO;LOCAL_COBRANCA;DS_PRODUTO;DS_ABRANGENCIA;DS_DESC_ABRANG;DS_REGISTRO_ANS;DS_ACOMODACAO;COD_EMPRESA;DT_FIM_CPT;NM_EMPRESA;'||
			'DS_SEGMENTACAO;COBERTURA_1;DT_FIM_1;COBERTURA_2;DT_FIM_2;COBERTURA_3;DT_FIM_3;COBERTURA_4;DT_FIM_4;COBERTURA_5;DT_FIM_5;COBERTURA_6;DT_FIM_6;COBERTURA_7;DT_FIM_7;COBERTURA_8;'||
			'DT_FIM_8;COBERTURA_9;DT_FIM_9;COBERTURA_10;DT_FIM_10;COBERTURA_11;DT_FIM_11;COBERTURA_12;DT_FIM_12;COBERTURA_13;DT_FIM_13;COBERTURA_14;DT_FIM_14;'||
			'MSG_SELO_CARNE;OBS_1;OBS_2;TP_CONTRATO;TI_GERACAO;DR_REGISTRO;DS_REGIAO_1;DS_REGIAO_2;DS_REGIAO_3;TRILHA1;TRILHA2;TRILHA3;DS_MENSAGEM;CART�O_NACIONAL_DE_SAUDE';
elsif	(cd_interface_p = 2704) then
	ds_header_w	:= 'COD_CLIENTE;DT_NASCIMENTO;TP_CONTRATACAO;DT_VAL_CARTEIRA;NM_USUARIO;LOCAL_COBRANCA;DS_PRODUTO;DS_ABRANGENCIA;DS_DESC_ABRANG;DS_REGISTRO_ANS;DS_ACOMODACAO;COD_EMPRESA;DT_FIM_CPT;NM_EMPRESA;'||
			'COBERTURA_1;DT_FIM_1;COBERTURA_2;DT_FIM_2;COBERTURA_3;DT_FIM_3;COBERTURA_4;DT_FIM_4;COBERTURA_5;DT_FIM_5;COBERTURA_6;DT_FIM_6;COBERTURA_7;DT_FIM_7;COBERTURA_8;'||
			'DT_FIM_8;COBERTURA_9;DT_FIM_9;COBERTURA_10;DT_FIM_10;COBERTURA_11;DT_FIM_11;COBERTURA_12;DT_FIM_12;COBERTURA_13;DT_FIM_13;COBERTURA_14;DT_FIM_14;'||
			'MSG_SELO_CARNE;OBS_1;OBS_2;TP_CONTRATO;TI_GERACAO;DR_REGISTRO;DS_REGIAO_1;DS_REGIAO_2;DS_REGIAO_3;TRILHA1;TRILHA2;TRILHA3;DS_MENSAGEM;CART�O_NACIONAL_DE_SAUDE;PRODUTO;'||
			'SEGMENTACAO;DT_ADESAO;DS_PRODUTO1;DS_PRODUTO2';
end if;

delete	w_pls_interface_carteira
where	nr_seq_lote	= nr_seq_lote_p;

insert into w_pls_interface_carteira
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		nr_seq_lote,ds_header,ie_tipo_reg)
values	(	w_pls_interface_carteira_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		nr_seq_lote_p,ds_header_w,1);

open C01;
loop
fetch C01 into
	nr_seq_segurado_w,
	ie_tipo_contrato_w;
exit when C01%notfound;
	begin
	if	(ie_tipo_contrato_w = 'C') then
		select	c.cd_usuario_plano,
			substr(a.nm_pessoa_fisica,1,25),
			to_char(a.dt_nascimento,'dd/mm/yyyy'),
			substr(d.ds_plano,1,40),
			substr(nvl(pls_obter_dados_cart_unimed(b.nr_sequencia,d.nr_sequencia,'DASJ',1),'N�O SE APLICA'),1,50),
			to_char(c.dt_validade_carteira,'dd/mm/yyyy'),
			to_char(c.dt_inicio_vigencia,'dd/mm/yyyy'),
			f.cd_cgc_estipulante,
			b.cd_operadora_empresa,
			decode(d.ie_abrangencia,'E','ES','GE','A','GM','RB','M','MU','N','NA'),
			upper(Obter_Valor_Dominio(1667,d.ie_abrangencia)),
			c.nr_via_solicitacao,
			d.nr_sequencia,
			decode(d.ie_preco,'3','CO','VD'),
			decode(d.ie_regulamentacao,'P',d.nr_protocolo_ans,d.CD_SCPA),
			substr(replace(replace(ds_trilha1,'%',''),'?',''),1,25),
			replace(replace(ds_trilha2,';',''),'?',''),
			ds_trilha3,
			f.cd_pf_estipulante,
			b.dt_contratacao,
			b.dt_inclusao_operadora,
			b.ie_tipo_segurado,
			substr(a.nm_abreviado,1,25),
			f.nr_sequencia,
			a.nr_cartao_nac_sus,
			decode(d.ie_segmentacao,'5','Refer�ncia',substr(Obter_Valor_Dominio(1665,d.ie_segmentacao),1,57)) ds_segmentacao,
			b.nr_seq_segurado_ant,
			d.ds_plano_carteira,
			b.nr_seq_subestipulante
		into	cd_usuario_plano_w,
			nm_beneficiario_w,
			dt_nascimento_w,
			ds_produto_w,
			ds_acomodacao_w,
			dt_validade_carteira_w,
			dt_inicio_vigencia_w,
			cd_cgc_estipulante_w,
			cod_empresa_w,
			cd_abrangencia_w,
			ds_abrangencia_w,
			nr_via_solicitacao_w,
			nr_seq_plano_w,
			tp_contrato_w,
			nr_protocolo_ans_w,
			ds_trilha_1_w,
			ds_trilha_2_w,
			ds_trilha_3_w,
			cd_pf_estipulante_w,
			dt_contratacao_w,
			dt_inclusao_operadora_w,
			ie_tipo_segurado_w,
			nm_abreviado_w,
			nr_seq_contrato_w,
			nr_cartao_nac_sus_w,
			ds_segmentacao_w,
			nr_seq_segurado_ant_w,
			ds_plano_carteira_w,
			nr_seq_subestipulante_w
		from	pessoa_fisica		a,
			pls_segurado_carteira	c,
			pls_segurado		b,
			pls_contrato		f,
			pls_plano		d
		where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
		and	c.nr_seq_segurado	= b.nr_sequencia
		and	b.nr_seq_plano		= d.nr_sequencia
		and	b.nr_seq_contrato	= f.nr_sequencia
		and	b.nr_sequencia		= nr_seq_segurado_w;

	elsif	(ie_tipo_contrato_w = 'I') then
		select	c.cd_usuario_plano,
			substr(a.nm_pessoa_fisica,1,25),
			to_char(a.dt_nascimento,'dd/mm/yyyy'),
			substr(d.ds_carteira_acomodacao,1,17),
			substr(nvl(pls_obter_dados_cart_unimed(b.nr_sequencia,d.nr_sequencia,'DASJ',1),'N�O SE APLICA'),1,50),
			to_char(c.dt_validade_carteira,'dd/mm/yyyy'),
			f.cd_cgc,
			DECODE(D.IE_TIPO_CONTRATACAO,'I','Individual ou Familiar ','CA','Coletivo por Ades�o','CE','Coletivo Empresarial'),
			DECODE(D.IE_REGULAMENTACAO,'R','PLANO N�O REGULAMENTADO','P','PLANO REGULAMENTADO','A','PLANO ADAPTADO'),
			b.cd_operadora_empresa,
			decode(d.ie_abrangencia,'E','ES','GE','A','GM','RB','M','MU','N','NA'),
			upper(Obter_Valor_Dominio(1667,d.ie_abrangencia)),
			c.nr_via_solicitacao,
			d.nr_sequencia,
			decode(d.ie_preco,'3','CO','VD'),
			DECODE(D.IE_REGULAMENTACAO,'P',d.nr_protocolo_ans,d.CD_SCPA),
			substr(replace(replace(ds_trilha1,'%',''),'?',''),1,25),
			replace(replace(ds_trilha2,';',''),'?',''),
			ds_trilha3,
			f.cd_pessoa_fisica,
			b.dt_contratacao,
			b.dt_inclusao_operadora,
			substr(a.nm_abreviado,1,25),
			a.nr_cartao_nac_sus,
			decode(d.ie_segmentacao,'5','Refer�ncia',substr(Obter_Valor_Dominio(1665,d.ie_segmentacao),1,57)) ds_segmentacao,
			d.ds_plano_carteira,
			b.nr_seq_subestipulante
		into	cd_usuario_plano_w,
			nm_beneficiario_w,
			dt_nascimento_w,
			ds_produto_w,
			ds_acomodacao_w,
			dt_validade_carteira_w,
			cd_cgc_estipulante_w,
			tp_contratacao_w,
			ds_registro_ans_w,
			cod_empresa_w,
			cd_abrangencia_w,
			ds_abrangencia_w,
			nr_via_solicitacao_w,
			nr_seq_plano_w,
			tp_contrato_w,
			nr_protocolo_ans_w,
			ds_trilha_1_w,
			ds_trilha_2_w,
			ds_trilha_3_w,
			cd_pf_estipulante_w,
			dt_contratacao_w,
			dt_inclusao_operadora_w,
			nm_abreviado_w,
			nr_cartao_nac_sus_w,
			ds_segmentacao_w,
			ds_plano_carteira_w,
			nr_seq_subestipulante_w
		from	pls_segurado_carteira	c,
			pls_segurado		b,
			pessoa_fisica		a,
			pls_intercambio		f,
			pls_plano		d
		where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
		and	c.nr_seq_segurado	= b.nr_sequencia
		and	b.nr_seq_intercambio	= f.nr_sequencia
		and	b.nr_seq_plano		= d.nr_sequencia
		and	b.nr_sequencia		= nr_seq_segurado_w;
	end if;

	if	(nr_seq_subestipulante_w is not null) then
		select	max(cd_cgc)
		into	cd_cgc_subestipulante_w
		from	pls_sub_estipulante
		where	nr_sequencia	= nr_seq_subestipulante_w;
	else
		cd_cgc_subestipulante_w	:= null;
	end if;

	if	(cd_interface_p = 2205) then
		select 	decode(d.ie_tipo_contratacao,'I','2-F�SICA','CA','4-ADES�O','CE','3-EMPRESARIAL'),
			decode(d.ie_regulamentacao,'R','PLANO N�O REGULAMENTADO','P','PLANO REGULAMENTADO','A','PLANO ADAPTADO')
		into	tp_contratacao_w,
			ds_registro_ans_w
		from 	pls_plano d
		where 	nr_sequencia = nr_seq_plano_w;
	else
		select 	decode(d.ie_tipo_contratacao,'I','Individual ou Familiar','CA','Coletivo por Ades�o','CE','Coletivo Empresarial'),
			decode(d.ie_regulamentacao,'R','N�o Regulamentado','P','Regulamentado','A','Adaptado')
		into	tp_contratacao_w,
			ds_registro_ans_w
		from 	pls_plano d
		where 	nr_sequencia = nr_seq_plano_w;
	end if;

	cd_usuario_plano_w	:= substr(cd_usuario_plano_w,1,1)||' '||substr(cd_usuario_plano_w,2,3)||' '||substr(cd_usuario_plano_w,5,12)||' '||substr(cd_usuario_plano_w,17,1);

	if	(trim(nm_abreviado_w) is null) then
		nm_beneficiario_w	:= upper(pls_gerar_nome_abreviado(nm_beneficiario_w));
	else
		nm_beneficiario_w	:= upper(nm_abreviado_w);
		ds_trilha_1_w		:= upper(nm_abreviado_w);
	end if;

	ds_acomodacao_w		:= upper(ds_acomodacao_w);


	if	(cd_cgc_subestipulante_w is not null) then
		nm_empresa_w	:= substr(nvl(obter_dados_pf_pj('',cd_cgc_subestipulante_w,'ABV'),obter_dados_pf_pj('',cd_cgc_subestipulante_w,'F')),1,18);
	elsif	(cd_cgc_estipulante_w is not null) then
		nm_empresa_w	:= substr(nvl(obter_dados_pf_pj('',cd_cgc_estipulante_w,'ABV'),obter_dados_pf_pj('',cd_cgc_estipulante_w,'F')),1,18);
	else
		nm_empresa_w	:= 'INDIV./FAMILIAR';
	end if;

	nm_empresa_w	:= upper(nm_empresa_w);

	ds_carencia_w		:= '';
	ds_mensagem_carencia_w	:= '';

	select	count(1)
	into	qt_carencias_w
	from	pls_carencia	b,
		pls_segurado	a
	where	b.nr_seq_segurado	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_segurado_w
	and	b.ie_cpt		= 'N'
	and	rownum			<= 1;

	select	max(ie_carencia_abrangencia_ant)
	into	ie_carencia_abrangencia_ant_w
	from	pls_parametros
	where	cd_estabelecimento = cd_estabelecimento_p;

	ie_carencia_abrang_nova_w := 'N';

	if	(nvl(ie_carencia_abrangencia_ant_w,'N') = 'S') and
		(nr_seq_segurado_ant_w is not null) then

		select 	decode(max(b.ie_abrangencia),'E','ES','GE','A','GM','RB','M','MU','N','NA')
		into	cd_abrangencia_ant_w
		from	pls_segurado a,
			pls_plano    b
		where	a.nr_seq_plano = b.nr_sequencia
		and	a.nr_sequencia = nr_seq_segurado_ant_w;

		if(cd_abrangencia_ant_w <> cd_abrangencia_w) then
		/* verifica se a abrang�ncia atual � maior que a anterior ,*/
			if(cd_abrangencia_w = 'NA' ) or
			(cd_abrangencia_w = 'A'  and cd_abrangencia_ant_w <> 'NA') or
			(cd_abrangencia_w = 'ES' and cd_abrangencia_ant_w <> 'NA' and cd_abrangencia_ant_w <> 'A') or
			(cd_abrangencia_w = 'RB' and cd_abrangencia_ant_w = 'MU') then
				ie_carencia_abrang_nova_w := 'S';
			end if;
		end if;
	end if;

	if	(qt_carencias_w > 0) then
		select	count(1)
		into	qt_registros_w
		from	pls_carencia		b,
			pls_segurado		a,
			pls_tipo_carencia	c
		where	b.nr_seq_segurado	= a.nr_sequencia
		and	b.nr_seq_tipo_carencia	= c.nr_sequencia
		and	a.nr_sequencia		= nr_seq_segurado_w
		and	b.ie_cpt		= 'N'
		and	c.ie_padrao_carteira	= 'S'
		and	pls_consistir_sexo_carencia(nr_seq_segurado_w,c.nr_sequencia) = 'S'
		and	((ie_carencia_abrang_nova_w = 'N' and nvl(dt_inicio_vigencia,dt_inclusao_operadora_w) + nvl(qt_dias_fora_abrang_ant,qt_dias) > sysdate) or
			 (ie_carencia_abrang_nova_w = 'S' and a.dt_contratacao + nvl(qt_dias_fora_abrang_ant,qt_dias) > sysdate));
	else
		select	count(1)
		into	qt_registros_w
		from	pls_carencia	b,
			pls_segurado	a,
			pls_tipo_carencia	d,
			pls_plano	c
		where	b.nr_seq_plano		= c.nr_sequencia
		and	a.nr_seq_plano		= c.nr_sequencia
		and	b.nr_seq_tipo_carencia	= d.nr_sequencia
		and	a.nr_sequencia		= nr_seq_segurado_w
		and	b.ie_cpt		= 'N'
		and	d.ie_padrao_carteira	= 'S'
		and	pls_consistir_sexo_carencia(nr_seq_segurado_w,d.nr_sequencia) = 'S'
		and	((ie_carencia_abrang_nova_w = 'N' and nvl(dt_inicio_vigencia,dt_inclusao_operadora_w) + nvl(qt_dias_fora_abrang_ant,qt_dias) > sysdate) or
			 (ie_carencia_abrang_nova_w = 'S' and a.dt_contratacao + nvl(qt_dias_fora_abrang_ant,qt_dias) > sysdate));
	end if;

	if	(qt_registros_w = 0) then
		select	sum(qt)
		into	qt_registros_w
		from	(select	count(1) qt
			from	pls_segurado			a,
				pls_contrato			b,
				pls_contrato_mensagem_cart	c
			where	a.nr_seq_contrato	= b.nr_sequencia
			and	c.nr_seq_contrato	= b.nr_sequencia
			and	a.nr_sequencia		= nr_seq_segurado_w
			and	c.ie_situacao		= 'A'
			and	SYSDATE	BETWEEN	nvl(c.dt_inicio_vigencia,SYSDATE) and nvl(c.dt_fim_vigencia,SYSDATE)
			union all
			select	count(1) qt
			from	pls_segurado			a,
				pls_congenere			b,
				pls_congenere_mensagem		c
			where	a.nr_seq_congenere	= b.nr_sequencia
			and	c.nr_seq_congenere	= b.nr_sequencia
			and	a.nr_sequencia		= nr_seq_segurado_w
			union all
			select	count(1) qt
			from	pls_segurado		a,
				pls_plano		b,
				pls_plano_mensagem_cart	c
			where	a.nr_seq_plano	= b.nr_sequencia
			and	c.nr_seq_plano	= b.nr_sequencia
			and	a.nr_sequencia	= nr_seq_segurado_w
			and	c.ie_situacao	= 'A'
			and	SYSDATE	BETWEEN	nvl(c.dt_inicio_vigencia,SYSDATE) and nvl(c.dt_fim_vigencia,SYSDATE));
	end if;

	ie_gerou_carencia_w := 'N';

	if	(qt_registros_w > 0) then
		for i in 1..14 loop
			begin
			ds_carencia_ww	:= nvl(substr(pls_obter_dados_cart_unimed(nr_seq_segurado_w, nr_seq_plano_w,'CAUL',i),1,255),';');

			select	count(1)
			into	qt_carencia_gerada_w
			from	dual
			where	ds_carencia_ww like '%;'||to_char(dt_contratacao_w,'dd/mm/yy');

			if	(i < 14) then
				ds_carencia_w	:= ds_carencia_w ||ds_carencia_ww||';';
			else
				ds_carencia_w	:= ds_carencia_w ||ds_carencia_ww;
			end if;

			if	(ds_carencia_ww <> ';') and (qt_carencia_gerada_w = 0) then
				ie_gerou_carencia_w := 'S';
			end if;
			end;
		end loop;

		if	(ie_gerou_carencia_w = 'N') then
			ds_carencia_w	:= ';;;;;;;;;;;;;;;;;;;;;;;;;;;';
			ds_mensagem_carencia_w := 'SEM CAR�NCIAS A CUMPRIR ';
		end if;
	else
		ds_carencia_w	:= ';;;;;;;;;;;;;;;;;;;;;;;;;;;';
		ds_mensagem_carencia_w := 'SEM CAR�NCIAS A CUMPRIR ';
	end if;

	/*if	(nr_seq_contrato_w is not null) then
		open C04;
		loop
		fetch C04 into
			ds_mensagem_cart_w;
		exit when C04%notfound;
			begin

			if	(length(ds_mensagem_carteira_w||ds_mensagem_cart_w) > 255) then
				exit;
			else
				ds_mensagem_carteira_w := ds_mensagem_carteira_w||ds_mensagem_cart_w;
			end if;

			ds_mensagem_carteira_w := ds_mensagem_carteira_w||';';

			end;
		end loop;
		close C04;

		ds_mensagem_carteira_w := substr(ds_mensagem_carteira_w,1,length(ds_mensagem_carteira_w)-1);
	end if;*/

	ds_regiao_1_w	:= '';
	ds_regiao_2_w	:= '';
	ds_regiao_3_w	:= '';
	if	(ie_tipo_segurado_w = 'R') then
		select	max(b.nm_fantasia)
		into	ds_congenere_w
		from	pls_segurado_repasse	c,
			pessoa_juridica		b,
			pls_congenere		a
		where	b.cd_cgc		= a.cd_cgc
		and	a.nr_sequencia		= c.nr_seq_congenere
		and	c.nr_seq_segurado	= nr_seq_segurado_w
		and	c.dt_liberacao is not null
		and	c.dt_fim_repasse is null;

		ds_regiao_1_w	:= 'REPASSE ATT. ' || ds_congenere_w || ' ';
		ds_regiao_2_w	:= '';
		ds_regiao_3_w	:= '';
	else
		if	(cd_abrangencia_w in ('RB','MU')) then
			ds_regiao_1_w	:= 'Abrang�ncia Grupo de Munic�pios: ';
			ds_regiao_2_w	:= '';
			ds_regiao_3_w	:= '';

			open C02;
			loop
			fetch C02 into
				nr_seq_regiao_w;
			exit when C02%notfound;
				begin
				open C03;
				loop
				fetch C03 into
					ds_municipio_w;
				exit when C03%notfound;
					begin
					if	(length(ds_regiao_1_w||ds_municipio_w) <= 76) then
						ds_regiao_1_w := ds_regiao_1_w||ds_municipio_w||',';
					elsif	(length(ds_regiao_2_w||ds_municipio_w) <= 76) then
						ds_regiao_2_w := ds_regiao_2_w||ds_municipio_w||',';
					elsif	(length(ds_regiao_3_w||ds_municipio_w) <= 76) then
						ds_regiao_3_w := ds_regiao_3_w||ds_municipio_w||',';
					end if;
					end;
				end loop;
				close C03;

				end;
			end loop;
			close C02;
		end if;
	end if;

	ds_regiao_w	:= substr(ds_regiao_1_w,1,length(ds_regiao_1_w)-1)||';'||substr(ds_regiao_2_w,1,length(ds_regiao_2_w)-1)||';'||substr(ds_regiao_3_w,1,length(ds_regiao_3_w)-1);

	ds_mensagem_cpt_w	:= pls_obter_dados_cart_unimed(nr_seq_segurado_w, nr_seq_plano_w,'DCPT',6);
	ds_rede_atendimento_w	:= pls_obter_rede_ref_produto(nr_seq_plano_w,cd_estabelecimento_p,'C');

	if	(ds_rede_atendimento_w is null) then
		ds_rede_atendimento_w	:= ';';
	else
		ds_rede_atendimento_w	:= ds_rede_atendimento_w||' B�SICO';
	end if;

	if	(cd_abrangencia_w in ('ES','A')) then
		ds_observacao_1_w	:= 'ATENDIMENTO NO ESTADO DE SANTA CATARINA';
	elsif	(cd_abrangencia_w = 'NA') then
		ds_observacao_1_w	:= 'ATENDIMENTO EM TODO TERRIT�RIO NACIONAL';
	else
		ds_observacao_1_w	:= 'ATENDIMENTO SOMENTE NA AREA DE ACAO';
	end if;

	ds_observacao_2_w	:= 'URG�NCIA E EMERG�NCIA EM TODA A REDE NACIONAL';

	if	(ie_tipo_segurado_w = 'R') then
		select	max(a.cd_cooperativa)
		into	cd_local_cobranca_w
		from	pls_segurado_repasse	b,
			pls_congenere		a
		where	b.nr_seq_congenere	= a.nr_sequencia
		and	b.nr_seq_segurado	= nr_seq_segurado_w
		and	b.dt_liberacao is not null
		and	b.dt_fim_repasse is null;

		if	(cd_local_cobranca_w is null) then
			cd_local_cobranca_w	:= '0242';
		end if;
	else
		cd_local_cobranca_w	:= '0242';
	end if;

	cd_local_cobranca_w	:= lpad(cd_local_cobranca_w,4,'0');

	if	(cd_interface_p = 2704) then
		if	(ds_plano_carteira_w is not null) then
			nm_plano_w := substr(ds_plano_carteira_w,1,30) || ';' || substr(ds_plano_carteira_w,31,60);
		else
			nm_plano_w := substr(ds_produto_w,1,30) || ';' || substr(ds_produto_w,31,60);
		end if;
	else
		nm_plano_w := ds_produto_w;
	end if;

	insert into w_pls_interface_carteira
		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			nr_seq_lote,ie_tipo_reg,cd_usuario_plano,dt_nascimento,ds_tipo_contratacao,
			dt_validade_carteira,nm_beneficiario,cd_local_cobranca,nm_plano,cd_abrangencia,
			ds_abrangencia,ds_registro_ans,ds_acomodacao,cd_operadora_empresa,ds_cns ,dt_cpt,
			ds_estipulante,ds_rede_atendimento,ds_carencia,ds_observacao,ds_observacao_2,
			tp_contrato,nr_protocolo_ans,ds_trilha_1,ds_trilha_2,ds_trilha_3,nr_seq_segurado,
			nr_via_cartao,ds_regiao,ds_branco,ds_mensagem_carencia, ds_segmentacao,dt_adesao,
			nm_plano_2)
	values	(	w_pls_interface_carteira_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
			nr_seq_lote_p,2,cd_usuario_plano_w,dt_nascimento_w,tp_contratacao_w,
			dt_validade_carteira_w,nm_beneficiario_w,cd_local_cobranca_w,nm_plano_w,cd_abrangencia_w,
			ds_abrangencia_w,ds_registro_ans_w,ds_acomodacao_w,cod_empresa_w,nr_cartao_nac_sus_w,ds_mensagem_cpt_w,
			nm_empresa_w,ds_rede_atendimento_w,ds_carencia_w,ds_observacao_1_w,ds_observacao_2_w,
			tp_contrato_w,nr_protocolo_ans_w,ds_trilha_1_w,ds_trilha_2_w,ds_trilha_3_w,nr_seq_segurado_w,
			nr_via_solicitacao_w,ds_regiao_w,' ',ds_mensagem_carencia_w, ds_segmentacao_w,dt_inicio_vigencia_w,
			ds_produto_w);
	end;
end loop;
close C01;

commit;

end pls_gerar_cart_unimed_litoral;
/