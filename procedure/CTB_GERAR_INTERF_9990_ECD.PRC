create or replace
procedure ctb_gerar_interf_9990_ecd(
			nr_seq_controle_p		number,
			nm_usuario_p			Varchar2,
			cd_estabelecimento_p		number,
			dt_inicio_p			date,
			dt_fim_p			date,
			cd_empresa_p			number,
			qt_linha_p		in out	number,
			nr_sequencia_p		in out	number) is 


qt_registros_w			number(10);
ds_arquivo_w			varchar2(4000);
ds_compl_arquivo_w		varchar2(4000);
ds_linha_w			varchar2(8000);
nr_linha_w			number(10) := qt_linha_p;
nr_seq_registro_w		number(10) := nr_sequencia_p;

begin

select	(select count(*) + 1
from	ctb_sped_registro
where	nr_seq_controle_sped	= 11
and	substr(cd_registro,1,1)	= '9') qt_linhas
into	qt_registros_w
from	dual;

ds_linha_w	:= substr('|' || '9990' || '|' || qt_registros_w || '|',1,8000);

ds_arquivo_w		:= substr(ds_linha_w,1,4000);
ds_compl_arquivo_w	:= substr(ds_linha_w,4001,4000);
nr_seq_registro_w	:= nr_seq_registro_w + 1;
nr_linha_w		:= nr_linha_w + 1;	

insert into ctb_sped_registro(
		nr_sequencia,                   
		ds_arquivo,                     
		dt_atualizacao,                 
		nm_usuario,                     
		dt_atualizacao_nrec,            
		nm_usuario_nrec,                
		nr_seq_controle_sped,           
		ds_arquivo_compl,               
		cd_registro,
		nr_linha)
	values(
		nr_seq_registro_w,
		ds_arquivo_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		ds_compl_arquivo_w,
		'9990',
		nr_linha_w);
		
commit;
qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;
end ctb_gerar_interf_9990_ecd;
/