create or replace
procedure sus_vincular_apac_laudo(	nr_seq_interno_p		number,
				nr_apac_p		number,
				dt_retorno_secr_p		date,
				dt_inicio_val_p		date default null,
				dt_fim_val_p		date default null,
				nm_usuario_p		varchar) is

qt_meses_autorizado_w	number(5);
ie_gerar_apac_laudo_w	varchar2(1)	:= 'N';
nr_atendimento_w		number(10);
cd_motivo_alta_w		varchar2(3);
cd_motivo_cobranca_w	varchar2(2);

begin

ie_gerar_apac_laudo_w		:= obter_valor_param_usuario(1006,16,obter_perfil_ativo,nm_usuario_p,0);

begin
update	sus_laudo_paciente
set	nr_apac			= decode(nr_apac_p,0,null,nr_apac_p),
	dt_retorno_secr		= dt_retorno_secr_p,
	dt_inicio_val_apac		= nvl(dt_inicio_val_p,dt_inicio_val_apac),
	dt_fim_val_apac		= nvl(dt_fim_val_p,dt_fim_val_apac),
	nm_usuario		= nm_usuario_p,
	dt_atualizacao		= sysdate
where	nr_seq_interno		= nr_seq_interno_p;
exception
	when others then
	wheb_mensagem_pck.exibir_mensagem_abort(221834);
	/*N�o foi poss�vel vincular a APAC ao laudo.*/
	end;

if	(ie_gerar_apac_laudo_w = 'S') and
	(nvl(nr_apac_p,0) <> 0) then
	begin

	begin
	select	max(b.nr_atendimento),
		max(a.cd_motivo_alta)
	into	nr_atendimento_w,
		cd_motivo_alta_w
	from	atendimento_paciente a,
		sus_laudo_paciente b
	where	b.nr_atendimento = a.nr_atendimento
	and	b.nr_seq_interno = nr_seq_interno_p;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(221835);
		/*N�o foi poss�vel obter os dados do atendimento durante a gera��o da APAC atrav�s do laudo.*/
		end;

	begin
	select	b.cd_motivo_cobranca
	into	cd_motivo_cobranca_w
	from	motivo_alta a,
		sus_motivo_cobr_unif b
	where	a.cd_motivo_alta_sus = b.cd_motivo_cobranca
	and	a.cd_motivo_alta = cd_motivo_alta_w;
	exception
		when others then
		wheb_mensagem_pck.exibir_mensagem_abort(221836);
		/*N�o foi poss�vel obter o motivo de alta do paciente durante a gera��o da APAC atrav�s do laudo.*/
		end;

	if	(nvl(nr_atendimento_w,0) <> 0) and
		(nvl(cd_motivo_cobranca_w,'0') <> '0') then
		begin
		sus_gerar_apac_laudo_unif(nr_apac_p,nr_atendimento_w,cd_motivo_cobranca_w,null,null,nm_usuario_p);
		end;
	end if;

	end;
end if;

end sus_vincular_apac_laudo;
/