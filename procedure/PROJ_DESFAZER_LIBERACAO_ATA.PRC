create or replace
procedure proj_desfazer_liberacao_ata(	nr_sequencia_p	number,
					nm_usuario_p	Varchar2) is 

begin

update	proj_ata
set	dt_liberacao	= null,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_sequencia_p;

commit;

end proj_desfazer_liberacao_ata;
/