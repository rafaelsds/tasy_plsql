create or replace
procedure excluir_itens_prescr_cirurgica(	nr_prescricao_p	number,
					ds_classe_medic_excl_cir_p varchar2) is

begin

if	((nr_prescricao_p is not null) 
	and (ds_classe_medic_excl_cir_p is not null)) then
	begin
	
	delete	from prescr_material 
	where	nr_prescricao = nr_prescricao_p
	and	Obter_Se_Contido(obter_classe_material(cd_material), ds_classe_medic_excl_cir_p ) = 'S';
	
	commit;

	end;
end if;

end excluir_itens_prescr_cirurgica;
/


