create or replace
procedure TISS_OBTER_COD_PROCED_CONV(	cd_proc_convenio_p		varchar2,
					cd_convenio_p			number,
					cd_procedimento_p	IN OUT	number,
					ie_origem_proced_p	IN OUT	number) is

cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);

cursor c01 is
select	cd_procedimento,
	ie_origem_proced
from	conversao_proc_convenio
where	cd_convenio			= cd_convenio_p
and	ltrim(cd_proc_convenio,'0')	= ltrim(cd_proc_convenio_p,'0') /*No TISS completa com '0'(Zero) at� 8 casas, tendo enviado com convers�o ou n�o */
and	ie_situacao			= 'A'				/*Por este motivo � necess�rio retirar neste select, pois a convers�o pode ou n�o ter estes '0' (ZEROS)*/
order by nvl(cd_procedimento,0),					/*N�O PODE SER TO_NUMBER OU SOMENTE_NUMERO PORQUE PODE TER LETRAS NA CONVERS�O*/
	nvl(cd_grupo_proced,0),
	nvl(cd_especial_proced,0),
	nvl(cd_area_proced,0);

cursor c02 is
select	cd_material
from	conversao_material_convenio
where	ltrim(cd_material_convenio,'0')	= ltrim(cd_proc_convenio_p,'0')
and	cd_convenio			= cd_convenio_p
and	ie_situacao			= 'A';

begin

open c01;
loop
fetch c01 into
	cd_procedimento_w,
	ie_origem_proced_w;
exit when c01%notfound;
end loop;
close c01;

if	(nvl(cd_procedimento_w,0) = 0) then
	open c02;
	loop
	fetch c02 into
		cd_procedimento_w;
	exit when c02%notfound;
	end loop;
	close c02;
end if;

cd_procedimento_p	:= nvl(cd_procedimento_w,cd_procedimento_p);
ie_origem_proced_p	:= nvl(ie_origem_proced_w,ie_origem_proced_p);

end TISS_OBTER_COD_PROCED_CONV;
/