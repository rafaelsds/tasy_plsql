create or replace
procedure pls_atual_analise_conta_item
			(	nr_seq_item_analise_p			Number,
				nr_seq_conta_p				Number,
				nr_seq_conta_proc_p			Number,
				nr_seq_conta_mat_p			Number,				
				nr_seq_partic_proc_p			number,				
				nr_seq_conta_glosa_p			Number,
				nr_seq_ocorrencia_benef_p		Number,				
				qt_glosada_p				Number,
				vl_glosado_p				Number,				
				cd_estabelecimento_p			Number,
				nm_usuario_p				Varchar2,
				nr_seq_analise_p			Number,				
				nr_seq_glosa_oc_gerada_p	out	Number	) is
				
/*Diego OS 329472 - 22/06/20111 - Esta rotina possui a fun��o de copiar uma glosa ou ocorr�ncia da conta para a an�lise.*/
cd_codigo_w		varchar2(15);
ds_observacao_w		varchar2(4000);
ie_tipo_w		varchar2(1);	
nr_seq_conta_w		number(10);
nr_seq_conta_mat_w	number(10);
nr_seq_conta_proc_w	number(10);
nr_seq_glosa_oc_w	number(10);
ie_status_w		varchar2(1);
nr_seq_motivo_padrao_w	number(10);
ie_tipo_motivo_w	varchar2(1) := 'P';
nr_seq_item_w		number(10);
ie_existe_grupo_glosa_w varchar2(1);
nr_seq_conta_item_w	number(10);
nr_seq_glosa_ref_w	number(10);
qt_glosa_w		number(10);
vl_glosa_w		number(10,2);
ie_auditoria_w		varchar2(1);
nr_seq_ocorrencia_w	number(10);
nr_seq_glosa_w		number(10);
ie_finalizar_analise_w	varchar2(1);
ie_tipo_glosa_w		varchar2(1);
nr_seq_analise_item_w	number(10);
ds_obs_glosa_oc_w	varchar2(255);
ie_glosar_pagamento_w	varchar2(1);
ie_glosar_faturamento_w varchar2(1);
ie_origem_conta_w	varchar2(1);
nr_seq_grupo_w		number(10);
ie_tipo_despesa_proc_w	Varchar2(1);
ie_tipo_despesa_mat_w	Varchar2(1);
nr_seq_grupo_oc_w	Number(10);
qt_despesa_w		Number(10);
ie_gera_grupo_w		Varchar2(1) := 'S';
ie_origem_analise_w	pls_analise_conta.ie_origem_analise%type;
Cursor C01 is
	select	a.nr_seq_grupo,
		a.nr_sequencia
	from	pls_ocorrencia_grupo a,
		pls_ocorrencia_benef b,
		pls_ocorrencia c
	where	a.nr_seq_ocorrencia = c.nr_sequencia 
	and	b.nr_seq_ocorrencia = c.nr_sequencia
	and	nvl(a.ie_origem_conta,ie_origem_conta_w) = ie_origem_conta_w
	and	nvl(a.ie_conta_medica,'S') = 'S'
	and	((a.ie_tipo_analise	is null) or (a.ie_tipo_analise = 'A') or (a.ie_tipo_analise = 'C' and ie_origem_analise_w in ('1','3','4','5','6')) or
		(a.ie_tipo_analise	= 'P' and ie_origem_analise_w in ('2','7')))
	and	b.nr_sequencia = nr_seq_ocorrencia_w
	and	not exists	(select	x.nr_sequencia
				 from	pls_analise_grupo_item x
				 where	x.nr_seq_item_analise	= nr_seq_item_w
				 and	x.nr_seq_grupo		= a.nr_seq_grupo);

begin

select	substr(tiss_obter_motivo_glosa(nr_seq_motivo_glosa,'C'),1,10) cd_codigo,								
	substr(ds_observacao,1,255) ds_observacao,
	'G' ie_tipo,
	a.nr_seq_conta 		nr_seq_conta,
	a.nr_seq_conta_mat	nr_seq_mat,
	a.nr_seq_conta_proc	nr_seq_proc,
	a.nr_sequencia,
	null,
	a.qt_glosa,
	a.vl_glosa,
	null,
	null,
	a.nr_sequencia,
	null,
	'S',
	null
into	cd_codigo_w,
	ds_observacao_w,
	ie_tipo_w,
	nr_seq_conta_w,
	nr_seq_conta_mat_w,
	nr_seq_conta_proc_w,
	nr_seq_glosa_oc_w,
	nr_seq_glosa_ref_w,
	qt_glosa_w, 
	vl_glosa_w,
	ie_auditoria_w,
	nr_seq_ocorrencia_w,
	nr_seq_glosa_w,
	ie_finalizar_analise_w,
	ie_glosar_pagamento_w,
	ie_glosar_faturamento_w
from	pls_conta_glosa	a		
where	a.nr_sequencia 			= nr_seq_conta_glosa_p	
union
select	b.cd_ocorrencia cd_codigo,
	substr(b.ds_documentacao,1,4000) ds_observacao,
	'O' ie_tipo,
	a.nr_seq_conta	nr_seq_conta,
	a.nr_seq_mat	nr_seq_mat,
	a.nr_seq_proc	nr_seq_proc,										
	a.nr_sequencia,
	nvl(a.nr_seq_glosa, (	select	max(x.nr_sequencia)
				from	pls_conta_glosa x
				where	x.nr_seq_ocorrencia_benef = a.nr_sequencia ) ), -- Diego 28/04/2011 - Cria o vinculo com a glosa ser� aquela gerada pela ocorr�ncia ou que gerou a ocorr�ncia
	a.qt_glosa,
	a.vl_glosa,
	nvl(b.ie_auditoria_conta,'N'),	
	a.nr_sequencia,
	null,
	a.ie_finalizar_analise,
	b.ie_glosar_pagamento,
	b.ie_glosar_faturamento
from	pls_ocorrencia_benef	a,
	pls_ocorrencia		b
where	a.nr_seq_ocorrencia = b.nr_sequencia
and	nr_seq_guia_plano is null
and	nr_seq_requisicao is null
and	nr_seq_conta is not null
and 	a.nr_sequencia = nr_seq_ocorrencia_benef_p;

if	(ie_auditoria_w = 'N') and
	(ie_tipo_w = 'O') then
	/*Diego Os 342553 - Caso a ocorr�ncia n�o seja de auditoria ser� meramente informativa.*/
	ie_tipo_motivo_w := 'I';	
else
	if	(nvl(ie_finalizar_analise_w,'S') = 'S') then
		ie_tipo_motivo_w := 'P';
	else
		ie_tipo_motivo_w := 'E';
	end if;
end if;

if	(ie_tipo_w = 'O') 	 and
	(nr_seq_glosa_ref_w > 0) then
	
	select	max(nr_sequencia)
	into	nr_seq_analise_item_w
	from	pls_analise_conta_item
	where	ie_tipo		= 'G'
	and	nr_seq_glosa	= nr_seq_glosa_ref_w
	and	nr_seq_analise	= nr_seq_analise_p;
	
end if;	

if	(ie_glosar_pagamento_w	 = 'S') and
	(ie_glosar_faturamento_w = 'S') then
	ie_tipo_glosa_w := 'A';
elsif	(ie_glosar_pagamento_w = 'S') then
	ie_tipo_glosa_w := 'P';
elsif	(ie_glosar_faturamento_w = 'S') then
	ie_tipo_glosa_w := 'F';
else
	ie_tipo_glosa_w := null;
end if;

select	max(ie_origem_analise)
into	ie_origem_analise_w
from	pls_analise_conta
where	nr_sequencia = nr_seq_analise_p;


select	max(ie_origem_conta)
into	ie_origem_conta_w
from	pls_conta
where	nr_sequencia = nr_seq_conta_p;
if	(nvl(nr_seq_analise_item_w,0) > 0) then
	/*No caso de a glosa ter virado uma ocorr�ncia.*/
	update	pls_analise_conta_item
	set	ie_tipo 	  = 'O',
		nr_seq_glosa_oc   = nr_seq_glosa_oc_w,
		cd_codigo	  = cd_codigo_w,
		ds_obs_glosa_oc   = ds_observacao_w,
		nr_seq_glosa 	  = null,
		nr_seq_ocorrencia = nr_seq_ocorrencia_w,
		nr_seq_glosa_ref  = nr_seq_glosa_ref_w
	where	nr_sequencia	  = nr_seq_analise_item_w;
	
	nr_seq_item_w := nr_seq_analise_item_w;
else	
	select  pls_analise_conta_item_seq.nextval
	into	nr_seq_item_w
	from	dual;
	
	insert into pls_analise_conta_item
		(cd_codigo, ds_obs_glosa_oc, dt_atualizacao,
		 dt_atualizacao_nrec, ie_situacao, ie_status,
		 ie_tipo, nm_usuario, nm_usuario_nrec,
		 nr_seq_analise, nr_seq_conta, nr_seq_conta_mat,
		 nr_seq_conta_proc, nr_seq_glosa_oc, nr_sequencia,
		 qt_glosa, vl_glosa, nr_seq_glosa_ref,
		 nr_seq_ocorrencia, nr_seq_glosa, ie_finalizar_analise,
		 nr_seq_proc_partic, ie_principal, ie_tipo_glosa,
		 nr_seq_w_resumo_conta	)	
	values	(cd_codigo_w, ds_observacao_w, sysdate,
		 sysdate, 'A', ie_tipo_motivo_w,  
		 ie_tipo_w, nm_usuario_p, nm_usuario_p,
		 nr_seq_analise_p, nr_seq_conta_p, nr_seq_conta_mat_w,
		 nr_seq_conta_proc_w, nr_seq_glosa_oc_w, nr_seq_item_w,
		 nvl(qt_glosada_p,0), nvl(vl_glosado_p,0), nr_seq_glosa_ref_w,
		 nr_seq_ocorrencia_w, nr_seq_glosa_w, ie_finalizar_analise_w,
		 nr_seq_partic_proc_p, 'S', ie_tipo_glosa_w,
		 nr_seq_item_analise_p	);
end if;

if	(nvl(nr_seq_ocorrencia_benef_p,0) > 0) then	
	open C01;
	loop
	fetch C01 into	
		nr_seq_grupo_w,
		nr_seq_grupo_oc_w;
	exit when C01%notfound;
		begin
		ie_gera_grupo_w := 'S';
		
		ie_tipo_despesa_proc_w:= null;
		ie_tipo_despesa_mat_w:= null;
		
		select	count(1)
		into 	qt_despesa_w
		from	pls_oc_grupo_tipo_desp
		where	nr_seq_ocorrencia_grupo = nr_seq_grupo_oc_w;
		
		if	(qt_despesa_w > 0) then
			if	(nr_seq_conta_proc_p > 0) then
				select	max(ie_tipo_despesa)
				into	ie_tipo_despesa_proc_w
				from	pls_conta_proc
				where	nr_sequencia = nr_seq_conta_proc_p;
				
				if	(ie_tipo_despesa_proc_w is not null) then
					select	count(1)
					into 	qt_despesa_w
					from	pls_oc_grupo_tipo_desp
					where	nr_seq_ocorrencia_grupo = nr_seq_grupo_oc_w
					and	ie_tipo_despesa_proc = ie_tipo_despesa_proc_w;
					
					if	(nvl(qt_despesa_w,0) = 0) then
						ie_gera_grupo_w := 'N';
					end if;
				end if;
			elsif	(nr_seq_conta_mat_p > 0) then
				select	max(ie_tipo_despesa)
				into	ie_tipo_despesa_mat_w
				from	pls_conta_mat
				where	nr_sequencia = nr_seq_conta_mat_p;
				
				if	(ie_tipo_despesa_mat_w is not null) then
					select	count(1)
					into 	qt_despesa_w
					from	pls_oc_grupo_tipo_desp
					where	nr_seq_ocorrencia_grupo = nr_seq_grupo_oc_w
					and	ie_tipo_despesa_mat = ie_tipo_despesa_mat_w;
					
					if	(nvl(qt_despesa_w,0) = 0) then
						ie_gera_grupo_w := 'N';
					end if;
				end if;
			end if;
		end if;
		
		if	(ie_gera_grupo_w = 'S') then
			insert into pls_analise_grupo_item
				(nr_sequencia, cd_estabelecimento, dt_atualizacao,
				 nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
				 nr_seq_grupo, nr_seq_item_analise)
			values	(pls_analise_grupo_item_seq.nextval, cd_estabelecimento_p, sysdate,
				nm_usuario_p, sysdate, nm_usuario_p,
				nr_seq_grupo_w, nr_seq_item_w);
		end if;
		
		end;
	end loop;
	close C01;
end if;
	 
nr_seq_glosa_oc_gerada_p := nr_seq_item_w;

end pls_atual_analise_conta_item;
/