create or replace
procedure man_replicar_avaliacao_os(
			nr_seq_avaliacao_p	number,
			nr_seq_ordem_serv_p	number,
			nm_usuario_p		varchar2) is 

nr_seq_aval_w		number(10);
nr_seq_tipo_aval_w		number(10);
			
begin

select	nr_seq_tipo_avaliacao
into	nr_seq_tipo_aval_w
from	man_ordem_serv_avaliacao
where	nr_sequencia = nr_seq_avaliacao_p;

select	man_ordem_serv_avaliacao_seq.nextval
into	nr_seq_aval_w
from	dual;

insert into man_ordem_serv_avaliacao(
		nr_sequencia,
		nr_seq_ordem_servico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		dt_atualizacao,
		nm_usuario,
		dt_avaliacao,
		nr_seq_tipo_avaliacao)
	values(	nr_seq_aval_w,
		nr_seq_ordem_serv_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_tipo_aval_w);
		
insert into man_ordem_serv_aval_result(
		nr_seq_item, 
		nr_seq_ordem_serv_aval, 
		dt_atualizacao, 
		nm_usuario,
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		qt_resultado, 
		ds_resultado)
	select	nr_seq_item,
		nr_seq_aval_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		qt_resultado,
		ds_resultado
	from	man_ordem_serv_aval_result
	where	nr_seq_ordem_serv_aval = nr_seq_avaliacao_p;

commit;

end man_replicar_avaliacao_os;
/