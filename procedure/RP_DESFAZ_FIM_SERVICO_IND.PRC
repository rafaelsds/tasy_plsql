create or replace
procedure rp_desfaz_fim_servico_ind(nr_seq_servico_p	varchar2,
				    nm_usuario_p	Varchar2) is 		    
				        
begin

update	rp_pac_agend_individual
set	nr_seq_motivo_fim_tratamento = Null,
	dt_fim_tratamento 		= Null,
	nm_usuario 		= nm_usuario_p,
	dt_atualizacao		= sysdate,
	dt_desfeita 		= sysdate
where	nr_sequencia     		= nr_seq_servico_p;

update	agenda_consulta
set	ie_status_agenda 		= 'N',
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_seq_rp_item_ind 		= nr_seq_servico_p
and	ie_status_agenda not in ('A','B','E','N')
and	trunc(dt_agenda) > trunc(sysdate);

commit;

end rp_desfaz_fim_servico_ind;
/
