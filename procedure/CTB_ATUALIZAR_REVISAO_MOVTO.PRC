create or replace
procedure ctb_atualizar_revisao_movto(	nr_sequencia_p	number,
				ie_revisao_p	varchar2,
				nm_usuario_p	Varchar2) is 

begin

update	ctb_movimento
set	ie_revisado	= ie_revisao_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	nm_usuario_revisao	= decode(ie_revisao_p, 'S',nm_usuario_p, null),
	dt_revisao	= decode(ie_revisao_p, 'S',sysdate, null)
where	nr_sequencia	= nr_sequencia_p;
			
commit;

end ctb_atualizar_revisao_movto;
/