create or replace
procedure pls_atualiza_tiss_motivo_glosa is 

ie_possui_glosa_evento_w	varchar2(4000);
nr_seq_motivo_glosa_w		number(15);
nr_seq_base_wheb_w		number(10)	:= null;
cd_motivo_tiss_w		tiss_motivo_glosa.cd_motivo_tiss%type;

Cursor C01 is
	select	a.nr_sequencia,
		a.cd_motivo_tiss
	from	tiss_motivo_glosa	a
	where	a.cd_motivo_tiss 	IN ('1001', '1801', '9902');

begin
/* Atualizar a��o PLS */
update	tiss_motivo_glosa
set	ie_acao_pls	= 'N';

update	tiss_motivo_glosa
set	ie_acao_pls	= 'S'
where	cd_motivo_tiss in (	'1002','1005','1006','1009','1016','1018','1101','1104','1201','1206','1207','1208','1209','1212','1213',
				'1215','1216','1217','1299','1304','1312','1317','1321','1402','1405','1406','1411','1419','1504','1505',
				'1506','1603','1604','1607','1608','1610','1615','1806','1808','1899','1906','1913','1914','2005',
				'2009','2099','2105','2199','2204','2206','2210','2401','2601','2604','2699','9920');

update	tiss_motivo_glosa
set	ie_acao_pls	= 'D'
where	cd_motivo_tiss	in (	'1001','1004','1011','9919','9920','1013','1014','1017','1024','1099','1199','1202','1203','1210','1214',
				'1301','1302','1303','1306','1307','1308','1311','1399','1401','1402','1404','1410','1413','1416','1418',
				'1420','1421','1423','1499','1502','1503','1508','1509','1601','1602','1609','1701','1705','1706','1707',
				'1801','1802','1803','1815','1820','1901','1904','1909','1999','2001','2003','2006','2101','2201','2205',
				'2301','2421','2514','2603','2801','2818','9901','9902','9905','9906','9907','9908','9910','9911','9912',
				'9913','9914','9915','9916','3004','3007','3011','3027','9917','9919','5033','1838');

update	tiss_motivo_glosa
set	ie_permite_alteracao_ops = 'S'
where	cd_motivo_tiss = '9920';

-- jjung Atualizar campo ie_param_ops para 'N' pois est� nulo em clientes
update	tiss_motivo_glosa
set	ie_param_ops = 'N';

update	tiss_motivo_glosa
set	ie_param_ops	= 'S'
where	cd_motivo_tiss	in (	'1304','1308','1309','1401','1402','1404','1406','1508','1613','1615','1803','1808','1906','1909','1913',
				'1914','2206','9909','9910','9911','9913','9919','1014','1307','1705','1706');

open C01;
loop
fetch C01 into	
	nr_seq_motivo_glosa_w,
	cd_motivo_tiss_w;
exit when C01%notfound;
	begin
	select	count(1)
	into	ie_possui_glosa_evento_w
	from	pls_glosa_evento	a
	where	a.nr_seq_motivo_glosa	= nr_seq_motivo_glosa_w
	and	a.ie_evento 		= 'IA'
	and	rownum			<= 1;
	
	if	(ie_possui_glosa_evento_w = 0) then
		begin
		select	max(nr_sequencia)
		into	nr_seq_base_wheb_w
		from	pls_glosa_evento a
		where	a.cd_motivo_tiss = cd_motivo_tiss_w
		and	a.ie_evento	= 'IA';
		
		if	(nr_seq_base_wheb_w is null) then
			select	pls_glosa_evento_seq.nextval
			into	nr_seq_base_wheb_w
			from	dual;
		end if;
		
		insert into pls_glosa_evento
			(nr_sequencia,
			nr_seq_motivo_glosa,
			ie_evento,
			ie_plano,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec)
		values	(nr_seq_base_wheb_w,
			nr_seq_motivo_glosa_w,
			'IA',
			'S',
			sysdate,
			sysdate,
			'Tasy',
			'Tasy');
		exception
		when others then
			tasy_posicionar_sequence('PLS_GLOSA_EVENTO','NR_SEQUENCIA','R');
		end;
	else
		update 	pls_glosa_evento
		set	ie_plano 		= 'S',
			nm_usuario		= 'Tasy',
			dt_atualizacao		= sysdate
		where	nr_seq_motivo_glosa 	= nr_seq_motivo_glosa_w
		and	ie_evento 		= 'IA';
	end if;	
	end;
end loop;
close C01;

commit;

end pls_atualiza_tiss_motivo_glosa;
/