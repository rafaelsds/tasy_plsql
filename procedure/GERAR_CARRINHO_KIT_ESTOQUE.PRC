create or replace
procedure gerar_carrinho_kit_estoque(	cd_estabelecimento_p	in number,
					cd_pessoa_fisica_p		in number,
					nm_usuario_p		in varchar2,
					nr_prescricao_p		in number,
					cd_local_estoque_p	in number,
					ds_retorno_p		out varchar2, -- para registro de (kit kit_estoque_reg)
					ds_retorno_2_p		out varchar2) -- para kit (kit_estoque)
					is 


cd_kit_material_w			number(5);
nr_sequencia_w			number(10);
nr_seq_kit_estoque_w		number(10);	
nr_seq_kit_material_w		number(10);	
cd_material_w			number(6);
cd_material_original_w		number(6);
qt_material_w			number(11,4);
nr_seq_kit_estoque_comp_w 		number(10);	
nr_seq_lote_fornec_w		number(10);
qt_material_prescr_w		number(11,4);
qt_material_subst_w		number(6);
nr_seq_devolucao_w		number(10);
qt_material_atendido_w		number(15,3);
cd_material_atendido_w		number(6);
nr_seq_lote_fornec_atendido_w	number(10);
nr_seq_kit_estoque_atendido_w	number(10);
qt_material_consistido_w		number(15,3);
nr_seq_reg_kit_w			number(10);
nr_seq_reg_kit_ant_w		number(10) := 0;
ds_retorno_w			varchar2(255):= null;
ds_retorno_ww			varchar2(255):= null;
nr_seq_item_kit_w			number(5);
ds_registro_kit_w		varchar2(255);

qt_loop_w			number(5);
i				number(10);
qt_consistir_w			number(15,4);
qt_saldo_dev_w			number(15,4);

ie_estrutura_original_w		varchar2(1);
ie_gerado_barras_w 		varchar2(1);
ds_erro_w			varchar2(4000);
ds_aviso_w			varchar2(4000);

nr_sequencia_ww		number(5);
qt_material_ww		number(14,3);
qt_existe_ww		number(5);
cd_material_ww		number(6);
ie_gerado_barras_ww	varchar2(1);
nr_seq_lote_fornec_ww	number(10);
nr_Seq_item_kit_ww	number(5);
ie_somente_reg_kit_estoque_w	varchar2(1);
ie_kit_basico_desdobrado_w	varchar2(1);
ie_basico_w			varchar2(1);

cursor c01 is
select	a.nr_seq_kit_estoque,
	nvl(decode(ie_kit_basico_desdobrado_w,'S', nvl(b.nr_seq_reg_kit_basico, b.nr_seq_reg_kit),b.nr_seq_reg_kit),0) nr_seq_reg_kit
from 	prescr_material a,
	kit_estoque b
where	a.nr_seq_kit_estoque = b.nr_sequencia
and	a.nr_prescricao =  nr_prescricao_p
and 	a.nr_seq_kit_estoque is not null
and	ie_somente_reg_kit_estoque_w in ('S','Q') 
and	nvl(a.ie_gerado_kit_estoque,'N') = 'N'
and	obter_se_kit_gera_remontagem(b.cd_kit_material) = 'S'
group by a.nr_seq_kit_estoque,
      nvl(decode(ie_kit_basico_desdobrado_w,'S', nvl(b.nr_seq_reg_kit_basico, b.nr_seq_reg_kit),b.nr_seq_reg_kit),0)
union all
select 	a.nr_seq_kit_estoque,
	nvl(decode(ie_kit_basico_desdobrado_w,'S', nvl(b.nr_seq_reg_kit_basico, b.nr_seq_reg_kit),b.nr_seq_reg_kit),0) nr_seq_reg_kit
from 	prescr_material a,
	kit_estoque b
where	a.nr_seq_kit_estoque = b.nr_sequencia
and	a.nr_prescricao =  nr_prescricao_p
and 	a.nr_seq_kit_estoque is not null
and	a.nr_seq_reg_kit is not null
and 	ie_somente_reg_kit_estoque_w in ('R','RQ') 
and	nvl(a.ie_gerado_kit_estoque,'N') = 'N'
and	obter_se_kit_gera_remontagem(b.cd_kit_material) = 'S'
group by a.nr_seq_kit_estoque,
      nvl(decode(ie_kit_basico_desdobrado_w,'S', nvl(b.nr_seq_reg_kit_basico, b.nr_seq_reg_kit),b.nr_seq_reg_kit),0)
order by 2,1;

cursor c02 is 	
select 	a.cd_material, 
	a.nr_seq_item_kit,
	sum(a.qt_material),
	nvl(max(a.nr_seq_lote_fornec),0)
from 	kit_estoque_comp a
where	a.nr_seq_kit_estoque = nr_seq_kit_material_w
group by a.cd_material, 
	a.nr_seq_lote_fornec,
	a.nr_seq_item_kit;

Cursor C03 is
SELECT	MIN(nr_sequencia),
	SUM(qt_material),
	COUNT(*),
	cd_material,
	ie_gerado_barras,
	nr_seq_lote_fornec,
	nr_Seq_item_kit
FROM	kit_Estoque_comp
WHERe	nr_seq_kit_Estoque = nr_seq_kit_estoque_w
GROUP BY cd_material,
	ie_gerado_barras,
	nr_seq_lote_fornec,
	nr_Seq_item_kit;
	
begin
ie_estrutura_original_w := substr(nvl(obter_valor_param_usuario(900, 423, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N'),1,1);

/* Somente ir� gerar os Kits de Estoque(Agrupados) */
ie_somente_reg_kit_estoque_w	:=	substr(nvl(obter_valor_param_usuario(900, 254, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N'),1,1);
ie_kit_basico_desdobrado_w	:=	substr(nvl(obter_valor_param_usuario(900, 543, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N'),1,1);

open c01;
loop
fetch c01 into	
	nr_seq_kit_material_w,
	nr_seq_reg_kit_w;
exit when c01%notfound;
	begin
	if	(ie_kit_basico_desdobrado_w = 'S') then
		select	nvl(max(ie_basico),'N')
		into	ie_basico_w
		from	kit_estoque_reg
		where	nr_sequencia = nr_seq_reg_kit_w;
	else
		ie_basico_w	:= 'N';
	end if;
	
	if	(nr_seq_reg_kit_w <> nr_seq_reg_kit_ant_w) then
		begin
		
		select	ds_registro_kit
		into	ds_registro_kit_w
		from	kit_estoque_reg
		where	nr_sequencia = nr_seq_reg_kit_w;
			
		select	kit_estoque_reg_seq.nextval
		into	nr_sequencia_w
		from	dual;
		
		if	(ds_retorno_w is null) then
			ds_retorno_w := nr_sequencia_w;
		else	
			ds_retorno_w := ds_retorno_w ||','|| nr_sequencia_w;
		end if;	

		insert 	into kit_estoque_reg(
			nr_sequencia,
			cd_estabelecimento,  
			dt_atualizacao,
			nm_usuario,                
			dt_atualizacao_nrec,
			nm_usuario_nrec,           
			cd_pessoa_resp,               
			dt_liberacao,                
			nm_usuario_lib,
			ds_registro_kit,                
			dt_utilizacao,               
			nm_usuario_util,
			ie_situacao,               
			ie_excluido,                   
			nr_seq_motivo_exclusao,
			nr_seq_pedido_agenda,
			ie_basico,
			cd_local_estoque,
			ie_origem,
			nr_seq_reg_origem)
		values (	nr_sequencia_w,
			cd_estabelecimento_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_pessoa_fisica_p,
			null,
			null,
			ds_registro_kit_w,
			null,
			null,
			'A',
			'N',
			null,
			null,
			ie_basico_w,
			cd_local_estoque_p,
			'S',
			nr_seq_reg_kit_w);
			
		nr_seq_reg_kit_ant_w := nr_seq_reg_kit_w;
		end;			
	end if;
	
	select	cd_kit_material
	into	cd_kit_material_w
	from 	kit_estoque
	where 	nr_sequencia = nr_seq_kit_material_w;
	
	select	kit_estoque_seq.nextval
	into	nr_seq_kit_estoque_w
	from	dual;
	
	if	(nr_seq_reg_kit_w = 0) then
		if	(ds_retorno_ww is null) then
			ds_retorno_ww := nr_seq_kit_estoque_w;
		else	
			ds_retorno_ww := ds_retorno_ww ||','|| nr_seq_kit_estoque_w;
		end if;
	end if;				
	
	insert 	into kit_estoque(
		nr_sequencia, 
		cd_kit_material,
		dt_atualizacao,
		nm_usuario,
		dt_montagem,
		nm_usuario_montagem,
		dt_utilizacao,           
		nm_usuario_util,
		nr_cirurgia,
		cd_estabelecimento,
		cd_medico,
		nr_prescricao,
		nr_atendimento,
		cd_local_estoque,
		nr_seq_reg_kit,
		ie_status,
		nr_seq_kit_origem)
	values (	nr_seq_kit_estoque_w,
		cd_kit_material_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		null,
		null,
		null,
		cd_estabelecimento_p,
		null,
		null,
		null,
		cd_local_estoque_p,
		nr_sequencia_w,
		null,
		nr_seq_kit_material_w);
		
	if	(ie_estrutura_original_w = 'S') then
		begin
		begin
		gera_componente_kit_mat(cd_kit_material_w, nr_seq_kit_estoque_w, 'A', 'S', nm_usuario_p, ds_erro_w);
		exception
		when others then
			ds_erro_w := substr(ds_erro_w,1,4000);
		end;
		end;
	end if;
	
	open c02;
	loop
	fetch c02 into	
		cd_material_w,
		nr_seq_item_kit_w,
		qt_material_w,
		nr_seq_lote_fornec_w;
	exit when c02%notfound;
		begin
		select	nvl(sum(qt_material),0),
			nvl(max(cd_material),0),
			nvl(max(nr_seq_lote_fornec),0)
		into	qt_material_atendido_w,
			cd_material_atendido_w,
			nr_seq_lote_fornec_atendido_w
		from (
			select 	sum(qt_material) qt_material, 
				max(cd_material) cd_material,
				max(nr_seq_lote_fornec) nr_seq_lote_fornec
			from 	material_atend_paciente
			where 	nr_prescricao = nr_prescricao_p
			and	nr_seq_kit_estoque = nr_seq_kit_material_w
			and	cd_material = cd_material_w
			and	nvl(nr_seq_lote_fornec,0) = nr_seq_lote_fornec_w
			and 	nr_seq_kit_estoque is not null
			union all
			select 	sum(qt_material),
				max(cd_material),
				max(nr_seq_lote_fornec)
			from 	prescr_material a
			where	a.nr_prescricao =  nr_prescricao_p
			and 	a.nr_seq_kit_estoque is not null
			and	a.cd_material = cd_material_w
			and	a.nr_seq_kit_estoque = nr_seq_kit_material_w
			and	nvl(nr_seq_lote_fornec,0) = nr_seq_lote_fornec_w
			and	a.qt_material		 = 0
			and	nvl(a.ie_gerado_kit_estoque,'N') = 'N'
			union all
			select 	sum(qt_material),
				max(cd_material),
				max(nr_seq_lote_fornec)
			from 	prescr_material a
			where	a.nr_prescricao =  nr_prescricao_p
			and 	a.nr_seq_kit_estoque is not null
			and	a.cd_material = cd_material_w 
			and	a.nr_seq_kit_estoque = nr_seq_kit_material_w
			and	nvl(nr_seq_lote_fornec,0) = nr_seq_lote_fornec_w
			and	a.qt_material		 > 0
			and	nvl(a.ie_gerado_kit_estoque,'N') = 'N'
			and	not exists(select 	1
					   from 	material_atend_paciente
					   where 	nr_prescricao = nr_prescricao_p
					   and		nr_seq_kit_estoque = nr_seq_kit_material_w
					   and		cd_material = cd_material_w
					   and		nvl(nr_seq_lote_fornec,0) = nr_seq_lote_fornec_w
					   and 		nr_seq_kit_estoque is not null));		
			
		if	(cd_material_w > 0) then
			begin
			if	(qt_material_atendido_w = 0) then
				begin	
				if	(ie_estrutura_original_w = 'S') then
					begin
					select	decode(nr_seq_lote_fornec_w,0,null,nr_seq_lote_fornec_w)
					into	nr_seq_lote_fornec_ww
					from	dual;
					
					qt_loop_w 	:= ceil(qt_material_w);
					qt_saldo_dev_w	:= qt_material_w;
					
					for i in 1..qt_loop_w loop
						begin
						if	(qt_saldo_dev_w > 1) then
							qt_consistir_w	:= 1;
						else
							qt_consistir_w := qt_saldo_dev_w;
						end if;
						
						qt_saldo_dev_w := (qt_saldo_dev_w - qt_consistir_w);
						
						if	(qt_consistir_w > 0) then
							begin
							begin
							ds_erro_w := null;
							consist_leitura_comp_kit_barra(	
								nr_seq_kit_estoque_w, 
								cd_material_w, 
								qt_consistir_w, 
								'S', 
								'S', 
								nr_seq_item_kit_w, 
								'C',
								0,
								ds_erro_w,
								ds_aviso_w);
								
							if	(ds_erro_w is null) then
								consist_componente_kit_barra(
									cd_material_w, 
									nr_seq_kit_estoque_w, 
									qt_consistir_w, 
									nm_usuario_p, 
									'S', 
									'S', 
									nr_seq_lote_fornec_ww, 
									nr_seq_item_kit_w, 
									'C',
									ds_erro_w);
							end if;
							exception
							when others then
								ds_erro_w := substr(ds_erro_w,1,4000);
							end;
							end;
						end if;
						end;
					end loop;					
					end;
				else
					begin
				
					select	nvl(max(nr_sequencia),0) + 1
					into	nr_seq_kit_estoque_comp_w
					from	kit_estoque_comp
					where	nr_seq_kit_estoque = nr_seq_kit_estoque_w;
					
					insert into kit_estoque_comp(
						nr_seq_kit_estoque,
						nr_sequencia,
						cd_material,
						dt_atualizacao,
						nm_usuario,
						qt_material,
						ie_gerado_barras,
						nr_seq_lote_fornec,
						nr_seq_item_kit)
					values (	nr_seq_kit_estoque_w,
						nr_seq_kit_estoque_comp_w,
						cd_material_w,
						sysdate,
						nm_usuario_p,
						qt_material_w,
						'S',
						decode(nr_seq_lote_fornec_w,0,null,nr_seq_lote_fornec_w),
						nr_seq_item_kit_w);
					end;
				end if;
				end;
			end if;		
			
			if	(nvl(qt_material_w,0) > 0) and	(nvl(qt_material_atendido_w,0) > 0) then
				begin
				qt_material_consistido_w := 0;
				
				if	(cd_material_atendido_w = cd_material_w) and
					(nr_seq_lote_fornec_w = nr_seq_lote_fornec_atendido_w) then
					qt_material_consistido_w := (qt_material_w - qt_material_atendido_w);
				end if;
				
				if	(qt_material_consistido_w > 0) then
					begin		
					if	(ie_estrutura_original_w = 'S') then
						begin
						select	decode(nr_seq_lote_fornec_w,0,null,nr_seq_lote_fornec_w)
						into	nr_seq_lote_fornec_ww
						from	dual;
						
						qt_loop_w 	:= ceil(qt_material_consistido_w);
						qt_saldo_dev_w	:= qt_material_consistido_w;
						
						for i in 1..qt_loop_w loop
							begin
							if	(qt_saldo_dev_w > 1) then
								qt_consistir_w	:= 1;
							else
								qt_consistir_w := qt_saldo_dev_w;
							end if;
							
							qt_saldo_dev_w := (qt_saldo_dev_w - qt_consistir_w);
							
							if	(qt_consistir_w > 0) then
								begin
								begin
								ds_erro_w := null;
								consist_leitura_comp_kit_barra(	
									nr_seq_kit_estoque_w, 
									cd_material_w, 
									qt_consistir_w, 
									'S', 
									'S', 
									nr_seq_item_kit_w, 
									'C', 
									0,
									ds_erro_w,
									ds_aviso_w);
									
								if	(ds_erro_w is null) then
									consist_componente_kit_barra(
										cd_material_w, 
										nr_seq_kit_estoque_w, 
										qt_consistir_w, 
										nm_usuario_p, 
										'S', 
										'S', 
										nr_seq_lote_fornec_ww, 
										nr_seq_item_kit_w, 
										'C', 
										ds_erro_w);
								end if;
								exception
								when others then
									ds_erro_w := substr(ds_erro_w,1,4000);
								end;
								end;
							end if;
							end;
						end loop;
						end;
					else
						begin					
						select	nvl(max(nr_sequencia),0) + 1
						into	nr_seq_kit_estoque_comp_w
						from	kit_estoque_comp
						where	nr_seq_kit_estoque = nr_seq_kit_estoque_w;
						
						insert into kit_estoque_comp(
							nr_seq_kit_estoque,
							nr_sequencia,
							cd_material,
							dt_atualizacao,
							nm_usuario,
							qt_material,
							ie_gerado_barras,
							nr_seq_lote_fornec,
							nr_seq_item_kit)
						values (	nr_seq_kit_estoque_w,
							nr_seq_kit_estoque_comp_w,
							cd_material_w,
							sysdate,
							nm_usuario_p,
							qt_material_consistido_w,
							'S',
							decode(nr_seq_lote_fornec_w,0,null,nr_seq_lote_fornec_w),
							nr_seq_item_kit_w);	
						end;
					end if;
					end;
				end if;	
				end;
			end if;
			
			if 	(qt_material_atendido_w > 0) and
				(ie_estrutura_original_w <> 'S') then
				begin					
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_seq_kit_estoque_comp_w
				from	kit_estoque_comp
				where	nr_seq_kit_estoque = nr_seq_kit_estoque_w;
				
				insert into kit_estoque_comp(
					nr_seq_kit_estoque,
					nr_sequencia,
					cd_material,
					dt_atualizacao,
					nm_usuario,
					qt_material,
					ie_gerado_barras,
					nr_seq_lote_fornec,
					nr_seq_item_kit)
				values (	nr_seq_kit_estoque_w,
					nr_seq_kit_estoque_comp_w,
					cd_material_w,
					sysdate,
					nm_usuario_p,
					qt_material_atendido_w,
					'N',
					decode(nr_seq_lote_fornec_w,0,null,nr_seq_lote_fornec_w),
					nr_seq_item_kit_w);				
				end;
			end if;
			
			
			end;
		end if;			
		end;
	end loop;
	close c02; 
	
	if	(ie_estrutura_original_w = 'S') then
		begin
		open C03;
		loop
		fetch C03 into	
			nr_sequencia_ww,
			qt_material_ww,
			qt_existe_ww,
			cd_material_ww,
			ie_gerado_barras_ww,
			nr_seq_lote_fornec_ww,
			nr_Seq_item_kit_ww;
		exit when C03%notfound;
			begin
			if	(qt_existe_ww > 1) then
				begin
				update	kit_Estoque_comp
				set	qt_material = qt_material_ww
				where	nr_Seq_kit_Estoque = nr_SeQ_kit_Estoque_w
				and	cd_material = cd_material_ww
				and	ie_gerado_barras = ie_gerado_barras_ww
				and	nvl(nr_Seq_item_kit,0) = nvl(nr_Seq_item_kit_ww,0)
				and	nvl(nr_seq_lote_fornec,0) = nvl(nr_seq_lote_fornec_ww,0);
				
				delete	kit_Estoque_comp
				where	nr_Seq_kit_Estoque = nr_SeQ_kit_Estoque_w
				and	cd_material = cd_material_ww
				and	ie_gerado_barras = ie_gerado_barras_ww
				and	nvl(nr_Seq_item_kit,0) = nvl(nr_Seq_item_kit_ww,0)
				and	nvl(nr_seq_lote_fornec,0) = nvl(nr_seq_lote_fornec_ww,0)
				and	nr_sequencia <> nr_sequencia_ww;
				end;
			end if;
			end;
		end loop;
		close C03;
		
		update	kit_Estoque_comp
		set	nr_sequencia = rownum
		where	nr_seq_kit_estoque = nr_SeQ_kit_Estoque_w;
		end;
	end if;
	end;
end loop;
close c01;

update	prescr_material
set	ie_gerado_kit_estoque 	= 'S'
where	nr_prescricao 		= nr_prescricao_p;
commit;

ds_retorno_p 	:= ds_retorno_w;
ds_retorno_2_p	:= ds_retorno_ww;

end gerar_carrinho_kit_estoque;
/
