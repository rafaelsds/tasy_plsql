CREATE OR REPLACE
PROCEDURE Inativar_protocolo_onco (
				nr_seq_paciente_p	number,
				ie_inativar_ant_p	varchar2,
				ds_mensagem_p		out varchar2) is

nr_seq_paciente_w		Number(10);
cd_pessoa_fisica_w	Varchar2(10);
cd_protocolo_w		number(10);
nr_seq_medicacao_w	number(6);
ds_mensagem_w		varchar2(32000);	

BEGIN


select	max(cd_protocolo),
	max(nr_seq_medicacao),
	max(cd_pessoa_fisica)
into	cd_protocolo_w,
	nr_seq_medicacao_w,
	cd_pessoa_fisica_w
from	paciente_setor
where	nr_seq_paciente	= nr_seq_paciente_p;

if	(ie_inativar_ant_p = 'S') then
	
	update	paciente_setor
	set	ie_status = 'I'
	where	cd_pessoa_fisica	= cd_pessoa_fisica_w
	and	ie_status <> 'F'	
	and	nr_seq_paciente	<> nr_seq_paciente_p;

elsif 	(ie_inativar_ant_p = 'M') and 
	( cd_protocolo_w is not null) and
	( nr_seq_medicacao_w is not null) then
                
	update	paciente_setor
	set	ie_status = 'I'
	where	cd_pessoa_fisica 	= cd_pessoa_fisica_w
	and	nr_seq_paciente 	<> nr_seq_paciente_p
	and	ie_status <> 'F'
	and	nr_seq_medicacao 	=  nr_seq_medicacao_w
	and	cd_protocolo 	=  cd_protocolo_w;

elsif ( ie_inativar_ant_p = 'T') then

	Consistir_inativar_protocolo(cd_pessoa_fisica_w,nr_seq_paciente_p,'I', ds_mensagem_w);

elsif	(ie_inativar_ant_p = 'Q') then
	Consistir_inativar_protocolo(cd_pessoa_fisica_w,nr_seq_paciente_p,'VR',ds_mensagem_w);
	if	(ds_mensagem_w is not null) then	
		ds_mensagem_p := substr(ds_mensagem_w,1,255);
	end if;
end if;

commit;

END Inativar_protocolo_onco;
/