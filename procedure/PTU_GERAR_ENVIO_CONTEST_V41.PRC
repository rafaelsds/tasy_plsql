create or replace 
procedure ptu_gerar_envio_contest_v41
			(	nr_seq_camara_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar o arquivo A550 V4.1
-------------------------------------------------------------------------------------------------------------------
OPS - Controle de Contestações
Locais de chamada direta: 
[X]  Objetos do dicionário [ ]  Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
				
-- 552
ds_conteudo_aux_w		varchar2(650);
nr_seq_questionamento_w		number(10);
ie_tipo_acordo_w		varchar2(2);

-- 553
ds_conteudo_553_w		varchar2(600);

-- 559
qt_552_w			number(10)	:= 0;

ds_conteudo_w			varchar2(650);
nr_seq_registro_w		number(10)	:= 0;
nr_seq_nota_cobranca_w		number(10);
nr_seq_ptu_fatura_w		number(10);
nr_seq_lote_contest_w		number(10);
nr_seq_nota_servico_w		number(10);
vl_reconhecido_w		number(15,2) := 0;
vl_cobrado_w			number(15,2) := 0;
vl_contestacao_w		number(15,2) := 0;
ie_tipo_arquivo_w		number(5);
ie_motivo_ques_w		varchar2(1) := 'N';
vl_tot_aco_servico_w		number(15,2) := 0;
vl_tot_aco_taxa_w		number(15,2) := 0;

Cursor C00 is
	select	'552' ||
		lpad(nvl(nr_lote,'0'),8,'0') ||
		lpad(' ',4,' ') ||
		lpad(nvl(nr_nota,'0'),11,'0') ||
		lpad(nvl(cd_unimed,'0'),4,'0') ||
		lpad(nvl(cd_usuario_plano,' '),13,' ') ||
		rpad(nvl(elimina_caractere_especial(elimina_acentuacao(upper(substr(nm_beneficiario,1,25)))),' '),25,' ') ||
		lpad(nvl(to_char(dt_atendimento,'yyyy/mm/dd') || to_char(dt_atendimento,'hh24:mi:ss') ||'-03',' '),21,' ') ||
		lpad(' ',124,' ') ||
		nvl(to_char(ie_tipo_tabela),' ') ||
		lpad(nvl(cd_servico,'0'),8,'0') ||
		lpad(replace(replace(campo_mascara_virgula(
				nvl(pls_obter_dados_itens_fatura(nr_seq_conta_proc,nr_seq_conta_mat,'vsc'),0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconhecido,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo,0)),'.',''),',',''),14,'0') ||
		lpad(nvl(to_char(dt_acordo,'yyyymmdd'),' '),8,' ') ||
		lpad(nvl(ie_tipo_acordo,' '),2,' ') ||
		lpad(nvl(qt_cobrada,'0'),8,'0') ||
		rpad(nvl(elimina_acentuacao(ds_servico),' '),80,' ') ||
		lpad(nvl(nr_seq_a500,'0'),8,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_adic_serv,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_adic_serv,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_adic_serv,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_adic_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_adic_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_adic_co,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobr_adic_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconh_adic_filme,0)),'.',''),',',''),14,'0') ||
		lpad(replace(replace(campo_mascara_virgula(nvl(vl_acordo_adic_filme,0)),'.',''),',',''),14,'0') ||
		lpad(nvl(qt_reconh,'0'),8,'0') ||
		nvl(ie_pacote,'N') ||
		lpad(nvl(cd_pacote,'0'),8,'0') ||
		lpad(nvl(to_char(dt_servico,'yyyymmdd'),' '),8,' ') ||
		lpad(nvl(hr_realiz,' '),8,' '),
		nr_sequencia,
		lpad(nvl(ie_tipo_acordo,' '),2,' ')
	from	ptu_questionamento
	where	nr_seq_contestacao	= nr_seq_camara_p
	and	ie_tipo_acordo not in ('11');

Cursor C01 is
	select	'553' ||
		lpad((substr(a.cd_motivo,1,4)),4,'0') ||
		rpad(trim(decode(b.ds_parecer_glosa, null, substr(replace(elimina_caractere_especial(Elimina_Acentuacao(a.ds_motivo)),'|',''),1,520), 
						replace(elimina_caractere_especial(Elimina_Acentuacao(b.ds_parecer_glosa)),'|',''))),520, ' ')
	from	ptu_questionamento_codigo	b,
		ptu_motivo_questionamento	a
	where	a.nr_sequencia		= b.nr_seq_mot_questionamento
	and	b.nr_seq_registro	= nr_seq_questionamento_w
	and	ie_motivo_ques_w = 'N'
	union
	select	'553' ||
		lpad((substr(a.cd_motivo,1,4)),4,'0') ||
		rpad(trim(replace(elimina_caractere_especial(Elimina_Acentuacao(a.ds_motivo)),'|','')),520,' ')
	from	ptu_motivo_questionamento	a
	where	a.cd_motivo	= '99'
	and	ie_motivo_ques_w = 'S';

begin
delete w_ptu_envio_arq where nm_usuario = nm_usuario_p;

nr_seq_registro_w	:= nr_seq_registro_w + 1;

select	max(nr_seq_lote_contest),
	max(ie_tipo_arquivo)
into	nr_seq_lote_contest_w,
	ie_tipo_arquivo_w
from	ptu_camara_contestacao
where	nr_sequencia	= nr_seq_camara_p;

select	max(nr_seq_ptu_fatura)
into	nr_seq_ptu_fatura_w
from	pls_lote_contestacao
where	nr_sequencia = nr_seq_lote_contest_w;

vl_reconhecido_w := 0;
vl_cobrado_w	 := 0;
vl_contestacao_w := 0;

select	sum(nvl(x.vl_reconhecido,0) + nvl(x.vl_reconh_adic_co,0) + 
	nvl(vl_reconh_adic_filme,0) + nvl(x.vl_reconh_adic_serv,0) + 
	nvl(x.vl_reconh_co,0) + nvl(x.vl_reconh_filme,0)) vl_rec,
	sum(nvl(x.vl_cobrado,0) + nvl(x.vl_cobr_adic_co,0) +
	nvl(x.vl_cobr_adic_filme,0) + nvl(x.vl_cobr_adic_serv,0) +
	nvl(x.vl_cobr_co,0) + nvl(x.vl_cobr_filme,0)) vl_cobr
into	vl_reconhecido_w,
	vl_cobrado_w
from	ptu_questionamento x
where	x.nr_seq_contestacao = nr_seq_camara_p
and	x.ie_tipo_acordo not in ('11');

vl_contestacao_w := vl_cobrado_w - vl_reconhecido_w;

select	lpad(nr_seq_registro_w,8,'0') ||
	'551' ||
	lpad(nvl(cd_unimed_destino,'0'),4,'0') ||
	lpad(nvl(cd_unimed_origem,'0'),4,'0') ||
	rpad(nvl(to_char(dt_geracao,'yyyymmdd'),' '),8,' ') ||
	lpad(nvl(cd_unimed_credora,'0'),4,'0') ||
	lpad(' ',11,' ') ||
	lpad(nvl(nr_fatura,'0'),11,'0') ||
	rpad(nvl(to_char(dt_venc_fatura,'yyyymmdd'),' '),8,' ') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_fatura,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_contestacao,vl_contestacao_w)),'.',''),',',''),14,'0') ||
	lpad('0',14,'0') ||
	nvl(to_char(ie_tipo_arquivo),' ') ||
	'07' ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_pago,0)),'.',''),',',''),14,'0') ||
	lpad(nvl(to_char(nr_documento),'0'),11,'0') ||
	lpad(nvl(to_char(dt_venc_doc,'yyyymmdd'),' '),8,' ') ||
	nvl(to_char(ie_conclusao),'0') ||
	nvl(ie_classif_cobranca_a500,'2') ||
	lpad(nvl(nr_nota_credito_debito_a500,'0'),11,'0') ||
	lpad(nvl(to_char(dt_vencimento_ndc_a500,'yyyymmdd'),' '),8,' ') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_ndc_a500,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_contest_ndc,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_total_pago_ndc,0)),'.',''),',',''),14,'0') ||
	lpad(nvl(to_char(nr_documento2),'0'),11,'0') ||
	lpad(nvl(to_char(dt_venc_doc2,'yyyymmdd'),' '),8,' '),
	nr_seq_lote_contest
into	ds_conteudo_w,
	nr_seq_lote_contest_w
from	ptu_camara_contestacao
where	nr_sequencia	= nr_seq_camara_p;

insert into w_ptu_envio_arq
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ds_conteudo)
values	(w_ptu_envio_arq_seq.nextval,
	sysdate,
	nm_usuario_p,
	ds_conteudo_w);

nr_seq_registro_w	:= nr_seq_registro_w + 1;

open C00;
loop
fetch C00 into
	ds_conteudo_aux_w,
	nr_seq_questionamento_w,
	ie_tipo_acordo_w;
exit when C00%notfound;
	begin
	ds_conteudo_w	:= lpad(nr_seq_registro_w,8,'0') || ds_conteudo_aux_w;
	
	if	(ie_tipo_arquivo_w = 1) and
		(ie_tipo_acordo_w = '11') then
		ie_motivo_ques_w := 'S';
	else
		ie_motivo_ques_w := 'N';
	end if;

	insert into w_ptu_envio_arq
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		ds_conteudo)
	values	(w_ptu_envio_arq_seq.nextval,
		sysdate,
		nm_usuario_p,
		ds_conteudo_w);

	update	ptu_questionamento
	set	nr_seq_arquivo	= nr_seq_registro_w
	where	nr_sequencia	= nr_seq_questionamento_w;

	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	qt_552_w		:= qt_552_w + 1;

	open C01;
	loop
	fetch C01 into
		ds_conteudo_553_w;
	exit when C01%notfound;
		begin
		ds_conteudo_w	:= substr(lpad(nr_seq_registro_w, 8, '0') || replace(replace(replace(ds_conteudo_553_w, '¿', ' '), chr(13), ' '), chr(10), ''), 1, 515);

		insert into w_ptu_envio_arq
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ds_conteudo)
		values	(w_ptu_envio_arq_seq.nextval,
			sysdate,
			nm_usuario_p,
			ds_conteudo_w);

		nr_seq_registro_w	:= nr_seq_registro_w + 1;
		end;
	end loop;
	close C01;
	end;
end loop;
close C00;

vl_reconhecido_w := 0;
vl_cobrado_w	 := 0;

select	sum(nvl(vl_reconhecido,0) + nvl(vl_reconh_adic_co,0) + 
	nvl(vl_reconh_adic_filme,0) + nvl(vl_reconh_adic_serv,0) + 
	nvl(vl_reconh_co,0) + nvl(vl_reconh_filme,0)) vl_reconhecido_w,
	sum(nvl(vl_cobrado,0) + nvl(vl_cobr_adic_co,0) +
	nvl(vl_cobr_adic_filme,0) + nvl(vl_cobr_adic_serv,0) +
	nvl(vl_cobr_co,0) + nvl(vl_cobr_filme,0)) vl_cobrado_w,
	sum(nvl(vl_acordo,0) + nvl(vl_acordo_co,0) +
	nvl(vl_acordo_filme,0)) vl_tot_aco_servico_w,
	sum(nvl(vl_acordo_adic_serv,0) + nvl(vl_acordo_adic_co,0) +
	nvl(vl_acordo_adic_filme,0)) vl_tot_aco_taxa_w
into	vl_reconhecido_w,
	vl_cobrado_w,
	vl_tot_aco_servico_w,
	vl_tot_aco_taxa_w
from	ptu_questionamento x
where	x.nr_seq_contestacao = nr_seq_camara_p
and	x.ie_tipo_acordo not in ('11');

if	(ie_tipo_arquivo_w not in (5,6)) then
	vl_tot_aco_servico_w := 0;
	vl_tot_aco_taxa_w := 0;
end if;			

select	lpad(nr_seq_registro_w,8,'0') ||
	'559' ||
	lpad(qt_552_w,5,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_cobrado_w,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_reconhecido_w,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_tot_aco_servico_w,0)),'.',''),',',''),14,'0') ||
	lpad(replace(replace(campo_mascara_virgula(nvl(vl_tot_aco_taxa_w,0)),'.',''),',',''),14,'0')
into	ds_conteudo_w
from	ptu_camara_contestacao
where	nr_sequencia	= nr_seq_camara_p;

insert into w_ptu_envio_arq
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	ds_conteudo)
values	(w_ptu_envio_arq_seq.nextval,
	sysdate,
	nm_usuario_p,
	ds_conteudo_w);

commit;

end ptu_gerar_envio_contest_v41;
/