create or replace
procedure sup_atualiza_alerta_gerado(
				nr_sequencia_p			number,
				ie_verificado_p			varchar2) is 

begin

update	sup_alerta_gerado
set		ie_verificado = ie_verificado_p
where	nr_sequencia = nr_sequencia_p;

commit;

end sup_atualiza_alerta_gerado;
/