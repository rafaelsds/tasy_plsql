create or replace procedure reg_activate_program_prs(
                                                     
                                                     nr_seq_requirement_p number,
                                                     
                                                     ds_reason_p varchar2,
                                                     
                                                     nm_usuario_p varchar2) is

  cursor C01 is
  
    select a.nr_seq_reg_program,
           
           b.nr_sequencia nr_seq_caso_teste
    
      from reg_program_prs a,
           
           reg_caso_teste b
    
     where a.nr_sequencia = nr_seq_requirement_p
          
       and b.nr_seq_product = a.nr_seq_product_requirement;

begin

  update reg_program_prs
  
     set ie_situacao = 'A',
         
         ds_motivo_exclusao = null,
         
         nm_usuario_exclusao = null,
         
         ds_motivo_inclusao = ds_reason_p,
         
         nm_usuario_inclusao = nm_usuario_p,
         
         nm_usuario = nm_usuario_p,
         
         dt_atualizacao = sysdate
  
   where nr_sequencia = nr_seq_requirement_p;

  commit;

  for C01_w in C01 loop
  
    reg_activate_program_cts(
                             
                             nr_seq_reg_program_p => C01_w.nr_seq_reg_program,
                             
                             nr_seq_caso_teste_p => C01_w.nr_seq_caso_teste,
                             
                             ds_reason_p => ds_reason_p,
                             
                             ie_opcao_p => 'PRS',
                             
                             nm_usuario_p => nm_usuario_p
                             
                             );
  
  end loop;

end reg_activate_program_prs;
/
