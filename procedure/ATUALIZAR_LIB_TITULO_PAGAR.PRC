create or replace
procedure ATUALIZAR_LIB_TITULO_PAGAR(	nr_titulo_p	number,
					nm_usuario_p	varchar2,
					ds_resultado_p	out varchar2) is

ie_liberar_w		varchar2(1);
ie_att_libs_w		varchar2(1);
ds_resultado_w		varchar2(1);
cd_estabelecimento_w	number(4);
nr_seq_regra_w		number(15);
nm_usuario_lib_w	varchar2(255);
vl_titulo_w		number(15,2);
ie_nivel_w		number(10);
cd_operacao_nf_w	number(4);
nr_seq_nota_fiscal_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
cd_cgc_w		varchar2(14);
cd_tipo_pessoa_w	number(3);
vl_nota_fiscal_w	number(15,2);
dt_emissao_w		date;
cd_perfil_w		number(5);
ie_tipo_titulo_w	varchar2(2);
nr_seq_classe_w		number(10);
ie_origem_titulo_w	varchar2(10);
count_lib_w		number(10);
nr_ordem_compra_w	number(10);
nm_usuario_w				titulo_pagar.nm_usuario%type;
nr_seq_proj_rec_w			titulo_pagar.nr_seq_proj_rec%type;

cursor c01 is
select	a.nr_sequencia
from	regra_lib_tit_pagar a
where	(nvl(a.ie_ordem_compra, 'N') = 'N' or nr_ordem_compra_w is not null)
and	cd_estabelecimento	= cd_estabelecimento_w
and	dt_emissao_w	between trunc(nvl(a.dt_inicio_vigencia,dt_emissao_w)) and fim_dia(nvl(a.dt_fim_vigencia,dt_emissao_w))
and	nvl(a.cd_cgc,nvl(cd_cgc_w,'0')) = nvl(cd_cgc_w,0)
and	nvl(a.cd_tipo_pj,nvl(cd_tipo_pessoa_w,0)) = nvl(cd_tipo_pessoa_w,0)
and	(nvl(a.ie_tipo_pessoa,'A') = 'A' or
	(a.ie_tipo_pessoa = 'F' and cd_pessoa_fisica_w is not null) or
	(a.ie_tipo_pessoa = 'J' and cd_cgc_w is not null))
and	((nvl(a.ie_valor_nf,'N') = 'N' and vl_titulo_w between nvl(a.vl_minimo,vl_titulo_w) and nvl(a.vl_maximo,vl_titulo_w)) or
	(a.ie_valor_nf = 'S' and nvl(vl_nota_fiscal_w,vl_titulo_w) between nvl(a.vl_minimo,nvl(vl_nota_fiscal_w,vl_titulo_w)) and nvl(a.vl_maximo,nvl(vl_nota_fiscal_w,vl_titulo_w))))
and	nvl(a.ie_liberar,'N')	= 'N'
and	nvl(cd_operacao_nf,nvl(cd_operacao_nf_w,0))		= nvl(cd_operacao_nf_w,0)
and	nvl(a.ie_tipo_titulo, nvl(ie_tipo_titulo_w,'X'))	= nvl(ie_tipo_titulo_w,'X')
and	nvl(a.nr_seq_classe, nvl(nr_seq_classe_w, 0)) 		= nvl(nr_seq_classe_w, 0)
and	nvl(a.ie_origem_titulo, nvl(ie_origem_titulo_w, 0)) 	= nvl(ie_origem_titulo_w, 0)
and	( (nvl(a.ie_exige_nota_fiscal,'N') = 'N') or 
      ( (nvl(a.ie_exige_nota_fiscal,'N') = 'S') and (nr_seq_nota_fiscal_w is not null) ) 
	);   	


cursor c02 is
select	a.nm_usuario_lib,
	a.ie_nivel
from	regra_lib_tit_usuario a
where	a.ie_nivel		is not null
and	not exists
	(select	1
	from	conta_pagar_lib x
	where	nvl(x.ie_nivel,0)	= nvl(a.ie_nivel,0)
	and	x.nm_usuario_lib		= a.nm_usuario_lib
	and	x.nr_titulo				= nr_titulo_p)
and	a.nr_seq_regra				= nr_seq_regra_w
and	a.nm_usuario_lib			is not null;

cursor	c03 is
select	a.cd_perfil,
	a.ie_nivel
from	regra_lib_tit_perfil a
where	a.ie_nivel		is not null
and	not exists
	(select	1
	from	conta_pagar_lib x
	where	nvl(x.ie_nivel,0)	= nvl(a.ie_nivel,0)
	and	x.cd_perfil				= a.cd_perfil
	and	x.nr_titulo				= nr_titulo_p)
and	a.cd_perfil					is not null
and	a.nr_seq_regra				= nr_seq_regra_w;

begin

/*Excluir os usuarios para liberacao ja gerados para o titulo que ainda nao foram liberados*/
select	count(*)
into	count_lib_w
from	conta_pagar_lib
where	nr_titulo	= nr_titulo_p
and	dt_liberacao	is not null;

if	(count_lib_w = 0) then

	delete	from conta_pagar_lib
	where	nr_titulo	= nr_titulo_p;

	select	max(a.vl_titulo),
		max(a.nr_seq_nota_fiscal),
		max(a.cd_estabelecimento),
		max(a.cd_pessoa_fisica),
		max(a.cd_cgc),
		max(b.cd_tipo_pessoa),
		nvl(max(a.dt_emissao),sysdate),
		max(a.ie_tipo_titulo),
		max(a.nr_seq_classe),
		max(a.ie_origem_titulo)
	into	vl_titulo_w,
		nr_seq_nota_fiscal_w,
		cd_estabelecimento_w,
		cd_pessoa_fisica_w,
		cd_cgc_w,
		cd_tipo_pessoa_w,
		dt_emissao_w,
		ie_tipo_titulo_w,
		nr_seq_classe_w,
		ie_origem_titulo_w
	from	pessoa_juridica b,
		titulo_pagar a
	where	a.cd_cgc		= b.cd_cgc(+)
	and	a.nr_titulo		= nr_titulo_p;


	if	(nr_seq_nota_fiscal_w is not null) then
		select	max(a.cd_operacao_nf),
			max(a.vl_total_nota),
			max(a.nr_ordem_compra)
		into	cd_operacao_nf_w,
			vl_nota_fiscal_w,
			nr_ordem_compra_w
		from	nota_fiscal a
		where	a.nr_sequencia	= nr_seq_nota_fiscal_w;
	end if;

	open c01;
	loop
	fetch c01 into
		nr_seq_regra_w;
	exit when C01%notfound;

		open c02;
		loop
		fetch c02 into
			nm_usuario_lib_w,
			ie_nivel_w;
		exit when C02%notfound;

			insert into conta_pagar_lib(
				dt_atualizacao,
				dt_atualizacao_nrec,
				dt_liberacao,
				ie_nivel,
				nm_usuario,
				nm_usuario_lib,
				nm_usuario_nrec,
				nr_seq_regra_tit_pagar,
				nr_titulo,
				nr_sequencia)
			values	(sysdate,
				sysdate,
				null,
				ie_nivel_w,
				nm_usuario_p,
				nm_usuario_lib_w,
				nm_usuario_p,
				nr_seq_regra_w,
				nr_titulo_p,
				conta_pagar_lib_seq.nextval);

		end loop;
		close c02;

		open	c03;
		loop
		fetch	c03 into
			cd_perfil_w,
			ie_nivel_w;
		exit	when c03%notfound;

			insert into conta_pagar_lib(
				cd_perfil,
				dt_atualizacao,
				dt_atualizacao_nrec,
				dt_liberacao,
				ie_nivel,
				nm_usuario,
				nm_usuario_nrec,
				nr_seq_regra_tit_pagar,
				nr_titulo,
				nr_sequencia)
			values	(cd_perfil_w,
				sysdate,
				sysdate,
				null,
				ie_nivel_w,
				nm_usuario_p,
				nm_usuario_p,
				nr_seq_regra_w,
				nr_titulo_p,
				conta_pagar_lib_seq.nextval);

		end	loop;
		close	c03;

	end loop;
	close c01;

	select 	max(cd_cgc),
			max(vl_titulo),
			max(cd_estabelecimento),
			max(nm_usuario),
			max(nr_seq_proj_rec),
			max(ie_tipo_titulo),
			max(nr_seq_classe),
			max(ie_origem_titulo),
			max(dt_emissao),
			max(nr_seq_nota_fiscal)
	into	cd_cgc_w,
			vl_titulo_w,
			cd_estabelecimento_w,
			nm_usuario_w,
			nr_seq_proj_rec_w,
			ie_tipo_titulo_w,
			nr_seq_classe_w,
			ie_origem_titulo_w,
			dt_emissao_w,
			nr_seq_nota_fiscal_w
	from 	titulo_pagar
	where 	nr_titulo = nr_titulo_p;

	obter_regra_lib_tit_pagar(nr_seq_nota_fiscal_w,
			vl_titulo_w,
			cd_cgc_w,
			cd_estabelecimento_w,
			nm_usuario_w,
			nr_seq_regra_w,
			ie_liberar_w,
			ds_resultado_w,
			dt_emissao_w,
			nr_seq_proj_rec_w,
			ie_tipo_titulo_w,
			nr_seq_classe_w,
			ie_origem_titulo_w);
			
			obter_param_usuario(851,214,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_w,ie_att_libs_w);
			ds_resultado_p := null;
			
	if	(ds_resultado_w	= 'S') and (ie_liberar_w = 'S') then
		if (ie_att_libs_w) = 'C' then
			ds_resultado_p := 'S';
		else  
			update 	titulo_pagar
			set 	dt_liberacao 	= sysdate,
				nm_usuario_lib 	= nm_usuario_w
			where 	nr_titulo 	= nr_titulo_p;
			commit;
		end if;  
	else
		if (ie_att_libs_w) = 'C' then
			ds_resultado_p := 'N'; 
		else    
			update 	titulo_pagar
			set 	dt_liberacao 	= null,
				nm_usuario_lib 	= null
			where 	nr_titulo 	= nr_titulo_p;
			commit;
		end if;
	end if;
end if;

end ATUALIZAR_LIB_TITULO_PAGAR;
/
