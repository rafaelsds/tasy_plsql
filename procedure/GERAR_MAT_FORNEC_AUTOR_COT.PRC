create or replace
procedure gerar_mat_fornec_autor_cot(nr_seq_autor_cirurgia_p	number,
					nm_usuario_p		Varchar2) is 


cd_cgc_w			pessoa_juridica.cd_cgc%type;					
nr_seq_mat_autor_cir_w		material_autor_cirurgia.nr_sequencia%type;
cd_estab_logado_w		estabelecimento.cd_estabelecimento%type;
qt_mat_autor_fornec_w		number(10,0);
cd_material_w			material.cd_material%type;
Cursor C01 is
	select	b.cd_cgc,
		a.nr_sequencia,
		a.cd_material
	from	material_autor_cirurgia a,
		autorizacao_cirurgia c,
		material_fornec b
	where	c.nr_sequencia 		= a.nr_seq_autorizacao
	and	a.cd_material 		= b.cd_material
	and	a.nr_seq_autorizacao	= nr_seq_autor_cirurgia_p
	and	nvl(c.cd_estabelecimento,cd_estab_logado_w)	= b.cd_estabelecimento
	group  by b.cd_cgc, a.nr_sequencia, a.cd_material
	order by nr_sequencia;					
					
begin

open C01;
loop
fetch C01 into	
	cd_cgc_w,
	nr_seq_mat_autor_cir_w,
	cd_material_w;
exit when C01%notfound;
	begin
	
	select	count(*)
	into	qt_mat_autor_fornec_w
	from	material_autor_cir_cot x
	where	x.cd_cgc	= cd_cgc_w
	and	x.nr_sequencia	= nr_seq_mat_autor_cir_w;
	
	if	(qt_mat_autor_fornec_w = 0) then
		
		insert	into material_autor_cir_cot (
			cd_cgc,
			cd_condicao_pagamento,
			dt_atualizacao,
			dt_atualizacao_nrec,
			ie_aprovacao,
			nm_usuario,
			nm_usuario_nrec,
			nr_orcamento,
			nr_seq_marca,
			nr_sequencia,
			vl_cotado,
			vl_unitario_cotado)
		values	(
			cd_cgc_w,
			null,
			sysdate,
			sysdate,
			'N',
			nm_usuario_p,
			nm_usuario_p,
			null,
			null,
			nr_seq_mat_autor_cir_w,
			0,
			0);	
		
	end if;
	
	
	
	end;
end loop;
close C01;


--if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_mat_fornec_autor_cot;
/
