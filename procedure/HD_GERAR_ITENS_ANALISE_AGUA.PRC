create or replace
procedure hd_gerar_itens_analise_agua	(nr_seq_protocolo_p	number,
					nr_seq_analise_agua_p	number,
					nm_usuario_p		varchar2) is

nr_seq_exame_w		number(10);
nr_seq_unid_med_w	varchar2(10);
nr_seq_metodo_w		number(10);
ie_prioridade_w		number(3);
					
cursor c01 is
	select	a.nr_seq_exame,
		nr_seq_unid_med
	from	lab_protocolo_ext_item a,
		exame_laboratorio b
	where	nr_seq_protocolo = nr_seq_protocolo_p
	and	a.nr_seq_exame = b.nr_seq_exame;
	
begin

open c01;
loop
fetch c01 into	
	nr_seq_exame_w,
	nr_seq_unid_med_w;
exit when c01%notfound;
	begin
	
	select	nvl(min(ie_prioridade),0)
	into	ie_prioridade_w
	from	exame_lab_metodo
	where 	nr_seq_exame = nr_seq_exame_w;
	
	if	(ie_prioridade_w > 0) then
	
		select	nvl(min(nr_seq_metodo),0)
		into	nr_seq_metodo_w
		from	exame_lab_metodo
		where 	nr_seq_exame = nr_seq_exame_w
		and	ie_prioridade = ie_prioridade_w;
		
	else
		select	nvl(min(nr_seq_metodo),0)
		into	nr_seq_metodo_w
		from	exame_lab_metodo
		where 	nr_seq_exame = nr_seq_exame_w;
	end if;
	
	insert into hd_analise_agua_item(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_analise_agua,
					nr_seq_exame,
					nr_seq_unidade_medida,
					nr_seq_metodo)
				values(	hd_analise_agua_item_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_analise_agua_p,
					nr_seq_exame_w,
					nr_seq_unid_med_w,
					nr_seq_metodo_w
				);
	
	
	end;
end loop;
close c01;

commit;

end hd_gerar_itens_analise_agua;
/