create or replace
procedure Calcular_Taxa_Esp_Conta
		(nr_interno_conta_p		number,
		nr_sequencia_p			number,
		ie_proc_mat_p			number,
		nm_usuario_p			varchar2) is


nr_sequencia_w			number(10,0);
nr_seq_regra_preco_w		number(10,0);
VL_SERVICO_W			number(15,4) := 0;
pr_total_w			number(15,4);

cd_area_procedimento_w		number(15,0);
cd_especialidade_w		number(15,0);
cd_grupo_proc_w			number(15,0);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);

cd_grupo_material_w		number(10,0);
cd_subgrupo_material_w		number(10,0);
cd_classe_material_w		number(10,0);
cd_material_w			number(10,0);


cd_area_procedimento_ww		number(15,0);
cd_especialidade_ww		number(15,0);
cd_grupo_proc_ww		number(15,0);
cd_procedimento_ww		number(15,0);
ie_origem_proced_ww		number(10,0);

cd_grupo_material_ww		number(10,0);
cd_subgrupo_material_ww		number(10,0);
cd_classe_material_ww		number(10,0);
cd_material_ww			number(10,0);

nr_seq_regra_taxa_w		number(10,0);

vl_item_w			number(15,4);
vl_ajuste_w			number(15,4);
nr_atendimento_w		number(10,0);
nr_seq_item_w			number(10,0);
nr_seq_proc_pacote_w		number(10,0);
nr_seq_proc_interno_w		number(10,0);
nr_seq_mat_proc_w		number(10,0);
ie_proc_mat_w			number(3);
cd_motivo_exc_conta_w		number(10,0);
ie_pacote_w			varchar2(1);
nr_seq_pacote_w			number(10,0);
ie_consignado_w			varchar2(1);
ie_consignado_ww		varchar2(1);

cd_estabelecimento_w		number(4,0);
ie_responsavel_credito_w	varchar2(5);
vl_limite_w			regra_preco_proc_crit.vl_limite%type;
cd_convenio_w			regra_preco_proc_crit.cd_convenio%type;

cursor	c01 is
	select  a.nr_sequencia,
		a.nr_seq_regra_preco,
		a.cd_convenio
	from  	procedimento_paciente a
	where 	a.nr_interno_conta    	= nr_interno_conta_p
	and	a.nr_seq_regra_preco	is not null;


Cursor c09 is
select 	pr_total,
	nvl(cd_area_procedimento,0) cd_area_procedimento,
	nvl(cd_especialidade, 0) cd_especialidade,
	nvl(cd_grupo_proc, 0) cd_grupo_proc,
	nvl(cd_procedimento, 0) cd_procedimento,
	nvl(ie_origem_proced, 0) ie_origem_proced,
	nvl(cd_grupo_material, 0) cd_grupo_material,
	nvl(cd_subgrupo_material, 0) cd_subgrupo_material,
	nvl(cd_classe_material, 0) cd_classe_material,
	nvl(cd_material, 0) cd_material,
	nvl(ie_consignado, '2') ie_consignado,
	nvl(vl_limite,0) vl_limite
from  	regra_preco_proc_crit a
where 	nr_seq_regra 						= nr_seq_regra_preco_w
and	nvl(cd_area_procedimento, cd_area_procedimento_w)	= cd_area_procedimento_w
and	nvl(cd_especialidade, cd_especialidade_w)		= cd_especialidade_w
and	nvl(cd_grupo_proc, cd_grupo_proc_w)			= cd_grupo_proc_w
and	nvl(cd_area_procedimento, cd_area_procedimento_w)	= cd_area_procedimento_w
and	nvl(cd_procedimento, cd_procedimento_w)			= cd_procedimento_w
and 	nvl(vl_item_w,0) between nvl(a.vl_inicial, nvl(vl_item_w,0)) and nvl(a.vl_final, nvl(vl_item_w,0))
and	nvl(ie_origem_proced, ie_origem_proced_w)		= ie_origem_proced_w
	and	(((nvl(a.ie_consignado,'2')	= '0') and (ie_consignado_w = '0')) or
		((nvl(a.ie_consignado,'2') 	= '1') and (ie_consignado_w = '1')) or
		((nvl(a.ie_consignado,'2') 	= '3') and (ie_consignado_w = '2')) or	  --Criada nova opcao OS821589		
		(nvl(a.ie_consignado,'2')	= '2'))
and	nvl(nr_seq_proc_interno, nvl(nr_seq_proc_interno_w,0))		= nvl(nr_seq_proc_interno_w,0)
and	(nvl(cd_area_procedimento,0) +
		nvl(cd_especialidade,0) +
		nvl(cd_grupo_proc,0) +
		nvl(cd_procedimento,0) + 
		nvl(nr_seq_proc_interno,0))				<> 0
and	cd_procedimento_w	is not null
and	nvl(cd_estabelecimento, nvl(cd_estabelecimento_w,0)) = nvl(cd_estabelecimento_w,0)
and 	nvl(a.ie_responsavel_credito, nvl(ie_responsavel_credito_w,'0')) = nvl(ie_responsavel_credito_w,'0')
and	((a.ie_pacote		= 'A') or
	 ((a.ie_pacote		= 'S') and (nr_seq_proc_pacote_w is not null)) or
	 ((a.ie_pacote		= 'N') and (nr_seq_proc_pacote_w is null)))
and	vl_item_w <> 0
and	nvl(a.cd_convenio, nvl(cd_convenio_w, 0)) = nvl(cd_convenio_w, 0)
union
select 	pr_total,
	nvl(cd_area_procedimento,0) cd_area_procedimento,
	nvl(cd_especialidade, 0) cd_especialidade,
	nvl(cd_grupo_proc, 0) cd_grupo_proc,
	nvl(cd_procedimento, 0) cd_procedimento,
	nvl(ie_origem_proced, 0) ie_origem_proced,
	nvl(cd_grupo_material, 0) cd_grupo_material,
	nvl(cd_subgrupo_material, 0) cd_subgrupo_material,
	nvl(cd_classe_material, 0) cd_classe_material,
	nvl(cd_material, 0) cd_material,
	nvl(ie_consignado, '2') ie_consignado,
	nvl(vl_limite,0) vl_limite
from  	regra_preco_proc_crit a
where 	nr_seq_regra 						= nr_seq_regra_preco_w
and	nvl(cd_grupo_material, cd_grupo_material_w)		= cd_grupo_material_w
and	nvl(cd_subgrupo_material, cd_subgrupo_material_w)	= cd_subgrupo_material_w
and	nvl(cd_classe_material, cd_classe_material_w)		= cd_classe_material_w
and 	nvl(cd_material, cd_material_w)				= cd_material_w
and 	nvl(vl_item_w,0) between nvl(a.vl_inicial, nvl(vl_item_w,0)) and nvl(a.vl_final, nvl(vl_item_w,0))
and	(((nvl(a.ie_consignado,'2')	= '0') and (ie_consignado_w = '0')) or
		((nvl(a.ie_consignado,'2') 	= '1') and (ie_consignado_w = '1')) or
		((nvl(a.ie_consignado,'3') 	= '1') and (ie_consignado_w = '2')) or		 --Criada nova opcao OS821589	
		(nvl(a.ie_consignado,'2')	= '2'))
and	nvl(cd_grupo_material,0) +
		nvl(cd_subgrupo_material,0) +
		nvl(cd_classe_material,0) +
		nvl(cd_material,0)				<> 0
and	cd_material_w		is not null
and	nvl(cd_estabelecimento, nvl(cd_estabelecimento_w,0)) = nvl(cd_estabelecimento_w,0)
and 	nvl(a.ie_responsavel_credito, nvl(ie_responsavel_credito_w,'0')) = nvl(ie_responsavel_credito_w,'0')
and	((a.ie_pacote		= 'A') or
	 ((a.ie_pacote		= 'S') and (nr_seq_proc_pacote_w is not null)) or
	 ((a.ie_pacote		= 'N') and (nr_seq_proc_pacote_w is null)))
and	vl_item_w <> 0
and	nvl(a.cd_convenio, nvl(cd_convenio_w, 0)) = nvl(cd_convenio_w, 0)
order by cd_area_procedimento,
		cd_especialidade,
		cd_grupo_proc,
		cd_procedimento,
		ie_origem_proced,
		cd_grupo_material,
		cd_subgrupo_material,
		cd_classe_material,
		cd_material,
		ie_consignado;
		
Cursor C04 is
	select	nr_seq_mat_proc,
		ie_proc_mat
	from	taxa_resumo_conta
	where	nr_atendimento = nr_atendimento_w
	order by 2,1;
	
Cursor C05 is
	select	nr_seq_mat_proc,
		ie_proc_mat
	from	taxa_resumo_conta
	where	nr_atendimento = nr_atendimento_w
	and 	ie_pacote_w = 'N'
	order by 2,1;


begin

if	(ie_proc_mat_p	= 1) then
	select	max(b.cd_area_procedimento),
		max(b.cd_especialidade),
		max(b.cd_grupo_proc),
		max(b.cd_procedimento),
		max(b.ie_origem_proced),
		sum(a.vl_procedimento),
		max(nr_atendimento),
		max(nr_sequencia),
		max(nr_seq_proc_pacote),
		max(nr_seq_proc_interno),
		max(ie_responsavel_credito)
	into	cd_area_procedimento_w,
		cd_especialidade_w,
		cd_grupo_proc_w,
		cd_procedimento_w,
		ie_origem_proced_w,
		vl_item_w,
		nr_atendimento_w,
		nr_seq_item_w,
		nr_seq_proc_pacote_w,
		nr_seq_proc_interno_w,
		ie_responsavel_credito_w
	from	estrutura_procedimento_v b,
		procedimento_paciente a
	where	a.cd_procedimento	= b.cd_procedimento
	and	a.ie_origem_proced	= b.ie_origem_proced
	and	a.nr_sequencia		= nr_sequencia_p
	and 	a.nr_interno_conta	= nr_interno_conta_p
	and 	a.cd_motivo_exc_conta is null;
else
	select	max(b.cd_grupo_material),
		max(b.cd_subgrupo_material),
		max(b.cd_classe_material),
		max(b.cd_material),
		sum(a.vl_material),
		max(nr_atendimento),
		max(nr_sequencia),
		max(nr_seq_proc_pacote),
		nvl(max(ie_consignado),2),
		max(ie_responsavel_credito)
	into	cd_grupo_material_w,
		cd_subgrupo_material_w,
		cd_classe_material_w,
		cd_material_w,
		vl_item_w,
		nr_atendimento_w,
		nr_seq_item_w,
		nr_seq_proc_pacote_w,
		ie_consignado_w,
		ie_responsavel_credito_w
	from	estrutura_material_v b,
		material_atend_paciente a
	where	a.cd_material		= b.cd_material
	and	a.nr_sequencia		= nr_sequencia_p
	and 	a.nr_interno_conta 	= nr_interno_conta_p
	and 	a.cd_motivo_exc_conta is null;
end if;

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	conta_paciente
where	nr_interno_conta = nr_interno_conta_p;

open C04;
loop
fetch C04 into	
	nr_seq_mat_proc_w,
	ie_proc_mat_w;
exit when C04%notfound;
	begin
	
	if	(ie_proc_mat_w = 1) then
		--Procedimento
		begin
		select 	cd_motivo_exc_conta
		into	cd_motivo_exc_conta_w
		from 	procedimento_paciente
		where 	nr_sequencia = nr_seq_mat_proc_w;
		exception
			when others then
			delete	from TAXA_RESUMO_CONTA
			where	nr_atendimento	= nr_atendimento_w
			and	nr_seq_mat_proc	= nr_seq_mat_proc_w;
		end;
		
		if  	(cd_motivo_exc_conta_w is not null) then
			delete	from TAXA_RESUMO_CONTA
			where	nr_atendimento	= nr_atendimento_w
			and	nr_seq_mat_proc	= nr_seq_mat_proc_w;
		end if;		
	else
		--Material
		begin
		select 	cd_motivo_exc_conta
		into	cd_motivo_exc_conta_w
		from 	material_atend_paciente
		where 	nr_sequencia = nr_seq_mat_proc_w;
		exception
			when others then
			delete	from TAXA_RESUMO_CONTA
			where	nr_atendimento	= nr_atendimento_w
			and	nr_seq_mat_proc	= nr_seq_mat_proc_w;
		end;
		
		if  	(cd_motivo_exc_conta_w is not null) then
			delete	from TAXA_RESUMO_CONTA
			where	nr_atendimento	= nr_atendimento_w
			and	nr_seq_mat_proc	= nr_seq_mat_proc_w;
		end if;
	end if;
	
	end;	
end loop;
close C04;

open	c01;
loop
fetch	c01 into 
	nr_sequencia_w,
	nr_seq_regra_preco_w,
	cd_convenio_w;
	begin
	exit when C01%NOTFOUND;	
	
	
	--Quando a regra for "FORA PACOTE" Apagar todos os registros que est�o na tabela TAXA_RESUMO_CONTA e que est�o no pacote
	select 	nvl(max(ie_pacote),'A')
	into	ie_pacote_w
	from  	regra_preco_proc_crit
	where 	nr_seq_regra = nr_seq_regra_preco_w;
	
	-- Procedimento
	open C05;
	loop
	fetch C05 into	
		nr_seq_mat_proc_w,
		ie_proc_mat_w;
	exit when C05%notfound;
		begin

		if	(ie_proc_mat_w = 1) then
			
			--Procedimento
			select 	max(nr_seq_proc_pacote)
			into	nr_seq_pacote_w
			from 	procedimento_paciente
			where 	nr_sequencia = nr_seq_mat_proc_w;

		
			if  	(nr_seq_pacote_w is not null) then
				delete	from TAXA_RESUMO_CONTA
				where	nr_atendimento	= nr_atendimento_w
				and	nr_seq_mat_proc	= nr_seq_mat_proc_w;
			end if;		
		else
			--Material
			select 	max(nr_seq_proc_pacote)
			into	nr_seq_pacote_w
			from 	material_atend_paciente
			where 	nr_sequencia = nr_seq_mat_proc_w;
					
			if  	(nr_seq_pacote_w is not null) then
				delete	from TAXA_RESUMO_CONTA
				where	nr_atendimento	= nr_atendimento_w
				and	nr_seq_mat_proc	= nr_seq_mat_proc_w;
			end if;
		end if;
		
		end;
	end loop;
	close C05;
	
		
	if	(nvl(nr_seq_regra_preco_w,0) <> 0) and
		(nr_seq_item_w <> nr_sequencia_w) then
		begin		
		
		delete	from TAXA_RESUMO_CONTA
		where	nr_seq_servico	= nr_sequencia_w
		and	nr_seq_mat_proc	= nr_sequencia_p;

		pr_total_w:= null;
		
		open	c09;
		loop
		fetch	c09 into
			pr_total_w,
			cd_area_procedimento_w,
			cd_especialidade_ww,
			cd_grupo_proc_ww,
			cd_procedimento_ww,
			ie_origem_proced_ww,
			cd_grupo_material_ww,
			cd_subgrupo_material_ww,
			cd_classe_material_ww,
			cd_material_ww,
			ie_consignado_ww,
			vl_limite_w;
		exit 	when c09%notfound;
			begin
		
			pr_total_w	:= pr_total_w;
			vl_limite_w	:= vl_limite_w;

			end;
		end loop;
		close c09;		

		if 	(pr_total_w is not null) then

			select	taxa_resumo_conta_seq.nextval
			into	nr_seq_regra_taxa_w
			from	dual;

			vl_ajuste_w	:=  (vl_item_w * pr_total_w / 100);

			if	(nvl(vl_limite_w,0) > 0) and
				(vl_ajuste_w > vl_limite_w) then
				vl_ajuste_w := vl_limite_w;
			end if;

			insert	into TAXA_RESUMO_CONTA
				(NR_SEQUENCIA,           
				NR_atendimento,
				NR_SEQ_MAT_PROC,
				DT_ATUALIZACAO,
				NM_USUARIO,
				DT_ATUALIZACAO_NREC,
				NM_USUARIO_NREC,
				NR_SEQ_REGRA,
				PR_TAXA,
				IE_PROC_MAT,
				vl_item,
				vl_ajuste,	
				nr_seq_servico)
			values
				(nr_seq_regra_taxa_w,
				NR_atendimento_w,
				nr_sequencia_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_regra_preco_w,
				pr_total_w,
				ie_proc_mat_p,
				vl_item_w,
				vl_ajuste_w,
				nr_sequencia_w);
		end if;

		end;	

	select	nvl(sum(vl_ajuste),0)
	into	vl_servico_w
	from	TAXA_RESUMO_CONTA
	where	nr_seq_servico		= nr_sequencia_w
	and 	nr_atendimento		= nr_atendimento_w;
	
	begin
	update	procedimento_paciente
	set	vl_procedimento		= vl_servico_w
	where	nr_sequencia		= nr_sequencia_w;
	exception
		when others then	
			vl_servico_w	:= vl_servico_w;
	end;
			
	end if; 
	end;
end loop;
close c01;

end Calcular_Taxa_Esp_Conta;
/