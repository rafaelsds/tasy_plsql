create or replace
procedure obter_horas_conv_onc(		cd_convenio_p		number,
					nr_seq_paciente_p	number,
					cd_estabelecimento_p	number,
					qt_horas_p		out Number,
					cd_convenio_retorno_p	out Number) is
					
Qt_horas_w		Number(10,5) := 0;
cd_convenio_w		Number(5);
cd_convenio_retorno_w	NUmber(5) := 0;
					
begin
begin

	if	(cd_convenio_p = 0) or
		(cd_convenio_p is null) then
		
		select	max(cd_convenio)
		into	cd_convenio_w
		from	paciente_setor_convenio
		where	nr_seq_paciente = nr_seq_paciente_p;

		cd_convenio_retorno_w := cd_convenio_w;	
		
	else
		cd_convenio_w := cd_convenio_p;
		
	end if;	
	
	if	(cd_convenio_w > 0) then
	
		select 	(nvl(QT_HORAS_INICIO_CICLO,0) / 24) 
		into	Qt_horas_w
		from 	convenio_estabelecimento 
		where 	cd_convenio = cd_convenio_w
		and 	cd_estabelecimento = cd_estabelecimento_p;
		
	end if;

exception
	when others then
	Qt_horas_w := 0;
	cd_convenio_retorno_w := 0;
end;

cd_convenio_retorno_p := cd_convenio_retorno_w;
qt_horas_p := Qt_horas_w;

end obter_horas_conv_onc;
/