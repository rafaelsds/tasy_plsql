create or replace
procedure mprev_gerar_captacao_indicacao( nr_seq_indicacao_p    number,
                      nm_usuario_p        varchar2,
                      nr_seq_captacao_p  out  number) is
      
nr_seq_captacao_w  number(10);
cd_pessoa_fisica_w  varchar(10);
nr_seq_forma_ingresso_w  mprev_forma_ingresso.nr_sequencia%type;
ie_origem_capt_w  mprev_forma_ingresso.ie_origem%type;
cd_estabelecimento_origem_w  mprev_indicacao_paciente.CD_ESTABELECIMENTO_ORIGEM%type;

begin

begin  
  select  x.nr_sequencia
  into  nr_seq_captacao_w
  from  mprev_captacao x
  where  x.nr_seq_indicacao = nr_seq_indicacao_p;
exception  
  when  no_data_found then
    nr_seq_captacao_w := null;
end;
  
if  (nr_seq_captacao_w is null) then

  select  max(a.cd_pessoa_fisica),
      max(a.nr_seq_forma_ingresso),
      max(a.cd_estabelecimento_origem)    
  into  cd_pessoa_fisica_w,
      nr_seq_forma_ingresso_w,
      cd_estabelecimento_origem_w
  from  mprev_indicacao_paciente a
  where  a.nr_sequencia = nr_seq_indicacao_p;
  
  select   nvl(max(ie_origem),'OU') -- OU "Outros".
  into  ie_origem_capt_w
  from    MPREV_FORMA_INGRESSO
  where  nr_sequencia = nr_seq_forma_ingresso_w;
  
  select  mprev_captacao_seq.nextval
  into  nr_seq_captacao_w
  from  dual;
  
  insert  into mprev_captacao(nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec, 
                cd_pessoa_fisica, nr_seq_indicacao, ie_status, 
                ie_origem, cd_estabelecimento)
               values(nr_seq_captacao_w, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
                cd_pessoa_fisica_w, nr_seq_indicacao_p, 'N', 
                ie_origem_capt_w, cd_estabelecimento_origem_w);
  commit;
  
end if;

nr_seq_captacao_p := nr_seq_captacao_w;

end mprev_gerar_captacao_indicacao;
/
