create or replace
procedure pls_obter_base_calc_mens
			(	nr_seq_mensalidade_p	in	number,
				nr_seq_item_p		in	number,
				cd_tributo_p		in	number,
				ie_tipo_tributo_p	in	varchar2,
				vl_base_p		out	number) is

cd_cgc_contr_w			pls_contrato.cd_cgc_estipulante%type;
ie_ato_cooperado_w		pls_regra_base_trib_mens.ie_ato_cooperado%type		:= 'N';
ie_ato_auxiliar_w		pls_regra_base_trib_mens.ie_ato_auxiliar%type		:= 'N';
ie_ato_nao_cooperado_w		pls_regra_base_trib_mens.ie_ato_nao_cooperado%type	:= 'N';
ie_incide_base_w		pls_regra_base_trib_mens.ie_incide_base%type		:= 'N';
ie_contas_intercambio_w		pls_regra_base_trib_mens.ie_contas_intercambio%type;
ie_rede_propria_w		pls_regra_base_trib_mens.ie_rede_propria%type;
ie_tipo_pessoa_prestador_w	pls_regra_base_trib_mens.ie_tipo_pessoa_prestador%type;
vl_item_w			number(15,2);
vl_proc_cooperado_w		number(15,2);
vl_item_base_w			number(15,2)	:= 0;
ie_tipo_item_w			pls_mensalidade_seg_item.ie_tipo_item%type;
vl_ato_cooperado_w		pls_mensalidade_seg_item.vl_ato_cooperado%type		:= 0;
vl_ato_auxiliar_w		pls_mensalidade_seg_item.vl_ato_auxiliar%type		:= 0;
vl_ato_nao_cooperado_w		pls_mensalidade_seg_item.vl_ato_nao_cooperado%type	:= 0;
nr_seq_segurado_w		pls_segurado.nr_sequencia%type;
nr_seq_mensalidade_seg_w	pls_mensalidade_segurado.nr_sequencia%type;
qt_ato_cooperado_w		pls_integer;
nr_seq_tipo_lanc_w		pls_tipo_lanc_adic.nr_sequencia%type;
vl_contas_intercambio_mat_w	number(15,2);
vl_contas_intercambio_proc_w	number(15,2);
vl_ato_cooperado_ww		pls_mensalidade_seg_item.vl_ato_cooperado%type		:= 0;
vl_ato_auxiliar_ww		pls_mensalidade_seg_item.vl_ato_auxiliar%type		:= 0;
vl_ato_nao_cooperado_ww		pls_mensalidade_seg_item.vl_ato_nao_cooperado%type	:= 0;

Cursor C01 is
	select	sum(c.vl_item) vl_item,
		c.ie_tipo_item,
		c.nr_seq_tipo_lanc
	from	pls_mensalidade_seg_item	c,
		pls_mensalidade_segurado	b,
		pls_mensalidade			a
	where	b.nr_sequencia	= c.nr_seq_mensalidade_seg
	and	a.nr_sequencia	= b.nr_seq_mensalidade
	and	a.nr_sequencia	= nr_seq_mensalidade_p
	group by
		c.ie_tipo_item,
		c.nr_seq_tipo_lanc;

Cursor C02 is
	select	sum(c.vl_item) vl_item,
		sum(c.vl_ato_cooperado) vl_ato_cooperado,
		sum(vl_ato_auxiliar) vl_ato_auxiliar,
		sum(vl_ato_nao_cooperado) vl_ato_nao_cooperado,
		c.ie_tipo_item,
		b.nr_seq_segurado,
		b.nr_sequencia nr_seq_mensalidade_seg,
		c.nr_seq_tipo_lanc
	from	pls_mensalidade_seg_item	c,
		pls_mensalidade_segurado	b,
		pls_mensalidade			a
	where	b.nr_sequencia	= c.nr_seq_mensalidade_seg
	and	a.nr_sequencia	= b.nr_seq_mensalidade
	and	a.nr_sequencia	= nr_seq_mensalidade_p
	group by
		c.ie_tipo_item,
		b.nr_seq_segurado,
		b.nr_sequencia,
		c.nr_seq_tipo_lanc;
		
		/*
			ie_reg_nota_servico nao e utilizado como restricao da regra e sim para determinar se busca ou nao valores de nota de servico. Como nao restringia, nao dava 
			para determinar que teria sucesso na busca por valor da nota de servico, pois tanto uma regra para prestador cooperado e regra para nao cooperado podem ser
			validas, resultando em insucesso
		*/
Cursor C03 (	ie_tipo_item_pc		pls_mensalidade_seg_item.ie_tipo_item%type,
		nr_seq_tipo_lanc_pc	pls_tipo_lanc_adic.nr_sequencia%type) is
	select	ie_incide_base,
		nvl(ie_ato_cooperado,'N') ie_ato_cooperado,
		nvl(ie_ato_auxiliar,'N') ie_ato_auxiliar,
		nvl(ie_ato_nao_cooperado,'N') ie_ato_nao_cooperado,
		nvl(ie_contas_intercambio,'N') ie_contas_intercambio,
		nvl(ie_rede_propria,'N') ie_rede_propria,
		nvl(ie_tipo_pessoa_prestador,'F') ie_tipo_pessoa_prestador,
		( 	select	decode(count(1),0,'N','S')
			from	ptu_nota_servico		x,
					pls_mensalidade_seg_item	a,
					pls_conta_proc			b,
					pls_conta			c,
					pls_protocolo_conta		d
			where	b.nr_sequencia		= x.nr_seq_conta_proc
			and		c.nr_sequencia		= a.nr_seq_conta
			and		b.nr_seq_conta		= c.nr_sequencia
			and		c.nr_seq_protocolo	= d.nr_sequencia
			and		d.ie_tipo_protocolo	= 'I'
			and		x.ie_rede_propria	= nvl(z.ie_rede_propria,'N')
			and 	nvl(z.ie_contas_intercambio,'N') = 'S' 
			and		((z.ie_tipo_pessoa_prestador = 'A') or 
					(x.ie_tipo_pessoa_prestador = z.ie_tipo_pessoa_prestador))
			and		 (((b.ie_ato_cooperado = '1') and (z.ie_ato_cooperado = 'S')) or
					((b.ie_ato_cooperado = '2') and (z.ie_ato_auxiliar = 'S')) or
					((b.ie_ato_cooperado = '3') and (z.ie_ato_nao_cooperado = 'S')))
			and		a.nr_sequencia		= nr_seq_item_p) ie_registros_nota_servico	
	from	pls_regra_base_trib_mens z
	where	cd_tributo	= cd_tributo_p
	and	((ie_tipo_item_mens = ie_tipo_item_pc)	or (ie_tipo_item_mens is null))
	and	((nr_seq_tipo_lanc = nr_seq_tipo_lanc_pc) or (nr_seq_tipo_lanc is null))
	order by
		ie_registros_nota_servico,
		nvl(ie_tipo_item_mens,' '),
		nvl(nr_seq_tipo_lanc,0),
		ie_incide_base,
		ie_ato_cooperado;

begin
vl_item_base_w		:= 0;
qt_ato_cooperado_w	:= 0;

select	count(1)
into	qt_ato_cooperado_w
from	pls_regra_base_trib_mens
where	cd_tributo		= cd_tributo_p
and	((ie_ato_cooperado = 'S') or (ie_ato_auxiliar = 'S') or (ie_ato_nao_cooperado = 'S'));


if	(nr_seq_item_p is null) then
	if	(qt_ato_cooperado_w > 0) then
		
		for r_c02_w in C02 loop
			vl_item_w			:= r_c02_w.vl_item;
			vl_ato_cooperado_w		:= r_c02_w.vl_ato_cooperado;
			vl_ato_auxiliar_w		:= r_c02_w.vl_ato_auxiliar;
			vl_ato_nao_cooperado_w		:= r_c02_w.vl_ato_nao_cooperado;
			ie_tipo_item_w			:= r_c02_w.ie_tipo_item;
			nr_seq_segurado_w		:= r_c02_w.nr_seq_segurado;
			nr_seq_mensalidade_seg_w	:= r_c02_w.nr_seq_mensalidade_seg;
			nr_seq_tipo_lanc_w		:= r_c02_w.nr_seq_tipo_lanc;
			
			ie_incide_base_w	:= 'N';
			
			for r_c03_w in C03(ie_tipo_item_w, nr_seq_tipo_lanc_w) loop
				ie_incide_base_w	:= r_c03_w.ie_incide_base;
				ie_ato_cooperado_w	:= r_c03_w.ie_ato_cooperado;
				ie_ato_auxiliar_w	:= r_c03_w.ie_ato_auxiliar;
				ie_ato_nao_cooperado_w	:= r_c03_w.ie_ato_nao_cooperado;
				ie_contas_intercambio_w	:= r_c03_w.ie_contas_intercambio;
				ie_rede_propria_w	:= r_c03_w.ie_rede_propria;
				ie_tipo_pessoa_prestador_w := r_c03_w.ie_tipo_pessoa_prestador;
			end loop;
			
			if	(ie_ato_cooperado_w = 'S') or
				(ie_ato_auxiliar_w = 'S') or
				(ie_ato_nao_cooperado_w = 'S') then
				select	max(a.cd_cgc_estipulante)
				into	cd_cgc_contr_w
				from	pls_segurado	b,
					pls_contrato	a
				where	b.nr_seq_contrato	= a.nr_sequencia
				and	b.nr_sequencia		= nr_seq_segurado_w;

				if	(cd_cgc_contr_w is not null) then
					if	(ie_tipo_item_w = '3') then /*Coparticipacao */					
						if	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'A') then
							select	sum(t.vl_ato_auxiliar),
								sum(t.vl_ato_cooperado),
								sum(t.vl_ato_nao_cooperado)
							into	vl_ato_auxiliar_ww,
								vl_ato_cooperado_ww,
								vl_ato_nao_cooperado_ww
							from	pls_conta_copartic_contab	t,
								pls_conta_coparticipacao	p,
								pls_conta			e,
								pls_mensalidade_seg_item	d,
								pls_mensalidade_segurado	c,
								pls_protocolo_conta		pc
							where	t.nr_seq_conta_copartic		= p.nr_sequencia
							and	p.nr_seq_mensalidade_seg	= c.nr_sequencia
							and	e.nr_sequencia			= d.nr_seq_conta
							and	c.nr_sequencia			= d.nr_seq_mensalidade_seg
							and	e.nr_sequencia 			= p.nr_seq_conta
							and	pc.nr_sequencia 		= e.nr_seq_protocolo
							and	pc.ie_tipo_protocolo 		<> 'I'
							and	nvl(p.ie_status_coparticipacao,'S')  = 'S'
							and	c.nr_sequencia			= nr_seq_mensalidade_seg_w;

							vl_proc_cooperado_w := 0;

							if	(ie_ato_auxiliar_w = 'S') then
								vl_proc_cooperado_w	:= vl_ato_auxiliar_ww;
							end if;

							if	(ie_ato_cooperado_w = 'S') then
								vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_cooperado_ww;
							end if;

							if	(ie_ato_nao_cooperado_w = 'S') then
								vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_nao_cooperado_ww;
							end if;
						elsif	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'F') then
							select	sum(t.vl_ato_auxiliar),
								sum(t.vl_ato_cooperado),
								sum(t.vl_ato_nao_cooperado)
							into	vl_ato_auxiliar_ww,
								vl_ato_cooperado_ww,
								vl_ato_nao_cooperado_ww
							from	pls_conta_copartic_contab	t,
								pls_conta_coparticipacao	p,
								--pls_cooperado			i,
								pls_prestador			h,
								pls_conta			e,
								pls_mensalidade_seg_item	d,
								pls_mensalidade_segurado	c,
								pls_protocolo_conta		pc
							where	t.nr_seq_conta_copartic		= p.nr_sequencia
							and	p.nr_seq_mensalidade_seg	= c.nr_sequencia
							--and	h.cd_pessoa_fisica		= i.cd_pessoa_fisica
							and	pc.nr_sequencia 		= e.nr_seq_protocolo
							and	pc.ie_tipo_protocolo 		<> 'I'
							and	exists (select	1
									from	pls_cooperado x
									where	x.cd_pessoa_fisica = h.cd_pessoa_fisica)
							and	e.nr_sequencia			= d.nr_seq_conta
							and	c.nr_sequencia			= d.nr_seq_mensalidade_seg
							and	t.nr_seq_prestador_pgto 	= h.nr_sequencia
							and	e.nr_sequencia			= p.nr_seq_conta
							and 	h.cd_pessoa_fisica is not null
							and	nvl(p.ie_status_coparticipacao,'S')  = 'S'
							and	c.nr_sequencia		= nr_seq_mensalidade_seg_w;
							
							vl_proc_cooperado_w := 0;

							if	(ie_ato_auxiliar_w = 'S') then
								vl_proc_cooperado_w	:= vl_ato_auxiliar_ww;
							end if;

							if	(ie_ato_cooperado_w = 'S') then
								vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_cooperado_ww;
							end if;

							if	(ie_ato_nao_cooperado_w = 'S') then
								vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_nao_cooperado_ww;
							end if;
						elsif	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'J') then						
							select	sum(t.vl_ato_auxiliar),
								sum(t.vl_ato_cooperado),
								sum(t.vl_ato_nao_cooperado)
							into	vl_ato_auxiliar_ww,
								vl_ato_cooperado_ww,
								vl_ato_nao_cooperado_ww
							from	pls_conta_copartic_contab	t,
								pls_conta_coparticipacao	p,
								--pls_cooperado			i,
								pls_prestador			h,
								pls_conta			e,
								pls_mensalidade_seg_item	d,
								pls_mensalidade_segurado	c,
								pls_protocolo_conta		pc
							where	t.nr_seq_conta_copartic		= p.nr_sequencia
							and	p.nr_seq_mensalidade_seg	= c.nr_sequencia
							--and	h.cd_cgc			= i.cd_cgc
							and	pc.nr_sequencia 		= e.nr_seq_protocolo
							and	pc.ie_tipo_protocolo 		<> 'I'
							and	exists (select	1
									from	pls_cooperado x
									where	x.cd_cgc = h.cd_cgc)
							and	e.nr_sequencia			= d.nr_seq_conta
							and	c.nr_sequencia			= d.nr_seq_mensalidade_seg
							and	t.nr_seq_prestador_pgto		= h.nr_sequencia
							and	e.nr_sequencia			= p.nr_seq_conta
							and 	h.cd_cgc is not null
							and	nvl(p.ie_status_coparticipacao,'S')  = 'S'
							and	c.nr_sequencia		= nr_seq_mensalidade_seg_w;
							
							vl_proc_cooperado_w := 0;

							if	(ie_ato_auxiliar_w = 'S') then
								vl_proc_cooperado_w	:= vl_ato_auxiliar_ww;
							end if;

							if	(ie_ato_cooperado_w = 'S') then
								vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_cooperado_ww;
							end if;

							if	(ie_ato_nao_cooperado_w = 'S') then
								vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_nao_cooperado_ww;
							end if;
						end if;	
			
						if	(ie_contas_intercambio_w = 'S') then
							select	nvl(sum(z.vl_item),0) vl_coparticipacao
							into	vl_contas_intercambio_mat_w
							from	ptu_nota_servico		x,
								pls_mensalidade_seg_item	a,
								pls_conta_mat			c,
								pls_conta			d,
								pls_protocolo_conta		e,
								pls_conta_coparticipacao 	y,
								pls_mensalidade_item_conta 	z
							where	c.nr_sequencia			= x.nr_seq_conta_mat
							and	d.nr_sequencia			= a.nr_seq_conta
							and	c.nr_seq_conta			= d.nr_sequencia
							and	d.nr_seq_protocolo		= e.nr_sequencia
							and  	y.nr_sequencia 			= z.nr_seq_conta_copartic
							and  	a.nr_sequencia 			= z.nr_seq_item
							and 	y.nr_seq_conta_mat 		= c.nr_sequencia
							and	e.ie_tipo_protocolo		= 'I'
							and	((x.ie_rede_propria		= ie_rede_propria_w) or ('A' = ie_rede_propria_w))
							and	((ie_tipo_pessoa_prestador_w 	= 'A') or 
								(x.ie_tipo_pessoa_prestador 	= ie_tipo_pessoa_prestador_w))
							and	(((c.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
								 ((c.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
								 ((c.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
							and	a.nr_seq_mensalidade_seg	= nr_seq_mensalidade_seg_w;
						
							select	nvl(sum(z.vl_item),0) vl_coparticipacao
							into	vl_contas_intercambio_proc_w
							from	ptu_nota_servico		x,
								pls_mensalidade_seg_item	a,
								pls_conta_proc			c,
								pls_conta			d,
								pls_protocolo_conta		e,
								pls_conta_coparticipacao 	y,
								pls_mensalidade_item_conta 	z
							where	c.nr_sequencia			= x.nr_seq_conta_proc
							and	d.nr_sequencia			= a.nr_seq_conta
							and	c.nr_seq_conta			= d.nr_sequencia
							and	d.nr_seq_protocolo		= e.nr_sequencia
							and  	y.nr_sequencia 			= z.nr_seq_conta_copartic
							and  	a.nr_sequencia 			= z.nr_seq_item
							and 	y.nr_seq_conta_proc 		= c.nr_sequencia
							and	e.ie_tipo_protocolo		= 'I'
							and	((x.ie_rede_propria		= ie_rede_propria_w) or ('A' = ie_rede_propria_w))
							and	((ie_tipo_pessoa_prestador_w 	= 'A') or 
								(x.ie_tipo_pessoa_prestador 	= ie_tipo_pessoa_prestador_w))
							and	(((c.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
								 ((c.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
								 ((c.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
							and	a.nr_seq_mensalidade_seg	= nr_seq_mensalidade_seg_w;

							if	((vl_contas_intercambio_mat_w + vl_contas_intercambio_proc_w) > 0) then
								vl_proc_cooperado_w	:= nvl(vl_proc_cooperado_w,0) + (vl_contas_intercambio_mat_w + vl_contas_intercambio_proc_w);
							end if;
						end if;
					elsif	(ie_tipo_item_w = '6') then /* Pos-estabelecido */
						select	sum(e.vl_custo_operacional)
						into	vl_proc_cooperado_w
						from	pls_conta_pos_estab_contab	e,
							pls_conta_pos_estabelecido	d,
							pls_conta			c,
							pls_mensalidade_segurado	b,
							pls_mensalidade_seg_item	a,
							pls_protocolo_conta		p
						where	e.nr_seq_conta_pos	= d.nr_sequencia
						and	d.nr_seq_conta		= c.nr_sequencia
						and	c.nr_sequencia		= a.nr_seq_conta
						and	p.nr_sequencia 		= c.nr_seq_protocolo
						and	p.ie_tipo_protocolo 	<> 'I'
						and	b.nr_sequencia		= a.nr_seq_mensalidade_seg						
						and	a.ie_tipo_item		= '6'
						and	b.nr_sequencia		= nr_seq_mensalidade_seg_w
						and	((nvl(ie_tipo_pessoa_prestador_w,'A') = 'A' and	exists	(select 1
														from	pls_conta_medica_resumo	k,															
															pls_prestador		g 
														where   k.nr_sequencia		= e.nr_seq_conta_resumo
														and	k.nr_seq_conta		= e.nr_seq_conta
														and	k.nr_seq_prestador_pgto	= g.nr_sequencia														
														and	(((k.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
															 ((k.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
															 ((k.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
														and	((k.ie_situacao != 'I') or (k.ie_situacao is null))))
							or	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'F' and	exists	(select 1
															from	pls_conta_medica_resumo	k,
																pls_cooperado		j,
																pls_prestador		g 
															where   k.nr_sequencia		= e.nr_seq_conta_resumo
															and	k.nr_seq_conta		= e.nr_seq_conta
															and	k.nr_seq_prestador_pgto	= g.nr_sequencia
															and	g.cd_pessoa_fisica	= j.cd_pessoa_fisica
															and	g.cd_pessoa_fisica is not null
															and	(((k.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
																 ((k.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
																 ((k.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
															and	((k.ie_situacao != 'I') or (k.ie_situacao is null))))
							or	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'J' and	exists	(select 1
															from	pls_conta_medica_resumo	k,
																pls_cooperado		j,
																pls_prestador		g 
															where   k.nr_sequencia		= e.nr_seq_conta_resumo
															and	k.nr_seq_conta		= e.nr_seq_conta
															and	k.nr_seq_prestador_pgto	= g.nr_sequencia
															and	g.cd_cgc		= j.cd_cgc
															and	g.cd_cgc is not null
															and	(((k.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
																 ((k.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
																 ((k.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
															and	((k.ie_situacao != 'I') or (k.ie_situacao is null)))));

						if	(ie_contas_intercambio_w = 'S') then
							select	nvl(sum(z.vl_item),0) vl_beneficiario
							into	vl_contas_intercambio_mat_w
							from	ptu_nota_servico		x,
								pls_mensalidade_seg_item	a,
								pls_conta_mat			c,
								pls_conta			d,
								pls_protocolo_conta		e,
								pls_conta_pos_estabelecido 	y,
								pls_mensalidade_item_conta 	z
							where	c.nr_sequencia			= x.nr_seq_conta_mat
							and	d.nr_sequencia			= a.nr_seq_conta
							and	c.nr_seq_conta			= d.nr_sequencia
							and	d.nr_seq_protocolo		= e.nr_sequencia
							and  	y.nr_sequencia 			= z.nr_seq_conta_pos_estab
							and  	a.nr_sequencia 			= z.nr_seq_item
							and 	y.nr_seq_conta_mat 		= c.nr_sequencia
							and	e.ie_tipo_protocolo		= 'I'
							and	((x.ie_rede_propria		= ie_rede_propria_w) or ('A' = ie_rede_propria_w))
							and	((ie_tipo_pessoa_prestador_w = 'A') or 
								(x.ie_tipo_pessoa_prestador 	= ie_tipo_pessoa_prestador_w))
							and	(((c.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
								 ((c.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
								 ((c.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
							and	a.nr_seq_mensalidade_seg	= nr_seq_mensalidade_seg_w;

							select	nvl(sum(z.vl_item),0) vl_beneficiario
							into	vl_contas_intercambio_proc_w
							from	ptu_nota_servico		x,
								pls_mensalidade_seg_item	a,
								pls_conta_proc			c,
								pls_conta			d,
								pls_protocolo_conta		e,
								pls_conta_pos_estabelecido 	y,
								pls_mensalidade_item_conta 	z
							where	c.nr_sequencia			= x.nr_seq_conta_proc
							and	d.nr_sequencia			= a.nr_seq_conta
							and	c.nr_seq_conta			= d.nr_sequencia
							and	d.nr_seq_protocolo		= e.nr_sequencia
							and  	y.nr_sequencia 			= z.nr_seq_conta_pos_estab
							and  	a.nr_sequencia 			= z.nr_seq_item
							and 	y.nr_seq_conta_proc 		= c.nr_sequencia
							and	e.ie_tipo_protocolo		= 'I'
							and	((x.ie_rede_propria		= ie_rede_propria_w) or ('A' = ie_rede_propria_w))
							and	((ie_tipo_pessoa_prestador_w = 'A') or 
								(x.ie_tipo_pessoa_prestador = ie_tipo_pessoa_prestador_w))
							and	(((c.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
								 ((c.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
								 ((c.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
							and	a.nr_seq_mensalidade_seg	= nr_seq_mensalidade_seg_w;

							if	((vl_contas_intercambio_proc_w + vl_contas_intercambio_mat_w) > 0) then
								vl_proc_cooperado_w	:= nvl(vl_proc_cooperado_w,0) + (vl_contas_intercambio_proc_w + vl_contas_intercambio_mat_w);
							end if;
						end if;
					else
						vl_proc_cooperado_w	:= 0;
						if	(ie_ato_cooperado_w = 'S') then
							vl_proc_cooperado_w	:= vl_ato_cooperado_w;
						end if;
						
						if	(ie_ato_auxiliar_w = 'S') then
							vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_auxiliar_w;
						end if;
						
						if	(ie_ato_nao_cooperado_w = 'S') then
							vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_nao_cooperado_w;
						end if;
						
						if	(nvl(vl_proc_cooperado_w,0) = 0) then
							select	(vl_item_w  * pls_obter_tx_ato_cooperado('1',ie_tipo_item_w,nr_seq_tipo_lanc_w))
							into	vl_proc_cooperado_w
							from	dual;
						end if;
					end if;

					if	(nvl(vl_proc_cooperado_w,0) <> 0) then
						vl_item_w	:= vl_proc_cooperado_w;
					else
						vl_item_w	:= 0;
					end if;
				end if;
			end if;

			if	(ie_incide_base_w = 'S') then
				vl_item_base_w	:= vl_item_base_w + vl_item_w;
			end if;
		end loop;
	else
		for r_c01_w in C01 loop
			ie_incide_base_w	:= 'N';
				
			for r_c03_w in C03(r_c01_w.ie_tipo_item, r_c01_w.nr_seq_tipo_lanc) loop
				ie_incide_base_w	:= r_c03_w.ie_incide_base;
			end loop;
			
			if	(ie_incide_base_w = 'S') then
				vl_item_base_w	:= vl_item_base_w + r_c01_w.vl_item;
			end if;
		end loop;
	end if;
else
	if	(qt_ato_cooperado_w > 0) then
		vl_item_w		:= pls_store_data_mens_pck.get_trib_vl_item;
		ie_tipo_item_w		:= pls_store_data_mens_pck.get_trib_ie_tipo_item;
		nr_seq_tipo_lanc_w	:= pls_store_data_mens_pck.get_trib_nr_seq_tipo_lanc;
		
		ie_incide_base_w	:= 'N';				
		
		for r_c03_w in C03(ie_tipo_item_w, nr_seq_tipo_lanc_w) loop
			ie_incide_base_w	:= r_c03_w.ie_incide_base;
			ie_ato_cooperado_w	:= r_c03_w.ie_ato_cooperado;
			ie_ato_auxiliar_w	:= r_c03_w.ie_ato_auxiliar;
			ie_ato_nao_cooperado_w	:= r_c03_w.ie_ato_nao_cooperado;
			ie_contas_intercambio_w	:= r_c03_w.ie_contas_intercambio;
			ie_rede_propria_w	:= r_c03_w.ie_rede_propria;
			ie_tipo_pessoa_prestador_w := r_c03_w.ie_tipo_pessoa_prestador;
		end loop;
		
		if	(ie_ato_cooperado_w = 'S') or
			(ie_ato_auxiliar_w = 'S') or
			(ie_ato_nao_cooperado_w = 'S') then
			select	max(a.cd_cgc_estipulante)
			into	cd_cgc_contr_w
			from	pls_segurado	b,
				pls_contrato	a
			where	b.nr_seq_contrato	= a.nr_sequencia
			and	b.nr_sequencia		= pls_store_data_mens_pck.get_trib_nr_seq_segurado;
			
			if	(cd_cgc_contr_w is not null) then
				if	(ie_tipo_item_w = '3') then /*Coparticipacao */
					if	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'A') then
						select	sum(t.vl_ato_auxiliar),
							sum(t.vl_ato_cooperado),
							sum(t.vl_ato_nao_cooperado)
						into	vl_ato_auxiliar_ww,
							vl_ato_cooperado_ww,
							vl_ato_nao_cooperado_ww
						from	pls_conta_copartic_contab	t,
							pls_conta_coparticipacao	p,
							pls_conta			e,
							pls_mensalidade_seg_item	d,
							pls_protocolo_conta		pc
						where	t.nr_seq_conta_copartic		= p.nr_sequencia
						and	p.nr_seq_conta			= e.nr_sequencia
						and	e.nr_sequencia			= d.nr_seq_conta
						and	pc.nr_sequencia			= e.nr_seq_protocolo
						and	pc.ie_tipo_protocolo 		<> 'I'
						and	nvl(p.ie_status_coparticipacao,'S')  = 'S'
						and	d.nr_seq_mensalidade_seg	= p.nr_seq_mensalidade_seg
						and	d.nr_sequencia			= nr_seq_item_p;
						
						vl_proc_cooperado_w := 0;

						if	(ie_ato_auxiliar_w = 'S') then
							vl_proc_cooperado_w	:= vl_ato_auxiliar_ww;
						end if;

						if	(ie_ato_cooperado_w = 'S') then
							vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_cooperado_ww;
						end if;

						if	(ie_ato_nao_cooperado_w = 'S') then
							vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_nao_cooperado_ww;
						end if;
					elsif	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'F') then
						select	sum(t.vl_ato_auxiliar),
							sum(t.vl_ato_cooperado),
							sum(t.vl_ato_nao_cooperado)
						into	vl_ato_auxiliar_ww,
							vl_ato_cooperado_ww,
							vl_ato_nao_cooperado_ww
						from	pls_conta_copartic_contab	t,
							pls_conta_coparticipacao	p,
							--pls_cooperado			i,
							pls_prestador			h,
							pls_conta			e,
							pls_mensalidade_seg_item	d,
							pls_protocolo_conta		pc
						where	t.nr_seq_conta_copartic		= p.nr_sequencia
						and	p.nr_seq_conta			= e.nr_sequencia
						and	pc.nr_sequencia			= e.nr_seq_protocolo
						and	pc.ie_tipo_protocolo 		<> 'I'
						--and	h.cd_pessoa_fisica		= i.cd_pessoa_fisica
						and	exists (select	1
								from	pls_cooperado x
								where	x.cd_pessoa_fisica = h.cd_pessoa_fisica)
						and	h.cd_pessoa_fisica is not null
						and	e.nr_sequencia			= d.nr_seq_conta
						and	t.nr_seq_prestador_pgto		= h.nr_sequencia
						and	nvl(p.ie_status_coparticipacao,'S')  = 'S'
						and	d.nr_seq_mensalidade_seg	= p.nr_seq_mensalidade_seg
						and	d.nr_sequencia			= nr_seq_item_p;

						vl_proc_cooperado_w := 0;

						if	(ie_ato_auxiliar_w = 'S') then
							vl_proc_cooperado_w	:= vl_ato_auxiliar_ww;
						end if;

						if	(ie_ato_cooperado_w = 'S') then
							vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_cooperado_ww;
						end if;

						if	(ie_ato_nao_cooperado_w = 'S') then
							vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_nao_cooperado_ww;
						end if;
					elsif	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'J') then
						select	sum(t.vl_ato_auxiliar),
							sum(t.vl_ato_cooperado),
							sum(t.vl_ato_nao_cooperado)
						into	vl_ato_auxiliar_ww,
							vl_ato_cooperado_ww,
							vl_ato_nao_cooperado_ww
						from	pls_conta_copartic_contab	t,
							pls_conta_coparticipacao	p,
							--pls_cooperado			i,
							pls_prestador			h,
							pls_conta			e,
							pls_mensalidade_seg_item	d,
							pls_protocolo_conta		pc
						where	t.nr_seq_conta_copartic		= p.nr_sequencia
						and	p.nr_seq_conta			= e.nr_sequencia
						and	pc.nr_sequencia			= e.nr_seq_protocolo
						and	pc.ie_tipo_protocolo 		<> 'I'
						--and	h.cd_cgc			= i.cd_cgc
						and	exists (select	1
								from	pls_cooperado x
								where	x.cd_cgc = h.cd_cgc)
						and	h.cd_cgc is not null
						and	t.nr_seq_prestador_pgto		= h.nr_sequencia
						and	e.nr_sequencia			= d.nr_seq_conta
						and	nvl(p.ie_status_coparticipacao,'S')  = 'S'
						and	d.nr_seq_mensalidade_seg	= p.nr_seq_mensalidade_seg
						and	d.nr_sequencia			= nr_seq_item_p;
						
						vl_proc_cooperado_w := 0;

						if	(ie_ato_auxiliar_w = 'S') then
							vl_proc_cooperado_w	:= vl_ato_auxiliar_ww;
						end if;

						if	(ie_ato_cooperado_w = 'S') then
							vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_cooperado_ww;
						end if;

						if	(ie_ato_nao_cooperado_w = 'S') then
							vl_proc_cooperado_w	:= vl_proc_cooperado_w + vl_ato_nao_cooperado_ww;
						end if;
					end if;
					
					if	(ie_contas_intercambio_w = 'S') and
						(nvl(vl_proc_cooperado_w,0) = 0) then
						select	nvl(sum(z.vl_item),0) vl_coparticipacao
						into	vl_contas_intercambio_mat_w
						from	ptu_nota_servico		x,
							pls_mensalidade_seg_item	a,
							pls_conta_mat			c,
							pls_conta			d,
							pls_protocolo_conta		e,
							pls_conta_coparticipacao 	y,
							pls_mensalidade_item_conta 	z
						where	c.nr_sequencia			= x.nr_seq_conta_mat
						and	d.nr_sequencia			= a.nr_seq_conta
						and	c.nr_seq_conta			= d.nr_sequencia
						and	d.nr_seq_protocolo		= e.nr_sequencia
						and  	y.nr_sequencia 			= z.nr_seq_conta_copartic
						and  	a.nr_sequencia 			= z.nr_seq_item
						and 	y.nr_seq_conta_mat 		= c.nr_sequencia
						and	e.ie_tipo_protocolo		= 'I'
						and	((x.ie_rede_propria		= ie_rede_propria_w) or ('A' = ie_rede_propria_w))
						and	((ie_tipo_pessoa_prestador_w = 'A') or 
							(x.ie_tipo_pessoa_prestador 	= ie_tipo_pessoa_prestador_w))
						and	(((c.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
							 ((c.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
							 ((c.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
						and	a.nr_sequencia			= nr_seq_item_p;
					
						select	nvl(sum(z.vl_item),0) vl_coparticipacao
						into	vl_contas_intercambio_proc_w
						from	ptu_nota_servico		x,
							pls_mensalidade_seg_item	a,
							pls_conta_proc			c,
							pls_conta			d,
							pls_protocolo_conta		e,
							pls_conta_coparticipacao 	y,
							pls_mensalidade_item_conta 	z
						where	c.nr_sequencia			= x.nr_seq_conta_proc
						and	d.nr_sequencia			= a.nr_seq_conta
						and	c.nr_seq_conta			= d.nr_sequencia
						and	d.nr_seq_protocolo		= e.nr_sequencia
						and  	y.nr_sequencia 			= z.nr_seq_conta_copartic
						and  	a.nr_sequencia 			= z.nr_seq_item
						and 	y.nr_seq_conta_proc 		= c.nr_sequencia
						and	e.ie_tipo_protocolo		= 'I'
						and	((x.ie_rede_propria		= ie_rede_propria_w) or ('A' = ie_rede_propria_w))
						and	((ie_tipo_pessoa_prestador_w = 'A') or 
							(x.ie_tipo_pessoa_prestador = ie_tipo_pessoa_prestador_w))
						and	(((c.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
							 ((c.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
							 ((c.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
						and	a.nr_sequencia			= nr_seq_item_p;
						
						vl_proc_cooperado_w	:= vl_contas_intercambio_proc_w + vl_contas_intercambio_mat_w;
					end if;
				elsif	(ie_tipo_item_w = '6') then /* Pos-estabelecido */
					select	nvl(sum(e.vl_custo_operacional),0)
					into	vl_proc_cooperado_w
					from	pls_conta_pos_estab_contab	e,
						pls_conta_pos_estabelecido	d,
						pls_conta			b,
						pls_mensalidade_seg_item	a,
						pls_mensalidade_segurado	c,
						pls_protocolo_conta		p
					where	e.nr_seq_conta_pos	= d.nr_sequencia
					and	d.nr_seq_conta		= b.nr_sequencia
					and	b.nr_sequencia		= a.nr_seq_conta					
					and	c.nr_sequencia		= d.nr_seq_mensalidade_seg
					and	c.nr_sequencia		= a.nr_seq_mensalidade_seg
					and	a.ie_tipo_item		= '6'
					and	a.nr_sequencia		= nr_seq_item_p
					and	p.nr_sequencia 		= b.nr_seq_protocolo
					and	p.ie_tipo_protocolo 	<> 'I'
					and	((nvl(ie_tipo_pessoa_prestador_w,'A') = 'A' and exists	(select 1
							from	pls_conta_medica_resumo	k,							
								pls_prestador		g
							where   k.nr_sequencia		= e.nr_seq_conta_resumo
							and	k.nr_seq_conta		= b.nr_sequencia
							and	k.nr_seq_prestador_pgto	= g.nr_sequencia							
							and	(((k.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
								((k.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
								((k.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
							and	((k.ie_situacao != 'I') or (k.ie_situacao is null))))
						or	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'F' and exists	(select 1
														from	pls_conta_medica_resumo	k,
															pls_cooperado		j,
															pls_prestador		g
														where   k.nr_sequencia		= e.nr_seq_conta_resumo
														and	k.nr_seq_conta		= b.nr_sequencia
														and	k.nr_seq_prestador_pgto	= g.nr_sequencia
														and	g.cd_pessoa_fisica	= j.cd_pessoa_fisica
														and	g.cd_pessoa_fisica is not null
														and	(((k.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
															((k.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
															((k.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
														and	((k.ie_situacao != 'I') or (k.ie_situacao is null))))
						or	(nvl(ie_tipo_pessoa_prestador_w,'A') = 'J' and exists	(select 1
														from	pls_conta_medica_resumo	k,
															pls_cooperado		j,
															pls_prestador		g
														where   k.nr_sequencia		= e.nr_seq_conta_resumo
														and	k.nr_seq_conta		= b.nr_sequencia
														and	k.nr_seq_prestador_pgto	= g.nr_sequencia
														and	g.cd_cgc	= j.cd_cgc
														and	g.cd_cgc is not null
														and	(((k.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
															((k.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
															((k.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
														and	((k.ie_situacao != 'I') or (k.ie_situacao is null)))));
					if	(ie_contas_intercambio_w = 'S') and
						(vl_proc_cooperado_w = 0) then
						select	nvl(sum(z.vl_item),0) vl_beneficiario
						into	vl_contas_intercambio_mat_w
						from	ptu_nota_servico		x,
							pls_mensalidade_seg_item	a,
							pls_conta_mat			b,
							pls_conta			c,
							pls_protocolo_conta		d,
							pls_conta_pos_estabelecido 	y,
							pls_mensalidade_item_conta 	z
						where	b.nr_sequencia			= x.nr_seq_conta_mat
						and	c.nr_sequencia			= a.nr_seq_conta
						and	b.nr_seq_conta			= c.nr_sequencia
						and	c.nr_seq_protocolo		= d.nr_sequencia
						and  	y.nr_sequencia 			= z.nr_seq_conta_pos_estab
						and  	a.nr_sequencia 			= z.nr_seq_item
						and 	y.nr_seq_conta_mat 		= b.nr_sequencia
						and	d.ie_tipo_protocolo		= 'I'
						and	((x.ie_rede_propria		= ie_rede_propria_w) or ('A' = ie_rede_propria_w))
						and	((ie_tipo_pessoa_prestador_w 	= 'A') or 
							(x.ie_tipo_pessoa_prestador 	= ie_tipo_pessoa_prestador_w))
						and	(((b.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
							 ((b.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
							 ((b.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
						and	a.nr_sequencia			= nr_seq_item_p;

						select	nvl(sum(z.vl_item),0) vl_beneficiario
						into	vl_contas_intercambio_proc_w
						from	ptu_nota_servico		x,
							pls_mensalidade_seg_item	a,
							pls_conta_proc			b,
							pls_conta			c,
							pls_protocolo_conta		d,
							pls_conta_pos_estabelecido 	y,
							pls_mensalidade_item_conta 	z
						where	b.nr_sequencia			= x.nr_seq_conta_proc
						and	c.nr_sequencia			= a.nr_seq_conta
						and	b.nr_seq_conta			= c.nr_sequencia
						and	c.nr_seq_protocolo		= d.nr_sequencia
						and  	y.nr_sequencia 			= z.nr_seq_conta_pos_estab
						and  	a.nr_sequencia 			= z.nr_seq_item
						and 	y.nr_seq_conta_proc 		= b.nr_sequencia
						and	d.ie_tipo_protocolo		= 'I'
						and	((x.ie_rede_propria		= ie_rede_propria_w) or ('A' = ie_rede_propria_w))
						and	((ie_tipo_pessoa_prestador_w 	= 'A') or 
							(x.ie_tipo_pessoa_prestador 	= ie_tipo_pessoa_prestador_w))
						and	(((b.ie_ato_cooperado = '1') and (ie_ato_cooperado_w = 'S')) or
							 ((b.ie_ato_cooperado = '2') and (ie_ato_auxiliar_w = 'S')) or
							 ((b.ie_ato_cooperado = '3') and (ie_ato_nao_cooperado_w = 'S')))
						and	a.nr_sequencia			= nr_seq_item_p;

						vl_proc_cooperado_w	:= vl_contas_intercambio_mat_w + vl_contas_intercambio_proc_w;
					end if;
				else
					vl_proc_cooperado_w	:= 0;
				end if;
				
				if	(vl_proc_cooperado_w > 0) then
					vl_item_w	:= vl_proc_cooperado_w;
				else
					vl_item_w	:= 0;
				end if;
			end if;
		end if;
		
		if	(ie_incide_base_w = 'S') then
			vl_item_base_w	:= 0;
			if	(ie_tipo_item_w in ('3','6')) then
				vl_item_base_w	:= vl_item_w;
			else
				if	(ie_ato_cooperado_w = 'S') then
					vl_item_base_w	:= pls_store_data_mens_pck.get_trib_vl_ato_cooperado;
				end if;
				
				if	(ie_ato_auxiliar_w = 'S') then
					vl_item_base_w	:= vl_item_base_w + pls_store_data_mens_pck.get_trib_vl_ato_auxiliar;
				end if;
				
				if	(ie_ato_nao_cooperado_w = 'S') then
					vl_item_base_w	:= vl_item_base_w + pls_store_data_mens_pck.get_trib_vl_ato_n_cooperado;
				end if;
			end if;
		end if;
	else
		ie_incide_base_w	:= 'N';
		
		for r_c03_w in C03(pls_store_data_mens_pck.get_trib_ie_tipo_item, pls_store_data_mens_pck.get_trib_nr_seq_tipo_lanc) loop
			ie_incide_base_w	:= r_c03_w.ie_incide_base;
		end loop;
		
		if	(ie_incide_base_w = 'S') then
			vl_item_base_w	:= pls_store_data_mens_pck.get_trib_vl_item;
		end if;
	end if;
end if;

vl_base_p	:= vl_item_base_w;


end pls_obter_base_calc_mens;
/