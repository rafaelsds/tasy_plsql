create or replace
procedure alterar_man_ordem_serv_exec(
			dt_fim_exec_sql_p	date,
			nr_sequencia_p		number) is 

begin

update	man_ordem_servico_exec 
set	dt_fim_execucao = dt_fim_exec_sql_p 
where	nr_sequencia = nr_sequencia_p;

commit;

end alterar_man_ordem_serv_exec;
/