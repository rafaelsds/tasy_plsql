create or replace
procedure alterar_pessoa_titulo_receber(cd_pessoa_fisica_p	varchar2,
					cd_cgc_p		varchar2,
					nr_titulo_p		number,
					nm_usuario_p		varchar2) is 

begin

if (nr_titulo_p is not null) then
	begin

	update	titulo_receber
	set	cd_pessoa_fisica	= cd_pessoa_fisica_p,
		cd_cgc			= cd_cgc_p
	where	nr_titulo		= nr_titulo_p;

	commit;

	end;
end if;

end alterar_pessoa_titulo_receber;
/