create or replace 
procedure pls_oc_cta_tratar_val_63(
			dados_regra_p		pls_tipos_ocor_pck.dados_regra,
			nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
			nm_usuario_p		usuario.nm_usuario%type) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:	Aplicar a valida��o 63 - Validar apresenta��o de contas enquanto benefici�rio internado
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
-------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

Altera��es:
-------------------------------------------------------------------------------------------------------------------
jjung - OS 707203 - 23/04/2014 - Cria��o da rotina.
------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

tb_seq_w		dbms_sql.number_table;
tb_valido_w		dbms_sql.varchar2_table;
tb_seqs_selecao_w	dbms_sql.clob_table;
tb_observacao_w		dbms_sql.varchar2_table;

ds_rest_conta_w		varchar2(1000);
ds_sql_w		varchar2(4000);
cd_cursor_w		sql_pck.t_cursor;

-- Dados das regras.
cursor	C01 (	dados_regra_pc 	pls_tipos_ocor_pck.dados_regra) is
	select	nr_sequencia,
		ie_numero_guia,
		nvl(ie_considerar_auto,'N') ie_considerar_auto,
		nvl(ie_considera_horario, 'S') ie_considera_horario,
		nvl(ie_considera_cta_glosada,'S') ie_considera_cta_glosada
	from	pls_oc_cta_val_cta_per_int regra
	where	regra.nr_seq_oc_cta_comb = dados_regra_pc.nr_sequencia;

--
function obter_restricao_internacao(	ie_momento_p			varchar2,
					dados_regra_p			pls_tipos_ocor_pck.dados_regra,
					ie_numero_guia_p		pls_oc_cta_val_cta_per_int.ie_numero_guia%type,
					ie_considera_horario_p		pls_oc_cta_val_cta_per_int.ie_considera_horario%type,
					ie_considera_cta_glosada_p 	pls_oc_cta_val_cta_per_int.ie_considera_cta_glosada%type)
					return varchar2 is
/*
Finalidade:	Montar o acesso de pesquisa a conta de interna��o quando o benefici�rio estiver internado.
*/
ds_rest_conta_w	varchar2(1000);
begin

-- Sempre ser�o verificadas apenas contas do mesmo benefici�rio e sempre desconsidera a mesma conta.
ds_rest_conta_w	:= 	' and	x.nr_seq_segurado = conta.nr_seq_segurado ' || pls_util_pck.enter_w ||
			' and	x.nr_sequencia <> conta.nr_sequencia ' || pls_util_pck.enter_w ||
			' and 	x.ie_status not in (''C'') ';

-- Verificar se estamos buscando as guias de interna��o ou os demais atendimentos.
if	(ie_momento_p = 'INTER') then

	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and x.ie_tipo_guia = ''5'' ';

	-- Sempre que for buscar as interna��es ser� visto se a conta sendo consistida foi realizada dentro do per�odo da interna��o.
	if	(dados_regra_p.ie_evento = 'IMP') then

		if	(ie_considera_horario_p = 'N') then
			ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
						' and	x.dt_entrada_trunc <= conta.dt_atendimento_imp_dd ' || pls_util_pck.enter_w ||
						' and	(x.dt_alta_trunc is null or x.dt_alta_trunc >= conta.dt_atendimento_imp_dd) ';
		else
			ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
						' and	x.dt_entrada <= conta.dt_atendimento_imp ' || pls_util_pck.enter_w ||
						' and	(x.dt_alta is null or x.dt_alta >= conta.dt_atendimento_imp) ';
		end if;


	else
		if	(ie_considera_horario_p = 'N') then
			ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
						' and	x.dt_entrada_trunc <= conta.dt_atendimento_dd ' || pls_util_pck.enter_w ||
						' and	(x.dt_alta_trunc is null or x.dt_alta_trunc >= conta.dt_atendimento_dd) ';
		else
			ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
						' and	x.dt_entrada <= conta.dt_atendimento ' || pls_util_pck.enter_w ||
						' and	(x.dt_alta is null or x.dt_alta >= conta.dt_atendimento) ';
		end if;
	end if;

else
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and (x.ie_tipo_guia <> ''5'' and x.cd_tiss_atendimento <> ''7'') ';

	-- Sempre que for buscar o que n�o for interna��o ent�o verifica se o atendimento foi realizado no per�odo da interna��o sendo consistida.
	if	(dados_regra_p.ie_evento = 'IMP') then

		if	(ie_considera_horario_p = 'N') then
			ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
						' and	x.dt_atendimento_dd between conta.dt_entrada_imp_trunc and nvl(conta.dt_alta_imp_trunc, x.dt_atendimento_dd) ';
		else
			ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
						' and	x.dt_atendimento between conta.dt_entrada_imp and nvl(conta.dt_alta_imp, x.dt_atendimento) ';
		end if;
	else
		if	(ie_considera_horario_p = 'N') then
			ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
						' and	x.dt_atendimento_dd between conta.dt_entrada_trunc and nvl(conta.dt_alta_trunc, x.dt_atendimento_dd) ';
		else
			ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
						' and	x.dt_atendimento between conta.dt_entrada and nvl(conta.dt_alta, x.dt_atendimento) ';
		end if;
	end if;

end if;

-- Valida apenas contas com mesmo n�mero de guia
if	(ie_numero_guia_p = 'M') then

	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	x.cd_guia_referencia = conta.cd_guia_referencia ';

-- Valida apenas contas com n�mero de guia diferente
elsif	(ie_numero_guia_p = 'D') then

	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	x.cd_guia_referencia <> conta.cd_guia_referencia ' || pls_util_pck.enter_w ||
				' and	nvl(x.nr_seq_guia,0) <> nvl(conta.nr_seq_guia,0) ';
end if;

if	(ie_considera_cta_glosada_p = 'N') then
	ds_rest_conta_w := ds_rest_conta_w || pls_util_pck.enter_w ||
	' and	x.ie_glosa = ''N'' ';
end if;

return ds_rest_conta_w;

end obter_restricao_internacao;


function obter_restricao_inter_auto(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					ie_numero_guia_p	pls_oc_cta_val_cta_per_int.ie_numero_guia%type,
					ie_considera_horario_p	pls_oc_cta_val_cta_per_int.ie_considera_horario%type)	return varchar2 is
/*
Finalidade:	Montar o acesso de pesquisa a guia de autoriza��o de interna��o quando o benefici�rio estiver internado.
*/
ds_rest_conta_w	varchar2(1000);
begin
ds_rest_conta_w	:= 	' and	x.nr_seq_segurado = conta.nr_seq_segurado ' || pls_util_pck.enter_w ||
			' and	x.dt_cancelamento is null '|| pls_util_pck.enter_w || -- N�o considera as guias canceladas
			' and	x.ie_tipo_guia = ''1'' ' || pls_util_pck.enter_w || -- Retona apenas as guias de solicita��o de interna��o
			' and	x.ie_status = ''1'' '; -- Retona apenas as guias autorizadas


-- Valida apenas contas com mesmo n�mero de guia
if	(ie_numero_guia_p = 'M') then

	if	(dados_regra_p.ie_evento = 'IMP') then

		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and	x.nr_sequencia = conta.cd_guia_referencia ';
	else
		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and	x.nr_sequencia = conta.cd_guia_referencia ';
	end if;

-- Valida apenas contas com n�mero de guia diferente
elsif	(ie_numero_guia_p = 'D') then

	if	(dados_regra_p.ie_evento = 'IMP') then

		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and	x.nr_sequencia <> conta.cd_guia_referencia ' || pls_util_pck.enter_w ||
					' and	nvl(x.nr_sequencia,0) <> nvl(conta.nr_seq_guia,0) ';
	else

		ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
					' and	x.nr_sequencia <> conta.cd_guia_referencia ' || pls_util_pck.enter_w ||
					' and	nvl(x.nr_sequencia,0) <> nvl(conta.nr_seq_guia,0) ';
	end if;

end if;

-- Sempre que for buscar as autoriza��es de interna��es ser� visto se a conta sendo consistida foi realizada dentro do per�odo da interna��o.
-- caso a data de alta da autoriza��o n�o estiver preenchida, ser� considerado o dia de interna��o somada com as diarias autorizadas
-- � necess�rio ainda considerar as diarias acumuladas de todas as possiveis guias de solicita��o de prorroga��o de interna��o

if	(ie_considera_horario_p = 'N') then
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	trunc(y.dt_internacao,''dd'') <= trunc(a.dt_atendimento_referencia,''dd'') ' || pls_util_pck.enter_w ||
				' and	(nvl(trunc(y.dt_alta,''dd''),  trunc(y.dt_internacao,''dd'') + nvl(x.qt_dia_autorizado,1) + nvl(( ' || pls_util_pck.enter_w ||
				'												select	sum(nvl(z.qt_dia_autorizado,1)) ' || pls_util_pck.enter_w ||
				'												from	pls_guia_plano		z ' || pls_util_pck.enter_w ||
				'												where	z.nr_seq_guia_principal	= x.nr_sequencia ' || pls_util_pck.enter_w ||
				'												and	z.dt_cancelamento is null '|| pls_util_pck.enter_w || -- N�o considera as guias canceladas
				'												and	z.ie_tipo_guia = ''8'' ' || pls_util_pck.enter_w ||  -- Retona apenas as guias de solicita��o de prorroga��o de interna��o
				'												and	z.ie_status = ''1'' ), 0)' || pls_util_pck.enter_w ||  -- Retona apenas as guias autorizadas
				') >= trunc(a.dt_atendimento_referencia,''dd'')) ';
else
	ds_rest_conta_w :=	ds_rest_conta_w || pls_util_pck.enter_w ||
				' and	y.dt_internacao <= a.dt_atendimento_referencia ' || pls_util_pck.enter_w ||
				' and	(nvl(y.dt_alta,  y.dt_internacao + nvl(x.qt_dia_autorizado,1) + nvl((	select	sum(nvl(z.qt_dia_autorizado,1)) ' || pls_util_pck.enter_w ||
				'										from	pls_guia_plano		z ' || pls_util_pck.enter_w ||
				'										where	z.nr_seq_guia_principal	= x.nr_sequencia ' || pls_util_pck.enter_w ||
				'										and	z.dt_cancelamento is null '|| pls_util_pck.enter_w || -- N�o considera as guias canceladas
				'										and	z.ie_tipo_guia = ''8'' ' || pls_util_pck.enter_w ||  -- Retona apenas as guias de solicita��o de prorroga��o de interna��o
				'										and	z.ie_status = ''1'' ), 0)' || pls_util_pck.enter_w ||  -- Retona apenas as guias autorizadas
				') >= a.dt_atendimento_referencia) ';
end if;

return ds_rest_conta_w;

end obter_restricao_inter_auto;


begin
-- Verificar se a regra informada � v�lida.
if	(dados_regra_p.nr_sequencia is not null) then

	-- Percorrer as regra cadastradas para a valida��o.
	for	r_C01_w in C01(dados_regra_p) loop

		-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
		pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);

		begin
			-- Primeiro vai atr�s do que n�o � interna��o e est� sendo consistido para veriricar se tinha outra conta no per�odo.
			ds_rest_conta_w := obter_restricao_internacao('INTER', dados_regra_p, r_C01_w.ie_numero_guia, r_C01_w.ie_considera_horario, r_C01_w.ie_considera_cta_glosada );

			ds_sql_w	:= 	'select ''S'' ie_valido, ' || pls_util_pck.enter_w ||
						'	''O benefici�rio estava internado no per�odo informado.'' || chr(13) || chr(10) || ' || pls_util_pck.enter_w ||
						'	''Conta interna��o: '' || ' || pls_util_pck.enter_w ||
						'	(select	min(x.nr_sequencia) ' || pls_util_pck.enter_w ||
						'	from	pls_conta_v x ' || pls_util_pck.enter_w ||
						'	where	1 = 1 ' || pls_util_pck.enter_w ||
						ds_rest_conta_w || pls_util_pck.enter_w ||
						'	) ds_observacao, ' || pls_util_pck.enter_w ||
						'	pls_tipos_ocor_pck.obter_sequencia_selecao( ' || pls_util_pck.enter_w ||
						'	null, null, conta.nr_sequencia, :nr_id_transacao, ''N'', null, :ie_excecao, ''V'') seqs ' || pls_util_pck.enter_w ||
						'from	pls_oc_cta_selecao_ocor_v sel, ' || pls_util_pck.enter_w ||
						'	pls_conta_ocor_v conta ' || pls_util_pck.enter_w ||
						'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_util_pck.enter_w ||
						'and	sel.ie_valido = ''S'' ' || pls_util_pck.enter_w ||
						'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_util_pck.enter_w ||
						-- neste primeiro momento verifica tudo que n�o for interna��o para barrar caso o benefici�rio estava internado.
						'and	conta.ie_tipo_guia <> ''5'' ' || pls_util_pck.enter_w ||
						'and	exists (select	1 ' || pls_util_pck.enter_w ||
						'		from	pls_conta_v x ' || pls_util_pck.enter_w ||
						'		where	1 = 1 ' || pls_util_pck.enter_w ||
						'	' || ds_rest_conta_w || pls_util_pck.enter_w ||
						'	) ';

			if	(r_C01_w.ie_considerar_auto = 'S') then
				ds_rest_conta_w		:= obter_restricao_inter_auto(dados_regra_p, r_C01_w.ie_numero_guia, r_C01_w.ie_considera_horario);
				ds_sql_w	:= 	ds_sql_w || pls_util_pck.enter_w ||
							'union all ' || pls_util_pck.enter_w ||
							pls_util_pck.enter_w || 'select ''S'' ie_valido, ' || pls_util_pck.enter_w ||
							'	''O benefici�rio estava internado no per�odo informado.'' || chr(13) || chr(10) || ' || pls_util_pck.enter_w ||
							'	''Autoriza��o interna��o: '' || ' || pls_util_pck.enter_w ||
							'	(select	min(x.nr_sequencia) ' || pls_util_pck.enter_w ||
							'	from	pls_guia_plano x, ' || pls_util_pck.enter_w ||
							'		pls_guia_atendimento	y ' || pls_util_pck.enter_w ||
							'	where	1 = 1 ' || pls_util_pck.enter_w ||
							'		and	y.nr_seq_guia	= x.nr_sequencia ' || pls_util_pck.enter_w ||
							ds_rest_conta_w || pls_util_pck.enter_w ||
							'	) ds_observacao, ' || pls_util_pck.enter_w ||
							'	pls_tipos_ocor_pck.obter_sequencia_selecao( ' || pls_util_pck.enter_w ||
							'	null, null, conta.nr_sequencia, '||to_char(nr_id_transacao_p)||', ''N'', null, '''||dados_regra_p.ie_excecao||''', ''V'') seqs ' || pls_util_pck.enter_w ||
							'from	pls_oc_cta_selecao_ocor_v	sel, ' || pls_util_pck.enter_w ||
							'	pls_conta_ocor_v		conta, ' || pls_util_pck.enter_w ||
							'	pls_conta			a '|| pls_util_pck.enter_w ||
							'where	sel.nr_id_transacao = '||to_char(nr_id_transacao_p)||' ' || pls_util_pck.enter_w ||
							'and	sel.ie_valido = ''S'' ' || pls_util_pck.enter_w ||
							'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_util_pck.enter_w ||
							'and	a.nr_sequencia = conta.nr_sequencia ' || pls_util_pck.enter_w ||
							-- neste primeiro momento verifica tudo que n�o for interna��o para barrar caso o benefici�rio estava internado.
							'and	conta.ie_tipo_guia <> ''5'' ' || pls_util_pck.enter_w ||
							'and	exists (select	1 ' || pls_util_pck.enter_w ||
							'		from	pls_guia_plano 		x, ' || pls_util_pck.enter_w ||
							'			pls_guia_atendimento	y ' || pls_util_pck.enter_w ||
							'		where	1 = 1 ' || pls_util_pck.enter_w ||
							'		and	y.nr_seq_guia	= x.nr_sequencia ' || pls_util_pck.enter_w ||
							'	' || ds_rest_conta_w || pls_util_pck.enter_w ||
							'	) ';

			end if;
			
			open	cd_cursor_w for ds_sql_w
			using	nr_id_transacao_p, dados_regra_p.ie_excecao, nr_id_transacao_p;
			loop
				fetch cd_cursor_w
				bulk collect into tb_valido_w, tb_observacao_w, tb_seqs_selecao_w
				limit pls_cta_consistir_pck.qt_registro_transacao_w;

				exit when tb_seqs_selecao_w.count = 0;

				-- Ser� passado uma lista com todas a sequencias da sele��o para a conta e para seus itens, estas sequ�ncias ser�o atualizadas com os mesmos dados da conta,
				-- conforme passado por par�metro,
				pls_tipos_ocor_pck.gerencia_selecao_validacao(	tb_seq_w, tb_seqs_selecao_w,
										'LISTA', tb_observacao_w, tb_valido_w,
										nm_usuario_p);
			end loop;
			close cd_cursor_w;


			-- Agora vai atr�s das contas de interna��o que est�o sendo consistidas para gerar a ocorr�ncia.
			ds_rest_conta_w := obter_restricao_internacao('OUTROS', dados_regra_p, r_C01_w.ie_numero_guia, r_C01_w.ie_considera_horario, r_C01_w.ie_considera_cta_glosada);

			ds_sql_w	:= 	'select ''S'' ie_valido, ' || pls_util_pck.enter_w ||
						'	''O benefici�rio pode ter realizado outro atendimento durante o per�odo em que estava internado.'' || chr(13) || chr(10) || ' || pls_util_pck.enter_w ||
						'	''Conta : '' || ' || pls_util_pck.enter_w ||
						'	(select	min(x.nr_sequencia) ' || pls_util_pck.enter_w ||
						'	from	pls_conta_v x ' || pls_util_pck.enter_w ||
						'	where	1 = 1 ' || pls_util_pck.enter_w ||
						ds_rest_conta_w || pls_util_pck.enter_w ||
						'	) ds_observacao, ' || pls_util_pck.enter_w ||
						'	pls_tipos_ocor_pck.obter_sequencia_selecao( ' || pls_util_pck.enter_w ||
						'	null, null, conta.nr_sequencia, :nr_id_transacao, ''N'', null, :ie_excecao, ''V'') seqs ' || pls_util_pck.enter_w ||
						'from	pls_oc_cta_selecao_ocor_v sel, ' || pls_util_pck.enter_w ||
						'	pls_conta_ocor_v conta ' || pls_util_pck.enter_w ||
						'where	sel.nr_id_transacao = :nr_id_transacao ' || pls_util_pck.enter_w ||
						'and	sel.ie_valido = ''S'' ' || pls_util_pck.enter_w ||
						'and	conta.nr_sequencia = sel.nr_seq_conta ' || pls_util_pck.enter_w ||
						-- neste momento verifica tudo que for interna��o para que n�o seja poss�vel apresentar contas de interna��o caso
						-- o benefici�rio tiver outra conta neste per�odo.
						'and	conta.ie_tipo_guia = ''5'' ' || pls_util_pck.enter_w ||
						'and	exists (select	1 ' || pls_util_pck.enter_w ||
						'		from	pls_conta_v x ' || pls_util_pck.enter_w ||
						'		where	1 = 1 ' || pls_util_pck.enter_w ||
						'	' || ds_rest_conta_w || pls_util_pck.enter_w ||
						'	) ';

			open	cd_cursor_w for ds_sql_w
			using	nr_id_transacao_p, dados_regra_p.ie_excecao, nr_id_transacao_p;
			loop
				fetch cd_cursor_w
				bulk collect into tb_valido_w, tb_observacao_w, tb_seqs_selecao_w
				limit pls_cta_consistir_pck.qt_registro_transacao_w;

				exit when tb_seqs_selecao_w.count = 0;

				-- Ser� passado uma lista com todas a sequencias da sele��o para a conta e para seus itens, estas sequ�ncias ser�o atualizadas com os mesmos dados da conta,
				-- conforme passado por par�metro,
				pls_tipos_ocor_pck.gerencia_selecao_validacao(	tb_seq_w, tb_seqs_selecao_w,
										'LISTA', tb_observacao_w, tb_valido_w,
										nm_usuario_p);
			end loop;
			close cd_cursor_w;

		exception
		when others then

			-- Verifica se o cursor ainda est� aberto para fech�-lo.
			if	(cd_cursor_w%isopen) then

				close cd_cursor_w;
			end if;

			-- E gravar o log de erro.
			pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p, ds_sql_w, nr_id_transacao_p, nm_usuario_p);
		end;
	end loop; -- C01

	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;

end pls_oc_cta_tratar_val_63;
/