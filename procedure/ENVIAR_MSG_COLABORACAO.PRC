create or replace procedure enviar_msg_colaboracao(cd_destinatarios_p varchar2, nm_usuario_p varchar2, ds_mensagem_p clob, ds_assunto_p varchar2, nr_exame_p varchar2, cd_remetente_p varchar2) is

cursor c01 is
with data as
(select cd_destinatarios_p str from dual)
SELECT trim(regexp_substr(str, '[^,]+', 1, LEVEL)) valor_w
from data
connect by instr(str, ',', 1, level - 1) > 0;

linha_w			         c01%rowtype;

begin
  begin
    open C01;
  exception when others then
    wheb_mensagem_pck.exibir_mensagem_abort(881309);
  end;

  loop
  fetch C01 into
    linha_w;
  exit when c01%notfound;
    begin
      if (linha_w.valor_w is not null) then
        insert into mensagem_colaboracao(nr_sequencia,ie_lido,dt_atualizacao,nm_usuario,ds_mensagem,ds_assunto,ie_excluido,NM_USUARIO_DESTINATARIO,NR_EXAME)        
        values (mensagem_colaboracao_seq.nextval, 'N', sysdate, nm_usuario_p,
                ds_mensagem_p, ds_assunto_p, 'N', linha_w.valor_w, nr_exame_p);
      end if;
    end;
  end loop;
  commit;
end enviar_msg_colaboracao;
/