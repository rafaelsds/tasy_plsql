create or replace
procedure pls_gerar_w_analise_pos_ocor
			(	nr_seq_analise_p		number,
				nr_seq_conta_p			number,
				nr_seq_grupo_atual_p		number,
				nr_seq_conta_proc_p		number,
				nr_seq_conta_mat_p		number,
				nr_seq_conta_pos_estab_p	number,
				ie_glosando_item_p		varchar2,
				nm_usuario_p			varchar2,
				ie_tipo_item_p			varchar2 default null) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar a tabela tempor�ria para visualiza��o das glosas e ocorr�ncias dos itens da an�lise Nova.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_glosa_ocorrencia_w		varchar2(255);
ds_tipo_w			varchar2(255);
ds_status_w			varchar2(255);
ds_glosa_vinc_w			varchar2(255);
ds_pagamento_w			varchar2(255);
ds_faturamento_w		varchar2(255);
ds_nivel_liberacao_w		varchar2(255);
ds_grupos_analise_w		varchar2(255);
nm_grupo_w			varchar2(255);
cd_ocorrencia_w			varchar2(30);
ie_status_w			varchar2(10);
ie_status_cursor_w		varchar2(10);
ie_situacao_w			varchar2(5);
ie_fechar_conta_w		varchar2(5);
ie_glosar_pagamento_w		varchar2(3);
ie_glosar_faturamento_w		varchar2(3);
ie_permissao_grupo_w		varchar2(1);
ie_finalizar_analise_w		varchar2(1);
nr_seq_motivo_glosa_w		number(10);
nr_seq_ocorrencia_w		number(10);
nr_seq_ocor_benef_w		number(10);
nr_seq_conta_glosa_w		number(10);
nr_sequencia_w			number(10)	:= 0;
nr_seq_motivo_vinc_w		number(10);
nr_seq_nivel_lib_w		number(10);
nr_nivel_liberacao_w		number(10);
nr_seq_glosa_w			number(10);
nr_seq_fluxo_grupo_w		number(10);
nr_seq_grupo_cursor_w		number(10);
qt_concluida_w			number(10);
qt_grupos_w			number(10);
qt_fluxo_w			number(10);
ie_auditoria_w			varchar2(10);
ds_observacao_w			varchar2(4000);
ie_selecionada_w		varchar2(1)	:= null;
ie_forma_inativacao_w		varchar2(3);
ds_forma_inativacao_w		varchar2(255);
nr_seq_oc_cta_comb_w		w_pls_analise_glosa_ocor.nr_seq_oc_cta_comb%type;
ie_novo_pos_estab_w		pls_visible_false.ie_novo_pos_estab%type;

Cursor C01 is
	/* Ocorr�ncias da conta */
	select	a.nr_sequencia,
		a.nr_seq_ocorrencia,
		b.cd_ocorrencia,
		b.ds_ocorrencia,
		a.ie_situacao,
		a.ie_fechar_conta,
		b.ie_glosar_pagamento,
		b.ie_glosar_faturamento,
		b.nr_seq_nivel_lib,
		a.nr_seq_glosa,
		a.ie_finalizar_analise,
		a.ie_auditoria,
		a.ds_observacao,
		a.ie_forma_inativacao,
		a.nr_seq_oc_cta_comb
	from	pls_ocorrencia		b,
		pls_ocorrencia_benef	a
	where	a.nr_seq_ocorrencia		= b.nr_sequencia
	and	a.nr_seq_conta_pos_estab	= nr_seq_conta_pos_estab_p
	and	ie_novo_pos_estab_w = 'N'
	and	nr_seq_conta_pos_estab_p is not null
	union all
	select	a.nr_sequencia,
		a.nr_seq_ocorrencia,
		b.cd_ocorrencia,
		b.ds_ocorrencia,
		a.ie_situacao,
		a.ie_fechar_conta,
		b.ie_glosar_pagamento,
		b.ie_glosar_faturamento,
		b.nr_seq_nivel_lib,
		a.nr_seq_glosa,
		a.ie_finalizar_analise,
		a.ie_auditoria,
		a.ds_observacao,
		a.ie_forma_inativacao,
		a.nr_seq_oc_cta_comb
	from	pls_ocorrencia		b,
		pls_ocorrencia_benef	a
	where	a.nr_seq_ocorrencia		= b.nr_sequencia
	and	a.nr_seq_conta_pos_proc	= nr_seq_conta_pos_estab_p
	and	ie_tipo_item_p = 'P'
	and	ie_novo_pos_estab_w = 'S'
	and	nr_seq_conta_pos_estab_p is not null
	union all
	select	a.nr_sequencia,
		a.nr_seq_ocorrencia,
		b.cd_ocorrencia,
		b.ds_ocorrencia,
		a.ie_situacao,
		a.ie_fechar_conta,
		b.ie_glosar_pagamento,
		b.ie_glosar_faturamento,
		b.nr_seq_nivel_lib,
		a.nr_seq_glosa,
		a.ie_finalizar_analise,
		a.ie_auditoria,
		a.ds_observacao,
		a.ie_forma_inativacao,
		a.nr_seq_oc_cta_comb
	from	pls_ocorrencia		b,
		pls_ocorrencia_benef	a
	where	a.nr_seq_ocorrencia		= b.nr_sequencia
	and	a.nr_seq_conta_pos_mat	= nr_seq_conta_pos_estab_p
	and	ie_tipo_item_p = 'M'
	and	ie_novo_pos_estab_w = 'S'
	and	nr_seq_conta_pos_estab_p is not null;
	
/* Fluxo de an�lise */
Cursor C03 is
	select	a.nr_seq_grupo,
		a.ie_status,
		b.nm_grupo_auditor
	from	pls_grupo_auditor b,
		pls_analise_glo_ocor_grupo a
	where	a.nr_seq_grupo		= b.nr_sequencia
	and	a.nr_seq_analise	= nr_seq_analise_p
	and	a.nr_seq_ocor_benef	= nr_seq_ocor_benef_w;

begin
if	(nvl(ie_glosando_item_p,'N') = 'S') then
	delete from w_pls_analise_glosa_ocor
	where	nr_seq_analise	= nr_seq_analise_p
	and	ie_inserido_auditor <> 'S';
else
	delete from w_pls_analise_glosa_ocor
	where	nr_seq_analise	= nr_seq_analise_p;
end if;

select 	nvl(max(ie_novo_pos_estab),'N')
into	ie_novo_pos_estab_w
from	pls_visible_false;


-- Inser��o das ocorr�ncias 
nr_seq_motivo_glosa_w	:= null;
nr_seq_ocorrencia_w	:= null;
ds_glosa_ocorrencia_w	:= null;
nr_seq_ocor_benef_w	:= null;
nr_seq_conta_glosa_w	:= null;
ie_situacao_w		:= null;
ie_status_w		:= null;
cd_ocorrencia_w		:= null;
ds_tipo_w		:= null;
ds_status_w		:= null;
ds_glosa_vinc_w		:= null;
ie_fechar_conta_w	:= null;
ds_pagamento_w		:= null;
ds_faturamento_w	:= null;

open C01;
loop
fetch C01 into	
	nr_seq_ocor_benef_w,
	nr_seq_ocorrencia_w,
	cd_ocorrencia_w,
	ds_glosa_ocorrencia_w,
	ie_situacao_w,
	ie_fechar_conta_w,
	ie_glosar_pagamento_w,
	ie_glosar_faturamento_w,
	nr_seq_nivel_lib_w,
	nr_seq_glosa_w,
	ie_finalizar_analise_w,
	ie_auditoria_w,
	ds_observacao_w,
	ie_forma_inativacao_w,
	nr_seq_oc_cta_comb_w;
exit when C01%notfound;
	begin
	ds_forma_inativacao_w	:= null;
	if	(ie_forma_inativacao_w = 'S') then
		ds_forma_inativacao_w	:= 'Sistema';
	elsif	(ie_forma_inativacao_w = 'U') then
		ds_forma_inativacao_w	:= 'Usu�rio';
	elsif	(ie_forma_inativacao_w = 'US') then
		ds_forma_inativacao_w	:= 'Usu�rio e Sistema';
	end if;
	nr_sequencia_w	:= nr_sequencia_w + 1;
	
	ds_tipo_w	:= 'Ocorr�ncia';
	
	if	(nr_seq_glosa_w is not null) then
		select	nr_seq_motivo_glosa,
			b.ds_motivo_tiss
		into	nr_seq_motivo_vinc_w,
			ds_glosa_vinc_w
		from	tiss_motivo_glosa b,
			pls_conta_glosa a
		where	a.nr_seq_motivo_glosa = b.nr_sequencia
		and	a.nr_sequencia	= nr_seq_glosa_w;
	end if;
	
	if	(ie_glosar_pagamento_w = 'S') then
		ds_pagamento_w	:= 'Impede';
	else
		ds_pagamento_w	:= 'N�o impede';
	end if;
	
	if	(ie_glosar_faturamento_w = 'S') then
		ds_faturamento_w	:= 'Impede';
	else
		ds_faturamento_w	:= 'N�o impede';
	end if;
	
	nr_nivel_liberacao_w	:= null;
	ds_nivel_liberacao_w	:= null;
	
	if	(nr_seq_nivel_lib_w is not null) then
		select	a.nr_nivel_liberacao,
			a.ds_nivel_liberacao
		into	nr_nivel_liberacao_w,
			ds_nivel_liberacao_w
		from	pls_nivel_liberacao a
		where	a.nr_sequencia	= nr_seq_nivel_lib_w;
	end if;
	
	-- Verifica��o do status 
	ie_status_w	:= 'F'; -- Inicia como Sem Fluxo 
	
	-- Cursor do fluxo de an�lise da ocorr�ncia 
	ie_permissao_grupo_w	:= 'N';
	ds_grupos_analise_w	:= null;
	qt_concluida_w		:= 0;
	qt_grupos_w		:= 0;
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_grupo_cursor_w,
		ie_status_cursor_w,
		nm_grupo_w;
	exit when C03%notfound;
		begin
		if	(nr_seq_grupo_cursor_w = nr_seq_grupo_atual_p) then
			ie_permissao_grupo_w	:= 'S';
		end if;
		
		qt_grupos_w	:= qt_grupos_w + 1;
		-- Se tem algum grupo ainda pendente, o status fica pendente de an�lise
		if	(ie_status_cursor_w = 'P') then
			ie_status_w	:= 'P';
		else
			qt_concluida_w	:= qt_concluida_w + 1;
		end if;
		
		-- Apenas listagem descritiva dos grupos de an�lise da ocorr�ncia 
		if	(ds_grupos_analise_w is null) then
			ds_grupos_analise_w	:= nm_grupo_w;
		else
			ds_grupos_analise_w	:= substr(ds_grupos_analise_w || ', ' || nm_grupo_w,1,255);
		end if;
		end;
	end loop;
	close C03;
	
	if	(qt_grupos_w > 0) and
		(qt_concluida_w = qt_grupos_w) then
		if	(ie_situacao_w = 'I') then
			ie_status_w	:= 'I';
		else
			if	(ie_auditoria_w = 'N') then
				ie_status_w	:= 'N';
			else
				ie_status_w	:= 'C';
			end if;
		end if;
	else
		if	(ie_auditoria_w = 'N') then
			ie_status_w	:= 'N';
		end if;
	end if;
	
	ds_status_w	:= obter_valor_dominio(5186,ie_status_w);
	
	ie_selecionada_w	:= null;
	if	(ie_situacao_w = 'A') then
		ie_selecionada_w	:= 'S';
	end if;
	
	nr_sequencia_w	:= nr_sequencia_w + 1;
	
	insert into w_pls_analise_glosa_ocor
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_analise,
		nr_seq_conta,
		nr_seq_conta_proc,
		nr_seq_conta_mat,
		--nr_seq_conta_pos_estab,
		nr_seq_ocorrencia,
		nr_seq_motivo_glosa,
		ds_glosa_ocorrencia,
		nr_seq_ocor_benef,
		nr_seq_conta_glosa,
		ie_situacao,
		ie_status,
		cd_ocorrencia,
		ds_tipo,
		ds_glosa_vinc,
		ie_fechar_conta,
		nr_nivel_liberacao,
		ds_status,
		ds_nivel_liberacao,
		ds_pagamento,
		ds_faturamento,
		ie_permissao_grupo,
		ds_grupos_analise,
		ie_finalizar_analise,
		ds_observacao,
		ie_selecionada,
		nr_seq_conta_pos_estab,
		ie_forma_inativacao,
		ds_forma_inativacao,
		nr_seq_oc_cta_comb)
	values	(nr_sequencia_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_analise_p,
		nr_seq_conta_p,
		nr_seq_conta_proc_p,
		nr_seq_conta_mat_p,
		--nr_seq_conta_pos_estab_p,
		nr_seq_ocorrencia_w,
		nr_seq_motivo_glosa_w,
		ds_glosa_ocorrencia_w,
		nr_seq_ocor_benef_w,
		nr_seq_conta_glosa_w,
		ie_situacao_w,
		ie_status_w,
		cd_ocorrencia_w,
		ds_tipo_w,
		ds_glosa_vinc_w,
		ie_fechar_conta_w,
		nr_nivel_liberacao_w,
		ds_status_w,
		ds_nivel_liberacao_w,
		ds_pagamento_w,
		ds_faturamento_w,
		ie_permissao_grupo_w,
		ds_grupos_analise_w,
		decode(ie_finalizar_analise_w,'S',null,ie_finalizar_analise_w),
		ds_observacao_w,
		ie_selecionada_w,
		nr_seq_conta_pos_estab_p,
		ie_forma_inativacao_w,
		ds_forma_inativacao_w,
		nr_seq_oc_cta_comb_w);
	end;
	
	--Se nova gera��o de p�s,  n�o ser� populado o nr_seq_conta_pos_estab e sim o campo espec�fico para material ou procedimento
	if	(nr_seq_conta_pos_estab_p is not null and ie_novo_pos_estab_w = 'S') then 
	
		if	(ie_tipo_item_p = 'P') then
		
			update	w_pls_analise_glosa_ocor
			set	nr_seq_conta_pos_proc = nr_seq_conta_pos_estab_p,
				nr_seq_conta_pos_estab = null
			where	nr_sequencia = nr_sequencia_w;
			
		elsif	(ie_tipo_item_p = 'M') then
		
			update	w_pls_analise_glosa_ocor
			set	nr_seq_conta_pos_mat = nr_seq_conta_pos_estab_p,
				nr_seq_conta_pos_estab = null
			where	nr_sequencia = nr_sequencia_w;
						
		end if;
	end if;
end loop;
close C01;

commit;

end pls_gerar_w_analise_pos_ocor;
/
