create or replace
procedure man_gerar_solic_executor
			( 	nr_sequencia_p 		number,
				nr_seq_grupo_trab_p	number,
				nm_usuario_p		Varchar2) is 

nm_usuario_exec_w		varchar2(60);
nr_seq_funcao_w			man_ordem_servico_exec.nr_seq_funcao%type;
	
begin

select	substr(nvl(max(obter_usuario_pessoa(cd_pessoa_solicitante)), nm_usuario_p),1,60)
into 	nm_usuario_exec_w
from 	man_ordem_servico
where 	nr_sequencia = nr_sequencia_p;

nr_seq_funcao_w	:= man_obter_funcao_ativ_exec(nr_seq_grupo_trab_p, null, nm_usuario_p);

if	(nr_seq_funcao_w = 0) then
	nr_seq_funcao_w	:= null;
end if;

insert 	into man_ordem_servico_exec
	(	nr_sequencia,
		nr_seq_ordem, 
		dt_atualizacao, 
		nm_usuario, 	
		nm_usuario_exec, 
		qt_min_prev, 	
		dt_ult_visao, 
		nr_seq_funcao,
		dt_recebimento)
values	(	man_ordem_servico_exec_seq.nextval,
		nr_sequencia_p,
		sysdate,
		nm_usuario_p,
		nm_usuario_exec_w,
		null,
		null,
		nr_seq_funcao_w,
		sysdate);
		
commit;

end man_gerar_solic_executor;
/
