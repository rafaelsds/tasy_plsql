create or replace 
procedure fis_gerar_reg_E210_icmsipi(	nr_seq_controle_p	number,
										sg_uf_p 			varchar2) is

/*REGISTRO E210: AJUSTE/BENEF�CIO/INCENTIVO DA APURA��O DO ICMS SUBSTITUI��O TRIBUT�RIA.*/

-- VARIABLES
ie_gerou_dados_bloco_w 			varchar2(1) 	:= 'N';
qt_cursor_w 					number(10) 		:= 0;
nr_vetor_w  					number(10) 		:= 0;

-- Totalizadores
vl_sld_cred_ant_st_w			number(15, 2) 	:= 0;
vl_devol_st_w            		number(15, 2) 	:= 0;
vl_ressarc_st_w					number(15, 2) 	:= 0;
vl_out_cred_st_w				number(15, 2) 	:= 0;
vl_retencao_st_w				number(15, 2) 	:= 0;
vl_sld_dev_ant_st_w				number(15, 2) 	:= 0;
vl_icms_recol_st_w				number(15, 2) 	:= 0;
vl_sld_cred_st_transportar_w	number(15, 2) 	:= 0;
ie_mov_st_w						number(10)		:= 0;

--FIS_EFD_ICMSIPI_E210
nr_seq_icmsipi_E210_w 			fis_efd_icmsipi_E210.nr_sequencia%type;

-- USUARIO
nm_usuario_w 					usuario.nm_usuario%type;

/*Cursor que retorna a totaliza��o dos Creditos de ICMS*/
cursor c_vl_sld_credor_ant is
	select 	a.vl_sld_cred_ant_st
	from 	fis_efd_icmsipi_e210 a ,
			fis_efd_icmsipi_e200 b ,
			fis_efd_icmsipi_controle c
	where 	a.nr_seq_controle = c.nr_seq_anterior
	and 	c.nr_sequencia = nr_seq_controle_p
	and     a.sg_uf = sg_uf_p
	and     b.sg_uf = sg_uf_p;

/*Cursor que retorna a totaliza��o dos VL_DEVOL_ST*/
cursor c_vl_devol_st is
	select nvl(sum(vl_icms_st),0) vl_devol_st
	from(
		/* C190 PF*/
		select 	b.vl_icms_st vl_icms_st
		from 	fis_efd_icmsipi_c100     a,
				fis_efd_icmsipi_c190     b,
				pessoa_fisica            c,
				compl_pessoa_fisica      d
		where 	a.cd_part = c.nr_cpf
		and 	a.nr_seq_nota = b.nr_seq_nota
		and 	a.nr_seq_controle = b.nr_seq_controle
		and 	b.cd_cfop in ('1410', '1411', '1414', '1415', '1660', '1661', '1662', '2410', '2411', '2414', '2415', '2660', '2661' , '2662')
		and 	c.cd_pessoa_fisica = d.cd_pessoa_fisica
		and 	a.nr_seq_controle = nr_seq_controle_p
		and 	d.sg_estado = sg_uf_p
		union
		/* C190 PJ*/
		select 	b.vl_icms_st vl_icms_st
		from 	fis_efd_icmsipi_c100     a,
				fis_efd_icmsipi_c190     b,
				pessoa_juridica          c
		where 	a.cd_part = c.cd_cgc
		and 	a.nr_seq_nota = b.nr_seq_nota
		and 	a.nr_seq_controle = b.nr_seq_controle
		and 	b.cd_cfop in ('1410', '1411', '1414', '1415', '1660', '1661', '1662', '2410', '2411', '2414', '2415', '2660', '2661' , '2662')
		and 	a.nr_seq_controle = nr_seq_controle_p
		and 	c.sg_estado = sg_uf_p);

/*Cursor que retorna a totaliza��o dos VL_RESSARC_ST*/
cursor c_vl_ressarc_st is
	select nvl(sum(vl_icms_st),0) vl_devol_st
	from(
		/* C190 PF*/
		select	b.vl_icms_st vl_icms_st
		from 	fis_efd_icmsipi_c100     a,
				fis_efd_icmsipi_c190     b,
				pessoa_fisica            c,
				compl_pessoa_fisica      d
		where	a.cd_part = c.nr_cpf
		and		a.nr_seq_nota = b.nr_seq_nota
		and		a.nr_seq_controle = b.nr_seq_controle
		and		b.cd_cfop in ('1603', '2603')
		and		c.cd_pessoa_fisica = d.cd_pessoa_fisica
		and		a.nr_seq_controle = nr_seq_controle_p
		and		d.sg_estado = sg_uf_p
		union
		/* C190 PJ*/
		select	b.vl_icms_st vl_icms_st
		from   	fis_efd_icmsipi_c100     a,
				fis_efd_icmsipi_c190     b,
				pessoa_juridica          c
		where	a.cd_part = c.cd_cgc
		and		a.nr_seq_nota = b.nr_seq_nota
		and		a.nr_seq_controle = b.nr_seq_controle
		and		b.cd_cfop in ('1603', '2603')
		and		a.nr_seq_controle = nr_seq_controle_p
		and		c.sg_estado = sg_uf_p);

/*Cursor que retorna a totaliza��o dos VL_OUT_CRED_ST*/
cursor c_vl_out_cred_st is
  select nvl(sum(vl_icms_st),0) vl_out_cred_st
  from(
	/* C190 PF*/
	select	b.vl_icms_st vl_icms_st
	from	fis_efd_icmsipi_c100     a,
			fis_efd_icmsipi_c190     b,
			pessoa_fisica            c,
			compl_pessoa_fisica      d
	where	a.cd_part = c.nr_cpf
	and		a.nr_seq_nota = b.nr_seq_nota
	and		a.nr_seq_controle = b.nr_seq_controle
	and		substr(b.cd_cfop,1,1) in ('1','2')
	and		b.cd_cfop not in ('1410', '1411', '1414', '1415', '1660', '1661', '1662', '2410', '2411', '2414', '2415', '2660', '2661' , '2662')
	and		c.cd_pessoa_fisica = d.cd_pessoa_fisica
	and		a.nr_seq_controle = nr_seq_controle_p
	and		d.sg_estado = sg_uf_p
	union
	/* C190 PJ*/
	select	b.vl_icms_st vl_icms_st
	from	fis_efd_icmsipi_c100     a,
			fis_efd_icmsipi_c190     b,
			pessoa_juridica          c
	where	a.cd_part = c.cd_cgc
	and		a.nr_seq_nota = b.nr_seq_nota
	and		a.nr_seq_controle = b.nr_seq_controle
	and		substr(b.cd_cfop,1,1) in ('1','2')
	and		b.cd_cfop not in ('1410', '1411', '1414', '1415', '1660', '1661', '1662', '2410', '2411', '2414', '2415', '2660', '2661' , '2662')
	and		a.nr_seq_controle = nr_seq_controle_p
	and		c.sg_estado = sg_uf_p);

/*Cursor que retorna a totaliza��o dos VL_OUT_CRED_ST*/
cursor c_vl_reten�ao_st is
  select nvl(sum(vl_icms_st),0) vl_reten�ao_st
  from(
	/* C190 PF*/
	select	b.vl_icms_st vl_icms_st
	from	fis_efd_icmsipi_c100     a,
			fis_efd_icmsipi_c190     b,
			pessoa_fisica            c,
			compl_pessoa_fisica      d
	where	a.cd_part = c.nr_cpf
	and		a.nr_seq_nota = b.nr_seq_nota
	and		a.nr_seq_controle = b.nr_seq_controle
	and		substr(b.cd_cfop,1,1) in ('5','6')
	and		c.cd_pessoa_fisica = d.cd_pessoa_fisica
	and		a.nr_seq_controle = nr_seq_controle_p
	and		d.sg_estado = sg_uf_p
	union
	/* C190 PJ*/
	select	b.vl_icms_st vl_icms_st
	from	fis_efd_icmsipi_c100     a,
			fis_efd_icmsipi_c190     b,
			pessoa_juridica          c
	where	a.cd_part = c.cd_cgc
	and		a.nr_seq_nota = b.nr_seq_nota
	and		a.nr_seq_controle = b.nr_seq_controle
	and		substr(b.cd_cfop,1,1) in ('5','6')
	and		a.nr_seq_controle = nr_seq_controle_p
	and		c.sg_estado = sg_uf_p
	union
	/* C590 PF */
	select	b.vl_icms_st vl_icms_st
	from	fis_efd_icmsipi_c500     a,
			fis_efd_icmsipi_c590     b,
			pessoa_fisica            c,
			compl_pessoa_fisica      d
	where	a.cd_part = c.nr_cpf
	and		a.nr_seq_nota = b.nr_seq_nota
	and		a.nr_seq_controle = b.nr_seq_controle
	and		substr(b.cd_cfop,1,1) in ('5','6')
	and		c.cd_pessoa_fisica = d.cd_pessoa_fisica
	and		a.nr_seq_controle = nr_seq_controle_p
	and		d.sg_estado = sg_uf_p
	union
	/* C590 PJ*/
	select	b.vl_icms_st vl_icms_st
	from	fis_efd_icmsipi_c500     a,
			fis_efd_icmsipi_c590     b,
			pessoa_juridica          c
	where	a.cd_part = c.cd_cgc
	and		a.nr_seq_nota = b.nr_seq_nota
	and		a.nr_seq_controle = b.nr_seq_controle
	and		substr(b.cd_cfop,1,1) in ('5','6')
	and		a.nr_seq_controle = nr_seq_controle_p
	and		c.sg_estado = sg_uf_p
	union
	/* D590 PF */
	select	b.VL_ICMS_UF VL_ICMS_ST
	from	fis_efd_icmsipi_d500     a,
			fis_efd_icmsipi_d590     b,
			pessoa_fisica            c,
			compl_pessoa_fisica      d
	where	a.cd_part = c.nr_cpf
	and		a.nr_seq_nota = b.nr_seq_nota
	and		a.nr_seq_controle = b.nr_seq_controle
	and		substr(b.cd_cfop,1,1) in ('5','6')
	and		c.cd_pessoa_fisica = d.cd_pessoa_fisica
	and		a.nr_seq_controle = nr_seq_controle_p
	and		d.sg_estado = sg_uf_p
	union
	/* D590 PJ*/
	select	b.VL_ICMS_UF VL_ICMS_ST
	from	fis_efd_icmsipi_d500     a,
			fis_efd_icmsipi_d590     b,
			pessoa_juridica          c
	where	a.cd_part = c.cd_cgc
	and		a.nr_seq_nota = b.nr_seq_nota
	and		a.nr_seq_controle = b.nr_seq_controle
	and		substr(b.cd_cfop,1,1) in ('5','6')
	and		a.nr_seq_controle = nr_seq_controle_p
	and		c.sg_estado = sg_uf_p );

/*Cursor que retorna as informa��es para o registro E210 restringindo pela sequencia da nota fiscal*/
  cursor c_reg_E210 is
  select  'E210' cd_reg,
	'0' ie_mov_st,
	null vl_sld_cred_ant_st,
	null vl_devol_st,
	null vl_ressarc_st,
	0 vl_out_cred_st,
	0 vl_aj_creditos_st,
	null vl_retencao_st,
	0 vl_out_deb_st,
	0 vl_aj_debitos_st,
	null vl_sld_dev_ant_st,
	0 vl_deducoes_st,
	null vl_icms_recol_st,
	null vl_sld_cred_st_tran,
	0 vl_deb_esp_st
  from dual;

  /*Cria��o do array com o tipo sendo do cursor eespecificado - c_reg_E210*/
  type reg_c_reg_E210 is table of c_reg_E210%RowType;
  vetRegE210 reg_c_reg_E210;

  /*Cria��o do array com o tipo sendo da tabela eespecificada - FIS_EFD_ICMSIPI_E210 */
  type registro is table of fis_efd_icmsipi_E210%rowtype index by pls_integer;
  fis_registros_w registro;

begin
/*Obte��o do usu�rio ativo no tasy*/
nm_usuario_w := Obter_Usuario_Ativo;

open c_reg_E210;
loop
fetch c_reg_E210 bulk collect  into vetRegE210 limit 1000;
	  for i in 1 .. vetRegE210.Count loop
	  begin

	  /*Incrementa a variavel para o array*/
	  qt_cursor_w:=  qt_cursor_w + 1;

	  ie_mov_st_w := vetRegE210(i).ie_mov_st;

	  if (ie_gerou_dados_bloco_w = 'N') then
	    ie_gerou_dados_bloco_w := 'S';
	  end if;

	  /* c_vl_tot_creditos */
	  open c_vl_sld_credor_ant;
	  fetch c_vl_sld_credor_ant  into vl_sld_cred_ant_st_w;
	    if 	c_vl_sld_credor_ant%FOUND then
			vl_sld_cred_ant_st_w := 0;
	    end if;
	  close c_vl_sld_credor_ant;

	  /* c_vl_devol_st */
	  open c_vl_devol_st;
	  fetch c_vl_devol_st  into vl_devol_st_w;
	  close c_vl_devol_st;

	  /* c_vl_ressarc_st */
	  open c_vl_ressarc_st;
	  fetch c_vl_ressarc_st  into vl_ressarc_st_w;
	  close c_vl_ressarc_st;

	  /* c_vl_out_cred_st */
	  open c_vl_out_cred_st;
	  fetch c_vl_out_cred_st  into vl_out_cred_st_w;
	  close c_vl_out_cred_st;

	  /* c_vl_reten�ao_st */
	  open c_vl_reten�ao_st;
	  fetch c_vl_reten�ao_st  into vl_retencao_st_w;
	  close c_vl_reten�ao_st;
	  
	  fis_efd_icmsipi_calcula_e210(vl_retencao_st_w, 
					vetRegE210(i).vl_out_deb_st,
					vetRegE210(i).vl_aj_debitos_st,
					vl_sld_cred_ant_st_w,
					vl_devol_st_w,
					vl_ressarc_st_w,
					vl_out_cred_st_w,
					vetRegE210(i).vl_aj_creditos_st,
					vetRegE210(i).vl_deducoes_st,
					vl_sld_dev_ant_st_w,
					vl_icms_recol_st_w,
					vl_sld_cred_st_transportar_w,
					ie_mov_st_w);

					/*Busca da sequencia da tabela especificada - fis_efd_icmsipi_E210 */
	  select fis_efd_icmsipi_E210_seq.nextval  into nr_seq_icmsipi_E210_w  from dual;

	  /*Inserindo valores no array para realiza��o do forall posteriormente*/
	  fis_registros_w(qt_cursor_w).nr_sequencia     	:= nr_seq_icmsipi_E210_w;
	  fis_registros_w(qt_cursor_w).dt_atualizacao     	:= sysdate;
	  fis_registros_w(qt_cursor_w).nm_usuario     		:= nm_usuario_w;
	  fis_registros_w(qt_cursor_w).dt_atualizacao_nrec	:= sysdate;
	  fis_registros_w(qt_cursor_w).nm_usuario_nrec     	:= nm_usuario_w;
	  fis_registros_w(qt_cursor_w).cd_reg       		:= 'E210';
	  fis_registros_w(qt_cursor_w).ie_mov_st      		:= ie_mov_st_w;
	  fis_registros_w(qt_cursor_w).vl_sld_cred_ant_st   := vl_sld_cred_ant_st_w;
	  fis_registros_w(qt_cursor_w).vl_devol_st    		:= vl_devol_st_w;
	  fis_registros_w(qt_cursor_w).vl_ressarc_st    	:= vl_ressarc_st_w;
	  fis_registros_w(qt_cursor_w).vl_out_cred_st    	:= vetrege210(i).vl_out_cred_st;
	  fis_registros_w(qt_cursor_w).vl_aj_creditos_st   	:= vetrege210(i).vl_aj_creditos_st;
	  fis_registros_w(qt_cursor_w).vl_retencao_st    	:= vl_retencao_st_w;
	  fis_registros_w(qt_cursor_w).vl_out_deb_st    	:= vetrege210(i).vl_out_deb_st;
	  fis_registros_w(qt_cursor_w).vl_aj_debitos_st    	:= vetrege210(i).vl_aj_debitos_st;
	  fis_registros_w(qt_cursor_w).vl_sld_dev_ant_st    := vl_sld_dev_ant_st_w;
	  fis_registros_w(qt_cursor_w).vl_deducoes_st    	:= vetrege210(i).vl_deducoes_st;
	  fis_registros_w(qt_cursor_w).vl_icms_recol_st    	:= vl_icms_recol_st_w;
	  fis_registros_w(qt_cursor_w).vl_sld_cred_st_tran  := vl_sld_cred_st_transportar_w;
	  fis_registros_w(qt_cursor_w).vl_deb_esp_st    	:= vetrege210(i).vl_deb_esp_st;	  
	  fis_registros_w(qt_cursor_w).sg_uf		    	:= sg_uf_p;
	  fis_registros_w(qt_cursor_w).nr_seq_controle     	:= nr_seq_controle_p;

	  if (nr_vetor_w >= 1000) then
	  begin
	  /*Inserindo registros definitivamente na tabela especifica - FIS_EFD_ICMSIPI_E210 */
	  forall i in fis_registros_w.first .. fis_registros_w.last
	    insert into fis_efd_icmsipi_E210 values fis_registros_w (i);

	    nr_vetor_w := 0;
	    fis_registros_w.delete;

	    commit;

	  end;
	  end if;

	  /*incrementa variavel para realizar o forall quando chegar no valor limite*/
	  nr_vetor_w := nr_vetor_w + 1;

	  end;
	  end loop;
exit when c_reg_E210%notfound;
end loop;
close c_reg_E210;

if (fis_registros_w.count > 0) then
begin
/*Inserindo registro que n�o entraram outro for all devido a quantidade de registros no vetor*/
forall i in fis_registros_w.first .. fis_registros_w.last
  insert into fis_efd_icmsipi_E210 values fis_registros_w (i);

  fis_registros_w.delete;

  commit;

end;
end if;

/*Libera memoria*/
dbms_session.free_unused_user_memory;

/*Atualiza��o informa��o no controle de gera��o de registro para SIM*/
if (ie_gerou_dados_bloco_w = 'S') then
update fis_efd_icmsipi_controle
set ie_mov_E = 'S'
where nr_sequencia = nr_seq_controle_p;
end if;

end fis_gerar_reg_E210_icmsipi;
/
