create or replace
procedure GERAR_W_INTEGRACAO_BANCO(	nr_seq_lote_p			number,
					ds_conteudo_p			varchar2,
					ie_tipo_registro_p		varchar2,
					nm_usuario_p			varchar2,
					ie_commit_p			varchar2) is

begin

insert	into w_integracao_banco
	(ds_conteudo,
	dt_atualizacao,
	ie_tipo_registro,
	nm_usuario,
	nr_sequencia,
	nr_seq_lote)
values	(ds_conteudo_p,
	sysdate,
	ie_tipo_registro_p,
	nm_usuario_p,
	w_integracao_banco_seq.nextval,
	nr_seq_lote_p);

if	(nvl(ie_commit_p,'N')	= 'S') then

	commit;

end if;

end GERAR_W_INTEGRACAO_BANCO;
/