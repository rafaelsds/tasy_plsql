create or replace
procedure Atualizar_data_hem_proc( nr_sequencia_p	number,
			data_p		Varchar2) is 

begin

	if (nr_sequencia_P is not null) then
		UPDATE 	HEM_PROC
		SET 	DT_PRIM_INSULF = TO_DATE(data_p, 'dd/mm/yyyy hh24:mi:ss')
		WHERE 	NR_SEQUENCIA = nr_sequencia_p;
	end if;
	
commit;

end Atualizar_data_hem_proc;
/