create or replace
procedure efetivar_carta_compromisso(	nr_interno_conta_p		conta_paciente.nr_interno_conta%type default null,
				nr_titulo_pf_p		titulo_receber.nr_titulo%type,
				ie_origem_info_p		varchar2,
				cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,
				nm_usuario_p		usuario.nm_usuario%type) is

/*IE_ORIGEM_INFO_P: 
'C' - Conta Paciente, 
'T' - Manuten��o de T�tulos a Receber
*/

ds_erro_w 		varchar(4000);
nr_interno_conta_w		conta_paciente.nr_interno_conta%type;
cd_tipo_recebimento_w 	parametro_contas_receber.cd_tipo_receb_carta%type;

cursor c01_cartas_conta is
    select 	nr_sequencia
	from 	carta_compromisso
	where 	ie_situacao = 'A'
	and		dt_liberacao is not null
	and		dt_inativacao is null
	and		nr_titulo_gerado is null
	and		nr_titulo_baixa is null
	and		nr_interno_conta = nr_interno_conta_p
    order by nr_sequencia;
	
cursor c02_cartas_titulo is
	select 	nr_sequencia
	from 	carta_compromisso
	where 	ie_situacao = 'A'
	and		dt_liberacao is not null
	and		dt_inativacao is null
	and		nr_titulo_gerado is null
	and		nr_interno_conta is null
	and		nr_seq_baixa is null
	and		nr_titulo_baixa = nr_titulo_pf_p
    order by nr_sequencia;
	
begin
	
	if (ie_origem_info_p = 'C' and nr_interno_conta_p is not null) then
	
		for c01_w in c01_cartas_conta loop
			begin
				
			/*Gerar t�tulo institui��o*/
			gerar_titulo_rec_carta(c01_w.nr_sequencia, 'N', cd_estabelecimento_p, nm_usuario_p);
			
			/*Baixar t�tulo paciente*/
			baixar_titulo_rec_carta(c01_w.nr_sequencia, nr_titulo_pf_p, 'N', cd_estabelecimento_p, nm_usuario_p);
			
			update	carta_compromisso
			set		ie_origem_titulo = ie_origem_info_p
			where	nr_sequencia = c01_w.nr_sequencia;
				
			exception
			when others then
				wheb_mensagem_pck.exibir_mensagem_abort(substr(sqlerrm, 1, 500));	
			end;
			
		end loop;
	
	elsif (ie_origem_info_p = 'T' and nr_titulo_pf_p is not null) then

		for c02_w in c02_cartas_titulo loop
			begin
				
			/*Gerar t�tulo institui��o*/
			gerar_titulo_rec_carta(c02_w.nr_sequencia, 'N', cd_estabelecimento_p, nm_usuario_p);
			
			/*Baixar t�tulo paciente*/
			baixar_titulo_rec_carta(c02_w.nr_sequencia, nr_titulo_pf_p, 'N', cd_estabelecimento_p, nm_usuario_p);
			
			select	max(nr_interno_conta)
			into	nr_interno_conta_w
			from	titulo_receber
			where	nr_titulo = nr_titulo_pf_p;
			
			update	carta_compromisso
			set		ie_origem_titulo = ie_origem_info_p,
					nr_interno_conta = nr_interno_conta_w
			where	nr_sequencia = c02_w.nr_sequencia;
			
			exception when others then
				rollback;
				
				update 	carta_compromisso
				set		nr_titulo_baixa = null,
						nr_seq_baixa = null
				where	nr_sequencia = c02_w.nr_sequencia;
				commit;

				wheb_mensagem_pck.exibir_mensagem_abort(substr(sqlerrm, 1, 500));
			end;
		end loop;

		commit;
	end if;
	
end efetivar_carta_compromisso;
/