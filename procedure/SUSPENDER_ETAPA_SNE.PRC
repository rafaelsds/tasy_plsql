create or replace
procedure suspender_etapa_sne(	nr_seq_horario_p	number,
				nm_usuario_p		varchar2,
				nr_seq_motivo_p		number,
				ds_justificativa_p	varchar2) is 
				
nr_sequencia_w		number(10);
nr_sequencia_ww		number(10);
nr_prescricao_w		number(14);
dt_aprazamento_w	date;

begin

update	prescr_mat_hor
set	dt_suspensao 	= sysdate,
	nm_usuario 	= nm_usuario_p,
	nm_usuario_susp	= nm_usuario_p
where	nr_sequencia 	= nr_seq_horario_p;

select	max(nr_prescricao),
	max(nr_seq_material),
	max(dt_horario)
into	nr_prescricao_w,	
	nr_sequencia_ww,
	dt_aprazamento_w
from	prescr_mat_hor
where	nr_sequencia 	= nr_seq_horario_p;

update	prescr_material
set	ie_horario_susp	= 'S'
where	nr_prescricao	= nr_prescricao_w
and	nr_sequencia	= nr_sequencia_ww;

select	prescr_solucao_evento_seq.nextval
into	nr_sequencia_w
from	dual;

insert into prescr_solucao_evento	(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_prescricao,
	nr_seq_solucao,
	nr_seq_material,
	nr_seq_procedimento,
	nr_seq_nut,
	nr_seq_nut_neo,
	ie_forma_infusao,
	ie_tipo_dosagem,
	qt_dosagem,
	qt_vol_infundido,
	qt_vol_desprezado,
	cd_pessoa_fisica,
	ie_alteracao,
	dt_alteracao,
	ie_evento_valido,
	nr_seq_motivo,
	ie_tipo_solucao,
	ds_justificativa,
	dt_aprazamento)
values	(
	nr_sequencia_w,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_prescricao_w,
	null,
	nr_sequencia_ww,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	obter_dados_usuario_opcao(nm_usuario_p, 'C'),
	12,
	sysdate,
	'S',
	nr_seq_motivo_p,
	2,
	ds_justificativa_p,
	dt_aprazamento_w);
commit;

end suspender_etapa_sne;
/
