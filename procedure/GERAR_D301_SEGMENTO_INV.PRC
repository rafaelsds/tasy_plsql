create or replace
procedure gerar_d301_segmento_INV(	nr_seq_dataset_p	number,	
					nm_usuario_p		varchar2) is 

cd_usuario_convenio_w		d301_segmento_inv.cd_usuario_convenio%type;
nr_seq_301_status_segurado_w	d301_segmento_inv.nr_seq_301_status_segurado%type;
nr_seq_301_grupo_pessoa_w	d301_segmento_inv.nr_seq_301_grupo_pessoa%type;
nr_seq_301_doenca_cronica_w	d301_segmento_inv.nr_seq_301_doenca_cronica%type;
ds_mesano_validade_cart_w	d301_segmento_inv.ds_mesano_validade_cart%type;
nr_interno_segurado_w		d301_segmento_inv.nr_interno_segurado%type;
nr_seq_seqmento_inv_w		d301_segmento_inv.nr_sequencia%type;
nr_episodio_convenio_w		d301_segmento_inv.nr_episodio_convenio%type;
nr_arquivo_convenio_w		d301_segmento_inv.nr_arquivo_convenio%type;
ds_dt_inicio_cobert_conv_w	d301_segmento_inv.ds_dt_inicio_cobert_conv%type;
nr_contrato_conv_w		d301_segmento_inv.nr_contrato_conv%type;

nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
ie_tipo_conveniado_w		atend_categoria_convenio.ie_tipo_conveniado%type;
ie_tipo_doenca_w		diagnostico_doenca.ie_tipo_doenca%type;


begin

nr_seq_301_status_segurado_w 	:= null;
nr_arquivo_convenio_w		:= null;
nr_contrato_conv_w		:= '00xxxxxxx'; --Pendente de defini��o

select	nr_atendimento
into	nr_atendimento_w
from	d301_dataset_envio
where	nr_sequencia = nr_seq_dataset_p;

select	substr(max(a.cd_usuario_convenio),1,12),
	max(a.ie_tipo_conveniado),
	max(c.ie_tipo_doenca),
	max(to_char(dt_validade_carteira,'YYMM')), --AJUSTADA A MASCARA
	max(b.cd_pessoa_fisica),
	max(e.nr_episodio),
	max(to_char(dt_inicio_vigencia,'YYYYMMDD'))
into	cd_usuario_convenio_w,
	ie_tipo_conveniado_w,
	ie_tipo_doenca_w,
	ds_mesano_validade_cart_w,
	nr_interno_segurado_w,
	nr_episodio_convenio_w,
	ds_dt_inicio_cobert_conv_w
from	atend_categoria_convenio a,
	atendimento_paciente b,
	diagnostico_doenca c,
	pessoa_fisica d,
	episodio_paciente e
where	a.nr_atendimento 	= b.nr_atendimento
and	b.cd_medico_resp 	= c.cd_medico(+)
and	b.cd_pessoa_fisica 	= d.cd_pessoa_fisica
and	d.cd_pessoa_fisica	= e.cd_pessoa_fisica
and	a.nr_seq_interno 	= obter_atecaco_atendimento(nr_atendimento_w);



select	max(obter_conversao_301('c301_12_grupo_pessoa','atend_categoria_convenio',null,ie_tipo_conveniado_w,'I')),
	max(obter_conversao_301('c301_12_doenca_cronica','diagnostico_doenca',null,ie_tipo_doenca_w,'I'))
into	nr_seq_301_grupo_pessoa_w,
	nr_seq_301_doenca_cronica_w
from 	dual;

select	d301_segmento_inv_seq.nextval
into	nr_seq_seqmento_inv_w
from 	dual;

insert into D301_SEGMENTO_INV(	cd_usuario_convenio,
				dt_atualizacao,
				nm_usuario,
				nr_seq_301_status_segurado,
				nr_seq_301_grupo_pessoa,
				nr_seq_301_doenca_cronica,
				ds_mesano_validade_cart,
				nr_interno_segurado,
				nr_seq_dataset,
				nr_sequencia,
				nr_episodio_convenio,
				nr_arquivo_convenio,
				ds_dt_inicio_cobert_conv,
				nr_contrato_conv)
			values(	cd_usuario_convenio_w,
				sysdate,
				nm_usuario_p,
				nr_seq_301_status_segurado_w,
				nr_seq_301_grupo_pessoa_w,
				nr_seq_301_doenca_cronica_w,
				ds_mesano_validade_cart_w,
				nr_interno_segurado_w,
				nr_seq_dataset_p,
				nr_seq_seqmento_inv_w,
				nr_episodio_convenio_w,
				nr_arquivo_convenio_w,
				ds_dt_inicio_cobert_conv_w,
				nr_contrato_conv_w);

commit;
				
end gerar_d301_segmento_INV;
/