create or replace
procedure sib_importar_dev_cco
			(	nr_seq_lote_sib_p	Number,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2) is 

ie_registro_w			number(1);
cd_motivo_w			number(2);
cd_usuario_plano_w		varchar2(30);
nm_beneficiario_w		varchar2(70);
dt_nascimento_w			date;
cd_usuario_plano_ant_w		varchar2(30);
nr_cco_w			number(10);
ie_dig_verificador_w		number(2);
nr_seq_interno_w		number(7);
ds_texto_w			varchar2(255);
cd_usuario_plano_ww		varchar2(1);
nm_beneficiario_ww		varchar2(1);
		
Cursor C01 is
	select	ds_texto
	from	w_sib_devolucao;
	
begin

delete	from sib_devolucao_cco
where	nr_seq_lote	= nr_seq_lote_sib_p;

delete	sib_devolucao_log
where	nr_seq_lote_sib_envio	= nr_seq_lote_sib_p
and	ie_tipo_arquivo		= 'C';

delete	sib_devolucao_log
where	nr_seq_lote_sib_reenvio	= nr_seq_lote_sib_p
and	ie_tipo_arquivo		= 'C';

open C01;
loop
fetch C01 into	
	ds_texto_w;
exit when C01%notfound;
	begin
	select	to_number(nvl(substr(ds_texto_w,1,7),'0')),
		to_number(nvl(substr(ds_texto_w,8,1),'0')),
		to_number(nvl(substr(ds_texto_w,9,2),'0')),
		limpa_espacos_entre(substr(ds_texto_w,11,30)),
		limpa_espacos_entre(substr(ds_texto_w,41,70)),
		substr(ds_texto_w,119,30),
		to_number(nvl(substr(ds_texto_w,149,10),'0')),
		to_number(nvl(substr(ds_texto_w,159,2),'0'))
	into	nr_seq_interno_w,
		ie_registro_w,
		cd_motivo_w,
		cd_usuario_plano_w,
		nm_beneficiario_w,
		cd_usuario_plano_ant_w,
		nr_cco_w,
		ie_dig_verificador_w
	from	dual;
	
	select	substr(cd_usuario_plano_w,length(cd_usuario_plano_w),1)
	into	cd_usuario_plano_ww
	from	dual;
	
	begin
	dt_nascimento_w	:=	to_date(substr(ds_texto_w,111,8),'yyyy/mm/dd');
	exception
	when others then
		/*aaschlote 11/05/2011 OS - 317669*/
		sib_gravar_log_devolucao(nr_seq_lote_sib_p,'C',nr_seq_interno_w,'A data de nascimento est� inv�lida -> '|| substr(ds_texto_w,111,8),
					cd_estabelecimento_p,nm_usuario_p);
	end;
	
	
	if	(cd_usuario_plano_ww = ' ') then
		cd_usuario_plano_w	:= substr(cd_usuario_plano_w,1,length(cd_usuario_plano_w)-1);
	end if;
	
	select	substr(nm_beneficiario_w,length(nm_beneficiario_w),1)
	into	nm_beneficiario_ww
	from	dual;
	
	if	(nm_beneficiario_ww = ' ') then
		nm_beneficiario_w	:= substr(nm_beneficiario_w,1,length(nm_beneficiario_w)-1);
	end if;

	begin
	insert into sib_devolucao_cco (	nr_sequencia,
					nr_seq_lote,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_interno,
					ie_registro,
					cd_motivo,
					cd_usuario_plano,
					nm_beneficiario,
					dt_nascimento,
					cd_usuario_plano_ant,
					nr_cco,
					ie_digito_cco)
				values(	sib_devolucao_cco_seq.NextVal,
					nr_seq_lote_sib_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_interno_w,
					ie_registro_w,
					cd_motivo_w,
					cd_usuario_plano_w,
					nm_beneficiario_w,
					dt_nascimento_w,
					cd_usuario_plano_ant_w,
					nr_cco_w,
					ie_dig_verificador_w);
	exception
	when others then
		/*aaschlote 11/05/2011 OS - 317669*/
		sib_gravar_log_devolucao(nr_seq_lote_sib_p,'C',nr_seq_interno_w,'Erro ao importrar o arquivo: ' || sqlerrm(sqlcode),
					cd_estabelecimento_p,nm_usuario_p);
	end;
	commit;
	end;
end loop;
close C01;

commit;

end sib_importar_dev_cco;
/
