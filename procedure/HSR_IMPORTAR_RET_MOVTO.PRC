create or replace
procedure HSR_IMPORTAR_RET_MOVTO
		(nr_seq_retorno_p	number) is
		
cd_convenio_w		number(15);
nr_interno_conta_w	number(10);		
cd_autorizacao_w	varchar2(255);
cd_procedimento_w	number(15);
vl_pago_w		number(17,4);
nr_seq_item_conta_w	number(17,4);
cd_proc_mat_conta_w	number(17,4);
vl_cobrado_w		number(17,4);
qt_cobrada_w		number(9,3);

cursor C01 is
select	trim(substr(ds_conteudo,2,10)) cd_autorizacao,
	somente_numero(substr(ds_conteudo,19,8)) cd_procedimento,
	somente_numero(substr(ds_conteudo,45,12)) vl_pago,
	somente_numero(substr(ds_conteudo,40,4)) qt_cobrada
from	w_conv_ret_movto
where	nr_Seq_retorno	= nr_seq_retorno_p
and	substr(ds_conteudo,1,1) = 2;

begin

delete 	from CONVENIO_RET_MOVTO_HIST
where	nr_Seq_retorno	= nr_seq_retorno_p;
commit;

select	max(cd_convenio)
into	cd_convenio_w
from	convenio_retorno a
where	a.nr_sequencia	= nr_seq_retorno_p;

open C01;
loop
fetch	C01
into	cd_autorizacao_w,
	cd_procedimento_w,
	vl_pago_w,
	qt_cobrada_w;
exit when C01%notfound;
	begin
	
	cd_autorizacao_w	:= ltrim(cd_autorizacao_w,'0');
	
	select	max(b.nr_interno_conta)
	into	nr_interno_conta_w
	from	conta_paciente_guia b,
		conta_paciente c
	where	b.nr_interno_conta	= c.nr_interno_conta
	and	b.cd_autorizacao	= cd_autorizacao_w
	and	c.cd_convenio_parametro	= cd_convenio_w;
	
	select	max(a.nr_sequencia),
		max(a.cd_procedimento),
		nvl(max(a.vl_procedimento),0)
	into	nr_seq_item_conta_w,
		cd_proc_mat_conta_w,
		vl_cobrado_w
	from	procedimento_paciente a
	where	(a.cd_procedimento = cd_procedimento_w 	or a.cd_procedimento_convenio = cd_procedimento_w)
	and	nvl(a.nr_doc_convenio,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
	and	a.nr_interno_conta	= nr_interno_conta_w
	and	a.cd_motivo_exc_conta	is null;
		
	if	(nr_seq_item_conta_w	is null) then
	
		select	max(a.nr_sequencia),
			max(a.cd_material),
			nvl(max(a.vl_material),0)
		into	nr_seq_item_conta_w,
			cd_proc_mat_conta_w,
			vl_cobrado_w
		from	material_atend_paciente a
		where	(a.cd_material 				= somente_numero(cd_procedimento_w) or 
			a.cd_material_convenio 			= cd_procedimento_w or
			somente_numero(a.cd_material_tiss)	= somente_numero(cd_procedimento_w))
		and	nvl(a.nr_doc_convenio,'N�o Informada')	= nvl(cd_autorizacao_w,'N�o Informada')
		and	a.nr_interno_conta	= nr_interno_conta_w
		and	a.cd_motivo_exc_conta	is null;
	end if;	
	
		vl_pago_w	:= dividir(vl_pago_w,100);
		qt_cobrada_w	:= dividir(qt_cobrada_w,100);
		
		insert 	into convenio_retorno_movto
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_Seq_retorno,
		nr_doc_convenio,
		cd_item,
		vl_pago,
		vl_total_pago,
		nr_conta,
		nr_seq_item_conta,
		vl_cobrado,
		qt_cobrada)
	values	(convenio_retorno_movto_seq.nextval,
		Sysdate,
		'Tasy',
		nr_Seq_retorno_p,
		cd_autorizacao_w,
		cd_procedimento_w,
		vl_pago_w,
		vl_pago_w,
		nr_interno_conta_w,
		nr_seq_item_conta_w,
		vl_cobrado_w,
		qt_cobrada_w);
	end;
end loop;
close C01;

delete 	from w_conv_ret_movto
where	nr_Seq_retorno	= nr_seq_retorno_p;
commit;

end 	HSR_IMPORTAR_RET_MOVTO;
/