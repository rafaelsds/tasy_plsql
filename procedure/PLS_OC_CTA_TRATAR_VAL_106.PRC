create or replace
procedure pls_oc_cta_tratar_val_106(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_selecao_ocor_cta.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

qt_cnt_w		pls_integer;
i			pls_integer;
tb_seq_selecao_w	dbms_sql.number_table;
tb_observacao_w		dbms_sql.varchar2_table;
tb_valido_w		dbms_sql.varchar2_table;
ds_observacao_w		varchar2(4000);
					
Cursor C01 (nr_seq_oc_cta_comb_pc	pls_oc_cta_combinada.nr_sequencia%type) is
	select	ie_valida_medico
	from	pls_oc_cta_val_aft_coop a
	where	a.nr_seq_oc_cta_comb = nr_seq_oc_cta_comb_pc;
	
Cursor C02 (	nr_id_transacao_pc		pls_selecao_ocor_cta.nr_id_transacao%type,
		ie_valida_medico_pc		pls_oc_cta_val_aft_coop.ie_valida_medico%type) is
	select	sel.nr_sequencia nr_seq_selecao,
		conta.cd_medico_executor cd_medico,
		null cd_procedimento,
		(select	count(1)
		 from	pls_cooperado a,
			pls_cooperado_ausencia b
		 where	a.nr_sequencia = b.nr_seq_cooperado
		 and	a.cd_pessoa_fisica = conta.cd_medico_executor
		 and	a.cd_pessoa_fisica is not null
		 and	conta.dt_atendimento_referencia between b.dt_inicio and nvl(b.dt_fim,conta.dt_atendimento_referencia)) qt_coop
	from	pls_selecao_ocor_cta sel,
		pls_conta conta
	where	sel.nr_seq_conta 	= conta.nr_sequencia
	and	sel.nr_id_transacao 	= nr_id_transacao_pc
	and	sel.ie_valido 		= 'S'
	and	ie_valida_medico_pc 	= 'ME'
	union all
	select	sel.nr_sequencia nr_seq_selecao,
		conta.cd_medico_solicitante cd_medico,
		null cd_procedimento,
		(select	count(1)
		 from	pls_cooperado a,
			pls_cooperado_ausencia b
		 where	a.nr_sequencia = b.nr_seq_cooperado
		 and	a.cd_pessoa_fisica = conta.cd_medico_solicitante
		 and	a.cd_pessoa_fisica is not null
		 and	conta.dt_atendimento_referencia between b.dt_inicio and nvl(b.dt_fim,conta.dt_atendimento_referencia)) qt_coop
	from	pls_selecao_ocor_cta sel,
		pls_conta conta
	where	sel.nr_seq_conta 	= conta.nr_sequencia
	and	sel.nr_id_transacao 	= nr_id_transacao_pc
	and	sel.ie_valido 		= 'S'
	and	ie_valida_medico_pc 	= 'MS'
	union all
	select	sel.nr_sequencia nr_seq_selecao,
		partic.cd_medico,
		proc.cd_procedimento,
		(select	count(1)
		 from	pls_cooperado a,
			pls_cooperado_ausencia b
		 where	a.nr_sequencia = b.nr_seq_cooperado
		 and	a.cd_pessoa_fisica = partic.cd_medico
		 and	a.cd_pessoa_fisica is not null
		 and	proc.dt_procedimento_referencia between b.dt_inicio and nvl(b.dt_fim,conta.dt_atendimento_referencia)) qt_coop
	from	pls_selecao_ocor_cta sel,
		pls_conta conta,
		pls_conta_proc proc,
		pls_proc_participante partic
	where	sel.nr_seq_conta	= conta.nr_sequencia
	and	proc.nr_seq_conta 	= conta.nr_sequencia
	and	proc.nr_sequencia 	= partic.nr_seq_conta_proc
	and	sel.nr_id_transacao 	= nr_id_transacao_pc
	and	sel.ie_valido 		= 'S'
	and	ie_valida_medico_pc 	= 'MP';

begin
-- Somente valida se tiver regra cadastrada
if	(dados_regra_p.nr_sequencia is not null) then

	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
	
	--Grava a quantidade de registro por transa��o
	qt_cnt_w := pls_cta_consistir_pck.qt_registro_transacao_w;
	
	-- Inicializa as vari�veis
	tb_seq_selecao_w	:= pls_tipos_ocor_pck.num_table_vazia;
	tb_observacao_w		:= pls_tipos_ocor_pck.vchr2_table_vazia;
	tb_valido_w		:= pls_tipos_ocor_pck.vchr2_table_vazia;
	i			:= 0;
	
	for r_C01_w in C01 (dados_regra_p.nr_sequencia) loop
		
		for r_C02_w in C02 (nr_id_transacao_p, r_C01_w.ie_valida_medico) loop
			
			if	(r_C02_w.qt_coop > 0) then
			
				ds_observacao_w := '';
				
				if	(r_C01_w.ie_valida_medico = 'ME') then
					ds_observacao_w := 'Atendimento realizado durante per�odo de afastamento do m�dico executor da conta. ' || pls_util_pck.enter_w ||
							   'M�dico executor: ' || r_C02_w.cd_medico || '.';
				elsif	(r_C01_w.ie_valida_medico = 'MS') then
					ds_observacao_w := 'Atendimento realizado durante per�odo de afastamento do m�dico solicitante da conta. ' || pls_util_pck.enter_w ||
							   'M�dico solicitante: ' || r_C02_w.cd_medico || '.';
				elsif	(r_C01_w.ie_valida_medico = 'MP') then
					ds_observacao_w := 'Atendimento realizado durante per�odo de afastamento do m�dico participante. ' || pls_util_pck.enter_w ||
							   'Procedimento: ' || r_C02_w.cd_procedimento || '.' || pls_util_pck.enter_w ||
							   'M�dico participante: ' || r_C02_w.cd_medico || '.';
				end if;
				
				tb_seq_selecao_w(i) := r_C02_w.nr_seq_selecao;
				tb_observacao_w(i) := ds_observacao_w;
				tb_valido_w(i) := 'S';
				
				if	(tb_seq_selecao_w.count > qt_cnt_w) then
					pls_tipos_ocor_pck.gerencia_selecao_validacao(	tb_seq_selecao_w,
											pls_tipos_ocor_pck.clob_table_vazia,
											'SEQ',
											tb_observacao_w,
											tb_valido_w,
											nm_usuario_p);
												
					tb_seq_selecao_w	:= pls_tipos_ocor_pck.num_table_vazia;
					tb_observacao_w		:= pls_tipos_ocor_pck.vchr2_table_vazia;
					tb_valido_w		:= pls_tipos_ocor_pck.vchr2_table_vazia;
					i			:= 0;
				else
					i := i + 1;				
				end if;
			end if;
		end loop;
		
		if	(tb_seq_selecao_w.count > 0) then		
			pls_tipos_ocor_pck.gerencia_selecao_validacao(	tb_seq_selecao_w,
									pls_tipos_ocor_pck.clob_table_vazia,
									'SEQ',
									tb_observacao_w,
									tb_valido_w,
									nm_usuario_p);
										
			tb_seq_selecao_w	:= pls_tipos_ocor_pck.num_table_vazia;
			tb_observacao_w		:= pls_tipos_ocor_pck.vchr2_table_vazia;
			tb_valido_w		:= pls_tipos_ocor_pck.vchr2_table_vazia;
			i			:= 0;
		end if;
	end loop;
	
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
	
end if;

end pls_oc_cta_tratar_val_106;
/