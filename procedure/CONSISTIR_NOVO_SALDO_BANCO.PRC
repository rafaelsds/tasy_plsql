create or replace
procedure CONSISTIR_NOVO_SALDO_BANCO(
			dt_referencia_p		date) is 			
begin

if (dt_referencia_p is not null) then
	
	if ( trunc(dt_referencia_p,'dd') <> trunc(dt_referencia_p,'month')  ) then
		wheb_mensagem_pck.exibir_mensagem_abort(322195);
	end if;
end if;

end consistir_novo_saldo_banco;
/
