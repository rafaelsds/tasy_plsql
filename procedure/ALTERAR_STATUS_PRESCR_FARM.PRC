create or replace
procedure alterar_status_prescr_farm(	nr_prescricao_p	number,
					nr_seq_status_p	number,
					nm_usuario_p	varchar2 ) is 

begin

if	(nr_prescricao_p > 0) and (nr_seq_status_p > 0) then
	update	prescr_medica
	set	nm_usuario		=	nm_usuario_p,
		dt_atualizacao		=	sysdate,
		nr_seq_status_farm	=	nr_seq_status_p
	where	nr_prescricao		=	nr_prescricao_p;
	commit;
end if;

end alterar_status_prescr_farm;
/