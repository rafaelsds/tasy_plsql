create or replace
procedure enviar_sms_autorizacao(
			ds_remetente_p          varchar2,
			ds_destinatario_p       varchar2,
			ds_mensagem_p           varchar2,
			nm_usuario_p            varchar2) is			

id_sms_w        number(10);

begin

if	((ds_remetente_p is not null) and
	(ds_destinatario_p is not null) and
        (ds_mensagem_p is not null) and
        (nm_usuario_p is not null)) then	
	begin
	
	wheb_sms.enviar_sms(ds_remetente_p, ds_destinatario_p, ds_mensagem_p, nm_usuario_p,id_sms_w);
	
        insert into log_envio_sms (
                nr_sequencia,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                dt_atualizacao,
                nm_usuario,
                dt_envio,
                cd_agenda,
                nr_seq_agenda,
                nr_telefone,
                ds_mensagem,
                id_sms)
        values (
                log_envio_sms_seq.nextval,
                sysdate,
                nm_usuario_p,
                sysdate,
                nm_usuario_p,
                sysdate,
                null,
                null,
                ds_destinatario_p,
                ds_mensagem_p,
                id_sms_w);
	end;
end if;

commit;

end enviar_sms_autorizacao;
/