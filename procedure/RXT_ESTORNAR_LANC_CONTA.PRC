create or replace
procedure rxt_estornar_lanc_conta
			(	nr_seq_rxt_tratamento_p		number,
				nr_seq_rxt_agenda_p		number,
				nm_usuario_p			varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
	Estornar procedimentos da conta, que tenham sido gerados a partir de um tratamento ou agenda
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_proc_gerada_w		procedimento_paciente.nr_sequencia%type;
nr_atendimento_w		rxt_tumor.nr_atendimento%type;
cd_convenio_w			atend_categoria_convenio.cd_convenio%type;

cursor C01 is
	select	nr_sequencia,
			nr_seq_propaci
	from	rxt_tratamento_proced
	where	nr_seq_tratamento	= nr_seq_rxt_tratamento_p
	and		ie_situacao		= 'A'
	and		(select	count(*)
			from	rxt_agenda x
			where	x.nr_seq_tratamento = nr_seq_rxt_tratamento_p
			and		x.ie_status_agenda = 'E') = 0
	union
	select	null,
			c.nr_sequencia nr_seq_propaci
	from	rxt_agenda		b,
			procedimento_paciente	c
	where	b.nr_sequencia	= c.nr_seq_agenda_rxt
	and		b.nr_sequencia	= nr_seq_rxt_agenda_p;

begin
if	(nr_seq_rxt_agenda_p is null) then
	select	max(a.nr_atendimento)
	into	nr_atendimento_w
	from	rxt_tumor	a,
		rxt_tratamento	b
	where	a.nr_sequencia	= b.nr_seq_tumor
	and	b.nr_sequencia	= nr_seq_rxt_tratamento_p;
else
	select	max(nr_atendimento)
	into	nr_atendimento_w
	from	rxt_agenda
	where	nr_sequencia = nr_seq_rxt_agenda_p;
end if;


cd_convenio_w	:= obter_convenio_atendimento(nr_atendimento_w);

for	row_C01 in C01 loop
	
	Duplicar_Proc_Paciente(	row_C01.nr_seq_propaci,
				nm_usuario_p,
				nr_seq_proc_gerada_w);			
	
	if	(nr_seq_proc_gerada_w is not null) then
		update	procedimento_paciente 
		set	nr_interno_conta 	= null,
			qt_devolvida		= qt_devolvida			* -1,
			qt_filme		= qt_filme			* -1,
			qt_porte_anestesico	= qt_porte_anestesico		* -1,
			qt_procedimento		= qt_procedimento		* -1,
			vl_adic_plant		= vl_adic_plant			* -1,
			vl_anestesista		= vl_anestesista		* -1,
			vl_auxiliares		= vl_auxiliares			* -1,
			vl_custo_operacional	= vl_custo_operacional		* -1,
			vl_desp_tiss		= vl_desp_tiss			* -1,
			vl_materiais		= vl_materiais			* -1,
			vl_medico		= vl_medico			* -1,
			vl_original_tabela	= vl_original_tabela		* -1,
			vl_procedimento		= vl_procedimento		* -1,
			vl_tx_adm		= vl_tx_adm			* -1,
			vl_tx_desconto		= vl_tx_desconto		* -1
		where	nr_sequencia		= nr_seq_proc_gerada_w;
	
		update	procedimento_paciente
		set	nr_seq_proc_est	= nr_seq_proc_gerada_w
		where	nr_sequencia	= row_C01.nr_seq_propaci;

		update	rxt_tratamento_proced 
		set	ie_situacao	= 'I'
		where	nr_sequencia	= row_C01.nr_sequencia;
		
		atualiza_preco_procedimento(	nr_seq_proc_gerada_w,
						cd_convenio_w,
						nm_usuario_p);
	end if;

	commit;
	
end loop;

commit;

end rxt_estornar_lanc_conta;
/
