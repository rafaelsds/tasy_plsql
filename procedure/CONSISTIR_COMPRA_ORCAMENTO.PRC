create or replace
procedure consistir_compra_orcamento(		nr_ordem_compra_p	number,
					nm_usuario_p		varchar2,
					ie_mensal_p   varchar2 default 'S',
					ie_anual_p	  varchar2 default 'N') is 

nr_seq_mes_ref_w			number(10);
cd_estabelecimento_w		number(10);
vl_empenhado_w			number(15,4);
vl_orcado_w			number(15,4);
vl_liquido_w			number(15,4);
vl_diferenca_w			number(15,4);
cd_conta_contabil_w		varchar2(20);
cd_centro_custo_w			number(8);
ds_conta_contabil_w		varchar2(255);
ds_centro_custo_w			varchar2(255);
ds_observacao_w			varchar2(4000);
nr_seq_texto_w			dic_objeto.nr_sequencia%type;
ie_mensal_w         varchar2(5);
ie_anual_w          varchar2(5);
					
cursor c01 is
select	b.cd_centro_custo,
	b.cd_conta_contabil,
	substr(obter_desc_centro_custo(b.cd_centro_custo),1,255),
	substr(obter_desc_conta_contabil(b.cd_conta_contabil),1,255),
	nvl(sum(b.vl_item_liquido),0)
from	ordem_compra_item b
where	b.nr_ordem_compra = nr_ordem_compra_p
group by b.cd_centro_custo,
	b.cd_conta_contabil;

begin

delete from w_consist_orcamento_compra
where	ie_funcao = 'OC'
and	nr_documento = nr_ordem_compra_p;

select	nvl(max(nr_sequencia),0)
into	nr_seq_mes_ref_w
from	ctb_mes_ref
where	trunc(dt_referencia,'month') = trunc(sysdate,'month');

if (ie_mensal_p is null and ie_anual_p is null) then
  ie_mensal_w := 'S';
  ie_anual_w := 'N';
else
  ie_mensal_w := ie_mensal_p;
  ie_anual_w := ie_anual_p;
end if;

if	(nr_seq_mes_ref_w > 0) then
	begin
	
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_p;
	
	open C01;
	loop
	fetch C01 into	
		cd_centro_custo_w,
		cd_conta_contabil_w,
		ds_centro_custo_w,
		ds_conta_contabil_w,
		vl_liquido_w;		
	exit when C01%notfound;
		begin
		
		select	nvl(ctb_obter_empenho_orcamento(nr_seq_mes_ref_w, cd_conta_contabil_w, cd_centro_custo_w, cd_estabelecimento_w),0)  vl_empenhado
		  into	vl_empenhado_w
		  from	dual;
		  
		For aux in 1..2 loop 
		
			vl_orcado_w := 0;
			vl_diferenca_w := 0;
		
			if (ie_mensal_w = 'S' and aux = 1) then
				
				select	nvl(ctb_obter_valor_orcado(trunc(sysdate,'month'), cd_conta_contabil_w, cd_centro_custo_w, cd_estabelecimento_w),0) vl_orcado
				  into vl_orcado_w
				  from	dual;
			
				nr_seq_texto_w := 298766;
			end if;
			
			if (ie_anual_w = 'S' and aux = 2) then
			
				select sum(nvl(ctb_obter_valor_orcado(trunc(which_month,'month'), cd_conta_contabil_w, cd_centro_custo_w, cd_estabelecimento_w),0)) 
				  into vl_orcado_w
				  from (select add_months(trunc(sysdate, 'YEAR'), rownum-1) which_month
						  from dic_objeto
						 where rownum <= months_between(pkg_date_utils.end_of(sysdate, 'YEAR',0), add_months(pkg_date_utils.start_of(sysdate, 'YEAR',0), -1))
						 order by which_month);
						 
				nr_seq_texto_w := 1114712;
			
			end if;
			
			if	(vl_orcado_w > 0) then
				vl_diferenca_w	:= vl_orcado_w - vl_empenhado_w - vl_liquido_w;
			else
				vl_diferenca_w := 0;
			end if;

			if	(vl_diferenca_w < 0) then 
			
				ds_observacao_w	:=	substr(	WHEB_MENSAGEM_PCK.get_texto(nr_seq_texto_w,
							'DS_CONTA_CONTABIL_W='||DS_CONTA_CONTABIL_W||
							';DS_CENTRO_CUSTO_W='||DS_CENTRO_CUSTO_W||
							';VL_ORCADO_W='||campo_mascara_virgula_casas(vl_orcado_w,4)||
							';VL_EMPENHADO_W='||campo_mascara_virgula_casas(vl_empenhado_w,4)||
							';VL_LIQUIDO_W='||campo_mascara_virgula_casas(vl_liquido_w,4)),1,4000);
								
							/*Atingiu o valor maximo orcado para essa conta contabil e para esse centro de custo.
							Conta contabil: #@DS_CONTA_CONTABIL_W#@
							Centro custo: #@DS_CENTRO_CUSTO_W#@
							Valor orcado: #@VL_ORCADO_W#@
							Valor empenhado: #@VL_EMPENHADO_W#@
							Valor compra: #@VL_LIQUIDO_W#@*/

				
				insert into w_consist_orcamento_compra(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					ie_funcao,
					nr_documento,
					ds_consistencia,
					ds_observacao)
				values(	w_consist_orcamento_compra_seq.nextval,
					sysdate,
					nm_usuario_p,
					'OC',
					nr_ordem_compra_p,
					Wheb_mensagem_pck.get_Texto(298767),
					ds_observacao_w);
			end if;
		end loop;
		end;
	end loop;
	close C01;	
	end;
end if;

commit;

end consistir_compra_orcamento;
/