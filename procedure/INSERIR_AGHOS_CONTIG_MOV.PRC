create or replace
procedure inserir_aghos_contig_mov(	nm_usuario_p		varchar2,
					ds_erro_p		varchar2,
					nr_atendimento_p	integer,
					nr_internacao_p		integer,
					nr_seq_atepacu_p	integer,
					cd_setor_origem_p	integer) is 

begin

insert into aghos_contigencia_mov (nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					ds_erro,
					nr_atendimento,
					nr_internacao,
					nr_seq_atepacu,
					cd_setor_origem,
					dt_resolvida,
					nm_usuario_resol)
				values	(aghos_contigencia_mov_seq.nextval,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					ds_erro_p,
					nr_atendimento_p,
					nr_internacao_p,
					nr_seq_atepacu_p,
					cd_setor_origem_p,
					null,
					null);


if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end inserir_aghos_contig_mov;
/
