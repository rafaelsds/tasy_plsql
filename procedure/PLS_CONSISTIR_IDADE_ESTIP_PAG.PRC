create or replace
procedure pls_consistir_idade_estip_pag
		(	nr_seq_proposta_p	number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2,
			ds_erro_p	out	varchar2) is

cd_pagador_pf_w			varchar2(10);
cd_estipulante_pf_w		varchar2(10);
qt_idade_estip_w		number(10);
qt_idade_pagador_w		number(10);
ie_consistir_estipulante_w	varchar2(1);
ie_consistir_pagador_w		varchar2(1);
ds_erro_w			varchar2(4000);
ie_emancipado_w			varchar2(10);

Cursor C01 is
	select	cd_pagador
	from	pls_proposta_pagador
	where	nr_seq_proposta	= nr_seq_proposta_p
	and	cd_cgc_pagador is null;

begin

ie_consistir_estipulante_w	:= nvl(obter_valor_param_usuario(1232, 52, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');
ie_consistir_pagador_w		:= nvl(obter_valor_param_usuario(1232, 108, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p), 'N');

select	max(cd_estipulante)
into	cd_estipulante_pf_w
from	pls_proposta_adesao
where	nr_sequencia	= nr_seq_proposta_p;

if	(ie_consistir_estipulante_w	= 'S') and
	(cd_estipulante_pf_w is not null) then
	select	nvl(ie_emancipado,'N')
	into	ie_emancipado_w
	from	pessoa_fisica
	where	cd_pessoa_fisica	= cd_estipulante_pf_w;
	
	qt_idade_estip_w	:= obter_idade_pf(cd_estipulante_pf_w,sysdate,'A');
	if	(qt_idade_estip_w	< 18) and
		(ie_emancipado_w	= 'N') then
		ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280472, 'CD_ESTIPULANTE_P=' || obter_nome_pf(cd_estipulante_pf_w));
	end if;
end if;

if	(ie_consistir_pagador_w = 'S') then
	open C01;
	loop
	fetch C01 into
		cd_pagador_pf_w;
	exit when C01%notfound;
		begin
		select	nvl(ie_emancipado,'N')
		into	ie_emancipado_w
		from	pessoa_fisica
		where	cd_pessoa_fisica	= cd_pagador_pf_w;
		
		qt_idade_pagador_w	:= obter_idade_pf(cd_pagador_pf_w,sysdate,'A');
		if	(qt_idade_pagador_w	< 18) and
			(ie_emancipado_w	= 'N') then
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(280473, 'CD_PAGADOR_P=' || obter_nome_pf(cd_pagador_pf_w));
		end if;
		end;
	end loop;
	close C01;
end if;

ds_erro_p	:= ds_erro_w;

end pls_consistir_idade_estip_pag;
/