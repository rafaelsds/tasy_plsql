create or replace procedure importar_exame_lab_result(NR_PRESCRICAO_P number, NM_USUARIO_P varchar2, DT_RESULTADO_P date) is

nr_seq_resultado_w number(10);
begin

	select	max(a.nr_seq_resultado)
	into	nr_seq_resultado_w
	from	exame_lab_resultado a
	where	a.nr_prescricao = NR_PRESCRICAO_P;
		if	(nr_seq_resultado_w is null) then
			select	nvl(max(nr_seq_resultado),0) + 1
			into	nr_seq_resultado_w
			from	exame_lab_resultado;
		end if;

	insert into exame_lab_resultado(nr_seq_resultado, dt_resultado, dt_atualizacao, nm_usuario, nr_prescricao)
	values(nr_seq_resultado_w,nvl(DT_RESULTADO_P,sysdate), sysdate, NM_USUARIO_P, NR_PRESCRICAO_P);
	
	commit;

end importar_exame_lab_result;
/
