create or replace
procedure pls_gerar_cartao_santamalia
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is
type 	rc_titul is record	(nr_seq_titul_w			number(10));

type 	tb_titul is table of rc_titul index by pls_integer;


tb_titul_w	tb_titul;


nr_seq_segurado_w		number(10);
ds_header_w			varchar2(4000);
cd_usuario_plano_w		varchar2(30);
nm_beneficiario_w		varchar2(250);
tp_contratacao_w		varchar2(20);
dt_nascimento_w			varchar2(20);
ds_produto_w			varchar2(50);
ds_abrangencia_w		varchar2(50);
ds_acomodacao_w			varchar2(50);
nm_empresa_w			varchar2(50);
tp_contrato_w			varchar2(10);
ds_trilha_1_w			varchar2(79);
ds_trilha_2_w			varchar2(40);
nr_protocolo_ans_w		varchar2(100);
dt_contratacao_w		varchar2(20);
nm_titular_w			varchar2(255);
ds_estipulante_w		varchar2(255);
ie_segmentacao_w		varchar2(10);
ds_segmentacao_w		varchar2(255);
dt_validade_carteira_w		pls_segurado_carteira.dt_validade_carteira%type;
ds_carencia_ww			varchar2(255);
ds_dt_carencia_ww		varchar2(255);
nr_seq_beneficiario_w		varchar2(255);
ds_carencia_w			varchar2(4000);
ds_dt_carencia_w		varchar2(4000);
ds_benefic_w			varchar2(255);
nr_seq_plano_w			number(10);
nr_titul_count_w		number(10) := 1;
nr_dependentes_w		number(10);
nr_seq_intercambio_w		number(10);
nr_seq_contrato_w		number(10);
nr_titulares_w			number(10):= 0;
nr_dependnetes_w		number(10):= 0;
nr_total_lote_w			number(10):= 0;

Cursor C01 is
	select	b.nr_sequencia, -- Cursor para buscar todos os titulares do lote
		pls_obter_dados_segurado(b.nr_sequencia, 'N') nm_segurado 
	from	pls_segurado_carteira	a,
		pls_segurado		b,
		pls_carteira_emissao	c,
		pls_lote_carteira	d
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	c.nr_seq_seg_carteira	= a.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and 	(b.nr_seq_contrato is not null
	or	nr_seq_intercambio is not null)
	and 	b.nr_seq_titular is null 
	and	d.nr_sequencia		= nr_seq_lote_p
	order by nm_segurado;
	
Cursor C02 is
	select 	b.nr_sequencia, -- Cursor para buscar os dependentes dos titulares do lote
		pls_obter_dados_segurado(b.nr_sequencia, 'N') nm_segurado 
	from	pls_segurado_carteira	a,
		pls_segurado		b,
		pls_carteira_emissao	c,
		pls_lote_carteira	d
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	c.nr_seq_seg_carteira	= a.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_p
	and 	(b.nr_seq_contrato is not null
	or	nr_seq_intercambio is not null)
	and 	b.nr_seq_titular 	= nr_seq_segurado_w 
	order by	nm_segurado;

Cursor C03 is
	select	b.nr_sequencia,
		pls_obter_dados_segurado(b.nr_sequencia, 'N') nm_segurado
	from	pls_segurado_carteira	a,
		pls_segurado		b,
		pls_carteira_emissao	c,
		pls_lote_carteira	d
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	c.nr_seq_seg_carteira	= a.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and 	(b.nr_seq_contrato is not null
	or	nr_seq_intercambio is not null)
	and	d.nr_sequencia		= nr_seq_lote_p
	and nr_seq_titular is not null
	and not exists (select 	1
			from pls_segurado_carteira	z,
				pls_segurado		y,
				pls_carteira_emissao	g,
				pls_lote_carteira	p
			where	  z.nr_seq_segurado	    = y.nr_sequencia
			and	    g.nr_seq_seg_carteira	= z.nr_sequencia
			and	    g.nr_seq_lote		= p.nr_sequencia
			and	  p.nr_sequencia		= nr_seq_lote_p
			and   b.nr_seq_titular  = y.nr_sequencia)
	order by	nm_segurado;
begin

DS_HEADER_W	:= 	'MATRICULA;BENEFICIARIO;TARJA_TRILHA_1_CODIGO_FORMATADO;TARJA_TRILHA_2_CODIGO_INTERNO;TARJA_TRILHA_3_VS_CARTEIRINHA;TITULAR;PLANO;EMPRESA;DATA_DE_NASCIMENTO;'||
			'DT_VALIDADE_CARTEIRA;CONTRATACAO;DATA_DE_INICIO;ABRANGENCIA;SEGMENTACAO;CODIGO_PRODUTO_ANS;' ||'CARENCIA1;CARENCIA2;CARENCIA3;CARENCIA4;CARENCIA5;CARENCIA6;'||
			'CARENCIA7;ACOMODACAO;DCAR1;DCAR2;DCAR3;DCAR4;DCAR5;DCAR6;DCAR7';

delete	w_pls_interface_carteira
where	nr_seq_lote	= nr_seq_lote_p;

insert	into	w_pls_interface_carteira
	(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
		nr_seq_lote, ds_header, ie_tipo_reg)
values	(	w_pls_interface_carteira_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
		nr_seq_lote_p, ds_header_w, 1);

open C01;
loop
fetch C01 into
	nr_seq_segurado_w,
	ds_benefic_w;
exit when C01%notfound;
	begin
	tb_titul_w( nr_titul_count_w ).nr_seq_titul_w := nr_seq_segurado_w;
	nr_titul_count_w := nr_titul_count_w + 1;
	nr_titulares_w := nr_titulares_w +1;
	select 	count(*)
	into 	nr_dependentes_w
	from 	pls_segurado
	where 	nr_seq_titular = nr_seq_segurado_w;
	
	if (nr_dependentes_w > 0) then
		open C02;
		loop
		fetch C02 into
			nr_seq_beneficiario_w,
			ds_benefic_w;
		exit when C02%notfound;
			begin
			tb_titul_w(nr_titul_count_w).nr_seq_titul_w := nr_seq_beneficiario_w;
			nr_titul_count_w := nr_titul_count_w + 1;
			nr_dependnetes_w := nr_dependnetes_w + 1;
			end;
		end loop;
		close C02;
	end if;
	end;
end loop;
close C01;
select	 count (1)
into	nr_total_lote_w
from	pls_segurado_carteira	a,
	pls_segurado		b,
	pls_carteira_emissao	c,
	pls_lote_carteira	d
where	a.nr_seq_segurado	= b.nr_sequencia
and	c.nr_seq_seg_carteira	= a.nr_sequencia
and	c.nr_seq_lote		= d.nr_sequencia
and 	(b.nr_seq_contrato is not null
or	nr_seq_intercambio is not null)
and	d.nr_sequencia		= nr_seq_lote_p;

if 	(nr_total_lote_w > (nr_titulares_w + nr_dependnetes_w)) then
	open C03;
	loop
	fetch C03 into
		nr_seq_segurado_w,
		ds_benefic_w;
	exit when C03%notfound;
	begin
		tb_titul_w(nr_titul_count_w).nr_seq_titul_w := nr_seq_segurado_w;
				nr_titul_count_w := nr_titul_count_w + 1;
	end;
	end loop;
	close C03;
end if;


for x in 1..tb_titul_w.count  loop 
	begin
	select	c.cd_usuario_plano,
		substr(a.nm_pessoa_fisica,1,60),
		replace(replace(ds_trilha1,'%',''),'?',''),
		replace(replace(ds_trilha2,';',''),'?',''),
		decode(b.nr_seq_titular,null,nm_pessoa_fisica,substr(pls_obter_dados_segurado(b.nr_sequencia,'NT'),1,255)),
		substr(d.nm_fantasia,1,17),
		nvl(b.nr_seq_intercambio,0),
		nvl(b.nr_seq_contrato,0),
		to_char(a.dt_nascimento,'dd/mm/yyyy'),
		decode(D.ie_tipo_contratacao,'I','INDIVIDUAL FAMILIAR','CA','COLETIVO ADES�O','CE','COLETIVO EMPRESARIAL'),
		to_char(b.dt_contratacao,'dd/mm/yyyy'),
		decode(D.ie_abrangencia,'GM','GRUPO DE MUNIC�PIOS','E','ESTADUAL','GE','GRUPO DE ESTADOS','M','MUNICIPAL','N','NACIONAL'),
		d.ie_segmentacao,
		d.nr_protocolo_ans,
		substr(nvl(pls_obter_dados_cart_unimed(b.nr_sequencia,d.nr_sequencia,'DASJ',1),'N�O SE APLICA'),1,50),
		d.nr_sequencia,
		c.dt_validade_carteira
	into	cd_usuario_plano_w,
		nm_beneficiario_w,
		ds_trilha_1_w,
		ds_trilha_2_w,
		nm_titular_w,
		ds_produto_w,
		nr_seq_intercambio_w,
		nr_seq_contrato_w,
		dt_nascimento_w,
		tp_contratacao_w,
		dt_contratacao_w,
		ds_abrangencia_w,
		ie_segmentacao_w,
		nr_protocolo_ans_w,
		ds_acomodacao_w,
		nr_seq_plano_w,
		dt_validade_carteira_w
	from	pls_plano		d,
		pls_segurado_carteira	c,
		pls_segurado		b,
		pessoa_fisica		a
	where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	c.nr_seq_segurado	= b.nr_sequencia
	and	b.nr_seq_plano		= d.nr_sequencia
	and	b.nr_sequencia		= tb_titul_w(x).nr_seq_titul_w;	
	
	if 	(nr_seq_intercambio_w <> 0 )then
		select 	decode (cd_cgc,null,'','Santam�lia Sa�de',pls_obter_dados_contrato_inter(nr_seq_intercambio_w, null, 'E'))
		into	ds_estipulante_w
		from	pls_intercambio
		where 	nr_sequencia 	= nr_seq_intercambio_w;
	elsif 	(nr_seq_contrato_w <> 0 )then
		select 	decode(cd_cgc_estipulante,'','Santam�lia Sa�de',substr(obter_nome_pf_pj('',cd_cgc_estipulante),1,255))
			into	ds_estipulante_w
			from 	pls_contrato
			where 	nr_sequencia = nr_seq_contrato_w ;
	end if;
	
	ds_acomodacao_w		:= upper(ds_acomodacao_w);
	
	ds_abrangencia_w	:= replace(ds_abrangencia_w,'�','I');
	
	if	(ie_segmentacao_w = '1') then
		ds_segmentacao_w	:= 'AMB';
	elsif	(ie_segmentacao_w = '1') then
		ds_segmentacao_w	:= 'HOSP+OBST';
	elsif	(ie_segmentacao_w = '3') then
		ds_segmentacao_w	:= 'HOSP';
	elsif	(ie_segmentacao_w = '4') then
		ds_segmentacao_w	:= 'ODONT';
	elsif	(ie_segmentacao_w = '5') then
		ds_segmentacao_w	:= 'REF+AMB+HOSP+OBST';
	elsif	(ie_segmentacao_w = '6') then
		ds_segmentacao_w	:= 'AMB+HOSP+OBST';
	elsif	(ie_segmentacao_w = '7') then
		ds_segmentacao_w	:= 'AMB+HOSP';
	elsif	(ie_segmentacao_w = '8') then
		ds_segmentacao_w	:= 'AMB+ODONT';
	elsif	(ie_segmentacao_w = '9') then
		ds_segmentacao_w	:= 'HOSP+OBST+ODONT';
	elsif	(ie_segmentacao_w = '10') then
		ds_segmentacao_w	:= 'HOSP+ODONT';
	elsif	(ie_segmentacao_w = '11') then
		ds_segmentacao_w	:= 'AMB+HOSP+OBST+ODONT';
	elsif	(ie_segmentacao_w = '12') then
		ds_segmentacao_w	:= 'AMB+HOSP+ODONT';
	end if;
	
	ds_carencia_w		:= '';
	ds_dt_carencia_w	:= '';
	
	for i in 1..7 loop
		begin
		ds_carencia_ww		:= substr(pls_obter_dados_cart_unimed(tb_titul_w(x).nr_seq_titul_w , nr_seq_plano_w,'CAM5',i),1,255);
		ds_dt_carencia_ww	:= substr(ds_carencia_ww,instr(ds_carencia_ww,';')+1,length(ds_carencia_ww));
		ds_carencia_ww		:= substr(ds_carencia_ww,1,instr(ds_carencia_ww,';')-1);
		
		if	(UPPER(ds_dt_carencia_ww) = 'IMEDIATO') THEN
			ds_dt_carencia_ww	:= 'N�O H�';
		end if;
		
		if	(i < 7) then
			ds_carencia_w		:= ds_carencia_w ||ds_carencia_ww||';';
			ds_dt_carencia_w	:= ds_dt_carencia_w || ds_dt_carencia_ww||';';
		else
			ds_carencia_w	:= ds_carencia_w ||ds_carencia_ww;
			ds_dt_carencia_w	:= ds_dt_carencia_w || ds_dt_carencia_ww;
		end if;
		end;
	end loop;
	
	insert	into	w_pls_interface_carteira
		(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
			nr_seq_lote, ie_tipo_reg, cd_usuario_plano, nm_beneficiario, ds_trilha_1,
			ds_trilha_2, nm_titular, nm_plano, ds_estipulante, dt_nascimento,
			ds_tipo_contratacao, dt_adesao, ds_abrangencia, ds_segmentacao, nr_protocolo_ans,
			ds_acomodacao, nr_seq_segurado, ds_carencia, ds_dt_carencia, dt_validade_carteira )
	values	(	w_pls_interface_carteira_seq.nextval, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
			nr_seq_lote_p, 2, cd_usuario_plano_w, nm_beneficiario_w, ds_trilha_1_w,
			ds_trilha_2_w, nm_titular_w, ds_produto_w, ds_estipulante_w, dt_nascimento_w,
			tp_contratacao_w, dt_contratacao_w, ds_abrangencia_w, ds_segmentacao_w, nr_protocolo_ans_w,
			ds_acomodacao_w, tb_titul_w(x).nr_seq_titul_w , ds_carencia_w, ds_dt_carencia_w, dt_validade_carteira_w );
	end;
end loop;

commit;

end pls_gerar_cartao_santamalia;
/
