create or replace procedure Gerar_Agenda_Motivo_Log
		(CD_AGENDA_p		number,
		cd_agenda_origem_p	number,
		nr_seq_motivo_p		number,
		DT_AGENDA_ORIGEM_p	date,
		DT_AGENDA_DESTINO_p	Date,
		NM_PACIENTE_p           varchar2,
		CD_CONVENIO_p           number, 
		CD_PROCEDIMENTO_p       number, 
		IE_ORIGEM_PROCED_p      number, 
		CD_MEDICO_p             varchar2,
		cd_pessoa_fisica_p	varchar2,
		nr_seq_agenda_orig_p	number,
		nm_usuario_p		varchar2) is

nr_seq_log_w		number(10,0);
cd_agenda_w		number(10,0)	:= null;
cd_convenio_w		number(10,0)	:= null;
cd_procedimento_w	number(15,0)	:= null;
ie_origem_proced_w	number(10,0)	:= null;
cd_medico_w		varchar2(10)	:= null;
nm_paciente_w		varchar2(80)	:= null;
cd_pessoa_fisica_w	varchar2(10)	:= null;
nm_pessoa_fisica_w	varchar2(80)	:= null;
cd_tipo_agenda_w	number(10,0);
nr_telefone_w		varchar2(80);

begin

select	nvl(max(cd_tipo_agenda),0)
into	cd_tipo_agenda_w
from	agenda
where	cd_agenda	= cd_agenda_p;

if	(nr_seq_agenda_orig_p > 0) then
	begin
	if	(cd_tipo_agenda_w <> 3) then
		select	max(cd_agenda),
			max(cd_convenio),
			max(cd_procedimento),
			max(ie_origem_proced),
			max(cd_medico),
			max(nm_paciente),
			max(cd_pessoa_fisica),
			substr(max(nr_telefone),1,80)
		into	cd_agenda_w,
			cd_convenio_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			cd_medico_w,
			nm_paciente_w,
			cd_pessoa_fisica_w,
			nr_telefone_w
		from	agenda_paciente
		where	nr_sequencia	= nr_seq_agenda_orig_p;
	else
		select	max(cd_agenda),
			max(cd_convenio),
			max(nm_paciente),
			max(cd_pessoa_fisica),
			max(nr_telefone)
		into	cd_agenda_w,
			cd_convenio_w,
			nm_paciente_w,
			cd_pessoa_fisica_w,
			nr_telefone_w
		from	agenda_consulta
		where	nr_sequencia	= nr_seq_agenda_orig_p;
	end if;
	end;
end if;	


select	MAX(SUBSTR(OBTER_NOME_PF(CD_PESSOA_FISICA),1,60))--max(nm_pessoa_fisica)
into	nm_pessoa_fisica_w
from	pessoa_fisica
where	cd_pessoa_fisica	= nvl(cd_pessoa_fisica_w, cd_pessoa_fisica_p);


select	agenda_motivo_log_seq.nextval
into	nr_seq_log_w
from 	dual;

insert	into agenda_motivo_log(
	NR_SEQUENCIA           ,
	CD_AGENDA              ,
	DT_ACAO                ,
	DT_AGENDA_ORIGEM       ,
	DT_ATUALIZACAO         ,
	NM_USUARIO             ,
	DT_AGENDA_DESTINO      ,
	NM_PACIENTE            ,
	CD_CONVENIO            ,
	CD_PROCEDIMENTO        ,
	IE_ORIGEM_PROCED       ,
	CD_MEDICO              ,
	nr_seq_motivo,
	cd_agenda_destino,
	nr_telefone)
values
	(nr_seq_log_w,
	nvl(cd_agenda_w, CD_AGENDA_p),
	sysdate,
	DT_AGENDA_ORIGEM_p,
	sysdate,
	nm_usuario_p	,
	DT_AGENDA_DESTINO_p,
	substr(nvl(nm_pessoa_fisica_w, nvl(nm_paciente_W, NM_PACIENTE_p)),1,40),
	nvl(cd_convenio_w, CD_CONVENIO_p),
	nvl(cd_procedimento_w, CD_PROCEDIMENTO_p),
	nvl(IE_ORIGEM_PROCED_w, IE_ORIGEM_PROCED_p),
	nvl(cd_medico_w, CD_MEDICO_p), nr_seq_motivo_p, cd_agenda_p, nr_telefone_w);

commit;

end Gerar_Agenda_Motivo_Log;
/