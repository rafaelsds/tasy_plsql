create or replace
procedure scola_alterar_convenio_atend (	nr_atendimento_p	number,						
				cd_convenio_p		number,
				cd_categoria_p		varchar2,
				cd_plano_convenio_p	varchar2,
				dt_inicio_vigencia_p	date,					
				cd_usuario_convenio_p	varchar2,
				dt_validade_carteira_p	date,						
				cd_erro_p		out number,
				ds_erro_p			out varchar2) is 

ie_existe_convenio_w	varchar2(1);
ie_existe_categoria_w	varchar2(1);
ie_existe_plano_w	varchar2(1);
nr_seq_convenio_w	number(10);
						
/* Lista de erros
0 - SUCESSO
1 - Par�metro n�o informado
	1.1 - CD_CONVENIO_P n�o informado.
	1.2 - CD_CATEGORIA_P n�o informado.
	1.3 - CD_PLANO_CONVENIO_P n�o informado.
	1.4 - DT_INICIO_VIGENCIA_P n�o informado.
2 - Informa��o sem cadastro	
	2.1 - CD_CONVENIO_P (Este conv�nio n�o esta cadastrado).
	2.2 - CD_CATEGORIA_P (Categoria n�o cadastrada para este conv�nio).
	2.3 - CD_PLANO_CONVENIO_P (Plano do conv�nio n�o esta cadastrado).
*/
						
begin
cd_erro_p	:= 0;

if	(cd_convenio_p is null) then
	cd_erro_p := 1.1;
	ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(277999,null);
elsif	(cd_categoria_p is null) then
	cd_erro_p := 1.2;
	ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(278001,null);
elsif	(cd_plano_convenio_p is null) then
	cd_erro_p := 1.3;
	ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(278002,null);
elsif	(dt_inicio_vigencia_p is null) then
	cd_erro_p := 1.4;
	ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(278003,null);
end if;

if	(cd_erro_p = 0) then
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_convenio_w
	from	convenio a
	where	ie_situacao = 'A'
	and	a.cd_convenio = cd_convenio_p;
	
	select	decode(count(*),0,'N','S')
	into	ie_existe_categoria_w
	from	categoria_convenio a
	where	ie_situacao = 'A'
	and	a.cd_convenio = cd_convenio_p
	and	a.cd_categoria = cd_categoria_p;
		
	select	decode(count(*),0,'N','S')
	into	ie_existe_plano_w
	from	convenio_plano a
	where	ie_situacao = 'A'
	and	a.cd_convenio = cd_convenio_p
	and	a.cd_plano = cd_plano_convenio_p;
	
	if	(ie_existe_convenio_w = 'N') then
		cd_erro_p := 2.1;
		ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(278028,'CD_CONVENIO='||to_char(cd_convenio_p));
	elsif	(ie_existe_categoria_w = 'N') then
		cd_erro_p := 2.2;
		ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(278029,'CD_CATEGORIA='||to_char(cd_categoria_p));
	elsif	(ie_existe_plano_w = 'N') then
		cd_erro_p := 2.3;
		ds_erro_p := WHEB_MENSAGEM_PCK.get_texto(278030,'CD_PLANO_CONVENIO='||to_char(cd_plano_convenio_p));
	end if;
	
	if (cd_erro_p = 0) then		
		begin
		
		select	nvl(max(nr_seq_interno),0)
		into	nr_seq_convenio_w
		from	atend_categoria_convenio
		where	nr_atendimento = nr_atendimento_p;
		
		if	(nr_seq_convenio_w > 0) then
			update	atend_categoria_convenio
			set	dt_final_vigencia = (dt_inicio_vigencia_p-(1/86400)),	
				nm_usuario = 'SCOLA'
			where	nr_seq_interno = nr_seq_convenio_w
			and	dt_final_vigencia is null;
		end if;
		
		insert into atend_categoria_convenio 
						(nr_seq_interno,
						nr_atendimento,	
						cd_convenio,
						cd_categoria,
						cd_plano_convenio,
						dt_inicio_vigencia,
						cd_usuario_convenio,
						dt_validade_carteira,
						dt_atualizacao,
						nm_usuario)
				values	(	atend_categoria_convenio_seq.NextVal,
						nr_atendimento_p,
						cd_convenio_p,
						cd_categoria_p,
						cd_plano_convenio_p,
						dt_inicio_vigencia_p,
						cd_usuario_convenio_p,
						dt_validade_carteira_p,
						sysdate,
						'SCOLA');	
						
		commit;
		
		exception
		when others then
			cd_erro_p := 3;
			ds_erro_p := substr(sqlerrm,1,2000);
		end;
	end if;
	
end if;

end scola_alterar_convenio_atend;
/