create or replace
procedure ops_atualizacao_padrao_param
			(  nr_ordem_execucao_regra_p number,
			   nr_sequencia_p	number) is 
begin			   				           
update pls_pp_cta_combinada 
set nr_ordem_execucao = nr_ordem_execucao_regra_p
where nr_sequencia = nr_sequencia_p; 	
commit;			
											
end ops_atualizacao_padrao_param;
/
