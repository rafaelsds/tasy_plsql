create or replace procedure Gerar_W_Preco_Mat_Conv(
			cd_estabelecimento_p		number,
			cd_convenio_p			number,
			cd_grupo_material_p		number,
			cd_subgrupo_material_p	number,
			cd_classe_material_p		number,
			dt_vigencia_p			date) is

cd_material_w			number(6);
cd_categoria_w			varchar2(10);
vl_material_w			number(15,4);
pr_margem_w			number(15,4);
pr_margem_cm_w			Number(15,4);
dt_vigencia_w			date;
cd_tab_peco_mat_w 		number(4);
ie_origem_preco_w 		number(1);
vl_preco_ult_compra_w		number(15,4);
vl_custo_medio_w		number(15,4) := 0;
nr_seq_bras_preco_w		number(10,0);
nr_seq_mat_bras_w		number(10,0);
nr_seq_conv_bras_w		number(10,0);
nr_seq_conv_simpro_w		number(10,0);
nr_seq_mat_simpro_w		number(10,0);
nr_seq_simpro_preco_w		number(10,0);
nr_seq_ajuste_mat_w		number(10,0);

cursor	c01 is
	select	cd_categoria
	from	categoria_convenio
	where	cd_convenio	= cd_convenio_p;

cursor 	c02 is
	select		cd_material
	from		estrutura_material_v
	where		ie_situacao 	= 'A'
	and		nvl(cd_grupo_material_p, cd_grupo_material)		= cd_grupo_material
	and		nvl(cd_subgrupo_material_p, cd_subgrupo_material) 	= cd_subgrupo_material
	and		nvl(cd_classe_material_p, cd_classe_material)		= cd_classe_material
	order by 	cd_material;

begin

Exec_sql_Dinamico('Gerar_W_Preco_Material', 'truncate table w_preco_material');

open	c01;
loop
fetch	c01 into cd_categoria_w;
	exit when c01%notfound;
	begin

	open C02;
	loop
	fetch 	C02 into cd_material_w;
	exit 	when C02%notfound;
		begin
	
		define_preco_material(	cd_estabelecimento_p, 
					cd_convenio_p,
					cd_categoria_w,
					dt_vigencia_p,
					cd_material_w,
					0, 0, 0, '',
					0, 0, null, null, null, null, null, null, null, null,
					vl_material_w,
					dt_vigencia_w,
					cd_tab_peco_mat_w,
					ie_origem_preco_w,
					nr_seq_bras_preco_w,
					nr_seq_mat_bras_w,
					nr_seq_conv_bras_w,
					nr_seq_conv_simpro_w,
					nr_seq_mat_simpro_w,
					nr_seq_simpro_preco_w,
					nr_seq_ajuste_mat_w);

		select 	Obter_Valor_Ultima_Compra(cd_estabelecimento_p, null, cd_material_w, null, 'N')
		into 	vl_preco_ult_compra_w
		from 	dual;

		insert into w_preco_material (
			cd_convenio, cd_categoria, cd_material,
			dt_ultima_vigencia, vl_preco_venda, 
			vl_preco_ult_compra, vl_custo_medio, 
			pr_margem, pr_margem_cm)
		values (	
			cd_convenio_p, cd_categoria_w, cd_material_w, 
			dt_vigencia_p, vl_material_w,
			vl_preco_ult_compra_w, vl_custo_medio_w, 
			0, 0);
	
		end;
	end loop;
	close C02;
	end;
end loop;
close c01;

commit;

end Gerar_W_Preco_Mat_Conv;
/
