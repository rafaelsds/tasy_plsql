create or replace
procedure pls_atualizar_tipo_anexo_guia (	nr_seq_guia_plano_p	pls_guia_plano.nr_sequencia%type,
						nr_seq_requisicao_p	pls_requisicao.nr_sequencia%type,
						nm_usuario_p		usuario.nm_usuario%type) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Atualizar o cabe�alho da Requisi��o e da Autoriza��o, informando que a guia
possui anexo
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x  ] Tasy (Delphi/Java) [   x ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cursor C01 ( nr_seq_guia_plano_pc 	pls_guia_plano.nr_sequencia%type  ) is
	select	ie_tipo_anexo
	from	pls_guia_plano_proc
	where	nr_seq_guia = nr_seq_guia_plano_pc
	and	ie_tipo_anexo is not null
	union	
	select	ie_tipo_anexo
	from	pls_guia_plano_mat
	where	nr_seq_guia = nr_seq_guia_plano_pc
	and	ie_tipo_anexo is not null;
	

Cursor C02 ( nr_seq_requisicao_pc	pls_requisicao.nr_sequencia%type ) is
	select	ie_tipo_anexo
	from	pls_requisicao_proc
	where	nr_seq_requisicao = nr_seq_requisicao_pc
	and	ie_tipo_anexo is not null
	union	
	select	ie_tipo_anexo
	from	pls_requisicao_mat
	where	nr_seq_requisicao = nr_seq_requisicao_pc
	and	ie_tipo_anexo is not null;


	
ie_anexo_guia_w			pls_guia_plano.ie_anexo_guia%type		:= 'N';
ie_anexo_opme_w			pls_guia_plano.ie_anexo_opme%type		:= 'N';
ie_anexo_quimioterapia_w	pls_guia_plano.ie_anexo_quimioterapia%type 	:= 'N';
ie_anexo_radioterapia_w		pls_guia_plano.ie_anexo_radioterapia%type 	:= 'N';

begin


--Verifica se a atualiza��o � na Guia ou na Requisi��o
if	( nr_seq_guia_plano_p is not null ) then
	for	r_C01_w in C01( nr_seq_guia_plano_p ) loop
		ie_anexo_guia_w := 'S';		
		
		--Verifica se existe anexo de OPME
		if	( r_C01_w.ie_tipo_anexo = 'OP' ) then
			ie_anexo_opme_w := 'S';
		end if;
			
		--Verifica se existe anexo de Quimioterapia
		if	( r_C01_w.ie_tipo_anexo = 'QU' ) then
			ie_anexo_quimioterapia_w := 'S';
		end if;	

		--Verifica se existe anexo de Radioterapia
		if	( r_C01_w.ie_tipo_anexo = 'RA' ) then
			ie_anexo_radioterapia_w := 'S';
		end if;			
	end loop;
	
	update	pls_guia_plano
	set	dt_atualizacao 		= sysdate,
		nm_usuario		= nm_usuario_p,
		ie_anexo_guia  		= ie_anexo_guia_w,
		ie_anexo_opme          	= ie_anexo_opme_w,
		ie_anexo_quimioterapia 	= ie_anexo_quimioterapia_w,
		ie_anexo_radioterapia	= ie_anexo_radioterapia_w
	where	nr_sequencia 		= nr_seq_guia_plano_p;

elsif	( nr_seq_requisicao_p is not null ) then

	for	r_C02_w in C02( nr_seq_requisicao_p ) loop
		ie_anexo_guia_w := 'S';		
		
		--Verifica se existe anexo de OPME
		if	( r_C02_w.ie_tipo_anexo = 'OP' ) then
			ie_anexo_opme_w := 'S';
		end if;
			
		--Verifica se existe anexo de Quimioterapia
		if	( r_C02_w.ie_tipo_anexo = 'QU' ) then
			ie_anexo_quimioterapia_w := 'S';
		end if;	

		--Verifica se existe anexo de Radioterapia
		if	( r_C02_w.ie_tipo_anexo = 'RA' ) then
			ie_anexo_radioterapia_w := 'S';
		end if;		
	end loop;
	
	update	pls_requisicao
	set	dt_atualizacao 		= sysdate,
		nm_usuario		= nm_usuario_p,
		ie_anexo_guia  		= ie_anexo_guia_w,
		ie_anexo_opme          	= ie_anexo_opme_w,
		ie_anexo_quimioterapia 	= ie_anexo_quimioterapia_w,
		ie_anexo_radioterapia	= ie_anexo_radioterapia_w
	where	nr_sequencia 		= nr_seq_requisicao_p;
end if;

commit;

end pls_atualizar_tipo_anexo_guia;
/