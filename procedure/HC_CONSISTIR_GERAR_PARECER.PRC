create or replace
procedure hc_consistir_gerar_parecer	(	cd_pessoa_fisica_p			Varchar2,
						cd_estabelecimento_p	Number,
						ie_bloqueia_p	out	Varchar2) is 
					
ie_exige_aval_parecer_w	Varchar2(1);
					
begin

if	( cd_pessoa_fisica_p is not null) then

	select	nvl(max(ie_exige_aval_parecer),'N')
	into	ie_exige_aval_parecer_w
	from	hc_parametro
	where	cd_estabelecimento = cd_estabelecimento_p;
	
	if ( ie_exige_aval_parecer_w = 'S') then
		begin
		select	decode(count(*),0,'S','N')
		into	ie_bloqueia_p
		from	atendimento_checklist a,
			checklist_processo_regra b,
			checklist_processo c,
			med_avaliacao_paciente d
		where	b.nr_seq_checklist	= c.nr_sequencia
		and	a.nr_seq_checklist	= c.nr_sequencia
		AND	a.nr_sequencia		= d.nr_seq_atend_checklist
		AND	d.dt_liberacao		is not null
		and	a.cd_pessoa_fisica	= cd_pessoa_fisica_p
		and	b.ie_momento		= 'P';
		end;
	else
		ie_bloqueia_p := 'N';
	end if;
end if;
commit;

end hc_consistir_gerar_parecer;
/