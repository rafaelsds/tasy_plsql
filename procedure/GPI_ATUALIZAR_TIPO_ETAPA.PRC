create or replace
procedure gpi_atualizar_tipo_etapa(	nr_sequencia_p		number,
					nr_seq_tipo_etapa_p	number,
					nm_usuario_p		Varchar2) is 

begin
	update	gpi_cron_etapa
	set	nr_seq_tipo_etapa = nr_seq_tipo_etapa_p,
		nm_usuario	= nm_usuario_p,
		dt_atualizacao	= sysdate
	where	nr_sequencia	= nr_sequencia_p;

commit;

end gpi_atualizar_tipo_etapa;
/
