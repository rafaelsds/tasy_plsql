create or replace
procedure man_duplicar_informacoes_build(
		nr_sequencia_ctrl_p	number,
		nr_sequencia_ctrl_build_p	number,
		cd_versao_selecionada_p		varchar2,
		ds_lista_versao_p	varchar2,
		nm_usuario_p		Varchar2) is

ds_lista_versao_w		varchar2(2000);
nr_pos_virgula_w		number(10,0);
cd_versao_w			varchar2(15);
nr_controle_loop_w		number(3,0) := 0;
nr_seq_registro_w		number(10)  := 0;
nr_sequencia_build_alt_w	number(10);
ds_projeto_build_alt_w 		varchar2(60);
ds_classe_build_alt_w 		varchar2(255);
cd_pessoa_fisica_build_alt_w	varchar2(10);
cd_ramal_build_alt_w 		number(10);
ie_localizacao_build_alt_w 	varchar2(2);
ds_alteracao_build_alt_w 	varchar2(2000);


Cursor C01 is
	select	nr_sequencia,
		ds_projeto,
		ds_classe,
		cd_pessoa_fisica,
		cd_ramal,
		ie_localizacao,
		substr(ds_alteracao, 1, 1999)
 	from	man_os_ctrl_build_alt
	where	nr_seq_man_os_ctrl_build = nr_sequencia_ctrl_build_p;

begin
if	(ds_lista_versao_p is not null) and
	(nr_sequencia_ctrl_p is not null) and
	(nm_usuario_p is not null) then
	begin

	ds_lista_versao_w := ds_lista_versao_p;
	while 	(ds_lista_versao_w is not null) and
		(nr_controle_loop_w < 100) loop
		begin
		
		nr_pos_virgula_w := instr(ds_lista_versao_w,',');

		if	(nr_pos_virgula_w > 0) then
			begin
			cd_versao_w	:= substr(ds_lista_versao_w,0,nr_pos_virgula_w-1);
			ds_lista_versao_w	:= substr(ds_lista_versao_w,nr_pos_virgula_w+1,length(ds_lista_versao_w));
			end;
		else
			begin
			cd_versao_w	:= to_number(ds_lista_versao_w);
			ds_lista_versao_w	:= null;
			end;
		end if;

		if (cd_versao_selecionada_p <> cd_versao_w) then

		if	(cd_versao_w is not null) then
			begin

			-- Verifica se j� existe uma sequencia da vers�o na OS.
			begin
			select	distinct(nvl(a.nr_sequencia,0))
			into	nr_seq_registro_w
			from	man_os_ctrl_build a,
				man_os_ctrl_desc b
			where	a.nr_seq_man_os_ctrl_desc = nr_sequencia_ctrl_p
			and	a.cd_build is null
			and 	b.dt_liberacao is null
			and 	b.dt_inativacao is null
			and	a.cd_versao = cd_versao_w;

			exception
			when others then
				nr_seq_registro_w := 0;
			end;


			if	(nr_seq_registro_w = 0) then

				begin

				select	man_os_ctrl_build_seq.nextval
				into	nr_seq_registro_w
				from	dual;

				insert into man_os_ctrl_build(	nr_sequencia,
								dt_atualizacao,
								nm_usuario,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								cd_versao, 
								cd_build,
								dt_inicio_geracao,
								nr_seq_man_os_ctrl_desc,
								dt_fim_geracao,
								nm_usuario_geracao,
								ie_situacao)
							values(
								nr_seq_registro_w,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								cd_versao_w,
								null,
								null,								
								nr_sequencia_ctrl_p,
								null,
								null,
								'A');
						
				commit;
				end;
			end if;

			open C01;
			loop
			fetch C01 into
				nr_sequencia_build_alt_w,
				ds_projeto_build_alt_w,
				ds_classe_build_alt_w,
				cd_pessoa_fisica_build_alt_w,
				cd_ramal_build_alt_w,
				ie_localizacao_build_alt_w,
				ds_alteracao_build_alt_w;
			exit when C01%notfound;

				begin
				insert into man_os_ctrl_build_alt ( 	nr_sequencia,
 									dt_atualizacao,
 									nm_usuario,
 									dt_atualizacao_nrec,
 									nm_usuario_nrec,
 									ds_projeto,
 									ds_classe,
 									cd_pessoa_fisica,
									cd_ramal,
 									ie_localizacao,
 									ds_alteracao,
 									nr_seq_man_os_ctrl_build
 									) 
								values(
									man_os_ctrl_build_alt_seq.nextval,
									sysdate,
									nm_usuario_p,
									sysdate,
									nm_usuario_p,
									ds_projeto_build_alt_w,
									ds_classe_build_alt_w,
									cd_pessoa_fisica_build_alt_w,
									cd_ramal_build_alt_w,
									ie_localizacao_build_alt_w,
									ds_alteracao_build_alt_w,
									nr_seq_registro_w);
				commit;
				end;
			end loop;
			close C01;

			nr_seq_registro_w := 0;
			end;
		end if;
		end if;
		nr_controle_loop_w := nr_controle_loop_w + 1;
		end;
	end loop;
	end;
end if;
commit;
end man_duplicar_informacoes_build;
/
