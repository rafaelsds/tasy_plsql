create or replace
procedure gerar_w_itens_contrato_nf(	nr_seq_regra_contrato_p	number,
				nr_seq_contrato_p		number,
				cd_material_p		number,
				nr_seq_nota_p		number,
				qt_contrato_p		number,
				nm_usuario_p		Varchar2) is 

begin

insert into w_itens_contrato_nf(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_contrato,
	nr_seq_regra_contrato,
	cd_material,
	nr_seq_nota,
	qt_contrato)
values(	w_itens_contrato_nf_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_contrato_p,
	nr_seq_regra_contrato_p,
	cd_material_p,
	nr_seq_nota_p,
	qt_contrato_p);
	
commit;

end gerar_w_itens_contrato_nf;
/