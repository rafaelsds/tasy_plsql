create or replace
procedure pls_usuario_lib_manual_aut
			(	nr_seq_guia_p		number,
				nm_usuario_lib_p	Varchar2,
				nm_usuario_p		Varchar2) is 

ie_status_w		varchar2(2);
				
begin

select	ie_status
into	ie_status_w
from	pls_guia_plano
where	nr_sequencia = nr_seq_guia_p;

if	(ie_status_w = '1') then
	update	pls_guia_motivo_lib
	set	nm_usuario_liberacao = nm_usuario_lib_p
	where	nr_seq_guia = nr_seq_guia_p;

	update	pls_guia_plano
	set	nm_usuario_liberacao = nm_usuario_lib_p
	where	nr_sequencia = nr_seq_guia_p;

	update	pls_guia_plano_proc
	set	nm_usuario_liberacao = nm_usuario_lib_p
	where	nr_seq_guia = nr_seq_guia_p
	and	cd_procedimento is not null;

	update	pls_guia_plano_mat
	set	nm_usuario_liberacao = nm_usuario_lib_p
	where	nr_seq_guia = nr_seq_guia_p
	and	nr_seq_material is not null;
end if;

commit;

end pls_usuario_lib_manual_aut;
/
