create or replace 
procedure Gravar_Autor_Atend_Intenado is

nr_atendimento_w	number(10,0);

cursor	c01 is
	select	a.nr_atendimento
	from	unidade_atendimento a
	where	a.nr_atendimento	is not null;

begin

open	c01;
loop
fetch	c01 into nr_atendimento_w;
exit	when c01%notfound;
	begin

	Gravar_Pend_Autorizacao_Atend (nr_atendimento_w);

	end;
end loop;
close c01;

end Gravar_Autor_Atend_Intenado;
/