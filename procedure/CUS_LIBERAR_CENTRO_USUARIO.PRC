create or replace
procedure cus_liberar_centro_usuario(	cd_empresa_p		number,
					cd_estabelecimento_p	number,
					nm_usuario_lib_p	varchar2,
					ds_centro_controle_p	varchar2,
					nm_usuario_p		varchar2) is 

cd_centro_controle_w			centro_controle.cd_centro_controle%type;
ds_centro_controle_w			varchar2(4000);
nr_pos_sep_w				number(10);
begin

ds_centro_controle_w	:= substr(ds_centro_controle_p,1,4000);

while ds_centro_controle_w is not null loop 
	begin
	nr_pos_sep_w	:= nvl(instr(ds_centro_controle_w,','),0);
	if	(nr_pos_sep_w > 0) then
		cd_centro_controle_w	:= somente_numero(substr(ds_centro_controle_w,1,(nr_pos_sep_w - 1)));
	else
		cd_centro_controle_w	:= somente_numero(ds_centro_controle_w);
	end if;
	
	if	(nvl(cd_centro_controle_w,0) <> 0) then
		begin
		insert into centro_controle_usuario(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_centro_controle,
			cd_estabelecimento,
			nm_usuario_lib,
			ie_dependente)
		values(	centro_controle_usuario_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_centro_controle_w,
			cd_estabelecimento_p,
			nm_usuario_lib_p,
			'N');
		end;
	end if;
	ds_centro_controle_w	:= substr(ds_centro_controle_w, nr_pos_sep_w +1,4000);
	end;
end loop;

commit;

end cus_liberar_centro_usuario;
/