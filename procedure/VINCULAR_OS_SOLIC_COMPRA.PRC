create or replace
procedure	vincular_os_solic_compra(
			nr_solic_compra_p		number,
			nr_seq_ordem_serv_p	number,
			nm_usuario_p		varchar2) is

qt_existe_w		number(10);

begin

select	count(*)
into	qt_existe_w
from	man_ordem_servico
where	nr_sequencia = nvl(nr_seq_ordem_serv_p,0);

if	(qt_existe_w > 0) and
	(nvl(nr_solic_compra_p,0) <> 0)then
	begin

	update	solic_compra
	set	nr_seq_ordem_serv = nr_seq_ordem_serv_p
	where	nr_solic_compra = nr_solic_compra_p;

	commit;			

	end;
end if;

end vincular_os_solic_compra;
/