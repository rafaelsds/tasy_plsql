create or replace 
PROCEDURE atualiza_tab_preco_OPMENexo(	
				cd_material_p			NUMBER,
				dt_vigencia_p			DATE,
				cd_tab_preco_mat_p		NUMBER,
				cd_estabelecimento_p 		NUMBER,
				vl_preco_p			NUMBER,
				cd_cgc_p			VARCHAR2,
				nm_usuario_p			VARCHAR2) IS

vl_preco_venda_w			NUMBER(13,4):= 0;
dt_vigencia_w			DATE;
dt_vigencia_ww			DATE;
qt_existe_w			NUMBER(10,0);
cd_moeda_w			NUMBER(3,0):= 1;
qt_existe_dia_w			NUMBER(10,0);
qt_dias_antes_vig_w		NUMBER(5);
cd_cgc_w			pessoa_juridica.cd_cgc%TYPE;

CURSOR	c01 IS
	SELECT	
		a.vl_preco_venda,
		a.dt_inicio_vigencia,
		a.cd_moeda
	FROM	preco_material a
	WHERE	a.cd_tab_preco_mat = cd_tab_preco_mat_p
	AND	a.cd_estabelecimento = cd_estabelecimento_p
	AND	a.cd_material = cd_material_p
	AND	NVL(a.ie_situacao, 'A') = 'A'
	AND	a.dt_inicio_vigencia <= dt_vigencia_p
	AND	NVL(a.ie_preco_venda, 'S') = 'S'
	ORDER BY
		a.dt_inicio_vigencia;

BEGIN
/* 1 - Pegar as informa��es do pre�o vigente do item */
OPEN C01;
LOOP
FETCH C01 INTO
	vl_preco_venda_w,
	dt_vigencia_w,
	cd_moeda_w;
EXIT WHEN C01%NOTFOUND;
	BEGIN
	vl_preco_venda_w := vl_preco_venda_w;
	dt_vigencia_w	 := dt_vigencia_w;
	cd_moeda_w	 := cd_moeda_w;
	END;
END LOOP;
CLOSE C01;

SELECT	NVL(MAX(qt_dias_antes_vig),0)
INTO	qt_dias_antes_vig_w
FROM	parametros_opme
WHERE	cd_estabelecimento = cd_estabelecimento_p
AND		ie_sistema_integracao = 'P';

IF	(qt_dias_antes_vig_w <> 0) THEN
	dt_vigencia_ww := (dt_vigencia_p - qt_dias_antes_vig_w);
END IF;

cd_cgc_w := cd_cgc_p;

/* 2 - Verificar se o valor que est� vindo da integra��o � maior ou menor que o praticado*/
IF	(NVL(vl_preco_p,0) > vl_preco_venda_w) THEN

	/* 2.1 - Se o valor for maior, ent�o verifica-se no m�s que vem o valor novo existe o registro no dia 01 do m�s*/
	SELECT	COUNT(*)
	INTO	qt_existe_w
	FROM	preco_material
	WHERE	cd_estabelecimento 	= cd_estabelecimento_p
	AND	cd_tab_preco_mat		= cd_tab_preco_mat_p
	AND	cd_material				= cd_material_p
	AND	dt_inicio_vigencia		= TRUNC(NVL(dt_vigencia_ww, dt_vigencia_p), 'month');

	/* 2.2 - Se n�o tem o registro no primeiro dia daquele m�s ent�o cria-se um novo registro*/
	IF	(qt_existe_w = 0) THEN

		INSERT INTO preco_material(
			cd_estabelecimento,
			cd_tab_preco_mat,
			cd_material,
			dt_inicio_vigencia,
			vl_preco_venda,
			cd_moeda,
			ie_brasindice,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			ie_preco_venda,
			ie_integracao,
			cd_cgc_fornecedor)
		VALUES(
			cd_estabelecimento_p,
			cd_tab_preco_mat_p,
			cd_material_p,
			TRUNC(NVL(dt_vigencia_ww, dt_vigencia_p),'month'),
			NVL(vl_preco_p,0),
			cd_moeda_w,
			'N',
			SYSDATE,
			nm_usuario_p,
			'A',
			'S',
			'P',
			cd_cgc_w);
		

	ELSIF	(qt_existe_w > 0) THEN

		SELECT	COUNT(*)
		INTO	qt_existe_dia_w
		FROM	preco_material
		WHERE	cd_estabelecimento 			= cd_estabelecimento_p
		AND	cd_tab_preco_mat 				= cd_tab_preco_mat_p
		AND	cd_material 					= cd_material_p
		AND	TRUNC(dt_inicio_vigencia,'dd') 	= TRUNC(NVL(dt_vigencia_ww, dt_vigencia_p),'dd');

		IF	(qt_existe_dia_w = 0) THEN

			INSERT INTO preco_material(
				cd_estabelecimento,
				cd_tab_preco_mat,
				cd_material,
				dt_inicio_vigencia,
				vl_preco_venda,
				cd_moeda,
				ie_brasindice,
				dt_atualizacao,
				nm_usuario,
				ie_situacao,
				ie_preco_venda,
				ie_integracao,
				cd_cgc_fornecedor)
			VALUES(
				cd_estabelecimento_p,
				cd_tab_preco_mat_p,
				cd_material_p,
				TRUNC(NVL(dt_vigencia_ww, dt_vigencia_p),'dd'),
				NVL(vl_preco_p,0),
				cd_moeda_w,
				'N',
				SYSDATE,
				nm_usuario_p,
				'A',
				'S',
				'P',
				cd_cgc_w);

		END IF;
	END IF;

ELSE	
	/* 3 - O valor que est� vindo da integra��o � menor que o praticado..

	/* 3.1 - Lan�ar o item na tabela com data que vem da integra��o, antes verificar se n�o existe um j�*/
	SELECT	COUNT(*)
	INTO	qt_existe_w
	FROM	preco_material		
	WHERE	cd_estabelecimento	= cd_estabelecimento_p
	AND	cd_tab_preco_mat	= cd_tab_preco_mat_p
	AND	cd_material		= cd_material_p
	AND	TRUNC(dt_inicio_vigencia,'dd') = TRUNC(NVL(dt_vigencia_ww, dt_vigencia_p), 'dd');

	/* 3.2 - Se n�o tem o registro no primeiro dia daquele m�s ent�o cria-se um novo registro*/
	IF	(qt_existe_w = 0) THEN

		INSERT INTO preco_material(
			cd_estabelecimento,
			cd_tab_preco_mat,
			cd_material,
			dt_inicio_vigencia,
			vl_preco_venda,
			cd_moeda,
			ie_brasindice,
			dt_atualizacao,
			nm_usuario,
			ie_situacao,
			ie_preco_venda,
			ie_integracao,
			cd_cgc_fornecedor)
		VALUES(
			cd_estabelecimento_p,
			cd_tab_preco_mat_p,
			cd_material_p,
			TRUNC(NVL(dt_vigencia_ww, dt_vigencia_p),'dd'),
			NVL(vl_preco_p,0),
			cd_moeda_w,
			'N',
			SYSDATE,
			nm_usuario_p,
			'A',
			'N',
			'P',
			cd_cgc_w);
		
	ELSIF	(qt_existe_w > 0) THEN	
		/* 3.3 - Se j�  tem o registro no dia vem vem a integra��o ent�o atualiza-se  o registro, pois quando isso acontecer, seria um erro segundo conversa com Lilian S�rio/Daniela Z.*/

		UPDATE 	preco_material
		SET	vl_preco_venda 			= NVL(vl_preco_p,0)
		WHERE	cd_estabelecimento	= cd_estabelecimento_p
		AND	cd_tab_preco_mat		= cd_tab_preco_mat_p
		AND	cd_material				= cd_material_p
		AND	dt_inicio_vigencia      = TRUNC(NVL(dt_vigencia_ww, dt_vigencia_p), 'dd');

	END IF;

END IF;

COMMIT;

END atualiza_tab_preco_OPMENexo;
/