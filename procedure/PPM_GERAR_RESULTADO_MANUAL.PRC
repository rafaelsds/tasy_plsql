create or replace
procedure PPM_GERAR_RESULTADO_MANUAL(	nr_seq_metrica_p	number,
					dt_resultado_p		date,
					vl_resultado_p		number,
					cd_pessoa_fisica_p	varchar2,
					nr_seq_grupo_p		number,
					nr_seq_gerencia_p	number,
					nm_usuario_p		varchar2) is

nr_seq_ppm_metrica_w	number(10);
dt_resultado_w		date;					

cursor	c01 is
select	a.nr_sequencia
from	ppm_objetivo_metrica a,
	ppm_objetivo_meta b,
	ppm_objetivo c,
	usuario d
where	a.nr_seq_metrica	= nr_seq_metrica_p
and	a.nr_seq_meta		= b.nr_sequencia
and	b.nr_seq_objetivo	= c.nr_sequencia
and	c.cd_pessoa_fisica	= d.cd_pessoa_fisica
and	dt_resultado_w		between dt_inicio_vigencia and nvl(dt_final_vigencia,dt_resultado_w)
and	exists
	(select	1
	from	usuario_grupo_des x
	where	x.nr_seq_grupo		= nr_seq_grupo_p
	and	x.nm_usuario_grupo	= d.nm_usuario);
	
cursor	c02 is
select	a.nr_sequencia
from	ppm_objetivo_metrica a,
	ppm_objetivo_meta b,
	ppm_objetivo c,
	usuario d
where	a.nr_seq_metrica	= nr_seq_metrica_p
and	a.nr_seq_meta		= b.nr_sequencia
and	b.nr_seq_objetivo	= c.nr_sequencia
and	c.cd_pessoa_fisica	= d.cd_pessoa_fisica
and	dt_resultado_w		between dt_inicio_vigencia and nvl(dt_final_vigencia,dt_resultado_w)
and	exists
	(select	1
	from	usuario_grupo_des x,
		grupo_desenvolvimento y
	where	x.nr_seq_grupo		= y.nr_sequencia
	and	y.nr_seq_gerencia	= nr_seq_gerencia_p
	and	x.nm_usuario_grupo	= d.nm_usuario);	
	
	procedure gravar_resultado(nr_seq_ppm_metrica_p	number) is
	nr_seq_result_w	number(10);
	begin
	
	select	max(nr_sequencia)
	into	nr_seq_result_w
	from	ppm_objetivo_result
	where	nr_seq_metrica = nr_seq_ppm_metrica_p
	and	PKG_DATE_UTILS.start_of(dt_referencia,'MONTH') = PKG_DATE_UTILS.start_of(dt_resultado_w,'MONTH');
	
	if	(nr_seq_result_w is not null) then
		
		update	ppm_objetivo_result
		set	vl_resultado_calc 	= vl_resultado_p,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia 		= nr_seq_result_w;
	else	
		select	ppm_objetivo_result_seq.nextval
		into	nr_seq_result_w
		from	dual;

		insert into ppm_objetivo_result
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_metrica,
			dt_referencia,
			vl_resultado_calc)
		values	(nr_seq_result_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_ppm_metrica_p,
			dt_resultado_w,
			nvl(vl_resultado_p,0));

		insert into ppm_objetivo_result_ind
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_result,
			nr_seq_metrica)
		values	(ppm_objetivo_result_ind_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_result_w,
			nr_seq_ppm_metrica_p);
			
		insert into ppm_objetivo_result_gestor
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_result,
			nr_seq_metrica)
		values	(ppm_objetivo_result_gestor_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_result_w,
			nr_seq_ppm_metrica_p);
	end if;
	
	end gravar_resultado;

begin

dt_resultado_w	:= pkg_date_utils.start_of(dt_resultado_p,'MONTH');

if	(cd_pessoa_fisica_p is not null) then

	begin
	select	a.nr_sequencia
	into	nr_seq_ppm_metrica_w
	from	ppm_objetivo_metrica a,
		ppm_objetivo_meta b,
		ppm_objetivo c
	where	a.nr_seq_metrica	= nr_seq_metrica_p
	and	a.nr_seq_meta		= b.nr_sequencia
	and	b.nr_seq_objetivo	= c.nr_sequencia
	and	c.cd_pessoa_fisica	= cd_pessoa_fisica_p
	and	dt_resultado_w		between dt_inicio_vigencia and nvl(dt_final_vigencia,dt_resultado_w)
	and	rownum	= 1;	
	exception
	when others then
		nr_seq_ppm_metrica_w := null;
	end;
	
	if	(nr_seq_ppm_metrica_w is not null) then
		gravar_resultado(nr_seq_ppm_metrica_w);
	end if;

elsif	(nvl(nr_seq_grupo_p,0) > 0) then

	open C01;
	loop
	fetch C01 into
		nr_seq_ppm_metrica_w;
	exit when C01%notfound;		
		gravar_resultado(nr_seq_ppm_metrica_w);	
	end loop;
	close C01;

elsif	(nvl(nr_seq_gerencia_p,0) > 0) then

	open C02;
	loop
	fetch C02 into
		nr_seq_ppm_metrica_w;
	exit when C02%notfound;		
		gravar_resultado(nr_seq_ppm_metrica_w);	
	end loop;
	close C02;
	
end if;

commit;

end PPM_GERAR_RESULTADO_MANUAL;
/