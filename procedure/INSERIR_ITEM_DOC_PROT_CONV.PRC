create or replace
procedure inserir_item_doc_prot_conv(	nr_seq_prot_doc_p	number,
					nr_seq_prot_conv_p	number,
					ie_tipo_prot_doc_p	varchar2,
					nm_usuario_p		Varchar2) is 

nr_atendimento_w	number(10,0);
nr_protocolo_w		number(10,0);
nr_interno_conta_w	number(10,0);
qt_reg_protocolo	number(10,0);
nr_seq_item_w		number(10,0);
cd_convenio_w		number(10,0);
Qt_Maxima_Contas_Prot_w	number(10,0);
qt_registro_w		number(10,0);
nr_seq_tipo_item_w	number(10,0);
ie_tipo_documento_w	varchar2(1);
			
Cursor C01 is
select	nr_atendimento,
	nr_interno_conta
from	conta_paciente
where	nr_seq_protocolo = nr_seq_prot_conv_p
order by nr_atendimento;			
			
begin
obter_param_usuario(290, 102, obter_perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, Qt_Maxima_Contas_Prot_w);
obter_param_usuario(290, 106, obter_perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_tipo_documento_w);

if	(nr_seq_prot_conv_p > 0) and 
	(nvl(ie_tipo_prot_doc_p, 'X') in ('2','5')) then
	open C01;
	loop
	fetch C01 into	
		nr_atendimento_w,
		nr_interno_conta_w;
	exit when C01%notfound;
		begin
		if	(ie_tipo_documento_w = 'S') then
			select	nr_seq_tipo_doc_item
			into	nr_seq_tipo_item_w
			from	protocolo_documento
			where	nr_sequencia = nr_seq_prot_doc_p;
		end if;
		
		select	nvl(max(nr_seq_item),0)+1
		into	nr_seq_item_w
		from	protocolo_doc_item
		where	nr_sequencia	= nr_seq_prot_doc_p;

		begin
		select 	cd_convenio
		into	cd_convenio_w
		from	atend_categoria_convenio
		where 	nr_seq_interno	= obter_atecaco_atendimento(nr_atendimento_w)
		and	nr_atendimento	= nr_atendimento_w;
		exception
		when others then
			cd_convenio_w := null;
		end;
		
		if	(ie_tipo_prot_doc_p = '5') and
			(Qt_Maxima_Contas_Prot_w > 0) then
			select	count(*)
			into	qt_registro_w
			from	protocolo_doc_item
			where	nr_sequencia	= nr_seq_prot_doc_p
			and	nr_seq_item	<> nr_seq_item_w;
			
			if	((qt_registro_w+1) > Qt_Maxima_Contas_Prot_w) then
				wheb_mensagem_pck.exibir_mensagem_abort(248875);
			end if;
		end if;
		
		insert into protocolo_doc_item (
				nr_sequencia,
				nr_seq_item,
				nr_documento,
				nr_seq_interno,
				nm_usuario,
				dt_atualizacao,
				cd_convenio,
				nr_seq_tipo_item)
		values	(	nr_seq_prot_doc_p,
				nr_seq_item_w,
				nr_atendimento_w,
				decode(ie_tipo_prot_doc_p,'2','',nr_interno_conta_w),
				nm_usuario_p,
				sysdate,
				decode(ie_tipo_prot_doc_p,'2',cd_convenio_w,''),
				nr_seq_tipo_item_w);
		end;
	end loop;
	close C01;
end if;

commit;

end inserir_item_doc_prot_conv;
/