create or replace
procedure sus_consiste_procedimento(	nr_interno_conta_p		number,
				nr_atendimento_p		number,
				nm_usuario_p		varchar2) is

ie_tipo_proc_w		number(1);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
cd_medico_executor_w	varchar2(10);
ds_erro_w		varchar2(255);
qt_procedimento_w		number(9,3)	:= 0;
cd_pessoa_fisica_w	varchar2(10);
cd_profissional_exec_w	varchar2(10);
nr_seq_proc_w		number(10);
nr_seq_proc_princ_w	number(10);
ie_funcao_w		number(1);
cd_estabelecimento_w	number(4);
dt_procedimento_w		date;
nr_seq_participante_w	number(10);
ie_consiste_cbo_w		varchar2(2)	:= 'N';
ie_tipo_atendimento_w	number(3) 	:= 0;
ie_regra_tipo_atend_w	varchar2(1):= 'A';

cursor cursor01(nr_interno_conta_pp	procedimento_paciente.nr_interno_conta%type,
		nr_atendimento_pp	procedimento_paciente.nr_atendimento%type,
		ie_tipo_atendimento_pp	atendimento_paciente.ie_tipo_atendimento%type) is
	select	/*+ INDEX (a PROPACI_CONPACI_I)*/
		1 ie_tipo_proc,
		a.cd_procedimento,
		a.ie_origem_proced,
		a.cd_medico_executor,
		a.qt_procedimento,
		c.cd_pessoa_fisica,
		a.cd_pessoa_fisica cd_profissional_exec,
		a.nr_sequencia nr_seq_proc,
		a.nr_seq_proc_princ,
		0 ie_funcao,
		c.cd_estabelecimento,
		a.dt_procedimento,
		null nr_seq_participante
	from	atendimento_paciente	c,
		conta_paciente		b,
		procedimento_paciente	a
	where	ie_origem_proced	= 7
	and	a.nr_interno_conta	= nr_interno_conta_pp
	and	a.nr_atendimento	= nr_atendimento_pp
	and	a.nr_interno_conta	= b.nr_interno_conta
	and	b.nr_atendimento	= c.nr_atendimento
	and	a.cd_motivo_exc_conta is null
	union
	select	/*+ INDEX (a PROPACI_CONPACI_I)*/
		2 ie_tipo_proc,
		a.cd_procedimento,
		a.ie_origem_proced,
		d.cd_pessoa_fisica cd_medico_executor,
		a.qt_procedimento,
		c.cd_pessoa_fisica,
		'' cd_profissional_exec,
		a.nr_sequencia nr_seq_proc,
		a.nr_seq_proc_princ,
		nvl(sus_obter_indicador_equipe(d.ie_funcao),0) ie_funcao,
		c.cd_estabelecimento,
		a.dt_procedimento,
		d.nr_seq_partic nr_seq_participante
	from	procedimento_participante	d,
		atendimento_paciente		c,
		conta_paciente			b,
		procedimento_paciente		a
	where	ie_origem_proced	= 7
	and	a.nr_interno_conta	= nr_interno_conta_pp
	and	a.nr_atendimento	= nr_atendimento_pp
	and	a.nr_interno_conta	= b.nr_interno_conta
	and	b.nr_atendimento	= c.nr_atendimento
	and	a.nr_sequencia		= d.nr_sequencia
	and	ie_tipo_atendimento_pp	= 1
	and	a.cd_motivo_exc_conta is null;	
	
cursor01_w	cursor01%rowtype;

begin

if 	(obter_funcao_ativa = 1123) then
	begin
	ie_consiste_cbo_w := nvl(obter_valor_param_usuario(1123,94, obter_perfil_ativo, nm_usuario_p, 0),'S');
	end;
end if;

/* Obter dados da conta e do procedimento */
begin
select	b.cd_estabelecimento,
	a.ie_tipo_atendimento
into	cd_estabelecimento_w,
	ie_tipo_atendimento_w
from	atendimento_paciente	a,
	conta_paciente 		b
where	a.nr_atendimento	= b.nr_atendimento
and	b.nr_interno_conta	= nr_interno_conta_p;
exception
	when others then
	ie_tipo_atendimento_w	:= 0;
	cd_estabelecimento_w	:= wheb_usuario_pck.get_cd_estabelecimento;
end;

/*Obter parametros de Faturamento*/
select	nvl(max(ie_regra_tipo_atend),'A')
into	ie_regra_tipo_atend_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_w;

if	(nvl(ie_regra_tipo_atend_w,'A') = 'C') then
	begin
	select	nvl(max(ie_tipo_atend_conta),ie_tipo_atendimento_w)
	into	ie_tipo_atendimento_w
	from 	conta_paciente
	where 	nr_interno_conta = nr_interno_conta_p;
	exception
		when others then
		ie_tipo_atendimento_w:= ie_tipo_atendimento_w;
	end;
end if;

for cursor01_w in cursor01(nr_interno_conta_p,nr_atendimento_p,ie_tipo_atendimento_w) loop
	begin
	ie_tipo_proc_w		:= cursor01_w.ie_tipo_proc;
	cd_procedimento_w	:= cursor01_w.cd_procedimento;
	ie_origem_proced_w	:= cursor01_w.ie_origem_proced;
	cd_medico_executor_w	:= cursor01_w.cd_medico_executor;
	qt_procedimento_w	:= cursor01_w.qt_procedimento;
	cd_pessoa_fisica_w	:= cursor01_w.cd_pessoa_fisica;
	cd_profissional_exec_w	:= cursor01_w.cd_profissional_exec;
	nr_seq_proc_w		:= cursor01_w.nr_seq_proc;
	nr_seq_proc_princ_w	:= cursor01_w.nr_seq_proc_princ;
	ie_funcao_w		:= cursor01_w.ie_funcao;
	cd_estabelecimento_w	:= cursor01_w.cd_estabelecimento;
	dt_procedimento_w	:= cursor01_w.dt_procedimento;
	nr_seq_participante_w	:= cursor01_w.nr_seq_participante;
	
	sus_consiste_proccbo(nr_interno_conta_p, cd_procedimento_w, ie_origem_proced_w, cd_medico_executor_w, nr_seq_proc_w, ie_funcao_w,
				nm_usuario_p, 1,nr_seq_participante_w,ds_erro_w); /* Passando o medico */
	if	(ie_tipo_proc_w = 1) then
		if	(ie_consiste_cbo_w = 'N') and
			(nvl(cd_medico_executor_w,'X') = 'X') and
			(nvl(cd_profissional_exec_w,'X') <> 'X') then
			sus_consiste_proccbo(nr_interno_conta_p, cd_procedimento_w, ie_origem_proced_w, cd_profissional_exec_w, nr_seq_proc_w,
				ie_funcao_w, nm_usuario_p, 1, nr_seq_participante_w,ds_erro_w); /* Passando o profissional */
		elsif	(ie_consiste_cbo_w = 'S') and
			(nvl(cd_profissional_exec_w,'X') <> 'X') then
			sus_consiste_proccbo(nr_interno_conta_p, cd_procedimento_w, ie_origem_proced_w, cd_profissional_exec_w, nr_seq_proc_w,
				ie_funcao_w, nm_usuario_p, 1, nr_seq_participante_w,ds_erro_w); /* Passando o profissional */
		end if;
		sus_consiste_proc(nr_interno_conta_p, cd_procedimento_w, ie_origem_proced_w, cd_medico_executor_w, qt_procedimento_w, nr_atendimento_p, nr_seq_proc_w,
					nm_usuario_p, 1, dt_procedimento_w,ds_erro_w);
		sus_consiste_procpac(nr_interno_conta_p, cd_procedimento_w, ie_origem_proced_w, cd_pessoa_fisica_w, nm_usuario_p, 1, ds_erro_w, nr_seq_proc_w);
		if	(sus_validar_regra(4, cd_procedimento_w, ie_origem_proced_w,dt_procedimento_w) > 0) then
			sus_consiste_procopm(nr_interno_conta_p, nr_atendimento_p, cd_procedimento_w, ie_origem_proced_w, qt_procedimento_w, nr_seq_proc_w,
					nr_seq_proc_princ_w, nm_usuario_p, cd_estabelecimento_w, 1, ds_erro_w);
		end if;
	end if;
	end;
end loop;

end sus_consiste_procedimento;
/
