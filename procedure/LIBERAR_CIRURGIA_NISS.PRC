create or replace
procedure liberar_cirurgia_niss(	nr_cirurgia_p	number,
					nm_usuario_p	varchar2,
					ie_acao_p	varchar2 )
					is

/*

ie_acao_p

L - Liberar
E - Estornar liberacao

*/

begin

update	cirurgia
set	dt_lib_niss	=	decode(ie_acao_p,'L',sysdate,null),
	nm_usuario	=	nm_usuario_p
where	nr_cirurgia	=	nr_cirurgia_p;

commit;

end liberar_cirurgia_niss;
/