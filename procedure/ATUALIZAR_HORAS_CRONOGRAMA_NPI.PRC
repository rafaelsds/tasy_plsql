create or replace
procedure atualizar_horas_cronograma_npi(	nr_sequencia_p	Number,
						nm_usuario_p	Varchar2) is 

qt_total_w		Number(15,2);
nr_seq_cronograma_w	Number(10);
dt_inicio_prev_w	Date;
dt_fim_prev_w		Date;

begin

	select	max(nr_seq_cronograma)
	into	nr_seq_cronograma_w
	from	proj_cron_etapa
	where	nr_sequencia = nr_sequencia_p;

	if (nr_seq_cronograma_w > 0) then
	
		select	sum(nvl(a.qt_hora_prev,0)),
			min(a.dt_inicio_prev),
			max(a.dt_fim_prev)
		into	qt_total_w,
			dt_inicio_prev_w,
			dt_fim_prev_w
		from	proj_cron_etapa a,
			proj_cronograma b
		where	b.nr_sequencia = a.nr_seq_cronograma
		and	b.nr_sequencia = nr_seq_cronograma_w;
		
		update	proj_cronograma
		set	qt_total_horas = qt_total_w,
			nm_usuario = nm_usuario_p,
			dt_inicio = dt_inicio_prev_w,
			dt_fim = dt_fim_prev_w
		where	nr_sequencia = nr_seq_cronograma_w;
		
	end if;
	
commit;

end atualizar_horas_cronograma_npi;
/