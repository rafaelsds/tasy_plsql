create or replace
procedure insert_tre_pesquisa(
							cd_estabelecimento_p 	varchar2,
							nm_usuario_p			varchar2,
							nr_seq_modulo_p			varchar2) is

Cursor C01 is

	select	cd_pessoa_fisica
	from	tre_inscrito a,
			tre_inscrito_presenca b,
			tre_evento_modulo c
	where	a.nr_seq_evento = c.nr_seq_evento
	and		b.nr_seq_inscrito = a.nr_sequencia
	and		c.nr_sequencia = nr_seq_modulo_p
	and		a.nr_seq_mod_evento = nr_seq_modulo_p
	and		b.ie_presente = 'S';

c01_w			c01%rowtype;

cd_pessoa_resp_w varchar2(10);
cd_pessoa_palestrante_w varchar2(10);
nr_seq_modalidade_w	varchar2(10);
nr_seq_tipo_avaliacao_w varchar2(4);
nr_seq_treinamento_w	varchar2(10);
ds_pessoa_email_w	varchar2(255);
ds_senha_w	varchar2(20);
ds_modulo_w	varchar2(40);

begin


if(nm_usuario_p = 'wcsilva') then
begin


select	a.cd_palestrante,
		a.nr_seq_modalidade,
		a.nr_sequencia,
		substr(obter_descricao_modulo(b.nr_seq_modulo),1,40)
into	cd_pessoa_palestrante_w,
		nr_seq_modalidade_w,
		nr_seq_treinamento_w,
		ds_modulo_w
from    tre_evento a,
		tre_evento_modulo b
where	b.nr_seq_evento = a.nr_sequencia
and		b.nr_sequencia = nr_seq_modulo_p;

if(nr_seq_modalidade_w = '1') then
	nr_seq_tipo_avaliacao_w:= '1327';
end if;
if(nr_seq_modalidade_w = '2') then
	nr_seq_tipo_avaliacao_w:= '1326';
end if;

open C01;
loop
fetch C01 into
	cd_pessoa_resp_w;
exit when C01%notfound;
	begin
	insert into tre_pesquisa (
								NR_SEQUENCIA,
								CD_ESTABELECIMENTO,
								DT_ATUALIZACAO,
								NM_USUARIO,
 								DT_ATUALIZACAO_NREC,
 								NM_USUARIO_NREC,
 								DT_AVALIACAO,
								CD_PESSOA_FISICA,
 								NR_SEQ_TIPO_AVALIACAO,
 								DT_LIBERACAO,
 								DT_CANCELAMENTO,
 								NM_USUARIO_CANCELAMENTO,
 								NR_SEQ_APRES,
 								DS_OBSERVACAO,
 								NR_SEQ_TREINAMENTO,
 								CD_PESSOA_RESP,
								NR_SEQ_MODULO)
					values (
								tre_pesquisa_seq.nextval,
								cd_estabelecimento_p,
								sysdate,
								nm_usuario_p,
								sysdate,
								nm_usuario_p,
								sysdate,
								cd_pessoa_palestrante_w,
								nr_seq_tipo_avaliacao_w,
								null,
								null,
								null,
								null,
								null,
								nr_seq_treinamento_w,
								cd_pessoa_resp_w,
								nr_seq_modulo_p);
		
			
	select obter_compl_pf(cd_pessoa_resp_w,1,'M'),
		ds_senha
	into ds_pessoa_email_w,
		ds_senha_w
	from pessoa_fisica
	where cd_pessoa_fisica = cd_pessoa_resp_w;
								
								
								
								
	Enviar_Email('Avalia��o do m�dulo '||ds_modulo_w,'Avalia��o de Treinamento Philips-Tasy!
	
Sua opini�o � muito importante para que possamos melhorar cada vez mais os treinamentos do sistema Tasy!
Favor acessar o link abaixo para realiza��o da avalia��o referente ao treinamento realizado.
	
Usu�rio: '||cd_pessoa_resp_w||'
Senha: '||ds_senha_w||'
Endere�o: http://192.168.0.230/TreinamentoWeb/',
	'treinamento@wheb.com.br',ds_pessoa_email_w,nm_usuario_p,'M');

	insert into tre_log_envio_aval (
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_modulo,
						ds_email_destino,
						ds_email_origem)
					values(
						tre_log_envio_aval_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_modulo_p,
						ds_pessoa_email_w,
						'treinamento@wheb.com.br');
							

	
	end;
end loop;
close C01;



end;
end if;



commit;

end insert_tre_pesquisa;
/
