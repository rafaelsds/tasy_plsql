create or replace
procedure san_receber_pre_estoque (	nr_seq_producao_p		number,
					nr_seq_local_armazenamento_p	number,
					nm_usuario_p			varchar2) is 

begin

update	san_producao
set	nm_usuario_recebimento	= nm_usuario_p,
	nm_usuario 		= nm_usuario_p,
	dt_recebimento		= sysdate,
	dt_atualizacao		= sysdate,
	nr_seq_armazenamento	= nr_seq_local_armazenamento_p
where	nr_sequencia = nr_seq_producao_p;

commit;

end san_receber_pre_estoque;
/