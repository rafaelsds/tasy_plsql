create or replace
procedure gerar_processo_aprov_licitacao(	nr_seq_licitacao_p			number,
				nm_usuario_p			varchar2) is 


cd_estabelecimento_w			estabelecimento.cd_estabelecimento%type;
cd_perfil_ativo_w				perfil.cd_perfil%type;
nr_seq_aprovacao_w			processo_compra.nr_sequencia%type;
cd_material_w				material.cd_material%type;
nr_seq_lic_item_w				reg_lic_item.nr_seq_lic_item%type;
nr_seq_proj_rec_w				projeto_recurso.nr_sequencia%type;
cd_processo_aprov_w			processo_aprov_estrut.nr_sequencia%type;
dt_liberacao_w				reg_licitacao.dt_liberacao%type;
nr_sequencia_w				processo_aprov_resp.nr_sequencia%type;
ie_responsavel_w				processo_aprov_resp.ie_responsavel%type;
cd_cargo_w				processo_aprov_resp.cd_cargo%type;
nm_usuario_regra_w			processo_aprov_resp.nm_usuario_regra%type;
cd_responsavel_w				processo_aprov_compra.cd_pessoa_fisica%type;
nr_nivel_aprovacao_w			processo_aprov_resp.nr_nivel_aprovacao%type;
nr_nivel_aprovacao_w2			processo_aprov_resp.nr_nivel_aprovacao%type;
ie_aprovacao_nivel_w			parametro_compras.ie_aprovacao_nivel%type;
dt_emissao_w				date;
nr_itens_sem_aprov_w			number(10);


cursor c01 is
select	b.nr_seq_aprovacao,
	b.cd_material,
	b.nr_seq_lic_item,
	b.nr_seq_proj_rec
from 	reg_licitacao a,
	reg_lic_item b,
	estrutura_material_v e
where	a.nr_sequencia = b.nr_seq_licitacao
and	b.cd_material = e.cd_material
and 	a.dt_aprovacao is null
and	a.nr_sequencia = nr_seq_licitacao_p
order by e.cd_grupo_material,
	e.cd_subgrupo_material,
	e.cd_classe_material,
	e.cd_material;
	
cursor c02 is
select	nr_sequencia,
	ie_responsavel,
	cd_cargo,
	nm_usuario_regra,
	nr_nivel_aprovacao
from	processo_aprov_resp
where	cd_processo_aprov		= cd_processo_aprov_w
and	nvl(ie_reg_licitacao,'N')	= 'S'
order by	nr_sequencia;

cursor c03 is
select	distinct nr_seq_aprovacao
from	reg_lic_item
where	nr_seq_licitacao = nr_seq_licitacao_p
and	nr_seq_aprovacao is not null;

begin

select	cd_estabelecimento,
	obter_perfil_ativo,
	dt_emissao
into	cd_estabelecimento_w,
	cd_perfil_ativo_w,
	dt_emissao_w
from	reg_licitacao
where	nr_sequencia = nr_seq_licitacao_p;

open C01;
loop
fetch C01 into	
	nr_seq_aprovacao_w,
	cd_material_w,
	nr_seq_lic_item_w,
	nr_seq_proj_rec_w;
exit when C01%notfound;
	begin
	
	if	(nr_seq_aprovacao_w is null) then
			
		obter_processo_aprovacao(
			cd_material_w,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,			
			'L',
			'N',
			cd_estabelecimento_w,
			cd_perfil_ativo_w,
			nr_seq_proj_rec_w,
			nr_seq_licitacao_p,
			cd_processo_aprov_w);
		
		if	(cd_processo_aprov_w is not null) then
		
			select	nvl(max(b.nr_sequencia),0)
			into 	nr_seq_aprovacao_w
			from 	processo_aprov_compra a,
				processo_compra b
			where 	b.nr_sequencia = a.nr_sequencia
			and 	b.cd_processo_aprov = cd_processo_aprov_w
			and 	a.nr_documento = nr_seq_licitacao_p
			and 	a.cd_estabelecimento = cd_estabelecimento_w
			and	a.ie_tipo = 'L';
		
			if	(nr_seq_aprovacao_w = 0) then
				begin
				
				select	processo_compra_seq.nextval
				into	nr_seq_aprovacao_w
				from	dual;

				insert into processo_compra(
					nr_sequencia, 
					cd_processo_aprov, 
					dt_atualizacao, 
					nm_usuario) 
				values(	nr_seq_aprovacao_w, 
					cd_processo_aprov_w, 
					sysdate, 
					nm_usuario_p);
					
				dt_liberacao_w			:= sysdate;
				
				open c02;
				loop
				fetch c02 into
					nr_sequencia_w,
					ie_responsavel_w,
					cd_cargo_w,
					nm_usuario_regra_w,
					nr_nivel_aprovacao_w;
				exit when c02%notfound;
					begin
				
					cd_responsavel_w	:= '';
				
					if	(ie_responsavel_w = 'C') then
						
						if	(nr_nivel_aprovacao_w is not null)
							and (dt_liberacao_w is null) then
							
							select	obter_se_proc_por_nivel(nr_seq_aprovacao_w, cd_estabelecimento_w)
							into	ie_aprovacao_nivel_w
							from	dual;
							
							if	(ie_aprovacao_nivel_w = 'S') and
								(nr_nivel_aprovacao_w2 = nr_nivel_aprovacao_w) then
								dt_liberacao_w	:= sysdate;
							else
								dt_liberacao_w	:= null;
							end if;
						end if;
						
						insert into processo_aprov_compra (
							nr_sequencia,
							nr_seq_proc_aprov,
							dt_atualizacao,
							nm_usuario,
							nm_usuario_nrec,
							cd_pessoa_fisica,
							cd_cargo,
							dt_liberacao,
							dt_definicao,
							ie_aprov_reprov,
							cd_estabelecimento,
							ie_urgente,
							nr_documento,
							ie_tipo,
							dt_documento)
						values(	nr_seq_aprovacao_w,
							nr_sequencia_w,
							sysdate,
							nm_usuario_p,
							nm_usuario_p,
							cd_responsavel_w,
							cd_cargo_w,
							dt_liberacao_w,
							decode(cd_cargo_w,null, sysdate, null),
							'P',
							cd_estabelecimento_w,
							'N',
							nr_seq_licitacao_p,
							'L',
							dt_emissao_w);
						
						if 	(cd_cargo_w is not null) then
							dt_liberacao_w	:= null;
						end if;
				
					elsif	(ie_responsavel_w = 'F') then
				
						select	obter_pessoa_fisica_usuario(nm_usuario_regra_w,'C')
						into	cd_responsavel_w
						from	dual;
				
						if	(cd_responsavel_w is null) then
							/*'O usuario aprovador ' || NM_USUARIO_REGRA_W || 'nao possui nenhuma pessoa fisica vinculada. Favor ajustar o cadastro deste usuario');*/
							wheb_mensagem_pck.exibir_mensagem_abort(191563,'NM_USUARIO_REGRA_W=' ||NM_USUARIO_REGRA_W);
						end if;
						
						if	(nr_nivel_aprovacao_w is not null)
							and (dt_liberacao_w is null) then
							
							select	obter_se_proc_por_nivel(nr_seq_aprovacao_w, cd_estabelecimento_w)
							into	ie_aprovacao_nivel_w
							from	dual;
							
							if	(ie_aprovacao_nivel_w = 'S') and
								(nr_nivel_aprovacao_w2 = nr_nivel_aprovacao_w) then
								dt_liberacao_w	:= sysdate;
							else
								dt_liberacao_w	:= null;
							end if;
						end if;
				
						insert into processo_aprov_compra (
							nr_sequencia,
							nr_seq_proc_aprov,
							dt_atualizacao,
							nm_usuario,
							nm_usuario_nrec,
							cd_pessoa_fisica,
							cd_cargo,
							dt_liberacao,
							dt_definicao,
							ie_aprov_reprov,
							cd_estabelecimento,
							ie_urgente,
							nr_documento,
							ie_tipo,
							dt_documento)
						values(	nr_seq_aprovacao_w,
							nr_sequencia_w,
							sysdate,
							nm_usuario_p,
							nm_usuario_p,
							cd_responsavel_w,
							null,
							dt_liberacao_w,
							null,
							'P',
							cd_estabelecimento_w,
							'N',
							nr_seq_licitacao_p,
							'L',
							dt_emissao_w);
					
						if	(cd_responsavel_w is not null) then
							dt_liberacao_w	:= null;
						end if;

					end if;
					end;
				end loop;
				close c02;
				end;
			end if;
			
			update	reg_licitacao
			set	dt_lib_aprovacao	= sysdate,
				nm_usuario_lib_aprov	= nm_usuario_p
			where	nr_sequencia		= nr_seq_licitacao_p;
				
			update	reg_lic_item
			set	nr_seq_aprovacao	= nr_seq_aprovacao_w
			where	nr_seq_licitacao	= nr_seq_licitacao_p
			and	nr_seq_lic_item		= nr_seq_lic_item_w;

		end if;
	end if;	
	end;
end loop;
close C01;

select	count(*)
into	nr_itens_sem_aprov_w
from	reg_lic_item
where	dt_aprovacao is null
and	nr_seq_licitacao = nr_seq_licitacao_p;

if	(nr_itens_sem_aprov_w = 0) then
	update	reg_licitacao
	set	dt_aprovacao		= sysdate,
		nm_usuario_aprov		= nm_usuario_p
	where	nr_sequencia		= nr_seq_licitacao_p;
end if;
	
open c03;
loop
fetch c03 into
	nr_seq_aprovacao_w;
exit when c03%notfound;
	aprovacao_automatica_req(nr_seq_aprovacao_w, nm_usuario_p); 
end loop;
close c03;

commit;

end gerar_processo_aprov_licitacao;
/