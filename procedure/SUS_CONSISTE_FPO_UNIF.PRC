create or replace
procedure sus_consiste_fpo_unif (	dt_parametro_p		in date,
				cd_procedimento_p		in number,
				ie_origem_proced_p	in number,
				qt_procedimento_p		in number,
				nr_atendimento_p		in number,
				cd_estabelecimento_p	in number,
				ie_tipo_atendimento_p	in number,
				cd_cbo_p		in varchar2,
				ie_opcao_p		in varchar2,
				ds_retorno_p		out varchar2,
				cd_setor_atendimento_p  in number,
				cd_procedencia_p	in number,
				nm_usuario_p		in varchar2)	is
/*
A - Ambos
V - Valor
Q - Quantidade
*/
qt_fisico_w		number(10) := 0;
vl_orcamentario_w		number(12,2);
cd_convenio_w		number(5);
qt_exec_w		number(10) := 0;
dt_entrega_w		date;
ds_retorno_w		varchar2(2000);
vl_exec_w		number(12,2);
vl_info_w			number(12,2);
nr_seq_regra_w		number(10);
qt_executado_w		number(10) := 0;
qt_protocolo_w		number(10) := 0;
qt_agenda_w		number(10) := 0;
vl_executado_w		number(12,2);
vl_protocolo_w		number(12,2);
vl_agenda_w		number(12,2);
ie_consiste_agenda_w	varchar2(1) := 'N';
ie_carater_inter_sus_w	atendimento_paciente.ie_carater_inter_sus%type;

pragma autonomous_transaction;

begin

ie_consiste_agenda_w := nvl(obter_valor_param_usuario(820, 421, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p),'N');

select	nvl(max(cd_convenio_sus),0)
into	cd_convenio_w
from	parametro_faturamento
where	cd_estabelecimento = cd_estabelecimento_p;

begin
select	nvl(ie_carater_inter_sus,'00')
into	ie_carater_inter_sus_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;
exception
when others then
	ie_carater_inter_sus_w := '00';
end;

if	(ie_origem_proced_p = 7) and
	((nr_atendimento_p is not null) or
	(cd_convenio_w <> 0)) then
	
	sus_obter_dados_fpo_unif(cd_procedimento_p,
				ie_origem_proced_p,
				ie_tipo_atendimento_p,
				dt_parametro_p,
				cd_cbo_p,
				1,
				cd_estabelecimento_p,
				0,
				cd_setor_atendimento_p,
				cd_procedencia_p,
				qt_fisico_w,
				vl_orcamentario_w,
				nr_seq_regra_w,
				ie_carater_inter_sus_w);
	
	/*valor procedimento*/
	if	(ie_tipo_atendimento_p <> 1) then
		vl_info_w	:= nvl(sus_obter_preco_proced(cd_estabelecimento_p,dt_parametro_p,ie_tipo_atendimento_p,cd_procedimento_p,ie_origem_proced_p,2),0);
	else	
		vl_info_w	:= nvl(sus_obter_preco_proced(cd_estabelecimento_p,dt_parametro_p,ie_tipo_atendimento_p,cd_procedimento_p,ie_origem_proced_p,1),0);
	end if;

	/*quantidade e valor ja executado do procedimento para a data de vigencia*/
	qt_executado_w	:= nvl(sus_obter_resultado_fpo(nr_seq_regra_w,1,'E'),0);
	vl_executado_w	:= nvl(sus_obter_resultado_fpo(nr_seq_regra_w,2,'E'),0);
	--qt_protocolo_w	:= nvl(sus_obter_resultado_fpo(nr_seq_regra_w,1,'P'),0);
	--vl_protocolo_w	:= nvl(sus_obter_resultado_fpo(nr_seq_regra_w,2,'P'),0);
	
	if	(ie_consiste_agenda_w = 'S') and
		(obter_funcao_ativa = 820) then
		begin
		qt_agenda_w := nvl(sus_obter_resultado_fpo(nr_seq_regra_w,1,'A'),0);
		vl_agenda_w := nvl(sus_obter_resultado_fpo(nr_seq_regra_w,2,'A'),0);
		end;
	else
		begin
		qt_agenda_w := 0;	
		vl_agenda_w := 0;
		end;
	end if;
	

	qt_exec_w := qt_executado_w + qt_agenda_w;
	vl_exec_w := vl_executado_w + vl_agenda_w;

	qt_exec_w	:= nvl(qt_exec_w,0) + nvl(qt_procedimento_p,0);
	vl_exec_w	:= nvl(vl_exec_w,0) + nvl((vl_info_w * qt_procedimento_p),0);

	if	(ie_opcao_p = 'V') and
		(vl_exec_w	> vl_orcamentario_w) then				
				--'Valor(R$' || campo_mascara(vl_exec_w,2) || ') ultrapassou o teto que � de R$' || campo_mascara(vl_orcamentario_w,2) || '.';
		ds_retorno_w	:= wheb_mensagem_pck.get_texto(299670,'VL_EXEC='||campo_mascara(vl_exec_w,2)||';'||'VL_ORCAMENTARIO='||campo_mascara(vl_orcamentario_w,2));
	elsif	(ie_opcao_p = 'Q') and
		(qt_exec_w	> qt_fisico_w) then
				--'Quantidade(' || qt_exec_w || ') ultrapassou o teto que � de ' || qt_fisico_w || '.';
		ds_retorno_w	:= wheb_mensagem_pck.get_texto(299671,'QT_EXEC='||qt_exec_w||';'||'QT_FISICO='||qt_fisico_w);
	elsif	(ie_opcao_p = 'A') and
		((qt_exec_w	> qt_fisico_w) or
		(vl_exec_w	> vl_orcamentario_w)) then
				/*'Valor(R$' || campo_mascara(vl_exec_w,2) || ') ou quantidade(' || qt_exec_w || ') ultrapassaram o teto que � de R$' ||
				campo_mascara(vl_orcamentario_w,2) || ' para o valor, e de ' || qt_fisico_w || ' para quantidade.'||dt_parametro_p ||' - '|| cd_procedimento_p ||' - '||nr_seq_regra_w ;*/		
		ds_retorno_w	:= wheb_mensagem_pck.get_texto(311057,	'VL_EXEC='||campo_mascara(vl_exec_w,2)||';'||
							'QT_EXEC='||qt_exec_w||';'||
							'VL_ORCAMENTARIO='||campo_mascara(vl_orcamentario_w,2)||';'||
							'QT_FISICO='||qt_fisico_w||';'||
							'DS_REGRA_W='||dt_parametro_p ||' - '|| cd_procedimento_p ||' - '||nr_seq_regra_w);
	else
		ds_retorno_w	:= '';
	end if;
else
	ds_retorno_w	:= '';
end if;

ds_retorno_p	:= ds_retorno_w;

end sus_consiste_fpo_unif;
/
