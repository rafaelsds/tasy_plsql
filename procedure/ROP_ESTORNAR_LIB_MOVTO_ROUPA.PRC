create or replace
procedure rop_estornar_lib_movto_roupa(
      nr_seq_lote_movto_p number,
      nm_usuario_p    varchar2,
      ds_erro_p   out varchar2) is

ds_operacao_w   varchar2(10);
nr_seq_local_orig_w rop_lote_movto.nr_seq_local%type;
nr_seq_local_dest_w rop_lote_movto.nr_seq_local_orig_dest%type;
nr_seq_roupa_movto_w  rop_movto_roupa.nr_seq_roupa%type;
nr_seq_lote_w   rop_movto_roupa.nr_seq_lote%type;
cd_barra_roupa_w  rop_roupa.cd_barra_roupa%type;
cd_material_w   rop_lote_roupa.cd_material%type;
qt_roupa_w    rop_movto_roupa.qt_roupa%type;
qt_saldo_local_orig_w rop_saldo_roupa.qt_saldo%type;
qt_saldo_local_dest_w rop_saldo_roupa.qt_saldo%type;
dt_liberacao_w    rop_lote_movto.dt_liberacao%type;
qt_saldo_w rop_saldo_roupa.qt_saldo%type;

cursor c01 is
  select  nr_seq_roupa 
  from  rop_movto_roupa
  where nr_seq_lote = nr_seq_lote_movto_p;

begin

open c01;
loop
fetch c01 into 
  nr_seq_roupa_movto_w;
exit when c01%notfound;
  begin
  select  a.nr_seq_lote,
    b.cd_barra_roupa,
    c.cd_material
  into  nr_seq_lote_w,
    cd_barra_roupa_w,
    cd_material_w
  from  rop_lote_movto d,
    rop_lote_roupa c,
    rop_roupa b,
    rop_movto_roupa a
  where b.nr_sequencia = a.nr_seq_roupa
  and c.nr_sequencia = b.nr_seq_lote_roupa
  and d.nr_sequencia = a.nr_seq_lote
  and a.nr_seq_roupa = nr_seq_roupa_movto_w
  and a.nr_seq_lote = (select max(x.nr_seq_lote)
        from  rop_movto_roupa x
        where x.nr_seq_roupa = a.nr_seq_roupa);
  
  if  (nr_seq_lote_w = nr_seq_lote_movto_p) then 
    begin
    select  a.qt_roupa,
      b.nr_seq_local,
      b.nr_seq_local_orig_dest,
      rop_obter_saldo_roupa(a.nr_seq_roupa,b.nr_seq_local,trunc(b.dt_liberacao,'mm')),
      rop_obter_saldo_roupa(a.nr_seq_roupa,b.nr_seq_local_orig_dest,trunc(b.dt_liberacao,'mm')),
      substr(rop_obter_dados_operacao(b.nr_seq_operacao,'T'),1,15),
      b.dt_liberacao
    into  qt_roupa_w,
      nr_seq_local_orig_w,
      nr_seq_local_dest_w,
      qt_saldo_local_orig_w,
      qt_saldo_local_dest_w,
      ds_operacao_w,
      dt_liberacao_w
    from  rop_lote_movto b,
      rop_movto_roupa a
    where b.nr_sequencia = a.nr_seq_lote
    and a.nr_seq_roupa = nr_seq_roupa_movto_w
    and b.nr_sequencia = nr_seq_lote_movto_p;
    
    if  (nr_seq_local_orig_w is not null) and
      (nr_seq_local_dest_w is null) then
      select nvl(qt_saldo_local_orig_w - qt_roupa_w, 0)
      into qt_saldo_w
      from dual;      
      if (qt_saldo_w > 0) then
	update  rop_saldo_roupa
        set qt_saldo = qt_saldo_w
        where nr_seq_local = nr_seq_local_orig_w
        and dt_mesano_referencia = trunc(dt_liberacao_w,'mm')
        and nr_seq_roupa = nr_seq_roupa_movto_w;
      else
        update  rop_saldo_roupa
        set qt_saldo = 0
        where nr_seq_local = nr_seq_local_orig_w
        and dt_mesano_referencia = trunc(dt_liberacao_w,'mm')
        and nr_seq_roupa = nr_seq_roupa_movto_w;
      end if;      
    elsif (ds_operacao_w = 'TS') and
      (nr_seq_local_orig_w is not null) and
      (nr_seq_local_dest_w is not null)  then
      select nvl(qt_saldo_local_dest_w - qt_roupa_w, 0)
      into qt_saldo_w
      from dual;
      if (qt_saldo_w > 0) then
        update  rop_saldo_roupa
        set qt_saldo = qt_saldo_w
        where nr_seq_local = nr_seq_local_dest_w
        and dt_mesano_referencia = trunc(dt_liberacao_w,'mm')
        and nr_seq_roupa = nr_seq_roupa_movto_w;
      else
        update  rop_saldo_roupa
        set qt_saldo = 0
        where nr_seq_local = nr_seq_local_dest_w
        and dt_mesano_referencia = trunc(dt_liberacao_w,'mm')
        and nr_seq_roupa = nr_seq_roupa_movto_w;
      end if;
      update  rop_saldo_roupa
      set qt_saldo = qt_saldo_local_orig_w + qt_roupa_w
      where nr_seq_local = nr_seq_local_orig_w
      and dt_mesano_referencia = trunc(dt_liberacao_w,'mm')
      and nr_seq_roupa = nr_seq_roupa_movto_w;      
    end if;

    if  (ds_operacao_w <> 'B') then
      update  rop_roupa
      set nm_usuario = nm_usuario_p,
        nr_seq_local = nr_seq_local_orig_w
      where nr_sequencia = nr_seq_roupa_movto_w;
    else
      update  rop_roupa
      set dt_baixa  = null,
        cd_motivo_baixa = null,
        ie_situacao = 'A',
        dt_atualizacao  = sysdate,
        cd_local_baixa  = null
      where nr_sequencia  = nr_seq_roupa_movto_w;
    end if;
    end;
  else
    begin
    rollback;
    ds_erro_p := wheb_mensagem_pck.get_texto(1055721,'CD_MATERIAL=' || cd_material_w || ';CD_BARRAS_ROUPA=' || cd_barra_roupa_w);
    return;
    end;
  end if;
  end;
end loop;

update  rop_lote_movto
set nm_usuario  = nm_usuario_p,
  dt_liberacao  = null
where nr_sequencia  = nr_seq_lote_movto_p;

commit;

end rop_estornar_lib_movto_roupa;
/
