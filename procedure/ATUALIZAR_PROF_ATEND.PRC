create or replace procedure atualizar_prof_atend(	nr_atendimento_p	number,
					nm_usuario_p		varchar2) is

cd_pessoa_fisica_w		varchar2(10);
ie_profissional_w		varchar2(10);


				
begin
	
select 	case max(ie_tipo_evolucao)
	when '1'  then 'M'
	when '3'  then 'E'
	when '4'  then 'N'
	when '5'  then 'P'
	when '8'  then 'F'
	when '10' then 'FI'	   
	else null end,
	cd_pessoa_fisica
into 	ie_profissional_w, 
	cd_pessoa_fisica_w
from 	usuario
where 	nm_usuario = nm_usuario_p
group by cd_pessoa_fisica;					 

if (ie_profissional_w is not null) then 
	begin
	atualizar_atend_profissional(nr_atendimento_p,
				     cd_pessoa_fisica_w,
				     ie_profissional_w,
				     nm_usuario_p);
	end;
end if;
	 
	 
end atualizar_prof_atend;
/