create or replace
procedure pls_oc_cta_aplicar_fil_inter( 
		dados_regra_p			pls_tipos_ocor_pck.dados_regra,
		dados_filtro_p			pls_tipos_ocor_pck.dados_filtro,
		dados_consistencia_p		pls_tipos_ocor_pck.dados_consistencia,
		dados_forma_geracao_ocor_p	pls_tipos_ocor_pck.dados_forma_geracao_ocor,
		ie_incidencia_regra_p		varchar2,
		nr_id_transacao_p		pls_selecao_ocor_cta.nr_id_transacao%type,
		cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
		nm_usuario_p			usuario.nm_usuario%type,
		qt_registro_selecao_p		pls_integer default null) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Respons�vel por aplicar os filtros para as contas de interc�mbio da regra da ocorr�ncia.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:

N�o incluir restri��es nessa procedure, ela � respons�vel apenas  por passar no cursor
dos filtros de conta e incluir na sele��o das contas que devem ter a ocorr�ncia gerada.

Respeitar o conceito de granularidade dos dados durante a montagem das restri��es, deve ser respeitado a 
ordem de acesso as tabelas, olhando sempre do maior n�vel para o menor, come�ando do lote para o protocolo,
da� para a conta, para os itens e enfim os participantes.

Altera��es:
------------------------------------------------------------------------------------------------------------------
jjung OS 601441 06/06/2013 - 
Altera��o:	Linha: 103
	Alterado par�metro ie_tipo_registro_p  da rotina 
	PLS_TIPOS_OCOR_PCK.GERENCIA_SELECAO_REGISTROS para usar o valor
	do parameto ie_incidencia_regra_p.
Motivo:
	Para que a ocorr�ncia seja lan�ada para o item ou para a conta de maneira correta.
------------------------------------------------------------------------------------------------------------------
jjung OS 601977 10/06/2013 -
Altera��o: 	Linhas 60 e 113 - Inclu�do tratamento para alimenta��o do campo ie_valido_temp

Motivo: 	Para que quando o registro for v�lido para um filtro e n�o for v�lido para o outro
	o campo ie_valido da tabela de selecao fique como N para que n�o seja gerada 
	ocorrencia para o item.
------------------------------------------------------------------------------------------------------------------
jjung OS 601998 14/06/2013 -
Altera��o: 	Alterado forma de aplicar os filtros para usar a restri��o  dentro dos select que ser� obtido
	e n�o concatenar a restri��o no select padr�o.

Motivo: 	Quando a valida��o for aplicada em procedimentos ou materiais para que seja poss�vel aplicar
	os filtros corretos conforme os cadastros do usu�rio .
------------------------------------------------------------------------------------------------------------------
jjung OS 602057 18/06/2013 - 

Altera��o:	Foi inclu�do par�metro na procedure pls_tipos_ocor_pck.gerencia_selecao_registros
	para utilizar retorno da function pls_tipos_ocor_pck.obter_se_valido.
	
Motivo:	A l�gica deste tratamento foi removida da procedure de gerenciamento de selecao dos registros
	para que evitasse problemas e confus�o nas valida��es.
------------------------------------------------------------------------------------------------------------------
jjung 29/06/2013 

Altera��o:	Adicionado parametro nos m�todos de atualiza��o dos campos IE_VALIDO e IE_VALIDO_TEMP 
	da PLS_TIPOS_OCOR_PCK
	
Motivo:	Se tornou necess�rio diferenciar os filtros das valida��es na hora de realizar esta opera��o
	para que os filtros de exce��o funcionem corretamente.
------------------------------------------------------------------------------------------------------------------
jjung 10/07/2013 

Altera��o:	Modificado lugar de chamada da atualiza��o dos campos ie_valido_temp e ie_valido da tabela
	de sele��o para fora do for.
	
Motivo:	Foi identificado que quando um filtro tinha mais de uma regrinha cadastrado para ele n�o estava 
	funcionando corretamente.
------------------------------------------------------------------------------------------------------------------
jjung OS 604666 - 29/07/2013 -

Altera��o:	Substitu�do rotinas de manipula��o de restri��es e selects din�micos para as novas:
	PLS_OC_CTA_MONTAR_SEL_PAD e PLS_OC_CTA_OBTER_RESTR_PADRAO.
	
Motivo:	Surgiu a necessidade de separar as restri��es devido a cria��o de reconsist�ncia das ocorr�ncias
	durante a��es no processo de an�lise. Como deparamos com um cen�rio diferente do anterior
	decidimos que seriam criadas novas rotinas para n�o estragarmos o estado atual e para que pud�ssemos
	atender da melhor forma as novas necessidades das ocorr�ncias.
------------------------------------------------------------------------------------------------------------------
jjung OS 651768 - 24/10/2013 - 

Altera��o:	Substitu�do a forma de grava��o dos registros no banco para ser feito com arrays e utilizar
	a estrutura do DEFINE_ARRAY para alimentar as listas em apenas um comando COLUMN_VALUE
	
Motivo:	Foi identificado que grande parte do consumo de recursos da gera��o da ocorr�ncia combinada
	estava sendo consumido em acessos ao banco para gravar e atualizar a tabela de sele��o. Foi utilizado 
	ent�o o comando FORALL que grava e atualiza todos os registros de uma lista no banco em apenas um 
	acesso ao contexto SQL.
------------------------------------------------------------------------------------------------------------------
jjung OS 666950 - 13/11/2013 - 

Altera��o:	Foi recolocado a chamada para a procedure PLS_TIPOS_OCOR_PCK.ATUALIZA_SEL_IE_VALIDO_TEMP.

Motivo:	Foi identificado que a falta da execu��o desta rotina ocasionava em falhas no processo
	de gera��o da ocorr�ncia onde existiam dois filtros no n�vel da conta, onde por conta  o registro era v�lido
	e pela outra caracter�stica o item n�o era atendido, desta forma ainda estava sendo gerada a ocorr�ncia.
------------------------------------------------------------------------------------------------------------------
jjung OS 709376 - 03/03/2014
 
Altera��o:	Adicionado par�metro dados_forma_geracao_p para passar at� na pls_oc_cta_obter_restr_padrao

Motivo:	Como no campo Consist�ncia web na regra pode ser informado Ambos o mais correto � tratar
	pelo evento sendo executado e n�o pelo evento informado na regra.
 ------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

dados_filtro_interc_w	pls_tipos_ocor_pck.dados_filtro_interc;			

select_completo_w	varchar2(8000);
dados_restricao_w	pls_tipos_ocor_pck.dados_restricao_select;

var_cur_w 		pls_integer;
var_exec_w		pls_integer;
var_retorno_w		pls_integer;
qt_cnt_w		pls_integer;

nr_seq_conta_w		dbms_sql.number_table;
nr_seq_conta_proc_w	dbms_sql.number_table;
nr_seq_conta_mat_w	dbms_sql.number_table;
nr_seq_selecao_w	dbms_sql.clob_table;
ds_observacao_w		dbms_sql.varchar2_table;

ret_null_w		varchar2(1);

-- FIltros informados
Cursor C_filtro
	(nr_seq_filtro_pc	pls_oc_cta_filtro.nr_sequencia%type) is
	select	a.ie_conta_intercambio,
		a.ie_tipo_contrato_intercambio,
		a.ie_tipo_intercambio,
		a.nr_seq_congenere,
		a.nr_seq_intercambio,
		a.sg_estado_operadora
	from	pls_oc_cta_filtro_interc a
	where	a.nr_seq_oc_cta_filtro	= nr_seq_filtro_pc;
begin

if	(dados_filtro_p.nr_sequencia is not null) then

	-- Obter o controle padr�o para quantidade de registros que ser� enviada a cada vez para a tabela de sele��o.
	qt_cnt_w := pls_cta_consistir_pck.qt_registro_transacao_w;
	
	-- Atualizar o campo ie_valido_temp.
	pls_tipos_ocor_pck.atualiza_sel_ie_valido_temp(nr_id_transacao_p, dados_filtro_p, 'F', dados_regra_p);
	
	-- Passar para todos os filtros da regra.
	for	r_C_filtro_w in C_filtro(dados_filtro_p.nr_sequencia) loop

		-- Atualizar a vari�vel com os dados do filtro
		dados_filtro_interc_w.ie_conta_intercambio            	:= r_C_filtro_w.ie_conta_intercambio;
		dados_filtro_interc_w.ie_tipo_contrato_intercambio    	:= r_C_filtro_w.ie_tipo_contrato_intercambio;
		dados_filtro_interc_w.ie_tipo_intercambio             	:= r_C_filtro_w.ie_tipo_intercambio;
		dados_filtro_interc_w.nr_seq_congenere                	:= r_C_filtro_w.nr_seq_congenere;
		dados_filtro_interc_w.nr_seq_intercambio              	:= r_C_filtro_w.nr_seq_intercambio;
		dados_filtro_interc_w.sg_estado_operadora		:= r_C_filtro_w.sg_estado_operadora;
		
		-- Obter restri��es 
		dados_restricao_w := pls_oc_cta_obter_restr_padrao(
						'RESTRICAO', dados_consistencia_p, nr_id_transacao_p, dados_regra_p,
						ie_incidencia_regra_p, null, dados_filtro_p, dados_forma_geracao_ocor_p,
						cd_estabelecimento_p, nm_usuario_p, 'S', 'N', qt_registro_selecao_p);
							
		-- Como a restri��o por intercambio � aplic�vel para o n�vel de conta ent�o a restri��o montada pelo filtro ser� aplicada juntamente com a restri��o padr�o por conta, pois conforme 
		-- a regra de granularidade eliminando a conta n�o precisamos verificar os itens, portanto cada tipo de filtro deve ser aplicado ao seu n�vel.
		dados_restricao_w.ds_restricao_conta := dados_restricao_w.ds_restricao_conta || 
							pls_oc_cta_obter_restr_interc('RESTRICAO',dados_regra_p,var_cur_w,dados_filtro_interc_w);
		
		-- Montar o select a ser executado dinamicamente
		select_completo_w := pls_tipos_ocor_pck.montar_select_padrao(dados_regra_p, dados_filtro_p, ie_incidencia_regra_p, dados_restricao_w, nm_usuario_p);
		
		-- Abrir um novo cursor
		var_cur_w := dbms_sql.open_cursor;
		begin
			-- Criar o cursor
			dbms_sql.parse(var_cur_w, select_completo_w, 1);
			
			-- Trocar BINDS 
			-- Do select original 
			dados_restricao_w := pls_oc_cta_obter_restr_padrao(
							'BINDS', dados_consistencia_p, nr_id_transacao_p, dados_regra_p, 
							ie_incidencia_regra_p, var_cur_w, dados_filtro_p, dados_forma_geracao_ocor_p,
							cd_estabelecimento_p, nm_usuario_p, 'S', 'N', qt_registro_selecao_p);
			
			-- Trocar binds do select Benef
			ret_null_w := pls_oc_cta_obter_restr_interc('BIND', dados_regra_p, var_cur_w, dados_filtro_interc_w);
			
			-- Definir para o DBMS_SQL que o retorno do select ser�  preenchido em arrays, definindo a quantidade de linhas que o array ter� a cada itera��o do loop
			-- e a posi��o inicial que estes ocupar�o no array.
			dbms_sql.define_array(var_cur_w, 1, nr_seq_conta_w, qt_cnt_w, 1);
			dbms_sql.define_array(var_cur_w, 2, nr_seq_conta_proc_w, qt_cnt_w, 1);
			dbms_sql.define_array(var_cur_w, 3, nr_seq_conta_mat_w, qt_cnt_w, 1);
			dbms_sql.define_array(var_cur_w, 4, ds_observacao_w, qt_cnt_w, 1);
			dbms_sql.define_array(var_cur_w, 5, nr_seq_selecao_w, qt_cnt_w, 1);
			
			var_exec_w := dbms_sql.execute(var_cur_w);
			loop
			-- O fetch rows ir� preencher os buffers do Oracle com as linhas que ser�o passadas para a lista quando o COLUMN_VALUE for chamado.
			var_retorno_w := dbms_sql.fetch_rows(var_cur_w);
			
				-- zerar as listas para que o mesmo valor n�o seja inserido mais de uma vez na tabela.
				nr_seq_conta_w.delete;
				nr_seq_conta_proc_w.delete;
				nr_seq_conta_mat_w.delete;
				ds_observacao_w.delete;
				nr_seq_selecao_w.delete;
			
				-- Obter as listas que foram populadas.
				dbms_sql.column_value(var_cur_w, 1, nr_seq_conta_w);
				dbms_sql.column_value(var_cur_w, 2, nr_seq_conta_proc_w);
				dbms_sql.column_value(var_cur_w, 3, nr_seq_conta_mat_w);
				dbms_sql.column_value(var_cur_w, 4, ds_observacao_w);
				dbms_sql.column_value(var_cur_w, 5, nr_seq_selecao_w);
				
				-- Insere todos os registros das listas na tabela de sele��o em um �nico insert.
				pls_tipos_ocor_pck.gerencia_selecao(	nr_id_transacao_p, nr_seq_conta_w ,
									nr_seq_conta_proc_w, nr_seq_conta_mat_w,
									nr_seq_selecao_w, ds_observacao_w, 'S', 
									nm_usuario_p, 'F', dados_filtro_p, dados_regra_p);
									
				-- Quando n�mero de linhas que foram aplicadas no array for diferente do definido significa que esta foi a �ltima itera��o do loop e que todas as linhas foram
				-- passadas.
				exit when var_retorno_w != qt_cnt_w;
			end loop; -- Contas filtradas
			dbms_sql.close_cursor(var_cur_w);
		exception
			when others then
			
			-- Fechar os cursores que continuam abertos, os cursores que utilizam FOR - LOOP n�o necessitam serem fechados, ser�o fechados autom�ticamente.
			-- Contas.
			if	(dbms_sql.is_open(var_cur_w)) then
			
				dbms_sql.close_cursor(var_cur_w);
			end if;
			
			-- Insere o log na tabela e aborta a opera��o
			pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p,select_completo_w,nr_id_transacao_p,nm_usuario_p);
		end;
		
	end loop;--C_filtro
	
	-- Atualiza o campo ie_valido da tabela PLS_SELECAO_OCOR_CTA para N aonde o ie_valido_temp continuar N
	pls_tipos_ocor_pck.atualiza_sel_ie_valido(nr_id_transacao_p, dados_filtro_p, 'F', dados_regra_p);
end if;
end pls_oc_cta_aplicar_fil_inter;
/