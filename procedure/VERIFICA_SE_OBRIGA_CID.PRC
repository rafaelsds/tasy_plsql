create or replace
procedure verifica_se_obriga_cid (	nr_atendimento_p	in number,
					cd_motivo_alta_p	in number,
					ds_retorno_p		out varchar2)
					is

ie_tipo_atendimento_w	number(3,0);
ie_exige_cid_w		varchar2(1) := 'N';
cd_setor_atendimento_w	number(10);
cd_convenio_w		number(5);
nr_sequencia_w		Number(10);
ie_tipo_diag_regra_w	Number(10);
ie_permite_w		varchar2(1);
ds_retorno_w		varchar2(255);
cursor c01 is
	select	nr_sequencia,
		nvl(ie_obriga,'S')
	from	regra_obriga_cid
	where	nvl(ie_tipo_atendimento,ie_tipo_atendimento_w)		= ie_tipo_atendimento_w
	and	nvl(cd_setor_atendimento,cd_setor_atendimento_w) 	= cd_setor_atendimento_w
	and	nvl(Cd_motivo_alta,nvl(cd_motivo_alta_p,0))	 	= nvl(cd_motivo_alta_p,0)
	and     nvl(cd_convenio,cd_convenio_w)				= cd_convenio_w
	order by  cd_motivo_alta desc;
begin

if	(nr_atendimento_p is not null) then

	select	nvl(max(ie_tipo_atendimento),0),
		nvl(max(cd_setor_atendimento),0),
		nvl(max(cd_convenio),0)
	into	ie_tipo_atendimento_w,
		cd_setor_atendimento_w,
		cd_convenio_w
	from	resumo_atendimento_paciente_v
	where	nr_atendimento		=	nr_atendimento_p;

open C01;
loop
fetch C01 into	
	nr_sequencia_w,
	ie_exige_cid_w;
exit when C01%notfound;
	begin
	nr_sequencia_w  :=	nr_sequencia_w;
	ie_exige_cid_w	:=	ie_exige_cid_w;
	end;
end loop;
close C01;

end if;

if (nvl(nr_sequencia_w,0) > 0) then
	
	select	nvl(max(ie_tipo_diag_regra),0)
	into	ie_tipo_diag_regra_w
	from	regra_obriga_cid
	where	nr_sequencia = nr_sequencia_w;
	
	if (nvl(ie_tipo_diag_regra_w,0) = 1) then		
	
		select	decode(count(*),0,'N','S')
		into	ie_permite_w
		from	diagnostico_medico
		where	nr_atendimento = nr_atendimento_p
		and	ie_tipo_diagnostico = 1;

		if (ie_permite_w = 'N') then
			ds_retorno_w 	:= obter_desc_expressao(729723)|| nr_sequencia_w || chr(13) || chr(10);
		end if;
	elsif (nvl(ie_tipo_diag_regra_w,0) = 2) then

		select	decode(count(*),0,'N','S')
		into	ie_permite_w
		from	diagnostico_medico
		where	nr_atendimento = nr_atendimento_p
		and	ie_tipo_diagnostico = 2;

		if (ie_permite_w = 'N') then
			ds_retorno_w 	:= obter_desc_expressao(729723)|| nr_sequencia_w || chr(13) || chr(10);
		end if;
	elsif (nvl(ie_tipo_diag_regra_w,0) = 999) then

		select	decode(count(*),0,'N','S')
		into	ie_permite_w
		from	diagnostico_medico
		where	nr_atendimento = nr_atendimento_p;

		if (ie_permite_w = 'N') then
			ds_retorno_w 	:= obter_desc_expressao(729723)|| nr_sequencia_w || chr(13) || chr(10);
		end if;
	end if;
end if;
if	(ie_exige_cid_w = 'S') and 
	(nvl(ie_tipo_diag_regra_w,0) <> 999) and
	(ds_retorno_w is null) then
	ds_retorno_w :=  obter_desc_expressao(510483);
end if;
ds_retorno_p := ds_retorno_w;

end verifica_se_obriga_cid;
/
