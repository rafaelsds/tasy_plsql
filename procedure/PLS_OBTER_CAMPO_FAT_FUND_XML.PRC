create or replace
procedure pls_obter_campo_fat_fund_xml(	nm_campo_conversao_p	in	pls_fat_regra_prest.nm_campo%type,
					ds_valor_original_p	in	pls_fat_regra_prest.ds_valor_original%type,
					ds_valor_convertido_p	out	pls_fat_regra_prest.ds_valor_convertido%type,
					cd_estabelecimento_p	in	estabelecimento.cd_estabelecimento%type) is
					
ds_valor_original_w		pls_fat_regra_prest.ds_valor_original%type;
ds_valor_convertido_w		pls_fat_regra_prest.ds_valor_convertido%type;
ds_valor_convertido_aux_w	pls_fat_regra_prest.ds_valor_convertido%type;

begin

ds_valor_original_w		:= upper(ds_valor_original_p);
ds_valor_convertido_aux_w	:= ds_valor_original_p;

select	max(ds_valor_convertido)
into	ds_valor_convertido_w
from	pls_fat_regra_prest
where	nm_campo			= nm_campo_conversao_p
and	upper(ds_valor_original)	= ds_valor_original_w
and	cd_estabelecimento		= cd_estabelecimento_p;

if	(ds_valor_convertido_w is not null) then
	ds_valor_convertido_aux_w	:= ds_valor_convertido_w;
end if;

ds_valor_convertido_p	:= ds_valor_convertido_aux_w;

end pls_obter_campo_fat_fund_xml;
/