create or replace
procedure pls_gerar_lote_reenvio_trans
			(	nr_seq_guia_p			Number,
				nr_seq_requisicao_p		Number,
				nr_seq_lote_p			Number,
				nm_usuario_p			Varchar2,
				nr_seq_lote_novo_p	out	Number) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar lote de reenvio das transa��es do SCS pela fun��o OPS - Manuten��o de Processos Operacionais do Autorizador.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
nr_seq_pedido_reenv_w		Number(10);
ie_tipo_guia_w			Varchar2(255);
nr_seq_prestador_exec_w		Number(10);
nr_seq_segurado_w		Number(10);
ie_tipo_item_w			Varchar2(255);
cd_proc_mat_w			Varchar2(255);
ds_proc_mat_w			Varchar2(255);
qt_aprovada_w			Number(10);

Cursor C01 is
	select	'P',
		to_char(cd_procedimento),
		substr(obter_descricao_procedimento(cd_procedimento,ie_origem_proced),1,255),
		qt_solicitado
	from	pls_requisicao_proc
	where	nr_seq_requisicao	= nr_seq_requisicao_p
	union
	select	'M',
		substr(pls_obter_seq_codigo_material(nr_seq_material,''),1,255),
		substr(pls_obter_desc_material(nr_seq_material),1,255),
		qt_solicitado
	from	pls_requisicao_mat
	where	nr_seq_requisicao	= nr_seq_requisicao_p;
	
Cursor C02 is
	select	'P',
		to_char(cd_procedimento),
		substr(obter_descricao_procedimento(cd_procedimento,ie_origem_proced),1,255),
		qt_solicitada
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_p
	union
	select	'M',
		substr(pls_obter_seq_codigo_material(nr_seq_material,''),1,255),
		substr(pls_obter_desc_material(nr_seq_material),1,255),
		qt_solicitada
	from	pls_guia_plano_mat
	where	nr_seq_guia	= nr_seq_guia_p;

begin

if	(nr_seq_requisicao_p	is not null) then
	select	nr_seq_segurado,
		nr_seq_prestador_exec,
		ie_tipo_guia
	into	nr_seq_segurado_w,
		nr_seq_prestador_exec_w,
		ie_tipo_guia_w
	from	pls_requisicao
	where	nr_sequencia	= nr_seq_requisicao_p;
elsif	(nr_seq_guia_p	is not null) then
	select	nr_seq_segurado,
		nr_seq_prestador,
		ie_tipo_guia
	into	nr_seq_segurado_w,
		nr_seq_prestador_exec_w,
		ie_tipo_guia_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_p;
end if;

if	(nr_seq_lote_p	is null) then
	select	pls_lote_reenvio_scs_seq.NextVal
	into	nr_seq_lote_novo_p
	from	dual;

	insert	into pls_lote_reenvio_scs
		(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec,
		 dt_reenvio, nm_usuario, nm_usuario_nrec)
	values	(nr_seq_lote_novo_p, sysdate, sysdate,
		 sysdate, nm_usuario_p, nm_usuario_p);
elsif	(nr_seq_lote_p	is not null) then
	select	pls_pedido_reenvio_scs_seq.NextVal
	into	nr_seq_pedido_reenv_w
	from	dual;
	
	insert	into pls_pedido_reenvio_scs
		(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec,
		 ie_tipo_guia, nm_usuario, nm_usuario_nrec,
		 nr_seq_lote_reenvio, nr_seq_prestador, nr_seq_requisicao,
		 nr_seq_guia, nr_seq_segurado, ie_estagio)
	values	(nr_seq_pedido_reenv_w, sysdate, sysdate,
		 ie_tipo_guia_w, nm_usuario_p, nm_usuario_p,
		 nr_seq_lote_p, nr_seq_prestador_exec_w, nr_seq_requisicao_p,
		 nr_seq_guia_p, nr_seq_segurado_w, 1);
	
	if	(nr_seq_requisicao_p	is not null) then
		open C01;
		loop
		fetch C01 into
			ie_tipo_item_w,
			cd_proc_mat_w,
			ds_proc_mat_w,
			qt_aprovada_w;
		exit when C01%notfound;
			begin
			insert	into pls_itens_reenvio_scs
				(nr_sequencia, cd_proc_mat, ds_proc_mat,
				 dt_atualizacao, dt_atualizacao_nrec, ie_tipo_item,
				 nm_usuario, nm_usuario_nrec, nr_seq_ped_reenvio_scs,
				 qt_aprovado)
			values	(pls_itens_reenvio_scs_seq.NextVal, cd_proc_mat_w, ds_proc_mat_w,
				 sysdate, sysdate, ie_tipo_item_w,
				 nm_usuario_p, nm_usuario_p, nr_seq_pedido_reenv_w,
				 qt_aprovada_w);
			end;
		end loop;
		close C01;
	elsif	(nr_seq_guia_p	is not null) then
		open C02;
		loop
		fetch C02 into
			ie_tipo_item_w,
			cd_proc_mat_w,
			ds_proc_mat_w,
			qt_aprovada_w;
		exit when C02%notfound;
			begin
			insert	into pls_itens_reenvio_scs
				(nr_sequencia, cd_proc_mat, ds_proc_mat,
				 dt_atualizacao, dt_atualizacao_nrec, ie_tipo_item,
				 nm_usuario, nm_usuario_nrec, nr_seq_ped_reenvio_scs,
				 qt_aprovado)
			values	(pls_itens_reenvio_scs_seq.NextVal, cd_proc_mat_w, ds_proc_mat_w,
				 sysdate, sysdate, ie_tipo_item_w,
				 nm_usuario_p, nm_usuario_p, nr_seq_pedido_reenv_w,
				 qt_aprovada_w);
			end;
		end loop;
		close C02;
	end if;
	
	pls_gravar_hist_reenvio_scs(nr_seq_guia_p, nr_seq_requisicao_p, nm_usuario_p);
end if;

commit;

end pls_gerar_lote_reenvio_trans;
/