CREATE OR REPLACE
PROCEDURE Agrupar_Protocolo_Exclusao(	nr_seq_protocolo_p		number) is 


Cursor C01 is
	select	/*+ index (TISSLOG_PROCONV_FK_I)*/
		nr_sequencia
	from	tiss_log
	where	nr_seq_protocolo	= nr_seq_protocolo_p;
	
nr_sequencia_w		number(15);
BEGIN

open C01;
loop
fetch C01 into	
	nr_sequencia_w;
exit when C01%notfound;
	begin
		
	delete /*+ index( a TISLOXM_TISSLOG_FK_I)*/from  tiss_log_xml a
	where	a.nr_seq_log_tiss		= nr_sequencia_w;
	
	delete /*+ index( a TISLVXM_TISSLOG_FK_I)*/ from tiss_log_valor_xml a
	where	a.nr_seq_log_tiss		= nr_sequencia_w;
	
	end;
end loop;
close C01;

delete 	/*+index(a TISSLOG_PROCONV_FK_I)*/ from tiss_log a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p;

delete	/*+ index(a PROCONV_PK)*/from protocolo_convenio a
where	a.nr_seq_protocolo	= nr_seq_protocolo_p;

commit;

END Agrupar_Protocolo_Exclusao;
/