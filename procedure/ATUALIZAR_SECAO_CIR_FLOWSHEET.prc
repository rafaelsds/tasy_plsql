create or replace procedure atualizar_secao_cir_flowsheet(
			nr_sequencia_p number,  ie_checado_p 	varchar2) is 
begin
  update w_flowsheet_config_grupo set dt_atualizacao = sysdate,
										nm_usuario =  WHEB_USUARIO_PCK.GET_NM_USUARIO,
										ie_modo_visualizacao = decode(ie_checado_p, 'S', 'VI', 'IV')
    where nr_sequencia =  nr_sequencia_p;
commit;

end atualizar_secao_cir_flowsheet;
/