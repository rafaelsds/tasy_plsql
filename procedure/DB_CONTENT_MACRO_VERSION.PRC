create or replace procedure db_content_macro_version(
    nr_seq_macro_p NUMBER,
    nm_usuario_p   VARCHAR2,
    nr_seq_new_version_p out number)  
IS
  nr_seq_w db_content_macro.nr_sequencia%type;
BEGIN

  SELECT db_content_macro_seq.nextval INTO nr_seq_w FROM dual;
  
  INSERT
  INTO db_content_macro
    (
      nr_sequencia,
      nm_usuario_nrec,
      dt_atualizacao_nrec,
      nm_usuario,
      dt_atualizacao,
      ds_macro,
      ie_macro_type,
      ds_description,
      ds_sql_command,
      nr_version
    )
  SELECT nr_seq_w,
    nm_usuario_p,
    sysdate,
    nm_usuario_p,
    sysdate,
    ds_macro,
    ie_macro_type,    
    ds_description,
    ds_sql_command,
    (select max (nr_version) +1 from db_content_macro b where a.ds_macro = b.ds_macro)
  FROM db_content_macro a 
  WHERE nr_sequencia = nr_seq_macro_p;
  
  INSERT
  INTO db_content_macro_param
    (
      nr_sequencia,
      nr_seq_content_macro,
      nm_usuario_nrec,
      dt_atualizacao_nrec,
      nm_usuario,
      dt_atualizacao,
      ds_parameter
    )
  SELECT db_content_macro_param_seq.nextval,
    nr_seq_w,
    nm_usuario_p,
    sysdate,
    nm_usuario_p,
    sysdate,
    ds_parameter
  FROM db_content_macro_param
  WHERE nr_seq_content_macro = nr_seq_macro_p;
  
  COMMIT;
  
  nr_seq_new_version_p := nr_seq_w;
  
END db_content_macro_version;
/
