create or replace
procedure nova_cobranca_titulo(
				nr_titulo_p		number,
				vl_titulo_p		number,
				vl_saldo_titulo_p	number,
				dt_liquidacao_p		date,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				ds_mensagem_p	out	varchar2) is

nr_seq_cobranca_w	cobranca.nr_sequencia%type;
ie_baixa_cobranca_w	tipo_recebimento.ie_baixa_cobranca%type;
vl_recebido_w		titulo_receber_liq.vl_recebido%type;
vl_saldo_acobrar_w	cobranca.vl_acobrar%type;
vl_cambial_passivo_w	titulo_receber_liq.vl_cambial_passivo%type;

begin

if (nr_titulo_p is not null) then
	insert	into cobranca
			(cd_estabelecimento,
			cd_portador,
			cd_tipo_portador,
			dt_atualizacao,
			dt_inclusao,
			dt_previsao_cobranca,
			ie_status,
			nm_usuario, 
			nr_sequencia,
			nr_titulo,
			vl_acobrar,
			vl_original)
	select		cd_estabelecimento_p,
			a.cd_portador, 
			a.cd_tipo_portador,
			sysdate,
			sysdate, 
			sysdate, 
			'P',
			nm_usuario_p,
			cobranca_seq.nextval,
			a.nr_titulo, 
			a.vl_saldo_titulo, 
			a.vl_saldo_titulo
	from	titulo_receber a 
	where	a.nr_titulo = nr_titulo_p;
end if;
	
select	max(a.nr_sequencia)
into	nr_seq_cobranca_w
from	cobranca a
where	a.nr_titulo = nr_titulo_p;

select	max(a.ie_baixa_cobranca),
	max(b.vl_recebido),
	max(b.vl_cambial_passivo)
into	ie_baixa_cobranca_w,
	vl_recebido_w,
	vl_cambial_passivo_w
from	titulo_receber_liq b,
	tipo_recebimento a
where	a.cd_tipo_recebimento	= b.cd_tipo_recebimento
and	b.nr_titulo		= nr_titulo_p;

if (ie_baixa_cobranca_w = 'S') then
	if (vl_recebido_w > 0) then
		vl_saldo_acobrar_w :=  vl_titulo_p - vl_recebido_w - nvl(vl_cambial_passivo_w,0);
	
		update	cobranca
		set	vl_original	= vl_titulo_p,
			vl_acobrar	= vl_saldo_acobrar_w
		where	nr_sequencia	= nr_seq_cobranca_w;
	else
		update	cobranca
		set	vl_original	= vl_titulo_p,
			vl_acobrar	= vl_recebido_w
		where	nr_sequencia	= nr_seq_cobranca_w;
	end if;
end if;

if (dt_liquidacao_p is not null) and
   (vl_saldo_titulo_p = 0) then
	update	cobranca
	set	vl_original	= vl_titulo_p,
		ie_status	= 'E'
	where	nr_sequencia	= nr_seq_cobranca_w;
end if;

if (nr_seq_cobranca_w is null) then
	ds_mensagem_p := obter_texto_tasy(177396, wheb_usuario_pck.get_nr_seq_idioma);
else
	ds_mensagem_p := obter_texto_dic_objeto(177401, wheb_usuario_pck.get_nr_seq_idioma, 'NR_SEQ_COBRANCA='||nr_seq_cobranca_w);
end if;

commit;

end nova_cobranca_titulo;
/