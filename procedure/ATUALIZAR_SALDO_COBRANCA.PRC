create or replace
procedure atualizar_saldo_cobranca(	
		nr_seq_cobranca_p		number,
		nm_usuario_p		varchar2,
		ie_commit_p		varchar2) is

nr_titulo_w		number(10);
vl_titulo_original_w		number(15,2);
vl_saldo_w		number(15,2);
cd_estabelecimento_w	number(4);
ie_saldo_cob_w		varchar2(255)	:= 'N';

dt_recebimento_w		date;
vl_recebido_w		number(15,2);
vl_juros_w		number(15,2);
vl_multa_w		number(15,2);
vl_desconto_w		number(15,2);
vl_glosa_w		number(15,2);
vl_perdas_w		number(15,2);
vl_tributo_w		number(15,2);
vl_alteracao_w		number(15,2);
vl_titulo_w		number(15,2);
vl_nota_credito_w		number(15,2);
vl_recup_perda_w		number(15,2);
ie_encerrar_cob_liq_w	varchar2(5)	:= 'N';
ie_nota_credito_cobr_w	varchar2(1);
vl_cambial_passivo_w	titulo_receber_liq.vl_cambial_passivo%type;

begin

select	cd_estabelecimento,
	nr_titulo
into	cd_estabelecimento_w,
	nr_titulo_w
from	cobranca
where	nr_sequencia	= nr_seq_cobranca_p;

select	nvl(max(ie_saldo_cob),'N'),
	nvl(max(ie_encerrar_cob_liq),'N'),
	nvl(max(ie_nota_credito_cobr),'N')
into	ie_saldo_cob_w,
	ie_encerrar_cob_liq_w,
	ie_nota_credito_cobr_w
from	parametro_contas_receber
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_saldo_cob_w in ('S','P')) then

	if	(nr_titulo_w is not null) then
		begin
		if	(ie_saldo_cob_w = 'S') then
			select	nvl(max(dt_recebimento),sysdate),
				nvl(sum(vl_recebido),0),
				nvl(sum(vl_juros),0),
				nvl(sum(vl_multa),0),
				nvl(sum(vl_descontos),0),
				nvl(sum(vl_glosa),0),
				nvl(sum(vl_perdas),0),
				nvl(sum(vl_nota_credito),0),
				nvl(sum(vl_cambial_passivo),0)
			into 	dt_recebimento_w,
				vl_recebido_w,
				vl_juros_w,
				vl_multa_w,
				vl_desconto_w,
				vl_glosa_w,
				vl_perdas_w,
				vl_nota_credito_w,
				vl_cambial_passivo_w
			from	tipo_recebimento b,
				titulo_receber_liq a
			where	a.cd_tipo_recebimento	= b.cd_tipo_recebimento
			and	a.nr_titulo 		= nr_titulo_w
			and	nvl(ie_lib_caixa, 'S') = 'S';
		else
			select	nvl(max(dt_recebimento),sysdate),
				nvl(sum(vl_recebido),0),
				nvl(sum(vl_juros),0),
				nvl(sum(vl_multa),0),
				nvl(sum(vl_descontos),0),
				nvl(sum(vl_glosa),0),
				nvl(sum(vl_perdas),0),
				nvl(sum(vl_nota_credito),0),
				nvl(sum(vl_cambial_passivo),0)
			into 	dt_recebimento_w,
				vl_recebido_w,
				vl_juros_w,
				vl_multa_w,
				vl_desconto_w,
				vl_glosa_w,
				vl_perdas_w,
				vl_nota_credito_w,
				vl_cambial_passivo_w
			from	tipo_recebimento b,
				titulo_receber_liq a
			where	a.cd_tipo_recebimento	= b.cd_tipo_recebimento
			and	a.nr_titulo 		= nr_titulo_w
			and	((b.ie_tipo_consistencia <> 9) or 
				((b.ie_tipo_consistencia = 9) and exists (	
					select	1
					from	perda_contas_receber x
					where	x.nr_titulo = a.nr_titulo
					and	x.nr_seq_baixa = a.nr_sequencia
					and	nvl(x.ie_tipo_perda,'R') = 'I'
					and	not exists (	select	1
								from	perda_contas_receb_baixa y,
									fin_tipo_baixa_perda z
								where	y.nr_seq_perda = x.nr_sequencia
								and	y.nr_seq_tipo_baixa = z.nr_sequencia
								and	z.ie_tipo_consistencia = '4'))))
			and	nvl(ie_lib_caixa, 'S') = 'S';
		end if;
		
		if	(ie_nota_credito_cobr_w <> 'S') then
			vl_nota_credito_w := 0;
		end if;

		select	nvl(max(vl_titulo),0),
			nvl(max(vl_saldo_titulo),0)
		into	vl_titulo_original_w,
			vl_saldo_w
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

		select	nvl(sum(decode(ie_aumenta_diminui, 'A', vl_alteracao, 'D', vl_alteracao * -1)), 0)
		into	vl_alteracao_w
		from	alteracao_valor
		where	nr_titulo	=	nr_titulo_w;
	
		select	nvl(sum(vl_tributo),0)
		into	vl_tributo_w
		from	titulo_receber_trib
		where	nr_titulo	= nr_titulo_w
		and	nvl(ie_origem_tributo, 'C') = 'D';
		
		select	nvl(sum(b.vl_baixa),0)
		into	vl_recup_perda_w
		from	perda_contas_receber a,
			perda_contas_receb_baixa b,
			fin_tipo_baixa_perda c
		where	a.nr_sequencia = b.nr_seq_perda
		and	b.nr_seq_tipo_baixa = c.nr_sequencia
		and	c.ie_tipo_consistencia in (0,1)
		and	a.nr_titulo = nr_titulo_w;

		vl_titulo_w 	:=  vl_titulo_original_w + vl_alteracao_w - (vl_recebido_w + vl_glosa_w + vl_desconto_w + vl_perdas_w + vl_recup_perda_w + vl_nota_credito_w + nvl(vl_cambial_passivo_w,0)) - vl_tributo_w;

		update	cobranca
		set	vl_acobrar	= vl_titulo_w,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_cobranca_p;
		end;
	end if;
end if;


if	(ie_commit_p = 'S') then
	commit;
end if;

end atualizar_saldo_cobranca;
/