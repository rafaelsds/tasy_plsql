create or replace
procedure Consiste_Grupo_Quim_ATC(	cd_grupo_quimico_p		varchar2,
					ds_grupo_quimico_p		varchar2,
					cd_subcla_terap_p		varchar2,
					nm_usuario_p			varchar2) is

qt_registros_w		number(10);

begin

begin
select	count(*)
into	qt_registros_w
from	med_grupo_quim_farm
where	cd_grupo_quimico	= cd_grupo_quimico_p;
exception
	when others then
	qt_registros_w	:= 0;
end;

if	(qt_registros_w = 0) then
	insert into med_grupo_quim_farm (
		cd_grupo_quimico,
		dt_atualizacao,
		nm_usuario,
		ds_grupo_quimico,
		cd_subclasse_terapeutica,
		dt_atualizacao_nrec,
		nm_usuario_nrec
	) values (
		cd_grupo_quimico_p,
		sysdate,
		nm_usuario_p,
		ds_grupo_quimico_p,
		cd_subcla_terap_p,
		sysdate,
		nm_usuario_p
	);
else	
	update	med_grupo_quim_farm
	set	ds_grupo_quimico	= ds_grupo_quimico_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	cd_grupo_quimico	= cd_grupo_quimico_p;
end if;

commit;

end Consiste_Grupo_Quim_ATC;
/	