create or replace
procedure importar_w_pf_FUCS is

/* Estrutura cliente */

cd_sistema_ant_w		varchar2(20);
nm_pessoa_w			varchar2(255);
nm_pessoa_fisica_sem_acento_w	varchar2(60);
nr_prontuario_w			varchar2(100);
nr_cartao_nac_sus_w		varchar2(100);

identidade_w			varchar2(255);
nr_cpf_w			varchar2(255);
nr_pis_pasep_w			varchar2(11);
dt_nasc_w			date;
pai_w				varchar2(255);
mae_w				varchar2(255);
ie_sexo_w			varchar2(255);

ds_end_w			varchar2(255);
nr_end_w			varchar2(255);
ds_compl_w			varchar2(255);
ds_bairro_w			varchar2(255);
ds_munic_w			varchar2(255);
cd_cep_w			varchar2(255);
		
sg_estado_w			varchar2(255);
nr_telefone_w			varchar2(255);
ds_fone_adic_w			varchar2(255);

/* Internos */
cd_pessoa_fisica_w		varchar2(10);
nr_sequencia_w			number(5);
ie_erro_compl_pf_w		varchar2(1)	:= 'N';
ie_erro_pf_w			varchar2(1)	:= 'N';
qt_registro_w			number(10);
trigger_name_w			varchar2(255);
sql_errm_w			varchar2(2000);
qt_inserido_w			number(15,0);
ie_pos_barra_w			number(10);
ds_valor_dominio_w		varchar2(255);


Cursor c01 is
select	registro,
	nome,
	prontuario,
	cartao_sus,
	identidade,
	cpf,
	pis,
	nascimento,
	pai,
	mae,
	sexo,
	endereco,
	numero,
	complement,
	estado,
	bairro,
	cidade,
	cep,
	fone
from	w_fucs_pessoa_fisica
order by 2;

cursor c02 is
select	trigger_name
from	user_triggers
where	table_name = 'PESSOA_FISICA'
union all
select	trigger_name
from	user_triggers
where	table_name = 'COMPL_PESSOA_FISICA'
order by
	1;

cursor c03 is
select	trigger_name
from	user_triggers
where	status = 'DISABLED'
order by
	1;

begin

open c02;
loop
fetch c02 into
	trigger_name_w;
exit when c02%notfound;
	begin
	exec_sql_dinamico('OS125178_PF', 'alter trigger ' || trigger_name_w || ' disable');
	end;
end loop;
close c02;

begin

delete	from pessoa_fisica
where	nm_usuario_nrec = 'Importacao';
commit;

qt_inserido_w	:= 0;

open c01;
loop
fetch c01 into
	cd_sistema_ant_w,
	nm_pessoa_w,
	nr_prontuario_w,
	nr_cartao_nac_sus_w,
	identidade_w,
	nr_cpf_w,
	nr_pis_pasep_w,
	dt_nasc_w,
	pai_w,
	mae_w,
	ie_sexo_w,
	ds_end_w,
	nr_end_w,
	ds_compl_w,
	sg_estado_w,
	ds_bairro_w,
	ds_munic_w,
	cd_cep_w,
	nr_telefone_w;
exit when c01%notfound;
	ie_erro_pf_w		:= 'N';
	ds_fone_adic_w		:= '';
	qt_inserido_w		:= qt_inserido_w + 1;
	ds_valor_dominio_w	:= '';
	ie_sexo_w		:= trim(ie_sexo_w);
	if	(ie_sexo_w is not null) then
		ie_sexo_w		:= substr(ie_sexo_w,1,1);
	end if;
	if	(nr_telefone_w is not null) then
		begin
		ie_pos_barra_w	:= instr(nr_telefone_w,'/');
		
		if	(ie_pos_barra_w > 0) then
			ds_fone_adic_w	:= substr(nr_telefone_w,ie_pos_barra_w+1,length(nr_telefone_w));
			nr_telefone_w	:= substr(nr_telefone_w,1,ie_pos_barra_w-1);
		end if;
		end;
	end if;
	
	if	(nr_prontuario_w is not null) then
		nr_prontuario_w		:= somente_numero(substr(nr_prontuario_w,1,10));
	end if;
	if	(nr_cartao_nac_sus_w is not null) then
		nr_cartao_nac_sus_w	:= somente_numero(substr(nr_cartao_nac_sus_w,1,15));
	end if;
	if	(nr_end_w is not null) then
		nr_end_w	:= somente_numero(substr(nr_end_w,1,5));
	end if;
	
	/* Tratamento de tamanho */
	nm_pessoa_w			:= substr(nm_pessoa_w,1,60);
	mae_w				:= substr(mae_w,1,60);
	pai_w				:= substr(pai_w,1,60);
	identidade_w			:= substr(identidade_w,1,15);
	ds_end_w			:= substr(ds_end_w,1,40);
	nm_pessoa_fisica_sem_acento_w	:= elimina_acentuacao(nm_pessoa_w);
	sg_estado_w			:= substr(sg_estado_w,1,2);
	ds_bairro_w			:= substr(ds_bairro_w,1,40);
	ds_munic_w			:= substr(ds_munic_w,1,40);
	nr_telefone_w			:= substr(nr_telefone_w,1,15);
	ds_compl_w			:= substr(ds_compl_w,1,40);
	cd_cep_w			:= substr(cd_cep_w,1,15);
	
	
	if	(sg_estado_w is not null) then
	
		select	obter_valor_dominio(50,sg_estado_w)
		into	ds_valor_dominio_w
		from	dual;
		
		if	(ds_valor_dominio_w is null) then
			sg_Estado_w	:= null;
		end if;
	end if;
	
	if	(ie_erro_pf_w = 'N') then	
		begin

		select	pessoa_fisica_seq.nextval
		into	cd_pessoa_fisica_w
		from	dual;

		insert into pessoa_fisica(
			cd_pessoa_fisica,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			nm_pessoa_fisica,
			dt_nascimento,
			ie_sexo,
			nr_cpf,
			nr_identidade,
			ie_funcionario,
			cd_sistema_ant,
			ie_tipo_pessoa,
			nr_prontuario,
			nr_cartao_nac_sus,
			nr_pis_pasep,
			nm_pessoa_fisica_sem_acento)
		values	(cd_pessoa_fisica_w,
			'Importacao',
			sysdate,
			'Importacao',
			sysdate,
			nm_pessoa_w,
			dt_nasc_w,
			ie_sexo_w,
			nr_cpf_w,
			identidade_w,
			'N',
			cd_sistema_ant_w,
			1,
			nr_prontuario_w,
			nr_cartao_nac_sus_w,
			nr_pis_pasep_w,
			nm_pessoa_fisica_sem_acento_w);

		exception when others then
			sql_errm_w	:= sqlerrm;
			insert	into	log_tasy
				(cd_log,
				nm_usuario,
				dt_atualizacao,
				ds_log)
			values	(55764,
				'OS127978_PF',
				sysdate,
				'Erro ao inserir pessoa. ' || chr(13) || chr(10) ||
				'C�digo= ' || cd_sistema_ant_w || chr(13) || chr(10) ||
				'Nome= ' || nm_pessoa_w || chr(13) || chr(10) ||
				'Erro= ' || sql_errm_w);

			ie_erro_pf_w	:= 'S';
		end;

		if	(ie_erro_pf_w = 'N') then

			/* Residencial - sempre insere */

			select	nvl(max(nr_sequencia),0) + 1
			into	nr_sequencia_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
		
			insert into compl_pessoa_fisica(
				nr_sequencia,
				cd_pessoa_fisica,
				ie_tipo_complemento,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nm_usuario,
				dt_atualizacao,
				nm_contato,
				cd_cep,
				ds_endereco,
				nr_endereco,
				ds_complemento,
				ds_bairro,
				ds_municipio,
				sg_estado,
				nr_telefone,
				ds_fone_adic)
			values(	nr_sequencia_w,
				cd_pessoa_fisica_w,
				1,
				'Importacao',
				sysdate,
				'Importacao',
				sysdate,
				'',
				cd_cep_w,
				ds_end_w,
				decode(somente_numero(nr_end_w),0,null,somente_numero(nr_end_w)),
				ds_compl_w,
				ds_bairro_w,
				ds_munic_w,
				sg_estado_w,
				nr_telefone_w,
				ds_fone_adic_w);
				
			if	(mae_w is not null) then
		
				nr_sequencia_w	:= nr_Sequencia_w + 1;
				
				insert into compl_pessoa_fisica(
					nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					nm_contato,
					cd_cep,
					ds_endereco,
					nr_endereco,
					ds_complemento,
					ds_bairro,
					ds_municipio,
					sg_estado)
				values(	nr_sequencia_w,
					cd_pessoa_fisica_w,
					5,
					'Importacao',
					sysdate,
					'Importacao',
					sysdate,
					mae_w,
					'',
					'',
					'',
					'',
					'',
					'',
					'');
			end if;	
			
			if	(pai_w is not null) then
		
				nr_sequencia_w	:= nr_Sequencia_w + 1;
				
				insert into compl_pessoa_fisica(
					nr_sequencia,
					cd_pessoa_fisica,
					ie_tipo_complemento,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nm_usuario,
					dt_atualizacao,
					nm_contato,
					cd_cep,
					ds_endereco,
					nr_endereco,
					ds_complemento,
					ds_bairro,
					ds_municipio,
					sg_estado)
				values(	nr_sequencia_w,
					cd_pessoa_fisica_w,
					4,
					'Importacao',
					sysdate,
					'Importacao',
					sysdate,
					pai_w,
					'',
					'',
					'',
					'',
					'',
					'',
					'');
			end if;
		end if; 

		if	(qt_inserido_w	>= 3000) then
			commit;
			qt_inserido_w	:= 0;
		end if;

	end if; 

end loop;
close c01;

commit;

exception
	when others then

	open c03;
	loop
	fetch c03 into
		trigger_name_w;
	exit when c03%notfound;
		begin
		exec_sql_dinamico('OS125178_PF', 'alter trigger ' || trigger_name_w || ' enable');
		end;
	end loop;
	close c03;

	raise_application_error(-20011,'Erro: ' || sqlerrm || chr(13) || chr(10) 
				|| ' C�digo = ' || cd_sistema_ant_w);
end;

open c03;
loop
fetch c03 into
	trigger_name_w;
exit when c03%notfound;
	begin
	exec_sql_dinamico('OS125178_PF', 'alter trigger ' || trigger_name_w || ' enable');
	end;
end loop;
close c03;

end importar_w_pf_FUCS;
/
