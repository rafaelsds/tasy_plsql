create or replace
procedure autorizar_pendencia_entrega(	nr_sequencia_p		number,
				nm_usuario_p		Varchar2) is 

nr_ordem_compra_w		number(10);
nr_item_oci_w			number(10);
dt_prevista_entrega_w		date;
qt_prevista_entrega_w		number(13,4);
ds_motivo_pend_w			varchar2(255);
ds_razao_social_w			varchar2(255);
cd_material_w			number(10);
ds_material_w			varchar2(255);
cd_unidade_medida_compra_w	varchar2(30);
ds_email_adicional_w		varchar2(2000);
cd_perfil_dispara_w			number(5);
nr_seq_regra_w			number(10);
ie_usuario_w			varchar2(3);
ds_usuario_origem_w		varchar2(255);
ds_email_comprador_w		varchar2(255);
ds_usuario_comprador_w		comprador.nm_guerra%type;
cd_comprador_w			varchar2(10);
ds_email_origem_w			varchar2(255);
ds_email_destino_w			varchar2(255);
ds_assunto_w			varchar2(255);
ds_mensagem_w			varchar2(2000);
cd_estabelecimento_w		number(10);
qt_existe_w			number(10);
ie_momento_envio_w		varchar2(1);
ds_email_remetente_w		varchar2(255);

begin

update	ordem_compra_item_entrega
set	dt_fim_pendencia	= sysdate,
	nm_usuario_fim_pend	= nm_usuario_p
where	nr_sequencia		= nr_sequencia_p;

select	nvl(max(nr_ordem_compra),0),
	nvl(max(nr_item_oci),0),
	max(dt_prevista_entrega),
	max(qt_prevista_entrega),
	substr(max(obter_desc_motivo_pend_entr(nr_seq_motivo_pend)),1,255) ds_motivo_pend	
into	nr_ordem_compra_w,
	nr_item_oci_w,
	dt_prevista_entrega_w,
	qt_prevista_entrega_w,
	ds_motivo_pend_w	
from	ordem_compra_item_entrega
where	nr_sequencia = nr_sequencia_p;

gerar_ordem_compra_hist(
	nr_ordem_compra_W,
	'S',
	Wheb_mensagem_pck.get_Texto(297667),
	Wheb_mensagem_pck.get_Texto(297669,'NR_ITEM_OCI_W='||NR_ITEM_OCI_W||';DT_PREVISTA_ENTREGA_W='||DT_PREVISTA_ENTREGA_W),
	nm_usuario_p);
	
if	(nr_ordem_compra_w > 0) and
	(nr_item_oci_w > 0) then

	select	a.cd_estabelecimento,
		substr(obter_nome_pf_pj(null,a.cd_cgc_fornecedor),1,100),
		b.cd_material,
		substr(obter_desc_material(b.cd_material),1,255),
		b.cd_unidade_medida_compra,
		substr(obter_dados_pf_pj_estab(a.cd_estabelecimento, cd_pessoa_fisica, cd_cgc_fornecedor, 'M'),1,255),
		nvl(cd_comprador,'X')
	into	cd_estabelecimento_w,
		ds_razao_social_w,
		cd_material_w,
		ds_material_w,
		cd_unidade_medida_compra_w,
		ds_email_destino_w,
		cd_comprador_w
	from	ordem_compra a,
		ordem_compra_item b
	where	a.nr_ordem_compra	= b.nr_ordem_compra
	and	a.nr_ordem_compra	= nr_ordem_compra_w
	and	b.nr_item_oci		= nr_item_oci_w;
	
	if	(cd_comprador_w <> 'X') then
		select	ds_email,
			nm_guerra
		into	ds_email_comprador_w,
			ds_usuario_comprador_w
		from	comprador
		where	cd_pessoa_fisica		= cd_comprador_w
		and	cd_estabelecimento		= cd_estabelecimento_w;
	end if;

	select	nvl(max(nr_sequencia),0),
		nvl(max(ds_email_remetente),'X'),
		max(replace(ds_email_adicional,',',';')),
		max(cd_perfil_disparar),
		nvl(max(ie_momento_envio),'I')
	into	nr_seq_regra_w,
		ds_email_remetente_w,
		ds_email_adicional_w,
		cd_perfil_dispara_w,
		ie_momento_envio_w
	from	regra_envio_email_compra
	where	ie_tipo_mensagem = 52
	and	ie_situacao = 'A'
	and	cd_estabelecimento = cd_estabelecimento_w
	and	obter_se_envia_email_regra(nr_ordem_compra_w, 'OC', 52, cd_estabelecimento_w) = 'S';
	
	if	(ds_email_destino_w is not null) and
		(nr_seq_regra_w > 0) and
		((cd_perfil_dispara_w is null) or
		((cd_perfil_dispara_w is not null) and (cd_perfil_dispara_w = obter_perfil_ativo))) then
		begin	

		select	substr(ds_assunto,1,255),
			substr(ds_mensagem_padrao,1,2000)
		into	ds_assunto_w,
			ds_mensagem_w
		from	regra_envio_email_compra
		where	nr_sequencia = nr_seq_regra_w;
		
		ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ordem',nr_ordem_compra_w),1,255);
		ds_assunto_w := substr(replace_macro(ds_assunto_w,'@nr_item', nr_item_oci_w),1,255);
		ds_assunto_w := substr(replace_macro(ds_assunto_w,'@dt_prev_entrega',dt_prevista_entrega_w),1,255);
		ds_assunto_w := substr(replace_macro(ds_assunto_w,'@qt_prev_entrega',qt_prevista_entrega_w),1,255);
		ds_assunto_w := substr(replace_macro(ds_assunto_w,'@material',cd_material_w),1,255);
		ds_assunto_w := substr(replace_macro(ds_assunto_w,'@descricao',ds_material_w),1,255);
		ds_assunto_w := substr(replace_macro(ds_assunto_w,'@cd_unid_medida',cd_unidade_medida_compra_w),1,255);
		ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ds_fornecedor',ds_razao_social_w),1,255);
		ds_assunto_w := substr(replace_macro(ds_assunto_w,'@ds_motivo',ds_motivo_pend_w),1,255);
		
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@ordem',nr_ordem_compra_w),1,2000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@nr_item', nr_item_oci_w),1,2000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@dt_prev_entrega',dt_prevista_entrega_w),1,2000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@qt_prev_entrega',qt_prevista_entrega_w),1,2000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@material',cd_material_w),1,2000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@descricao',ds_material_w),1,2000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@cd_unid_medida',cd_unidade_medida_compra_w),1,2000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@ds_fornecedor',ds_razao_social_w),1,2000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w,'@ds_motivo',ds_motivo_pend_w),1,2000);

		select	nvl(max(ie_usuario),'U')
		into	ie_usuario_w
		from	regra_envio_email_compra
		where	nr_sequencia = nr_seq_regra_w;
	
		if	(ie_usuario_w = 'U') then --Usuario
			select	ds_email,
				nm_usuario
			into	ds_email_origem_w,
				ds_usuario_origem_w
			from	usuario
			where	nm_usuario = nm_usuario_p;
		elsif	(ie_usuario_w = 'C') then --Setor compras
			select	ds_email
			into	ds_email_origem_w
			from	parametro_compras
			where	cd_estabelecimento = cd_estabelecimento_w;
			
			select	nvl(ds_fantasia,ds_razao_social)
			into	ds_usuario_origem_w
			from	estabelecimento_v
			where	cd_estabelecimento = cd_estabelecimento_w;
		elsif	(ie_usuario_w = 'O') then --Comprador
			ds_email_origem_w	:= ds_email_comprador_w;
			ds_usuario_origem_w	:= ds_usuario_comprador_w;
		end if;

		if	(ds_email_adicional_w is not null) then
			ds_email_destino_w := ds_email_destino_w || ';' || ds_email_adicional_w;
		end if;

		if	(ds_email_remetente_w <> 'X') then
			ds_email_origem_w	:= ds_email_remetente_w;
		end if;
	
		select	count(*)
		into	qt_existe_w
		from  	dual
		where 	obter_se_envia_email_internet(52,null,cd_estabelecimento_w,nr_ordem_compra_w) = 'S' 
		and	obter_se_envia_email_ordem(52,nr_ordem_compra_w,cd_estabelecimento_w) = 'S';
		
		if	(qt_existe_w > 0) then
			begin
						
			if	(ie_momento_envio_w = 'A') then
				begin

				sup_grava_envio_email(
					'OC',
					'52',
					null,
					null,
					nr_ordem_compra_w,
					ds_email_destino_w,
					ds_usuario_origem_w,
					ds_email_origem_w,
					ds_assunto_w,
					ds_mensagem_w,
					cd_estabelecimento_w,
					nm_usuario_p);

				end;
			else
				begin
				enviar_email(ds_assunto_w, ds_mensagem_w, ds_email_origem_w, ds_email_destino_w, ds_usuario_origem_w, 'M');
				exception when others then
					ds_assunto_w := '';
				end;
			end if;

			end;
		end if;	
		end;
	end if;		
end if;

commit;

end autorizar_pendencia_entrega;
/