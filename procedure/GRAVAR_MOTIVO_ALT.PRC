create or replace
procedure gravar_motivo_alt(
			nr_seq_cronograma_p		number,
			ds_motivo_p				varchar2,
			ds_comentario_p			varchar2,
			nr_seq_cron_etapa_p		number) is 


nr_sequencia_w			number(10);			
			
			
begin
select 	max(nr_sequencia)
into	nr_sequencia_w
from  	gpi_log_data
where	nr_seq_cronograma  = nr_seq_cronograma_p
and		nr_seq_cron_etapa	=	nr_seq_cron_etapa_p;

update gpi_log_data
set		ds_motivo 	  = ds_motivo_p,
		ds_observacao = ds_comentario_p
where	nr_sequencia  = nr_sequencia_w;


commit;

end gravar_motivo_alt;
/