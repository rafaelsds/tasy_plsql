create or replace
procedure rep_jejum_afterpost_rep_pt_js(
				nr_acao_executada_p	number,
				nr_prescricao_p		number,
				nr_atendimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				nm_usuario_p		varchar2,
				cd_perfil_p		number,
				cd_estabelecimento_p	number,
				ie_dieta_vig_p	out	varchar2,
				ds_pergunta_p	out	varchar2
				) is
				
ie_acao_w		varchar2(1) 	:= '';
ie_dieta_vig_w		varchar2(10) 	:= '';
ds_pergunta_w		varchar2(255) 	:= '';
ie_cons_dieta_ving_w	varchar2(1) 	:= 'N';
				
begin

if	(nr_acao_executada_p in (2,3)) then
	begin
	if	(nr_acao_executada_p = 2) then
		begin
		ie_acao_w := 'A';
		end;
	else
		begin
		ie_acao_w := 'E';
		end;
	end if;
	
	atualiza_horario_nut_jejum(nr_prescricao_p,ie_acao_w,nm_usuario_p,cd_perfil_p);
	
	end;
end if;

atualizar_dose_esp_dieta(nr_prescricao_p,cd_perfil_p,nm_usuario_p);

obter_param_usuario(950, 70, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_cons_dieta_ving_w);

if	(ie_cons_dieta_ving_w = 'S') and
	(nr_acao_executada_p <> 3) then
	begin
	ie_dieta_vig_w	:= obter_se_dieta_vig(nr_atendimento_p,dt_inicio_p,dt_fim_p,nm_usuario_p);
	
	if	(ie_dieta_vig_w = 'S') then
		begin
		ds_pergunta_w	:= substr(obter_texto_tasy(93817, wheb_usuario_pck.get_nr_seq_idioma),1,255);
		end;
	end if;
	
	end;
end if;

ie_dieta_vig_p	:= ie_dieta_vig_w;
ds_pergunta_p	:= ds_pergunta_w;

end rep_jejum_afterpost_rep_pt_js;
/