create or replace
procedure pls_ajuste_phil_reg_int is 


nr_seq_filtro_w  pls_oc_cta_filtro.nr_sequencia%type;
nr_seq_valid_w  pls_oc_cta_combinada.nr_sequencia%type;

Cursor C01 is
  select   nr_sequencia
  from  pls_ocorrencia
  where  cd_ocorrencia = 'PHILREGINT';
  
  
begin

begin

	for r_c01_w in C01 loop
	 
	  select   max(nr_sequencia)
	  into   nr_seq_valid_w
	  from   pls_oc_cta_combinada
	  where  nr_seq_ocorrencia = r_c01_w.nr_sequencia;
	  
	  insert into pls_oc_cta_filtro (  nr_sequencia, dt_atualizacao, nm_usuario, 
		  dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_oc_cta_comb, 
		  nm_filtro, nr_ordem_geral, ie_excecao, 
		  nr_ordem_conta, nr_ordem_proc, nr_ordem_mat, 
		  nr_ordem_benef, nr_ordem_prest, nr_ordem_interc, 
		  nr_ordem_contrato, nr_ordem_produto, nr_ordem_prof, 
		  ie_situacao, ie_filtro_conta, 
		  ie_filtro_proc, ie_filtro_mat, ie_filtro_benef, 
		  ie_filtro_prest, ie_filtro_interc, ie_filtro_contrato, 
		  ie_filtro_produto, ie_filtro_prof, ie_filtro_protocolo, 
		  ie_valida_todo_atend, ie_filtro_oper_benef, ie_valida_conta_princ)
	  values ( pls_oc_cta_filtro_seq.nextval, to_date('16-10-2017 11:45:50', 'dd-mm-yyyy hh24:mi:ss'), 'dgkorz', 
	    to_date('16-10-2017 11:45:50', 'dd-mm-yyyy hh24:mi:ss'), 'dgkorz', nr_seq_valid_w, 
	    'Regime de internacao invalido ou nao informado', null, 'S', 
	    null, null, null, 
	    null, null, null, 
	    null, null, null, 
	    'A', 'S', 
	    'N', 'N', 'N', 
	    'N', 'N', 'N', 
	    'N', 'N', 'N', 
	    'N', 'N', 'N') returning nr_sequencia into nr_seq_filtro_w;

	    
	  if ( nr_seq_filtro_w is not null) then
	  
	    insert into pls_oc_cta_filtro_conta 
	    (   nr_sequencia, dt_atualizacao, nm_usuario, 
	      dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_oc_cta_filtro, 
	      ie_tipo_guia, cd_doenca_cid, nr_seq_tipo_atendimento, 
	      ie_regime_internacao, ie_tipo_internado, ie_carater_internacao, 
	      ie_tipo_consulta, nr_seq_saida_int, nr_seq_clinica, 
	      ie_origem_conta, nr_seq_tipo_acomod_conta, ie_internacao_obstetrica,
	      nr_seq_grupo_doenca, ie_internado, ie_tipo_conta, 
	      ie_situacao, ie_indicador_dorn, ie_observacao,
	      ie_vinc_internacao, ie_recem_nascido, ie_tipo_faturamento, 
	      ie_indicacao_acidente, ie_acomodacao_autorizada, tp_rede_min, 
	      ie_conta_autorizada)
	    values ( pls_oc_cta_filtro_conta_seq.nextval, to_date('16-10-2017 11:45:50', 'dd-mm-yyyy hh24:mi:ss'), 'dgkorz', 
	      to_date('16-10-2017 11:45:50', 'dd-mm-yyyy hh24:mi:ss'), 'dgkorz', nr_seq_filtro_w, 
	      '5', '', null, 
	      '', '', '', 
	      null, null, null, 
	      'A', null, 'N', 
	      null, 'N', '', 
	      'A', 'N', 'A', 
	      'N', 'N', 'P', 
	      '', 'N', null, 
	      'N');
	      
	    insert into pls_oc_cta_filtro_conta 
	    (   nr_sequencia, dt_atualizacao, nm_usuario, 
	      dt_atualizacao_nrec, nm_usuario_nrec, nr_seq_oc_cta_filtro, 
	      ie_tipo_guia, cd_doenca_cid, nr_seq_tipo_atendimento, 
	      ie_regime_internacao, ie_tipo_internado, ie_carater_internacao, 
	      ie_tipo_consulta, nr_seq_saida_int, nr_seq_clinica, 
	      ie_origem_conta, nr_seq_tipo_acomod_conta, ie_internacao_obstetrica,
	      nr_seq_grupo_doenca, ie_internado, ie_tipo_conta, 
	      ie_situacao, ie_indicador_dorn, ie_observacao,
	      ie_vinc_internacao, ie_recem_nascido, ie_tipo_faturamento, 
	      ie_indicacao_acidente, ie_acomodacao_autorizada, tp_rede_min, 
	      ie_conta_autorizada)
	    values ( pls_oc_cta_filtro_conta_seq.nextval, to_date('16-10-2017 11:45:50', 'dd-mm-yyyy hh24:mi:ss'), 'dgkorz', 
	      to_date('16-10-2017 11:45:50', 'dd-mm-yyyy hh24:mi:ss'), 'dgkorz', nr_seq_filtro_w, 
	      '5', '', null, 
	      '', '', '', 
	      null, null, null, 
	      'A', null, 'N', 
	      null, 'N', '', 
	      'A', 'N', 'A', 
	      'N', 'N', 'C', 
	      '', 'N', null, 
	      'N');


	  
	  end if;

	  
	 end loop;


exception
when others then
	null;
end;
	 
 commit;
 
end pls_ajuste_phil_reg_int;
/