create or replace
procedure ctb_pls_contab_rec
			(	nr_lote_contabil_p	in	Number,
				nm_usuario_p		in	Varchar2,
				ie_exclusao_p		in	Varchar2,
				ds_retorno_p  		in out	Varchar2) is

dt_referencia_w			Date;
cd_estabelecimento_w		Number(4);
cd_conta_contabil_w		Varchar2(20);
ie_debito_credito_w		Varchar2(20);
vl_contabil_w			Number(15,4);
vl_retorno_w			Varchar2(2000);
dt_referencia_mens_w		date;
nr_seq_w_movto_cont_w		number(10);
cd_historico_w			Number(10);
nr_seq_mensalidade_w		Number(10);
nr_seq_nota_fiscal_w		Number(10);
pr_imposto_w			Number(7,4);
cd_tributo_w			Number(3);
vl_imposto_w			Number(15,4);
pr_imposto_total_w		Number(7,4);
cd_conta_imposto_w		Varchar2(20);
cd_centro_custo_w		Number(8);
ie_centro_custo_w		Varchar2(1);
nr_seq_item_w			Number(10);
ds_item_w			varchar2(255);
nr_lote_contabil_w		Number(10);
ie_tipo_valor_w			varchar2(2);
nr_seq_pagador_w		number(10);
ie_tipo_pagador_w		varchar2(2);
cd_historico_imposto_cred_w	number(10);
cd_historico_imposto_deb_w	number(10);
cd_historico_antecip_w		number(10);
ie_cancelamento_w		varchar2(1);
nr_seq_regra_w			number(10);
ie_tipo_item_w			varchar2(2);
nr_lote_contab_antecip_w	number(10);
ie_tipo_conta_w			varchar2(2);
nr_nota_fiscal_w		varchar2(255);
nr_titulo_w			Number(10);
cd_cgc_pagador_w		Varchar2(14);
cd_pf_pagador_w			Varchar2(10);
ie_compl_hist_w			Varchar2(1);
ds_conteudo_w			Varchar2(4000);
ds_compl_historico_w		Varchar2(255);
ds_compl_historico_ww		Varchar2(255);
ds_compl_hist_imposto_cred_w	Varchar2(255);
ds_compl_hist_imposto_cred_ww	Varchar2(255);
ds_compl_hist_imposto_deb_w	Varchar2(255);
ds_compl_hist_imposto_deb_ww	Varchar2(255);
cd_conta_imposto_cred_w		Varchar2(20);
cd_conta_imposto_deb_w		Varchar2(20);
ie_tipo_segurado_w		Varchar2(3);
ie_tipo_contratacao_w		Varchar2(2);
nr_seq_copartic_w		Number(10);
nm_pagador_w			Varchar2(255);
nr_seq_regra_cc_w		Number(10);
nr_seq_segurado_w		number(10);
dt_referencia_trunc_w		date;

Cursor C01 is
	select	b.nr_sequencia,
		b.dt_referencia,
		d.nr_sequencia nr_seq_nota_fiscal,
		b.nr_seq_pagador,
		b.ie_cancelamento,
		a.nr_lote_contab_antecip,
		c.nr_titulo
	from	pls_mensalidade b,
		titulo_receber	c,
		nota_fiscal	d,
		pls_lote_mensalidade a
	where	b.nr_sequencia	= c.nr_seq_mensalidade(+)
	and	b.nr_sequencia	= d.nr_seq_mensalidade(+)
	and	a.nr_sequencia	= b.nr_seq_lote
	and	(a.nr_lote_contabil		= nr_lote_contabil_p
	or	(a.nr_lote_contab_antecip	= nr_lote_contabil_p and b.ie_cancelamento is null));

Cursor C02 is
	select	'D',
		'N',
		decode(ie_tipo_conta_w, 'DA', a.cd_conta_deb_antecip, a.cd_conta_deb),
		nvl(a.vl_item,0),
		nvl(a.cd_historico,1),
		a.nr_sequencia,
		(	select	substr(ds_valor_dominio,1,254)
			from	valor_dominio_v
			where	cd_dominio	= 1930
			and	vl_dominio	= a.ie_tipo_item) ds_item,
		a.nr_seq_regra_ctb_mensal_deb,
		a.ie_tipo_item,
		c.ie_tipo_segurado,
		e.ie_tipo_contratacao,
		decode(a.ie_tipo_item,'13',decode(ie_cancelamento,null,1,d.nr_sequencia),0),
		c.nr_sequencia
	from	pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_segurado		c,
		pls_mensalidade		d,
		pls_plano		e
	where	b.nr_sequencia		= a.nr_seq_mensalidade_seg
	and	b.nr_seq_segurado	= c.nr_sequencia
	and	d.nr_sequencia		= b.nr_seq_mensalidade
	and	c.nr_seq_plano		= e.nr_sequencia
	and	b.nr_seq_mensalidade	= nr_seq_mensalidade_w
	and	a.ie_tipo_item <> '3'
	union all
	select	'C',
		'N',
		decode(ie_tipo_conta_w, 'CA', a.cd_conta_rec_antecip, a.cd_conta_rec),
		nvl(a.vl_item,0),
		nvl(a.cd_historico,1),
		a.nr_sequencia,
		(	select	substr(ds_valor_dominio,1,254)
			from	valor_dominio_v
			where	cd_dominio	= 1930
			and	vl_dominio	= a.ie_tipo_item) ds_item,
		a.nr_seq_regra_ctb_mensal,
		a.ie_tipo_item,
		c.ie_tipo_segurado,
		e.ie_tipo_contratacao,
		decode(a.ie_tipo_item,'13',decode(ie_cancelamento,null,1,d.nr_sequencia),0),
		c.nr_sequencia
	from	pls_mensalidade_seg_item a,
		pls_mensalidade_segurado b,
		pls_segurado		c,
		pls_mensalidade		d,
		pls_plano		e
	where	b.nr_seq_segurado	= c.nr_sequencia
	and	c.nr_seq_plano		= e.nr_sequencia
	and	d.nr_sequencia		= b.nr_seq_mensalidade
	and	b.nr_sequencia		= a.nr_seq_mensalidade_seg
	and	b.nr_seq_mensalidade	= nr_seq_mensalidade_w
	and	a.ie_tipo_item <> '3'
	union all
	select	'D',
		'C',
		decode(ie_tipo_conta_w,'DA',f.cd_conta_deb_antecip,f.cd_conta_deb),
		nvl(f.vl_copartic_mens, f.vl_coparticipacao) vl_coparticipacao,
		nvl(f.cd_historico,1),
		c.nr_sequencia,
		(	select	substr(ds_valor_dominio,1,254)
			from	valor_dominio_v
			where	cd_dominio	= 1930
			and	vl_dominio	= c.ie_tipo_item) ds_item,
		f.nr_seq_regra_ctb_deb,
		c.ie_tipo_item,
		g.ie_tipo_segurado,
		h.ie_tipo_contratacao,
		f.nr_sequencia,
		g.nr_sequencia
	from	pls_mensalidade a,
		pls_mensalidade_segurado b,
		pls_mensalidade_seg_item c,
		pls_conta		d,
		pls_conta_coparticipacao f,
		pls_segurado		g,
		pls_plano		h
	where	a.nr_sequencia	= b.nr_seq_mensalidade
	and	b.nr_sequencia	= c.nr_seq_mensalidade_seg
	and	c.nr_seq_conta	= d.nr_sequencia
	and	d.nr_sequencia	= f.nr_seq_conta
	and	g.nr_sequencia	= b.nr_seq_segurado
	and	h.nr_sequencia	= g.nr_seq_plano
	and	a.nr_sequencia	= nr_seq_mensalidade_w
	and	c.ie_tipo_item = '3'
	union all
	select	'C',
		'C',
		decode(ie_tipo_conta_w,'CA',f.cd_conta_cred_antecip,f.cd_conta_cred),
		nvl(f.vl_copartic_mens, f.vl_coparticipacao) vl_coparticipacao,
		nvl(f.cd_historico,1),
		c.nr_sequencia,
		(	select	substr(ds_valor_dominio,1,254)
			from	valor_dominio_v
			where	cd_dominio	= 1930
			and	vl_dominio	= c.ie_tipo_item) ds_item,
		f.nr_seq_regra_ctb_cred,
		c.ie_tipo_item,
		g.ie_tipo_segurado,
		h.ie_tipo_contratacao,
		f.nr_sequencia,
		g.nr_sequencia
	from	pls_mensalidade 		a,
		pls_mensalidade_segurado 	b,
		pls_mensalidade_seg_item	c,
		pls_conta 			d,
		pls_conta_coparticipacao	f,
		pls_segurado			g,
		pls_plano			h
	where	a.nr_sequencia	= b.nr_seq_mensalidade
	and	b.nr_sequencia	= c.nr_seq_mensalidade_seg
	and	c.nr_seq_conta	= d.nr_sequencia
	and	d.nr_sequencia	= f.nr_seq_conta
	and	g.nr_sequencia	= b.nr_seq_segurado
	and	h.nr_sequencia	= g.nr_seq_plano	
	and	a.nr_sequencia	= nr_seq_mensalidade_w
	and	c.ie_tipo_item	= '3';

BEGIN

select	dt_referencia,
	trunc(dt_referencia,'month'),
	cd_estabelecimento,
	nr_lote_contabil
into 	dt_referencia_w,
	dt_referencia_trunc_w,
	cd_estabelecimento_w,
	nr_lote_contabil_w
from 	lote_contabil
where 	nr_lote_contabil 	= nr_lote_contabil_p;

select	obter_se_compl_tipo_lote(cd_estabelecimento_w, 21)
into	ie_compl_hist_w
from	dual;

delete	from	w_pls_movimento_sem_conta
where	ie_tipo_item = 'M';

if	(ie_exclusao_p = 'S') then
	begin
	delete from movimento_contabil 
	where  nr_lote_contabil		= nr_lote_contabil_p;

	Update	lote_contabil
	set	vl_credito 		= 0,
		vl_debito  		= 0
	where	nr_lote_contabil	= nr_lote_contabil_p;
	
	update	pls_lote_mensalidade
	set	nr_lote_contabil	= null
	where	trunc(dt_mesano_referencia,'month') = dt_referencia_trunc_w
	and	ie_status	= 2
	and	cd_estabelecimento	= cd_estabelecimento_w;
	
	update	pls_lote_mensalidade
	set	nr_lote_contab_antecip	= null
	where	trunc(dt_contabilizacao,'month') = dt_referencia_trunc_w
	and	ie_status	= 2
	and	cd_estabelecimento	= cd_estabelecimento_w;
	end;
else
	begin
	delete	w_movimento_contabil
	where	nr_lote_contabil	= nr_lote_contabil_p;
	
	update	pls_lote_mensalidade
	set	nr_lote_contabil	= nr_lote_contabil_p
	where	nvl(nr_lote_contabil,0)	= 0
  	and	trunc(dt_mesano_referencia,'month') = dt_referencia_trunc_w
	and	ie_status	= 2
	and	cd_estabelecimento	= cd_estabelecimento_w;
	
	update	pls_lote_mensalidade
	set	nr_lote_contab_antecip	= nr_lote_contabil_p
	where	nvl(nr_lote_contab_antecip,0)	= 0
	and	trunc(dt_contabilizacao,'month') = dt_referencia_trunc_w
	and	ie_status	= 2
	and	cd_estabelecimento	= cd_estabelecimento_w;
	
	OPEN C01;
	LOOP
	FETCH C01 into
		nr_seq_mensalidade_w,
		dt_referencia_mens_w,
		nr_seq_nota_fiscal_w,
		nr_seq_pagador_w,
		ie_cancelamento_w,
		nr_lote_contab_antecip_w,
		nr_titulo_w;
	exit when c01%notfound;
		begin
		select	decode(a.cd_pessoa_fisica,null,'PJ','PF'),
			a.cd_cgc,
			a.cd_pessoa_fisica,
			(	select x.nm_pessoa_fisica
				from	pessoa_fisica x
				where	x.cd_pessoa_fisica = a.cd_pessoa_fisica
				UNION ALL
				select	y.ds_razao_social
				from	pessoa_juridica y
				where	y.cd_cgc	= a.cd_cgc) nm_pagador
		into	ie_tipo_pagador_w,
			cd_cgc_pagador_w,
			cd_pf_pagador_w,
			nm_pagador_w
		from	pls_contrato_pagador	a
		where	a.nr_sequencia = nr_seq_pagador_w;
		
		/* Obter o valor total dos impostos */
		select	nvl(sum(vl_tributo),0)
		into	vl_imposto_w
		from	nota_fiscal_trib
		where	nr_sequencia = nr_seq_nota_fiscal_w;
		
		/* Obter a conta de cr�dito do imposto */
		select	max(cd_conta_contabil),
			max(cd_historico)
		into	cd_conta_imposto_cred_w,
			cd_historico_imposto_cred_w
		from	pls_conta_contabil_imposto
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	trunc(sysdate,'dd') between trunc(dt_vigencia_inicial,'dd') and trunc(nvl(dt_vigencia_final,sysdate),'dd')
		and	nvl(ie_debito_credito,'C') = 'C'
		and	((ie_tipo_pessoa = ie_tipo_pagador_w) or (ie_tipo_pessoa = 'A'));
		
		if	(nvl(cd_conta_imposto_cred_w,'0') <> '0') and
			(vl_imposto_w > 0) then
			if	(ie_compl_hist_w = 'S') then
				select	obter_compl_historico(21, cd_historico_imposto_cred_w, ds_conteudo_w)
				into	ds_compl_hist_imposto_cred_ww
				from	dual;
				ds_compl_hist_imposto_cred_w	:= substr(nvl(ds_compl_hist_imposto_cred_ww, ds_compl_hist_imposto_cred_w),1,255);
			end if;
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_seq_w_movto_cont_w
			from	w_movimento_contabil;
			
			insert into w_movimento_contabil
				(nr_lote_contabil, nr_sequencia, cd_conta_contabil,
				ie_debito_credito, cd_historico, dt_movimento,
				vl_movimento, cd_estabelecimento, cd_centro_custo,
				ds_compl_historico)
			values(	nr_lote_contabil_p, nr_seq_w_movto_cont_w, cd_conta_imposto_cred_w,
				'C', cd_historico_imposto_cred_w, dt_referencia_w,
				vl_imposto_w, cd_estabelecimento_w, null,
				ds_compl_hist_imposto_cred_w);
		end if;
		
		/* Obter a conta de cr�dito do imposto */
		select	max(cd_conta_contabil),
			max(cd_historico)
		into	cd_conta_imposto_deb_w,
			cd_historico_imposto_deb_w
		from	pls_conta_contabil_imposto
		where	cd_estabelecimento	= cd_estabelecimento_w
		and	trunc(sysdate,'dd') between trunc(dt_vigencia_inicial,'dd') and trunc(nvl(dt_vigencia_final,sysdate),'dd')
		and	nvl(ie_debito_credito,'D') = 'D'
		and	((ie_tipo_pessoa = ie_tipo_pagador_w) or (ie_tipo_pessoa = 'A'));
		
		if	(nvl(cd_conta_imposto_deb_w,'0') <> '0') and
			(vl_imposto_w > 0) then
			if	(ie_compl_hist_w = 'S') then
				select	obter_compl_historico(21, cd_historico_imposto_deb_w, ds_conteudo_w)
				into	ds_compl_hist_imposto_deb_ww
				from	dual;
				ds_compl_hist_imposto_deb_w	:= substr(nvl(ds_compl_hist_imposto_deb_ww, ds_compl_hist_imposto_deb_w),1,255);
			end if;
			
			select	nvl(max(nr_sequencia),0) + 1
			into	nr_seq_w_movto_cont_w
			from	w_movimento_contabil;
			
			insert into w_movimento_contabil
				(nr_lote_contabil, nr_sequencia, cd_conta_contabil,
				ie_debito_credito, cd_historico, dt_movimento,
				vl_movimento, cd_estabelecimento, cd_centro_custo,
				ds_compl_historico)
			values(	nr_lote_contabil_p, nr_seq_w_movto_cont_w, cd_conta_imposto_deb_w,
				'D', cd_historico_imposto_deb_w, dt_referencia_w,
				vl_imposto_w, cd_estabelecimento_w, null,
				ds_compl_hist_imposto_deb_w);
		end if;
		
		if	(nvl(nr_lote_contab_antecip_w,0) <> 0) then
			if 	(nr_lote_contab_antecip_w = nr_lote_contabil_p) then
				ie_tipo_conta_w := 'CA'; /* Cr�dito antecipa��o */
			elsif	(nr_lote_contab_antecip_w <> nr_lote_contabil_p) then
				ie_tipo_conta_w := 'DA'; /* D�bito antecipa��o */
			end if;
		else
			ie_tipo_conta_w := 'N'; /* Normal */
		end if;
		
		OPEN C02;
		LOOP
		FETCH C02 into
			ie_debito_credito_w,
			ie_tipo_valor_w,
			cd_conta_contabil_w,
			vl_contabil_w,
			cd_historico_w,
			nr_seq_item_w,
			ds_item_w,
			nr_seq_regra_w,
			ie_tipo_item_w,
			ie_tipo_segurado_w,
			ie_tipo_contratacao_w,
			nr_seq_copartic_w,
			nr_seq_segurado_w;
		exit when c02%notfound;
			begin
			cd_centro_custo_w := null;
			
			begin
			select	max(cd_historico)
			into	cd_historico_antecip_w
			from	pls_conta_receita_futura
			where	cd_estabelecimento	= cd_estabelecimento_w
			and	trunc(sysdate) between trunc(dt_vigencia_inicial) and fim_dia(nvl(dt_vigencia_final,sysdate))
			and	nvl(ie_tipo_contratacao,nvl(ie_tipo_contratacao_w,'0'))	= nvl(ie_tipo_contratacao_w,'0')
			and	ie_tipo_segurado	= ie_tipo_segurado_w;
			exception
			when others then
				wheb_mensagem_pck.exibir_mensagem_abort( 267257, null );
				/* N�o foi realizado o cadastro da conta financeira de antecipa��o. ' || chr(13) || '(Shift + F11 -> Plano de Sa�de -> OPS - Contabilidade -> Regra cont�bil de receitas antecipa��o mensalidade) */
			end;
			
			if 	(ie_debito_credito_w	= 'C') then				
				select	decode(ie_tipo_conta_w, 'CA', cd_historico_antecip_w, cd_historico_w)
				into	cd_historico_w
				from	dual;
			elsif 	(ie_debito_credito_w	= 'D') then
				select	decode(ie_tipo_conta_w, 'DA', cd_historico_antecip_w, cd_historico_w)
				into	cd_historico_w
				from	dual;
			end if;
			
			if	(ie_cancelamento_w = 'E') then /* Segundo contato com Adriano em 26/08/2008, o estorno deve ser contabilizado ao contr�rio se coparticipa��o*/
				select	max(cd_conta_estorno),
					max(cd_historico_estorno)
				into	cd_conta_contabil_w,
					cd_historico_w
				from	pls_regra_ctb_mensal
				where	nr_sequencia	= nr_seq_regra_w;
				
				if (ie_tipo_valor_w = 'C' and ie_debito_credito_w = 'C') then
					ie_debito_credito_w := 'D';
				elsif (ie_tipo_valor_w = 'C' and ie_debito_credito_w = 'D') then
					ie_debito_credito_w := 'C';
				end if;
			end if;
			
			if	(cd_conta_contabil_w is null) then
				insert into w_pls_movimento_sem_conta(	
					nr_sequencia, cd_item, ds_item,
					ie_tipo_item, dt_atualizacao, nm_usuario,
					vl_item, dt_referencia, nr_lote_contabil,
					ie_proc_mat_item, ie_deb_cred, ds_observacao)
				values(	w_pls_movimento_sem_conta_seq.nextval, nr_seq_item_w, ds_item_w,
					'M', sysdate, nm_usuario_p,
					vl_contabil_w, dt_referencia_w, nr_lote_contabil_w,
					null, ie_debito_credito_w, decode(ie_tipo_valor_w,'C',obter_desc_expressao(286241),''));
			else
				select	ie_centro_custo
				into	ie_centro_custo_w
				from	conta_contabil
				where	cd_conta_contabil = cd_conta_contabil_w;
				
				if 	(ie_centro_custo_w = 'S') then
					pls_obter_centro_custo('R', null, cd_estabelecimento_w,
							'', '', '', 
							nr_seq_segurado_w, ie_tipo_item_w, cd_centro_custo_w, 
							nr_seq_regra_cc_w);
				end if;
				
				if	(ie_compl_hist_w = 'S') then
					/* Felipe 10/10/2008 - OS 111091 */
					select	max(nr_nota_fiscal)
					into	nr_nota_fiscal_w
					from	nota_fiscal
					where	nr_sequencia	= nr_seq_nota_fiscal_w;
					
					nr_nota_fiscal_w	:= somente_numero(nr_nota_fiscal_w);
					
					ds_conteudo_w	:= substr(nr_nota_fiscal_w	|| '#@' ||
								cd_cgc_pagador_w 	|| '#@' ||
								cd_pf_pagador_w		|| '#@' ||
								nr_titulo_w		|| '#@' ||
								nm_pagador_w,1,4000);
					
					select	obter_compl_historico(21, cd_historico_w, ds_conteudo_w)
					into	ds_compl_historico_ww
					from	dual;
					ds_compl_historico_w	:= substr(ds_compl_historico_ww,1,255);--substr(nvl(ds_compl_historico_ww, ds_compl_historico_w),1,255);
				end if;
				/* Felipe - Fim da altera��o */
				
				select	nvl(max(nr_sequencia),0) + 1
				into	nr_seq_w_movto_cont_w
				from	w_movimento_contabil;
				
				insert into w_movimento_contabil 
					(nr_lote_contabil,
					nr_sequencia,
					cd_conta_contabil,
					ie_debito_credito,
					cd_historico,
					dt_movimento,
					vl_movimento,
					cd_estabelecimento,
					cd_centro_custo,
					ds_compl_historico,
					nr_seq_agrupamento)
				values (nr_lote_contabil_p,
					nr_seq_w_movto_cont_w,
					cd_conta_contabil_w,
					ie_debito_credito_w,
					cd_historico_w,
					dt_referencia_w,
					vl_contabil_w,
					cd_estabelecimento_w,
					cd_centro_custo_w,
					ds_compl_historico_w,
					nr_seq_copartic_w);
			end if;
			end;
		end loop;
		close C02;
		end;
	end loop;
	close c01;
	Agrupa_Movimento_Contabil(nr_lote_contabil_p, nm_usuario_p); 
	end;
end if;

if   	(ds_retorno_p is null) then
	begin
	update lote_contabil
	set	ie_situacao = 'A'
	where	nr_lote_contabil 	= nr_lote_contabil_p;

	if	(ie_exclusao_p = 'S') then
		ds_retorno_p	:= wheb_mensagem_pck.get_texto(165188);
	else
		ds_retorno_p	:= wheb_mensagem_pck.get_texto(165187);
	end if;

	commit;
	end;
else
	rollback;
end if;

end ctb_pls_contab_rec;
/