create or replace procedure	POPULATE_FAN_TABLE(	dt_exportar_de_p	date ,
							dt_exportar_ate_p	date ,
							nm_usuario_p		varchar2 ,
							returned_value_p	out NOCOPY number ,
							other_exception_p 	out NOCOPY varchar2 )
is
dt_exportar_de_w		date;
dt_exportar_ate_w		date;
returned_value_w		number;
/*--FAN table columns*/
nr_patient_identifier_w		varchar2( 8 byte );
nr_admission_number_w		varchar2( 12 byte );
nr_former_name_ident_w		varchar2( 2 byte );
nm_patient_surname_w		varchar2( 24 byte );
nm_patient_first_name_w		varchar2( 15 byte );
nm_patient_second_ident_w	varchar2( 15 byte );
line_count_w			number( 10 )	:= 0;
downloaded_report_count_w	number( 10 )	:= 0;
v_errm				varchar2( 100 );
exc_raised_in_fan_tab		exception;
PRAGMA exception_init( exc_raised_in_fan_tab, -20004 );									 																			  
error_result			clob		:= null;
cursor c01
is
	select	distinct adiciona_zeros_esquerda(pfa.nr_prontuario,8) ,
		adiciona_zeros_esquerda(ap.nr_atendimento,12),
		obter_count_name_change( ap.cd_pessoa_fisica , pfa.nr_sequencia , dt_exportar_de_w , dt_exportar_ate_w ) ,
		chr( 34 )||trim( substr( parse_previous_name( pfa.nm_pessoa_fisica , 'LN' ) , 1 , 22 ) )||chr( 34 ) ,
		chr( 34 )||trim( substr( parse_previous_name( pfa.nm_pessoa_fisica , 'FN' ) , 1 , 13 ) )||chr( 34 ) ,
		nvl2( parse_previous_name( pfa.nm_pessoa_fisica , 'MN' ) , chr( 34 )||trim( substr( parse_previous_name( pfa.nm_pessoa_fisica , 'MN' ) , 1 , 13 ) )||
		chr( 34 ) , null )
	from	atendimento_paciente ap ,
		diagnostico_doenca dd ,
		pessoa_fisica_alteracao pfa ,
		registro_cancer rc
	where	ap.nr_atendimento =rc.nr_atendimento(+)
		and ap.nr_atendimento   =dd.nr_atendimento
		and ap.cd_pessoa_fisica =pfa.cd_pessoa_fisica
		and dd.cd_doenca in(
			select cd_doenca_cid
			from cid_doenca
			where(	cd_doenca_cid like 'C%'		OR
				cd_doenca_cid like 'D0%' 	OR
				cd_doenca_cid like 'D3%' 	OR
				cd_doenca_cid like 'D4%' 	OR
				cd_doenca_cid like 'Z85%')
			minus
			select cd_doenca_cid
			from cid_doenca
			where(	cd_doenca_cid like 'C44%' 	OR
				cd_doenca_cid like 'C77%' 	OR
				cd_doenca_cid like 'C78%' 	OR
				cd_doenca_cid like 'C79%' 	OR
				cd_doenca_cid like 'D1%'  	OR
				cd_doenca_cid like 'D2%'  	OR
				cd_doenca_cid like 'D31%' 	OR
				cd_doenca_cid like 'D32%' 	OR
				cd_doenca_cid like 'D34%' 	OR
				cd_doenca_cid like 'D35%' 	OR
				cd_doenca_cid like 'D36%' 	OR
				cd_doenca_cid like 'D04%' 	OR
				cd_doenca_cid like 'D48.5%' 	OR
				cd_doenca_cid like 'Z85.9')
			UNION ALL
			select cd_doenca_cid
			from cid_doenca
			where(	cd_doenca_cid like 'D18.02' 	OR
				cd_doenca_cid like 'Q85.0' 	OR
				cd_doenca_cid like 'D76.0')
		)
		and pfa.ie_alteracao_nome='S'
		and rc.dt_liberacao     is not null
		and rc.dt_diagnostico between dt_exportar_de_w and dt_exportar_ate_w
		and pfa.dt_aprovacao between dt_exportar_de_w and dt_exportar_ate_w
		order by 2;
begin
	dt_exportar_de_w	:= dt_exportar_de_p;
	dt_exportar_ate_w	:= dt_exportar_ate_p;
	if( dt_exportar_de_w is not null and dt_exportar_ate_w is not null ) then
		open c01;
		loop
		begin  
      			line_count_w	:= line_count_w+1;
		fetch c01 into 
			nr_patient_identifier_w ,
			nr_admission_number_w ,
			nr_former_name_ident_w ,
			nm_patient_surname_w ,
			nm_patient_first_name_w ,
			nm_patient_second_ident_w;
		exit when c01%notfound;
			begin
				insert into patient_former_names(
					nr_sequencia ,
					dt_atualizacao ,
					nm_usuario ,
					dt_atualizacao_nrec ,
					nm_usuario_nrec ,
					nr_patient_identifier ,
					nr_admission_number ,
					nr_former_name_identifier ,
					nm_patient_surname ,
					nm_patient_first_name ,
					nm_patient_second_identifier ,
					dt_export_from ,
					dt_export_to ,
					nr_report_sequence)
				values(	patient_former_names_seq.nextval ,
					sysdate ,
					nm_usuario_p ,
					sysdate ,
					nm_usuario_p ,
					nr_patient_identifier_w ,
					nr_admission_number_w ,
					nr_former_name_ident_w ,
					nm_patient_surname_w ,
					nm_patient_first_name_w ,
					nm_patient_second_ident_w ,
					dt_exportar_de_w ,
					dt_exportar_ate_w ,
					null);
			exception when others then
			v_errm      := substr( sqlerrm , 1 , 100 );
			error_result:= error_result || ' '|| v_errm||' . error happened at line '||line_count_w||' for encounter '|| nr_admission_number_w ||chr( 13 )||chr( 10 );
			end;				    				 		   
		exception
		when value_error or dup_val_on_index or invalid_cursor then
			v_errm      := substr( sqlerrm , 1 , 100 );
			error_result:= error_result || ' '|| v_errm||' . error happened at line '||line_count_w||chr( 13 )||chr( 10 );
		end;
		end loop;
		returned_value_w	:= 1;
		other_exception_p	:= null;
			if( error_result   is not null )then
				returned_value_w	:= 0;
				other_exception_p	:= error_result;
				raise exc_raised_in_fan_tab;					
			end if;
		close c01;
	end if;
		returned_value_p	:= returned_value_w;
exception
when others then
	returned_value_p	:= 2;
	other_exception_p	:= other_exception_p||wheb_mensagem_pck.get_texto( 1073278 , 'DS_ERROR='||'populate_fan_table. Error:'|| sqlerrm );
end populate_fan_table; 
/