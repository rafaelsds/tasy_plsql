create or replace
procedure gerar_evolucao_Morse (	nm_usuario_p 			varchar2,					
									nr_sequencia_p		 	Number) is

ie_tipo_evolucao_w	    varchar2(3);
ie_situacao_w			varchar2(1);
ie_relev_resumo_alta_w  varchar2(1);
ds_evolucao_w			varchar2(4000);
cd_especial_medico_w	number(5);
cd_medico_w				varchar2(10);
ie_evolucao_cliente_w	varchar2(1);

-- Dados Escala Morse
nr_atendimento_w  		number(10);
cd_profissional_w 		varchar2(10);
dt_avaliacao_w 			date;
ie_hist_queda_w 		varchar2(1);
ie_diag_secundario_w 	varchar2(1);
ie_ajuda_deambular_w 	number(3);
ie_uso_heparina_w 		varchar2(1);
ie_modo_andar_w 		number(3);
ie_estado_mental_w 		number(3);
qt_pontuacao_w  		number(10);
cd_pessoa_fisica_w		varchar2(10);

begin

select	nvl(max(ie_situacao),'I'), 
		nvl(max(ie_evolucao_cliente),'N')
into	ie_situacao_w,
		ie_evolucao_cliente_w
from 	tipo_evolucao
where   cd_tipo_evolucao = 'EEM';

if (ie_evolucao_cliente_w = 'S') and
	(ie_situacao_w = 'A') then

 
	ie_tipo_evolucao_w 	:= Obter_Dados_Usuario_Opcao(nm_usuario_p,'F');
	
	select 	max(nr_atendimento),
			max(cd_profissional),
			max(dt_avaliacao),
			max(ie_hist_queda),
			max(ie_diag_secundario),
			max(ie_ajuda_deambular),
			max(ie_uso_heparina),
			max(ie_modo_andar),
			max(ie_estado_mental),
			max(qt_pontuacao),
			max(OBTER_PESSOA_ATENDIMENTO(NR_ATENDIMENTO,'C'))
	into	nr_atendimento_w,
			cd_profissional_w,
			dt_avaliacao_w,
			ie_hist_queda_w,
			ie_diag_secundario_w,
			ie_ajuda_deambular_w,
			ie_uso_heparina_w,
			ie_modo_andar_w,
			ie_estado_mental_w,
			qt_pontuacao_w,
			cd_pessoa_fisica_w
	from	ESCALA_MORSE
	where 	nr_sequencia = nr_sequencia_p;
	
	if (obter_se_medico(cd_profissional_w,'M') = 'S') then
		cd_medico_w := cd_profissional_w;
	end if;
	
	ds_evolucao_w :=  obter_desc_expressao(631158);
	
	ds_evolucao_w :=  ds_evolucao_w || chr(13) || obter_desc_expressao(286576) || ' ' || dt_avaliacao_w;
	
	if (ie_hist_queda_w = 'S') then
		ds_evolucao_w :=  ds_evolucao_w || chr(13) || obter_desc_expressao(643963);
	end if;
	if (ie_diag_secundario_w = 'S') then
		ds_evolucao_w :=  ds_evolucao_w || chr(13) || obter_desc_expressao(614297);
	end if;
	
	ds_evolucao_w :=  ds_evolucao_w || chr(13) || obter_desc_expressao(614295) || ': ';
	if (ie_ajuda_deambular_w = 0) then
		ds_evolucao_w :=  ds_evolucao_w || 'Nenhuma; profissional da sa�de; cadeira de rodas; acamado';
	elsif (ie_ajuda_deambular_w = 15) then
		ds_evolucao_w :=  ds_evolucao_w || 'Muleta; bengala; andador';
	elsif (ie_ajuda_deambular_w = 30) then
		ds_evolucao_w :=  ds_evolucao_w || 'Deambula apoiado junto a mob�lia';
	end if;
	
	if (ie_uso_heparina_w = 'S') then
		ds_evolucao_w :=  ds_evolucao_w || chr(13) || obter_desc_expressao(644029);
	end if;

	ds_evolucao_w :=  ds_evolucao_w || chr(13) || obter_desc_expressao(284569) || ': ';
	if (ie_modo_andar_w = 0) then
		ds_evolucao_w :=  ds_evolucao_w || 'Normal; sem deambula��o; cadeira de rodas; acamado';
	elsif (ie_modo_andar_w = 10) then
		ds_evolucao_w :=  ds_evolucao_w || obter_desc_expressao(487812);
	elsif (ie_modo_andar_w = 20) then
		ds_evolucao_w :=  ds_evolucao_w || 'Comprometida';
	end if;

	if (ie_estado_mental_w = 0) then
		ds_evolucao_w :=  ds_evolucao_w || chr(13) || obter_desc_expressao(289541) || ': Ciente das pr�prias capacidades e limita��es';
	elsif (ie_estado_mental_w = 15) then
		ds_evolucao_w :=  ds_evolucao_w || chr(13) || obter_desc_expressao(289541) || ': Superestima capacidade; esquece as limita��es';
	end if;
	
	ds_evolucao_w := ds_evolucao_w || chr(13) || obter_desc_expressao(328217) || ' ' || qt_pontuacao_w || ' ' || Obter_desc_escala_morse(qt_pontuacao_w);
	
	select	nvl(max(ie_relev_resumo_alta),'N')
	into	ie_relev_resumo_alta_w
	from	tipo_evolucao
	where   cd_tipo_evolucao = 'EEM';
	
	cd_especial_medico_w := obter_especialidade_pf(cd_profissional_w);

	insert into 	evolucao_paciente
			 (cd_evolucao,
			 dt_evolucao,
			 ie_tipo_evolucao,		
			 cd_pessoa_fisica,
			 nr_atendimento,				
			 ie_situacao,
			 dt_atualizacao,
			 nm_usuario,
			 ds_evolucao,
			 cd_medico,
			 cd_especialidade_medico,
			 dt_liberacao,
			 ie_evolucao_clinica,
			 ie_relev_resumo_alta)
	values (
			 evolucao_paciente_seq.nextval,
			 sysdate,
			 ie_tipo_evolucao_w,		
			 cd_pessoa_fisica_w,

			 nr_atendimento_w,		
			 'A',
			 sysdate,
			 nm_usuario_p,
			 ds_evolucao_w,
			 nvl(cd_medico_w,cd_profissional_w),

			 cd_especial_medico_w,
			 sysdate,
			 'EEM',
			 ie_relev_resumo_alta_w);

	commit;
	
end if;

end gerar_evolucao_Morse;
/