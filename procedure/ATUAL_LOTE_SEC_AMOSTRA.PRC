create or replace 
procedure atual_lote_sec_amostra(	nr_prescricao_p		number,
									ie_amostra_p		varchar2) is
	
pragma autonomous_transaction;
	
begin

update	lote_ent_sec_ficha
set		ie_amostra = ie_amostra_p
where	nr_prescricao = nr_prescricao_p
and		nvl(ie_amostra,'N') = 'N';

commit;

end atual_lote_sec_amostra;
/