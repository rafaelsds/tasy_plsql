create or replace
procedure Consistir_horario_copia_prescr (nr_prescricao_p	number,
					  nm_usuario_p		varchar2) is

DT_VALIDADE_PRESCR_w	Date;
DT_INICIO_PRESCR_w	Date;
dt_horario_w		Date;
hr_prim_horario_w	varchar2(10);
ie_primeiro_horario_w	varchar2(10);
hr_prim_horario_prescr_w Date;
dt_prim_horario_w	Date;
nr_seq_material_w	number(10,0);
nr_sequencia_w		number(10,0);
cd_material_w		number(6,0);
qt_horas_intervalo_w	number(6,0);

cursor c01 is
select	nr_sequencia,
	cd_material,
	TO_DATE(TO_CHAR(b.dt_prescricao,'dd/mm/yyyy') ||' '|| NVL(a.hr_prim_horario,TO_CHAR(NVL(b.dt_primeiro_horario,b.dt_prescricao),'hh24:mi')),'dd/mm/yyyy hh24:mi:ss'),
	obter_ocorrencia_intervalo(a.cd_intervalo, b.nr_horas_validade, 'H')
from	prescr_material a,
	prescr_medica b
where	a.nr_prescricao	= b.nr_prescricao
and	a.nr_prescricao	= nr_prescricao_p;

cursor c02 is
select	dt_horario
from	prescr_horario_pre
where	nr_prescricao	= nr_prescricao_p
and	cd_material	= cd_material_w
and	nr_seq_material	= nr_seq_material_w
order by dt_horario;

begin

delete	from prescr_mat_hor_copia
where	nr_prescricao	= nr_prescricao_p;

select	DT_INICIO_PRESCR,
	DT_VALIDADE_PRESCR
into	DT_INICIO_PRESCR_w,
	DT_VALIDADE_PRESCR_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_p;

open c01;
loop
fetch c01 into
	nr_seq_material_w,
	cd_material_w,
	dt_prim_horario_w,
	qt_horas_intervalo_w;
exit when c01%notfound;

	ie_primeiro_horario_w	:= 'N';

	open c02;
	loop
	fetch c02 into
		dt_horario_w;
	exit when c02%notfound;

		if	(dt_horario_w > DT_VALIDADE_PRESCR_w) then
			select	prescr_mat_hor_copia_seq.nextval
			into	nr_sequencia_w
			from	dual;

			insert into prescr_mat_hor_copia
				(nr_sequencia, 
				nm_usuario, 
				dt_atualizacao,
				nm_usuario_nrec, 
				dt_atualizacao_nrec,
				nr_prescricao, 
				nr_seq_material, 
				dt_horario, 
				cd_material, 
				ds_erro)
			values	(nr_sequencia_w, 
				nm_usuario_p, 
				sysdate,
				nm_usuario_p, 
				sysdate,
				nr_prescricao_p, 
				nr_seq_material_w, 
				dt_horario_w, 
				cd_material_w, 
				obter_desc_expressao(781889)/*'Hor�rio gerado supera a validade da prescri��o'*/);
		else
			if	((dt_prim_horario_w + qt_horas_intervalo_w/24) < dt_horario_w) and
				(ie_primeiro_horario_w = 'N') then

				select	prescr_mat_hor_copia_seq.nextval
				into	nr_sequencia_w
				from	dual;

				insert into prescr_mat_hor_copia
					(nr_sequencia, 
					nm_usuario, 
					dt_atualizacao,
					nm_usuario_nrec, 
					dt_atualizacao_nrec,
					nr_prescricao, 
					nr_seq_material, 
					dt_horario, 
					cd_material, 
					ds_erro)
				values	(nr_sequencia_w, 
					nm_usuario_p, 
					sysdate,
					nm_usuario_p, 
					sysdate,
					nr_prescricao_p, 
					nr_seq_material_w, 
					dt_horario_w - qt_horas_intervalo_w/24, 
					cd_material_w, 
					obter_desc_expressao(781885)/*'Hor�rio n�o foi gerado'*/);
			else
				ie_primeiro_horario_w	:= 'S';
			end if;
		end if;

	end loop;
	close c02;

end loop;
close c01;

commit;

end Consistir_horario_copia_prescr;
/
