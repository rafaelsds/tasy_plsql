create or replace
procedure desfazer_enviar_prot_doc(
			nr_sequencia_p	        	number,
			nm_usuario_p		Varchar2) is 

cd_atual_w				protocolo_documento.cd_pessoa_destino%type;
cd_pessoa_w				protocolo_documento.cd_pessoa_destino%type;
dt_rec_destino_w		protocolo_documento.dt_rec_destino%type;
ie_status_prot_w		varchar2(1);

begin

if	(nr_sequencia_p is not null) and 
	(nm_usuario_p is not null) then
	begin
	
	select	max(dt_rec_destino),
		max(cd_pessoa_destino)
	into	dt_rec_destino_w,
		cd_atual_w
	from	protocolo_documento
	where	nr_sequencia = nr_sequencia_p;
	
	select	substr(obter_dados_usuario_opcao(nm_usuario_p,'C'),1,10) 
	into	cd_pessoa_w
	from	dual;
	
	if	(dt_rec_destino_w is not null) then
		
		ie_status_prot_w := 'T';
		
		if	(cd_atual_w = cd_pessoa_w) then
			ie_status_prot_w := 'R';
		end if;
	end if;
	
	update	protocolo_documento
	set    	dt_envio 			= null,
		nm_usuario 		= nm_usuario_p,
		nm_usuario_envio 		= null,
		ie_status_protocolo = ie_status_prot_w
	where  nr_sequencia 		= nr_sequencia_p;
	commit;
	end;
end if;

end desfazer_enviar_prot_doc;
/
