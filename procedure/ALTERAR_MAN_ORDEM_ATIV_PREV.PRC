create or replace
procedure alterar_man_ordem_ativ_prev(
			nr_seq_estagio_p	number,
			nr_sequencia_p		number) is 

begin

update	man_ordem_ativ_prev 
set	dt_real = sysdate, 
	nr_seq_estagio = nr_seq_estagio_p 
where	nr_sequencia = nr_sequencia_p; 

commit;

end alterar_man_ordem_ativ_prev;
/