create or replace
procedure definir_valores_conta(
			nr_interno_conta_p		conta_paciente.nr_interno_conta%type,
			nm_usuario_p		varchar2,
			pr_coseguro_hosp_p	conta_paciente.pr_coseguro_hosp%type,
			pr_coseguro_honor_p	conta_paciente.pr_coseguro_honor%type,
			pr_coseg_nivel_hosp_p	conta_paciente.pr_coseg_nivel_hosp%type,
			vl_coseguro_hosp_p	conta_paciente.vl_coseguro_hosp%type,
			vl_coseguro_honor_p	conta_paciente.vl_coseguro_honor%type,
			vl_coseg_nivel_hosp_p	conta_paciente.vl_coseg_nivel_hosp%type,
			vl_deduzido_p		conta_paciente.vl_deduzido%type,
			vl_base_conta_p		conta_paciente.vl_base_conta%type,
			vl_maximo_coseguro_p	conta_paciente.vl_maximo_coseguro%type) is 

begin

update	conta_paciente
set	pr_coseguro_hosp	= pr_coseguro_hosp_p,
	pr_coseguro_honor	= pr_coseguro_honor_p,
	pr_coseg_nivel_hosp	= pr_coseg_nivel_hosp_p,
	vl_coseguro_hosp	= vl_coseguro_hosp_p,
	vl_coseguro_honor	= vl_coseguro_honor_p,
	vl_coseg_nivel_hosp	= vl_coseg_nivel_hosp_p,
	vl_deduzido		= vl_deduzido_p,
	vl_base_conta		= vl_base_conta_p,
	vl_maximo_coseguro	= vl_maximo_coseguro_p,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	dt_definicao_conta = sysdate
where	nr_interno_conta = nr_interno_conta_p;

commit;

end definir_valores_conta;
/
