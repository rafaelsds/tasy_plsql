create or replace
procedure js_obter_dados_atend_quimio(
		nr_seq_paciente_p			number,
		nr_atendimento_p			number,
		ds_estagio_autor_p			varchar2,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p		number,
        NR_SEQ_ESTAG_AUTOR_p     number default 0,
		cd_pessoa_fisica_p		out	varchar2,
		cd_medico_resp_p		out	varchar2,
		ds_erro_p			out	varchar2) is 

ie_permite_atend_autorizado_w	varchar2(1);

begin

/* Quimioterapia - Par�metro [99] - Permitir gerar atendimento apenas para pacientes onde o Est�gio autoriza��o esteja como Autorizado */
obter_param_usuario(3130, 99, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_permite_atend_autorizado_w);

if	(ie_permite_atend_autorizado_w = 'S') and
	(nvl(NR_SEQ_ESTAG_AUTOR_p,1) <> 10) then
	ds_erro_p	:= substr(obter_texto_tasy(60068, wheb_usuario_pck.get_nr_seq_idioma),1,255);

elsif	(nr_atendimento_p is null) then
	begin
	select	max(cd_pessoa_fisica),
		max(cd_medico_resp)
	into	cd_pessoa_fisica_p,
		cd_medico_resp_p
	from	paciente_setor
	where	nr_seq_paciente = nr_seq_paciente_p;
	end;
end if;

end js_obter_dados_atend_quimio;
/
