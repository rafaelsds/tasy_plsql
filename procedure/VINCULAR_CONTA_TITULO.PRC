create or replace
procedure Vincular_Conta_Titulo
		(nr_interno_conta_p		number,
		nr_titulo_p		number,
		nm_usuario_p		varchar2,
		cd_estab_titulo_p		number) is


begin

update	titulo_receber
set	nr_interno_conta	= nr_interno_conta_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	cd_estabelecimento	= nvl(cd_estab_titulo_p, cd_estabelecimento)
where	nr_titulo		= nr_titulo_p;

/*insert 	into logxxxxxx_tasy
	(dt_atualizacao,
	nm_usuario,
	cd_log,
	ds_log)
values	(sysdate,
	nm_usuario_p,
	5501,
	'Vincula��o t�tulo conta ' || ' T�tulo: ' || nr_titulo_p || ' Conta: ' || nr_interno_conta_p);*/

commit;

end Vincular_Conta_Titulo;
/