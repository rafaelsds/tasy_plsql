create or replace
procedure GERAR_PAGTO_ITAU_CNAB_240_V40
		(	nr_seq_envio_p		number,
			nm_usuario_p		varchar2) is

/* PADR�O CNAB 240 VERS�O 4.0 */

ds_conteudo_w		varchar2(240);
cd_estabelecimento_w	number(4);
nr_seq_apres_w		number(10)	:= 0;

/* header de arquivo */
cd_agencia_estab_w	varchar2(4);
cd_cgc_estab_w		varchar2(14);
cd_convenio_banco_w	varchar2(13);
dt_geracao_w		varchar2(14);
nm_empresa_w		varchar2(30);
nr_conta_estab_w	varchar2(5);
nr_remessa_w		number(10);
ie_digito_estab_w	varchar2(1);
ds_banco_w		varchar2(30);
cd_banco_cobr_w		number(3);

/* header de lote - DOC */
nr_lote_servico_w	number(10);
ie_forma_lanc_w		varchar2(2);
ie_tipo_pagamento_w	varchar2(3);
ie_finalidade_w		varchar2(2);
ds_endereco_w		varchar2(30);
nr_endereco_w		varchar2(5);
ds_complemento_w	varchar2(15);
ds_municipio_w		varchar2(20);
nr_cep_w		varchar2(8);
sg_estado_w		varchar2(15);

cursor	c01 is
select	distinct
	decode(b.ie_tipo_pagamento,'CC','01','DOC','03','TED','41','OP','30','BLQ',decode(b.cd_banco,341,'30','31'),'03'),
	b.ie_tipo_pagamento,
	decode(b.ie_tipo_pagamento,'CC','01','99')
from	titulo_pagar_escrit b,
	banco_escritural a
where	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* detalhe - DOC */
nr_sequencia_w		number(10);
cd_camara_compensacao_w	varchar2(3);
cd_banco_w		number(3);
cd_agencia_bancaria_w	varchar2(4);
nr_conta_w		varchar2(5);
nm_pessoa_w		varchar2(30);
nr_titulo_w		number(10);
dt_remessa_retorno_w	date;
vl_escritural_w		number(15,2);
vl_acrescimo_w		number(15,2);
vl_desconto_w		number(15,2);
vl_despesa_w		number(15,2);
ie_tipo_inscricao_w	varchar2(1);
nr_inscricao_w		varchar2(14);
dt_vencimento_w		date;
vl_juros_w		number(15,2);
vl_multa_w		number(15,2);
cd_agencia_conta_w	varchar2(20);
ie_digito_conta_w	varchar2(1);
ds_endereco_det_w	varchar2(30);
nr_endereco_det_w	varchar2(5);
ds_complemento_det_w	varchar2(15);
ds_municipio_det_w	varchar2(20);
nr_cep_det_w		varchar2(8);
sg_estado_det_w		varchar2(15);
ds_bairro_det_w		varchar2(15);
cd_camara_w		varchar2(3);
qt_favorecido_w		number(10);

cursor	c02 is
select	b.cd_banco,
	lpad(substr(b.cd_agencia_bancaria,1,4),4,'0'),
	rpad(substr(obter_nome_pf_pj(c.cd_pessoa_fisica,c.cd_cgc),1,30),30,' '),
	c.nr_titulo,
	b.vl_escritural,
	b.vl_acrescimo,
	b.vl_desconto,
	b.vl_despesa,
	lpad(nvl(d.nr_cpf,c.cd_cgc),14,'0'),
	substr(b.nr_conta,1,5) nr_conta,
	substr(b.ie_digito_conta,1,1),
	c.dt_vencimento_atual,
	b.vl_juros,
	b.vl_multa,
	decode(c.cd_pessoa_fisica,null,'2','1') ie_tipo_inscricao,
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'R'),1,30),' '),30,' '),
	lpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'NR'),1,5),'0'),5,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CO'),1,15),' '),15,' '),
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CI'),1,20),' '),20,' '),
	lpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'CEP'),1,8),'0'),8,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'UF'),1,2),' '),2,' '),
	rpad(nvl(substr(obter_dados_pf_pj(c.cd_pessoa_fisica,c.cd_cgc,'B'),1,15),' '),15,' '),
	decode(b.ie_tipo_pagamento,'DOC','700','TED','018','000') cd_camara
from	pessoa_fisica d,
	titulo_pagar c,
	titulo_pagar_escrit b,
	banco_escritural a
where	c.cd_pessoa_fisica	= d.cd_pessoa_fisica(+)
and	b.nr_titulo		= c.nr_titulo
and	b.ie_tipo_pagamento	= ie_tipo_pagamento_w
and	a.nr_sequencia		= b.nr_seq_escrit
and	a.nr_sequencia		= nr_seq_envio_p;

/* trailer lote - DOC */
vl_total_w		number(15,2);

/* detalhe - BLQ */
nr_bloqueto_w		varchar2(44);
nr_nosso_numero_w	varchar2(20);
ie_tipo_servico_w	varchar2(2);
ie_converte_bloq_w	varchar2(1);

/* trailer do arquivo */
qt_registro_w		number(10);

begin

delete	from w_envio_banco
where	nm_usuario	= nm_usuario_p;

/* header de arquivo */
nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;

select	lpad(nvl(b.cd_cgc,'0'),14,'0'),
	rpad(substr(nvl(c.cd_convenio_banco,' '),1,13),13,' '),
	lpad(substr(c.cd_agencia_bancaria,1,4),4,'0'),
	lpad(somente_numero(substr(c.cd_conta,1,5)),5,'0'),
	rpad(substr(obter_nome_estabelecimento(b.cd_estabelecimento),1,30),30,' '),
	to_char(sysdate,'ddmmyyyyhhmiss'),
	nvl(a.nr_remessa,a.nr_sequencia),
	b.cd_estabelecimento,
	a.dt_remessa_retorno,
	rpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'R'),1,30),' '),30,' '),
	lpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'NR'),1,5),'0'),5,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'CO'),1,15),' '),15,' '),
	rpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'CI'),1,20),' '),20,' '),
	lpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'CEP'),1,8),'0'),8,'0'),
	rpad(nvl(substr(obter_dados_pf_pj(null,b.cd_cgc,'UF'),1,2),' '),2,' '),
	nvl(c.ie_digito_conta,'0'),
	c.cd_banco,
	substr(e.ds_banco,1,30) ds_banco
into	cd_cgc_estab_w,
	cd_convenio_banco_w,
	cd_agencia_estab_w,
	nr_conta_estab_w,
	nm_empresa_w,
	dt_geracao_w,
	nr_remessa_w,
	cd_estabelecimento_w,
	dt_remessa_retorno_w,
	ds_endereco_w,
	nr_endereco_w,
	ds_complemento_w,
	ds_municipio_w,
	nr_cep_w,
	sg_estado_w,
	ie_digito_estab_w,
	cd_banco_cobr_w,
	ds_banco_w
from	banco e,
	agencia_bancaria d,
	banco_estabelecimento c,
	estabelecimento b,
	banco_escritural a
where	c.cd_banco		= e.cd_banco
and	c.cd_banco		= d.cd_banco
and	c.cd_agencia_bancaria	= d.cd_agencia_bancaria
and	a.nr_seq_conta_banco	= c.nr_sequencia
and	a.cd_estabelecimento	= b.cd_estabelecimento
and	a.nr_sequencia		= nr_seq_envio_p;

obter_param_usuario(857,36,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_converte_bloq_w);

ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
			'0000' ||
			'0' ||
			rpad(' ',9,' ') ||
			'2' ||
			cd_cgc_estab_w ||
			cd_convenio_banco_w ||
			rpad(' ',7,' ') ||
			'0' ||
			cd_agencia_estab_w ||
			' ' ||
			rpad('0',7,'0') ||
			nr_conta_estab_w ||
			' ' ||
			ie_digito_estab_w ||
			nm_empresa_w ||
			rpad(upper(ds_banco_w),30,' ') ||
			rpad(' ',10,' ') ||
			'1' ||
			dt_geracao_w ||
			lpad(nr_remessa_w,6,'0') ||
			'040' ||
			'00000' ||
			rpad(' ',69,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

open	c01;
loop
fetch	c01 into
	ie_forma_lanc_w,
	ie_tipo_pagamento_w,
	ie_finalidade_w;
exit	when c01%notfound;

	/* header de lote */
	nr_lote_servico_w	:= nvl(nr_lote_servico_w,0) + 1;
	nr_seq_apres_w		:= nvl(nr_seq_apres_w,0) + 1;
	nr_sequencia_w		:= 0;
	vl_total_w		:= 0;

	ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
				lpad(nr_lote_servico_w,4,'0') ||
				'1' ||
				'D' ||
				'05' ||
				'50' ||
				'030' ||
				' ' ||
				'2' ||
				cd_cgc_estab_w ||
				cd_convenio_banco_w ||
				rpad(' ',7,' ') ||
				'0' ||
				cd_agencia_estab_w ||
				' ' ||
				rpad('0',7,'0') ||
				nr_conta_estab_w ||
				' ' ||
				ie_digito_estab_w ||
				nm_empresa_w ||
				rpad(' ',40,' ') ||
				ds_endereco_w ||
				nr_endereco_w ||
				ds_complemento_w ||
				ds_municipio_w ||
				nr_cep_w ||
				substr(sg_estado_w,1,2) ||
				rpad(' ',18,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

	open	c02;
	loop
	fetch	c02 into
		cd_banco_w,
		cd_agencia_bancaria_w,
		nm_pessoa_w,
		nr_titulo_w,
		vl_escritural_w,
		vl_acrescimo_w,
		vl_desconto_w,
		vl_despesa_w,
		nr_inscricao_w,
		nr_conta_w,
		ie_digito_conta_w,
		dt_vencimento_w,
		vl_juros_w,
		vl_multa_w,
		ie_tipo_inscricao_w,
		ds_endereco_det_w,
		nr_endereco_det_w,
		ds_complemento_det_w,
		ds_municipio_det_w,
		nr_cep_det_w,
		sg_estado_det_w,
		ds_bairro_det_w,
		cd_camara_w;
	exit	when c02%notfound;

		select	count(*)
		into	qt_favorecido_w
		from	titulo_pagar_favorecido a
		where	a.nr_titulo	= nr_titulo_w;

		if	(nvl(qt_favorecido_w,0)	> 0) then

			select	somente_numero(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'B'),1,3)) cd_banco,
				lpad(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'A'),1,5) ||
				substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DA'),1,1),6,'0') cd_agencia_bancaria,
				substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'C'),1,255) nr_conta,
				nvl(substr(obter_conta_pessoa(nvl(a.cd_cgc,a.cd_pessoa_fisica),decode(a.cd_cgc,null,'F','J'),'DC'),1,1),'0') ie_digito_conta
			into	cd_banco_w,
				cd_agencia_bancaria_w,
				nr_conta_w,
				ie_digito_conta_w
			from	titulo_pagar_favorecido a
			where	a.nr_titulo	= nr_titulo_w;

		end if;

		/* segmento A */
		nr_sequencia_w	:= nvl(nr_sequencia_w,0) + 1;
		nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;
		qt_registro_w	:= nvl(qt_registro_w,0) + 1;
		vl_total_w	:= nvl(vl_total_w,0) + (nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0));

		ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
					lpad(nr_lote_servico_w,4,'0') ||
					'3' ||
					lpad(nr_sequencia_w,5,'0') ||
					'A' ||
					'000' ||
					cd_camara_w ||
					lpad(cd_banco_w,3,'0') ||
					'0' ||
					cd_agencia_bancaria_w ||
					' ' ||
					rpad('0',7,'0') ||
					lpad(substr(nr_conta_w,1,5),5,'0') ||
					' ' ||
					ie_digito_conta_w ||
					nm_pessoa_w ||
					rpad(nr_titulo_w,20,' ') ||
					to_char(dt_remessa_retorno_w,'ddmmyyyy') ||
					'BRL' ||
					'000000000000000' ||
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0),'9999999999990.00')),15,'0') ||
					rpad(' ',20,' ') ||
					'00000000' ||
					lpad(somente_numero(to_char(nvl(vl_escritural_w,0) - nvl(vl_desconto_w,0) + nvl(vl_acrescimo_w,0) + nvl(vl_despesa_w,0),'9999999999990.00')),15,'0') ||
					'00' ||
					rpad('0',17,'0') ||
					rpad(' ',16,' ') ||
					rpad(' ',4,' ') ||
					nr_inscricao_w ||
					rpad(' ',10,' ');

		insert	into w_envio_banco
			(cd_estabelecimento,
			ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_seq_apres,
			nr_seq_apres_2,
			nr_sequencia)
		values	(cd_estabelecimento_w,
			ds_conteudo_w,
			sysdate,
			nm_usuario_p,
			nr_seq_apres_w,
			nr_seq_apres_w,
			w_envio_banco_seq.nextval);

	end	loop;
	close	c02;

	/* trailer de lote */
	nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

	ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
				lpad(nr_lote_servico_w,4,'0') ||
				'5' ||
				rpad(' ',9,' ') ||
				lpad(nr_sequencia_w + 2,6,'0') ||
				lpad(somente_numero(to_char(nvl(vl_total_w,0),'9999999999999990.00')),18,'0') ||
				'000000000000000000' ||
				'000000' ||
				rpad(' ',175,' ');

	insert	into w_envio_banco
		(cd_estabelecimento,
		ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_seq_apres,
		nr_seq_apres_2,
		nr_sequencia)
	values	(cd_estabelecimento_w,
		ds_conteudo_w,
		sysdate,
		nm_usuario_p,
		nr_seq_apres_w,
		nr_seq_apres_w,
		w_envio_banco_seq.nextval);

end	loop;
close	c01;

if	(cd_banco_cobr_w	= 1) then

	ie_tipo_servico_w	:= '20';

else

	ie_tipo_servico_w	:= '03';

end if;

/* trailer de arquivo */
nr_seq_apres_w	:= nvl(nr_seq_apres_w,0) + 1;

ds_conteudo_w	:=	lpad(cd_banco_cobr_w,3,'0') ||
			'9999' ||
			'9' ||
			rpad(' ',9,' ') ||
			lpad(nr_lote_servico_w,6,'0') ||
			lpad(nvl(qt_registro_w,0) + (nvl(nr_lote_servico_w,0) * 2) + 2,6,'0') ||	/* registros detalhe + header e trailer dos lotes + header e trailer do arquivo */
			'000000' ||
			rpad(' ',205,' ');

insert	into w_envio_banco
	(cd_estabelecimento,
	ds_conteudo,
	dt_atualizacao,
	nm_usuario,
	nr_seq_apres,
	nr_seq_apres_2,
	nr_sequencia)
values	(cd_estabelecimento_w,
	ds_conteudo_w,
	sysdate,
	nm_usuario_p,
	nr_seq_apres_w,
	nr_seq_apres_w,
	w_envio_banco_seq.nextval);

commit;

end GERAR_PAGTO_ITAU_CNAB_240_V40;
/