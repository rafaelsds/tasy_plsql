create or replace
procedure proj_aprovar_rat
			(	nr_seq_rat_p	in	number,
				nm_usuario_p	in	varchar2) is


qt_hora_cobrar_w		Number(17,2);
vl_hora_cobrar_w		Number(15,2);
vl_hora_w		Number(15,4);
vl_pagar_w		Number(15,2);
vl_parametro_p		varchar2(1);	
nr_seq_cliente_w		Number(10,0);
cd_consultor_w		Varchar2(10);
dt_final_rat_w		Date;
ie_paga_rat_w		Varchar2(01);
ie_funcao_exec_w		Varchar2(15);
NR_SEQ_PROJ_w		number(10,0);
qt_valida_w		number(10);
ie_regime_contr_w	proj_rat.ie_regime_contr%type;

Cursor C01 is
	select	vl_hora
	from	proj_consultor_aval
	where	nr_seq_cliente		= nr_seq_cliente_w
	and	cd_consultor		= cd_consultor_w
	and	dt_inicio_validade		<= dt_final_rat_w
	and	ie_funcao_exec		= ie_funcao_exec_w
	order by dt_inicio_validade;

BEGIN

select	cd_executor,
	nr_seq_cliente,
	dt_final,
	qt_hora_cobrar,
	ie_paga_rat,
	nvl(ie_funcao_exec,'CONSULT'),
	NR_SEQ_PROJ,
	ie_regime_contr
into	cd_consultor_w,
	nr_seq_cliente_w,
	dt_final_rat_w,
	qt_hora_cobrar_w,
	ie_paga_rat_w,
	ie_funcao_exec_w,
	NR_SEQ_PROJ_w,
	ie_regime_contr_w
from	proj_rat
where	nr_sequencia	= nr_seq_rat_p;

open c01;
loop
fetch c01 into 
	vl_hora_w;
exit when c01%notfound;
	begin
	vl_hora_w	:= vl_hora_w;
	end;
end loop;
close c01;

select	Obter_Hora_RAT_Cobrar(NR_SEQ_PROJ_w, cd_consultor_w, dt_final_rat_w)
into	vl_hora_cobrar_w
from	dual;

vl_pagar_w	:= vl_hora_w * qt_hora_cobrar_w;

Obter_Param_Usuario(993,151,obter_perfil_ativo,nm_usuario_p,obter_estabelecimento_ativo,vl_parametro_p); 

if (vl_parametro_p is not null or vl_parametro_p <> '') then
	
	select	count(nr_sequencia)
	into	qt_valida_w
	from	proj_rat  a
	where	nr_sequencia	= nr_seq_rat_p	
	and	exists (select	1			
			from	proj_consultor_aval b
			where	cd_consultor = cd_consultor_w
			and	nr_seq_proj = nr_seq_proj_w
			and	b.dt_inicio_validade between (a.dt_inicio-to_number(vl_parametro_p)) and a.dt_final);
			
	if	(qt_valida_w = 0) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(284281);
	end if; 

end if;

if(ie_regime_contr_w = 'PJ') then
	vl_hora_w	:= 0;
	vl_pagar_w	:= 0;
end if;

update	proj_rat
set	vl_hora_pagar	= vl_hora_w,
	vl_pagar		= vl_pagar_w,
	vl_hora_cobrar	= vl_hora_cobrar_w,
	dt_aprovacao	= sysdate,
	nm_usuario_aprov	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p
where	nr_sequencia	= nr_seq_rat_p;

commit;

end proj_aprovar_rat;
/
