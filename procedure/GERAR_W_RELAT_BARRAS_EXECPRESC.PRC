create or replace
PROCEDURE GERAR_W_RELAT_BARRAS_EXECPRESC(	cd_material_p		NUMBER,
						ds_validade_p		VARCHAR2,
						cd_unidade_medida_p	VARCHAR2,
						qt_material_p		NUMBER,
						nm_usuario_p		VARCHAR2) IS

ds_material_w	VARCHAR2(255);

BEGIN

ds_material_w:=	SUBSTR(obter_desc_material(cd_material_p),1,200);

INSERT INTO w_relat_barras_execpresc(	cd_material,
					ds_validade,
					cd_unidade_medida,
					qt_material,
					nm_usuario,
					dt_atualizacao)
    VALUES		  (	cd_material_p,
				ds_validade_p,
				cd_unidade_medida_p,
				qt_material_p,
				nm_usuario_p,
				SYSDATE);

COMMIT;

END gerar_w_relat_barras_execpresc;
/