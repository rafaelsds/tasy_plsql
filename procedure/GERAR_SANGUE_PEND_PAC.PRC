create or replace procedure gerar_sangue_pend_pac(nr_seq_reserva_p	number,
                                                  nr_seq_item_reserva_p number) is 

nr_sequencia_w san_reserva.nr_sequencia%type;
begin
select 	max(b.nr_sequencia)
into	nr_sequencia_w
from	san_reserva_item c,
		san_reserva b,
		san_transfusao a
where	c.nr_seq_reserva = b.nr_sequencia
and		b.nr_sequencia = nr_seq_reserva_p
and     nr_seq_derivado not in (select  nr_seq_derivado  
								from    san_producao x,
										san_emprestimo y
								where   y.nr_sequencia = x.nr_seq_emp_ent
								and     y.cd_pf_destino = b.cd_pessoa_fisica);
								
if (nvl(nr_sequencia_w,0) > 0) then
	update san_reserva_item
	set IE_PEND_PACIENTE = 'S'
	where nr_seq_reserva = nr_sequencia_w
    and   (nr_seq_item = nr_seq_item_reserva_p or nr_seq_item_reserva_p is null);
	
	commit;
end if;

end gerar_sangue_pend_pac;
/