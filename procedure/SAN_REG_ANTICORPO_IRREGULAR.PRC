create or replace
procedure san_reg_anticorpo_irregular(
			nm_usuario_p		Varchar2,
			qt_reg_anticorpo_irregular_p	number,
			nr_seq_anticorpo_irregular_p	number) is 
indice_w	integer;
begin

if	(qt_reg_anticorpo_irregular_p > 0) then
	begin
	
	for indice_w in 1..qt_reg_anticorpo_irregular_p loop
		begin
		
		insert into san_anticorpo_irreg_val(nr_sequencia,
						    dt_atualizacao,
						    nm_usuario,
						    dt_atualizacao_nrec,
						    nm_usuario_nrec,
						    nr_seq_anticorpo_irreg,
						    nr_seq_apresent)
					values(san_anticorpo_irreg_val_seq.nextval,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_anticorpo_irregular_p,
						indice_w);
		end;
	end loop;
	end;
				
end if;

commit;

end san_reg_anticorpo_irregular;
/