create or replace
procedure define_dt_juros_tit_rec (
			nr_titulo_p		number,
			ie_data_juros_multa_p	varchar2) is 

begin

if	(nr_titulo_p is not null) and
	(ie_data_juros_multa_p is not null) then
	
	update	titulo_receber
	set	ie_data_juros_multa = ie_data_juros_multa_p
	where	nr_titulo = nr_titulo_p;
	
end if;

commit;

end define_dt_juros_tit_rec;
/