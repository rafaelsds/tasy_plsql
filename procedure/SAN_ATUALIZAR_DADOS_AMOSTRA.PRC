create or replace
procedure San_Atualizar_Dados_Amostra(	nr_seq_doacao_p		number,
					nm_usuario_p		Varchar2) is 

nr_seq_doacao_amostra_w	Number(10);
qt_peso_w		Number(6,3);
qt_altura_w		Number(4,1);
qt_temperatura_w	Number(4,2);
pr_hematocrito_w	Number(10,4);
					
begin

if (nr_seq_doacao_p is not null) then

	select	max(nr_seq_doacao_amostra)
	into	nr_seq_doacao_amostra_w
	from	san_doacao
	where	nr_sequencia = nr_seq_doacao_p;

	select	max(qt_peso),
		max(qt_altura),
		max(qt_temperatura),
		max(pr_hematocrito)
	into	qt_peso_w,
		qt_altura_w,
		qt_temperatura_w,
		pr_hematocrito_w
	from	san_doacao
	where	nr_sequencia = nr_seq_doacao_amostra_w;

	update	san_doacao
	set	qt_peso		= qt_peso_w,
		qt_altura	= qt_altura_w,
		qt_temperatura	= qt_temperatura_w,
		pr_hematocrito	= pr_hematocrito_w
	where	nr_sequencia = nr_seq_doacao_p;
	
end if;

commit;

end San_Atualizar_Dados_Amostra;
/
