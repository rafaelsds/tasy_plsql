create or replace
procedure pls_atualizar_intercmabio_lote(
			nr_sequencia_p		number,
			cd_estabelecimento_p	number,
			nm_usuario_p		varchar2) is 

begin
if	(nr_sequencia_p is not null) and
	(cd_estabelecimento_p is not null) and
	(nm_usuario_p is not null) then
	begin

	update	ptu_intercambio_lote_envio
	set	dt_geracao_arquivo	= sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;
	
	end;
end if;

commit;

end pls_atualizar_intercmabio_lote;
/