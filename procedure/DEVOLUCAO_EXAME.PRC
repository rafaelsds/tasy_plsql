CREATE OR REPLACE
PROCEDURE Devolucao_Exame(
			nr_prontuario_p		Number,
			nm_usuario_p		Varchar2) IS 

qt_emprestimo_w		Number(10);

BEGIN

select	count(*)
into	qt_emprestimo_w
from	exame_emprestimo
where	nr_prontuario	= nr_prontuario_p
and	dt_devolucao is null;

if	(qt_emprestimo_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(282431);
end if;

update	exame_emprestimo
set	dt_devolucao	= sysdate,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	nm_usuario_dev	= nm_usuario_p
where	nr_prontuario	= nr_prontuario_p
and	dt_devolucao is null;

END Devolucao_Exame;
/