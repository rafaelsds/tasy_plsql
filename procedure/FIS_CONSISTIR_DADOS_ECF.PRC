create or replace
procedure fis_consistir_dados_ecf( cd_empresa_p		number,
				   cd_estabelecimento_p		number,
				   dt_inicial_p		date,
				   dt_final_p		date,
				   nm_usuario_p		varchar2) is 



/*variaveis gerais */
cd_empresa_w				empresa.cd_empresa%type;
cd_estabelecimento_w			estabelecimento.cd_estabelecimento%type;
cd_consistencia_w			w_ctb_sped_consistencia.cd_consistencia%type;
ds_consistencia_w			w_ctb_sped_consistencia.ds_consistencia%type;
i					integer	:= 0;

type registros is table of w_ctb_sped_consistencia%rowtype index by pls_integer;
consistencia_w				registros;


/*vari�veis que obtem os dados para consist�ncia */
	
dt_inicio_apuracao_w			 fis_controle_ecf.dt_inicio_apuracao%type;
dt_fim_apuracao_w			 fis_controle_ecf.dt_fim_apuracao%type;
qt_registro_w				 number(10);
pr_mov_sem_agrup_w			 number(15,2); 
cd_cgc_w                                 pessoa_juridica.cd_cgc%type;
ds_endereco_w                            pessoa_juridica.ds_endereco%type;
nr_endereco_w                            pessoa_juridica.nr_endereco%type;
ds_bairro_w                              pessoa_juridica.ds_bairro%type;
sg_estado_w                              pessoa_juridica.sg_estado%type;
cd_municipio_ibge_w                      pessoa_juridica.cd_municipio_ibge%type;
cd_cep_w                                 pessoa_juridica.cd_cep%type;
nr_seq_cnae_w                            pessoa_juridica.nr_seq_cnae%type;
ds_email_w                               pessoa_juridica_estab.ds_email%type;
dt_alt_w                                 date;
cd_nat_juridica_w                        pessoa_juridica.nr_seq_nat_juridica%type;
cd_conta_contabil_w                      conta_contabil.cd_conta_contabil%type;
qtd_w                                    number(10);
mes_ref_final_w				 date;
dt_ref_inicial_w                         date; 
dt_ref_final_w                           date;
cd_encerramento_exercicio_w		 number;
qt_mes_aberto_w                          number;
qt_lote_encerramento_w		         number;




begin

delete	w_ctb_sped_consistencia
where	nr_seq_controle_sped is null
and	nm_usuario = nm_usuario_p;
commit;

cd_empresa_w		:= cd_empresa_p;
cd_estabelecimento_w	:= cd_estabelecimento_p;
dt_ref_inicial_w	:= dt_inicial_p;
dt_ref_final_w		:= dt_final_p;
mes_ref_final_w 	:= TRUNC(dt_ref_inicial_w, 'MM');

select cd_cgc
into   cd_cgc_w
from   estabelecimento
where  cd_estabelecimento = cd_estabelecimento_p;

select count(*)
into   qtd_w
from   conta_contabil a
where  a.ie_tipo = 'A'
and    (a.dt_fim_vigencia is null or a.dt_fim_vigencia between dt_inicial_p and dt_final_p)
and    (a.cd_conta_contabil in (select b.cd_conta_contabil
			       from   conta_contabil_classif_ecd b
			       where  cd_versao <> '3.1')
			       or not exists 
			       (select 1
			       from   conta_contabil_classif_ecd x
			       where  x.cd_conta_contabil = a.cd_conta_contabil));


select  ds_endereco,                           
	nr_endereco,                          
	ds_bairro,                              
	sg_estado,                             
	cd_municipio_ibge,                     
	cd_cep,                               
	nr_seq_cnae,
	nr_seq_nat_juridica
into	ds_endereco_w,
        nr_endereco_w,
	ds_bairro_w,
	sg_estado_w,
	cd_municipio_ibge_w,
	cd_cep_w,
	nr_seq_cnae_w,
	cd_nat_juridica_w
from  	pessoa_juridica a
where 	a.cd_cgc = cd_cgc_w;  

			
select a.ds_email
into   ds_email_w 
from   pessoa_juridica_estab a,
       pessoa_juridica b
where  a.cd_cgc = b.cd_cgc
and    b.cd_cgc = cd_cgc_w;    

select	count(*) 
into	qt_mes_aberto_w
from	ctb_mes_ref
where	dt_referencia between dt_inicial_p and dt_final_p
and	dt_fechamento is null;
  


if (nvl(ds_endereco_w,'X') = 'X') then 

   i				     := i + 1;
   consistencia_w(i).cd_consistencia := 1;
   consistencia_w(i).ds_consistencia := 'Falta informar o endere�o do estabelecimento.';
   consistencia_w(i).ds_acao         := 'Cadastrar o endere�o na fun��o Pessoa J�ridica.';
   consistencia_w(i).qt_tempo	     := 1;

end if;

if (nvl(nr_endereco_w,'X') = 'X') then

    i				      := i + 1; 
    consistencia_w(i).cd_consistencia := 2;
    consistencia_w(i).ds_consistencia := 'Falta informar o n�mero do endere�o do estabelecimento.';
    consistencia_w(i).ds_acao         := 'Cadastrar o n�mero do endere�o na fun��o Pessoa J�ridica.';
    consistencia_w(i).qt_tempo	      := 1;



end if;

if (nvl(ds_bairro_w,'X') = 'X') then 
    i				      := i + 1;
    consistencia_w(i).cd_consistencia := 3;
    consistencia_w(i).ds_consistencia := 'Falta informar o bairro da empresa.';
    consistencia_w(i).ds_acao         := 'Cadastrar o bairro da empresa na fun��o Pessoa Jur�dica.';
    consistencia_w(i).qt_tempo	      := 1; 



end if;

if (nvl(sg_estado_w,'X') = 'X') then 

   i				     := i + 1;
   consistencia_w(i).cd_consistencia := 4;
   consistencia_w(i).ds_consistencia := 'Falta informar a unidade da federa��o do estabelecimento.';
   consistencia_w(i).ds_acao         := 'Cadastrar o estado da federa��o na fun��o Pessoa J�ridica.';
   consistencia_w(i).qt_tempo	     := 1; 



end if;

if (nvl(cd_municipio_ibge_w,'X') = 'X') then 

    i				      := i + 1;
    consistencia_w(i).cd_consistencia := 5;
    consistencia_w(i).ds_consistencia := 'Falta informar o munic�pio do estabelecimento.';
    consistencia_w(i).ds_acao         := 'Cadastrar o munic�pio na fun��o Pessoa J�ridica.';
    consistencia_w(i).qt_tempo	      := 1; 


end if;

if (nvl(cd_cep_w,'X') = 'X') then

   i				      := i + 1; 
   consistencia_w(i).cd_consistencia  := 6;
   consistencia_w(i).ds_consistencia  := 'Falta informar o CEP do estabelecimento.';
   consistencia_w(i).ds_acao          := 'Cadastrar o CEP na fun��o Pessoa J�ridica.';
   consistencia_w(i).qt_tempo	      := 1;


end if;

if (nvl(nr_seq_cnae_w,0) = 0) then

   i				      := i + 1; 
   consistencia_w(i).cd_consistencia  := 7;
   consistencia_w(i).ds_consistencia  := 'Falta informar o CNAE (C�digo da atividade econ�mica) da empresa.';
   consistencia_w(i).ds_acao          := 'Cadastrar o c�digo CNAE da empresa na fun��o Pessoa Jur�dica.';
   consistencia_w(i).qt_tempo	      := 5;


end if;


if (nvl(ds_email_w,'X') = 'X') then

  i	                             := i + 1;
  consistencia_w(i).cd_consistencia  := 8;
  consistencia_w(i).ds_consistencia  := 'Falta o informar o Email da empresa.';
  consistencia_w(i).ds_acao          := 'Cadastrar o email da empresa na fun��o Pessoa Jur�dica.';
  consistencia_w(i).qt_tempo	     := 5;
   
 else
     if not (ds_email_w like '%@%') then
     
     i	                                := i + 1;
     consistencia_w(i).cd_consistencia  := 8;  
     consistencia_w(i).ds_consistencia  := 'O email cadastrado � inv�lido.';
     consistencia_w(i).ds_acao          := 'Informar um email v�lido para a empresa na fun��o Pessoa Jur�dica.';
     consistencia_w(i).qt_tempo	        := 5;
     
     end if;

end if;

if (qtd_w  > 0) then
	
    i	                                   := i + 1;
    consistencia_w(i).cd_consistencia      := 9;
    consistencia_w(i).ds_consistencia      := 'Existem'||' '||qtd_w||' '|| 'contas cont�beis anal�ticas vigentes sem conta cont�bil referencial 3.1 vinculada';
    consistencia_w(i).ds_acao              := 'Vincular uma conta cont�bil referencial para as contas na fun��o Empresa/Estabelecimento/Contas/CC, pasta Conta referencial.';
    consistencia_w(i).qt_tempo	           := qtd_w * 1;
		
end if;

cd_encerramento_exercicio_w := nvl(obter_valor_param_usuario(923, 6, obter_perfil_ativo,  nm_usuario_p, cd_estabelecimento_w),13);

select count(*)
into	qt_lote_encerramento_w
from	lote_contabil a,
	estabelecimento b
where	b.cd_estabelecimento		= a.cd_estabelecimento
and	b.cd_empresa			= cd_empresa_w
and	a.cd_estabelecimento		= nvl(cd_estabelecimento_w, a.cd_estabelecimento)
and	trunc(dt_referencia, 'MM')	= trunc(mes_ref_final_w,'mm')
and	a.cd_tipo_lote_contabil		= cd_encerramento_exercicio_w;

if	(qt_lote_encerramento_w = 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 10;
	consistencia_w(i).ds_consistencia	:= 'N�o existe lote de encerramento de exerc�cio.';
	consistencia_w(i).ds_acao		:= 'Na fun��o Contabilidade, no m�s de encerramento deve ser executada a funcionalidade Encerramento Exerc�cio.';
	consistencia_w(i).qt_tempo		:= 10;
end if;

if	(qt_mes_aberto_w > 0)then
	i					:= i + 1;
	consistencia_w(i).cd_consistencia	:= 11;
	consistencia_w(i).ds_consistencia	:= 'Quantidade de meses abertos na Contabilidade: ' || Trim(TO_CHAR(qt_mes_aberto_w, '999G9999G999'));
	consistencia_w(i).ds_acao		:= 'Fechar o m�s na fun��o Contabilidade.';
	consistencia_w(i).qt_tempo		:= 10;
end if;



for i in 1.. consistencia_w.count  loop   
   
insert into w_ctb_sped_consistencia (  
	nr_sequencia,         
	dt_atualizacao,
	nm_usuario,          
	cd_consistencia,    
	ds_consistencia,      
	nr_seq_controle_sped,
	ie_tipo,                      
	ds_acao,
	qt_tempo)
values(	w_ctb_sped_consistencia_seq.nextval,
	sysdate,
	nm_usuario_p,
	consistencia_w(i).cd_consistencia,
	consistencia_w(i).ds_consistencia,
	null,   
	consistencia_w(i).ie_tipo,
	consistencia_w(i).ds_acao,
	consistencia_w(i).qt_tempo);  
end loop;

commit;



end fis_consistir_dados_ecf;
/
