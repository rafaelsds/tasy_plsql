Create or replace
procedure Gerar_interf_banrisul	(nr_seq_envio_p		number,
				nm_usuario_p		varchar2) is


tp_registro_w		number(35);
cd_banco_w		varchar2(255);
cd_seg_registro_w	varchar2(255);
cd_instrucao_w		varchar2(255);
cd_camara_w		varchar2(255);
cd_banco_favorecido_w	varchar2(255);
cd_ocor_retorno_w	number(35);
cd_convenio_w		varchar2(255);
cd_agencia_w		varchar2(255);
cd_remessa_retorno_w	varchar2(255);
cd_barras_w		varchar2(255);
cd_instr_alteracao_w	varchar2(255);
cd_pag_inss_w		varchar2(255);
cd_banco_dest_w		varchar2(255);
cd_moeda_w		varchar2(255);
cd_livre_barras_w	varchar2(255);
ds_zeros_w		varchar2(255); 
ds_densidade_w		varchar2(255);
ds_endereco_w		varchar2(255);
ds_complemento_w	varchar2(255);
ds_class_registro_w	varchar2(255);
ds_maquina_w		varchar2(255);
ds_fator_venc_w		varchar2(255);
dt_credito_w		date;
dt_efetivacao_credito_w	date;
dt_geracao_w		date;
dt_hr_geracao_w		varchar2(255);
dt_pagamento_w		date;
dt_competencia_pag_w	date;
dt_vencimento_w		date;
ie_tipo_registro_w	varchar2(255);
ie_tipo_movimento_w	varchar2(255);
ie_tipo_moeda_w		varchar2(255);
ie_emite_aviso_w	varchar2(255);
ie_tipo_operacao_w	varchar2(255);
ie_tipo_servico_w	varchar2(255);
ie_forma_lancamento_w	varchar2(255);
ie_tipo_inscricao_w	varchar2(255);
ie_pagamento_w		varchar2(255);
ie_dig_verificador_w	varchar2(255);
nm_favorecido_w		varchar2(255);
nm_empresa_w		varchar2(255);
nm_banco_w		varchar2(255);
nm_cidade_w		varchar2(255);
nm_cedente_w		varchar2(255);
nm_contribuinte_w	varchar2(255);
nr_seq_registro_w	number(35);
nr_seu_numero_w		number(35);
nr_nosso_numero_w	number(35);
nr_inscricao_w		number(35);
nr_inscr_empresa_w	number(35);
nr_conta_corrente_w	varchar2(35);
nr_conta_w		varchar2(35);
nr_seq_envio_w		number(35);
nr_versao_w		number(35);
nr_local_w		number(35);
nr_cep_w		number(35);
nr_seq_lote_w		number(35);
nr_maquina_w		number(35);
qt_moeda_w		number(35);
qt_registros_w		number(35);
sg_uf_w			varchar2(255);
vl_creditar_w		number(20,2);
vl_credito_efet_w	number(20,2);
vl_pagamento_w		number(20,2);
vl_principal_w		number(20,2);
vl_atual_monetaria_w	number(20,2);
vl_multa_w		number(20,2);
vl_juros_w		number(20,2);
vl_cod_barras_w		number(20,2);
vl_nom_titulo_w		number(20,2);
vl_desc_abatimento_w	number(20,2);
vl_mora_multa_w		number(20,2);
vl_soma_valores_w	number(20,2);
vl_soma_qt_moedas_w	number(20,2);
nr_documento_w		number(10);
vl_tot_registro_w	number(20);
qt_lotes_w		number(35)	:= 0;
nr_seq_apres_w		number(10)	:= 0;
qt_tot_reg_w		number(10)	:= 0;
nr_lote_w		number(10)	:= 1;
nr_registro_w		number(10)	:= 1;


cursor c01 is
select	tp_registro,
	cd_banco,
	cd_seg_registro,
	cd_instrucao,
	cd_camara,
	cd_banco_favorecido,
	cd_ocor_retorno,
	cd_convenio,
	cd_agencia,
	cd_remessa_retorno,
	cd_barras,
	cd_instr_alteracao,
	cd_pag_inss,
	cd_banco_dest,
	cd_moeda,
	cd_livre_barras,
	ds_zeros,
	ds_densidade,
	upper(Elimina_Acentuacao(ds_endereco)) ds_endereco,
	upper(Elimina_Acentuacao(ds_complemento)) ds_complemento,
	ds_class_registro,
	ds_maquina,
	ds_fator_venc,
	dt_credito,
	dt_efetivacao_credito,
	dt_geracao,
	dt_hr_geracao,
	dt_pagamento,
	dt_competencia_pag,
	dt_vencimento,
	ie_tipo_registro,
	ie_tipo_movimento,
	ie_tipo_moeda,
	ie_emite_aviso,
	ie_tipo_operacao,
	ie_tipo_servico,
	ie_forma_lancamento,
	ie_tipo_inscricao,
	ie_pagamento,
	ie_dig_verificador,
	upper(Elimina_Acentuacao(nm_favorecido)) nm_favorecido,
	upper(Elimina_Acentuacao(nm_empresa)) nm_empresa,
	upper(Elimina_Acentuacao(nm_banco)) nm_banco,
	upper(Elimina_Acentuacao(nm_cidade)) nm_cidade,
	upper(Elimina_Acentuacao(nm_cedente)) nm_cedente,
	upper(Elimina_Acentuacao(nm_contribuinte)) nm_contribuinte,
	nr_seq_registro,
	nr_seu_numero,
	nr_nosso_numero,
	nr_inscricao,
	nr_inscr_empresa,
	nr_conta_corrente,
	nr_conta,
	nr_seq_envio,
	nr_versao,
	nr_local,
	nr_cep,
	nr_seq_lote,
	nr_maquina,
	qt_moeda,
	qt_registros,
	upper(Elimina_Acentuacao(sg_uf)) sg_uf,
	vl_creditar,
	vl_credito_efet,
	vl_pagamento,
	vl_principal,
	vl_atual_monetaria,
	vl_multa,
	vl_juros,
	vl_cod_barras,
	vl_nom_titulo,	
	vl_desc_abatimento,
	vl_mora_multa,
	vl_tot_registro,
	vl_soma_valores,
	vl_soma_qt_moedas,
	nr_documento
from 	(
/* Header do Arquivo*/
select	1			tp_registro,
	'041'			cd_banco,
	'' cd_seg_registro,
	'' cd_instrucao,
	'' cd_camara,
	'' cd_banco_favorecido,
	0  cd_ocor_retorno,
	nvl(e.cd_convenio_banco,b.cd_convenio_banco)	cd_convenio,
	b.cd_agencia_bancaria	cd_agencia,
	decode(e.ie_remessa_retorno,'R',1,'I',2,3) cd_remessa_retorno,
	'0'  cd_barras,
	'' cd_instr_alteracao,
	'0' cd_pag_inss,
	'0' cd_banco_dest,
	'' cd_moeda,
	'0' cd_livre_barras,
	'000'	ds_zeros,
	' '	ds_densidade,
	'' ds_endereco,
	'' ds_complemento,
	' ' ds_class_registro,
	'0' ds_maquina,
	'0' ds_fator_venc,
	sysdate dt_credito,
	sysdate dt_efetivacao_credito,
	sysdate dt_geracao,
	to_char(sysdate,'hh24miss') dt_hr_geracao,
	'' dt_pagamento,
	sysdate dt_competencia_pag,
	'' dt_vencimento,
	'0'		ie_tipo_registro,
	'' ie_tipo_movimento,
	'' ie_tipo_moeda,
	'' ie_emite_aviso,
	'' ie_tipo_operacao,
	'' ie_tipo_servico,
	'0' ie_forma_lancamento,
	'2'		ie_tipo_inscricao,
	'0' ie_pagamento,
	'0'  ie_dig_verificador,
	'' nm_favorecido,
	upper(p.ds_razao_social) nm_empresa,
	upper(b.ds_banco) nm_banco,
	'' nm_cidade,
	'0' nm_cedente,
	'0' nm_contribuinte,
	'0' nr_seq_registro,
	'0' nr_seu_numero,
	'0' nr_nosso_numero,
	a.cd_cgc nr_inscricao,
	a.cd_cgc		nr_inscr_empresa,
	b.cd_conta		nr_conta_corrente,
	b.cd_conta || b.ie_digito_conta nr_conta,
	e.nr_sequencia		nr_seq_envio,
	'020'			nr_versao,
	'' nr_local,
	'' nr_cep,
	'0' nr_seq_lote,
	'0' nr_maquina,
	'0' qt_moeda,
	0  qt_registros,
	'' sg_uf,
	0 vl_creditar,
	0 vl_credito_efet,
	0 vl_pagamento,
	0 vl_principal,
	0 vl_atual_monetaria,
	0 vl_multa,
	0 vl_juros,
	'0' vl_cod_barras,
	0 vl_nom_titulo,
	0 vl_desc_abatimento,
	0 vl_mora_multa,
	0 vl_tot_registro,
	0 vl_soma_valores,
	0 vl_soma_qt_moedas,
	0 nr_documento
from	banco_estabelecimento_v b,
	estabelecimento a,
	pessoa_juridica p,
	banco_escritural e
where	e.cd_estabelecimento    = a.cd_estabelecimento
and	e.cd_estabelecimento    = b.cd_estabelecimento
and	a.cd_cgc                = p.cd_cgc
and	e.cd_banco              = b.cd_banco
AND	e.nr_seq_conta_banco	= b.nr_sequencia
and	b.ie_tipo_relacao       in ('EP','ECC')
UNION	
/*Header Lote*/	
select	distinct 2 				tp_registro,
	'041'				cd_banco,
	'' cd_seg_registro,
	'' cd_instrucao,
	'' cd_camara,
	'' cd_banco_favorecido,
	0  cd_ocor_retorno,
	nvl(lpad(a.cd_convenio_banco,5,'0'),lpad(b.cd_convenio_banco,5,'0'))	cd_convenio,
	lpad(b.cd_agencia_bancaria,5,'0')	cd_agencia,
	0  cd_remessa_retorno,
	'0' cd_barras,
	'' cd_instr_alteracao,
	'0' cd_pag_inss,
	'' cd_banco_dest,
	'' cd_moeda,
	'0' cd_livre_barras,
	''  ds_zeros,
	' ' ds_densidade,
	upper(substr(obter_dados_pf_pj(null,e.cd_cgc,'R'),1,255)) ds_endereco,
	upper(j.ds_complemento)		ds_complemento,
	' '				ds_class_registro,
	'0' ds_maquina,
	'0' ds_fator_venc,
	sysdate dt_credito,
	sysdate dt_efetivacao_credito,
	sysdate dt_geracao,
	''  dt_hr_geracao,
	''  dt_pagamento,
	sysdate dt_competencia_pag,
	'' dt_vencimento,
	'1'				ie_tipo_registro,
	'' ie_tipo_movimento,
	'' ie_tipo_moeda,
	'' ie_emite_aviso,
	'C'				ie_tipo_operacao,
	substr(c.ie_tipo_servico,4,2)	ie_tipo_servico,
	decode(c.ie_tipo_pagamento,'CC','01','CCP','01','TED','03','OP','10','PC','33','BLQ',decode(c.cd_banco,041,'30','31'),'DOC','03') ie_forma_lancamento,
	'2'				ie_tipo_inscricao,
	'0' ie_pagamento,
	'0' ie_dig_verificador,
	'' nm_favorecido,
	upper(j.ds_razao_social)	nm_empresa,
	'' nm_banco,
	upper(substr(obter_dados_pf_pj(null,e.cd_cgc,'CI'),1,255)) nm_cidade,
	'0' nm_cedente,
	'0' nm_contribuinte,
	'0' nr_seq_registro,
	'0' nr_seu_numero,
	'0' nr_nosso_numero,
	e.cd_cgc nr_inscricao,
	e.cd_cgc			nr_inscr_empresa,
	b.cd_conta			nr_conta_corrente,
	b.cd_conta || b.ie_digito_conta nr_conta,
	a.nr_sequencia nr_seq_envio,
	'020'				nr_versao,
	upper(substr(obter_dados_pf_pj(null,e.cd_cgc,'NR'),1,255)) nr_local,
	substr(obter_dados_pf_pj(null,e.cd_cgc,'CEP'),1,8) nr_cep,
	'0' nr_seq_lote,
	'0' nr_maquina,
	'0' qt_moeda,
	0 qt_registros,
	upper(substr(obter_dados_pf_pj(null,e.cd_cgc,'UF'),1,2)) sg_uf,
	0 vl_creditar,
	0 vl_credito_efet,
	0 vl_pagamento,
	0 vl_principal,
	0 vl_atual_monetaria,
	0 vl_multa,
	0 vl_juros,
	'0' vl_cod_barras,
	0 vl_nom_titulo,
	0 vl_desc_abatimento,
	0 vl_mora_multa,
	0 vl_tot_registro,
	0 vl_soma_valores,
	0 vl_soma_qt_moedas,
	0 nr_documento
from	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar_escrit c
where	e.cd_cgc		= j.cd_cgc
and	a.nr_sequencia          = c.nr_seq_escrit
and	a.nr_seq_conta_banco    = b.nr_sequencia
and	a.cd_estabelecimento    = b.cd_estabelecimento
and	a.cd_estabelecimento    = e.cd_estabelecimento
AND	a.nr_seq_conta_banco	= b.nr_sequencia
and	b.ie_tipo_relacao       in ('EP','ECC')
UNION
/*Detalhe - Seg. A*/ 
select	3				tp_registro,
	to_char(c.cd_banco)		cd_banco,
	'A' 				cd_seg_registro,
	'00' 				cd_instrucao,
	decode(c.ie_tipo_pagamento,'TED','018','010') cd_camara,
	'' cd_banco_favorecido,
	c.cd_ocorrencia_ret		cd_ocor_retorno,
	'0' cd_convenio,
	c.cd_agencia_bancaria cd_agencia,
	0  cd_remessa_retorno,
	'0'  cd_barras,
	'' cd_instr_alteracao,
	'0' cd_pag_inss,
	'041' 				cd_banco_dest,
	'9' 				cd_moeda,
	'0' 				cd_livre_barras,
	''  ds_zeros,
	' ' ds_densidade,
	''  ds_endereco,
	''  ds_complemento,
	' ' ds_class_registro,
	'0' ds_maquina,
	'0' 				ds_fator_venc,
	sysdate dt_credito,
	to_date(to_char(sysdate,'ddmmyyyy'),'ddmmyyyy') dt_efetivacao_credito,
	sysdate dt_geracao,
	''  dt_hr_geracao,
	to_char(sysdate,'ddmmyyyy') 	dt_pagamento,
	sysdate dt_competencia_pag,
	to_char(d.dt_vencimento_atual,'ddmmyyyy') dt_vencimento,
	'3'				ie_tipo_registro,
	'0' 				ie_tipo_movimento,
	'' ie_tipo_moeda,
	'' ie_emite_aviso,
	'' ie_tipo_operacao,
	'' ie_tipo_servico,
	decode(c.ie_tipo_pagamento,'CC','01','CCP','01','TED','03','OP','10','PC','33','BLQ',decode(c.cd_banco,041,'30','31'),'DOC','03') ie_forma_lancamento,
	'2' 				ie_tipo_inscricao,
	'0' ie_pagamento,
	'0' ie_dig_verificador,
	'' nm_favorecido,
	'' nm_empresa,
	'' nm_banco,
	'' nm_cidade,
	upper(substr(obter_pessoa_titulo_pagar(c.nr_titulo),1,255)) nm_cedente,
	'0' nm_contribuinte,
	'00001' 			nr_seq_registro,
	'0' nr_seu_numero,
	'0' nr_nosso_numero,
	e.cd_cgc			nr_inscricao,
	'' nr_inscr_empresa,
	decode(c.ie_tipo_pagamento,'CC','000' ||b.cd_conta,'CCP','000' ||b.cd_conta,'TED',b.cd_conta||b.ie_digito_conta,'OP','0000000000000') nr_conta_corrente,
	c.nr_conta nr_conta,
	a.nr_sequencia nr_seq_envio,
	'' nr_versao,
	'' nr_local,
	'' nr_cep,
	'0' nr_seq_lote,
	'0' nr_maquina,
	'0' 				qt_moeda,
	0  qt_registros,
	'' sg_uf,
	0 vl_creditar,
	0 vl_credito_efet,
	d.vl_saldo_titulo		vl_pagamento,
	0 vl_principal,
	0 vl_atual_monetaria,
	0 vl_multa,
	0 vl_juros,
	'0' vl_cod_barras,
	d.vl_titulo			vl_nom_titulo,
	0 				vl_desc_abatimento,
	(nvl(vl_saldo_juros,0) + nvl(vl_saldo_multa,0)) vl_mora_multa,
	0 vl_tot_registro,
	0 vl_soma_valores,
	0 vl_soma_qt_moedas,
	c.nr_titulo nr_documento
from	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar_v2 d,
	titulo_pagar_escrit c
where	c.nr_titulo             = d.nr_titulo
and	e.cd_cgc		= j.cd_cgc
and	a.nr_sequencia          = c.nr_seq_escrit
and	a.nr_seq_conta_banco    = b.nr_sequencia
and	a.cd_estabelecimento    = b.cd_estabelecimento
and	a.cd_estabelecimento    = e.cd_estabelecimento
and	b.ie_tipo_relacao       in ('EP','ECC')
and 	c.ie_tipo_pagamento	in ('DOC','TED','CC','CCP','OP','PC')
UNION
/*Detalhe - Seg. J*/
select	6				tp_registro,
	'041'				cd_banco,
	'J' 				cd_seg_registro,
	'00' 				cd_instrucao,
	'' cd_camara,
	'' cd_banco_favorecido,
	c.cd_ocorrencia_ret		cd_ocor_retorno,
	'0' cd_convenio,
	b.cd_agencia_bancaria cd_agencia,
	0  cd_remessa_retorno,
	d.nr_bloqueto  cd_barras,
	'' cd_instr_alteracao,
	'0' cd_pag_inss,
	substr(nr_bloqueto,1,3)		cd_banco_dest,
	substr(nr_bloqueto,4,1)		cd_moeda,
	substr(nr_bloqueto,20,25)	cd_livre_barras,
	''  ds_zeros,
	' ' ds_densidade,
	''  ds_endereco,
	''  ds_complemento,
	' ' ds_class_registro,
	'0' ds_maquina,
	substr(nr_bloqueto,6,4)		ds_fator_venc,
	sysdate dt_credito,
	sysdate dt_efetivacao_credito,
	sysdate dt_geracao,
	''  dt_hr_geracao,
	to_char(sysdate,'ddmmyyyy') 	dt_pagamento,
	sysdate dt_competencia_pag,
	to_char(d.dt_vencimento_atual,'ddmmyyyy') dt_vencimento,
	'3'				ie_tipo_registro,
	'0' 				ie_tipo_movimento,
	'' ie_tipo_moeda,
	'' ie_emite_aviso,
	'' ie_tipo_operacao,
	'' ie_tipo_servico,
	decode(c.ie_tipo_pagamento,'CC','01','CCP','01','TED','03','OP','10','PC','33','BLQ',decode(c.cd_banco,041,'30','31'),'DOC','03') ie_forma_lancamento,
	'2' 				ie_tipo_inscricao,
	'0' ie_pagamento,
	substr(nr_bloqueto,5,1)		ie_dig_verificador,
	'' nm_favorecido,
	'' nm_empresa,
	'' nm_banco,
	'' nm_cidade,
	substr(obter_pessoa_titulo_pagar(c.nr_titulo),1,255) nm_cedente,
	'0' nm_contribuinte,
	'00001' 			nr_seq_registro,
	'0' nr_seu_numero,
	'0' nr_nosso_numero,
	e.cd_cgc			nr_inscricao,
	'' nr_inscr_empresa,
	'' nr_conta_corrente,
	'' nr_conta,
	a.nr_sequencia nr_seq_envio,
	'' nr_versao,
	'' nr_local,
	'' nr_cep,
	'0' nr_seq_lote,
	'0' nr_maquina,
	'0' 				qt_moeda,
	0  qt_registros,
	'' sg_uf,
	0 vl_creditar,
	0 vl_credito_efet,
	 (c.vl_escritural + c.vl_acrescimo - c.vl_desconto + nvl(c.vl_juros,0) + nvl(c.vl_multa,0)) vl_pagamento,
	0 vl_principal,
	0 vl_atual_monetaria,
	0 vl_multa,
	0 vl_juros,
	substr(nr_bloqueto,10,10)		vl_cod_barras,
	d.vl_titulo			vl_nom_titulo,
	c.vl_desconto 				vl_desc_abatimento,
	(nvl(vl_saldo_juros,0) + nvl(vl_saldo_multa,0)) vl_mora_multa,
	0 vl_tot_registro,
	0 vl_soma_valores,
	0 vl_soma_qt_moedas,
	c.nr_titulo nr_documento
from	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar_v2 d,
	titulo_pagar_escrit c
where	c.nr_titulo             = d.nr_titulo
and	e.cd_cgc		= j.cd_cgc
and	a.nr_sequencia          = c.nr_seq_escrit
and	a.nr_seq_conta_banco    = b.nr_sequencia
and	a.cd_estabelecimento    = b.cd_estabelecimento
and	a.cd_estabelecimento    = e.cd_estabelecimento
and	b.ie_tipo_relacao       in ('EP','ECC')
and 	c.ie_tipo_pagamento	= 'BLQ'
and 	c.cd_banco 		= 041
UNION
/*Detalhe - Seg. J - Outros Bancos*/
select	6				tp_registro,
	'041'				cd_banco,
	'J' 				cd_seg_registro,
	'00' 				cd_instrucao,
	'' cd_camara,
	'' cd_banco_favorecido,
	c.cd_ocorrencia_ret		cd_ocor_retorno,
	'0' cd_convenio,
	b.cd_agencia_bancaria cd_agencia,
	0  cd_remessa_retorno,
	nr_bloqueto  cd_barras,
	'' cd_instr_alteracao,
	'0' cd_pag_inss,
	substr(nr_bloqueto,1,3)		cd_banco_dest,
	substr(nr_bloqueto,4,1)		cd_moeda,
	substr(nr_bloqueto,20,25)	cd_livre_barras,
	''  ds_zeros,
	' ' ds_densidade,
	''  ds_endereco,
	''  ds_complemento,
	' ' ds_class_registro,
	'0' ds_maquina,
	substr(nr_bloqueto,6,4)		ds_fator_venc,
	sysdate dt_credito,
	sysdate dt_efetivacao_credito,
	sysdate dt_geracao,
	''  dt_hr_geracao,
	to_char(sysdate,'ddmmyyyy') 	dt_pagamento,
	sysdate dt_competencia_pag,
	to_char(d.dt_vencimento_atual,'ddmmyyyy') dt_vencimento,
	'3'				ie_tipo_registro,
	'0' 				ie_tipo_movimento,
	'' ie_tipo_moeda,
	'' ie_emite_aviso,
	'' ie_tipo_operacao,
	'' ie_tipo_servico,
	'31' ie_forma_lancamento,
	'2' ie_tipo_inscricao,
	'0' ie_pagamento,
	substr(nr_bloqueto,5,1)		ie_dig_verificador,
	'' nm_favorecido,
	'' nm_empresa,
	'' nm_banco,
	'' nm_cidade,
	upper(Elimina_Acentuacao(substr(obter_pessoa_titulo_pagar(c.nr_titulo),1,255))) nm_cedente,
	'0' nm_contribuinte,
	'00001' 			nr_seq_registro,
	'0' nr_seu_numero,
	'0' nr_nosso_numero,
	e.cd_cgc			nr_inscricao,
	'' nr_inscr_empresa,
	'' nr_conta_corrente,
	'' nr_conta,
	a.nr_sequencia nr_seq_envio,
	'' nr_versao,
	'' nr_local,
	'' nr_cep,
	'0' nr_seq_lote,
	'0' nr_maquina,
	'0' 				qt_moeda,
	0  qt_registros,
	'' sg_uf,
	0 vl_creditar,
	0 vl_credito_efet,
	(c.vl_escritural + c.vl_acrescimo - c.vl_desconto + nvl(c.vl_juros,0) + nvl(c.vl_multa,0)) vl_pagamento,
	0 vl_principal,
	0 vl_atual_monetaria,
	0 vl_multa,
	0 vl_juros,
	substr(nr_bloqueto,10,10)		vl_cod_barras,
	d.vl_titulo			vl_nom_titulo,
	c.vl_desconto				vl_desc_abatimento,
	(nvl(vl_saldo_juros,0) + nvl(vl_saldo_multa,0)) vl_mora_multa,
	0 vl_tot_registro,
	0 vl_soma_valores,
	0 vl_soma_qt_moedas,
	c.nr_titulo nr_documento
from	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar_v2 d,
	titulo_pagar_escrit c
where	c.nr_titulo             = d.nr_titulo
and	e.cd_cgc		= j.cd_cgc
and	a.nr_sequencia          = c.nr_seq_escrit
and	a.nr_seq_conta_banco    = b.nr_sequencia
and	a.cd_estabelecimento    = b.cd_estabelecimento
and	a.cd_estabelecimento    = e.cd_estabelecimento
and	b.ie_tipo_relacao       in ('EP','ECC')
and 	c.ie_tipo_pagamento	= 'BLQ'
and	c.cd_banco 		<> 041
and	c.cd_banco 		is not null
UNION
/*Trailler Lote*/
select	7				tp_registro,
	'041'				cd_banco,
	'' cd_seg_registro,
	'' cd_instrucao,
	'' cd_camara,
	'' cd_banco_favorecido,
	c.cd_ocorrencia_ret		cd_ocor_retorno,
	'0' cd_convenio,
	b.cd_agencia_bancaria cd_agencia,
	0  cd_remessa_retorno,
	'0'  cd_barras,
	'' cd_instr_alteracao,
	'0' cd_pag_inss,
	'0' cd_banco_dest,
	'' cd_moeda,
	'0' cd_livre_barras,
	''  ds_zeros,
	' ' ds_densidade,
	''  ds_endereco,
	''  ds_complemento,
	' ' ds_class_registro,
	'0' ds_maquina,
	'0' ds_fator_venc,
	sysdate dt_credito,
	sysdate dt_efetivacao_credito,
	sysdate dt_geracao,
	''  dt_hr_geracao,
	''  dt_pagamento,
	sysdate dt_competencia_pag,
	'' dt_vencimento,
	'5'				ie_tipo_registro,
	'' ie_tipo_movimento,
	'' ie_tipo_moeda,
	'' ie_emite_aviso,
	'' ie_tipo_operacao,
	'' ie_tipo_servico,
	decode(c.ie_tipo_pagamento,'CC','01','CCP','01','TED','03','OP','10','PC','33','BLQ',decode(c.cd_banco,041,'30','31'),'DOC','03') ie_forma_lancamento,
	'2' ie_tipo_inscricao,
	'0' ie_pagamento,
	'0' ie_dig_verificador,
	'' nm_favorecido,
	'' nm_empresa,
	'' nm_banco,
	'' nm_cidade,
	'0' nm_cedente,
	'0' nm_contribuinte,
	'0' nr_seq_registro,
	'0' nr_seu_numero,
	'0' nr_nosso_numero,
	e.cd_cgc nr_inscricao,
	'' nr_inscr_empresa,
	'' nr_conta_corrente,
	'' nr_conta,
	a.nr_sequencia nr_seq_envio,
	'' nr_versao,
	'' nr_local,
	'' nr_cep,
	'0' nr_seq_lote,
	'0' nr_maquina,
	'0' qt_moeda,
	0  qt_registros,
	'' sg_uf,
	0 vl_creditar,
	0 vl_credito_efet,
	0 vl_pagamento,
	0 vl_principal,
	0 vl_atual_monetaria,
	0 vl_multa,
	0 vl_juros,
	'0' vl_cod_barras,
	0 vl_nom_titulo,
	0 vl_desc_abatimento,
	0 vl_mora_multa,
	count(d.nr_titulo) + 2		vl_tot_registro,
	(nvl(sum(c.vl_escritural),0) + nvl(sum(c.vl_acrescimo),0) - nvl(sum(c.vl_desconto),0) + nvl(sum(c.vl_juros),0) + nvl(sum(c.vl_multa),0)) 		vl_soma_valores,
	0 				vl_soma_qt_moedas,
	0 nr_documento
from	pessoa_juridica j,
	Estabelecimento e,
	banco_estabelecimento_v b,
	banco_escritural a,
	titulo_pagar_v2 d,
	titulo_pagar_escrit c
where	c.nr_titulo             = d.nr_titulo
and	e.cd_cgc		= j.cd_cgc
and	a.nr_sequencia          = c.nr_seq_escrit
and	a.nr_seq_conta_banco    = b.nr_sequencia
and	a.cd_estabelecimento    = b.cd_estabelecimento
and	a.cd_estabelecimento    = e.cd_estabelecimento
and	b.ie_tipo_relacao       in ('EP','ECC')
and	c.cd_banco is not null
group by a.nr_sequencia, 
	c.cd_ocorrencia_ret, 
	b.cd_agencia_bancaria, 
	e.cd_cgc, 
	decode(c.ie_tipo_pagamento,'CC','01','CCP','01','TED','03','OP','10','PC','33','BLQ',decode(c.cd_banco,041,'30','31'),'DOC','03')
UNION
/*Trailler*/
select	9				tp_registro,
	'041' 				cd_banco,
	'' cd_seg_registro,
	'' cd_instrucao,
	'' cd_camara,
	'' cd_banco_favorecido,
	0  cd_ocor_retorno,
	'0' cd_convenio,
	b.cd_agencia_bancaria cd_agencia,
	0  cd_remessa_retorno,
	'0'  cd_barras,
	'' cd_instr_alteracao,
	'0' cd_pag_inss,
	'0' cd_banco_dest,
	'' cd_moeda,
	'0' cd_livre_barras,
	'000000'			ds_zeros,
	' ' ds_densidade,
	''  ds_endereco,
	''  ds_complemento,
	' ' ds_class_registro,
	'0' ds_maquina,
	'0' ds_fator_venc,
	sysdate dt_credito,
	sysdate dt_efetivacao_credito,
	sysdate dt_geracao,
	''  dt_hr_geracao,
	''  dt_pagamento,
	sysdate dt_competencia_pag,
	'' dt_vencimento,
	'9' ie_tipo_registro,
	'' ie_tipo_movimento,
	'' ie_tipo_moeda,
	'' ie_emite_aviso,
	'' ie_tipo_operacao,
	'' ie_tipo_servico,
	'999' ie_forma_lancamento,
	'2' ie_tipo_inscricao,
	'0' ie_pagamento,
	'0' ie_dig_verificador,
	'' nm_favorecido,
	'' nm_empresa,
	'' nm_banco,
	'' nm_cidade,
	'0' nm_cedente,
	'0' nm_contribuinte,
	'0' nr_seq_registro,
	'0' nr_seu_numero,
	'0' nr_nosso_numero,
	a.cd_cgc nr_inscricao,
	'' nr_inscr_empresa,
	'' nr_conta_corrente,
	'' nr_conta,
	e.nr_sequencia nr_seq_envio,
	'' nr_versao,
	'' nr_local,
	'' nr_cep,
	'0' nr_seq_lote,
	'0' nr_maquina,
	'0' qt_moeda,
	count(c.nr_titulo) + 4	qt_registros,
	'' sg_uf,
	0 vl_creditar,
	0 vl_credito_efet,
	0 vl_pagamento,
	0 vl_principal,
	0 vl_atual_monetaria,
	0 vl_multa,
	0 vl_juros,
	'0' vl_cod_barras,
	0 vl_nom_titulo,
	0 vl_desc_abatimento,
	0 vl_mora_multa,
	0 vl_tot_registro,
	0 vl_soma_valores,
	0 vl_soma_qt_moedas,
	0 nr_documento
from	banco_estabelecimento_v b,
	estabelecimento a,
	banco_escritural e,
	titulo_pagar_escrit c
where	e.cd_estabelecimento    = a.cd_estabelecimento
and	b.ie_tipo_relacao       in ('EP','ECC')
and	b.nr_sequencia		= e.nr_seq_conta_banco
and	e.nr_sequencia     	= c.nr_seq_escrit
and	c.cd_banco is not null
group by e.nr_sequencia, 
	b.cd_agencia_bancaria, 
	a.cd_cgc
)
where nr_seq_envio = nr_seq_envio_p
order by ie_forma_lancamento, 
	tp_registro, 
	ie_tipo_registro;

begin

delete	from w_interf_itau;
commit;

open c01;
loop
fetch c01 into
	tp_registro_w,		
	cd_banco_w,	
	cd_seg_registro_w,	 
	cd_instrucao_w,		 
	cd_camara_w,		 
	cd_banco_favorecido_w,	 
	cd_ocor_retorno_w,	 
	cd_convenio_w,		 
	cd_agencia_w,		 
	cd_remessa_retorno_w,	 
	cd_barras_w,		 
	cd_instr_alteracao_w,	 
	cd_pag_inss_w,		 
	cd_banco_dest_w,		 
	cd_moeda_w,		 
	cd_livre_barras_w,	 
	ds_zeros_w,		 
	ds_densidade_w,		 
	ds_endereco_w,		 
	ds_complemento_w,	 
	ds_class_registro_w,	 
	ds_maquina_w,		 
	ds_fator_venc_w,		 
	dt_credito_w,		  
	dt_efetivacao_credito_w,	  
	dt_geracao_w,		  
	dt_hr_geracao_w,		  
	dt_pagamento_w,		  
	dt_competencia_pag_w,	  
	dt_vencimento_w,		  
	ie_tipo_registro_w,	 
	ie_tipo_movimento_w,	 
	ie_tipo_moeda_w,		 
	ie_emite_aviso_w,
	ie_tipo_operacao_w,
	ie_tipo_servico_w, 
	ie_forma_lancamento_w,
	ie_tipo_inscricao_w,
	ie_pagamento_w, 
	ie_dig_verificador_w,
	nm_favorecido_w,		 
	nm_empresa_w,		 
	nm_banco_w,		 
	nm_cidade_w,		 
	nm_cedente_w,		 
	nm_contribuinte_w,	 
	nr_seq_registro_w,	 
	nr_seu_numero_w,	 
	nr_nosso_numero_w,	 
	nr_inscricao_w,		 
	nr_inscr_empresa_w,	 
	nr_conta_corrente_w,	 
	nr_conta_w,		 
	nr_seq_envio_w,		 
	nr_versao_w,		 
	nr_local_w,		 
	nr_cep_w,		 
	nr_seq_lote_w,		 
	nr_maquina_w,		 
	qt_moeda_w,		 
	qt_registros_w,		 
	sg_uf_w,			 
	vl_creditar_w,		 
	vl_credito_efet_w,	 
	vl_pagamento_w,		 
	vl_principal_w,		 
	vl_atual_monetaria_w,	 
	vl_multa_w,		 
	vl_juros_w,		 
	vl_cod_barras_w,	 
	vl_nom_titulo_w,	 
	vl_desc_abatimento_w,	 
	vl_mora_multa_w,		 
	vl_tot_registro_w,	 
	vl_soma_valores_w,	 
	vl_soma_qt_moedas_w,
	nr_documento_w;		 
exit when c01%notfound;


	qt_tot_reg_w := qt_tot_reg_w + 1;
	nr_seq_apres_w	:= nr_seq_apres_w + 1;
	
insert	into	w_interf_itau
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_apres,
		nr_seq_envio,
		ie_forma_pagto,
		ie_tipo_registro, 
		nr_inscricao,
		cd_agencia_bancaria,
		cd_conta,
		nm_empresa,
		ds_banco,
		dt_geracao,
		ds_endereco,
		ds_cidade,
		cd_cep,
		sg_estado,
		cd_banco_fornec,
		cd_camara_compensacao,
		nm_fornecedor,
		nr_titulo,
		dt_pagto,
		vl_pagto,
		qt_registros,
		cd_barras,
		dt_vencimento,
		vl_titulo,
		vl_desconto,
		vl_acrescimo,
		qt_tot_reg,
		vl_juros,
		ie_tipo_servico,
		ds_complemento,
		ie_forma_lancamento,
		nr_lote,
		nr_registro,
		nr_endereco,
		qt_lote_arquivo,
		vl_total_pagto,
		cd_convenio_banco,
		qt_moeda)

	values	(w_interf_itau_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_apres_w,
		nr_seq_envio_w,
		ie_forma_lancamento_w,
		tp_registro_w, 
		nr_inscricao_w,
		cd_agencia_w,
		nr_conta_w,
		nm_empresa_w,
		nm_banco_w,
		dt_geracao_w,
		ds_endereco_w,
		nm_cidade_w,
		nr_cep_w,
		sg_uf_w,
		cd_banco_w,
		cd_camara_w,
		nm_cedente_w,
		nr_documento_w,
		dt_pagamento_w,
		vl_pagamento_w,
		qt_registros_w,
		cd_barras_w,
		dt_vencimento_w,
		vl_nom_titulo_w,
		vl_desc_abatimento_w,
		vl_mora_multa_w,
		decode(tp_registro_w,'9',qt_tot_reg_w,0),
		vl_mora_multa_w,
		ie_tipo_servico_w,
		ds_complemento_w,
		ie_forma_lancamento_w,
		nr_lote_w,
		nr_registro_w,
		nr_local_w,
		qt_lotes_w,
		vl_soma_valores_w,
		cd_convenio_w,
		vl_tot_registro_w);



	/* So incrementar o numero do registro quando detalhe */
	if	(tp_registro_w in ('3','6')) then
		nr_registro_w	:= nr_registro_w + 1;
	end if;

	/* Quando passou do Trailler muda o numero do lote */
	if	(tp_registro_w in ('7')) then
		qt_lotes_w	:= qt_lotes_w + 1;
		nr_lote_w	:= nr_lote_w + 1;
		nr_registro_w	:= 1;
	end if;	

end loop;
close c01;

commit;

end Gerar_interf_banrisul;
/