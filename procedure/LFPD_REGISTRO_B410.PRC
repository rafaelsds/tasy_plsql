create or replace
procedure lfpd_registro_B410(	nr_seq_controle_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number,
				dt_inicio_p		date,
				dt_fim_p		date,
				ds_separador_p		varchar2,
				qt_linha_p		in out number,
				nr_sequencia_p		in out number) is

contador_w		number(10)	:= 0;
ds_arquivo_w		varchar2(4000);
ds_arquivo_compl_w	varchar2(4000);
ds_linha_w		varchar2(8000);
nr_linha_w		number(10)	:= qt_linha_p;
nr_seq_registro_w	number(10)	:= nr_sequencia_p;
sep_w			varchar2(1)	:= ds_separador_p;

cd_cgc_w		varchar2(14);
cd_municipio_w		varchar2(10);
nr_seq_pais_w		varchar2(15);

vl_resumo_w		number(15,2);

cursor c01 is
	select	'B410'														cd_registro,
		'0'														ie_totalizador, -- subtotal aquisições internas
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_contabil,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_base_calculo,
		'0'														vl_total_deducoes,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_retido,
		'0'														vl_total_isento,
		'0'														vl_total_destacado
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o
	where	i.nr_sequencia = n.nr_sequencia
	and	n.cd_operacao_nf = o.cd_operacao_nf
	and	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'E'
	and 	(o.ie_servico = 'S')
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	i.cd_material is not null
	and	cd_municipio_w = obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'CDMDV')
	union
	select	'B410'														cd_registro,
		'1'														ie_totalizador, -- aquisições outros municípios
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_contabil,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_base_calculo,
		'0'														vl_total_deducoes,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_retido,
		'0'														vl_total_isento,
		'0'														vl_total_destacado
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o
	where	i.nr_sequencia = n.nr_sequencia
	and	n.cd_operacao_nf = o.cd_operacao_nf
	and	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'E'
	and 	(o.ie_servico = 'S')
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	i.cd_material is not null
	and	cd_municipio_w 	!= obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'CDMDV')
	and	((nr_seq_pais_w = obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'P'))
	or	(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'P') is null))
	union
	select	'B410'														cd_registro,
		'2'														ie_totalizador, -- aquisições exterior
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_contabil,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_base_calculo,
		'0'														vl_total_deducoes,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_retido,
		'0'														vl_total_isento,
		'0'														vl_total_destacado
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o
	where	i.nr_sequencia = n.nr_sequencia
	and	n.cd_operacao_nf = o.cd_operacao_nf
	and	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'E'
	and 	(o.ie_servico = 'S')
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	i.cd_material is not null
	and	nr_seq_pais_w <> obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'P')
	and	obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'P') is not null
	union
	select	'B410'														cd_registro,
		'3'														ie_totalizador, -- total aquisições internas
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_contabil,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_base_calculo,
		'0'														vl_total_deducoes,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_retido,
		'0'														vl_total_isento,
		'0'														vl_total_destacado
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o
	where	i.nr_sequencia = n.nr_sequencia
	and	n.cd_operacao_nf = o.cd_operacao_nf
	and	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'E'
	and 	(o.ie_servico = 'S')
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	i.cd_material is not null
	union
	select	'B410'														cd_registro,
		'4'														ie_totalizador, -- prestações internas
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_contabil,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_base_calculo,
		'0'														vl_total_deducoes,
		replace(Campo_Mascara(nvl(sum(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_retido,
		'0'														vl_total_isento,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_destacado
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o
	where	i.nr_sequencia = n.nr_sequencia
	and	n.cd_operacao_nf = o.cd_operacao_nf
	and	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S'
	and 	(o.ie_servico = 'S')
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	i.cd_procedimento is not null
	and	cd_municipio_w = obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'CDMDV')
	union
	select	'B410'														cd_registro,
		'5'														ie_totalizador, -- prestações outros municípios
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')  	vl_total_contabil,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_base_calculo,
		'0'														vl_total_deducoes,
		replace(Campo_Mascara(nvl(sum(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_retido,
		'0'														vl_total_isento,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_destacado
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o
	where	i.nr_sequencia = n.nr_sequencia
	and	n.cd_operacao_nf = o.cd_operacao_nf
	and	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S'
	and 	(o.ie_servico = 'S')
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	i.cd_procedimento is not null
	and	cd_municipio_w 	<> obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'CDMDV')
	and	((nr_seq_pais_w = obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'P'))
	or	(obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'P') is null))
	union
	select	'B410'														cd_registro,
		'6'														ie_totalizador, -- prestações exterior
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_contabil,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_base_calculo,
		'0'														vl_total_deducoes,
		replace(Campo_Mascara(nvl(sum(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_retido,
		'0'														vl_total_isento,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_destacado
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o
	where	i.nr_sequencia = n.nr_sequencia
	and	n.cd_operacao_nf = o.cd_operacao_nf
	and	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S'
	and 	(o.ie_servico = 'S')
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	i.cd_procedimento is not null
	and	nr_seq_pais_w <> obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'P')
	and	obter_dados_pf_pj(n.cd_pessoa_fisica, n.cd_cgc,'P') is not null
	union
	select	'B410'														cd_registro,
		'7'														ie_totalizador, -- total das prestações
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_contabil,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'B', 'ISS')),0),2),'.',',')		vl_total_base_calculo,
		'0'														vl_total_deducoes,
		replace(Campo_Mascara(nvl(sum(Obter_Valor_tipo_Trib_Retido(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_retido,
		'0'														vl_total_isento,
		replace(Campo_Mascara(nvl(sum(obter_valor_tipo_tributo_nota(n.nr_sequencia, 'V', 'ISS')),0),2),'.',',')		vl_total_destacado
	from	nota_fiscal n,
		nota_fiscal_item i,
		operacao_nota o
	where	i.nr_sequencia = n.nr_sequencia
	and	n.cd_operacao_nf = o.cd_operacao_nf
	and	n.dt_emissao between dt_inicio_p and dt_fim_p
	and	n.cd_estabelecimento = cd_estabelecimento_p
	and	n.dt_atualizacao_estoque is not null
	and	nvl(Obter_se_nota_entrada_saida(n.nr_sequencia),'E') = 'S'
	and 	(o.ie_servico = 'S')
	and	n.vl_total_nota > 0
	and	n.ie_situacao = 1
	and	i.cd_procedimento is not null;

vet01	c01%RowType;

begin

select	obter_dados_pf_pj(null, cd_cgc, 'CDMDV'),
	to_char(obter_dados_pf_pj(null, cd_cgc, 'P'))
into	cd_municipio_w,
	nr_seq_pais_w
from	estabelecimento
where	cd_estabelecimento = cd_estabelecimento_p;

open C01;
loop
fetch C01 into
	vet01;
exit when C01%notfound;
	begin

	contador_w := contador_w + 1;

	ds_linha_w := substr(	sep_w	||	vet01.cd_registro			|| sep_w ||
						vet01.ie_totalizador			|| sep_w ||
						vet01.vl_total_contabil			|| sep_w ||
						vet01.vl_total_base_calculo		|| sep_w ||
						vet01.vl_total_deducoes			|| sep_w ||
						vet01.vl_total_retido			|| sep_w ||
						vet01.vl_total_isento			|| sep_w ||
						vet01.vl_total_destacado		|| sep_w, 1, 8000);

	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	insert into fis_lfpd_arquivo	(	nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_linha,
						ds_arquivo,
						ds_arquivo_compl,
						cd_registro,
						nr_seq_controle_lfpd)
				values	(	fis_lfpd_arquivo_seq.NextVal,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_linha_w,
						ds_arquivo_w,
						ds_arquivo_compl_w,
						vet01.cd_registro,
						nr_seq_controle_p);

	if (mod(contador_w,100) = 0) then
		commit;
	end if;

	end;
end loop;
close C01;

commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

end lfpd_registro_B410;
/