create or replace
procedure proj_copiar_equipe_cliente(nr_seq_equipe_p		number,
					ds_lista_projeto_p	varchar2,
					nm_usuario_p		Varchar2) is
					
nr_seq_projeto_w	number(10);
nr_seq_nova_equipe_w	number(10);

Cursor c01 is
select	a.nr_sequencia
from	proj_projeto a
where	' ' || ds_lista_projeto_p || ' ' like '% ' || nr_sequencia || ' %'
and	ds_lista_projeto_p is not null;						

begin

open c01;
loop
fetch c01 into	
	nr_seq_projeto_w;
exit when c01%notfound;
	begin
	
	select	proj_equipe_seq.nextval
	into	nr_seq_nova_equipe_w
	from	dual;
	
	insert into proj_equipe
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_cliente,
		dt_inicio,
		ds_objetivo,
		nr_seq_equipe_funcao,
		ie_interna_externa,
		ie_virada,
		nr_seq_apres,
		nr_seq_proj,
		dt_liberacao,
		nr_seq_virada)
	select	nr_seq_nova_equipe_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_cliente,
		dt_inicio,
		ds_objetivo,
		nr_seq_equipe_funcao,
		ie_interna_externa,
		ie_virada,
		nr_seq_apres,
		nr_seq_projeto_w,
		dt_liberacao,
		nr_seq_virada
	from	proj_equipe a
	where	a.nr_sequencia	= nr_seq_equipe_p;

	insert into proj_equipe_papel
		(nr_sequencia,
		nr_seq_equipe,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_apres,
		nr_seq_funcao,
		nm_pessoa,
		cd_pessoa_fisica,
		ds_setor,
		ds_fone,
		dt_fim,
		nr_seq_cadastro,
		ds_atuacao,
		ds_cargo,
		nm_guerra,
		ds_email,
		ie_alerta,
		IE_ALOCADO_ORIGINAL)
	select	proj_equipe_papel_seq.nextval,
		nr_seq_nova_equipe_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_apres,
		nr_seq_funcao,
		nm_pessoa,
		cd_pessoa_fisica,
		ds_setor,
		ds_fone,
		dt_fim,
		nr_seq_cadastro,
		ds_atuacao,
		ds_cargo,
		nm_guerra,
		ds_email,
		ie_alerta,
		'S'
	from	proj_equipe_papel a
	where	a.nr_seq_equipe	= nr_seq_equipe_p;
		
	end;
end loop;
close c01;

commit;

end proj_copiar_equipe_cliente;
/