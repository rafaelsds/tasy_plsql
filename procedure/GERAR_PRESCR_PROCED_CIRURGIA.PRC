create or replace
procedure gerar_prescr_proced_cirurgia(	nr_prescricao_p		number,
					nr_seq_proc_paciente_p	number,
					nr_seq_agenda_p		number,
					ie_tipo_p		varchar,
					ie_lado_p		varchar,
					nm_usuario_p		Varchar2) IS 

nr_sequencia_w		number(10,0);
dt_prescricao_w		date;
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);	
nr_seq_proc_interno_w	number(10);
qt_procedimento_w	number(8,3) := 1;

begin

if	(ie_tipo_p = 1) then
	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w	
	from	procedimento_paciente
	where	nr_sequencia	= nr_seq_proc_paciente_p;
elsif	(ie_tipo_p = 2)	 then
	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w	
	from	agenda_paciente
	where	nr_sequencia	= nr_seq_proc_paciente_p;
elsif	(ie_tipo_p = 3)	 then	
	select	cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno,
		nvl(qt_procedimento, 1)
	into	cd_procedimento_w,
		ie_origem_proced_w,
		nr_seq_proc_interno_w,
		qt_procedimento_w
	from	agenda_paciente_proc
	where	nr_sequencia	= nr_seq_proc_paciente_p
	and	nr_seq_agenda	= nr_seq_agenda_p;
end if;	

select	dt_prescricao
into 	dt_prescricao_w
from 	prescr_medica
where 	nr_prescricao = nr_prescricao_p;

select	nvl(max(nr_sequencia), 0) + 1
into	nr_sequencia_w
from	prescr_procedimento
where	nr_prescricao	= nr_prescricao_p;

insert into prescr_procedimento(
	nr_prescricao,
	nr_sequencia,
	cd_procedimento,
	ie_origem_proced,
	qt_procedimento,
	ie_urgencia,
	ie_suspenso,
	dt_prev_execucao,
	dt_atualizacao,
	nm_usuario,
	ie_origem_inf,
	nr_seq_interno,
	ie_avisar_result,
	cd_motivo_baixa,
	cd_medico_exec,
	nr_seq_proc_interno,
	ie_descricao_cirurgica,
	ie_lado)
values	(nr_prescricao_p,
	nr_sequencia_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	qt_procedimento_w, 
	'N', 
	'N',
	dt_prescricao_w,
	sysdate, 
	nm_usuario_p,
	'1', 
	prescr_procedimento_seq.NextVal,
	'N',
	0,
	Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C'),
	nr_seq_proc_interno_w,
	'S',
	ie_lado_p);

commit;

end gerar_prescr_proced_cirurgia;
/