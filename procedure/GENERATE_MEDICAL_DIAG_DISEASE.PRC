create or replace 
procedure generate_medical_diag_disease (	
		nr_atendimento_p			number,
		cd_doenca_p			varchar2,
		nr_seq_classificacao_p			number,
		dt_diagnostico_p			date,
		nr_seq_medic_diag_doe_p			number,
		nm_usuario_p			varchar2,
		cd_estabelecimento_p			number,
		ie_diagnostico_admissao_p			out varchar2,
		ds_retorno_abort_p			out varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finality: Insert data
---------------------------------------------------------------------------------------------------
Direct calls: 
[  ]  Object dictionary [ X ] Tasy (Delphi/Java) [  ] Portal [  ]  Reports [ ] Others:
---------------------------------------------------------------------------------------------------
Attention points:
---------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_pessoa_fisica_w			pessoa_fisica.cd_pessoa_fisica%type;

ie_diagnostico_admissao_w			classificacao_diagnostico.ie_diagnostico_admissao%type;
qt_diagnostico_w			classificacao_diagnostico.qt_diagnostico%type;
ie_classif_doenca_w			classificacao_diagnostico.ie_classificacao_doenca%type;

nr_seq_medic_diag_doe_w		medic_diagnostico_doenca.nr_sequencia%type;

qt_medic_diag_w				number(10);

ds_diagnostico_w			varchar2(255);
dt_diagnostico_w			varchar2(255);

ds_macros_w			varchar2(255);
ds_abort_w			varchar2(2000);

nr_seq_episodio_w			episodio_paciente.nr_sequencia%type;

Cursor C01 is
	select	concat(concat(a.cd_doenca, ' - '), substr(obter_desc_cid(a.cd_doenca),1,240)) ds_diagnostico,
		date_as_varchar2(a.dt_diagnostico, 'shortDate') dt_diagnostico
	from	medic_diagnostico_doenca a,
		diagnostico_doenca b
	where   a.cd_doenca	= b.cd_doenca
	and	a.nr_atendimento	= b.nr_atendimento
	and	a.dt_diagnostico	= b.dt_diagnostico
	and	a.nr_atendimento	in (select 	x.nr_atendimento 
								from 	atendimento_paciente x, 
										episodio_paciente y 
								where 	x.nr_seq_episodio = y.nr_sequencia 
								and 	y.nr_sequencia = nr_seq_episodio_w)
	and	a.nr_seq_classificacao	= nr_seq_classificacao_p
	and	a.ie_situacao	= 'A'
	and	b.ie_situacao	= 'A';

begin
cd_pessoa_fisica_w	:= Obter_Pessoa_Fisica_Usuario(nm_usuario_p,'C');

select	nr_seq_episodio
into	nr_seq_episodio_w
from	atendimento_paciente
where	nr_atendimento = nr_atendimento_p;

if	(nr_seq_medic_diag_doe_p is null) then

	if(nr_seq_classificacao_p is not null)then

		select nvl(max(qt_diagnostico), -1),
			nvl(max(ie_classificacao_doenca),'N')
		into   qt_diagnostico_w,
			ie_classif_doenca_w
		from   classificacao_diagnostico
		where  nr_sequencia = nr_seq_classificacao_p;
		
		select	count(1)
		into	qt_medic_diag_w
		from	medic_diagnostico_doenca a,
			diagnostico_doenca b
		where   a.cd_doenca	= b.cd_doenca
		and	a.nr_atendimento	= b.nr_atendimento
		and	a.dt_diagnostico	= b.dt_diagnostico
		and	a.nr_atendimento	in (select 	x.nr_atendimento 
									from 	atendimento_paciente x, 
											episodio_paciente y 
									where 	x.nr_seq_episodio = y.nr_sequencia 
									and 	y.nr_sequencia = nr_seq_episodio_w)
		and	a.nr_seq_classificacao	= nr_seq_classificacao_p
		and	a.ie_situacao	= 'A'
		and	b.ie_situacao	= 'A';
	
		if (qt_diagnostico_w = -1 or qt_diagnostico_w > qt_medic_diag_w) then
		
			insert into medic_diagnostico_doenca
				(	cd_doenca,
					cd_estabelecimento,
					cd_pessoa_fisica,
					dt_atualizacao,
					dt_atualizacao_nrec,
					dt_diagnostico,
					ie_situacao,
					nm_usuario,
					nm_usuario_nrec,
					nr_seq_classificacao,
					nr_atendimento,
					nr_sequencia)
			values	(	cd_doenca_p,
					cd_estabelecimento_p,
					cd_pessoa_fisica_w,
					sysdate,
					sysdate,
					dt_diagnostico_p,
					'A',
					nm_usuario_p,
					nm_usuario_p,
					nr_seq_classificacao_p,
					nr_atendimento_p,
					medic_diagnostico_doenca_seq.nextval);
					
			select	max(a.nr_sequencia)
			into	nr_seq_medic_diag_doe_w
			from	medic_diagnostico_doenca a,
				classificacao_diagnostico b
			where	a.nr_seq_classificacao = b.nr_sequencia
			and	a.cd_doenca	= cd_doenca_p
			and	a.nr_atendimento	in (select 	x.nr_atendimento 
										from 	atendimento_paciente x, 
												episodio_paciente y 
										where 	x.nr_seq_episodio = y.nr_sequencia 
										and 	y.nr_sequencia = nr_seq_episodio_w)
			and	a.dt_diagnostico	= dt_diagnostico_p
			and	b.ie_classificacao_doenca	= decode(ie_classif_doenca_w, 'P' , 'S', 'S', 'P')
			and	a.ie_situacao	= 'A';
			
			if	(nr_seq_medic_diag_doe_w is not null) then
				update	medic_diagnostico_doenca
				set	ie_situacao	= 'I',
					dt_atualizacao	= sysdate,
					nm_usuario_inativacao	= nm_usuario_p
				where	nr_sequencia	= nr_seq_medic_diag_doe_w;
				
				update	diagnostico_doenca
				set	ie_classificacao_doenca = ie_classif_doenca_w
				where	cd_doenca	= cd_doenca_p
				and	nr_atendimento	= nr_atendimento_p
				and	dt_diagnostico	= dt_diagnostico_p;
			end if;
			
			select  ie_diagnostico_admissao
			into    ie_diagnostico_admissao_p
			from    classificacao_diagnostico
			where   nr_sequencia = nr_seq_classificacao_p;
		else 
		
			ds_retorno_abort_p := wheb_mensagem_pck.get_texto (1015948); --'N�mero de registros para esta classifica��o atingida. CID''s que j� possuem esta classifica��o: ';
			open C01;
			loop
			fetch C01 into	
				ds_diagnostico_w,
				dt_diagnostico_w;
			exit when C01%notfound;
				begin
				ds_macros_w := 'DS_DIAGNOSTICO=' || ds_diagnostico_w || ';DT_DIAGNOSTICO=' || dt_diagnostico_w;				
				ds_retorno_abort_p := ds_retorno_abort_p || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(1015949, ds_macros_w) || ';';
				end;
			end loop;
			close C01;
		end if;
	end if;
else
	update	medic_diagnostico_doenca
	set	ie_situacao		= 'I',
		nm_usuario_inativacao	= nm_usuario_p
	where	nr_sequencia		= nr_seq_medic_diag_doe_p;
end if;
		
commit;

end generate_medical_diag_disease;
/
