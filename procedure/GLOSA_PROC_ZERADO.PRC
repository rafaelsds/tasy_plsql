CREATE OR REPLACE 
PROCEDURE GLOSA_PROC_ZERADO(nr_sequencia_p		Number) IS

cd_estabelecimento_w		number(8);
vl_disponivel_w			Number(15,2);
vl_autorizado_w			Number(15,2);
vl_utilizado_w			Number(15,2);
vl_procedimento_w		Number(15,2);
nr_seq_autorizacao_w		varchar2(10);
cd_procedimento_w		Number(15);
ie_origem_proced_w		Number(10,0);
cd_convenio_w			Number(05,0);
cd_categoria_w			Varchar2(10);
nm_usuario_w			Varchar2(15);
nr_sequencia_w			Number(10,0);
vl_medico_w			Number(15,2);
vl_anestesista_w		Number(15,2);
vl_auxiliares_w			Number(15,2);
vl_custo_operacional_w		Number(15,2);
vl_materiais_w			Number(15,2);
vl_adic_plant_w			Number(15,2);
cd_convenio_glosa_w		Number(05,0) := null;
cd_categoria_glosa_w		Varchar2(10) := null;
nr_atendimento_w		Number(10);

BEGIN

select	a.cd_convenio,
	a.cd_categoria,
	a.nr_atendimento,
	nvl(a.vl_procedimento,0),
	a.nr_seq_autorizacao,
	a.cd_procedimento,
	a.ie_origem_proced,
	a.nm_usuario,
	nvl(a.vl_medico,0),
	nvl(a.vl_anestesista,0),
	nvl(a.vl_auxiliares,0),
	nvl(a.vl_custo_operacional,0),
	nvl(a.vl_materiais,0),
	nvl(a.vl_adic_plant,0),
	b.cd_estabelecimento
into	cd_convenio_w,
	cd_categoria_w,
	nr_atendimento_w,
	vl_procedimento_w,
	nr_seq_autorizacao_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	nm_usuario_w,
	vl_medico_w,
	vl_anestesista_w,
	vl_auxiliares_w,
	vl_custo_operacional_w,
	vl_materiais_w,
	vl_adic_plant_w,
	cd_estabelecimento_w
from	atendimento_paciente b,
	Procedimento_paciente a
where	a.nr_atendimento = b.nr_atendimento
and	a.nr_sequencia	= nr_sequencia_p;


begin
select	nvl(cd_convenio_glosa,0),
	nvl(cd_categoria_glosa,' ')
into	cd_convenio_glosa_w,
	cd_categoria_glosa_w
from	atend_categoria_convenio
where	nr_seq_interno	=
	(select nvl(max(x.nr_seq_interno),0)
		from 	atend_categoria_convenio x
		where	x.nr_atendimento = nr_atendimento_w
		and	x.cd_convenio	 = cd_convenio_w
		and	x.cd_categoria	 = cd_categoria_w);
exception
	when others then
		cd_convenio_glosa_w	:= 0;
		cd_categoria_glosa_w	:= ' ';
end;

if	(cd_convenio_glosa_w = 0) then	
	obter_convenio_particular(cd_estabelecimento_w, cd_convenio_glosa_w,cd_categoria_glosa_w);
end if;


duplicar_proc_paciente(nr_sequencia_p, nm_usuario_w, nr_sequencia_w);


update procedimento_paciente
set	ie_valor_informado	= 'S',
	vl_medico		= 0,
	vl_anestesista		= 0,
	vl_auxiliares		= 0,
	vl_custo_operacional	= 0,
	vl_materiais		= 0,
	vl_adic_plant		= 0,
	vl_Procedimento		= 0,
	qt_procedimento		= 0
where	nr_sequencia		= nr_sequencia_p;

update procedimento_paciente
set	ie_valor_informado	= 'S',
	vl_medico		= vl_medico_w,
	vl_anestesista		= vl_anestesista_w,
	vl_auxiliares		= vl_auxiliares_w,
	vl_custo_operacional	= vl_custo_operacional_w,
	vl_materiais		= vl_materiais_w,
	vl_adic_plant		= vl_adic_plant_w,
	vl_procedimento		= vl_Procedimento_w,
	cd_situacao_glosa	= 0,	
	cd_convenio		= cd_convenio_glosa_w,
	cd_categoria		= cd_categoria_glosa_w,
	nr_seq_proc_princ	= nr_sequencia_p,
	nr_interno_conta	= null
where	nr_sequencia		= nr_sequencia_w;


END GLOSA_PROC_ZERADO;
/