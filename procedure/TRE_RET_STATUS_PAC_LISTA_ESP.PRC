create or replace
procedure tre_ret_status_pac_lista_esp(	
			nr_sequencia_p		number,
			cd_pessoa_fisica_p	varchar2) is 

nr_seq_curso_w		number(10);
cd_pessoa_fisica_w	varchar2(10);

cursor c01 is
select	a.nr_seq_curso,
	c.cd_pessoa_fisica
from	tre_agenda a,
	tre_candidato c
where	c.nr_seq_agenda = a.nr_sequencia
and     c.nr_seq_agenda = nr_sequencia_p
and	c.cd_pessoa_fisica = nvl(cd_pessoa_fisica_p,c.cd_pessoa_fisica);

begin

open c01;
loop	
fetch c01 into
	nr_seq_curso_w,
	cd_pessoa_fisica_w;
exit when c01%notfound;
	update	tre_pf_lista_espera
	set	ie_status 	= 'A'
	where	nr_seq_curso	= nr_seq_curso_w
	and	cd_pessoa_fisica = cd_pessoa_fisica_w;
end loop;
close c01;

commit;

end tre_ret_status_pac_lista_esp;
/