create or replace
procedure Recist_Inserir_Localizacao(	ds_localizacao_p	varchar2,
										IE_MENSURAR_p		varchar2,
										NR_SEQ_REGISTRO_p	number,
										nm_usuario_p		Varchar2) is 

begin

insert into RECIST_LOCALIZACAO (
			 NR_SEQUENCIA,
			 DT_ATUALIZACAO,
			 NM_USUARIO,
			 DT_ATUALIZACAO_NREC,
			 NM_USUARIO_NREC,
			 NR_SEQ_REGISTRO,
			 DS_LOCALIZACAO,
			 IE_MENSURAR)
		values (
			 RECIST_LOCALIZACAO_seq.nextval,
			 sysdate,
			 nm_usuario_p,
			 sysdate,
			 nm_usuario_p,
			 NR_SEQ_REGISTRO_p,
			 ds_localizacao_p,
			 IE_MENSURAR_p);



commit;

end Recist_Inserir_Localizacao;
/