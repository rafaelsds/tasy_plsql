create or replace
procedure desfazer_historico_manutencao(nr_sequencia_p	number,
			nm_usuario_p		Varchar2) is 

begin

if	(nr_sequencia_p is not null) then
	begin
	update	titulo_receber_hist 
	set 	dt_liberacao = null 
	where 	nr_sequencia = nr_sequencia_p;
	
	commit;
	end;
end if;

end desfazer_historico_manutencao;
/