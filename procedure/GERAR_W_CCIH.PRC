create or replace
procedure GERAR_W_CCIH (ie_somente_internado_p	varchar2,
			dt_inicial_p		date,
			dt_final_p		date,
			cd_setor_p		number,
			nr_seq_regra_p		number, 			
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	number,
			ie_somente_controle_p	varchar2,
			ie_tipo_atendimento_p	number,
			ie_possui_ficha_p	varchar2,
			ie_somente_alta_p	varchar2,
			ie_setor_internacao_p	varchar2,
			nr_atendimento_p	number,
			nr_agrupamento_p	number,
			ie_controle_pos_alta_p	varchar2 default 'N',
			ie_origem_infeccao_p	varchar2 default 'N',
			nr_dias_consulta_se_p	number) is 
			
			
ds_sep_bv_w			varchar2(50);
C01				integer;
C04				integer;
ds_comando_w			varchar2(4000);
ds_parametros_w			varchar2(4000);	
		

begin

ds_sep_bv_w	:= obter_separador_bv;

exec_sql_dinamico(nm_usuario_p,'truncate table w_ccih');
exec_sql_dinamico(nm_usuario_p,'truncate table w_ccih_temp');

if	(ie_somente_internado_p = 'N') then

	if	(nr_seq_regra_p is null) then
		ds_comando_w	:= 						   'insert into w_ccih_temp(nr_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'cd_setor_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'ds_setor_atendimento)';
		ds_comando_w	:= ds_comando_w ||chr(10)||'select  distinct';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		b.nr_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		d.cd_setor_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		d.ds_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'from    atendimento_paciente b,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		atend_paciente_unidade c,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		setor_atendimento d';
		ds_comando_w	:= ds_comando_w ||chr(10)||'where    b.nr_atendimento        = c.nr_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'and	 d.cd_setor_atendimento = c.cd_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'and	 b.nr_atendimento is not null';
		ds_comando_w	:= ds_comando_w ||chr(10)||'and obter_se_reg_lib_atencao(b.cd_pessoa_fisica, b.cd_medico_resp, b.ie_nivel_atencao, b.nm_usuario, 0, 0) = ''S''';
			
			if (ie_setor_internacao_p = 'N') then
				ds_comando_w	:= ds_comando_w ||chr(10)||'and c.nr_seq_interno = (select nvl(max(x.nr_seq_interno),0) ';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						from atend_paciente_unidade x';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						where x.nr_atendimento = b.nr_atendimento ';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						and nvl(x.dt_saida_unidade, x.dt_entrada_unidade + 9999) =';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						(select max(nvl(a.dt_saida_unidade, a.dt_entrada_unidade + 9999))';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						from 	atend_paciente_unidade a';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						where	a.nr_atendimento 		= b.nr_atendimento))';
			else
				ds_comando_w	:= ds_comando_w ||chr(10)||'and  d.cd_classif_setor IN (3,4,8,12)';
				ds_comando_w	:= ds_comando_w ||chr(10)||'and c.nr_seq_interno = (select max(x.nr_seq_interno)';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						from atend_paciente_unidade x';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						where x.nr_atendimento = b.nr_atendimento';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						and nvl(x.dt_saida_unidade, x.dt_entrada_unidade + 9999) =';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						(select max(nvl(a.dt_saida_unidade, a.dt_entrada_unidade + 9999))';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						from 	setor_atendimento e,';
				ds_comando_w	:= ds_comando_w ||chr(10)||'								atend_paciente_unidade a';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						where	a.nr_atendimento 		= b.nr_atendimento';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						and	e.cd_setor_atendimento		= a.cd_setor_atendimento';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						and 	e.cd_classif_setor in (3,4,8,12)))';
			end if;
	else	
		ds_comando_w	:= 						   'insert into w_ccih_temp(nr_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'cd_setor_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'ds_setor_atendimento)';
		ds_comando_w	:= ds_comando_w ||chr(10)||'select  distinct';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		b.nr_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		d.cd_setor_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		d.ds_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'from    atendimento_paciente b,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		atend_paciente_unidade c,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		setor_atendimento d,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		cih_setor_regra e';
		ds_comando_w	:= ds_comando_w ||chr(10)||'where    b.nr_atendimento        = c.nr_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'and	 d.cd_setor_atendimento = c.cd_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'and	 b.nr_atendimento is not null';
		ds_comando_w	:= ds_comando_w ||chr(10)||'and	c.cd_setor_atendimento		= e.cd_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'and obter_se_reg_lib_atencao(b.cd_pessoa_fisica, b.cd_medico_resp, b.ie_nivel_atencao, b.nm_usuario, 0, 0) = ''S''';
			
			
			if (ie_setor_internacao_p = 'N') then
				ds_comando_w	:= ds_comando_w ||chr(10)||'and c.nr_seq_interno = (select nvl(max(x.nr_seq_interno),0) ';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						from atend_paciente_unidade x';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						where x.nr_atendimento = b.nr_atendimento ';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						and nvl(x.dt_saida_unidade, x.dt_entrada_unidade + 9999) =';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						(select max(nvl(a.dt_saida_unidade, a.dt_entrada_unidade + 9999))';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						from 	atend_paciente_unidade a';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						where	a.nr_atendimento 		= b.nr_atendimento))';
			else
				ds_comando_w	:= ds_comando_w ||chr(10)||'and  d.cd_classif_setor IN (3,4,8,12)';
				ds_comando_w	:= ds_comando_w ||chr(10)||'and c.nr_seq_interno = (select max(x.nr_seq_interno)';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						from atend_paciente_unidade x';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						where x.nr_atendimento = b.nr_atendimento';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						and nvl(x.dt_saida_unidade, x.dt_entrada_unidade + 9999) =';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						(select max(nvl(a.dt_saida_unidade, a.dt_entrada_unidade + 9999))';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						from 	setor_atendimento e,';
				ds_comando_w	:= ds_comando_w ||chr(10)||'								atend_paciente_unidade a';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						where	a.nr_atendimento 		= b.nr_atendimento';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						and	e.cd_setor_atendimento		= a.cd_setor_atendimento';
				ds_comando_w	:= ds_comando_w ||chr(10)||'						and 	e.cd_classif_setor in (3,4,8,12)))';
			end if;
			
			ds_comando_w	:= ds_comando_w ||chr(10)||' and e.nr_seq_regra = :nr_seq_regra ';
			ds_parametros_w	:= ds_parametros_w||'nr_seq_regra='||nr_seq_regra_p|| ds_sep_bv_w;

	end if;
	if	(ie_controle_pos_alta_p = 'N') then
		If	(ie_somente_alta_p = 'S') then
			ds_comando_w	:= ds_comando_w ||chr(10)||' AND	   b.dt_alta BETWEEN :dt_inicial AND fim_dia(:dt_final) ';
		elsif (ie_origem_infeccao_p = 'N') then
			ds_comando_w	:= ds_comando_w ||chr(10)||' AND	   b.dt_entrada BETWEEN :dt_inicial AND fim_dia(:dt_final) ';
		End if;	
		
		if ((ie_somente_alta_p = 'S') or (ie_origem_infeccao_p = 'N')) then --cuidar para somente adicionar os parametros caso for adicionado a bind do mesmo, caso contrario ira causar erro no DBMS_SQL.BIND_VARIABLE
			ds_parametros_w	:= ds_parametros_w||'DT_INICIAL='||to_char(dt_inicial_p,'dd/mm/yyyy hh24:mi:ss')|| ds_sep_bv_w;
			ds_parametros_w	:= ds_parametros_w||'DT_FINAL='||to_char(dt_final_p,'dd/mm/yyyy hh24:mi:ss')|| ds_sep_bv_w;
		end if;
	end if;	
	
	if	(cd_setor_p is not null) then
		ds_comando_w	:= ds_comando_w ||chr(10)||' and d.cd_setor_atendimento = :cd_setor_atendimento ';
		ds_parametros_w	:= ds_parametros_w||'cd_setor_atendimento='||cd_setor_p|| ds_sep_bv_w;
	end if;
	
	if	(nr_agrupamento_p is not null) then
		ds_comando_w	:= ds_comando_w ||chr(10)||' and Obter_agrupamento_setor(d.cd_setor_atendimento) = :nr_agrupamento ';
		ds_parametros_w	:= ds_parametros_w||'nr_agrupamento='||nr_agrupamento_p|| ds_sep_bv_w;
	end if;
	
	if	(nr_atendimento_p is not null) then
		ds_comando_w	:= ds_comando_w ||chr(10)||' and b.nr_atendimento = :nr_atendimento ';
		ds_parametros_w	:= ds_parametros_w||'nr_atendimento='||nr_atendimento_p|| ds_sep_bv_w;
	end if;
	
	
	if	(ie_tipo_atendimento_p is not null) then
		ds_comando_w	:= ds_comando_w ||chr(10)||' and b.ie_tipo_atendimento = :ie_tipo_atendimento  ';
		ds_parametros_w	:= ds_parametros_w||'ie_tipo_atendimento='||ie_tipo_atendimento_p|| ds_sep_bv_w;
	end if;
	
	if	(ie_origem_infeccao_p = 'S') then
		ds_comando_w := ds_comando_w ||chr(10)||'  and exists (select 	1  ';
		ds_comando_w := ds_comando_w ||chr(10)||'              from 	cih_ficha_ocorrencia x, CIH_LOCAL_INFECCAO y ';
		ds_comando_w := ds_comando_w ||chr(10)||'              where 	x.NR_FICHA_OCORRENCIA = y.NR_FICHA_OCORRENCIA ';
		ds_comando_w := ds_comando_w ||chr(10)||'              and 	x.nr_atendimento = b.nr_atendimento ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' 	       AND	y.DT_ORIGEM_INFECCAO BETWEEN :dt_inicial AND fim_dia(:dt_final)) ';
		ds_parametros_w	:= ds_parametros_w||'DT_INICIAL='||to_char(dt_inicial_p,'dd/mm/yyyy hh24:mi:ss')|| ds_sep_bv_w;
		ds_parametros_w	:= ds_parametros_w||'DT_FINAL='||to_char(dt_final_p,'dd/mm/yyyy hh24:mi:ss')|| ds_sep_bv_w;	
	end if;
	
	
	if	(ie_possui_ficha_p = 'S') then		
		ds_comando_w	:= ds_comando_w ||chr(10)||' and exists (select 1 from cih_ficha_ocorrencia where nr_atendimento = b.nr_atendimento)  ';		
	elsif	(ie_possui_ficha_p = 'N') then	
		ds_comando_w	:= ds_comando_w ||chr(10)||' and not exists (select 1 from cih_ficha_ocorrencia where nr_atendimento = b.nr_atendimento)  ';
	elsif	(ie_possui_ficha_p = 'L') then
		ds_comando_w	:= ds_comando_w ||chr(10)||' and  exists (select 1 from cih_ficha_ocorrencia where nr_atendimento = b.nr_atendimento and dt_liberacao is null )';
	
	end if;
	
	
	ds_comando_w	:= ds_comando_w ||chr(10)||' and	  D.ie_situacao = :ie_situacao ';
	ds_parametros_w	:= ds_parametros_w||'ie_situacao='||'A'|| ds_sep_bv_w;
	
	ds_comando_w	:= ds_comando_w ||chr(10)||' and D.cd_estabelecimento = :cd_estabelecimento ';
	ds_parametros_w	:= ds_parametros_w||'cd_estabelecimento='||cd_estabelecimento_p|| ds_sep_bv_w;
	
	if	(nvl(nr_dias_consulta_se_p,0) <> 0) then	
		ds_comando_w	:= ds_comando_w ||chr(10)||' and exists (select 1 ' ;
		ds_comando_w	:= ds_comando_w ||chr(10)||' from atendimento_paciente c ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' where b.cd_pessoa_fisica = c.cd_pessoa_fisica ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' and c.nr_atendimento <> b.nr_atendimento ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' and c.ie_tipo_atendimento = 1 ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' and c.dt_alta >=  b.dt_entrada - :nr_dias_consulta_se) ';
		ds_parametros_w	:= ds_parametros_w||'nr_dias_consulta_se='||nr_dias_consulta_se_p|| ds_sep_bv_w;		
	end if;
	
	Exec_sql_Dinamico_bv('CCIH',ds_comando_w,ds_parametros_w);
	
else
	if	(nr_seq_regra_p is null) then
		ds_comando_w	:=                         'insert into w_ccih_temp(nr_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						cd_setor_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						ds_setor_atendimento)';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						select  distinct';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						        b.nr_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'								c.cd_setor_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'		 						c.ds_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						from    unidade_atendimento b,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'								setor_atendimento c,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'								atendimento_paciente e';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						where   b.cd_setor_atendimento        	= c.cd_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						and	e.nr_atendimento		= b.nr_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						and	b.nr_atendimento is not null';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						and obter_se_reg_lib_atencao(e.cd_pessoa_fisica, e.cd_medico_resp, e.ie_nivel_atencao, e.nm_usuario, 0, 0) = ''S''';
	else
		ds_comando_w	:= 						   'insert into w_ccih_temp(nr_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						cd_setor_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						ds_setor_atendimento)';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						select  distinct';
		ds_comando_w	:= ds_comando_w ||chr(10)||'								b.nr_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'								c.cd_setor_atendimento,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'								c.ds_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						from    unidade_atendimento b,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'								setor_atendimento c,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'								CIH_SETOR_REGRA d,';
		ds_comando_w	:= ds_comando_w ||chr(10)||'								atendimento_paciente e';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						where   b.cd_setor_atendimento = c.cd_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						and	c.cd_setor_atendimento	    	= d.cd_setor_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						and	e.nr_atendimento		= b.nr_atendimento';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						and	b.nr_atendimento is not null';
		ds_comando_w	:= ds_comando_w ||chr(10)||'						and obter_se_reg_lib_atencao(e.cd_pessoa_fisica, e.cd_medico_resp, e.ie_nivel_atencao, e.nm_usuario, 0, 0) = ''S''';
						
			ds_comando_w	:= ds_comando_w ||chr(10)||' and d.nr_seq_regra = :nr_seq_regra ';
			ds_parametros_w	:= ds_parametros_w||'nr_seq_regra='||nr_seq_regra_p|| ds_sep_bv_w;

	end if;
	
		
	if	(cd_setor_p is not null) then
		ds_comando_w	:= ds_comando_w ||chr(10)||' and b.cd_setor_atendimento = :cd_setor_atendimento ';
		ds_parametros_w	:= ds_parametros_w||'cd_setor_atendimento='||cd_setor_p|| ds_sep_bv_w;
	end if;
	
	if	(nr_agrupamento_p is not null) then
		ds_comando_w	:= ds_comando_w ||chr(10)||' and Obter_agrupamento_setor(b.cd_setor_atendimento) = :nr_agrupamento ';
		ds_parametros_w	:= ds_parametros_w||'nr_agrupamento='||nr_agrupamento_p|| ds_sep_bv_w;
	end if;
	
	if	(nr_atendimento_p is not null) then
		ds_comando_w	:= ds_comando_w ||chr(10)||' and e.nr_atendimento = :nr_atendimento ';
		ds_parametros_w	:= ds_parametros_w||'nr_atendimento='||nr_atendimento_p|| ds_sep_bv_w;
	end if;
	
	
	if	(ie_tipo_atendimento_p is not null) then
		ds_comando_w	:= ds_comando_w ||chr(10)||' and e.ie_tipo_atendimento = :ie_tipo_atendimento  ';
		ds_parametros_w	:= ds_parametros_w||'ie_tipo_atendimento='||ie_tipo_atendimento_p|| ds_sep_bv_w;
	end if;
	
	
	if	(ie_possui_ficha_p = 'S') then
		
		ds_comando_w	:= ds_comando_w ||chr(10)||' and exists (select 1 from cih_ficha_ocorrencia where nr_atendimento = b.nr_atendimento)  ';
		
	elsif	(ie_possui_ficha_p = 'N') then
	
		ds_comando_w	:= ds_comando_w ||chr(10)||' and not exists (select 1 from cih_ficha_ocorrencia where nr_atendimento = b.nr_atendimento)  ';
	elsif	(ie_possui_ficha_p = 'L') then
	
		ds_comando_w	:= ds_comando_w ||chr(10)||' and  exists (select 1 from cih_ficha_ocorrencia where nr_atendimento = b.nr_atendimento and dt_liberacao is null )';
	end if;
		
		
	if	(ie_origem_infeccao_p = 'S') then
		ds_comando_w := ds_comando_w ||chr(10)||'  and exists (select 	1  ';
		ds_comando_w := ds_comando_w ||chr(10)||'              from 	cih_ficha_ocorrencia x, CIH_LOCAL_INFECCAO y ';
		ds_comando_w := ds_comando_w ||chr(10)||'              where 	x.NR_FICHA_OCORRENCIA = y.NR_FICHA_OCORRENCIA ';
		ds_comando_w := ds_comando_w ||chr(10)||'              and 	x.nr_atendimento = b.nr_atendimento ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' 	       AND	y.DT_ORIGEM_INFECCAO BETWEEN :dt_inicial AND fim_dia(:dt_final)) ';
		ds_parametros_w	:= ds_parametros_w||'DT_INICIAL='||to_char(dt_inicial_p,'dd/mm/yyyy hh24:mi:ss')|| ds_sep_bv_w;
		ds_parametros_w	:= ds_parametros_w||'DT_FINAL='||to_char(dt_final_p,'dd/mm/yyyy hh24:mi:ss')|| ds_sep_bv_w;	
	end if;	
	
	
	ds_comando_w	:= ds_comando_w ||chr(10)||' and c.ie_situacao = :ie_situacao ';
	ds_parametros_w	:= ds_parametros_w||'ie_situacao='||'A'|| ds_sep_bv_w;
	
	ds_comando_w	:= ds_comando_w ||chr(10)||' and c.cd_estabelecimento = :cd_estabelecimento ';
	ds_parametros_w	:= ds_parametros_w||'cd_estabelecimento='||cd_estabelecimento_p|| ds_sep_bv_w;
	
	if	(nvl(nr_dias_consulta_se_p,0) <> 0) then
		
		ds_comando_w	:= ds_comando_w ||chr(10)||' and exists (select 1 ' ;
		ds_comando_w	:= ds_comando_w ||chr(10)||' from atendimento_paciente c ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' where e.cd_pessoa_fisica = c.cd_pessoa_fisica ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' and c.nr_atendimento <> e.nr_atendimento ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' and c.ie_tipo_atendimento = 1 ';
		ds_comando_w	:= ds_comando_w ||chr(10)||' and c.dt_alta >=  e.dt_entrada - :nr_dias_consulta_se) ';
		ds_parametros_w	:= ds_parametros_w||'nr_dias_consulta_se='||nr_dias_consulta_se_p|| ds_sep_bv_w;
		
	end if;	

	Exec_sql_Dinamico_bv('CCIH',ds_comando_w,ds_parametros_w);

end if;
ds_parametros_w	:= null;

ds_comando_w	:=  					   'insert into w_ccih(	nr_atendimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					cd_pessoa_fisica,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					nr_prontuario,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					ds_sexo,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					dt_nascimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					ds_idade,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					dt_entrada,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					dt_alta,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					nm_paciente,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					ds_setor_atendimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					ds_unidade_atendimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					nm_medico_resp,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					cd_setor_atendimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'					nr_ficha_ocorrencia)';
ds_comando_w	:= ds_comando_w ||chr(10)||'					SELECT  DISTINCT';
ds_comando_w	:= ds_comando_w ||chr(10)||'							a.nr_atendimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							a.cd_pessoa_fisica,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							d.nr_prontuario,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							d.ie_sexo ds_sexo,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							d.dt_nascimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							SUBSTR(obter_idade_pf(a.cd_pessoa_fisica, SYSDATE, ''S''),1,30)ds_idade,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							a.dt_entrada,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							a.dt_alta,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							d.nm_pessoa_fisica nm_paciente,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							B.ds_setor_atendimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							SUBSTR(obter_unidade_atendimento(a.nr_atendimento,''A'',''SU''),1,220) ds_unidade_atendimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							SUBSTR(obter_nome_pf(a.cd_medico_resp),1,60) nm_medico_resp,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							b.cd_setor_atendimento,';
ds_comando_w	:= ds_comando_w ||chr(10)||'							(select max(nr_ficha_ocorrencia)';
ds_comando_w	:= ds_comando_w ||chr(10)||'							from cih_ficha_ocorrencia f';
ds_comando_w	:= ds_comando_w ||chr(10)||'							where f.nr_atendimento = b.nr_atendimento) nr_ficha_ocorrencia';
ds_comando_w	:= ds_comando_w ||chr(10)||'							FROM    w_ccih_temp b,';
ds_comando_w	:= ds_comando_w ||chr(10)||'									pessoa_fisica d,';
ds_comando_w	:= ds_comando_w ||chr(10)||'									atendimento_paciente a';
ds_comando_w	:= ds_comando_w ||chr(10)||'							WHERE   a.nr_atendimento              = b.nr_atendimento';
ds_comando_w	:= ds_comando_w ||chr(10)||'							AND	a.cd_pessoa_fisica           = d.cd_pessoa_fisica ';


if 	(ie_somente_controle_p = 'N')	 then
	
	if	(ie_somente_internado_p = 'S') then
		ds_comando_w	:= ds_comando_w ||chr(10)||' AND	   a.dt_alta is null ';
	end if;	
	
	if	(ie_somente_alta_p = 'S') then
		ds_comando_w	:= ds_comando_w ||chr(10)||' AND	   a.dt_alta is not null ';
	end if;	

end if;
	
Exec_sql_Dinamico_bv('CCIH',ds_comando_w,ds_parametros_w);

commit;

end GERAR_W_CCIH;
/
