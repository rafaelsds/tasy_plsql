create or replace
procedure LIMPAR_W_FUNCAO_NAO_CADASTRADA(	nm_usuario_p	varchar2,
						dt_referencia_p	date) is

begin

delete	from w_funcao_nao_cadastrada
where	dt_atualizacao	<= fim_dia(dt_referencia_p - 1)
and	nm_usuario	= nm_usuario_p;

commit;

end LIMPAR_W_FUNCAO_NAO_CADASTRADA;
/