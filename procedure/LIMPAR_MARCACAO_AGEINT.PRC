create or replace
procedure Limpar_Marcacao_Ageint(
			nr_seq_ageint_p		number,
			nm_usuario_p		Varchar2) is 

cd_tipo_agenda_w	Number(10);
cd_agenda_w		Number(10); 
nr_seq_agenda_w		Number(10);	
nr_sequencia_w		Number(10);	
nr_seq_turno_w		number(10);
nr_minuto_duracao_marc_w 	number(10);
	
Cursor C01 is
	select	cd_agenda,
		nr_Seq_agenda,
		nr_Sequencia,
		nr_minuto_duracao
	from	ageint_marcacao_usuario
	where	nm_usuario	 	= nm_usuario_p
	and	nvl(ie_Gerado,'N')	<> 'S'
	and	nr_seq_ageint		= nr_seq_ageint_p;
			
begin
open C01;
loop
fetch C01 into	
	cd_Agenda_w,
	nr_seq_agenda_w,
	nr_sequencia_w,
	nr_minuto_duracao_marc_w;
exit when C01%notfound;
	begin
	select	cd_tipo_agenda
	into	cd_tipo_agenda_w
	from	agenda
	where	cd_agenda	= cd_agenda_w;
	
	if	(cd_tipo_agenda_w	= 2) then
		liberar_horario_agenda_exame(nr_seq_Agenda_w, nm_usuario_p);

		liberar_hor_dur_age_exame(nr_seq_Agenda_w, nr_minuto_duracao_marc_w, nm_usuario_p);
		
		/*update	agenda_paciente
		set	ie_status_agenda	= 'L',
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate			
		where	nr_sequencia		= nr_seq_agenda_w;*/
	else
		select	max(nr_seq_turno)
		into	nr_seq_turno_w
		from	agenda_consulta
		where	nr_sequencia	= nr_seq_agenda_w;
		liberar_horario_agecons(nr_seq_agenda_w, nm_usuario_p, nr_seq_turno_w);
		/*update	agenda_consulta
		set	ie_status_agenda	= 'L',
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_sequencia		= nr_seq_agenda_w;*/
	end if;
	
	end;
end loop;
close C01;

delete	ageint_encaixe_usuario
where	nm_usuario	= nm_usuario_p
and	nr_seq_ageint	= nr_seq_ageint_p;

delete	ageint_horarios_usuario
where	nm_usuario		= nm_usuario_p;

delete 	ageint_marcacao_usuario
where	nm_usuario		= nm_usuario_p
and	nvl(ie_Gerado,'N')	<> 'S'
and	nr_seq_ageint		= nr_seq_ageint_p;

delete	ageint_lib_usuario 
where	nm_usuario		= nm_usuario_p
and	nr_seq_ageint		= nr_seq_ageint_p; 

delete 	ageint_sugestao_horarios
where	nm_usuario	= nm_usuario_p;
	
commit;

end Limpar_Marcacao_Ageint;
/