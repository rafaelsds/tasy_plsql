create or replace
procedure pls_selecionar_item_atrib(	ie_tipo_tabela_p	Varchar2,
					ie_tipo_preco_p		Varchar2,
					nm_atributo_p		Varchar2,
					ie_tipo_regra_p		Varchar2,
					cd_estabelecimento_p	Number,
					nm_usuario_p		Varchar2) is 
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [X ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

/*  ie_tipo_regra_p 
	0= Ordem da regra
	1= Exce��o da regra*/

nr_seq_apresentacao_w	Number(10);
begin

if	(nm_atributo_p is not null) then
	if	(ie_tipo_regra_p = 0) then
		begin
		select	max(nr_seq_apresentacao)+1
		into	nr_seq_apresentacao_w
		from	pls_regra_preco_ordem
		where	ie_tipo_tabela = ie_tipo_tabela_p;
		exception
		when others then
			nr_seq_apresentacao_w := 1;
		end;
	elsif	(ie_tipo_regra_p = 1) then
		begin
		select	max(nr_seq_apresentacao)+1
		into	nr_seq_apresentacao_w
		from	pls_regra_preco_ordem_mat
		where	ie_tipo_tabela = ie_tipo_tabela_p;
		exception
		when others then
			nr_seq_apresentacao_w := 1;
		end;
	elsif	(ie_tipo_regra_p = 2) then
		begin
		select	max(nr_seq_apresentacao)+1
		into	nr_seq_apresentacao_w
		from	pls_regra_preco_ordem_serv
		where	ie_tipo_tabela = ie_tipo_tabela_p;
		exception
		when others then
			nr_seq_apresentacao_w := 1;
		end;
	end if;

	if	(nr_seq_apresentacao_w is null) then
		nr_seq_apresentacao_w := 1;
	end if;
	
	if	(ie_tipo_regra_p = 0) then
		insert into	pls_regra_preco_ordem	( 	cd_estabelecimento, dt_atualizacao, dt_atualizacao_nrec,     
								ie_tipo_preco, ie_tipo_tabela, nm_atributo,             
								nm_usuario, nm_usuario_nrec, nr_seq_apresentacao ,    
								nr_sequencia)
					values		(	cd_estabelecimento_p, sysdate, sysdate,     
								ie_tipo_preco_p, ie_tipo_tabela_p, upper(nm_atributo_p),             
								nm_usuario_p, nm_usuario_p, nr_seq_apresentacao_w ,    
								pls_regra_preco_ordem_seq.nextval);
	elsif	(ie_tipo_regra_p = 1) then
		insert into	pls_regra_preco_ordem_mat	( 	cd_estabelecimento, dt_atualizacao, dt_atualizacao_nrec,     
									ie_tipo_preco, ie_tipo_tabela, nm_atributo,             
									nm_usuario, nm_usuario_nrec, nr_seq_apresentacao ,    
									nr_sequencia)
						values		(	cd_estabelecimento_p, sysdate, sysdate,     
									ie_tipo_preco_p, ie_tipo_tabela_p, upper(nm_atributo_p),             
									nm_usuario_p, nm_usuario_p, nr_seq_apresentacao_w ,    
									pls_regra_preco_ordem_mat_seq.nextval);
	elsif	(ie_tipo_regra_p = 2) then
		insert into	pls_regra_preco_ordem_serv	( 	cd_estabelecimento, dt_atualizacao, dt_atualizacao_nrec,     
									ie_tipo_preco, ie_tipo_tabela, nm_atributo,             
									nm_usuario, nm_usuario_nrec, nr_seq_apresentacao ,    
									nr_sequencia)
						values		(	cd_estabelecimento_p, sysdate, sysdate,     
									ie_tipo_preco_p, ie_tipo_tabela_p, upper(nm_atributo_p),             
									nm_usuario_p, nm_usuario_p, nr_seq_apresentacao_w ,    
									pls_regra_preco_ordem_serv_seq.nextval);
	end if;
end if;


commit;

end pls_selecionar_item_atrib;
/
