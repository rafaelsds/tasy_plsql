create or replace
procedure registra_ciente_comunic_cih (	nr_sequencia_p		in	number,
										cd_pessoa_fisica_p	in	varchar2,
										ie_concordo_p 		in	varchar2 default 'S') is 

begin

if(ie_concordo_p = 'S') then
	CPOE_GERAR_SUBS_SUG_CIH (nr_sequencia_p, wheb_usuario_pck.get_nm_usuario);
end if;

update	prescr_mat_comunic_cih
set	cd_pessoa_ciente	= cd_pessoa_fisica_p,
	dt_ciente		  = sysdate,
	ie_concordo 	  = ie_concordo_p
where	nr_sequencia  = nr_sequencia_p;

commit;

end registra_ciente_comunic_cih;
/
