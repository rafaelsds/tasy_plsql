create or replace
procedure nut_atualiza_emissao_dieta(dt_servico_p	date) is 
	
nr_seq_dieta_w		number(10);
nr_seq_presc_w		number(10);
nr_seq_nut_impress_w	nut_impressao_relatorio.nr_sequencia%type;
	
cursor c01 is
	select	c.nr_sequencia	
	from	nut_atend_serv_dia a,
		prescr_medica b,
		prescr_dieta c,
		nut_servico d
	where	a.nr_atendimento = b.nr_atendimento
	and	((a.dt_servico between b.dt_inicio_prescr and b.dt_validade_prescr) or (b.dt_validade_prescr is null))
	and	b.dt_liberacao is not null
	and	b.nr_prescricao = c.nr_prescricao
	and	a.nr_seq_servico = d.nr_sequencia
	and	exists	(select	max(1)
			from	nut_servico_horario e
			where	e.nr_seq_servico = d.nr_sequencia
			and	dt_servico_p between to_date(to_char(dt_servico_p,'dd/mm/yyyy')||nvl(e.ds_horario_corte_inic,' 00:00'),'dd/mm/yyyy hh24:mi') and to_date(to_char(dt_servico_p,'dd/mm/yyyy')||nvl(e.ds_horario_corte_fim,' 23:59'),'dd/mm/yyyy hh24:mi')
			and 	rownum = 1)
	and	trunc(a.dt_servico) = trunc(dt_servico_p)
	and	c.ie_urgencia = 'S'
	and	c.dt_impressao_nutricao is null
	group by c.nr_sequencia;
	
cursor c02 is
	SELECT	c.nr_prescricao nr_seq
	FROM	prescr_medica b,
		prescr_dieta c
	WHERE	b.dt_liberacao IS NOT NULL
	AND	b.nr_prescricao = c.nr_prescricao
	AND	c.ie_urgencia = 'S'
	AND	c.dt_impressao_nutricao IS NULL
	AND 	trunc(b.dt_prescricao) = trunc(sysdate);
	
cursor c03 is
	SELECT	w.nr_prescricao nr_seq
	FROM	prescr_medica w,
		rep_jejum q
	WHERE	w.dt_liberacao IS NOT NULL
	AND	trunc(w.dt_prescricao) 	= trunc(sysdate)
	AND	w.nr_prescricao 	= q.nr_prescricao
	AND 	NOT EXISTS	(select 1 
				from 	nut_impressao_relatorio t
				where	t.ie_tipo_relatorio = 'TJ'
				and	t.nr_prescricao	    = q.nr_prescricao);

begin

open c01;
loop
fetch c01 into nr_seq_dieta_w;
	exit when c01%notfound;
		begin
		
		update	prescr_dieta
		set	dt_impressao_nutricao = dt_servico_p
		where	nr_sequencia = nr_seq_dieta_w;				
		
		end;
end loop;
close c01;

open c02;
loop
fetch c02 into nr_seq_presc_w;
	exit when c02%notfound;
		begin
		
		update	prescr_dieta
		set	dt_impressao_nutricao = sysdate
		where	nr_prescricao = nr_seq_presc_w;
		
		end;
end loop;
close c02;

if(Obter_Valor_Param_Usuario(10028, 10, Obter_Perfil_Ativo, Wheb_Usuario_Pck.get_nm_usuario, 0) = 'S')then

	open c03;
	loop
	fetch c03 into nr_seq_presc_w;
		exit when c03%notfound;
			begin
			
			select	nut_impressao_relatorio_seq.nextval
			into	nr_seq_nut_impress_w
			from	dual;
			
			insert into nut_impressao_relatorio(nr_sequencia, dt_atualizacao, dt_atualizacao_nrec, 
								dt_impressao, nm_usuario, ie_tipo_relatorio,
								nr_prescricao)
			values	(nr_seq_nut_impress_w, sysdate, sysdate, 
				sysdate, Wheb_Usuario_Pck.get_nm_usuario, 'TJ',
				nr_seq_presc_w);
			
			end;
	end loop;
	close c03;

end if;

commit;

end nut_atualiza_emissao_dieta;
/