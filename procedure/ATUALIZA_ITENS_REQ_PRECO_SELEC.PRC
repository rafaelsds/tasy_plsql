create or replace
procedure atualiza_Itens_Req_Preco_Selec(nr_sequencia_p		number,
					 qt_item_p		number
									) is 
begin

update	w_itens_reg_preco_selec
set	qt_item = qt_item_p
where 	nr_sequencia = nr_sequencia_p;

commit;

end atualiza_Itens_Req_Preco_Selec;
/