create or replace
procedure atualizar_asa(ie_asa_estado_paciente_p varchar2,
			 nr_cirurgia_p 		  number,
			 ie_status_p		  varchar2, 
			 nm_usuario_p		  Varchar2) is 
/* 
      'A' Atualizar 
     'D' Desfazer
   */
begin
if 	(ie_asa_estado_paciente_p is not null) and 
	(ie_status_p = 'A') and 
	(nr_cirurgia_p is not null) then 
	update	cirurgia
	set 	ie_asa_estado_paciente 	= ie_asa_estado_paciente_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where 	nr_cirurgia 		= nr_cirurgia_p;
elsif 	(ie_status_p = 'D') and 
	(nr_cirurgia_p is not null) then 
	update	cirurgia
	set 	ie_asa_estado_paciente 	= null,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where 	nr_cirurgia 		= nr_cirurgia_p;
end if;

commit;

end atualizar_asa;
/