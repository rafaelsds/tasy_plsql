create or replace 
procedure atualiza_conv_receb( nr_seq_receb_p	number) is

begin
update convenio_receb
set    ie_status = 'N'
where  nr_sequencia = nr_seq_receb_p;

commit;

end atualiza_conv_receb;
/