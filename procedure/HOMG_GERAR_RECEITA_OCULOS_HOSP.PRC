Create or Replace
procedure HOMG_Gerar_Receita_Oculos_HOSP(
                         	nr_sequencia_p     	number,
				nm_usuario_p		varchar2) IS



nr_sequencia_w		   Number(10);
ds_receita_w		   varchar2(32000);
nr_atendimento_w	   number(10);
nm_pessoa_fisica_w	varchar2(60);
cd_pessoa_fisica_w	varchar2(10);
nm_medico_w		      varchar2(60);
cd_medico_w		      varchar2(10);
ie_dinamica_w		   varchar2(1);
ie_estatica_w		   varchar2(1);
ie_receita_w		   varchar2(1);
ie_adicao_w		      varchar2(1);
vl_de_rd_od_w		   varchar2(10);
vl_dc_rd_od_w		   varchar2(10);
vl_eixo_rd_od_w		varchar2(10);
ds_visao_rd_od_w	   varchar2(40);
vl_de_rd_oe_w		   varchar2(10);
vl_dc_rd_oe_w		   varchar2(10);
vl_eixo_rd_oe_w		varchar2(10);
ds_visao_rd_oe_w	   varchar2(40);
vl_de_re_od_w		   varchar2(10);
vl_dc_re_od_w		   varchar2(10);
vl_eixo_re_od_w		varchar2(10);
ds_visao_re_od_w	   varchar2(40);
vl_de_re_oe_w		   varchar2(10);
vl_dc_re_oe_w		   varchar2(10);
vl_eixo_re_oe_w		varchar2(10);
ds_visao_re_oe_w	   varchar2(40);
vl_de_receita_od_w	varchar2(10);
vl_dc_receita_od_w	varchar2(10);
vl_eixo_receita_od_w	varchar2(10);
ds_visao_receita_od_w	varchar2(40);
vl_de_receita_oe_w	varchar2(10);
vl_dc_receita_oe_w	varchar2(10);
vl_eixo_receita_oe_w	varchar2(10);
ds_visao_receita_oe_w	varchar2(40);
vl_adicao_od_w		   varchar2(10);
vl_adicao_oe_w		   varchar2(10);

vl_soma_de_rd_od_w	varchar2(10);
vl_soma_de_rd_oe_w	varchar2(10);
vl_soma_dc_rd_od_w	varchar2(10);
vl_soma_dc_rd_oe_w	varchar2(10);

ds_esferica_w		   Varchar2(60) := Lpad('Esf�rica',60,' ');
vl_esf_od_w		      Varchar2(20);
vl_esf_oe_w		      Varchar2(20);
ds_cilindrica_w		Varchar2(20) := Lpad('Cil�ndrica',20,' ');
vl_cil_od_w		      Varchar2(20);
vl_cil_oe_w		      Varchar2(20);
ds_eixo_w		      Varchar2(20) := Lpad('Eixo',20,' ');
vl_eixo_od_w		   Varchar2(20);
vl_eixo_oe_w		   Varchar2(20);
ds_dnp_w		         Varchar2(20) := Lpad('D.N.P.',20,' ');
vl_dnp_w		         Varchar2(20) := Lpad('Medir',20,' ');
ds_olho_direito_w	   Varchar2(40) := Rpad('Olho Direito',40,' ');
ds_olho_esquerdo_w	Varchar2(40) := Rpad('Olho Esquerdo',36,' ');

vl_soma_eixo_oe_w	   varchar2(20);
vl_soma_eixo_od_w	   varchar2(20);

ds_tabela_w		      varchar2(10000);
ds_grauc_od_w		   varchar2(30);
ds_grauc_oe_w		   varchar2(30);	
ds_graue_od_w		   varchar2(30);	
ds_graue_oe_w		   varchar2(30);	
ds_eixo_od_w		   varchar2(30);
ds_eixo_oe_w		   varchar2(30);
ie_gerar_observacao_padrao_w	varchar2(1);
ds_obs_receita_w	   varchar2(255);
ds_linha_w		      varchar2(3000);
ie_somente_perto_w   varchar2(1);

begin

select	substr(obter_pessoa_atendimento(nr_atendimento,'N'),1,60),
         obter_pessoa_atendimento(nr_atendimento,'C'),
         cd_profissional,
         substr(obter_nome_medico(cd_profissional,'P'),1,60),
         nr_atendimento,
         ie_dinamica,
         ie_estatica,
         ie_receita,
         ie_adicao,
         vl_de_receita_od,
         vl_dc_receita_od,
         vl_eixo_receita_od,
         vl_de_receita_oe,
         vl_dc_receita_oe,
         vl_eixo_receita_oe,
         ds_obs_receita,
         vl_adicao_oe,
         vl_adicao_od,
         ie_somente_perto
into	   nm_pessoa_fisica_w,
         cd_pessoa_fisica_w,
         cd_medico_w,
         nm_medico_w,
         nr_atendimento_w,
         ie_dinamica_w,
         ie_estatica_w,
         ie_receita_w,
         ie_adicao_w,
         vl_de_receita_od_w,
         vl_dc_receita_od_w,
         vl_eixo_receita_od_w,
         vl_de_receita_oe_w,
         vl_dc_receita_oe_w,
         vl_eixo_receita_oe_w,
         ds_obs_receita_w,
         vl_adicao_oe_w,
         vl_adicao_od_w,
         ie_somente_perto_w
from	   oft_consulta_medica
where	   nr_sequencia	= nr_sequencia_p;

ie_gerar_observacao_padrao_w	:= nvl(Obter_Valor_Param_Usuario(281, 456, Obter_perfil_Ativo, nm_usuario_p, 0),'S');



if	(ie_receita_w = 'S') then
	begin
	ds_graue_od_w	:= vl_de_receita_od_w;
	ds_grauc_od_w	:= vl_dc_receita_od_w;
	ds_eixo_od_w	:= vl_eixo_receita_od_w;
	ds_graue_oe_w	:= vl_de_receita_oe_w;
	ds_grauc_oe_w	:= vl_dc_receita_oe_w;
	ds_eixo_oe_w	:= vl_eixo_receita_oe_w;
	end;
elsif	(ie_estatica_w = 'S') then
	begin
	ds_graue_od_w	:= vl_de_re_od_w;
	ds_grauc_od_w	:= vl_dc_re_od_w;
	ds_eixo_od_w	:= vl_eixo_re_od_w;
	ds_graue_oe_w	:= vl_de_re_oe_w;
	ds_grauc_oe_w	:= vl_dc_re_oe_w;
	ds_eixo_oe_w	:= vl_eixo_re_oe_w;
	end;
elsif	(ie_dinamica_w = 'S') then
	begin
	ds_graue_od_w	:= vl_de_rd_od_w;
	ds_grauc_od_w	:= vl_dc_rd_od_w;
	ds_eixo_od_w	:= vl_eixo_rd_od_w;
	ds_graue_oe_w	:= vl_de_rd_oe_w;
	ds_grauc_oe_w	:= vl_dc_rd_oe_w;
	ds_eixo_oe_w	:= vl_eixo_rd_oe_w;
	end;
elsif	(ie_dinamica_w = 'N') then
	begin
	ds_graue_od_w	:= vl_soma_de_rd_od_w;
	ds_grauc_od_w	:= vl_soma_dc_rd_od_w;
	ds_eixo_od_w	:= vl_soma_eixo_od_w;
	ds_graue_oe_w	:= vl_soma_de_rd_oe_w;
	ds_grauc_oe_w	:= vl_soma_dc_rd_oe_w;
	ds_eixo_oe_w	:= vl_soma_eixo_oe_w;
	end;
end if;

/* Verifica se o campo n�o foi preenchido com valor num�rico v�lido */
if	((ds_graue_od_w = '-') or (ds_graue_od_w = '+')) then
	ds_graue_od_w	:= ' ';
end if;
if	((ds_grauc_od_w = '-') or (ds_grauc_od_w = '+')) then
	ds_grauc_od_w	:= ' ';
end if;
if	((ds_graue_oe_w = '-') or (ds_graue_oe_w = '+')) then
	ds_graue_oe_w	:= ' ';
end if;
if	((ds_grauc_oe_w = '-') or (ds_grauc_oe_w = '+')) then
	ds_grauc_oe_w	:= ' ';
end if; 

	
ds_tabela_w	:= '{\rtf1\ansi\ansicpg1252\deff0{\fonttbl{\f0\fswiss\fcharset0 Arial;}{\f1\fnil\fcharset0 Calibri;}}
{\colortbl ;\red0\green0\blue0;}
{\*\generator Msftedit 5.41.21.2510;}\viewkind4\uc1\pard\sa200\sl276\slmult1\lang22\f0\fs24\par
\trowd\trgaph10\trleft-30\trpaddl10\trpaddr10\trpaddfl3\trpaddfr3
\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx1746\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx2907\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx4052\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx5188\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx6207\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx7225\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrr\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx8338\pard\intbl\qc\lang1023\b\fs22 PARA LONGE\cell Esf�rico\cell Cil�ndrico\cell Eixo\cell Prisma\cell Base\cell DNP\cell\row\trowd\trgaph10\trleft-30\trpaddl10\trpaddr10\trpaddfl3\trpaddfr3
\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx1746\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx2907\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx4052\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx5188\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx6207\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx7225\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrr\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx8338\pard\intbl\qc OD\cell\b0 @ODEL\cell @ODCL\cell @ODXL\cell\cell\cell\cell\row\trowd\trgaph10\trleft-30\trpaddl10\trpaddr10\trpaddfl3\trpaddfr3
\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx1746\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx2907\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx4052\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx5188\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx6207\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx7225\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrr\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx8338\pard\intbl\qc\b OE\cell\b0 @OEEL\cell @OECL\cell @OEXL\cell\cell\cell\cell\row\pard\sa200\sl276\slmult1\lang22\par
\trowd\trgaph10\trleft-30\trpaddl10\trpaddr10\trpaddfl3\trpaddfr3
\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx1746\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx2899\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx4059\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx5188\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx6207\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx7233\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrt\brdrw1\brdrs\brdrcf1\clbrdrr\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx8338\pard\intbl\qc\lang1023\b PARA PERTO\cell Esf�rico\cell Cil�ndrico\cell Eixo\cell Prisma\cell Base\cell DNP\cell\row\trowd\trgaph10\trleft-30\trpaddl10\trpaddr10\trpaddfl3\trpaddfr3
\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx1746\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx2899\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx4059\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx5188\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx6207\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx7233\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrr\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx8338\pard\intbl\qc OD\cell\b0 @ODEP\cell @ODCP\cell @ODXP\cell\cell\cell\cell\row\trowd\trgaph10\trleft-30\trpaddl10\trpaddr10\trpaddfl3\trpaddfr3
\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx1746\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx2899\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx4059\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx5188\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx6207\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx7233\clbrdrl\brdrw1\brdrs\brdrcf1\clbrdrr\brdrw1\brdrs\brdrcf1\clbrdrb\brdrw1\brdrs\brdrcf1 \cellx8338\pard\intbl\qc\b OE\cell\b0 @OEEP\cell @OECP\cell @OEXP\cell\cell\cell\cell\row\pard\sa200\sl276\slmult1\lang22\par
\pard\lang1046\fs24
\pard\sa200\sl276\slmult1\lang22\f1\fs22';

ds_receita_w	:= ds_receita_w || ds_tabela_w;


ds_receita_w	:= ds_receita_w;


/*LONGE */
if (ie_somente_perto_w = 'N')then
   begin
   if 	(vl_de_receita_od_w > 0) then
      ds_receita_w	:= replace(ds_receita_w,'@ODEL','+'||trim(to_char(vl_de_receita_od_w,'90.99')));
   else
      ds_receita_w	:= replace(ds_receita_w,'@ODEL',trim(to_char(vl_de_receita_od_w,'90.99')));
   end if;

   if 	(vl_dc_receita_od_W > 0) then
      ds_receita_w	:= replace(ds_receita_w,'@ODCL','+'||trim(to_char(vl_dc_receita_od_W,'90.99')));
   else
      ds_receita_w	:= replace(ds_receita_w,'@ODCL',to_char(vl_dc_receita_od_W,'90.99'));
   end if;

   if	(VL_EIXO_RECEITA_OD_w is not null) then
      ds_receita_w	:= replace(ds_receita_w,'@ODXL',VL_EIXO_RECEITA_OD_w||'�');
   else	
      ds_receita_w	:= replace(ds_receita_w,'@ODXL','');
   end if;

   if 	(VL_DE_RECEITA_OE_w > 0) then
      ds_receita_w	:= replace(ds_receita_w,'@OEEL','+'||trim(to_char(VL_DE_RECEITA_OE_w,'90.99')));
   else
      ds_receita_w	:= replace(ds_receita_w,'@OEEL',trim(to_char(VL_DE_RECEITA_OE_w,'90.99')));
   end if;
   ds_receita_w	:= replace(ds_receita_w,'@OECL',to_char(VL_DC_RECEITA_OE_W,'90.99'));

   if	(VL_EIXO_RECEITA_OE_W is not null) then
      ds_receita_w	:= replace(ds_receita_w,'@OEXL',VL_EIXO_RECEITA_OE_W||'�');
   else
      ds_receita_w	:= replace(ds_receita_w,'@OEXL','');
   end if;
   end;
else
	ds_receita_w	:= replace(ds_receita_w,'@ODEL','');
	ds_receita_w	:= replace(ds_receita_w,'@ODCL','');
	ds_receita_w	:= replace(ds_receita_w,'@ODXL','');
	ds_receita_w	:= replace(ds_receita_w,'@OEEL','');
   ds_receita_w	:= replace(ds_receita_w,'@OEEL','');
   ds_receita_w	:= replace(ds_receita_w,'@OECL','');
	ds_receita_w	:= replace(ds_receita_w,'@OEXL','');
end if;






/*PERTO*/
if 	((vl_de_receita_od_w + vl_adicao_od_w) > 0) then
	vl_de_receita_od_w := '+'||trim(to_char((vl_de_receita_od_w + vl_adicao_od_w),'90.99'));
else
	vl_de_receita_od_w := trim(to_char((vl_de_receita_od_w + vl_adicao_od_w),'90.99'));
end if;
ds_receita_w	:= replace(ds_receita_w,'@ODEP',vl_de_receita_od_w);

if	((vl_adicao_od_w is not null) or (vl_adicao_oe_w is not null)) then
	if (vl_dc_receita_od_W > 0) then
	ds_receita_w	:= replace(ds_receita_w,'@ODCP','+'||trim(to_char(vl_dc_receita_od_W,'90.99')));
	else
	ds_receita_w	:= replace(ds_receita_w,'@ODCP',to_char(vl_dc_receita_od_W,'90.99'));
	end if;
	if	(VL_EIXO_RECEITA_OD_w is not null) then
		ds_receita_w	:= replace(ds_receita_w,'@ODXP',VL_EIXO_RECEITA_OD_w||'�');
	else
		ds_receita_w	:= replace(ds_receita_w,'@ODXP','');
	end if;
else
ds_receita_w	:= replace(ds_receita_w,'@ODCP','');
ds_receita_w	:= replace(ds_receita_w,'@ODXP','');
end if;

if 	((vl_de_receita_oe_w + vl_adicao_oe_w) > 0) then
	vl_de_receita_oe_w := '+'||trim(to_char(vl_de_receita_oe_w + vl_adicao_oe_w,'90.99'));
else
	vl_de_receita_oe_w :=trim(to_char (vl_de_receita_oe_w + vl_adicao_oe_w,'90.99'));
end if;
ds_receita_w	:= replace(ds_receita_w,'@OEEP',VL_DE_RECEITA_OE_w);

if	((vl_adicao_od_w is not null) or (vl_adicao_oe_w is not null)) then
	if (VL_DC_RECEITA_OE_W > 0)then
	ds_receita_w	:= replace(ds_receita_w,'@OECP','+'||trim(to_char(VL_DC_RECEITA_OE_W,'90.99')));
	else
	ds_receita_w	:= replace(ds_receita_w,'@OECP',to_char(VL_DC_RECEITA_OE_W,'90.99'));
	end if;	
	if	(VL_EIXO_RECEITA_OE_W is not null) then
	ds_receita_w	:= replace(ds_receita_w,'@OEXP',VL_EIXO_RECEITA_OE_W||'�');	
	else
	ds_receita_w	:= replace(ds_receita_w,'@OEXP','');
	end if;
else
ds_receita_w	:= replace(ds_receita_w,'@OECP','');
ds_receita_w	:= replace(ds_receita_w,'@OEXP','');
end if;
ds_linha_w := '  _____________________________________________________________________________________________________________';


ds_tabela_w	:= '\par Favor conferir seus �culos antes de us�-los. '||
		   'Ao usar �culos pela primeira vez ou nas altera��es de lentes � normal haver certo desconforto com n�useas, tonturas leves, percep��o de curvas e algumas dificuldades de orienta��o quanto ao solo. '||
		   'Trata-se de um per�odo de adapta��o que � passageiro. '||
		   'Se esses sintomas persistirem por duas ou tr�s semanas, volte a procurar seu m�dico. ';


ds_receita_w	:= ds_receita_w ||wheb_rtf_pck.get_negrito(true)|| '\fs17 Observa��o: \ul'||wheb_rtf_pck.get_negrito(false)||ds_obs_receita_w||'\ulnone \par'
||chr(10)||ds_linha_w||chr(10)||ds_linha_w||chr(10)||ds_linha_w||chr(10)||ds_linha_w;




if	(ie_gerar_observacao_padrao_w	= 'S') then
	ds_receita_w	:= ds_receita_w || ds_tabela_w;
end if;

ds_receita_w	:= ds_receita_w || '}';

select 	med_receita_seq.nextval
into	nr_sequencia_w
from	dual;

insert into med_receita(
	nr_sequencia,
	dt_atualizacao,
	ds_receita,
	nm_usuario,
	dt_receita,
	nr_atendimento_hosp,
	cd_pessoa_fisica,
	cd_medico,
	ie_tipo_receita,
	ie_situacao)
values(	nr_sequencia_w,
	sysdate,
	ds_receita_w,
	nm_usuario_p,
	sysdate,
	nr_atendimento_w,
	cd_pessoa_fisica_w,
	cd_medico_w,
	'C',
	'A');
	
commit;

end HOMG_Gerar_Receita_Oculos_HOSP;
/