CREATE OR REPLACE
PROCEDURE Gera_Valores_Medicamento_CCIH
                         (NR_FICHA_OCORRENCIA_P     NUMBER) IS

nr_atendimento_w		numeric(10,0);
cd_medicamento_w		numeric(10,0);
nr_dias_util_total_w	numeric(3,0);
nr_dias_utilizacao_w	numeric(3,0);
nr_medic_count_w		numeric(3,0);
nr_medic_item_w		numeric(3,0);
nr_seq_medicamento_w	numeric(10,0);
vl_material_w		numeric(15,2);
vl_material_aux_w		numeric(15,2);
vl_diferenca_w		numeric(15,2);

CURSOR C01 IS
	select 
		cd_medicamento,
      	(sum(dt_final_utilizacao - dt_inicial_utilizacao) + 1) 		nr_dias_utilizacao,
		nr_atendimento
	from cih_ficha_ocorrencia b, cih_medic_utilizado a
	where a.nr_ficha_ocorrencia = nr_ficha_ocorrencia_p
	  and a.nr_ficha_ocorrencia = b.nr_ficha_ocorrencia
	group by cd_medicamento,
               nr_atendimento;

CURSOR C02 IS
	select nr_seq_medicamento,
        	((dt_final_utilizacao - dt_inicial_utilizacao) + 1) nr_dias_utilizacao
	from cih_medic_utilizado
	where nr_ficha_ocorrencia = nr_ficha_ocorrencia_p
	  and cd_medicamento      = cd_medicamento_w
	order by 1;

BEGIN

OPEN C01;
LOOP
  	FETCH C01 into  	cd_medicamento_w,
			      nr_dias_util_total_w,
				nr_atendimento_w;
  	exit when C01%notfound;
  	begin
  	vl_material_aux_w 	:= 0;
  	vl_diferenca_w 		:= 0;
  	nr_medic_item_w 		:= 0;

	select 	/*+ INDEX(A MATPACI_I1) */
			nvl(sum(a.vl_material),0)
    	into vl_material_w
  	from 	material b,
      	material_atend_paciente a
  	where a.cd_material 	= b.cd_material
    	  and a.nr_atendimento 	= nr_atendimento_w
    	  and b.cd_medicamento 	= cd_medicamento_w;

	select count(*)
    	into nr_medic_count_w
  	from cih_medic_utilizado
  	where nr_ficha_ocorrencia = nr_ficha_ocorrencia_p
    	  and cd_medicamento = cd_medicamento_w;
  	OPEN C02;
  	LOOP
    	FETCH C02 into 	nr_seq_medicamento_w,
				nr_dias_utilizacao_w;
    	exit when C02%notfound;
    		begin
    		nr_medic_item_w := nr_medic_item_w + 1;
    		vl_material_aux_w := vl_material_aux_w + ((vl_material_w / 				nr_dias_util_total_w) * nr_dias_utilizacao_w);
    		if 	(nr_medic_item_w = nr_medic_count_w) then
			vl_diferenca_w := (vl_material_w - vl_material_aux_w);
    		end if;
    		update cih_medic_utilizado
    		set vl_medicamento = 	(((vl_material_w / nr_dias_util_total_w) * 						nr_dias_utilizacao_w) + vl_diferenca_w)
    		where nr_ficha_ocorrencia = nr_ficha_ocorrencia_p
		and nr_seq_medicamento = nr_seq_medicamento_w;
        	end;
  	END LOOP;
  	CLOSE C02;
  	end;
END LOOP;
CLOSE C01;
commit;	
END Gera_Valores_Medicamento_CCIH;
/