create or replace
procedure ctb_vincular_evento_oper( 	cd_operacao_estoque_p	number,
										cd_tipo_lote_contabil_p	number,
										cd_evento_p				number,
										ie_operacao_p			varchar2,
										nm_usuario_p			varchar2) is 



begin

if (nvl(ie_operacao_p, 'X') = 'D') then
	begin
	
	update 	operacao_estoque set 	cd_evento 				= null,
									cd_tipo_lote_contabil 	= null,
									dt_atualizacao 			= sysdate,
									nm_usuario 				= nm_usuario_p
	where	cd_operacao_estoque = cd_operacao_estoque_p;
	
	end;
elsif (nvl(ie_operacao_p, 'X') = 'V') then
	begin
	
	update 	operacao_estoque set 	cd_evento 				= cd_evento_p,
									cd_tipo_lote_contabil 	= cd_tipo_lote_contabil_p,
									dt_atualizacao 			= sysdate,
									nm_usuario 				= nm_usuario_p
	where	cd_operacao_estoque = cd_operacao_estoque_p;
	
	end;
end if;


commit;

end ctb_vincular_evento_oper;
/