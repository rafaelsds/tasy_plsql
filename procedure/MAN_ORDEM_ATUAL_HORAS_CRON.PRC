create or replace
procedure man_ordem_atual_horas_cron (
			nr_seq_ativ_os_p	number,
			nr_seq_ativ_proj_p	number,
			nm_usuario_p		varchar2) is
	
qt_hora_ativ_os_w	number(15,2);
qt_hora_prev_w		number(15,2);
qt_hora_real_w		number(15,2);
qt_hora_saldo_w		number(15,2);
pr_etapa_w		number(15,2) := 0;
nr_seq_atividade_w	number(10,0);
nr_seq_cronograma_w	number(10,0);
ie_status_etapa_w	Varchar2(1);
nr_seq_ordem_serv_w	number(10);
begin
if	(nr_seq_ativ_proj_p is not null) then
	begin					
	SELECT	dividir(NVL(SUM(qt_minuto),0),60)
	INTO	qt_hora_real_w
	FROM	man_ordem_serv_ativ a
	WHERE	EXISTS (SELECT 1 FROM man_ordem_servico x WHERE a.nr_seq_ordem_serv = x.nr_sequencia AND x.nr_seq_proj_cron_etapa = nr_seq_ativ_proj_p);
	
	select	nvl(sum(qt_hora_prev),0),		
		nvl(sum(qt_hora_saldo),0),
		nvl(max(nr_seq_superior),0),
		nvl(max(nr_seq_cronograma),0),
		decode(nvl(max(ie_status_etapa),'0'),'4','4','3')
	into	qt_hora_prev_w,		
		qt_hora_saldo_w,
		nr_seq_atividade_w,
		nr_seq_cronograma_w,
		ie_status_etapa_w
	from	proj_cron_etapa
	where	nr_sequencia = nr_seq_ativ_proj_p;
		
	qt_hora_saldo_w	:= qt_hora_prev_w - qt_hora_real_w;
	pr_etapa_w	:= obter_porc_utilizado(qt_hora_prev_w, qt_hora_real_w);
		
	update	proj_cron_etapa
	set	qt_hora_real = qt_hora_real_w,
		qt_hora_saldo = qt_hora_saldo_w,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate,
		ie_status_etapa = 2
	where	nr_sequencia = nr_seq_ativ_proj_p;
	commit;
	atualizar_horas_etapa_cron(nr_seq_ativ_proj_p);
	atualizar_horas_etapa_cron(nr_seq_atividade_w);	
	atualizar_total_horas_cron(nr_seq_cronograma_w);
	
	gerar_hist_ativ_os_etapa_cron(nr_seq_ativ_os_p, nr_seq_ativ_proj_p, nm_usuario_p);
	end;
end if;
commit;
end man_ordem_atual_horas_cron;
/
