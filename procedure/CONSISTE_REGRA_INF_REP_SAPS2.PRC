create or replace 
procedure consiste_regra_inf_rep_saps2(
				nr_atendimento_p 	number,
				cd_pessoa_fisica_p 	number,
				nr_seq_regra_p		number,
				ie_inconsistente_p 	out boolean,
				ie_informacao_w_p 	out varchar2,
				ie_escala_indice_p 	out varchar2,
				ie_informacao_p 	varchar2,
				quebra_p 			varchar2,
				ds_mensag_p 		varchar2,
				ds_mensagem_p 		out varchar2) is
qt_registros_w number(10);
qt_mov_uti_w number(10);
cd_classif_setor_old_w number(10);
cd_setor_atendimento_w number(10);
cd_classif_setor_w number(10);
dt_criacao_regra_w date;

cursor c01 is
	select 		a.cd_setor_atendimento, 
				b.cd_classif_setor
	from		atend_paciente_unidade a,
				setor_atendimento b					
	where 		a.nr_atendimento = nr_atendimento_p
	and			a.cd_setor_atendimento = b.cd_setor_atendimento
	and 		ie_passagem_setor = 'N'
	and			a.dt_entrada_unidade >= dt_criacao_regra_w
	order by 	a.nr_sequencia;

begin
	select	count(*)
	into	qt_registros_w  -- QUANTIDADE DE REGISTRO SAPS
	from	atendimento_paciente b,
			pe_prescricao a
	where 	a.nr_atendimento = b.nr_atendimento
	and 	b.nr_atendimento = nvl(nr_atendimento_p,b.nr_atendimento)
	and		b.cd_pessoa_fisica = cd_pessoa_fisica_p
	and		dt_liberacao is not null
	and		dt_inativacao is null
	and		a.ie_tipo = 'SAPS2';					
		
	select	max(dt_atualizacao_nrec)
	into	dt_criacao_regra_w
	from	regra_consiste_inf_rep
	where 	nr_sequencia = nr_seq_regra_p;
		
	qt_mov_uti_w := 0;
	cd_classif_setor_old_w := -1;
	
	open c01;
	loop
		fetch c01 into 
			cd_setor_atendimento_w,
			cd_classif_setor_w;
		exit when c01%notfound;
			begin
				if 	(cd_classif_setor_old_w <> cd_classif_setor_w and cd_classif_setor_w = 4) then
					qt_mov_uti_w := qt_mov_uti_w + 1;
				end if;
				
				cd_classif_setor_old_w := cd_classif_setor_w;
			end;
	end loop;
	close c01;

	if	(qt_registros_w = '0' or qt_mov_uti_w > qt_registros_w) then 
		select	count(*)
		into	qt_registros_w
		from	escala_saps3
		where	nr_atendimento = nr_atendimento_p
		and		dt_liberacao is not null
		and		dt_inativacao is null;

		if	(qt_registros_w	= '0' or qt_mov_uti_w > qt_registros_w) then
				if (ds_mensag_p is not null) then
					ds_mensagem_p := ds_mensag_p || quebra_p;
				else
					ds_mensagem_p := ds_mensagem_p || wheb_mensagem_pck.get_texto(306664, null) || quebra_p; -- N�O FOI CADASTRADA A ESCALA SAPS III PARA ESTE ATENDIMENTO.
				end if;
			ie_inconsistente_p := true;
			ie_informacao_w_p := ie_informacao_p;
			ie_escala_indice_p := 'SAPS2';
		end if;
	end if;
end;
/