create or replace
procedure match_patient_trend_upload as

nr_sequencia_w  W_ATEND_VS_INTEGR_HL7.NR_SEQUENCIA%type;
ds_query_w	VARCHAR2(8000);
ds_execute_start_w VARCHAR2(8000);
ds_execute_end_w VARCHAR2(8000);
ds_tables_w	VARCHAR2(8000);
ds_where_w	VARCHAR2(8000);
ds_sep_bv_w     VARCHAR2(30)    := obter_separador_bv;

ds_value_w  varchar2(2000);
ds_parametros_w VARCHAR2(8000);

NR_PRONT_EXT_W				varchar2(1000);
ACCOUNT_NUMBER_EXT_W				varchar2(1000);
NR_CNH_W				varchar2(1000);
HEALTH_CARD_EXT_W				varchar2(1000);
MEDICAID_NUMBER_EXT_W				varchar2(1000);
MEDICARE_NUMBER_EXT_W				varchar2(1000);
CD_PESSOA_FISICA_W				varchar2(1000);
CD_PESSOA_IDENTIFIER_EXT_W				varchar2(1000);
CD_RFC_W				varchar2(1000);
VISIT_NUMBER_EXT_W				varchar2(1000);
CD_PESSOA_FISICA_EXTERNO_W				varchar2(1000);
ALTERNATE_PATIENT_NUM_EXT_W				varchar2(1000);
ACCOUNT_NUMBER_EXT_2_W				varchar2(1000);
CD_RFC_2_W				varchar2(1000);
NR_CNH_2_W				varchar2(1000);
PREADMIT_NUMBER_EXT_W				varchar2(1000);
VISIT_NUMBER_EXT_2_W				varchar2(1000);
DS_GIVEN_NAME_W				varchar2(1000);
DS_MIDDLE_NAME_W				varchar2(1000);
DS_FAMILY_NAME_W				varchar2(1000);
DT_NASCIMENTO_W				date;
IE_SEXO_W				varchar2(1000);
NR_PRONT_EXT_2_W				varchar2(1000);
TEMP_ENCOUNTERID_EXT_W				varchar2(1000);
CD_PESSOA_ALT_EXT_W				varchar2(1000);

CD_PESSOA_FISICA_TASY_W  PESSOA_FISICA.CD_PESSOA_FISICA%type;
NR_ATENDIMENTO_W    W_ATEND_VS_INTEGR_HL7.NR_ATENDIMENTO%type;

cursor C01 is
select * from W_ATEND_VS_INTEGR_HL7
where nr_atendimento is null
order by nr_sequencia desc;

  PROCEDURE cleanVars IS
  BEGIN
    nr_sequencia_w  := null;
    ds_query_w := '';
    ds_tables_w := '';
    ds_where_w := '';
    ds_value_w	:= null;
    NR_PRONT_EXT_W	:= null;
    ACCOUNT_NUMBER_EXT_W	:= null;
    NR_CNH_W	:= null;
    HEALTH_CARD_EXT_W	:= null;
    MEDICAID_NUMBER_EXT_W	:= null;
    MEDICARE_NUMBER_EXT_W	:= null;
    CD_PESSOA_FISICA_W	:= null;
    CD_PESSOA_IDENTIFIER_EXT_W	:= null;
    CD_RFC_W	:= null;
    VISIT_NUMBER_EXT_W	:= null;
    CD_PESSOA_FISICA_EXTERNO_W	:= null;
    ALTERNATE_PATIENT_NUM_EXT_W	:= null;
    ACCOUNT_NUMBER_EXT_2_W	:= null;
    CD_RFC_2_W	:= null;
    NR_CNH_2_W	:= null;
    PREADMIT_NUMBER_EXT_W	:= null;
    VISIT_NUMBER_EXT_2_W	:= null;
    DS_GIVEN_NAME_W	:= null;
    DS_MIDDLE_NAME_W	:= null;
    DS_FAMILY_NAME_W	:= null;
    DT_NASCIMENTO_W	:= null;
    IE_SEXO_W	:= null;
    NR_PRONT_EXT_2_W	:= null;
    TEMP_ENCOUNTERID_EXT_W	:= null;
    CD_PESSOA_ALT_EXT_W	:= null;
    CD_PESSOA_FISICA_W	:= null;
    NR_ATENDIMENTO_W	:= null;
  END;

  FUNCTION getValuefOfSegment(trend_upload_w W_ATEND_VS_INTEGR_HL7%rowtype, ds_segment_w varchar2) return varchar2 is
  BEGIN
    case (ds_segment_w) 
      when 'PATIENT_ID' then
        return trend_upload_w.DS_PATIENT_EXT_ID;
      when 'PATIENT_IDENTIFIER_ID' then
        return trend_upload_w.DS_PATIENT_INT_ID;
      when 'ALTERNATE_PATIENT_ID' then
        return trend_upload_w.DS_PATIENT_ALT_ID;
      when 'PATIENT_ACCOUNT_NUMBER' then
        return trend_upload_w.DS_PATIENT_ACCOUNT_NUMBER;
      when 'SSN_NUMBER' then
        return trend_upload_w.DS_SSN_NUMBER;
      when 'DRIVERS_LICENSE_NUMBER' then
        return trend_upload_w.DS_DRIVER_LICENSE_NUMBER;
      when 'PREADMIT_NUMBER' then
        return trend_upload_w.DS_PRE_ADMIT_NUMBER;
      when 'VISIT_NUMBER' then
        return trend_upload_w.DS_VISIT_NUMBER;
    end case;
  END;

  PROCEDURE appendWithPrefixWhenNonEmpty(ds_comando_w in out nocopy varchar2, ds_value_w varchar2, ds_prefix_w varchar2) IS
  BEGIN
    if (nvl(instr(ds_comando_w, ds_value_w),0) = 0) then
      if (length(ds_comando_w) > 0) then
        ds_comando_w := ds_comando_w || ds_prefix_w || ds_value_w;
      else
        ds_comando_w := ds_value_w;
      end if;
    end if;
  END;

  PROCEDURE buildQueryOfIdentifierType(ds_segment_type_w varchar2, ds_value_w varchar2, 
    ds_tables_w in out nocopy varchar2, ds_where_w in out nocopy varchar2, ds_parametros_w in out nocopy varchar2) IS
  BEGIN
    case (ds_segment_type_w)
      when 'MEDICAL_RECORD_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PESSOA_FISICA PF', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PF.NR_PRONT_EXT = NR_PRONT_EXT_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'NR_PRONT_EXT_W, ';
        NR_PRONT_EXT_W := ds_value_w;        
      when 'ACCOUNT_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.ACCOUNT_NUMBER_EXT = ACCOUNT_NUMBER_EXT_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'ACCOUNT_NUMBER_EXT_W, ';
        ACCOUNT_NUMBER_EXT_W := ds_value_w;        
      when 'DRIVERS_LICENSE_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PESSOA_FISICA PF', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PF.NR_CNH = NR_CNH_W', ' AND');
        NR_CNH_W := ds_value_w;        
      when 'HEALTH_CARD_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.HEALTH_CARD_EXT = HEALTH_CARD_EXT_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'HEALTH_CARD_EXT_W, ';
        HEALTH_CARD_EXT_W := ds_value_w;        
      when 'MEDICAID_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.MEDICAID_NUMBER_EXT = MEDICAID_NUMBER_EXT_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'MEDICAID_NUMBER_EXT_W, ';
        MEDICAID_NUMBER_EXT_W := ds_value_w;        
      when 'MEDICARE_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.MEDICARE_NUMBER_EXT = MEDICARE_NUMBER_EXT_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'MEDICARE_NUMBER_EXT_W, ';
        MEDICARE_NUMBER_EXT_W := ds_value_w;
      when 'PATIENT_INTERNAL_IDENTIFIER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.CD_PESSOA_FISICA = CD_PESSOA_FISICA_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'CD_PESSOA_FISICA_W, ';
        CD_PESSOA_FISICA_W := ds_value_w;
      when 'PATIENT_EXTERNAL_IDENTIFIER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.CD_PESSOA_IDENTIFIER_EXT = CD_PESSOA_IDENTIFIER_EXT_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'CD_PESSOA_IDENTIFIER_EXT_W, ';
        CD_PESSOA_IDENTIFIER_EXT_W := ds_value_w;        
      when 'SOCIAL_SECURITY_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PESSOA_FISICA PF', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PF.CD_RFC = CD_RFC_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'CD_RFC_W, ';
        CD_RFC_W := ds_value_w;        
      when 'VISIT_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.VISIT_NUMBER_EXT = VISIT_NUMBER_EXT_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'VISIT_NUMBER_EXT_W, ';
        VISIT_NUMBER_EXT_W := ds_value_w;        
    end case;
  END;

  PROCEDURE buildQueryOfSegment(ds_segment_w varchar2, ds_segment_type_w varchar2, ds_value_w varchar2, 
    ds_tables_w in out nocopy varchar2, ds_where_w in out nocopy varchar2, ds_parametros_w in out nocopy varchar2) IS
  BEGIN
    case (ds_segment_w) 
      when 'PATIENT_ID' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.CD_PESSOA_FISICA_EXTERNO = CD_PESSOA_FISICA_EXTERNO_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'CD_PESSOA_FISICA_EXTERNO_W, ';
        CD_PESSOA_FISICA_EXTERNO_W := ds_value_w;
      when 'PATIENT_IDENTIFIER_ID' then
        buildQueryOfIdentifierType(ds_segment_type_w, ds_value_w, ds_tables_w, ds_where_w, ds_parametros_w);
      when 'ALTERNATE_PATIENT_ID' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.ALTERNATE_PATIENT_NUM_EXT = ALTERNATE_PATIENT_NUM_EXT_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'ALTERNATE_PATIENT_NUM_EXT_W, ';
        ALTERNATE_PATIENT_NUM_EXT_W := ds_value_w;
      when 'PATIENT_ACCOUNT_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.ACCOUNT_NUMBER_EXT = ACCOUNT_NUMBER_EXT_2_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'ACCOUNT_NUMBER_EXT_2_W, ';
        ACCOUNT_NUMBER_EXT_2_W := ds_value_w; 
      when 'SSN_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PESSOA_FISICA PF', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PF.CD_RFC = CD_RFC_2_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'CD_RFC_2_W, ';
        CD_RFC_2_W := ds_value_w;
      when 'DRIVERS_LICENSE_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PESSOA_FISICA PF', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PF.NR_CNH = NR_CNH_2_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'NR_CNH_2_W, ';
        NR_CNH_2_W := ds_value_w;
      when 'PREADMIT_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.PREADMIT_NUMBER_EXT = PREADMIT_NUMBER_EXT_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'PREADMIT_NUMBER_EXT_W, ';
        PREADMIT_NUMBER_EXT_W := ds_value_w;
      when 'VISIT_NUMBER' then
        appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.VISIT_NUMBER_EXT = VISIT_NUMBER_EXT_2_W', ' AND');
        ds_parametros_w := ds_parametros_w || 'VISIT_NUMBER_EXT_2_W, ';
        VISIT_NUMBER_EXT_2_W := ds_value_w;
    end case;
  END;

  PROCEDURE buildJoinTables(ds_tables_w in out nocopy varchar2, ds_where_w in out nocopy varchar2) IS
  BEGIN
    if (nvl(instr(ds_tables_w, 'PESSOA_FISICA PF'),0) > 0
      and nvl(instr(ds_tables_w, 'PF_CODIGO_EXTERNO PFE'),0) > 0) then
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PF.CD_PESSOA_FISICA = PFE.CD_PESSOA_FISICA', ' AND');
    end if;

    if (nvl(instr(ds_tables_w, 'PESSOA_FISICA PF'),0) > 0
      and nvl(instr(ds_tables_w, 'PERSON_NAME PN'),0) > 0) then
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PF.NR_SEQ_PERSON_NAME = PN.NR_SEQUENCIA', ' AND');
        appendWithPrefixWhenNonEmpty(ds_where_w, ' PN.DS_TYPE = ''main''', ' AND');
    end if;
  END;

  PROCEDURE buildQueryWithBasicPatientInf(trend_upload_w W_ATEND_VS_INTEGR_HL7%rowtype,
    ds_tables_w in out nocopy varchar2, ds_where_w in out nocopy varchar2, ds_parametros_w in out nocopy varchar2) IS
  BEGIN
    appendWithPrefixWhenNonEmpty(ds_tables_w, 'PESSOA_FISICA PF', ', ');
    appendWithPrefixWhenNonEmpty(ds_tables_w, 'PERSON_NAME PN', ', ');

    if (trend_upload_w.DS_GIVEN_NAME is not null) then
      appendWithPrefixWhenNonEmpty(ds_where_w, ' UPPER(PN.DS_GIVEN_NAME) = DS_GIVEN_NAME_W', ' AND');
      ds_parametros_w := ds_parametros_w || 'DS_GIVEN_NAME_W, ';
      DS_GIVEN_NAME_W := upper(trend_upload_w.DS_GIVEN_NAME);
    end if;
    if (trend_upload_w.DS_MIDDLE_NAME is not null) then
      appendWithPrefixWhenNonEmpty(ds_where_w, ' UPPER(PN.DS_COMPONENT_NAME_1) = DS_MIDDLE_NAME_W', ' AND');
      ds_parametros_w := ds_parametros_w || 'DS_MIDDLE_NAME_W, ';
      DS_MIDDLE_NAME_W := upper(trend_upload_w.DS_MIDDLE_NAME);
    end if;
    if (trend_upload_w.DS_FAMILY_NAME is not null) then
      appendWithPrefixWhenNonEmpty(ds_where_w, ' UPPER(PN.DS_FAMILY_NAME) = DS_FAMILY_NAME_W', ' AND');
      ds_parametros_w := ds_parametros_w || 'DS_FAMILY_NAME_W, ';
      DS_FAMILY_NAME_W := upper(trend_upload_w.DS_FAMILY_NAME);
    end if;
    if (trend_upload_w.DT_NASCIMENTO is not null) then
      appendWithPrefixWhenNonEmpty(ds_where_w, ' TRUNC(PF.DT_NASCIMENTO) = TRUNC(DT_NASCIMENTO_W)', ' AND');
      ds_parametros_w := ds_parametros_w || 'DT_NASCIMENTO_W, ';
      DT_NASCIMENTO_W := trend_upload_w.DT_NASCIMENTO;
    end if;
    if (trend_upload_w.IE_GENDER is not null) then
      appendWithPrefixWhenNonEmpty(ds_where_w, ' PF.IE_SEXO = IE_SEXO_W', ' AND');
      ds_parametros_w := ds_parametros_w || 'IE_SEXO_W, ';
      IE_SEXO_W := trend_upload_w.IE_GENDER;
    end if;
  END;

  PROCEDURE buildQuery(trend_upload_w W_ATEND_VS_INTEGR_HL7%rowtype, 
    ds_tables_w in out nocopy varchar2, 
    ds_where_w in out nocopy varchar2, 
    ds_parametros_w in out nocopy varchar2) IS
	BEGIN
    if (trend_upload_w.DS_LIFETIME_ID_SEG is not null) then
      ds_value_w := getValuefOfSegment(trend_upload_w, trend_upload_w.DS_LIFETIME_ID_SEG);
      appendWithPrefixWhenNonEmpty(ds_tables_w, 'PESSOA_FISICA PF', ', ');
      appendWithPrefixWhenNonEmpty(ds_where_w, ' PF.NR_PRONT_EXT = NR_PRONT_EXT_2_W', ' AND');
      NR_PRONT_EXT_2_W := ds_value_w;
      buildQueryOfSegment(trend_upload_w.DS_LIFETIME_ID_SEG, trend_upload_w.DS_ID_CODE_LFT, ds_value_w, ds_tables_w, ds_where_w, ds_parametros_w);
    end if;

    if (trend_upload_w.DS_ENCOUNTER_ID_SEG is not null) then
      ds_value_w := getValuefOfSegment(trend_upload_w, trend_upload_w.DS_ENCOUNTER_ID_SEG);
      appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
      appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.TEMP_ENCOUNTERID_EXT = TEMP_ENCOUNTERID_EXT_W', ' AND');
      appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.IE_ENCOUNTERID = ''A''', ' AND');
      TEMP_ENCOUNTERID_EXT_W := ds_value_w;
      buildQueryOfSegment(trend_upload_w.DS_ENCOUNTER_ID_SEG, trend_upload_w.DS_ID_CODE_EI, ds_value_w, ds_tables_w, ds_where_w, ds_parametros_w);
    end if;

    if (trend_upload_w.DS_ALTERNATE_ID_SEG is not null) then
      ds_value_w := getValuefOfSegment(trend_upload_w, trend_upload_w.DS_ALTERNATE_ID_SEG);
      appendWithPrefixWhenNonEmpty(ds_tables_w, 'PF_CODIGO_EXTERNO PFE', ', ');
      appendWithPrefixWhenNonEmpty(ds_where_w, ' PFE.CD_PESSOA_ALT_EXT = CD_PESSOA_ALT_EXT_W', ' AND');
      CD_PESSOA_ALT_EXT_W := ds_value_w;
      buildQueryOfSegment(trend_upload_w.DS_ALTERNATE_ID_SEG, trend_upload_w.DS_ID_CODE_AI, ds_value_w, ds_tables_w, ds_where_w, ds_parametros_w);

      if (trend_upload_w.DS_LIFETIME_ID_SEG is not null 
        and trend_upload_w.DS_ENCOUNTER_ID_SEG is not null) then
          buildQueryWithBasicPatientInf(trend_upload_w, ds_tables_w, ds_where_w, ds_parametros_w);
      end if;
    end if;

    buildJoinTables(ds_tables_w, ds_where_w);
	END;

begin

ds_execute_start_w := 'DECLARE

NR_PRONT_EXT_W				varchar2(1000) := :NR_PRONT_EXT_W;
ACCOUNT_NUMBER_EXT_W				varchar2(1000) := :ACCOUNT_NUMBER_EXT_W;
NR_CNH_W				varchar2(1000) := :NR_CNH_W;
HEALTH_CARD_EXT_W				varchar2(1000) := :HEALTH_CARD_EXT_W;
MEDICAID_NUMBER_EXT_W				varchar2(1000) := :MEDICAID_NUMBER_EXT_W;
MEDICARE_NUMBER_EXT_W				varchar2(1000) := :MEDICARE_NUMBER_EXT_W;
CD_PESSOA_FISICA_W				varchar2(1000) := :CD_PESSOA_FISICA_W;
CD_PESSOA_IDENTIFIER_EXT_W				varchar2(1000) := :CD_PESSOA_IDENTIFIER_EXT_W;
CD_RFC_W				varchar2(1000) := :CD_RFC_W;
VISIT_NUMBER_EXT_W				varchar2(1000) := :VISIT_NUMBER_EXT_W;
CD_PESSOA_FISICA_EXTERNO_W				varchar2(1000) := :CD_PESSOA_FISICA_EXTERNO_W;
ALTERNATE_PATIENT_NUM_EXT_W				varchar2(1000) := :ALTERNATE_PATIENT_NUM_EXT_W;
ACCOUNT_NUMBER_EXT_2_W				varchar2(1000) := :ACCOUNT_NUMBER_EXT_2_W;
CD_RFC_2_W				varchar2(1000) := :CD_RFC_2_W;
NR_CNH_2_W				varchar2(1000) := :NR_CNH_2_W;
PREADMIT_NUMBER_EXT_W				varchar2(1000) := :PREADMIT_NUMBER_EXT_W;
VISIT_NUMBER_EXT_2_W				varchar2(1000) := :VISIT_NUMBER_EXT_2_W;
DS_GIVEN_NAME_W				varchar2(1000) := :DS_GIVEN_NAME_W;
DS_MIDDLE_NAME_W				varchar2(1000) := :DS_MIDDLE_NAME_W;
DS_FAMILY_NAME_W				varchar2(1000) := :DS_FAMILY_NAME_W;
DT_NASCIMENTO_W				date := :DT_NASCIMENTO_W;
IE_SEXO_W				varchar2(1000) := :IE_SEXO_W;
NR_PRONT_EXT_2_W				varchar2(1000) := :NR_PRONT_EXT_2_W;
TEMP_ENCOUNTERID_EXT_W				varchar2(1000) := :TEMP_ENCOUNTERID_EXT_W;
CD_PESSOA_ALT_EXT_W				varchar2(1000) := :CD_PESSOA_ALT_EXT_W;

BEGIN

  SELECT MAX(CD_PESSOA_FISICA)
  INTO :CD_PESSOA_FISICA_TASY
  from (';

ds_execute_end_w := ') where rownum = 1
;

END;'
;

for c01_row in C01 loop
  cleanVars();

  nr_sequencia_w := c01_row.nr_sequencia;
  
  if (c01_row.cd_pessoa_fisica is null) then
    buildQuery(c01_row, ds_tables_w, ds_where_w, ds_parametros_w);
  
    if (nvl(instr(ds_tables_w, 'PESSOA_FISICA PF'),0) > 0) then
      ds_query_w := 'SELECT DISTINCT PF.CD_PESSOA_FISICA';
    else
      ds_query_w := 'SELECT DISTINCT PFE.CD_PESSOA_FISICA';
    end if;
  
    ds_query_w := ds_query_w || ' FROM ' || ds_tables_w || ' WHERE ' || ds_where_w;
  
    EXECUTE IMMEDIATE (ds_execute_start_w || ds_query_w || ds_execute_end_w)
  USING NR_PRONT_EXT_W, ACCOUNT_NUMBER_EXT_W, 
    NR_CNH_W, HEALTH_CARD_EXT_W, 
    MEDICAID_NUMBER_EXT_W, MEDICARE_NUMBER_EXT_W, 
    CD_PESSOA_FISICA_W, CD_PESSOA_IDENTIFIER_EXT_W, 
    CD_RFC_W, VISIT_NUMBER_EXT_W, 
    CD_PESSOA_FISICA_EXTERNO_W, ALTERNATE_PATIENT_NUM_EXT_W, 
    ACCOUNT_NUMBER_EXT_2_W, CD_RFC_2_W, 
    NR_CNH_2_W, PREADMIT_NUMBER_EXT_W, 
    VISIT_NUMBER_EXT_2_W, DS_GIVEN_NAME_W, 
    DS_MIDDLE_NAME_W, DS_FAMILY_NAME_W, 
    DT_NASCIMENTO_W, IE_SEXO_W, 
    NR_PRONT_EXT_2_W, TEMP_ENCOUNTERID_EXT_W, 
    CD_PESSOA_ALT_EXT_W, OUT CD_PESSOA_FISICA_TASY_W;
  else
    CD_PESSOA_FISICA_TASY_W := c01_row.cd_pessoa_fisica;
  end if;
  
  if (CD_PESSOA_FISICA_TASY_W is not null) then

    SELECT MAX(AP.NR_ATENDIMENTO) 
    INTO NR_ATENDIMENTO_W
    FROM ATENDIMENTO_PACIENTE AP 
    WHERE AP.CD_PESSOA_FISICA = CD_PESSOA_FISICA_TASY_W 
      AND AP.DT_ALTA IS NULL;

    update W_ATEND_VS_INTEGR_HL7 
      set CD_PESSOA_FISICA = CD_PESSOA_FISICA_TASY_W,
        NR_ATENDIMENTO = NR_ATENDIMENTO_W
      WHERE NR_SEQUENCIA = c01_row.NR_SEQUENCIA;
    commit;
  end if;

end loop;

end match_patient_trend_upload;
/
