create or replace 
procedure vipe_obter_item_composto (
		nr_prescricao_p		number,
		qt_item_p		number,
		nr_prescricoes_p	varchar2,		
		cd_item_p		varchar2,		
		ds_compostos_p		out varchar2) is
	
nr_prescr_w		number(10,0);
nr_seq_horario_w	number(10,0);
ds_compostos_w		varchar2(255);

begin

if	(nr_prescricao_p > 0) then
	nr_prescr_w	:= nr_prescricao_p;
else
	nr_prescr_w	:= Obter_Max_NrPrescricao(nr_prescricoes_p);
end if;

if	(nr_prescr_w > 0) then
	begin
	
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_horario_w
	from	prescr_mat_hor 
	where	cd_material	= cd_item_p
	and	nr_prescricao	= nr_prescr_w;
	--and	qt_dose		= qt_item_p;
	
	if	(nr_seq_horario_w > 0) then
		ds_compostos_w	:= substr(VIPE_obter_compostos(nr_seq_horario_w),1,255);
	end if;
	
	end;
end if;

ds_compostos_p	:= ds_compostos_w;

end vipe_obter_item_composto;
/