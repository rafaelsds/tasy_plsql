create or replace
procedure atualizar_medico_atend (nr_atendimento_p  	NUMBER,
				ie_atualizar_p 	out varchar2) IS

qt_reg_w 		NUMBER(1);
cd_medico_w 		prescr_medica.cd_medico%TYPE;
cd_medico_atual_w	atendimento_paciente.cd_medico_resp%type;

BEGIN

ie_atualizar_p := 'N';

SELECT  NVL(MAX(1),0),
 	MAX(cd_medico)
INTO  	qt_reg_w,
 	cd_medico_w
FROM  	prescr_medica
WHERE  	nr_atendimento = nr_atendimento_p;

IF	(qt_reg_w = 1) THEN
	select 	max(cd_medico_resp)
	into	cd_medico_atual_w
	from 	atendimento_paciente
	where 	nr_atendimento = nr_atendimento_p;

	if (cd_medico_atual_w <> cd_medico_w) then	
 		ie_atualizar_p := 'S';
		
		UPDATE  atendimento_paciente
 		SET cd_medico_resp = cd_medico_w
 		WHERE  nr_atendimento = nr_atendimento_p;

 		COMMIT;
	end if;
END IF;

end atualizar_medico_atend;
/