create or replace
procedure chamar_paciente_triagem(	
				nr_atendimento_p Varchar2,
				nm_usuario_p Varchar2 )	is

begin

if	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then
	
	
		update 	TRIAGEM_PRONTO_ATEND 
		set 	dt_chamada_painel = sysdate,
				nm_usuario     =  nm_usuario_p
		where 	nr_atendimento  = nr_atendimento_p;
		
		commit;
		
end if;


end chamar_paciente_triagem;
/