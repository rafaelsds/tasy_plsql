create or replace
procedure pls_oc_cta_tratar_val_71(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_selecao_ocor_cta.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 
 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Validar a��es sobre os itens na an�lise. Esta valida��o ir� verificar se o item sofreu 
alguma a��o de inclus�o ou substitui��o na an�lise de contas m�dicas.
	
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------

Altera��es:
------------------------------------------------------------------------------------------------------------------
jjkruk OS 781493
Altera��o: N�o usar cursor dinamico
Motivo:	Desempenho, nao � necess�rio ser dinamico.
------------------------------------------------------------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
ds_restr_inclusao_w		pls_conta_proc.ie_acao_analise%type;
ds_restr_substituicao_w		pls_conta_proc.ie_acao_analise%type;
dados_tb_selecao_w		pls_tipos_ocor_pck.dados_table_selecao_ocor;

cursor C01 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.ie_inclusao_item ie_inclusao_item,
		a.ie_substituicao_item ie_substituicao_item
	from	pls_oc_cta_val_manut_conta a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p;
	
cursor C02 (	nr_id_transacao_pc		pls_oc_cta_selecao_ocor_v.nr_sequencia%type,
		ds_restr_inclusao_pc		pls_conta_proc.ie_acao_analise%type,
		ds_restr_substituicao_pc	pls_conta_proc.ie_acao_analise%type) is
	select 		x.nr_sequencia,
			'S' ie_valido,
			'O item foi inclu�do ou substitu�do' ds_observacao
	from	pls_oc_cta_selecao_ocor_v	x,
		pls_conta_proc_ocor_v		a
	where	x.ie_valido		= 'S'
	and	x.nr_id_transacao	= nr_id_transacao_pc
	and	a.nr_sequencia = x.nr_seq_conta_proc
	and 	a.ie_acao_analise in (ds_restr_inclusao_pc, ds_restr_substituicao_pc) 
	union all
	select 		x.nr_sequencia,
			'S' ie_valido,
			'O item foi inclu�do ou substitu�do' ds_observacao
	from	pls_oc_cta_selecao_ocor_v	x,
		pls_conta_mat_ocor_v		a
	where	x.ie_valido		= 'S'
	and	x.nr_id_transacao	= nr_id_transacao_pc
	and	a.nr_sequencia = x.nr_seq_conta_mat
	and 	a.ie_acao_analise in (ds_restr_inclusao_pc, ds_restr_substituicao_pc);

begin
if	(dados_regra_p.nr_sequencia is not null)  then
	for r_C01_w in C01(dados_regra_p.nr_sequencia) loop
		if	(r_C01_w.ie_inclusao_item = 'S' or r_C01_w.ie_substituicao_item = 'S')	then
			if	(r_C01_w.ie_inclusao_item = 'S' and r_C01_w.ie_substituicao_item = 'S') then
				ds_restr_inclusao_w:= 'I';
				ds_restr_substituicao_w:= 'S';
			elsif	(r_C01_w.ie_inclusao_item = 'S') then
				ds_restr_inclusao_w:= 'I';
				ds_restr_substituicao_w:= 'I';
			elsif	(r_C01_w.ie_substituicao_item = 'S') then
				ds_restr_inclusao_w:= 'S';
				ds_restr_substituicao_w:= 'S';
			end if;
		
			begin
				pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_selecao_w);
				-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
				pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
				open c02(nr_id_transacao_p, ds_restr_inclusao_w, ds_restr_substituicao_w);
				loop
					fetch 	c02 bulk collect
					into	dados_tb_selecao_w.nr_seq_selecao,
						dados_tb_selecao_w.ie_valido,
						dados_tb_selecao_w.ds_observacao
					limit pls_util_cta_pck.qt_registro_transacao_w;
					exit when dados_tb_selecao_w.nr_seq_selecao.count = 0;

					pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_selecao_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
											'SEQ', dados_tb_selecao_w.ds_observacao, dados_tb_selecao_w.ie_valido, nm_usuario_p);

				end loop;
				close c02;
			exception
			when others then
				if (c02%isopen) then
					close c02;
				end if;
				pls_tipos_ocor_pck.trata_erro_sql_dinamico(dados_regra_p, sqlerrm, nr_id_transacao_p, nm_usuario_p);
			end;
			pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
		end if;
	end loop;
end if;
null;
end pls_oc_cta_tratar_val_71;
/