create or replace
procedure lib_inat_reg_funcao_macro(
			nm_usuario_p	varchar2,
			nr_sequencia_p  number,
			ie_opcao_p      varchar2,
			ds_justificativa_inat_p	varchar2) is 

begin
if (nr_sequencia_p is not null) and
   (ie_opcao_p is not null) and
   (ds_justificativa_inat_p is not null) then
   
	if	(ie_opcao_p = 'L') then
	
		update	funcao_macro
		set 	dt_liberacao 	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where 	nr_sequencia	= nr_sequencia_p;
	
	elsif   	(ie_opcao_p = 'I') then
	
		update	funcao_macro
		set 	dt_inativacao 	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate,
			ie_situacao 	= 'I',
			ds_justificativa 	= substr(ds_justificativa_inat_p,1,255)
		where 	nr_sequencia	= nr_sequencia_p;
		
	end if;
end if;

commit;

end lib_inat_reg_funcao_macro;
/