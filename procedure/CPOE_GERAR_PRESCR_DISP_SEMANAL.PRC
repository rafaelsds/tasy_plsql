create or replace
procedure cpoe_gerar_prescr_disp_semanal (	cd_setor_atendimento_p			in	cpoe_week_dispens_rule.cd_setor_atendimento%type,
											ie_via_aplicacao_p				in	cpoe_week_dispens_rule.ie_via_aplicacao%type,
											dt_reference_p					in	date,
											nr_atendimento_p				in	atendimento_paciente.nr_atendimento%type,
											cd_estabelecimento_p			in	estabelecimento.cd_estabelecimento%type,
											cd_perfil_p						in	perfil.cd_perfil%type,
											nm_usuario_p					in	usuario.nm_usuario%type,
											itens_liberar_p					in	varchar2,
											ie_inconsistencia_out_p			out varchar2,
											cd_pessoa_fisica_p				in	pessoa_fisica.cd_pessoa_fisica%type default null,
											ie_copia_diaria_p				in	char default 'N',
											ie_interv_farmacia_p			in	varchar2 default 'N',
											ie_motivo_prescr_p				in	varchar2 default null,
											ie_liberacao_ang_p				in	varchar2 default 'N',
											nr_prescricao_out_p				out number,
											nr_prescricao_lib_p				in	number default null,
											nr_seq_consulta_oft_p			in	number default null,
											nr_seq_pend_pac_acao_p			in	number default null,
											ie_copia_sem_lib_enf_farm_p		in	varchar2 default 'N',
											ie_adep_p						in	varchar2 default 'S',
											nm_usuario_validacao_p			in	varchar2 default null,
											ds_prescricoes_geradas_out_p   	out varchar2,
											nm_usuario_lib_enf_p			in	cpoe_material.nm_usuario_lib_enf%type	default null,
											cd_farmac_lib_p					in	cpoe_material.cd_farmac_lib%type	default null) is
	
nr_seq_rule_w		cpoe_week_dispens_rule.nr_sequencia%type;
hr_disp_time_w		cpoe_week_dispens_rule.hr_dispensation_time%type;
si_disp_day_w		cpoe_week_dispens_rule.si_dispensation_day%type;
dt_dispensation_w	date;
qt_dias_w			pls_integer;
dt_prescr_w			date;

begin

	cpoe_regra_disp_semanal(cd_setor_atendimento_p, ie_via_aplicacao_p, nr_seq_rule_w, hr_disp_time_w, si_disp_day_w);
	dt_dispensation_w := cpoe_prox_dt_disp_semanal(dt_reference_p, si_disp_day_w, hr_disp_time_w);
	
	if (dt_dispensation_w is not null) then
	
		qt_dias_w := pkg_date_utils.get_DiffDate(dt_reference_p, dt_dispensation_w,'DAY');
		
		dt_prescr_w := dt_dispensation_w - qt_dias_w;		
		
		while (dt_prescr_w < dt_dispensation_w)
		loop

		CPOE_GERAR_PRESCRICAO(nr_atendimento_p,
							  dt_reference_p,
							  cd_estabelecimento_p,
							  cd_perfil_p,
							  cd_setor_atendimento_p,
							  nm_usuario_p,
							  itens_liberar_p,
							  ie_inconsistencia_out_p,
							  cd_pessoa_fisica_p,
							  ie_copia_diaria_p,
							  ie_interv_farmacia_P,
							  ie_motivo_prescr_p,
							  ie_liberacao_ang_p,
							  nr_prescricao_out_p,
							  nr_prescricao_lib_p,
							  nr_seq_consulta_oft_p,
							  nr_seq_pend_pac_acao_p,
							  'N',
							  ie_adep_p,
							  nm_usuario_validacao_p,
							  ds_prescricoes_geradas_out_p,
							  nm_usuario_lib_enf_p,
							  cd_farmac_lib_p);		
		
		dt_prescr_w := dt_prescr_w +1;
	
		end loop;
	
	end if;
	
end cpoe_gerar_prescr_disp_semanal;
/
