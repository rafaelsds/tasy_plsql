create or replace procedure update_cpoe_date_proc( 
								ds_seq_proc_check_list_p			varchar2,
								ds_seq_proc_uncheck_list_p			varchar2,
								ie_option							varchar2) is 

begin

/* ie_option 
D - Interval differentiated by dates
W - Differentiated interval
*/

if (ie_option = 'D') then 
	if (nvl(length(ds_seq_proc_check_list_p),0) > 0) then 
	execute immediate
		'update 	cpoe_proc_diff_order_unit 
		set 	ie_selected = ''S'' 
		where 	nr_sequencia in ('||ds_seq_proc_check_list_p||')';
	end if;
	
	if (nvl(length(ds_seq_proc_uncheck_list_p),0) > 0) then 
    execute immediate
		'update 	cpoe_proc_diff_order_unit 
		set 	ie_selected =''N'' 
		where 	nr_sequencia in ('||ds_seq_proc_uncheck_list_p||')';
	end if; 
elsif (ie_option = 'W') then
	if (nvl(length(ds_seq_proc_check_list_p),0) > 0) then 
    execute immediate
		'update 	cpoe_weekday_proc_item 
		set 	ie_selected = ''S''
		where 	nr_sequencia in ('||ds_seq_proc_check_list_p||')';
	end if;
	
	if (nvl(length(ds_seq_proc_uncheck_list_p),0) > 0) then 
     execute immediate
			'update 	cpoe_weekday_proc_item 
		set 	ie_selected = ''N''
		where 	nr_sequencia in ('||ds_seq_proc_uncheck_list_p||')';
	end if; 
end if;

commit;

end update_cpoe_date_proc;
/