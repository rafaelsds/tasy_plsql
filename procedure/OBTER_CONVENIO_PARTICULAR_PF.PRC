create or replace
procedure Obter_Convenio_Particular_PF
		(cd_estabelecimento_p	in	Number,
		cd_convenio_param_p	in	number,
		cd_pessoa_fisica_p	in	varchar2,
		dt_referencia_p		in	date,
		CD_CONVENIO_P		OUT	NUMBER,
		CD_CATEGORIA_P		OUT	VARCHAR2) IS

nr_seq_cartao_w		number(10,0);
cd_convenio_w		number(05,0)	:= 0;
cd_categoria_w		varchar2(10);

cursor	c01 is
	select	cd_convenio,
		cd_categoria
	from	param_cartao_fidelidade
	where	nr_seq_cartao	= nr_seq_cartao_w;

begin

/* select	nvl(max(nr_seq_cartao), 0)
into	nr_seq_cartao_w
from	pf_cartao_fidelidade
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	dt_referencia_p between dt_inicio_validade and nvl(dt_fim_validade, sysdate);

if	(nr_seq_cartao_w <> 0) then
	begin

	open	c01;
	loop
	fetch	c01 into
		cd_convenio_w,
		cd_categoria_w;
	exit	when c01%notfound;
		begin

		cd_convenio_w	:= cd_convenio_w;

		end;
	end loop;
	close c01;		

	end;
end if;

*/

if	(cd_convenio_w	= 0) then

	select	nvl(max(CD_CONVENIO_GLOSA),0),
		nvl(max(cd_categoria_glosa),'')
	into	cd_convenio_w,
		cd_categoria_w
	from	convenio
	where	cd_convenio		= cd_convenio_param_p;

	if	(cd_convenio_w	= 0) then
		obter_convenio_particular
			(cd_estabelecimento_p,
			cd_convenio_w,
			cd_categoria_w);
	end if;
end if;

CD_CONVENIO_P		:= cd_convenio_w;
CD_CATEGORIA_P		:= cd_categoria_w;

end Obter_Convenio_Particular_PF;
/