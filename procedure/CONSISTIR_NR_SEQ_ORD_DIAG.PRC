CREATE OR REPLACE PROCEDURE CONSISTIR_NR_SEQ_ORD_DIAG ( nr_seq_pe_prescr_p number, NR_SEQ_ORDENACAO_P pe_prescr_diag.NR_SEQ_ORDENACAO%TYPE) as
    cursor wCursor is SELECT 
                            a.NR_SEQUENCIA, a.NR_SEQ_DIAG, NVL(a.IE_DIAG_COLAB,'N')
                        FROM pe_prescr_diag a
                        WHERE 
                            a.nr_seq_prescr = nr_seq_pe_prescr_p AND NR_SEQ_ORDENACAO IS NULL;
                            
    Nr_seq_diag_w pe_prescr_diag.NR_SEQ_DIAG%TYPE := null;
    Nr_sequencia_w pe_prescr_diag.NR_SEQUENCIA%TYPE := null;
    NR_SEQ_ORDENACAO_W pe_prescr_diag.NR_SEQ_ORDENACAO%TYPE := null;
    IE_DIAG_COLAB_W pe_prescr_diag.IE_DIAG_COLAB%TYPE := null;
BEGIN
    UPDATE pe_prescr_diag SET NR_SEQ_ORDENACAO = null WHERE NR_SEQ_ORDENACAO > NR_SEQ_ORDENACAO_P AND nr_seq_pe_prescr_p = nr_seq_prescr ;
    
    open	wCursor;
    loop fetch	wCursor into Nr_sequencia_w,Nr_seq_diag_w,IE_DIAG_COLAB_W;
    exit	when wCursor%notfound;

        NR_SEQ_ORDENACAO_W := OBTER_NUM_ORDENACAO_DIAG(nr_seq_pe_prescr_p,Nr_seq_diag_w, IE_DIAG_COLAB_W);

        UPDATE pe_prescr_diag a SET
            NR_SEQ_ORDENACAO = NR_SEQ_ORDENACAO_W
        WHERE
            a.nr_seq_prescr = nr_seq_pe_prescr_p AND
            a.NR_SEQUENCIA =  Nr_sequencia_w;

    end loop;
    close wCursor;
END CONSISTIR_NR_SEQ_ORD_DIAG;
/