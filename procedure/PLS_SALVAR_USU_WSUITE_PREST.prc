create or replace PROCEDURE pls_salvar_usu_wsuite_prest(
        nr_seq_usuario_p in pls_prestador_usuario_web.nr_seq_usuario%TYPE
        ) is

  CURSOR c01 IS
	SELECT	nr_seq_prestador		 
	FROM pls_cad_prestador_auxiliar
    WHERE nm_usuario = wheb_usuario_pck.get_nm_usuario
	ORDER BY nr_seq_prestador;
  c01_w	c01%ROWTYPE;

BEGIN

OPEN C01;
LOOP
FETCH C01 INTO
	c01_w;
EXIT WHEN C01%NOTFOUND;
	BEGIN

	insert into pls_prestador_usuario_web 
		(nr_sequencia, nr_seq_usuario, nr_seq_prestador, ie_situacao, 
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec) 
	values(pls_prestador_usuario_web_seq.NEXTVAL, nr_seq_usuario_p, 
		c01_w.nr_seq_prestador, 'I', sysdate, wheb_usuario_pck.get_nm_usuario, 
	sysdate, wheb_usuario_pck.get_nm_usuario);

	END;
END LOOP;
CLOSE C01;

DELETE FROM pls_cad_prestador_auxiliar where nm_usuario = wheb_usuario_pck.get_nm_usuario;

end pls_salvar_usu_wsuite_prest;
/
