create or replace
procedure excluir_hemocomponente_inutil( nr_seq_producao_p	number,
					 nm_usuario_p		Varchar2) is 

begin

if (nr_seq_producao_p is not null) then

	update 	san_producao
	set	nr_seq_inutil 		= null,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		dt_inutilizacao		= sysdate, 
		nm_usuario_inut		= nm_usuario_p
	where	nr_sequencia 		= nr_seq_producao_p;

end if;

commit;

end excluir_hemocomponente_inutil;
/