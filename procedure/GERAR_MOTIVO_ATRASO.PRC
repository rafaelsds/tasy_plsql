create or replace
procedure gerar_motivo_atraso(	ds_justificativa_p	varchar2,
				nr_cirurgia_p		number,
				nr_seq_pepo_p		number,
				nr_seq_tempo_p		number,
				qt_tempo_atraso_p	number,
				nr_seq_motivo_atraso_p	number,
				nm_usuario_p		Varchar2,
				nr_seq_grupo_atraso_p	number) is 

begin


insert 	into cirurgia_tempo_atraso(	
		nr_sequencia,
		ds_justificativa,
		dt_atualizacao,
		dt_atualizacao_nrec,
		nm_usuario,
		nm_usuario_nrec,
		nr_cirurgia,
		nr_seq_pepo,
		nr_seq_tempo,
		qt_tempo_atraso,
		nr_seq_motivo_atraso,
		ie_situacao,
		nr_seq_grupo_atraso)
	select 	cirurgia_tempo_atraso_seq.nextval,
		substr(ds_justificativa_p,1,1000),
		sysdate,
		sysdate,
		nm_usuario_p,
		nm_usuario_p,
		nr_cirurgia_p,
		nr_seq_pepo_p,
		nr_seq_tempo_p,
		qt_tempo_atraso_p,
		nr_seq_motivo_atraso_p,
		'A',
		nr_seq_grupo_atraso_p
	from	dual;
	

commit;

end gerar_motivo_atraso;
/
