create or replace
procedure gerar_evolucao_sintomaPA (	nr_atendimento_p 		number,
									nm_usuario_p 			varchar2,					
									ds_evolucao_p 			Long,
									cd_pessoa_fisica_p		varchar2,
									cd_medico_p				varchar2,
									cd_especial_medico_p 	Number) is

ie_tipo_evolucao_w	    varchar2(3);
cd_evolucao_w			number(15);
ie_situacao_w			varchar2(1);
ie_relev_resumo_alta_w  varchar2(1);

begin

select	nvl(max(ie_situacao),'I')
into	ie_situacao_w
from 	tipo_evolucao
where   cd_tipo_evolucao = 'SPA';

if (ie_situacao_w = 'A') then

	ie_tipo_evolucao_w 	:= Obter_Dados_Usuario_Opcao(nm_usuario_p,'F');

	select	evolucao_paciente_seq.nextval
	into	cd_evolucao_w
	from 	dual;
	
	select	nvl(max(ie_relev_resumo_alta),'N')
	into	ie_relev_resumo_alta_w
	from	tipo_evolucao
	where   cd_tipo_evolucao = 'SPA';

	insert into 	evolucao_paciente
			 (cd_evolucao,
			 dt_evolucao,
			 ie_tipo_evolucao,		
			 cd_pessoa_fisica,
			 nr_atendimento,				
			 ie_situacao,
			 dt_atualizacao,
			 nm_usuario,
			 ds_evolucao,
			 cd_medico,
			 cd_especialidade_medico,
			 dt_liberacao,
			 ie_evolucao_clinica,
			 ie_relev_resumo_alta)
	values (
			 cd_evolucao_w,
			 sysdate,
			 ie_tipo_evolucao_w,		
			 cd_pessoa_fisica_p,
			 nr_atendimento_p,		
			 'A',
			 sysdate,
			 nm_usuario_p,
			 ds_evolucao_p,
			 cd_medico_p,
			 cd_especial_medico_p,
			 sysdate,
			 'SPA',
			 ie_relev_resumo_alta_w);

	commit;
	
end if;

end gerar_evolucao_sintomaPA;
/
