create or replace
procedure gerar_itens_glosados(	nr_interno_conta_p		number,
				ie_emite_conta_p		number,
				cd_autorizacao_p		varchar2,
				nr_seq_ret_item_p		number,
				vl_glosado_p		number,
				nm_usuario_p		varchar2,
				cd_procedimento_p	varchar2,
				cd_material_p		varchar2,
				dt_inicial_p		date,
				dt_final_p		date,
				cd_motivo_glosa_p		varchar2,
				ds_observacao_p		varchar2,
				cd_area_proc_p		varchar2,
				cd_espec_proc_p		varchar2,
				cd_grupo_proc_p		varchar2,
				cd_grupo_mat_p		varchar2,
				cd_subgrupo_mat_p	varchar2,
				cd_classe_mat_p		varchar2,
				ie_glosar_itens_conta_p	varchar2 default 'N') is

cd_procedimento_w 		number(15);
cd_material_w 			number(15);
ie_origem_proced_w		number(10);
cd_procedimento_convenio_w	procedimento_paciente.cd_procedimento_convenio%type;
cd_material_convenio_w		material_atend_paciente.cd_material_convenio%type;
cd_setor_atendimento_w		number(5);
nr_seq_material_w		number(10);
nr_seq_procedimento_w		number(10);
nr_seq_ret_item_w		number(10);
ie_emite_conta_w 		number(10);
vl_procedimento_w		number(15,2);
vl_material_w			number(15,2);
qt_procedimento_w		number(15,2);
qt_material_w			number(15,2);
vl_glosa_w			number(15,2);
vl_total_materiais_w		number(15,2);
vl_total_procedimentos_w	number(15,2);
vl_total_w			number(15,2);
ds_observacao_w			varchar2(4000) := null;
vl_total_glosa_w		number(15,2);
nr_seq_item_w			number(10);
vl_glosado_w			convenio_retorno_item.vl_amenor%type;
ds_texto_pck_w			varchar2(2000);

cursor c01 is
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.cd_procedimento_convenio,
	a.cd_setor_atendimento, 
	a.nr_sequencia,
	a.ie_emite_conta,
	a.vl_procedimento,
	a.qt_procedimento
from	procedimento_paciente a,
	estrutura_procedimento_v b
where	a.cd_procedimento = b.cd_procedimento
and	a.ie_origem_proced = b.ie_origem_proced
and	nvl(a.ie_emite_conta, '-1')			= nvl(ie_emite_conta_p, nvl(a.ie_emite_conta, '-1'))
and	a.nr_interno_conta				= nr_interno_conta_p
and	nvl(a.nr_doc_convenio,ds_texto_pck_w) 		= nvl(cd_autorizacao_p,ds_texto_pck_w)
and	a.cd_procedimento					= nvl(cd_procedimento_p, a.cd_procedimento)
and	a.dt_procedimento	 between trunc(nvl(dt_inicial_p, a.dt_procedimento - 1), 'dd') and trunc(nvl(dt_final_p, a.dt_procedimento + 1), 'dd')
and	nvl(cd_area_proc_p,b.cd_area_procedimento) 	= b.cd_area_procedimento
and	nvl(cd_espec_proc_p,b.cd_especialidade) 	= b.cd_especialidade
and	nvl(cd_grupo_proc_p,b.cd_grupo_proc)		= b.cd_grupo_proc
and	nvl(cd_material_p,'X') = 'X'
and	nvl(cd_grupo_mat_p,'X') = 'X'
and	nvl(cd_classe_mat_p,'X') = 'X';
/*and	((nvl(cd_area_proc_p,'X') = 'X') or
	 to_char(obter_area_procedimento(a.cd_procedimento,a.ie_origem_proced))		= cd_area_proc_p)
and	((nvl(cd_espec_proc_p,'X') = 'X') or
	 to_char(obter_especialidade_proced(a.cd_procedimento,a.ie_origem_proced))	= cd_espec_proc_p)
and	((nvl(cd_grupo_proc_p,'X') = 'X') or
	 obter_grupo_procedimento(a.cd_procedimento,a.ie_origem_proced,'C')		= cd_grupo_proc_p);*/

cursor C02 is
select	b.cd_material,
	b.cd_material_convenio,
	b.cd_setor_atendimento,
	b.nr_sequencia,
	b.ie_emite_conta,
	b.vl_material,
	b.qt_material
from	estrutura_material_v a,
	material_atend_paciente b
where	nvl(b.ie_emite_conta, '-1')			= nvl(ie_emite_conta_p, nvl(b.ie_emite_conta, '-1'))
and	b.cd_material					= a.cd_material
and    	b.nr_interno_conta 				= nr_interno_conta_p
and	nvl(b.nr_doc_convenio,ds_texto_pck_w) 		= nvl(cd_autorizacao_p, ds_texto_pck_w)
and	b.cd_material					= nvl(cd_material_p, b.cd_material)
and	b.dt_atendimento				between trunc(nvl(dt_inicial_p, dt_atendimento - 1), 'dd') and trunc(nvl(dt_final_p, dt_atendimento + 1), 'dd')
and	nvl(cd_grupo_mat_p,a.cd_grupo_material) 	= a.cd_grupo_material
and	nvl(cd_subgrupo_mat_p,a.cd_subgrupo_material) 	= a.cd_subgrupo_material
and	nvl(cd_classe_mat_p,a.cd_classe_material) 	= a.cd_classe_material
and	nvl(cd_area_proc_p,'X') = 'X'
and	nvl(cd_espec_proc_p,'X') = 'X'
and	nvl(cd_grupo_proc_p,'X') = 'X';


/*and	((nvl(cd_grupo_mat_p,'X') = 'X') or
	 to_char(a.cd_grupo_material)	= cd_grupo_mat_p)
and	((nvl(cd_subgrupo_mat_p,'X') = 'X') or
	 to_char(a.cd_subgrupo_material)	= cd_subgrupo_mat_p)
and	((nvl(cd_classe_mat_p,'X') = 'X') or
	 to_char(a.cd_classe_material)	= cd_classe_mat_p);	*/

begin

vl_total_materiais_w		:= 0;
vl_total_procedimentos_w	:= 0;
vl_total_w		:= 0;
vl_glosa_w		:= 0;
nr_seq_ret_item_w		:= nr_seq_ret_item_p;
ds_observacao_w		:= substr(ds_observacao_p,1,3999);
vl_total_glosa_w	:= 0;
vl_glosado_w		:= vl_glosado_p;
ds_texto_pck_w		:= substr(Wheb_mensagem_pck.get_Texto(306761),1,2000);--N�o Informada

select	sum(vl_material)
into	vl_total_materiais_w
from	material_atend_paciente b,
	estrutura_material_v a
where	nvl(ie_emite_conta_p,b.ie_emite_conta)			= b.ie_emite_conta
and	b.nr_interno_conta 			= nr_interno_conta_p
and	nvl(b.nr_doc_convenio,ds_texto_pck_w) = nvl(cd_autorizacao_p, ds_texto_pck_w)
and	b.dt_atendimento				between trunc(nvl(dt_inicial_p, dt_atendimento - 1), 'dd') and trunc(nvl(dt_final_p, dt_atendimento + 1), 'dd')
and	b.cd_material					= a.cd_material
and	nvl(cd_material_p,b.cd_material)		= b.cd_material
and	nvl(cd_grupo_mat_p,a.cd_grupo_material) 	= a.cd_grupo_material
and	nvl(cd_subgrupo_mat_p,a.cd_subgrupo_material) 	= a.cd_subgrupo_material
and	nvl(cd_classe_mat_p,a.cd_classe_material) 	= a.cd_classe_material
and	nvl(cd_area_proc_p,'X') = 'X'
and	nvl(cd_espec_proc_p,'X') = 'X'
and	nvl(cd_grupo_proc_p,'X') = 'X';

select	sum(vl_procedimento)
into	vl_total_procedimentos_w
from	procedimento_paciente b,
	estrutura_procedimento_v a
where	nvl(ie_emite_conta_p,b.ie_emite_conta)			= b.ie_emite_conta
and	b.nr_interno_conta 			= nr_interno_conta_p
and	a.cd_procedimento	= b.cd_procedimento
and	b.ie_origem_proced	= a.ie_origem_proced
and	nvl(b.nr_doc_convenio,ds_texto_pck_w) = nvl(cd_autorizacao_p, ds_texto_pck_w)
and	b.cd_procedimento					= nvl(cd_procedimento_p, b.cd_procedimento)
and	b.dt_procedimento	 between trunc(nvl(dt_inicial_p, b.dt_procedimento - 1), 'dd') and trunc(nvl(dt_final_p, b.dt_procedimento + 1), 'dd')
and	nvl(cd_area_proc_p,a.cd_area_procedimento) 	= a.cd_area_procedimento
and	nvl(cd_espec_proc_p,a.cd_especialidade) 	= a.cd_especialidade
and	nvl(cd_grupo_proc_p,a.cd_grupo_proc)		= a.cd_grupo_proc
and	nvl(cd_material_p,'X') = 'X'
and	nvl(cd_grupo_mat_p,'X') = 'X'
and	nvl(cd_classe_mat_p,'X') = 'X';

if	(ie_glosar_itens_conta_p = 'S') and
	(Nvl(vl_glosado_w,0) = 0) then
	select	nvl(sum(a.vl_amenor + a.vl_glosado),0)
	into	vl_glosado_w
	from	convenio_retorno_item a
	where	nr_interno_conta = nr_interno_conta_p
	and	a.nr_sequencia = nr_seq_ret_item_p;
end if;

vl_total_w := 	nvl(vl_total_materiais_w,0) + nvl(vl_total_procedimentos_w,0);

open C01;
loop
fetch C01 into
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_procedimento_convenio_w,
	cd_setor_atendimento_w,
	nr_seq_procedimento_w,
	ie_emite_conta_w,
	vl_procedimento_w,
	qt_procedimento_w;
exit when C01%notfound;
	begin
	
	vl_glosa_w	:= nvl(dividir_sem_round((vl_glosado_w * vl_procedimento_w), vl_total_w),0);
	vl_total_glosa_w	:= nvl(vl_total_glosa_w,0) + nvl(vl_glosa_w,0);

	/* ahoffelder - OS 330401 - 22/06/2011 - � necess�rio porque necessita-se saber qual foi �ltimo item gerado para colocar a sobra do valor.
	N�o fazer select max() foram dos cursores, porque pode pegar um item que n�o foi gerado por esta procedure. */
	select	convenio_retorno_glosa_seq.nextval
	into	nr_seq_item_w
	from	dual;

	insert into convenio_retorno_glosa(
		nr_sequencia,
		nr_seq_ret_item,
		vl_glosa, 
		dt_atualizacao,
		nm_usuario,
		cd_procedimento, 
		ie_origem_proced, 
		ie_atualizacao,
		cd_item_convenio, 
		qt_glosa, 
		cd_setor_atendimento, 
		nr_seq_propaci, 
		ie_emite_conta, 
		vl_cobrado,
		qt_cobrada,
		cd_motivo_glosa,
		ds_observacao) 
	values(	nr_seq_item_w,
		nr_seq_ret_item_w,
		vl_glosa_w,
		sysdate,
		nm_usuario_p,
		cd_procedimento_w, 
		ie_origem_proced_w, 
		'N',
		cd_procedimento_convenio_w, 
		qt_procedimento_w,
		cd_setor_atendimento_w, 
		nr_seq_procedimento_w, 
		ie_emite_conta_w, 
		vl_procedimento_w,
		qt_procedimento_w,
		decode(cd_motivo_glosa_p, 0, null, cd_motivo_glosa_p),
		ds_observacao_w);
	end;
end loop;
close C01;

open C02;
loop
fetch C02 into	
	cd_material_w,
	cd_material_convenio_w,
	cd_setor_atendimento_w,
	nr_seq_material_w,
	ie_emite_conta_w,
	vl_material_w,
	qt_material_w;
exit when C02%notfound;
	begin

	vl_glosa_w	:= nvl(dividir_sem_round((vl_glosado_w * vl_material_w), vl_total_w),0);
	vl_total_glosa_w	:= nvl(vl_total_glosa_w,0) + nvl(vl_glosa_w,0);

	select	convenio_retorno_glosa_seq.nextval
	into	nr_seq_item_w
	from	dual;

	insert into convenio_retorno_glosa(
		nr_sequencia,
		nr_seq_ret_item,
		vl_glosa, 
		dt_atualizacao,
		nm_usuario,
		cd_material, 
		ie_atualizacao,
		cd_item_convenio, 
		qt_glosa, 
		cd_setor_atendimento, 
		nr_seq_matpaci, 
		ie_emite_conta, 
		vl_cobrado,
		qt_cobrada,
		cd_motivo_glosa,
		ds_observacao) 
	values(	nr_seq_item_w,
		nr_seq_ret_item_w,
		vl_glosa_w, 
		sysdate,
		nm_usuario_p,
		cd_material_w,
		'N',
		cd_material_convenio_w, 
		qt_material_w,
		cd_setor_atendimento_w, 
		nr_seq_material_w, 
		ie_emite_conta_w, 
		vl_material_w,
		qt_material_w,
		decode(cd_motivo_glosa_p, 0, null, cd_motivo_glosa_p),
		ds_observacao_w);
	end;
end loop;
close C02;

/* ahoffelder - OS 330401 - 22/06/2011 - colocar a sobra no �ltimo item */

update	convenio_retorno_glosa
--set	vl_glosa	= nvl(vl_glosa,0) + (nvl(vl_glosado_p,0) - nvl(vl_total_glosa_w,0))
set	vl_glosa	= nvl(vl_glosa,0) + (nvl(vl_glosado_w,0) - nvl(vl_total_glosa_w,0))
where	nr_seq_ret_item	= nr_seq_ret_item_w
and	nr_sequencia	= nr_seq_item_w;

commit;

end gerar_itens_glosados;
/