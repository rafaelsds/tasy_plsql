create or replace
procedure GERAR_LOTE_301(nm_usuario_p	varchar2) is

ie_gerar_w	varchar2(1);
dt_geracao_w	date;
dt_fim_ref_w	date;
nr_seq_lote_w	number(10);
qt_w		number(1);

cursor c01 is
select	nr_sequencia,
	cd_estabelecimento,
	cd_convenio,
	ie_tipo_lote,
	dt_ultima_execucao,
	ds_hora_execucao
from	c301_regra_lote
where	ie_situacao	= 'A'
order by cd_estabelecimento,
	nvl(cd_convenio,0) desc;
	
c01_w	c01%rowtype;

begin

open C01;
loop
fetch C01 into
	c01_w;
exit when C01%notfound;

	OBTER_SE_GERA_LOTE_301(c01_w.ds_hora_execucao, ie_gerar_w, dt_geracao_w);
	
	if	(ie_gerar_w = 'S') then
		
		--a data do �ltimo lote gerado pela regra, para utilizar como base na data final de refer�ncia
		select	max(dt_fim_ref)
		into	dt_fim_ref_w
		from	d301_lote_envio
		where	nr_seq_regra_lote	is not null
		and	cd_estabelecimento	= c01_w.cd_estabelecimento
		and	ie_tipo_lote		= c01_w.ie_tipo_lote
		and	nvl(cd_convenio,nvl(c01_w.cd_convenio,0)) = nvl(c01_w.cd_convenio,0);
		
		if	(dt_fim_ref_w is null) then
			dt_fim_ref_w	:= pkg_date_utils.START_OF(sysdate,'DAY');
		else
			dt_fim_ref_w	:= dt_fim_ref_w +(1/24/60/60);
		end if;
		
		select	d301_lote_envio_seq.nextval
		into	nr_seq_lote_w
		from	dual;
		
		insert into d301_lote_envio
			(nr_sequencia,
			nm_usuario,
			dt_atualizacao,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			cd_estabelecimento,
			cd_convenio,
			nr_seq_regra_lote,
			ie_tipo_lote,
			dt_inicio_geracao,
			dt_inicio_ref,
			dt_fim_ref)
		values	(nr_seq_lote_w,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			c01_w.cd_estabelecimento,
			c01_w.cd_convenio,
			c01_w.nr_sequencia,
			c01_w.ie_tipo_lote,
			sysdate,
			dt_fim_ref_w,
			dt_geracao_w);
			
		gerar_dados_lote_301(nr_seq_lote_w,null,nm_usuario_p);
		
		begin
		select	1
		into	qt_w
		from	d301_arquivo_envio a,	
			d301_dataset_envio b
		where	a.nr_sequencia		= b.nr_seq_arquivo
		and	a.nr_seq_lote_envio	= nr_seq_lote_w
		and	rownum			= 1;
		exception
		when others then
			qt_w	:= 0;
		end;	
		
		if	(qt_w  = 0) then
			delete	from d301_lote_envio
			where	nr_sequencia	= nr_seq_lote_w;
		end if;

		update	c301_regra_lote
		set	dt_ultima_execucao	= sysdate
		where	nr_sequencia		= c01_w.nr_sequencia;
		
	end if;

end loop;
close C01;

commit;

end GERAR_LOTE_301;
/