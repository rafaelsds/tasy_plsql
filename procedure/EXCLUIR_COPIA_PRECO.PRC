create or replace
procedure excluir_copia_preco(cd_estabelecimento_p number,
				cd_tabela_servico_p number,
				cd_procedimento_p number,
				dt_inicio_vigencia_p date) is

begin

delete	from preco_servico
where	cd_estabelecimento = cd_estabelecimento_p
and	cd_tabela_servico = cd_tabela_servico_p
and	cd_procedimento = cd_procedimento_p
and	dt_inicio_vigencia = dt_inicio_vigencia_p;

commit;

end excluir_copia_preco;
/