create or replace
procedure iniciar_cirurgia_pepo (
		nr_atendimento_p		number,
		nr_cirurgia_p			number,
		dt_entrada_unidade_p		date,
		dt_inicio_real_p		date,
		cd_setor_atendimento_p		number,
		cd_unidade_basica_p		varchar2,
		cd_unidade_compl_p		varchar2,
		cd_tipo_acomodacao_p		number,
		nm_usuario_p			varchar2,
		cd_funcao_p			number,
		cd_estabelecimento_p		number,
		nr_cirurgia_inicio_real_p	number) is

dt_inicio_real_w date;

--iniciar a cirurgia
begin
iniciar_cirurgia(nr_atendimento_p,
                 nr_cirurgia_p,
                 dt_entrada_unidade_p,
                 dt_inicio_real_p,
                 cd_setor_atendimento_p,
                 cd_unidade_basica_p,
                 cd_unidade_compl_p,
                 cd_tipo_acomodacao_p,
                 nm_usuario_p,
                 cd_funcao_p,
                 cd_estabelecimento_p);

--verifica in�cio real da cirurgia
/*select	dt_inicio_real
into	dt_inicio_real_w
from	cirurgia
where	nr_cirurgia = nr_cirurgia_inicio_real_p;*/

--persiste log
/* gravar_logX_tasy(55808, nr_cirurgia_inicio_real_p ||'; A7; Iniciar cirurgia; Data in�cio real: '||dt_inicio_real_w, nm_usuario_p); */

commit;
end iniciar_cirurgia_pepo;
/