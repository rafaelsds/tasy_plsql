create or replace
procedure gerar_os_nec_cor_proj_migr (
		nr_seq_os_teste_p		number,
		nm_usuario_p			varchar2,
		nr_seq_os_correcao_p out	number) is
		
nr_seq_os_projeto_w		number(10,0);
cd_funcao_w			number(5,0);
nr_seq_grupo_des_migr_w		number(10,0);
nr_seq_projeto_w		number(10,0);
cd_pessoa_usuario_w		varchar2(10);
nr_seq_os_nec_vinc_w		number(10,0);
nm_usuario_programador_w	varchar2(15);

cursor c01 is
select	obter_usuario_pf(nvl(pp.cd_pessoa_fisica,'0'))
from	proj_equipe_papel pp,
	proj_equipe pe
where	pp.nr_seq_equipe = pe.nr_sequencia
and	pp.nr_seq_funcao = 44
and	pp.ie_funcao_rec_migr = 'C'
and	pe.nr_seq_equipe_funcao = 11
and	nvl(pp.ie_situacao,'A') = 'A'
and	pe.nr_seq_proj = nr_seq_projeto_w
order by
	pp.nr_seq_apres desc;

begin
if	(nr_seq_os_teste_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	max(o.nr_seq_origem),
		max(o.cd_funcao),
		max(p.nr_seq_grupo_des),
		max(p.nr_sequencia)
	into	nr_seq_os_projeto_w,
		cd_funcao_w,
		nr_seq_grupo_des_migr_w,
		nr_seq_projeto_w
	from	proj_projeto p,
		man_ordem_servico o
	where	p.nr_seq_ordem_serv = o.nr_seq_origem
	and	p.cd_funcao = o.cd_funcao
	and	o.nr_sequencia = nr_seq_os_teste_p;
	
	if	(nr_seq_projeto_w is not null) then
		begin
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_usuario_w
		from	usuario
		where	nm_usuario = nm_usuario_p;
		
		select	man_ordem_servico_seq.nextval
		into	nr_seq_os_nec_vinc_w
		from	dual;
		
		insert into man_ordem_servico (
			nr_sequencia,
			dt_ordem_servico,
			cd_pessoa_solicitante,
			nr_seq_localizacao,
			nr_seq_equipamento,
			ds_dano_breve,
			ie_prioridade,
			ds_dano,		
			cd_funcao,
			nr_seq_grupo_des,
			ie_classificacao,
			ie_tipo_ordem,
			ie_status_ordem,
			dt_inicio_previsto,
			dt_fim_previsto,
			dt_inicio_desejado,
			dt_conclusao_desejada,		
			nr_seq_estagio,
			nr_grupo_trabalho,
			nr_grupo_planej,
			nm_usuario,
			dt_atualizacao,
			ie_parado,
			ie_obriga_news,
			nr_seq_origem,
			ie_exclusiva,
			nm_usuario_nrec,
			dt_atualizacao_nrec,
			ie_origem_os,
			nr_seq_complex,
			nr_seq_classif,
			nr_seq_nivel_valor,
			ie_prioridade_desen)
		values (
			nr_seq_os_nec_vinc_w,
			sysdate,
			cd_pessoa_usuario_w,
			1272,
			41,
			' ',
			'A',
			' ',
			cd_funcao_w,
			nr_seq_grupo_des_migr_w,
			'S',
			4,
			'2',
			sysdate,
			sysdate+7,
			sysdate,
			sysdate+7,
			732,
			2,
			1,
			nm_usuario_p,
			sysdate,
			'N',
			'N',
			nr_seq_os_teste_p,
			'P',
			nm_usuario_p,
			sysdate,
			'1',
			null,
			22,
			1,
			6);
			
		/*delete
		from	man_ordem_servico_exec
		where	nr_seq_ordem = nr_seq_os_nec_vinc_w;
			
		delete
		from	man_ordem_ativ_prev
		where	nr_seq_ordem_serv = nr_seq_os_nec_vinc_w;
			
		open c01;
		loop
		fetch c01 into nm_usuario_programador_w;
		exit when c01%notfound;
			begin
			if	(nm_usuario_programador_w is not null) then
				begin
				insert into man_ordem_servico_exec (
					nr_sequencia,
					nr_seq_ordem,
					dt_atualizacao,
					nm_usuario,
					nm_usuario_exec,
					qt_min_prev,
					dt_ult_visao,
					nr_seq_funcao,
					dt_recebimento,
					nr_seq_tipo_exec)
				values (
					man_ordem_servico_exec_seq.nextval,
					nr_seq_os_nec_vinc_w,
					sysdate,
					nm_usuario_p,
					nm_usuario_programador_w,
					null,
					null,
					null,
					null,
					2);
				end;
			end if;
			end;
		end loop;
		close c01;*/
			
		insert into proj_ordem_servico (
			nr_sequencia,
			nr_seq_proj,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_ordem,
			ie_tipo_ordem)
		values (
			proj_ordem_servico_seq.nextval,
			nr_seq_projeto_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_os_nec_vinc_w,
			'C');
			
		update	w_plan_teste_migr_proj
		set	ie_status = 'C'
		where	nr_seq_projeto = nr_seq_projeto_w;
		end;
	end if;
	end;
end if;
nr_seq_os_correcao_p := nr_seq_os_nec_vinc_w;
commit;
end gerar_os_nec_cor_proj_migr;
/