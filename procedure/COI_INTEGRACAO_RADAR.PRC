create or replace
procedure COI_INTEGRACAO_RADAR
			(dt_inicial_p		date,
			dt_final_p		date,
			ie_tipo_data_p		number,
	 		nm_usuario_p		varchar2) is

cd_favorecido_w		varchar2(14);
dt_emissao_w		date;
dt_vencimento_w		date;
vl_titulo_w		number(15,2);
nr_documento_w		number(20);
nr_seq_nota_fiscal_w	number(10);
nr_nota_fiscal_w		varchar2(255);
cd_serie_nf_w		nota_fiscal.cd_serie_nf%type;
dt_emissao_nf_w		date;
vl_mercadoria_w		number(15,2);
vl_total_nota_w		number(15,2);
nr_titulo_w		number(10);
vl_classificacao_w		number(15,2);
cd_conta_contabil_w	varchar2(20);
ds_observacao_w		varchar2(255);
qt_nota_mat_med_w	number(10) := 0;
cd_centro_custo_w	number(8);

cursor	c01 is
select	nvl(a.cd_cgc,substr(obter_cpf_pessoa_fisica(a.cd_pessoa_fisica),1,255)) cd_pessoa,
	a.nr_documento,
	a.dt_emissao,
	a.dt_vencimento_atual,
	b.vl_total_nota,
	a.nr_seq_nota_fiscal,
	a.nr_titulo,
	substr(b.ds_observacao,1,255)
from	nota_fiscal b,
	titulo_pagar a
where	decode(ie_tipo_data_p,0,b.dt_emissao,1,a.dt_vencimento_atual,2,a.dt_liquidacao)	between dt_inicial_p and fim_dia(dt_final_p)
and	a.nr_seq_nota_fiscal	= b.nr_sequencia
and	a.ie_situacao		<> 'C'
order by	1;

cursor	c02 is
select	a.vl_titulo,	
	a.cd_centro_custo,
	a.cd_conta_contabil
from	titulo_pagar_classif a
where	a.nr_titulo	= nr_titulo_w;

begin

delete	from w_interf_sefip
where	nm_usuario	= nm_usuario_p;

open	c01;
loop
fetch	c01 into
	cd_favorecido_w,
	nr_documento_w,
	dt_emissao_w,
	dt_vencimento_w,
	vl_titulo_w,
	nr_seq_nota_fiscal_w,
	nr_titulo_w,
	ds_observacao_w;
exit	when c01%notfound;

	select	count(*)
	into	qt_nota_mat_med_w
	from	nota_fiscal_item b,
		nota_fiscal a
	where	b.nr_sequencia_nf	= a.nr_sequencia
	and	a.nr_sequencia		= nr_seq_nota_fiscal_w
	and	b.cd_material		is not null;
	
	/*Se for de material, observacao deve ser 443*/
	if	(qt_nota_mat_med_w > 0) then
		ds_observacao_w	:= '443';	
	end if;	

	/* T - Registro tipo t�tulo */
	insert	into w_interf_sefip
		(ds_conteudo,
		dt_atualizacao,
		nm_usuario,
		nr_sequencia)
	values	('T1' || ';' || cd_favorecido_w || ';' || nr_documento_w || ';001;' || to_char(sysdate,'ddmmyyyy') || ';' || to_char(dt_emissao_w,'ddmmyyyy') || ';' ||
		to_char(dt_vencimento_w,'ddmmyyyy') || ';' || vl_titulo_w || ';'|| ds_observacao_w || ';',
		sysdate,
		nm_usuario_p,
		w_interf_sefip_seq.nextval);

	/* O - Registro tipo origem */
	if	(nr_seq_nota_fiscal_w	is not null) then

		select	max(a.nr_nota_fiscal),
			max(a.cd_serie_nf),
			max(a.dt_emissao),
			max(a.vl_mercadoria),
			max(a.vl_total_nota)
		into	nr_nota_fiscal_w,
			cd_serie_nf_w,
			dt_emissao_nf_w,
			vl_mercadoria_w,
			vl_total_nota_w
		from	nota_fiscal a
		where	a.nr_sequencia	= nr_seq_nota_fiscal_w;

		insert	into w_interf_sefip
			(ds_conteudo,
			dt_atualizacao,
			nm_usuario,
			nr_sequencia)
		values	('O' || nr_nota_fiscal_w || ';' || cd_serie_nf_w || ';' || to_char(dt_emissao_nf_w,'ddmmyyyy') || ';' || vl_total_nota_w || ';' || vl_total_nota_w,
			sysdate,
			nm_usuario_p,
			w_interf_sefip_seq.nextval);

	end if;

	/* C - Registro tipo classifica��o */
	open	c02;
	loop
	fetch	c02 into
		vl_classificacao_w,
		cd_centro_custo_w,
		cd_conta_contabil_w;
	exit	when c02%notfound;
	
		if (cd_conta_contabil_w is not null) then
			insert	into w_interf_sefip
				(ds_conteudo,
				dt_atualizacao,
				nm_usuario,
				nr_sequencia)
			values	('C' || cd_conta_contabil_w || ';' || vl_classificacao_w || ';1;53;9149',										
				sysdate,
				nm_usuario_p,
				w_interf_sefip_seq.nextval);
		end if;
		if (cd_centro_custo_w is not null) then
			insert	into w_interf_sefip
				(ds_conteudo,
				dt_atualizacao,
				nm_usuario,
				nr_sequencia)
			values	('G' || cd_centro_custo_w || ';' || vl_classificacao_w || ';1;53;9149',										
				sysdate,
				nm_usuario_p,
				w_interf_sefip_seq.nextval);	
		end if;

	end	loop;
	close	c02;

end	loop;
close	c01;

commit;

end COI_INTEGRACAO_RADAR;
/
