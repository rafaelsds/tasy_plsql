create or replace
procedure eis_inserir_registros_cubo( tabela_cubo in TABELA_CUBO )
as
begin

if	(tabela_cubo.COUNT > 0) then
	begin
	
	--Apaga os registros por usuario
	delete 	w_eis_ger_r1 w
	where	nm_usuario	= tabela_cubo(1).nm_usuario;
	commit;

	for i in 1 .. tabela_cubo.COUNT loop
		begin
		insert into w_eis_ger_r1(	
				nr_sequencia,
				nm_usuario,
				ie_tipo_coluna,
				vl_coluna_1,
				vl_coluna_2,
				vl_coluna_3,
				vl_coluna_4,
				vl_coluna_5,
				vl_coluna_6,
				vl_coluna_7,
				vl_coluna_8,
				vl_coluna_9,
				vl_coluna_10,
				vl_coluna_11,
				vl_coluna_12,
				vl_total,
				vl_total_geral
		) values (		w_eis_ger_r1_seq.nextval,
				tabela_cubo(i).nm_usuario,
				tabela_cubo(i).ie_tipo_coluna,
				tabela_cubo(i).vl_coluna_1,
				tabela_cubo(i).vl_coluna_2,
				tabela_cubo(i).vl_coluna_3,
				tabela_cubo(i).vl_coluna_4,
				tabela_cubo(i).vl_coluna_5,
				tabela_cubo(i).vl_coluna_6,
				tabela_cubo(i).vl_coluna_7,
				tabela_cubo(i).vl_coluna_8,
				tabela_cubo(i).vl_coluna_9,
				tabela_cubo(i).vl_coluna_10,
				tabela_cubo(i).vl_coluna_11,
				tabela_cubo(i).vl_coluna_12,
				tabela_cubo(i).vl_total,
				tabela_cubo(i).vl_total_geral
			);
		end;
	end loop;
	commit;
	end;
end if;
end eis_inserir_registros_cubo;
/