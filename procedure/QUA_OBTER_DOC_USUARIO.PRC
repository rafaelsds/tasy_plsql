create or replace
PROCEDURE qua_obter_doc_usuario(
			cd_estabelecimento_p		number,
			nm_usuario_p			varchar2,
			ie_lib_setor_usuario_p		varchar2,
			ie_consid_toda_regra_p		varchar2, -- troca or por and
			qt_elaborar_p			out	number,
			qt_validar_p			out	number,
			qt_revisar_p			out	number,
			qt_aprovar_p			out	number,
			qt_ler_p			out	number,
			ie_exibir_p			out	varchar2) is

cd_pessoa_fisica_w	varchar2(10);
qt_elaborar_w		number(10);
qt_validar_w		number(10);
qt_revisar_w		number(10);
qt_aprovar_w		number(10);
qt_ler_w			number(10);
ie_exibir_w		varchar2(1);
nm_user_w		varchar2(50);
qt_aprovar_rev_w		number(10);

begin

select	substr(obter_pessoa_fisica_usuario(nm_usuario_p,'C'),1,10)
into	cd_pessoa_fisica_w
from	dual;

select	count(*) qt_elaborar
into	qt_elaborar_w
from	qua_documento a
where	a.dt_elaboracao is null
and	a.cd_pessoa_elaboracao = cd_pessoa_fisica_w
and	obter_acesso_docto_qual(a.nr_sequencia,nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p,ie_consid_toda_regra_p) = 'S'
and	obter_acesso_idioma_qual(a.nr_sequencia, nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p, ie_consid_toda_regra_p) = 'S'
and	a.cd_estabelecimento = cd_estabelecimento_p;

select	count(*) qt_validar
into	qt_validar_w
from	qua_documento a
where	a.dt_validacao is null
and	a.cd_pessoa_validacao = cd_pessoa_fisica_w
and	obter_acesso_docto_qual(a.nr_sequencia,nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p,ie_consid_toda_regra_p) = 'S'
and	obter_acesso_idioma_qual(a.nr_sequencia, nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p, ie_consid_toda_regra_p) = 'S'
and	a.cd_estabelecimento = cd_estabelecimento_p;

select	count(*) qt_revisao
into	qt_revisar_w
from	qua_doc_revisao a,
	qua_documento b
where	a.nr_seq_doc = b.nr_sequencia
and	a.dt_revisao is null
and	a.cd_pessoa_revisao = cd_pessoa_fisica_w
and	obter_acesso_docto_qual(a.nr_seq_doc,nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p,ie_consid_toda_regra_p) = 'S'
and	obter_acesso_idioma_qual(a.nr_sequencia, nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p, ie_consid_toda_regra_p) = 'S'
and	b.cd_estabelecimento = cd_estabelecimento_p;

select	count(*) qt_aprovar
into	qt_aprovar_w
from	qua_documento a
where	a.dt_aprovacao is null
and	a.cd_pessoa_aprov = cd_pessoa_fisica_w
and	obter_acesso_docto_qual(a.nr_sequencia,nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p,ie_consid_toda_regra_p) = 'S'
and	obter_acesso_idioma_qual(a.nr_sequencia, nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p, ie_consid_toda_regra_p) = 'S'
and 	qua_obter_se_doc_pend_aprov(a.nr_sequencia,cd_pessoa_fisica_w) = 'S'
and	a.cd_estabelecimento = cd_estabelecimento_p;

select	nvl(count(*), 0) qt_aprovar_rev
into	qt_aprovar_rev_w
from	qua_doc_revisao a,
	qua_documento b
where	a.nr_seq_doc = b.nr_sequencia
and	a.dt_aprovacao is null
and	a.cd_pessoa_aprovacao = cd_pessoa_fisica_w
and	obter_acesso_docto_qual(a.nr_seq_doc,nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p,ie_consid_toda_regra_p) = 'S'
and	obter_acesso_idioma_qual(a.nr_sequencia, nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p, ie_consid_toda_regra_p) = 'S'
and	b.cd_estabelecimento = cd_estabelecimento_p;

qt_aprovar_w := qt_aprovar_w + qt_aprovar_rev_w;

select	username
into	nm_user_w
from	user_users;


if	(nm_user_w = 'CORP') then
	begin
		select 	count(*) qt_ler
		into	qt_ler_w
		from	qua_documento a
		where	substr(obter_acesso_docto_qual(a.nr_sequencia, nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p, ie_consid_toda_regra_p ), 1, 1) = 'S'
		and 	substr(obter_acesso_idioma_qual(a.nr_sequencia, nm_usuario_p,cd_estabelecimento_p,ie_lib_setor_usuario_p,ie_consid_toda_regra_p ), 1, 1) = 'S'
		and 	substr(qua_obter_se_subproc_doc_lib(a.nr_sequencia, nm_usuario_p),1,1) = 'S'
		and 	a.ie_situacao = 'A'
		and 	((ie_pergunta = 'S' and obter_se_doc_respondido(a.nr_sequencia, nm_usuario_p) = 'N')
			or 	(nvl(ie_pergunta,'N') = 'N' and nvl(obter_se_doc_lido_usuario(a.nr_sequencia, nm_usuario_p),'N') = 'N'))
		and	qua_obter_se_doc_obrigatorio(a.nr_sequencia, nm_usuario_p) = 'S'
		and	((substr(obter_se_contido_char(a.ie_status, obter_valor_param_usuario(4003, 2, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento)),1,1) = 'S')
			or	((a.ie_status = 'R') and (a.dt_validacao is not null)))
		and	dt_aprovacao is not null
		and	((nr_seq_idioma = wheb_usuario_pck.get_nr_seq_idioma) or (nr_seq_idioma is null));
	end;
else	
	begin
		select	count(*) qt_ler
		into	qt_ler_w
		from	qua_documento a
		where	((nvl(a.ie_pergunta, 'N') = 'N' and nvl(obter_se_doc_lido_usuario(a.nr_sequencia, nm_usuario_p),'N') = 'N')
			or (a.ie_pergunta = 'S' and obter_se_doc_respondido(a.nr_sequencia, nm_usuario_p) = 'N'))
		and	obter_acesso_docto_qual(a.nr_sequencia,nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p, ie_consid_toda_regra_p) = 'S'
		and	obter_acesso_idioma_qual(a.nr_sequencia, nm_usuario_p, cd_estabelecimento_p, ie_lib_setor_usuario_p, ie_consid_toda_regra_p) = 'S'
		and	qua_obter_se_subproc_doc_lib(a.nr_sequencia,nm_usuario_p) = 'S'
		--and	a.cd_estabelecimento = cd_estabelecimento_p
		and	a.ie_situacao	= 'A'
		--and	a.dt_aprovacao is not null
		and	a.ie_status = 'D'
		and	((a.nr_seq_idioma = philips_param_pck.get_nr_seq_idioma) or (a.nr_seq_idioma is null))
		and	qua_obter_se_doc_obrigatorio(a.nr_sequencia, nm_usuario_p) = 'S';
		end;
end if;

ie_exibir_w	:= 'N';

if	(qt_elaborar_w > 0) or (qt_validar_w > 0) or (qt_revisar_w > 0) or (qt_aprovar_w > 0) or (qt_ler_w > 0) then
	ie_exibir_w	:= 'S';
end if;

qt_elaborar_p	:= qt_elaborar_w;
qt_validar_p	:= qt_validar_w;
qt_revisar_p	:= qt_revisar_w;
qt_aprovar_p	:= qt_aprovar_w;
qt_ler_p	:= qt_ler_w;
ie_exibir_p	:= ie_exibir_w;

end qua_obter_doc_usuario;
/