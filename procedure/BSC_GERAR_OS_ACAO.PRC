create or replace
procedure bsc_gerar_os_acao(	cd_pessoa_solicitante_p		number,
				nr_sequencia_p			number,
				nm_usuario_exec_p			varchar2,
				nr_seq_equipamento_p		number,
				nr_seq_localizacao_p		number,
				nm_usuario_p			varchar2,
				nr_seq_estagio_p			number,
				nr_seq_ordem_p		out	number) is
					
ds_dano_w		varchar2(4000);
ds_dano_breve_w		varchar2(80);
nr_seq_ordem_w		number(10);
nr_sequencia_w		number(10);
nr_seq_estagio_w		number(10) := null;
dt_prev_solucao_w		date;
nr_seq_grupo_planej_w		number(10);
nr_seq_grupo_trabalho_w		number(10);

begin
select	substr(c.ds_pendencia,1,80),
	c.dt_prev_solucao,
	substr(c.ds_pendencia ||': '|| c.ds_observacao,1,4000) ds_dano
into	ds_dano_breve_w,
	dt_prev_solucao_w,
	ds_dano_w
from	bsc_ind_inf_analise_acao c
where	c.nr_sequencia		= nr_sequencia_p;

select	man_ordem_servico_seq.nextval
into	nr_seq_ordem_w
from	dual;

select	max(nr_seq_planej), 
	max(nr_seq_trab)
into	nr_seq_grupo_planej_w,
	nr_seq_grupo_trabalho_w
from	man_equipamento
where	nr_sequencia = nr_seq_equipamento_p;


if	(nr_seq_estagio_p <> 0) then
	nr_seq_estagio_w := nr_seq_estagio_p;
end if;

insert into man_ordem_servico
		(cd_pessoa_solicitante,
		ds_dano,
		ds_dano_breve,
		dt_atualizacao,
		dt_ordem_servico,
		ie_parado,
		ie_prioridade,
		ie_tipo_ordem,
		nm_usuario,
		nr_seq_equipamento,
		nr_seq_localizacao,
		nr_sequencia,
		ie_status_ordem,
		nr_seq_estagio,
		dt_conclusao_desejada,
		dt_fim_previsto,
		ie_origem_os,
		nr_grupo_planej,
		nr_grupo_trabalho)
	values	(cd_pessoa_solicitante_p,
		ds_dano_w,
		ds_dano_breve_w,
		sysdate,
		sysdate,
		'N',
		'M',
		'14',
		nm_usuario_p,
		nr_seq_equipamento_p,
		nr_seq_localizacao_p,
		nr_seq_ordem_w,
		1,
		nr_seq_estagio_w,
		dt_prev_solucao_w,
		dt_prev_solucao_w,
		6,
		nr_seq_grupo_planej_w,
		nr_seq_grupo_trabalho_w);

nr_seq_ordem_p := nr_seq_ordem_w;

select	man_ordem_servico_exec_seq.nextval
into	nr_sequencia_w
from	dual;

insert into man_ordem_servico_exec
		(dt_atualizacao,
		nm_usuario,
		nm_usuario_exec,
		nr_seq_ordem,
		nr_sequencia)
	values	(sysdate,
		nm_usuario_p,
		nm_usuario_exec_p,
		nr_seq_ordem_w,
		nr_sequencia_w);

update	bsc_ind_inf_analise_acao
set	nm_usuario		= nm_usuario_p,
	dt_atualizacao  	= sysdate,
	nr_seq_ordem_servico	= nr_seq_ordem_p	
where	nr_sequencia	= nr_sequencia_p;
	
commit;

end bsc_gerar_os_acao;
/
