create or replace
procedure med_agenda_cons_gridDblClick(	cd_pessoa_fisica_p		varchar2,
					cd_medico_p		varchar2,
					nm_usuario_p		varchar2,
					nr_seq_cliente_p		out number,
					qt_primeira_consulta_p	out number)is

nr_seq_cliente_w	number(10,0) := 0;
qt_primeira_consulta_w	number(10,0) := 0;
		
begin

if	(cd_pessoa_fisica_p is not null) and
	(cd_medico_p is not null) then
	begin
	/* Utilizado para vincular o paciente ao m�dico */
	incluir_med_cliente(cd_pessoa_fisica_p, cd_medico_p, nm_usuario_p);
	
	/* Busca a sequencia do cliente e se o paciente possui primeira consulta */
	med_obter_cliente_prim_cons(cd_pessoa_fisica_p, cd_medico_p, nr_seq_cliente_w, qt_primeira_consulta_w);	
	end;
end if;

nr_seq_cliente_p		:= nr_seq_cliente_w;
qt_primeira_consulta_p	:= qt_primeira_consulta_w;

end med_agenda_cons_gridDblClick;
/