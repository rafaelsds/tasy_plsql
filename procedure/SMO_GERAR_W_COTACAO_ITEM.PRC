create or replace
procedure smo_gerar_w_cotacao_item(	nr_cot_compra_p	number,
					nm_usuario_p	varchar2) is

nr_seq_cot_item_w			number(10);
nr_seq_cot_col_w			number(10);
nr_item_cot_compra_w		number(5);
cd_material_w			number(6);
cd_fornecedor_w			varchar2(14);
qt_material_w			number(13,4);
vl_unitario_material_w		number(13,4);
nr_seq_coluna_w			number(5);
ds_marca_w			varchar2(30);
vl_ipi_w				number(15,4);
cd_condicao_pagamento_w		number(10);
vl_frete_w			number(15,2);
qt_dias_entrega_w			number(5);
vl_unitario_inicial_w			number(13,4);
vl_liquido_w			number(13,4);
ie_vencedor_w			varchar2(1);
nr_seq_cot_item_forn_w		number(10);
ds_observacao_w			varchar2(255);
ds_observacao_item_w		varchar2(255);
ds_motivo_venc_alt_w		varchar2(4000);
ie_motivo_alter_venc_w		varchar2(15);
ds_cond_pagto_w			varchar2(255);
vl_minimo_nf_w			number(15,2);
cd_estabelecimento_w		number(10);
vl_total_w			number(13,2);

cursor c01 is
	select	nr_item_cot_compra,
		cd_material,
		ds_motivo_venc_alt,
		ie_motivo_alter_venc
	from	cot_compra_item
	where	nr_cot_compra = nr_cot_compra_p;

cursor c02 is
	select	a.nr_sequencia,
		f.cd_cgc_fornecedor,
		f.cd_condicao_pagamento,
		f.vl_previsto_frete,
		f.qt_dias_entrega,
		a.qt_material,
		a.vl_unitario_material,
		a.vl_unitario_liquido,
		a.vl_unitario_inicial,
		a.ds_marca,
		substr(nvl(obter_se_fornec_venc_cotacao(nr_cot_compra_p,a.nr_item_cot_compra,f.nr_sequencia),'N'),1,1) ie_vencedor,
		substr(f.ds_observacao,1,255),
		substr(a.ds_observacao,1,255),
		substr(obter_desc_cond_pagto(f.cd_condicao_pagamento),1,100),
		obter_dados_pf_pj_estab(cd_estabelecimento_w,null,f.cd_cgc_fornecedor,'EVM')
	from	cot_compra_forn_item a,
		cot_compra_forn f,
		cot_compra_item c
	where	f.nr_sequencia		= a.nr_seq_cot_forn
	and	a.nr_cot_compra 		= nr_cot_compra_p
	and 	c.nr_cot_compra = a.nr_cot_compra
	and	a.nr_item_cot_compra = c.nr_item_cot_compra
	and	a.nr_item_cot_compra 	= nr_item_cot_compra_w
	order by substr(obter_dados_pf_pj(null, f.cd_cgc_fornecedor,'N'),1,200);

BEGIN
--Relat�rio do cliente Associacao da Santa Casa de Misericordia de Ourinhos
--� uma c�pia da gerar_w_cotacao_item_coluna, foi retirado as restri��es do delete

delete from w_cotacao_item_coluna;
delete from w_cotacao_item;

select	cd_estabelecimento
into	cd_estabelecimento_w
from	cot_compra
where	nr_cot_compra = nr_cot_compra_p;

open c01;
loop
	fetch c01 into
		nr_item_cot_compra_w,
		cd_material_w,
		ds_motivo_venc_alt_w,
		ie_motivo_alter_venc_w;
	exit when c01%notfound;

	select	w_cotacao_item_seq.nextval
	into	nr_seq_cot_item_w
	from	dual;

	insert into w_cotacao_item(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_material,
		ds_motivo_venc_alt,
		ie_motivo_alter_venc,
		nr_cot_compra)
	values(	nr_seq_cot_item_w,
		sysdate,
		nm_usuario_p,
		cd_material_w,
		ds_motivo_venc_alt_w,
		ie_motivo_alter_venc_w,
		nr_cot_compra_p);
	commit;

	open c02;
	loop
		fetch c02 into
			nr_seq_cot_item_forn_w,
			cd_fornecedor_w,
			cd_condicao_pagamento_w,
			vl_frete_w,
			qt_dias_entrega_w,
			qt_material_w,
			vl_unitario_material_w,
			vl_liquido_w,
			vl_unitario_inicial_w,
			ds_marca_w,
			ie_vencedor_w,
			ds_observacao_w,
			ds_observacao_item_w,
			ds_cond_pagto_w,
			vl_minimo_nf_w;			
		exit when c02%notfound;

			select	w_cotacao_item_coluna_seq.nextval
			into	nr_seq_cot_col_w
			from	dual;

			select	nvl(max(nr_seq_coluna),0)
			into	nr_seq_coluna_w
			from	w_cotacao_item_coluna
			where	cd_fornecedor = cd_fornecedor_w
			and	nm_usuario = nm_usuario_p
			and	nr_cot_compra = nr_cot_compra_p;
		
			if	(nr_seq_coluna_w = 0) then
				select	nvl(max(nr_seq_coluna),0) + 1
				into	nr_seq_coluna_w
				from	w_cotacao_item_coluna
				where	nm_usuario = nm_usuario_p
				and	nr_cot_compra = nr_cot_compra_p;
			end if;
			
			select	nvl(sum(b.vl_tributo),0)
			into	vl_ipi_w
			from	cot_compra_forn_item_tr b,
				tributo c
			where	b.cd_tributo = c.cd_tributo
			and	c.ie_tipo_tributo = 'IPI'
			and	b.nr_seq_cot_item_forn = nr_seq_cot_item_forn_w;
			
			vl_total_w := 0;
			if	(ie_vencedor_w = 'S') then
				vl_total_w := vl_unitario_material_w * qt_material_w;
			end if;

			insert into w_cotacao_item_coluna(
				nr_sequencia,
				nr_seq_coluna,
				dt_atualizacao,
				nm_usuario,
				nr_seq_cotacao_item,
				cd_fornecedor,
				qt_fornecedor,
				vl_fornecedor,
				vl_liquido,
				vl_unitario_inicial,
				ds_marca,
				vl_ipi,
				cd_condicao_pagamento,
				vl_frete,
				qt_dias_entrega,
				ie_vencedor,
				ds_observacao,
				ds_observacao_item,
				ds_motivo_venc_alt,
				nr_cot_compra,
				ds_cond_pagto,
				vl_minimo_nf,
				vl_total)
			values(	nr_seq_cot_col_w,
				nr_seq_coluna_w,
				sysdate,
				nm_usuario_p,
				nr_seq_cot_item_w,
				cd_fornecedor_w,
				qt_material_w,
				vl_unitario_material_w,
				vl_liquido_w,
				vl_unitario_inicial_w,
				ds_marca_w,
				vl_ipi_w,
				cd_condicao_pagamento_w,
				vl_frete_w,
				qt_dias_entrega_w,
				ie_vencedor_w,
				ds_observacao_w,
				ds_observacao_item_w,
				substr(ds_motivo_venc_alt_w,1,255),
				nr_cot_compra_p,
				ds_cond_pagto_w,
				vl_minimo_nf_w,
				vl_total_w);
			commit;
	end loop;
	close c02;
end loop;
close c01;

end smo_gerar_w_cotacao_item;
/
