create or replace
procedure marcar_item_auditado(	nr_sequencia_p		number,
				nm_tabela_p		varchar2,
				nr_seq_motivo_p		number,
				nm_usuario_p		varchar2) is 

begin

if	(nm_tabela_p	is not null) and
	(nr_sequencia_p	is not null) then
	begin
	if	(upper(nm_tabela_p) = 'AUDITORIA_MATPACI') then
		begin 
		update	auditoria_matpaci
		set	ie_tipo_auditoria	= 'D',
			nm_usuario	= nm_usuario_p,
			nr_seq_motivo	= nr_seq_motivo_p,
			dt_atualizacao	= sysdate
		where 	nr_sequencia 	= nr_sequencia_p;
		end;
	elsif	(upper(nm_tabela_p) = 'AUDITORIA_PROPACI') then
		begin 
		update 	auditoria_propaci
		set	ie_tipo_auditoria	= 'D',
			nm_usuario	= nm_usuario_p,
			nr_seq_motivo	= nr_seq_motivo_p,
			dt_atualizacao	= sysdate
		where 	nr_sequencia 	= nr_sequencia_p;
		end;
	elsif	(upper(nm_tabela_p) = 'AUDITORIA_EXTERNA')then 
		begin 
		update 	auditoria_externa
		set	ie_auditado	= 'S',
			nm_usuario	= nm_usuario_p,
			nr_seq_motivo	= nr_seq_motivo_p,
			dt_atualizacao	= sysdate
		where 	nr_sequencia 	= nr_sequencia_p;	
		end;
	end if;
	end;
end if;

commit;

end marcar_item_auditado;
/