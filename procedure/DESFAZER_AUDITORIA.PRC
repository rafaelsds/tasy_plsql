create or replace
procedure Desfazer_auditoria(nr_seq_auditoria_p	number,
			     nm_usuario_p	Varchar2) is 

			     
ie_tipo_auditoria_w		varchar2(1);
nr_seq_matpaci_w		number(10,0);
nr_seq_propaci_w		number(10,0);
nr_interno_conta_w		number(10,0);
cd_convenio_w			number(6,0);
cd_categoria_w			varchar2(10);
vl_tabela_original_w		number(15,2);
cd_motivo_exc_conta_w		number(3,0);
cd_estabelecimento_w		number(4,0);
dt_liberacao_w			date;
nr_seq_item_gerado_w		number(10,0);
ie_recalcular_conta_w   	varchar2(1);
cd_procedimento_w		number(15,0);
ie_origem_proced_w		number(10,0);
ie_responsavel_credito_w  	varchar2(5);
cd_setor_atendimento_w		number(5,0);
nr_atendimento_w		number(10,0);
cd_especialidade_medica_w  	number(05,0):= 0;
dt_procedimento_w		date;
cd_cgc_prestador_w		varchar2(14);
cd_estrutura_w			number(5) 		:= 0;
cd_estrutura_honor_w		number(5) 		:= 0;
ie_emite_conta_w           	varchar2(3);
ie_emite_conta_honor_w        	varchar2(3);
ie_tipo_atendimento_w		number(3,0);
cd_medico_executor_w		varchar2(10);
nr_seq_cor_exec_w		number(10,0);
cd_material_w			number(10,0);
ie_Gerar_Pendencia_w		varchar2(1);

Cursor C01 is
	select	ie_tipo_auditoria,
		nr_seq_matpaci
	from	auditoria_matpaci
	where	nr_seq_auditoria = nr_seq_auditoria_p
	order by nr_seq_matpaci;
	
Cursor C02 is
	select	ie_tipo_auditoria,
		nr_seq_propaci
	from	auditoria_propaci
	where	nr_seq_auditoria = nr_seq_auditoria_p
	order by nr_seq_propaci;
	
cursor	c03 is
	select 	nr_sequencia,
		nr_seq_cor_exec
	from 	material_atend_paciente
	where	nr_interno_conta = nr_interno_conta_w
	and 	ie_auditoria = 'S'
	and 	Obter_se_gerado_lib_audit(nr_sequencia, 2) = 'S'
	and 	dt_liberacao_w is not null
	and 	nr_seq_proc_pacote is null
	order by nr_sequencia;
	
cursor	c04 is
	select 	nr_sequencia,
		nr_seq_cor_exec
	from 	procedimento_paciente
	where	nr_interno_conta = nr_interno_conta_w
	and 	ie_auditoria = 'S'
	and 	Obter_se_gerado_lib_audit(nr_sequencia, 1) = 'S'
	and 	dt_liberacao_w is not null
	and 	nr_seq_proc_pacote is null
	order by nr_sequencia;
		
			     
begin

select 	max(nr_interno_conta),
	max(dt_liberacao)
into	nr_interno_conta_w,
	dt_liberacao_w
from 	auditoria_conta_paciente
where 	nr_sequencia = nr_seq_auditoria_p;

select 	max(cd_convenio_parametro),
	max(cd_categoria_parametro),
	max(cd_estabelecimento),
	max(nr_atendimento)
into	cd_convenio_w,
	cd_categoria_w,
	cd_estabelecimento_w,
	nr_atendimento_w
from 	conta_paciente
where 	nr_interno_conta = nr_interno_conta_w;


select 	max(ie_tipo_atendimento)
into	ie_tipo_atendimento_w
from 	atendimento_paciente
where 	nr_atendimento =  nr_atendimento_w;
	

cd_motivo_exc_conta_w:= obter_valor_param_usuario(1116, 19, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w);
ie_recalcular_conta_w:= obter_valor_param_usuario(1116, 130, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w);
ie_Gerar_Pendencia_w := obter_valor_param_usuario(1116, 136, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w);

if	(nvl(cd_motivo_exc_conta_w,0) = 0) then

	select 	max(cd_motivo_exc_conta)
	into	cd_motivo_exc_conta_w
	from 	parametro_faturamento
	where 	cd_estabelecimento = cd_estabelecimento_w;

end if;

-- Cursor dos materiais que foram gerados atrav�s do ajuste de quantidades informado nos itens
open C03;
loop
fetch C03 into	
	nr_seq_item_gerado_w,
	nr_seq_cor_exec_w;
exit when C03%notfound;
	begin
	
	if	(nvl(nr_seq_cor_exec_w,0) = 431) then
		excluir_matproc_conta(nr_seq_item_gerado_w,nr_interno_conta_w,cd_motivo_exc_conta_w,WHEB_MENSAGEM_PCK.get_texto(299357),'M',nm_usuario_p);
	else
		excluir_matproc_auditoria(nr_seq_item_gerado_w, 'M', nr_seq_auditoria_p);
	end if;
	
	end;
end loop;
close C03;

-- Cursor dos procedimentos que foram gerados atrav�s do ajuste de quantidades informado nos itens
open C04;
loop
fetch C04 into	
	nr_seq_item_gerado_w,
	nr_seq_cor_exec_w;
exit when C04%notfound;
	begin
	
	if	(nvl(nr_seq_cor_exec_w,0) = 431) then
		excluir_matproc_conta(nr_seq_item_gerado_w,nr_interno_conta_w,cd_motivo_exc_conta_w,WHEB_MENSAGEM_PCK.get_texto(299357),'P',nm_usuario_p);	
	else
		excluir_matproc_auditoria(nr_seq_item_gerado_w, 'P', nr_seq_auditoria_p);
	end if;	
	
	end;
end loop;
close C04;


open C01;
loop
fetch C01 into	
	ie_tipo_auditoria_w,
	nr_seq_matpaci_w;
exit when C01%notfound;
	begin
	
	--Execu��o, Novo lan�amento
	if	(ie_tipo_auditoria_w = 'E') then

		delete from   auditoria_matpaci
		where nr_seq_matpaci = nr_seq_matpaci_w
		and   nr_seq_auditoria = nr_seq_auditoria_p;
	
		excluir_matproc_conta(nr_seq_matpaci_w,nr_interno_conta_w,cd_motivo_exc_conta_w,WHEB_MENSAGEM_PCK.get_texto(299358),'M',nm_usuario_p);
		
	end if;
	
	
	-- Exclu�do da conta
	if	(ie_tipo_auditoria_w = 'X') then
					
		select	max(cd_material),
			max(cd_setor_atendimento),
			max(ie_responsavel_credito)
		into	cd_material_w,
			cd_setor_atendimento_w,
			ie_responsavel_credito_w
		from 	material_atend_paciente
		where 	nr_sequencia = nr_seq_matpaci_w;
		
		ie_emite_conta_w := null;
		
		obter_estrut_conta_mat(	cd_convenio_w,
					cd_material_w,
					cd_setor_atendimento_w,
					ie_tipo_atendimento_w,
					ie_responsavel_credito_w,
					cd_estabelecimento_w,
					cd_estrutura_w);
		if	(cd_estrutura_w	> 0) then
			ie_emite_conta_w	:= cd_estrutura_w;
		end if;
		
		Update	material_atend_paciente
                set    	NR_INTERNO_CONTA 	= nr_interno_conta_w,
                        CD_MOTIVO_EXC_CONTA 	= null, 
			DS_COMPL_MOTIVO_EXCON 	= null,
                        cd_convenio    		= cd_convenio_w,
                        cd_categoria   		= cd_categoria_w,
			ie_emite_conta		= ie_emite_conta_w
                where  	nr_sequencia 		= nr_seq_matpaci_w;
		
		delete from   auditoria_matpaci
		where nr_seq_matpaci = nr_seq_matpaci_w
		and   nr_seq_auditoria = nr_seq_auditoria_p;
	
	end if;
	
	-- Altera��o de valor
	if	(ie_tipo_auditoria_w = 'V') then
	
		delete from   auditoria_matpaci
		where nr_seq_matpaci = nr_seq_matpaci_w
		and   nr_seq_auditoria = nr_seq_auditoria_p;
		
		update	material_atend_paciente
		set	ie_valor_informado = 'N'
		where 	nr_sequencia = nr_seq_matpaci_w;
		
		atualiza_preco_material(nr_seq_matpaci_w, nm_usuario_p);		
		
	end if;
	
	-- Demais op��es
	if	(ie_tipo_auditoria_w not in ('E','X','V')) then
	
		delete from   auditoria_matpaci
		where nr_seq_matpaci = nr_seq_matpaci_w
		and   nr_seq_auditoria = nr_seq_auditoria_p;
		
	end if;
	
	excluir_matproc_auditoria(nr_seq_matpaci_w, 'M', nr_seq_auditoria_p);
	
	end;
end loop;
close C01;



open C02;
loop
fetch C02 into	
	ie_tipo_auditoria_w,
	nr_seq_propaci_w;
exit when C02%notfound;
	begin
	
	--Execu��o, Novo lan�amento
	if	(ie_tipo_auditoria_w = 'E') then
		delete from   auditoria_propaci
		where nr_seq_propaci = nr_seq_propaci_w
		and   nr_seq_auditoria = nr_seq_auditoria_p;
	
		excluir_matproc_conta(nr_seq_propaci_w,nr_interno_conta_w,cd_motivo_exc_conta_w,WHEB_MENSAGEM_PCK.get_texto(299358),'P',nm_usuario_p);
		
	end if;
	
	
	-- Exclu�do da conta
	if	(ie_tipo_auditoria_w = 'X') then		
		
		
		select	max(cd_procedimento),
			max(ie_origem_proced),
			max(cd_medico_executor),
			max(ie_responsavel_credito),
			max(cd_setor_atendimento),
			max(cd_especialidade),
			max(dt_procedimento),
			max(cd_cgc_prestador)
		into	cd_procedimento_w,
			ie_origem_proced_w,
			cd_medico_executor_w,
			ie_responsavel_credito_w,
			cd_setor_atendimento_w,
			cd_especialidade_medica_w,
			dt_procedimento_w,
			cd_cgc_prestador_w
		from 	procedimento_paciente
		where 	nr_sequencia = nr_seq_propaci_w;
		
		obter_estrut_conta_proc(
			cd_convenio_w,
			cd_procedimento_w,
			ie_origem_proced_w,
			null,
			cd_medico_executor_w,
			ie_responsavel_credito_w,
			nr_seq_propaci_w,
			cd_setor_atendimento_w,
			ie_tipo_atendimento_w,
			cd_estabelecimento_w,
			cd_especialidade_medica_w,
			cd_categoria_w,
			dt_procedimento_w,
			cd_cgc_prestador_w,
			cd_estrutura_w,
			cd_estrutura_honor_w);
			
		ie_emite_conta_w			:= null;
		ie_emite_conta_honor_w			:= null;
		
		if	(cd_estrutura_w	> 0) then
			ie_emite_conta_w	:= cd_estrutura_w;
		end if;
		if	(cd_estrutura_honor_w	> 0) then
			ie_emite_conta_honor_w	:= cd_estrutura_honor_w;
		end if;
		
		Update	procedimento_paciente
                set    	NR_INTERNO_CONTA 	= nr_interno_conta_w,
                        CD_MOTIVO_EXC_CONTA 	= null, 
			DS_COMPL_MOTIVO_EXCON 	= null,
                        cd_convenio    		= cd_convenio_w,
                        cd_categoria   		= cd_categoria_w,
			ie_emite_conta		= ie_emite_conta_w,
			ie_emite_conta_honor	= ie_emite_conta_honor_w
                where  	nr_sequencia 		= nr_seq_propaci_w;
		
		delete from   auditoria_propaci
		where nr_seq_propaci = nr_seq_propaci_w
		and   nr_seq_auditoria = nr_seq_auditoria_p;
	
	end if;
	
	-- Altera��o de valor
	if	(ie_tipo_auditoria_w = 'V') then
	
		delete from   auditoria_propaci
		where nr_seq_propaci = nr_seq_propaci_w
		and   nr_seq_auditoria = nr_seq_auditoria_p;
		
		update	procedimento_paciente
		set	ie_valor_informado = 'N'
		where 	nr_sequencia = nr_seq_propaci_w;
		
		update	procedimento_participante
		set	ie_valor_informado = 'N',
			pr_faturar	   = 100
		where 	nr_sequencia = nr_seq_propaci_w;
		
		atualiza_preco_procedimento(nr_seq_propaci_w, cd_convenio_w, nm_usuario_p);
		
	end if;
	
	-- Demais op��es
	if	(ie_tipo_auditoria_w not in ('E','X','V')) then
	
		delete from   auditoria_propaci
		where nr_seq_propaci = nr_seq_propaci_w
		and   nr_seq_auditoria = nr_seq_auditoria_p;
		
	end if;
	
	excluir_matproc_auditoria(nr_seq_propaci_w, 'P', nr_seq_auditoria_p);
	
	end;
end loop;
close C02;


if	(ie_Gerar_Pendencia_w = 'V') then

	update	auditoria_conta_paciente
	set	nr_seq_audit_vinc = null
	where	nr_seq_audit_vinc = nr_seq_auditoria_p;

	Excluir_Pendencia_Vinculada(nr_interno_conta_w, nr_seq_auditoria_p, nm_usuario_p);
	
end if;

delete from auditoria_conta_paciente
where nr_sequencia = nr_seq_auditoria_p;

if	(nvl(ie_recalcular_conta_w,'S') = 'S') then
	recalcular_conta_paciente(nr_interno_conta_w, nm_usuario_p);
end if;

commit;

end Desfazer_auditoria;
/
