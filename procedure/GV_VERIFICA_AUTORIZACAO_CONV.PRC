create or replace
procedure gv_verifica_autorizacao_conv(
					cd_convenio_p     		number,
					cd_procedimento_p    		number,
					ie_origem_proced_p   	   	number,
					cd_plano_convenio_p		varchar2,
					cd_categoria_p			varchar2,
					nr_seq_proc_interno_p		number,
					cd_estabelecimento_p		number,
					ie_sexo_p			varchar2,
					nr_sequencia_p			number,
					ie_autorizacao_p	out	varchar2,
					ie_regra_p		out	number) is 

ds_erro_w		varchar2(255);
ie_regra_w		Number(5);
nr_seq_regra_w		number(10);
ie_glosa_w		Varchar2(1);
cd_edicao_ajuste_w      number(10);
cd_proc_adic_w		number(15);
ie_origem_proc_adic_w	number(10);
nr_seq_proc_adic_w	number(10);	
nr_sequencia_w		number(10);	
cd_estabelecimento_w	number(4);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
nr_seq_proc_interno_w   number(10);
ie_autorizacao_w	varchar2(1);
ie_resp_autor_w		varchar2(10);
vl_procedimento_w	Number(15,2);
ie_tipo_convenio_w	Number(3);
ie_edicao_w             varchar2(1);
ie_pacote_w		varchar2(1);
qt_item_edicao_w        number(10);
cd_convenio_partic_w    number(5);
cd_categoria_partic_w   varchar2(10);
ds_regra_w		varchar2(30);			
			
begin


gv_consiste_plano_conv(
       			null,
			cd_convenio_p,
			cd_procedimento_p,
			ie_origem_proced_p,
			sysdate,
			1,
			7,
			cd_plano_convenio_p,
			null,
			ds_erro_w,
			0,
			null,
			ie_regra_w,
			null,
			nr_seq_regra_w,
			nr_seq_proc_interno_p,
			cd_categoria_p,
			cd_estabelecimento_p,
			null,
			ie_sexo_p,
			ie_glosa_w,
			cd_edicao_ajuste_w,
			null);
			
	 
select	max(ie_tipo_convenio)
into	ie_tipo_convenio_w
from	convenio
where	cd_convenio	= cd_convenio_p;
	
ie_edicao_w	:= gv_obter_se_proc_conv(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, sysdate, cd_procedimento_p, ie_origem_proced_p,nr_seq_proc_interno_p,0);             
		
ie_pacote_w	:= obter_Se_pacote_convenio(cd_procedimento_p, ie_origem_proced_p, cd_convenio_p, cd_estabelecimento_p);
	
if	(ie_edicao_w 			= 'N') and 
	(nvl(cd_edicao_ajuste_w,0) 	= 0) and 
	(ie_pacote_w			= 'N') then
	ie_glosa_w        := 'T';
end if;
		  
if	(ie_edicao_w 			= 'N') and 
	(nvl(cd_edicao_ajuste_w,0) 	> 0) and 
	(ie_pacote_w			= 'N') then
			
	select   count(*)
	into     qt_item_edicao_w
	from     preco_amb
	where    cd_edicao_amb = cd_edicao_ajuste_w
	and      cd_procedimento = cd_procedimento_p
	and      ie_origem_proced = ie_origem_proced_p;
			       
	if      (qt_item_edicao_w = 0) then
		ie_glosa_w :=    'G'; 
	end if;
					      
end if;
	
ie_autorizacao_p	:= 'L';


if	(ie_Regra_w in (1,2,5)) or 
	(ie_Regra_w = 8) then
	
	ie_autorizacao_p	:= 'B';
	
	
elsif	(ie_Regra_w in (3,6,7)) then

	select 	nvl(max(ie_resp_autor),'H')
	into	ie_resp_autor_w
	from 	regra_convenio_plano
	where 	nr_sequencia = nr_seq_regra_w;
	
	if	(ie_resp_autor_w	= 'H') then
		ie_autorizacao_p	:= 'PAH';
	
	elsif	(ie_resp_autor_w	= 'P') then
		ie_autorizacao_p	:= 'PAP';
	end if;
end if;
							
if	(ie_glosa_w in ('G','T','D','F')) then

	ie_autorizacao_p	:= 'B';
	
end if;
		

if	((nvl(ie_Regra_w,0) not in (1,2,5)) or
	(nvl(ie_glosa_w,'') not in ('T','E','R','B','H','Z',''))) and
	(ie_tipo_convenio_w <> 1) then
	vl_procedimento_w	:= 0;

end if;

ie_regra_p := ie_regra_w;

end gv_verifica_autorizacao_conv;
/