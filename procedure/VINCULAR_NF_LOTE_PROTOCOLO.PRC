create or replace
procedure	vincular_nf_lote_protocolo(
			nr_seq_nf_p			number,
			nr_seq_lote_prot_p		number,
			nm_usuario_p			varchar2,
			ds_erro_p		out	varchar2) is

qt_existe_w		number(10);
ds_erro_w		varchar2(255);
ds_observacao_w		varchar2(255);

begin

select	count(*)
into	qt_existe_w
from	nota_fiscal
where	nr_seq_lote_prot = nr_seq_lote_prot_p
and	ie_situacao = '1';

if	(qt_existe_w > 0) and
	(ds_erro_w is null) then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(281651);
end if;

select	count(*)
into	qt_existe_w
from	protocolo_convenio
where	nr_seq_lote_protocolo = nr_seq_lote_prot_p;

if	(qt_existe_w = 0) and
	(ds_erro_w is null) then
	ds_erro_w := WHEB_MENSAGEM_PCK.get_texto(281652);
end if;

if	(ds_erro_w is null) then
	begin

	update	nota_fiscal
	set	nr_seq_lote_prot = nr_seq_lote_prot_p,
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_seq_nf_p;

	ds_observacao_w	:= WHEB_MENSAGEM_PCK.get_texto(281653) || to_char(nr_seq_lote_prot_p) || WHEB_MENSAGEM_PCK.get_texto(281654);

	insert into nota_fiscal_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_nota,
		cd_evento,
		ds_historico) values (
			nota_fiscal_hist_seq.nextval,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_nf_p,
			'47',
			ds_observacao_w);

	end;
else
	ds_erro_p := ds_erro_w;
end if;

commit;

end vincular_nf_lote_protocolo;
/
