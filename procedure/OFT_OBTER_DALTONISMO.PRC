create or replace
procedure oft_obter_daltonismo (	nr_seq_consulta_p			number,
											nr_seq_consulta_form_p	number,
											cd_pessoa_fisica_p		varchar2,
											ie_opcao_p					varchar2,
											vListaDaltonismo			in out strRecTypeFormOft) is

dt_exame_w					oft_daltonismo.dt_registro%type;
ds_observacao_w			oft_daltonismo.ds_observacao%type;
vl_od_w						oft_daltonismo.vl_od%type;
vl_oe_w						oft_daltonismo.vl_oe%type;
ie_resultado_w				oft_daltonismo.ie_resultado%type;
cd_profissional_w			oft_daltonismo.cd_profissional%TYPE;
dt_liberacao_w				date;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type := wheb_usuario_pck.get_cd_estabelecimento;
nm_usuario_w				usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
ds_erro_w					varchar2(4000);

cursor	daltonismo_form is
	select	a.*
	from		oft_daltonismo a,
				oft_consulta_formulario b
	where		a.nr_seq_consulta_form 	=	b.nr_sequencia
	and		a.nr_seq_consulta_form 	=	nr_seq_consulta_form_p
	and		a.nr_seq_consulta			=	nr_seq_consulta_p
	and		((a.dt_liberacao is not null) or (a.nm_usuario = nm_usuario_w))
	and		((a.dt_inativacao is null) or (b.dt_inativacao is not null))
	order by dt_registro;
	
cursor	daltonismo_paciente is
	select	a.*
	from		oft_daltonismo a,
				oft_consulta b
	where		a.nr_seq_consulta		=	b.nr_sequencia
	and		b.cd_pessoa_fisica	=	cd_pessoa_fisica_p
	and		a.dt_liberacao 		is not null
	and		a.dt_inativacao 		is null
	and		b.nr_sequencia 		<> nr_seq_consulta_p
	order by dt_registro;
	
											
begin
begin

if	(nvl(nr_seq_consulta_p,0) > 0) and (vListaDaltonismo.count > 0) then
	if	(ie_opcao_p = 'F') then
		FOR c_daltonismo IN daltonismo_form LOOP
			begin
			dt_exame_w			:=	c_daltonismo.dt_registro;
			ds_observacao_w	:=	c_daltonismo.ds_observacao;
			vl_od_w				:=	c_daltonismo.vl_od;
			vl_oe_w				:=	c_daltonismo.vl_oe;
			ie_resultado_w		:=	c_daltonismo.ie_resultado;
			dt_liberacao_w		:=	c_daltonismo.dt_liberacao;
			cd_profissional_w	:=	c_daltonismo.cd_profissional;
			end;
		end loop;	
	else
		FOR c_daltonismo IN daltonismo_paciente LOOP
			begin
			ds_observacao_w	:=	c_daltonismo.ds_observacao;
			vl_od_w				:=	c_daltonismo.vl_od;
			vl_oe_w				:=	c_daltonismo.vl_oe;
			ie_resultado_w		:=	c_daltonismo.ie_resultado;
			cd_profissional_w	:=	obter_pf_usuario(nm_usuario_w,'C');
			dt_exame_w			:= sysdate;
			end;
		end loop;	
	end if;	

	for i in 1..vListaDaltonismo.count loop
		begin
		if	(ie_opcao_p = 'F') or (vListaDaltonismo(i).ie_obter_resultado = 'S') then
			vListaDaltonismo(i).dt_liberacao	:= dt_liberacao_w;
			case upper(vListaDaltonismo(i).nm_campo)
				WHEN 'CD_PROFISSIONAL' THEN
					vListaDaltonismo(i).ds_valor	:= cd_profissional_w;
				when 'DT_REGISTRO' then
					vListaDaltonismo(i).dt_valor	:= dt_exame_w;
				when 'DS_OBSERVACAO' then
					vListaDaltonismo(i).ds_valor	:=	ds_observacao_w;
				when 'VL_OD' then
					vListaDaltonismo(i).nr_valor	:=	vl_od_w;
				when 'VL_OE' then
					vListaDaltonismo(i).nr_valor	:=	vl_oe_w;
				when 'IE_RESULTADO' then
					vListaDaltonismo(i).ds_valor	:=	ie_resultado_w;
				else
					null;	
			end case;	
		end if;	
	end;
	end loop;
end if;	

exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,4000);
end;

end oft_obter_daltonismo;
/