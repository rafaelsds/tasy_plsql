create or replace
procedure atualiza_dev_cheque( nr_seq_cheque_p    number ,          
			     nm_usuario_p	varchar2) is 

begin

 update   cheque_cr  
 set         dt_devolucao   = null,
               nm_usuario     = nm_usuario_p
 where    nr_seq_cheque  = nr_seq_cheque_p;


commit;

end atualiza_dev_cheque;
/