create or replace
procedure pls_obter_se_existe_benef_ativ
		(	nr_seq_segurado_p			number,
			ie_existe_benef_ativo_p		out	varchar2,
			nr_contrato_p			out	number) is

ds_retorno_w		varchar2(1) := 'N';
cd_pessoa_fisica_w	pls_segurado.cd_pessoa_fisica%type;
nr_seq_plano_w		pls_segurado.nr_seq_plano%type;
dt_contratacao_w	pls_segurado.dt_contratacao%type;
nr_protocolo_ans_w	pls_plano.nr_protocolo_ans%type;
nr_contrato_w		pls_contrato.nr_contrato%type;
			
begin

select	cd_pessoa_fisica,
	nr_seq_plano,
	dt_contratacao
into	cd_pessoa_fisica_w,
	nr_seq_plano_w,
	dt_contratacao_w
from	pls_segurado
where	nr_sequencia = nr_seq_segurado_p;

select	nr_protocolo_ans
into	nr_protocolo_ans_w
from	pls_plano
where	nr_sequencia = nr_seq_plano_w;

select	max(c.nr_contrato)
into	nr_contrato_w
from	pls_contrato	c,
	pls_segurado	b,
	pls_plano	a
where	a.nr_sequencia 		 = b.nr_seq_plano
and	c.nr_sequencia		 = b.nr_seq_contrato
and	b.cd_pessoa_fisica 	 = cd_pessoa_fisica_w
and	trim(a.nr_protocolo_ans) = trim(nr_protocolo_ans_w)
and	(b.dt_rescisao is null or b.dt_rescisao	> dt_contratacao_w)
and	b.dt_liberacao is not null;

if	(nvl(nr_contrato_w,0) > 0) then
	ds_retorno_w := 'S';	
end if;

ie_existe_benef_ativo_p := ds_retorno_w;
nr_contrato_p := nr_contrato_w;

end pls_obter_se_existe_benef_ativ;
/