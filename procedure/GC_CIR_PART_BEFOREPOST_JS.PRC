create or replace
procedure gc_cir_part_BeforePost_js (
						nr_cirurgia_p		number,
						cd_pessoa_fisica_p	varchar2,
						nr_seq_procedimento_p	number,
						nr_seq_procpaci_p	number,
						ie_funcao_p		varchar2,
						nr_evento_p		number,
						cd_convenio_p		number,
						cd_categoria_p		varchar2,
						cd_edicao_amb_p		number,
						cd_procedimento_p	number,
						ie_origem_proced_p	number,
						dt_conta_p		date,
						nr_seq_proced_pac_p	number,
						nm_usuario_p		varchar2,
						cd_estabelecimento_p	number,
						ds_erro_p		out varchar2) is

consiste_lista_conv_med_w	varchar(255);
ie_contido_char			varchar(5);
ie_medico_cred_cirurgia_w	varchar(5);
ie_status_acerto_w		number(1) := 0;
consiste_qtd_participantes_w	varchar(1);
ie_regra_funcao_partic_cir_w	varchar(1);
ie_cod_espec_anestesista_w	varchar(255);
ie_anestesista_w		varchar(1);
ie_especialidade_w		varchar(1);
ie_consiste_parti_w		varchar(1);
ie_visua_partic_por_proced_w	varchar(1);
qt_de_anest_w			number(10);
qt_de_aux_w			number(10);
ie_auxiliar_w			varchar(1);
ie_aux_anest_w			varchar(3);
qt_de_permitida_w		number(10);
ie_Lib_Funcao_Espec_Medico	varchar2(1);

begin		

ds_erro_p := '';

obter_param_usuario(900, 255, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_cod_espec_anestesista_w);	
obter_param_usuario(900, 259, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, consiste_lista_conv_med_w);
obter_param_usuario(900, 187, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, consiste_qtd_participantes_w);
obter_param_usuario(900, 332, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_consiste_parti_w);
obter_param_usuario(900, 137, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_visua_partic_por_proced_w);
obter_param_usuario(900, 401, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p, ie_Lib_Funcao_Espec_Medico);


select	obter_se_contido_char(cd_convenio,consiste_lista_conv_med_w) 
into	ie_contido_char
from    cirurgia
where   nr_cirurgia = nr_cirurgia_p;

if	(nr_seq_procedimento_p is not null) then
	begin
	select	nvl(max(ie_status_acerto),1)
	into	ie_status_acerto_w
	from	conta_paciente a,procedimento_paciente b
	where	b.nr_sequencia = nr_seq_procedimento_p
	and	b.nr_interno_conta = a.nr_interno_conta;
	
	select  max(obter_regra_funcao_partic_cir(cd_estabelecimento_p,cd_convenio,cd_categoria,cd_procedimento,ie_origem_proced,dt_conta,nr_sequencia,ie_funcao_p))
	into 	ie_regra_funcao_partic_cir_w
	from    procedimento_paciente
	where   nr_sequencia = nr_seq_procedimento_p;
	end;
end if;

if	(consiste_lista_conv_med_w is not null) and
	(cd_pessoa_fisica_p is not null) and
	(ie_contido_char = 'S') and
	(consiste_medico_cred_cirurgia(nr_cirurgia_p,cd_pessoa_fisica_p) = 'N') then
	
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(47956);	
	
end if;

if 	(ie_Lib_Funcao_Espec_Medico = 'S') and
	(ie_funcao_p is not null) and
	(cd_pessoa_fisica_p is not null) and 
	(obter_lib_funcao_medic_espec(ie_funcao_p,cd_pessoa_fisica_p) = 'N')then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(117853);
end if;
		
if	(nr_seq_procedimento_p is not null) and
	(ie_status_acerto_w <> '1') then
	begin
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(47957);
	end;
end if;

if	(consiste_qtd_participantes_w = 'S') and
	(ie_regra_funcao_partic_cir_w = 'N') then
	begin
	ds_erro_p := 'DS_ERRO_P';
	--WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(47958);
	end;
end if;

				 
if	(ie_cod_espec_anestesista_w is not null) and
	(ie_funcao_p is not null) and
	(cd_pessoa_fisica_p is not null) then
	begin
	
	select	nvl(ie_anestesista,'N')
	into	ie_anestesista_w
	from    funcao_medico
	where	cd_funcao = ie_funcao_p;
	
		                                                 
	if	(ie_anestesista_w = 'S') then
		begin
		ie_especialidade_w := obter_se_especialidade_medico(cd_pessoa_fisica_p,ie_cod_espec_anestesista_w);
		
		if	(ie_especialidade_w = 'N') then
			begin
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(47959);			
			end;
		end if;
		end;
	end if;
	end;
end if;

if	(ie_consiste_parti_w = 'S') and
	(ie_visua_partic_por_proced_w = 'S') and
	(nr_evento_p = 1) then
	begin
	
	select	count(*)
	into	qt_de_anest_w
	from 	funcao_medico b,
		cirurgia_participante a
	where	a.ie_funcao = b.cd_funcao
	and      	a.nr_seq_procedimento =  nr_seq_proced_pac_p
	and 	a.nr_cirurgia = nr_cirurgia_p
	and   	ie_anestesista = 'S';
	
	select 	count(*)
	into	qt_de_aux_w
	from	funcao_medico b,
		cirurgia_participante a
	where	a.nr_cirurgia = nr_cirurgia_p
	and      	a.nr_seq_procedimento =  nr_seq_proced_pac_p
	and   	a.ie_funcao = b.cd_funcao 	
	and   	ie_auxiliar    = 'S';
	
	select	nvl(ie_anestesista,'N'),
		nvl(ie_auxiliar,'N')
	into	ie_anestesista_w,
		ie_auxiliar_w
	from	funcao_medico
	where  	cd_funcao = ie_funcao_p;
	
	if	(ie_anestesista_w = 'S') then
		begin
		ie_aux_anest_w := 'P';
		end;
	elsif	(ie_auxiliar_w = 'S') then
		begin
		ie_aux_anest_w := 'X';
		end;
	end if;
	
	if ((ie_anestesista_w <> 'N') or (ie_auxiliar_w <> 'N')) then
		qt_de_permitida_w :=  Obter_Dados_Preco_Proc(cd_estabelecimento_p,cd_convenio_p,cd_categoria_p,cd_edicao_amb_p,cd_procedimento_p,ie_origem_proced_p,dt_conta_p,ie_aux_anest_w);
	end if;
	
	if	(ie_aux_anest_w = 'P') and
		(qt_de_anest_w >= qt_de_permitida_w) then
		begin
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(66300);		
		end;
	elsif	(ie_aux_anest_w = 'X') and
		(qt_de_aux_w >= qt_de_permitida_w) then
		begin		
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(66300);
		end;
	end if;
	end;
end if;
		
		
commit;
end gc_cir_part_BeforePost_js;
/
