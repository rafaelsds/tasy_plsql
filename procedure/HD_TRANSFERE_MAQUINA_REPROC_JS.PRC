create or replace
procedure hd_transfere_maquina_reproc_js( 	nr_sequencia_p 		number,
						nr_seq_unid_origem_p	number,
						nr_seq_unid_destino_p	number,
						nr_seq_ponto_dialise_p	number,
						dt_transferencia_p	date,
						nm_usuario_p		varchar2,
						ie_opcao_p		varchar2,						
						ds_erro_p		out varchar2,
						ds_observacao_p		varchar2) is 
ds_erro_w	varchar2(255);					
ie_param_205_w	varchar2(1);
begin
obter_param_usuario(7009, 205, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_param_205_w);

if 	(ie_opcao_p = 'M') then
	HD_Transfere_Maquina(nr_sequencia_p, nr_seq_unid_origem_p, nr_seq_unid_destino_p, nr_seq_ponto_dialise_p, dt_transferencia_p, nm_usuario_p,ds_observacao_p, ds_erro_w);		
	
	ds_erro_p := ds_erro_w;
	
	if	(ie_param_205_w = 'S') and
		(ds_erro_w = '') then
		hd_transferir_equip_maquina(nr_sequencia_p,nr_seq_ponto_dialise_p,nm_usuario_p);
	end if;
		

elsif 	(ie_opcao_p = 'R') then
	HD_Transfere_Reproc(nr_sequencia_p, nr_seq_unid_origem_p, nr_seq_unid_destino_p, dt_transferencia_p, nm_usuario_p, ds_erro_w);

	ds_erro_p := ds_erro_w;
end if;

end hd_transfere_maquina_reproc_js;
/
