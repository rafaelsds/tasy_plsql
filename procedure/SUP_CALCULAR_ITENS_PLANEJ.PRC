create or replace
procedure sup_calcular_itens_planej(
			nr_seq_planejamento_p	number,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number) is

nr_seq_item_w			number(10);
cd_material_w			number(6);
cd_material_estoque_w		number(6);
dt_periodo_inicial_w		date;
dt_periodo_final_w			date;
dt_primeira_entrega_w		date;
dt_planejamento_w			date;
qt_dia_compra_w			number(15,4);
qt_dia_consumo_w			number(15,4);
qt_entregas_w			number(15,4);
qt_dia_interv_entrega_w		number(15,4);
qt_consumo_diario_w		number(15,4) := 0;
qt_tot_compra_w			number(15,4) := 0;
qt_entrega_w			number(15,4) := 0;
qt_estoque_w			number(13,4) := 0;
qt_cons_prev_prim_entrega_w	number(15,4) := 0;
qt_entrega_prev_prim_entrega_w	number(15,4) := 0;
qt_falta_prim_entrega_w		number(15,4) := 0;
qt_est_prev_entrega_w		number(15,4) := 0;
qt_dia_w				number(05,0) := 180;
qt_ordem_compra_w		number(15,4) := 0;
cd_unid_med_compra_w		varchar2(30);
cd_unid_med_compra_sc_w		varchar2(30);
qt_conv_compra_estoque_w		number(15,4) := 0;
qt_conv_estoque_consumo_w	number(15,4) := 0;
qt_solic_compra_w			number(15,4) := 0;
qt_cot_compra_w         		number(15,4) := 0;
nr_solic_compra_w			number(10,0);
nr_cot_compra_w			number(10,0);
nr_item_cot_compra_w		number(10,0);
qt_material_w			number(15,3);
qt_reg_w				number(05,0);
qt_estoque_minimo_w		number(13,4);
ie_origem_preco_w			varchar2(1);
vl_preco_unitario_w			number(13,4);
nr_seq_nf_w			number(10);
ie_data_base_compra_pend_w	varchar2(80);
qt_consumo_w			number(13,4);
qt_maximo_entrega_w		number(13,4);
qt_compra_melhor_w		number(15,4) := 0;
qt_compra_melhor_ajust_w		number(15,4) := 0;

dt_inicio_w	date;
dt_fim_w	date;

cursor C01 is
	select	a.nr_sequencia,
		a.cd_material	   
	from	sup_regra_planej_item a
	where	a.nr_seq_planejamento = nr_seq_planejamento_p;

cursor c02 is
	select	nr_solic_compra,
		nr_cot_compra,
		nr_item_cot_compra,
		qt_material,
		a.cd_unidade_medida_compra
	from	Solic_compra_item a
	where	a.dt_baixa is null
	and	a.cd_material in (
		select	cd_material
		from	material
		where	cd_material_estoque = cd_material_estoque_w)
	and	a.dt_atualizacao <= dt_primeira_entrega_w;

begin

select	dt_periodo_inicial,
	dt_periodo_final,
	dt_primeira_entrega,
	dt_planejamento,
	nvl(qt_dia_compra,0),
	nvl(qt_dia_consumo,0),
	nvl(qt_entregas,0),
	nvl(qt_dia_interv_entrega,0),
	nvl(ie_origem_preco,'U')
into	dt_periodo_inicial_w,
	dt_periodo_final_w,
	dt_primeira_entrega_w,
	dt_planejamento_w,
	qt_dia_compra_w,
	qt_dia_consumo_w,
	qt_entregas_w,
	qt_dia_interv_entrega_w,
	ie_origem_preco_w
from	sup_planejamento_compras
where	nr_sequencia = nr_seq_planejamento_p;

select	nvl(max(qt_dia_compra_pend), 180),
	nvl(max(ie_data_base_compra_pend), 'DT_ATUALIZACAO')
into	qt_dia_w,
	ie_data_base_compra_pend_W
from	parametro_compras
where	cd_estabelecimento	= cd_estabelecimento_p;

open C01;
loop
fetch C01 into
	nr_seq_item_w,
	cd_material_w;
exit when C01%notfound;
	begin
	select	cd_material_estoque,
		substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMC'),1,30) cd_unidade_medida_compra,
		qt_conv_compra_estoque,
		qt_conv_estoque_consumo,
		nvl(qt_compra_melhor,0)
	into	cd_material_estoque_w,
		cd_unid_med_compra_w,
		qt_conv_compra_estoque_w,
		qt_conv_estoque_consumo_w,
		qt_compra_melhor_w
	from	material
	where	cd_material = cd_material_w;

	if	(ie_origem_preco_w = 'U') then
		Obter_ultima_compra_material(
					cd_estabelecimento_p,cd_material_estoque_w,
					cd_unid_med_compra_w,'C',sysdate - 180,
					nr_seq_nf_w,vl_preco_unitario_w);
	elsif	(ie_origem_preco_w = 'F') then
		vl_preco_unitario_w	:= 0;
	elsif	(ie_origem_preco_w = 'M') then
		select	obter_custo_medio_material(cd_estabelecimento_p,sysdate,cd_material_estoque_w)
		into	vl_preco_unitario_w
		from	dual;
	end if;

	dt_inicio_w	:= trunc(sysdate,'dd') - qt_dia_consumo_w;
	dt_fim_w	:= (TRUNC(SYSDATE,'dd') + 86399/86400);

	/*Busca os consumo*/
	select	sum(qt_consumo)
	into	qt_consumo_w
	from	material m,
		movimento_estoque_v a
	where	a.cd_material_estoque	= m.cd_material_estoque
	and	m.cd_material		= cd_material_estoque_w
	and	a.cd_estabelecimento	= cd_estabelecimento_p
	and	cd_centro_custo is not null
	and	dt_movimento_estoque between dt_inicio_w and dt_fim_w;

	qt_consumo_diario_w	:= dividir(qt_consumo_w, qt_dia_consumo_w);


	/* Obter Ordens de compra pendentes  */
	select	nvl(sum(
		decode(cd_unidade_medida_compra, cd_unid_med_compra_w, nvl(c.qt_prevista_entrega,0) * qt_conv_compra_estoque_w, nvl(c.qt_prevista_entrega,0)) - 
		decode(cd_unidade_medida_compra, cd_unid_med_compra_w, nvl(c.qt_real_entrega,0) * qt_conv_compra_estoque_w, nvl(c.qt_real_entrega,0))), 0)
	into	qt_ordem_compra_w
	from 	ordem_compra b,
		ordem_compra_item a,
		ordem_compra_item_entrega c
	where 	a.nr_ordem_compra = b.nr_ordem_compra
	and	a.nr_ordem_compra = c.nr_ordem_compra
	and	a.nr_item_oci = c.nr_item_oci
	and	c.qt_prevista_entrega > nvl(c.qt_real_entrega,0)
	and	a.dt_reprovacao is null
  	and	b.dt_baixa is null
	and	b.cd_estabelecimento	= cd_estabelecimento_p
	and	trunc(c.dt_prevista_entrega,'dd') <= trunc(dt_primeira_entrega_w,'dd')
	and	c.dt_cancelamento is null
	and	decode(ie_data_base_compra_pend_w,
			'DT_ATUALIZACAO', a.dt_atualizacao,
			'DT_ORDEM_COMPRA', b.dt_ordem_compra,
			'DT_ENTREGA', b.dt_entrega) > sysdate - qt_dia_w
	and	a.cd_material 	in (
			select	cd_material
			from	material
			where	cd_material_estoque = cd_material_estoque_w);

	qt_solic_compra_w	:= 0;
	qt_cot_compra_w		:= 0;
	
	/* Obter solicita��o e cota��o de compra pendentes*/
	open c02;
	loop
	fetch c02 into
		nr_solic_compra_w,
		nr_cot_compra_w,
		nr_item_cot_compra_w,
		qt_material_w,
		cd_unid_med_compra_sc_w;
	exit when c02%notfound;
		begin
		select	decode(cd_unid_med_compra_sc_w, cd_unid_med_compra_w,nvl(sum(qt_material_w),0) * qt_conv_compra_estoque_w, nvl(sum(qt_material_w),0))
		into	qt_material_w
		from	solic_compra
	        where	nr_solic_compra	= nr_solic_compra_w
	        and	dt_liberacao is not null
	        and	cd_estabelecimento = cd_estabelecimento_p
	        and	dt_baixa is null;

		if	(nr_cot_compra_w is null) then
			qt_solic_compra_w := qt_solic_compra_w + qt_material_w;
		else	begin
			select	count(*)
			into	qt_reg_w
			from	ordem_compra_item
      			where	nr_cot_compra		= nr_cot_compra_w
	  		and	nr_item_cot_compra	= nr_item_cot_compra_w;
	
			if	(qt_reg_w = 0) then
				qt_cot_compra_w := qt_cot_compra_w + qt_material_w;
			end if;
			end;
		end if;
		end;
	end loop;
	close c02;

	qt_estoque_w			:= obter_saldo_disp_estoque(cd_estabelecimento_p, cd_material_w, null, trunc(sysdate,'mm'));
	qt_estoque_minimo_w		:= nvl(obter_mat_estabelecimento(cd_estabelecimento_p, 0, cd_material_w, 'MI'),0);
	qt_entrega_prev_prim_entrega_w	:= nvl((qt_ordem_compra_w + qt_solic_compra_w + qt_cot_compra_w),0);
	qt_cons_prev_prim_entrega_w	:= nvl((trunc(dt_primeira_entrega_w) - trunc(dt_planejamento_w)) * qt_consumo_diario_w, 0);
	qt_est_prev_entrega_w		:= nvl((qt_estoque_w + qt_entrega_prev_prim_entrega_w - dividir(dividir(qt_cons_prev_prim_entrega_w, qt_conv_compra_estoque_w), qt_conv_estoque_consumo_w)), 0);
	qt_falta_prim_entrega_w		:= qt_cons_prev_prim_entrega_w - qt_entrega_prev_prim_entrega_w - qt_estoque_w + qt_estoque_minimo_w;
	qt_tot_compra_w			:= round(dividir(dividir(
						(qt_consumo_diario_w * qt_dia_compra_w) - (qt_estoque_w - qt_estoque_minimo_w + qt_entrega_prev_prim_entrega_w),
					qt_conv_compra_estoque_w), qt_conv_estoque_consumo_w));
	qt_entrega_w			:= nvl(trunc(dividir(qt_tot_compra_w, qt_entregas_w)),0);

	select	nvl(sup_obter_regra_planej_compra(cd_estabelecimento_p, cd_material_w,'Q'),0)
	into	qt_maximo_entrega_w
	from	dual;
	
	if	(qt_maximo_entrega_w > 0) and
		(qt_entrega_w > qt_maximo_entrega_w) then
		qt_entrega_w := qt_maximo_entrega_w;
	end if;

	if	(qt_compra_melhor_w <> 0) then
		while (qt_compra_melhor_ajust_w < qt_tot_compra_w) loop
			begin
			qt_compra_melhor_ajust_w := qt_compra_melhor_ajust_w + qt_compra_melhor_w;
			end;
		end loop;
	end if;

	update	sup_regra_planej_item
	set	dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p,
		qt_consumo_diario		= qt_consumo_diario_w,
		qt_tot_compra		= qt_tot_compra_w,
		qt_entrega		= qt_entrega_w,
		qt_est_atual		= qt_estoque_w,
		qt_cons_prev		= qt_cons_prev_prim_entrega_w,
		qt_entrega_prev		= qt_entrega_prev_prim_entrega_w,
		qt_falta_prim_entrega	= qt_falta_prim_entrega_w,
		qt_est_prev_entrega	= qt_est_prev_entrega_w,
		vl_preco_unitario		= vl_preco_unitario_w,
		qt_estoque_minimo		= qt_estoque_minimo_w,
		qt_compra_ajustada     	= decode(qt_compra_melhor_ajust_w,0,null,qt_compra_melhor_ajust_w)
	where	nr_sequencia		= nr_seq_item_w;
	end;
end loop;
close c01;

commit;

end sup_calcular_itens_planej;
/