create or replace
procedure ctb_copiar_distr_saldo_conta(
					nr_seq_regra_rat_cont_p		number,
					cd_empresa_origem_p             number,
					cd_estab_origem_p		number,
					cd_estab_destino_p              number,
					ie_copiar_todos_estab_p		varchar2,
					nm_usuario_p			Varchar2) is

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
nr_seq_regra_rat_contab_w	ctb_regra_rat_contabil.nr_sequencia%type;

Cursor c_estabelecimento is
	select	cd_estabelecimento
	from	estabelecimento
	where	cd_empresa = cd_empresa_origem_p
	and	cd_estabelecimento <> cd_estab_origem_p
	and	((ie_copiar_todos_estab_p = 'S')
	or	(cd_estabelecimento = cd_estab_destino_p));

cursor	c_regra_dest is
	select	cd_centro_custo_dest,
		cd_conta_contabil_dest,
		nr_seq_grupo,
		nr_sequencia,
		pr_rateio
	from	ctb_regra_rat_dest
	where	nr_seq_regra_rateio	= nr_seq_regra_rat_cont_p;

vet_regra_dest		c_regra_dest%rowtype;

begin

open c_estabelecimento;
loop
fetch c_estabelecimento into
	cd_estabelecimento_w;
exit when c_estabelecimento%notfound;
	begin

	select	ctb_regra_rat_contabil_seq.nextval
	into	nr_seq_regra_rat_contab_w
	from	dual;
	
	insert into ctb_regra_rat_contabil(
			nr_sequencia,
			nm_usuario,
			nm_usuario_nrec,
			dt_atualizacao,
			dt_atualizacao_nrec,
			cd_centro_origem,
			cd_conta_origem,
			cd_estabelecimento,
			cd_historico,
			ds_titulo,
			dt_final_vigencia,
			dt_inicio_vigencia,
			ie_regra_rat_saldo,
			nr_seq_calculo,
			pr_total_rateio)
		(select	nr_seq_regra_rat_contab_w,
			nm_usuario_p,
			nm_usuario_p,
			sysdate,
			sysdate,
			cd_centro_origem,
			cd_conta_origem,
			cd_estabelecimento_w,
			cd_historico,
			ds_titulo,
			dt_final_vigencia,
			dt_inicio_vigencia,
			ie_regra_rat_saldo,
			nr_seq_calculo,
			pr_total_rateio
		from	ctb_regra_rat_contabil
		where	nr_sequencia	= nr_seq_regra_rat_cont_p);

	open c_regra_dest;
	loop
	fetch c_regra_dest into
		vet_regra_dest;
	exit when c_regra_dest%notfound;
		begin

		insert into ctb_regra_rat_dest(
			nr_sequencia,
			dt_atualizacao,
			dt_atualizacao_nrec,
			nm_usuario,
			nm_usuario_nrec,
			cd_centro_custo_dest,
			cd_conta_contabil_dest,
			nr_seq_grupo,
			nr_seq_regra_rateio,
			pr_rateio)
		values (
			ctb_regra_rat_dest_seq.nextval,
			sysdate,
			sysdate,
			nm_usuario_p,
			nm_usuario_p,
			vet_regra_dest.cd_centro_custo_dest,
			vet_regra_dest.cd_conta_contabil_dest,
			vet_regra_dest.nr_seq_grupo,
			nr_seq_regra_rat_contab_w,
			vet_regra_dest.pr_rateio);

		end;
	end loop;
	close c_regra_dest;

	end;
end loop;
close c_estabelecimento;

commit;

end ctb_copiar_distr_saldo_conta;
/