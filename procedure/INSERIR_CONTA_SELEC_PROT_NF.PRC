create or replace
procedure	inserir_conta_selec_prot_nf(
			nr_interno_conta_p		number,
			nr_seq_protocolo_p		number,
			ie_excluir_ant_p			varchar2,
			nm_usuario_p			varchar2) is

qt_existe_w		number(10);

begin

if	(nvl(ie_excluir_ant_p,'N') = 'S') then
	begin

	delete
	from	w_contas_selecionadas_prot
	where	nm_usuario = nm_usuario_p;

	end;
end if;

select	count(*)
into	qt_existe_w
from	conta_paciente_nf
where	nr_interno_conta = nr_interno_conta_p;

insert into w_contas_selecionadas_prot(
	nr_interno_conta,
	nr_seq_protocolo,
	ie_possui_nf,
	dt_atualizacao,
	nm_usuario)
values (
	nr_interno_conta_p,
	nr_seq_protocolo_p,
	decode(qt_existe_w,0,'N','S'),
	sysdate,
	nm_usuario_p);

commit;

end inserir_conta_selec_prot_nf;
/