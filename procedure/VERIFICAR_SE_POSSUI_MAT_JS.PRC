create or replace
procedure verificar_se_possui_mat_js (	nr_prescricao_p		number,
					ds_lista_seq_p		varchar2,
					ds_mensagem_p		out varchar2) is

lista_informacao_w	varchar2(1000);
ie_contador_w		number(10,0)	:= 0;
tam_lista_w		number(10,0);
ie_pos_virgula_w	number(3,0);
nr_seq_prescr_w		number(10,0);
ie_exige_w		varchar2(1) := 'N';
ie_exige_peso_coleta_w varchar2(1) := 'N';

begin

lista_informacao_w	:= ds_lista_seq_p;

if (substr(lista_informacao_w, length(lista_informacao_w), 1) <> ',') then
  lista_informacao_w := lista_informacao_w || ',';
end if;

while	(((ie_exige_w = 'N') or (ie_exige_peso_coleta_w = 'N')) and
	(lista_informacao_w is not null)) loop
	begin
	
	tam_lista_w			:= length(lista_informacao_w);
	ie_pos_virgula_w		:= instr(lista_informacao_w,',');

	if	(ie_pos_virgula_w <> 0) then
		nr_seq_prescr_w	:= substr(lista_informacao_w,1,(ie_pos_virgula_w - 1));
		lista_informacao_w	:= substr(lista_informacao_w,(ie_pos_virgula_w + 1),tam_lista_w);
	end if;
	
	ie_exige_w := lab_obter_se_exige_alt_peso(nr_prescricao_p,nr_seq_prescr_w);
	ie_exige_peso_coleta_w := lab_obter_exige_alt_peso_col(nr_prescricao_p,nr_seq_prescr_w);
	
	if	((ie_exige_w = 'S') or (ie_exige_peso_coleta_w = 'S')) then

		ds_mensagem_p :=  obter_desc_expressao(620355);
	end if;
	
	end;
end loop;

end verificar_se_possui_mat_js;
/