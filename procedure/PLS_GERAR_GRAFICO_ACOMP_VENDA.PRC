create or replace
procedure pls_gerar_grafico_acomp_venda
			(	nr_seq_equipe_p		number,
				nr_seq_canal_venda_p	number,
				dt_referencia_inicial_p	date,
				dt_referencia_final_p	date,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
			
dt_referencia_w			date;
qt_meta_global_w		number(10);
qt_atual_global_w		number(10);
pr_atual_global_w		number(7,4);
qt_meta_pf_w			number(10);
qt_atual_pf_w			number(10);
pr_atual_pf_w			number(7,4);
qt_meta_pj_w			number(10);
qt_atual_pj_w			number(10);
pr_atual_pj_w			number(7,4);
qt_paga_atual_w			number(10);

Cursor C01 is
	select	dt_mes
	from	mes_v
	where	dt_mes between dt_referencia_inicial_p and dt_referencia_final_p;

begin

delete	pls_grafico_acomp_venda
where	nr_seq_equipe	= nr_seq_equipe_p
and	dt_referencia between dt_referencia_inicial_p and dt_referencia_final_p;

gravar_processo_longo('Gerar mensalidades' ,'PLS_GERAR_GRAFICO_ACOMP_VENDA',0);

open C01;
loop
fetch C01 into	
	dt_referencia_w;
exit when C01%notfound;
	begin
	
	gravar_processo_longo(to_char(dt_referencia_w,'Month/yyyy'),'PLS_GERAR_GRAFICO_ACOMP_VENDA',-1);
	
	
	if	(nr_seq_canal_venda_p is null) then
		qt_meta_global_w	:= pls_obter_qt_meta_equipe(nr_seq_equipe_p,null,dt_referencia_w,'G');
		qt_meta_pf_w		:= pls_obter_qt_meta_equipe(nr_seq_equipe_p,null,dt_referencia_w,'PF');
		qt_meta_pj_w		:= pls_obter_qt_meta_equipe(nr_seq_equipe_p,null,dt_referencia_w,'PJ');
		
		select	count(*)
		into	qt_atual_global_w
		from	pls_contrato		e,
			pls_segurado		d,
			pls_vendedor		c,
			pls_equipe_vend_vinculo b,
			pls_equipe_vendedor	a
		where	b.nr_seq_equipe		= a.nr_sequencia
		and	b.nr_seq_vendedor	= c.nr_sequencia
		and	d.nr_seq_vendedor_canal	= c.nr_sequencia
		and	d.nr_seq_contrato	= e.nr_sequencia
		and	a.nr_sequencia		= nr_seq_equipe_p
		and	trunc(dt_contratacao,'Month')		= dt_referencia_w
		and	dt_liberacao is not null
		and	dt_referencia_w between nvl(b.dt_inicio_vigencia, dt_referencia_w) and nvl(b.dt_fim_vigencia, dt_referencia_w);
		
		select	count(*)
		into	qt_atual_pf_w
		from	pls_contrato		e,
			pls_segurado		d,
			pls_vendedor		c,
			pls_equipe_vend_vinculo b,
			pls_equipe_vendedor	a
		where	b.nr_seq_equipe		= a.nr_sequencia
		and	b.nr_seq_vendedor	= c.nr_sequencia
		and	d.nr_seq_vendedor_canal	= c.nr_sequencia
		and	d.nr_seq_contrato	= e.nr_sequencia
		and	a.nr_sequencia		= nr_seq_equipe_p
		and	e.cd_cgc_estipulante is null
		and	trunc(dt_contratacao,'Month')		= dt_referencia_w
		and	dt_liberacao is not null
		and	dt_referencia_w between nvl(b.dt_inicio_vigencia, dt_referencia_w) and nvl(b.dt_fim_vigencia, dt_referencia_w);
		
		select	count(*)
		into	qt_atual_pj_w
		from	pls_contrato		e,
			pls_segurado		d,
			pls_vendedor		c,
			pls_equipe_vend_vinculo b,
			pls_equipe_vendedor	a
		where	b.nr_seq_equipe		= a.nr_sequencia
		and	b.nr_seq_vendedor	= c.nr_sequencia
		and	d.nr_seq_vendedor_canal	= c.nr_sequencia
		and	d.nr_seq_contrato	= e.nr_sequencia
		and	a.nr_sequencia		= nr_seq_equipe_p
		and	e.cd_cgc_estipulante is not null
		and	trunc(dt_contratacao,'Month')		= dt_referencia_w
		and	dt_liberacao is not null
		and	dt_referencia_w between nvl(b.dt_inicio_vigencia, dt_referencia_w) and nvl(b.dt_fim_vigencia, dt_referencia_w);
				
		begin
		if	(qt_meta_global_w	= 0) then
			pr_atual_global_w	:= dividir((qt_atual_global_w*100),1);
		else
			pr_atual_global_w	:= dividir((qt_atual_global_w*100),qt_meta_global_w);
		end if;
		exception
		when others then
			pr_atual_global_w	:= 0;
		end;
		
		
		IF	(qt_meta_pf_w = 0) then
			begin
			pr_atual_pf_w		:= dividir((qt_atual_pf_w*100),1);
			exception
			when others then
			pr_atual_pf_w		:= 100;
			end;
		else
			pr_atual_pf_w		:= dividir((qt_atual_pf_w*100),qt_meta_pf_w);
		end if;
		
		
		if	(qt_meta_pj_w	= 0) then
			begin
			pr_atual_pj_w		:= dividir(qt_atual_pj_w*100,1);
			exception
			when others then
			pr_atual_pj_w		:= 100;
			end;
		else
			pr_atual_pj_w		:= dividir(qt_atual_pj_w*100,qt_meta_pj_w);
		end if;
		
		
		qt_paga_atual_w		:= pls_obter_resumo_equipe_vend(nr_seq_equipe_p,dt_referencia_w,'GP');
		
	else
		
		qt_meta_global_w	:= pls_qt_metas_canal_vendas(nr_seq_equipe_p,null,dt_referencia_w,'G');
		qt_meta_pf_w		:= pls_qt_metas_canal_vendas(nr_seq_equipe_p,null,dt_referencia_w,'PF');
		qt_meta_pj_w		:= pls_qt_metas_canal_vendas(nr_seq_equipe_p,null,dt_referencia_w,'PJ');
		
		select	count(*)
		into	qt_atual_global_w
		from	pls_segurado		c,
			pls_contrato		b,
			pls_vendedor		a
		where	c.nr_seq_vendedor_canal	= a.nr_sequencia
		and	c.nr_seq_contrato	= b.nr_sequencia
		and	a.nr_sequencia		= nr_seq_canal_venda_p
		and	trunc(dt_contratacao,'Month')		= dt_referencia_w
		and	dt_liberacao is not null;
		
		select	count(*)
		into	qt_atual_pf_w
		from	pls_segurado		c,
			pls_contrato		b,
			pls_vendedor		a
		where	c.nr_seq_vendedor_canal	= a.nr_sequencia
		and	c.nr_seq_contrato	= b.nr_sequencia
		and	a.nr_sequencia		= nr_seq_canal_venda_p
		and	trunc(dt_contratacao,'Month')		= dt_referencia_w
		and	b.cd_cgc_estipulante is null
		and	dt_liberacao is not null;
		
		select	count(*)
		into	qt_atual_pj_w
		from	pls_segurado		c,
			pls_contrato		b,
			pls_vendedor		a
		where	c.nr_seq_vendedor_canal	= a.nr_sequencia
		and	c.nr_seq_contrato	= b.nr_sequencia
		and	a.nr_sequencia		= nr_seq_canal_venda_p
		and	trunc(dt_contratacao,'Month')		= dt_referencia_w
		and	b.cd_cgc_estipulante is not null
		and	dt_liberacao is not null;
		
		
		begin
		if	(qt_meta_global_w	= 0) then
			pr_atual_global_w	:= dividir((qt_atual_global_w*100),1);
		else
			pr_atual_global_w	:= dividir((qt_atual_global_w*100),qt_meta_global_w);
		end if;
		exception
		when others then
			pr_atual_global_w	:= 0;
		end;
		
		
		IF	(qt_meta_pf_w = 0) then
			begin
			pr_atual_pf_w		:= dividir((qt_atual_pf_w*100),1);
			exception
			when others then
			pr_atual_pf_w		:= 100;
			end;
		else
			pr_atual_pf_w		:= dividir((qt_atual_pf_w*100),qt_meta_pf_w);
		end if;
		
		
		if	(qt_meta_pj_w	= 0) then
			begin
			pr_atual_pj_w		:= dividir(qt_atual_pj_w*100,1);
			exception
			when others then
			pr_atual_pj_w		:= 100;
			end;
		else
			pr_atual_pj_w		:= dividir(qt_atual_pj_w*100,qt_meta_pj_w);
		end if;
		
		
		qt_paga_atual_w		:= pls_obter_resumo_equipe_canal(nr_seq_canal_venda_p,dt_referencia_w,'GP');
	end if;
		
		insert into pls_grafico_acomp_venda
			(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
				nr_seq_equipe,qt_meta_pf,qt_meta_global,qt_meta_pj,qt_atual_pf,
				qt_atual_global,qt_atual_pj,pr_meta_pf,pr_meta_global,pr_meta_pj,
				dt_referencia,qt_atual_pago)
		values	(	pls_grafico_acomp_venda_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
				nr_seq_equipe_p,qt_meta_pf_w,qt_meta_global_w,qt_meta_pj_w,qt_atual_pf_w,
				qt_atual_global_w,qt_atual_pj_w,pr_atual_pf_w,pr_atual_global_w,pr_atual_pj_w,
				dt_referencia_w,qt_paga_atual_w);
	
	end;
end loop;
close C01;


commit;

end pls_gerar_grafico_acomp_venda;
/