create or replace
procedure copiar_cron_migr_adic_ativ (
		cd_funcao_origem_p	number,
		cd_funcao_destino_p	number,
		nm_usuario_p		varchar2) is
		
begin
if	(cd_funcao_origem_p is not null) and
	(cd_funcao_destino_p is not null) and
	(nm_usuario_p is not null) then
	begin
	if	(1 = 2) then
		begin
		copiar_cron_migr_adic_menuitem(cd_funcao_origem_p, cd_funcao_destino_p, nm_usuario_p);
		copiar_cron_migr_adic_dbpanel(cd_funcao_origem_p, cd_funcao_destino_p, nm_usuario_p);
		copiar_cron_migr_adic_wcpanel(cd_funcao_origem_p, cd_funcao_destino_p, nm_usuario_p);
		copiar_cron_migr_adic_wdlg(cd_funcao_origem_p, cd_funcao_destino_p, nm_usuario_p);
		copiar_cron_migr_adic_wscb(cd_funcao_origem_p, cd_funcao_destino_p, nm_usuario_p);
		end;
	end if;
	end;
end if;
commit;
end copiar_cron_migr_adic_ativ;
/