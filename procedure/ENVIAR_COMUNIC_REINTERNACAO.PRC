create or replace
procedure enviar_comunic_reinternacao(	nr_atendimento_p	number,
					cd_pessoa_fisica_p	varchar2,
					ds_perfil_p		varchar2,
					qt_dia_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is
qt_reg_w		number(5);

begin

select	count(*)
into	qt_reg_w
from	atendimento_paciente
where	cd_pessoa_fisica	= cd_pessoa_fisica_p
and	nr_atendimento <> nr_atendimento_p
and	ie_tipo_atendimento = 1 
and	dt_alta >= (sysdate - qt_dia_p);

if	(qt_reg_w >= 1) then

	insert	into comunic_interna (
		dt_comunicado,
		ds_titulo,
		ds_comunicado,
		nm_usuario,
		dt_atualizacao,
		ie_geral,
		nm_usuario_destino,
		nr_sequencia,
		ie_gerencial,
		nr_seq_classif,
		ds_perfil_adicional,
		cd_setor_destino,
		cd_estab_destino,
		ds_setor_adicional,
		dt_liberacao)
	values (sysdate,
		substr(obter_texto_tasy(294579,wheb_usuario_pck.get_nr_seq_idioma),1,255),
		obter_texto_dic_objeto(294580, wheb_usuario_pck.get_nr_seq_idioma, 'PACIENTE='||obter_nome_pf(cd_pessoa_fisica_p)) || chr(13) || chr(10) || chr(13) || chr(10) ||
		obter_texto_dic_objeto(294581, wheb_usuario_pck.get_nr_seq_idioma, 'NR_ATENDIMENTO='||nr_atendimento_p) || chr(13) || chr(10) || chr(13) || chr(10) ||
		obter_texto_dic_objeto(294582, wheb_usuario_pck.get_nr_seq_idioma, 'QT_DIA='||qt_dia_p),
		nm_usuario_p,
		sysdate,
		'N',
		null,
		comunic_interna_seq.nextval,
		'N',
		null,
		ds_perfil_p,
		null,
		cd_estabelecimento_p,
		null,
		sysdate);

	commit;

	end	if;

end enviar_comunic_reinternacao;
/