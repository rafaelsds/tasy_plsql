create or replace
procedure pls_alterar_via_carteira	
		(	nr_seq_segurado_p	number,
			nr_via_p		number,
			nm_usuario_p		varchar2,
			cd_estabelecimento_p	number) is
					
nr_seq_carteira_w		number(10);


begin

select	nr_sequencia
into	nr_seq_carteira_w
from	pls_segurado_carteira
where	nr_seq_segurado = nr_seq_segurado_p;

update	pls_segurado_carteira
set	nr_via_solicitacao 	= nr_via_p,
	dt_atualizacao		= sysdate,
	nm_usuario		= nm_usuario_p
where	nr_sequencia		= nr_seq_carteira_w;			

/*Gravar hist�rico no benefici�rio*/
pls_gerar_segurado_historico(	nr_seq_segurado_p, '70', sysdate, 'Alterado o n�mero da via da carteira.',
				'pls_alterar_via_carteira', null, null, null,
				null, sysdate, null, null,
				null, null, null, null,
				nm_usuario_p, 'N');		
commit;

end pls_alterar_via_carteira;
/