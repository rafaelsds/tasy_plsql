create or replace procedure incluir_deducao_reinf_2070 (nr_sequencia_p 	number,
							cd_estabelecimento_p 	number,
							nm_usuario_p 		varchar2,
							cd_tipo_deducao_p	number,
							vl_reducao_p		number) is

begin
			
			insert into fis_reinf_ded_r2070 (
				nr_sequencia,
				cd_estabelecimento,
				dt_atualizacao,
				nm_usuario,
				cd_tipo_deducao,
				vl_reducao,
				nr_seq_reg_sup) 
			values(	fis_reinf_ded_r2070_seq.nextval,
				cd_estabelecimento_p,
				sysdate,
				nm_usuario_p,
				cd_tipo_deducao_p,
				vl_reducao_p,
				nr_sequencia_p);
	
commit;

end incluir_deducao_reinf_2070;
/