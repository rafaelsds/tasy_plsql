create or replace
procedure man_atribuir_feature_release(
			nr_feature_p		number,
			nr_release_p		number,
			nr_release_atual_p	number,
			nm_usuario_p		varchar2,
			ds_erro_p	out	varchar2) is 

nr_release_planejado_w	desenv_release.nr_sequencia%type;
dt_release_planejado_w	desenv_release.dt_inicial%type;
dt_release_final_plan_w	desenv_release.dt_final%type;
dt_final_w		desenv_release.dt_final%type;
qt_existe_w		number(10);
qt_stories_release_w	number(10);
			
begin

select	max(dt_final)
into	dt_final_w
from	desenv_release
where	nr_sequencia = nr_release_p;

if	(dt_final_w < sysdate) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336102); --Este PI j� terminou.
	return;
end if;

select	count(*)
into	qt_existe_w
from	desenv_feature_release a,
	desenv_release b
where	nr_feature = nr_feature_p
and	a.nr_release = b.nr_sequencia
and	((nr_release_p = a.nr_release) or (not sysdate between b.dt_inicial and b.dt_final))
and	b.dt_final > sysdate
and	nvl(nr_release,0) <> nvl(nr_release_atual_p,0);

if	(qt_existe_w > 0) then
	ds_erro_p := wheb_mensagem_pck.get_texto(336103); --Esta Feature j� foi planejada..
	return;
end if;

select	max(a.nr_release),
	max(b.dt_inicial),
	max(b.dt_final)
into	nr_release_planejado_w,
	dt_release_planejado_w,
	dt_release_final_plan_w
from	desenv_feature_release a,
	desenv_release b
where	a.nr_release = b.nr_sequencia
and	a.nr_feature = nr_feature_p
and	b.nr_sequencia = nr_release_atual_p;

select	count(*)
into	qt_stories_release_w
from	desenv_story_sprint a,
	desenv_sprint b,
	desenv_story c
where	b.nr_release = nr_release_atual_p
and	a.nr_story = c.nr_sequencia
and	c.nr_feature = nr_feature_p
and	a.nr_sprint is not null;

if	(dt_release_planejado_w > sysdate) or
	((dt_release_planejado_w is not null) and (qt_stories_release_w = 0)) then
	update	desenv_feature_release
	set	nr_release = nr_release_p
	where	nr_release = nr_release_planejado_w
	and	nr_feature = nr_feature_p;
else
	delete	from desenv_feature_release
	where	nr_feature = nr_feature_p
	and	nr_release is null;
	insert	into desenv_feature_release(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_feature,
		nr_release)
	values(	desenv_story_sprint_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_feature_p,
		nr_release_p);
end if;

commit;

end man_atribuir_feature_release;
/