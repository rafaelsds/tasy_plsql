create or replace
PROCEDURE gerar_resultado_eclipse (  	nr_prescricao_p		VARCHAR2,
										cd_exame_p			VARCHAR2,
										nr_seq_interno_p	VARCHAR2,
										ds_tipo_resultado_p	VARCHAR2,
										ds_resultado_p		VARCHAR2,
										ds_observacao_p		VARCHAR2,
										ds_referencia_p		VARCHAR2,
										ds_result_status_p	VARCHAR2,
										ds_date_time_obs_p	VARCHAR2,
										ds_units_p			VARCHAR2,
										nm_usuario_p		VARCHAR2,
										cd_exame_princ_p	OUT VARCHAR2) IS


nr_seq_prescr_w    		prescr_procedimento.nr_sequencia%TYPE;
ie_status_atend_w		prescr_procedimento.ie_status_atend%TYPE;
ie_status_receb_w		prescr_procedimento.ie_status_atend%TYPE;

cd_exame_princ_w		exame_laboratorio.cd_exame%TYPE;
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%TYPE;
ie_sexo_w				pessoa_fisica.ie_sexo%TYPE;
nr_seq_resultado_w		exame_lab_resultado.nr_seq_resultado%TYPE;
nr_seq_exame_princ_w	exame_laboratorio.nr_seq_exame%TYPE;

cd_analito_w			exame_laboratorio.cd_exame%TYPE;
nr_seq_analito_w		exame_laboratorio.nr_seq_exame%TYPE;

nr_seq_material_w		material_exame_lab.nr_sequencia%TYPE;

ds_resultado_w			exame_lab_result_item.ds_resultado%TYPE;
qt_resultado_w			exame_lab_result_item.qt_resultado%TYPE;
pr_resultado_w			exame_lab_result_item.pr_resultado%TYPE;

nr_anos_w				NUMBER(3);
ds_erro_w				VARCHAR2(4000);

ie_campo_calculo_w		VARCHAR2(1);
ie_formato_resultado_w	VARCHAR2(3);

BEGIN

SELECT 	MAX(nr_sequencia),
		MAX(ie_status_atend),
		MAX(NVL(b.cd_exame_integracao, b.cd_exame)),
		MAX(a.nr_seq_exame)
INTO	nr_seq_prescr_w,
		ie_status_atend_w,
		cd_exame_princ_w,
		nr_seq_exame_princ_w
FROM 	prescr_procedimento a,
		exame_laboratorio b
WHERE 	a.nr_seq_exame = b.nr_seq_exame
AND		a.nr_prescricao = nr_prescricao_p
AND		a.nr_seq_interno = nr_seq_interno_p
AND		NVL(a.ie_suspenso,'N') = 'N'
AND		NVL(b.ie_situacao,'A') = 'A';

cd_exame_princ_p := cd_exame_princ_w;

SELECT	MAX(cd_estabelecimento),
        MAX(obter_sexo_prescricao(nr_prescricao))
INTO	cd_estabelecimento_w,
        ie_sexo_w
FROM	prescr_medica
WHERE	nr_prescricao = nr_prescricao_p;

SELECT	MAX(ie_status_receb)
INTO	ie_status_receb_w
FROM	lab_parametro
WHERE	cd_estabelecimento = cd_estabelecimento_w;

SELECT  TRUNC((SYSDATE -  dt_nascimento)/ 365.25)
INTO   	nr_anos_w
FROM 	prescr_medica a,
        pessoa_fisica b
WHERE 	a.cd_pessoa_fisica = b.cd_pessoa_fisica
AND		a.nr_prescricao = nr_prescricao_p;

IF (ie_status_atend_w <= 30) THEN --nvl(ie_status_receb_w,30)) then

	SELECT  MAX(a.nr_seq_resultado),
	        MAX(b.nr_seq_material)
	INTO	nr_seq_resultado_w,
	        nr_seq_material_w
	FROM	exame_lab_resultado a,
			exame_lab_result_item b
	WHERE	a.nr_seq_resultado = b.nr_seq_resultado
	AND		a.nr_prescricao = nr_prescricao_p
	AND		b.nr_seq_prescr	= nr_seq_prescr_w
	AND		b.nr_seq_material IS NOT NULL;

IF 	(nr_seq_resultado_w IS NULL) THEN
	gera_exame_result_lab(nr_prescricao_p, nr_seq_prescr_w, 'N', 'N', 'S', nm_usuario_p);

		SELECT 	MAX(a.nr_seq_resultado),
				MAX(b.nr_seq_material)
		INTO	nr_seq_resultado_w,
				nr_seq_material_w
		FROM	exame_lab_resultado a,
				exame_lab_result_item b
		WHERE	a.nr_seq_resultado = b.nr_seq_resultado
		AND		a.nr_prescricao = nr_prescricao_p
		AND		b.nr_seq_prescr	= nr_seq_prescr_w
		AND 	b.nr_seq_material	IS NOT NULL;
	END IF;

	SELECT	MAX(NVL(e.cd_exame_integracao, e.cd_exame)),
			MAX(e.nr_seq_exame)
	INTO 	cd_analito_w,
			nr_seq_analito_w
	FROM	exame_laboratorio e
	WHERE 	e.nr_seq_exame = (	SELECT 	nr_seq_exame
								FROM 	equipamento_lab b,
										lab_exame_equip a
								WHERE	a.cd_equipamento = b.cd_equipamento
								AND		a.cd_exame_equip = cd_exame_p
								AND		UPPER(b.ds_sigla) = 'ECLIPSE'
								AND		a.nr_seq_exame = e.nr_seq_exame)
	START WITH nr_seq_superior = nr_seq_exame_princ_w
	CONNECT BY PRIOR nr_seq_exame = nr_seq_superior;

	gera_resultado_lab( nr_seq_resultado_w,	nr_prescricao_p,nr_seq_prescr_w, nr_seq_exame_princ_w, nr_seq_material_w, nr_anos_w, ie_sexo_w,	nm_usuario_p);


	gravar_log_lab(1234, 'atualizar_lab_result_item ('||nr_prescricao_p||', '||nr_seq_prescr_w||', nvl('||cd_analito_w||','||cd_exame_p||'), '||ds_resultado_p||', 0, '||ds_resultado_p||', , null, n, eclipse, to_date('||ds_date_time_obs_p||', yyyymmddhh24miss), '||ds_referencia_p||', '||ds_units_p||', null, null, ds_erro_w, null);','ddmarco');

	SELECT	MAX(ie_formato_resultado)
	INTO	ie_formato_resultado_w
	FROM	exame_laboratorio
	WHERE	nr_seq_exame	 = NVL(nr_seq_analito_w, nr_seq_exame_princ_w);

	IF (SUBSTR(ie_formato_resultado_w,1,1) = 'P') THEN
		SELECT 	TO_NUMBER(NVL(REPLACE(ds_resultado_p, '.', ','),0))
		INTO   	pr_resultado_w
		FROM 	dual;
	ELSIF (SUBSTR(ie_formato_resultado_w,1,1) = 'V') OR
		  (SUBSTR(ie_formato_resultado_w,2,1) = 'V') THEN
		BEGIN
		SELECT 	TO_NUMBER(NVL(ds_resultado_p,0))
		INTO 	qt_resultado_w
		FROM 	dual;
		EXCEPTION
			WHEN OTHERS THEN
			BEGIN
			SELECT 	TO_NUMBER(NVL(REPLACE(ds_resultado_p, '.', ','),0))
			INTO 	qt_resultado_w
			FROM 	dual;
			EXCEPTION
				WHEN OTHERS THEN
					BEGIN
					SELECT 	TO_NUMBER(NVL(REPLACE(ds_resultado_p, ',', '.'),0))
					INTO 	qt_resultado_w
					FROM 	dual;

					EXCEPTION
						WHEN OTHERS THEN
						ds_resultado_w := trim(ds_resultado_p);
					END;
			END;
		END;
	ELSE
		ds_resultado_w := trim(ds_resultado_p);
	END IF;


	atualizar_lab_result_item (nr_prescricao_p, nr_seq_prescr_w, NVL(cd_analito_w,cd_exame_princ_w), qt_resultado_w, pr_resultado_w, ds_resultado_w, ds_observacao_p, NULL, 'N', 'ECLIPSE', TO_DATE(ds_date_time_obs_p, 'yyyymmddhh24miss'), ds_referencia_p, ds_units_p, NULL, NULL, ds_erro_w, nr_seq_analito_w);

COMMIT;
END IF;

END gerar_resultado_eclipse;
/
