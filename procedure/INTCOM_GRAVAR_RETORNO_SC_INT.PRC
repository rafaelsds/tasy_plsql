create or replace
procedure intcom_gravar_retorno_sc_int(	nr_seq_registro_p		number,					
					ie_status_p		varchar2,
					ie_sistema_origem_p	varchar2,
					ds_erro_p			varchar2,
					ds_retorno_p		varchar2,
					ds_xml_p			long,
					nm_usuario_p		Varchar2) is 
qt_existe_w		number(10);
ie_tipo_operacao_w	varchar2(30);
nr_solic_compra_w	number(10);

begin

select	count(*)
into	qt_existe_w
from	registro_integr_com_xml
where	nr_seq_registro	= nr_seq_registro_p
and	ie_operacao	= 'E'
and	ie_status		= 'NP';

if	(qt_existe_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(173194);
end if;

select	nr_solic_compra
into	nr_solic_compra_w
from	registro_integr_compras
where	nr_sequencia = nr_seq_registro_p;

select	nvl(max(ie_tipo_operacao),'')
into	ie_tipo_operacao_w
from	registro_integr_compras
where	nr_sequencia = nr_seq_registro_p;

insert into registro_integr_com_xml(
	nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_registro,
	ie_status,
	ie_operacao,
	ie_sistema_origem,
	ds_erro,
	ds_retorno,
	ds_xml,
	ie_tipo_operacao)
values(	registro_integr_com_xml_seq.nextval,
	sysdate,
	nm_usuario_p,
	sysdate,
	nm_usuario_p,
	nr_seq_registro_p,
	ie_status_p,
	'R',
	ie_sistema_origem_p,
	ds_erro_p,
	ds_retorno_p,
	ds_xml_p,
	ie_tipo_operacao_w);
	
if	(ie_status_p = 'P') then	
	
	update	solic_compra
	set	nr_documento_externo = to_number(ds_retorno_p)
	where	nr_solic_compra = nr_solic_compra_w;
		
	insert into solic_compra_hist(	
		nr_sequencia,
		nr_solic_compra,
		dt_atualizacao,
		nm_usuario,
		dt_historico,
		ds_titulo,
		ds_historico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo,
		cd_evento,
		dt_liberacao)
	values(	solic_compra_hist_seq.nextval,
		nr_solic_compra_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		WHEB_MENSAGEM_PCK.get_texto(279973),
		WHEB_MENSAGEM_PCK.get_texto(279974),
		sysdate,
		nm_usuario_p,
		'S',
		'V',
		sysdate);	
	
elsif	(ie_status_p = 'E') then

	insert into solic_compra_hist(	
		nr_sequencia,
		nr_solic_compra,
		dt_atualizacao,
		nm_usuario,
		dt_historico,
		ds_titulo,
		ds_historico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_tipo,
		cd_evento,
		dt_liberacao)
	values(	solic_compra_hist_seq.nextval,
		nr_solic_compra_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		WHEB_MENSAGEM_PCK.get_texto(279975),
		WHEB_MENSAGEM_PCK.get_texto(279976) || chr(13) || chr(10) || WHEB_MENSAGEM_PCK.get_texto(279977) || ds_erro_p,
		sysdate,
		nm_usuario_p,
		'S',
		'V',
		sysdate);
		
	/* Caso erro no envio, desmarca os itens como integrados */
	update solic_compra_item
	set IE_ENVIADO_INTEGR = 'N'
	where nr_solic_compra = nr_solic_compra_w;
		
	end if;

update	registro_integr_com_xml
set	ie_status	= ie_status_p,
	dt_atualizacao	= sysdate,
	nm_usuario	= nm_usuario_p,
	id_pdc		= ds_retorno_p
where	nr_seq_registro = nr_seq_registro_p
and	ie_operacao	= 'E';

commit;

end intcom_gravar_retorno_sc_int;
/
