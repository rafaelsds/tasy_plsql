create or replace
procedure gerar_log_avaliacao_paciente(	nr_seq_avaliacao_p	number,
					nm_usuario_p		varchar2) is 
qt_itens_w	number(5);
begin

select	count(*)
into	qt_itens_w
from	Med_Avaliacao_Paciente
where	nr_sequencia = nr_seq_avaliacao_p;


if	(qt_itens_w > 0) and
	(nr_seq_avaliacao_p > 0) and
	(nm_usuario_p is not null) then
	
	insert into	med_avaliacao_paciente_log
		(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_avaliacao
		)
	values
		(
		med_avaliacao_paciente_log_seq.NextVal,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_avaliacao_p
		);
	
end if;

commit;

end gerar_log_avaliacao_paciente;
/
