create or replace
procedure Gerar_prontuario_CIHA(
			nm_usuario_p		Varchar2) is 

			
nr_atendimento_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
qt_sem_prontuario_W	number(3);			
			
cursor 	c01 is
	select 	distinct nr_atendimento
	from	lote_cih_item b
	where	nvl(nr_prontuario,0) = 0
	and	nvl(nr_atendimento,0) > 0;
			
begin

open C01;
loop
fetch C01 into	
	nr_atendimento_w;
exit when C01%notfound;
	begin
	
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;
	
	if	(nvl(cd_pessoa_fisica_w,0) > 0) then
		begin
		select	rownum
		into	qt_sem_prontuario_W
		from	pessoa_fisica
		where	cd_pessoa_fisica = cd_pessoa_fisica_w
		and	nvl(nr_prontuario,0) > 0
		and	rownum = 1;
		exception
			when 	no_data_found then
				qt_sem_prontuario_w := 0;
		end;
		Wheb_mensagem_pck.exibir_mensagem_abort( 264152 , 'QT_SEM_PRONTUARIO_W='||qt_sem_prontuario_w);
		if	(nvl(qt_sem_prontuario_w,0) > 0) then
			gerar_prontuario_paciente(cd_pessoa_fisica_w);
		end if;
	end if;
	exception
	when others then
	null;
	end;
end loop;
close C01;

commit;

end Gerar_prontuario_CIHA;
/
