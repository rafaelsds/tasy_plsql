create or replace
procedure update_acao_glosa_lote(	vl_glosa_p		number,
				vl_amenor_p		number,
				ie_acao_glosa_p		varchar2,
				nr_seq_item_p		number) is
begin

if	(vl_glosa_p is not null) and
	(vl_amenor_p is not null) and
	(ie_acao_glosa_p is not null) and
	(nr_seq_item_p is not null) then
	begin
	update	lote_audit_hist_item
	set	ie_acao_glosa	= ie_acao_glosa_p,
		vl_glosa		= vl_glosa_p,
		vl_amenor	= vl_amenor_p
	where	nr_sequencia	= nr_seq_item_p; 
	end;
end if;
commit;

end update_acao_glosa_lote;
/