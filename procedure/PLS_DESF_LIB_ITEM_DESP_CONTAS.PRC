create or replace
procedure pls_desf_lib_item_desp_contas
			(	nr_seq_conta_p 		number, 
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2) is 
				
qt_ocorrencia_pend_w		number(9,3);
ie_tipo_despesa_w		varchar2(5);
qt_apresentado_w		varchar2(9);


Cursor C01 is
	select	qt_ocorrencia_pend,
		qt_apresentado,
		ie_tipo_despesa
	from	pls_resumo_conta
	where	nr_seq_conta	= nr_seq_conta_p;

begin
open C01;
loop
fetch C01 into	
	qt_ocorrencia_pend_w, 
	qt_apresentado_w,
	ie_tipo_despesa_w;
exit when C01%notfound;
	begin
		if ((qt_ocorrencia_pend_w = 0)and (qt_apresentado_w <> 0)) then
			pls_desfazer_lib_item_despesa(nr_seq_conta_p, ie_tipo_despesa_w, cd_estabelecimento_p, nm_usuario_p);
		end if;
	end;
end loop;
close C01;

end pls_desf_lib_item_desp_contas;
/