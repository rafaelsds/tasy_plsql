create or replace
procedure atualiza_just_excesso_serv	(	nr_seq_unid_p		Number,
						ds_motivo_p		varchar2,
						nm_usuario_p		Varchar2) is 

begin
if 	(nvl(nr_seq_unid_p,0) > 0) and
	(ds_motivo_p is not null)  then

	update	sl_unid_atend
	set	ds_just_tempo_serv	= ds_motivo_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia 		= nr_seq_unid_p;
	
end if;
commit;

end atualiza_just_excesso_serv;
/