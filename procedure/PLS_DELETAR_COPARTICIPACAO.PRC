create or replace
procedure pls_deletar_coparticipacao
			(	nr_seq_conta_p			number,
				nr_seq_conta_copartic_p 	number,
				ie_commit_p			varchar2,
				ie_rec_glosa_p			varchar2,
				nr_seq_rec_glosa_conta_p	pls_rec_glosa_conta.nr_sequencia%type,
				nr_seq_protocolo_p		pls_rec_glosa_protocolo.nr_sequencia%type,
				nm_usuario_p			varchar2,
				cd_estabelecimento_p		varchar2
				) is

qt_lote_liberacao_w		pls_integer;
nr_seq_conta_w			pls_conta_coparticipacao.nr_seq_conta%type;
nr_seq_conta_proc_w		pls_conta_coparticipacao.nr_seq_conta_proc%type;
nr_seq_conta_mat_w		pls_conta_coparticipacao.nr_seq_conta_mat%type;
nr_seq_mensalidade_seg_w	pls_conta_coparticipacao.nr_seq_mensalidade_seg%type;
nr_seq_lote_w			pls_ted_copartic_pagador.nr_seq_lote%type;
nr_seq_ted_benef_w		pls_ted_copartic_item.nr_seq_ted_benef%type;
nr_lote_contabil_estorno_w	pls_conta_coparticipacao.nr_lote_contabil_estorno%type;
nr_lote_contabil_prov_w		pls_conta_coparticipacao.nr_lote_contabil_prov%type;
nr_seq_regra_limite_copartic_w	pls_conta_coparticipacao.nr_seq_regra_limite_copartic%type;
qt_registros_mensalidade_w	number(10);

cursor c_registros_copart is 
	select 	a.nr_sequencia
	from	pls_conta_coparticipacao a,
			pls_rec_glosa_conta b
	where	b.nr_sequencia = a.nr_seq_conta_rec
	and		b.nr_seq_protocolo = nr_seq_protocolo_p;

begin
-- Se ele passar o numero da coparticipa��o, ele s� vai deletar aquela coparticioa��o, se apenas passar o numero da conta ele vai  deletar todas as contas,


if ( ie_rec_glosa_p = 'S') then

	if (nr_seq_protocolo_p is not null) then
		
		for r_c_registros_copart in  c_registros_copart loop
		
			delete from pls_conta_copartic_aprop
			where	nr_seq_conta_coparticipacao = r_c_registros_copart.nr_sequencia;
		

			delete from 	pls_conta_coparticipacao 
			where 	nr_Sequencia = r_c_registros_copart.nr_sequencia; 
				
		end loop;
	else
	
		delete from pls_conta_copartic_aprop
		where	nr_seq_conta_coparticipacao in (	select 	nr_sequencia 
													from 	pls_conta_coparticipacao 
													where 	nr_seq_conta_rec = nr_seq_rec_glosa_conta_p);
									

		delete from pls_conta_coparticipacao 
		where 	nr_seq_conta_rec = nr_seq_rec_glosa_conta_p; 
		
	end if;

else

	if	(nr_seq_conta_copartic_p is not null) then
		select	max(nr_seq_mensalidade_seg),
			nvl(max(nr_lote_contabil_estorno),0),
			nvl(max(nr_lote_contabil_prov),0),
			max(nr_seq_regra_limite_copartic)
		into	nr_seq_mensalidade_seg_w,
			nr_lote_contabil_estorno_w,
			nr_lote_contabil_prov_w,
			nr_seq_regra_limite_copartic_w
		from	pls_conta_coparticipacao
		where	nr_sequencia	= nr_seq_conta_copartic_p;
		
		
		--Utilizei o max para podes exibir um lote na mensagem(Caso tiver).
		select 	max(nr_seq_ted_benef)
		into	nr_seq_ted_benef_w
		from	pls_ted_copartic_item
		where   nr_seq_conta_copartic = nr_seq_conta_copartic_p;
		
		--Registro de coparticipa��o com v�nculo com lote no TED.
		if	(nr_seq_ted_benef_w is not null) then
			--Busca o lote de controle de copartic da fun��o TED
			select 	max(nr_seq_lote)
			into	nr_seq_lote_w
			from	pls_ted_copartic_pagador
			where	nr_sequencia =  ( select nr_seq_ted_pagador
						  from	 pls_ted_copartic_benef a
						  where	 nr_sequencia = nr_seq_ted_benef_w);
			
			wheb_mensagem_pck.exibir_mensagem_abort(360130, 'NR_SEQ_LOTE='||nr_seq_lote_w);
		end if;
		
		if	((nr_seq_mensalidade_seg_w is null) and (nr_seq_regra_limite_copartic_w is null)) then
			select  count(1)
			into 	qt_registros_mensalidade_w
			from  	pls_mensalidade_item_conta a
			where  	a.nr_seq_conta_copartic = nr_seq_conta_copartic_p;
		else
			qt_registros_mensalidade_w	:= 0;
		end if;
		
		if	((nr_seq_mensalidade_seg_w is not null) or (nr_seq_regra_limite_copartic_w is not null) or (qt_registros_mensalidade_w > 0)) then
			wheb_mensagem_pck.exibir_mensagem_abort(328665); --J� existe mensalidade para a coparticipa��o Favor verificar.	
		elsif	(nr_lote_contabil_estorno_w = 0) and -- somente pode excluir coparticipa��o se n�o foi contabilizada
			(nr_lote_contabil_prov_w = 0) then
			select	count(1)
			into	qt_lote_liberacao_w
			from	pls_lib_coparticipacao
			where	nr_seq_conta_coparticipacao	= nr_seq_conta_copartic_p;
			
			if	(qt_lote_liberacao_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(328668); --J� existe lote de libera��o para a coparticipa��o. Favor verificar na fun��o OPS - Controle de Coparticipa��es.
			else
				select	nr_seq_conta_mat,
					nr_seq_conta_proc
				into	nr_seq_conta_mat_w,
					nr_seq_conta_proc_w
				from	pls_conta_coparticipacao
				where	nr_sequencia = nr_seq_conta_copartic_p;
				
				delete	pls_conta_mat_aprop
				where	nr_seq_conta_mat = nr_seq_conta_mat_w;
				
				delete	pls_conta_proc_aprop
				where	nr_seq_conta_proc = nr_seq_conta_proc_w;
				
				delete	pls_conta_copartic_aprop
				where	nr_seq_conta_coparticipacao = nr_seq_conta_copartic_p;
							
				delete	from pls_coparticipacao_critica
				where	nr_seq_lib_copartic in (select 	nr_sequencia 
								from 	pls_lib_coparticipacao 
								where	nr_seq_conta_coparticipacao = nr_seq_conta_copartic_p);
				
				delete from pls_lib_coparticipacao 
				where	nr_seq_conta_coparticipacao = nr_seq_conta_copartic_p;
				
				delete	pls_conta_coparticipacao
				where	nr_sequencia	= nr_seq_conta_copartic_p;
				
				update	pls_conta_proc set
					vl_coparticipacao = null
				where	nr_sequencia = nr_seq_conta_proc_w;
			end if;
		end if;
	elsif	(nr_seq_conta_p is not null) then
		select	max(nr_seq_mensalidade_seg),
			nvl(max(nr_lote_contabil_estorno),0),
			nvl(max(nr_lote_contabil_prov),0),
			max(nr_seq_regra_limite_copartic)
		into	nr_seq_mensalidade_seg_w,
			nr_lote_contabil_estorno_w,
			nr_lote_contabil_prov_w,
			nr_seq_regra_limite_copartic_w
		from	pls_conta_coparticipacao
		where	nr_seq_conta	= nr_seq_conta_p;
		
		--Utilizei o max para podes exibir um lote na mensagem(Caso tiver).
		select 	max(nr_seq_ted_benef)
		into	nr_seq_ted_benef_w
		from	pls_ted_copartic_item
		where  nr_seq_conta_copartic in ( select nr_sequencia 
								  from	 pls_conta_coparticipacao
								  where	 nr_seq_conta	= nr_seq_conta_p);
		
		--Registro de coparticipa��o com v�nculo com lote no TED.
		if	(nr_seq_ted_benef_w is not null) then
			--Busca o lote de controle de copartic da fun��o TED
			
			select 	max(nr_seq_lote)
			into	nr_seq_lote_w
			from	pls_ted_copartic_pagador
			where	nr_sequencia =  ( select nr_seq_ted_pagador
						  from	 pls_ted_copartic_benef a
						  where	 nr_sequencia = nr_seq_ted_benef_w);
			
			wheb_mensagem_pck.exibir_mensagem_abort(360130, 'NR_SEQ_LOTE='||nr_seq_lote_w);
			
		end if;
		
		if	((nr_seq_mensalidade_seg_w is null) and (nr_seq_regra_limite_copartic_w is null)) then
			select  count(1)
			into 	qt_registros_mensalidade_w
			from  	pls_mensalidade_item_conta a
			where  	a.nr_seq_conta_copartic = nr_seq_conta_copartic_p;
		else
			qt_registros_mensalidade_w	:= 0;
		end if;

		if	((nr_seq_mensalidade_seg_w is not null) or (nr_seq_regra_limite_copartic_w is not null) or (qt_registros_mensalidade_w > 0)) then
			wheb_mensagem_pck.exibir_mensagem_abort(328665); --J� existe mensalidade para a coparticipa��o Favor verificar.
		elsif	(nr_lote_contabil_estorno_w = 0) and -- somente pode excluir coparticipa��o se n�o foi contabilizada
			(nr_lote_contabil_prov_w = 0) then
			select	count(1) 
			into	qt_lote_liberacao_w
			from	pls_lib_coparticipacao a,
				pls_lote_coparticipacao b
			where	a.nr_seq_conta	= nr_seq_conta_p
			and	a.nr_seq_lote = b.nr_sequencia
			and	b.ie_status = 'D'; --Definitivo
			
			if	(qt_lote_liberacao_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(328668); --J� existe lote de libera��o para a coparticipa��o. Favor verificar na fun��o OPS - Controle de Coparticipa��es.
			else
				delete	pls_conta_copartic_aprop
				where	nr_seq_conta_coparticipacao in (select  nr_sequencia
									from	pls_conta_coparticipacao
									where	nr_seq_conta  = nr_seq_conta_p);
				
				delete	pls_conta_mat_aprop
				where	nr_seq_conta_mat in (	select	nr_sequencia
								from	pls_conta_mat
								where	nr_seq_conta  = nr_seq_conta_p);
				
				delete	pls_conta_proc_aprop
				where	nr_seq_conta_proc in (	select	nr_sequencia
								from	pls_conta_proc
								where	nr_seq_conta  = nr_seq_conta_p);
				
				delete	from pls_coparticipacao_critica
				where	nr_seq_lib_copartic in (select 	a.nr_sequencia 
								from 	pls_lib_coparticipacao a,
									pls_conta_coparticipacao b
								where	a.nr_seq_conta_coparticipacao 	= b.nr_sequencia
								and	b.nr_seq_conta 			= nr_seq_conta_p);
				
				delete from pls_lib_coparticipacao 
				where	nr_seq_conta_coparticipacao in (select	nr_sequencia 
									from 	pls_conta_coparticipacao 
									where 	nr_seq_conta = nr_seq_conta_p);
				
				
				/*
					Esse update � em car�ter tempor�rio, pois ser� realizado um ajuste ainda em rela��o a pls_mensalidade_item_conta
					OS1222239
				*/
				update 	pls_mensalidade_item_conta 
				set 	nr_seq_conta_copartic = null 
				where  nr_seq_conta_copartic in( select nr_sequencia
								from	pls_conta_coparticipacao 
								where	nr_seq_conta = nr_seq_conta_p);
				
				delete	pls_conta_coparticipacao
				where	nr_seq_conta	= nr_seq_conta_p;
				
				update	pls_conta_proc set
					vl_coparticipacao = null
				where	nr_seq_conta	= nr_seq_conta_p;
			end if;
		end if;
	end if;
end if;
	
if	(ie_commit_p = 'S') then
	commit;
end if;

end pls_deletar_coparticipacao;
/