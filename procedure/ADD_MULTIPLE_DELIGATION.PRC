create or replace procedure add_multiple_deligation (
    cd_cargo_p            number,
    cd_departamento_p     number,
    proxy_list_p          varchar2,
    profile_list_p        varchar2,
    dt_inicio_delegacao_p date,
    dt_fim_delegacao_p    date,
    cd_razao_p            number,
    ds_observacao_p       varchar2
) is

proxy_list_w       varchar2(2000);
begin
    proxy_list_w    := nvl(proxy_list_p, '-');
    for rec_w in (select a.nr_registro as cd_profile, b.cd_registro nm_proxy from  table(lista_pck.obter_lista(profile_list_p,',')) a,
    table(lista_pck.obter_lista_char(proxy_list_w,',')) b)
    loop
        insert into
            perfil_delegacao(
                nr_sequencia,
                nm_usuario_nrec,
                nm_usuario,
                dt_atualizacao_nrec,
                dt_atualizacao,
                cd_cargo,
                cd_departamento,
                cd_perfil,
                cd_razao,
                ds_observacao,
				ie_situacao,
                dt_fim_delegacao,
                dt_inicio_delegacao,
                nm_proxy)
            values (PERFIL_DELEGACAO_SEQ.nextval,
                wheb_usuario_pck.get_nm_usuario,
                wheb_usuario_pck.get_nm_usuario,
                sysdate,
                sysdate,
                cd_cargo_p,
                cd_departamento_p,
                rec_w.cd_profile,
                cd_razao_p,
                ds_observacao_p,
				'A',
                dt_fim_delegacao_p,
                dt_inicio_delegacao_p,
                decode(rec_w.nm_proxy, '-', null, rec_w.nm_proxy)
            );
    end loop;
    commit;

end add_multiple_deligation;
/
