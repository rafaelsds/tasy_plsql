create or replace
procedure liberar_fatura(
		nr_sequencia_p		number,
		nm_usuario_p		varchar2) is 

begin
	update   via_fatura 
	set      dt_liberacao = sysdate,               
		 nm_usuario   = nm_usuario_p   
	where    nr_sequencia = nr_sequencia_p;
commit;

end liberar_fatura;
/