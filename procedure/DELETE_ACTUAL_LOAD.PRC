create or replace 
procedure delete_actual_load( 
          			NR_SEQUENCIA_P                	GER_CARGA_INICIAL.NR_SEQUENCIA%type,
         			IE_INICIO_IMPORTACAO_P    OUT  	Varchar2) is 

DT_INICIO_IMPORTACAO_W	GER_CARGA_INICIAL.DT_INICIO_IMPORTACAO%type;

begin

select	DT_INICIO_IMPORTACAO 
into	DT_INICIO_IMPORTACAO_W
from	GER_CARGA_INICIAL 
where	NR_SEQUENCIA = NR_SEQUENCIA_P; 

if (DT_INICIO_IMPORTACAO_W is not null) then
	begin
		IE_INICIO_IMPORTACAO_P := 'S';
	end;
else
	begin
		IE_INICIO_IMPORTACAO_P := 'N';
		delete from	GER_CARGA_INICIAL 
		where       	NR_SEQUENCIA = NR_SEQUENCIA_P;
  	end;
end if;
commit;
end delete_actual_load;
/
