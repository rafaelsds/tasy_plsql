create or replace
procedure pls_atualiza_guia_imp_xml(	ie_opcao_p			varchar2,
					nr_seq_guia_p			number,
					nm_prestador_imp_p		varchar2,
					cd_cnes_imp_p			varchar2,
					nr_seq_prestador_imp_p		number,
					cd_cgc_prestador_imp_p		varchar2,
					cd_cpf_prestador_imp_p		varchar2,
					nm_usuario_p			varchar2) is 

/* ie_opcao_p
'C' - Contratado
 */			
			
begin
if 	(ie_opcao_p is not null) then
	if 	(ie_opcao_p = 'C') then
		update 	pls_guia_plano
		set   	nm_prestador_imp     	= nm_prestador_imp_p,
			cd_cnes_imp		= cd_cnes_imp_p,
			nr_seq_prestador_imp	= nr_seq_prestador_imp_p,
			cd_cgc_prestador_imp	= cd_cgc_prestador_imp_p,
			cd_cpf_prestador_imp	= cd_cpf_prestador_imp_p
		where	nr_sequencia		= nr_seq_guia_p;
	end if;
	
	commit;
end if;

end pls_atualiza_guia_imp_xml;
/