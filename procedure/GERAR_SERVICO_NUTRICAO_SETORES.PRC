create or replace procedure gerar_servico_nutricao_setores(ds_lista_servico_p	varchar2,
				nr_atendimento_p	number,
				cd_setor_atendimento_p	number,
				ie_status_gestao_p	varchar2,
				dt_referencia_p		date,
				nm_usuario_p		Varchar2,
				cd_estabelecimento_p	number,
				nr_prescricao_p		number) is 
					
nr_atendimento_w			number(10);	
cd_setor_Atendimento_w			number(5);
ds_lista_servico_w			varchar2(4000);	
ie_pos_virgula_w			number(3);
tam_lista_w				number(10);		
nr_seq_servico_w			number(10);	
nr_seq_serv_dia_w			number(10);
dt_servico_w				date;
qt_servicos_w				number(10);
ds_observacao_w				varchar2(4000);
ds_observacao_acomp_w			varchar2(255);
dt_referencia_w				date;
ds_horarios_w				varchar2(5);
nm_pessoa_fisica_w			varchar(255);
nr_Seq_acompanhante_w			number(10);
qt_dieta_acomp_w			number(10);
ie_gera_acomp_w				varchar2(1);
ds_consistencia_w			varchar2(4000);
cd_acompanhante_w			varchar2(10);
nm_acompanhante_w			varchar2(80);
nr_seq_atend_acompanhante_w		number(10);
nr_seq_atend_acomp_novo_w		number(10);
cd_dieta_w				number(10);
ie_servico_hora_inf_w			varchar2(1);

i					number(10);

/*Parametros*/
ie_gera_servico_Acompanhante_w		varchar2(1);
ie_acompanhante_dia_anterior_w		varchar2(1);
ie_preencher_auto_obs_w			varchar2(1);
ie_gerar_auto_conduta_dieta_w		varchar2(1);
qtd_w					number(10);



/*CURSOR C01 IS
	SELECT	b.nr_atendimento,
		a.cd_setor_atendimento
	FROM    atendimento_paciente b,
		ocupacao_unidade_v a,
		setor_atendimento s
	WHERE   a.cd_setor_atendimento = s.cd_setor_atendimento
	AND	a.nr_atendimento = b.nr_atendimento
	AND     a.nr_atendimento IS NOT NULL
	AND 	((SUBSTR(Nut_Obter_Status_Atend(a.nr_atendimento, a.cd_setor_atendimento, dt_referencia_p),1,30) = ie_status_gestao_p) OR (ie_status_gestao_p = 'T'))
	AND	((cd_setor_atendimento_p = 0) OR (a.cd_setor_atendimento = cd_setor_atendimento_p))
	AND	((a.nr_atendimento = nr_atendimento_p) OR (nr_atendimento_p = 0))
	AND	s.cd_classif_setor IN ('3','4')
	UNION
	SELECT	b.nr_atendimento,
		obter_setor_atendimento(b.nr_atendimento)
	FROM    atendimento_paciente b,
		setor_atendimento s
	WHERE	s.cd_setor_atendimento = obter_setor_atendimento(b.nr_atendimento)
	AND 	((SUBSTR(Nut_Obter_Status_Atend(b.nr_atendimento, obter_setor_atendimento(b.nr_atendimento), dt_referencia_p),1,30) = ie_status_gestao_p) OR (ie_status_gestao_p = 'T'))
	AND	((b.nr_atendimento = nr_atendimento_p) OR (nr_atendimento_p = 0))
	AND	 s.cd_classif_setor NOT IN ('3','4');
*/

Cursor C01 is
	select	nr_atendimento,
		cd_setor_atendimento
	from	w_nutricao_setores
	where	((nr_atendimento = nr_atendimento_p) OR (nr_atendimento_p = 0))
	AND	((cd_setor_atendimento_p = 0) OR (cd_setor_atendimento = cd_setor_atendimento_p))
	and	((SUBSTR(Nut_Obter_Status_Atend(nr_atendimento, cd_setor_atendimento, dt_referencia_p),1,30) = ie_status_gestao_p) OR (ie_status_gestao_p = 'T'));
	
	


cursor c03 is
	select	nr_sequencia
	from	nut_atend_serv_dia
	where	nr_atendimento 		= nr_atendimento_w
	and	cd_setor_atendimento 	= cd_setor_atendimento_w
	and	nr_Seq_servico 		= nr_seq_servico_w
	and	dt_liberacao is null
	and	dt_servico between inicio_dia(dt_referencia_w) and  fim_dia(dt_referencia_w);
	
cursor c04 is
	select	b.cd_pessoa_fisica,
		substr(nvl(obter_nome_pf(b.cd_pessoa_fisica), b.nm_pessoa_fisica),1,80),
		b.ds_observacao,
		b.nr_sequencia
	from  	nut_atend_serv_dia a,
		nut_atend_acompanhante b
	where	a.nr_sequencia = b.nr_seq_atend_serv_dia
	and	((b.cd_pessoa_fisica is not null) or (b.nm_pessoa_fisica is not null))
	and	a.nr_seq_servico 	= nr_seq_servico_w
	and	dt_servico between inicio_dia(dt_referencia_w-1) and  fim_dia(dt_referencia_w-1)
	and	a.nr_atendimento = nr_atendimento_w;
	
cursor c05 is
	select cd_dieta
	from   nut_atend_acomp_dieta a
	where  a.nr_seq_atend_acomp = nr_seq_atend_acompanhante_w;
					
begin

ie_gera_servico_Acompanhante_w	:= Obter_Valor_Param_Usuario(1003, 50, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p);
ie_acompanhante_dia_anterior_w	:= nvl(Obter_Valor_Param_Usuario(1003, 59, Obter_Perfil_Ativo, nm_usuario_p, cd_estabelecimento_p),'N');
ie_servico_hora_inf_w		:= obter_valor_param_usuario(1003,37,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
ie_preencher_auto_obs_w		:= obter_valor_param_usuario(1003,16,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);
ie_gerar_auto_conduta_dieta_w	:= obter_valor_param_usuario(1003,36,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_p);

dt_referencia_w := dt_referencia_p;

open c01;
	loop
	fetch c01 into	nr_atendimento_w,
			cd_setor_atendimento_w;
	exit when c01%notfound;
		begin
		
		if	(ds_lista_servico_p is not null) then
			ds_lista_servico_w := ds_lista_servico_p;
			qtd_w := 1;
			while (ds_lista_servico_w is not null) and (qtd_w < 1000)loop
				begin 
				qtd_w		:= qtd_w + 1;
				tam_lista_w	:= length(ds_lista_servico_w);
				ie_pos_virgula_w	:= instr(ds_lista_servico_w,',');
		
				if	(ie_pos_virgula_w <> 0) then
					nr_seq_servico_w	:= substr(ds_lista_servico_w,1,(ie_pos_virgula_w - 1));
				else
					nr_seq_servico_w	:= ds_lista_servico_w;
				end if;
		
				ds_lista_servico_w	:= substr(ds_lista_servico_w,(ie_pos_virgula_w + 1),tam_lista_w);
		
				if (ie_servico_hora_inf_w = 'S') then
					ds_horarios_w := obter_horario_servico(nr_seq_servico_w, cd_setor_atendimento_w);
					
					if (ds_horarios_w is not null) and 
					   (to_date(to_char(dt_referencia_p,'dd/mm/yyyy')||' '||ds_horarios_w, 'dd/mm/yyyy hh24:mi') < 
					    to_date(to_char(sysdate,'dd/mm/yyyy')||' '||to_char(dt_referencia_p,'hh24:mi'), 'dd/mm/yyyy hh24:mi')) then
						dt_referencia_w := dt_referencia_p +1;
					else
						dt_referencia_w := dt_referencia_p;
					end if;
				end if;
		
				select	count(*)	
				into	qt_servicos_w
				from	nut_atend_serv_dia
				where	nr_atendimento 		= nr_atendimento_w
				and	cd_setor_atendimento 	= cd_setor_atendimento_w
				and	nr_Seq_servico 		= nr_seq_servico_w
				and	dt_servico between inicio_dia(dt_referencia_w) and fim_dia(dt_referencia_w)
				and	Nut_Obter_Se_Conduta_Suspensa(nr_sequencia) = 'N';
								
				if (qt_servicos_w = 0) then
					
					select 	nut_atend_serv_dia_seq.nextval
					into	nr_seq_serv_dia_w
					from	dual;
			
					select	to_date(to_char(dt_referencia_w,'dd/mm/yyyy')||' '||obter_horario_servico(nr_seq_servico_w, cd_setor_atendimento_w), 'dd/mm/yyyy hh24:mi')
					into	dt_servico_w
					from	dual;
			
					if (ie_preencher_auto_obs_w = 'S') then
						ds_observacao_w := substr(obter_obs_anterior_servico(dt_referencia_w,nr_Atendimento_w,nr_Seq_Servico_w),1,4000);
					end if;
			
					insert into nut_atend_serv_dia (
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						nr_Seq_servico,
						dt_servico,
						cd_setor_atendimento,
						nr_atendimento,
						ie_status,
						ds_observacao
						)
					values(nr_seq_serv_dia_w,
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nr_seq_servico_w,
						dt_servico_w,
						cd_setor_atendimento_w,
						nr_atendimento_w,
						'A',
						ds_observacao_w);
					
					inserir_serv_prescr_aval(nr_prescricao_p,nr_seq_serv_dia_w,nm_usuario_p);
					if (ie_gerar_auto_conduta_dieta_w = 'S') then
						Gerar_Dietas_Servico(nr_seq_serv_dia_w,nm_usuario_p);
					end if;
					
					if (ie_acompanhante_dia_anterior_w = 'S') then
					
						open c04;
						loop
						fetch c04 into 	cd_acompanhante_w,
								nm_acompanhante_w,
								ds_observacao_acomp_w,
								nr_seq_atend_acompanhante_w;
							exit when c04%notfound;
								begin
								
								select	nut_atend_acompanhante_seq.NextVal
								into	nr_seq_atend_acomp_novo_w
								from	dual;
								
								insert into nut_atend_acompanhante(	nr_sequencia,
													nr_seq_atend_serv_dia,
													cd_pessoa_fisica, 
													nm_pessoa_fisica, 
													ds_observacao, 
													dt_atualizacao,
													dt_atualizacao_nrec, 
													nm_usuario, 
													nm_usuario_nrec)
											values	(	nr_seq_atend_acomp_novo_w,
													nr_seq_serv_dia_w,
													cd_acompanhante_w,
													nm_acompanhante_w,
													ds_observacao_acomp_w,
													sysdate,
													sysdate,
													nm_usuario_p,
													nm_usuario_p);
													
								open c05;
								loop
								fetch c05 into cd_dieta_w;
								exit when c05%notfound;
									begin
									
									insert into nut_atend_acomp_dieta(	nr_sequencia, 
														nr_seq_atend_acomp, 
														cd_dieta,
														dt_atualizacao,
														dt_atualizacao_nrec, 
														nm_usuario, 
														nm_usuario_nrec)
												values 		(nut_atend_acomp_dieta_seq.NextVal,
														nr_seq_atend_acomp_novo_w,
														cd_dieta_w,
														sysdate,
														sysdate,
														nm_usuario_p,
														nm_usuario_p);
									
									end;
								end loop;
								close c05;
								
								end;
						end loop;
						close c04;
					
					end if;
						
					Consiste_acomp_dieta_categoria(nr_seq_serv_dia_w,nm_usuario_p,cd_estabelecimento_p,ds_consistencia_w);
						
					if (ie_gera_servico_Acompanhante_w = 'S') and (ds_consistencia_w is null) then
						
						select	nvl(max(b.qt_dieta_acomp),0),
							max(SUBSTR(OBTER_NOME_PF(c.CD_PESSOA_FISICA),0,255))
						into	qt_dieta_acomp_w,
							nm_pessoa_fisica_w
						from	atendimento_paciente a,
							atend_categoria_convenio b,
							pessoa_fisica c
						where	a.nr_Atendimento = b.nr_Atendimento
						and	b.nr_seq_interno = (	select 	max(d.nr_seq_interno) 
										from	atend_categoria_convenio d
										where	d.nr_atendimento = a.nr_atendimento
										/*and	nvl(d.dt_final_vigencia,dt_servico_w) >= dt_servico_w
										and	dt_servico_w >= nvl(d.dt_inicio_vigencia,dt_servico_w)*/
										)
						and	a.cd_pessoa_Fisica = c.cd_pessoa_fisica
						and	a.nr_atendimento = nr_atendimento_w;
						
						i := 0;
						
						for i in 1..qt_dieta_acomp_w loop
							begin
							
							inserir_acompanhante_nutricao(	nr_seq_servico_w,
											dt_servico_w,
											nr_atendimento_w,
											cd_setor_atendimento_w,
											null,
											wheb_mensagem_pck.get_texto(732356)||' '||nm_pessoa_fisica_w,
											nm_usuario_p,
											nr_Seq_acompanhante_w);
							end;
						end loop;
						
					end if;
					
				else
					open c03;
					loop
					fetch c03 into nr_seq_Serv_dia_w;
					exit when c03%notfound;
						begin
						inserir_serv_prescr_aval(nr_prescricao_p,nr_seq_serv_dia_w,nm_usuario_p);
						end;				
					end loop;
					close c03;
				end if;				
				
				end;
				end loop;
					
		end if;
	
		end;
	end loop;
close c01;

commit;

end Gerar_servico_Nutricao_setores;
/