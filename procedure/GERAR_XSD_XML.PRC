create or replace
procedure gerar_xsd_xml
		(nr_seq_projeto_p	in	xml_projeto.nr_sequencia%type,
		 nm_usuario_p		in	usuario.nm_usuario%type) is

nr_seq_item_w		xsd_item.nr_sequencia%type;
nm_elemento_w		xml_elemento.nm_elemento%type;
ie_tipo_atributo_w	tabela_atributo.nm_atributo%type;
qt_tamanho_w		tabela_atributo.qt_tamanho%type;
qt_decimais_w		tabela_atributo.qt_decimais%type;
ds_obrigatorio_w	varchar2(255);
nm_item_mensagem_w	xsd_item.nm_item%type;
ds_xsd_w			clob;
		 
cursor c00 is
select	nr_sequencia,
		nr_seq_elemento,
		nm_atributo_xml,
		nm_atributo,
		ie_obrigatorio,
		ie_tipo_atributo,
		ds_mascara,
		nr_seq_atrib_elem,
		nm_tabela_base,
		nm_atributo_base,
		nvl(qt_tamanho, 1) qt_tamanho,
		nvl(qt_decimais, 0) qt_decimais,
		cd_dominio,
		nr_seq_item_xsd
from	xml_atributo
where	nr_seq_atrib_elem is null
and		nr_seq_elemento in
		(select	nr_sequencia
		 from	xml_elemento
		 where	nr_seq_projeto	= nr_seq_projeto_p)
order by nvl(nr_seq_apresentacao, nr_sequencia);

cursor c01 (cd_dominio_cp	valor_dominio.cd_dominio%type)is
select	*
from	valor_dominio
where	cd_dominio		= cd_dominio_cp
and		ie_situacao		= 'A'
order 	by ds_valor_dominio;
		 
cursor c02 is
select	*
from	xml_elemento a
where	a.nr_seq_projeto		= nr_seq_projeto_p
order by nvl(a.nr_seq_apresentacao, a.nr_sequencia);

cursor c03 (nr_seq_elemento_cp 	xml_atributo.nr_seq_elemento%type)is
select	a.*,
		b.nm_item nm_item_xsd
from	xsd_item b,
		xml_atributo a
where	a.nr_seq_item_xsd	= b.nr_sequencia(+)
and		a.nr_seq_elemento 	= nr_seq_elemento_cp
order by nvl(a.nr_seq_apresentacao, a.nr_sequencia);

cursor c04 (ie_tipo_item_cp 	xsd_item.ie_tipo_item%type)is 
select	b.ds_linha,
		a.nr_sequencia
from	xsd_item_linha b,
		xsd_item a
where	a.nr_sequencia			= b.nr_seq_item_xsd
and		a.nr_seq_projeto_xml	= nr_seq_projeto_p
and		a.ie_tipo_item			= ie_tipo_item_cp
order 	by a.ie_tipo_item, b.nr_sequencia;

begin

delete from xsd_item where nr_seq_projeto_xml = nr_seq_projeto_p;
delete from xsd_texto where nr_seq_projeto_xml = nr_seq_projeto_p;

gerar_item_xsd(nr_seq_projeto_p, 'S',  'cabecalho', nr_seq_item_w, nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,  '<?xml version="1.0" encoding="ISO-8859-1"?>', nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,  '<schema xmlns="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">', nm_usuario_p);
--gerar_linha_xsd(nr_seq_item_w,  '<schema xmlns="http://www.w3.org/2001/XMLSchema" xmlns:phi="inserir o caminho aqui" targetNamespace="inserir o caminho aqui" elementFormDefault="qualified">', nm_usuario_p);

for	r_c00_w	in c00 loop

	if	(r_c00_w.cd_dominio is not null) then		-- gerar com base no dompinio

		if	(obter_item_xsd(nr_seq_projeto_p, 'st_dom' || r_c00_w.cd_dominio) is null) then
			gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_dom' || r_c00_w.cd_dominio, nr_seq_item_w, nm_usuario_p);
			gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_dom' || r_c00_w.cd_dominio || '">', nm_usuario_p);
			gerar_linha_xsd(nr_seq_item_w,  '<restriction base="string">', nm_usuario_p);
			gerar_linha_xsd(nr_seq_item_w,  '<maxLength value="255"/>', nm_usuario_p);
			for	r_c01_w in c01(r_c00_w.cd_dominio) loop
				gerar_linha_xsd(nr_seq_item_w,  '<enumeration value="' || r_c01_w.vl_dominio || '"/>', nm_usuario_p);
			end loop;
			for	r_c01_w in c01(r_c00_w.cd_dominio) loop
				gerar_linha_xsd(nr_seq_item_w,  '<!--' || r_c01_w.vl_dominio || ' - ' || r_c01_w.ds_valor_dominio || '-->', nm_usuario_p);
			end loop;
			gerar_linha_xsd(nr_seq_item_w,  '</restriction>', nm_usuario_p);
			gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
		end if;

		update	xml_atributo
		set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_dom' || r_c00_w.cd_dominio)
		where	nr_sequencia		= r_c00_w.nr_sequencia;
				
	elsif	(r_c00_w.nm_atributo_base is not null) then	-- gerar com base no atributo na base de dados
	
		select	max(ie_tipo_atributo),
				nvl(max(qt_tamanho), 1),
				nvl(max(qt_decimais), 0)
		into	ie_tipo_atributo_w,
				qt_tamanho_w,
				qt_decimais_w
		from	tabela_atributo
		where	nm_tabela 	= r_c00_w.nm_tabela_base
		and		nm_atributo = r_c00_w.nm_atributo_base;
		
		if		(ie_tipo_atributo_w = 'DATE') then
			if		(upper(r_c00_w.ds_mascara ) = 'DD/MM/YYYY') then
				if	(obter_item_xsd(nr_seq_projeto_p, 'st_data') is null) then
					gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_data', nr_seq_item_w, nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_data">', nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '<restriction base="date"/>', nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
				end if;
				update	xml_atributo
				set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_data')
				where	nr_sequencia		= r_c00_w.nr_sequencia;
			else
				if	(obter_item_xsd(nr_seq_projeto_p, 'st_dataHora') is null) then
					gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_dataHora', nr_seq_item_w, nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_dataHora">', nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '<restriction base="dateTime"/>', nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
				end if;
				update	xml_atributo
				set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_dataHora')
				where	nr_sequencia		= r_c00_w.nr_sequencia;
			end if;
		elsif	(ie_tipo_atributo_w = 'NUMBER') then

			if	(obter_item_xsd(nr_seq_projeto_p, 'st_number' || qt_tamanho_w || '_' || qt_decimais_w) is null) then
				gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_number' || qt_tamanho_w || '_' || qt_decimais_w, nr_seq_item_w, nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_number' || qt_tamanho_w || '_' || qt_decimais_w || '">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<restriction base="decimal">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<totalDigits value="' || qt_tamanho_w || '"/>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<fractionDigits value="' || qt_decimais_w || '"/>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</restriction>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
			end if;
			update	xml_atributo
			set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_number' || qt_tamanho_w || '_' || qt_decimais_w)
			where	nr_sequencia		= r_c00_w.nr_sequencia;
			
		elsif	(ie_tipo_atributo_w = 'TIMESTAMP') then
			if	(obter_item_xsd(nr_seq_projeto_p, 'st_dataHora') is null) then
				gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_dataHora', nr_seq_item_w, nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_dataHora">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<restriction base="dateTime"/>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
			end if;
			update	xml_atributo
			set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_dataHora')
			where	nr_sequencia		= r_c00_w.nr_sequencia;
		elsif	(ie_tipo_atributo_w = 'VARCHAR2') then
		
			if	(obter_item_xsd(nr_seq_projeto_p, 'st_string' || qt_tamanho_w) is null) then
				gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_string' || qt_tamanho_w, nr_seq_item_w, nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_string' || qt_tamanho_w || '">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<restriction base="string">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<maxLength value="' || qt_tamanho_w || '"/>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</restriction>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
			end if;
			update	xml_atributo
			set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_string' || qt_tamanho_w)
			where	nr_sequencia		= r_c00_w.nr_sequencia;
		end if;
				
	else
		if		(r_c00_w.ie_tipo_atributo = 'DATE') then				-- gerar conforme cadastrado no projeto xml
			if		(upper(r_c00_w.ds_mascara ) = 'DD/MM/YYYY') then
				if	(obter_item_xsd(nr_seq_projeto_p, 'st_data') is null) then
					gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_data', nr_seq_item_w, nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_data">', nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '<restriction base="date"/>', nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
				end if;
				update	xml_atributo
				set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_data')
				where	nr_sequencia		= r_c00_w.nr_sequencia;
			else
				if	(obter_item_xsd(nr_seq_projeto_p, 'st_dataHora') is null) then
					gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_dataHora', nr_seq_item_w, nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_dataHora">', nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '<restriction base="dateTime"/>', nm_usuario_p);
					gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
				end if;
				update	xml_atributo
				set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_dataHora')
				where	nr_sequencia		= r_c00_w.nr_sequencia;
			end if;
		elsif	(r_c00_w.ie_tipo_atributo = 'NUMBER') then
			if	(obter_item_xsd(nr_seq_projeto_p, 'st_number' || r_c00_w.qt_tamanho || '_' || r_c00_w.qt_decimais) is null) then
				gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_number' || r_c00_w.qt_tamanho || '_' || r_c00_w.qt_decimais, nr_seq_item_w, nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_number' || r_c00_w.qt_tamanho || '_' || r_c00_w.qt_decimais || '">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<restriction base="decimal">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<totalDigits value="' || r_c00_w.qt_tamanho || '"/>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<fractionDigits value="' || r_c00_w.qt_decimais || '"/>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</restriction>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
			end if;
			update	xml_atributo
			set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_number' || r_c00_w.qt_tamanho || '_' || r_c00_w.qt_decimais)
			where	nr_sequencia		= r_c00_w.nr_sequencia;
		elsif	(r_c00_w.ie_tipo_atributo = 'TIMESTAMP') then
			if	(obter_item_xsd(nr_seq_projeto_p, 'st_dataHora') is null) then
				gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_dataHora', nr_seq_item_w, nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_dataHora">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<restriction base="dateTime"/>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
			end if;
			update	xml_atributo
			set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_dataHora')
			where	nr_sequencia		= r_c00_w.nr_sequencia;
		elsif	(r_c00_w.ie_tipo_atributo = 'VARCHAR2') then
			if	(obter_item_xsd(nr_seq_projeto_p, 'st_string' || r_c00_w.qt_tamanho) is null) then
				gerar_item_xsd(nr_seq_projeto_p, 'S',  'st_string' || r_c00_w.qt_tamanho, nr_seq_item_w, nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<simpleType name="st_string' || r_c00_w.qt_tamanho || '">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<restriction base="string">', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '<maxLength value="' || r_c00_w.qt_tamanho || '"/>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</restriction>', nm_usuario_p);
				gerar_linha_xsd(nr_seq_item_w,  '</simpleType>', nm_usuario_p);
			end if;
			update	xml_atributo
			set		nr_seq_item_xsd	= obter_item_xsd(nr_seq_projeto_p, 'st_string' || r_c00_w.qt_tamanho)
			where	nr_sequencia		= r_c00_w.nr_sequencia;
		end if;
	end if;
end loop;
gerar_item_xsd(nr_seq_projeto_p, 'S',  'rodape', nr_seq_item_w, nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,'</schema>', nm_usuario_p);

gerar_item_xsd(nr_seq_projeto_p, 'C',  'cabecalho', nr_seq_item_w, nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,  '<?xml version="1.0" encoding="ISO-8859-1"?>', nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,  '<schema xmlns="http://www.w3.org/2001/XMLSchema" xmlns:phi="c:\temp\" targetNamespace="c:\temp\" elementFormDefault="qualified">', nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,  '<include schemaLocation="simpleTypes' || nr_seq_projeto_p || '.xsd"/>', nm_usuario_p);

for	r_c02_w	in c02 loop

	gerar_item_xsd(nr_seq_projeto_p, 'C',  'ct_' || r_c02_w.nm_elemento|| '_' || r_c02_w.nr_sequencia, nr_seq_item_w, nm_usuario_p);
	gerar_linha_xsd(nr_seq_item_w,  '<complexType name="ct_' || r_c02_w.nm_elemento|| '_' || r_c02_w.nr_sequencia ||  '">', nm_usuario_p);
	gerar_linha_xsd(nr_seq_item_w,  '<sequence>', nm_usuario_p);
	for	r_c03_w	in c03(r_c02_w.nr_sequencia) loop
		ds_obrigatorio_w		:= ' minOccurs="0" ';
		if	(r_c03_w.ie_obrigatorio = 'S') then
			ds_obrigatorio_w	:= ' minOccurs="1" ';
		end if;
		if (r_c03_w.nr_seq_atrib_elem is null) then
			gerar_linha_xsd(nr_seq_item_w, '<element name="' || r_c03_w.nm_atributo_xml || '" type="phi:' || r_c03_w.nm_item_xsd || '"' || ds_obrigatorio_w || '/>', nm_usuario_p);
		else
			select	max(nm_elemento)
			into	nm_elemento_w
			from	xml_elemento
			where	nr_sequencia	= r_c03_w.nr_seq_atrib_elem;
			gerar_linha_xsd(nr_seq_item_w, '<element name="' || r_c03_w.nm_atributo_xml || '" type="phi:ct_' || nm_elemento_w || '_' || r_c03_w.nr_seq_atrib_elem || '"' || ds_obrigatorio_w || '/>', nm_usuario_p);
		end if;
	end loop;
	gerar_linha_xsd(nr_seq_item_w, '</sequence>',nm_usuario_p);
	gerar_linha_xsd(nr_seq_item_w, '</complexType>',nm_usuario_p);
	update	xml_elemento
	set		nr_seq_item_xsd = nr_seq_item_w
	where	nr_sequencia	= r_c02_w.nr_sequencia;
end loop;

gerar_item_xsd(nr_seq_projeto_p, 'C',  'rodape', nr_seq_item_w, nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,'</schema>', nm_usuario_p);

gerar_item_xsd(nr_seq_projeto_p, 'M',  'mensagem', nr_seq_item_w, nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,'<schema  xmlns="http://www.w3.org/2001/XMLSchema" xmlns:phi="c:\temp\" targetNamespace="c:\temp\" elementFormDefault="qualified" attributeFormDefault="unqualified">', nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,'<include schemaLocation="c:\temp\complexTypes' || nr_seq_projeto_p || '.xsd"/>', nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,'<include schemaLocation="c:\temp\simpleTypes' || nr_seq_projeto_p || '.xsd"/>', nm_usuario_p);
select	max(b.nm_item),
		max(a.nm_elemento)
into	nm_item_mensagem_w,
		nm_elemento_w
from	xsd_item b,
		xml_elemento a
where	a.nr_seq_item_xsd		= b.nr_sequencia
and		a.nr_seq_projeto		= nr_seq_projeto_p
and		not exists (select 1 from xml_atributo x where x.nr_seq_atrib_elem = a.nr_sequencia);
gerar_linha_xsd(nr_seq_item_w,'<element name="' || nm_elemento_w || '" type="phi:' || nm_item_mensagem_w || '"/>', nm_usuario_p);
gerar_linha_xsd(nr_seq_item_w,'</schema>', nm_usuario_p);

ds_xsd_w		:= null;
for	r_c04_w	in c04('S') loop -- gerar tipos simples
	ds_xsd_w	:= ds_xsd_w  || r_c04_w.ds_linha || chr(13) || chr(10);
end loop;
insert into xsd_texto
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_projeto_xml,
		ie_tipo_texto,
		ds_texto)
select	xsd_texto_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_projeto_p,
		'S',
		to_clob(ds_xsd_w)
from dual;

ds_xsd_w		:= null;
for	r_c04_w	in c04('C') loop -- gerar tipos complexos
	ds_xsd_w	:= ds_xsd_w	  || r_c04_w.ds_linha || chr(13) || chr(10);
end loop;
insert into xsd_texto
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_projeto_xml,
		ie_tipo_texto,
		ds_texto)
select	xsd_texto_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_projeto_p,
		'C',
		to_clob(ds_xsd_w)
from 	dual;

ds_xsd_w		:= null;
for	r_c04_w	in c04('M') loop -- gerar tipos complexos
	ds_xsd_w	:= ds_xsd_w	 || r_c04_w.ds_linha || chr(13) || chr(10);
end loop;
insert into xsd_texto
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_projeto_xml,
		ie_tipo_texto,
		ds_texto)
select	xsd_texto_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_projeto_p,
		'M',
		to_clob(ds_xsd_w)
from 	dual;

commit;

end gerar_xsd_xml;
/

