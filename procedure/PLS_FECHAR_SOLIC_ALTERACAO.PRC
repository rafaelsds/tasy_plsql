create or replace
procedure pls_fechar_solic_alteracao(	nr_seq_solicitacao_p	number,
					cd_estabelecimento_p	number,
					nm_usuario_p		varchar2) is 

qt_registros_w			number(10);
nm_tabela_w			varchar2(30);
nm_atributo_w			varchar2(50);
ds_chave_simples_w		varchar2(255);
ds_chave_composta_w		varchar2(500);
ds_valor_old_w			varchar2(2000);
ds_valor_new_w			varchar2(2000);
ds_sql_w			varchar2(2000);
ds_parametros_w			varchar2(2000);
ds_sep_bv_w			varchar2(10);
vl_posicao_w			number(10);
dt_analise_w			date;
ie_tipo_complemento_w		varchar2(2);
cd_pessoa_fisica_w		varchar2(10);
nr_seq_compl_pf_w		number(10);
nr_seq_tipo_compl_adic_w	compl_pessoa_fisica.nr_seq_tipo_compl_adic%type;
nr_seq_compl_pf_tel_adic_w	pls_prestador.nr_seq_compl_pf_tel_adic%type;
qt_rejeicao_estipulante_w	number(10);
nr_seq_contrato_w		pls_segurado.nr_seq_contrato%type;
ie_lancar_mensagem_alt_w	varchar(10);

Cursor C01 is
	select	nm_tabela,
		nm_atributo,
		ds_chave_simples,
		ds_chave_composta,
		ds_valor_old,
		ds_valor_new
	from	tasy_solic_alt_campo
	where	nr_seq_solicitacao = nr_seq_solicitacao_p
	and		nm_tabela <> 'PESSOA_TITULAR_CONVENIO'
	and	ie_status = 'A';
	
Cursor C02 is
	select	ds_chave_simples,
			ds_chave_composta
	from	tasy_solic_alt_campo
	where	nr_seq_solicitacao = nr_seq_solicitacao_p
	and		nm_tabela = 'PESSOA_TITULAR_CONVENIO'
	and		ie_status = 'A'
	group by ds_chave_simples, ds_chave_composta;

Cursor C03 is
	select	nm_atributo,
			ds_valor_old,
			ds_valor_new
	from	tasy_solic_alt_campo
	where	nr_seq_solicitacao = nr_seq_solicitacao_p
	and		nm_tabela = 'PESSOA_TITULAR_CONVENIO'
	and		ie_status = 'A'
	and		ds_chave_composta = ds_chave_composta_w;

begin
/*
exec_sql_dinamico('APROVSOLICALTPF','alter trigger PESSOA_FISICA_UPDATE disable');
exec_sql_dinamico('APROVSOLICALTPF','alter trigger COMPL_PESSOA_FISICA_UPDATE disable');
*/
/*aaschlote 05/10/2012 OS - 496203*/
pls_usuario_pck.set_ie_exec_trigger_solic_pf('N');
select	dt_analise
into	dt_analise_w
from	tasy_solic_alteracao
where	nr_sequencia = nr_seq_solicitacao_p;

if	(dt_analise_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(210238);
end if;

select	count(*)
into	qt_registros_w
from	tasy_solic_alt_campo
where	nr_seq_solicitacao = nr_seq_solicitacao_p
and		ie_status = 'P';

ds_sep_bv_w := obter_separador_bv;

if	(qt_registros_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(210239);
end if;

open C01;
loop fetch C01 into
	nm_tabela_w,
	nm_atributo_w,
	ds_chave_simples_w,
	ds_chave_composta_w,
	ds_valor_old_w,
	ds_valor_new_w;	
exit when C01%notfound;
	begin
	ds_sql_w := 'UPDATE ' || nm_tabela_w || ' SET nm_usuario = :nm_usuario_p, dt_atualizacao = sysdate ';
	ds_parametros_w := 'nm_usuario_p='||nm_usuario_p||ds_sep_bv_w;
	
	ds_sql_w := ds_sql_w || ', ' || nm_atributo_w || ' = :' || nm_atributo_w;
	ds_parametros_w := ds_parametros_w || nm_atributo_w || '=' || ds_valor_new_w || ds_sep_bv_w;
	
	if	(ds_chave_simples_w is null)	and
		(ds_chave_composta_w is null)	then
		select	max(cd_pessoa_fisica)
		into	ds_chave_simples_w
		from	pls_revisao_solic_alt_pf a
		where	a.nr_seq_solic_alt = nr_seq_solicitacao_p;		
	end if;	
	
	if	(ds_chave_simples_w is not null) then
		ds_sql_w := ds_sql_w || ' WHERE cd_pessoa_fisica = :cd_pessoa_fisica_p ';
		ds_parametros_w := ds_parametros_w || 'cd_pessoa_fisica_p=' || ds_chave_simples_w;
		
		if (nm_tabela_w = upper('COMPL_PF_TEL_ADIC')) then
			select	max(a.nr_seq_compl_pf_tel_adic)
			into	nr_seq_compl_pf_tel_adic_w
			from	pls_prestador a
			where	a.cd_pessoa_fisica	= ds_chave_simples_w;
			
			ds_sql_w	:= ds_sql_w || ' and nr_sequencia = ' || nr_seq_compl_pf_tel_adic_w;			
		end if;
		
	elsif	(ds_chave_composta_w is not null) then
		select	substr(ds_chave_composta_w,instr(ds_chave_composta_w,'IE_TIPO_COMPLEMENTO=') + 20, length(ds_chave_composta_w))
		into	ie_tipo_complemento_w
		from	dual;
		
		
		ds_sql_w := ds_sql_w || ' WHERE cd_pessoa_fisica = :cd_pessoa_fisica_p AND ie_tipo_complemento = ' || ie_tipo_complemento_w;		
		
		vl_posicao_w := instr(ds_chave_composta_w, '#@#@');
		vl_posicao_w := vl_posicao_w - instr(ds_chave_composta_w, '=');
		
		cd_pessoa_fisica_w	:= substr(ds_chave_composta_w, instr(ds_chave_composta_w, '=')+1, vl_posicao_w-1);		
	
		if	(cd_pessoa_fisica_w is null) then
			select	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	pls_revisao_solic_alt_pf a
			where	a.nr_seq_solic_alt = nr_seq_solicitacao_p;
		end if;
		
		ds_parametros_w := ds_parametros_w || 'cd_pessoa_fisica_p='|| cd_pessoa_fisica_w;
		
		--Se o complemento for do tipo Adicional
		if 	(nm_tabela_w = upper('COMPL_PESSOA_FISICA') and ie_tipo_complemento_w = 9) then
			select 	max(nr_seq_tipo_compl_adic)
			into	nr_seq_tipo_compl_adic_w
			from 	compl_pessoa_fisica
			where 	cd_pessoa_fisica = cd_pessoa_fisica_w;
			
			if (nr_seq_tipo_compl_adic_w is not null) then
				ds_sql_w	:= ds_sql_w || ' and nr_seq_tipo_compl_adic = ' || nr_seq_tipo_compl_adic_w;
			end if;	
		end if;			
		
		select	count(1)
		into	qt_registros_w
		from	compl_pessoa_fisica
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	ie_tipo_complemento	= ie_tipo_complemento_w
		and	rownum			= 1;
		
		if	(qt_registros_w = 0) then
			select	max(nr_sequencia)
			into	nr_seq_compl_pf_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w;
			
			if	(nr_seq_compl_pf_w is null) then
				nr_seq_compl_pf_w	:= 0;
			end if;
			
			insert into compl_pessoa_fisica
				(	CD_PESSOA_FISICA,NR_SEQUENCIA,IE_TIPO_COMPLEMENTO,DT_ATUALIZACAO,NM_USUARIO)
			values	(	cd_pessoa_fisica_w,nr_seq_compl_pf_w+1,ie_tipo_complemento_w,sysdate,nm_usuario_p);
		else
			select	max(nr_sequencia)
			into	nr_seq_compl_pf_w
			from	compl_pessoa_fisica
			where	cd_pessoa_fisica	= cd_pessoa_fisica_w
			and	ie_tipo_complemento	= ie_tipo_complemento_w;
			
			if	(nr_seq_compl_pf_w is null) then
				nr_seq_compl_pf_w	:= 0;
			end if;
		end if;
	end if;

	exec_sql_dinamico_bv('',ds_sql_w,ds_parametros_w);
	
	if	(cd_pessoa_fisica_w is not null) and
		(nr_seq_compl_pf_w is not null) then

		pls_atualizar_compl_pf(cd_pessoa_fisica_w,nr_seq_compl_pf_w,cd_estabelecimento_p,nm_usuario_p,ie_lancar_mensagem_alt_w);
	end if;
	end;

end loop;
close C01;


open C02;
loop
fetch C02 into	
	ds_chave_simples_w,
	ds_chave_composta_w;	
exit when C02%notfound;
	begin
	ds_sql_w := 'UPDATE PESSOA_TITULAR_CONVENIO SET nm_usuario = :nm_usuario_p, dt_atualizacao = sysdate ';
	ds_parametros_w := 'nm_usuario_p='||nm_usuario_p||ds_sep_bv_w;
	open C03;
	loop
	fetch C03 into	
		nm_atributo_w,
		ds_valor_old_w,
		ds_valor_new_w;
	exit when C03%notfound;
		begin
			ds_sql_w := ds_sql_w || ', ' || nm_atributo_w || ' = :' || nm_atributo_w;
			ds_parametros_w := ds_parametros_w || nm_atributo_w || '=' || ds_valor_new_w || ds_sep_bv_w;
		end;
	end loop;
	close C03;
	ds_sql_w := ds_sql_w || ' WHERE cd_pessoa_fisica = :cd_pessoa_fisica_p ';
	ds_parametros_w := ds_parametros_w || 'cd_pessoa_fisica_p=' || ds_chave_simples_w|| ds_sep_bv_w;
	ds_sql_w := ds_sql_w || ' and nr_sequencia = :nr_sequencia_p ';
	ds_parametros_w := ds_parametros_w || 'nr_sequencia_p=' || ds_chave_composta_w;
	exec_sql_dinamico_bv('',ds_sql_w,ds_parametros_w);	
	end;
end loop;
close C02;

update	tasy_solic_alteracao
set	dt_analise = sysdate,
	nm_usuario = nm_usuario_p,
	dt_atualizacao = sysdate,
	ie_status = 'L'
where	nr_sequencia = nr_seq_solicitacao_p;

select	count(*)
into	qt_rejeicao_estipulante_w
from   	tasy_solic_alteracao a,
	tasy_solic_alt_campo b
where  	a.nr_sequencia       	= b.nr_seq_solicitacao
and    	a.nr_sequencia       	= nr_seq_solicitacao_p
and    	b.ie_status          	= 'R'
and    	b.ie_tipo_login_solic  	= 'EP';

if (qt_rejeicao_estipulante_w > 0) then	 
	select	a.nr_seq_contrato
	into	nr_seq_contrato_w
	from   	tasy_solic_alteracao a
	where  	a.nr_sequencia       	= nr_seq_solicitacao_p;
	
	if (nr_seq_contrato_w is not null) then
		pls_alerta_rejeicao_mov( null, null, nr_seq_contrato_w, 3, nr_seq_solicitacao_p, nm_usuario_p);
	end if;
end if;
/*
exec_sql_dinamico('APROVSOLICALTPF','alter trigger PESSOA_FISICA_UPDATE enable');
exec_sql_dinamico('APROVSOLICALTPF','alter trigger COMPL_PESSOA_FISICA_UPDATE enable');
*/

commit;

/*aaschlote 05/10/2012 OS - 496203*/
pls_usuario_pck.set_ie_exec_trigger_solic_pf('S');  

end pls_fechar_solic_alteracao;  
/
