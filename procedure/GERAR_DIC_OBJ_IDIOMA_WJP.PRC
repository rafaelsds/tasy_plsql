create or replace
procedure gerar_dic_obj_idioma_wjp(
		cd_funcao_p	number,
		nr_seq_idioma_p	number,
		nm_usuario_p	varchar2) is
		
nr_seq_objeto_w		number(10,0);
nm_atributo_w		varchar2(50);
ds_texto_w		varchar2(4000);
cd_funcao_w		number(5,0);
nr_seq_traducao_w	number(10,0);

cursor c01 is
select	a.nr_sequencia,
	'DS_TEXTO',
	a.ds_texto,
	a.cd_funcao
from	dic_objeto a
where	a.cd_funcao = cd_funcao_p
and	a.ie_tipo_objeto = 'P'
and	a.ds_texto is not null
and	not exists (
		select	1
		from	dic_objeto_idioma x
		where	x.nr_seq_objeto = a.nr_sequencia
		and	x.nm_atributo = 'DS_TEXTO'
		and	x.nr_seq_idioma = nr_seq_idioma_p)
order by
	a.nr_sequencia;

begin
if	(cd_funcao_p is not null) and
	(nr_seq_idioma_p is not null) and
	(nm_usuario_p is not null) then
	begin
	open c01;
	loop
	fetch c01 into	nr_seq_objeto_w,
			nm_atributo_w,
			ds_texto_w,
			cd_funcao_w;
	exit when c01%notfound;
		begin
		if	(trim(ds_texto_w) is not null) then
			begin
			select	dic_objeto_idioma_seq.nextval
			into	nr_seq_traducao_w
			from	dual;
			
			insert into dic_objeto_idioma (
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_atualizacao,
				nm_usuario,
				nr_seq_objeto,
				nm_atributo,
				ds_descricao,
				cd_funcao,
				nr_seq_idioma,			
				ds_traducao,
				ie_necessita_revisao,
				ie_tipo_objeto)
			values (
				nr_seq_traducao_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_objeto_w,
				nm_atributo_w,
				ds_texto_w,
				cd_funcao_w,
				nr_seq_idioma_p,			
				' ',
				'T',
				'P');
			end;
		end if;
		end;
	end loop;
	close c01;
	end;
end if;
commit;
end gerar_dic_obj_idioma_wjp;
/