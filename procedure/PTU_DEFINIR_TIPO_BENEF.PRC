create or replace
procedure ptu_definir_tipo_benef
			(	nr_seq_intercambio_p		ptu_intercambio.nr_sequencia%type,
				ie_tipo_compartilhamento_p	ptu_intercambio_benef.ie_tipo_compartilhamento%type,
				cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type,
				nm_usuario_p			usuario.nm_usuario%type) is

nr_seq_empresa_w		number(10);
ds_razao_social_w		varchar2(40);
qt_registros_w			number(10);
qt_imp_w			number(10);
cd_empresa_origem_w		number(10);
nr_seq_ops_congenere_w		number(10);
ie_tipo_contrato_w		varchar2(10);
ie_tipo_pessoa_w		number(10);
cd_pessoa_fisica_w		varchar2(10);
ie_repasse_w			varchar2(10);
cd_unimed_origem_w		varchar2(10);
cd_cgc_cpf_w			varchar2(14);
nr_seq_congenere_w		number(10);
nr_seq_intercambio_atual_w	number(10);
nr_seq_grupo_inter_w		pls_regra_grupo_inter.nr_sequencia%type;

Cursor C01 is
	select	nr_sequencia,
		nm_empr_abrev,
		cd_empresa_origem,
		ie_tipo_pessoa,
		cd_cgc_cpf
	from	ptu_intercambio_empresa
	where	nr_seq_intercambio = nr_seq_intercambio_p;

Cursor C02 (	nr_seq_empresa_pc	ptu_intercambio_empresa.nr_sequencia%type) is
	select	nr_sequencia nr_seq_benef
	from	ptu_intercambio_benef
	where	nr_seq_empresa	= nr_seq_empresa_pc;

cursor c03 is
	select	b.nr_sequencia
	from	pls_regra_grupo_inter	b,
		pls_regra_benef_grupo	a
	where	a.nr_seq_regra		= b.nr_sequencia
	and	b.ie_situacao		= 'A'
	and	((a.nr_seq_cooperativa	= nr_seq_congenere_w and a.nr_seq_cooperativa is not null) or (a.nr_seq_cooperativa is null))
	and	((a.nr_seq_congenere	= nr_seq_congenere_w and a.nr_seq_congenere is not null) or (a.nr_seq_congenere is null))
	and	((a.cd_cgc_empresa	= cd_cgc_cpf_w and a.cd_cgc_empresa is not null) or (a.cd_cgc_empresa is null))
	and	((a.ie_tipo_repasse	= ie_repasse_w and a.ie_tipo_repasse is not null) or (a.ie_tipo_repasse is null))
	and	((a.cd_operadora_empresa = cd_empresa_origem_w and a.cd_operadora_empresa is not null) or (a.cd_operadora_empresa is null))
	order by nvl(a.nr_seq_cooperativa,0),
		nvl(a.nr_seq_congenere,0),
		nvl(a.ie_tipo_repasse,' '),
		nvl(a.cd_cgc_empresa,0),
		nvl(a.cd_operadora_empresa,0);
	
begin

gravar_processo_longo(wheb_mensagem_pck.get_texto(1130933) ,'PTU_DEFINIR_TIPO_BENEF',0);

select	ie_tipo_contrato,
	cd_unimed_origem
into	ie_tipo_contrato_w,
	cd_unimed_origem_w
from	ptu_intercambio
where	nr_sequencia	= nr_seq_intercambio_p;

select	max(nr_sequencia)
into	nr_seq_congenere_w
from	pls_congenere
where	to_number(cd_cooperativa) = to_number(cd_unimed_origem_w);

open C01;
loop
fetch C01 into
	nr_seq_empresa_w,
	ds_razao_social_w,
	cd_empresa_origem_w,
	ie_tipo_pessoa_w,
	cd_cgc_cpf_w;
exit when C01%notfound;
	begin
	
	/*Quando for contrato de fundacao, gravar a operadora congenere dona do contrato*/
	if	(ie_tipo_contrato_w = 'F') and
		(cd_empresa_origem_w is not null) then
		
		select	max(b.nr_sequencia)
		into	nr_seq_ops_congenere_w
		from	pls_pj_empresa	a,
			pls_congenere	b
		where	a.cd_cgc	= b.cd_cgc
		and	to_number(a.cd_empresa)	= cd_empresa_origem_w;
		
		if	(nvl(nr_seq_ops_congenere_w,0) <> 0) then
			update	ptu_intercambio_empresa
			set	nr_seq_congenere	= nr_seq_ops_congenere_w
			where	nr_sequencia		= nr_seq_empresa_w;
		end if;
	end if;
	
	nr_seq_intercambio_atual_w	:= null;
	nr_seq_grupo_inter_w		:= null;
	
	select	max(ie_repasse)
	into	ie_repasse_w
	from	ptu_intercambio_benef
	where	nr_seq_empresa	= nr_seq_empresa_w;
	
	for r_c03_w in c03 loop
		begin
		nr_seq_grupo_inter_w	:= r_c03_w.nr_sequencia;
		end;
	end loop;

	if	(ie_tipo_pessoa_w = 1) then
		select	max(nr_sequencia)
		into	nr_seq_intercambio_atual_w
		from	pls_intercambio
		where	cd_cgc			= cd_cgc_cpf_w
		and	cd_operadora_empresa	= cd_empresa_origem_w
		and	nr_seq_congenere	= nr_seq_congenere_w
		and	ie_tipo_contrato	= ie_tipo_contrato_w
		and	ie_tipo_repasse		= ie_repasse_w
		and	(nr_seq_grupo_inter_w is null or nr_seq_grupo_intercambio = nr_seq_grupo_inter_w);
	elsif	(ie_tipo_pessoa_w = 2) then
		cd_cgc_cpf_w	:= substr(cd_cgc_cpf_w,1,11);
		
		select	max(cd_pessoa_fisica)
		into	cd_pessoa_fisica_w
		from	pessoa_fisica
		where	nr_cpf	= cd_cgc_cpf_w;
		
		select	max(nr_sequencia)
		into	nr_seq_intercambio_atual_w
		from	pls_intercambio
		where	cd_pessoa_fisica	= cd_pessoa_fisica_w
		and	cd_operadora_empresa	= cd_empresa_origem_w
		and	nr_seq_congenere	= nr_seq_congenere_w
		and	ie_tipo_contrato	= ie_tipo_contrato_w
		and	ie_tipo_repasse		= ie_repasse_w
		and	(nr_seq_grupo_inter_w is null or nr_seq_grupo_intercambio = nr_seq_grupo_inter_w);
	end if;
	
	update	ptu_intercambio_empresa
	set	nr_seq_contrato	= nr_seq_intercambio_atual_w
	where	nr_sequencia	= nr_seq_empresa_w;
	
	select	count(1)
	into	qt_registros_w
	from	ptu_intercambio_benef
	where	nr_seq_empresa	= nr_seq_empresa_w;
	
	qt_imp_w	:= 0;
	for r_c02_w in C02(nr_seq_empresa_w) loop
		begin
		qt_imp_w	:= qt_imp_w + 1;
		gravar_processo_longo('Benef. da empresa ' || ds_razao_social_w||' . Importados ' || qt_imp_w|| ' de ' || qt_registros_w,'PTU_DEFINIR_TIPO_BENEF',-1);
		
		ptu_definir_benef_imp(r_c02_w.nr_seq_benef,nm_usuario_p);
		
		ptu_gravar_produto_benef_imp(r_c02_w.nr_seq_benef,ie_tipo_contrato_w,cd_estabelecimento_p,nm_usuario_p);
		
		if	(ie_tipo_compartilhamento_p is not null) then
			update	ptu_intercambio_benef
			set	ie_tipo_compartilhamento = ie_tipo_compartilhamento_p
			where	nr_sequencia = r_c02_w.nr_seq_benef;
		end if;
		end;
	end loop; --C02
	
	end;
end loop;
close C01;

commit;

end ptu_definir_tipo_benef;
/