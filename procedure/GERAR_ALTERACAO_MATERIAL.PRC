create or replace
procedure gerar_alteracao_material(		nr_prescricao_p		number,
					cd_perfil_p		number,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number
					)
					is

nr_sequencia_w				number(10);
cd_intervalo_padrao_w		varchar2(7);
cd_intervalo_padrao_ww		varchar2(7);
cd_material_w				number(6);
ie_via_aplicacao_w			varchar2(5);
cd_setor_atendimento_w		number(5);
cd_pessoa_fisica_w			varchar2(10);
qt_peso_w					number(6,3);
qt_dose_padrao_w			number(18,6);
qt_dose_mat_prescr_w		number(18,6);
cd_intervalo_mat_prescr_w	varchar2(7);
cd_pessoa_alteracao_w		varchar2(10);
cd_unidade_padrao_w			varchar2(30);
cd_unidade_padrao_ww		varchar2(30);
cd_unidade_medida_dose_w	varchar2(30);
ie_Unidade_Medida_Medic_w	number(10);
ie_DefinePadraoAposVia_w	varchar2(1);
ie_agrupador_w				number(2);
nr_seq_diluicao_w			number(10);
ds_mensagem_w				varchar2(255);
nr_atendimento_w			number(10);
ie_bomba_infusao_w			prescr_material.ie_bomba_infusao%type;


cursor c01 	is
	select	a.nr_sequencia,
			a.cd_material,
			a.ie_via_aplicacao,
			b.cd_setor_atendimento,
			b.cd_pessoa_fisica,
			b.qt_peso,
			a.qt_dose,
			a.cd_intervalo,
			a.cd_unidade_medida_dose,
			a.ie_agrupador,
			a.nr_sequencia_diluicao,
			b.nr_atendimento,
			a.ie_bomba_infusao
	from		prescr_material a,
			prescr_medica b
	where	a.nr_prescricao	= b.nr_prescricao
	and		a.nr_prescricao	= nr_prescricao_p
	and		ie_suspenso <> 'S'
	and		a.ie_agrupador = 1
	and		a.nr_seq_kit is null
	and		b.nr_seq_protocolo is null;


begin

obter_param_usuario(924, 50, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_Unidade_Medida_Medic_w);
obter_param_usuario(924, 623, cd_perfil_p, nm_usuario_p, cd_estabelecimento_p, ie_DefinePadraoAposVia_w);

select	max(cd_pessoa_fisica)
into	cd_pessoa_alteracao_w
from	usuario
where	nm_usuario = nm_usuario_p;

open c01;
loop
fetch c01 into
	nr_sequencia_w,
	cd_material_w,
	ie_via_aplicacao_w,
	cd_setor_atendimento_w,
	cd_pessoa_fisica_w,
	qt_peso_w,
	qt_dose_mat_prescr_w,
	cd_intervalo_mat_prescr_w,
	cd_unidade_medida_dose_w,
	ie_agrupador_w,
	nr_seq_diluicao_w,
	nr_atendimento_w,
	ie_bomba_infusao_w;
exit when c01%notfound;
	select	cd_intervalo_padrao
	into	cd_intervalo_padrao_w
	from 	material
	where 	cd_material = cd_material_w;

	qt_dose_padrao_w	:= Obter_Padrao_Param_Prescr(
								nr_atendimento_w,
								cd_material_w,
								ie_via_aplicacao_w,
								cd_setor_atendimento_w,
								cd_pessoa_fisica_w,
								Obter_Idade_PF(cd_pessoa_fisica_w, sysdate, 'A'),
								qt_peso_w, 'N',
								'D',
								cd_intervalo_mat_prescr_w);
	if	(qt_dose_padrao_w is null) or
		(qt_dose_padrao_w = '') then
		qt_dose_padrao_w := 1;
	end if;

	if	(nvl(ie_Unidade_Medida_Medic_w,1) = 1) then
		select	substr(obter_dados_material_estab(cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo
		into	cd_unidade_padrao_w
		from 	material
		where 	cd_material = cd_material_w;
	elsif	(nvl(ie_Unidade_Medida_Medic_w,1) = 2) then
		select 	max(cd_unidade_medida)
		into	cd_unidade_padrao_w
		from 	material_conversao_unidade
		where 	cd_material = cd_material_w
		and 	upper(cd_unidade_medida) = upper(obter_unid_med_usua('ML'));
	elsif	(nvl(ie_Unidade_Medida_Medic_w,1) = 3) then
		select 	max(a.cd_unidade_medida)
		into	cd_unidade_padrao_w
		from 	unidade_medida_dose_v a
		where 	a.cd_material	= cd_material_w
		and	a.ie_prioridade = (	select 	min(b.ie_prioridade)
							from 	unidade_medida_dose_v b
							where	a.cd_material = b.cd_material);
	end if;

	if	(ie_DefinePadraoAposVia_w = 'S') then
		cd_unidade_padrao_ww	:= Obter_Padrao_Param_Prescr(
									nr_atendimento_w,
									cd_material_w,
									ie_via_aplicacao_w,
									cd_setor_atendimento_w,
									cd_pessoa_fisica_w,
									Obter_Idade_PF(cd_pessoa_fisica_w, sysdate, 'A'),
									qt_peso_w, 'N',
									'U',
									cd_intervalo_mat_prescr_w);
		if	(cd_unidade_padrao_ww <> '') or
			(cd_unidade_padrao_ww is not null) then
			cd_unidade_padrao_w := cd_unidade_padrao_ww;
		end if;

		cd_intervalo_padrao_ww	:= Obter_Padrao_Param_Prescr(
									nr_atendimento_w,
									cd_material_w,
									ie_via_aplicacao_w,
									cd_setor_atendimento_w,
									cd_pessoa_fisica_w,
									Obter_Idade_PF(cd_pessoa_fisica_w, sysdate, 'A'),
									qt_peso_w, 'N',
									'I',
									cd_intervalo_mat_prescr_w);
		if	(cd_intervalo_padrao_ww <> '') or
			(cd_intervalo_padrao_ww is not null) then
			cd_intervalo_padrao_w := cd_intervalo_padrao_ww;
		end if;
	end if;

	if	(qt_dose_mat_prescr_w <> qt_dose_padrao_w) then
		insert into	prescr_material_alteracao
				(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_prescricao,
				ie_acao,
				cd_pessoa_alteracao,
				ds_alteracao
				)
		values
				(
				prescr_material_alteracao_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_prescricao_p,
				nr_sequencia_w,
				'D',
				cd_pessoa_alteracao_w,
				WHEB_MENSAGEM_PCK.get_texto(458030,'de='||qt_dose_padrao_w||';para='||qt_dose_mat_prescr_w)/*Alterado de ___ para ___*/
				);
	end if;

	if	(nvl(cd_unidade_medida_dose_w,OBTER_DESC_EXPRESSAO(305401)) <> nvl(cd_unidade_padrao_w,OBTER_DESC_EXPRESSAO(305401))) then
		insert into	prescr_material_alteracao
				(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_prescricao,
				ie_acao,
				cd_pessoa_alteracao,
				ds_alteracao
				)
		values
				(
				prescr_material_alteracao_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_prescricao_p,
				nr_sequencia_w,
				'U',
				cd_pessoa_alteracao_w,
				WHEB_MENSAGEM_PCK.get_texto(458030,'de='||decode(cd_unidade_padrao_w,null,OBTER_DESC_EXPRESSAO(305401),Obter_Desc_Unid_Med(cd_unidade_padrao_w))
				||';para='||decode(cd_unidade_medida_dose_w,null,OBTER_DESC_EXPRESSAO(305401),Obter_Desc_Unid_Med(cd_unidade_medida_dose_w)))
				);
	end if;

	if	(nvl(cd_intervalo_padrao_w,OBTER_DESC_EXPRESSAO(305401)) <> nvl(cd_intervalo_mat_prescr_w,OBTER_DESC_EXPRESSAO(305401))) then

		insert into	prescr_material_alteracao
				(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_prescricao,
				nr_seq_prescricao,
				ie_acao,
				cd_pessoa_alteracao,
				ds_alteracao
				)
		values
				(
				prescr_material_alteracao_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_prescricao_p,
				nr_sequencia_w,
				'I',
				cd_pessoa_alteracao_w,
				WHEB_MENSAGEM_PCK.get_texto(458030,'de='||decode(cd_intervalo_padrao_w,null,OBTER_DESC_EXPRESSAO(305401),obter_desc_intervalo(cd_intervalo_padrao_w))
				||';para='||decode(cd_intervalo_mat_prescr_w,null,OBTER_DESC_EXPRESSAO(305401),obter_desc_intervalo(cd_intervalo_mat_prescr_w)))
				);
	end if;
end loop;
close c01;
if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end gerar_alteracao_material;
/
