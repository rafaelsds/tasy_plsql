create or replace
procedure gerar_erro_nut_pac_elem(
				nr_prescricao_p			number,
				nr_seq_nut_elem_mat_p	number,
				nr_regra_p				number,
				ie_permite_liberar_p	varchar2,
				ds_compl_erro_p			varchar2,
				cd_perfil_p				number,
				nm_usuario_p			varchar2,
				nr_seq_erro_p 		out	number) is
													
nr_seq_erro_w		number(10,0);
ie_regra_w			varchar2(15);
ds_regra_w			varchar2(255);
ds_msg_perfil_w		varchar2(4000);
ds_msg_wheb_w		varchar2(4000);
ds_inf_w			varchar2(2000);
ie_prioridade_w		number(3,0);
ds_cor_w			varchar2(15);
ie_agrupador_w		number(2,0);

cursor c01 is
select	ds_mensagem_cliente
from	regra_consiste_prescr_par
where	nr_seq_regra = nr_regra_p
and		nvl(cd_perfil,cd_perfil_p)	= cd_perfil_p
order by
		nr_sequencia;
											
begin
if	(nr_prescricao_p is not null) and
	(nr_seq_nut_elem_mat_p is not null) and
	(ie_permite_liberar_p is not null) and
	(nm_usuario_p is not null) then
	
	open c01;
	loop
	fetch c01 into ds_msg_perfil_w;
	exit when c01%notfound;
		begin
		ds_msg_perfil_w := ds_msg_perfil_w;
		end;
	end loop;
	close c01;
	
	select	ie_tipo_regra,
			obter_desc_expressao(cd_exp_descricao,ds_regra_prescr),
			obter_desc_expressao(cd_exp_mensagem,ds_mensagem_usuario),
			obter_desc_expressao(cd_exp_informacao_adic,ds_informacao_adic),
			ie_prioridade,
			ds_cor		
	into	ie_regra_w,
			ds_regra_w,
			ds_msg_wheb_w,
			ds_inf_w,
			ie_prioridade_w,
			ds_cor_w
	from	regra_consiste_prescr
	where	nr_sequencia = nr_regra_p;
	
	select	prescr_medica_erro_seq.nextval
	into	nr_seq_erro_w
	from	dual;
	
	insert into prescr_medica_erro (
		nr_sequencia,
		nr_prescricao,
		nr_seq_npt_elem_mat,
		nr_regra,
		ds_inconsistencia,
		ds_erro,
		ds_compl_erro,
		ie_libera,
		ie_tipo_regra,		
		ds_inf_adicional,
		ie_prioridade,
		ds_cor,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nm_usuario,
		dt_atualizacao)
	values (
		nr_seq_erro_w,
		nr_prescricao_p,
		nr_seq_nut_elem_mat_p,
		nr_regra_p,
		ds_regra_w,
		nvl(ds_msg_perfil_w,ds_msg_wheb_w),
		ds_compl_erro_p,
		ie_permite_liberar_p,
		ie_regra_w,		
		ds_inf_w,
		ie_prioridade_w,
		ds_cor_w,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate);

	commit;

	nr_seq_erro_p	:= nr_seq_erro_w;

end if;

end gerar_erro_nut_pac_elem;
/