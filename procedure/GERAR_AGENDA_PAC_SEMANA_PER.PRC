create or replace
PROCEDURE gerar_agenda_pac_semana_per	(nr_seq_agenda_p	NUMBER,
				dt_agenda_p		DATE,
				ie_frequencia_p		VARCHAR2,
				qt_frequencia_p		NUMBER,
				ie_final_semana_p		VARCHAR2,
				nm_usuario_p		VARCHAR2,
				ds_erro_p OUT		VARCHAR2) IS

cd_agenda_w			NUMBER(10);
cd_pessoa_fisica_w		VARCHAR2(10);
dt_agenda_w			DATE;
hr_inicio_w			DATE;
nr_minuto_duracao_w		NUMBER(10);
nm_usuario_w			VARCHAR2(15);
dt_atualizacao_w			DATE;
cd_medico_w			VARCHAR2(10);
nm_pessoa_contato_w		VARCHAR2(50);
cd_procedimento_w		NUMBER(15);
ds_observacao_w			VARCHAR2(4000);
cd_convenio_w			NUMBER(5);
nr_cirurgia_w			NUMBER(10);
ds_cirurgia_w			VARCHAR2(500);
qt_idade_paciente_w		NUMBER(3);
cd_tipo_anestesia_w		VARCHAR2(2);
ie_origem_proced_w		NUMBER(10);
ie_status_agenda_w		VARCHAR2(3);
nm_instrumentador_w		VARCHAR2(20);
nm_circulante_w			VARCHAR2(20);
ie_ortese_protese_w		VARCHAR2(1);
ie_cdi_w				VARCHAR2(1);
ie_uti_w				VARCHAR2(1);
ie_banco_sangue_w		VARCHAR2(1);
ie_serv_especial_w			VARCHAR2(1);
cd_motivo_cancelamento_w		agenda_paciente.cd_motivo_cancelamento%type;
--nr_sequencia_w			number(10);
ds_senha_w			VARCHAR2(20);
cd_turno_w			VARCHAR2(1);
cd_anestesista_w			VARCHAR2(10);
cd_pediatra_w			VARCHAR2(10);
nm_paciente_w			VARCHAR2(60);
ie_anestesia_w			VARCHAR2(1);
nr_atendimento_w			NUMBER(10);
ie_carater_cirurgia_w		VARCHAR2(1);
cd_usuario_convenio_w		VARCHAR2(30);
nm_usuario_orig_w			VARCHAR2(15);
qt_idade_mes_w			NUMBER(2);
cd_plano_w			VARCHAR2(10);
ie_leito_w				VARCHAR2(1);
nr_telefone_w			VARCHAR2(255);
dt_agendamento_w			DATE;
ie_equipamento_w			VARCHAR2(1);
ie_autorizacao_w			VARCHAR2(3);
vl_previsto_w			NUMBER(15,2);
nr_seq_age_cons_w		NUMBER(10);
cd_medico_exec_w			VARCHAR2(10);
ie_video_w			VARCHAR2(1);
nr_seq_classif_agenda_w		NUMBER(10);
ie_uc_w				VARCHAR2(1);
cd_procedencia_w			NUMBER(5);
cd_categoria_w			VARCHAR2(10);
cd_tipo_acomodacao_w		NUMBER(4);
nr_doc_convenio_w		VARCHAR2(20);
dt_validade_carteira_w		DATE;
dt_confirmacao_w			DATE;
nr_seq_proc_interno_w		NUMBER(10);
nr_seq_status_pac_w		NUMBER(10);
nm_usuario_confirm_w		VARCHAR2(15);
ie_lado_w			VARCHAR2(1);
ie_biopsia_w			VARCHAR2(1);
ie_congelacao_w			VARCHAR2(1);
ds_laboratorio_w			VARCHAR2(80);
qt_min_padrao_w			NUMBER(15);
cd_doenca_cid_w			VARCHAR2(10);
dt_nascimento_pac_w		DATE;
nr_seq_sala_w			NUMBER(10);
nm_medico_externo_w		VARCHAR2(60);
ie_tipo_atendimento_w		NUMBER(3);
ie_consulta_anestesica_w		VARCHAR2(1);
ie_pre_internacao_w		VARCHAR2(1);
ie_reserva_leito_w			VARCHAR2(3);
ie_tipo_anestesia_w		NUMBER(2);
dt_chegada_w			DATE;
cd_medico_req_w			VARCHAR2(10);
nr_seq_pq_proc_w			NUMBER(10);
qt_diaria_prev_w			NUMBER(3);
dt_chegada_fim_w			DATE;
ie_arco_c_w			VARCHAR2(1);
nr_seq_indicacao_w		NUMBER(10);
cd_pessoa_indicacao_w		VARCHAR2(10);
dt_termino_w			DATE;
dt_atual_w			DATE;
dt_dia_semana_w			NUMBER(1);
ie_feriado_w			VARCHAR2(1);
nr_seq_esp_w			NUMBER(10);
ie_hor_adic_w			VARCHAR2(1);
ie_valido_w			VARCHAR2(1);
qt_horario_w			NUMBER(10);
qt_horario_livre_w			NUMBER(10);
nr_sequencia_w			NUMBER(10);
ds_erro_w			VARCHAR2(1000);
ds_erro_2_w			VARCHAR2(1000);
ds_erro_3_w			VARCHAR2(1000);
ds_erro_4_w			VARCHAR2(1000);
qt_frequencia_w			NUMBER(5,0);
ie_final_semana_w			VARCHAR2(1);
nr_seq_livre_w			NUMBER(10,0);
qt_bloqueio_periodo_w	NUMBER(10);
ie_se_gera_hor_bloq_w	VARCHAR2(1);
ds_obs_erro_w		VARCHAR(255);
ie_bloqueado_w		VARCHAR2(1);


nr_seq_proced_w			NUMBER(10);
cd_procedimento_adic_w		NUMBER(15);
ie_origem_proced_adic_w		NUMBER(10);
nr_seq_proc_interno_adic_w		NUMBER(10);
ie_lado_adic_w			VARCHAR2(1);
cd_medico_adic_w		VARCHAR2(10);
Ie_gerar_proced_adic_w		VARCHAR2(1);
ie_consistir_sobrep_w		varchar2(1);
qt_min_tot_agend_w			number(10) := 0;
qt_min_dur_tempo_proced_w	number(10) := 0;
qt_tot_min_dur_proc_adic_w	number(10) := 0;
qt_min_dur_tempo_proc_adic_w	number(10) := 0;
cd_procedimento_w_regra		number(15,0);
ie_origem_proced_regra_w	number(10,0);
nr_seq_proc_interno_regra_w	number(10,0);
ie_lado_adic_regra_w		varchar2(1);
cd_medico_adic_regra_w		varchar2(10);
ie_se_hor_sobreposto_w	varchar2(1);

CURSOR c01 IS
SELECT	cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno,
		ie_lado,
		cd_medico
FROM	agenda_paciente_proc
WHERE	nr_sequencia = nr_seq_agenda_p
ORDER BY nr_seq_agenda;


cursor c02 is
select	cd_procedimento,
		ie_origem_proced,
		nr_seq_proc_interno,
		ie_lado,
		cd_medico
from	agenda_paciente_proc
where	nr_sequencia = nr_seq_agenda_p
order by
		nr_seq_agenda;

BEGIN
ds_erro_w 	:= '';
ds_erro_3_w := '';
ds_erro_4_w := wheb_mensagem_pck.get_texto(794117);				

IF	(nr_seq_agenda_p IS NOT NULL) AND
	(dt_agenda_p IS NOT NULL) AND
	(ie_frequencia_p IS NOT NULL) AND
	(ie_final_semana_p IS NOT NULL) AND
	(nm_usuario_p IS NOT NULL) THEN
	/* obter dados agenda */
	SELECT	cd_agenda,
		cd_pessoa_fisica,
		dt_agenda,
		hr_inicio,
		nr_minuto_duracao,
		--nm_usuario,
		dt_atualizacao,
		cd_medico,
		nm_pessoa_contato,
		cd_procedimento,
		ds_observacao,
		cd_convenio,
		nr_cirurgia,
		ds_cirurgia,
		qt_idade_paciente,
		cd_tipo_anestesia,
		ie_origem_proced,
		--ie_status_agenda,
		nm_instrumentador,
		nm_circulante,
		ie_ortese_protese,
		ie_cdi,
		ie_uti,
		ie_banco_sangue,
		ie_serv_especial,
		cd_motivo_cancelamento,
		--nr_sequencia,
		ds_senha,
		cd_turno,
		cd_anestesista,
		cd_pediatra,
		nm_paciente,
		ie_anestesia,
		nr_atendimento,
		ie_carater_cirurgia,
		cd_usuario_convenio,
		--nm_usuario_orig,
		qt_idade_mes,
		cd_plano,
		ie_leito,
		nr_telefone,
		dt_agendamento,
		ie_equipamento,
		ie_autorizacao,
		vl_previsto,
		nr_seq_age_cons,
		cd_medico_exec,
		ie_video,
		nr_seq_classif_agenda,
		ie_uc,
		cd_procedencia,
		cd_categoria,
		cd_tipo_acomodacao,
		nr_doc_convenio,
		dt_validade_carteira,
		dt_confirmacao,
		nr_seq_proc_interno,
		nr_seq_status_pac,
		--nm_usuario_confirm,
		ie_lado,
		ie_biopsia,
		ie_congelacao,
		ds_laboratorio,
		qt_min_padrao,
		cd_doenca_cid,
		dt_nascimento_pac,
		nr_seq_sala,
		nm_medico_externo,
		ie_tipo_atendimento,
		ie_consulta_anestesica,
		ie_pre_internacao,
		ie_reserva_leito,
		ie_tipo_anestesia,
		dt_chegada,
		cd_medico_req,
		nr_seq_pq_proc,
		qt_diaria_prev,
		dt_chegada_fim,
		ie_arco_c,
		nr_seq_indicacao,
		cd_pessoa_indicacao
	INTO	cd_agenda_w,
		cd_pessoa_fisica_w,
		dt_agenda_w,
		hr_inicio_w,
		nr_minuto_duracao_w,
		--nm_usuario_w,
		dt_atualizacao_w,
		cd_medico_w,
		nm_pessoa_contato_w,
		cd_procedimento_w,
		ds_observacao_w,
		cd_convenio_w,
		nr_cirurgia_w,
		ds_cirurgia_w,
		qt_idade_paciente_w,
		cd_tipo_anestesia_w,
		ie_origem_proced_w,
		--ie_status_agenda_w,
		nm_instrumentador_w,
		nm_circulante_w,
		ie_ortese_protese_w,
		ie_cdi_w,
		ie_uti_w,
		ie_banco_sangue_w,
		ie_serv_especial_w,
		cd_motivo_cancelamento_w,
		--nr_sequencia_w,
		ds_senha_w,
		cd_turno_w,
		cd_anestesista_w,
		cd_pediatra_w,
		nm_paciente_w,
		ie_anestesia_w,
		nr_atendimento_w,
		ie_carater_cirurgia_w,
		cd_usuario_convenio_w,
		--nm_usuario_orig_w,
		qt_idade_mes_w,
		cd_plano_w,
		ie_leito_w,
		nr_telefone_w,
		dt_agendamento_w,
		ie_equipamento_w,
		ie_autorizacao_w,
		vl_previsto_w,
		nr_seq_age_cons_w,
		cd_medico_exec_w,
		ie_video_w,
		nr_seq_classif_agenda_w,
		ie_uc_w,
		cd_procedencia_w,
		cd_categoria_w,
		cd_tipo_acomodacao_w,
		nr_doc_convenio_w,
		dt_validade_carteira_w,
		dt_confirmacao_w,
		nr_seq_proc_interno_w,
		nr_seq_status_pac_w,
		--nm_usuario_confirm_w,
		ie_lado_w,
		ie_biopsia_w,
		ie_congelacao_w,
		ds_laboratorio_w,
		qt_min_padrao_w,
		cd_doenca_cid_w,
		dt_nascimento_pac_w,
		nr_seq_sala_w,
		nm_medico_externo_w,
		ie_tipo_atendimento_w,
		ie_consulta_anestesica_w,
		ie_pre_internacao_w,
		ie_reserva_leito_w,
		ie_tipo_anestesia_w,
		dt_chegada_w,
		cd_medico_req_w,
		nr_seq_pq_proc_w,
		qt_diaria_prev_w,
		dt_chegada_fim_w,
		ie_arco_c_w,
		nr_seq_indicacao_w,
		cd_pessoa_indicacao_w
	FROM	agenda_paciente
	WHERE	nr_sequencia = nr_seq_agenda_p;

	SELECT	NVL(MAX(obter_valor_param_usuario(820, 159, obter_perfil_ativo, nm_usuario_p, obter_estab_agenda(cd_agenda_w))), 'S')
	INTO	Ie_gerar_proced_adic_w
	FROM	dual;
	
	select 	nvl(max(ie_consiste_duracao),'I')
	into	ie_consistir_sobrep_w	
	from 	parametro_agenda
	where 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	/* obter frequencia */
	IF	(ie_frequencia_p = 'S') THEN
		qt_frequencia_w := 7;

	ELSIF	(ie_frequencia_p = 'D') THEN
		qt_frequencia_w := 1;

	ELSIF	(ie_frequencia_p = 'O') THEN
		qt_frequencia_w := NVL(qt_frequencia_p,0);
	END IF;

	/* validar frequencia */
	IF	(qt_frequencia_w <= 0) THEN
		Wheb_mensagem_pck.exibir_mensagem_abort(251139);
	END IF;

	/* obter datas */
	dt_termino_w := TO_DATE(TO_CHAR(dt_agenda_p,'dd/mm/yyyy') || ' ' || TO_CHAR(hr_inicio_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
	dt_atual_w := hr_inicio_w + qt_frequencia_w;

	/* validar data */
	IF	(dt_agenda_p < dt_agenda_w) THEN
		Wheb_mensagem_pck.exibir_mensagem_abort(251140);
	END IF;

	IF	(ie_frequencia_p = 'S') AND
		(dt_agenda_p < dt_atual_w) THEN
		Wheb_mensagem_pck.exibir_mensagem_abort(262642);
	END IF;

	/* obter dias semana */
	WHILE	(dt_atual_w <= dt_termino_w) LOOP
		BEGIN
		/* obter dia semana */
		SELECT	obter_cod_dia_semana(dt_atual_w)
		INTO	dt_dia_semana_w
		FROM	dual;

		/* obter se feriado */
		SELECT	DECODE(COUNT(*),0,'N','S')
		INTO	ie_feriado_w
		FROM 	feriado a,
			agenda b
		WHERE 	a.cd_estabelecimento = obter_estab_agenda(cd_agenda_w)
		AND	a.dt_feriado = TRUNC(dt_atual_w)
		AND 	b.cd_agenda = cd_agenda_w;

		/* obter se hor�rio especial */
		SELECT	NVL(MAX(nr_sequencia),0),
			NVL(MAX(ie_horario_adicional),'N')
		INTO	nr_seq_esp_w,
			ie_hor_adic_w
		FROM	agenda_horario_esp
		WHERE	cd_agenda = cd_agenda_w
		AND	dt_agenda = TRUNC(dt_agenda_w,'dd')
		AND	hr_inicio_w BETWEEN  hr_inicial AND hr_final;

		IF (nr_seq_esp_w > 0) AND
		   (ie_hor_adic_w = 'N') THEN


		   ds_erro_2_w := WHEB_MENSAGEM_PCK.get_texto(277592,null);

		END IF;


		/* obter se final semana */
		IF	(dt_dia_semana_w IN (7,1)) THEN
			ie_final_semana_w := 'S';
		ELSE
			ie_final_semana_w := 'N';
		END IF;

		/* validar hor�rio x cadastro */
		SELECT	DECODE(COUNT(*),0,'N','S')
		INTO	ie_valido_w
		FROM	agenda_horario
		WHERE	cd_agenda = cd_agenda_w
		AND	((dt_dia_semana = dt_dia_semana_w) OR ((dt_dia_semana = 9) AND (dt_dia_semana_w NOT IN (7,1))))
		AND	((nr_seq_esp_w = 0) OR (ie_hor_adic_w = 'S'))
		AND	((dt_final_vigencia IS NULL) OR (dt_final_vigencia >= TRUNC(dt_atual_w)))
		AND	((dt_inicio_vigencia IS NULL) OR (dt_inicio_vigencia <= TRUNC(dt_atual_w)))
		AND 	hr_inicial < hr_final
		AND	NVL(nr_minuto_intervalo,0) > 0
		AND	ie_feriado_w <> 'S';

		/* validar hor�rio gerado */
		SELECT	COUNT(*)
		INTO	qt_horario_w
		FROM	agenda_paciente
		WHERE	cd_agenda = cd_agenda_w
		AND	hr_inicio = dt_atual_w;

		/* validar hor�rio livre */
		SELECT	COUNT(*)
		INTO	qt_horario_livre_w
		FROM	agenda_paciente
		WHERE	cd_agenda = cd_agenda_w
		AND		hr_inicio = dt_atual_w
		AND		ie_status_agenda = 'L'
		AND		obter_se_feriado(obter_estab_agenda(cd_agenda_w), dt_atual_w) = 0;

		/*INICIO - validar se existe bloqueio cadastrado para a agenda*/

			consistir_bloq_Agenda_obs(cd_agenda_w, dt_atual_w, dt_dia_semana_w, ie_bloqueado_w, ds_obs_erro_w);

			IF (ie_bloqueado_w = 'S') THEN
				ie_valido_w := 'N';
			END IF;
			/* Alterado pela ortina acima em 15/11/2013 - Elton

			select	nvl(count(*), 0)
			into	qt_bloqueio_periodo_w
			from	agenda_bloqueio
			where	cd_agenda	= cd_agenda_w
			and		trunc(dt_atual_w)	between	dt_inicial and dt_final
			and		to_date(dt_atual_w,'dd/mm/yyyy hh24:mi:ss') >=
					to_date(to_char(dt_inicial, 'dd/mm/yyyy') ||' '|| to_char(nvl(hr_inicio_bloqueio, to_date('30/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss')),'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')
			and		to_date(dt_atual_w,'dd/mm/yyyy hh24:mi:ss') <=
					to_date(to_char(dt_final, 'dd/mm/yyyy') ||' '|| to_char(nvl(hr_final_bloqueio, to_date('30/12/1899 23:59:59', 'dd/mm/yyyy hh24:mi:ss')),'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss');

			if	(qt_bloqueio_periodo_w > 0) then
				ie_valido_w	:= 'N';
			end if;

			*/
		/*FIM - validar se existe bloqueio cadastrado para a agenda*/
		
		
		/*Consistir a regra 'Tempo Proced' ao efetuar a c�pia/transfer�ncia*/
		if	(cd_agenda_w is not null)then
			begin		
			select	obter_tempo_duracao_proced(	cd_agenda_w,
												cd_medico_exec_w,
												cd_procedimento_w,
												ie_origem_proced_w,
												cd_pessoa_fisica_w,
												nr_seq_proc_interno_w,
												ie_lado_w,
												cd_convenio_w,
												cd_categoria_w,
												cd_plano_w,
												nr_seq_agenda_p,
												null)
			into	qt_min_dur_tempo_proced_w
			from	dual;
			exception
			when others then
				qt_min_dur_tempo_proced_w := 0;	
			end;	
		end if;	
		
		/*Consistir a regra 'Tempo Proced' ao efetuar a c�pia/transfer�ncia para os procedimentos adicionais*/
		open c02;
		loop
		fetch c02 into	cd_procedimento_w_regra,
						ie_origem_proced_regra_w,
						nr_seq_proc_interno_regra_w,
						ie_lado_adic_regra_w,
						cd_medico_adic_regra_w;
		exit when c02%notfound;
			begin						
			if	(cd_agenda_w is not null)then
				begin		
				select	obter_tempo_duracao_proced(	cd_agenda_w,
													nvl(cd_medico_adic_regra_w, cd_medico_exec_w),
													cd_procedimento_w_regra,
													ie_origem_proced_regra_w,
													cd_pessoa_fisica_w,
													nr_seq_proc_interno_regra_w,
													ie_lado_adic_regra_w,
													cd_convenio_w,
													cd_categoria_w,
													cd_plano_w,
													nr_seq_agenda_p,
													null)
				into	qt_min_dur_tempo_proc_adic_w
				from	dual;
				exception
				when others then
					qt_min_dur_tempo_proc_adic_w := 0;	
				end;	
				
			qt_tot_min_dur_proc_adic_w	:= qt_tot_min_dur_proc_adic_w + qt_min_dur_tempo_proc_adic_w;
			end if;			
			end;
		end loop;
		close c02;	
		
		qt_min_tot_agend_w	:= qt_min_dur_tempo_proced_w + qt_tot_min_dur_proc_adic_w;
		
		/*Consistir sobreposi��o de hor�rios caso o pr�ximo agendamento n�o estiver livre*/		
		if	(ie_consistir_sobrep_w = 'I') then
			
			
			--ie_se_hor_sobreposto_w := obter_se_sobreposicao_horario(cd_agenda_w, dt_atual_w, qt_min_tot_agend_w);
			ie_se_hor_sobreposto_w := obter_se_agenda_sobreposta(cd_agenda_w, dt_atual_w, dt_atual_w, qt_min_tot_agend_w, nr_seq_agenda_p);		
			
			if (ie_se_hor_sobreposto_w = 'S')then 
				ie_valido_w	:= 'N';				
				ds_erro_3_w := ds_erro_3_w || ' ' || to_char(dt_atual_w,'dd/mm/yyyy');
			end if;		
		end if;
		
		
		IF	((ie_valido_w = 'S') AND
			 (qt_horario_w = 0)) OR
			((ie_valido_w = 'S') AND
			 (qt_horario_livre_w > 0)) THEN

			IF	(ie_final_semana_p = 'S') OR
				(ie_final_semana_w = 'N') THEN

				SELECT	MAX(nr_sequencia)
				INTO	nr_seq_livre_w
				FROM	agenda_paciente
				WHERE	cd_agenda = cd_agenda_w
				AND	hr_inicio = dt_atual_w
				AND	ie_status_agenda = 'L'
				AND	obter_se_feriado(obter_estab_agenda(cd_agenda_w), dt_atual_w) = 0;

				IF	(nr_seq_livre_w IS NULL) THEN

					/* obter sequencia */
					SELECT	agenda_paciente_seq.NEXTVAL
					INTO	nr_sequencia_w
					FROM	dual;

					/* gerar agenda */
					INSERT INTO agenda_paciente	(
									cd_agenda,
									cd_pessoa_fisica,
									dt_agenda,
									hr_inicio,
									nr_minuto_duracao,
									nm_usuario,
									dt_atualizacao,
									cd_medico,
									nm_pessoa_contato,
									cd_procedimento,
									ds_observacao,
									cd_convenio,
									nr_cirurgia,
									ds_cirurgia,
									qt_idade_paciente,
									cd_tipo_anestesia,
									ie_origem_proced,
									ie_status_agenda,
									nm_instrumentador,
									nm_circulante,
									ie_ortese_protese,
									ie_cdi,
									ie_uti,
									ie_banco_sangue,
									ie_serv_especial,
									cd_motivo_cancelamento,
									nr_sequencia,
									ds_senha,
									cd_turno,
									cd_anestesista,
									cd_pediatra,
									nm_paciente,
									ie_anestesia,
									nr_atendimento,
									ie_carater_cirurgia,
									cd_usuario_convenio,
									nm_usuario_orig,
									qt_idade_mes,
									cd_plano,
									ie_leito,
									nr_telefone,
									dt_agendamento,
									ie_equipamento,
									ie_autorizacao,
									vl_previsto,
									nr_seq_age_cons,
									cd_medico_exec,
									ie_video,
									nr_seq_classif_agenda,
									ie_uc,
									cd_procedencia,
									cd_categoria,
									cd_tipo_acomodacao,
									nr_doc_convenio,
									dt_validade_carteira,
									dt_confirmacao,
									nr_seq_proc_interno,
									nr_seq_status_pac,
									nm_usuario_confirm,
									ie_lado,
									ie_biopsia,
									ie_congelacao,
									ds_laboratorio,
									qt_min_padrao,
									cd_doenca_cid,
									dt_nascimento_pac,
									nr_seq_sala,
									nm_medico_externo,
									ie_tipo_atendimento,
									ie_consulta_anestesica,
									ie_pre_internacao,
									ie_reserva_leito,
									ie_tipo_anestesia,
									dt_chegada,
									cd_medico_req,
									nr_seq_pq_proc,
									qt_diaria_prev,
									dt_chegada_fim,
									ie_arco_c,
									nr_seq_indicacao,
									cd_pessoa_indicacao
									)
								VALUES	(
									cd_agenda_w,
									cd_pessoa_fisica_w,
									--dt_agenda_w,
									TRUNC(dt_atual_w),
									--hr_inicio_w,
									dt_atual_w,
									nr_minuto_duracao_w,
									--nm_usuario_w,
									nm_usuario_p,
									dt_atualizacao_w,
									cd_medico_w,
									nm_pessoa_contato_w,
									cd_procedimento_w,
									ds_observacao_w,
									cd_convenio_w,
									nr_cirurgia_w,
									ds_cirurgia_w,
									qt_idade_paciente_w,
									cd_tipo_anestesia_w,
									ie_origem_proced_w,
									--ie_status_agenda_w,
									'N',
									nm_instrumentador_w,
									nm_circulante_w,
									ie_ortese_protese_w,
									ie_cdi_w,
									ie_uti_w,
									ie_banco_sangue_w,
									ie_serv_especial_w,
									cd_motivo_cancelamento_w,
									nr_sequencia_w,
									ds_senha_w,
									cd_turno_w,
									cd_anestesista_w,
									cd_pediatra_w,
									nm_paciente_w,
									ie_anestesia_w,
									nr_atendimento_w,
									ie_carater_cirurgia_w,
									cd_usuario_convenio_w,
									--nm_usuario_orig_w,
									nm_usuario_p,
									qt_idade_mes_w,
									cd_plano_w,
									ie_leito_w,
									nr_telefone_w,
									dt_agendamento_w,
									ie_equipamento_w,
									ie_autorizacao_w,
									vl_previsto_w,
									nr_seq_age_cons_w,
									cd_medico_exec_w,
									ie_video_w,
									nr_seq_classif_agenda_w,
									ie_uc_w,
									cd_procedencia_w,
									cd_categoria_w,
									cd_tipo_acomodacao_w,
									nr_doc_convenio_w,
									dt_validade_carteira_w,
									dt_confirmacao_w,
									nr_seq_proc_interno_w,
									nr_seq_status_pac_w,
									--nm_usuario_confirm_w,
									nm_usuario_p,
									ie_lado_w,
									ie_biopsia_w,
									ie_congelacao_w,
									ds_laboratorio_w,
									qt_min_padrao_w,
									cd_doenca_cid_w,
									dt_nascimento_pac_w,
									nr_seq_sala_w,
									nm_medico_externo_w,
									ie_tipo_atendimento_w,
									ie_consulta_anestesica_w,
									ie_pre_internacao_w,
									ie_reserva_leito_w,
									ie_tipo_anestesia_w,
									dt_chegada_w,
									cd_medico_req_w,
									nr_seq_pq_proc_w,
									qt_diaria_prev_w,
									dt_chegada_fim_w,
									ie_arco_c_w,
									nr_seq_indicacao_w,
									cd_pessoa_indicacao_w
									);
					IF	(Ie_gerar_proced_adic_w	= 'S') THEN

						OPEN c01;
						LOOP
						FETCH c01 INTO	cd_procedimento_adic_w,
									ie_origem_proced_adic_w,
									nr_seq_proc_interno_adic_w,
									ie_lado_adic_w,
									cd_medico_adic_w;
						EXIT WHEN c01%NOTFOUND;
							BEGIN
							/* obter sequ�ncia */
							SELECT	NVL(MAX(nr_seq_agenda),0)+1
							INTO	nr_seq_proced_w
							FROM	agenda_paciente_proc
							WHERE	nr_sequencia = nr_sequencia_w;

							INSERT INTO agenda_paciente_proc	(
												nr_sequencia,
												nr_seq_agenda,
												cd_procedimento,
												ie_origem_proced,
												nr_seq_proc_interno,
												dt_atualizacao,
												nm_usuario,
												ie_lado,
												cd_medico
												)
											VALUES	(
												nr_sequencia_w,
												nr_seq_proced_w,
												cd_procedimento_adic_w,
												ie_origem_proced_adic_w,
												nr_seq_proc_interno_adic_w,
												SYSDATE,
												nm_usuario_p,
												ie_lado_adic_w,
												cd_medico_adic_w
												);
							END;
						END LOOP;
						CLOSE c01;
					END IF;
				ELSIF	(nr_seq_livre_w IS NOT NULL) THEN

					/* gerar agenda */
					UPDATE	agenda_paciente
					SET	cd_pessoa_fisica               =                cd_pessoa_fisica_w,
						dt_agenda                      =                TRUNC(dt_atual_w),
						hr_inicio                      =                dt_atual_w,
						nr_minuto_duracao              =                nr_minuto_duracao_w,
						nm_usuario                     =                nm_usuario_p,
						dt_atualizacao                 =                dt_atualizacao_w,
						cd_medico                      =                cd_medico_w,
						nm_pessoa_contato              =                nm_pessoa_contato_w,
						cd_procedimento                =                cd_procedimento_w,
						ds_observacao                  =                ds_observacao_w,
						cd_convenio                    =                cd_convenio_w,
						nr_cirurgia                    =                nr_cirurgia_w,
						ds_cirurgia                    =                ds_cirurgia_w,
						qt_idade_paciente              =                qt_idade_paciente_w,
						cd_tipo_anestesia              =                cd_tipo_anestesia_w,
						ie_origem_proced               =                ie_origem_proced_w,
						ie_status_agenda               =                'N',
						nm_instrumentador              =                nm_instrumentador_w,
						nm_circulante                  =                nm_circulante_w,
						ie_ortese_protese              =                ie_ortese_protese_w,
						ie_cdi                         =                ie_cdi_w,
						ie_uti                         =                ie_uti_w,
						ie_banco_sangue                =                ie_banco_sangue_w,
						ie_serv_especial               =                ie_serv_especial_w,
						cd_motivo_cancelamento         =                cd_motivo_cancelamento_w,
						ds_senha                       =                ds_senha_w,
						cd_turno                       =                cd_turno_w,
						cd_anestesista                 =                cd_anestesista_w,
						cd_pediatra                    =                cd_pediatra_w,
						nm_paciente                    =                nm_paciente_w,
						ie_anestesia                   =                ie_anestesia_w,
						nr_atendimento                 =                nr_atendimento_w,
						ie_carater_cirurgia            =                ie_carater_cirurgia_w,
						cd_usuario_convenio            =                cd_usuario_convenio_w,
						nm_usuario_orig                =                nm_usuario_p,
						qt_idade_mes                   =                qt_idade_mes_w,
						cd_plano                       =                cd_plano_w,
						ie_leito                       =                ie_leito_w,
						nr_telefone                    =                nr_telefone_w,
						dt_agendamento                 =                dt_agendamento_w,
						ie_equipamento                 =                ie_equipamento_w,
						ie_autorizacao                 =                ie_autorizacao_w,
						vl_previsto                    =                vl_previsto_w,
						nr_seq_age_cons                =                nr_seq_age_cons_w,
						cd_medico_exec                 =                cd_medico_exec_w,
						ie_video                       =                ie_video_w,
						nr_seq_classif_agenda          =                nr_seq_classif_agenda_w,
						ie_uc                          =                ie_uc_w,
						cd_procedencia                 =                cd_procedencia_w,
						cd_categoria                   =                cd_categoria_w,
						cd_tipo_acomodacao             =                cd_tipo_acomodacao_w,
						nr_doc_convenio                =                nr_doc_convenio_w,
						dt_validade_carteira           =                dt_validade_carteira_w,
						dt_confirmacao                 =                dt_confirmacao_w,
						nr_seq_proc_interno            =                nr_seq_proc_interno_w,
						nr_seq_status_pac              =                nr_seq_status_pac_w,
						nm_usuario_confirm             =                nm_usuario_p,
						ie_lado                        =                ie_lado_w,
						ie_biopsia                     =                ie_biopsia_w,
						ie_congelacao                  =                ie_congelacao_w,
						ds_laboratorio                 =                ds_laboratorio_w,
						qt_min_padrao                  =                qt_min_padrao_w,
						cd_doenca_cid                  =                cd_doenca_cid_w,
						dt_nascimento_pac              =                dt_nascimento_pac_w,
						nr_seq_sala                    =                nr_seq_sala_w,
						nm_medico_externo              =                nm_medico_externo_w,
						ie_tipo_atendimento            =                ie_tipo_atendimento_w,
						ie_consulta_anestesica         =                ie_consulta_anestesica_w,
						ie_pre_internacao              =                ie_pre_internacao_w,
						ie_reserva_leito               =                ie_reserva_leito_w,
						ie_tipo_anestesia              =                ie_tipo_anestesia_w,
						dt_chegada                     =                dt_chegada_w,
						cd_medico_req                  =                cd_medico_req_w,
						nr_seq_pq_proc                 =                nr_seq_pq_proc_w,
						qt_diaria_prev                 =                qt_diaria_prev_w,
						dt_chegada_fim                 =                dt_chegada_fim_w,
						ie_arco_c                      =                ie_arco_c_w,
						nr_seq_indicacao               =                nr_seq_indicacao_w,
						cd_pessoa_indicacao            =                cd_pessoa_indicacao_w
					WHERE	nr_sequencia 		       = 		nr_seq_livre_w;

					IF	(Ie_gerar_proced_adic_w	= 'S') THEN

						OPEN c01;
						LOOP
						FETCH c01 INTO	cd_procedimento_adic_w,
									ie_origem_proced_adic_w,
									nr_seq_proc_interno_adic_w,
									ie_lado_adic_w,
									cd_medico_adic_w;
						EXIT WHEN c01%NOTFOUND;
							BEGIN
							/* obter sequ�ncia */
							SELECT	NVL(MAX(nr_seq_agenda),0)+1
							INTO	nr_seq_proced_w
							FROM	agenda_paciente_proc
							WHERE	nr_sequencia = nr_seq_livre_w;

							INSERT INTO agenda_paciente_proc	(
												nr_sequencia,
												nr_seq_agenda,
												cd_procedimento,
												ie_origem_proced,
												nr_seq_proc_interno,
												dt_atualizacao,
												nm_usuario,
												ie_lado,
												cd_medico
												)
											VALUES	(
												nr_seq_livre_w,
												nr_seq_proced_w,
												cd_procedimento_adic_w,
												ie_origem_proced_adic_w,
												nr_seq_proc_interno_adic_w,
												SYSDATE,
												nm_usuario_p,
												ie_lado_adic_w,
												cd_medico_adic_w
												);
							END;
						END LOOP;
						CLOSE c01;
					END IF;

				END IF;

			END IF;
		ELSE
			IF	(ie_final_semana_p = 'S') OR
				(ie_final_semana_w = 'N') THEN
				ds_erro_w := ds_erro_w || TO_CHAR(dt_atual_w,'dd/mm/yyyy hh24:mi:ss') || ', ';
			END IF;
		END IF;
		dt_atual_w := dt_atual_w + qt_frequencia_w;
		END;
	END LOOP;
END IF;

IF	(NVL(LENGTH(ds_erro_w),0) > 0) THEN
	ds_erro_p := SUBSTR(WHEB_MENSAGEM_PCK.get_texto(277593,'DS_ERRO_W='||SUBSTR(ds_erro_w,1,LENGTH(ds_erro_w)-2)),1,255);
END IF;

IF	(NVL(LENGTH(ds_erro_2_w),0) > 0) THEN

	ds_erro_p := SUBSTR(ds_erro_2_w,1,255);
END IF;

IF	(NVL(LENGTH(ds_erro_3_w),0) > 0) THEN

	ds_erro_p := SUBSTR(ds_erro_4_w || ds_erro_3_w,1,255);
END IF;

COMMIT;

END gerar_agenda_pac_semana_per;
/
