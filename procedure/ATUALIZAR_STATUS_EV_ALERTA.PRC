create or replace
procedure	atualizar_status_ev_alerta(	nr_sequencia_p	number,
					nr_seq_evento_p	number,
					ie_status_p	varchar2) is

begin

if	(nr_seq_evento_p > 0) and
	(nr_sequencia_p > 0) then
	
	
	if	(ie_status_p = 'L') then

		update	ev_evento_paciente
		set	ie_status = ie_status_p
		where	nr_sequencia = nr_sequencia_p
		and	nr_seq_evento = nr_seq_evento_p;
		
		update	ev_evento_pac_destino
		set	dt_atualizacao		=	sysdate,
			ie_status 		=	ie_status_p,
			dt_ciencia		=	sysdate
		where	nr_seq_ev_pac		=	nr_sequencia_p		
		and	dt_ciencia is null;			
	
	else
	

		update	ev_evento_paciente
		set	ie_status = ie_status_p
		where	nr_sequencia = nr_sequencia_p
		and	nr_seq_evento = nr_seq_evento_p;
	
	
	end if;
	commit;

end if;

end atualizar_status_ev_alerta;
/
