create or replace
procedure pls_gravar_critica_copartic
			(	nr_seq_lib_coparticipacao_p	number,
				nr_seq_critica_p		number,
				nm_usuario_p			varchar2) is

begin

insert	into	pls_coparticipacao_critica
	(	nr_sequencia, nr_seq_lib_copartic, nr_seq_critica,
		dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec)
	values
	(	pls_coparticipacao_critica_seq.nextval, nr_seq_lib_coparticipacao_p, nr_seq_critica_p,
		sysdate, nm_usuario_p, sysdate, nm_usuario_p);

commit;

end pls_gravar_critica_copartic;
/