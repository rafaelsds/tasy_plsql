create or replace
procedure hfp_registrar_acesso(	nr_seq_avaliacao_p	number,
				cd_pessoa_fisica_p	varchar2,
				nm_usuario_p		varchar2) is 

begin
if (nr_seq_avaliacao_p is not null) then
	insert into hfp_acesso 
	(  nr_sequencia,
	   dt_atualizacao,
	   nm_usuario,
	   dt_atualizacao_nrec,
	   nm_usuario_nrec,
	   nr_seq_aval,
	   dt_entrada,
	   nm_usuario_entr,
	   cd_pessoa_fisica) values 
	(  hfp_acesso_seq.nextval,
	   sysdate,
	   nm_usuario_p,
	   sysdate,
	   nm_usuario_p,
	   nr_seq_avaliacao_p,
	   sysdate,
	   nm_usuario_p,
	   cd_pessoa_fisica_p);
end if;

commit;

end hfp_registrar_acesso;
/
