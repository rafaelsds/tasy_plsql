create or replace 
procedure calcular_agua_REP_HE(	nr_sequencia_p     	number,
					nm_usuario_p         	varchar2) is

qt_peso_w			number(15,3);	
nr_seq_elem_agua_w		number(10);
qt_vol_total_w			number(15,4);
qt_aporte_hidrico_diario_w	number(15,4);
qt_elem_kg_dia_w		number(15,4);

begin

select	qt_peso,
	qt_aporte_hidrico_diario
into	qt_peso_w,
	qt_aporte_hidrico_diario_w
from	prescr_rep_he
where	nr_sequencia		= nr_sequencia_p;

/*Rotina para atualizar o volume do elemento do tipo �gua para ficar compat�vel com Aporte h�drico */
select	max(a.nr_sequencia)
into	nr_seq_elem_agua_w
from	nut_elemento b,
	Prescr_Rep_HE_Elem a
where	a.nr_seq_rep_he		= nr_sequencia_p
and	a.nr_seq_elemento	= b.nr_sequencia
and	b.ie_tipo_elemento	= 'A';

if	(nr_seq_elem_agua_w > 0) then
	begin
	select	nvl(sum(Obter_vol_elem_REP_HE(nr_sequencia)),0)
	into	qt_vol_total_w	
	from	Prescr_Rep_HE_Elem
	where	nr_seq_rep_he	= nr_sequencia_p;
	
	if	(qt_aporte_hidrico_diario_w > qt_vol_total_w) then
		begin
		qt_elem_kg_dia_w	:= dividir((qt_aporte_hidrico_diario_w - qt_vol_total_w),qt_peso_w);
		
		update	prescr_rep_he_elem
		set	qt_elem_kg_dia	=  qt_elem_kg_dia + nvl(qt_elem_kg_dia_w,0)
		where	nr_seq_rep_he	= nr_sequencia_p
		and	nr_sequencia	= nr_seq_elem_agua_w;

		end;
	elsif	(qt_aporte_hidrico_diario_w < qt_vol_total_w) then
		begin
		select	sum(qt_elem_kg_dia)
		into	qt_elem_kg_dia_w
		from	prescr_rep_he_elem
		where	nr_seq_rep_he	= nr_sequencia_p
		and	nr_sequencia	= nr_seq_elem_agua_w;
		
		qt_elem_kg_dia_w	:= qt_elem_kg_dia_w - (dividir((qt_vol_total_w - qt_aporte_hidrico_diario_w),qt_peso_w));
		
		update	prescr_rep_he_elem
		set	qt_elem_kg_dia	= nvl(qt_elem_kg_dia_w,0)
		where	nr_seq_rep_he	= nr_sequencia_p
		and	nr_sequencia	= nr_seq_elem_agua_w;
	
		end;
	end if;
	end;
end if;

commit;

END calcular_agua_REP_HE;
/
