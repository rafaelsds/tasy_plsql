create or replace procedure pls_desfazer_lote_recuperacao
			(	nr_seq_lote_p		number,
				nm_usuario_p		varchar2) is
nr_seq_lote_w		pls_lote_reemb_recuperacao.nr_sequencia%type;
begin
if	(nr_seq_lote_p is not null) then
	select 	max(a.nr_sequencia)
	into	nr_seq_lote_w
	from	pls_lote_reemb_recuperacao a
	where	a.nr_sequencia	= nr_seq_lote_p
	and	a.dt_geracao_lote is not null
	and	a.dt_geracao_titulo is null;
	
	if	(nr_seq_lote_w is not null) then
		delete
		from	pls_lote_reemb_rec_item
		where	nr_seq_lote = nr_seq_lote_w;
		update	pls_lote_reemb_recuperacao
		set	dt_geracao_lote	= null,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate,
			vl_lote		= 0
		where	nr_sequencia	= nr_seq_lote_w;

		commit;
	end if;
end if;

end pls_desfazer_lote_recuperacao;
/
