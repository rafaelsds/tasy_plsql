create or replace
procedure pls_cancelar_fatura(	nr_seq_fatura_p		number,
				nr_seq_fatura_rec_p	number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	varchar2,
				nr_seq_mot_canc_p	number,
				ds_motivo_canc_p	varchar2,
				ie_commit_p		varchar2 default 'S') is
				
nr_seq_lote_w			pls_lote_faturamento.nr_sequencia%type;
nr_titulo_w			titulo_receber.nr_titulo%type;
ie_mes_fechado_w		varchar2(1);
nr_seq_fatura_w			pls_fatura.nr_sequencia%type;
nr_seq_fat_evento_new_w		pls_fatura_evento.nr_sequencia%type;
nr_seq_fat_conta_new_w		pls_fatura_conta.nr_sequencia%type;
nr_seq_fat_trib_new_w		pls_fatura_trib.nr_sequencia%type;
nr_seq_nota_fiscal_w		nota_fiscal.nr_sequencia%type;

cursor c01 (	nr_seq_fatura_pc	pls_fatura.nr_sequencia%type) is
	select	nr_sequencia
	from	pls_fatura_evento
	where	nr_seq_fatura = nr_seq_fatura_pc;
	
cursor c02 (	nr_seq_fat_evento_pc	pls_fatura_evento.nr_sequencia%type) is
	select	nr_sequencia,
		nr_seq_conta
	from	pls_fatura_conta
	where	nr_seq_fatura_evento = nr_seq_fat_evento_pc;
	
cursor c03 (	nr_seq_fat_conta_pc	pls_fatura_conta.nr_Sequencia%type) is
	select	nr_sequencia
	from	pls_fatura_proc
	where	nr_seq_fatura_conta = nr_seq_fat_conta_pc;
	
cursor c04 (	nr_seq_fat_conta_pc	pls_fatura_conta.nr_Sequencia%type) is
	select	nr_sequencia
	from	pls_fatura_mat
	where	nr_seq_fatura_conta = nr_seq_fat_conta_pc;
	
cursor c05 (	nr_seq_fatura_pc	pls_fatura.nr_sequencia%type) is
	select	nr_sequencia
	from	pls_fatura_trib
	where	nr_seq_fatura = nr_seq_fatura_pc;
	
begin

if	(nr_seq_fatura_p is not null) then
	select	nr_seq_lote,
		nr_titulo
	into	nr_seq_lote_w,
		nr_titulo_w
	from	pls_fatura
	where	nr_sequencia = nr_seq_fatura_p;
	
	ie_mes_fechado_w := pls_obter_se_mes_fechado(sysdate, 'T', cd_estabelecimento_p);
	
	if	(ie_mes_fechado_w = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(180908);
	end if;
	
	select	nvl(pls_obter_dados_pls_fatura(nr_seq_fatura_p, 'SNF'), pls_obter_dados_pls_fatura(nr_seq_fatura_p, 'SNFN')) nr_nota_fiscal
	into	nr_seq_nota_fiscal_w
	from	dual;
	
	if	(nr_seq_nota_fiscal_w is not null) then
		estornar_nota_fiscal(nr_seq_nota_fiscal_w, nm_usuario_p, ie_commit_p, ds_motivo_canc_p, 'CE');
	else
		pls_desv_tit_lote_faturamento(nr_seq_fatura_p, null, nm_usuario_p, 'N', nr_seq_mot_canc_p, ds_motivo_canc_p, 'C');
	end if;
	
	pls_gerar_fatura_log(nr_seq_lote_w, nr_seq_fatura_p, null, 'PLS_CANCELAR_FATURA', 'CF', ie_commit_p, nm_usuario_p);
	
	update	pls_fatura
	set	dt_cancelamento_fat	= sysdate,
		ie_cancelamento		= 'C',
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		nr_seq_motivo_canc	= nr_seq_mot_canc_p,
		ds_observacao		= substr(decode(ds_observacao, '', ds_motivo_canc_p, ds_observacao || chr(13) || chr(10) || ds_motivo_canc_p), 1, 255)
	where	nr_sequencia		= nr_seq_fatura_p;
	
	select	pls_fatura_seq.nextval
	into	nr_seq_fatura_w
	from	dual;
	
	insert into pls_fatura(	nr_sequencia,		nr_seq_lote,		nr_seq_congenere,
				nr_seq_pagador,		nr_agrupamento,		vl_fatura,
				dt_mes_competencia, 	dt_vencimento, 		ie_cancelamento,
				nm_usuario_nrec, 	nm_usuario, 		dt_atualizacao_nrec,
				dt_atualizacao, 	nr_seq_cancel_fat, 	vl_total_ndc,
				dt_vencimento_ndc, 	nr_seq_motivo_canc, 	ds_observacao,
				ie_tipo_fatura)
			select	nr_seq_fatura_w, 	nr_seq_lote_w, 		nr_seq_congenere,
				nr_seq_pagador, 	nr_agrupamento, 	vl_fatura * -1,
				sysdate, 		dt_vencimento, 		'E',
				nm_usuario_p, 		nm_usuario_p, 		sysdate,
				sysdate, 		nr_seq_fatura_rec_p, 	vl_total_ndc * -1,
				dt_vencimento_ndc, 	nr_seq_motivo_canc, 	ds_observacao,
				ie_tipo_fatura
			from	pls_fatura
			where 	nr_sequencia = nr_seq_fatura_p;
			
	for r_c01_w in c01 ( nr_seq_fatura_p ) loop
		select	pls_fatura_evento_seq.nextval
		into	nr_seq_fat_evento_new_w
		from	dual;
		
		insert into pls_fatura_evento(	nr_sequencia, 			nr_seq_fatura, 		nr_seq_evento,
						vl_evento, 			nm_usuario_nrec,	nm_usuario,
						dt_atualizacao_nrec, 		dt_atualizacao)
					select	nr_seq_fat_evento_new_w, 	nr_seq_fatura_w,	nr_seq_evento,
						vl_evento * -1, 		nm_usuario_p, 		nm_usuario_p,
						sysdate, 			sysdate
					from	pls_fatura_evento
					where	nr_sequencia = r_c01_w.nr_sequencia;
					
		for r_c02_w in c02 ( r_c01_w.nr_sequencia ) loop
			-- Limpa as informacoes da pasta Pos-Estabelecido -> Contabil
			update	pls_conta_pos_estab_contab
			set	nr_seq_lote_fat		= null,
				nr_seq_evento_fat	= null,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_seq_conta_pos in (	select	nr_sequencia
							from	pls_conta_pos_estabelecido
							where	nr_seq_conta 		= r_c02_w.nr_seq_conta
							and	nr_seq_lote_fat 	= nr_seq_lote_w
							and	((ie_situacao = 'A') or (ie_situacao is null)));
							
			-- Limpa as informacoes do pls_conta_pos_estabelecido
			update	pls_conta_pos_estabelecido
			set	nr_seq_lote_fat		= null,
				nr_seq_evento_fat	= null,
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_seq_conta 		= r_c02_w.nr_seq_conta
			and	nr_seq_lote_fat 	= nr_seq_lote_w
			and	((ie_situacao = 'A') or (ie_situacao is null));
			
			-- Limpa as informacoes do pls_conta_pos_proc
			update	pls_conta_pos_proc
			set	nr_seq_lote_fat 	= null,
				nr_seq_evento_fat	= null,
				ie_status_faturamento	= 'L',
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_seq_conta 		= r_c02_w.nr_seq_conta
			and	nr_seq_lote_fat		= nr_seq_lote_w;
			
			-- Limpa as informacoes do pls_conta_pos_mat
			update	pls_conta_pos_mat
			set	nr_seq_lote_fat 	= null,
				nr_seq_evento_fat	= null,
				ie_status_faturamento	= 'L',
				dt_atualizacao		= sysdate,
				nm_usuario		= nm_usuario_p
			where	nr_seq_conta 		= r_c02_w.nr_seq_conta
			and	nr_seq_lote_fat		= nr_seq_lote_w;
			
			select	pls_fatura_conta_seq.nextval
			into	nr_seq_fat_conta_new_w
			from	dual;
			
			insert into pls_fatura_conta(	nr_sequencia, 			nr_seq_fatura_evento, 		nr_seq_conta,
							nr_seq_segurado, 		vl_faturado, 			nr_lote_contabil,
							ie_tipo_cobranca, 		cd_conta_credito, 		cd_conta_debito,
							nm_usuario_nrec, 		nm_usuario, 			dt_atualizacao_nrec,
							dt_atualizacao, 		vl_faturado_ndc, 		nr_seq_congenere)
						select	nr_seq_fat_conta_new_w, 	nr_seq_fat_evento_new_w, 	nr_seq_conta,
							nr_seq_segurado, 		vl_faturado * -1, 		0,
							ie_tipo_cobranca, 		cd_conta_credito, 		cd_conta_debito,
							nm_usuario_p, 			nm_usuario_p, 			sysdate,
							sysdate, 			vl_faturado_ndc * -1, 		nr_seq_congenere
						from	pls_fatura_conta
						where	nr_sequencia = r_c02_w.nr_sequencia;
						
			for r_c03_w in c03 ( r_c02_w.nr_sequencia ) loop
				insert into pls_fatura_proc(	nr_sequencia, 			nr_seq_fatura_conta, 		nr_seq_conta_pos_estab,
								nr_seq_pos_proc,		nr_seq_conta_proc, 		nr_lote_contabil,
								cd_conta_credito,		cd_conta_debito, 		vl_faturado,
								nm_usuario_nrec,		nm_usuario, 			dt_atualizacao_nrec,
								dt_atualizacao,			nr_seq_fat_proc_cancel, 	vl_faturado_ndc,
								nr_seq_conta_pos_contab,	nr_seq_pos_proc_fat,		ie_tipo_cobranca)
							select	pls_fatura_proc_seq.nextval,	nr_seq_fat_conta_new_w, 	nr_seq_conta_pos_estab,
								nr_seq_pos_proc,		nr_seq_conta_proc, 		0,
								cd_conta_credito,		cd_conta_debito, 		vl_faturado * -1,
								nm_usuario_p,			nm_usuario_p, 			sysdate,
								sysdate,			nr_sequencia, 			vl_faturado_ndc * -1,
								nr_seq_conta_pos_contab,	nr_seq_pos_proc_fat,		ie_tipo_cobranca
							from	pls_fatura_proc
							where	nr_sequencia = r_c03_w.nr_sequencia;
			end loop;
			
			for r_c04_w in c04 ( r_c02_w.nr_sequencia ) loop
				insert into pls_fatura_mat(	nr_sequencia, 			nr_seq_fatura_conta, 		nr_seq_conta_pos_estab,
								nr_seq_pos_mat,			nr_lote_contabil, 		nr_seq_conta_mat,
								cd_conta_credito,		cd_conta_debito, 		vl_faturado,
								nm_usuario_nrec,		nm_usuario, 			dt_atualizacao_nrec,
								dt_atualizacao,			nr_seq_fat_mat_cancel, 		vl_faturado_ndc,
								nr_seq_conta_pos_contab,	nr_seq_pos_mat_fat,		ie_tipo_cobranca)
							select	pls_fatura_mat_seq.nextval,	nr_seq_fat_conta_new_w, 	nr_seq_conta_pos_estab,
								nr_seq_pos_mat,			0, 				nr_seq_conta_mat,
								cd_conta_credito,		cd_conta_debito, 		vl_faturado * -1,
								nm_usuario_p,			nm_usuario_p, 			sysdate,
								sysdate,			nr_sequencia, 			vl_faturado_ndc * -1,
								nr_seq_conta_pos_contab,	nr_seq_pos_mat_fat,		ie_tipo_cobranca
							from	pls_fatura_mat
							where	nr_sequencia = r_c04_w.nr_sequencia;
			end loop;
		end loop;
	end loop;
	
	-- TRIBUTOS
	for r_c05_w in c05 ( nr_seq_fatura_p ) loop
		select	pls_fatura_trib_seq.nextval
		into	nr_seq_fat_trib_new_w
		from	dual;
		
		-- TRIBUTO
		insert into pls_fatura_trib(	nr_sequencia, 				dt_atualizacao, 			nm_usuario,
						dt_atualizacao_nrec, 			nm_usuario_nrec, 			nr_seq_fatura,
						cd_tributo, 				tx_tributo, 				vl_tributo,
						vl_base_calculo, 			vl_trib_nao_retido, 			vl_base_nao_retido,
						vl_trib_adic, 				vl_base_adic, 				dt_tributo,
						ie_tipo_valor_fat, 			ie_origem_tributo, 			nr_seq_regra_trib)
					select	nr_seq_fat_trib_new_w, 			sysdate, 				nm_usuario_p,
						sysdate, 				nm_usuario_p, 				nr_seq_fatura_w,
						cd_tributo, 				tx_tributo * -1, 			vl_tributo * -1,
						vl_base_calculo * -1, 			vl_trib_nao_retido * -1, 		vl_base_nao_retido * -1,
						vl_trib_adic * -1, 			vl_base_adic * -1, 			dt_tributo,
						ie_tipo_valor_fat, 			ie_origem_tributo, 			nr_seq_regra_trib
					from	pls_fatura_trib
					where	nr_sequencia = r_c05_w.nr_sequencia;
		-- LOG
		insert into pls_regra_base_fat_log(	nr_sequencia,				dt_atualizacao,				nm_usuario,
							dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_fatura_trib,
							nr_seq_regra_base_fat,			cd_cgc,					ie_tipo_pessoa_prest_exec,
							cd_pessoa_fisica,			ie_tipo_pessoa_prest_pag,		ie_tipo_pessoa_prest_atend,
							ie_tipo_pessoa_prest_solic,		ie_ato_cooperado,			nr_seq_evento_fat,
							nr_seq_regra_fat,			ie_tipo_valor,				ie_tipo_pessoa_prest_ptu,
							ie_reducao_base_nf)
						select	pls_regra_base_fat_log_seq.nextval,	sysdate,				nm_usuario_p,
							sysdate,				nm_usuario_p,				nr_seq_fat_trib_new_w,
							nr_seq_regra_base_fat,			cd_cgc,					ie_tipo_pessoa_prest_exec,
							cd_pessoa_fisica,			ie_tipo_pessoa_prest_pag,		ie_tipo_pessoa_prest_atend,
							ie_tipo_pessoa_prest_solic,		ie_ato_cooperado,			nr_seq_evento_fat,
							nr_seq_regra_fat,			ie_tipo_valor,				ie_tipo_pessoa_prest_ptu,
							ie_reducao_base_nf
						from	pls_regra_base_fat_log
						where	nr_seq_fatura_trib = r_c05_w.nr_sequencia;
		-- CONTABIL
		insert into pls_fatura_item_trib(	nr_sequencia,				dt_atualizacao,				nm_usuario,
							dt_atualizacao_nrec,			nm_usuario_nrec,			nr_seq_fatura_evento,
							nr_seq_fatura_trib,			vl_evento,				vl_evento_origem,
							ie_tipo_valor_fat,			cd_classif_prov_cred,			cd_classif_prov_deb,
							cd_conta_prov_cred,			cd_conta_prov_deb,			cd_historico_prov,
							nr_seq_esquema_prov,			nr_lote_contabil_prov,			cd_classif_cred,
							cd_classif_deb,				cd_conta_cred,				cd_conta_deb,
							cd_historico,				nr_seq_esquema,				nr_lote_contabil)
						select	pls_fatura_item_trib_seq.nextval,	sysdate,				nm_usuario_p,
							sysdate,				nm_usuario_p,				nr_seq_fatura_evento,
							nr_seq_fat_trib_new_w,			vl_evento * -1,				vl_evento_origem * -1,
							ie_tipo_valor_fat,			null cd_classif_prov_cred,		null cd_classif_prov_deb,
							null cd_conta_prov_cred,		null cd_conta_prov_deb,			null cd_historico_prov,
							null nr_seq_esquema_prov,		null nr_lote_contabil_prov,		null cd_classif_cred,
							null cd_classif_deb,			null cd_conta_cred,			null cd_conta_deb,
							null cd_historico,			null nr_seq_esquema,			null nr_lote_contabil
						from	pls_fatura_item_trib
						where	nr_seq_fatura_trib = r_c05_w.nr_sequencia;
	end loop;
	
	if	(nr_seq_fatura_rec_p is null) and (nvl(ie_commit_p, 'S') = 'S') then
		commit;
	end if;
end if;

end pls_cancelar_fatura;
/
