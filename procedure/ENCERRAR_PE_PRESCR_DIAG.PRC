create or replace procedure encerrar_pe_prescr_diag(nr_sequencia_p  number,
                                                    ds_motivo_p     varchar2)
is
    dt_end_w        pe_prescr_diag.dt_end%type := sysdate;
    
begin
    update  patient_cp_intervention
    set     dt_end = dt_end_w
    where   dt_end is null
    and     nr_seq_pat_cp_interv_plan in    (select nr_sequencia
                                            from    patient_cp_interv_plan
                                            where   nr_seq_prescr_diag = nr_sequencia_p);

    update  patient_cp_interv_plan
    set     dt_end = dt_end_w
    where   dt_end is null
    and     nr_seq_prescr_diag = nr_sequencia_p;

    update  patient_cp_indicator
    set     dt_end = dt_end_w
    where   dt_end is null
    and     nr_seq_pat_cp_goal in   (select nr_sequencia
                                    from    patient_cp_goal
                                    where   nr_seq_prescr_diag = nr_sequencia_p);

    update  patient_cp_goal
    set     dt_end = dt_end_w
    where   dt_end is null
    and     nr_seq_prescr_diag = nr_sequencia_p;

    update  pe_prescr_proc
    set     dt_fim = dt_end_w
    where   dt_fim is null
    and     nr_seq_rel_diag = nr_sequencia_p;

    update  pe_prescr_proc
    set     dt_fim = dt_end_w
    where   dt_fim is null
    and     ie_related_problem = nr_sequencia_p;

    update  pe_prescr_diag
    set     ds_motivo = ds_motivo_p,
			dt_end = dt_end_w
    where   dt_end is null
    and     nr_sequencia = nr_sequencia_p;

    commit;
end encerrar_pe_prescr_diag;
/