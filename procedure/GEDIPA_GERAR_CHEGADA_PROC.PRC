create or replace
procedure gedipa_gerar_chegada_proc(nr_processo_p	number,
				    nm_usuario_p	Varchar2) is 

begin

if 	(nr_processo_p is not null) then 
	update	adep_processo 
	set	dt_entrega	   = sysdate, 
		nm_usuario_entrega = nm_usuario_p
	where 	nr_sequencia 	   = nr_processo_p;
end if;

commit;

end gedipa_gerar_chegada_proc;
/

