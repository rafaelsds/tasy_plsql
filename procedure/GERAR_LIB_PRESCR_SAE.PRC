create or replace
procedure gerar_lib_prescr_sae(
		nr_prescricao_p		number,
		nr_atendimento_p	number,
		ie_lib_prescr_farm_p	varchar2,
		ie_tipo_pessoa_p	varchar2,
		ie_prescricao_p		varchar2,
		cd_perfil_p		number,
		nm_usuario_p		varchar2) is

ds_erro_w	varchar2(2000);
		
begin
if	(nr_prescricao_p is not null) and
	(nr_atendimento_p is not null) and
	(nm_usuario_p is not null) then
	begin
	liberar_prescricao(
		nr_prescricao_p,
		nr_atendimento_p,
		ie_tipo_pessoa_p,
		cd_perfil_p,
		nm_usuario_p,
		ie_prescricao_p,
		ds_erro_w);
		
	if	(ie_lib_prescr_farm_p = 'S') then
		begin
		liberar_prescricao_farmacia(
			nr_prescricao_p,
			0,
			nm_usuario_p,
			'N');
		end;
	end if;
	end;
end if;
commit;
end gerar_lib_prescr_sae;
/