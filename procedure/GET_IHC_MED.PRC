create or replace
procedure get_ihc_med(	nr_sequencia_p       number, 
			nm_usuario_p	     varchar2) is 

nr_account_w   		 	ihc_claim.nr_account%type;
ie_origem_proced_w	 	procedimento_paciente.ie_origem_proced%type;
cd_procedimento_w	 	procedimento.cd_procedimento_loc%type;		
vl_charge_w     	 	ihc_med.vl_charge%type;
nm_serv_provider_w	 	ihc_med.nm_serv_provider%type;
dt_procedimento_w       procedimento_paciente.dt_procedimento%type;
nr_provider_w		 	medical_provider_number.nr_provider%type;
cd_pessoa_fisica_w	 	atendimento_paciente.cd_pessoa_fisica%type;
vl_medical_w 			ihc_claim.vl_medical%type	:= 0;
vl_charge_cid_w			ihc_claim.vl_charge%type;
ie_service_type_w		ihc_med.ie_service_type%type;
cd_medico_executor_w	procedimento_paciente.cd_medico_executor%type;

cursor  c01 is
select	nvl(a.vl_procedimento,0) * 100,
		b.cd_procedimento_loc cd_procedimento,
		a.ie_origem_proced,
		obter_nome_pf(a.cd_medico_executor),
		a.dt_procedimento,
		a.cd_medico_executor
from    procedimento_paciente a,
		procedimento b
where   b.cd_procedimento = a.cd_procedimento
and 	b.ie_origem_proced = a.ie_origem_proced
and 	b.cd_procedimento_loc = 'MED0123'
and  	a.nr_interno_conta = nr_account_w;

begin

select  max(nr_account)
into    nr_account_w
from    ihc_claim
where	nr_sequencia = nr_sequencia_p;

begin
open c01;
loop
fetch c01 into
	vl_charge_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	nm_serv_provider_w,
	dt_procedimento_w,
	cd_medico_executor_w;
exit when c01%notfound;
	begin
	select  max(nr_provider)
	into	nr_provider_w
	from 	medical_provider_number
	where   cd_medico = cd_medico_executor_w;

	if (nm_serv_provider_w is null) then
		generate_inco_eclipse(nr_account_w, 1, 'Proc.: ' || cd_procedimento_w || '. ' || obter_desc_expressao(964731),nm_usuario_p);
	end if;

	if (nr_provider_w is null) then
		generate_inco_eclipse(nr_account_w, 1, 'Proc.: ' || cd_procedimento_w || '. ' || obter_desc_expressao(964733),nm_usuario_p);
	end if;

	if (cd_procedimento_w is null) then
		generate_inco_eclipse(nr_account_w, 1, 'Proc.: ' || cd_procedimento_w || '. ' || obter_desc_expressao(964725),nm_usuario_p);
	end if;

	if (ie_origem_proced_w is null) then
		generate_inco_eclipse(nr_account_w, 1, 'Proc.: ' || cd_procedimento_w || '. ' || obter_desc_expressao(964727),nm_usuario_p);
	end if;

	if (dt_procedimento_w is null) then
		generate_inco_eclipse(nr_account_w, 1, 'Proc.: ' || cd_procedimento_w || '. ' || obter_desc_expressao(964729),nm_usuario_p);
	end if;

	get_eclipse_conversion('IE_ORIGEM_PROCED', ie_origem_proced_w, 'IHC', 'MED', nr_account_w, ie_service_type_w);

	if (BILLING_I18N_PCK.GET_VALIDATE_ECLIPSE() = 'N') then		
		vl_medical_w := vl_medical_w + nvl(vl_charge_w,0);
		
		insert into ihc_med ( 	 vl_charge,
					ie_charge_raised,
					cd_service,
					ie_service_type,		 
					dt_service,
					nm_serv_provider,
					cd_payee_provider,
					cd_provider,				 
					nm_usuario,
					nm_usuario_nrec,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nr_sequencia,
					nr_seq_claim)
				values ( vl_charge_w,
					decode(vl_charge_w,0,'I','C'),
					cd_procedimento_w,
					ie_service_type_w,
					dt_procedimento_w,
					nm_serv_provider_w,
					nr_provider_w,
					nr_provider_w,
					nm_usuario_p,
					nm_usuario_p,
					sysdate,
					sysdate,
					ihc_med_seq.nextval,	
					nr_sequencia_p);

	end if;
	end;
end loop;
close c01;
end;

select 	vl_charge
into 	vl_charge_cid_w
from 	ihc_claim
where 	nr_sequencia = nr_sequencia_p;

update 	ihc_claim
set 	vl_medical = vl_medical_w,
		vl_charge = vl_charge_cid_w + vl_medical_w
where 	nr_sequencia = nr_sequencia_p;

commit;

end get_ihc_med;
/