create or replace
procedure pls_oc_cta_tratar_val_60(	dados_regra_p		pls_tipos_ocor_pck.dados_regra,
					nr_id_transacao_p	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type,
					nm_usuario_p		usuario.nm_usuario%type) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Aplicar a valida��o das datas do protocolo.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

i			pls_integer;
dados_tb_sel_w		pls_tipos_ocor_pck.dados_table_selecao_ocor;
ie_gera_ocorrencia_w	varchar2(1);

--Dados da regra
cursor C01 (	nr_seq_oc_cta_comb_p	dados_regra_p.nr_sequencia%type) is
	select	a.ie_data_protocolo ie_data_protocolo
	from	pls_oc_cta_val_data_prot a
	where	a.nr_seq_oc_cta_comb	= nr_seq_oc_cta_comb_p ;
	
--Dados das contas
cursor C02 (	nr_id_transacao_pc	pls_oc_cta_selecao_ocor_v.nr_id_transacao%type) is
	select 	b.dt_recebimento_prot,
		b.dt_protocolo,
		b.nr_sequencia nr_seq_conta,
		b.dt_mes_competencia
	from 	pls_conta_ocor_v b
	where exists (	select 1
			from   pls_selecao_ocor_cta a
			where a.nr_id_transacao = nr_id_transacao_pc
			and   a.ie_valido = 'S'
			and   a.nr_seq_conta = b.nr_sequencia);

begin
-- Deve ter a informa��o da regra para que seja aplicada a valida��o.
if	(dados_regra_p.nr_sequencia is not null)  then
	-- tratamento em campo auxiliar para identificar posteriormente os registros que foram alterados
	pls_tipos_ocor_pck.atualiza_campo_auxiliar ('V', nr_id_transacao_p, null, dados_regra_p);
	for	r_C01_w in C01( dados_regra_p.nr_sequencia) loop

		if	( r_C01_w.ie_data_protocolo is not null) then
			
			i := 0;
			pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_sel_w);
			for	r_C02_w in C02 ( nr_id_transacao_p) loop
				
				ie_gera_ocorrencia_w := 'N';
				case ( r_C01_w.ie_data_protocolo)
					/*Data do recebimento menor que a data do protocolo(Se for maior ou igual, n�o gera ocorr�ncia)*/
					when '1' then
						if	( trunc(r_C02_w.dt_recebimento_prot,'dd') < trunc(r_C02_w.dt_protocolo,'dd')) then

							ie_gera_ocorrencia_w := 'S';
						end if;
					when '2' then
						-- Compara��o entre mes competencia do protocolo com o mes atual.
						-- Ambas as datas s�o convertidas para o primeiro dia do mes vigente, somente v�o possuir diferen�a se a competencia n�o estiver compreendido no mes vigente
						if	( trunc(r_C02_w.dt_mes_competencia, 'MM') < trunc(sysdate, 'MM')) then
							ie_gera_ocorrencia_w := 'S';
						end if;
					else
						null;
				end case;
				
				if 	(ie_gera_ocorrencia_w = 'S') then
				
					--Passa nr_seq_conta ao inv�s do nr_seq_selecao, pois ser� feito valida��o a n�vel de conta
					dados_tb_sel_w.nr_seq_selecao(i) := r_C02_w.nr_seq_conta;
					dados_tb_sel_w.ds_observacao(i) := null;
					dados_tb_sel_w.ie_valido(i) := 'S';
					
					if	(i >= pls_util_pck.qt_registro_transacao_w) then
					
						pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_sel_w.nr_seq_selecao, 
												pls_util_cta_pck.clob_table_vazia_w,
												'SEQ_CONTA', 
												dados_tb_sel_w.ds_observacao, 
												dados_tb_sel_w.ie_valido, 
												nm_usuario_p, 
												nr_id_transacao_p);
						pls_tipos_ocor_pck.limpar_nested_tables(dados_tb_sel_w);
						i := 0;
					else
						i := i + 1;
					end if;
				end if;
			end loop; --C02
		end if;
	end loop; -- C01
	
	pls_tipos_ocor_pck.gerencia_selecao_validacao(	dados_tb_sel_w.nr_seq_selecao, pls_util_cta_pck.clob_table_vazia_w,
							'SEQ_CONTA', dados_tb_sel_w.ds_observacao, 
							dados_tb_sel_w.ie_valido, nm_usuario_p, 
							nr_id_transacao_p);
							
	-- seta os registros que ser�o v�lidos ou inv�lidos ap�s o processamento 
	pls_tipos_ocor_pck.atualiza_campo_valido ('V', nr_id_transacao_p, null, dados_regra_p);
end if;
end pls_oc_cta_tratar_val_60;
/
