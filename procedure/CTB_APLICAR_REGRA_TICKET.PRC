create or replace
procedure ctb_aplicar_regra_ticket(	nr_seq_cenario_p		number,	
				nr_sequencia_p		number,
				cd_centro_custo_p		number,
				cd_conta_contabil_p	varchar2,
				nr_seq_mes_ref_p		number,
				nr_seq_metrica_p		number,
				dt_mes_inic_p		date,
				dt_mes_fim_p		date,
				ie_regra_p		varchar2,
				pr_aplicar_p		number,
				ie_sobrepor_p		varchar2,
				nr_seq_grupo_centro_p	number,
				nr_seq_grupo_conta_p	number,
				vl_fixo_p			number,
				cd_empresa_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

/*
ie_regra_valor - dominio 1719
PO              percentual sobre o valor de origem
PV              percentual do valor do m�s em quest�o
VF              valor fixo
*/
cd_centro_custo_w		number(10);
cd_conta_contabil_w		varchar2(20);
nr_seq_mes_ref_w			number(10,0);
nr_seq_metrica_w			number(10);
nr_sequencia_w			number(10);
qt_reg_w				number(15);
vl_ticket_w			number(17,4);
vl_ticket_ww			number(17,4);

/* M�ses de referencia*/
Cursor  C01 is
select	nr_sequencia
from	ctb_mes_ref
where	dt_referencia between dt_mes_inic_p and dt_mes_fim_p
and		cd_empresa		= cd_empresa_p
order by dt_referencia;


/* Centros de custo*/
cursor c02 is
select	cd_centro_custo_p
from	dual
where	cd_centro_custo_p is not null
union all
select	distinct
		cd_centro_custo
from	ctb_cen_ticket_medio
where	nr_seq_cenario	= nr_seq_cenario_p
and		ie_regra_p	= 'PV'
and		cd_centro_custo_p is null
and		nr_seq_grupo_centro_p is null
union all
select	b.cd_centro_custo
from	ctb_cen_grupo_centro a,
		ctb_cen_centro_grupo b
where	a.nr_sequencia	= b.nr_seq_grupo
and		a.nr_sequencia	= nr_seq_grupo_centro_p
and		cd_centro_custo_p is null
and 	nvl(cd_estab_exclusivo, cd_estabelecimento_p) = cd_estabelecimento_p
and		nr_seq_grupo_centro_p is not null
union all
select 	distinct
		a.cd_centro_custo
from	ctb_orc_metrica a
where	cd_centro_custo_p is null
and 	nvl(a.cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p
and		a.nr_seq_metrica 	= 	nr_seq_metrica_p; 


/* M�tricas do centro de custo*/
cursor c03 is
select	nr_seq_metrica_p
from	dual
where	nr_seq_metrica_p is not null
union all
select	a.nr_seq_metrica
from	ctb_orc_metrica a
where	a.cd_centro_custo		= cd_centro_custo_w
and		a.cd_estabelecimento	= cd_estabelecimento_p
and		nr_seq_metrica_p is null;

/* Conta cont�bil da regra ou do grupo de contas*/
cursor c04 is
select	cd_conta_contabil_p
from	dual
where	cd_conta_contabil_p is not null
union all
select	b.cd_conta_contabil
from	ctb_cen_grupo_conta a,
		ctb_cen_grupo_conta_conta b
where	a.nr_sequencia	= b.nr_seq_grupo
and		a.nr_sequencia	= nr_seq_grupo_conta_p
and 	nvl(a.cd_estab_exclusivo, cd_estabelecimento_p) = cd_estabelecimento_p
and		cd_conta_contabil_p is null
and		nr_seq_grupo_conta_p is not null;

/* Bsucar os valores base que ser�o utilizados para aplicar os ajustes*/
cursor c05 is
select	vl_ticket_ww /* Percentual de origem */
from	dual
where	ie_regra_p	= 'PO'
union all
select	a.vl_medio /* Percentual da quantidade */
from	ctb_cen_ticket_medio a
where	a.nr_seq_cenario		= nr_seq_cenario_p
and		a.nr_seq_metrica		= nr_seq_metrica_w
and		a.cd_centro_custo 		= cd_centro_custo_w
and		a.cd_conta_contabil 		= cd_conta_contabil_w
and		a.nr_seq_mes_ref		= nr_seq_mes_ref_w
and		nr_seq_mes_ref_p 	is null
and		ie_regra_p			= 'PV';

BEGIN

open C02;
loop
fetch C02 into	
	cd_centro_custo_w;
exit when C02%notfound;
	begin
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_metrica_w;
	exit when C03%notfound;
		begin
		
		
	
		open C04;
		loop
		fetch C04 into	
			cd_conta_contabil_w;
		exit when C04%notfound;
			begin
			
			if	(ie_regra_p = 'PO') then
				select	nvl(max(vl_medio),0)
				into	vl_ticket_ww
				from 	ctb_cen_ticket_medio
				where 	nr_seq_cenario		= nr_seq_cenario_p
				and		nr_seq_mes_ref		= nr_seq_mes_ref_p
				and		nr_seq_metrica		= nr_seq_metrica_w
				and		cd_centro_custo		= cd_centro_custo_w
				and		cd_conta_contabil	= cd_conta_contabil_w;
			end if;
			open C01;
			loop
			fetch C01 into	
				nr_seq_mes_ref_w;
			exit when C01%notfound;
				begin

				open c05;
				loop
				fetch c05 into
					vl_ticket_w;
				exit when c05%notfound;
					begin
					
					if	(ie_regra_p = 'PO') then /* Percentual sobre o valor base*/
						vl_ticket_w	:= vl_ticket_w + dividir((vl_ticket_w * pr_aplicar_p),100);
					elsif	(ie_regra_p = 'PV') then /* Percentual do valor */
						vl_ticket_w	:= vl_ticket_w * dividir(pr_aplicar_p, 100);
					end if;
				
					select	count(*)
					into	qt_reg_w
					from 	ctb_cen_ticket_medio
					where 	nr_seq_cenario		= nr_seq_cenario_p
					and	cd_estabelecimento		= cd_estabelecimento_p
					and	nr_seq_mes_ref			= nr_seq_mes_ref_w
					and	nr_seq_metrica			= nr_seq_metrica_w
					and	cd_centro_custo			= cd_centro_custo_w
					and	cd_conta_contabil		= cd_conta_contabil_w;

					if	(qt_reg_w = 0)	and
						(nvl(vl_ticket_w,0) <> 0) then
						select	ctb_cen_ticket_medio_seq.nextval
						into	nr_sequencia_w
						from	dual;
						
						insert into ctb_cen_ticket_medio(
							nr_sequencia,
							nr_seq_cenario,
							cd_estabelecimento,
							nr_seq_mes_ref,
							dt_atualizacao,
							nm_usuario,
							nr_seq_metrica,
							cd_centro_custo,
							cd_conta_contabil,
							vl_medio,
							vl_original)
						values (nr_sequencia_w,
							nr_seq_cenario_p,
							cd_estabelecimento_p,
							nr_seq_mes_ref_w,
							sysdate,
							nm_usuario_p,
							nr_seq_metrica_w,
							cd_centro_custo_w,
							cd_conta_contabil_w,
							vl_ticket_w,
							vl_ticket_w);
					elsif	(ie_sobrepor_p = 'S') then
						update	ctb_cen_ticket_medio 
						set	dt_atualizacao		= sysdate,
							nm_usuario		= nm_usuario_p,
							vl_medio		= vl_ticket_w
						where 	nr_seq_cenario		= nr_seq_cenario_p
						and	nr_seq_mes_ref 		= nr_seq_mes_ref_w
						and	nr_seq_metrica 		= nr_seq_metrica_w
						and	cd_centro_custo 	= cd_centro_custo_w
						and	cd_estabelecimento	= cd_estabelecimento_p
						and	cd_conta_contabil	= cd_conta_contabil_w;
					end if;
					end;
				end loop;
				close c05;
				end;
			end loop;
			close C01;
				
			end;
		end loop;
		close C04;
		end;
	end loop;
	close C03;
end;
end loop;
close C02;


commit;

end ctb_aplicar_regra_ticket;
/