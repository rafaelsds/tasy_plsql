create or replace
procedure delete_nota_fiscal_consist(	nr_sequencia_p	number,
					ie_nota_item_p	varchar2,
					ie_commit_p	varchar2 default 'S') is 

begin


delete from nota_fiscal_consist
where	nr_seq_nota = nr_sequencia_p
and	ie_nota_item = ie_nota_item_p;

if	(ie_commit_p = 'S') then
	commit;
end if;

end delete_nota_fiscal_consist;
/
