create or replace
procedure sum_atualizar_classif_diag(
	nr_seq_diag_p		number,
	ie_classif_diag_p	varchar2,
	nm_usuario_p		varchar2) is 

ie_classif_diag_w	varchar2(1);

begin

if	(nr_seq_diag_p is not null) then
	begin
	if	(ie_classif_diag_p is null) then
		select	max(a.ie_tipo_diagnostico)
		into	ie_classif_diag_w
		from	diagnostico_classif_adic a
		where	a.cd_doenca = (	select	max(b.cd_doenca)
					from	diagnostico_doenca b
					where	b.nr_seq_interno = nr_seq_diag_p);
	else
		ie_classif_diag_w := ie_classif_diag_p;
	end if;

	update	diagnostico_doenca
	set	ie_tipo_diag_classif = ie_classif_diag_w,
		nm_usuario = nm_usuario_p
	where	nr_seq_interno = nr_seq_diag_p;
	end;
end if;

commit;

end sum_atualizar_classif_diag;
/