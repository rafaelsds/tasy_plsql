create or replace
procedure san_registrar_orientacoes(
		nr_seq_triagem_p	number,
		ds_orientacoes_p	varchar2,
		ie_liberar_p	varchar2,
		ie_status_p	number,
		nm_usuario_p	varchar2) is

begin

san_gravar_orientacoes_doacao(nr_seq_triagem_p, ds_orientacoes_p, nm_usuario_p);

if	(ie_liberar_p = 'S') then
	san_atualizar_status_doacao(nr_seq_triagem_p, nm_usuario_p, ie_status_p);
end if;

end san_registrar_orientacoes;
/