create or replace
procedure Transfere_Conta_Protocolo( 	nr_seq_protocolo_p		number,
				nr_interno_conta_p		number,
				nm_usuario_p		Varchar2) is

nr_protocolo_w		varchar2(40);
dt_mesano_referencia_w	date;
ie_status_protocolo_old_w protocolo_convenio.ie_status_protocolo%type;

begin

begin
	select b.ie_status_protocolo
	into	ie_status_protocolo_old_w
	from conta_paciente a,
		protocolo_convenio b
	where a.nr_interno_conta = nr_interno_conta_p
	and a.nr_seq_protocolo = b.nr_seq_protocolo;
exception
	when others then
		ie_status_protocolo_old_w := 0;
end;

if (ie_status_protocolo_old_w <> 2) then
	begin
	
		select	nr_protocolo,
			dt_mesano_referencia
		into	nr_protocolo_w,
			dt_mesano_referencia_w
		from	protocolo_convenio
		where	nr_seq_protocolo	= nr_seq_protocolo_p;

		update	conta_paciente
		set	nr_protocolo		= nr_protocolo_w,
			dt_mesano_referencia	= dt_mesano_referencia_w,
			nr_seq_protocolo		= nr_seq_protocolo_p,
			nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate
		where	nr_interno_conta		= nr_interno_conta_p;

		commit;
	
	end;
end if;

end Transfere_Conta_Protocolo;
/
