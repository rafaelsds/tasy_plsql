create or replace
procedure inserir_adiantamento_titulo(
		nr_adiantamento_p	number,
		nr_seq_trans_fin_p	number,
		nr_titulo_p	number,
		vl_adiantamento_p	number,
		nr_seq_p		out number,
		nm_usuario_p	varchar2) is 
nr_seq_w	number(10);
begin
if	(nm_usuario_p is not null) then
	begin	
	select  	nvl(max(a.nr_sequencia),0) + 1
	into	nr_seq_w
	from    	titulo_pagar_adiant a
	where   	nr_titulo   = nr_titulo_p;
	
	insert 	into titulo_pagar_adiant
		(dt_atualizacao,
		dt_contabil,
		nm_usuario,
		nr_adiantamento,
		nr_lote_contabil,
		nr_seq_trans_fin,
		nr_sequencia,
		nr_titulo,
		vl_adiantamento)
	values  	(sysdate,
		sysdate,
		nm_usuario_p,
		nr_adiantamento_p,
		0,
		nr_seq_trans_fin_p,
		nr_seq_w,
		nr_titulo_p,
		vl_adiantamento_p);
	commit;
	end;
end if;
nr_seq_p	:= nr_seq_w;
end inserir_adiantamento_titulo;
/