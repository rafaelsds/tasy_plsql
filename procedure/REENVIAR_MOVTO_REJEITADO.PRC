create or replace
procedure reenviar_movto_rejeitado
			(	ds_lista_movto_p	varchar2,
				nr_seq_saldo_caixa_p	number,
				ds_historico_p		varchar2,
				nm_usuario_p		varchar2) is
				
nr_seq_caixa_od_rej_w		number(10);
nr_seq_movto_rejeitado_w	number(10);
nr_seq_movto_w			number(10);

Cursor c01 is
	select	a.nr_sequencia,
	a.nr_seq_movto_rejeitado
from	movto_trans_financ a
where	' ' || ds_lista_movto_p || ' ' like '% ' || a.nr_sequencia || ' %'
and	ds_lista_movto_p is not null
and	a.nr_seq_movto_transf is null;
				
begin
if	(ds_lista_movto_p is not null) then
	open C01;
	loop
	fetch C01 into
		nr_seq_movto_w,
		nr_seq_movto_rejeitado_w;
	exit when C01%notfound;
		begin
		if	(nr_seq_movto_rejeitado_w is not null) then
			select	max(nr_seq_caixa_od_rej)
			into	nr_seq_caixa_od_rej_w
			from	movto_trans_financ a
			where	a.nr_sequencia	= nr_seq_movto_rejeitado_w;
		
			if	(nr_seq_caixa_od_rej_w is not null) then
				gerar_transferencia_caixa(nr_seq_saldo_caixa_p,
							nr_seq_caixa_od_rej_w,
							nr_seq_movto_w || ' ',
							null,
							nm_usuario_p,
							sysdate,
							ds_historico_p,
							null);
			end if;
		end if;
		end;
	end loop;
	close C01;
end if;

commit;

end reenviar_movto_rejeitado;
/