create or replace
procedure gerar_tit_rec_contrato(
				dt_vencimento_p		date,
				qt_titulo_p		number,
				nr_seq_regra_p		number,
				nr_seq_contrato_p	number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2,
				dt_contabil_p		date) is

ie_forma_pagto_w		varchar2(01);
cd_pessoa_contratado_w	varchar2(60);
cd_cgc_contratado_w	varchar2(14);
cd_pessoa_contratante_w	varchar2(15);
cd_cgc_contratante_w	varchar2(15);
vl_total_w		number(15,2);
qt_titulo_w		number(02,0);
qt_meses_w		number(02,0) := 0;
qt_dias_w		number(10,0) := 0;
nr_titulo_w		number(10,0);
cd_conta_financ_w		number(10,0);
nr_seq_classif_w		number(10,0);
cd_estab_financeiro_w	number(10,0);
ie_origem_w		varchar2(10);
nr_seq_trans_financ_w	number(10,0);
nr_seq_trans_financ_baixa_w	number(15,0);
dt_inicio_vigencia_w	date;
dt_venc_atual_w		date;
dt_final_vigencia_w		date;
dt_contabil_w		date;
ie_tipo_titulo_w		varchar2(2) := '1';
vl_tipo_titulo_w		varchar2(2);
cd_tipo_juros_w			number(10);
tx_juros_w			contrato_regra_pagto.tx_juros%type;
cd_tipo_multa_w			number(10);
tx_multa_w			contrato_regra_pagto.tx_multa%type;
qt_periodo_variavel_w		number(5);
ie_periodo_variavel_w		varchar2(1);
nr_seq_conta_banco_w		number(10);
nr_seq_crit_rateio_w		number(10);
ie_centro_contrato_w		varchar2(1);
cd_centro_rateio_w		number(10);
cd_conta_contabil_rateio_w	varchar2(20);
cd_conta_financ_rateio_w	number(10);
pr_alocacao_w			number(15,3);
cd_centro_custo_w		number(10);
vl_total_rateio_w		number(15,2);
vl_rateio_w			number(15,2);
vl_titulo_w			number(15,2);
cd_conta_contabil_w		varchar2(255);
ie_gerar_adiantamento_w		varchar2(1);
nr_adiantamento_w		number(10);
cd_tipo_recebimento_w		number(10);
cd_perfil_w			number(10);
ie_consiste_vl_contrato_w	varchar2(1);
vl_total_contrato_w		number(15,2);
vl_titulos_w			number(15,2);
ie_considera_contratante_w	varchar2(1);
nr_seq_classe_tit_rec_w		contrato_regra_pagto.nr_seq_classe_tit_rec%type;

cursor c01 is
select	a.cd_centro_custo,
	a.pr_alocacao
from	contrato_centro_custo a
where	a.nr_seq_contrato	= nr_seq_contrato_p
and	((a.dt_inicio_vigencia is null) or
	(a.dt_fim_vigencia is null) or
	(PKG_DATE_UTILS.start_of(sysdate,'dd',0) between PKG_DATE_UTILS.start_of(a.dt_inicio_vigencia,'dd',0) and PKG_DATE_UTILS.start_of(a.dt_fim_vigencia,'dd',0)));

cursor c02 is
select	a.cd_centro_custo,
	a.cd_conta_contabil,
	a.cd_conta_financ,
	a.pr_rateio
from	ctb_criterio_rateio_item a,
	ctb_criterio_rateio b
where	b.nr_sequencia	= a.nr_seq_criterio
and	b.nr_sequencia	= nr_seq_crit_rateio_w;

begin
cd_perfil_w	:= obter_perfil_ativo;

dt_venc_atual_w		:= dt_vencimento_p;
qt_titulo_w		:= nvl(qt_titulo_p,0);
dt_contabil_w		:= nvl(dt_contabil_p,PKG_DATE_UTILS.start_of(PKG_DATE_UTILS.END_OF(dt_venc_atual_w, 'MONTH', 0),'dd',0));
obter_param_usuario(1200,68,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,vl_tipo_titulo_w);
obter_param_usuario(1200,139,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_consiste_vl_contrato_w);
obter_param_usuario(1200,141,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_gerar_adiantamento_w);
obter_param_usuario(1200,142,cd_perfil_w,nm_usuario_p,cd_estabelecimento_p,ie_considera_contratante_w);


if	(nvl(vl_tipo_titulo_w,'1') <> '1') then
	ie_tipo_titulo_w := vl_tipo_titulo_w;
end if;


select 	ie_forma,
	nvl(vl_pagto,0),
	cd_conta_financ,
	dt_inicio_vigencia,
	dt_final_vigencia,
	nr_seq_trans_financ,
	nr_seq_trans_fin_baixa,
	nvl(CD_TIPO_TAXA_JURO,2),
	nvl(TX_JUROS,0),
	nvl(CD_TIPO_TAXA_MULTA,2),
	nvl(TX_MULTA,0),
	nvl(ie_periodo_variavel,'D'),
	nvl(qt_periodo_variavel,0),
	nr_seq_conta_banco,
	ie_centro_contrato,
	nr_seq_crit_rateio,
	cd_conta_contabil,
	nr_seq_classe_tit_rec
into 	ie_forma_pagto_w,
	vl_total_w,
	cd_conta_financ_w,
	dt_inicio_vigencia_w,
	dt_final_vigencia_w,
	nr_seq_trans_financ_w,
	nr_seq_trans_financ_baixa_w,
	cd_tipo_juros_w,
	tx_juros_w,
	cd_tipo_multa_w,
	tx_multa_w,
	ie_periodo_variavel_w,
	qt_periodo_variavel_w,
	nr_seq_conta_banco_w,
	ie_centro_contrato_w,
	nr_seq_crit_rateio_w,
	cd_conta_contabil_w,
	nr_seq_classe_tit_rec_w
from 	contrato_regra_pagto
where	nr_sequencia	= nr_seq_regra_p;

gerenciamento_contrato_pck.consistir_tit_receber_contrato(nr_seq_contrato_p, 0, qt_titulo_p * vl_total_w, nm_usuario_p, 'N');

if	(vl_total_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(351780);	
	/*Para a gera��o do t�tulo � pagar, o valor da regra deve ser maior que zero.*/
end if;

if	(dt_inicio_vigencia_w is not null) and
	(dt_final_vigencia_w is not null) and
	((trunc(dividir(Obter_Dias_Entre_Datas(dt_inicio_vigencia_w, dt_final_vigencia_w),30)) + 1) < qt_titulo_p) then
	/*(-20011,'A quantidade de parcelas supera a quantidade de meses da vig�ncia da regra.');*/
	wheb_mensagem_pck.exibir_mensagem_abort(177853);
end if;

select	cd_pessoa_contratada,
		cd_cgc_contratado,
		nvl(vl_total_contrato,0),
		cd_pessoa_contratante,
		cd_cnpj_contratante
into	cd_pessoa_contratado_w,
		cd_cgc_contratado_w,
		vl_total_contrato_w,
		cd_pessoa_contratante_w,
		cd_cgc_contratante_w
from	contrato
where	nr_sequencia	= nr_seq_contrato_p;

if	(cd_pessoa_contratado_w is not null) and
	(cd_cgc_contratado_w is not null) then
	cd_pessoa_contratado_w := null;
end if;

if	(cd_pessoa_contratante_w is not null) and
	(cd_cgc_contratante_w is not null) then
	cd_pessoa_contratante_w := null;
end if;

if	(ie_forma_pagto_w = 'A') then
	qt_meses_w	:= 12;
elsif	(ie_forma_pagto_w = 'B') then
	qt_meses_w	:= 2;
elsif	(ie_forma_pagto_w = 'M') then
	qt_meses_w	:= 1;
elsif	(ie_forma_pagto_w = 'T') then
	qt_meses_w	:= 3;
elsif	(ie_forma_pagto_w = 'F') then
	qt_meses_w	:= 0;
elsif	(ie_forma_pagto_w = 'R') then /* Semestral */
	qt_meses_w	:= 6;
elsif	(ie_forma_pagto_w = 'Q') then
	qt_dias_w	:= 15;
elsif	(ie_forma_pagto_w = 'S') then
	qt_dias_w	:= 7;
elsif	(ie_forma_pagto_w = 'V') then
	begin

	if	(ie_periodo_variavel_w = 'M') then
		qt_meses_w	:= qt_periodo_variavel_w;
	else
		qt_dias_w	:= qt_periodo_variavel_w;
	end if;

	end;	
end if;

select	nvl(cd_estab_financeiro, cd_estabelecimento)
into	cd_estab_financeiro_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_p;

/* Francisco - OS 77083 - 13/12/2007 */
select	nvl(max(ie_origem_tit_contrato),'9')
into	ie_origem_w
from	parametro_contas_receber
where	cd_estabelecimento	= cd_estabelecimento_p;

while	(qt_titulo_w > 0) loop
	begin
	select	titulo_seq.nextval
	into	nr_titulo_w
	from	dual;

	if	(ie_consiste_vl_contrato_w = 'S') and
		(vl_total_contrato_w > 0) then
		select	sum(vl_titulo)
		into	vl_titulos_w
		from	titulo_receber
		where	nr_seq_contrato = nr_seq_contrato_p
		and	ie_situacao <> 3;

		vl_titulos_w := (vl_titulos_w + vl_total_w);
		if	(vl_titulos_w > vl_total_contrato_w) then
			/* O valor do t�tulo ultrapassou o saldo do contrato.*/
			wheb_mensagem_pck.exibir_mensagem_abort(245437);
		end if;
	end if;

	insert into titulo_receber
			(nr_titulo,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			dt_contabil,
			dt_emissao,
			dt_vencimento,
			dt_pagamento_previsto,
			vl_titulo,
			vl_saldo_titulo,
			vl_saldo_juros,
			vl_saldo_multa,
			cd_moeda,
			cd_portador,
			cd_tipo_portador,
			tx_juros,
			tx_multa,
			cd_tipo_taxa_juro,
			cd_tipo_taxa_multa,
			tx_desc_antecipacao,
			ie_situacao,
			ie_tipo_emissao_titulo,
			ie_origem_titulo,
			ie_tipo_titulo,
			ie_tipo_inclusao,
			cd_pessoa_fisica,
			cd_cgc,
			nr_seq_contrato,
			cd_estab_financeiro,
			nr_seq_trans_fin_contab,
			nm_usuario_orig,
			dt_inclusao,
			nr_seq_trans_fin_baixa,
			nr_seq_conta_banco,
			nr_seq_classe)
		values(	nr_titulo_w,
			cd_estabelecimento_p,
			sysdate,
			nm_usuario_p,
			dt_contabil_w,
			sysdate,
			dt_venc_atual_w,
			dt_venc_atual_w,
			vl_total_w,
			vl_total_w,
			0,
			0,
			1,
			0,
			0,
			tx_juros_w,
			tx_multa_w,
			cd_tipo_juros_w,
			cd_tipo_multa_w,
			0,
			'1',
			1,
			ie_origem_w,
			ie_tipo_titulo_w,
			'2',
			decode(nvl(ie_considera_contratante_w,'N'),'S', cd_pessoa_contratante_w, cd_pessoa_contratado_w),
			decode(nvl(ie_considera_contratante_w,'N'),'S', cd_cgc_contratante_w, cd_cgc_contratado_w),
			nr_seq_contrato_p,
			cd_estab_financeiro_w,
			nr_seq_trans_financ_w,
			nm_usuario_p,
			sysdate,
			nr_seq_trans_financ_baixa_w,
			nr_seq_conta_banco_w,
			nr_seq_classe_tit_rec_w);
			
	if	(cd_conta_financ_w is not null) and
		(nr_seq_crit_rateio_w is null) and
		(ie_centro_contrato_w = 'N') then
				
		select	nvl(max(nr_sequencia),0) + 1
		into	nr_seq_classif_w
		from	titulo_receber_classif
		where	nr_titulo = nr_titulo_w;	
	
		insert into titulo_receber_classif
				(cd_centro_custo,
				cd_conta_financ,
				dt_atualizacao,
				nm_usuario,
				nr_sequencia,
				nr_titulo,
				vl_classificacao,
				vl_desconto,
				vl_original)
			values(	null,
				cd_conta_financ_w,
				sysdate,
				nm_usuario_p,
				nr_seq_classif_w,
				nr_titulo_w,
				vl_total_w,
				0,
				0);						
	
	end if;	
	
	if	(ie_centro_contrato_w = 'S') and
		(nr_seq_crit_rateio_w is null) then
		
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_classif_w
		from	titulo_receber_classif
		where	nr_titulo = nr_titulo_w;

		vl_total_rateio_w			:= 0;
		open C01;
		loop
		fetch C01 into	
			cd_centro_custo_w,
			pr_alocacao_w;
		exit when C01%notfound;
			begin


			vl_rateio_w	:= round(vl_total_w * (pr_alocacao_w / 100),2);
			vl_total_rateio_w	:= vl_total_rateio_w + vl_rateio_w;
			nr_seq_classif_w	:= nr_seq_classif_w + 1;

			
			insert into titulo_receber_classif(
				nr_titulo,
				nr_sequencia,
				cd_centro_custo,
				cd_conta_financ,
				nr_seq_contrato,
				dt_atualizacao,
				nm_usuario,
				vl_classificacao,
				vl_desconto,
				vl_original,
				cd_conta_contabil)
			values(	nr_titulo_w,
				nr_seq_classif_w,
				cd_centro_custo_w,
				cd_conta_financ_w,
				nr_seq_contrato_p,
				sysdate,
				nm_usuario_p,
				vl_rateio_w,
				0,
				0,
				cd_conta_contabil_w);
			end;
		end loop;
		close C01;


		if	(vl_total_rateio_w <> 0) then
			begin
			select	vl_titulo
			into	vl_titulo_w
			from	titulo_receber
			where	nr_titulo	= nr_titulo_w;

			update	titulo_receber_classif
			set	vl_classificacao 	= vl_classificacao + vl_titulo_w - vl_total_rateio_w
			where	nr_titulo		= nr_titulo_w
			and	nr_sequencia	= nr_seq_classif_w;
			
			end;
		end if;
	end if;
	
	if	(nr_seq_crit_rateio_w is not null) then
		begin
		
		vl_total_rateio_w	:= 0;
		
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_classif_w
		from	titulo_receber_classif
		where	nr_titulo = nr_titulo_w;
				
		open C02;
		loop
		fetch C02 into	
			cd_centro_rateio_w,
			cd_conta_contabil_rateio_w,
			cd_conta_financ_rateio_w,
			pr_alocacao_w;
		exit when C02%notfound;
			begin
			
			vl_rateio_w	:= round(vl_total_w * (pr_alocacao_w / 100),2);
			vl_total_rateio_w	:= vl_total_rateio_w + vl_rateio_w;
			nr_seq_classif_w	:= nr_seq_classif_w + 1;

			
			insert into titulo_receber_classif(
				nr_titulo,
				nr_sequencia,
				cd_centro_custo,
				cd_conta_financ,
				nr_seq_contrato,
				dt_atualizacao,
				nm_usuario,
				vl_classificacao,
				vl_desconto,
				vl_original,
				cd_conta_contabil)
			values(	nr_titulo_w,
				nr_seq_classif_w,
				cd_centro_rateio_w,
				cd_conta_financ_rateio_w,
				nr_seq_contrato_p,
				sysdate,
				nm_usuario_p,
				vl_rateio_w,
				0,
				0,
				cd_conta_contabil_rateio_w);
				
			end;
		end loop;
		close C02;
		
		if	(vl_total_rateio_w <> 0) then
			begin
			select	vl_titulo
			into	vl_titulo_w
			from	titulo_receber
			where	nr_titulo	= nr_titulo_w;

			update	titulo_receber_classif
			set	vl_classificacao	= vl_classificacao + vl_titulo_w - vl_total_rateio_w
			where	nr_titulo		= nr_titulo_w
			and	nr_sequencia	= nr_seq_classif_w;
			
			end;
		end if;
		
		end;
	end if;
	
	if	(qt_meses_w > 0) then
		select	PKG_DATE_UTILS.ADD_MONTH(dt_venc_atual_w,qt_meses_w,0)
		into	dt_venc_atual_w
		from	dual;
	elsif	(qt_dias_w > 0) then
		select	dt_venc_atual_w + qt_dias_w
		into	dt_venc_atual_w
		from	dual;
	end if;	

	qt_titulo_w := (qt_titulo_w - 1);
	
	end;

	if (nvl(ie_gerar_adiantamento_w,'N') = 'S') and (nvl(ie_tipo_titulo_w,'1') = '12') then
	
		select	nvl(max(cd_tipo_recebimento),0)
		into	cd_tipo_recebimento_w
		from   	tipo_recebimento
		where  	ie_tipo_consistencia = 7;
		
		if (nvl(cd_tipo_recebimento_w,0) = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(244616);
			--R+aise_application_error(-20011,'N�o foi encontrado um tipo de recebimento do tipo Adiantamento.');
		end if;
		
		select	adiantamento_seq.nextval
		into	nr_adiantamento_w
		from	dual;

		if (nvl(nr_adiantamento_w,0) <> 0 ) then
			insert into adiantamento
				(nr_adiantamento,
				cd_cgc,
				cd_estabelecimento,
				cd_moeda,
				cd_pessoa_fisica,
				cd_tipo_recebimento,
				ds_observacao,
				dt_adiantamento,
				dt_atualizacao,
				dt_baixa,
				dt_contabil,
				ie_status,
				ie_situacao,
				nm_usuario,
				nr_seq_trans_fin,
				vl_adiantamento,
				vl_especie,
				vl_saldo,
				NR_SEQ_CONTRATO)
			VALUES	(nr_adiantamento_w,
				decode(nvl(ie_considera_contratante_w,'N'),'S', cd_cgc_contratante_w, cd_cgc_contratado_w),
				cd_estabelecimento_p,
				1,
				decode(nvl(ie_considera_contratante_w,'N'),'S', cd_pessoa_contratante_w, cd_pessoa_contratado_w),
				cd_tipo_recebimento_w,
				wheb_mensagem_pck.get_texto(303031) || ' ' ||nr_seq_contrato_p||'.',
				sysdate,
				sysdate,
				null,
				dt_contabil_w,
				'D',
				'A',
				nm_usuario_p,
				nr_seq_trans_financ_w,
				vl_total_w,
				0,
				vl_total_w,
				nr_seq_contrato_p);
		end if;
	end if;

end loop;

commit;

end gerar_tit_rec_contrato;
/
