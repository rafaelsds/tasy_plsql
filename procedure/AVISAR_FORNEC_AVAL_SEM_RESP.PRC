create or replace
procedure avisar_fornec_aval_sem_resp(  	cd_estabelecimento_p 	number,
					nm_usuario_p 		varchar2) as

/* Campos Avalia��o */
nr_seq_avaliacao_w		number(10);
ds_tipo_avaliacao_w		varchar2(255);
cd_cgc_w			varchar2(14);
cd_pessoa_resp_w			varchar2(10);
nm_pessoa_resp_w			varchar2(100);
dt_limite_w			date;
ds_email_w			varchar2(255);
ds_email_remetente_w		varchar2(255);

/* Campos CI e EMAIL */
ds_email_origem_w			varchar2(60);
ds_email_destino_w			Varchar2(60);
nm_usuario_w			varchar2(15);
ie_momento_envio_w		varchar2(1);

/* Campos CI */
pessoa_envio_w			varchar(15);
nr_sequencia_w			number(10);

cd_pf_pj_w			varchar2(14);
ie_envia_email_ci_w		boolean;

/*Campos Regra Email*/
nr_seq_regra_w			number(10);
ds_email_adicional_w		varchar2(2000);
cd_perfil_disparar_w		number(5);
ds_assunto_w			varchar2(80);
ds_mensagem_w			varchar2(4000);

/*Valor para a macro*/
ds_avaliacao_tipo_w		varchar2(4000);

ds_txt_dt_limite_w		varchar2(30) := wheb_mensagem_pck.get_texto(313591);
ds_txt_responsavel_w		varchar2(30) := wheb_mensagem_pck.get_texto(241183);
ds_txt_email_w			varchar2(15) := wheb_mensagem_pck.get_texto(275291)||':';
cursor 	C01 is
	select	distinct
		a.cd_cnpj
	from    avf_resultado a
	where   a.dt_resp_portal is null
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	a.dt_liberacao is not null
	and     	exists( 	select	1
			from    	avf_tipo_avaliacao b
			where   	b.nr_sequencia = a.nr_seq_tipo_aval
			and     	nvl(b.qt_dias_resposta_aval,0) > 0
			and     	nvl(b.ie_portal,'N') = 'S'
			and		substr(obter_se_avf_tipo_lib(b.nr_sequencia),1,1) = 'S'
			and     	obter_dias_entre_datas(a.dt_avaliacao, sysdate) > b.qt_dias_resposta_aval)
	order   by	cd_cnpj;

Cursor C02 is
	select	distinct
		a.cd_pessoa_resp
	from    avf_resultado a
	where   a.dt_resp_portal is null
	and	a.dt_liberacao is not null
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and     	exists( 	select	1
			from    	avf_tipo_avaliacao b
			where   	b.nr_sequencia = a.nr_seq_tipo_aval
			and     	nvl(b.qt_dias_resposta_aval,0) > 0
			and     	nvl(b.ie_portal,'N') = 'S'
			and		substr(obter_se_avf_tipo_lib(b.nr_sequencia),1,1) = 'S'
			and     	obter_dias_entre_datas(a.dt_avaliacao, sysdate) > b.qt_dias_resposta_aval)
	order   by	cd_pessoa_resp;

Cursor C03 is
	select	distinct
		a.nr_sequencia,
		substr(obter_nome_avf_tipo_avaliacao(a.nr_seq_tipo_aval),1,250) ds_tipo_aval,
		a.dt_liberacao + nvl(b.qt_dias_resposta_aval,0),
		substr(obter_nome_pf(a.cd_pessoa_resp),1,100),
		substr(obter_dados_usuario_opcao(obter_usuario_pessoa(a.cd_pessoa_resp),'E'),1,255)
	from    avf_resultado a,
		avf_tipo_avaliacao b
	where   a.nr_seq_tipo_aval = b.nr_Sequencia
	and	a.dt_resp_portal is null
	and	a.dt_liberacao is not null
	and	a.cd_estabelecimento = cd_estabelecimento_p
	and	substr(obter_se_avf_tipo_lib(a.nr_sequencia),1,1) = 'S'
	and	((a.cd_pessoa_resp = cd_pf_pj_w) or (a.cd_cnpj = cd_pf_pj_w))
	and     	exists( 	select	1
			from    	avf_tipo_avaliacao b
			where   	b.nr_sequencia = a.nr_seq_tipo_aval
			and     	nvl(b.qt_dias_resposta_aval,0) > 0
			and     	nvl(b.ie_portal,'N') = 'S'
			and		substr(obter_se_avf_tipo_lib(b.nr_sequencia),1,1) = 'S'
			and     	obter_dias_entre_datas(a.dt_avaliacao, sysdate) > b.qt_dias_resposta_aval)
	order   by	nr_sequencia;

begin

select	nvl(max(nr_sequencia),0),
	nvl(max(ds_email_remetente),'X'),
	max(replace(ds_email_adicional,',',';')),
	nvl(max(ie_momento_envio),'I')
into	nr_seq_regra_w,
	ds_email_remetente_w,
	ds_email_adicional_w,
	ie_momento_envio_w
from	regra_envio_email_compra
where	ie_tipo_mensagem = 55 -- Pessoa jur�dica - Avalia��o sem resposta (Avisa fornecedor e respons�vel)
and	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_p;

if	(nr_seq_regra_w > 0) then
	begin

	/* Envia Email para o FORNECEDOR referente ao atraso da resposta da avalia��o */
	open C01;
	loop
	fetch C01 into
		cd_pf_pj_w;
	exit when C01%notfound;
		begin
		/* EXCEPTION */
		begin

		ie_envia_email_ci_w := false;
		ds_avaliacao_tipo_w := '';
		open C03;
		loop
		fetch C03 into	
			nr_seq_avaliacao_w,
			ds_tipo_avaliacao_w,
			dt_limite_w,
			nm_pessoa_resp_w,			
			ds_email_w;
		exit when C03%notfound;
			begin
			ds_avaliacao_tipo_w := substr(	ds_avaliacao_tipo_w || nr_seq_avaliacao_w || ' - ' || ds_tipo_avaliacao_w || chr(13) || chr(10) ||
							ds_txt_dt_limite_w|| ' '|| dt_limite_w 		|| chr(13) || chr(10) ||
							ds_txt_responsavel_w|| ' ' || nm_pessoa_resp_w 	|| chr(13) || chr(10) ||
							ds_txt_email_w|| ' '	|| ds_email_w		|| chr(13) || chr(10) || chr(13) || chr(10),1,4000);
			ie_envia_email_ci_w := true;
			end;
		end loop;
		close C03;
		

		
		select	substr(ds_assunto,1,80),
			substr(ds_mensagem_padrao,1,4000)
		into	ds_assunto_w,
			ds_mensagem_w
		from	regra_envio_email_compra
		where	nr_sequencia = nr_seq_regra_w;

		ds_mensagem_w := substr(replace_macro(ds_mensagem_w, '@fornecedor_responsavel', wheb_mensagem_pck.get_texto(312719)),1,4000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w, '@avaliacao_tipo',ds_avaliacao_tipo_w),1,4000);

		select	nvl(ds_email,'X')
		into	ds_email_destino_w
		from	pessoa_juridica_estab
		where	cd_cgc = cd_pf_pj_w
		and	cd_estabelecimento = cd_estabelecimento_p;

		select	ds_email,
			obter_usuario_pf(cd_responsavel_compras)
		into	ds_email_origem_w,
			nm_usuario_w
		from	parametro_compras
		where	cd_estabelecimento = cd_estabelecimento_p;

		if	(ds_email_remetente_w <> 'X') then
			ds_email_origem_w	:= ds_email_remetente_w;
		end if;

		if	(cd_pf_pj_w is not null) and
			(ds_email_destino_w <> 'X') and
			(ds_email_origem_w is not null) and
			--(nm_usuario_w is not null) and
			(ie_envia_email_ci_w) then
			
			if	(ie_momento_envio_w = 'A') then
				begin

				sup_grava_envio_email(
					'PJ',
					'55',
					0,
					null,
					null,
					ds_email_destino_w,
					nm_usuario_w,					
					ds_email_origem_w,
					ds_assunto_w,
					ds_mensagem_w,
					cd_estabelecimento_p,
					nm_usuario_p);
				end;
			else
				begin
				enviar_email(	
					ds_assunto_w,
					ds_mensagem_w,
					ds_email_origem_w,
					ds_email_destino_w,
					nm_usuario_w,
					'M');
				end;
			end if;
		end if;

		exception
		when others then
			null;
		end; /* EXCEPTION */

		end;
	end loop;
	close C01;

	/* Envia Email para o RESPONS�VEL da avalia�a�, referente ao atraso da resposta da mesma */
	open C02;
	loop
	fetch C02 into	
		cd_pf_pj_w;
	exit when C02%notfound;
		begin

		/* EXCEPTION */
		begin

		ie_envia_email_ci_w := false;
		ds_avaliacao_tipo_w := ''; 
		open C03;
		loop
		fetch C03 into	
			nr_seq_avaliacao_w,
			ds_tipo_avaliacao_w,
			dt_limite_w,
			nm_pessoa_resp_w,
			ds_email_w;
		exit when C03%notfound;
			begin
			ds_avaliacao_tipo_w := substr(	ds_avaliacao_tipo_w || nr_seq_avaliacao_w || ' - ' || ds_tipo_avaliacao_w || chr(13) || chr(10) ||
							ds_txt_dt_limite_w|| ' '	|| dt_limite_w 		|| chr(13) || chr(10) ||
							ds_txt_responsavel_w|| ' ' || nm_pessoa_resp_w 	|| chr(13) || chr(10) ||
							ds_txt_email_w|| ' ' || ds_email_w		|| chr(13) || chr(10) || chr(13) || chr(10),1,4000);
			
			ie_envia_email_ci_w := true;
			end;
		end loop;
		close C03;
		
				

	

		select	substr(ds_assunto,1,80),
			substr(ds_mensagem_padrao,1,4000)
		into	ds_assunto_w,
			ds_mensagem_w
		from	regra_envio_email_compra
		where	nr_sequencia = nr_seq_regra_w;

		ds_mensagem_w := substr(replace_macro(ds_mensagem_w, '@fornecedor_responsavel', wheb_mensagem_pck.get_texto(301713)),1,4000);
		ds_mensagem_w := substr(replace_macro(ds_mensagem_w, '@avaliacao_tipo', ds_avaliacao_tipo_w),1,4000);

		if	(cd_pf_pj_w is not null) and
			(ie_envia_email_ci_w) then

			select	obter_dados_usuario_opcao(obter_usuario_pessoa(cd_pf_pj_w), 'E') nm_usuario
			into	ds_email_destino_w
			from	dual;

			select	ds_email,
				obter_usuario_pf(cd_responsavel_compras) nm_usuario_origem
			into	ds_email_origem_w,
				nm_usuario_w
			from	parametro_compras
			where	cd_estabelecimento = cd_estabelecimento_p;

			if	(ds_email_remetente_w <> 'X') then
				ds_email_origem_w	:= ds_email_remetente_w;
			end if;

			if	(ds_email_destino_w <> 'X') and
				(ds_email_origem_w is not null) and
				--(nm_usuario_w is not null) and
				(ie_envia_email_ci_w) then
				begin

				if	(ie_momento_envio_w = 'A') then
					begin

					sup_grava_envio_email(
						'PJ',
						'55',
						0,
						null,
						null,
						ds_email_destino_w,
						nm_usuario_w,						
						ds_email_origem_w,
						ds_assunto_w,
						ds_mensagem_w,
						cd_estabelecimento_p,
						nm_usuario_p);

					end;
				else
					begin
					enviar_email(	ds_assunto_w,
							ds_mensagem_w,
							ds_email_origem_w,
							ds_email_destino_w,
							nm_usuario_w,
							'M');

					end;
				end if;

				end;
			end if;
		end if;

		exception
		when others then
			null;
		end; /* EXCEPTION */

		end;
	end loop;
	close C02;
	end;
end if;

commit;

end avisar_fornec_aval_sem_resp;
/