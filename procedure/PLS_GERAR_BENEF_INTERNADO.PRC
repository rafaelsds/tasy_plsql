create or replace
procedure pls_gerar_benef_internado
			(	nr_seq_conta_p		Number,
				nm_usuario_p		Varchar2) is 
			
ie_benef_internado_w		Varchar2(1)	:= 'S';
nr_seq_segurado_w		Number(10);
cd_estabelecimento_w		Number(4);
nr_seq_internacao_w		Number(10);
nr_seq_contrato_w		Number(10);
cd_guia_w			Varchar2(20);
ie_tipo_plano_w			Varchar2(3);
ie_tipo_beneficiario_w		Varchar2(3);
qt_diarias_w			Number(10)	:= 0;
nr_seq_protocolo_w		Number(10);
dt_mesref_protocolo_w		Date;
ie_tipo_guia_w			Varchar2(2);
nr_seq_clinica_w		Number(10);
ie_tipo_segurado_w		Varchar2(3);

begin

ie_benef_internado_w	:= pls_obter_se_internado(nr_seq_conta_p, 'C');

/* Obter dados da conta */
begin
select	nr_seq_segurado,
	cd_estabelecimento,
	nvl(cd_guia_referencia, cd_guia),
	ie_tipo_plano,
	ie_tipo_beneficiario,
	nr_seq_protocolo,
	ie_tipo_guia,
	nr_seq_clinica
into	nr_seq_segurado_w,
	cd_estabelecimento_w,
	cd_guia_w,
	ie_tipo_plano_w,
	ie_tipo_beneficiario_w,
	nr_seq_protocolo_w,
	ie_tipo_guia_w,
	nr_seq_clinica_w
from	pls_conta
where	nr_sequencia	= nr_seq_conta_p;
exception
	when others then
	wheb_mensagem_pck.exibir_mensagem_abort(267424,
						'NR_SEQ_CONTA_P=' || nr_seq_conta_p);
end;

if	(nr_seq_segurado_w	> 0) then
	/* Obter dados do segurado */
	begin
	select	nr_seq_contrato,
		ie_tipo_segurado
	into	nr_seq_contrato_w,
		ie_tipo_segurado_w
	from	pls_segurado
	where	nr_sequencia	= nr_seq_segurado_w;
	exception
		when others then		
		wheb_mensagem_pck.exibir_mensagem_abort(267425,
							'NR_SEQ_CONTA_P=' || nr_seq_conta_p||';'||
							'NR_SEQ_SEGURADO_W=' || nr_seq_segurado_w);
	end;
	
	if	(ie_tipo_segurado_w in ('B','A')) then
		/* Obter dados do protocolo */
		select	dt_mes_competencia
		into	dt_mesref_protocolo_w
		from	pls_protocolo_conta
		where	nr_sequencia	= nr_seq_protocolo_w;

		/* Obter a quantidade de di�rias da conta */
		select	nvl(sum(qt_procedimento),0)
		into	qt_diarias_w
		from	pls_conta_proc
		where	nr_seq_conta	= nr_seq_conta_p
		and	ie_tipo_despesa	= 3;
		
		select	nvl(max(nr_sequencia),0)
		into	nr_seq_internacao_w
		from	pls_beneficiario_internado
		where	nr_seq_segurado	= nr_seq_segurado_w
		and	cd_guia		= cd_guia_w;

		if	(nr_seq_internacao_w	= 0) then
			insert into pls_beneficiario_internado
				(nr_sequencia,
				cd_estabelecimento,
				nr_seq_segurado,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_contrato,
				cd_guia,
				ie_tipo_plano,
				ie_tipo_beneficiario,
				qt_diarias,
				nr_seq_conta,
				dt_mesref_protocolo,
				ie_tipo_guia,
				nr_seq_clinica)
			values(	pls_beneficiario_internado_seq.nextval,
				cd_estabelecimento_w,
				nr_seq_segurado_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_contrato_w,
				cd_guia_w,
				ie_tipo_plano_w,
				ie_tipo_beneficiario_w,
				qt_diarias_w,
				nr_seq_conta_p,
				dt_mesref_protocolo_w,
				ie_tipo_guia_w,
				nr_seq_clinica_w);
		else
			update	pls_beneficiario_internado
			set	nr_seq_contrato		= nr_seq_contrato_w,
				cd_guia			= cd_guia_w,
				ie_tipo_plano		= ie_tipo_plano_w,
				ie_tipo_beneficiario	= ie_tipo_beneficiario_w,
				qt_diarias		= qt_diarias + qt_diarias_w,
				nr_seq_conta		= nr_seq_conta_p,
				dt_mesref_protocolo	= dt_mesref_protocolo_w,
				ie_tipo_guia		= ie_tipo_guia_w,
				nr_seq_clinica		= nr_seq_clinica_w
			where	nr_sequencia	= nr_seq_internacao_w;
		end if;

		--commit;
	end if;
end if;

end pls_gerar_benef_internado;
/