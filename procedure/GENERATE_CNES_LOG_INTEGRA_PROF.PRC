create or replace
procedure generate_cnes_log_integra_prof (      cd_pessoa_fisica_p              varchar2,
                                                ds_detalhe_log_p	        varchar2,
                                                nm_usuario_p                    varchar2,
                                                nm_usuario_nrec_p               varchar2,
                                                nr_seq_cnes_identificacao_p     number,
                                                nr_seq_tipo_vinculo_p           number,
                                                nr_seq_vinculo_p                number,
                                                ie_opcao_p		        varchar2,
                                                cd_cpf_profissional_p           varchar2,
                                                nm_profissional_p               varchar2) is
                                             
begin

if      (ie_opcao_p = 'I') then
        begin 
        insert into cnes_log_integra_prof(      cd_pessoa_fisica,
                                                ds_detalhe_log,
                                                dt_atualizacao,
                                                dt_atualizacao_nrec,
                                                nm_usuario,
                                                nm_usuario_nrec,
                                                nr_seq_cnes_identificacao,
                                                nr_seq_tipo_vinculo,
                                                nr_sequencia,
                                                nr_seq_vinculo,
                                                cd_cpf_profissional,
                                                nm_profissional)
                                        values( cd_pessoa_fisica_p,
                                                ds_detalhe_log_p,
                                                sysdate,
                                                sysdate,
                                                nm_usuario_p,
                                                nm_usuario_nrec_p,
                                                nr_seq_cnes_identificacao_p,
                                                nr_seq_tipo_vinculo_p,
                                                cnes_log_integra_prof_seq.nextval,
                                                nr_seq_vinculo_p,
                                                cd_cpf_profissional_p,
                                                nm_profissional_p);
        commit;
        end;
elsif   (ie_opcao_p = 'D') then
        begin
        delete from cnes_log_integra_prof 
        where   nm_usuario = nm_usuario_p
        or      nm_usuario_nrec = nm_usuario_nrec_p;
        commit;
        end;
end if;

end generate_cnes_log_integra_prof;
/
