create or replace
procedure Man_Gerar_SLA_OS(	cd_estabelecimento_p	                        number,
				nr_sequencia_p			        number,
				nm_usuario_p			        varchar2,
				ie_prioridade_p			        varchar2,
				ie_classificacao_p			        varchar2,
				nr_seq_localizacao_p		        number,
				nr_seq_grupo_trab_p		        number,
				dt_ordem_servico_p		        date,
				dt_inicio_p			        date,
				nr_seq_estagio_p			        number,
				nr_seq_equipamento_p		        number,
				nr_seq_classif_p			        number,
				ie_parado_p			        varchar2) is

qt_min_inicio_w			    number(15,0);
qt_min_termino_w		                    number(15,0);
nr_seq_sla_w			    number(15,0);
nr_seq_sla_regra_w	    	                    number(15,0);
nr_sequencia_w			    number(15,0);
dt_inicio_prev_w		                    date;
dt_termino_prev_w		                    date;
qt_reg_w		                                    number(15,0);
ie_tempo_w			    varchar2(15);
nr_seq_estagio_w		                    number(10,0);
nr_seq_tipo_equip_w	                    number(10,0);
nr_seq_tipo_equip_ww		    number(10,0);
nr_seq_equipamento_w		    number(10,0);
ie_tipo_sla_w		    	    man_sla_regra.ie_tipo_sla%type;
ie_utiliza_sla_w		    varchar2(255) := obter_valor_param_usuario(299,57,wheb_usuario_pck.get_cd_perfil,nvl(nm_usuario_p,wheb_usuario_pck.get_nm_usuario),wheb_usuario_pck.get_cd_estabelecimento);

cursor C01 is
select	b.nr_seq_sla,
	b.qt_min_inicio,
	b.qt_min_termino,
	b.ie_tempo,
	b.nr_seq_estagio,
	b.nr_seq_equipamento,
	b.nr_seq_tipo_equipamento,
	b.nr_sequencia,
	b.ie_tipo_sla
from	man_sla_loc c,
	man_sla_regra b,
	man_sla a
where	a.nr_sequencia					= b.nr_seq_sla
and	a.nr_sequencia					= c.nr_seq_sla(+)
and	a.dt_vigencia					<= sysdate
and	nvl(c.nr_seq_local,nr_seq_localizacao_p)	= nr_seq_localizacao_p
and	nvl(a.nr_seq_grupo_trab, nvl(nr_seq_grupo_trab_p,0))	= nvl(nr_seq_grupo_trab_p,0)
and	b.ie_classificacao					= ie_classificacao_p
and	nvl(ie_prioridade,ie_prioridade_p)			= ie_prioridade_p
and	nvl(nr_seq_estagio, nvl(nr_seq_estagio_p,0))	 	= nvl(nr_seq_estagio_p,0)
and	nvl(nr_seq_classif, nvl(nr_seq_classif_p,0))	 	= nvl(nr_seq_classif_p,0)
and	nvl(nr_seq_equipamento, nvl(nr_seq_equipamento_p,0)) 	= nvl(nr_seq_equipamento_p,0)
and	nvl(nr_seq_tipo_equipamento, nvl(nr_seq_tipo_equip_w,0))	= nvl(nr_seq_tipo_equip_w,0)
and	nvl(b.ie_parado, nvl(ie_parado_p,'X'))			= nvl(ie_parado_p,'X')
and	qt_reg_w						= 0
and	a.ie_situacao				= 'A'
and	(c.nr_seq_local is not null or ie_utiliza_sla_w = 'L')
order by nvl(ie_prioridade,' '),
        c.nr_seq_sla,
        decode(ie_utiliza_sla_w, 'l', '', c.nr_seq_local) desc,
        a.nr_seq_grupo_trab desc,
        b.nr_seq_equipamento desc,
        b.nr_seq_estagio desc,
        b.nr_seq_classif desc,
        b.nr_seq_tipo_equipamento desc,
        b.ie_parado desc,
        c.nr_seq_local desc;

BEGIN
select	count(*)
into	qt_reg_w
from	man_ordem_serv_sla
where	nr_seq_ordem		= nr_sequencia_p;

select	nvl(max(nr_seq_tipo_equip),0)
into	nr_seq_tipo_equip_w
from	man_equipamento
where	nr_sequencia = nr_seq_equipamento_p;

nr_seq_sla_w			:= 0;
open C01;
loop
fetch C01 into	
	nr_seq_sla_w,
	qt_min_inicio_w,
	qt_min_termino_w,
	ie_tempo_w,
	nr_seq_estagio_w,
	nr_seq_equipamento_w,
	nr_seq_tipo_equip_ww,
	nr_seq_sla_regra_w,
	ie_tipo_sla_w;		
exit when C01%notfound;
	nr_seq_sla_w		:= nr_seq_sla_w;
	nr_seq_sla_regra_w	:= nr_seq_sla_regra_w;
End loop;
close C01;
if	(nvl(nr_seq_sla_w,0) > 0) then
	select	man_ordem_serv_sla_seq.nextval
	into	nr_sequencia_w	
	from	dual;
	if	(ie_tempo_w = 'COR') then
		dt_inicio_prev_w	:= dt_ordem_servico_p + (qt_min_inicio_w / 1440);
		dt_termino_prev_w	:= dt_ordem_servico_p + (qt_min_termino_w / 1440);
	else
		dt_inicio_prev_w	:= man_obter_hor_com(cd_estabelecimento_p, dt_ordem_servico_p, qt_min_inicio_w);
		dt_termino_prev_w	:= man_obter_hor_com(cd_estabelecimento_p, dt_ordem_servico_p, qt_min_termino_w);
	end if;
	
	if	(nvl(nr_seq_estagio_w,0) > 0) then
		if	(ie_tempo_w = 'COR') then
			dt_inicio_prev_w	:= sysdate + (qt_min_inicio_w / 1440);
			dt_termino_prev_w	:= sysdate + (qt_min_termino_w / 1440);
		else
			dt_inicio_prev_w	:= man_obter_hor_com(cd_estabelecimento_p, sysdate, qt_min_inicio_w);
			dt_termino_prev_w	:= man_obter_hor_com(cd_estabelecimento_p, sysdate, qt_min_termino_w);
		end if;
	end if;
	
	insert into man_ordem_serv_sla (
		nr_sequencia,
		nr_seq_ordem,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_sla,
		ie_prioridade,
		ie_classificacao,
		qt_min_inicio,
		qt_min_termino,
		dt_inicio_prev,
		dt_termino_prev,
		nr_seq_status,
		ie_tempo,
		cd_estabelecimento,
		dt_ordem,
		qt_desvio_inic,
		dt_inicio,
		nr_seq_estagio,
		nr_seq_equipamento,
		nr_seq_tipo_equipamento,
		nr_seq_sla_regra,
		ie_tipo_sla)
	values(	nr_sequencia_w,
		nr_sequencia_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_sla_w,
		ie_prioridade_p,
		ie_classificacao_p,
		qt_min_inicio_w,
		qt_min_termino_w,
		dt_inicio_prev_w,
		dt_termino_prev_w,
		415,
		ie_tempo_w,
		cd_estabelecimento_p,
		dt_ordem_servico_p,
		0,
		dt_inicio_p,
		nr_seq_estagio_w,
		nr_seq_equipamento_w,
		nr_seq_tipo_equip_ww,
		nr_seq_sla_regra_w,
		ie_tipo_sla_w);
end if;

END Man_Gerar_SLA_OS;
/