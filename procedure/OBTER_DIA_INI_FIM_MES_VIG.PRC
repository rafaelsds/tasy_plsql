create or replace
procedure  obter_dia_ini_fim_mes_vig (	dt_mes_ref_p		date,
					dt_ini_periodo_p	out date,
					dt_fim_periodo_p	out date) is
			
--vetor
type qt_semana_w is record (dia_w date);
type vetor is table of qt_semana_w index by binary_integer;
			

vetor_w		vetor;
i 		number(3)   	:=0;
--dt_ref_w	date;
dt_dia_w 	date := pkg_date_utils.start_of(dt_mes_ref_p,'MONTH',0);

begin

while (dt_dia_w <= trunc(pkg_date_utils.end_of(dt_mes_ref_p, 'MONTH', 0))) loop
	if (pkg_date_utils.get_weekday(dt_dia_w) = 1) then
		begin
		i := i +1;
		vetor_w(i).dia_w := trunc(dt_dia_w);
		end;
	end if;
	dt_dia_w:= trunc(dt_dia_w) + 1;
	end loop;
	
dt_ini_periodo_p	:= 	obter_inicio_fim_semana(trunc(vetor_w(1).dia_w),'I');
dt_fim_periodo_p	:= 	obter_inicio_fim_semana(trunc(vetor_w(i).dia_w),'F');

end obter_dia_ini_fim_mes_vig;
/ 