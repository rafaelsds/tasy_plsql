create or replace
procedure Obter_Estabelecimento_Convenio
		(cd_convenio_p		in 	number,
		cd_estab_original_p	in	number,
		cd_estab_convenio_p	out	number) is

cd_estab_retorno_w	number(04,0);

begin

select	nvl(max(a.cd_estabelecimento),0)
into	cd_estab_retorno_w
from	convenio_estabelecimento a
where	a.cd_convenio 		= cd_convenio_p
and	not exists
	(select	1
	from	convenio_estabelecimento b
	where	b.cd_convenio		= a.cd_convenio
	and	b.cd_estabelecimento	= cd_estab_original_p);

if	(cd_estab_retorno_w = 0) then
	cd_estab_retorno_w	:= cd_estab_original_p;
end if;


cd_estab_convenio_p	:= cd_estab_retorno_w;


end Obter_Estabelecimento_Convenio;
/