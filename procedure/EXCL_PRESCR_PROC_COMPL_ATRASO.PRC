create or replace
PROCEDURE EXCL_PRESCR_PROC_COMPL_ATRASO(
    nr_sequencia_p NUMBER,
    nr_prescricao_p number) IS

    nr_seq_proc_compl_p number;
BEGIN
   select nr_seq_proc_compl
     into nr_seq_proc_compl_p
     from prescr_procedimento
    where nr_prescricao = nr_prescricao_p
      and nr_sequencia = nr_sequencia_p;

   update prescr_procedimento set nr_seq_proc_compl = null
    where nr_prescricao = nr_prescricao_p
      and nr_sequencia = nr_sequencia_p;

  delete from prescr_procedimento_compl
  where nr_sequencia = nr_seq_proc_compl_p and 
        dt_result_atrasado is not null;

  COMMIT;
END EXCL_PRESCR_PROC_COMPL_ATRASO;
/