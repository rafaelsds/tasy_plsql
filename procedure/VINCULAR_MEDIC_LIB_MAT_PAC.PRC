create or replace
procedure vincular_medic_lib_mat_pac	(	nr_prescricao_p		number,
						nr_sequencia_p		number) is 

nr_sequencia_w		number(10);
						
begin

select	max(nr_sequencia)
into	nr_sequencia_w
from	lib_material_paciente
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_material	= nr_sequencia_p
and		dt_suspenso is null;

if	(nr_sequencia_w	is not null) then
	
	update	prescr_material
	set	nr_seq_lib_mat_pac	= nr_sequencia_w
	where	nr_prescricao		= nr_prescricao_p
	and	nr_sequencia		= nr_sequencia_p;
	
end if;

commit;

end vincular_medic_lib_mat_pac;
/
