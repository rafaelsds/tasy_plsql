create or replace
procedure atualizar_seq_hor_glicemia (nr_prescricao_p number) is

nr_seq_horario_w		prescr_mat_hor.nr_sequencia%type;
nr_seq_prescr_w			prescr_material.nr_sequencia%type;
dt_horario_w			prescr_mat_hor.dt_horario%type;
nr_seq_procedimento_w	prescr_material.nr_sequencia_proc%type;
nr_seq_prot_glic_w		prescr_procedimento.nr_seq_prot_glic%type;
nr_seq_hor_glic_w		prescr_proc_hor.nr_sequencia%type;

cursor c01 is
select	a.nr_sequencia,
		b.nr_sequencia,
		a.dt_horario,
		b.nr_sequencia_proc
from	prescr_mat_hor a,
		prescr_material b
where	a.nr_prescricao = b.nr_prescricao
and		a.nr_seq_material = b.nr_sequencia
and		a.nr_prescricao	= nr_prescricao_p
and		b.dt_baixa is null
and		b.dt_suspensao is null
and		a.ie_agrupador = 5
order by b.nr_sequencia_proc;

begin

	open c01;
	loop
		fetch c01 into
			nr_seq_horario_w,
			nr_seq_prescr_w,
			dt_horario_w,
			nr_seq_procedimento_w;
		exit when c01%notfound;
		
		select	max(nr_seq_prot_glic)
		into	nr_seq_prot_glic_w
		from	prescr_procedimento
		where	nr_prescricao = nr_prescricao_p
		and		nr_sequencia = nr_seq_procedimento_w;
		
		if	(nr_seq_prot_glic_w is not null) then
			
			nr_seq_hor_glic_w := null;
			
			select	max(nr_sequencia)
			into	nr_seq_hor_glic_w
			from 	prescr_proc_hor
			where	nr_prescricao = nr_prescricao_p
			and		nr_seq_procedimento = nr_seq_procedimento_w
			and		dt_horario = dt_horario_w
			and		dt_suspensao is null;
			
			if	(nr_seq_hor_glic_w is not null) then
			
				update	prescr_mat_hor
				set		nr_seq_hor_glic = nr_seq_hor_glic_w
				where	nr_sequencia = nr_seq_horario_w;
			
			end if;
			
		end if;
		
	end loop;
	close c01;

	commit;

end atualizar_seq_hor_glicemia;
/