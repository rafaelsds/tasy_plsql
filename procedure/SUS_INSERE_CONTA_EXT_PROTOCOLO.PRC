create or replace
procedure sus_insere_conta_ext_protocolo(	nr_seq_prot_orig_p	number,
						nr_seq_prot_dest_p	number,
						nr_interno_conta_p	number,
						nm_usuario_p		Varchar2) is 

nr_interno_conta_w		number(10);
nr_protocolo_w			varchar2(40) := '';

Cursor C01 is
	select	nr_interno_conta
	from	conta_paciente
	where	nr_seq_protocolo = nr_seq_prot_orig_p
	and	ie_cancelamento = 'C'
	and	dt_cancelamento is not null;
						
begin

begin
select	nr_protocolo
into	nr_protocolo_w
from	protocolo_convenio
where	nr_seq_protocolo = nr_seq_prot_dest_p;
exception
when others then
	nr_protocolo_w := '';
end;

if	(nvl(nr_seq_prot_orig_p,0) > 0) and
	(nvl(nr_protocolo_w,'X') <> 'X')then
	begin
	
	open C01;
	loop
	fetch C01 into	
		nr_interno_conta_w;
	exit when C01%notfound;
		begin
		
		update 	conta_paciente a
		set 	a.nr_seq_protocolo	= nr_seq_prot_dest_p,
			a.nr_protocolo		= nr_protocolo_w,
			a.dt_atualizacao		= sysdate,
			a.nm_usuario		= nm_usuario_p
		where 	a.nr_seq_conta_origem 	= nr_interno_conta_w
		and	a.ie_cancelamento 	= 'E'
		and	not exists (	select	1
					from	protocolo_convenio x
					where	x.nr_seq_protocolo = a.nr_seq_protocolo
					and	x.ie_status_protocolo = 2);
		
		end;
	end loop;
	close C01;
	
	end;
elsif	(nvl(nr_interno_conta_p,0) > 0) and
	(nvl(nr_protocolo_w,'X') <> 'X')then
	begin
	
	update 	conta_paciente
	set 	nr_seq_protocolo	= nr_seq_prot_dest_p,
		nr_protocolo		= nr_protocolo_w,
		dt_atualizacao 		= sysdate,
		nm_usuario 		= nm_usuario_p
	where 	nr_seq_conta_origem 	= nr_interno_conta_p
	and	ie_cancelamento 	= 'E';
	
	end;
end if;

commit;

end sus_insere_conta_ext_protocolo;
/
