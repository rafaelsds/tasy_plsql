create or replace 
procedure consiste_estornar_nota_fiscal
			(	nr_sequencia_p			number,
				ie_sem_estoque_p		varchar2,
				ie_titulo_bordero_p		varchar2,
				ds_retorno_p		out	varchar2,
				nm_usuario_p			varchar2,
				ie_estorna_nf_titulo_p		varchar2) is

ds_erro_w			varchar2(255);
qt_estoque_w			number(15,4) := 0;
cd_estabelecimento_w		number(5);
cd_material_w			number(6);
cd_material_estoque_w		number(6);
cd_material_estoque_ww		number(6);
ds_material_w			varchar2(255);
cd_local_estoque_w		number(4);
qt_item_estoque_w		number(13,4);
nr_item_nf_w			number(5);
ie_local_direto_w		number(3);
ie_consignado_w			varchar2(01);
qt_estoque_consig_w		number(15,4) := 0;
qt_estoque_result_w		number(15,4) := 0;
dt_mesano_referencia_w		date;
qt_existe_w			number(10);
cd_cgc_emitente_w		varchar2(14);
dt_atualizacao_estoque_w	date;
nr_bordero_w			number(10);
ie_tributo_gerado_w		varchar2(255);
cd_tributo_w			number(10);
ds_lista_titulos_w		varchar2(255);
ds_lista_repasses_w		varchar2(255);
ds_lista_notas_w		varchar2(255);
ds_listas_pag_prest_w		varchar2(255);
ie_permite_excl_nf_tributo_w	varchar2(1);
qt_titulo_nf_w			number(05,0);
qt_titulo_nf_lib_w		number(5);
qt_titulo_baixado_nf_w		number(05,0);
ie_entrada_saida_w		varchar2(1);
nr_titulo_w			number(10);
vl_total_baixa_w		number(15,2) := 0;
vl_alterado_w			number(15,2) := 0;
vl_saldo_titulo_w		number(15,2) := 0;
ie_estorna_nf_mes_anterior_w	varchar2(01);
cd_operacao_nf_w		number(04,0);
cd_operacao_estoque_w		number(03,0);
ie_consignado_op_estoque_w	varchar2(01);	/*dom 92*/
ie_estorna_tit_escrit_w		varchar2(01);
ie_tit_cobranca_w		varchar2(01);
ie_compl_tributo_w		varchar2(01) := 'N';
ie_material_estoque_w		varchar2(1);
ie_material_estoque_ww		varchar2(1);
nr_solic_compra_w		number(10);
ds_historico_w			varchar2(255);
ie_commit_w			varchar2(10);
nr_seq_mensalidade_w		number(10);
ie_estorna_nf_titulo_w		varchar2(1);
ie_estorna_nf_tit_liberado_w	varchar2(1);
nr_ordem_compra_w		number(10);
ie_tipo_ordem_w			varchar2(1);
qt_nota_entrada_w		number(10);
ie_tipo_nota_w			varchar2(3);
ie_estorna_transf_entrada_w	varchar2(1);
qt_dias_estorna_nf_w		number(10,0);
dt_entrada_saida_w		date;
ie_bloqueio_intpd_w		nota_fiscal.ie_bloqueio_intpd%type;
ie_devolucao_w			operacao_nota.ie_devolucao%type;
nr_adiantamento_w		adiantamento_pago.nr_adiantamento%type;
dt_cons_mes_fechado_w		date;
ds_erro_mat_est_w		varchar2(255);
ds_erro_opcao_est_w		varchar2(255);
ds_item_w			varchar2(255);
ds_erro_nf_bordero_w		varchar2(255);
ds_erro_tributo_w		varchar2(255);
ds_erro_estorno_w		varchar2(255);
ds_erro_estorno_ww		varchar2(255);
ds_erro_titulo_w		varchar2(255);
ds_erro_adiantamento_w		varchar2(255);
cd_perfil_w			perfil.cd_perfil%type;
ie_bloqueia_nf_livro_w		varchar2(1);
qt_count_w			number(10);
vl_tributo_w			titulo_receber_trib.vl_tributo%type;
ie_excluir_trib_tit_w 			varchar2(1) := 'N';
vl_titulo_w			titulo_receber.vl_titulo%type;
ie_nota_credito_w			operacao_nota.ie_nota_credito%type;
ie_consist_tit_w		varchar2(1) := 'S';
ie_bloqueia_titulo_vinculado_w		varchar2(1) := 'N';
qt_existe_titulo_receber_w		number(1);
ie_exige_desv_titulo_receb_w		operacao_nota.ie_exige_desv_titulo_receb%type;

cursor c01 is
	select	a.nr_item_nf,
		a.cd_material,
		replace(substr(obter_desc_material(a.cd_material),1,100),'-',''),
		a.cd_local_estoque,
		a.qt_item_estoque,
		b.cd_cgc_emitente,
		substr(obter_se_nota_entrada_saida(b.nr_sequencia),1,1),
		a.cd_material_estoque,
		a.ie_material_estoque,
		b.nr_ordem_compra,
		b.ie_tipo_nota
	from	nota_fiscal b,
		nota_fiscal_item a
	where	a.nr_sequencia		= b.nr_sequencia
	and	a.nr_sequencia		= nr_sequencia_p;

cursor c02 is
	select	cd_tributo
	from	nota_fiscal_trib
	where	nr_sequencia		= nr_sequencia_p;

cursor c03 is
	select	nr_titulo,
		a.vl_saldo_titulo
	from 	titulo_pagar a
	where 	a.nr_seq_nota_fiscal	= nr_sequencia_p;

cursor c04 is
	select	nr_titulo,
		a.vl_saldo_titulo
	from 	titulo_receber a
	where 	a.nr_seq_nf_saida 	= nr_sequencia_p;

cursor c05 is
	select	b.nr_titulo,
		a.nr_adiantamento
	from	titulo_pagar_adiant a,
		titulo_pagar b,
		adiantamento_pago c
	where	a.nr_titulo		= b.nr_titulo
	and	a.nr_adiantamento	= c.nr_adiantamento
	and	c.nr_seq_nf		= nr_sequencia_p;

begin


delete nota_fiscal_consist
where nr_seq_nota = nr_sequencia_p;

cd_perfil_w	:= obter_perfil_ativo;

-- Item: 
ds_item_w := wheb_mensagem_pck.get_texto(278209);
-- O material de estoque do item esta diferente do que estava definido no calculo da NF
ds_erro_mat_est_w := wheb_mensagem_pck.get_texto(298808);
-- A opcao Material estoque do cadastro do item esta diferente do que estava definido no calculo da NF
ds_erro_opcao_est_w := wheb_mensagem_pck.get_texto(298809);
--Esta nota possui titulo vinculado ao bordero
ds_erro_nf_bordero_w := wheb_mensagem_pck.get_texto(298829);
-- E necessario excluir os tributos para estornar esta nota, pois estes tributos serviram de base de calculo para outros tributos.
ds_erro_tributo_w := wheb_mensagem_pck.get_texto(298848);
-- Esta nota fiscal nao pode ser estornada. Ja possui titulo baixado ou desdobrado.
ds_erro_estorno_w := wheb_mensagem_pck.get_texto(298852);
-- Esta NF nao pode ser estornada. Ja possui titulo liberado.
ds_erro_estorno_ww := wheb_mensagem_pck.get_texto(298858);
-- Esta NF nao pode ser estornada porque esta vinculada com um adiantamento ja utilizado.
ds_erro_adiantamento_w := wheb_mensagem_pck.get_texto(298920);

select	a.dt_atualizacao_estoque,
	a.cd_estabelecimento,
	a.cd_operacao_nf,
	o.cd_operacao_estoque,
	a.ie_compl_tributo,
	a.nr_seq_mensalidade,
	a.dt_entrada_saida,
	o.ie_devolucao,
	a.ie_bloqueio_intpd,
	o.ie_exige_desv_titulo_receb
into	dt_atualizacao_estoque_w,
	cd_estabelecimento_w,
	cd_operacao_nf_w,
	cd_operacao_estoque_w,
	ie_compl_tributo_w,
	nr_seq_mensalidade_w,
	dt_entrada_saida_w,
	ie_devolucao_w,
	ie_bloqueio_intpd_w,
	ie_exige_desv_titulo_receb_w
from	operacao_nota o,
	nota_fiscal a
where	o.cd_operacao_nf	= a.cd_operacao_nf
and	a.nr_sequencia		= nr_sequencia_p;

ie_commit_w	:= 'S';

/*aaschlote 18/07/1993 - Quando for de origem de mensalidade OPS entao nao realiza commit*/
if	(nr_seq_mensalidade_w is not null) then
	ie_commit_w	:= 'N';
end if;

if	(nvl(cd_operacao_estoque_w,0) > 0) then
	select	nvl(ie_consignado,'X')
	into	ie_consignado_op_estoque_w
	from	operacao_estoque
	where	cd_operacao_estoque = cd_operacao_estoque_w;
end if;

select	max(dt_mesano_vigente)
into 	dt_mesano_referencia_w
from 	parametro_estoque
where 	cd_estabelecimento = cd_estabelecimento_w;

ie_estorna_nf_mes_anterior_w	:= nvl(obter_valor_param_usuario(40, 98, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w), 'S');
ie_estorna_tit_escrit_w		:= nvl(obter_valor_param_usuario(40, 112, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w), 'S');
ie_permite_excl_nf_tributo_w	:= nvl(obter_valor_param_usuario(0, 71, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w), 'N');
ie_estorna_nf_titulo_w		:= nvl(obter_valor_param_usuario(40, 12, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w), 'N');
ie_estorna_nf_tit_liberado_w	:= nvl(obter_valor_param_usuario(40, 408, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w), 'N');
ie_estorna_transf_entrada_w	:= nvl(obter_valor_param_usuario(40, 414, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w), 'N');
qt_dias_estorna_nf_w		:= somente_numero(nvl(obter_valor_param_usuario(40, 422, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w), '0'));
ie_bloqueia_nf_livro_w		:= nvl(obter_valor_param_usuario(40, 466, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w), 'N');
ie_excluir_trib_tit_w := nvl(Obter_Valor_Param_Usuario(40, 471, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w),'N');
ie_bloqueia_titulo_vinculado_w  := nvl(obter_valor_param_usuario(40, 506, cd_perfil_w, nm_usuario_p, cd_estabelecimento_w), 'N');
dt_cons_mes_fechado_w	:= null;

if	(ie_estorna_nf_mes_anterior_w = 'N') then
	dt_cons_mes_fechado_w	:= dt_atualizacao_estoque_w;
elsif	(ie_estorna_nf_mes_anterior_w = 'E') then
	dt_cons_mes_fechado_w	:= dt_entrada_saida_w;
end if;

if	(dt_mesano_referencia_w is not null) and
	(dt_cons_mes_fechado_w is not null) and
	(trunc(dt_cons_mes_fechado_w,'mm') < trunc(dt_mesano_referencia_w,'mm')) then
	/*(-20011,'Sem permissao para estornar uma nota calculada no mes anterior ao mes atual do estoque. Parametro [98] da nota  fiscal.');*/
	wheb_mensagem_pck.Exibir_Mensagem_Abort(186709);
end if;

if	(nvl(ie_estorna_tit_escrit_w,'S') = 'N') then
	select	nvl(substr(obter_se_tit_nf_cob_escrit(nr_sequencia_p),1,1),'N')
	into	ie_tit_cobranca_w
	from	dual;
	
	if	(ie_tit_cobranca_w = 'S') then
		/*(-20011,	'Sem permissao para estornar uma nota em que o titulo ja esteja vinculado a um pagamento/cobranca escritural.' || chr(13) || chr(10) ||'Verifique o parametro [112] da nota fiscal.');*/
		wheb_mensagem_pck.Exibir_Mensagem_Abort(186710);
	end if;	
end if;

if	(nvl(qt_dias_estorna_nf_w,0) > 0) then
	if	(trunc(dt_entrada_saida_w,'dd') < (trunc(sysdate,'dd') - qt_dias_estorna_nf_w)) then
		-- O numero de dias permitido para o estorno da nota foi ultrapassado. Parametro [422] da nota fiscal.
		wheb_mensagem_pck.Exibir_Mensagem_Abort(251074);
	end if;
end if;

if	(ie_bloqueia_nf_livro_w = 'S') then
	
	select	count(*)
	into	qt_existe_w
	from	fis_lote a,
		fis_lote_nota_fiscal b
	where	a.nr_sequencia = b.nr_seq_lote
	and	b.nr_seq_nota_fiscal = nr_sequencia_p
	and	a.ie_status_lote in ('E','F')
	and	rownum <= 1;
	
	if	(qt_existe_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(315878);
		/*Esta nota nao pode ser estornada porque esta dentro de um livro fiscal gerado.*/
	end if;

end if;

if	(nvl(ie_bloqueio_intpd_w,'N') = 'S') then
	-- 852333 -  Nota fiscal bloqueada pelo sistema externo! Favor consultar o sistema externo para desbloquea-la.
	wheb_mensagem_pck.Exibir_Mensagem_Abort(852333);
end if;	

if ((ie_bloqueia_titulo_vinculado_w = 'S') or (ie_bloqueia_titulo_vinculado_w = 'D' and ie_exige_desv_titulo_receb_w = 'S' )) then

	select  count(*)
	into  qt_existe_titulo_receber_w
		from  titulo_receber
	where nr_seq_nf_saida = nr_sequencia_p
	and   rownum = 1;

	if	(qt_existe_titulo_receber_w > 0) then
		gravar_nota_fiscal_consist(  nr_sequencia_p,wheb_mensagem_pck.get_texto(1190679),'S','I',wheb_mensagem_pck.get_texto(1190907),nm_usuario_p,'S');
	end if; 
end if;

open c01;
loop
fetch c01 into
	nr_item_nf_w,
	cd_material_w,
	ds_material_w,
	cd_local_estoque_w,
	qt_item_estoque_w,
	cd_cgc_emitente_w,
	ie_entrada_saida_w,
	cd_material_estoque_w,
	ie_material_estoque_w,
	nr_ordem_compra_w,
	ie_tipo_nota_w;
exit when c01%notfound;
	begin
	qt_estoque_result_w	:= qt_item_estoque_w;
	
	select 	nvl(nr_solic_compra,0)
	into	nr_solic_compra_w
	from 	nota_fiscal_item
	where 	nr_item_nf	= nr_item_nf_w
	and	nr_sequencia	= nr_sequencia_p;
	
	if	(nvl(cd_material_w,0) > 0) and
		(nvl(cd_material_estoque_w,0) > 0) then
		begin
		select	nvl(max(cd_material_estoque),cd_material_w)
		into	cd_material_estoque_ww
		from	material
		where	cd_material	= cd_material_w;
		
		if	(cd_material_estoque_w <> cd_material_estoque_ww) then
			ds_erro_w	:= substr(ds_erro_w  || nr_item_nf_w || '-' || 'S -' || ds_erro_mat_est_w || chr(13) || chr(10),1,255);
			
			gravar_nota_fiscal_consist(	nr_sequencia_p,
							ds_item_w || nr_item_nf_w || ' - ' || ds_erro_mat_est_w,
							'S',
							'I',
							ds_erro_mat_est_w,
							nm_usuario_p,
							ie_commit_w);
		end if;
		
		if	(ie_material_estoque_w is not null) then
			begin
			select	nvl(max(ie_material_estoque), 'N')
			into	ie_material_estoque_ww
			from	material_estab
			where	cd_material = cd_material_estoque_w
			and	cd_estabelecimento = cd_estabelecimento_w;
			
			if	(ie_material_estoque_w <> ie_material_estoque_ww) then
				ds_erro_w	:= substr(ds_erro_w  || nr_item_nf_w || '-' || 'S -' || ds_erro_opcao_est_w || chr(13) || chr(10),1,255);
				
				gravar_nota_fiscal_consist(	nr_sequencia_p,
								ds_item_w || nr_item_nf_w || ' - ' || ds_erro_opcao_est_w,
								'S',
								'I',
								ds_erro_opcao_est_w,
								nm_usuario_p,
								ie_commit_w);
				
			end if;
			end;
		end if;
		end;
	end if;

	if	(dt_atualizacao_estoque_w is not null) and
		(ie_sem_estoque_p = 'S') then
		begin
		if	(ie_entrada_saida_w = 'E') then
			begin
			select	count(*)
			into	qt_existe_w
			from	fornecedor_mat_consignado a
			where	a.cd_estabelecimento		= cd_estabelecimento_w
			and	a.cd_material			= cd_material_w
			and	a.cd_local_estoque		= cd_local_estoque_w
			and	a.cd_fornecedor			= cd_cgc_emitente_w
			and	a.dt_mesano_referencia		= dt_mesano_referencia_w;

			select	Obter_se_mat_Consignado(cd_material_w)
			into	ie_consignado_w
			from	dual;

			if	(ie_consignado_w = '0') or
				((ie_consignado_w = '2') and (qt_existe_w = 0)) and
				((ie_consignado_op_estoque_w = 'X') or (ie_consignado_op_estoque_w = '0')) then
				begin
				selecT	obter_saldo_disp_estoque(
					cd_estabelecimento_w,
					cd_material_w,
					cd_local_estoque_w,
					trunc(sysdate,'month'))
				into	qt_estoque_w
				from	dual;

				qt_estoque_result_w	:= qt_estoque_w;
				end;
			elsif	(ie_consignado_w = '1') or
				((ie_consignado_w = '2') and (qt_existe_w > 0)) and
				((ie_consignado_op_estoque_w = 'X') or (ie_consignado_op_estoque_w <> '0')) then
				begin
				select	Obter_Saldo_Estoque_Consig(
					cd_estabelecimento_w,
					cd_cgc_emitente_w,
					cd_material_w,
					cd_local_estoque_w)
				into	qt_estoque_consig_w
				from	dual;

				qt_estoque_result_w	:= qt_estoque_consig_w;
				end;
			end if;

			ie_local_direto_w	:= 0;
			
			if	(cd_local_estoque_w is not null) then
				select	count(*)
				into	ie_local_direto_w
				from	local_estoque
				where	ie_tipo_local = 8
				and	cd_estabelecimento	= cd_estabelecimento_w
				and	cd_local_estoque	= cd_local_estoque_w;
			end if;

			if	(qt_estoque_result_w < qt_item_estoque_w) and
				(ie_local_direto_w = 0) then
				ds_erro_w := substr(ds_erro_w || nr_item_nf_w || '-' || 'S -' || wheb_mensagem_pck.get_texto(298819,'DS_MATERIAL='||ds_material_w) || chr(13) || chr(10),1,255);
				
				gravar_nota_fiscal_consist(	nr_sequencia_p,
								wheb_mensagem_pck.get_texto(298824,'NR_ITEM_NF='||nr_item_nf_w),
								'S',
								'I',
								wheb_mensagem_pck.get_texto(298825),
								nm_usuario_p,
								ie_commit_w);
			end if;
			end;
		end if;
		end;
	end if;
	
	ds_historico_w := wheb_mensagem_pck.get_texto(298826,'NR_NOTA_FISCAL='||nr_sequencia_p||';DS_MATERIAL='||ds_material_w);
	
	if	(nr_solic_compra_w > 0) then
		grava_hist_solic_estorn(nm_usuario_p,
					nr_solic_compra_w,
					ds_historico_w,
					ie_commit_w);
	end if;
	
	/*Identifica se permite estornar a nota vinculada a um Bordero de pagamento*/
	if	(dt_atualizacao_estoque_w is not null) and
		(ie_titulo_bordero_p = 'N') then
		select	nvl(max(nr_bordero),0)
		into	nr_bordero_w
		from	titulo_pagar_bordero_v
		where	nr_seq_nota_fiscal  = nr_sequencia_p
		and	ie_situacao <> 'C';
		if	(nr_bordero_w > 0) then
			ds_erro_w	:= substr(nr_bordero_w  || '-' || 'S -' || ds_erro_nf_bordero_w || '.' || chr(13) || chr(10),1,255);
			
			gravar_nota_fiscal_consist(
					nr_sequencia_p,
					wheb_mensagem_pck.get_texto(298830) || ': ' || nr_bordero_w || ' - ' || ds_erro_nf_bordero_w || '.',
					'S','N',
					ds_erro_nf_bordero_w || '.',
					nm_usuario_p,ie_commit_w);
					
		end if;
	end if;
		
	end;
end loop;
close c01;

ie_tributo_gerado_w		:= 'N';


open c02;
loop
fetch c02 into
	cd_tributo_w;
exit when c02%notfound;

	obter_tributos_posteriores(	null,
					nr_sequencia_p,
					null,
					null,
					cd_tributo_w,
					ds_lista_titulos_w,
					ds_lista_repasses_w,
					ds_lista_notas_w,
					ds_listas_pag_prest_w,
					'N');

	if	(ds_lista_titulos_w is not null) or
		(ds_lista_repasses_w is not null) or
		(ds_lista_notas_w is not null) then
		ie_tributo_gerado_w	:= 'S';
	end if;

end loop;
close c02;

if	(ie_tributo_gerado_w = 'S') and
	(ie_permite_excl_nf_tributo_w = 'N') then
	ds_erro_w	:= substr(ds_erro_w  || '-' || 'S -' || ds_erro_tributo_w || chr(13) || chr(10),1,255);
	
	gravar_nota_fiscal_consist(	nr_sequencia_p,
					ds_erro_tributo_w,
					'S',
					'N',
					ds_erro_tributo_w,
					nm_usuario_p,
					ie_commit_w);		
end if;

select	count(*)
into	qt_titulo_nf_w
from	titulo_pagar_v
where	nr_seq_nota_fiscal	= nr_sequencia_p;

select	count(*)
into	qt_titulo_baixado_nf_w
from	titulo_pagar_v
where	nr_seq_nota_fiscal	= nr_sequencia_p
and	ie_situacao not in ('A','C');

select	count(*)
into	qt_titulo_nf_lib_w
from	titulo_pagar_v
where	nr_seq_nota_fiscal	= nr_sequencia_p
and	dt_liberacao is not null;

if	(qt_titulo_nf_w > 0) and
	(qt_titulo_baixado_nf_w > 0) and
	(nvl(ie_estorna_nf_titulo_w ,'S') = 'N') then
	ds_erro_w	:= substr(ds_erro_w  || '-' || 'S -' || ds_erro_estorno_w || chr(13) || chr(10),1,255);
				
	gravar_nota_fiscal_consist(	nr_sequencia_p,
					ds_erro_estorno_w,
					'S',
					'N',
					ds_erro_estorno_w,
					nm_usuario_p,
					ie_commit_w);
end if;

if	(nvl(ie_estorna_nf_titulo_w ,'S') = 'N') then
	open c03;
	loop
	fetch c03 into
		nr_titulo_w,
		vl_saldo_titulo_w;
	exit when c03%notfound;
		ds_erro_titulo_w := wheb_mensagem_pck.get_texto(298853,'NR_TITULO='||nr_titulo_w);
		if	(obter_dados_tit_pagar(nr_titulo_w, 'VT') <> vl_saldo_titulo_w)  then
			ds_erro_w	:=  substr(ds_erro_w || '-' || 'S -' || ds_erro_titulo_w || chr(13) || chr(10),1,255);
	
			gravar_nota_fiscal_consist(	nr_sequencia_p,
							ds_erro_titulo_w,
							'S',
							'N',
							ds_erro_titulo_w,
							nm_usuario_p,
							ie_commit_w);
		end if;
	end loop;
	close c03;
end if;

if	(qt_titulo_nf_w > 0) and
	(qt_titulo_nf_lib_w > 0) and
	(nvl(ie_estorna_nf_tit_liberado_w,'S') = 'S') then
	ds_erro_w	:= substr(ds_erro_w  || '-' || 'S -' || ds_erro_estorno_ww || chr(13) || chr(10),1,255);
				
	gravar_nota_fiscal_consist(	nr_sequencia_p,
					ds_erro_estorno_ww,
					'S',
					'N',
					wheb_mensagem_pck.get_texto(298858),
					nm_usuario_p,
					ie_commit_w);
end if;

open c04;
loop
fetch c04 into
	nr_titulo_w,
	vl_saldo_titulo_w;
exit when c04%notfound;
	ds_erro_titulo_w := wheb_mensagem_pck.get_texto(298853,'NR_TITULO='||nr_titulo_w);

	/*OS 1092810 - Se for nota de credito nao pode restringir pro mexico, porque tem estorno do abatimento.*/
	select	nvl(max(ie_nota_credito),'N')
	into	ie_nota_credito_w
	from	operacao_nota o,
			nota_fiscal a
	where	a.nr_sequencia 		= nr_sequencia_p
	and		a.cd_operacao_nf 	= o.cd_operacao_nf;
	
	/*Se for no mexico e for nota de credito nao pode consistir*/
	if ((philips_param_pck.get_cd_pais = 2) and (nvl(ie_nota_credito_w,'N') = 'S')) then
		ie_consist_tit_w := 'N';
	else
		ie_consist_tit_w := 'S';
	end if;	
	
	if ((obter_dados_titulo_receber(nr_titulo_w, 'V') <> vl_saldo_titulo_w) and (ie_excluir_trib_tit_w = 'S')) and (nvl(ie_consist_tit_w,'S') = 'S')  then
	
		select 	count(*)
		into 	qt_count_w
		from   	titulo_receber_liq
		where 	nr_titulo = nr_titulo_w;
		
		if (nvl(qt_count_w, 0) = 0) then		
		
			select	nvl(sum(a.vl_tributo),0)
			into 	vl_tributo_w
			from	titulo_receber_trib a
			where	a.nr_titulo = nr_titulo_w
			and     nvl(a.ie_origem_tributo, 'C') 	= 'CD'
			and 	not exists ( select 1
								 from   titulo_receber_trib_baixa x
								 where  x.nr_seq_tit_trib = a.nr_sequencia);
							
			select	max(a.vl_titulo)
			into 	vl_titulo_w
			from	titulo_receber a
			where	a.nr_titulo = nr_titulo_w;		
							
			if ((vl_saldo_titulo_w + vl_tributo_w) = vl_titulo_w) then				
				delete	titulo_receber_trib a
				where	a.nr_titulo = nr_titulo_w
				and		nvl(a.ie_origem_tributo, 'C') 	= 'CD'
				and 	not exists ( select 1 
									 from	titulo_receber_trib_baixa x
									 where	x.nr_seq_tit_trib = a.nr_sequencia);
				/*OS 1288102 - Se excluir o trbuto que interfere no saldo do titulo, precisa atualizar  o saldo do titulo*/
				Atualizar_Saldo_Tit_Rec(nr_titulo_w, nm_usuario_p);							
			end if;
			
			select	nvl(sum(a.vl_tributo),0)
			into 	vl_tributo_w
			from	titulo_receber_trib a
			where	a.nr_titulo = nr_titulo_w
			and     nvl(a.ie_origem_tributo, 'C') 	= 'CD'
			and 	not exists ( select 1
							from   titulo_receber_trib_baixa x
							where  x.nr_seq_tit_trib = a.nr_sequencia);			
		
			if ((vl_saldo_titulo_w + vl_tributo_w) <> vl_titulo_w) then
				ds_erro_w	:=  substr(ds_erro_w || '-' || 'S -' || ds_erro_titulo_w || chr(13) || chr(10),1,255);
				
				gravar_nota_fiscal_consist(	nr_sequencia_p,
						ds_erro_titulo_w,
						'S',
						'N',
						ds_erro_titulo_w,
						nm_usuario_p,
						ie_commit_w);
			end if;
			
		end if;	
	end if;
	
	if	((obter_dados_titulo_receber(nr_titulo_w, 'V') <> vl_saldo_titulo_w) and (ie_excluir_trib_tit_w = 'N')) and (nvl(ie_consist_tit_w,'S') = 'S') then
		ds_erro_w	:=  substr(ds_erro_w || '-' || 'S -' || ds_erro_titulo_w || chr(13) || chr(10),1,255);
		
		gravar_nota_fiscal_consist(	nr_sequencia_p,
						ds_erro_titulo_w,
						'S',
						'N',
						ds_erro_titulo_w,
						nm_usuario_p,
						ie_commit_w);
	end if;
end loop;
close c04;

if	(ie_estorna_transf_entrada_w = 'N') and
	(ie_tipo_nota_w = 'ST') and
	(nr_ordem_compra_w is not null) then
	select	max(ie_tipo_ordem)
	into	ie_tipo_ordem_w
	from	ordem_compra
	where	nr_ordem_compra = nr_ordem_compra_w;

	if (dt_atualizacao_estoque_w is not null) then
		if	(ie_tipo_ordem_w = 'T') then
			select	count(*)
			into	qt_nota_entrada_w
			from	nota_fiscal
			where	nr_ordem_compra	= nr_ordem_compra_w
			and	ie_tipo_nota	= 'EN'
			and	dt_cancelamento is null
			and	dt_atualizacao_estoque is not null
			and	ie_situacao	= '1'
			and	cd_estabelecimento <> cd_estabelecimento_w
			and nr_nota_fiscal	= (select nr_nota_fiscal from nota_fiscal where nr_sequencia = nr_sequencia_p and rownum <= 1);
			
			if	(qt_nota_entrada_w > 0) then
				ds_erro_w := substr(ds_erro_w || '-' || 'S -' || wheb_mensagem_pck.get_texto(298905) || chr(13) || chr(10),1,255);
				
				gravar_nota_fiscal_consist(	nr_sequencia_p,
								wheb_mensagem_pck.get_texto(298913),
								'S',
								'N',
								wheb_mensagem_pck.get_texto(1038304),
								nm_usuario_p,
								ie_commit_w);
			end if;
		end if;
	end if;	
end if;

if	(ie_devolucao_w = 'S') then
	select	count(*)
	into	qt_existe_w
	from	adiantamento_pago
	where	nr_seq_nf	= nr_sequencia_p;
	
	if	(qt_existe_w > 0) then
		open C05;
		loop
		fetch C05 into	
			nr_titulo_w,
			nr_adiantamento_w;
		exit when C05%notfound;
			begin
			ds_erro_w :=  substr(ds_erro_w || '-' || 'S -' || ds_erro_adiantamento_w || chr(13) || chr(10),1,255);
			
			gravar_nota_fiscal_consist(	nr_sequencia_p,
							ds_erro_adiantamento_w,
							'S',
							'N',
							wheb_mensagem_pck.get_texto(298927,'NR_ADIANTAMENTO='||nr_adiantamento_w||';NR_TITULO='||nr_titulo_w),
							nm_usuario_p,
							ie_commit_w);
			end;
		end loop;
		close C05;
	end if;
end if;

ds_retorno_p	:= ds_erro_w;

end consiste_estornar_nota_fiscal;
/
