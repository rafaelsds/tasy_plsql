create or replace
procedure copiar_indicador_gestao_perfil
			(cd_perfil_origem_p			number,
			cd_perfil_destino_p			number,
			nr_seq_indicador_p			number,
			nm_usuario_p				varchar2) as

nr_seq_indicador_w	number(10);
nr_seq_apresent_w	number(10);	

begin

select	nr_seq_indicador,
	nr_seq_apresent
into	nr_seq_indicador_w,
	nr_seq_apresent_w
from	indicador_gestao_perfil
where	cd_perfil = cd_perfil_origem_p
and	nr_seq_indicador = nr_seq_indicador_p;


insert	into indicador_gestao_perfil
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_perfil,
		nr_seq_indicador,
		nr_seq_apresent)
	values	(indicador_gestao_perfil_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_perfil_destino_p,
		nr_seq_indicador_w,
		nr_seq_apresent_w);

commit;

end copiar_indicador_gestao_perfil;
/
