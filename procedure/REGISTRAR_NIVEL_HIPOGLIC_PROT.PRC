create or replace 
procedure registrar_nivel_hipoglic_prot	(	nr_atendimento_p		number,
						nr_prescricao_p			number,
						nr_seq_proced_p			number,
						nr_seq_horario_p		number,
						nr_seq_glic_atend_p		number,
						nr_seq_protocolo_p		number,
						dt_glicemia_p			date,
						qt_glicemia_p			number,
						nm_usuario_p			varchar2,
						ie_aprazar_p			varchar2,
						ie_extrapol_p			varchar2,
						nr_seq_pergunta_p		number,
						ie_resposta_p			varchar2,
						ds_parametros_p			varchar2,						
						nr_seq_glicemia_p 	out	number,
						ie_medic_suplementar_p	out	varchar2,
						ds_orientacao_item_p	out	varchar2) is

nr_seq_anterior_w		number(10,0);
nr_sequencia_w			number(10,0);
nr_seq_proced_w			number(6);
nr_prescr_proced_w		number(14);
nr_seq_nivel_w			number(10,0);
nr_seq_proc_interno_w		number(10,0);
ie_inconsistencia_ccg_w		varchar2(20);
ds_inconsistentes_ccg_w		varchar2(20);
ie_manutencao_w			varchar2(20);
ie_acm_sn_w			varchar2(20);
nr_glicemia_atend_w		number(10,0);
qt_procedimento_w		number(8,3);
nr_glicemia_w			number(10,0);
nr_seq_assinatura_w		number(10,0);
dt_glicemia_w			date;
dt_proximo_controle_w		date;
dt_prev_controle_w		date;
dt_prev_ant_w			date;
dt_controle_ant_w		date;
dt_atualizacao_w		date		:= sysdate;
qt_glicemia_atual_w		number(5,0);
qt_glicemia_atual_mmol_w	number(11,6);
qt_glicemia_mg_w		number(5,0);
qt_glicemia_mmol_w	number(11,6) := 0;
qt_ui_insulina_atual_w		number(5,1)	:= 0;
qt_ui_insulina_int_atual_w	number(10,2)	:= 0;
qt_glicose_w			number(5,1);
qt_glicose_adm_w		atendimento_glicemia.qt_glicose_adm%type;
qt_var_glicemia_w		number(5,0)	:= 0;
qt_min_variacao_w		number(10,0)	:= 0;
qt_ui_insulina_calc_w		number(5,1);
qt_ui_insulina_int_calc_w	number(10,2);
ds_sugestao_w			varchar2(2000)	:= '';
ds_observacao_w			varchar2(255)	:= '';
ie_status_glicemia_w		varchar2(1);
ie_gera_adm_w			varchar2(1);
qt_min_ccg_w			number(10);

cd_intervalo_w			varchar2(7);
cd_interv_ccg_w			varchar2(7);
qt_min_interv_w			number(4,0);
qt_min_glic_w			number(6,0);
nr_seq_glicemia_w		number(10,0);
cd_material_w			number(6,0);
ie_inconsistencia_w		varchar2(255);
ds_inconsistentes_w		varchar2(255);
dt_horario_w			date;
dt_horario_ccg_w		date;
ie_glicose_w			varchar2(5);
qt_glicemia_w			Number(5,1);
ie_prot_livre_w			Varchar2(1);
cd_procedimento_w		Number(15);
Ie_registrar_sv_w		Varchar2(1);
nr_seq_prot_glic_w		Number(10);
qt_min_pergunta_w		Number(10);
ds_orientacao_pergunta_w	Varchar2(2000);
ie_via_aplicacao_w		Varchar2(5);
ds_orientacao_item_w	Varchar2(255);
qt_minutos_apraz_w		number(3);
qt_min_apraz_item_w		number(3);
ie_gera_hor_assoc_w		varchar(1);
ie_unidade_glicemia_estab_w 	varchar2(20);

Cursor C01 is
	select	cd_material,
			nr_sequencia,
			cd_intervalo
	from	prescr_material a
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia_proc = nr_seq_proced_p
	and		nvl(ie_glicemia, 'N') in (ie_glicose_w)
	and		nvl(nr_seq_pergunta_p,0) = 0
	and 	nvl(ie_suspenso, 'N') = 'N';

Cursor C02 is /*Sistema de hipoglicemia (Perguntas e Respostas)*/
	select	a.cd_material,
			a.ie_via_aplicacao,
			a.qt_dose,
			a.ds_orientacao			
	from 	prot_glic_nivel_assoc a
	where	a.nr_seq_registro = nr_seq_pergunta_p
	and		((a.ie_resposta = 'A') or (a.ie_resposta = ie_resposta_p))
	and		(((nvl(a.qt_glic_inic,0) = 0) and (nvl(a.qt_glic_fim,0) = 0)) or
			 (qt_glicemia_p between nvl(a.qt_glic_inic,0) and nvl(a.qt_glic_fim,0)))
	and		exists (select	1
					from	prescr_material x
					where	nr_prescricao = nr_prescricao_p
					and		nr_sequencia_proc = nr_seq_proced_p
					and		x.cd_material = a.cd_material
					and		nvl(ie_suspenso,'N') = 'N');

begin

obter_param_usuario(1113, 191, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gera_hor_assoc_w);
obter_param_usuario(1113, 360, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gera_adm_w);
obter_param_usuario(1113, 478, wheb_usuario_pck.get_cd_perfil, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, Ie_registrar_sv_w);

if	(nr_atendimento_p is not null) and
	(nr_seq_protocolo_p is not null) and
	(qt_glicemia_p is not null) and
	(nm_usuario_p is not null) then
	
	ie_unidade_glicemia_estab_w := Obter_Param_Medico(wheb_usuario_pck.get_cd_estabelecimento, 'IE_UNIDADE_GLICEMIA');
	
	/* obter parametros */
	select	nvl(max(obter_valor_param_usuario(1113, 21, obter_perfil_ativo, nm_usuario_p, obter_estab_atend(nr_atendimento_p))), 10)
	into	qt_min_glic_w
	from	dual;

	/* validar data glicemia */
	if	(dt_glicemia_p is not null) then
		dt_glicemia_w := dt_glicemia_p;
	else
		dt_glicemia_w := dt_atualizacao_w;
	end if;
	
	if	(nr_seq_pergunta_p > 0) then
		select	max(qt_minutos_apraz),
				max(ds_orientacao)
		into	qt_min_pergunta_w,
				ds_orientacao_pergunta_w
		from	prot_glic_nivel_questao
		where	nr_sequencia	= nr_seq_pergunta_p;
	end if;

	/* consistir data medi��o glicemia */
	consistir_data_medicao_glic(nr_atendimento_p, nvl(nr_seq_glic_atend_p,0), nr_seq_protocolo_p, dt_glicemia_w);

	/* obter registros cadastrados */
	select	nvl(max(nr_sequencia),0)
	into	nr_seq_anterior_w
	from	atendimento_glicemia
	where	nr_atendimento = nr_atendimento_p
	and	nvl(nr_seq_glicemia,0) = nvl(nr_seq_glic_atend_p,nr_seq_glicemia)
	and	nr_seq_protocolo = nr_seq_protocolo_p
	and	ie_status_glicemia not in ('T', 'V');

	/* obter glicemia atendimento */
	if	(nr_seq_anterior_w = 0) then
		select	nvl(max(nr_glicemia_atend),0) + 1
		into	nr_glicemia_atend_w
		from	atendimento_glicemia
		where	nr_atendimento = nr_atendimento_p
		and	nvl(nr_seq_glicemia,0) = nvl(nr_seq_glic_atend_p,nr_seq_glicemia)
		and	nr_seq_protocolo = nr_seq_protocolo_p
		and     ie_status_glicemia <> 'V';
	else
		select	max(nr_glicemia_atend)
		into	nr_glicemia_atend_w
		from	atendimento_glicemia
		where	nr_atendimento = nr_atendimento_p
		and	nvl(nr_seq_glicemia,0) = nvl(nr_seq_glic_atend_p,nr_seq_glicemia)
		and	nr_seq_protocolo = nr_seq_protocolo_p
		and	ie_status_glicemia not in ('T', 'V');
	end if;

	/* obter glicemia */
	select	nvl(max(nr_glicemia),0) + 1
	into	nr_glicemia_w
	from	atendimento_glicemia
	where	nr_atendimento = nr_atendimento_p
	and	nvl(nr_seq_glicemia,0) = nvl(nr_seq_glic_atend_p,nr_seq_glicemia)
	and	nr_seq_protocolo = nr_seq_protocolo_p
	and	ie_status_glicemia not in('T', 'V');
	
	/* calcular e gerar valores */
	if	(nr_seq_anterior_w = 0) then
		/* obter nivel protocolo */
		if	(nvl(nr_prescricao_p,0) = 0) and
			(nvl(nr_seq_proced_p,0) = 0) and
			(nvl(nr_seq_glic_atend_p,0) = 0) then
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_nivel_w
			from	pep_prot_glic_nivel
			where	nr_seq_protocolo = nr_seq_protocolo_p
			and		qt_glicemia_p between qt_glic_inic and qt_glic_fim
			and		((ie_glic_extrapol is null) or (ie_glic_extrapol = ie_extrapol_p));
			
			if	(nr_seq_nivel_w = 0) then
				select	nvl(max(nr_sequencia),0),
					nvl(max('S'),'N')
				into	nr_seq_nivel_w,
					ie_manutencao_w
				from	prot_glic_manutencao
				where	nr_seq_protocolo = nr_seq_protocolo_p
				and	qt_glicemia_p between qt_glic_inic and qt_glic_fim
				and	((ie_glic_extrapol is null) or (ie_glic_extrapol = ie_extrapol_p));
			end if;

		elsif	(nvl(nr_prescricao_p,0) > 0) and
			(nvl(nr_seq_proced_p,0) > 0) and
			(nvl(nr_seq_glic_atend_p,0) > 0) then
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_nivel_w
			from	prescr_proc_glic
			where	nr_prescricao = nr_prescricao_p
			and	nr_seq_procedimento = nr_seq_proced_p
			and	nr_seq_protocolo = nr_seq_protocolo_p
			and	qt_glicemia_p between qt_glic_inic and qt_glic_fim;
			
			if	(nr_seq_nivel_w = 0) then
				select	nvl(max(nr_sequencia),0),
					nvl(max('S'),'N')
				into	nr_seq_nivel_w,
					ie_manutencao_w
				from	prot_glic_manutencao
				where	nr_seq_protocolo = nr_seq_protocolo_p
				and	qt_glicemia_p between qt_glic_inic and qt_glic_fim
				and	((ie_glic_extrapol is null) or (ie_glic_extrapol = ie_extrapol_p));
			end if;

		else
			--N�o foi poss�vel realizar a busca dos n�veis de glicemia!
			Wheb_mensagem_pck.exibir_mensagem_abort(174243);
		end if;
		
		if	(nr_seq_nivel_w > 0) then
			/* gerar glicemia atendimento */
			if	(nvl(nr_seq_glic_atend_p,0) = 0) then
				gerar_atend_glicemia(nr_atendimento_p,null,null,nr_seq_protocolo_p,nm_usuario_p,nr_seq_glicemia_w);

				/* atualizar glicemia atendimento */
				if	(nvl(nr_seq_glicemia_w,0) > 0) then
					update	atend_glicemia
					set	ie_status_glic	= 'N',
						dt_inicio_glic	= sysdate,
						cd_pf_inic_glic	= substr(obter_dados_usuario_opcao(nm_usuario_p,'C'),1,10)
					where	nr_sequencia		= nr_seq_glicemia_w;
				end if;
			else
				nr_seq_glicemia_w := nr_seq_glic_atend_p;
			end if;
			
			/* obter valores protocolo */
			if	(nvl(nr_prescricao_p,0) = 0) and
				(nvl(nr_seq_proced_p,0) = 0) and
				(nvl(nr_seq_glic_atend_p,0) = 0) then
				select	qt_ui_insulina,
						qt_glicose,
						nvl(ds_orientacao_pergunta_w, ds_sugestao),
						nvl(qt_min_pergunta_w,qt_minutos_medicao),
					qt_ui_insulina_int	
				into	qt_ui_insulina_calc_w,
						qt_glicose_w,
						ds_sugestao_w,
						qt_min_ccg_w,
					qt_ui_insulina_int_calc_w
				from	pep_prot_glic_nivel
				where	nr_sequencia = nr_seq_nivel_w;
				
				if	(ie_manutencao_w = 'S') then
					select	qt_insulina_bolus,
							qt_glicose,
							nvl(ds_orientacao_pergunta_w, ds_sugestao)
					into	qt_ui_insulina_calc_w,
							qt_glicose_w,
							ds_sugestao_w
					from	prot_glic_manutencao
					where	nr_sequencia = nr_seq_nivel_w;
				end if;

			elsif	(nvl(nr_prescricao_p,0) > 0) and
				(nvl(nr_seq_proced_p,0) > 0) and
				(nvl(nr_seq_glic_atend_p,0) > 0) then
				select	max(qt_ui_insulina),
					max(qt_glicose),
					max(nvl(ds_orientacao_pergunta_w, ds_sugestao)),
					max(nvl(qt_min_pergunta_w,qt_minutos_medicao)),
					max(qt_ui_insulina_int)
				into	qt_ui_insulina_calc_w,
					qt_glicose_w,
					ds_sugestao_w,
					qt_min_ccg_w,
					qt_ui_insulina_int_calc_w
				from	prescr_proc_glic
				where	nr_sequencia = nr_seq_nivel_w;
				
				if	(nvl(qt_min_ccg_w,0) = 0) then
					select	nvl(max(nr_sequencia),0)
					into	nr_seq_nivel_w
					from	pep_prot_glic_nivel
					where	nr_seq_protocolo = nr_seq_protocolo_p
					and	qt_glicemia_p between qt_glic_inic and qt_glic_fim;
					
					select	max(nvl(qt_min_pergunta_w,qt_minutos_medicao))
					into	qt_min_ccg_w
					from	pep_prot_glic_nivel
					where	nr_sequencia = nr_seq_nivel_w;
				end if;
				
				if	(nr_seq_nivel_w = 0) then
					select	nvl(max(nr_sequencia),0),
						nvl(max('S'),'N')
					into	nr_seq_nivel_w,
						ie_manutencao_w
					from	prot_glic_manutencao
					where	nr_seq_protocolo = nr_seq_protocolo_p
					and	qt_glicemia_p between qt_glic_inic and qt_glic_fim
					and	((ie_glic_extrapol is null) or (ie_glic_extrapol = ie_extrapol_p));
				end if;
				
				if	(ie_manutencao_w = 'S') then
					select	qt_insulina_bolus,
							qt_glicose,
							nvl(ds_orientacao_pergunta_w, ds_sugestao)
					into	qt_ui_insulina_calc_w,
						qt_glicose_w,
						ds_sugestao_w
					from	prot_glic_manutencao
					where	nr_sequencia = nr_seq_nivel_w;
				end if;
			end if;
			if	(nr_seq_horario_p > 0) then
				select	max(a.nr_sequencia),
					max(a.cd_procedimento), 
					max(a.cd_intervalo),
					max(a.qt_procedimento),
					max(a.nr_seq_proc_interno),
					max(a.nr_prescricao),
					max(decode(a.ie_acm,'N',decode(a.ie_se_necessario,'S','S','N'),a.ie_acm)),
					max(a.nr_seq_prot_glic)
				into	nr_seq_proced_w,
					cd_procedimento_w,
					cd_interv_ccg_w,
					qt_procedimento_w,
					nr_seq_proc_interno_w,
					nr_prescr_proced_w,
					ie_acm_sn_w,
					nr_seq_prot_glic_w
				from	prescr_procedimento a,
					prescr_proc_hor b
				where	a.nr_sequencia = b.nr_seq_procedimento
				and	a.nr_prescricao = b.nr_prescricao
				and	b.nr_sequencia = nr_seq_horario_p;
				
				select	max(b.dt_horario)
				into	dt_horario_ccg_w
				from	prescr_proc_hor b
				where	nr_sequencia 	= nr_seq_horario_p;
				
			else
				select	max(a.nr_sequencia),
					max(a.cd_procedimento), 
					max(a.cd_intervalo),
					max(a.qt_procedimento),
					max(a.nr_prescricao),
					max(a.nr_seq_proc_interno),
					max(decode(a.ie_acm,'N',decode(a.ie_se_necessario,'S','S','N'),a.ie_acm)),
					max(a.nr_seq_prot_glic)
				into	nr_seq_proced_w,
					cd_procedimento_w,
					cd_interv_ccg_w,
					qt_procedimento_w,
					nr_prescr_proced_w,
					nr_seq_proc_interno_w,
					ie_acm_sn_w,
					nr_seq_prot_glic_w
				from	prescr_procedimento a
				where	a.nr_prescricao	= nr_prescricao_p
				and	a.nr_sequencia	= nr_seq_proced_p;
				
				select	max(b.dt_horario)
				into	dt_horario_ccg_w
				from	prescr_proc_hor b
				where	b.nr_seq_procedimento 	= nr_seq_proced_w
				and	b.nr_prescricao		= nr_prescr_proced_w;	
				
			end if;
			
			-- OS690242
			if 	(nvl(qt_glicose_w,0) > 0) then			
				ie_glicose_w := 'S';
				qt_glicemia_w := qt_glicose_w;			
			elsif 	(nvl(qt_ui_insulina_calc_w,0) > 0 ) then			
				ie_glicose_w := 'N';
				qt_glicemia_w := qt_ui_insulina_calc_w;		
			elsif 	(nvl(qt_ui_insulina_int_calc_w,0) > 0 ) then			
				ie_glicose_w := 'I';
				qt_glicemia_w := qt_ui_insulina_int_calc_w;
			end if;	

			if	(ie_gera_hor_assoc_w	= 'N') and
				(nvl(nr_seq_pergunta_p,0)	> 0) then
				select	nvl(max(x.qt_minutos_apraz),0)
				into	qt_minutos_apraz_w
				from	prot_glic_nivel_assoc x,
					prescr_material a
				where	x.cd_material	= a.cd_material
				and	a.nr_prescricao = nr_prescricao_p
				and	a.nr_sequencia_proc = nr_seq_proced_p
				--and	nvl(a.ie_glicemia, 'N') in (ie_glicose_w)
				and	x.nr_seq_registro = nr_seq_pergunta_p
				and 	nvl(a.ie_suspenso, 'N') = 'N';

			else
				qt_minutos_apraz_w	:= 0;
			end if;	
			--Fim OS690242	
			
			if	(nvl(qt_min_ccg_w,0) > 0) and
				(qt_minutos_apraz_w	= 0) then
				Aprazar_item_prescr('N', 
				obter_estab_atend(nr_atendimento_p), 
				nr_atendimento_p, 
				'G', 
				0, 
				cd_procedimento_w, 
				cd_interv_ccg_w, 
				qt_procedimento_w, 
				(dt_horario_ccg_w + (qt_min_ccg_w/1440)), 
				to_char(nr_prescricao_p),  
				nr_prescricao_p, 
				nr_seq_proced_w, 
				'N', 
				null, 
				obter_desc_expressao(346946), 
				nm_usuario_p, 
				ie_inconsistencia_ccg_w, 
				ds_inconsistentes_ccg_w, 
				nr_seq_proc_interno_w, 
				null, 
				ie_acm_sn_w,
				null,
				null,
				'S',
				nr_seq_horario_p,
				nr_seq_prot_glic_w, 
				null, 
				nr_seq_pergunta_p, 
				'',
				null,
				null);
				
			end if;
			
			/* Apraza o item associado a glicemia quando necess�rio */ 
			if	(nvl(ie_aprazar_p, 'N') = 'S') and
				(ie_gera_hor_assoc_w	= 'N') and			
				(nvl(nr_seq_pergunta_p,0) = 0) and
				((nvl(qt_ui_insulina_calc_w,0) > 0) or
				 (nvl(qt_ui_insulina_int_calc_w,0) > 0) or
				 (nvl(qt_glicose_w,0) > 0 )) then
				 
				
				if 	(nvl(qt_glicose_w,0) > 0) then			
					ie_glicose_w := 'S';
					qt_glicemia_w := qt_glicose_w;			
				elsif 	(nvl(qt_ui_insulina_calc_w,0) > 0 ) then			
					ie_glicose_w := 'N';
					qt_glicemia_w := qt_ui_insulina_calc_w;		
				elsif 	(nvl(qt_ui_insulina_int_calc_w,0) > 0 ) then			
					ie_glicose_w := 'I';
					qt_glicemia_w := qt_ui_insulina_int_calc_w;
				end if;	
				
				
				open C01;
				loop
				fetch C01 into	
					cd_material_w,
					nr_sequencia_w,
					cd_intervalo_w;
				exit when C01%notfound;
					select   nvl(nr_seq_prot_glic,0)
					into     nr_seq_prot_glic_w
					from     prescr_procedimento
					where    nr_prescricao =  nr_prescricao_p
					and      nr_sequencia  = nr_seq_proced_p;
					
					aprazar_item_prescr('N', 
					obter_estab_atend(nr_atendimento_p), 
					nr_atendimento_p, 
					'IAG', 
					0, 
					cd_material_w, 
					cd_intervalo_w, 
					qt_glicemia_w, 
					dt_horario_ccg_w, 
					nr_prescricao_p, 
					nr_prescricao_p, 
					nr_sequencia_w, 
					'N', 
					null, 
					null, 
					nm_usuario_p, 
					ie_inconsistencia_w, 
					ds_inconsistentes_w, 
					null, 
					null, 
					null,
					null,
					null,
					'S', 
					nr_seq_horario_p,
					nr_seq_prot_glic_w, 
					null, 
					nr_seq_pergunta_p, 
					'',
					null,
					null);
										
					
				end loop;
				close C01;

			elsif	(nvl(ie_aprazar_p, 'N') = 'S') and					
				(nvl(nr_seq_pergunta_p,0) > 0) then 	
					
					open C02;
					loop
					fetch C02 into	
						cd_material_w,
						ie_via_aplicacao_w,
						qt_glicemia_w,
						ds_orientacao_item_w;
					exit when C02%notfound;
						select   nvl(nr_seq_prot_glic,0)
						into     nr_seq_prot_glic_w
						from     prescr_procedimento
						where    nr_prescricao =  nr_prescricao_p
						and      nr_sequencia  = nr_seq_proced_p;
						
						/* OS 690242
						if	(nvl(qt_glicemia_w,0) = 0) then
							if 	(nvl(qt_glicose_w,0) > 0) then
								ie_glicose_w := 'S';
								qt_glicemia_w := qt_glicose_w;
							elsif 	(nvl(qt_ui_insulina_calc_w,0) > 0 ) then
								ie_glicose_w := 'N';
								qt_glicemia_w := qt_ui_insulina_calc_w;
							end if;
						end if;
						*/
						select	max(cd_intervalo),
							max(nr_sequencia),
							decode(nvl(qt_glicemia_w,0), 0, max(qt_dose), nvl(qt_glicemia_w,0))
						into	cd_intervalo_w,
							nr_sequencia_w,
							qt_glicemia_w
						from	prescr_material
						where	nr_prescricao = nr_prescricao_p
						and	nr_sequencia_proc = nr_seq_proced_p
						and	cd_material = cd_material_w
						and	nvl(ie_suspenso,'N') = 'N';
					
						aprazar_item_prescr('N', 
						obter_estab_atend(nr_atendimento_p), 
						nr_atendimento_p, 
						'IAG', 
						0, 
						cd_material_w, 
						cd_intervalo_w, 
						qt_glicemia_w, 
						dt_horario_ccg_w, 
						nr_prescricao_p, 
						nr_prescricao_p, 
						nr_sequencia_w, 
						'N', 
						null, 
						null, 
						nm_usuario_p, 
						ie_inconsistencia_w, 
						ds_inconsistentes_w, 
						null, 
						null, 
						null,
						null,
						null,
						'S', 
						nr_seq_horario_p,
						nr_seq_prot_glic_w, 
						null, 
						nr_seq_pergunta_p, 
						'',
						null,
						null);
						
						--OS 690242
						if	(qt_minutos_apraz_w	> 0) and
							(nvl(nr_seq_pergunta_p,0)	> 0) then
							select	nvl(max(qt_minutos_apraz),0)
							into	qt_min_apraz_item_w
							from 	prot_glic_nivel_assoc x
							where	x.cd_material	= cd_material_w
							and		x.nr_seq_registro = nr_seq_pergunta_p;
							
							if	(qt_min_apraz_item_w	> 0) then
								dt_horario_w := to_date(to_char(dt_horario_ccg_w,'dd/mm/yyyy hh24:mi') || ':00','dd/mm/yyyy hh24:mi:ss');							
								
								update	prescr_mat_hor
								set	qt_minutos_apraz_cg	= qt_min_apraz_item_w
								where	nr_prescricao		= nr_prescricao_p
								and	nr_seq_material		= nr_sequencia_w
								and	nr_seq_hor_glic		= nr_seq_horario_p
								and	dt_horario		= dt_horario_w;
							end if;
						end if;
						--OS 690242
												
						if	(ds_orientacao_item_w is not null) then
							ds_sugestao_w := ds_orientacao_item_w;
						end if;					
					
					end loop;
					close C02;					
			end if;
			/* obter intervalo */
			select	cd_intervalo
			into	cd_intervalo_w
			from	pep_protocolo_glicemia
			where	nr_sequencia = nr_seq_protocolo_p;

			/* Inicio OS  209422 - Victor - 15/04/2010*/
			/* Verifica se permite alterar o intervalo na prescri��o. Se sim, busca o intervalo na prescri��o com nvl para o protocolo.. */
			select	nvl(ie_protocolo_livre,'N')
			into	ie_prot_livre_w
			from	pep_protocolo_glicemia
			where	nr_sequencia = nr_seq_protocolo_p;
			
			if	(ie_prot_livre_w = 'S') then
				Select	nvl(cd_Intervalo,cd_intervalo_w)
				into	cd_intervalo_w
				from	prescr_procedimento
				where	nr_prescricao = nr_prescricao_p
				and	nr_sequencia = nr_seq_proced_p;
			end if;
			
			/* fim OS  209422 - Victor - 15/04/2010*/
			
			/* calcular pr�ximo controle */
			obter_prox_medicao_prot_glic(cd_intervalo_w, null, null, dt_glicemia_w, dt_proximo_controle_w, qt_min_interv_w);

			/* obter sequ�ncia */
			select	atendimento_glicemia_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			if (ie_unidade_glicemia_estab_w = 'mo') then
				qt_glicemia_mmol_w := qt_glicemia_p;
				qt_glicemia_mg_w := converter_mg_mmol(qt_glicemia_mmol_w, 'MG');
			else
				qt_glicemia_mg_w := qt_glicemia_p;
				qt_glicemia_mmol_w := converter_mg_mmol(qt_glicemia_mg_w, 'MMOL');				
			end if;

			insert into atendimento_glicemia	
					(
					nr_sequencia,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_atualizacao,
					nm_usuario,
					nr_atendimento,
					nr_seq_protocolo,
					nr_glicemia_atend,
					nr_glicemia,
					ie_status_glicemia,
					dt_prev_controle,
					dt_controle,
					dt_proximo_controle,
					dt_limite_ant,
					dt_limite_pos,
					qt_glicemia,
					qt_glicemia_mmol,
					qt_var_glicemia,
					qt_min_var,
					qt_min_interv_fut,
					ds_sugestao,
					ds_observacao,
					qt_ui_insulina_calc,
					qt_ui_insulina_adm,
					qt_glicose,
					qt_glicose_adm,
					nr_seq_glicemia,
					nr_seq_horario,
					ie_glic_extrapol,
					nr_seq_pergunta,
					ie_resposta,
					ds_respostas,
					qt_ui_insulina_int_calc,
					qt_ui_insulina_int_adm					
					)
				values	(
					nr_sequencia_w,
					dt_atualizacao_w,
					nm_usuario_p,
					dt_atualizacao_w,
					nm_usuario_p,
					nr_atendimento_p,
					nr_seq_protocolo_p,
					nr_glicemia_atend_w,
					nr_glicemia_w,
					'N',
					null,
					dt_glicemia_w,
					dt_proximo_controle_w,
					null,
					null,
					qt_glicemia_mg_w,
					qt_glicemia_mmol_w,
					qt_var_glicemia_w,
					qt_min_variacao_w,
					qt_min_interv_w,
					ds_sugestao_w,
					null,
					qt_ui_insulina_calc_w,
					decode(ie_gera_adm_w, 'S', qt_ui_insulina_calc_w, null),
					qt_glicose_w,
					decode(ie_gera_adm_w, 'S', qt_glicose_w,null),
					nr_seq_glicemia_w,
					nr_seq_horario_p,
					ie_extrapol_p,
					nr_seq_pergunta_p,
					ie_resposta_p,
					ds_parametros_p,
					qt_ui_insulina_int_calc_w,
					decode(ie_gera_adm_w, 'S', qt_ui_insulina_int_calc_w, null)
					);		
		
		/* atualizar atend glicemia (status x executor )*/
		atualizar_atend_glicemia(nr_seq_glicemia_w,'I',sysdate,nm_usuario_p);
		end if;
	else
		/* obter dados medicao anterior */
		select	max(dt_controle),
			nvl(max(qt_glicemia),0),
			nvl(max(qt_glicemia_mmol),0),
			nvl(max(qt_ui_insulina_calc),0),
			nvl(max(qt_min_interv_fut),60),
			max(dt_proximo_controle),
			nvl(max(nr_seq_glicemia),0),
			nvl(max(qt_ui_insulina_int_calc),0)			
		into	dt_controle_ant_w,
			qt_glicemia_atual_w,
			qt_glicemia_atual_mmol_w,
			qt_ui_insulina_atual_w,
			qt_min_interv_w,
			dt_prev_controle_w,
			nr_seq_glicemia_w,
			qt_ui_insulina_int_atual_w
		from	atendimento_glicemia
		where	nr_sequencia = nr_seq_anterior_w;

		/* calcular variacao */
		if (ie_unidade_glicemia_estab_w = 'mo') then
			qt_var_glicemia_w := qt_glicemia_p - qt_glicemia_atual_mmol_w;
		else
			qt_var_glicemia_w := qt_glicemia_p - qt_glicemia_atual_w;
		end if;

		/* calcular variacao horaria */
		qt_min_variacao_w := trunc((dt_glicemia_w - dt_controle_ant_w) * 1440);

		/* obter nivel protocolo */
		if	(nvl(nr_prescricao_p,0) = 0) and
			(nvl(nr_seq_proced_p,0) = 0) and
			(nvl(nr_seq_glic_atend_p,0) = 0) then
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_nivel_w
			from	pep_prot_glic_nivel
			where	nr_seq_protocolo = nr_seq_protocolo_p
			and	qt_glicemia_p between qt_glic_inic	and	qt_glic_fim
			and	((ie_glic_extrapol is null) or (ie_glic_extrapol = ie_extrapol_p));
			
			if	(nr_seq_nivel_w = 0) then
				select	nvl(max(nr_sequencia),0),
					nvl(max('S'),'N')
				into	nr_seq_nivel_w,
					ie_manutencao_w
				from	prot_glic_manutencao
				where	nr_seq_protocolo = nr_seq_protocolo_p
				and	qt_glicemia_p between qt_glic_inic and qt_glic_fim
				and	((ie_glic_extrapol is null) or (ie_glic_extrapol = ie_extrapol_p));
			end if;

		elsif	(nvl(nr_prescricao_p,0) > 0) and
			(nvl(nr_seq_proced_p,0) > 0) and
			(nvl(nr_seq_glic_atend_p,0) > 0) then
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_nivel_w
			from	prescr_proc_glic
			where	nr_prescricao 		= nr_prescricao_p
			and	nr_seq_procedimento 	= nr_seq_proced_p
			and	nr_seq_protocolo 	= nr_seq_protocolo_p
			and	qt_glicemia_p between qt_glic_inic and qt_glic_fim;
			
			if	(nr_seq_nivel_w = 0) then
				select	nvl(max(nr_sequencia),0),
					nvl(max('S'),'N')
				into	nr_seq_nivel_w,
					ie_manutencao_w
				from	prot_glic_manutencao
				where	nr_seq_protocolo = nr_seq_protocolo_p
				and	qt_glicemia_p between qt_glic_inic and qt_glic_fim
				and	((ie_glic_extrapol is null) or (ie_glic_extrapol = ie_extrapol_p));
			end if;

		else
			--N�o foi poss�vel realizar a busca dos n�veis de glicemia!
			Wheb_mensagem_pck.exibir_mensagem_abort(174243);
		end if;

		if	(nr_seq_nivel_w > 0) then
			/* obter valores protocolo */
			if	(nvl(nr_prescricao_p,0) = 0) and
				(nvl(nr_seq_proced_p,0) = 0) and
				(nvl(nr_seq_glic_atend_p,0) = 0) then
				select	qt_ui_insulina,
					qt_glicose,
					nvl(ds_orientacao_pergunta_w, ds_sugestao),
					nvl(qt_min_pergunta_w, qt_minutos_medicao),
					qt_ui_insulina_int
				into	qt_ui_insulina_calc_w,
					qt_glicose_w,
					ds_sugestao_w,
					qt_min_ccg_w,
					qt_ui_insulina_int_calc_w
				from	pep_prot_glic_nivel
				where	nr_sequencia = nr_seq_nivel_w;
				
				if	(ie_manutencao_w = 'S') then
					select	qt_insulina_bolus,
						qt_glicose,
						nvl(ds_orientacao_pergunta_w, ds_sugestao)
					into	qt_ui_insulina_calc_w,
						qt_glicose_w,
						ds_sugestao_w
					from	prot_glic_manutencao
					where	nr_sequencia = nr_seq_nivel_w;
				end if;

			elsif	(nvl(nr_prescricao_p,0) > 0) and
				(nvl(nr_seq_proced_p,0) > 0) and
				(nvl(nr_seq_glic_atend_p,0) > 0) then
				select	max(qt_ui_insulina),
						max(qt_glicose),
						max(nvl(ds_orientacao_pergunta_w, ds_sugestao)),
						max(nvl(qt_min_pergunta_w,qt_minutos_medicao)),
					max(qt_ui_insulina_int)
				into	qt_ui_insulina_calc_w,
						qt_glicose_w,
						ds_sugestao_w,
						qt_min_ccg_w,
					qt_ui_insulina_int_calc_w
				from	prescr_proc_glic
				where	nr_sequencia = nr_seq_nivel_w;
				
				if	(nvl(qt_min_ccg_w,0) = 0) then
					select	nvl(max(nr_sequencia),0)
					into	nr_seq_nivel_w
					from	pep_prot_glic_nivel
					where	nr_seq_protocolo = nr_seq_protocolo_p
					and	qt_glicemia_p between qt_glic_inic and qt_glic_fim;
					
					select	max(nvl(qt_min_pergunta_w,qt_minutos_medicao))
					into	qt_min_ccg_w
					from	pep_prot_glic_nivel
					where	nr_sequencia = nr_seq_nivel_w;
				end if;
				
				if	(ie_manutencao_w = 'S') then
					select	qt_insulina_bolus,
						qt_glicose,
						nvl(ds_orientacao_pergunta_w, ds_sugestao)
					into	qt_ui_insulina_calc_w,
						qt_glicose_w,
						ds_sugestao_w
					from	prot_glic_manutencao
					where	nr_sequencia = nr_seq_nivel_w;
				end if;
			end if;
			
			if	(nr_seq_horario_p > 0) then
				select	max(a.nr_sequencia),
					max(a.cd_procedimento), 
					max(a.cd_intervalo),
					max(a.qt_procedimento),
					max(a.nr_prescricao),
					max(a.nr_seq_proc_interno),
					max(decode(a.ie_acm,'N',decode(a.ie_se_necessario,'S','S','N'),a.ie_acm)),
					max(a.nr_seq_prot_glic)
				into	nr_seq_proced_w,
					cd_procedimento_w,
					cd_interv_ccg_w,
					qt_procedimento_w,
					nr_prescr_proced_w,
					nr_seq_proc_interno_w,
					ie_acm_sn_w,
					nr_seq_prot_glic_w
				from	prescr_procedimento a,
					prescr_proc_hor b
				where	a.nr_sequencia = b.nr_seq_procedimento
				and	a.nr_prescricao = b.nr_prescricao
				and	b.nr_sequencia = nr_seq_horario_p;
				
				select	max(b.dt_horario)
				into	dt_horario_ccg_w
				from	prescr_proc_hor b
				where	nr_sequencia 	= nr_seq_horario_p;
				
			else
				select	max(a.nr_sequencia),
					max(a.cd_procedimento), 
					max(a.cd_intervalo),
					max(a.qt_procedimento),
					max(a.nr_prescricao),
					max(a.nr_seq_proc_interno),
					max(decode(a.ie_acm,'N',decode(a.ie_se_necessario,'S','S','N'),a.ie_acm)),
					max(a.nr_seq_prot_glic)
				into	nr_seq_proced_w,
					cd_procedimento_w,
					cd_interv_ccg_w,
					qt_procedimento_w,
					nr_prescr_proced_w,
					nr_seq_proc_interno_w,
					ie_acm_sn_w,
					nr_seq_prot_glic_w
				from	prescr_procedimento a
				where	a.nr_prescricao	= nr_prescricao_p
				and	a.nr_sequencia	= nr_seq_proced_p;
				
				select	max(b.dt_horario)
				into	dt_horario_ccg_w
				from	prescr_proc_hor b
				where	b.nr_seq_procedimento	= nr_seq_proced_w
				and	b.nr_prescricao 	= nr_prescr_proced_w;
			end if;
			
			--OS 690242
			if 	(nvl(qt_glicose_w,0) > 0) then			
				ie_glicose_w := 'S';
				qt_glicemia_w := qt_glicose_w;			
			elsif 	(nvl(qt_ui_insulina_calc_w,0) > 0 ) then			
				ie_glicose_w := 'N';
				qt_glicemia_w := qt_ui_insulina_calc_w;		
			elsif 	(nvl(qt_ui_insulina_int_calc_w,0) > 0 ) then			
				ie_glicose_w := 'I';
				qt_glicemia_w := qt_ui_insulina_int_calc_w;
			end if;				
		
			if	(ie_gera_hor_assoc_w	= 'N') and
				(nvl(nr_seq_pergunta_p,0)	> 0) then
				select	nvl(max(x.qt_minutos_apraz),0)
				into	qt_minutos_apraz_w
				from	prot_glic_nivel_assoc x,
						prescr_material a
				where	x.cd_material	= a.cd_material
				and		a.nr_prescricao = nr_prescricao_p
				and		a.nr_sequencia_proc = nr_seq_proced_p
				--and		nvl(a.ie_glicemia, 'N') in (ie_glicose_w)
				and		x.nr_seq_registro = nr_seq_pergunta_p
				and 	nvl(a.ie_suspenso, 'N') = 'N';
				
			else
				qt_minutos_apraz_w	:= 0;
			end if;				
			--OS690242	

			
			if	(nvl(qt_min_ccg_w,0) > 0) and							
				(qt_minutos_apraz_w	= 0) then --OS 690242
				Aprazar_item_prescr('N', 
				obter_estab_atend(nr_atendimento_p), 
				nr_atendimento_p, 
				'G', 
				0, 
				cd_procedimento_w, 
				cd_interv_ccg_w, 
				qt_procedimento_w, 
				(dt_horario_ccg_w + (qt_min_ccg_w/1440)), 
				to_char(nr_prescricao_p), 
				nr_prescricao_p, 
				nr_seq_proced_w, 
				'N', 
				null, 
				obter_desc_expressao(346946), 
				nm_usuario_p, 
				ie_inconsistencia_ccg_w, 
				ds_inconsistentes_ccg_w, 
				nr_seq_proc_interno_w,
				null, 
				ie_acm_sn_w, 
				null,
				null,
				'N',
				nr_seq_horario_p,
				nr_seq_prot_glic_w, 
				null, 
				nr_seq_pergunta_p, 
				'',
				null,
				null);
				
			end if;
			
			if	(nvl(ie_aprazar_p, 'N') = 'S') and	
				(ie_gera_hor_assoc_w	= 'N') and
				(nvl(nr_seq_pergunta_p,0) = 0) and
				((nvl(qt_ui_insulina_calc_w,0) > 0) or
				 (nvl(qt_ui_insulina_int_calc_w,0) > 0) or
				 (nvl(qt_glicose_w,0) > 0 )) then
				 
				
				if 	(nvl(qt_glicose_w,0) > 0) then			
					ie_glicose_w := 'S';
					qt_glicemia_w := qt_glicose_w;			
				elsif 	(nvl(qt_ui_insulina_calc_w,0) > 0 ) then			
					ie_glicose_w := 'N';
					qt_glicemia_w := qt_ui_insulina_calc_w;		
				elsif 	(nvl(qt_ui_insulina_int_calc_w,0) > 0 ) then			
					ie_glicose_w := 'I';
					qt_glicemia_w := qt_ui_insulina_int_calc_w;
				end if;	 
				
				open C01;
				loop
				fetch C01 into	
					cd_material_w,
					nr_sequencia_w,
					cd_intervalo_w;
				exit when C01%notfound;
					select   nvl(nr_seq_prot_glic,0)
					into     nr_seq_prot_glic_w
					from     prescr_procedimento
					where    nr_prescricao =  nr_prescricao_p
					and      nr_sequencia  = nr_seq_proced_p;
					
					aprazar_item_prescr('N', obter_estab_atend(nr_atendimento_p), nr_atendimento_p, 'IAG', 0, cd_material_w, cd_intervalo_w, qt_glicemia_w, dt_horario_ccg_w, nr_prescricao_p, nr_prescricao_p, nr_sequencia_w, 'N', null, null, nm_usuario_p, ie_inconsistencia_w, ds_inconsistentes_w, null, null, null,null,null,'S',nr_seq_horario_p,nr_seq_prot_glic_w, null, nr_seq_pergunta_p, '',null, null);
										
					
				end loop;
				close C01;
			
			elsif	(nvl(ie_aprazar_p, 'N') = 'S') and	
				(nvl(nr_seq_pergunta_p,0) > 0) then 	
					
					open C02;
					loop
					fetch C02 into	
						cd_material_w,
						ie_via_aplicacao_w,
						qt_glicemia_w,
						ds_orientacao_item_w;
					exit when C02%notfound;
					
						select   nvl(nr_seq_prot_glic,0)
						into     nr_seq_prot_glic_w
						from     prescr_procedimento
						where    nr_prescricao =  nr_prescricao_p
						and      nr_sequencia  = nr_seq_proced_p;
						
						/* OS 690242
						if	(nvl(qt_glicemia_w,0) = 0) then
							if 	(nvl(qt_glicose_w,0) > 0) then
								ie_glicose_w := 'S';
								qt_glicemia_w := qt_glicose_w;
							elsif 	(nvl(qt_ui_insulina_calc_w,0) > 0 ) then
								ie_glicose_w := 'N';
								qt_glicemia_w := qt_ui_insulina_calc_w;
							end if;
						end if;
						*/
						
						select	max(cd_intervalo),
								max(nr_sequencia),
								decode(nvl(qt_glicemia_w,0), 0, max(qt_dose), nvl(qt_glicemia_w,0))
						into	cd_intervalo_w,
								nr_sequencia_w,
								qt_glicemia_w
						from	prescr_material
						where	nr_prescricao = nr_prescricao_p
						and		nr_sequencia_proc = nr_seq_proced_p
						and		cd_material = cd_material_w
						and		nvl(ie_suspenso,'N') = 'N';
					
						aprazar_item_prescr('N', obter_estab_atend(nr_atendimento_p), nr_atendimento_p, 'IAG', 0, cd_material_w, cd_intervalo_w, qt_glicemia_w, dt_horario_ccg_w, nr_prescricao_p, nr_prescricao_p, nr_sequencia_w, 'N', null, null, nm_usuario_p, ie_inconsistencia_w, ds_inconsistentes_w, null, null, null,null,null,'S',nr_seq_horario_p,nr_seq_prot_glic_w, null, nr_seq_pergunta_p, '',null,null);
						
						--OS 690242
						if	(qt_minutos_apraz_w	> 0) and
							(nvl(nr_seq_pergunta_p,0)	> 0) then
							select	nvl(max(qt_minutos_apraz),0)
							into	qt_min_apraz_item_w
							from 	prot_glic_nivel_assoc x
							where	x.cd_material	= cd_material_w
							and		x.nr_seq_registro = nr_seq_pergunta_p;
							
							if	(qt_min_apraz_item_w	> 0) then
								
								dt_horario_w := to_date(to_char(dt_horario_ccg_w,'dd/mm/yyyy hh24:mi') || ':00','dd/mm/yyyy hh24:mi:ss');							
								
								update	prescr_mat_hor
								set		qt_minutos_apraz_cg	= qt_min_apraz_item_w
								where	nr_prescricao	= nr_prescricao_p
								and		nr_seq_material	= nr_sequencia_w
								and		nr_seq_hor_glic	= nr_seq_horario_p
								and		dt_horario		= dt_horario_w;
								
							end if;
						end if;	
						-- OS 690242	
						
						if	(ds_orientacao_item_w is not null) then
							ds_sugestao_w := ds_orientacao_item_w;
						end if;
						
					end loop;
					close C02;				
			end if;
			
			/* obter intervalo */
			select	cd_intervalo
			into	cd_intervalo_w
			from	pep_protocolo_glicemia
			where	nr_sequencia = nr_seq_protocolo_p;

			/* obter data prev anterior */
			select	max(dt_proximo_controle)
			into	dt_prev_ant_w
			from	atendimento_glicemia
			where	nr_atendimento = nr_atendimento_p
			and	nr_seq_protocolo = nr_seq_protocolo_p
			and	nr_glicemia_atend = nr_glicemia_atend_w
			and	dt_proximo_controle < dt_prev_controle_w;

			/* calcular pr�ximo controle */
			obter_prox_medicao_prot_glic(cd_intervalo_w, dt_prev_ant_w, dt_prev_controle_w, dt_glicemia_w, dt_proximo_controle_w, qt_min_interv_w);

			/* obter sequ�ncia */
			select	atendimento_glicemia_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			if (ie_unidade_glicemia_estab_w = 'mo') then
				qt_glicemia_mmol_w := qt_glicemia_p;
				qt_glicemia_mg_w := converter_mg_mmol(qt_glicemia_mmol_w, 'MG');
			else
				qt_glicemia_mg_w := qt_glicemia_p;
				qt_glicemia_mmol_w := converter_mg_mmol(qt_glicemia_mg_w, 'MMOL');				
			end if;

			insert into atendimento_glicemia	(
								nr_sequencia,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								dt_atualizacao,
								nm_usuario,
								nr_atendimento,
								nr_seq_protocolo,
								nr_glicemia_atend,
								nr_glicemia,
								ie_status_glicemia,
								dt_prev_controle,
								dt_controle,
								dt_proximo_controle,
								dt_limite_ant,
								dt_limite_pos,
								qt_glicemia,
								qt_glicemia_mmol,
								qt_var_glicemia,
								qt_min_var,
								qt_min_interv_fut,
								ds_sugestao,
								ds_observacao,
								qt_ui_insulina_calc,
								qt_ui_insulina_adm,
								qt_glicose,
								qt_glicose_adm,
								nr_seq_glicemia,
								nr_seq_horario,
								ie_glic_extrapol,
								nr_seq_pergunta,
								ie_resposta,
								ds_respostas,
								qt_ui_insulina_int_calc,
								qt_ui_insulina_int_adm
								)
							values	(
								nr_sequencia_w,
								dt_atualizacao_w,
								nm_usuario_p,
								dt_atualizacao_w,
								nm_usuario_p,
								nr_atendimento_p,
								nr_seq_protocolo_p,
								nr_glicemia_atend_w,
								nr_glicemia_w,
								'N',
								dt_prev_controle_w,
								dt_glicemia_w,
								dt_proximo_controle_w,
								dt_prev_controle_w - qt_min_glic_w / 1440,
								dt_prev_controle_w + qt_min_glic_w / 1440,
								qt_glicemia_mg_w,
								qt_glicemia_mmol_w,
								qt_var_glicemia_w,
								qt_min_variacao_w,
								qt_min_interv_w,
								ds_sugestao_w,
								null,
								qt_ui_insulina_calc_w,
								decode(ie_gera_adm_w, 'S', qt_ui_insulina_calc_w, null),
								qt_glicose_w,
								decode(ie_gera_adm_w, 'S', qt_glicose_w,null),
								decode(nr_seq_glicemia_w,0,null,nr_seq_glicemia_w),
								nr_seq_horario_p,
								ie_extrapol_p,
								nr_seq_pergunta_p,
								ie_resposta_p,
								ds_parametros_p,
								qt_ui_insulina_int_calc_w,
								decode(ie_gera_adm_w, 'S', qt_ui_insulina_int_calc_w, null)
								);			
		end if;
	end if;
	/* gerar evento */
	if	(nr_sequencia_w > 0) then
		gerar_alteracao_glicemia(nr_sequencia_w, 1, ' ', nm_usuario_p);
	end if;
	
	if	(Ie_registrar_sv_w = 'S') then
		select	max(nr_seq_assinatura)
		into	nr_seq_assinatura_w
		from	prescr_mat_alteracao
		where	nr_prescricao 		= nr_prescricao_p
		and		nr_seq_procedimento = nr_seq_proced_p
		and		nr_seq_horario_proc	= nr_seq_horario_p;
		
		if	(ie_gera_adm_w = 'S') then
			qt_glicose_adm_w :=  qt_glicose_w;
		else
			qt_glicose_adm_w := null;
		end if;
		
		ADEP_gerar_SV_Glicemia(nr_atendimento_p, qt_glicemia_mg_w, nm_usuario_p, qt_ui_insulina_calc_w, dt_glicemia_w, nr_seq_assinatura_w, qt_ui_insulina_int_calc_w, ie_extrapol_p, qt_glicose_adm_w, qt_glicemia_mmol_w);		
	end if;
	
end if;

nr_seq_glicemia_p := nr_sequencia_w;

ds_orientacao_item_p := ds_orientacao_item_w;

if	((nvl(qt_ui_insulina_calc_w,0) > 0) or
	(nvl(qt_ui_insulina_int_calc_w,0) > 0) or
	(nvl(qt_glicemia_w,0) > 0) or
	(nvl(qt_glicose_w,0) > 0)) and
	(ie_gera_hor_assoc_w = 'N')	then
	ie_medic_suplementar_p := 'S';
else
	ie_medic_suplementar_p := 'N';
end if;

commit;

end registrar_nivel_hipoglic_prot;
/