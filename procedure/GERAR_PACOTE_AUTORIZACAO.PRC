create or replace
procedure GERAR_PACOTE_AUTORIZACAO
			(nr_sequencia_autor_p	number,
			nm_usuario_p		Varchar2) is 

begin

GERAR_PACOTE_AUTORIZACAO_PCK.gerar_pacote_autorizacao(nr_sequencia_autor_p,nm_usuario_p);

update	autorizacao_convenio
set	dt_geracao_pacote	= sysdate
where	nr_sequencia		= nr_sequencia_autor_p;

commit;

end GERAR_PACOTE_AUTORIZACAO;
/