create or replace
procedure create_dashboard_kpi(nm_usuario_p			varchar2,
							   nr_seq_dashboard_p   out number	) is 

begin

	select	dashboard_base_seq.nextval
	into	nr_seq_dashboard_p
	from	dual;
	
	insert into dashboard_base (	
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec)
	values(	nr_seq_dashboard_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p);

commit;

end create_dashboard_kpi;
/