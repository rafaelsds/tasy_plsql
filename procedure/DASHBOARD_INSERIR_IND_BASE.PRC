create or replace
procedure dashboard_inserir_ind_base(
			ds_indicador_p 	in varchar2,
			nm_usuario_p 	in varchar2,
			ds_origem_inf_p in varchar2,
			nr_sequencia_p out ind_base.nr_sequencia%type) is
		
begin

	select	ind_base_seq.nextval
	into	nr_sequencia_p
	from	dual;

	insert	into ind_base
		(nr_sequencia, 
		dt_atualizacao, 
		nm_usuario, 
		dt_atualizacao_nrec, 
		nm_usuario_nrec, 
		ds_indicador, 
		nr_seq_ordem_serv, 
		ie_padrao_sistema, 
		ds_sql_where, 
		ds_origem_inf, 
		cd_exp_indicador, 
		ds_objetivo, 
		ie_situacao,
		ie_origem)
	values	(nr_sequencia_p, 
		sysdate, 
		nm_usuario_p, 
		sysdate, 
		nm_usuario_p, 
		ds_indicador_p, 
		null, 
		'N', 
		null, 
		ds_origem_inf_p, 
		null, 
		null, 
		'A',
		'P');

	commit;					 
end dashboard_inserir_ind_base;
/
