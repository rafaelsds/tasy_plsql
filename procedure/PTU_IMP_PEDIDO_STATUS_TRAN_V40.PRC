create or replace
procedure ptu_imp_pedido_status_tran_v40
			(	ds_arquivo_p		Varchar2,
				cd_estabelecimento_p	Number,
				nm_usuario_p		Varchar2,
				nr_seq_origem_p	out	Number) is
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Importar pedido de status tran_v40

rotina utilizada nas transa��es ptu via scs homologadas com a unimed brasil.
quando for alterar, favor verificar com o an�lista respons�vel para a realiza��o de testes.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
Performance
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
cd_transacao_w			Varchar2(255);
ie_tipo_cliente_w		Varchar2(255);
cd_unimed_exec_w		Varchar2(255);
cd_unimed_benef_w		Varchar2(255);
nr_ident_exec_w			Number(15);
nr_seq_pedido_w			Number(15);
ds_conteudo_w			Varchar2(4000);
nr_seq_import_w			Number(15);
nr_seq_pedido_aut_compl_w	Number(15);
nr_seq_origem_w			Number(15);
nr_seq_pedido_aut_w		Number(15);
nr_seq_pedido_compl_w		Number(15);
nr_seq_guia_w			Number(15);
nr_seq_requisicao_w		Number(15);

cursor c01 is
	select	nr_seq_importacao,
		ds_valores
	from	w_scs_importacao
	where	nm_usuario	= nm_usuario_p
	order by nr_seq_importacao;

begin

open C01;
loop
fetch C01 into	
	nr_seq_import_w,
	ds_conteudo_w;
exit when C01%notfound;
	begin
	if	(substr(ds_conteudo_w,1,4)	<> 'FIM$') then
		if	(nr_seq_import_w	= 1) then
			cd_transacao_w			:= substr(ds_conteudo_w,1,5);
			ie_tipo_cliente_w		:= trim(substr(ds_conteudo_w,6,15));
			cd_unimed_exec_w		:= trim(substr(ds_conteudo_w,21,4));
			cd_unimed_benef_w		:= trim(substr(ds_conteudo_w,25,4));
			nr_ident_exec_w			:= to_number(substr(ds_conteudo_w,29,10));

			begin
				select	nr_sequencia
				into	nr_seq_pedido_compl_w
				from	ptu_pedido_compl_aut
				where	nr_seq_execucao	= nr_ident_exec_w;
			exception
			when others then
				begin					
					select	nr_sequencia
					into	nr_seq_pedido_aut_w
					from	ptu_pedido_autorizacao
					where	nr_seq_execucao	= nr_ident_exec_w;
				exception
				when others then
					nr_seq_pedido_aut_w	:= null;
					nr_seq_pedido_compl_w	:= null;
				end;
			end;

			if	(nvl(nr_seq_pedido_aut_w,0)	<> 0) then
				begin
					select	nr_seq_guia,
						nr_seq_requisicao
					into	nr_seq_guia_w,
						nr_seq_requisicao_w
					from	ptu_pedido_autorizacao
					where	nr_sequencia	= nr_seq_pedido_aut_w;
				exception
				when others then
					nr_seq_guia_w		:= null;
					nr_seq_requisicao_w	:= null;
				end;
			
				nr_seq_pedido_aut_compl_w	:= nr_seq_pedido_aut_w;
			elsif	(nvl(nr_seq_pedido_compl_w,0)	<> 0) then
				begin
					select	nr_seq_guia,
						nr_seq_requisicao
					into	nr_seq_guia_w,
						nr_seq_requisicao_w
					from	ptu_pedido_compl_aut
					where	nr_sequencia	= nr_seq_pedido_compl_w;
				exception
				when others then
					nr_seq_guia_w		:= null;
					nr_seq_requisicao_w	:= null;
				end;
				
				nr_seq_pedido_aut_compl_w	:= nr_seq_pedido_compl_w;
			end if;

			if	(ie_tipo_cliente_w	= 'UNIMED') then
				ie_tipo_cliente_w	:= 'U';
			elsif	(ie_tipo_cliente_w	= 'PORTAL') then
				ie_tipo_cliente_w	:= 'P';
			elsif	(ie_tipo_cliente_w	= 'PRESTADOR') then
				ie_tipo_cliente_w	:= 'R';
			end if;
			
			if	(cd_transacao_w	= '00360') then
				select	ptu_pedido_status_seq.NextVal
				into	nr_seq_pedido_w
				from	dual;
				
				insert	into ptu_pedido_status
					(nr_sequencia, cd_transacao, cd_unimed_beneficiario,
					 cd_unimed_executora, dt_atualizacao, dt_atualizacao_nrec,
					 ie_tipo_cliente, nm_usuario, nm_usuario_nrec,
					 nr_transacao_uni_exec, nr_seq_guia, nr_seq_requisicao)
				values	(nr_seq_pedido_w, '00360', cd_unimed_benef_w,
					 cd_unimed_exec_w, sysdate, sysdate,
					 ie_tipo_cliente_w, nm_usuario_p, nm_usuario_p,
					 nr_ident_exec_w, nr_seq_guia_w, nr_seq_requisicao_w);
			end if;
		end if;
	end if;
	end;
end loop;
close C01;

ptu_gestao_env_res_status_tran(nr_seq_pedido_aut_compl_w, nr_seq_pedido_w, cd_estabelecimento_p, nm_usuario_p, nr_seq_origem_w);

nr_seq_origem_p	:= nr_seq_origem_w;

commit;

end ptu_imp_pedido_status_tran_v40;
/
