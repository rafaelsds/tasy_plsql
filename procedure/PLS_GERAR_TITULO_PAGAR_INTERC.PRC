create or replace
procedure pls_gerar_titulo_pagar_interc (	nr_seq_fat_arq_p	pls_faturamento_arq.nr_sequencia%type,
						nr_seq_regra_tit_p	pls_regra_tit_pag_fat.nr_sequencia%type,
						ie_commit_p		varchar2,
						nm_usuario_p		usuario.nm_usuario%type,
						cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type) is
						
nr_seq_trans_fin_baixa_w	pls_regra_tit_pag_fat.nr_seq_trans_fin_baixa%type;
nr_seq_trans_fin_contab_w	pls_regra_tit_pag_fat.nr_seq_trans_fin_contab%type;
ie_status_tit_pagar_w		pls_regra_tit_pag_fat.ie_status_tit_pagar%type;
nr_seq_classe_w			pls_regra_tit_pag_fat.nr_seq_classe%type;
qt_dias_vencimento_w		pls_regra_tit_pag_fat.qt_dias_vencimento%type;
vl_total_imp_w			pls_faturamento_conta.vl_total_imp%type;
nr_seq_prest_inter_w		pls_faturamento_prot.nr_seq_prest_inter%type;
nr_seq_prest_inter_cpf_w	pls_prestador_intercambio.nr_cpf%type;
nr_seq_prest_inter_cgc_w	pls_prestador_intercambio.cd_cgc_intercambio%type;
nr_seq_outorgante_w		pls_faturamento_prot.nr_seq_outorgante%type;
cd_cgc_w			pls_congenere.cd_cgc%type;
cd_moeda_cp_w			parametros_contas_pagar.cd_moeda_padrao%type;
cd_tipo_taxa_juro_cp_w		parametros_contas_pagar.cd_tipo_taxa_juro%type;
cd_tipo_taxa_multa_cp_w		parametros_contas_pagar.cd_tipo_taxa_multa%type;
tx_juros_cp_w			parametros_contas_pagar.pr_juro_padrao%type;
tx_multa_cp_w			parametros_contas_pagar.pr_multa_padrao%type;
nr_titulo_pagar_w		titulo_pagar.nr_titulo%type;

begin

if	(nr_seq_fat_arq_p is not null) and (nr_seq_regra_tit_p is not null) then
	select	max(nr_seq_trans_fin_baixa),
		max(nr_seq_trans_fin_contab),
		nvl(max(ie_status_tit_pagar), 'P'),
		max(nr_seq_classe),
		nvl(max(null), 0)
	into	nr_seq_trans_fin_baixa_w,
		nr_seq_trans_fin_contab_w,
		ie_status_tit_pagar_w,
		nr_seq_classe_w,
		qt_dias_vencimento_w
	from	pls_regra_tit_pag_fat
	where	nr_sequencia = nr_seq_regra_tit_p;
	
	select	nvl(sum(b.vl_total_imp), 0),
		max(a.nr_seq_prest_inter),
		max(pls_obter_dados_prest_inter(a.nr_seq_prest_inter, 'CDPF')),
		max(pls_obter_dados_prest_inter(a.nr_seq_prest_inter, 'CGC')),
		max(a.nr_seq_outorgante)
	into	vl_total_imp_w,
		nr_seq_prest_inter_w,
		nr_seq_prest_inter_cpf_w,
		nr_seq_prest_inter_cgc_w,
		nr_seq_outorgante_w
	from	pls_faturamento_conta	b,
		pls_faturamento_prot	a
	where	a.nr_sequencia		= b.nr_seq_protocolo
	and	a.nr_seq_fat_arquivo	= nr_seq_fat_arq_p;
	
	select	max(cd_cgc)
	into	cd_cgc_w
	from	pls_congenere
	where	nr_sequencia = nr_seq_outorgante_w;
	
	if	(cd_cgc_w is not null) then
		nr_seq_prest_inter_cpf_w 	:= null;
		nr_seq_prest_inter_cgc_w	:= cd_cgc_w;
	end if;
	
	if	(vl_total_imp_w > 0) and ((nr_seq_prest_inter_cpf_w is not null) or (nr_seq_prest_inter_cgc_w is not null)) then
		begin
			select	cd_moeda_padrao,
				cd_tipo_taxa_juro,
				cd_tipo_taxa_multa,
				pr_juro_padrao,
				pr_multa_padrao
			into	cd_moeda_cp_w,
				cd_tipo_taxa_juro_cp_w,
				cd_tipo_taxa_multa_cp_w,
				tx_juros_cp_w,
				tx_multa_cp_w
			from	parametros_contas_pagar
			where	cd_estabelecimento = cd_estabelecimento_p;
		exception
			when no_data_found then
			wheb_mensagem_pck.exibir_mensagem_abort(189164);
		end;
		
		insert into titulo_pagar (	nr_titulo,			nm_usuario,			dt_atualizacao,			cd_estabelecimento,
						vl_titulo,			vl_saldo_titulo,		dt_emissao,			dt_vencimento_original,
						dt_vencimento_atual,		vl_saldo_juros,			vl_saldo_multa,			cd_moeda,
						cd_tipo_taxa_juro,		cd_tipo_taxa_multa,		tx_juros,			tx_multa,
						ie_origem_titulo,		ie_tipo_titulo,			ie_situacao,			dt_contabil,
						cd_pessoa_fisica,		cd_cgc,				ie_pls,				nr_lote_contabil,
						nr_seq_trans_fin_baixa,		nr_seq_trans_fin_contab,	ie_desconto_dia,		ds_observacao_titulo,
						ie_bloqueto,			ie_status,			nr_seq_classe)
					values (titulo_pagar_seq.nextval,	nm_usuario_p,			sysdate,			cd_estabelecimento_p,
						vl_total_imp_w,			vl_total_imp_w,			sysdate,			sysdate + qt_dias_vencimento_w,
						sysdate + qt_dias_vencimento_w,	0,				0,				cd_moeda_cp_w,
						cd_tipo_taxa_juro_cp_w,		cd_tipo_taxa_multa_cp_w,	tx_juros_cp_w,			tx_multa_cp_w,
						'16' /* OPS - Interc�mbio */,		'23' /* Fatura */,			'A',				sysdate,
						nr_seq_prest_inter_cpf_w,	nr_seq_prest_inter_cgc_w,	'S',				0,
						nr_seq_trans_fin_baixa_w,	nr_seq_trans_fin_contab_w,	'N',				'T�tulo gerado � partir da importa��o do protocolo de interc�mbio',
						'N',				ie_status_tit_pagar_w,		nr_seq_classe_w) returning nr_titulo into nr_titulo_pagar_w;
						
		update	pls_faturamento_arq
		set	nr_titulo_pagar	= nr_titulo_pagar_w
		where	nr_sequencia	= nr_seq_fat_arq_p;
		
		if	(nvl(ie_commit_p, 'N') = 'S') then
			commit;
		end if;
	end if;
end if;

end pls_gerar_titulo_pagar_interc;
/