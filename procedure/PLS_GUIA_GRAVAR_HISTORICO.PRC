create or replace
procedure pls_guia_gravar_historico
			(	nr_seq_guia_p		Number,
				ie_tipo_log_p		Number,
				ds_observacao_p		Varchar2,
				ds_parametro_p		Varchar2,
				nm_usuario_p		Varchar2) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gravar os históricos referentes as guias de autorização.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
	
cd_guia_w			Varchar2(20);
ds_observacao_w			Varchar2(4000);
begin

ds_observacao_w		:= substr(ds_observacao_p, 1,3970);

begin
	select	cd_guia
	into	cd_guia_w
	from	pls_guia_plano
	where	nr_sequencia	= nr_seq_guia_p;
exception
when others then
	cd_guia_w := null;
end;

insert into pls_guia_plano_historico 
	(nr_sequencia, nr_seq_guia, ie_tipo_log,
	dt_historico, dt_atualizacao, nm_usuario,
	ds_observacao, ie_origem_historico, ie_tipo_historico)
values(	pls_guia_plano_historico_seq.nextval, nr_seq_guia_p, ie_tipo_log_p,
	sysdate, sysdate, nm_usuario_p,
	'Guia: ' || cd_guia_w || ' / ' || ds_observacao_w,'A','L');

end pls_guia_gravar_historico;
/
