create or replace
procedure release_multiple_events_gqa(nr_atendimento_p in ATENDIMENTO_PACIENTE.NR_ATENDIMENTO%type,
                    ie_is_batch_release_p	in varchar2 default 'N',
                    nr_seq_prot_int_paciente_p in PROTOCOLO_INT_PACIENTE.nr_sequencia%type,
                    dt_prevista_p	in PROTOCOLO_INT_PAC_EVENTO.dt_prevista%type,
                    nm_usuario_p	in usuario.nm_usuario%type,
                    ds_nr_seq_pend_pac_p out NOCOPY varchar2) is 

ds_nr_seq_pend_pac_w varchar2(1000);
ds_nr_seq_pend_pac_aux_w varchar2(1000);

cursor c01 is
select distinct d.nr_sequencia nr_seq_evento 
from
(
	select 
	c.nr_sequencia, 
	f.nr_seq_regra_acao, 
	nvl(decode(max(obter_informacoes_pendencia(f.nr_sequencia,'D')),null,'N','S'),'N') ie_has_executed
	from protocolo_int_paciente a
	inner join protocolo_int_pac_etapa b on b.nr_seq_protocolo_int_pac = a.nr_sequencia
	inner join protocolo_int_pac_evento c on c.nr_seq_prt_int_pac_etapa = b.nr_sequencia
	inner join gqa_pendencia_regra d on d.nr_sequencia = c.nr_seq_plano
	left join gqa_pendencia_pac e on e.nr_seq_protocolo_int_pac_even = c.nr_sequencia
	left join gqa_pend_pac_acao f on f.nr_seq_pend_pac = e.nr_sequencia
	where (
		(nvl(ie_is_batch_release_p,'N') = 'N' AND trunc(c.dt_prevista) = trunc(dt_prevista_p))
		or nvl(ie_is_batch_release_p,'N') = 'S'
	)
	and a.nr_sequencia = nr_seq_prot_int_paciente_p
	group by f.nr_seq_regra_acao,c.nr_sequencia
) a,
protocolo_int_paciente b
inner join protocolo_int_pac_etapa c on c.nr_seq_protocolo_int_pac = b.nr_sequencia
inner join protocolo_int_pac_evento d on d.nr_seq_prt_int_pac_etapa = c.nr_sequencia
inner join gqa_pendencia_regra e on e.nr_sequencia = d.nr_seq_plano
inner join gqa_acao f on f.nr_seq_pend_regra = e.nr_sequencia
where a.ie_has_executed = 'N'
and (f.nr_sequencia = a.nr_seq_regra_acao or a.nr_seq_regra_acao is null)
and (
	(nvl(ie_is_batch_release_p,'N') = 'N' AND trunc(d.dt_prevista) = trunc(dt_prevista_p))
	or nvl(ie_is_batch_release_p,'N') = 'S'
)
and d.nr_sequencia = a.nr_sequencia
and b.nr_sequencia = nr_seq_prot_int_paciente_p;

begin

  ds_nr_seq_pend_pac_w := '';
  ds_nr_seq_pend_pac_aux_w := '';

  for c01_w in c01 loop
  begin

    GQA_LIBERACAO_PROT_INT_EVENTO(c01_w.nr_seq_evento, nm_usuario_p, nr_atendimento_p, ds_nr_seq_pend_pac_aux_w);

    ds_nr_seq_pend_pac_w := concat(ds_nr_seq_pend_pac_w,ds_nr_seq_pend_pac_aux_w || ',');

  end;
  end loop;
		
commit;

ds_nr_seq_pend_pac_p := rtrim(ds_nr_seq_pend_pac_w,',');

end release_multiple_events_gqa;
/
