create or replace
procedure dl_gerar_titulo_pagar_distrib(
			nr_seq_lote_p		number,	
			nr_seq_trans_fin_p	varchar2,
			nm_usuario_p		Varchar2) is 

cd_estabelecimento_w		number(4);
cd_estab_financeiro_w		number(4);
nr_seq_distribuicao_w		number(10);
nr_adiantamento_pago_w		number(10);
cd_pessoa_fisica_socio_w	varchar(10);
dt_mesano_ref_w			date; 		
dt_vencimento_w			date;
vl_distribuicao_w		number(15,2);
nr_titulo_pagar_w		number(15,2);

Cursor C01 is
	select	substr(dl_obter_dados_socio(b.nr_seq_socio, 'PF'),1,255),
		a.dt_mesano_ref, 
		b.dt_vencimento,
		b.nr_sequencia,
		nvl(dl_obter_dados_distribuicao(b.nr_sequencia,'A'),null),
		b.vl_distribuicao
	from	dl_lote_distribuicao a,
		dl_distribuicao b
	where	a.nr_sequencia		= nr_seq_lote_p		
	and	a.nr_sequencia		= b.nr_seq_lote
	and	a.dt_fechamento is not null
	and	a.cd_estabelecimento	= cd_estabelecimento_w
	and 	nvl(dl_obter_dados_distribuicao(b.nr_sequencia,'T'),null) is null;
begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	dl_lote_distribuicao
where	nr_sequencia	= nr_seq_lote_p;

select	nvl(cd_estab_financeiro, cd_estabelecimento)
into	cd_estab_financeiro_w
from	estabelecimento
where	cd_estabelecimento	= cd_estabelecimento_w;

open C01;
loop
fetch C01 into	
	cd_pessoa_fisica_socio_w,
	dt_mesano_ref_w,
	dt_vencimento_w,
	nr_seq_distribuicao_w,
	nr_adiantamento_pago_w,
	vl_distribuicao_w;
exit when C01%notfound;
	begin
	
	select	titulo_pagar_seq.nextval
	into	nr_titulo_pagar_w
	from	dual;

	insert into titulo_pagar (	
		nr_titulo,                      
		cd_estabelecimento,             
		dt_atualizacao,                 
		nm_usuario,                     
		dt_emissao,                     
		dt_vencimento_original,         
		dt_vencimento_atual,            
		vl_titulo,                     
		vl_saldo_titulo,                
		vl_saldo_juros,                 
		vl_saldo_multa,                 
		cd_moeda,                       
		tx_juros,                       
		tx_multa,                       
		cd_tipo_taxa_juro,              
		cd_tipo_taxa_multa,             
		tx_desc_antecipacao,            
		dt_limite_antecipacao,          
		vl_dia_antecipacao,             
		cd_tipo_taxa_antecipacao,       
		ie_situacao,                    
		ie_origem_titulo,               
		ie_tipo_titulo,                 
		nr_seq_nota_fiscal,             
		cd_pessoa_fisica,               
		cd_cgc,                         
		nr_documento,                   
		nr_bloqueto,                    
		dt_liquidacao,                  
		nr_lote_contabil,              
		ds_observacao_titulo,           
		nr_bordero,                     
		vl_bordero,                     
		vl_juros_bordero,               
		vl_multa_bordero,               
		vl_desconto_bordero,            
		ie_desconto_dia,                
		nr_titulo_original,             
		nr_parcelas,                    
		nr_total_parcelas ,             
		dt_contabil,                    
		vl_ir,                          
		nr_seq_trans_fin_baixa,         
		dt_liberacao,                   
		nm_usuario_lib,                 
		nr_seq_rpa,                     
		nr_repasse_terceiro,            
		nr_nosso_numero,                
		vl_imposto_munic,               
		vl_inss,                        
		nr_seq_trans_fin_contab,        
		nr_seq_tributo,                 
		vl_out_desp_bordero,            
		ds_compl_contab,                
		dt_inclusao,                    
		nm_usuario_orig,                
		dt_integracao_externa,          
		ie_status_tributo,              
		nr_lote_transf_trib,            
		nr_adiant_pago,                 
		nr_seq_contrato,                
		cd_tributo,                     
		cd_variacao,                    
		ie_periodicidade,               
		cd_estab_financeiro,            
		nr_titulo_externo,              
		vl_bloq_associado,              
		vl_bloq_juros,                  
		vl_bloq_desc,                   
		ie_pls,                        
		nr_seq_prot_conta,              
		cd_tipo_baixa_neg,              
		cd_darf,                        
		cd_banco_portador,              
		vl_outros_acrescimos,           
		nr_seq_reembolso,               
		nr_seq_lote_res_pls,            
		nr_adiant_rec,                  
		nr_seq_adiant_dev,              
		nr_titulo_transf,               
		ie_bloqueto,                    
		nr_seq_proj_rec,                
		nr_seq_processo,                
		nr_seq_taxa_saude,              
		nr_seq_lote_audit,              
		nr_seq_provisao,                
		nr_seq_distribuicao,            
		nr_seq_proj_gpi,                
		nr_seq_etapa_gpi,               
		nr_seq_lote_audit_hist,
		ie_status)
	values(	nr_titulo_pagar_w,                      
		cd_estabelecimento_w,             
		sysdate,                 
		nm_usuario_p,                     
		sysdate,                     
		sysdate,	--dt_vencimento_original,         
		sysdate, 	--dt_vencimento_atual,            
		vl_distribuicao_w,                     
		vl_distribuicao_w,                
		0,                 
		0,                 
		1,                       
		0,                       
		0,                       
		1,              
		1,             
		0,            
		null, 	--dt_limite_antecipacao,          
		0, 	--vl_dia_antecipacao,             
		null, 	--cd_tipo_taxa_antecipacao,       
		'A',                    
		13, 	-- distribuicao lucros               
		10, 	-- diversos                 
		null,             
		cd_pessoa_fisica_socio_w,               
		null, 	--cd_cgc,                         
		nr_seq_distribuicao_w,                   
		null, 	--nr_bloqueto,                    
		null, 	--dt_liquidacao,                  
		null, 	--nr_lote_contabil,              
		null, 	--ds_observacao_titulo,           
		null, 	--nr_bordero,                     
		0, 	--vl_bordero,                     
		0, 	--vl_juros_bordero,               
		0, 	--vl_multa_bordero,               
		0, 	--vl_desconto_bordero,            
		'N',                
		null, 	--nr_titulo_original,             
		null, 	--nr_parcelas,                    
		null, 	--nr_total_parcelas ,             
		null, 	--dt_contabil,                    
		0, 	--vl_ir                         
		nr_seq_trans_fin_p,
		null, 	--dt_liberacao,                   
		null,                 
		null,                     
		null, 	--nr_repasse_terceiro,            
		null, 	--nr_nosso_numero,                
		0, 	--vl_imposto_munic,              
		0, 	--vl_inss,                        
		null, 	--nr_seq_trans_fin_contab,        
		null, 	--nr_seq_tributo,                 
		0, 	--vl_out_desp_bordero,            
		null, 	--ds_compl_contab,                
		null, 	--dt_inclusao,                    
		null,                
		null, 	--dt_integracao_externa,          
		'N',              
		0,            
		nr_adiantamento_pago_w,
		null, 	--nr_seq_contrato,                
		null, 	--cd_tributo,                     
		null, 	--cd_variacao,                    
		null, 	--ie_periodicidade,               
		cd_estab_financeiro_w,            
		null, 	--nr_titulo_externo,              
		0, 	--vl_bloq_associado,              
		0, 	--vl_bloq_juros,                  
		0, 	--vl_bloq_desc,                   
		null, 	--ie_pls,                        
		null, 	--nr_seq_prot_conta,              
		null, 	--cd_tipo_baixa_neg,              
		null, 	--cd_darf,                        
		null, 	--cd_banco_portador,              
		0, 	--vl_outros_acrescimos,           
		null, 	--nr_seq_reembolso,               
		null, 	--nr_seq_lote_res_pls,            
		null, 	--nr_adiant_rec,                  
		null, 	--nr_seq_adiant_dev,              
		null, 	--nr_titulo_transf,               
		null, 	--ie_bloqueto,                    
		null, 	--nr_seq_proj_rec,                
		null, 	--nr_seq_processo,                
		null, 	--nr_seq_taxa_saude,              
		null, 	--nr_seq_lote_audit,              
		null, 	--nr_seq_provisao,                
		nr_seq_distribuicao_w,            
		null, 	--nr_seq_proj_gpi,                
		null, 	--nr_seq_etapa_gpi,               
		null, --nr_seq_lote_audit_hist
		'D'); 	
	ATUALIZAR_INCLUSAO_TIT_PAGAR(nr_titulo_pagar_w, nm_usuario_p);		
		
	end;
end loop;
close C01;

commit;

end dl_gerar_titulo_pagar_distrib;
/