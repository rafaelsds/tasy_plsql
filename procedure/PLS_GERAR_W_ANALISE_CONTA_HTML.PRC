create or replace
procedure pls_gerar_w_analise_conta_html
			(	nr_seq_analise_p	number,
				nr_seq_conta_p		number,
				nr_seq_grupo_p		number,
				ie_minha_analise_p	varchar2,
				ie_pendentes_p		varchar2,
				nm_usuario_p		varchar2,
				ie_somente_ocor_p	varchar2 default 'N',
				nr_id_transacao_p	w_pls_analise_item.nr_id_transacao%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar as linhas de conta na tabela tempor�ria (An�lise Nova)
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ie_status_w			varchar2(255);
ds_tipo_guia_w			varchar2(200);
cd_guia_w			varchar2(30);
ie_status_analise_w		varchar2(10);
ie_pend_grupo_w			varchar2(10)	:= null;
ie_nao_finalizar_w		varchar2(3);
ie_tipo_guia_w			varchar2(2);
ie_selecionado_w		varchar2(1)	:= 'N';
ie_minha_analise_w		varchar2(1);
ie_pendentes_w			varchar2(1);
ie_sem_fluxo_w			varchar2(1)	:= null;
nr_sequencia_w			number(10);
qt_glo_ocorr_w			number(10)	:= 0;
qt_selecao_w			number(10);
qt_inf_guia_w			number(10)	:= 0;
nr_nivel_w			number(10);
qt_exp_w			number(10);
ie_expandido_w			varchar2(1)	:= 'N';
ds_ident_w			varchar2(30);
qt_contas_guia_w		number(10);
ie_pcmso_w			varchar2(1);
qt_somente_ocor_w		pls_integer := 0;
nr_seq_guia_w			pls_conta.nr_seq_guia%type;

begin
if	(nr_seq_analise_p is not null) and
	(nr_seq_conta_p is not null) then
	ie_minha_analise_w	:= nvl(ie_minha_analise_p,'N');
	ie_pendentes_w		:= nvl(ie_pendentes_p,'N');
	ie_pend_grupo_w		:= pls_obter_pend_grupo_analise(nr_seq_analise_p,nr_seq_conta_p, null, null, null, nr_seq_grupo_p, 'N');
	nr_nivel_w		:= pls_consulta_analise_pck.get_nr_nivel;
	
	select	ie_pcmso
	into	ie_pcmso_w
	from	pls_analise_conta
	where	nr_sequencia	= nr_seq_analise_p;

	select	max(a.ie_status),
		max(a.nr_seq_guia)
	into	ie_status_w,
		nr_seq_guia_w
	from	pls_conta	a
	where	a.nr_sequencia	= nr_seq_conta_p;

	if	(ie_somente_ocor_p = 'S') then

		select	count(1)
		into	qt_somente_ocor_w
		from	dual
		where	exists(	select	1
				from	pls_conta_glosa
				where	ie_situacao	= 'A'
				and	nr_seq_conta	= nr_seq_conta_p
				and	nr_seq_conta_proc is null
				and	nr_seq_conta_mat is null
				and	nr_seq_proc_partic is null
				union all
				select	1
				from	pls_ocorrencia_benef
				where	ie_situacao	= 'A'
				and	nr_seq_conta	= nr_seq_conta_p
				and	nr_seq_conta_proc is null
				and	nr_seq_conta_mat is null
				and	nr_seq_proc_partic is null);
	end if;

	if	((ie_minha_analise_w = 'N' and ie_pendentes_w = 'N') or
		(ie_minha_analise_w = 'S' and ie_pend_grupo_w is not null) or
		(ie_pendentes_w = 'S' and ie_pend_grupo_w = 'S')) and
		(ie_status_w <> 'C') and
		( (ie_somente_ocor_p = 'N') or (ie_somente_ocor_p = 'S' and qt_somente_ocor_w > 0))then

		/* Se for incluir a linha, verificar o tipo de guia, se for SP/SADT ou Honor�rio, criar linha agrupadora */
		ie_tipo_guia_w	:= pls_consulta_analise_pck.get_ie_tipo_guia;

		qt_inf_guia_w	:= 0;
		if	(ie_tipo_guia_w in ('4','6')) then
			select	count(1)
			into	qt_inf_guia_w
			from	w_pls_analise_item a
			where	a.nr_seq_analise	= nr_seq_analise_p
			and	a.ie_tipo_linha = 'C'
			and	a.ie_tipo_guia	= ie_tipo_guia_w
			and	a.ie_informativo = 'S';

			select	count(1)
			into	qt_contas_guia_w
			from	pls_conta a
			where	a.nr_seq_analise	= nr_seq_analise_p
			and	a.ie_tipo_guia		= ie_tipo_guia_w;

			if	(qt_inf_guia_w = 0) and
				(qt_contas_guia_w > 1) then
				nr_sequencia_w	:= pls_consulta_analise_pck.get_nr_seq_item;
				if	(ie_tipo_guia_w = '4') then
					ds_tipo_guia_w	:= 'Guias de SP/SADT';
				elsif	(ie_tipo_guia_w = '6') then
					ds_tipo_guia_w	:= 'Guias de Honor�rio Individual';
				end if;
				ie_status_analise_w	:= pls_obter_status_analise_item(nr_seq_analise_p,nr_seq_conta_p, null, null, null,null,'S');

				select	count(1)
				into	qt_selecao_w
				from	w_pls_analise_selecao_item	a
				where	a.nr_seq_analise	= nr_seq_analise_p
				and	a.nr_seq_w_item		= nr_sequencia_w;

				if	(qt_selecao_w > 0) then
					ie_selecionado_w	:= 'S';
				else
					ie_selecionado_w	:= 'N';
				end if;

				select	decode(count(1),0,'N','S')
				into	ie_expandido_w
				from	w_pls_analise_item_exp a
				where	a.nr_seq_analise	= nr_seq_analise_p
				and	a.ie_tipo_guia		= ie_tipo_guia_w
				and	a.nm_usuario		= nm_usuario_p
				and	a.nr_seq_conta_proc is null;

				/* Linha agrupadora */
				insert into w_pls_analise_item
					(nr_sequencia,
					nm_usuario,
					dt_atualizacao,
					nm_usuario_nrec,
					dt_atualizacao_nrec,
					nr_seq_analise,
					nr_seq_conta,
					ie_tipo_linha,
					ie_tipo_item,
					ie_tipo_despesa,
					ds_item,
					ie_status_analise,
					ie_selecionado,
					ie_pend_grupo,
					ie_sem_fluxo,
					ie_tipo_guia,
					ie_expandido,
					ie_informativo,
					ie_pcmso,
					nr_id_transacao,
					nr_seq_guia,
					nr_seq_protocolo)
				values	(nr_sequencia_w,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					sysdate,
					nr_seq_analise_p,
					null,
					'C',
					'CO',
					'CON',
					ds_tipo_guia_w,
					ie_status_analise_w,
					ie_selecionado_w,
					ie_pend_grupo_w,
					ie_sem_fluxo_w,
					ie_tipo_guia_w,
					ie_expandido_w,
					'S',
					ie_pcmso_w,
					nr_id_transacao_p,
					nr_seq_guia_w,
					pls_consulta_analise_pck.get_nr_seq_protocolo);

				qt_inf_guia_w	:= 1;

				pls_consulta_analise_pck.set_nr_seq_item(nr_sequencia_w + 1);
			end if;
		end if;

		select	count(1)
		into	qt_exp_w
		from	w_pls_analise_item_exp a
		where	a.nr_seq_analise	= nr_seq_analise_p
		and	a.ie_tipo_guia		= ie_tipo_guia_w
		and	a.nm_usuario		= nm_usuario_p
		and	a.nr_seq_conta_proc is null;

		if	(qt_inf_guia_w = 0) or
			((nr_nivel_w > 2) or (qt_exp_w > 0)) then
			nr_sequencia_w	:= pls_consulta_analise_pck.get_nr_seq_item;
			cd_guia_w	:= pls_consulta_analise_pck.get_cd_guia;
			ds_tipo_guia_w	:= pls_consulta_analise_pck.get_ds_tipo_guia;
			ie_tipo_guia_w	:= pls_consulta_analise_pck.get_ie_tipo_guia;

			ds_ident_w	:= null;
			if	(cd_guia_w is null) then
				cd_guia_w	:= 'Seq: ' || nr_seq_conta_p;
			end if;

			if	(qt_inf_guia_w > 0) then
				ds_ident_w	:= '      ';
			end if;

			/* Status geral da an�lise */
			ie_status_analise_w	:= pls_obter_status_analise_item(nr_seq_analise_p,nr_seq_conta_p, null, null, null,null,'N');
	
			/* Obter se possui fluxo de an�lise */
			ie_sem_fluxo_w		:= pls_obter_se_item_sem_fluxo(nr_seq_analise_p,nr_seq_conta_p, null, null,null);

			select	count(1)
			into	qt_selecao_w
			from	w_pls_analise_selecao_item	a
			where	a.nr_seq_analise	= nr_seq_analise_p
			and	a.nr_seq_w_item		= nr_sequencia_w;

			if	(qt_selecao_w > 0) then
				ie_selecionado_w	:= 'S';
			else
				ie_selecionado_w	:= 'N';
			end if;

			insert into w_pls_analise_item
				(nr_sequencia,
				nm_usuario,
				dt_atualizacao,
				nm_usuario_nrec,
				dt_atualizacao_nrec,
				nr_seq_analise,
				nr_seq_conta,
				ie_tipo_linha,
				ie_tipo_item,
				ie_tipo_despesa,
				ds_item,
				ie_status_analise,
				ie_selecionado,
				ie_pend_grupo,
				ie_sem_fluxo,
				ie_tipo_guia,
				ie_pcmso,
				nr_id_transacao,
				ds_tipo_guia,
				cd_guia,
				nm_prestador_solic,
				nm_prestador_exec,
				nr_seq_prestador_exec,
				nr_seq_guia,
				nr_seq_protocolo)
			values	(nr_sequencia_w,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				sysdate,
				nr_seq_analise_p,
				nr_seq_conta_p,
				'C',
				'CO',
				'CON',
				ds_ident_w || cd_guia_w || ' - ' || ds_tipo_guia_w,
				ie_status_analise_w,
				ie_selecionado_w,
				ie_pend_grupo_w,
				ie_sem_fluxo_w,
				ie_tipo_guia_w,
				ie_pcmso_w,
				nr_id_transacao_p,
				ds_tipo_guia_w,
				pls_consulta_analise_pck.get_cd_guia,
				pls_consulta_analise_pck.get_nm_prestador_solic,
				pls_consulta_analise_pck.get_nm_prestador_exec,
				pls_consulta_analise_pck.get_nr_seq_prestador_exec,				
				nr_seq_guia_w,
				pls_consulta_analise_pck.get_nr_seq_protocolo);

			pls_consulta_analise_pck.set_nr_seq_item(nr_sequencia_w + 1);
		end if;

	end if;
end if;

/* Interna, sem commit */

end pls_gerar_w_analise_conta_html;
/