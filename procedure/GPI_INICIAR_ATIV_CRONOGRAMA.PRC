create or replace
procedure gpi_iniciar_ativ_cronograma(	nr_sequencia_p	number,
				dt_inicio_real_p	date,
				nm_usuario_p	Varchar2) is

begin

if	(trunc(nvl(dt_inicio_real_p,sysdate)) > trunc(sysdate)) then
	/*A data de in�cio da atividade n�o pode ser superior a data atual!*/
	wheb_mensagem_pck.exibir_mensagem_abort(266519);
end if;
update	gpi_cron_etapa
set	dt_inicio_real	= nvl(dt_inicio_real_p,sysdate),
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_sequencia_p;

gpi_atualizar_etapa_superior(nr_sequencia_p,nm_usuario_p);

commit;

end gpi_iniciar_ativ_cronograma;
/
