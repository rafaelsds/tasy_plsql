create or replace
procedure pls_obter_dados_operadora
		(	cd_estabelecimento_p	in	Number,
			nm_usuario_p		in	Varchar2,
			cd_cgc_p		out	Varchar2,
			cd_registro_ans_p	out	Varchar2) is 
			
cd_cgc_outorgante_w	Varchar2(14);
cd_ans_w		Varchar2(6);

begin

begin
select	cd_cgc_outorgante,
	substr(cd_ans,1,6)
into	cd_cgc_outorgante_w,
	cd_ans_w
from	pls_outorgante
where	cd_estabelecimento	= cd_estabelecimento_p;
exception
	when others then
	cd_cgc_outorgante_w	:= '';
	cd_ans_w		:= '';
end;

cd_cgc_p		:= cd_cgc_outorgante_w;
cd_registro_ans_p	:= cd_ans_w;

end pls_obter_dados_operadora;
/