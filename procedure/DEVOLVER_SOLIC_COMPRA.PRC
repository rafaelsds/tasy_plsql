create or replace
procedure devolver_solic_compra(		nr_cot_compra_p		number,
					ds_observacao_p		varchar2,
					nm_usuario_p		varchar2) is 

					
nr_solic_compra_w			number(10);
nr_seq_aprovacao_w		number(10);
cd_estabelecimento_w		number(10);
nr_seq_regra_w			number(10);
ds_email_destino_w			varchar2(2000);
ie_momento_envio_w		varchar2(1);
ie_usuario_w			varchar2(15);
cd_comprador_w			varchar2(10);
ds_email_origem_w			varchar2(255);
ds_usuario_origem_w		varchar2(255);
ds_email_comprador_w		varchar2(255);
ds_usuario_comprador_w		comprador.nm_guerra%type;
ds_pessoa_solic_w			varchar2(100);
ds_assunto_w			varchar2(80);
ds_mensagem_w			varchar2(4000);
ds_email_adicional_w		varchar2(2000);
ds_email_pessoa_solic_w		varchar2(255);
cd_setor_atendimento_w		number(5);
cd_evento_w			number(3);
cd_perfil_w			varchar2(10);
qt_regra_usuario_w			number(10);
ds_setor_adicional_w		varchar2(2000) := '';
cd_setor_regra_usuario_w		number(5);
ds_destinatario_w			varchar2(255);
nr_seq_classif_w			number(10);
nr_sequencia_w			number(10);
ds_observacao_w			varchar2(4000);
ds_email_remetente_w		varchar2(255);
qt_registros_w			number(10);
nr_cot_compra_w			cot_compra.nr_cot_compra%type;

cursor c01 is
select	distinct
	nr_solic_compra
from	(
	select	nr_solic_compra
	from	cot_compra_item
	where	nr_cot_compra = nr_cot_compra_p
	and	nr_solic_compra is not null
	union all
	select	nr_solic_compra
	from	cot_compra_solic_agrup
	where	nr_cot_compra = nr_cot_compra_p
	and	nr_solic_compra is not null);

cursor c02 is
Select	distinct nr_seq_aprovacao
from	solic_compra_item
where	nr_solic_compra	= nr_solic_compra_w;

cursor c03 is
select	nr_sequencia,
	nvl(ds_email_remetente,'X'),
	replace(ds_email_adicional,',',';'),
	nvl(ie_momento_envio,'I'),
	ie_usuario
from	regra_envio_email_compra
where	ie_tipo_mensagem = 60
and	ie_situacao = 'A'
and	cd_estabelecimento = cd_estabelecimento_w;

cursor c04 is
select	b.nr_sequencia,
	b.cd_evento,
	b.cd_perfil
from	regra_envio_comunic_evento b,
	regra_envio_comunic_compra a
where	a.nr_sequencia = b.nr_seq_regra
and	a.cd_estabelecimento = cd_estabelecimento_w
and	b.cd_evento = 52
and	b.ie_situacao = 'A'
and	(((cd_setor_atendimento_w is not null) and (b.cd_setor_destino = cd_setor_atendimento_w)) or
	((cd_setor_atendimento_w is null) and (b.cd_setor_destino is null)) or (b.cd_setor_destino is null))
and	substr(obter_se_envia_ci_regra_compra(b.nr_sequencia,nr_cot_compra_p,'CC',obter_perfil_ativo,nm_usuario_p,null),1,1) = 'S';
	
Cursor c05 is
select	nvl(a.cd_setor_atendimento,0) cd_setor_atendimento
from	regra_envio_comunic_usu a
where	a.nr_seq_evento = nr_seq_regra_w;

cursor c06 is
select	distinct
	nr_cot_compra
from	solic_compra_agrup_v
where	nr_solic_compra = nr_solic_compra_w
and	nr_cot_compra <> nr_cot_compra_p;
	

begin

select	cd_estabelecimento
into	cd_estabelecimento_w
from	cot_compra
where	nr_cot_compra = nr_cot_compra_p;

open C01;
loop
fetch C01 into	
	nr_solic_compra_w;
exit when C01%notfound;
	begin
	
	open C06;
	loop
	fetch C06 into	
		nr_cot_compra_w;
	exit when C06%notfound;
		begin
		
		select	count(*)
		into	qt_registros_w
		from	ordem_compra_item
		where	nr_cot_compra = nr_cot_compra_w;

		if	(qt_registros_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(954141);
			/*N�o � poss�vel devolver essa solicita��o de compras, pois ela j� est� vinculada com uma ordem de compra.*/
		end if;
		end;
	end loop;
	close C06;
	
	
	update	solic_compra
	set	dt_liberacao		= null,
		dt_pre_liberacao		= null,
		nm_usuario_lib		= '',
		nm_usuario_pre_lib		= '',
		dt_autorizacao		= null,
		dt_baixa			= null
	where	nr_solic_compra		= nr_solic_compra_w;
	
	delete from reg_lic_item_solic
	where nr_solic_compra = nr_solic_compra_w;

	open C02;
	loop
	fetch C02 into	
		nr_seq_aprovacao_w;
	exit when C02%notfound;
		begin
		
		delete
		from	processo_aprov_compra
		where	nr_sequencia 	= nr_seq_aprovacao_w;
		
		end;
	end loop;
	close C02;

	select	nvl(cd_comprador_resp,'0'),
		substr(obter_nome_pf(cd_pessoa_solicitante),1,100),
		substr(obter_dados_usuario_opcao(obter_usuario_pessoa(cd_pessoa_solicitante),'E'),1,255),
		cd_setor_atendimento
	into	cd_comprador_w,
		ds_pessoa_solic_w,
		ds_email_pessoa_solic_w,
		cd_setor_atendimento_w
	from	solic_compra
	where	nr_solic_compra = nr_solic_compra_w;
	
	update	solic_compra_item
	set	nr_seq_aprovacao	= null,
		dt_autorizacao		= null
	where	nr_solic_compra 	= nr_solic_compra_w;
	
	gerar_historico_solic_compra(
			nr_solic_compra_w,
			WHEB_MENSAGEM_PCK.get_texto(299538),
			WHEB_MENSAGEM_PCK.get_texto(299539),
			'R',
			nm_usuario_p);
	
	sup_cancela_email_pendente(null,nr_solic_compra_w,'SC',cd_estabelecimento_w,nm_usuario_p);

	ds_observacao_w := substr(WHEB_MENSAGEM_PCK.get_texto(299540,'DS_OBSERVACAO_P='|| ds_observacao_p),1,4000);
	
	gerar_hist_solic_sem_commit(
		nr_solic_compra_w,
		WHEB_MENSAGEM_PCK.get_texto(299542),
		ds_observacao_w,
		'DE',
		nm_usuario_p);
	
	
	/*Envia e-mail*/
	open C03;
	loop
	fetch C03 into	
		nr_seq_regra_w,
		ds_email_remetente_w,
		ds_email_adicional_w,
		ie_momento_envio_w,
		ie_usuario_w;
	exit when C03%notfound;
		begin
		if	(ie_usuario_w = 'U') then --Usuario
			begin
			select	ds_email,
				nm_usuario
			into	ds_email_origem_w,
				ds_usuario_origem_w
			from	usuario
			where	nm_usuario = nm_usuario_p;
			end;
		elsif	(ie_usuario_w = 'C') then --Setor compras
			begin
			select	ds_email
			into	ds_email_origem_w
			from	parametro_compras
			where	cd_estabelecimento = cd_estabelecimento_w;

			select	nvl(ds_fantasia,ds_razao_social)
			into	ds_usuario_origem_w
			from	estabelecimento_v
			where	cd_estabelecimento = cd_estabelecimento_w;
			end;
		elsif	(ie_usuario_w = 'O') then --Comprador
			begin

			select	max(ds_email),
				max(nm_guerra)
			into	ds_email_comprador_w,
				ds_usuario_comprador_w
			from	comprador
			where	cd_pessoa_fisica = cd_comprador_w
			and	cd_estabelecimento = cd_estabelecimento_w;

			ds_email_origem_w	:= ds_email_comprador_w;
			ds_usuario_origem_w	:= ds_usuario_comprador_w;
			end;
		end if;

		if	(ds_email_remetente_w <> 'X') then
			ds_email_origem_w	:= ds_email_remetente_w;
		end if;
		
		ds_email_destino_w	:= ds_email_pessoa_solic_w;
		
		if	(ds_email_adicional_w is not null) then
			ds_email_destino_w	:= ds_email_pessoa_solic_w || '; ' || ds_email_adicional_w;
		end if;
		
		select	substr(ds_assunto,1,80),			
			ds_mensagem_padrao
		into	ds_assunto_w,
			ds_mensagem_w
		from	regra_envio_email_compra
		where	nr_sequencia = nr_seq_regra_w;
		
		ds_mensagem_w := SUBSTR(replace_macro(ds_mensagem_w,'@nm_solicitante', ds_pessoa_solic_w),1,4000); 
		ds_mensagem_w := SUBSTR(replace_macro(ds_mensagem_W,'@ds_observacao', ds_observacao_p),1,4000); 
		ds_mensagem_w := SUBSTR(replace_macro(ds_mensagem_W,'@nr_solic_compra', nr_solic_compra_w),1,4000); 
		ds_mensagem_w := ds_mensagem_w; 				 	
		
		if	(ie_momento_envio_w = 'A') then
			begin
			
			sup_grava_envio_email(
				'SC',
				'60',
				nr_solic_compra_w,
				null,
				null,
				ds_email_destino_w,
				ds_usuario_origem_w,
				ds_email_origem_w,
				ds_assunto_w,
				ds_mensagem_w,
				cd_estabelecimento_w,
				nm_usuario_p);

			end;
		else
			begin
			enviar_email(
				ds_assunto_w,
				ds_mensagem_w,
				ds_email_origem_w,
				ds_email_destino_w,
				ds_usuario_origem_w,
				'M');
			exception
			when others then
				/*gravar__log__tasy(91301,'Falha ao enviar e-mail compras - Evento: 60 - Seq. Regra: ' || nr_seq_regra_w, nm_usuario_p);*/
				gerar_hist_solic_sem_commit(
					nr_solic_compra_w,
					WHEB_MENSAGEM_PCK.get_texto(299543),
					WHEB_MENSAGEM_PCK.get_texto(299548,'NR_SEQ_REGRA_W='||nr_seq_regra_w),
					'FGE',
					nm_usuario_p);
			end;
		end if;	
		end;
	end loop;
	close C03;
	/*FIM Envia e-mail*/
	
	/*Envia CI*/
	open C04;
	loop
	fetch C04 into	
		nr_seq_regra_w,
		cd_evento_w,
		cd_perfil_w;
	exit when C04%notfound;
		begin
		
		select	count(*)
		into	qt_regra_usuario_w
		from	regra_envio_comunic_compra a,
			regra_envio_comunic_evento b,
			regra_envio_comunic_usu c
		where	a.nr_sequencia = b.nr_seq_regra
		and	b.nr_sequencia = c.nr_seq_evento
		and	b.nr_sequencia = nr_seq_regra_w;
	
		if	(nr_seq_regra_w > 0) then

			open C05;
			loop
			fetch C05 into	
				cd_setor_regra_usuario_w;
			exit when C05%notfound;
				begin
				if	(cd_setor_regra_usuario_w <> 0) and
					(obter_se_contido_char(cd_setor_regra_usuario_w, ds_setor_adicional_w) = 'N') then
					ds_setor_adicional_w := substr(ds_setor_adicional_w || cd_setor_regra_usuario_w || ',',1,2000);
				end if;
				end;
			end loop;
			close C05;		
			
			ds_destinatario_w := obter_usuarios_comunic_compras(nr_solic_compra_w,null,cd_evento_w,nr_seq_regra_w,'');
			
			select	substr(ds_titulo,1,80),
				ds_comunicacao
			into	ds_assunto_w,
				ds_mensagem_w
			from	regra_envio_comunic_evento
			where	nr_sequencia = nr_seq_regra_w;		
			
			ds_mensagem_w := SUBSTR(replace_macro(ds_mensagem_w,'@nm_solicitante', ds_pessoa_solic_w),1,4000); 
			ds_mensagem_w := SUBSTR(replace_macro(ds_mensagem_W,'@ds_observacao', ds_observacao_p),1,4000); 
			ds_mensagem_w := SUBSTR(replace_macro(ds_mensagem_W,'@nr_solic_compra', nr_solic_compra_w),1,4000); 
			ds_mensagem_w := ds_mensagem_w;  				 				

			select	obter_classif_comunic('F')
			into	nr_seq_classif_w
			from	dual;

			select	comunic_interna_seq.nextval
			into	nr_sequencia_w
			from	dual;
			
			if	(cd_perfil_w is not null) then
				cd_perfil_w := cd_perfil_w ||',';
			end if;

			insert	into comunic_interna(
					dt_comunicado,
					ds_titulo,
					ds_comunicado,
					nm_usuario,
					dt_atualizacao,
					ie_geral,
					nm_usuario_destino,
					nr_sequencia,
					ie_gerencial,
					nr_seq_classif,
					dt_liberacao,
					ds_perfil_adicional,
					ds_setor_adicional)
			values(		sysdate,
					ds_assunto_w,
					ds_mensagem_w,
					WHEB_MENSAGEM_PCK.get_texto(299549),
					sysdate,
					'N',
					ds_destinatario_w,
					nr_sequencia_w,
					'N',
					nr_seq_classif_w,
					sysdate,
					cd_perfil_w,
					ds_setor_adicional_w);
		end if;
		end;
	end loop;
	close C04;
	/*FIM envio CI*/
	end;
end loop;
close C01;

begin
delete from cot_compra
where nr_cot_compra = nr_cot_compra_p;
exception
when others then
	wheb_mensagem_pck.exibir_mensagem_abort(188146);
end;


commit;

end devolver_solic_compra;
/
