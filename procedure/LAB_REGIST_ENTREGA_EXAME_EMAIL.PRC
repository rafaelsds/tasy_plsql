create or replace
procedure lab_regist_entrega_exame_email(	seq_prescr_p		number,
				nr_prescricao_p		number,
				ds_email_p		varchar,			
				dt_entrega_p		date,
				nm_usuario_p		Varchar2) is
					
nr_seq_prescr_w		number(5);
nr_seq_entrega_w	number(10);					

ieParametro334		varchar2(1);

ie_ds_observacao_obrig_w		varchar2(1);

cursor c01 is
select	nr_sequencia
from	prescr_procedimento
where	nr_prescricao = nr_prescricao_p
and	ie_status_atend > 30
and	((seq_prescr_p = nr_sequencia) or (seq_prescr_p is null));

begin

ieParametro334	:= obter_valor_param_usuario(722, 334, obter_perfil_ativo, nm_usuario_p, obter_estabelecimento_ativo);

open c01;
loop
fetch c01 into
	nr_seq_prescr_w;
exit when c01%notfound;

	select 	lab_entrega_exame_seq.nextval 
	into	nr_seq_entrega_w
	from 	dual;
	
	insert	into lab_entrega_exame(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_prescricao,
		nr_seq_prescr,			
		ds_email,
		dt_entrega,
		ie_situacao)
	values(
		nr_seq_entrega_w,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_prescricao_p,
		nr_seq_prescr_w,				
		substr(nvl(ds_email_p, ''), 1, 255),
		dt_entrega_p,
		'A');
	
end loop; 
close c01;		

commit;

end lab_regist_entrega_exame_email;
/