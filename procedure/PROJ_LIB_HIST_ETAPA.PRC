create or replace
procedure proj_lib_hist_etapa( 	nr_sequencia_p 		number,
								nm_usuario_p		Varchar2) is 

begin
if	(nr_sequencia_p is not null) then
	update	PROJ_CRON_ETAPA_HIST
	set		DT_LIBERACAO = sysdate
	where	nr_sequencia = nr_Sequencia_p;
end if;

commit;

end proj_lib_hist_etapa;
/