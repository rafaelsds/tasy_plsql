CREATE OR REPLACE procedure TASY.hmsl_integracao_atendimento_v2(cd_estabelecimento_p         number) is

arq_texto_da_w            utl_file.file_type;
arq_texto_ca_w            utl_file.file_type;
arq_texto_mp_w            utl_file.file_type;
arq_texto_ap_w            utl_file.file_type;
nm_local_origem_w        varchar2(60) :=  '';          --Caminho onde ser salvo o arquivo   **** CAMINHO VLIDO, HABILITADO NO ORACLE ****
nm_local_grav_w            varchar2(60) :=  '';          --Caminho onde ser salvo o arquivo   **** CAMINHO VLIDO, HABILITADO NO ORACLE ****
nm_arquivo_orig_w        varchar2(255);
nm_arquivo_grav_w        varchar2(255);
qt_tentativas_w            number(10) := 0;
ie_aberto_w            varchar2(1);
ie_eof_w            varchar2(1);
ds_line_da_w            varchar2(4000);
ds_line_ca_w            varchar2(4000);
ds_line_mp_w            varchar2(4000);
ds_line_ap_w            varchar2(4000);
nr_pos_inicio_w            number(10);
cd_unidade_basica_w        varchar2(20);
cd_estabelecimento_w        number(5);
nr_atendimento_w        number(10);
cd_motivo_alta_w        number(5);
cd_setor_atendimento_w        number(5);
ie_tipo_atendimento_w        number(3);
dt_alteracao_w            date;
qt_pos_separador_w        number(10);
i                number(2) := 0;
nm_arquivo_orig_da_w        varchar2(255);
nm_arquivo_orig_ca_w        varchar2(255);
nm_arquivo_orig_mp_w        varchar2(255);
nm_arquivo_orig_ap_w        varchar2(255);
j                varchar2(2);
qt_min                date;
qt_min_inicio            date;
cd_setor_atual_w        number(5);
cd_unidade_atual_w        varchar2(10);


begin


select     '/tmp/',
    '/tmp/'
into    nm_local_origem_w,
    nm_local_grav_w
from     dual;

if    (cd_estabelecimento_p = 13) then
    begin
    select    '/transfer/hpmorumbi/importar/',
        '/transfer/hpmorumbi/importados/'
    into    nm_local_origem_w,
        nm_local_grav_w
    from    dual;
    end;
end if;

if    (cd_estabelecimento_p = 14) then
    begin
    select    '/transfer/hpitaim/importar/',
        '/transfer/hpitaim/importados/'
    into    nm_local_origem_w,
        nm_local_grav_w
    from    dual;
    end;
end if;

ie_aberto_w := 'N';

nm_arquivo_orig_da_w := 'da.txt';
nm_arquivo_orig_ca_w := 'ca.txt';
nm_arquivo_orig_mp_w := 'mp.txt';
nm_arquivo_orig_ap_w := 'ap.txt';

    /*---------- MOVIMENTAÇÃO PACIENTE ---------*/


    /*Copiar arquivo*/

        while   (ie_aberto_w = 'N') and (qt_tentativas_w < 80) loop
        begin
            qt_tentativas_w := qt_tentativas_w + 1;
            utl_file.fcopy(nm_local_origem_w, nm_arquivo_orig_mp_w, nm_local_origem_w, 'w'||nm_arquivo_orig_mp_w);
            ie_aberto_w := 'S';
        exception
            when     others then
                ie_aberto_w := 'N';
        end;
        end loop;

    /*Abrir arquivo*/

        if    (qt_tentativas_w < 80) and (ie_aberto_w = 'S') then
        begin
            arq_texto_mp_w := utl_file.fopen(nm_local_origem_w, 'w' || nm_arquivo_orig_mp_w, 'R');
            ie_eof_w := 'N';

            while (ie_eof_w = 'N') loop
            begin
                utl_file.get_line(arq_texto_mp_w, ds_line_mp_w);
                if    (substr(ds_line_mp_w, 1, 1) in ('0','1','2','3','4','5','6','7','8','9')) then
                begin
                    if    (not ds_line_mp_w is null)    then
                    begin

                        qt_pos_separador_w     := instr(ds_line_mp_w,'|');
                        cd_estabelecimento_w    := substr(ds_line_mp_w,1,qt_pos_separador_w-1);
                        ds_line_mp_w        := substr(ds_line_mp_w,qt_pos_separador_w+1,(length(ds_line_mp_w)-qt_pos_separador_w));

                        qt_pos_separador_w     := instr(ds_line_mp_w,'|');
                        nr_atendimento_w    := trim(substr(ds_line_mp_w,1,qt_pos_separador_w-1));
                        ds_line_mp_w        := substr(ds_line_mp_w,qt_pos_separador_w+1,(length(ds_line_mp_w)-qt_pos_separador_w));

                        qt_pos_separador_w     := instr(ds_line_mp_w,'|');
                        cd_setor_atendimento_w    := trim(substr(ds_line_mp_w,1,qt_pos_separador_w-1));
                        ds_line_mp_w        := substr(ds_line_mp_w,qt_pos_separador_w+1,(length(ds_line_mp_w)-qt_pos_separador_w));

                        qt_pos_separador_w     := instr(ds_line_mp_w,'|');
                        cd_unidade_basica_w    := substr(ds_line_mp_w,1,qt_pos_separador_w-1);
                        ds_line_mp_w        := substr(ds_line_mp_w,qt_pos_separador_w+1,(length(ds_line_mp_w)-qt_pos_separador_w));

                        dt_alteracao_w        := to_date(ds_line_mp_w,'dd/mm/yyyy hh24:mi:ss');

                        ds_line_mp_w        := '';
                        begin
                        cd_unidade_atual_w := Obter_Unidade_Atendimento(hmsl_obter_atend_ficha(nr_atendimento_w),'A','NR');
                        Gerar_Transferencia_Paciente(hmsl_obter_atend_ficha(nr_atendimento_w),cd_setor_atendimento_w,cd_unidade_basica_w,' ',obter_tipo_acomod_leito(cd_setor_atendimento_w,cd_unidade_basica_w,' ','C'),0,NULL,NULL,'TASY',dt_alteracao_w);

                        update    unidade_atendimento
                        set    ie_status_unidade     = 'L',
                            nm_usuario        = 'TASY',
                            dt_atualizacao        = sysdate,
                            nr_atendimento        = ''
                        where    nr_seq_interno = cd_unidade_atual_w;

                        exception
                            when others then
                            insert into log_tasy (dt_atualizacao,
									nm_usuario,
									cd_log,
									ds_log) 
				values (sysdate,'TASY',88915,'Estab: '|| cd_estabelecimento_w || chr(13) ||
                                                  'Atend: '|| nr_atendimento_w       || chr(13) ||
                                                  'Setor:  '|| cd_setor_atendimento_w || chr(13) ||
                                                  'Unidade: '|| cd_unidade_basica_w || chr(13) ||
                                                  'Data:  '|| dt_alteracao_w || chr(13));
                            commit;
                        end;
                    end;
                    end if;
                end;
                end if;
            exception
                when NO_DATA_FOUND then
                ie_eof_w := 'S';
            end;
            end loop;

            utl_file.fclose(arq_texto_mp_w);
            utl_file.fremove(nm_local_origem_w,nm_arquivo_orig_mp_w);
            utl_file.fremove(nm_local_origem_w,'w' || nm_arquivo_orig_mp_w);

        end;
        end if;



    /* -----------    GERAR ALTA    ------------- */

        qt_tentativas_w := 0;
        ie_aberto_w := 'N';

    /* Copiar arquivo */
while   (ie_aberto_w = 'N') and (qt_tentativas_w < 80) loop
begin
    qt_tentativas_w := qt_tentativas_w + 1;
    utl_file.fcopy(nm_local_origem_w, nm_arquivo_orig_ap_w, nm_local_origem_w, 'w'||nm_arquivo_orig_ap_w);
    ie_aberto_w := 'S';
    exception
        when     others then
        ie_aberto_w := 'N';
        end;
end loop;
    /* Abrir arquivo */
    if    (qt_tentativas_w < 80) and (ie_aberto_w = 'S') then
    begin
        arq_texto_ap_w := utl_file.fopen(nm_local_origem_w, 'w' || nm_arquivo_orig_ap_w, 'R');
        ie_eof_w := 'N';

        while (ie_eof_w = 'N') loop
        begin
            utl_file.get_line(arq_texto_ap_w, ds_line_ap_w);

            if    (substr(ds_line_ap_w, 1, 1) in ('0','1','2','3','4','5','6','7','8','9')) then
            begin
                if    (not ds_line_ap_w is null)    then
                begin

                    qt_pos_separador_w     := instr(ds_line_ap_w,'|');
                    cd_estabelecimento_w    := substr(ds_line_ap_w,1,qt_pos_separador_w-1);
                    ds_line_ap_w        := substr(ds_line_ap_w,qt_pos_separador_w+1,(length(ds_line_ap_w)-qt_pos_separador_w));

                    qt_pos_separador_w     := instr(ds_line_ap_w,'|');
                    nr_atendimento_w    := trim(substr(ds_line_ap_w,1,qt_pos_separador_w-1));
                    ds_line_ap_w        := substr(ds_line_ap_w,qt_pos_separador_w+1,(length(ds_line_ap_w)-qt_pos_separador_w));

                    qt_pos_separador_w     := instr(ds_line_ap_w,'|');
                    cd_motivo_alta_w    := trim(substr(ds_line_ap_w,1,qt_pos_separador_w-1));
                    ds_line_ap_w        := substr(ds_line_ap_w,qt_pos_separador_w+1,(length(ds_line_ap_w)-qt_pos_separador_w));
                    dt_alteracao_w        := to_date(ds_line_ap_w,'dd/mm/yyyy hh24:mi:ss');

                    j := '';
                    begin
                    gerar_estornar_alta(hmsl_obter_atend_ficha(nr_atendimento_w),'A',null,cd_motivo_alta_w,dt_alteracao_w,'TASY',j,null,' ',null);
                    exception
                        when others then

                        insert into log_tasy (dt_atualizacao,
									nm_usuario,
									cd_log,
									ds_log) 
			values (sysdate,'TASY',88914,'Estab: '|| cd_estabelecimento_w || chr(13) ||
                                                  'Atend: '|| nr_atendimento_w       || chr(13) ||
                                                  'Tipo:  '|| ie_tipo_atendimento_w || chr(13) ||
                                                  'Data:  '|| dt_alteracao_w || chr(13));
                        commit;
                    end;
                end;
                end if;
            end;
            end if;

        exception
            when NO_DATA_FOUND then
            ie_eof_w := 'S';
        end;
        end loop;

        utl_file.fclose(arq_texto_ap_w);
        utl_file.fremove(nm_local_origem_w,nm_arquivo_orig_ap_w);
        utl_file.fremove(nm_local_origem_w,'w' || nm_arquivo_orig_ap_w);

    end;
    end if;

    ie_aberto_w := 'N';
    qt_tentativas_w := 0;



    /*    DESFAZER ALTA - Copiar Arquivo    */

while   (ie_aberto_w = 'N') and (qt_tentativas_w < 80) loop
begin
    qt_tentativas_w := qt_tentativas_w + 1;
    utl_file.fcopy(nm_local_origem_w, nm_arquivo_orig_da_w, nm_local_origem_w, 'w'||nm_arquivo_orig_da_w);
    ie_aberto_w := 'S';
    exception
        when     others then
            ie_aberto_w := 'N';
    end;
end loop;

    /*     Abrir arquivo    */

if    (qt_tentativas_w < 80) and (ie_aberto_w = 'S') then
begin
    arq_texto_da_w := utl_file.fopen(nm_local_origem_w, 'w'||nm_arquivo_orig_da_w, 'R');
    ie_eof_w := 'N';
    while (ie_eof_w = 'N') loop
    begin
        utl_file.get_line(arq_texto_da_w, ds_line_da_w);

        if    (substr(ds_line_da_w, 1, 1) in ('0','1','2','3','4','5','6','7','8','9')) then
        begin

            qt_pos_separador_w     := instr(ds_line_da_w,'|');
            cd_estabelecimento_w    := substr(ds_line_da_w,1,qt_pos_separador_w-1);
            ds_line_da_w        := substr(ds_line_da_w,qt_pos_separador_w+1,(length(ds_line_da_w)-qt_pos_separador_w));

            qt_pos_separador_w     := instr(ds_line_da_w,'|');
            nr_atendimento_w    := trim(substr(ds_line_da_w,1,qt_pos_separador_w-1));
            ds_line_da_w        := substr(ds_line_da_w,qt_pos_separador_w+1,(length(ds_line_da_w)-qt_pos_separador_w));

            dt_alteracao_w        := to_date(ds_line_da_w,'dd/mm/yyyy hh24:mi:ss');
            ds_line_da_w        := '';
            begin
            gerar_estornar_alta(hmsl_obter_atend_ficha(nr_atendimento_w),'E',null,null,to_date(to_char(dt_alteracao_w,'dd/mm/yyyy hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'),'TASY',ds_line_da_w,'',' ',' ');
            exception
                when others then
                insert into log_tasy (dt_atualizacao,
									nm_usuario,
									cd_log,
									ds_log) 
		values (sysdate,'TASY',88916,'Estab: '|| cd_estabelecimento_w || chr(13) ||
                                  'Atend: '|| nr_atendimento_w       || chr(13) ||
                                  'Data:  '|| dt_alteracao_w || chr(13));
                commit;
            end;

            end;
        end if;
            exception
                when NO_DATA_FOUND then
                    ie_eof_w := 'S';
    end;
    end loop;
    utl_file.fclose(arq_texto_da_w);
    utl_file.fremove(nm_local_origem_w,nm_arquivo_orig_da_w);
    utl_file.fremove(nm_local_origem_w,'w' || nm_arquivo_orig_da_w);
    end;
    end if;

    /*  -------------    CANCELAR ATENDIMENTO   ----------*/

        qt_tentativas_w := 0;
        ie_aberto_w := 'N';

    /*   Copiar Arquivo   */

        while   (ie_aberto_w = 'N') and (qt_tentativas_w < 80) loop
        begin
            qt_tentativas_w := qt_tentativas_w + 1;
            utl_file.fcopy(nm_local_origem_w, nm_arquivo_orig_ca_w, nm_local_origem_w, 'w'||nm_arquivo_orig_ca_w);
            ie_aberto_w := 'S';
            exception
            when     others then
            ie_aberto_w := 'N';
        end;
        end loop;

    /*   Abrir Arquivo   */

        if    (qt_tentativas_w < 80) and (ie_aberto_w = 'S') then
        begin
            arq_texto_ca_w := utl_file.fopen(nm_local_origem_w, 'w' || nm_arquivo_orig_ca_w, 'R');
            ie_eof_w := 'N';
            while (ie_eof_w = 'N') loop
            begin

                utl_file.get_line(arq_texto_ca_w, ds_line_ca_w);
                    if    (substr(ds_line_ca_w, 1, 1) in ('0','1','2','3','4','5','6','7','8','9')) then
                    begin

                        qt_pos_separador_w     := instr(ds_line_ca_w,'|');
                        cd_estabelecimento_w    := substr(ds_line_ca_w,1,qt_pos_separador_w-1);
                        ds_line_ca_w        := substr(ds_line_ca_w,qt_pos_separador_w+1,(length(ds_line_ca_w)-qt_pos_separador_w));

                        qt_pos_separador_w     := instr(ds_line_ca_w,'|');
                        nr_atendimento_w    := trim(substr(ds_line_ca_w,1,qt_pos_separador_w-1));
                        ds_line_ca_w        := substr(ds_line_ca_w,qt_pos_separador_w+1,(length(ds_line_ca_w)-qt_pos_separador_w));

                        qt_pos_separador_w     := instr(ds_line_ca_w,'|');
                        ie_tipo_atendimento_w    := trim(substr(ds_line_ca_w,1,qt_pos_separador_w-1));
                        ds_line_ca_w        := substr(ds_line_ca_w,qt_pos_separador_w+1,(length(ds_line_ca_w)-qt_pos_separador_w));
                        dt_alteracao_w        := to_date(ds_line_ca_w,'dd/mm/yyyy hh24:mi:ss');
                        ds_line_ca_w        := '';
                        begin
                        cancelar_atendimento_paciente(hmsl_obter_atend_ficha(nr_atendimento_w),'TASY',null,OBTER_PESSOA_ATENDIMENTO(hmsl_obter_atend_ficha(nr_atendimento_w),'C'),null);
                        exception
                            when others then
                            insert into log_tasy (dt_atualizacao,
									nm_usuario,
									cd_log,
									ds_log) 
				values (sysdate,'TASY',88913,'Estab: '|| cd_estabelecimento_w || chr(13) ||
                                                  'Atend: '|| nr_atendimento_w       || chr(13) ||
                                                  'Tipo:  '|| ie_tipo_atendimento_w || chr(13) ||
                                                  'Data:  '|| dt_alteracao_w || chr(13));
                            commit;
                        end;
                    end;
                    end if;
                exception
                    when NO_DATA_FOUND then
                        ie_eof_w := 'S';
            end;
            end loop;
            utl_file.fclose(arq_texto_ca_w);
            utl_file.fremove(nm_local_origem_w,nm_arquivo_orig_ca_w);
            utl_file.fremove(nm_local_origem_w,'w' || nm_arquivo_orig_ca_w);
        end;
        end if;



commit;

end hmsl_integracao_atendimento_v2;
/
