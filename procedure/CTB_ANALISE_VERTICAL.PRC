CREATE OR REPLACE
PROCEDURE CTB_analise_vertical(	
			nr_seq_demo_p		Number,
			nm_usuario_p		Varchar2) IS 

nr_sequencia_w		Number(10,0);
nr_seq_col_w			Number(10,0);
nr_seq_coluna_w		Number(03,0);
nr_seq_coluna_1_w		Number(03,0);
nr_seq_rubrica_w		Number(10,0);
ds_divisor_w			Varchar2(4000);
vl_referencia_w		Number(15,2);
vl_dividendo_w		Number(15,2);
vl_divisor_w			Number(15,2);

Cursor C01 is
	select	nr_sequencia,
		nr_seq_coluna,
		nr_seq_col_1
	from	ctb_demo_mes
	where	nr_seq_demo	= nr_seq_demo_p
	and	ie_valor	= 'A';

Cursor C02 is
	select	b.nr_sequencia,
		to_number(somente_numero(b.ds_divisor))
	from	ctb_modelo_rubrica b,
		ctb_demo_rubrica a
	where	a.nr_seq_rubrica	= b.nr_sequencia
	and	a.nr_seq_demo		= nr_seq_demo_p
	and	a.nr_seq_col		= nr_seq_col_w
	and	b.ds_divisor is not null;
	
BEGIN

OPEN	C01;
LOOP
FETCH	C01
into	nr_seq_col_w,
	nr_seq_coluna_w,
	nr_seq_coluna_1_w;
EXIT	when c01%notfound;
	OPEN	C02;
	LOOP
	FETCH	C02
	into	nr_seq_rubrica_w,
		ds_divisor_w;
	EXIT	when c02%notfound;
		vl_referencia_w		:= 0;

		select	nr_sequencia
		into	nr_sequencia_w
		from	ctb_demo_mes
		where	nr_seq_demo	= nr_seq_demo_p
		and	nr_seq_coluna	= nr_seq_coluna_1_w;

		select	vl_referencia
		into	vl_dividendo_w
		from	ctb_demo_rubrica
		where	nr_seq_demo	= nr_seq_demo_p
		and	nr_seq_rubrica	= ds_divisor_w
		and	nr_seq_col	= nr_sequencia_w;

		select	vl_referencia
		into	vl_divisor_w
		from	ctb_demo_rubrica
		where	nr_seq_demo	= nr_seq_demo_p
		and	nr_seq_rubrica	= nr_seq_rubrica_w
		and	nr_seq_col	= nr_sequencia_w;

		select	nr_sequencia
		into	nr_sequencia_w
		from	ctb_demo_mes
		where	nr_seq_demo	= nr_seq_demo_p
		and	nr_seq_coluna	= nr_seq_coluna_w;

		update	ctb_demo_rubrica
		set	vl_referencia	= dividir(vl_divisor_w, vl_dividendo_w) * 100
		where	nr_seq_demo	= nr_seq_demo_p
		and	nr_seq_rubrica	= nr_seq_rubrica_w
		and	nr_seq_col	= nr_sequencia_w;

		update	ctb_demo_rubrica
		set	vl_referencia	= 100
		where	nr_seq_demo	= nr_seq_demo_p
		and	nr_seq_rubrica	= ds_divisor_w
		and	nr_seq_col	= nr_sequencia_w;

	end loop;
	close c02;
end loop;
close c01;

END CTB_analise_vertical;
/