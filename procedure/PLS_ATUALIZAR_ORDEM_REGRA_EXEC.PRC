create or replace
procedure pls_atualizar_ordem_regra_exec( nr_sequencia_p  number) is 

nr_ordem_execucao_w			number(10);
											
begin

select	pls_pp_tit_pag_rec_pck.obter_ordem_sug_adi_pag_regra(nr_sequencia_p)
into	nr_ordem_execucao_w
from	dual;


if	(nr_ordem_execucao_w is not null) and 
	(nr_sequencia_p is not null)	then
	
	update pls_evento_regra_adiant 
	set    nr_ordem_execucao = nr_ordem_execucao_w
	where  nr_sequencia      = nr_sequencia_p;

end if;

commit;

end pls_atualizar_ordem_regra_exec;
/

