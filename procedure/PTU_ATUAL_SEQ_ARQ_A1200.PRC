create or replace
procedure ptu_atual_seq_arq_a1200(	nr_sequencia_p		in	ptu_pacote.nr_sequencia%type,
					nr_seq_congenere_p	in	pls_congenere.nr_sequencia%type,
					cd_cooperativa_p	in	pls_congenere.cd_cooperativa%type,
					cd_estabelecimento_p	in	estabelecimento.cd_estabelecimento%type,
					nm_usuario_p		in	usuario.nm_usuario%type,
					nm_exportacao_p		out 	ptu_pacote.ds_arquivo%type) is 

nr_seq_arquivo_w		ptu_pacote.nr_seq_arquivo%type;
cd_unimed_origem_w		ptu_pacote.cd_unimed_origem%type;
versao_transacao_w		ptu_pacote.nr_versao_transacao%type;
nome_exportacao_w		varchar(14);

begin

	select  nvl(ptu_batch_xml_pck.obter_versao_transacao(cd_estabelecimento_p, nr_seq_congenere_p, cd_cooperativa_p, sysdate, 'A1200'), '05'),
		ptu_batch_xml_pck.obter_nome_exportacao(nr_sequencia_p, cd_estabelecimento_p, nr_seq_congenere_p, cd_cooperativa_p, sysdate, 'A1200', null)
	into 	versao_transacao_w,
		nome_exportacao_w
	from 	dual;

	update	ptu_pacote
	set	nr_versao_transacao	= versao_transacao_w,
		nr_seq_arquivo		= nvl(to_number(SUBSTR(nome_exportacao_w, 9, 2)), 1),
		dt_geracao_arquivo	= sysdate,
		nm_usuario_arq		= nm_usuario_p,
		ds_arquivo		= nome_exportacao_w
	where	nr_sequencia		= nr_sequencia_p;
	
	nm_exportacao_p := nome_exportacao_w;
	
	commit;

end ptu_atual_seq_arq_a1200;
/