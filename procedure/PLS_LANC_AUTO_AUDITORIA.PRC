create or replace
procedure pls_lanc_auto_auditoria
			(	nr_seq_auditoria_p	number,
				nr_seq_item_p		number,
				ie_tipo_item_p		varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Lan�ar itens na an��lise de autoriza��es conforme a regra criada
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[ X ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
	Performance
	Restritividade
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
	PLS_INCLUIR_ITEM_AUDITORIA
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

cd_cgc_w			varchar2(255);
ie_grupo_w			varchar2(255)	:= 'S';
ie_regra_preco_w		varchar2(3);
ie_restringe_estab_w		varchar2(2);
ie_tipo_despesa_w		varchar2(2)	:= null;
ie_tipo_item_w			varchar2(1);
ie_pacote_w			varchar2(1);
vl_preco_w			number(15,2);
cd_procedimento_w		number(15);
ie_origem_proced_w		number(10);
nr_seq_material_w		number(10);
nr_seq_regra_w			number(10);
cd_procedimento_item_w		number(15);
ie_origem_proced_item_w		number(10);
nr_seq_material_item_w		number(10);
qt_proc_regra_w			number(10);
qt_mat_regra_w			number(10);
nr_seq_prestador_w		number(10);
nr_seq_auditoria_item_w		number(10);
nr_seq_requisicao_w		number(10);
nr_seq_guia_w			number(10);
nr_seq_regra_preco_pac_w	number(10);
nr_seq_pacote_w			number(10);
nr_seq_uni_exec_w		number(10);
nr_seq_prest_fornec_w		number(10);
cd_fornecedor_w			number(10);
nr_seq_material_calc_w		number(10);
nr_seq_classificacao_w		number(10);
nr_seq_tipo_prestador_w		number(10);
nr_seq_grupo_w			number(10);
qt_item_lancamento_w		pls_regra_lanc_aut_item.qt_item_lancamento%type;
dt_preco_w			date;
nr_seq_proc_w			number(10);
nr_seq_mat_w			number(10);

Cursor C01 is
	select	a.nr_sequencia,
		a.nr_seq_grupo_prestador
	from	pls_regra_lanc_automatico	a
	where	a.ie_situacao	= 'A'
	and	a.ie_evento	= 10
	/* Itens (material, procedimento) */
	and	(a.cd_procedimento = cd_procedimento_w or a.cd_procedimento is null)
	and	(a.ie_origem_proced = ie_origem_proced_w or a.ie_origem_proced is null)
	and	(a.nr_seq_material = nr_seq_material_w or a.nr_seq_material is null)
	and	(a.ie_tipo_despesa = ie_tipo_despesa_w or a.ie_tipo_despesa is null)
	/* Unimed */
	and	(a.nr_seq_uni_exec = nr_seq_uni_exec_w or a.nr_seq_uni_exec is null)
	/* Prestador */
	and	(a.nr_seq_prestador = nr_seq_prestador_w or a.nr_seq_prestador is null)
	and	(a.nr_seq_classificacao = nr_seq_classificacao_w or a.nr_seq_classificacao is null)
	and	(a.nr_seq_tipo_prestador = nr_seq_tipo_prestador_w or a.nr_seq_tipo_prestador is null)
	/* Geral */
	and	((ie_restringe_estab_w	= 'N')
	or	((ie_restringe_estab_w	= 'S') and (a.cd_estabelecimento = cd_estabelecimento_p)))
	and	sysdate between (nvl(a.dt_inicio_vigencia,sysdate)) and (fim_dia(nvl(a.dt_fim_vigencia,sysdate)))
	order by
		nvl(a.cd_procedimento, 0),
		nvl(a.nr_seq_material, 0),
		nvl(a.ie_tipo_despesa, '0'),--
		nvl(a.nr_seq_uni_exec, 0),
		nvl(a.nr_seq_prestador, 0),
		nvl(a.nr_seq_tipo_prestador, 0),
		nvl(a.nr_seq_classificacao, 0),
		nvl(a.nr_seq_grupo_prestador, 0);

Cursor C02 is
	select	a.cd_procedimento,
		a.ie_origem_proced,
		a.nr_seq_material,
		a.nr_seq_prest_fornec,
		a.qt_item_lancamento
	from	pls_regra_lanc_aut_item	a
	where	a.nr_seq_regra	= nr_seq_regra_w
	and	a.ie_situacao	= 'A';

begin
begin
select	a.cd_procedimento,
	a.ie_origem_proced,
	a.nr_seq_material,
	a.ie_tipo_despesa
into	cd_procedimento_w,
	ie_origem_proced_w,
	nr_seq_material_w,
	ie_tipo_despesa_w
from	pls_auditoria_item	a
where	a.nr_sequencia	= nr_seq_item_p;
exception
when others then
	cd_procedimento_w	:= null;
	ie_origem_proced_w	:= null;
	nr_seq_material_w	:= null;
	ie_tipo_despesa_w	:= null;
end;

if	(ie_tipo_item_p <> 'M') then
	ie_tipo_despesa_w	:= null;
end if;

begin
select	nvl(a.nr_seq_requisicao, 0),
	nvl(a.nr_seq_guia, 0),
	a.nr_seq_prestador
into	nr_seq_requisicao_w,
	nr_seq_guia_w,
	nr_seq_prestador_w
from	pls_auditoria	a
where	a.nr_sequencia	= nr_seq_auditoria_p;
exception
when others then
	nr_seq_requisicao_w	:= 0;
	nr_seq_guia_w		:= 0;
	nr_seq_prestador_w	:= 0;
end;

select	max(a.nr_seq_classificacao),
	max(a.nr_seq_tipo_prestador)
into	nr_seq_classificacao_w,
	nr_seq_tipo_prestador_w
from	pls_prestador	a
where	a.nr_sequencia	= nr_seq_prestador_w;

begin
select	a.nr_seq_uni_exec
into	nr_seq_uni_exec_w
from	pls_guia_plano	a
where	a.nr_sequencia	= nr_seq_guia_w;
exception
when others then
	nr_seq_uni_exec_w	:= 0;
end;

begin
select	a.nr_seq_uni_exec
into	nr_seq_uni_exec_w
from	pls_requisicao	a
where	a.nr_sequencia	= nr_seq_requisicao_w;
exception
when others then
	nr_seq_uni_exec_w	:= 0;
end;

ie_restringe_estab_w	:= pls_obter_se_controle_estab('LA');

open C01;
loop
fetch C01 into
	nr_seq_regra_w,
	nr_seq_grupo_w;
exit when C01%notfound;
	begin
	ie_grupo_w	:= 'S';
	
	if	(nr_seq_grupo_w is not null) and
		(nr_seq_prestador_w is not null) then
		ie_grupo_w	:= nvl(substr(pls_obter_se_prestador_grupo(nr_seq_grupo_w, nr_seq_prestador_w),1,255), 'S');
	end if;
	
	if	(ie_grupo_w = 'S') then
		open C02;
		loop
		fetch C02 into
			cd_procedimento_item_w,
			ie_origem_proced_item_w,
			nr_seq_material_item_w,
			nr_seq_prest_fornec_w,
			qt_item_lancamento_w;
		exit when C02%notfound;
			begin
			if	(cd_procedimento_item_w	is not null) then
				select	count(1)
				into	qt_proc_regra_w
				from	pls_auditoria_item	a
				where	a.cd_procedimento	= cd_procedimento_item_w
				and	a.ie_origem_proced	= ie_origem_proced_item_w
				and	a.nr_seq_auditoria	= nr_seq_auditoria_p;

				if	(qt_proc_regra_w = 0) then
					select 	nvl(max(a.ie_classificacao), 1)
					into	ie_tipo_despesa_w
					from	procedimento	a
					where	a.cd_procedimento	= cd_procedimento_item_w
					and	a.ie_origem_proced	= ie_origem_proced_item_w;

					if	(nr_seq_prestador_w	> 0) then
						select	max(a.nr_sequencia)
						into	nr_seq_pacote_w
						from	pls_pacote	a
						where	nvl(a.nr_seq_prestador,nvl(nr_seq_prestador_w,0)) = nvl(nr_seq_prestador_w,0)
						and	a.cd_procedimento	= cd_procedimento_item_w
						and	a.ie_origem_proced	= ie_origem_proced_item_w
						and	a.ie_situacao		= 'A';

						if	(nr_seq_pacote_w is not null) then
							ie_tipo_despesa_w	:= '4';
						end if;
					end if;

					select	pls_auditoria_item_seq.nextval
					into	nr_seq_auditoria_item_w
					from	dual;

					insert into pls_auditoria_item
						(nr_sequencia,
						nr_seq_auditoria,
						cd_procedimento,
						ie_origem_proced,
						qt_original,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						qt_ajuste,
						ie_status,
						ie_acao_auditor,
						ie_tipo_despesa)
					values	(nr_seq_auditoria_item_w,
						nr_seq_auditoria_p,
						cd_procedimento_item_w,
						ie_origem_proced_item_w,
						nvl(qt_item_lancamento_w, 1),
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nvl(qt_item_lancamento_w, 1),
						'P',
						'I',
						ie_tipo_despesa_w);

					/* Francisco - 16/05/2012 - OS 447352 */
					if	(nr_seq_pacote_w is not null) then
						select	nvl(a.ie_regra_preco, 'N')
						into	ie_regra_preco_w
						from	pls_pacote a
						where	a.nr_sequencia	= nr_seq_pacote_w;

						if	(ie_regra_preco_w = 'S') then
							pls_obter_regra_preco_pacote(	cd_procedimento_item_w,
											ie_origem_proced_item_w,
											'AA',
											nr_seq_auditoria_item_w,
											nm_usuario_p,
											nr_seq_pacote_w,
											nr_seq_regra_preco_pac_w);

							if	(nr_seq_pacote_w is null) and
								(ie_tipo_despesa_w = '4') then
								select 	nvl(max(a.ie_classificacao), 1)
								into	ie_tipo_despesa_w
								from	procedimento	a
								where	a.cd_procedimento	= cd_procedimento_item_w
								and	a.ie_origem_proced	= ie_origem_proced_item_w;

								update	pls_auditoria_item
								set	ie_tipo_despesa	= ie_tipo_despesa_w
								where	nr_sequencia	= nr_seq_auditoria_item_w;
							end if;
						end if;
					end if;

					if	(nvl(nr_seq_requisicao_w, 0) > 0) then
						pls_consistir_itens_inseridos(	nr_seq_auditoria_p,
										nr_seq_auditoria_item_w,
										nr_seq_requisicao_w,
										0,
										'P',
										nm_usuario_p,
										cd_estabelecimento_p,
										null,
										nr_seq_proc_w,
										nr_seq_mat_w);
					end if;
				end if;
			end if;

			if	(nr_seq_material_item_w	is not null) then
				select	count(1)
				into	qt_mat_regra_w
				from	pls_auditoria_item	a
				where	a.nr_seq_material	= nr_seq_material_item_w
				and	a.nr_seq_auditoria	= nr_seq_auditoria_p;

				if	(qt_mat_regra_w	= 0) then
					select 	max(a.ie_tipo_despesa)
					into	ie_tipo_despesa_w
					from	pls_material	a
					where	a.nr_sequencia	= nr_seq_material_item_w;

					select	pls_auditoria_item_seq.nextval
					into	nr_seq_auditoria_item_w
					from	dual;

					if	(nr_seq_prest_fornec_w is not null) then
						begin
						select	a.cd_cgc
						into	cd_cgc_w
						from	pls_prestador	a
						where	a.nr_sequencia	= nr_seq_prest_fornec_w;
						exception
						when others then
							cd_cgc_w	:= 'X';
						end;

						if	(nvl(cd_cgc_w, 'X') <> 'X') then
							begin
							select	max(a.cd_fornecedor)
							into	cd_fornecedor_w
							from	pls_fornec_mat_fed_sc	a
							where	a.cd_cgc	= cd_cgc_w
							and	a.ie_situacao	= 'A';
							exception
							when others then
								cd_fornecedor_w	:= 0;
							end;

							if	(nvl(cd_fornecedor_w, 0) <> 0) then
								begin
								select	max(c.dt_preco),
									c.vl_preco
								into	dt_preco_w,
									vl_preco_w
								from	pls_fornec_mat_fed_sc	d,
									pls_mat_uni_fed_sc_preco c,
									pls_mat_unimed_fed_sc b,
									pls_material a
								where	b.nr_sequencia 	= c.nr_seq_mat_unimed
								and	b.cd_material	= a.cd_material_ops
								and	d.cd_fornecedor	= c.cd_fornecedor_fed_sc
								and	d.ie_situacao	= 'A'
								and	d.cd_fornecedor	= cd_fornecedor_w
								and	a.nr_sequencia	= nr_seq_material_item_w
								group by
									c.vl_preco;
								exception
								when others then
									vl_preco_w	:= 0;
								end;
							end if;
						end if;
					end if;

					insert into pls_auditoria_item
						(nr_sequencia,
						nr_seq_auditoria,
						nr_seq_material,
						qt_original,
						dt_atualizacao,
						nm_usuario,
						dt_atualizacao_nrec,
						nm_usuario_nrec,
						qt_ajuste,
						ie_status,
						ie_acao_auditor,
						ie_tipo_despesa,
						nr_seq_prest_fornec,
						vl_item,
						vl_original)
					values	(nr_seq_auditoria_item_w,
						nr_seq_auditoria_p,
						nr_seq_material_item_w,
						nvl(qt_item_lancamento_w, 1),
						sysdate,
						nm_usuario_p,
						sysdate,
						nm_usuario_p,
						nvl(qt_item_lancamento_w, 1),
						'P',
						'I',
						ie_tipo_despesa_w,
						nr_seq_prest_fornec_w,
						vl_preco_w,
						vl_preco_w);

					if	(nvl(nr_seq_requisicao_w, 0) > 0) then
						pls_consistir_itens_inseridos(	nr_seq_auditoria_p,
										nr_seq_auditoria_item_w,
										nr_seq_requisicao_w,
										0,
										'M',
										nm_usuario_p,
										cd_estabelecimento_p,
										null,
										nr_seq_proc_w,
										nr_seq_mat_w);
					end if;
				end if;
			end if;
			end;
		end loop;
		close C02;
	end if;
	end;
end loop;
close C01;

--commit;

end pls_lanc_auto_auditoria;
/
