create or replace procedure GERAR_MOVTO_SINT_OPER_MES(	nm_usuario_p			varchar2,
					cd_estab_p			number,
					dt_mesano_referencia_p		date,
					ie_consignado_p			varchar2,
					cd_operacao_estoque_p		number,
					cd_material_p			number,
					cd_grupo_material_p		number,
					cd_subgrupo_material_p		number,
					cd_classe_material_p		number,
					cd_local_estoque_p		number,
					ie_tipo_local_p			Varchar2,
					ie_dif_saldo_p			Varchar2,
					ie_dif_valor_p			Varchar2,
					ie_geral_p			Varchar2,
					ie_Grupo_p			Varchar2,
					ie_subgrupo_p			Varchar2,
					ie_Classe_p			Varchar2,
					ie_material_p			Varchar2) is

nr_sequencia_w			Number(10);
ds_descricao_w			varchar2(100);
cd_grupo_w			Number(3);
cd_subgrupo_w			Number(3);
cd_operacao_estoque_w		Number(3);
ie_entrada_saida_w		varchar2(1);
ie_nivel_w			Number(3);
cd_classe_w			Number(5);
cd_material_w			Number(6);
qt_movto_w			Number(18,4);
vl_movto_w			Number(18,4);
qt_quant_w			Number(10);
cd_local_estoque_w		Number(4,0);
cd_estabelecimento_w		number(4);
dt_mes_inicial_w		date;
dt_mes_final_w			date;
dt_mes_saldo_ant_w		date;
dt_mes_saldo_pos_w		date;
dt_mesano_referencia_w		date;

Cursor c01 is
/* Primeiro select para as operacoes*/
select	e.cd_grupo_material,
	e.cd_subgrupo_material,
	e.cd_classe_material,
	e.cd_material,
	a.cd_operacao_estoque,
	a.cd_local_estoque,
	a.ie_entrada_saida,
	sum(a.qt_estoque),
	sum(a.vl_estoque),
	a.dt_mesano_referencia
from	local_estoque c,
	estrutura_material_v e,
	movto_estoque_operacao_v a
where	a.dt_mesano_referencia	between dt_mes_inicial_w and dt_mes_final_w
and	a.cd_material 		= e.cd_material
and	a.ie_consignado         = ie_consignado_p
and	((cd_estabelecimento_w = 0) or
	((cd_estabelecimento_w > 0) and (a.cd_estabelecimento	= cd_estabelecimento_w)))
and	a.cd_local_estoque	= c.cd_local_estoque
and	c.ie_proprio		= decode(ie_tipo_local_p,'P','S','T','N',c.ie_proprio)
and	a.cd_operacao_estoque 	= nvl(cd_operacao_estoque_p, a.cd_operacao_estoque)
and	a.cd_material		= nvl(cd_material_p, a.cd_material)
and	e.cd_grupo_material	= nvl(cd_grupo_material_p, e.cd_grupo_material)
and	e.cd_subgrupo_material	= nvl(cd_subgrupo_material_p, e.cd_subgrupo_material)
and	e.cd_classe_material	= nvl(cd_classe_material_p, e.cd_classe_material)
and	a.cd_local_estoque	= nvl(cd_local_estoque_p, a.cd_local_estoque)
group by
	e.cd_grupo_material,
	e.cd_subgrupo_material,
	e.cd_classe_material,
	e.cd_material,
	a.cd_local_estoque,
	a.cd_operacao_estoque,
	a.ie_entrada_saida,
	a.dt_mesano_referencia
union
/* segundo select para o valor anterior*/
select	e.cd_grupo_material,
	e.cd_subgrupo_material,
	e.cd_classe_material,
	e.cd_material,
	0,
	a.cd_local_estoque,
	'A',
	sum(qt_estoque),
   	sum(vl_estoque),
	PKG_DATE_UTILS.ADD_MONTH(a.dt_mesano_referencia,1,0)
from	local_estoque c,
	estrutura_material_v e,
	saldo_estoque a
where	a.dt_mesano_referencia between dt_mes_saldo_ant_w and dt_mes_saldo_pos_w
and	((cd_estabelecimento_w = 0) or
	((cd_estabelecimento_w > 0) and (a.cd_estabelecimento	= cd_estabelecimento_w)))
and	a.cd_local_estoque	= c.cd_local_estoque
and	c.ie_proprio		= decode(ie_tipo_local_p,'P','S','T','N',c.ie_proprio)
and	a.cd_material		= e.cd_material
and	a.cd_material		= nvl(cd_material_p, a.cd_material)
and	e.cd_grupo_material	= nvl(cd_grupo_material_p, e.cd_grupo_material)
and	e.cd_subgrupo_material	= nvl(cd_subgrupo_material_p, e.cd_subgrupo_material)
and	e.cd_classe_material	= nvl(cd_classe_material_p, e.cd_classe_material)
and	a.cd_local_estoque	= nvl(cd_local_estoque_p, a.cd_local_estoque)
group by
	e.cd_grupo_material,
	e.cd_subgrupo_material,
	e.cd_classe_material,
	e.cd_material,
	a.cd_local_estoque,
	a.dt_mesano_referencia
union
/* terceiro select para o valor atual*/
select	e.cd_grupo_material,
	e.cd_subgrupo_material,
	e.cd_classe_material,
	e.cd_material,
	0,
	a.cd_local_estoque,
	'X',
	sum(qt_estoque),
   	sum(vl_estoque),
	a.dt_mesano_referencia
from	local_estoque c,
	estrutura_material_v e,
	saldo_estoque a
where	a.dt_mesano_referencia between dt_mes_inicial_w and dt_mes_final_w
and	((cd_estabelecimento_w = 0) or
	((cd_estabelecimento_w > 0) and (a.cd_estabelecimento	= cd_estabelecimento_w)))
and	a.cd_local_estoque	= c.cd_local_estoque
and	c.ie_proprio		= decode(ie_tipo_local_p,'P','S','T','N',c.ie_proprio)
and	a.cd_material		= e.cd_material
and	a.cd_material		= nvl(cd_material_p, a.cd_material)
and	e.cd_grupo_material	= nvl(cd_grupo_material_p, e.cd_grupo_material)
and	e.cd_subgrupo_material	= nvl(cd_subgrupo_material_p, e.cd_subgrupo_material)
and	e.cd_classe_material	= nvl(cd_classe_material_p, e.cd_classe_material)
and	a.cd_local_estoque	= nvl(cd_local_estoque_p, a.cd_local_estoque)
group by
	e.cd_grupo_material,
	e.cd_subgrupo_material,
	e.cd_classe_material,
	e.cd_material,
	a.cd_local_estoque,
	a.dt_mesano_referencia;

BEGIN

dt_mes_inicial_w	:= PKG_DATE_UTILS.ADD_MONTH(PKG_DATE_UTILS.start_of(dt_mesano_referencia_p,'MM',0),-5,0);
dt_mes_final_w		:= pkg_date_utils.start_of(dt_mesano_referencia_p,'MONTH',0);

dt_mes_saldo_ant_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_mes_inicial_w,-1,0);
dt_mes_saldo_pos_w	:= PKG_DATE_UTILS.ADD_MONTH(dt_mes_final_w,-1,0);

cd_estabelecimento_w := nvl(cd_estab_p,0);

delete from w_movto_sint_oper;

if	(ie_dif_saldo_p = 'S') then
	Verifica_Dif_Saldo(dt_mes_inicial_w, 0);
end if;
if	(ie_dif_valor_p = 'S') then
	Verifica_Dif_Valor(dt_mes_inicial_w,0);
end if;

OPEN c01;
LOOP
FETCH c01 into
	cd_grupo_w,
	cd_subgrupo_w,
	cd_classe_w,
	cd_material_w,
	cd_operacao_estoque_w,
	cd_local_estoque_w,
	ie_entrada_saida_w,
	qt_movto_w,
	vl_movto_w,
	dt_mesano_referencia_w;
	EXIT when c01%notfound;
	begin

	/* Verifica se o material existe na tabela w_saldo_estoque
	quando informado itens com diferenša de saldo ou valor*/
	qt_quant_w	:= 1;
	if	(ie_dif_saldo_p = 'S') or (ie_dif_valor_p = 'S') then
		select	count(*)
		into	qt_quant_w
		from	w_saldo_estoque
		where	cd_material = cd_material_w;
	end if;

	if	(qt_quant_w > 0) then
		begin
		FOR	ie_nivel_w in 1 .. 5 LOOP
		    	begin
			if	((ie_nivel_w = 5) and (ie_material_p = 'S')) or
				((ie_nivel_w = 4) and (ie_classe_p = 'S')) or
				((ie_nivel_w = 3) and (ie_subgrupo_p = 'S')) or
				((ie_nivel_w = 2) and (ie_grupo_p = 'S')) or
				((ie_nivel_w = 1) and (ie_geral_p = 'S'))then
				begin

				select	nvl(max(nr_sequencia),0)
				into	nr_sequencia_w
				from	w_movto_sint_oper
				where	cd_grupo		= cd_grupo_w
				and	cd_subgrupo		= cd_subgrupo_w
				and	cd_classe		= cd_classe_w
				and	cd_material		= cd_material_w
				and	cd_operacao_estoque	= cd_operacao_estoque_w
				and	ie_nivel		= ie_nivel_w
				and	ie_entrada_saida	= ie_entrada_saida_w
				and	dt_atualizacao		= dt_mesano_referencia_w;

				if	(nr_sequencia_w = 0) then
					begin

					select	decode(	ie_nivel_w,
						1,obter_desc_expressao(503454),
						2,substr(obter_desc_estrut_mat(cd_grupo_w,'','',''),1,100),
						3,substr(obter_desc_estrut_mat('',cd_subgrupo_w,'',''),1,100),
						4,substr(obter_desc_estrut_mat('','',cd_classe_w,''),1,100),
						5,substr(obter_desc_estrut_mat('','','',cd_material_w),1,100))
					into	ds_descricao_w
					from	dual;

					insert into w_movto_sint_oper(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						ie_nivel,
						cd_grupo,
						cd_subgrupo,
						cd_classe,
						cd_material,
						ds_descricao,
						cd_operacao_estoque,
						cd_local_estoque,
						ie_entrada_saida,
						qt_movto,
						vl_movto)
					values(	w_movto_sint_oper_seq.nextval,
						dt_mesano_referencia_w,
						nm_usuario_p,
						ie_nivel_w,
						cd_grupo_w,
						cd_subgrupo_w,
						cd_classe_w,
						cd_material_w,
						ds_descricao_w,
						cd_operacao_estoque_w,
						cd_local_estoque_w,
						ie_entrada_saida_w,
						qt_movto_w,
						vl_movto_w);
					end;
				else
					update	w_movto_sint_oper
					set	qt_movto	= qt_movto + qt_movto_w,
						vl_movto	= vl_movto + vl_movto_w
					where	nr_sequencia	= nr_sequencia_w;
				end if;
				end;
			end if;
			end;
		end loop;
		end;
	end if;
	end;
END LOOP;
CLOSE c01;

commit;

END GERAR_MOVTO_SINT_OPER_MES;
/