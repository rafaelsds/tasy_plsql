create or replace
procedure lab_desaprovar_exame(nr_prescricao_p	number,
			nr_seq_prescr_p		number,
			nm_usuario_p		Varchar2) is 
PRAGMA AUTONOMOUS_TRANSACTION;			
nr_seq_resultado_w	number(10);
			
			
begin
select 	max(nr_seq_resultado)
into	nr_seq_resultado_w
from	exame_lab_resultado
where	nr_prescricao = nr_prescricao_p;

update 	exame_lab_result_item
set 	dt_aprovacao = null
where  	nr_seq_resultado = nr_seq_resultado_w
and	nr_seq_prescr = nr_seq_prescr_p
and	dt_aprovacao is not null;	

commit;

end lab_desaprovar_exame;
/