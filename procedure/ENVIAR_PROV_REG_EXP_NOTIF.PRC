create or replace PROCEDURE ENVIAR_PROV_REG_EXP_NOTIF(ds_sender_email_p varchar2, sender_nm_usuario_p varchar2) IS

cd_pessoa_fisica_w      CRM_ESTADO.CD_PESSOA_FISICA%TYPE;
nm_usuario_w            CRM_ESTADO.NM_USUARIO%TYPE;
ds_usuario_w            USUARIO.DS_USUARIO%TYPE;
ds_email_w              USUARIO.DS_EMAIL%TYPE;
ds_titulo_w		        VARCHAR2(255);
ds_mensagem_w		    VARCHAR2(4000);

CURSOR C01 IS
    SELECT cd_pessoa_fisica, nm_usuario 
    FROM CRM_ESTADO 
    WHERE trunc(dt_fim_vigencia) = trunc(sysdate);

CURSOR C02 IS
    SELECT cd_pessoa_fisica, nm_usuario 
    FROM PROFISSIONAL_SEGURO 
    WHERE trunc(dt_fim_vigencia) = trunc(sysdate);

BEGIN
    ds_titulo_w := wheb_mensagem_pck.get_texto(1120380);
    OPEN C01;
    LOOP
        FETCH C01 INTO cd_pessoa_fisica_w, nm_usuario_w;
        EXIT WHEN C01%NOTFOUND;
        BEGIN
            select max(ds_usuario) into  ds_usuario_w from usuario where cd_pessoa_fisica = cd_pessoa_fisica_w;
            ds_mensagem_w := wheb_mensagem_pck.get_texto(1120375,'DS_USUARIO='||ds_usuario_w||';CD_PESSOA_FISICA='||cd_pessoa_fisica_w);

            select max(ds_email) into ds_email_w from usuario where nm_usuario = nm_usuario_w;

            IF (ds_email_w is not null) THEN
                enviar_Email( to_char(sysdate,'DD-MON-YYYY')||': '||ds_titulo_w,ds_mensagem_w,ds_sender_email_p,ds_email_w,sender_nm_usuario_p,'A');
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO log_tasy(DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG)
                    VALUES (SYSDATE,'Job',9999, 'Provincial Registration Expiry Notification failed due to invalid email-id of Responsible Person:'||nm_usuario_w);
            WHEN OTHERS THEN
                INSERT INTO log_tasy(DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG)
                    VALUES (SYSDATE,'Job',9999, 'Provincial Registration Expiry Notification for :'||cd_pessoa_fisica_w||' failed. Responsible Person:'||nm_usuario_w);
        END;
    END LOOP;
    CLOSE C01;

	ds_titulo_w := wheb_mensagem_pck.get_texto(1121213);
	OPEN C02;
    LOOP
        FETCH C02 INTO cd_pessoa_fisica_w, nm_usuario_w;
        EXIT WHEN C02%NOTFOUND;
        BEGIN
            select max(ds_usuario) into  ds_usuario_w from usuario where cd_pessoa_fisica = cd_pessoa_fisica_w;
            ds_mensagem_w := wheb_mensagem_pck.get_texto(1121214,'DS_USUARIO='||ds_usuario_w||';CD_PESSOA_FISICA='||cd_pessoa_fisica_w);

            select max(ds_email) into ds_email_w from usuario where nm_usuario = nm_usuario_w;

            IF (ds_email_w is not null) THEN
                enviar_Email( to_char(sysdate,'DD-MON-YYYY')||': '||ds_titulo_w,ds_mensagem_w,ds_sender_email_p,ds_email_w,sender_nm_usuario_p,'A');
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                INSERT INTO log_tasy(DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG)
                    VALUES (SYSDATE,'Job',9999, 'Physician Insurance Expiry Notification failed due to invalid email-id of Responsible Person:'||nm_usuario_w);
            WHEN OTHERS THEN
                INSERT INTO log_tasy(DT_ATUALIZACAO, NM_USUARIO, CD_LOG, DS_LOG)
                    VALUES (SYSDATE,'Job',9999, 'Physician Insurance Expiry  Notification for :'||cd_pessoa_fisica_w||' failed. Responsible Person:'||nm_usuario_w);
        END;
    END LOOP;
    CLOSE C02;

END;
/

