create or replace procedure create_ontology_structure_tree(cd_funcao_p number) is

--Fields
NM_TABELA_W                 ONTOLOGIA_LISTA_W.Nm_Tabela%Type;
NM_ATRIBUTO_W               ONTOLOGIA_LISTA_W.Nm_Atributo%Type;
NR_SEQ_ORDENACAO_W          ONTOLOGIA_LISTA_W.Nr_Seq_Ordenacao%Type; 
DS_VALUE_W                  ONTOLOGIA_LISTA_W.Ds_Value%Type;
CD_DOMINIO_W                ONTOLOGIA_LISTA_W.Cd_Dominio%Type; 
NR_NIVEL_W                  ONTOLOGIA_LISTA_W.Nr_Nivel%Type; 
CD_FUNCAO_W                 ONTOLOGIA_LISTA_W.Cd_Funcao%Type;
DS_TABELA_W                 ONTOLOGIA_LISTA_W.DS_TABELA%Type;
DS_ATRIBUTO_W               ONTOLOGIA_LISTA_W.DS_ATRIBUTO%Type;
DS_DOMINIO_W                ONTOLOGIA_LISTA_W.DS_DOMINIO%Type;
DS_VALUE_LISTA_W            ONTOLOGIA_LISTA_W.DS_VALUE_LISTA%Type;
VL_CHAVE_W			        	  ONTOLOGIA_LISTA_W.VL_CHAVE%Type;


--sequencias
NR_SEQ_ORD_TABELA_W         number(10)  := 0;
NR_SEQ_ORD_ATRIBUTO_W       number(10)  := 0;
NR_SEQ_ORD_DOMINIO_W        number(10)  := 0;
NR_SEQ_ORD_VALORES_W        number(10)  := 0;

count_regs_w  				number(10)  := 0;

context_w					varchar2(50);

table_name_w				varchar2(100);
strQuery_w					varchar2(500);

nm_usuario_w 				varchar2(100);


cursor c01 is
SELECT x.NM_TABELA          NM_TABELA,
       x.NM_ATRIBUTO        NM_ATRIBUTO,
       x.DESC_CADASTRO      DESC_CADASTRO,
       x.NR_SEQUENCIA       NR_SEQUENCIA,
       x.CD_DOMINIO         CD_DOMINIO,
       x.DS_VALORES         DS_VALORES,
       x.DESCRICAO          DESCRICAO
  FROM (SELECT A.NM_TABELA,
               A.NM_ATRIBUTO,
               DESC_CADASTRO,
               NR_SEQUENCIA,
               a.CD_DOMINIO,
               a.DS_VALORES,
               a.DESCRICAO,
               MAX(NR_SEQUENCIA) OVER(PARTITION BY A.NM_TABELA, A.NM_ATRIBUTO ORDER BY 1) max_nrseq
          FROM (SELECT obter_desc_expressao( nvl(a.CD_EXP_LABEL, a.CD_EXP_LABEL_GRID)) DESCRICAO,
                       A.nm_atributo NM_ATRIBUTO,
                       A.CD_DOMINIO CD_DOMINIO,
                       A.DS_VALORES DS_VALORES,
                       B.NM_TABELA NM_TABELA,
                       b.CD_EXP_TITULO,
                       obter_desc_expressao(CD_EXP_TITULO) DESC_CADASTRO,
                       A.NR_SEQUENCIA NR_SEQUENCIA
                  FROM TABELA_VISAO_ATRIBUTO A, TABELA_VISAO B
                 WHERE a.NR_SEQUENCIA IN
                       (SELECT W.NR_SEQ_VISAO
                          FROM objeto_schematic w
                         WHERE w.NR_SEQ_FUNCAO_SCHEMATIC = ( select nr_Sequencia from FUNCAO_SCHEMATIC where ie_situacao_funcao = 'A' and cd_funcao = cd_funcao_p)
                           AND w.NR_SEQ_VISAO IS NOT NULL
                           AND w.NM_TABELA IS NOT NULL
                         GROUP BY NM_TABELA, NR_SEQ_VISAO)
                   AND a.CD_EXP_LABEL IS NOT NULL
                   AND B.NR_SEQUENCIA = A.NR_SEQUENCIA
                 GROUP BY a.CD_EXP_LABEL,
                          a.CD_EXP_LABEL_GRID,
                          a.nm_atributo,
                          a.CD_DOMINIO,
                          a.DS_VALORES,
                          B.NM_TABELA,
                          b.CD_EXP_TITULO,
                          a.NR_SEQUENCIA) A,
               TABELA_ATRIBUTO C,
               TABELA_SISTEMA T
         WHERE A.NM_TABELA = C.NM_TABELA
           AND T.NM_TABELA = C.NM_TABELA
           AND A.NM_ATRIBUTO = C.NM_ATRIBUTO
           AND C.IE_TIPO_ATRIBUTO NOT IN ('FUNCTION', 'VISUAL', 'XMLTYPE')
           and a.nm_tabela = NM_TABELA_W) x
 WHERE x.NR_SEQUENCIA = x.max_nrseq
 and ((x.descricao is not null and x.descricao <> ' ') or x.ds_valores is not null)
 ORDER BY 1 ASC;
 c01_w c01%rowtype;
 
  cursor c01_cpoe is
 SELECT x.NM_TABELA          NM_TABELA,
       x.NM_ATRIBUTO        NM_ATRIBUTO,
       x.DESC_CADASTRO      DESC_CADASTRO,
       x.NR_SEQUENCIA       NR_SEQUENCIA,
       x.CD_DOMINIO         CD_DOMINIO,
       x.DS_VALORES         DS_VALORES,
       (case 
         when LENGTH(x.DESCRICAO) <= 3 or x.DESCRICAO is null then 
           x.NM_ATRIBUTO
         else
           x.DESCRICAO  ||' ('|| x.NM_ATRIBUTO ||')'
       end) DESCRICAO,
       x.IE_TIPO_ATRIBUTO   IE_TIPO_ATRIBUTO,
       x.IE_COMPONENTE      IE_COMPONENTE
  FROM (SELECT A.NM_TABELA,
               A.NM_ATRIBUTO,
               DESC_CADASTRO,
               NR_SEQUENCIA,
               a.CD_DOMINIO,
               a.DS_VALORES,
               a.DESCRICAO,
               MAX(NR_SEQUENCIA) OVER(PARTITION BY A.NM_TABELA, A.NM_ATRIBUTO ORDER BY 1) max_nrseq,
               C.IE_TIPO_ATRIBUTO,
               A.IE_COMPONENTE
          FROM (SELECT obter_desc_expressao( nvl(a.CD_EXP_LABEL, a.CD_EXP_LABEL_GRID)) DESCRICAO,
                       A.nm_atributo NM_ATRIBUTO,
                       A.CD_DOMINIO CD_DOMINIO,
                       A.DS_VALORES DS_VALORES,
                       B.NM_TABELA NM_TABELA,
                       b.CD_EXP_TITULO,
                       obter_desc_expressao(CD_EXP_TITULO) DESC_CADASTRO,
                       A.NR_SEQUENCIA NR_SEQUENCIA,
                       A.IE_COMPONENTE IE_COMPONENTE
                  FROM TABELA_VISAO_ATRIBUTO A, TABELA_VISAO B
                 WHERE a.NR_SEQUENCIA IN
                       (SELECT W.NR_SEQ_VISAO
                          FROM objeto_schematic w
                         WHERE w.NR_SEQ_FUNCAO_SCHEMATIC = ( select nr_Sequencia from FUNCAO_SCHEMATIC where ie_situacao_funcao = 'A' and cd_funcao = cd_funcao_p)
                           AND w.NR_SEQ_VISAO IS NOT NULL
                           AND w.NM_TABELA IS NOT NULL
                         GROUP BY NM_TABELA, NR_SEQ_VISAO)
                   AND a.CD_EXP_LABEL IS NOT NULL
                   AND B.NR_SEQUENCIA = A.NR_SEQUENCIA
                 GROUP BY a.CD_EXP_LABEL,
                          a.CD_EXP_LABEL_GRID,
                          a.nm_atributo,
                          a.CD_DOMINIO,
                          a.DS_VALORES,
                          B.NM_TABELA,
                          b.CD_EXP_TITULO,
                          a.NR_SEQUENCIA,
                          A.IE_COMPONENTE) A,
               TABELA_ATRIBUTO C,
               TABELA_SISTEMA T
         WHERE A.NM_TABELA = C.NM_TABELA
           AND T.NM_TABELA = C.NM_TABELA
           AND A.NM_ATRIBUTO = C.NM_ATRIBUTO
           AND C.IE_TIPO_ATRIBUTO NOT IN ('FUNCTION', 'VISUAL', 'XMLTYPE')
           and a.nm_tabela = NM_TABELA_W) x
 WHERE x.NR_SEQUENCIA = x.max_nrseq
 and ((x.descricao is not null and x.descricao <> ' ') or x.ds_valores is not null)
 ORDER BY 1 ASC;
 
 c01_cpoe_w c01_cpoe%rowtype;
 
cursor c01_new_atributes is
select a.nm_atributo,
       a.nm_tabela,
       obter_desc_expressao(nvl(a.CD_EXP_DESC, a.CD_EXP_LABEL_LONGO)) DESCRICAO,
       a.CD_DOMINIO,
       a.cd_exp_valores ds_valores
  from ONTOLOGIA_TABELA_ATRIBUTO a
 where a.nm_tabela = NM_TABELA_W;
c01_new_atributes_w c01_new_atributes%rowtype;
 
 cursor c02 is
  select b.nm_tabela nm_tabela, 
        obter_desc_expressao(b.CD_EXP_CADASTRO) ds_tabela
  from OBJETO_SCHEMATIC a, ONTOLOGIA_TABELA b, ONTOLOGIA_TABELA_ATRIBUTO c
 where a.nm_tabela = b.nm_tabela
   and b.nm_tabela = c.nm_tabela
   and a.cd_funcao = cd_funcao_p
   and a.nm_tabela is not null
   group by b.nm_tabela,b.CD_EXP_CADASTRO;
   c02_w c02%rowtype;
   
   
 cursor c03 is
 select a.cd_dominio cd_dominio, 
        a.vl_dominio vl_dominio, 
        a.CD_EXP_VALOR_DOMINIO ds_valor_dominio 
 from VALOR_DOMINIO a 
 where a.cd_dominio = CD_DOMINIO_W;
 c03_w c03%rowtype;
 
 cursor c04 is
 select tb.CD CD, tb.DS DS, tb.DS_EXP DS_EXP
   from table(get_val_from_field_pck.get_valores(
					(select obter_desc_expressao(
								(select cd_exp_valores
								   from ONTOLOGIA_TABELA_ATRIBUTO
								where nm_tabela =
										NM_TABELA_W
									and nm_atributo =
										NM_ATRIBUTO_W))
					from dual))) tb;
  c04_w c04%rowtype;

begin

  nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

  select  count(*)  
  into count_regs_w 
  from ONTOLOGIA_LISTA_W
  where nm_usuario = nm_usuario_w;
  if(count_regs_w > 0)then
   select sys_context( 'userenv', 'current_schema' ) 
     into context_w 
     from dual;
     
   strQuery_w := 'delete ONTOLOGIA_LISTA_W where nm_usuario = ' ||'''' || nm_usuario_w || '''';
   EXECUTE IMMEDIATE strQuery_w;
  end if;
        
      for c02_w in c02 LOOP
      begin
        NM_TABELA_W := c02_w.nm_tabela;
        DS_TABELA_W := c02_w.ds_tabela;
        NR_NIVEL_W  := 1;
        
        if(cd_funcao_p = 2314)then
            for c01_new_atributes_w in c01_new_atributes LOOP
            begin                       
                NR_NIVEL_W             := 2;
                NR_SEQ_ORD_ATRIBUTO_W  := NR_SEQ_ORD_ATRIBUTO_W + 1;                
                NM_ATRIBUTO_W          := c01_new_atributes_w.NM_ATRIBUTO;
                DS_ATRIBUTO_W          := c01_new_atributes_w.DESCRICAO;
                CD_DOMINIO_W           := c01_new_atributes_w.CD_DOMINIO;
                DS_VALUE_W             := c01_new_atributes_w.ds_valores;
                
                if(CD_DOMINIO_W is not null)then
                    for c03_w in c03 LOOP
                    begin
                      NR_SEQ_ORD_ATRIBUTO_W  := NR_SEQ_ORD_ATRIBUTO_W + 1;
                      DS_DOMINIO_W           := c03_w.ds_valor_dominio;
                      VL_CHAVE_W           := c03_w.vl_dominio;
                      NR_NIVEL_W             := 3;
                       
                      insert into ONTOLOGIA_LISTA_W (
                                                    NM_TABELA,
                                                    NM_ATRIBUTO	,
                                                    DT_ATUALIZACAO	,
                                                    NM_USUARIO	,
                                                    DT_ATUALIZACAO_NREC	,
                                                    NM_USUARIO_NREC	,
                                                    NR_SEQ_ORDENACAO,	
                                                    DS_VALUE	,
                                                    CD_DOMINIO,	
                                                    NR_NIVEL	,
                                                    CD_FUNCAO	,
                                                    DS_TABELA	,
                                                    DS_ATRIBUTO,	
                                                    DS_DOMINIO	,
                                                    VL_CHAVE,
                                                    DS_VALUE_LISTA )                           
                                             values(NM_TABELA_W,
                                                    NM_ATRIBUTO_W,
                                                    sysdate,
                                                    nm_usuario_w,
                                                    sysdate,
                                                    nm_usuario_w,
                                                    NR_SEQ_ORD_ATRIBUTO_W,
                                                    null,
                                                    CD_DOMINIO_W,
                                                    NR_NIVEL_W,
                                                    cd_funcao_p,
                                                    DS_TABELA_W,
                                                    DS_ATRIBUTO_W,
                                                    DS_DOMINIO_W,
                                                    VL_CHAVE_W,
                                                    null);
                     end;
                  end loop;
                end if;
                
                
                if(DS_VALUE_W is not null)then
                for c04_w in c04 LOOP
                    begin
                      NR_SEQ_ORD_ATRIBUTO_W  := NR_SEQ_ORD_ATRIBUTO_W + 1;
                      NR_NIVEL_W             := 3;
                      DS_VALUE_W             := c04_w.DS_EXP;
                      DS_VALUE_LISTA_W       := c04_w.DS;
                      VL_CHAVE_W			  := C04_W.CD;
                       
                      insert into ONTOLOGIA_LISTA_W (
                                                    NM_TABELA,
                                                    NM_ATRIBUTO	,
                                                    DT_ATUALIZACAO	,
                                                    NM_USUARIO	,
                                                    DT_ATUALIZACAO_NREC	,
                                                    NM_USUARIO_NREC	,
                                                    NR_SEQ_ORDENACAO,	
                                                    DS_VALUE	,
                                                    CD_DOMINIO,	
                                                    NR_NIVEL	,
                                                    CD_FUNCAO	,
                                                    DS_TABELA	,
                                                    DS_ATRIBUTO,	
                                                    DS_DOMINIO	,
                                                    VL_CHAVE,
                                                    DS_VALUE_LISTA ) 
                                  values(NM_TABELA_W,
                                                    NM_ATRIBUTO_W,
                                                    sysdate,
                                                    nm_usuario_w,
                                                    sysdate,
                                                    nm_usuario_w,
                                                    NR_SEQ_ORD_ATRIBUTO_W,
                                                    DS_VALUE_W,
                                                    null,
                                                    NR_NIVEL_W,
                                                    cd_funcao_p,
                                                    DS_TABELA_W,
                                                    DS_ATRIBUTO_W,
                                                    null,
                                                    VL_CHAVE_W,
                                                    DS_VALUE_LISTA_W);
                    
                    end;
                  end loop;
                end if;
                                                
                if(CD_DOMINIO_W is null and DS_VALUE_W is null)then                       
                     NR_NIVEL_W             := 2;
                      insert into ONTOLOGIA_LISTA_W (
                                                    NM_TABELA,
                                                    NM_ATRIBUTO	,
                                                    DT_ATUALIZACAO	,
                                                    NM_USUARIO	,
                                                    DT_ATUALIZACAO_NREC	,
                                                    NM_USUARIO_NREC	,
                                                    NR_SEQ_ORDENACAO,	
                                                    DS_VALUE	,
                                                    CD_DOMINIO,	
                                                    NR_NIVEL	,
                                                    CD_FUNCAO	,
                                                    DS_TABELA	,
                                                    DS_ATRIBUTO,	
                                                    DS_DOMINIO	,
                                                    DS_VALUE_LISTA ) 
                      values(NM_TABELA_W,
                                                  NM_ATRIBUTO_W,
                                                  sysdate,
                                                  nm_usuario_w,
                                                  sysdate,
                                                  nm_usuario_w,
                                                  NR_SEQ_ORD_ATRIBUTO_W,
                                                  null,
                                                  null,
                                                  NR_NIVEL_W,
                                                  cd_funcao_p,
                                                  DS_TABELA_W,
                                                  DS_ATRIBUTO_W,
                                                  null,
                                                  null);                
                end if;
              end;
              end loop;                  
        else
              for c01_new_atributes_w in c01_new_atributes LOOP
              begin                       
                NR_NIVEL_W             := 2;
                NR_SEQ_ORD_ATRIBUTO_W  := NR_SEQ_ORD_ATRIBUTO_W + 1;
                
                NM_ATRIBUTO_W          := c01_new_atributes_w.NM_ATRIBUTO;
                DS_ATRIBUTO_W          := c01_new_atributes_w.DESCRICAO;
                CD_DOMINIO_W           := c01_new_atributes_w.CD_DOMINIO;
                DS_VALUE_W             := c01_new_atributes_w.ds_valores;
                
                if(CD_DOMINIO_W is not null)then
                    for c03_w in c03 LOOP
                    begin
                      NR_SEQ_ORD_ATRIBUTO_W  := NR_SEQ_ORD_ATRIBUTO_W + 1;
                      DS_DOMINIO_W           := c03_w.ds_valor_dominio;
                      VL_CHAVE_W           := c03_w.vl_dominio;
                      NR_NIVEL_W             := 3;
                       
                      insert into ONTOLOGIA_LISTA_W (
                                                    NM_TABELA,
                                                    NM_ATRIBUTO	,
                                                    DT_ATUALIZACAO	,
                                                    NM_USUARIO	,
                                                    DT_ATUALIZACAO_NREC	,
                                                    NM_USUARIO_NREC	,
                                                    NR_SEQ_ORDENACAO,	
                                                    DS_VALUE	,
                                                    CD_DOMINIO,	
                                                    NR_NIVEL	,
                                                    CD_FUNCAO	,
                                                    DS_TABELA	,
                                                    DS_ATRIBUTO,	
                                                    DS_DOMINIO	,
                                                    VL_CHAVE,
                                                    DS_VALUE_LISTA )                           
                                             values(NM_TABELA_W,
                                                    NM_ATRIBUTO_W,
                                                    sysdate,
                                                    nm_usuario_w,
                                                    sysdate,
                                                    nm_usuario_w,
                                                    NR_SEQ_ORD_ATRIBUTO_W,
                                                    null,
                                                    CD_DOMINIO_W,
                                                    NR_NIVEL_W,
                                                    cd_funcao_p,
                                                    DS_TABELA_W,
                                                    DS_ATRIBUTO_W,
                                                    DS_DOMINIO_W,
                                                    VL_CHAVE_W,
                                                    null);
                     end;
                  end loop;
                end if;
                
                
                if(DS_VALUE_W is not null)then
                    for c04_w in c04 LOOP
                    begin
                       NR_SEQ_ORD_ATRIBUTO_W  := NR_SEQ_ORD_ATRIBUTO_W + 1;
                       NR_NIVEL_W             := 3;
                       DS_VALUE_W             := c04_w.DS_EXP;
                       DS_VALUE_LISTA_W       := c04_w.DS;
                      VL_CHAVE_W			  := C04_W.CD;
                       
                      insert into ONTOLOGIA_LISTA_W (
                                                    NM_TABELA,
                                                    NM_ATRIBUTO	,
                                                    DT_ATUALIZACAO	,
                                                    NM_USUARIO	,
                                                    DT_ATUALIZACAO_NREC	,
                                                    NM_USUARIO_NREC	,
                                                    NR_SEQ_ORDENACAO,	
                                                    DS_VALUE	,
                                                    CD_DOMINIO,	
                                                    NR_NIVEL	,
                                                    CD_FUNCAO	,
                                                    DS_TABELA	,
                                                    DS_ATRIBUTO,	
                                                    DS_DOMINIO	,
                                                    VL_CHAVE,
                                                    DS_VALUE_LISTA ) 
                       values(NM_TABELA_W,
                                                    NM_ATRIBUTO_W,
                                                    sysdate,
                                                    nm_usuario_w,
                                                    sysdate,
                                                    nm_usuario_w,
                                                    NR_SEQ_ORD_ATRIBUTO_W,
                                                    DS_VALUE_W,
                                                    null,
                                                    NR_NIVEL_W,
                                                    cd_funcao_p,
                                                    DS_TABELA_W,
                                                    DS_ATRIBUTO_W,
                                                    null,
                                                    VL_CHAVE_W,
                                                    DS_VALUE_LISTA_W);
                    
                    end;
                  end loop;
                end if;
                
                if(CD_DOMINIO_W is null and DS_VALUE_W is null)then                       
                     NR_NIVEL_W             := 2;
                      insert into ONTOLOGIA_LISTA_W (
                                                    NM_TABELA,
                                                    NM_ATRIBUTO	,
                                                    DT_ATUALIZACAO	,
                                                    NM_USUARIO	,
                                                    DT_ATUALIZACAO_NREC	,
                                                    NM_USUARIO_NREC	,
                                                    NR_SEQ_ORDENACAO,	
                                                    DS_VALUE	,
                                                    CD_DOMINIO,	
                                                    NR_NIVEL	,
                                                    CD_FUNCAO	,
                                                    DS_TABELA	,
                                                    DS_ATRIBUTO,	
                                                    DS_DOMINIO	,
                                                    DS_VALUE_LISTA ) 
                                values(NM_TABELA_W,
                                                  NM_ATRIBUTO_W,
                                                  sysdate,
                                                  nm_usuario_w,
                                                  sysdate,
                                                  nm_usuario_w,
                                                  NR_SEQ_ORD_ATRIBUTO_W,
                                                  null,
                                                  null,
                                                  NR_NIVEL_W,
                                                  cd_funcao_p,
                                                  DS_TABELA_W,
                                                  DS_ATRIBUTO_W,
                                                  null,
                                                  null);
                
                end if;                
              end;
              
               end loop;
            end if;
      end;
    end loop;
commit;
end create_ontology_structure_tree;
/
