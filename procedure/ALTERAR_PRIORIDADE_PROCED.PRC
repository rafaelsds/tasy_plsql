create or replace
procedure Alterar_prioridade_proced(	nr_seq_prioridade_p	number,
					nr_prescricao_p		number,
					nr_seq_proced_p		number,
					nm_usuario_p		Varchar2) is 

begin

update	prescr_procedimento
set	nr_seq_prioridade	= nr_seq_prioridade_p
where	nr_prescricao		= nr_prescricao_p
and	nr_sequencia		= nr_seq_proced_p;

commit;

end Alterar_prioridade_proced;
/