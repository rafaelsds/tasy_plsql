create or replace 
procedure pls_desfazer_aplicar_recalc(	nr_seq_lote_p			pls_lote_recalculo.nr_sequencia%type,
					ie_apenas_fins_contabeis_p	varchar2 default null,	
					nm_usuario_p			usuario.nm_usuario%type,
					cd_estabelecimento_p		estabelecimento.cd_estabelecimento%type) is

nr_seq_procedimento_w		pls_item_recalculo.nr_seq_procedimento%type;
nr_seq_material_w		pls_item_recalculo.nr_seq_material%type;
vl_lib_anterior_w		number(15,2)	:= 0;
vl_item_w			number(15,2)	:= 0;
aux_w				pls_integer;
ie_recalcular_copartic_w	varchar2(1);
ie_recalcular_item_w      	varchar2(1);
ie_recalcular_pos_w       	varchar2(1);
qt_item_w			pls_integer;
nr_seq_protocolo_conta_w	pls_protocolo_conta.nr_sequencia%type;
nr_lote_contabil_w		pls_protocolo_conta.nr_lote_contabil%type;
nr_lote_contab_pag_w		pls_protocolo_conta.nr_lote_contab_pag%type;
nr_lote_prov_copartic_w		pls_protocolo_conta.nr_lote_prov_copartic%type;
qt_pgto_proc_mat_w		pls_integer := 0;
nr_seq_conta_rec_w		pls_conta.nr_sequencia%type;
vl_tot_partic_w			pls_proc_participante.vl_participante%type;
vl_dif_partic_w			number(15,2);
vl_calculado_w			pls_conta_proc.vl_procedimento%type;
vl_liberado_atual_w		pls_conta_proc.vl_liberado%type;
ie_somente_contabil_w		pls_lote_recalculo.ie_somente_contabil%type;
nr_lote_contabilizado_w		pls_conta_medica_resumo.nr_lote_contabil_prov%type;
ie_tipo_regra_w			pls_regra_lote_recalculo.ie_tipo_regra%type;
nr_seq_regra_w			pls_lote_recalculo.nr_seq_regra%type;
nr_seq_criterio_w		pls_item_recalculo.nr_regra_recalculo%type;
nr_seq_resumo_w			pls_item_recalculo.nr_seq_conta_resumo%type;
ie_verificar_lote_pgto	varchar2(1) := 'S';
qt_criterios_prest_pag_w	pls_integer := 0;
vl_atual_rec_w			pls_item_recalculo.vl_item_atual%type;	
dt_mes_competencia_w		pls_protocolo_conta.dt_mes_competencia%type;
ds_tipo_lote_contabil_w		tipo_lote_contabil.ds_tipo_lote_contabil%type;
Cursor C01 is
	select	b.nr_seq_procedimento,
		b.nr_seq_material,
		b.vl_item,
		a.nr_seq_protocolo,
		a.nr_seq_conta,
		b.nr_regra_recalculo,
		b.nr_seq_conta_resumo,
		b.vl_item_atual vl_atual
	from	pls_item_recalculo	b,
		pls_conta_recalculo	a
	where	a.nr_sequencia = b.nr_seq_conta
	and	a.nr_seq_lote = nr_seq_lote_p;

Cursor C02 is
	select	a.nr_seq_conta,
		(select	max(nr_seq_protocolo)
		from	pls_conta x
		where	x.nr_sequencia = a.nr_seq_conta) nr_seq_protocolo
	from	pls_conta_recalculo a
	where	a.nr_seq_lote = nr_seq_lote_p
	group by a.nr_seq_conta;

Cursor C03 is
	select	nr_seq_prestador_pag
	from	pls_criterio_recalculo
	where	nr_sequencia = nr_seq_criterio_w;
	
begin

select	nvl(max(a.ie_recalcular_copartic), 'N'),
	nvl(max(a.ie_recalcular_item), 'S'),
	nvl(max(a.ie_recalcular_pos), 'N'),
	nvl(max(a.ie_somente_contabil), 'N'),
	max(nr_seq_regra)
into	ie_recalcular_copartic_w,
	ie_recalcular_item_w,
	ie_recalcular_pos_w,
	ie_somente_contabil_w,
	nr_seq_regra_w
from	pls_lote_recalculo a
where	a.nr_sequencia = nr_seq_lote_p;

select	max(x.ie_tipo_regra)
into	ie_tipo_regra_w
from	pls_regra_lote_recalculo x
where	x.nr_sequencia = nr_seq_regra_w;

select	count(1)
into	aux_w
from	sip_nv_dados a
where	a.ie_conta_enviada_ans = 'S'
and	a.nr_seq_conta in (	select	nr_seq_conta
				from	pls_conta_recalculo
				where	nr_seq_lote = nr_seq_lote_p)
and	exists(	select	1
		from	pls_lote_sip b
		where	b.nr_sequencia = a.nr_seq_lote_sip
		and	b.dt_envio is not null);

if	(aux_w > 0 and ie_somente_contabil_w <> 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(338028);
end if;

open C01;
loop
fetch C01 into
	nr_seq_procedimento_w,
	nr_seq_material_w,
	vl_lib_anterior_w,
	nr_seq_protocolo_conta_w,
	nr_seq_conta_rec_w,
	nr_seq_criterio_w,
	nr_seq_resumo_w,	
	vl_atual_rec_w;
exit when C01%notfound;
--aldellandrea os 856156 verifica se o item j� n�o est� em algum lote cont�bil
	select	max(nr_lote_contabil),
		max(nr_lote_contab_pag),
		max(nr_lote_prov_copartic)
	into	nr_lote_contabil_w,
		nr_lote_contab_pag_w,
		nr_lote_prov_copartic_w
	from	pls_protocolo_conta
	where	nr_sequencia = nr_seq_protocolo_conta_w;

--caso estaja em algum lote cont�bil n�o permite desfazer o recalculo
	if	(nvl(nr_lote_contabil_w,0) <> 0 and ie_somente_contabil_w <> 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(335018, 'NR_LOTE_CONTABIL_W=' || nr_lote_contabil_w);
	end if;
	if	(nvl(nr_lote_contab_pag_w,0) <> 0 and ie_somente_contabil_w <> 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(335020, 'NR_LOTE_CONTABIL_W=' || nr_lote_contab_pag_w);
	end if;
	
	select	nvl(max(nr_lote_contabil_prov),0) nr_lote_contabil
	into	nr_lote_contabilizado_w
	from	pls_conta_medica_resumo
	where	nr_seq_protocolo = nr_seq_protocolo_conta_w
	and     ie_situacao = 'A'
	and     ie_tipo_item != 'I'
	and	nvl(nr_lote_contabil_prov,0 ) <> 0;
	
	if	(nr_lote_contabilizado_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(324839, 'NR_LOTE_CONTABILIZADO=' || nr_lote_contabilizado_w);
	end if;
--aldellandrea fim da altera��o os 856156

	-- caso n�o seja para recalcular o item e o tipo de regra for 4 ent�o n�o precisa validar o pagamento
	if	(ie_recalcular_item_w = 'S') or 
		((ie_recalcular_item_w = 'N') and (ie_tipo_regra_w <> 4)) then
		
		if	(ie_tipo_regra_w = 1 and nr_seq_procedimento_w is not null)   then
			
			qt_pgto_proc_mat_w := 0;
			ie_verificar_lote_pgto := 'S';
			for r_c03_w in c03 loop
			
				if	(r_c03_w.nr_seq_prestador_pag is not null) then
				
					--Se entrar aqui, ent�o � por que tem crit�rio por prestador de pagto, ent�o nos procedimentos, verifica se tem
					--lote de pagto para o registro do resumo espec�fico  do prestador
					ie_verificar_lote_pgto := 'N';
					
					select	sum(qt)
					into	qt_pgto_proc_mat_w
					from	(
						select	count(1) qt
						from 	pls_conta_medica_resumo
						where	nr_seq_conta = nr_seq_conta_rec_w
						and	nr_seq_conta_proc = nr_seq_procedimento_w
						and	nr_seq_prestador_pgto = r_c03_w.nr_seq_prestador_pag
						and	ie_situacao = 'A'
						and	ie_tipo_item <> 'I'
						and 	nr_seq_lote_pgto is not null
						and	rownum = 1
						union all
						select	count(1) qt
						from	pls_conta_medica_resumo
						where	nr_seq_conta = nr_seq_conta_rec_w
						and	nr_seq_conta_proc = nr_seq_procedimento_w
						and	nr_seq_prestador_pgto = r_c03_w.nr_seq_prestador_pag
						and	ie_situacao = 'A'
						and	ie_tipo_item <> 'I'
						and 	nr_seq_pp_lote is not null
						and	rownum = 1
					);	
			
				end if;
			end loop;
		
		end if;
		
		--Verifica se os itens j� est�o em um lote de pagamento
		--Se ie_verificar_lote_pgto for igual a N, ent�o j� foram verificados os registros do resumo dos prestdores pgto presentes no criterio da regra de recalculo. 
		if 	(nvl(nr_seq_procedimento_w,0) > 0 and ie_verificar_lote_pgto = 'S') then

			select	sum(qt)
			into	qt_pgto_proc_mat_w
			from	(
				select	count(1) qt
				from 	pls_conta_medica_resumo
				where	nr_seq_conta = nr_seq_conta_rec_w
				and	nr_seq_conta_proc = nr_seq_procedimento_w
				and	ie_situacao = 'A'
				and	ie_tipo_item <> 'I'
				and 	nr_seq_lote_pgto is not null
				and	rownum = 1
				union all
				select	count(1) qt
				from	pls_conta_medica_resumo
				where	nr_seq_conta = nr_seq_conta_rec_w
				and	nr_seq_conta_proc = nr_seq_procedimento_w
				and	ie_situacao = 'A'
				and	ie_tipo_item <> 'I'
				and 	nr_seq_pp_lote is not null
				and	rownum = 1
			);
		elsif	(nvl(nr_seq_material_w,0) > 0) then

			select	sum(qt)
			into	qt_pgto_proc_mat_w
			from	(
				select	count(1) qt
				from 	pls_conta_medica_resumo
				where	nr_seq_conta = nr_seq_conta_rec_w
				and	nr_seq_conta_mat = nr_seq_material_w
				and	ie_situacao = 'A'
				and	ie_tipo_item <> 'I'
				and 	nr_seq_lote_pgto is not null
				and	rownum = 1
				union all
				select	count(1) qt
				from	pls_conta_medica_resumo
				where	nr_seq_conta = nr_seq_conta_rec_w
				and	nr_seq_conta_mat = nr_seq_material_w
				and	ie_situacao = 'A'
				and	ie_tipo_item <> 'I'
				and 	nr_seq_pp_lote is not null
				and	rownum = 1
			);
		end if;
		
		if	(qt_pgto_proc_mat_w > 0 and ie_somente_contabil_w <> 'S') then
			/* N�o foi poss�vel desfazer o recalculo, j� existem itens vinculados a um lote de pagamento. */
			wheb_mensagem_pck.exibir_mensagem_abort(334860);
		end if;
	end if;

	--Modificado aqui, pois se esta desfazendo a aplica��o do ajuste e o valor atual � diferente do original, sempre deve voltar os valores
	--Somente n�o fazendo nada quando estiver parametrizado para somente valores cont�beis
	if	(nvl(ie_apenas_fins_contabeis_p,'N') <> 'S')then

		if	(nvl(nr_seq_procedimento_w,0) > 0) then

			select	nvl(max(vl_lib_original),0),
				nvl(max(vl_liberado),0)
			into	vl_item_w,
				vl_liberado_atual_w
			from	pls_conta_proc
			where	nr_sequencia = nr_seq_procedimento_w;
			
			if ( vl_item_w = 0 ) then
			
				/*NEcessidade de tratamento especial para uso do ajuste de valoriza��o com contas n�o fechadas, pois nesse caso, n�o ter� ainda o valor liberado original. Essa utiliza��o por conta n�o fecha, quje n�o � ideal de ocorrer, est� sendo realizada pela
				USJRP com rateio por valor fixo*/
				if (ie_tipo_regra_w = 3) then
					
					select	max(nr_seq_prestador_pag)
					into	qt_criterios_prest_pag_w
					from	pls_criterio_recalculo
					where	nr_sequencia = nr_seq_criterio_w
					and	nr_seq_prestador_pag is not null;
					
					--N�o pode ter crit�rios por prestador de pagamento, pois nesse caso, talvez apenas uma parte de um valor de procedimento seja reajustada.
					if (qt_criterios_prest_pag_w is null) then
						vl_item_w := vl_atual_rec_w;
					end if;
				
				end if;
			
			end if;

			if	(vl_liberado_atual_w <> vl_item_w) and
				(vl_liberado_atual_w > 0) then

				update	pls_conta_proc
				set	vl_liberado		= vl_item_w,
					vl_pag_medico_conta 	= decode(vl_pag_medico_conta, 0, 0, vl_item_w),
					vl_total_partic 	= 0,
					vl_prestador 		= vl_item_w,
					vl_unitario 		= dividir(vl_item_w,qt_procedimento),
					vl_glosa 		= vl_procedimento_imp - vl_item_w
				where	nr_sequencia 		= nr_seq_procedimento_w;

				update	pls_conta_proc
				set	vl_glosa = 0
				where	nr_sequencia = nr_seq_procedimento_w
				and	vl_glosa < 0;

				select	max(vl_procedimento)
				into	vl_calculado_w
				from	pls_conta_proc
				where	nr_sequencia = nr_seq_procedimento_w;

				update	pls_proc_participante
				set	vl_participante			= dividir((vl_item_w * vl_calculado),vl_calculado_w)
				where	nr_seq_conta_proc		= nr_seq_procedimento_w
				and	((ie_status is null) or (ie_status != 'C'))
				and	((ie_gerada_cta_honorario is null) or (ie_gerada_cta_honorario <> 'S'));

				select 	nvl(sum(vl_participante),0)
				into	vl_tot_partic_w
				from	pls_proc_participante
				where	nr_seq_conta_proc		= nr_seq_procedimento_w
				and	((ie_status is null) or (ie_status != 'C'))
				and	((ie_gerada_cta_honorario is null) or (ie_gerada_cta_honorario <> 'S'));

				if	(vl_tot_partic_w >  vl_item_w) then
					vl_dif_partic_w	:= vl_tot_partic_w - vl_item_w;

					update	pls_proc_participante	a
					set	a.vl_participante	= a.vl_participante - vl_dif_partic_w
					where	a.nr_sequencia	= (	select	max(x.nr_sequencia)
									from	pls_proc_participante	x
									where	x.nr_seq_conta_proc	= nr_seq_procedimento_w
									and	((x.ie_status is null) or (x.ie_status != 'C'))
									and	((x.ie_gerada_cta_honorario is null) or (x.ie_gerada_cta_honorario <> 'S'))
									and	x.vl_participante >= vl_dif_partic_w);
				end if;

				insert into pls_conta_proc_hist(
					nr_sequencia, nr_seq_proc, nr_seq_lote,
					dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, vl_liberado, vl_lib_anterior
				) values (
					pls_conta_proc_hist_seq.nextval, nr_seq_procedimento_w, nr_seq_lote_p,
					sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, vl_item_w, vl_lib_anterior_w
				);

				select	count(1)
				into	qt_item_w
				from	pls_conta_proc
				where	nr_sequencia = nr_seq_procedimento_w;

				if	( qt_item_w > 0) then
						
					select	max(nr_seq_prestador_pag)
					into	qt_criterios_prest_pag_w
					from	pls_criterio_recalculo
					where	nr_sequencia = nr_seq_criterio_w
					and	nr_seq_prestador_pag is not null;
				
					--Se for recurso proprio e tiver criterio por prestador de pagto, ent�o n�o pode chamar a pls_atualiza_conta_resumo_item e sim atualizar o registro espec�fico
					if	( ie_tipo_regra_w = 1 and qt_criterios_prest_pag_w > 0)   then
						
						update	pls_conta_medica_resumo
						set 	vl_liberado_ant	= null,
							vl_liberado 	= vl_atual_rec_w,
							vl_lib_original	= vl_atual_rec_w
						where	nr_seq_conta_proc 	= nr_seq_procedimento_w
						and	nr_sequencia 		= nr_seq_resumo_w;

						update  pls_conta_medica_resumo
						set	vl_unitario 		= dividir(vl_liberado, qt_item),
							vl_apres_ind 		= vl_lib_original - vl_glosa
						where	nr_seq_conta_proc 	= nr_seq_procedimento_w
						and	nr_sequencia 		= nr_seq_resumo_w;
										
					else
						pls_atualiza_conta_resumo_item(	nr_seq_procedimento_w, 'P', nm_usuario_p, 'N');
					end if;
				end if;
			end if;
		elsif	(nvl(nr_seq_material_w,0) > 0) then

			select	nvl(max(vl_lib_original),0),
				nvl(max(vl_liberado),0)
			into	vl_item_w,
				vl_liberado_atual_w
			from	pls_conta_mat
			where	nr_sequencia = nr_seq_material_w;
			
			if ( vl_item_w = 0 ) then
			
				/*NEcessidade de tratamento especial para uso do ajuste de valoriza��o com contas n�o fechadas, pois nesse caso, n�o ter� ainda o valor liberado original. Essa utiliza��o por conta n�o fecha, quje n�o � ideal de ocorrer, est� sendo realizada pela
				USJRP com rateio por valor fixo*/
				if (ie_tipo_regra_w = 3) then					
					
					vl_item_w := vl_atual_rec_w;
		
				end if;
			
			end if;

			if	(vl_liberado_atual_w <> vl_item_w) and
				(vl_liberado_atual_w > 0) then

				update	pls_conta_mat
				set	vl_liberado			= vl_item_w,
					nr_seq_regra_recalculo 		= null,
					nr_seq_regra_preco_recalc 	= null,
					vl_unitario			= dividir(vl_item_w,qt_material),
					vl_glosa			= vl_material_imp - vl_item_w
				where	nr_sequencia			= nr_seq_material_w;

				update	pls_conta_mat
				set	vl_glosa	= 0
				where	nr_sequencia	= nr_seq_material_w
				and	vl_glosa	< 0;

				insert into pls_conta_mat_hist(
					nr_sequencia, nr_seq_material, nr_seq_lote,
					dt_atualizacao, nm_usuario, dt_atualizacao_nrec,
					nm_usuario_nrec, vl_liberado, vl_lib_anterior
				) values (
					pls_conta_mat_hist_seq.nextval, nr_seq_material_w, nr_seq_lote_p,
					sysdate, nm_usuario_p, sysdate,
					nm_usuario_p, vl_item_w, vl_lib_anterior_w
				);

				select	count(1)
	 			into	qt_item_w
				from	pls_conta_mat
				where	nr_sequencia = nr_seq_material_w;

				if	(qt_item_w > 0) then
					pls_atualiza_conta_resumo_item(	nr_seq_material_w, 'M', nm_usuario_p,
									'N');
				end if;
			end if;
		end if;
	end if;
end loop;
close C01;

if	(((ie_recalcular_copartic_w = 'S') or
	(ie_recalcular_pos_w = 'S')) and
	(ie_recalcular_item_w = 'S')) or
	((nvl(ie_recalcular_item_w,'S') = 'S') and
	((nvl(ie_recalcular_copartic_w,'N') = 'N') or
	(nvl(ie_recalcular_pos_w,'N') = 'N')))then

	for r_c02_w in c02 loop

		-- S� faz algo se encontrou o protocolo
		if	(r_c02_w.nr_seq_protocolo is not null) then

			pls_atualiza_valor_conta(	r_c02_w.nr_seq_conta, nm_usuario_p);
			
			--Atualiza evento de pagamento(Pagamento novo).					
			pls_filtro_regra_event_cta_pck.gerencia_regra_filtro(	null, r_c02_w.nr_seq_conta, cd_estabelecimento_p, nm_usuario_p);

			pls_cta_consistir_pck.gerar_resumo_conta(	null, null, null, 
									r_c02_w.nr_seq_conta, nm_usuario_p, cd_estabelecimento_p);

			pls_gerar_valores_protocolo(	r_c02_w.nr_seq_protocolo, nm_usuario_p);
			
			select	max(dt_mes_competencia)
			into	dt_mes_competencia_w
			from	pls_protocolo_conta
			where	nr_Sequencia = r_c02_w.nr_seq_protocolo;
			
			pls_atualizar_contab_val_adic (dt_mes_competencia_w, nm_usuario_p, ds_tipo_lote_contabil_w, r_c02_w.nr_seq_protocolo, null);
		end if;
		
	end loop;
end if;

update	pls_lote_recalculo
set	dt_aplicacao	= null,
	dt_fim_recalculo = null,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate,
	ie_somente_contabil = null
where	nr_sequencia	= nr_seq_lote_p;

commit;

end pls_desfazer_aplicar_recalc;
/
