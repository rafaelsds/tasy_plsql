create or replace
procedure pls_gerar_cartao_benecamp
			(	nr_seq_lote_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

nr_seq_segurado_w		number(10);
ds_header_w			varchar2(4000);
ds_sub_header_w			varchar2(4000);
cd_usuario_plano_w		varchar2(30);
nm_beneficiario_w		varchar2(250);
dt_nascimento_w			varchar2(20);
ds_abrangencia_w		varchar2(50);
ds_registro_ans_w		varchar2(50);
ds_acomodacao_w			varchar2(50);
nm_empresa_w			varchar2(60);
ds_carencia_ww			varchar2(4000);	
ds_carencia_w			varchar2(4000);
ds_observacao_1_w		varchar2(100) := ' ';
ds_trilha_2_w			varchar2(40);
ds_trilha_1_w			varchar2(100);
ds_segmentacao_w		varchar2(100);
nr_seq_plano_w			number(10);

Cursor C01 is
	select	b.nr_sequencia
	from	pls_segurado_carteira	a,
		pls_segurado		b,
		pls_carteira_emissao	c,
		pls_lote_carteira	d
	where	a.nr_seq_segurado	= b.nr_sequencia
	and	c.nr_seq_seg_carteira	= a.nr_sequencia
	and	c.nr_seq_lote		= d.nr_sequencia
	and	d.nr_sequencia		= nr_seq_lote_p;

begin

ds_header_w	:= 	'C�DIGO_BENEFICI�RIO;BENEFICI�RIO;PRODUTO;ABRAMG�NCIA;ACOMODA��O;DT_NASC;OBSTETRICIA;PAGADOR;CAR�NCIA_1;'  ||
			'CAR�NCIA_2;CAR�NCIA_3;CAR�NCIA_4;CAR�NCIA_5;CAR�NCIA_6;CAR�NCIA_7;CAR�NCIA_8;CAR�NCIA_9;CAR�NCIA_10;'||
			'CAR�NCIA_11;CAR�NCIA_12;MENSAGEM;TARJA_MAGN�TICA_TRILHA1;TARJA_MAGN�TICA_TRILHA2';


delete	w_pls_interface_carteira
where	nr_seq_lote	= nr_seq_lote_p;

insert into w_pls_interface_carteira
	(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
		nr_seq_lote,ds_header,ie_tipo_reg)
values	(	w_pls_interface_carteira_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
		nr_seq_lote_p,ds_header_w,1);

open C01;
loop
fetch C01 into
	nr_seq_segurado_w;
exit when C01%notfound;
	begin
	ds_observacao_1_w	:= ' ';
	
	select	c.cd_usuario_plano,
		a.nm_pessoa_fisica,
		to_char(a.dt_nascimento,'dd/mm/yyyy'),
		decode(d.ie_acomodacao,'I','Individual','C','Coletivo','N','Nenhum'),
		decode(d.ie_regulamentacao,'R','N�o Regulamentado','Regulamentado'),
		upper(Obter_Valor_Dominio(1667,d.ie_abrangencia)),
		replace(ds_trilha2,';',''),
		ds_trilha1,
		decode(a.ie_sexo,'M','Nao',decode(d.ie_segmentacao,'1','Nao','2','Sim','3','Nao','4','Nao','5','Sim','6','Sim','7','Nao','8','Nao','9','Sim','10','Nao','11','Sim','12','Nao')),
		decode(f.cd_cgc, null, obter_nome_pf(f.cd_pessoa_fisica), obter_nome_fantasia_pj(f.cd_cgc)) nm_pagador,
		d.nr_sequencia
	into	cd_usuario_plano_w,
		nm_beneficiario_w,
		dt_nascimento_w,
		ds_acomodacao_w,
		ds_registro_ans_w,
		ds_abrangencia_w,
		ds_trilha_2_w,
		ds_trilha_1_w,
		ds_segmentacao_w,
		nm_empresa_w,
		nr_seq_plano_w
	from	pls_contrato_pagador	f,
		pls_plano		d,
		pls_segurado_carteira	c,
		pls_segurado		b,
		pessoa_fisica		a
	where	c.nr_seq_segurado	= b.nr_sequencia
	and	b.nr_seq_plano		= d.nr_sequencia
	and	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	b.nr_seq_pagador	= f.nr_sequencia
	and	b.nr_sequencia		= nr_seq_segurado_w;
	
	ds_carencia_w	:= '';
	
	for i in 1..12 loop
		begin
		
		ds_carencia_ww	:= nvl(substr(pls_obter_dados_cart_unimed(nr_seq_segurado_w, nr_seq_plano_w,'CA',i),1,255),'-');
		 
		ds_carencia_ww	:= replace(ds_carencia_ww,';',' ');
		
		if	(i < 12) then
			ds_carencia_w	:= ds_carencia_w ||ds_carencia_ww||';';
		else
			ds_carencia_w	:= ds_carencia_w ||ds_carencia_ww;
		end if;
		
		end;
	end loop;
	
	if	(nr_seq_plano_w not in (172,176)) then
		select	max(a.ds_mensagem)
		into 	ds_observacao_1_w
		from 	pls_plano_mensagem_cart a
		where 	a.nr_seq_plano 		= nr_seq_plano_w
		and 	sysdate between a.dt_inicio_vigencia and nvl(a.dt_fim_vigencia, sysdate);
	end if;
	
	insert into w_pls_interface_carteira
		(	nr_sequencia,dt_atualizacao,nm_usuario,dt_atualizacao_nrec,nm_usuario_nrec,
			nr_seq_lote,ie_tipo_reg,cd_usuario_plano,dt_nascimento,nm_beneficiario,
			ds_registro_ans,ds_abrangencia,ds_acomodacao,ds_segmentacao,ds_estipulante,
			ds_carencia,ds_observacao,ds_trilha_2,ds_trilha_1,nr_seq_segurado)
	values	(	w_pls_interface_carteira_seq.nextval,sysdate,nm_usuario_p,sysdate,nm_usuario_p,
			nr_seq_lote_p,2,cd_usuario_plano_w,dt_nascimento_w,nm_beneficiario_w,
			ds_registro_ans_w,ds_abrangencia_w,ds_acomodacao_w,ds_segmentacao_w,nm_empresa_w,
			ds_carencia_w,ds_observacao_1_w,ds_trilha_2_w,ds_trilha_1_w,nr_seq_segurado_w);
	end;
end loop;
close C01;

commit;

end pls_gerar_cartao_benecamp;
/