CREATE OR REPLACE PROCEDURE PLS_GERAR_ARQ_FSFX_LI016_D
            (    nr_sequencia_p        number,
                ds_local_p        varchar2,
                nm_arquivo_p         varchar2) is

ds_erro_w            varchar2(255);
ds_conteudo_w            varchar2(4000);
arq_texto_w            utl_file.file_type;
DT_REF_MENSALIDADE_w        date;
vl_beneficiario_w        number(15,2);
vl_empresa_w            number(15,2);
nr_seq_conta_w            number(10);
dt_atendimento_w        date;
cd_guia_w            varchar2(20);
nr_seq_prestador_w        number(10);
ds_endereco_w            varchar2(255);
ds_bairro_w            varchar2(255);
uf_sg_w                varchar2(255);
cd_cep_w            varchar2(255);
ds_especialidade_w        varchar2(255);
nm_prestador_w            varchar2(255);
ds_cidade_w            varchar2(255);
nr_seq_item_w            number(10);
nr_seq_cbo_saude_exec_w        number(10);

t_ds_conteudo_w            dbms_sql.varchar2_table;
index_w                number(10) := 0;

Cursor C01 is
    select    c.cd_operadora_empresa,
        nvl(d.cd_sistema_ant,d.cd_pessoa_fisica) cd_sistema_ant,
        lpad(nvl(c.CD_MATRICULA_ESTIPULANTE,'0'),8,'0') CD_MATRICULA_ESTIPULANTE,
        d.nm_pessoa_fisica,
        c.ie_titularidade,
        decode(c.nr_seq_plano,103,'A',decode(c.nr_seq_plano,104,'B',decode(c.nr_seq_plano,106,'C',(select max(x.CD_CODIGO)
          from    PLS_CONTRATO_CODIGO x
          where    x.nr_seq_contrato = c.nr_seq_contrato
          and    x.NR_SEQ_TIPO_CODIGO = 3)))) tp_plano,
        decode(c.nr_seq_plano,45,'S',decode(c.nr_seq_plano,103,'N',decode(c.nr_seq_plano,104,'N',decode(c.nr_seq_plano,106,'N',' ')))) tp_plano_2,
        f.CD_PROCED_MAT,
        f.DS_PROCED_MAT,
        f.QT_COBRADA,
        e.CD_COBRANCA,
        f.NR_SEQ_CONTA_COPARTICIPACAO,
        f.VL_COPARTICIPACAO vl_servico,
        null nr_seq_mensalidade_seg_item,
        d.cd_pessoa_fisica
    from    pessoa_fisica        d,
        pls_segurado        c,
        pls_plano        b,
        PLS_AME_LOTE_REM_VALOR    a,
        PLS_AME_LOTE_REM_VALOR_DET e,
        PLS_AME_LOTE_REM_VALOR_COP f
    where    d.cd_pessoa_fisica    = c.cd_pessoa_fisica
    and    a.nr_seq_segurado    = c.nr_sequencia
    and    c.nr_seq_plano        = b.nr_sequencia
    and    e.NR_SEQ_LOTE_REM_VALOR    = a.nr_sequencia
    and    f.NR_SEQ_REM_VALOR_DETALHE = e.nr_sequencia
    and    a.NR_SEQ_LOTE_REM_ARQ    = nr_sequencia_p
    union all
    select    c.cd_operadora_empresa,
        nvl(d.cd_sistema_ant,d.cd_pessoa_fisica) cd_sistema_ant,
        lpad(nvl(c.CD_MATRICULA_ESTIPULANTE,'0'),8,'0') CD_MATRICULA_ESTIPULANTE,
        d.nm_pessoa_fisica,
        c.ie_titularidade,
        decode(c.nr_seq_plano,103,'A',decode(c.nr_seq_plano,104,'B',decode(c.nr_seq_plano,106,'C',(select max(x.CD_CODIGO)
          from    PLS_CONTRATO_CODIGO x
          where    x.nr_seq_contrato = c.nr_seq_contrato
          and    x.NR_SEQ_TIPO_CODIGO = 3)))) tp_plano,
        decode(c.nr_seq_plano,45,'S',decode(c.nr_seq_plano,103,'N',decode(c.nr_seq_plano,104,'N',decode(c.nr_seq_plano,106,'N',' ')))) tp_plano_2,
        '' CD_PROCED_MAT,
        '' DS_PROCED_MAT,
        0 QT_COBRADA,
        e.CD_COBRANCA,
        null NR_SEQ_CONTA_COPARTICIPACAO,
        e.vl_detalhe vl_servico,
        f.NR_SEQUENCIA,
        d.cd_pessoa_fisica
    from    pessoa_fisica        d,
        pls_segurado        c,
        pls_plano        b,
        PLS_AME_LOTE_REM_VALOR    a,
        PLS_AME_LOTE_REM_VALOR_DET e,
        PLS_MENSALIDADE_SEG_ITEM f
    where    d.cd_pessoa_fisica    = c.cd_pessoa_fisica
    and    a.nr_seq_segurado    = c.nr_sequencia
    and    c.nr_seq_plano        = b.nr_sequencia
    and    e.NR_SEQ_LOTE_REM_VALOR    = a.nr_sequencia
    and    f.nr_seq_mensalidade_seg = a.nr_seq_mensalidade_seg
    and    f.ie_tipo_item        = '13'
    and    e.ie_tipo_item        = '13'
    and    a.NR_SEQ_LOTE_REM_ARQ    = nr_sequencia_p;

begin

select    b.DT_REF_MENSALIDADE
into    DT_REF_MENSALIDADE_w
from    PLS_AME_LOTE_REM_ARQUIVO    d,
    PLS_AME_LOTE_REM_DESTINO    c,
    PLS_AME_LOTE_REMESSA        b,
    PLS_AME_REGRA_GERACAO        a
where    d.NR_SEQ_LOTE_REM_DEST        = c.nr_sequencia
and    c.NR_SEQ_LOTE_REM        = b.nr_sequencia
and    b.NR_SEQ_REGRA_GERACAO        = a.nr_sequencia
and    d.nr_sequencia            = nr_sequencia_p;


begin
arq_texto_w := utl_file.fopen(ds_local_p,nm_arquivo_p,'W');
exception
when others then
    if (sqlcode = -29289) then
        ds_erro_w  := 'O acesso ao arquivo foi negado pelo sistema operacional (access_denied).';
    elsif (sqlcode = -29298) then
        ds_erro_w  := 'O arquivo foi aberto usando FOPEN_NCHAR,  mas efetuaram-se opera��es de I/O usando fun��es nonchar comos PUTF ou GET_LINE (charsetmismatch).';
    elsif (sqlcode = -29291) then
        ds_erro_w  := 'N�o foi poss�vel apagar o arquivo (delete_failed).';
    elsif (sqlcode = -29286) then
        ds_erro_w  := 'Erro interno desconhecido no package UTL_FILE (internal_error).';
    elsif (sqlcode = -29282) then
        ds_erro_w  := 'O handle do arquivo n�o existe (invalid_filehandle).';
    elsif (sqlcode = -29288) then
        ds_erro_w  := 'O arquivo com o nome especificado n�o foi encontrado neste local (invalid_filename).';
    elsif (sqlcode = -29287) then
        ds_erro_w  := 'O valor de MAX_LINESIZE para FOPEN() � inv�lido; deveria estar na faixa de 1 a 32767 (invalid_maxlinesize).';
    elsif (sqlcode = -29281) then
        ds_erro_w  := 'O par�metro open_mode na chamda FOPEN � inv�lido (invalid_mode).';
    elsif (sqlcode = -29290) then
        ds_erro_w  := 'O par�metro ABSOLUTE_OFFSET para a chamada FSEEK() � inv�lido; deveria ser maior do que 0 e menor do que o n�mero total de bytes do arquivo (invalid_offset).';
    elsif (sqlcode = -29283) then
        ds_erro_w  := 'O arquivo n�o p�de ser aberto ou operado da forma desejada - ou o caminho n�o foi encontrado (invalid_operation).';
    elsif (sqlcode = -29280) then
        ds_erro_w  := 'O caminho especificado n�o existe ou n�o est� vis�vel ao Oracle (invalid_path).';
    elsif (sqlcode = -29284) then
        ds_erro_w  := 'N�o � poss�vel efetuar a leitura do arquivo (read_error).';
    elsif (sqlcode = -29292) then
        ds_erro_w  := 'N�o � poss�vel renomear o arquivo.';
    elsif (sqlcode = -29285) then
        ds_erro_w  := 'N�o foi poss�vel gravar no arquivo (write_error).';
    else
        ds_erro_w  := 'Erro desconhecido no package UTL_FILE.';
    end if;
end;

if    (ds_erro_w is not null) then
    Raise_application_error(-20011,ds_erro_w);
end if;

for r_c01_w in C01 loop
    begin

    vl_beneficiario_w    := 0;
    vl_empresa_w        := 0;

    if    (r_c01_w.NR_SEQ_CONTA_COPARTICIPACAO is not null) then
        select    sum(nvl(b.VL_APROPRIACAO,0))
        into    vl_beneficiario_w
        from    PLS_CONTA_COPARTIC_APROP    b,
            PLS_CENTRO_APROPRIACAO    a
        where    b.NR_SEQ_CENTRO_APROPRIACAO    = a.nr_sequencia
        and    b.NR_SEQ_CONTA_COPARTICIPACAO    = r_c01_w.NR_SEQ_CONTA_COPARTICIPACAO
        and    a.IE_RESPONSAVEL_APROPRIACAO = '1';

        select    sum(nvl(b.VL_APROPRIACAO,0))
        into    vl_empresa_w
        from    PLS_CONTA_COPARTIC_APROP    b,
            PLS_CENTRO_APROPRIACAO    a
        where    b.NR_SEQ_CENTRO_APROPRIACAO    = a.nr_sequencia
        and    b.NR_SEQ_CONTA_COPARTICIPACAO    = r_c01_w.NR_SEQ_CONTA_COPARTICIPACAO
        and    a.IE_RESPONSAVEL_APROPRIACAO = '2';

        select    nr_seq_conta
        into    nr_seq_conta_w
        from    pls_conta_coparticipacao
        where    nr_sequencia    = r_c01_w.NR_SEQ_CONTA_COPARTICIPACAO;

    elsif    (r_c01_w.nr_seq_mensalidade_seg_item is not null) then

        select    sum(nvl(b.VL_APROPRIACAO,0))
        into    vl_beneficiario_w
        from    PLS_MENS_SEG_ITEM_APROP    b,
            PLS_CENTRO_APROPRIACAO    a
        where    b.NR_SEQ_CENTRO_APROPRIACAO    = a.nr_sequencia
        and    b.NR_SEQ_ITEM        = r_c01_w.nr_seq_mensalidade_seg_item
        and    a.IE_RESPONSAVEL_APROPRIACAO = '1';

        select    sum(nvl(b.VL_APROPRIACAO,0))
        into    vl_empresa_w
        from    PLS_MENS_SEG_ITEM_APROP    b,
            PLS_CENTRO_APROPRIACAO    a
        where    b.NR_SEQ_CENTRO_APROPRIACAO    = a.nr_sequencia
        and    b.NR_SEQ_ITEM        = r_c01_w.nr_seq_mensalidade_seg_item
        and    a.IE_RESPONSAVEL_APROPRIACAO = '2';

        select    nr_seq_conta
        into    nr_seq_conta_w
        from    pls_mensalidade_seg_item
        where    nr_sequencia    = r_c01_w.nr_seq_mensalidade_seg_item;

        select    max(nr_sequencia)
        into    nr_seq_item_w
        FROM    pls_conta_proc
        where    nr_seq_conta        = nr_seq_conta_w
        and    ie_cobranca_prevista    = 'S';

        r_c01_w.CD_PROCED_MAT    := pls_obter_dados_conta_proc(nr_seq_item_w,'C');
        r_c01_w.DS_PROCED_MAT    := pls_obter_dados_conta_proc(nr_seq_item_w,'D');
        r_c01_w.QT_COBRADA    := pls_obter_dados_conta_proc(nr_seq_item_w,'QL');

    end if;

    dt_atendimento_w    := null;
    cd_guia_w        := '';
    nm_prestador_w        := '';
    ds_endereco_w        := '';
    ds_bairro_w        := '';
    uf_sg_w            := '';
    cd_cep_w        := '';
    ds_especialidade_w    := '';

    if    (nr_seq_conta_w is not null) then
        select    cd_guia,
            nvl(dt_atendimento,dt_emissao),
            nvl(nr_seq_prestador_exec,a.nr_seq_prestador),
            nvl(nr_seq_cbo_saude,NR_SEQ_CBO_SAUDE_SOLIC)
        into    cd_guia_w,
            dt_atendimento_w,
            nr_seq_prestador_w,
            nr_seq_cbo_saude_exec_w
        from    pls_conta        b,
            pls_protocolo_conta    a
        where    b.nr_seq_protocolo    = a.nr_sequencia
        and    b.nr_sequencia        = nr_seq_conta_w;

        nm_prestador_w        := substr(pls_obter_dados_prestador(nr_seq_prestador_w,'N'),1,255);

        ds_especialidade_w    := obter_cbo_saude(nr_seq_cbo_saude_exec_w);

    end if;

    ds_endereco_w        := substr(obter_compl_pf(r_c01_w.cd_pessoa_fisica, 1, 'EN'),1,255);
    ds_bairro_w        := substr(obter_compl_pf(r_c01_w.cd_pessoa_fisica, 1,'B'),1,255);
    cd_cep_w        := substr(obter_compl_pf(r_c01_w.cd_pessoa_fisica, 1,'CEP'),1,255);
    ds_cidade_w        := substr(obter_compl_pf(r_c01_w.cd_pessoa_fisica, 1,'DM'),1,255);
    uf_sg_w            := substr(obter_compl_pf(r_c01_w.cd_pessoa_fisica, 1,'UF'),1,255);

    index_w            := C01%rowCount;

    t_ds_conteudo_w(index_w)    :=  lpad(r_c01_w.cd_operadora_empresa,7,'0')||substr(lpad(r_c01_w.cd_sistema_ant,6,'0'),1,6)||lpad(r_c01_w.CD_MATRICULA_ESTIPULANTE,15,'0')||
                  rpad(fsfx_elimina_Acentos(upper(r_c01_w.nm_pessoa_fisica),'S'),40,' ')||r_c01_w.ie_titularidade||rpad(fsfx_elimina_acentos(upper(nm_prestador_w),'S'),40,' ')||substr(nvl(r_c01_w.tp_plano,' '),1,1)||nvl(r_c01_w.tp_plano_2,' ')||
                    lpad(r_c01_w.CD_PROCED_MAT,8,'0')|| rpad(fsfx_elimina_acentos(upper(r_c01_w.DS_PROCED_MAT),'S'),60,' ')|| lpad(r_c01_w.QT_COBRADA,4,'0')|| lpad(nvl(cd_guia_w,0),13,'0') ||
                  to_char(dt_atendimento_w,'yyyymmdd') ||'  '||lpad(replace(campo_mascara(nvl(vl_beneficiario_w,0),2),'.',''),13,'0')||
                    lpad(replace(campo_mascara(nvl(r_c01_w.vl_servico,0),2),'.',''),13,'0')||lpad(replace(campo_mascara(nvl(vl_empresa_w,0),2),'.',''),13,'0') || '0000000000000'||
                  lpad(nvl(r_c01_w.CD_COBRANCA,' '),4,' ')||rpad(fsfx_elimina_Acentos(upper(nvl(ds_endereco_w,' ')),'S'),40,' ')|| rpad(fsfx_elimina_acentos(upper(nvl(ds_bairro_w,' ')),'S'),20, ' ')|| rpad(fsfx_elimina_acentos(upper(nvl(ds_cidade_w,' ')),'S'),20, ' ') ||upper(nvl(uf_sg_w,'  '))||
                  lpad(nvl(cd_cep_w,0),8,'0')||rpad(upper(nvl(ds_especialidade_w,' ')),20, ' ')||lpad(nvl(r_c01_w.CD_COBRANCA,' '),4,' ');



    end;
end loop;

utl_file.put_line(arq_texto_w,to_char(DT_REF_MENSALIDADE_w,'yyyymm') || lpad(t_ds_conteudo_w.count,7,'0') || chr(13));
utl_file.fflush(arq_texto_w);

for i in t_ds_conteudo_w.first .. t_ds_conteudo_w.count loop
    utl_file.put_line(arq_texto_w,t_ds_conteudo_w(i) || chr(13));
    utl_file.fflush(arq_texto_w);
end loop;

utl_file.fclose(arq_texto_w);

end PLS_GERAR_ARQ_FSFX_LI016_D;
/