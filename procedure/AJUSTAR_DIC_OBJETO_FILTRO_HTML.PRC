create or replace
procedure Ajustar_Dic_Objeto_filtro_Html(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 

QT_POS_Y_w	number(5);
NR_SEQ_APRESENT_w	number(3)	:= 10;
QT_TAM_HTML_w		number(4);

Cursor C01 is
	select 	NM_ATRIBUTO, 
		QT_TAMANHO, 
		QT_POS_X, 
		QT_POS_Y, 
		Obter_Multiplo_Numero(QT_POS_Y, QT_POS_Y_w) qt_Linha,
		nr_sequencia
	from 	dic_objeto_filtro
	where 	nr_seq_objeto = nr_sequencia_p
	order by qt_linha, QT_POS_X;
	
c01_w	c01%rowtype;

qt_linha_old_w	number;

qt_linha_w		number(10)	:= 0;
qt_max_x_w		number(10);
qt_min_x_w		number(10);
qt_max_tamanho_w	number(10);
qt_tamanho_linha_w	number(10);

qt_tamanho_w		number(10);

	function calcularTamanhoHtml(	qt_tamanho_p	number) return number is
	vl_retorno_w	number(10,5);
	begin
	vl_retorno_w	:= (qt_tamanho_p * 12) / qt_tamanho_linha_w;
	
	return vl_retorno_w;	
		
	end;	

	function calcularTamanhoPixel(	qt_tamanho_p	number) return number is
	qt_multiplo_w		number(18,6)	:= 50;
	nr_multiplo_w		Number(15,4)	:= 0;
	begin
	
	if	((trunc(qt_tamanho_p / qt_multiplo_w) * qt_multiplo_w) = qt_tamanho_p) then
		nr_multiplo_w	:= (qt_tamanho_p / qt_multiplo_w);
	else
		nr_multiplo_w   := trunc(qt_tamanho_p / qt_multiplo_w) + 1;
	end if;
	
	return qt_multiplo_w * nr_multiplo_w;
		
	end;	

begin

select	nvl(min(QT_POS_Y),0) + 21
into	QT_POS_Y_w
from	dic_objeto_filtro
where	nr_seq_objeto	= nr_sequencia_p;


open C01;
loop
fetch C01 into	
	c01_w;
exit when C01%notfound;
	begin
	
	if	(qt_linha_old_w	is null)	or
		(c01_w.qt_linha	<> qt_linha_old_w) then
		
		qt_linha_w	:= qt_linha_w + 10;
		
		select	max(QT_POS_X)
		into	qt_max_x_w
		from	dic_objeto_filtro
		where	nr_seq_objeto	= nr_sequencia_p
		and	Obter_Multiplo_Numero(QT_POS_Y, QT_POS_Y_w)	= c01_w.qt_linha;
		
		select	max(QT_TAMANHO)
		into	qt_max_tamanho_w
		from	dic_objeto_filtro
		where	nr_seq_objeto	= nr_sequencia_p
		and	QT_POS_X	= qt_max_x_w
		and	Obter_Multiplo_Numero(QT_POS_Y, QT_POS_Y_w)	= c01_w.qt_linha;
		
		
		select	min(QT_POS_X)
		into	qt_min_x_w
		from	dic_objeto_filtro
		where	nr_seq_objeto	= nr_sequencia_p
		and	Obter_Multiplo_Numero(QT_POS_Y, QT_POS_Y_w)	= c01_w.qt_linha;
		
		
		qt_tamanho_linha_w	:= nvl(qt_max_tamanho_w,0) + nvl(qt_max_x_w,0) - nvl(qt_min_x_w,0);
	
		
	end if;
	
	qt_tamanho_w	:= round(calcularTamanhoHtml(c01_w.qt_tamanho));
	QT_TAM_HTML_w	:= calcularTamanhoPixel(c01_w.qt_tamanho);
	
	
	
	
	update	dic_objeto_filtro
	set	nr_seq_apresent	= nr_seq_apresent_w,
		QT_POS_Y	= qt_linha_w,
		qt_pos_x	= 0,
		qt_tamanho	= qt_tamanho_w,
		QT_TAM_HTML	= QT_TAM_HTML_w
	where	nr_sequencia	= c01_w.nr_sequencia;
	
	
	
	
	
	
	nr_seq_apresent_w	:= nr_seq_apresent_w + 10;
	qt_linha_old_w		:= c01_w.qt_linha;
	end;
end loop;
close C01;


commit;

end Ajustar_Dic_Objeto_filtro_Html;
/
