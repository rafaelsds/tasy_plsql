create or replace
procedure Alterar_status_anexo_age_HTML5(nr_seq_agenda_p number,
						nr_seq_lista_espera_p number,
						nr_seq_status_p number ,
						nm_usuario_p 	varchar2) is 

begin
	if	(nr_seq_agenda_p is not null) then
		ALTERAR_STATUS_ANEXO_AGENDA(nr_seq_agenda_p, nr_seq_status_p, nm_usuario_p);
	elsif	(nr_seq_lista_espera_p is not null) then
		ALT_STATUS_ANEXO_LISTA_ESPERA(nr_seq_lista_espera_p, nr_seq_status_p, nm_usuario_p);
	end if;
commit;

end Alterar_status_anexo_age_HTML5;
/
