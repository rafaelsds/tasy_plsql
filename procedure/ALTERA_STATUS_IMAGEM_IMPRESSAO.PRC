CREATE OR REPLACE PROCEDURE altera_status_imagem_impressao (
  nr_sequencia_p        NUMBER,
  ie_impressao_p        VARCHAR2,
  nr_prescricao_p       NUMBER,
  nr_seq_prescricao_p	NUMBER,
  ie_marca_todas_p      VARCHAR2) AS


nr_seq_log_atual_w NUMBER(10);
ie_log_w           VARCHAR2(10);

nr_seq_captura_w   NUMBER(10);
ie_impressao_old_w VARCHAR2(10);

CURSOR C01 IS
  SELECT nr_seq_captura
  FROM   prescr_proc_captura_img
  WHERE  nr_prescricao = nr_prescricao_p
  AND 	 nr_sequencia_prescricao = nr_seq_prescricao_p;

CURSOR C02 IS
  SELECT ie_impressao
  FROM   captura_imagem
  WHERE  nr_sequencia = nr_seq_captura_w;

BEGIN
  IF ie_marca_todas_p = 'S' THEN
  BEGIN
    OPEN C01;
    LOOP
    FETCH C01 INTO
      nr_seq_captura_w;
    EXIT WHEN C01%NOTFOUND;
      OPEN C02;
      LOOP
      FETCH C02 INTO
        ie_impressao_old_w;
      EXIT WHEN C02%NOTFOUND;
        gravar_log_alteracao(ie_impressao_old_w, ie_impressao_p, obter_usuario_ativo, nr_seq_log_atual_w, 'IE_IMPRESSAO', ie_log_w, NULL, 'CAPTURA_IMAGEM', nr_seq_captura_w, NULL);
        ie_log_w := NULL;
      END LOOP;
      CLOSE C02;
    END LOOP;
    CLOSE C01;

    UPDATE captura_imagem
    SET    ie_impressao = ie_impressao_p
    WHERE  nr_sequencia IN (SELECT nr_seq_captura
                            FROM   prescr_proc_captura_img
                            WHERE  nr_prescricao = nr_prescricao_p
                            AND    nr_sequencia_prescricao = nr_seq_prescricao_p);
  END;
  ELSE
  BEGIN
    SELECT ie_impressao
    INTO   ie_impressao_old_w
    FROM   captura_imagem
    WHERE  nr_sequencia = nr_sequencia_p;

    gravar_log_alteracao(ie_impressao_old_w, ie_impressao_p, obter_usuario_ativo, nr_seq_log_atual_w, 'IE_IMPRESSAO', ie_log_w, NULL, 'CAPTURA_IMAGEM', nr_sequencia_p, NULL);
    
    UPDATE captura_imagem 
    SET    ie_impressao = ie_impressao_p 
    WHERE  nr_sequencia = nr_sequencia_p;
  END;
  END IF;

  COMMIT;

END;
/