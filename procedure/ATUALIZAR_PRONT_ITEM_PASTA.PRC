CREATE OR REPLACE
PROCEDURE atualizar_pront_item_pasta IS

nr_sequencia_w				number(10);
qt_registro_w				number(3);
c01	  						integer;		
retorno_w					number(5);
ds_comando_w				varchar2(2000);

BEGIN

ds_comando_w := ' select	a.nr_sequencia '||
				' from		tasy_versao.prontuario_item_pasta a' ||
				' where		not exists (select	b.nr_sequencia 	'||
				'						from	prontuario_item_pasta b)';
				
C01 		:= DBMS_SQL.OPEN_CURSOR;
DBMS_SQL.PARSE(C01, ds_comando_w, dbms_sql.Native);
DBMS_SQL.DEFINE_COLUMN(C01, 1, nr_sequencia_w);
retorno_w 	:= DBMS_SQL.execute(C01);

while	( DBMS_SQL.FETCH_ROWS(C01) > 0 ) loop
	BEGIN
		
		DBMS_SQL.COLUMN_VALUE(C01, 1, nr_sequencia_w);
		
		ds_comando_w := ' insert 	into prontuario_item_pasta( ' ||
						'			nr_sequencia,'||
						'			dt_atualizacao,'||
						'			nm_usuario,'||
						' select	nr_sequencia,'||
						'			dt_atualizacao,'||
						'			nm_usuario,'||
						' from		tasy_versao.prontuario_item_pasta'||
						' where		nr_sequencia	= :nr_sequencia';
		exec_sql_dinamico_bv('Prontuario Item Pasta',ds_comando_w,'nr_sequencia='||nr_sequencia_w);
	
	end;
END LOOP;
DBMS_SQL.CLOSE_CURSOR(C01);
commit;	

END atualizar_pront_item_pasta;
/
