create or replace
procedure usjrp_gerar_mens_envio_cobr(	nr_titulo_p		number,
					ie_tipo_reg_p		in out varchar2,
					cd_flash_p		varchar2,
					ie_tipo_coluna_p	in out varchar2,
					nr_seq_cobranca_p	number,
					nr_seq_coluna_p		in out number,
					ie_opcao_p		varchar2,
					ie_elimina_caract_esp_p	varchar2,
					ie_elimina_acentuacao_p	varchar2,
					arq_texto_log_p		utl_file.file_type,
					nm_usuario_p		varchar2,
					cd_estabelecimento_p	number) is 

dt_referencia_w			varchar2(8);
nr_seq_pagador_w		number(10);
ds_endereco_w			varchar2(255);
					
begin	
select	max(nr_seq_pagador)
into	nr_seq_pagador_w
from	titulo_receber
where	nr_titulo    = nr_titulo_p;

if	(nr_seq_pagador_w is not null) then
	begin
	select	to_char(b.dt_referencia,'mm/yyyy')
	into	dt_referencia_w
	from	pls_mensalidade  b,
		titulo_receber  a
	where	a.nr_seq_mensalidade  = b.nr_sequencia
	and	a.nr_titulo    = nr_titulo_p;
	exception
	when others then
		utl_file.put_line(arq_texto_log_p,'Problemas ao buscar data de refer�ncia da mensalidade ' || nr_titulo_p || chr(13));
		utl_file.fflush(arq_texto_log_p);
	end;

	ds_endereco_w    := null;
	
	begin
	select	rpad(' ',16,' ') || rpad(substr(pls_obter_dados_pagador(nr_sequencia,'N'),1,96),96,' ') ||
		rpad(substr(pls_obter_dados_pagador(nr_sequencia,'NC'),1,15),15,' ')
	into	ds_endereco_w
	from	pls_contrato_pagador
	where	nr_sequencia   = nr_seq_pagador_w;
	exception
	when others then
		utl_file.put_line(arq_texto_log_p,'Problemas ao buscar a carteira de identificacao do pagador ' || nr_seq_pagador_w || chr(13));
		utl_file.fflush(arq_texto_log_p);
	end;

	usjrp_gerar_mensagem_cob_flash(	ds_endereco_w,nr_seq_cobranca_p,nr_titulo_p,'1','N','S',nm_usuario_p,cd_estabelecimento_p);

	ds_endereco_w    := null;

	begin
	select	rpad(' ',12,' ') || 
		rpad(substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'E'),pls_obter_compl_pagador(d.nr_sequencia,'E')),1,30) || ' ' ||
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'NR'),pls_obter_compl_pagador(d.nr_sequencia,'NR')),1,10) || ' ' ||
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'CO'),pls_obter_compl_pagador(d.nr_sequencia,'CO')),1,20),80, ' ') ||
		substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'B'),pls_obter_compl_pagador(d.nr_sequencia,'B')),1,20)
	into	ds_endereco_w
	from	pls_contrato_pagador d
	where	d.nr_sequencia = nr_seq_pagador_w;
	exception
	when others then
		utl_file.put_line(arq_texto_log_p,'Problemas ao buscar o endere�o do pagador(UF/CEP) ' || nr_seq_pagador_w || chr(13));
		utl_file.fflush(arq_texto_log_p);
	end;
	
	usjrp_gerar_mensagem_cob_flash(	ds_endereco_w,nr_seq_cobranca_p,nr_titulo_p,'1','N','S',nm_usuario_p,cd_estabelecimento_p);

	ds_endereco_w    := null;

	begin
	select	rpad(' ',12,' ') ||
		rpad(substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'CI'),pls_obter_compl_pagador(d.nr_sequencia,'CI')),1,30),34,' ') ||
		rpad(substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'UF'),pls_obter_compl_pagador(d.nr_sequencia,'UF')),1,10),43,' ') ||
		rpad(substr(decode(d.nr_sequencia,null,obter_dados_pf_pj(d.cd_pessoa_fisica, d.cd_cgc, 'CEP'),pls_obter_compl_pagador(d.nr_sequencia,'CEP')),1,20),20,' ')
	into	ds_endereco_w
	from	pls_contrato_pagador d
	where   nr_sequencia    = nr_seq_pagador_w;
	exception
	when others then
		utl_file.put_line(arq_texto_log_p,'Problemas ao buscar o endere�o do pagador(UF/CEP) ' || nr_seq_pagador_w || chr(13));
		utl_file.fflush(arq_texto_log_p);
	end;
	
	usjrp_gerar_mensagem_cob_flash(	ds_endereco_w,nr_seq_cobranca_p,nr_titulo_p,'1','N','S',nm_usuario_p,cd_estabelecimento_p);

	ds_endereco_w    := null;

	begin
	select	rpad(nvl(rpad(' ',11,' ') || rpad(nvl(nvl(obter_cpf_pessoa_fisica(cd_pessoa_fisica),cd_cgc),' '),62,' ') ||
		rpad(nvl(obter_dados_pf_pj(cd_pessoa_fisica, cd_cgc, 'IE'),' '),112,' '), ' '),112,' ') || rpad(nvl(dt_referencia_w,' '),12,' ')
	into	ds_endereco_w
	from	titulo_receber
	where	nr_titulo   = nr_titulo_p;
	exception
	when others then
		utl_file.put_line(arq_texto_log_p,'Problemas ao buscar CNPJ/CPF ' || nr_seq_pagador_w || chr(13));
		utl_file.fflush(arq_texto_log_p);
	end;
	
	usjrp_gerar_mensagem_cob_flash(	ds_endereco_w,nr_seq_cobranca_p,nr_titulo_p,'1','N','S',nm_usuario_p,cd_estabelecimento_p);			

	ds_endereco_w    := null;
	
	if	(ie_tipo_coluna_p = '2') then
		usjrp_gerar_mensagem_cob_flash(	' ',nr_seq_cobranca_p,nr_titulo_p,'1','N','S',nm_usuario_p,cd_estabelecimento_p);
	end if;
end if;
	
end usjrp_gerar_mens_envio_cobr;
/