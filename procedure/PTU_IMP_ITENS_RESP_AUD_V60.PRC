create or replace
procedure ptu_imp_itens_resp_aud_v60(	nr_seq_resp_aud_p   		ptu_resposta_auditoria.nr_sequencia%type,
					cd_servico_p  			ptu_resp_auditoria_servico.cd_servico%type,
					ds_servico_p			ptu_resp_auditoria_servico.ds_servico%type,
					ie_autorizado_p         	ptu_resp_auditoria_servico.ie_autorizado%type,
					nr_seq_item_p			ptu_resp_auditoria_servico.nr_seq_item%type,
					ie_tipo_tabela_p        	ptu_resp_auditoria_servico.ie_tipo_tabela%type,
					qt_autorizado_p         	ptu_resp_auditoria_servico.qt_autorizado%type,
					nm_usuario_p			usuario.nm_usuario%type,
					nr_seq_aud_servico_p 	out	ptu_resp_auditoria_servico.nr_sequencia%type) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Realizar a importa��o do arquivo de 00404 - Resposta de Auditoria
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ x] Tasy (Delphi/Java) [  x] Portal [  ]  Relat�rios [ x] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o: 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

begin

insert	into ptu_resp_auditoria_servico
	(nr_sequencia, ie_tipo_tabela, cd_servico,
	ds_servico, ie_autorizado,
	dt_atualizacao, nm_usuario, qt_autorizado,
	nr_seq_auditoria, nm_usuario_nrec, dt_atualizacao_nrec,
	nr_seq_item)
values	(ptu_resp_auditoria_servico_seq.nextval, ie_tipo_tabela_p, cd_servico_p,
	nvl(ds_servico_p,'N�o encontrado'), ie_autorizado_p,
	sysdate, nm_usuario_p, qt_autorizado_p,
	nr_seq_resp_aud_p, nm_usuario_p, sysdate,
	nr_seq_item_p) returning nr_sequencia into nr_seq_aud_servico_p;

commit;

end ptu_imp_itens_resp_aud_v60;
/