create or replace
procedure gpt_registrar_inconsistencia(	nr_seq_cpoe_p			number,
										nm_tabela_p				varchar2,
										ie_tipo_p				varchar2,
										nr_seq_inconsistencia_p	number,
										ie_opcao_p				varchar2,
										nm_usuario_p			Varchar2) is 
										
Cursor c01(nr_seq_cpoe_pc	number,
			nm_tabela_pc	varchar2,
			ie_tipo_pc		varchar2) is
	select	a.nr_prescricao,
			a.nr_sequencia
	from	prescr_material a
	where	a.nr_seq_mat_cpoe = nr_seq_cpoe_pc
	and		a.ie_agrupador = 1
	and		nm_tabela_pc	= 'CPOE_MATERIAL'
	and		ie_tipo_pc = 'M'
	union all
	select	a.nr_prescricao,
			a.nr_sequencia_solucao nr_sequencia
	from	prescr_material a
	where	a.ie_agrupador		= 4
	and		a.nr_seq_mat_cpoe	= nr_seq_cpoe_pc
	and		nm_tabela_pc	= 'CPOE_MATERIAL'
	and		ie_tipo_pc = 'SOL'
	union all
	select	a.nr_prescricao,
			a.nr_seq_solucao nr_sequencia
	from	prescr_solucao a
	where	a.nr_seq_dialise_cpoe	= nr_seq_cpoe_pc
	and		nm_tabela_pc	= 'CPOE_DIALISE'
	and		ie_tipo_pc = 'SOL';

begin
for	r_c01 in c01(nr_seq_cpoe_p,nm_tabela_p,ie_tipo_p) loop
	/* Solu��o */
	if	(ie_tipo_p = 'SOL') then
		registrar_inconsistencia_soluc(r_c01.nr_prescricao,
									r_c01.nr_sequencia,
									nr_seq_inconsistencia_p,
									ie_opcao_p,
									nm_usuario_p);
	/* Material */
	elsif	(ie_tipo_p = 'M') then
		registrar_inconsistencia_medic(r_c01.nr_prescricao,
									r_c01.nr_sequencia,
									nr_seq_inconsistencia_p,
									ie_opcao_p,
									nm_usuario_p);
	end if;
end loop;	

commit;

end gpt_registrar_inconsistencia;
/