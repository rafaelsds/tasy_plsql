create or replace
procedure inativar_alerta_carta_comprom(
			nr_sequencia_p	number,
			cd_pessoa_fisica_p varchar2) is 

qt_alerta_w		pls_integer;

begin

if (nr_sequencia_p is not null) then

	select	count(*)
	into 	qt_alerta_w
	from 	alerta_paciente
	where 	cd_pessoa_fisica = cd_pessoa_fisica_p
	and		dt_inativacao is null
	and		ie_situacao = 'A'
	and 	ds_observacao like '%'|| nr_sequencia_p || '%';
	
	if (qt_alerta_w > 0) then

		update	alerta_paciente
		set		dt_inativacao = sysdate,
				ie_situacao = 'I',
				dt_atualizacao = sysdate,
				nm_usuario = wheb_usuario_pck.get_nm_usuario
		where 	cd_pessoa_fisica = cd_pessoa_fisica_p
		and		ds_observacao like '%'|| nr_sequencia_p || '%';

		commit;
	
	end if;
	
end if;

end inativar_alerta_carta_comprom;
/