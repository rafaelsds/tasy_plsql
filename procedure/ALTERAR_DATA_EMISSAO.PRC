create or replace
procedure alterar_data_emissao(
		nr_titulo_p	number,
		dt_emissao_p	date) is 
begin
if	(nr_titulo_p is not null)  and
	(dt_emissao_p is not null) then
	begin
	update	titulo_pagar
	set     	dt_emissao = dt_emissao_p
	where   	nr_titulo = nr_titulo_p;
	commit;
	end;
end if;
end alterar_data_emissao;
/