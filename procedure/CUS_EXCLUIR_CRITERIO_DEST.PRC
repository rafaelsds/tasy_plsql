create or replace
procedure cus_excluir_criterio_dest(	cd_sequencia_criterio_p	number,
				cd_estabelecimento_p	number,	
				nm_usuario_p		Varchar2) is 

begin

delete	from	criterio_distr_orc_dest
where	cd_sequencia_criterio = cd_sequencia_criterio_p;

commit;

end cus_excluir_criterio_dest;
/
