create or replace
procedure pls_sib_vincular_benef_conf
			(	nr_seq_inclusao_benef_p		number,
				nr_seq_segurado_p		number,
				nm_usuario_p			varchar2) is

begin

update	pls_sib_conferencia
set	nr_seq_segurado	= nr_seq_segurado_p
where	nr_sequencia	= nr_seq_inclusao_benef_p;

commit;

end pls_sib_vincular_benef_conf;
/