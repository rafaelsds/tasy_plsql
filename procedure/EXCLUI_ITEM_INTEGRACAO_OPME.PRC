create or replace
procedure exclui_item_integracao_opme(	nr_seq_map_p		number ) is 

qt_pac_opme_w		number(10);
qt_pac_int_opme_w	number(10);

begin

select	count(*)
into	qt_pac_opme_w
from	agenda_pac_opme
where	nr_seq_map = nr_seq_map_p;

select	count(*)
into	qt_pac_int_opme_w
from	atend_pac_int_opme_item
where	nr_seq_map = nr_seq_map_p;

if	(nvl(qt_pac_opme_w,0) > 0) then
	begin
	
	delete	agenda_pac_opme
	where	nr_seq_map = nr_seq_map_p;
	
	end;
elsif	(nvl(qt_pac_int_opme_w,0) >0) then
	begin
	
	delete	atend_pac_int_opme_item
	where	nr_seq_map = nr_seq_map_p;
	
	end;
end if;

end exclui_item_integracao_opme;
/