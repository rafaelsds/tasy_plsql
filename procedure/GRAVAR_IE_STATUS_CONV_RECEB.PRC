create or replace
procedure GRAVAR_IE_STATUS_CONV_RECEB(IE_STATUS_P	varchar2,
				      NR_SEQUENCIA_P	number) is 



begin

	update CONVENIO_RECEB 
	set IE_STATUS = IE_STATUS_P
	where NR_SEQUENCIA = NR_SEQUENCIA_P;
	
	commit;

end GRAVAR_IE_STATUS_CONV_RECEB;
/
