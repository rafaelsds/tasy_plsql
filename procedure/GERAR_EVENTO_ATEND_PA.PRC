create or replace
procedure gerar_evento_atend_pa(nr_atendimento_p	number,
				cd_pessoa_fisica_p	varchar2,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	number) is 

qt_atend_tipo_guia_hc_w		number(10);
nr_seq_evento_w			number(10);
ie_tipo_atendimento_w		number(3);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	cd_estabelecimento	= cd_estabelecimento_p
	and	ie_evento_disp 		= 'APHC'
	and	nvl(ie_situacao,'A') = 'A';
			
begin

if	(nvl(nr_atendimento_p,0) > 0) then


	select	max(ie_tipo_atendimento)
	into	ie_tipo_atendimento_w
	from	atendimento_Paciente
	where	nr_atendimento	= nr_atendimento_p;

	select	count(*)
	into	qt_atend_tipo_guia_hc_w
	from	atendimento_paciente a,
		atend_categoria_convenio b
	where   a.cd_pessoa_fisica = cd_pessoa_fisica_p
	and	a.nr_atendimento = b.nr_atendimento	
	and	a.dt_alta is null
	and     b.ie_tipo_guia = 'H';
	
	if	(qt_atend_tipo_guia_hc_w >= 1) and
		(ie_tipo_atendimento_w = 3) then
		
		open C01;
		loop
		fetch C01 into	
			nr_seq_evento_w;
		exit when C01%notfound;
			begin
			gerar_evento_paciente(nr_seq_evento_w,nr_atendimento_p,cd_pessoa_fisica_p,null,nm_usuario_p,null);	
			end;
		end loop;
		close C01;
	end if;

end if;

commit;

end gerar_evento_atend_pa;
/