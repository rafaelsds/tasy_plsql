create or replace
procedure ALTERAR_CONTA_FINANC_TIT_REC
			(cd_conta_financ_ant_p	in	number,
			cd_conta_financ_nova_p	in	number,
			cd_centro_custo_ant_p	in	number,
			cd_centro_custo_novo_p	in	number,
			cd_conta_contab_ant_p	in	varchar2,
			cd_conta_contab_nova_p 	in	varchar2,
			nr_seq_produto_ant_p	in	number,
			nr_seq_produto_novo_p	in	number,
			nr_titulo_p			in	varchar2,
			dt_vencimento_p			in	date,
			ie_vencimento_p			in	varchar2,
			ie_conta_financ_p		in	varchar2,
			ie_centro_custo_p		in	varchar2,
			ie_conta_contabil_p		in	varchar2,
			ie_produto_p			in	varchar2,
			nm_usuario_p			in	varchar2) is

nr_sequencia_w		number(10,0);
dt_vencimento_w		date;
cd_motivo_w		number(3,0);
nr_lote_contabil_w	number(10,0);
ie_situacao_w		number(1,0);
nr_titulo_w		number(10,0);
vl_titulo_w		number(15,2);
cont_w			number(10,0);
cd_estabelecimento_w	number(10,0);
cd_convenio_w		number(10,0);
nr_seq_produto_w	number(10,0);
cd_cgc_w		varchar2(255);
ie_tipo_convenio_w	varchar2(255);
nr_seq_grupo_w		number(10,0);

ie_sit_w		varchar2(1);
cd_estab_w		number(4);
cd_estab_ativo_w	number(4);
ie_tipo_w		varchar2(1);

cursor c01 is
select	nr_titulo,
	dt_vencimento,
	nvl(nr_lote_contabil, 0),
	ie_situacao,
	vl_titulo,
	cd_estabelecimento,
	cd_convenio_conta,
	cd_cgc,
	obter_tipo_convenio(cd_convenio_conta)
from	titulo_receber a
where	' ' || nr_titulo_p || ' ' like '% ' || to_char(a.nr_titulo) || ' %';


begin

select 	substr(obter_estabelecimento_ativo,1,10)
into	cd_estab_ativo_w
from 	dual;

if ( cd_conta_financ_nova_p is not null ) then
	select	a.ie_situacao,
		a.cd_estabelecimento
	into	ie_sit_w,
		cd_estab_w
	from	conta_financeira a
	where 	a.cd_conta_financ = cd_conta_financ_nova_p;
	
	if ( nvl(ie_sit_w,'A') = 'I' ) then
		--R.aise_application_error(-20011,'Essa conta financeira est� inativa! Favor selecionar outra.');
		wheb_mensagem_pck.exibir_mensagem_abort(240832);
	elsif ( cd_estab_w is not null ) and ( cd_estab_w <> cd_estab_ativo_w ) then
		--R.aise_application_error(-20011,'Essa conta financeira � de outro estabelecimento! Favor selecionar outra.');
		wheb_mensagem_pck.exibir_mensagem_abort(240834);
	end if;	
end if;

if ( cd_centro_custo_novo_p is not null ) then
	select	a.ie_tipo
	into	ie_tipo_w
	from	centro_custo a
	where	a.cd_centro_custo	= cd_centro_custo_novo_p;
	
	if ( nvl(ie_tipo_w,'A') = 'T' ) then
		--R.aise_application_error(-20011,'N�o � poss�vel informar um centro de custo do tipo t�tulo');
		wheb_mensagem_pck.exibir_mensagem_abort(240835);
	end if;
end if;
	
open c01;
loop
fetch c01 into
	nr_titulo_w,
	dt_vencimento_w,
	nr_lote_contabil_w,
	ie_situacao_w,
	vl_titulo_w,
	cd_estabelecimento_w,
	cd_convenio_w,
	cd_cgc_w,
	ie_tipo_convenio_w;
exit when c01%notfound;

	select	count(*)
	into	cont_w
	from	titulo_receber_classif
	where	nr_titulo		= nr_titulo_w;

	if	(cont_w = 0) then
		insert	into titulo_receber_classif
			(NR_TITULO,
			NR_SEQUENCIA,
			VL_CLASSIFICACAO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			CD_CONTA_FINANC,
			CD_CENTRO_CUSTO,
			VL_DESCONTO,
			VL_ORIGINAL,
			CD_CONTA_CONTABIL,
			NR_SEQ_CONTRATO)
		values	(nr_titulo_w,
			1,
			vl_titulo_w,
			sysdate,
			nm_usuario_p,
			cd_conta_financ_nova_p,
			cd_centro_custo_novo_p,
			0,
			vl_titulo_w,
			cd_conta_contab_nova_p,
			null);
	end if;

	if	(ie_conta_financ_p = 'S') then

		update	titulo_receber_classif
		set	nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			cd_conta_financ		= cd_conta_financ_nova_p
		where	nr_titulo		= nr_titulo_w
		and	nvl(cd_conta_financ,0)	= nvl(cd_conta_financ_ant_p,0);

	end if;

	if	(ie_centro_custo_p = 'S') and
		(nr_lote_contabil_w = 0) then

		update	titulo_receber_classif
		set	nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			cd_centro_custo		= cd_centro_custo_novo_p
		where	nr_titulo		= nr_titulo_w
		and	nvl(cd_centro_custo,0)	= nvl(cd_centro_custo_ant_p,0);

	end if;

	if	(ie_conta_contabil_p = 'S') and
		(nr_lote_contabil_w = 0) then

		update	titulo_receber_classif
		set	nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			cd_conta_contabil	= cd_conta_contab_nova_p
		where	nr_titulo		= nr_titulo_w
		and	nvl(cd_conta_contabil,0)	= nvl(cd_conta_contab_ant_p,0);

	end if;

	if	(ie_produto_p = 'S') then
		update	titulo_receber_classif
		set	nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			nr_seq_produto		= nr_seq_produto_novo_p
		where	nr_titulo		= nr_titulo_w
		and	nvl(nr_seq_produto,0)	= nvl(nr_seq_produto_ant_p,0);
	elsif	(ie_produto_p = 'R') then
		obter_produto_financeiro(cd_estabelecimento_w, 
					cd_convenio_w,
					cd_cgc_w,
					nr_seq_produto_w,
					ie_tipo_convenio_w,
					null,
					nr_seq_grupo_w);
		update	titulo_receber_classif
		set	nm_usuario		= nm_usuario_p,
			dt_atualizacao		= sysdate,
			nr_seq_produto		= nr_seq_produto_w
		where	nr_titulo		= nr_titulo_w
		and	nvl(nr_seq_produto,0)	= nvl(nr_seq_produto_ant_p,0);		
		
	end if;

	if	(ie_vencimento_p = 'S') and 
		(ie_situacao_w = 1) then

		select	nvl(max(nr_sequencia), 0) + 1
		into	nr_sequencia_w
		from	alteracao_vencimento
		where	nr_titulo	= nr_titulo_w;

		if	(dt_vencimento_p < dt_vencimento_w) then
			cd_motivo_w		:= 2;
		else
			cd_motivo_w		:= 1;
		end if;

		insert	into alteracao_vencimento
			(NR_TITULO,
			NR_SEQUENCIA,
			DT_ANTERIOR,
			DT_VENCIMENTO,
			CD_MOTIVO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_ALTERACAO,
			DS_OBSERVACAO)
		select	nr_titulo,
			nr_sequencia_w,
			dt_vencimento,
			dt_vencimento_p,
			cd_motivo_w,
			sysdate,
			nm_usuario_p,
			dt_vencimento_p,
			null
		from	titulo_receber
		where	nr_titulo	= nr_titulo_w;

		update	titulo_receber
		set	dt_vencimento		= dt_vencimento_p,
			dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p
		where	nr_titulo		= nr_titulo_w;
	end if;


end loop;
close c01;

commit;

end ALTERAR_CONTA_FINANC_TIT_REC;
/