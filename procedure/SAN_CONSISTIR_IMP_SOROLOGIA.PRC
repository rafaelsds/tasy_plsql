create or replace
procedure san_consistir_imp_sorologia(		nr_Seq_interface_p	number,
						ds_lista_dados_p	varchar2,
						ds_lista_dados2_p	varchar2,
						ds_lista_dados3_p	varchar2,
						cd_estabelecimento_p	number,
						ds_retorno_p	out	varchar2) is

ds_lista_dados_w	varchar2(32000);
ds_lista_Dados2_w	varchar2(32000);
ds_lista_Dados3_w	varchar2(32000);

nr_seq_tipo_exame_w	number(10);
nr_seq_exame_w		number(10);
ie_pos_virgula_w	number(3);
tam_lista_w		number(10);
qt_exame_lote_regra_w	number(10);

ds_lote_kit_w		varchar2(255);
ie_pos_virgula2_w	number(3);
tam_lista2_w		number(10);
ds_exame_w		varchar2(255);

dt_venc_kit_w		varchar2(255);
ie_pos_virgula3_w	number(3);
tam_lista3_w		number(10);
begin

if 	(nr_seq_interface_p = 1924) or
	(nr_seq_interface_p = 2236) then

	select	count(*)
	into	qt_exame_lote_regra_w
	from	san_kit_exame;
	
	if (qt_exame_lote_regra_w > 0) then
		
		ds_lista_dados_w  := ds_lista_dados_p;
		ds_lista_Dados2_w := ds_lista_dados2_p;
		ds_lista_Dados3_w := ds_lista_dados3_p;

		while (ds_lista_dados_w is not null) loop

			tam_lista_w		:= length(ds_lista_dados_w);
			ie_pos_virgula_w	:= instr(ds_lista_dados_w, ',');

			tam_lista2_w		:= length(ds_lista_dados2_w);
			ie_pos_virgula2_w	:= instr(ds_lista_dados2_w, ',');

			tam_lista3_w		:= length(ds_lista_dados3_w);
			ie_pos_virgula3_w	:= instr(ds_lista_dados3_w, ',');
			
			if	(ie_pos_virgula_w <> 0) then
				nr_seq_tipo_exame_w	:= to_number(substr(ds_lista_dados_w, 1, (ie_pos_virgula_w - 1)));
				ds_lista_dados_w	:= substr(ds_lista_dados_w, (ie_pos_virgula_w + 1), tam_lista_w);

				ds_lote_kit_w		:= substr(ds_lista_dados2_w, 1, (ie_pos_virgula2_w - 1));
				ds_lista_dados2_w	:= substr(ds_lista_dados2_w, (ie_pos_virgula2_w + 1), tam_lista2_w);
				
				dt_venc_kit_w		:= substr(ds_lista_dados3_w, 1, (ie_pos_virgula3_w - 1));
				ds_lista_dados3_w	:= substr(ds_lista_dados3_w, (ie_pos_virgula3_w + 1), tam_lista3_w);

			end if;

			if (nr_seq_tipo_exame_w <> 0) then

				select	nvl(max(nr_sequencia),0),
					max(ds_exame)
				into	nr_seq_exame_w,
					ds_exame_w
				from 	san_exame
				where	cd_codigo_externo = nr_seq_tipo_exame_w
				and	nvl(cd_estabelecimento, cd_estabelecimento_p) = cd_estabelecimento_p;

				select	count(*)
				into	qt_exame_lote_regra_w
				from  	san_kit_exame
				where	nr_seq_exame = nr_seq_exame_w
				and	ds_lote_kit = ds_lote_kit_w
				and	sysdate between dt_vigencia_ini and dt_vigencia_final
				and	trunc(dt_vigencia_final) = to_date(dt_venc_kit_w,'dd/mm/yyyy');

				if (qt_exame_lote_regra_w = 0) then
					ds_retorno_p := wheb_mensagem_pck.get_texto(309619, 'DS_EXAME_W=' || ds_exame_w);
								/*
									O exame #@DS_EXAME_W#@ n�o esta no cadastro de kit de exames.
									Ou a data de vencimento cadastrado � diferente da data de validade do arquivo.
								*/
				end if;

			end if;


		end loop;
	end if;

end if;


commit;

end san_consistir_imp_sorologia;
/
