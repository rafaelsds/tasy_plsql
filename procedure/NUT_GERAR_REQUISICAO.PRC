create or replace
procedure nut_gerar_requisicao
			(	nr_seq_ordem_p			number,
				cd_estabelecimento_p		number,
				nm_usuario_p			varchar2) is 


				
nr_seq_local_w				number(10);
cd_setor_atendimento_w			number(5);
cd_pessoa_fisica_w			varchar2(10);
cd_local_estoque_w			number(10,0);
cd_operacao_estoque_w			NUMBER(3);
nr_requisicao_w				number(10,0);
cd_material_w				number(6,0);
qt_componente_w				number(15,3);
cd_unidade_medida_estoque_w		varchar2(30);
cd_unidade_medida_consumo_w		varchar2(30);
qt_conversao_w				number(15,4);
nr_seq_req_item_w			number(10,0);
ie_gerar_req_ind_w			varchar2(1);
cd_centro_custo_w			number(8);
nr_sequencia_w				number(5) := 0;
ds_ordem_w				varchar2(80);
cd_unidade_medida_genero_w		varchar2(30);
ds_erro_w				varchar2(4000);
ds_parametro_120_w  			varchar2(1);

Cursor C01 is
	SELECT 	distinct
		x.nr_seq_local,
		f.cd_centro_custo
	FROM   	nut_receita a,
		nut_receita_comp b,
		nut_genero_alim c,
		nut_pac_opcao_rec d,
		nut_cardapio_dia e,
		nut_local_estoque_mat x,
		nut_local_estoque y,
		local_estoque z,
		nut_local_refeicao f
	WHERE   a.nr_sequencia 	   	= b.nr_seq_receita
	AND	c.nr_sequencia 		= b.nr_seq_gen_alim	
	AND	((ds_parametro_120_w = 'N' and d.nr_seq_cardapio_dia 	= e.nr_sequencia) 
  		or  (ds_parametro_120_w = 'S' and d.nr_seq_cardapio_dia 	= e.nr_sequencia and nvl(e.nr_seq_cycle,0)>0))
	AND	a.nr_sequencia 		= d.nr_seq_receita
	AND	x.cd_material		= c.cd_material
	and 	x.nr_seq_local 		= y.nr_sequencia
	and 	y.nr_seq_local_estoque 	= z.cd_local_estoque
	and 	z.cd_estabelecimento 	= cd_estabelecimento_p
	and	e.nr_seq_local		= f.nr_sequencia(+)
	AND	e.nr_sequencia IN	(SELECT nr_seq_cardapio
	   					   FROM   nut_ordem_prod_card
						   WHERE  nr_seq_ordem   = nr_seq_ordem_p)
	order by 1;
	
Cursor C02 is  									
	select 	DISTINCT(c.cd_material),
		SUM(decode( ie_gerar_req_ind_w,'S', d.qt_refeicao,e.qt_pessoa_atend) * (b.qt_componente/NVL(a.qt_rendimento,b.qt_componente))),		
		substr(obter_dados_material_estab(c.cd_material,cd_estabelecimento_p,'UMS'),1,30) cd_unidade_medida_consumo,
		substr(obter_dados_material_estab(c.cd_material,cd_estabelecimento_p,'UME'),1,30) cd_unidade_medida_estoque,
		c.qt_conversao,
		nvl(c.cd_unidade_medida,'G')
	FROM   	nut_receita a,
		nut_receita_comp b,
		nut_genero_alim c,
		nut_pac_opcao_rec d,
		nut_cardapio_dia e,
		material m,
		nut_local_refeicao f
	WHERE  	a.nr_sequencia 		= b.nr_seq_receita
	AND	c.nr_sequencia 		= b.nr_seq_gen_alim	
	AND	((ds_parametro_120_w = 'N' and d.nr_seq_cardapio_dia 	= e.nr_sequencia) 
		or  (ds_parametro_120_w = 'S' and d.nr_seq_cardapio_dia 	= e.nr_sequencia and nvl(e.nr_seq_cycle,0)>0))
	AND	a.nr_sequencia 		= d.nr_seq_receita
	AND	m.cd_material		= c.cd_material
        AND((f.cd_estabelecimento = cd_estabelecimento_p) or (f.cd_estabelecimento is null))
	and	e.nr_seq_local		= f.nr_sequencia(+)
	AND	e.nr_sequencia		IN (SELECT nr_seq_cardapio
	   				    FROM   nut_ordem_prod_card
					    WHERE  nr_seq_ordem   = nr_seq_ordem_p)
	AND	nr_seq_local_w		= (SELECT MAX(x.nr_seq_local) 
	   				   FROM   nut_local_estoque_mat x,
						nut_local_estoque y,
						local_estoque z
					   WHERE  x.cd_material = c.cd_material
						   and x.nr_seq_local = y.nr_sequencia
                    					   and y.nr_seq_local_estoque = z.cd_local_estoque
                    					   and z.cd_estabelecimento = cd_estabelecimento_p)
	AND	nvl(f.cd_centro_custo,0)	= nvl(cd_centro_custo_w,0)
	GROUP BY	c.cd_material,
			substr(obter_dados_material_estab(c.cd_material,cd_estabelecimento_p,'UMS'),1,30),
			substr(obter_dados_material_estab(c.cd_material,cd_estabelecimento_p,'UME'),1,30),
			c.qt_conversao,
			nvl(c.cd_unidade_medida,'G');
			
Cursor C04 is
	select	g.nr_sequencia,
		g.cd_material
	from	nut_receita a,
		nut_receita_comp b,
		nut_ordem_prod_card h,
		nut_pac_opcao_rec d,
		nut_genero_alim c,
		item_requisicao_material g
	where	a.nr_sequencia 		= b.nr_seq_receita	
	and	c.nr_sequencia 		= b.nr_seq_gen_alim
	and	d.nr_seq_cardapio_dia	= h.nr_seq_cardapio
	and	a.nr_sequencia 		= d.nr_seq_receita
	and	c.cd_material		= g.cd_material
	and	nvl(c.ie_arred_req,'N') = 'S'
	and	g.nr_requisicao 	= nr_requisicao_w
	and	h.nr_seq_ordem		= nr_seq_ordem_p
	group by g.nr_sequencia,
		g.cd_material;

begin
if	(nr_seq_ordem_p is not null) then
ds_parametro_120_w := obter_valor_param_usuario(1003,120,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);
	select 	cd_setor_atendimento,
		cd_pessoa_fisica
	into	cd_setor_atendimento_w,
		cd_pessoa_fisica_w
	from	usuario
	where	nm_usuario	= nm_usuario_p;	
	
	select 	max(cd_operacao_estoque),
		max(ds_ordem)
	into	cd_operacao_estoque_w,
		ds_ordem_w
	from 	nut_ordem_prod
	where	nr_sequencia	= nr_seq_ordem_p;
	
	obter_param_usuario(1003, 88, Obter_perfil_Ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento, ie_gerar_req_ind_w);
	
	open C01;
	loop
	fetch C01 into	
		nr_seq_local_w,
		cd_centro_custo_w;
	exit when C01%notfound;
		begin
		select 	nr_seq_local_estoque
		into	cd_local_estoque_w
		from	nut_local_estoque
		where	nr_sequencia	= nr_seq_local_w;
		
		ds_erro_w	:= '';
		
		if	(cd_pessoa_fisica_w is null) then
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(783994);
		end if;
		
		if	(cd_operacao_estoque_w is null) then
			if	(ds_erro_w is not null) then
				ds_erro_w	:= ds_erro_w || chr(10) || chr(13);
			end if;
		
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(783998);
		end if;
		
		if	(cd_local_estoque_w is null) then
			if	(ds_erro_w is not null) then
				ds_erro_w	:= ds_erro_w || chr(10) || chr(13);
			end if;
			
			ds_erro_w	:= ds_erro_w || wheb_mensagem_pck.get_texto(783999);
		end if;
		
		begin
		select	requisicao_seq.nextval
		into	nr_requisicao_w
		from	dual;
		
		insert into requisicao_material
			(	nr_requisicao,
				cd_estabelecimento,
				cd_setor_atendimento,
				cd_local_estoque,
				dt_solicitacao_requisicao,
				dt_atualizacao,
				nm_usuario,
				cd_operacao_estoque,
				cd_pessoa_requisitante,
				cd_estabelecimento_destino,
				cd_local_estoque_destino,
				ie_urgente,
				ds_observacao,
				cd_centro_custo)
		values	(	nr_requisicao_w,
				cd_estabelecimento_p,
				cd_setor_atendimento_w,
				cd_local_estoque_w,
				sysdate,
				sysdate,
				nm_usuario_p,
				cd_operacao_estoque_w,
				cd_pessoa_fisica_w,
				cd_estabelecimento_p,
				null,
				'N',
				wheb_mensagem_pck.get_texto(303619,'NR_SEQ_ORDEM_P='|| nr_seq_ordem_p ||';DS_ORDEM_W='|| ds_ordem_w),
				cd_centro_custo_w);

		insert into nut_ordem_prod_req
			(	nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_ordem,
				nr_requisicao)
		values	(	nut_ordem_prod_req_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_ordem_p,
				nr_requisicao_w);
		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(192874,'DS_ERRO=' || ds_erro_w);
            RAISE;
		end;
		
		open C02;
		loop
		fetch C02 into	
			cd_material_w,
			qt_componente_w,
			cd_unidade_medida_consumo_w,
			cd_unidade_medida_estoque_w,
			qt_conversao_w,
			cd_unidade_medida_genero_w;
		exit when C02%notfound;
			begin
		
			if	(nvl(qt_componente_w, 0) = 0) then
				begin				
				wheb_mensagem_pck.exibir_mensagem_abort(1039798);
				end;
			end if;
			
			if	(upper(cd_unidade_medida_consumo_w) <> upper(cd_unidade_medida_genero_w)) then			
				qt_componente_w	:= Obter_dose_convertida(cd_material_w,qt_componente_w,cd_unidade_medida_genero_w,cd_unidade_medida_consumo_w);
			end if;
			
			select 	nvl(max(nr_sequencia),0) + 1
			into	nr_seq_req_item_w
			from	item_requisicao_material
			where	nr_requisicao	= nr_requisicao_w;
			
			insert into item_requisicao_material
				(	nr_requisicao,
					nr_sequencia,
					cd_estabelecimento,
					cd_material,
					qt_material_requisitada,
					qt_material_atendida,
					vl_material,
					dt_atualizacao,
					nm_usuario,
					cd_unidade_medida,
					qt_estoque,
					cd_unidade_medida_estoque,
					ie_acao,
					cd_motivo_baixa)
			values	(	nr_requisicao_w,
					nr_seq_req_item_w,
					cd_estabelecimento_p,
					cd_material_w,
					qt_componente_w,
					0,
					0,
					sysdate,
					nm_usuario_p,
					cd_unidade_medida_consumo_w,
					qt_componente_w,
					cd_unidade_medida_consumo_w,
					'1',
					0);
			end;
		end loop;
		close C02;
	
		open C04;
		loop
		fetch C04 into	
			nr_sequencia_w,
			cd_material_w;
		exit when C04%notfound;
			begin
			update	item_requisicao_material
			set	qt_estoque 		= ceil(qt_estoque),
				qt_material_requisitada	= ceil(qt_material_requisitada)
			where	nr_requisicao		= nr_requisicao_w
			and	nr_sequencia		= nr_sequencia_w
			and	cd_material		= cd_material_w;
			end;
		end loop;
		close C04;
		end;
	end loop;
	close C01;	
end if;

commit;

end nut_gerar_requisicao;
/
