create or replace procedure        POPULATE_HDR_TABLE(
    dt_exportar_de_p  date ,
    dt_exportar_ate_p date ,
    nm_usuario_p      varchar2 ,
    returned_value_p out NOCOPY number ,
    other_exception_p out NOCOPY varchar2 )
is
  dt_exportar_de_w  date;
  dt_exportar_ate_w date;
  returned_value_w  number;
  /*--HDR table columns*/
  cd_facility_number_w      varchar2( 5 );
  nr_cad_records_w          varchar2( 5 );
  nr_can_records_w          varchar2( 5 );
  nr_fan_records_w          varchar2( 5 );
  nr_cdx_records_w          varchar2( 5 );
  line_count_w              number( 10 ):=0;
  downloaded_report_count_w number( 10 ):=0;
  v_errm                    varchar2( 100 );
  exc_raised_in_hdr_tab		exception;
  PRAGMA exception_init( exc_raised_in_hdr_tab, -20003 );								   														 
  error_result clob:=null;
  cursor c01
  is
    select
        (select   coalesce( trim( substr( nvl( obter_dados_pf_pj( null , obter_cgc_estabelecimento( WHEB_USUARIO_PCK.get_cd_estabelecimento ) , 'RFC' ) , '00000' ) , 1 , 5 ) ) , trim( to_char( nvl(
            obter_cgc_estabelecimento( WHEB_USUARIO_PCK.get_cd_estabelecimento ) , '00000' ) , '00000' ) ) )
          from dual
        ) ,
      (select   trim( to_char( count( * ) , '00000' ) )
        from cancer_admission_details
        where dt_export_from=dt_exportar_de_w
        and dt_export_to    =dt_exportar_ate_w
        and nr_report_sequence is null
      ) ,
      (select   trim( to_char( count( * ) , '00000' ) )
        from cancer_details
        where dt_export_from=dt_exportar_de_w
        and dt_export_to    =dt_exportar_ate_w
        and nr_report_sequence is null
      ) ,
      (select   trim( to_char( count( * ) , '00000' ) )
        from patient_former_names
        where dt_export_from=dt_exportar_de_w
        and dt_export_to    =dt_exportar_ate_w
        and nr_report_sequence is null
      ) ,
      (select   trim( to_char( count( * ) , '00000' ) )
        from clinical_diagosis_detail
        where dt_export_from=dt_exportar_de_w
        and dt_export_to    =dt_exportar_ate_w
        and nr_report_sequence is null
      )
    from dual;
begin
  dt_exportar_de_w :=dt_exportar_de_p;
  dt_exportar_ate_w:=dt_exportar_ate_p;
  if( dt_exportar_de_w is not null and dt_exportar_ate_w is not null ) then
    open c01;
    loop
    begin
        line_count_w:=line_count_w+1;
        fetch c01
          into cd_facility_number_w ,
            nr_cad_records_w ,
            nr_can_records_w ,
            nr_fan_records_w ,
            nr_cdx_records_w;
        exit
      when c01%notfound;
		begin  
			insert
			into header_detail
            (
              nr_sequencia ,
              dt_atualizacao ,
              nm_usuario ,
              dt_atualizacao_nrec ,
              nm_usuario_nrec ,
              cd_facility_number ,
              nr_cad_records ,
              nr_can_records ,
              nr_fan_records ,
              nr_cdx_records ,
              dt_export_from ,
              dt_export_to ,
              nr_report_sequence
            )
            values
            (
              header_detail_seq.nextval ,
              sysdate ,
              nm_usuario_p ,
              sysdate ,
              nm_usuario_p ,
              cd_facility_number_w ,
              nr_cad_records_w ,
              nr_can_records_w ,
              nr_fan_records_w ,
              nr_cdx_records_w ,
              dt_exportar_de_w ,
              dt_exportar_ate_w ,
              null
            );
		exception when others then
			v_errm      := substr( sqlerrm , 1 , 100 );
			error_result:= error_result || ' '|| v_errm||' . error happened at line '||line_count_w||' for facility number '|| cd_facility_number_w ||chr( 13 )||chr( 10 );
		end;						  
    exception
		when value_error or dup_val_on_index or invalid_cursor then
        v_errm      := substr( sqlerrm , 1 , 100 );
        error_result:= error_result || ' '|| v_errm||' . error happened at line '||line_count_w||chr( 13 )||chr( 10 );
    end;
    end loop;
    returned_value_w :=1;
    other_exception_p:=null;
    if( error_result   is not null )then
      returned_value_w :=0;
      other_exception_p:=error_result;
	  raise exc_raised_in_hdr_tab;					   
    end if;
    close c01;
  end if;
  returned_value_p:=returned_value_w;
exception
when others then
  returned_value_p :=2;
  other_exception_p:=other_exception_p||wheb_mensagem_pck.get_texto( 1073278 , 'DS_ERROR='||'populate_hdr_table. Error:'|| sqlerrm );
end populate_hdr_table; 
/