create or replace
procedure estornar_lib_autor_carga_estoq(	nr_sequencia_p		number,
					nm_usuario_p		Varchar2) is 

qt_existe_w	number(10);
					
begin

select	count(*)
into	qt_existe_w
from	sup_autor_carga_est_hist
where	nr_seq_autor = nr_sequencia_p
and	ie_origem in ('G','C','S','X','I');

if	(qt_existe_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(265981);
	--'Essa autoriza��o j� foi utilizada para efetuar alguma a��o na carga de arquivos, portanto n�o pode mais estornar a  libera��o da mesma.'
end if;

update	sup_autor_carga_estoque
set	dt_liberacao	= null,
	nm_usuario_lib	= ''
where	nr_sequencia	= nr_sequencia_p;

gerar_hist_autor_carga_estoque(
	nr_sequencia_p,
	'E',
	wheb_mensagem_pck.get_Texto(313408), /*'Estorno da libera��o da autoriza��o',*/
	wheb_mensagem_pck.get_Texto(313409), /*'Foi estornado a libera��o da autoriza��o para carga de estoque.',*/
	nm_usuario_p);

commit;

end estornar_lib_autor_carga_estoq;
/
