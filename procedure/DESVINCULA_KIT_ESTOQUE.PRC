CREATE OR REPLACE 
PROCEDURE desvincula_kit_estoque(nr_prescricao_p	number)
								is
								
nr_seq_reg_kit_w	number(10);								
nm_usuario_w		varchar2(15);
								
Cursor c01 is
select	a.nr_sequencia
from	kit_estoque_reg a
where	exists	(select 1 
				from 	kit_estoque b 
				where 	a.nr_sequencia 	= b.nr_seq_reg_kit 
				and 	nr_prescricao 	= nr_prescricao_p);

								
begin

nm_usuario_w	 := wheb_usuario_pck.get_nm_usuario; 

open C01;
	loop
	fetch C01 into	
	nr_seq_reg_kit_w;
	exit when C01%notfound;
		begin
		update	kit_estoque_reg
		set	dt_utilizacao	= null,
			nm_usuario_util = null,
			dt_atualizacao 	= sysdate,
			nm_usuario	= nm_usuario_w
		where	nr_sequencia 	= nr_seq_reg_kit_w;
		end;
	end loop;
close C01;

update	kit_estoque
set	nm_usuario_util 	= null,
	dt_utilizacao	= null,
	ie_status	= 'C'
where	nr_prescricao	= nr_prescricao_p;		

commit;

END;
/