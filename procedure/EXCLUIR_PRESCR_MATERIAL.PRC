create or replace
procedure excluir_prescr_material(	
		nr_prescricao_p		number,
		nr_seq_prescricao_p	number) is 

begin

delete	from prescr_material
where	nr_prescricao	= nr_prescricao_p
and	nr_sequencia	= nr_seq_prescricao_p;

commit;

end excluir_prescr_material;
/