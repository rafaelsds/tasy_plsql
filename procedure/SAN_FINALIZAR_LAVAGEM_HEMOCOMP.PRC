create or replace
procedure san_finalizar_lavagem_hemocomp( nr_seq_lavagem_p	number,
					nm_usuario_p		Varchar2) is 

begin

if (nr_seq_lavagem_p is not null) then

	update 	san_producao_lavagem
	set	dt_fim_lavagem 		= sysdate,
		nm_usuario_fim		= nm_usuario_p,
		dt_atualizacao		= sysdate,
		nm_usuario		= nm_usuario_p
	where	nr_sequencia		= nr_seq_lavagem_p;
end if;

commit;

end san_finalizar_lavagem_hemocomp;
/