CREATE OR replace PROCEDURE Generate_qhapdc_snp_content(nr_seq_dataset_p  NUMBER 
, 
nm_usuario_p      VARCHAR2, 
returned_value_p  OUT nocopy NUMBER, 
other_exception_p OUT nocopy VARCHAR2) 
IS 
  ds_segment_w                 VARCHAR2(32767); 
  ds_segment_clob_w            CLOB := empty_clob; 
  is_clob_insertion_successful NUMBER := 0; 
  excp_caught_dataset_clob_ins VARCHAR2( 500 ); 
BEGIN 
    BEGIN 
        SELECT 'N' --Record Identifier  
               || Lpad(nr_atendimento, 12, '0') -- Unique Number  
               || Lpad(Obter_pessoa_atendimento(nr_atendimento, 'C'), 8, '0') 
               --Patient Identifier  
               || Lpad(nr_atendimento, 12, '0') --Admission Number  
               || Decode(an_snap_group, NULL, '001', 
                                        Lpad(Trim(an_snap_group), 3, '0')) 
               -- Please note we are using an_snap_group as snap episode number as this field is not currently used
               || Trim(snap_type) 
               || '   ' 
               || Decode(dt_snap_start, NULL, '        ', 
                                        To_char(dt_snap_start, 'yyyymmdd')) 
               || Decode(dt_snap_end, NULL, '        ', 
                                      To_char(dt_snap_end, 'yyyymmdd')) 
               || Decode(multidisc_care_plan_flag, NULL, 'U', 
                  Trim(multidisc_care_plan_flag)) 
               || Decode(dt_multidisc_care_plan, NULL, '        ', 
                                                 To_char(dt_multidisc_care_plan, 
                                                 'yyyymmdd')) 
               || Decode(principal_ref_service, NULL, '   ', 
                                                Rpad(Trim(principal_ref_service) 
                                                , 3, 
                                                '0' 
                                                )) 
               || Decode(primary_impair_type, NULL, '       ', 
                                              Rpad(Trim(primary_impair_type), 7, 
                                              ' ' 
                                              )) 
               || Decode(ie_clinical_only, NULL, '1', 
                                           Trim(ie_clinical_only)) 
               || Chr(10) 
        INTO   ds_segment_w 
        FROM   qhapdc_segment_snp 
        WHERE  nr_dataset = nr_seq_dataset_p; 
    EXCEPTION 
        WHEN OTHERS THEN 
          ds_segment_w := NULL; 
    END; 

    SELECT Concat(ds_segment_clob_w, ds_segment_w) 
    INTO   ds_segment_clob_w 
    FROM   dual; 

    Insert_dataset_content(nm_usuario_p, nr_seq_dataset_p, ds_segment_clob_w, 
    is_clob_insertion_successful, excp_caught_dataset_clob_ins); 

    IF( is_clob_insertion_successful = 1 ) THEN 
      returned_value_p := 1; 

      other_exception_p := NULL; 
    ELSE 
      IF( excp_caught_dataset_clob_ins IS NOT NULL ) THEN 
        returned_value_p := 0; 

        other_exception_p := excp_caught_dataset_clob_ins; 
      END IF; 
    END IF; 
END generate_qhapdc_snp_content; 
/