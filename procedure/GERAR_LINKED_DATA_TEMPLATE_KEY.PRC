create or replace procedure gerar_linked_data_template_key (
                                      nr_seq_template_p number,
                                      nr_seq_linked_data_p number,
                                      nm_usuario_p varchar2,
                                      keys_p clob
                                )  is

/*mesma consulta do wcpanel 1162150 (AtePacCG): exceto a parte do split da key*/
cursor c01 is
select  
          distinct a.nm_atributo nm_atributo, 
          CASE 
            WHEN b.nm_tabela='PACIENTE_ACESSORIO' AND a.nm_atributo 
              IN ('QT_FREQ_MAGNETICA','DS_ELETR_ATRIAL','QT_ERI','DS_GERADOR_MARCAPASSO','DS_ELETR_VENTRICULAR') 
              THEN 'VARCHAR2' 
            WHEN b.nm_tabela='PACIENTE_ACESSORIO' AND a.nm_atributo 
              IN ('DT_IMPL_ELETRODO_ATRIAL', 'DT_IMPL_ELETR_VENTRICULAR') 
              THEN 'DATE'
            ELSE c.ie_tipo_atributo
          END ie_tipo_atributo,
          a.cd_exp_label,
      a.cd_exp_label_grid,
          obter_desc_expressao(a.cd_exp_label) ds_exp_label, 
          a.cd_exp_valores, 
          obter_desc_expressao(a.cd_exp_valores) ds_exp_valores, 
          a.cd_dominio, 
          obter_nome_dominio(a.cd_dominio) ds_dominio, 
          a.nr_seq_localizador, 
          a.ds_mascara, 
          a.nm_atributo_pai, 
          obter_desc_expressao(a.cd_exp_label_grid) ds_label_grid, 
          a.nr_seq_apresent nr_seq_apres, 
          nvl(a.nr_seq_grid, a.nr_seq_apresent) nr_seq_grid, 
          a.ie_nova_linha, 
          a.qt_tam_delphi qt_tamanho, 
          a.qt_altura, 
          a.vl_padrao, 
          a.ie_obrigatorio, 
          a.ie_readonly, 
          a.ie_tabstop, 
          c.ie_informacao_sensivel ie_sensivel, 
          a.qt_coluna, 
          nvl(a.qt_tam_grid, 120) qt_tam_grid,
          decode(upper(a.ie_componente), null, 'de', a.ie_componente) ie_componente,
          obter_valor_dominio(1043, a.ie_componente) ds_componente,
          (case when a.cd_exp_label is not null then 'S' else 'N' end) ie_marcar,
          (select ds_sql from dic_objeto where a.nr_seq_localizador is not null and nr_sequencia = a.nr_seq_dic_objeto and ie_tipo_objeto = 'SQL') ds_dic_objeto,
          a.ds_valores
from    tabela_visao_atributo  a, 
        tabela_visao  b, 
        tabela_atributo  c, 
        linked_data  d, 
        --splitando string "key1,key2,key3..." por linha
        (with temp as  ( 
            select keys_p ds_atributo  from dual 
          ) select trim(regexp_substr(ds_atributo, '[^,]+', 1, level)) ds_atributo 
              from (select  ds_atributo from temp) t 
              connect by instr(ds_atributo, ',', 1, level - 1) > 0 
        ) e 
where   d.nr_sequencia = nr_seq_linked_data_p
and     a.nr_sequencia = d.nr_seq_vision
and     b.nr_sequencia = a.nr_sequencia 
and     b.nm_tabela    = c.nm_tabela 
and     c.nm_atributo  = a.nm_atributo 
and     b.nm_tabela = d.nm_table 
and     a.nm_atributo not in ( 
            select nm_atributo 
            from tabela_atributo_linked 
            where nr_seq_elemento = nr_seq_template_p 
        ) 
and     a.nm_atributo = TO_CHAR(e.ds_atributo)  --pegando apenas keys que foram enviadas 
order by a.nm_atributo;

  CURSOR c02(nm_atributo_pai_p VARCHAR2) IS
    SELECT DISTINCT a.nm_atributo nm_atributo
                   ,b.ie_tipo_atributo ie_tipo_atributo
                   ,a.cd_exp_label
           ,a.cd_exp_label_grid
                   ,obter_desc_expressao(a.cd_exp_label) ds_exp_label
                   ,a.cd_exp_valores
                   ,obter_desc_expressao(a.cd_exp_valores) ds_exp_valores
                   ,a.cd_dominio
                   ,obter_nome_dominio(a.cd_dominio) ds_dominio
                   ,a.nr_seq_localizador
                   ,a.ds_mascara
                   ,a.nm_atributo_pai
                   ,obter_desc_expressao(a.cd_exp_label_grid) ds_label_grid
                   ,a.nr_seq_apresent nr_seq_apres
                   ,nvl(a.nr_seq_grid, a.nr_seq_apresent) nr_seq_grid
                   ,a.ie_nova_linha
                   ,a.qt_tam_delphi qt_tamanho
                   ,a.qt_altura
                   ,a.vl_padrao
                   ,a.ie_obrigatorio
                   ,a.ie_readonly
                   ,a.ie_tabstop
                   ,b.ie_informacao_sensivel ie_sensivel
                   ,a.qt_coluna
                   ,nvl(a.qt_tam_grid, 120) qt_tam_grid
                   ,decode(upper(a.ie_componente), NULL, 'de', a.ie_componente) ie_componente
                   ,obter_valor_dominio(1043, a.ie_componente) ds_componente
                   ,(SELECT ds_sql
                       FROM dic_objeto
                      WHERE a.nr_seq_localizador IS NOT NULL
                        AND nr_sequencia = a.nr_seq_dic_objeto
                        AND ie_tipo_objeto = 'SQL') ds_dic_objeto
                   ,a.ds_valores
      FROM tabela_visao_atributo a
          ,tabela_atributo       b
          ,linked_data           c
     WHERE a.nr_sequencia = c.nr_seq_vision
       AND a.nm_atributo = b.nm_atributo
       AND b.nm_tabela = c.nm_table
       AND c.nr_sequencia = nr_seq_linked_data_p
       AND a.nm_atributo NOT IN
           (SELECT nm_atributo
              FROM tabela_atributo_linked
             WHERE nr_seq_elemento = nr_seq_template_p)
       AND a.nm_atributo = nm_atributo_pai_p;
       
  CURSOR c03 IS
    SELECT a.nm_atributo
          ,a.ie_visivel_grid
          ,a.ie_visivel_detalhe
          ,a.nr_sequencia
      FROM tabela_atributo_linked a
     WHERE a.nr_seq_elemento = nr_seq_template_p
       AND a.nr_seq_linked = nr_seq_linked_data_p
       AND a.nm_atributo IN (SELECT DISTINCT TRIM(regexp_substr(t.nm_atributo_pai, '[^,]+', 1, LEVEL)) nm_atributo
                               FROM (SELECT t.nm_atributo_pai
                                       FROM tabela_atributo_linked t
                                      WHERE t.nr_seq_elemento = nr_seq_template_p
                                        AND t.nr_seq_linked = nr_seq_linked_data_p
                                        AND t.nm_atributo_pai IS NOT NULL) t
                            CONNECT BY instr(nm_atributo_pai, ',', 1, LEVEL - 1) > 0)
       AND a.ie_visivel_grid = 'S';
  
nm_usuario_w                    tabela_atributo_linked.nm_usuario%type;
nr_atributo_seq_w               tabela_atributo_linked.nr_sequencia%type;
ie_not_visible_philips_w        tabela_atributo_linked.ie_not_visible_philips%type := 'N';
ie_readonly_philips_w           tabela_atributo_linked.ie_readonly_philips%type    := 'N';
ie_obrigatorio_philips_w        tabela_atributo_linked.ie_obrigatorio_philips%type := 'N';
ds_sql_lookup_w                 tabela_atributo_linked.ds_sql_lookup%type;
ds_valores_w                  tabela_atributo_linked.DS_VALORES%type;
ds_valor_padrao_w               varchar2(4000);
ds_sensivel_w                   varchar2(1) := 'N';
qt_tamanho_w                    tabela_atributo_linked.qt_tamanho%type;
ie_visivel_grid_w               tabela_atributo_linked.ie_visivel_grid%type;
ie_visivel_detalhe_w            tabela_atributo_linked.ie_visivel_detalhe%type;
nm_atributo_pai_lista           varchar2(1000);
ie_cluster_w                    ehr_template.ie_cluster%TYPE;

begin

if (nr_seq_template_p is not null and nr_seq_linked_data_p is not null and keys_p is not null) then

  if(nm_usuario_p is null or nm_usuario_p = '') then
    select obter_usuario_ativo 
    into nm_usuario_w 
    from dual;
  else
    nm_usuario_w := nm_usuario_p;
  end if;
  
  SELECT nvl(t.ie_cluster, 'N') ie_cluster
    INTO ie_cluster_w
    FROM ehr_template t
   WHERE t.nr_sequencia =
         (SELECT e.nr_seq_template
            FROM ehr_template_conteudo e
           WHERE e.nr_sequencia = nr_seq_template_p);

  for atributo in c01 loop
    select  tabela_atributo_linked_seq.nextval
    into  nr_atributo_seq_w
    from  dual;
    
    ie_not_visible_philips_w := 'N';
    ie_readonly_philips_w := 'N';
    ie_obrigatorio_philips_w := 'N';
    ds_sql_lookup_w := null;
    
    if(atributo.nr_seq_apres is null) then
      ie_not_visible_philips_w := 'S';
    end if;
    
    if(atributo.ie_readonly = 'S') then
      ie_readonly_philips_w := 'S';
    end if;
    
    if(atributo.ie_obrigatorio = 'S') then
      ie_obrigatorio_philips_w := 'S';
    end if;
    
    ds_valor_padrao_w := atributo.vl_padrao;
    ds_valores_w := null;
    
    if(atributo.nr_seq_localizador is not null and atributo.ds_dic_objeto is not null) then
      /*A CONSULTA PARA LOCALIZADOR PRECISA INICIAR E FINALIZAR COM ()*/
      ds_sql_lookup_w := atributo.ds_dic_objeto;
    elsif (atributo.ie_componente is not null and upper(atributo.ie_componente) = 'LCB' and atributo.ds_valores is not null) then
      ds_sql_lookup_w := atributo.ds_valores;
    elsif (atributo.ie_componente is not null and upper(atributo.ie_componente) = 'ATC' and atributo.ds_valores is not null) then
      ds_valores_w := atributo.ds_valores;
    elsif (atributo.vl_padrao is not null and trim(upper(atributo.vl_padrao)) = '@SQL' and atributo.ds_valores is not null) then
      ds_valor_padrao_w := '@SQL ' || atributo.ds_valores;
    elsif (atributo.ie_componente is not null and upper(atributo.ie_componente) = 'DE' and atributo.ds_valores is not null) then
      ds_valores_w := atributo.ds_valores;
    elsif (atributo.ie_componente is not null and upper(atributo.ie_componente) = 'CB' and atributo.ds_valores is not null) then
      ds_valores_w := atributo.ds_valores;
    elsif (atributo.ie_componente is not null and upper(atributo.ie_componente) = 'TX' and atributo.ds_valores is not null) then
      ds_valores_w := atributo.ds_valores;
    end if;
        
    --tamanho em pixel
    if (atributo.qt_tamanho is null) then
      qt_tamanho_w := 150;
    elsif (atributo.qt_tamanho > 0 or atributo.qt_tamanho <= 12) then
      if (atributo.qt_tamanho = 1) then
        qt_tamanho_w := 50;
      elsif (atributo.qt_tamanho = 2) then
        qt_tamanho_w := 100;
      elsif (atributo.qt_tamanho = 3) then
        qt_tamanho_w := 150;
      elsif (atributo.qt_tamanho = 4) then
        qt_tamanho_w := 225;
      elsif (atributo.qt_tamanho = 5) then
        qt_tamanho_w := 350;
      elsif (atributo.qt_tamanho = 6) then
        qt_tamanho_w := 600;
      elsif (atributo.qt_tamanho = 7) then
        qt_tamanho_w := 750;
      elsif (atributo.qt_tamanho = 8) then
        qt_tamanho_w := 950;
      elsif (atributo.qt_tamanho = 9) then
        qt_tamanho_w := 1150;
      elsif (atributo.qt_tamanho = 10) then
        qt_tamanho_w := 1350;
      elsif (atributo.qt_tamanho = 11) then
        qt_tamanho_w := 1550;
      elsif (atributo.qt_tamanho = 12) then
        qt_tamanho_w := 1920;
      end if;
    elsif (atributo.qt_tamanho <= 50) then
      qt_tamanho_w := 50;
    else 
      qt_tamanho_w := atributo.qt_tamanho;
    end if;
  
  


    ie_visivel_grid_w := 'S';
    ie_visivel_detalhe_w := 'S';
  
  If (upper(atributo.ie_tipo_atributo) = 'FUNCTION') then
  
    if (atributo.CD_EXP_LABEL is not null) then
    
      ie_visivel_grid_w := 'N';
      
    elsif (atributo.cd_exp_label_grid is not null) then
    
      ie_visivel_detalhe_w  := 'N';
    
    end if;
  
  
  end if;
    
    IF ie_cluster_w = 'N' THEN
      ie_visivel_grid_w := 'N';
    END IF;
  
    if (atributo.nm_atributo = 'NR_SEQUENCIA') then
      ie_visivel_grid_w := 'N';
      ie_visivel_detalhe_w := 'N';
    end if;
    
    if atributo.nm_atributo_pai is not null then
      nm_atributo_pai_lista := nm_atributo_pai_lista || ',' || atributo.nm_atributo_pai;
    end if;

    insert into tabela_atributo_linked (
                ds_elemento, 
                ds_exp_cliente, 
                ds_formula, 
                ds_msg_aviso, 
                ds_msg_bloqueio, 
                ds_propriedade, 
                ds_sql, 
                dt_atualizacao_nrec, 
                dt_atualizacao, 
                ie_copia, 
                ie_evolucao, 
                ie_info_paciente, 
                ie_macro, 
                ie_not_visible_philips, 
                ie_obrigatorio_philips, 
                ie_opcional, 
                ie_readonly_philips, 
                nm_usuario_nrec, 
                nm_usuario, 
                nr_elemento, 
                nr_seq_camp_sup, 
                nr_seq_elem_visual, 
                nr_seq_elemento, 
                nr_seq_linked, 
                nr_seq_texto_padrao, 
                nr_sequencia, 
                qt_caracter, 
                vl_maximo_aviso, 
                vl_maximo_bloqueio, 
                vl_minimo_aviso, 
                vl_minimo_bloqueio, 
                /*--*/
                cd_dominio, 
                cd_exp_label, 
                cd_exp_valores, 
                ds_label_grid, 
                ds_mascara, 
                ie_componente,
                ie_nova_linha, 
                ie_obrigatorio, 
                ie_readonly, 
                ie_sensivel, 
                ie_tabstop, 
                ie_tipo_atributo, 
                nm_atributo_pai, 
                nm_atributo, 
                nr_seq_apres, 
                nr_seq_grid, 
                nr_seq_localizador, 
                qt_altura, 
                qt_coluna, 
                qt_tamanho, 
                vl_padrao,
                ds_sql_lookup,
                ie_visivel_grid,
                ie_visivel_detalhe,
                ds_valores,
                qt_tamanho_larg,
                cd_exp_label_grid
				
    ) values (
        null, 
        null, 
        null, 
        null, 
        null, 
        null, 
        null, 
        sysdate, 
        sysdate, 
        null, 
        null, 
        null, 
        null, 
        ie_not_visible_philips_w, 
        ie_obrigatorio_philips_w, 
        null, 
        ie_readonly_philips_w, 
        nm_usuario_w, 
        nm_usuario_w, 
        null, 
        null, 
        null, 
        nr_seq_template_p, 
        nr_seq_linked_data_p,
        null, 
        nr_atributo_seq_w,
        null, 
        null, 
        null, 
        null, 
        null, 
          /*--*/
        atributo.cd_dominio, 
        atributo.cd_exp_label, 
        atributo.cd_exp_valores, 
        null, 
        atributo.ds_mascara, 
        atributo.ie_componente,
        atributo.ie_nova_linha, 
        atributo.ie_obrigatorio, 
        atributo.ie_readonly, 
        ds_sensivel_w,
        atributo.ie_tabstop, 
        atributo.ie_tipo_atributo, 
        atributo.nm_atributo_pai, 
        atributo.nm_atributo, 
        atributo.nr_seq_apres, 
        atributo.nr_seq_grid, 
        atributo.nr_seq_localizador, 
        atributo.qt_altura, 
        atributo.qt_coluna, 
        qt_tamanho_w, 
        ds_valor_padrao_w,
        ds_sql_lookup_w,
        ie_visivel_grid_w,
        ie_visivel_detalhe_w,
        ds_valores_w,
        atributo.qt_tam_grid,
        atributo.cd_exp_label_grid
        
    );    
  end loop;
  
  for r_atributo_pai in (select distinct trim(regexp_substr(nm_atributo_pai, '[^,]+', 1, level)) nm_atributo
                           from (select nm_atributo_pai_lista nm_atributo_pai
                                   from dual) t
                          where trim(regexp_substr(nm_atributo_pai, '[^,]+', 1, level)) is not null
               						  and trim(regexp_substr(nm_atributo_pai, '[^,]+', 1, level)) not in ('NR_ATENDIMENTO','NR_SEQUENCIA')
                        connect by instr(nm_atributo_pai, ',', 1, level - 1) > 0) loop
    for r_atributo_visao in c02(r_atributo_pai.nm_atributo) loop 

      select tabela_atributo_linked_seq.nextval
        into nr_atributo_seq_w
        from dual;
        
      ie_readonly_philips_w := 'N';
      ie_obrigatorio_philips_w := 'N';
      ie_not_visible_philips_w := 'N';
      ds_sql_lookup_w := null;
        
      if(r_atributo_visao.nr_seq_apres is null) then
        ie_not_visible_philips_w := 'S';
      end if;
    
      if(r_atributo_visao.ie_readonly = 'S') then
        ie_readonly_philips_w := 'S';
      end if;
    
      if(r_atributo_visao.ie_obrigatorio = 'S') then
        ie_obrigatorio_philips_w := 'S';
      end if;
    
      ds_valor_padrao_w := r_atributo_visao.vl_padrao;
      ds_valores_w := null;
    
      if(r_atributo_visao.nr_seq_localizador is not null and r_atributo_visao.ds_dic_objeto is not null) then
        /*A CONSULTA PARA LOCALIZADOR PRECISA INICIAR E FINALIZAR COM ()*/
        ds_sql_lookup_w := r_atributo_visao.ds_dic_objeto;
      elsif (r_atributo_visao.ie_componente is not null and upper(r_atributo_visao.ie_componente) = 'LCB' and r_atributo_visao.ds_valores is not null) then
        ds_sql_lookup_w := r_atributo_visao.ds_valores;
      elsif (r_atributo_visao.ie_componente is not null and upper(r_atributo_visao.ie_componente) = 'ATC' and r_atributo_visao.ds_valores is not null) then
        ds_valores_w := r_atributo_visao.ds_valores;
      elsif (r_atributo_visao.vl_padrao is not null and trim(upper(r_atributo_visao.vl_padrao)) = '@SQL' and r_atributo_visao.ds_valores is not null) then
        ds_valor_padrao_w := '@SQL ' || r_atributo_visao.ds_valores;
      elsif (r_atributo_visao.ie_componente is not null and upper(r_atributo_visao.ie_componente) = 'DE' and r_atributo_visao.ds_valores is not null) then
        ds_valores_w := r_atributo_visao.ds_valores;
      end if;
        
      --tamanho em pixel
      if (r_atributo_visao.qt_tamanho is null) then
        qt_tamanho_w := 150;
      elsif (r_atributo_visao.qt_tamanho > 0 or r_atributo_visao.qt_tamanho <= 12) then
        if (r_atributo_visao.qt_tamanho = 1) then
          qt_tamanho_w := 50;
        elsif (r_atributo_visao.qt_tamanho = 2) then
          qt_tamanho_w := 100;
        elsif (r_atributo_visao.qt_tamanho = 3) then
          qt_tamanho_w := 150;
        elsif (r_atributo_visao.qt_tamanho = 4) then
          qt_tamanho_w := 225;
        elsif (r_atributo_visao.qt_tamanho = 5) then
          qt_tamanho_w := 350;
        elsif (r_atributo_visao.qt_tamanho = 6) then
          qt_tamanho_w := 600;
        elsif (r_atributo_visao.qt_tamanho = 7) then
          qt_tamanho_w := 750;
        elsif (r_atributo_visao.qt_tamanho = 8) then
          qt_tamanho_w := 950;
        elsif (r_atributo_visao.qt_tamanho = 9) then
          qt_tamanho_w := 1150;
        elsif (r_atributo_visao.qt_tamanho = 10) then
          qt_tamanho_w := 1350;
        elsif (r_atributo_visao.qt_tamanho = 11) then
          qt_tamanho_w := 1550;
        elsif (r_atributo_visao.qt_tamanho = 12) then
          qt_tamanho_w := 1920;
        end if;
      elsif (r_atributo_visao.qt_tamanho <= 50) then
        qt_tamanho_w := 50;
      else 
        qt_tamanho_w := r_atributo_visao.qt_tamanho;
      end if;

      ie_visivel_grid_w := 'N';
      ie_visivel_detalhe_w := 'N';
      
      insert into tabela_atributo_linked (
        ds_elemento, 
        ds_exp_cliente, 
        ds_formula, 
        ds_msg_aviso, 
        ds_msg_bloqueio, 
        ds_propriedade, 
        ds_sql, 
        dt_atualizacao_nrec, 
        dt_atualizacao, 
        ie_copia, 
        ie_evolucao, 
        ie_info_paciente, 
        ie_macro, 
        ie_not_visible_philips, 
        ie_obrigatorio_philips, 
        ie_opcional, 
        ie_readonly_philips, 
        nm_usuario_nrec, 
        nm_usuario, 
        nr_elemento, 
        nr_seq_camp_sup, 
        nr_seq_elem_visual, 
        nr_seq_elemento, 
        nr_seq_linked, 
        nr_seq_texto_padrao, 
        nr_sequencia, 
        qt_caracter, 
        vl_maximo_aviso, 
        vl_maximo_bloqueio, 
        vl_minimo_aviso, 
        vl_minimo_bloqueio, 
        /*--*/
        cd_dominio, 
        cd_exp_label, 
        cd_exp_valores, 
        ds_label_grid, 
        ds_mascara, 
        ie_componente,
        ie_nova_linha, 
        ie_obrigatorio, 
        ie_readonly, 
        ie_sensivel, 
        ie_tabstop, 
        ie_tipo_atributo, 
        nm_atributo_pai, 
        nm_atributo, 
        nr_seq_apres, 
        nr_seq_grid, 
        nr_seq_localizador, 
        qt_altura, 
        qt_coluna, 
        qt_tamanho, 
        vl_padrao,
        ds_sql_lookup,
        ie_visivel_grid,
        ie_visivel_detalhe,
        ds_valores,
        qt_tamanho_larg)
      values (
        null, 
        null, 
        null, 
        null, 
        null, 
        null, 
        null, 
        sysdate, 
        sysdate, 
        null, 
        null, 
        null, 
        null, 
        ie_not_visible_philips_w, 
        ie_obrigatorio_philips_w, 
        null, 
        ie_readonly_philips_w, 
        nm_usuario_w, 
        nm_usuario_w, 
        null, 
        null, 
        null, 
        nr_seq_template_p, 
        nr_seq_linked_data_p,
        null, 
        nr_atributo_seq_w,
        null, 
        null, 
        null, 
        null, 
        null, 
        /*--*/
        r_atributo_visao.cd_dominio, 
        r_atributo_visao.cd_exp_label, 
        r_atributo_visao.cd_exp_valores, 
        null, 
        r_atributo_visao.ds_mascara, 
        r_atributo_visao.ie_componente,
        r_atributo_visao.ie_nova_linha, 
        r_atributo_visao.ie_obrigatorio, 
        r_atributo_visao.ie_readonly, 
        ds_sensivel_w,
        r_atributo_visao.ie_tabstop, 
        r_atributo_visao.ie_tipo_atributo, 
        r_atributo_visao.nm_atributo_pai, 
        r_atributo_visao.nm_atributo, 
        r_atributo_visao.nr_seq_apres, 
        r_atributo_visao.nr_seq_grid, 
        r_atributo_visao.nr_seq_localizador, 
        r_atributo_visao.qt_altura, 
        r_atributo_visao.qt_coluna, 
        qt_tamanho_w, 
        ds_valor_padrao_w,
        ds_sql_lookup_w,
        ie_visivel_grid_w,
        ie_visivel_detalhe_w,
        ds_valores_w,
        r_atributo_visao.qt_tam_grid); 
        
    end loop;
  end loop;
  
  FOR r_grid IN c03 LOOP
    UPDATE tabela_atributo_linked t
       SET t.ie_visivel_grid = 'N'
     WHERE t.nr_sequencia = r_grid.nr_sequencia;
  END LOOP;
end if;

commit;

end gerar_linked_data_template_key;
/