create or replace
procedure	far_altera_qtde_item_ped(
			nr_seq_pedido_p		number,
			nr_seq_item_p		number,
			qt_item_p		number) is

vl_unitario_item_w			number(13,4);
vl_total_item_w			number(13,2);
vl_desconto_w			number(13,4);
vl_liquido_item_w			number(13,2);
vl_rateado_w			number(13,4);

begin

select	vl_unitario_item,
	vl_desconto,
	vl_rateado
into	vl_unitario_item_w,
	vl_desconto_w,
	vl_rateado_w
from	far_pedido_item
where	nr_seq_pedido = nr_seq_pedido_p
and	nr_seq_item = nr_seq_item_p;

vl_total_item_w	:= round((qt_item_p * vl_unitario_item_w),2);
vl_liquido_item_w	:= (nvl(vl_total_item_w,0) - (nvl(vl_desconto_w,0) + nvl(vl_rateado_w,0)));

update	far_pedido_item
set	qt_material = qt_item_p,
	vl_total_item = vl_total_item_w,
	vl_liquido_item = vl_liquido_item_w,
	vl_desconto = (vl_desconto_w * qt_item_p)
where	nr_seq_pedido = nr_seq_pedido_p
and	nr_seq_item = nr_seq_item_p;

commit;

far_atualizar_vl_total_ped(nr_seq_pedido_p);

end far_altera_qtde_item_ped;
/