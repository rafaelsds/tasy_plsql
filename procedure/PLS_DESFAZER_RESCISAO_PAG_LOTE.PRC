create or replace
procedure pls_desfazer_rescisao_pag_lote
			(	nr_seq_lote_p		number,
				nm_usuario_p		varchar2,
				cd_estabelecimento_p	varchar2) is

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  desfazer a rescis�o dos pagadores  do lote
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:Performance.
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

nr_seq_notific_pagador_w	number(10);
ie_status_ant_w			varchar2(2);
nr_seq_contrato_w		number(10);

Cursor C01 is
	select	nr_sequencia,
		ie_status_ant
	from	pls_notificacao_pagador
	where	nr_seq_lote	= nr_seq_lote_p;

Cursor C02 is
	select	nr_seq_contrato
	from	pls_notificacao_item
	where	nr_seq_notific_pagador 	= nr_seq_notific_pagador_w;

begin
open C01;
loop
fetch C01 into
	nr_seq_notific_pagador_w,
	ie_status_ant_w;
exit when C01%notfound;
	begin
	open C02;
	loop
	fetch C02 into
		nr_seq_contrato_w;
	exit when C02%notfound;
		begin
		pls_reativar_contrato(	nr_seq_contrato_w,
					'',
					'',
					'',
					sysdate,
					'',
					'S',
					'S',
					'S',
					'S',
					'S',
					nm_usuario_p,
					cd_estabelecimento_p,
					'N');
		end;
	end loop;
	close C02;
	
	update	pls_notificacao_pagador
	set	dt_rescisao		= null,
		ie_status		= nvl(ie_status_ant_w, 'G'),
		nm_usuario_rescisao	= '',
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_seq_notific_pagador_w;
	end;
end loop;
close C01;

update	pls_notificacao_lote
set	dt_rescisao	= null,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_lote_p;

commit;

end pls_desfazer_rescisao_pag_lote;
/
