create or replace
procedure lab_marcar_repeticao( nr_prescricao_p	number,
				     nr_seq_prescr_p	number,
				     nm_usuario_p	varchar2 ) is 
begin
	insere_usuario_repeticao( nr_prescricao_p, nr_seq_prescr_p, nm_usuario_p);
	
	update	prescr_procedimento 
	set	ie_exame_bloqueado = 'R',
		nm_usuario = nm_usuario_p,
		dt_atualizacao = sysdate
	where	nr_sequencia = nr_seq_prescr_p
	and	nr_prescricao = nr_prescricao_p;
commit;
end lab_marcar_repeticao;
/
