create or replace
procedure pls_gerar_tabela_segurado
		(	nr_seq_segurado_p	pls_segurado.nr_sequencia%type,
			nm_usuario_p		usuario.nm_usuario%type,
			ie_commit_p		varchar2) is

nr_seq_contrato_w	number(10);
nr_seq_tabela_w		number(10);
cd_estabelecimento_w	number(10);
vl_parametro_w		varchar2(2);
ie_reajuste_w		varchar2(1);
nm_beneficiario_w	varchar2(255);
nr_seq_tabela_nova_w	number(10);
qt_faixa_etaria_w	number(10);
nr_seq_preco_w		number(10);
qt_idade_inicial_w	number(10);
nr_segurado_w		pls_tabela_preco.nr_segurado%type;

Cursor C01 is
	select	nr_sequencia,
		qt_idade_inicial
	from	pls_plano_preco
	where	nr_seq_tabela = nr_seq_tabela_w
	order by qt_idade_inicial;

begin

select	a.nr_seq_contrato,
	a.nr_seq_tabela,
	a.cd_estabelecimento,
	b.nr_segurado,
	obter_nome_pf(a.cd_pessoa_fisica)
into	nr_seq_contrato_w,
	nr_seq_tabela_w,
	cd_estabelecimento_w,
	nr_segurado_w,
	nm_beneficiario_w
from	pls_segurado a,
	pls_tabela_preco b
where	b.nr_sequencia = a.nr_seq_tabela
and	a.nr_sequencia = nr_seq_segurado_p;

Obter_Param_Usuario(1202, 114, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, vl_parametro_w);

if	(nvl(vl_parametro_w,'N') = 'S' and nr_segurado_w is null) then
	select	ie_reajuste
	into	ie_reajuste_w
	from	pls_contrato
	where	nr_sequencia = nr_seq_contrato_w;
	
	if	(ie_reajuste_w = 'A') then
		select	pls_tabela_preco_seq.nextval
		into	nr_seq_tabela_nova_w
		from	dual;
		
		insert into pls_tabela_preco
			(	nr_sequencia, dt_atualizacao, nm_usuario, nm_tabela,
				dt_inicio_vigencia, nr_seq_plano, cd_codigo_ant, ie_tabela_base,
				nr_seq_tabela_origem, nr_segurado, nr_contrato, dt_liberacao,
				dt_atualizacao_nrec, nm_usuario_nrec, dt_fim_vigencia, ie_tabela_contrato,
				ie_proposta_adesao, ie_simulacao_preco, nr_seq_sca, nr_seq_tabela_ant,
				nr_seq_proposta, nr_seq_faixa_etaria, nr_seq_contrato_inter,
				nr_seq_tabela_original, nr_seq_contrato_repasse, nr_seq_simul_perfil,
				nr_seq_simulacao_preco, nr_seq_tabela_simul_orig, ie_preco_vidas_contrato,
				ie_calculo_vidas, tx_adaptacao, nr_seq_tabela_interc)
			(	select	nr_seq_tabela_nova_w, sysdate, nm_usuario_p, wheb_mensagem_pck.get_texto(1109063,'NR_SEQ_SEGURADO=' || to_char(nr_seq_segurado_p)  || ';' ||'NM_BENEFICIARIO='||nm_beneficiario_w),
					dt_inicio_vigencia, nr_seq_plano, cd_codigo_ant, 'N',
					nr_sequencia, nr_seq_segurado_p, nr_seq_contrato_w, sysdate,
					sysdate, nm_usuario_p, dt_fim_vigencia, ie_tabela_contrato,
					ie_proposta_adesao, ie_simulacao_preco, nr_seq_sca, nr_seq_tabela_ant,
					nr_seq_proposta, nr_seq_faixa_etaria, nr_seq_contrato_inter,
					nr_seq_tabela_original, nr_seq_contrato_repasse, nr_seq_simul_perfil,
					nr_seq_simulacao_preco, nr_seq_tabela_simul_orig, ie_preco_vidas_contrato,
					ie_calculo_vidas, tx_adaptacao, nr_seq_tabela_interc
				from	pls_tabela_preco
				where	nr_sequencia = nr_seq_tabela_w);

		select	count(*)
		into	qt_faixa_etaria_w
		from	pls_plano_preco
		where	nr_seq_tabela = nr_seq_tabela_w;

		if	(qt_faixa_etaria_w > 0) then
			open C01;
			loop
			fetch C01 into
				nr_seq_preco_w,
				qt_idade_inicial_w;
			exit when C01%notfound;
				begin

				insert into pls_plano_preco
					(	nr_sequencia, dt_atualizacao, nm_usuario, nr_seq_tabela,
						qt_idade_inicial, qt_idade_final, vl_preco_inicial, vl_preco_atual,
						tx_acrescimo, ie_grau_titularidade, qt_vidas_inicial, qt_vidas_final,
						vl_adaptacao, vl_preco_nao_subsidiado, vl_preco_nao_subsid_atual,
						dt_atualizacao_nrec, nm_usuario_nrec, vl_minimo, ie_manutencao_preco)
					(	select	pls_plano_preco_seq.nextval, sysdate, nm_usuario_p, nr_seq_tabela_nova_w,
							qt_idade_inicial, qt_idade_final, vl_preco_atual, vl_preco_atual,
							0, ie_grau_titularidade, qt_vidas_inicial, qt_vidas_final,
							vl_adaptacao, vl_preco_nao_subsidiado, vl_preco_nao_subsid_atual,
							sysdate, nm_usuario_p, vl_minimo, ie_manutencao_preco
						from	pls_plano_preco
						where	nr_sequencia = nr_seq_preco_w);

				end;
			end loop;
			close C01;
		end if;

		update	pls_segurado
		set	nr_seq_tabela = nr_seq_tabela_nova_w,
			nr_seq_tabela_origem = nr_seq_tabela_w
		where	nr_sequencia = nr_seq_segurado_p;

	end if;
end if;

if	(nvl(ie_commit_p,'N') = 'S') then
	commit;
end if;

end pls_gerar_tabela_segurado;
/