create or replace
procedure qua_gerar_pf_lib_doc_trein(
					nr_seq_treinamento_p	number,
					cd_liberacao_p		varchar2,
					ie_opcao_p		varchar2,
					nm_usuario_p		Varchar2) is

cd_estabelecimento_w			number(04,0);					
ie_somente_funcionario_w			varchar2(01);
cd_pessoa_fisica_w			varchar2(10);
cd_setor_atendimento_w			number(10,0);

Cursor C01 is
	select	distinct
		a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	usuario_perfil b,
		usuario a		
	where	b.nm_usuario	= a.nm_usuario
	and	a.ie_situacao	= 'A'
	and	b.cd_perfil	= cd_liberacao_p
	and	a.cd_pessoa_fisica is not null
	and	(ie_somente_funcionario_w = 'N' or nvl(substr(obter_dados_pf(a.cd_pessoa_fisica,'F'),1,1),'N') = 'S')
	and not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);
				
Cursor C02 is
	select	distinct
		a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	pessoa_fisica b,
		usuario a	
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica	
	and	a.ie_situacao		= 'A'
	and	b.cd_cargo 		= cd_liberacao_p
	and	a.cd_pessoa_fisica is not null
	and	(ie_somente_funcionario_w = 'N' or nvl(b.ie_funcionario,'N') = 'S')
	and not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);
				
Cursor C03 is
	select	distinct
		a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	usuario a
	where	a.ie_situacao		= 'A'
	and	a.cd_setor_atendimento	= cd_liberacao_p
	and	a.cd_pessoa_fisica is not null
	and	(ie_somente_funcionario_w = 'N' or nvl(substr(obter_dados_pf(a.cd_pessoa_fisica,'F'),1,1),'S') = 'S')
	and not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);
				
Cursor C04 is
	select	distinct
		a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	qua_cargo_agrup d,
		qua_grupo_cargo c,
		pessoa_fisica b,
		usuario a	
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
	and	c.nr_sequencia		= d.nr_seq_agrup
	and	b.cd_cargo		= d.cd_cargo
	and	a.ie_situacao		= 'A'
	and	c.nr_sequencia 		= cd_liberacao_p
	and	a.cd_pessoa_fisica is not null
	and	(ie_somente_funcionario_w = 'N' or nvl(b.ie_funcionario,'N') = 'S')
	and not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);
				
Cursor C05 is
	select	distinct
		a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	usuario_grupo c,
		grupo_usuario b,
		usuario a
	where	a.nm_usuario		= c.nm_usuario
	and	b.nr_sequencia		= c.nr_seq_grupo
	and	a.ie_situacao		= 'A'
	and	c.ie_situacao		= 'A'
	and	b.nr_sequencia		= cd_liberacao_p
	and	a.cd_pessoa_fisica is not null
	and	(ie_somente_funcionario_w = 'N' or nvl(substr(obter_dados_pf(a.cd_pessoa_fisica,'F'),1,1),'S') = 'S')	
	and not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);
				
Cursor C06 is
	select	distinct
		a.cd_pessoa_fisica,
		a.cd_setor_atendimento
	from	grupo_perfil_item d,
		grupo_perfil c,
		usuario_perfil b,
		usuario a		
	where	b.nm_usuario	= a.nm_usuario
	and	c.nr_sequencia 	= d.nr_seq_grupo_perfil
	and	b.cd_perfil	= d.cd_perfil
	and	a.ie_situacao	= 'A'
	and	c.nr_sequencia	= cd_liberacao_p
	and	a.cd_pessoa_fisica is not null
	and	(ie_somente_funcionario_w = 'N' or nvl(substr(obter_dados_pf(a.cd_pessoa_fisica,'F'),1,1),'N') = 'S')	
	and not exists	(	select	1
				from	qua_doc_trein_pessoa x
				where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
				and	x.nr_seq_treinamento	= nr_seq_treinamento_p);

begin

if	(nvl(nr_seq_treinamento_p,0) > 0) and
	(nvl(cd_liberacao_p,'X') <> 'X') and
	(nvl(ie_opcao_p,'X') <> 'X') then
	begin	
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	qua_documento
	where	nr_sequencia = (select	nr_seq_documento
				from	qua_doc_treinamento
				where	nr_sequencia = nr_seq_treinamento_p);

	select	substr(nvl(obter_valor_param_usuario(4000, 75, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w),'N'),1,1)
	into	ie_somente_funcionario_w
	from	dual;
	
	if	(ie_opcao_p = 'P') then
		begin
		open C01;
		loop
		fetch C01 into	
			cd_pessoa_fisica_w,
			cd_setor_atendimento_w;
		exit when C01%notfound;
			begin
			insert into qua_doc_trein_pessoa(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_treinamento,
						cd_pessoa_fisica,
						ie_faltou,
						cd_setor_atendimento)
					values(	qua_doc_trein_pessoa_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_treinamento_p,
						cd_pessoa_fisica_w,
						'N',
						cd_setor_atendimento_w);
			end;
		end loop;
		close C01;
		end;
	elsif	(ie_opcao_p = 'C') then
		begin
		open C02;
		loop
		fetch C02 into	
			cd_pessoa_fisica_w,
			cd_setor_atendimento_w;
		exit when C02%notfound;
			begin
			insert into qua_doc_trein_pessoa(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_treinamento,
						cd_pessoa_fisica,
						ie_faltou,
						cd_setor_atendimento)
					values(	qua_doc_trein_pessoa_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_treinamento_p,
						cd_pessoa_fisica_w,
						'N',
						cd_setor_atendimento_w);
			end;
		end loop;
		close C02;
		end;
	elsif	(ie_opcao_p = 'U') then
		begin
		select	max(a.cd_pessoa_fisica),
			max(cd_setor_atendimento)
		into	cd_pessoa_fisica_w,
			cd_setor_atendimento_w
		from	usuario a
		where	a.nm_usuario = cd_liberacao_p
		and not exists	(	select	1
					from	qua_doc_trein_pessoa x
					where	x.cd_pessoa_fisica	= a.cd_pessoa_fisica
					and	x.nr_seq_treinamento	= nr_seq_treinamento_p);
		
		if	(nvl(cd_pessoa_fisica_w,'0') <> '0') then
			insert into qua_doc_trein_pessoa(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_treinamento,
						cd_pessoa_fisica,
						ie_faltou,
						cd_setor_atendimento)
					values(	qua_doc_trein_pessoa_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_treinamento_p,
						cd_pessoa_fisica_w,
						'N',
						cd_setor_atendimento_w);
		end if;		
		end;
	elsif	(ie_opcao_p = 'S') then
		begin
		open C03;
		loop
		fetch C03 into	
			cd_pessoa_fisica_w,
			cd_setor_atendimento_w;
		exit when C03%notfound;
			begin
			insert into qua_doc_trein_pessoa(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_treinamento,
						cd_pessoa_fisica,
						ie_faltou,
						cd_setor_atendimento)
					values(	qua_doc_trein_pessoa_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_treinamento_p,
						cd_pessoa_fisica_w,
						'N',
						cd_setor_atendimento_w);
			end;
		end loop;
		close C03;
		end;
	elsif	(ie_opcao_p = 'A') then
		begin
		open C04;
		loop
		fetch C04 into	
			cd_pessoa_fisica_w,
			cd_setor_atendimento_w;
		exit when C04%notfound;
			begin
			insert into qua_doc_trein_pessoa(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_treinamento,
						cd_pessoa_fisica,
						ie_faltou,
						cd_setor_atendimento)
					values(	qua_doc_trein_pessoa_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_treinamento_p,
						cd_pessoa_fisica_w,
						'N',
						cd_setor_atendimento_w);
			end;
		end loop;
		close C04;
		end;
	elsif	(ie_opcao_p = 'GU') then
		begin
		open C05;
		loop
		fetch C05 into	
			cd_pessoa_fisica_w,
			cd_setor_atendimento_w;
		exit when C05%notfound;
			begin
			insert into qua_doc_trein_pessoa(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_treinamento,
						cd_pessoa_fisica,
						ie_faltou,
						cd_setor_atendimento)
					values(	qua_doc_trein_pessoa_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_treinamento_p,
						cd_pessoa_fisica_w,
						'N',
						cd_setor_atendimento_w);
			end;
		end loop;
		close C05;
		end;
	elsif	(ie_opcao_p = 'GP') then
		begin
		open C06;
		loop
		fetch C06 into	
			cd_pessoa_fisica_w,
			cd_setor_atendimento_w;
		exit when C06%notfound;
			begin
			insert into qua_doc_trein_pessoa(
						nr_sequencia,
						dt_atualizacao,
						nm_usuario,
						nr_seq_treinamento,
						cd_pessoa_fisica,
						ie_faltou,
						cd_setor_atendimento)
					values(	qua_doc_trein_pessoa_seq.nextval,
						sysdate,
						nm_usuario_p,
						nr_seq_treinamento_p,
						cd_pessoa_fisica_w,
						'N',
						cd_setor_atendimento_w);
			end;
		end loop;
		close C06;
		end;
	end if;
	
	commit;
	end;
end if;

end qua_gerar_pf_lib_doc_trein;
/