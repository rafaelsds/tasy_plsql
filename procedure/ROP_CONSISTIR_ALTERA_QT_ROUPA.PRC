create or replace procedure rop_consistir_altera_qt_roupa(	nr_seq_rop_roupa_p			number,
						qt_altera_p				number,
						cd_operacao_p			number,
						cd_local_p				number,
						nm_usuario_p			varchar2,
						cd_estabelecimento_p	number,
						ie_tipo_altera_p		number,
						ds_erro				out	varchar2) is

/*
criada a partir das consistencias das procedures
rop_baixar_qt_roupa,
rop_adicionar_qt_roupa
Rop_Consiste_item_Movto_Roupa
*/

/*
ie_tipo_altera_P
1 -> Adicionar quantidade
2 -> Diminui quantidade
*/

qt_roupa_ww				number(13,4);
ds_historico_w			varchar2(4000);
ds_motivo_w				varchar2(255);
nr_seq_lote_movto_w		number(10);
cd_estabelecimento_w	number(10);
cd_operacao_w			number(10);
ds_erro_w				varchar2(255):= '';

cd_material_w			material.cd_material%type;
cd_barra_roupa_w		rop_roupa.cd_barra_roupa%type;
ie_situacao_w			rop_roupa.ie_situacao%type;
dt_baixa_w				rop_roupa.dt_baixa%type;
nr_seq_local_atual_w	rop_roupa.nr_seq_local%type;
ds_local_atual_w		rop_local.ds_local%type;
nr_seq_lote_roupa_w		rop_roupa.nr_seq_lote_roupa%type;
dt_liberacao_w			rop_lote_roupa.dt_liberacao%type;
qt_roupa_w				rop_movto_roupa.qt_roupa%type;
ie_operacao_w			rop_operacao.ie_operacao%type;
ie_entrada_saida_w			rop_operacao.ie_entrada_saida%type;
qt_saldo_w			rop_saldo_roupa.qt_saldo%type;
qt_existe_w			number(10);
ds_local_baixa_w			rop_local.ds_local%type;

ie_local_dif_lote_w			funcao_param_usuario.vl_parametro%type;
ie_consiste_qtde_w			funcao_param_usuario.vl_parametro%type;

nr_seq_roupa_w			rop_movto_roupa.nr_seq_roupa%type;

begin

cd_operacao_w		:= nvl(cd_operacao_p,0);
ie_operacao_w		:= substr(rop_obter_dados_operacao(cd_operacao_w, 'T'),1,15);
nr_seq_roupa_w 		:= nr_seq_rop_roupa_p;


select	a.cd_estabelecimento,
	nvl(b.qt_roupa,0)
into	cd_estabelecimento_w,
	qt_roupa_w
from	rop_lote_roupa a,
	rop_roupa b
where	a.nr_sequencia = b.nr_seq_lote_roupa
and	b.nr_sequencia = nr_seq_rop_roupa_p;

if(ie_tipo_altera_p = 2) then
	if	(qt_altera_p > qt_roupa_w) then
		ds_erro_w := substr(ds_erro_w || wheb_mensagem_pck.get_texto(265533) || chr(13) || chr(10),1,255);
		--'A quantidade � ser baixada � maior que a quantidade atual da roupa.'
	end if;
end if;

if (nvl(ds_erro_w,'X') = 'X') then
	select	b.cd_material,
		a.cd_barra_roupa,
		a.ie_situacao,
		a.dt_baixa,
		a.nr_seq_local,
		substr(rop_obter_dados_local(a.nr_seq_local, 'D'),1,80) ds_local_atual,
		a.nr_seq_lote_roupa,
		b.dt_liberacao
	into	cd_material_w,
		cd_barra_roupa_w,
		ie_situacao_w,
		dt_baixa_w,
		nr_seq_local_atual_w,
		ds_local_atual_w,
		nr_seq_lote_roupa_w,
		dt_liberacao_w
	from	rop_roupa a,
		rop_lote_roupa b
	where	a.nr_seq_lote_roupa	= b.nr_sequencia
	and	a.nr_sequencia	= nr_seq_rop_roupa_p;

	select	nvl(max(obter_valor_param_usuario(1301, 1, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'N'),
		nvl(max(obter_valor_param_usuario(1301, 86, obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_p)), 'N')
	into	ie_local_dif_lote_w,
		ie_consiste_qtde_w
	from	dual;


	if	(dt_liberacao_w is null) then
		--ds_erro_w := substr(ds_erro_w || '* Esta pe�a ' || cd_material_w || ' n�o pode ser utilizada pois o lote da roupa n�o est� liberada - C�d.Barras: ' || cd_barra_roupa_w || chr(13) || chr(10),1,255);
		ds_erro_w := substr(ds_erro_w || wheb_mensagem_pck.get_texto(312273, 'CD_MATERIAL=' || cd_material_w || ';' || 'CD_BARRAS_ROUPA=' || cd_barra_roupa_w) || chr(13) || chr(10),1,255);
	end if;

	if	(ie_situacao_w = 'I') then
		--ds_erro_w := substr(ds_erro_w || '* Esta pe�a ' || cd_material_w || ' n�o pode ser utilizada pois est� inativa - C�d.Barras: ' || cd_barra_roupa_w || chr(13) || chr(10),1,255);
		ds_erro_w := substr(ds_erro_w || wheb_mensagem_pck.get_texto(312277, 'CD_MATERIAL=' || cd_material_w || ';' || 'CD_BARRAS_ROUPA=' || cd_barra_roupa_w) || chr(13) || chr(10),1,255);
	end if;

	if	(dt_baixa_w is not null) then
		--ds_erro_w := substr(ds_erro_w || '* Esta pe�a ' || cd_material_w || ' n�o pode ser utilizada pois est� baixada - C�d.Barras: ' || cd_barra_roupa_w || chr(13) || chr(10),1,255);
		ds_erro_w := substr(ds_erro_w || wheb_mensagem_pck.get_texto(312279, 'CD_MATERIAL=' || cd_material_w || ';' || 'CD_BARRAS_ROUPA=' || cd_barra_roupa_w) || chr(13) || chr(10),1,255);
	end if;

	if	(ie_consiste_qtde_w = 'N') and
		(qt_roupa_w > 0) and
		(ie_operacao_w <> 'Reg') and
		(ie_operacao_w <> 'NF') and
		(ie_operacao_w <> 'AI') and
		(ie_operacao_w <> 'AM') then

		-- somado qt_altera_p para simular a cria��o do movimento da alteracao de quantidade roupa
		if	(qt_altera_p > 0) and
			(qt_altera_p > qt_roupa_w) then
			--ds_erro_w := substr(ds_erro_w || '* A quantidade movimentada da pe�a ' || cd_material_w || ' � maior que a quantidade definida no cadastro - C�d.Barras: ' || cd_barra_roupa_w || chr(13) || chr(10),1,255);
			ds_erro_w := substr(ds_erro_w || wheb_mensagem_pck.get_texto(312281, 'CD_MATERIAL=' || cd_material_w || ';' || 'CD_BARRAS_ROUPA=' || cd_barra_roupa_w) || chr(13) || chr(10),1,255);
		end if;
	end if;

	if	(nr_seq_local_atual_w is not null) then
		if	(ie_local_dif_lote_w = 'N') and
			(cd_local_p <> nr_seq_local_atual_w) and
			(ie_operacao_w <> 'NF') and
			(ie_operacao_w <> 'AI') and
			(ie_operacao_w <> 'AM') then
			--ds_erro_w := substr(ds_erro_w || '* O local atual: ' || ds_local_atual_w || ' da pe�a ' || cd_material_w || ' � diferente do local da movimenta��o - C�d.Barras: ' || cd_barra_roupa_w || chr(13) || chr(10),1,255);
			ds_erro_w := substr(ds_erro_w || wheb_mensagem_pck.get_texto(312282, 'DS_LOCAL_ATUAL=' || ds_local_atual_w || ';' || 'CD_MATERIAL=' || cd_material_w || ';' || 'CD_BARRA_ROUPA=' || cd_barra_roupa_w) || chr(13) || chr(10),1,255);
		end if;

	elsif	(nr_seq_local_atual_w is null) and
		(ie_operacao_w <> 'Reg') and
		(ie_operacao_w <> 'NF') and
		(ie_operacao_w <> 'AI') and
		(ie_operacao_w <> 'AM') then
		--ds_erro_w := substr(ds_erro_w || '* A pe�a ' || cd_material_w || ' n�o possui local atual, deve-se registrar a movimenta��o - C�d.Barras: ' || cd_barra_roupa_w || chr(13) || chr(10),1,255);
		ds_erro_w := substr(ds_erro_w || wheb_mensagem_pck.get_texto(312283, 'CD_MATERIAL=' || cd_material_w || ';' || 'CD_BARRAS_ROUPA=' || cd_barra_roupa_w) || chr(13) || chr(10),1,255);

	end if;

	if	(ie_operacao_w = 'Reg') then
		select	count(*)
		into	qt_existe_w
		from	rop_lote_movto a,
			rop_movto_roupa b
		where	a.nr_sequencia = b.nr_seq_lote
		and	a.cd_Estabelecimento = cd_estabelecimento_p
		and	a.dt_liberacao is not null
		and	substr(rop_obter_dados_operacao(a.nr_seq_operacao, 'T'),1,15) = 'Reg'
		and	a.nr_seq_local = cd_local_p
		and	b.nr_seq_roupa = nr_seq_roupa_w;

		if	(qt_existe_w > 0) then
			--ds_erro_w := substr(ds_erro_w || '* A pe�a ' || cd_material_w || ' j� foi registrada neste local - C�d.Barras: ' || cd_barra_roupa_w || chr(13) || chr(10),1,255);
			ds_erro_w := substr(ds_erro_w || wheb_mensagem_pck.get_texto(312285, 'CD_MATERIAL=' || cd_material_w || ';' || 'CD_BARRAS_ROUPA=' || cd_barra_roupa_w) || chr(13) || chr(10),1,255);
		end if;

	end if;

	if	(cd_operacao_w > 0) then

		select	nvl(ie_entrada_saida,'N')
		into	ie_entrada_saida_w
		from	rop_operacao
		where	nr_sequencia = cd_operacao_w;

		if	(ie_entrada_saida_w = 'S') then

			select	rop_obter_saldo_roupa(nr_seq_rop_roupa_p, cd_local_p, trunc(sysdate,'mm'))
			into	qt_saldo_w
			from	dual;

			if	(qt_altera_p > qt_saldo_w) then
				ds_local_baixa_w := substr(rop_obter_dados_local(cd_local_p, 'D'),1,80);
				--ds_erro_w = substr(ds_erro_w || '* A pe�a ' || cd_material_w || ' - ' || cd_barra_roupa_w || ' - ' || nr_seq_roupa || ' n�o tem saldo suficiente no local ' || ds_local_baixa_w || ' para atender essa opera��o.' || chr(13) || chr(10),1,255)
				ds_erro_w := substr(ds_erro_w || wheb_mensagem_pck.get_texto(334721, 'CD_MATERIAL_P='|| cd_material_w || ' - ' || cd_barra_roupa_w || ' - ' || nr_seq_rop_roupa_p || ';DS_LOCAL_P='|| ds_local_baixa_w) || chr(13) || chr(10),1,255);

			end if;

		end if;

	end if;

end if;

ds_erro := ds_erro_w;

end rop_consistir_altera_qt_roupa;
/