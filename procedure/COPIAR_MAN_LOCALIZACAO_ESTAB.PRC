create or replace
procedure copiar_man_localizacao_estab(	cd_estab_origem_p		number,
					cd_estab_destino_p	number,
					nm_usuario_p		Varchar2) is 
					
nr_seq_local_w				number(10,0);
cd_empresa_w				number(04,0);

cursor c01 is
select	a.nr_sequencia,
	a.ds_localizacao,
	a.cd_setor,
	a.ie_terceiro,
	a.ie_calcula_valor,
	a.ie_situacao, 
	a.dt_atualizacao_nrec,
	a.nm_usuario_nrec,
	a.ie_aviso_os_prod, 
	a.ie_gerar_ordem_prev,
	a.cd_cnpj,
	a.qt_aviso, 
	a.ie_webservice,
	a.nr_seq_apres,
	a.ie_email,
	a.qt_metragem
from	man_localizacao a
where	a.cd_estabelecimento	= cd_estab_origem_p;

vet01	C01%RowType;

begin
select	max(cd_empresa)
into	cd_empresa_w
from	estabelecimento
where	cd_estabelecimento = cd_estab_destino_p;

open C01;
loop
fetch C01 into	
	vet01;
exit when C01%notfound;
	begin
	
	select	man_localizacao_seq.nextval
	into	nr_seq_local_w
	from	dual;
	
	insert into man_localizacao ( 
		nr_sequencia,
		ds_localizacao,
		cd_estabelecimento, 
		cd_setor,
		dt_atualizacao,
		nm_usuario, 
		ie_terceiro,
		ie_calcula_valor,
		ie_situacao, 
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_aviso_os_prod, 
		ie_gerar_ordem_prev,
		cd_cnpj,
		qt_aviso, 
		ie_webservice,
		nr_seq_apres,
		ie_email,
		cd_empresa,
		qt_metragem)
	values( nr_seq_local_w,
		vet01.ds_localizacao,
		cd_estab_destino_p,
		vet01.cd_setor,
		sysdate,
		nm_usuario_p, 
		vet01.ie_terceiro,
		vet01.ie_calcula_valor,
		vet01.ie_situacao, 
		sysdate,
		nm_usuario_p,
		vet01.ie_aviso_os_prod, 
		vet01.ie_gerar_ordem_prev,
		vet01.cd_cnpj,
		vet01.qt_aviso, 
		vet01.ie_webservice,
		vet01.nr_seq_apres,
		vet01.ie_email,
		cd_empresa_w,
		vet01.qt_metragem);
	
	insert into man_equip_local_adic(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_local,
		nr_seq_equipamento)
	select	man_equip_local_adic_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_local_w,
		nr_seq_equipamento
	from	man_equip_local_adic
	where	nr_seq_local	= vet01.nr_sequencia;
	
	insert into MAN_LOCALIZACAO_PJ(
		nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		nr_seq_localizacao,
		cd_cnpj)
	select	man_localizacao_pj_seq.nextval,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		sysdate,
		nr_seq_local_w,
		cd_cnpj
	from	man_localizacao_pj
	where	nr_seq_localizacao	= vet01.nr_sequencia;
	
	end;
end loop;
close C01;

commit;

end copiar_man_localizacao_estab;
/