create or replace
procedure inserir_protocolo_fechamento(	
				nm_usuario_p		varchar2,
				nr_protocolo_p		varchar2,
				nr_seq_protocolo_p		number,
				dt_mesano_referencia_p	date,
				nr_interno_conta_p		number
				) is 

begin

if	(nr_interno_conta_p is not null) then
	begin
	if	(dt_mesano_referencia_p is not null) then
		begin
		update	conta_paciente
		set	dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			nr_protocolo		= nr_protocolo_p,
			nr_seq_protocolo		= nr_seq_protocolo_p,
			dt_mesano_referencia	= dt_mesano_referencia_p
		where	nr_interno_conta		= nr_interno_conta_p;
		end;
	else
		begin
		update	conta_paciente
		set	dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			nr_protocolo		= nr_protocolo_p,
			nr_seq_protocolo		= nr_seq_protocolo_p
		where	nr_interno_conta		= nr_interno_conta_p;
		end;
	end if;
	commit;
	end;
end if;

end inserir_protocolo_fechamento;
/