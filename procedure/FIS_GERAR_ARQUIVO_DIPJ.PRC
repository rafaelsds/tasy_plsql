create or replace
procedure fis_gerar_arquivo_dipj(
			nr_seq_controle_p	number,
			nm_usuario_p		Varchar2) is 

			
dt_ref_final_w			date;
dt_ref_inicial_w			date;	
dt_ano_w				date;

qt_contador_w			number(10);
nr_seq_regra_dipj_w		number(10);
cd_estabelecimento_w		number(5);
cd_empresa_w			number(4);
qt_linha_w			number(10) := 0;
nr_sequencia_w			number(10);
qt_linhas_w			number(10);
qt_total_w			number(10);
ds_arquivo_w			varchar2(4000);
ds_arquivo_ww			varchar2(4000);
ie_consolida_empresa_w		varchar2(15);
cd_registro_w			varchar2(15);
sep_w				varchar2(1)	:= '';

cursor c01 is
select	cd_registro
from	fis_regra_dipj_registro
where	nr_seq_regra_dipj	=	nr_seq_regra_dipj_w
and	ie_gerar		=	'S'
order by nr_sequencia;
		
begin

select	max(dt_inicial),
	max(dt_final),
	max(nr_seq_regra_dipj),
	max(cd_estabelecimento)
into	dt_ref_inicial_w,
	dt_ref_final_w,
	nr_seq_regra_dipj_w,
	cd_estabelecimento_w
from	fis_controle_dipj
where	nr_sequencia	=	nr_seq_controle_p;

select	nvl(max(ie_consolida_empresa),'N'),
	max(dt_ano)
into	ie_consolida_empresa_w,
	dt_ano_w	
from	fis_regra_dipj
where	nr_sequencia 	=	nr_seq_regra_dipj_w;

select	count(*) 
into	qt_total_w
from	fis_regra_dipj_registro
where	nr_seq_regra_dipj	= nr_seq_regra_dipj_w;

select	cd_empresa
into	cd_empresa_w
from	fis_controle_dipj
where	nr_sequencia	=	nr_seq_controle_p;

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	fis_dipj_registro;

delete	fis_dipj_registro
where	nr_seq_controle_dipj	=	nr_seq_controle_p;

qt_contador_w	:= 0;
gravar_processo_longo('Carregando informações' ,'FIS_GERAR_ARQUIVO_DIPJ',qt_contador_w);
open c01;
loop
fetch c01 into	
	cd_registro_w;
exit when c01%notfound;
	begin
	nr_sequencia_w	:=	nr_sequencia_w;
	qt_linha_w	:=	qt_linha_w;
	qt_contador_w 	:= 	qt_contador_w + 1;
	gravar_processo_longo('Carregando Registro: ' || cd_registro_w || ' ('|| qt_contador_w ||'/' ||qt_total_w ||')','FIS_GERAR_ARQUIVO_DIPJ',qt_contador_w);
	if	(cd_registro_w = 'HEA') then
		fis_gerar_reg_header_dipj(nr_seq_controle_p,nm_usuario_p,cd_estabelecimento_w,dt_ref_inicial_w,dt_ref_final_w,cd_empresa_w,qt_linha_w,nr_sequencia_w);
	elsif	(cd_registro_w = 'R01') then
		FIS_GERAR_REG_R01_DIPJ(nr_seq_controle_p,nm_usuario_p,cd_estabelecimento_w,dt_ref_inicial_w,dt_ref_final_w,cd_empresa_w,qt_linha_w,nr_sequencia_w);
	elsif	(cd_registro_w = 'R02') then
		FIS_GERAR_REG_R02_DIPJ(nr_seq_controle_p,nm_usuario_p,cd_estabelecimento_w,dt_ref_inicial_w,dt_ref_final_w,cd_empresa_w,qt_linha_w,nr_sequencia_w);
	elsif	(cd_registro_w = 'R03') then
		FIS_GERAR_REG_R03_DIPJ(nr_seq_controle_p,nm_usuario_p,cd_estabelecimento_w,dt_ref_inicial_w,dt_ref_final_w,cd_empresa_w,qt_linha_w,nr_sequencia_w);
		
	elsif	(cd_registro_w = 'R57') then
		FIS_GERAR_REG_R57_DIPJ(nr_seq_controle_p,nm_usuario_p,cd_estabelecimento_w,dt_ref_inicial_w,dt_ref_final_w,cd_empresa_w,qt_linha_w,nr_sequencia_w);
	elsif	(cd_registro_w = 'T9') then
		fis_gerar_reg_trailler_dipj(nr_seq_controle_p,nm_usuario_p,cd_estabelecimento_w,dt_ref_inicial_w,dt_ref_final_w,cd_empresa_w,qt_linha_w,nr_sequencia_w);
			
	end if;	
	end;
end loop;
close c01;





update	fis_controle_dipj
set	dt_geracao	= sysdate,
	nm_usuario_ger	= nm_usuario_p
where	nr_sequencia	= nr_seq_controle_p;

commit;

end fis_gerar_arquivo_dipj;
/