create or replace
procedure sus_atualiza_receita_sh (	nr_interno_conta_p	number,
					nm_usuario_p	varchar2) is

vl_tot_original_w	number(15,2);
vl_tot_preco_w		number(15,2);
nr_sequencia_w		number(10);
vl_preco_w		number(15,2);
tx_item_w		number(10,4);
vl_receita_w		number(15,2);
vl_tot_receita_w	number(15,2) := 0;
qt_itens_w		number(10,0) := 0;
qt_itens_cursor_w	number(10,0) := 0;

Cursor C01 is
select	a.nr_sequencia,
	a.vl_preco
from	w_conta_sus_sh a
where	a.nr_interno_conta	= nr_interno_conta_p
and	a.vl_preco		<> 0;

type 		fetch_array is table of c01%rowtype;
s_array 	fetch_array;
i		integer := 1;
type vetor is table of fetch_array index by binary_integer;
vetor_c01_w			vetor;

begin

select	sum(vl_original)
into	vl_tot_original_w
from	w_conta_sus_sh
where	nr_interno_conta 	= nr_interno_conta_p
and	nvl(ie_origem_proced,0)	= 7;

select	sum(vl_preco)
into	vl_tot_preco_w
from	w_conta_sus_sh
where	nr_interno_conta 	= nr_interno_conta_p;

select  count(*)
into	qt_itens_cursor_w
from	w_conta_sus_sh a
where	a.nr_interno_conta	= nr_interno_conta_p
and	a.vl_preco		<> 0;

open c01;
loop
fetch c01 bulk collect into s_array limit 100000;
	vetor_c01_w(i) := s_array;
	i := i + 1;
exit when c01%notfound;
end loop;
close c01;

for i in 1..vetor_c01_w.count loop
	begin
	s_array := vetor_c01_w(i);
	for z in 1..s_array.count loop
		begin
		
		nr_sequencia_w		:= s_array(z).nr_sequencia;
		vl_preco_w              := s_array(z).vl_preco;
		
		tx_item_w		:= round(dividir_sem_round(vl_preco_w * 100,vl_tot_preco_w),4);
	
		/* INICIO - OS 333814 */
	
		qt_itens_w := qt_itens_w + 1;
		
		if	(qt_itens_w = qt_itens_cursor_w) then
			vl_receita_w	:= (vl_tot_original_w - vl_tot_receita_w);
		else
			vl_receita_w	:= dividir(tx_item_w * vl_tot_original_w,100);				
		end if;
		
		vl_tot_receita_w := vl_tot_receita_w + vl_receita_w;
		
		/*FIM*/
	
		update w_conta_sus_sh
		set	vl_receita 	= vl_receita_w,
			tx_rateio		= tx_item_w
		where	nr_sequencia	= nr_sequencia_w;
			
		end;
	end loop;
	end;
end loop;

end sus_atualiza_receita_sh;
/
