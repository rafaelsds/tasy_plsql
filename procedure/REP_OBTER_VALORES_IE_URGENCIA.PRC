create or replace
procedure rep_obter_valores_ie_urgencia(
		cd_intervalo_p			varchar2,
		dt_prescr_p			date, 
		dt_proced_p			date,
		cd_setor_p			number,
		nr_prescricao_p			number,
		ie_data_p			varchar2,
		ie_agora_p		out	varchar2,
		cd_intervalo_agora_p	out	varchar2,
		ie_data_prev_exec_p	out	date) is 

ie_agora_w		varchar2(1) := 'N';
cd_intervalo_agora_w	varchar2(7) := null;
ie_data_prev_exec_w	date := null;

begin

if	(cd_intervalo_p is not null) then
	begin
	select	nvl(max('S'),'N')
	into	ie_agora_w
	from	intervalo_prescricao
	where	ie_agora = 'S'
	and	cd_intervalo = cd_intervalo_p;
	end;
end if;
	
if	(ie_agora_w = 'N') then
	begin
	select	max(cd_intervalo)
	into	cd_intervalo_agora_w
	from	intervalo_prescricao
	where	ie_agora = 'S'
	and	ie_situacao = 'A';
	end;
end if;

if	(dt_prescr_p is not null) and 
	(dt_proced_p is not null) and 
	(cd_setor_p is not null) and 
	(nr_prescricao_p is not null) and
	(ie_data_p is not null) then
	begin 
	select	obter_data_prev_exec(dt_prescr_p,dt_proced_p,cd_setor_p,nr_prescricao_p, ie_data_p)
	into	ie_data_prev_exec_w
	from	dual;
	end;
end if;

	ie_agora_p		:= ie_agora_w;
	cd_intervalo_agora_p	:= cd_intervalo_agora_w;
	ie_data_prev_exec_p	:= ie_data_prev_exec_w;

end rep_obter_valores_ie_urgencia;
/