create or replace
procedure hd_nova_dialise_hd_prescr(
			cd_pessoa_fisica_p		Varchar2,
			nr_prescricao_p		Number,
			nm_usuario_p		Varchar2,
			cd_estabelecimento_p	Number) is 
			
ie_possui_pac_w		Varchar2(1);
nr_seq_unidade_w		Number(10);
ie_cateter_w		Varchar2(1);
nr_seq_tecnica_w		Number(10);		

begin

if (cd_pessoa_fisica_p is not null) then

	begin
	
	select 	decode(count(*),0,'N','S') ie_possui_pac
	into	ie_possui_pac_w
	from	hd_pac_renal_cronico
	where	cd_pessoa_fisica = cd_pessoa_fisica_p;
	
	end;
	
end if;	
	
if (ie_possui_pac_w = 'N') then	

	begin
	
	select	max(nr_seq_unidade)
	into	nr_seq_unidade_w
	from	hd_parametro
	where	cd_estabelecimento = cd_estabelecimento_p;
	
	insert into hd_pac_renal_cronico(
			cd_estabelecimento,	
			cd_pessoa_fisica,
			dt_atualizacao,
			ie_diabetico,
			nm_usuario,
			nr_seq_unid_dialise)
		values (
			cd_estabelecimento_p,
			cd_pessoa_fisica_p,
			sysdate,
			'N',
			nm_usuario_p,
			nr_seq_unidade_w);
			
	end;		
			
end if;			
			
select	max(ie_cateter)
into	ie_cateter_w
from	hd_prescricao
where	nr_prescricao = nr_prescricao_p;

if (ie_cateter_w is not null) then

	begin
	
	select	max(nr_sequencia)
	into	nr_seq_tecnica_w
	from	hd_tecnica_acesso
	where	ie_acesso_prescricao = ie_cateter_w;
	
	if (nr_seq_tecnica_w is not null) then

		insert 	into hd_acesso(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				cd_pessoa_fisica,
				nr_seq_tecnica)
		values 	(
				hd_acesso_seq.nextval,
				sysdate,
				nm_usuario_p,
				cd_pessoa_fisica_p,
				nr_seq_tecnica_w);
	
	end if;
	
	end;

end if;
	
commit;

end hd_nova_dialise_hd_prescr;
/