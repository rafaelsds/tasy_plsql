create or replace
procedure gpi_atualizar_real_ativ(	nr_seq_ordem_serv_p	number,
				nr_seq_etapa_gpi_p	number,
				nm_usuario_p		Varchar2) is

qt_hora_real_w			number(15,2)	:= 0;
qt_total_minuto_w			number(10);
nr_seq_proj_gpi_w			number(10);

begin

select	max(b.nr_seq_projeto)
into	nr_seq_proj_gpi_w
from	gpi_cronograma b,
	gpi_cron_etapa a
where	a.nr_seq_cronograma	= b.nr_sequencia
and	a.nr_sequencia		= nr_seq_etapa_gpi_p;

select	nvl(sum(a.qt_minuto),0)
into	qt_total_minuto_w
from	man_ordem_serv_ativ a,
	man_ordem_servico b
where	a.nr_seq_ordem_serv	= b.nr_sequencia
and	b.nr_seq_etapa_gpi		= nr_seq_etapa_gpi_p
and	b.nr_seq_proj_gpi		= nr_seq_proj_gpi_w
and	nvl(a.qt_minuto,0)	<> 0;

qt_hora_real_w	:= dividir(qt_total_minuto_w,60);

update	gpi_cron_etapa
set	qt_hora_real	= qt_hora_real_w,
	nm_usuario	= nm_usuario_p,
	dt_atualizacao	= sysdate
where	nr_sequencia	= nr_seq_etapa_gpi_p;

/*Aten��o - recursiva*/
gpi_atualizar_etapa_superior(nr_seq_etapa_gpi_p, nm_usuario_p);

commit;

end gpi_atualizar_real_ativ;
/