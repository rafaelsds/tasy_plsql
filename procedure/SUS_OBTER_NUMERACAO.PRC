create or replace
procedure Sus_Obter_Numeracao(
			ie_tipo_numeracao_p		number,
			nr_numeracao_p			out number) is 
nr_sequencia_w	number(10);
nr_retorno_w	number(13);
begin

select min(b.nr_sequencia)
into	nr_sequencia_w
from	sus_lote_numeracao a,
	sus_lote_numeracao_item b
where	a.nr_sequencia		= b.nr_seq_lote_numeracao
and	ie_tipo_numeracao	= ie_tipo_numeracao_p
and	ie_utilizado		= 'N'
and	a.ie_situacao		= 'A';

if	(nr_sequencia_w	is not null) then
	select	nr_numeracao_sus
	into	nr_retorno_w
	from	sus_lote_numeracao_item
	where	nr_sequencia	= nr_sequencia_w;
	
	update	sus_lote_numeracao_item
	set	ie_utilizado	= 'S'
	where	nr_sequencia	= nr_sequencia_w;
end if;

nr_numeracao_p		:= nr_retorno_w;

end Sus_Obter_Numeracao;
/
