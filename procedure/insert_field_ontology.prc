create or replace procedure insert_field_ontology(NM_TABELA_P varchar2, NM_ATRIBUTO_P varchar2) is
nm_usuario_w                                      ONTOLOGIA_TABELA_ATRIBUTO.nm_usuario%type;
Cd_Exp_Desc_w                                     ONTOLOGIA_TABELA_ATRIBUTO.Cd_Exp_Desc%type;
Cd_Exp_Label_Longo_w                              ONTOLOGIA_TABELA_ATRIBUTO.Cd_Exp_Label_Longo%type;
Cd_Exp_Valores_w                                  ONTOLOGIA_TABELA_ATRIBUTO.Cd_Exp_Valores%type;
Cd_Dominio_w                                      ONTOLOGIA_TABELA_ATRIBUTO.Cd_Dominio%type;
Cd_Condicao_w                                     ONTOLOGIA_TABELA_ATRIBUTO.Cd_Condicao%type;
Ie_Filtro_w                                       ONTOLOGIA_TABELA_ATRIBUTO.Ie_Visible%type;
Cd_Valor_Default_w                                ONTOLOGIA_TABELA_ATRIBUTO.Cd_Valor_Default%type;
ie_visible_w                                      ONTOLOGIA_TABELA_ATRIBUTO.Ie_Visible%type;


cursor c01 is
select Cd_Exp_Desc,
       Cd_Exp_Label_Longo,
       Cd_Exp_Valores,
       Cd_Dominio,
	   nvl(cd_exp_label,cd_exp_label_grid) Cd_Exp_label
  from TABELA_ATRIBUTO
 where nm_tabela = NM_TABELA_P
   and nm_atributo = NM_ATRIBUTO_P;

begin
   nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
   
   for c01_w in c01 loop
   
     Cd_Exp_Label_Longo_w  := c01_w.cd_exp_label;
	 Cd_Exp_Desc_w         := c01_w.cd_exp_desc;    
     Cd_Exp_Valores_w      := c01_w.cd_exp_valores;
     Cd_Dominio_w          := c01_w.cd_dominio;
     Cd_Condicao_w         := null;
     Ie_Filtro_w           := null;
     Cd_Valor_Default_w    := null;
     ie_visible_w          := null;
     
   
    insert into ONTOLOGIA_TABELA_ATRIBUTO
      (nr_sequencia,
       Nm_Atributo,
       nm_tabela,
       Cd_Exp_Desc,
       Cd_Exp_Label_Longo,
       Nm_Usuario_Nrec,
       Nm_Usuario,
       Dt_Atualizacao,
       Dt_Atualizacao_Nrec,
       Cd_Exp_Valores,
       Cd_Dominio,
       Cd_Condicao,
       Ie_Filtro,
       Cd_Valor_Default,
       ie_visible)
    values
      ((select nvl(max(nr_sequencia),0) + 1 from ONTOLOGIA_TABELA_ATRIBUTO),
       NM_ATRIBUTO_P,
       NM_TABELA_P,
       Cd_Exp_Label_Longo_w,
	   Cd_Exp_Desc_w,
       nm_usuario_w,
       nm_usuario_w,
       sysdate,
       sysdate,
       Cd_Exp_Valores_w,
       Cd_Dominio_w,
       Cd_Condicao_w,
       Ie_Filtro_w,
       Cd_Valor_Default_w,
       ie_visible_w);

     
     end loop;

end insert_field_ontology;
/
