create or replace
procedure ATUALIZAR_TIT_RECEBER_CLASSIF
		(nr_titulo_p	in	number,
		ie_commit_p	in	varchar2,
		nm_usuario_p	in	varchar2) is

vl_original_tit_w	number(15,2);
vl_com_alteracao_w	number(15,2);

vl_total_classif_w	number(15,2);
vl_diferenca_w		number(15,2);

nr_seq_classif_w	number(10);
vl_original_classif_w	number(15,2);
vl_classificacao_w	number(15,2);
vl_desconto_w		number(15,2);
vl_recebido_w		number(15,2);
vl_desconto_classif_w	number(15,2);
ie_situacao_w		varchar2(255);
vl_total_desconto_w	titulo_receber_classif.vl_desconto%type;


Cursor c01 is
select	nr_sequencia,
	nvl(vl_original,0),
	vl_classificacao
from	titulo_receber_classif
where	nr_titulo	= nr_titulo_p;

begin

select	vl_titulo,
	to_number(obter_dados_titulo_receber(nr_titulo_p,'V')),
	ie_situacao
into	vl_original_tit_w,
	vl_com_alteracao_w,
	ie_situacao_w
from	titulo_receber
where	nr_titulo	= nr_titulo_p;

select	nvl(sum(vl_descontos),0),
	nvl(sum(vl_recebido),0)
into	vl_desconto_w,
	vl_recebido_w
from	titulo_receber_liq
where	nr_titulo		= nr_titulo_p
and	nvl(ie_lib_caixa, 'S')	= 'S';

open c01;
loop
fetch c01 into
	nr_seq_classif_w,
	vl_original_classif_w,
	vl_classificacao_w;
exit when c01%notfound;

	if	(vl_original_classif_w	= 0) then
		vl_original_classif_w	:= vl_classificacao_w;
	end if;

	vl_classificacao_w	:= dividir_sem_round(vl_com_alteracao_w,vl_original_tit_w) * vl_original_classif_w;

	if	(vl_desconto_w <> 0) then
		/*13/10/16 AAMFIRMO OS 1253448  - Fiz igual foi feito na OS 64011 para a procedure ATUALIZAR_TIT_PAGAR_CLASSIF. Estava vl_baixa_w no lugar de vl_com_alteracao_w, onde gerava vl de desconto errado*/
		vl_desconto_classif_w	:= vl_classificacao_w * dividir_sem_round(vl_desconto_w,vl_com_alteracao_w);
	end if;

	update	titulo_receber_classif
	set	vl_original		= vl_original_classif_w,
		vl_classificacao	= vl_classificacao_w,
		vl_desconto		= vl_desconto_classif_w,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_titulo		= nr_titulo_p
	and	nr_sequencia		= nr_seq_classif_w;
end loop;
close c01;

/* Verificar se deu diferenca de centavos */
	
select	sum(vl_classificacao),
		sum(vl_desconto)
into	vl_total_classif_w,
		vl_total_desconto_w
from	titulo_receber_classif
where	nr_titulo	= nr_titulo_p;

if	(vl_total_classif_w > 0) then

	if	(vl_com_alteracao_w <> vl_total_classif_w) then

		vl_diferenca_w	:= vl_com_alteracao_w - vl_total_classif_w;
	
		update	titulo_receber_classif
		set	vl_classificacao	= vl_classificacao + vl_diferenca_w
		where	nr_titulo		= nr_titulo_p
		and	nr_sequencia		= 1;	
	end if;
end if;

/*Verificar se deu diferenša nos descontos e ajustar.*/
if (nvl(vl_total_desconto_w,0) > 0) then

	vl_diferenca_w := 0;
	
	/*Buscar a sequencia de classif que possui maior valor de descontos para sofrer o arredondamento se necessario. Isso pois se pegar a sequencia 1, e o valor de ajuste for maior que o valor do desconto existente, pode ficar negativo*/
	select	max(a.nr_sequencia)
	into	nr_seq_classif_w
	from	titulo_receber_classif a
	where	a.vl_desconto = (select max(x.vl_desconto) from titulo_receber_classif x where x.nr_titulo = nr_titulo_p)
	and		a.nr_titulo    = nr_titulo_p;
	
	if	(vl_desconto_w > vl_total_desconto_w) then
	
		vl_diferenca_w := nvl(vl_desconto_w,0) - nvl(vl_total_desconto_w,0);
		
		update	titulo_receber_classif
		set		vl_desconto			= nvl(vl_desconto,0) + nvl(vl_diferenca_w,0)
		where	nr_titulo			= nr_titulo_p
		and		nr_sequencia		= nr_seq_classif_w;

	elsif (vl_desconto_w < vl_total_desconto_w) then
	
		vl_diferenca_w := nvl(vl_total_desconto_w,0) - nvl(vl_desconto_w,0);
		
		update	titulo_receber_classif
		set		vl_desconto			= nvl(vl_desconto,0) - nvl(vl_diferenca_w,0)
		where	nr_titulo			= nr_titulo_p
		and		nr_sequencia		= nr_seq_classif_w;
	
	end if;
			
end if;

if	(ie_commit_p = 'S') then
	commit;
end if;

end ATUALIZAR_TIT_RECEBER_CLASSIF;
/