create or replace
procedure consiste_mat_orc(	nr_seq_orcamento_p	number,
				cd_material_p		number,
				qt_material_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		Varchar2,
				ie_autorizacao_p	out varchar2,
				ie_regra_p		out varchar2 ) is 
				
nr_atendimento_w		number(10);				
dt_orcamento_w		date;
ie_tipo_atendimento_w	number(10);
cd_convenio_w		number(10);
cd_categoria_w		varchar2(20 char);
cd_plano_w		varchar2(10 char);
ie_glosa_w		varchar2(20 char);
ds_retorno_w		varchar2(200 char);
nr_retorno_w		number(15,7);
ie_regra_w		varchar2(10 char);
ie_consiste_glosa_partic_w 	varchar2(10 char);
ie_consiste_regra_plano_w	varchar2(10 char);

begin


Obter_Param_Usuario(106,44,obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_p, ie_consiste_glosa_partic_w);
Obter_Param_Usuario(106,28,obter_perfil_ativo, nm_usuario_p,cd_estabelecimento_p, ie_consiste_regra_plano_w);

select 	nvl(nr_atendimento,0),
	dt_orcamento,
	nvl(ie_tipo_atendimento,0),
	cd_convenio,
	cd_categoria,
	cd_plano
into	nr_atendimento_w,
	dt_orcamento_w,
	ie_tipo_atendimento_w,
	cd_convenio_w,	
	cd_categoria_w,
	cd_plano_w
from	orcamento_paciente
where	nr_sequencia_orcamento  =  nr_seq_orcamento_p;

if	(ie_consiste_glosa_partic_w = 'S') then

	glosa_material(	cd_estabelecimento_p,	-- cd_estabelecimento_p
			nr_atendimento_w,       -- nr_atendimento_p
			dt_orcamento_w,         -- dt_atendimento_p
			cd_material_p,          -- cd_material_p
			qt_material_p,          -- qt_material_p
			0,                      -- cd_tipo_acomodacao_p
			ie_tipo_atendimento_w,  -- ie_tipo_atendimento_p
			0,                      -- cd_setor_atendimento_p
			0,                      -- qt_idade_p
			0,                      -- cd_proc_referencia_p
			0,                      -- ie_origem_proced_p
			0,                      -- nr_sequencia_p
			0,                      -- nr_seq_proc_interno_p
			cd_convenio_w,	        -- cd_convenio_p     	 in
			cd_categoria_w,         -- cd_categoria_p    	 in
			nr_retorno_w,           -- ie_tipo_convenio_p	 out
			ds_retorno_w,           -- ie_classif_convenio_p out
			ds_retorno_w,           -- cd_autorizacao_p    	 out
			nr_retorno_w,           -- nr_seq_autorizacao_p  out
			nr_retorno_w,           -- qt_autorizada_p    	 out
			ds_retorno_w,           -- cd_senha_p    	 out
			ds_retorno_w,           -- nm_responsavel_p    	 out
			ie_glosa_w,             -- ie_glosa_p		 out
			nr_retorno_w,           -- cd_situacao_glosa_p	 out
			nr_retorno_w,           -- pr_glosa_p		 out
			nr_retorno_w,		-- vl_glosa_p		 out
			nr_retorno_w,           -- cd_motivo_exc_conta_p out
			ds_retorno_w,           -- ie_autor_particular_p out
			nr_retorno_w,           -- cd_convenio_glosa_p	 out
			ds_retorno_w,           -- cd_categoria_glosa_p	 out
			nr_retorno_w,           -- nr_seq_regra_ajuste_p out
			nr_seq_orcamento_p);    -- nr_seq_orcamento_p	 in
end if;
 
if	(ie_glosa_w <> 'L') then
	ie_autorizacao_p	:= 'B';
	ie_regra_p		:= 0;
elsif	(nvl(cd_plano_w, '0') <> '0') and
	(ie_consiste_regra_plano_w = 'S' or
	ie_consiste_regra_plano_w = 'M') then
	
	consiste_mat_plano_convenio(	cd_convenio_w,         
					cd_plano_w,                     
					cd_material_p,
					nr_atendimento_w,
					0,
					ds_retorno_w,                   
					ds_retorno_w,
					ie_regra_w,
					nr_retorno_w,                 
					qt_material_p,                  
					dt_orcamento_w,
					null,                
					cd_estabelecimento_p,
					cd_categoria_w,
					ie_tipo_atendimento_w);
					
	if	(ie_regra_w = '1')  or
		(ie_regra_w = '2') then
		ie_autorizacao_p := 'B';
	elsif  (ie_regra_w = '3') then
		ie_autorizacao_p := 'P';
	end if;
	
	ie_regra_p	:= ie_regra_w;	
end if;	

end consiste_mat_orc;
/