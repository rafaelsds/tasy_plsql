create or replace
procedure update_status_qua_nao_conform(	nr_sequencia_p		number,
					ie_status_p		varchar2,
					nm_usuario_p		varchar2) is 

begin

if	(nr_sequencia_p is not null) and
	(ie_status_p is not null) and
	(nm_usuario_p is not null) then
	begin	
	update	qua_nao_conformidade
	set	ie_status = ie_status_p,
		dt_atualizacao = sysdate,
		nm_usuario = nm_usuario_p
	where	nr_sequencia = nr_sequencia_p;
	commit;
	
	end;
end if;

end update_status_qua_nao_conform;
/
