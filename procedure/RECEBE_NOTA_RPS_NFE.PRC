create or replace
procedure recebe_nota_rps_nfe(
			cd_estabelecimento_p	Number,
			nr_nota_fiscal_p	Number,
			cd_serie_nf_p		varchar2,
			cd_cpf_cnpj_prest_p	varchar2,
			dt_emissao_p		varchar2,
			nr_nfe_imp_p		varchar2,
			ds_observacao_p		varchar2,
			nr_seq_lote_p		number,
			ie_situacao_p		varchar2 default '') is

nr_sequencia_w	Number(10);
cd_serie_nr_w	varchar2(5);
dt_emissao_w	date;

begin

/*conversao de formato de data*/
begin

dt_emissao_w	:= dt_emissao_p;

exception when others then
	begin
	dt_emissao_w		:= to_date(dt_emissao_p,'yyyy-mm-dd');
	exception when others then
		begin
		dt_emissao_w	:= to_date(dt_emissao_p,'dd-mm-yyyy');
		exception when others then
			wheb_mensagem_pck.exibir_mensagem_abort(266357,'DT_EMISSAO=' || dt_emissao_p);
			--'N�o foi poss�vel converter a data de emiss�o enviada no arquivo: ' + dt_emissao_p);
		end;
	end;
end;

begin

select	nvl(max(replace(cd_serie_nf_p,' ','')),'X')
into	cd_serie_nr_w
from	dual;

select	nvl(max(nr_sequencia),0)
into	nr_sequencia_w
from	nota_fiscal
where	cd_estabelecimento	= cd_estabelecimento_p
and	nr_nota_fiscal		= to_char(nr_nota_fiscal_p)
and	(cd_serie_nf		= cd_serie_nf_p or cd_serie_nr_w = 'X')
and	cd_cgc_emitente		= cd_cpf_cnpj_prest_p
and	(dt_emissao >= trunc(dt_emissao_w, 'dd') and dt_emissao < trunc(dt_emissao_w + 1, 'dd'))
and	nvl(nr_nfe_imp,0)	<> 	nr_nfe_imp_p;

if	(nr_sequencia_w > 0) then

	if	(ie_situacao_p = 'C') then
		update	nota_fiscal
		set	nr_nfe_imp 		= nr_nfe_imp_p,
			nr_seq_importada	= nr_seq_lote_p,
			ie_status_envio		= ie_situacao_p
		where	nr_sequencia 		= nr_sequencia_w;
	else
		update	nota_fiscal
		set	nr_nfe_imp 		= nr_nfe_imp_p,
			nr_seq_importada	= nr_seq_lote_p
		where	nr_sequencia 		= nr_sequencia_w;
	end if;
	update	nota_fiscal_item
	set	nr_nfe_imp 	= nr_nfe_imp_p
	where	nr_sequencia 	= nr_sequencia_w;
	
	insert into nota_fiscal_hist(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_nota,
		cd_evento,
		ds_historico)
	values(	nota_fiscal_hist_seq.nextval,
		sysdate,
		'Tasy',
		sysdate,
		'Tasy',
		nr_sequencia_w,
		'27',
		--substr('NF-e importada: ' || nr_nfe_imp_p || ' ' || ds_observacao_p,1,255));
		substr(wheb_mensagem_pck.get_texto(311869, 'NR_NFE_IMP=' || nr_nfe_imp_p) || ' ' || ds_observacao_p,1,255));
end if;


exception when others then
	wheb_mensagem_pck.exibir_mensagem_abort(266358,'NR_NOTA_FISCAL=' || nr_nota_fiscal_p || ';' ||
							'CD_ESTABELECIMENTO=' || cd_estabelecimento_p || ';' ||
							'CD_SERIE_NF=' || cd_serie_nf_p || ';' ||
							'DT_EMISSAO=' || dt_emissao_w || ';' ||
							'CD_CPF_CNPJ=' || cd_cpf_cnpj_prest_p || ';' ||
							'NR_NFE_IMP=' || nr_nfe_imp_p);
	--'Erro na atualiza��o da nf: ' 	|| nr_nota_fiscal_p		|| chr(13) || chr(10) ||
	--'Estab: '		|| cd_estabelecimento_p 	|| chr(13) || chr(10) ||
	--'S�rie: ' 		|| cd_serie_nf_p		|| chr(13) || chr(10) ||
	--'Emiss�o: ' 		|| dt_emissao_w		|| chr(13) || chr(10) ||
	--'CNPJ: '		|| cd_cpf_cnpj_prest_p 	|| chr(13) || chr(10) ||
	--'Nr NF-e: '		|| nr_nfe_imp_p);
end;

commit;

end recebe_nota_rps_nfe;
/