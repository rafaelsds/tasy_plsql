create or replace procedure renomear_pasta_report_dar(
			nr_sequencia_p number,
			ie_tipo_p    varchar2,
			ds_titulo_p    varchar2,
			nm_usuario_p  varchar2
  )  	
	is
  
  begin
	  
		if (ie_tipo_p = 'P') then
    
			update tree_report_dar
			set nr_sequencia = nr_sequencia_p, 
			nm_usuario_nrec = wheb_usuario_pck.get_nm_usuario,
			dt_atualizacao_nrec = sysdate,
			ds_titulo = ds_titulo_p
		where nr_sequencia = nr_sequencia_p;
 
		end if;
		
end renomear_pasta_report_dar;
/