create or replace
procedure ece_aprovar_resultado(		nr_prescricao_p		number,
										nr_seq_interno_p	number,
										cd_exame_princ_p	varchar2) is 

nr_seq_prescr_w			prescr_procedimento.nr_sequencia%type;
ie_status_atend_w		prescr_procedimento.ie_status_atend%type;

nr_seq_exame_w			exame_laboratorio.nr_seq_exame%type;

nr_seq_resultado_w		exame_lab_resultado.nr_seq_resultado%type;

nr_seq_material_w		exame_lab_result_item.nr_seq_material%type;
qt_resultado_w			exame_lab_result_item.qt_resultado%type;
pr_resultado_w			exame_lab_result_item.pr_resultado%type;
ds_resultado_w			exame_lab_result_item.ds_resultado%type;

ds_erro_w				varchar2(4000);
										
cursor c02 is
	select	r.nr_seq_exame,
		nvl(r.qt_resultado,0),
		r.pr_resultado,
		r.ds_resultado
	from    exame_lab_result_item r,
			exame_laboratorio e
	where   r.nr_seq_resultado	= nr_seq_resultado_w
	and   	r.nr_seq_prescr		= nr_seq_prescr_w
	and   	((r.qt_resultado <> 0) or (r.pr_resultado <> 0) or (r.ds_resultado is not null))
	and   	r.nr_seq_exame 		= e.nr_seq_exame
	order   by 	r.nr_sequencia,
		e.nr_seq_apresent;
					
begin


begin

select	max(a.nr_sequencia),
		nvl(max(ie_status_atend),0)
into	nr_seq_prescr_w,
		ie_status_atend_w
from  	prescr_procedimento a
where 	a.nr_prescricao = nr_prescricao_p
and		a.nr_seq_interno = nr_seq_interno_p;

select	max(nr_seq_resultado)
into	nr_seq_resultado_w
from	exame_lab_resultado
where	nr_prescricao = nr_prescricao_p;

if		(nr_seq_resultado_w 	is not null) and
		(nr_seq_prescr_w 	is not null) /*and
		(ie_status_atend_w <= 30)*/ then

	select	max(nr_seq_material)
	into	nr_seq_material_w
	from 	exame_lab_result_item
	where 	nr_seq_resultado	= nr_seq_resultado_w
	and 	nr_seq_prescr		= nr_seq_prescr_w
	and 	nr_seq_material 	is not null;
		
	open c02;
	loop
	fetch c02 into	nr_seq_exame_w,
			qt_resultado_w,
			pr_resultado_w,
			ds_resultado_w;
		exit when c02%notfound;

		calcular_valores_exame(nr_seq_resultado_w,
					nr_prescricao_p,
					nr_seq_prescr_w,
					nr_seq_material_w,
					nr_seq_exame_w,
					qt_resultado_w,
					pr_resultado_w,
					ds_resultado_w);


	end loop;
	close c02; 
	
	delete	result_laboratorio
	where	nr_prescricao = nr_prescricao_p
	and		nr_seq_prescricao = nr_seq_prescr_w;

	update	prescr_procedimento
	set		ie_status_atend = 35
	where	nr_prescricao = nr_prescricao_p
	and		nr_sequencia = nr_seq_prescr_w
	and		ie_status_atend < 35;
	
end if;




exception
when others then
	ds_erro_w	:= substr(sqlerrm,1,1000);		
	gravar_log_lab(69999,
			substr('ECE - '||
			obter_desc_expressao(712423)||' '||
			obter_desc_expressao(722599)||' '||nr_seq_resultado_w||' - '||
			obter_desc_expressao(325814)||' '||nr_prescricao_p||' - '||
			'seq_prescricao: '||nr_seq_prescr_w||' - '||
			obter_desc_expressao(326091)||' '||nr_seq_material_w||' - '||
			obter_desc_expressao(621805)||' '||nr_seq_exame_w||' - '||
			'qt_resultado: '||qt_resultado_w||' - '||
			'pr_resultado: '||pr_resultado_w||' - '||
			obter_desc_expressao(504115)||' '||ds_erro_w,1,2000), 'ECE', nr_prescricao_p);
end;

commit;

end ece_aprovar_resultado;
/