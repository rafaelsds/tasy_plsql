create or replace
procedure sus_gerar_conta_proc_aih(	nr_atendimento_p		number,
				nm_usuario_p		Varchar2) is 

nr_sequencia_w			number(10);	
qt_proc_bpa_w			number(10);	
nr_interno_conta_ant_w		number(10);
nr_interno_conta_nova_w		number(10);
cd_convenio_atend_w		number(5);
ie_classificacao_w			varchar2(5);
	
Cursor C01 is
	select	a.nr_sequencia,
		c.ie_classificacao
	from	procedimento c,
		procedimento_paciente a,
		sus_procedimento_registro b
	where	a.cd_procedimento	= b.cd_procedimento
	and	a.ie_origem_proced	= b.ie_origem_proced
	and	a.cd_procedimento	= c.cd_procedimento
	and	a.ie_origem_proced	= c.ie_origem_proced
	and	b.cd_registro in (3,4,5)
	and	a.nr_atendimento 	= nr_atendimento_p;
	
begin

select 	max(cd_convenio)
into	cd_convenio_atend_w
from	atend_categoria_convenio
where	nr_atendimento = nr_atendimento_p;

select	count(*)
into	qt_proc_bpa_w
from	procedimento_paciente a,
	sus_procedimento_registro b
where	a.cd_procedimento	= b.cd_procedimento
and	a.ie_origem_proced	= a.ie_origem_proced
and	b.cd_registro in (1,2,6,7)
and	a.nr_atendimento 	= nr_atendimento_p;

if	(qt_proc_bpa_w > 0) then
	begin
	
	select	max(nr_interno_conta)
	into	nr_interno_conta_ant_w
	from	conta_paciente
	where	nr_atendimento = nr_atendimento_p
	and	ie_status_acerto = 1
	and	ie_cancelamento is null
	and	obter_tipo_convenio(cd_convenio_parametro) = 3;
	
	select	Conta_Paciente_seq.nextval
	into	nr_interno_conta_nova_w
	from	dual;

	begin
	insert into Conta_Paciente(
		nr_atendimento,
		dt_acerto_conta,
		ie_status_acerto,
		dt_periodo_inicial,
		dt_periodo_final,
		dt_atualizacao,
		nm_usuario,
		cd_convenio_parametro,
		nr_protocolo,
		dt_mesano_referencia,
		dt_mesano_contabil,
		cd_convenio_calculo,
		cd_categoria_calculo,
		nr_interno_conta,
		nr_seq_protocolo,
		cd_categoria_parametro,
		cd_estabelecimento)
	select	a.nr_atendimento,
		a.dt_acerto_conta  + (2/86400),
		1, 
		a.dt_periodo_inicial,
		a.dt_periodo_final,
		sysdate,
		nm_usuario_p,
		a.cd_convenio_parametro,
		'0',
		trunc(b.dt_ref_valida,'dd'),
		a.dt_mesano_contabil,
		a.cd_convenio_calculo,
		a.cd_categoria_calculo,
		nr_interno_conta_nova_w,
		null,
		a.cd_categoria_parametro,
		a.cd_estabelecimento
	from	Convenio b,
		conta_paciente a
	where	nr_interno_conta	= nr_interno_conta_ant_w
	and	b.cd_convenio		= a.cd_convenio_parametro;
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(185814);	
	end;	
	
	open C01;
	loop
	fetch C01 into	
		nr_sequencia_w,
		ie_classificacao_w;
	exit when C01%notfound;
		begin
		
		begin
		update	procedimento_paciente
		set	nr_interno_conta = nr_interno_conta_nova_w
		where	nr_sequencia	= nr_sequencia_w;
		exception
		when others then
			null;
		end;
					
		
		if	(ie_classificacao_w = 1) or (ie_classificacao_w = 3) then
			atualiza_preco_procedimento(nr_sequencia_w, cd_convenio_atend_w, nm_usuario_p);
		elsif	(ie_classificacao_w = 2) then
			atualiza_preco_servico(nr_sequencia_w, nm_usuario_p);
		end if;
		
		end;
	end loop;
	close C01;
	
	commit;
		
	end;
end if;

end sus_gerar_conta_proc_aih;
/
