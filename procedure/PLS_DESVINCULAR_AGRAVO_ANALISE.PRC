create or replace
procedure pls_desvincular_agravo_analise
			(	nr_seq_analise_agravo_p		number,
				nm_usuario_p			varchar2) is 


begin

delete	from	pls_agravo_parcela
where	nr_seq_analise_agravo	= nr_seq_analise_agravo_p;
	
delete	from	pls_analise_agravo
where	nr_sequencia	= nr_seq_analise_agravo_p;

commit;

end pls_desvincular_agravo_analise;
/
