create or replace
procedure Gerar_Prof_Agenda_servico (	cd_agenda_p		number,
					nr_seq_agenda_p		number,
					nr_seq_turno_p		number,
					dt_agenda_p		date,
					nm_usuario_p		varchar2) is

ie_profissional_w		varchar2(1);
qt_registros_w			number(10,0);
dt_agenda_w			date;

ie_gerar_prof_ordem_w		varchar2(1);
nr_seq_ordem_max_w		number(10,0);
nr_seq_prof_w			number(10,0);

begin

select	substr(obter_se_agenda_prof(cd_agenda_p),1,1)
into	ie_profissional_w
from	dual;

select	nvl(max(ie_gerar_prof_hor),'N')
into	ie_gerar_prof_ordem_w
from	agenda
where	cd_agenda	= cd_agenda_p;

select	count(*) 
into	qt_registros_w
from	agenda_consulta_prof
where	nr_seq_agenda	= nr_seq_agenda_p;

if	(ie_profissional_w = 'S') and
	(qt_registros_w = 0) then

	if	(ie_gerar_prof_ordem_w = 'S') then
		
		select	max(a.nr_seq_ordem)
		into	nr_seq_ordem_max_w
		from	agenda_turno_prof a,
			agenda_consulta_prof b,
			agenda_consulta c
		where	b.cd_pessoa_fisica	= a.cd_pessoa_fisica
		and	b.nr_seq_ordem		= a.nr_seq_ordem
		and	b.nr_seq_agenda		= c.nr_sequencia
		and	a.nr_seq_turno		= nr_seq_turno_p
		and	c.dt_agenda		= dt_agenda_p
		and	c.cd_agenda		= cd_agenda_p;
		
		select	min(nr_sequencia)
		into	nr_seq_prof_w
		from	agenda_turno_prof
		where	nr_seq_turno	= nr_seq_turno_p
		and	nr_seq_ordem 	> nr_seq_ordem_max_w;
				
		insert into agenda_consulta_prof(
			nr_sequencia,
			nr_seq_agenda,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_ordem,
			ie_tipo_profissional,
			cd_pessoa_fisica,
			ie_solicita_retorno
		) 
		select	agenda_consulta_prof_seq.nextval,
			nr_seq_agenda_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_ordem,
			ie_tipo_profissional,
			cd_pessoa_fisica,
			ie_solicita_retorno
		from	agenda_turno_prof
		where	nr_sequencia	= nr_seq_prof_w;
	
	else
		insert into agenda_consulta_prof(
			nr_sequencia,
			nr_seq_agenda,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_ordem,
			ie_tipo_profissional,
			cd_pessoa_fisica,
			ie_solicita_retorno
		) 
		select	agenda_consulta_prof_seq.nextval,
			nr_seq_agenda_p,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_ordem,
			ie_tipo_profissional,
			cd_pessoa_fisica,
			ie_solicita_retorno
		from	agenda_turno_prof
		where	nr_seq_turno	= nr_seq_turno_p;
	
	end if;
end if;

end Gerar_Prof_Agenda_servico;
/