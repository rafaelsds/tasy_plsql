create or replace
procedure fa_inserir_atend_pac(	nr_sequencia_p		number,
				nr_atendimento_p	number) is 

begin
if	(nr_sequencia_p	is not null) and
	(nr_atendimento_p is not null) then

	update	fa_paciente_entrega
	set	nr_atendimento = nr_atendimento_p
	where	nr_sequencia = nr_sequencia_p;

end if;

commit;

end fa_inserir_atend_pac;
/