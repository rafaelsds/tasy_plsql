create or replace
procedure ck_inicio_etapa_grid_painel(
					nr_seq_checkup_etapa_p		number,
					nm_usuario_p			varchar2,
					ds_pergunta_p		out	varchar2,
					ds_mensagem_etapa_p	out	varchar2)is

ds_pergunta_w		varchar2(255) := '';
ds_mensagem_etapa_w	varchar2(255) := '';
		
begin

valida_etapa_requerida (nr_seq_checkup_etapa_p,nm_usuario_p);

select	a.ds_mensagem
into	ds_mensagem_etapa_w
from	etapa_checkup a,
	checkup_etapa b
where	a.nr_sequencia = b.nr_seq_etapa
and	b.nr_sequencia = nr_seq_checkup_etapa_p;

ds_pergunta_w	:= obter_texto_tasy (61014, wheb_usuario_pck.get_nr_seq_idioma);

ds_pergunta_p		:= ds_pergunta_w;
ds_mensagem_etapa_p	:= ds_mensagem_etapa_w;

end ck_inicio_etapa_grid_painel;
/	