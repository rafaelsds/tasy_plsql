create or replace	
procedure baixar_concil_redecard_fin(
			nr_seq_extrato_arq_p	number,
			nm_usuario_p		Varchar2) is 

nr_seq_extrato_w		number(10);
cd_estabelecimento_w		number(4);
nr_seq_conta_banco_w		number(10);
nr_seq_trans_financ_w		number(10);
nr_seq_trans_indevido_cred_w	number(10);
ie_forma_lanc_lote_cartao_w	varchar2(255);
ie_gerar_movto_banco_w		varchar2(1);
vl_conciliado_w			number(15,4);
nr_seq_parcela_w		number(10);
nr_seq_ext_parcela_w		number(10);
nr_seq_grupo_w			number(10);	
dt_prev_pagto_w			date;
nr_seq_extrato_parcela_fin_w	number(10);
nr_seq_extrato_parcela_cred_w	number(10);	
vl_transacao_w			number(15,2);
nr_seq_conta_banco_res_w	number(10);
vl_despesa_w			number(15,2);
qt_movto_fin_concil_w		number(10);
nr_parcela_w			number(10);
nr_seq_movto_trans_w		number(10);
nr_seq_trans_indevido_w		number(10);
nr_seq_tf_indevido_w		number(10);
vl_ajuste_w			number(15,2);
ie_tipo_arquivo_w		varchar2(5);
nr_seq_movto_fin_w		number(10);
nr_seq_parcela_ajuste_w		number(10);
vl_total_parcela_w		number(15,2);
vl_total_original_w		number(15,2);
vl_original_w			number(15,2);
vl_liquido_w			number(15,2);
vl_ajuste_sobra_w		number(15,2);
nr_seq_movto_w			number(10);
qt_ajuste_w			number(10);
nr_seq_baixa_w			movto_cartao_cr_baixa.nr_sequencia%type;

Cursor C04 is -- financeiro
select	distinct
	b.nr_seq_extrato_parcela,
	c.dt_prev_pagto,
	a.nr_parcela,
	a.nr_sequencia,
	a.vl_ajuste
from	extrato_cartao_cr_res c,
	extrato_cartao_cr_movto a,
	ext_cartao_cr_movto_concil b
where	a.nr_seq_extrato_res	= c.nr_sequencia
and	a.nr_seq_extrato_arq 	= nr_seq_extrato_arq_p
and	b.nr_seq_ext_movto	= a.nr_sequencia;

Cursor C05 is -- credito
select	a.nr_seq_extrato_parcela,
	b.vl_conciliado
from	extrato_cartao_cr_arq c,
	extrato_cartao_cr_movto a,
	ext_cartao_cr_movto_concil b
where	b.nr_seq_ext_movto		= a.nr_sequencia
and	b.nr_seq_extrato_parcela	= nr_seq_extrato_parcela_fin_w 
and	a.nr_seq_extrato_arq 		= c.nr_sequencia
and	c.ie_tipo_arquivo 		= 'C';

Cursor C06 is -- movtos tasy
select	a.nr_sequencia
from	movto_cartao_cr_parcela a
where	a.nr_seq_extrato_parcela		= nr_seq_extrato_parcela_cred_w
and	(to_number(obter_numero_parcela_cartao(a.nr_seq_movto,a.nr_sequencia)) = nr_parcela_w or nvl(nr_parcela_w,0) = 0);

Cursor c02 is -- movto indevidos
select	nvl(a.vl_liquido,a.vl_parcela),
	b.dt_prev_pagto,
	b.nr_seq_conta_banco
from	extrato_cartao_cr_res b,
	extrato_cartao_cr_movto a
where	a.nr_seq_extrato_res	= b.nr_sequencia
and	a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p
and	a.ie_pagto_indevido	= 'S';
	
Cursor C03 is -- movto despesas
select	a.vl_liquido,
	a.dt_prev_pagto
from	extrato_cartao_cr_desp a
where	a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p;

/* movtos conciliados diretamente com as parcelas do Tasy */
cursor	c07 is
select	a.nr_sequencia,
	d.dt_prev_pagto,
	b.vl_ajuste,
	b.nr_sequencia
from	movto_cartao_cr e,
	extrato_cartao_cr_res d,
	extrato_cartao_cr_parcela c,
	extrato_cartao_cr_movto b,
	movto_cartao_cr_parcela a
where	a.nr_seq_extrato_parcela	= c.nr_sequencia
and	b.nr_seq_extrato_parcela	= c.nr_sequencia
and	b.nr_seq_extrato_res		= d.nr_sequencia
and	a.nr_seq_movto			= e.nr_sequencia
and	e.dt_cancelamento		is null
and	c.nr_seq_extrato_arq		= nr_seq_extrato_arq_p
and	(nvl(b.nr_parcela,0) = 0 or to_number(obter_numero_parcela_cartao(a.nr_seq_movto,a.nr_sequencia)) = b.nr_parcela);

cursor	c08 is
select	a.nr_sequencia,
	a.nr_seq_extrato_parcela,
	trunc(nvl(b.dt_prev_pagto,sysdate),'dd')
from	extrato_cartao_cr_res b,
	extrato_cartao_cr_movto a
where	a.nr_seq_extrato_res		= b.nr_sequencia
and	nvl(a.ie_pagto_indevido,'N')	<> 'S'
and	a.nr_seq_extrato_arq		= nr_seq_extrato_arq_p;

/* parcelas conciliados diretamente com o arquivo financeiro */
cursor	c09 is
select	b.nr_sequencia
from	movto_cartao_cr_baixa b,
	movto_cartao_cr_parcela a
where	trunc(b.dt_baixa,'dd')		= dt_prev_pagto_w
and	a.nr_sequencia			= b.nr_seq_parcela
and	a.nr_seq_extrato_parcela	= nr_seq_extrato_parcela_fin_w;

/* parcelas conciliadas com o arquivo de cr�dito */
cursor	c10 is
select	distinct
	b.nr_seq_extrato_parcela
from	ext_cartao_cr_movto_concil b,
	extrato_cartao_cr_movto a
where	a.nr_sequencia		= b.nr_seq_ext_movto
and	a.nr_sequencia		= nr_seq_movto_fin_w;

cursor	c11 is
select	distinct
	f.nr_sequencia
from	movto_cartao_cr_baixa f,
	movto_cartao_cr_parcela e,
	extrato_cartao_cr_arq d,
	extrato_cartao_cr_res c,
	extrato_cartao_cr_movto b,
	ext_cartao_cr_movto_concil a
where	trunc(f.dt_baixa,'dd')		= dt_prev_pagto_w
and	e.nr_sequencia			= f.nr_seq_parcela
and	b.nr_seq_extrato_parcela	= e.nr_seq_extrato_parcela
and	d.ie_tipo_arquivo		= 'C'
and	c.nr_seq_extrato_arq		= d.nr_sequencia
and	b.nr_seq_extrato_res		= c.nr_sequencia
and	a.nr_seq_ext_movto		= b.nr_sequencia
and	a.nr_seq_extrato_parcela	= nr_seq_extrato_parcela_cred_w;

begin

select	count(*)
into	qt_movto_fin_concil_w
from	ext_cartao_cr_movto_concil c,
	extrato_cartao_cr_movto b,
	extrato_cartao_cr_res a
where	a.nr_seq_extrato_arq	= nr_seq_extrato_arq_p
and	a.nr_sequencia		= b.nr_seq_extrato_res
and	b.nr_sequencia		= c.nr_seq_ext_movto;

/* se o arquivo financeiro foi conciliado direto com os movtos do Tasy, realiza a baixa igual ao cr�dito e d�bito */
if	(nvl(qt_movto_fin_concil_w,0) = 0) then

	baixar_concil_cartao_redecard(nr_seq_extrato_arq_p,nm_usuario_p,null);

else

	select	max(b.nr_sequencia),
		max(c.nr_sequencia),
		max(b.cd_estabelecimento),
		max(c.nr_seq_conta_banco),
		max(c.nr_seq_trans_indevido_cred),
		max(c.nr_seq_trans_desp_equip),
		max(c.nr_seq_trans_indevido),
		max(a.ie_tipo_arquivo)
	into	nr_seq_extrato_w,
		nr_seq_grupo_w,
		cd_estabelecimento_w,
		nr_seq_conta_banco_w,
		nr_seq_trans_indevido_cred_w,
		nr_seq_trans_financ_w,
		nr_seq_trans_indevido_w,
		ie_tipo_arquivo_w
	from	grupo_bandeira_cr c,
		extrato_cartao_cr b,
		extrato_cartao_cr_arq a
	where	a.nr_sequencia		= nr_seq_extrato_arq_p
	and	a.nr_seq_extrato	= b.nr_sequencia
	and	b.nr_seq_grupo		= c.nr_sequencia;

	if	(nr_seq_grupo_w	is not null) then

		open C02;
		loop
		fetch	C02 into
			vl_transacao_w,
			dt_prev_pagto_w,
			nr_seq_conta_banco_res_w;
		exit	when C02%notfound;

			if	(ie_tipo_arquivo_w	= 'C') or
				(ie_tipo_arquivo_w	= 'F') then
				nr_seq_tf_indevido_w	:= nr_seq_trans_indevido_cred_w;
			else
				nr_seq_tf_indevido_w	:= nr_seq_trans_indevido_w;
			end if;

			if	(nr_seq_tf_indevido_w is null) then
				--'Falta informar a transa��o de pagamento indevido no cadastro do grupo de bandeiras.');
				wheb_mensagem_pck.exibir_mensagem_abort(186815);
			end if;

			select	movto_trans_financ_seq.nextval
			into	nr_seq_movto_trans_w
			from	dual;

			insert	into movto_trans_financ
				(nr_sequencia,
				dt_transacao,
				nr_seq_trans_financ,
				vl_transacao,
				dt_atualizacao,
				nm_usuario,
				nr_lote_contabil,
				ie_conciliacao,
				nr_seq_banco,
				dt_referencia_saldo)
			values (nr_seq_movto_trans_w,
				nvl(dt_prev_pagto_w,sysdate),
				nr_seq_tf_indevido_w,
				abs(vl_transacao_w),
				sysdate,
				nm_usuario_p,
				0,
				'N',
				nvl(nr_seq_conta_banco_w,nr_seq_conta_banco_res_w),
				sysdate);

			atualizar_transacao_financeira(cd_estabelecimento_w,nr_seq_movto_trans_w,nm_usuario_p,'I');

		end loop;
		close C02;

	end if;

	select	nvl(max(ie_forma_lanc_lote_cartao),'P')
	into	ie_forma_lanc_lote_cartao_w
	from	parametro_contas_receber
	where	cd_estabelecimento	= cd_estabelecimento_w;

	if	(ie_forma_lanc_lote_cartao_w = 'P') then
		ie_gerar_movto_banco_w	:= 'S';
	else
		ie_gerar_movto_banco_w	:= 'N';
	end if;

	open C04;
	loop
	fetch	C04 into	
		nr_seq_extrato_parcela_fin_w,
		dt_prev_pagto_w,
		nr_parcela_w,
		nr_seq_movto_fin_w,
		vl_ajuste_w;
	exit	when C04%notfound;

		nr_seq_parcela_ajuste_w	:= null;
		nr_seq_parcela_w	:= null;
		vl_total_parcela_w	:= 0;
		vl_total_original_w	:= 0;
		vl_liquido_w		:= 0;
		vl_original_w		:= 0;

		open C05;
		loop
		fetch C05 into	
			nr_seq_extrato_parcela_cred_w,
			vl_conciliado_w;
		exit when C05%notfound;

			open C06;
			loop
			fetch C06 into	
				nr_seq_parcela_w;
			exit when C06%notfound;

				ajustar_parcela_cartao_cr(nr_seq_parcela_w,nr_seq_grupo_w,vl_ajuste_w,nm_usuario_p,nr_seq_movto_fin_w);

				select	max(a.vl_original),
					nvl(vl_total_parcela_w,0) + nvl(max(a.vl_liquido),0),
					nvl(vl_total_original_w,0) + nvl(max(decode(nvl(a.vl_original,0),0,a.vl_liquido,a.vl_original)),0)
				into	vl_original_w,
					vl_total_parcela_w,
					vl_total_original_w
				from	movto_cartao_cr_parcela a
				where	a.nr_sequencia	= nr_seq_parcela_w;

				if	(nvl(vl_original_w,0)	<> 0) then
					nr_seq_parcela_ajuste_w	:= nr_seq_parcela_w;
				end if;

				Baixar_parcela_cartao_cr(nr_seq_parcela_w,nvl(dt_prev_pagto_w,sysdate),nm_usuario_p,'S','S',null,'N','N',0,0,null);

			end loop;
			close C06;

		end loop;
		close C05;

		if	(nr_seq_parcela_ajuste_w	is not null) or
			(nr_seq_parcela_w		is not null) then

			if	(nr_seq_parcela_ajuste_w	is null) then

				update	movto_cartao_cr_parcela
				set	vl_original		= nvl(vl_liquido,0),
					vl_liquido		= nvl(vl_liquido,0) + nvl(vl_ajuste_w,0),
					vl_saldo_liquido	= vl_saldo_liquido + nvl(vl_ajuste_w,0),
					vl_despesa		= nvl(vl_despesa,0) - nvl(vl_ajuste_w,0)
				where	nr_sequencia		= nr_seq_parcela_w;

			else

				vl_ajuste_sobra_w	:= nvl(vl_ajuste_w,0) - (nvl(vl_total_parcela_w,0) - nvl(vl_total_original_w,0));

				update	movto_cartao_cr_parcela
				set	vl_liquido		= nvl(vl_liquido,0) + nvl(vl_ajuste_sobra_w,0),
					vl_saldo_liquido	= vl_saldo_liquido + nvl(vl_ajuste_sobra_w,0),
					vl_despesa		= nvl(vl_despesa,0) - nvl(vl_ajuste_sobra_w,0)
				where	nr_sequencia		= nr_seq_parcela_ajuste_w;

			end if;

			select	max(a.nr_seq_movto),
				max(a.vl_liquido),
				max(a.vl_despesa)
			into	nr_seq_movto_w,
				vl_liquido_w,
				vl_despesa_w
			from	movto_cartao_cr_parcela a
			where	a.nr_sequencia	= nvl(nr_seq_parcela_ajuste_w,nr_seq_parcela_w);

			update	movto_cartao_cr_baixa a
			set	a.vl_baixa		= vl_liquido_w,
				a.vl_despesa		= vl_despesa_w
			where	a.nr_sequencia		=
				(select	max(x.nr_sequencia)
				from	movto_cartao_cr_baixa x
				where	x.nr_seq_parcela	= nvl(nr_seq_parcela_ajuste_w,nr_seq_parcela_w))
			and	a.nr_seq_parcela		= nvl(nr_seq_parcela_ajuste_w,nr_seq_parcela_w);

			atualizar_saldo_cartao_cr(nr_seq_movto_w, nm_usuario_p);

		end if;

	end loop;
	close C04;

	nr_seq_parcela_ajuste_w	:= null;
	nr_seq_parcela_w	:= null;
	vl_total_parcela_w	:= 0;
	vl_total_original_w	:= 0;
	vl_liquido_w		:= 0;
	vl_original_w		:= 0;

	open	c07;
	loop
	fetch	c07 into
		nr_seq_parcela_w,
		dt_prev_pagto_w,
		vl_ajuste_w,
		nr_seq_movto_fin_w;
	exit	when c07%notfound;

		/* s� aplica o ajuste se ele n�o tiver sido aplicado pelas baixas do arquivo de cr�dito */
		select	count(*)
		into	qt_ajuste_w
		from	movto_cartao_cr_baixa f,
			movto_cartao_cr_parcela e,
			extrato_cartao_cr_arq d,
			extrato_cartao_cr_res c,
			extrato_cartao_cr_movto b,
			ext_cartao_cr_movto_concil a,
			ext_cartao_cr_movto_concil x
		where	trunc(f.dt_baixa,'dd')		= trunc(nvl(dt_prev_pagto_w,sysdate),'dd')
		and	e.nr_sequencia			= f.nr_seq_parcela
		and	b.nr_seq_extrato_parcela	= e.nr_seq_extrato_parcela
		and	d.ie_tipo_arquivo		= 'C'
		and	c.nr_seq_extrato_arq		= d.nr_sequencia
		and	b.nr_seq_extrato_res		= c.nr_sequencia
		and	a.nr_seq_ext_movto		= b.nr_sequencia
		and	a.nr_seq_extrato_parcela	= x.nr_seq_extrato_parcela
		and	x.nr_seq_ext_movto		= nr_seq_movto_fin_w;

		if	(qt_ajuste_w	= 0) then

			ajustar_parcela_cartao_cr(nr_seq_parcela_w,nr_seq_grupo_w,vl_ajuste_w,nm_usuario_p,nr_seq_movto_fin_w);

		else

			vl_ajuste_w	:= 0;

		end if;

		select	max(a.vl_original),
			nvl(vl_total_parcela_w,0) + nvl(max(a.vl_liquido),0),
			nvl(vl_total_original_w,0) + nvl(max(decode(nvl(a.vl_original,0),0,a.vl_liquido,a.vl_original)),0)
		into	vl_original_w,
			vl_total_parcela_w,
			vl_total_original_w
		from	movto_cartao_cr_parcela a
		where	a.nr_sequencia	= nr_seq_parcela_w;

		if	(nvl(vl_original_w,0)	<> 0) then
			nr_seq_parcela_ajuste_w	:= nr_seq_parcela_w;
		end if;

		Baixar_parcela_cartao_cr(nr_seq_parcela_w,nvl(dt_prev_pagto_w,sysdate),nm_usuario_p,'S','S',null,'N','N',0,0,null);

	end	loop;
	close	c07;

	if	(nr_seq_parcela_ajuste_w	is not null) or
		(nr_seq_parcela_w		is not null) then

		if	(nr_seq_parcela_ajuste_w	is null) then

			update	movto_cartao_cr_parcela
			set	vl_original		= nvl(vl_liquido,0),
				vl_liquido		= nvl(vl_liquido,0) + nvl(vl_ajuste_w,0),
				vl_saldo_liquido	= vl_saldo_liquido + nvl(vl_ajuste_w,0),
				vl_despesa		= nvl(vl_despesa,0) - nvl(vl_ajuste_w,0)
			where	nr_sequencia		= nr_seq_parcela_w;

		else

			vl_ajuste_sobra_w	:= nvl(vl_ajuste_w,0) - (nvl(vl_total_parcela_w,0) - nvl(vl_total_original_w,0));

			update	movto_cartao_cr_parcela
			set	vl_liquido		= nvl(vl_liquido,0) + nvl(vl_ajuste_sobra_w,0),
				vl_saldo_liquido	= vl_saldo_liquido + nvl(vl_ajuste_sobra_w,0),
				vl_despesa		= nvl(vl_despesa,0) - nvl(vl_ajuste_sobra_w,0)
			where	nr_sequencia		= nr_seq_parcela_ajuste_w;

		end if;

		select	max(a.nr_seq_movto),
			max(a.vl_liquido),
			max(a.vl_despesa)
		into	nr_seq_movto_w,
			vl_liquido_w,
			vl_despesa_w
		from	movto_cartao_cr_parcela a
		where	a.nr_sequencia	= nvl(nr_seq_parcela_ajuste_w,nr_seq_parcela_w);

		update	movto_cartao_cr_baixa a
		set	a.vl_baixa		= vl_liquido_w,
			a.vl_despesa		= vl_despesa_w
		where	a.nr_sequencia		=
			(select	max(x.nr_sequencia)
			from	movto_cartao_cr_baixa x
			where	x.nr_seq_parcela	= nvl(nr_seq_parcela_ajuste_w,nr_seq_parcela_w))
		and	a.nr_seq_parcela		= nvl(nr_seq_parcela_ajuste_w,nr_seq_parcela_w);

		atualizar_saldo_cartao_cr(nr_seq_movto_w, nm_usuario_p);

	end if;

	/* gerar a movimenta��o no Controle Banc�rio
	� necess�rio gerar depois por causa dos ajustes das sobras */
	if	(nvl(ie_gerar_movto_banco_w,'N') = 'S') then

		open	c08;
		loop
		fetch	c08 into
			nr_seq_movto_fin_w,
			nr_seq_extrato_parcela_fin_w,
			dt_prev_pagto_w;
		exit	when c08%notfound;

			open	c09;
			loop
			fetch	c09 into
				nr_seq_baixa_w;
			exit	when c09%notfound;

				gerar_movto_baixa_cartao(nr_seq_baixa_w,nm_usuario_p,0);

			end	loop;
			close	c09;

			open	c10;
			loop
			fetch	c10 into
				nr_seq_extrato_parcela_cred_w;
			exit	when c10%notfound;

				open	c11;
				loop
				fetch	c11 into
					nr_seq_baixa_w;
				exit	when c11%notfound;

					gerar_movto_baixa_cartao(nr_seq_baixa_w,nm_usuario_p,0);

				end	loop;
				close	c11;

			end	loop;
			close	c10;

		end	loop;
		close	c08;

	end if;

	if	(nr_seq_grupo_w is not null) and (nr_seq_trans_financ_w is not null) and (nr_seq_conta_banco_w is not null) then
		open c03;
		loop
		fetch c03 into
			vl_despesa_w,
			dt_prev_pagto_w;
		exit when c03%notfound;
			gerar_movto_despesa_equip(vl_despesa_w,nr_seq_trans_financ_w,nr_seq_conta_banco_w,nvl(dt_prev_pagto_w,sysdate),nm_usuario_p,'N');
		end loop;
		close c03;
	end if;

	update	extrato_cartao_cr_arq
	set	dt_baixa	= sysdate
	where	nr_sequencia	= nr_seq_extrato_arq_p;

	commit;

end if;

end baixar_concil_redecard_fin;
/
