create or replace
procedure same_adicionar_dados_pendencia (	nr_seq_prontuario_p	number,
						nr_seq_tipo_documento_p	number,
						ds_barras_p		varchar2,
						cd_responsavel_p	varchar2,
						ie_same_documentacao_p	number,
						ie_manual_p		in varchar2,
						ds_retorno_p		out varchar2,
						ds_erro_p		out varchar2) is 
						
ie_tipo_documento_w	varchar2(1);
ie_tipo_existe_w	varchar2(10);
ds_retorno_w		varchar2(4000);
nr_seq_assinatura_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
ie_registro_pac_pront_w	varchar2(1);
ie_situacao_w		varchar2(1);

begin

if	(nr_seq_tipo_documento_p is not null) and
	(ds_barras_p is not null) then
	
	select	max(a.cd_pessoa_fisica)
	into	cd_pessoa_fisica_w
	from	same_prontuario a
	where	a.nr_sequencia = nr_seq_prontuario_p;	
	
	select	max(a.ie_tipo_documento)
	into	ie_tipo_documento_w
	from	same_tipo_documento a
	where	a.nr_sequencia = nr_seq_tipo_documento_p;
	
	if (ie_tipo_documento_w is not null) then
	
		select	decode(count(*),0,'N','S')
		into	ie_registro_pac_pront_w
		from	dual
		where	(ie_tipo_documento_w = 'P' and exists(	select 1 from prescr_medica where nr_prescricao = ds_barras_p and cd_pessoa_fisica = cd_pessoa_fisica_w))
		or	(ie_tipo_documento_w = 'A' and exists(	select 1 from anamnese_paciente where nr_sequencia = ds_barras_p and nvl(cd_pessoa_fisica,obter_pessoa_atendimento(nr_atendimento,'C')) = cd_pessoa_fisica_w))
		or	(ie_tipo_documento_w = 'E' and exists(	select 1 from evolucao_paciente where cd_evolucao = ds_barras_p and cd_pessoa_fisica = cd_pessoa_fisica_w));
	
		if (ie_registro_pac_pront_w = 'S') then
			
			--error(-20011,ie_tipo_documento_w);
			if (ie_tipo_documento_w = 'P') then
				begin
				select	'S,',
					a.nr_seq_assinatura,
					'A' ie_situacao
				into	ie_tipo_existe_w,
					nr_seq_assinatura_w,
					ie_situacao_w
				from	prescr_medica a
				where	a.nr_prescricao = ds_barras_p;
				exception
				when others then
					ie_tipo_existe_w := 'N,';
				end;					
			elsif (ie_tipo_documento_w = 'A') then
				begin
				select	'S,',
					a.nr_seq_assinatura,
					ie_situacao
				into	ie_tipo_existe_w,
					nr_seq_assinatura_w,
					ie_situacao_w
				from	anamnese_paciente a
				where	a.nr_sequencia = ds_barras_p;
				exception 
				when others then
					ie_tipo_existe_w := 'N,';
				end;
			elsif (ie_tipo_documento_w = 'E') then	
				begin
				select	'S,',
					a.nr_seq_assinatura,
					ie_situacao
				into	ie_tipo_existe_w,
					nr_seq_assinatura_w,
					ie_situacao_w
				from	evolucao_paciente a
				where	a.cd_evolucao = ds_barras_p;
				exception 
				when others then
					ie_tipo_existe_w := 'N,';
				end;				
			end if;			
				
			--Assinatura
			if (nr_seq_assinatura_w is not null) then
				ds_retorno_w := ds_retorno_w || 'S,';
			else                                    
				ds_retorno_w := ds_retorno_w || 'N,';
			end if;
			
			--Possui Tasy
			ds_retorno_w := ds_retorno_w||ie_tipo_existe_w;
			
			--Tipo de documento
			ds_retorno_w	:= ds_retorno_w || nr_seq_tipo_documento_p || ',' || substr(same_obter_tipo_documento(nr_seq_tipo_documento_p),1,255) ||',';
			
			--N� do documento			
			ds_retorno_w	:= ds_retorno_w || ds_barras_p||',';								
			
			--Documenta��o
			if (ie_same_documentacao_p = 0) then
				ds_retorno_w	:= ds_retorno_w || 'P,';
			elsif (ie_same_documentacao_p = 1) then
				ds_retorno_w	:= ds_retorno_w || 'A,';
			elsif (ie_same_documentacao_p = 2) then
				ds_retorno_w	:= ds_retorno_w || 'N,';
			else
				ds_retorno_w	:= ds_retorno_w || ' ,';
			end if;
			
			--Respons�vel
			ds_retorno_w	:= ds_retorno_w || cd_responsavel_p || ',' || substr(obter_nome_pf(cd_responsavel_p),1,255) ||',';
			
			--Paciente
			ds_retorno_w	:= ds_retorno_w || cd_pessoa_fisica_w || ',' || substr(obter_nome_pf(cd_pessoa_fisica_w),1,255) ||',';		
			
			--Barras		
			if (ie_manual_p = 'S') then
				ds_retorno_w	:= ds_retorno_w || ',';
			else
				ds_retorno_w	:= ds_retorno_w || ds_barras_p ||',';
			end if;
			
			--Situa��o
			ds_retorno_w	:= ds_retorno_w || ie_situacao_w ||',';
			
		else
			ds_erro_p	:= obter_texto_tasy(89668, wheb_usuario_pck.get_nr_seq_idioma);		
		end if;
		ds_retorno_p	:= ds_retorno_w;		
	end if;
end if;

commit;

end same_adicionar_dados_pendencia;
/
