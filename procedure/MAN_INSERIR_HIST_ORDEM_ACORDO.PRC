create or replace procedure man_inserir_hist_ordem_acordo (
		nr_sequencia_p number,
		nm_usuario_p   varchar2,
		ie_tipo_p      varchar2)
is
	ds_titulo_w		varchar2 (1000);
	dt_datafim_w		date;
	ds_justificativa_w	varchar2 (4000);
	nr_seq_acordo_w		number (10);
	nr_seq_ordem_serv	number (10);
	nr_seq_tipo_hist_w	man_tipo_hist.nr_sequencia%type;
begin
	select	max (ds_titulo),
		max (b.dt_entrega_desejada),
		max (b.nr_seq_acordo),
		max (b.nr_seq_ordem_servico)
	into	ds_titulo_w,
		dt_datafim_w,
		nr_seq_acordo_w,
		nr_seq_ordem_serv
	from	desenv_acordo a,
		desenv_acordo_os b
	where	a.nr_sequencia = b.nr_seq_acordo
	and	b.nr_sequencia   = nr_sequencia_p;

	if	(ie_tipo_p        = 'I') then
		begin
			ds_justificativa_w := obter_desc_expressao_idioma (877804, 'Ordem de servi�o adicionada no acordo', 3) ||': ' || nr_seq_acordo_w || ' - ' || ds_titulo_w || chr (10) || obter_desc_expressao_idioma (314533, 'Usu�rio', 3) ||':  ' || nm_usuario_p || chr (10) || obter_desc_expressao_idioma (784655, 'Data de entrega', 3) ||': ' || dt_datafim_w;
			nr_seq_tipo_hist_w := 230; -- Acordo - entrada de OS
		end;
	elsif (ie_tipo_p = 'A') then
		begin
			ds_justificativa_w := obter_desc_expressao_idioma (877858, 'Movido para o acordo', 3) ||': ' || nr_seq_acordo_w || ' - ' || ds_titulo_w || chr (10) || obter_desc_expressao_idioma (314533, 'Usu�rio', 3) ||':  ' || nm_usuario_p || chr (10) || obter_desc_expressao_idioma (784655, 'Data de entrega', 3) ||': ' || dt_datafim_w;
			nr_seq_tipo_hist_w := 232; -- Acordo - alteracao
		end;
	elsif (ie_tipo_p = 'E') then
		begin
			ds_justificativa_w := obter_desc_expressao_idioma (877860, 'Removido do acordo', 3) ||': ' || nr_seq_acordo_w || ' - ' || ds_titulo_w || chr (10) || obter_desc_expressao_idioma (314533, 'Usu�rio', 3) ||':  ' || nm_usuario_p || chr (10) || obter_desc_expressao_idioma (784655, 'Data de entrega', 3) ||': ' || dt_datafim_w;
			nr_seq_tipo_hist_w := 231; -- Acordo - exclusao de OS
		end;
	end if;
	insert
	into man_ordem_serv_tecnico
		(
			nr_sequencia,
			nr_seq_ordem_serv,
			dt_atualizacao,
			nm_usuario,
			ds_relat_tecnico,
			dt_historico,
			dt_liberacao,
			nm_usuario_lib,
			nr_seq_tipo,
			ie_origem
		)
		values
		(
			man_ordem_serv_tecnico_seq.nextval,
			nr_seq_ordem_serv,
			sysdate,
			nm_usuario_p,
			ds_justificativa_w,
			sysdate,
			sysdate,
			nm_usuario_p,
			nr_seq_tipo_hist_w,
			'I'
		) ;
	commit;
end man_inserir_hist_ordem_acordo;
/