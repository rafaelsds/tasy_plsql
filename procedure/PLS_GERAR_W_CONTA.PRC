create or replace
procedure pls_gerar_w_conta
			(	nr_seq_protocolo_p	number,
				nr_seq_prestador_p	number,
				nr_seq_lote_p		number,
				nm_usuario_p		varchar2) is

nr_seq_protocolo_w	number(10);
nr_seq_pls_conta_w	number(10);
nr_seq_item_w		number(10);
nr_seq_conta_w		number(10);
cd_guia_w		varchar2(20);
cd_senha_w		varchar2(20);
nm_segurado_w		varchar2(255);
cd_usuario_plano_w	varchar2(20);	
vl_cobrado_w		number(15,2);
vl_total_w		number(15,2);
vl_glosa_conta_w	number(15,2);
cd_motivo_glosa_conta_w	varchar2(20);
cd_grau_partic_w	varchar2(2);
dt_item_w		date;
ds_item_w		varchar2(255);
cd_tabela_item_w	varchar2(2);
cd_item_w		number(15);
qt_item_w		number(9,3);
vl_item_w		number(15,2);
vl_liberado_item_w	number(15,2);
vl_glosa_item_w		number(15,2);
cd_motivo_glosa_item_w	varchar2(20);
count_w			number(10) := 0;

cd_ans_w		varchar2(50);
ds_operadora_w		varchar2(255);
cd_cgc_operadora_w	varchar2(20);
nr_demonstrativo_w	varchar2(50);
cd_prestador_w		varchar2(50);
cd_cgc_prestador_w	varchar2(20);	
nr_cpf_prestador_w	varchar2(50);
ds_prestador_w		varchar2(255);
cd_cnes_prestador_w	varchar2(50);
nr_fatura_w		varchar2(50);
nr_lote_w		varchar2(50);
dt_envio_lote_w		date;
nr_protocolo_w		varchar2(50);
vl_fatura_w		number(15,2);
vl_lib_fatura_w		number(15,2);
vl_glosa_fatura_w	number(15,2);
dt_pagamento_w		date;
ie_forma_pagto_w	varchar2(5);
cd_banco_w		varchar2(20);		
cd_agencia_w		varchar2(20);
nr_conta_w		varchar2(50);
nr_seq_lote_pgto_w	number(10);
dt_mes_competencia_w	date;
nr_seq_evento_w		number(10);
ds_evento_w		varchar2(255);
qt_registros_w		number(10) := 0;
nr_seq_pagamento_w	number(10);

cd_ans_ww		varchar2(50);
nm_fantasia_w		varchar2(255);
cd_cgc_outorgante_w	varchar2(255);
nr_seq_prestador_w	number(10);
cd_cgc_w		varchar2(14);
nr_cpf_w		varchar2(11);
nm_pessoa_w		varchar2(255);
cd_cnes_w		varchar2(255);
nr_protocolo_prestador_w	varchar2(255);
dt_protocolo_w		date;
vl_cobrado_ww		number(15,2);
vl_glosado_ww		number(15,2);
vl_total_ww		number(15,2);
cd_estabelecimento_w	number(10);

cursor c01 is
select	a.nr_sequencia,
	a.cd_guia,
	a.cd_senha,
	substr(pls_obter_dados_segurado(a.nr_seq_segurado,'N'),1,255),
	substr(pls_obter_dados_segurado(a.nr_seq_segurado,'C'),1,30),
	a.vl_cobrado,
	a.vl_total,
	a.vl_glosa
from	pls_conta a
Where	a.nr_seq_protocolo	= nr_seq_protocolo_p;

cursor	c02 is
select	a.dt_procedimento,
	substr(obter_descricao_procedimento(a.cd_procedimento,a.ie_origem_proced),1,255),
	b.cd_tabela_xml,
	a.cd_procedimento,
	a.qt_procedimento,
	a.vl_procedimento,
	a.vl_liberado,
	a.vl_glosa
from	pls_conta_proc a,
	tiss_tipo_tabela b
where 	a.nr_seq_tiss_tabela 	= b.nr_sequencia(+)
and	a.nr_seq_conta		= nr_seq_conta_w
union
select	a.dt_atendimento,
	substr(obter_descricao_padrao('PLS_MATERIAL','DS_MATERIAL',a.nr_seq_material),1,255),
	b.cd_tabela_xml,
	c.cd_material,
	a.qt_material,
	a.vl_material,
	a.vl_liberado,
	a.vl_glosa
from	pls_conta_mat a,
	tiss_tipo_tabela b,
	pls_material c
Where	a.nr_seq_tiss_tabela	= b.nr_sequencia(+)
and	a.nr_seq_material	= c.nr_sequencia
and	a.nr_seq_conta		= nr_seq_conta_w;

Cursor C03 is
	select	nr_seq_evento,
		substr(pls_obter_desc_evento(nr_seq_evento),1,255),
		vl_item,
		dt_atualizacao
	from	pls_pagamento_item
	where	nr_seq_pagamento	= nr_seq_pagamento_w;

begin

delete	from 	w_pls_protocolo
where	nm_usuario	= nm_usuario_p;

delete	from 	w_pls_conta
where	nm_usuario	= nm_usuario_p;

delete	from 	w_pls_item
where	nm_usuario	= nm_usuario_p;

select	w_pls_protocolo_seq.nextval
into	nr_seq_protocolo_w
from 	dual;

if	(nr_seq_protocolo_p	is not null) then
	select	b.cd_ans,
		b.nm_fantasia,
		b.cd_cgc_outorgante,
		c.nr_sequencia,
		c.cd_cgc,
		obter_cpf_pessoa_fisica(c.cd_pessoa_fisica),
		substr(obter_nome_pf_pj(c.cd_pessoa_fisica, c.cd_cgc),1,254),
		decode(c.cd_pessoa_fisica, null, substr(obter_dados_pf_pj(null,c.cd_cgc, 'CNES'),1,20), substr(obter_dados_pf(c.cd_pessoa_fisica,'CNES'),1,20)),
		Somente_Numero(a.nr_protocolo_prestador),
		a.dt_protocolo,
		nvl(pls_obter_valor_protocolo(a.nr_sequencia,'C'),0),
		nvl(pls_obter_valor_protocolo(a.nr_sequencia,'G'),0),
		nvl(pls_obter_valor_protocolo(a.nr_sequencia,'T'),0),
		a.cd_estabelecimento
	into	cd_ans_ww,
		nm_fantasia_w,
		cd_cgc_outorgante_w,
		nr_seq_prestador_w,
		cd_cgc_w,
		nr_cpf_w,
		nm_pessoa_w,
		cd_cnes_w,
		nr_protocolo_prestador_w,
		dt_protocolo_w,
		vl_cobrado_ww,
		vl_glosado_ww,
		vl_total_ww,
		cd_estabelecimento_w
	from	pls_protocolo_conta a,
		pls_outorgante b,
		pls_prestador c
	where	a.nr_seq_outorgante	= b.nr_sequencia(+)				
	and 	a.nr_seq_prestador	= c.nr_sequencia(+)
	and	a.nr_sequencia		= nr_seq_protocolo_p;
	
	if	(nvl(cd_cgc_outorgante_w,'0') = '0') then
		begin
		select	a.cd_ans,
			a.nm_fantasia,
			a.cd_cgc_outorgante
		into	cd_ans_ww,
			nm_fantasia_w,
			cd_cgc_outorgante_w
		from	pls_outorgante a
		where	a.cd_estabelecimento = cd_estabelecimento_w;
		exception
		when others then
			cd_ans_ww		:= null;
			nm_fantasia_w		:= null;
			cd_cgc_outorgante_w	:= null;
		end;
	end if;
	
	insert	into w_pls_protocolo(	nr_sequencia, dt_atualizacao, nm_usuario, dt_atualizacao_nrec, nm_usuario_nrec,
					cd_ans, ds_operadora, cd_cgc_operadora, nr_demonstrativo, dt_emissao,
					cd_prestador, cd_cgc_prestador, nr_cpf_prestador, ds_prestador, cd_cnes_prestador,
					nr_fatura, nr_lote, dt_envio_lote, nr_protocolo, vl_protocolo,
					vl_glosa_protocolo, cd_glosa_protocolo, vl_fatura, vl_lib_fatura, vl_glosa_fatura,
					vl_geral, vl_lib_geral, vl_glosa_geral, nr_seq_protocolo)
			values	(	nr_seq_protocolo_w, sysdate, nm_usuario_p, sysdate, nm_usuario_p,
					cd_ans_ww, nm_fantasia_w, cd_cgc_outorgante_w, nr_seq_protocolo_p, sysdate,
					nr_seq_prestador_w, cd_cgc_w, nr_cpf_w, nm_pessoa_w, cd_cnes_w,
					nr_seq_protocolo_p, nr_protocolo_prestador_w, dt_protocolo_w, nr_seq_protocolo_p, vl_cobrado_ww,
					vl_glosado_ww, null, vl_cobrado_ww, vl_total_ww, vl_glosado_ww,
					vl_cobrado_ww, vl_total_ww, vl_glosado_ww, nr_seq_protocolo_p);

	open c01;
	loop
	fetch c01 into
		nr_seq_conta_w,
		cd_guia_w,
		cd_senha_w,
		nm_segurado_w,
		cd_usuario_plano_w,
		vl_cobrado_w,
		vl_total_w,
		vl_glosa_conta_w;
	exit when c01%notfound;
		select	max(substr(tiss_obter_motivo_glosa(b.nr_seq_motivo_glosa,'C'),1,255))
		into	cd_motivo_glosa_conta_w
		from	pls_conta_glosa b
		Where	b.nr_seq_conta	= nr_seq_conta_w;

		select	w_pls_conta_seq.nextval
		into	nr_seq_pls_conta_w
		from 	dual;

		begin
		insert	into w_pls_conta(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_protocolo,
			nr_seq_conta,
			cd_guia,
			cd_senha,
			nm_segurado,
			cd_segurado,
			vl_conta,
			vl_lib_conta,
			vl_glosa_conta,
			cd_glosa_conta)
		values (nr_seq_pls_conta_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_protocolo_w,
			nr_seq_conta_w,
			cd_guia_w,
			cd_senha_w,
			nm_segurado_w,
			cd_usuario_plano_w,
			vl_cobrado_w,
			vl_total_w,
			vl_glosa_conta_w,
			cd_motivo_glosa_conta_w);
		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(267433,'NR_SEQ_PROTOCOLO_W=' || nr_seq_protocolo_w);
		end;

		open c02;
		loop
		fetch c02 into
			dt_item_w,
			ds_item_w,
			cd_tabela_item_w,
			cd_item_w,
			qt_item_w,
			vl_item_w,
			vl_liberado_item_w,
			vl_glosa_item_w;
		exit when c02%notfound;

			select	count(*)
			into	count_w
			from	w_pls_item
			where	nr_seq_conta	= nr_seq_pls_conta_w;

			if	(count_w >= 3)	then
				select	w_pls_conta_seq.nextval
				into	nr_seq_pls_conta_w
				from 	dual;

				insert	into w_pls_conta(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_protocolo,
					nr_seq_conta,	
					cd_guia,
					cd_senha,
					nm_segurado,
					cd_segurado,
					vl_conta,
					vl_lib_conta,
					vl_glosa_conta,
					cd_glosa_conta)
				values (nr_seq_pls_conta_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_protocolo_w,
					nr_seq_conta_w,
					cd_guia_w,
					cd_senha_w,
					nm_segurado_w,
					cd_usuario_plano_w,
					vl_cobrado_w,
					vl_total_w,
					vl_glosa_conta_w,
					cd_motivo_glosa_conta_w);	

				count_w := 0;

			end if;

			select	max(substr(tiss_obter_motivo_glosa(b.nr_seq_motivo_glosa,'C'),1,255))
			into	cd_motivo_glosa_item_w
			from	pls_conta_glosa b
			where 	b.nr_seq_conta_proc	= nr_seq_conta_w
			or	b.nr_seq_conta_mat	= nr_seq_conta_w;

			select	max(a.cd_tiss)
			into	cd_grau_partic_w
			from	pls_grau_participacao a,
				pls_proc_participante b,
				pls_conta_proc c,
				pls_conta d
			where	a.nr_sequencia		= b.nr_seq_grau_partic
			and	b.nr_seq_conta_proc	= c.nr_sequencia
			and	c.nr_seq_conta		= d.nr_sequencia
			and	d.ie_tipo_guia		= '6';

			select	w_pls_item_seq.nextval
			into	nr_seq_item_w
			from	dual;

			insert	into w_pls_item(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					dt_item,
					ds_item,
					cd_tabela,
					cd_item,
					cd_grau_partic,
					qt_item,
					vl_item,
					vl_lib_item,
					vl_glosa_item,
					cd_glosa_item,
					nr_seq_conta)
				values	(nr_seq_item_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					dt_item_w,
					ds_item_w,
					cd_tabela_item_w,
					cd_item_w,
					cd_grau_partic_w,
					qt_item_w,
					vl_item_w,
					vl_liberado_item_w,
					vl_glosa_item_w,
					cd_motivo_glosa_item_w,
					nr_seq_pls_conta_w);		

		end loop;
		close c02;

		select	count(*)
		into	count_w
		from	w_pls_item
		where	nr_seq_conta = nr_seq_pls_conta_w;

		while	(count_w < 3) loop

			select	w_pls_item_seq.nextval
			into	nr_seq_item_w
			from	dual;

			insert	into w_pls_item(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_conta)
				values	(nr_seq_item_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_pls_conta_w);

			count_w := count_w + 1;

		end loop;

	end loop;
	close c01;
elsif	(nr_seq_prestador_p	is not null) and (nr_seq_lote_p	is not null) then
	begin
	select	a.nr_sequencia,
		trunc(a.dt_geracao_titulos),
		a.dt_mes_competencia,
		b.vl_pagamento,
		b.nr_seq_prestador,
		c.cd_cgc,
		obter_cpf_pessoa_fisica(c.cd_pessoa_fisica),
		substr(obter_nome_pf_pj(c.cd_pessoa_fisica, c.cd_cgc),1,254),
		decode(c.cd_pessoa_fisica, null, substr(obter_dados_pf_pj(null,c.cd_cgc, 'CNES'),1,20), substr(obter_dados_pf(c.cd_pessoa_fisica,'CNES'),1,20)),
		d.cd_ans,
		d.nm_fantasia,
		d.cd_cgc_outorgante,
		e.cd_agencia_bancaria,
		e.cd_banco,
		e.cd_conta,
		b.nr_sequencia
	into	nr_lote_w,
		dt_mes_competencia_w,
		dt_pagamento_w,
		vl_fatura_w,
		cd_prestador_w,
		cd_cgc_prestador_w,
		nr_cpf_prestador_w,
		ds_prestador_w,
		cd_cnes_prestador_w,
		cd_ans_w,
		ds_operadora_w,
		cd_cgc_operadora_w,
		cd_agencia_w,
		cd_banco_w,
		nr_conta_w,
		nr_seq_pagamento_w
	from	banco_estabelecimento	e,
		pls_outorgante		d,
		pls_prestador		c,
		pls_pagamento_prestador	b,
		pls_lote_pagamento	a
	where	b.nr_seq_prestador	= nr_seq_prestador_p
	and	b.nr_seq_lote		= nr_seq_lote_p
	and	b.nr_seq_prestador	= c.nr_sequencia
	and	b.nr_seq_lote		= a.nr_sequencia
	and	c.cd_estabelecimento	= d.cd_estabelecimento
	and	b.nr_seq_conta_banco	= e.nr_sequencia(+);
	exception
	when others then
		wheb_mensagem_pck.exibir_mensagem_abort(267432);
	end;
	
	insert	into w_pls_protocolo(	
			nr_sequencia,           
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			cd_ans,
			ds_operadora,
			cd_cgc_operadora,
			nr_demonstrativo,      
			dt_emissao,
			cd_prestador,
			cd_cgc_prestador,
			nr_cpf_prestador,
			ds_prestador,
			cd_cnes_prestador,
			nr_fatura,
			nr_lote,
			dt_envio_lote,
			nr_protocolo,
			vl_fatura,
			vl_lib_fatura,
			vl_glosa_fatura,
			nr_seq_protocolo,
			dt_pagamento,
			ie_forma_pagto,
			cd_banco,
			cd_agencia,
			nr_conta,
			nr_seq_lote_pgto,
			vl_geral,
			vl_lib_geral,
			vl_glosa_geral)
		values	(nr_seq_protocolo_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			cd_ans_w,
			ds_operadora_w,
			cd_cgc_operadora_w,
			nr_seq_protocolo_w,
			sysdate,
			cd_prestador_w,
			cd_cgc_prestador_w,
			nr_cpf_prestador_w,
			ds_prestador_w,
			cd_cnes_prestador_w,
			nr_lote_w,
			nr_lote_w,
			dt_mes_competencia_w,
			nr_protocolo_w,
			vl_fatura_w,
			vl_fatura_w,
			null,
			null,
			dt_pagamento_w,
			ie_forma_pagto_w,
			cd_banco_w,
			cd_agencia_w,
			nr_conta_w,
			nr_lote_w,
			vl_fatura_w,
			vl_fatura_w,
			null);
		
	select	w_pls_conta_seq.nextval
	into	nr_seq_pls_conta_w
	from 	dual;

	insert	into	w_pls_conta(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_protocolo,
			nr_seq_conta)
		values (nr_seq_pls_conta_w,
			sysdate,
			nm_usuario_p,
			sysdate,
			nm_usuario_p,
			nr_seq_protocolo_w,
			nr_seq_protocolo_w);
	
	open C03;
	loop
	fetch C03 into	
		nr_seq_evento_w,
		ds_evento_w,
		vl_item_w,
		dt_item_w;
	exit when C03%notfound;
		begin
		
		select	w_pls_item_seq.nextval
		into	nr_seq_item_w
		from	dual;

		insert	into w_pls_item(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				dt_item,
				ds_item,
				cd_tabela,
				cd_item,
				cd_grau_partic,
				qt_item,
				vl_item,
				vl_lib_item,
				vl_glosa_item,
				cd_glosa_item,
				nr_seq_conta)
			values	(nr_seq_item_w,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				dt_item_w,
				ds_evento_w,
				null,
				nr_seq_evento_w,
				null,
				1,
				vl_item_w,
				null,
				null,
				null,
				nr_seq_pls_conta_w);
		
		select	count(*)
		into	count_w
		from	w_pls_item
		where	nr_seq_conta = nr_seq_pls_conta_w;

		while	(count_w < 3) loop

			select	w_pls_item_seq.nextval
			into	nr_seq_item_w
			from	dual;

			insert	into w_pls_item(
					nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_conta)
				values	(nr_seq_item_w,
					sysdate,
					nm_usuario_p,
					sysdate,
					nm_usuario_p,
					nr_seq_pls_conta_w);

			count_w := count_w + 1;

		end loop;
		
		end;
	end loop;
	close C03;
	
	
end if;

commit;

end pls_gerar_w_conta;
/