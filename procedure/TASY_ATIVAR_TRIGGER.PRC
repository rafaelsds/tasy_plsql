CREATE OR REPLACE
PROCEDURE Tasy_ativar_trigger IS


ds_comando_w varchar2(255);
nm_trigger_w varchar2(30);
vl_retorno_w varchar2(255);


CURSOR C01 IS
SELECT  TRIGGER_NAME
FROM  	USER_TRIGGERS
WHERE  	STATUS = 'DISABLED';

BEGIN

OPEN C01;
LOOP
FETCH C01 into
 nm_trigger_w;
exit when c01%notfound;
 begin

 ds_comando_w := 'ALTER TRIGGER ' || nm_trigger_w || ' enable ';

 Obter_Valor_Dinamico(ds_comando_w, vl_retorno_w);

 end;
END LOOP;
CLOSE C01;

END Tasy_ativar_trigger;
/
