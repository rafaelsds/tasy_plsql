create or replace
procedure  gerar_participante_proced(	nr_cirurgia_p		number,
				nm_usuario_p		varchar2) is

ie_gera_partic_cirur_proc_w	varchar2(1) := 'N';
cd_medico_anestesista_w	varchar2(10);
cd_medico_cirurgiao_w	varchar2(10);
cd_medico_exec_w		varchar2(10);
cd_funcao_w		number(3,0);
nr_seq_agenda_w		number(10,0);
	
Cursor C01 is
	select	distinct a.cd_medico_exec
	from	prescr_procedimento a,
		cirurgia b
	where	a.nr_prescricao = b.nr_prescricao
	and	nr_cirurgia = nr_cirurgia_p
	order by 1;
	
begin

select	max(nr_sequencia)
into	nr_seq_agenda_w
from	agenda_paciente
where 	nr_cirurgia = nr_cirurgia_p;

select	max(cd_medico_anestesista),   
		max(cd_medico_cirurgiao)
into	cd_medico_anestesista_w,   
		cd_medico_cirurgiao_w
from	cirurgia
where	nr_cirurgia	=	nr_cirurgia_p;
	
if	(cd_medico_anestesista_w is not null) then
	select	min(cd_funcao)
	into	cd_funcao_w
	from	funcao_medico
	where	ie_anestesista	=	'S';
	
	if	(cd_funcao_w > 0) then
		gerar_participante_cirurgia(cd_medico_anestesista_w, nr_cirurgia_p,cd_funcao_w,'A',nm_usuario_p);
	end if;
end if;

if	(cd_medico_cirurgiao_w is not null) then
	select	min(cd_funcao)
	into	cd_funcao_w
	from	funcao_medico
	where	ie_cirurgiao	=	'S';
	
	if	(cd_funcao_w > 0) then
		gerar_participante_cirurgia(cd_medico_cirurgiao_w, nr_cirurgia_p,cd_funcao_w,'C',nm_usuario_p);
	end if;
end if;

select	min(cd_funcao)
into	cd_funcao_w
from	funcao_medico
where	ie_cirurgiao = 'S';

if	(cd_funcao_w is not null) then
	open C01;
	loop
	fetch C01 into	
		cd_medico_exec_w;
	exit when C01%notfound;
		begin
			if	(cd_medico_exec_w is not null) then
				gerar_participante_cirurgia(cd_medico_exec_w, nr_cirurgia_p,cd_funcao_w,'C',nm_usuario_p);
			end if;
		end;
	end loop;
	close C01;
end if;

commit;

end gerar_participante_proced;
/