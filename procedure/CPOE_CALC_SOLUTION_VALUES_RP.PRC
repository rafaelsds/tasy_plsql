create or replace
procedure cpoe_calc_solution_values_rp (	nr_seq_cpoe_mat_p	in	cpoe_material.nr_sequencia%type)
										is
										
cd_material_w				cpoe_material.cd_material%type;
qt_dose_w					cpoe_material.qt_dose%type;
cd_unidade_medida_w			cpoe_material.cd_unidade_medida%type;
qt_volume_w					cpoe_material.qt_volume%type;
qt_total_solucao_w			cpoe_material.qt_solucao_total%type := 0;
qt_hora_fase_w				cpoe_material.qt_hora_fase%type;
qt_hora_fase_ww             varchar2(32000);
ie_tipo_solucao_w			cpoe_material.ie_tipo_solucao%type;
ie_tipo_dosagem_w			cpoe_material.ie_tipo_dosagem%type;
qt_tempo_aplicacao_w		cpoe_material.qt_tempo_aplicacao%type;
qt_dosagem_w				cpoe_material.qt_dosagem%type;
qt_dosagem_final_w          varchar(50);
qt_volume_sol_w				cpoe_material.qt_volume%type := 0;
cd_intervalo_w				cpoe_material.cd_intervalo%type;
ie_ref_calculo_w			cpoe_material.ie_ref_calculo%type;
ie_bomba_infusao_w			cpoe_material.ie_bomba_infusao%type;
qt_solucao_total_w			cpoe_material.qt_solucao_total%type;
qt_total_etapa_w			cpoe_material.qt_solucao%type := 0;
nr_etapas_w					cpoe_material.nr_etapas%type;
ds_ref_calculo_w			cpoe_material.ds_ref_calculo%type;
qt_vol_w					cpoe_material.qt_volume%type;
ds_erro_w					varchar2(32000);

cursor c01 is
select	cd_material,
		qt_dose,
		cd_unidade_medida
from	cpoe_material_add_comp
where	nr_seq_cpoe_material = nr_seq_cpoe_mat_p;


procedure cpoe_calcular_volume_total_sol (	ie_tipo_dosagem_p		varchar2,
											qt_tempo_aplicacao_p	cpoe_material.qt_tempo_aplicacao%type,
											qt_solucao_total_p		number,
											qt_dosagem_p			number,
											campo_p					number,
											vl_retorno_p			out number,
											ie_arredonda_p          varchar2,
											nr_etapas_p             number default 1,
											ie_vel_conforme_etapa_p varchar2 default 'N',
											qt_hora_fase_p          number default null) is
qt_gotas_aux_w     number(18, 6);
qt_tempo_aux_w     number(18, 6);
qt_resultado_aux_w number(18, 6);
nr_etapas_aux_w    number(18, 6);
qt_solucao_aux_w   number(18, 6);

begin

	if (ie_tipo_dosagem_p    = 'GTM') then
		qt_gotas_aux_w        := 20;
		qt_tempo_aux_w        := 60;
	elsif (ie_tipo_dosagem_p = 'MLH') then
		qt_gotas_aux_w        := 1;
		qt_tempo_aux_w        := 1;
	else
		qt_gotas_aux_w := 60;
		qt_tempo_aux_w := 60;
	end if;

	qt_resultado_aux_w := 0;

	case campo_p
		when 1 then

			if (ie_vel_conforme_etapa_p = 'N') then
				qt_resultado_aux_w := dividir(dividir((round(qt_solucao_total_p) * qt_gotas_aux_w), qt_tempo_aplicacao_p), qt_tempo_aux_w);
			else
				nr_etapas_aux_w	:= nr_etapas_p;

				if (nr_etapas_p = 0) then
					nr_etapas_aux_w := 1;
				end if;

				qt_solucao_aux_w   := qt_solucao_total_p / nr_etapas_aux_w;
				qt_resultado_aux_w := dividir(dividir((round(qt_solucao_aux_w) * qt_gotas_aux_w), qt_hora_fase_p), qt_tempo_aux_w);
			end if;
		when 2 then
			qt_resultado_aux_w := round(dividir((qt_dosagem_p * qt_tempo_aux_w * qt_tempo_aplicacao_p), qt_gotas_aux_w));
			qt_resultado_aux_w := (round(qt_resultado_aux_w   / 100) * 100);
		when 3 then
			qt_resultado_aux_w := round(dividir(dividir((qt_solucao_total_p * qt_gotas_aux_w), qt_dosagem_p), qt_tempo_aux_w));
	end case;

	if (qt_resultado_aux_w = 0) then
		qt_resultado_aux_w  := null;
	elsif (ie_arredonda_p  = 'S') and (ie_tipo_dosagem_p = 'GTM') and (nvl(ie_bomba_infusao_w, 'N') = 'N') then
		qt_resultado_aux_w  := round(qt_resultado_aux_w);
	end if;

  vl_retorno_p := qt_resultado_aux_w;
  
end;


begin

	select	cd_material,
			qt_dose,
			cd_unidade_medida,
			nr_etapas,
			qt_hora_fase,
			ie_tipo_solucao,
			ie_tipo_dosagem,
			qt_tempo_aplicacao,
			qt_dosagem,
			cd_intervalo,
			ie_ref_calculo,
			ie_bomba_infusao,
			qt_solucao_total
	into	cd_material_w,
			qt_dose_w,
			cd_unidade_medida_w,
			nr_etapas_w,
			qt_hora_fase_w,
			ie_tipo_solucao_w,
			ie_tipo_dosagem_w,
			qt_tempo_aplicacao_w,
			qt_dosagem_w,
			cd_intervalo_w,
			ie_ref_calculo_w,
			ie_bomba_infusao_w,
			qt_solucao_total_w
	from	cpoe_material
	where	nr_sequencia = nr_seq_cpoe_mat_p;
	
		
	qt_total_solucao_w := nvl(qt_total_solucao_w,0) + obter_conversao_ml(cd_material_w, NVL(qt_dose_w, 0), cd_unidade_medida_w);
		
	
	for rc01_w in c01 loop
		if (rc01_w.cd_material is not null) then
		
			qt_total_solucao_w := nvl(qt_total_solucao_w,0) + obter_conversao_ml(rc01_w.cd_material, NVL(rc01_w.qt_dose, 0), rc01_w.cd_unidade_medida);
			
		end if;
	end loop;
	  	
	qt_volume_sol_w := qt_solucao_total_w / nvl(nr_etapas_w, 1);
	
	qt_total_etapa_w := qt_total_solucao_w * nvl(nr_etapas_w, 1);

	qt_hora_fase_ww := substr(converter_decimal_para_hora(qt_hora_fase_w),0,5);

	if (ie_tipo_solucao_w = 'I') then
		qt_tempo_aplicacao_w := nr_etapas_w * (obter_minutos_hora(qt_hora_fase_ww) / 60);
	end if;

	if (ie_tipo_solucao_w = 'I' or ie_tipo_solucao_w = 'C') then
		
		CPOE_Calcular_volume_total_sol(upper(ie_tipo_dosagem_w), qt_tempo_aplicacao_w, qt_total_etapa_w, qt_dosagem_w, 1, qt_dosagem_final_w, 'N', nr_etapas_w, 'N');
	
	end if;
	
	if (substr(qt_dosagem_final_w, 1, 1) = ',') then
		qt_dosagem_final_w := '0' || qt_dosagem_final_w;
		qt_dosagem_final_w := replace(qt_dosagem_final_w, '.', ',');
	end if;
	
	update	cpoe_material
	set		qt_solucao_total = qt_total_etapa_w,
			qt_hora_fase     = qt_hora_fase_ww,
			nr_etapas        = nr_etapas_w,
			qt_dosagem       = nvl(qt_dosagem_final_w, qt_dosagem_w),
			qt_volume        = cpoe_obter_volume_total( cd_material, qt_dose, cd_unidade_medida,
								cd_mat_dil, qt_dose_dil, cd_unid_med_dose_dil,
								qt_solucao_red,
								cd_mat_red, qt_dose_red, cd_unid_med_dose_red,
								cd_mat_comp1, qt_dose_comp1, cd_unid_med_dose_comp1,
								cd_mat_comp2, qt_dose_comp2, cd_unid_med_dose_comp2,
								cd_mat_comp3, qt_dose_comp3, cd_unid_med_dose_comp3,
								cd_mat_comp4, qt_dose_comp4, cd_unid_med_dose_comp4,
								cd_mat_comp5, qt_dose_comp5, cd_unid_med_dose_comp5,
								cd_mat_comp6, qt_dose_comp6, cd_unid_med_dose_comp6,
								cd_mat_comp7, qt_dose_comp7, cd_unid_med_dose_comp7,
								null, cd_pessoa_fisica, nr_atendimento, ie_via_aplicacao,
								nr_seq_cpoe_mat_p)
	where	nr_sequencia 	= nr_seq_cpoe_mat_p;

	
	ds_ref_calculo_w := CPOE_obter_diluicao( ie_tipo_solucao_w, qt_volume_sol_w, qt_tempo_aplicacao_w, cd_intervalo_w, nr_etapas_w, to_char(qt_dosagem_final_w), qt_solucao_total_w,
	lpad(qt_hora_fase_w, 2, '0') || ':00', substr(qt_hora_fase_w, 0,2), null, ie_ref_calculo_w, null);

    update	cpoe_material
    set		ds_ref_calculo = ds_ref_calculo_w
    where	nr_sequencia = nr_seq_cpoe_mat_p;

	
	exception
	when others then
    ds_erro_w := sqlerrm(sqlcode);
	gravar_log_cpoe(substr('CPOE_CALC_SOLUTION_VALUES_RP  '|| substr(ds_erro_w, 1, 2000), 1,2000), null, 'I', nr_seq_cpoe_mat_p);
	
	commit;
end cpoe_calc_solution_values_rp;
/
