create or replace
procedure pls_consistir_guia_imp
		(		nr_seq_guia_p		number,
				cd_estabelecimento_p	number,
				nm_usuario_p		varchar2) is

cd_usuario_plano_w		varchar2(30);
ie_carater_internacao_imp_w	varchar2(1);
nr_seq_clinica_imp_w		varchar2(1);
ie_regime_internacao_imp_w	varchar2(1);
ds_conselho_profissional_imp_w	varchar2(255);
nr_crm_imp_w			varchar2(20);
uf_crm_imp_w			varchar2(2);
cd_cnes_imp_w			varchar2(20);
cd_guia_imp_w			varchar2(20);
nm_medico_solicitante_imp_w	varchar2(255);
cd_usuario_plano_imp_w		varchar2(30);
ds_plano_imp_w			varchar2(255);
dt_validade_cartao_imp_w	date;
nm_segurado_imp_w		varchar2(255);
cd_ans_imp_w			varchar2(20);
nm_prestador_imp_w		varchar2(255);
nr_seq_prestador_imp_w		varchar(10);
dt_emissao_imp_w		date;
cd_medico_w			number(10);
qt_benef_valido_w		number(5);
qt_prestador_valido_w		number(5) := 0;
dt_validade_carteira_w		date;
nm_segurado_w			varchar2(255);
dt_contratacao_w		date;
dt_rescisao_w			date;
cd_cnes_w			varchar2(20);
nm_prestador_w			varchar2(255);
nr_seq_segurado_w		number(10) := null;
nr_seq_plano_w			number(10) := null;
dt_solicitacao_imp_w		date;
qt_dia_autorizado_imp_w		varchar2(5);
cd_guia_principal_imp_w		varchar2(20);
ie_tipo_doenca_imp_w		varchar2(1);
ie_indicacao_acidente_imp_w	varchar2(1);
cd_doenca_imp_w			varchar2(10);
nr_seq_diagnostico_w		number(10);
ds_diagnostico_imp_w		varchar2(2000);
ie_classificacao_imp_w		varchar2(1);
nr_seq_guia_proc_w		number(10);
cd_procedimento_imp_w		number(15);
ds_procedimento_imp_w		varchar2(255);
qt_solicitada_imp_w		number(3);
cd_tipo_tabela_imp_w		varchar2(20);
ie_origem_proced_w		number(10);
nr_seq_guia_mat_w		number(10);
cd_material_imp_w		number(6);
ds_material_imp_w		varchar2(255);
qt_autorizada_imp_w		number(5);
nr_seq_tipo_acomodacao_w	number(10);
cd_tipo_acomodacao_imp_w	varchar2(10);
nr_prestador_valido_w		number(10);
ds_observacao_imp_w		varchar(2000);
ie_origem_solic_w		pls_guia_plano.ie_origem_solic%type;
ie_tipo_guia_w			pls_guia_plano.ie_tipo_guia%type;
cursor c01 is
	select	nr_sequencia,
		cd_procedimento_imp,
		ds_procedimento_imp,
		qt_solicitada_imp,
		cd_tipo_tabela_imp
	from	pls_guia_plano_proc
	where	nr_seq_guia	= nr_seq_guia_p;

cursor c02 is
	select	nr_sequencia,
		cd_material_imp,
		ds_material_imp,
		qt_solicitada_imp
	from	pls_guia_plano_mat
	where	nr_seq_guia	= nr_seq_guia_p;

cursor c03 is
	select	nr_sequencia,
		ie_tipo_doenca_imp,
		ie_indicacao_acidente_imp,
		cd_doenca_imp,
		ds_diagnostico_imp,
		ie_classificacao_imp
	from	pls_diagnostico
	where	nr_seq_guia	= nr_seq_guia_p;

begin

select	ie_carater_internacao_imp,
	nr_seq_clinica_imp,
	ie_regime_internacao_imp,
	ds_conselho_profissional_imp,
	nr_crm_imp,
	uf_crm_imp,
	cd_cnes_imp,
	cd_guia_imp,
	upper(elimina_acentuacao(nm_medico_solicitante_imp)),
	cd_usuario_plano_imp,
	upper(elimina_acentuacao(ds_plano_imp)),
	dt_validade_cartao_imp,
	nm_segurado_imp,
	cd_ans_imp,
	nm_prestador_imp,
	dt_emissao_imp,
	dt_solicitacao_imp,
	qt_dia_autorizado_imp,
	cd_guia_principal_imp,
	ds_observacao_imp,
	pls_obter_prestador_imp(cd_cgc_prestador_imp, cd_cpf_prestador_imp, nr_seq_prestador_imp, '', '', ''),
	ie_origem_solic,
	ie_tipo_guia
into	ie_carater_internacao_imp_w,
	nr_seq_clinica_imp_w,
	ie_regime_internacao_imp_w,
	ds_conselho_profissional_imp_w,
	nr_crm_imp_w,
	uf_crm_imp_w,
	cd_cnes_imp_w,
	cd_guia_imp_w,
	nm_medico_solicitante_imp_w,
	cd_usuario_plano_imp_w,
	ds_plano_imp_w,
	dt_validade_cartao_imp_w,
	nm_segurado_imp_w,
	cd_ans_imp_w,
	nm_prestador_imp_w,
	dt_emissao_imp_w,
	dt_solicitacao_imp_w,
	qt_dia_autorizado_imp_w,
	cd_guia_principal_imp_w,
	ds_observacao_imp_w,
	nr_seq_prestador_imp_w,
	ie_origem_solic_w,
	ie_tipo_guia_w
from	pls_guia_plano
where	nr_sequencia	= nr_seq_guia_p;

select	nvl(max(cd_usuario_plano),0)
into	cd_usuario_plano_w
from	pls_segurado_carteira
where	cd_usuario_plano = cd_usuario_plano_imp_w
and	dt_inicio_vigencia <= sysdate
and	nvl(dt_validade_carteira,sysdate) >= sysdate;

if	(cd_usuario_plano_w = 0) then
	pls_gravar_motivo_glosa('1001',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
else
	select	count(*)
	into	qt_benef_valido_w
	from	pls_segurado_carteira b,
		pls_segurado a
	where	a.nr_sequencia		= b.nr_seq_segurado
	and	b.cd_usuario_plano	= cd_usuario_plano_w
	and	nvl(dt_validade_carteira,sysdate) >= sysdate
	and	dt_inicio_vigencia <= sysdate;

	if	(qt_benef_valido_w = 1) then
		select	a.nr_sequencia,
			nvl(b.dt_validade_carteira,trunc(sysdate)),
			substr(elimina_acentuacao(upper(pls_obter_dados_segurado(a.nr_sequencia,'N'))),1,255),
			trunc(dt_contratacao_w),
			dt_rescisao,
			pls_obter_produto_benef(a.nr_sequencia, sysdate)
		into	nr_seq_segurado_w,
			dt_validade_carteira_w,
			nm_segurado_w,
			dt_contratacao_w,
			dt_rescisao_w,
			nr_seq_plano_w
		from	pls_segurado_carteira b,
			pls_segurado a
		where	a.nr_sequencia		= b.nr_seq_segurado
		and	b.cd_usuario_plano	= cd_usuario_plano_w;
	else
		pls_gravar_motivo_glosa('1017',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
	end if;

	select	somente_numero(nr_seq_prestador_imp_w)
	into	nr_prestador_valido_w
	from	dual;

	if	(nr_seq_prestador_imp_w <> to_char(nr_prestador_valido_w)) then
		pls_gravar_motivo_glosa('1203',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
	else
		select	nvl(max(nr_sequencia),0)
		into	qt_prestador_valido_w
		from	pls_prestador
		where	nr_sequencia	= nr_prestador_valido_w;

		if	(qt_prestador_valido_w > 0) then
			select	substr(pls_obter_cnes_prestador(nr_sequencia),1,255),
				substr(pls_obter_dados_prestador(nr_sequencia,'N'),1,255)
			into	cd_cnes_w,
				nm_prestador_w
			from	pls_prestador
			where	nr_sequencia	= nr_seq_prestador_imp_w;

			if	(cd_cnes_w <> cd_cnes_imp_w) then
				pls_gravar_motivo_glosa('1202',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
			end if;

			select	nvl(max(somente_numero(cd_medico)),0)
			into	cd_medico_w
			from	pls_prestador_medico
			where	nr_seq_prestador	= nr_seq_prestador_imp_w
			and	upper(elimina_acentuacao(substr(obter_nome_medico(cd_medico,'N'),1,255))) = nm_medico_solicitante_imp_w
			and	ie_situacao	= 'A'
			and	trunc(nvl(dt_solicitacao_imp_w,sysdate),'dd') between trunc(nvl(dt_inclusao,nvl(dt_solicitacao_imp_w,sysdate)),'dd') and  fim_dia(nvl(dt_exclusao,nvl(dt_solicitacao_imp_w,sysdate)));

			if	(cd_medico_w = 0) then
				pls_gravar_motivo_glosa('1210',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
			end if;

			if	(nm_medico_solicitante_imp_w = '') then
				pls_gravar_motivo_glosa('1411',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
			end if;
		else
			pls_gravar_motivo_glosa('1203',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
		end if;
	end if;

	open c01;
	loop
	fetch c01 into	
		nr_seq_guia_proc_w,
		cd_procedimento_imp_w,
		ds_procedimento_imp_w,
		qt_solicitada_imp_w,
		cd_tipo_tabela_imp_w;
	exit when c01%notfound;
		begin
		if	(cd_tipo_tabela_imp_w in ('01','02','03','04','07','08')) then
			ie_origem_proced_w	:= 1;
		elsif	(cd_tipo_tabela_imp_w = '06') then
			ie_origem_proced_w	:= 5;
		elsif	(cd_tipo_tabela_imp_w = '10') then
			ie_origem_proced_w	:= 3;
		elsif	(cd_tipo_tabela_imp_w = '11') then
			ie_origem_proced_w	:= 2;
		else
			ie_origem_proced_w	:= 4;
		end if;

		begin
		select	cd_procedimento
		into	cd_procedimento_imp_w
		from	procedimento
		where	cd_procedimento		= cd_procedimento_imp_w
		and	ie_origem_proced	= ie_origem_proced_w;
		exception
			when others then
			pls_gravar_motivo_glosa('1801',null,nr_seq_guia_proc_w,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
			cd_procedimento_imp_w	:= null;
			ie_origem_proced_w	:= null;
		end;

		update	pls_guia_plano_proc
		set	dt_atualizacao		= sysdate,
			ie_status		= 'U',
			nm_usuario		= nm_usuario_p,
			cd_procedimento		= cd_procedimento_imp_w,
			ie_origem_proced	= ie_origem_proced_w,
			qt_solicitada		= qt_solicitada_imp
		where	nr_sequencia		= nr_seq_guia_proc_w;
		end;
	end loop;
	close c01;

	/*
	open c02;
	loop
	fetch c02 into	
		nr_seq_guia_mat_w,
		cd_material_imp_w,
		ds_material_imp_w,
		qt_solicitada_imp_w;
	exit when c02%notfound;
	end loop;
	close c02;
	*/

	open c03;
	loop
	fetch c03 into
		nr_seq_diagnostico_w,
		ie_tipo_doenca_imp_w,
		ie_indicacao_acidente_imp_w,
		cd_doenca_imp_w,
		ds_diagnostico_imp_w,
		ie_classificacao_imp_w;
	exit when c03%notfound;
		begin
		if	(ie_tipo_doenca_imp_w <> 'A') and
			(ie_tipo_doenca_imp_w <> 'C') then
			pls_gravar_motivo_glosa('1502',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
			ie_tipo_doenca_imp_w	:= '';
		end if;

		if	(ie_indicacao_acidente_imp_w <> '0') and
			(ie_indicacao_acidente_imp_w <> '1') and
			(ie_indicacao_acidente_imp_w <> '2') and
			((ie_origem_solic_w <> 'E') or (ie_origem_solic_w = 'E' and ie_tipo_guia_w = 1)) then
			---Para as guias geradas pelo WebService ( ie_origem_solic = 'E') , s� dever� consistir a Glosa se o tipo de guia for de interna��o.
			pls_gravar_motivo_glosa('1503',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
			ie_indicacao_acidente_imp_w	:= '';
		end if;

		if	(cd_doenca_imp_w = '') then
			pls_gravar_motivo_glosa('1508',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
		else
			begin
			select	cd_doenca_cid
			into	cd_doenca_imp_w
			from	cid_doenca
			where	cd_doenca_cid	= cd_doenca_imp_w;
			exception
				when others then
					pls_gravar_motivo_glosa('1509',nr_seq_guia_p,null,null,'',nm_usuario_p,'','IG',nr_seq_prestador_imp_w, null,null);
					cd_doenca_imp_w	:=  null;
			end;
		end if;

		update	pls_diagnostico
		set	dt_atualizacao		= sysdate,
			nm_usuario		= nm_usuario_p,
			ie_tipo_doenca		= ie_tipo_doenca_imp_w,
			ie_indicacao_acidente	= ie_indicacao_acidente_imp_w,
			ie_classificacao	= ie_classificacao_imp_w,
			cd_doenca		= cd_doenca_imp_w,
			ds_diagnostico		= ds_diagnostico_imp_w
		where	nr_sequencia		= nr_seq_diagnostico_w;	
		end;
	end loop;
	close c03;
end if;

update	pls_guia_plano
set	nr_seq_prestador	= decode(qt_prestador_valido_w,0,null,nr_seq_prestador_imp_w),
	nr_seq_segurado		= nr_seq_segurado_w,
	nr_seq_plano		= decode(nr_seq_plano_w,0,null,nr_seq_plano_w),
	cd_medico_solicitante	= decode(cd_medico_w,0,null,to_char(cd_medico_w)),
	ie_carater_internacao	= ie_carater_internacao_imp_w,
	nr_seq_clinica		= decode(nr_seq_clinica_imp_w,null,null,somente_numero(nr_seq_clinica_imp_w)),
	ie_regime_internacao	= ie_regime_internacao_imp_w,
	cd_guia_prestador	= cd_guia_imp_w,
	dt_emissao		= dt_emissao_imp,
	dt_solicitacao		= dt_solicitacao_imp_w,
	qt_dia_solicitado	= qt_dia_autorizado_imp_w,
	cd_guia_principal	= cd_guia_principal_imp_w,
	ds_observacao		= ds_observacao_imp_w
where	nr_sequencia		= nr_seq_guia_p;

pls_consistir_guia(nr_seq_guia_p, cd_estabelecimento_p, nm_usuario_p);

commit;

end pls_consistir_guia_imp;
/