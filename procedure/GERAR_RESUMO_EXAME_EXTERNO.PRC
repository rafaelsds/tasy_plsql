create or replace
procedure gerar_resumo_exame_externo (cd_estabelecimento_p		number,
				nr_seq_consulta_p 		number,	
				cd_pessoa_fisica_p		varchar2,
				ie_mostra_todos_p		varchar2,
				nr_seq_item_p			number,
				nm_usuario_p			varchar2) as 


nm_profissional_w	varchar2(255);
ds_espaco_20_w		varchar(255) 	:=  lpad(' ',1,' ');
ds_enter_w		varchar2(30)	:=  chr(13) || chr(10);	
ds_observacao_w		varchar2(1000);
dt_registro_w		varchar2(30);
ds_exame_externo_w	varchar2(32000);
ds_exame_exter_od_w	varchar2(1000);
ds_exame_exter_oe_w	varchar2(1000);

expressao1_w	varchar2(255) := obter_desc_expressao_idioma(327202, null, wheb_usuario_pck.get_nr_seq_idioma);--DATA:
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(728918, null, wheb_usuario_pck.get_nr_seq_idioma);--PROFISSIONAL:
expressao3_w	varchar2(255) := obter_desc_expressao_idioma(289699, null, wheb_usuario_pck.get_nr_seq_idioma);--EXAME EXTERNO OD
expressao4_w	varchar2(255) := obter_desc_expressao_idioma(289700, null, wheb_usuario_pck.get_nr_seq_idioma);--EXAME EXTERNO OE
expressao5_w	varchar2(255) := obter_desc_expressao_idioma(294639, null, wheb_usuario_pck.get_nr_seq_idioma);--Observação

CURSOR C01 IS
SELECT	obter_nome_pf(cd_profissional) nm_profissional,
	TO_CHAR(dt_exame,'dd/mm/yyyy hh24:mi:ss'),
	ds_exame_exter_od, 
	ds_exame_exter_oe, 
	ds_observacao
FROM	OFT_EXAME_EXTERNO
WHERE  	((nr_seq_consulta = nr_seq_consulta_p) OR ((ie_mostra_todos_p = 'S') AND  nr_seq_consulta IN (SELECT c.nr_sequencia 
                                                                        	 	  	      FROM    atendimento_paciente b, 
                                                                              				      oft_consulta c 
                                                                       				      WHERE   c.nr_atendimento = b.nr_atendimento 
                                                                        			      AND    b.cd_pessoa_fisica = cd_pessoa_fisica_p)))
AND	NVL(ie_situacao,'A') = 'A'
ORDER   BY dt_exame desc;

begin

open C01;
loop
	fetch C01 into
		nm_profissional_w,
		dt_registro_w,
		ds_exame_exter_od_w,
		ds_exame_exter_oe_w,
		ds_observacao_w;
	exit when C01%notfound;
	begin
	ds_exame_externo_w	:=	'';
	ds_exame_externo_w	:= 	ds_exame_externo_w || upper(expressao1_w) || ' ' || dt_registro_w || ds_espaco_20_w ||
								upper(expressao2_w) || ' ' ||nm_profissional_w || ds_enter_w ||ds_enter_w;
	if	(ds_exame_exter_od_w is not null) then
		ds_exame_externo_w	:= ds_exame_externo_w || upper(expressao3_w) || ': ' || ds_exame_exter_od_w ||ds_enter_w;
	end if;
	
	if	(ds_exame_exter_oe_w is not null) then
		ds_exame_externo_w	:= ds_exame_externo_w || upper(expressao4_w) || ': ' || ds_exame_exter_oe_w ||ds_enter_w;
	end if;
	
	ds_exame_externo_w := ds_exame_externo_w || ds_enter_w || upper(expressao5_w) || ': ' || ds_observacao_w ||ds_enter_w;
		
	gravar_registro_resumo_oft(ds_exame_externo_w,nr_seq_item_p,nm_usuario_p,to_date(dt_registro_w,'dd/mm/yyyy hh24:mi:ss'));
	end;
end loop;
close C01;	

commit;

end gerar_resumo_exame_externo;
/
