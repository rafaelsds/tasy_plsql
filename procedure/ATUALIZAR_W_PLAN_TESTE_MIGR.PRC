create or replace
procedure atualizar_w_plan_teste_migr(
		nr_sequencia_p	number,
		nm_usuario_p	varchar2) is

ie_aprovada_w	varchar2(1);
begin
if	(nr_sequencia_p is not null) and
	(nm_usuario_p is not null) then
	begin
	select	decode(nvl(dt_aprovacao,''),'','N','S')
	into	ie_aprovada_w
	from	w_plan_teste_migr
	where	nr_sequencia = nr_sequencia_p;
	
	if	(ie_aprovada_w = 'N') then
		begin
		update	w_plan_teste_migr
		set	dt_aprovacao	= sysdate,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_sequencia_p;
		end;
	else
		begin
		update	w_plan_teste_migr
		set	dt_aprovacao	= null,
			nm_usuario	= nm_usuario_p,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_sequencia_p;
		end;
	end if;
	end;
end if;
commit;
end atualizar_w_plan_teste_migr;
/