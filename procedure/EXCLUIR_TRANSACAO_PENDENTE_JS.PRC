create or replace
procedure excluir_transacao_pendente_js(				
		nr_sequencia_p	number) is 

begin
if	(nr_sequencia_p is not null) then
	begin
	delete 
	from 	concil_banc_pend_tasy
	where 	nr_sequencia = nr_sequencia_p;
	commit;
	end;
end if;

end excluir_transacao_pendente_js;
/