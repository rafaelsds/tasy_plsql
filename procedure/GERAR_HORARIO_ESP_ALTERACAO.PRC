create or replace
procedure gerar_horario_esp_alteracao	(cd_estabelecimento_p	number,
						cd_tipo_agenda_p		number,
						cd_agenda_p			number,
						dt_agenda_p			date,
						nm_usuario_p			varchar2) is

ie_dia_semana_w		number(5,0);
ie_feriado_w			varchar2(1);
hr_inicial_intervalo_w	date;
hr_final_intervalo_w		date;
hr_inicial_w			date;
hr_final_w			date;
nr_minuto_duracao_w		number(10,0);
cd_medico_req_w		varchar2(10);
nr_seq_medico_exec_w		number(10,0);
nr_seq_sala_w			number(10,0);
nr_seq_classif_w		number(10,0);
ds_observacao_w		varchar2(4000);
nr_seq_especial_w		number(10,0);

nr_seq_proc_interno_w	agenda_horario.nr_seq_proc_interno%type;
cd_procedimento_w	agenda_horario.cd_procedimento%type;
ie_origem_proced_w	agenda_horario.ie_origem_proced%type;

cursor c01 is
select	hr_inicio,
	hr_inicio + nr_minuto_duracao / 1440,
	nr_minuto_duracao,
	cd_medico,
	obter_reg_executor_agenda_med(cd_agenda_p, cd_medico_exec) nr_seq_medico_exec,
	nr_seq_sala,
	nr_seq_classif_agenda,
	ds_observacao
from	agenda_paciente
where	cd_agenda = cd_agenda_p
and	trunc(dt_agenda,'dd') = trunc(dt_agenda_p,'dd')
and	ie_status_agenda not in ('F','I','II','LF','C')
order by
	hr_inicio;

begin
if	(cd_tipo_agenda_p is not null) and
	(cd_agenda_p is not null) and
	(dt_agenda_p is not null) and
	(nm_usuario_p is not null) then
	/* agenda exame */
	if	(cd_tipo_agenda_p = 2) then
		/* obter dia semana */
		select	obter_cod_dia_semana(dt_agenda_p)
		into	ie_dia_semana_w
		from	dual;

		/* verificar feriado */
		select	decode(count(*),0,'N','S')
		into	ie_feriado_w
		from 	feriado a, 
			agenda b
		where 	a.cd_estabelecimento = cd_estabelecimento_p
		and	a.dt_feriado = dt_agenda_p
		and 	b.cd_agenda = cd_agenda_p;

		

		/* eliminar horarios gerados anteriormente */
		delete
		from	agenda_horario_esp
		where	cd_agenda = cd_agenda_p
		and	dt_agenda = dt_agenda_p
		and	ie_agendamento = 'S';

		/* gerar hor�rios especiais agendamento */
		open c01;
		loop
		fetch c01 into	hr_inicial_w,
					hr_final_w,
					nr_minuto_duracao_w,
					cd_medico_req_w,
					nr_seq_medico_exec_w,
					nr_seq_sala_w,
					nr_seq_classif_w,
					ds_observacao_w;
		exit when c01%notfound;
			begin
			
			/* obter dados horarios agenda */
			select	max(hr_inicial_intervalo),
				max(hr_final_intervalo),
				max(nr_seq_proc_interno), 
				max(cd_procedimento), 
				max(ie_origem_proced)
			into	hr_inicial_intervalo_w,
				hr_final_intervalo_w,
				nr_seq_proc_interno_w,
				cd_procedimento_w,
				ie_origem_proced_w
			from	agenda_horario
			where	cd_agenda = cd_agenda_p
			and		PKG_DATE_UTILS.get_DateTime(hr_inicial,hr_inicial_w) between hr_inicial and hr_final
			and	((dt_inicio_vigencia is null) or (dt_inicio_vigencia <= trunc(dt_agenda_p)))
			and	((dt_final_vigencia is null) or (dt_final_vigencia >= trunc(dt_agenda_p)))
			and		nr_minuto_intervalo > 0
			and	((dt_dia_semana = ie_dia_semana_w) or (dt_dia_semana = 9))
			and	((ie_feriado_w <> 'S') or (obter_se_agenda_feriado(cd_agenda_p) = 'S'));
		
			/* obter sequence */
			select	agenda_horario_esp_seq.nextval
			into	nr_seq_especial_w
			from	dual;

			/* gerar horarios */
			insert into agenda_horario_esp	(
								nr_sequencia,
								cd_agenda,
								dt_agenda,
								dt_atualizacao,
								nm_usuario,
								hr_inicial,
								hr_final,
								nr_minuto_intervalo,
								hr_inicial_intervalo,
								hr_final_intervalo,
								cd_medico,
								nr_seq_medico_exec,
								nr_seq_sala,
								dt_atualizacao_nrec,
								nm_usuario_nrec,
								ds_observacao,
								ie_horario_adicional,
								nr_seq_classif_agenda,
								ie_agendamento,
								nr_seq_proc_interno,
								cd_procedimento,
								ie_origem_proced
								)
			values					(
								nr_seq_especial_w,
								cd_agenda_p,
								dt_agenda_p,
								sysdate,
								nm_usuario_p,
								hr_inicial_w,
								hr_final_w,
								nr_minuto_duracao_w,
								hr_inicial_intervalo_w,
								hr_final_intervalo_w,
								cd_medico_req_w,
								nr_seq_medico_exec_w,
								nr_seq_sala_w,
								sysdate,
								nm_usuario_p,
								substr(ds_observacao_w,1,255),
								'S',
								nr_seq_classif_w,
								'S',
								nr_seq_proc_interno_w,
								cd_procedimento_w,
								ie_origem_proced_w
								);
			end;
		end loop;
		close c01;
	end if;
end if;

commit;

end gerar_horario_esp_alteracao;
/