create or replace procedure PFCS_DETAILED_TELE_CENSUS(
    nr_seq_indicator_p   number,
    cd_estabelecimento_p varchar2,
    nm_usuario_p         varchar2) is
	
	nr_seq_panel_w 						pfcs_panel.nr_sequencia%type;
	pfcs_flag_settings          		number(15) := 0;

	ds_planned_status					varchar2(15) := 'PLANNED';
	ds_arrived_status					varchar2(15) := 'ARRIVED';
	ds_active_status_w					varchar2(15) := 'ACTIVE';
	ds_inactive_status					varchar2(15) := 'INACTIVE';

    ie_matching_status					varchar2(5) := 'S';
	ie_requested_status 				varchar2(5) := 'R';

	ds_monitor_dev_type_w 				varchar2(10) := 'Monitor';
	ds_service_requested				varchar2(10) := 'E0404';
    ie_active_w 							varchar2(1) := '1';

	-- census constant
	ds_complete_status				varchar2(15) := 'COMPLETED';
	ds_service_completed			varchar2(10) := 'E0404';
	ds_service_requested_use     	varchar2(10) := 'E0403';


    --- available device
    ds_dev_unknown_status_w			varchar2(15) := 'UNKNOWN';
	ds_dev_inactive_status_w		varchar2(15) := 'INACTIVE';
    ds_tl_dev_type 					varchar2(10) := 'TL';
    ie_started_status 				varchar2(5) :='S';
	ie_finished_status 				varchar2(5) :='F';

/* telemetry Census = Patients on Tele / Total  Devices */
/* Cursor to read from pfcs integration tables */
cursor c01_pfcs is
	select ds_unit,
	sum(qt_patients_census_w) qt_patients_census_w,
	sum(qt_device_available_w) qt_device_available_w
    from (
        select	loc.ds_department ds_unit,
            count(1) qt_patients_census_w,
			0 qt_device_available_w
			from	pfcs_device dev,
					pfcs_location loc,
					pfcs_organization org
			where	dev.si_status = ds_active_status_w
			and		dev.ds_device_type = ds_monitor_dev_type_w
			and		loc.nr_sequencia = dev.nr_seq_location
			and		dev.nr_seq_organization = org.nr_sequencia
			and		org.cd_estabelecimento = to_number(cd_estabelecimento_p)
			group by loc.ds_department
	union all
	select
		loc.ds_department ds_unit,
		0 qt_patients_census_w,
		count(1) qt_device_available_w
	from
		pfcs_device dev,
		pfcs_location loc,
		pfcs_organization org
	where dev.ds_device_type = ds_monitor_dev_type_w
		and dev.nr_seq_location = loc.nr_sequencia
		and dev.nr_seq_organization = org.nr_sequencia
		and (dev.si_status not in ds_dev_unknown_status_w or dev.si_status is null)
		and	loc.si_status = ds_active_status_w
		and pfcs_get_bed_status(loc.cd_operational_status, 'C', cd_estabelecimento_p, 'Y') = 'A'
		and org.cd_estabelecimento = to_number(cd_estabelecimento_p)
		group by loc.ds_department
	union all
	select	Obter_desc_expressao(344145) ds_unit,
			0 qt_patients_census_w,
			count(1) qt_device_available_w
	from	pfcs_device dev,
			pfcs_organization org
	where	dev.si_status in (ds_dev_inactive_status_w)
	and		dev.ds_device_type = ds_monitor_dev_type_w
	and		dev.nr_seq_organization = org.nr_sequencia
	and		dev.nr_seq_location is null
	and		org.cd_estabelecimento = to_number(cd_estabelecimento_p)
	having count(1) > 0
	)
	group by ds_unit;

begin

	for c01_w in c01_pfcs loop
	begin
	  pfcs_pck_v2.pfcs_generate_results(
			vl_indicator_p => c01_w.qt_patients_census_w,
			vl_indicator_aux_p => c01_w.qt_device_available_w,
			ds_reference_value_p => c01_w.ds_unit,
			nr_seq_indicator_p => nr_seq_indicator_p,
			nr_seq_operational_level_p => to_number(cd_estabelecimento_p),
			nm_usuario_p => nm_usuario_p,
			nr_seq_panel_p => nr_seq_panel_w);
	end;
	end loop;

	commit;

	pfcs_pck_v2.pfcs_activate_records(
		nr_seq_indicator_p => nr_seq_indicator_p,
		nr_seq_operational_level_p => to_number(cd_estabelecimento_p),
		nm_usuario_p => nm_usuario_p);

end PFCS_DETAILED_TELE_CENSUS;
/
