create or replace
procedure man_excluir_ordem_servico_par(	nr_ordem_servico_p	number,
					nm_usuario_p		Varchar2) is 

begin

delete from man_doc_erro
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_ativ_prev
where	nr_seq_ordem_serv		= nr_ordem_servico_p;

delete from man_ordem_justificativa
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_servico_exec
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ord_serv_orc_item
where	nr_seq_orc_ordem in (	select	nr_sequencia
				from 	man_ordem_servico_orc
				where	nr_seq_ordem = nr_ordem_servico_p);

delete from man_ordem_servico_orc
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_serv_arq
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_serv_ativ
where	nr_seq_ordem_serv		= nr_ordem_servico_p;

delete from man_ordem_serv_ciente
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_serv_custo
where	nr_seq_ordem_serv		= nr_ordem_servico_p;

delete from man_ordem_serv_envio
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_serv_estagio
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_serv_externo
where	nr_seq_ordem_serv		= nr_ordem_servico_p;

delete from man_ordem_serv_gestao
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_serv_hist
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_serv_prio
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_serv_problema
where	nr_seq_ordem_serv		= nr_ordem_servico_p;

delete from man_ordem_serv_proj_gpi
where	nr_seq_ordem_serv		= nr_ordem_servico_p;

delete from man_ordem_serv_sla
where	nr_seq_ordem 		= nr_ordem_servico_p;

delete from man_ordem_serv_tecnico
where	nr_seq_ordem_serv		= nr_ordem_servico_p;

delete from man_ordem_servico_parada
where	nr_seq_ordem_serv		= nr_ordem_servico_p;

delete from man_ordem_servico
where	nr_sequencia		= nr_ordem_servico_p;

/*insert into logxxx_tasy(	dt_atualizacao,
		nm_usuario,
		cd_log,
		ds_log)
	values(	sysdate,
		nm_usuario_p,
		55792,
		'Ordem de servi�o ' || nr_ordem_servico_p || ' exclu�da.' || chr(13) ||
		'Procedure man_excluir_ordem_servico.');*/
end man_excluir_ordem_servico_par;
/
