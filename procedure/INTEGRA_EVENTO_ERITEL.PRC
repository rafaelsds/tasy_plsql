create or replace
procedure Integra_evento_Eritel(
			nr_ramal_p		number,
			cd_usuario_p		varchar2,
			cd_evento_p		number,
			cd_estabelecimento_p	number,
			cd_unidade_basica_p	varchar2 default null,
			cd_unidade_compl_p	varchar2 default null) is 

nm_usuario_w			varchar2(15);
cd_unidade_basica_w		varchar2(10);
cd_unidade_compl_w		varchar2(10);
cd_setor_atendimento_w		number(5);
nr_seq_gv_w			number(10);
evt_integ_w			number(10);
cd_pessoa_fisica_w		varchar2(10);
nr_seq_interno_w		number(10);
ie_aguarda_hig_w		varchar2(1);
ie_verificar_pac_leito_w	varchar2(1);
ie_pac_leito_w			varchar2(1) := 'N';
qt_tempo_prev_higien_w	number(3);
ie_status_anterior_isol_w	varchar2(1);
ie_status_anterior_hig_w	varchar2(3);
nr_seq_unidade_w		number(10);
nr_atendimento_w		number(10);
ie_paciente_isolado_w	varchar2(1);
ie_permite_aprov_antes_fim_w	varchar2(1);
dt_aprovacao_w		date;
dt_fim_w			date;
ie_local_liberacao_leito_w	varchar2(1);

Cursor C01 is
	select	qt_tempo_prev_higien
	from	regra_integracao_eritel 
	where	ie_isolamento = ie_status_anterior_isol_w
	and		nvl(ie_situacao,'A') = 'A'
	order by 1;

begin

Select	max(a.cd_unidade_basica),
	max(a.CD_UNIDADE_COMPL),
	max(a.cd_setor_atendimento)
into	cd_unidade_basica_w,
	cd_unidade_compl_w,
	cd_setor_atendimento_w	
from	setor_Atendimento b,
	unidade_atendimento a
where	a.nr_ramal = nr_ramal_p
and    	a.ie_situacao = 'A'
and	a.cd_setor_atendimento = b.cd_setor_atendimento
and	b.cd_estabelecimento = cd_estabelecimento_p;

if (cd_setor_atendimento_w is null) and (cd_unidade_basica_p is not null) and (cd_unidade_compl_p is not null) then
	begin
		select 	a.nr_seq_interno
		into	nr_seq_interno_w
		from	setor_Atendimento b,
			unidade_atendimento a
		where 	cd_unidade_basica = cd_unidade_basica_p
		and 	cd_unidade_compl = cd_unidade_compl_p		
		and    	a.ie_situacao = 'A'
		and	a.cd_setor_atendimento = b.cd_setor_atendimento
		and	b.cd_estabelecimento = cd_estabelecimento_p;		
				
		Select	max(a.cd_unidade_basica),
			max(a.CD_UNIDADE_COMPL),
			max(a.cd_setor_atendimento)
		into	cd_unidade_basica_w,
			cd_unidade_compl_w,
			cd_setor_atendimento_w	
		from	setor_Atendimento b,
			unidade_atendimento a
		where	a.cd_setor_atendimento = b.cd_setor_atendimento
		and	a.nr_seq_interno = nr_seq_interno_w;	
	exception
	when others then
		nr_seq_interno_w := null;
	end;
end if;

Select	max(a.nm_usuario),
	max(b.cd_pessoa_fisica)
into	nm_usuario_w,
	cd_pessoa_fisica_w
from	usuario a,
	pessoa_fisica b
where 	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	b.cd_funcionario	= cd_usuario_p;

select 	nvl(max(ie_evento_integracao),0)
into 	evt_integ_w
from 	regra_evento_integr_ramal
where 	ie_empresa_integ = 1
and    	cd_evento = cd_evento_p;

/* Regras
	
	1	    Inicio de higieniza��o        
	2           Fim de higieniza��o           
	3           In�cio de limpeza concorrente  
	4           Fim de limpeza concorrente    
	5           Inicio de limpeza sala cir�rig 
	6           Fim de limpeza sala cir�rgica  
	7           Chamado manuten��o           
	8           Inicio manuten��o            
	9           Fim de manuten��o            
	10          Aprovar Servi�o
	*/

obter_param_usuario(3111, 266,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_p,ie_verificar_pac_leito_w);	
obter_param_usuario(45, 145,obter_perfil_ativo,nm_usuario_w,cd_estabelecimento_p,ie_aguarda_hig_w);
Obter_param_Usuario(75, 111, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_p, ie_permite_aprov_antes_fim_w);
Obter_param_Usuario(75, 114, obter_perfil_ativo, nm_usuario_w, cd_estabelecimento_p, ie_local_liberacao_leito_w);

if	((cd_evento_p in (13,14)) or ((evt_integ_w > 0) and (evt_integ_w = 1))) then
	begin
	
	if (ie_verificar_pac_leito_w = 'N') then
	
		select	decode(count(*),0,'N','S')
		into	ie_pac_leito_w
		from	unidade_atendimento
		where	cd_unidade_basica	= cd_unidade_basica_w
		and	cd_unidade_compl	= cd_unidade_compl_w
		and	cd_setor_atendimento	= cd_setor_atendimento_w
		and	nr_atendimento is not null;
		
		if (ie_pac_leito_w = 'S') then
			Wheb_mensagem_pck.exibir_mensagem_abort(238384);
		end if;
	end if;
	
	SELECT	MAX(nr_seq_interno)
	INTO  	nr_seq_unidade_w
	FROM  	unidade_atendimento
	WHERE	cd_unidade_basica		= cd_unidade_basica_w
	AND		cd_unidade_compl		= cd_unidade_compl_w
	AND		cd_setor_atendimento	= cd_setor_atendimento_w;
	
	SELECT	MAX(nr_atendimento)
	INTO	nr_atendimento_w
	FROM	unidade_atend_hist a
	WHERE	a.nr_seq_unidade = nr_seq_unidade_w
	AND		a.dt_historico = (	SELECT 	MAX(x.dt_historico)
								FROM	unidade_atend_hist x
								WHERE	a.nr_seq_unidade = x.nr_seq_unidade
								AND		x.ie_status_unidade <> 'A'
							 );
							 
	select	max(ie_paciente_isolado)
	into	ie_paciente_isolado_w
	from	atendimento_paciente
	where	nr_atendimento = nr_atendimento_w;
	
	if	(ie_paciente_isolado_w = 'S') then
		ie_status_anterior_isol_w	:= 'S';
	else
		ie_status_anterior_isol_w	:= 'N';
	end if;	
	
	open C01;
	loop
	fetch C01 into	
		qt_tempo_prev_higien_w;
	exit when C01%notfound;
		begin
		qt_tempo_prev_higien_w	:= qt_tempo_prev_higien_w;
		end;
	end loop;
	close C01;
		
	Update	Unidade_atendimento
	set	nm_usuario_higienizacao	= nm_usuario_w,
		dt_inicio_higienizacao	= sysdate,
		dt_higienizacao		= null,
		ie_status_unidade	= 'H',
		nm_usuario		= nvl(nm_usuario_w,nm_usuario),
		qt_tempo_prev_higien	= nvl(qt_tempo_prev_higien_w,'')
	where	cd_unidade_basica	= cd_unidade_basica_w
	and	cd_unidade_compl	= cd_unidade_compl_w
	and	cd_setor_atendimento	= cd_setor_atendimento_w;
	
	commit;

	select 	max(b.nr_sequencia)
	into	nr_seq_gv_w
	from	unidade_atendimento a,
		sl_unid_atend b
	where	trunc(b.dt_prevista) = trunc(sysdate)
	and 	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	a.cd_unidade_basica = cd_unidade_basica_w
	and	a.cd_unidade_compl = cd_unidade_compl_w
	and	b.nr_seq_unidade = a.nr_seq_interno
	and	b.dt_inicio is null;
	
	if (nr_seq_gv_w > 0) then
	gerar_dados_gestao_servico(nr_seq_gv_w, sysdate,sysdate,cd_pessoa_fisica_w,'I','',cd_pessoa_fisica_w,null,null,null,evt_integ_w);
	end if;
	end;
	
elsif	((cd_evento_p = 15) or ((evt_integ_w > 0) and (evt_integ_w = 2))) then
	begin
	
	if (nvl(ie_local_liberacao_leito_w,'A') = 'F') then
		if (ie_verificar_pac_leito_w = 'N') then
		
			select	decode(count(*),0,'N','S')
			into	ie_pac_leito_w
			from	unidade_atendimento
			where	cd_unidade_basica		= cd_unidade_basica_w
			and		cd_unidade_compl		= cd_unidade_compl_w
			and		cd_setor_atendimento	= cd_setor_atendimento_w
			and		nr_atendimento is not null;
			
			if (ie_pac_leito_w = 'S') then
				Wheb_mensagem_pck.exibir_mensagem_abort(238384);
			end if;
		end if;	
			
						
		Update	Unidade_atendimento
		set		nm_usuario_fim_higienizacao	= nm_usuario_w,
				dt_higienizacao				= sysdate,
				ie_status_unidade			= decode(nvl(cd_paciente_reserva,nm_pac_reserva), null, 'L', 'R'),
				nm_usuario					= nvl(nm_usuario_w,nm_usuario)
		where	cd_unidade_basica			= cd_unidade_basica_w
		and		cd_unidade_compl			= cd_unidade_compl_w
		and		cd_setor_atendimento		= cd_setor_atendimento_w;
		
		commit;


	elsif (nvl(ie_local_liberacao_leito_w,'A') = 'A') then
	
		Update	Unidade_atendimento
		set		nm_usuario_higienizacao	= nm_usuario_w,
				dt_inicio_higienizacao	= sysdate,
				dt_higienizacao			= null,
				ie_status_unidade		= 'H',
				nm_usuario				= nvl(nm_usuario_w,nm_usuario),
				qt_tempo_prev_higien	= nvl(qt_tempo_prev_higien_w,'')
		where	cd_unidade_basica		= cd_unidade_basica_w
		and		cd_unidade_compl		= cd_unidade_compl_w
		and		cd_setor_atendimento	= cd_setor_atendimento_w
		and		ie_status_unidade		<> 'H';
		
		commit;
		
	end if;	
	
	select 	max(b.nr_sequencia)
	into	nr_seq_gv_w
	from	unidade_atendimento a,
			sl_unid_atend b
	where	trunc(b.dt_prevista) 	= trunc(sysdate)
	and 	a.cd_setor_atendimento 	= cd_setor_atendimento_w
	and		a.cd_unidade_basica 	= cd_unidade_basica_w
	and		a.cd_unidade_compl 		= cd_unidade_compl_w
	and		b.nr_seq_unidade 		= a.nr_seq_interno
	and		b.dt_inicio is not null
	and 	b.dt_fim is null;
	
	if (nr_seq_gv_w > 0) then
		gerar_dados_gestao_servico(nr_seq_gv_w, sysdate,sysdate,cd_pessoa_fisica_w,'F','',cd_pessoa_fisica_w,null,null,null,evt_integ_w);
	end if;	
	
	end;

elsif	((evt_integ_w > 0) and (evt_integ_w = 7)) then
	Update	Unidade_atendimento
	set	ie_status_unidade	= 'C',
		nm_usuario		= nvl(nm_usuario_w,nm_usuario)
	where	cd_unidade_basica	= cd_unidade_basica_w
	and	cd_unidade_compl	= cd_unidade_compl_w
	and	cd_setor_atendimento	= cd_setor_atendimento_w
	and	nr_atendimento is null;

elsif	((evt_integ_w > 0) and (evt_integ_w = 8)) then
	Update	Unidade_atendimento
	set	ie_status_unidade	= 'E',
		nm_usuario		= nvl(nm_usuario_w,nm_usuario)
	where	cd_unidade_basica	= cd_unidade_basica_w
	and	cd_unidade_compl	= cd_unidade_compl_w
	and	cd_setor_atendimento	= cd_setor_atendimento_w
	and	nr_atendimento is null;

elsif	((evt_integ_w > 0) and (evt_integ_w = 9)) then
	

	if (ie_aguarda_hig_w = 'S') then
		Update	Unidade_atendimento
		set	ie_status_unidade	= 'G',
			nm_usuario		= nvl(nm_usuario_w,nm_usuario)
		where	cd_unidade_basica	= cd_unidade_basica_w
		and	cd_unidade_compl	= cd_unidade_compl_w
		and	cd_setor_atendimento	= cd_setor_atendimento_w
		and	nr_atendimento is null;
	else
		Update	Unidade_atendimento
		set	ie_status_unidade	= decode(nvl(cd_paciente_reserva,nm_pac_reserva), null, 'L', 'R'),
			nm_usuario		= nvl(nm_usuario_w,nm_usuario)
		where	cd_unidade_basica	= cd_unidade_basica_w
		and	cd_unidade_compl	= cd_unidade_compl_w
		and	cd_setor_atendimento	= cd_setor_atendimento_w
		and	nr_atendimento is null;
	end if;
	
	commit;
	
	select nr_seq_interno
	into nr_seq_interno_w
	from unidade_atendimento
	where	cd_unidade_basica    = cd_unidade_basica_w
	and	cd_unidade_compl     = cd_unidade_compl_w
	and 	cd_setor_atendimento = cd_setor_atendimento_w;

	gerar_higienizacao_leito_unid(sysdate, nm_usuario_w,cd_estabelecimento_p, 'FM', nr_seq_interno_w, null);		

elsif	((evt_integ_w > 0) and (evt_integ_w in (3,5))) then
	begin
	select max(b.nr_sequencia)
	into	nr_seq_gv_w
	from	unidade_atendimento a,
		sl_unid_atend b
	where	trunc(b.dt_prevista) = trunc(sysdate)
	and 	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	a.cd_unidade_basica = cd_unidade_basica_w
	and	a.cd_unidade_compl = cd_unidade_compl_w
	and	b.nr_seq_unidade = a.nr_seq_interno
	and	b.dt_inicio is null
	and 	b.dt_fim is null
	and dt_prevista =(
		select 	min(b.dt_prevista)
		from	unidade_atendimento a,
			sl_unid_atend b
		where	trunc(b.dt_prevista) = trunc(sysdate)
		and 	a.cd_setor_atendimento = cd_setor_atendimento_w
		and	a.cd_unidade_basica = cd_unidade_basica_w
		and	a.cd_unidade_compl = cd_unidade_compl_w
		and	b.nr_seq_unidade = a.nr_seq_interno
		and	b.dt_inicio is null
		and 	b.dt_fim is null);
	
	if (nr_seq_gv_w > 0) then
	gerar_dados_gestao_servico(nr_seq_gv_w, sysdate,sysdate,cd_pessoa_fisica_w,'I','',cd_pessoa_fisica_w,null,null,null,evt_integ_w);
	end if;
	end;

elsif	((evt_integ_w > 0) and (evt_integ_w in (4,6))) then
	begin
	select 	max(b.nr_sequencia)
	into	nr_seq_gv_w
	from	unidade_atendimento a,
		sl_unid_atend b
	where	trunc(b.dt_prevista) = trunc(sysdate)
	and 	a.cd_setor_atendimento = cd_setor_atendimento_w
	and	a.cd_unidade_basica = cd_unidade_basica_w
	and	a.cd_unidade_compl = cd_unidade_compl_w
	and	b.nr_seq_unidade = a.nr_seq_interno
	and	b.dt_inicio is not null
	and 	b.dt_fim is null;
	
	if (nr_seq_gv_w > 0) then
	gerar_dados_gestao_servico(nr_seq_gv_w, sysdate,sysdate,cd_pessoa_fisica_w,'F','',cd_pessoa_fisica_w,null,null,null,evt_integ_w);
	end if;
	end;	
	
elsif ((cd_evento_p = 16) or ((evt_integ_w > 0) and (evt_integ_w = 10))) then

	select 	max(b.dt_fim),
			max(b.dt_aprovacao),
			max(nr_sequencia)
	into	dt_fim_w,
			dt_aprovacao_w,
			nr_seq_gv_w
	from	unidade_atendimento a,
			sl_unid_atend b
	where	trunc(b.dt_prevista) = trunc(sysdate)
	and 	a.cd_setor_atendimento = cd_setor_atendimento_w
	and		a.cd_unidade_basica = cd_unidade_basica_w
	and		a.cd_unidade_compl = cd_unidade_compl_w
	and		b.nr_seq_unidade = a.nr_seq_interno
	and		b.dt_inicio is not null;
	
	if 	((ie_permite_aprov_antes_fim_w = 'N') and
		(dt_aprovacao_w is not null) and
		(dt_aprovacao_w < dt_fim_w)) then 
		wheb_mensagem_pck.exibir_mensagem_abort(728462, 'DS_DATA_MENOR=' || obter_desc_expressao(286713) || 
								';DS_DATA_MAIOR=' || obter_desc_expressao(286879) || 
								';NR_PARAMETRO=111'); -- Data de aprova��o n�o pode ser inferior a Data fim! Par�metro [111].
	end if;
	
	if (nvl(ie_local_liberacao_leito_w,'A') = 'A') then
		if (ie_verificar_pac_leito_w = 'N') then
		
			select	decode(count(*),0,'N','S')
			into	ie_pac_leito_w
			from	unidade_atendimento
			where	cd_unidade_basica		= cd_unidade_basica_w
			and		cd_unidade_compl		= cd_unidade_compl_w
			and		cd_setor_atendimento	= cd_setor_atendimento_w
			and		nr_atendimento is not null;
			
			if (ie_pac_leito_w = 'S') then
				Wheb_mensagem_pck.exibir_mensagem_abort(238384);
			end if;
		end if;	
			
		Update	Unidade_atendimento
		set		nm_usuario_fim_higienizacao	= nm_usuario_w,
				dt_higienizacao				= sysdate,
				ie_status_unidade			= decode(nvl(cd_paciente_reserva,nm_pac_reserva), null, 'L', 'R'),
				nm_usuario					= nvl(nm_usuario_w,nm_usuario)
		where	cd_unidade_basica			= cd_unidade_basica_w
		and		cd_unidade_compl			= cd_unidade_compl_w
		and		cd_setor_atendimento		= cd_setor_atendimento_w;
		
		commit;

		select 	max(b.nr_sequencia)
		into	nr_seq_gv_w
		from	unidade_atendimento a,
				sl_unid_atend b
		where	trunc(b.dt_prevista) 	= trunc(sysdate)
		and 	a.cd_setor_atendimento 	= cd_setor_atendimento_w
		and		a.cd_unidade_basica 	= cd_unidade_basica_w
		and		a.cd_unidade_compl 		= cd_unidade_compl_w
		and		b.nr_seq_unidade 		= a.nr_seq_interno
		and		b.dt_inicio is not null
		and 	b.dt_fim is null;
		
		if (nr_seq_gv_w > 0) then
		gerar_dados_gestao_servico(nr_seq_gv_w, sysdate,sysdate,cd_pessoa_fisica_w,'F','',cd_pessoa_fisica_w,null,null,null,evt_integ_w);
		end if;
	elsif (nvl(ie_local_liberacao_leito_w,'A') = 'F') then
		if (nr_seq_gv_w > 0) then
			gerar_dados_gestao_servico(nr_seq_gv_w, sysdate,sysdate,cd_pessoa_fisica_w,'V','',cd_pessoa_fisica_w,null,null,null,evt_integ_w);
		end if;
	end if;
end if;

commit;

end Integra_evento_Eritel;
/