create or replace
procedure sip_gerar_item_assist
			(	nr_seq_lote_sip_p	Number,
				nm_usuario_p		Varchar2) is 
				
dt_periodo_inicial_w		Date;
dt_periodo_final_w		Date;
nr_seq_item_sip_w		Number(10);
ie_evento_w			Varchar2(1);
ie_benef_carencia_w		Varchar2(1);
ie_despesa_w			Varchar2(1);
ie_tipo_contratacao_w		Varchar2(10);
ie_segmentacao_sip_w		Number(3);
sg_uf_sip_w			Varchar2(2);
qt_evento_w			Number(9,3);
qt_beneficiario_w		Number(10);
vl_despesa_w			Number(15,2);
nr_seq_apres_w			Number(10);
cd_classificacao_w		Varchar2(20);
cd_classificacao_sip_w		Varchar2(20);
nr_seq_superior_w		Number(10);
qt_registro_w			Number(5)	:= 0;
ie_conta_w			Varchar2(1);
qt_sip_lote_item_w		number(10);
nr_sip_lote_item_assis_w	number(10);
nr_seq_superior_ww		number(10);
ie_ambulatorial_w		varchar2(1);
ie_hospitalar_w			varchar2(1);
ie_hospitalar_obs_w		varchar2(1);
ie_odontologico_w		varchar2(1);
nr_seq_sip_item_w		number(10);
qt_lote_item_assist_w		number(10);
qt_evento_ww			Number(9,3);
qt_beneficiario_ww 		Number(10);
vl_despesa_ww			Number(15,2);

qt_evento_mat_w			Number(9,3);
qt_beneficiario_mat_w		Number(10);
vl_despesa_mat_w		Number(15,2);
nr_seq_item_assis_w		Number(10);

Cursor C01 is
	select	nr_sequencia,
		ie_evento,
		ie_benef_carencia,
		ie_despesa,
		nr_seq_apres,
		cd_classificacao,
		nr_seq_superior
	from	sip_item_assistencial;
	
Cursor C02 is
	select  nvl(sum(qt_evento),0),
		nvl(sum(1),0),
		nvl(sum(vl_despesa),0),
		ie_tipo_contratacao,
		ie_segmentacao,
		sg_uf
	from	sip_mov_item_assistencial
	where	trunc(dt_item_sip) between inicio_dia(dt_periodo_inicial_w) and fim_dia(dt_periodo_final_w)
	and	nr_seq_item_sip	= nr_seq_item_sip_w
	group by 
		 ie_tipo_contratacao,
		 ie_segmentacao,
		 sg_uf;
/* Somar as estruturas filhas do SIP */
Cursor C03 is
	select	nr_seq_superior,
		ie_tipo_contratacao,
		ie_segmentacao_sip,
		sg_uf
	from	sip_lote_item_assistencial
	where	nr_seq_lote	= nr_seq_lote_sip_p
	and	nr_seq_superior	is not null
	group by 	nr_seq_superior,
			ie_tipo_contratacao,
			ie_segmentacao_sip,
			sg_uf
	order by nvl(nr_seq_superior,0)	desc;

/* Somar a quantidade de eventos de internado,
Adicionada  a estrutura F - Cousas internas pois deve ter o mesmo tratamento de interna��o 1 vez internado n�o importa a quantidade de procedimento
Ajustado esta rotina no 1 cursor*/
/*Cursor C04 is
	select	nr_seq_item_sip,
		ie_tipo_contratacao,
		ie_segmentacao_sip,
		sg_uf
	from	sip_lote_item_assistencial
	where	nr_seq_lote		= nr_seq_lote_sip_p
	and	((substr(cd_classificacao_sip,1,1) = 'E')
	or	(substr(cd_classificacao_sip,1,1) = 'F'))
	order by nr_seq_superior desc;*/
	
Cursor C04 is
	select	sg_uf
	from	sip_lote_item_assistencial
	where	nr_seq_lote	= nr_seq_lote_sip_p
	group by sg_uf;

/* Gerar as estruturas que n�o havia regra dentro da mesma segmenta��o */
Cursor C05 is
	select	a.nr_sequencia,
		b.ie_segmentacao_sip,
		c.ie_tipo_contratacao,
		a.ie_evento,
		a.ie_benef_carencia,
		a.ie_despesa,
		a.nr_seq_apres,
		a.cd_classificacao,
		a.nr_seq_superior,
		a.ie_ambulatorial,
		a.ie_hospitalar,
		a.ie_hospitalar_obs,
		a.ie_odontologico
	from	sip_item_assistencial a,
		(select	vl_dominio	ie_segmentacao_sip
		from	valor_dominio
		where	cd_dominio	= 3543)	b,
		(select	vl_dominio	ie_tipo_contratacao
		from	valor_dominio
		where	cd_dominio	= 1666) c
	order by  nvl(nr_seq_superior,0) desc;
	
begin
delete	sip_lote_item_assistencial
where	nr_seq_lote	= nr_seq_lote_sip_p;

/*delete	logxxxx_tasy
where	cd_log = 12310;*/

begin
select	dt_periodo_inicial,
	nvl(dt_periodo_final, sysdate)
into	dt_periodo_inicial_w,
	dt_periodo_final_w
from	pls_lote_sip
where	nr_sequencia	= nr_seq_lote_sip_p;
exception
	when others then
	wheb_mensagem_pck.exibir_mensagem_abort('Problema na leitura dos dados do lote SIP (' || nr_seq_lote_sip_p || ')');
end;
open C01;
loop
fetch C01 into	
	nr_seq_item_sip_w,
	ie_evento_w,
	ie_benef_carencia_w,
	ie_despesa_w,
	nr_seq_apres_w,
	cd_classificacao_w,
	nr_seq_superior_w;
exit when C01%notfound;
	begin
	
	select	count(*)
	into	qt_registro_w
	from	sip_item_assistencial;	
	
	/*insert into log_tasy(dt_atualizacao, nm_usuario, cd_log, ds_log)
	values (sysdate, nm_usuario_p, 12310, cd_classificacao_w || ' - ' || nr_seq_item_sip_w || ' (' || qt_registro_w || ')');*/

	open C02;
	loop
	fetch C02 into	
		qt_evento_w,
		qt_beneficiario_w,
		vl_despesa_w,
		ie_tipo_contratacao_w,
		ie_segmentacao_sip_w,
		sg_uf_sip_w;
	exit when C02%notfound;
		begin
		if	(qt_evento_w <> 0) then
			begin
			select	max(nr_sequencia)
			into	nr_seq_item_assis_w
			from	sip_lote_item_assistencial
			where	nr_seq_lote		= nr_seq_lote_sip_p
			and	nr_seq_item_sip		= nr_seq_item_sip_w
			and	ie_tipo_contratacao	= ie_tipo_contratacao_w
			and	ie_segmentacao_sip	= ie_segmentacao_sip_w
			and	sg_uf			= sg_uf_sip_w;
			exception
			when others then
				nr_seq_item_assis_w	:= 0;
			end;
			if	(nvl(nr_seq_item_assis_w,0) = 0) then
				insert into sip_lote_item_assistencial
					(nr_sequencia, nr_seq_lote, sg_uf,
					qt_evento, qt_beneficiario, vl_despesa,
					dt_atualizacao, nm_usuario, nr_seq_item_sip,
					ie_evento, ie_benef_carencia, ie_despesa,
					nr_seq_apres, ie_tipo_contratacao, ie_segmentacao_sip,
					cd_classificacao_sip, nr_seq_superior)
				values(	sip_lote_item_assistencial_seq.nextval, nr_seq_lote_sip_p, sg_uf_sip_w, --sg_uf_sip_w
					qt_evento_w, qt_beneficiario_w, vl_despesa_w,
					sysdate, nm_usuario_p, nr_seq_item_sip_w,
					ie_evento_w, ie_benef_carencia_w, ie_despesa_w,
					nr_seq_apres_w, ie_tipo_contratacao_w, ie_segmentacao_sip_w,
					cd_classificacao_w, nr_seq_superior_w);
			elsif	(nvl(nr_seq_item_assis_w,0) <> 0) then
				update sip_lote_item_assistencial
				set	qt_beneficiario	= qt_beneficiario_w,
					qt_evento	= qt_evento + qt_evento_w,
					vl_despesa	= vl_despesa + vl_despesa_w
				where	nr_sequencia	= nr_seq_item_assis_w
				and	ie_tipo_contratacao	= ie_tipo_contratacao_w
				and	ie_segmentacao_sip	= ie_segmentacao_sip_w
				and	sg_uf			= sg_uf_sip_w
				and	nr_seq_lote		= nr_seq_lote_sip_p;
			end if;
		end if;
		end;
	end loop;
	close C02;
	end;
end loop;
close C01;

open C04;
loop
fetch C04 into	
	sg_uf_sip_w;
exit when C04%notfound;
	begin
	open C05;
	loop
	fetch C05 into	
		nr_seq_sip_item_w,
		ie_segmentacao_sip_w,
		ie_tipo_contratacao_w,
		ie_evento_w,
		ie_benef_carencia_w,
		ie_despesa_w,
		nr_seq_apres_w,
		cd_classificacao_w,
		nr_seq_superior_w,
		ie_ambulatorial_w,
		ie_hospitalar_w,
		ie_hospitalar_obs_w,
		ie_odontologico_w;
	exit when C05%notfound;
		begin
		/*para ver se n�o existe nos item com os dados ie_tipo_contratacao, nr_seq_item_sip e ie_segmentacao_sip no lote e que sej� da mesma classifica��o*/
		select	count(*)
		into	qt_lote_item_assist_w
		from	sip_lote_item_assistencial
		where	nr_seq_lote		= nr_seq_lote_sip_p
		and	ie_tipo_contratacao	= ie_tipo_contratacao_w
		and	nr_seq_item_sip		= nr_seq_sip_item_w
		and	ie_segmentacao_sip	= ie_segmentacao_sip_w
		and	sg_uf			= sg_uf_sip_w
		and	substr(cd_classificacao_sip,1,1) = substr(cd_classificacao_w,1,1);

		if (qt_lote_item_assist_w = 0) then
			if	(ie_segmentacao_sip_w	= 1) and
				(ie_ambulatorial_w	= 'S') then
				--if	(substr(cd_classificacao_w,1,1) <> 'F') then
					select	count(*)
					into	qt_lote_item_assist_w
					from	sip_lote_item_assistencial
					where	nr_seq_lote		= nr_seq_lote_sip_p
					and	ie_tipo_contratacao	= ie_tipo_contratacao_w
					and	nr_seq_item_sip		<> nr_seq_sip_item_w
					and	ie_segmentacao_sip	= ie_segmentacao_sip_w
					and	substr(cd_classificacao_sip,1,1) = substr(cd_classificacao_w,1,1)
					and	sg_uf			= sg_uf_sip_w
					and	vl_despesa	> 0;
				/*else
					qt_lote_item_assist_w	:= 1;
				end if;*/
				/*para ver se existe em outras classifica��o nos item com os dados ie_tipo_contratacao, nr_seq_item_sip e ie_segmentacao_sip no lote e que sej� da mesma classifica��o*/
				if 	(qt_lote_item_assist_w > 0) then
					insert into sip_lote_item_assistencial
						(nr_sequencia, nr_seq_lote, sg_uf,
						qt_evento, qt_beneficiario, vl_despesa,
						dt_atualizacao, nm_usuario, nr_seq_item_sip,
						ie_evento, ie_benef_carencia, ie_despesa,
						nr_seq_apres, ie_tipo_contratacao, ie_segmentacao_sip,
						cd_classificacao_sip, nr_seq_superior)
					values(	sip_lote_item_assistencial_seq.nextval, nr_seq_lote_sip_p, sg_uf_sip_w,
						0, 0, 0,
						sysdate, nm_usuario_p, nr_seq_sip_item_w,
						ie_evento_w, ie_benef_carencia_w, ie_despesa_w,
						nr_seq_apres_w, ie_tipo_contratacao_w, ie_segmentacao_sip_w,
						cd_classificacao_w, nr_seq_superior_w); 
				end if;
			elsif	(ie_segmentacao_sip_w	= 2) and
				(ie_hospitalar_w	= 'S')  then

				--if	(substr(cd_classificacao_w,1,1) <> 'F') then
					select	count(*)
					into	qt_lote_item_assist_w
					from	sip_lote_item_assistencial
					where	nr_seq_lote		= nr_seq_lote_sip_p
					and	ie_tipo_contratacao	= ie_tipo_contratacao_w
					and	nr_seq_item_sip		<> nr_seq_sip_item_w
					and	ie_segmentacao_sip	= 3
					and	substr(cd_classificacao_sip,1,1) = substr(cd_classificacao_w,1,1)
					and	sg_uf			= sg_uf_sip_w
					and	vl_despesa	> 0;
				/*else
					qt_lote_item_assist_w	:= 1;
				end if;*/

				if (qt_lote_item_assist_w > 0) then
					insert into sip_lote_item_assistencial
						(nr_sequencia, nr_seq_lote, sg_uf,
						qt_evento, qt_beneficiario, vl_despesa,
						dt_atualizacao, nm_usuario, nr_seq_item_sip,
						ie_evento, ie_benef_carencia, ie_despesa,
						nr_seq_apres, ie_tipo_contratacao, ie_segmentacao_sip,
						cd_classificacao_sip, nr_seq_superior)
					values(	sip_lote_item_assistencial_seq.nextval, nr_seq_lote_sip_p, sg_uf_sip_w,
						0, 0, 0,
						sysdate, nm_usuario_p, nr_seq_sip_item_w,
						ie_evento_w, ie_benef_carencia_w, ie_despesa_w,
						nr_seq_apres_w, ie_tipo_contratacao_w, 2,
						cd_classificacao_w, nr_seq_superior_w);
				end if;
			elsif	(ie_segmentacao_sip_w	= 3) and
				(ie_hospitalar_obs_w	= 'S') then
				
				--if	(substr(cd_classificacao_w,1,1) <> 'F') then
					select	count(*)
					into	qt_lote_item_assist_w
					from	sip_lote_item_assistencial
					where	nr_seq_lote		= nr_seq_lote_sip_p
					and	ie_tipo_contratacao	= ie_tipo_contratacao_w
					and	nr_seq_item_sip		<> nr_seq_sip_item_w
					and	ie_segmentacao_sip	= ie_segmentacao_sip_w
					and	substr(cd_classificacao_sip,1,1) = substr(cd_classificacao_w,1,1)
					and	sg_uf			= sg_uf_sip_w
					and	vl_despesa	> 0;
				/*else
					qt_lote_item_assist_w	:= 1;
				end if;*/

				if (qt_lote_item_assist_w > 0) then
					insert into sip_lote_item_assistencial
						(nr_sequencia, nr_seq_lote, sg_uf,
						qt_evento, qt_beneficiario, vl_despesa,
						dt_atualizacao, nm_usuario, nr_seq_item_sip,
						ie_evento, ie_benef_carencia, ie_despesa,
						nr_seq_apres, ie_tipo_contratacao, ie_segmentacao_sip,
						cd_classificacao_sip, nr_seq_superior)
					values(	sip_lote_item_assistencial_seq.nextval, nr_seq_lote_sip_p, sg_uf_sip_w,
						0, 0, 0,
						sysdate, nm_usuario_p, nr_seq_sip_item_w,
						ie_evento_w, ie_benef_carencia_w, ie_despesa_w,
						nr_seq_apres_w, ie_tipo_contratacao_w, ie_segmentacao_sip_w,
						cd_classificacao_w, nr_seq_superior_w);
				end if;
			elsif	(ie_segmentacao_sip_w	= 4) and
				(ie_odontologico_w	= 'S') then

				--if	(substr(cd_classificacao_w,1,1) <> 'F') then
					select	count(*)
					into	qt_lote_item_assist_w
					from	sip_lote_item_assistencial
					where	nr_seq_lote		= nr_seq_lote_sip_p
					and	ie_tipo_contratacao	= ie_tipo_contratacao_w
					and	nr_seq_item_sip		<> nr_seq_sip_item_w
					and	ie_segmentacao_sip	= ie_segmentacao_sip_w
					and	substr(cd_classificacao_sip,1,1) = substr(cd_classificacao_w,1,1)
					and	sg_uf			= sg_uf_sip_w
					and	vl_despesa	> 0;
				/*else
					qt_lote_item_assist_w	:= 1;
				end if;*/

				if (qt_lote_item_assist_w > 0) then
					insert into sip_lote_item_assistencial
						(nr_sequencia, nr_seq_lote, sg_uf,
						qt_evento, qt_beneficiario, vl_despesa,
						dt_atualizacao, nm_usuario, nr_seq_item_sip,
						ie_evento, ie_benef_carencia, ie_despesa,
						nr_seq_apres, ie_tipo_contratacao, ie_segmentacao_sip,
						cd_classificacao_sip, nr_seq_superior)
					values(	sip_lote_item_assistencial_seq.nextval, nr_seq_lote_sip_p, sg_uf_sip_w,
						0, 0, 0,
						sysdate, nm_usuario_p, nr_seq_sip_item_w,
						ie_evento_w, ie_benef_carencia_w, ie_despesa_w,
						nr_seq_apres_w, ie_tipo_contratacao_w, ie_segmentacao_sip_w,
						cd_classificacao_w, nr_seq_superior_w);
				end if;
			end if;
		end if;
		
		end;
	end loop;
	close C05;
	end;
end loop;
close C04;

/*open C04;
loop
fetch C04 into	
	nr_seq_item_sip_w,
	ie_tipo_contratacao_w,
	ie_segmentacao_sip_w,
	sg_uf_sip_w;
exit when C04%notfound;
	begin
	/*select	count(*)
	into	qt_evento_w
	from	pls_protocolo_conta	b,
		pls_conta		a
	where	a.nr_seq_protocolo	= b.nr_sequencia
	and	pls_obter_se_internado(a.nr_sequencia,'') = 'S'
	and	trunc(b.dt_mes_competencia) between dt_periodo_inicial_w and fim_dia(dt_periodo_final_w)
	and	nr_seq_item_sip_w in (	select	x.nr_seq_item_sip
					from	pls_conta_proc	x 
					where	x.nr_seq_conta		= a.nr_sequencia
					and	x.ie_tipo_contratacao	= ie_tipo_contratacao_w
					and	x.ie_segmentacao_sip	= ie_segmentacao_sip_w
					and	x.sg_uf_sip		= sg_uf_sip_w);
					
	select	count(*)
	into	qt_evento_w
	from	(select	distinct a.cd_guia_referencia, 
			a.nr_seq_segurado 
		from	pls_conta	a,
			pls_protocolo_conta	b
		where	b.nr_sequencia	= a.nr_seq_protocolo
		and	((a.ie_tipo_guia = '5') or ((a.ie_tipo_guia = '4') and (a.nr_seq_tipo_atendimento = 7)))
		and	trunc(b.dt_mes_competencia) between dt_periodo_inicial_w and fim_dia(dt_periodo_final_w)
		and	a.nr_sequencia in (	select	x.nr_seq_conta
							from	pls_conta_proc	x 
							where	x.nr_seq_conta		= a.nr_sequencia
							and	x.ie_tipo_contratacao	= ie_tipo_contratacao_w
							and	x.ie_segmentacao_sip	= ie_segmentacao_sip_w
							and	x.sg_uf_sip		= sg_uf_sip_w
							and	x.nr_seq_item_sip	= nr_seq_item_sip_w));
					
	update	sip_lote_item_assistencial
	set	qt_evento		= nvl(qt_evento_w,0)
	where	nr_seq_lote		= nr_seq_lote_sip_p
	and	nr_seq_item_sip		= nr_seq_item_sip_w
	and	ie_tipo_contratacao	= ie_tipo_contratacao_w
	and	ie_segmentacao_sip	= ie_segmentacao_sip_w
	and	sg_uf			= sg_uf_sip_w;
	end;
end loop;
close C04;*/


open C03;
loop
fetch C03 into
	nr_seq_superior_w,
	ie_tipo_contratacao_w,
	ie_segmentacao_sip_w,
	sg_uf_sip_w;
exit when C03%notfound;
	begin
	select	nvl(sum(qt_evento),0), 
		nvl(sum(qt_beneficiario),0), 
		nvl(sum(vl_despesa),0)
	into	qt_evento_w, 
		qt_beneficiario_w, 
		vl_despesa_w
	from	sip_lote_item_assistencial
	where	nr_seq_lote	= nr_seq_lote_sip_p
	and	nr_seq_superior	= nr_seq_superior_w
	and	ie_tipo_contratacao	= ie_tipo_contratacao_w
	and	ie_segmentacao_sip	= ie_segmentacao_sip_w
	and	sg_uf			= sg_uf_sip_w;

	update	sip_lote_item_assistencial
	set	qt_evento	= nvl(qt_evento,0) + qt_evento_w, 
		qt_beneficiario	= nvl(qt_beneficiario,0) + qt_beneficiario_w,
		vl_despesa	= nvl(vl_despesa,0) + vl_despesa_w
	where	nr_seq_lote	= nr_seq_lote_sip_p
	and	nr_seq_item_sip	= nr_seq_superior_w
	and	ie_tipo_contratacao	= ie_tipo_contratacao_w
	and	ie_segmentacao_sip	= ie_segmentacao_sip_w
	and	sg_uf			= sg_uf_sip_w;
	
	commit;
	end; 
end loop;
close C03;
sip_gerar_benef_fora_carencia(nr_seq_lote_sip_p, nm_usuario_p);
commit;

end sip_gerar_item_assist;
/