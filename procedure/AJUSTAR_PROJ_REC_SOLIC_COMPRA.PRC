create or replace
procedure ajustar_proj_rec_solic_compra(	nr_solic_compra_p 		number,
					nr_seq_projeto_anterior_p	number,
					nr_seq_projeto_novo_p	number,
					nm_usuario_p		Varchar2) is

nr_sequencia_hist_w	number(10);

ds_historico_anterior_w	varchar2(2000);
ds_historico_novo_w	varchar2(2000);

/* Campos Projeto */
ds_projeto_anterior_w	varchar2(80);
ds_projeto_novo_w		varchar2(80);

begin

update	solic_compra_item
set	nr_seq_proj_rec = nr_seq_projeto_novo_p
where	nr_seq_proj_rec is not null
and	nr_solic_compra = nr_solic_compra_p;

select	ds_projeto
into	ds_projeto_anterior_w
from	projeto_recurso
where	nr_sequencia = nr_seq_projeto_anterior_p;

select	ds_projeto
into	ds_projeto_novo_w
from	projeto_recurso
where	nr_sequencia = nr_seq_projeto_novo_p;

if	(nvl(nr_seq_projeto_novo_p,0) <> nvl(nr_seq_projeto_anterior_p,0)) then
	begin
	/* Cria um hist�rico no novo projeto */
	/*
	ds_historico_novo_w := 	'Altera��o do Projeto Recurso ' || chr(13) || chr(10) || chr(13) || chr(10) ||
				'Solicita��o de compras nr�: ' || nr_solic_compra_p || chr(13) || chr(10) ||
				'Projeto recurso anterior: ' || ds_projeto_anterior_w || chr(13) || chr(10) ||
				'Projeto recurso atual: ' || ds_projeto_novo_w;*/
	ds_historico_novo_w := 	wheb_mensagem_pck.get_texto(297046,	'NR_SOLIC_COMPRA=' || nr_solic_compra_p ||
						';NR_PROJ_ANTERIOR=' || ds_projeto_anterior_w ||
						';NR_PROJ_ATUAL=' || ds_projeto_novo_w);

	select	projeto_recurso_hist_seq.nextval	
	into	nr_sequencia_hist_w
	from	dual;	

	insert into projeto_recurso_hist (	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_projeto,
					ie_geracao,
					ds_historico,
					dt_liberacao)
		values			(nr_sequencia_hist_w,
					sysdate,
					nm_usuario_p,
					nr_seq_projeto_novo_p,
					'S',
					ds_historico_novo_w,
					sysdate);

	/* Cria um hist�rico  no antigo projeto */
	/*
	ds_historico_anterior_w := 	'Altera��o do Projeto Recurso ' || chr(13) || chr(10) || chr(13) || chr(10) ||
				'Solicita��o de compras nr�: ' || nr_solic_compra_p || chr(13) || chr(10) ||
				'Projeto recurso anterior: ' || ds_projeto_anterior_w || chr(13) || chr(10) ||
				'Projeto recurso atual: ' || ds_projeto_novo_w;*/
	ds_historico_anterior_w := 	wheb_mensagem_pck.get_texto(297046,	'NR_SOLIC_COMPRA=' || nr_solic_compra_p ||
						';NR_PROJ_ANTERIOR=' || ds_projeto_anterior_w ||
						';NR_PROJ_ATUAL=' || ds_projeto_novo_w);

	select	projeto_recurso_hist_seq.nextval
	into	nr_sequencia_hist_w
	from	dual;

	insert into projeto_recurso_hist (	nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_seq_projeto,
					ie_geracao,
					ds_historico,
					dt_liberacao)
		values			(nr_sequencia_hist_w,
					sysdate,
					nm_usuario_p,
					nr_seq_projeto_anterior_p,
					'S',
					ds_historico_anterior_w,
					sysdate);
	end;
end if;

commit;

end ajustar_proj_rec_solic_compra;
/