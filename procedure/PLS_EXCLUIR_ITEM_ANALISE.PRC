create or replace
procedure pls_excluir_item_analise
		      ( ds_sequencia_itens_p 	Varchar2,
			cd_estabelecimento_p  	number,
			nr_seq_analise_p 	number,
			nm_usuario_p    	Varchar2  ) is 

ds_sequencia_itens_w  	 	varchar2(4000);
nr_seq_item_w     	 	number(10);
nr_seq_item_ww      		number(10);
ie_tipo_item_w      		varchar2(1);
nr_seq_proc_w      		number(10);
nr_seq_mat_w      		number(10);
nr_seq_analise_conta_item_w  	number(10);
nr_seq_proc_partic_w    	number(10);
nr_seq_partic_w      		number(10);
ie_origem_analise_w    		number(1);
nr_seq_conta_proc_w    		number(10);
nr_seq_conta_ww      		number(10);
qt_participante_w		Number(10) 	:= 0;

Cursor C01 is
  select  	nr_sequencia
  from  	pls_analise_conta_item
  where  	nr_seq_w_resumo_conta = nr_seq_item_w
  and  		nr_seq_analise  = nr_seq_analise_p
  order by 1;

Cursor C02 is
  select  	nr_sequencia
  from  	pls_proc_participante
  where  	nr_seq_conta_proc  = nr_seq_proc_w
  order by 1;
  
Cursor C03 is
  select  	nr_sequencia
  from  	pls_analise_conta_item
  where  	nr_seq_proc_partic  = nr_seq_proc_partic_w
  and  		nr_seq_analise    = nr_seq_analise_p
  order by 1;
Cursor C04 is
  select  nr_sequencia
  from  pls_analise_conta_item
  where  ((nr_seq_conta_proc   = nr_seq_proc_w)
  or   (nr_seq_conta_mat    = nr_seq_mat_w)
  or   (nr_seq_proc_partic  = nr_seq_partic_w))  
  and  nr_seq_analise  = nr_seq_analise_p
  order by 1;
  
begin
select  max(ie_origem_analise)
into  	ie_origem_analise_w
from  	pls_analise_conta
where  	nr_sequencia = nr_seq_analise_p;

ds_sequencia_itens_w := ds_sequencia_itens_p;

while  (instr(ds_sequencia_itens_w,',') <> 0) loop
  begin      
 
 
  nr_seq_item_w    := substr(ds_sequencia_itens_w,1,instr(ds_sequencia_itens_w,',')-1); 
  ds_sequencia_itens_w  := substr(ds_sequencia_itens_w,instr(ds_sequencia_itens_w,',')+1,255);
  
  select  nr_seq_item,
	  ie_tipo_item,
	  nr_seq_partic_proc,
	  nr_Seq_conta
  into    nr_seq_item_ww,
	  ie_tipo_item_w,
	  nr_seq_partic_w,
	  nr_Seq_conta_ww
  from  w_pls_resumo_conta
  where  nr_sequencia = nr_seq_item_w
  and  nr_seq_analise  = nr_seq_analise_p;
  
  if  (ie_tipo_item_w = 'P') then
    nr_seq_proc_w  := nr_seq_item_ww;
    nr_seq_mat_w  := null;
    nr_seq_partic_w  := null;
  elsif  (ie_tipo_item_w = 'M') then
    nr_seq_mat_w   := nr_seq_item_ww;
    nr_seq_proc_w  := null;
    nr_seq_partic_w  := null;
  elsif  (ie_tipo_item_w = 'R') then
    select  max(nr_seq_conta_proc)
    into  nr_seq_conta_proc_w
    from  pls_proc_participante
    where  nr_sequencia = nr_seq_partic_w;
  
    nr_seq_mat_w   := null;
    nr_seq_proc_w  := null;    
  end if;

  open C01;
  loop
  fetch C01 into  
    nr_seq_analise_conta_item_w;
  exit when C01%notfound;
    begin 
    delete  pls_analise_parecer_item
    where  nr_seq_item = nr_seq_analise_conta_item_w;

    delete  pls_analise_conta_item_log
    where  nr_seq_analise_item = nr_seq_analise_conta_item_w;  
    
    delete  pls_analise_conta_item
    where  nr_sequencia = nr_seq_analise_conta_item_w;
    
    end;
  end loop;
  close C01; 
  
  open C04;
  loop
  fetch C04 into  
    nr_seq_analise_conta_item_w;
  exit when C04%notfound;
    begin
    
    delete  pls_analise_parecer_item
    where  nr_seq_item = nr_seq_analise_conta_item_w;

    delete  pls_analise_conta_item_log
    where  nr_seq_analise_item = nr_seq_analise_conta_item_w;  
    
    delete  pls_analise_conta_item
    where  nr_sequencia = nr_seq_analise_conta_item_w;
    
    end;
  end loop;
  close C04;  
  
     commit;
  if  (ie_tipo_item_w in ('P', 'M')) then    
    delete  w_pls_resumo_conta
    where  (nr_seq_conta_proc = nr_seq_proc_w or nr_seq_conta_mat = nr_seq_mat_w)
    and  nr_seq_analise  = nr_seq_analise_p;    
  elsif  (ie_tipo_item_w = 'R') then
    delete  w_pls_resumo_conta
    where  nr_seq_partic_proc   = nr_seq_partic_w
    and  nr_seq_analise    = nr_seq_analise_p;
  end if;  
  
  delete  pls_ocorrencia_benef
  where  nr_seq_guia_plano   is null
  and  nr_seq_requisicao   is null
  and  ((nr_seq_proc     = nr_seq_proc_w)
  or   (nr_seq_mat      = nr_seq_mat_w)
  or   (nr_seq_proc_partic  = nr_seq_partic_w));  
  
  delete  pls_conta_glosa
  where  ((nvl(nr_seq_conta_proc,0) = nr_seq_proc_w)
  or   (nvl(nr_seq_conta_mat,0)  = nr_seq_mat_w));
  
  open C02;
  loop
  fetch C02 into  
    nr_seq_proc_partic_w;
  exit when C02%notfound;
    begin
    
    open C03;
    loop
    fetch C03 into  
      nr_seq_analise_conta_item_w;
    exit when C03%notfound;
      begin
      
      delete  pls_analise_parecer_item
      where  nr_seq_item = nr_seq_analise_conta_item_w;
      
      end;
    end loop;
    close C03;
    
    delete  pls_analise_conta_item
    where  nr_seq_proc_partic  = nr_seq_proc_partic_w;
    
    delete  pls_ocorrencia_benef  
    where  nr_seq_guia_plano   is null
    and  nr_seq_requisicao   is null
    and  nr_seq_conta_pos_estab  is null
    and  nr_seq_proc_partic  = nr_seq_proc_partic_w;  
    
    delete  pls_conta_glosa
    where  nr_seq_proc_partic  = nr_seq_proc_partic_w;
    
    delete pls_analise_fluxo_item
    where  nr_seq_proc_partic = nr_seq_proc_partic_w;
    
    delete  pls_proc_participante
    where  nr_sequencia  = nr_seq_proc_partic_w;
    
    end;
  end loop;
  close C02;  
  
  /*vai sincronizar a analise pos-estabelecido para que n�o haja inconsistencia de integridade durante a dele��o*/
  pls_atualizar_conta_pos_estab( nr_Seq_conta_ww, nr_seq_proc_w,nr_seq_mat_w, nr_seq_analise_p,'N',cd_estabelecimento_p, nm_usuario_p);
  
  if	(ie_tipo_item_w = 'P')	then
  
	pls_delete_conta_medica_resumo(nr_seq_conta_ww,nr_seq_proc_w,null,nm_usuario_P);

  elsif	(ie_tipo_item_w = 'M')	then
	pls_delete_conta_medica_resumo(nr_seq_conta_ww,null,nr_seq_mat_w,nm_usuario_P);
  end if;

  delete pls_analise_fluxo_item
  where	 nr_seq_conta_proc = nr_seq_proc_w;
  
  delete  pls_conta_proc
  where  nr_sequencia =  nr_seq_proc_w;
  
  delete pls_analise_fluxo_item
  where	 nr_seq_conta_mat = nr_seq_mat_w;
  
  delete  pls_conta_mat
  where  nr_sequencia =  nr_seq_mat_w;
  
  delete pls_analise_fluxo_item
  where	 nr_seq_proc_partic = nr_seq_proc_partic_w;
  
  delete  pls_proc_participante
  where  nr_sequencia  = nr_seq_partic_w;
  
  if  (ie_origem_analise_w = 3) and
    (ie_tipo_item_w = 'R') then    
    pls_remover_proc_partic_ptu(nr_seq_item_ww, cd_estabelecimento_p, nm_usuario_p);
  end if;
  
  end;  
end loop;

commit;

end pls_excluir_item_analise;
/