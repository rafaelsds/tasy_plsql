create or replace
procedure gerar_cheque_adiant_pago(	nr_adiantamento_p	in 	number,
					cd_banco_p		in	number,
					cd_agencia_p		in	varchar2,
					dt_emissao_p		in	date,
					nr_seq_conta_banco_p	in	number,
					nr_cheque_inicial_p	in	number,
					vl_maximo_p		in	number,
					nr_seq_trans_fin_p	in	number,
					nm_usuario_p		in	varchar2,
					ie_baixa_titulo_p   in varchar2 default null,
                    ds_utilizacao_p     in varchar2 default null) is

cd_cgc_estabelecimento_w	varchar2(14);
cd_cgc_w			varchar2(14);
cd_pessoa_fisica_w		varchar2(10);
vl_saldo_w			number(15,2);
cd_estabelecimento_w		number(3);
nr_cheque_w			varchar2(15);
nr_seq_cheque_w			number(10);
nr_seq_adiant_pago_cheque_w	number(10);
vl_maximo_w			number(15,2);
vl_unit_cheque_w		number(15,2);
qt_dias_vencimento_w		number(10);
ie_vincular_cheque_movto_w	varchar2(10);
nr_seq_movto_trans_financ_w	number(10);
/* Projeto Multimoeda - Vari�veis*/
vl_saldo_estrang_w		number(15,2);
vl_un_cheque_estrang_w		number(15,2);
vl_adto_estrang_w		number(15,2);
vl_complemento_w		number(15,2);
vl_cotacao_w			cotacao_moeda.vl_cotacao%type;
cd_moeda_w			number(5);
nr_titulo_gerado_w  number(10);
vl_tributo_w		number(15,2);
	
begin

nr_cheque_w	:= nr_cheque_inicial_p;

select	a.cd_cgc,
	cd_pessoa_fisica,
	nvl(vl_saldo,0),
	a.cd_estabelecimento,
	b.cd_cgc, 				/* Rafael. OS46721. 16/12/2006. Inclu� o CGC no select, pois o cd_cgc_w era sempre null.  */
	b.vl_adto_estrang,
	b.vl_saldo_estrang,
	b.vl_cotacao,
	b.cd_moeda
into	cd_cgc_estabelecimento_w,
	cd_pessoa_fisica_w,
	vl_saldo_w,
	cd_estabelecimento_w,
	cd_cgc_w,
	vl_adto_estrang_w,
	vl_saldo_estrang_w,
	vl_cotacao_w,
	cd_moeda_w
from	estabelecimento a,
	adiantamento_pago b
where	a.cd_estabelecimento	= b.cd_estabelecimento
and	b.nr_adiantamento	= nr_adiantamento_p;

select to_number(obter_dados_adiant_pago(nr_adiantamento_p,'TG')) 
into nr_titulo_gerado_w
from dual;

vl_maximo_w	:= nvl(vl_maximo_p,0);

if	(vl_maximo_w = 0) then
	vl_maximo_w	:= vl_saldo_w;
end if;

Obter_Param_Usuario(127,17,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,qt_dias_vencimento_w);
Obter_Param_Usuario(858,21,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_vincular_cheque_movto_w);

while	(vl_saldo_w > 0) loop
	vl_unit_cheque_w 	:= vl_saldo_w;
	if	(vl_saldo_w > vl_maximo_w) then
		vl_unit_cheque_w:= vl_maximo_w;
	end if;
	
	if (philips_param_pck.get_cd_pais = 2) and (nr_titulo_gerado_w is not null) then

		select	nvl(sum(b.vl_imposto), 0)
		into	vl_tributo_w
		from	titulo_pagar_imposto b,
			titulo_pagar a
		where	b.nr_titulo	= nr_titulo_gerado_w
		and	a.nr_titulo	= b.nr_titulo
		and	b.ie_pago_prev = 'V';		
		
		vl_unit_cheque_w := vl_unit_cheque_w + vl_tributo_w;
		
	end if;
	
	/* Projeto Multimoeda - Verifica se o adiantamento � em moeda estrangeira, caso positivo calcula os valores*/
	if (nvl(vl_adto_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
		vl_un_cheque_estrang_w := vl_saldo_estrang_w;
		if (vl_saldo_estrang_w > (vl_maximo_w / vl_cotacao_w)) then
			vl_un_cheque_estrang_w := vl_maximo_w / vl_cotacao_w;
		end if;
		vl_complemento_w := vl_unit_cheque_w - vl_un_cheque_estrang_w;
	else
		vl_un_cheque_estrang_w 	:= null;
		vl_complemento_w	:= null;
		vl_cotacao_w		:= null;
		cd_moeda_w		:= null;
	end if;

	select	cheque_seq.nextval
	into	nr_seq_cheque_w
	from	dual;

    
	
	if (ie_baixa_titulo_p is null) then	
	
		insert into cheque
				(cd_banco,
				cd_agencia_bancaria,
				nr_cheque,
				dt_emissao,
				vl_cheque,
				dt_atualizacao,
				nm_usuario,
				cd_cgc_emitente,
				cd_cgc_destinatario,
				cd_pessoa_destinatario,
				ds_destinatario,
				dt_impressao,
				ie_situacao,
				cd_estabelecimento,
				nr_sequencia,
				nr_seq_conta_banco,
				ds_observacao,
				dt_vencimento,
				nr_seq_trans_fin_emissao,
				vl_cheque_estrang,
				vl_complemento,
				vl_cotacao,
				cd_moeda,
				ds_utilizacao)
		values	(cd_banco_p,
				cd_agencia_p,
				nr_cheque_w,
				nvl(dt_emissao_p,sysdate),
				vl_unit_cheque_w,
				sysdate,
				nm_usuario_p,
				cd_cgc_estabelecimento_w,
				cd_cgc_w,
				cd_pessoa_fisica_w,
				null,
				null,
				null,
				cd_estabelecimento_w,
				nr_seq_cheque_w,
				nr_seq_conta_banco_p,
				null,
				nvl(dt_emissao_p,sysdate) + nvl(qt_dias_vencimento_w,0),
				nr_seq_trans_fin_p,
				vl_un_cheque_estrang_w,
				vl_complemento_w,
				vl_cotacao_w,
				cd_moeda_w,
				ds_utilizacao_p);
	else    
	
		insert into cheque
				(cd_banco,
				cd_agencia_bancaria,
				nr_cheque,
				dt_emissao,
				vl_cheque,
				dt_atualizacao,
				nm_usuario,
				cd_cgc_emitente,
				cd_cgc_destinatario,
				cd_pessoa_destinatario,
				ds_destinatario,
				dt_impressao,
				ie_situacao,
				cd_estabelecimento,
				nr_sequencia,
				nr_seq_conta_banco,
				ds_observacao,
				dt_vencimento,
				nr_seq_trans_fin_emissao,
				vl_cheque_estrang,
				vl_complemento,
				vl_cotacao,
				cd_moeda,
				ds_utilizacao,
				dt_pagamento,
				dt_prev_pagamento)
		values	(cd_banco_p,
				cd_agencia_p,
				nr_cheque_w,
				nvl(dt_emissao_p,sysdate),
				vl_unit_cheque_w,
				sysdate,
				nm_usuario_p,
				cd_cgc_estabelecimento_w,
				cd_cgc_w,
				cd_pessoa_fisica_w,
				null,
				null,
				null,
				cd_estabelecimento_w,
				nr_seq_cheque_w,
				nr_seq_conta_banco_p,
				null,
				nvl(dt_emissao_p,sysdate) + nvl(qt_dias_vencimento_w,0),
				nr_seq_trans_fin_p,
				vl_un_cheque_estrang_w,
				vl_complemento_w,
				vl_cotacao_w,
				cd_moeda_w,
				ds_utilizacao_p,
				sysdate,
				sysdate);
    end if;	

	if(ie_baixa_titulo_p = 'S') and (nr_titulo_gerado_w is not null) then
	
		baixa_titulo_pagar(cd_estabelecimento_w,
							7,
							nr_titulo_gerado_w,
							vl_unit_cheque_w,
							nm_usuario_p,
							nr_seq_trans_fin_p,
							null,
							null,
							sysdate,
							nr_seq_conta_banco_p);		
		Atualizar_Saldo_Tit_Pagar(nr_titulo_gerado_w,nm_usuario_p);	
	end if;
	
	if  (nr_titulo_gerado_w is not null) then
		vincular_cheque_titulo_pagar(nr_seq_cheque_w,nr_cheque_w,cd_banco_p,cd_agencia_p,nr_titulo_gerado_w,'N',nm_usuario_p,null,'S','V');	
	end if;
	
	select	adiantamento_pago_cheque_seq.nextval
	into	nr_seq_adiant_pago_cheque_w
	from	dual;

	insert into adiantamento_pago_cheque
			(dt_atualizacao,
			nm_usuario,
			nr_adiantamento,
			nr_seq_cheque,
			nr_sequencia)
	values		(sysdate,
			nm_usuario_p,
			nr_adiantamento_p,
			nr_seq_cheque_w,
			nr_seq_adiant_pago_cheque_w);	
	
	nr_cheque_w	:= nr_cheque_w + 1;
	vl_saldo_w 	:= vl_saldo_w - vl_unit_cheque_w;
	/* Projeto Multimoeda - Verifica se o adiantamento � em moeda estrangeira, caso positivo calcula os valores*/
	if (nvl(vl_adto_estrang_w,0) <> 0 and nvl(vl_cotacao_w,0) <> 0) then
		vl_saldo_estrang_w := vl_saldo_estrang_w - vl_un_cheque_estrang_w;
	end if;

end loop;

/*lhalves OS337063 em 08/07/2011*/
if	(nvl(ie_vincular_cheque_movto_w,'N') = 'S') then
	select 	max(nr_sequencia)
	into	nr_seq_movto_trans_financ_w
	from	movto_trans_financ
	where	nr_adiant_pago		= nr_adiantamento_p
	and	nr_seq_cheque_cp 	is null;

	if	(nr_seq_movto_trans_financ_w is not null) then
		update	movto_trans_financ
		set	nr_seq_cheque_cp	= nr_seq_cheque_w
		where	nr_sequencia		= nr_seq_movto_trans_financ_w;
	end if;
end if;

commit;

end GERAR_CHEQUE_ADIANT_PAGO;
/