CREATE OR REPLACE
PROCEDURE Desfazer_data_admin_dose(
				dt_referencia_p		Date,
				dt_real_p		Date,
				nr_prescricao_p		Number,
				nm_usuario_p		Varchar2,
				nr_seq_horario_p number default null) IS

BEGIN


if	(nr_seq_horario_p is not null) then
	update	can_ordem_prod
	set	 	dt_administracao = null,
			dt_checagem		 = null,
        	nm_usuario		 = nm_usuario_p,
        	nm_usuario_adm	 = nm_usuario_p         		 
	where	nr_prescricao	 = nr_prescricao_p  
  	and     nr_sequencia     = (select max (b.nr_seq_ordem)
                            	from   can_ordem_item_prescr b
                          		where  b.nr_seq_mat_hor  =   nr_seq_horario_p);


elsif	(dt_real_p is not null) then

	update	can_ordem_prod
	set	dt_administracao	= null,
		dt_checagem	= null,
		nm_usuario		= nm_usuario_p,
		nm_usuario_adm		= nm_usuario_p
	where	nr_prescricao		= nr_prescricao_p
	and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_prevista)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_real_p);



else
	update	can_ordem_prod
	set	dt_administracao	= null,
		dt_checagem	= null,
		nm_usuario		= nm_usuario_p,
		nm_usuario_adm		= nm_usuario_p
	where	nr_prescricao		= nr_prescricao_p
	and	ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_prevista)	= ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(dt_referencia_p);

end if;

commit;

end Desfazer_data_admin_dose; 
/

