create or replace
procedure atualiza_dt_devolucao_cheque(	nm_usuario_p	varchar2,
					nr_seq_cheque_p	number) is 

begin

update	cheque_cr
set	dt_devolucao   = sysdate,
	nm_usuario     = nm_usuario_p,
	dt_atualizacao = sysdate
where	nr_seq_cheque  = nr_seq_cheque_p;

commit;

end atualiza_dt_devolucao_cheque;
/