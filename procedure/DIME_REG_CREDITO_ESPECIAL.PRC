create or replace
procedure dime_reg_credito_especial
			(	nr_seq_controle_p		number,
				cd_estabelecimento_p		varchar2,
				nm_usuario_p			varchar2,
				dt_referencia_p			date,
				ds_separador_p			varchar2,
				nr_seq_dime_p			number,
				qt_linha_p		in out	number,
				nr_sequencia_p		in out	number) is 

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar informa��es referentes � Cr�ditos por Autoriza��es Especiais do DIME
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
-------------------------------------------------------------------------------------------------------------------
Refer�ncias:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

ds_arquivo_w			varchar2(4000);
ds_arquivo_compl_w		varchar2(4000);
ds_linha_w			varchar2(8000);
ds_quadro_w			varchar2(2);
tp_registro_w			varchar2(2);
separador_w			varchar2(1)	:= ds_separador_p;
vl_contabil_w			number(15,2);
nr_linha_w			number(10)	:= qt_linha_p;
nr_seq_registro_w		number(10) 	:= nr_sequencia_p;
qt_reg_especial_w		number(10);
dt_final_w			date;

cd_ident_reg_especial_w		fis_regra_dime_reg_46.cd_ident_reg_especial%type;
cd_conta_devol_icms_w		fis_regra_dime_reg_46.cd_conta_devol_icms%type;
ie_origem_credito_w		fis_regra_dime_reg_46.ie_origem_credito%type;

begin
select	max(cd_ident_reg_especial),
	max(cd_conta_devol_icms),
	max(ie_origem_credito),
	count(1)
into	cd_ident_reg_especial_w,
	cd_conta_devol_icms_w,
	ie_origem_credito_w,
	qt_reg_especial_w
from	fis_regra_dime_reg_46	a
where	nr_seq_regra_dime	= nr_seq_dime_p;

if	(qt_reg_especial_w > 0) then
	tp_registro_w	:= '46';
	ds_quadro_w	:= '46';
	dt_final_w	:= pkg_date_utils.start_of(dt_referencia_p,'MONTH',0);
	--dt_final_w	:= add_months(dt_final_w,11);

	if	(nvl(cd_conta_devol_icms_w,'X') <> 'X') then
		begin

		select	sum(a.vl_saldo)
		into	vl_contabil_w
		from	ctb_mes_ref	b,
			ctb_saldo	a
		where	b.nr_sequencia		= a.nr_seq_mes_ref
		and	b.dt_referencia 	= dt_final_w
		and	a.cd_conta_contabil	= cd_conta_devol_icms_w;

		end;
	else

		select	sum(a.vl_icms_devolucao)
		into	vl_contabil_w
		from	fis_livro_fiscal a
		where	a.ie_tipo_livro_fiscal	= 'I'
		and	dt_final_w between a.dt_inicial and a.dt_final;

	end if;

	vl_contabil_w	:= nvl(vl_contabil_w,0);

	/*	Sequ�ncia 1	*/
	ds_linha_w	:=	tp_registro_w								|| separador_w || -- campo 01: tipo de registro [tamanho 02] preencher com "46"
				ds_quadro_w								|| separador_w || -- campo 02:  quadro [tamanho 02] preencher com "46"
				'001'									|| separador_w || -- campo 03: sequ�ncia [tamanho 03]
				lpad(nvl(cd_ident_reg_especial_w,0),15,'0')				|| separador_w || -- campo 04: identifica��o [tamanho 15] Identifica��o do Regime ou da Autoriza��o Especial
				lpad(replace(campo_mascara(nvl(vl_contabil_w,0),2),'.',''),17,'0')	|| separador_w || -- campo 05: valor [tamanho 17] Valor do cr�dito utilizado na apura��o
				lpad(nvl(ie_origem_credito_w,0),2,'0');							  -- campo 06: origem [tamanho 02] Preencher com: 1 � Cr�dito por transfer�ncia de cr�ditos; 14 � Cr�ditos por DCIP;
				
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	-- insert na tabela do DIME			
	insert into w_dime_arquivo
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_controle_dime,
		nr_linha,
		cd_registro,
		ds_arquivo)
	values	(w_dime_arquivo_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		nr_linha_w,
		tp_registro_w,
		ds_arquivo_w);
	
	/*	Totalizador	*/
	ds_linha_w	:=	tp_registro_w								|| separador_w || -- campo 01: tipo de registro [tamanho 02] preencher com "46"
				ds_quadro_w								|| separador_w || -- campo 02:  quadro [tamanho 02] preencher com "46"
				'990'									|| separador_w || -- campo 03: sequ�ncia [tamanho 03]
				lpad('0',15,'0')							|| separador_w || -- campo 04: identifica��o [tamanho 15] Identifica��o do Regime ou da Autoriza��o Especial
				lpad(replace(campo_mascara(nvl(vl_contabil_w,0),2),'.',''),17,'0')	|| separador_w || -- campo 05: valor [tamanho 17] Valor do cr�dito utilizado na apura��o
				lpad('0',2,'0'); 									  -- campo 06: origem [tamanho 02] Preencher com: 1 � Cr�dito por transfer�ncia de cr�ditos; 14 � Cr�ditos por DCIP;
				
	ds_arquivo_w		:= substr(ds_linha_w,1,4000);
	ds_arquivo_compl_w	:= substr(ds_linha_w,4001,4000);
	nr_seq_registro_w	:= nr_seq_registro_w + 1;
	nr_linha_w		:= nr_linha_w + 1;

	-- insert na tabela do DIME
	insert into w_dime_arquivo
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_controle_dime,
		nr_linha,
		cd_registro,
		ds_arquivo)
	values	(w_dime_arquivo_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		nr_seq_controle_p,
		nr_linha_w,
		tp_registro_w,
		ds_arquivo_w);
end if;
	
commit;

qt_linha_p	:= nr_linha_w;
nr_sequencia_p	:= nr_seq_registro_w;

commit;

end dime_reg_credito_especial;
/