create or replace
procedure consiste_ap_lote_suspenso(	nr_seq_lote_p		number,
				cd_material_p		number,
				qt_total_baixar_p		number,
				ds_erro_p	out		varchar2) as



qt_total_dispensar_w	number(15,2);
qt_suspenso_w		number(15,2);
ds_material_w		varchar2(255);
ds_erro_w		varchar2(255);
qt_total_w		number(15,2);

begin

select	nvl(sum(qt_dispensar), 0)
into	qt_total_dispensar_w
from	ap_lote_item
where	nr_seq_lote = nr_seq_lote_p
and	cd_material = cd_material_p;

select	nvl(sum(qt_dispensar), 0)
into	qt_suspenso_w
from	ap_lote_item
where	nr_seq_lote = nr_seq_lote_p
and	cd_material = cd_material_p
and	dt_supensao is not null;

qt_total_w	:= ( qt_total_dispensar_w - qt_total_baixar_p);

if	((qt_total_w) >= 0) and (qt_suspenso_w > (qt_total_w)) then
	ds_erro_w	:= WHEB_MENSAGEM_PCK.get_texto(280143);
end if;


ds_erro_p	:= substr(ds_erro_w,1,255);

end consiste_ap_lote_suspenso;
/
