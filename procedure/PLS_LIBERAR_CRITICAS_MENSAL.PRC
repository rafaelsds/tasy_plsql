/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
Rotina para a libera��o das cr�ticas da mensalidade
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicion�rio [ X] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */ 

create or replace
procedure pls_liberar_criticas_mensal
		(	nr_seq_mens_critica_p	number,
			nm_usuario_p		Varchar2) is 

begin

update	pls_mensalidade_critica
set	nm_usuario_liberacao	= nm_usuario_p,
	dt_liberacao		= sysdate
where	nr_sequencia		= nr_seq_mens_critica_p;

commit;

end pls_liberar_criticas_mensal;
/
