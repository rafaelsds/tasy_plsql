create or replace
procedure PLT_deletar_erro_copia(	nr_prescricao_p		number,
					nr_sequencia_p		number,
					ie_tipo_item_p		varchar2,
					ie_motivo_p		varchar2,
					nm_usuario_p		varchar2) is 

begin

delete	w_copia_plano
where	nr_prescricao	= nr_prescricao_p
and	nr_seq_item	= nr_sequencia_p
and	ie_tipo_item	= ie_tipo_item_p
and	ie_motivo	in (ie_motivo_p)
and	nm_usuario	= nm_usuario_p;

if (nvl(wheb_usuario_pck.get_ie_commit, 'S') = 'S') then commit; end if;

end PLT_deletar_erro_copia;
/
