create or replace
procedure gravar_painel_usuario(	nr_seq_indicador_p	number,
					nm_usuario_p		varchar2,
					ie_opcao_p		varchar2,
					cd_perfil_p		number)
					is

qt_registro_w	number(10,0);
saveForProfile	boolean;

begin

qt_registro_w := 0;
saveForProfile := ((cd_perfil_p is not null) and (cd_perfil_p <> 0));

if	(ie_opcao_p = 'A') then

	if	saveForProfile then
		select	count(*)
		into	qt_registro_w	
		from	indicador_gestao_usuario
		where	nr_seq_indicador = nr_seq_indicador_p
		and	cd_perfil = cd_perfil_p;
	else
		select	count(*)
		into	qt_registro_w	
		from	indicador_gestao_usuario
		where	nr_seq_indicador = nr_seq_indicador_p
		and	nm_usuario = nm_usuario_p;
	end if;
			
	if	(qt_registro_w = 0) then
		if	saveForProfile then
			insert into indicador_gestao_usuario(
				nr_sequencia,
				dt_atualizacao,
				cd_perfil,
				nr_seq_indicador,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values (
				indicador_gestao_usuario_seq.nextval,
				sysdate,
				cd_perfil_p,
				nr_seq_indicador_p,
				sysdate,
				nm_usuario_p);
		else
			insert into indicador_gestao_usuario(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				nr_seq_indicador,
				dt_atualizacao_nrec,
				nm_usuario_nrec)
			values (
				indicador_gestao_usuario_seq.nextval,
				sysdate,
				nm_usuario_p,
				nr_seq_indicador_p,
				sysdate,
				nm_usuario_p);		
		end if;
	end if;

elsif	(ie_opcao_p = 'E') then
	if	saveForProfile then
		delete	from indicador_gestao_usuario
		where	nr_seq_indicador	=	nr_seq_indicador_p
		and	cd_perfil		=	cd_perfil_p;	 		
	else
		delete	from indicador_gestao_usuario
		where	nr_seq_indicador	=	nr_seq_indicador_p
		and	nm_usuario		=	nm_usuario_p;	 	
	end if;
end if;


commit;

end gravar_painel_usuario;
/