create or replace
procedure gerar_comunicacao_transf(	nr_seq_agenda_p		number,
					dt_origem_p		date,
					nm_usuario_p		varchar,
					cd_estabelecimento_p 	number)
					is

											
nr_seq_evento_w		number(10);											
nr_atendimento_w	number(10);
cd_pessoa_fisica_w	varchar2(10);
cd_agenda_w		number(10);
hr_inicio_w		date;
cd_medico_w		varchar2(10);
cd_procedimento_w	number(15);
ie_origem_proced_w	number(10);
qt_idade_w		number(10);
qt_opme_w		Number(10);
cd_convenio_w		number(15);
qt_opme_ww		number(10);
nr_seq_evento_ww	number(10);	
ie_status_agenda_w	varchar2(3) := null;
ds_observacao_w		varchar2(255);

Cursor C01 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.ie_evento_disp	= 'TAC'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(cd_medico,nvl(cd_medico_w,'0')) = nvl(cd_medico_w,'0')
	and	(obter_regra_alerta_agenda(nr_sequencia,cd_agenda_w,ie_status_agenda_w) = 'S')
	and (obter_classif_regra(nr_sequencia,nvl(obter_classificacao_pf(cd_pessoa_fisica_w),0)) = 'S')
	and	nvl(a.ie_situacao,'A') = 'A';
	
Cursor	C02	is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.ie_evento_disp	= 'TACO'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	qt_opme_w > 0
	and	(obter_regra_alerta_agenda(nr_sequencia,cd_agenda_w,ie_status_agenda_w) = 'S')
	and	nvl(a.ie_situacao,'A') = 'A';
	
cursor	c03	is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.cd_estabelecimento	= cd_estabelecimento_p
	and	a.ie_evento_disp	= 'TAOA'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	qt_opme_ww > 0
	and	(obter_regra_alerta_agenda(nr_sequencia,cd_agenda_w,ie_status_agenda_w) = 'S')
	and	nvl(a.ie_situacao,'A') = 'A';	


begin	
select	nr_atendimento,
	cd_pessoa_fisica,
	cd_agenda,
	hr_inicio,
	cd_medico,
	cd_procedimento,
	ie_origem_proced,
	cd_convenio,
	ie_status_agenda,
	substr(ds_observacao,1,255)
into
	nr_atendimento_w,
	cd_pessoa_fisica_w,
	cd_agenda_w,
	hr_inicio_w,
	cd_medico_w,
	cd_procedimento_w,
	ie_origem_proced_w,
	cd_convenio_w,
	ie_status_agenda_w,
	ds_observacao_w
from	agenda_paciente
where	nr_sequencia = nr_seq_agenda_p;		
qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);
open C01;
loop
fetch C01 into	
	nr_seq_evento_w;
exit when C01%notfound;
	begin
	gerar_evento_agenda_trigger(	nr_seq_evento_w,
					nr_atendimento_w,
					cd_pessoa_fisica_w,
					null,
					nm_usuario_p,
					cd_agenda_w, 
					hr_inicio_w, 
					cd_medico_w, 
					cd_procedimento_w,
					ie_origem_proced_w, 
					null,
					null,
					null,
					null,
					cd_convenio_w,
					null,
					'N',
					nr_seq_agenda_p,
					dt_origem_p,
					null,
					null,
					null,
					ds_observacao_w);
	end;
end loop;
close C01;

select	count(*)
into	qt_opme_w
from	agenda_pac_opme
where	nr_Seq_agenda	= nr_seq_agenda_p
and	nvl(ie_origem_inf,'I') = 'I';

select	count(*)
into	qt_opme_ww
from	agenda_pac_opme
where	nr_Seq_agenda	= nr_seq_agenda_p
and	nvl(ie_origem_inf,'I') = 'I'
and	ie_autorizado	= 'R';

open C02;
loop
fetch C02 into	
	nr_seq_evento_w;
exit when C02%notfound;
	begin
	gerar_evento_agenda_trigger(nr_seq_evento_w,nr_atendimento_w,cd_pessoa_fisica_w,null,nm_usuario_p,
				cd_agenda_w, hr_inicio_w, cd_medico_w, cd_procedimento_w,
				ie_origem_proced_w, null,null,null,null, cd_convenio_w, null,'N',nr_seq_agenda_p,dt_origem_p,
				null,null,null,ds_observacao_w);
	end;
end loop;
close C02;

open C03;
loop
fetch C03 into	
	nr_seq_evento_ww;
exit when C03%notfound;
	begin
	gerar_evento_agenda_trigger(nr_seq_evento_ww,nr_atendimento_w,cd_pessoa_fisica_w,null,nm_usuario_p,
				cd_agenda_w, hr_inicio_w, cd_medico_w, cd_procedimento_w,
				ie_origem_proced_w, null,null,null,null, cd_convenio_w, null,'N',nr_seq_agenda_p,dt_origem_p,
				null,null,null,ds_observacao_w);
	end;
end loop;
close C03;

end gerar_comunicacao_transf;
/