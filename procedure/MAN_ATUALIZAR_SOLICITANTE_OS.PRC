create or replace
procedure man_atualizar_solicitante_os(	nr_sequencia_p		number,
				cd_pessoa_solicitante_p	varchar2,
				ie_historico_p		varchar2,
				nm_usuario_p		varchar2) is 


cd_pessoa_solicitante_w		varchar2(10);
ds_historico_w			varchar2(4000);

begin

select	cd_pessoa_solicitante
into	cd_pessoa_solicitante_w
from	man_ordem_servico
where	nr_sequencia	= nr_sequencia_p;

if	(cd_pessoa_solicitante_w <> cd_pessoa_solicitante_p) then
	begin
	update	man_ordem_servico
	set	cd_pessoa_solicitante	= cd_pessoa_solicitante_p,
		nm_usuario		= nm_usuario_p,
		dt_atualizacao		= sysdate
	where	nr_sequencia		= nr_sequencia_p;
	
	if	(ie_historico_p	= 'S') then

		ds_historico_w	:= substr('O usu�rio: ' || nm_usuario_p || ' alterou o solicitante desta OS' || chr(13) || chr(10) ||
					'de ' || substr(obter_nome_pf(cd_pessoa_solicitante_w),1,60)   || ' para ' || 
					substr(obter_nome_pf(cd_pessoa_solicitante_p),1,60),1,4000);
	
		man_gravar_historico_ordem(	nr_sequencia_p,
					sysdate,
					ds_historico_w,
					'I',
					null,
					nm_usuario_p);
	end if;

	commit;
	end;
end if;

end man_atualizar_solicitante_os;
/