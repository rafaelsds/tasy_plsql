create or replace
procedure liberar_evolucao_consulta(
		cd_evolucao_p		number,
		cd_evolucao_pep_p	number,
		nr_atendimento_p	number,
		ie_liberar_p		varchar2,
		ie_exige_lib_evolucao_p	varchar2,
		cd_perfil_p		number,
		cd_estabelecimento_p	number,
		ie_perg_prev_alta_p out varchar2,
		nm_usuario_p		varchar2) is 

ie_perg_prev_alta_w	varchar2(1) := 'N';
ie_enviar_ci_w		varchar2(1) := 'N';
ie_enviar_ci_lib_w	varchar2(1) := 'N';
ie_enviar_ci_resp_w	varchar2(1);
ie_evolucao_clinica_w	varchar2(1);
ie_tipo_in_funcao_w	varchar2(1);
ie_tipo_evolucao_w	varchar2(3);
ie_funcao_w		varchar2(255);
cd_medico_w		varchar2(10);
dt_liberacao_w		date;
		
begin
if	(cd_evolucao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	if	(ie_liberar_p = 'S') then
		begin
		liberar_evolucao(cd_evolucao_p,nm_usuario_p);
		end;
	end if;
	
	obter_param_usuario(281,105,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_enviar_ci_lib_w);
	
	select	ie_evolucao_clinica,
		dt_liberacao,
		ie_tipo_evolucao,
		cd_medico
	into	ie_evolucao_clinica_w,
		dt_liberacao_w,
		ie_tipo_evolucao_w,
		cd_medico_w
	from	evolucao_paciente
	where	cd_evolucao = cd_evolucao_p;
	
	if	(ie_enviar_ci_lib_w = 'S' or
		ie_enviar_ci_lib_w = 'E') and
		(ie_evolucao_clinica_w = 'P') and
		(dt_liberacao_w is not null) then
		begin		
		ie_enviar_ci_w := 'S';
		end;	
	else if	(ie_enviar_ci_lib_w not in ('S','E')) then
		begin		
		obter_param_usuario(281,252,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_enviar_ci_resp_w);
		
		if	(ie_enviar_ci_resp_w = 'S') and
			(ie_evolucao_clinica_w = 'R') and
			(dt_liberacao_w is not null) then
			begin
			ie_enviar_ci_w := 'S';
			end;
		end if;
		end;
	end if;
	end if;
	
	if	(ie_enviar_ci_w = 'S') then
		begin
		enviar_comunic_interna_medico(cd_evolucao_p,nm_usuario_p);
		end;
	end if;
	
	if	(cd_evolucao_pep_p is not null) and
		((ie_exige_lib_evolucao_p <> 'S') or
		(dt_liberacao_w is null)) then
		begin		
		obter_param_usuario(281,174,cd_perfil_p,nm_usuario_p,cd_estabelecimento_p,ie_funcao_w);
		ie_tipo_in_funcao_w	:= obter_se_contido(ie_tipo_evolucao_w,ie_funcao_w);
		
		if	(ie_tipo_in_funcao_w = 'S') then
			begin
			atualizar_atend_profissional(nr_atendimento_p,cd_medico_w,'N',nm_usuario_p);
			end;
		end if;
		end;
	end if;
	
	if	(dt_liberacao_w is not null) then
		begin
		ie_perg_prev_alta_w	:= obter_se_perg_prev_alta(nr_atendimento_p,cd_medico_w,cd_estabelecimento_p,nm_usuario_p);
		end;
	end if;
	end;
end if;
ie_perg_prev_alta_p := ie_perg_prev_alta_w;
commit;
end liberar_evolucao_consulta;
/