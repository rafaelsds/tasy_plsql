create or replace
procedure abater_valor_acresc_tit_liq(	nr_titulo_p		number,
					nr_seq_baixa_p		number,
					ds_motivo_p		varchar2,
					vl_juros_p		number,
					vl_multa_p		number,
					nm_usuario_p		varchar2) is 

vl_juros_w	number(15,2);
vl_multa_w	number(15,2);	
					
begin
if	(ds_motivo_p is not null) and
	(vl_juros_p is not null) and
	(vl_multa_p is not null) then
	/* Antes de atualizar, obter os valores de juros e multa que estavam na baixa */
	select	max(vl_juros),
		max(vl_multa)
	into	vl_juros_w,
		vl_multa_w
	from	titulo_receber_liq
	where	nr_titulo	= nr_titulo_p;
	
	insert into titulo_rec_liq_abat_acres	
		(nr_sequencia,
		nr_titulo,
		nr_seq_baixa,
		ds_motivo,
		nm_usuario,
		dt_atualizacao,
		vl_juros_anterior,
		vl_multa_anterior)
	values	(titulo_rec_liq_abat_acres_seq.nextval,
		nr_titulo_p,
		nvl(nr_seq_baixa_p,0),
		ds_motivo_p,
		nm_usuario_p,
		sysdate,
		vl_juros_w,
		vl_multa_w);
	
	update	titulo_receber_liq
	set	vl_juros  	= vl_juros_p,
		vl_multa  	= vl_multa_p,
		nm_usuario 	= nm_usuario_p,
		dt_atualizacao  = sysdate
	where	nr_titulo 	= nr_titulo_p;
	
	commit;	
end if;	

end abater_valor_acresc_tit_liq;
/