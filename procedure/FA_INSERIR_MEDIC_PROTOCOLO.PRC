create or replace procedure FA_Inserir_Medic_Protocolo(	nr_seq_protocolo_p	Number,
					nr_seq_receita_p	Number,
					nm_usuario_p		Varchar2) is

cd_material_w		Number(10);
qt_dose_w		Number(18,6);
ie_via_aplicacao_w	Varchar2(15);
nr_dias_receita_w	Number(5);
nr_dias_receita_item_w	Number(5);
cd_unidade_medida_w	Varchar2(30);
cd_intervalo_w		Varchar2(7);
ie_segunda_w		Varchar2(1);
ie_terca_w		Varchar2(1);
ie_quarta_w		Varchar2(1);
ie_quinta_w		Varchar2(1);
ie_sexta_w		Varchar2(1);
ie_sabado_w		Varchar2(1);
ie_domingo_w		Varchar2(1);
ie_uso_continuo_w	Varchar2(1);
ie_se_necessario_w	Varchar2(1);
nr_ciclo_w		Number(5);
ds_texto_receita_w	Long;
dt_inicio_receita_w	Date;
dt_validade_receita_w	Date;
cd_pessoa_fisica_w	Varchar2(10);
ie_permite_varias_rec_w	Varchar2(1);
nr_seq_receita_item_w	Number(10);
qt_medicacao_w		Number(4);

Cursor C01 is

	select	cd_material,
		qt_dose,
		ie_via_aplicacao,
		nvl(nr_dias_receita,nr_dias_receita_w),
		cd_unidade_medida,
		cd_intervalo,
		ie_segunda,
		ie_terca,
		ie_quarta,
		ie_quinta,
		ie_sexta,
		ie_sabado,
		ie_domingo,
		ie_uso_continuo,
		nr_ciclo,
		ie_se_necessario,
    qt_medicacao
	from	fa_protocolo_farmacia_item
	where	nr_seq_protocolo = nr_seq_protocolo_p;

begin

ie_permite_varias_rec_w	:= obter_valor_param_usuario(10015, 40, obter_perfil_ativo, nm_usuario_p, wheb_usuario_pck.get_cd_estabelecimento);



if (nr_seq_protocolo_p is not null) and (nr_seq_receita_p is not null) then

	select	dt_inicio_receita,
		nr_dias_receita,
		cd_pessoa_fisica
	into	dt_inicio_receita_w,
		nr_dias_receita_w,
		cd_pessoa_fisica_w
	from	fa_receita_farmacia
	where	nr_sequencia = nr_seq_receita_p;


	open C01;
	loop

	fetch C01 into
		cd_material_w,
		qt_dose_w,
		ie_via_aplicacao_w,
		nr_dias_receita_item_w,
		cd_unidade_medida_w,
		cd_intervalo_w,
		ie_segunda_w,
		ie_terca_w,
		ie_quarta_w,
		ie_quinta_w,
		ie_sexta_w,
		ie_sabado_w,
		ie_domingo_w,
		ie_uso_continuo_w,
		nr_ciclo_w,
		ie_se_necessario_w,
    qt_medicacao_w;
	exit when C01%notfound;
		begin



		if (nr_dias_receita_item_w > nr_dias_receita_w) then
			nr_dias_receita_item_w := nr_dias_receita_w;
		end if;

		dt_validade_receita_w := dt_inicio_receita_w + nr_dias_receita_item_w - 1;

		if 	(obter_se_interv_agora_acm_sn(cd_intervalo_w) <> 'SN') and (dt_validade_receita_w <= dt_inicio_receita_w) then
			--gravar_log_tasy(30425,'Data de validade n�o pode ser menor que a data de in�cio do medicamento ou menor que a data de in�cio da receita. Receita: '||nr_seq_receita_p||' Medicamento: '||cd_material_w||' Protocolo: '||nr_seq_protocolo_p,nm_usuario_p);
			gravar_log_tasy(30425,WHEB_MENSAGEM_PCK.get_texto(457663,'nr_seq_receita_p='|| nr_seq_receita_p ||';cd_material_w='|| cd_material_w||';nr_seq_protocolo_p='|| nr_seq_protocolo_p),nm_usuario_p);
		elsif	(obtain_user_locale(wheb_usuario_pck.get_nm_usuario) <> 'en_AU') and (ie_permite_varias_rec_w = 'N') and (fa_obter_se_possui_receita(cd_material_w,cd_pessoa_fisica_w,null,dt_inicio_receita_w,null) = 'S') then
			--gravar_log_tasy(30425,'Este medicamento j� esta prescrito em outras receitas. Receita: '||nr_seq_receita_p||' Medicamento: '||cd_material_w||' Protocolo: '||nr_seq_protocolo_p,nm_usuario_p);
			gravar_log_tasy(30425,WHEB_MENSAGEM_PCK.get_texto(457664,'nr_seq_receita_p='|| nr_seq_receita_p ||';cd_material_w='|| cd_material_w||';nr_seq_protocolo_p='|| nr_seq_protocolo_p),nm_usuario_p);
		else
			insert into fa_receita_farmacia_item(
				nr_sequencia,
				dt_atualizacao,
				nm_usuario,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				nr_seq_receita,
				cd_material,
				qt_dose,
				ie_via_aplicacao,
				nr_dias_receita,
				cd_unidade_medida,
				cd_intervalo,
				ie_segunda,
				ie_terca,
				ie_quarta,
				ie_quinta,
				ie_sexta,
				ie_sabado,
				ie_domingo,
				ie_uso_continuo,
				nr_ciclo,
				dt_inicio_receita,
				dt_validade_receita,
				ie_se_necessario,
        qt_medicacao)
			values(	fa_receita_farmacia_item_seq.nextval,
				sysdate,
				nm_usuario_p,
				sysdate,
				nm_usuario_p,
				nr_seq_receita_p,
				cd_material_w,
				qt_dose_w,
				ie_via_aplicacao_w,
				nr_dias_receita_item_w,
				cd_unidade_medida_w,
				cd_intervalo_w,
				ie_segunda_w,
				ie_terca_w,
				ie_quarta_w,
				ie_quinta_w,
				ie_sexta_w,
				ie_sabado_w,
				ie_domingo_w,
				ie_uso_continuo_w,
				nr_ciclo_w,
				dt_inicio_receita_w,
				dt_validade_receita_w,
				ie_se_necessario_w,
        qt_medicacao_w);

			select	max(nr_sequencia)
			into	nr_seq_receita_item_w
			from	fa_receita_farmacia_item;


			copia_campo_long_de_para(	'fa_protocolo_farmacia_item',
							'ds_texto_receita',
							' where	nr_seq_protocolo = :nr_seq_protocolo ',
							'nr_seq_protocolo='||nr_seq_protocolo_p,
							'fa_receita_farmacia_item',
							'ds_texto_receita',
							' where	nr_sequencia = :nr_sequencia ',
							'nr_sequencia='||nr_seq_receita_item_w);
		end if;
		end;
	end loop;
	close C01;

end if;

commit;

end FA_Inserir_Medic_Protocolo;
/