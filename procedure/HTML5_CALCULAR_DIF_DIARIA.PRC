create or replace
procedure HTML5_Calcular_Dif_Diaria(
				cd_estabelecimento_p			Number,
				cd_convenio_p					Number,
				cd_categoria_p					Varchar2,
				cd_plano_p						Varchar2,
				cd_tipo_acomod_convenio_p		Number,
				cd_tipo_acomodacao_p			Number,
				dt_referencia_p					date,
				ie_paciente_isolado_p			varchar2,
				dt_entrada_p					date,
				cd_procedimento_p		in out	Number,
				ie_origem_proced_p		in out	Number,
				vl_Categoria_p			in out	Number,
				vl_diferenca_p			in out	Number,
				ie_calculo_diferenca_p	in out	Varchar2) is 
				
qt_dia_w 	number(10);

begin
qt_dia_w := round(sysdate - dt_entrada_p);

if (qt_dia_w is not null) then
	Calcular_Diferenca_Diaria(cd_estabelecimento_p, cd_convenio_p, cd_categoria_p, cd_plano_p,
		cd_tipo_acomod_convenio_p, cd_tipo_acomodacao_p, dt_referencia_p, qt_dia_w,
		ie_paciente_isolado_p, cd_procedimento_p, ie_origem_proced_p, vl_Categoria_p,
		vl_diferenca_p, ie_calculo_diferenca_p);
end if;

end HTML5_Calcular_Dif_Diaria;
/