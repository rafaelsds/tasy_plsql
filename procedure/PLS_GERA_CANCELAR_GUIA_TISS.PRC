create or replace
procedure pls_gera_cancelar_guia_tiss (	nr_seq_lote_cancel_p	pls_guia_plano_lote_cancel.nr_sequencia%type,
					ie_tipo_guia_p		Number,
					cd_estabelecimento_p	estabelecimento.cd_estabelecimento%type,					
					nm_usuario_p		varchar2) is 
			
			
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Gerar o cancelamento da guias e das contas do TISS
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta: 
[  ]  Objetos do dicionário [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatórios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atenção: Pedido de Cancelamento de Guia pode ser aplicado tanto na Autorização
como na Conta, pelo campo IE_TIPO_GUIA - 1 Solicitação e  2 Faturamento 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
begin

--Solicitação
if	( ie_tipo_guia_p = 1 ) then	
	pls_gera_canc_guia_imp_xml( nr_seq_lote_cancel_p, cd_estabelecimento_p, nm_usuario_p);

--Faturamento
elsif	( ie_tipo_guia_p = 2 ) then
	pls_gera_canc_conta_imp_xml( nr_seq_lote_cancel_p, cd_estabelecimento_p, nm_usuario_p);
end if;

end pls_gera_cancelar_guia_tiss;
/