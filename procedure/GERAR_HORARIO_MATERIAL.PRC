create or replace
PROCEDURE gerar_horario_material(nm_usuario_p VARCHAR2,
                        nr_prescricao_p NUMBER) IS

ds_horarios_w  VARCHAR2(2000);
ds_horarios_aux_w VARCHAR2(2000);
nr_seq_w  NUMBER(10);
nr_seq_rec_w  NUMBER(10);

CURSOR c01 IS
SELECT  ds_horarios,
  nr_sequencia
FROM  prescr_material
WHERE   nr_prescricao = nr_prescricao_p
AND nm_usuario = nm_usuario_p
AND ie_suspenso  <> 'S';

CURSOR c02 IS
SELECT  ds_horarios,
  nr_sequencia
FROM  prescr_recomendacao
WHERE   nr_prescricao = nr_prescricao_p
AND nm_usuario = nm_usuario_p
AND ie_suspenso  <> 'S';

BEGIN

DELETE
FROM w_upg
WHERE nm_usuario = nm_usuario_p;

OPEN c01;
LOOP
FETCH c01 INTO
 ds_horarios_w,
 nr_seq_rec_w;
EXIT WHEN c01%NOTFOUND;

	WHILE (ds_horarios_w IS NOT NULL) LOOP

		IF (INSTR(ds_horarios_w,':') > 0) THEN
			ds_horarios_aux_w := SUBSTR(ds_horarios_w,1,5);
			ds_horarios_w  := trim(SUBSTR(ds_horarios_w, 5, LENGTH(ds_horarios_w)));
		ELSE
			ds_horarios_aux_w := SUBSTR(ds_horarios_w,1,2);
			ds_horarios_w  := trim(SUBSTR(ds_horarios_w, 3, LENGTH(ds_horarios_w)));
		END IF;

		SELECT	w_upg_seq.NEXTVAL
		INTO	nr_seq_w
		FROM	dual;

		INSERT INTO w_upg(
				nr_sequencia,
				nr_seq_rec,
				ds_horario,
				nr_prescricao,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				ie_recomendacao)
		VALUES (nr_seq_w,
				nr_seq_rec_w,
				ds_horarios_aux_w,
				nr_prescricao_p,
				SYSDATE,
				SYSDATE,
				nm_usuario_p,
				nm_usuario_p,
				'N');
	END LOOP;
END LOOP;
CLOSE c01;

OPEN c02;
LOOP
FETCH c02 INTO
	ds_horarios_w,
	nr_seq_rec_w;
EXIT WHEN c02%NOTFOUND;

	WHILE (ds_horarios_w IS NOT NULL) LOOP

		IF (INSTR(ds_horarios_w,':') > 0) THEN
			ds_horarios_aux_w := SUBSTR(ds_horarios_w,1,5);
			ds_horarios_w  := trim(SUBSTR(ds_horarios_w, 5, LENGTH(ds_horarios_w)));
		ELSE
			ds_horarios_aux_w := SUBSTR(ds_horarios_w,1,2);
			ds_horarios_w  := trim(SUBSTR(ds_horarios_w, 3, LENGTH(ds_horarios_w)));
		END IF;

		SELECT w_upg_seq.NEXTVAL
		INTO nr_seq_w
		FROM dual;

		INSERT INTO w_upg(
				nr_sequencia,
				nr_seq_rec,
				ds_horario,
				nr_prescricao,
				dt_atualizacao,
				dt_atualizacao_nrec,
				nm_usuario,
				nm_usuario_nrec,
				ie_recomendacao)
		VALUES (nr_seq_w,
				nr_seq_rec_w,
				ds_horarios_aux_w,
				nr_prescricao_p,
				SYSDATE,
				SYSDATE,
				nm_usuario_p,
				nm_usuario_p,
				'S');
	END LOOP;
END LOOP;
CLOSE c02;

COMMIT;

END gerar_horario_material;
/