create or replace
procedure gerar_conta_repasse_medico(
			nr_repasse_terceiro_p  		number,
			ie_tipo_convenio_p		number,
			nm_usuario_p			varchar2,
			ie_gerar_estornados_p		varchar2,
			ie_gerar_rep_perc_saldo_p	varchar2,
			ie_valor_lib_zerado_p		varchar2,
			nr_seq_protocolo_p		number,
			ie_tipo_atendimento_p		number,
			ds_filtro_convenios_p		varchar2,
			ds_filtro_procedimentos_p	varchar2,
			cd_area_proc_p			number,
			cd_espec_proc_p			number,
			cd_grupo_proc_p			number,
			ds_filtro_medicos_p		varchar2,
			ds_filtro_proc_p			varchar2,
			ds_observacao_p			varchar2 default null) IS


nr_seq_terceiro_w		number(10,0);
cd_convenio_w			number(05,0);
ie_tipo_data_w			number(01,0);
dt_inicial_W			date;
dt_final_w			date;
dt_mesano_referencia_w		date;
nr_sequencia_w			number(10,0);
qt_reg_proc_w			number(10,0);
qt_reg_mat_w			number(10,0);
qt_reg_item_w			number(10,0);
nr_seq_repasse_item_w		number(10,0);
cont_w				number(10,0);
ie_tipo_convenio_regra_w	number(10,0);
ie_excluir_rep_zerado_w		varchar2(255);
ie_saldo_w			varchar2(255);
ie_valor_w			varchar2(255);
cd_estabelecimento_w		number(4);
nr_seq_terc_rep_w		number(10,0);
tx_repasse_w			number(15,4);
nr_seq_terc_destino_w		number(10,0);
nr_seq_trans_financ_w		number(10,0);
nr_repasse_terceiro_dest_w	number(10,0);
nr_sequencia_item_w		number(10,0);
vl_item_w			number(15,2);
vl_repassar_w			number(15,2);
vl_repasse_ie_valor_w		number(15,2);
vl_repasse_w			number(15,2);
vl_saldo_repasse_w		number(15,2);
vl_repasse_proc_w		number(15,2);
vl_repasse_mat_w		number(15,2);
vl_repasse_regra_w		number(15,2);
ie_calcular_item_w		varchar2(1);
ie_desconsiderar_glosados_w	varchar2(255); -- afstringari 193824 31/03/2010
ie_estorno_w			varchar2(100);
ds_convenios_w			varchar2(255);
ds_convenios_not_w		varchar2(1);
ds_procedimentos_w		varchar2(255);
ds_procedimentos_not_w		varchar2(1);
nr_seq_tipo_w			number(10);
ie_procedimento_w		varchar2(1);
ie_material_w			varchar2(1);
ie_item_repasse_w		varchar2(1);
ds_tipo_repasse_w		varchar2(255);
dt_inicial_repasse_w		date;
dt_final_repasse_w		date;
ie_tipo_convenio_w		number(5);
ds_medicos_w			varchar2(2000);
ds_medicos_not_w		varchar2(1);
ds_proc_w			varchar2(255);
ds_proc_not_w			varchar2(1);
ie_vinc_rep_estorno_w		varchar2(1);
cd_medico_terc_w		varchar2(10);
nr_repasse_terceiro_w		number(10);
qt_repasse_w			number(10);
ie_status_w			varchar2(1);
qt_promat_w			number(10);

ie_status_p			varchar2(255);
ds_sql_w				varchar2(4000)	:= '';
ds_from_w			varchar2(4000)	:= '';
ds_sql_rest_w			varchar2(4000)	:= '';
ds_sql_filtro_w			varchar2(4000)	:= '';
ds_sql_t				varchar2(4000)	:= '';

var_cur_w			pls_integer;
var_exec_w			pls_integer;
var_retorno_w			pls_integer;



cursor c02 is
select	a.nr_sequencia
from   	atendimento_paciente e,
	convenio d,
	Conta_paciente c,
	Material_atend_paciente b,
	material_repasse a
where	nvl(c.nr_seq_protocolo,0)						= nvl(nr_seq_protocolo_p,nvl(c.nr_seq_protocolo,0))
and	a.nr_seq_material							= b.nr_sequencia
and	b.nr_interno_conta							= c.nr_interno_conta
and	nvl(cd_convenio_w, c.cd_convenio_parametro) 				= c.cd_convenio_parametro
and	d.cd_convenio								= c.cd_convenio_parametro
and	nvl(ie_tipo_convenio_p, d.ie_tipo_convenio)				= d.ie_tipo_convenio
and	a.ie_status							in (decode(nvl(ie_desconsiderar_glosados_w,'S'),'S','G','X'),'L','S','R', decode(ie_gerar_estornados_p, 'S', 'E', null))
and	substr(obter_se_gerar_repasse(nr_repasse_terceiro_w, null, a.nr_sequencia),1,255)	= 'S'
and	((((ie_tipo_data_w = 0)	and (nvl(a.dt_liberacao, a.dt_atualizacao)	between dt_inicial_w and dt_final_w))
	or ((ie_tipo_data_w = 1)	and (c.dt_mesano_referencia		between dt_inicial_w and dt_final_w))
	or ((ie_tipo_data_w = 2)	and (b.dt_atendimento			between dt_inicial_w and dt_final_w))
	or ((ie_tipo_data_w = 3)	and (to_date(nvl(obter_dados_ret_convenio(a.nr_seq_item_retorno, 2),'01/01/2099')) between dt_inicial_w and dt_final_w))) -- Edgar 05/10/2006 Os 42080, inclui este or
	or ((ie_tipo_data_w = 4)	and to_date(nvl(obter_dados_nota_fiscal(nvl(obter_nf_conta(b.nr_interno_conta,8), nvl(obter_nf_conta(b.nr_interno_conta,1), obter_nf_conta(b.nr_interno_conta,4))),9),'01/01/2099')) between dt_inicial_w and dt_final_w)
	or ((ie_tipo_data_w = 5)	and to_date(obter_dados_laudo_procedimento(b.nr_sequencia,'DL')) between dt_inicial_w and dt_final_w))
and	a.nr_seq_terceiro							= nr_seq_terceiro_w
and	a.nr_repasse_terceiro   						is null
and	((nvl(a.ie_estorno,'N') = 'N')						or (ie_estorno_w = 'S'))
and	(((nvl(ie_gerar_rep_perc_saldo_p, 'S') = 'N') and (substr(OBTER_DADOS_FORMA_REPASSE(a.cd_regra, a.nr_seq_regra_item, 'S'),1,255) <> 'S'))
	 or (nvl(ie_gerar_rep_perc_saldo_p, 'S') = 'S'))
and	((nvl(ie_valor_lib_zerado_p,'S') = 'N' and a.vl_liberado <> 0) or
	 (nvl(ie_valor_lib_zerado_p,'S') = 'S'))
and	c.nr_atendimento							= e.nr_atendimento
and	e.ie_tipo_atendimento							= nvl(ie_tipo_atendimento_p,e.ie_tipo_atendimento)
and	nvl(a.cd_medico,'X')							= nvl(nvl(cd_medico_terc_w,a.cd_medico),'X')
and	((nvl(ie_vinc_rep_estorno_w,'S')	= 'S') or ((nvl(ie_vinc_rep_estorno_w,'S') = 'N' and ((a.ie_status <> 'E' and c.ie_status_acerto = 2) or c.ie_status_acerto = 2)) 
						and	not exists(	select	1
									from	material_repasse x
									where	x.nr_seq_material = b.nr_sequencia
									and	x.ie_status	= 'A')))
and	(ds_convenios_w is null or
		(ds_convenios_not_w = 'N' and (','||ds_convenios_w||',' like '%,'||to_char(c.cd_convenio_parametro)||',%')	
		) or
		(ds_convenios_not_w = 'S' and not(','||ds_convenios_w||',' like ' '||'%,'||to_char(c.cd_convenio_parametro)||',%'||' ')
		)
	)
and	(ds_medicos_w is null or
		(ds_medicos_not_w = 'N' and (','||ds_medicos_w||',' like '%,'||to_char(a.cd_medico)||',%')) or
		(ds_medicos_not_w = 'S' and not(','||ds_medicos_w||',' like '%,'||to_char(a.cd_medico)||',%'))
	);

cursor c03 is
select	a.nr_sequencia
from 	repasse_terceiro_item a
where	a.dt_liberacao 		between dt_inicial_w and dt_final_w
and	a.nr_seq_terceiro		= nr_seq_terceiro_w
and	a.nr_repasse_terceiro 	is null;

cursor c04 is
select	nr_sequencia,
	tx_repasse,
	nr_seq_terc_destino,
	nr_seq_trans_financ,
	nvl(vl_repasse, 0),
	nvl(ie_saldo, 'N'),
	nvl(ie_valor, 'T')
from	terceiro_repasse
where	nr_seq_terceiro					= nr_seq_terceiro_w
and	ie_situacao					= 'A'
and	nvl(ie_tipo_convenio, ie_tipo_convenio_regra_w)	= ie_tipo_convenio_regra_w
order 	by nvl(ie_prioridade, 0);	

cursor c05 is
select	a.cd_pessoa_fisica	
from	terceiro a
where	a.cd_pessoa_fisica is not null
and	a.nr_sequencia	= nr_seq_terceiro_w
union
select	a.cd_pessoa_fisica	
from	terceiro_pessoa_fisica a
where	a.nr_seq_terceiro	= nr_seq_terceiro_w;

BEGIN

/* Francisco - OS 174252 - 28/10/2009 */
--liberar_repasse_regra(nr_repasse_terceiro_w,nm_usuario_p);

select	nr_seq_terceiro,
	cd_convenio,
	dt_periodo_inicial,
	dt_periodo_final,
	ie_tipo_data,
	cd_estabelecimento,
	dt_mesano_referencia,
	nr_seq_tipo,
	ie_tipo_convenio,
	ie_status
into	nr_seq_terceiro_w,
	cd_convenio_w,
	dt_inicial_w,
	dt_final_w,
	ie_tipo_data_w,
	cd_estabelecimento_w,
	dt_mesano_referencia_w,
	nr_seq_tipo_w,
	ie_tipo_convenio_w,
	ie_status_w
from	repasse_terceiro
where	nr_repasse_terceiro	= nr_repasse_terceiro_p;

select	nvl(max(a.ie_procedimento),'S'),
	nvl(max(a.ie_material),'S'),
	nvl(max(a.ie_item_repasse),'S'),
	max(a.ds_tipo_repasse)
into	ie_procedimento_w,
	ie_material_w,
	ie_item_repasse_w,
	ds_tipo_repasse_w
from	tipo_repasse a
where	a.nr_sequencia	= nr_seq_tipo_w;

if	(nvl(ie_procedimento_w,'S') = 'N') and
	(nvl(ie_material_w,'S') = 'N') and
	(nvl(ie_item_repasse_w,'S') = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(205485, 'DS_TIPO_REPASSE_W='||ds_tipo_repasse_w);
end if;

obter_param_usuario(89,67,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_desconsiderar_glosados_w);
obter_param_usuario(89,76,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_estorno_w);
obter_param_usuario(89,137,obter_perfil_ativo, nm_usuario_p, cd_estabelecimento_w, ie_vinc_rep_estorno_w);

/* Francisco - OS 71440 - 19/10/2007 */
select	nvl(max(ie_excluir_rep_zerado),'N')
into	ie_excluir_rep_zerado_w
from	parametro_faturamento
where	cd_estabelecimento	= cd_estabelecimento_w;

if (ds_filtro_convenios_p is not null) then
	if (instr(ds_filtro_convenios_p, 'not') = 1) then
		ds_convenios_w := substr(ds_filtro_convenios_p, 4);
		ds_convenios_not_w := 'S';
	else
		ds_convenios_w := ds_filtro_convenios_p;
		ds_convenios_not_w := 'N';
	end if;
else
	ds_convenios_w := null;
	ds_convenios_not_w := null;	
end if;

if (ds_filtro_procedimentos_p is not null) then
	if (instr(ds_filtro_procedimentos_p, 'not') = 1) then
		ds_procedimentos_w := substr(ds_filtro_procedimentos_p, 4);
		ds_procedimentos_not_w := 'S';
	else
		ds_procedimentos_w := ds_filtro_procedimentos_p;
		ds_procedimentos_not_w := 'N';
	end if;
else
	ds_procedimentos_w := null;
	ds_procedimentos_not_w := null;
end if;

if	(ds_filtro_medicos_p is not null) then
	if	(instr(ds_filtro_medicos_p, 'not') = 1) then
		ds_medicos_w	:= substr(ds_filtro_medicos_p,4);
		ds_medicos_not_w	:= 'S';
	else
		ds_medicos_w	:= ds_filtro_medicos_p;
		ds_medicos_not_w	:= 'N';
	end if;
else
	ds_medicos_w		:= null;
	ds_medicos_not_w	:= null;
end if;

ds_medicos_w	:= trim(replace(ds_medicos_w,chr(39),''));

if	(ds_filtro_proc_p is not null) then
	if (instr(ds_filtro_proc_p, 'not') = 1) then
		ds_proc_w	:= substr(ds_filtro_proc_p,4);
		ds_proc_not_w	:= 'S';
	else
		ds_proc_w	:= ds_filtro_proc_p;
		ds_proc_not_w	:= 'N';
	end if;
else
	ds_proc_w	:= null;
	ds_proc_not_w	:= null;
end if;

qt_repasse_w		:= 1;

open c05;
loop
fetch c05 into
	cd_medico_terc_w;
exit when c05%notfound;	
	if	(nvl(qt_repasse_w,1)	<> 1) then
		
		select	repasse_terceiro_seq.nextval
		into	nr_repasse_terceiro_w
		from	dual;
		
		insert	into	repasse_terceiro
					(nr_repasse_terceiro,
					nm_usuario,
					nr_seq_terceiro,
					ie_tipo_data,
					ie_status,
					dt_mesano_referencia,
					dt_atualizacao,
					cd_estabelecimento,
					ie_tipo_convenio,
					cd_convenio,
					dt_periodo_inicial,
					dt_periodo_final,
					nr_seq_tipo,
					ds_observacao
					)
			values	(	nr_repasse_terceiro_w,
					nm_usuario_p,
					nr_seq_terceiro_w,
					ie_tipo_data_w,
					ie_status_w,
					dt_mesano_referencia_w,
					sysdate,
					cd_estabelecimento_w,
					ie_tipo_convenio_w,
					cd_convenio_w,
					dt_inicial_w,
					dt_final_w,
					nr_seq_tipo_w,
					ds_observacao_p
					);
	else
		nr_repasse_terceiro_w	:= nr_repasse_terceiro_p;
	end if;
	
	if	(nvl(ie_procedimento_w,'S')	= 'S') then
	
		ds_sql_w		:= 	'select 	a.nr_sequencia '			||
					'from   	estrutura_procedimento_v g, '	||
					'	procedimento f, '			||
					'	atendimento_paciente e, '		||
					'	convenio d, '			||
					'	Conta_paciente c, '		||
					'	procedimento_paciente b, '		||
					'	procedimento_repasse a ';
					
		ds_sql_rest_w 	:= 	'where	1 = 1 ';
		
		if (nvl(nr_seq_protocolo_p,0) > 0) then 
			ds_sql_rest_w := ds_sql_rest_w || 	'and	nvl(c.nr_seq_protocolo,0)	= :nr_seq_protocolo ';
		end if;	
		
			ds_sql_rest_w := ds_sql_rest_w || 	'and	a.nr_seq_procedimento 	= b.nr_sequencia ';
			ds_sql_rest_w := ds_sql_rest_w ||	'and 	b.nr_interno_conta      	= c.nr_interno_conta ';
			ds_sql_rest_w := ds_sql_rest_w ||	'and	d.cd_convenio		= c.cd_convenio_parametro ';
						
		if (nvl(ie_tipo_convenio_p,0) > 0) then 
			ds_sql_rest_w := ds_sql_rest_w || 	'and	d.ie_tipo_convenio	= :ie_tipo_convenio ';
		end if;
		
		if (nvl(cd_convenio_w,0) > 0) then 
			ds_sql_rest_w := ds_sql_rest_w ||	'and	c.cd_convenio_parametro	= :cd_convenio ';
		end if;
		
			ds_sql_rest_w := ds_sql_rest_w ||	'and	f.cd_procedimento		= g.cd_procedimento ';
			ds_sql_rest_w := ds_sql_rest_w ||	'and	f.ie_origem_proced	= g.ie_origem_proced ';
							

			ds_sql_rest_w := ds_sql_rest_w 	||' and	a.ie_status in (decode(nvl(:ie_desconsiderar_glosados,' 
							|| chr(39) || 'S' || chr(39) || '),' 
							|| chr(39) || 'S' || chr(39) || ',' 
							|| chr(39) || 'G' || chr(39) || ',' 
							|| chr(39) || 'X' || chr(39) || '),' 
							|| chr(39) || 'L' || chr(39) || ',' 
							|| chr(39) || 'S' || chr(39) || ',' 
							|| chr(39) || 'R' || chr(39) || ', decode(:ie_gerar_estornados,' 
							|| chr(39) || 'S' || chr(39) || ',' 
							|| chr(39) || 'E' || chr(39) || ', null)) ';
							
		if (nvl(ie_tipo_data_w,0) = 0) then 
		
			ds_sql_rest_w := ds_sql_rest_w ||	'and 	nvl(a.dt_liberacao,a.dt_atualizacao)	between :dt_inicial and :dt_final ';	
			
		elsif (nvl(ie_tipo_data_w,0) = 1) then 
			
			ds_sql_rest_w := ds_sql_rest_w ||	'and 	c.dt_mesano_referencia	between :dt_inicial and :dt_final ';	
	
		elsif (nvl(ie_tipo_data_w,0) = 2) then 
				
			ds_sql_rest_w := ds_sql_rest_w ||	'and 	b.dt_procedimento		between :dt_inicial and :dt_final ';	

		elsif (nvl(ie_tipo_data_w,0) = 3) then 
				
			ds_sql_rest_w := ds_sql_rest_w ||	'and 	(to_date(nvl(obter_dados_ret_convenio(a.nr_seq_item_retorno, 2),' || chr(39) || '01/01/2099' || chr(39) || ')) between :dt_inicial and :dt_final) ';	

		elsif (nvl(ie_tipo_data_w,0) = 4) then 
				
			ds_sql_rest_w := ds_sql_rest_w ||	'and 	(nvl(obter_dt_emissao_nf(b.nr_interno_conta),to_date(nvl(obter_dados_nota_fiscal(nvl(obter_nf_conta(b.nr_interno_conta,8), nvl(obter_nf_conta(b.nr_interno_conta,1),'||
							'obter_nf_conta(b.nr_interno_conta,4))),9),' || chr(39) || '01/01/2099' || chr(39) || '))) between :dt_inicial and :dt_final) ';	

		elsif (nvl(ie_tipo_data_w,0) = 5) then
				
			ds_sql_rest_w := ds_sql_rest_w ||	'and 	to_date(nvl(obter_dados_laudo_procedimento(b.nr_sequencia,' || chr(39) || 'DL' || chr(39) || '),' || chr(39) || '01/01/2099 00:00:00' || chr(39) || 
							'),' || chr(39) || 'dd/mm/yyyy hh24:mi:ss' || chr(39) || ') between :dt_inicial and :dt_final ';	

		end if;
		
			ds_sql_rest_w := ds_sql_rest_w ||	'and	a.nr_seq_terceiro		= :nr_seq_terceiro ';
			ds_sql_rest_w := ds_sql_rest_w ||	'and	a.nr_repasse_terceiro	is null ';
			
		if (nvl(ie_estorno_w,'N') = 'N') then 
			ds_sql_rest_w := ds_sql_rest_w ||	'and	nvl(a.ie_estorno,' || chr(39) || 'N' || chr(39) || ') = ' || chr(39) || 'N' || chr(39);
		end if;
		
		if (nvl(ie_gerar_rep_perc_saldo_p,'S') = 'N') then
			ds_sql_rest_w := ds_sql_rest_w ||	'and 	(substr(OBTER_DADOS_FORMA_REPASSE(a.cd_regra, a.nr_seq_regra_item, ' || chr(39) || 'S' || chr(39) || '),1,255) <> ' || chr(39) || 'S' || chr(39) || ') ' ;
		end if;
		
		if (nvl(ie_valor_lib_zerado_p,'S') = 'N') then
			ds_sql_rest_w := ds_sql_rest_w || 'and 	a.vl_liberado <> 0 ';
		end if;
				
			ds_sql_rest_w := ds_sql_rest_w || 'and	c.nr_atendimento		= e.nr_atendimento ';
		
		if (nvl(ie_tipo_atendimento_p,0) <> 0) then
			ds_sql_rest_w := ds_sql_rest_w ||  'and	e.ie_tipo_atendimento	= :ie_tipo_atendimento ';
		end if;
			
			ds_sql_rest_w := ds_sql_rest_w ||  'and	f.cd_procedimento 	= b.cd_procedimento ';
			ds_sql_rest_w := ds_sql_rest_w ||  'and	f.ie_origem_proced 	= b.ie_origem_proced ';
		
		if (nvl(cd_medico_terc_w,0) <> 0) then
			ds_sql_rest_w := ds_sql_rest_w ||  'and	nvl(a.cd_medico,' || chr(39) || '0' || chr(39) || ')	= :cd_medico_terc ';
		end if;
			
		if (nvl(cd_area_proc_p,0) <> 0) then
			ds_sql_rest_w := ds_sql_rest_w ||  'and	g.cd_area_procedimento 	= :cd_area_proc ';
		end if;
		
		if (nvl(cd_espec_proc_p,0) <> 0) then
			ds_sql_rest_w := ds_sql_rest_w ||  'and	g.cd_especialidade 	= :cd_espec_proc ';
		end if;
		
		if (nvl(cd_grupo_proc_p,0) <> 0) then
			ds_sql_rest_w := ds_sql_rest_w ||  'and	g.cd_grupo_proc 		= :cd_grupo_proc ';
		end if;

		if (nvl(ie_vinc_rep_estorno_w,'N') = 'N') then
			ds_sql_rest_w := ds_sql_rest_w ||  	' and 	(((a.ie_status <> ' || chr(39) || 'E' || chr(39) || ' and c.ie_status_acerto = 2) or c.ie_status_acerto = 2) ' ||
							'		and not exists( select 1 from procedimento_repasse x where x.nr_seq_procedimento = b.nr_sequencia and x.ie_status = ' || chr(39) || 'A' || chr(39) || ')) ';
		end if;
				
		if (ds_convenios_w is not null) then
			if (nvl(ds_convenios_not_w,'N') = 'N') then
				ds_sql_rest_w := ds_sql_rest_w ||  ' and obter_se_contido(c.cd_convenio_parametro, :ds_convenios) = '|| chr(39) || 'S' || chr(39);
			else 
				ds_sql_rest_w := ds_sql_rest_w ||  ' and obter_se_contido(c.cd_convenio_parametro, :ds_convenios) = '|| chr(39) || 'N' || chr(39);
			end if;		
		end if;
		
		if (ds_procedimentos_w is not null) then
			if (nvl(ds_procedimentos_not_w,'N') = 'N') then
				ds_sql_rest_w := ds_sql_rest_w ||  ' and obter_se_contido(f.cd_tipo_procedimento, :ds_procedimentos) = '|| chr(39) || 'S' || chr(39);
			else 
				ds_sql_rest_w := ds_sql_rest_w ||  ' and obter_se_contido(f.cd_tipo_procedimento, :ds_procedimentos) = '|| chr(39) || 'N' || chr(39);
			end if;		
		end if;
		
		
		if (ds_medicos_w is not null) then
			if (nvl(ds_medicos_not_w,'N') = 'N') then
				ds_sql_rest_w := ds_sql_rest_w ||  ' and obter_se_contido(a.cd_medico, :ds_medicos) = '|| chr(39) || 'S' || chr(39);
			else 
				ds_sql_rest_w := ds_sql_rest_w ||  ' and obter_se_contido(a.cd_medico, :ds_medicos) = '|| chr(39) || 'N' || chr(39);
			end if;		
		end if;
		
		if (ds_proc_w is not null) then
			if (nvl(ds_proc_not_w,'N') = 'N') then
				ds_sql_rest_w := ds_sql_rest_w ||  ' and obter_se_contido(b.cd_procedimento, :ds_proc) = '|| chr(39) || 'S' || chr(39);
			else 
				ds_sql_rest_w := ds_sql_rest_w ||  ' and obter_se_contido(b.cd_procedimento, :ds_proc) = '|| chr(39) || 'N' || chr(39);
			end if;		
		end if;
					
		ds_sql_w := ds_sql_w || ds_sql_rest_w;
				
		var_cur_w	:=	dbms_sql.open_cursor;
		dbms_sql.parse(var_cur_w,  ds_sql_w, 1);
		
		if	(nr_seq_protocolo_p is not null) then
			dbms_sql.bind_variable(var_cur_w, ':nr_seq_protocolo', nr_seq_protocolo_p);
		end if;
		
		if	(ie_tipo_convenio_p is not null) then
			dbms_sql.bind_variable(var_cur_w, ':ie_tipo_convenio', ie_tipo_convenio_p);
		end if;

		if	(cd_convenio_w is not null) then
			dbms_sql.bind_variable(var_cur_w, ':cd_convenio' ,cd_convenio_w);
		end if;
		
		if	(cd_medico_terc_w is not null) then
			dbms_sql.bind_variable(var_cur_w, ':cd_medico_terc' ,cd_medico_terc_w);	
		end if;
		
		if	(ie_tipo_convenio_p is not null) then
			dbms_sql.bind_variable(var_cur_w, ':ie_tipo_convenio', ie_tipo_convenio_p);
		end if;

	
		dbms_sql.bind_variable(var_cur_w, ':nr_seq_terceiro', nr_seq_terceiro_w);
		dbms_sql.bind_variable(var_cur_w, ':ie_desconsiderar_glosados', ie_desconsiderar_glosados_w);
		dbms_sql.bind_variable(var_cur_w, ':ie_gerar_estornados', ie_gerar_estornados_p);
		
		if	(ie_tipo_atendimento_p is not null) then
			dbms_sql.bind_variable(var_cur_w, ':ie_tipo_atendimento', ie_tipo_atendimento_p);
		end if;
		
		if	(ie_tipo_data_w <> 6) then
			dbms_sql.bind_variable(var_cur_w, ':dt_inicial', dt_inicial_w);
			dbms_sql.bind_variable(var_cur_w, ':dt_final', dt_final_w);
		end if;
		
		if	(ds_convenios_w is not null) then
			dbms_sql.bind_variable(var_cur_w, ':ds_convenios', ds_convenios_w);
		end if;
		
		if	(ds_procedimentos_w is not null) then
			dbms_sql.bind_variable(var_cur_w, ':ds_procedimentos', ds_procedimentos_w);
		end if;
		
		if	(ds_medicos_w is not null) then
			dbms_sql.bind_variable(var_cur_w, ':ds_medicos', ds_medicos_w);
		end if;
		
		if	(ds_proc_w is not null) then
			dbms_sql.bind_variable(var_cur_w, ':ds_proc', ds_proc_w);
		end if;
		
		if	(cd_area_proc_p is not null) then
			dbms_sql.bind_variable(var_cur_w, ':cd_area_proc', cd_area_proc_p);
		end if;
		
		if	(cd_espec_proc_p is not null) then
			dbms_sql.bind_variable(var_cur_w, ':cd_espec_proc', cd_espec_proc_p);
		end if;
		
		if	(cd_grupo_proc_p is not null) then
			dbms_sql.bind_variable(var_cur_w, ':cd_grupo_proc', cd_grupo_proc_p);
		end if;
		
		dbms_sql.define_column(var_cur_w, 1, nr_sequencia_w);
		var_exec_w := dbms_sql.execute(var_cur_w);
		
		loop
		var_retorno_w := dbms_sql.fetch_rows(var_cur_w);
		exit when var_retorno_w = 0;		
		dbms_sql.column_value(var_cur_w, 1, nr_sequencia_w);	
			if(obter_se_gerar_repasse(nr_repasse_terceiro_w,nr_sequencia_w,null) = 'S') then
				update	procedimento_repasse
				set	nr_repasse_terceiro	= nr_repasse_terceiro_w,
					nm_usuario 		= nm_usuario_p,
					dt_atualizacao 		= sysdate
				where	nr_sequencia		= nr_sequencia_w;
			end if;		
		end loop;
		dbms_sql.close_cursor(var_cur_w);
	end if;	
	

	if	(nvl(ie_material_w,'S')		= 'S') then
		open c02;
		loop
		fetch c02 into
			nr_sequencia_w;
		exit when c02%notfound;

			update	material_repasse
			set	nr_repasse_terceiro	= nr_repasse_terceiro_w
			where	nr_sequencia 		= nr_sequencia_w;

		end loop;
		close c02;
	end if;	
	
	select	count(x.nr_sequencia)
	into	qt_promat_w
	from	(select	a.nr_sequencia
		from	procedimento_repasse a
		where	a.nr_repasse_terceiro	= nr_repasse_terceiro_w
		union	all
		select	a.nr_sequencia
		from	material_repasse a
		where	a.nr_repasse_terceiro	= nr_repasse_terceiro_w) x;
	
	if	(nvl(qt_promat_w,0)	<> 0) then
		if	(nvl(ie_item_repasse_w,'S')	= 'S') then
			
			open c03;
			loop
			fetch c03 into
				nr_seq_repasse_item_w;
			exit when c03%notfound;

				begin
				select 	nvl(max(nr_sequencia_item), 0) +1
				into	nr_sequencia_item_w
				from 	repasse_terceiro_item
				where	nr_repasse_terceiro	= nr_repasse_terceiro_w;

				update	repasse_terceiro_item
				set	nr_repasse_terceiro	= nr_repasse_terceiro_w,
					nr_sequencia_item	= nr_sequencia_item_w
				where	nr_sequencia 		= nr_seq_repasse_item_w;
				exception
				when dup_val_on_index then
					wheb_mensagem_pck.exibir_mensagem_abort(191472, 'nr_repasse_terceiro_w = ' || nr_repasse_terceiro_w ||
											';NR_SEQUENCIA_ITEM_W = ' || nr_sequencia_item_w ||
											';NR_SEQ_REPASSE_ITEM_W = ' ||  nr_seq_repasse_item_w ||
											';SQLERRM = ' || sqlerrm);
				end;

			end loop;
			close c03;
		end if;	

		vl_repasse_w		:= nvl(obter_valor_repasse(nr_repasse_terceiro_w, 'R'),0);
		vl_saldo_repasse_w	:= nvl(obter_valor_repasse(nr_repasse_terceiro_w, 'R'),0);



		select	nvl(sum(a.vl_repasse),0)
		into	vl_repasse_proc_w
		from	convenio d,
			conta_paciente c,
			procedimento_paciente b,
			procedimento_repasse a
		where	a.nr_repasse_terceiro	= nr_repasse_terceiro_w
		and	a.nr_seq_procedimento	= b.nr_sequencia
		and	b.nr_interno_conta	= c.nr_interno_conta
		and	c.cd_convenio_parametro	= d.cd_convenio
		and	d.ie_tipo_convenio	= 2;

		select	nvl(sum(a.vl_repasse),0)
		into	vl_repasse_mat_w
		from	convenio d,
			conta_paciente c,
			material_atend_paciente b,
			material_repasse a
		where	a.nr_repasse_terceiro	= nr_repasse_terceiro_w
		and	a.nr_seq_material	= b.nr_sequencia
		and	b.nr_interno_conta	= c.nr_interno_conta
		and	c.cd_convenio_parametro	= d.cd_convenio
		and	d.ie_tipo_convenio	= 2;

		select	nvl(sum(vl_repasse),0)
		into	vl_repasse_regra_w
		from	repasse_terceiro_item a
		where	nr_repasse_terceiro     = nr_repasse_terceiro_w
		and	nr_seq_regra_esp	is not null;

		select	nvl(max(ie_tipo_convenio),0)
		into	ie_tipo_convenio_regra_w
		from	REPASSE_TERCEIRO_REGRA
		where	nr_repasse_terceiro	= nr_repasse_terceiro_w;

		if	(nvl(ie_item_repasse_w,'S') = 'S') then

			dt_inicial_repasse_w	:= trunc(dt_mesano_referencia_w,'month');
			dt_final_repasse_w	:= fim_dia(fim_mes(dt_mesano_referencia_w));
			
			open c04;
			loop
			fetch c04 into
				nr_seq_terc_rep_w,
				tx_repasse_w,
				nr_seq_terc_destino_w,
				nr_seq_trans_financ_w,
				vl_repassar_w,
				ie_saldo_w,
				ie_valor_w;
			exit when c04%notfound;

				if	(ie_valor_w = 'PMC') then
					--vl_repassar_w	:= vl_repasse_mat_w + vl_repasse_proc_w;
					if	(vl_repassar_w = 0) then
						vl_repasse_ie_valor_w	:= nvl(obter_valor_repasse(nr_repasse_terceiro_w, 'RPM'),0);--vl_repasse_mat_w + vl_repasse_proc_w;
					end if;		
				elsif	(ie_valor_w = 'R') then
					--vl_repassar_w	:= vl_repasse_regra_w;
					if	(vl_repassar_w = 0) then
						vl_repasse_ie_valor_w	:= nvl(obter_valor_repasse(nr_repasse_terceiro_w, 'RI'),0);--vl_repasse_regra_w;
					end if;
				end if;

				select	count(*)
				into	cont_w
				from	repasse_terceiro_item b,
					repasse_terceiro a
				where	a.nr_repasse_terceiro			= b.nr_repasse_terceiro
				and	a.dt_mesano_referencia			between dt_inicial_repasse_w and dt_final_repasse_w
				and	a.nr_seq_terceiro			= nr_seq_terc_destino_w
				and	b.nr_seq_terc_rep			= nr_seq_terc_rep_w;

				if	(cont_w = 0) then

					OBTER_REPASSE_TERCEIRO(dt_mesano_referencia_w,
								nr_seq_terc_destino_w,
								nm_usuario_p,			
								null,
								'I',
								nr_repasse_terceiro_dest_w,
								cd_estabelecimento_w,
								ie_tipo_convenio_w);

					if	(nvl(nr_repasse_terceiro_dest_w,0) > 0) then


						if	(ie_saldo_w = 'N') then
							if	(vl_repassar_w <> 0) then
								vl_item_w	:= vl_repassar_w;
							else
								vl_item_w	:= (dividir_sem_round(tx_repasse_w, 100) * vl_repasse_ie_valor_w); -- vl_repasse_w
							end if;
						elsif	(ie_saldo_w = 'S') then
							vl_item_w	:= (dividir_sem_round(tx_repasse_w, 100) * vl_saldo_repasse_w);
						end if;

						vl_saldo_repasse_w	:= vl_saldo_repasse_w - vl_item_w;

						/* Gerar valor negativo do repasse origem */
						select	nvl(max(nr_sequencia_item),0) + 1
						into	nr_sequencia_item_w
						from	repasse_terceiro_item
						where	nr_repasse_terceiro	= nr_repasse_terceiro_w;

						insert	into repasse_terceiro_item
							(nr_repasse_terceiro,
							nr_sequencia_item,
							vl_repasse,
							dt_atualizacao,
							nm_usuario,
							nr_lote_contabil,
							ds_observacao,
							nr_seq_trans_fin,
							dt_lancamento,
							nr_seq_terceiro,
							nr_sequencia,
							nr_seq_terc_rep,
							ie_partic_tributo)
						select	nr_repasse_terceiro_w,
							nr_sequencia_item_w,
							nvl(vl_item_w,0) * -1,
							sysdate,
							nm_usuario_p,
							0,
							wheb_mensagem_pck.get_texto(800045,'NM_TERCEIRO=' || obter_nome_terceiro(nr_seq_terceiro_w)),
							nr_seq_trans_financ_w,
							sysdate,
							nr_seq_terceiro_w,
							repasse_terceiro_item_seq.nextval,
							nr_seq_terc_rep_w,
							'S'
						from	dual;

						/* Gerar valor do repasse destino */
						select	nvl(max(nr_sequencia_item),0) + 1
						into	nr_sequencia_item_w
						from	repasse_terceiro_item
						where	nr_repasse_terceiro	= nr_repasse_terceiro_dest_w;

						insert	into repasse_terceiro_item
							(nr_repasse_terceiro,
							nr_sequencia_item,
							vl_repasse,
							dt_atualizacao,
							nm_usuario,
							nr_lote_contabil,
							ds_observacao,
							nr_seq_trans_fin,
							dt_lancamento,
							nr_seq_terceiro,
							nr_sequencia,
							nr_seq_terc_rep,
							ie_partic_tributo)
						select	nr_repasse_terceiro_dest_w,
							nr_sequencia_item_w,
							nvl(vl_item_w,0),
							sysdate,
							nm_usuario_p,
							0,
							wheb_mensagem_pck.get_texto(800045,'NM_TERCEIRO=' || obter_nome_terceiro(nr_seq_terceiro_w)),
							nr_seq_trans_financ_w,
							sysdate,
							nr_seq_terc_destino_w,
							repasse_terceiro_item_seq.nextval,
							nr_seq_terc_rep_w,
							'S'
						from	dual;
					end if;
				end if;

			end loop;
			close c04;
		end if;	
	
		obter_param_usuario(89,57,obter_perfil_ativo,nm_usuario_p,cd_estabelecimento_w,ie_calcular_item_w);

		if	(ie_calcular_item_w = 'G') and 
			(nvl(ie_item_repasse_w,'S') = 'S') then
			calcular_item_repasse(nr_repasse_terceiro_w,nm_usuario_p);
		end if;
	end if;
	
	select	count(*)
	into	qt_reg_proc_w
	from	procedimento_repasse
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;

	select	count(*)
	into	qt_reg_mat_w
	from	material_repasse
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;

	select	count(*)
	into	qt_reg_item_w
	from	repasse_terceiro_item
	where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
	
	if	(qt_reg_proc_w = 0) and
		(qt_reg_mat_w = 0) and
		(qt_reg_item_w = 0) and
		((ie_excluir_rep_zerado_w = 'S') or
		(nvl(qt_promat_w,0) = 0))then
		delete	from repasse_terceiro
		where	nr_repasse_terceiro	= nr_repasse_terceiro_w;
	end if;
	qt_repasse_w	:= qt_repasse_w + 1;	
end loop;
close c05;

END gerar_conta_repasse_medico;
/