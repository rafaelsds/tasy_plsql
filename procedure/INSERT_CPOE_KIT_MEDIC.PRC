create or replace
procedure INSERT_CPOE_KIT_MEDIC(
			cd_material_p		cpoe_kit_medic.cd_material%type,
			cd_kit_material_p	cpoe_kit_medic.cd_kit_material%type,
			nr_seq_mat_cpoe_p	cpoe_kit_medic.nr_seq_cpoe_material%type,
			nm_usuario_p		cpoe_kit_medic.nm_usuario%type) as
begin
if	(cd_material_p is not null) and
	(cd_kit_material_p is not null) and
	(nr_seq_mat_cpoe_p is not null) and
	(nm_usuario_p is not null) then
	begin
	insert into
	cpoe_kit_medic (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_material,
		cd_kit_material,
		nr_seq_cpoe_material
	) values (
		cpoe_kit_medic_seq.nextval,
		sysdate,
		nm_usuario_p,
		sysdate,
		nm_usuario_p,
		cd_material_p,
		cd_kit_material_p,
		nr_seq_mat_cpoe_p
	);
	commit;
	end;
end if;
end INSERT_CPOE_KIT_MEDIC;
/
