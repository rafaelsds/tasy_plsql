create or replace
procedure atualizar_copiar_prescricao(	nr_prescricao_p				number,
										ds_nutr_dieta_oral_selec_p	varchar2,
										ds_nutr_suplemento_selec_p	varchar2,
										ds_nutr_sne_selec_p			varchar2,
										ds_nutr_npt_selec_p			varchar2,
										ds_solucao_selec_p			varchar2,
										ds_medicamento_selec_p		varchar2,
										ds_material_selec_p			varchar2,
										ds_procedimento_selec_p		varchar2,
										ds_recomendacao_selec_p		varchar2,
										ds_banco_sangue_selec_p		varchar2,
										ds_oxigenioterapia_selec_p	varchar2,
										ds_hemod_prescricao_selec_p	varchar2,
										ds_hemod_solucao_selec_p	varchar2,
										ds_hemod_medicamento_selec_p varchar2,
										ds_anatomia_patologica_selec_p varchar2,
										ds_citopatologico_selec_p	varchar2,
										ds_histopatologico_selec_p	varchar2,
										ds_gasoterapia_selec_p		varchar2,
										ds_nutr_npt_ped_selec_p		varchar2,
										ds_nutr_dieta_oral_p		varchar2,
										ds_nutr_suplemento_p		varchar2,
										ds_nutr_sne_p				varchar2,
										ds_nutr_npt_p				varchar2,
										ds_solucao_p				varchar2,
										ds_medicamento_p			varchar2,
										ds_material_p				varchar2,
										ds_procedimento_p			varchar2,
										ds_recomendacao_p			varchar2,
										ds_banco_sangue_p			varchar2,
										ds_oxigenioterapia_p		varchar2,
										ds_hemod_prescricao_p		varchar2,
										ds_hemod_solucao_p			varchar2,
										ds_hemod_medicamento_p		varchar2,
										ds_anatomia_patologica_p	varchar2,
										ds_citopatologico_p			varchar2,
										ds_histopatologico_p		varchar2,
										ds_gasoterapia_p			varchar2,
										ds_nutr_npt_ped_p			varchar2,
										nm_usuario_p				varchar2,
										ds_nutr_leitderiv_p			varchar2,
										ds_nutr_leitderiv_selec_p	varchar2,
										ds_justificativa_elim_p		varchar2) is

begin
if	(nr_prescricao_p is not null) and
	(nm_usuario_p is not null) then
	begin
	
	elimina_item_copia_prescr(
		nr_prescricao_p,
		ds_nutr_dieta_oral_p,
		ds_nutr_suplemento_p,
		ds_nutr_sne_p,
		ds_nutr_npt_p,
		ds_solucao_p,
		ds_medicamento_p,
		ds_material_p,
		ds_procedimento_p,
		ds_recomendacao_p,
		ds_banco_sangue_p,
		ds_oxigenioterapia_p,
		ds_hemod_prescricao_p,
		ds_hemod_solucao_p,
		ds_hemod_medicamento_p,
		ds_anatomia_patologica_p,
		ds_citopatologico_p,
		ds_histopatologico_p,
		ds_gasoterapia_p,
		ds_nutr_npt_ped_p,
		ds_nutr_leitderiv_p); --- Parametro novo criado para o delphi para itens de leites e derivados

	atualizar_inf_copia_prescr(nr_prescricao_p,ds_nutr_dieta_oral_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_nutr_suplemento_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_nutr_sne_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_nutr_npt_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_solucao_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_medicamento_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_material_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_procedimento_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_recomendacao_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_banco_sangue_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_oxigenioterapia_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_hemod_prescricao_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_hemod_solucao_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_hemod_medicamento_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_anatomia_patologica_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_citopatologico_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_histopatologico_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_gasoterapia_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_nutr_npt_ped_selec_p,nm_usuario_p);
	atualizar_inf_copia_prescr(nr_prescricao_p,ds_nutr_leitderiv_selec_p,nm_usuario_p);
	
	update	prescr_material
	set		nr_seq_substituto	= null
	where   nr_seq_substituto is not null
	and		nr_prescricao		= nr_prescricao_p;
	
	if	(ds_justificativa_elim_p is not null) then
		update	prescr_medica
		set		ds_justificativa = substr(replace(decode(ds_justificativa, null, null, ds_justificativa || '$') || ds_justificativa_elim_p, '$', chr(10)), 1, 2000)
		where	nr_prescricao = nr_prescricao_p;
	end if;
	
	end;
end if;
commit;
end atualizar_copiar_prescricao;
/