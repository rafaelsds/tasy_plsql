create or replace
procedure san_imp_result_exames(
			cd_barras_p		Varchar2,
			dt_resultado_p		Date,
			cd_resultado_p		Varchar,
			nm_usuario_p		Varchar2,
			ds_invalidos_p	out	Varchar2) is 
			
lista_exames_w		dbms_sql.varchar2_table;			
nr_seq_exame_lote_w	number(10);
nr_seq_exame_w		number(10);
ds_resultado_w		varchar(255);
cd_resultado_w		varchar(4);
dt_liberacao_w		date;
cd_barras_w		varchar(13);

cursor c01 is
	select	nr_seq_exame, ds_resultado
	from 	san_resultado_maestro
	where 	cd_resultado = cd_resultado_w;

cursor c02 is
	select	dt_liberacao
	from	san_exame_realizado
	where 	nr_seq_exame_lote = nr_seq_exame_lote_w
	and	nr_seq_exame = nr_seq_exame_w;
begin
	if	(cd_barras_p is not null) then		
		lista_exames_w := obter_lista_string(cd_resultado_p, ',');
		ds_invalidos_p := '';
		cd_barras_w := substr(replace(cd_barras_p, '=', ''), 1, 13);
		
		-- Registro de Reserva (inicia com 'R')
		if 	(substr(cd_barras_w, 1, 1) = 'R') then
		
			select 	max(nr_sequencia)
			into	nr_seq_exame_lote_w
			from	san_exame_lote a
			where	a.nr_seq_reserva = substr(cd_barras_w, 2, length(cd_barras_w) - 1);
						
			if 	(nr_seq_exame_lote_w is null) then
				ds_invalidos_p := '01=' || cd_barras_w;
				gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794795)||' ' || cd_barras_w, nm_usuario_p);
			else
				
				for	i in lista_exames_w.first..lista_exames_w.last loop
					cd_resultado_w := lista_exames_w(i);
					
					open c01;
					fetch c01 into 	nr_seq_exame_w, 
							ds_resultado_w;
					
					if 	(C01%notfound) then
						ds_invalidos_p := ds_invalidos_p || '05=' || cd_resultado_w || '//';
						gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794830,
											'CD_RESULTADO='||cd_resultado_w||
											';CD_BARRAS='||cd_barras_w), nm_usuario_p);						
					elsif (nr_seq_exame_w is not null) then
						open c02;
						fetch c02 into 	dt_liberacao_w;
					
						if 	(C02%notfound) then						
							ds_invalidos_p := ds_invalidos_p || '06=' || cd_resultado_w || ',' || nr_seq_exame_w || '//';
							gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794832,
											'NR_SEQ_EXAME='||nr_seq_exame_w||
											';CD_RESULTADO='||cd_resultado_w||
											';CD_BARRAS='||cd_barras_w), nm_usuario_p);
						elsif (dt_liberacao_w is not null) then
							ds_invalidos_p := ds_invalidos_p || '07=' || nr_seq_exame_w || ',' || dt_liberacao_w || '//';
							gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794833,
											'NR_SEQ_EXAME='||nr_seq_exame_w||
											';CD_BARRAS='||cd_barras_w||
											';DT_LIBERACAO='||dt_liberacao_w), nm_usuario_p);
						else
							update	san_exame_realizado set
								ds_resultado = ds_resultado_w,
								dt_realizado = nvl(dt_resultado_p,sysdate)
							where 	nr_seq_exame_lote = nr_seq_exame_lote_w
							and	nr_seq_exame = nr_seq_exame_w
							and	dt_liberacao is null;
						end if;
						close c02;
					end if;	
					close c01;
				end loop;
				
				
			end if;
		
		-- Registro de Transfus�o (inicia com 'T')
		elsif 	(substr(cd_barras_w, 1, 1) = 'T') then
		
			select 	max(nr_sequencia)
			into	nr_seq_exame_lote_w
			from	san_exame_lote a
			where	a.nr_seq_transfusao = substr(cd_barras_w, 2, length(cd_barras_w) - 1);
						
			if 	(nr_seq_exame_lote_w is null) then
				ds_invalidos_p := '01=' || cd_barras_w;
				gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794795)||' ' || cd_barras_w, nm_usuario_p);
			else
				
				for	i in lista_exames_w.first..lista_exames_w.last loop
					cd_resultado_w := lista_exames_w(i);
					
					open c01;
					fetch c01 into 	nr_seq_exame_w, 
							ds_resultado_w;
					
					if 	(C01%notfound) then
						ds_invalidos_p := ds_invalidos_p || '08=' || cd_resultado_w || '//';
						gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794834,
											'CD_RESULTADO='||cd_resultado_w||
											';CD_BARRAS='||cd_barras_w), nm_usuario_p);
					elsif (nr_seq_exame_w is not null) then
						open c02;
						fetch c02 into 	dt_liberacao_w;
					
						if 	(C02%notfound) then						
							ds_invalidos_p := ds_invalidos_p || '09=' || cd_resultado_w || ',' || nr_seq_exame_w || '//';
							gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794835,
											'NR_SEQ_EXAME='||nr_seq_exame_w||
											';CD_RESULTADO='||cd_resultado_w||
											';CD_BARRAS='||cd_barras_w), nm_usuario_p);
						elsif (dt_liberacao_w is not null) then
							ds_invalidos_p := ds_invalidos_p || '10=' || nr_seq_exame_w || ',' || dt_liberacao_w || '//';
							gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794836,
											'NR_SEQ_EXAME='||nr_seq_exame_w||
											';CD_BARRAS='||cd_barras_w||
											';DT_LIBERACAO='||dt_liberacao_w), nm_usuario_p);
						else
							update	san_exame_realizado set
								ds_resultado = ds_resultado_w,
								dt_realizado = nvl(dt_resultado_p,sysdate)
							where 	nr_seq_exame_lote = nr_seq_exame_lote_w
							and	nr_seq_exame = nr_seq_exame_w
							and	dt_liberacao is null;
						end if;
						close c02;						
					end if;	
					close c01;
				end loop;
				
				
			end if;
		-- Registro de Doa��o / ISBT (tem sempre 13 d�gitos)
		elsif 	(Length(trim(cd_barras_w)) = 13) then
			
			select 	max(a.nr_sequencia)
			into	nr_seq_exame_lote_w
			from	san_exame_lote a,
				san_doacao b
			where	a.nr_seq_doacao = b.nr_sequencia
			and	obter_isbt_doador(a.nr_seq_doacao, NULL,'I')  = cd_barras_w;
			
			if 	(nr_seq_exame_lote_w is null) then
				ds_invalidos_p := '01=' || cd_barras_w;
				gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794795)||' ' || cd_barras_w, nm_usuario_p);
			else 
				
				for	i in lista_exames_w.first..lista_exames_w.last loop
					cd_resultado_w := lista_exames_w(i);
					
					open c01;
					fetch c01 into 	nr_seq_exame_w, 
							ds_resultado_w;
					
					if 	(C01%notfound) then						
						ds_invalidos_p := ds_invalidos_p || '02=' || cd_resultado_w || '//';
						gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794837,
											'CD_RESULTADO='||cd_resultado_w||
											';CD_BARRAS='||cd_barras_w), nm_usuario_p);
					elsif 	(nr_seq_exame_w is not null) then
						open c02;
						fetch c02 into 	dt_liberacao_w;
					
						if 	(C02%notfound) then						
							ds_invalidos_p := ds_invalidos_p || '03=' || cd_resultado_w || ',' || nr_seq_exame_w || '//';
							gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794838,
											'NR_SEQ_EXAME='||nr_seq_exame_w||
											';CD_RESULTADO='||cd_resultado_w||
											';CD_BARRAS='||cd_barras_w), nm_usuario_p);
						elsif 	(dt_liberacao_w is not null) then
							ds_invalidos_p := ds_invalidos_p || '04=' || nr_seq_exame_w || ',' || dt_liberacao_w || '//';
							gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794839,
											'NR_SEQ_EXAME='||nr_seq_exame_w||
											';CD_BARRAS='||cd_barras_w||
											';DT_LIBERACAO='||dt_liberacao_w), nm_usuario_p);
						else
							update	san_exame_realizado set
								ds_resultado = ds_resultado_w,
								dt_realizado = nvl(dt_resultado_p,sysdate)
							where 	nr_seq_exame_lote = nr_seq_exame_lote_w
							and	nr_seq_exame = nr_seq_exame_w
							and	dt_liberacao is null;
						end if;
						close c02;
					end if;						
					close c01;
				end loop;
				
				
			end if;
		
		else
			ds_invalidos_p := '01=' || cd_barras_w;
			gravar_log_tasy(9245, wheb_mensagem_pck.get_texto(794795)||' ' || cd_barras_w, nm_usuario_p);			
		end if;
	
	end if;

commit;

end san_imp_result_exames;
/
