DROP TABLE TASY.W_AGENDAS_LIBERADAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_AGENDAS_LIBERADAS
(
  NM_USUARIO      VARCHAR2(15 BYTE),
  CD_AGENDA       NUMBER(10)                    NOT NULL,
  CD_TIPO_AGENDA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGELIB_I1 ON TASY.W_AGENDAS_LIBERADAS
(CD_AGENDA, NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


GRANT SELECT ON TASY.W_AGENDAS_LIBERADAS TO NIVEL_1;

