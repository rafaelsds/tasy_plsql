ALTER TABLE TASY.MATERIAL_AUTORIZADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_AUTORIZADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_AUTORIZADO
(
  NR_ATENDIMENTO             NUMBER(10),
  NR_SEQ_AUTORIZACAO         NUMBER(10),
  CD_MATERIAL                NUMBER(6)          NOT NULL,
  QT_AUTORIZADA              NUMBER(15,3)       NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_OBSERVACAO              VARCHAR2(2000 BYTE),
  NM_USUARIO_APROV           VARCHAR2(15 BYTE),
  DT_APROVACAO               DATE,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  QT_SOLICITADA              NUMBER(15,3),
  NR_SEQUENCIA_AUTOR         NUMBER(10)         NOT NULL,
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_PRESCRICAO              NUMBER(14),
  NR_SEQ_PRESCRICAO          NUMBER(6),
  CD_MATERIAL_CONVENIO       VARCHAR2(20 BYTE),
  CD_CGC_FABRICANTE          VARCHAR2(14 BYTE),
  VL_UNITARIO                NUMBER(17,4),
  DS_MAT_CONVENIO            VARCHAR2(255 BYTE),
  NR_SEQ_FABRICANTE          NUMBER(10),
  IE_VALOR_CONTA             VARCHAR2(1 BYTE),
  PR_ADICIONAL               NUMBER(15,4),
  IE_ENVIAR                  VARCHAR2(1 BYTE),
  NR_SEQ_MARCA               NUMBER(10),
  NR_ORCAMENTO               VARCHAR2(20 BYTE),
  VL_COTADO                  NUMBER(15,2),
  NR_SEQ_REGRA_PLANO         NUMBER(10),
  NR_SEQ_MAT_AUTOR_CIRURGIA  NUMBER(10),
  NR_SEQ_OPME                NUMBER(10),
  NR_SEQ_AGRUP_REL           NUMBER(5),
  NR_ORDEM_COMPRA            NUMBER(10),
  NR_ITEM_OCI                NUMBER(5),
  IE_FATURAMENTO_DIRETO      VARCHAR2(1 BYTE),
  IE_REDUCAO_ACRESCIMO       VARCHAR2(1 BYTE),
  NM_USUARIO_FATUR_DIRETO    VARCHAR2(15 BYTE),
  CD_SETOR_FATUR_DIRETO      NUMBER(5),
  DT_FATUR_DIRETO            DATE,
  VL_TOTAL_AUTORIZADO        NUMBER(15,2),
  IE_ORIGEM_PRECO            NUMBER(2),
  IE_VIA_APLICACAO           VARCHAR2(5 BYTE),
  VL_ULTIMA_COMPRA           NUMBER(13,4),
  CD_CGC_ULTIMA_COMPRA       VARCHAR2(14 BYTE),
  DT_ULTIMA_COMPRA           DATE,
  IE_ENVIADO_TISS            VARCHAR2(1 BYTE),
  NR_SEQ_TUSS_MAT_ITEM       NUMBER(10),
  CD_MATERIAL_TUSS           NUMBER(15),
  DS_MATERIAL_TUSS           VARCHAR2(255 BYTE),
  CD_INTERVALO_MEDIC_QUIMIO  VARCHAR2(7 BYTE),
  NR_SEQ_REGRA_QUIMIO        NUMBER(10),
  CD_MATERIAL_INFORMADO      NUMBER(6),
  NR_SEQ_AUTOR_PACOTE        NUMBER(10),
  QT_DOSE_QUIMIO             NUMBER(18,6),
  IE_TIPO_PREC_ESPECIAL      VARCHAR2(2 BYTE),
  VL_ORCAMENTO               NUMBER(15,2),
  VL_IVA_AUTORIZADO          NUMBER(15,2),
  VL_COSEGURO                NUMBER(15,2),
  VL_IVA_COSEGURO            NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATAUTO_AUTCNPCT_FK_I ON TASY.MATERIAL_AUTORIZADO
(NR_SEQ_AUTOR_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUTO_AUTCONV_FK_I ON TASY.MATERIAL_AUTORIZADO
(NR_SEQUENCIA_AUTOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUTO_I1 ON TASY.MATERIAL_AUTORIZADO
(NR_SEQ_OPME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUTO_MATERIA_FK_I ON TASY.MATERIAL_AUTORIZADO
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUTO_MATFABR_FK_I ON TASY.MATERIAL_AUTORIZADO
(NR_SEQ_FABRICANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUTO_MATFABR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATAUTO_MATMARC_FK_I ON TASY.MATERIAL_AUTORIZADO
(CD_MATERIAL, NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          136K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUTO_PESJURI_FK_I ON TASY.MATERIAL_AUTORIZADO
(CD_CGC_FABRICANTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUTO_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATAUTO_PK ON TASY.MATERIAL_AUTORIZADO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUTO_PRESMAT_FK_I ON TASY.MATERIAL_AUTORIZADO
(NR_PRESCRICAO, NR_SEQ_PRESCRICAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATAUTO_SETATEN_FK_I ON TASY.MATERIAL_AUTORIZADO
(CD_SETOR_FATUR_DIRETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUTO_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATAUTO_VIAAPLI_FK_I ON TASY.MATERIAL_AUTORIZADO
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATAUTO_VIAAPLI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.material_autorizado_update
after update ON TASY.MATERIAL_AUTORIZADO for each row
declare
ds_titulo_w			varchar2(255) := WHEB_MENSAGEM_PCK.get_texto(311735); --Log de alteracao no material autorizado
ds_historico_w			varchar2(4000):= '';
dt_parametro_w			date	:= ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(sysdate);

ds_atributo_w			varchar2(255);
ds_valor_ant_w			varchar2(2000);
ds_valor_atual_w		varchar2(2000);
ds_valor_desc_ant_w		varchar2(2000);
ds_valor_desc_atual_w		varchar2(2000);

nr_seq_tipo_hist_mat_w		number(10,0) := 0;
begin

ds_atributo_w	 	:= '';
ds_valor_ant_w		:= '';
ds_valor_atual_w	:= '';

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	begin

		begin
		ds_titulo_w	:= 	substr(ds_titulo_w ||' '||:old.cd_material||' - '||substr(obter_desc_material(:old.cd_material),1,255),1,255);

		if	(nvl(:new.nr_seq_agrup_rel,0) <> nvl(:old.nr_seq_agrup_rel,0)) then
			ds_atributo_w	:= 'NR_SEQ_AGRUP_REL';
			ds_valor_ant_w	:= :old.nr_seq_agrup_rel;
			ds_valor_atual_w:= :new.nr_seq_agrup_rel;
			--	ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_AGRUP_REL - Antes: '|| :old.nr_seq_agrup_rel ||' Depois: '|| :new.nr_seq_agrup_rel ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nr_seq_opme,0) <> nvl(:old.nr_seq_opme,0)) then
			ds_atributo_w	:= 'NR_SEQ_OPME';
			ds_valor_ant_w	:= :old.nr_seq_opme;
			ds_valor_atual_w:= :new.nr_seq_opme;
			--	ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_OPME - Antes: '|| :old.nr_seq_opme ||' Depois: '|| :new.nr_seq_opme ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nr_seq_mat_autor_cirurgia,0) <> nvl(:old.nr_seq_mat_autor_cirurgia,0)) then
			ds_atributo_w	:= 'NR_SEQ_MAT_AUTOR_CIRURGIA';
			ds_valor_ant_w	:= :old.nr_seq_mat_autor_cirurgia;
			ds_valor_atual_w:= :new.nr_seq_mat_autor_cirurgia;
			--	ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_MAT_AUTOR_CIRURGIA - Antes: '|| :old.nr_seq_mat_autor_cirurgia ||' Depois: '|| :new.nr_seq_mat_autor_cirurgia ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nr_seq_regra_plano,0) <> nvl(:old.nr_seq_regra_plano,0)) then
			ds_atributo_w	:= 'NR_SEQ_REGRA_PLANO';
			ds_valor_ant_w	:= :old.nr_seq_regra_plano;
			ds_valor_atual_w:= :new.nr_seq_regra_plano;
			--	ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_REGRA_PLANO - Antes: '|| :old.nr_seq_regra_plano ||' Depois: '|| :new.nr_seq_regra_plano ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.vl_cotado,0) <> nvl(:old.vl_cotado,0)) then
			ds_atributo_w	:= 'VL_COTADO';
			ds_valor_ant_w	:= :old.vl_cotado;
			ds_valor_atual_w:= :new.vl_cotado;
			--	ds_historico_w := substr(ds_historico_w||'Campo: VL_COTADO - Antes: '|| :old.vl_cotado ||' Depois: '|| :new.vl_cotado ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nr_orcamento,0) <> nvl(:old.nr_orcamento,0)) then
			ds_atributo_w	:= 'NR_ORCAMENTO';
			ds_valor_ant_w	:= :old.nr_orcamento;
			ds_valor_atual_w:= :new.nr_orcamento;
		--		ds_historico_w := substr(ds_historico_w||'Campo: NR_ORCAMENTO - Antes: '|| :old.nr_orcamento ||' Depois: '|| :new.nr_orcamento ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nr_seq_marca,0) <> nvl(:old.nr_seq_marca,0)) then
			ds_atributo_w	:= 'NR_SEQ_MARCA';
			ds_valor_ant_w	:= :old.nr_seq_marca;
			ds_valor_atual_w:= :new.nr_seq_marca;
			--ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_MARCA - Antes: '|| :old.nr_seq_marca ||' Depois: '|| :new.nr_seq_marca ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.ie_enviar,0) <> nvl(:old.ie_enviar,0)) then
			ds_atributo_w	:= 'IE_ENVIAR';
			ds_valor_ant_w	:= :old.ie_enviar;
			ds_valor_atual_w:= :new.ie_enviar;
			--ds_historico_w := substr(ds_historico_w||'Campo: IE_ENVIAR - Antes: '|| :old.ie_enviar ||' Depois: '|| :new.ie_enviar ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.pr_adicional,0) <> nvl(:old.pr_adicional,0)) then
			ds_atributo_w	:= 'PR_ADICIONAL';
			ds_valor_ant_w	:= :old.pr_adicional;
			ds_valor_atual_w:= :new.pr_adicional;
			--ds_historico_w := substr(ds_historico_w||'Campo: PR_ADICIONAL - Antes: '|| :old.pr_adicional ||' Depois: '|| :new.pr_adicional ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.ie_valor_conta,0) <> nvl(:old.ie_valor_conta,0)) then
			ds_atributo_w	:= 'IE_VALOR_CONTA';
			ds_valor_ant_w	:= :old.ie_valor_conta;
			ds_valor_atual_w:= :new.ie_valor_conta;
			--ds_historico_w := substr(ds_historico_w||'Campo: IE_VALOR_CONTA - Antes: '|| :old.ie_valor_conta ||' Depois: '|| :new.ie_valor_conta ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nr_seq_fabricante,0) <> nvl(:old.nr_seq_fabricante,0)) then
			ds_atributo_w	:= 'NR_SEQ_FABRICANTE';
			ds_valor_ant_w	:= :old.nr_seq_fabricante;
			ds_valor_atual_w:= :new.nr_seq_fabricante;
			--ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_FABRICANTE - Antes: '|| :old.nr_seq_fabricante ||' Depois: '|| :new.nr_seq_fabricante ||chr(13)||chr(10),1,4000)
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.ds_mat_convenio,0) <> nvl(:old.ds_mat_convenio,0)) then
			ds_atributo_w	:= 'DS_MAT_CONVENIO';
			ds_valor_ant_w	:= :old.ds_mat_convenio;
			ds_valor_atual_w:= :new.ds_mat_convenio;
			--ds_historico_w := substr(ds_historico_w||'Campo: DS_MAT_CONVENIO - Antes: '|| :old.ds_mat_convenio ||' Depois: '|| :new.ds_mat_convenio ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.vl_unitario,0) <> nvl(:old.vl_unitario,0)) then
			ds_atributo_w	:= 'VL_UNITARIO';
			ds_valor_ant_w	:= :old.vl_unitario;
			ds_valor_atual_w:= :new.vl_unitario;
			--ds_historico_w := substr(ds_historico_w||'Campo: VL_UNITARIO - Antes: '|| :old.vl_unitario ||' Depois: '|| :new.vl_unitario ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.ie_origem_preco,0) <> nvl(:old.ie_origem_preco,0)) then
			ds_atributo_w	:= wheb_mensagem_pck.get_texto(802667);
			ds_valor_ant_w	:= nvl(substr(obter_valor_dominio(7061,:old.ie_origem_preco),1,120),wheb_mensagem_pck.get_texto(802666));
			ds_valor_atual_w:= Nvl(substr(obter_valor_dominio(7061,:new.ie_origem_preco),1,120),wheb_mensagem_pck.get_texto(802666));
			--ds_historico_w := substr(ds_historico_w||'Campo: Origem Preco - Antes: '|| nvl(substr(obter_valor_dominio(7061,:old.ie_origem_preco),1,120),wheb_mensagem_pck.get_texto(802666)) ||' Depois: '|| Nvl(substr(obter_valor_dominio(7061,:new.ie_origem_preco),1,120),wheb_mensagem_pck.get_texto(802666)) ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.cd_cgc_fabricante,0) <> nvl(:old.cd_cgc_fabricante,0)) then
			ds_atributo_w	:= 'CD_CGC_FABRICANTE';
			ds_valor_ant_w	:= :old.cd_cgc_fabricante;
			ds_valor_atual_w:= :new.cd_cgc_fabricante;
			--ds_historico_w := substr(ds_historico_w||'Campo: CD_CGC_FABRICANTE - Antes: '|| :old.cd_cgc_fabricante ||' Depois: '|| :new.cd_cgc_fabricante ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.cd_material_convenio,0) <> nvl(:old.cd_material_convenio,0)) then
			ds_atributo_w	:= 'CD_MATERIAL_CONVENIO';
			ds_valor_ant_w	:= :old.cd_material_convenio;
			ds_valor_atual_w:= :new.cd_material_convenio;
			--ds_historico_w := substr(ds_historico_w||'Campo: CD_MATERIAL_CONVENIO - Antes: '|| :old.cd_material_convenio ||' Depois: '|| :new.cd_material_convenio ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nr_seq_prescricao,0) <> nvl(:old.nr_seq_prescricao,0)) then
			ds_atributo_w	:= 'NR_SEQ_PRESCRICAO';
			ds_valor_ant_w	:= :old.nr_seq_prescricao;
			ds_valor_atual_w:= :new.nr_seq_prescricao;
			--ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_PRESCRICAO - Antes: '|| :old.nr_seq_prescricao ||' Depois: '|| :new.nr_seq_prescricao ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nr_prescricao,0) <> nvl(:old.nr_prescricao,0)) then
			ds_atributo_w	:= 'NR_PRESCRICAO';
			ds_valor_ant_w	:= :old.nr_prescricao;
			ds_valor_atual_w:= :new.nr_prescricao;
			--ds_historico_w := substr(ds_historico_w||'Campo: NR_PRESCRICAO - Antes: '|| :old.nr_prescricao ||' Depois: '|| :new.nr_prescricao ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.qt_solicitada,0) <> nvl(:old.qt_solicitada,0)) then
			ds_atributo_w	:= 'QT_SOLICITADA';
			ds_valor_ant_w	:= :old.qt_solicitada;
			ds_valor_atual_w:= :new.qt_solicitada;
			--ds_historico_w := substr(ds_historico_w||'Campo: QT_SOLICITADA - Antes: '|| :old.qt_solicitada ||' Depois: '|| :new.qt_solicitada ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.dt_aprovacao,dt_parametro_w) <> nvl(:old.dt_aprovacao,dt_parametro_w)) then
			ds_atributo_w	:= 'DT_APROVACAO';
			ds_valor_ant_w	:= PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_aprovacao,'timestamp',ESTABLISHMENT_TIMEZONE_UTILS.gettimezone);
			ds_valor_atual_w:= PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_aprovacao,'timestamp',ESTABLISHMENT_TIMEZONE_UTILS.gettimezone);
			--ds_historico_w := substr(ds_historico_w||'Campo: DT_APROVACAO - Antes: '|| to_char(:old.dt_aprovacao,'dd/mm/yyyy hh24:mi:ss') ||' Depois: '|| to_char(:new.dt_aprovacao,'dd/mm/yyyy hh24:mi:ss') ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nm_usuario_aprov,0) <> nvl(:old.nm_usuario_aprov,0)) then
			ds_atributo_w	:= 'NM_USUARIO_APROV';
			ds_valor_ant_w	:= :old.nm_usuario_aprov;
			ds_valor_atual_w:= :new.nm_usuario_aprov;
			--ds_historico_w := substr(ds_historico_w||'Campo: NM_USUARIO_APROV - Antes: '|| :old.nm_usuario_aprov ||' Depois: '|| :new.nm_usuario_aprov ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.ds_observacao,0) <> nvl(:old.ds_observacao,0)) then
			ds_atributo_w	:= 'DS_OBSERVACAO';
			ds_valor_ant_w	:= :old.ds_observacao;
			ds_valor_atual_w:= :new.ds_observacao;
			--ds_historico_w := substr(ds_historico_w||'Campo: DS_OBSERVACAO - Antes: '|| :old.ds_observacao ||' Depois: '|| :new.ds_observacao ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.qt_autorizada,0) <> nvl(:old.qt_autorizada,0)) then
			ds_atributo_w	:= 'QT_AUTORIZADA';
			ds_valor_ant_w	:= :old.qt_autorizada;
			ds_valor_atual_w:= :new.qt_autorizada;
			--ds_historico_w := substr(ds_historico_w||'Campo: QT_AUTORIZADA - Antes: '|| :old.qt_autorizada ||' Depois: '|| :new.qt_autorizada ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.cd_material,0) <> nvl(:old.cd_material,0)) then
			ds_atributo_w	:= 'CD_MATERIAL';
			ds_valor_ant_w	:= :old.cd_material;
			ds_valor_atual_w:= :new.cd_material;
			--ds_historico_w := substr(ds_historico_w||'Campo: CD_MATERIAL - Antes: '|| :old.cd_material ||' Depois: '|| :new.cd_material ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);



			SELECT	MAX(NR_SEQUENCIA)
			INTO	nr_seq_tipo_hist_mat_w
			FROM	AUTORIZACAO_TIPO_HIST
			WHERE	NVL(IE_TIPO,'X') = 'M'
			and	nvl(ie_situacao,'A') = 'A';

			if	(nr_seq_tipo_hist_mat_w > 0) then
				ds_valor_desc_ant_w	:= ds_valor_ant_w || ' - ' || substr(obter_desc_material(:old.cd_material),1,255);
				ds_valor_desc_atual_w	:= ds_valor_atual_w || ' - '|| substr(obter_desc_material(:new.cd_material),1,255);
			end if;

		end if;
		if	(nvl(:new.nr_seq_autorizacao,0) <> nvl(:old.nr_seq_autorizacao,0)) then
			ds_atributo_w	:= 'NR_SEQ_AUTORIZACAO';
			ds_valor_ant_w	:= :old.nr_seq_autorizacao;
			ds_valor_atual_w:= :new.nr_seq_autorizacao;
			--ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_AUTORIZACAO - Antes: '|| :old.nr_seq_autorizacao ||' Depois: '|| :new.nr_seq_autorizacao ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0)) then
			ds_atributo_w	:= 'NR_ATENDIMENTO';
			ds_valor_ant_w	:= :old.nr_atendimento;
			ds_valor_atual_w:= :new.nr_atendimento;
			--_historico_w := substr(ds_historico_w||'Campo: NR_ATENDIMENTO - Antes: '|| :old.nr_atendimento ||' Depois: '|| :new.nr_atendimento ||chr(13)||chr(10),1,4000);--
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		if	(nvl(:new.ie_reducao_acrescimo,0) <> nvl(:old.ie_reducao_acrescimo,0)) then
			ds_atributo_w	:= 'IE_REDUCAO_ACRESCIMO';
			ds_valor_ant_w	:= :old.ie_reducao_acrescimo;
			ds_valor_atual_w:= :new.ie_reducao_acrescimo;
			--ds_historico_w := substr(ds_historico_w||'Campo: IE_REDUCAO_ACRESCIMO - Antes: '|| :old.ie_reducao_acrescimo ||' Depois: '|| :new.ie_reducao_acrescimo ||chr(13)||chr(10),1,4000);
			ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
																							'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
																							'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
		end if;
		exception
		when others then
			ds_historico_w := 'X';
		end;

		begin

		if	(nvl(:new.cd_cgc_fabricante,0) <> nvl(:old.cd_cgc_fabricante,0)) then
			update	agenda_pac_consignado
			set	cd_cgc 		= :new.cd_cgc_fabricante,
				ds_fornecedor	= substr(obter_nome_pf_pj(null,:new.cd_cgc_fabricante),1,255)
			where	nr_seq_mat_autor= :new.nr_sequencia;
		end if;

		exception
		when others then
			ds_historico_w := 'X';
		end;

		if	(nvl(ds_historico_w,'X') <> 'X') then
			gravar_autor_conv_log_alter(:new.nr_sequencia_autor,ds_titulo_w,substr(ds_historico_w,1,2000),:new.nm_usuario);


			if	(nvl(nr_seq_tipo_hist_mat_w,0) > 0) then

				insert into autorizacao_convenio_hist
					(nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					nr_atendimento,
					nr_seq_autorizacao,
					ds_historico,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_sequencia_autor,
					nr_seq_tipo_hist,
					dt_liberacao,
					nm_usuario_lib)
				select	autorizacao_convenio_hist_seq.nextval,
					sysdate,
					:new.nm_usuario,
					:new.nr_atendimento,
					:new.nr_seq_autorizacao,
					WHEB_MENSAGEM_PCK.get_texto(312535) || ' ' || ds_valor_desc_ant_w || chr(13)|| WHEB_MENSAGEM_PCK.get_texto(182372) || ' ' ||ds_valor_desc_atual_w,
					sysdate,
					:new.nm_usuario,
					:new.nr_sequencia_autor,
					nr_seq_tipo_hist_mat_w,
					sysdate,
					:new.nm_usuario
				from	dual;


			end if;




		end if;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_autorizado_delete
After delete ON TASY.MATERIAL_AUTORIZADO for each row
declare
ie_gerar_historico_w	varchar2(15) := 'N';
begin

obter_param_usuario(3004,139, obter_perfil_ativo, :old.nm_usuario, wheb_usuario_pck.Get_cd_estabelecimento, ie_gerar_historico_w);

if	(nvl('N','N') = 'S') then

	insert into AUTORIZACAO_CONVENIO_HIST
		(NR_SEQUENCIA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		NR_ATENDIMENTO,
		DS_HISTORICO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		NR_SEQUENCIA_AUTOR)
	values	(
		autorizacao_convenio_hist_seq.nextval,
		sysdate,
		:old.nm_usuario,
		:old.nr_atendimento,
		substr(WHEB_MENSAGEM_PCK.get_texto(311724) || chr(13) || chr(10) || --Material exclu�do da autoriza��o
		:old.cd_material || ' - ' || substr(obter_desc_material(:old.cd_material),1,100) || chr(13) || chr(10) ||
		WHEB_MENSAGEM_PCK.get_texto(311725) || :old.qt_solicitada || chr(13) || chr(10) || --Qtde solicitada:
		WHEB_MENSAGEM_PCK.get_texto(311726) || :old.qt_autorizada,1,4000), ---Qtde autorizada:
		sysdate,
		:old.nm_usuario,
		:old.nr_sequencia_autor);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_autorizado_insert
before insert or update ON TASY.MATERIAL_AUTORIZADO for each row
declare

ie_tipo_atendimento_w	number(10);
cd_mat_convenio_w	varchar2(20)	:= '';
cd_grupo_w		varchar2(20)	:= '';
cd_cgc_w		varchar2(20);
cd_cgc_convert_w		varchar2(20);
nr_seq_conversao_w	number(10);
nr_atendimento_w		number(10);
cd_estabelecimento_w	number(10);
cd_convenio_w		number(10);
ds_mat_convenio_w	varchar2(255) := '';
ie_gerar_historico_w	varchar2(15) := 'N';
ie_carater_inter_sus_w	varchar2(2);
cd_categoria_w		varchar2(10);
tx_conversao_w		number(15,4) := 0;
cd_setor_origem_w	number(5,0);
cd_unidade_convenio_w	varchar2(5);
nr_seq_tuss_mat_item_w	number(10);
ie_clinica_w		atendimento_paciente.ie_clinica%type;
cd_material_tuss_w	material_tuss.cd_material_tuss%type;
ds_material_tuss_w	tuss_material_item.ds_material%type;
ds_historico_w		varchar(2000);

ie_aplicar_tx_conversao_w	varchar2(15);
begin
obter_param_usuario(3004,139, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.Get_cd_estabelecimento, ie_gerar_historico_w);
obter_param_usuario(3004,163, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.Get_cd_estabelecimento, ie_aplicar_tx_conversao_w);

/* os 51884 - gravar proc convenio */
select	a.cd_convenio,
	a.nr_atendimento,
	Nvl(a.cd_estabelecimento,to_number(obter_dados_autor_convenio(a.nr_sequencia, 'E'))),
	b.cd_cgc,
	to_number(substr(obter_tipo_atend_autor(a.nr_atendimento,a.nr_seq_agenda,a.nr_seq_agenda_consulta,nr_seq_age_integ,a.nr_seq_paciente_setor,'C'),1,1)),
	substr(Obter_dados_autor_convenio(a.nr_sequencia,'CC'),1,10),
	a.cd_setor_origem
into	cd_convenio_w,
	nr_atendimento_w,
	cd_estabelecimento_w,
	cd_cgc_w,
	ie_tipo_atendimento_w,
	cd_categoria_w,
	cd_setor_origem_w
from	convenio b,
	autorizacao_convenio a
where	a.nr_sequencia	= :new.nr_sequencia_autor
and	a.cd_convenio	= b.cd_convenio;

if	(nr_atendimento_w is not null) then
	select	ie_tipo_atendimento,
		ie_carater_inter_sus,
		ie_clinica
	into	ie_tipo_atendimento_w,
		ie_carater_inter_sus_w,
		ie_clinica_w
	from	atendimento_paciente
	where	nr_atendimento	= nr_atendimento_w;

end if;

if	(:new.cd_material is not null) then

	converte_material_convenio
		(cd_convenio_w,
		:new.cd_material,
		null,
		null,
		nvl(:new.cd_cgc_fabricante, cd_cgc_w),
		cd_estabelecimento_w,
		sysdate,
		null,
		cd_mat_convenio_w,
		cd_grupo_w,
		nr_seq_conversao_w,
		ie_tipo_atendimento_w,
		null,
		0,
		null,
		ie_carater_inter_sus_w,
		null,
		null,
		ie_clinica_w,
		:new.ie_tipo_prec_especial);

	:new.vl_ultima_compra		:= to_number(obter_dados_ultima_compra(cd_estabelecimento_w,:new.cd_material,'VU'));
	:new.cd_cgc_ultima_compra		:= obter_dados_ultima_compra(cd_estabelecimento_w,:new.cd_material,'PJ');
	:new.dt_ultima_compra		:= Obter_Data_Ultima_Compra(:new.cd_material,cd_estabelecimento_w); --to_date(obter_dados_ultima_compra(cd_estabelecimento_w,:new.cd_material,'DT'),'dd/mm/yyyy');

	DEFINE_MATERIAL_TUSS_AUTOR
		(cd_estabelecimento_w,
		:new.cd_material,
		cd_convenio_w,
		cd_categoria_w,
		ie_tipo_atendimento_w,
		:new.ie_origem_preco,
		sysdate,
		cd_material_tuss_w,
		nr_seq_tuss_mat_item_w,
		ds_material_tuss_w,
		:new.cd_cgc_fabricante,
		:new.nr_seq_marca,
		nr_atendimento_w);

		select decode(cd_material_tuss_w,'0',null, cd_material_tuss_w)
		into cd_material_tuss_w
		from dual;

	:new.nr_seq_tuss_mat_item		:= nr_seq_tuss_mat_item_w;
	:new.cd_material_tuss		:= cd_material_tuss_w;
	:new.ds_material_tuss		:= ds_material_tuss_w;

end if;

if	(nr_seq_conversao_w is not null) then

	begin
	select	nvl(a.tx_conversao_qtde,0),
		upper(a.cd_unidade_convenio),
		substr(a.ds_material_convenio,1,255)
	into	tx_conversao_w,
		cd_unidade_convenio_w,
		ds_mat_convenio_w
	from	conversao_material_convenio a
	where	a.nr_sequencia = nr_seq_conversao_w;
	exception
	when others then
		tx_conversao_w := 0;
		cd_unidade_convenio_w := null;
		ds_mat_convenio_w     := null;
	end;

	if 	(nvl(ie_aplicar_tx_conversao_w,'N') in ('S','A')) and
		(nvl(tx_conversao_w,0) > 0) and
		(:new.qt_solicitada > 0) and
        (Inserting or :old.qt_solicitada <> :new.qt_solicitada) then
		:new.qt_solicitada := dividir(:new.qt_solicitada,tx_conversao_w);
	end if;

	if	(cd_mat_convenio_w is not null) then
		:new.cd_material_convenio	:= cd_mat_convenio_w;
	end if;


	if	(ds_mat_convenio_w is not null) then
		:new.ds_mat_convenio		:= ds_mat_convenio_w;
	end if;


end if;

if	(nvl(ie_gerar_historico_w, 'N') = 'S') and
	(Inserting) then

	ds_historico_w := substr(:new.cd_material || ' - ' || substr(obter_desc_material(:new.cd_material),1,100) || ' - ' || :new.cd_cgc_fabricante || chr(13) || chr(10) || wheb_mensagem_pck.get_texto(311725) || :new.qt_solicitada,1,2000);

	gravar_autor_conv_log_alter(:new.nr_sequencia_autor, substr(wheb_mensagem_pck.get_texto(311727),1,100), substr(ds_historico_w,1,2000),:new.nm_usuario);

	insert into autorizacao_convenio_hist
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_atendimento,
		ds_historico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_sequencia_autor)
	values	(
		autorizacao_convenio_hist_seq.nextval,
		sysdate,
		:new.nm_usuario,
		:new.nr_atendimento,
		substr(WHEB_MENSAGEM_PCK.get_texto(311727) || chr(13) || chr(10) || --Incluido material na autorizacao
		:new.cd_material || ' - ' || substr(obter_desc_material(:new.cd_material),1,100) || chr(13) || chr(10) ||
		WHEB_MENSAGEM_PCK.get_texto(311725) || :new.qt_solicitada,1,4000), --Qtde solicitada:
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia_autor);

end if;

end;
/


ALTER TABLE TASY.MATERIAL_AUTORIZADO ADD (
  CONSTRAINT MATAUTO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_AUTORIZADO ADD (
  CONSTRAINT MATAUTO_AUTCNPCT_FK 
 FOREIGN KEY (NR_SEQ_AUTOR_PACOTE) 
 REFERENCES TASY.AUTOR_CONV_PACOTE (NR_SEQUENCIA),
  CONSTRAINT MATAUTO_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT MATAUTO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_FATUR_DIRETO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT MATAUTO_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FABRICANTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MATAUTO_AUTCONV_FK 
 FOREIGN KEY (NR_SEQUENCIA_AUTOR) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATAUTO_PRESMAT_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA),
  CONSTRAINT MATAUTO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATAUTO_MATFABR_FK 
 FOREIGN KEY (NR_SEQ_FABRICANTE) 
 REFERENCES TASY.MAT_FABRICANTE (NR_SEQUENCIA),
  CONSTRAINT MATAUTO_MATMARC_FK 
 FOREIGN KEY (CD_MATERIAL, NR_SEQ_MARCA) 
 REFERENCES TASY.MATERIAL_MARCA (CD_MATERIAL,NR_SEQUENCIA));

GRANT SELECT ON TASY.MATERIAL_AUTORIZADO TO NIVEL_1;

