ALTER TABLE TASY.CIRURGIA_DESCRICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIRURGIA_DESCRICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIRURGIA_DESCRICAO
(
  NR_CIRURGIA                NUMBER(10),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_DIAGNOSTICO             VARCHAR2(2000 BYTE),
  DS_CIRURGIA                LONG,
  DS_RESUMO_CIRURGIA         VARCHAR2(2000 BYTE),
  DS_DIAGNOSTICO_POS         VARCHAR2(2000 BYTE),
  DS_EXAME_RAD               VARCHAR2(2000 BYTE),
  DS_EXAME_ANATOMO           VARCHAR2(2000 BYTE),
  DT_LIBERACAO               DATE,
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_RESPONSAVEL             VARCHAR2(10 BYTE),
  IE_TIPO_DESCRICAO          VARCHAR2(3 BYTE)   NOT NULL,
  NR_SEQ_PROTOCOLO           NUMBER(10),
  DS_INTERCORRENCIA          VARCHAR2(2000 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DT_REGISTRO                DATE,
  NR_SEQ_PEPO                NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  DS_ACHADOS_OPERATORIOS     VARCHAR2(2000 BYTE),
  NR_SEQ_PROCEDIMENTO        NUMBER(10),
  DS_TECNICA_ANESTESICA      VARCHAR2(2000 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  QT_CARACTERES_DESC         NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  CD_DOENCA_CID              VARCHAR2(10 BYTE),
  CD_DOENCA_CID_PRE          VARCHAR2(10 BYTE),
  DS_PO_IMEDIATO             VARCHAR2(2000 BYTE),
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_EVOLUCAO                NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  CD_AVALIADOR_AUX           VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIRDESC_CIDDOEN_FK_I ON TASY.CIRURGIA_DESCRICAO
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRDESC_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRDESC_CIDDOEN_FK2_I ON TASY.CIRURGIA_DESCRICAO
(CD_DOENCA_CID_PRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRDESC_CIDDOEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRDESC_CIRPROT_FK_I ON TASY.CIRURGIA_DESCRICAO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRDESC_CIRPROT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRDESC_CIRURGI_FK_I ON TASY.CIRURGIA_DESCRICAO
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRDESC_PEPOCIR_FK_I ON TASY.CIRURGIA_DESCRICAO
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRDESC_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRDESC_PERFIL_FK_I ON TASY.CIRURGIA_DESCRICAO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRDESC_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRDESC_PESFISI_FK_I ON TASY.CIRURGIA_DESCRICAO
(CD_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRDESC_PESFISI_FK2_I ON TASY.CIRURGIA_DESCRICAO
(CD_AVALIADOR_AUX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIRDESC_PK ON TASY.CIRURGIA_DESCRICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRDESC_TASASDI_FK_I ON TASY.CIRURGIA_DESCRICAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRDESC_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRDESC_TASASDI_FK2_I ON TASY.CIRURGIA_DESCRICAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRDESC_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cirurgia_descricao_update
BEFORE UPDATE ON TASY.CIRURGIA_DESCRICAO FOR EACH ROW
DECLARE

qt_reg_w number(10);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	((:old.dt_inativacao is null) and (:new.dt_inativacao is not null)) and
	((:old.nm_usuario_inativacao is null) and (:new.nm_usuario_inativacao is not null)) and
	(:new.cd_evolucao is not null) then
	update	evolucao_paciente
	set	dt_inativacao 		= sysdate,
		nm_usuario_inativacao	= :new.nm_usuario_inativacao,
		ie_situacao		= 'I',
		ds_justificativa	= :new.ds_justificativa
	where	cd_evolucao		= :new.cd_evolucao
	and	dt_inativacao 		is null
	and	dt_liberacao		is not null;

	delete  pep_item_pendente
	where 	cd_evolucao 		= :new.cd_evolucao;

end if;



<<Final>>

qt_reg_w	:= 0;


END;
/


CREATE OR REPLACE TRIGGER TASY.CIRURGIA_DESCRICAO_DELETE
AFTER DELETE ON TASY.CIRURGIA_DESCRICAO FOR EACH ROW
DECLARE

qt_reg_w		number(1);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto final_trigger;
end if;

DELETE 	pep_item_pendente
WHERE  	ie_tipo_registro = 'CDC'
AND 	nvl(ie_tipo_pendencia,'l')	 = 'L'
AND	   	nr_seq_registro  = :old.nr_sequencia;

commit;

<<final_trigger>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_descricao_cir_atual_pend
AFTER INSERT OR UPDATE OR DELETE ON TASY.CIRURGIA_DESCRICAO FOR EACH ROW
DECLARE

cd_pessoa_fisica_w  pessoa_fisica.cd_pessoa_fisica%type;
nr_atendimento_w  atendimento_paciente.nr_atendimento%type;

BEGIN


    if (nvl(:new.nr_seq_pepo,:old.nr_seq_pepo) > 0) then
    
        select  max(c.nr_atendimento),
                max(c.cd_pessoa_fisica)
        into    nr_atendimento_w,
                cd_pessoa_fisica_w
        from    pepo_cirurgia c
        where   c.nr_sequencia = nvl(:new.nr_cirurgia, :old.nr_cirurgia);

    else

        select  max(c.nr_atendimento),
                max(c.cd_pessoa_fisica)
        into    nr_atendimento_w,
                cd_pessoa_fisica_w
        from    cirurgia c
        where   c.nr_cirurgia = nvl(:new.nr_cirurgia, :old.nr_cirurgia);

    end if;

     
    if inserting then
    
        if(:new.dt_liberacao is null) then
            Gerar_registro_pendente_PEP('CDC', :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, obter_usuario_pf(:new.CD_RESPONSAVEL));
        else
            Gerar_registro_pendente_PEP('XCDC', :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, obter_usuario_pf(:new.CD_RESPONSAVEL));    
        end if;
        
    elsif updating then
    
        if(:new.dt_liberacao is null)then
            if(:new.CD_RESPONSAVEL != :old.CD_RESPONSAVEL)then
                Gerar_registro_pendente_PEP('XCDC', :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, obter_usuario_pf(:old.CD_RESPONSAVEL));
                Gerar_registro_pendente_PEP('CDC', :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, obter_usuario_pf(:new.CD_RESPONSAVEL));
            end if;
        else
            Gerar_registro_pendente_PEP('XCDC', :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, obter_usuario_pf(:new.CD_RESPONSAVEL));
        end if;     

    elsif deleting then
        
        delete pep_item_pendente
        where ie_tipo_registro = 'CDC'
        and nr_atendimento = nr_atendimento_w
        and cd_pessoa_fisica = cd_pessoa_fisica_w
        and nr_seq_registro =  :old.nr_sequencia;
    
    end if;    
    

END;
/


CREATE OR REPLACE TRIGGER TASY.cirurgia_descricao_atual
AFTER UPDATE OR INSERT ON TASY.CIRURGIA_DESCRICAO FOR EACH ROW
DECLARE

cd_pessoa_fisica_w		pessoa_fisica.cd_pessoa_fisica%type;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
ie_tipo_w			varchar2(10);
ie_lib_desc_cirurgia	varchar2(5);

BEGIN
obter_param_usuario(872, 7, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, obter_estabelecimento_ativo, ie_lib_desc_cirurgia);

if (ie_lib_desc_cirurgia = 'S') then
	if	(:new.nr_seq_pepo > 0) then
		select	max(c.nr_atendimento),
				max(c.cd_pessoa_fisica)
		into	nr_atendimento_w,
				cd_pessoa_fisica_w
		from	pepo_cirurgia c
		where	c.nr_sequencia = :new.nr_seq_pepo;

	else
		select	max(c.nr_atendimento),
				max(c.cd_pessoa_fisica)
		into	nr_atendimento_w,
				cd_pessoa_fisica_w
		from	cirurgia c
		where	c.nr_cirurgia = :new.nr_cirurgia;

	end if;

	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'CDC';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCDC';
	end if;

	if (ie_tipo_w is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.CIRURGIA_DESCRICAO_INSUP
before insert or update ON TASY.CIRURGIA_DESCRICAO for each row
declare


begin
if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if (inserting and :new.IE_SITUACAO <> 'A') then
	:new.IE_SITUACAO := 'A';
end if;

end;
/


ALTER TABLE TASY.CIRURGIA_DESCRICAO ADD (
  CONSTRAINT CIRDESC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIRURGIA_DESCRICAO ADD (
  CONSTRAINT CIRDESC_PESFISI_FK2 
 FOREIGN KEY (CD_AVALIADOR_AUX) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIRDESC_PESFISI_FK 
 FOREIGN KEY (CD_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIRDESC_CIRPROT_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.CIRURGIA_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT CIRDESC_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT CIRDESC_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CIRDESC_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CIRDESC_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CIRDESC_CIDDOEN_FK2 
 FOREIGN KEY (CD_DOENCA_CID_PRE) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT CIRDESC_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT CIRDESC_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA));

GRANT SELECT ON TASY.CIRURGIA_DESCRICAO TO NIVEL_1;

