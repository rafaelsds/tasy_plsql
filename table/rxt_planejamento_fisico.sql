ALTER TABLE TASY.RXT_PLANEJAMENTO_FISICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RXT_PLANEJAMENTO_FISICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RXT_PLANEJAMENTO_FISICO
(
  NR_SEQUENCIA                    NUMBER(10)    NOT NULL,
  CD_ESTABELECIMENTO              NUMBER(4)     NOT NULL,
  DT_ATUALIZACAO                  DATE          NOT NULL,
  NM_USUARIO                      VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC             DATE,
  NM_USUARIO_NREC                 VARCHAR2(15 BYTE),
  NR_SEQ_FASE                     NUMBER(10)    NOT NULL,
  QT_X1                           NUMBER(17,3),
  QT_X2                           NUMBER(17,3),
  QT_Y1                           NUMBER(17,3),
  QT_Y2                           NUMBER(17,3),
  QT_ANGULO_GANTRY                NUMBER(17,3),
  QT_ANGULO_COLIMADOR             NUMBER(17,3),
  QT_ANGULO_MESA_X                NUMBER(17,3),
  QT_ANGULO_MESA_Y                NUMBER(17,3),
  QT_ANGULO_MESA_Z                NUMBER(17,3),
  DS_TAMANHO_CAMPO                VARCHAR2(80 BYTE),
  QT_CAMPO_EQUIVALENTE            NUMBER(17,3),
  QT_DISTANCIA_ISOCENTRO          NUMBER(17,3),
  QT_DOSE_TUMOR_REAL              NUMBER(17,3),
  QT_DOSE_TUMOR_DIARIA            NUMBER(17,3),
  QT_NUMERO_APLICACOES            NUMBER(17,3),
  QT_PROFUNDIDADE                 NUMBER(17,3),
  QT_PERCENT_DOSE_PROFUNDA        NUMBER(17,3),
  QT_TMR                          NUMBER(17,3),
  QT_RENDIMENTO_FINAL             NUMBER(17,3),
  QT_ABERTURA_COLIMADOR           NUMBER(17,3),
  QT_FATOR_RETROESPALHAMENTO      NUMBER(17,3),
  QT_FATOR_FILTRO                 NUMBER(17,3),
  QT_FATOR_DISTANCIA              NUMBER(17,3),
  QT_DIAMETRO_ANTERO_POST         NUMBER(17,3),
  QT_LATERO_LATERAL               NUMBER(17,3),
  QT_DISTANCIA_MESA_CENTRO        NUMBER(17,3),
  QT_UNIDADE_MONITOR              NUMBER(17,3),
  NR_SEQ_CAMPO                    NUMBER(10),
  QT_FATOR_BANDEJA                NUMBER(15,3),
  QT_TAM_X                        NUMBER(15,3),
  QT_TAM_Y                        NUMBER(15,3),
  IE_TIPO_X                       VARCHAR2(1 BYTE),
  IE_TIPO_Y                       VARCHAR2(1 BYTE),
  QT_SSD                          NUMBER(15,2),
  QT_ANGULO_MESA                  NUMBER(15,2),
  IE_SITUACAO                     VARCHAR2(1 BYTE),
  DT_LIBERACAO                    DATE,
  NM_USUARIO_LIB                  VARCHAR2(15 BYTE),
  DT_INATIVACAO                   DATE,
  NM_USUARIO_INATIVACAO           VARCHAR2(15 BYTE),
  NR_SEQ_ENERGIA_RAD              NUMBER(10),
  NR_SEQ_ENE_FOTONS               NUMBER(10),
  QT_ANGULO_GANTRY_FINAL          NUMBER(15,4),
  QT_ANG_COLIMADOR_FINAL          NUMBER(15,2),
  DS_CAMPO_TRATAMENTO             VARCHAR2(80 BYTE),
  NR_SEQ_RXT_CAMPO                NUMBER(10),
  DS_OBSERVACAO_FISICO            VARCHAR2(2000 BYTE),
  NR_SEQ_PLANEJAMENTO_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RXTPLFI_ESTABEL_FK_I ON TASY.RXT_PLANEJAMENTO_FISICO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTPLFI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RXTPLFI_PK ON TASY.RXT_PLANEJAMENTO_FISICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTPLFI_PK
  MONITORING USAGE;


CREATE INDEX TASY.RXTPLFI_RXTCAMP_FK_I ON TASY.RXT_PLANEJAMENTO_FISICO
(NR_SEQ_RXT_CAMPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RXTPLFI_RXTCAPR_FK_I ON TASY.RXT_PLANEJAMENTO_FISICO
(NR_SEQ_CAMPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTPLFI_RXTCAPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RXTPLFI_RXTENEE_FK_I ON TASY.RXT_PLANEJAMENTO_FISICO
(NR_SEQ_ENERGIA_RAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RXTPLFI_RXTENEE_FK2_I ON TASY.RXT_PLANEJAMENTO_FISICO
(NR_SEQ_ENE_FOTONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RXTPLFI_RXTFATR_FK_I ON TASY.RXT_PLANEJAMENTO_FISICO
(NR_SEQ_FASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTPLFI_RXTFATR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.RXT_PLANEJAMENTO_FISICO_tp  after update ON TASY.RXT_PLANEJAMENTO_FISICO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.QT_X1,1,4000),substr(:new.QT_X1,1,4000),:new.nm_usuario,nr_seq_w,'QT_X1',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CAMPO,1,4000),substr(:new.NR_SEQ_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CAMPO',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_Y1,1,4000),substr(:new.QT_Y1,1,4000),:new.nm_usuario,nr_seq_w,'QT_Y1',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_Y2,1,4000),substr(:new.QT_Y2,1,4000),:new.nm_usuario,nr_seq_w,'QT_Y2',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ANGULO_GANTRY,1,4000),substr(:new.QT_ANGULO_GANTRY,1,4000),:new.nm_usuario,nr_seq_w,'QT_ANGULO_GANTRY',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ANGULO_COLIMADOR,1,4000),substr(:new.QT_ANGULO_COLIMADOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_ANGULO_COLIMADOR',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ANGULO_MESA_X,1,4000),substr(:new.QT_ANGULO_MESA_X,1,4000),:new.nm_usuario,nr_seq_w,'QT_ANGULO_MESA_X',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ANGULO_MESA_Y,1,4000),substr(:new.QT_ANGULO_MESA_Y,1,4000),:new.nm_usuario,nr_seq_w,'QT_ANGULO_MESA_Y',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ANGULO_MESA_Z,1,4000),substr(:new.QT_ANGULO_MESA_Z,1,4000),:new.nm_usuario,nr_seq_w,'QT_ANGULO_MESA_Z',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TAMANHO_CAMPO,1,4000),substr(:new.DS_TAMANHO_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TAMANHO_CAMPO',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CAMPO_EQUIVALENTE,1,4000),substr(:new.QT_CAMPO_EQUIVALENTE,1,4000),:new.nm_usuario,nr_seq_w,'QT_CAMPO_EQUIVALENTE',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DISTANCIA_ISOCENTRO,1,4000),substr(:new.QT_DISTANCIA_ISOCENTRO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DISTANCIA_ISOCENTRO',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_TUMOR_REAL,1,4000),substr(:new.QT_DOSE_TUMOR_REAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_TUMOR_REAL',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_TUMOR_DIARIA,1,4000),substr(:new.QT_DOSE_TUMOR_DIARIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_TUMOR_DIARIA',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_NUMERO_APLICACOES,1,4000),substr(:new.QT_NUMERO_APLICACOES,1,4000),:new.nm_usuario,nr_seq_w,'QT_NUMERO_APLICACOES',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PROFUNDIDADE,1,4000),substr(:new.QT_PROFUNDIDADE,1,4000),:new.nm_usuario,nr_seq_w,'QT_PROFUNDIDADE',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PERCENT_DOSE_PROFUNDA,1,4000),substr(:new.QT_PERCENT_DOSE_PROFUNDA,1,4000),:new.nm_usuario,nr_seq_w,'QT_PERCENT_DOSE_PROFUNDA',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TMR,1,4000),substr(:new.QT_TMR,1,4000),:new.nm_usuario,nr_seq_w,'QT_TMR',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_RENDIMENTO_FINAL,1,4000),substr(:new.QT_RENDIMENTO_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_RENDIMENTO_FINAL',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ABERTURA_COLIMADOR,1,4000),substr(:new.QT_ABERTURA_COLIMADOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_ABERTURA_COLIMADOR',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_FATOR_RETROESPALHAMENTO,1,4000),substr(:new.QT_FATOR_RETROESPALHAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'QT_FATOR_RETROESPALHAMENTO',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_FATOR_FILTRO,1,4000),substr(:new.QT_FATOR_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'QT_FATOR_FILTRO',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_FATOR_DISTANCIA,1,4000),substr(:new.QT_FATOR_DISTANCIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_FATOR_DISTANCIA',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAMETRO_ANTERO_POST,1,4000),substr(:new.QT_DIAMETRO_ANTERO_POST,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAMETRO_ANTERO_POST',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LATERO_LATERAL,1,4000),substr(:new.QT_LATERO_LATERAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_LATERO_LATERAL',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DISTANCIA_MESA_CENTRO,1,4000),substr(:new.QT_DISTANCIA_MESA_CENTRO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DISTANCIA_MESA_CENTRO',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_UNIDADE_MONITOR,1,4000),substr(:new.QT_UNIDADE_MONITOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_UNIDADE_MONITOR',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_X2,1,4000),substr(:new.QT_X2,1,4000),:new.nm_usuario,nr_seq_w,'QT_X2',ie_log_w,ds_w,'RXT_PLANEJAMENTO_FISICO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RXT_PLANEJAMENTO_FISICO ADD (
  CONSTRAINT RXTPLFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RXT_PLANEJAMENTO_FISICO ADD (
  CONSTRAINT RXTPLFI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RXTPLFI_RXTCAPR_FK 
 FOREIGN KEY (NR_SEQ_CAMPO) 
 REFERENCES TASY.RXT_CAMPO_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT RXTPLFI_RXTFATR_FK 
 FOREIGN KEY (NR_SEQ_FASE) 
 REFERENCES TASY.RXT_FASE_TRATAMENTO (NR_SEQUENCIA),
  CONSTRAINT RXTPLFI_RXTCAMP_FK 
 FOREIGN KEY (NR_SEQ_RXT_CAMPO) 
 REFERENCES TASY.RXT_CAMPO (NR_SEQUENCIA),
  CONSTRAINT RXTPLFI_RXTENEE_FK 
 FOREIGN KEY (NR_SEQ_ENERGIA_RAD) 
 REFERENCES TASY.RXT_ENERGIA_EQUIP (NR_SEQUENCIA),
  CONSTRAINT RXTPLFI_RXTENEE_FK2 
 FOREIGN KEY (NR_SEQ_ENE_FOTONS) 
 REFERENCES TASY.RXT_ENERGIA_EQUIP (NR_SEQUENCIA),
  CONSTRAINT RXTPLFI_RXTPLFI_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.RXT_PLANEJAMENTO_FISICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RXT_PLANEJAMENTO_FISICO TO NIVEL_1;

