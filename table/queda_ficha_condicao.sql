ALTER TABLE TASY.QUEDA_FICHA_CONDICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUEDA_FICHA_CONDICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUEDA_FICHA_CONDICAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_FICHA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONDICAO      NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QUEFICO_PK ON TASY.QUEDA_FICHA_CONDICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEFICO_PK
  MONITORING USAGE;


CREATE INDEX TASY.QUEFICO_QUECOND_FK_I ON TASY.QUEDA_FICHA_CONDICAO
(NR_SEQ_CONDICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEFICO_QUECOND_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUEFICO_QUEFIOCO_FK_I ON TASY.QUEDA_FICHA_CONDICAO
(NR_SEQ_FICHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEFICO_QUEFIOCO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.QUEDA_FICHA_CONDICAO ADD (
  CONSTRAINT QUEFICO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUEDA_FICHA_CONDICAO ADD (
  CONSTRAINT QUEFICO_QUECOND_FK 
 FOREIGN KEY (NR_SEQ_CONDICAO) 
 REFERENCES TASY.QUEDA_CONDICAO (NR_SEQUENCIA),
  CONSTRAINT QUEFICO_QUEFIOCO_FK 
 FOREIGN KEY (NR_SEQ_FICHA) 
 REFERENCES TASY.QUEDA_FICHA_OCORRENCIA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.QUEDA_FICHA_CONDICAO TO NIVEL_1;

