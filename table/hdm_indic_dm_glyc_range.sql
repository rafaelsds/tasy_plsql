ALTER TABLE TASY.HDM_INDIC_DM_GLYC_RANGE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HDM_INDIC_DM_GLYC_RANGE CASCADE CONSTRAINTS;

CREATE TABLE TASY.HDM_INDIC_DM_GLYC_RANGE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_RANGE             VARCHAR2(255 BYTE)       NOT NULL,
  NR_MINIMUM           NUMBER(8,4),
  NR_MAXIMUM           NUMBER(8,4),
  NR_AGE_MINIMUM       NUMBER(3),
  NR_AGE_MAXIMUM       NUMBER(3),
  SI_SEX               VARCHAR2(1 BYTE),
  NR_RANGE_ORDER       NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.HDMIDGR_PK ON TASY.HDM_INDIC_DM_GLYC_RANGE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HDM_INDIC_DM_GLYC_RANGE ADD (
  CONSTRAINT HDMIDGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.HDM_INDIC_DM_GLYC_RANGE TO NIVEL_1;

