ALTER TABLE TASY.PLS_CONTA_AUDITOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_AUDITOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_AUDITOR
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_CONTA             NUMBER(10)           NOT NULL,
  NR_SEQ_GRUPO_MEMBRO_MED  NUMBER(10),
  NR_SEQ_GRUPO_MEMBRO_ENF  NUMBER(10),
  NR_SEQ_ANALISE           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCTAU_PK ON TASY.PLS_CONTA_AUDITOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAU_PLSANCO_FK_I ON TASY.PLS_CONTA_AUDITOR
(NR_SEQ_ANALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAU_PLSCOME_FK_I ON TASY.PLS_CONTA_AUDITOR
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAU_PLSMGRA_FK_I ON TASY.PLS_CONTA_AUDITOR
(NR_SEQ_GRUPO_MEMBRO_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCTAU_PLSMGRA_FK2_I ON TASY.PLS_CONTA_AUDITOR
(NR_SEQ_GRUPO_MEMBRO_ENF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_conta_auditor_delete
before delete ON TASY.PLS_CONTA_AUDITOR for each row
declare
ds_log_call_w		Varchar2(1500);
ds_observacao_w		Varchar2(4000);

begin

	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);
	ds_observacao_w :=	'An�lise: '||:old.nr_seq_analise||chr(13)||chr(10)||
				'Grupo membro m�d.: '||:old.nr_seq_grupo_membro_med||chr(13)||chr(10)||
				'Grupo membro enf.: '||:old.nr_seq_grupo_membro_enf;

	insert into plsprco_cta (	nr_sequencia, dt_atualizacao, nm_usuario,
					dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
					ds_log, ds_log_call, ds_funcao_ativa, nr_seq_conta,
					ie_aplicacao_tasy, nm_maquina, ie_opcao )
			values	(	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
					sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14), 'PLS_CONTA_AUDITOR',
					ds_observacao_w, ds_log_call_w, obter_funcao_ativa, :old.nr_seq_conta,
					pls_se_aplicacao_tasy, wheb_usuario_pck.get_machine, '0');

end;
/


ALTER TABLE TASY.PLS_CONTA_AUDITOR ADD (
  CONSTRAINT PLSCTAU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_AUDITOR ADD (
  CONSTRAINT PLSCTAU_PLSANCO_FK 
 FOREIGN KEY (NR_SEQ_ANALISE) 
 REFERENCES TASY.PLS_ANALISE_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCTAU_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCTAU_PLSMGRA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_MEMBRO_MED) 
 REFERENCES TASY.PLS_MEMBRO_GRUPO_AUD (NR_SEQUENCIA),
  CONSTRAINT PLSCTAU_PLSMGRA_FK2 
 FOREIGN KEY (NR_SEQ_GRUPO_MEMBRO_ENF) 
 REFERENCES TASY.PLS_MEMBRO_GRUPO_AUD (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTA_AUDITOR TO NIVEL_1;

