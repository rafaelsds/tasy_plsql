ALTER TABLE TASY.SPA_FORMA_NEGOCIACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SPA_FORMA_NEGOCIACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SPA_FORMA_NEGOCIACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_FORMA_NEGOCIACAO  NUMBER(5)                NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SPFONEG_PK ON TASY.SPA_FORMA_NEGOCIACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPFONEG_PK
  MONITORING USAGE;


CREATE INDEX TASY.SPFONEG_SPATIPO_FK_I ON TASY.SPA_FORMA_NEGOCIACAO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPFONEG_SPATIPO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SPA_FORMA_NEGOCIACAO ADD (
  CONSTRAINT SPFONEG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SPA_FORMA_NEGOCIACAO ADD (
  CONSTRAINT SPFONEG_SPATIPO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.SPA_TIPO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SPA_FORMA_NEGOCIACAO TO NIVEL_1;

