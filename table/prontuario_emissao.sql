ALTER TABLE TASY.PRONTUARIO_EMISSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRONTUARIO_EMISSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRONTUARIO_EMISSAO
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  NM_PESSOA_RECEB                VARCHAR2(60 BYTE),
  CD_PESSOA_FISICA               VARCHAR2(10 BYTE) NOT NULL,
  DT_IMPRESSAO                   DATE,
  NM_USUARIO_EMISSAO             VARCHAR2(15 BYTE),
  IE_ANAMNESE                    VARCHAR2(1 BYTE) NOT NULL,
  IE_EVOLUCAO                    VARCHAR2(1 BYTE) NOT NULL,
  IE_DIAGNOSTICO                 VARCHAR2(1 BYTE) NOT NULL,
  IE_ATESTADO                    VARCHAR2(1 BYTE) NOT NULL,
  IE_SINAIS_VITAIS               VARCHAR2(1 BYTE) NOT NULL,
  IE_PRESCRICAO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_EXAME_LAB                   VARCHAR2(1 BYTE) NOT NULL,
  IE_LAUDO                       VARCHAR2(1 BYTE) NOT NULL,
  IE_HISTORICO_SAUDE             VARCHAR2(1 BYTE) NOT NULL,
  IE_OFTALMOLOGIA                VARCHAR2(1 BYTE) NOT NULL,
  IE_ORIENTACAO_ALTA             VARCHAR2(1 BYTE) NOT NULL,
  IE_PARECER_MEDICO              VARCHAR2(1 BYTE) NOT NULL,
  IE_RECEITA                     VARCHAR2(1 BYTE) NOT NULL,
  IE_SAE                         VARCHAR2(1 BYTE) NOT NULL,
  IE_SOLIC_EXAME_EXT             VARCHAR2(1 BYTE) NOT NULL,
  IE_DECLARACAO_RECEB            VARCHAR2(1 BYTE) NOT NULL,
  IE_CONSENTIMENTO               VARCHAR2(1 BYTE) NOT NULL,
  CD_SETOR_ATENDIMENTO           NUMBER(5),
  DT_LIBERACAO                   DATE,
  DS_MAQUINA                     VARCHAR2(80 BYTE),
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  DT_INICIAL                     DATE,
  DT_FINAL                       DATE,
  CD_PESSOA_RETIROU              VARCHAR2(10 BYTE),
  NR_ATENDIMENTO                 NUMBER(10),
  IE_CONSULTA_AMB                VARCHAR2(1 BYTE) NOT NULL,
  IE_GANHO_PERDA                 VARCHAR2(1 BYTE) NOT NULL,
  IE_OBSTETRICIA_NASC            VARCHAR2(1 BYTE) NOT NULL,
  IE_AVALIACAO                   VARCHAR2(1 BYTE) NOT NULL,
  IE_CHECKUP                     VARCHAR2(1 BYTE) NOT NULL,
  IE_DIAG_TUMOR                  VARCHAR2(1 BYTE) NOT NULL,
  IE_TRAT_ONCO                   VARCHAR2(1 BYTE) NOT NULL,
  IE_SITUACAO                    VARCHAR2(1 BYTE),
  DT_INATIVACAO                  DATE,
  NM_USUARIO_INATIVACAO          VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA               VARCHAR2(255 BYTE),
  IE_AGENTES_ANESTESICOS         VARCHAR2(1 BYTE),
  IE_MEDICAMENTOS                VARCHAR2(1 BYTE),
  IE_TECNICA_DESCRICAO           VARCHAR2(1 BYTE),
  IE_TERAPIA_HIDROELETROLITICA   VARCHAR2(1 BYTE),
  IE_HEMODERIVADOS               VARCHAR2(1 BYTE),
  IE_POSICAO                     VARCHAR2(1 BYTE),
  IE_MATERIAIS                   VARCHAR2(1 BYTE),
  IE_PELE                        VARCHAR2(1 BYTE),
  IE_INCISAO                     VARCHAR2(1 BYTE),
  IE_CURATIVO                    VARCHAR2(1 BYTE),
  IE_APAE                        VARCHAR2(1 BYTE),
  IE_RESUMO_DESCRICAO            VARCHAR2(1 BYTE),
  IE_FATURAMENTO_MEDICO          VARCHAR2(1 BYTE),
  IE_LIBERACAO_SRA               VARCHAR2(1 BYTE),
  IE_TEMPLATE                    VARCHAR2(1 BYTE) NOT NULL,
  DS_UTC                         VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO               VARCHAR2(15 BYTE),
  IE_ALERTAS                     VARCHAR2(1 BYTE),
  DS_UTC_ATUALIZACAO             VARCHAR2(50 BYTE),
  CD_TRANSACAO_UNIMED            VARCHAR2(8 BYTE),
  IE_TIPO_INATIVACAO_UNIMED      VARCHAR2(5 BYTE),
  NR_SEQ_PERFIL_OPENEHR          NUMBER(10),
  NR_SEQ_EPISODIO                NUMBER(10),
  IE_KV_FORM                     VARCHAR2(1 BYTE),
  IE_ESCALA                      VARCHAR2(1 BYTE),
  IE_ENTLASSUSMANAGEMENT         VARCHAR2(1 BYTE),
  IE_PPR                         VARCHAR2(1 BYTE),
  IE_DISPOSITIVO                 VARCHAR2(1 BYTE),
  IE_AVALIACAO_LACRIMAL          VARCHAR2(1 BYTE),
  IE_GONIOSCOPIA                 VARCHAR2(1 BYTE),
  IE_CURVA_TENSIONAL             VARCHAR2(1 BYTE),
  IE_BIOMETRIA                   VARCHAR2(1 BYTE),
  IE_BIOMICROSCOPIA              VARCHAR2(1 BYTE),
  IE_FUNDOSCOPIA                 VARCHAR2(1 BYTE),
  IE_TOMOGRAFIA_OLHO             VARCHAR2(1 BYTE),
  IE_FOTOCOAGULACAO              VARCHAR2(1 BYTE),
  IE_CAPSULOTOMIA                VARCHAR2(1 BYTE),
  IE_ULTRASSONOGRAFIA            VARCHAR2(1 BYTE),
  IE_TOMOGRAFIA_COERENCIA        VARCHAR2(1 BYTE),
  IE_REFRACAO                    VARCHAR2(1 BYTE),
  IE_AUTO_REFRACAO               VARCHAR2(1 BYTE),
  IE_PUPILOMETRIA                VARCHAR2(1 BYTE),
  IE_MICROSCOPIA_ESPECULAR       VARCHAR2(1 BYTE),
  IE_MOTILIDADE_OCULAR_COMPLETA  VARCHAR2(1 BYTE),
  IE_ANGIOFLUORESCEINOGRAFIA     VARCHAR2(1 BYTE),
  IE_MOTILIDADE_OCULAR           VARCHAR2(1 BYTE),
  IE_TONOMETRIA_PNEUMATICA       VARCHAR2(1 BYTE),
  IE_ACUIDADE_VISUAL             VARCHAR2(1 BYTE),
  IE_RECEITA_OCULOS              VARCHAR2(1 BYTE),
  IE_ANAMNESE_INICIAL            VARCHAR2(1 BYTE),
  IE_EXAME_OCULAR_EXTERNO        VARCHAR2(1 BYTE),
  IE_TONOMETRIA_APLANACAO        VARCHAR2(1 BYTE),
  IE_CONDUTA                     VARCHAR2(1 BYTE),
  IE_PAQUIMETRIA                 VARCHAR2(1 BYTE),
  IE_CAMPIMETRIA                 VARCHAR2(1 BYTE),
  IE_CERATOMETRIA                VARCHAR2(1 BYTE),
  IE_POTENCIAL_ACUIDADE          VARCHAR2(1 BYTE),
  IE_IRIDECTOMIA                 VARCHAR2(1 BYTE),
  IE_DALTONISMO                  VARCHAR2(1 BYTE),
  IE_SOBRECARGA_HIDRICA          VARCHAR2(1 BYTE),
  IE_MAPEAMENTO_RETINA           VARCHAR2(1 BYTE),
  IE_RETINOGRAFIA                VARCHAR2(1 BYTE),
  IE_DISTANCIA_PUPILAR           VARCHAR2(1 BYTE),
  IE_ORIENTACOES_GERAIS          VARCHAR2(1 BYTE),
  IE_UTILIZADOS_CIRURGIA         VARCHAR2(1 BYTE),
  IE_TODOS                       VARCHAR2(1 BYTE),
  DS_OBSERVACAO                  VARCHAR2(2000 BYTE),
  IE_ABERROMETRIA_OCULAR         VARCHAR2(1 BYTE),
  IE_WOUND_MNGMNT                VARCHAR2(1 BYTE),
  IE_INTRAOPERATIVE_CARE         VARCHAR2(1 BYTE),
  IE_ECM                         VARCHAR2(1 BYTE),
  IE_INFECTION_CONTROL           VARCHAR2(1 BYTE),
  IE_ESTIMATED_DISCHARGE         VARCHAR2(1 BYTE),
  IE_ATTACH_ASSMNT               VARCHAR2(1 BYTE),
  IE_DSCHRG_TNSFR_SUMRY          VARCHAR2(1 BYTE),
  IE_EVENTS                      VARCHAR2(1 BYTE),
  IE_ATTACH_CONSENTS             VARCHAR2(1 BYTE),
  IE_ATTACH_JUST_REQ             VARCHAR2(1 BYTE),
  IE_JUSTF_REQUESTS              VARCHAR2(1 BYTE),
  IE_ATTACH_CARE_DIR             VARCHAR2(1 BYTE),
  IE_PROCEDURES                  VARCHAR2(1 BYTE),
  IE_CALL_HISTORY                VARCHAR2(1 BYTE),
  IE_NURSING_ACTIONS             VARCHAR2(1 BYTE),
  IE_ATTACH_DEATH_CERT           VARCHAR2(1 BYTE),
  IE_ICUNSW                      VARCHAR2(1 BYTE),
  IE_ATTACH_MPI                  VARCHAR2(1 BYTE),
  IE_DEATH_CERTF                 VARCHAR2(1 BYTE),
  IE_EQUIPMENT                   VARCHAR2(1 BYTE),
  IE_ATTACH_PATHOLOGY            VARCHAR2(1 BYTE),
  IE_TIME_MOTION                 VARCHAR2(1 BYTE),
  IE_ATTACH_IMAGING_DIAG         VARCHAR2(1 BYTE),
  IE_INSTRUMENT                  VARCHAR2(1 BYTE),
  IE_TOURNIQUET                  VARCHAR2(1 BYTE),
  IE_DIATHERMY                   VARCHAR2(1 BYTE),
  IE_SPEC_PATH                   VARCHAR2(1 BYTE),
  IE_DEMARCATION                 VARCHAR2(1 BYTE),
  IE_DEMARCATION_CONF            VARCHAR2(1 BYTE),
  IE_CARE_DIRECTIVE              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROEMIS_ATEPACI_FK_I ON TASY.PRONTUARIO_EMISSAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROEMIS_EHRPEREX_FK_I ON TASY.PRONTUARIO_EMISSAO
(NR_SEQ_PERFIL_OPENEHR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROEMIS_ESTABEL_FK_I ON TASY.PRONTUARIO_EMISSAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROEMIS_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROEMIS_PESFISI_FK_I ON TASY.PRONTUARIO_EMISSAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROEMIS_PESFISI_FK2_I ON TASY.PRONTUARIO_EMISSAO
(CD_PESSOA_RETIROU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROEMIS_PK ON TASY.PRONTUARIO_EMISSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROEMIS_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROEMIS_SETATEN_FK_I ON TASY.PRONTUARIO_EMISSAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROEMIS_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PRONTUARIO_EMISSAO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PRONTUARIO_EMISSAO FOR EACH ROW
DECLARE
 ds_param_integ_hl7_w varchar2(4000);
 ie_case_paciente_w	varchar2(1) := 'N';
 nr_atendimento_episodio_w	atendimento_paciente.nr_atendimento%type;
BEGIN
if (nvl(:old.DT_ATUALIZACAO_NREC,sysdate+10) <> :new.DT_ATUALIZACAO_NREC) and
 (:new.DT_ATUALIZACAO_NREC is not null) then
 :new.ds_utc  := obter_data_utc(:new.DT_ATUALIZACAO_NREC, 'HV');
end if;

if (nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
 (:new.DT_LIBERACAO is not null) then
 :new.ds_utc_atualizacao := obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if ((:new.DT_LIBERACAO is not null) and (:old.DT_LIBERACAO is null)) then
    ds_param_integ_hl7_w := 'nr_seq_prontuario_emissao='    || :new.nr_sequencia            || ';' ||
                            'nr_atendimento='               || :new.nr_atendimento          || ';' ||
                            'nr_seq_perfil_ehr='            || :new.nr_seq_perfil_openehr   || ';' ;
    gravar_agend_integracao(650, ds_param_integ_hl7_w);
end if;

if ((:new.DT_LIBERACAO is not null) and (:old.DT_LIBERACAO is null)) then
    if (:new.CD_TRANSACAO_UNIMED is not null) then

    if ((:new.CD_TRANSACAO_UNIMED = '00760') and (:new.IE_TIPO_INATIVACAO_UNIMED is null)) then
        :new.IE_TIPO_INATIVACAO_UNIMED := '03';
    end if;

        ds_param_integ_hl7_w := 'nr_seq_prontuario_emissao=' || :new.nr_sequencia              || ';' ||
                                'nr_atendimento='            || :new.nr_atendimento            || ';' ||
                                'cd_transacao='              || :new.CD_TRANSACAO_UNIMED       || ';' ||
                                'ie_tipo_inativacao='        || :new.IE_TIPO_INATIVACAO_UNIMED || ';' ;

        gravar_agend_integracao(682, ds_param_integ_hl7_w, :new.CD_SETOR_ATENDIMENTO);
    end if;
end if;

if 	(inserting) and
	(nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'de_DE') and
	(:new.nr_atendimento is null) then

	ie_case_paciente_w := obter_se_episodio_paciente(:new.nr_seq_episodio,:new.cd_pessoa_fisica);

	if (:new.nr_seq_episodio is not null and ie_case_paciente_w = 'S') then

		select	max(a.nr_atendimento)
		into	nr_atendimento_episodio_w
		from	atendimento_paciente a,
				episodio_paciente b
		where	a.nr_seq_episodio = b.nr_sequencia
		and		((b.nr_episodio is not null and b.nr_episodio = :new.nr_seq_episodio) or b.nr_sequencia = :new.nr_seq_episodio);

		:new.nr_atendimento := nr_atendimento_episodio_w;
	else
		:new.nr_atendimento := obter_atendimento_paciente(:new.cd_pessoa_fisica, wheb_usuario_pck.get_cd_estabelecimento);
	end if;
end if;

if	(inserting) and
	(:new.ds_maquina is null) then
	:new.ds_maquina := obter_origem_acesso(:new.nm_usuario, :new.dt_atualizacao);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_prontuario_emissao_atual

before insert or update ON TASY.PRONTUARIO_EMISSAO for each row
declare
 
begin

    if(:new.ie_avaliacao = 'S')then
        :new.ie_avaliacao := 'N';
    end if;

    if(:new.ie_oftalmologia = 'S')then
        :new.ie_oftalmologia := 'N';
    end if;
    
    
    if(:new.ie_parecer_medico = 'S')then
        :new.ie_parecer_medico := 'N';
    end if;
    
    
    if(:new.ie_declaracao_receb = 'S')then
        :new.ie_declaracao_receb := 'N';
    end if;
        
        
    if(:new.ie_consulta_amb = 'S')then
        :new.ie_consulta_amb := 'N';
    end if;
        
        
    if(:new.ie_checkup = 'S')then
        :new.ie_checkup := 'N';
    end if;
        
        
    if(:new.ie_template = 'S')then
        :new.ie_template := 'N';
    end if; 
        
    
    if(:new.ie_alertas = 'S')then
        :new.ie_alertas := 'N';
    end if;
       
     
    if(:new.ie_materiais = 'S')then
        :new.ie_materiais := 'N';
    end if;
       
     
    if(:new.ie_posicao = 'S')then
        :new.ie_posicao := 'N';
    end if;  
        
    
    if(:new.ie_pele = 'S')then
        :new.ie_pele := 'N';
    end if;
        
    
    if(:new.ie_incisao = 'S')then
        :new.ie_incisao := 'N';
    end if;
        
        
    if(:new.ie_curativo = 'S')then
        :new.ie_curativo := 'N';
    end if;
      
        
    if(:new.ie_faturamento_medico = 'S')then
        :new.ie_faturamento_medico := 'N';
    end if;
    
    if(:new.ie_sae = 'S')then
        :new.ie_sae := 'N';
    end if;    
end;
/


ALTER TABLE TASY.PRONTUARIO_EMISSAO ADD (
  CONSTRAINT PROEMIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRONTUARIO_EMISSAO ADD (
  CONSTRAINT PROEMIS_EHRPEREX_FK 
 FOREIGN KEY (NR_SEQ_PERFIL_OPENEHR) 
 REFERENCES TASY.EHR_PERFIL_EXPORTACAO (NR_SEQUENCIA),
  CONSTRAINT PROEMIS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROEMIS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PROEMIS_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RETIROU) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROEMIS_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PROEMIS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.PRONTUARIO_EMISSAO TO NIVEL_1;

