ALTER TABLE TASY.W_PLS_RELAT_CARTEIRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PLS_RELAT_CARTEIRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLS_RELAT_CARTEIRA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_SEGURADO       NUMBER(10),
  NR_LINHA              NUMBER(10),
  NR_COLUNA             NUMBER(10),
  NR_SEQ_TITULAR        NUMBER(10),
  DS_ENDERECO           VARCHAR2(4000 BYTE),
  NR_SEQ_BENEF_INTERNO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPLSRLC_PK ON TASY.W_PLS_RELAT_CARTEIRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPLSRLC_PK
  MONITORING USAGE;


CREATE INDEX TASY.WPLSRLC_PLSSEGU_FK_I ON TASY.W_PLS_RELAT_CARTEIRA
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPLSRLC_PLSSEGU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_PLS_RELAT_CARTEIRA ADD (
  CONSTRAINT WPLSRLC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_PLS_RELAT_CARTEIRA ADD (
  CONSTRAINT WPLSRLC_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_PLS_RELAT_CARTEIRA TO NIVEL_1;

