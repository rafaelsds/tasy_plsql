ALTER TABLE TASY.PLS_GUIA_DADOS_COMPL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GUIA_DADOS_COMPL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GUIA_DADOS_COMPL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_GUIA          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_NASCIMENTO        DATE,
  NR_CPF               VARCHAR2(11 BYTE),
  NR_IDENTIDADE        VARCHAR2(15 BYTE),
  IE_REVISAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSGUDC_PK ON TASY.PLS_GUIA_DADOS_COMPL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGUDC_PLSGUIA_FK_I ON TASY.PLS_GUIA_DADOS_COMPL
(NR_SEQ_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_GUIA_DADOS_COMPL_tp  after update ON TASY.PLS_GUIA_DADOS_COMPL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_NASCIMENTO,1,4000),substr(:new.DT_NASCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'PLS_GUIA_DADOS_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_IDENTIDADE,1,4000),substr(:new.NR_IDENTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_IDENTIDADE',ie_log_w,ds_w,'PLS_GUIA_DADOS_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'PLS_GUIA_DADOS_COMPL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_GUIA_DADOS_COMPL ADD (
  CONSTRAINT PLSGUDC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GUIA_DADOS_COMPL ADD (
  CONSTRAINT PLSGUDC_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_GUIA_DADOS_COMPL TO NIVEL_1;

