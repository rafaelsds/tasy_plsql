ALTER TABLE TASY.AG_LISTA_ESPERA_ESTAGIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AG_LISTA_ESPERA_ESTAGIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AG_LISTA_ESPERA_ESTAGIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ESTAGIO           VARCHAR2(255 BYTE)       NOT NULL,
  IE_ACAO              VARCHAR2(10 BYTE),
  IE_BLOQUEIO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AGLIESE_PK ON TASY.AG_LISTA_ESPERA_ESTAGIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ag_lista_espera_estagio_Atual
BEFORE INSERT OR UPDATE ON TASY.AG_LISTA_ESPERA_ESTAGIO FOR EACH ROW
DECLARE
qt_reg_w	number(10);
pragma autonomous_transaction;

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

SELECT	count(*)
into	qt_reg_w
from	ag_lista_espera_estagio
where	nr_sequencia <> :new.nr_sequencia
and		ie_acao = :new.ie_acao;

if (qt_reg_w > 0) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(1040663);
end if;

<<Final>>
qt_reg_w := 0;

END;
/


ALTER TABLE TASY.AG_LISTA_ESPERA_ESTAGIO ADD (
  CONSTRAINT AGLIESE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AG_LISTA_ESPERA_ESTAGIO TO NIVEL_1;

