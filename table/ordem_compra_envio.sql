ALTER TABLE TASY.ORDEM_COMPRA_ENVIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA_ENVIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA_ENVIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_ORDEM_COMPRA      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ENVIO             DATE                     NOT NULL,
  IE_FORMA_ENVIO       VARCHAR2(1 BYTE)         NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORDCOEN_ORDCOMP_FK_I ON TASY.ORDEM_COMPRA_ENVIO
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORDCOEN_PK ON TASY.ORDEM_COMPRA_ENVIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDCOEN_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_ENVIO_tp  after update ON TASY.ORDEM_COMPRA_ENVIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ENVIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'ORDEM_COMPRA_ENVIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'ORDEM_COMPRA_ENVIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'ORDEM_COMPRA_ENVIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_ENVIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ENVIO,1,4000),substr(:new.DT_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ENVIO',ie_log_w,ds_w,'ORDEM_COMPRA_ENVIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_ENVIO,1,4000),substr(:new.IE_FORMA_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_ENVIO',ie_log_w,ds_w,'ORDEM_COMPRA_ENVIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'ORDEM_COMPRA_ENVIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_COMPRA,1,4000),substr(:new.NR_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_ENVIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ORDEM_COMPRA_ENVIO ADD (
  CONSTRAINT ORDCOEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          448K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA_ENVIO ADD (
  CONSTRAINT ORDCOEN_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ORDEM_COMPRA_ENVIO TO NIVEL_1;

