ALTER TABLE TASY.PAT_LOCAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAT_LOCAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAT_LOCAL
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DS_LOCAL                 VARCHAR2(80 BYTE)    NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  CD_CENTRO_CUSTO          NUMBER(8),
  NR_SEQ_LOCAL_MANUTENCAO  NUMBER(10),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_LOCAL                 VARCHAR2(40 BYTE),
  CD_PF_RESP_LOCAL         VARCHAR2(10 BYTE),
  IE_VINCULO_BEM           VARCHAR2(1 BYTE)     NOT NULL,
  CD_CLASSIFICACAO         VARCHAR2(40 BYTE),
  CD_SETOR_ATENDIMENTO     NUMBER(5),
  QT_METRAGEM              NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PATLOCA_CENCUST_FK_I ON TASY.PAT_LOCAL
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATLOCA_ESTABEL_FK_I ON TASY.PAT_LOCAL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATLOCA_MANLOCA_FK_I ON TASY.PAT_LOCAL
(NR_SEQ_LOCAL_MANUTENCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATLOCA_PESFISI_FK_I ON TASY.PAT_LOCAL
(CD_PF_RESP_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PATLOCA_PK ON TASY.PAT_LOCAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATLOCA_SETATEN_FK_I ON TASY.PAT_LOCAL
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATLOCA_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PAT_LOCAL_ATUAL
before insert or update ON TASY.PAT_LOCAL for each row
declare

ie_situacao_w	varchar2(1);
cd_estab_centro_w	centro_custo.cd_estabelecimento%type;

begin

if	(:new.cd_setor_atendimento is not null) then

	select	max(ie_situacao)
	into	ie_situacao_w
	from	setor_atendimento
	where	cd_setor_atendimento	= :new.cd_setor_atendimento;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(184179);

	end if;
end if;

if	(nvl(:new.cd_centro_custo,0) <> 0) and
	(nvl(:new.cd_estabelecimento,0) <> 0) then
	begin
	select	max(a.cd_estabelecimento)
	into	cd_estab_centro_w
	from	centro_custo a
	where	a.cd_centro_custo = :new.cd_centro_custo;

	if	(nvl(cd_estab_centro_w,0) <> 0) and
		(nvl(cd_estab_centro_w,0) <> :new.cd_estabelecimento) then
		wheb_mensagem_pck.exibir_mensagem_abort(673890);
	end if;

	end;
end if;

end;
/


ALTER TABLE TASY.PAT_LOCAL ADD (
  CONSTRAINT PATLOCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PAT_LOCAL ADD (
  CONSTRAINT PATLOCA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PATLOCA_MANLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_MANUTENCAO) 
 REFERENCES TASY.MAN_LOCALIZACAO (NR_SEQUENCIA),
  CONSTRAINT PATLOCA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PATLOCA_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT PATLOCA_PESFISI_FK 
 FOREIGN KEY (CD_PF_RESP_LOCAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PAT_LOCAL TO NIVEL_1;

