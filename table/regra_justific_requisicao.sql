ALTER TABLE TASY.REGRA_JUSTIFIC_REQUISICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_JUSTIFIC_REQUISICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_JUSTIFIC_REQUISICAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_OBRIGA_JUSTIF      VARCHAR2(1 BYTE)        NOT NULL,
  CD_GRUPO_MATERIAL     NUMBER(3),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  CD_MATERIAL           NUMBER(6),
  CD_OPERACAO_ESTOQUE   NUMBER(3),
  IE_PADRAO_LOCAL       VARCHAR2(1 BYTE)        NOT NULL,
  CD_LOCAL_ESTOQUE      NUMBER(4),
  IE_URGENTE            VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REJUSRE_CLAMATE_FK_I ON TASY.REGRA_JUSTIFIC_REQUISICAO
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REJUSRE_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REJUSRE_ESTABEL_FK_I ON TASY.REGRA_JUSTIFIC_REQUISICAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REJUSRE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REJUSRE_GRUMATE_FK_I ON TASY.REGRA_JUSTIFIC_REQUISICAO
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REJUSRE_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REJUSRE_LOCESTO_FK_I ON TASY.REGRA_JUSTIFIC_REQUISICAO
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REJUSRE_MATERIA_FK_I ON TASY.REGRA_JUSTIFIC_REQUISICAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REJUSRE_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REJUSRE_OPEESTO_FK_I ON TASY.REGRA_JUSTIFIC_REQUISICAO
(CD_OPERACAO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REJUSRE_OPEESTO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REJUSRE_PK ON TASY.REGRA_JUSTIFIC_REQUISICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REJUSRE_SUBMATE_FK_I ON TASY.REGRA_JUSTIFIC_REQUISICAO
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REJUSRE_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_JUSTIFIC_REQUISICAO_tp  after update ON TASY.REGRA_JUSTIFIC_REQUISICAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_SUBGRUPO_MATERIAL,1,4000),substr(:new.CD_SUBGRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_MATERIAL',ie_log_w,ds_w,'REGRA_JUSTIFIC_REQUISICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MATERIAL,1,4000),substr(:new.CD_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MATERIAL',ie_log_w,ds_w,'REGRA_JUSTIFIC_REQUISICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'REGRA_JUSTIFIC_REQUISICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRAO_LOCAL,1,4000),substr(:new.IE_PADRAO_LOCAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRAO_LOCAL',ie_log_w,ds_w,'REGRA_JUSTIFIC_REQUISICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGA_JUSTIF,1,4000),substr(:new.IE_OBRIGA_JUSTIF,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGA_JUSTIF',ie_log_w,ds_w,'REGRA_JUSTIFIC_REQUISICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_MATERIAL,1,4000),substr(:new.CD_GRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_MATERIAL',ie_log_w,ds_w,'REGRA_JUSTIFIC_REQUISICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_OPERACAO_ESTOQUE,1,4000),substr(:new.CD_OPERACAO_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_OPERACAO_ESTOQUE',ie_log_w,ds_w,'REGRA_JUSTIFIC_REQUISICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'REGRA_JUSTIFIC_REQUISICAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_JUSTIFIC_REQUISICAO ADD (
  CONSTRAINT REJUSRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_JUSTIFIC_REQUISICAO ADD (
  CONSTRAINT REJUSRE_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT REJUSRE_OPEESTO_FK 
 FOREIGN KEY (CD_OPERACAO_ESTOQUE) 
 REFERENCES TASY.OPERACAO_ESTOQUE (CD_OPERACAO_ESTOQUE),
  CONSTRAINT REJUSRE_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT REJUSRE_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT REJUSRE_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT REJUSRE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT REJUSRE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.REGRA_JUSTIFIC_REQUISICAO TO NIVEL_1;

