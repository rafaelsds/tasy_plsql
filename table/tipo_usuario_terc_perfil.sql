ALTER TABLE TASY.TIPO_USUARIO_TERC_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_USUARIO_TERC_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_USUARIO_TERC_PERFIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_USUARIO  NUMBER(10)               NOT NULL,
  CD_PERFIL            NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TPUOTPFL_PERFIL_FK_I ON TASY.TIPO_USUARIO_TERC_PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TPUOTPFL_PK ON TASY.TIPO_USUARIO_TERC_PERFIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TPUOTPFL_TPUSUTER_FK_I ON TASY.TIPO_USUARIO_TERC_PERFIL
(NR_SEQ_TIPO_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_USUARIO_TERC_PERFIL ADD (
  CONSTRAINT TPUOTPFL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_USUARIO_TERC_PERFIL ADD (
  CONSTRAINT TPUOTPFL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TPUOTPFL_TPUSUTER_FK 
 FOREIGN KEY (NR_SEQ_TIPO_USUARIO) 
 REFERENCES TASY.TIPO_USUARIO_TERCEIRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TIPO_USUARIO_TERC_PERFIL TO NIVEL_1;

