ALTER TABLE TASY.KIT_ESTOQUE_REG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.KIT_ESTOQUE_REG CASCADE CONSTRAINTS;

CREATE TABLE TASY.KIT_ESTOQUE_REG
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_RESP          VARCHAR2(10 BYTE)     NOT NULL,
  DT_LIBERACAO            DATE,
  NM_USUARIO_LIB          VARCHAR2(15 BYTE),
  DS_REGISTRO_KIT         VARCHAR2(255 BYTE),
  DT_UTILIZACAO           DATE,
  NM_USUARIO_UTIL         VARCHAR2(15 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  IE_EXCLUIDO             VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_MOTIVO_EXCLUSAO  NUMBER(10),
  NR_SEQ_PEDIDO_AGENDA    NUMBER(10),
  IE_BASICO               VARCHAR2(15 BYTE)     NOT NULL,
  CD_LOCAL_ESTOQUE        NUMBER(4),
  IE_ORIGEM               VARCHAR2(15 BYTE),
  NR_SEQ_REG_ORIGEM       NUMBER(10),
  NR_SEQ_AGENDA           NUMBER(10),
  DS_LACRE                VARCHAR2(255 BYTE),
  IE_EXIGE_LACRE          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.KITESRE_AGEPACI_FK_I ON TASY.KIT_ESTOQUE_REG
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.KITESRE_AGEPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.KITESRE_AGEPAPE_FK_I ON TASY.KIT_ESTOQUE_REG
(NR_SEQ_PEDIDO_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KITESRE_ESTABEL_FK_I ON TASY.KIT_ESTOQUE_REG
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.KITESRE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.KITESRE_I1 ON TASY.KIT_ESTOQUE_REG
(CD_ESTABELECIMENTO, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.KITESRE_I1
  MONITORING USAGE;


CREATE INDEX TASY.KITESRE_I2 ON TASY.KIT_ESTOQUE_REG
(DT_UTILIZACAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KITESRE_KITESRE_FK_I ON TASY.KIT_ESTOQUE_REG
(NR_SEQ_REG_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.KITESRE_KITESRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.KITESRE_LOCESTO_FK_I ON TASY.KIT_ESTOQUE_REG
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.KITESRE_LOCESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.KITESRE_MOEXKIT_FK_I ON TASY.KIT_ESTOQUE_REG
(NR_SEQ_MOTIVO_EXCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.KITESRE_MOEXKIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.KITESRE_PESFISI_FK_I ON TASY.KIT_ESTOQUE_REG
(CD_PESSOA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.KITESRE_PK ON TASY.KIT_ESTOQUE_REG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.KIT_ESTOQUE_REG ADD (
  CONSTRAINT KITESRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.KIT_ESTOQUE_REG ADD (
  CONSTRAINT KITESRE_MOEXKIT_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_EXCLUSAO) 
 REFERENCES TASY.MOTIVO_EXCLUSAO_KIT (NR_SEQUENCIA),
  CONSTRAINT KITESRE_AGEPAPE_FK 
 FOREIGN KEY (NR_SEQ_PEDIDO_AGENDA) 
 REFERENCES TASY.AGENDA_PAC_PEDIDO (NR_SEQUENCIA),
  CONSTRAINT KITESRE_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT KITESRE_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT KITESRE_KITESRE_FK 
 FOREIGN KEY (NR_SEQ_REG_ORIGEM) 
 REFERENCES TASY.KIT_ESTOQUE_REG (NR_SEQUENCIA),
  CONSTRAINT KITESRE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT KITESRE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.KIT_ESTOQUE_REG TO NIVEL_1;

