ALTER TABLE TASY.CIH_PROC_REACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIH_PROC_REACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIH_PROC_REACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  NR_SEQ_REACAO        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIHPRREA_CIHPROC_FK_I ON TASY.CIH_PROC_REACAO
(CD_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHPRREA_CIHPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIHPRREA_CIHREAC_FK_I ON TASY.CIH_PROC_REACAO
(NR_SEQ_REACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHPRREA_CIHREAC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CIHPRREA_PK ON TASY.CIH_PROC_REACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHPRREA_PK
  MONITORING USAGE;


ALTER TABLE TASY.CIH_PROC_REACAO ADD (
  CONSTRAINT CIHPRREA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIH_PROC_REACAO ADD (
  CONSTRAINT CIHPRREA_CIHPROC_FK 
 FOREIGN KEY (CD_PROCEDIMENTO) 
 REFERENCES TASY.CIH_PROCEDIMENTO (CD_PROCEDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT CIHPRREA_CIHREAC_FK 
 FOREIGN KEY (NR_SEQ_REACAO) 
 REFERENCES TASY.CIH_REACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CIH_PROC_REACAO TO NIVEL_1;

