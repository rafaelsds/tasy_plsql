ALTER TABLE TASY.AUTORIZACAO_CONV_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUTORIZACAO_CONV_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUTORIZACAO_CONV_ETAPA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_AUTOR_CONV       NUMBER(10),
  NR_SEQ_AUTOR_CIR        NUMBER(10),
  DT_ETAPA                DATE                  NOT NULL,
  NR_SEQ_ETAPA            NUMBER(10)            NOT NULL,
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  DT_FIM_ETAPA            DATE,
  DT_INICIO_VIGENCIA      DATE,
  DT_FIM_VIGENCIA         DATE,
  CD_PESSOA_EXEC          VARCHAR2(10 BYTE),
  DT_RECEBIMENTO          DATE,
  NM_USUARIO_RECEBIMENTO  VARCHAR2(15 BYTE),
  VL_AUTORIZACAO          NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUTCOET_AUTCIRU_FK_I ON TASY.AUTORIZACAO_CONV_ETAPA
(NR_SEQ_AUTOR_CIR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCOET_AUTCIRU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCOET_AUTCONV_FK_I ON TASY.AUTORIZACAO_CONV_ETAPA
(NR_SEQ_AUTOR_CONV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCOET_AUTCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCOET_AUTORET_FK_I ON TASY.AUTORIZACAO_CONV_ETAPA
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCOET_AUTORET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCOET_PESFISI_FK_I ON TASY.AUTORIZACAO_CONV_ETAPA
(CD_PESSOA_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AUTCOET_PK ON TASY.AUTORIZACAO_CONV_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCOET_SETATEN_FK_I ON TASY.AUTORIZACAO_CONV_ETAPA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCOET_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCOET_USUARIO_FK_I ON TASY.AUTORIZACAO_CONV_ETAPA
(NM_USUARIO_RECEBIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCOET_USUARIO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.autorizacao_conv_etapa_update
after update ON TASY.AUTORIZACAO_CONV_ETAPA for each row
declare
ds_titulo_w			varchar2(255) 	:= wheb_mensagem_pck.get_texto(313582);
ds_historico_w			varchar2(4000)	:= '';
dt_parametro_w			date		:= trunc(sysdate) + 89399/89400;
ds_txt_campo_w			varchar2(30) 	:= wheb_mensagem_pck.get_texto(182370); --Campo:
ds_txt_antes_w			varchar2(30) 	:= wheb_mensagem_pck.get_texto(182371); -- - Antes:
ds_txt_depois_w			varchar2(30) 	:= wheb_mensagem_pck.get_texto(182372); -- Depois:
begin

begin
if (nvl(:new.vl_autorizacao,0) <> nvl(:old.vl_autorizacao,0)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' VL_AUTORIZACAO '|| ds_txt_antes_w ||' '|| :old.vl_autorizacao ||' '||ds_txt_depois_w ||' '|| :new.vl_autorizacao ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.nm_usuario_recebimento,0) <> nvl(:old.nm_usuario_recebimento,0)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' NM_USUARIO_RECEBIMENTO '|| ds_txt_antes_w ||' '|| :old.nm_usuario_recebimento ||' '||ds_txt_depois_w ||' '|| :new.nm_usuario_recebimento ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.dt_recebimento,dt_parametro_w) <> nvl(:old.dt_recebimento,dt_parametro_w)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' DT_RECEBIMENTO '|| ds_txt_antes_w ||' '|| to_char(:old.dt_recebimento,'dd/mm/yyyy hh24:mi:ss') ||' '||ds_txt_depois_w ||' '|| to_char(:new.dt_recebimento,'dd/mm/yyyy hh24:mi:ss') ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.cd_pessoa_exec,0) <> nvl(:old.cd_pessoa_exec,0)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' CD_PESSOA_EXEC '|| ds_txt_antes_w ||' '|| :old.cd_pessoa_exec ||' '|| ds_txt_depois_w ||' '|| :new.cd_pessoa_exec ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.dt_fim_vigencia,dt_parametro_w) <> nvl(:old.dt_fim_vigencia,dt_parametro_w)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' DT_FIM_VIGENCIA '|| ds_txt_antes_w ||' '|| to_char(:old.dt_fim_vigencia,'dd/mm/yyyy hh24:mi:ss') ||' '||ds_txt_depois_w ||' '|| to_char(:new.dt_fim_vigencia,'dd/mm/yyyy hh24:mi:ss') ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.dt_inicio_vigencia,dt_parametro_w) <> nvl(:old.dt_inicio_vigencia,dt_parametro_w)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' DT_INICIO_VIGENCIA '|| ds_txt_antes_w ||' '|| to_char(:old.dt_inicio_vigencia,'dd/mm/yyyy hh24:mi:ss') ||' '||ds_txt_depois_w ||' '|| to_char(:new.dt_inicio_vigencia,'dd/mm/yyyy hh24:mi:ss') ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.dt_fim_etapa,dt_parametro_w) <> nvl(:old.dt_fim_etapa,dt_parametro_w)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' DT_FIM_ETAPA '|| ds_txt_antes_w ||' '|| to_char(:old.dt_fim_etapa,'dd/mm/yyyy hh24:mi:ss') ||' '||ds_txt_depois_w ||' '|| to_char(:new.dt_fim_etapa,'dd/mm/yyyy hh24:mi:ss') ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.ds_observacao,0) <> nvl(:old.ds_observacao,0)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' DS_OBSERVACAO '|| ds_txt_antes_w ||' '|| :old.ds_observacao ||' '||ds_txt_depois_w ||' '|| :new.ds_observacao ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.cd_setor_atendimento,0) <> nvl(:old.cd_setor_atendimento,0)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' CD_SETOR_ATENDIMENTO '|| ds_txt_antes_w ||' '|| :old.cd_setor_atendimento ||' '||ds_txt_depois_w ||' '|| :new.cd_setor_atendimento ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.nr_seq_etapa,0) <> nvl(:old.nr_seq_etapa,0)) then
	ds_historico_w := substr(ds_historico_w||ds_txt_campo_w || ' NR_SEQ_ETAPA '|| ds_txt_antes_w ||' '|| :old.nr_seq_etapa ||' '||ds_txt_depois_w ||' '|| :new.nr_seq_etapa ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.nr_seq_autor_cir,0) <> nvl(:old.nr_seq_autor_cir,0)) then
	ds_historico_w  := substr(ds_historico_w||ds_txt_campo_w || ' NR_SEQ_AUTOR_CIR '|| ds_txt_antes_w ||' '|| :old.nr_seq_autor_cir ||' '||ds_txt_depois_w ||' '|| :new.nr_seq_autor_cir ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.dt_etapa,dt_parametro_w) <> nvl(:old.dt_etapa,dt_parametro_w)) then
	ds_historico_w  := substr(ds_historico_w||ds_txt_campo_w || ' DT_ETAPA '|| ds_txt_antes_w ||' '|| to_char(:old.dt_etapa,'dd/mm/yyyy hh24:mi:ss') ||' '||ds_txt_depois_w ||' '|| to_char(:new.dt_etapa,'dd/mm/yyyy hh24:mi:ss') ||chr(13)||chr(10),1,4000);
end if;
exception
when others then
	ds_historico_w := 'X';
end;

if (nvl(ds_historico_w,'X') <> 'X') then
	gravar_autor_conv_log_alter(:new.nr_seq_autor_conv,ds_titulo_w,substr(ds_historico_w,1,2000),:new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.autorizacao_conv_etapa_insert
before insert ON TASY.AUTORIZACAO_CONV_ETAPA for each row
declare

dt_inicio_vigencia_w	date;
dt_fim_vigencia_w	date;
nr_seq_autor_conv_w	number(10);
ie_param_w		varchar2(1);


pragma autonomous_transaction;

begin

if	(:new.nr_seq_autor_conv is null) and
	(:new.nr_seq_autor_cir is not null) then
	begin

	select	max(nr_seq_autor_conv)
	into	nr_seq_autor_conv_w
	from	autorizacao_cirurgia
	where	nr_sequencia = :new.nr_seq_autor_cir;

	if	(nr_seq_autor_conv_w is not null) then
		begin
		:new.nr_seq_autor_conv  := nr_seq_autor_conv_w;
		end;
	end if;

	end;
end if;

select	max(dt_inicio_vigencia),
	max(dt_fim_vigencia)
into	dt_inicio_vigencia_w,
	dt_fim_vigencia_w
from	autorizacao_convenio
where 	nr_sequencia	= :new.nr_seq_autor_conv;

if	(dt_inicio_vigencia_w is not null) and
	(:new.dt_inicio_vigencia is null) then
	:new.dt_inicio_vigencia	:= dt_inicio_vigencia_w;
end if;

if	(dt_fim_vigencia_w is not null) and
	(:new.dt_fim_vigencia is null) then
	:new.dt_fim_vigencia	:= dt_fim_vigencia_w;
end if;

if	(:new.nr_seq_autor_conv is not null) then
	begin
	:new.vl_autorizacao		:=  nvl(obter_valor_autorizacao(:new.nr_seq_autor_conv),0);
	exception
		when others then
		:new.vl_autorizacao		:= 0;
	end;
end if;

obter_param_usuario(3004, 116, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_param_w);

if (ie_param_w = 'S') then

	update	autorizacao_conv_etapa
	set     dt_fim_etapa = sysdate
	where   nr_seq_autor_conv = :new.nr_seq_autor_conv
	and     dt_fim_etapa is null
	and     nr_sequencia <> :new.nr_sequencia;

end if;

commit;

end;
/


ALTER TABLE TASY.AUTORIZACAO_CONV_ETAPA ADD (
  CONSTRAINT AUTCOET_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUTORIZACAO_CONV_ETAPA ADD (
  CONSTRAINT AUTCOET_AUTCIRU_FK 
 FOREIGN KEY (NR_SEQ_AUTOR_CIR) 
 REFERENCES TASY.AUTORIZACAO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT AUTCOET_AUTCONV_FK 
 FOREIGN KEY (NR_SEQ_AUTOR_CONV) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCOET_AUTORET_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.AUTORIZACAO_ETAPA (NR_SEQUENCIA),
  CONSTRAINT AUTCOET_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_EXEC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AUTCOET_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AUTCOET_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_RECEBIMENTO) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.AUTORIZACAO_CONV_ETAPA TO NIVEL_1;

