ALTER TABLE TASY.FUNCAO_GRUPO_LOC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_GRUPO_LOC CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_GRUPO_LOC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_FUNCAO            NUMBER(5),
  IE_CLASSIFICACAO     VARCHAR2(1 BYTE),
  HR_INICIO            DATE                     NOT NULL,
  HR_FIM               DATE                     NOT NULL,
  NR_SEQ_CLIENTE       NUMBER(10)               NOT NULL,
  NR_SEQ_GRUPO_DES     NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUNGRLO_COMCLIE_FK_I ON TASY.FUNCAO_GRUPO_LOC
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNGRLO_GRUDESE_FK_I ON TASY.FUNCAO_GRUPO_LOC
(NR_SEQ_GRUPO_DES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUNGRLO_PK ON TASY.FUNCAO_GRUPO_LOC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FUNCAO_GRUPO_LOC_tp  after update ON TASY.FUNCAO_GRUPO_LOC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CLASSIFICACAO,1,4000),substr(:new.IE_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIFICACAO',ie_log_w,ds_w,'FUNCAO_GRUPO_LOC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_DES,1,4000),substr(:new.NR_SEQ_GRUPO_DES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_DES',ie_log_w,ds_w,'FUNCAO_GRUPO_LOC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'FUNCAO_GRUPO_LOC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.FUNCAO_GRUPO_LOC ADD (
  CONSTRAINT FUNGRLO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_GRUPO_LOC ADD (
  CONSTRAINT FUNGRLO_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT FUNGRLO_GRUDESE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_DES) 
 REFERENCES TASY.GRUPO_DESENVOLVIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.FUNCAO_GRUPO_LOC TO NIVEL_1;

