ALTER TABLE TASY.FUNCAO_PARAM_ESTAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_PARAM_ESTAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_PARAM_ESTAB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_FUNCAO            NUMBER(5)                NOT NULL,
  NR_SEQ_PARAM         NUMBER(5)                NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  VL_PARAMETRO         VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUNPAES_ESTABEL_FK_I ON TASY.FUNCAO_PARAM_ESTAB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNPAES_FUNPARA_FK_I ON TASY.FUNCAO_PARAM_ESTAB
(CD_FUNCAO, NR_SEQ_PARAM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUNPAES_PK ON TASY.FUNCAO_PARAM_ESTAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FUNCAO_PARAM_ESTAB_dt_param
after INSERT OR DELETE OR UPDATE ON TASY.FUNCAO_PARAM_ESTAB for each row
declare
	ie_consistir_valor_w	varchar2(1);
	vl_parametro_padrao_w	varchar2(255);
begin

update	funcao
set	dt_atualizacao_param = sysdate
where	cd_funcao = nvl(:new.cd_funcao,:old.cd_funcao);

if (inserting or updating) then
	select	ie_consistir_valor,
		vl_parametro_padrao
	into	ie_consistir_valor_w,
		vl_parametro_padrao_w
	from	funcao_parametro
	where	cd_funcao = nvl(:new.cd_funcao, :old.cd_funcao)
	and	nr_sequencia = nvl(:new.nr_seq_param, :old.nr_seq_param);

	if	(ie_consistir_valor_w = 'S') and
		(somente_numero(:new.vl_parametro) < somente_numero(vl_parametro_padrao_w)) then
		wheb_mensagem_pck.exibir_mensagem_abort(55208);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_estab_delete
before delete ON TASY.FUNCAO_PARAM_ESTAB for each row
declare

ie_protocolo_vig_w	varchar2(1);

Cursor C01 is
	select 	distinct cd_perfil
	from 	funcao_perfil
	where 	cd_funcao = :old.cd_funcao
	order 	by 1;

c01_w				C01%rowtype;

Cursor C02 is
	select 	distinct
			nr_seq_protocolo
	from 	concorde_perfil
	where 	cd_perfil = c01_w.cd_perfil
	order 	by 1;

c02_w				C02%rowtype;

begin

if 	(:old.vl_parametro is not null) then
	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;
		begin
			open C02;
			loop
			fetch C02 into
				c02_w;
			exit when C02%notfound;
				begin
					select 	decode(count(*),0,'N','S')
					into   	ie_protocolo_vig_w
					from	concorde_protocolo
					where   nr_sequencia = c02_w.nr_seq_protocolo
					and		sysdate between dt_inicio_vigencia and dt_fim_vigencia;

					if (ie_protocolo_vig_w = 'S') then
						update 	concorde_protocolo
						set		dt_alteracao = sysdate,
								dt_atualizacao = sysdate,
								nm_usuario = :old.nm_usuario
						where 	nr_sequencia = c02_w.nr_seq_protocolo;

						update 	perfil
						set		dt_alteracao = sysdate,
								dt_atualizacao = sysdate,
								nm_usuario = :old.nm_usuario
						where 	cd_perfil = c01_w.cd_perfil;

					end if;
				end;
			end loop;
			close C02;
		end;
	end loop;
	close C01;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_estab_ins_upd
before insert or update ON TASY.FUNCAO_PARAM_ESTAB for each row
declare

ie_protocolo_vig_w	varchar2(1);

Cursor C01 is
	select 	distinct cd_perfil
	from 	funcao_perfil
	where 	cd_funcao = :new.cd_funcao
	order 	by 1;

c01_w				C01%rowtype;

Cursor C02 is
	select 	distinct
			nr_seq_protocolo
	from 	concorde_perfil
	where 	cd_perfil = c01_w.cd_perfil
	order 	by 1;

c02_w				C02%rowtype;

begin

if 	((:old.vl_parametro is null) and (:new.vl_parametro is not null)) or
	(:old.vl_parametro  <> :new.vl_parametro)then
	open C01;
	loop
	fetch C01 into
		c01_w;
	exit when C01%notfound;
		begin
			open C02;
			loop
			fetch C02 into
				c02_w;
			exit when C02%notfound;
				begin
					select 	decode(count(*),0,'N','S')
					into   	ie_protocolo_vig_w
					from	concorde_protocolo
					where   nr_sequencia = c02_w.nr_seq_protocolo
					and		sysdate between dt_inicio_vigencia and dt_fim_vigencia;

					if (ie_protocolo_vig_w = 'S') then
						update 	concorde_protocolo
						set		dt_alteracao = sysdate,
								dt_atualizacao = sysdate,
								nm_usuario = :new.nm_usuario
						where 	nr_sequencia = c02_w.nr_seq_protocolo;

						update 	perfil
						set		dt_alteracao = sysdate,
								dt_atualizacao = sysdate,
								nm_usuario = :new.nm_usuario
						where 	cd_perfil = c01_w.cd_perfil;
					end if;
				end;
			end loop;
			close C02;
		end;
	end loop;
	close C01;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.FUNCAO_PARAM_ESTAB_tp  after update ON TASY.FUNCAO_PARAM_ESTAB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'FUNCAO_PARAM_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'FUNCAO_PARAM_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PARAM,1,4000),substr(:new.NR_SEQ_PARAM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PARAM',ie_log_w,ds_w,'FUNCAO_PARAM_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PARAMETRO,1,4000),substr(:new.VL_PARAMETRO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PARAMETRO',ie_log_w,ds_w,'FUNCAO_PARAM_ESTAB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'FUNCAO_PARAM_ESTAB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_estab_atual
before insert or update ON TASY.FUNCAO_PARAM_ESTAB for each row
declare

ie_possui_reg_w		varchar2(1);
pragma autonomous_transaction;
begin
select	decode(count(*),0,'N','S')
into	ie_possui_reg_w
from	funcao_param_estab
where	cd_estabelecimento = :new.cd_estabelecimento
and	nr_sequencia <> :new.nr_sequencia
and	cd_funcao = :new.cd_funcao
and	nr_seq_param = :new.nr_seq_param;

if	(ie_possui_reg_w = 'S') then
	Wheb_mensagem_pck.exibir_mensagem_abort(289025);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.funcao_param_estab_subject FOR
  DELETE OR INSERT OR UPDATE ON TASY.FUNCAO_PARAM_ESTAB 
COMPOUND TRIGGER
  apply_sync     BOOLEAN := false;
  apply_config   BOOLEAN := false;
  AFTER EACH ROW IS BEGIN
    apply_sync := psa_is_auth_parameter(
      :new.cd_funcao,
      :new.nr_seq_param
    ) = 'TRUE' OR apply_sync;

    apply_config := psa_is_config_parameter(
      :new.cd_funcao,
      :new.nr_seq_param
    ) = 'TRUE' OR apply_config;

  END AFTER EACH ROW;
  AFTER STATEMENT IS BEGIN
    IF
      apply_config
    THEN
      psa_configure;
    END IF;
    IF
      apply_sync
    THEN
      psa_synchronize_subject;
    END IF;
  END AFTER STATEMENT;
END;
/


CREATE OR REPLACE TRIGGER TASY.update_param_estab
before update or insert ON TASY.FUNCAO_PARAM_ESTAB for each row
declare


pragma autonomous_transaction;


begin

IF INSTR(:NEW.VL_PARAMETRO, '#@') > 0 THEN
  wheb_mensagem_pck.exibir_mensagem_abort(1040016);
END IF;

atualizar_log_alt_parametros(
			:new.nm_usuario,
			:new.cd_estabelecimento,
			:new.cd_estabelecimento,
			:old.cd_estabelecimento,
			null,
			null,
			null,
			null,
			:new.nr_seq_param,
			:new.vl_parametro,
			:old.vl_parametro,
			'E',
			:new.cd_funcao);
commit;

end;
/


ALTER TABLE TASY.FUNCAO_PARAM_ESTAB ADD (
  CONSTRAINT FUNPAES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_PARAM_ESTAB ADD (
  CONSTRAINT FUNPAES_FUNPARA_FK 
 FOREIGN KEY (CD_FUNCAO, NR_SEQ_PARAM) 
 REFERENCES TASY.FUNCAO_PARAMETRO (CD_FUNCAO,NR_SEQUENCIA),
  CONSTRAINT FUNPAES_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.FUNCAO_PARAM_ESTAB TO NIVEL_1;

