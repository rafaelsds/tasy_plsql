ALTER TABLE TASY.SAME_SOLIC_PRONT_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAME_SOLIC_PRONT_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAME_SOLIC_PRONT_HIST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_HISTORICO           VARCHAR2(4000 BYTE)    NOT NULL,
  NR_SEQ_SOLIC           NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO_HISTORICO  NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SAMSOPH_PK ON TASY.SAME_SOLIC_PRONT_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMSOPH_PK
  MONITORING USAGE;


CREATE INDEX TASY.SAMSOPH_SAMSOPR_FK_I ON TASY.SAME_SOLIC_PRONT_HIST
(NR_SEQ_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMSOPH_SAMSOPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SAMSOPH_SAMTIHS_FK_I ON TASY.SAME_SOLIC_PRONT_HIST
(NR_SEQ_TIPO_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMSOPH_SAMTIHS_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hsj_same_solic_pront_hist
after insert ON TASY.SAME_SOLIC_PRONT_HIST 
for each row
declare 
 
dt_liberacao_w date;
cd_pessoa_fisica_w number(10);
nr_atendimento_w number(10);
dt_inicial_w date;
dt_final_w date;

begin 

    if(:new.nr_seq_tipo_historico = 2 and :new.dt_inativacao is null)then  --Pronto para Gera��o
        
        select  dt_liberacao,
                cd_pessoa_fisica,
                nr_atendimento,
                dt_periodo_inicial,
                dt_periodo_final
        into    dt_liberacao_w,
                cd_pessoa_fisica_w,
                nr_atendimento_w,
                dt_inicial_w,
                dt_final_w
        from SAME_SOLIC_PRONT
        where nr_sequencia = :new.nr_seq_solic;
    
        if(dt_liberacao_w is not null)then
            
            hsj_solic_prontuario_emissao(:new.nr_seq_solic, cd_pessoa_fisica_w, nr_atendimento_w, dt_inicial_w, dt_final_w);
        
        end if;
    
    elsif(:new.nr_seq_tipo_historico = 3 and :new.dt_inativacao is null)then  --Prontu�rio Entregue
    
        update same_solic_pront set IE_STATUS = 'E'
        where IE_STATUS = 'P'
        and nr_sequencia = :new.nr_seq_solic;
    
    end if; 

end hsj_same_solic_pront_hist;
/


CREATE OR REPLACE TRIGGER TASY.hsj_same_solic_pront_hist_upd
after update ON TASY.SAME_SOLIC_PRONT_HIST 
for each row
declare 
 

nr_seq_emissao_w number(10);

begin 

    if(:new.nr_seq_tipo_historico = 2 and :new.dt_inativacao is not null and :new.ie_situacao = 'I')then  --Pronto para Gera��o
        
        select nr_seq_emissao
        into nr_seq_emissao_w
        from hsj_same_solic_emissao_pront
        where nr_seq_solicitacao = :new.nr_seq_solic;
        
        if(nr_seq_emissao_w is not null)then
            update PRONTUARIO_EMISSAO set dt_inativacao = sysdate, nm_usuario_inativacao = :new.nm_usuario, ie_situacao = 'I'
            where nr_sequencia = nr_seq_emissao_w;
        end if;
        
        delete hsj_same_solic_emissao_pront
        where nr_seq_solicitacao = :new.nr_seq_solic;
        
    end if; 

end hsj_same_solic_pront_hist_upd;
/


ALTER TABLE TASY.SAME_SOLIC_PRONT_HIST ADD (
  CONSTRAINT SAMSOPH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAME_SOLIC_PRONT_HIST ADD (
  CONSTRAINT SAMSOPH_SAMSOPR_FK 
 FOREIGN KEY (NR_SEQ_SOLIC) 
 REFERENCES TASY.SAME_SOLIC_PRONT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SAMSOPH_SAMTIHS_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HISTORICO) 
 REFERENCES TASY.SAME_TIPO_HISTORICO_SOLIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAME_SOLIC_PRONT_HIST TO NIVEL_1;

