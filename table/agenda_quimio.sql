ALTER TABLE TASY.AGENDA_QUIMIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_QUIMIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_QUIMIO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE)   NOT NULL,
  NR_SEQ_PROF               NUMBER(10),
  NR_SEQ_LOCAL              NUMBER(10),
  NR_SEQ_ATENDIMENTO        NUMBER(10),
  DT_AGENDA                 DATE,
  NR_MINUTO_DURACAO         NUMBER(10),
  IE_STATUS_AGENDA          VARCHAR2(15 BYTE)   NOT NULL,
  IE_TIPO_PEND_AGENDA       VARCHAR2(15 BYTE),
  DT_AGUARDANDO             DATE,
  DT_EM_QUIMIO              DATE,
  DT_EXECUTADA              DATE,
  DT_CANCELADA              DATE,
  NR_ATENDIMENTO            NUMBER(10),
  IE_TIPO_AGENDAMENTO       VARCHAR2(15 BYTE),
  NR_SEQ_MOT_REAGENDAMENTO  NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4),
  CD_MEDICO_RESP            VARCHAR2(10 BYTE),
  NR_SEQ_ATEND_SIM          NUMBER(10),
  NR_SEQ_AGENDA_CONS        NUMBER(10),
  NR_SEQ_AGEINT_ITEM        NUMBER(10),
  NR_SEQ_MOT_CANCELAMENTO   NUMBER(10),
  DT_FALTA                  DATE,
  NR_SEQ_PEND_AGENDA        NUMBER(10),
  DS_MOTIVO_STATUS          VARCHAR2(255 BYTE),
  NM_USUARIO_CANCEL         VARCHAR2(15 BYTE),
  IE_ANESTESIA              VARCHAR2(1 BYTE),
  IE_ENCAIXE                VARCHAR2(1 BYTE),
  IE_RESERVA                VARCHAR2(1 BYTE),
  NR_SEQ_MOT_FALTA          NUMBER(10),
  NM_USUARIO_CONFIRM_FORC   VARCHAR2(15 BYTE),
  DT_SUSPENSO               DATE,
  CD_PROTOCOLO              NUMBER(10),
  NR_SEQ_MEDICACAO          NUMBER(6),
  DT_CONFIRMACAO            DATE,
  DT_EM_PREPARO             DATE,
  DT_MEDIC_LIBERADO         DATE,
  NR_SEQ_PAC_SENHA_FILA     NUMBER(10),
  IE_ALOCADO                VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEQUIM_AGECONS_FK_I ON TASY.AGENDA_QUIMIO
(NR_SEQ_AGENDA_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_ATEPACI_FK_I ON TASY.AGENDA_QUIMIO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_ESTABEL_FK_I ON TASY.AGENDA_QUIMIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQUIM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEQUIM_I1 ON TASY.AGENDA_QUIMIO
(DT_AGENDA, NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_I2 ON TASY.AGENDA_QUIMIO
(NR_SEQ_AGEINT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_I3 ON TASY.AGENDA_QUIMIO
(IE_STATUS_AGENDA, DT_AGENDA, NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_PACATEN_FK_I ON TASY.AGENDA_QUIMIO
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_PACATSM_FK_I ON TASY.AGENDA_QUIMIO
(NR_SEQ_ATEND_SIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQUIM_PACATSM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEQUIM_PACSEFI_FK_I ON TASY.AGENDA_QUIMIO
(NR_SEQ_PAC_SENHA_FILA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_PESFISI_FK_I ON TASY.AGENDA_QUIMIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_PESFISI_FK2_I ON TASY.AGENDA_QUIMIO
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGEQUIM_PK ON TASY.AGENDA_QUIMIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_PROMEDI_FK_I ON TASY.AGENDA_QUIMIO
(CD_PROTOCOLO, NR_SEQ_MEDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQUIM_PROMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEQUIM_PROTOCO_FK_I ON TASY.AGENDA_QUIMIO
(CD_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQUIM_PROTOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEQUIM_QTLOCAL_FK_I ON TASY.AGENDA_QUIMIO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEQUIM_QTMOTCA_FK_I ON TASY.AGENDA_QUIMIO
(NR_SEQ_MOT_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQUIM_QTMOTCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEQUIM_QTMOTRG_FK_I ON TASY.AGENDA_QUIMIO
(NR_SEQ_MOT_REAGENDAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQUIM_QTMOTRG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEQUIM_QTMTFAL_FK_I ON TASY.AGENDA_QUIMIO
(NR_SEQ_MOT_FALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQUIM_QTMTFAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEQUIM_QTPROFI_FK_I ON TASY.AGENDA_QUIMIO
(NR_SEQ_PROF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEQUIM_QTPROFI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_quimio_delete
before delete ON TASY.AGENDA_QUIMIO for each row
declare

atrib_oldvalue_w		varchar2(255);
atrib_newvalue_w		varchar2(255);


begin

	atrib_oldvalue_w := substr(obter_valor_dominio(3192,:old.ie_status_agenda),1,255);
	atrib_newvalue_w := substr(obter_valor_dominio(3192,:new.ie_status_agenda),1,255);

	/*Controle de exclus�o para que n�o fique registros na tabela Agenda_Quimio_Marcacao*/
	if (:old.nr_seq_atendimento is not null) then
		delete agenda_quimio_marcacao where nr_seq_atendimento = :old.nr_seq_atendimento;

		delete w_agenda_quimio where nr_seq_atendimento = :old.nr_seq_atendimento;
	end if;

	if	(:old.nr_seq_pend_agenda is not null) then
		/*gravar log's de altera��es(ICESP)*/
		insert into agenda_quimio_log	(NR_SEQUENCIA,
						 DT_ATUALIZACAO,
						 NM_USUARIO,
						 DT_ATUALIZACAO_NREC,
						 NM_USUARIO_NREC,
						 DS_LOG,
						 NR_SEQ_PEND_AGENDA,
						 DT_LOG,
						 DS_STACK)
					values
						(agenda_quimio_log_seq.nextval,
						sysdate,
						:old.nm_usuario,
						sysdate,
						:old.nm_usuario,
						substr(wheb_mensagem_pck.get_texto(800192,
										'ATRIB_OLDVALUE='||atrib_oldvalue_w||
										';ATRIB_NEWVALUE='||atrib_newvalue_w||
										';DS_MOTIVO_STATUS_OLD='||:old.ds_motivo_status||
										';CD_PESSOA_FISICA_OLD='||:old.cd_pessoa_fisica||
										';NR_SEQ_PEND_AGENDA_OLD='||:old.nr_seq_pend_agenda||
										';NR_SEQ_LOCAL_OLD='||:old.nr_seq_local||
										';DT_CANCELADA_OLD='||:old.dt_cancelada||
										';NR_SEQ_MOT_CANCELAMENTO_OLD='||:old.nr_seq_mot_cancelamento),1,4000),
						:old.nr_seq_pend_agenda,
						sysdate,
						substr(wheb_mensagem_pck.get_texto(800207,
										'DS_FUNCAO_ATIVA='||obter_funcao_Ativa||
										';DS_PERFIL='||obter_perfil_ativo) || ' -  Stack - ' || dbms_utility.format_call_stack, 1,4000));
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Quimio_Notification



AFTER UPDATE OR INSERT ON TASY.AGENDA_QUIMIO 


FOR EACH Row
DECLARE ie_status_w varchar(1) := 'Y';



BEGIN

IF (:New.Ie_Status_Agenda = 'N' ) THEN

Begin

select decode(count(*), 0, 'N', 'Y') into  ie_status_w from wsuite_notification WHERE nr_primary_key  = :new.Nr_Sequencia AND ie_notification_type ='Q';

If(ie_status_w = 'N') then

Wsuite_Add_Notification('Q',:New.Nr_Sequencia, NULL, :New.Nm_Usuario, :new.cd_pessoa_fisica,:New.Nr_Sequencia);

end if;

end;

Elsif (:New.Ie_Status_Agenda  = 'C' ) THEN

begin

Wsuite_Remove_Notification('Q',:New.Nr_Sequencia);

Wsuite_Add_Notification('Q',:New.Nr_Sequencia, NULL, :New.Nm_Usuario, :new.cd_pessoa_fisica,:New.Nr_Sequencia);

end;

END IF;



END;
/


CREATE OR REPLACE TRIGGER TASY.agenda_quimio_atual
before insert or update ON TASY.AGENDA_QUIMIO for each row
declare

atrib_oldvalue_w		varchar2(255);
atrib_newvalue_w		varchar2(255);
cd_estabelecimento_w	number(20);
nr_seq_pac_senha_fila_w number(10);
begin

if	((:old.ie_status_agenda is not null) and
	(:new.ie_status_agenda is not null) and
	(:old.ie_status_agenda <> :new.ie_status_agenda)) and
	(:new.nr_seq_pend_agenda is not null) then

	atrib_oldvalue_w := substr(obter_valor_dominio(3192,:old.ie_status_agenda),1,255);
	atrib_newvalue_w := substr(obter_valor_dominio(3192,:new.ie_status_agenda),1,255);

	gerar_agenda_quimio_hist(:new.nr_sequencia,:old.cd_pessoa_fisica, wheb_mensagem_pck.get_texto(791793,
												'DS_ATRIB_OLDVALUE='||atrib_oldvalue_w ||
												';DS_ATRIB_NEWVALUE='||atrib_newvalue_w), :old.dt_agenda,:new.nm_usuario);

	/*gravar logs de alteracoes(USJCCT)*/
	insert into agenda_quimio_log	(NR_SEQUENCIA,
					 DT_ATUALIZACAO,
                     --NR_SEQ_MEDICACAO,
                     --CD_PROTOCOLO,
					 NM_USUARIO,
					 DT_ATUALIZACAO_NREC,
					 NM_USUARIO_NREC,
					 DS_LOG,
					 NR_SEQ_PEND_AGENDA,
					 DT_LOG,
					 DS_STACK)
				values
					(agenda_quimio_log_seq.nextval,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:old.nm_usuario,
					substr(	wheb_mensagem_pck.get_texto(791795,
							'DS_ATRIB_OLDVALUE='||atrib_oldvalue_w ||
							';DS_ATRIB_NEWVALUE='||atrib_newvalue_w ||
							';DS_MOTIVO_STATUS='||:new.ds_motivo_status||
                            --';NR_SEQ_MEDICACAO='||:new.nr_seq_medicacao||
                            --';CD_PROTOCOLO=' ||:new.cd_protocolo||
							';CD_PESSOA_FISICA_OLD='||:old.cd_pessoa_fisica)||chr(10)||
						wheb_mensagem_pck.get_texto(791797,
							'NR_SEQ_PEND_AGENDA='||:new.nr_seq_pend_agenda ||
							';NR_SEQ_LOCAL='||:new.nr_seq_local ||
							';DT_CANCELADA='||:new.dt_cancelada||
							';NR_SEQ_MOT_CANCELAMENTO='||:new.nr_seq_mot_cancelamento)||chr(10)||
							wheb_mensagem_pck.get_texto(799798) || ': '||:new.nr_sequencia||'/'||:new.nr_seq_atendimento,1,4000),
					:new.nr_seq_pend_agenda,
					sysdate,
					substr(	wheb_mensagem_pck.get_texto(791798,
							'DS_FUNCAO_ATIVA='||obter_funcao_Ativa ||
							';DS_PERFIL='||obter_perfil_ativo) || ' -  Stack' || dbms_utility.format_call_stack, 1,4000));

end if;

if 	(((:new.ie_status_agenda is not null) and ((:new.ie_status_agenda = 'C') and (:old.ie_status_agenda <> :new.ie_status_agenda))) and
	((:old.nr_seq_atendimento is not null) or (:new.nr_seq_atendimento is not null))) then
	delete agenda_quimio_marcacao where nr_seq_atendimento = nvl(:new.nr_seq_atendimento,:old.nr_seq_atendimento);

	delete w_agenda_quimio where nr_seq_atendimento = nvl(:new.nr_seq_atendimento,:old.nr_seq_atendimento);
end if;

if	(:old.ie_status_agenda is null) and
	(:new.ie_status_agenda is not null) and
	(:new.ie_status_agenda = 'N') then

	atrib_oldvalue_w := substr(obter_valor_dominio(3192,:old.ie_status_agenda),1,255);
	atrib_newvalue_w := substr(obter_valor_dominio(3192,:new.ie_status_agenda),1,255);

	gerar_agenda_quimio_hist(:new.nr_sequencia,:old.cd_pessoa_fisica, wheb_mensagem_pck.get_texto(791799), :new.dt_agenda,:new.nm_usuario);

end if;

if  ((:old.dt_agenda is not null) and (trunc(:old.dt_agenda) <> :old.dt_agenda) and (:old.dt_agenda <> :new.dt_agenda)) or
	((:old.nr_seq_local is not null) and (:old.nr_seq_local <> :new.nr_seq_local)) or
	((:old.nr_minuto_duracao is not null) and (:old.nr_minuto_duracao <> :new.nr_minuto_duracao))	then

	delete	agenda_quimio_marcacao
	where 	dt_Agenda between trunc(nvl(:old.dt_agenda,:new.dt_agenda)) and trunc(nvl(:old.dt_agenda,:new.dt_agenda))+86399/86400
	and 	nr_seq_agenda = nvl(:new.nr_sequencia, :old.nr_sequencia);

	delete	w_agenda_quimio
	where	dt_horario between trunc(nvl(:old.dt_agenda,:new.dt_agenda)) and trunc(nvl(:old.dt_agenda,:new.dt_agenda))+86399/86400
	and		nr_seq_agequi = nvl(:new.nr_sequencia, :old.nr_sequencia);

end if;

if	(:old.dt_agenda is not null) and
	(:new.dt_agenda is not null) and
	(:old.dt_agenda <> :new.dt_agenda)  then

	atrib_oldvalue_w := to_char(:old.dt_agenda,'dd/mm/yyyy hh24:mi');
	atrib_newvalue_w := to_char(:new.dt_agenda,'dd/mm/yyyy hh24:mi');

	gerar_agenda_quimio_hist(:new.nr_sequencia,:old.cd_pessoa_fisica, wheb_mensagem_pck.get_texto(791800,
												'DS_ATRIB_OLDVALUE='||atrib_oldvalue_w ||
												';DS_ATRIB_NEWVALUE='||atrib_newvalue_w), :old.dt_agenda,:new.nm_usuario);

	if	(:new.nr_seq_pend_agenda is not null) then
		/*gravar logs de alteracoes(USJCCT)*/
		insert into agenda_quimio_log	(NR_SEQUENCIA,
						 DT_ATUALIZACAO,
                         --NR_SEQ_MEDICACAO,
                         --CD_PROTOCOLO,
						 NM_USUARIO,
						 DT_ATUALIZACAO_NREC,
						 NM_USUARIO_NREC,
						 DS_LOG,
						 NR_SEQ_PEND_AGENDA,
						 DT_LOG,
						 DS_STACK)
					values
						(agenda_quimio_log_seq.nextval,
						sysdate,
						:new.nm_usuario,
						sysdate,
						:old.nm_usuario,
						substr(	wheb_mensagem_pck.get_texto(791801,
									'DS_ATRIB_OLDVALUE='||atrib_oldvalue_w ||
									';DS_ATRIB_NEWVALUE='||atrib_newvalue_w ||
									';DS_MOTIVO_STATUS='||:new.ds_motivo_status||
                                    --';NR_SEQ_MEDICACAO='||:new.nr_seq_medicacao||
                                    --';CD_PROTOCOLO=' ||:new.cd_protocolo||
									';CD_PESSOA_FISICA_OLD='||:old.cd_pessoa_fisica)||chr(10)||
							wheb_mensagem_pck.get_texto(791797,
									'NR_SEQ_PEND_AGENDA='||:new.nr_seq_pend_agenda ||
									';NR_SEQ_LOCAL='||:new.nr_seq_local ||
									';DT_CANCELADA='||:new.dt_cancelada||
									';NR_SEQ_MOT_CANCELAMENTO='||:new.nr_seq_mot_cancelamento)||chr(10)||
									wheb_mensagem_pck.get_texto(799798) || ': '||:new.nr_sequencia||'/'||:new.nr_seq_atendimento,1,4000),
						:new.nr_seq_pend_agenda,
						sysdate,
						substr(wheb_mensagem_pck.get_texto(791798,
							'DS_FUNCAO_ATIVA='||obter_funcao_Ativa ||
							';DS_PERFIL='||obter_perfil_ativo) || ' -  Stack' || dbms_utility.format_call_stack, 1,4000)
						);

	end if;
end if;


if	(:old.ie_status_Agenda is null) and
	(:new.ie_status_agenda is not null) and
	(:new.nr_seq_pend_agenda is not null) then

	insert into agenda_quimio_log	(NR_SEQUENCIA,
					 DT_ATUALIZACAO,
                     --NR_SEQ_MEDICACAO,
                     --CD_PROTOCOLO,
					 NM_USUARIO,
					 DT_ATUALIZACAO_NREC,
					 NM_USUARIO_NREC,
					 DS_LOG,
					 NR_SEQ_PEND_AGENDA,
					 DT_LOG,
					 DS_STACK)
				values
					(agenda_quimio_log_seq.nextval,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:old.nm_usuario,
					substr(	wheb_mensagem_pck.get_texto(791804,
								'DS_ATRIB_OLDVALUE='||atrib_oldvalue_w ||
								';DS_ATRIB_NEWVALUE='||atrib_newvalue_w ||
								';DS_MOTIVO_STATUS='||:new.ds_motivo_status||
                                --';NR_SEQ_MEDICACAO='||:new.nr_seq_medicacao||
                                --';CD_PROTOCOLO=' ||:new.cd_protocolo||
								';CD_PESSOA_FISICA_OLD='||:old.cd_pessoa_fisica)||chr(10)||
						wheb_mensagem_pck.get_texto(791797,
								'NR_SEQ_PEND_AGENDA='||:new.nr_seq_pend_agenda ||
								';NR_SEQ_LOCAL='||:new.nr_seq_local ||
								';DT_CANCELADA='||:new.dt_cancelada||
								';NR_SEQ_MOT_CANCELAMENTO='||:new.nr_seq_mot_cancelamento)||chr(10)||
								wheb_mensagem_pck.get_texto(799798) || ': '||:new.nr_sequencia||'/'||:new.nr_seq_atendimento,1,4000),
					:new.nr_seq_pend_agenda,
					sysdate,
					substr(wheb_mensagem_pck.get_texto(791798,
							'DS_FUNCAO_ATIVA='||obter_funcao_Ativa ||
							';DS_PERFIL='||obter_perfil_ativo) || ' -  Stack' || dbms_utility.format_call_stack, 1,4000));

end if;

--Gerar historico de cancelamento da pendencia

if	(:new.ie_status_agenda is not null) and
	(:old.ie_status_agenda is not null) and
	(:old.ie_status_agenda <> 'C') and
	(:new.ie_status_agenda = 'C') then
	gerar_agenda_quimio_hist(:new.nr_sequencia,:old.cd_pessoa_fisica, wheb_mensagem_pck.get_texto(791806), :old.dt_agenda,:new.nm_usuario);

end if;

if (nvl(:new.nr_seq_local,0) > 0) and
   (nvl(:old.nr_seq_local,0) <> nvl(:new.nr_seq_local,0)) then

	select	max(cd_estabelecimento)
	into	cd_estabelecimento_w
	from	qt_local
	where	nr_sequencia = :new.nr_seq_local;

	:new.cd_estabelecimento := cd_estabelecimento_w;
end if;

if	(:old.nr_atendimento is null) and
	(:new.nr_atendimento is not null) and
	(:new.nr_seq_pac_senha_fila is null) then

	select	max(nr_seq_pac_senha_fila)
	into	nr_seq_pac_senha_fila_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	if	(nvl(nr_seq_pac_senha_fila_w,0) > 0) then
		:new.nr_seq_pac_senha_fila := nr_seq_pac_senha_fila_w;
	end if;
end if;

if	(:old.cd_pessoa_fisica is not null) and
	(:old.cd_pessoa_fisica <> :new.cd_pessoa_fisica) and
	(:new.nr_seq_atendimento is not null) then

	if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
		insert into agenda_quimio_log	(NR_SEQUENCIA,
					 DT_ATUALIZACAO,
					 NM_USUARIO,
					 DT_ATUALIZACAO_NREC,
					 NM_USUARIO_NREC,
					 DS_LOG,
					 NR_SEQ_PEND_AGENDA,
					 DT_LOG,
					 DS_STACK)
				values
					(agenda_quimio_log_seq.nextval,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:old.nm_usuario,
					substr(wheb_mensagem_pck.get_texto(791811,
							'CD_PESSOA_FISICA_OLD='||:old.cd_pessoa_fisica ||
							';CD_PESSOA_FISICA_NEW='||:new.cd_pessoa_fisica)||chr(10)||
							wheb_mensagem_pck.get_texto(799798) || ': '||:new.nr_sequencia||'/'||:new.nr_seq_atendimento,1,4000),
					:new.nr_seq_pend_agenda,
					sysdate,
					substr(	wheb_mensagem_pck.get_texto(791798,
							'DS_FUNCAO_ATIVA='||obter_funcao_Ativa ||
							';DS_PERFIL='||obter_perfil_ativo) || ' -  Stack: ' || dbms_utility.format_call_stack, 1,4000));

	else
		wheb_mensagem_pck.exibir_mensagem_abort(658383);
	end if;
end if;

end;
/


ALTER TABLE TASY.AGENDA_QUIMIO ADD (
  CONSTRAINT AGEQUIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_QUIMIO ADD (
  CONSTRAINT AGEQUIM_PACSEFI_FK 
 FOREIGN KEY (NR_SEQ_PAC_SENHA_FILA) 
 REFERENCES TASY.PACIENTE_SENHA_FILA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGEQUIM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AGEQUIM_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEQUIM_PACATSM_FK 
 FOREIGN KEY (NR_SEQ_ATEND_SIM) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO_SIM (NR_SEQUENCIA),
  CONSTRAINT AGEQUIM_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_CONS) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT AGEQUIM_QTMOTCA_FK 
 FOREIGN KEY (NR_SEQ_MOT_CANCELAMENTO) 
 REFERENCES TASY.QT_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEQUIM_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT AGEQUIM_QTMOTRG_FK 
 FOREIGN KEY (NR_SEQ_MOT_REAGENDAMENTO) 
 REFERENCES TASY.QT_MOTIVO_REAGENDAMENTO (NR_SEQUENCIA),
  CONSTRAINT AGEQUIM_QTMTFAL_FK 
 FOREIGN KEY (NR_SEQ_MOT_FALTA) 
 REFERENCES TASY.QT_MOTIVO_FALTA (NR_SEQUENCIA),
  CONSTRAINT AGEQUIM_PROMEDI_FK 
 FOREIGN KEY (CD_PROTOCOLO, NR_SEQ_MEDICACAO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (CD_PROTOCOLO,NR_SEQUENCIA),
  CONSTRAINT AGEQUIM_PROTOCO_FK 
 FOREIGN KEY (CD_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO (CD_PROTOCOLO),
  CONSTRAINT AGEQUIM_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEQUIM_QTPROFI_FK 
 FOREIGN KEY (NR_SEQ_PROF) 
 REFERENCES TASY.QT_PROFISSIONAL (NR_SEQUENCIA),
  CONSTRAINT AGEQUIM_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT AGEQUIM_PACATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO (NR_SEQ_ATENDIMENTO));

GRANT SELECT ON TASY.AGENDA_QUIMIO TO NIVEL_1;

