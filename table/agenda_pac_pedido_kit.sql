ALTER TABLE TASY.AGENDA_PAC_PEDIDO_KIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_PAC_PEDIDO_KIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_PAC_PEDIDO_KIT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PEDIDO        NUMBER(10)               NOT NULL,
  CD_KIT_MATERIAL      NUMBER(10)               NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  CD_MEDICO            VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEPAPK_AGEPAPE_FK_I ON TASY.AGENDA_PAC_PEDIDO_KIT
(NR_SEQ_PEDIDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEPAPK_KITMATE_FK_I ON TASY.AGENDA_PAC_PEDIDO_KIT
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEPAPK_MEDICO_FK_I ON TASY.AGENDA_PAC_PEDIDO_KIT
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGEPAPK_PK ON TASY.AGENDA_PAC_PEDIDO_KIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAPK_PK
  MONITORING USAGE;


CREATE INDEX TASY.AGEPAPK_PROCEDI_FK_I ON TASY.AGENDA_PAC_PEDIDO_KIT
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1032K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAPK_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_pac_pedido_kit_atual
before insert or update ON TASY.AGENDA_PAC_PEDIDO_KIT for each row
declare
ds_alteracao_w		varchar2(4000);
ds_equipamento_w	varchar2(255);
nr_seq_agenda_w		number(10);

begin

begin
select	nvl(max(nr_seq_agenda),0)
into	nr_seq_agenda_w
from	agenda_pac_pedido
where	nr_sequencia = :new.nr_seq_pedido;
exception
when others then
	nr_seq_agenda_w	:= 0;
end;

if	(updating) then
	if	(nvl(:old.cd_kit_material,0) <> nvl(:new.cd_kit_material,0)) then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(800123,
									'DS_KIT_OLD='||obter_descricao_padrao('KIT_MATERIAL','DS_KIT_MATERIAL',:old.cd_kit_material)||
									';DS_KIT_NEW='||obter_descricao_padrao('KIT_MATERIAL','DS_KIT_MATERIAL',:new.cd_kit_material)),1,4000);
	end if;
	if	(nvl(:old.DS_OBSERVACAO,' ') <> nvl(:new.DS_OBSERVACAO,' ')) then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(800124,
									'DS_KIT_NEW='||obter_descricao_padrao('KIT_MATERIAL','DS_KIT_MATERIAL',:new.cd_kit_material)||
									';DS_OBSERVACAO_OLD='||:old.DS_OBSERVACAO||
									';DS_OBSERVACAO_NEW='||:new.DS_OBSERVACAO),1,4000);
	end if;
	if	(ds_alteracao_w is not null) and (nr_seq_agenda_w > 0) then
		gravar_historico_montagem(nr_seq_agenda_w,'AK',ds_alteracao_w,:new.nm_usuario);
	end if;
else
	ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(800125, 'DS_KIT_NEW='||obter_descricao_padrao('KIT_MATERIAL','DS_KIT_MATERIAL',:new.cd_kit_material)),1,4000);

	if	(ds_alteracao_w is not null) and (nr_seq_agenda_w > 0) then
		gravar_historico_montagem(nr_seq_agenda_w,'IK',ds_alteracao_w,:new.nm_usuario);
	end if;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_pac_pedido_kit_delete
before delete ON TASY.AGENDA_PAC_PEDIDO_KIT for each row
declare
ds_alteracao_w		varchar2(4000);
nr_seq_agenda_w		number(10);
begin

begin
select	nvl(max(nr_seq_agenda),0)
into	nr_seq_agenda_w
from	agenda_pac_pedido
where	nr_sequencia = :old.nr_seq_pedido;
exception
when others then
	nr_seq_agenda_w	:= 0;
end;

ds_alteracao_w	:=	substr(obter_desc_expressao(500808)||' '||obter_descricao_padrao('KIT_MATERIAL','DS_KIT_MATERIAL',:old.cd_kit_material),1,4000);

if	(ds_alteracao_w is not null) and (nr_seq_agenda_w > 0) then
	gravar_historico_montagem(nr_seq_agenda_w,'EK',ds_alteracao_w,:old.nm_usuario);
end if;

end;
/


ALTER TABLE TASY.AGENDA_PAC_PEDIDO_KIT ADD (
  CONSTRAINT AGEPAPK_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_PAC_PEDIDO_KIT ADD (
  CONSTRAINT AGEPAPK_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGEPAPK_AGEPAPE_FK 
 FOREIGN KEY (NR_SEQ_PEDIDO) 
 REFERENCES TASY.AGENDA_PAC_PEDIDO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGEPAPK_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT AGEPAPK_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AGENDA_PAC_PEDIDO_KIT TO NIVEL_1;

