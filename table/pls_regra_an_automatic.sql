ALTER TABLE TASY.PLS_REGRA_AN_AUTOMATIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_AN_AUTOMATIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_AN_AUTOMATIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_REGRA             VARCHAR2(255 BYTE)       NOT NULL,
  QT_ANALISE           NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRNA_PK ON TASY.PLS_REGRA_AN_AUTOMATIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_an_automatic_alt
after insert or update ON TASY.PLS_REGRA_AN_AUTOMATIC for each row
declare

qt_registros_ant_w		pls_integer	:= 0;
qt_registros_gerar_w		pls_integer	:= 0;
ds_interval_w			varchar(100)	:= 0;
qt_interval_w			number(7,4);
ds_comando_job_w		varchar2(2000);
jobno				number;
ds_sqlerrm_w	varchar2(4000);

cursor C01 is
	select	job
	from	job_v
	where	comando	like '%PLS_PROCESSAR_ANALISE_CTA_PEND%';

begin

--Verifica se � update para buscar os registros anteriores
if	(updating) then
	qt_registros_ant_w	:= :old.qt_analise;
end if;

--Verifica se o registro antigo � diferente do novo
if	(qt_registros_ant_w <> :new.qt_analise) then

	--Remove todas as JOBS atuais
	for r_c01_w in C01 loop
		dbms_job.remove(r_C01_w.job);
	end loop;


	qt_registros_gerar_w	:= :new.qt_analise;
	qt_interval_w		:= 0.100;

	--Aqui faz o loop para criar todas as JOBS conforme a quantidade de processos que o cliente informar
	while	(qt_registros_gerar_w > 0) loop

		begin
			--Cria um intervalo a cada 4 segundos para cada JOB
			ds_interval_w		:= '(SYSDATE) + ' || replace(to_char(qt_interval_w),',','.')||'/24';
			ds_comando_job_w	:= 'PLS_PROCESSAR_ANALISE_CTA_PEND(' || pls_util_pck.aspas_w  || 'TASY'|| pls_util_pck.aspas_w|| ',null);';
			DBMS_JOB.SUBMIT(JOBNO,
					ds_comando_job_w,
					sysdate + 5 * (1/24/60/60),
					ds_interval_w);
			commit;
		exception
		when others then
			null;
		end;

		--Incrementa o intervalo
		qt_interval_w	:= qt_interval_w + 0.001;
		--Decrementa o intervalo
		qt_registros_gerar_w	:= qt_registros_gerar_w - 1;

	end loop;

end if;

end;
/


ALTER TABLE TASY.PLS_REGRA_AN_AUTOMATIC ADD (
  CONSTRAINT PLSRNA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_REGRA_AN_AUTOMATIC TO NIVEL_1;

