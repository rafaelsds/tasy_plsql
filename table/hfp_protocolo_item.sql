ALTER TABLE TASY.HFP_PROTOCOLO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HFP_PROTOCOLO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.HFP_PROTOCOLO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  NR_SEQ_MUSC          NUMBER(10)               NOT NULL,
  QT_VALOR             NUMBER(10,3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HFPRIT_HFPMUSC_FK_I ON TASY.HFP_PROTOCOLO_ITEM
(NR_SEQ_MUSC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HFPRIT_HFPRMU_FK_I ON TASY.HFP_PROTOCOLO_ITEM
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HFPRIT_PK ON TASY.HFP_PROTOCOLO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HFP_PROTOCOLO_ITEM ADD (
  CONSTRAINT HFPRIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HFP_PROTOCOLO_ITEM ADD (
  CONSTRAINT HFPRIT_HFPMUSC_FK 
 FOREIGN KEY (NR_SEQ_MUSC) 
 REFERENCES TASY.HFP_MUSCULATURA (NR_SEQUENCIA),
  CONSTRAINT HFPRIT_HFPRMU_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.HFP_PROTOCOLO_MUSC (NR_SEQUENCIA));

GRANT SELECT ON TASY.HFP_PROTOCOLO_ITEM TO NIVEL_1;

