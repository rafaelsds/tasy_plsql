ALTER TABLE TASY.PLS_SCA_REGRA_OBITO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SCA_REGRA_OBITO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SCA_REGRA_OBITO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_SCA              NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_CONTRATO        VARCHAR2(2 BYTE),
  NR_SEQ_EMISSOR          NUMBER(10),
  NR_SEQ_PLANO_SCA        NUMBER(10),
  NR_SEQ_VENDEDOR_CANAL   NUMBER(10),
  NR_SEQ_VENDEDOR_PF      NUMBER(10),
  NR_SEQ_MOTIVO_INCLUSAO  NUMBER(10),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  NR_SEQ_TABELA           NUMBER(10),
  QT_VALIDADE_CONTRATO    NUMBER(10),
  NR_SEQ_MOTIVO_RESCISAO  NUMBER(10),
  IE_ACAO_SEGURO          VARCHAR2(2 BYTE),
  NR_SEQ_BONIFICACAO      NUMBER(10),
  NR_SEQ_CAUSA_RESCISAO   NUMBER(10),
  DT_CONTRATO_INICIAL     DATE,
  DT_CONTRATO_FINAL       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSCRO_ESTABEL_FK_I ON TASY.PLS_SCA_REGRA_OBITO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSCRO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSSCRO_PK ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSCRO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSSCRO_PLSBONI_FK_I ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQ_BONIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSCRO_PLSBONI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSCRO_PLSCARS_FK_I ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQ_CAUSA_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSCRO_PLSMOCA_FK_I ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQ_MOTIVO_RESCISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSCRO_PLSMOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSCRO_PLSMOISE_FK_I ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQ_MOTIVO_INCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSCRO_PLSMOISE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSCRO_PLSPLAN_FK_I ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQ_SCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSCRO_PLSPLAN_FK2_I ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQ_PLANO_SCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSCRO_PLSPLAN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSCRO_PLSTAPR_FK_I ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSCRO_PLSTAPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSCRO_PLSVEND_FK_I ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQ_VENDEDOR_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSCRO_PLSVEND_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSSCRO_PLSVEVI_FK_I ON TASY.PLS_SCA_REGRA_OBITO
(NR_SEQ_VENDEDOR_PF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSSCRO_PLSVEVI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_SCA_REGRA_OBITO ADD (
  CONSTRAINT PLSSCRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SCA_REGRA_OBITO ADD (
  CONSTRAINT PLSSCRO_PLSMOCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_RESCISAO) 
 REFERENCES TASY.PLS_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSSCRO_PLSTAPR_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.PLS_TABELA_PRECO (NR_SEQUENCIA),
  CONSTRAINT PLSSCRO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSSCRO_PLSMOISE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_INCLUSAO) 
 REFERENCES TASY.PLS_MOTIVO_INCLUSAO_SEG (NR_SEQUENCIA),
  CONSTRAINT PLSSCRO_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_SCA) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSSCRO_PLSPLAN_FK2 
 FOREIGN KEY (NR_SEQ_PLANO_SCA) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSSCRO_PLSVEND_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR_CANAL) 
 REFERENCES TASY.PLS_VENDEDOR (NR_SEQUENCIA),
  CONSTRAINT PLSSCRO_PLSVEVI_FK 
 FOREIGN KEY (NR_SEQ_VENDEDOR_PF) 
 REFERENCES TASY.PLS_VENDEDOR_VINCULADO (NR_SEQUENCIA),
  CONSTRAINT PLSSCRO_PLSBONI_FK 
 FOREIGN KEY (NR_SEQ_BONIFICACAO) 
 REFERENCES TASY.PLS_BONIFICACAO (NR_SEQUENCIA),
  CONSTRAINT PLSSCRO_PLSCARS_FK 
 FOREIGN KEY (NR_SEQ_CAUSA_RESCISAO) 
 REFERENCES TASY.PLS_CAUSA_RESCISAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SCA_REGRA_OBITO TO NIVEL_1;

