ALTER TABLE TASY.PLS_PROC_CONTA_INCONSIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PROC_CONTA_INCONSIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PROC_CONTA_INCONSIST
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_PROC_CONTA      NUMBER(10)             NOT NULL,
  NR_SEQ_INCONSISTENCIA  NUMBER(10)             NOT NULL,
  DS_ACAO                VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLPRCOI_PK ON TASY.PLS_PROC_CONTA_INCONSIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLPRCOI_PLINPRO_FK_I ON TASY.PLS_PROC_CONTA_INCONSIST
(NR_SEQ_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLPRCOI_PLSPCON_FK_I ON TASY.PLS_PROC_CONTA_INCONSIST
(NR_SEQ_PROC_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_PROC_CONTA_INCONSIST ADD (
  CONSTRAINT PLPRCOI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PROC_CONTA_INCONSIST ADD (
  CONSTRAINT PLPRCOI_PLINPRO_FK 
 FOREIGN KEY (NR_SEQ_INCONSISTENCIA) 
 REFERENCES TASY.PLS_INCONSIST_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT PLPRCOI_PLSPCON_FK 
 FOREIGN KEY (NR_SEQ_PROC_CONTA) 
 REFERENCES TASY.PLS_PROCESSO_CONTA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_PROC_CONTA_INCONSIST TO NIVEL_1;

