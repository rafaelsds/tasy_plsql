ALTER TABLE TASY.PESSOA_FISICA_AVALIACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_AVALIACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_AVALIACAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_SEQ_TIPO_AVALIACAO  NUMBER(10)             NOT NULL,
  DT_PROX_AVALIACAO      DATE,
  DT_LIBERACAO           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESFISAV_ESTABEL_FK_I ON TASY.PESSOA_FISICA_AVALIACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISAV_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISAV_MEDTIAV_FK_I ON TASY.PESSOA_FISICA_AVALIACAO
(NR_SEQ_TIPO_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFISAV_MEDTIAV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESFISAV_PESFISI_FK_I ON TASY.PESSOA_FISICA_AVALIACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFISAV_PK ON TASY.PESSOA_FISICA_AVALIACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PESSOA_FISICA_AVALIACAO ADD (
  CONSTRAINT PESFISAV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA_AVALIACAO ADD (
  CONSTRAINT PESFISAV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PESFISAV_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PESFISAV_MEDTIAV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_AVALIACAO) 
 REFERENCES TASY.MED_TIPO_AVALIACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PESSOA_FISICA_AVALIACAO TO NIVEL_1;

