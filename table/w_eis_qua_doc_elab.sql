DROP TABLE TASY.W_EIS_QUA_DOC_ELAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_EIS_QUA_DOC_ELAB
(
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_ANO                VARCHAR2(10 BYTE),
  NR_ANO                NUMBER(4),
  QT_JAN                NUMBER(15,4),
  QT_FEV                NUMBER(15,4),
  QT_MAR                NUMBER(15,4),
  QT_ABR                NUMBER(15,4),
  QT_MAI                NUMBER(15,4),
  QT_JUN                NUMBER(15,4),
  QT_JUL                NUMBER(15,4),
  QT_AGO                NUMBER(15,4),
  QT_SET                NUMBER(15,4),
  QT_OUT                NUMBER(15,4),
  QT_NOV                NUMBER(15,4),
  QT_DEZ                NUMBER(15,4),
  QT_MEDIA              NUMBER(15,4),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_EIS_QUA_DOC_ELAB TO NIVEL_1;

