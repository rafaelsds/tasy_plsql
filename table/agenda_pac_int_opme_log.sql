ALTER TABLE TASY.AGENDA_PAC_INT_OPME_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_PAC_INT_OPME_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_PAC_INT_OPME_LOG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA        NUMBER(10),
  DS_DESCRICAO         VARCHAR2(4000 BYTE),
  CD_EVENTO_LOG        VARCHAR2(15 BYTE)        NOT NULL,
  CD_FORNECEDOR        VARCHAR2(14 BYTE),
  NR_ATENDIMENTO       NUMBER(10),
  NR_SEQ_ATEND_OPME    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.APOPMEL_AGEPACI_FK_I ON TASY.AGENDA_PAC_INT_OPME_LOG
(NR_SEQ_AGENDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APOPMEL_AGEPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.APOPMEL_ATEPACI_FK_I ON TASY.AGENDA_PAC_INT_OPME_LOG
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APOPMEL_ATPAIOP_FK_I ON TASY.AGENDA_PAC_INT_OPME_LOG
(NR_SEQ_ATEND_OPME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APOPMEL_ATPAIOP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.APOPMEL_PESJURI_FK_I ON TASY.AGENDA_PAC_INT_OPME_LOG
(CD_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APOPMEL_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.APOPMEL_PK ON TASY.AGENDA_PAC_INT_OPME_LOG
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APOPMEL_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_pac_int_opme_log_atual
before insert or update ON TASY.AGENDA_PAC_INT_OPME_LOG for each row
declare

qt_existe_integrada_w 		number(10);
qt_existe_atend_w		number(10);
ie_opme_integracao_w		varchar2(1);
qt_existe_opme_w		number(10);
nr_cirurgia_w			number(10);
nr_seq_pac_int_opme_w		number(10);

begin

if	(:new.nr_seq_agenda is not null) and (:new.nr_atendimento is null)  then
	select 	max(nr_cirurgia)
	into	nr_cirurgia_w
	from 	agenda_paciente
	where 	nr_sequencia = :new.nr_seq_agenda;

end if;

if	((:new.nr_atendimento is null)  and (:new.nr_seq_agenda is not null) and (nvl(nr_cirurgia_w,0) = 0) and
	((:new.cd_evento_log = '2') or (:new.cd_evento_log = '92'))) then

	select	count(*)
	into	qt_existe_opme_w
	from	agenda_pac_opme
	where	nr_seq_agenda = :new.nr_seq_agenda;

	select	count(*)
	into	qt_existe_integrada_w
	from	agenda_integrada_item
	where	nr_seq_agenda_exame = :new.nr_seq_agenda;

	select 	max(ie_opme_integracao)
	into	ie_opme_integracao_w
	from	agenda_paciente
	where	nr_sequencia = :new.nr_seq_agenda;

	if	((nvl(qt_existe_opme_w,0) > 0) and (nvl(qt_existe_integrada_w,0) > 0) and (nvl(ie_opme_integracao_w,'N') = 'S') and
		((:new.cd_evento_log = '2') or (:new.cd_evento_log = '92'))) then
		begin
		if	(:new.nr_seq_agenda is not null) then
			begin
			gravar_agend_integracao(46,'nr_sequencia=' || :new.nr_seq_agenda || ';');

			insert into agenda_pac_int_opme_log(
					nr_sequencia,		dt_atualizacao,
					nm_usuario,		dt_atualizacao_nrec,
					nm_usuario_nrec,	nr_seq_agenda,
					ds_descricao,		cd_evento_log,
					nr_atendimento)
			values	(	agenda_pac_int_opme_log_seq.nextval,	sysdate + 2/86400,
					:new.nm_usuario,	sysdate,
					:new.nm_usuario,	:new.nr_seq_Agenda,
					:new.ds_descricao,	'60',
					'');

			end;
		end if;
		end;
	end if;
end if;

if	((:new.nr_atendimento is not null)  and (:new.nr_seq_agenda is null) and (nvl(nr_cirurgia_w,0) = 0) and
	((:new.cd_evento_log = '2') or (:new.cd_evento_log = '92'))) then
	begin

	select	count(*)
	into	qt_existe_atend_w
	from	atend_pac_int_opme
	where	nr_atendimento = :new.nr_atendimento;

	if	(nvl(qt_existe_atend_w,0) > 0) then
		begin
		gravar_agend_integracao(46,'nr_atendimento=' || :new.nr_atendimento || ';');

		select	nvl(max(nr_sequencia),0)
		into	nr_seq_pac_int_opme_w
		from	atend_pac_int_opme
		where	nr_atendimento = :new.nr_atendimento
		and	trunc(dt_atualizacao) = trunc(sysdate);

		if	(nr_seq_pac_int_opme_w = 0) then

			select	max(nr_sequencia)
			into	nr_seq_pac_int_opme_w
			from	atend_pac_int_opme
			where	nr_atendimento = :new.nr_atendimento
			order by dt_atualizacao desc;

		end if;

		insert into agenda_pac_int_opme_log(
				nr_sequencia,		dt_atualizacao,
				nm_usuario,		dt_atualizacao_nrec,
				nm_usuario_nrec,	nr_seq_agenda,
				ds_descricao,		cd_evento_log,
				nr_atendimento,		nr_seq_atend_opme)
		values	(	agenda_pac_int_opme_log_seq.nextval,	sysdate + 2/86400,
				:new.nm_usuario,	sysdate,
				:new.nm_usuario,	'',
				:new.ds_descricao,	'60',
				:new.nr_atendimento,	nr_seq_pac_int_opme_w);
		end ;
	end if;

	end;
end if;


end;
/


ALTER TABLE TASY.AGENDA_PAC_INT_OPME_LOG ADD (
  CONSTRAINT APOPMEL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_PAC_INT_OPME_LOG ADD (
  CONSTRAINT APOPMEL_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT APOPMEL_PESJURI_FK 
 FOREIGN KEY (CD_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT APOPMEL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT APOPMEL_ATPAIOP_FK 
 FOREIGN KEY (NR_SEQ_ATEND_OPME) 
 REFERENCES TASY.ATEND_PAC_INT_OPME (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_PAC_INT_OPME_LOG TO NIVEL_1;

