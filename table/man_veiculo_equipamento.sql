ALTER TABLE TASY.MAN_VEICULO_EQUIPAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_VEICULO_EQUIPAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_VEICULO_EQUIPAMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_VEICULO       NUMBER(10)               NOT NULL,
  NR_SEQ_EQUIPAMENTO   NUMBER(10)               NOT NULL,
  IE_TIPO              VARCHAR2(15 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANVEEQ_EMEVEIC_FK_I ON TASY.MAN_VEICULO_EQUIPAMENTO
(NR_SEQ_VEICULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANVEEQ_EMEVEIC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANVEEQ_MANEQUI_FK_I ON TASY.MAN_VEICULO_EQUIPAMENTO
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANVEEQ_MANEQUI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANVEEQ_PK ON TASY.MAN_VEICULO_EQUIPAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_VEICULO_EQUIPAMENTO ADD (
  CONSTRAINT MANVEEQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_VEICULO_EQUIPAMENTO ADD (
  CONSTRAINT MANVEEQ_EMEVEIC_FK 
 FOREIGN KEY (NR_SEQ_VEICULO) 
 REFERENCES TASY.EME_VEICULO (NR_SEQUENCIA),
  CONSTRAINT MANVEEQ_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_VEICULO_EQUIPAMENTO TO NIVEL_1;

