ALTER TABLE TASY.ESCALA_STEWART
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_STEWART CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_STEWART
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_AVALIACAO               DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  IE_CONSCIENCIA             NUMBER(1)          NOT NULL,
  IE_VIA_AEREA               NUMBER(1)          NOT NULL,
  IE_MOVIMENTO               NUMBER(1)          NOT NULL,
  IE_SAO2                    VARCHAR2(1 BYTE)   NOT NULL,
  IE_NORMOTERMIA             VARCHAR2(1 BYTE)   NOT NULL,
  IE_DOR_CONTROLADA          VARCHAR2(1 BYTE)   NOT NULL,
  IE_AUSENCIA_VOMITO         VARCHAR2(1 BYTE)   NOT NULL,
  IE_AUSENCIA_SANGRAMENTO    VARCHAR2(1 BYTE)   NOT NULL,
  IE_SINAIS_ESTAVEIS         VARCHAR2(1 BYTE)   NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_PONTUACAO               NUMBER(10),
  NR_CIRURGIA                NUMBER(10),
  NR_SEQ_PEPO                NUMBER(10),
  NR_HORA                    NUMBER(2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSTEW_ATEPACI_FK_I ON TASY.ESCALA_STEWART
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSTEW_CIRURGI_FK_I ON TASY.ESCALA_STEWART
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSTEW_PEPOCIR_FK_I ON TASY.ESCALA_STEWART
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSTEW_PESFISI_FK_I ON TASY.ESCALA_STEWART
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSTEW_PK ON TASY.ESCALA_STEWART
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSTEW_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCSTEW_TASASDI_FK_I ON TASY.ESCALA_STEWART
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSTEW_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCSTEW_TASASDI_FK2_I ON TASY.ESCALA_STEWART
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCSTEW_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_STEWART_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ESCALA_STEWART FOR EACH ROW
declare
qt_reg_w	number(1);
BEGIN


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_pontuacao	:= 	:new.IE_CONSCIENCIA +
				:new.IE_VIA_AEREA +
				:new.IE_MOVIMENTO;
if	(nvl(:new.IE_SAO2,'N') = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;
if	(nvl(:new.IE_NORMOTERMIA,'N') = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;
if	(nvl(:new.IE_DOR_CONTROLADA,'N') = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;
if	(nvl(:new.IE_AUSENCIA_VOMITO,'N') = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;
if	(nvl(:new.IE_AUSENCIA_SANGRAMENTO,'N') = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;
if	(nvl(:new.IE_SINAIS_ESTAVEIS,'N') = 'S') then
	:new.qt_pontuacao	:= :new.qt_pontuacao + 1;
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.escala_stewart_pend_atual
after insert or update ON TASY.ESCALA_STEWART for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'71');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_STEWART_delete
after delete ON TASY.ESCALA_STEWART for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '71'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_STEWART ADD (
  CONSTRAINT ESCSTEW_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_STEWART ADD (
  CONSTRAINT ESCSTEW_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCSTEW_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCSTEW_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCSTEW_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCSTEW_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT ESCSTEW_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_STEWART TO NIVEL_1;

