ALTER TABLE TASY.REGRA_PRESCR_MEDIC_SN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PRESCR_MEDIC_SN CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PRESCR_MEDIC_SN
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_MATERIAL           NUMBER(10),
  CD_GRUPO_MATERIAL     NUMBER(3),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  IE_PERMITE_SN         VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RPREMSN_CLAMATE_FK_I ON TASY.REGRA_PRESCR_MEDIC_SN
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPREMSN_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPREMSN_GRUMATE_FK_I ON TASY.REGRA_PRESCR_MEDIC_SN
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPREMSN_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPREMSN_MATERIA_FK_I ON TASY.REGRA_PRESCR_MEDIC_SN
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPREMSN_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RPREMSN_PK ON TASY.REGRA_PRESCR_MEDIC_SN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RPREMSN_SUBMATE_FK_I ON TASY.REGRA_PRESCR_MEDIC_SN
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPREMSN_SUBMATE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_PRESCR_MEDIC_SN ADD (
  CONSTRAINT RPREMSN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_PRESCR_MEDIC_SN ADD (
  CONSTRAINT RPREMSN_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT RPREMSN_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT RPREMSN_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT RPREMSN_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL));

GRANT SELECT ON TASY.REGRA_PRESCR_MEDIC_SN TO NIVEL_1;

