ALTER TABLE TASY.TABELA_ATRIBUTO_IDIOMA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TABELA_ATRIBUTO_IDIOMA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TABELA_ATRIBUTO_IDIOMA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_TABELA            VARCHAR2(50 BYTE)        NOT NULL,
  NM_ATRIBUTO          VARCHAR2(50 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_VISAO         NUMBER(10),
  NR_SEQ_IDIOMA        NUMBER(10)               NOT NULL,
  DS_ATRIBUTO          VARCHAR2(4000 BYTE)      NOT NULL,
  DS_LABEL             VARCHAR2(50 BYTE),
  DS_LABEL_GRID        VARCHAR2(50 BYTE),
  DS_LABEL_LONGO       VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TABATID_PK ON TASY.TABELA_ATRIBUTO_IDIOMA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TABATID_PK
  MONITORING USAGE;


CREATE INDEX TASY.TABATID_TABATRI_FK_I ON TASY.TABELA_ATRIBUTO_IDIOMA
(NM_TABELA, NM_ATRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATID_TABVIAT_FK_I ON TASY.TABELA_ATRIBUTO_IDIOMA
(NR_SEQ_VISAO, NM_ATRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABATID_TASYIDI_FK_I ON TASY.TABELA_ATRIBUTO_IDIOMA
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TABATID_TASYIDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TABELA_ATRIBUTO_IDIOMA ADD (
  CONSTRAINT TABATID_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TABELA_ATRIBUTO_IDIOMA ADD (
  CONSTRAINT TABATID_TABATRI_FK 
 FOREIGN KEY (NM_TABELA, NM_ATRIBUTO) 
 REFERENCES TASY.TABELA_ATRIBUTO (NM_TABELA,NM_ATRIBUTO),
  CONSTRAINT TABATID_TASYIDI_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.TASY_IDIOMA (NR_SEQUENCIA),
  CONSTRAINT TABATID_TABVIAT_FK 
 FOREIGN KEY (NR_SEQ_VISAO, NM_ATRIBUTO) 
 REFERENCES TASY.TABELA_VISAO_ATRIBUTO (NR_SEQUENCIA,NM_ATRIBUTO));

GRANT SELECT ON TASY.TABELA_ATRIBUTO_IDIOMA TO NIVEL_1;

