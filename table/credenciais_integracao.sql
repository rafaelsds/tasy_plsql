ALTER TABLE TASY.CREDENCIAIS_INTEGRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CREDENCIAIS_INTEGRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CREDENCIAIS_INTEGRACAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_SISTEMA               VARCHAR2(15 BYTE)    NOT NULL,
  DS_ENDERECO              VARCHAR2(255 BYTE),
  DS_LOGIN                 VARCHAR2(255 BYTE),
  DS_SENHA                 VARCHAR2(255 BYTE),
  IE_FORMATO_ARQUIVO       VARCHAR2(15 BYTE),
  NR_IP_SERV_PROXY         VARCHAR2(100 BYTE),
  NR_PORTA_SERV_PROXY      VARCHAR2(100 BYTE),
  NM_USUARIO_SERV_PROXY    VARCHAR2(100 BYTE),
  DS_SENHA_SERV_PROXY      VARCHAR2(100 BYTE),
  DS_ACTION                VARCHAR2(255 BYTE),
  DS_ENDERECO_CERTIFICADO  VARCHAR2(255 BYTE),
  DS_SENHA_CERTIFICADO     VARCHAR2(255 BYTE),
  DS_USUARIO_HTTP          VARCHAR2(100 BYTE),
  DS_SENHA_HTTP            VARCHAR2(200 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CREDINTE_ESTABEL_FK_I ON TASY.CREDENCIAIS_INTEGRACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CREDINTE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CREDINTE_PK ON TASY.CREDENCIAIS_INTEGRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CREDENCIAIS_INTEGRACAO_tp  after update ON TASY.CREDENCIAIS_INTEGRACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_IP_SERV_PROXY,1,4000),substr(:new.NR_IP_SERV_PROXY,1,4000),:new.nm_usuario,nr_seq_w,'NR_IP_SERV_PROXY',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PORTA_SERV_PROXY,1,4000),substr(:new.NR_PORTA_SERV_PROXY,1,4000),:new.nm_usuario,nr_seq_w,'NR_PORTA_SERV_PROXY',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_SERV_PROXY,1,4000),substr(:new.NM_USUARIO_SERV_PROXY,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_SERV_PROXY',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA_SERV_PROXY,1,4000),substr(:new.DS_SENHA_SERV_PROXY,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA_SERV_PROXY',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA,1,4000),substr(:new.DS_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SISTEMA,1,4000),substr(:new.IE_SISTEMA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SISTEMA',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOGIN,1,4000),substr(:new.DS_LOGIN,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOGIN',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ACTION,1,4000),substr(:new.DS_ACTION,1,4000),:new.nm_usuario,nr_seq_w,'DS_ACTION',ie_log_w,ds_w,'CREDENCIAIS_INTEGRACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CREDENCIAIS_INTEGRACAO ADD (
  CONSTRAINT CREDINTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CREDENCIAIS_INTEGRACAO ADD (
  CONSTRAINT CREDINTE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CREDENCIAIS_INTEGRACAO TO NIVEL_1;

