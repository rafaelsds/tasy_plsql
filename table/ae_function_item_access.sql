ALTER TABLE TASY.AE_FUNCTION_ITEM_ACCESS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AE_FUNCTION_ITEM_ACCESS CASCADE CONSTRAINTS;

CREATE TABLE TASY.AE_FUNCTION_ITEM_ACCESS
(
  EVENT_ID                      VARCHAR2(36 BYTE) NOT NULL,
  TIMESTAMP                     TIMESTAMP(6)    NOT NULL,
  TIMESTAMP_ZONE_ID             VARCHAR2(256 BYTE),
  PARENT_EVENT_ID               VARCHAR2(36 BYTE),
  ROOT_EVENT_ID                 VARCHAR2(36 BYTE),
  USER_ID                       VARCHAR2(100 BYTE),
  USER_FULLNAME                 VARCHAR2(256 BYTE),
  ESTABLISHMENT_CODE            NUMBER(8),
  ESTABLISHMENT_NAME            VARCHAR2(256 BYTE),
  PATIENT_DATA_ACCESS_EVENT_ID  VARCHAR2(36 BYTE),
  JUSTIFICATION_CODE            NUMBER(10),
  JUSTIFICATION_TEXT            VARCHAR2(2000 BYTE),
  SCHEMATIC_OBJECT_CODE         NUMBER(10),
  RULE_CODE                     NUMBER(10),
  RULE_NAME                     VARCHAR2(256 BYTE),
  ITEM_DESCRIPTION              VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PK_AE_FUNCTION_ITEM_ACCESS ON TASY.AE_FUNCTION_ITEM_ACCESS
(EVENT_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AE_FUNCTION_ITEM_ACCESS ADD (
  CONSTRAINT PK_AE_FUNCTION_ITEM_ACCESS
 PRIMARY KEY
 (EVENT_ID)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AE_FUNCTION_ITEM_ACCESS TO NIVEL_1;

