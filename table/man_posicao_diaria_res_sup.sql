ALTER TABLE TASY.MAN_POSICAO_DIARIA_RES_SUP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_POSICAO_DIARIA_RES_SUP CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_POSICAO_DIARIA_RES_SUP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_POSICAO       NUMBER(10)               NOT NULL,
  NR_SEQ_GERENCIA_SUP  NUMBER(10)               NOT NULL,
  NR_SEQ_GRUPO_SUP     NUMBER(10)               NOT NULL,
  QT_60_DIA            NUMBER(10)               NOT NULL,
  QT_45_DIA            NUMBER(10)               NOT NULL,
  QT_30_DIA            NUMBER(10)               NOT NULL,
  QT_15_DIA            NUMBER(10)               NOT NULL,
  QT_07_DIA            NUMBER(10),
  QT_SEMANA            NUMBER(10)               NOT NULL,
  QT_TOTAL             NUMBER(10)               NOT NULL,
  QT_OS_ANTIGA         NUMBER(10)               NOT NULL,
  PR_OS_ANTIGA         NUMBER(15)               NOT NULL,
  NR_SEQ_ESTAGIO       NUMBER(10),
  IE_TIPO              VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MAPODRS_GERWHEB_FK_I ON TASY.MAN_POSICAO_DIARIA_RES_SUP
(NR_SEQ_GERENCIA_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MAPODRS_GRUSUPO_FK_I ON TASY.MAN_POSICAO_DIARIA_RES_SUP
(NR_SEQ_GRUPO_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MAPODRS_MANPODI_FK_I ON TASY.MAN_POSICAO_DIARIA_RES_SUP
(NR_SEQ_POSICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MAPODRS_PK ON TASY.MAN_POSICAO_DIARIA_RES_SUP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_POSICAO_DIARIA_RES_SUP ADD (
  CONSTRAINT MAPODRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_POSICAO_DIARIA_RES_SUP ADD (
  CONSTRAINT MAPODRS_GERWHEB_FK 
 FOREIGN KEY (NR_SEQ_GERENCIA_SUP) 
 REFERENCES TASY.GERENCIA_WHEB (NR_SEQUENCIA),
  CONSTRAINT MAPODRS_GRUSUPO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SUP) 
 REFERENCES TASY.GRUPO_SUPORTE (NR_SEQUENCIA),
  CONSTRAINT MAPODRS_MANPODI_FK 
 FOREIGN KEY (NR_SEQ_POSICAO) 
 REFERENCES TASY.MAN_POSICAO_DIARIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_POSICAO_DIARIA_RES_SUP TO NIVEL_1;

