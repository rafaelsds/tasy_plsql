ALTER TABLE TASY.SISMAMA_ANAMNESE_RAD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SISMAMA_ANAMNESE_RAD CASCADE CONSTRAINTS;

CREATE TABLE TASY.SISMAMA_ANAMNESE_RAD
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_CGC_CLINICA            VARCHAR2(14 BYTE),
  DT_RECEBIMENTO            DATE,
  NR_EXAME                  NUMBER(10),
  IE_NAO_SUS                VARCHAR2(1 BYTE),
  DT_ULT_MESTRUACAO         DATE,
  IE_NUNCA_MENSTRUOU        VARCHAR2(1 BYTE)    NOT NULL,
  IE_NAO_LEMBRA_ULT_MENSTR  VARCHAR2(1 BYTE),
  NR_IDADE_MENOPAUSA        NUMBER(3),
  IE_NAO_LEMBRA_MENOPAUSA   VARCHAR2(1 BYTE),
  IE_REMEDIO_MENOPAUSA      VARCHAR2(1 BYTE),
  IE_GRAVIDA                VARCHAR2(1 BYTE),
  IE_RADIOTERAPIA           VARCHAR2(1 BYTE),
  DT_RADIOTERAPIA_DIR       VARCHAR2(4 BYTE),
  DT_RADIOTERAPIA_ESQ       VARCHAR2(4 BYTE),
  IE_NAO_FEZ_CIRURGIA       VARCHAR2(1 BYTE),
  DT_TUMORECTOMIA_ESQ       VARCHAR2(4 BYTE),
  DT_TUMORECTOMIA_DIR       VARCHAR2(4 BYTE),
  DT_SEGMENTECTOMIA_ESQ     VARCHAR2(4 BYTE),
  DT_SEGMENTECTOMIA_DIR     VARCHAR2(4 BYTE),
  DT_DUTECTOMIA_ESQ         VARCHAR2(4 BYTE),
  DT_DUTECTOMIA_DIR         VARCHAR2(4 BYTE),
  DT_MASTECTOMIA_DIR        VARCHAR2(4 BYTE),
  DT_MASTECTOMIA_ESQ        VARCHAR2(4 BYTE),
  DT_MASTECTOMIA_PELE_DIR   VARCHAR2(4 BYTE),
  DT_MASTECTOMIA_PELE_ESQ   VARCHAR2(4 BYTE),
  DT_ESVAZIAMENTO_ESQ       VARCHAR2(4 BYTE),
  DT_ESVAZIAMENTO_DIR       VARCHAR2(4 BYTE),
  DT_BIOPSIA_LINF_SENT_ESQ  VARCHAR2(4 BYTE),
  DT_BIOPSIA_LINF_SENT_DIR  VARCHAR2(4 BYTE),
  DT_RECONSTRUCAO_DIR       VARCHAR2(4 BYTE),
  DT_RECONSTRUCAO_ESQ       VARCHAR2(4 BYTE),
  DT_PLASTIC_REDUTORA_DIR   VARCHAR2(4 BYTE),
  DT_PLASTIC_REDUTORA_ESQ   VARCHAR2(4 BYTE),
  DT_PLASTIC_IMPLANTE_ESQ   VARCHAR2(4 BYTE),
  DT_PLASTIC_IMPLANTE_DIR   VARCHAR2(4 BYTE),
  DT_RADIOTERAPIA_DIR2      VARCHAR2(4 BYTE),
  DT_RADIOTERAPIA_ESQ2      VARCHAR2(4 BYTE),
  NR_SEQ_SISMAMA            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SISANAR_PESJURI_FK_I ON TASY.SISMAMA_ANAMNESE_RAD
(CD_CGC_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SISANAR_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SISANAR_PK ON TASY.SISMAMA_ANAMNESE_RAD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SISANAR_SISMATE_FK_I ON TASY.SISMAMA_ANAMNESE_RAD
(NR_SEQ_SISMAMA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SISMAMA_ANAMNESE_RAD ADD (
  CONSTRAINT SISANAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SISMAMA_ANAMNESE_RAD ADD (
  CONSTRAINT SISANAR_PESJURI_FK 
 FOREIGN KEY (CD_CGC_CLINICA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT SISANAR_SISMATE_FK 
 FOREIGN KEY (NR_SEQ_SISMAMA) 
 REFERENCES TASY.SISMAMA_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SISMAMA_ANAMNESE_RAD TO NIVEL_1;

