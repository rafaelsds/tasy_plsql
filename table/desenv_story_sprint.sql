ALTER TABLE TASY.DESENV_STORY_SPRINT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DESENV_STORY_SPRINT CASCADE CONSTRAINTS;

CREATE TABLE TASY.DESENV_STORY_SPRINT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_STORY             NUMBER(10),
  NR_SPRINT            NUMBER(10),
  CD_STATUS            NUMBER(10),
  NR_ORDEM_SERVICO     NUMBER(10),
  IE_CANCELADO_SPRINT  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DESSTSP_DESSPRI_FK_I ON TASY.DESENV_STORY_SPRINT
(NR_SPRINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESSTSP_DESSTOR_FK_I ON TASY.DESENV_STORY_SPRINT
(NR_STORY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESSTSP_MANORSE_FK_I ON TASY.DESENV_STORY_SPRINT
(NR_ORDEM_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DESSTSP_PK ON TASY.DESENV_STORY_SPRINT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.desenv_story_sprint_atual
after insert or update ON TASY.DESENV_STORY_SPRINT for each row
declare

cd_tipo_w	varchar2(3);

begin

if	(inserting) then
	cd_tipo_w := 'SC';
else
	cd_tipo_w := 'SU';
end if;

insert into desenv_atividade(
	nr_sequencia,
	nr_epic,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_release,
	nr_release_destino,
	nr_art,
	nr_art_destino,
	cd_tipo,
	nr_feature,
	nr_story,
	cd_status,
	nr_sprint)
values(	desenv_atividade_seq.nextval,
	null,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	null,
	null,
	null,
	null,
	cd_tipo_w,
	null,
	:new.nr_story,
	:new.cd_status,
	:new.nr_sprint);

end;
/


ALTER TABLE TASY.DESENV_STORY_SPRINT ADD (
  CONSTRAINT DESSTSP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DESENV_STORY_SPRINT ADD (
  CONSTRAINT DESSTSP_DESSPRI_FK 
 FOREIGN KEY (NR_SPRINT) 
 REFERENCES TASY.DESENV_SPRINT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DESSTSP_DESSTOR_FK 
 FOREIGN KEY (NR_STORY) 
 REFERENCES TASY.DESENV_STORY (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DESSTSP_MANORSE_FK 
 FOREIGN KEY (NR_ORDEM_SERVICO) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.DESENV_STORY_SPRINT TO NIVEL_1;

