ALTER TABLE TASY.PROJ_REMUNERACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_REMUNERACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_REMUNERACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  DT_REFERENCIA        DATE                     NOT NULL,
  DT_CALCULO           DATE                     NOT NULL,
  VL_MARGEM            NUMBER(15,2)             NOT NULL,
  QT_ESCORE            NUMBER(15,2)             NOT NULL,
  VL_BASE_CALCULO      NUMBER(15,2)             NOT NULL,
  PR_GERENTE           NUMBER(15,2)             NOT NULL,
  PR_COORDENADOR       NUMBER(15,2)             NOT NULL,
  VL_GERENTE           NUMBER(15,2)             NOT NULL,
  VL_COORDENADOR       NUMBER(15,2)             NOT NULL,
  VL_GERENTES          NUMBER(15,2)             NOT NULL,
  VL_COORDENADORES     NUMBER(15,2)             NOT NULL,
  DT_LIBERACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROJREM_ESTABEL_FK_I ON TASY.PROJ_REMUNERACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJREM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROJREM_PK ON TASY.PROJ_REMUNERACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJREM_PK
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_REMUNERACAO ADD (
  CONSTRAINT PROJREM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_REMUNERACAO ADD (
  CONSTRAINT PROJREM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PROJ_REMUNERACAO TO NIVEL_1;

