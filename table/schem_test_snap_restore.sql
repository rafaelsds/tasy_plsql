ALTER TABLE TASY.SCHEM_TEST_SNAP_RESTORE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SCHEM_TEST_SNAP_RESTORE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SCHEM_TEST_SNAP_RESTORE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVE     VARCHAR2(255 BYTE),
  DS_BKP               VARCHAR2(1000 BYTE),
  NR_SEQ_SNAPSHOT      NUMBER(10),
  NR_SEQ_SNAP_BKP      NUMBER(10),
  DS_NAME              VARCHAR2(1000 BYTE),
  DS_INFO              VARCHAR2(1000 BYTE),
  IE_JOBS              VARCHAR2(255 BYTE),
  IE_SWITCH            VARCHAR2(255 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  DT_EXEC              DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SCTESRE_PK ON TASY.SCHEM_TEST_SNAP_RESTORE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTESRE_SCTESBK_FK_I ON TASY.SCHEM_TEST_SNAP_RESTORE
(NR_SEQ_SNAP_BKP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCTESRE_SCTESNA_FK_I ON TASY.SCHEM_TEST_SNAP_RESTORE
(NR_SEQ_SNAPSHOT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SCHEM_TEST_SNAP_RESTORE ADD (
  CONSTRAINT SCTESRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.SCHEM_TEST_SNAP_RESTORE TO NIVEL_1;

