ALTER TABLE TASY.EME_REGRA_ENVIO_EMAIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EME_REGRA_ENVIO_EMAIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.EME_REGRA_ENVIO_EMAIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TITULO            VARCHAR2(80 BYTE)        NOT NULL,
  DS_CONTEUDO          VARCHAR2(4000 BYTE)      NOT NULL,
  IE_TIPO_EMAIL        VARCHAR2(3 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.EMEREEE_PK ON TASY.EME_REGRA_ENVIO_EMAIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.eme_regra_envio_email_atual
before insert or update ON TASY.EME_REGRA_ENVIO_EMAIL for each row
declare
qt_reg_emissao_boleto_w 	number	:= 0;
begin

if (:new.ie_tipo_email = 'E') then

	select count(*)
	into qt_reg_emissao_boleto_w
	from eme_regra_envio_email
	where ie_tipo_email = 'E'
	and nr_sequencia <> :new.nr_sequencia;

	if (qt_reg_emissao_boleto_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1142147);
	end if;
end if;

end;
/


ALTER TABLE TASY.EME_REGRA_ENVIO_EMAIL ADD (
  CONSTRAINT EMEREEE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.EME_REGRA_ENVIO_EMAIL TO NIVEL_1;

