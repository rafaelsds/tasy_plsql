ALTER TABLE TASY.TISS_CONTA_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_CONTA_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_CONTA_PROC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_INTERNO_CONTA           NUMBER(15),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_SEQ_GUIA                NUMBER(10),
  CD_PROCEDIMENTO            VARCHAR2(20 BYTE),
  CD_EDICAO_AMB              VARCHAR2(10 BYTE),
  QT_PROCEDIMENTO            NUMBER(15,4),
  VL_REDUCAO_ACRESCIMO       NUMBER(15,2),
  VL_PROCEDIMENTO            NUMBER(15,2),
  IE_VIA_ACESSO              VARCHAR2(10 BYTE),
  VL_UNITARIO                NUMBER(15,2),
  DS_PROCEDIMENTO            VARCHAR2(255 BYTE),
  DT_PROCEDIMENTO            DATE,
  IE_TISS_TIPO_GUIA          VARCHAR2(20 BYTE)  NOT NULL,
  CD_AUTORIZACAO             VARCHAR2(20 BYTE)  NOT NULL,
  NR_SEQ_PROC                NUMBER(15),
  DT_INICIO_CIR              DATE,
  DT_FIM_CIR                 DATE,
  CD_CGC_PRESTADOR           VARCHAR2(14 BYTE),
  IE_FUNCAO_MEDICO           VARCHAR2(20 BYTE),
  CD_MEDICO_EXECUTOR         VARCHAR2(10 BYTE),
  CD_ESPECIALIDADE           NUMBER(10),
  IE_HONORARIO               VARCHAR2(3 BYTE),
  IE_TECNICA_UTILIZADA       VARCHAR2(40 BYTE),
  IE_RESPONSAVEL_CREDITO     VARCHAR2(5 BYTE),
  CD_SENHA                   VARCHAR2(200 BYTE),
  CD_CBOS                    VARCHAR2(20 BYTE),
  CD_PRESTADOR_TISS          VARCHAR2(40 BYTE),
  CD_CGC_PREST_SOLIC_TISS    VARCHAR2(14 BYTE),
  CD_MEDICO_EXEC_TISS        VARCHAR2(10 BYTE),
  NR_SEQ_MED_FATUR           NUMBER(10),
  DS_PRESTADOR_TISS          VARCHAR2(255 BYTE),
  IE_CONTRAT_MED             VARCHAR2(5 BYTE),
  IE_TIPO_ATEND_TISS         VARCHAR2(20 BYTE),
  NR_ATEND_MED               NUMBER(10),
  NR_SEQ_PROT_MED            NUMBER(10),
  CD_MED_SOLIC_TASYMED       VARCHAR2(10 BYTE),
  NR_SEQ_REAP_CONTA          NUMBER(10),
  DS_JUSTIFICATIVA_REVISAO   VARCHAR2(4000 BYTE),
  CD_MEDICO_SOLIC_TISS       VARCHAR2(10 BYTE),
  CD_MEDICO_PROF_SOLIC_TISS  VARCHAR2(10 BYTE),
  CD_FUNCIONARIO_HONOR       VARCHAR2(10 BYTE),
  NR_SEQ_LOTE_HIST           NUMBER(10),
  IE_AGRUPADO                VARCHAR2(1 BYTE),
  CD_PRESTADOR_SOLIC_TISS    VARCHAR2(40 BYTE),
  DS_PRESTADOR_SOLIC_TISS    VARCHAR2(255 BYTE),
  CD_MEDICO_HONOR_TISS       VARCHAR2(10 BYTE),
  IE_XML                     VARCHAR2(1 BYTE),
  CD_MEDICO_CONVENIO         VARCHAR2(20 BYTE),
  CD_CGC_PREST_HONOR_TISS    VARCHAR2(14 BYTE),
  NM_MEDICO_EXTERNO          VARCHAR2(255 BYTE),
  CRM_MEDICO_EXTERNO         VARCHAR2(30 BYTE),
  NR_SEQ_APRESENTACAO        NUMBER(10),
  CD_MEDICO_EXEC_PROC_HONOR  VARCHAR2(10 BYTE),
  DS_INDICACAO_PROC          VARCHAR2(255 BYTE),
  DS_FACE_DENTE              VARCHAR2(5 BYTE),
  CD_DENTE                   NUMBER(3),
  CD_REGIAO_BOCA             VARCHAR2(20 BYTE),
  NR_GUIA_PRESTADOR          VARCHAR2(20 BYTE),
  NR_GUIA_OPERADORA          VARCHAR2(255 BYTE),
  CD_SETOR_EXECUCAO          NUMBER(10),
  NR_SEQUENCIA_ITEM          NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          136M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSCOP_CONPACI_FK_I ON TASY.TISS_CONTA_PROC
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          22M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSCOP_LOTAUHI_FK_I ON TASY.TISS_CONTA_PROC
(NR_SEQ_LOTE_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSCOP_LOTAUHI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSCOP_MEDATEN_FK_I ON TASY.TISS_CONTA_PROC
(NR_ATEND_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSCOP_MEDFATU_FK_I ON TASY.TISS_CONTA_PROC
(NR_SEQ_MED_FATUR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSCOP_MEDFATU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSCOP_MEDPRCO_FK_I ON TASY.TISS_CONTA_PROC
(NR_SEQ_PROT_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSCOP_MEDPRCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSCOP_PESFISI_FK_I ON TASY.TISS_CONTA_PROC
(CD_MEDICO_EXEC_TISS)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSCOP_PESFISI_FK2_I ON TASY.TISS_CONTA_PROC
(CD_MED_SOLIC_TASYMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSCOP_PESFISI_FK3_I ON TASY.TISS_CONTA_PROC
(CD_MEDICO_HONOR_TISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSCOP_PESJURI_FK_I ON TASY.TISS_CONTA_PROC
(CD_CGC_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSCOP_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSCOP_PESJURI_FK2_I ON TASY.TISS_CONTA_PROC
(CD_CGC_PREST_SOLIC_TISS)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSCOP_PESJURI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSCOP_PK ON TASY.TISS_CONTA_PROC
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          19M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSCOP_TISSCOG_FK_I ON TASY.TISS_CONTA_PROC
(NR_SEQ_GUIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          20M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TISSCOP_TISSREC_FK_I ON TASY.TISS_CONTA_PROC
(NR_SEQ_REAP_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSCOP_TISSREC_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TISS_CONTA_PROC ADD (
  CONSTRAINT TISSCOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          19M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_CONTA_PROC ADD (
  CONSTRAINT TISSCOP_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TISSCOP_TISSCOG_FK 
 FOREIGN KEY (NR_SEQ_GUIA) 
 REFERENCES TASY.TISS_CONTA_GUIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TISSCOP_PESJURI_FK2 
 FOREIGN KEY (CD_CGC_PREST_SOLIC_TISS) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TISSCOP_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_EXEC_TISS) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TISSCOP_MEDFATU_FK 
 FOREIGN KEY (NR_SEQ_MED_FATUR) 
 REFERENCES TASY.MED_FATURAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TISSCOP_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA)
    ON DELETE CASCADE,
  CONSTRAINT TISSCOP_MEDATEN_FK 
 FOREIGN KEY (NR_ATEND_MED) 
 REFERENCES TASY.MED_ATENDIMENTO (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT TISSCOP_MEDPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROT_MED) 
 REFERENCES TASY.MED_PROT_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TISSCOP_PESFISI_FK2 
 FOREIGN KEY (CD_MED_SOLIC_TASYMED) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TISSCOP_TISSREC_FK 
 FOREIGN KEY (NR_SEQ_REAP_CONTA) 
 REFERENCES TASY.TISS_REAP_CONTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TISSCOP_LOTAUHI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_HIST) 
 REFERENCES TASY.LOTE_AUDIT_HIST (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TISSCOP_PESFISI_FK3 
 FOREIGN KEY (CD_MEDICO_HONOR_TISS) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.TISS_CONTA_PROC TO NIVEL_1;

