ALTER TABLE TASY.PLS_TIPO_ANEXO_TWS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TIPO_ANEXO_TWS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TIPO_ANEXO_TWS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_REGRA_ANEXO     NUMBER(10)             NOT NULL,
  IE_OBRIGATORIO         VARCHAR2(1 BYTE)       NOT NULL,
  DS_LABEL_CAMPO         VARCHAR2(60 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_DOCUMENTO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTANT_PK ON TASY.PLS_TIPO_ANEXO_TWS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTANT_PLSRAT_FK_I ON TASY.PLS_TIPO_ANEXO_TWS
(NR_SEQ_REGRA_ANEXO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTANT_TIPODOC_FK_I ON TASY.PLS_TIPO_ANEXO_TWS
(NR_SEQ_TIPO_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_TIPO_ANEXO_TWS ADD (
  CONSTRAINT PLSTANT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TIPO_ANEXO_TWS ADD (
  CONSTRAINT PLSTANT_PLSRAT_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ANEXO) 
 REFERENCES TASY.PLS_REGRA_ANEXO_TWS (NR_SEQUENCIA),
  CONSTRAINT PLSTANT_TIPODOC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOCUMENTO) 
 REFERENCES TASY.TIPO_DOCUMENTACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TIPO_ANEXO_TWS TO NIVEL_1;

