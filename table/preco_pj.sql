ALTER TABLE TASY.PRECO_PJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRECO_PJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_PJ
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4),
  CD_CGC                    VARCHAR2(14 BYTE)   NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_VIGENCIA               DATE,
  CD_ITEM                   VARCHAR2(20 BYTE),
  DS_ITEM                   VARCHAR2(100 BYTE),
  CD_BARRA                  VARCHAR2(20 BYTE),
  VL_ITEM                   NUMBER(17,4),
  CD_MATERIAL               NUMBER(6),
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_VIGENCIA_FIM           DATE,
  CD_UNIDADE_FORNEC         VARCHAR2(20 BYTE),
  CD_NATUREZA_OPERACAO      NUMBER(4),
  CD_UNIDADE_MEDIDA_COMPRA  VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREPJ_ESTABEL_FK_I ON TASY.PRECO_PJ
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPJ_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREPJ_MATERIA_FK_I ON TASY.PRECO_PJ
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPJ_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREPJ_NATOPER_FK_I ON TASY.PRECO_PJ
(CD_NATUREZA_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPJ_NATOPER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREPJ_PESJURI_FK_I ON TASY.PRECO_PJ
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPJ_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREPJ_PK ON TASY.PRECO_PJ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREPJ_UNIMEDI_FK_I ON TASY.PRECO_PJ
(CD_UNIDADE_MEDIDA_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREPJ_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PRECO_PJ_tp  after update ON TASY.PRECO_PJ FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'PRECO_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ITEM,1,4000),substr(:new.CD_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'CD_ITEM',ie_log_w,ds_w,'PRECO_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_BARRA,1,4000),substr(:new.CD_BARRA,1,4000),:new.nm_usuario,nr_seq_w,'CD_BARRA',ie_log_w,ds_w,'PRECO_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ITEM,1,4000),substr(:new.DS_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_ITEM',ie_log_w,ds_w,'PRECO_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA_FIM,1,4000),substr(:new.DT_VIGENCIA_FIM,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA_FIM',ie_log_w,ds_w,'PRECO_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'PRECO_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA,1,4000),substr(:new.DT_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA',ie_log_w,ds_w,'PRECO_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_FORNEC,1,4000),substr(:new.CD_UNIDADE_FORNEC,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_FORNEC',ie_log_w,ds_w,'PRECO_PJ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ITEM,1,4000),substr(:new.VL_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'VL_ITEM',ie_log_w,ds_w,'PRECO_PJ',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PRECO_PJ ADD (
  CONSTRAINT PREPJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRECO_PJ ADD (
  CONSTRAINT PREPJ_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_COMPRA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PREPJ_NATOPER_FK 
 FOREIGN KEY (CD_NATUREZA_OPERACAO) 
 REFERENCES TASY.NATUREZA_OPERACAO (CD_NATUREZA_OPERACAO),
  CONSTRAINT PREPJ_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PREPJ_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PREPJ_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.PRECO_PJ TO NIVEL_1;

