ALTER TABLE TASY.WL_REGRA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.WL_REGRA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.WL_REGRA_ITEM
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  IE_ESCALA                     VARCHAR2(15 BYTE) DEFAULT null,
  IE_EVOLUCAO_CLINICA           VARCHAR2(3 BYTE),
  IE_CLASSIFICACAO_DOENCA       VARCHAR2(1 BYTE),
  IE_CLASSIFICACAO_DIAGNOSTICO  VARCHAR2(1 BYTE),
  QT_TEMPO_REGRA_ATRASO         NUMBER(10),
  NR_SEQ_REGRA                  NUMBER(10),
  NR_SEQ_TIPO_PARECER           NUMBER(10),
  QT_TEMPO_REGRA                NUMBER(10),
  IE_OPCAO_WL                   VARCHAR2(2 BYTE),
  IE_TIPO_DIAGNOSTICO           NUMBER(3),
  NR_SEQ_DISPOSITIVO            NUMBER(10),
  IE_SITUACAO                   VARCHAR2(1 BYTE),
  QT_TEMPO_ATRASO               NUMBER(10),
  DS_ACAO                       VARCHAR2(255 BYTE),
  NR_SEQ_CLASSIF_DIAG           NUMBER(10),
  QT_TEMPO_NORMAL               NUMBER(5),
  IE_ENCERRAR_ALTA              VARCHAR2(1 BYTE),
  IE_CONSIDERAR_ALTA            VARCHAR2(1 BYTE),
  IE_TIPO_ATENDIMENTO           NUMBER(3),
  NR_SEQ_TIPO_ADMISSAO_FAT      NUMBER(10),
  IE_TIPO_PEND_CARTA            VARCHAR2(1 BYTE),
  IE_CLASSIF_DIAG               VARCHAR2(3 BYTE),
  IE_ENCERRAR_ALTA_ADM          VARCHAR2(1 BYTE) DEFAULT null,
  IE_TIPO_PEND_EMISSAO          VARCHAR2(10 BYTE),
  IE_TIPO_ITEM_PEP              VARCHAR2(15 BYTE),
  IE_DISCHARGE                  VARCHAR2(10 BYTE),
  IE_EXTERNAL_TRANSFER          VARCHAR2(10 BYTE),
  IE_INTERNAL_TRANSFER          VARCHAR2(10 BYTE),
  IE_TIPO_PEND_LAUDO            VARCHAR2(2 BYTE),
  IE_EVENTO_CARTA_MED           VARCHAR2(1 BYTE),
  IE_MODELO_CARTA               VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WLREGIT_DISPOSI_FK_I ON TASY.WL_REGRA_ITEM
(NR_SEQ_DISPOSITIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WLREGIT_PK ON TASY.WL_REGRA_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WLREGIT_TIPEVOL_FK_I ON TASY.WL_REGRA_ITEM
(IE_EVOLUCAO_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WLREGIT_WLREWO_FK_I ON TASY.WL_REGRA_ITEM
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.wl_regra_item_atual
before insert ON TASY.WL_REGRA_ITEM for each row
declare
ie_regra_item_w		char(1);

begin


select	nvl2(max(ri.nr_sequencia),'S','N')
into	ie_regra_item_w
from	wl_regra_item ri,
	wl_regra_worklist rw,
	wl_item	it
where	rw.nr_sequencia = ri.nr_seq_regra
and	it.nr_sequencia = rw.nr_seq_item
and	ri.nr_seq_regra = :new.nr_seq_regra
and	it.cd_categoria = 'MT';

if (ie_regra_item_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(1014493,'DS_CATEGORIA=' || obter_desc_expressao(857157));
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.WL_REGRA_ITEM_tp  after update ON TASY.WL_REGRA_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TIPO_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'WL_REGRA_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIFICACAO_DOENCA,1,4000),substr(:new.IE_CLASSIFICACAO_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIFICACAO_DOENCA',ie_log_w,ds_w,'WL_REGRA_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.WL_REGRA_ITEM ADD (
  CONSTRAINT WLREGIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.WL_REGRA_ITEM ADD (
  CONSTRAINT WLREGIT_DISPOSI_FK 
 FOREIGN KEY (NR_SEQ_DISPOSITIVO) 
 REFERENCES TASY.DISPOSITIVO (NR_SEQUENCIA),
  CONSTRAINT WLREGIT_TIPEVOL_FK 
 FOREIGN KEY (IE_EVOLUCAO_CLINICA) 
 REFERENCES TASY.TIPO_EVOLUCAO (CD_TIPO_EVOLUCAO),
  CONSTRAINT WLREGIT_WLREWO_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.WL_REGRA_WORKLIST (NR_SEQUENCIA));

GRANT SELECT ON TASY.WL_REGRA_ITEM TO NIVEL_1;

