ALTER TABLE TASY.PLS_MATERIAL_A900
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MATERIAL_A900 CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MATERIAL_A900
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_MATERIAL         NUMBER(10)            NOT NULL,
  CD_MATERIAL_A900        VARCHAR2(30 BYTE)     NOT NULL,
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  DT_FIM_VIGENCIA         DATE,
  QT_CONVERSAO_A900       NUMBER(13,4),
  NR_SEQ_MATERIAL_UNIMED  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSA900_I1 ON TASY.PLS_MATERIAL_A900
(CD_MATERIAL_A900)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSA900_PK ON TASY.PLS_MATERIAL_A900
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSA900_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSA900_PLSMAT_FK_I ON TASY.PLS_MATERIAL_A900
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSA900_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSA900_PLSMAUN_FK_I ON TASY.PLS_MATERIAL_A900
(NR_SEQ_MATERIAL_UNIMED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_material_a900_atual
before update or insert ON TASY.PLS_MATERIAL_A900 for each row
declare
qt_registro_w		pls_integer := 0;

pragma autonomous_transaction;

begin
select	count(1)
into	qt_registro_w
from	dual
where	exists (select	1
		from	pls_material_a900 x
		where	nvl(x.dt_inicio_vigencia, :new.dt_inicio_vigencia) <= :new.dt_inicio_vigencia
		and	nvl(x.dt_fim_vigencia, :new.dt_inicio_vigencia) >= :new.dt_inicio_vigencia
		and	x.nr_seq_material = :new.nr_seq_material
		and	x.nr_sequencia	  <> :new.nr_sequencia);

if	(nvl(qt_registro_w,0) > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(285105);
end if;

if	(:new.nr_seq_material_unimed is null) or
	(:old.cd_material_a900 <> :new.cd_material_a900) then
	:new.nr_seq_material_unimed := pls_obter_dados_material_a900(:new.cd_material_a900,null,'NR');
end if;

end;
/


ALTER TABLE TASY.PLS_MATERIAL_A900 ADD (
  CONSTRAINT PLSA900_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MATERIAL_A900 ADD (
  CONSTRAINT PLSA900_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSA900_PLSMAUN_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL_UNIMED) 
 REFERENCES TASY.PLS_MATERIAL_UNIMED (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MATERIAL_A900 TO NIVEL_1;

