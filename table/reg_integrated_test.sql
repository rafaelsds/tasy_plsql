ALTER TABLE TASY.REG_INTEGRATED_TEST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_INTEGRATED_TEST CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_INTEGRATED_TEST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INTENCAO_USO  NUMBER(10)               NOT NULL,
  DS_TITLE             VARCHAR2(255 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  CD_TEST_ID           VARCHAR2(35 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGINTETES_PK ON TASY.REG_INTEGRATED_TEST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGINTETES_REGINU_FK_I ON TASY.REG_INTEGRATED_TEST
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_integrated_test_update
before insert ON TASY.REG_INTEGRATED_TEST 
for each row
declare
ds_prefixo_w           varchar2(15);
nr_sequencia_w         number(15);
nr_seq_it_w            number(10);


begin

if (:old.cd_test_id is null) then

    select ds_prefixo
      into ds_prefixo_w
      from reg_intencao_uso
     where nr_sequencia = :new.nr_seq_intencao_uso;

    select max(a.nr_sequencia)
      into nr_seq_it_w
      from reg_integrated_test a
     where a.nr_seq_intencao_uso = :new.nr_seq_intencao_uso;

    if nr_seq_it_w is not null then

      select nvl(obter_somente_numero(cd_test_id), 0) + 1
        into nr_sequencia_w
        from reg_integrated_test
       where nr_sequencia = nr_seq_it_w;

    end if;

    :new.cd_test_id := nvl(ds_prefixo_w, :new.nr_seq_intencao_uso) || '_IT_' || nvl(nr_sequencia_w, 1);

end if;

end;
/


ALTER TABLE TASY.REG_INTEGRATED_TEST ADD (
  CONSTRAINT REGINTETES_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REG_INTEGRATED_TEST ADD (
  CONSTRAINT REGINTETES_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_INTEGRATED_TEST TO NIVEL_1;

