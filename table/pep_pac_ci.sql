ALTER TABLE TASY.PEP_PAC_CI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEP_PAC_CI CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEP_PAC_CI
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE              NOT NULL,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE) NOT NULL,
  CD_PROFISSIONAL             VARCHAR2(10 BYTE) NOT NULL,
  DS_TITULO                   VARCHAR2(255 BYTE) NOT NULL,
  DS_TEXTO                    LONG,
  NR_ATENDIMENTO              NUMBER(10),
  DT_LIBERACAO                DATE,
  NR_SEQ_AGENDA               NUMBER(10),
  NR_SEQ_AVAL_PRE             NUMBER(10),
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA            VARCHAR2(255 BYTE),
  NR_SEQ_AGE_CONS             NUMBER(10),
  CD_PERFIL_ATIVO             NUMBER(5),
  NR_SEQ_REG_ELEMENTO         NUMBER(10),
  IE_TIPO_CONSENTIMENTO       VARCHAR2(3 BYTE),
  NR_SEQ_TEXTO                NUMBER(10),
  NR_SEQ_PROC_INTERNO         NUMBER(10),
  IE_LADO                     VARCHAR2(1 BYTE),
  CD_PROTOCOLO                NUMBER(10),
  NR_SEQ_MEDICACAO            NUMBER(6),
  NR_CICLO                    NUMBER(3),
  DS_DIA_CICLO                VARCHAR2(5 BYTE),
  NR_SEQ_ASSINATURA           NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO   NUMBER(10),
  NR_PRESCRICAO               NUMBER(15),
  IE_RN                       VARCHAR2(1 BYTE),
  DS_UTC                      VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO            VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO          VARCHAR2(50 BYTE),
  IE_CONTROLA_ALTA            VARCHAR2(2 BYTE),
  NR_SEQ_MOTIVO_CANCEL        NUMBER(10),
  DT_CRIACAO                  DATE,
  NR_SEQ_ATEND_CONS_PEPA      NUMBER(10),
  IE_ACAO                     VARCHAR2(1 BYTE),
  IE_STATUS                   VARCHAR2(1 BYTE),
  DS_JUST_RECUSA              VARCHAR2(255 BYTE),
  NM_USUARIO_STATUS           VARCHAR2(15 BYTE),
  DT_ACEITE_RECUSA            DATE,
  NR_CIRURGIA                 NUMBER(10),
  IE_TIPO_TERMO_CIR           VARCHAR2(1 BYTE),
  DT_SCHEDULE_PROC            DATE,
  NR_SEQ_PROC_CPOE            NUMBER(10),
  DT_EXPLANATION_START        DATE,
  DT_EXPLANATION_END          DATE,
  IE_EXPLAIN_PATIENT          VARCHAR2(1 BYTE),
  IE_EXPLAIN_OTHER_PERSON     VARCHAR2(1 BYTE),
  NR_SEQ_RELAT_RECEIVED_EXPL  NUMBER(10),
  NR_SEQ_NAIS_INSURANCE       NUMBER(10),
  IE_NIVEL_ATENCAO            VARCHAR2(1 BYTE),
  CD_EVOLUCAO                 NUMBER(10),
  IE_PERMITE_ALTERAR_TEXTO    VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE_MED        NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPPACI_AGECONS_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_AGE_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_AGECONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_AGEPACI_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_ATCONSPEPA_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_ATEPACI_FK_I ON TASY.PEP_PAC_CI
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_AVAPRAN_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_AVAL_PRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_AVAPRAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_CIRURGI_FK_I ON TASY.PEP_PAC_CI
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_EHRREEL_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_ESTABEL_FK_I ON TASY.PEP_PAC_CI
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_EVOPACI_FK_I ON TASY.PEP_PAC_CI
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_GRAUPA_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_RELAT_RECEIVED_EXPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_I1 ON TASY.PEP_PAC_CI
(DT_LIBERACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_I1
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_MOTCANPA_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_NAISINS_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_NAIS_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_PERFIL_FK_I ON TASY.PEP_PAC_CI
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_PESFISI_FK_I ON TASY.PEP_PAC_CI
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_PESFISI_FK2_I ON TASY.PEP_PAC_CI
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPPACI_PK ON TASY.PEP_PAC_CI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_PRESMED_FK_I ON TASY.PEP_PAC_CI
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPPACI_PROINTE_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_PROMEDI_FK_I ON TASY.PEP_PAC_CI
(CD_PROTOCOLO, NR_SEQ_MEDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_PROMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_PROTOCO_FK_I ON TASY.PEP_PAC_CI
(CD_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_PROTOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_TASASDI_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_TASASDI_FK2_I ON TASY.PEP_PAC_CI
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PEPPACI_TEXPADR_FK_I ON TASY.PEP_PAC_CI
(NR_SEQ_TEXTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPPACI_TEXPADR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pep_pac_ci_pend_atual
after insert or update or delete ON TASY.PEP_PAC_CI for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

qt_reg_w	               number(1);
ie_tipo_w		            varchar2(10);
ie_lib_consentimento_w	varchar2(10);
nr_atendimento_w  atendimento_paciente.nr_atendimento%type;
cd_convenio_w  Atend_Categoria_Convenio.cd_convenio%type;
cd_medico_resp_w varchar2(10);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

	if	(inserting) or
		(updating) then
		select	max(IE_LIB_CONSENTIMENTO)
		into	ie_lib_consentimento_w
		from	parametro_medico
		where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

		if	(nvl(ie_lib_consentimento_w,'N') = 'S') then
			if	(:new.dt_liberacao is null) then
				ie_tipo_w := 'CNL';
			elsif	(:old.dt_liberacao is null) and
		       (:new.dt_liberacao is not null) then
		       ie_tipo_w := 'XCNL';
			end if;

			begin
			if	(ie_tipo_w	is not null) then
				Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, :new.nr_atendimento, :new.nm_usuario);
			end if;
			exception
				when others then
				null;
			end;

			if (:new.ie_tipo_consentimento = 'L') and
			   (:old.dt_liberacao is null) and
		   (:new.dt_liberacao is not null) then

			begin

			select max(a.nr_atendimento),
				   max(cd_medico_resp)
			into   nr_atendimento_w,
				   cd_medico_resp_w
			from   atendimento_paciente a,
				   atend_categoria_convenio b,
				   conversao_meio_externo c
			where  a.nr_atendimento = b.nr_atendimento
			and    upper(c.cd_externo) = 'RESUNIMED'
			and	   upper(c.nm_tabela) = 'ATEND_CATEGORIA_CONVENIO'
			and    c.cd_interno = to_char(b.cd_convenio)
			and    a.cd_pessoa_fisica = :new.cd_pessoa_fisica;

			select OBTER_CONVENIO_ATENDIMENTO(nr_atendimento_w)
			into cd_convenio_w
			from dual;

			--  Gerar RES UNIMED - 00790 - Ativacao do RES do beneficiario
		    gerar_transacao_res(nr_atendimento_w,'00790',obter_usuario_pessoa(cd_medico_resp_w),cd_convenio_w,'','',:new.nr_sequencia);

			exception
				when others then
				null;
			end;
		end if;

	end if;


	elsif	(deleting) then
            delete 	from pep_item_pendente
            where 	IE_TIPO_REGISTRO = 'CNL'
            and	      nr_seq_registro = :old.nr_sequencia
            and	      nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

            commit;
   end if;

if (:old.dt_inativacao is null and :new.dt_inativacao is not null and :new.cd_evolucao is not null) then
 update evolucao_paciente
	set dt_inativacao = sysdate,
	ie_situacao ='I',
	dt_atualizacao = sysdate ,
	nm_usuario = wheb_usuario_pck.get_nm_usuario,
	nm_usuario_inativacao = wheb_usuario_pck.get_nm_usuario,
	ds_justificativa = :new.ds_justificativa
	where cd_evolucao = :new.cd_evolucao;
	delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao
	and ie_med_rec_type ='CONSENT'
	and ie_stage = 1
	and ie_soap_type = 'P'
	and nr_seq_med_item = :new.nr_sequencia;
	end if;
	commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.pep_pac_ci_insert
before insert or update ON TASY.PEP_PAC_CI for each row
declare

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

	if	(:new.cd_pessoa_fisica is null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(189913);
	end if;

end if;

if	(inserting) then
	:new.ie_status := 'P';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PEP_PAC_CI_tp  after update ON TASY.PEP_PAC_CI FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_MOTIVO_CANCEL,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_CANCEL,1,4000),substr(:new.NR_SEQ_MOTIVO_CANCEL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_CANCEL',ie_log_w,ds_w,'PEP_PAC_CI',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROLA_ALTA,1,4000),substr(:new.IE_CONTROLA_ALTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROLA_ALTA',ie_log_w,ds_w,'PEP_PAC_CI',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PEP_PAC_CI_AFT_UPD
after update ON TASY.PEP_PAC_CI for each row
begin

if	((:old.DT_LIBERACAO is null and  :new.DT_LIBERACAO is not null) and (nvl(pkg_i18n.get_user_locale, 'pt_BR')='ja_JP')) then
	delete  from pep_item_pendente
	where   nr_seq_registro = :new.nr_atendimento
	and     ie_tipo_registro = 'HOSP';
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.PEP_PAC_CI_SBIS_DEL
before delete ON TASY.PEP_PAC_CI for each row
declare
nr_log_seq_w		number(10);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 nvl(obter_pessoa_atendimento(:old.nr_atendimento,'C'),:old.cd_pessoa_fisica),
									 obter_desc_expressao(285717),
									  :old.nr_atendimento,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));
end;
/


CREATE OR REPLACE TRIGGER TASY.ish_pep_pac_ci_after
after insert or update or delete ON TASY.PEP_PAC_CI for each row
declare

reg_integracao_w      gerar_int_padrao.reg_integracao;
nr_seq_episodio_w     atendimento_paciente.nr_seq_episodio%type;
cd_pessoa_fisica_w    atendimento_paciente.cd_pessoa_fisica%type;

begin

select	max(nr_seq_episodio),
	max(cd_pessoa_fisica)
into	nr_seq_episodio_w,
	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento  =  nvl(:new.nr_atendimento, :old.nr_atendimento);

reg_integracao_w.cd_pessoa_fisica	:=	cd_pessoa_fisica_w;
reg_integracao_w.nr_atendimento		:=	nvl(:new.nr_atendimento, :old.nr_atendimento);
reg_integracao_w.ie_operacao		:=	'A';

if	((get_case_encounter_type(null, null, reg_integracao_w.nr_atendimento, null) = '1') and
	(:old.dt_liberacao is null and :new.dt_liberacao is not null)) then
	gerar_int_padrao.gravar_integracao('106',nr_seq_episodio_w,nvl(:new.nm_usuario, :old.nm_usuario), reg_integracao_w);
end if;

end ish_pep_pac_ci_after;
/


CREATE OR REPLACE TRIGGER TASY.PEP_PAC_CI_ATUAL
before insert or update ON TASY.PEP_PAC_CI for each row
declare


begin
if	(nvl(:old.DT_ATUALIZACAO_NREC,sysdate+10) <> :new.DT_ATUALIZACAO_NREC) and
	(:new.DT_ATUALIZACAO_NREC is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ATUALIZACAO_NREC,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_PEP_PAC_CI
before insert or update ON TASY.PEP_PAC_CI for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PEP_PAC_CI_SBIS_IN
before insert or update ON TASY.PEP_PAC_CI for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  IF (INSERTING) THEN
    select 	log_alteracao_prontuario_seq.nextval
    into 	nr_log_seq_w
    from 	dual;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       obter_desc_expressao(656665) ,
                       wheb_usuario_pck.get_nm_maquina,
                       nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'),:new.cd_pessoa_fisica),
                       obter_desc_expressao(285717),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
  else
    ie_inativacao_w := 'N';

    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';
    end if;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
                       wheb_usuario_pck.get_nm_maquina,
                       nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'),:new.cd_pessoa_fisica),
                       obter_desc_expressao(285717),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
  end if;
end;
/


ALTER TABLE TASY.PEP_PAC_CI ADD (
  CONSTRAINT PEPPACI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PEP_PAC_CI ADD (
  CONSTRAINT PEPPACI_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT PEPPACI_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT PEPPACI_MOTCANPA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.MOTIVO_CANCEL_PROC_ALTA (NR_SEQUENCIA),
  CONSTRAINT PEPPACI_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PEPPACI_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT PEPPACI_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_RELAT_RECEIVED_EXPL) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT PEPPACI_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGE_CONS) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPPACI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PEPPACI_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPPACI_TEXPADR_FK 
 FOREIGN KEY (NR_SEQ_TEXTO) 
 REFERENCES TASY.TEXTO_PADRAO (NR_SEQUENCIA),
  CONSTRAINT PEPPACI_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PEPPACI_PROMEDI_FK 
 FOREIGN KEY (CD_PROTOCOLO, NR_SEQ_MEDICACAO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (CD_PROTOCOLO,NR_SEQUENCIA),
  CONSTRAINT PEPPACI_PROTOCO_FK 
 FOREIGN KEY (CD_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO (CD_PROTOCOLO),
  CONSTRAINT PEPPACI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPPACI_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PEPPACI_NAISINS_FK 
 FOREIGN KEY (NR_SEQ_NAIS_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT PEPPACI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PEPPACI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPPACI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PEPPACI_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEPPACI_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPPACI_AVAPRAN_FK 
 FOREIGN KEY (NR_SEQ_AVAL_PRE) 
 REFERENCES TASY.AVAL_PRE_ANESTESICA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PEP_PAC_CI TO NIVEL_1;

