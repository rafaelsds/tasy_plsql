ALTER TABLE TASY.SUS_AIH_UNIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_AIH_UNIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_AIH_UNIF
(
  NR_AIH                  NUMBER(13)            NOT NULL,
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  IE_IDENTIFICACAO_AIH    VARCHAR2(2 BYTE)      NOT NULL,
  NR_PROXIMA_AIH          NUMBER(13),
  NR_ANTERIOR_AIH         NUMBER(13),
  DT_EMISSAO              DATE                  NOT NULL,
  IE_MUDANCA_PROC         VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO_SOLIC   NUMBER(15)            NOT NULL,
  IE_ORIGEM_PROC_SOLIC    NUMBER(10)            NOT NULL,
  CD_PROCEDIMENTO_REAL    NUMBER(15)            NOT NULL,
  IE_ORIGEM_PROC_REAL     NUMBER(10)            NOT NULL,
  CD_MEDICO_SOLIC         VARCHAR2(10 BYTE)     NOT NULL,
  CD_CID_PRINCIPAL        VARCHAR2(10 BYTE)     NOT NULL,
  CD_CID_SECUNDARIO       VARCHAR2(10 BYTE),
  CD_CID_CAUSA_COMPL      VARCHAR2(10 BYTE),
  CD_CID_CAUSA_MORTE      VARCHAR2(10 BYTE),
  NR_INTERNO_CONTA        NUMBER(10),
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  CD_MEDICO_RESPONSAVEL   VARCHAR2(10 BYTE)     NOT NULL,
  CD_MODALIDADE           NUMBER(2)             NOT NULL,
  CD_MOTIVO_COBRANCA      NUMBER(2),
  CD_ESPECIALIDADE_AIH    NUMBER(2)             NOT NULL,
  IE_CODIGO_AUTORIZACAO   NUMBER(3),
  QT_NASCIDO_VIVO         NUMBER(1),
  QT_NASCIDO_MORTO        NUMBER(1),
  QT_SAIDA_ALTA           NUMBER(1),
  QT_SAIDA_TRANSFERENCIA  NUMBER(1),
  QT_SAIDA_OBITO          NUMBER(1),
  NR_GESTANTE_PRENATAL    NUMBER(12),
  CD_ORGAO_EMISSOR_AIH    VARCHAR2(10 BYTE)     NOT NULL,
  CD_CARATER_INTERNACAO   VARCHAR2(2 BYTE)      NOT NULL,
  DT_INICIAL              DATE,
  DT_FINAL                DATE,
  NR_PROC_INTERNO_SOLIC   NUMBER(10),
  NR_PROC_INTERNO_REAL    NUMBER(10),
  QT_PERMANENCIA_SUS      NUMBER(3),
  QT_PERMANENCIA_REAL     NUMBER(3),
  QT_LONGA_PERMANENCIA    NUMBER(3),
  VL_PONTO_SP             NUMBER(15,5),
  QT_PONTO_SP             NUMBER(17,2),
  VL_TOT_SP               NUMBER(15,2),
  CD_MEDICO_AUTORIZADOR   VARCHAR2(10 BYTE),
  IE_COMPLEXIDADE         VARCHAR2(2 BYTE),
  IE_TIPO_FINANCIAMENTO   VARCHAR2(4 BYTE),
  DT_IMPRESSAO            DATE,
  DS_JUSTIFICATIVA_CNS    VARCHAR2(255 BYTE),
  IE_CLASSIF_DIAG_SEQ     VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO2      VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO3      VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO4      VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO5      VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO6      VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO7      VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO8      VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO9      VARCHAR2(10 BYTE),
  IE_CLASSIF_DIAG_SEQ2    VARCHAR2(10 BYTE),
  IE_CLASSIF_DIAG_SEQ3    VARCHAR2(10 BYTE),
  IE_CLASSIF_DIAG_SEQ4    VARCHAR2(10 BYTE),
  IE_CLASSIF_DIAG_SEQ5    VARCHAR2(10 BYTE),
  IE_CLASSIF_DIAG_SEQ6    VARCHAR2(10 BYTE),
  IE_CLASSIF_DIAG_SEQ7    VARCHAR2(999 BYTE),
  IE_CLASSIF_DIAG_SEQ8    VARCHAR2(10 BYTE),
  IE_CLASSIF_DIAG_SEQ9    VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSAIUN_ATEPACI_FK_I ON TASY.SUS_AIH_UNIF
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK_I ON TASY.SUS_AIH_UNIF
(CD_CID_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK10_I ON TASY.SUS_AIH_UNIF
(CD_CID_SECUNDARIO7)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK11_I ON TASY.SUS_AIH_UNIF
(CD_CID_SECUNDARIO8)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK12_I ON TASY.SUS_AIH_UNIF
(CD_CID_SECUNDARIO9)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK2_I ON TASY.SUS_AIH_UNIF
(CD_CID_SECUNDARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_CIDDOEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK3_I ON TASY.SUS_AIH_UNIF
(CD_CID_CAUSA_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_CIDDOEN_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK4_I ON TASY.SUS_AIH_UNIF
(CD_CID_CAUSA_MORTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_CIDDOEN_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK5_I ON TASY.SUS_AIH_UNIF
(CD_CID_SECUNDARIO2)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK6_I ON TASY.SUS_AIH_UNIF
(CD_CID_SECUNDARIO3)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK7_I ON TASY.SUS_AIH_UNIF
(CD_CID_SECUNDARIO4)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK8_I ON TASY.SUS_AIH_UNIF
(CD_CID_SECUNDARIO5)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_CIDDOEN_FK9_I ON TASY.SUS_AIH_UNIF
(CD_CID_SECUNDARIO6)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_CONPACI_FK_I ON TASY.SUS_AIH_UNIF
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_ESTABEL_FK_I ON TASY.SUS_AIH_UNIF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_PESFISI_FK_I ON TASY.SUS_AIH_UNIF
(CD_MEDICO_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_PESFISI_FK2_I ON TASY.SUS_AIH_UNIF
(CD_MEDICO_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_PESFISI_FK3_I ON TASY.SUS_AIH_UNIF
(CD_MEDICO_AUTORIZADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SUSAIUN_PK ON TASY.SUS_AIH_UNIF
(NR_AIH, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSAIUN_PROCEDI_FK_I ON TASY.SUS_AIH_UNIF
(CD_PROCEDIMENTO_SOLIC, IE_ORIGEM_PROC_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_PROCEDI_FK2_I ON TASY.SUS_AIH_UNIF
(CD_PROCEDIMENTO_REAL, IE_ORIGEM_PROC_REAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_PROCEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_PROINTE_FK_I ON TASY.SUS_AIH_UNIF
(NR_PROC_INTERNO_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_PROINTE_FK2_I ON TASY.SUS_AIH_UNIF
(NR_PROC_INTERNO_REAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_PROINTE_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_SUSCAIN_FK_I ON TASY.SUS_AIH_UNIF
(CD_CARATER_INTERNACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_SUSCAIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_SUSESPE_FK_I ON TASY.SUS_AIH_UNIF
(CD_ESPECIALIDADE_AIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_SUSESPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_SUSMCOU_FK_I ON TASY.SUS_AIH_UNIF
(CD_MOTIVO_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_SUSMCOU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSAIUN_SUSMODA_FK_I ON TASY.SUS_AIH_UNIF
(CD_MODALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSAIUN_SUSMODA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sus_aih_unifafterup
after update ON TASY.SUS_AIH_UNIF for each row
declare

begin

if	(:new.nr_aih <> :old.nr_aih) then
	begin

	begin
	update	sus_laudo_paciente
	set	nr_aih	= :new.nr_aih,
		nm_usuario = :new.nm_usuario,
		dt_atualizacao = sysdate
	where	nr_aih	= :old.nr_aih
	and	nr_atendimento = :new.nr_atendimento;
	exception
		when no_data_found then
		null;
		end;

	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.sus_aih_unifafterpost
after insert or update ON TASY.SUS_AIH_UNIF for each row
declare


nr_sequencia_w		Number(10);


begin

if	(:new.nr_interno_conta <> :old.nr_interno_conta) or
	(:new.nr_interno_conta is null) then

	select	aih_hist_conta_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into aih_hist_conta(
		nr_sequencia,
		nr_aih,
		nr_seq_aih,
		nr_atendimento,
		nr_interno_conta,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_observacao)
	values(	nr_sequencia_w,
		:new.nr_aih,
		:new.nr_sequencia,
		:new.nr_atendimento,
		:new.nr_interno_conta,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		wheb_mensagem_pck.get_texto(310575));--substr('Alterado ou removido o v�nculo com conta paciente',1,2000));

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.sus_aih_unif_delete
before delete ON TASY.SUS_AIH_UNIF for each row
declare

nr_sequencia_w		number(10);

begin

Sus_Cancelar_Numeracao(:old.nr_aih,:old.nm_usuario);

select	aih_hist_conta_seq.nextval
into	nr_sequencia_w
from	dual;

insert into aih_hist_conta(
	nr_sequencia,
	nr_aih,
	nr_seq_aih,
	nr_atendimento,
	nr_interno_conta,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	ds_observacao)
values(	nr_sequencia_w,
	:old.nr_aih,
	:old.nr_sequencia,
	:old.nr_atendimento,
	:old.nr_interno_conta,
	sysdate,
	:old.nm_usuario,
	sysdate,
	:old.nm_usuario,
	wheb_mensagem_pck.get_texto(310585));--substr('Exclus�o',1,2000));

end;
/


CREATE OR REPLACE TRIGGER TASY.SUS_AIH_UNIFAtual
BEFORE INSERT OR UPDATE ON TASY.SUS_AIH_UNIF FOR EACH ROW
DECLARE

cd_convenio_w			number(5);
ie_tipo_financiamento_w		varchar2(4);
ie_complexidade_w		varchar2(2);
ie_dados_comp_conta_w		varchar2(15);
ie_codigo_autorizacao_w		number(15);
cd_motivo_cobranca_w		number(15);
cd_cid_causa_morte_w		varchar2(15);

BEGIN

cd_convenio_w	:= cd_convenio_w;
/* Felipe 14/03/2008 - OS 86154
if	(:new.nr_interno_conta is null) and
	(:old.nr_interno_conta is null) then

	select	max(a.cd_convenio)
	into	cd_convenio_w
	from	atend_categoria_convenio	a,
		convenio			c
	where	c.cd_convenio		= a.cd_convenio
	and	c.ie_tipo_convenio	= 3
	and	a.nr_atendimento	= :new.nr_atendimento;

	select	nvl(max(a.nr_interno_conta),null)
	into	:new.nr_interno_conta
	from 	conta_paciente	a
	where 	a.nr_atendimento 	= :new.nr_atendimento
	and	a.cd_convenio_parametro	= cd_convenio_w
	and	a.ie_status_acerto	= 1
	and	a.nr_interno_conta not in (select x.nr_interno_conta from sus_aih_unif x where a.nr_interno_conta = x.nr_interno_conta);
end if;
*/
begin
select	ie_tipo_financiamento,
	ie_complexidade
into	ie_tipo_financiamento_w,
	ie_complexidade_w
from	sus_procedimento
where	cd_procedimento = :new.cd_procedimento_real
and	ie_origem_proced = 7;
exception
	when others then
		null;
	end;

if	(:new.ie_tipo_financiamento is null) then
	:new.ie_tipo_financiamento	:= ie_tipo_financiamento_w;
end if;

if	(:new.ie_complexidade is null) then
	:new.ie_complexidade	:= ie_complexidade_w;
end if;

if	(:new.cd_procedimento_real	<> :old.cd_procedimento_real) then
	:new.ie_complexidade	:= ie_complexidade_w;
	:new.ie_tipo_financiamento	:= ie_tipo_financiamento_w;
end if;

ie_dados_comp_conta_w := nvl(Obter_Valor_Param_Usuario(1123,207, Obter_perfil_Ativo, wheb_usuario_pck.get_nm_usuario, 0),'N');

if	(nvl(ie_dados_comp_conta_w,'N') = 'S') then
	begin
	if	(nvl(:new.nr_interno_conta,0) <> 0) then
		begin

		begin
		select	ie_codigo_autorizacao,
			cd_motivo_cobranca,
			cd_cid_causa_morte
		into	ie_codigo_autorizacao_w,
			cd_motivo_cobranca_w,
			cd_cid_causa_morte_w
		from	sus_dados_aih_conta
		where	nr_interno_conta = :new.nr_interno_conta;
		exception
		when others then
			ie_codigo_autorizacao_w	:= :new.ie_codigo_autorizacao;
			cd_motivo_cobranca_w	:= :new.cd_motivo_cobranca;
			cd_cid_causa_morte_w	:= :new.cd_cid_causa_morte;
		end;

		:new.ie_codigo_autorizacao := ie_codigo_autorizacao_w;
		:new.cd_motivo_cobranca := cd_motivo_cobranca_w;
		:new.cd_cid_causa_morte := cd_cid_causa_morte_w;

		end;
	end if;
	end;
end if;

END;
/


ALTER TABLE TASY.SUS_AIH_UNIF ADD (
  CONSTRAINT SUSAIUN_PK
 PRIMARY KEY
 (NR_AIH, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          960K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_AIH_UNIF ADD (
  CONSTRAINT SUSAIUN_CIDDOEN_FK10 
 FOREIGN KEY (CD_CID_SECUNDARIO7) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK11 
 FOREIGN KEY (CD_CID_SECUNDARIO8) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK12 
 FOREIGN KEY (CD_CID_SECUNDARIO9) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK5 
 FOREIGN KEY (CD_CID_SECUNDARIO2) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK6 
 FOREIGN KEY (CD_CID_SECUNDARIO3) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK7 
 FOREIGN KEY (CD_CID_SECUNDARIO4) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK8 
 FOREIGN KEY (CD_CID_SECUNDARIO5) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK9 
 FOREIGN KEY (CD_CID_SECUNDARIO6) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SUSAIUN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT SUSAIUN_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA),
  CONSTRAINT SUSAIUN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SUSAIUN_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_SOLIC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SUSAIUN_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO_SOLIC, IE_ORIGEM_PROC_SOLIC) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT SUSAIUN_PROCEDI_FK2 
 FOREIGN KEY (CD_PROCEDIMENTO_REAL, IE_ORIGEM_PROC_REAL) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT SUSAIUN_SUSMODA_FK 
 FOREIGN KEY (CD_MODALIDADE) 
 REFERENCES TASY.SUS_MODALIDADE (CD_MODALIDADE),
  CONSTRAINT SUSAIUN_SUSESPE_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_AIH) 
 REFERENCES TASY.SUS_ESPECIALIDADE (CD_ESPECIALIDADE),
  CONSTRAINT SUSAIUN_SUSMCOU_FK 
 FOREIGN KEY (CD_MOTIVO_COBRANCA) 
 REFERENCES TASY.SUS_MOTIVO_COBR_UNIF (CD_MOTIVO_COBRANCA),
  CONSTRAINT SUSAIUN_SUSCAIN_FK 
 FOREIGN KEY (CD_CARATER_INTERNACAO) 
 REFERENCES TASY.SUS_CARATER_INTERNACAO (CD_CARATER_INTERNACAO),
  CONSTRAINT SUSAIUN_PROINTE_FK 
 FOREIGN KEY (NR_PROC_INTERNO_SOLIC) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT SUSAIUN_PROINTE_FK2 
 FOREIGN KEY (NR_PROC_INTERNO_REAL) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT SUSAIUN_PESFISI_FK3 
 FOREIGN KEY (CD_MEDICO_AUTORIZADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SUSAIUN_CIDDOEN_FK 
 FOREIGN KEY (CD_CID_PRINCIPAL) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK2 
 FOREIGN KEY (CD_CID_SECUNDARIO) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK3 
 FOREIGN KEY (CD_CID_CAUSA_COMPL) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT SUSAIUN_CIDDOEN_FK4 
 FOREIGN KEY (CD_CID_CAUSA_MORTE) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.SUS_AIH_UNIF TO NIVEL_1;

