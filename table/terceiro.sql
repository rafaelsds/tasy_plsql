ALTER TABLE TASY.TERCEIRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TERCEIRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TERCEIRO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_CGC                     VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  CD_CONVENIO                NUMBER(5),
  CD_CATEGORIA               VARCHAR2(10 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  CD_CONTA_CONTABIL          VARCHAR2(20 BYTE),
  IE_FORMA_PAGTO             VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  CD_CONTA_FINANC            NUMBER(10),
  IE_UTILIZACAO              VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_CENTRO_CUSTO            NUMBER(8),
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  IE_GERAR_TIT_REC           VARCHAR2(2 BYTE)   NOT NULL,
  DT_INICIO_VIGENCIA         DATE,
  DT_FIM_VIGENCIA            DATE,
  CD_EXTERNO                 VARCHAR2(40 BYTE),
  CD_CONTA_FINANC_CRE        NUMBER(10),
  NR_SEQ_TRANS_FIN_BAIXA     NUMBER(10),
  DS_EMAIL                   VARCHAR2(255 BYTE),
  NR_SEQ_TRANS_FIN_BAIXA_CR  NUMBER(10),
  CD_BARRAS                  VARCHAR2(40 BYTE),
  NR_SEQ_TRANS_FIN_TIT       NUMBER(10),
  VL_MAX_OPERACAO            NUMBER(15,2),
  IE_GERAR_OPER              VARCHAR2(1 BYTE),
  VL_PISO_REPASSE            NUMBER(15,2),
  CD_CONDICAO_PAGAMENTO      NUMBER(10),
  DS_PROCEDURE               VARCHAR2(255 BYTE),
  DS_TEXTO_EMAIL             VARCHAR2(4000 BYTE),
  CD_CONTA_CONTABIL_CR       VARCHAR2(20 BYTE),
  NR_SEQ_TRANS_ITEM_REP      NUMBER(10),
  NR_SEQ_TIPO_REP            NUMBER(10),
  IE_GERAR_CONTA_REP_PF      VARCHAR2(1 BYTE),
  NR_SEQ_TERC_UNIFIC         NUMBER(10),
  IE_GLOSA_POSTERIOR         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TERCEIR_CATCONV_FK_I ON TASY.TERCEIRO
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_CENCUST_FK_I ON TASY.TERCEIRO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_CONCONT_FK_I ON TASY.TERCEIRO
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_CONCONT_FK1_I ON TASY.TERCEIRO
(CD_CONTA_CONTABIL_CR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TERCEIR_CONFINA_FK_I ON TASY.TERCEIRO
(CD_CONTA_FINANC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_CONFINA_FK2_I ON TASY.TERCEIRO
(CD_CONTA_FINANC_CRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_CONFINA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_CONPAGA_FK_I ON TASY.TERCEIRO
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_ESTABEL_FK_I ON TASY.TERCEIRO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TERCEIR_PESFISI_FK_I ON TASY.TERCEIRO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TERCEIR_PESJURI_FK_I ON TASY.TERCEIRO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TERCEIR_PK ON TASY.TERCEIRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TERCEIR_TERCEIR_FK_I ON TASY.TERCEIRO
(NR_SEQ_TERC_UNIFIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TERCEIR_TIPOREP_FK_I ON TASY.TERCEIRO
(NR_SEQ_TIPO_REP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_TIPOREP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_TRAFINA_FK_I ON TASY.TERCEIRO
(NR_SEQ_TRANS_FIN_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_TRAFINA_FK2_I ON TASY.TERCEIRO
(NR_SEQ_TRANS_FIN_BAIXA_CR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_TRAFINA_FK3_I ON TASY.TERCEIRO
(NR_SEQ_TRANS_FIN_TIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_TRAFINA_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.TERCEIR_TRAFINA_FK4_I ON TASY.TERCEIRO
(NR_SEQ_TRANS_ITEM_REP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TERCEIR_TRAFINA_FK4_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.terceiro_atual
BEFORE INSERT or UPDATE ON TASY.TERCEIRO FOR EACH ROW
DECLARE
qt_reg_w				number(15,0);
cont_w					number(15,0);
ie_alt_terc_repasse_w	varchar2(255);
ds_conta_contabil_cr_w	varchar2(40);
ds_conta_contabil_cp_w	varchar2(40);
ds_erro_w				varchar2(2000);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.cd_cgc is not null) and (:new.cd_pessoa_fisica is not null) then
	--ra.ise_application_error(-20011, 'O terceiro s� pode ter uma pessoa f�sica ou uma pessoa jur�dica informada!' || chr(13) ||
	--				'N�o � poss�vel ter um terceiro com uma pessoa f�sica e uma pessoa jur�dica ao mesmo tempo.');
	wheb_mensagem_pck.exibir_mensagem_abort(176282);
end if;
if	(:new.cd_cgc is null) and (:new.cd_pessoa_fisica is null) then
	--r.aise_application_error(-20011, 'O terceiro deve ter uma pessoa f�sica ou uma pessoa jur�dica informada!');
	wheb_mensagem_pck.exibir_mensagem_abort(176283);

end if;

select	nvl(max(ie_alt_terc_repasse),'S')
into	ie_alt_terc_repasse_w
from	parametro_repasse
where	cd_estabelecimento		= :new.cd_estabelecimento;

if	(ie_alt_terc_repasse_w = 'N') and
	(
		(nvl(:new.cd_cgc, 'X') <> nvl(:old.cd_cgc,'X'))  or
		(nvl(:new.cd_pessoa_fisica, 'X') <> nvl(:old.cd_pessoa_fisica,'X'))
	)  then

	select	count(*)
	into	cont_w
	from	repasse_terceiro
	where	nr_seq_terceiro	= :new.nr_sequencia;

	if	(cont_w > 0) then
		--r.aise_application_error(-20011, 'Este terceiro j� possui repasses gerados! N�o � poss�vel alterar a pessoa dest terceiro');
		wheb_mensagem_pck.exibir_mensagem_abort(176287);
	end if;
end if;

ds_conta_contabil_cr_w		:= substr(wheb_mensagem_pck.get_texto(354099,''),1,40);
ds_conta_contabil_cp_w		:= substr(wheb_mensagem_pck.get_texto(354098,''),1,40);

ctb_consistir_conta_titulo(:new.cd_conta_contabil, ds_conta_contabil_cp_w);
ctb_consistir_conta_titulo(:new.cd_conta_contabil_cr, ds_conta_contabil_cr_w);

ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_contabil, ds_conta_contabil_cp_w);
ctb_consistir_conta_estab(:new.cd_estabelecimento, :new.cd_conta_contabil_cr, ds_conta_contabil_cr_w);
/*
con_consiste_vigencia_conta(:new.cd_conta_contabil, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;

con_consiste_vigencia_conta(:new.cd_conta_contabil_cr, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W=' || ds_erro_w);
end if;*/

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.TERCEIRO_tp  after update ON TASY.TERCEIRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_CGC,1,500);gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GERAR_CONTA_REP_PF,1,500);gravar_log_alteracao(substr(:old.IE_GERAR_CONTA_REP_PF,1,4000),substr(:new.IE_GERAR_CONTA_REP_PF,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_CONTA_REP_PF',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONVENIO,1,500);gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CATEGORIA,1,500);gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONTA_CONTABIL,1,500);gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_FORMA_PAGTO,1,500);gravar_log_alteracao(substr(:old.IE_FORMA_PAGTO,1,4000),substr(:new.IE_FORMA_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_PAGTO',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONTA_FINANC,1,500);gravar_log_alteracao(substr(:old.CD_CONTA_FINANC,1,4000),substr(:new.CD_CONTA_FINANC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_FINANC',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_UTILIZACAO,1,500);gravar_log_alteracao(substr(:old.IE_UTILIZACAO,1,4000),substr(:new.IE_UTILIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_UTILIZACAO',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CENTRO_CUSTO,1,500);gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO,1,4000),substr(:new.CD_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_FIM_VIGENCIA,1,500);gravar_log_alteracao(to_char(:old.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_INICIO_VIGENCIA,1,500);gravar_log_alteracao(to_char(:old.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONTA_FINANC_CRE,1,500);gravar_log_alteracao(substr(:old.CD_CONTA_FINANC_CRE,1,4000),substr(:new.CD_CONTA_FINANC_CRE,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_FINANC_CRE',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.VL_PISO_REPASSE,1,500);gravar_log_alteracao(substr(:old.VL_PISO_REPASSE,1,4000),substr(:new.VL_PISO_REPASSE,1,4000),:new.nm_usuario,nr_seq_w,'VL_PISO_REPASSE',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONDICAO_PAGAMENTO,1,500);gravar_log_alteracao(substr(:old.CD_CONDICAO_PAGAMENTO,1,4000),substr(:new.CD_CONDICAO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONDICAO_PAGAMENTO',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONTA_CONTABIL_CR,1,500);gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL_CR,1,4000),substr(:new.CD_CONTA_CONTABIL_CR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL_CR',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO_REP,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_REP,1,4000),substr(:new.NR_SEQ_TIPO_REP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_REP',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'TERCEIRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TERCEIRO ADD (
  CONSTRAINT TERCEIR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TERCEIRO ADD (
  CONSTRAINT TERCEIR_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_BAIXA_CR) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TERCEIR_TRAFINA_FK3 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_TIT) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TERCEIR_CONCONT_FK1 
 FOREIGN KEY (CD_CONTA_CONTABIL_CR) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TERCEIR_TERCEIR_FK 
 FOREIGN KEY (NR_SEQ_TERC_UNIFIC) 
 REFERENCES TASY.TERCEIRO (NR_SEQUENCIA),
  CONSTRAINT TERCEIR_TRAFINA_FK4 
 FOREIGN KEY (NR_SEQ_TRANS_ITEM_REP) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TERCEIR_TIPOREP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_REP) 
 REFERENCES TASY.TIPO_REPASSE (NR_SEQUENCIA),
  CONSTRAINT TERCEIR_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT TERCEIR_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT TERCEIR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TERCEIR_PESJURI_FK_I 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TERCEIR_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT TERCEIR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TERCEIR_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT TERCEIR_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TERCEIR_CONFINA_FK2 
 FOREIGN KEY (CD_CONTA_FINANC_CRE) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT TERCEIR_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_BAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TERCEIRO TO NIVEL_1;

