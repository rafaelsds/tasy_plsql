ALTER TABLE TASY.CONV_REGRA_PROC_GV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONV_REGRA_PROC_GV CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONV_REGRA_PROC_GV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_VAGA         VARCHAR2(15 BYTE)        NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  NR_SEQ_PROC_INTERNO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COREPRG_CONVENI_FK_I ON TASY.CONV_REGRA_PROC_GV
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREPRG_CONVENI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COREPRG_PK ON TASY.CONV_REGRA_PROC_GV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COREPRG_PROCEDI_FK_I ON TASY.CONV_REGRA_PROC_GV
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COREPRG_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COREPRG_PROINTE_FK_I ON TASY.CONV_REGRA_PROC_GV
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.conv_regra_proc_gv_atual
before insert or update ON TASY.CONV_REGRA_PROC_GV for each row
declare
qt_proc_inativo_w  number(1);
begin

begin
	select  1
	into	qt_proc_inativo_w
	from    procedimento
	where   cd_procedimento = :new.cd_procedimento
	and     ie_origem_proced = :new.ie_origem_proced
	and     ie_situacao      = 'I'
	and	rownum = 1;

exception
when others then
	qt_proc_inativo_w := 0;
end;
if (nvl(qt_proc_inativo_w,0) > 0) then
	--Procedimento inativo, verifique na fun��o Procedimentos!
	Wheb_mensagem_pck.exibir_mensagem_abort(456778);
end if;
begin
	select  1
	into	qt_proc_inativo_w
	from    proc_interno
	where   nr_sequencia = :new.nr_seq_proc_interno
	and     ie_situacao      = 'I';


exception
when others then
	qt_proc_inativo_w := 0;
end;

if (nvl(qt_proc_inativo_w,0) > 0) then
	--Procedimento interno inativo, verifique na fun��o Exames e Procedimentos Internos!
	Wheb_mensagem_pck.exibir_mensagem_abort(456780);
end if;

end;
/


ALTER TABLE TASY.CONV_REGRA_PROC_GV ADD (
  CONSTRAINT COREPRG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONV_REGRA_PROC_GV ADD (
  CONSTRAINT COREPRG_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT COREPRG_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT COREPRG_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.CONV_REGRA_PROC_GV TO NIVEL_1;

