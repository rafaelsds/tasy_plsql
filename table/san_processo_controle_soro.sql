ALTER TABLE TASY.SAN_PROCESSO_CONTROLE_SORO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_PROCESSO_CONTROLE_SORO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_PROCESSO_CONTROLE_SORO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DOACAO        NUMBER(10),
  NR_SEQ_TRANSFUSAO    NUMBER(10),
  DT_PROCESSO_SORO     DATE                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANPRSO_PK ON TASY.SAN_PROCESSO_CONTROLE_SORO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPRSO_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANPRSO_SANDOAC_FK_I ON TASY.SAN_PROCESSO_CONTROLE_SORO
(NR_SEQ_DOACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPRSO_SANDOAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANPRSO_SANTRAN_FK_I ON TASY.SAN_PROCESSO_CONTROLE_SORO
(NR_SEQ_TRANSFUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANPRSO_SANTRAN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SAN_PROCESSO_CONTROLE_SORO ADD (
  CONSTRAINT SANPRSO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_PROCESSO_CONTROLE_SORO ADD (
  CONSTRAINT SANPRSO_SANDOAC_FK 
 FOREIGN KEY (NR_SEQ_DOACAO) 
 REFERENCES TASY.SAN_DOACAO (NR_SEQUENCIA),
  CONSTRAINT SANPRSO_SANTRAN_FK 
 FOREIGN KEY (NR_SEQ_TRANSFUSAO) 
 REFERENCES TASY.SAN_TRANSFUSAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_PROCESSO_CONTROLE_SORO TO NIVEL_1;

