ALTER TABLE TASY.PLS_OC_CTA_FILTRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_FILTRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_FILTRO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_OC_CTA_COMB     NUMBER(10)             NOT NULL,
  NM_FILTRO              VARCHAR2(255 BYTE)     NOT NULL,
  NR_ORDEM_GERAL         NUMBER(5),
  IE_EXCECAO             VARCHAR2(1 BYTE)       NOT NULL,
  NR_ORDEM_CONTA         NUMBER(10),
  NR_ORDEM_PROC          NUMBER(10),
  NR_ORDEM_MAT           NUMBER(10),
  NR_ORDEM_BENEF         NUMBER(10),
  NR_ORDEM_PREST         NUMBER(10),
  NR_ORDEM_INTERC        NUMBER(10),
  NR_ORDEM_CONTRATO      NUMBER(10),
  NR_ORDEM_PRODUTO       NUMBER(10),
  NR_ORDEM_PROF          NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_CONTA        VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_PROC         VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_MAT          VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_BENEF        VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_PREST        VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_INTERC       VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_CONTRATO     VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_PRODUTO      VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_PROF         VARCHAR2(1 BYTE)       NOT NULL,
  IE_FILTRO_PROTOCOLO    VARCHAR2(1 BYTE)       NOT NULL,
  IE_VALIDA_TODO_ATEND   VARCHAR2(1 BYTE),
  IE_FILTRO_OPER_BENEF   VARCHAR2(1 BYTE)       NOT NULL,
  IE_VALIDA_CONTA_PRINC  VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOCCF_I1 ON TASY.PLS_OC_CTA_FILTRO
(NR_SEQ_OC_CTA_COMB, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSOCCF_PK ON TASY.PLS_OC_CTA_FILTRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCCF_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_FILTRO
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCCF_PLSOCCC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_FILTRO_tp  after update ON TASY.PLS_OC_CTA_FILTRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_FILTRO,1,500);gravar_log_alteracao(substr(:old.NM_FILTRO,1,4000),substr(:new.NM_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NM_FILTRO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PROTOCOLO,1,4000),substr(:new.IE_FILTRO_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PROTOCOLO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDA_CONTA_PRINC,1,4000),substr(:new.IE_VALIDA_CONTA_PRINC,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDA_CONTA_PRINC',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_GERAL,1,4000),substr(:new.NR_ORDEM_GERAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_GERAL',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXCECAO,1,4000),substr(:new.IE_EXCECAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXCECAO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_CONTA,1,4000),substr(:new.NR_ORDEM_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_CONTA',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_PROC,1,4000),substr(:new.NR_ORDEM_PROC,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_PROC',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_MAT,1,4000),substr(:new.NR_ORDEM_MAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_MAT',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_BENEF,1,4000),substr(:new.NR_ORDEM_BENEF,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_BENEF',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_PREST,1,4000),substr(:new.NR_ORDEM_PREST,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_PREST',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_INTERC,1,4000),substr(:new.NR_ORDEM_INTERC,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_INTERC',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_CONTRATO,1,4000),substr(:new.NR_ORDEM_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_CONTRATO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_PRODUTO,1,4000),substr(:new.NR_ORDEM_PRODUTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_PRODUTO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_PROF,1,4000),substr(:new.NR_ORDEM_PROF,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_PROF',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_CONTA,1,4000),substr(:new.IE_FILTRO_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_CONTA',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PROC,1,4000),substr(:new.IE_FILTRO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PROC',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_MAT,1,4000),substr(:new.IE_FILTRO_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_MAT',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_BENEF,1,4000),substr(:new.IE_FILTRO_BENEF,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_BENEF',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PREST,1,4000),substr(:new.IE_FILTRO_PREST,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PREST',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_INTERC,1,4000),substr(:new.IE_FILTRO_INTERC,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_INTERC',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_CONTRATO,1,4000),substr(:new.IE_FILTRO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_CONTRATO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PRODUTO,1,4000),substr(:new.IE_FILTRO_PRODUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PRODUTO',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PROF,1,4000),substr(:new.IE_FILTRO_PROF,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PROF',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDA_TODO_ATEND,1,4000),substr(:new.IE_VALIDA_TODO_ATEND,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDA_TODO_ATEND',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_OPER_BENEF,1,4000),substr(:new.IE_FILTRO_OPER_BENEF,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_OPER_BENEF',ie_log_w,ds_w,'PLS_OC_CTA_FILTRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_FILTRO ADD (
  CONSTRAINT PLSOCCF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_FILTRO ADD (
  CONSTRAINT PLSOCCF_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_FILTRO TO NIVEL_1;

