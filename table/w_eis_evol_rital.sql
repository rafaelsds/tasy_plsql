DROP TABLE TASY.W_EIS_EVOL_RITAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_EIS_EVOL_RITAL
(
  DS_DIMENSAO  VARCHAR2(200 BYTE),
  CD_DIMENSAO  VARCHAR2(40 BYTE),
  VL_DATA      NUMBER,
  DT_DATA      DATE,
  DS_MASCARA   CHAR(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_EIS_EVOL_RITAL TO NIVEL_1;

