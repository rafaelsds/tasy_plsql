ALTER TABLE TASY.MPREV_CAPTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_CAPTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_CAPTACAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  NR_SEQ_INDICACAO        NUMBER(10),
  NR_SEQ_DEMANDA_ESPONT   NUMBER(10),
  IE_ORIGEM               VARCHAR2(2 BYTE)      NOT NULL,
  IE_STATUS               VARCHAR2(2 BYTE),
  NR_SEQ_POP_ALVO_PESSOA  NUMBER(10),
  DT_INCLUSAO             DATE,
  NR_SEQ_FORMA_INGRESSO   NUMBER(10),
  NR_SEQ_BUSCA_EMP        NUMBER(10),
  NR_SEQ_EQUIPE           NUMBER(10),
  NR_SEQ_CAPTACAO_CANC    NUMBER(10),
  DS_OBS_CAPTACAO         VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRCAPT_MPRBUEM_FK_I ON TASY.MPREV_CAPTACAO
(NR_SEQ_BUSCA_EMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRCAPT_MPRDEES_FK_I ON TASY.MPREV_CAPTACAO
(NR_SEQ_DEMANDA_ESPONT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRCAPT_MPRDEES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRCAPT_MPREQUI_FK_I ON TASY.MPREV_CAPTACAO
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRCAPT_MPRFING_FK_I ON TASY.MPREV_CAPTACAO
(NR_SEQ_FORMA_INGRESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRCAPT_MPRINPA_FK_I ON TASY.MPREV_CAPTACAO
(NR_SEQ_INDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRCAPT_MPRINPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRCAPT_MPRPAPE_FK_I ON TASY.MPREV_CAPTACAO
(NR_SEQ_POP_ALVO_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRCAPT_MPRPAPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRCAPT_PESFISI_FK_I ON TASY.MPREV_CAPTACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRCAPT_PK ON TASY.MPREV_CAPTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MPREV_CAPTACAO_INSERT
after insert ON TASY.MPREV_CAPTACAO for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Incluir como participante em capta��o caso n�o existir na lista de participantes
ainda.
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

qt_registro_w	pls_integer;

begin

select	count(1)
into	qt_registro_w
from	mprev_participante a
where	a.cd_pessoa_fisica = :new.cd_pessoa_fisica;

if	(qt_registro_w = 0) then
	insert into mprev_participante
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nm_usuario_nrec,
		dt_atualizacao_nrec,
		cd_pessoa_fisica,
		ie_status,
		ie_situacao,
		ie_internacao,
		ie_cronico)
	values	(mprev_participante_seq.nextval,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.cd_pessoa_fisica,
		'1', /* Capta��o */
		'A', /* Ativo*/
		'N', /* N�o internado */
		'N' /* N�o cr�nico */);
end if;

end;
/


ALTER TABLE TASY.MPREV_CAPTACAO ADD (
  CONSTRAINT MPRCAPT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_CAPTACAO ADD (
  CONSTRAINT MPRCAPT_MPRBUEM_FK 
 FOREIGN KEY (NR_SEQ_BUSCA_EMP) 
 REFERENCES TASY.MPREV_BUSCA_EMPRESARIAL (NR_SEQUENCIA),
  CONSTRAINT MPRCAPT_MPRFING_FK 
 FOREIGN KEY (NR_SEQ_FORMA_INGRESSO) 
 REFERENCES TASY.MPREV_FORMA_INGRESSO (NR_SEQUENCIA),
  CONSTRAINT MPRCAPT_MPREQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.MPREV_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT MPRCAPT_MPRPAPE_FK 
 FOREIGN KEY (NR_SEQ_POP_ALVO_PESSOA) 
 REFERENCES TASY.MPREV_POP_ALVO_PESSOA (NR_SEQUENCIA),
  CONSTRAINT MPRCAPT_MPRDEES_FK 
 FOREIGN KEY (NR_SEQ_DEMANDA_ESPONT) 
 REFERENCES TASY.MPREV_DEMANDA_ESPONT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRCAPT_MPRINPA_FK 
 FOREIGN KEY (NR_SEQ_INDICACAO) 
 REFERENCES TASY.MPREV_INDICACAO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRCAPT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.MPREV_CAPTACAO TO NIVEL_1;

