ALTER TABLE TASY.TASY_AJUSTE_BASE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_AJUSTE_BASE CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_AJUSTE_BASE
(
  CD_AJUSTE_BASE         NUMBER(10)             NOT NULL,
  NM_TABELA              VARCHAR2(50 BYTE),
  IE_TIPO_OBJETO         VARCHAR2(15 BYTE),
  NM_OBJETO              VARCHAR2(50 BYTE),
  DS_SCRIPT              VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  IE_REGRA_TABELA        VARCHAR2(15 BYTE),
  IE_ACOMPANHAMENTO      VARCHAR2(1 BYTE),
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NM_USUARIO_LIBERACAO   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TAAJBAS_I1 ON TASY.TASY_AJUSTE_BASE
(NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAAJBAS_I2 ON TASY.TASY_AJUSTE_BASE
(NM_TABELA, NM_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TAAJBAS_PK ON TASY.TASY_AJUSTE_BASE
(CD_AJUSTE_BASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TASY_AJUSTE_BASE_tp  after update ON TASY.TASY_AJUSTE_BASE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_AJUSTE_BASE);  ds_c_w:=null; ds_w:=substr(:new.IE_ACOMPANHAMENTO,1,500);gravar_log_alteracao(substr(:old.IE_ACOMPANHAMENTO,1,4000),substr(:new.IE_ACOMPANHAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACOMPANHAMENTO',ie_log_w,ds_w,'TASY_AJUSTE_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_REGRA_TABELA,1,500);gravar_log_alteracao(substr(:old.IE_REGRA_TABELA,1,4000),substr(:new.IE_REGRA_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_TABELA',ie_log_w,ds_w,'TASY_AJUSTE_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_TABELA,1,500);gravar_log_alteracao(substr(:old.NM_TABELA,1,4000),substr(:new.NM_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'NM_TABELA',ie_log_w,ds_w,'TASY_AJUSTE_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_OBJETO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_OBJETO,1,4000),substr(:new.IE_TIPO_OBJETO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_OBJETO',ie_log_w,ds_w,'TASY_AJUSTE_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TASY_AJUSTE_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_SCRIPT,1,500);gravar_log_alteracao(substr(:old.DS_SCRIPT,1,4000),substr(:new.DS_SCRIPT,1,4000),:new.nm_usuario,nr_seq_w,'DS_SCRIPT',ie_log_w,ds_w,'TASY_AJUSTE_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TASY_AJUSTE_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_AJUSTE_BASE,1,500);gravar_log_alteracao(substr(:old.CD_AJUSTE_BASE,1,4000),substr(:new.CD_AJUSTE_BASE,1,4000),:new.nm_usuario,nr_seq_w,'CD_AJUSTE_BASE',ie_log_w,ds_w,'TASY_AJUSTE_BASE',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_OBJETO,1,500);gravar_log_alteracao(substr(:old.NM_OBJETO,1,4000),substr(:new.NM_OBJETO,1,4000),:new.nm_usuario,nr_seq_w,'NM_OBJETO',ie_log_w,ds_w,'TASY_AJUSTE_BASE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TASY_AJUSTE_BASE ADD (
  CONSTRAINT TAAJBAS_PK
 PRIMARY KEY
 (CD_AJUSTE_BASE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TASY_AJUSTE_BASE TO NIVEL_1;

