ALTER TABLE TASY.PLS_EVENTO_REGRA_FIXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EVENTO_REGRA_FIXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EVENTO_REGRA_FIXO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_EVENTO             NUMBER(10)          NOT NULL,
  DT_INICIO_VIGENCIA        DATE                NOT NULL,
  DT_FIM_VIGENCIA           DATE,
  NR_SEQ_PRESTADOR          NUMBER(10),
  IE_FORMA_INCIDENCIA       VARCHAR2(10 BYTE)   NOT NULL,
  VL_REGRA                  NUMBER(15,2),
  IE_COOPERADO              VARCHAR2(1 BYTE),
  DT_MES_REFERENCIA         DATE,
  IE_FORMA_VALOR            VARCHAR2(3 BYTE),
  NR_SEQ_TIPO_PRESTADOR     NUMBER(10),
  IE_SITUACAO_PREST         VARCHAR2(1 BYTE),
  NR_SEQ_SIT_COOP           NUMBER(10),
  NR_SEQ_CLASSE_TITULO      NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(2000 BYTE),
  NR_SEQ_CLASSE_TIT_PAGAR   NUMBER(10),
  NR_SEQ_TRANS_FIN_BAIXA    NUMBER(10),
  NR_SEQ_TRANS_FIN_CONTAB   NUMBER(10),
  CD_CONDICAO_PAGAMENTO     NUMBER(10),
  NR_SEQ_PERIODO            NUMBER(10),
  QT_DIA_VENC               NUMBER(2),
  NR_SEQ_ORDEM              NUMBER(10),
  IE_PRESTADOR_MATRIZ       VARCHAR2(1 BYTE),
  DT_MOVIMENTO              DATE,
  IE_TITULO_PAGAR           VARCHAR2(1 BYTE),
  CD_PF_TITULO_PAGAR        VARCHAR2(10 BYTE),
  NR_SEQ_PREST_ORIG         NUMBER(10),
  CD_CENTRO_CUSTO           NUMBER(8),
  IE_ORDENACAO_SUGERIDA     VARCHAR2(1 BYTE),
  NR_ORDEM_EXECUCAO         NUMBER(20),
  IE_GERAR_APOS_TRIBUTACAO  VARCHAR2(1 BYTE),
  CD_CGC_TITULO_PAGAR       VARCHAR2(14 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSEVRF_CENCUST_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEVRF_CLATIRE_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_CLASSE_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_CLATIRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVRF_CLTITPA_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_CLASSE_TIT_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_CLTITPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVRF_PESFISI_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(CD_PF_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEVRF_PESJURI_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(CD_CGC_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSEVRF_PK ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVRF_PLSEVEN_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_PLSEVEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVRF_PLSPEPA_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_PLSPEPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVRF_PLSPRES_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVRF_PLSPRES_FK2_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_PREST_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSEVRF_PLSSICO_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_SIT_COOP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_PLSSICO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVRF_PLSTIPR_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_PLSTIPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVRF_TRAFINA_FK_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_TRANS_FIN_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEVRF_TRAFINA_FK2_I ON TASY.PLS_EVENTO_REGRA_FIXO
(NR_SEQ_TRANS_FIN_CONTAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEVRF_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_evento_regra_fixo_atual
before insert or update ON TASY.PLS_EVENTO_REGRA_FIXO for each row
declare

nr_seq_lote_w	pls_evento_movimento.nr_seq_lote%type;

begin

if	(nvl(:new.ie_forma_valor,'I') = 'I') and
	(:new.vl_regra is null) then
	-- Este lan�amento, deve possuir um valor informado.
	wheb_mensagem_pck.exibir_mensagem_abort(266792);
end if;

select	nvl(max(nr_seq_lote),0)
into	nr_seq_lote_w
from	pls_evento_movimento
where	nr_seq_regra_fixo	= :new.nr_sequencia
and	ie_cancelamento is null;

if	(nr_seq_lote_w > 0) and
	(:new.ie_forma_incidencia = 'U') then
	--N�o � poss�vel alterar este lan�amento!
	--Este lan�amento �nico j� est� gerado no lote de ocorr�ncias financeiras #@NR_SEQ_LOTE#@.
	wheb_mensagem_pck.exibir_mensagem_abort(296438, 'NR_SEQ_LOTE=' || nr_seq_lote_w);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_EVENTO_REGRA_FIXO_tp  after update ON TASY.PLS_EVENTO_REGRA_FIXO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_PREST_ORIG,1,4000),substr(:new.NR_SEQ_PREST_ORIG,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PREST_ORIG',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_MOVIMENTO,1,4000),substr(:new.DT_MOVIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_MOVIMENTO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PF_TITULO_PAGAR,1,4000),substr(:new.CD_PF_TITULO_PAGAR,1,4000),:new.nm_usuario,nr_seq_w,'CD_PF_TITULO_PAGAR',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO,1,4000),substr(:new.CD_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORDENACAO_SUGERIDA,1,4000),substr(:new.IE_ORDENACAO_SUGERIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO_SUGERIDA',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_EXECUCAO,1,4000),substr(:new.NR_ORDEM_EXECUCAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_EXECUCAO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_EVENTO,1,4000),substr(:new.NR_SEQ_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EVENTO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_INCIDENCIA,1,4000),substr(:new.IE_FORMA_INCIDENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_INCIDENCIA',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_REGRA,1,4000),substr(:new.VL_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'VL_REGRA',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COOPERADO,1,4000),substr(:new.IE_COOPERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COOPERADO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_MES_REFERENCIA,1,4000),substr(:new.DT_MES_REFERENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_MES_REFERENCIA',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_VALOR,1,4000),substr(:new.IE_FORMA_VALOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_VALOR',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO_PREST,1,4000),substr(:new.IE_SITUACAO_PREST,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO_PREST',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_SIT_COOP,1,4000),substr(:new.NR_SEQ_SIT_COOP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SIT_COOP',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSE_TITULO,1,4000),substr(:new.NR_SEQ_CLASSE_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSE_TITULO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSE_TIT_PAGAR,1,4000),substr(:new.NR_SEQ_CLASSE_TIT_PAGAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSE_TIT_PAGAR',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_FIN_BAIXA,1,4000),substr(:new.NR_SEQ_TRANS_FIN_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_FIN_BAIXA',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_FIN_CONTAB,1,4000),substr(:new.NR_SEQ_TRANS_FIN_CONTAB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_FIN_CONTAB',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONDICAO_PAGAMENTO,1,4000),substr(:new.CD_CONDICAO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONDICAO_PAGAMENTO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PERIODO,1,4000),substr(:new.NR_SEQ_PERIODO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PERIODO',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ORDEM,1,4000),substr(:new.NR_SEQ_ORDEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ORDEM',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_VENC,1,4000),substr(:new.QT_DIA_VENC,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_VENC',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRESTADOR_MATRIZ,1,4000),substr(:new.IE_PRESTADOR_MATRIZ,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESTADOR_MATRIZ',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TITULO_PAGAR,1,4000),substr(:new.IE_TITULO_PAGAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TITULO_PAGAR',ie_log_w,ds_w,'PLS_EVENTO_REGRA_FIXO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_EVENTO_REGRA_FIXO ADD (
  CONSTRAINT PLSEVRF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_EVENTO_REGRA_FIXO ADD (
  CONSTRAINT PLSEVRF_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_BAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSEVRF_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_CONTAB) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSEVRF_PESFISI_FK 
 FOREIGN KEY (CD_PF_TITULO_PAGAR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSEVRF_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PREST_ORIG) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSEVRF_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT PLSEVRF_PESJURI_FK 
 FOREIGN KEY (CD_CGC_TITULO_PAGAR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSEVRF_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PLSEVRF_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSEVRF_CLATIRE_FK 
 FOREIGN KEY (NR_SEQ_CLASSE_TITULO) 
 REFERENCES TASY.CLASSE_TITULO_RECEBER (NR_SEQUENCIA),
  CONSTRAINT PLSEVRF_CLTITPA_FK 
 FOREIGN KEY (NR_SEQ_CLASSE_TIT_PAGAR) 
 REFERENCES TASY.CLASSE_TITULO_PAGAR (NR_SEQUENCIA),
  CONSTRAINT PLSEVRF_PLSSICO_FK 
 FOREIGN KEY (NR_SEQ_SIT_COOP) 
 REFERENCES TASY.PLS_SITUACAO_COOPERADO (NR_SEQUENCIA),
  CONSTRAINT PLSEVRF_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSEVRF_PLSPEPA_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.PLS_PERIODO_PAGAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_EVENTO_REGRA_FIXO TO NIVEL_1;

