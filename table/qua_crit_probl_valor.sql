ALTER TABLE TASY.QUA_CRIT_PROBL_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_CRIT_PROBL_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_CRIT_PROBL_VALOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CRITERIO      NUMBER(10)               NOT NULL,
  VL_VALOR             NUMBER(5)                NOT NULL,
  DS_DESCRICAO         VARCHAR2(80 BYTE)        NOT NULL,
  CD_EXP_DESCRICAO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUACPVA_DICEXPR_FK_I ON TASY.QUA_CRIT_PROBL_VALOR
(CD_EXP_DESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.QUACPVA_PK ON TASY.QUA_CRIT_PROBL_VALOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUACPVA_QUACRPR_FK_I ON TASY.QUA_CRIT_PROBL_VALOR
(NR_SEQ_CRITERIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUACPVA_QUACRPR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.QUACPVA_UK ON TASY.QUA_CRIT_PROBL_VALOR
(NR_SEQ_CRITERIO, VL_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QUA_CRIT_PROBL_VALOR ADD (
  CONSTRAINT QUACPVA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT QUACPVA_UK
 UNIQUE (NR_SEQ_CRITERIO, VL_VALOR)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_CRIT_PROBL_VALOR ADD (
  CONSTRAINT QUACPVA_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_DESCRICAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT QUACPVA_QUACRPR_FK 
 FOREIGN KEY (NR_SEQ_CRITERIO) 
 REFERENCES TASY.QUA_CRIT_PROBLEMA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.QUA_CRIT_PROBL_VALOR TO NIVEL_1;

