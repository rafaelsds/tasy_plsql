ALTER TABLE TASY.ESCALA_MEWS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MEWS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MEWS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  QT_PA_SISTOLICA        NUMBER(3),
  QT_FREQ_CARDIACA       NUMBER(3),
  QT_FREQ_RESP           NUMBER(3),
  QT_TEMP                NUMBER(4,1),
  IE_NIVEL_CONSCIENCIA   VARCHAR2(15 BYTE),
  QT_PONTUACAO           NUMBER(3),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  QT_PA_DIASTOLICA       NUMBER(3),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCMEWS_ATEPACI_FK_I ON TASY.ESCALA_MEWS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMEWS_I1 ON TASY.ESCALA_MEWS
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMEWS_PESFISI_FK_I ON TASY.ESCALA_MEWS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCMEWS_PK ON TASY.ESCALA_MEWS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_MEWS_ATUAL
before insert or update ON TASY.ESCALA_MEWS for each row
declare
qt_pontuacao_w	number(10);

param1537		varchar2(1);
atributo		ITENS_ESCALA_MEWS.NM_ATRIBUTO%TYPE;
minimo			ITENS_ESCALA_MEWS.QT_MIN%TYPE;
maximo			ITENS_ESCALA_MEWS.QT_MAX%TYPE;
pontuacao		ITENS_ESCALA_MEWS.QT_SCORE%TYPE;
indice			ITENS_ESCALA_MEWS.IE_NIVEL_CONSCIENCIA%TYPE;

cursor c01 is
select NM_ATRIBUTO, QT_MIN, QT_MAX, QT_SCORE from ITENS_ESCALA_MEWS where NM_ATRIBUTO = 'QT_FREQ_CARDIACA';

cursor c02 is
select NM_ATRIBUTO, QT_MIN, QT_MAX, QT_SCORE from ITENS_ESCALA_MEWS where NM_ATRIBUTO = 'QT_FREQ_RESP';

cursor c03 is
select NM_ATRIBUTO, QT_MIN, QT_MAX, QT_SCORE from ITENS_ESCALA_MEWS where NM_ATRIBUTO = 'QT_PA_SISTOLICA';

cursor c04 is
select NM_ATRIBUTO, QT_MIN, QT_MAX, QT_SCORE from ITENS_ESCALA_MEWS where NM_ATRIBUTO = 'QT_TEMP';

cursor c05 is
select NM_ATRIBUTO, IE_NIVEL_CONSCIENCIA, QT_SCORE from ITENS_ESCALA_MEWS where NM_ATRIBUTO = 'IE_NIVEL_CONSCIENCIA';

begin

if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

qt_pontuacao_w	:= 0;

Obter_Param_Usuario(281, 1537, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario,obter_estabelecimento_ativo, param1537);

if (nvl(param1537, 'N') = 'N') then

if (:new.qt_freq_cardiaca is not null)  then
	if	(nvl(:new.qt_freq_cardiaca,0)	<= 40) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	elsif	(nvl(:new.qt_freq_cardiaca,0)	>= 41) and
		(:new.qt_freq_cardiaca	<= 50) then
		qt_pontuacao_w	:= qt_pontuacao_w + 1;
	elsif	(nvl(:new.qt_freq_cardiaca,0)	>= 51) and
		(:new.qt_freq_cardiaca	<= 100) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(nvl(:new.qt_freq_cardiaca,0)	>=101) and
		(:new.qt_freq_cardiaca	<= 110) then
		qt_pontuacao_w	:= qt_pontuacao_w + 1;
	elsif	(nvl(:new.qt_freq_cardiaca,0)	>=111) and
		(:new.qt_freq_cardiaca	<= 129) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	elsif	(nvl(:new.qt_freq_cardiaca,0)	>= 130) then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	end if;
end if;

if (:new.qt_freq_resp is not null) then
	if	(nvl(:new.qt_freq_resp,0)	< 9) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	elsif	(nvl(:new.qt_freq_resp,0)	>= 9) and
		(nvl(:new.qt_freq_resp,0)	<= 14) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(nvl(:new.qt_freq_resp,0)	>= 15) and
		(nvl(:new.qt_freq_resp,0)	<= 20) then
		qt_pontuacao_w	:= qt_pontuacao_w + 1;
	elsif	(nvl(:new.qt_freq_resp,0)	>= 21) and
		(nvl(:new.qt_freq_resp,0)	<= 29) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	elsif	(nvl(:new.qt_freq_resp,0)	>= 30) then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	end if;
end if;

if (:new.QT_PA_SISTOLICA is not null) then
	if	(nvl(:new.QT_PA_SISTOLICA,0)	< 71 ) then
		qt_pontuacao_w	:= qt_pontuacao_w + 3;
	elsif	(nvl(:new.QT_PA_SISTOLICA,0)	>=71) and
		(nvl(:new.QT_PA_SISTOLICA,0)	<= 80) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	elsif	(nvl(:new.QT_PA_SISTOLICA,0)	>=81) and
		(nvl(:new.QT_PA_SISTOLICA,0)	<= 100) then
		qt_pontuacao_w	:= qt_pontuacao_w + 1;
	elsif	(nvl(:new.QT_PA_SISTOLICA,0)	>=101) and
		(nvl(:new.QT_PA_SISTOLICA,0)	<= 199) then
		qt_pontuacao_w	:= qt_pontuacao_w + 0;
	elsif	(nvl(:new.QT_PA_SISTOLICA,0)	>=200) then
		qt_pontuacao_w	:= qt_pontuacao_w + 2;
	end if;
end if;

qt_pontuacao_w	:= qt_pontuacao_w + nvl(:new.IE_NIVEL_CONSCIENCIA,0);

if (:new.QT_TEMP is not null) then
	if	(nvl(:new.QT_TEMP,0)	< 35) then
		qt_pontuacao_w	:= qt_pontuacao_w	+ 2;
	elsif	(nvl(:new.QT_TEMP,0)	>=35) and
		(nvl(:new.QT_TEMP,0)	<= 38.4) then
		qt_pontuacao_w	:= qt_pontuacao_w	+ 0;
	elsif	(nvl(:new.QT_TEMP,0)	>=38.5) then
		qt_pontuacao_w	:= qt_pontuacao_w	+ 2;
	end if;
end if;

else
	if (:new.qt_freq_cardiaca is not null)  then
		open c01;
		loop
		fetch c01 into
			atributo,
			minimo,
			maximo,
			pontuacao;
		exit when C01%notfound;
			begin
				if	(minimo is not null and maximo is not null)  and (nvl(:new.qt_freq_cardiaca,0) >= minimo) and (nvl(:new.qt_freq_cardiaca,0) <= maximo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				elsif (minimo is null and maximo is not null)  and (nvl(:new.qt_freq_cardiaca,0) <= maximo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				elsif (minimo is not null and maximo is null)  and (nvl(:new.qt_freq_cardiaca,0) >= minimo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				end if;
			end;
		end loop;
		close C01;
	end if;

	if (:new.qt_freq_resp is not null)  then
		open c02;
		loop
		fetch c02 into
			atributo,
			minimo,
			maximo,
			pontuacao;
		exit when c02%notfound;
			begin
				if	(minimo is not null and maximo is not null)  and (nvl(:new.qt_freq_resp,0) >= minimo) and (nvl(:new.qt_freq_resp,0) <= maximo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				elsif	(minimo is null and maximo is not null)  and (nvl(:new.qt_freq_resp,0) <= maximo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				elsif	(minimo is not null and maximo is null)  and (nvl(:new.qt_freq_resp,0) >= minimo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				end if;
			end;
		end loop;
		close c02;
	end if;

	if (:new.QT_PA_SISTOLICA is not null)  then
		open c03;
		loop
		fetch c03 into
			atributo,
			minimo,
			maximo,
			pontuacao;
		exit when c03%notfound;
			begin
				if	(minimo is not null and maximo is not null)  and (nvl(:new.QT_PA_SISTOLICA,0) >= minimo) and (nvl(:new.QT_PA_SISTOLICA,0)	<= maximo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				elsif	(minimo is null and maximo is not null)  and (nvl(:new.QT_PA_SISTOLICA,0) <= maximo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				elsif	(minimo is not null and maximo is null)  and (nvl(:new.QT_PA_SISTOLICA,0) >= minimo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				end if;
			end;
		end loop;
		close c03;
	end if;

	if (:new.QT_TEMP is not null)  then
		open c04;
		loop
		fetch c04 into
			atributo,
			minimo,
			maximo,
			pontuacao;
		exit when c04%notfound;
			begin
				if	(minimo is not null and maximo is not null)  and (nvl(:new.QT_TEMP,0) >= minimo) and (nvl(:new.QT_TEMP,0) <= maximo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				elsif	(minimo is null and maximo is not null)  and (nvl(:new.QT_TEMP,0) <= maximo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				elsif	(minimo is not null and maximo is null)  and (nvl(:new.QT_TEMP,0) >= minimo) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				end if;
			end;
		end loop;
		close c04;
	end if;

	if (:new.IE_NIVEL_CONSCIENCIA is not null)  then
		open c05;
		loop
		fetch c05 into
			atributo,
			indice,
			pontuacao;
		exit when c05%notfound;
			begin
				if	(indice is not null)  and (nvl(:new.IE_NIVEL_CONSCIENCIA,0) = indice) then
					qt_pontuacao_w	:= qt_pontuacao_w + pontuacao;
				end if;
			end;
		end loop;
		close c05;
	end if;
end if;

:new.qt_pontuacao	:= qt_pontuacao_w;
end;
/


ALTER TABLE TASY.ESCALA_MEWS ADD (
  CONSTRAINT ESCMEWS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MEWS ADD (
  CONSTRAINT ESCMEWS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCMEWS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_MEWS TO NIVEL_1;

