ALTER TABLE TASY.MAN_ORIGEM_DANO_PLANEJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORIGEM_DANO_PLANEJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORIGEM_DANO_PLANEJ
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_ORIGEM_DANO         NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO_PLANEJAMENTO  NUMBER(10)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANODPL_MANGRPL_FK_I ON TASY.MAN_ORIGEM_DANO_PLANEJ
(NR_SEQ_GRUPO_PLANEJAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANODPL_MANGRPL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANODPL_MANORDA_FK_I ON TASY.MAN_ORIGEM_DANO_PLANEJ
(NR_SEQ_ORIGEM_DANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANODPL_MANORDA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANODPL_PK ON TASY.MAN_ORIGEM_DANO_PLANEJ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANODPL_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_ORIGEM_DANO_PLANEJ ADD (
  CONSTRAINT MANODPL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORIGEM_DANO_PLANEJ ADD (
  CONSTRAINT MANODPL_MANORDA_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM_DANO) 
 REFERENCES TASY.MAN_ORIGEM_DANO (NR_SEQUENCIA),
  CONSTRAINT MANODPL_MANGRPL_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PLANEJAMENTO) 
 REFERENCES TASY.MAN_GRUPO_PLANEJAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORIGEM_DANO_PLANEJ TO NIVEL_1;

