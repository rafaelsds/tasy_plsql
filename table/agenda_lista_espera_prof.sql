ALTER TABLE TASY.AGENDA_LISTA_ESPERA_PROF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_LISTA_ESPERA_PROF CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_LISTA_ESPERA_PROF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_PROFISSIONAL      VARCHAR2(10 BYTE),
  NR_SEQ_EQUIPE        NUMBER(10),
  CD_FUNCAO            NUMBER(3),
  IE_INDICACAO         VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LISTA         NUMBER(10)               NOT NULL,
  CD_ESPECIALIDADE     NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGELIESPRO_AGLIESP_FK_I ON TASY.AGENDA_LISTA_ESPERA_PROF
(NR_SEQ_LISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGELIESPRO_ESPMEDI_FK_I ON TASY.AGENDA_LISTA_ESPERA_PROF
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGELIESPRO_FUNCAO_FK_I ON TASY.AGENDA_LISTA_ESPERA_PROF
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGELIESPRO_PESFISI_FK_I ON TASY.AGENDA_LISTA_ESPERA_PROF
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGELIESPRO_PFEQUIP_FK_I ON TASY.AGENDA_LISTA_ESPERA_PROF
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGELIESPRO_PK ON TASY.AGENDA_LISTA_ESPERA_PROF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGENDA_LISTA_ESPERA_PROF ADD (
  CONSTRAINT AGELIESPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.AGENDA_LISTA_ESPERA_PROF ADD (
  CONSTRAINT AGELIESPRO_AGLIESP_FK 
 FOREIGN KEY (NR_SEQ_LISTA) 
 REFERENCES TASY.AGENDA_LISTA_ESPERA (NR_SEQUENCIA),
  CONSTRAINT AGELIESPRO_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT AGELIESPRO_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO_MEDICO (CD_FUNCAO),
  CONSTRAINT AGELIESPRO_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGELIESPRO_PFEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.PF_EQUIPE (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_LISTA_ESPERA_PROF TO NIVEL_1;

