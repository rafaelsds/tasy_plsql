ALTER TABLE TASY.AGENDA_LISTA_ESPERA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_LISTA_ESPERA CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_LISTA_ESPERA
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO_NREC          DATE             NOT NULL,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  CD_AGENDA                    NUMBER(10),
  DT_AGENDAMENTO               DATE             NOT NULL,
  NM_USUARIO_AGENDA            VARCHAR2(15 BYTE) NOT NULL,
  DT_DESEJADA                  DATE,
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE),
  NM_PESSOA_LISTA              VARCHAR2(60 BYTE),
  NR_TELEFONE                  VARCHAR2(255 BYTE),
  NM_PESSOA_CONTATO            VARCHAR2(50 BYTE),
  CD_CONVENIO                  NUMBER(5),
  CD_CATEGORIA                 VARCHAR2(10 BYTE),
  DS_OBSERVACAO                VARCHAR2(2000 BYTE),
  IE_STATUS_ESPERA             VARCHAR2(1 BYTE),
  NR_SEQ_AGECONS               NUMBER(10),
  IE_URGENTE                   VARCHAR2(1 BYTE),
  NR_SEQ_AGENDA                NUMBER(10),
  IE_CLASSIF_AGENDA            VARCHAR2(5 BYTE),
  DT_CHEGADA                   DATE,
  DT_PERIODO_INICIAL           DATE,
  DT_PERIODO_FINAL             DATE,
  IE_FORMA_AGENDAMENTO         NUMBER(3),
  CD_SETOR_ATENDIMENTO         NUMBER(5),
  CD_MEDICO_EXEC               VARCHAR2(10 BYTE),
  IE_LADO                      VARCHAR2(1 BYTE),
  CD_EMPRESA_REF               NUMBER(10),
  NR_DOC_CONVENIO              VARCHAR2(20 BYTE),
  CD_PLANO                     VARCHAR2(10 BYTE),
  DT_VALIDADE_CARTEIRA         DATE,
  CD_USUARIO_CONVENIO          VARCHAR2(30 BYTE),
  CD_PROCEDENCIA               NUMBER(5),
  QT_PESO                      NUMBER(6,3),
  QT_ALTURA_CM                 NUMBER(5,2),
  NR_SEQ_CLASSIF_AGENDA        NUMBER(10),
  NR_SEQ_INDICACAO             NUMBER(10),
  NR_SEQ_PROC_INTERNO          NUMBER(10),
  CD_MEDICO                    VARCHAR2(10 BYTE),
  CD_PROCEDIMENTO              NUMBER(15),
  IE_ORIGEM_PROCED             NUMBER(10),
  CD_COMPLEMENTO               VARCHAR2(30 BYTE),
  QT_PROCEDIMENTO              NUMBER(5),
  NR_ACESSO_DICOM              NUMBER(15),
  CD_ESPECIALIDADE             NUMBER(5),
  NR_SEQ_MOTIVO_URG            NUMBER(10),
  DS_OBS_ADICIONAL             VARCHAR2(255 BYTE),
  NR_SEQ_JUSTIFICATIVA         NUMBER(10),
  CD_CID                       VARCHAR2(10 BYTE),
  DT_AGENDA_ORIGEM             DATE,
  DT_PREVISTA_INTERNACAO       DATE,
  IE_AUTORIZACAO               VARCHAR2(3 BYTE),
  CD_SENHA                     VARCHAR2(20 BYTE),
  NR_SEQ_TIPO_DEFICIENCIA      NUMBER(10),
  CD_CGC                       VARCHAR2(14 BYTE),
  DS_JUSTIFICATIVA_ESP_ANS     VARCHAR2(255 BYTE),
  CD_TIPO_AGENDA               NUMBER(10),
  NR_SEQ_OPM                   NUMBER(10),
  NR_DDI                       VARCHAR2(3 BYTE),
  CD_CIAP                      VARCHAR2(5 BYTE),
  CD_MATERIAL_EXAME            VARCHAR2(20 BYTE),
  NR_SEQ_EXAME                 NUMBER(10),
  DS_EXAME_SEM_CAD             VARCHAR2(255 BYTE),
  NR_SEQ_EXAME_LAB             NUMBER(10),
  NR_SEQ_PROGRAMA_SAUDE        NUMBER(10)       DEFAULT null,
  CD_PROFISSIONAL_SOL          VARCHAR2(10 BYTE),
  NR_SEQ_PEDIDO                NUMBER(10),
  NM_TABELA_PEDIDO             VARCHAR2(50 BYTE),
  IE_PRIORIDADE                NUMBER(3),
  NR_SEQ_AGENDA_INT            NUMBER(10),
  CD_TIPO_ANESTESIA            VARCHAR2(2 BYTE),
  NR_SEQ_TIPO_CLASSIF_PAC      NUMBER(10),
  NR_SEQ_SEGURADO              NUMBER(10),
  NR_SEQ_PARECE                NUMBER(10),
  NR_ATENDIMENTO               NUMBER(10),
  CD_DEPARTAMENTO_MEDICO       NUMBER(6),
  NR_SEQ_PRIORIDADE            NUMBER(10),
  NR_SEQ_ESTAGIO               NUMBER(10),
  CD_ESTABELECIMENTO           NUMBER(4),
  NR_SEQ_REGULACAO             NUMBER(10),
  NR_SEQ_WAITING_LIST_TYPE     NUMBER(10),
  IE_ANESTESIA                 VARCHAR2(1 BYTE),
  DS_CIRURGIA                  VARCHAR2(500 BYTE),
  NR_MINUTO_DURACAO            NUMBER(10),
  CD_MATERIAL_ENVIO            VARCHAR2(255 BYTE),
  CD_MATERIAL                  NUMBER(6),
  NR_SEQ_LISTA_ESPERA_ORIGEM   NUMBER(10),
  CNPJ_ORIGEM                  VARCHAR2(14 BYTE),
  NM_ESTABELECIMENTO_ORIGEM    VARCHAR2(255 BYTE),
  DT_AGENDA_EXTERNO            DATE,
  CNPJ_LOCAL_AGENDA_EXTERNO    VARCHAR2(14 BYTE),
  DS_LOCAL_AGENDA_EXTERNO      VARCHAR2(255 BYTE),
  IE_INTEGRACAO                VARCHAR2(5 BYTE),
  IE_CONSORCIO                 VARCHAR2(5 BYTE),
  CD_DEPARTAMENTO_MEDICO_EXEC  NUMBER(6),
  CD_ANESTESISTA               VARCHAR2(10 BYTE),
  IE_STATUS_TFD                VARCHAR2(5 BYTE),
  IE_PASSIVEL_TFD              VARCHAR2(3 BYTE),
  IE_NECESSITA_ACOMP_TFD       VARCHAR2(3 BYTE),
  DS_JUST_ACOMPANHANTE_TFD     VARCHAR2(255 BYTE),
  IE_POSICAO                   VARCHAR2(3 BYTE),
  DT_ENTRADA_LISTA             DATE,
  DT_REALIZACAO_AGENDA         DATE,
  IE_FALTA                     VARCHAR2(5 BYTE),
  NR_SEQ_CPOE_PROCEDIMENTO     NUMBER(10),
  NR_PROTOCOLO_ANS             VARCHAR2(21 BYTE),
  NR_SEQ_MOTIVO                NUMBER(10),
  DT_EXECUCAO                  DATE,
  NR_SEQ_MED_GUID_ORDER        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGLIESP_AGECONS_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_AGECONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_AGENDA_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_AGLIESE_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_ATEPACI_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_CIDDOEN_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGLIESP_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGLIESP_CONVENI_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGLIESP_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGLIESP_CPOEPRO_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_CPOE_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_DEPMED_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_DEPARTAMENTO_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_DEPMED_FK2_I ON TASY.AGENDA_LISTA_ESPERA
(CD_DEPARTAMENTO_MEDICO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_ESPMEDI_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_ESTABEL_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_EXALABO_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_EXAME_LAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_JULIESP_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_JUSTIFICATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGLIESP_JULIESP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGLIESP_MATEXLA_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_MATERIAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_MEDEXPA_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_MEDGUIOR_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_MED_GUID_ORDER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_MPRPROG_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_PROGRAMA_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_MTAGLES_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_MTURGLE_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_MOTIVO_URG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_ORPROPM_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_OPM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_PESAVARE_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_MEDICO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_PESFISI_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_PESFISI_FK2_I ON TASY.AGENDA_LISTA_ESPERA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_PESJURI_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGLIESP_PK ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_PLSSEGU_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_PRIREGU_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_PRIORIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_PROCEDE_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_PROCEDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_PROCEDI_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGLIESP_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGLIESP_PROINTE_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGLIESP_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGLIESP_SETATEN_FK_I ON TASY.AGENDA_LISTA_ESPERA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGLIESP_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGLIESP_TICLAPA_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_TIPO_CLASSIF_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLIESP_TIPDEFI_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_TIPO_DEFICIENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGLIESP_TIPDEFI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGLIESP_WALITY_FK_I ON TASY.AGENDA_LISTA_ESPERA
(NR_SEQ_WAITING_LIST_TYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.AGENDA_LISTA_ESPERA_INSAFTER
AFTER DELETE OR INSERT ON TASY.AGENDA_LISTA_ESPERA FOR EACH ROW
BEGIN
  IF INSERTING THEN
	BEGIN
		FOR EQ IN (SELECT * FROM CPOE_EQUIPE_CIRURGIA WHERE NR_SEQ_PROC_CPOE = :NEW.NR_SEQ_CPOE_PROCEDIMENTO) LOOP
                    INSERT INTO EQUIPE_LISTA_ESPERA(NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC,
                        NM_USUARIO_NREC, NR_SEQ_EQUIPE, NR_SEQ_EQUIPE_CPOE, IE_INDICACAO,
                        CD_PROFISSIONAL, CD_FUNCAO, CD_ESPECIALIDADE, NR_SEQ_LISTA_ESPERA)
                    VALUES (EQUIPE_LISTA_ESPERA_SEQ.NEXTVAL, SYSDATE, :NEW.NM_USUARIO, SYSDATE, :NEW.NM_USUARIO, EQ.NR_SEQ_EQUIPE, EQ.NR_SEQUENCIA, EQ.IE_INDICACAO,
                        EQ.CD_PROFISSIONAL, EQ.CD_FUNCAO_MEDICO, EQ.CD_ESPECIALIDADE, :NEW.NR_SEQUENCIA);
		END LOOP;

                FOR EQ IN (SELECT * FROM CPOE_PROCED_ADICIONAL WHERE NR_SEQ_PROC_CPOE = :NEW.NR_SEQ_CPOE_PROCEDIMENTO) LOOP
                    INSERT INTO LISTA_ESPERA_PROC_ADD(NR_SEQUENCIA, DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC,
                        NM_USUARIO_NREC, NR_SEQ_LISTA_ESPERA, NR_ATENDIMENTO, CD_POSICAO, NR_SEQ_PROC_INTERNO)
                    VALUES (LISTA_ESPERA_PROC_ADD_SEQ.NEXTVAL, SYSDATE, :NEW.NM_USUARIO, SYSDATE, :NEW.NM_USUARIO, :NEW.NR_SEQUENCIA,EQ.NR_ATENDIMENTO,
                        EQ.CD_POSICAO, EQ.NR_SEQ_PROC_INTERNO);
		END LOOP;
    END;
  END IF;

  IF DELETING THEN
	DELETE FROM EQUIPE_LISTA_ESPERA WHERE NR_SEQ_LISTA_ESPERA = :OLD.NR_SEQUENCIA;
  END IF;

END;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_LISTA_ESPERA_tp  after update ON TASY.AGENDA_LISTA_ESPERA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_PESSOA_LISTA,1,500);gravar_log_alteracao(substr(:old.NM_PESSOA_LISTA,1,4000),substr(:new.NM_PESSOA_LISTA,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_LISTA',ie_log_w,ds_w,'AGENDA_LISTA_ESPERA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_lista_espera_atual
before insert or update ON TASY.AGENDA_LISTA_ESPERA for each row
declare

ie_utiliza_lista_espera_ag_w	varchar2(1);
nr_conta_w				number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then
	begin
		if	(:new.cd_pessoa_fisica is not null) then
			:new.nm_pessoa_lista := obter_nome_pf(:new.cd_pessoa_fisica);
		end if;
	exception
		when others then
		null;
	end;

	begin
		update	agenda_paciente
		set	dt_chegada_prev = :new.dt_prevista_internacao
		where	nr_seq_lista	= :new.nr_sequencia;
	exception
		when others then
		null;
	end;

	begin

	select 	max(nvl(ie_utiliza_lista_espera_ag,'N'))
	into	ie_utiliza_lista_espera_ag_w
	from	parametro_agenda
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	if 	(ie_utiliza_lista_espera_ag_w = 'S') then

		if (obter_funcao_ativa = 821) then
			:new.cd_tipo_agenda := 3;
		elsif (obter_funcao_ativa = 820) then
			:new.cd_tipo_agenda := 2;
		elsif (obter_funcao_ativa = 866) then
			:new.cd_tipo_agenda := 5;
		elsif (obter_funcao_ativa = 871) then
			:new.cd_tipo_agenda := 1;
		end if;

	end if;

	if ((:old.ie_status_espera <> :new.ie_status_espera) and
	    (:new.ie_status_espera = 'C')) then
	  wheb_usuario_pck.set_ie_commit('N');

	 if (obter_qt_agend_opm(:new.nr_seq_opm, :new.DT_AGENDAMENTO, :new.nr_sequencia, 'CLE') = 0) then
	    gravar_status_op_opm(:new.nr_seq_opm,'CLE',:new.cd_agenda,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento);
	  end if;
	  wheb_usuario_pck.set_ie_commit('S');
	end if;

	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_lista_espera_before_u
BEFORE UPDATE ON TASY.AGENDA_LISTA_ESPERA FOR EACH ROW
DECLARE
qt_reg_w	number(10);
nr_seq_estagio_w	number(10);
nr_seq_estagio_ant_w	number(10);
nr_seq_lista_espera_w	number(10);
pragma autonomous_transaction;

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if 	(:old.IE_STATUS_ESPERA <> 'C') and
	(:new.IE_STATUS_ESPERA = 'C') then

	SELECT	count(*)
	into	qt_reg_w
	from	ag_lista_espera_estagio
  where  IE_SITUACAO = 'A'
  and    ie_acao = 'C';

  if (qt_reg_w > 0) then
    select  count(*)
    into  qt_reg_w
    from  ag_lista_esp_estagio_ant a,
        agenda_lista_espera b
    where  a.nr_seq_estagio = (SELECT  c.nr_sequencia
                  from  ag_lista_espera_estagio c
                  where  c.IE_SITUACAO = 'A'
                  and    c.ie_acao = 'AG')
    and    a.nr_seq_estagio_ant = b.nr_seq_estagio
    and    b.nr_sequencia = :new.nr_sequencia;

    if (qt_reg_w > 0) then
      SELECT  max(nr_sequencia )
      into  nr_seq_estagio_w
      from  ag_lista_espera_estagio b
      where  b.IE_SITUACAO = 'A'
      and    b.ie_acao = 'C';

      if  (nr_seq_estagio_w is not null) then
        :new.nr_seq_estagio := nr_seq_estagio_w;
      end if;
    end if;
  end if;
end if;

if   (:old.NR_SEQ_ESTAGIO <> :new.NR_SEQ_ESTAGIO) or
  (:old.NR_SEQ_ESTAGIO is null and :new.NR_SEQ_ESTAGIO is not null) then

    insert into AGENDA_LISTA_ESPERA_LOG (NR_SEQUENCIA,DT_ATUALIZACAO, NM_USUARIO, DT_ATUALIZACAO_NREC, NM_USUARIO_NREC, NR_SEQ_ESTAGIO, NR_SEQ_ESTAGIO_ANT, NR_SEQ_LISTA_ESPERA)
    values (AGENDA_LISTA_ESPERA_LOG_SEQ.nextval,
        sysdate,
        obter_usuario_ativo,
        sysdate,
        obter_usuario_ativo,
        :new.nr_seq_estagio,
        :old.NR_SEQ_ESTAGIO,
        :new.NR_SEQUENCIA);



end if;

if   (:old.IE_STATUS_ESPERA <> :new.IE_STATUS_ESPERA) or
  (:old.IE_STATUS_ESPERA is null and :new.IE_STATUS_ESPERA is not null) then
      envia_lista_espera_integracao(:new.NR_SEQUENCIA, 'S ', null, :new.IE_STATUS_ESPERA );
end if;

<<Final>>
qt_reg_w := 0;

commit;

END;
/


ALTER TABLE TASY.AGENDA_LISTA_ESPERA ADD (
  CONSTRAINT AGLIESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_LISTA_ESPERA ADD (
  CONSTRAINT AGLIESP_CPOEPRO_FK 
 FOREIGN KEY (NR_SEQ_CPOE_PROCEDIMENTO) 
 REFERENCES TASY.CPOE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_MTAGLES_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_AGENDA_LISTA_ESPERA (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_MEDGUIOR_FK 
 FOREIGN KEY (NR_SEQ_MED_GUID_ORDER) 
 REFERENCES TASY.MEDICAL_GUIDANCE_ORDER (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_MTURGLE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_URG) 
 REFERENCES TASY.MOTIVO_URG_LISTA_ESPERA (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_JULIESP_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFICATIVA) 
 REFERENCES TASY.JUSTIFICATIVA_LISTA_ESPERA (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_CIDDOEN_FK 
 FOREIGN KEY (CD_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT AGLIESP_PESAVARE_FK 
 FOREIGN KEY (CD_MEDICO_EXEC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGLIESP_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGLIESP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGLIESP_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT AGLIESP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGLIESP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AGLIESP_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AGLIESP_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_PROCEDE_FK 
 FOREIGN KEY (CD_PROCEDENCIA) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA),
  CONSTRAINT AGLIESP_DEPMED_FK2 
 FOREIGN KEY (CD_DEPARTAMENTO_MEDICO_EXEC) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT AGLIESP_TIPDEFI_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DEFICIENCIA) 
 REFERENCES TASY.TIPO_DEFICIENCIA (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT AGLIESP_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME_LAB) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT AGLIESP_ORPROPM_FK 
 FOREIGN KEY (NR_SEQ_OPM) 
 REFERENCES TASY.ORDEM_PRODUCAO_OPM (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_MEDEXPA_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.MED_EXAME_PADRAO (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_MPRPROG_FK 
 FOREIGN KEY (NR_SEQ_PROGRAMA_SAUDE) 
 REFERENCES TASY.MPREV_PROGRAMA (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_TICLAPA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CLASSIF_PAC) 
 REFERENCES TASY.TIPO_CLASSIFICAO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA),
  CONSTRAINT AGLIESP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT AGLIESP_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTAMENTO_MEDICO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT AGLIESP_PRIREGU_FK 
 FOREIGN KEY (NR_SEQ_PRIORIDADE) 
 REFERENCES TASY.PRIORIDADE_REGULACAO (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_AGLIESE_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.AG_LISTA_ESPERA_ESTAGIO (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AGLIESP_WALITY_FK 
 FOREIGN KEY (NR_SEQ_WAITING_LIST_TYPE) 
 REFERENCES TASY.WAITING_LIST_TYPE (NR_SEQUENCIA),
  CONSTRAINT AGLIESP_MATEXLA_FK 
 FOREIGN KEY (CD_MATERIAL_EXAME) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (CD_MATERIAL_EXAME));

GRANT SELECT ON TASY.AGENDA_LISTA_ESPERA TO NIVEL_1;

