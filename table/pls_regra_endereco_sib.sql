ALTER TABLE TASY.PLS_REGRA_ENDERECO_SIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ENDERECO_SIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ENDERECO_SIB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO       NUMBER(10),
  IE_TIPO_ENDERECO      VARCHAR2(2 BYTE),
  IE_PRIMEIRO_ENDERECO  NUMBER(2),
  IE_SEGUNDO_ENDERECO   NUMBER(2),
  IE_TERCEIRO_ENDERECO  NUMBER(2),
  IE_QUARTO_ENDERECO    NUMBER(2),
  NR_CONTRATO           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSESIB_PK ON TASY.PLS_REGRA_ENDERECO_SIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSESIB_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSESIB_PLSCONT_FK_I ON TASY.PLS_REGRA_ENDERECO_SIB
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_endereco_sib_ins_udt
before insert or update ON TASY.PLS_REGRA_ENDERECO_SIB for each row
declare

nr_seq_contrato_w	pls_contrato.nr_sequencia%type;

begin

if	(:new.nr_contrato is not null) and
	(:new.nr_contrato <> nvl(:old.nr_contrato,0)) then

	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato = :new.nr_contrato;

	if	(nr_seq_contrato_w is not null) then
		:new.nr_seq_contrato := nr_seq_contrato_w;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(853575); --O contrato informado nao existe. Verifique!
	end if;
else
	:new.nr_seq_contrato := null;
end if;

end;
/


ALTER TABLE TASY.PLS_REGRA_ENDERECO_SIB ADD (
  CONSTRAINT PLSESIB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_ENDERECO_SIB ADD (
  CONSTRAINT PLSESIB_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_ENDERECO_SIB TO NIVEL_1;

