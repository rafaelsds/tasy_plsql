ALTER TABLE TASY.DIS_REGRA_LOCAL_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIS_REGRA_LOCAL_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIS_REGRA_LOCAL_SETOR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_LOCAL_ESTOQUE        NUMBER(4),
  NR_SEQ_DIS_REGRA_SETOR  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIRELOS_DISRESE_FK_I ON TASY.DIS_REGRA_LOCAL_SETOR
(NR_SEQ_DIS_REGRA_SETOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIRELOS_PK ON TASY.DIS_REGRA_LOCAL_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DIRELOS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.dis_regra_local_setor_atual
before insert or delete or update ON TASY.DIS_REGRA_LOCAL_SETOR for each row
declare
begin

if	(inserting)then
	gerar_int_dankia_pck.dankia_regra_local_setor(:new.nr_seq_dis_regra_setor,:new.cd_local_estoque,'I',:new.nm_usuario);
elsif(deleting) then
	gerar_int_dankia_pck.dankia_regra_local_setor(:old.nr_seq_dis_regra_setor,:old.cd_local_estoque,'E',:old.nm_usuario);
elsif(updating) then
	if	(:old.cd_local_estoque is not null) and
		(:new.cd_local_estoque is not null) and
		(:old.cd_local_estoque <> :new.cd_local_estoque) then
		gerar_int_dankia_pck.dankia_regra_local_setor(:old.nr_seq_dis_regra_setor,:old.cd_local_estoque,'E',:new.nm_usuario);
		gerar_int_dankia_pck.dankia_regra_local_setor(:new.nr_seq_dis_regra_setor,:new.cd_local_estoque,'I',:new.nm_usuario);
	end if;
end if;

end;
/


ALTER TABLE TASY.DIS_REGRA_LOCAL_SETOR ADD (
  CONSTRAINT DIRELOS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIS_REGRA_LOCAL_SETOR ADD (
  CONSTRAINT DIRELOS_DISRESE_FK 
 FOREIGN KEY (NR_SEQ_DIS_REGRA_SETOR) 
 REFERENCES TASY.DIS_REGRA_SETOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.DIS_REGRA_LOCAL_SETOR TO NIVEL_1;

