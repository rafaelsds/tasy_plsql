ALTER TABLE TASY.LATAM_RUP_VISAO_AS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LATAM_RUP_VISAO_AS CASCADE CONSTRAINTS;

CREATE TABLE TASY.LATAM_RUP_VISAO_AS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OBSERVACAO        LONG                     NOT NULL,
  NR_SEQ_MODULO        NUMBER(10),
  NR_SEQ_GAP           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LATAMVAS_LATAMGAP_FK_I ON TASY.LATAM_RUP_VISAO_AS
(NR_SEQ_GAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATAMVAS_LATAMGAP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LATAMVAS_LATMODU_FK_I ON TASY.LATAM_RUP_VISAO_AS
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATAMVAS_LATMODU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LATAMVAS_PK ON TASY.LATAM_RUP_VISAO_AS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATAMVAS_PK
  MONITORING USAGE;


ALTER TABLE TASY.LATAM_RUP_VISAO_AS ADD (
  CONSTRAINT LATAMVAS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LATAM_RUP_VISAO_AS ADD (
  CONSTRAINT LATAMVAS_LATAMGAP_FK 
 FOREIGN KEY (NR_SEQ_GAP) 
 REFERENCES TASY.LATAM_GAP (NR_SEQUENCIA),
  CONSTRAINT LATAMVAS_LATMODU_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.LATAM_MODULO (NR_SEQUENCIA));

GRANT SELECT ON TASY.LATAM_RUP_VISAO_AS TO NIVEL_1;

