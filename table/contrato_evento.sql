ALTER TABLE TASY.CONTRATO_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTRATO_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTRATO_EVENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO      NUMBER(10)               NOT NULL,
  CD_REPRESENTANTE     VARCHAR2(10 BYTE)        NOT NULL,
  CD_VENDEDOR          VARCHAR2(10 BYTE)        NOT NULL,
  DS_EVENTO            VARCHAR2(255 BYTE)       NOT NULL,
  DS_ENDERECO          VARCHAR2(4000 BYTE)      NOT NULL,
  DS_COMPLEMENTO       VARCHAR2(4000 BYTE),
  DT_EVENTO            DATE,
  DS_HORARIOS          VARCHAR2(2000 BYTE),
  DS_EQUIPE            VARCHAR2(4000 BYTE),
  VL_EVENTO            NUMBER(13,4),
  DT_PAGAMENTO         DATE,
  NR_SEQ_FORMA_PAGTO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONTEVE_CONTRAT_FK_I ON TASY.CONTRATO_EVENTO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTEVE_CONTRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTEVE_FORPAGA_FK_I ON TASY.CONTRATO_EVENTO
(NR_SEQ_FORMA_PAGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTEVE_FORPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONTEVE_PESFISI_FK_I ON TASY.CONTRATO_EVENTO
(CD_REPRESENTANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONTEVE_PESFISI_FK1_I ON TASY.CONTRATO_EVENTO
(CD_VENDEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONTEVE_PK ON TASY.CONTRATO_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONTEVE_PK
  MONITORING USAGE;


ALTER TABLE TASY.CONTRATO_EVENTO ADD (
  CONSTRAINT CONTEVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTRATO_EVENTO ADD (
  CONSTRAINT CONTEVE_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CONTEVE_FORPAGA_FK 
 FOREIGN KEY (NR_SEQ_FORMA_PAGTO) 
 REFERENCES TASY.FORMA_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT CONTEVE_PESFISI_FK 
 FOREIGN KEY (CD_REPRESENTANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CONTEVE_PESFISI_FK1 
 FOREIGN KEY (CD_VENDEDOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CONTRATO_EVENTO TO NIVEL_1;

