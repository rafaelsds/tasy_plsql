ALTER TABLE TASY.CAN_TNM_SUB_LOCAL_AUX
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAN_TNM_SUB_LOCAL_AUX CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAN_TNM_SUB_LOCAL_AUX
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_SUB_LOCALIZACAO  NUMBER(10)            NOT NULL,
  DS_SUB_LOCALIZACAO      VARCHAR2(255 BYTE)    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CANTSLA_CANTSUL_FK_I ON TASY.CAN_TNM_SUB_LOCAL_AUX
(NR_SEQ_SUB_LOCALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CANTSLA_PK ON TASY.CAN_TNM_SUB_LOCAL_AUX
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CAN_TNM_SUB_LOCAL_AUX ADD (
  CONSTRAINT CANTSLA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CAN_TNM_SUB_LOCAL_AUX ADD (
  CONSTRAINT CANTSLA_CANTSUL_FK 
 FOREIGN KEY (NR_SEQ_SUB_LOCALIZACAO) 
 REFERENCES TASY.CAN_TNM_SUB_LOCALIZACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CAN_TNM_SUB_LOCAL_AUX TO NIVEL_1;

