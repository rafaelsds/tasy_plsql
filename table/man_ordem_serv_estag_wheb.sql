ALTER TABLE TASY.MAN_ORDEM_SERV_ESTAG_WHEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERV_ESTAG_WHEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERV_ESTAG_WHEB
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_ORDEM_SERVICO       NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DS_ESTAGIO                 VARCHAR2(255 BYTE) NOT NULL,
  DS_EXPLIC_ESTAGIO_PHILIPS  VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANOSEW_MANORSE_FK_I ON TASY.MAN_ORDEM_SERV_ESTAG_WHEB
(NR_SEQ_ORDEM_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANOSEW_PK ON TASY.MAN_ORDEM_SERV_ESTAG_WHEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_ORDEM_SERV_ESTAG_WHEB ADD (
  CONSTRAINT MANOSEW_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERV_ESTAG_WHEB ADD (
  CONSTRAINT MANOSEW_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERVICO) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORDEM_SERV_ESTAG_WHEB TO NIVEL_1;

