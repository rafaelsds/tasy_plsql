ALTER TABLE TASY.GRUPO_SERVICO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GRUPO_SERVICO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.GRUPO_SERVICO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO_SERV    NUMBER(10),
  CD_ITEM_SERVICO      VARCHAR2(20 BYTE)        NOT NULL,
  DS_ITEM_SERVICO      VARCHAR2(500 BYTE)       NOT NULL,
  CD_PREFEITURA        VARCHAR2(15 BYTE),
  IE_FACULTATIVO       VARCHAR2(1 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  PR_FONTE_IBPT        NUMBER(7,4),
  PR_FONTE_IBPT_FED    NUMBER(7,4),
  PR_FONTE_IBPT_EST    NUMBER(7,4),
  PR_FONTE_IBPT_MUN    NUMBER(7,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GRUSRVIT_GRUSERV_FK_I ON TASY.GRUPO_SERVICO_ITEM
(NR_SEQ_GRUPO_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GRUSRVIT_GRUSERV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.GRUSRVIT_PK ON TASY.GRUPO_SERVICO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.GRUPO_SERVICO_ITEM_tp  after update ON TASY.GRUPO_SERVICO_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_SERV,1,4000),substr(:new.NR_SEQ_GRUPO_SERV,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_SERV',ie_log_w,ds_w,'GRUPO_SERVICO_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ITEM_SERVICO,1,4000),substr(:new.CD_ITEM_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ITEM_SERVICO',ie_log_w,ds_w,'GRUPO_SERVICO_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'GRUPO_SERVICO_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PREFEITURA,1,4000),substr(:new.CD_PREFEITURA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PREFEITURA',ie_log_w,ds_w,'GRUPO_SERVICO_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FACULTATIVO,1,4000),substr(:new.IE_FACULTATIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FACULTATIVO',ie_log_w,ds_w,'GRUPO_SERVICO_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ITEM_SERVICO,1,4000),substr(:new.DS_ITEM_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ITEM_SERVICO',ie_log_w,ds_w,'GRUPO_SERVICO_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.GRUPO_SERVICO_ITEM ADD (
  CONSTRAINT GRUSRVIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GRUPO_SERVICO_ITEM ADD (
  CONSTRAINT GRUSRVIT_GRUSERV_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERV) 
 REFERENCES TASY.GRUPO_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.GRUPO_SERVICO_ITEM TO NIVEL_1;

