ALTER TABLE TASY.CAN_ORDEM_PROD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAN_ORDEM_PROD CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAN_ORDEM_PROD
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_PREVISTA                DATE               NOT NULL,
  IE_VIA_APLICACAO           VARCHAR2(5 BYTE)   NOT NULL,
  QT_HORA_VALIDADE           NUMBER(5)          NOT NULL,
  QT_MIN_VALIDADE            NUMBER(3)          NOT NULL,
  QT_HORA_APLIC              NUMBER(3)          NOT NULL,
  QT_MIN_APLIC               NUMBER(4)          NOT NULL,
  CD_MEDICO_RESP             VARCHAR2(10 BYTE),
  CD_FARMACEUTICO            VARCHAR2(10 BYTE),
  CD_MANIPULADOR             VARCHAR2(10 BYTE),
  CD_AUXILIAR                VARCHAR2(10 BYTE),
  DT_INICIO_PREPARO          DATE,
  DT_FIM_PREPARO             DATE,
  DT_ENTREGA_SETOR           DATE,
  DT_ADMINISTRACAO           DATE,
  DT_DEVOLUCAO               DATE,
  DT_IMPRESSAO               DATE,
  NR_SEQ_ATENDIMENTO         NUMBER(10),
  NR_PRESCRICAO              NUMBER(14),
  NM_USUARIO_INIC_PREP       VARCHAR2(15 BYTE),
  NM_USUARIO_FIM_PREP        VARCHAR2(15 BYTE),
  NM_USUARIO_DISP            VARCHAR2(15 BYTE),
  NM_USUARIO_ADM             VARCHAR2(15 BYTE),
  NM_USUARIO_DEVOL           VARCHAR2(15 BYTE),
  NR_SEQ_ETAPA_PROD          NUMBER(10),
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  NR_SEQ_MOTIVO_DUPL         NUMBER(10),
  QT_DOSAGEM                 NUMBER(15,4),
  IE_TIPO_DOSAGEM            VARCHAR2(3 BYTE),
  CD_SETOR_PACIENTE          NUMBER(5),
  NR_ATENDIMENTO             NUMBER(10),
  CD_PESSOA_DEVOL            VARCHAR2(10 BYTE),
  DS_PRECAUCAO               VARCHAR2(255 BYTE),
  DT_VALIDADE_ORDEM          DATE,
  DT_VALIDADE_ROTULO         DATE,
  IE_CANCELADA               VARCHAR2(1 BYTE)   NOT NULL,
  DS_ORIENTACAO_ADM          VARCHAR2(255 BYTE),
  NM_USUARIO_CONF            VARCHAR2(15 BYTE),
  IE_CONFERIDO               VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  QT_DIAS_UTIL               NUMBER(3),
  IE_SUSPENSA                VARCHAR2(1 BYTE)   NOT NULL,
  DT_CANCELAMENTO            DATE,
  NM_USUARIO_CANCEL          VARCHAR2(15 BYTE),
  DT_SUSPENSAO               DATE,
  NM_USUARIO_SUSP            VARCHAR2(15 BYTE),
  DT_ESTORNO_CANCEL          DATE,
  NM_USUARIO_ESTORNO_CANCEL  VARCHAR2(15 BYTE),
  DT_CHECAGEM                DATE,
  QT_PESO                    NUMBER(6,3),
  QT_SUPERF_CORPOREA         NUMBER(7,2),
  DT_GERACAO_ORDEM           DATE,
  QT_VOLUME_ASPIRADO         NUMBER(15,3),
  IE_PRE_MEDICACAO           VARCHAR2(1 BYTE),
  DT_SOLIC_PERDA             DATE,
  DT_TRANSFERENCIA           DATE,
  CD_ESTAB_ADMINISTRACAO     NUMBER(4),
  IE_MEDIC_PACIENTE          VARCHAR2(1 BYTE),
  QT_TEMP                    NUMBER(5,2),
  DS_JUSTIF_REIMPRESSAO      VARCHAR2(255 BYTE),
  DT_EXPEDIDO                DATE,
  NM_EXPEDIDO                VARCHAR2(15 BYTE),
  DT_RECEBIDO                DATE,
  NM_RECEBIDO                VARCHAR2(15 BYTE),
  NM_ENTREGUE                VARCHAR2(15 BYTE),
  DT_ENTREGUE                DATE,
  IE_BOMBA_INFUSAO           VARCHAR2(1 BYTE),
  NR_SEQ_ORDEM_ORIGEM        NUMBER(10),
  DT_CONFERENCIA             DATE,
  DT_IMPRESSAO_PRE_PREPARO   DATE,
  DT_CONF_ORDEM_ATEND        DATE,
  NM_CONF_ORDEM_ATEND        VARCHAR2(15 BYTE),
  DS_STACK_ORIGEM            VARCHAR2(4000 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  DT_INICIO_SEPARACAO        DATE,
  DT_FIM_SEPARACAO           DATE,
  NM_USUARIO_SEPARACAO       VARCHAR2(25 BYTE),
  DT_HIGIENIZACAO            DATE,
  NM_USUARIO_HIGIEN          VARCHAR2(15 BYTE),
  DT_CONF_PREPARO            DATE,
  NM_USUARIO_CONF_PREP       VARCHAR2(15 BYTE),
  NM_USUARIO_CONF_MEDIC      VARCHAR2(15 BYTE),
  DT_CONF_MEDICAMENTO        DATE,
  IE_CONF_RECEBIMENTO        VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_INCONF       NUMBER(10),
  DT_RECEB_ENF               DATE,
  NM_USUARIO_REC_ENF         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CANORPR_ATEPACI_FK_I ON TASY.CAN_ORDEM_PROD
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_CANORPR_FK_I ON TASY.CAN_ORDEM_PROD
(NR_SEQ_ORDEM_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPR_CANORPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPR_ESTABEL_FK_I ON TASY.CAN_ORDEM_PROD
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPR_ESTABEL_FK2_I ON TASY.CAN_ORDEM_PROD
(CD_ESTAB_ADMINISTRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPR_ESTABEL_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPR_FARETPR_FK_I ON TASY.CAN_ORDEM_PROD
(NR_SEQ_ETAPA_PROD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_FARMODO_FK_I ON TASY.CAN_ORDEM_PROD
(NR_SEQ_MOTIVO_DUPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPR_FARMODO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPR_I1 ON TASY.CAN_ORDEM_PROD
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_I2 ON TASY.CAN_ORDEM_PROD
(NR_PRESCRICAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_I3 ON TASY.CAN_ORDEM_PROD
(CD_PESSOA_FISICA, DT_PREVISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_I4 ON TASY.CAN_ORDEM_PROD
(DT_PREVISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_PESFISI_FK_I ON TASY.CAN_ORDEM_PROD
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPR_PESFISI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPR_PESFISI_FK2_I ON TASY.CAN_ORDEM_PROD
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_PESFISI_FK3_I ON TASY.CAN_ORDEM_PROD
(CD_FARMACEUTICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_PESFISI_FK5_I ON TASY.CAN_ORDEM_PROD
(CD_MANIPULADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_PESFISI_FK6_I ON TASY.CAN_ORDEM_PROD
(CD_AUXILIAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_PESFISI_FK8_I ON TASY.CAN_ORDEM_PROD
(CD_PESSOA_DEVOL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CANORPR_PK ON TASY.CAN_ORDEM_PROD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CANORPR_SETATEN_FK_I ON TASY.CAN_ORDEM_PROD
(CD_SETOR_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPR_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CANORPR_VIAAPLI_FK_I ON TASY.CAN_ORDEM_PROD
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CANORPR_VIAAPLI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CAN_ORDEM_PROD_tp  after update ON TASY.CAN_ORDEM_PROD FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_ETAPA_PROD,1,4000),substr(:new.NR_SEQ_ETAPA_PROD,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ETAPA_PROD',ie_log_w,ds_w,'CAN_ORDEM_PROD',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.CAN_ORDEM_PROD_INSERT
BEFORE INSERT ON TASY.CAN_ORDEM_PROD FOR EACH ROW
declare
nr_atendimento_w	Number(10);
ds_origem_w		varchar2(4000);
cd_pessoa_fisica_atend_w			pessoa_fisica.cd_pessoa_fisica%type;

BEGIN

if	(:new.nr_prescricao is not null) then
	select	nvl(max(nr_atendimento),0)
	into	nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao	= :new.nr_prescricao;


	if	(nr_atendimento_w > 0) then
		:new.nr_atendimento	:= nr_atendimento_w;
	end if;
end if;

	:new.ds_stack_origem := substr(dbms_utility.format_call_stack,1,4000);

if	(:new.DT_GERACAO_ORDEM is null) then
	:new.DT_GERACAO_ORDEM := sysdate;
end if;

if (:new.nr_atendimento is not null) and
	(:new.cd_pessoa_fisica is not null) then

	select  max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_atend_w
	from 	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	if (:new.cd_pessoa_fisica <> nvl(cd_pessoa_fisica_atend_w,:new.cd_pessoa_fisica)) then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(1005227);
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.CAN_ORDEM_PROD_ATUAL
before insert or update ON TASY.CAN_ORDEM_PROD for each row
declare


begin
if	(nvl(:old.DT_PREVISTA,sysdate+10) <> :new.DT_PREVISTA) and
	(:new.DT_PREVISTA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_PREVISTA, 'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CAN_ORDEM_PROD_UPDATE
BEFORE UPDATE ON TASY.CAN_ORDEM_PROD FOR EACH ROW
declare
nr_atendimento_w			Number(10);
qt_reg_w					number(1);
ds_origem_w					varchar2(4000);
ie_consiste_dt_preparo_w	Varchar2(1);
ie_consiste_movimentacao_w	varchar2(1);
ie_possui_nota_w			varchar2(1);
cd_pessoa_fisica_w			pessoa_fisica.cd_pessoa_fisica%type;
cd_pessoa_fisica_atend_w			pessoa_fisica.cd_pessoa_fisica%type;

pragma autonomous_transaction;

BEGIN

Obter_Param_Usuario(3130,289,obter_perfil_ativo, :new.nm_usuario, 0 , ie_consiste_dt_preparo_w);
Obter_Param_Usuario(3130,340,obter_perfil_ativo, :new.nm_usuario, 0 , ie_consiste_movimentacao_w);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(ie_consiste_movimentacao_w = 'S') and
	(:new.nr_seq_etapa_prod <> :old.nr_seq_etapa_prod) then
	select	nvl(max('S'),'N')
	into	ie_possui_nota_w
	from	transf_ordem_prod a,
			nota_fiscal b,
			nota_fiscal_item c
	where	b.nr_sequencia = c.nr_sequencia
	and		a.nr_ordem_compra = b.nr_ordem_compra
	and		a.nr_seq_ordem_prod = :new.nr_sequencia
	and		substr(obter_se_nota_entrada_saida(b.nr_sequencia),1,1) = 'E';

	if 	(ie_possui_nota_w = 'S') then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(196854);
	end if;
end if;

if (ie_consiste_dt_preparo_w = 'S') then
	if	(:new.dt_inicio_preparo is null) and
		(:new.dt_fim_preparo is not null) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(176187);
	end if;

	if 	(:new.dt_inicio_preparo > :new.dt_fim_preparo) and
		(:new.dt_fim_preparo is not null) then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(176189 );
	end if;
end if;

if	(:new.dt_inicio_preparo is null) and
	(:new.dt_fim_preparo is not null) then
	:new.dt_inicio_preparo := sysdate;
end if;

if	(:new.nr_prescricao is not null) and
	(:new.nr_atendimento is null) then
	select	nvl(max(nr_atendimento),0)
	into	nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao	= :new.nr_prescricao;

	if	(nr_atendimento_w > 0) then
		:new.nr_atendimento	:= nr_atendimento_w;
	end if;
end if;

if	(:new.ie_cancelada = 'S') and
	(:old.ie_cancelada = 'N') then
	:new.dt_cancelamento	:= sysdate;
	:new.nm_usuario_cancel	:= :new.nm_usuario;
elsif	(:new.ie_cancelada = 'N') and
	(:old.ie_cancelada = 'S') then
	:new.dt_cancelamento	:= null;
	:new.nm_usuario_cancel	:= null;
end if;

if	(:new.ie_suspensa = 'S') and
	(:old.ie_suspensa = 'N') then
	:new.dt_suspensao	:= sysdate;
	:new.nm_usuario_susp	:= :new.nm_usuario;
elsif	(:new.ie_suspensa = 'N') and
	(:old.ie_suspensa = 'S') then
	:new.dt_suspensao	:= null;
	:new.nm_usuario_susp	:= null;
end if;

--Adicionado na OS 980082, pois havia casos onde o usuario alterava a pessoa fisica, ficando em desconformidade com a prescricao
if	(:new.cd_pessoa_fisica is not null) and
	(:new.nr_prescricao is not null) and
	(:new.cd_pessoa_fisica <> :old.cd_pessoa_fisica) then
	select	nvl(max(cd_pessoa_fisica),0)
	into	cd_pessoa_fisica_w
	from	prescr_medica
	where	nr_prescricao	= :new.nr_prescricao;

	if	(cd_pessoa_fisica_w <> 0) then
		if (:new.cd_pessoa_fisica <> cd_pessoa_fisica_w) then
			--Mensagem: O paciente informado n�o � mesmo paciente da prescri��o.
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(387253);
		end if;
	end if;
end if;

if (:new.nr_atendimento is not null) and
	(:new.cd_pessoa_fisica is not null) then

	select  max(cd_pessoa_fisica)
	into	cd_pessoa_fisica_atend_w
	from 	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	if (:new.cd_pessoa_fisica <> nvl(cd_pessoa_fisica_atend_w,:new.cd_pessoa_fisica)) then
		wheb_mensagem_pck.Exibir_Mensagem_Abort(1005227);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;

/*
if 	((:new.ie_medic_paciente is not null) and
	(:old.ie_medic_paciente is null))  or
	((:new.ie_medic_paciente is null) and
	(:old.ie_medic_paciente is not null))  then
	ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);

	insert	into logx_tasy (
		dt_atualizacao,
		nm_usuario,
		cd_log,
		ds_log)
	values 	(sysdate,
		:new.nm_usuario,
		18910,
		substr(	'; funcao_ativa='		||to_char(wheb_usuario_pck.get_cd_funcao) ||
			'; usu�rio='			||:new.nm_usuario||
			'; ie_medic_paciente='		||:new.ie_medic_paciente||
			'; perfil='			||to_char(wheb_usuario_pck.get_cd_perfil) ||
			'nr_sequencia='||:new.nr_sequencia||ds_origem_w,1,2000));
end if;

if 	(:old.nr_seq_etapa_prod is not null) and
	(:new.nr_seq_etapa_prod <> :old.nr_seq_etapa_prod) then
	insert	into logx_tasy (
		dt_atualizacao,
		nm_usuario,
		cd_log,
		ds_log)
	values 	(sysdate,
		:new.nm_usuario,
		18999,
		substr(	'; funcao_ativa='		||to_char(wheb_usuario_pck.get_cd_funcao) ||
		'; etapa antiga='||:old.nr_seq_etapa_prod||
		'; modificada para='||:new.nr_seq_etapa_prod||
		'; usu�rio='			||:new.nm_usuario||
		'; ie_medic_paciente='		||:new.ie_medic_paciente||
		'; perfil='			||to_char(wheb_usuario_pck.get_cd_perfil) ||
		'nr_sequencia='||:new.nr_sequencia||ds_origem_w,1,2000));
end if;

if 	(:new.dt_fim_preparo is not null) and
	(:old.dt_fim_preparo is null) then
	ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);

	insert	into logx_tasy (
		dt_atualizacao,
		nm_usuario,
		cd_log,
		ds_log)
	values 	(sysdate,
		:new.nm_usuario,
		18999,
		substr(	'; funcao_ativa='		||to_char(wheb_usuario_pck.get_cd_funcao) ||
			'; usu�rio='			||:new.nm_usuario||
			'; ie_medic_paciente='		||:new.ie_medic_paciente||
			'; perfil='			||to_char(wheb_usuario_pck.get_cd_perfil) ||
			'nr_sequencia='||:new.nr_sequencia||ds_origem_w,1,2000));
end if;

if 	((:old.dt_fim_preparo is not null) and (:new.dt_fim_preparo is null)) or
	((:old.dt_inicio_preparo is not null) and (:new.dt_inicio_preparo is null)) or
	((:old.dt_administracao is not null) and (:new.dt_administracao is null)) or
	((:old.dt_inicio_preparo is not null) and (:new.dt_inicio_preparo is null)) or
	((:old.dt_cancelamento is not null) and (:new.dt_cancelamento is null)) or
	((:old.dt_checagem is not null) and (:new.dt_checagem is null)) or
	((:old.dt_devolucao is not null) and (:new.dt_devolucao is null)) then
	begin
	ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
	insert	into logx_tasy 	(dt_atualizacao, nm_usuario, cd_log, ds_log)
		values 		(sysdate, :new.nm_usuario, 366544,
				 ':old.dt_fim_preparo='		||:old.dt_fim_preparo||
					';:new.dt_fim_preparo'		||:new.dt_fim_preparo||
					'; new.dt_administracao='	||:new.dt_administracao||
					'; old.dt_administracao='	||:old.dt_administracao||
					'; new.dt_inicio_preparo='	||:new.dt_inicio_preparo||
					'; old.dt_inicio_preparo='	||:old.dt_inicio_preparo||
					'; old.dt_cancelamento='	||:old.dt_cancelamento||
					'; new.dt_cancelamento='	||:new.dt_cancelamento||
					'; old.dt_checagem='		||:old.dt_checagem||
					'; new.dt_checagem='		||:new.dt_checagem||
					'; old.dt_devolucao='		||:old.dt_devolucao||
					'; new.dt_devolucao='		||:new.dt_devolucao||
					'; nr_sequencia='		||:new.nr_sequencia||
					'; funcao_ativa='		||to_char(wheb_usuario_pck.get_cd_funcao) ||
					'; usu�rio='			||:new.nm_usuario||
					'; perfil='			||to_char(wheb_usuario_pck.get_cd_perfil) ||
					'; Stack='			||substr(ds_origem_w,1,1000));
	exception
	when others then
		ds_origem_w	:= '';
	end;
end if;

if 	((:old.dt_fim_preparo is null) and (:new.dt_fim_preparo is not null)) or
	((:old.dt_inicio_preparo is null) and (:new.dt_inicio_preparo is not null)) or
	((:old.dt_administracao is null) and (:new.dt_administracao is not null)) or
	((:old.dt_inicio_preparo is null) and (:new.dt_inicio_preparo is not null)) or
	((:old.dt_cancelamento is null) and (:new.dt_cancelamento is not null)) or
	((:old.dt_checagem is null) and (:new.dt_checagem is not null)) or
	((:old.dt_devolucao is null) and (:new.dt_devolucao is not null)) then
	begin
	ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
	insert	into logx_tasy 	(dt_atualizacao, nm_usuario, cd_log, ds_log)
		values 		(sysdate, :new.nm_usuario, 366544,
				 ':old.dt_fim_preparo='		||:old.dt_fim_preparo||
					';:new.dt_fim_preparo'		||:new.dt_fim_preparo||
					'; new.dt_administracao='	||:new.dt_administracao||
					'; old.dt_administracao='	||:old.dt_administracao||
					'; new.dt_inicio_preparo='	||:new.dt_inicio_preparo||
					'; old.dt_inicio_preparo='	||:old.dt_inicio_preparo||
					'; old.nm_usuario_fim_prep='	||:old.nm_usuario_fim_prep||
					'; new.nm_usuario_fim_prep='	||:new.nm_usuario_fim_prep||
					'; old.dt_cancelamento='	||:old.dt_cancelamento||
					'; new.dt_cancelamento='	||:new.dt_cancelamento||
					'; old.dt_checagem='		||:old.dt_checagem||
					'; new.dt_checagem='		||:new.dt_checagem||
					'; old.dt_devolucao='		||:old.dt_devolucao||
					'; new.dt_devolucao='		||:new.dt_devolucao||
					'; nr_sequencia='		||:new.nr_sequencia||
					'; funcao_ativa='		||to_char(wheb_usuario_pck.get_cd_funcao) ||
					'; usu�rio='			||:new.nm_usuario||
					'; perfil='			||to_char(wheb_usuario_pck.get_cd_perfil) ||
					'; Stack='			||substr(ds_origem_w,1,1000));
	exception
	when others then
		ds_origem_w	:= '';
	end;
end if;
*/

END;
/


CREATE OR REPLACE TRIGGER TASY.LOG_CAN_ORDEM_PROD
BEFORE INSERT OR UPDATE ON TASY.CAN_ORDEM_PROD FOR EACH ROW
DECLARE
BEGIN
	IF (:old.DT_ENTREGA_SETOR is null) and (:new.DT_ENTREGA_SETOR is not null) THEN

		gravar_log_tasy(2705,'NR_SEQ_ORDEM='||:new.NR_SEQUENCIA||' Usu�rio: '||OBTER_USUARIO_ATIVO||
				     ' Perfil: '||OBTER_PERFIL_ATIVO||
				     ' Fun��o: '||OBTER_FUNCAO_ATIVA||
				     ' Stack:  '||substr(dbms_utility.format_call_stack,1,4000),'ooliveira');
	END IF;
END;
/


ALTER TABLE TASY.CAN_ORDEM_PROD ADD (
  CONSTRAINT CANORPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CAN_ORDEM_PROD ADD (
  CONSTRAINT CANORPR_FARMODO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DUPL) 
 REFERENCES TASY.FAR_MOTIVO_DUPL_ORDEM (NR_SEQUENCIA),
  CONSTRAINT CANORPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CANORPR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_PACIENTE) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CANORPR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CANORPR_PESFISI_FK8 
 FOREIGN KEY (CD_PESSOA_DEVOL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CANORPR_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_ADMINISTRACAO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CANORPR_CANORPR_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_ORIGEM) 
 REFERENCES TASY.CAN_ORDEM_PROD (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CANORPR_PESFISI_FK6 
 FOREIGN KEY (CD_AUXILIAR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CANORPR_PESFISI_FK5 
 FOREIGN KEY (CD_MANIPULADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CANORPR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CANORPR_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CANORPR_PESFISI_FK3 
 FOREIGN KEY (CD_FARMACEUTICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CANORPR_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT CANORPR_FARETPR_FK 
 FOREIGN KEY (NR_SEQ_ETAPA_PROD) 
 REFERENCES TASY.FAR_ETAPA_PRODUCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CAN_ORDEM_PROD TO NIVEL_1;

