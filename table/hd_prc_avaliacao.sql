ALTER TABLE TASY.HD_PRC_AVALIACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_PRC_AVALIACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_PRC_AVALIACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DIALISE       NUMBER(10)               NOT NULL,
  DT_AVALIACAO         DATE                     NOT NULL,
  IE_PESO              VARCHAR2(1 BYTE)         NOT NULL,
  IE_PRESSAO           VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_UNID_DIALISE  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDPRCAV_HDDIALE_FK_I ON TASY.HD_PRC_AVALIACAO
(NR_SEQ_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDPRCAV_HDUNIDD_FK_I ON TASY.HD_PRC_AVALIACAO
(NR_SEQ_UNID_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPRCAV_HDUNIDD_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDPRCAV_PK ON TASY.HD_PRC_AVALIACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPRCAV_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hd_prc_avaliacao_befup_sign
before update ON TASY.HD_PRC_AVALIACAO for each row
declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dialise = :new.nr_seq_dialise
and 	ie_opcao = 'CP'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_sequencia		<> :new.nr_sequencia
		or :old.dt_atualizacao_nrec     <> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec         <> :new.nm_usuario_nrec
		or :old.nr_seq_dialise          <> :new.nr_seq_dialise
		or :old.dt_avaliacao            <> :new.dt_avaliacao
		or :old.ie_peso                 <> :new.ie_peso
		or :old.ie_pressao              <> :new.ie_pressao
		or :old.nr_seq_unid_dialise     <> :new.nr_seq_unid_dialise
		or (:old.nr_sequencia 		is null		and :new.nr_sequencia 		is not null)
		or (:old.dt_atualizacao_nrec 	is null		and :new.dt_atualizacao_nrec 	is not null)
		or (:old.nm_usuario_nrec 	is null		and :new.nm_usuario_nrec 	is not null)
		or (:old.nr_seq_dialise 	is null		and :new.nr_seq_dialise 	is not null)
		or (:old.dt_avaliacao 		is null		and :new.dt_avaliacao 		is not null)
		or (:old.ie_peso 		is null		and :new.ie_peso 		is not null)
		or (:old.ie_pressao 		is null		and :new.ie_pressao 		is not null)
		or (:old.nr_seq_unid_dialise 	is null		and :new.nr_seq_unid_dialise 	is not null)
		or (:old.nr_sequencia 		is not null	and :new.nr_sequencia 		is null)
		or (:old.dt_atualizacao_nrec 	is not null	and :new.dt_atualizacao_nrec 	is null)
		or (:old.nm_usuario_nrec 	is not null	and :new.nm_usuario_nrec 	is null)
		or (:old.nr_seq_dialise 	is not null	and :new.nr_seq_dialise 	is null)
		or (:old.dt_avaliacao 		is not null	and :new.dt_avaliacao 		is null)
		or (:old.ie_peso 		is not null	and :new.ie_peso 		is null)
		or (:old.ie_pressao 		is not null	and :new.ie_pressao 		is null)
		or (:old.nr_seq_unid_dialise 	is not null	and :new.nr_seq_unid_dialise 	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/


ALTER TABLE TASY.HD_PRC_AVALIACAO ADD (
  CONSTRAINT HDPRCAV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_PRC_AVALIACAO ADD (
  CONSTRAINT HDPRCAV_HDDIALE_FK 
 FOREIGN KEY (NR_SEQ_DIALISE) 
 REFERENCES TASY.HD_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDPRCAV_HDUNIDD_FK 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_PRC_AVALIACAO TO NIVEL_1;

