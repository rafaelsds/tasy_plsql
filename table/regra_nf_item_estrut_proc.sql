ALTER TABLE TASY.REGRA_NF_ITEM_ESTRUT_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_NF_ITEM_ESTRUT_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_NF_ITEM_ESTRUT_PROC
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_CONTA_NF_IT  NUMBER(10)          NOT NULL,
  CD_AREA_PROCED            NUMBER(15),
  CD_GRUPO_PROCED           NUMBER(15),
  CD_ESPECIAL_PROCED        NUMBER(15),
  CD_PROCEDIMENTO           NUMBER(15),
  IE_ORIGEM_PROCED          NUMBER(10),
  CD_TIPO_PROCEDIMENTO      NUMBER(3),
  NR_SEQ_GRUPO_LAB          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RCNFESP_AREPROC_FK_I ON TASY.REGRA_NF_ITEM_ESTRUT_PROC
(CD_AREA_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RCNFESP_ESPPROC_FK_I ON TASY.REGRA_NF_ITEM_ESTRUT_PROC
(CD_ESPECIAL_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RCNFESP_GRUEXLA_FK_I ON TASY.REGRA_NF_ITEM_ESTRUT_PROC
(NR_SEQ_GRUPO_LAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RCNFESP_GRUPROC_FK_I ON TASY.REGRA_NF_ITEM_ESTRUT_PROC
(CD_GRUPO_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RCNFESP_PK ON TASY.REGRA_NF_ITEM_ESTRUT_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RCNFESP_PROCEDI_FK_I ON TASY.REGRA_NF_ITEM_ESTRUT_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RCNFESP_REGCNFI_FK_I ON TASY.REGRA_NF_ITEM_ESTRUT_PROC
(NR_SEQ_REGRA_CONTA_NF_IT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_NF_ITEM_ESTRUT_PROC ADD (
  CONSTRAINT RCNFESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_NF_ITEM_ESTRUT_PROC ADD (
  CONSTRAINT RCNFESP_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCED) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT RCNFESP_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIAL_PROCED) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT RCNFESP_GRUEXLA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_LAB) 
 REFERENCES TASY.GRUPO_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT RCNFESP_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROCED) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT RCNFESP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT RCNFESP_REGCNFI_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONTA_NF_IT) 
 REFERENCES TASY.REGRA_CONTA_NF_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_NF_ITEM_ESTRUT_PROC TO NIVEL_1;

