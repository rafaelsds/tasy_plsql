ALTER TABLE TASY.PROTOCOLO_MEDICACAO_ACESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_MEDICACAO_ACESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_MEDICACAO_ACESSO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_MEDICACAO     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ACESSO        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROMEAC_PK ON TASY.PROTOCOLO_MEDICACAO_ACESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROMEAC_PROMEDI_FK_I ON TASY.PROTOCOLO_MEDICACAO_ACESSO
(NR_SEQ_MEDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROMEAC_QTACESS_FK_I ON TASY.PROTOCOLO_MEDICACAO_ACESSO
(NR_SEQ_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROMEAC_QTACESS_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROTOCOLO_MEDICACAO_ACESSO ADD (
  CONSTRAINT PROMEAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROTOCOLO_MEDICACAO_ACESSO ADD (
  CONSTRAINT PROMEAC_PROMEDI_FK 
 FOREIGN KEY (NR_SEQ_MEDICACAO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (NR_SEQ_INTERNA),
  CONSTRAINT PROMEAC_QTACESS_FK 
 FOREIGN KEY (NR_SEQ_ACESSO) 
 REFERENCES TASY.QT_ACESSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROTOCOLO_MEDICACAO_ACESSO TO NIVEL_1;

