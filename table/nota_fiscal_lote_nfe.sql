ALTER TABLE TASY.NOTA_FISCAL_LOTE_NFE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL_LOTE_NFE CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL_LOTE_NFE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_GERACAO           DATE                     NOT NULL,
  DT_ENVIO             DATE,
  DS_MENSAGEM_ENVIO    LONG,
  DT_RETORNO           DATE,
  DS_MENSAGEM_RETORNO  VARCHAR2(4000 BYTE),
  NR_PROTOCOLO         VARCHAR2(80 BYTE),
  IE_STATUS_NFE        VARCHAR2(15 BYTE),
  NR_SEQ_TRANSMISSAO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NFLONFE_ESTABEL_FK_I ON TASY.NOTA_FISCAL_LOTE_NFE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFLONFE_NFE_TRANS_FK_I ON TASY.NOTA_FISCAL_LOTE_NFE
(NR_SEQ_TRANSMISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NFLONFE_PK ON TASY.NOTA_FISCAL_LOTE_NFE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.NOTA_FISCAL_LOTE_NFE_tp  after update ON TASY.NOTA_FISCAL_LOTE_NFE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ENVIO,1,4000),substr(:new.DT_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ENVIO',ie_log_w,ds_w,'NOTA_FISCAL_LOTE_NFE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PROTOCOLO,1,4000),substr(:new.NR_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'NR_PROTOCOLO',ie_log_w,ds_w,'NOTA_FISCAL_LOTE_NFE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MENSAGEM_RETORNO,1,4000),substr(:new.DS_MENSAGEM_RETORNO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MENSAGEM_RETORNO',ie_log_w,ds_w,'NOTA_FISCAL_LOTE_NFE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_GERACAO,1,4000),substr(:new.DT_GERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_GERACAO',ie_log_w,ds_w,'NOTA_FISCAL_LOTE_NFE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_lote_nfe_update
before update ON TASY.NOTA_FISCAL_LOTE_NFE for each row
declare

ds_param_integ_hl7_w	varchar2(255);
qt_registro_w		number(10);
nr_seq_transmissao_w	nfe_transmissao.nr_sequencia%type;
nr_seq_lote_w		nota_fiscal_lote_nfe.nr_sequencia%type;
qt_espera_consulta_w	w_nfse_envio_automatico.qt_espera_consulta%type := 0;
ie_envio_aut_nfse_w	parametro_compras.ie_envio_aut_nfse%type := 'N';

begin

if	(updating) and (:new.nr_protocolo is not null) and (:old.nr_protocolo is null) then
	begin

	select	count(1)
	into	qt_registro_w
	from	parametro_compras
	where	cd_estabelecimento = :new.cd_estabelecimento;

	if	(qt_registro_w > 0) then
		begin

		select	nvl(max(ie_envio_aut_nfse),'N')
		into	ie_envio_aut_nfse_w
		from	parametro_compras
		where	cd_estabelecimento = :new.cd_estabelecimento;

		end;
	end if;


	if	(ie_envio_aut_nfse_w = 'S') then
		begin

		select	count(1)
		into	qt_registro_w
		from	w_nfse_envio_automatico
		where	nr_seq_transmissao = :new.nr_seq_transmissao;

		if	(qt_registro_w > 0) then
			begin

			select	nvl(max(qt_espera_consulta),0)
			into	qt_espera_consulta_w
			from	w_nfse_envio_automatico
			where	nr_seq_transmissao = :new.nr_seq_transmissao;

			end;
		end if;

		gerar_transmissao_nf(:new.nm_usuario,3,'NFSE',nr_seq_transmissao_w);

		gravar_nf_transmissao_consulta(nr_seq_transmissao_w,:new.nr_seq_transmissao,:new.nm_usuario);

		select	max(nr_sequencia)
		into	nr_seq_lote_w
		from	nota_fiscal_lote_nfe
		where	nr_seq_transmissao = :new.nr_seq_transmissao;


		ds_param_integ_hl7_w := 'NR_SEQ_TRANSMISSAO=' || nr_seq_transmissao_w || ';CD_ESTABELECIMENTO='|| :new.cd_estabelecimento ||';NM_USUARIO='|| :new.nm_usuario || ';LOTE_NFSE=' || nr_seq_lote_w || ';CD_PERFIL=' || obter_perfil_ativo || ';'
					|| 'QT_ESPERA_CONSULTA=' || qt_espera_consulta_w || ';';
		gravar_agend_integracao(532, ds_param_integ_hl7_w);

		end;
	end if;


	end;
end if;

end;
/


ALTER TABLE TASY.NOTA_FISCAL_LOTE_NFE ADD (
  CONSTRAINT NFLONFE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL_LOTE_NFE ADD (
  CONSTRAINT NFLONFE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT NFLONFE_NFE_TRANS_FK 
 FOREIGN KEY (NR_SEQ_TRANSMISSAO) 
 REFERENCES TASY.NFE_TRANSMISSAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.NOTA_FISCAL_LOTE_NFE TO NIVEL_1;

