ALTER TABLE TASY.PLS_CTA_REGRA_BLOQ_ACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CTA_REGRA_BLOQ_ACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CTA_REGRA_BLOQ_ACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE,
  DT_FIM_VIGENCIA_REF  DATE,
  IE_TIPO_DATA         VARCHAR2(10 BYTE),
  IE_BLOQ_EXCLUSAO     VARCHAR2(5 BYTE),
  IE_BLOQ_REABERTURA   VARCHAR2(5 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4),
  IE_DATA_REFERENCIA   VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCRBA_ESTABEL_FK_I ON TASY.PLS_CTA_REGRA_BLOQ_ACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCRBA_PK ON TASY.PLS_CTA_REGRA_BLOQ_ACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_cta_regra_bloq_acao_atual
before update or insert ON TASY.PLS_CTA_REGRA_BLOQ_ACAO for each row
declare

begin

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899 desta forma podemos utilizar um between ou fazer uma compara��o com este campo
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_cta_regra_bloq_acao_atual;
/


ALTER TABLE TASY.PLS_CTA_REGRA_BLOQ_ACAO ADD (
  CONSTRAINT PLSCRBA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CTA_REGRA_BLOQ_ACAO ADD (
  CONSTRAINT PLSCRBA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_CTA_REGRA_BLOQ_ACAO TO NIVEL_1;

