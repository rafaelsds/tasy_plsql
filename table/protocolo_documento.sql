ALTER TABLE TASY.PROTOCOLO_DOCUMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_DOCUMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_DOCUMENTO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ENVIO                 DATE,
  NM_USUARIO_ENVIO         VARCHAR2(15 BYTE),
  IE_TIPO_PROTOCOLO        VARCHAR2(3 BYTE),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_PESSOA_ORIGEM         VARCHAR2(10 BYTE),
  CD_SETOR_ORIGEM          NUMBER(5),
  CD_PESSOA_DESTINO        VARCHAR2(10 BYTE),
  CD_SETOR_DESTINO         NUMBER(5),
  CD_CGC_DESTINO           VARCHAR2(14 BYTE),
  DT_REC_DESTINO           DATE,
  NM_USUARIO_RECEB         VARCHAR2(15 BYTE),
  DS_OBS                   VARCHAR2(4000 BYTE),
  DT_FECHAMENTO            DATE,
  DT_CANCELAMENTO          DATE,
  NM_USUARIO_CANC          VARCHAR2(15 BYTE),
  IE_FORMA_ARMAZENAMENTO   VARCHAR2(15 BYTE),
  DS_FORMA_RECUPERACAO     VARCHAR2(255 BYTE),
  DS_FORMA_PROTECAO        VARCHAR2(255 BYTE),
  IE_FORMA_DESCARTE        VARCHAR2(15 BYTE),
  QT_TEMPO_RETENCAO        NUMBER(4,2),
  IE_TEMPO_RETENCAO        VARCHAR2(15 BYTE),
  DT_DESCARTE              DATE,
  NM_USUARIO_DESCARTE      VARCHAR2(15 BYTE),
  IE_FORMA_DESCARTE_REAL   VARCHAR2(15 BYTE),
  DS_CONTROLE              VARCHAR2(80 BYTE),
  DT_DEVOLUCAO             DATE,
  NR_SEQ_PROTOCOLO_DEV     NUMBER(10),
  CD_ESTABELECIMENTO       NUMBER(4),
  IE_STATUS_PROTOCOLO      VARCHAR2(2 BYTE),
  NR_SEQ_TIPO_DOC_ITEM     NUMBER(10),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_DESTINO_REMESSA       VARCHAR2(3 BYTE),
  NR_SEQ_PROTOCOLO_ORIGEM  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRODOCU_ESTABEL_FK_I ON TASY.PROTOCOLO_DOCUMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOCU_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRODOCU_I1 ON TASY.PROTOCOLO_DOCUMENTO
(IE_TIPO_PROTOCOLO, CD_SETOR_ORIGEM)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_I2 ON TASY.PROTOCOLO_DOCUMENTO
(DT_ENVIO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_I3 ON TASY.PROTOCOLO_DOCUMENTO
(DT_REC_DESTINO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_I4 ON TASY.PROTOCOLO_DOCUMENTO
(DT_FECHAMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_I5 ON TASY.PROTOCOLO_DOCUMENTO
(DT_DESCARTE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_I6 ON TASY.PROTOCOLO_DOCUMENTO
(DT_ATUALIZACAO_NREC)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_I7 ON TASY.PROTOCOLO_DOCUMENTO
(IE_TIPO_PROTOCOLO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_PESFISI_FK_I ON TASY.PROTOCOLO_DOCUMENTO
(CD_PESSOA_ORIGEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_PESFISI_FK_I2 ON TASY.PROTOCOLO_DOCUMENTO
(CD_PESSOA_DESTINO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_PESJURI_FK_I ON TASY.PROTOCOLO_DOCUMENTO
(CD_CGC_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOCU_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRODOCU_PK ON TASY.PROTOCOLO_DOCUMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_SETATEN_FK_I ON TASY.PROTOCOLO_DOCUMENTO
(CD_SETOR_ORIGEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_SETATEN_FK_I2 ON TASY.PROTOCOLO_DOCUMENTO
(CD_SETOR_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOCU_TPDOCIT_FK_I ON TASY.PROTOCOLO_DOCUMENTO
(NR_SEQ_TIPO_DOC_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOCU_TPDOCIT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.protocolo_documento_atual
BEFORE UPDATE OR INSERT ON protocolo_documento
FOR EACH ROW
DECLARE
qt_reg_w	number(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.dt_rec_destino	is not null) 	and
	(:old.dt_rec_destino  	is null)    	and
	(:new.nm_usuario_receb	is null)	then
	:new.nm_usuario_receb 	:= :new.nm_usuario;
end if;
<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.PROTOCOLO_DOCUMENTO_tp  after update ON TASY.PROTOCOLO_DOCUMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_CGC_DESTINO,1,4000),substr(:new.CD_CGC_DESTINO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_DESTINO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS_PROTOCOLO,1,4000),substr(:new.IE_STATUS_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_PROTOCOLO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBS,1,4000),substr(:new.DS_OBS,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBS',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ENVIO,1,4000),substr(:new.DT_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ENVIO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_ORIGEM,1,4000),substr(:new.CD_PESSOA_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_ORIGEM',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_DESTINO,1,4000),substr(:new.CD_PESSOA_DESTINO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_DESTINO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ORIGEM,1,4000),substr(:new.CD_SETOR_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ORIGEM',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_DESTINO,1,4000),substr(:new.CD_SETOR_DESTINO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_DESTINO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_REC_DESTINO,1,4000),substr(:new.DT_REC_DESTINO,1,4000),:new.nm_usuario,nr_seq_w,'DT_REC_DESTINO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_ENVIO,1,4000),substr(:new.NM_USUARIO_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_ENVIO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_RECEB,1,4000),substr(:new.NM_USUARIO_RECEB,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_RECEB',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FECHAMENTO,1,4000),substr(:new.DT_FECHAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_FECHAMENTO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CANCELAMENTO,1,4000),substr(:new.DT_CANCELAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_CANCELAMENTO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_CANC,1,4000),substr(:new.NM_USUARIO_CANC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_CANC',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_DESCARTE,1,4000),substr(:new.NM_USUARIO_DESCARTE,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_DESCARTE',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_DESCARTE_REAL,1,4000),substr(:new.IE_FORMA_DESCARTE_REAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_DESCARTE_REAL',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_ARMAZENAMENTO,1,4000),substr(:new.IE_FORMA_ARMAZENAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_ARMAZENAMENTO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FORMA_RECUPERACAO,1,4000),substr(:new.DS_FORMA_RECUPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_FORMA_RECUPERACAO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FORMA_PROTECAO,1,4000),substr(:new.DS_FORMA_PROTECAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_FORMA_PROTECAO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_DESCARTE,1,4000),substr(:new.IE_FORMA_DESCARTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_DESCARTE',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TEMPO_RETENCAO,1,4000),substr(:new.QT_TEMPO_RETENCAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_TEMPO_RETENCAO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TEMPO_RETENCAO,1,4000),substr(:new.IE_TEMPO_RETENCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TEMPO_RETENCAO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONTROLE,1,4000),substr(:new.DS_CONTROLE,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONTROLE',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PROTOCOLO,1,4000),substr(:new.IE_TIPO_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PROTOCOLO',ie_log_w,ds_w,'PROTOCOLO_DOCUMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PROTOCOLO_DOCUMENTO ADD (
  CONSTRAINT PRODOCU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROTOCOLO_DOCUMENTO ADD (
  CONSTRAINT PRODOCU_TPDOCIT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOC_ITEM) 
 REFERENCES TASY.TIPO_DOC_ITEM (NR_SEQUENCIA),
  CONSTRAINT PRODOCU_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_DESTINO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRODOCU_PESJURI_FK 
 FOREIGN KEY (CD_CGC_DESTINO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PRODOCU_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_ORIGEM) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRODOCU_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_DESTINO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRODOCU_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ORIGEM) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRODOCU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PROTOCOLO_DOCUMENTO TO NIVEL_1;

