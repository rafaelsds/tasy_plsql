DROP TABLE TASY.W_TOT_GASTO_PROJ_REC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_TOT_GASTO_PROJ_REC
(
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  IE_TRANSACAO        VARCHAR2(2 BYTE),
  VL_TRANSACAO        NUMBER(22,4),
  NR_SEQ_PROJ_REC     NUMBER(10),
  DS_TRANSACAO        VARCHAR2(255 BYTE),
  QT_TRANSACAO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WTOGAPR_ESTABEL_FK_I ON TASY.W_TOT_GASTO_PROJ_REC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WTOGAPR_PRORECU_FK_I ON TASY.W_TOT_GASTO_PROJ_REC
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.w_tot_gas_proj_rec_bf_ins_upd
before insert or update ON TASY.W_TOT_GASTO_PROJ_REC for each row
begin

    if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
        wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
    end if;

end;
/


ALTER TABLE TASY.W_TOT_GASTO_PROJ_REC ADD (
  CONSTRAINT WTOGAPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT WTOGAPR_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_TOT_GASTO_PROJ_REC TO NIVEL_1;

