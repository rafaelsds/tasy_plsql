ALTER TABLE TASY.REGRA_GERACAO_OS_SD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_GERACAO_OS_SD CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_GERACAO_OS_SD
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOCALIZACAO   NUMBER(10)               NOT NULL,
  NR_SEQ_EQUIPAMENTO   NUMBER(10)               NOT NULL,
  IE_CLASSIFICACAO     VARCHAR2(1 BYTE)         NOT NULL,
  IE_STATUS_ORDEM      VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_ESTAGIO       NUMBER(10)               NOT NULL,
  NR_GRUPO_TRABALHO    NUMBER(10),
  NR_GRUPO_PLANEJ      NUMBER(10),
  DS_DANO_BREVE        VARCHAR2(80 BYTE)        NOT NULL,
  DS_DANO              VARCHAR2(4000 BYTE)      NOT NULL,
  IE_TIPO_ORDEM        NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGEOSSD_MAESTPR_FK_I ON TASY.REGRA_GERACAO_OS_SD
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGEOSSD_MANEQUI_FK_I ON TASY.REGRA_GERACAO_OS_SD
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGEOSSD_MANGRPL_FK_I ON TASY.REGRA_GERACAO_OS_SD
(NR_GRUPO_PLANEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGEOSSD_MANGRTR_FK_I ON TASY.REGRA_GERACAO_OS_SD
(NR_GRUPO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGEOSSD_MANLOCA_FK_I ON TASY.REGRA_GERACAO_OS_SD
(NR_SEQ_LOCALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGEOSSD_PK ON TASY.REGRA_GERACAO_OS_SD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_GERACAO_OS_SD ADD (
  CONSTRAINT REGEOSSD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_GERACAO_OS_SD ADD (
  CONSTRAINT REGEOSSD_MAESTPR_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.MAN_ESTAGIO_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT REGEOSSD_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT REGEOSSD_MANGRPL_FK 
 FOREIGN KEY (NR_GRUPO_PLANEJ) 
 REFERENCES TASY.MAN_GRUPO_PLANEJAMENTO (NR_SEQUENCIA),
  CONSTRAINT REGEOSSD_MANGRTR_FK 
 FOREIGN KEY (NR_GRUPO_TRABALHO) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT REGEOSSD_MANLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO) 
 REFERENCES TASY.MAN_LOCALIZACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_GERACAO_OS_SD TO NIVEL_1;

