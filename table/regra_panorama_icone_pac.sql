ALTER TABLE TASY.REGRA_PANORAMA_ICONE_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PANORAMA_ICONE_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PANORAMA_ICONE_PAC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_REGRA          NUMBER(10),
  CD_ICONE_PANORAMA     NUMBER(10),
  IE_IMAGEM             LONG RAW,
  DS_ICONE_INSTITUICAO  VARCHAR2(255 BYTE),
  NR_SEQ_PRIORIDADE     NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REPICOP_PK ON TASY.REGRA_PANORAMA_ICONE_PAC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPICOP_REPERPA_FK_I ON TASY.REGRA_PANORAMA_ICONE_PAC
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_PANORAMA_ICONE_PAC ADD (
  CONSTRAINT REPICOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REGRA_PANORAMA_ICONE_PAC ADD (
  CONSTRAINT REPICOP_REPERPA_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_PERM_PANORAMA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_PANORAMA_ICONE_PAC TO NIVEL_1;

