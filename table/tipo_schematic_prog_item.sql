ALTER TABLE TASY.TIPO_SCHEMATIC_PROG_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_SCHEMATIC_PROG_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_SCHEMATIC_PROG_ITEM
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_SCHEM_PROGR  NUMBER(10)           NOT NULL,
  DS_RESUMO                VARCHAR2(255 BYTE)   NOT NULL,
  DS_HISTORICO             VARCHAR2(4000 BYTE)  NOT NULL,
  QT_MIN_PADRAO            NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TPSCPRIT_PK ON TASY.TIPO_SCHEMATIC_PROG_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TPSCPRIT_TPSCHEPR_FK_I ON TASY.TIPO_SCHEMATIC_PROG_ITEM
(NR_SEQ_TIPO_SCHEM_PROGR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_SCHEMATIC_PROG_ITEM ADD (
  CONSTRAINT TPSCPRIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_SCHEMATIC_PROG_ITEM ADD (
  CONSTRAINT TPSCPRIT_TPSCHEPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_SCHEM_PROGR) 
 REFERENCES TASY.TIPO_SCHEMATIC_PROG (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TIPO_SCHEMATIC_PROG_ITEM TO NIVEL_1;

