DROP TABLE TASY.WOCUPACAO_SAIDA_TEMPORARIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.WOCUPACAO_SAIDA_TEMPORARIA
(
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_ATENDIMENTO                NUMBER(10)      NOT NULL,
  DT_FINAL_JEJUM                DATE,
  DT_FINAL_SAIDA                DATE,
  IE_SEM_JEJUM                  VARCHAR2(1 BYTE) NOT NULL,
  DT_INICIO_JEJUM               DATE,
  DT_INICIO_SAIDA               DATE            NOT NULL,
  NR_SEQUENCIA_DIETA            NUMBER(10),
  NR_INFORMACAO_CONTATO         NUMBER(10),
  NR_DESTINO                    NUMBER(10),
  NR_MOTIVO_TEMPORARIO          NUMBER(10),
  NR_CATEGORIA_REFEICAO_INICIO  NUMBER(10),
  NR_CATEGORIA_REFEICAO_FIM     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WOCUPSTEMP_ATEPACI_FK_I ON TASY.WOCUPACAO_SAIDA_TEMPORARIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.wocupacao_saida_temp_before
before insert or update or delete ON TASY.WOCUPACAO_SAIDA_TEMPORARIA for each row
declare
dt_inicio_w        cpoe_dieta.dt_inicio%type;
hr_prim_horario_w  cpoe_dieta.hr_prim_horario%type;
nr_sequencia_w     cpoe_dieta.nr_sequencia%type;
cd_pessoa_fisica_w atendimento_paciente.cd_pessoa_fisica%type;
nm_usuario_w       varchar2(15);
begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
  if (inserting) then
    :new.nm_usuario := obter_usuario_ativo;
    :new.dt_atualizacao := sysdate;

    if :new.nr_atendimento is not null then
	  update atend_paciente_unidade
	     set dt_saida_temporaria = :NEW.Dt_Inicio_Saida,
		 dt_retorno_saida_temporaria = null,
		 nm_usuario = nvl(obter_usuario_ativo,nm_usuario),
		 dt_atualizacao = sysdate
	   where nr_atendimento = :new.nr_atendimento;
    end if;

    if (:new.ie_sem_jejum = 'N' and :new.dt_inicio_jejum is not null) then
      dt_inicio_w := trunc(:new.dt_inicio_jejum,'hh24') + 1/24;
      hr_prim_horario_w  := substr(cpoe_obter_primeiro_horario( :new.nr_atendimento, null, null, null, null, null, null),1,5);

	  if (hr_prim_horario_w is not null) then
	    dt_inicio_w	:= to_date(to_char(dt_inicio_w,'dd/mm/yyyy')  || hr_prim_horario_w, 'dd/mm/yyyy hh24:mi');
	  end if;

      cd_pessoa_fisica_w := obter_cd_pes_fis_atend(:new.nr_atendimento);

       select cpoe_dieta_seq.nextval
	 into	nr_sequencia_w
	 from	dual;

      :new.nr_sequencia_dieta := nr_sequencia_w;

      insert into CPOE_DIETA
      (nr_sequencia,
       nr_atendimento,
       ie_tipo_dieta,
       nr_seq_tipo,
       ie_se_necessario,
       ds_observacao,
       dt_inicio,
       dt_fim,
       ie_duracao,
       ie_administracao,
       dt_prim_horario,
       nm_usuario,
       dt_atualizacao,
       cd_pessoa_fisica,
       dt_liberacao)
      values
      (nr_sequencia_w,
       :NEW.nr_atendimento,
       'J',
       :NEW.nr_categoria_refeicao_inicio,
       'N',
       null,
       :NEW.dt_inicio_jejum,
       :NEW.dt_final_jejum,
       'P',
       'P',
       dt_inicio_w,
       obter_usuario_ativo,
       sysdate,
       cd_pessoa_fisica_w,
      :NEW.dt_inicio_jejum);

    end if;
  end if;

  if (updating) then
    if :new.dt_inicio_saida <> :old.dt_inicio_saida then
	  update atend_paciente_unidade
	     set dt_saida_temporaria = :NEW.dt_inicio_saida,
		 dt_retorno_saida_temporaria = null,
		 nm_usuario = nvl(obter_usuario_ativo,nm_usuario),
		 dt_atualizacao = sysdate
	   where nr_atendimento = :new.nr_atendimento;
    end if;

    if (:old.dt_final_saida is null and :new.dt_final_saida is not null) then
      if :old.nr_sequencia_dieta is not null then
        update CPOE_DIETA
         set dt_suspensao = sysdate,
           dt_lib_suspensao = sysdate
        where nr_sequencia = :old.nr_sequencia_dieta;
      end if;
    end if;

    if (:new.ie_sem_jejum = 'N' and
       (:new.dt_inicio_jejum <> :old.dt_inicio_jejum or
       :new.nr_categoria_refeicao_inicio <> :old.nr_categoria_refeicao_inicio or
       :new.nr_categoria_refeicao_fim <> :old.nr_categoria_refeicao_fim or
       :new.dt_final_jejum <> :old.dt_final_jejum)) or
       (:new.ie_sem_jejum = 'N' and :old.ie_sem_jejum = 'S') then

       update CPOE_DIETA
       set dt_suspensao = sysdate,
           dt_lib_suspensao = sysdate
       where nr_sequencia = :old.nr_sequencia_dieta;

       if (:new.ie_sem_jejum = 'N' and :new.dt_inicio_jejum is not null) then
	     dt_inicio_w := trunc(:new.dt_inicio_jejum,'hh24') + 1/24;
	     hr_prim_horario_w  := substr(cpoe_obter_primeiro_horario( :new.nr_atendimento, null, null, null, null, null, null),1,5);

	     if (hr_prim_horario_w is not null) then
	       dt_inicio_w	:= to_date(to_char(dt_inicio_w,'dd/mm/yyyy')  || hr_prim_horario_w, 'dd/mm/yyyy hh24:mi');
	     end if;

        cd_pessoa_fisica_w := obter_cd_pes_fis_atend(:new.nr_atendimento);

	select	cpoe_dieta_seq.nextval
	  into	nr_sequencia_w
	  from	dual;

        :new.nr_sequencia_dieta := nr_sequencia_w;

        insert into CPOE_DIETA
        (nr_sequencia,
         nr_atendimento,
         ie_tipo_dieta,
         nr_seq_tipo,
         ie_se_necessario,
         ds_observacao,
         dt_inicio,
         dt_fim,
         ie_duracao,
         ie_administracao,
         dt_prim_horario,
         nm_usuario,
         dt_atualizacao,
         cd_pessoa_fisica,
         dt_liberacao)
        values
        (nr_sequencia_w,
         :OLD.nr_atendimento,
         'J',
         :NEW.nr_categoria_refeicao_inicio,
         'N',
         null,
         :NEW.dt_inicio_jejum,
         :NEW.dt_final_jejum,
         'P',
         'P',
         dt_inicio_w,
         obter_usuario_ativo,
         sysdate,
         cd_pessoa_fisica_w,
         :NEW.dt_inicio_jejum);

       end if;
    end if;

    if :new.ie_sem_jejum = 'S' then
      if :old.nr_sequencia_dieta is not null then
        update CPOE_DIETA
        set dt_suspensao = sysdate,
            dt_lib_suspensao = sysdate
        where nr_sequencia = :old.nr_sequencia_dieta;

        :new.nr_sequencia_dieta := null;
      end if;
    end if;

  end if;

  if (deleting) then
	update atend_paciente_unidade
	   set dt_saida_temporaria = null,
	       dt_retorno_saida_temporaria = null,
		   nm_usuario = nvl(obter_usuario_ativo,nm_usuario),
		   dt_atualizacao = sysdate
	where nr_atendimento = :old.nr_atendimento;

    if :old.nr_sequencia_dieta is not null then
      update CPOE_DIETA
      set dt_suspensao = sysdate,
          dt_lib_suspensao = sysdate
      where nr_sequencia = :old.nr_sequencia_dieta;
    end if;
  end if;
end if;
end;
/


ALTER TABLE TASY.WOCUPACAO_SAIDA_TEMPORARIA ADD (
  CONSTRAINT WOCUPSTEMP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

