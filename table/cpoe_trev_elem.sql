ALTER TABLE TASY.CPOE_TREV_ELEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_TREV_ELEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_TREV_ELEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CPOE_TREV     NUMBER(15),
  NR_SEQ_ELEMENTO      NUMBER(10)               NOT NULL,
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE)        NOT NULL,
  QT_ELEM_KG_DIA       NUMBER(15,4)             NOT NULL,
  QT_DIARIA            NUMBER(15,4)             NOT NULL,
  QT_VOLUME            NUMBER(15,4)             NOT NULL,
  QT_VOLUME_CORRIGIDO  NUMBER(15,4)             NOT NULL,
  QT_VOLUME_ETAPA      NUMBER(15,4)             NOT NULL,
  IE_UNIDADE_PED_CALC  VARCHAR2(1 BYTE),
  CD_UNIDADE_MED_ELEM  VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPTREL_CPTREV_FK_I ON TASY.CPOE_TREV_ELEM
(NR_SEQ_CPOE_TREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPTREL_NUTELEM_FK_I ON TASY.CPOE_TREV_ELEM
(NR_SEQ_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPTREL_PK ON TASY.CPOE_TREV_ELEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPTREL_UNIMEDI_FK_I ON TASY.CPOE_TREV_ELEM
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cpoe_trev_elem_ins_upd_after
after insert or update ON TASY.CPOE_TREV_ELEM for each row
declare

ds_stack_w		varchar2(2000 char);
ds_log_cpoe_w	varchar2(2000 char);

begin
	if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

		if	(nvl(:new.cd_unidade_med_elem,'XPTO') <> nvl(:old.cd_unidade_med_elem,'XPTO')) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' cd_unidade_med_elem(' || nvl(:old.cd_unidade_med_elem,'<NULL>') || '/' || nvl(:new.cd_unidade_med_elem,'<NULL>')||'); ',1,2000);
		end if;

		if	(nvl(:new.cd_unidade_medida,'XPTO') <> nvl(:old.cd_unidade_medida,'XPTO')) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' cd_unidade_medida(' || nvl(:old.cd_unidade_medida,'<NULL>') || '/' || nvl(:new.cd_unidade_medida,'<NULL>')||'); ',1,2000);
		end if;

		if	(nvl(:new.ie_unidade_ped_calc,'XPTO') <> nvl(:old.ie_unidade_ped_calc,'XPTO')) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' ie_unidade_ped_calc(' || nvl(:old.ie_unidade_ped_calc,'<NULL>') || '/' || nvl(:new.ie_unidade_ped_calc,'<NULL>')||'); ',1,2000);
		end if;

		if	(nvl(:new.nr_seq_cpoe_trev,0) <> nvl(:old.nr_seq_cpoe_trev,0)) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' nr_seq_cpoe_trev(' || nvl(:old.nr_seq_cpoe_trev,0) || '/' || nvl(:new.nr_seq_cpoe_trev,0)||'); ',1,2000);
		end if;

		if	(nvl(:new.nr_seq_elemento,0) <> nvl(:old.nr_seq_elemento,0)) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' nr_seq_elemento(' || nvl(:old.nr_seq_elemento,0) || '/' || nvl(:new.nr_seq_elemento,0)||'); ',1,2000);
		end if;

		if	(nvl(:new.qt_diaria,0) <> nvl(:old.qt_diaria,0)) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' qt_diaria(' || nvl(:old.qt_diaria,0) || '/' || nvl(:new.qt_diaria,0)||'); ',1,2000);
		end if;

		if	(nvl(:new.qt_elem_kg_dia,0) <> nvl(:old.qt_elem_kg_dia,0)) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' qt_elem_kg_dia(' || nvl(:old.qt_elem_kg_dia,0) || '/' || nvl(:new.qt_elem_kg_dia,0)||'); ',1,2000);
		end if;

		if	(nvl(:new.qt_volume,0) <> nvl(:old.qt_volume,0)) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' qt_volume(' || nvl(:old.qt_volume,0) || '/' || nvl(:new.qt_volume,0)||'); ',1,2000);
		end if;

		if	(nvl(:new.qt_volume_corrigido,0) <> nvl(:old.qt_volume_corrigido,0)) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' qt_volume_corrigido(' || nvl(:old.qt_volume_corrigido,0) || '/' || nvl(:new.qt_volume_corrigido,0)||'); ',1,2000);
		end if;

		if	(nvl(:new.qt_volume_etapa,0) <> nvl(:old.qt_volume_etapa,0)) then
			ds_log_cpoe_w := substr(ds_log_cpoe_w || ' qt_volume_etapa(' || nvl(:old.qt_volume_etapa,0) || '/' || nvl(:new.qt_volume_etapa,0)||'); ',1,2000);
		end if;

		if	(ds_log_cpoe_w is not null) then
			if (nvl(:old.nr_sequencia, 0) > 0) then
				ds_log_cpoe_w := substr('CPOE_TREV_ELEM Alteracoes(old/new)= NR_SEQUENCIA: ' || :new.nr_sequencia || ds_log_cpoe_w,1,2000);
			else
				ds_log_cpoe_w := substr('CPOE_TREV_ELEM Criacao(old/new)= NR_SEQUENCIA: ' || :new.nr_sequencia || ds_log_cpoe_w,1,2000);
			end if;

			ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
			ds_log_cpoe_w := substr(ds_log_cpoe_w ||' FUNCAO('||to_char(obter_funcao_ativa)||'); PERFIL('||to_char(obter_perfil_ativo)||')',1,2000);

			insert into log_cpoe(nr_sequencia, dt_atualizacao, nm_usuario, ds_log, ds_stack) values (log_cpoe_seq.nextval, sysdate, :new.nm_usuario, ds_log_cpoe_w, ds_stack_w);
		end if;

	end if;

exception
	when others then
		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);

		insert into log_cpoe(nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							ds_log,
							ds_stack)
		values (			log_cpoe_seq.nextval,
							sysdate,
							:new.nm_usuario,
							'EXCEPTION CPOE_TREV_ELEM_INS_UPD_AFTER',
							ds_stack_w);
end;
/


ALTER TABLE TASY.CPOE_TREV_ELEM ADD (
  CONSTRAINT CPTREL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_TREV_ELEM ADD (
  CONSTRAINT CPTREL_CPTREV_FK 
 FOREIGN KEY (NR_SEQ_CPOE_TREV) 
 REFERENCES TASY.CPOE_TREV (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CPTREL_NUTELEM_FK 
 FOREIGN KEY (NR_SEQ_ELEMENTO) 
 REFERENCES TASY.NUT_ELEMENTO (NR_SEQUENCIA),
  CONSTRAINT CPTREL_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.CPOE_TREV_ELEM TO NIVEL_1;

