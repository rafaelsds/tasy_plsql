DROP TABLE TASY.W_REL_RESSUP_CONS_DIARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_REL_RESSUP_CONS_DIARIO
(
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  QT_CONSUMO                NUMBER(15,4),
  QT_MEDIA_CONSUMO          NUMBER(15,4),
  QT_ESTOQUE                NUMBER(15,4),
  QT_COMPRAR                NUMBER(15,4),
  CD_UNIDADE_MEDIDA_COMPRA  VARCHAR2(30 BYTE),
  QT_COMPRA_CONVERTIDA      NUMBER(15,4),
  QT_DIAS_CONSUMO           NUMBER(4),
  QT_DIAS_ESTOQUE           NUMBER(4),
  CD_MATERIAL               NUMBER(10)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WRELRCD_ESTABEL_FK_I ON TASY.W_REL_RESSUP_CONS_DIARIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRELRCD_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WRELRCD_MATERIA_FK_I ON TASY.W_REL_RESSUP_CONS_DIARIO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRELRCD_MATERIA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_REL_RESSUP_CONS_DIARIO ADD (
  CONSTRAINT WRELRCD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT WRELRCD_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.W_REL_RESSUP_CONS_DIARIO TO NIVEL_1;

