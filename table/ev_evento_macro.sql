ALTER TABLE TASY.EV_EVENTO_MACRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EV_EVENTO_MACRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.EV_EVENTO_MACRO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DS_TITULO            VARCHAR2(80 BYTE)        NOT NULL,
  DS_DESCRICAO         VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_CAT_MACRO     NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EVEVMAC_EVECMAC_FK_I ON TASY.EV_EVENTO_MACRO
(NR_SEQ_CAT_MACRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EVEVMAC_PK ON TASY.EV_EVENTO_MACRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EV_EVENTO_MACRO ADD (
  CONSTRAINT EVEVMAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EV_EVENTO_MACRO ADD (
  CONSTRAINT EVEVMAC_EVECMAC_FK 
 FOREIGN KEY (NR_SEQ_CAT_MACRO) 
 REFERENCES TASY.EV_EVENTO_CAT_MACRO (NR_SEQUENCIA));

GRANT SELECT ON TASY.EV_EVENTO_MACRO TO NIVEL_1;

