ALTER TABLE TASY.PACOTE_MATERIAL_SUBST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACOTE_MATERIAL_SUBST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACOTE_MATERIAL_SUBST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PACOTE        NUMBER(10)               NOT NULL,
  NR_SEQ_MATERIAL      NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(6)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PAMASUBS_MATERIA_FK_I ON TASY.PACOTE_MATERIAL_SUBST
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PAMASUBS_PACMATE_FK_I ON TASY.PACOTE_MATERIAL_SUBST
(NR_SEQ_PACOTE, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PAMASUBS_PK ON TASY.PACOTE_MATERIAL_SUBST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PACOTE_MATERIAL_SUBST ADD (
  CONSTRAINT PAMASUBS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACOTE_MATERIAL_SUBST ADD (
  CONSTRAINT PAMASUBS_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PAMASUBS_PACMATE_FK 
 FOREIGN KEY (NR_SEQ_PACOTE, NR_SEQ_MATERIAL) 
 REFERENCES TASY.PACOTE_MATERIAL (NR_SEQ_PACOTE,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PACOTE_MATERIAL_SUBST TO NIVEL_1;

