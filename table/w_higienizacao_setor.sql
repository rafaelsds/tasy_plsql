DROP TABLE TASY.W_HIGIENIZACAO_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_HIGIENIZACAO_SETOR
(
  CD_SETOR_ATENDIMENTO  NUMBER(10),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_HIGIENIZACAO_SETOR TO NIVEL_1;

