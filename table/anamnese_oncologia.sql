ALTER TABLE TASY.ANAMNESE_ONCOLOGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ANAMNESE_ONCOLOGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ANAMNESE_ONCOLOGIA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  DS_QUEIXA_PRINCIPAL        VARCHAR2(2000 BYTE),
  DS_HIST_DOENCA_ATUAL       VARCHAR2(4000 BYTE),
  DS_HIST_PATOLOG_PREGR      VARCHAR2(4000 BYTE),
  DS_HIST_FAMIL_PAI          VARCHAR2(500 BYTE),
  DS_HIST_FAMIL_MAE          VARCHAR2(500 BYTE),
  DS_HIST_FAMIL_FILHO        VARCHAR2(500 BYTE),
  DS_HIST_FAMIL_IRMAO        VARCHAR2(500 BYTE),
  DS_OBS_HIST_FAMIL          VARCHAR2(4000 BYTE),
  DS_GASTRO_INTESTINAL       VARCHAR2(500 BYTE),
  DS_GENITO_URINARIO         VARCHAR2(500 BYTE),
  DS_CARDIO_RESPIRATORIO     VARCHAR2(500 BYTE),
  DS_NEURO_MUSCULAR          VARCHAR2(500 BYTE),
  DS_OBS_ISDA                VARCHAR2(500 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  DS_HIP_DIAG                VARCHAR2(500 BYTE),
  DS_ESTADIOMETRIA           VARCHAR2(500 BYTE),
  DS_CONDUTA                 VARCHAR2(500 BYTE),
  DS_EXAME_COMPLEMENTAR      VARCHAR2(2000 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  DS_OUTROS_SISTEMAS         VARCHAR2(500 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  NR_SEQ_FORMULARIO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ANAMONC_ATCONSPEPA_FK_I ON TASY.ANAMNESE_ONCOLOGIA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAMONC_ATEPACI_FK_I ON TASY.ANAMNESE_ONCOLOGIA
(NR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAMONC_EHRREGI_FK_I ON TASY.ANAMNESE_ONCOLOGIA
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAMONC_I1 ON TASY.ANAMNESE_ONCOLOGIA
(DT_LIBERACAO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAMONC_PERFIL_FK_I ON TASY.ANAMNESE_ONCOLOGIA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ANAMONC_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ANAMONC_PESFISI_FK_I ON TASY.ANAMNESE_ONCOLOGIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ANAMONC_PK ON TASY.ANAMNESE_ONCOLOGIA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAMONC_TASASDI_FK_I ON TASY.ANAMNESE_ONCOLOGIA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ANAMONC_TASASDI_FK2_I ON TASY.ANAMNESE_ONCOLOGIA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ANAMNESE_ONCOLOGIA_atual
before insert or update ON TASY.ANAMNESE_ONCOLOGIA for each row
declare


begin
if	(nvl(:old.DT_ATUALIZACAO_NREC,sysdate+10) <> :new.DT_ATUALIZACAO_NREC) and
	(:new.DT_ATUALIZACAO_NREC is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_ATUALIZACAO_NREC,'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.anamnese_oncologia_pend_atual
after insert or update or delete ON TASY.ANAMNESE_ONCOLOGIA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_anamnese_onc_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

begin

    if	(inserting) or (updating) then

        select	max(ie_lib_anamnese_onc)
        into	ie_lib_anamnese_onc_w
        from	parametro_medico
        where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

        if	(nvl(ie_lib_anamnese_onc_w,'N') = 'S') then

            if	(:new.dt_liberacao is null) then
                ie_tipo_w := 'AON';
            elsif	(:old.dt_liberacao is null) and (:new.dt_liberacao is not null) then
                ie_tipo_w := 'XAON';
            end if;

            if	(ie_tipo_w	is not null) then

                Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario);
            end if;
        end if;

        elsif	(deleting) then

            delete	from pep_item_pendente
            where 	IE_TIPO_REGISTRO = 'AON'
            and	nr_seq_registro = :old.nr_sequencia
            and	nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

    end if;

exception
when others then
	null;
end;


<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ANAMNESE_ONCOLOGIA ADD (
  CONSTRAINT ANAMONC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ANAMNESE_ONCOLOGIA ADD (
  CONSTRAINT ANAMONC_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ANAMONC_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ANAMONC_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ANAMONC_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ANAMONC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ANAMONC_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ANAMONC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ANAMNESE_ONCOLOGIA TO NIVEL_1;

