ALTER TABLE TASY.PAT_CONTA_CONTABIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAT_CONTA_CONTABIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAT_CONTA_CONTABIL
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  CD_CONTA_CONTABIL       VARCHAR2(20 BYTE)     NOT NULL,
  DT_VIGENCIA             DATE                  NOT NULL,
  PR_DEPRECIACAO          NUMBER(15,4)          NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4),
  CD_CONTA_DEPREC_ACUM    VARCHAR2(20 BYTE)     NOT NULL,
  CD_CONTA_DEPREC_RES     VARCHAR2(20 BYTE)     NOT NULL,
  CD_HISTORICO            NUMBER(10)            NOT NULL,
  CD_CONTA_BAIXA          VARCHAR2(20 BYTE)     NOT NULL,
  CD_HIST_BAIXA           NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_HIST_TRANSF          NUMBER(10),
  PR_DEPREC_FISCAL        NUMBER(15,4),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_CONTA_AJUSTE_PAT     VARCHAR2(20 BYTE),
  CD_CONTA_CAP_SOCIAL     VARCHAR2(20 BYTE),
  CD_HIST_AJUSTE_PAT      NUMBER(10),
  CD_EMPRESA              NUMBER(4)             NOT NULL,
  DS_REGRA                VARCHAR2(255 BYTE),
  IE_CONTAB_ENTRADA_AVAL  VARCHAR2(1 BYTE),
  CD_HIST_ENTRADA_AVAL    NUMBER(10),
  CD_CONTA_ENTRADA_AVAL   VARCHAR2(20 BYTE),
  NR_SEQ_CONTA_REF        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PATCOCO_CONCONT_FK_I ON TASY.PAT_CONTA_CONTABIL
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATCOCO_CONCONT_FK2_I ON TASY.PAT_CONTA_CONTABIL
(CD_CONTA_DEPREC_RES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_CONCONT_FK3_I ON TASY.PAT_CONTA_CONTABIL
(CD_CONTA_DEPREC_ACUM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_CONCONT_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_CONCONT_FK4_I ON TASY.PAT_CONTA_CONTABIL
(CD_CONTA_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_CONCONT_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_CONCONT_FK5_I ON TASY.PAT_CONTA_CONTABIL
(CD_CONTA_AJUSTE_PAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_CONCONT_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_CONCONT_FK6_I ON TASY.PAT_CONTA_CONTABIL
(CD_CONTA_CAP_SOCIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_CONCONT_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_CONCONT_FK7_I ON TASY.PAT_CONTA_CONTABIL
(CD_CONTA_ENTRADA_AVAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATCOCO_EMPRESA_FK_I ON TASY.PAT_CONTA_CONTABIL
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_ESTABEL_FK_I ON TASY.PAT_CONTA_CONTABIL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_HISPADR_FK_I ON TASY.PAT_CONTA_CONTABIL
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_HISPADR_FK2_I ON TASY.PAT_CONTA_CONTABIL
(CD_HIST_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_HISPADR_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_HISPADR_FK3_I ON TASY.PAT_CONTA_CONTABIL
(CD_HIST_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_HISPADR_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_HISPADR_FK4_I ON TASY.PAT_CONTA_CONTABIL
(CD_HIST_AJUSTE_PAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATCOCO_HISPADR_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PATCOCO_HISPADR_FK5_I ON TASY.PAT_CONTA_CONTABIL
(CD_HIST_ENTRADA_AVAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATCOCO_PATCOCO_FK_I ON TASY.PAT_CONTA_CONTABIL
(NR_SEQ_CONTA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PATCOCO_PK ON TASY.PAT_CONTA_CONTABIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pat_conta_contabil_delete
before delete ON TASY.PAT_CONTA_CONTABIL for each row
declare

qt_registro_w		number(10);

BEGIN

select	count(*)
into	qt_registro_w
from	pat_bem
where	cd_conta_contabil	= :old.cd_conta_contabil;

if	(qt_registro_w > 0) then
	/*'N�o � poss�vel excluir esta regra, pois existem bens ' || chr(13) || chr(10) ||
					'vinculados a esta conta cont�bil!#@#@');*/
	wheb_mensagem_pck.exibir_mensagem_abort(266503);
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.pat_conta_contabil_atual
BEFORE INSERT OR UPDATE ON TASY.PAT_CONTA_CONTABIL for each row
declare

cd_estabelecimento_w	number(4);
cd_empresa_w			number(4);
ie_conta_vigente_w		Varchar2(1);

ds_ajuste_pat_w		varchar2(100);
ds_baixa_w			varchar2(100);
ds_cap_social_w		varchar2(100);
ds_contabil_w		varchar2(100);
ds_deprec_acum_w	varchar2(100);
ds_deprec_res_w		varchar2(100);
ds_entrada_aval_w	varchar2(100);

begin
ds_ajuste_pat_w		:= substr(wheb_mensagem_pck.get_texto(351019,''),1, 100);
ds_baixa_w			:= substr(wheb_mensagem_pck.get_texto(351026,''),1, 100);
ds_cap_social_w		:= substr(wheb_mensagem_pck.get_texto(351027,''),1, 100);
ds_contabil_w		:= substr(wheb_mensagem_pck.get_texto(351029,''),1, 100);
ds_deprec_acum_w	:= substr(wheb_mensagem_pck.get_texto(351030,''),1, 100);
ds_deprec_res_w		:= substr(wheb_mensagem_pck.get_texto(351031,''),1, 100);
ds_entrada_aval_w	:= substr(wheb_mensagem_pck.get_texto(351032,''),1, 100);


if	(:new.cd_empresa is null) then
	begin
	cd_estabelecimento_w	:= nvl(:new.cd_estabelecimento, wheb_usuario_pck.get_cd_estabelecimento);

	begin
	select	cd_empresa
	into	cd_empresa_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_w;
	exception when others then
		cd_empresa_w	:= null;
	end;

	:new.cd_empresa	:= cd_empresa_w;
	end;
end if;

CTB_Consistir_Conta_Titulo(:new.cd_conta_ajuste_pat, ds_ajuste_pat_w);
CTB_Consistir_Conta_Titulo(:new.cd_conta_baixa, ds_baixa_w);
CTB_Consistir_Conta_Titulo(:new.cd_conta_cap_social, ds_cap_social_w);
CTB_Consistir_Conta_Titulo(:new.cd_conta_contabil, ds_contabil_w);
CTB_Consistir_Conta_Titulo(:new.cd_conta_deprec_acum, ds_deprec_acum_w);
CTB_Consistir_Conta_Titulo(:new.cd_conta_deprec_res, ds_deprec_res_w);
CTB_Consistir_Conta_Titulo(:new.cd_conta_entrada_aval, ds_entrada_aval_w);

select	substr(obter_se_conta_vigente(:new.cd_conta_contabil, :new.dt_vigencia),1,1)
into	ie_conta_vigente_w
from	dual;
if	(ie_conta_vigente_w = 'N') then
	/* 'A conta cont�bil informada est� fora da data de vig�ncia.');*/
	wheb_mensagem_pck.exibir_mensagem_abort(232821,'CD_CONTA_CONTABIL_W=' || :new.cd_conta_contabil);

end if;

select	substr(obter_se_conta_vigente(:new.cd_conta_deprec_acum, :new.dt_vigencia),1,1)
into	ie_conta_vigente_w
from	dual;
if	(ie_conta_vigente_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(266465);
	/*A conta de deprec. acumulada est� fora da data de vig�ncia.');*/
end if;

select	substr(obter_se_conta_vigente(:new.cd_conta_deprec_res, :new.dt_vigencia),1,1)
into	ie_conta_vigente_w
from	dual;
if	(ie_conta_vigente_w = 'N') then
	/*A conta de deprec. resultado est� fora da data de vig�ncia.');*/
	wheb_mensagem_pck.exibir_mensagem_abort(266466);
end if;

select	substr(obter_se_conta_vigente(:new.cd_conta_baixa, :new.dt_vigencia),1,1)
into	ie_conta_vigente_w
from	dual;
if	(ie_conta_vigente_w = 'N') then
	/*A conta cont�bil de baixa est� fora da data de vig�ncia.');*/
	wheb_mensagem_pck.exibir_mensagem_abort(266467);
end if;

end;
/


ALTER TABLE TASY.PAT_CONTA_CONTABIL ADD (
  CONSTRAINT PATCOCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PAT_CONTA_CONTABIL ADD (
  CONSTRAINT PATCOCO_PATCOCO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_REF) 
 REFERENCES TASY.PAT_CONTA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PATCOCO_CONCONT_FK7 
 FOREIGN KEY (CD_CONTA_ENTRADA_AVAL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PATCOCO_HISPADR_FK5 
 FOREIGN KEY (CD_HIST_ENTRADA_AVAL) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PATCOCO_CONCONT_FK5 
 FOREIGN KEY (CD_CONTA_AJUSTE_PAT) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PATCOCO_CONCONT_FK6 
 FOREIGN KEY (CD_CONTA_CAP_SOCIAL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PATCOCO_HISPADR_FK4 
 FOREIGN KEY (CD_HIST_AJUSTE_PAT) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PATCOCO_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PATCOCO_CONCONT_FK3 
 FOREIGN KEY (CD_CONTA_DEPREC_ACUM) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PATCOCO_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PATCOCO_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_DEPREC_RES) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PATCOCO_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PATCOCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PATCOCO_CONCONT_FK4 
 FOREIGN KEY (CD_CONTA_BAIXA) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PATCOCO_HISPADR_FK2 
 FOREIGN KEY (CD_HIST_BAIXA) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PATCOCO_HISPADR_FK3 
 FOREIGN KEY (CD_HIST_TRANSF) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO));

GRANT SELECT ON TASY.PAT_CONTA_CONTABIL TO NIVEL_1;

