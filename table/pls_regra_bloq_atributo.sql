ALTER TABLE TASY.PLS_REGRA_BLOQ_ATRIBUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_BLOQ_ATRIBUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_BLOQ_ATRIBUTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_DOENCA            VARCHAR2(10 BYTE),
  IE_TIPO_GUIA         VARCHAR2(2 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRBAT_CIDDOEN_FK_I ON TASY.PLS_REGRA_BLOQ_ATRIBUTO
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRBAT_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRBAT_ESTABEL_FK_I ON TASY.PLS_REGRA_BLOQ_ATRIBUTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRBAT_PK ON TASY.PLS_REGRA_BLOQ_ATRIBUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRBAT_PK
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_BLOQ_ATRIBUTO ADD (
  CONSTRAINT PLSRBAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_BLOQ_ATRIBUTO ADD (
  CONSTRAINT PLSRBAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRBAT_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.PLS_REGRA_BLOQ_ATRIBUTO TO NIVEL_1;

