ALTER TABLE TASY.COT_COMPRA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COT_COMPRA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.COT_COMPRA_ITEM
(
  NR_COT_COMPRA               NUMBER(10)        NOT NULL,
  NR_ITEM_COT_COMPRA          NUMBER(5)         NOT NULL,
  CD_MATERIAL                 NUMBER(6)         NOT NULL,
  QT_MATERIAL                 NUMBER(13,4)      NOT NULL,
  CD_UNIDADE_MEDIDA_COMPRA    VARCHAR2(30 BYTE) NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  DT_LIMITE_ENTREGA           DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  NR_COT_VENC_SIS             NUMBER(10),
  NR_ITEM_COT_VENC_SIS        NUMBER(5),
  CD_CGC_FORNECEDOR_VENC_SIS  VARCHAR2(14 BYTE),
  NR_COT_VENC_ALT             NUMBER(10),
  NR_ITEM_COT_VENC_ALT        NUMBER(5),
  CD_CGC_FORNECEDOR_VENC_ALT  VARCHAR2(14 BYTE),
  DS_MATERIAL_DIRETO_W        VARCHAR2(255 BYTE),
  NR_SEQ_COT_ITEM_FORN        NUMBER(10),
  IE_REGRA_PRECO              VARCHAR2(15 BYTE),
  NR_SOLIC_COMPRA             NUMBER(10),
  NR_ITEM_SOLIC_COMPRA        NUMBER(5),
  CD_ESTAB_ITEM               NUMBER(4),
  DS_MOTIVO_VENC_ALT          VARCHAR2(4000 BYTE),
  NR_SEQ_LICITACAO            NUMBER(10),
  NR_SEQ_LIC_ITEM             NUMBER(5),
  NR_SEQ_TIPO_COMPRA          NUMBER(10),
  NR_SEQ_MOD_COMPRA           NUMBER(10),
  NR_SEQ_REG_COMPRA           NUMBER(10),
  NR_SEQ_REG_COMPRA_ITEM      NUMBER(5),
  NR_SEQ_PROJ_REC             NUMBER(10),
  NR_SEQ_APROVACAO            NUMBER(10),
  DT_APROVACAO                DATE,
  DT_REPROVACAO               DATE,
  NM_USUARIO_APROV            VARCHAR2(15 BYTE),
  NR_ITEM_SOLIC_COMPRA_ENTR   NUMBER(5),
  DT_DESDOBR_APROV            DATE,
  NR_SEQ_CENTRAL_COMPRA_ITEM  NUMBER(10),
  IE_MOTIVO_REPROVACAO        VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA_REPROV     VARCHAR2(255 BYTE),
  NR_CONTRATO                 NUMBER(10),
  IE_MOTIVO_ALTER_VENC        VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO               NUMBER(10),
  NR_SEQ_PROC_APROV           NUMBER(10),
  VL_UNIT_PREVISTO            NUMBER(17,4),
  IE_CONFIRMADO_INTEGR        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          14M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COCOITE_COCOFOA_I ON TASY.COT_COMPRA_ITEM
(NR_COT_VENC_ALT, NR_ITEM_COT_VENC_ALT, CD_CGC_FORNECEDOR_VENC_ALT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOITE_COCOFOA_I
  MONITORING USAGE;


CREATE INDEX TASY.COCOITE_COCOFOI_I ON TASY.COT_COMPRA_ITEM
(NR_COT_VENC_SIS, NR_ITEM_COT_VENC_SIS, CD_CGC_FORNECEDOR_VENC_SIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOITE_COCOFOI_I
  MONITORING USAGE;


CREATE INDEX TASY.COCOITE_COTCOMP_FK_I ON TASY.COT_COMPRA_ITEM
(NR_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COCOITE_MATERIA_FK_I ON TASY.COT_COMPRA_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COCOITE_PK ON TASY.COT_COMPRA_ITEM
(NR_COT_COMPRA, NR_ITEM_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COCOITE_UNIMEDI_FK_I ON TASY.COT_COMPRA_ITEM
(CD_UNIDADE_MEDIDA_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COCOITE_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOIT_CONTRAT_FK_I ON TASY.COT_COMPRA_ITEM
(NR_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOIT_CONTRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOIT_COTCOFI_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SEQ_COT_ITEM_FORN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOIT_COTMOAQ_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOIT_COTMOAQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOIT_ESTABEL_FK_I ON TASY.COT_COMPRA_ITEM
(CD_ESTAB_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOIT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOIT_PROCOMP_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SEQ_APROVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOIT_PRORECU_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COTCOIT_REGLIIT_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SEQ_LICITACAO, NR_SEQ_LIC_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOIT_REGLIIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOIT_REGLIMC_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SEQ_MOD_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOIT_REGLIMC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOIT_REGLITC_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SEQ_TIPO_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOIT_REGLITC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOIT_RELICIT_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SEQ_LICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOIT_RELICIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOIT_SCCMPIT_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SEQ_CENTRAL_COMPRA_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COTCOIT_SCCMPIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COTCOIT_SOLCOIT_FK_I ON TASY.COT_COMPRA_ITEM
(NR_SOLIC_COMPRA, NR_ITEM_SOLIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cot_compra_item_af_ins_upd
after insert or update ON TASY.COT_COMPRA_ITEM for each row
begin

    if (obter_bloc_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
        wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
    end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.cot_compra_item_bf_ins_upd
before insert or update ON TASY.COT_COMPRA_ITEM for each row
begin

    if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
        wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
    end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.cot_compra_item_ins_upd
before insert or update ON TASY.COT_COMPRA_ITEM for each row
declare

ie_status_w INTEGER := 0;

begin

    select 	obter_bloc_canc_proj_rec(:new.nr_seq_proj_rec)
    into 	ie_status_w
    from    dual;

    if (ie_status_w = 1) then
        wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- 1037932 Registro associado a um projeto bloqueado ou cancelado.
    end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Cot_Compra_Item_Delete
BEFORE DELETE ON TASY.COT_COMPRA_ITEM for each row
declare
pragma autonomous_transaction;

BEGIN

update solic_compra_item
set	nr_cot_compra		= null,
	nr_item_cot_compra	= null
where	nr_cot_compra		= :old.nr_cot_compra
  and nr_item_cot_compra	= :old.nr_item_cot_compra;

update	material_autor_cirurgia
set	nr_cot_compra 		= null,
	nr_item_cot_compra	= null,
    nm_usuario = wheb_usuario_pck.get_nm_usuario,
    dt_atualizacao = sysdate
where	nr_cot_compra		= :old.nr_cot_compra
and 	nr_item_cot_compra	= :old.nr_item_cot_compra;

commit;

END;
/


ALTER TABLE TASY.COT_COMPRA_ITEM ADD (
  CONSTRAINT COCOITE_PK
 PRIMARY KEY
 (NR_COT_COMPRA, NR_ITEM_COT_COMPRA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COT_COMPRA_ITEM ADD (
  CONSTRAINT COCOITE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT COCOITE_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_COMPRA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT COCOITE_COTCOMP_FK 
 FOREIGN KEY (NR_COT_COMPRA) 
 REFERENCES TASY.COT_COMPRA (NR_COT_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT COTCOIT_COTCOFI_FK 
 FOREIGN KEY (NR_SEQ_COT_ITEM_FORN) 
 REFERENCES TASY.COT_COMPRA_FORN_ITEM (NR_SEQUENCIA),
  CONSTRAINT COTCOIT_SOLCOIT_FK 
 FOREIGN KEY (NR_SOLIC_COMPRA, NR_ITEM_SOLIC_COMPRA) 
 REFERENCES TASY.SOLIC_COMPRA_ITEM (NR_SOLIC_COMPRA,NR_ITEM_SOLIC_COMPRA),
  CONSTRAINT COTCOIT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_ITEM) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT COTCOIT_RELICIT_FK 
 FOREIGN KEY (NR_SEQ_LICITACAO) 
 REFERENCES TASY.REG_LICITACAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT COTCOIT_REGLIIT_FK 
 FOREIGN KEY (NR_SEQ_LICITACAO, NR_SEQ_LIC_ITEM) 
 REFERENCES TASY.REG_LIC_ITEM (NR_SEQ_LICITACAO,NR_SEQ_LIC_ITEM)
    ON DELETE CASCADE,
  CONSTRAINT COTCOIT_REGLITC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COMPRA) 
 REFERENCES TASY.REG_LIC_TIPO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT COTCOIT_REGLIMC_FK 
 FOREIGN KEY (NR_SEQ_MOD_COMPRA) 
 REFERENCES TASY.REG_LIC_MOD_COMPRA (NR_SEQUENCIA),
  CONSTRAINT COTCOIT_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA),
  CONSTRAINT COTCOIT_PROCOMP_FK 
 FOREIGN KEY (NR_SEQ_APROVACAO) 
 REFERENCES TASY.PROCESSO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT COTCOIT_SCCMPIT_FK 
 FOREIGN KEY (NR_SEQ_CENTRAL_COMPRA_ITEM) 
 REFERENCES TASY.SUP_CENTRAL_COMPRA_ITEM (NR_SEQUENCIA),
  CONSTRAINT COTCOIT_CONTRAT_FK 
 FOREIGN KEY (NR_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT COTCOIT_COTMOAQ_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.COT_MOTIVO_ALT_QTDE (NR_SEQUENCIA));

GRANT SELECT ON TASY.COT_COMPRA_ITEM TO NIVEL_1;

