ALTER TABLE TASY.DOMINIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DOMINIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DOMINIO
(
  CD_DOMINIO           NUMBER(5)                NOT NULL,
  NM_DOMINIO           VARCHAR2(80 BYTE)        NOT NULL,
  DS_DOMINIO           VARCHAR2(240 BYTE)       NOT NULL,
  IE_ALTERAR           VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_MODULO        NUMBER(10)               NOT NULL,
  IE_IMPLANTAR         VARCHAR2(1 BYTE)         NOT NULL,
  CD_EXP_DOMINIO       NUMBER(10)               NOT NULL,
  CD_EXP_DESC_DOMINIO  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.DOMINIO.CD_DOMINIO IS 'Codigo do Dominio';

COMMENT ON COLUMN TASY.DOMINIO.NM_DOMINIO IS 'Nome do Dominio';

COMMENT ON COLUMN TASY.DOMINIO.DS_DOMINIO IS 'Descri�ao do Dominio';

COMMENT ON COLUMN TASY.DOMINIO.IE_ALTERAR IS 'Indica se o Usuario pode alterar';

COMMENT ON COLUMN TASY.DOMINIO.DT_ATUALIZACAO IS 'Data atualizacao';

COMMENT ON COLUMN TASY.DOMINIO.NM_USUARIO IS 'Usuario Atualizador';


CREATE INDEX TASY.DOMINIO_DICEXPR_FK_I ON TASY.DOMINIO
(CD_EXP_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DOMINIO_DICEXPR_FK2_I ON TASY.DOMINIO
(CD_EXP_DESC_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DOMINIO_MODTASY_FK_I ON TASY.DOMINIO
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DOMINIO_PK ON TASY.DOMINIO
(CD_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DOMINIO_tp  after update ON TASY.DOMINIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_DOMINIO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EXP_DESC_DOMINIO,1,4000),substr(:new.CD_EXP_DESC_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXP_DESC_DOMINIO',ie_log_w,ds_w,'DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EXP_DOMINIO,1,4000),substr(:new.CD_EXP_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXP_DOMINIO',ie_log_w,ds_w,'DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOMINIO,1,4000),substr(:new.CD_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOMINIO',ie_log_w,ds_w,'DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MODULO,1,4000),substr(:new.NR_SEQ_MODULO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MODULO',ie_log_w,ds_w,'DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DOMINIO,1,4000),substr(:new.DS_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_DOMINIO',ie_log_w,ds_w,'DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTERAR,1,4000),substr(:new.IE_ALTERAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTERAR',ie_log_w,ds_w,'DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPLANTAR,1,4000),substr(:new.IE_IMPLANTAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPLANTAR',ie_log_w,ds_w,'DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_DOMINIO,1,4000),substr(:new.NM_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_DOMINIO',ie_log_w,ds_w,'DOMINIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.DOMINIO ADD (
  CONSTRAINT DOMINIO_PK
 PRIMARY KEY
 (CD_DOMINIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DOMINIO ADD (
  CONSTRAINT DOMINIO_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_DOMINIO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT DOMINIO_MODTASY_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.MODULO_TASY (NR_SEQUENCIA),
  CONSTRAINT DOMINIO_DICEXPR_FK2 
 FOREIGN KEY (CD_EXP_DESC_DOMINIO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.DOMINIO TO NIVEL_1;

