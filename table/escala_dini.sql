ALTER TABLE TASY.ESCALA_DINI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_DINI CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_DINI
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_ATIVIDADE           NUMBER(1)              NOT NULL,
  IE_INTERV_CONTROLES    NUMBER(1)              NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_OXIGENACAO          NUMBER(1)              NOT NULL,
  IE_TERAPEUTICA_MEDIC   NUMBER(1)              NOT NULL,
  IE_INTEGRIDADE_MUCOSA  NUMBER(1)              NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_ALIMENTACAO_HIDRAT  NUMBER(1)              NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_ELIMINACOES         NUMBER(1)              NOT NULL,
  IE_HIGIENE_CORPORAL    NUMBER(1)              NOT NULL,
  IE_MOBILIDADE          NUMBER(1)              NOT NULL,
  IE_PART_ACOMPANHANTE   NUMBER(1)              NOT NULL,
  IE_REDE_APOIO_SUPORTE  NUMBER(1)              NOT NULL,
  QT_PONTUACAO           NUMBER(2)              NOT NULL,
  DT_LIBERACAO           DATE,
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCDINI_ATEPACI_FK_I ON TASY.ESCALA_DINI
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCDINI_I1 ON TASY.ESCALA_DINI
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCDINI_PESFISI_FK2_I ON TASY.ESCALA_DINI
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCDINI_PK ON TASY.ESCALA_DINI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCDINI_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_dini_atual
before insert or update ON TASY.ESCALA_DINI for each row
declare
qt_reg_w	number(1);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_pontuacao	:=	:new.ie_atividade +
			:new.ie_interv_controles +
			:new.ie_oxigenacao +
			:new.ie_terapeutica_medic +
			:new.ie_integridade_mucosa +
			:new.ie_alimentacao_hidrat +
			:new.ie_eliminacoes +
			:new.ie_higiene_corporal +
			:new.ie_mobilidade +
			:new.ie_part_acompanhante +
			:new.ie_rede_apoio_suporte;
<<Final>>
qt_reg_w	:= 0;


end escala_dini_atual;
/


ALTER TABLE TASY.ESCALA_DINI ADD (
  CONSTRAINT ESCDINI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_DINI ADD (
  CONSTRAINT ESCDINI_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCDINI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_DINI TO NIVEL_1;

