ALTER TABLE TASY.DES_ATIV_EXEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DES_ATIV_EXEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.DES_ATIV_EXEC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DS_ATIVIDADE           VARCHAR2(80 BYTE)      NOT NULL,
  QT_MIN_ATIV            NUMBER(3)              NOT NULL,
  CD_SETOR_ATIV          NUMBER(5),
  IE_NET                 VARCHAR2(1 BYTE)       NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  NR_SEQ_TIPO_AVALIACAO  NUMBER(10),
  CD_EXP_ATIVIDADE       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DESATEX_DICEXPR_FK_I ON TASY.DES_ATIV_EXEC
(CD_EXP_ATIVIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESATEX_MEDTIAV_FK_I ON TASY.DES_ATIV_EXEC
(NR_SEQ_TIPO_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESATEX_MEDTIAV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DESATEX_PK ON TASY.DES_ATIV_EXEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESATEX_SETATEN_FK_I ON TASY.DES_ATIV_EXEC
(CD_SETOR_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESATEX_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.DES_ATIV_EXEC ADD (
  CONSTRAINT DESATEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DES_ATIV_EXEC ADD (
  CONSTRAINT DESATEX_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATIV) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT DESATEX_MEDTIAV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_AVALIACAO) 
 REFERENCES TASY.MED_TIPO_AVALIACAO (NR_SEQUENCIA),
  CONSTRAINT DESATEX_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_ATIVIDADE) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.DES_ATIV_EXEC TO NIVEL_1;

