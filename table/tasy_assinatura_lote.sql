ALTER TABLE TASY.TASY_ASSINATURA_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_ASSINATURA_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_ASSINATURA_LOTE
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NR_ATENDIMENTO      NUMBER(10),
  CD_PESSOA_FISICA    VARCHAR2(10 BYTE)         NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  NR_PROJ_ASSINATURA  NUMBER(15),
  NM_TABELA           VARCHAR2(255 BYTE),
  NR_SEQ_REGISTRO     NUMBER(10),
  NR_SEQ_VISAO        NUMBER(10),
  CD_FUNCAO           NUMBER(10),
  IE_ITEM_PENDENCIA   VARCHAR2(5 BYTE),
  DT_REGISTRO         DATE                      NOT NULL,
  IE_TIPO_ASSINATURA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TAASSLO_ATEPACI_FK_I ON TASY.TASY_ASSINATURA_LOTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAASSLO_FUNCAO_FK_I ON TASY.TASY_ASSINATURA_LOTE
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAASSLO_PESFISI_FK_I ON TASY.TASY_ASSINATURA_LOTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TAASSLO_PK ON TASY.TASY_ASSINATURA_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAASSLO_TABVISA_FK_I ON TASY.TASY_ASSINATURA_LOTE
(NR_SEQ_VISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAASSLO_TASPRAS_FK_I ON TASY.TASY_ASSINATURA_LOTE
(NR_PROJ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TASY_ASSINATURA_LOTE ADD (
  CONSTRAINT TAASSLO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_ASSINATURA_LOTE ADD (
  CONSTRAINT TAASSLO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT TAASSLO_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT TAASSLO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TAASSLO_TABVISA_FK 
 FOREIGN KEY (NR_SEQ_VISAO) 
 REFERENCES TASY.TABELA_VISAO (NR_SEQUENCIA),
  CONSTRAINT TAASSLO_TASPRAS_FK 
 FOREIGN KEY (NR_PROJ_ASSINATURA) 
 REFERENCES TASY.TASY_PROJETO_ASSINATURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TASY_ASSINATURA_LOTE TO NIVEL_1;

