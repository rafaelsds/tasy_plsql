ALTER TABLE TASY.AGENDA_PACIENTE_AUXILIAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_PACIENTE_AUXILIAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_PACIENTE_AUXILIAR
(
  NR_SEQUENCIA                    NUMBER(10)    NOT NULL,
  CD_DEPTO_MEDICO                 NUMBER(6),
  DT_ATUALIZACAO                  DATE          NOT NULL,
  NM_USUARIO                      VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC             DATE,
  NM_USUARIO_NREC                 VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO            NUMBER(5),
  NR_SEQ_AGENDA                   NUMBER(10),
  IE_POSICAO                      NUMBER(3),
  NR_SEQ_CLASSIF_ATEND            NUMBER(10),
  IE_ADV_APPOINTMENT              VARCHAR2(1 BYTE),
  NR_SEQ_CPOE_PROCEDIMENTO        NUMBER(10),
  IE_REFERENCE_LETTER             VARCHAR2(3 BYTE),
  IE_EMERGENCY_RESPONSE_REQUIRED  VARCHAR2(1 BYTE),
  DS_PROCEDENCIA_VAR              VARCHAR2(255 BYTE),
  CD_EVOLUCAO                     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGPACAUX_AGEPACI_FK_I ON TASY.AGENDA_PACIENTE_AUXILIAR
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPACAUX_DEPMED_FK_I ON TASY.AGENDA_PACIENTE_AUXILIAR
(CD_DEPTO_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPACAUX_EVOPACI_FK_I ON TASY.AGENDA_PACIENTE_AUXILIAR
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGPACAUX_PK ON TASY.AGENDA_PACIENTE_AUXILIAR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGPACAUX_SETATEN_FK_I ON TASY.AGENDA_PACIENTE_AUXILIAR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGENDA_PACIENTE_AUXILIAR ADD (
  CONSTRAINT AGPACAUX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_PACIENTE_AUXILIAR ADD (
  CONSTRAINT AGPACAUX_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGPACAUX_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT AGPACAUX_DEPMED_FK 
 FOREIGN KEY (CD_DEPTO_MEDICO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT AGPACAUX_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.AGENDA_PACIENTE_AUXILIAR TO NIVEL_1;

