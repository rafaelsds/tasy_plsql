ALTER TABLE TASY.PESSOA_JUR_CONTA_CONT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JUR_CONTA_CONT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JUR_CONTA_CONT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CGC               VARCHAR2(14 BYTE)        NOT NULL,
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE)        NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  IE_TIPO_CONTA        VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  CD_ESTABELECIMENTO   NUMBER(4),
  CD_ESTAB_DESTINO     NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PJCOCON_CONCONT_FK_I ON TASY.PESSOA_JUR_CONTA_CONT
(CD_CONTA_CONTABIL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PJCOCON_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PJCOCON_EMPRESA_FK_I ON TASY.PESSOA_JUR_CONTA_CONT
(CD_EMPRESA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PJCOCON_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PJCOCON_ESTABEL_FK_I ON TASY.PESSOA_JUR_CONTA_CONT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PJCOCON_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PJCOCON_I2_I ON TASY.PESSOA_JUR_CONTA_CONT
(CD_ESTAB_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PJCOCON_I2_I
  MONITORING USAGE;


CREATE INDEX TASY.PJCOCON_PESJURI_FK_I ON TASY.PESSOA_JUR_CONTA_CONT
(CD_CGC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PJCOCON_PK ON TASY.PESSOA_JUR_CONTA_CONT
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Pessoa_Jur_conta_cont_Atual
BEFORE INSERT OR UPDATE ON TASY.PESSOA_JUR_CONTA_CONT FOR EACH ROW
declare
ds_erro_w		varchar2(2000);
ie_tipo_w			varchar2(1);
BEGIN

if	(:new.cd_conta_contabil is not null) then

	select	max(ie_tipo)
	into	ie_tipo_w
	from	conta_contabil
	where	cd_conta_contabil	= :new.cd_conta_contabil;

	if	(ie_tipo_w = 'T') then
		/*(-20011,'N�o pode ser informado conta do tipo T�tulo! ' ||
						'Conta: ' || :new.cd_conta_contabil || ' PJ: ' || :new.cd_cgc || '#@#@');*/
		wheb_mensagem_pck.exibir_mensagem_abort(263192,'CD_CONTA=' || :new.cd_conta_contabil ||
								';CD_CGC=' || :new.cd_cgc);
	end if;
end if;

con_consiste_vigencia_conta(:new.cd_conta_contabil, :new.dt_inicio_vigencia, :new.dt_fim_vigencia, ds_erro_w);
if	(ds_erro_w is not null) then
	/*(-20111, ds_erro_w);*/
	wheb_mensagem_pck.exibir_mensagem_abort(263193,'DS_ERRO=' || ds_erro_w);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_JUR_CONTA_CONT_tp  after update ON TASY.PESSOA_JUR_CONTA_CONT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_CONTA,1,4000),substr(:new.IE_TIPO_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONTA',ie_log_w,ds_w,'PESSOA_JUR_CONTA_CONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'PESSOA_JUR_CONTA_CONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PESSOA_JUR_CONTA_CONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PESSOA_JUR_CONTA_CONT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PESSOA_JUR_CONTA_CONT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_JUR_CONTA_CONT ADD (
  CONSTRAINT PJCOCON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_JUR_CONTA_CONT ADD (
  CONSTRAINT PJCOCON_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT PJCOCON_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PJCOCON_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA)
    ON DELETE CASCADE,
  CONSTRAINT PJCOCON_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PJCOCON_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_DESTINO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PESSOA_JUR_CONTA_CONT TO NIVEL_1;

