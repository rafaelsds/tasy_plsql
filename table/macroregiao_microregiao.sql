ALTER TABLE TASY.MACROREGIAO_MICROREGIAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MACROREGIAO_MICROREGIAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MACROREGIAO_MICROREGIAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MACROREGIAO   NUMBER(10)               NOT NULL,
  NR_SEQ_MICROREGIAO   NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MACROMICRO_MACROREG_FK_I ON TASY.MACROREGIAO_MICROREGIAO
(NR_SEQ_MACROREGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MACROMICRO_MICRORE_FK_I ON TASY.MACROREGIAO_MICROREGIAO
(NR_SEQ_MICROREGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MACROMICRO_PK ON TASY.MACROREGIAO_MICROREGIAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MACROREGIAO_MICROREGIAO ADD (
  CONSTRAINT MACROMICRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MACROREGIAO_MICROREGIAO ADD (
  CONSTRAINT MACROMICRO_MACROREG_FK 
 FOREIGN KEY (NR_SEQ_MACROREGIAO) 
 REFERENCES TASY.MACROREGIAO (NR_SEQUENCIA),
  CONSTRAINT MACROMICRO_MICRORE_FK 
 FOREIGN KEY (NR_SEQ_MICROREGIAO) 
 REFERENCES TASY.MICROREGIAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MACROREGIAO_MICROREGIAO TO NIVEL_1;

