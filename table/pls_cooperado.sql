ALTER TABLE TASY.PLS_COOPERADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COOPERADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COOPERADO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  CD_CGC                VARCHAR2(14 BYTE),
  DT_INCLUSAO           DATE                    NOT NULL,
  DT_EXCLUSAO           DATE,
  DT_SUSPENSAO          DATE,
  IE_STATUS             VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIFICACAO  NUMBER(10),
  CD_MATRICULA          VARCHAR2(30 BYTE),
  NR_SEQ_SITUACAO       NUMBER(10)              NOT NULL,
  CD_SISTEMA_ANT        VARCHAR2(40 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCOOR_ESTABEL_FK_I ON TASY.PLS_COOPERADO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOOR_PESFISI_FK_I ON TASY.PLS_COOPERADO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOOR_PESJURI_FK_I ON TASY.PLS_COOPERADO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCOOR_PK ON TASY.PLS_COOPERADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOOR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOOR_PLSCLCO_FK_I ON TASY.PLS_COOPERADO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOOR_PLSCLCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOOR_PLSSICO_FK_I ON TASY.PLS_COOPERADO
(NR_SEQ_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOOR_PLSSICO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_cooperado_atual
before update or update ON TASY.PLS_COOPERADO for each row
declare

dt_status_w		date;

begin
select	a.ie_status
into	:new.ie_status
from	pls_situacao_cooperado	a
where	a.nr_sequencia	= :new.nr_seq_situacao;

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then

	select	decode(:new.ie_status,'A',:new.dt_inclusao,'I',:new.dt_exclusao,null)
	into	dt_status_w
	from	dual;


	insert into pls_cooperado_situacao
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_cooperado,
		ds_motivo,
		dt_alteracao,
		dt_status,
		ie_situacao,
		nr_seq_motivo_susp,
		nr_seq_situacao_coop)
	values	(pls_cooperado_situacao_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia,
		null,
		sysdate,
		dt_status_w,
		:new.ie_status,
		null,
		:new.nr_seq_situacao);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_COOPERADO_tp  after update ON TASY.PLS_COOPERADO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PLS_COOPERADO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CGC,1,500);gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'PLS_COOPERADO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_EXCLUSAO,1,500);gravar_log_alteracao(substr(:old.DT_EXCLUSAO,1,4000),substr(:new.DT_EXCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_EXCLUSAO',ie_log_w,ds_w,'PLS_COOPERADO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MATRICULA,1,500);gravar_log_alteracao(substr(:old.CD_MATRICULA,1,4000),substr(:new.CD_MATRICULA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATRICULA',ie_log_w,ds_w,'PLS_COOPERADO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_SUSPENSAO,1,500);gravar_log_alteracao(substr(:old.DT_SUSPENSAO,1,4000),substr(:new.DT_SUSPENSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_SUSPENSAO',ie_log_w,ds_w,'PLS_COOPERADO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_STATUS,1,500);gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'PLS_COOPERADO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_CLASSIFICACAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIFICACAO,1,4000),substr(:new.NR_SEQ_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIFICACAO',ie_log_w,ds_w,'PLS_COOPERADO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_COOPERADO ADD (
  CONSTRAINT PLSCOOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COOPERADO ADD (
  CONSTRAINT PLSCOOR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSCOOR_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PLSCOOR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCOOR_PLSCLCO_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.PLS_CLASSIF_COOPERADO (NR_SEQUENCIA),
  CONSTRAINT PLSCOOR_PLSSICO_FK 
 FOREIGN KEY (NR_SEQ_SITUACAO) 
 REFERENCES TASY.PLS_SITUACAO_COOPERADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COOPERADO TO NIVEL_1;

