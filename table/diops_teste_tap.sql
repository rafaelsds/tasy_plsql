ALTER TABLE TASY.DIOPS_TESTE_TAP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIOPS_TESTE_TAP CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIOPS_TESTE_TAP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_TIPO_MOVIMENTO    NUMBER(10),
  DS_AJUSTE_TABUA      VARCHAR2(255 BYTE),
  TX_CANCELAMENTO      NUMBER(7,4),
  PR_INFLACAO          NUMBER(7,4),
  PR_REAJUSTE_INDIV    NUMBER(7,4),
  PR_REAJUTE_COLETIVO  NUMBER(7,4),
  DS_UTILIZACAO        VARCHAR2(255 BYTE),
  DS_INTERPOLACAO      VARCHAR2(255 BYTE),
  VL_ESTIMATIVA_FLUXO  NUMBER(15,2),
  NR_SEQ_PERIODO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIOPSTTAP_DIOPERI_FK_I ON TASY.DIOPS_TESTE_TAP
(NR_SEQ_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIOPSTTAP_PK ON TASY.DIOPS_TESTE_TAP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DIOPS_TESTE_TAP ADD (
  CONSTRAINT DIOPSTTAP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIOPS_TESTE_TAP ADD (
  CONSTRAINT DIOPSTTAP_DIOPERI_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.DIOPS_PERIODO (NR_SEQUENCIA));

GRANT SELECT ON TASY.DIOPS_TESTE_TAP TO NIVEL_1;

