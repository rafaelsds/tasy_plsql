ALTER TABLE TASY.GERENCIA_WHEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GERENCIA_WHEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.GERENCIA_WHEB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_GERENCIA           VARCHAR2(80 BYTE)       NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_RESPONSAVEL        VARCHAR2(10 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  DS_GERENCIA_EXTERNO   VARCHAR2(120 BYTE),
  NR_TELEFONE_PLANTAO   VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO    NUMBER(4),
  IE_AREA_GERENCIA      VARCHAR2(5 BYTE),
  CD_EXP_GERENCIA       NUMBER(10),
  NR_DDI_PLANTAO        VARCHAR2(3 BYTE),
  NR_SEQ_DIRETORIA      NUMBER(10),
  DS_ABREVIACAO         VARCHAR2(15 BYTE),
  DS_COR                VARCHAR2(15 BYTE),
  IE_TIPO_GRUPO         VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GERWHEB_DICEXPR_FK_I ON TASY.GERENCIA_WHEB
(CD_EXP_GERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GERWHEB_DIRPHIL_FK_I ON TASY.GERENCIA_WHEB
(NR_SEQ_DIRETORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GERWHEB_ESTABEL_FK_I ON TASY.GERENCIA_WHEB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GERWHEB_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GERWHEB_PESFISI_FK_I ON TASY.GERENCIA_WHEB
(CD_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GERWHEB_PK ON TASY.GERENCIA_WHEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GERWHEB_SETATEN_FK_I ON TASY.GERENCIA_WHEB
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.gerencia_wheb_before_update
before update ON TASY.GERENCIA_WHEB for each row
declare

    qt_grupo number(10) := 0;

begin

    if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
		goto Final;
	end if;

    if (updating) and (:old.ie_tipo_grupo <> :new.ie_tipo_grupo) then

        if (:old.ie_tipo_grupo = 'S' and :new.ie_tipo_grupo = 'D') then

            select count(1)
              into qt_grupo
              from grupo_suporte
             where nr_seq_gerencia_sup = :new.nr_sequencia;

        elsif (:old.ie_tipo_grupo = 'D' and :new.ie_tipo_grupo = 'S') then

            select count(1)
              into qt_grupo
              from grupo_desenvolvimento
             where nr_seq_gerencia = :new.nr_sequencia;

        end if;

        if (qt_grupo > 0) then

            wheb_mensagem_pck.exibir_mensagem_abort(1162133);

        end if;

    end if;

    <<Final>>

    qt_grupo := 0;

end;
/


ALTER TABLE TASY.GERENCIA_WHEB ADD (
  CONSTRAINT GERWHEB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GERENCIA_WHEB ADD (
  CONSTRAINT GERWHEB_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_GERENCIA) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT GERWHEB_DIRPHIL_FK 
 FOREIGN KEY (NR_SEQ_DIRETORIA) 
 REFERENCES TASY.DIRETORIA_PHILIPS (NR_SEQUENCIA),
  CONSTRAINT GERWHEB_PESFISI_FK 
 FOREIGN KEY (CD_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT GERWHEB_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT GERWHEB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.GERENCIA_WHEB TO NIVEL_1;

