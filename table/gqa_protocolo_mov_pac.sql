ALTER TABLE TASY.GQA_PROTOCOLO_MOV_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GQA_PROTOCOLO_MOV_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.GQA_PROTOCOLO_MOV_PAC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  QT_PONTOS            NUMBER(10),
  DT_INICIO            DATE,
  NM_USUARIO_EXECUTOR  VARCHAR2(255 BYTE),
  NR_SEQ_ETAPA_SUP     NUMBER(10)               NOT NULL,
  DS_MOVIMENTO         VARCHAR2(255 BYTE)       NOT NULL,
  DT_FIM               DATE,
  NR_SEQ_ETAPAC_PAC    NUMBER(10),
  QT_TEMPO_EXECUCAO    NUMBER(10),
  CD_PROT_MOV          NUMBER(10)               NOT NULL,
  IE_ACAO              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GQPRMOP_GQPRETP_FK_I ON TASY.GQA_PROTOCOLO_MOV_PAC
(NR_SEQ_ETAPAC_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GQPRMOP_PK ON TASY.GQA_PROTOCOLO_MOV_PAC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GQA_PROTOCOLO_MOV_PAC ADD (
  CONSTRAINT GQPRMOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.GQA_PROTOCOLO_MOV_PAC ADD (
  CONSTRAINT GQPRMOP_GQPRETP_FK 
 FOREIGN KEY (NR_SEQ_ETAPAC_PAC) 
 REFERENCES TASY.GQA_PROTOCOLO_ETAPA_PAC (NR_SEQUENCIA));

GRANT SELECT ON TASY.GQA_PROTOCOLO_MOV_PAC TO NIVEL_1;

