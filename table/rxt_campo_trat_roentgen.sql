ALTER TABLE TASY.RXT_CAMPO_TRAT_ROENTGEN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RXT_CAMPO_TRAT_ROENTGEN CASCADE CONSTRAINTS;

CREATE TABLE TASY.RXT_CAMPO_TRAT_ROENTGEN
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  QT_DOSE_DIA               NUMBER(15,4)        NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_TRATAMENTO         NUMBER(10),
  NR_SEQ_VOLUME_TRATAMENTO  NUMBER(10),
  NR_SEQ_CAMPO_PROT         NUMBER(10),
  NM_CAMPO                  VARCHAR2(40 BYTE)   NOT NULL,
  QT_DIA_TRAT               NUMBER(5)           NOT NULL,
  QT_DOSE_CAMPO             NUMBER(15,4)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RXTCPTR_PK ON TASY.RXT_CAMPO_TRAT_ROENTGEN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RXTCPTR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.RXT_CAMPO_TRAT_ROENTGEN_tp  after update ON TASY.RXT_CAMPO_TRAT_ROENTGEN FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NM_CAMPO,1,4000),substr(:new.NM_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'NM_CAMPO',ie_log_w,ds_w,'RXT_CAMPO_TRAT_ROENTGEN',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_DIA,1,4000),substr(:new.QT_DOSE_DIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_DIA',ie_log_w,ds_w,'RXT_CAMPO_TRAT_ROENTGEN',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_CAMPO,1,4000),substr(:new.QT_DOSE_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_CAMPO',ie_log_w,ds_w,'RXT_CAMPO_TRAT_ROENTGEN',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_TRAT,1,4000),substr(:new.QT_DIA_TRAT,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_TRAT',ie_log_w,ds_w,'RXT_CAMPO_TRAT_ROENTGEN',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RXT_CAMPO_TRAT_ROENTGEN ADD (
  CONSTRAINT RXTCPTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.RXT_CAMPO_TRAT_ROENTGEN TO NIVEL_1;

