ALTER TABLE TASY.HD_PAC_PROT_EXAME_EXTERNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_PAC_PROT_EXAME_EXTERNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_PAC_PROT_EXAME_EXTERNO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_REGRA_SOLIC       VARCHAR2(15 BYTE),
  IE_FORMA_SOLIC       VARCHAR2(2 BYTE)         NOT NULL,
  NR_DIA               NUMBER(10)               NOT NULL,
  NR_MES               NUMBER(10)               NOT NULL,
  IE_FORMA_GERACAO     VARCHAR2(1 BYTE),
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  IE_EXAME             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDPACEE_HDPAREC_FK_I ON TASY.HD_PAC_PROT_EXAME_EXTERNO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPACEE_HDPAREC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDPACEE_MEDPOEX_FK_I ON TASY.HD_PAC_PROT_EXAME_EXTERNO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDPACEE_MEDPOEX_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDPACEE_PK ON TASY.HD_PAC_PROT_EXAME_EXTERNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HD_PAC_PROT_EXAME_EXTERNO ADD (
  CONSTRAINT HDPACEE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_PAC_PROT_EXAME_EXTERNO ADD (
  CONSTRAINT HDPACEE_HDPAREC_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.HD_PAC_RENAL_CRONICO (CD_PESSOA_FISICA),
  CONSTRAINT HDPACEE_MEDPOEX_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.MED_PROTOCOLO_EXAME (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_PAC_PROT_EXAME_EXTERNO TO NIVEL_1;

