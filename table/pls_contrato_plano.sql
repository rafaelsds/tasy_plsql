ALTER TABLE TASY.PLS_CONTRATO_PLANO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTRATO_PLANO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTRATO_PLANO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO       NUMBER(10)              NOT NULL,
  NR_SEQ_PLANO          NUMBER(10)              NOT NULL,
  NR_SEQ_TABELA         NUMBER(10),
  DT_INATIVACAO         DATE,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  NR_SEQ_TABELA_ORIGEM  NUMBER(10),
  IE_TIPO_OPERACAO      VARCHAR2(3 BYTE),
  DT_INICIO_VIGENCIA    DATE,
  DT_FIM_VIGENCIA       DATE,
  IE_INCLUSAO_PORTAL    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCOPL_PK ON TASY.PLS_CONTRATO_PLANO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOPL_PLSCONT_FK_I ON TASY.PLS_CONTRATO_PLANO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOPL_PLSPLAN_FK_I ON TASY.PLS_CONTRATO_PLANO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOPL_PLSTAPR_FK_I ON TASY.PLS_CONTRATO_PLANO
(NR_SEQ_TABELA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOPL_PLSTAPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOPL_PLSTAPR_FK2_I ON TASY.PLS_CONTRATO_PLANO
(NR_SEQ_TABELA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_contrato_plano_delete
before delete ON TASY.PLS_CONTRATO_PLANO for each row
declare

qt_registros_w		number(10);

begin

select	count(*)
into	qt_registros_w
from	pls_segurado
where	nr_seq_plano	= :old.nr_seq_plano
and	nr_seq_contrato	= :old.nr_seq_contrato;

if	(qt_registros_w > 0) then
	--Produto vinculado a algum benefici�rio. N�o pode ser exclu�do!
	wheb_mensagem_pck.exibir_mensagem_abort(267136);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_contrato_plano_insert
before insert or update ON TASY.PLS_CONTRATO_PLANO for each row
declare

nr_seq_contrato_w	number(10);
nr_seq_plano_tabela_w	number(10);

begin

if	(:new.nr_seq_tabela is not null) then
	select	nr_seq_plano,
		nr_contrato
	into	nr_seq_plano_tabela_w,
		nr_seq_contrato_w
	from	pls_tabela_preco
	where	nr_sequencia	= :new.nr_seq_tabela;

	if	(nr_seq_plano_tabela_w <> :new.nr_seq_plano) then
		-- A tabela selecionada n�o corresponde a este produto! Verifique.
		wheb_mensagem_pck.exibir_mensagem_abort(267051);
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_CONTRATO_PLANO ADD (
  CONSTRAINT PLSCOPL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTRATO_PLANO ADD (
  CONSTRAINT PLSCOPL_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCOPL_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSCOPL_PLSTAPR_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.PLS_TABELA_PRECO (NR_SEQUENCIA),
  CONSTRAINT PLSCOPL_PLSTAPR_FK2 
 FOREIGN KEY (NR_SEQ_TABELA_ORIGEM) 
 REFERENCES TASY.PLS_TABELA_PRECO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTRATO_PLANO TO NIVEL_1;

