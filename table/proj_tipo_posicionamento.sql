ALTER TABLE TASY.PROJ_TIPO_POSICIONAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_TIPO_POSICIONAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_TIPO_POSICIONAMENTO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIF          NUMBER(10)            NOT NULL,
  DS_TIPO_POSICIONAMENTO  VARCHAR2(255 BYTE)    NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROJTPPOSI_PK ON TASY.PROJ_TIPO_POSICIONAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJTPPOSI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROJTPPOSI_PROCLAF_FK_I ON TASY.PROJ_TIPO_POSICIONAMENTO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJTPPOSI_PROCLAF_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_TIPO_POSICIONAMENTO ADD (
  CONSTRAINT PROJTPPOSI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_TIPO_POSICIONAMENTO ADD (
  CONSTRAINT PROJTPPOSI_PROCLAF_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PROJ_CLASSIF (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_TIPO_POSICIONAMENTO TO NIVEL_1;

