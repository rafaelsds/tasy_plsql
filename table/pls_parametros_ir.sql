ALTER TABLE TASY.PLS_PARAMETROS_IR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PARAMETROS_IR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PARAMETROS_IR
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_CONSIDERAR_BAIXAS      VARCHAR2(1 BYTE),
  IE_BAIXA_NEGOCIACAO       VARCHAR2(2 BYTE),
  IE_CONSIDERAR_JURMUL_NEG  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPAIR_PK ON TASY.PLS_PARAMETROS_IR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_parametros_ir_afterupdate
after update ON TASY.PLS_PARAMETROS_IR 
for each row
declare

begin

if	(wheb_usuario_pck.get_cd_estabelecimento is not null) then -- N�o tem o campo CD_ESTABELECIMENTO na tabela pls_parametros_ir, deve ser criado o mesmo para retirar est� restri��o
	if	(:new.ie_baixa_negociacao <> :old.ie_baixa_negociacao) then
		pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_IR', 'IE_BAIXA_NEGOCIACAO', :old.ie_baixa_negociacao, 
						:new.ie_baixa_negociacao, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	end if;

	if	(:new.ie_considerar_baixas <> :old.ie_considerar_baixas) then
		pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_IR', 'IE_CONSIDERAR_BAIXAS', :old.ie_considerar_baixas, 
						:new.ie_considerar_baixas, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	end if;

	if	(:new.ie_considerar_jurmul_neg <> :old.ie_considerar_jurmul_neg) then
		pls_gerar_log_alt_parametros(	'PLS_PARAMETROS_IR', 'IE_CONSIDERAR_JURMUL_NEG', :old.ie_considerar_jurmul_neg, 
						:new.ie_considerar_jurmul_neg, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_PARAMETROS_IR ADD (
  CONSTRAINT PLSPAIR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_PARAMETROS_IR TO NIVEL_1;

