ALTER TABLE TASY.BOMBA_INFUSAO_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BOMBA_INFUSAO_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.BOMBA_INFUSAO_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_BOMBA         NUMBER(14)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_HISTORICO         VARCHAR2(255 BYTE),
  DT_HISTORICO         DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  IE_TIPO_HISTORICO    VARCHAR2(2 BYTE),
  NR_SEQ_CANAL_BOMBA   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BOINFHIS_BOINF_FK_I ON TASY.BOMBA_INFUSAO_HIST
(NR_SEQ_BOMBA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BOINFHIS_MANEQBO_FK_I ON TASY.BOMBA_INFUSAO_HIST
(NR_SEQ_CANAL_BOMBA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BOINFHIS_PK ON TASY.BOMBA_INFUSAO_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.BOMBA_INFUSAO_HIST ADD (
  CONSTRAINT BOINFHIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.BOMBA_INFUSAO_HIST ADD (
  CONSTRAINT BOINFHIS_BOINF_FK 
 FOREIGN KEY (NR_SEQ_BOMBA) 
 REFERENCES TASY.BOMBA_INFUSAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT BOINFHIS_MANEQBO_FK 
 FOREIGN KEY (NR_SEQ_CANAL_BOMBA) 
 REFERENCES TASY.MAN_EQUIPAMENTO_BOMBA (NR_SEQUENCIA));

GRANT SELECT ON TASY.BOMBA_INFUSAO_HIST TO NIVEL_1;

