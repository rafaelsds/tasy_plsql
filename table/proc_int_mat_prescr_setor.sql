ALTER TABLE TASY.PROC_INT_MAT_PRESCR_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_INT_MAT_PRESCR_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_INT_MAT_PRESCR_SETOR
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_REGRA          NUMBER(10)              NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_CONVENIO_EXC       NUMBER(10),
  IE_TIPO_ATENDIMENTO   NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROIMPS_CONVENI_FK_I ON TASY.PROC_INT_MAT_PRESCR_SETOR
(CD_CONVENIO_EXC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROIMPS_CONVENI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROIMPS_PK ON TASY.PROC_INT_MAT_PRESCR_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROIMPS_PRINTMAP_FK_I ON TASY.PROC_INT_MAT_PRESCR_SETOR
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROIMPS_SETATEN_FK_I ON TASY.PROC_INT_MAT_PRESCR_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROIMPS_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROC_INT_MAT_PRESCR_SETOR ADD (
  CONSTRAINT PROIMPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_INT_MAT_PRESCR_SETOR ADD (
  CONSTRAINT PROIMPS_PRINTMAP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PROC_INT_MAT_PRESCR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PROIMPS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PROIMPS_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO_EXC) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.PROC_INT_MAT_PRESCR_SETOR TO NIVEL_1;

