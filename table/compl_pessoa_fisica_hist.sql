ALTER TABLE TASY.COMPL_PESSOA_FISICA_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMPL_PESSOA_FISICA_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMPL_PESSOA_FISICA_HIST
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  IE_TIPO_COMPLEMENTO     NUMBER(2)             NOT NULL,
  DS_ENDERECO             VARCHAR2(100 BYTE),
  CD_CEP                  VARCHAR2(15 BYTE),
  NR_ENDERECO             NUMBER(5),
  DS_COMPLEMENTO          VARCHAR2(40 BYTE),
  DS_BAIRRO               VARCHAR2(80 BYTE),
  DS_MUNICIPIO            VARCHAR2(40 BYTE),
  SG_ESTADO               VARCHAR2(15 BYTE),
  CD_MUNICIPIO_IBGE       VARCHAR2(6 BYTE),
  NR_SEQ_PAIS             NUMBER(10),
  CD_TIPO_LOGRADOURO      VARCHAR2(3 BYTE),
  NM_CONTATO              VARCHAR2(60 BYTE),
  NR_TELEFONE             VARCHAR2(15 BYTE),
  NR_RAMAL                NUMBER(5),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  DS_EMAIL                VARCHAR2(255 BYTE),
  CD_EMPRESA_REFER        NUMBER(10),
  CD_PROFISSAO            NUMBER(10),
  NR_IDENTIDADE           VARCHAR2(15 BYTE),
  NR_CPF                  VARCHAR2(11 BYTE),
  DS_SETOR_TRABALHO       VARCHAR2(30 BYTE),
  DS_HORARIO_TRABALHO     VARCHAR2(30 BYTE),
  DS_WEBSITE              VARCHAR2(255 BYTE),
  NR_MATRICULA_TRABALHO   VARCHAR2(20 BYTE),
  DS_FONE_ADIC            VARCHAR2(80 BYTE),
  DS_FAX                  VARCHAR2(80 BYTE),
  NR_SEQ_PARENTESCO       NUMBER(10),
  CD_PESSOA_FISICA_REF    VARCHAR2(10 BYTE),
  NR_DDD_TELEFONE         VARCHAR2(3 BYTE),
  NR_DDI_TELEFONE         VARCHAR2(3 BYTE),
  NR_DDI_FAX              VARCHAR2(3 BYTE),
  NR_DDD_FAX              VARCHAR2(3 BYTE),
  NM_CONTATO_PESQUISA     VARCHAR2(60 BYTE),
  QT_DEPENDENTE           NUMBER(2),
  DS_FONETICA             VARCHAR2(60 BYTE),
  IE_NF_CORREIO           VARCHAR2(1 BYTE),
  CD_ZONA_PROCEDENCIA     VARCHAR2(3 BYTE),
  IE_OBRIGA_EMAIL         VARCHAR2(1 BYTE),
  IE_MALA_DIRETA          VARCHAR2(1 BYTE),
  IE_FATOR_RH             VARCHAR2(1 BYTE),
  IE_TIPO_SANGUE          VARCHAR2(2 BYTE),
  IE_EMPRESA_PAGADORA     VARCHAR2(1 BYTE),
  IE_CORRESPONDENCIA      VARCHAR2(1 BYTE),
  NR_SEQ_LOCAL_ATEND_MED  NUMBER(10),
  NR_SEQ_REGIAO           NUMBER(10),
  NR_SEQ_IDENT_CNES       NUMBER(10),
  CD_PESSOA_END_REF       VARCHAR2(10 BYTE),
  NR_SEQ_END_REF          NUMBER(3),
  NR_DDI_FONE_ADIC        VARCHAR2(3 BYTE),
  NR_DDD_FONE_ADIC        VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COPEFIH_CNESIDE_FK_I ON TASY.COMPL_PESSOA_FISICA_HIST
(NR_SEQ_IDENT_CNES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPEFIH_CNESIDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPEFIH_I ON TASY.COMPL_PESSOA_FISICA_HIST
(IE_TIPO_COMPLEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPEFIH_I
  MONITORING USAGE;


CREATE INDEX TASY.COPEFIH_LOCATME_FK_I ON TASY.COMPL_PESSOA_FISICA_HIST
(NR_SEQ_LOCAL_ATEND_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPEFIH_LOCATME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPEFIH_PAIS_FK_I ON TASY.COMPL_PESSOA_FISICA_HIST
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPEFIH_PAIS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPEFIH_PESFISI_FK_I ON TASY.COMPL_PESSOA_FISICA_HIST
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COPEFIH_PK ON TASY.COMPL_PESSOA_FISICA_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPEFIH_PK
  MONITORING USAGE;


CREATE INDEX TASY.COPEFIH_SUSMUNI_FK_I ON TASY.COMPL_PESSOA_FISICA_HIST
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPEFIH_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPEFIH_SUSMURE_FK_I ON TASY.COMPL_PESSOA_FISICA_HIST
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPEFIH_SUSMURE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPEFIH_SUSTILO_FK_I ON TASY.COMPL_PESSOA_FISICA_HIST
(CD_TIPO_LOGRADOURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPEFIH_SUSTILO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.COMPL_PESSOA_FISICA_HIST_tp  after update ON TASY.COMPL_PESSOA_FISICA_HIST FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SANGUE,1,4000),substr(:new.IE_TIPO_SANGUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SANGUE',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_COMPLEMENTO,1,4000),substr(:new.IE_TIPO_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPLEMENTO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAIS,1,4000),substr(:new.NR_SEQ_PAIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAIS',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_LOGRADOURO,1,4000),substr(:new.CD_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOGRADOURO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FATOR_RH,1,4000),substr(:new.IE_FATOR_RH,1,4000),:new.nm_usuario,nr_seq_w,'IE_FATOR_RH',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'COMPL_PESSOA_FISICA_HIST',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.COMPL_PESSOA_FISICA_HIST ADD (
  CONSTRAINT COPEFIH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COMPL_PESSOA_FISICA_HIST ADD (
  CONSTRAINT COPEFIH_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT COPEFIH_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA),
  CONSTRAINT COPEFIH_CNESIDE_FK 
 FOREIGN KEY (NR_SEQ_IDENT_CNES) 
 REFERENCES TASY.CNES_IDENTIFICACAO (NR_SEQUENCIA),
  CONSTRAINT COPEFIH_LOCATME_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_ATEND_MED) 
 REFERENCES TASY.LOCAL_ATENDIMENTO_MEDICO (NR_SEQUENCIA),
  CONSTRAINT COPEFIH_SUSMURE_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.SUS_MUNICIPIO_REGIAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.COMPL_PESSOA_FISICA_HIST TO NIVEL_1;

