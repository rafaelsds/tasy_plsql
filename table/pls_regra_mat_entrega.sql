ALTER TABLE TASY.PLS_REGRA_MAT_ENTREGA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_MAT_ENTREGA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_MAT_ENTREGA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_REGRA_ENTREGA  NUMBER(10)              NOT NULL,
  NR_SEQ_MATERIAL       NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_MATERIAL           NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRMAEN_MATERIA_FK_I ON TASY.PLS_REGRA_MAT_ENTREGA
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRMAEN_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSRMAEN_PK ON TASY.PLS_REGRA_MAT_ENTREGA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRMAEN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRMAEN_PLSMAT_FK_I ON TASY.PLS_REGRA_MAT_ENTREGA
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRMAEN_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRMAEN_PLSREMM_FK_I ON TASY.PLS_REGRA_MAT_ENTREGA
(NR_SEQ_REGRA_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRMAEN_PLSREMM_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_MAT_ENTREGA ADD (
  CONSTRAINT PLSRMAEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_MAT_ENTREGA ADD (
  CONSTRAINT PLSRMAEN_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PLSRMAEN_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSRMAEN_PLSREMM_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ENTREGA) 
 REFERENCES TASY.PLS_REGRA_ENTREGA_MAT_MED (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_MAT_ENTREGA TO NIVEL_1;

