ALTER TABLE TASY.MOTIVO_EXC_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOTIVO_EXC_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOTIVO_EXC_CONTA
(
  CD_MOTIVO_EXC_CONTA  NUMBER(3)                NOT NULL,
  DS_MOTIVO_EXC_CONTA  VARCHAR2(100 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_GRUPO_MOTIVO      NUMBER(3),
  IE_EXCLUSAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_AJUSTE            VARCHAR2(1 BYTE)         NOT NULL,
  IE_CUSTO_HOSPITAL    VARCHAR2(1 BYTE),
  DS_REGRA_UTILIZACAO  VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_GRUPO_EXCLUSAO    VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MOTEXCO_PK ON TASY.MOTIVO_EXC_CONTA
(CD_MOTIVO_EXC_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MOTIVO_EXC_CONTA ADD (
  CONSTRAINT MOTEXCO_PK
 PRIMARY KEY
 (CD_MOTIVO_EXC_CONTA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MOTIVO_EXC_CONTA TO NIVEL_1;

