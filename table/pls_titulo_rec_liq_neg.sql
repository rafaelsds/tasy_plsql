ALTER TABLE TASY.PLS_TITULO_REC_LIQ_NEG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TITULO_REC_LIQ_NEG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TITULO_REC_LIQ_NEG
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_NEG_MENS_ITEM  NUMBER(10)              NOT NULL,
  NR_TITULO             NUMBER(10)              NOT NULL,
  NR_SEQ_BAIXA          NUMBER(10)              NOT NULL,
  VL_BAIXA              NUMBER(15,2)            NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_SEGURADO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTCLN_PK ON TASY.PLS_TITULO_REC_LIQ_NEG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTCLN_PLSNMIT_FK_I ON TASY.PLS_TITULO_REC_LIQ_NEG
(NR_SEQ_NEG_MENS_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTCLN_PLSSEGU_FK_I ON TASY.PLS_TITULO_REC_LIQ_NEG
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTCLN_TIRELIQ_FK_I ON TASY.PLS_TITULO_REC_LIQ_NEG
(NR_TITULO, NR_SEQ_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_titulo_rec_liq_neg_after
after insert or update ON TASY.PLS_TITULO_REC_LIQ_NEG for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w				number(10);

begin

if (:new.nr_seq_baixa is not null) and (:new.nr_titulo is not null) then

	/*Esse select � para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and		nr_seq_item_documento	= to_char(:new.nr_seq_baixa)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if (qt_reg_w = 0) then
		reg_integracao_p.nr_seq_item_documento_p :=	:new.nr_seq_baixa;
		gerar_int_padrao.gravar_integracao('27', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);
	end if;

end if;

end;
/


ALTER TABLE TASY.PLS_TITULO_REC_LIQ_NEG ADD (
  CONSTRAINT PLSTCLN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TITULO_REC_LIQ_NEG ADD (
  CONSTRAINT PLSTCLN_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSTCLN_PLSNMIT_FK 
 FOREIGN KEY (NR_SEQ_NEG_MENS_ITEM) 
 REFERENCES TASY.PLS_NEGOCIACAO_MENS_ITEM (NR_SEQUENCIA),
  CONSTRAINT PLSTCLN_TIRELIQ_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_BAIXA) 
 REFERENCES TASY.TITULO_RECEBER_LIQ (NR_TITULO,NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TITULO_REC_LIQ_NEG TO NIVEL_1;

