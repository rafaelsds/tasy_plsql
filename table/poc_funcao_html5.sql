ALTER TABLE TASY.POC_FUNCAO_HTML5
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.POC_FUNCAO_HTML5 CASCADE CONSTRAINTS;

CREATE TABLE TASY.POC_FUNCAO_HTML5
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_POC           NUMBER(10)               NOT NULL,
  NR_SEQ_FUNCAO        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFHTML_FUNHTML_FK_I ON TASY.POC_FUNCAO_HTML5
(NR_SEQ_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFHTML_PK ON TASY.POC_FUNCAO_HTML5
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PFHTML_POHTML_FK_I ON TASY.POC_FUNCAO_HTML5
(NR_SEQ_POC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.POC_FUNCAO_HTML5_tp  after update ON TASY.POC_FUNCAO_HTML5 FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'POC_FUNCAO_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'POC_FUNCAO_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_FUNCAO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_FUNCAO,1,4000),substr(:new.NR_SEQ_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FUNCAO',ie_log_w,ds_w,'POC_FUNCAO_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_NREC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'POC_FUNCAO_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_POC,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_POC,1,4000),substr(:new.NR_SEQ_POC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_POC',ie_log_w,ds_w,'POC_FUNCAO_HTML5',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'POC_FUNCAO_HTML5',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.POC_FUNCAO_HTML5 ADD (
  CONSTRAINT PFHTML_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.POC_FUNCAO_HTML5 ADD (
  CONSTRAINT PFHTML_FUNHTML_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO) 
 REFERENCES TASY.FUNCOES_HTML5 (NR_SEQUENCIA),
  CONSTRAINT PFHTML_POHTML_FK 
 FOREIGN KEY (NR_SEQ_POC) 
 REFERENCES TASY.POC_HTML5 (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.POC_FUNCAO_HTML5 TO NIVEL_1;

