ALTER TABLE TASY.PROJ_CRON_ETAPA_AUDIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_CRON_ETAPA_AUDIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_CRON_ETAPA_AUDIT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_STATUS              VARCHAR2(2 BYTE)       NOT NULL,
  IE_PENDENCIA           VARCHAR2(2 BYTE),
  NR_SEQ_TIPO_DOCUMENTO  NUMBER(10),
  DS_ATIVIDADE           VARCHAR2(255 BYTE)     NOT NULL,
  DS_PROBLEMA            VARCHAR2(4000 BYTE),
  CD_COORDENADOR         VARCHAR2(10 BYTE),
  NR_SEQ_ETAPA           NUMBER(10)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCEAUD_PESFISI_FK_I ON TASY.PROJ_CRON_ETAPA_AUDIT
(CD_COORDENADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PCEAUD_PK ON TASY.PROJ_CRON_ETAPA_AUDIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCEAUD_PROCRET_FK_I ON TASY.PROJ_CRON_ETAPA_AUDIT
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCEAUD_PROJTPDOC_FK_I ON TASY.PROJ_CRON_ETAPA_AUDIT
(NR_SEQ_TIPO_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROJ_CRON_ETAPA_AUDIT ADD (
  CONSTRAINT PCEAUD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PROJ_CRON_ETAPA_AUDIT ADD (
  CONSTRAINT PCEAUD_PESFISI_FK 
 FOREIGN KEY (CD_COORDENADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PCEAUD_PROCRET_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.PROJ_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT PCEAUD_PROJTPDOC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOCUMENTO) 
 REFERENCES TASY.PROJ_TIPO_DOCUMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_CRON_ETAPA_AUDIT TO NIVEL_1;

