ALTER TABLE TASY.PLS_REGRA_QTDE_PACOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_QTDE_PACOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_QTDE_PACOTE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  QT_INICIAL                NUMBER(7,3)         NOT NULL,
  QT_LANCAMENTO             NUMBER(7,3)         NOT NULL,
  NR_SEQ_REGRA_CONV_PACOTE  NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  QT_FINAL                  NUMBER(7,3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRQPA_PK ON TASY.PLS_REGRA_QTDE_PACOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRQPA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRQPA_PLSRECP_FK_I ON TASY.PLS_REGRA_QTDE_PACOTE
(NR_SEQ_REGRA_CONV_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRQPA_PLSRECP_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_QTDE_PACOTE_tp  after update ON TASY.PLS_REGRA_QTDE_PACOTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_REGRA_QTDE_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LANCAMENTO,1,4000),substr(:new.QT_LANCAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'QT_LANCAMENTO',ie_log_w,ds_w,'PLS_REGRA_QTDE_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_FINAL,1,4000),substr(:new.QT_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_FINAL',ie_log_w,ds_w,'PLS_REGRA_QTDE_PACOTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_INICIAL,1,4000),substr(:new.QT_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_INICIAL',ie_log_w,ds_w,'PLS_REGRA_QTDE_PACOTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_QTDE_PACOTE ADD (
  CONSTRAINT PLSRQPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_QTDE_PACOTE ADD (
  CONSTRAINT PLSRQPA_PLSRECP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONV_PACOTE) 
 REFERENCES TASY.PLS_REGRA_CONVERSAO_PACOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_QTDE_PACOTE TO NIVEL_1;

