ALTER TABLE TASY.PLS_MOVIMENTACAO_BENEF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MOVIMENTACAO_BENEF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MOVIMENTACAO_BENEF
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_LOTE              NUMBER(10)           NOT NULL,
  NR_SEQ_SEGURADO          NUMBER(10)           NOT NULL,
  IE_TIPO_MOVIMENTACAO     VARCHAR2(1 BYTE)     NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_TITULAR           NUMBER(10),
  DS_ENDERECO              VARCHAR2(100 BYTE),
  CD_CEP                   VARCHAR2(15 BYTE),
  NR_ENDERECO              NUMBER(5),
  DS_COMPLEMENTO           VARCHAR2(40 BYTE),
  DS_BAIRRO                VARCHAR2(40 BYTE),
  DS_MUNICIPIO             VARCHAR2(40 BYTE),
  SG_ESTADO                VARCHAR2(2 BYTE),
  NR_TELEFONE              VARCHAR2(15 BYTE),
  DT_NASCIMENTO            DATE,
  IE_SEXO                  VARCHAR2(1 BYTE),
  IE_ESTADO_CIVIL          VARCHAR2(2 BYTE),
  NR_CPF                   VARCHAR2(11 BYTE),
  NR_IDENTIDADE            VARCHAR2(15 BYTE),
  NR_TELEFONE_CELULAR      VARCHAR2(40 BYTE),
  NR_SEQ_PARENTESCO        NUMBER(10),
  NM_BENEFICIARIO          VARCHAR2(255 BYTE),
  DT_CONTRATACAO           DATE,
  CD_COD_ANTERIOR          VARCHAR2(20 BYTE),
  CD_COD_ANTERIOR_TITULAR  VARCHAR2(20 BYTE),
  VL_PRECO_SEGURADO        NUMBER(15,2),
  NM_MAE                   VARCHAR2(255 BYTE),
  CD_PLANO                 VARCHAR2(20 BYTE),
  CD_MATRICULA_FAMILIA     NUMBER(10),
  NR_SEQUENCIAL_FAMILIA    NUMBER(10),
  DT_NASCIMENTO_MAE        DATE,
  DS_EMAIL                 VARCHAR2(255 BYTE),
  CD_CARGO                 NUMBER(10),
  NR_PIS_PASEP             VARCHAR2(11 BYTE),
  DT_ADMISSAO              DATE,
  CD_USUARIO_PLANO         VARCHAR2(30 BYTE),
  DS_PLANO                 VARCHAR2(255 BYTE),
  DT_RESCISAO              DATE,
  CD_MATRICULA             VARCHAR2(20 BYTE),
  NR_SEQ_PAGADOR           NUMBER(10),
  CD_DECLARACAO_NASC_VIVO  VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMVBE_CARGO_FK_I ON TASY.PLS_MOVIMENTACAO_BENEF
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMVBE_CARGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMVBE_GRAUPA_FK_I ON TASY.PLS_MOVIMENTACAO_BENEF
(NR_SEQ_PARENTESCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMVBE_GRAUPA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSMVBE_PK ON TASY.PLS_MOVIMENTACAO_BENEF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMVBE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSMVBE_PLCONPAG_FK_I ON TASY.PLS_MOVIMENTACAO_BENEF
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMVBE_PLCONPAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMVBE_PLSLMBE_FK_I ON TASY.PLS_MOVIMENTACAO_BENEF
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMVBE_PLSLMBE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMVBE_PLSSEGU_FK_I ON TASY.PLS_MOVIMENTACAO_BENEF
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMVBE_PLSSEGU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMVBE_PLSSEGU_FK2_I ON TASY.PLS_MOVIMENTACAO_BENEF
(NR_SEQ_TITULAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMVBE_PLSSEGU_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_MOVIMENTACAO_BENEF_tp  after update ON TASY.PLS_MOVIMENTACAO_BENEF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PIS_PASEP,1,4000),substr(:new.NR_PIS_PASEP,1,4000),:new.nm_usuario,nr_seq_w,'NR_PIS_PASEP',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTADO_CIVIL,1,4000),substr(:new.IE_ESTADO_CIVIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTADO_CIVIL',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_IDENTIDADE,1,4000),substr(:new.NR_IDENTIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_IDENTIDADE',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_NASCIMENTO,1,4000),substr(:new.DT_NASCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE_CELULAR,1,4000),substr(:new.NR_TELEFONE_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE_CELULAR',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'PLS_MOVIMENTACAO_BENEF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_MOVIMENTACAO_BENEF ADD (
  CONSTRAINT PLSMVBE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MOVIMENTACAO_BENEF ADD (
  CONSTRAINT PLSMVBE_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT PLSMVBE_PLSLMBE_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_LOTE_MOV_BENEF (NR_SEQUENCIA),
  CONSTRAINT PLSMVBE_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSMVBE_PLSSEGU_FK2 
 FOREIGN KEY (NR_SEQ_TITULAR) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSMVBE_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT PLSMVBE_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MOVIMENTACAO_BENEF TO NIVEL_1;

