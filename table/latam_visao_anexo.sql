ALTER TABLE TASY.LATAM_VISAO_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LATAM_VISAO_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.LATAM_VISAO_ANEXO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_GAP           NUMBER(10)               NOT NULL,
  NR_SEQ_MODULO        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LATAMVAN_LATAMGAP_FK_I ON TASY.LATAM_VISAO_ANEXO
(NR_SEQ_GAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATAMVAN_LATAMGAP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LATAMVAN_LATMODU_FK_I ON TASY.LATAM_VISAO_ANEXO
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATAMVAN_LATMODU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LATAMVAN_PK ON TASY.LATAM_VISAO_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LATAMVAN_PK
  MONITORING USAGE;


ALTER TABLE TASY.LATAM_VISAO_ANEXO ADD (
  CONSTRAINT LATAMVAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LATAM_VISAO_ANEXO ADD (
  CONSTRAINT LATAMVAN_LATAMGAP_FK 
 FOREIGN KEY (NR_SEQ_GAP) 
 REFERENCES TASY.LATAM_GAP (NR_SEQUENCIA),
  CONSTRAINT LATAMVAN_LATMODU_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.LATAM_MODULO (NR_SEQUENCIA));

GRANT SELECT ON TASY.LATAM_VISAO_ANEXO TO NIVEL_1;

