ALTER TABLE TASY.FORMULARIO_TRANSPORTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FORMULARIO_TRANSPORTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.FORMULARIO_TRANSPORTE
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  IE_DOENCA_TERCEIROS            VARCHAR2(1 BYTE),
  DS_OUTROS_DOENCA_TERCEIRO      VARCHAR2(2000 BYTE),
  IE_INTERNACAO                  VARCHAR2(1 BYTE),
  DS_OBSERV_INTERNACAO           VARCHAR2(2000 BYTE),
  IE_CIRURGIA_AMB                VARCHAR2(1 BYTE),
  DS_OBSERV_CIRURGIA             VARCHAR2(2000 BYTE),
  NR_ATENDIMENTO                 NUMBER(10)     NOT NULL,
  IE_CONSULTA_AMBULATORIAL       VARCHAR2(1 BYTE),
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  DS_OUTROS_SA                   VARCHAR2(2000 BYTE),
  DT_LIBERACAO                   DATE,
  DT_INATIVACAO                  DATE,
  NM_USUARIO_INATIVACAO          VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA               VARCHAR2(255 BYTE),
  IE_MOTIVO_EXCECAO              VARCHAR2(1 BYTE),
  DS_MOTIVO_EXCECAO              VARCHAR2(2000 BYTE),
  QT_PERIODICIDADE_SEMANAL       NUMBER(10),
  QT_PERIODO_MENSAL              NUMBER(10),
  QT_PERIODO_TOTAL               NUMBER(10),
  IE_RESTRICAO_MOBILIDADE        VARCHAR2(1 BYTE),
  DS_MOTIVO_RESTRICAO            VARCHAR2(2000 BYTE),
  QT_DURACAO_PREVISTA            NUMBER(10),
  IE_MEIO_TRANSPORTE             VARCHAR2(1 BYTE),
  DS_OUTROS_MEIOS_TRANSPORTE     VARCHAR2(2000 BYTE),
  DS_MOTIVO_MEIO_TRANSPORTE      VARCHAR2(2000 BYTE),
  IE_EQUIPAMENTO_MEDICO          VARCHAR2(1 BYTE),
  DS_OUTROS_EQUIPAMENTOS         VARCHAR2(2000 BYTE),
  IE_TRAJETO                     VARCHAR2(1 BYTE),
  IE_DE_RESIDENCIA               VARCHAR2(1 BYTE),
  IE_PARA_RESIDENCIA             VARCHAR2(1 BYTE),
  IE_DE_CONSULTORIO              VARCHAR2(1 BYTE),
  IE_PARA_CONSULTORIO            VARCHAR2(1 BYTE),
  IE_DE_HOSPITAL                 VARCHAR2(1 BYTE),
  IE_PARA_HOSPITAL               VARCHAR2(1 BYTE),
  IE_DE_OUTROS                   VARCHAR2(1 BYTE),
  IE_PARA_OUTROS                 VARCHAR2(1 BYTE),
  DS_OUTROS_DE_PARA              VARCHAR2(2000 BYTE),
  IE_DE                          VARCHAR2(1 BYTE),
  IE_PARA                        VARCHAR2(1 BYTE),
  QT_TEMPO_ESPERA                NUMBER(10),
  QT_PASSAGEIROS                 NUMBER(10),
  IE_NECESSIDADE_ACOMPANHAMENTO  VARCHAR2(1 BYTE),
  DS_ACOMPANHAMENTOS             VARCHAR2(2000 BYTE),
  IE_AUTORIZACAO_CONVENIO        VARCHAR2(1 BYTE),
  DS_MOTIVO_APROVACAO            VARCHAR2(2000 BYTE),
  DS_MOTIVO_DESAPROVACAO         VARCHAR2(2000 BYTE),
  CD_PESSOA_FISICA               VARCHAR2(10 BYTE),
  IE_TAXA                        VARCHAR2(1 BYTE) DEFAULT null,
  CD_ESPECIALIDADE               NUMBER(5),
  IE_MOTIVO_PASSEIO              VARCHAR2(1 BYTE),
  DT_INICIO_TRATAMENTO           DATE,
  IE_MOTIVO_RESTRICAO            VARCHAR2(1 BYTE),
  IE_TRATAMENTO_FREQ             VARCHAR2(1 BYTE),
  CD_MEDICO                      VARCHAR2(10 BYTE),
  DS_LOCAL_TRATAMENTO            VARCHAR2(255 BYTE),
  IE_TAXI                        VARCHAR2(1 BYTE),
  IE_MOTIVO_MEIO_TRANSPORTE      VARCHAR2(1 BYTE),
  DS_MOTIVO_PASSEIO              VARCHAR2(255 BYTE),
  DT_FIM_TRATAMENTO              DATE,
  DS_MOTIVO_TRANSPORTE           VARCHAR2(255 BYTE),
  NR_RQE                         VARCHAR2(20 BYTE),
  NR_BSNR                        VARCHAR2(25 BYTE),
  IE_TRATAMENTO_AMBULATORIAL     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FORTRAN_ATEPACI_FK_I ON TASY.FORMULARIO_TRANSPORTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORTRAN_ESPMEDI_FK_I ON TASY.FORMULARIO_TRANSPORTE
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORTRAN_PESFISI_FK_I ON TASY.FORMULARIO_TRANSPORTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORTRAN_PESFISI_FK2_I ON TASY.FORMULARIO_TRANSPORTE
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FORTRAN_PK ON TASY.FORMULARIO_TRANSPORTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FORMULARIO_TRANSPORTE_UPDATE
before update ON TASY.FORMULARIO_TRANSPORTE for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';

begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

    select	 decode (count(*),0,'N','S')
    into 	is_rule_exists_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO  = 'KV4'
    and   	obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');

    if (is_rule_exists_w = 'S') then
    select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO  = 'KV4'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


		wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
    end if;
	end if;
end;
/


ALTER TABLE TASY.FORMULARIO_TRANSPORTE ADD (
  CONSTRAINT FORTRAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FORMULARIO_TRANSPORTE ADD (
  CONSTRAINT FORTRAN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT FORTRAN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FORTRAN_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT FORTRAN_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.FORMULARIO_TRANSPORTE TO NIVEL_1;

