DROP TABLE TASY.W_INTEGRACAO_LAUDO_BCO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INTEGRACAO_LAUDO_BCO
(
  NR_SEQ_INTERNO      NUMBER(10),
  NR_ACCES_NUMBER     VARCHAR2(50 BYTE),
  NR_LAUDO_EXTERNO    NUMBER(10),
  NR_PRESCRICAO       NUMBER(14),
  DS_TITULO_LAUDO     VARCHAR2(255 BYTE),
  CD_MEDICO_LAUDANTE  VARCHAR2(10 BYTE),
  NR_CRM              VARCHAR2(20 BYTE),
  UF_CRM              VARCHAR2(2 BYTE),
  DT_LAUDO            DATE,
  DT_LIBERACAO        DATE,
  IE_STATUS_LAUDO     VARCHAR2(15 BYTE),
  DS_LAUDO            CLOB,
  DT_INTEGRACAO       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_LAUDO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_INTEGRACAO_LAUDO_BCO TO NIVEL_1;

