ALTER TABLE TASY.REGRA_ENVIO_COMUNIC_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_ENVIO_COMUNIC_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_ENVIO_COMUNIC_EVENTO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_REGRA           NUMBER(10)             NOT NULL,
  CD_EVENTO              VARCHAR2(3 BYTE)       NOT NULL,
  DS_TITULO              VARCHAR2(80 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_SETOR_DESTINO       NUMBER(5),
  NM_USUARIOS_ADIC       VARCHAR2(255 BYTE),
  DS_COMUNICACAO         VARCHAR2(2000 BYTE)    NOT NULL,
  CD_PERFIL              NUMBER(5),
  IE_CI_LIDA             VARCHAR2(1 BYTE)       NOT NULL,
  DS_MACROS_DISPONIVEIS  VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REENCEV_PERFIL_FK_I ON TASY.REGRA_ENVIO_COMUNIC_EVENTO
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REENCEV_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REENCEV_PK ON TASY.REGRA_ENVIO_COMUNIC_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REENCEV_REECOCO_FK_I ON TASY.REGRA_ENVIO_COMUNIC_EVENTO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REENCEV_SETATEN_FK_I ON TASY.REGRA_ENVIO_COMUNIC_EVENTO
(CD_SETOR_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REENCEV_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_ENVIO_COMUNIC_EVENTO_tp  after update ON TASY.REGRA_ENVIO_COMUNIC_EVENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_TITULO,1,4000),substr(:new.DS_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TITULO',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA,1,4000),substr(:new.NR_SEQ_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EVENTO,1,4000),substr(:new.CD_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EVENTO',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MACROS_DISPONIVEIS,1,4000),substr(:new.DS_MACROS_DISPONIVEIS,1,4000),:new.nm_usuario,nr_seq_w,'DS_MACROS_DISPONIVEIS',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_DESTINO,1,4000),substr(:new.CD_SETOR_DESTINO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_DESTINO',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIOS_ADIC,1,4000),substr(:new.NM_USUARIOS_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIOS_ADIC',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CI_LIDA,1,4000),substr(:new.IE_CI_LIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CI_LIDA',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMUNICACAO,1,4000),substr(:new.DS_COMUNICACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMUNICACAO',ie_log_w,ds_w,'REGRA_ENVIO_COMUNIC_EVENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_ENVIO_COMUNIC_EVENTO ADD (
  CONSTRAINT REENCEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_ENVIO_COMUNIC_EVENTO ADD (
  CONSTRAINT REENCEV_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT REENCEV_REECOCO_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_ENVIO_COMUNIC_COMPRA (NR_SEQUENCIA),
  CONSTRAINT REENCEV_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_DESTINO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.REGRA_ENVIO_COMUNIC_EVENTO TO NIVEL_1;

