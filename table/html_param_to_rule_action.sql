ALTER TABLE TASY.HTML_PARAM_TO_RULE_ACTION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HTML_PARAM_TO_RULE_ACTION CASCADE CONSTRAINTS;

CREATE TABLE TASY.HTML_PARAM_TO_RULE_ACTION
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_RULE               NUMBER(10)          NOT NULL,
  IE_TIPO_ACAO              VARCHAR2(15 BYTE)   NOT NULL,
  NR_SEQ_OBJ_SCHEMATIC      NUMBER(10),
  NR_SEQ_DIC_OBJETO         NUMBER(10),
  NR_SEQ_DIC_OBJETO_FILTRO  NUMBER(10),
  NM_ATRIBUTO               VARCHAR2(50 BYTE),
  NR_SEQ_VISAO              NUMBER(10),
  SI_COMPONENT_VISIBLE      VARCHAR2(1 BYTE),
  SI_INSERT                 VARCHAR2(2 BYTE),
  SI_UPDATE                 VARCHAR2(2 BYTE),
  SI_DELETE                 VARCHAR2(2 BYTE),
  SI_DUPLICATE              VARCHAR2(2 BYTE),
  SI_FIELD_ENABLE           VARCHAR2(1 BYTE),
  SI_FIELD_VISIBLE          VARCHAR2(1 BYTE),
  DS_FIELD_DEFAULT_VALUE    VARCHAR2(255 BYTE),
  SI_FIELD_MANDATORY        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HTMPTRA_HTMPTRU_FK_I ON TASY.HTML_PARAM_TO_RULE_ACTION
(NR_SEQ_RULE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HTMPTRA_PK ON TASY.HTML_PARAM_TO_RULE_ACTION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.HTML_PARAM_TO_RULE_ACTION_tp  after update ON TASY.HTML_PARAM_TO_RULE_ACTION FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.SI_DELETE,1,4000),substr(:new.SI_DELETE,1,4000),:new.nm_usuario,nr_seq_w,'SI_DELETE',ie_log_w,ds_w,'HTML_PARAM_TO_RULE_ACTION',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SI_UPDATE,1,4000),substr(:new.SI_UPDATE,1,4000),:new.nm_usuario,nr_seq_w,'SI_UPDATE',ie_log_w,ds_w,'HTML_PARAM_TO_RULE_ACTION',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SI_INSERT,1,4000),substr(:new.SI_INSERT,1,4000),:new.nm_usuario,nr_seq_w,'SI_INSERT',ie_log_w,ds_w,'HTML_PARAM_TO_RULE_ACTION',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SI_DUPLICATE,1,4000),substr(:new.SI_DUPLICATE,1,4000),:new.nm_usuario,nr_seq_w,'SI_DUPLICATE',ie_log_w,ds_w,'HTML_PARAM_TO_RULE_ACTION',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.HTML_PARAM_TO_RULE_ACTION ADD (
  CONSTRAINT HTMPTRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HTML_PARAM_TO_RULE_ACTION ADD (
  CONSTRAINT HTMPTRA_HTMPTRU_FK 
 FOREIGN KEY (NR_SEQ_RULE) 
 REFERENCES TASY.HTML_PARAM_TO_RULE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HTML_PARAM_TO_RULE_ACTION TO NIVEL_1;

