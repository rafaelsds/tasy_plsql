ALTER TABLE TASY.PROCESSO_APROVACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCESSO_APROVACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCESSO_APROVACAO
(
  CD_PROCESSO_APROV    NUMBER(10)               NOT NULL,
  DS_PROCESSO_APROV    VARCHAR2(80 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4),
  QT_MINIMO_APROVADOR  NUMBER(5),
  IE_APROVACAO_NIVEL   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROAPRO_ESTABEL_FK_I ON TASY.PROCESSO_APROVACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAPRO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROAPRO_PK ON TASY.PROCESSO_APROVACAO
(CD_PROCESSO_APROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PROCESSO_APROVACAO_tp  after update ON TASY.PROCESSO_APROVACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_PROCESSO_APROV);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PROCESSO_APROV,1,4000),substr(:new.CD_PROCESSO_APROV,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCESSO_APROV',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROCESSO_APROV,1,4000),substr(:new.DS_PROCESSO_APROV,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROCESSO_APROV',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APROVACAO_NIVEL,1,4000),substr(:new.IE_APROVACAO_NIVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_APROVACAO_NIVEL',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMO_APROVADOR,1,4000),substr(:new.QT_MINIMO_APROVADOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMO_APROVADOR',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'PROCESSO_APROVACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PROCESSO_APROVACAO ADD (
  CONSTRAINT PROAPRO_PK
 PRIMARY KEY
 (CD_PROCESSO_APROV)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCESSO_APROVACAO ADD (
  CONSTRAINT PROAPRO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PROCESSO_APROVACAO TO NIVEL_1;

