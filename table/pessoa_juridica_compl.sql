ALTER TABLE TASY.PESSOA_JURIDICA_COMPL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JURIDICA_COMPL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JURIDICA_COMPL
(
  CD_CGC                    VARCHAR2(14 BYTE)   NOT NULL,
  NR_SEQUENCIA              NUMBER(5)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DS_ENDERECO               VARCHAR2(60 BYTE),
  NR_ENDERECO               VARCHAR2(10 BYTE),
  DS_COMPLEMENTO            VARCHAR2(100 BYTE),
  DS_BAIRRO                 VARCHAR2(40 BYTE),
  DS_MUNICIPIO              VARCHAR2(40 BYTE),
  SG_ESTADO                 VARCHAR2(15 BYTE),
  CD_CEP                    VARCHAR2(15 BYTE),
  NR_TELEFONE               VARCHAR2(15 BYTE),
  NR_FAX                    VARCHAR2(15 BYTE),
  DS_EMAIL                  VARCHAR2(80 BYTE),
  NR_RAMAL_CONTATO          NUMBER(5),
  NM_PESSOA_CONTATO         VARCHAR2(40 BYTE),
  IE_TIPO_COMPLEMENTO       NUMBER(2)           NOT NULL,
  NR_TELEFONE_CELULAR       VARCHAR2(20 BYTE),
  CD_CBO_RED                NUMBER(6),
  DS_CARGO                  VARCHAR2(255 BYTE),
  CD_MUNICIPIO_IBGE         VARCHAR2(6 BYTE),
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_RECEBE_PROP            VARCHAR2(1 BYTE),
  NR_DDD_TELEFONE           VARCHAR2(3 BYTE),
  DS_SETOR_CONTATO          VARCHAR2(255 BYTE),
  DS_IDENTIFIC_COMPLEMENTO  VARCHAR2(255 BYTE),
  NR_SEQ_REGIAO             NUMBER(10),
  NR_SEQ_IDENT_CNES         NUMBER(10),
  IE_RECEBE_REVISTA         VARCHAR2(1 BYTE),
  DS_OBSERVACAO             VARCHAR2(2000 BYTE),
  IE_GERENTE_CANAL          VARCHAR2(1 BYTE),
  CD_PESSOA_FISICA_CANAL    VARCHAR2(10 BYTE),
  IE_PLS                    VARCHAR2(1 BYTE),
  IE_CONTATO_REF            VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_LOGRADOURO    NUMBER(10),
  IE_PARTICIPA_EVENTOS      VARCHAR2(1 BYTE),
  IE_PLC_CUSTOMER_SPONSOR   VARCHAR2(1 BYTE),
  IE_PLC_PLATAFORM_MANAGER  VARCHAR2(1 BYTE),
  NR_DDI_TELEFONE           VARCHAR2(3 BYTE),
  NR_DDI_FAX                VARCHAR2(3 BYTE),
  NR_DDI_CELULAR            VARCHAR2(3 BYTE),
  IE_PLC_MATRICULA_CONC     VARCHAR2(1 BYTE),
  NR_TOTAL_FUNCIONARIOS     NUMBER(10),
  IE_CONTRIBUINTE_RECEITA   VARCHAR2(2 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4),
  NR_SEQ_PESSOA_ENDERECO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESJUCP_CBORED_FK_I ON TASY.PESSOA_JURIDICA_COMPL
(CD_CBO_RED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCP_CBORED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCP_CNESIDE_FK_I ON TASY.PESSOA_JURIDICA_COMPL
(NR_SEQ_IDENT_CNES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUCP_CNSTILO_FK_I ON TASY.PESSOA_JURIDICA_COMPL
(NR_SEQ_TIPO_LOGRADOURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUCP_ESTABEL_FK_I ON TASY.PESSOA_JURIDICA_COMPL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUCP_PESFISI_FK_I ON TASY.PESSOA_JURIDICA_COMPL
(CD_PESSOA_FISICA_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUCP_PESJURI_FK_I ON TASY.PESSOA_JURIDICA_COMPL
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUCP_PESSEND_FK_I ON TASY.PESSOA_JURIDICA_COMPL
(NR_SEQ_PESSOA_ENDERECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESJUCP_PK ON TASY.PESSOA_JURIDICA_COMPL
(CD_CGC, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUCP_SUSMUNI_FK_I ON TASY.PESSOA_JURIDICA_COMPL
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCP_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCP_SUSMURE_FK_I ON TASY.PESSOA_JURIDICA_COMPL
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCP_SUSMURE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_compl_afterDel
after delete ON TASY.PESSOA_JURIDICA_COMPL for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
cd_tipo_pessoa_w	pessoa_juridica.cd_tipo_pessoa%type;

begin

select  obter_tipo_pessoa_juridica(:old.cd_cgc)
into	cd_tipo_pessoa_w
from    dual;

reg_integracao_p.ie_operacao		:=	'I';
reg_integracao_p.cd_tipo_pessoa		:=	cd_tipo_pessoa_w;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :old.cd_cgc,:old.nm_usuario, reg_integracao_p);

end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_JURIDICA_COMPL_tp  after update ON TASY.PESSOA_JURIDICA_COMPL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_CGC='||to_char(:old.CD_CGC)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA);gravar_log_alteracao(substr(:old.CD_CBO_RED,1,4000),substr(:new.CD_CBO_RED,1,4000),:new.nm_usuario,nr_seq_w,'CD_CBO_RED',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_LOGRADOURO,1,4000),substr(:new.NR_SEQ_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_LOGRADOURO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_FAX,1,4000),substr(:new.NR_FAX,1,4000),:new.nm_usuario,nr_seq_w,'NR_FAX',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RAMAL_CONTATO,1,4000),substr(:new.NR_RAMAL_CONTATO,1,4000),:new.nm_usuario,nr_seq_w,'NR_RAMAL_CONTATO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_COMPLEMENTO,1,4000),substr(:new.IE_TIPO_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPLEMENTO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_PESSOA_CONTATO,1,4000),substr(:new.NM_PESSOA_CONTATO,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_CONTATO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE_CELULAR,1,4000),substr(:new.NR_TELEFONE_CELULAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE_CELULAR',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'PESSOA_JURIDICA_COMPL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.compl_pessoa_juridica_a400_up
before update ON TASY.PESSOA_JURIDICA_COMPL for each row
declare
nr_seq_prestador_w	pls_prestador.nr_sequencia%type;
ie_divulga_email_w	pls_prestador.ie_divulga_email%type;
qt_email_w		pls_integer;

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(:new.ie_tipo_complemento in(1, 2, 6)) and
		((:new.ds_email <> :old.ds_email) or
		 (:new.ds_email is null and :old.ds_email is not null))then

		select	max(ie_divulga_email),
			max(nr_sequencia)
		into	ie_divulga_email_w,
			nr_seq_prestador_w
		from	pls_prestador
		where	cd_cgc = :new.cd_cgc
		and	ie_divulga_email	= 'S'
		and	ie_ptu_a400		= 'S';

		if	(nr_seq_prestador_w is not null) and
			(nvl(ie_divulga_email_w, 'N') = 'S') and
			(nvl(obter_valor_param_usuario(1323, 6, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento), 'S') = 'N') then

			case 	(:new.ie_tipo_complemento)
				when 	1 then -- Endere�o correspond�ncia

					select	count(1)
					into	qt_email_w
					from	ptu_prestador a,
						ptu_prestador_endereco b
					where	a.nr_sequencia = b.nr_seq_prestador
					and	a.nr_seq_prestador = nr_seq_prestador_w
					and	b.ds_email = :old.ds_email
					and	b.ie_tipo_endereco_original = 'PJC';
				when	2	then --Endere�o fin�nceiro
					select	count(1)
					into	qt_email_w
					from	ptu_prestador a,
						ptu_prestador_endereco b
					where	a.nr_sequencia = b.nr_seq_prestador
					and	a.nr_seq_prestador = nr_seq_prestador_w
					and	b.ds_email = :old.ds_email
					and	b.ie_tipo_endereco_original = 'PJF';
				when	6	then -- Endere�o atendimento
					select	count(1)
					into	qt_email_w
					from	ptu_prestador a,
						ptu_prestador_endereco b
					where	a.nr_sequencia = b.nr_seq_prestador
					and	a.nr_seq_prestador = nr_seq_prestador_w
					and	b.ds_email = :old.ds_email
					and	b.ie_tipo_endereco_original = 'PJA';
				else
					qt_email_w := 0;
			end case;

			if	(qt_email_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(387909);
			end if;
		end if;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_compl_afterUp
after update or insert ON TASY.PESSOA_JURIDICA_COMPL for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_opcao_w	varchar2(1) := 'I';
cd_tipo_pessoa_w	pessoa_juridica.cd_tipo_pessoa%type;

begin

if 	(updating) then
	ie_opcao_w	:= 'A';
end if;

select 	max(cd_tipo_pessoa)
into	cd_tipo_pessoa_w
from 	pessoa_juridica
where 	cd_cgc = :new.cd_cgc
and 	ie_situacao = 'A';

reg_integracao_p.ie_operacao		:=	ie_opcao_w;
reg_integracao_p.cd_tipo_pessoa		:=	cd_tipo_pessoa_w;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :new.cd_cgc,:new.nm_usuario, reg_integracao_p);

end;
/


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_compl_pls_rps
after update ON TASY.PESSOA_JURIDICA_COMPL for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare
ie_alterou_w	varchar2(1) := 'N';

begin
begin
	if	(nvl(:new.cd_cep,'0')		!= nvl(:old.cd_cep,'0')) or
		(nvl(:new.cd_cgc,'0')		!= nvl(:old.cd_cgc,'0')) or
		(nvl(:new.cd_municipio_ibge,'0')!= nvl(:old.cd_municipio_ibge,'0')) or
		(nvl(:new.ds_bairro,'0') 	!= nvl(:old.ds_bairro,'0')) or
		(nvl(:new.ds_complemento,'0')	!= nvl(:old.ds_complemento,'0')) or
		(nvl(:new.ds_endereco,'0')	!= nvl(:old.ds_endereco,'0')) or
		(nvl(:new.ds_municipio,'0')	!= nvl(:old.ds_municipio,'0')) or
		(nvl(:new.nr_endereco,0)	!= nvl(:old.nr_endereco,0)) or
		(nvl(:new.sg_estado,'0')	!= nvl(:old.sg_estado,'0')) then
		ie_alterou_w := 'S';
	end if;

	if	(ie_alterou_w = 'S') then
		pls_gerar_alt_prest_rps(:new.cd_cgc, null, null, :old.cd_cgc, :old.cd_municipio_ibge, null, :new.nm_usuario, null, null, :new.nr_sequencia);
	end if;
exception
when others then
	null;
end;
end;
/


ALTER TABLE TASY.PESSOA_JURIDICA_COMPL ADD (
  CONSTRAINT PESJUCP_PK
 PRIMARY KEY
 (CD_CGC, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_JURIDICA_COMPL ADD (
  CONSTRAINT PESJUCP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PESJUCP_PESSEND_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_ENDERECO) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT PESJUCP_CNSTILO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LOGRADOURO) 
 REFERENCES TASY.CNS_TIPO_LOGRADOURO (NR_SEQUENCIA),
  CONSTRAINT PESJUCP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA_CANAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PESJUCP_CNESIDE_FK 
 FOREIGN KEY (NR_SEQ_IDENT_CNES) 
 REFERENCES TASY.CNES_IDENTIFICACAO (NR_SEQUENCIA),
  CONSTRAINT PESJUCP_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT PESJUCP_CBORED_FK 
 FOREIGN KEY (CD_CBO_RED) 
 REFERENCES TASY.CBO_RED (CD_CBO),
  CONSTRAINT PESJUCP_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT PESJUCP_SUSMURE_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.SUS_MUNICIPIO_REGIAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PESSOA_JURIDICA_COMPL TO NIVEL_1;

