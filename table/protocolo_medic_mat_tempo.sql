ALTER TABLE TASY.PROTOCOLO_MEDIC_MAT_TEMPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_MEDIC_MAT_TEMPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_MEDIC_MAT_TEMPO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_VIA_APLICACAO     VARCHAR2(5 BYTE),
  QT_MINUTO_APLICACAO  NUMBER(4),
  QT_HORA_APLICACAO    NUMBER(3),
  NR_SEQ_PROT_MED      NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRMEMAT_PK ON TASY.PROTOCOLO_MEDIC_MAT_TEMPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMEMAT_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRMEMAT_PROMEMA_FK_I ON TASY.PROTOCOLO_MEDIC_MAT_TEMPO
(NR_SEQ_PROT_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMEMAT_PROMEMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRMEMAT_VIAAPLI_FK_I ON TASY.PROTOCOLO_MEDIC_MAT_TEMPO
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMEMAT_VIAAPLI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROTOCOLO_MEDIC_MAT_TEMPO ADD (
  CONSTRAINT PRMEMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROTOCOLO_MEDIC_MAT_TEMPO ADD (
  CONSTRAINT PRMEMAT_PROMEMA_FK 
 FOREIGN KEY (NR_SEQ_PROT_MED) 
 REFERENCES TASY.PROTOCOLO_MEDIC_MATERIAL (NR_SEQ_INTERNO)
    ON DELETE CASCADE,
  CONSTRAINT PRMEMAT_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO));

GRANT SELECT ON TASY.PROTOCOLO_MEDIC_MAT_TEMPO TO NIVEL_1;

