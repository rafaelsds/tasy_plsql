ALTER TABLE TASY.PLS_REC_GLOSA_GLOSAS_IMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REC_GLOSA_GLOSAS_IMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REC_GLOSA_GLOSAS_IMP
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_CONTA_IMP      NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PROC_IMP       NUMBER(10),
  NR_SEQ_MAT_IMP        NUMBER(10),
  NR_SEQ_GLOSA_CONTA    NUMBER(10),
  NR_SEQ_PROC_PARTIC    NUMBER(10),
  CD_GLOSA              VARCHAR2(4 BYTE),
  DS_JUSTIFICATIVA      VARCHAR2(150 BYTE),
  IE_TIPO_GLOSA         VARCHAR2(4 BYTE),
  NR_SEQ_PROTOCOLO_CTA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRGGIMP_PK ON TASY.PLS_REC_GLOSA_GLOSAS_IMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRGGIMP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRGGIMP_PLSCOGL_FK_I ON TASY.PLS_REC_GLOSA_GLOSAS_IMP
(NR_SEQ_GLOSA_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRGGIMP_PLSCOGL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRGGIMP_PLSPRPAR_FK_I ON TASY.PLS_REC_GLOSA_GLOSAS_IMP
(NR_SEQ_PROC_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRGGIMP_PLSPRPAR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRGGIMP_PLSRGPI_FK_I ON TASY.PLS_REC_GLOSA_GLOSAS_IMP
(NR_SEQ_PROTOCOLO_CTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRGGIMP_PLSRGPI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRGGIMP_PRGCIMP_FK_I ON TASY.PLS_REC_GLOSA_GLOSAS_IMP
(NR_SEQ_CONTA_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRGGIMP_PRGCIMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRGGIMP_PRGMIMP_FK_I ON TASY.PLS_REC_GLOSA_GLOSAS_IMP
(NR_SEQ_CONTA_IMP, NR_SEQ_MAT_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRGGIMP_PRGMIMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRGGIMP_PRGMIMP_FK1_I ON TASY.PLS_REC_GLOSA_GLOSAS_IMP
(NR_SEQ_MAT_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRGGIMP_PRGMIMP_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PRGGIMP_PRGPIMP_FK_I ON TASY.PLS_REC_GLOSA_GLOSAS_IMP
(NR_SEQ_CONTA_IMP, NR_SEQ_PROC_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRGGIMP_PRGPIMP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRGGIMP_PRGPIMP_FK1_I ON TASY.PLS_REC_GLOSA_GLOSAS_IMP
(NR_SEQ_PROC_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRGGIMP_PRGPIMP_FK1_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REC_GLOSA_GLOSAS_IMP ADD (
  CONSTRAINT PRGGIMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REC_GLOSA_GLOSAS_IMP ADD (
  CONSTRAINT PRGGIMP_PLSRGPI_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO_CTA) 
 REFERENCES TASY.PLS_REC_GLOSA_PROT_CTA_IMP (NR_SEQUENCIA),
  CONSTRAINT PRGGIMP_PLSCOGL_FK 
 FOREIGN KEY (NR_SEQ_GLOSA_CONTA) 
 REFERENCES TASY.PLS_CONTA_GLOSA (NR_SEQUENCIA),
  CONSTRAINT PRGGIMP_PLSPRPAR_FK 
 FOREIGN KEY (NR_SEQ_PROC_PARTIC) 
 REFERENCES TASY.PLS_PROC_PARTICIPANTE (NR_SEQUENCIA),
  CONSTRAINT PRGGIMP_PRGCIMP_FK 
 FOREIGN KEY (NR_SEQ_CONTA_IMP) 
 REFERENCES TASY.PLS_REC_GLOSA_CONTA_IMP (NR_SEQUENCIA),
  CONSTRAINT PRGGIMP_PRGMIMP_FK 
 FOREIGN KEY (NR_SEQ_CONTA_IMP, NR_SEQ_MAT_IMP) 
 REFERENCES TASY.PLS_REC_GLOSA_MAT_IMP (NR_SEQ_CONTA_IMP,NR_SEQUENCIA),
  CONSTRAINT PRGGIMP_PRGMIMP_FK1 
 FOREIGN KEY (NR_SEQ_MAT_IMP) 
 REFERENCES TASY.PLS_REC_GLOSA_MAT_IMP (NR_SEQUENCIA),
  CONSTRAINT PRGGIMP_PRGPIMP_FK 
 FOREIGN KEY (NR_SEQ_CONTA_IMP, NR_SEQ_PROC_IMP) 
 REFERENCES TASY.PLS_REC_GLOSA_PROC_IMP (NR_SEQ_CONTA_IMP,NR_SEQUENCIA),
  CONSTRAINT PRGGIMP_PRGPIMP_FK1 
 FOREIGN KEY (NR_SEQ_PROC_IMP) 
 REFERENCES TASY.PLS_REC_GLOSA_PROC_IMP (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REC_GLOSA_GLOSAS_IMP TO NIVEL_1;

