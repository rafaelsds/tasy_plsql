ALTER TABLE TASY.RECOMENDACAO_DOENCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RECOMENDACAO_DOENCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.RECOMENDACAO_DOENCA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_DOENCA_CID        VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQ_RECOMENDACAO  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RECDOE_CIDDOEN_FK_I ON TASY.RECOMENDACAO_DOENCA
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECDOE_CPOEREC_FK_I ON TASY.RECOMENDACAO_DOENCA
(NR_SEQ_RECOMENDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RECDOE_PK ON TASY.RECOMENDACAO_DOENCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RECOMENDACAO_DOENCA ADD (
  CONSTRAINT RECDOE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.RECOMENDACAO_DOENCA ADD (
  CONSTRAINT RECDOE_CPOEREC_FK 
 FOREIGN KEY (NR_SEQ_RECOMENDACAO) 
 REFERENCES TASY.CPOE_RECOMENDACAO (NR_SEQUENCIA),
  CONSTRAINT RECDOE_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.RECOMENDACAO_DOENCA TO NIVEL_1;

