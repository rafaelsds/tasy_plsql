DROP TABLE TASY.BIFROST_EVENTS CASCADE CONSTRAINTS;

CREATE TABLE TASY.BIFROST_EVENTS
(
  EVENT    VARCHAR2(80 BYTE)                    NOT NULL,
  UPDATED  DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          200K
            NEXT             24K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.BIFROST_EVENTS TO NIVEL_1;

