ALTER TABLE TASY.PCS_MATERIAL_SUBSTITUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PCS_MATERIAL_SUBSTITUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PCS_MATERIAL_SUBSTITUTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  NR_PRIORIDADE        NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCSMSUB_MATERIA_FK_I ON TASY.PCS_MATERIAL_SUBSTITUTO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSMSUB_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PCSMSUB_PCSSUMT_FK_I ON TASY.PCS_MATERIAL_SUBSTITUTO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSMSUB_PCSSUMT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PCSMSUB_PK ON TASY.PCS_MATERIAL_SUBSTITUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSMSUB_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PCS_MATERIAL_SUBSTITUTO_tp  after update ON TASY.PCS_MATERIAL_SUBSTITUTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'PCS_MATERIAL_SUBSTITUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PRIORIDADE,1,4000),substr(:new.NR_PRIORIDADE,1,4000),:new.nm_usuario,nr_seq_w,'NR_PRIORIDADE',ie_log_w,ds_w,'PCS_MATERIAL_SUBSTITUTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PCS_MATERIAL_SUBSTITUTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pcs_mat_subst_ordem_update
before insert or update ON TASY.PCS_MATERIAL_SUBSTITUTO for each row
declare

qt_existe_w	number(5);

PRAGMA autonomous_transaction;

begin

select	count(*)
into	qt_existe_w
from	pcs_material_substituto
where	cd_material = :new.cd_material
and	nr_seq_regra = :new.nr_seq_regra
and	nr_sequencia <> :new.nr_sequencia;

if (qt_existe_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(341258, null);
	/* Esta material j� esta cadastrado para a regra */
end if;

select	count(*)
into	qt_existe_w
from	pcs_material_substituto
where	nr_prioridade = :new.nr_prioridade
and	ie_situacao = 'A'
and	nr_seq_regra = :new.nr_seq_regra
and	nr_sequencia <> :new.nr_sequencia;

if (qt_existe_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(341259, null);
	/* Est� prioridade j� existe para o item substitu�do*/
end if;

end;
/


ALTER TABLE TASY.PCS_MATERIAL_SUBSTITUTO ADD (
  CONSTRAINT PCSMSUB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PCS_MATERIAL_SUBSTITUTO ADD (
  CONSTRAINT PCSMSUB_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PCSMSUB_PCSSUMT_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PCS_SUBSTITUICAO_MATERIAL (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PCS_MATERIAL_SUBSTITUTO TO NIVEL_1;

