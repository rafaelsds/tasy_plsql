ALTER TABLE TASY.AGENDA_CONSULTA_PROF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_CONSULTA_PROF CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_CONSULTA_PROF
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_AGENDA               NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_ORDEM                NUMBER(10)        NOT NULL,
  IE_TIPO_PROFISSIONAL        VARCHAR2(15 BYTE) NOT NULL,
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE),
  HR_INICIO                   DATE,
  HR_FIM                      DATE,
  QT_DIAS_RETORNO             NUMBER(10),
  IE_SOLICITA_RETORNO         VARCHAR2(1 BYTE)  NOT NULL,
  DT_CANCELAMENTO             DATE,
  NR_SEQ_MOTIVO_CANCELAMENTO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGECONP_AGECONS_FK_I ON TASY.AGENDA_CONSULTA_PROF
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGECONP_AGMOCAN_FK_I ON TASY.AGENDA_CONSULTA_PROF
(NR_SEQ_MOTIVO_CANCELAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECONP_AGMOCAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGECONP_PESFISI_FK_I ON TASY.AGENDA_CONSULTA_PROF
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGECONP_PK ON TASY.AGENDA_CONSULTA_PROF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECONP_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGECONP_UK ON TASY.AGENDA_CONSULTA_PROF
(NR_SEQ_AGENDA, NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGECONP_UK
  MONITORING USAGE;


ALTER TABLE TASY.AGENDA_CONSULTA_PROF ADD (
  CONSTRAINT AGECONP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT AGECONP_UK
 UNIQUE (NR_SEQ_AGENDA, NR_SEQ_ORDEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_CONSULTA_PROF ADD (
  CONSTRAINT AGECONP_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGECONP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGECONP_AGMOCAN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCELAMENTO) 
 REFERENCES TASY.AGENDA_MOTIVO_CANCELAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_CONSULTA_PROF TO NIVEL_1;

