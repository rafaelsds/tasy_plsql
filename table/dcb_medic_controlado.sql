ALTER TABLE TASY.DCB_MEDIC_CONTROLADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DCB_MEDIC_CONTROLADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DCB_MEDIC_CONTROLADO
(
  NR_SEQUENCIA         NUMBER(10),
  CD_DCB               VARCHAR2(20 BYTE)        NOT NULL,
  DS_DCB               VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_VERSAO            NUMBER(4),
  CD_ANTIGO            VARCHAR2(20 BYTE),
  CD_CAS               VARCHAR2(20 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DCBMECO_I1 ON TASY.DCB_MEDIC_CONTROLADO
(CD_DCB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DCBMECO_PK ON TASY.DCB_MEDIC_CONTROLADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DCB_MEDIC_CONTROLADO_tp  after update ON TASY.DCB_MEDIC_CONTROLADO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_DCB,1,4000),substr(:new.CD_DCB,1,4000),:new.nm_usuario,nr_seq_w,'CD_DCB',ie_log_w,ds_w,'DCB_MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DCB,1,4000),substr(:new.DS_DCB,1,4000),:new.nm_usuario,nr_seq_w,'DS_DCB',ie_log_w,ds_w,'DCB_MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CAS,1,4000),substr(:new.CD_CAS,1,4000),:new.nm_usuario,nr_seq_w,'CD_CAS',ie_log_w,ds_w,'DCB_MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_VERSAO,1,4000),substr(:new.CD_VERSAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_VERSAO',ie_log_w,ds_w,'DCB_MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ANTIGO,1,4000),substr(:new.CD_ANTIGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ANTIGO',ie_log_w,ds_w,'DCB_MEDIC_CONTROLADO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'DCB_MEDIC_CONTROLADO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.DCB_MEDIC_CONTROLADO ADD (
  CONSTRAINT DCBMECO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.DCB_MEDIC_CONTROLADO TO NIVEL_1;

