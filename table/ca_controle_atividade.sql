ALTER TABLE TASY.CA_CONTROLE_ATIVIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CA_CONTROLE_ATIVIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CA_CONTROLE_ATIVIDADE
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_ATIVIDADE         NUMBER(10)           NOT NULL,
  DS_RESULTADO             VARCHAR2(255 BYTE),
  IE_STATUS                VARCHAR2(1 BYTE)     NOT NULL,
  DT_PREVISTA              DATE                 NOT NULL,
  DT_EXECUCAO              DATE,
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  DT_CANCELAMENTO          DATE,
  NM_USUARIO_CANCELAMENTO  VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE),
  NM_USUARIO_EXEC          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CACONAT_CAATIVI_FK_I ON TASY.CA_CONTROLE_ATIVIDADE
(NR_SEQ_ATIVIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CACONAT_CAATIVI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CACONAT_ESTABEL_FK_I ON TASY.CA_CONTROLE_ATIVIDADE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CACONAT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CACONAT_PESFISI_FK_I ON TASY.CA_CONTROLE_ATIVIDADE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CACONAT_PK ON TASY.CA_CONTROLE_ATIVIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CACONAT_PK
  MONITORING USAGE;


ALTER TABLE TASY.CA_CONTROLE_ATIVIDADE ADD (
  CONSTRAINT CACONAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CA_CONTROLE_ATIVIDADE ADD (
  CONSTRAINT CACONAT_CAATIVI_FK 
 FOREIGN KEY (NR_SEQ_ATIVIDADE) 
 REFERENCES TASY.CA_ATIVIDADE (NR_SEQUENCIA),
  CONSTRAINT CACONAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CACONAT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CA_CONTROLE_ATIVIDADE TO NIVEL_1;

