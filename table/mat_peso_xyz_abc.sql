ALTER TABLE TASY.MAT_PESO_XYZ_ABC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAT_PESO_XYZ_ABC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAT_PESO_XYZ_ABC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_ABC                 VARCHAR2(1 BYTE)       NOT NULL,
  IE_XYZ                 VARCHAR2(1 BYTE)       NOT NULL,
  QT_DIA_INTERV_RESSUP   NUMBER(15),
  QT_DIA_RESSUP_FORN     NUMBER(15),
  QT_DIA_ESTOQUE_MINIMO  NUMBER(15),
  CD_MATERIAL            NUMBER(6),
  CD_GRUPO_MATERIAL      NUMBER(3),
  CD_SUBGRUPO_MATERIAL   NUMBER(3),
  CD_CLASSE_MATERIAL     NUMBER(5),
  NR_SEQ_FABRIC          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATPEXA_CLAMATE_FK_I ON TASY.MAT_PESO_XYZ_ABC
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATPEXA_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATPEXA_ESTABEL_FK_I ON TASY.MAT_PESO_XYZ_ABC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATPEXA_GRUMATE_FK_I ON TASY.MAT_PESO_XYZ_ABC
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATPEXA_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATPEXA_MATERIA_FK_I ON TASY.MAT_PESO_XYZ_ABC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATPEXA_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATPEXA_MATFABR_FK_I ON TASY.MAT_PESO_XYZ_ABC
(NR_SEQ_FABRIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATPEXA_MATFABR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATPEXA_PK ON TASY.MAT_PESO_XYZ_ABC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATPEXA_SUBMATE_FK_I ON TASY.MAT_PESO_XYZ_ABC
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATPEXA_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MAT_PESO_XYZ_ABC_tp  after update ON TASY.MAT_PESO_XYZ_ABC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ABC,1,4000),substr(:new.IE_ABC,1,4000),:new.nm_usuario,nr_seq_w,'IE_ABC',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_XYZ,1,4000),substr(:new.IE_XYZ,1,4000),:new.nm_usuario,nr_seq_w,'IE_XYZ',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_INTERV_RESSUP,1,4000),substr(:new.QT_DIA_INTERV_RESSUP,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_INTERV_RESSUP',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_RESSUP_FORN,1,4000),substr(:new.QT_DIA_RESSUP_FORN,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_RESSUP_FORN',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FABRIC,1,4000),substr(:new.NR_SEQ_FABRIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FABRIC',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_MATERIAL,1,4000),substr(:new.CD_GRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_MATERIAL',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SUBGRUPO_MATERIAL,1,4000),substr(:new.CD_SUBGRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_MATERIAL',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MATERIAL,1,4000),substr(:new.CD_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MATERIAL',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_ESTOQUE_MINIMO,1,4000),substr(:new.QT_DIA_ESTOQUE_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_ESTOQUE_MINIMO',ie_log_w,ds_w,'MAT_PESO_XYZ_ABC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.mat_peso_xyz_abc_atual
before insert or update ON TASY.MAT_PESO_XYZ_ABC for each row
declare
qt_existe_w	number(4);

pragma autonomous_transaction;
begin
if	(:new.cd_material is not null) then
	select	count(*)
	into	qt_existe_w
	from	mat_peso_xyz_abc
	where	cd_material = :new.cd_material
	and	ie_abc = :new.ie_abc
	and	ie_xyz = :new.ie_xyz
	and	nr_sequencia <> :new.nr_sequencia
	and	cd_estabelecimento = :new.cd_estabelecimento;
elsif	(:new.cd_classe_material is not null) then
	select	count(*)
	into	qt_existe_w
	from	mat_peso_xyz_abc
	where	cd_material is null
	and	cd_classe_material = :new.cd_classe_material
	and	ie_abc = :new.ie_abc
	and	ie_xyz = :new.ie_xyz
	and	nr_sequencia <> :new.nr_sequencia
	and	cd_estabelecimento = :new.cd_estabelecimento;
elsif	(:new.cd_subgrupo_material is not null) then
	select	count(*)
	into	qt_existe_w
	from	mat_peso_xyz_abc
	where	cd_material is null
	and	cd_classe_material is null
	and	cd_subgrupo_material = :new.cd_subgrupo_material
	and	ie_abc = :new.ie_abc
	and	ie_xyz = :new.ie_xyz
	and	nr_sequencia <> :new.nr_sequencia
	and	cd_estabelecimento = :new.cd_estabelecimento;
elsif	(:new.cd_grupo_material is not null) then
	select	count(*)
	into	qt_existe_w
	from	mat_peso_xyz_abc
	where	cd_material is null
	and	cd_classe_material is null
	and	cd_subgrupo_material is null
	and	cd_grupo_material = :new.cd_grupo_material
	and	ie_abc = :new.ie_abc
	and	ie_xyz = :new.ie_xyz
	and	nr_sequencia <> :new.nr_sequencia
	and	cd_estabelecimento = :new.cd_estabelecimento;
else
	select	count(*)
	into	qt_existe_w
	from	mat_peso_xyz_abc
	where	cd_material is null
	and	cd_classe_material is null
	and	cd_subgrupo_material is null
	and	cd_grupo_material is null
	and	nr_seq_fabric is null
	and	ie_abc = :new.ie_abc
	and	ie_xyz = :new.ie_xyz
	and	nr_sequencia <> :new.nr_sequencia
	and	cd_estabelecimento = :new.cd_estabelecimento;
end if;

if	(qt_existe_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(188124);
end if;
commit;
end;
/


ALTER TABLE TASY.MAT_PESO_XYZ_ABC ADD (
  CONSTRAINT MATPEXA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAT_PESO_XYZ_ABC ADD (
  CONSTRAINT MATPEXA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MATPEXA_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT MATPEXA_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT MATPEXA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATPEXA_MATFABR_FK 
 FOREIGN KEY (NR_SEQ_FABRIC) 
 REFERENCES TASY.MAT_FABRICANTE (NR_SEQUENCIA),
  CONSTRAINT MATPEXA_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.MAT_PESO_XYZ_ABC TO NIVEL_1;

