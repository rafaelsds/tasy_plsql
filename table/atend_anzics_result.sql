ALTER TABLE TASY.ATEND_ANZICS_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_ANZICS_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_ANZICS_RESULT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_ANZICS  NUMBER(10)               NOT NULL,
  NR_SEQ_ANZICS_ITEM   NUMBER(10)               NOT NULL,
  NR_SEQ_PRESENTATION  NUMBER(10),
  QT_VALUE             NUMBER(15,4),
  DS_VALUE             VARCHAR2(255 BYTE),
  DT_VALUE             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATNANZRES_ANZITM_FK_I ON TASY.ATEND_ANZICS_RESULT
(NR_SEQ_ANZICS_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATNANZRES_ATENDANZ_FK_I ON TASY.ATEND_ANZICS_RESULT
(NR_SEQ_ATEND_ANZICS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATNANZRES_PK ON TASY.ATEND_ANZICS_RESULT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ATEND_ANZICS_RESULT ADD (
  CONSTRAINT ATNANZRES_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ATEND_ANZICS_RESULT ADD (
  CONSTRAINT ATNANZRES_ANZITM_FK 
 FOREIGN KEY (NR_SEQ_ANZICS_ITEM) 
 REFERENCES TASY.ANZICS_ITEM (NR_SEQUENCIA),
  CONSTRAINT ATNANZRES_ATENDANZ_FK 
 FOREIGN KEY (NR_SEQ_ATEND_ANZICS) 
 REFERENCES TASY.ATEND_ANZICS (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ATEND_ANZICS_RESULT TO NIVEL_1;

