ALTER TABLE TASY.BUILD_USER_INTEREST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BUILD_USER_INTEREST CASCADE CONSTRAINTS;

CREATE TABLE TASY.BUILD_USER_INTEREST
(
  NR_SEQUENCE        NUMBER(10)                 NOT NULL,
  NM_USER_BUILD      VARCHAR2(15 BYTE)          NOT NULL,
  DT_INTEREST        DATE                       NOT NULL,
  NR_SEQ_SCHE_BUILD  NUMBER(10)                 NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BLDUSRINT_AGENDBU_FK_I ON TASY.BUILD_USER_INTEREST
(NR_SEQ_SCHE_BUILD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BLDUSRINT_PK_I ON TASY.BUILD_USER_INTEREST
(NR_SEQUENCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.BUILD_USER_INTEREST ADD (
  CONSTRAINT BLDUSRINT_PK_I
 PRIMARY KEY
 (NR_SEQUENCE));

ALTER TABLE TASY.BUILD_USER_INTEREST ADD (
  CONSTRAINT BLDUSRINT_AGENDBU_FK 
 FOREIGN KEY (NR_SEQ_SCHE_BUILD) 
 REFERENCES TASY.AGENDAMENTO_BUILD (NR_SEQUENCIA));

GRANT SELECT ON TASY.BUILD_USER_INTEREST TO NIVEL_1;

