ALTER TABLE TASY.CTB_REGRA_GERACAO_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_REGRA_GERACAO_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_REGRA_GERACAO_LOTE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_TIPO_LOTE_CONTABIL  NUMBER(2)              NOT NULL,
  CD_EMPRESA             NUMBER(4)              NOT NULL,
  CD_ESTAB_EXCLUSIVO     NUMBER(4),
  IE_PERIODO             VARCHAR2(15 BYTE)      NOT NULL,
  QT_PERIODO             NUMBER(2),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBRGERLT_EMPRESA_FK_I ON TASY.CTB_REGRA_GERACAO_LOTE
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBRGERLT_ESTABEL_FK_I ON TASY.CTB_REGRA_GERACAO_LOTE
(CD_ESTAB_EXCLUSIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBRGERLT_PK ON TASY.CTB_REGRA_GERACAO_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBRGERLT_TIPLOCO_FK_I ON TASY.CTB_REGRA_GERACAO_LOTE
(CD_TIPO_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ctb_regra_geracao_lote_atual
before insert or update ON TASY.CTB_REGRA_GERACAO_LOTE for each row
declare

begin
 if (:new.ie_periodo = 'D') then
    :new.qt_periodo := 1;
 elsif (:new.ie_periodo = 'M') then
    :new.qt_periodo := 30;
 elsif (:new.ie_periodo = 'S') then
    :new.qt_periodo := 7;
    elsif (:new.ie_periodo = 'Q') then
    :new.qt_periodo := 15;
 end if;

end ctb_regra_geracao_lote_atual;
/


CREATE OR REPLACE TRIGGER TASY.CTB_REGRA_GERACAO_LOTE_tp  after update ON TASY.CTB_REGRA_GERACAO_LOTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_TIPO_LOTE_CONTABIL,1,4000),substr(:new.CD_TIPO_LOTE_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOTE_CONTABIL',ie_log_w,ds_w,'CTB_REGRA_GERACAO_LOTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CTB_REGRA_GERACAO_LOTE ADD (
  CONSTRAINT CTBRGERLT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_REGRA_GERACAO_LOTE ADD (
  CONSTRAINT CTBRGERLT_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT CTBRGERLT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_EXCLUSIVO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBRGERLT_TIPLOCO_FK 
 FOREIGN KEY (CD_TIPO_LOTE_CONTABIL) 
 REFERENCES TASY.TIPO_LOTE_CONTABIL (CD_TIPO_LOTE_CONTABIL));

GRANT SELECT ON TASY.CTB_REGRA_GERACAO_LOTE TO NIVEL_1;

