ALTER TABLE TASY.MPREV_ATEND_EV_PROF_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_ATEND_EV_PROF_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_ATEND_EV_PROF_ADIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_EVENTO  NUMBER(10)               NOT NULL,
  CD_PROFISSIONAL      VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRAEPA_MPRATEV_FK_I ON TASY.MPREV_ATEND_EV_PROF_ADIC
(NR_SEQ_ATEND_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAEPA_PESFISI_FK_I ON TASY.MPREV_ATEND_EV_PROF_ADIC
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRAEPA_PK ON TASY.MPREV_ATEND_EV_PROF_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MPREV_ATEND_EV_PROF_ADIC ADD (
  CONSTRAINT MPRAEPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_ATEND_EV_PROF_ADIC ADD (
  CONSTRAINT MPRAEPA_MPRATEV_FK 
 FOREIGN KEY (NR_SEQ_ATEND_EVENTO) 
 REFERENCES TASY.MPREV_ATENDIMENTO_EVENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRAEPA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.MPREV_ATEND_EV_PROF_ADIC TO NIVEL_1;

