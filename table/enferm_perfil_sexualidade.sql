DROP TABLE TASY.ENFERM_PERFIL_SEXUALIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ENFERM_PERFIL_SEXUALIDADE
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  DT_REGISTRO                  DATE             NOT NULL,
  CD_PROFISSIONAL              VARCHAR2(10 BYTE) NOT NULL,
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE) NOT NULL,
  NR_ATENDIMENTO               NUMBER(10)       NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_CABECALHO             NUMBER(10)       NOT NULL,
  DT_LIBERACAO                 DATE,
  DT_INATIVACAO                DATE,
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE),
  NR_SEQ_ASSINATURA            NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO    NUMBER(10),
  IE_DOENCAS_GINECOLOGICAS     VARCHAR2(255 BYTE),
  DS_DOENCAS_GINECOLOGICAS     VARCHAR2(255 BYTE),
  IE_CONS_SEX                  VARCHAR2(1 BYTE) NOT NULL,
  IE_CONS_SEX_REL_DOENCA       VARCHAR2(1 BYTE) NOT NULL,
  IE_CONS_SEX_REL_MEDICAMENTO  VARCHAR2(1 BYTE) NOT NULL,
  IE_CONS_SEX_REL_PARCEIRO     VARCHAR2(1 BYTE) NOT NULL,
  IE_CONS_SEX_REL_OUTRO        VARCHAR2(1 BYTE) NOT NULL,
  DS_CONS_SEX_REL_OUTRO        VARCHAR2(2000 BYTE),
  DT_MENARCA                   DATE,
  IE_MENOPAUSA                 VARCHAR2(1 BYTE),
  DS_INF_MENOPAUSA             VARCHAR2(2000 BYTE),
  DT_ULT_MENSTRUACAO           DATE,
  IE_RITMO_MENSTRUAL           VARCHAR2(1 BYTE),
  QT_DIAS_CICLO                NUMBER(2),
  IE_ORD_CLICO_MENSTRUAL       VARCHAR2(1 BYTE),
  IE_IRREG_CLICO_MENSTRUAL     VARCHAR2(1 BYTE),
  IE_PAC_GRAVIDA               VARCHAR2(1 BYTE) NOT NULL,
  DS_PAC_GRAVIDA_TEMPO         VARCHAR2(255 BYTE),
  DT_INICIO_GESTACAO           DATE,
  QT_SEM_IG_CRONOLOGICA        NUMBER(5,2),
  QT_DIA_IG_CRONOLOGICA        NUMBER(5,2),
  DT_PROV_PARTO                DATE,
  DT_FIM_GESTACAO              DATE,
  IE_FILHOS                    VARCHAR2(1 BYTE) NOT NULL,
  QT_FILHOS                    NUMBER(2),
  IE_FILHOS_FUTURO             VARCHAR2(1 BYTE) NOT NULL,
  IE_SINT_MESTRUACAO           VARCHAR2(1 BYTE) NOT NULL,
  IE_SINT_MESTRUACAO_DOR_ABD   VARCHAR2(1 BYTE) NOT NULL,
  IE_SINT_MESTRUACAO_ANEMIA    VARCHAR2(1 BYTE) NOT NULL,
  IE_SINT_MESTRUACAO_DOR_LOMB  VARCHAR2(1 BYTE) NOT NULL,
  IE_SINT_MESTRUACAO_IRRITADA  VARCHAR2(1 BYTE) NOT NULL,
  IE_SINT_MESTRUACAO_OUTRO     VARCHAR2(1 BYTE) NOT NULL,
  DS_SINT_MESTRUACAO_OUTRO     VARCHAR2(255 BYTE),
  IE_PROB_PROSTATA             VARCHAR2(1 BYTE) NOT NULL,
  DS_PROB_PROSTATA             VARCHAR2(255 BYTE),
  DS_SEX_OUTRO                 VARCHAR2(255 BYTE),
  DT_SEXUALIDADE               DATE,
  IE_SEXUALIDADE_PROBLEMAS     VARCHAR2(1 BYTE) NOT NULL,
  DS_SEXUALIDADE_PROBLEMAS     VARCHAR2(1 BYTE) NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ENFERMSEXU_ATEPACI_FK_I ON TASY.ENFERM_PERFIL_SEXUALIDADE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFERMSEXU_CABECAL_FK_I ON TASY.ENFERM_PERFIL_SEXUALIDADE
(NR_SEQ_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFERMSEXU_ESTABEL_FK_I ON TASY.ENFERM_PERFIL_SEXUALIDADE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFERMSEXU_PESFISI_FK_I ON TASY.ENFERM_PERFIL_SEXUALIDADE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFERMSEXU_PESFISI_FK2_I ON TASY.ENFERM_PERFIL_SEXUALIDADE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFERMSEXU_TASASDI_FK_I ON TASY.ENFERM_PERFIL_SEXUALIDADE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ENFERMSEXU_TASASDI_FK2_I ON TASY.ENFERM_PERFIL_SEXUALIDADE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ENFERM_PERFIL_SEXUALIDADE_tp  after update ON TASY.ENFERM_PERFIL_SEXUALIDADE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null; gravar_log_alteracao(to_char(:old.DT_SEXUALIDADE,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_SEXUALIDADE,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_SEXUALIDADE',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILHOS,1,4000),substr(:new.IE_FILHOS,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILHOS',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SEXUALIDADE_PROBLEMAS,1,4000),substr(:new.DS_SEXUALIDADE_PROBLEMAS,1,4000),:new.nm_usuario,nr_seq_w,'DS_SEXUALIDADE_PROBLEMAS',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONS_SEX,1,4000),substr(:new.IE_CONS_SEX,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONS_SEX',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONS_SEX_REL_DOENCA,1,4000),substr(:new.IE_CONS_SEX_REL_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONS_SEX_REL_DOENCA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONS_SEX_REL_MEDICAMENTO,1,4000),substr(:new.IE_CONS_SEX_REL_MEDICAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONS_SEX_REL_MEDICAMENTO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONS_SEX_REL_PARCEIRO,1,4000),substr(:new.IE_CONS_SEX_REL_PARCEIRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONS_SEX_REL_PARCEIRO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONS_SEX_REL_OUTRO,1,4000),substr(:new.IE_CONS_SEX_REL_OUTRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONS_SEX_REL_OUTRO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONS_SEX_REL_OUTRO,1,4000),substr(:new.DS_CONS_SEX_REL_OUTRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONS_SEX_REL_OUTRO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PAC_GRAVIDA_TEMPO,1,4000),substr(:new.DS_PAC_GRAVIDA_TEMPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_PAC_GRAVIDA_TEMPO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_PROV_PARTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_PROV_PARTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_PROV_PARTO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SINT_MESTRUACAO_DOR_ABD,1,4000),substr(:new.IE_SINT_MESTRUACAO_DOR_ABD,1,4000),:new.nm_usuario,nr_seq_w,'IE_SINT_MESTRUACAO_DOR_ABD',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SINT_MESTRUACAO_ANEMIA,1,4000),substr(:new.IE_SINT_MESTRUACAO_ANEMIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SINT_MESTRUACAO_ANEMIA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SINT_MESTRUACAO_DOR_LOMB,1,4000),substr(:new.IE_SINT_MESTRUACAO_DOR_LOMB,1,4000),:new.nm_usuario,nr_seq_w,'IE_SINT_MESTRUACAO_DOR_LOMB',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SINT_MESTRUACAO_IRRITADA,1,4000),substr(:new.IE_SINT_MESTRUACAO_IRRITADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SINT_MESTRUACAO_IRRITADA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SINT_MESTRUACAO_OUTRO,1,4000),substr(:new.IE_SINT_MESTRUACAO_OUTRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SINT_MESTRUACAO_OUTRO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SINT_MESTRUACAO_OUTRO,1,4000),substr(:new.DS_SINT_MESTRUACAO_OUTRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_SINT_MESTRUACAO_OUTRO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROB_PROSTATA,1,4000),substr(:new.IE_PROB_PROSTATA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROB_PROSTATA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROB_PROSTATA,1,4000),substr(:new.DS_PROB_PROSTATA,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROB_PROSTATA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SEX_OUTRO,1,4000),substr(:new.DS_SEX_OUTRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_SEX_OUTRO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DOENCAS_GINECOLOGICAS,1,4000),substr(:new.DS_DOENCAS_GINECOLOGICAS,1,4000),:new.nm_usuario,nr_seq_w,'DS_DOENCAS_GINECOLOGICAS',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_MENARCA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_MENARCA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_MENARCA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ULT_MENSTRUACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ULT_MENSTRUACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ULT_MENSTRUACAO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_CICLO,1,4000),substr(:new.QT_DIAS_CICLO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_CICLO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RITMO_MENSTRUAL,1,4000),substr(:new.IE_RITMO_MENSTRUAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_RITMO_MENSTRUAL',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SINT_MESTRUACAO,1,4000),substr(:new.IE_SINT_MESTRUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SINT_MESTRUACAO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PAC_GRAVIDA,1,4000),substr(:new.IE_PAC_GRAVIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PAC_GRAVIDA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILHOS_FUTURO,1,4000),substr(:new.IE_FILHOS_FUTURO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILHOS_FUTURO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_GESTACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_GESTACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_GESTACAO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_GESTACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_GESTACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_GESTACAO',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_FILHOS,1,4000),substr(:new.QT_FILHOS,1,4000),:new.nm_usuario,nr_seq_w,'QT_FILHOS',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DOENCAS_GINECOLOGICAS,1,4000),substr(:new.IE_DOENCAS_GINECOLOGICAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_DOENCAS_GINECOLOGICAS',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MENOPAUSA,1,4000),substr(:new.IE_MENOPAUSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MENOPAUSA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_INF_MENOPAUSA,1,4000),substr(:new.DS_INF_MENOPAUSA,1,4000),:new.nm_usuario,nr_seq_w,'DS_INF_MENOPAUSA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IRREG_CLICO_MENSTRUAL,1,4000),substr(:new.IE_IRREG_CLICO_MENSTRUAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_IRREG_CLICO_MENSTRUAL',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORD_CLICO_MENSTRUAL,1,4000),substr(:new.IE_ORD_CLICO_MENSTRUAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORD_CLICO_MENSTRUAL',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_IG_CRONOLOGICA,1,4000),substr(:new.QT_DIA_IG_CRONOLOGICA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_IG_CRONOLOGICA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_SEM_IG_CRONOLOGICA,1,4000),substr(:new.QT_SEM_IG_CRONOLOGICA,1,4000),:new.nm_usuario,nr_seq_w,'QT_SEM_IG_CRONOLOGICA',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXUALIDADE_PROBLEMAS,1,4000),substr(:new.IE_SEXUALIDADE_PROBLEMAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXUALIDADE_PROBLEMAS',ie_log_w,ds_w,'ENFERM_PERFIL_SEXUALIDADE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ENFERM_PERFIL_SEXUALIDADE ADD (
  CONSTRAINT ENFERMSEXU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ENFERMSEXU_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ENFERMSEXU_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ENFERMSEXU_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ENFERMSEXU_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ENFERMSEXU_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ENFERMSEXU_CABECAL_FK 
 FOREIGN KEY (NR_SEQ_CABECALHO) 
 REFERENCES TASY.ENFERM_PERFIL_CABECALHO (NR_SEQUENCIA));

