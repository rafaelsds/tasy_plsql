ALTER TABLE TASY.PROBLEMA_CLASSIF_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROBLEMA_CLASSIF_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROBLEMA_CLASSIF_PERFIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CLASSIF       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_PERFIL            NUMBER(10)               NOT NULL,
  IE_ATUALIZAR         VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROCLPE_PERFIL_FK_I ON TASY.PROBLEMA_CLASSIF_PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROCLPE_PK ON TASY.PROBLEMA_CLASSIF_PERFIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCLPE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROCLPE_PROCLAS_FK_I ON TASY.PROBLEMA_CLASSIF_PERFIL
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROBLEMA_CLASSIF_PERFIL ADD (
  CONSTRAINT PROCLPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROBLEMA_CLASSIF_PERFIL ADD (
  CONSTRAINT PROCLPE_PROCLAS_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PROBLEMA_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT PROCLPE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.PROBLEMA_CLASSIF_PERFIL TO NIVEL_1;

