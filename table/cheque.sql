ALTER TABLE TASY.CHEQUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHEQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHEQUE
(
  CD_BANCO                  NUMBER(3)           NOT NULL,
  CD_AGENCIA_BANCARIA       VARCHAR2(8 BYTE),
  NR_CHEQUE                 VARCHAR2(15 BYTE),
  DT_EMISSAO                DATE                NOT NULL,
  VL_CHEQUE                 NUMBER(13,2)        NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_CGC_EMITENTE           VARCHAR2(14 BYTE)   NOT NULL,
  CD_CGC_DESTINATARIO       VARCHAR2(14 BYTE),
  CD_PESSOA_DESTINATARIO    VARCHAR2(10 BYTE),
  DS_DESTINATARIO           VARCHAR2(40 BYTE),
  DT_IMPRESSAO              DATE,
  IE_SITUACAO               VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  NR_SEQUENCIA              NUMBER(10),
  NR_SEQ_LOTE               NUMBER(10),
  NR_SEQ_SALDO_CAIXA        NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(4000 BYTE),
  NR_SEQ_CONTA_BANCO        NUMBER(10),
  DT_CANCELAMENTO           DATE,
  DT_COMPENSACAO            DATE,
  DT_VENCIMENTO             DATE,
  NR_TALAO                  VARCHAR2(15 BYTE),
  DS_UTILIZACAO             VARCHAR2(255 BYTE),
  NR_SEQ_TRANS_FIN_EMISSAO  NUMBER(10),
  NR_SEQ_TRANS_FIN_COMP     NUMBER(10),
  DT_PAGAMENTO              DATE,
  DT_PREV_PAGAMENTO         DATE,
  VL_CHEQUE_ESTRANG         NUMBER(13,2),
  CD_MOEDA                  NUMBER(5),
  VL_COTACAO                NUMBER(21,10),
  VL_COMPLEMENTO            NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHEQUE_AGEBANC_FK ON TASY.CHEQUE
(CD_BANCO, CD_AGENCIA_BANCARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          960K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUE_BANESTA_FK_I ON TASY.CHEQUE
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUE_ESTABEL_FK_I ON TASY.CHEQUE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUE_I ON TASY.CHEQUE
(CD_BANCO, CD_AGENCIA_BANCARIA, NR_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUE_MOEDA_FK_I ON TASY.CHEQUE
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUE_PESDEST_FK ON TASY.CHEQUE
(CD_CGC_DESTINATARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUE_PESDEST_FK
  MONITORING USAGE;


CREATE INDEX TASY.CHEQUE_PESEMIT_FK ON TASY.CHEQUE
(CD_CGC_EMITENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUE_PESFISI_FK ON TASY.CHEQUE
(CD_PESSOA_DESTINATARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CHEQUE_PK ON TASY.CHEQUE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUE_TRAFINA_FK_I ON TASY.CHEQUE
(NR_SEQ_TRANS_FIN_EMISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHEQUE_TRAFINA_FK2_I ON TASY.CHEQUE
(NR_SEQ_TRANS_FIN_COMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHEQUE_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CHEQUE_DELETE
before delete ON TASY.CHEQUE for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_sequencia,null,'CP',nvl(:old.dt_prev_pagamento,:old.dt_vencimento),'E',:old.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CHEQUE_AFTER_ATUAL
after insert or update ON TASY.CHEQUE for each row
declare
nr_cheque_atual_w	varchar2(30);
qt_registro_w		number(10);

begin

if (updating) then
	if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
		begin
		nr_cheque_atual_w	:= :new.nr_cheque;

		select	count(*)
		into	qt_registro_w
		from	cheque_bordero_titulo a
		where	a.nr_seq_cheque	= :new.nr_sequencia
		and	a.nr_cheque <> nr_cheque_atual_w
		and	a.nr_cheque is not null;

		if	(qt_registro_w > 0) then

			update	cheque_bordero_titulo a
			set	nr_cheque 		= nr_cheque_atual_w
			where	a.nr_seq_cheque	= :new.nr_sequencia
			and	a.nr_cheque <> nr_cheque_atual_w
			and	a.nr_cheque is not null;
		end if;
		end;
	end if;
	/* Grava o agendamento da informação para atualização do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_sequencia,null,'CP',nvl(:new.dt_prev_pagamento,:new.dt_vencimento),'A',:new.nm_usuario);
elsif (inserting) then
	/* Grava o agendamento da informação para atualização do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_sequencia,null,'CP',nvl(:new.dt_prev_pagamento,:new.dt_vencimento),'I',:new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CHEQUE_ATUAL
before update or insert ON TASY.CHEQUE for each row
declare
ie_data_comp_cheque_w	varchar2(255);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	nvl(max(ie_data_comp_cheque), 'S')
into	ie_data_comp_cheque_w
from	parametros_contas_pagar
where	cd_estabelecimento		= :new.cd_estabelecimento;

if	(ie_data_comp_cheque_w = 'N') and
	(:new.dt_compensacao is not null) and
	(:new.dt_emissao is not null) and
	(:new.dt_compensacao <> nvl(:old.dt_compensacao, :new.dt_compensacao - 1)) and
	(trunc(:new.dt_compensacao, 'dd') < trunc(:new.dt_emissao, 'dd')) then
	-- A data de compensacao do cheque deve ser maior ou igual a data de emissao do mesmo!
	wheb_mensagem_pck.exibir_mensagem_abort(266911);
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CHEQUE_INSERT
before insert ON TASY.CHEQUE for each row
declare

ie_dia_fechado_w		varchar2(1);
nr_seq_banco_saldo_w	number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(nvl(:new.nr_seq_conta_banco,0) > 0) then

	select	max(nr_sequencia)
	into	nr_seq_banco_saldo_w
	from	banco_saldo
	where	nr_seq_conta			= :new.nr_seq_conta_banco
	and	trunc(dt_referencia, 'month')	= trunc(:new.dt_emissao, 'month')
	and	dt_fechamento			is not null;

	if	(nr_seq_banco_saldo_w is not null) then
		/* O mes de referencia desta transacao ja tem saldo bancario fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(207890);
	end if;


	select	substr(obter_se_banco_fechado(:new.nr_seq_conta_banco,:new.dt_emissao),1,1)
	into	ie_dia_fechado_w
	from	dual;

	if	(nvl(ie_dia_fechado_w,'N')	= 'S') then
		/* O dia dt_transacao ja foi fechado no banco!
		Consulte o fechamento na pasta Fechamento banco do Controle Bancario.
		Parametro [72] */
		wheb_mensagem_pck.exibir_mensagem_abort(231442,'DT_TRANSACAO='||:new.dt_emissao);
	end if;
end if;

end if;

end;
/


ALTER TABLE TASY.CHEQUE ADD (
  CONSTRAINT CHEQUE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHEQUE ADD (
  CONSTRAINT CHEQUE_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT CHEQUE_AGEBANC_FK 
 FOREIGN KEY (CD_BANCO, CD_AGENCIA_BANCARIA) 
 REFERENCES TASY.AGENCIA_BANCARIA (CD_BANCO,CD_AGENCIA_BANCARIA),
  CONSTRAINT CHEQUE_PESDEST_FK 
 FOREIGN KEY (CD_CGC_DESTINATARIO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CHEQUE_PESEMIT_FK 
 FOREIGN KEY (CD_CGC_EMITENTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CHEQUE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_DESTINATARIO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CHEQUE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CHEQUE_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT CHEQUE_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_EMISSAO) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT CHEQUE_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_FIN_COMP) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CHEQUE TO NIVEL_1;

