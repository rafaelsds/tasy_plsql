DROP TABLE TASY.W_VALORES_HSJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_VALORES_HSJ
(
  NM_USUARIO        VARCHAR2(15 BYTE),
  CD_MATERIAL       NUMBER(6),
  DS_MATERIAL       VARCHAR2(255 BYTE),
  QT_MATERIAL       NUMBER(9),
  VL_MATERIAL       NUMBER(15,2),
  VL_UNITARIO       NUMBER(15,2),
  VL_CUSTO_MEDIO    NUMBER(15,2),
  CD_APRESENTACAO   VARCHAR2(9 BYTE),
  DS_APRESENTACAO   VARCHAR2(60 BYTE),
  IE_TIPO_PRECO     VARCHAR2(3 BYTE),
  IE_TIPO_RESTRITO  VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_VALORES_HSJ TO NIVEL_1;

