ALTER TABLE TASY.TISS_REGRA_RET_GUIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TISS_REGRA_RET_GUIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TISS_REGRA_RET_GUIA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_MASCARA_GUIA        VARCHAR2(50 BYTE),
  IE_USUARIO_CONVENIO    VARCHAR2(1 BYTE)       NOT NULL,
  IE_GUIA_PRESTADOR      VARCHAR2(5 BYTE)       NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_CONVENIO            NUMBER(5)              NOT NULL,
  IE_TIPO_PROTOCOLO      NUMBER(2),
  DS_MASCARA_GUIA_PREST  VARCHAR2(50 BYTE),
  IE_GUIA                VARCHAR2(5 BYTE)       NOT NULL,
  NR_ADICIONAL           VARCHAR2(255 BYTE),
  DS_MASCARA_HOSP        VARCHAR2(50 BYTE),
  IE_DIG_GUIA_HOSP       NUMBER(10),
  NR_ANTERIOR            VARCHAR2(255 BYTE),
  DS_MASCARA_USUARIO     VARCHAR2(255 BYTE),
  DT_INICIO_VIGENCIA     DATE,
  DT_FIM_VIGENCIA        DATE,
  IE_DATA_REALIZACAO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TISSRRG_CONVENI_FK_I ON TASY.TISS_REGRA_RET_GUIA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRRG_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TISSRRG_ESTABEL_FK_I ON TASY.TISS_REGRA_RET_GUIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TISSRRG_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TISSRRG_PK ON TASY.TISS_REGRA_RET_GUIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TISS_REGRA_RET_GUIA_tp  after update ON TASY.TISS_REGRA_RET_GUIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TIPO_PROTOCOLO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_PROTOCOLO,1,4000),substr(:new.IE_TIPO_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PROTOCOLO',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MASCARA_GUIA,1,500);gravar_log_alteracao(substr(:old.DS_MASCARA_GUIA,1,4000),substr(:new.DS_MASCARA_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA_GUIA',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_USUARIO_CONVENIO,1,500);gravar_log_alteracao(substr(:old.IE_USUARIO_CONVENIO,1,4000),substr(:new.IE_USUARIO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_USUARIO_CONVENIO',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_PRESTADOR,1,4000),substr(:new.IE_GUIA_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_PRESTADOR',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_INICIO_VIGENCIA,1,500);gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_ANTERIOR,1,500);gravar_log_alteracao(substr(:old.NR_ANTERIOR,1,4000),substr(:new.NR_ANTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_ANTERIOR',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MASCARA_GUIA_PREST,1,500);gravar_log_alteracao(substr(:old.DS_MASCARA_GUIA_PREST,1,4000),substr(:new.DS_MASCARA_GUIA_PREST,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA_GUIA_PREST',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_DIG_GUIA_HOSP,1,500);gravar_log_alteracao(substr(:old.IE_DIG_GUIA_HOSP,1,4000),substr(:new.IE_DIG_GUIA_HOSP,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIG_GUIA_HOSP',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MASCARA_HOSP,1,500);gravar_log_alteracao(substr(:old.DS_MASCARA_HOSP,1,4000),substr(:new.DS_MASCARA_HOSP,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA_HOSP',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_ADICIONAL,1,500);gravar_log_alteracao(substr(:old.NR_ADICIONAL,1,4000),substr(:new.NR_ADICIONAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_ADICIONAL',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA,1,500);gravar_log_alteracao(substr(:old.IE_GUIA,1,4000),substr(:new.IE_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA',ie_log_w,ds_w,'TISS_REGRA_RET_GUIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TISS_REGRA_RET_GUIA ADD (
  CONSTRAINT TISSRRG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TISS_REGRA_RET_GUIA ADD (
  CONSTRAINT TISSRRG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TISSRRG_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TISS_REGRA_RET_GUIA TO NIVEL_1;

