ALTER TABLE TASY.PESQUISA_SATISFACAO_RESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESQUISA_SATISFACAO_RESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESQUISA_SATISFACAO_RESP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PERGUNTA      NUMBER(10)               NOT NULL,
  NR_SEQ_PESQUISA      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_RESULTADO         VARCHAR2(255 BYTE),
  DT_RESULTADO         DATE,
  VL_RESULTADO         NUMBER(15,4),
  DS_OPCAO             VARCHAR2(3 BYTE),
  DS_PERGUNTA          VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESQR_PERGUNT_FK_I ON TASY.PESQUISA_SATISFACAO_RESP
(NR_SEQ_PERGUNTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESQR_PERGUNT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESQR_PESST_FK_I ON TASY.PESQUISA_SATISFACAO_RESP
(NR_SEQ_PESQUISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESQR_PESST_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PESQR_PK ON TASY.PESQUISA_SATISFACAO_RESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESQR_PK
  MONITORING USAGE;


ALTER TABLE TASY.PESQUISA_SATISFACAO_RESP ADD (
  CONSTRAINT PESQR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESQUISA_SATISFACAO_RESP ADD (
  CONSTRAINT PESQR_PERGUNT_FK 
 FOREIGN KEY (NR_SEQ_PERGUNTA) 
 REFERENCES TASY.PERGUNTA (NR_SEQUENCIA),
  CONSTRAINT PESQR_PESST_FK 
 FOREIGN KEY (NR_SEQ_PESQUISA) 
 REFERENCES TASY.PESQUISA_SATISFACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PESQUISA_SATISFACAO_RESP TO NIVEL_1;

