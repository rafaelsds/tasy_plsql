ALTER TABLE TASY.PLS_PROG_REAJ_EXCECAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PROG_REAJ_EXCECAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PROG_REAJ_EXCECAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_LOTE             NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO         NUMBER(10),
  NR_CONTRATO             NUMBER(10),
  CD_CLASSIF_CONTRATO     VARCHAR2(10 BYTE),
  NR_SEQ_TIPO_ADITAMENTO  NUMBER(10),
  IE_REGULAMENTACAO       VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPREX_PK ON TASY.PLS_PROG_REAJ_EXCECAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPREX_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPREX_PLSCONT_FK_I ON TASY.PLS_PROG_REAJ_EXCECAO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPREX_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPREX_PLSPRLT_FK_I ON TASY.PLS_PROG_REAJ_EXCECAO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPREX_PLSPRLT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPREX_PLSTPAD_FK_I ON TASY.PLS_PROG_REAJ_EXCECAO
(NR_SEQ_TIPO_ADITAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPREX_PLSTPAD_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_PROG_REAJ_EXCECAO ADD (
  CONSTRAINT PLSPREX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PROG_REAJ_EXCECAO ADD (
  CONSTRAINT PLSPREX_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSPREX_PLSPRLT_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_PROG_REAJ_COLET_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSPREX_PLSTPAD_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ADITAMENTO) 
 REFERENCES TASY.PLS_TIPO_ADITAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PROG_REAJ_EXCECAO TO NIVEL_1;

