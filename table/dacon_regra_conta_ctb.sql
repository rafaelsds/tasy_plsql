ALTER TABLE TASY.DACON_REGRA_CONTA_CTB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DACON_REGRA_CONTA_CTB CASCADE CONSTRAINTS;

CREATE TABLE TASY.DACON_REGRA_CONTA_CTB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DACON         NUMBER(10)               NOT NULL,
  CD_REGISTRO          VARCHAR2(15 BYTE)        NOT NULL,
  IE_TIPO_CONTA        VARCHAR2(15 BYTE),
  IE_TIPO_VALOR        VARCHAR2(15 BYTE)        NOT NULL,
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DACONCTB_CONCONT_FK_I ON TASY.DACON_REGRA_CONTA_CTB
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DACONCTB_DACON_FK_I ON TASY.DACON_REGRA_CONTA_CTB
(NR_SEQ_DACON)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DACONCTB_PK ON TASY.DACON_REGRA_CONTA_CTB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DACON_REGRA_CONTA_CTB ADD (
  CONSTRAINT DACONCTB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DACON_REGRA_CONTA_CTB ADD (
  CONSTRAINT DACONCTB_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT DACONCTB_DACON_FK 
 FOREIGN KEY (NR_SEQ_DACON) 
 REFERENCES TASY.DACON (NR_SEQUENCIA));

GRANT SELECT ON TASY.DACON_REGRA_CONTA_CTB TO NIVEL_1;

