ALTER TABLE TASY.PLS_DMA_PROC_GLOSA_IMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_DMA_PROC_GLOSA_IMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_DMA_PROC_GLOSA_IMP
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_DMA_PROCEDIMENTO  NUMBER(10)           NOT NULL,
  CD_GLOSA                 VARCHAR2(4 BYTE)     NOT NULL,
  VL_GLOSA                 NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSDARG_PK ON TASY.PLS_DMA_PROC_GLOSA_IMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSDARG_PLSDAPP_FK_I ON TASY.PLS_DMA_PROC_GLOSA_IMP
(NR_SEQ_DMA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_DMA_PROC_GLOSA_IMP ADD (
  CONSTRAINT PLSDARG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_DMA_PROC_GLOSA_IMP ADD (
  CONSTRAINT PLSDARG_PLSDAPP_FK 
 FOREIGN KEY (NR_SEQ_DMA_PROCEDIMENTO) 
 REFERENCES TASY.PLS_DMA_PROCEDIMENTO_IMP (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_DMA_PROC_GLOSA_IMP TO NIVEL_1;

