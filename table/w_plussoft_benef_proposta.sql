DROP TABLE TASY.W_PLUSSOFT_BENEF_PROPOSTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PLUSSOFT_BENEF_PROPOSTA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_ITEM                  NUMBER(10)           NOT NULL,
  NR_TITULAR_SOLIC         NUMBER(10),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  NR_SEQ_PARENTESCO        NUMBER(10),
  DT_NASCIMENTO            DATE,
  IE_NASCIDO_PLANO         VARCHAR2(2 BYTE),
  NR_SEQ_TITULAR_PROP      NUMBER(10),
  NR_SEQ_TITULAR_CONTRATO  NUMBER(10),
  NR_SEQ_BENEF_PROP        NUMBER(10),
  NR_SEQ_REGRA_CARENCIA    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WPLBPROP_PESFISI_FK_I ON TASY.W_PLUSSOFT_BENEF_PROPOSTA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_PLUSSOFT_BENEF_PROPOSTA ADD (
  CONSTRAINT WPLBPROP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.W_PLUSSOFT_BENEF_PROPOSTA TO NIVEL_1;

