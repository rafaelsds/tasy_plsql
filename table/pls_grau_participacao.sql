ALTER TABLE TASY.PLS_GRAU_PARTICIPACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GRAU_PARTICIPACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GRAU_PARTICIPACAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_GRAU_PARTICIPACAO  VARCHAR2(255 BYTE)      NOT NULL,
  CD_TISS               VARCHAR2(20 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_MEDICO             VARCHAR2(1 BYTE)        NOT NULL,
  IE_ANESTESISTA        VARCHAR2(1 BYTE)        NOT NULL,
  IE_AUXILIAR           VARCHAR2(1 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  NR_SEQ_ANTERIOR       NUMBER(10),
  CD_PTU                VARCHAR2(1 BYTE),
  NR_SEQ_APRES          NUMBER(10),
  VL_POSICAO_AUXILIAR   NUMBER(10),
  IE_VERSAO             VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSGRPA_ESTABEL_FK_I ON TASY.PLS_GRAU_PARTICIPACAO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGRPA_I1 ON TASY.PLS_GRAU_PARTICIPACAO
(CD_TISS, CD_ESTABELECIMENTO, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSGRPA_PK ON TASY.PLS_GRAU_PARTICIPACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_GRAU_PARTICIPACAO_tp  after update ON TASY.PLS_GRAU_PARTICIPACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_VERSAO,1,4000),substr(:new.IE_VERSAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_GRAU_PARTICIPACAO,1,4000),substr(:new.DS_GRAU_PARTICIPACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_GRAU_PARTICIPACAO',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TISS,1,4000),substr(:new.CD_TISS,1,4000),:new.nm_usuario,nr_seq_w,'CD_TISS',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_POSICAO_AUXILIAR,1,4000),substr(:new.VL_POSICAO_AUXILIAR,1,4000),:new.nm_usuario,nr_seq_w,'VL_POSICAO_AUXILIAR',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ANESTESISTA,1,4000),substr(:new.IE_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ANESTESISTA',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUXILIAR,1,4000),substr(:new.IE_AUXILIAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUXILIAR',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PTU,1,4000),substr(:new.CD_PTU,1,4000),:new.nm_usuario,nr_seq_w,'CD_PTU',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MEDICO,1,4000),substr(:new.IE_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDICO',ie_log_w,ds_w,'PLS_GRAU_PARTICIPACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_GRAU_PARTICIPACAO ADD (
  CONSTRAINT PLSGRPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GRAU_PARTICIPACAO ADD (
  CONSTRAINT PLSGRPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_GRAU_PARTICIPACAO TO NIVEL_1;

