ALTER TABLE TASY.CIRURGIA_TEC_ANESTESICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIRURGIA_TEC_ANESTESICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIRURGIA_TEC_ANESTESICA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_TECNICA             NUMBER(10)         NOT NULL,
  DT_INICIO                  DATE,
  DT_FINAL                   DATE,
  NR_CIRURGIA                NUMBER(10),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_SEQ_PEPO                NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_PROTOCOLO           NUMBER(10),
  NR_PROTOCOLO_PEPO          NUMBER(10),
  NR_SEQ_EVENTO              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIRTEAN_CIRURGI_FK_I ON TASY.CIRURGIA_TEC_ANESTESICA
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRTEAN_CITEANE_FK_I ON TASY.CIRURGIA_TEC_ANESTESICA
(NR_SEQ_TECNICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRTEAN_CITEANE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRTEAN_PEPOCIR_FK_I ON TASY.CIRURGIA_TEC_ANESTESICA
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRTEAN_PESFISI_FK_I ON TASY.CIRURGIA_TEC_ANESTESICA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIRTEAN_PK ON TASY.CIRURGIA_TEC_ANESTESICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRTEAN_TASASDI_FK_I ON TASY.CIRURGIA_TEC_ANESTESICA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRTEAN_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRTEAN_TASASDI_FK2_I ON TASY.CIRURGIA_TEC_ANESTESICA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRTEAN_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cirurgia_tec_anestesica_insert
before insert ON TASY.CIRURGIA_TEC_ANESTESICA for each row
declare
nm_usuario_w   usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
qt_reg_w		   number(1);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if (:new.dt_atualizacao_nrec is null) then
   :new.dt_atualizacao_nrec := sysdate;
end if;

if (:new.nm_usuario_nrec is null) then
   :new.nm_usuario_nrec := nm_usuario_w;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.cirurgia_tec_anest_pend_atual
after insert or update ON TASY.CIRURGIA_TEC_ANESTESICA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);
ie_libera_anestesia_w	varchar2(15);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	nvl(max(ie_libera_anestesia),'N')
into		ie_libera_anestesia_w
from		parametro_medico
where		cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_anestesia_w = 'S') then
	select	max(c.nr_atendimento),
		max(c.cd_pessoa_fisica)
	into	nr_atendimento_w,
		cd_pessoa_fisica_w
	from	cirurgia c
	where	c.nr_cirurgia = :new.nr_cirurgia;

	if 	(cd_pessoa_fisica_w is null) then
		select	max(c.nr_atendimento),
			max(c.cd_pessoa_fisica)
		into	nr_atendimento_w,
			cd_pessoa_fisica_w
		from	pepo_cirurgia c
		where	c.nr_sequencia = :new.nr_seq_pepo;
	end if;

	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'CTA';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCTA';
	end if;

	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario,null, null, null, null, null, null, null);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.CIR_TEC_ANEST_BEF_DEL before
  DELETE ON TASY.CIRURGIA_TEC_ANESTESICA FOR EACH row
DECLARE
  BEGIN

    UPDATE med_avaliacao_paciente
    SET nr_seq_tec_anest   = NULL
    WHERE nr_seq_tec_anest = :old.nr_sequencia;

EXCEPTION
WHEN OTHERS THEN
  NULL;
END;
/


CREATE OR REPLACE TRIGGER TASY.hsj_verifica_antib_pepo
before insert or update ON TASY.CIRURGIA_TEC_ANESTESICA for each row
DISABLE
declare
cursor v_antibi is
select A.*, case when (A.dt_liberacao)  is null then 0 else 1 end v_data
from CIRURGIA_AGENTE_ANEST_OCOR A
join CIRURGIA_AGENTE_ANESTESICO B on (B.NR_SEQUENCIA=A.NR_SEQ_CIRUR_AGENTE)
where  B.nr_cirurgia=:new.nr_cirurgia
and A.ie_situacao='A'
and OBTER_DADOS_MATERIAL(cd_material,'CGRU')=1;


v_data number(10);

begin

for v_01 in v_antibi loop

if v_01.v_data=0  then
raise_application_error(-20500,'Falta registrar data de administração dos Antibióticos da Cirurgia!');
end if;

end loop;

end;
/


ALTER TABLE TASY.CIRURGIA_TEC_ANESTESICA ADD (
  CONSTRAINT CIRTEAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIRURGIA_TEC_ANESTESICA ADD (
  CONSTRAINT CIRTEAN_CITEANE_FK 
 FOREIGN KEY (NR_SEQ_TECNICA) 
 REFERENCES TASY.TECNICA_ANESTESICA (NR_SEQUENCIA),
  CONSTRAINT CIRTEAN_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIRTEAN_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT CIRTEAN_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CIRTEAN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CIRTEAN_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.CIRURGIA_TEC_ANESTESICA TO NIVEL_1;

