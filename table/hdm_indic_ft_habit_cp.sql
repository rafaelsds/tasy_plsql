ALTER TABLE TASY.HDM_INDIC_FT_HABIT_CP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HDM_INDIC_FT_HABIT_CP CASCADE CONSTRAINTS;

CREATE TABLE TASY.HDM_INDIC_FT_HABIT_CP
(
  NR_SEQ_DIMENSION  NUMBER(10)                  NOT NULL,
  NR_SEQ_FACT       NUMBER(10)                  NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDMIFHC_HDMIDCA_FK_I ON TASY.HDM_INDIC_FT_HABIT_CP
(NR_SEQ_DIMENSION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.HDMIFHC_HDMIFHA_FK_I ON TASY.HDM_INDIC_FT_HABIT_CP
(NR_SEQ_FACT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDMIFHC_PK ON TASY.HDM_INDIC_FT_HABIT_CP
(NR_SEQ_DIMENSION, NR_SEQ_FACT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.HDM_INDIC_FT_HABIT_CP ADD (
  CONSTRAINT HDMIFHC_PK
 PRIMARY KEY
 (NR_SEQ_DIMENSION, NR_SEQ_FACT)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HDM_INDIC_FT_HABIT_CP ADD (
  CONSTRAINT HDMIFHC_HDMIDCA_FK 
 FOREIGN KEY (NR_SEQ_DIMENSION) 
 REFERENCES TASY.HDM_INDIC_DM_CAMPAIGN (NR_SEQUENCIA),
  CONSTRAINT HDMIFHC_HDMIFHA_FK 
 FOREIGN KEY (NR_SEQ_FACT) 
 REFERENCES TASY.HDM_INDIC_FT_HABIT (NR_SEQUENCIA));

GRANT SELECT ON TASY.HDM_INDIC_FT_HABIT_CP TO NIVEL_1;

