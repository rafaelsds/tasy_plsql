ALTER TABLE TASY.REGRA_HORARIO_SOLUCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_HORARIO_SOLUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_HORARIO_SOLUCAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ETAPAS            NUMBER(3),
  DT_PRIM_HORARIO      VARCHAR2(5 BYTE)         NOT NULL,
  HR_INICIO            VARCHAR2(5 BYTE),
  HR_FIM               VARCHAR2(5 BYTE),
  DT_PRIM_HORARIO_AUX  DATE,
  DT_FIM               DATE,
  DT_INICIO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGHORS_PK ON TASY.REGRA_HORARIO_SOLUCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGHORS_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.atual_regra_horario_solucao
before insert or update ON TASY.REGRA_HORARIO_SOLUCAO for each row
declare

begin

if (:new.hr_inicio is not null) and
   ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_inicio is null)) then
	:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

if (:new.hr_fim is not null) and
   ((:new.hr_fim <> :old.hr_fim) or (:old.dt_fim is null)) then
	:new.dt_fim := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_fim || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

end;
/


ALTER TABLE TASY.REGRA_HORARIO_SOLUCAO ADD (
  CONSTRAINT REGHORS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REGRA_HORARIO_SOLUCAO TO NIVEL_1;

