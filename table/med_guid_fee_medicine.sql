DROP TABLE TASY.MED_GUID_FEE_MEDICINE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_GUID_FEE_MEDICINE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(6),
  DT_START             DATE,
  DT_END               DATE,
  SI_CHECK_MEDICINE    VARCHAR2(5 BYTE),
  NR_SEQ_MED_GUID_FEE  NUMBER(10),
  DT_ATUALIZACAO       DATE,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FK_MED_GUID_FEE_I ON TASY.MED_GUID_FEE_MEDICINE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


