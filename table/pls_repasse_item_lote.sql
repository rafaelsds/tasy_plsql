ALTER TABLE TASY.PLS_REPASSE_ITEM_LOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REPASSE_ITEM_LOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REPASSE_ITEM_LOTE
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_REPASSE           NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  QT_VIDAS                 NUMBER(5),
  QT_META                  NUMBER(10),
  PR_META                  NUMBER(7,4),
  NR_SEQ_EQUIPE_VEND_META  NUMBER(10),
  IE_TIPO_CONTRATO         VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSREIL_PK ON TASY.PLS_REPASSE_ITEM_LOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREIL_PLSEVME_FK_I ON TASY.PLS_REPASSE_ITEM_LOTE
(NR_SEQ_EQUIPE_VEND_META)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREIL_PLSREVE_FK_I ON TASY.PLS_REPASSE_ITEM_LOTE
(NR_SEQ_REPASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REPASSE_ITEM_LOTE ADD (
  CONSTRAINT PLSREIL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REPASSE_ITEM_LOTE ADD (
  CONSTRAINT PLSREIL_PLSEVME_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE_VEND_META) 
 REFERENCES TASY.PLS_EQUIPE_VEND_META (NR_SEQUENCIA),
  CONSTRAINT PLSREIL_PLSREVE_FK 
 FOREIGN KEY (NR_SEQ_REPASSE) 
 REFERENCES TASY.PLS_REPASSE_VEND (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REPASSE_ITEM_LOTE TO NIVEL_1;

