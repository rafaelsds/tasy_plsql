ALTER TABLE TASY.PLS_CONTRATO_GRUPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTRATO_GRUPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTRATO_GRUPO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_GRUPO          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  NR_SEQ_CONTRATO       NUMBER(10),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIFICACAO  NUMBER(10),
  NR_SEQ_INTERCAMBIO    NUMBER(10),
  NR_CONTRATO           NUMBER(10),
  IE_REAJUSTE_GRUPO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCOGR_ESTABEL_FK_I ON TASY.PLS_CONTRATO_GRUPO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOGR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSCOGR_PK ON TASY.PLS_CONTRATO_GRUPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOGR_PLSCONT_FK_I ON TASY.PLS_CONTRATO_GRUPO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOGR_PLSGRCC_FK_I ON TASY.PLS_CONTRATO_GRUPO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOGR_PLSGRCO_FK_I ON TASY.PLS_CONTRATO_GRUPO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOGR_PLSINCA_FK_I ON TASY.PLS_CONTRATO_GRUPO
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_contrato_grupo_after
after insert or delete or update ON TASY.PLS_CONTRATO_GRUPO for each row
declare

begin

null;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_contrato_grupo_bef_update
before update ON TASY.PLS_CONTRATO_GRUPO for each row
declare

begin

if	((:old.nr_contrato <> :new.nr_contrato) or (:old.nr_seq_intercambio <> :new.nr_seq_intercambio)) then
	wheb_mensagem_pck.exibir_mensagem_abort(1033992);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_contrato_grupo_bef_insert
before insert ON TASY.PLS_CONTRATO_GRUPO for each row
declare
ie_tipo_relacionamento_w	pls_grupo_contrato.ie_tipo_relacionamento%type;
nr_seq_grupo_w			pls_contrato_grupo.nr_seq_grupo%type;
ie_permite_vinculo_w		varchar2(1);

begin
--Permitir vincular um contrato em mais de um grupo
ie_permite_vinculo_w	:= nvl(obter_valor_param_usuario(1278, 7, Obter_Perfil_Ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento), 'N');

if	(ie_permite_vinculo_w = 'N') then
	-- Tipo de relacionamento do grupo em que o contrato est�.
	select	max(b.ie_tipo_relacionamento)
	into	ie_tipo_relacionamento_w
	from	pls_grupo_contrato b
	where	b.nr_sequencia = :new.nr_seq_grupo;

	-- Verifica se o contrato est� vinculado em outro grupo com o mesmo tipo de relacionamento
	select	max(b.nr_sequencia)
	into	nr_seq_grupo_w
	from	pls_contrato_grupo a,
		pls_grupo_contrato b
	where	b.nr_sequencia = a.nr_seq_grupo
	and	a.nr_contrato = :new.nr_contrato
	and	b.ie_tipo_relacionamento = ie_tipo_relacionamento_w;

	if	(nr_seq_grupo_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(347963, 'NR_CONTRATO_W='   || :new.nr_contrato ||
								';NR_SEQ_GRUPO='   || nr_seq_grupo_w);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ssj_atualiza_grupo_contrato
before insert or update
ON TASY.PLS_CONTRATO_GRUPO REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
declare

qt_registros_w number(10);
nr_seq_contrato_w number(10);
nr_seq_grupo_w number(10);

begin

    nr_seq_grupo_w:= :new.nr_seq_grupo;
    nr_seq_contrato_w:= :new.nr_seq_contrato;

    select count(*)
    into qt_registros_w
    from pls_segurado_valores 
    where nr_seq_contrato = nr_seq_contrato_w
    and (nr_seq_grupo_contrato != nr_seq_grupo_w
    or nr_seq_grupo_contrato is null)
    and nr_seq_grupo_w is not null;

    
    if(qt_registros_w > 0)then
        
        update pls_segurado_valores set nr_seq_grupo_contrato = nr_seq_grupo_w
        where nr_seq_contrato = nr_seq_contrato_w
        and (nr_seq_grupo_contrato != nr_seq_grupo_w
        or nr_seq_grupo_contrato is null)
        and nr_seq_grupo_w is not null;
    
    end if;
    
    
    
end ssj_atualiza_grupo_contrato;
/


ALTER TABLE TASY.PLS_CONTRATO_GRUPO ADD (
  CONSTRAINT PLSCOGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTRATO_GRUPO ADD (
  CONSTRAINT PLSCOGR_PLSGRCO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PLS_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCOGR_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCOGR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCOGR_PLSGRCC_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.PLS_GRUPO_CONTR_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT PLSCOGR_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTRATO_GRUPO TO NIVEL_1;

