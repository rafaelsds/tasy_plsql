ALTER TABLE TASY.EHR_TEMPLATE_CONT_INF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EHR_TEMPLATE_CONT_INF CASCADE CONSTRAINTS;

CREATE TABLE TASY.EHR_TEMPLATE_CONT_INF
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  NR_SEQ_EXAME          NUMBER(10),
  IE_RESULT_EXAME       VARCHAR2(15 BYTE),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_INFORMACAO         VARCHAR2(15 BYTE)       NOT NULL,
  IE_ATENDIMENTO        VARCHAR2(1 BYTE),
  NR_SEQ_TEMP_CONTEUDO  NUMBER(10)              NOT NULL,
  IE_INF_ENTIDADE       VARCHAR2(1 BYTE),
  IE_PROFISSIONAL       VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EHTECOI_EHTTEIN_FK_I ON TASY.EHR_TEMPLATE_CONT_INF
(NR_SEQ_TEMP_CONTEUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EHTECOI_ESTABEL_FK_I ON TASY.EHR_TEMPLATE_CONT_INF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EHTECOI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EHTECOI_EXALABO_FK_I ON TASY.EHR_TEMPLATE_CONT_INF
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EHTECOI_EXALABO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.EHTECOI_PK ON TASY.EHR_TEMPLATE_CONT_INF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EHTECOI_PK
  MONITORING USAGE;


ALTER TABLE TASY.EHR_TEMPLATE_CONT_INF ADD (
  CONSTRAINT EHTECOI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EHR_TEMPLATE_CONT_INF ADD (
  CONSTRAINT EHTECOI_EHTTEIN_FK 
 FOREIGN KEY (NR_SEQ_TEMP_CONTEUDO) 
 REFERENCES TASY.EHR_TEMPLATE_CONTEUDO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EHTECOI_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT EHTECOI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.EHR_TEMPLATE_CONT_INF TO NIVEL_1;

