ALTER TABLE TASY.SUS_S_PASRV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_S_PASRV CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_S_PASRV
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_PROCEDIMENTO     NUMBER(15),
  IE_ORIGEM_PROCED    NUMBER(10),
  CD_SERVICO          VARCHAR2(3 BYTE),
  CD_CLASSIF_SERVICO  VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSPASR_PK ON TASY.SUS_S_PASRV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSPASR_PK
  MONITORING USAGE;


ALTER TABLE TASY.SUS_S_PASRV ADD (
  CONSTRAINT SUSPASR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUS_S_PASRV TO NIVEL_1;

