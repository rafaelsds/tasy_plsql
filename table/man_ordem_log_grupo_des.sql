ALTER TABLE TASY.MAN_ORDEM_LOG_GRUPO_DES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_LOG_GRUPO_DES CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_LOG_GRUPO_DES
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ORDEM_SERV    NUMBER(10)               NOT NULL,
  NR_SEQ_GRUPO_DES     NUMBER(10),
  CD_FUNCAO            NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANLGGD_FUNCAO_FK_I ON TASY.MAN_ORDEM_LOG_GRUPO_DES
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANLGGD_GRUDESE_FK_I ON TASY.MAN_ORDEM_LOG_GRUPO_DES
(NR_SEQ_GRUPO_DES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANLGGD_I2 ON TASY.MAN_ORDEM_LOG_GRUPO_DES
(DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANLGGD_MANORSE_FK_I ON TASY.MAN_ORDEM_LOG_GRUPO_DES
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANLGGD_PK ON TASY.MAN_ORDEM_LOG_GRUPO_DES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_ORDEM_LOG_GRUPO_DES ADD (
  CONSTRAINT MANLGGD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_LOG_GRUPO_DES ADD (
  CONSTRAINT MANLGGD_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT MANLGGD_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANLGGD_GRUDESE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_DES) 
 REFERENCES TASY.GRUPO_DESENVOLVIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORDEM_LOG_GRUPO_DES TO NIVEL_1;

