ALTER TABLE TASY.ESUS_ATEND_ODON_PROCED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESUS_ATEND_ODON_PROCED CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESUS_ATEND_ODON_PROCED
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_PROCEDIMENTO      NUMBER(10,2)             NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10)               NOT NULL,
  NR_SEQ_ATEND_ONCO    NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESUSANP_ESUSAOD_FK_I ON TASY.ESUS_ATEND_ODON_PROCED
(NR_SEQ_ATEND_ONCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESUSANP_PK ON TASY.ESUS_ATEND_ODON_PROCED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESUSANP_PROCEDI_FK_I ON TASY.ESUS_ATEND_ODON_PROCED
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ESUS_ATEND_ODON_PROCED ADD (
  CONSTRAINT ESUSANP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESUS_ATEND_ODON_PROCED ADD (
  CONSTRAINT ESUSANP_ESUSAON_FK 
 FOREIGN KEY (NR_SEQ_ATEND_ONCO) 
 REFERENCES TASY.ESUS_ATEND_ONCOLOGICO (NR_SEQUENCIA),
  CONSTRAINT ESUSANP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT ESUSANP_ESUSAOD_FK 
 FOREIGN KEY (NR_SEQ_ATEND_ONCO) 
 REFERENCES TASY.ESUS_ATEND_ODONTOLOG (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESUS_ATEND_ODON_PROCED TO NIVEL_1;

