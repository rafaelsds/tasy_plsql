ALTER TABLE TASY.MAN_SLA_LOC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_SLA_LOC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_SLA_LOC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_SLA           NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOCAL         NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANSLAL_MANLOCA_FK_I ON TASY.MAN_SLA_LOC
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANSLAL_MANSLA_FK_I ON TASY.MAN_SLA_LOC
(NR_SEQ_SLA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANSLAL_PK ON TASY.MAN_SLA_LOC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hsj_man_sla_loc_insupd
before insert or update or delete ON TASY.MAN_SLA_LOC for each row
begin

    if(wheb_usuario_pck.get_cd_perfil != 272)then
    
        if(inserting)then
            hsj_gerar_log('Perfil sem permiss�o para inserir o registro!');
        elsif(updating)then
            hsj_gerar_log('Perfil sem permiss�o para alterar o registro!');
        elsif(deleting)then
            hsj_gerar_log('Perfil sem permiss�o para excluir o registro!');
        else
            hsj_gerar_log('Perfil sem permiss�o!');
        end if;
    
    end if;
    
end hsj_man_sla_loc_insupd;
/


ALTER TABLE TASY.MAN_SLA_LOC ADD (
  CONSTRAINT MANSLAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_SLA_LOC ADD (
  CONSTRAINT MANSLAL_MANSLA_FK 
 FOREIGN KEY (NR_SEQ_SLA) 
 REFERENCES TASY.MAN_SLA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANSLAL_MANLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.MAN_LOCALIZACAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MAN_SLA_LOC TO NIVEL_1;

