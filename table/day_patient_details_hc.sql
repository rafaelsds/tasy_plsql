ALTER TABLE TASY.DAY_PATIENT_DETAILS_HC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DAY_PATIENT_DETAILS_HC CASCADE CONSTRAINTS;

CREATE TABLE TASY.DAY_PATIENT_DETAILS_HC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ADMISSION_TIME    TIMESTAMP(6),
  DT_EXIT_TIME         TIMESTAMP(6),
  NR_BAND              NUMBER(10),
  DS_ANAESTHETIC       VARCHAR2(10 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  DT_START_TIME_TH     VARCHAR2(50 BYTE),
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_END_TIME_TH       VARCHAR2(50 BYTE),
  NR_ATENDIMENTO       NUMBER(10)               NOT NULL,
  DS_TIME_IN_THEATRE   VARCHAR2(500 BYTE),
  DS_TIME_IN_THEATRE1  VARCHAR2(100 BYTE),
  DS_TIME_IN_THEATRE2  VARCHAR2(100 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DP_HC21_ATEPACI_FK_I ON TASY.DAY_PATIENT_DETAILS_HC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DP_HC21_PK ON TASY.DAY_PATIENT_DETAILS_HC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DAY_PATIENT_DETAILS_HC ADD (
  CONSTRAINT DP_HC21_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.DAY_PATIENT_DETAILS_HC ADD (
  CONSTRAINT DP_HC21_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.DAY_PATIENT_DETAILS_HC TO NIVEL_1;

