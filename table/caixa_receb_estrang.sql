ALTER TABLE TASY.CAIXA_RECEB_ESTRANG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAIXA_RECEB_ESTRANG CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAIXA_RECEB_ESTRANG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CAIXA_RECEB   NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MOEDA             NUMBER(5)                NOT NULL,
  VL_ESPECIE           NUMBER(15,2)             NOT NULL,
  VL_COTACAO           NUMBER(21,10)            NOT NULL,
  VL_COMPLEMENTO       NUMBER(15,2)             NOT NULL,
  VL_ESPECIE_ESTRANG   NUMBER(15,2)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CAIRECEST_CAIREC_FK_I ON TASY.CAIXA_RECEB_ESTRANG
(NR_SEQ_CAIXA_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAIRECEST_MOEDA_FK_I ON TASY.CAIXA_RECEB_ESTRANG
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CAIRECEST_PK ON TASY.CAIXA_RECEB_ESTRANG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CAIXA_RECEB_ESTRANG ADD (
  CONSTRAINT CAIRECEST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CAIXA_RECEB_ESTRANG ADD (
  CONSTRAINT CAIRECEST_CAIREC_FK 
 FOREIGN KEY (NR_SEQ_CAIXA_RECEB) 
 REFERENCES TASY.CAIXA_RECEB (NR_SEQUENCIA),
  CONSTRAINT CAIRECEST_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA));

GRANT SELECT ON TASY.CAIXA_RECEB_ESTRANG TO NIVEL_1;

