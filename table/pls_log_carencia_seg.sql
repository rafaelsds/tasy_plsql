ALTER TABLE TASY.PLS_LOG_CARENCIA_SEG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOG_CARENCIA_SEG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOG_CARENCIA_SEG
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_CARENCIA          NUMBER(10),
  NR_SEQ_SEGURADO          NUMBER(10),
  NR_SEQ_PLANO             NUMBER(10),
  NR_SEQ_REGRA_ALT_ACOMOD  NUMBER(10),
  DT_INICIIO_VIGENCIA      DATE,
  QT_DIAS                  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLCSG_PK ON TASY.PLS_LOG_CARENCIA_SEG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLCSG_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSLCSG_PLSRACA_FK_I ON TASY.PLS_LOG_CARENCIA_SEG
(NR_SEQ_REGRA_ALT_ACOMOD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLCSG_PLSRACA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_LOG_CARENCIA_SEG ADD (
  CONSTRAINT PLSLCSG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOG_CARENCIA_SEG ADD (
  CONSTRAINT PLSLCSG_PLSRACA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ALT_ACOMOD) 
 REFERENCES TASY.PLS_REGRA_ALT_CAREN_ACOMOD (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOG_CARENCIA_SEG TO NIVEL_1;

