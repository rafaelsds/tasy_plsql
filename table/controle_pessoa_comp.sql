ALTER TABLE TASY.CONTROLE_PESSOA_COMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTROLE_PESSOA_COMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTROLE_PESSOA_COMP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_TIPO_CONTROLE     NUMBER(5)                NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_ENTRADA           DATE                     NOT NULL,
  DT_SAIDA             DATE,
  DS_OBSERVACAO        VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONPECO_PESFISI_FK_I ON TASY.CONTROLE_PESSOA_COMP
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONPECO_PK ON TASY.CONTROLE_PESSOA_COMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPECO_PK
  MONITORING USAGE;


CREATE INDEX TASY.CONPECO_TIPCOPE_FK_I ON TASY.CONTROLE_PESSOA_COMP
(CD_TIPO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPECO_TIPCOPE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hsj_controle_pessoa_wifi_upd
BEFORE INSERT or update
ON TASY.CONTROLE_PESSOA_COMP FOR EACH ROW
DECLARE

qt_reg_w number(10);

BEGIN

    if(:NEW.cd_tipo_controle = 1)then -- Se o tipo for = "Pessoa Fisica"
    
        select count(*)
        into qt_reg_w
        from radius.RADUSERGROUP
        where username = to_char(:new.NR_SEQUENCIA);
    
        if(inserting and :new.dt_entrada is not null and :new.dt_saida is null and qt_reg_w = 0)then
            /*
                Ao inserir registro e o mesmo possuir data de entrada e nao possuir data de saida, al�m de n�o existir no radius group, ent�o ser� inserido
            */        
                
            begin             
                insert into radius.RADUSERGROUP
                (
                    id,
                    username,
                    groupname
                )
                values
                (
                    radius.RADUSERGROUP_SEQ.nextval,
                    to_char(:new.NR_SEQUENCIA),
                    'BIBLIOTECA'
                                
                );      
            EXCEPTION WHEN OTHERS THEN
               hsj_gerar_log('Erro ao gerar voucher!');
                               
            end;
            
        elsif(updating and :new.dt_saida is not null and qt_reg_w > 0)then
            /*
                Ap�s inserir data de sa�da no controle de pessoas e este usu�rio esteja no radius group, ent�o o registro ser� deletado
            */   
            
            delete radius.RADUSERGROUP
            where username = to_char(:new.NR_SEQUENCIA)
            and groupname =  'BIBLIOTECA';
        
        end if;    
    end if;
    
END hsj_controle_pessoa_wifi_upd;
/


CREATE OR REPLACE TRIGGER TASY.hsj_vouchers_update
BEFORE INSERT
ON TASY.CONTROLE_PESSOA_COMP REFERENCING NEW AS New OLD AS Old
FOR EACH ROW
DECLARE
tmpVar VARCHAR(50);
/******************************************************************************
   NAME:       
   PURPOSE:    

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        22/02/2017             1. Created this trigger.

   NOTES:

   Automatically available Auto Replace Keywords:
      Object Name:     
      Sysdate:         22/02/2017
      Date and Time:   22/02/2017, 09:06:48, and 22/02/2017 09:06:48
      Username:         (set in TOAD Options, Proc Templates)
      Table Name:       (set in the "New PL/SQL Object" dialog)
      Trigger Options:  (set in the "New PL/SQL Object" dialog)
******************************************************************************/
BEGIN
   if :NEW.cd_tipo_controle = 1 then
   begin
   tmpVar := '';

    
   select min(voucher)  
   into tmpVar
   from hsj_vouchers_biblio where dt_utilizacao is null;
   
   
   :NEW.DS_OBSERVACAO := tmpVar;
   
   UPDATE hsj_vouchers_biblio set dt_utilizacao = sysdate where voucher = tmpVar;
   
   EXCEPTION
     WHEN OTHERS THEN
       -- Consider logging the error and then re-raise
       RAISE;
       
  end;
  end if;
END ;
/


ALTER TABLE TASY.CONTROLE_PESSOA_COMP ADD (
  CONSTRAINT CONPECO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTROLE_PESSOA_COMP ADD (
  CONSTRAINT CONPECO_TIPCOPE_FK 
 FOREIGN KEY (CD_TIPO_CONTROLE) 
 REFERENCES TASY.TIPO_CONTROLE_PESSOA (CD_TIPO_CONTROLE),
  CONSTRAINT CONPECO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CONTROLE_PESSOA_COMP TO NIVEL_1;

