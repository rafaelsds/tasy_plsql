ALTER TABLE TASY.PLS_PLANO_SCA_RESTRICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PLANO_SCA_RESTRICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PLANO_SCA_RESTRICAO
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  NR_SEQ_PLANO_SCA               NUMBER(10)     NOT NULL,
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  IE_CLASSIFICACAO_SCA_CONTRATO  VARCHAR2(1 BYTE),
  QT_DIAS_INCLUSAO_OPS           NUMBER(10),
  NR_CONTRATO                    NUMBER(10),
  NR_SEQ_CONTRATO                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPLSR_PK ON TASY.PLS_PLANO_SCA_RESTRICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPLSR_PLSCONT_FK_I ON TASY.PLS_PLANO_SCA_RESTRICAO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPLSR_PLSSEAD_FK_I ON TASY.PLS_PLANO_SCA_RESTRICAO
(NR_SEQ_PLANO_SCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPLSR_PLSSEAD_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_plano_sca_restricao_befins
before insert or update ON TASY.PLS_PLANO_SCA_RESTRICAO for each row
declare

nr_seq_contrato_w 	pls_contrato.nr_sequencia%type;

begin

if	(:new.nr_contrato is not null) then
	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato = :new.nr_contrato;

	if	(nr_seq_contrato_w is not null) then
		:new.nr_seq_contrato := nr_seq_contrato_w;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(853575); --O contrato informado n�o existe. Verifique!
	end if;
else
	:new.nr_seq_contrato	:= null;
end if;

end;
/


ALTER TABLE TASY.PLS_PLANO_SCA_RESTRICAO ADD (
  CONSTRAINT PLSPLSR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PLANO_SCA_RESTRICAO ADD (
  CONSTRAINT PLSPLSR_PLSSEAD_FK 
 FOREIGN KEY (NR_SEQ_PLANO_SCA) 
 REFERENCES TASY.PLS_PLANO_SERVICO_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSPLSR_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PLANO_SCA_RESTRICAO TO NIVEL_1;

