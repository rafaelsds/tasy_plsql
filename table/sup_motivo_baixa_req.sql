ALTER TABLE TASY.SUP_MOTIVO_BAIXA_REQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_MOTIVO_BAIXA_REQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_MOTIVO_BAIXA_REQ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MOTIVO_BAIXA      NUMBER(3)                NOT NULL,
  DS_MOTIVO_BAIXA      VARCHAR2(80 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_REQ_CONJUNTO      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUMOBAR_PK ON TASY.SUP_MOTIVO_BAIXA_REQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUP_MOTIVO_BAIXA_REQ_DELETE
BEFORE DELETE ON TASY.SUP_MOTIVO_BAIXA_REQ FOR EACH ROW
DECLARE

begin

if	(:old.nr_sequencia in (0,1,2,3,4,5,6)) then
	wheb_mensagem_pck.exibir_mensagem_abort(266291);
	--'N�o � permitido excluir esse motivo de baixa, pois ele � padr�o do Tasy!');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.sup_motivo_baixa_req_update
before update ON TASY.SUP_MOTIVO_BAIXA_REQ for each row
declare

begin

if (:new.nr_sequencia = 0 and :new.cd_motivo_baixa <> 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(228784);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.SUP_MOTIVO_BAIXA_REQ_tp  after update ON TASY.SUP_MOTIVO_BAIXA_REQ FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SUP_MOTIVO_BAIXA_REQ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REQ_CONJUNTO,1,4000),substr(:new.IE_REQ_CONJUNTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REQ_CONJUNTO',ie_log_w,ds_w,'SUP_MOTIVO_BAIXA_REQ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MOTIVO_BAIXA,1,4000),substr(:new.DS_MOTIVO_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'DS_MOTIVO_BAIXA',ie_log_w,ds_w,'SUP_MOTIVO_BAIXA_REQ',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOTIVO_BAIXA,1,4000),substr(:new.CD_MOTIVO_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_BAIXA',ie_log_w,ds_w,'SUP_MOTIVO_BAIXA_REQ',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_MOTIVO_BAIXA_REQ ADD (
  CONSTRAINT SUMOBAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SUP_MOTIVO_BAIXA_REQ TO NIVEL_1;

