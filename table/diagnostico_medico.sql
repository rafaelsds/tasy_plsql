ALTER TABLE TASY.DIAGNOSTICO_MEDICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIAGNOSTICO_MEDICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIAGNOSTICO_MEDICO
(
  NR_ATENDIMENTO       NUMBER(10)               NOT NULL,
  DT_DIAGNOSTICO       DATE                     NOT NULL,
  IE_TIPO_DIAGNOSTICO  NUMBER(3)                NOT NULL,
  CD_MEDICO            VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_DIAGNOSTICO       VARCHAR2(2000 BYTE),
  IE_TIPO_DOENCA       VARCHAR2(2 BYTE),
  IE_UNIDADE_TEMPO     VARCHAR2(2 BYTE),
  QT_TEMPO             NUMBER(15),
  IE_TIPO_ATENDIMENTO  NUMBER(3),
  IE_NIVEL_ATENCAO     VARCHAR2(1 BYTE),
  DS_UTC               VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO     VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO   VARCHAR2(50 BYTE),
  NR_RQE               VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          20M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIAMEDI_ATEPACI_FK_I ON TASY.DIAGNOSTICO_MEDICO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIAMEDI_PESFISI_FK_I ON TASY.DIAGNOSTICO_MEDICO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIAMEDI_PK ON TASY.DIAGNOSTICO_MEDICO
(NR_ATENDIMENTO, DT_DIAGNOSTICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.DIAGNOSTICO_MEDICO_tp  after update ON TASY.DIAGNOSTICO_MEDICO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_ATENDIMENTO='||to_char(:old.NR_ATENDIMENTO)||'#@#@DT_DIAGNOSTICO='||to_char(:old.DT_DIAGNOSTICO);gravar_log_alteracao(substr(:old.NR_RQE,1,4000),substr(:new.NR_RQE,1,4000),:new.nm_usuario,nr_seq_w,'NR_RQE',ie_log_w,ds_w,'DIAGNOSTICO_MEDICO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.diagnostico_medico_log_atual
after update ON TASY.DIAGNOSTICO_MEDICO for each row
declare
nr_sequencia_new_w   			diagnostico_doenca_hist.nr_sequencia%type;
cd_pessoa_fisica_atend_w		atendimento_paciente.cd_pessoa_fisica%type;
cd_doenca_w						diagnostico_doenca.cd_doenca%type;
ie_status_w						diagnostico_doenca_hist.ie_status%type;

begin

if 	(:new.ie_tipo_diagnostico <> :old.ie_tipo_diagnostico) then
	begin
	select nvl(diagnostico_doenca_hist_seq.nextval, 1)
	into nr_sequencia_new_w
	from dual;

	select cd_pessoa_fisica
	into cd_pessoa_fisica_atend_w
	from atendimento_paciente
	where nr_atendimento = :new.nr_atendimento;

	select nvl(max(cd_doenca), '0')
	into cd_doenca_w
	from diagnostico_doenca
	where dt_diagnostico = :new.dt_diagnostico
	and nr_atendimento = :new.nr_atendimento;

	if (cd_doenca_w <> '0') then
		begin

		if :new.ie_tipo_diagnostico = 1 then
			ie_status_w := obter_diagnostico_status('PR');
		else
			ie_status_w := obter_diagnostico_status('D');
		end if;

		insert into diagnostico_doenca_hist(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			ie_status,
			ds_observacao,
			cd_pessoa_fisica,
			cd_responsavel,
			cd_doenca,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_diagnostico,
			nr_atendimento)
		values (
			nr_sequencia_new_w,
			sysdate,
			:new.nm_usuario,
			ie_status_w,
			:new.ds_diagnostico,
			cd_pessoa_fisica_atend_w,
			:new.cd_medico,
			cd_doenca_w,
			sysdate,
			:new.nm_usuario,
			:new.dt_diagnostico,
			:new.nr_atendimento);
		end;
	end if;
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.diagnostico_medico_atual
before insert or update ON TASY.DIAGNOSTICO_MEDICO 
for each row
declare
ie_tipo_atendimento_w	number(3,0);
qt_reg_w	number(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(:old.DT_DIAGNOSTICO,sysdate+10) <> :new.DT_DIAGNOSTICO) and
	(:new.DT_DIAGNOSTICO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_DIAGNOSTICO, 'HV');	
end if;

if	(:new.ie_tipo_atendimento is null) then

	/* obter tipo atendimento atual atend */
	select	MAX(ie_tipo_atendimento)
	into	ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;
	
	/* gerar tipo atendimento diag */
	:new.ie_tipo_atendimento := ie_tipo_atendimento_w;
	
end if;
<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.DIAGNOSTICO_MEDICO ADD (
  CONSTRAINT DIAMEDI_PK
 PRIMARY KEY
 (NR_ATENDIMENTO, DT_DIAGNOSTICO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          12M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIAGNOSTICO_MEDICO ADD (
  CONSTRAINT DIAMEDI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT DIAMEDI_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.DIAGNOSTICO_MEDICO TO NIVEL_1;

