ALTER TABLE TASY.PLS_LOTE_SIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_SIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_SIB
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_MESANO_REFERENCIA     DATE,
  DT_ENVIO                 DATE,
  IE_STATUS                VARCHAR2(1 BYTE),
  DT_GERACAO               DATE,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_CONSISTENCIA          DATE,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_MES_COMPETENCIA       DATE,
  IE_TIPO_ENVIO            VARCHAR2(1 BYTE)     NOT NULL,
  NM_ARQUIVO               VARCHAR2(15 BYTE),
  IE_RESUMO_IMPORTADO      VARCHAR2(2 BYTE),
  IE_ERRO_IMPORTADO        VARCHAR2(2 BYTE),
  IE_CCO_IMPORTADO         VARCHAR2(2 BYTE),
  IE_CCO_ATUALIZADO        VARCHAR2(2 BYTE),
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  NR_SEQ_LOTE_ENVIO        NUMBER(10),
  IE_AJUSTE_CONFERENCIA    VARCHAR2(1 BYTE),
  DT_GERACAO_ARQUIVO       DATE,
  NR_SEQ_LOTE_CONFERENCIA  NUMBER(10),
  IE_FLEXIBILIZADO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLOSIB_ESTABEL_FK_I ON TASY.PLS_LOTE_SIB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSLOSIB_PK ON TASY.PLS_LOTE_SIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOSIB_PLSLOSIB_FK_I ON TASY.PLS_LOTE_SIB
(NR_SEQ_LOTE_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOSIB_PLSLRSI_FK_I ON TASY.PLS_LOTE_SIB
(NR_SEQ_LOTE_CONFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOSIB_PLSLRSI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_LOTE_SIB ADD (
  CONSTRAINT PLSLOSIB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_SIB ADD (
  CONSTRAINT PLSLOSIB_PLSLRSI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_CONFERENCIA) 
 REFERENCES TASY.PLS_LOTE_RETORNO_SIB (NR_SEQUENCIA),
  CONSTRAINT PLSLOSIB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSLOSIB_PLSLOSIB_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ENVIO) 
 REFERENCES TASY.PLS_LOTE_SIB (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_SIB TO NIVEL_1;

