ALTER TABLE TASY.CONVERSAO_UNIDADE_MEDIDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVERSAO_UNIDADE_MEDIDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVERSAO_UNIDADE_MEDIDA
(
  CD_CONVENIO          NUMBER(5)                NOT NULL,
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE)        NOT NULL,
  CD_UNIDADE_CONVENIO  VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONUNMED_CONVENI_FK_I ON TASY.CONVERSAO_UNIDADE_MEDIDA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONUNMED_PK ON TASY.CONVERSAO_UNIDADE_MEDIDA
(CD_CONVENIO, CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONUNMED_UNIMEDI_FK_I ON TASY.CONVERSAO_UNIDADE_MEDIDA
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONUNMED_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CONVERSAO_UNIDADE_MEDIDA_tp  after update ON TASY.CONVERSAO_UNIDADE_MEDIDA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_CONVENIO='||to_char(:old.CD_CONVENIO)||'#@#@CD_UNIDADE_MEDIDA='||to_char(:old.CD_UNIDADE_MEDIDA);gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'CONVERSAO_UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA,1,4000),substr(:new.CD_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA',ie_log_w,ds_w,'CONVERSAO_UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_CONVENIO,1,4000),substr(:new.CD_UNIDADE_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_CONVENIO',ie_log_w,ds_w,'CONVERSAO_UNIDADE_MEDIDA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CONVERSAO_UNIDADE_MEDIDA ADD (
  CONSTRAINT CONUNMED_PK
 PRIMARY KEY
 (CD_CONVENIO, CD_UNIDADE_MEDIDA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVERSAO_UNIDADE_MEDIDA ADD (
  CONSTRAINT CONUNMED_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT CONUNMED_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CONVERSAO_UNIDADE_MEDIDA TO NIVEL_1;

