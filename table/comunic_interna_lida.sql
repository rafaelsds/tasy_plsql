ALTER TABLE TASY.COMUNIC_INTERNA_LIDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMUNIC_INTERNA_LIDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMUNIC_INTERNA_LIDA
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          14M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.COMINLI_PK ON TASY.COMUNIC_INTERNA_LIDA
(NR_SEQUENCIA, NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          21M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Comunic_Interna_Lida_insert
after insert ON TASY.COMUNIC_INTERNA_LIDA for each row
declare
nr_seq_ev_pac_dest_w		number(10);
nr_seq_ev_pac_w			number(10);
ie_alterar_status_lido_w		varchar2(1);
qt_evento_pac_w			number(10);
qt_evento_pac_ci_lido_w		number(10);

begin

--Obter_Param_Usuario(7032,7,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_alterar_status_lido_w);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

	select	nvl(max(nr_seq_ev_pac_dest),0)
	into	nr_seq_ev_pac_dest_w
	from	comunic_interna
	where	nr_sequencia = :new.nr_sequencia;

	if (nr_seq_ev_pac_dest_w > 0) then

		select	max(nr_seq_ev_pac)
		into	nr_seq_ev_pac_w
		from	ev_evento_pac_destino
		where 	nr_sequencia = nr_seq_ev_pac_dest_w;

		update	ev_evento_pac_destino
		set	ie_status 	= 'L',
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= sysdate
		where	nr_sequencia	= nr_seq_ev_pac_dest_w;

		select 	count(*)
		into	qt_evento_pac_w
		from	ev_evento_pac_destino
		where	nr_seq_ev_pac = nr_seq_ev_pac_w;

		select 	count(*)
		into	qt_evento_pac_ci_lido_w
		from	ev_evento_pac_destino
		where	nr_seq_ev_pac = nr_seq_ev_pac_w
		and	ie_status = 'L'
		and	ie_forma_ev = 3;

		if (qt_evento_pac_w = qt_evento_pac_ci_lido_w) then
			update 	ev_evento_paciente
			set	ie_status = 'L'
			where	nr_sequencia = nr_seq_ev_pac_w;
		end if;
	end if;

end if;
end;
/


ALTER TABLE TASY.COMUNIC_INTERNA_LIDA ADD (
  CONSTRAINT COMINLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NM_USUARIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          21M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COMUNIC_INTERNA_LIDA ADD (
  CONSTRAINT COMINLI_COMINTE_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.COMUNIC_INTERNA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.COMUNIC_INTERNA_LIDA TO NIVEL_1;

