ALTER TABLE TASY.W_SUS_INTERF_LAUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_SUS_INTERF_LAUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_SUS_INTERF_LAUDO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_EXPORTACAO      NUMBER(10),
  CD_CID_PRINCIPAL       VARCHAR2(4 BYTE),
  DT_LAUDO               DATE,
  NR_PRONTUARIO          NUMBER(10),
  IE_TIPO_SOLIC          VARCHAR2(1 BYTE),
  CD_CARATER_INTERNACAO  VARCHAR2(2 BYTE),
  IE_CLINICA             NUMBER(5),
  CD_PROCEDIMENTO_SOLIC  NUMBER(15),
  CD_CNES                NUMBER(7),
  NR_CPF_SOLIC           VARCHAR2(11 BYTE),
  DT_SOLICITACAO         DATE,
  DS_MOTIVO_SOLIC        VARCHAR2(200 BYTE),
  NR_SEQ_PACIENTE        NUMBER(10),
  QT_PROCEDIMENTO        NUMBER(9,3),
  NR_LAUDO               NUMBER(10),
  NR_CPF_PACIENTE        VARCHAR2(11 BYTE),
  NR_CNS_PACIENTE        NUMBER(15),
  NM_PACIENTE            VARCHAR2(60 BYTE),
  IE_SEXO                VARCHAR2(1 BYTE),
  DT_NASC_PACIENTE       DATE,
  NM_MAE_PACIENTE        VARCHAR2(60 BYTE),
  DS_LOGRADOURO          VARCHAR2(40 BYTE),
  NR_LOGRADOURO          NUMBER(10),
  DS_COMPL_LOGRADOURO    VARCHAR2(25 BYTE),
  DS_BAIRRO              VARCHAR2(30 BYTE),
  CD_CEP                 VARCHAR2(15 BYTE),
  CD_MUN_IBGE            VARCHAR2(7 BYTE),
  CD_COR_PELE            NUMBER(2),
  NM_RESP_PACIENTE       VARCHAR2(60 BYTE),
  NR_CNS_SOLIC           NUMBER(15),
  NM_MEDICO_SOLIC        VARCHAR2(60 BYTE),
  NR_SEQ_LOTE            NUMBER(10),
  NR_CPF_RESP            VARCHAR2(11 BYTE),
  NR_CNS_RESP            NUMBER(15),
  NM_MEDICO_RESP         VARCHAR2(60 BYTE),
  CD_ETNIA               VARCHAR2(4 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WSUSINL_PK ON TASY.W_SUS_INTERF_LAUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WSUSINL_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_SUS_INTERF_LAUDO ADD (
  CONSTRAINT WSUSINL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_SUS_INTERF_LAUDO TO NIVEL_1;

