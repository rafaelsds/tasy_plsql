ALTER TABLE TASY.TITULO_PAGAR_ALT_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_ALT_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_ALT_VALOR
(
  NR_TITULO           NUMBER(10)                NOT NULL,
  NR_SEQUENCIA        NUMBER(5)                 NOT NULL,
  DT_ALTERACAO        DATE                      NOT NULL,
  VL_ANTERIOR         NUMBER(15,2)              NOT NULL,
  VL_ALTERACAO        NUMBER(15,2)              NOT NULL,
  CD_MOEDA            NUMBER(5)                 NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  NR_LOTE_CONTABIL    NUMBER(10),
  DS_OBSERVACAO       VARCHAR2(255 BYTE),
  NR_SEQ_TRANS_FIN    NUMBER(10),
  NR_EXTERNO          VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO       NUMBER(10),
  DS_STACK            VARCHAR2(4000 BYTE),
  NR_CODIGO_CONTROLE  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITPAAV_LOTCONT_FK_I ON TASY.TITULO_PAGAR_ALT_VALOR
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAAV_MOEDA_FK_I ON TASY.TITULO_PAGAR_ALT_VALOR
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAAV_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPAAV_MOTALVA_FK_I ON TASY.TITULO_PAGAR_ALT_VALOR
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAAV_MOTALVA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TITPAAV_PK ON TASY.TITULO_PAGAR_ALT_VALOR
(NR_TITULO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAAV_TITPAGA_FK_I ON TASY.TITULO_PAGAR_ALT_VALOR
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPAAV_TRAFINA_FK_I ON TASY.TITULO_PAGAR_ALT_VALOR
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPAAV_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_alt_valor_after
after insert ON TASY.TITULO_PAGAR_ALT_VALOR for each row
declare

vl_movimento_w		ctb_documento.vl_movimento%type;
cd_estabelecimento_w	ctb_documento.cd_estabelecimento%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
vl_movimento_w:= nvl(:new.vl_alteracao,0) - nvl(:new.vl_anterior,0);

if	(nvl(vl_movimento_w,0) <> 0) then
	begin

	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	titulo_pagar
	where	nr_titulo = :new.nr_titulo;

	ctb_concil_financeira_pck.ctb_gravar_documento	(	cd_estabelecimento_w,
							:new.dt_alteracao,
							7,
							:new.nr_seq_trans_fin,
							15,
							:new.nr_titulo,
							:new.nr_sequencia,
							null,
							vl_movimento_w,
							'TITULO_PAGAR_ALT_VALOR',
							'VL_ALTERADO',
							:new.nm_usuario);
	end;
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TITULO_PAGAR_ALT_VALOR_INSERT
BEFORE INSERT ON TASY.TITULO_PAGAR_ALT_VALOR FOR EACH ROW
DECLARE

cd_estabelecimento_w		number(5);
ie_altera_valor_tit_escrit_w	varchar2(1);
qt_registro_w			number(5);
ie_tipo_titulo_cpa_w		varchar2(2);
ie_processo_camara_w		pls_parametros_camara.ie_processo_camara%type;
nr_titulo_pagar_w			pls_titulo_lote_camara.nr_titulo_pagar%type;
nr_seq_lote_w				pls_titulo_lote_camara.nr_sequencia%type;
ie_baixa_camara_compensacao_w	number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	max(cd_estabelecimento),
	max(ie_tipo_titulo)
into	cd_estabelecimento_w,
	ie_tipo_titulo_cpa_w
from	titulo_pagar
where	nr_titulo	= :new.nr_titulo;

if	(fin_obter_se_mes_aberto(cd_estabelecimento_w, :new.dt_alteracao,'CP',ie_tipo_titulo_cpa_w,null,null,null) = 'N') then
	/* O mes/dia financeiro de emissao do titulo ja esta fechado! Nao e possivel incluir novos titulos neste mes/dia. */
	wheb_mensagem_pck.exibir_mensagem_abort(262134);
end if;

/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_alteracao);

/* nao deixar salvar se tem registro na titulo_pagar_escrit vinculado ao titulo */

select	nvl(max(ie_altera_valor_tit_escrit),'S')
into	ie_altera_valor_tit_escrit_w
from	parametros_contas_pagar
where	cd_estabelecimento	= cd_estabelecimento_w;

if	(ie_altera_valor_tit_escrit_w = 'N') then

	select	count(*)
	into	qt_registro_w
	from	titulo_pagar_escrit
	where	nr_titulo	= :new.nr_titulo;

	if	(qt_registro_w > 0) then
		/* Titulo ja vinculado a uma remessa de pagamento escritural.
		O mesmo so pode ser baixado pela funcao "Pagamento escritural".
		Titulo: :new.nr_titulo */
		wheb_mensagem_pck.exibir_mensagem_abort(262125,'NR_TITULO_W='||:new.nr_titulo);
	end if;
end if;

if (cd_estabelecimento_w is not null) then

	/*verificar se o titulo que esta recebendo a baixa esta em um lote de camra de compensacao que nao esta baixado.*/
	select	count(a.nr_titulo_pagar),
			max(b.nr_sequencia)
	into	nr_titulo_pagar_w,
			nr_seq_lote_w
	from	pls_titulo_lote_camara a,
			pls_lote_camara_comp b
	where	a.nr_seq_lote_camara	= b.nr_sequencia
	and		a.nr_titulo_pagar		= :new.nr_titulo;

	select	max(a.ie_processo_camara)
	into	ie_processo_camara_w
	from	pls_parametros_camara a
	where 	cd_estabelecimento = cd_estabelecimento_w;

	if	(nvl(ie_processo_camara_w,'CO') = 'CA') and (nr_titulo_pagar_w > 0) then /*se  o processo camara de compensacao for regime de caixa e o titulo estiver na camara*/

			wheb_mensagem_pck.exibir_mensagem_abort(335779,	'NR_SEQ_LOTE_W=' || nr_seq_lote_w ||
									';NR_TITULO_W=' || :new.nr_titulo);
	end if;
end if;

:new.DS_STACK := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TITULO_PAGAR_ALT_VALOR_atual
before insert or update ON TASY.TITULO_PAGAR_ALT_VALOR for each row
declare

ie_tipo_alteracao_w	varchar2(1);
ds_motivo_w		varchar2(255);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:new.NR_SEQ_MOTIVO is not null) then
	begin
	select	nvl(ie_tipo_alteracao,'T'),
		ds_motivo
	into	ie_tipo_alteracao_w,
		ds_motivo_w
	from	motivo_alt_valor
	where	nr_sequencia = :new.NR_SEQ_MOTIVO;

	if	(ie_tipo_alteracao_w = 'A') and
		(:new.VL_ALTERACAO < :new.VL_ANTERIOR) then
		wheb_mensagem_pck.exibir_mensagem_abort(207613,'ds_motivo='||ds_motivo_w);
	elsif	(ie_tipo_alteracao_w = 'D') and
		(:new.VL_ALTERACAO > :new.VL_ANTERIOR) then
		wheb_mensagem_pck.exibir_mensagem_abort(207612,'ds_motivo='||ds_motivo_w);
	end if;
	end;
end if;

end if;

end;
/


ALTER TABLE TASY.TITULO_PAGAR_ALT_VALOR ADD (
  CONSTRAINT TITPAAV_PK
 PRIMARY KEY
 (NR_TITULO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_ALT_VALOR ADD (
  CONSTRAINT TITPAAV_MOTALVA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_ALT_VALOR (NR_SEQUENCIA),
  CONSTRAINT TITPAAV_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITPAAV_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT TITPAAV_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO)
    ON DELETE CASCADE,
  CONSTRAINT TITPAAV_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TITULO_PAGAR_ALT_VALOR TO NIVEL_1;

