ALTER TABLE TASY.REGRA_NUMERACAO_DECLARACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_NUMERACAO_DECLARACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_NUMERACAO_DECLARACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_TIPO_NUMERACAO    VARCHAR2(3 BYTE)         NOT NULL,
  NR_INICIAL           NUMBER(10)               NOT NULL,
  NR_FINAL             NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RENUDEC_I1 ON TASY.REGRA_NUMERACAO_DECLARACAO
(IE_TIPO_NUMERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RENUDEC_PK ON TASY.REGRA_NUMERACAO_DECLARACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_NUMERACAO_DECLA_AftIns
after insert ON TASY.REGRA_NUMERACAO_DECLARACAO for each row
declare
nr_inicial_w	number(10);
NR_FINAL_w	number(10);
begin
if	(:new.NR_INICIAL	> :new.NR_FINAL) then
	--  A numera��o inicial est� maior que a numera��o final.
	Wheb_mensagem_pck.exibir_mensagem_abort(264549);
end if;

nr_inicial_w	:= :new.NR_INICIAL;
NR_FINAL_w	:= :new.NR_FINAL;

for i in nr_inicial_w..NR_FINAL_w loop
	begin

	insert into REGRA_NUMERACAO_DEC_ITEM	(	nr_sequencia,
							dt_atualizacao,
							nm_usuario,
							dt_atualizacao_nrec,
							nm_usuario_nrec,
							nr_declaracao,
							ie_disponivel,
							nr_seq_regra_num)
				values		(	REGRA_NUMERACAO_DEC_ITEM_seq.nextval,
							sysdate,
							:new.nm_usuario,
							sysdate,
							:new.nm_usuario,
							i,
							'S',
							:new.nr_sequencia);

	end;
end loop;

end;
/


CREATE OR REPLACE TRIGGER TASY.REGRA_NUMERACAO_DECLA_BEFINS
before insert or update ON TASY.REGRA_NUMERACAO_DECLARACAO for each row
declare
retorno_w number (10);

PRAGMA AUTONOMOUS_TRANSACTION;

begin

select count(*)
into retorno_w
from REGRA_NUMERACAO_DEC_ITEM x
where NR_DECLARACAO >= :new.NR_INICIAL
and NR_DECLARACAO <= :new.NR_FINAL
and (SELECT IE_TIPO_NUMERACAO
		FROM REGRA_NUMERACAO_DEC_ITEM R,
			REGRA_NUMERACAO_DECLARACAO RN
		WHERE R.NR_SEQ_REGRA_NUM = RN.NR_SEQUENCIA
        AND R.NR_SEQUENCIA = X.NR_SEQUENCIA ) = :NEW.IE_TIPO_NUMERACAO;

if (retorno_w > 0) then
	Wheb_mensagem_pck.exibir_mensagem_abort(1092632);
end if;


end;
/


ALTER TABLE TASY.REGRA_NUMERACAO_DECLARACAO ADD (
  CONSTRAINT RENUDEC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REGRA_NUMERACAO_DECLARACAO TO NIVEL_1;

