ALTER TABLE TASY.PLS_CP_CTA_ESERV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_ESERV CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_ESERV
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  TX_AJUSTE               NUMBER(15,4)          NOT NULL,
  NR_SEQ_CP_COMBINADA     NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_FIM_VIGENCIA         DATE,
  VL_NEGOCIADO            NUMBER(15,4),
  CD_TABELA_SERVICO       NUMBER(4)             NOT NULL,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  IE_AUTOGERADO           VARCHAR2(1 BYTE),
  TX_AJUSTE_PRECO         NUMBER(15,4),
  CD_MOEDA                NUMBER(3),
  IE_NAO_GERA_TX_INTER    VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLCPCTS_MOEDA_FK_I ON TASY.PLS_CP_CTA_ESERV
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLCPCTS_PK ON TASY.PLS_CP_CTA_ESERV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCTS_PLSCPCC_FK_I ON TASY.PLS_CP_CTA_ESERV
(NR_SEQ_CP_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_cp_cta_eserv_atual
before insert or update ON TASY.PLS_CP_CTA_ESERV for each row
declare

begin
-- previnir que seja gravada a hora junto na regra.
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;
if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_cp_cta_eserv_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_CP_CTA_ESERV_tp  after update ON TASY.PLS_CP_CTA_ESERV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_CP_COMBINADA,1,4000),substr(:new.NR_SEQ_CP_COMBINADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_COMBINADA',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE,1,4000),substr(:new.TX_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_NEGOCIADO,1,4000),substr(:new.VL_NEGOCIADO,1,4000),:new.nm_usuario,nr_seq_w,'VL_NEGOCIADO',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TABELA_SERVICO,1,4000),substr(:new.CD_TABELA_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TABELA_SERVICO',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PRECO,1,4000),substr(:new.TX_AJUSTE_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PRECO',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA,1,4000),substr(:new.CD_MOEDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA_REF,1,4000),substr(:new.DT_INICIO_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NAO_GERA_TX_INTER,1,4000),substr(:new.IE_NAO_GERA_TX_INTER,1,4000),:new.nm_usuario,nr_seq_w,'IE_NAO_GERA_TX_INTER',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA_REF,1,4000),substr(:new.DT_FIM_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTOGERADO,1,4000),substr(:new.IE_AUTOGERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTOGERADO',ie_log_w,ds_w,'PLS_CP_CTA_ESERV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CP_CTA_ESERV ADD (
  CONSTRAINT PLCPCTS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_ESERV ADD (
  CONSTRAINT PLCPCTS_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLCPCTS_PLSCPCC_FK 
 FOREIGN KEY (NR_SEQ_CP_COMBINADA) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CP_CTA_ESERV TO NIVEL_1;

