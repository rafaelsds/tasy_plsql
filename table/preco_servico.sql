ALTER TABLE TASY.PRECO_SERVICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRECO_SERVICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_SERVICO
(
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_TABELA_SERVICO    NUMBER(4)                NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  VL_SERVICO           NUMBER(16,5)             NOT NULL,
  CD_MOEDA             NUMBER(3)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10),
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INATIVACAO        DATE,
  DT_VIGENCIA_FINAL    DATE,
  DT_FIM_VIGENCIA_REF  DATE,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  CD_PROCEDIMENTO_LOC  VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRESERV_MOEDA_FK_I ON TASY.PRECO_SERVICO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESERV_MOEDA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRESERV_PK ON TASY.PRECO_SERVICO
(CD_ESTABELECIMENTO, CD_TABELA_SERVICO, CD_PROCEDIMENTO, DT_INICIO_VIGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESERV_PROCEDI_FK_I ON TASY.PRECO_SERVICO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESERV_TABSERV_FK_I ON TASY.PRECO_SERVICO
(CD_ESTABELECIMENTO, CD_TABELA_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESERV_UNIMEDI_FK_I ON TASY.PRECO_SERVICO
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESERV_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.preco_servico_atual
before insert or update ON TASY.PRECO_SERVICO for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela o campo fim ref
-- � alimentado com o campo fim ou se o mesmo for nulo � alimentado com a data 31/12/3000 desta forma podemos
-- utilizar um between ou fazer uma compara��o com estes campos sem precisar se preocupar se o campo vai estar nulo

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_vigencia_final, 'F');

end preco_servico_atual;
/


CREATE OR REPLACE TRIGGER TASY.Preco_Servico_Delete
before delete ON TASY.PRECO_SERVICO FOR EACH ROW
DECLARE

ds_log_w	varchar2(2000);

begin


ds_log_w := substr(	obter_desc_expressao(339327)/*' Fun��o ativa : '*/||': '|| obter_funcao_ativa || chr(13) ||chr(10)||
					' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);

END;
/


CREATE OR REPLACE TRIGGER TASY.PRECO_SERVICO_tp  after update ON TASY.PRECO_SERVICO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_ESTABELECIMENTO='||to_char(:old.CD_ESTABELECIMENTO)||'#@#@CD_TABELA_SERVICO='||to_char(:old.CD_TABELA_SERVICO)||'#@#@CD_PROCEDIMENTO='||to_char(:old.CD_PROCEDIMENTO)||'#@#@DT_INICIO_VIGENCIA='||to_char(:old.DT_INICIO_VIGENCIA); ds_w:=substr(:new.CD_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PRECO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PRECO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_SERVICO,1,4000),substr(:new.VL_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'VL_SERVICO',ie_log_w,ds_w,'PRECO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA,1,4000),substr(:new.CD_MOEDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA',ie_log_w,ds_w,'PRECO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PRECO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA,1,4000),substr(:new.CD_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA',ie_log_w,ds_w,'PRECO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INATIVACAO,1,4000),substr(:new.DT_INATIVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_INATIVACAO',ie_log_w,ds_w,'PRECO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VIGENCIA_FINAL,1,4000),substr(:new.DT_VIGENCIA_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'DT_VIGENCIA_FINAL',ie_log_w,ds_w,'PRECO_SERVICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TABELA_SERVICO,1,4000),substr(:new.CD_TABELA_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TABELA_SERVICO',ie_log_w,ds_w,'PRECO_SERVICO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PRECO_SERVICO ADD (
  CONSTRAINT PRESERV_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO, CD_TABELA_SERVICO, CD_PROCEDIMENTO, DT_INICIO_VIGENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRECO_SERVICO ADD (
  CONSTRAINT PRESERV_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PRESERV_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PRESERV_TABSERV_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_TABELA_SERVICO) 
 REFERENCES TASY.TABELA_SERVICO (CD_ESTABELECIMENTO,CD_TABELA_SERVICO),
  CONSTRAINT PRESERV_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.PRECO_SERVICO TO NIVEL_1;

