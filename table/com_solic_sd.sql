ALTER TABLE TASY.COM_SOLIC_SD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_SOLIC_SD CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_SOLIC_SD
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PF_SOLIC            VARCHAR2(10 BYTE)      NOT NULL,
  NR_SEQ_TIPO_AVALIACAO  NUMBER(10)             NOT NULL,
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  NR_SEQ_CLIENTE         NUMBER(10),
  DT_SOLICITACAO         DATE                   NOT NULL,
  DT_SUGERIDA            DATE                   NOT NULL,
  DT_SUGERIDA_POS        DATE,
  HR_INICIAL             DATE                   NOT NULL,
  HR_FINAL               DATE                   NOT NULL,
  NR_SEQ_TIPO_DEM        NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO_ABORDAGEM  NUMBER(10)             NOT NULL,
  CD_ESPECIALIDADE       NUMBER(5)              NOT NULL,
  IE_STATUS              VARCHAR2(3 BYTE)       NOT NULL,
  IE_PARTICULAR          VARCHAR2(1 BYTE),
  IE_CONVENIO            VARCHAR2(1 BYTE),
  IE_SUS                 VARCHAR2(1 BYTE),
  NR_ORDEM_SERVICO       NUMBER(10),
  DS_LOCAL_DEMONSTRACAO  VARCHAR2(255 BYTE),
  DT_CANCELAMENTO        DATE,
  NM_USUARIO_CANCEL      VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_CANCEL   NUMBER(10),
  DS_JUSTIF_CANCEL       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMSOLSD_COMCLIE_FK_I ON TASY.COM_SOLIC_SD
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMSOLSD_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMSOLSD_COMMOCAN_FK_I ON TASY.COM_SOLIC_SD
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMSOLSD_COMTIABOR_FK_I ON TASY.COM_SOLIC_SD
(NR_SEQ_TIPO_ABORDAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMSOLSD_COMTIDEM_FK_I ON TASY.COM_SOLIC_SD
(NR_SEQ_TIPO_DEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMSOLSD_ESPMEDI_FK_I ON TASY.COM_SOLIC_SD
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMSOLSD_ESTABEL_FK_I ON TASY.COM_SOLIC_SD
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMSOLSD_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMSOLSD_MANORSE_FK_I ON TASY.COM_SOLIC_SD
(NR_ORDEM_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMSOLSD_MEDTIAV_FK_I ON TASY.COM_SOLIC_SD
(NR_SEQ_TIPO_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMSOLSD_MEDTIAV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMSOLSD_PESFISI_FK2_I ON TASY.COM_SOLIC_SD
(CD_PF_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMSOLSD_PK ON TASY.COM_SOLIC_SD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMSOLSD_PK
  MONITORING USAGE;


ALTER TABLE TASY.COM_SOLIC_SD ADD (
  CONSTRAINT COMSOLSD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_SOLIC_SD ADD (
  CONSTRAINT COMSOLSD_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT COMSOLSD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT COMSOLSD_MEDTIAV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_AVALIACAO) 
 REFERENCES TASY.MED_TIPO_AVALIACAO (NR_SEQUENCIA),
  CONSTRAINT COMSOLSD_PESFISI_FK2 
 FOREIGN KEY (CD_PF_SOLIC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT COMSOLSD_COMMOCAN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.COM_MOTIVO_CANCEL_SD (NR_SEQUENCIA),
  CONSTRAINT COMSOLSD_COMTIABOR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ABORDAGEM) 
 REFERENCES TASY.COM_TIPO_ABORDAGEM (NR_SEQUENCIA),
  CONSTRAINT COMSOLSD_COMTIDEM_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DEM) 
 REFERENCES TASY.COM_TIPO_DEMONSTRACAO (NR_SEQUENCIA),
  CONSTRAINT COMSOLSD_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT COMSOLSD_MANORSE_FK 
 FOREIGN KEY (NR_ORDEM_SERVICO) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.COM_SOLIC_SD TO NIVEL_1;

