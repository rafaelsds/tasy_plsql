ALTER TABLE TASY.PLS_CADASTRO_REGRA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CADASTRO_REGRA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CADASTRO_REGRA_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ITEM              VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_MODULO        NUMBER(10)               NOT NULL,
  NR_SEQ_SUPERIOR      NUMBER(10),
  NR_SEQ_APRES         NUMBER(10),
  IE_LIBERACAO_PERFIL  VARCHAR2(1 BYTE),
  IE_UTILIZACAO        VARCHAR2(2 BYTE),
  NR_SEQ_PAINEL        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCRPE_MODTASY_FK_I ON TASY.PLS_CADASTRO_REGRA_ITEM
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCRPE_PK ON TASY.PLS_CADASTRO_REGRA_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCRPE_PLSCRPE_FK_I ON TASY.PLS_CADASTRO_REGRA_ITEM
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CADASTRO_REGRA_ITEM_tp  after update ON TASY.PLS_CADASTRO_REGRA_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'PLS_CADASTRO_REGRA_ITEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PLS_CADASTRO_REGRA_ITEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_SUPERIOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_SUPERIOR,1,4000),substr(:new.NR_SEQ_SUPERIOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SUPERIOR',ie_log_w,ds_w,'PLS_CADASTRO_REGRA_ITEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ITEM,1,500);gravar_log_alteracao(substr(:old.DS_ITEM,1,4000),substr(:new.DS_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_ITEM',ie_log_w,ds_w,'PLS_CADASTRO_REGRA_ITEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_LIBERACAO_PERFIL,1,500);gravar_log_alteracao(substr(:old.IE_LIBERACAO_PERFIL,1,4000),substr(:new.IE_LIBERACAO_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIBERACAO_PERFIL',ie_log_w,ds_w,'PLS_CADASTRO_REGRA_ITEM',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_NREC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'PLS_CADASTRO_REGRA_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CADASTRO_REGRA_ITEM ADD (
  CONSTRAINT PLSCRPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CADASTRO_REGRA_ITEM ADD (
  CONSTRAINT PLSCRPE_MODTASY_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.MODULO_TASY (NR_SEQUENCIA),
  CONSTRAINT PLSCRPE_PLSCRPE_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.PLS_CADASTRO_REGRA_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CADASTRO_REGRA_ITEM TO NIVEL_1;

