ALTER TABLE TASY.FATUR_ETAPA_ALTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FATUR_ETAPA_ALTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FATUR_ETAPA_ALTA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_CONVENIO           NUMBER(5),
  IE_TIPO_ATENDIMENTO   NUMBER(3),
  NR_SEQ_ETAPA          NUMBER(10)              NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  IE_EVENTO             VARCHAR2(2 BYTE)        NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  NR_SEQ_CLASSIFICACAO  NUMBER(10),
  IE_ULTIMA_CONTA       VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_PERFIL             NUMBER(5),
  PR_PREJUIZO           NUMBER(15,2),
  IE_STATUS             VARCHAR2(5 BYTE),
  CD_CATEGORIA          VARCHAR2(10 BYTE),
  NR_SEQ_ESTAGIO        NUMBER(10),
  NR_SEQ_ETAPA_FILTRO   NUMBER(10),
  NR_SEQ_MOTIVO_DEV     NUMBER(10),
  IE_TIPO_CONVENIO      NUMBER(2),
  CD_SETOR_ATEND_ETAPA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FATETAL_CATCONV_FK_I ON TASY.FATUR_ETAPA_ALTA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          616K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAL_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAL_CLAATEN_FK_I ON TASY.FATUR_ETAPA_ALTA
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAL_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAL_CONVENI_FK_I ON TASY.FATUR_ETAPA_ALTA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAL_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAL_CTAESPE_FK_I ON TASY.FATUR_ETAPA_ALTA
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAL_CTAESPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAL_ESTABEL_FK_I ON TASY.FATUR_ETAPA_ALTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAL_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAL_FATETAP_FK_I ON TASY.FATUR_ETAPA_ALTA
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FATETAL_FATMODE_FK_I ON TASY.FATUR_ETAPA_ALTA
(NR_SEQ_MOTIVO_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAL_FATMODE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FATETAL_PERFIL_FK_I ON TASY.FATUR_ETAPA_ALTA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAL_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FATETAL_PK ON TASY.FATUR_ETAPA_ALTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FATETAL_SETATEN_FK2_I ON TASY.FATUR_ETAPA_ALTA
(CD_SETOR_ATEND_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FATETAL_SETATEN_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.FATUR_ETAPA_ALTA_tp  after update ON TASY.FATUR_ETAPA_ALTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_CONVENIO,1,4000),substr(:new.IE_TIPO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONVENIO',ie_log_w,ds_w,'FATUR_ETAPA_ALTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.FATUR_ETAPA_ALTA ADD (
  CONSTRAINT FATETAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FATUR_ETAPA_ALTA ADD (
  CONSTRAINT FATETAL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT FATETAL_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT FATETAL_CTAESPE_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.CTA_ESTAGIO_PEND (NR_SEQUENCIA),
  CONSTRAINT FATETAL_FATMODE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEV) 
 REFERENCES TASY.FATUR_MOTIVO_DEVOL (NR_SEQUENCIA),
  CONSTRAINT FATETAL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT FATETAL_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT FATETAL_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT FATETAL_FATETAP_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.FATUR_ETAPA (NR_SEQUENCIA),
  CONSTRAINT FATETAL_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATEND_ETAPA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.FATUR_ETAPA_ALTA TO NIVEL_1;

