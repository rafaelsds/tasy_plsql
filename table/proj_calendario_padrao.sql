ALTER TABLE TASY.PROJ_CALENDARIO_PADRAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_CALENDARIO_PADRAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_CALENDARIO_PADRAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_DIA_SEMANA        VARCHAR2(1 BYTE)         NOT NULL,
  HR_INICIO            VARCHAR2(5 BYTE)         NOT NULL,
  HR_FINAL             VARCHAR2(5 BYTE)         NOT NULL,
  QT_MIN_INTERVALO     NUMBER(3)                NOT NULL,
  DT_HR_FINAL          DATE,
  NR_SEQ_CALENDARIO    NUMBER(10)               NOT NULL,
  DT_HR_INICIO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROJCALPD_CALPAD_FK_I ON TASY.PROJ_CALENDARIO_PADRAO
(NR_SEQ_CALENDARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROJCALPD_PK ON TASY.PROJ_CALENDARIO_PADRAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.proj_calendario_padrao_atual

before insert or update ON TASY.PROJ_CALENDARIO_PADRAO 
for each row
declare



begin

begin

	if (:new.hr_final is not null) and ((:new.hr_final <> :old.hr_final) or (:old.dt_hr_final is null)) then

		:new.dt_hr_final := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_final,'dd/mm/yyyy hh24:mi');

	end if;

	if (:new.hr_inicio is not null) and ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_hr_inicio is null)) then

		:new.dt_hr_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio,'dd/mm/yyyy hh24:mi');

	end if;

	if	(:new.dt_hr_final is not null ) and ((:new.dt_hr_final <> :old.dt_hr_final) or (:old.dt_hr_final is null)) then

		:new.hr_final := to_char(:new.dt_hr_final, 'hh24:mi');

	end if;

	if	(:new.dt_hr_inicio is not null ) and ((:new.dt_hr_inicio <> :old.dt_hr_inicio) or (:old.dt_hr_inicio is null)) then

		:new.hr_inicio := to_char(:new.dt_hr_inicio, 'hh24:mi');

	end if;

exception

	when others then

	null;

end;



end;
/


ALTER TABLE TASY.PROJ_CALENDARIO_PADRAO ADD (
  CONSTRAINT PROJCALPD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PROJ_CALENDARIO_PADRAO ADD (
  CONSTRAINT PROJCALPD_CALPAD_FK 
 FOREIGN KEY (NR_SEQ_CALENDARIO) 
 REFERENCES TASY.CALENDARIO_PADRAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROJ_CALENDARIO_PADRAO TO NIVEL_1;

