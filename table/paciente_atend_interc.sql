ALTER TABLE TASY.PACIENTE_ATEND_INTERC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_ATEND_INTERC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_ATEND_INTERC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_ATENDIMENTO         NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_INTERCORRENCIA          DATE               NOT NULL,
  DT_FIM_INTERCORRENCIA      DATE,
  DS_MOTIVO                  VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_INTERC         NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACATIN_PACATEN_FK_I ON TASY.PACIENTE_ATEND_INTERC
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACATIN_PK ON TASY.PACIENTE_ATEND_INTERC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATIN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PACATIN_TASASDI_FK_I ON TASY.PACIENTE_ATEND_INTERC
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATIN_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATIN_TASASDI_FK2_I ON TASY.PACIENTE_ATEND_INTERC
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATIN_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATIN_TINPAAT_FK_I ON TASY.PACIENTE_ATEND_INTERC
(NR_SEQ_TIPO_INTERC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATIN_TINPAAT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pac_atend_interc_pend_atual
after insert or update ON TASY.PACIENTE_ATEND_INTERC for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
ie_lib_interc_w			varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(ie_libera_intercorrencia)
into	ie_lib_interc_w
from	parametros_quimio;

select	max(nr_atendimento),
	max(obter_codigo_paciente_setor(nr_seq_paciente))
into	nr_atendimento_w,
	cd_pessoa_fisica_w
from	paciente_atendimento
where	nr_seq_atendimento	= :new.nr_seq_atendimento;

if	(nvl(ie_lib_interc_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'QLI';
	elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XQLI';
	end if;

	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PACIENTE_ATEND_INTERC ADD (
  CONSTRAINT PACATIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_ATEND_INTERC ADD (
  CONSTRAINT PACATIN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACATIN_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACATIN_PACATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO (NR_SEQ_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT PACATIN_TINPAAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_INTERC) 
 REFERENCES TASY.TIPO_INTERC_PAC_ATEND (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_ATEND_INTERC TO NIVEL_1;

