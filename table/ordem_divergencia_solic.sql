ALTER TABLE TASY.ORDEM_DIVERGENCIA_SOLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_DIVERGENCIA_SOLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_DIVERGENCIA_SOLIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ORDEM_COMPRA      NUMBER(10)               NOT NULL,
  NR_ITEM_OCI          NUMBER(5)                NOT NULL,
  IE_DIVERGENCIA       VARCHAR2(1 BYTE)         NOT NULL,
  DS_JUSTIFICATIVA     VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORDIVSO_ORCOITE_FK_I ON TASY.ORDEM_DIVERGENCIA_SOLIC
(NR_ORDEM_COMPRA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORDIVSO_ORDCOMP_FK_I ON TASY.ORDEM_DIVERGENCIA_SOLIC
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORDIVSO_ORDCOMP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ORDIVSO_PK ON TASY.ORDEM_DIVERGENCIA_SOLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ORDEM_DIVERGENCIA_SOLIC ADD (
  CONSTRAINT ORDIVSO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_DIVERGENCIA_SOLIC ADD (
  CONSTRAINT ORDIVSO_ORCOITE_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA, NR_ITEM_OCI) 
 REFERENCES TASY.ORDEM_COMPRA_ITEM (NR_ORDEM_COMPRA,NR_ITEM_OCI)
    ON DELETE CASCADE,
  CONSTRAINT ORDIVSO_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ORDEM_DIVERGENCIA_SOLIC TO NIVEL_1;

