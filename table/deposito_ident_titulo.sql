ALTER TABLE TASY.DEPOSITO_IDENT_TITULO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DEPOSITO_IDENT_TITULO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DEPOSITO_IDENT_TITULO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_DEPOSITO       NUMBER(10)              NOT NULL,
  NR_TITULO             NUMBER(10)              NOT NULL,
  VL_DEPOSITAR          NUMBER(15,2)            NOT NULL,
  VL_DEPOSITADO         NUMBER(15,2),
  VL_DEPOSITAR_ESTRANG  NUMBER(15,2),
  CD_MOEDA              NUMBER(5),
  VL_COTACAO            NUMBER(21,10),
  VL_COMPLEMENTO        NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DEPIDTI_DEPIDEN_FK_I ON TASY.DEPOSITO_IDENT_TITULO
(NR_SEQ_DEPOSITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DEPIDTI_DEPIDEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.DEPIDTI_MOEDA_FK_I ON TASY.DEPOSITO_IDENT_TITULO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DEPIDTI_PK ON TASY.DEPOSITO_IDENT_TITULO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DEPIDTI_PK
  MONITORING USAGE;


CREATE INDEX TASY.DEPIDTI_TITRECE_FK_I ON TASY.DEPOSITO_IDENT_TITULO
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DEPIDTI_TITRECE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DEPIDTI_UK ON TASY.DEPOSITO_IDENT_TITULO
(NR_SEQ_DEPOSITO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DEPIDTI_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.deposito_ident_titulo_befins
before insert ON TASY.DEPOSITO_IDENT_TITULO for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicionario [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ X ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare
nr_identificacao_w		varchar2(30);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	max(a.nr_identificacao)
into	nr_identificacao_w
from	deposito_identificado	a
where	a.nr_sequencia	= :new.nr_seq_deposito;

if	(nr_identificacao_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(225746);
end if;

end if;

end;
/


ALTER TABLE TASY.DEPOSITO_IDENT_TITULO ADD (
  CONSTRAINT DEPIDTI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT DEPIDTI_UK
 UNIQUE (NR_SEQ_DEPOSITO, NR_TITULO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DEPOSITO_IDENT_TITULO ADD (
  CONSTRAINT DEPIDTI_DEPIDEN_FK 
 FOREIGN KEY (NR_SEQ_DEPOSITO) 
 REFERENCES TASY.DEPOSITO_IDENTIFICADO (NR_SEQUENCIA),
  CONSTRAINT DEPIDTI_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT DEPIDTI_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA));

GRANT SELECT ON TASY.DEPOSITO_IDENT_TITULO TO NIVEL_1;

