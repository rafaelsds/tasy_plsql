ALTER TABLE TASY.PE_DIAGNOSTICO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_DIAGNOSTICO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_DIAGNOSTICO_PROC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DIAG          NUMBER(10)               NOT NULL,
  NR_SEQ_PROC          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_PROC         VARCHAR2(1 BYTE),
  IE_PERMITE_EXCLUSAO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEDIPRO_PEDIAGN_FK_I ON TASY.PE_DIAGNOSTICO_PROC
(NR_SEQ_DIAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEDIPRO_PEDIAGN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEDIPRO_PEPROCE_FK_I ON TASY.PE_DIAGNOSTICO_PROC
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEDIPRO_PEPROCE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEDIPRO_PK ON TASY.PE_DIAGNOSTICO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEDIPRO_UK ON TASY.PE_DIAGNOSTICO_PROC
(NR_SEQ_DIAG, NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEDIPRO_UK
  MONITORING USAGE;


ALTER TABLE TASY.PE_DIAGNOSTICO_PROC ADD (
  CONSTRAINT PEDIPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PEDIPRO_UK
 UNIQUE (NR_SEQ_DIAG, NR_SEQ_PROC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_DIAGNOSTICO_PROC ADD (
  CONSTRAINT PEDIPRO_PEDIAGN_FK 
 FOREIGN KEY (NR_SEQ_DIAG) 
 REFERENCES TASY.PE_DIAGNOSTICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEDIPRO_PEPROCE_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.PE_PROCEDIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PE_DIAGNOSTICO_PROC TO NIVEL_1;

