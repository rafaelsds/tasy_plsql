ALTER TABLE TASY.W_DIOPS_FIN_IDADESALDO_PAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DIOPS_FIN_IDADESALDO_PAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DIOPS_FIN_IDADESALDO_PAS
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_OPERADORA         NUMBER(10),
  NR_SEQ_TRANSACAO         NUMBER(10),
  NR_SEQ_PERIODO           NUMBER(10),
  VL_EVENTO_LIQUIDAR       NUMBER(15,2),
  VL_DEBITO_OPERADORA      NUMBER(15,2),
  VL_COMERCIALIZACAO       NUMBER(15,2),
  VL_DEBITO_OPER           NUMBER(15,2),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  IE_TIPO_VENCIMENTO       VARCHAR2(3 BYTE),
  VL_PREST_SERV_AS         NUMBER(15,2),
  VL_OUTROS_DEB_PAGAR      NUMBER(15,2),
  IE_IDADE_SALDO_PASSIVO   NUMBER(2),
  CD_CONTA_CONTABIL        VARCHAR2(20 BYTE),
  NR_TITULO                NUMBER(10),
  VL_DEB_BEN_CON_SEG_REC   NUMBER(15,2),
  VL_DEP_AQUIS_CARRE       NUMBER(15,2),
  NR_SEQ_CHEQUE_CP         NUMBER(10),
  NR_SEQ_MOVTO_BANCO_PEND  NUMBER(10),
  CD_ANS                   VARCHAR2(20 BYTE),
  VL_EVENTOS_SUS           NUMBER(15,2),
  VL_TITULOS_ENCARGOS      NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WDIFISP_I1 ON TASY.W_DIOPS_FIN_IDADESALDO_PAS
(NR_SEQ_PERIODO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WDIFISP_PK ON TASY.W_DIOPS_FIN_IDADESALDO_PAS
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDIFISP_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_DIOPS_FIN_IDADESALDO_PAS ADD (
  CONSTRAINT WDIFISP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_DIOPS_FIN_IDADESALDO_PAS TO NIVEL_1;

