ALTER TABLE TASY.PLS_INTERC_REGRA_AVISO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_INTERC_REGRA_AVISO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_INTERC_REGRA_AVISO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ACAO_INTERC   NUMBER(10)               NOT NULL,
  DS_MENSAGEM          VARCHAR2(4000 BYTE)      NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSIREA_PK ON TASY.PLS_INTERC_REGRA_AVISO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSIREA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSIREA_PLSPINA_FK_I ON TASY.PLS_INTERC_REGRA_AVISO
(NR_SEQ_ACAO_INTERC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSIREA_PLSPINA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_INTERC_REGRA_AVISO ADD (
  CONSTRAINT PLSIREA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_INTERC_REGRA_AVISO ADD (
  CONSTRAINT PLSIREA_PLSPINA_FK 
 FOREIGN KEY (NR_SEQ_ACAO_INTERC) 
 REFERENCES TASY.PLS_PROCESSO_INTERC_ACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_INTERC_REGRA_AVISO TO NIVEL_1;

