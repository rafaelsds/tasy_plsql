ALTER TABLE TASY.W_DIOPS_ENDERECO_ATIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DIOPS_ENDERECO_ATIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DIOPS_ENDERECO_ATIVO
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NR_SEQ_OPERADORA    NUMBER(10),
  NR_SEQ_TRANSACAO    NUMBER(10),
  NR_SEQ_PERIODO      NUMBER(10),
  DS_LOGRADOURO       VARCHAR2(40 BYTE),
  NR_LOGRADOURO       VARCHAR2(10 BYTE),
  DS_COMPLEMENTO      VARCHAR2(20 BYTE),
  DS_BAIRRO           VARCHAR2(40 BYTE),
  CD_MUNICIPIO_IBGE   VARCHAR2(6 BYTE),
  SG_UF               VARCHAR2(2 BYTE),
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_CEP              VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO  NUMBER(4),
  NR_SEQ_ATIVO_IMOB   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WDIENAT_PK ON TASY.W_DIOPS_ENDERECO_ATIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_DIOPS_ENDERECO_ATIVO ADD (
  CONSTRAINT WDIENAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_DIOPS_ENDERECO_ATIVO TO NIVEL_1;

