ALTER TABLE TASY.NOTA_FISCAL_VENC_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL_VENC_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL_VENC_TRIB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_VENCIMENTO        DATE                     NOT NULL,
  CD_TRIBUTO           NUMBER(3)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  VL_TRIBUTO           NUMBER(15,2)             NOT NULL,
  VL_BASE_CALCULO      NUMBER(15,2)             NOT NULL,
  TX_TRIBUTO           NUMBER(15,4)             NOT NULL,
  VL_TRIB_NAO_RETIDO   NUMBER(15,2),
  VL_BASE_NAO_RETIDO   NUMBER(15,2),
  VL_TRIB_ADIC         NUMBER(15,2),
  VL_BASE_ADIC         NUMBER(15,2),
  VL_REDUCAO           NUMBER(15,2),
  VL_DESC_BASE         NUMBER(15,2),
  CD_DARF              VARCHAR2(10 BYTE),
  DT_CONFERENCIA       DATE,
  NM_USUARIO_CONF      VARCHAR2(15 BYTE),
  IE_ORIGEM            VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_REGRA_TRIB    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NFVTRIB_NOTFIVE_FK_I ON TASY.NOTA_FISCAL_VENC_TRIB
(NR_SEQUENCIA, DT_VENCIMENTO, IE_ORIGEM)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NFVTRIB_PK ON TASY.NOTA_FISCAL_VENC_TRIB
(NR_SEQUENCIA, DT_VENCIMENTO, CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFVTRIB_TRIBUTO_FK_I ON TASY.NOTA_FISCAL_VENC_TRIB
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_venc_trib_atual
before insert or update ON TASY.NOTA_FISCAL_VENC_TRIB for each row
declare

vl_base_calculo_w		number(15,4);
ie_forma_arred_nf_w		varchar2(15);

begin
select	nvl(max(ie_forma_arred_nf), 'N')
into	ie_forma_arred_nf_w
from	tributo
where	cd_tributo	= :new.cd_tributo;

if	(ie_forma_arred_nf_w = 'S') then
	begin
	vl_base_calculo_w	:= nvl(:new.vl_base_calculo, 0) - nvl(:new.vl_reducao, 0);

	if	(vl_base_calculo_w is not null) and (:new.tx_tributo is not null) then

		if	(:new.vl_tributo <> 0) then /* RFOLIVEIRA - Inclui este IF pois existem vezes em que n�o � retido o valor (acumulado) e esta trigger acaba gerando mesmo assim*/
			begin
			:new.vl_tributo	:= trunc(dividir(vl_base_calculo_w * :new.tx_tributo, 100),2);
			exception when others then
				vl_base_calculo_w := vl_base_calculo_w;
				/*insert into log__tasy
					(dt_atualizacao,
					nm_usuario,
					cd_log,
					ds_log)
				values	(sysdate,
					'Trigger',
					1544,
					'Erro ao tentar ajustar o valor do tributo na nota' || chr(13) ||
					'Vencto NF: ' || :new.nr_sequencia || chr(13) ||
					'Dt vencto: ' || :new.dt_vencimento || chr(13) ||
					'Tributo: ' || :new.cd_tributo);*/
			end;
		end if;

	end if;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_venc_trib_delete
before delete ON TASY.NOTA_FISCAL_VENC_TRIB FOR EACH ROW
declare
PRAGMA AUTONOMOUS_TRANSACTION;
ds_benefic_w	varchar2(80);
dt_emissao_w	date;
vl_vencimento_w	number(15,2);
ds_tributo_w	varchar2(40);
dt_vencimento_w	date;

begin

select	substr(obter_nome_pf_pj(b.cd_pessoa_fisica,b.cd_cgc),1,80),
	b.dt_emissao,
	a.dt_vencimento,
	a.vl_vencimento
into	ds_benefic_w,
	dt_emissao_w,
	dt_vencimento_w,
	vl_vencimento_w
from	nota_fiscal b,
	nota_fiscal_venc a
where	a.nr_sequencia	= b.nr_sequencia
and	a.nr_sequencia	= :old.nr_sequencia
and	a.dt_vencimento	= :old.dt_vencimento;

select	ds_tributo
into	ds_tributo_w
from	tributo
where	cd_tributo = :old.cd_tributo;

/*insert	into log__tasy
	(dt_atualizacao,
	nm_usuario,
	cd_log,
	ds_log)
values	(sysdate,
	'Tasy',
	5800,
	'Pessoa nota: ' || ds_benefic_w || chr(13) ||
	'Emiss�o: ' || dt_emissao_w || chr(13) ||
	'Vencimento: ' || dt_vencimento_w || chr(13) ||
	'Dt cria��o tributo: ' || to_char(:old.dt_atualizacao_nrec,'dd/mm/yyyy hh24:mi:ss') || chr(13) ||
	'Valor vencimento: ' || vl_vencimento_w || chr(13) ||
	'Tributo: ' || ds_tributo_w || chr(13) ||
	'Valor tributo ' || :old.vl_tributo);*/


commit;
END;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_venc_trib_insert
BEFORE INSERT ON TASY.NOTA_FISCAL_VENC_TRIB FOR EACH ROW
BEGIN

if	(:new.vl_tributo < 0) then
	/* O valor do tributo n�o pode ser negativo!
	Vl_tributo = #@VL_TRIBUTO#@ */
	wheb_mensagem_pck.exibir_mensagem_abort(267107, 'VL_TRIBUTO=' || :new.vl_tributo);
end if;

if	(:new.ie_origem is null) then
	:new.ie_origem := 'N';
end if;

END;
/


ALTER TABLE TASY.NOTA_FISCAL_VENC_TRIB ADD (
  CONSTRAINT NFVTRIB_PK
 PRIMARY KEY
 (NR_SEQUENCIA, DT_VENCIMENTO, CD_TRIBUTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL_VENC_TRIB ADD (
  CONSTRAINT NFVTRIB_NOTFIVE_FK 
 FOREIGN KEY (NR_SEQUENCIA, DT_VENCIMENTO, IE_ORIGEM) 
 REFERENCES TASY.NOTA_FISCAL_VENC (NR_SEQUENCIA,DT_VENCIMENTO,IE_ORIGEM)
    ON DELETE CASCADE,
  CONSTRAINT NFVTRIB_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO));

GRANT SELECT ON TASY.NOTA_FISCAL_VENC_TRIB TO NIVEL_1;

