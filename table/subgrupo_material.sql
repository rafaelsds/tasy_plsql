ALTER TABLE TASY.SUBGRUPO_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUBGRUPO_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUBGRUPO_MATERIAL
(
  CD_SUBGRUPO_MATERIAL  NUMBER(3)               NOT NULL,
  DS_SUBGRUPO_MATERIAL  VARCHAR2(255 BYTE)      NOT NULL,
  CD_GRUPO_MATERIAL     NUMBER(3)               NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_CONTA_CONTABIL     VARCHAR2(20 BYTE),
  CD_SISTEMA_ANT        VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.SUBGRUPO_MATERIAL.CD_SUBGRUPO_MATERIAL IS 'Codigo do Subgrupo de Material';

COMMENT ON COLUMN TASY.SUBGRUPO_MATERIAL.DS_SUBGRUPO_MATERIAL IS 'Descri�ao do Subgrupo de Material';

COMMENT ON COLUMN TASY.SUBGRUPO_MATERIAL.CD_GRUPO_MATERIAL IS 'Codigo do Grupo do Material.';

COMMENT ON COLUMN TASY.SUBGRUPO_MATERIAL.IE_SITUACAO IS 'Indicador de Situacao';


CREATE INDEX TASY.SUBGRMA_GRUMATE_FK_I ON TASY.SUBGRUPO_MATERIAL
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SUBGRMA_PK ON TASY.SUBGRUPO_MATERIAL
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUBGRUPO_MATERIAL_tp  after update ON TASY.SUBGRUPO_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_SUBGRUPO_MATERIAL);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SUBGRUPO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'SUBGRUPO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_MATERIAL,1,4000),substr(:new.CD_GRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_MATERIAL',ie_log_w,ds_w,'SUBGRUPO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SUBGRUPO_MATERIAL,1,4000),substr(:new.DS_SUBGRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_SUBGRUPO_MATERIAL',ie_log_w,ds_w,'SUBGRUPO_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SUBGRUPO_MATERIAL,1,4000),substr(:new.CD_SUBGRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_MATERIAL',ie_log_w,ds_w,'SUBGRUPO_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.subgrupo_material_after
after insert or update ON TASY.SUBGRUPO_MATERIAL for each row
declare

reg_integracao_p	gerar_int_padrao.reg_integracao;

begin
	IF(WHEB_USUARIO_PCK.GET_IE_EXECUTAR_TRIGGER = 'N')THEN
		RETURN;
	END IF;

reg_integracao_p.cd_subgrupo_material	:=	:new.cd_subgrupo_material;
reg_integracao_p.cd_grupo_material	:=	:new.cd_grupo_material;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;

if	(inserting) then
	reg_integracao_p.ie_operacao	:=	'I';
elsif	(updating) then
	reg_integracao_p.ie_operacao	:=	'A';
end if;

gerar_int_padrao.gravar_integracao('4', :new.cd_subgrupo_material, :new.nm_usuario, reg_integracao_p);
end;
/


ALTER TABLE TASY.SUBGRUPO_MATERIAL ADD (
  CHECK ( ie_situacao IN ( 'A' , 'I' )  ),
  CONSTRAINT SUBGRMA_PK
 PRIMARY KEY
 (CD_SUBGRUPO_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUBGRUPO_MATERIAL ADD (
  CONSTRAINT SUBGRMA_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL));

GRANT SELECT ON TASY.SUBGRUPO_MATERIAL TO NIVEL_1;

