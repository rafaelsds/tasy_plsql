ALTER TABLE TASY.LOTE_ENCONTRO_CONTAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_ENCONTRO_CONTAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_ENCONTRO_CONTAS
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_LOTE                   DATE                NOT NULL,
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE),
  CD_CGC                    VARCHAR2(14 BYTE),
  IE_TIPO_DATA_CR           VARCHAR2(3 BYTE)    NOT NULL,
  DT_INICIAL_CR             DATE,
  DT_FINAL_CR               DATE,
  IE_TIPO_DATA_CP           VARCHAR2(3 BYTE)    NOT NULL,
  DT_INICIAL_CP             DATE,
  DT_FINAL_CP               DATE,
  DT_GERACAO                DATE,
  VL_SALDO                  NUMBER(15,2)        NOT NULL,
  DT_BAIXA                  DATE,
  IE_SEM_CAMARA_COMP        VARCHAR2(3 BYTE)    NOT NULL,
  IE_COOPERADOS             VARCHAR2(3 BYTE)    NOT NULL,
  IE_COOPERATIVAS           VARCHAR2(3 BYTE),
  IE_TIPO_PESSOA            VARCHAR2(3 BYTE),
  CD_EMPRESA                NUMBER(4),
  DT_CANCELAMENTO           DATE,
  NM_USUARIO_CANC           VARCHAR2(15 BYTE),
  NR_SEQ_CLASSE_PAG         NUMBER(10),
  NR_SEQ_CLASSE_REC         NUMBER(10),
  IE_FORMA_ENCONTRO_CONTAS  VARCHAR2(15 BYTE),
  NR_TIPO_TITULO_CR         VARCHAR2(2 BYTE),
  NR_TIPO_TITULO_CP         VARCHAR2(2 BYTE),
  IE_AUTOMATICO             VARCHAR2(1 BYTE),
  NR_SEQ_REGRA              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOTENCO_CLATIRE_FK_I ON TASY.LOTE_ENCONTRO_CONTAS
(NR_SEQ_CLASSE_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTENCO_CLTITPA_FK_I ON TASY.LOTE_ENCONTRO_CONTAS
(NR_SEQ_CLASSE_PAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTENCO_EMPRESA_FK_I ON TASY.LOTE_ENCONTRO_CONTAS
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOTENCO_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOTENCO_ESTABEL_FK_I ON TASY.LOTE_ENCONTRO_CONTAS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOTENCO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LOTENCO_LECRLA_FK_I ON TASY.LOTE_ENCONTRO_CONTAS
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTENCO_PESFISI_FK_I ON TASY.LOTE_ENCONTRO_CONTAS
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTENCO_PESJURI_FK_I ON TASY.LOTE_ENCONTRO_CONTAS
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOTENCO_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LOTENCO_PK ON TASY.LOTE_ENCONTRO_CONTAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LOTENCO_PK
  MONITORING USAGE;


ALTER TABLE TASY.LOTE_ENCONTRO_CONTAS ADD (
  CONSTRAINT LOTENCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_ENCONTRO_CONTAS ADD (
  CONSTRAINT LOTENCO_LECRLA_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_LANC_AUTO_EC (NR_SEQUENCIA),
  CONSTRAINT LOTENCO_CLATIRE_FK 
 FOREIGN KEY (NR_SEQ_CLASSE_REC) 
 REFERENCES TASY.CLASSE_TITULO_RECEBER (NR_SEQUENCIA),
  CONSTRAINT LOTENCO_CLTITPA_FK 
 FOREIGN KEY (NR_SEQ_CLASSE_PAG) 
 REFERENCES TASY.CLASSE_TITULO_PAGAR (NR_SEQUENCIA),
  CONSTRAINT LOTENCO_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT LOTENCO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT LOTENCO_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT LOTENCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.LOTE_ENCONTRO_CONTAS TO NIVEL_1;

