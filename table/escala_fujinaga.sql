ALTER TABLE TASY.ESCALA_FUJINAGA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_FUJINAGA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_FUJINAGA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_IDADE_CORRIGIDA     NUMBER(3),
  QT_PONTUACAO           NUMBER(3),
  QT_VOLUME              NUMBER(15,4),
  IE_ALIMENTACAO         VARCHAR2(3 BYTE),
  QT_IDADE_POS_NATAL     VARCHAR2(10 BYTE),
  QT_GANHO_PONDERAL      NUMBER(15,4),
  QT_IDADE_CORRIGIDA     NUMBER(3),
  QT_PESO                NUMBER(10,3),
  IE_ESTADO_CONSCIENCIA  NUMBER(3)              NOT NULL,
  IE_POSTURA_GLOBAL      NUMBER(3)              NOT NULL,
  IE_TONUS_GLOBAL        NUMBER(3)              NOT NULL,
  IE_POSTURA_LABIO       NUMBER(3)              NOT NULL,
  IE_POSTURA_LINGUA      NUMBER(3)              NOT NULL,
  IE_REFLEXO_PROCURA     NUMBER(3)              NOT NULL,
  IE_REFLEXO_SUCCAO      NUMBER(3)              NOT NULL,
  IE_REFLEXO_MORDIDA     NUMBER(3)              NOT NULL,
  IE_REFLEXO_VOMITO      NUMBER(3)              NOT NULL,
  IE_MOV_LINGUA          NUMBER(3)              NOT NULL,
  IE_CANOLAMENTO_LINGUA  NUMBER(3)              NOT NULL,
  IE_MOV_MANDIBULA       NUMBER(3)              NOT NULL,
  IE_FORCA_SUCCAO        NUMBER(3)              NOT NULL,
  IE_SUCCAO_PAUSA        NUMBER(3)              NOT NULL,
  IE_MANUT_RITMO         NUMBER(3)              NOT NULL,
  IE_MANUT_ALERTA        NUMBER(3)              NOT NULL,
  IE_SINAL_ESTRESSE      NUMBER(3)              NOT NULL,
  IE_ACOMULO_SALIVA      NUMBER(1)              NOT NULL,
  IE_BAT_ASA_NASAL       NUMBER(1)              NOT NULL,
  IE_VAR_COLORACAO       NUMBER(1)              NOT NULL,
  IE_APNEIA              NUMBER(1)              NOT NULL,
  IE_VAR_TONUS           NUMBER(1)              NOT NULL,
  IE_VAR_POSTURA         NUMBER(1)              NOT NULL,
  IE_TIRAGEM             NUMBER(1)              NOT NULL,
  IE_TREMORES_LINGUA     NUMBER(1)              NOT NULL,
  IE_SOLUCO              NUMBER(1)              NOT NULL,
  IE_CHORO               NUMBER(1)              NOT NULL,
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCFUJI_ATEPACI_FK_I ON TASY.ESCALA_FUJINAGA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCFUJI_PESFISI_FK_I ON TASY.ESCALA_FUJINAGA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCFUJI_PK ON TASY.ESCALA_FUJINAGA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCFUJI_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ESCALA_FUJINAGA_ATUAL
before update or insert ON TASY.ESCALA_FUJINAGA for each row
declare

qt_total_estresse_w		number(10);
ie_sinal_estresse_w		number(10);

begin
qt_total_estresse_w	:=	nvl(:new.IE_VAR_TONUS,0)		+
						nvl(:new.IE_VAR_POSTURA,0)		+
						nvl(:new.IE_VAR_COLORACAO,0)	+
						nvl(:new.IE_BAT_ASA_NASAL,0)	+
						nvl(:new.IE_TIRAGEM,0)			+
						nvl(:new.IE_APNEIA,0)			+
						nvl(:new.IE_ACOMULO_SALIVA,0)	+
						nvl(:new.IE_TREMORES_LINGUA,0)	+
						nvl(:new.IE_SOLUCO,0)			+
						nvl(:new.IE_CHORO,0);

if	(qt_total_estresse_w = 0) then
	:new.IE_SINAL_ESTRESSE := 2;
	ie_sinal_estresse_w := 2;
elsif	(qt_total_estresse_w <= 3) then
	:new.IE_SINAL_ESTRESSE := 1;
	ie_sinal_estresse_w := 1;
elsif	(qt_total_estresse_w > 3) then
	:new.IE_SINAL_ESTRESSE := 0;
	ie_sinal_estresse_w := 0;
end if;


:new.QT_PONTUACAO	:=	nvl(:new.IE_IDADE_CORRIGIDA,0)		+
						nvl(:new.IE_ESTADO_CONSCIENCIA,0)	+
						nvl(:new.IE_POSTURA_GLOBAL,0)		+
						nvl(:new.IE_TONUS_GLOBAL,0)			+
						nvl(:new.IE_POSTURA_LABIO,0)		+
						nvl(:new.IE_POSTURA_LINGUA,0)		+
						nvl(:new.IE_REFLEXO_PROCURA,0)		+
						nvl(:new.IE_REFLEXO_SUCCAO,0)		+
						nvl(:new.IE_REFLEXO_MORDIDA,0)		+
						nvl(:new.IE_REFLEXO_VOMITO,0)		+
						nvl(:new.IE_MOV_LINGUA,0)			+
						nvl(:new.IE_CANOLAMENTO_LINGUA,0)	+
						nvl(:new.IE_MOV_MANDIBULA,0)		+
						nvl(:new.IE_FORCA_SUCCAO,0)			+
						nvl(:new.IE_SUCCAO_PAUSA,0)			+
						nvl(:new.IE_MANUT_RITMO,0)			+
						nvl(:new.IE_MANUT_ALERTA,0)			+
						nvl(ie_sinal_estresse_w,0);
end;
/


ALTER TABLE TASY.ESCALA_FUJINAGA ADD (
  CONSTRAINT ESCFUJI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_FUJINAGA ADD (
  CONSTRAINT ESCFUJI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCFUJI_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_FUJINAGA TO NIVEL_1;

