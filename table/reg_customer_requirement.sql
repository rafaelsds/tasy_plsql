ALTER TABLE TASY.REG_CUSTOMER_REQUIREMENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_CUSTOMER_REQUIREMENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_CUSTOMER_REQUIREMENT
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_CODE                 VARCHAR2(255 BYTE),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_TITLE                VARCHAR2(255 BYTE)    NOT NULL,
  DS_DESCRIPTION          CLOB,
  DT_APROVACAO            DATE,
  NR_SEQ_APRESENTACAO     NUMBER(10,3),
  IE_SITUACAO             VARCHAR2(1 BYTE),
  NR_SEQ_FEATURES         NUMBER(10),
  DS_DESCRICAO_CR         VARCHAR2(4000 BYTE),
  DS_RATIONALE            VARCHAR2(4000 BYTE),
  NR_SEQ_ESTAGIO          NUMBER(10),
  NM_CURTO                VARCHAR2(255 BYTE),
  CD_VERSAO               VARCHAR2(255 BYTE),
  NM_LIBERACAO_VV         VARCHAR2(15 BYTE),
  DT_LIBERACAO_VV         DATE,
  CD_CRS_ID               VARCHAR2(35 BYTE),
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_MOTIVO_INATIVACAO    VARCHAR2(255 BYTE),
  IE_CLINICO              VARCHAR2(1 BYTE),
  DS_RACIONAL_CLINICO     VARCHAR2(4000 BYTE),
  NM_USUARIO_APROVACAO    VARCHAR2(15 BYTE),
  NM_USUARIO_LIBERACAO    VARCHAR2(15 BYTE),
  DT_LIBERACAO            DATE,
  DT_LANCAMENTO           DATE,
  NM_USUARIO_LANCAMENTO   VARCHAR2(15 BYTE),
  IE_INTERNAL_FEATURE     VARCHAR2(1 BYTE),
  NR_SEQ_INTENCAO_USO     NUMBER(10),
  NR_SEQ_ANALISE_IMPACTO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_DESCRIPTION) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGCR_MANOSIM_FK_I ON TASY.REG_CUSTOMER_REQUIREMENT
(NR_SEQ_ANALISE_IMPACTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGCR_PK ON TASY.REG_CUSTOMER_REQUIREMENT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCR_REGEST_FK_I ON TASY.REG_CUSTOMER_REQUIREMENT
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCR_REGFEC_FK_I ON TASY.REG_CUSTOMER_REQUIREMENT
(NR_SEQ_FEATURES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCR_REGINU_FK_I ON TASY.REG_CUSTOMER_REQUIREMENT
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGCR_UK ON TASY.REG_CUSTOMER_REQUIREMENT
(NR_SEQ_APRESENTACAO, NR_SEQ_FEATURES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.reg_customer_req_update

  before insert or update ON TASY.REG_CUSTOMER_REQUIREMENT 
  for each row
declare

  qt_product_requirements_w number(10);

  nr_seq_ap_area_w reg_area_customer.nr_seq_apresentacao%type;

  nr_seq_ap_feature_w reg_features_customer.nr_seq_apresentacao%type;

  nr_seq_intencao_uso_w number(10);

  nr_seq_urs_w number(10);

  ds_prefixo_w varchar2(15);

  nr_sequencia_w varchar2(15);

begin

	if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
		goto Final;
	end if;

  select count(1)

    into qt_product_requirements_w

    from reg_product_requirement

   where nr_customer_requirement = :new.nr_sequencia;

  if (:new.cd_crs_id is null) or
     (:new.cd_crs_id is not null and qt_product_requirements_w = 0) then

    if :new.nr_seq_features is not null then

      select ac.nr_seq_apresentacao,

             fc.nr_seq_apresentacao,

             iu.nr_sequencia,

             iu.ds_prefixo

        into nr_seq_ap_area_w,

             nr_seq_ap_feature_w,

             nr_seq_intencao_uso_w,

             ds_prefixo_w

        from reg_area_customer ac,

             reg_features_customer fc,

             reg_intencao_uso iu

       where ac.nr_sequencia = fc.nr_seq_area_customer

         and ac.nr_seq_intencao_uso = iu.nr_sequencia

         and fc.nr_sequencia = :new.nr_seq_features;

    elsif :new.nr_seq_intencao_uso is not null then

      select iu.nr_sequencia,

             iu.ds_prefixo

        into nr_seq_intencao_uso_w,

             ds_prefixo_w

        from reg_intencao_uso iu

       where iu.nr_sequencia = :new.nr_seq_intencao_uso;

    end if;

    -- Se a intencao de uso for do Tasy, manter nomeclatura do id



    if nr_seq_intencao_uso_w = 2 then

      :new.cd_crs_id := 'A_0_' || nr_seq_ap_area_w || '.' ||
                        nr_seq_ap_feature_w || '.' ||
                        :new.nr_seq_apresentacao;

    elsif inserting then

      select max(a.nr_sequencia)
        into nr_seq_urs_w
        from reg_customer_requirement a
       where a.nr_seq_intencao_uso = nvl(:new.nr_seq_intencao_uso, nr_seq_intencao_uso_w);


      if (nr_seq_urs_w is not null) then

        select nvl(obter_somente_numero(cd_crs_id), 0) + 1
          into nr_sequencia_w
          from reg_customer_requirement
         where nr_sequencia = nr_seq_urs_w;

      end if;

      :new.cd_crs_id := nvl(ds_prefixo_w, nr_seq_intencao_uso_w) || '_URS_' ||
                        nvl(nr_sequencia_w, '1');

    end if;

  end if;

	<<Final>>

	if (nr_seq_intencao_uso_w is not null) then
		:new.nr_seq_intencao_uso := nr_seq_intencao_uso_w;
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.reg_customer_req_insert
BEFORE INSERT ON TASY.REG_CUSTOMER_REQUIREMENT FOR EACH ROW
DECLARE
	dt_liberacao_w	reg_features_customer.dt_aprovacao%TYPE;
BEGIN
	SELECT	dt_aprovacao
	INTO	dt_liberacao_w
	FROM	reg_features_customer
	WHERE	nr_sequencia = :NEW.nr_seq_features;

	IF	dt_liberacao_w IS NULL THEN
		wheb_mensagem_pck.exibir_mensagem_abort(901104);--N�o � poss�vel adicionar um requisito de cliente em uma feature ainda n�o liberada
	END IF;
END;
/


CREATE OR REPLACE TRIGGER TASY.reg_customer_requirement_after
after update ON TASY.REG_CUSTOMER_REQUIREMENT for each row
declare

ie_urs_lancado_w		varchar2(1);
ie_tipo_alteracao_w		reg_customer_req_hist.ie_tipo_alteracao%type;

begin

select	decode(count(1), 0, 'N', 'S')
into	ie_urs_lancado_w -- Determinar se o URS (CRS) j?oi liberado em alguma revis?do documento de URS
from	reg_customer_req_hist
where	nr_seq_customer_req = :new.nr_sequencia
and	ie_tipo_alteracao = 'I'
and	nr_seq_reg_version_rev is not null;

if (:new.dt_aprovacao is not null) then

	delete	reg_customer_req_hist
	where	nr_seq_customer_req = :new.nr_sequencia
	and	nr_seq_reg_version_rev is null;

	if (ie_urs_lancado_w = 'S') then
		if	(:old.ie_situacao = 'A') and
			(:new.ie_situacao = 'I') then
			ie_tipo_alteracao_w := 'E';
		else
			ie_tipo_alteracao_w := 'A';
		end if;
	else
		if (:new.ie_situacao = 'A') then
			ie_tipo_alteracao_w := 'I';
		end if;
	end if;
end if;

if (ie_tipo_alteracao_w is not null) then

	insert into reg_customer_req_hist (
		nr_sequencia,
		cd_code,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ds_title,
		dt_aprovacao,
		nr_seq_apresentacao,
		ie_situacao,
		cd_versao,
		nr_seq_reg_version_rev,
		nr_seq_features,
		ds_descricao_cr,
		nr_seq_customer_req,
		ds_rationale,
		nr_seq_estagio,
		cd_crs_id,
		nm_curto,
		nm_liberacao_vv,
		dt_liberacao_vv,
		ie_tipo_alteracao,
		ie_internal_feature,
		dt_inativacao,
		nm_usuario_inativacao,
		ds_motivo_inativacao,
		ie_clinico,
		ds_racional_clinico,
		nm_usuario_aprovacao,
		dt_liberacao,
		nm_usuario_liberacao,
		dt_lancamento,
		nm_usuario_lancamento)
	values (
		reg_customer_req_hist_seq.nextval,
		:new.cd_code,
		:new.dt_atualizacao,
		:new.nm_usuario,
		:new.dt_atualizacao_nrec,
		:new.nm_usuario_nrec,
		:new.ds_title,
		:new.dt_aprovacao,
		:new.nr_seq_apresentacao,
		:new.ie_situacao,
		:new.cd_versao,
		null,
		:new.nr_seq_features,
		:new.ds_descricao_cr,
		:new.nr_sequencia,
		:new.ds_rationale,
		:new.nr_seq_estagio,
		:new.cd_crs_id,
		:new.nm_curto,
		:new.nm_liberacao_vv,
		:new.dt_liberacao_vv,
		ie_tipo_alteracao_w,
		:new.ie_internal_feature,
		:new.dt_inativacao,
		:new.nm_usuario_inativacao,
		:new.ds_motivo_inativacao,
		:new.ie_clinico,
		:new.ds_racional_clinico,
		:new.nm_usuario_aprovacao,
		:new.dt_liberacao,
		:new.nm_usuario_liberacao,
		:new.dt_lancamento,
		:new.nm_usuario_lancamento);

end if;

end;
/


ALTER TABLE TASY.REG_CUSTOMER_REQUIREMENT ADD (
  CONSTRAINT REGCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT REGCR_UK
 UNIQUE (NR_SEQ_APRESENTACAO, NR_SEQ_FEATURES)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_CUSTOMER_REQUIREMENT ADD (
  CONSTRAINT REGCR_MANOSIM_FK 
 FOREIGN KEY (NR_SEQ_ANALISE_IMPACTO) 
 REFERENCES TASY.MAN_ORDEM_SERV_IMPACTO (NR_SEQUENCIA),
  CONSTRAINT REGCR_REGEST_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.REG_ESTAGIO_CR (NR_SEQUENCIA),
  CONSTRAINT REGCR_REGFEC_FK 
 FOREIGN KEY (NR_SEQ_FEATURES) 
 REFERENCES TASY.REG_FEATURES_CUSTOMER (NR_SEQUENCIA),
  CONSTRAINT REGCR_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_CUSTOMER_REQUIREMENT TO NIVEL_1;

