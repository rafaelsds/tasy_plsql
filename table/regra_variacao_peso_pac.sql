ALTER TABLE TASY.REGRA_VARIACAO_PESO_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_VARIACAO_PESO_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_VARIACAO_PESO_PAC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  PR_VARIACAO_PESO      NUMBER(15,4)            NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  PR_VARIACAO_PESO_ATE  NUMBER(15,4)            NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_SEXO               VARCHAR2(1 BYTE),
  QT_IDADE_MIN          NUMBER(15,4),
  QT_IDADE_MIN_MES      NUMBER(15,4),
  QT_IDADE_MIN_DIA      NUMBER(15,4),
  DS_MENSAGEM           VARCHAR2(255 BYTE)      NOT NULL,
  QT_IDADE_MAX          NUMBER(15,4),
  QT_IDADE_MAX_MES      NUMBER(15,4),
  QT_IDADE_MAX_DIA      NUMBER(15,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGVARP_PK ON TASY.REGRA_VARIACAO_PESO_PAC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_VARIACAO_PESO_PAC ADD (
  CONSTRAINT REGVARP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REGRA_VARIACAO_PESO_PAC TO NIVEL_1;

