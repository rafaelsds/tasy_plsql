ALTER TABLE TASY.PCS_PERFIL_MOVIMENTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PCS_PERFIL_MOVIMENTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PCS_PERFIL_MOVIMENTACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_PERFIL            VARCHAR2(255 BYTE)       NOT NULL,
  VL_INICIAL           NUMBER(13,4)             NOT NULL,
  VL_FINAL             NUMBER(13,4)             NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PCSPEMV_PK ON TASY.PCS_PERFIL_MOVIMENTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSPEMV_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PCS_PERFIL_MOVIMENTACAO_tp  after update ON TASY.PCS_PERFIL_MOVIMENTACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_PERFIL,1,4000),substr(:new.DS_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_PERFIL',ie_log_w,ds_w,'PCS_PERFIL_MOVIMENTACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PCS_PERFIL_MOVIMENTACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FINAL,1,4000),substr(:new.VL_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_FINAL',ie_log_w,ds_w,'PCS_PERFIL_MOVIMENTACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_INICIAL,1,4000),substr(:new.VL_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_INICIAL',ie_log_w,ds_w,'PCS_PERFIL_MOVIMENTACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PCS_PERFIL_MOVIMENTACAO ADD (
  CONSTRAINT PCSPEMV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PCS_PERFIL_MOVIMENTACAO TO NIVEL_1;

