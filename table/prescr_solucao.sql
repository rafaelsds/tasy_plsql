ALTER TABLE TASY.PRESCR_SOLUCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_SOLUCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_SOLUCAO
(
  NR_PRESCRICAO           NUMBER(14)            NOT NULL,
  NR_SEQ_SOLUCAO          NUMBER(6)             NOT NULL,
  IE_VIA_APLICACAO        VARCHAR2(5 BYTE)      NOT NULL,
  CD_INTERVALO            VARCHAR2(7 BYTE),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  CD_UNIDADE_MEDIDA       VARCHAR2(30 BYTE)     NOT NULL,
  IE_TIPO_DOSAGEM         VARCHAR2(3 BYTE)      NOT NULL,
  QT_DOSAGEM              NUMBER(15,4),
  QT_SOLUCAO_TOTAL        NUMBER(15,4),
  QT_TEMPO_APLICACAO      NUMBER(15,4),
  DS_SOLUCAO              VARCHAR2(100 BYTE),
  QT_VOLUME               NUMBER(15,4),
  NR_ETAPAS               NUMBER(3),
  DS_HORARIOS             VARCHAR2(2000 BYTE),
  IE_BOMBA_INFUSAO        VARCHAR2(1 BYTE),
  IE_SUSPENSO             VARCHAR2(1 BYTE),
  NR_AGRUPAMENTO          NUMBER(6),
  IE_ESQUEMA_ALTERNADO    VARCHAR2(1 BYTE),
  HR_PRIM_HORARIO         VARCHAR2(5 BYTE),
  IE_CALC_AUT             VARCHAR2(1 BYTE),
  QT_HORA_FASE            NUMBER(15,4),
  IE_ACM                  VARCHAR2(1 BYTE),
  IE_URGENCIA             VARCHAR2(1 BYTE),
  DT_SUSPENSAO            DATE,
  NM_USUARIO_SUSP         VARCHAR2(15 BYTE),
  IE_STATUS               VARCHAR2(15 BYTE),
  DT_STATUS               DATE,
  QT_CONCENTRACAO         NUMBER(15,4),
  CD_UNID_MED_CONC        VARCHAR2(30 BYTE),
  IE_SOLUCAO_ESPECIAL     VARCHAR2(1 BYTE),
  QT_DOSE_TERAPEUTICA     NUMBER(15,4),
  NR_UNID_TERAPEUTICA     NUMBER(10),
  QT_VOLUME_ADEP          NUMBER(15,4),
  IE_FORMA_INFUSAO        VARCHAR2(1 BYTE),
  DS_ORIENTACAO           VARCHAR2(2000 BYTE),
  IE_TIPO_SOLUCAO         VARCHAR2(15 BYTE),
  QT_TEMP_SOLUCAO         NUMBER(5,1),
  IE_MOMENTO              VARCHAR2(1 BYTE),
  IE_HEMODIALISE          VARCHAR2(1 BYTE),
  NR_SEQ_PROTOCOLO        NUMBER(10),
  IE_POS_DIALISADOR       VARCHAR2(1 BYTE),
  QT_VOLUME_SUSPENSO      NUMBER(15,4),
  NR_ETAPAS_SUSPENSA      NUMBER(3),
  IE_UNID_VEL_INF         VARCHAR2(3 BYTE),
  CD_SETOR_EXEC_INIC      NUMBER(5),
  CD_SETOR_EXEC_FIM       NUMBER(5),
  NR_SEQ_DISPOSITIVO      NUMBER(10),
  QT_HORAS_ESTABILIDADE   NUMBER(15,3),
  NR_SEQ_DIALISE          NUMBER(10),
  IE_ULTIMA_BOLSA         VARCHAR2(1 BYTE),
  QT_TEMPO_DRENAGEM       NUMBER(5),
  QT_TEMPO_INFUSAO        NUMBER(5),
  QT_TEMPO_PERMANENCIA    NUMBER(5),
  IE_APAP                 VARCHAR2(15 BYTE),
  IE_SE_NECESSARIO        VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_SUSP      NUMBER(10),
  DS_MOTIVO_SUSP          VARCHAR2(255 BYTE),
  QT_DOSE_ATAQUE          NUMBER(15,2),
  IE_ERRO                 NUMBER(3),
  DS_COR_ERRO             VARCHAR2(15 BYTE),
  IE_TIPO_ANALGESIA       VARCHAR2(15 BYTE),
  IE_PCA_MODO_PROG        VARCHAR2(15 BYTE),
  QT_DOSE_INICIAL_PCA     NUMBER(10,1),
  QT_VOL_INFUSAO_PCA      NUMBER(10,1),
  QT_BOLUS_PCA            NUMBER(10,1),
  QT_INTERVALO_BLOQUEIO   NUMBER(10),
  QT_LIMITE_QUATRO_HORA   NUMBER(10,1),
  DT_PREV_PROX_ETAPA      DATE,
  NR_SEQ_EVENTO           NUMBER(10),
  IE_SOLUCAO_PCA          VARCHAR2(1 BYTE),
  CD_PERFIL_ATIVO         NUMBER(5),
  IE_CLASSIF_AGORA        VARCHAR2(10 BYTE),
  IE_ETAPA_ESPECIAL       VARCHAR2(1 BYTE),
  PR_CONCENTRACAO         NUMBER(8,3),
  HR_INICIO_CAPD          VARCHAR2(5 BYTE),
  HR_FIM_CAPD             VARCHAR2(5 BYTE),
  DT_LIB_MATERIAL         DATE,
  DT_LIB_FARMACIA         DATE,
  DT_LIB_ENFERMAGEM       DATE,
  IE_UM_DOSE_INICIO_PCA   VARCHAR2(15 BYTE),
  IE_UM_FLUXO_PCA         VARCHAR2(15 BYTE),
  IE_UM_BOLUS_PCA         VARCHAR2(15 BYTE),
  IE_UM_LIMITE_PCA        VARCHAR2(15 BYTE),
  IE_TIPO_SOL             VARCHAR2(5 BYTE),
  NR_SEQ_ASSINATURA       NUMBER(10),
  NR_SEQ_INTERNO          NUMBER(10),
  IE_ABERTO               VARCHAR2(1 BYTE),
  IE_ESTENDER_ETAPA       VARCHAR2(1 BYTE),
  QT_MINUTO_FASE          NUMBER(15,4),
  QT_LIMITE_UMA_HORA      NUMBER(10,1),
  IE_UM_LIMITE_HORA_PCA   VARCHAR2(15 BYTE),
  QT_VOLUME_FINAL         NUMBER(15,4),
  QT_HORA_INFUSAO         NUMBER(2),
  NR_SEQ_INTERF_FARM      NUMBER(10),
  NR_PRESCRICAO_ORIGINAL  NUMBER(14),
  NR_SEQ_ANTERIOR         NUMBER(15),
  IE_ESTENDIDO            VARCHAR2(1 BYTE),
  QT_VOLUME_HIDRICO       NUMBER(18,6),
  DT_EXTENSAO             DATE,
  IE_BOMBA_ELASTOMERICA   NUMBER(10),
  DS_JUSTIF_TROCA_DISP    VARCHAR2(255 BYTE),
  QT_HORA_PERMANENCIA     NUMBER(3),
  NR_SEQ_FABRIC           NUMBER(10),
  DS_ORIENTACAO_ENF       VARCHAR2(255 BYTE),
  QT_TEMPO_PERM_HOR       NUMBER(3),
  NR_SEQ_ASSINATURA_SUSP  NUMBER(10),
  IE_SUSPENDER            VARCHAR2(1 BYTE),
  IE_TIDAL                VARCHAR2(1 BYTE),
  IE_LAVAR_LINHAS         VARCHAR2(1 BYTE),
  NR_ETAPAS_MEDICO        NUMBER(5),
  NR_SEQ_ORDEM_ADEP       NUMBER(10),
  IE_PRE_MEDICACAO        VARCHAR2(15 BYTE),
  DS_STACK                VARCHAR2(2000 BYTE),
  CD_PROTOCOLO            NUMBER(10),
  NR_SEQ_PROT_ROTINA      NUMBER(10),
  NR_PRESCRICAO_ANTERIOR  NUMBER(14),
  NR_SEQUENCIA_ANTERIOR   NUMBER(10),
  IE_MODIFICADA           VARCHAR2(1 BYTE),
  QT_VOLUME_CORRIGIDO     NUMBER(15,4),
  QT_MIN_DOSE_ATAQUE      NUMBER(10),
  DT_SUSPENSAO_PROGR      DATE,
  IE_FATOR_CORRECAO       VARCHAR2(1 BYTE),
  NR_SEQ_INCONSISTENCIA   NUMBER(10),
  NR_SEQ_DIALISE_CPOE     NUMBER(10),
  IE_SOL_INTERMITENTE     VARCHAR2(1 BYTE),
  IE_HORARIO_SUSP         VARCHAR2(1 BYTE),
  DS_HORARIOS_MEDICO      VARCHAR2(2000 BYTE),
  DT_FIM_CAPD             DATE,
  DT_INICIO_CAPD          DATE,
  DT_PRIM_HORARIO         DATE,
  IE_ITEM_DISP_GERADO     VARCHAR2(1 BYTE),
  IE_UM_FINAL_CONC_PCA    VARCHAR2(15 BYTE),
  QT_FINAL_CONCENTRATION  NUMBER(15,4),
  QT_DOSE_NAIS            NUMBER(15,4),
  QT_UNID_MEDIDA_NAIS     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRESOLU_BOMELAS_FK_I ON TASY.PRESCR_SOLUCAO
(IE_BOMBA_ELASTOMERICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_BOMELAS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_DISPOSI_FK_I ON TASY.PRESCR_SOLUCAO
(NR_SEQ_DISPOSITIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_DISPOSI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_FK2 ON TASY.PRESCR_SOLUCAO
(NR_SEQ_DIALISE_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESOLU_HDPRESC_FK_I ON TASY.PRESCR_SOLUCAO
(NR_SEQ_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESOLU_INMEFAR_FK_I ON TASY.PRESCR_SOLUCAO
(NR_SEQ_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESOLU_INTPRES_FK_I ON TASY.PRESCR_SOLUCAO
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_I1 ON TASY.PRESCR_SOLUCAO
(NR_PRESCRICAO, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESOLU_I2 ON TASY.PRESCR_SOLUCAO
(DT_STATUS, IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESOLU_I3 ON TASY.PRESCR_SOLUCAO
(NR_PRESCRICAO_ORIGINAL, NR_SEQ_ANTERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          240K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESOLU_MATFABR_FK_I ON TASY.PRESCR_SOLUCAO
(NR_SEQ_FABRIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_MATFABR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_MOTSUPR_FK_I ON TASY.PRESCR_SOLUCAO
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_MOTSUPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_PERFIL_FK_I ON TASY.PRESCR_SOLUCAO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          48K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRESOLU_PK ON TASY.PRESCR_SOLUCAO
(NR_PRESCRICAO, NR_SEQ_SOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESOLU_PRESMED_FK_I ON TASY.PRESCR_SOLUCAO
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESOLU_PROTNPT_FK_I ON TASY.PRESCR_SOLUCAO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_PROTNPT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_PRSOLEV_FK_I ON TASY.PRESCR_SOLUCAO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_PRSOLEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_REGDOTE99_FK_I ON TASY.PRESCR_SOLUCAO
(NR_UNID_TERAPEUTICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESOLU_SETATEN_FK_I ON TASY.PRESCR_SOLUCAO
(CD_SETOR_EXEC_INIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_SETATEN_FK2_I ON TASY.PRESCR_SOLUCAO
(CD_SETOR_EXEC_FIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_TASASDI_FK_I ON TASY.PRESCR_SOLUCAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_TASASDI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRESOLU_UK ON TASY.PRESCR_SOLUCAO
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_UK
  MONITORING USAGE;


CREATE INDEX TASY.PRESOLU_UNIMEDI_FK2_I ON TASY.PRESCR_SOLUCAO
(CD_UNID_MED_CONC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESOLU_UNIMEDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Prescr_solucao_Insert
BEFORE INSERT ON TASY.PRESCR_SOLUCAO FOR EACH ROW
DECLARE

dt_lib_w	date;
cd_funcao_origem_w		prescr_medica.cd_funcao_origem%type;

BEGIN

if	(:new.nr_seq_interno is null) then
	select	Prescr_solucao_seq.NextVal
	into	:new.nr_seq_interno
	from	dual;
end if;

select	max(nvl(dt_liberacao, dt_liberacao_medico)),
		max(cd_funcao_origem)
into	dt_lib_w,
		cd_funcao_origem_w
from	prescr_medica
where	nr_prescricao	= :new.nr_prescricao;

if	(dt_lib_w is not null) and
	(cd_funcao_origem_w	<> 2314) then
	Wheb_mensagem_pck.exibir_mensagem_abort(182660);
end if;

:new.ie_bomba_elastomerica	:= obter_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao);

if	(:new.ie_bomba_elastomerica is not null) and
	(nvl(obter_vol_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao),0) > 0) then
	:new.qt_dosagem			:= obter_vol_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao);
	:new.qt_solucao_total		:= Obter_vel_sol_mlh(:new.ie_tipo_dosagem, :new.qt_dosagem) * :new.qt_tempo_aplicacao * :new.nr_etapas;
end if;

if	(:new.nr_etapas = 0) and
	((:new.ie_acm = 'S') or
	 (:new.IE_SOLUCAO_PCA = 'S') or
	 (:new.ie_se_necessario = 'S')) and
	(nvl(:new.ie_hemodialise,'N') = 'N') then
	:new.nr_etapas	:= null;
end if;

:new.nr_etapas_medico	:= :new.nr_etapas;

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

END;
/


CREATE OR REPLACE TRIGGER TASY.Prescr_solucao_Update
BEFORE UPDATE ON TASY.PRESCR_SOLUCAO FOR EACH ROW
DECLARE

ie_lib_medico_w		varchar2(1);
qt_hora_fase_w		number(15,4);
nr_etapas_w			number(15,4);
ds_alteracao_w		varchar2(1800);
ie_nova_calc_terap_w	varchar2(1);
ie_acm_protocolo_w		protocolo_npt.ie_acm%type;
cd_funcao_origem_w  	prescr_medica.cd_funcao_origem%type;
qt_horas_adicional_w	number(18,6);
ie_operacao_w			intervalo_prescricao.ie_operacao%type;
ie_intervalo_24_w		intervalo_prescricao.ie_24_h%type;
dt_inicio_prescr_w		prescr_medica.dt_inicio_prescr%type;
hr_prim_horario_w		prescr_solucao.hr_prim_horario%type;
dt_inicio_item_w		date;
dt_copia_cpoe_w			cpoe_material.dt_prox_geracao%type;
nr_seq_mat_cpoe_w		cpoe_material.nr_sequencia%type;
qt_horas_ant_copia_cpoe_w	number(18);
ds_log_w				varchar2(2000);
nm_usuario_w			usuario.nm_usuario%type;

procedure gerar_log_alteracoes is
begin
	if	(nvl(:new.qt_solucao_total,-1) <> nvl(:old.qt_solucao_total,-1)) then
		ds_alteracao_w := ds_alteracao_w || 'QT_SOLUCAO_TOTAL(' || to_char(:old.qt_solucao_total) || '/' || to_char(:new.qt_solucao_total) ||'); ';
	end if;

	if	(ds_alteracao_w is not null) then
		ds_alteracao_w := substr('Alterações(old/new)= ' || ds_alteracao_w || 'NR_SEQ_SOLUCAO('||to_char(:new.nr_seq_solucao)||'); FUNCAO('||to_char(wheb_usuario_pck.get_cd_funcao)||'); PERFIL('||to_char(wheb_usuario_pck.get_cd_perfil)||')',1,1800);

		gerar_log_prescricao(
			nr_prescricao_p	=> :new.nr_prescricao,
			nr_seq_item_p	=> :new.nr_seq_solucao,
			ie_agrupador_p	=> null,
			nr_seq_horario_p => null,
			ie_tipo_item_p	=> 'SOL',
			ds_log_p		=> ds_alteracao_w,
			nm_usuario_p	=> nm_usuario_w,
			nr_seq_objeto_p	=> 3566,
			ie_commit_p		=> 'N'
		);
	end if;
end;

BEGIN
	Obter_Param_Usuario(924, 1151,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento, ie_nova_calc_terap_w);
	dt_inicio_prescr_w	:= obter_datas_prescricao(:new.nr_prescricao,'I');
	cd_funcao_origem_w	:= obter_funcao_origem_prescr(:new.nr_prescricao);
	nm_usuario_w := nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario);

	gerar_log_alteracoes;

update	prescr_material
set		ie_suspenso		= :new.ie_suspenso,
		nm_usuario_susp		= :new.nm_usuario_susp
where	nr_prescricao		= :new.nr_prescricao
and		nr_sequencia_solucao	= :new.nr_seq_solucao
and	ie_agrupador		in(4,13)
and	nvl(ie_suspenso,'N')	= 'N'
and	nr_seq_substituto	is null;

update 	prescr_material
set	ie_acm 			= :new.ie_acm,
	ie_se_necessario	= :new.ie_se_necessario
where	nr_prescricao		= :new.nr_prescricao
and	nr_sequencia_solucao	= :new.nr_seq_solucao
and	ie_agrupador		in(4,13);


select max(nr_seq_mat_cpoe)
  into nr_seq_mat_cpoe_w
  from prescr_material
 where nr_prescricao = :new.nr_prescricao
   and nr_sequencia_solucao = :new.nr_seq_solucao;


if	(nvl(:new.ie_suspenso, 'N') = 'S') then
	begin
	:new.ie_status := 'S';
	end;
end if;

if	(nvl(:old.ie_suspenso, 'N') = 'S') and
	(nvl(:new.ie_suspenso,'N') = 'N') then
	begin

	update 	prescr_material
	set	ie_suspenso = null,
		dt_suspensao = null,
		nm_usuario_susp = null
	where	nr_prescricao = :new.nr_prescricao
	and	nr_sequencia_solucao = :new.nr_seq_solucao;

	update 	prescr_mat_hor
	set	dt_suspensao = null,
		nm_usuario_susp = null
	where	nr_prescricao = :new.nr_prescricao
	and	nr_seq_solucao = :new.nr_seq_solucao
	and	dt_fim_horario is null;

	:new.ie_status := 'S';

	end;
end if;

if	(nvl(:new.ie_solucao_especial,:old.ie_solucao_especial) = 'N') then
	:new.ie_bomba_elastomerica	:= obter_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao);

	if	(:new.ie_bomba_elastomerica is not null) and
		(nvl(obter_vol_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao),0) > 0) then
		:new.qt_dosagem			:= obter_vol_bomba_elast_sol(:new.nr_prescricao, :new.nr_Seq_solucao);
		:new.qt_solucao_total		:= Obter_vel_sol_mlh(:new.ie_tipo_dosagem, :new.qt_dosagem) * :new.qt_tempo_aplicacao * :new.nr_etapas;
	end if;
end if;

--if	(nvl(:new.qt_volume,0) = 0) then

--end if;

if	(:new.nr_etapas = 0) and
	((:new.ie_acm = 'S') or
	 (:new.IE_SOLUCAO_PCA = 'S') or
	 (:new.ie_se_necessario = 'S')) and
	(nvl(:new.ie_hemodialise,'N') = 'N') then
	:new.nr_etapas	:= null;
end if;

if	(:new.nr_seq_dialise is not null) then
	--if	(ie_lib_medico_w = 'N') then
		:new.nr_etapas_medico := :new.nr_etapas;
	--end if;
end if;


if (nvl(cd_funcao_origem_w,9999) <> 2314) or (nvl(:new.qt_volume,0) = 0) then
	if	(:new.qt_solucao_total <> :old.qt_solucao_total) then
		select	nvl(:new.qt_solucao_total,sum(nvl(qt_solucao,0)))
		into	:new.qt_volume
		from	prescr_material
		where	nr_prescricao		= :new.nr_prescricao
		and	nr_sequencia_solucao	= :new.nr_seq_solucao
		and		nr_seq_substituto is null;
	else
		select	sum(nvl(qt_solucao,0))
		into	:new.qt_volume
		from	prescr_material
		where	nr_prescricao		= :new.nr_prescricao
		and	nr_sequencia_solucao	= :new.nr_seq_solucao
		and		nr_seq_substituto is null;
	end if;
end if;

if	(:new.cd_intervalo 		is not null) and
	(nvl(:new.IE_CALC_AUT,'N')	= 'S') and
	(:new.qt_hora_fase 		is not null) then
	Calcular_etapas_interv_solucao(:new.cd_intervalo, :new.qt_tempo_aplicacao, 'S', qt_hora_fase_w, nr_etapas_w,null,null);
	if	(:new.qt_hora_fase	<> qt_hora_fase_w) then
		Calcular_etapas_interv_solucao(:new.cd_intervalo, :new.qt_tempo_aplicacao, 'N', qt_hora_fase_w, nr_etapas_w,null,null);
	end if;
end if;

if	(:new.nr_seq_protocolo is not null) and
	(:new.nr_seq_dialise is not null) then

	select	nvl(max(ie_acm),'N')
	into	ie_acm_protocolo_w
	from	protocolo_npt
	where	nr_sequencia = :new.nr_seq_protocolo;

	:new.ie_acm := ie_acm_protocolo_w;

end if;

if (nvl(:old.ie_bomba_infusao,'XPTO') <> nvl(:new.ie_bomba_infusao,'XPTO')) then
	:new.ie_item_disp_gerado := 'N';
end if;

if	((nvl(:new.nr_etapas_suspensa,0) <> nvl(:old.nr_etapas_suspensa,0)) or ((nvl(:new.ie_suspenso,'N') = 'S') and (nvl(:old.ie_suspenso,'N') = 'N'))) then
	ds_log_w	:= 'Prescr='||:new.nr_prescricao||';Seq='||:new.nr_seq_solucao||';Old et='||nvl(:old.nr_etapas_suspensa,0)||';New et='||nvl(:new.nr_etapas_suspensa,0)||
				   ';Susp='||nvl(:new.ie_suspenso,'N')||';Stack='||substr(dbms_utility.format_call_stack,1,1700);
	gravar_log_tasy(4589, ds_log_w, :new.nm_usuario);
end if;

if	(:new.ds_horarios <> :old.ds_horarios) and
	(:new.ds_horarios is not null) and
	(cd_funcao_origem_w = 2314) and
	((obter_funcao_ativa = 7015) or
	 (obter_funcao_ativa = 7010) or
	 (obter_funcao_ativa = 252)) and
	(obter_datas_prescricao(:new.nr_prescricao,'L') is not null) then

	qt_horas_adicional_w := 24;
	if (:new.cd_intervalo is not null) then
		select	nvl(max(ie_24_h),'N')
		into	ie_intervalo_24_w
		from	intervalo_prescricao
		where	cd_intervalo = :new.cd_intervalo
		and    	nvl(ie_operacao,'X') = 'H'
		and    	nvl(qt_operacao,1) > 24;
	end if;

	qt_horas_ant_copia_cpoe_w := get_qt_hours_after_copy_cpoe( obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo);

	if (ie_intervalo_24_w = 'S') then
		qt_horas_adicional_w := Obter_ocorrencia_intervalo(:new.cd_intervalo,24,'H');
	else

		select 	nvl(max(ie_operacao),'X')
		into	ie_operacao_w
		from	intervalo_prescricao
		where	cd_intervalo = :new.cd_intervalo;

		qt_horas_adicional_w := Obter_ocorrencia_intervalo(:new.cd_intervalo,24,'H');
		if (ie_operacao_w = 'H' and qt_horas_adicional_w > 12 and qt_horas_adicional_w < 24) then
			qt_horas_adicional_w := qt_horas_adicional_w + qt_horas_adicional_w;
		else
			qt_horas_adicional_w := 24;
		end if;
	end if;

	hr_prim_horario_w := obter_prim_dshorarios(:new.ds_horarios);
	dt_inicio_item_w := to_date(to_char(dt_inicio_prescr_w, 'dd/mm/yyyy') || ' ' || hr_prim_horario_w || ':00', 'dd/mm/yyyy hh24:mi:ss');

	if (dt_inicio_item_w < dt_inicio_prescr_w) then
		dt_inicio_item_w := dt_inicio_item_w + 1;
	end if;

	dt_copia_cpoe_w	:= (dt_inicio_item_w + ((qt_horas_adicional_w - qt_horas_ant_copia_cpoe_w)/24));

	update	cpoe_material
	set		dt_prox_geracao = trunc(dt_copia_cpoe_w,'hh')
	where	nr_sequencia = nr_seq_mat_cpoe_w;

end if;


END;
/


ALTER TABLE TASY.PRESCR_SOLUCAO ADD (
  CONSTRAINT PRESOLU_PK
 PRIMARY KEY
 (NR_PRESCRICAO, NR_SEQ_SOLUCAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PRESOLU_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          88K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_SOLUCAO ADD (
  CONSTRAINT PRESOLU_INMEFAR_FK 
 FOREIGN KEY (NR_SEQ_INCONSISTENCIA) 
 REFERENCES TASY.INCONSISTENCIA_MED_FARM (NR_SEQUENCIA),
  CONSTRAINT PRESOLU_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT PRESOLU_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT PRESOLU_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNID_MED_CONC) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PRESOLU_REGDOTE99_FK 
 FOREIGN KEY (NR_UNID_TERAPEUTICA) 
 REFERENCES TASY.REGRA_DOSE_TERAP (NR_SEQUENCIA),
  CONSTRAINT PRESOLU_PROTNPT_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_NPT (NR_SEQUENCIA),
  CONSTRAINT PRESOLU_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_EXEC_INIC) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRESOLU_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_EXEC_FIM) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRESOLU_DISPOSI_FK 
 FOREIGN KEY (NR_SEQ_DISPOSITIVO) 
 REFERENCES TASY.DISPOSITIVO (NR_SEQUENCIA),
  CONSTRAINT PRESOLU_HDPRESC_FK 
 FOREIGN KEY (NR_SEQ_DIALISE) 
 REFERENCES TASY.HD_PRESCRICAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRESOLU_MOTSUPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_PRESCR (NR_SEQUENCIA),
  CONSTRAINT PRESOLU_PRSOLEV_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PRESCR_SOLUCAO_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PRESOLU_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PRESOLU_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PRESOLU_BOMELAS_FK 
 FOREIGN KEY (IE_BOMBA_ELASTOMERICA) 
 REFERENCES TASY.BOMBA_ELASTOMERICA (NR_SEQUENCIA),
  CONSTRAINT PRESOLU_MATFABR_FK 
 FOREIGN KEY (NR_SEQ_FABRIC) 
 REFERENCES TASY.MAT_FABRICANTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_SOLUCAO TO NIVEL_1;

