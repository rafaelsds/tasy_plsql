ALTER TABLE TASY.PROC_PAC_DESCRICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_PAC_DESCRICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_PAC_DESCRICAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_REGISTRO                DATE               NOT NULL,
  CD_PROCEDIMENTO            NUMBER(15)         NOT NULL,
  IE_ORIGEM_PROCED           NUMBER(10),
  NR_SEQ_PROC_INTERNO        NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DS_DESCRICAO               LONG,
  DT_INICIO                  DATE,
  DT_FIM                     DATE,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  QT_PROCEDIMENTO            NUMBER(9,3)        NOT NULL,
  IE_JUSTIFICATIVA_CONV      VARCHAR2(3 BYTE),
  IE_GERAR_CONTA             VARCHAR2(3 BYTE),
  IE_PARTICIPANTES           VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  IE_AVALIADOR_AUX           VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX           DATE,
  CD_AVALIADOR_AUX           VARCHAR2(10 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  CD_DOENCA                  VARCHAR2(10 BYTE),
  NR_SEQ_AVALIACAO           NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_FORMULARIO          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROPADE_ATCONSPEPA_FK_I ON TASY.PROC_PAC_DESCRICAO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPADE_ATEPACI_FK_I ON TASY.PROC_PAC_DESCRICAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPADE_EHRREGI_FK_I ON TASY.PROC_PAC_DESCRICAO
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPADE_MEDAVPA_FK_I ON TASY.PROC_PAC_DESCRICAO
(NR_SEQ_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPADE_PESFISI_FK_I ON TASY.PROC_PAC_DESCRICAO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPADE_PESFISI2_FK_I ON TASY.PROC_PAC_DESCRICAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROPADE_PK ON TASY.PROC_PAC_DESCRICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPADE_PROCEDI_FK_I ON TASY.PROC_PAC_DESCRICAO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPADE_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPADE_PROINTE_FK_I ON TASY.PROC_PAC_DESCRICAO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROPADE_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROPADE_TASASDI_FK_I ON TASY.PROC_PAC_DESCRICAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPADE_TASASDI_FK2_I ON TASY.PROC_PAC_DESCRICAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PROC_PAC_DESCRICAO_PEND
before insert or update ON TASY.PROC_PAC_DESCRICAO for each row
declare

cd_procediemnto_w   varchar(100);
ie_origem_proced_w  varchar(100);

begin
    if (:new.nm_usuario = 'integration' and wheb_usuario_pck.get_ie_executar_trigger	= 'S' ) then
       begin
         select cd_procedimento,
                 ie_origem_proced
            into cd_procediemnto_w,
                 ie_origem_proced_w
          from proc_interno
          where nr_sequencia = :new.nr_seq_proc_interno;
        exception
            when no_data_found then
                cd_procediemnto_w   :=  null;
                ie_origem_proced_w  :=  null;
        end;
    :new.cd_procedimento  := cd_procediemnto_w;
    :new.ie_origem_proced := ie_origem_proced_w;
    end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PROC_PAC_DESCRICAO_PEND_ATUAL
after insert or update or delete ON TASY.PROC_PAC_DESCRICAO for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_atestado_w	varchar2(10);
nm_usuario_w        varchar2(15);

begin
	if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
		goto Final;
	end if;
	begin
	if 	((INSERTING) OR	(UPDATING)) then
		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'PPD';
		elsif	((:old.dt_liberacao is null) and (:new.dt_liberacao is not null)) then
			ie_tipo_w := 'XPPD';
		end if;
		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario);
		end if;
	elsif (deleting) THEN
		delete from pep_item_pendente
		where 	nr_seq_registro = :old.nr_sequencia
		and	nvl(IE_TIPO_PENDENCIA,'L')	= 'L'
		and	IE_TIPO_REGISTRO = 'PPD';

		commit;

	end if;
exception
when others then
	null;
end;
<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PROC_PAC_DESCRICAO ADD (
  CONSTRAINT PROPADE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_PAC_DESCRICAO ADD (
  CONSTRAINT PROPADE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PROPADE_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROPADE_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PROPADE_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PROPADE_PESFISI2_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROPADE_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PROPADE_MEDAVPA_FK 
 FOREIGN KEY (NR_SEQ_AVALIACAO) 
 REFERENCES TASY.MED_AVALIACAO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT PROPADE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PROPADE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PROPADE_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROC_PAC_DESCRICAO TO NIVEL_1;

