ALTER TABLE TASY.PLS_PRESTADOR_DOCUMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_DOCUMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_DOCUMENTO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_ARQUIVO             VARCHAR2(255 BYTE),
  NR_SEQ_PREST_TIPO      NUMBER(10)             NOT NULL,
  DT_DOCUMENTO           DATE,
  DT_ACEITE              DATE,
  DT_RECUSA              DATE,
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  IE_STATUS              VARCHAR2(5 BYTE),
  NR_SEQ_PREST_DOC_VENC  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPRDO_PK ON TASY.PLS_PRESTADOR_DOCUMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRDO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRDO_PLSPRDV_FK_I ON TASY.PLS_PRESTADOR_DOCUMENTO
(NR_SEQ_PREST_DOC_VENC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRDO_PLSPRTD_FK_I ON TASY.PLS_PRESTADOR_DOCUMENTO
(NR_SEQ_PREST_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRDO_PLSPRTD_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_PRESTADOR_DOCUMENTO ADD (
  CONSTRAINT PLSPRDO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_DOCUMENTO ADD (
  CONSTRAINT PLSPRDO_PLSPRDV_FK 
 FOREIGN KEY (NR_SEQ_PREST_DOC_VENC) 
 REFERENCES TASY.PLS_PRESTADOR_DOC_VENC (NR_SEQUENCIA),
  CONSTRAINT PLSPRDO_PLSPRTD_FK 
 FOREIGN KEY (NR_SEQ_PREST_TIPO) 
 REFERENCES TASY.PLS_PRESTADOR_TIPO_DOC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_DOCUMENTO TO NIVEL_1;

