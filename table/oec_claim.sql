ALTER TABLE TASY.OEC_CLAIM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OEC_CLAIM CASCADE CONSTRAINTS;

CREATE TABLE TASY.OEC_CLAIM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_SEQ_TRANSACTION      VARCHAR2(25 BYTE)     NOT NULL,
  IE_OEC_TYPE             VARCHAR2(3 BYTE),
  IE_SUBMISSION           VARCHAR2(1 BYTE),
  DT_ACCIDENT             DATE,
  IE_ACCIDENT             VARCHAR2(1 BYTE),
  NR_ACCOUNT              VARCHAR2(20 BYTE),
  IE_CLAIM_TYPE           VARCHAR2(2 BYTE),
  IE_EMERGENCY_ADM        VARCHAR2(1 BYTE),
  CD_FACILITY             VARCHAR2(8 BYTE),
  CD_FUND_BRAND           VARCHAR2(3 BYTE),
  QT_LENGTH_STAY          NUMBER(4),
  IE_PEA_REQUEST          VARCHAR2(1 BYTE),
  CD_ILLNESS              NUMBER(3),
  CD_ILLNESS_ITEM         VARCHAR2(5 BYTE),
  CD_PRINCIPAL_PROVIDER   VARCHAR2(8 BYTE),
  IE_SAME_DAY             VARCHAR2(1 BYTE),
  DS_EMAIL                VARCHAR2(128 BYTE),
  NM_CONTACT              VARCHAR2(40 BYTE),
  NR_PHONE                NUMBER(19),
  IE_SERVICE_TYPE         VARCHAR2(1 BYTE),
  IE_STATUS_TASY          VARCHAR2(10 BYTE)     NOT NULL,
  NR_ATENDIMENTO          NUMBER(10),
  NR_SEQUENCIA_AUTOR      NUMBER(10),
  DS_STS_REQ_CONTENT      VARCHAR2(100 BYTE),
  CD_STS_REPORT_STATUS    VARCHAR2(30 BYTE),
  CD_STS_PROCESSS_STATUS  VARCHAR2(50 BYTE),
  DT_STS_LODGEMENT        DATE,
  CD_OEC_PROCESS_STATUS   VARCHAR2(50 BYTE),
  NM_RESP_FIRST_NAME      VARCHAR2(40 BYTE),
  CD_RESP_MEDICARE_NUM    VARCHAR2(15 BYTE),
  CD_RESP_MEDICARE_REF    VARCHAR2(10 BYTE),
  CD_MEDICARE_STATUS      VARCHAR2(10 BYTE),
  CD_HEALTH_FUND_STATUS   VARCHAR2(10 BYTE),
  CD_STS_TRANSACTION      VARCHAR2(25 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.OEC_PK ON TASY.OEC_CLAIM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.OEC_CLAIM ADD (
  CONSTRAINT OEC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.OEC_CLAIM TO NIVEL_1;

