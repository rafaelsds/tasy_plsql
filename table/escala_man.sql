ALTER TABLE TASY.ESCALA_MAN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MAN CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MAN
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_AVALIACAO               DATE               NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_PESO                    NUMBER(10,3)       NOT NULL,
  QT_ALTURA                  NUMBER(3)          NOT NULL,
  QT_CIRCUN_BRACO            NUMBER(15,2),
  QT_ALTURA_JOELHO           NUMBER(3)          NOT NULL,
  QT_CIRCUN_PANTURRILHA      NUMBER(15,2),
  IE_PERDA_PESO              VARCHAR2(1 BYTE)   NOT NULL,
  IE_SOZINHO                 VARCHAR2(1 BYTE),
  IE_TRES_MEDICAMENTOS       VARCHAR2(1 BYTE),
  IE_STRESS                  VARCHAR2(1 BYTE)   NOT NULL,
  IE_MOBILIDADE              VARCHAR2(1 BYTE)   NOT NULL,
  IE_NEUROLOGICO             VARCHAR2(1 BYTE)   NOT NULL,
  IE_ESCARAS                 VARCHAR2(1 BYTE),
  QT_REFEICAO                VARCHAR2(1 BYTE),
  IE_LACTEO                  VARCHAR2(1 BYTE),
  IE_LEGUME                  VARCHAR2(1 BYTE),
  IE_FRUTA                   VARCHAR2(1 BYTE),
  IE_DECLINEO_INGESTAO       VARCHAR2(1 BYTE)   NOT NULL,
  IE_CARNE                   VARCHAR2(1 BYTE),
  QT_LIQUIDO                 VARCHAR2(1 BYTE),
  IE_FORMA_ALIMENTACAO       VARCHAR2(1 BYTE),
  IE_PAC_PROBL_NUTRICIONAL   VARCHAR2(1 BYTE),
  IE_COMP_IDADE              VARCHAR2(3 BYTE),
  QT_PONTUACAO               NUMBER(5,2),
  NR_HORA                    NUMBER(2),
  QT_PONTUACAO_TRIAGEM       NUMBER(5,2),
  QT_PONTUACAO_AVAL_GLOB     NUMBER(5,2),
  QT_IMC                     NUMBER(4,1),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCAMAN_ATEPACI_FK_I ON TASY.ESCALA_MAN
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCAMAN_EHRREEL_FK_I ON TASY.ESCALA_MAN
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAMAN_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCAMAN_PESFISI_FK_I ON TASY.ESCALA_MAN
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCAMAN_PK ON TASY.ESCALA_MAN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAMAN_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCAMAN_TASASDI_FK_I ON TASY.ESCALA_MAN
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAMAN_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCAMAN_TASASDI_FK2_I ON TASY.ESCALA_MAN
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCAMAN_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_man_atual
before insert or update ON TASY.ESCALA_MAN for each row
declare
qt_imc_w		number(10,2);
qt_selecionado_w	number(10);
begin


--Consiste_Liberacao_Escala(60);


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;

:new.qt_pontuacao		:= 0;
:new.qt_pontuacao_triagem	:= 0;
:new.qt_pontuacao_aval_glob	:= 0;


qt_imc_w	:= dividir(:new.qt_peso,(:new.qt_altura /100) * (:new.qt_altura /100));


if	(qt_imc_w	>=19) and
	(qt_imc_w	<21) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_triagem	:= :new.qt_pontuacao_triagem + 1;
elsif	(qt_imc_w	>=21) and
	(qt_imc_w	< 23) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 2;
	:new.qt_pontuacao_triagem	:= :new.qt_pontuacao_triagem + 2;
elsif	(qt_imc_w	>=23) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 3;
	:new.qt_pontuacao_triagem	:= :new.qt_pontuacao_triagem + 3;
end if;

:new.qt_imc := qt_imc_w;

if	(:new.qt_circun_braco	>=21) and
	(:new.qt_circun_braco	<= 22) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 0.5;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 0.5;

elsif	(:new.qt_circun_braco	> 22) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 1;
end if;

if	(:new.qt_circun_panturrilha	>=31) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 1;
end if;

if	(:new.IE_SOZINHO	= 'S') then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 1;
end if;

if	(:new.IE_STRESS	= 'N') then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 2;
	:new.qt_pontuacao_triagem	:= :new.qt_pontuacao_triagem + 2;
end if;

if	(:new.IE_TRES_MEDICAMENTOS	= 'N') then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 1;
end if;

if	(:new.IE_ESCARAS	= 'N') then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 1;
end if;


if	(:new.IE_FRUTA	= 'S') then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 1;
end if;


qt_selecionado_w	:= 0;

if	(:new.IE_LACTEO	= 'S') then
	qt_selecionado_w	:= qt_selecionado_w + 1;
end if;

if	(:new.IE_LEGUME	= 'S') then
	qt_selecionado_w	:= qt_selecionado_w + 1;
end if;

if	(:new.IE_CARNE	= 'S') then
	qt_selecionado_w	:= qt_selecionado_w + 1;
end if;

if	(qt_selecionado_w	= 2) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 0.5;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 0.5;
elsif	(qt_selecionado_w	= 3) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 1;
end if;

:new.qt_pontuacao		:= :new.qt_pontuacao + :new.IE_PERDA_PESO;
:new.qt_pontuacao_triagem	:= :new.qt_pontuacao_triagem + :new.IE_PERDA_PESO;
:new.qt_pontuacao		:= :new.qt_pontuacao + :new.IE_MOBILIDADE;
:new.qt_pontuacao_triagem	:= :new.qt_pontuacao_triagem + :new.IE_MOBILIDADE;
:new.qt_pontuacao		:= :new.qt_pontuacao + :new.IE_NEUROLOGICO;
:new.qt_pontuacao_triagem	:= :new.qt_pontuacao_triagem + :new.IE_NEUROLOGICO;
:new.qt_pontuacao		:= :new.qt_pontuacao + :new.QT_REFEICAO;
:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + :new.QT_REFEICAO;
:new.qt_pontuacao		:= :new.qt_pontuacao + :new.IE_DECLINEO_INGESTAO;
:new.qt_pontuacao_triagem	:= :new.qt_pontuacao_triagem + :new.IE_DECLINEO_INGESTAO;
:new.qt_pontuacao		:= :new.qt_pontuacao + :new.IE_FORMA_ALIMENTACAO;
:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + :new.IE_FORMA_ALIMENTACAO;
:new.qt_pontuacao		:= :new.qt_pontuacao + :new.IE_PAC_PROBL_NUTRICIONAL;
:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + :new.IE_PAC_PROBL_NUTRICIONAL;

if	(:new.IE_COMP_IDADE	= '1') then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 0.5;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 0.5;
elsif	(:new.IE_COMP_IDADE	= '2') then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 1;
elsif	(:new.IE_COMP_IDADE	= '3') then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 2;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 2;
end if;

if	(:new.QT_LIQUIDO	= 1) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 0.5;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 0.5;
elsif	(:new.qt_liquido	= 2) then
	:new.qt_pontuacao		:= :new.qt_pontuacao + 1;
	:new.qt_pontuacao_aval_glob	:= :new.qt_pontuacao_aval_glob + 1;
end if;

if (:new.qt_pontuacao_triagem >= 12)
 and (:new.IE_SOZINHO is null)
 and (:new.IE_TRES_MEDICAMENTOS is null)
 and (:new.IE_ESCARAS is null)
 and (:new.QT_REFEICAO is null)
 and (:new.IE_FRUTA is null)
 and (:new.QT_LIQUIDO is null)
 and (:new.IE_FORMA_ALIMENTACAO is null)
 and (:new.IE_PAC_PROBL_NUTRICIONAL is null)
 and (:new.IE_COMP_IDADE is null) then

 	:new.QT_PONTUACAO	:= :new.qt_pontuacao_triagem;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.escala_man_pend_atual
after insert or update ON TASY.ESCALA_MAN for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'60');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_MAN_delete
after delete ON TASY.ESCALA_MAN for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '60'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_MAN ADD (
  CONSTRAINT ESCAMAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MAN ADD (
  CONSTRAINT ESCAMAN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCAMAN_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCAMAN_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCAMAN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCAMAN_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_MAN TO NIVEL_1;

