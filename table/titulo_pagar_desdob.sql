ALTER TABLE TASY.TITULO_PAGAR_DESDOB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_DESDOB CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_DESDOB
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  NR_TITULO       NUMBER(10)                    NOT NULL,
  DT_VENCIMENTO   DATE                          NOT NULL,
  VL_TITULO       NUMBER(15,2)                  NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL,
  NR_TITULO_DEST  NUMBER(10),
  VL_SALDO_JUROS  NUMBER(15,2),
  VL_SALDO_MULTA  NUMBER(15,2),
  PR_TITULO       NUMBER(7,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TIPADES_PK ON TASY.TITULO_PAGAR_DESDOB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPADES_TITPAGA_FK_I ON TASY.TITULO_PAGAR_DESDOB
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPADES_TITPAGA2_FK_I ON TASY.TITULO_PAGAR_DESDOB
(NR_TITULO_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIPADES_TITPAGA2_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TITULO_PAGAR_DESDOB ADD (
  CONSTRAINT TIPADES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_DESDOB ADD (
  CONSTRAINT TIPADES_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT TIPADES_TITPAGA2_FK 
 FOREIGN KEY (NR_TITULO_DEST) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO));

GRANT SELECT ON TASY.TITULO_PAGAR_DESDOB TO NIVEL_1;

