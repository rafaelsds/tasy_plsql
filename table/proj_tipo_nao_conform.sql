ALTER TABLE TASY.PROJ_TIPO_NAO_CONFORM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_TIPO_NAO_CONFORM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_TIPO_NAO_CONFORM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TIPO_NC           VARCHAR2(255 BYTE),
  IE_RESP_NC           VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_NC       NUMBER(10)               NOT NULL,
  CD_EXP_TIPO_NC       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROJTPNC_DICEXPR_FK_I ON TASY.PROJ_TIPO_NAO_CONFORM
(CD_EXP_TIPO_NC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROJTPNC_PK ON TASY.PROJ_TIPO_NAO_CONFORM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJTPNC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROJTPNC_PROJCLTPNC_FK_I ON TASY.PROJ_TIPO_NAO_CONFORM
(NR_SEQ_TIPO_NC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJTPNC_PROJCLTPNC_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_TIPO_NAO_CONFORM ADD (
  CONSTRAINT PROJTPNC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_TIPO_NAO_CONFORM ADD (
  CONSTRAINT PROJTPNC_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_TIPO_NC) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT PROJTPNC_PROJCLTPNC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_NC) 
 REFERENCES TASY.PROJ_CLASSIF_TIPO_NC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROJ_TIPO_NAO_CONFORM TO NIVEL_1;

