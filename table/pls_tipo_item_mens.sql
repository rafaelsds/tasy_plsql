ALTER TABLE TASY.PLS_TIPO_ITEM_MENS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TIPO_ITEM_MENS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TIPO_ITEM_MENS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_ITEM         VARCHAR2(2 BYTE),
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  NR_SEQ_TIPO_LANC     NUMBER(10),
  NR_SEQ_ANTERIOR      NUMBER(10),
  CD_MATERIAL          NUMBER(6),
  NR_SEQ_GRUPO_ITEM    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTIME_ESTABEL_FK_I ON TASY.PLS_TIPO_ITEM_MENS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTIME_MATERIA_FK_I ON TASY.PLS_TIPO_ITEM_MENS
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIME_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSTIME_PK ON TASY.PLS_TIPO_ITEM_MENS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIME_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSTIME_PLSGIME_FK_I ON TASY.PLS_TIPO_ITEM_MENS
(NR_SEQ_GRUPO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTIME_PLSTLAD_FK_I ON TASY.PLS_TIPO_ITEM_MENS
(NR_SEQ_TIPO_LANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIME_PLSTLAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTIME_PROCEDI_FK_I ON TASY.PLS_TIPO_ITEM_MENS
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTIME_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_tipo_item_mens_atual
before insert or update ON TASY.PLS_TIPO_ITEM_MENS for each row
declare

begin
if	(:new.cd_procedimento is not null) and
	(:new.cd_material is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(262464);
	/* Mensagem: N�o pode ser criada uma regra com procedimento E material! */
end if;

if	(:new.nr_seq_grupo_item is not null) and
	(:new.ie_tipo_item is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(791557);
	/* Mensagem: N�o pode ser criada uma regra com grupo de itens mensalidade E itens mensalidade! */
end if;

if	(:new.cd_procedimento is null) and
	(:new.cd_material is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(262465);
	/* Mensagem: N�o pode ser criada uma regra sem procedimento ou material! */
end if;

/*aaschlote 26/08/2011*/
if	(:new.cd_procedimento is not null) and
	(:new.ie_origem_proced is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(262468);
	/* Mensagem: Favor informar a origem do procedimento. Utilize o localizador de procedimento para buscar a origem. */
end if;

end;
/


ALTER TABLE TASY.PLS_TIPO_ITEM_MENS ADD (
  CONSTRAINT PLSTIME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TIPO_ITEM_MENS ADD (
  CONSTRAINT PLSTIME_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSTIME_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSTIME_PLSTLAD_FK 
 FOREIGN KEY (NR_SEQ_TIPO_LANC) 
 REFERENCES TASY.PLS_TIPO_LANC_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSTIME_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PLSTIME_PLSGIME_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_ITEM) 
 REFERENCES TASY.PLS_GRUPO_ITEM_MENS (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TIPO_ITEM_MENS TO NIVEL_1;

