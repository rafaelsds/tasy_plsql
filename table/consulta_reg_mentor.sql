ALTER TABLE TASY.CONSULTA_REG_MENTOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONSULTA_REG_MENTOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONSULTA_REG_MENTOR
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_REGRA           NUMBER(10),
  DS_TITULO              VARCHAR2(255 BYTE),
  NM_TABELA_ORIGEM       VARCHAR2(255 BYTE),
  DS_VALOR               VARCHAR2(255 BYTE),
  NR_SEQUENCIA_ORIGEM    NUMBER(10),
  DS_CAMPO               VARCHAR2(255 BYTE),
  DS_UNID_MEDIDA         VARCHAR2(10 BYTE),
  NR_PRESCRICAO          NUMBER(14),
  NR_PROTOCOLO           NUMBER(10),
  NR_SEQ_PEND_PAC        NUMBER(10),
  NR_SEQ_PEND_PAC_ASSOC  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COREME_GQAPEPA_FK_I ON TASY.CONSULTA_REG_MENTOR
(NR_SEQ_PEND_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COREME_GQAPERE_FK_I ON TASY.CONSULTA_REG_MENTOR
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COREME_PK ON TASY.CONSULTA_REG_MENTOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONSULTA_REG_MENTOR ADD (
  CONSTRAINT COREME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONSULTA_REG_MENTOR ADD (
  CONSTRAINT COREME_GQAPERE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.GQA_PENDENCIA_REGRA (NR_SEQUENCIA),
  CONSTRAINT COREME_GQAPEPA_FK 
 FOREIGN KEY (NR_SEQ_PEND_PAC) 
 REFERENCES TASY.GQA_PENDENCIA_PAC (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONSULTA_REG_MENTOR TO NIVEL_1;

