ALTER TABLE TASY.NUT_REGRA_CONS_ACOMP_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_REGRA_CONS_ACOMP_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_REGRA_CONS_ACOMP_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SERVICO       NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA_ACOMP   NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NUTRECI_NUTRECA_FK_I ON TASY.NUT_REGRA_CONS_ACOMP_ITEM
(NR_SEQ_REGRA_ACOMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTRECI_NUTRECA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NUTRECI_NUTSERV_FK_I ON TASY.NUT_REGRA_CONS_ACOMP_ITEM
(NR_SEQ_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTRECI_NUTSERV_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NUTRECI_PK ON TASY.NUT_REGRA_CONS_ACOMP_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NUTRECI_PK
  MONITORING USAGE;


ALTER TABLE TASY.NUT_REGRA_CONS_ACOMP_ITEM ADD (
  CONSTRAINT NUTRECI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NUT_REGRA_CONS_ACOMP_ITEM ADD (
  CONSTRAINT NUTRECI_NUTSERV_FK 
 FOREIGN KEY (NR_SEQ_SERVICO) 
 REFERENCES TASY.NUT_SERVICO (NR_SEQUENCIA),
  CONSTRAINT NUTRECI_NUTRECA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ACOMP) 
 REFERENCES TASY.NUT_REGRA_CONSISTE_ACOMP (NR_SEQUENCIA));

GRANT SELECT ON TASY.NUT_REGRA_CONS_ACOMP_ITEM TO NIVEL_1;

