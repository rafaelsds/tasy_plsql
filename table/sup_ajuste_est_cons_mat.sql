ALTER TABLE TASY.SUP_AJUSTE_EST_CONS_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_AJUSTE_EST_CONS_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_AJUSTE_EST_CONS_MAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AJUSTE        NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(6)                NOT NULL,
  QT_AJUSTE            NUMBER(13,4),
  VL_AJUSTE            NUMBER(15,4),
  IE_FORMA_AJUSTE      VARCHAR2(1 BYTE),
  VL_CUSTO_MEDIO       NUMBER(15,4),
  CD_FORNECEDOR        VARCHAR2(14 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUECMAT_MATERIA_FK_I ON TASY.SUP_AJUSTE_EST_CONS_MAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUECMAT_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUECMAT_PESJURI_FK_I ON TASY.SUP_AJUSTE_EST_CONS_MAT
(CD_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUECMAT_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUECMAT_PK ON TASY.SUP_AJUSTE_EST_CONS_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUECMAT_SUAJECO_FK_I ON TASY.SUP_AJUSTE_EST_CONS_MAT
(NR_SEQ_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUECMAT_SUAJECO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sup_ajuste_est_cons_mat_atual
before insert or update ON TASY.SUP_AJUSTE_EST_CONS_MAT for each row
declare
ie_consignado_w		varchar2(1);
cd_estabelecimento_w	number(4);
ie_valorizacao_consig_w	varchar2(15);

begin
if	(:new.cd_material is not null) then
	begin
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	sup_ajuste_estoque_consumo
	where	nr_sequencia = :new.nr_seq_ajuste;

	select	nvl(max(ie_valorizacao_consig),'N')
	into	ie_valorizacao_consig_w
	from	parametro_estoque
	where	cd_estabelecimento = cd_estabelecimento_w;

	select	ie_consignado
	into	ie_consignado_w
	from	material
	where	cd_material = :new.cd_material;

	if	((ie_consignado_w = 1) or ((ie_consignado_w = 2) and (:new.cd_fornecedor is not null))) and (ie_valorizacao_consig_w <> 'CM') then
		wheb_mensagem_pck.exibir_mensagem_abort(266270);
		--'Neste estabelecimento n�o � apurado o custo m�dio de consignados, desta forma n�o � poss�vel realizar o ajuste para   este item!');
	elsif	(ie_consignado_w = 0) and (:new.cd_fornecedor is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266272);
		--'Para materiais do tipo normal n�o pode ser informado o fornecedor!');
	elsif	(ie_consignado_w = 1) and (:new.cd_fornecedor is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266273);
		--'Para materiais do tipo consignado � obrigat�rio informar o fornecedor!');
	end if;
	end;
end if;
end;
/


ALTER TABLE TASY.SUP_AJUSTE_EST_CONS_MAT ADD (
  CONSTRAINT SUECMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_AJUSTE_EST_CONS_MAT ADD (
  CONSTRAINT SUECMAT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT SUECMAT_SUAJECO_FK 
 FOREIGN KEY (NR_SEQ_AJUSTE) 
 REFERENCES TASY.SUP_AJUSTE_ESTOQUE_CONSUMO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SUECMAT_PESJURI_FK 
 FOREIGN KEY (CD_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.SUP_AJUSTE_EST_CONS_MAT TO NIVEL_1;

