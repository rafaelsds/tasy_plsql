ALTER TABLE TASY.RP_LISTA_ESPERA_MODELO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RP_LISTA_ESPERA_MODELO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RP_LISTA_ESPERA_MODELO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_MODELO             NUMBER(10),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE)   NOT NULL,
  DT_INCLUSAO_LISTA         DATE                NOT NULL,
  DT_LIBERACAO_MEDICO       DATE,
  IE_STATUS                 VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_DEFICIENCIA   NUMBER(10),
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  DT_PEDIDO                 DATE,
  CD_MEDICO_RESP            VARCHAR2(10 BYTE),
  CD_TIPO_AGENDA            NUMBER(10),
  CD_AGENDA                 NUMBER(10),
  CD_PROCEDIMENTO           NUMBER(15),
  IE_ORIGEM_PROCED          NUMBER(10),
  NR_SEQ_PROC_INTERNO       NUMBER(10),
  DT_PREV_INICIO            DATE,
  DT_PREV_FIM               DATE,
  CD_MEDICO_EXEC            VARCHAR2(10 BYTE),
  CD_ESPECIALIDADE          NUMBER(10),
  NR_MINUTO_MANHA           NUMBER(10),
  NR_MINUTO_TARDE           NUMBER(10),
  NR_MINUTO_LIVRE           NUMBER(10),
  NR_SESSOES                NUMBER(10),
  IE_FREQUENCIA             VARCHAR2(2 BYTE),
  CD_CONVENIO               NUMBER(5),
  CD_PROC_SERV              NUMBER(15),
  IE_ORIGEM_PROCED_SERV     NUMBER(10),
  NR_SEQ_PROC_INTERNO_SERV  NUMBER(10),
  IE_EXCLUSIVO              VARCHAR2(5 BYTE),
  NR_SEQ_CLASSIF            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RPLIESM_CONVENI_FK_I ON TASY.RP_LISTA_ESPERA_MODELO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIESM_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPLIESM_ESPMEDI_FK_I ON TASY.RP_LISTA_ESPERA_MODELO
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIESM_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPLIESM_ESTABEL_FK_I ON TASY.RP_LISTA_ESPERA_MODELO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIESM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPLIESM_PESFISI_FK_I ON TASY.RP_LISTA_ESPERA_MODELO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RPLIESM_PESFISI_FK2_I ON TASY.RP_LISTA_ESPERA_MODELO
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RPLIESM_PESFISI_FK3_I ON TASY.RP_LISTA_ESPERA_MODELO
(CD_MEDICO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RPLIESM_PK ON TASY.RP_LISTA_ESPERA_MODELO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIESM_PK
  MONITORING USAGE;


CREATE INDEX TASY.RPLIESM_RPCLASSPED_FK_I ON TASY.RP_LISTA_ESPERA_MODELO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIESM_RPCLASSPED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPLIESM_RPMODAG_FK_I ON TASY.RP_LISTA_ESPERA_MODELO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIESM_RPMODAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPLIESM_TIPDEFI_FK_I ON TASY.RP_LISTA_ESPERA_MODELO
(NR_SEQ_TIPO_DEFICIENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPLIESM_TIPDEFI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.RP_LISTA_ESPERA_MODELO_INSERT
After Insert ON TASY.RP_LISTA_ESPERA_MODELO for each row
declare

qt_vagas_restantes_w	number(10);
qt_lista_espera_w	number(10);

begin
--rp_insert_log_movto_modelo(:new.nr_seq_modelo,wheb_usuario_pck.get_nm_usuario);
if	(:new.nr_seq_modelo is not null) then
	qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:new.nr_seq_modelo);
	qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:new.nr_seq_modelo) + 1;

	insert into rp_log_movto_modelo (
		NR_SEQUENCIA,
		NR_SEQ_MODELO,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DT_LOG,
		QT_VAGAS_RESTANTES,
		QT_LISTA_ESPERA        )
	values(	rp_log_movto_modelo_seq.nextval,
		:new.nr_seq_modelo,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		qt_vagas_restantes_w,
		qt_lista_espera_w);
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.RP_LISTA_ESPERA_MODELO_DELETE
before delete ON TASY.RP_LISTA_ESPERA_MODELO for each row
declare
qt_vagas_restantes_w	number(10);
qt_lista_espera_w	number(10);
begin
--rp_insert_log_movto_modelo(:old.nr_seq_modelo,wheb_usuario_pck.get_nm_usuario);
if	(:old.nr_seq_modelo is not null) then
	qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:old.nr_seq_modelo);
	qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:old.nr_seq_modelo) - 1;

	insert into rp_log_movto_modelo (
		NR_SEQUENCIA,
		NR_SEQ_MODELO,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DT_LOG,
		QT_VAGAS_RESTANTES,
		QT_LISTA_ESPERA        )
	values(	rp_log_movto_modelo_seq.nextval,
		:old.nr_seq_modelo,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		qt_vagas_restantes_w,
		qt_lista_espera_w);
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.RP_LISTA_ESPERA_MODELO_UPDATE
After Update ON TASY.RP_LISTA_ESPERA_MODELO for each row
declare
qt_vagas_restantes_w	number(10);
qt_lista_espera_w	number(10);
qt_valida_log_existe_w	number(10);
nr_seq_log_w		number(10);
begin
select	count(*)
into	qt_valida_log_existe_w
from	rp_log_movto_modelo
where   nr_seq_modelo = :new.nr_seq_modelo
and	dt_atualizacao = sysdate;

if	(qt_valida_log_existe_w = 0) then
	if	(:new.NR_SEQ_MODELO is not null) and
		((((:old.NR_SEQ_MODELO <> :new.NR_SEQ_MODELO) or
		(:old.NR_SEQ_MODELO is null)) and
		(:new.ie_status = 'A')) or
		((:old.ie_status <> :new.ie_status) and
		(:new.ie_status <> 'T') and
		(:new.ie_status <> 'C')))
		then
		--rp_insert_log_movto_modelo(:new.nr_seq_modelo,wheb_usuario_pck.get_nm_usuario);
		qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:new.nr_seq_modelo);
		qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:new.nr_seq_modelo) + 1;

		insert into rp_log_movto_modelo (
			NR_SEQUENCIA,
			NR_SEQ_MODELO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO_NREC,
			DT_LOG,
			QT_VAGAS_RESTANTES,
			QT_LISTA_ESPERA        )
		values(	rp_log_movto_modelo_seq.nextval,
			:new.nr_seq_modelo,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			qt_vagas_restantes_w,
			qt_lista_espera_w);
	elsif  	(:new.NR_SEQ_MODELO is not null) and
		((:old.ie_status <> :new.ie_status) and
		((:new.ie_status = 'C') or
		(:new.ie_status = 'T')) and
		(:old.ie_status not in ('C','T'))) then

		qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:old.nr_seq_modelo);
		qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:old.nr_seq_modelo) - 1;

		insert into rp_log_movto_modelo (
			NR_SEQUENCIA,
			NR_SEQ_MODELO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO_NREC,
			DT_LOG,
			QT_VAGAS_RESTANTES,
			QT_LISTA_ESPERA        )
		values(	rp_log_movto_modelo_seq.nextval,
			:old.nr_seq_modelo,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			qt_vagas_restantes_w,
			qt_lista_espera_w);

	end if;

	if	(:new.NR_SEQ_MODELO is not null) and
		(:old.NR_SEQ_MODELO <> :new.NR_SEQ_MODELO) and
		((((:old.NR_SEQ_MODELO <> :new.NR_SEQ_MODELO) or
		(:old.NR_SEQ_MODELO is null)) and
		(:new.ie_status = 'A')) or
		((:old.ie_status <> :new.ie_status) and
		(:new.ie_status <> 'T'))) then

		qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:old.nr_seq_modelo);
		qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:old.nr_seq_modelo) - 1;

		insert into rp_log_movto_modelo (
			NR_SEQUENCIA,
			NR_SEQ_MODELO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_ATUALIZACAO_NREC,
			NM_USUARIO_NREC,
			DT_LOG,
			QT_VAGAS_RESTANTES,
			QT_LISTA_ESPERA        )
		values(	rp_log_movto_modelo_seq.nextval,
			:old.nr_seq_modelo,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			wheb_usuario_pck.get_nm_usuario,
			sysdate,
			qt_vagas_restantes_w,
			qt_lista_espera_w);

	end if;
else
	select	max(nr_sequencia)
	into	nr_seq_log_w
	from	rp_log_movto_modelo;

	qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:old.nr_seq_modelo) - 1;

	update	rp_log_movto_modelo
	set	qt_lista_espera = qt_lista_espera_w
	where	nr_sequencia = nr_seq_log_w
	and	nr_seq_modelo = :old.nr_seq_modelo;

end if;
end;
/


ALTER TABLE TASY.RP_LISTA_ESPERA_MODELO ADD (
  CONSTRAINT RPLIESM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RP_LISTA_ESPERA_MODELO ADD (
  CONSTRAINT RPLIESM_PESFISI_FK3 
 FOREIGN KEY (CD_MEDICO_EXEC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RPLIESM_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RPLIESM_RPMODAG_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.RP_MODELO_AGENDAMENTO (NR_SEQUENCIA),
  CONSTRAINT RPLIESM_TIPDEFI_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DEFICIENCIA) 
 REFERENCES TASY.TIPO_DEFICIENCIA (NR_SEQUENCIA),
  CONSTRAINT RPLIESM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RPLIESM_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT RPLIESM_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT RPLIESM_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT RPLIESM_RPCLASSPED_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.RP_CLASSIFICACAO_PEDIDO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RP_LISTA_ESPERA_MODELO TO NIVEL_1;

