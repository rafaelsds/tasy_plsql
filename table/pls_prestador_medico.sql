ALTER TABLE TASY.PLS_PRESTADOR_MEDICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_MEDICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_MEDICO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR       NUMBER(10)             NOT NULL,
  CD_MEDICO              VARCHAR2(10 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  IE_GUIA_MEDICO         VARCHAR2(1 BYTE)       NOT NULL,
  IE_TIPO_COMPLEMENTO    NUMBER(2),
  IE_TIPO_ENDERECO       VARCHAR2(4 BYTE),
  IE_TIPO_VINCULO        VARCHAR2(2 BYTE)       NOT NULL,
  NR_SEQ_TIPO_PRESTADOR  NUMBER(10),
  NR_SEQ_PRESTADOR_PGTO  NUMBER(10),
  DT_INCLUSAO            DATE,
  DT_EXCLUSAO            DATE,
  NR_SEQ_PRESTADOR_REF   NUMBER(10),
  NM_BUSCA_GUIA_MEDICO   VARCHAR2(2000 BYTE),
  NR_REFERENCIA_END      NUMBER(2),
  IE_RESIDENCIA_SAUDE    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPRME_MEDICO_FK_I ON TASY.PLS_PRESTADOR_MEDICO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPRME_PK ON TASY.PLS_PRESTADOR_MEDICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRME_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_MEDICO
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPRME_PLSPRES_FK2_I ON TASY.PLS_PRESTADOR_MEDICO
(NR_SEQ_PRESTADOR_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRME_PLSPRES_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRME_PLSPRES_FK3_I ON TASY.PLS_PRESTADOR_MEDICO
(NR_SEQ_PRESTADOR_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRME_PLSPRES_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSPRME_PLSTIPR_FK_I ON TASY.PLS_PRESTADOR_MEDICO
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPRME_PLSTIPR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_prestador_medico_delete
after delete ON TASY.PLS_PRESTADOR_MEDICO for each row
declare

qt_rotina_w	number(10) := 0;
ds_chave_w	varchar2(255);
nm_usuario_w	varchar2(15);

begin
begin
nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

ds_chave_w :=	substr('NR_SEQUENCIA='||:old.nr_sequencia||',NR_SEQ_PRESTADOR='||:old.nr_seq_prestador,1,255);

select	count(1)
into	qt_rotina_w
from 	v$session
where	audsid	= (select userenv('sessionid') from dual)
and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual))
and	action like 'PLS_COPIA_PARAMETRO_PRESTADOR%';

if	(qt_rotina_w > 0) then
	ds_chave_w := substr('A��o=C�pia de par�metros do prestador,Op��o=Excluir registros existentes,'||ds_chave_w,1,255);
else
	ds_chave_w := substr('A��o=Exclus�o,'||ds_chave_w,1,255);
end if;

insert into log_exclusao
	(ds_chave,
	dt_atualizacao,
	nm_tabela,
	nm_usuario)
values	(ds_chave_w,
	sysdate,
	'PLS_PRESTADOR_MEDICO',
	nm_usuario_w);

exception
when others then
	null;
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_PRESTADOR_MEDICO_tp  after update ON TASY.PLS_PRESTADOR_MEDICO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_MEDICO,1,500);gravar_log_alteracao(substr(:old.CD_MEDICO,1,4000),substr(:new.CD_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO',ie_log_w,ds_w,'PLS_PRESTADOR_MEDICO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_PRESTADOR_MEDICO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_PRESTADOR_MEDICO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_PRESTADOR_MEDICO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_VINCULO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_VINCULO,1,4000),substr(:new.IE_TIPO_VINCULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_VINCULO',ie_log_w,ds_w,'PLS_PRESTADOR_MEDICO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_GUIA_MEDICO,1,500);gravar_log_alteracao(substr(:old.IE_GUIA_MEDICO,1,4000),substr(:new.IE_GUIA_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GUIA_MEDICO',ie_log_w,ds_w,'PLS_PRESTADOR_MEDICO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_COMPLEMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_COMPLEMENTO,1,4000),substr(:new.IE_TIPO_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPLEMENTO',ie_log_w,ds_w,'PLS_PRESTADOR_MEDICO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_ENDERECO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ENDERECO,1,4000),substr(:new.IE_TIPO_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ENDERECO',ie_log_w,ds_w,'PLS_PRESTADOR_MEDICO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_excl_end_medico_excluido
before delete ON TASY.PLS_PRESTADOR_MEDICO for each row
declare

begin

delete from PLS_ENDERECO_GUIA_MEDICO a
where	a.nr_seq_prestador	= :old.nr_seq_prestador
and	a.cd_pessoa_fisica	= :old.cd_medico;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_excl_espec_medico_excluido
before delete ON TASY.PLS_PRESTADOR_MEDICO for each row
declare

begin

delete from PLS_PRESTADOR_MED_ESPEC a
where	a.nr_seq_prestador	= :old.nr_seq_prestador
and	a.cd_pessoa_fisica	= :old.cd_medico;

end;
/


CREATE OR REPLACE TRIGGER TASY.pls_prestador_medico_atual
before insert or update ON TASY.PLS_PRESTADOR_MEDICO for each row
declare

nm_medico_w			pessoa_fisica.nm_pessoa_fisica%type;
nm_guerra_w			medico.nm_guerra%type;

begin
if	(:new.cd_medico is not null) and
	(((:old.nm_busca_guia_medico is null) and
	 (:new.nm_busca_guia_medico is null)) or
	 (:new.cd_medico <> :old.cd_medico)) then
	select	max(a.nm_pessoa_fisica),
		max(b.nm_guerra)
	into	nm_medico_w,
		nm_guerra_w
	from	pessoa_fisica a,
		medico b
	where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica(+)
	and	a.cd_pessoa_fisica	= :new.cd_medico;

	:new.nm_busca_guia_medico := substr(pls_obter_nome_busca_guia(	null, nm_medico_w, nm_guerra_w, null, null),1,2000);
end if;

end;
/


ALTER TABLE TASY.PLS_PRESTADOR_MEDICO ADD (
  CONSTRAINT PLSPRME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_MEDICO ADD (
  CONSTRAINT PLSPRME_PLSPRES_FK3 
 FOREIGN KEY (NR_SEQ_PRESTADOR_REF) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRME_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRME_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PLSPRME_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPRME_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PRESTADOR_PGTO) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_MEDICO TO NIVEL_1;

