ALTER TABLE TASY.SOLIC_COMPRA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SOLIC_COMPRA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.SOLIC_COMPRA_ITEM
(
  NR_SOLIC_COMPRA           NUMBER(10)          NOT NULL,
  NR_ITEM_SOLIC_COMPRA      NUMBER(5)           NOT NULL,
  CD_MATERIAL               NUMBER(6)           NOT NULL,
  CD_UNIDADE_MEDIDA_COMPRA  VARCHAR2(30 BYTE)   NOT NULL,
  QT_MATERIAL               NUMBER(13,4)        NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DS_MATERIAL_DIRETO        VARCHAR2(255 BYTE),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  NR_COT_COMPRA             NUMBER(10),
  NR_ITEM_COT_COMPRA        NUMBER(5),
  CD_MOTIVO_BAIXA           NUMBER(5),
  DT_BAIXA                  DATE,
  DT_SOLIC_ITEM             DATE,
  NR_SEQ_APROVACAO          NUMBER(10),
  DT_AUTORIZACAO            DATE,
  VL_UNIT_PREVISTO          NUMBER(17,4),
  IE_GERACAO                VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_PROJ_REC           NUMBER(10),
  DT_REPROVACAO             DATE,
  NR_SEQ_MOTIVO_CANCEL      NUMBER(10),
  IE_FORMA_EXPORTAR         VARCHAR2(1 BYTE),
  NR_SOLIC_COMPRA_REF       NUMBER(10),
  NR_ITEM_SOLIC_COMPRA_REF  NUMBER(5),
  CD_CONTA_CONTABIL         VARCHAR2(20 BYTE),
  DS_OBSERVACAO_INT         VARCHAR2(255 BYTE),
  NR_CONTRATO               NUMBER(10),
  IE_BULA                   VARCHAR2(1 BYTE),
  IE_AMOSTRA                VARCHAR2(1 BYTE),
  IE_CATALOGO               VARCHAR2(1 BYTE),
  NR_SEQ_ORC_ITEM_GPI       NUMBER(10),
  QT_CONV_COMPRA_EST_ORIG   NUMBER(13,4),
  NR_SEQ_MARCA              NUMBER(10),
  NR_SEQ_REGRA_CONTRATO     NUMBER(10),
  QT_SALDO_DISP_ESTOQUE     NUMBER(13,4),
  QT_CONSUMO_MENSAL         NUMBER(15,4),
  DT_DESDOBR_APROV          DATE,
  NR_SEQ_LOTE_FORNEC        NUMBER(10),
  NR_SEQ_LICITACAO          NUMBER(10),
  NR_SEQ_LIC_ITEM           NUMBER(10),
  IE_MOTIVO_REPROVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA_REPROV   VARCHAR2(255 BYTE),
  VL_CUSTO_TOTAL            NUMBER(17,4),
  VL_ESTOQUE_TOTAL          NUMBER(17,4),
  NR_REQUISICAO             NUMBER(10),
  NR_SEQ_ITEM_REQUISICAO    NUMBER(5),
  NR_ORDEM_COMPRA_ORIG      NUMBER(10),
  CD_PERFIL_APROV           NUMBER(5),
  NR_SEQ_CRITERIO_RATEIO    NUMBER(10),
  DS_JUSTIF_DIVER           VARCHAR2(4000 BYTE),
  QT_DIAS_ESTOQUE           NUMBER(13,4),
  CD_CNPJ                   VARCHAR2(14 BYTE),
  IE_CANCELAMENTO_INTEGR    VARCHAR2(1 BYTE),
  NR_SEQ_NORMA_COMPRA       NUMBER(10),
  NR_SEQ_PROC_APROV         NUMBER(10),
  IE_SERVICO_REALIZADO      VARCHAR2(15 BYTE),
  DS_MARCAS_ACEITAVEIS      VARCHAR2(2000 BYTE),
  VL_ORCADO_GPI             NUMBER(17,4),
  DT_SOLIC_ITEM_ANT         DATE,
  NR_SEQ_JUSTIF             NUMBER(10),
  NR_SEQ_ORDEM_SERV         NUMBER(10),
  NR_SEQ_CONTA_FINANC       NUMBER(10),
  QT_AMOSTRA                NUMBER(13,4),
  NR_SEQ_REG_PCS            NUMBER(10),
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_REG_COMPRA         NUMBER(10),
  NR_SEQ_OP_COMP_OPM        NUMBER(10),
  NR_ITEM_OCI_ORIG          NUMBER(5),
  QT_SALDO_REG_PRECO        NUMBER(13,4),
  QT_MATERIAL_CANCELADO     NUMBER(13,4)        DEFAULT null,
  NR_SEQ_ITEM_PROJ_REC      NUMBER(10),
  DS_STACK                  VARCHAR2(2000 BYTE),
  IE_ENVIADO_INTEGR         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          22M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SOCOITE_COCOITE_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_COT_COMPRA, NR_ITEM_COT_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOCOITE_MATERIA_FK_I ON TASY.SOLIC_COMPRA_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SOCOITE_PK ON TASY.SOLIC_COMPRA_ITEM
(NR_SOLIC_COMPRA, NR_ITEM_SOLIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOCOITE_SOLCOMP_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SOLIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOCOITE_UNIMEDI_FK_I ON TASY.SOLIC_COMPRA_ITEM
(CD_UNIDADE_MEDIDA_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOLCOIT_CONCONT_FK_I ON TASY.SOLIC_COMPRA_ITEM
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOIT_CONFINA_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOLCOIT_CONRENF_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_REGRA_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOLCOIT_CONTRAT_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOLCOIT_CRITRAT_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_CRITERIO_RATEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_CRITRAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOIT_GPIORIT_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_ORC_ITEM_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_GPIORIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOIT_ITEREMA_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_REQUISICAO, NR_SEQ_ITEM_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_ITEREMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOIT_JUSTISC_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_JUSTIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_JUSTISC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOIT_MANORSE_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_MANORSE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOIT_MATLOFO_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_MATLOFO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOIT_ORCOITE_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_ORDEM_COMPRA_ORIG, NR_ITEM_OCI_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOLCOIT_PCSREAN_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_REG_PCS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOLCOIT_PERFIL_FK_I ON TASY.SOLIC_COMPRA_ITEM
(CD_PERFIL_APROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOIT_PROAPCO_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_APROVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOLCOIT_PRORECU_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_PRORECU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOIT_PROREIT_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_ITEM_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SOLCOIT_REGLIIT_FK_I ON TASY.SOLIC_COMPRA_ITEM
(NR_SEQ_LICITACAO, NR_SEQ_LIC_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SOLCOIT_REGLIIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SOLCOMP_MATERIA_FK_I ON TASY.SOLIC_COMPRA_ITEM
(CD_MATERIAL, DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.solic_compra_item_insert
BEFORE INSERT ON TASY.SOLIC_COMPRA_ITEM for each row
declare

ds_descricao_w			varchar2(255);
cd_estabelecimento_w		number(4);
cd_comprador_resp_w		varchar2(10);
cd_comprador_resp_ww		varchar2(10);
dt_solicitacao_compra_w		date;
vl_custo_total_w		number(17,4);
vl_estoque_total_w		number(17,4);
cd_perfil_ativo_w		number(10);
ie_copia_comprador_w		varchar2(1);
nm_usuario_w			varchar2(15);
qt_registros_w			number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

	:new.nm_usuario_nrec  	:= nvl(:new.nm_usuario_nrec, :new.nm_usuario);
	:new.dt_atualizacao_nrec	:= nvl(:new.dt_atualizacao_nrec, :new.dt_atualizacao);

	if (INSERTING)then
		:new.ds_stack := substr(dbms_utility.format_call_stack,1,2000);
	end if;


	select	cd_estabelecimento,
		cd_comprador_resp,
		dt_solicitacao_compra,
		nm_usuario
	into	cd_estabelecimento_w,
		cd_comprador_resp_w,
		dt_solicitacao_compra_w,
		nm_usuario_w
	from	solic_compra
	where	nr_solic_compra = :new.nr_solic_compra;

	cd_perfil_ativo_w := obter_perfil_ativo;

	ie_copia_comprador_w :=	substr(nvl(obter_valor_param_usuario(913, 238, cd_perfil_ativo_w, nm_usuario_w, cd_estabelecimento_w),'N'),1,1);

	select	nvl(max(substr(obter_desc_mat_compras(:new.cd_material, cd_estabelecimento_w),1,255)),'')
	into	ds_descricao_w
	from	regra_desc_material_compra;
	if	(ds_descricao_w is not null) and
		(:new.ds_material_direto is null) then
		:new.ds_material_direto	:= substr(ds_descricao_w || ' ' || replace(:new.ds_material_direto, ds_descricao_w || ' ', '') ,1,255);
	end if;

	if	((cd_comprador_resp_w is null) or
		(cd_comprador_resp_w = '')) and
		(ie_copia_comprador_w = 'S') then

		select	obter_comprador_material(:new.cd_material, ie_urgente, cd_estabelecimento_w)
		into	cd_comprador_resp_ww
		from	solic_compra
		where	nr_solic_compra = :new.nr_solic_compra;

		select	count(*)
		into	qt_registros_w
		from	comprador
		where	cd_pessoa_fisica = cd_comprador_resp_ww
		and	cd_estabelecimento = cd_estabelecimento_w
		and	ie_situacao = 'A';


		if	(qt_registros_w > 0) then
			/*Utilizado na trigger SOLIC_COMPRA_UPDATE, devido ao problema de tabela mutante na solic_compra_item*/
			compras_pck.set_is_sci_insert('S');
			compras_pck.set_nr_seq_proj_rec(:new.nr_seq_proj_rec);

			update	solic_compra
			set	cd_comprador_resp = cd_comprador_resp_ww
			where	nr_solic_compra = :new.nr_solic_compra;
		end if;
	end if;

end if;

compras_pck.set_nr_seq_proj_rec(null);
compras_pck.set_is_sci_insert('N');
END;
/


CREATE OR REPLACE TRIGGER TASY.solic_compra_item_afeterinsert
AFTER INSERT ON TASY.SOLIC_COMPRA_ITEM for each row
declare
ds_compl_item_w		gpi_orc_item.ds_compl_item%type;


begin

if	(:new.nr_seq_orc_item_gpi > 0) then

	select	ds_compl_item
	into	ds_compl_item_w
	from	gpi_orc_item
	where	nr_sequencia = :new.nr_seq_orc_item_gpi;

	if	(ds_compl_item_w is not null) then

		insert into solic_compra_item_detalhe(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_solic_compra,
			nr_item_solic_compra,
			ds_detalhe)
		values(	solic_compra_item_detalhe_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario_nrec,
			:new.nr_solic_compra,
			:new.nr_item_solic_compra,
			ds_compl_item_w);
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.solic_compra_item_atual
before update ON TASY.SOLIC_COMPRA_ITEM for each row
declare

cd_estabelecimento_w	number(4);
cd_comprador_resp_w	varchar2(10);
cd_perfil_w		number(5);
cd_operacao_estoque_w	requisicao_material.cd_operacao_estoque%type;
cd_motivo_baixa_w		sup_motivo_baixa_req.nr_sequencia%type;
ds_erro_w		varchar2(2000);
qt_pendentes_w		number(10);
nr_seq_motivo_cancel_w	solic_compra.nr_seq_motivo_cancel %type;
cd_comprador_w		comprador.cd_pessoa_fisica%type;
qt_registros_w		number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

	if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
	end if;

	select	cd_estabelecimento,
		cd_comprador_resp,
		nr_seq_motivo_cancel
	into	cd_estabelecimento_w,
		cd_comprador_resp_w,
		nr_seq_motivo_cancel_w
	from	solic_compra
	where	nr_solic_compra = :new.nr_solic_compra;

	if	(cd_comprador_resp_w is null) or
		(cd_comprador_resp_w = '') then

		select	obter_comprador_material(:new.cd_material, ie_urgente, cd_estabelecimento_w)
		into	cd_comprador_w
		from	solic_compra
		where	nr_solic_compra = :new.nr_solic_compra;

		select	count(*)
		into	qt_registros_w
		from	comprador
		where	cd_estabelecimento = cd_estabelecimento_w
		and	cd_pessoa_fisica = cd_comprador_w;

		if	(qt_registros_w > 0) then
			compras_pck.set_is_sci_insert('S');
			compras_pck.set_nr_seq_proj_rec(:new.nr_seq_proj_rec);

			update	solic_compra
			set	cd_comprador_resp = cd_comprador_w
			where	nr_solic_compra = :new.nr_solic_compra;
		end if;
	end if;

	cd_perfil_w	:= nvl(obter_perfil_ativo,0);

	if	(:old.dt_autorizacao is null) and
		(:new.dt_autorizacao is not null) and
		(cd_perfil_w > 0) then
		:new.cd_perfil_aprov := cd_perfil_w;
	end if;

	if	(:old.dt_autorizacao is not null) and
		(:new.dt_autorizacao is null) then
		:new.cd_perfil_aprov := '';
	end if;

end if;

compras_pck.set_nr_seq_proj_rec(null);
compras_pck.set_is_sci_insert('N');

end;
/


CREATE OR REPLACE TRIGGER TASY.solic_compra_item_delete
BEFORE DELETE ON TASY.SOLIC_COMPRA_ITEM for each row
declare

qt_registro_w		number(10);

begin


select	count(*)
into	qt_registro_w
from	far_registro_falta
where	nr_solic_compra = :old.nr_solic_compra
and	nr_item_solic_compra = :old.nr_item_solic_compra;

if	(qt_registro_w > 0) then

	update	far_registro_falta
	set	nr_solic_compra = null,
		nr_item_solic_compra = null
	where	nr_solic_compra = :old.nr_solic_compra
	and	nr_item_solic_compra = :old.nr_item_solic_compra;
end if;

select	count(*)
into	qt_registro_w
from	processo_aprov_compra
where	nr_sequencia = :old.NR_SEQ_APROVACAO
and	nvl(ie_aprov_reprov,'P') <> 'B';

if	(qt_registro_w = 0) then

	update	processo_aprov_compra
	set	IE_APROV_REPROV = 'B'
	where	nr_sequencia = :old.NR_SEQ_APROVACAO;

	delete from w_processo_aprov_compra
	where	nr_sequencia = :old.NR_SEQ_APROVACAO;

end if;

END;
/


ALTER TABLE TASY.SOLIC_COMPRA_ITEM ADD (
  CONSTRAINT SOCOITE_PK
 PRIMARY KEY
 (NR_SOLIC_COMPRA, NR_ITEM_SOLIC_COMPRA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          5M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SOLIC_COMPRA_ITEM ADD (
  CONSTRAINT SOLCOIT_PROREIT_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO_ITEM (NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_PCSREAN_FK 
 FOREIGN KEY (NR_SEQ_REG_PCS) 
 REFERENCES TASY.PCS_REG_ANALISE (NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_ORCOITE_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA_ORIG, NR_ITEM_OCI_ORIG) 
 REFERENCES TASY.ORDEM_COMPRA_ITEM (NR_ORDEM_COMPRA,NR_ITEM_OCI),
  CONSTRAINT SOLCOIT_JUSTISC_FK 
 FOREIGN KEY (NR_SEQ_JUSTIF) 
 REFERENCES TASY.JUSTIF_ITEM_SOLIC_COMPRA (NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_CONFINA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT SOCOITE_SOLCOMP_FK 
 FOREIGN KEY (NR_SOLIC_COMPRA) 
 REFERENCES TASY.SOLIC_COMPRA (NR_SOLIC_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT SOCOITE_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_COMPRA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT SOLCOIT_PROAPCO_FK 
 FOREIGN KEY (NR_SEQ_APROVACAO) 
 REFERENCES TASY.PROCESSO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT SOCOITE_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT SOLCOIT_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT SOLCOIT_CONTRAT_FK 
 FOREIGN KEY (NR_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_GPIORIT_FK 
 FOREIGN KEY (NR_SEQ_ORC_ITEM_GPI) 
 REFERENCES TASY.GPI_ORC_ITEM (NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_CONRENF_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONTRATO) 
 REFERENCES TASY.CONTRATO_REGRA_NF (NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNEC) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_REGLIIT_FK 
 FOREIGN KEY (NR_SEQ_LICITACAO, NR_SEQ_LIC_ITEM) 
 REFERENCES TASY.REG_LIC_ITEM (NR_SEQ_LICITACAO,NR_SEQ_LIC_ITEM),
  CONSTRAINT SOLCOIT_ITEREMA_FK 
 FOREIGN KEY (NR_REQUISICAO, NR_SEQ_ITEM_REQUISICAO) 
 REFERENCES TASY.ITEM_REQUISICAO_MATERIAL (NR_REQUISICAO,NR_SEQUENCIA),
  CONSTRAINT SOLCOIT_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_APROV) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT SOLCOIT_CRITRAT_FK 
 FOREIGN KEY (NR_SEQ_CRITERIO_RATEIO) 
 REFERENCES TASY.CTB_CRITERIO_RATEIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SOLIC_COMPRA_ITEM TO NIVEL_1;

