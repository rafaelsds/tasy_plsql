ALTER TABLE TASY.DAA_MED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DAA_MED CASCADE CONSTRAINTS;

CREATE TABLE TASY.DAA_MED
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  KEY_DAA              VARCHAR2(3 BYTE),
  ABKUERZUNG           VARCHAR2(5 BYTE),
  NAME                 VARCHAR2(60 BYTE),
  NR_SEQ_IMPORT        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DAAMED_ADAIMP_FK_I ON TASY.DAA_MED
(NR_SEQ_IMPORT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DAAMED_I ON TASY.DAA_MED
(KEY_DAA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DAAMED_PK ON TASY.DAA_MED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DAA_MED ADD (
  CONSTRAINT DAAMED_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DAA_MED ADD (
  CONSTRAINT DAAMED_ADAIMP_FK 
 FOREIGN KEY (NR_SEQ_IMPORT) 
 REFERENCES TASY.ABDAMED_IMPORT (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DAA_MED TO NIVEL_1;

