ALTER TABLE TASY.ROP_MOVTO_ROUPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_MOVTO_ROUPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_MOVTO_ROUPA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  NR_SEQ_LOTE              NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_ROUPA             NUMBER(10)           NOT NULL,
  DT_ORIGEM                DATE                 NOT NULL,
  DT_DESTINO               DATE,
  IE_OPER_CORRETA          VARCHAR2(1 BYTE)     NOT NULL,
  QT_PESO                  NUMBER(13,4),
  QT_ROUPA                 NUMBER(13,4),
  DT_CONF_SAIDA            DATE,
  NM_USUARIO_CONF_SAIDA    VARCHAR2(15 BYTE),
  DT_CONF_CHEGADA          DATE,
  NM_USUARIO_CONF_CHEGADA  VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_FALTA      NUMBER(10),
  DS_JUSTIF_FALTA          VARCHAR2(4000 BYTE),
  IE_RESSUPRIMENTO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ROPMORO_ESTABEL_FK_I ON TASY.ROP_MOVTO_ROUPA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPMORO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPMORO_I ON TASY.ROP_MOVTO_ROUPA
(NR_SEQ_ROUPA, NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ROPMORO_PK ON TASY.ROP_MOVTO_ROUPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPMORO_ROMOFPE_FK_I ON TASY.ROP_MOVTO_ROUPA
(NR_SEQ_MOTIVO_FALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPMORO_ROMOFPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPMORO_ROPLOMO_FK_I ON TASY.ROP_MOVTO_ROUPA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPMORO_ROROUPA_FK_I ON TASY.ROP_MOVTO_ROUPA
(NR_SEQ_ROUPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPMORO_ROROUPA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ROP_MOVTO_ROUPA_tp  after update ON TASY.ROP_MOVTO_ROUPA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ORIGEM,1,4000),substr(:new.DT_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'DT_ORIGEM',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_DESTINO,1,4000),substr(:new.DT_DESTINO,1,4000),:new.nm_usuario,nr_seq_w,'DT_DESTINO',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPER_CORRETA,1,4000),substr(:new.IE_OPER_CORRETA,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPER_CORRETA',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ROUPA,1,4000),substr(:new.NR_SEQ_ROUPA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ROUPA',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PESO,1,4000),substr(:new.QT_PESO,1,4000),:new.nm_usuario,nr_seq_w,'QT_PESO',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_CONF_CHEGADA,1,4000),substr(:new.NM_USUARIO_CONF_CHEGADA,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_CONF_CHEGADA',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_FALTA,1,4000),substr(:new.NR_SEQ_MOTIVO_FALTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_FALTA',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_JUSTIF_FALTA,1,4000),substr(:new.DS_JUSTIF_FALTA,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIF_FALTA',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CONF_SAIDA,1,4000),substr(:new.DT_CONF_SAIDA,1,4000),:new.nm_usuario,nr_seq_w,'DT_CONF_SAIDA',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_CONF_SAIDA,1,4000),substr(:new.NM_USUARIO_CONF_SAIDA,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_CONF_SAIDA',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CONF_CHEGADA,1,4000),substr(:new.DT_CONF_CHEGADA,1,4000),:new.nm_usuario,nr_seq_w,'DT_CONF_CHEGADA',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ROUPA,1,4000),substr(:new.QT_ROUPA,1,4000),:new.nm_usuario,nr_seq_w,'QT_ROUPA',ie_log_w,ds_w,'ROP_MOVTO_ROUPA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ROP_MOVTO_ROUPA ADD (
  CONSTRAINT ROPMORO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_MOVTO_ROUPA ADD (
  CONSTRAINT ROPMORO_ROPLOMO_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.ROP_LOTE_MOVTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ROPMORO_ROROUPA_FK 
 FOREIGN KEY (NR_SEQ_ROUPA) 
 REFERENCES TASY.ROP_ROUPA (NR_SEQUENCIA),
  CONSTRAINT ROPMORO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ROPMORO_ROMOFPE_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_FALTA) 
 REFERENCES TASY.ROP_MOTIVO_FALTA_PECAS (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_MOVTO_ROUPA TO NIVEL_1;

