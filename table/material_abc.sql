ALTER TABLE TASY.MATERIAL_ABC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_ABC CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_ABC
(
  CD_MATERIAL           NUMBER(6)               NOT NULL,
  DT_MESANO_REFERENCIA  DATE                    NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_CURVA_ABC          VARCHAR2(1 BYTE)        NOT NULL,
  PR_CURVA_ABC          NUMBER(15,4)            NOT NULL,
  IE_CURVA_ABC_GRUPO    VARCHAR2(1 BYTE)        NOT NULL,
  PR_CURVA_ABC_GRUPO    NUMBER(15,4)            NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATABC_ESTABEL_FK_I ON TASY.MATERIAL_ABC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATABC_I1 ON TASY.MATERIAL_ABC
(DT_MESANO_REFERENCIA, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATABC_MATERIA_FK_I ON TASY.MATERIAL_ABC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATABC_PK ON TASY.MATERIAL_ABC
(CD_MATERIAL, DT_MESANO_REFERENCIA, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MATERIAL_ABC_tp  after update ON TASY.MATERIAL_ABC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_MATERIAL='||to_char(:old.CD_MATERIAL)||'#@#@DT_MESANO_REFERENCIA='||to_char(:old.DT_MESANO_REFERENCIA)||'#@#@CD_ESTABELECIMENTO='||to_char(:old.CD_ESTABELECIMENTO);gravar_log_alteracao(substr(:old.IE_CURVA_ABC,1,4000),substr(:new.IE_CURVA_ABC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CURVA_ABC',ie_log_w,ds_w,'MATERIAL_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_MESANO_REFERENCIA,1,4000),substr(:new.DT_MESANO_REFERENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_MESANO_REFERENCIA',ie_log_w,ds_w,'MATERIAL_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_CURVA_ABC_GRUPO,1,4000),substr(:new.PR_CURVA_ABC_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'PR_CURVA_ABC_GRUPO',ie_log_w,ds_w,'MATERIAL_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CURVA_ABC_GRUPO,1,4000),substr(:new.IE_CURVA_ABC_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CURVA_ABC_GRUPO',ie_log_w,ds_w,'MATERIAL_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_CURVA_ABC,1,4000),substr(:new.PR_CURVA_ABC,1,4000),:new.nm_usuario,nr_seq_w,'PR_CURVA_ABC',ie_log_w,ds_w,'MATERIAL_ABC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MATERIAL_ABC ADD (
  CONSTRAINT MATABC_PK
 PRIMARY KEY
 (CD_MATERIAL, DT_MESANO_REFERENCIA, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_ABC ADD (
  CONSTRAINT MATABC_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATABC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.MATERIAL_ABC TO NIVEL_1;

