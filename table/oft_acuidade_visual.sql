ALTER TABLE TASY.OFT_ACUIDADE_VISUAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OFT_ACUIDADE_VISUAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.OFT_ACUIDADE_VISUAL
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_CONSULTA            NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_REGISTRO                DATE,
  VL_OD_PL_AV_SC             NUMBER(10),
  VL_OD_PL_AV_CC             NUMBER(10),
  VL_OE_PL_AV_SC             NUMBER(10),
  VL_OE_PL_AV_CC             NUMBER(10),
  VL_OE_PP_AV                NUMBER(10),
  VL_OD_PP_AV                NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(1000 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  VL_OD_PL_COR_SC            NUMBER(10),
  VL_OD_PL_CON_SC            NUMBER(10),
  VL_OD_PL_COR_CC            NUMBER(10),
  VL_OD_PL_CON_CC            NUMBER(10),
  VL_OE_PL_COR_SC            NUMBER(10),
  VL_OE_PL_CON_SC            NUMBER(10),
  VL_OE_PL_COR_CC            NUMBER(10),
  VL_OD_PP_CON_SC            NUMBER(10),
  VL_OD_PP_COR_SC            NUMBER(10),
  VL_OD_PP_COR_CC            NUMBER(10),
  VL_OD_PP_CON_CC            NUMBER(10),
  VL_OE_PL_CON_CC            NUMBER(10),
  IE_TIPO                    VARCHAR2(15 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_CONSULTA_FORM       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OFACVIS_OFCOFOR_FK_I ON TASY.OFT_ACUIDADE_VISUAL
(NR_SEQ_CONSULTA_FORM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFACVIS_OFTCONS_FK_I ON TASY.OFT_ACUIDADE_VISUAL
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFACVIS_OFTCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OFACVIS_PESFISI_FK_I ON TASY.OFT_ACUIDADE_VISUAL
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OFACVIS_PK ON TASY.OFT_ACUIDADE_VISUAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFACVIS_PK
  MONITORING USAGE;


CREATE INDEX TASY.OFACVIS_TASASDI_FK_I ON TASY.OFT_ACUIDADE_VISUAL
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFACVIS_TASASDI_FK2_I ON TASY.OFT_ACUIDADE_VISUAL
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.oft_acuidade_pend_atual
after insert or update or delete ON TASY.OFT_ACUIDADE_VISUAL for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

qt_reg_w	               number(1);
ie_tipo_w		            varchar2(10);
cd_pessoa_fisica_w	varchar2(30);
nr_atendimento_w    number(10);
ie_libera_exames_oft_w	varchar2(5);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	nvl(max(ie_libera_exames_oft),'N')
into	ie_libera_exames_oft_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_exames_oft_w = 'S') then
	if	(inserting) or
		(updating) then

		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'ACUL';
		elsif	(:old.dt_liberacao is null) and
               (:new.dt_liberacao is not null) then
               ie_tipo_w := 'XACUL';
		end if;

      select	max(a.nr_atendimento),
                  max(a.cd_pessoa_fisica)
      into	   nr_atendimento_w,
                  cd_pessoa_fisica_w
      from     oft_consulta a where a.nr_sequencia = :new.nr_seq_consulta;

		begin
		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
		end if;
		exception
			when others then
			null;
		end;

	elsif	(deleting) then
            delete 	from pep_item_pendente
            where 	IE_TIPO_REGISTRO = 'ACUL'
            and	      nr_seq_registro = :old.nr_sequencia
            and	      nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

            commit;
   end if;

	commit;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.OFT_ACUIDADE_VISUAL ADD (
  CONSTRAINT OFACVIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OFT_ACUIDADE_VISUAL ADD (
  CONSTRAINT OFACVIS_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFACVIS_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFACVIS_OFCOFOR_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA_FORM) 
 REFERENCES TASY.OFT_CONSULTA_FORMULARIO (NR_SEQUENCIA),
  CONSTRAINT OFACVIS_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT OFACVIS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.OFT_ACUIDADE_VISUAL TO NIVEL_1;

