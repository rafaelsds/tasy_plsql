ALTER TABLE TASY.PARAMETRO_NFS_CREDITO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARAMETRO_NFS_CREDITO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARAMETRO_NFS_CREDITO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO        NUMBER(15)             NOT NULL,
  IE_ORIGEM_PROCED       NUMBER(10)             NOT NULL,
  CD_CONDICAO_PAGAMENTO  NUMBER(10),
  CD_SERIE_NF            VARCHAR2(255 BYTE),
  CD_OPERACAO_NF         NUMBER(3),
  CD_NATUREZA_OPERACAO   NUMBER(4),
  CD_CONVENIO            NUMBER(5)              NOT NULL,
  IE_TIPO_FATURA         VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PANFCR_CONPAGA_FK_I ON TASY.PARAMETRO_NFS_CREDITO
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PANFCR_CONVENI_FK_I ON TASY.PARAMETRO_NFS_CREDITO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PANFCR_ESTABEL_FK_I ON TASY.PARAMETRO_NFS_CREDITO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PANFCR_NATOPER_FK_I ON TASY.PARAMETRO_NFS_CREDITO
(CD_NATUREZA_OPERACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PANFCR_OPENOTA_FK_I ON TASY.PARAMETRO_NFS_CREDITO
(CD_OPERACAO_NF)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PANFCR_PK ON TASY.PARAMETRO_NFS_CREDITO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PANFCR_PROCEDI_FK_I ON TASY.PARAMETRO_NFS_CREDITO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PANFCR_SERNOFI_FK_I ON TASY.PARAMETRO_NFS_CREDITO
(CD_SERIE_NF, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.bef_upd_ins_parametro_nfs_cred
before update or insert ON TASY.PARAMETRO_NFS_CREDITO for each row
declare
qt_registros_tipo_fatura_w	number(2) := 0;

pragma autonomous_transaction;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	if (inserting) then

		select count(*) qt
		into qt_registros_tipo_fatura_w
		from parametro_nfs_credito
		where cd_convenio = :new.cd_convenio
		and cd_estabelecimento = :new.cd_estabelecimento
		and ie_tipo_fatura = :new.ie_tipo_fatura;
		
	elsif (updating) then
	
		select count(*) qt
		into qt_registros_tipo_fatura_w
		from parametro_nfs_credito
		where cd_convenio = :new.cd_convenio
		and cd_estabelecimento = :new.cd_estabelecimento
		and ie_tipo_fatura = :new.ie_tipo_fatura
		and nr_sequencia <> :new.nr_sequencia;
	
	end if;
	
	if (qt_registros_tipo_fatura_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1169838);
	end if;

end if;


end;
/


ALTER TABLE TASY.PARAMETRO_NFS_CREDITO ADD (
  CONSTRAINT PANFCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PARAMETRO_NFS_CREDITO ADD (
  CONSTRAINT PANFCR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PANFCR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PANFCR_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT PANFCR_NATOPER_FK 
 FOREIGN KEY (CD_NATUREZA_OPERACAO) 
 REFERENCES TASY.NATUREZA_OPERACAO (CD_NATUREZA_OPERACAO),
  CONSTRAINT PANFCR_OPENOTA_FK 
 FOREIGN KEY (CD_OPERACAO_NF) 
 REFERENCES TASY.OPERACAO_NOTA (CD_OPERACAO_NF),
  CONSTRAINT PANFCR_SERNOFI_FK 
 FOREIGN KEY (CD_SERIE_NF, CD_ESTABELECIMENTO) 
 REFERENCES TASY.SERIE_NOTA_FISCAL (CD_SERIE_NF,CD_ESTABELECIMENTO),
  CONSTRAINT PANFCR_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

