ALTER TABLE TASY.SEGMENTO_COMPRAS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SEGMENTO_COMPRAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SEGMENTO_COMPRAS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_SEGMENTO            VARCHAR2(255 BYTE)     NOT NULL,
  QT_DIA_INTERV_ENTREGA  NUMBER(15,4),
  QT_DIA_COMPRA          NUMBER(15,4),
  QT_DIA_CONSUMO         NUMBER(15,4),
  QT_ENTREGAS            NUMBER(15,4),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  QT_DIA_FREQUENCIA      NUMBER(13,2),
  CD_EMPRESA             NUMBER(4)              NOT NULL,
  CD_PESSOA_RESPONSAVEL  VARCHAR2(10 BYTE),
  IE_CURVA_ABC           VARCHAR2(1 BYTE)       NOT NULL,
  VL_CURVAA              NUMBER(13,4),
  VL_CURVAB              NUMBER(13,4),
  VL_CURVAC              NUMBER(13,4),
  QT_MESES_CURVA_ABC     NUMBER(3),
  IE_FORMA_CURVA_ABC     VARCHAR2(1 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SEGCOMP_EMPRESA_FK_I ON TASY.SEGMENTO_COMPRAS
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SEGCOMP_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SEGCOMP_ESTABEL_FK_I ON TASY.SEGMENTO_COMPRAS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SEGCOMP_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SEGCOMP_PESFISI_FK_I ON TASY.SEGMENTO_COMPRAS
(CD_PESSOA_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SEGCOMP_PK ON TASY.SEGMENTO_COMPRAS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SEGMENTO_COMPRAS ADD (
  CONSTRAINT SEGCOMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SEGMENTO_COMPRAS ADD (
  CONSTRAINT SEGCOMP_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT SEGCOMP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SEGCOMP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.SEGMENTO_COMPRAS TO NIVEL_1;

