ALTER TABLE TASY.CP_INDICATOR_MEASURE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CP_INDICATOR_MEASURE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CP_INDICATOR_MEASURE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INDICATOR     NUMBER(10)               NOT NULL,
  NR_SEQ_MEASURE       NUMBER(10)               NOT NULL,
  IE_ORIGIN            VARCHAR2(10 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_GOAL_INDIC    NUMBER(10),
  SI_IMPORT_STATUS     VARCHAR2(2 BYTE),
  SI_RECORD_CHANGED    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPINMEA_CPGOALI_FK_I ON TASY.CP_INDICATOR_MEASURE
(NR_SEQ_GOAL_INDIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPINMEA_CPINDIC_FK_I ON TASY.CP_INDICATOR_MEASURE
(NR_SEQ_INDICATOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPINMEA_CPMEASU_FK_I ON TASY.CP_INDICATOR_MEASURE
(NR_SEQ_MEASURE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPINMEA_PK ON TASY.CP_INDICATOR_MEASURE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CP_INDICATOR_MEASURE ADD (
  CONSTRAINT CPINMEA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CP_INDICATOR_MEASURE ADD (
  CONSTRAINT CPINMEA_CPINDIC_FK 
 FOREIGN KEY (NR_SEQ_INDICATOR) 
 REFERENCES TASY.CP_INDICATOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CPINMEA_CPMEASU_FK 
 FOREIGN KEY (NR_SEQ_MEASURE) 
 REFERENCES TASY.CP_MEASURE (NR_SEQUENCIA),
  CONSTRAINT CPINMEA_CPGOALI_FK 
 FOREIGN KEY (NR_SEQ_GOAL_INDIC) 
 REFERENCES TASY.CP_GOAL_INDICATOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.CP_INDICATOR_MEASURE TO NIVEL_1;

