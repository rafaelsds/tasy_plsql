ALTER TABLE TASY.AGEINT_RODIZIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_RODIZIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_RODIZIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_CLASSIF_AGENDA    VARCHAR2(5 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  QT_MARC_ENTRE_PROF   NUMBER(3)                NOT NULL,
  IE_APRES_FORA_ROD    VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE     NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGIRODZ_ESPMEDI_FK_I ON TASY.AGEINT_RODIZIO
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGIRODZ_PK ON TASY.AGEINT_RODIZIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGEINT_RODIZIO ADD (
  CONSTRAINT AGIRODZ_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.AGEINT_RODIZIO ADD (
  CONSTRAINT AGIRODZ_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE));

GRANT SELECT ON TASY.AGEINT_RODIZIO TO NIVEL_1;

