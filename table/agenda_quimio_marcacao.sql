ALTER TABLE TASY.AGENDA_QUIMIO_MARCACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_QUIMIO_MARCACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_QUIMIO_MARCACAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_AGENDA                DATE,
  NR_SEQ_PEND_AGENDA       NUMBER(10),
  NR_SEQ_LOCAL             NUMBER(10),
  NR_DURACAO               NUMBER(10),
  IE_GERADO                VARCHAR2(1 BYTE),
  NR_SEQ_PROF              NUMBER(10),
  IE_TRANSFERENCIA         VARCHAR2(1 BYTE),
  IE_TIPO_AGENDAMENTO      VARCHAR2(15 BYTE),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  NR_SEQ_AGENDA            NUMBER(10),
  NR_SEQ_PACIENTE          NUMBER(10),
  NR_SEQ_AGEINT_ITEM       NUMBER(10),
  IE_ENCAIXE               VARCHAR2(1 BYTE),
  NR_SEQ_ATENDIMENTO       NUMBER(10),
  NM_USUARIO_CONFIRM_FORC  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGQUIMA_I1 ON TASY.AGENDA_QUIMIO_MARCACAO
(NR_SEQ_AGEINT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGQUIMA_I2 ON TASY.AGENDA_QUIMIO_MARCACAO
(NR_SEQ_LOCAL, DT_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGQUIMA_I3 ON TASY.AGENDA_QUIMIO_MARCACAO
(NR_SEQ_LOCAL, IE_GERADO, NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGQUIMA_PK ON TASY.AGENDA_QUIMIO_MARCACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGQUIMA_PK
  MONITORING USAGE;


CREATE INDEX TASY.AGQUIMA_QTLOCAL_FK_I ON TASY.AGENDA_QUIMIO_MARCACAO
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGQUIMA_QTLOCAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGQUIMA_QTPENAG_FK_I ON TASY.AGENDA_QUIMIO_MARCACAO
(NR_SEQ_PEND_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGQUIMA_QTPROFI_FK_I ON TASY.AGENDA_QUIMIO_MARCACAO
(NR_SEQ_PROF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGQUIMA_QTPROFI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_quimio_marcacao_Atual
before insert or update ON TASY.AGENDA_QUIMIO_MARCACAO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from quimio_controle_horario
where	dt_agenda >= trunc(:new.dt_agenda)
and	nr_seq_local	= :new.nr_seq_local;

exception
	when others then
      	dt_atualizacao := sysdate;
end;
END;
/


CREATE OR REPLACE TRIGGER TASY.agenda_quimio_marcacao_Delete
before delete ON TASY.AGENDA_QUIMIO_MARCACAO FOR EACH ROW
DECLARE
dt_atualizacao 	date	:= sysdate;
BEGIN
begin

delete	from quimio_controle_horario
where	dt_agenda = trunc(:old.dt_agenda)
and	nr_seq_local	 = :old.nr_seq_local;

exception
	when others then
      	dt_atualizacao := sysdate;
end;

if (:old.IE_GERADO = 'S') then
	insert into log_tasy
	(DT_ATUALIZACAO, NM_USUARIO, DS_LOG, CD_LOG)
	values
	(sysdate, :old.nm_usuario,
	substr('Log Deleted - nr_seq_atendimento:' ||  :old.nr_seq_atendimento || ' - nr_seq_local:' || :old.nr_seq_local || ' - nr_seq_pend_agenda:' || :old.nr_seq_pend_agenda || ' - ie_gerado:' || :old.ie_gerado || ' - ie_encaixe:' || :old.ie_encaixe || ' - ie_transferencia:' || :old.ie_transferencia || ' - ' ||
	obter_funcao_ativa || ' - ' || obter_usuario_ativo || ' - ' || obter_perfil_ativo || ' - stack:' ||
		dbms_utility.format_call_stack(),1,2000), 43312);
end if;

END;
/


ALTER TABLE TASY.AGENDA_QUIMIO_MARCACAO ADD (
  CONSTRAINT AGQUIMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_QUIMIO_MARCACAO ADD (
  CONSTRAINT AGQUIMA_QTPENAG_FK 
 FOREIGN KEY (NR_SEQ_PEND_AGENDA) 
 REFERENCES TASY.QT_PENDENCIA_AGENDA (NR_SEQUENCIA),
  CONSTRAINT AGQUIMA_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT AGQUIMA_QTPROFI_FK 
 FOREIGN KEY (NR_SEQ_PROF) 
 REFERENCES TASY.QT_PROFISSIONAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_QUIMIO_MARCACAO TO NIVEL_1;

