ALTER TABLE TASY.W_DESEMPENHO_FUNC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DESEMPENHO_FUNC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DESEMPENHO_FUNC
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  NM_USUARIO_REF    VARCHAR2(15 BYTE)           NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  QT_MIN_TRAB       NUMBER(15)                  NOT NULL,
  QT_MIN_PREV       NUMBER(10),
  QT_MIN_PEND_PREV  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WDESFUN_PK ON TASY.W_DESEMPENHO_FUNC
(NR_SEQUENCIA, NM_USUARIO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDESFUN_PK
  MONITORING USAGE;


CREATE INDEX TASY.WDESFUN_USUARIO_FK_I ON TASY.W_DESEMPENHO_FUNC
(NM_USUARIO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDESFUN_USUARIO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.WDESFUN_WDESEQP_FK_I ON TASY.W_DESEMPENHO_FUNC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDESFUN_WDESEQP_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_DESEMPENHO_FUNC ADD (
  CONSTRAINT WDESFUN_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NM_USUARIO_REF)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_DESEMPENHO_FUNC ADD (
  CONSTRAINT WDESFUN_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_REF) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT WDESFUN_WDESEQP_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.W_DESEMPENHO_EQUIPE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_DESEMPENHO_FUNC TO NIVEL_1;

