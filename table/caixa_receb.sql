ALTER TABLE TASY.CAIXA_RECEB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CAIXA_RECEB CASCADE CONSTRAINTS;

CREATE TABLE TASY.CAIXA_RECEB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_SALDO_CAIXA    NUMBER(10)              NOT NULL,
  NR_SEQ_TRANS_FINANC   NUMBER(10),
  DT_RECEBIMENTO        DATE                    NOT NULL,
  NR_RECIBO             NUMBER(10),
  VL_ESPECIE            NUMBER(15,2)            NOT NULL,
  DT_FECHAMENTO         DATE,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  CD_CGC                VARCHAR2(14 BYTE),
  DT_CANCELAMENTO       DATE,
  IE_TIPO_RECEB         VARCHAR2(1 BYTE),
  NR_SEQ_NEGOCIACAO_CR  NUMBER(10),
  NR_RPS                NUMBER(15),
  CD_TIPO_RECEB_CAIXA   NUMBER(5),
  VL_TROCO_INFORMADO    NUMBER(15,2),
  CD_MOEDA              NUMBER(3),
  VL_COTACAO            NUMBER(21,10),
  CD_MOEDA_ORIGINAL     NUMBER(5),
  VL_DEPOSITO_DIRETO    NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CAIREC_CAISADI_FK_I ON TASY.CAIXA_RECEB
(NR_SEQ_SALDO_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAIREC_MOEDA_FK_I ON TASY.CAIXA_RECEB
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAIREC_NEGOCCR_FK_I ON TASY.CAIXA_RECEB
(NR_SEQ_NEGOCIACAO_CR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CAIREC_NEGOCCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CAIREC_PESFISI_FK_I ON TASY.CAIXA_RECEB
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CAIREC_PESJURI_FK_I ON TASY.CAIXA_RECEB
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CAIREC_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CAIREC_PK ON TASY.CAIXA_RECEB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CAIREC_RECIBO_UK ON TASY.CAIXA_RECEB
(NR_RECIBO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CAIREC_RECIBO_UK
  MONITORING USAGE;


CREATE INDEX TASY.CAIREC_TIPRECE_FK_I ON TASY.CAIXA_RECEB
(CD_TIPO_RECEB_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CAIXA_RECEB_UPDATE
before update ON TASY.CAIXA_RECEB for each row
declare

qt_reg_w		number(1);
cd_estabelecimento_w	number(4);
ie_alterar_pessoa_w	varchar2(1);
ie_alterar_obs_w	varchar2(1);
ie_adto_mesma_pessoa_w	varchar2(1);
cd_pessoa_fisica_w 		adiantamento.cd_pessoa_fisica%type;
cd_cgc_w				adiantamento.cd_cgc%type;

Cursor C01 is
	select nvl(max(cd_pessoa_fisica),0),
		nvl(max(cd_cgc),0)
	from adiantamento
	where nr_seq_caixa_rec = :new.nr_sequencia;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(b.cd_estabelecimento)
into	cd_estabelecimento_w
from	caixa b,
	caixa_saldo_diario a
where	a.nr_seq_caixa	= b.nr_sequencia
and	a.nr_sequencia	= :new.nr_seq_saldo_caixa;

obter_param_usuario(813,102,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_alterar_pessoa_w);
obter_param_usuario(813,133,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_alterar_obs_w);
Obter_Param_Usuario(813, 207, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario,obter_estabelecimento_ativo, ie_adto_mesma_pessoa_w);

if	(:old.dt_fechamento is not null) and
	(:new.dt_cancelamento is null) and
	(nvl(ie_alterar_pessoa_w,'N') = 'N' or (nvl(:old.cd_pessoa_fisica,'X') = nvl(:new.cd_pessoa_fisica,'X') and nvl(:old.cd_cgc,'X') = nvl(:new.cd_cgc,'X'))) and
	(nvl(ie_alterar_obs_w,'N') = 'N' or nvl(:old.ds_observacao,'X') = nvl(:new.ds_observacao,'X')) and
	(nvl(:old.NR_RPS,0) = nvl(:new.NR_RPS,0)) then

	/* N�o � poss�vel alterar esse recibo!
	O recibo :old.nr_recibo j� foi fechado! */
	wheb_mensagem_pck.exibir_mensagem_abort(262425,'NR_RECIBO_W='||:old.nr_recibo);
end if;

if (nvl(ie_adto_mesma_pessoa_w,'N') = 'S') then
	open C01;
	loop
	fetch C01 into
		cd_pessoa_fisica_w,
		cd_cgc_w;
	exit when C01%notfound;
		begin

		if ((cd_pessoa_fisica_w > 0) and (nvl(:new.cd_pessoa_fisica,0) <> cd_pessoa_fisica_w)) or
			((cd_cgc_w > 0) and (nvl(:new.cd_cgc,0) <> cd_cgc_w)) then
			wheb_mensagem_pck.exibir_mensagem_abort(460322);
		end if;
		end;
	end loop;
	close C01;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.CAIXA_RECEB_DELETE
before delete ON TASY.CAIXA_RECEB for each row
declare

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:old.dt_fechamento is not null) then
	/* Nao e possivel excluir esse recibo!
	O recibo #@NR_RECIBO#@ ja foi fechado! */
	wheb_mensagem_pck.exibir_mensagem_abort(267124, 'NR_RECIBO=' || :old.nr_recibo);
end if;
end if;

end;
/


ALTER TABLE TASY.CAIXA_RECEB ADD (
  CONSTRAINT CAIREC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT CAIREC_RECIBO_UK
 UNIQUE (NR_RECIBO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CAIXA_RECEB ADD (
  CONSTRAINT CAIREC_TIPRECE_FK 
 FOREIGN KEY (CD_TIPO_RECEB_CAIXA) 
 REFERENCES TASY.TIPO_RECEBIMENTO (CD_TIPO_RECEBIMENTO),
  CONSTRAINT CAIREC_NEGOCCR_FK 
 FOREIGN KEY (NR_SEQ_NEGOCIACAO_CR) 
 REFERENCES TASY.NEGOCIACAO_CR (NR_SEQUENCIA),
  CONSTRAINT CAIREC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CAIREC_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CAIREC_CAISADI_FK 
 FOREIGN KEY (NR_SEQ_SALDO_CAIXA) 
 REFERENCES TASY.CAIXA_SALDO_DIARIO (NR_SEQUENCIA),
  CONSTRAINT CAIREC_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA));

GRANT SELECT ON TASY.CAIXA_RECEB TO NIVEL_1;

