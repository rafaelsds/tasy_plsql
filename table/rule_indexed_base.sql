ALTER TABLE TASY.RULE_INDEXED_BASE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RULE_INDEXED_BASE CASCADE CONSTRAINTS;

CREATE TABLE TASY.RULE_INDEXED_BASE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_CORE_NAME            VARCHAR2(100 BYTE),
  DS_SERVER_ADDRESS       VARCHAR2(255 BYTE),
  DS_DATA_IMPORT_ADDRESS  VARCHAR2(255 BYTE),
  QT_UPDATE_TIME          NUMBER(10),
  IE_ACTIVE               VARCHAR2(1 BYTE),
  IE_IMPORT_OPS           VARCHAR2(1 BYTE),
  IE_IMPORT_PRESTADOR     VARCHAR2(1 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  IE_GRAVAR_LOG           VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_CORES    DATE,
  DT_HORA_ATUALIZACAO     DATE,
  QT_BLOCO_STRING         NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RUINDBA_PK ON TASY.RULE_INDEXED_BASE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.RULE_INDEXED_BASE_tp  after update ON TASY.RULE_INDEXED_BASE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_SERVER_ADDRESS,1,500);gravar_log_alteracao(substr(:old.DS_SERVER_ADDRESS,1,4000),substr(:new.DS_SERVER_ADDRESS,1,4000),:new.nm_usuario,nr_seq_w,'DS_SERVER_ADDRESS',ie_log_w,ds_w,'RULE_INDEXED_BASE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPORT_PRESTADOR,1,4000),substr(:new.IE_IMPORT_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPORT_PRESTADOR',ie_log_w,ds_w,'RULE_INDEXED_BASE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'RULE_INDEXED_BASE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DATA_IMPORT_ADDRESS,1,4000),substr(:new.DS_DATA_IMPORT_ADDRESS,1,4000),:new.nm_usuario,nr_seq_w,'DS_DATA_IMPORT_ADDRESS',ie_log_w,ds_w,'RULE_INDEXED_BASE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_UPDATE_TIME,1,4000),substr(:new.QT_UPDATE_TIME,1,4000),:new.nm_usuario,nr_seq_w,'QT_UPDATE_TIME',ie_log_w,ds_w,'RULE_INDEXED_BASE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPORT_OPS,1,4000),substr(:new.IE_IMPORT_OPS,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPORT_OPS',ie_log_w,ds_w,'RULE_INDEXED_BASE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RULE_INDEXED_BASE ADD (
  CONSTRAINT RUINDBA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.RULE_INDEXED_BASE TO NIVEL_1;

