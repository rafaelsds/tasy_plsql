ALTER TABLE TASY.PACIENTE_MEDIC_USO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_MEDIC_USO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_MEDIC_USO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_MATERIAL                NUMBER(10),
  CD_UNID_MED                VARCHAR2(30 BYTE),
  CD_INTERVALO               VARCHAR2(7 BYTE),
  QT_DOSE                    NUMBER(15,2),
  DT_INICIO                  DATE,
  DT_FIM                     DATE,
  DS_REACAO                  VARCHAR2(255 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  DT_REGISTRO                DATE               NOT NULL,
  DS_MEDICAMENTO             VARCHAR2(60 BYTE),
  IE_INTENSIDADE             VARCHAR2(3 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  NR_SEQ_NIVEL_SEG           NUMBER(10),
  NR_SEQ_SAEP                NUMBER(10),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DT_ULTIMA_DOSE             DATE,
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  IE_NAO_SABE_INICIO         VARCHAR2(1 BYTE),
  IE_NAO_SABE_FIM            VARCHAR2(1 BYTE),
  IE_NAO_SABE_ULT_DOSE       VARCHAR2(1 BYTE),
  IE_NEGA_MEDICAMENTOS       VARCHAR2(1 BYTE),
  IE_MOTIVO_TERMINO          VARCHAR2(1 BYTE),
  IE_PROCEDENCIA             VARCHAR2(2 BYTE),
  IE_AUTO_MEDICACAO          VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  DT_REVISAO                 DATE,
  NM_USUARIO_REVISAO         VARCHAR2(15 BYTE),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_ALERTA                  VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  CD_SETOR_REVISAO           NUMBER(5),
  NM_USUARIO_TERMINO         VARCHAR2(15 BYTE),
  DS_JUST_TERMINO            VARCHAR2(255 BYTE),
  IE_NAO_SABE_MEDICAMENTOS   VARCHAR2(1 BYTE),
  IE_VIA_APLICACAO           VARCHAR2(5 BYTE),
  IE_NAO_SABE_DOSE           VARCHAR2(1 BYTE),
  DS_HORARIOS                VARCHAR2(255 BYTE),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  IE_CLASSIFICACAO           NUMBER(10),
  DS_DESCRICAO_COMPLEMENTAR  VARCHAR2(255 BYTE),
  QT_DOSE_ALMOCO             NUMBER(16,3),
  QT_DOSE_MANHA              NUMBER(16,3),
  QT_DOSE_NOITE              NUMBER(16,3),
  QT_DOSE_TARDE              NUMBER(16,3),
  CD_UNIDADE_MEDIDA_PLANO    VARCHAR2(10 BYTE),
  DS_MOTIVO                  VARCHAR2(255 BYTE),
  NR_SEQ_VERSAO              NUMBER(10),
  DS_INTERVALO_ITEM          VARCHAR2(220 BYTE),
  DS_ORIENTACAO              VARCHAR2(255 BYTE),
  DS_UNIDADE_MEDIDA_ESTOQUE  VARCHAR2(40 BYTE),
  DS_UNIDADE_MEDIDA_CONSUMO  VARCHAR2(40 BYTE),
  CD_UNIDADE_MEDIDA_DOSE     VARCHAR2(10 BYTE),
  DS_POTENCIA                VARCHAR2(255 BYTE),
  DS_UNIDADE_MEDIDA          VARCHAR2(255 BYTE),
  DS_HORARIO_ESPECIAL        VARCHAR2(255 BYTE),
  DS_FORMA_MEDICAMENTO       VARCHAR2(255 BYTE),
  DS_DOSE_DIFERENCIADA       VARCHAR2(50 BYTE),
  DS_PRINC_ATIVO             VARCHAR2(255 BYTE),
  DS_FORMA_DESC              VARCHAR2(100 BYTE),
  DS_UNID_MED_DESC           VARCHAR2(100 BYTE),
  IE_TIPO_MEDICAMENTO        VARCHAR2(1 BYTE),
  IE_EXIBIR_PLANO_MEDIC      VARCHAR2(1 BYTE),
  NR_SEQ_HIST_ROTINA         NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  IE_MEDICACAO_PACIENTE      VARCHAR2(1 BYTE),
  CD_IDENTIFICATION_REPORT   VARCHAR2(250 BYTE),
  NR_SEQ_RP                  NUMBER(10),
  CD_RP_JAPAN                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACMEUS_ATCONSPEPA_FK_I ON TASY.PACIENTE_MEDIC_USO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACMEUS_ATEPACI_FK_I ON TASY.PACIENTE_MEDIC_USO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACMEUS_EHRREEL_FK_I ON TASY.PACIENTE_MEDIC_USO
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_HISTSAU_FK_I ON TASY.PACIENTE_MEDIC_USO
(NR_SEQ_HIST_ROTINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACMEUS_INTPRES_FK_I ON TASY.PACIENTE_MEDIC_USO
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_I1 ON TASY.PACIENTE_MEDIC_USO
(DT_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACMEUS_MATERIA_FK_I ON TASY.PACIENTE_MEDIC_USO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_NISEALE_FK_I ON TASY.PACIENTE_MEDIC_USO
(NR_SEQ_NIVEL_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_NISEALE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_PESFISI_FK_I ON TASY.PACIENTE_MEDIC_USO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACMEUS_PESFISI_FK2_I ON TASY.PACIENTE_MEDIC_USO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACMEUS_PK ON TASY.PACIENTE_MEDIC_USO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACMEUS_PLANVER_FK_I ON TASY.PACIENTE_MEDIC_USO
(NR_SEQ_VERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACMEUS_SAEPERO_FK_I ON TASY.PACIENTE_MEDIC_USO
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_SETATEN_FK_I ON TASY.PACIENTE_MEDIC_USO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_SETATEN_FK2_I ON TASY.PACIENTE_MEDIC_USO
(CD_SETOR_REVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_TASASDI_FK_I ON TASY.PACIENTE_MEDIC_USO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_TASASDI_FK2_I ON TASY.PACIENTE_MEDIC_USO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_UNIMEDI_FK_I ON TASY.PACIENTE_MEDIC_USO
(CD_UNID_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACMEUS_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACMEUS_VIAAPLI_FK_I ON TASY.PACIENTE_MEDIC_USO
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PACIENTE_MEDIC_USO_ATUAL
before insert or update ON TASY.PACIENTE_MEDIC_USO for each row
declare
nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(4);
qt_idade_w		number(10);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	ie_evento_disp		= 'LHSP'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_situacao,'A') = 'A';

begin
if	(:new.nr_atendimento is not null) then
	:new.cd_setor_atendimento := obter_setor_atendimento(:new.nr_atendimento);
end if;

if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(NVL(:new.ie_nega_medicamentos,'S') not in ('S','D')) and
	(:new.dt_fim is not null) and
	(:old.dt_fim is null or :old.dt_fim <> :new.dt_fim)
	then
		:new.nm_usuario_termino := wheb_usuario_pck.get_nm_usuario;
end if;

if	(:new.dt_fim is null) and
	(:old.dt_fim is not null)then
	:new.nm_usuario_termino := '';
end if;

if	(:new.dt_liberacao is not null) then
        begin
	qt_idade_w	:= nvl(obter_idade_pf( :new.cd_pessoa_fisica,sysdate,'A'),0);
        open C01;
                loop
                fetch C01 into
                        nr_seq_evento_w;
                exit when C01%notfound;
                        begin
                        gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,:new.cd_pessoa_fisica,null,:new.nm_usuario,null,:new.dt_liberacao,null);
                        end;
                end loop;
        close C01;
        end;
end if;

if (:new.ds_unidade_medida is not null and :new.cd_unid_med is null) then
	:new.cd_unid_med := :new.ds_unidade_medida;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_MEDIC_USO_PLAN
after insert ON TASY.PACIENTE_MEDIC_USO for each row
declare

begin
if	(:new.ds_princ_ativo is not null or :new.ds_potencia is not null) then
	insert_princ_ativo_plan(:new.ds_princ_ativo, :new.ds_potencia, :new.nr_sequencia);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_medic_uso_pend_atual 
after insert or update ON TASY.PACIENTE_MEDIC_USO 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSMU'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSMU'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_MEDIC_USO_SBIS_IN
before insert or update ON TASY.PACIENTE_MEDIC_USO for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  IF (INSERTING) THEN
    select 	log_alteracao_prontuario_seq.nextval
    into 	nr_log_seq_w
    from 	dual;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       obter_desc_expressao(656665) ,
                       wheb_usuario_pck.get_nm_maquina,
                       nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.cd_pessoa_fisica),
                       obter_desc_expressao(293088),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
  else
    ie_inativacao_w := 'N';

    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';
    end if;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                        sysdate,
                        decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
                        wheb_usuario_pck.get_nm_maquina,
                        nvl(obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.cd_pessoa_fisica),
                        obter_desc_expressao(293088),
                        :new.nr_atendimento,
                        :new.dt_liberacao,
                        :new.dt_inativacao,
                        nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
  end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_MEDIC_USO_tp  after update ON TASY.PACIENTE_MEDIC_USO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_VIA_APLICACAO,1,500);gravar_log_alteracao(substr(:old.IE_VIA_APLICACAO,1,4000),substr(:new.IE_VIA_APLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIA_APLICACAO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_NEGA_MEDICAMENTOS,1,500);gravar_log_alteracao(substr(:old.IE_NEGA_MEDICAMENTOS,1,4000),substr(:new.IE_NEGA_MEDICAMENTOS,1,4000),:new.nm_usuario,nr_seq_w,'IE_NEGA_MEDICAMENTOS',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MATERIAL,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_UNID_MED,1,500);gravar_log_alteracao(substr(:old.CD_UNID_MED,1,4000),substr(:new.CD_UNID_MED,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_INTERVALO,1,500);gravar_log_alteracao(substr(:old.CD_INTERVALO,1,4000),substr(:new.CD_INTERVALO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERVALO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_DOSE,1,500);gravar_log_alteracao(substr(:old.QT_DOSE,1,4000),substr(:new.QT_DOSE,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_REACAO,1,500);gravar_log_alteracao(substr(:old.DS_REACAO,1,4000),substr(:new.DS_REACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_REACAO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBSERVACAO,1,500);gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_LIBERACAO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIBERACAO,1,500);gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_JUSTIFICATIVA,1,500);gravar_log_alteracao(substr(:old.DS_JUSTIFICATIVA,1,4000),substr(:new.DS_JUSTIFICATIVA,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIFICATIVA',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_CLASSIFICACAO,1,500);gravar_log_alteracao(substr(:old.IE_CLASSIFICACAO,1,4000),substr(:new.IE_CLASSIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIFICACAO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_DESCRICAO_COMPLEMENTAR,1,500);gravar_log_alteracao(substr(:old.DS_DESCRICAO_COMPLEMENTAR,1,4000),substr(:new.DS_DESCRICAO_COMPLEMENTAR,1,4000),:new.nm_usuario,nr_seq_w,'DS_DESCRICAO_COMPLEMENTAR',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_ORIENTACAO,1,500);gravar_log_alteracao(substr(:old.DS_ORIENTACAO,1,4000),substr(:new.DS_ORIENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ULTIMA_DOSE,1,500);gravar_log_alteracao(to_char(:old.DT_ULTIMA_DOSE,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ULTIMA_DOSE,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ULTIMA_DOSE',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_MEDICAMENTO,1,500);gravar_log_alteracao(substr(:old.DS_MEDICAMENTO,1,4000),substr(:new.DS_MEDICAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MEDICAMENTO',ie_log_w,ds_w,'PACIENTE_MEDIC_USO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_MEDIC_USO_SBIS_DEL
before delete ON TASY.PACIENTE_MEDIC_USO for each row
declare
nr_log_seq_w		number(10);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 :old.cd_pessoa_fisica,
									 obter_desc_expressao(293088),
									  :old.nr_atendimento,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario,:old.nm_usuario));
end;
/


ALTER TABLE TASY.PACIENTE_MEDIC_USO ADD (
  CONSTRAINT PACMEUS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_MEDIC_USO ADD (
  CONSTRAINT PACMEUS_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_REVISAO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PACMEUS_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT PACMEUS_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACMEUS_PLANVER_FK 
 FOREIGN KEY (NR_SEQ_VERSAO) 
 REFERENCES TASY.PLANO_VERSAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACMEUS_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HIST_ROTINA) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PACMEUS_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACMEUS_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACMEUS_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACMEUS_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACMEUS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PACMEUS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PACMEUS_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT PACMEUS_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PACMEUS_UNIMEDI_FK 
 FOREIGN KEY (CD_UNID_MED) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PACMEUS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACMEUS_NISEALE_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_SEG) 
 REFERENCES TASY.NIVEL_SEGURANCA_ALERTA (NR_SEQUENCIA),
  CONSTRAINT PACMEUS_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_MEDIC_USO TO NIVEL_1;

