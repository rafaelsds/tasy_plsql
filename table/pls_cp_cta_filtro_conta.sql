ALTER TABLE TASY.PLS_CP_CTA_FILTRO_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_FILTRO_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_FILTRO_CONTA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_CP_CTA_FILTRO      NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_TIPO_GUIA              VARCHAR2(2 BYTE),
  NR_SEQ_TIPO_ATENDIMENTO   NUMBER(10),
  IE_REGIME_INTERNACAO      VARCHAR2(1 BYTE),
  NR_SEQ_CLINICA            NUMBER(10),
  IE_ORIGEM_CONTA           VARCHAR2(2 BYTE),
  NR_SEQ_GRUPO_DOENCA       NUMBER(10),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_TIPO_ACOMODACAO    NUMBER(10),
  NR_SEQ_CATEGORIA          NUMBER(10),
  QT_DIAS_INTER_INICIO      NUMBER(10),
  QT_DIAS_INTER_FINAL       NUMBER(10),
  IE_INTERNADO              VARCHAR2(1 BYTE),
  NR_SEQ_CBO_SAUDE          NUMBER(10),
  IE_CARATER_INTERNACAO     VARCHAR2(1 BYTE),
  IE_TIPO_ACOMODACAO_PTU    VARCHAR2(2 BYTE),
  IE_TIPO_CONSULTA          NUMBER(1),
  IE_ATEND_PCMSO            VARCHAR2(1 BYTE),
  IE_REF_GUIA_INTERNACAO    VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_ATEND_PRINC   NUMBER(10),
  IE_ACOMODACAO_AUTORIZADA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLCPCFC_CBOSAUD_FK_I ON TASY.PLS_CP_CTA_FILTRO_CONTA
(NR_SEQ_CBO_SAUDE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLCPCFC_PK ON TASY.PLS_CP_CTA_FILTRO_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCFC_PLCPCTF_FK_I ON TASY.PLS_CP_CTA_FILTRO_CONTA
(NR_SEQ_CP_CTA_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCFC_PLSCATE_FK_I ON TASY.PLS_CP_CTA_FILTRO_CONTA
(NR_SEQ_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCFC_PLSCLIN_FK_I ON TASY.PLS_CP_CTA_FILTRO_CONTA
(NR_SEQ_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCFC_PLSPGDO_FK_I ON TASY.PLS_CP_CTA_FILTRO_CONTA
(NR_SEQ_GRUPO_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCFC_PLSTIAC_FK_I ON TASY.PLS_CP_CTA_FILTRO_CONTA
(NR_SEQ_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCFC_PLSTIAT_FK_I ON TASY.PLS_CP_CTA_FILTRO_CONTA
(NR_SEQ_TIPO_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CP_CTA_FILTRO_CONTA_tp  after update ON TASY.PLS_CP_CTA_FILTRO_CONTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REF_GUIA_INTERNACAO,1,4000),substr(:new.IE_REF_GUIA_INTERNACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REF_GUIA_INTERNACAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_CONTA,1,4000),substr(:new.IE_ORIGEM_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_CONTA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGIME_INTERNACAO,1,4000),substr(:new.IE_REGIME_INTERNACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGIME_INTERNACAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_DOENCA,1,4000),substr(:new.NR_SEQ_GRUPO_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_DOENCA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLINICA,1,4000),substr(:new.NR_SEQ_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLINICA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ATENDIMENTO,1,4000),substr(:new.NR_SEQ_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ATENDIMENTO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_INTER_INICIO,1,4000),substr(:new.QT_DIAS_INTER_INICIO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_INTER_INICIO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_INTER_FINAL,1,4000),substr(:new.QT_DIAS_INTER_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_INTER_FINAL',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CARATER_INTERNACAO,1,4000),substr(:new.IE_CARATER_INTERNACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CARATER_INTERNACAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INTERNADO,1,4000),substr(:new.IE_INTERNADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_INTERNADO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CBO_SAUDE,1,4000),substr(:new.NR_SEQ_CBO_SAUDE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CBO_SAUDE',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CATEGORIA,1,4000),substr(:new.NR_SEQ_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CATEGORIA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ACOMODACAO,1,4000),substr(:new.NR_SEQ_TIPO_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ACOMODACAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ACOMODACAO_AUTORIZADA,1,4000),substr(:new.IE_ACOMODACAO_AUTORIZADA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACOMODACAO_AUTORIZADA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ATEND_PRINC,1,4000),substr(:new.NR_SEQ_TIPO_ATEND_PRINC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ATEND_PRINC',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CONSULTA,1,4000),substr(:new.IE_TIPO_CONSULTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONSULTA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATEND_PCMSO,1,4000),substr(:new.IE_ATEND_PCMSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATEND_PCMSO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CP_CTA_FILTRO,1,4000),substr(:new.NR_SEQ_CP_CTA_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_CTA_FILTRO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_CONTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CP_CTA_FILTRO_CONTA ADD (
  CONSTRAINT PLCPCFC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_FILTRO_CONTA ADD (
  CONSTRAINT PLCPCFC_PLCPCTF_FK 
 FOREIGN KEY (NR_SEQ_CP_CTA_FILTRO) 
 REFERENCES TASY.PLS_CP_CTA_FILTRO (NR_SEQUENCIA),
  CONSTRAINT PLCPCFC_PLSCLIN_FK 
 FOREIGN KEY (NR_SEQ_CLINICA) 
 REFERENCES TASY.PLS_CLINICA (NR_SEQUENCIA),
  CONSTRAINT PLCPCFC_PLSPGDO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_DOENCA) 
 REFERENCES TASY.PLS_PRECO_GRUPO_DOENCA (NR_SEQUENCIA),
  CONSTRAINT PLCPCFC_PLSTIAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATENDIMENTO) 
 REFERENCES TASY.PLS_TIPO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLCPCFC_CBOSAUD_FK 
 FOREIGN KEY (NR_SEQ_CBO_SAUDE) 
 REFERENCES TASY.CBO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PLCPCFC_PLSCATE_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA) 
 REFERENCES TASY.PLS_CATEGORIA (NR_SEQUENCIA),
  CONSTRAINT PLCPCFC_PLSTIAC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACOMODACAO) 
 REFERENCES TASY.PLS_TIPO_ACOMODACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CP_CTA_FILTRO_CONTA TO NIVEL_1;

