ALTER TABLE TASY.EIS_PADRAO_INDICADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EIS_PADRAO_INDICADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.EIS_PADRAO_INDICADOR
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  NR_SEQ_INDICADOR      NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_ANO                NUMBER(4)               NOT NULL,
  CD_SEMESTRE           VARCHAR2(1 BYTE)        NOT NULL,
  VL_PADRAO             NUMBER(15,4)            NOT NULL,
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  VL_BENCHMARKING       NUMBER(15,4),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EISPAIN_ESTABEL_FK_I ON TASY.EIS_PADRAO_INDICADOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EISPAIN_INDGEST_FK_I ON TASY.EIS_PADRAO_INDICADOR
(NR_SEQ_INDICADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EISPAIN_PK ON TASY.EIS_PADRAO_INDICADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EISPAIN_PK
  MONITORING USAGE;


CREATE INDEX TASY.EISPAIN_SETATEN_FK_I ON TASY.EIS_PADRAO_INDICADOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EISPAIN_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.EIS_PADRAO_INDICADOR ADD (
  CONSTRAINT EISPAIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EIS_PADRAO_INDICADOR ADD (
  CONSTRAINT EISPAIN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT EISPAIN_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT EISPAIN_INDGEST_FK 
 FOREIGN KEY (NR_SEQ_INDICADOR) 
 REFERENCES TASY.INDICADOR_GESTAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.EIS_PADRAO_INDICADOR TO NIVEL_1;

