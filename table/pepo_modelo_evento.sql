ALTER TABLE TASY.PEPO_MODELO_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEPO_MODELO_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEPO_MODELO_EVENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MODELO        NUMBER(10)               NOT NULL,
  NR_ORDEM_EXIBICAO    NUMBER(5)                NOT NULL,
  NR_SEQ_EVENTO        NUMBER(10)               NOT NULL,
  NR_SEQ_SECAO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEMODEV_EVENCIR_FK_I ON TASY.PEPO_MODELO_EVENTO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEMODEV_PEMODSE_FK_I ON TASY.PEPO_MODELO_EVENTO
(NR_SEQ_SECAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEMODEV_PEPOMOD_FK_I ON TASY.PEPO_MODELO_EVENTO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEMODEV_PK ON TASY.PEPO_MODELO_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PEPO_MODELO_EVENTO ADD (
  CONSTRAINT PEMODEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PEPO_MODELO_EVENTO ADD (
  CONSTRAINT PEMODEV_EVENCIR_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.EVENTO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT PEMODEV_PEMODSE_FK 
 FOREIGN KEY (NR_SEQ_SECAO) 
 REFERENCES TASY.PEPO_MODELO_SECAO (NR_SEQUENCIA),
  CONSTRAINT PEMODEV_PEPOMOD_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.PEPO_MODELO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PEPO_MODELO_EVENTO TO NIVEL_1;

