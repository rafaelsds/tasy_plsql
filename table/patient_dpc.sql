ALTER TABLE TASY.PATIENT_DPC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PATIENT_DPC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PATIENT_DPC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  CD_DEPARTAMENTO            NUMBER(6),
  DT_START_DPC               DATE,
  NR_SEQ_MOST_EXP_DIAGNOSIS  NUMBER(10)         NOT NULL,
  SI_SURGERY                 VARCHAR2(1 BYTE),
  NR_SEQ_DPC_SCORE           NUMBER(10),
  NR_SEQ_EDITION             NUMBER(10)         NOT NULL,
  SI_CATEGORY                VARCHAR2(2 BYTE),
  SI_ADMISSION_REASON        VARCHAR2(2 BYTE),
  SI_INPATIENT_ROUTE         VARCHAR2(2 BYTE),
  SI_EXIT_POINT              VARCHAR2(2 BYTE),
  SI_OUT_COME                VARCHAR2(2 BYTE),
  SI_AMBULANCE               VARCHAR2(1 BYTE),
  DT_END_DPC                 DATE,
  SI_NORMAL_URGENT           VARCHAR2(1 BYTE),
  DT_CONFIRMATION            DATE,
  NM_USER_CONFIRMATION       VARCHAR2(15 BYTE),
  NR_DISEASE_NUMBER          NUMBER(10),
  DT_APPROVAL                DATE,
  NM_APPROVAL                VARCHAR2(15 BYTE),
  NM_BEHALF_PRSN             VARCHAR2(15 BYTE),
  DT_BEHALF_PRSN             DATE,
  CD_EVOLUCAO                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PATDPC_ATEPACI_FK_I ON TASY.PATIENT_DPC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATDPC_DEPMED_FK_I ON TASY.PATIENT_DPC
(CD_DEPARTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATDPC_DIADOEN_FK_I ON TASY.PATIENT_DPC
(NR_SEQ_MOST_EXP_DIAGNOSIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATDPC_DPCEDTN_FK_I ON TASY.PATIENT_DPC
(NR_SEQ_EDITION)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATDPC_DPCSCRE_FK_I ON TASY.PATIENT_DPC
(NR_SEQ_DPC_SCORE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PATDPC_EVOPACI_FK_I ON TASY.PATIENT_DPC
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PATDPC_PK ON TASY.PATIENT_DPC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.delete_pending_item_dpc2
after insert ON TASY.PATIENT_DPC for each row
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then

  if (GET_USES_DPC() = 'S') then
	delete from pep_item_pendente
	where  nr_atendimento  = :new.nr_atendimento
	and    ie_tipo_registro = 'DPC';
  end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PATIENT_DPC_INS_UPD_AFTER
after insert or update ON TASY.PATIENT_DPC for each row
declare
nm_usuario_w varchar2(30);
a_nm_usuario_w varchar2(30);
nr_atendimento_w ATENDIMENTO_PACIENTE.nr_atendimento%type;
DT_END_W date;
NR_SEQUENCIA_W PATIENT_DPC.NR_SEQUENCIA%type;
NR_SEQ_EDITION_W PATIENT_DPC.NR_SEQ_EDITION%type;


cursor c01 is
select distinct obter_usuario_pf(a.cd_prescritor)
from pessoa_fisica c,
     medico b,
     prescr_medica a
where b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.cd_prescritor  = b.cd_pessoa_fisica
and a.nr_atendimento = :new.nr_atendimento
and b.ie_situacao = 'A'
union
select distinct obter_usuario_pf(a.cd_medico)
from pessoa_fisica c,
     medico b,
     evolucao_paciente a
where b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.cd_medico = b.cd_pessoa_fisica
and a.nr_atendimento  = :new.nr_atendimento
and b.ie_situacao = 'A'
union
select distinct obter_usuario_pf(a.cd_medico_referido)
from pessoa_fisica c,
     medico b,
     atendimento_paciente a
where b.cd_pessoa_fisica = c.cd_pessoa_fisica
and a.cd_medico_referido = b.cd_pessoa_fisica
and a.nr_atendimento  = :new.nr_atendimento
and b.ie_situacao = 'A'
union
select distinct obter_usuario_pf(b.cd_medico)
from pessoa_fisica c,
     pf_medico_externo b,
     pessoa_fisica m
where b.cd_pessoa_fisica = c.cd_pessoa_fisica
and c.cd_pessoa_fisica = b.cd_pessoa_fisica
and m.cd_pessoa_fisica = b.cd_medico
and b.cd_pessoa_fisica = obter_pessoa_atendimento(:new.nr_atendimento,'C');
begin
if (GET_USES_DPC() = 'S') then

	nr_atendimento_w := :new.nr_atendimento;
	NR_SEQUENCIA_W := :new.nr_sequencia;
	NR_SEQ_EDITION_W := :new.nr_seq_edition;

select  obter_usuario_pf(cd_medico_resp)
into nm_usuario_w
from atendimento_paciente
where nr_atendimento = nr_atendimento_w;

select nvl(dt_end,sysdate)
into   dt_end_w
from   dpc_edition
where  nr_sequencia = nr_seq_edition_w;

if((:old.nr_seq_dpc_score is null) and (:NEW.nr_seq_dpc_score is not null)
	and  (nvl(:new.dt_end_dpc,sysdate) > dt_end_w)) then


insert into PEP_ITEM_PENDENTE(
		NR_SEQUENCIA,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DT_REGISTRO,
		CD_PESSOA_FISICA,
		NR_ATENDIMENTO,
		NR_SEQ_REGISTRO,
		IE_TIPO_PENDENCIA,
		IE_TIPO_REGISTRO,
		DS_ITEM)
	select	PEP_ITEM_PENDENTE_SEQ.NEXTVAL,
			sysdate,
			nm_usuario_w,
			sysdate,
			nm_usuario_w,
			DT_END_W,
			obter_pessoa_atendimento(:new.nr_atendimento,'C'),
			nr_atendimento_w,
			NR_SEQUENCIA_W,
			'L',
			'DPC2',
			obter_desc_expressao(974849)||' '|| to_char(DT_END_W,'YYYY-MM-DD')  as DS_ITEM  from dual;
	OPEN C01;
		LOOP
			FETCH C01 into a_nm_usuario_w;
			exit when c01%notfound;
            insert into PEP_ITEM_PENDENTE(
				NR_SEQUENCIA,
				DT_ATUALIZACAO,
				NM_USUARIO,
				DT_ATUALIZACAO_NREC,
				NM_USUARIO_NREC,
				DT_REGISTRO,
				CD_PESSOA_FISICA,
				NR_ATENDIMENTO,
				NR_SEQ_REGISTRO,
				IE_TIPO_PENDENCIA,
				IE_TIPO_REGISTRO,
				DS_ITEM)
			select
				PEP_ITEM_PENDENTE_SEQ.NEXTVAL,
				sysdate,
				a_nm_usuario_w,
				sysdate,
				a_nm_usuario_w,
				DT_END_W,
				obter_pessoa_atendimento(:new.nr_atendimento,'C'),
				nr_atendimento_w,
				NR_SEQUENCIA_W,
				'L',
				'DPC2',
				obter_desc_expressao(974849)||' '|| to_char(DT_END_W,'YYYY-MM-DD')  as DS_ITEM  from dual;
        END LOOP;
   	CLOSE C01;
end if;

if ((:old.nr_seq_dpc_score is null) and (:new.nr_seq_dpc_score is not null) and (:new.dt_end_dpc <= dt_end_w)) then

	    delete  pep_item_pendente
		where   nr_atendimento  = :new.nr_atendimento
		and     nr_seq_registro = :new.nr_sequencia
		and     ie_tipo_registro = 'DPC2';

end if;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.patient_dpc_afterup AFTER
  UPDATE ON TASY.PATIENT_DPC FOR EACH row
DECLARE qt_reg_w NUMBER(1);
  ds_param_integration_w VARCHAR2(500);
  BEGIN
    IF (wheb_usuario_pck.get_ie_executar_trigger = 'N') THEN
      GOTO Final;
    END IF;
    IF (((:new.dt_liberacao  IS NOT NULL) AND (:old.dt_liberacao IS NULL)) OR ((:new.dt_inativacao IS NOT NULL) AND ( :old.dt_inativacao IS NULL ))) THEN
      ds_param_integration_w := '{"recordId" : "' || :new.nr_sequencia || '"' || '}';
      execute_bifrost_integration(260, ds_param_integration_w);
    END IF;

if (:old.dt_inativacao is null and :new.dt_inativacao is not null and :new.cd_evolucao is not null) then

    update evolucao_paciente
    set dt_inativacao = sysdate,
    ie_situacao ='I',
    dt_atualizacao = sysdate ,
    nm_usuario = wheb_usuario_pck.get_nm_usuario,
    nm_usuario_inativacao = wheb_usuario_pck.get_nm_usuario
	where cd_evolucao = :new.cd_evolucao;
	delete from clinical_note_soap_data where cd_evolucao = :new.cd_evolucao and ie_med_rec_type = 'DPC'  and ie_stage = 1  and ie_soap_type = 'P' and nr_seq_med_item = :new.nr_sequencia;
	end if;

    <<Final>>
    qt_reg_w := 0;
  END;
/


CREATE OR REPLACE TRIGGER TASY.delete_registered_dpc
after insert ON TASY.PATIENT_DPC for each row
Begin
  if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') then
    if (:new.nr_atendimento is not null and get_uses_dpc() = 'S') then
      delete from pep_item_pendente
      where  nr_atendimento = :new.nr_atendimento
      and ie_tipo_registro = 'DPC2';
    end if;
   end if;
end;
/


ALTER TABLE TASY.PATIENT_DPC ADD (
  CONSTRAINT PATDPC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PATIENT_DPC ADD (
  CONSTRAINT PATDPC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PATDPC_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTAMENTO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT PATDPC_DIADOEN_FK 
 FOREIGN KEY (NR_SEQ_MOST_EXP_DIAGNOSIS) 
 REFERENCES TASY.DIAGNOSTICO_DOENCA (NR_SEQ_INTERNO),
  CONSTRAINT PATDPC_DPCEDTN_FK 
 FOREIGN KEY (NR_SEQ_EDITION) 
 REFERENCES TASY.DPC_EDITION (NR_SEQUENCIA),
  CONSTRAINT PATDPC_DPCSCRE_FK 
 FOREIGN KEY (NR_SEQ_DPC_SCORE) 
 REFERENCES TASY.DPC_SCORE (NR_SEQUENCIA),
  CONSTRAINT PATDPC_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO));

GRANT SELECT ON TASY.PATIENT_DPC TO NIVEL_1;

