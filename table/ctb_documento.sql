ALTER TABLE TASY.CTB_DOCUMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_DOCUMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_DOCUMENTO
(
  NR_SEQUENCIA           NUMBER(14)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_COMPETENCIA         DATE,
  CD_TIPO_LOTE_CONTABIL  NUMBER(10),
  NR_SEQ_TRANS_FINANC    NUMBER(10),
  NR_SEQ_INFO            NUMBER(10),
  NR_DOCUMENTO           NUMBER(14),
  NR_SEQ_DOC_COMPL       NUMBER(10),
  VL_MOVIMENTO           NUMBER(15,2),
  NM_TABELA              VARCHAR2(50 BYTE),
  NM_ATRIBUTO            VARCHAR2(255 BYTE),
  IE_STATUS              VARCHAR2(15 BYTE),
  NR_LOTE_CONTABIL       NUMBER(10),
  NR_DOC_ANALITICO       NUMBER(14),
  CD_ESTAB_CONTABIL      NUMBER(4),
  IE_SITUACAO_CTB        VARCHAR2(1 BYTE),
  DS_INCONSISTENCIA      VARCHAR2(4000 BYTE),
  DS_ORIGEM              VARCHAR2(4000 BYTE),
  DS_STACK               VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBDOC_ESTABEL_FK_I ON TASY.CTB_DOCUMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBDOC_ESTABEL_FK2_I ON TASY.CTB_DOCUMENTO
(CD_ESTAB_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBDOC_I1 ON TASY.CTB_DOCUMENTO
(NR_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBDOC_LOTCONT_FK_I ON TASY.CTB_DOCUMENTO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBDOC_PK ON TASY.CTB_DOCUMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBDOC_TRAFINA_FK_I ON TASY.CTB_DOCUMENTO
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ctb_documento_after_insert 
after insert ON TASY.CTB_DOCUMENTO 
for each row
declare

   ie_job_w                varchar2(25);
   ds_action_w             varchar2(255);
   pragma autonomous_transaction;

begin

   ie_job_w := 'DOC_PENDENTE_' || :new.nr_sequencia;
   
   ds_action_w := 'BEGIN CTB_CONTAB_ONL_LOTE_FIN_PCK.CTB_CONTABILIZA_DOCUMENTO(null,''' 
                   || :new.nm_usuario || ''',' || :new.nr_sequencia ||'); END;';

   --Criar job para processo da contabilizacao on line
   dbms_scheduler.create_job(job_name   => ie_job_w,
                             job_type   => 'PLSQL_BLOCK',
                             job_action => ds_action_w,
                             start_date => sysdate + 5 / 86400,
                             auto_drop  => TRUE,
                             enabled    => TRUE,
                             comments   => 'Contabilidade On Line de ctb_documento');

end;
/


CREATE OR REPLACE TRIGGER TASY.ctb_documento_before_update
before update of vl_movimento, dt_competencia, nr_seq_trans_financ ON TASY.CTB_DOCUMENTO for each row
declare
    ie_start_date  constant date := sysdate + 5 / 86400;
    ds_job_name    constant varchar2(25 char)  := 'DOC_PENDENTE_' || :new.nr_sequencia;
    ds_job_type    constant varchar2(25 char)  := 'PLSQL_BLOCK';
    ds_job_action  constant varchar2(255 char) := 'BEGIN CTB_CONTAB_ONL_LOTE_FIN_PCK.CTB_CONTABILIZA_DOCUMENTO(null,'''
                                                  || :new.nm_usuario || ''',' || :new.nr_sequencia ||'); END;';

   pragma autonomous_transaction;

begin
    if  (:new.ie_situacao_ctb = ctb_online_pck.ds_contabilizado and
        ((:old.vl_movimento <> :new.vl_movimento) or
        (:old.dt_competencia <> :new.dt_competencia) or
        (:old.nr_seq_trans_financ <> :new.nr_seq_trans_financ))) then
            :new.ie_situacao_ctb    := ctb_online_pck.ds_pendente;
            :new.nr_lote_contabil   := 0;
            :new.dt_atualizacao     := sysdate;

            dbms_scheduler.create_job(  job_name   => ds_job_name,
                                        job_type   => ds_job_type,
                                        job_action => ds_job_action,
                                        start_date => ie_start_date,
                                        auto_drop  => TRUE,
                                        enabled    => TRUE);
    end if;
end;
/


ALTER TABLE TASY.CTB_DOCUMENTO ADD (
  CONSTRAINT CTBDOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_DOCUMENTO ADD (
  CONSTRAINT CTBDOC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBDOC_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT CTBDOC_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT CTBDOC_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_CONTABIL) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.CTB_DOCUMENTO TO NIVEL_1;

