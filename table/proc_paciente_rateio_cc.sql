ALTER TABLE TASY.PROC_PACIENTE_RATEIO_CC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_PACIENTE_RATEIO_CC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_PACIENTE_RATEIO_CC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_CENTRO_CUSTO       NUMBER(8)               NOT NULL,
  CD_CONTA_CONTABIL     VARCHAR2(20 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PROC_PAC       NUMBER(10),
  VL_RATEIO             NUMBER(15,4),
  NR_SEQ_PAR_CC_RATEIO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROPACRAT_CENCUST_FK_I ON TASY.PROC_PACIENTE_RATEIO_CC
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPACRAT_CONCONT_FK_I ON TASY.PROC_PACIENTE_RATEIO_CC
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPACRAT_PARCCRAT_FK_I ON TASY.PROC_PACIENTE_RATEIO_CC
(NR_SEQ_PAR_CC_RATEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROPACRAT_PK ON TASY.PROC_PACIENTE_RATEIO_CC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROPACRAT_PROPACI_FK_I ON TASY.PROC_PACIENTE_RATEIO_CC
(NR_SEQ_PROC_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROC_PACIENTE_RATEIO_CC ADD (
  CONSTRAINT PROPACRAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_PACIENTE_RATEIO_CC ADD (
  CONSTRAINT PROPACRAT_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT PROPACRAT_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PROPACRAT_PARCCRAT_FK 
 FOREIGN KEY (NR_SEQ_PAR_CC_RATEIO) 
 REFERENCES TASY.PARAMETROS_CC_RATEIO (NR_SEQUENCIA),
  CONSTRAINT PROPACRAT_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROC_PAC) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROC_PACIENTE_RATEIO_CC TO NIVEL_1;

