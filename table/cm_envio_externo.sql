ALTER TABLE TASY.CM_ENVIO_EXTERNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_ENVIO_EXTERNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_ENVIO_EXTERNO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_DOCUMENTO          DATE                    NOT NULL,
  CD_CNPJ               VARCHAR2(14 BYTE)       NOT NULL,
  DT_LIBERACAO          DATE,
  CD_RESP_ENVIO         VARCHAR2(20 BYTE),
  DT_RETORNO            DATE,
  CD_RESP_RETORNO       VARCHAR2(20 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMENEXT_PESFISI_FK_I ON TASY.CM_ENVIO_EXTERNO
(CD_RESP_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMENEXT_PESFISI_FK2_I ON TASY.CM_ENVIO_EXTERNO
(CD_RESP_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMENEXT_PESJURI_FK_I ON TASY.CM_ENVIO_EXTERNO
(CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMENEXT_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMENEXT_PK ON TASY.CM_ENVIO_EXTERNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMENEXT_PK
  MONITORING USAGE;


CREATE INDEX TASY.CMENEXT_SETATEN_FK_I ON TASY.CM_ENVIO_EXTERNO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          48K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMENEXT_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.CM_ENVIO_EXTERNO ADD (
  CONSTRAINT CMENEXT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_ENVIO_EXTERNO ADD (
  CONSTRAINT CMENEXT_PESFISI_FK 
 FOREIGN KEY (CD_RESP_ENVIO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CMENEXT_PESFISI_FK2 
 FOREIGN KEY (CD_RESP_RETORNO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CMENEXT_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT CMENEXT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CM_ENVIO_EXTERNO TO NIVEL_1;

