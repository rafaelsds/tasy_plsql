ALTER TABLE TASY.PRESCR_PROC_CANCEL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_PROC_CANCEL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_PROC_CANCEL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_PRESCRICAO        NUMBER(10)               NOT NULL,
  NR_SEQ_PRESCR        NUMBER(10)               NOT NULL,
  NM_USUARIO_CANCEL    VARCHAR2(15 BYTE)        NOT NULL,
  DT_CANCELAMENTO      DATE                     NOT NULL,
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NM_USUARIO_CONFIRM   VARCHAR2(15 BYTE),
  DT_CONFIRMACAO       DATE,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRPRCAN_I1 ON TASY.PRESCR_PROC_CANCEL
(IE_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPRCAN_I1
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRPRCAN_PK ON TASY.PRESCR_PROC_CANCEL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPRCAN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRPRCAN_PRESPRO_FK_I ON TASY.PRESCR_PROC_CANCEL
(NR_PRESCRICAO, NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPRCAN_PRESPRO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Prescr_Proc_Cancel_Update
BEFORE UPDATE ON TASY.PRESCR_PROC_CANCEL FOR EACH ROW
declare
nr_sequencia_w	number(10);
cd_convenio_w	number(5);
BEGIN
if	(:new.ie_status = 'C') and
	(:old.ie_status = 'S') then
	select	nvl(min(nr_sequencia),0),
		nvl(min(cd_convenio),0)
	into	nr_sequencia_w,
		cd_convenio_w
	from procedimento_paciente
	where nr_prescricao = :new.nr_prescricao
	  and nr_sequencia_prescricao = :new.nr_seq_prescr;
	if	(nr_sequencia_w > 0) then
		Duplicar_Proc_Paciente(nr_sequencia_w, :new.nm_usuario_confirm, nr_sequencia_w);
		update procedimento_paciente
		set qt_procedimento = qt_procedimento * -1
		where nr_sequencia = nr_sequencia_w;
		atualiza_preco_procedimento(nr_sequencia_w, cd_convenio_w, :new.nm_usuario_confirm);
	end if;
end if;

if	(:new.ie_status = 'R') and /* Bruna 03/09/2007 OS 67641 */
	(:old.ie_status = 'S') then

	update 	prescr_procedimento
	set	ie_suspenso = 'N',
		nm_usuario_susp = null,
		dt_suspensao = null
	where	nr_prescricao = :new.nr_prescricao
	and	nr_sequencia = :new.nr_seq_prescr;

end if;

END;
/


ALTER TABLE TASY.PRESCR_PROC_CANCEL ADD (
  CONSTRAINT PRPRCAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_PROC_CANCEL ADD (
  CONSTRAINT PRPRCAN_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCR) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRESCR_PROC_CANCEL TO NIVEL_1;

