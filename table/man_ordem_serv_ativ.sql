ALTER TABLE TASY.MAN_ORDEM_SERV_ATIV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERV_ATIV CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERV_ATIV
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_ORDEM_SERV       NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATIVIDADE            DATE                  NOT NULL,
  NR_SEQ_FUNCAO           NUMBER(10)            NOT NULL,
  QT_MINUTO               NUMBER(10)            NOT NULL,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  QT_MINUTO_COBR          NUMBER(10),
  VL_COBRANCA             NUMBER(15,2),
  NM_USUARIO_EXEC         VARCHAR2(15 BYTE)     NOT NULL,
  DT_FIM_ATIVIDADE        DATE,
  VL_COBRANCA_VENDA       NUMBER(15,2),
  NR_SEQ_PROJ_CRON_ETAPA  NUMBER(10),
  DS_ATIVIDADE            VARCHAR2(2000 BYTE),
  NR_SEQ_ESTAGIO          NUMBER(10),
  NR_SEQ_GRUPO_TRABALHO   NUMBER(10),
  NR_SEQ_ATIV_PREV        NUMBER(10),
  NR_SEQ_GRUPO_DES        NUMBER(10),
  NR_SEQ_GRUPO_DES_OS     NUMBER(10),
  NR_SEQ_APLICACAO_SOFT   NUMBER(10),
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  CD_PESSOA_USUARIO       VARCHAR2(10 BYTE),
  NR_SEQ_MODULO_AP_SOFT   NUMBER(10),
  NR_SEQ_MOD_FUNC         NUMBER(10),
  NR_SEQ_TIPO_ATIV        NUMBER(10),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_GRUPO_SUP        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANORSA_APLICSO_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_APLICACAO_SOFT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_APLICSO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_APLSMOD_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_MODULO_AP_SOFT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_APLSMOD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_GRUDESE_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_GRUPO_DES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_GRUDESE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_GRUDESE_FK2_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_GRUPO_DES_OS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_GRUDESE_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_GRUSUPO_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_GRUPO_SUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORSA_I1 ON TASY.MAN_ORDEM_SERV_ATIV
(NM_USUARIO_EXEC, DT_ATIVIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORSA_I2 ON TASY.MAN_ORDEM_SERV_ATIV
(NM_USUARIO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_I2
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_I3 ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_ORDEM_SERV, DT_ATIVIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORSA_MAESTPR_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          104K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_MAESTPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_MANGRTR_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_GRUPO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          104K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_MANGRTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_MANORAP_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_ATIV_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          104K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORSA_MANORSE_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORSA_MANTIFU_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_MANTIFU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_MANTPATV_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_TIPO_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_MANTPATV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_PESFISI_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(CD_PESSOA_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANORSA_PK ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORSA_PROCRET_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(NR_SEQ_PROJ_CRON_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          104K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_PROCRET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORSA_SETATEN_FK_I ON TASY.MAN_ORDEM_SERV_ATIV
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORSA_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.man_ordem_serv_ativ_atual
before insert or update ON TASY.MAN_ORDEM_SERV_ATIV for each row
declare

nr_seq_classif_w	 number(10);
ie_em_programacao_w	varchar2(1);
ie_progr_analis_w	varchar2(1);
ie_origem_w		proj_projeto.ie_origem%type;
dt_inic_real_etapa_w	date;
nr_seq_proj_cron_etapa_w		number(10)	:= 0;
ie_status_ordem_w	varchar(1);
vl_param_59_w			varchar2(1);
ie_usuario_controle_w	varchar2(1);

BEGIN

if	((user = 'CORP' or user = 'TASY')
	and	wheb_usuario_pck.get_cd_setor_atendimento in (2,4,7)) then

	vl_param_59_w := Obter_Param_Usuario_padrao (296, 59, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);

	select	nvl(max('S'), 'N')
	into	ie_usuario_controle_w
	from	usuario_controle u
	where	u.nm_usuario = :new.nm_usuario_exec
	and		trunc(:new.dt_atividade) = trunc(u.dt_entrada);

	if (	(INSERTING or (UPDATING and :new.DT_ATIVIDADE != :old.DT_ATIVIDADE))
		and (ie_usuario_controle_w <> 'S' and vl_param_59_w = 'S')) then
		begin
			wheb_mensagem_pck.exibir_mensagem_abort(247219);
		end;
	end if;
end if;

if	(user = 'CORP') then
	select	nr_seq_proj_cron_etapa,
			ie_status_ordem
	into	nr_seq_proj_cron_etapa_w,
			ie_status_ordem_w
	from	man_ordem_servico
	where	nr_sequencia = :new.nr_seq_ordem_serv;

	/*if (nr_seq_proj_cron_etapa_w > 0 and ie_status_ordem_w = 3) then
		Wheb_mensagem_pck.exibir_mensagem_abort(450302);
	end if;
	*/
	:new.qt_minuto	:= (nvl(:new.DT_FIM_ATIVIDADE,:new.DT_ATIVIDADE) - :new.DT_ATIVIDADE) * 1440;
	if	(:new.DT_ATIVIDADE > nvl(:new.DT_FIM_ATIVIDADE,:new.DT_ATIVIDADE)) then
		Wheb_mensagem_pck.exibir_mensagem_abort(117033);
	end if;
	if	(nvl(:new.qt_minuto,0) > 600) then
		Wheb_mensagem_pck.exibir_mensagem_abort(316289);
	end if;

	select	decode(count(*),0,'N','S')
	into	ie_progr_analis_w
	from	man_ordem_servico
	where	nr_sequencia = :new.nr_seq_ordem_serv
	and	nr_seq_estagio in (732,731,1191,4);

	if	(ie_progr_analis_w = 'S') then

		if	(:new.nr_seq_funcao = 31) then 	/*An�lise*/
			select	decode(count(*),0,'N','S')
			into	ie_em_programacao_w
			from	man_ordem_servico
			where	nr_sequencia = :new.nr_seq_ordem_serv
			and	nr_seq_estagio = 732;

			if	(ie_em_programacao_w = 'N') and
				(:new.dt_fim_atividade is null) then
				update	man_ordem_servico
				set	nr_seq_estagio = 731	/*Desenv em an�lise*/
				where	nr_sequencia = :new.nr_seq_ordem_serv;
			end if;
		elsif	(:new.nr_seq_funcao in (11,551)) then	/*Programa��o*/
			if	(:new.dt_fim_atividade is null) then
				update	man_ordem_servico
				set	nr_seq_estagio = 732	/*Desenv em programa��o*/
				where	nr_sequencia = :new.nr_seq_ordem_serv;
			else
				update	man_ordem_servico
				set	nr_seq_estagio = 1191	/*Desenv aguardando programa��o*/
				where	nr_sequencia = :new.nr_seq_ordem_serv;
			end if;
		end if;
	end if;

	if	(:new.nr_seq_proj_cron_etapa is not null) then

		select	max(p.ie_origem),
			max(e.dt_inicio_real)
		into	ie_origem_w,
			dt_inic_real_etapa_w
		from	proj_projeto p,
			proj_cronograma c,
			proj_cron_etapa e
		where	e.nr_seq_cronograma = c.nr_sequencia
		and	c.nr_seq_proj = p.nr_sequencia
		and	e.nr_sequencia = :new.nr_seq_proj_cron_etapa;

		if	(ie_origem_w = 'D') and
			(dt_inic_real_etapa_w is null) then

			update	proj_cron_etapa
			set	dt_inicio_real = :new.dt_atividade,
				nm_usuario = :new.nm_usuario,
				dt_atualizacao = :new.dt_atualizacao
			where	nr_sequencia = :new.nr_seq_proj_cron_etapa;

		end if;
	end if;
end if;

END;
/


ALTER TABLE TASY.MAN_ORDEM_SERV_ATIV ADD (
  CONSTRAINT MANORSA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          384K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERV_ATIV ADD (
  CONSTRAINT MANORSA_GRUSUPO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SUP) 
 REFERENCES TASY.GRUPO_SUPORTE (NR_SEQUENCIA),
  CONSTRAINT MANORSA_MANTPATV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATIV) 
 REFERENCES TASY.MAN_TIPO_ATIV (NR_SEQUENCIA),
  CONSTRAINT MANORSA_MANGRTR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TRABALHO) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT MANORSA_MANORAP_FK 
 FOREIGN KEY (NR_SEQ_ATIV_PREV) 
 REFERENCES TASY.MAN_ORDEM_ATIV_PREV (NR_SEQUENCIA),
  CONSTRAINT MANORSA_GRUDESE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_DES) 
 REFERENCES TASY.GRUPO_DESENVOLVIMENTO (NR_SEQUENCIA),
  CONSTRAINT MANORSA_GRUDESE_FK2 
 FOREIGN KEY (NR_SEQ_GRUPO_DES_OS) 
 REFERENCES TASY.GRUPO_DESENVOLVIMENTO (NR_SEQUENCIA),
  CONSTRAINT MANORSA_APLICSO_FK 
 FOREIGN KEY (NR_SEQ_APLICACAO_SOFT) 
 REFERENCES TASY.APLICACAO_SOFTWARE (NR_SEQUENCIA),
  CONSTRAINT MANORSA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_USUARIO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MANORSA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT MANORSA_APLSMOD_FK 
 FOREIGN KEY (NR_SEQ_MODULO_AP_SOFT) 
 REFERENCES TASY.APLICACAO_SOFT_MODULO (NR_SEQUENCIA),
  CONSTRAINT MANORSA_MAESTPR_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.MAN_ESTAGIO_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT MANORSA_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANORSA_MANTIFU_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO) 
 REFERENCES TASY.MAN_TIPO_FUNCAO (NR_SEQUENCIA),
  CONSTRAINT MANORSA_PROCRET_FK 
 FOREIGN KEY (NR_SEQ_PROJ_CRON_ETAPA) 
 REFERENCES TASY.PROJ_CRON_ETAPA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORDEM_SERV_ATIV TO NIVEL_1;

