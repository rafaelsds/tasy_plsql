ALTER TABLE TASY.PLS_REGRA_PRECO_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_PRECO_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_PRECO_ANEXO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_PRECO_PROC  NUMBER(10),
  NR_SEQ_REGRA_PRECO_MAT   NUMBER(10),
  NR_SEQ_REGRA_PRECO_SERV  NUMBER(10),
  DS_DOCUMENTO             VARCHAR2(255 BYTE)   NOT NULL,
  DS_ANEXO                 VARCHAR2(255 BYTE)   NOT NULL,
  DT_INICIO_VIGENCIA       DATE                 NOT NULL,
  DT_FIM_VIGENCIA          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRPAX_PK ON TASY.PLS_REGRA_PRECO_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPAX_PLSREPM_FK_I ON TASY.PLS_REGRA_PRECO_ANEXO
(NR_SEQ_REGRA_PRECO_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPAX_PLSREPRP_FK_I ON TASY.PLS_REGRA_PRECO_ANEXO
(NR_SEQ_REGRA_PRECO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPAX_PLSREPS_FK_I ON TASY.PLS_REGRA_PRECO_ANEXO
(NR_SEQ_REGRA_PRECO_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_preco_anexo_atual
after update ON TASY.PLS_REGRA_PRECO_ANEXO for each row
declare
ds_alteracao_w 	varchar2(4000);
ds_log_w	pls_regra_preco_anexo_log.ds_log%type;

begin

ds_alteracao_w  := null;
ds_log_w	:= '';

if	(nvl(:old.ds_anexo, 'X') <> nvl(:new.ds_anexo, 'X')) then
	ds_alteracao_w	:= ' alterado o campo Anexo valor anterior: '||:old.ds_anexo||' valor atual: '||:new.ds_anexo;
end if;

if	(nvl(:old.ds_documento, 'X') <> nvl(:new.ds_documento, 'X')) then

	if	(ds_alteracao_w is not null) then
		ds_alteracao_w	:= ds_alteracao_w ||';';
	end if;

	ds_alteracao_w	:=  ds_alteracao_w ||' alterado o campo Documento valor anterior: '||:old.ds_documento||' valor atual: '||:new.ds_documento;

end if;

if	(:old.dt_fim_vigencia 			<> :new.dt_fim_vigencia) or
	 ((:old.dt_fim_vigencia is null)     	and (:new.dt_fim_vigencia is not null)) or
	 ((:old.dt_fim_vigencia is not null) 	and (:new.dt_fim_vigencia is null)) then

	if	(ds_alteracao_w is not null) then
		ds_alteracao_w	:= ds_alteracao_w ||';';
	end if;

	ds_alteracao_w	:= ds_alteracao_w ||' alterado o campo Dt fim vig�ncia valor anterior: '||:old.dt_fim_vigencia||' valor atual: '||:new.dt_fim_vigencia;

end if;

if	(:old.dt_inicio_vigencia 		<> :new.dt_inicio_vigencia) or
	 ((:old.dt_inicio_vigencia is null)     and (:new.dt_inicio_vigencia is not null))or
	 ((:old.dt_inicio_vigencia is not null) and (:new.dt_inicio_vigencia is null))then

	if	(ds_alteracao_w is not null) then
		ds_alteracao_w	:= ds_alteracao_w ||';';
	end if;

	ds_alteracao_w	:= ds_alteracao_w ||' alterado o campo Dt in�cio vig�ncia valor anterior: '||:old.dt_inicio_vigencia||' valor atual: '||:new.dt_inicio_vigencia;

end if;

if	(ds_alteracao_w is not null) then

	ds_log_w := 'Altera��e(s) efetuada(s): '||ds_alteracao_w;

	insert into pls_regra_preco_anexo_log
		( 	nr_sequencia,				dt_atualizacao,		nm_usuario,
			dt_atualizacao_nrec,			nm_usuario_nrec,  	nr_seq_regra_preco_anexo,  	ds_log)
		select	pls_regra_preco_anexo_log_seq.nextval, sysdate,		:new.nm_usuario,
			sysdate, 				:new.nm_usuario,	:new.nr_sequencia, 		ds_log_w
		from	dual;
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_PRECO_ANEXO ADD (
  CONSTRAINT PLSRPAX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_PRECO_ANEXO ADD (
  CONSTRAINT PLSRPAX_PLSREPM_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PRECO_MAT) 
 REFERENCES TASY.PLS_REGRA_PRECO_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSRPAX_PLSREPRP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PRECO_PROC) 
 REFERENCES TASY.PLS_REGRA_PRECO_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSRPAX_PLSREPS_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PRECO_SERV) 
 REFERENCES TASY.PLS_REGRA_PRECO_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_PRECO_ANEXO TO NIVEL_1;

