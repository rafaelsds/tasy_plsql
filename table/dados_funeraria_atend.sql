ALTER TABLE TASY.DADOS_FUNERARIA_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DADOS_FUNERARIA_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.DADOS_FUNERARIA_ATEND
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_CGC_FUNERARIA            VARCHAR2(14 BYTE),
  CD_PESSOA_FAMILIAR          VARCHAR2(10 BYTE),
  DT_ENTRADA_CARRO_FUNERARIA  DATE,
  DT_SAIDA_CARRO_FUNERARIA    DATE,
  CD_PLACA_CARRO              VARCHAR2(10 BYTE),
  NR_ATENDIMENTO              NUMBER(10)        NOT NULL,
  CD_PESSOA_FUNERARIA         VARCHAR2(10 BYTE),
  CD_PESSOA_RECEPCAO          VARCHAR2(10 BYTE),
  CD_PESSOA_PORTARIA          VARCHAR2(10 BYTE),
  DT_LIBERACAO                DATE,
  DS_MODELO_CARRO             VARCHAR2(30 BYTE),
  NM_FAMILIAR                 VARCHAR2(80 BYTE),
  NM_PESSOA_FUNERARIA         VARCHAR2(80 BYTE),
  DS_FUNERARIA                VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DSFUATE_ATEPACI_FK_I ON TASY.DADOS_FUNERARIA_ATEND
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DSFUATE_PESFISI_FK_I ON TASY.DADOS_FUNERARIA_ATEND
(CD_PESSOA_FAMILIAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DSFUATE_PESFISI_FK2_I ON TASY.DADOS_FUNERARIA_ATEND
(CD_PESSOA_FUNERARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DSFUATE_PESFISI_FK3_I ON TASY.DADOS_FUNERARIA_ATEND
(CD_PESSOA_RECEPCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DSFUATE_PESFISI_FK4_I ON TASY.DADOS_FUNERARIA_ATEND
(CD_PESSOA_PORTARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DSFUATE_PESJURI_FK_I ON TASY.DADOS_FUNERARIA_ATEND
(CD_CGC_FUNERARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DSFUATE_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DSFUATE_PK ON TASY.DADOS_FUNERARIA_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DSFUATE_PK
  MONITORING USAGE;


ALTER TABLE TASY.DADOS_FUNERARIA_ATEND ADD (
  CONSTRAINT DSFUATE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DADOS_FUNERARIA_ATEND ADD (
  CONSTRAINT DSFUATE_PESFISI_FK4 
 FOREIGN KEY (CD_PESSOA_PORTARIA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DSFUATE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT DSFUATE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FAMILIAR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DSFUATE_PESJURI_FK 
 FOREIGN KEY (CD_CGC_FUNERARIA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT DSFUATE_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FUNERARIA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DSFUATE_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_RECEPCAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.DADOS_FUNERARIA_ATEND TO NIVEL_1;

