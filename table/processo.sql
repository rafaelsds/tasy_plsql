ALTER TABLE TASY.PROCESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCESSO
(
  CD_PROCESSO          NUMBER(10)               NOT NULL,
  NM_PROCESSO          VARCHAR2(50 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE,
  NM_USUARIO           VARCHAR2(15 BYTE),
  DS_PROCESSO          VARCHAR2(255 BYTE),
  CD_TIPO_PROCESSO     VARCHAR2(5 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_APRESENTACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROCESS_PK ON TASY.PROCESSO
(CD_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROCESSO ADD (
  CONSTRAINT PROCESS_PK
 PRIMARY KEY
 (CD_PROCESSO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PROCESSO TO NIVEL_1;

