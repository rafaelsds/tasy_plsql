ALTER TABLE TASY.PLS_PJ_EMPRESA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PJ_EMPRESA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PJ_EMPRESA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CGC               VARCHAR2(14 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_EMPRESA           VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPJEM_PESJURI_FK_I ON TASY.PLS_PJ_EMPRESA
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSPJEM_PK ON TASY.PLS_PJ_EMPRESA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPJEM_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_pj_empresa_insert_update
before insert or update ON TASY.PLS_PJ_EMPRESA for each row
declare

qt_registro_w		number(10);

pragma autonomous_transaction;
begin

select	count(1)
into	qt_registro_w
from	pls_pj_empresa
where	cd_empresa = :new.cd_empresa
and	nr_sequencia <> :new.nr_sequencia;

if	(qt_registro_w > 0) then
	wheb_mensagem_pck.exibir_mensagem_abort( 195585, null);
	/* J� existe uma empresa cadastrada com o c�digo informado. Favor verifique. */
end if;

end;
/


ALTER TABLE TASY.PLS_PJ_EMPRESA ADD (
  CONSTRAINT PLSPJEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PJ_EMPRESA ADD (
  CONSTRAINT PLSPJEM_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.PLS_PJ_EMPRESA TO NIVEL_1;

