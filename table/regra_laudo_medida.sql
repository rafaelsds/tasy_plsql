ALTER TABLE TASY.REGRA_LAUDO_MEDIDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_LAUDO_MEDIDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_LAUDO_MEDIDA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PROTOCOLO         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO_MEDIDA   NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RELAMED_PK ON TASY.REGRA_LAUDO_MEDIDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELAMED_PROTOCO_FK_I ON TASY.REGRA_LAUDO_MEDIDA
(CD_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELAMED_PROTOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELAMED_TIPMELA_FK_I ON TASY.REGRA_LAUDO_MEDIDA
(NR_SEQ_TIPO_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELAMED_TIPMELA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_LAUDO_MEDIDA ADD (
  CONSTRAINT RELAMED_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_LAUDO_MEDIDA ADD (
  CONSTRAINT RELAMED_PROTOCO_FK 
 FOREIGN KEY (CD_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO (CD_PROTOCOLO)
    ON DELETE CASCADE,
  CONSTRAINT RELAMED_TIPMELA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_MEDIDA) 
 REFERENCES TASY.TIPO_MEDIDA_LAUDO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_LAUDO_MEDIDA TO NIVEL_1;

