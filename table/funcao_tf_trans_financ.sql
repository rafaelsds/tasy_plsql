ALTER TABLE TASY.FUNCAO_TF_TRANS_FINANC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_TF_TRANS_FINANC CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_TF_TRANS_FINANC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TF_CAMPO      NUMBER(10)               NOT NULL,
  NR_SEQ_TRANS_FINANC  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FTFTRFI_FUTFCAM_FK_I ON TASY.FUNCAO_TF_TRANS_FINANC
(NR_SEQ_TF_CAMPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FTFTRFI_PK ON TASY.FUNCAO_TF_TRANS_FINANC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FTFTRFI_PK
  MONITORING USAGE;


CREATE INDEX TASY.FTFTRFI_TRAFINA_FK_I ON TASY.FUNCAO_TF_TRANS_FINANC
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FTFTRFI_TRAFINA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.FUNCAO_TF_TRANS_FINANC ADD (
  CONSTRAINT FTFTRFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_TF_TRANS_FINANC ADD (
  CONSTRAINT FTFTRFI_FUTFCAM_FK 
 FOREIGN KEY (NR_SEQ_TF_CAMPO) 
 REFERENCES TASY.FUNCAO_TF_CAMPO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT FTFTRFI_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.FUNCAO_TF_TRANS_FINANC TO NIVEL_1;

