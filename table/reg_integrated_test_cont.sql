ALTER TABLE TASY.REG_INTEGRATED_TEST_CONT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_INTEGRATED_TEST_CONT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_INTEGRATED_TEST_CONT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_VERSAO            VARCHAR2(255 BYTE)       NOT NULL,
  NR_SEQ_INTENCAO_USO  NUMBER(10)               NOT NULL,
  IE_RESULTADO         VARCHAR2(2 BYTE),
  DT_INICIO            DATE,
  DT_FIM               DATE,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGINTTSTC_PK ON TASY.REG_INTEGRATED_TEST_CONT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGINTTSTC_REGINU_FK_I ON TASY.REG_INTEGRATED_TEST_CONT
(NR_SEQ_INTENCAO_USO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REG_INTEGRATED_TEST_CONT ADD (
  CONSTRAINT REGINTTSTC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REG_INTEGRATED_TEST_CONT ADD (
  CONSTRAINT REGINTTSTC_REGINU_FK 
 FOREIGN KEY (NR_SEQ_INTENCAO_USO) 
 REFERENCES TASY.REG_INTENCAO_USO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REG_INTEGRATED_TEST_CONT TO NIVEL_1;

