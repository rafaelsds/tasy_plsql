ALTER TABLE TASY.MED_CONVENIO_AVALIACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_CONVENIO_AVALIACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_CONVENIO_AVALIACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CONVENIO          NUMBER(5),
  NR_SEQ_TIPO_AVAL     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  IE_PRIORIDADE        NUMBER(3)                NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_CONVENIO     NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDCOAV_CONVENI_FK_I ON TASY.MED_CONVENIO_AVALIACAO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCOAV_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDCOAV_MEDTIAV_FK_I ON TASY.MED_CONVENIO_AVALIACAO
(NR_SEQ_TIPO_AVAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDCOAV_PK ON TASY.MED_CONVENIO_AVALIACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDCOAV_PK
  MONITORING USAGE;


ALTER TABLE TASY.MED_CONVENIO_AVALIACAO ADD (
  CONSTRAINT MEDCOAV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_CONVENIO_AVALIACAO ADD (
  CONSTRAINT MEDCOAV_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.MED_CONVENIO_AVALIACAO TO NIVEL_1;

