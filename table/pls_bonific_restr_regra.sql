ALTER TABLE TASY.PLS_BONIFIC_RESTR_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_BONIFIC_RESTR_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_BONIFIC_RESTR_REGRA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO_RESTRICAO  NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_PROCEDIMENTO        NUMBER(15),
  IE_ORIGEM_PROCED       NUMBER(10),
  CD_AREA_PROCEDIMENTO   NUMBER(15),
  CD_ESPECIALIDADE       NUMBER(15),
  CD_GRUPO_PROC          NUMBER(15),
  IE_LIBERADO            VARCHAR2(2 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSBORR_AREPROC_FK_I ON TASY.PLS_BONIFIC_RESTR_REGRA
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORR_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBORR_ESPPROC_FK_I ON TASY.PLS_BONIFIC_RESTR_REGRA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORR_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBORR_ESTABEL_FK_I ON TASY.PLS_BONIFIC_RESTR_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBORR_GRUPROC_FK_I ON TASY.PLS_BONIFIC_RESTR_REGRA
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORR_GRUPROC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSBORR_PK ON TASY.PLS_BONIFIC_RESTR_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSBORR_PLSBRES_FK_I ON TASY.PLS_BONIFIC_RESTR_REGRA
(NR_SEQ_TIPO_RESTRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORR_PLSBRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBORR_PROCEDI_FK_I ON TASY.PLS_BONIFIC_RESTR_REGRA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORR_PROCEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_BONIFIC_RESTR_REGRA ADD (
  CONSTRAINT PLSBORR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_BONIFIC_RESTR_REGRA ADD (
  CONSTRAINT PLSBORR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSBORR_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSBORR_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSBORR_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSBORR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSBORR_PLSBRES_FK 
 FOREIGN KEY (NR_SEQ_TIPO_RESTRICAO) 
 REFERENCES TASY.PLS_BONIFIC_TIPO_RESTRICAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_BONIFIC_RESTR_REGRA TO NIVEL_1;

