ALTER TABLE TASY.PLS_PRESTADOR_EXCL_SUBST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PRESTADOR_EXCL_SUBST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PRESTADOR_EXCL_SUBST
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_NIVEL_EXCLUSAO_PTU   VARCHAR2(5 BYTE),
  CD_EXCLUSAO_REDE        VARCHAR2(5 BYTE),
  CD_EXCLUSAO_PLA_PROD    VARCHAR2(20 BYTE),
  DT_TERMINO_PREST        DATE,
  DT_INICIO_PREST         DATE,
  NR_SEQ_PRESTADOR        NUMBER(10)            NOT NULL,
  NR_SEQ_PRESTADOR_SUBST  NUMBER(10),
  NR_SEQ_MOTIVO_EXCLUSAO  NUMBER(10),
  IE_EXIBE_PORTAL         VARCHAR2(70 BYTE),
  NR_SEQ_MOTIVO_SUBST     NUMBER(10),
  NR_OFICIO               VARCHAR2(45 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPEXS_PK ON TASY.PLS_PRESTADOR_EXCL_SUBST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPEXS_PLSPRES_FK_I ON TASY.PLS_PRESTADOR_EXCL_SUBST
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPEXS_PTUMESS_FK_I ON TASY.PLS_PRESTADOR_EXCL_SUBST
(NR_SEQ_MOTIVO_EXCLUSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPEXS_PTUMVSP_FK_I ON TASY.PLS_PRESTADOR_EXCL_SUBST
(NR_SEQ_MOTIVO_SUBST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_PRESTADOR_EXCL_SUBST ADD (
  CONSTRAINT PLSPEXS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PRESTADOR_EXCL_SUBST ADD (
  CONSTRAINT PLSPEXS_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSPEXS_PTUMVSP_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUBST) 
 REFERENCES TASY.PTU_MOTIVO_SUBST_PREST (NR_SEQUENCIA),
  CONSTRAINT PLSPEXS_PTUMESS_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_EXCLUSAO) 
 REFERENCES TASY.PTU_MOTIVO_EXCLUSAO_SUBST (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PRESTADOR_EXCL_SUBST TO NIVEL_1;

