ALTER TABLE TASY.RELATORIO_AUTO_IMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RELATORIO_AUTO_IMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.RELATORIO_AUTO_IMP
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_RELATORIO         NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  QT_COPIA                 NUMBER(3)            NOT NULL,
  IE_LOGO                  VARCHAR2(1 BYTE)     NOT NULL,
  IE_CONFIGURAR            VARCHAR2(1 BYTE)     NOT NULL,
  IE_IMPRIMIR              VARCHAR2(1 BYTE),
  IE_OUTPUTBIN             NUMBER(2),
  DS_IMPRESSORA            VARCHAR2(60 BYTE),
  NR_SEQ_APRESENT          NUMBER(10),
  IE_REGRA_REPET           VARCHAR2(3 BYTE)     NOT NULL,
  IE_DIA_SEMANA            VARCHAR2(15 BYTE),
  QT_INTERVALO_MIN         NUMBER(15),
  HR_FIXA                  VARCHAR2(5 BYTE),
  DT_ULTIMA_EMISSAO        DATE,
  DT_PROXIMA_EMISSAO       DATE,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_SALVAR_PDF            VARCHAR2(1 BYTE),
  NR_DIA_FIXO              VARCHAR2(5 BYTE),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  IE_FORMA                 VARCHAR2(3 BYTE),
  NR_SEQ_SCHEDULER         NUMBER(10),
  IE_VISUALIZA_NM_USUARIO  VARCHAR2(1 BYTE),
  DT_FIXA                  DATE,
  DT_FIXA_HTML5            DATE,
  NR_SEQ_GRUPO_RELATORIO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELAUIM_GRUPRELSCH_FK_I ON TASY.RELATORIO_AUTO_IMP
(NR_SEQ_GRUPO_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RELAUIM_PK ON TASY.RELATORIO_AUTO_IMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELAUIM_RELATOR_FK_I_I ON TASY.RELATORIO_AUTO_IMP
(NR_SEQ_RELATORIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELAUIM_USUSCHE_FK_I ON TASY.RELATORIO_AUTO_IMP
(NR_SEQ_SCHEDULER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hsj_relatorio_auto_imp_pront
before update ON TASY.RELATORIO_AUTO_IMP for each row
declare

ds_valor_w varchar2(255);
ds_ultima_emissao_w varchar2(255);

begin

    if :new.nr_sequencia = 177 then
        insert into temp (a,b) 
        values(':old.dt_ultima_emissao='||to_char(:old.dt_ultima_emissao,'dd/mm/yyyy hh24:mi:ss')||'    :new.dt_ultima_emissao='||to_char(:new.dt_ultima_emissao,'dd/mm/yyyy hh24:mi:ss')||'    :new.dt_proxima_emissao='||to_char(:new.dt_proxima_emissao,'dd/mm/yyyy hh24:mi:ss'),to_char(sysdate,'dd/mm/yyyy hh24:mi:ss'));
    end if;

    if :new.nr_sequencia = 177 and :old.dt_ultima_emissao != :new.dt_ultima_emissao and to_char(:new.dt_proxima_emissao,'dd/mm/yyyy') != '01/01/3000' then
    
        :new.dt_proxima_emissao:=  '01/01/3000';
        ds_ultima_emissao_w:= to_char(:new.dt_ultima_emissao,'dd_mm_yyyy_hh24_mi_ss');
        
        
        select  ds_valor
        into    ds_valor_w
        from relatorio_auto_param
        where nr_seq_rel_auto = 177
        and cd_parametro = 'NR_SEQUENCIA';
        
        
        insert into SAME_SOLIC_PRONT_ANEXO
        (
            nr_sequencia,
            dt_atualizacao,
            nm_usuario,
            dt_atualizacao_nrec,
            nm_usuario_nrec,
            ds_arquivo,
            ds_titulo,
            nr_seq_solic_prot
        )
        values
        (
            SAME_SOLIC_PRONT_ANEXO_SEQ.nextval,
            sysdate,
            'TASY',
            sysdate,
            'TASY',
            '\\srv-atualizacao\Prontuarios\HSJ - Prontuario do Paciente_'|| ds_ultima_emissao_w||'.pdf',
            'Prontu�rio do Paciente',
            ds_valor_w
        );

        
    end if;


end hsj_relatorio_auto_imp_pront;
/


CREATE OR REPLACE TRIGGER TASY.atual_relatorio_auto_imp
before insert or update ON TASY.RELATORIO_AUTO_IMP for each row
declare

begin

if (:new.hr_fixa is not null) and
   ((:new.hr_fixa <> :old.hr_fixa) or (:old.dt_fixa is null)) then
	:new.dt_fixa := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_fixa || ':00', 'dd/mm/yyyy hh24:mi:ss');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Relatorio_Auto_Imp_Atual
BEFORE INSERT or UPDATE ON TASY.RELATORIO_AUTO_IMP FOR EACH ROW
DECLARE

begin

if	(:new.ie_regra_repet = 'D') then
	if	(:new.hr_fixa	is null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(279506);
	end if;
elsif	(:new.ie_regra_repet = 'DFS') then
	if	(not nvl(:new.ie_dia_semana,0) between 1 and 9) then
		Wheb_mensagem_pck.exibir_mensagem_abort(279509);
	end if;
	if	(:new.hr_fixa	is null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(279506);
	end if;
elsif	(:new.ie_regra_repet = 'IT') then
	if	(nvl(:new.qt_intervalo_min,0) <= 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(279510);
	end if;
end if;

if	(:new.hr_fixa	is not null) then
	if	(length(:new.hr_fixa) <> 5) then
		Wheb_mensagem_pck.exibir_mensagem_abort(279506);
	end if;
	if	(substr(:new.hr_fixa,1,2) > 23) then
		Wheb_mensagem_pck.exibir_mensagem_abort(279511);
	end if;
	if	(substr(:new.hr_fixa,4,2) > 59) then
		Wheb_mensagem_pck.exibir_mensagem_abort(279513);
	end if;
	if	(substr(:new.hr_fixa,3,1) <> ':') then
		Wheb_mensagem_pck.exibir_mensagem_abort(279514);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.RELAT_HORARIO_UPDATE_HTML
BEFORE INSERT OR UPDATE ON TASY.RELATORIO_AUTO_IMP FOR EACH ROW
DECLARE

cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type;
nm_usuario_w      usuario.nm_usuario%type;

BEGIN

/*rotina espec�fica para a plataforma HTML5 */
/* campos ds_horarios date (para utiliza��o do dateTimePickerr) em HTML x ds_horarios String em Java*/
if (:new.DT_FIXA is not null and (:new.DT_FIXA <> :old.DT_FIXA or :old.DT_FIXA is null)) then
 begin

  cd_estabelecimento_w  := wheb_usuario_pck.get_cd_estabelecimento;
  nm_usuario_w    := wheb_usuario_pck.get_nm_usuario;
  :new.HR_FIXA    := substr(to_char(:new.DT_FIXA, pkg_date_formaters.localize_mask('shortTime', pkg_date_formaters.getUserLanguageTag(cd_estabelecimento_w, nm_usuario_w))),1,5);

 end;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.RELATORIO_AUTO_IMP_atual_after
after insert or update ON TASY.RELATORIO_AUTO_IMP for each row
declare

begin

begin

if	(inserting)	then

	insert	into relatorio_auto_param(
		nr_sequencia,
		nr_seq_rel_auto,
		cd_parametro,
		dt_atualizacao,
		nm_usuario,
		ds_valor)
	select	Relatorio_Auto_Param_seq.nextval,
		:new.nr_sequencia,
		cd_parametro,
		sysdate,
		:new.nm_usuario,
		vl_padrao
	from	relatorio_parametro
	where	nr_seq_relatorio	= :new.NR_SEQ_RELATORIO;

elsif	(:new.NR_SEQ_RELATORIO <> :old.NR_SEQ_RELATORIO ) then

	delete	from relatorio_auto_param
	where	nr_seq_rel_auto	= :new.nr_sequencia;

	insert	into relatorio_auto_param(
		nr_sequencia,
		nr_seq_rel_auto,
		cd_parametro,
		dt_atualizacao,
		nm_usuario,
		ds_valor)
	select	Relatorio_Auto_Param_seq.nextval,
		:new.nr_sequencia,
		cd_parametro,
		sysdate,
		:new.nm_usuario,
		vl_padrao
	from	relatorio_parametro
	where	nr_seq_relatorio	= :new.NR_SEQ_RELATORIO;

end if;

exception
	when others then
	null;
end;

end;
/


ALTER TABLE TASY.RELATORIO_AUTO_IMP ADD (
  CONSTRAINT RELAUIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RELATORIO_AUTO_IMP ADD (
  CONSTRAINT RELAUIM_USUSCHE_FK 
 FOREIGN KEY (NR_SEQ_SCHEDULER) 
 REFERENCES TASY.USUARIO_SCHEDULER (NR_SEQUENCIA),
  CONSTRAINT RELAUIM_GRUPRELSCH_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_RELATORIO) 
 REFERENCES TASY.GRUPO_REL_SCHEDULE (NR_SEQUENCIA),
  CONSTRAINT RELAUIM_RELATOR_FK_I 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.RELATORIO_AUTO_IMP TO NIVEL_1;

