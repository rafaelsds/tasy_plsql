ALTER TABLE TASY.REGRA_COBRANCA_SAE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_COBRANCA_SAE CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_COBRANCA_SAE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  NR_SEQ_PROC_INTERNO   NUMBER(10),
  NR_SEQ_EXAME          NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_SEQ_INTERVENCAO    NUMBER(10)              NOT NULL,
  QT_PROCEDIMENTO       NUMBER(8,3)             NOT NULL,
  IE_COBRAR_DIA         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGCOBS_EXALABO_FK_I ON TASY.REGRA_COBRANCA_SAE
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOBS_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOBS_PEPROCE_FK_I ON TASY.REGRA_COBRANCA_SAE
(NR_SEQ_INTERVENCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOBS_PEPROCE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGCOBS_PK ON TASY.REGRA_COBRANCA_SAE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOBS_PK
  MONITORING USAGE;


CREATE INDEX TASY.REGCOBS_PROCEDI_FK_I ON TASY.REGRA_COBRANCA_SAE
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOBS_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOBS_PROINTE_FK_I ON TASY.REGRA_COBRANCA_SAE
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOBS_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOBS_SETATEN_FK_I ON TASY.REGRA_COBRANCA_SAE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOBS_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_COBRANCA_SAE ADD (
  CONSTRAINT REGCOBS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_COBRANCA_SAE ADD (
  CONSTRAINT REGCOBS_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT REGCOBS_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT REGCOBS_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT REGCOBS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGCOBS_PEPROCE_FK 
 FOREIGN KEY (NR_SEQ_INTERVENCAO) 
 REFERENCES TASY.PE_PROCEDIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REGRA_COBRANCA_SAE TO NIVEL_1;

