ALTER TABLE TASY.INT_PONTO_OBJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INT_PONTO_OBJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.INT_PONTO_OBJ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PONTO         NUMBER(10)               NOT NULL,
  NM_OBJETO            VARCHAR2(50 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTPONOBJ_INTPTINTG_FK_I ON TASY.INT_PONTO_OBJ
(NR_SEQ_PONTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTPONOBJ_INTPTINTG_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.INTPONOBJ_PK ON TASY.INT_PONTO_OBJ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.INTPONOBJ_PK
  MONITORING USAGE;


ALTER TABLE TASY.INT_PONTO_OBJ ADD (
  CONSTRAINT INTPONOBJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INT_PONTO_OBJ ADD (
  CONSTRAINT INTPONOBJ_INTPTINTG_FK 
 FOREIGN KEY (NR_SEQ_PONTO) 
 REFERENCES TASY.INT_PONTO_INTEGR (NR_SEQUENCIA));

GRANT SELECT ON TASY.INT_PONTO_OBJ TO NIVEL_1;

