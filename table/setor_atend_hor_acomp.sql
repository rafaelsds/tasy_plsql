ALTER TABLE TASY.SETOR_ATEND_HOR_ACOMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SETOR_ATEND_HOR_ACOMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.SETOR_ATEND_HOR_ACOMP
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  IE_DIA_SEMANA         NUMBER(1)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_TIPO_ACOMODACAO    NUMBER(4),
  DT_INICIO_TROCA       DATE                    NOT NULL,
  DT_FIM_TROCA          DATE                    NOT NULL,
  NR_SEQ_TIPO           NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  CD_CONVENIO           NUMBER(5),
  CD_PERFIL             NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SEATEHOAC_CONVENI_FK_I ON TASY.SETOR_ATEND_HOR_ACOMP
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SEATEHOAC_PK ON TASY.SETOR_ATEND_HOR_ACOMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SEATEHOAC_SETATEN_FK_I ON TASY.SETOR_ATEND_HOR_ACOMP
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SEATEHOAC_TIPACOM_FK_I ON TASY.SETOR_ATEND_HOR_ACOMP
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SEATEHOAC_VISITAN_FK_I ON TASY.SETOR_ATEND_HOR_ACOMP
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SETOR_ATEND_HOR_ACOMP ADD (
  CONSTRAINT SEATEHOAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SETOR_ATEND_HOR_ACOMP ADD (
  CONSTRAINT SEATEHOAC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT SEATEHOAC_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT SEATEHOAC_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT SEATEHOAC_VISITAN_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.VISITANTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.SETOR_ATEND_HOR_ACOMP TO NIVEL_1;

