ALTER TABLE TASY.TRE_AGENDA_ORDEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TRE_AGENDA_ORDEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.TRE_AGENDA_ORDEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA        NUMBER(10),
  NR_SEQ_ORDEM         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGENORD_MANORSE_FK_I ON TASY.TRE_AGENDA_ORDEM
(NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGENORD_MANORSE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGENORD_PK ON TASY.TRE_AGENDA_ORDEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGENORD_PK
  MONITORING USAGE;


CREATE INDEX TASY.AGENORD_TREAGEN_FK_I ON TASY.TRE_AGENDA_ORDEM
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGENORD_TREAGEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TRE_AGENDA_ORDEM ADD (
  CONSTRAINT AGENORD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TRE_AGENDA_ORDEM ADD (
  CONSTRAINT AGENORD_TREAGEN_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.TRE_AGENDA (NR_SEQUENCIA),
  CONSTRAINT AGENORD_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TRE_AGENDA_ORDEM TO NIVEL_1;

