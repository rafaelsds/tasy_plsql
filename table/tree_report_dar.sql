ALTER TABLE TASY.TREE_REPORT_DAR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TREE_REPORT_DAR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TREE_REPORT_DAR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE),
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE,
  DT_ATUALIZACAO_NREC  DATE,
  NR_SEQ_PAI           NUMBER(10),
  NR_SEQ_ORDEM         NUMBER(10),
  IE_TIPO              VARCHAR2(2 BYTE),
  CD_RELATORIO         NUMBER(5),
  CD_CLASSIF_RELAT     VARCHAR2(4 BYTE),
  DS_TITULO            VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TREE_R_DAR_PK ON TASY.TREE_REPORT_DAR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TREE_REPORT_DAR ADD (
  CONSTRAINT TREE_R_DAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.TREE_REPORT_DAR TO NIVEL_1;

