ALTER TABLE TASY.PTU_NOTA_SERVICO_RRS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_NOTA_SERVICO_RRS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_NOTA_SERVICO_RRS
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_NOTA_COBR_RRS       NUMBER(10)         NOT NULL,
  NR_LOTE                    NUMBER(8),
  NR_NOTA                    VARCHAR2(20 BYTE),
  NR_NOTA_NUMERICO           VARCHAR2(20 BYTE),
  DT_SERVICO                 DATE,
  NM_PROFISSIONAL            VARCHAR2(70 BYTE),
  SG_CONS_PROF               VARCHAR2(12 BYTE),
  NR_CONS_PROF               VARCHAR2(15 BYTE),
  SG_UF_CONS_PROF            VARCHAR2(2 BYTE),
  TP_PARTICIP                VARCHAR2(1 BYTE),
  TP_TABELA                  NUMBER(1),
  CD_SERVICO                 NUMBER(15),
  QT_COBRADA                 NUMBER(12,4),
  VL_DIF_VL_INTER            NUMBER(15,2),
  VL_SERV_COB                NUMBER(15,2),
  NR_AUTORIZ                 NUMBER(10),
  DS_SERVICO                 VARCHAR2(80 BYTE),
  CD_PROCEDIMENTO            NUMBER(15),
  IE_ORIGEM_PROCED           NUMBER(10),
  NR_SEQ_A500                NUMBER(10),
  NR_SEQ_CONTA_PROC          NUMBER(10),
  NR_SEQ_CONTA_MAT           NUMBER(10),
  NR_SEQ_PROC_PARTIC         NUMBER(10),
  NR_SEQ_MATERIAL            NUMBER(10),
  VL_PAGO_BENEF              NUMBER(15,2),
  DS_STACK                   VARCHAR2(2000 BYTE),
  CD_ITEM_UNICO              VARCHAR2(28 BYTE),
  NR_SEQ_ITEM                NUMBER(4),
  IE_TIPO_TABELA_TISS        VARCHAR2(5 BYTE),
  IE_TIPO_PARTICIPACAO_TISS  VARCHAR2(2 BYTE),
  CD_SERVICO_TISS            NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUSRRS_PK ON TASY.PTU_NOTA_SERVICO_RRS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUSRRS_PLSCOMAT_FK_I ON TASY.PTU_NOTA_SERVICO_RRS
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUSRRS_PLSCOPRO_FK_I ON TASY.PTU_NOTA_SERVICO_RRS
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUSRRS_PLSMAT_FK_I ON TASY.PTU_NOTA_SERVICO_RRS
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUSRRS_PLSPRPAR_FK_I ON TASY.PTU_NOTA_SERVICO_RRS
(NR_SEQ_PROC_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUSRRS_PROCEDI_FK_I ON TASY.PTU_NOTA_SERVICO_RRS
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUSRRS_PTUCRRS_FK_I ON TASY.PTU_NOTA_SERVICO_RRS
(NR_SEQ_NOTA_COBR_RRS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ptu_nota_servico_rrs_atual
before insert or update ON TASY.PTU_NOTA_SERVICO_RRS for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicionario [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relatorios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de atencao:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

begin

if	(inserting) then
	:new.ds_stack := substr(dbms_utility.format_call_stack,1,2000);
end if;

:new.nr_nota_numerico := somente_numero(nvl(:new.nr_nota,:old.nr_nota));

end;
/


ALTER TABLE TASY.PTU_NOTA_SERVICO_RRS ADD (
  CONSTRAINT PTUSRRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_NOTA_SERVICO_RRS ADD (
  CONSTRAINT PTUSRRS_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA),
  CONSTRAINT PTUSRRS_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA),
  CONSTRAINT PTUSRRS_PLSPRPAR_FK 
 FOREIGN KEY (NR_SEQ_PROC_PARTIC) 
 REFERENCES TASY.PLS_PROC_PARTICIPANTE (NR_SEQUENCIA),
  CONSTRAINT PTUSRRS_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PTUSRRS_PTUCRRS_FK 
 FOREIGN KEY (NR_SEQ_NOTA_COBR_RRS) 
 REFERENCES TASY.PTU_NOTA_COBRANCA_RRS (NR_SEQUENCIA),
  CONSTRAINT PTUSRRS_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_NOTA_SERVICO_RRS TO NIVEL_1;

