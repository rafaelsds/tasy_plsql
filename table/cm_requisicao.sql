ALTER TABLE TASY.CM_REQUISICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_REQUISICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_REQUISICAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_REQUISICAO           DATE,
  DT_LIBERACAO            DATE,
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  DT_BAIXA                DATE,
  CD_PESSOA_REQUISITANTE  VARCHAR2(10 BYTE)     NOT NULL,
  DT_IMPRESSAO            DATE,
  NR_SEQ_AGENDA           NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  CD_ESTAB_DESTINO        NUMBER(4),
  NM_USUARIO_RECEB        VARCHAR2(15 BYTE),
  NM_USUARIO_CONF_ATEND   VARCHAR2(15 BYTE),
  DT_CONF_ATEND           DATE,
  DT_CONFIRMA_RECEB       DATE,
  IE_URGENTE              VARCHAR2(1 BYTE)      NOT NULL,
  NR_ATENDIMENTO          NUMBER(10),
  NR_CIRURGIA             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMREQUI_AGEPACO_FK_I ON TASY.CM_REQUISICAO
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMREQUI_AGEPACO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMREQUI_ATEPACI_FK_I ON TASY.CM_REQUISICAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMREQUI_ESTABEL_FK_I ON TASY.CM_REQUISICAO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMREQUI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMREQUI_PESFISI_FK_I ON TASY.CM_REQUISICAO
(CD_PESSOA_REQUISITANTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CMREQUI_PK ON TASY.CM_REQUISICAO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CMREQUI_SETATEN_FK_I ON TASY.CM_REQUISICAO
(CD_SETOR_ATENDIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMREQUI_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cm_requisicao_tie
after insert or update or delete ON TASY.CM_REQUISICAO for each row
declare

json_w		philips_json;
json_data_w	clob;

begin

if (wheb_usuario_pck.get_nm_usuario is not null) then

	if (inserting or updating) then

		json_w := philips_json();
		json_w.put('id', :new.nr_sequencia);
		json_w.put('urgent', :new.ie_urgente);
		json_w.put('requisitionDate', :new.dt_requisicao);
		json_w.put('department', :new.cd_setor_atendimento);
		json_w.put('requester', :new.cd_pessoa_requisitante);
		json_w.put('establishment', :new.cd_estabelecimento);
		json_w.put('settlementDate', :new.dt_baixa);
		json_w.put('releaseDate', :new.dt_liberacao);
		json_w.put('printingDate', :new.dt_impressao);
		json_w.put('schedule', :new.nr_seq_agenda);
		json_w.put('notes', :new.ds_observacao);
		json_w.put('destinationEstablishment', :new.cd_estab_destino);
		json_w.put('receiptUser', :new.nm_usuario_receb);
		json_w.put('receiptDate', :new.dt_confirma_receb);
		json_w.put('encounter', :new.nr_atendimento);
		json_w.put('encounterUser', :new.nm_usuario_conf_atend);
		json_w.put('encounterDate', :new.dt_conf_atend);
		json_w.put('surgery', :new.nr_cirurgia);
		json_w.put('lastUpdate', :new.dt_atualizacao);
		json_w.put('lastUpdatedBy', :new.nm_usuario);
		json_w.put('creationDate', :new.dt_atualizacao_nrec);
		json_w.put('creationUser', :new.nm_usuario_nrec);

		dbms_lob.createtemporary(json_data_w, true);
		json_w.to_clob(json_data_w);

		if (inserting) then
			json_data_w := bifrost.send_integration_content('cssd.management.add.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);
		elsif (updating) then
			json_data_w := bifrost.send_integration_content('cssd.management.update.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);
		end if;

	elsif (deleting) then

		json_w := philips_json();
		json_w.put('id', :old.nr_sequencia);

		dbms_lob.createtemporary(json_data_w, true);
		json_w.to_clob(json_data_w);

		json_data_w := bifrost.send_integration_content('cssd.management.delete.send.request', json_data_w, wheb_usuario_pck.get_nm_usuario);

	end if;

end if;

end;
/


ALTER TABLE TASY.CM_REQUISICAO ADD (
  CONSTRAINT CMREQUI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_REQUISICAO ADD (
  CONSTRAINT CMREQUI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CMREQUI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CMREQUI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_REQUISITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CMREQUI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CMREQUI_AGEPACO_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.CM_REQUISICAO TO NIVEL_1;

