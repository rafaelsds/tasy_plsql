ALTER TABLE TASY.SUS_ORTESE_PROTESE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_ORTESE_PROTESE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_ORTESE_PROTESE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_PROC_PRINCIPAL     NUMBER(15)              NOT NULL,
  IE_ORIGEM_PROC_PRINC  NUMBER(10)              NOT NULL,
  CD_PROCEDIMENTO       NUMBER(15)              NOT NULL,
  IE_ORIGEM_PROCED      NUMBER(10)              NOT NULL,
  QT_MAX_PROCEDIMENTO   NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSORPR_ESTABEL_FK_I ON TASY.SUS_ORTESE_PROTESE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSORPR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUSORPR_PK ON TASY.SUS_ORTESE_PROTESE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSORPR_PROCEDI_FK_I ON TASY.SUS_ORTESE_PROTESE
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSORPR_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSORPR_PROCEDI_FK1_I ON TASY.SUS_ORTESE_PROTESE
(CD_PROC_PRINCIPAL, IE_ORIGEM_PROC_PRINC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSORPR_PROCEDI_FK1_I
  MONITORING USAGE;


ALTER TABLE TASY.SUS_ORTESE_PROTESE ADD (
  CONSTRAINT SUSORPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_ORTESE_PROTESE ADD (
  CONSTRAINT SUSORPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SUSORPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED)
    ON DELETE CASCADE,
  CONSTRAINT SUSORPR_PROCEDI_FK1 
 FOREIGN KEY (CD_PROC_PRINCIPAL, IE_ORIGEM_PROC_PRINC) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUS_ORTESE_PROTESE TO NIVEL_1;

