ALTER TABLE TASY.IHC_MED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IHC_MED CASCADE CONSTRAINTS;

CREATE TABLE TASY.IHC_MED
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CLAIM         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  VL_CHARGE            VARCHAR2(12 BYTE),
  IE_CHARGE_RAISED     VARCHAR2(1 BYTE),
  CD_PAYEE_PROVIDER    VARCHAR2(10 BYTE),
  CD_SERVICE           VARCHAR2(15 BYTE),
  IE_SERVICE_TYPE      VARCHAR2(1 BYTE),
  DT_SERVICE           DATE,
  NM_SERV_PROVIDER     VARCHAR2(40 BYTE),
  CD_PROVIDER          VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IHCMEDE_IHCCLAI_FK_I ON TASY.IHC_MED
(NR_SEQ_CLAIM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.IHCMEDE_PK ON TASY.IHC_MED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IHC_MED ADD (
  CONSTRAINT IHCMEDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IHC_MED ADD (
  CONSTRAINT IHCMEDE_IHCCLAI_FK 
 FOREIGN KEY (NR_SEQ_CLAIM) 
 REFERENCES TASY.IHC_CLAIM (NR_SEQUENCIA));

GRANT SELECT ON TASY.IHC_MED TO NIVEL_1;

