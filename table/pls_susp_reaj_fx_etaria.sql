ALTER TABLE TASY.PLS_SUSP_REAJ_FX_ETARIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SUSP_REAJ_FX_ETARIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SUSP_REAJ_FX_ETARIA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  IE_TIPO_ESTIPULANTE      VARCHAR2(2 BYTE),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA       DATE,
  DT_FIM_VIGENCIA          DATE,
  NR_SEQ_GRUPO_RELAC       NUMBER(10),
  IE_TIPO_CONTRATACAO      VARCHAR2(2 BYTE),
  IE_REGULAMENTACAO        VARCHAR2(2 BYTE)     DEFAULT null,
  IE_SUSP_REAJ_APLICADO    VARCHAR2(1 BYTE)     DEFAULT null,
  DT_INICIO_SUSP_APLICADO  DATE                 DEFAULT null,
  DT_FIM_SUSP_APLICADO     DATE                 DEFAULT null,
  NR_SEQ_TIPO_ADITAMENTO   NUMBER(10)           DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSRFE_ESTABEL_FK_I ON TASY.PLS_SUSP_REAJ_FX_ETARIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSSRFE_PK ON TASY.PLS_SUSP_REAJ_FX_ETARIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRFE_PLSGRCO_FK_I ON TASY.PLS_SUSP_REAJ_FX_ETARIA
(NR_SEQ_GRUPO_RELAC)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRFE_PLSTPAD_FK_I ON TASY.PLS_SUSP_REAJ_FX_ETARIA
(NR_SEQ_TIPO_ADITAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_SUSP_REAJ_FX_ETARIA ADD (
  CONSTRAINT PLSSRFE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SUSP_REAJ_FX_ETARIA ADD (
  CONSTRAINT PLSSRFE_PLSTPAD_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ADITAMENTO) 
 REFERENCES TASY.PLS_TIPO_ADITAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSSRFE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSSRFE_PLSGRCO_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_RELAC) 
 REFERENCES TASY.PLS_GRUPO_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SUSP_REAJ_FX_ETARIA TO NIVEL_1;

