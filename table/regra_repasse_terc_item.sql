ALTER TABLE TASY.REGRA_REPASSE_TERC_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_REPASSE_TERC_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_REPASSE_TERC_ITEM
(
  CD_REGRA                   NUMBER(5)          NOT NULL,
  NR_SEQ_ITEM                NUMBER(5)          NOT NULL,
  TX_REPASSE                 NUMBER(15,4)       NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_CGC                     VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  TX_DESCONTO                NUMBER(15,4),
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  TX_MEDICO                  NUMBER(15,4)       NOT NULL,
  TX_ANESTESISTA             NUMBER(15,4)       NOT NULL,
  TX_MATERIAIS               NUMBER(15,4)       NOT NULL,
  TX_AUXILIARES              NUMBER(15,4)       NOT NULL,
  TX_CUSTO_OPERACIONAL       NUMBER(15,4)       NOT NULL,
  CD_CONTA_CONTABIL          VARCHAR2(20 BYTE),
  NR_SEQ_TERCEIRO            NUMBER(10),
  VL_REPASSE                 NUMBER(15,2)       NOT NULL,
  IE_BENEFICIARIO            VARCHAR2(1 BYTE)   NOT NULL,
  IE_PERC_SALDO              VARCHAR2(1 BYTE)   NOT NULL,
  IE_RESTRICAO_MEDICO        NUMBER(2),
  IE_GRAVACAO_MEDICO         VARCHAR2(3 BYTE)   NOT NULL,
  IE_REGRA_SALDO             VARCHAR2(1 BYTE)   NOT NULL,
  IE_POR_UNIDADE             VARCHAR2(1 BYTE),
  IE_PERC_EXECUTOR           VARCHAR2(1 BYTE),
  IE_FUNCAO_MEDICO           NUMBER(3),
  DT_INICIO_VIGENCIA         DATE,
  DT_FIM_VIGENCIA            DATE,
  NR_SEQ_CATEGORIA           NUMBER(10),
  IE_TIPO_TERCEIRO           VARCHAR2(1 BYTE),
  IE_GERA_ITEM_ZERADO        VARCHAR2(15 BYTE),
  IE_RECEBE_REGRA_PONTUACAO  VARCHAR2(15 BYTE),
  IE_VALIDA_TERCEIRO_FORMA   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGRETI_CONCONT_FK_I ON TASY.REGRA_REPASSE_TERC_ITEM
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGRETI_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGRETI_FUNMEDI_FK_I ON TASY.REGRA_REPASSE_TERC_ITEM
(IE_FUNCAO_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGRETI_FUNMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGRETI_MEDCATE_FK_I ON TASY.REGRA_REPASSE_TERC_ITEM
(NR_SEQ_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGRETI_PESFISI_FK_I ON TASY.REGRA_REPASSE_TERC_ITEM
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGRETI_PESJURI_FK_I ON TASY.REGRA_REPASSE_TERC_ITEM
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGRETI_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGRETI_PK ON TASY.REGRA_REPASSE_TERC_ITEM
(CD_REGRA, NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGRETI_REGRETE_FK_I ON TASY.REGRA_REPASSE_TERC_ITEM
(CD_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGRETI_REGRETE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGRETI_TERCEIR_FK_I ON TASY.REGRA_REPASSE_TERC_ITEM
(NR_SEQ_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGRETI_TERCEIR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_REPASSE_TERC_ITEM_tp  after update ON TASY.REGRA_REPASSE_TERC_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_REGRA='||to_char(:old.CD_REGRA)||'#@#@NR_SEQ_ITEM='||to_char(:old.NR_SEQ_ITEM);gravar_log_alteracao(substr(:old.NR_SEQ_ITEM,1,4000),substr(:new.NR_SEQ_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ITEM',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_MEDICO,1,4000),substr(:new.TX_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'TX_MEDICO',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_REGRA,1,4000),substr(:new.CD_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'CD_REGRA',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BENEFICIARIO,1,4000),substr(:new.IE_BENEFICIARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BENEFICIARIO',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TERCEIRO,1,4000),substr(:new.NR_SEQ_TERCEIRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TERCEIRO',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_REPASSE,1,4000),substr(:new.VL_REPASSE,1,4000),:new.nm_usuario,nr_seq_w,'VL_REPASSE',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERC_SALDO,1,4000),substr(:new.IE_PERC_SALDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERC_SALDO',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRAVACAO_MEDICO,1,4000),substr(:new.IE_GRAVACAO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAVACAO_MEDICO',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESTRICAO_MEDICO,1,4000),substr(:new.IE_RESTRICAO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESTRICAO_MEDICO',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_SALDO,1,4000),substr(:new.IE_REGRA_SALDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_SALDO',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_ANESTESISTA,1,4000),substr(:new.TX_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'TX_ANESTESISTA',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AUXILIARES,1,4000),substr(:new.TX_AUXILIARES,1,4000),:new.nm_usuario,nr_seq_w,'TX_AUXILIARES',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_CUSTO_OPERACIONAL,1,4000),substr(:new.TX_CUSTO_OPERACIONAL,1,4000),:new.nm_usuario,nr_seq_w,'TX_CUSTO_OPERACIONAL',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_MATERIAIS,1,4000),substr(:new.TX_MATERIAIS,1,4000),:new.nm_usuario,nr_seq_w,'TX_MATERIAIS',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_REPASSE,1,4000),substr(:new.TX_REPASSE,1,4000),:new.nm_usuario,nr_seq_w,'TX_REPASSE',ie_log_w,ds_w,'REGRA_REPASSE_TERC_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_REPASSE_TERC_ITEM ADD (
  CONSTRAINT REGRETI_PK
 PRIMARY KEY
 (CD_REGRA, NR_SEQ_ITEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_REPASSE_TERC_ITEM ADD (
  CONSTRAINT REGRETI_MEDCATE_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA) 
 REFERENCES TASY.MEDICO_CATEGORIA (NR_SEQUENCIA),
  CONSTRAINT REGRETI_FUNMEDI_FK 
 FOREIGN KEY (IE_FUNCAO_MEDICO) 
 REFERENCES TASY.FUNCAO_MEDICO (CD_FUNCAO),
  CONSTRAINT REGRETI_TERCEIR_FK 
 FOREIGN KEY (NR_SEQ_TERCEIRO) 
 REFERENCES TASY.TERCEIRO (NR_SEQUENCIA),
  CONSTRAINT REGRETI_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT REGRETI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REGRETI_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT REGRETI_REGRETE_FK 
 FOREIGN KEY (CD_REGRA) 
 REFERENCES TASY.REGRA_REPASSE_TERCEIRO (CD_REGRA));

GRANT SELECT ON TASY.REGRA_REPASSE_TERC_ITEM TO NIVEL_1;

