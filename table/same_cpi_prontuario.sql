ALTER TABLE TASY.SAME_CPI_PRONTUARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAME_CPI_PRONTUARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAME_CPI_PRONTUARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_PRONTUARIO        NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  QT_VOLUME            NUMBER(3)                NOT NULL,
  DS_LOCALIZACAO       VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          26M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SAMCPPR_ESTABEL_FK_I ON TASY.SAME_CPI_PRONTUARIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SAMCPPR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SAMCPPR_PESFISI_FK_I ON TASY.SAME_CPI_PRONTUARIO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SAMCPPR_PK ON TASY.SAME_CPI_PRONTUARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SAMCPPR_UK ON TASY.SAME_CPI_PRONTUARIO
(CD_ESTABELECIMENTO, NR_PRONTUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.same_cpi_prontuario_update
before insert or update ON TASY.SAME_CPI_PRONTUARIO for each row
declare
nr_prontuario_w		number(10,0);

pragma autonomous_transaction;
begin
if 	(:new.nr_prontuario is not null)  then
	nr_prontuario_w := nvl(obter_prontuario_pf(:new.cd_estabelecimento, :new.cd_pessoa_fisica),0);

	if	((nr_prontuario_w > 0) and
		(:new.nr_prontuario <> nr_prontuario_w)) then
		-- ESTE LOG EST� CADASTRADO NO Sistema\Dicion�rio de Dados\Tipos de log PARA GERAR AT� 16/09/2016. OS 934797
		gravar_log_tasy(4040,substr('NR_PRONTUARIO='||:new.nr_prontuario||'-CD_PESSOA_FISICA='||:new.cd_pessoa_fisica||' - '||dbms_utility.format_call_stack,1,2000), :new.nm_usuario);
		commit;
	end if;
end if;

end;
/


ALTER TABLE TASY.SAME_CPI_PRONTUARIO ADD (
  CONSTRAINT SAMCPPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          11M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT SAMCPPR_UK
 UNIQUE (CD_ESTABELECIMENTO, NR_PRONTUARIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          11M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAME_CPI_PRONTUARIO ADD (
  CONSTRAINT SAMCPPR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SAMCPPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.SAME_CPI_PRONTUARIO TO NIVEL_1;

