ALTER TABLE TASY.IND_DATA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IND_DATA CASCADE CONSTRAINTS;

CREATE TABLE TASY.IND_DATA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_INDICADOR        NUMBER(10)            NOT NULL,
  IE_TIPO_DATA            VARCHAR2(5 BYTE)      NOT NULL,
  DS_DATA                 VARCHAR2(255 BYTE),
  DS_ATRIBUTO             VARCHAR2(255 BYTE)    NOT NULL,
  CD_EXP_DATA             NUMBER(10),
  DS_OBJETIVO             VARCHAR2(4000 BYTE),
  NR_SEQ_APRES            NUMBER(3),
  NM_TABELA_REFERENCIA    VARCHAR2(50 BYTE)     DEFAULT null,
  NM_ATRIBUTO_REFERENCIA  VARCHAR2(50 BYTE)     DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INDDATA_INDBASE_FK_I ON TASY.IND_DATA
(NR_SEQ_INDICADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INDDATA_PK ON TASY.IND_DATA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.IND_DATA_tp  after update ON TASY.IND_DATA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_ATRIBUTO,1,500);gravar_log_alteracao(substr(:old.DS_ATRIBUTO,1,4000),substr(:new.DS_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ATRIBUTO',ie_log_w,ds_w,'IND_DATA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.IND_DATA ADD (
  CONSTRAINT INDDATA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.IND_DATA ADD (
  CONSTRAINT INDDATA_INDBASE_FK 
 FOREIGN KEY (NR_SEQ_INDICADOR) 
 REFERENCES TASY.IND_BASE (NR_SEQUENCIA));

GRANT SELECT ON TASY.IND_DATA TO NIVEL_1;

