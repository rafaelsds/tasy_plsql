ALTER TABLE TASY.USUARIO_ESTABELECIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USUARIO_ESTABELECIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.USUARIO_ESTABELECIMENTO
(
  NM_USUARIO_PARAM         VARCHAR2(15 BYTE)    NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_SETOR_PADRAO          NUMBER(5),
  NR_SEQ_PERFIL            NUMBER(10),
  CD_CERTIFICADO           VARCHAR2(100 BYTE),
  DS_CERTIFICADORA         VARCHAR2(15 BYTE),
  NR_SEQ_SIGNATURE_ENTITY  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.USUESTA_ESTABEL_FK_I ON TASY.USUARIO_ESTABELECIMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUESTA_PEPPEUS_FK_I ON TASY.USUARIO_ESTABELECIMENTO
(NR_SEQ_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUESTA_PEPPEUS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.USUESTA_PK ON TASY.USUARIO_ESTABELECIMENTO
(NM_USUARIO_PARAM, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUESTA_SETATEN_FK_I ON TASY.USUARIO_ESTABELECIMENTO
(CD_SETOR_PADRAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.USUESTA_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.USUESTA_SIGENTI_FK_I ON TASY.USUARIO_ESTABELECIMENTO
(NR_SEQ_SIGNATURE_ENTITY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUESTA_USUARIO_FK_I ON TASY.USUARIO_ESTABELECIMENTO
(NM_USUARIO_PARAM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.USUARIO_ESTABELECIMENTO_tp  after update ON TASY.USUARIO_ESTABELECIMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NM_USUARIO_PARAM='||to_char(:old.NM_USUARIO_PARAM)||'#@#@CD_ESTABELECIMENTO='||to_char(:old.CD_ESTABELECIMENTO);gravar_log_alteracao(substr(:old.NR_SEQ_PERFIL,1,4000),substr(:new.NR_SEQ_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PERFIL',ie_log_w,ds_w,'USUARIO_ESTABELECIMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.USUARIO_ESTABELECIMENTO ADD (
  CONSTRAINT USUESTA_PK
 PRIMARY KEY
 (NM_USUARIO_PARAM, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.USUARIO_ESTABELECIMENTO ADD (
  CONSTRAINT USUESTA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT USUESTA_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_PARAM) 
 REFERENCES TASY.USUARIO (NM_USUARIO)
    ON DELETE CASCADE,
  CONSTRAINT USUESTA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_PADRAO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT USUESTA_PEPPEUS_FK 
 FOREIGN KEY (NR_SEQ_PERFIL) 
 REFERENCES TASY.PEP_PERFIL_USUARIO (NR_SEQUENCIA),
  CONSTRAINT USUESTA_SIGENTI_FK 
 FOREIGN KEY (NR_SEQ_SIGNATURE_ENTITY) 
 REFERENCES TASY.SIGNATURE_ENTITY (NR_SEQUENCIA));

GRANT SELECT ON TASY.USUARIO_ESTABELECIMENTO TO NIVEL_1;

