ALTER TABLE TASY.USUARIO_CONTROLE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USUARIO_CONTROLE CASCADE CONSTRAINTS;

CREATE TABLE TASY.USUARIO_CONTROLE
(
  NR_SEQUENCIA      NUMBER(10)                  NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL,
  DT_ENTRADA        DATE                        NOT NULL,
  DT_SAIDA          DATE,
  QT_MIN_INTERVALO  NUMBER(5)                   NOT NULL,
  QT_MIN_OS         NUMBER(5)                   NOT NULL,
  QT_MIN_ATIVIDADE  NUMBER(5)                   NOT NULL,
  QT_MIN_TOTAL      NUMBER(5)                   NOT NULL,
  QT_MIN_NREG       NUMBER(5)                   NOT NULL,
  DT_REFERENCIA     DATE                        NOT NULL,
  QT_MIN_LANCHE     NUMBER(5)                   NOT NULL,
  QT_MIN_NORMAL     NUMBER(5)                   NOT NULL,
  QT_MIN_EXTRA      NUMBER(5)                   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.USUCONR_PK ON TASY.USUARIO_CONTROLE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.USUCONR_UK ON TASY.USUARIO_CONTROLE
(NM_USUARIO, DT_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Usuario_controle_atual
BEFORE INSERT OR UPDATE ON TASY.USUARIO_CONTROLE FOR EACH ROW
DECLARE

qt_min_w			Number(15,2);
qt_min_normal_w		Number(15,2);
dt_admissao_w			Date;
qt_dia_semana_w		Number(15,0);
cd_estabelecimento_w		Number(15,0);
qt_feriado_w			Number(15,0);
ie_funcionario_w		Varchar2(01);
sql_query_w				varchar2(4000);
ds_dt_entrada_w			varchar2(4000);
ds_dt_saida_w			varchar2(4000);
ds_qt_diferenca_w		varchar2(4000);
vl_param_59_w			varchar2(1);

BEGIN

if	((user = 'CORP' or user = 'TASY')
	and	wheb_usuario_pck.get_cd_setor_atendimento in (2,4,7)) then

	vl_param_59_w := Obter_Param_Usuario_padrao (296, 59, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);

	sql_query_w :=
	'select	max(dt_entrada)
	from	usuario_controle
	where	nm_usuario = :nm_usuario';

	obter_valor_dinamico_char_bv (sql_query_w, 'nm_usuario=' || :new.nm_usuario, ds_dt_entrada_w);

	if	(ds_dt_entrada_w is not null) then
		begin

			sql_query_w :=
			'select	max(u.dt_saida)
			from	usuario_controle u
			where	u.nm_usuario = :nm_usuario
			and		u.dt_entrada =	(
										select	max(dt_entrada)
										from	usuario_controle
										where	nm_usuario = :nm_usuario
									)';

			obter_valor_dinamico_char_bv (sql_query_w, 'nm_usuario=' || :new.nm_usuario, ds_dt_saida_w);

			sql_query_w :=
			'select	max(u.qt_min_nreg)
			from	usuario_controle u
			where	u.nm_usuario = :nm_usuario
			and		u.dt_entrada =	(
										select	max(dt_entrada)
										from	usuario_controle
										where	nm_usuario = :nm_usuario
									)';

			obter_valor_dinamico_char_bv (sql_query_w, 'nm_usuario=' || :new.nm_usuario, ds_qt_diferenca_w);

			if	((ds_dt_saida_w is null or ds_qt_diferenca_w <> 0)
				and vl_param_59_w = 'S'
				and inserting) then
				begin
					select	pkg_date_formaters.to_varchar(	ds_dt_entrada_w,
										pkg_date_formaters.localize_mask(	'shortThreeLetterMonth',
														pkg_date_formaters.getUserLanguageTag(	wheb_usuario_pck.get_cd_estabelecimento,
																			wheb_usuario_pck.get_nm_usuario
																			)
														),
										wheb_usuario_pck.get_cd_estabelecimento)
					into	ds_dt_entrada_w
					from dual;
					wheb_mensagem_pck.exibir_mensagem_abort(1056384, 'DT_ENTRADA=' || ds_dt_entrada_w);
				end;
			end if;
		end;
	end if;
end if;

select max(nvl(ie_funcionario,'N')),
	max(a.cd_estabelecimento)
into	ie_funcionario_w,
	cd_estabelecimento_w
from 	pessoa_fisica b,
	usuario a
where	a.cd_pessoa_fisica	= b.cd_pessoa_fisica
and	a.nm_usuario 		= :new.nm_usuario;

if	(ie_funcionario_w = 'N') then
	qt_min_normal_w	:= 480;
else
	qt_min_normal_w	:= 528;
end if;

select	Campo_Numerico(Obter_Cod_Dia_Semana(:new.dt_referencia))
into	qt_dia_semana_w
from	dual;

select	count(*)
into	qt_feriado_w
from	feriado
where	cd_estabelecimento	= cd_estabelecimento_w
and	dt_feriado		= :new.dt_referencia;
if	(qt_dia_semana_w in (1,7)) or
	(qt_feriado_w > 0) then
	qt_min_normal_w		:= 0;
end if;

qt_min_w	:= :new.qt_min_total - :new.qt_min_intervalo;
if	(qt_min_w > qt_min_normal_w) then
	:new.qt_min_normal	:= qt_min_normal_w;
	:new.qt_min_extra	:= qt_min_w - qt_min_normal_w;
else
	:new.qt_min_normal	:= qt_min_w;
	:new.qt_min_extra	:= 0;
end if;

END;
/


ALTER TABLE TASY.USUARIO_CONTROLE ADD (
  CONSTRAINT USUCONR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT USUCONR_UK
 UNIQUE (NM_USUARIO, DT_REFERENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.USUARIO_CONTROLE TO NIVEL_1;

