ALTER TABLE TASY.DIAGNOSTICO_ADMISSAO_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIAGNOSTICO_ADMISSAO_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIAGNOSTICO_ADMISSAO_ATEND
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ATENDIMENTO       NUMBER(10)               NOT NULL,
  NR_SEQ_DIAGNOSTICO   NUMBER(10),
  NR_SEQ_ORGAO         NUMBER(10),
  IE_OPERACAO          VARCHAR2(1 BYTE),
  IE_ELETIVA           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIAGADMATE_ATEPACI_FK_I ON TASY.DIAGNOSTICO_ADMISSAO_ATEND
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIAGADMATE_DIAGADMI_FK_I ON TASY.DIAGNOSTICO_ADMISSAO_ATEND
(NR_SEQ_DIAGNOSTICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIAGADMATE_ORGDAGADM_FK_I ON TASY.DIAGNOSTICO_ADMISSAO_ATEND
(NR_SEQ_ORGAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIAGADMATE_PK ON TASY.DIAGNOSTICO_ADMISSAO_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.diagnostico_admissao_ins_upd
  AFTER INSERT OR UPDATE ON TASY.DIAGNOSTICO_ADMISSAO_ATEND   FOR EACH ROW
DECLARE

  json_aux_bb                 philips_json;
  envio_integracao_bb         clob;
  retorno_integracao_bb       clob;
  ds_integracao_orgao_bb      ORGAO_DIAGNOSTICO_ADMISSAO.DS_INTEGRACAO%TYPE;
  ds_integracao_admissao_bb   DIAGNOSTICO_ADMISSAO.DS_INTEGRACAO%TYPE;
  ie_operacao_bb              DIAGNOSTICO_ADMISSAO.IE_OPERACAO%TYPE;

BEGIN

  BEGIN
    SELECT  ds_integracao
    INTO    ds_integracao_orgao_bb
    FROM    ORGAO_DIAGNOSTICO_ADMISSAO
    WHERE   NR_SEQUENCIA = :new.nr_seq_orgao;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      ds_integracao_orgao_bb := NULL;
  END;

  BEGIN
    SELECT  ds_integracao, ie_operacao
    INTO    ds_integracao_admissao_bb, ie_operacao_bb
    FROM    DIAGNOSTICO_ADMISSAO
    WHERE   NR_SEQUENCIA = :new.nr_seq_diagnostico;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      ds_integracao_admissao_bb := NULL;
      ie_operacao_bb := NULL;
  END;

  json_aux_bb := philips_json();
  json_aux_bb.put('typeID', 'ADMITDIAG');
  json_aux_bb.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD HH24:MI:SS.SSSSS'));
  json_aux_bb.put('patientHealthSystemStayID', LPAD(:new.nr_atendimento, 32, 0));
  json_aux_bb.put('admissionDiagnosisID', LPAD(:new.nr_sequencia, 32, 0));

  IF (:new.ie_operacao = 'S') THEN
    json_aux_bb.put('admittedFromOR', 'True');
  ELSE
    json_aux_bb.put('admittedFromOR', 'False');
  END IF;

  IF (:new.ie_eletiva = 'S') THEN
    json_aux_bb.put('admittedElective', 'True');
  ELSE
    json_aux_bb.put('admittedElective', 'False');
  END IF;

  json_aux_bb.put('objectID1', ds_integracao_orgao_bb);
  json_aux_bb.put('objectID2', ds_integracao_admissao_bb);

  IF (ie_operacao_bb IS NOT NULL AND ie_operacao_bb = 'S') THEN
    json_aux_bb.put('objectID3', 'fce7534340a84c5788463850b29a9c43');
  ELSE
    json_aux_bb.put('objectID3', 'cb3a7d6ae4ec431a8c7e3273dd118a8e');
  END IF;

  dbms_lob.createtemporary(envio_integracao_bb, TRUE);
  json_aux_bb.to_clob(envio_integracao_bb);
  SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Admission_Diagnosis',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;

END;
/


ALTER TABLE TASY.DIAGNOSTICO_ADMISSAO_ATEND ADD (
  CONSTRAINT DIAGADMATE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.DIAGNOSTICO_ADMISSAO_ATEND ADD (
  CONSTRAINT DIAGADMATE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT DIAGADMATE_DIAGADMI_FK 
 FOREIGN KEY (NR_SEQ_DIAGNOSTICO) 
 REFERENCES TASY.DIAGNOSTICO_ADMISSAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DIAGADMATE_ORGDAGADM_FK 
 FOREIGN KEY (NR_SEQ_ORGAO) 
 REFERENCES TASY.ORGAO_DIAGNOSTICO_ADMISSAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DIAGNOSTICO_ADMISSAO_ATEND TO NIVEL_1;

