ALTER TABLE TASY.AUTOR_CONV_PACOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUTOR_CONV_PACOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUTOR_CONV_PACOTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQUENCIA_AUTOR   NUMBER(10)               NOT NULL,
  NR_SEQ_PACOTE        NUMBER(10)               NOT NULL,
  IE_GERADO            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUTCNPCT_AUTCONV_FK_I ON TASY.AUTOR_CONV_PACOTE
(NR_SEQUENCIA_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCNPCT_CNVPCTAT_FK_I ON TASY.AUTOR_CONV_PACOTE
(NR_SEQ_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AUTCNPCT_PK ON TASY.AUTOR_CONV_PACOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AUTOR_CONV_PACOTE ADD (
  CONSTRAINT AUTCNPCT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUTOR_CONV_PACOTE ADD (
  CONSTRAINT AUTCNPCT_AUTCONV_FK 
 FOREIGN KEY (NR_SEQUENCIA_AUTOR) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCNPCT_CNVPCTAT_FK 
 FOREIGN KEY (NR_SEQ_PACOTE) 
 REFERENCES TASY.CONVENIO_PACOTE_AUTOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.AUTOR_CONV_PACOTE TO NIVEL_1;

