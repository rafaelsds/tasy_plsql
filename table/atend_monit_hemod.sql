ALTER TABLE TASY.ATEND_MONIT_HEMOD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_MONIT_HEMOD CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_MONIT_HEMOD
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_ATENDIMENTO               NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_MONITORACAO               DATE             NOT NULL,
  QT_ALTURA_CM                 NUMBER(5,2),
  QT_SUPER_CORP                NUMBER(14,2),
  QT_PESO                      NUMBER(15,4),
  QT_FC                        NUMBER(15,4),
  QT_PA_SIST                   NUMBER(15,4),
  QT_PA_DIAST                  NUMBER(15,4),
  QT_PA_SIST_AP                NUMBER(15,4),
  QT_PA_DIAST_AP               NUMBER(15,4),
  QT_PA_OCLUIDA_AP             NUMBER(15,4),
  QT_PVD_SIST                  NUMBER(15,4),
  QT_PVD_DIAST_FINAL           NUMBER(15,4),
  QT_PVC                       NUMBER(15,4),
  QT_PAM                       NUMBER(15,4),
  QT_DEBITO_CARD               NUMBER(15,4),
  QT_PA_MEDIA_AP               NUMBER(15,4),
  TX_INDICE_CARD               NUMBER(15,4),
  QT_VOLUME_SIST               NUMBER(15,4),
  TX_INDICE_SIST               NUMBER(15,4),
  QT_RV_SISTEMICA              NUMBER(15,4),
  QT_RV_PULMONAR               NUMBER(15,4),
  QT_TRAB_SIST_VE              NUMBER(15,4),
  QT_TRAB_SIST_VD              NUMBER(15,4),
  TX_RV_SISTEMICA              NUMBER(15,4),
  TX_RV_PULMONAR               NUMBER(15,4),
  TX_TRAB_SIST_VD              NUMBER(15,4),
  TX_TRAB_SIST_VE              NUMBER(15,4),
  IE_PAM_INFORMADA             VARCHAR2(1 BYTE),
  IE_PA_MEDIA_AP_INFORMADA     VARCHAR2(1 BYTE),
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE),
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  QT_SATUR_VEN_MISTA_OXIGENIO  NUMBER(15,4),
  NR_HORA                      NUMBER(2),
  IE_RELEVANTE_APAP            VARCHAR2(1 BYTE),
  IE_SITUACAO                  VARCHAR2(1 BYTE),
  DT_LIBERACAO                 DATE,
  DT_INATIVACAO                DATE,
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA             VARCHAR2(255 BYTE),
  DT_REFERENCIA                DATE,
  QT_GRAD_CO2                  NUMBER(14,4),
  QT_BE                        NUMBER(15,4),
  QT_DELTA_PVC                 NUMBER(15,4),
  QT_DELTA_PP                  NUMBER(15,4),
  QT_VPS                       NUMBER(15,4),
  CD_SETOR_ATENDIMENTO         NUMBER(5),
  NR_SEQ_PEPO                  NUMBER(10),
  CD_PERFIL_ATIVO              NUMBER(5),
  NR_SEQ_ASSINATURA            NUMBER(10),
  QT_CONT_ART_OXIG             NUMBER(15,4),
  QT_OFERTA_OXIG_TEC           NUMBER(15,4),
  QT_CONSUMO_TISS_OXIG         NUMBER(15,4),
  QT_CONT_VENOSO_OXIG          NUMBER(15,4),
  QT_CONT_ART_VEN_OXIG         NUMBER(15,4),
  NR_SEQ_SINAL_VITAL           NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO    NUMBER(10),
  QT_DELTA_PP_MMHG             NUMBER(15,4),
  IE_IMPORTADO                 VARCHAR2(1 BYTE),
  NR_CIRURGIA                  NUMBER(10),
  QT_PAE                       NUMBER(3),
  IE_INTEGRACAO                VARCHAR2(1 BYTE),
  DT_INICIO_COMP_ART           DATE,
  DT_FIM_COMP_ART              DATE,
  IE_INTEGRACAO_EXTRA          VARCHAR2(1 BYTE),
  DS_UTC                       VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO             VARCHAR2(15 BYTE),
  QT_SATUR_CEREBRAL            NUMBER(15,4),
  DS_UTC_ATUALIZACAO           VARCHAR2(50 BYTE),
  IE_VALOR_FORA_FAIXA          VARCHAR2(3 BYTE),
  QT_VVS                       NUMBER(3),
  QT_SPHB                      NUMBER(3,1),
  QT_PVI                       NUMBER(3),
  NR_SEQ_ATEND_CONS_PEPA       NUMBER(10),
  QT_PA_SIST_FEM               NUMBER(15,4),
  QT_PA_DIAST_FEM              NUMBER(15,4),
  QT_PA_MEDIA_FEM              NUMBER(15,4),
  QT_ID02                      NUMBER(4),
  QT_IV02                      NUMBER(3),
  QT_IPVP                      NUMBER(3,1),
  QT_LDFG                      NUMBER(4),
  QT_IAPF                      NUMBER(3,1),
  QT_FEG                       NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEMOHE_ATCONSPEPA_FK_I ON TASY.ATEND_MONIT_HEMOD
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMOHE_ATESIVI_FK_I ON TASY.ATEND_MONIT_HEMOD
(NR_SEQ_SINAL_VITAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMOHE_ATESIVI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMOHE_CIRURGI_FK_I ON TASY.ATEND_MONIT_HEMOD
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMOHE_CIRURGI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMOHE_I1 ON TASY.ATEND_MONIT_HEMOD
(DT_MONITORACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMOHE_PEPOCIR_FK_I ON TASY.ATEND_MONIT_HEMOD
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMOHE_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMOHE_PERFIL_FK_I ON TASY.ATEND_MONIT_HEMOD
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMOHE_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMOHE_PESFISI_FK_I ON TASY.ATEND_MONIT_HEMOD
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEMOHE_PK ON TASY.ATEND_MONIT_HEMOD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMOHE_SETATEN_FK_I ON TASY.ATEND_MONIT_HEMOD
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMOHE_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMOHE_TASASDI_FK_I ON TASY.ATEND_MONIT_HEMOD
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMOHE_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMOHE_TASASDI_FK2_I ON TASY.ATEND_MONIT_HEMOD
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMOHE_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Atend_Monit_Hemod_Atual
BEFORE INSERT OR UPDATE ON TASY.ATEND_MONIT_HEMOD FOR EACH ROW
DECLARE

ds_hora_w	varchar2(20);
dt_registro_w	date;
dt_apap_w	date;
qt_hora_w	number(15,2);
qt_reg_w		NUMBER(1);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.nr_hora is null) or
	(:new.dt_monitoracao <> :old.dt_monitoracao) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.dt_monitoracao,'hh24'),'hh24'));
	end;
end if;

if	(nvl(:old.DT_MONITORACAO,sysdate+10) <> :new.DT_MONITORACAO) and
	(:new.DT_MONITORACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_MONITORACAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.nr_hora is not null) and
	((:old.nr_hora is null) or
	 (:old.dt_monitoracao is null) or
	(:new.nr_hora <> :old.nr_hora) or
	 (:new.dt_monitoracao <> :old.dt_monitoracao)) then
	begin
	ds_hora_w	:= substr(obter_valor_dominio(2119,:new.nr_hora),1,2);
	dt_registro_w	:= trunc(:new.dt_monitoracao,'hh24');
	dt_apap_w	:= to_date(to_char(:new.dt_monitoracao,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss');
	if	(to_char(round(:new.dt_monitoracao,'hh24'),'hh24') = ds_hora_w) then
		:new.dt_referencia	:= round(:new.dt_monitoracao,'hh24');
	else
		begin
		qt_hora_w	:= (trunc(:new.dt_monitoracao,'hh24') - to_date(to_char(:new.dt_monitoracao,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss')) * 24;
		if	(qt_hora_w > 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_monitoracao + 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w > 0) and
			(qt_hora_w <= 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_monitoracao),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w >= -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_monitoracao),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w < -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_monitoracao - 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		end if;
		end;
	end if;
	end;
end if;

if	(nvl(:new.ie_importado,'N') = 'N') then
	begin
	if	(:new.ie_pam_informada <> 'S') then
		:new.qt_pam		:= (:new.qt_pa_sist + (2 * :new.qt_pa_diast))/3;
	end if;
	if	(:new.ie_pa_media_ap_informada <> 'S') then
		:new.qt_pa_media_ap	:= :new.qt_pa_diast_ap + (:new.qt_pa_sist_ap - :new.qt_pa_ocluida_ap)/3;
	end if;

	:new.qt_pa_media_fem	:= trunc((:new.qt_pa_sist_fem + (2 * :new.qt_pa_diast_fem))/3);


	:new.tx_indice_card	:= dividir(:new.qt_debito_card,:new.qt_super_corp);
	:new.qt_volume_sist	:= dividir(:new.qt_debito_card, :new.qt_fc) * 1000;
	:new.tx_indice_sist	:= dividir(:new.tx_indice_card, :new.qt_fc) * 1000;
	:new.qt_rv_sistemica	:= round(dividir((80 * (:new.qt_pam - :new.qt_pvc)),:new.qt_debito_card));
	:new.tx_rv_sistemica	:= round(dividir((80 * (:new.qt_pam - :new.qt_pvc)),:new.tx_indice_card));
	:new.qt_rv_pulmonar	:= dividir((80 * (:new.qt_pa_media_ap - :new.qt_pa_ocluida_ap)),:new.qt_debito_card);
	:new.tx_rv_pulmonar	:= dividir((80 * (:new.qt_pa_media_ap - :new.qt_pa_ocluida_ap)),:new.tx_indice_card);
	:new.qt_trab_sist_ve	:= :new.qt_volume_sist * (:new.qt_pam - :new.qt_pa_ocluida_ap) * 0.0136;
	:new.tx_trab_sist_ve	:= :new.tx_indice_sist * (:new.qt_pam - :new.qt_pa_ocluida_ap) * 0.0136;
	:new.qt_trab_sist_vd	:= :new.qt_volume_sist * (:new.qt_pa_media_ap - :new.qt_pvc) * 0.0136;
	:new.tx_trab_sist_vd	:= :new.tx_indice_sist * (:new.qt_pa_media_ap - :new.qt_pvc) * 0.0136;
	end;
end if;

if (inserting) and (nvl(:new.ie_integracao,'N')	= 'S') then
   record_integration_notify(null,:new.nr_atendimento,'MH',:new.nr_sequencia,null,'S');
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_MONIT_HEMOD_pend_atual 
after insert or update ON TASY.ATEND_MONIT_HEMOD 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_lib_sinal_vital_w	varchar2(10); 
cd_pessoa_fisica_w		varchar2(30); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_lib_sinal_vital) 
into	ie_lib_sinal_vital_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_lib_sinal_vital_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'SVMH'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XSVMH'; 
	end if; 
	 
	select 	max(cd_pessoa_fisica) 
	into	cd_pessoa_fisica_w 
	from	atendimento_paciente 
	where 	nr_atendimento	=	:new.nr_atendimento; 
 
	begin 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, :new.nr_atendimento, :new.nm_usuario); 
	end if; 
	exception 
		when others then 
		null; 
	end; 
	 
end if; 
 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


ALTER TABLE TASY.ATEND_MONIT_HEMOD ADD (
  CONSTRAINT ATEMOHE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_MONIT_HEMOD ADD (
  CONSTRAINT ATEMOHE_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ATEMOHE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEMOHE_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEMOHE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEMOHE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATEMOHE_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEMOHE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATEMOHE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEMOHE_ATESIVI_FK 
 FOREIGN KEY (NR_SEQ_SINAL_VITAL) 
 REFERENCES TASY.ATENDIMENTO_SINAL_VITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_MONIT_HEMOD TO NIVEL_1;

