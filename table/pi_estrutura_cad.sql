ALTER TABLE TASY.PI_ESTRUTURA_CAD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PI_ESTRUTURA_CAD CASCADE CONSTRAINTS;

CREATE TABLE TASY.PI_ESTRUTURA_CAD
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ESTRUTURA     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_PROC_INT      NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PIESCAD_PIESTTR_FK_I ON TASY.PI_ESTRUTURA_CAD
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PIESCAD_PIESTTR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PIESCAD_PK ON TASY.PI_ESTRUTURA_CAD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PIESCAD_PK
  MONITORING USAGE;


CREATE INDEX TASY.PIESCAD_PROINTE_FK_I ON TASY.PI_ESTRUTURA_CAD
(NR_SEQ_PROC_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PIESCAD_PROINTE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cpoe_estrutura_before_del
before delete ON TASY.PI_ESTRUTURA_CAD for each row
declare

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

  update cpoe_procedimento set nr_seq_proc_estrutura = null where nr_seq_proc_estrutura = :old.nr_sequencia;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.cpoe_estrutura_before_ins
before insert ON TASY.PI_ESTRUTURA_CAD for each row
declare

nr_seq_mensagem_w number(15) := 1151002;
ie_existe_proc_w varchar2(1);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

   select nvl(max('S'), 'N')
    into ie_existe_proc_w
    from pi_estrutura_cad c
    where c.nr_seq_estrutura = :new.nr_seq_estrutura
    and c.nr_seq_proc_int = :new.nr_seq_proc_int;

    if(ie_existe_proc_w = 'S') then
        wheb_mensagem_pck.exibir_mensagem_abort(nr_seq_mensagem_w);
    end if;

end if;
end;
/


ALTER TABLE TASY.PI_ESTRUTURA_CAD ADD (
  CONSTRAINT PIESCAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PI_ESTRUTURA_CAD ADD (
  CONSTRAINT PIESCAD_PIESTTR_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PI_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT PIESCAD_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INT) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PI_ESTRUTURA_CAD TO NIVEL_1;

