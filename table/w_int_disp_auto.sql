ALTER TABLE TASY.W_INT_DISP_AUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_INT_DISP_AUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INT_DISP_AUTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ATENDIMENTO       NUMBER(10),
  CD_MATERIAL          NUMBER(6),
  QT_MATERIAL          NUMBER(17,4),
  DS_ARQUIVO           VARCHAR2(255 BYTE),
  IE_LIDO              VARCHAR2(1 BYTE),
  CD_SETOR             NUMBER(4),
  DS_SETOR             VARCHAR2(255 BYTE),
  IE_STATUS            VARCHAR2(1 BYTE),
  IE_TIPO_COBRANCA     VARCHAR2(1 BYTE),
  DS_MENSAGEM          VARCHAR2(255 BYTE),
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WINTDIS_PK ON TASY.W_INT_DISP_AUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINTDIS_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_INT_DISP_AUTO ADD (
  CONSTRAINT WINTDIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_INT_DISP_AUTO TO NIVEL_1;

