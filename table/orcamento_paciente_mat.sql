ALTER TABLE TASY.ORCAMENTO_PACIENTE_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORCAMENTO_PACIENTE_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORCAMENTO_PACIENTE_MAT
(
  NR_SEQUENCIA_ORCAMENTO   NUMBER(10)           NOT NULL,
  CD_MATERIAL              NUMBER(6)            NOT NULL,
  QT_MATERIAL              NUMBER(9,3)          NOT NULL,
  VL_MATERIAL              NUMBER(15,2)         NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  VL_DESCONTO              NUMBER(15,2),
  IE_VALOR_INFORMADO       VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_REGRA_LANC        NUMBER(10),
  VL_CUSTO                 NUMBER(15,2),
  NR_SEQUENCIA             NUMBER(10),
  NR_SEQ_PROC_PRINC        NUMBER(10),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  PR_ORC_PACOTE            NUMBER(15,4),
  IE_ADICIONAL             VARCHAR2(1 BYTE),
  QT_DOSE                  NUMBER(15,4),
  CD_UNIDADE_MEDIDA        VARCHAR2(30 BYTE),
  CD_UNID_MED_PRESCR       VARCHAR2(30 BYTE),
  DS_DIAS_APLICACAO        VARCHAR2(255 BYTE),
  IE_AUTORIZACAO           VARCHAR2(3 BYTE),
  VL_PARTICULAR            NUMBER(15,2),
  QT_ORIGINAL              NUMBER(9,3),
  CD_PROTOCOLO             NUMBER(10),
  NR_SEQ_MEDICACAO         NUMBER(6),
  CD_INTERVALO             VARCHAR2(7 BYTE),
  QT_DIAS_UTIL             NUMBER(3),
  QT_SUPERFICIE_CORPOREA   NUMBER(15,4),
  DS_DIAS_APLIC_ORIG       VARCHAR2(255 BYTE),
  IE_ORIGEM_PRECO          NUMBER(2),
  CD_TAB_PRECO_MATERIAL    NUMBER(4),
  NR_SEQ_REGRA_AJUSTE_MAT  NUMBER(10),
  NR_SEQ_BRAS_PRECO        NUMBER(10),
  NR_SEQ_SIMP_PRECO        NUMBER(10),
  NR_SEQ_MARCA             NUMBER(10),
  VL_MAT_GLOSA             NUMBER(15,2),
  IE_LADO                  VARCHAR2(1 BYTE),
  NR_SEQ_ORCAMENTO_PADRAO  NUMBER(10),
  CD_SETOR_ATENDIMENTO     NUMBER(5),
  IE_REGRA_PLANO           NUMBER(2)            DEFAULT null,
  CD_KIT_MAT_EXEC          NUMBER(10),
  IE_VIA_APLICACAO         VARCHAR2(5 BYTE),
  DS_CICLOS_APLICACAO      VARCHAR2(4000 BYTE),
  DS_CICLOS_APLIC_ORIG     VARCHAR2(4000 BYTE),
  IE_REGRA_APRESENT_ONC    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.ORCPAMA_INTPRES_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPAMA_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPAMA_KITMATE_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(CD_KIT_MAT_EXEC)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPAMA_MATERIA_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(CD_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPAMA_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPAMA_MATMARC_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(CD_MATERIAL, NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPAMA_ORCPACI_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(NR_SEQUENCIA_ORCAMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPAMA_ORCPADR_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(NR_SEQ_ORCAMENTO_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCPAMA_PK ON TASY.ORCAMENTO_PACIENTE_MAT
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCPAMA_REGLAAU_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(NR_SEQ_REGRA_LANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPAMA_REGLAAU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPAMA_SETATEN_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.ORCPAMA_UNIMEDI_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPAMA_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPAMA_UNIMEDI_FK2_I ON TASY.ORCAMENTO_PACIENTE_MAT
(CD_UNID_MED_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCPAMA_UNIMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCPAMA_VIAAPLI_FK_I ON TASY.ORCAMENTO_PACIENTE_MAT
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.orcamento_paciente_mat_atual
before insert or update ON TASY.ORCAMENTO_PACIENTE_MAT for each row
declare

ie_valor_partic_w 	varchar2(1);
cd_estabelecimento_w	number(10);
cd_convenio_partic_w	number(5);
cd_categoria_partic_w	varchar2(20);
dt_orcamento_w		date;
cd_material_w		number(10,0):= null;
ie_material_conta_w	varchar2(1);
cd_convenio_w		number(10,0);
cd_categoria_w		varchar2(10);
ie_regra_qtde_fatur_w	varchar2(1);
qt_material_w		number(9,3);
cd_plano_w		varchar2(10);

cursor c01 is
	select 	cd_material_conta
	from	material_convenio
	where	cd_convenio		= cd_convenio_w
	and	cd_material		= :new.cd_material
	order by nvl(cd_convenio,0);

begin

select	cd_estabelecimento,
	dt_orcamento,
	cd_convenio,
	cd_categoria,
	cd_plano
into	cd_estabelecimento_w,
	dt_orcamento_w,
	cd_convenio_w,
	cd_categoria_w,
	cd_plano_w
from	orcamento_paciente
where	nr_sequencia_orcamento = :new.nr_sequencia_orcamento;

obter_param_usuario(106,50,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_valor_partic_w);
obter_param_usuario(106,77,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_material_conta_w);
obter_param_usuario(106,78,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w, ie_regra_qtde_fatur_w);

if	(nvl(ie_material_conta_w,'N') = 'S') and (inserting) then

	open C01;
	loop
	fetch C01 into
		cd_material_w;
	exit when C01%notfound;
		cd_material_w	:= cd_material_w;
	end loop;
	close c01;

	if	(cd_material_w is null) then
		cd_material_w:= obter_mat_estabelecimento(cd_estabelecimento_w, 0, :new.cd_material ,'MC');
	end if;

	if	(nvl(cd_material_w, :new.cd_material) <> :new.cd_material) then
		if	(:new.ds_observacao is null) then
			--:new.ds_observacao:= 'Material Orig: ' || :new.cd_material;
			:new.ds_observacao:= wheb_mensagem_pck.get_texto(309990, 'CD_MATERIAL=' || :new.cd_material);
		else
			--:new.ds_observacao:= substr(:new.ds_observacao || ' ' || 'Material Orig: ' || :new.cd_material,1,255);
			:new.ds_observacao:= substr(:new.ds_observacao || ' ' || wheb_mensagem_pck.get_texto(309990, 'CD_MATERIAL=' || :new.cd_material),1,255);
		end if;
	end if;

	if	(cd_material_w is not null) then
		:new.cd_material:= cd_material_w;
	end if;

elsif	(nvl(ie_material_conta_w,'N') = 'R') and (inserting) then

	select obter_material_conta(
		cd_estabelecimento_w,
		cd_convenio_w,
		cd_categoria_w,
		null,
		null,
		:new.cd_material,
		cd_plano_w,
		:new.cd_setor_atendimento,
		dt_orcamento_w,
		null,
		null)
	into	cd_material_w
	from	dual;

	if	(nvl(cd_material_w, :new.cd_material) <> :new.cd_material) then
		if	(:new.ds_observacao is null) then
			--:new.ds_observacao:= 'Material Orig: ' || :new.cd_material;
			:new.ds_observacao:= wheb_mensagem_pck.get_texto(309990, 'CD_MATERIAL=' || :new.cd_material);
		else
			--:new.ds_observacao:= substr(:new.ds_observacao || ' ' || 'Material Orig: ' || :new.cd_material,1,255);
			:new.ds_observacao:= substr(:new.ds_observacao || ' ' || wheb_mensagem_pck.get_texto(309990, 'CD_MATERIAL=' || :new.cd_material),1,255);
		end if;
	end if;

	if	(cd_material_w is not null) then
		:new.cd_material:= cd_material_w;
	end if;


end if;


if	(nvl(ie_regra_qtde_fatur_w,'N')	= 'S') and (inserting) then

	select	obter_qt_material_fat_orc(
				cd_estabelecimento_w,
				:new.cd_material,
				cd_convenio_w,
				cd_categoria_w,
				null,
				:new.qt_material,
				null,
				dt_orcamento_w,
				dt_orcamento_w,
				:new.qt_dose,
				:new.cd_unid_med_prescr)
	into	qt_material_w
	from	dual;

	if	(:new.qt_material <> qt_material_w) then
		:new.qt_original	:= :new.qt_material;
		:new.qt_material	:= qt_material_w;
	end if;

end if;



if	(nvl(ie_valor_partic_w,'N') = 'S') and
	(nvl(:new.ie_autorizacao,'L') <> 'L') then

	select 	cd_convenio_partic,
		cd_categoria_partic
	into	cd_convenio_partic_w,
	        cd_categoria_partic_w
	from	parametro_faturamento
	where	cd_estabelecimento = cd_estabelecimento_w;

	if	(cd_convenio_partic_w is not null) and
		(cd_categoria_partic_w is not null) then

	:new.vl_particular := Obter_Preco_Material(	cd_estabelecimento_w,
							cd_convenio_partic_w,
							cd_categoria_partic_w,
							dt_orcamento_w,
							:new.cd_material,
							0,
							0,
							:new.cd_setor_atendimento,
							null,
							0,
							0);
	:new.vl_particular  := :new.vl_particular * :new.qt_material;

	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ORCAMENTO_PACIENTE_MAT_tp  after update ON TASY.ORCAMENTO_PACIENTE_MAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'ORCAMENTO_PACIENTE_MAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ORCAMENTO_PACIENTE_MAT ADD (
  CONSTRAINT ORCPAMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORCAMENTO_PACIENTE_MAT ADD (
  CONSTRAINT ORCPAMA_ORCPADR_FK 
 FOREIGN KEY (NR_SEQ_ORCAMENTO_PADRAO) 
 REFERENCES TASY.ORCAMENTO_PADRAO (NR_SEQUENCIA),
  CONSTRAINT ORCPAMA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ORCPAMA_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MAT_EXEC) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT ORCPAMA_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT ORCPAMA_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT ORCPAMA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT ORCPAMA_ORCPACI_FK 
 FOREIGN KEY (NR_SEQUENCIA_ORCAMENTO) 
 REFERENCES TASY.ORCAMENTO_PACIENTE (NR_SEQUENCIA_ORCAMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ORCPAMA_REGLAAU_FK 
 FOREIGN KEY (NR_SEQ_REGRA_LANC) 
 REFERENCES TASY.REGRA_LANC_AUTOMATICO (NR_SEQUENCIA),
  CONSTRAINT ORCPAMA_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT ORCPAMA_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNID_MED_PRESCR) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT ORCPAMA_MATMARC_FK 
 FOREIGN KEY (CD_MATERIAL, NR_SEQ_MARCA) 
 REFERENCES TASY.MATERIAL_MARCA (CD_MATERIAL,NR_SEQUENCIA));

GRANT SELECT ON TASY.ORCAMENTO_PACIENTE_MAT TO NIVEL_1;

