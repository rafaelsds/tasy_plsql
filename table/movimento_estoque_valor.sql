ALTER TABLE TASY.MOVIMENTO_ESTOQUE_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOVIMENTO_ESTOQUE_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOVIMENTO_ESTOQUE_VALOR
(
  NR_MOVIMENTO_ESTOQUE  NUMBER(10)              NOT NULL,
  CD_TIPO_VALOR         NUMBER(5)               NOT NULL,
  VL_MOVIMENTO          NUMBER(15,4)            NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA_G
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          456M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOESVAL_MOVESTO_FK_I ON TASY.MOVIMENTO_ESTOQUE_VALOR
(NR_MOVIMENTO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          311M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MOESVAL_PK ON TASY.MOVIMENTO_ESTOQUE_VALOR
(NR_MOVIMENTO_ESTOQUE, CD_TIPO_VALOR)
LOGGING
TABLESPACE TASY_INDEX_G
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          328M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOESVAL_TIPOVAL_FK_I ON TASY.MOVIMENTO_ESTOQUE_VALOR
(CD_TIPO_VALOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOESVAL_TIPOVAL_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.movimento_estoque_val_log
before update or insert or delete ON TASY.MOVIMENTO_ESTOQUE_VALOR for each row
begin

if	(inserting) then
	begin
	insert into movimento_estoque_val_log (
			NR_MOVIMENTO_VALOR,
			CD_TIPO_VALOR,
			VL_MOVIMENTO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			IE_ACAO,
			DS_STACK)
	values (
			:new.NR_MOVIMENTO_ESTOQUE,
			:new.CD_TIPO_VALOR,
			nvl(:new.VL_MOVIMENTO,0),
			sysdate,
			:new.NM_USUARIO,
			'I',
			substr(dbms_utility.format_call_stack,1,4000));

	end;
elsif	(updating) then
	begin
	if (:old.NR_MOVIMENTO_ESTOQUE <> :new.NR_MOVIMENTO_ESTOQUE ) and
	(:old.CD_TIPO_VALOR <> :new.CD_TIPO_VALOR ) and
	(nvl(:old.VL_MOVIMENTO,0) <> nvl(:new.VL_MOVIMENTO,0)) then

	insert into movimento_estoque_val_log (
			NR_MOVIMENTO_VALOR,
			CD_TIPO_VALOR,
			VL_MOVIMENTO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			IE_ACAO,
			DS_STACK)
	values (
			:old.NR_MOVIMENTO_ESTOQUE,
			:old.CD_TIPO_VALOR,
			nvl(:old.VL_MOVIMENTO,0),
			sysdate,
			:old.NM_USUARIO,
			'U',
			substr(dbms_utility.format_call_stack,1,4000));
	end if;
	end;
else
	begin

	insert into movimento_estoque_val_log (
			NR_MOVIMENTO_VALOR,
			CD_TIPO_VALOR,
			VL_MOVIMENTO,
			DT_ATUALIZACAO,
			NM_USUARIO,
			IE_ACAO,
			DS_STACK)
	values (
			:old.NR_MOVIMENTO_ESTOQUE,
			:old.CD_TIPO_VALOR,
			nvl(:old.VL_MOVIMENTO,0),
			sysdate,
			:old.NM_USUARIO,
			'D',
			substr(dbms_utility.format_call_stack,1,4000));

	end;
end if;

end;
/


ALTER TABLE TASY.MOVIMENTO_ESTOQUE_VALOR ADD (
  CONSTRAINT MOESVAL_PK
 PRIMARY KEY
 (NR_MOVIMENTO_ESTOQUE, CD_TIPO_VALOR)
    USING INDEX 
    TABLESPACE TASY_INDEX_G
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          328M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOVIMENTO_ESTOQUE_VALOR ADD (
  CONSTRAINT MOESVAL_MOVESTO_FK 
 FOREIGN KEY (NR_MOVIMENTO_ESTOQUE) 
 REFERENCES TASY.MOVIMENTO_ESTOQUE (NR_MOVIMENTO_ESTOQUE)
    ON DELETE CASCADE,
  CONSTRAINT MOESVAL_TIPOVAL_FK 
 FOREIGN KEY (CD_TIPO_VALOR) 
 REFERENCES TASY.TIPO_VALOR (CD_TIPO_VALOR));

GRANT SELECT ON TASY.MOVIMENTO_ESTOQUE_VALOR TO NIVEL_1;

