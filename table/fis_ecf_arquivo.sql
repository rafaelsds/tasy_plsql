ALTER TABLE TASY.FIS_ECF_ARQUIVO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_ECF_ARQUIVO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_ECF_ARQUIVO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONTROLE_ECF  NUMBER(10),
  NR_LINHA             NUMBER(10),
  CD_REGISTRO          VARCHAR2(255 BYTE),
  DS_ARQUIVO           VARCHAR2(4000 BYTE),
  DS_ARQUIVO_COMPL     VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FISECFARQ_FISCONTECF_FK_I ON TASY.FIS_ECF_ARQUIVO
(NR_SEQ_CONTROLE_ECF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FISECFARQ_PK ON TASY.FIS_ECF_ARQUIVO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_ECF_ARQUIVO ADD (
  CONSTRAINT FISECFARQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_ECF_ARQUIVO ADD (
  CONSTRAINT FISECFARQ_FISCONTECF_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE_ECF) 
 REFERENCES TASY.FIS_CONTROLE_ECF (NR_SEQUENCIA));

GRANT SELECT ON TASY.FIS_ECF_ARQUIVO TO NIVEL_1;

