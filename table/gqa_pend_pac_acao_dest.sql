ALTER TABLE TASY.GQA_PEND_PAC_ACAO_DEST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GQA_PEND_PAC_ACAO_DEST CASCADE CONSTRAINTS;

CREATE TABLE TASY.GQA_PEND_PAC_ACAO_DEST
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  NR_SEQ_PEND_PAC_ACAO     NUMBER(10),
  DT_ENCERRAMENTO          DATE,
  DT_CIENCIA               DATE,
  DT_EXECUCAO              DATE,
  DT_JUSTIFICATIVA         DATE,
  DS_JUSTIFICATIVA         VARCHAR2(255 BYTE),
  IE_JUSTIFICADO_AUTO      VARCHAR2(1 BYTE),
  NR_SEQ_JUSTIFICATIVA     NUMBER(10),
  IE_JUSTIFICATIVA_ORIGEM  VARCHAR2(1 BYTE),
  IE_ADVISORY_SEVERITY     VARCHAR2(3 BYTE),
  IE_PROFISSIONAL_MEDICO   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GQAPPAD_GQAJUS_FK_I ON TASY.GQA_PEND_PAC_ACAO_DEST
(NR_SEQ_JUSTIFICATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GQAPPAD_GQAPPAC_FK_I ON TASY.GQA_PEND_PAC_ACAO_DEST
(NR_SEQ_PEND_PAC_ACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GQAPPAD_PESFISI_FK_I ON TASY.GQA_PEND_PAC_ACAO_DEST
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GQAPPAD_PK ON TASY.GQA_PEND_PAC_ACAO_DEST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GQAPPAD_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.HSJ_GQA_PEND_PAC_ACAO_DEST_UPD
AFTER UPDATE ON TASY.GQA_PEND_PAC_ACAO_DEST FOR EACH ROW
DECLARE

nr_seq_escala_w     number(10);
nr_atendimento_w    number(10);
ds_evolucao_w       long;
ds_escala_w         varchar2(255);
ds_protocolo_w      varchar2(255);
qt_sepse_pend_w     varchar2(10);

begin

/*
    SEPSE - Quando m�dico justificar pendencia mentor ir� alterar o status da escala sepse para "Risco de Sepsis grave descartado"
    se a escala estiver com o status "Suspeita Confirmada - Pendente A��o M�dica" no momento da justificativa mentor.
*/

if(:old.dt_encerramento is null and :new.dt_encerramento is not null and obter_pf_usuario(:new.nm_usuario,'C') = :new.cd_pessoa_fisica)then
     
    select max(e.nr_sequencia)
    into nr_seq_escala_w
    from  gqa_pend_pac_acao b 
    join gqa_pendencia_pac c on(b.nr_seq_pend_pac = c.nr_sequencia)
    join gqa_pendencia_regra d on(d.nr_sequencia = c.nr_seq_pend_regra and d.nr_seq_escala = 124)
    join escala_sepse e on(e.nr_sequencia = c.nr_seq_escala)
    where b.nr_sequencia = :new.nr_seq_pend_pac_acao
    and e.ie_status_sepsis in('SM','RN')
    and e.dt_liberacao is not null
    and e.ie_situacao = 'A'
    and e.dt_inativacao is null;
    
    select count(*)
    into qt_sepse_pend_w
    from escala_sepse
    where nr_sequencia = nr_seq_escala_w
    and ie_status_sepsis != 'RD';
    
    
    if nvl(nr_seq_escala_w,0) > 0 and qt_sepse_pend_w > 0 then
    
        update escala_sepse set ie_status_sepsis = 'RD', nm_usuario_status = :new.nm_usuario, dt_liberacao_status = sysdate, ds_justif_status = :new.ds_justificativa
        where nr_sequencia = nr_seq_escala_w;
        
    end if;
        
    
end if;

END HSJ_GQA_PEND_PAC_ACAO_DEST_UPD;
/


CREATE OR REPLACE TRIGGER TASY.HSJ_GQA_PEND_PAC_ACAO_DEST_IST
BEFORE INSERT ON TASY.GQA_PEND_PAC_ACAO_DEST FOR EACH ROW
DECLARE

qt_pend_w      number(10);

begin

    /*
        SEPSE - Caso a pend�ncia esteja ligada em uma escala sepse com status "Suspeita de Sepse descartada" ent�o ir� encerrar a pend�ncia.
        Objetivo: Encerrrar as pend�ncias geradas, pois n�o dever� mostrar mentor para gerar prescri��o de exames novamente.
    */

    select count(*)
    into qt_pend_w
    from  gqa_pend_pac_acao b 
    join gqa_pendencia_pac c on(b.nr_seq_pend_pac = c.nr_sequencia)
    join gqa_pendencia_regra d on(d.nr_sequencia = c.nr_seq_pend_regra and d.nr_seq_escala = 124)
    join escala_sepse e on(e.nr_sequencia = c.nr_seq_escala)
    where b.nr_sequencia = :new.nr_seq_pend_pac_acao
    and b.nr_seq_protocolo is not null
    and e.ie_status_sepsis in('SN')
    and e.dt_liberacao is not null
    and e.ie_situacao = 'A'
    and e.dt_inativacao is null
    and e.dt_liberacao_status is not null;

    if(qt_pend_w > 0)then
    
        :new.dt_encerramento := sysdate;
        :new.dt_justificativa := sysdate;
        :new.ds_justificativa := 'Existe Suspeita Confirmada nas �ltimas 48 horas - Status Autom�tico';
        :new.ie_justificado_auto := 'S';
 
    end if;


END HSJ_GQA_PEND_PAC_ACAO_DEST_IST;
/


ALTER TABLE TASY.GQA_PEND_PAC_ACAO_DEST ADD (
  CONSTRAINT GQAPPAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GQA_PEND_PAC_ACAO_DEST ADD (
  CONSTRAINT GQAPPAD_GQAJUS_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFICATIVA) 
 REFERENCES TASY.GQA_PEND_JUSTIFICATIVA (NR_SEQUENCIA),
  CONSTRAINT GQAPPAD_GQAPPAC_FK 
 FOREIGN KEY (NR_SEQ_PEND_PAC_ACAO) 
 REFERENCES TASY.GQA_PEND_PAC_ACAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT GQAPPAD_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.GQA_PEND_PAC_ACAO_DEST TO NIVEL_1;

