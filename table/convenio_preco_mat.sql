ALTER TABLE TASY.CONVENIO_PRECO_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_PRECO_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_PRECO_MAT
(
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  CD_CONVENIO             NUMBER(5)             NOT NULL,
  CD_CATEGORIA            VARCHAR2(10 BYTE)     NOT NULL,
  DT_LIBERACAO_TABELA     DATE                  NOT NULL,
  CD_TAB_PRECO_MAT        NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  TX_AJUSTE_TABELA_MAT    NUMBER(15,4)          NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE),
  NR_PRIORIDADE           NUMBER(3)             NOT NULL,
  DT_INICIO_VIGENCIA      DATE,
  DT_FINAL_VIGENCIA       DATE,
  IE_TAB_ADICIONAL        VARCHAR2(1 BYTE),
  CD_TAB_MAT_ATUALIZACAO  NUMBER(4),
  IE_INTEGRACAO_OPME      VARCHAR2(1 BYTE),
  CD_GRUPO_MATERIAL       NUMBER(3),
  CD_SUBGRUPO_MATERIAL    NUMBER(3),
  CD_CLASSE_MATERIAL      NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONPRMA_CATCONV_FK_I ON TASY.CONVENIO_PRECO_MAT
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPRMA_CLAMATE_FK_I ON TASY.CONVENIO_PRECO_MAT
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPRMA_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPRMA_GRUMATE_FK_I ON TASY.CONVENIO_PRECO_MAT
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPRMA_GRUMATE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONPRMA_PK ON TASY.CONVENIO_PRECO_MAT
(CD_ESTABELECIMENTO, CD_CONVENIO, CD_CATEGORIA, DT_LIBERACAO_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONPRMA_SUBMATE_FK_I ON TASY.CONVENIO_PRECO_MAT
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPRMA_SUBMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPRMA_TABPRMA_FK_I ON TASY.CONVENIO_PRECO_MAT
(CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONPRMA_TABPRMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONPRMA_TABPRMA_FK2_I ON TASY.CONVENIO_PRECO_MAT
(CD_ESTABELECIMENTO, CD_TAB_MAT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          328K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CONVENIO_PRECO_MAT ADD (
  CONSTRAINT CONPRMA_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO, CD_CONVENIO, CD_CATEGORIA, DT_LIBERACAO_TABELA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_PRECO_MAT ADD (
  CONSTRAINT CONPRMA_TABPRMA_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_TAB_PRECO_MAT) 
 REFERENCES TASY.TABELA_PRECO_MATERIAL (CD_ESTABELECIMENTO,CD_TAB_PRECO_MAT),
  CONSTRAINT CONPRMA_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT CONPRMA_TABPRMA_FK2 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_TAB_MAT_ATUALIZACAO) 
 REFERENCES TASY.TABELA_PRECO_MATERIAL (CD_ESTABELECIMENTO,CD_TAB_PRECO_MAT),
  CONSTRAINT CONPRMA_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT CONPRMA_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT CONPRMA_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.CONVENIO_PRECO_MAT TO NIVEL_1;

