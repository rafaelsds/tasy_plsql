ALTER TABLE TASY.RL_REF_TO_HOSP_DIAGNOSIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RL_REF_TO_HOSP_DIAGNOSIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.RL_REF_TO_HOSP_DIAGNOSIS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_RL_REF_TO_HOSP  NUMBER(10)             NOT NULL,
  NR_ATENDIMENTO         NUMBER(10),
  DT_DIAGNOSTICO         DATE,
  CD_DOENCA              VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RLREFTHD_CIDDOEN_FK_I ON TASY.RL_REF_TO_HOSP_DIAGNOSIS
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RLREFTHD_DIADOEN_FK_I ON TASY.RL_REF_TO_HOSP_DIAGNOSIS
(NR_ATENDIMENTO, DT_DIAGNOSTICO, CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RLREFTHD_PK ON TASY.RL_REF_TO_HOSP_DIAGNOSIS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RLREFTHD_RLREFTH_FK_I ON TASY.RL_REF_TO_HOSP_DIAGNOSIS
(NR_SEQ_RL_REF_TO_HOSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RL_REF_TO_HOSP_DIAGNOSIS ADD (
  CONSTRAINT RLREFTHD_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.RL_REF_TO_HOSP_DIAGNOSIS ADD (
  CONSTRAINT RLREFTHD_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT RLREFTHD_DIADOEN_FK 
 FOREIGN KEY (NR_ATENDIMENTO, DT_DIAGNOSTICO, CD_DOENCA) 
 REFERENCES TASY.DIAGNOSTICO_DOENCA (NR_ATENDIMENTO,DT_DIAGNOSTICO,CD_DOENCA),
  CONSTRAINT RLREFTHD_RLREFTH_FK 
 FOREIGN KEY (NR_SEQ_RL_REF_TO_HOSP) 
 REFERENCES TASY.RL_REF_TO_HOSPITAL (NR_SEQUENCIA)
    ON DELETE CASCADE);

