ALTER TABLE TASY.ESCALA_PROT_GORDURA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PROT_GORDURA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PROT_GORDURA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  NR_SEQ_PROTOCOLO       NUMBER(10)             NOT NULL,
  PR_GORDURA             NUMBER(15,4),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPRGO_ATEPACI_FK_I ON TASY.ESCALA_PROT_GORDURA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPRGO_PESFISI_FK_I ON TASY.ESCALA_PROT_GORDURA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPRGO_PK ON TASY.ESCALA_PROT_GORDURA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCPRGO_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCPRGO_PROTGOR_FK_I ON TASY.ESCALA_PROT_GORDURA
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCPRGO_PROTGOR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_prot_gordura_atual
after insert or update ON TASY.ESCALA_PROT_GORDURA for each row
declare

begin

if	(:old.nr_seq_protocolo is null) or
	(:old.nr_seq_protocolo <> :new.nr_seq_protocolo) then
	delete from escala_prot_gordura_prega where nr_seq_escala = :new.nr_sequencia;

	insert into escala_prot_gordura_prega (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_seq_escala,
		nr_seq_prot_prega,
		qt_medida)
	select	escala_prot_gordura_prega_seq.nextval,
		sysdate,
		:new.nm_usuario_nrec,
		sysdate,
		:new.nm_usuario_nrec,
		:new.nr_sequencia,
		nr_sequencia,
		null
	from	protocolo_gordura_prega
	where	nr_seq_protocolo = :new.nr_seq_protocolo;
end if;

end;
/


ALTER TABLE TASY.ESCALA_PROT_GORDURA ADD (
  CONSTRAINT ESCPRGO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_PROT_GORDURA ADD (
  CONSTRAINT ESCPRGO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPRGO_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCPRGO_PROTGOR_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_GORDURA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESCALA_PROT_GORDURA TO NIVEL_1;

