ALTER TABLE TASY.ESCALA_VTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_VTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_VTE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_AGE                  NUMBER(1),
  IE_MINOR_SURG_PLAN      VARCHAR2(1 BYTE),
  IE_BMI                  VARCHAR2(1 BYTE),
  IE_PRIOR_MAJ_SUR        VARCHAR2(1 BYTE),
  IE_SWOLLEN_LEGS         VARCHAR2(1 BYTE),
  IE_VERICOSE_VEINS       VARCHAR2(1 BYTE),
  IE_SEPSIS               VARCHAR2(1 BYTE),
  IE_COPD                 VARCHAR2(1 BYTE),
  IE_MYOCARDIAL           VARCHAR2(1 BYTE),
  IE_HEART_FAIL           VARCHAR2(1 BYTE),
  IE_HIST_IBD             VARCHAR2(1 BYTE),
  IE_BED_REST             VARCHAR2(1 BYTE),
  IE_PREGNANT             VARCHAR2(1 BYTE),
  IE_ABORTION             VARCHAR2(1 BYTE),
  IE_HARMONE              VARCHAR2(1 BYTE),
  IE_ANTHROSCOPIC         VARCHAR2(1 BYTE),
  IE_OPEN_SURGERY         VARCHAR2(1 BYTE),
  IE_LAPROSCOPIC_SURGERY  VARCHAR2(1 BYTE),
  IE_PRIOR_CANCER         VARCHAR2(1 BYTE),
  IE_PRESENT_CANCER       VARCHAR2(1 BYTE),
  IE_BED                  VARCHAR2(1 BYTE),
  IE_PLASTER              VARCHAR2(1 BYTE),
  IE_VENOUS               VARCHAR2(1 BYTE),
  QT_VTE                  NUMBER(2),
  IE_VTE                  VARCHAR2(1 BYTE),
  IE_VTE_FAMILY           VARCHAR2(1 BYTE),
  IE_CHEMO                VARCHAR2(1 BYTE),
  IE_LIEDEN               VARCHAR2(1 BYTE),
  IE_PROTHROMBIN          VARCHAR2(1 BYTE),
  IE_LUPUS                VARCHAR2(1 BYTE),
  IE_ELEVATED             VARCHAR2(1 BYTE),
  IE_SERUM                VARCHAR2(1 BYTE),
  IE_HEPARIN              VARCHAR2(1 BYTE),
  IE_CONGENITAL           VARCHAR2(1 BYTE),
  IE_MAJOR_SURG_6HRS      VARCHAR2(1 BYTE),
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL,
  IE_STROKE               VARCHAR2(1 BYTE),
  IE_EXTREMITY            VARCHAR2(1 BYTE),
  IE_FRACTURE             VARCHAR2(1 BYTE),
  IE_SPINAL               VARCHAR2(1 BYTE),
  IE_TRAUMA               VARCHAR2(1 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_PROFISSIONAL         VARCHAR2(10 BYTE),
  DT_LIBERACAO            DATE,
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  DT_REGISTRO             DATE,
  IE_NIVEL_ATENCAO        VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCVTE_ATEPACI_FK_I ON TASY.ESCALA_VTE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCVTE_PK ON TASY.ESCALA_VTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_VTE_ATUAL

before insert or update ON TASY.ESCALA_VTE 
for each row
declare

qt_pontuacao_w	number(10);

begin

qt_pontuacao_w	:= 0;

if (:new.ie_age = 1) then
qt_pontuacao_w	:= qt_pontuacao_w +1;
elsif(:new.ie_age = 2) then
qt_pontuacao_w	:= qt_pontuacao_w +2;
elsif (:new.ie_age = 3)then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_MINOR_SURG_PLAN = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_BMI = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_PRIOR_MAJ_SUR = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_SWOLLEN_LEGS = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_VERICOSE_VEINS = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_SEPSIS = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_COPD = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_MYOCARDIAL = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_HEART_FAIL = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_HIST_IBD = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_BED_REST = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_PREGNANT = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_ABORTION = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_HARMONE = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.IE_ANTHROSCOPIC = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +2;
end if;

if (:new.IE_OPEN_SURGERY = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +2;
end if;

if (:new.IE_LAPROSCOPIC_SURGERY = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +2;
end if;

if (:new.IE_PRIOR_CANCER = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +2;
end if;

if (:new.IE_PRESENT_CANCER = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +2;
end if;

if (:new.IE_BED = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +2;
end if;

if (:new.IE_PLASTER = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +2;
end if;

if (:new.IE_VENOUS = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +2;
end if;

if (:new.IE_VTE = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_VTE_FAMILY = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_CHEMO = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_LIEDEN = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_PROTHROMBIN = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_LUPUS = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_ELEVATED = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_SERUM = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_HEPARIN = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_CONGENITAL = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +3;
end if;

if (:new.IE_MAJOR_SURG_6HRS = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +5;
end if;

if (:new.IE_STROKE = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +5;
end if;

if (:new.IE_EXTREMITY = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +5;
end if;

if (:new.IE_FRACTURE = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +5;
end if;

if (:new.IE_SPINAL = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +5;
end if;

if (:new.IE_TRAUMA = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +5;
end if;

:new.qt_vte :=   qt_pontuacao_w;

end;
/


ALTER TABLE TASY.ESCALA_VTE ADD (
  CONSTRAINT ESCVTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_VTE ADD (
  CONSTRAINT ESCVTE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_VTE TO NIVEL_1;

