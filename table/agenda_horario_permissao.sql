ALTER TABLE TASY.AGENDA_HORARIO_PERMISSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_HORARIO_PERMISSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_HORARIO_PERMISSAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_AGENDA            NUMBER(10)               NOT NULL,
  NR_SEQ_HORARIO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEHOPE_AGEHORA_FK_I ON TASY.AGENDA_HORARIO_PERMISSAO
(NR_SEQ_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHOPE_AGENDA_FK_I ON TASY.AGENDA_HORARIO_PERMISSAO
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEHOPE_PESFISI_FK_I ON TASY.AGENDA_HORARIO_PERMISSAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGEHOPE_PK ON TASY.AGENDA_HORARIO_PERMISSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGENDA_HORARIO_PERMISSAO ADD (
  CONSTRAINT AGEHOPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.AGENDA_HORARIO_PERMISSAO ADD (
  CONSTRAINT AGEHOPE_AGEHORA_FK 
 FOREIGN KEY (NR_SEQ_HORARIO) 
 REFERENCES TASY.AGENDA_HORARIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGEHOPE_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA)
    ON DELETE CASCADE,
  CONSTRAINT AGEHOPE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AGENDA_HORARIO_PERMISSAO TO NIVEL_1;

