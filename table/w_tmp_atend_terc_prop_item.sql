ALTER TABLE TASY.W_TMP_ATEND_TERC_PROP_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_TMP_ATEND_TERC_PROP_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_TMP_ATEND_TERC_PROP_ITEM
(
  DT_LIB_INTERCOMPANY  DATE,
  IE_LIB_INTERCOMPANY  VARCHAR2(1 BYTE),
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  MES_REF              DATE                     NOT NULL,
  PREST_ID             NUMBER(5)                NOT NULL,
  GUIA_ID              NUMBER(2)                NOT NULL,
  GUIA_NR              NUMBER(12)               NOT NULL,
  PREST_ID_PPO         NUMBER(5)                NOT NULL,
  ITEM_SEQ             NUMBER(5)                NOT NULL,
  ITEM_EXEC_DT         DATE                     NOT NULL,
  ITEM_PCDM_COD        NUMBER(9)                NOT NULL,
  ITEM_QTD             NUMBER(5,2)              NOT NULL,
  ITEM_EMERG           VARCHAR2(1 BYTE)         NOT NULL,
  ITEM_VLR             NUMBER(14,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WTATPI_PK ON TASY.W_TMP_ATEND_TERC_PROP_ITEM
(MES_REF, PREST_ID, GUIA_ID, GUIA_NR, PREST_ID_PPO, 
ITEM_SEQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_TMP_ATEND_TERC_PROP_ITEM ADD (
  CONSTRAINT WTATPI_PK
 PRIMARY KEY
 (MES_REF, PREST_ID, GUIA_ID, GUIA_NR, PREST_ID_PPO, ITEM_SEQ)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_TMP_ATEND_TERC_PROP_ITEM ADD (
  CONSTRAINT WTATPI_WTATP_FK 
 FOREIGN KEY (MES_REF, PREST_ID, GUIA_ID, GUIA_NR, PREST_ID_PPO) 
 REFERENCES TASY.W_TMP_ATEND_TERC_PROPRIO (MES_REF,PREST_ID,GUIA_ID,GUIA_NR,PREST_ID_PPO));

GRANT SELECT ON TASY.W_TMP_ATEND_TERC_PROP_ITEM TO NIVEL_1;

