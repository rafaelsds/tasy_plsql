ALTER TABLE TASY.W_FLUXO_CAIXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_FLUXO_CAIXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_FLUXO_CAIXA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CONTA_FINANC      NUMBER(10),
  DS_CONTA_FINANC      VARCHAR2(255 BYTE),
  DS_CONTA_ESTRUT      VARCHAR2(255 BYTE),
  NR_SEQ_APRES         NUMBER(10),
  DT_MES_REF           DATE,
  VL_MES_REF           NUMBER(15,2),
  IE_INTEGRACAO        VARCHAR2(4 BYTE),
  IE_PERIODO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WFLUCAI_PK ON TASY.W_FLUXO_CAIXA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFLUCAI_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_FLUXO_CAIXA ADD (
  CONSTRAINT WFLUCAI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_FLUXO_CAIXA TO NIVEL_1;

