ALTER TABLE TASY.PLS_REGRA_POS_ESTAB_EXCE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_POS_ESTAB_EXCE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_POS_ESTAB_EXCE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PRESTADOR      NUMBER(10),
  NR_SEQ_CLINICA        NUMBER(10),
  IE_TIPO_PRESTADOR     VARCHAR2(5 BYTE),
  NR_SEQ_GRUPO_SERVICO  NUMBER(10),
  NR_SEQ_REGRA          NUMBER(10),
  IE_TIPO_GUIA          VARCHAR2(2 BYTE),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  IE_ORIGEM_PROTOCOLO   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRPE_PK ON TASY.PLS_REGRA_POS_ESTAB_EXCE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPE_PLSCLIN_FK_I ON TASY.PLS_REGRA_POS_ESTAB_EXCE
(NR_SEQ_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPE_PLSPRES_FK_I ON TASY.PLS_REGRA_POS_ESTAB_EXCE
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPE_PLSPRGS_FK_I ON TASY.PLS_REGRA_POS_ESTAB_EXCE
(NR_SEQ_GRUPO_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPE_PLSREPES_FK_I ON TASY.PLS_REGRA_POS_ESTAB_EXCE
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPE_PROCEDI_FK_I ON TASY.PLS_REGRA_POS_ESTAB_EXCE
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_POS_ESTAB_EXCE_tp  after update ON TASY.PLS_REGRA_POS_ESTAB_EXCE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PRESTADOR,1,4000),substr(:new.NR_SEQ_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_POS_ESTAB_EXCE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLINICA,1,4000),substr(:new.NR_SEQ_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLINICA',ie_log_w,ds_w,'PLS_REGRA_POS_ESTAB_EXCE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRESTADOR,1,4000),substr(:new.IE_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_POS_ESTAB_EXCE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_SERVICO,1,4000),substr(:new.NR_SEQ_GRUPO_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_SERVICO',ie_log_w,ds_w,'PLS_REGRA_POS_ESTAB_EXCE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_GUIA,1,4000),substr(:new.IE_TIPO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_GUIA',ie_log_w,ds_w,'PLS_REGRA_POS_ESTAB_EXCE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROTOCOLO,1,4000),substr(:new.IE_ORIGEM_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROTOCOLO',ie_log_w,ds_w,'PLS_REGRA_POS_ESTAB_EXCE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_POS_ESTAB_EXCE ADD (
  CONSTRAINT PLSRPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_POS_ESTAB_EXCE ADD (
  CONSTRAINT PLSRPE_PLSCLIN_FK 
 FOREIGN KEY (NR_SEQ_CLINICA) 
 REFERENCES TASY.PLS_CLINICA (NR_SEQUENCIA),
  CONSTRAINT PLSRPE_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSRPE_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERVICO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PLSRPE_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSRPE_PLSREPES_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_POS_ESTABELECIDO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_POS_ESTAB_EXCE TO NIVEL_1;

