ALTER TABLE TASY.AGENDA_CONTROLE_HORARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_CONTROLE_HORARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_CONTROLE_HORARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_AGENDA            NUMBER(10)               NOT NULL,
  DT_AGENDA            DATE                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGCTHOR_AGENDA_FK_I ON TASY.AGENDA_CONTROLE_HORARIO
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGCTHOR_PK ON TASY.AGENDA_CONTROLE_HORARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGCTHOR_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGCTHOR_UK ON TASY.AGENDA_CONTROLE_HORARIO
(CD_AGENDA, DT_AGENDA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.agenda_controle_horario_delete
  AFTER DELETE ON TASY.AGENDA_CONTROLE_HORARIO 
DECLARE
  PRAGMA AUTONOMOUS_TRANSACTION;

  qt_dias_sugestao_w number;
  qt_reg_w number(1) := 0;
BEGIN

  select nvl(max(qt_dias_sugestao), 0)
  into qt_dias_sugestao_w
  from parametro_agenda_integrada;

  select nvl(max(1), 0)
  into qt_reg_w
  from user_scheduler_running_jobs
  where job_name like 'AGINT_GERA_JOB%';

  IF (qt_dias_sugestao_w > 0) and (qt_reg_w = 0) THEN
    dbms_scheduler.create_job(job_name   => 'AGINT_GERA_JOB' ||
                                            to_char(SYSDATE+3/24/60/60, 'JHH24MISS'),
                              job_type   => 'PLSQL_BLOCK',
                              job_action => 'Begin AGEINT_SUGERIR_HORARIOS_PCK.AGEINT_VERIFICA_DIAS_LIBERADOS; END;',
                              enabled    => TRUE,
                              auto_drop  => TRUE);
  END IF;

EXCEPTION
  WHEN OTHERS THEN
    NULL;

END;
/


ALTER TABLE TASY.AGENDA_CONTROLE_HORARIO ADD (
  CONSTRAINT AGCTHOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT AGCTHOR_UK
 UNIQUE (CD_AGENDA, DT_AGENDA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_CONTROLE_HORARIO ADD (
  CONSTRAINT AGCTHOR_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA));

GRANT SELECT ON TASY.AGENDA_CONTROLE_HORARIO TO NIVEL_1;

