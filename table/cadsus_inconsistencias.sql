DROP TABLE TASY.CADSUS_INCONSISTENCIAS CASCADE CONSTRAINTS;

CREATE TABLE TASY.CADSUS_INCONSISTENCIAS
(
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  DS_INCONSISTENCIA  VARCHAR2(4000 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.CADSUS_INCONSISTENCIAS TO NIVEL_1;

