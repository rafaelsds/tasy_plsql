ALTER TABLE TASY.DESENV_STORY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DESENV_STORY CASCADE CONSTRAINTS;

CREATE TABLE TASY.DESENV_STORY
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SPRINT              NUMBER(10),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_FEATURE             NUMBER(10)             NOT NULL,
  DS_STORY               VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DS_DESCRICAO           VARCHAR2(2000 BYTE),
  CD_STATUS              NUMBER(10),
  NR_TEAM                NUMBER(10),
  QT_ESTIMATIVA          NUMBER(13,2),
  DS_CRITERIO_ACEITACAO  VARCHAR2(2000 BYTE),
  NR_PRIORIDADE          NUMBER(10),
  CD_TIPO                VARCHAR2(2 BYTE),
  NR_STORY               NUMBER(10),
  DS_BLOQUEIO            VARCHAR2(255 BYTE),
  IE_EXCLUIDO            VARCHAR2(1 BYTE),
  CD_EXTERNO             VARCHAR2(10 BYTE),
  IE_PRIORIDADE          NUMBER(10),
  DT_PREVISTA            DATE,
  NR_BUILD               VARCHAR2(255 BYTE),
  DS_DETALHE_TECNICO     LONG
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DESSTOR_DESFEAT_FK_I ON TASY.DESENV_STORY
(NR_FEATURE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESSTOR_DESSPRI_FK_I ON TASY.DESENV_STORY
(NR_SPRINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESSTOR_DESSTOR_FK_I ON TASY.DESENV_STORY
(NR_STORY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESSTOR_DESTEAM_FK_I ON TASY.DESENV_STORY
(NR_TEAM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESSTOR_PESFISI_FK_I ON TASY.DESENV_STORY
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DESSTOR_PK ON TASY.DESENV_STORY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.desenv_story_atual
after insert or update ON TASY.DESENV_STORY for each row
declare

cd_tipo_w	varchar2(3);

begin

if	(inserting) then
	cd_tipo_w := 'SC';
else
	cd_tipo_w := 'SU';
end if;

insert into desenv_atividade(
	nr_sequencia,
	nr_epic,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_release,
	nr_release_destino,
	nr_art,
	nr_art_destino,
	cd_tipo,
	nr_feature,
	nr_story,
	cd_status,
	dt_prevista)
values(	desenv_atividade_seq.nextval,
	null,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	null,
	null,
	null,
	null,
	cd_tipo_w,
	null,
	:new.nr_sequencia,
	null,
	:new.dt_prevista);

end;
/


CREATE OR REPLACE TRIGGER TASY.desenv_story_delete
after delete ON TASY.DESENV_STORY for each row
declare

begin

insert into desenv_atividade(
	nr_sequencia,
	nr_epic,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_sprint_destino,
	nr_release,
	nr_release_destino,
	nr_art,
	nr_art_destino,
	cd_tipo,
	nr_feature,
	nr_story,
	nr_sprint)
values(	desenv_atividade_seq.nextval,
	null,
	sysdate,
	:old.nm_usuario,
	sysdate,
	:old.nm_usuario,
	null,
	null,
	null,
	null,
	null,
	'SD',
	null,
	:old.nr_sequencia,
	null);

end;
/


ALTER TABLE TASY.DESENV_STORY ADD (
  CONSTRAINT DESSTOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DESENV_STORY ADD (
  CONSTRAINT DESSTOR_DESSTOR_FK 
 FOREIGN KEY (NR_STORY) 
 REFERENCES TASY.DESENV_STORY (NR_SEQUENCIA),
  CONSTRAINT DESSTOR_DESFEAT_FK 
 FOREIGN KEY (NR_FEATURE) 
 REFERENCES TASY.DESENV_FEATURE (NR_SEQUENCIA),
  CONSTRAINT DESSTOR_DESSPRI_FK 
 FOREIGN KEY (NR_SPRINT) 
 REFERENCES TASY.DESENV_SPRINT (NR_SEQUENCIA),
  CONSTRAINT DESSTOR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT DESSTOR_DESTEAM_FK 
 FOREIGN KEY (NR_TEAM) 
 REFERENCES TASY.DESENV_TEAM (NR_SEQUENCIA));

GRANT SELECT ON TASY.DESENV_STORY TO NIVEL_1;

