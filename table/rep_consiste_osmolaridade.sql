ALTER TABLE TASY.REP_CONSISTE_OSMOLARIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REP_CONSISTE_OSMOLARIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.REP_CONSISTE_OSMOLARIDADE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  QT_INICIAL            NUMBER(15,2)            NOT NULL,
  QT_FINAL              NUMBER(15,2)            NOT NULL,
  DS_MENSAGEM           VARCHAR2(255 BYTE),
  DS_COR                VARCHAR2(50 BYTE),
  IE_PERMITE_LIBERAR    VARCHAR2(1 BYTE),
  IE_VIA_ADMINISTRACAO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REPCONO_PK ON TASY.REP_CONSISTE_OSMOLARIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REP_CONSISTE_OSMOLARIDADE ADD (
  CONSTRAINT REPCONO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REP_CONSISTE_OSMOLARIDADE TO NIVEL_1;

