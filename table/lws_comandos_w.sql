DROP TABLE TASY.LWS_COMANDOS_W CASCADE CONSTRAINTS;

CREATE TABLE TASY.LWS_COMANDOS_W
(
  ID             NUMBER(5),
  ORD            NUMBER(2),
  CAB            VARCHAR2(21 BYTE),
  DATA           VARCHAR2(219 BYTE),
  TERM           VARCHAR2(1 BYTE),
  DT_GERACAO     DATE,
  NR_PRESCRICAO  NUMBER(14)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LWSCOMW_I1 ON TASY.LWS_COMANDOS_W
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LWSCOMW_I1
  MONITORING USAGE;


CREATE INDEX TASY.LWSCOMW_I2 ON TASY.LWS_COMANDOS_W
(DT_GERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LWSCOMW_I2
  MONITORING USAGE;


GRANT SELECT ON TASY.LWS_COMANDOS_W TO NIVEL_1;

