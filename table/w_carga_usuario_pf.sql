DROP TABLE TASY.W_CARGA_USUARIO_PF CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CARGA_USUARIO_PF
(
  NM_PESSOA_FISICA      VARCHAR2(60 BYTE),
  DS_SENHA              VARCHAR2(40 BYTE),
  DT_NASCIMENTO         DATE,
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_CPF                VARCHAR2(11 BYTE),
  NM_USUARIO            VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO    NUMBER(4),
  CD_CARGO              NUMBER(10),
  QT_DIA_SENHA          NUMBER(5),
  CD_FUNCIONARIO        VARCHAR2(15 BYTE),
  CD_PERFIL_INICIAL     NUMBER(5),
  NR_SEQ_CONSELHO       NUMBER(10),
  DS_LOGIN              VARCHAR2(50 BYTE),
  CD_PERFIL_01          NUMBER(5),
  CD_PERFIL_02          NUMBER(5),
  CD_PERFIL_03          NUMBER(5),
  CD_PERFIL_04          NUMBER(5),
  CD_PERFIL_05          NUMBER(5),
  CD_PERFIL_06          NUMBER(5),
  CD_PERFIL_07          NUMBER(5),
  CD_PERFIL_08          NUMBER(5),
  CD_PERFIL_09          NUMBER(5),
  CD_PERFIL_10          NUMBER(5),
  CD_SETOR_01           NUMBER(5),
  CD_SETOR_02           NUMBER(5),
  CD_SETOR_03           NUMBER(5),
  CD_SETOR_04           NUMBER(5),
  CD_SETOR_05           NUMBER(5),
  CD_SETOR_06           NUMBER(5),
  CD_SETOR_07           NUMBER(5),
  CD_SETOR_08           NUMBER(5),
  CD_SETOR_09           NUMBER(5),
  CD_SETOR_10           NUMBER(5),
  DS_CODIGO_PROF        VARCHAR2(15 BYTE),
  IE_TIPO_EVOLUCAO      VARCHAR2(3 BYTE),
  VL_PARAM_87           NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE OR REPLACE TRIGGER TASY.W_CARGA_USUARIO_PF_tp  after update ON TASY.W_CARGA_USUARIO_PF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_w:=substr(:new.NM_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.NM_PESSOA_FISICA,1,4000),substr(:new.NM_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'NM_PESSOA_FISICA',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCIONARIO,1,4000),substr(:new.CD_FUNCIONARIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCIONARIO',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_NASCIMENTO,1,4000),substr(:new.DT_NASCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_NASCIMENTO',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL_INICIAL,1,4000),substr(:new.CD_PERFIL_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL_INICIAL',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA,1,4000),substr(:new.DS_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LOGIN,1,4000),substr(:new.DS_LOGIN,1,4000),:new.nm_usuario,nr_seq_w,'DS_LOGIN',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_SENHA,1,4000),substr(:new.QT_DIA_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_SENHA',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CPF,1,4000),substr(:new.NR_CPF,1,4000),:new.nm_usuario,nr_seq_w,'NR_CPF',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONSELHO,1,4000),substr(:new.NR_SEQ_CONSELHO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONSELHO',ie_log_w,ds_w,'W_CARGA_USUARIO_PF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.w_carga_usuario_pf_insert
after insert ON TASY.W_CARGA_USUARIO_PF for each row
declare
cd_pessoa_fisica_w		varchar2(10);
qt_nm_usuario_w			number(10);
qt_ds_login_w			number(10);

begin
select	count(*)
into	qt_nm_usuario_w
from	usuario a
where	a.nm_usuario = :new.nm_usuario;

select	count(*)
into	qt_ds_login_w
from	usuario a
where	a.ds_login = :new.ds_login;

if	(qt_nm_usuario_w = 0) and
	(qt_ds_login_w = 0) then
	begin
	select	pessoa_fisica_seq.nextval
	into	cd_pessoa_fisica_w
	from	dual;

	insert into pessoa_fisica(
				cd_pessoa_fisica,
				dt_atualizacao,
				ie_tipo_pessoa,
				nm_pessoa_fisica,
				nm_usuario,
				nm_usuario_nrec,
				cd_funcionario,
				cd_cargo,
				nr_cpf,
				dt_nascimento,
				nr_seq_conselho)
			values(	cd_pessoa_fisica_w,
				sysdate,
				2,
				:new.nm_pessoa_fisica,
				'Tasy',
				'Tasy',
				:new.cd_funcionario,
				:new.cd_cargo,
				:new.nr_cpf,
				:new.dt_nascimento,
				:new.nr_seq_conselho);

	insert into usuario(	cd_estabelecimento,
				ds_senha,
				ds_usuario,
				dt_atualizacao,
				ie_anexar_arquivo,
				ie_comunic_interna,
				ie_evento_agenda,
				ie_evento_alerta,
				ie_evento_aprov_compra,
				ie_evento_aprov_doc,
				ie_evento_comunic,
				ie_evento_exame_urg,
				ie_evento_lib_telefone,
				ie_evento_obj_inv,
				ie_evento_ordem_serv,
				ie_evento_prescr,
				ie_evento_proc_agenda,
				ie_evento_processo,
				ie_evento_recoleta,
				ie_evento_sac,
				ie_fechar_tasymon,
				ie_mostrar_anexo_comunic,
				ie_situacao,
				ie_versao_anterior,
				nm_usuario,
				nm_usuario_atual,
				qt_dia_senha,
				cd_pessoa_fisica,
				dt_atualizacao_nrec,
				nm_usuario_orig,
				nm_usuario_nrec,
				cd_perfil_inicial,
				ds_login,
				cd_setor_atendimento,
				ie_tipo_evolucao)
			values(	:new.cd_estabelecimento,
				:new.ds_senha,
				:new.nm_pessoa_fisica,
				sysdate,
				'S',
				'S',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'N',
				'S',
				'S',
				'A',
				'N',
				:new.nm_usuario,
				:new.nm_usuario,
				:new.qt_dia_senha,
				cd_pessoa_fisica_w,
				sysdate,
				'Tasy',
				'Tasy',
				:new.cd_perfil_inicial,
				:new.ds_login,
				:new.cd_setor_atendimento,
				:new.ie_tipo_evolucao);

	if	(nvl(:new.vl_param_87,0) > 0) then
		insert into funcao_param_usuario (	cd_funcao,
							dt_atualizacao,
							nm_usuario,
							nm_usuario_param,
							nr_seq_interno,
							nr_sequencia,
							vl_parametro)
						values(	0,
							sysdate,
							:new.nm_usuario,
							:new.nm_usuario,
							funcao_param_usuario_seq.nextval,
							87,
							:new.vl_param_87);
	end if;
	if	(nvl(:new.cd_setor_01,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_01,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_setor_02,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_02,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_setor_03,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_03,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_setor_04,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_04,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_setor_05,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_05,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_setor_06,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_06,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_setor_07,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_07,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_setor_08,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_08,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_setor_09,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_09,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_setor_10,0) > 0) then
		insert into usuario_setor (	cd_setor_atendimento,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_param)
					values(	:new.cd_setor_10,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_01,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_01,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_02,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_02,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_03,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_03,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_04,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_04,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_05,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_05,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_06,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_06,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_07,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_07,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_08,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_08,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_09,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_09,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	if	(nvl(:new.cd_perfil_10,0) > 0) then
		insert into usuario_perfil (	cd_perfil,
						dt_atualizacao,
						nm_usuario,
						nm_usuario_atual)
					values(	:new.cd_perfil_10,
						sysdate,
						:new.nm_usuario,
						:new.nm_usuario);
	end if;
	end;
end if;

end;
/


GRANT SELECT ON TASY.W_CARGA_USUARIO_PF TO NIVEL_1;

