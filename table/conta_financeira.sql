ALTER TABLE TASY.CONTA_FINANCEIRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_FINANCEIRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_FINANCEIRA
(
  CD_CONTA_FINANC         NUMBER(10)            NOT NULL,
  DS_CONTA_FINANC         VARCHAR2(60 BYTE)     NOT NULL,
  IE_OPER_FLUXO           VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  CD_CONTA_SUPERIOR       NUMBER(10),
  CD_CONTA_APRES          VARCHAR2(20 BYTE),
  DS_COR                  VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO      NUMBER(4),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_EMPRESA              NUMBER(4)             NOT NULL,
  CD_SISTEMA_EXTERNO      VARCHAR2(255 BYTE),
  IE_VARIACAO             VARCHAR2(1 BYTE),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  IE_TOTALIZAR            VARCHAR2(1 BYTE),
  DS_COR_FUNDO            VARCHAR2(15 BYTE),
  IE_MEDIA_REGRA          VARCHAR2(1 BYTE),
  IE_ACUMULAR             VARCHAR2(1 BYTE),
  DS_COR_HTML             VARCHAR2(20 BYTE),
  CD_CONTA_FINANC_GLOBAL  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONFINA_CONFINA_FK_I ON TASY.CONTA_FINANCEIRA
(CD_CONTA_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONFINA_EMPRESA_FK_I ON TASY.CONTA_FINANCEIRA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONFINA_ESTABEL_FK_I ON TASY.CONTA_FINANCEIRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONFINA_PK ON TASY.CONTA_FINANCEIRA
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.conta_fin_bf_ins_upd
before insert or update ON TASY.CONTA_FINANCEIRA for each row
declare
ie_status_w  number;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select count(*)
into ie_status_w
from conta_financ_regra
where nr_seq_conta_financ = :new.cd_conta_financ and obter_bloq_canc_proj_rec(nr_seq_proj_rec) > 0;

if (ie_status_w > 0) then
    wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.CONTA_FINANCEIRA_tp  after update ON TASY.CONTA_FINANCEIRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_CONTA_FINANC);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ACUMULAR,1,4000),substr(:new.IE_ACUMULAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_ACUMULAR',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_SUPERIOR,1,4000),substr(:new.CD_CONTA_SUPERIOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_SUPERIOR',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OPER_FLUXO,1,4000),substr(:new.IE_OPER_FLUXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OPER_FLUXO',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_FINANC,1,4000),substr(:new.CD_CONTA_FINANC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_FINANC',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MEDIA_REGRA,1,4000),substr(:new.IE_MEDIA_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDIA_REGRA',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VARIACAO,1,4000),substr(:new.IE_VARIACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VARIACAO',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TOTALIZAR,1,4000),substr(:new.IE_TOTALIZAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TOTALIZAR',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_APRES,1,4000),substr(:new.CD_CONTA_APRES,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_APRES',ie_log_w,ds_w,'CONTA_FINANCEIRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.CONTA_FINANCEIRA ADD (
  CONSTRAINT CONFINA_PK
 PRIMARY KEY
 (CD_CONTA_FINANC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_FINANCEIRA ADD (
  CONSTRAINT CONFINA_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_SUPERIOR) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT CONFINA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CONFINA_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.CONTA_FINANCEIRA TO NIVEL_1;

