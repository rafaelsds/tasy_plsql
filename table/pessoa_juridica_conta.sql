ALTER TABLE TASY.PESSOA_JURIDICA_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JURIDICA_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JURIDICA_CONTA
(
  CD_CGC                   VARCHAR2(14 BYTE)    NOT NULL,
  CD_BANCO                 NUMBER(5)            NOT NULL,
  CD_AGENCIA_BANCARIA      VARCHAR2(8 BYTE)     NOT NULL,
  NR_CONTA                 VARCHAR2(20 BYTE)    NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  IE_SITUACAO              VARCHAR2(1 BYTE)     NOT NULL,
  NR_DIGITO_CONTA          VARCHAR2(2 BYTE),
  CD_CAMARA_COMPENSACAO    NUMBER(3),
  IE_DIGITO_AGENCIA        VARCHAR2(2 BYTE),
  DS_OBSERVACAO            VARCHAR2(255 BYTE),
  IE_CONTA_PAGAMENTO       VARCHAR2(1 BYTE)     NOT NULL,
  IE_PRESTADOR_PLS         VARCHAR2(1 BYTE),
  CD_CODIGO_IDENTIFICACAO  VARCHAR2(20 BYTE),
  IE_TIPO_CONTA            VARCHAR2(3 BYTE)     NOT NULL,
  IE_PROPRIEDADE_CONTA     VARCHAR2(3 BYTE),
  NR_SEQUENCIA             NUMBER(10),
  IE_CONTA_REPASSE         VARCHAR2(1 BYTE),
  CD_CNPJ_AGENCIA          VARCHAR2(14 BYTE),
  NR_CBU                   VARCHAR2(22 BYTE),
  DS_ALIAS                 VARCHAR2(25 BYTE),
  CD_BIC                   VARCHAR2(11 BYTE),
  CD_IBAN                  VARCHAR2(34 BYTE),
  IE_TIPO_CHAVE_PIX        VARCHAR2(2 BYTE),
  DS_CHAVE_PIX             VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESJUCO_BANCO_FK_I ON TASY.PESSOA_JURIDICA_CONTA
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCO_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCO_PESJURI_AGENCIA_FK_I ON TASY.PESSOA_JURIDICA_CONTA
(CD_CNPJ_AGENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUCO_PESJURI_FK_I ON TASY.PESSOA_JURIDICA_CONTA
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESJUCO_PK ON TASY.PESSOA_JURIDICA_CONTA
(CD_CGC, CD_BANCO, CD_AGENCIA_BANCARIA, NR_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCO_PK
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PESJUCO_UK ON TASY.PESSOA_JURIDICA_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCO_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pessoa_juridica_conta_delete
after delete ON TASY.PESSOA_JURIDICA_CONTA for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
cd_tipo_pessoa_w	pessoa_juridica.cd_tipo_pessoa%type;

begin

select  obter_tipo_pessoa_juridica(:old.cd_cgc)
into	cd_tipo_pessoa_w
from    dual;

reg_integracao_p.ie_operacao		:=	'A';
reg_integracao_p.cd_tipo_pessoa		:=	cd_tipo_pessoa_w;
reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
gerar_int_padrao.gravar_integracao('7', :old.cd_cgc,:old.nm_usuario, reg_integracao_p);

end;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_JURIDICA_CONTA_tp  after update ON TASY.PESSOA_JURIDICA_CONTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_CGC='||to_char(:old.CD_CGC)||'#@#@CD_BANCO='||to_char(:old.CD_BANCO)||'#@#@CD_AGENCIA_BANCARIA='||to_char(:old.CD_AGENCIA_BANCARIA)||'#@#@NR_CONTA='||to_char(:old.NR_CONTA);gravar_log_alteracao(substr(:old.CD_AGENCIA_BANCARIA,1,4000),substr(:new.CD_AGENCIA_BANCARIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_AGENCIA_BANCARIA',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CONTA,1,4000),substr(:new.NR_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_CONTA',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CAMARA_COMPENSACAO,1,4000),substr(:new.CD_CAMARA_COMPENSACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CAMARA_COMPENSACAO',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CODIGO_IDENTIFICACAO,1,4000),substr(:new.CD_CODIGO_IDENTIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CODIGO_IDENTIFICACAO',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DIGITO_CONTA,1,4000),substr(:new.NR_DIGITO_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_DIGITO_CONTA',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIGITO_AGENCIA,1,4000),substr(:new.IE_DIGITO_AGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIGITO_AGENCIA',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTA_PAGAMENTO,1,4000),substr(:new.IE_CONTA_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTA_PAGAMENTO',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRESTADOR_PLS,1,4000),substr(:new.IE_PRESTADOR_PLS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESTADOR_PLS',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_BANCO,1,4000),substr(:new.CD_BANCO,1,4000),:new.nm_usuario,nr_seq_w,'CD_BANCO',ie_log_w,ds_w,'PESSOA_JURIDICA_CONTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_JURIDICA_CONTA ADD (
  CONSTRAINT PESJUCO_PK
 PRIMARY KEY
 (CD_CGC, CD_BANCO, CD_AGENCIA_BANCARIA, NR_CONTA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PESJUCO_UK
 UNIQUE (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          24K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_JURIDICA_CONTA ADD (
  CONSTRAINT PESJUCO_PESJURI_AGENCIA_FK 
 FOREIGN KEY (CD_CNPJ_AGENCIA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PESJUCO_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO)
    ON DELETE CASCADE,
  CONSTRAINT PESJUCO_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PESSOA_JURIDICA_CONTA TO NIVEL_1;

