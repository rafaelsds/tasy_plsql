ALTER TABLE TASY.PROCEDIMENTO_AUTORIZADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCEDIMENTO_AUTORIZADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCEDIMENTO_AUTORIZADO
(
  NR_ATENDIMENTO            NUMBER(10),
  NR_SEQ_AUTORIZACAO        NUMBER(10),
  CD_PROCEDIMENTO           NUMBER(15)          NOT NULL,
  IE_ORIGEM_PROCED          NUMBER(10)          NOT NULL,
  QT_AUTORIZADA             NUMBER(15,3)        NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DS_OBSERVACAO             VARCHAR2(2000 BYTE),
  VL_AUTORIZADO             NUMBER(15,2),
  NR_PRESCRICAO             NUMBER(10),
  NR_SEQ_PRESCRICAO         NUMBER(10),
  NM_USUARIO_APROV          VARCHAR2(15 BYTE),
  DT_APROVACAO              DATE,
  QT_SOLICITADA             NUMBER(15,3),
  NR_SEQUENCIA_AUTOR        NUMBER(10)          NOT NULL,
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_PROC_INTERNO       NUMBER(10),
  CD_PROCEDIMENTO_CONVENIO  VARCHAR2(20 BYTE),
  NR_SEQ_AGENDA             NUMBER(10),
  CD_PROCEDIMENTO_TUSS      NUMBER(15),
  NR_SEQ_REGRA_PLANO        NUMBER(10),
  NR_SEQ_AGENDA_PROC        NUMBER(10),
  NR_SEQ_AGENDA_CONSULTA    NUMBER(10),
  NR_SEQ_EXAME              NUMBER(10),
  NR_SEQ_AGEINT_EXAME_LAB   NUMBER(10),
  IE_ENVIADO_TISS           VARCHAR2(1 BYTE),
  NR_SEQ_AUTOR_PACOTE       NUMBER(10),
  IE_LADO                   VARCHAR2(1 BYTE),
  DS_PROC_CONVENIO          VARCHAR2(240 BYTE),
  NR_SEQ_ITEM_TISS          NUMBER(4),
  CD_MEDICO_RESP            VARCHAR2(10 BYTE),
  QT_BONUS                  NUMBER(10),
  QT_BONUS_SOLIC            NUMBER(3),
  VL_COSEGURO               NUMBER(15,2),
  VL_IVA_COSEGURO           NUMBER(15,2),
  QT_BONUS_APRES            NUMBER(10),
  VL_IVA_AUTORIZADO         NUMBER(15,2),
  VL_ORCAMENTO              NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROAUTO_AGEPACI_FK_I ON TASY.PROCEDIMENTO_AUTORIZADO
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAUTO_AGIEXLB_FK_I ON TASY.PROCEDIMENTO_AUTORIZADO
(NR_SEQ_AGEINT_EXAME_LAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAUTO_AUTCNPCT_FK_I ON TASY.PROCEDIMENTO_AUTORIZADO
(NR_SEQ_AUTOR_PACOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAUTO_AUTCONV_FK_I ON TASY.PROCEDIMENTO_AUTORIZADO
(NR_SEQUENCIA_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAUTO_EXALABO_FK_I ON TASY.PROCEDIMENTO_AUTORIZADO
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAUTO_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROAUTO_I1 ON TASY.PROCEDIMENTO_AUTORIZADO
(NR_ATENDIMENTO, NR_SEQ_AUTORIZACAO, CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROAUTO_PK ON TASY.PROCEDIMENTO_AUTORIZADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAUTO_PRESPRO_FK_I ON TASY.PROCEDIMENTO_AUTORIZADO
(NR_PRESCRICAO, NR_SEQ_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAUTO_PROCEDI_FK_I ON TASY.PROCEDIMENTO_AUTORIZADO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROAUTO_PROINTE_FK_I ON TASY.PROCEDIMENTO_AUTORIZADO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROAUTO_PROINTE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.procedimento_autorizado_update
after update ON TASY.PROCEDIMENTO_AUTORIZADO for each row
DECLARE
--ds_titulo_w			varchar2(255) 	:= 'Log de altera��o no procedimento autorizado';
ds_titulo_w			varchar2(255) 	:= wheb_mensagem_pck.get_texto(311533);
ds_historico_w			varchar2(4000)	:= '';
dt_parametro_w			date		:= trunc(sysdate) + 89399/89400;

ds_atributo_w			varchar2(255);
ds_valor_ant_w			varchar2(2000);
ds_valor_atual_w		varchar2(2000);
ds_valor_desc_ant_w		varchar2(2000);
ds_valor_desc_atual_w		varchar2(2000);
nr_seq_tipo_hist_proc_w		number(10) := 0;
begin

ds_atributo_w	 	:= '';
ds_valor_ant_w		:= '';
ds_valor_atual_w	:= '';

begin
ds_titulo_w	:= 	substr(ds_titulo_w ||' '||:old.cd_procedimento||' - '||
			substr(Obter_Desc_Prescr_Proc(:old.cd_procedimento,:old.ie_origem_proced,:old.nr_seq_proc_interno),1,255),1,255);

if 	(nvl(:new.nr_seq_regra_plano,0) <> nvl(:old.nr_seq_regra_plano,0)) then
	ds_atributo_w	:= 'NR_SEQ_REGRA_PLANO';
	ds_valor_ant_w	:= :old.nr_seq_regra_plano;
	ds_valor_atual_w:= :new.nr_seq_regra_plano;
	--ds_historico_w	:= substr(ds_historico_w||'Campo: NR_SEQ_REGRA_PLANO - Antes: '|| :old.nr_seq_regra_plano ||' Depois: '|| :new.nr_seq_regra_plano ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.cd_procedimento_tuss,0) <> nvl(:old.cd_procedimento_tuss,0)) then
	ds_atributo_w:= 'CD_PROCEDIMENTO_TUSS';
	ds_valor_ant_w	:= :old.cd_procedimento_tuss;
	ds_valor_atual_w:= :new.cd_procedimento_tuss;
	--ds_historico_w	:= substr(ds_historico_w||'Campo: CD_PROCEDIMENTO_TUSS - Antes: '|| :old.cd_procedimento_tuss ||' Depois: '|| :new.cd_procedimento_tuss ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.nr_seq_agenda,0) <> nvl(:old.nr_seq_agenda,0)) then
	ds_atributo_w:= 'NR_SEQ_AGENDA';
	ds_valor_ant_w	:= :old.nr_seq_agenda;
	ds_valor_atual_w:= :new.nr_seq_agenda;
	--ds_historico_w	:= substr(ds_historico_w||'Campo: NR_SEQ_AGENDA - Antes: '|| :old.nr_seq_agenda ||' Depois: '|| :new.nr_seq_agenda ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.cd_procedimento_convenio,0) <> nvl(:old.cd_procedimento_convenio,0)) then
	ds_atributo_w:= 'CD_PROCEDIMENTO_CONVENIO';
	ds_valor_ant_w	:= :old.cd_procedimento_convenio;
	ds_valor_atual_w:= :new.cd_procedimento_convenio;
	--ds_historico_w	:= substr(ds_historico_w||'Campo: CD_PROCEDIMENTO_CONVENIO - Antes: '|| :old.cd_procedimento_convenio ||' Depois: '|| :new.cd_procedimento_convenio ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.nr_seq_proc_interno,0) <> nvl(:old.nr_seq_proc_interno,0)) then
	ds_atributo_w:= 'NR_SEQ_PROC_INTERNO';
	ds_valor_ant_w	:= :old.nr_seq_proc_interno;
	ds_valor_atual_w:= :new.nr_seq_proc_interno;
	--ds_historico_w 	:= substr(ds_historico_w||'Campo: NR_SEQ_PROC_INTERNO - Antes: '|| :old.nr_seq_proc_interno ||' Depois: '|| :new.nr_seq_proc_interno ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.qt_solicitada,0) <> nvl(:old.qt_solicitada,0)) then
	ds_atributo_w:= 'QT_SOLICITADA';
	ds_valor_ant_w	:= :old.qt_solicitada;
	ds_valor_atual_w:= :new.qt_solicitada;
	--ds_historico_w := substr(ds_historico_w||'Campo: QT_SOLICITADA - Antes: '|| :old.qt_solicitada ||' Depois: '|| :new.qt_solicitada ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.dt_aprovacao,dt_parametro_w) <> nvl(:old.dt_aprovacao,dt_parametro_w)) then
	ds_atributo_w:= 'DT_APROVACAO';
	ds_valor_ant_w	:= to_char(:old.dt_aprovacao,'dd/mm/yyyy hh24:mi:ss');
	ds_valor_atual_w:= to_char(:new.dt_aprovacao,'dd/mm/yyyy hh24:mi:ss');
	--ds_historico_w    := substr(ds_historico_w||'Campo: DT_APROVACAO - Antes: '|| to_char(:old.dt_aprovacao,'dd/mm/yyyy hh24:mi:ss') ||' Depois: '|| to_char(:new.dt_aprovacao,'dd/mm/yyyy hh24:mi:ss') ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.nm_usuario_aprov,0) <> nvl(:old.nm_usuario_aprov,0)) then
	ds_atributo_w:= 'NM_USUARIO_APROV';
	ds_valor_ant_w	:= :old.nm_usuario_aprov;
	ds_valor_atual_w:= :new.nm_usuario_aprov;
	--ds_historico_w := substr(ds_historico_w||'Campo: NM_USUARIO_APROV - Antes: '|| :old.nm_usuario_aprov ||' Depois: '|| :new.nm_usuario_aprov ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.nr_seq_prescricao,0) <> nvl(:old.nr_seq_prescricao,0)) then
	ds_atributo_w:= 'NR_SEQ_PRESCRICAO';
	ds_valor_ant_w	:= :old.nr_seq_prescricao;
	ds_valor_atual_w:= :new.nr_seq_prescricao;
	--ds_historico_w := substr(ds_historico_w||'Campo: NR_SEQ_PRESCRICAO - Antes: '|| :old.nr_seq_prescricao ||' Depois: '|| :new.nr_seq_prescricao ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if (nvl(:new.nr_prescricao,0) <> nvl(:old.nr_prescricao,0)) then
	ds_atributo_w:= 'NR_PRESCRICAO';
	ds_valor_ant_w	:= :old.nr_prescricao;
	ds_valor_atual_w:= :new.nr_prescricao;
	--ds_historico_w := substr(ds_historico_w||'Campo: NR_PRESCRICAO - Antes: '|| :old.nr_prescricao ||' Depois: '|| :new.nr_prescricao ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.vl_autorizado,0) <> nvl(:old.vl_autorizado,0)) then
	ds_atributo_w:= 'VL_AUTORIZADO';
	ds_valor_ant_w	:= :old.vl_autorizado;
	ds_valor_atual_w:= :new.vl_autorizado;
	--ds_historico_w := substr(ds_historico_w||'Campo: VL_AUTORIZADO - Antes: '|| :old.vl_autorizado ||' Depois: '|| :new.vl_autorizado ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.ds_observacao,0) <> nvl(:old.ds_observacao,0)) then
	ds_atributo_w:= 'DS_OBSERVACAO';
	ds_valor_ant_w	:= :old.ds_observacao;
	ds_valor_atual_w:= :new.ds_observacao;
	--ds_historico_w := substr(ds_historico_w||'Campo: DS_OBSERVACAO - Antes: '|| :old.ds_observacao ||' Depois: '|| :new.ds_observacao ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.qt_autorizada,0) <> nvl(:old.qt_autorizada,0)) then
	ds_atributo_w:= 'QT_AUTORIZADA';
	ds_valor_ant_w	:= :old.qt_autorizada;
	ds_valor_atual_w:= :new.qt_autorizada;
	--ds_historico_w := substr(ds_historico_w||'Campo: QT_AUTORIZADA - Antes: '|| :old.qt_autorizada ||' Depois: '|| :new.qt_autorizada ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.ie_origem_proced,0) <> nvl(:old.ie_origem_proced,0)) then
	ds_atributo_w:= 'IE_ORIGEM_PROCED';
	ds_valor_ant_w	:= :old.ie_origem_proced;
	ds_valor_atual_w:= :new.ie_origem_proced;
	--ds_historico_w := substr(ds_historico_w||'Campo: IE_ORIGEM_PROCED - Antes: '|| :old.ie_origem_proced ||' Depois: '|| :new.ie_origem_proced ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.cd_procedimento,0) <> nvl(:old.cd_procedimento,0)) then
	ds_atributo_w:= 'CD_PROCEDIMENTO';
	ds_valor_ant_w	:= :old.cd_procedimento;
	ds_valor_atual_w:= :new.cd_procedimento;
	--ds_historico_w   := substr(ds_historico_w||'Campo: CD_PROCEDIMENTO - Antes: '|| :old.cd_procedimento ||' Depois: '|| :new.cd_procedimento ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);

	SELECT	MAX(NR_SEQUENCIA)
	INTO	nr_seq_tipo_hist_proc_w
	FROM	AUTORIZACAO_TIPO_HIST
	WHERE	NVL(IE_TIPO,'X') = 'P'
	and	nvl(ie_situacao,'A') = 'A';

	if	(nr_seq_tipo_hist_proc_w > 0) then
		ds_valor_desc_ant_w	:= ds_valor_ant_w || ' - ' || substr(Obter_Desc_Prescr_Proc(:old.cd_procedimento,:old.ie_origem_proced,:old.nr_seq_proc_interno),1,255);
		ds_valor_desc_atual_w	:= ds_valor_atual_w || ' - '|| substr(Obter_Desc_Prescr_Proc(:new.cd_procedimento,:new.ie_origem_proced,:new.nr_seq_proc_interno),1,255);
	end if;
end if;
if 	(nvl(:new.nr_seq_autorizacao,0) <> nvl(:old.nr_seq_autorizacao,0)) then
	ds_atributo_w:= 'NR_SEQ_AUTORIZACAO';
	ds_valor_ant_w	:= :old.nr_seq_autorizacao;
	ds_valor_atual_w:= :new.nr_seq_autorizacao;
	--ds_historico_w  := substr(ds_historico_w||'Campo: NR_SEQ_AUTORIZACAO - Antes: '|| :old.nr_seq_autorizacao ||' Depois: '|| :new.nr_seq_autorizacao ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
if 	(nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0)) then
	ds_atributo_w:= 'NR_ATENDIMENTO';
	ds_valor_ant_w	:= :old.nr_atendimento;
	ds_valor_atual_w:= :new.nr_atendimento;
	--ds_historico_w  := substr(ds_historico_w||'Campo: NR_ATENDIMENTO - Antes: '|| :old.nr_atendimento ||' Depois: '|| :new.nr_atendimento ||chr(13)||chr(10),1,4000);
	ds_historico_w	:= substr(ds_historico_w|| wheb_mensagem_pck.get_texto(311530,  'DS_ATRIBUTO=' || ds_atributo_w || ';' ||
											'DS_VALOR_ANT=' || ds_valor_ant_w || ';' ||
											'DS_VALOR_ATUAL=' || ds_valor_atual_w) ||chr(13)||chr(10),1,4000);
end if;
exception
when others then
	ds_historico_w := 'X';
end;

if 	(nvl(ds_historico_w,'X') <> 'X') then
	gravar_autor_conv_log_alter(:new.nr_sequencia_autor,ds_titulo_w,substr(ds_historico_w,1,2000),:new.nm_usuario);

	if	(nvl(nr_seq_tipo_hist_proc_w,0) > 0) then

		insert into autorizacao_convenio_hist
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_atendimento,
			nr_seq_autorizacao,
			ds_historico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_sequencia_autor,
			nr_seq_tipo_hist,
			dt_liberacao,
			nm_usuario_lib)
		select	autorizacao_convenio_hist_seq.nextval,
			sysdate,
			:new.nm_usuario,
			:new.nr_atendimento,
			:new.nr_seq_autorizacao,
			WHEB_MENSAGEM_PCK.get_texto(312535) || ' ' || ds_valor_desc_ant_w || chr(13)|| WHEB_MENSAGEM_PCK.get_texto(182372) || ' ' ||ds_valor_desc_atual_w,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia_autor,
			nr_seq_tipo_hist_proc_w,
			sysdate,
			:new.nm_usuario
		from	dual;


	end if;


end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PROCEDIMENTO_AUTOR_BEFINSERT
BEFORE Insert or Update ON TASY.PROCEDIMENTO_AUTORIZADO FOR EACH ROW
DECLARE

ie_tipo_atendimento_w	number(3);
cd_proc_convenio_w	varchar2(20)	:= '';
cd_grupo_w		varchar2(10)	:= '';
nr_seq_conversao_w	number(10);
nr_seq_agenda_consulta_w number(10);
nr_atendimento_w	number(10);
cd_convenio_w		number(5);
cd_plano_w		varchar2(10)	:= '';
ie_clinica_w		number(5);
cd_tipo_acomod_conv_w	number(4);
cd_procedimento_tuss_w	number(15,0);
ie_origem_proc_tuss_w	number(10,0);
cd_estabelecimento_w	number(10);
ie_proc_tuss_autor_w	varchar2(5);
nr_seq_agenda_w		number(10);
qt_idade_w		number(15,0);
cd_pessoa_fisica_w	varchar2(10);
ie_sexo_w		varchar2(1);
cd_empresa_ref_w	number(10,0);
ie_carater_inter_sus_w	varchar2(2);
ie_gerar_historico_w	varchar2(15);
cd_estab_autor_w	estabelecimento.cd_estabelecimento%type;
cd_categoria_w		categoria_convenio.cd_categoria%type;
ds_proc_convenio_w	procedimento_autorizado.ds_proc_convenio%type;
begin
obter_param_usuario(3004,139, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.Get_cd_estabelecimento, ie_gerar_historico_w);
/* OS 51884 - Gravar proc conv�nio */
select	a.cd_convenio,
	a.nr_atendimento,
	a.nr_seq_agenda,
	a.cd_estabelecimento,
	b.cd_categoria,
	a.nr_seq_agenda_consulta
into	cd_convenio_w,
	nr_atendimento_w,
	nr_seq_agenda_w,
	cd_estab_autor_w,
	cd_categoria_w,
	nr_seq_agenda_consulta_w
from	autorizacao_convenio a,
	autorizacao_convenio_tiss b
where	a.nr_sequencia	= :new.nr_sequencia_autor
and	a.nr_sequencia	= b.nr_sequencia_autor(+);


if	(nr_atendimento_w is not null) then

	begin
	select	a.ie_tipo_atendimento,
		a.ie_clinica,
		c.cd_plano_convenio,
		c.CD_EMPRESA,
		c.cd_categoria,
		c.cd_tipo_acomodacao,
		a.cd_pessoa_fisica,
		a.ie_carater_inter_sus,
		p.ie_sexo,
		obter_idade(p.dt_nascimento, nvl(p.dt_obito,sysdate),'DIA'),
		a.cd_estabelecimento
	into	ie_tipo_atendimento_w,
		ie_clinica_w,
		cd_plano_w,
		cd_empresa_ref_w,
		cd_categoria_w,
		cd_tipo_acomod_conv_w,
		cd_pessoa_fisica_w,
		ie_carater_inter_sus_w,
		ie_sexo_w,
		qt_idade_w,
		cd_estabelecimento_w
	from	atendimento_paciente a,
		atend_categoria_convenio c,
		pessoa_fisica p
	where	a.nr_atendimento	= nr_atendimento_w
	and	a.nr_atendimento	= c.nr_atendimento
	and	p.cd_pessoa_fisica	= a.cd_pessoa_fisica
	and	c.nr_seq_interno	= obter_atecaco_atendimento(a.nr_atendimento);
	exception
	when others then
		ie_tipo_atendimento_w 	:= null;
		ie_clinica_w 		:= null;
		cd_plano_w 		:= null;
		cd_empresa_ref_w 	:= null;
		cd_categoria_w 		:= null;
		cd_tipo_acomod_conv_w	:= null;
		cd_pessoa_fisica_w	:= null;
		ie_carater_inter_sus_w	:= null;
		ie_sexo_w		:= null;
		qt_idade_w		:= 0;
	end;

	select 	max(nvl(ie_proc_tuss_autor,'S'))
	into 	ie_proc_tuss_autor_w
	from 	convenio_estabelecimento
	where 	cd_estabelecimento 	= cd_estabelecimento_w
	and 	cd_convenio		= cd_convenio_w;

elsif (nr_seq_agenda_w is not null) then

	begin
	select 	a.cd_estabelecimento,
			b.cd_categoria,
			b.ie_tipo_atendimento,
			b.ie_clinica
	into 	cd_estabelecimento_w,
			cd_categoria_w,
			ie_tipo_atendimento_w,
			ie_clinica_w
	from 	agenda a,
			agenda_paciente b
	where 	a.cd_agenda 	= b.cd_agenda
	and  	b.nr_sequencia 	= nr_seq_agenda_w;
	exception
	when others then
			cd_estabelecimento_w := null;
			cd_categoria_w := null;
			ie_tipo_atendimento_w := null;
			ie_clinica_w := null;
	end;

	select 	max(nvl(ie_proc_tuss_autor,'S'))
	into 	ie_proc_tuss_autor_w
	from 	convenio_estabelecimento
	where 	cd_estabelecimento 	= cd_estabelecimento_w
	and 	cd_convenio		= cd_convenio_w;

elsif (nr_seq_agenda_consulta_w is not null) then

	begin
	select 	a.ie_tipo_atendimento,
			a.cd_categoria,
			obter_estab_agenda(a.cd_agenda),
			a.cd_tipo_acomodacao,
			a.cd_pessoa_fisica
	into 	ie_tipo_atendimento_w,
			cd_categoria_w,
			cd_estabelecimento_w,
			cd_tipo_acomod_conv_w,
			cd_pessoa_fisica_w
	from 	agenda_consulta a
	where 	a.nr_sequencia 	= nr_seq_agenda_consulta_w;
	exception
	when others then
			ie_tipo_atendimento_w := null;
			cd_categoria_w := null;
			cd_estabelecimento_w := null;
			cd_tipo_acomod_conv_w := null;
			cd_pessoa_fisica_w := null;
	end;

end if;

if	(:new.cd_procedimento is not null) then

	converte_proc_convenio(	Nvl(cd_estab_autor_w,cd_estabelecimento_w),
				cd_convenio_w,cd_categoria_w,
				:new.cd_procedimento,
				:new.ie_origem_proced,
				null,
				null,
				ie_tipo_atendimento_w, sysdate,
				cd_proc_convenio_w,
				cd_grupo_w,
				nr_seq_conversao_w,
				null, null, :new.nr_seq_proc_interno , 'A', cd_plano_w,ie_clinica_w,0, null,
				cd_tipo_acomod_conv_w,
				qt_idade_w,
				ie_sexo_w,
				cd_empresa_ref_w,
				ie_carater_inter_sus_w,
				0,
				null,
				null, null);
end if;

/* lhalves OS201957 17/03/2010 - Gerar c�digo do procedimento TUSS */
if	(nvl(ie_proc_tuss_autor_w,'S') = 'S') then

	if 	(:new.nr_seq_proc_interno is not null) then

		cd_procedimento_tuss_w:= Define_procedimento_TUSS(cd_estabelecimento_w,  :new.nr_seq_proc_interno, cd_convenio_w, cd_categoria_w,ie_tipo_atendimento_w, sysdate, 0, 0, null, null, null);
		:new.cd_procedimento_tuss	:= cd_procedimento_tuss_w;


	elsif	(:new.nr_seq_exame is not null) then


		cd_procedimento_tuss_w := define_proc_tuss_exame(:new.nr_seq_exame, cd_convenio_w, cd_categoria_w, null,0);

		if	(cd_procedimento_tuss_w > 0) then
			:new.cd_procedimento_tuss := cd_procedimento_tuss_w;
		end if;

		if	(Nvl(cd_procedimento_tuss_w,0) = 0) then

			select 	max(cd_procedimento_tuss)
			into	cd_procedimento_tuss_w
			from 	exame_laboratorio
			where 	nr_seq_exame = :new.nr_seq_exame;

			if	(Nvl(cd_procedimento_tuss_w,0) > 0) then
				:new.cd_procedimento_tuss	:= cd_procedimento_tuss_w;
			end if;
		end if;


	end if;

end if;


if	(Nvl(nr_seq_conversao_w,0) > 0) then
	select	Nvl(max(DS_PROC_CONVENIO),'0')
	into	ds_proc_convenio_w
	from	CONVERSAO_PROC_CONVENIO
	where	nr_sequencia = nr_seq_conversao_w;


	:new.cd_procedimento_convenio	:= cd_proc_convenio_w;

	if	(nvl(ds_proc_convenio_w,'0') = '0') then
		:new.DS_PROC_CONVENIO	:= null;
	else
		:new.DS_PROC_CONVENIO	:= ds_proc_convenio_w;
	end if;

end if;

if	(nvl(ie_gerar_historico_w, 'N') = 'S') and
	(Inserting) then

	insert into autorizacao_convenio_hist
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		nr_atendimento,
		ds_historico,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_sequencia_autor)
	values	(
		autorizacao_convenio_hist_seq.nextval,
		sysdate,
		:new.nm_usuario,
		:new.nr_atendimento,
		--substr('Inclu�do procedimento na autoriza��o ' || chr(13) || chr(10) ||
		substr(wheb_mensagem_pck.get_texto(311520) || chr(13) || chr(10) ||
		:new.cd_procedimento || ' - ' || substr(obter_desc_prescr_proc(:new.cd_procedimento,:new.ie_origem_proced,:new.nr_seq_proc_interno),1,255) || ' ' || chr(13) || chr(10) ||
		--'Qtde solicitada: ' || :new.qt_solicitada,1,4000),
		wheb_mensagem_pck.get_texto(311521) || :new.qt_solicitada,1,4000),
		sysdate,
		:new.nm_usuario,
		:new.nr_sequencia_autor);

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.procedimento_autorizado_insert
AFTER INSERT ON TASY.PROCEDIMENTO_AUTORIZADO FOR EACH ROW
DECLARE
cd_proc_convenio_w		procedimento_autorizado.cd_procedimento_convenio%type;
cd_proc_tuss_w			procedimento_autorizado.cd_procedimento_tuss%type;
begin

 if	(Nvl(:new.cd_procedimento_tuss,0) = 0) then
	cd_proc_tuss_w := null;
else
	cd_proc_tuss_w := :new.cd_procedimento_tuss;
end if;

if	(Nvl(:new.cd_procedimento_convenio,'0') = '0') then
	cd_proc_convenio_w := null;
else
	cd_proc_convenio_w := :new.cd_procedimento_convenio;
end if;

Gerar_Autor_Integracao(	:new.nr_sequencia,
			:new.cd_procedimento,
			:new.ie_origem_proced,
			cd_proc_tuss_w,
			cd_proc_convenio_w,
			:new.qt_solicitada,
			:new.nr_sequencia_autor,
			:new.nm_usuario,
			'N');

end;
/


CREATE OR REPLACE TRIGGER TASY.PROCEDIMENTO_AUTORIZADO_tp  after update ON TASY.PROCEDIMENTO_AUTORIZADO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_AUTORIZACAO,1,4000),substr(:new.NR_SEQ_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AUTORIZACAO',ie_log_w,ds_w,'PROCEDIMENTO_AUTORIZADO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PROCEDIMENTO_AUTORIZADO ADD (
  CONSTRAINT PROAUTO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCEDIMENTO_AUTORIZADO ADD (
  CONSTRAINT PROAUTO_AGIEXLB_FK 
 FOREIGN KEY (NR_SEQ_AGEINT_EXAME_LAB) 
 REFERENCES TASY.AGEINT_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT PROAUTO_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PROAUTO_AUTCONV_FK 
 FOREIGN KEY (NR_SEQUENCIA_AUTOR) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PROAUTO_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PROAUTO_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCRICAO) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA),
  CONSTRAINT PROAUTO_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT PROAUTO_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT PROAUTO_AUTCNPCT_FK 
 FOREIGN KEY (NR_SEQ_AUTOR_PACOTE) 
 REFERENCES TASY.AUTOR_CONV_PACOTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROCEDIMENTO_AUTORIZADO TO NIVEL_1;

