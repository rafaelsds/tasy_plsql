ALTER TABLE TASY.CONSISTE_REGRA_SAE_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONSISTE_REGRA_SAE_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONSISTE_REGRA_SAE_LIB
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_SAE      NUMBER(10)              NOT NULL,
  CD_PERFIL             NUMBER(5),
  CD_SETOR_ATENDIMENTO  NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CORESLI_PERFIL_FK_I ON TASY.CONSISTE_REGRA_SAE_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORESLI_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CORESLI_PK ON TASY.CONSISTE_REGRA_SAE_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORESLI_PK
  MONITORING USAGE;


CREATE INDEX TASY.CORESLI_RECOISA_FK_I ON TASY.CONSISTE_REGRA_SAE_LIB
(NR_SEQ_REGRA_SAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORESLI_RECOISA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CORESLI_SETATEN_FK_I ON TASY.CONSISTE_REGRA_SAE_LIB
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CORESLI_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.CONSISTE_REGRA_SAE_LIB ADD (
  CONSTRAINT CORESLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONSISTE_REGRA_SAE_LIB ADD (
  CONSTRAINT CORESLI_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT CORESLI_RECOISA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_SAE) 
 REFERENCES TASY.REGRA_CONSISTE_INF_SAE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CORESLI_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CONSISTE_REGRA_SAE_LIB TO NIVEL_1;

