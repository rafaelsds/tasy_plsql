ALTER TABLE TASY.PLS_COMBINACAO_ITEM_CTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COMBINACAO_ITEM_CTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COMBINACAO_ITEM_CTA
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  NM_REGRA                       VARCHAR2(255 BYTE) NOT NULL,
  DT_INICIO_VIGENCIA             DATE           NOT NULL,
  DT_FIM_VIGENCIA                DATE,
  QT_DIAS_CONSIDERAR             NUMBER(5),
  IE_CONTA_IGUAL_COMB            VARCHAR2(1 BYTE),
  DT_LIBERACAO                   DATE,
  IE_OCORRENCIA_CONTA            VARCHAR2(1 BYTE) NOT NULL,
  IE_OU_EXCECAO                  VARCHAR2(1 BYTE),
  DS_OBSERVACAO                  VARCHAR2(4000 BYTE),
  IE_REGRA_QT_CONCORRENTE        VARCHAR2(1 BYTE),
  IE_TIPO_VERIFICACAO            VARCHAR2(1 BYTE) NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4),
  IE_MESMO_PRESTADOR_EXEC_PRINC  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCICT_ESTABEL_FK_I ON TASY.PLS_COMBINACAO_ITEM_CTA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCICT_PK ON TASY.PLS_COMBINACAO_ITEM_CTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCICT_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_combinacao_item_cta_atual
before insert or update or delete ON TASY.PLS_COMBINACAO_ITEM_CTA for each row
declare
nr_seq_combinacao_w	pls_combinacao_item_cta.nr_sequencia%type;
begin

-- se for apagar o registro
if	(deleting) then
	nr_seq_combinacao_w := :old.nr_sequencia;
else
	nr_seq_combinacao_w := :new.nr_sequencia;
end if;

pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_COMBINACAO_ITEM_TM', wheb_usuario_pck.get_nm_usuario,
							'PLS_COMBINACAO_ITEM_CTA_ATUAL',
							'nr_seq_combinacao_p=' || nr_seq_combinacao_w);

end pls_combinacao_item_cta_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_COMBINACAO_ITEM_CTA_tp  after update ON TASY.PLS_COMBINACAO_ITEM_CTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OU_EXCECAO,1,4000),substr(:new.IE_OU_EXCECAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OU_EXCECAO',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_QT_CONCORRENTE,1,4000),substr(:new.IE_REGRA_QT_CONCORRENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_QT_CONCORRENTE',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_VERIFICACAO,1,4000),substr(:new.IE_TIPO_VERIFICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_VERIFICACAO',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTA_IGUAL_COMB,1,4000),substr(:new.IE_CONTA_IGUAL_COMB,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTA_IGUAL_COMB',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_CONSIDERAR,1,4000),substr(:new.QT_DIAS_CONSIDERAR,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_CONSIDERAR',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OCORRENCIA_CONTA,1,4000),substr(:new.IE_OCORRENCIA_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_OCORRENCIA_CONTA',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_REGRA,1,4000),substr(:new.NM_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_REGRA',ie_log_w,ds_w,'PLS_COMBINACAO_ITEM_CTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_COMBINACAO_ITEM_CTA ADD (
  CONSTRAINT PLSCICT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COMBINACAO_ITEM_CTA ADD (
  CONSTRAINT PLSCICT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_COMBINACAO_ITEM_CTA TO NIVEL_1;

