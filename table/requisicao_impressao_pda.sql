ALTER TABLE TASY.REQUISICAO_IMPRESSAO_PDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REQUISICAO_IMPRESSAO_PDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REQUISICAO_IMPRESSAO_PDA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_REQUISICAO        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_IMPRESSORA    NUMBER(10),
  NR_SEQ_RELATORIO     NUMBER(10)               NOT NULL,
  DS_ENDERECO_REDE     VARCHAR2(255 BYTE),
  DS_ENDERECO_IP       VARCHAR2(255 BYTE),
  DS_ERRO              VARCHAR2(4000 BYTE),
  DT_IMPRESSAO         DATE,
  IE_PENDENTE          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REQIMPDA_IMPRESS_FK_I ON TASY.REQUISICAO_IMPRESSAO_PDA
(NR_SEQ_IMPRESSORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REQIMPDA_IMPRESS_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REQIMPDA_PK ON TASY.REQUISICAO_IMPRESSAO_PDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REQIMPDA_PK
  MONITORING USAGE;


CREATE INDEX TASY.REQIMPDA_REQMATE_FK_I ON TASY.REQUISICAO_IMPRESSAO_PDA
(NR_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REQIMPDA_REQMATE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REQUISICAO_IMPRESSAO_PDA ADD (
  CONSTRAINT REQIMPDA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REQUISICAO_IMPRESSAO_PDA ADD (
  CONSTRAINT REQIMPDA_IMPRESS_FK 
 FOREIGN KEY (NR_SEQ_IMPRESSORA) 
 REFERENCES TASY.IMPRESSORA (NR_SEQUENCIA),
  CONSTRAINT REQIMPDA_REQMATE_FK 
 FOREIGN KEY (NR_REQUISICAO) 
 REFERENCES TASY.REQUISICAO_MATERIAL (NR_REQUISICAO));

GRANT SELECT ON TASY.REQUISICAO_IMPRESSAO_PDA TO NIVEL_1;

