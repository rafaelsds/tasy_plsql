DROP TABLE TASY.W_CTB_LIVRO_AUX_INTERCAM CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CTB_LIVRO_AUX_INTERCAM
(
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_PRESTADOR        VARCHAR2(255 BYTE),
  CD_CPF_CNPJ         VARCHAR2(255 BYTE),
  DS_TIPO_PRESTADOR   VARCHAR2(255 BYTE),
  NR_TITULO           NUMBER(10),
  DT_VENCIMENTO       DATE,
  CD_GUIA             VARCHAR2(255 BYTE),
  DT_REALIZACAO       DATE,
  DS_ORIGEM           VARCHAR2(255 BYTE),
  CD_CARTEIRA         VARCHAR2(255 BYTE),
  NM_BENEFICIARIO     VARCHAR2(255 BYTE),
  VL_EVENTO           NUMBER(15,2),
  DT_CONTABILIZACAO   DATE,
  CD_CONTA_CONTABIL   VARCHAR2(20 BYTE),
  IE_DEBITO_CREDITO   VARCHAR2(1 BYTE),
  DS_CLASSIF_DIOPS    VARCHAR2(255 BYTE),
  DS_CONSISTENCIA     VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WCTBLAI_ESTABEL_FK_I ON TASY.W_CTB_LIVRO_AUX_INTERCAM
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WCTBLAI_1 ON TASY.W_CTB_LIVRO_AUX_INTERCAM
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_CTB_LIVRO_AUX_INTERCAM ADD (
  CONSTRAINT WCTBLAI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.W_CTB_LIVRO_AUX_INTERCAM TO NIVEL_1;

