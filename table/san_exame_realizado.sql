ALTER TABLE TASY.SAN_EXAME_REALIZADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_EXAME_REALIZADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_EXAME_REALIZADO
(
  NR_SEQ_EXAME_LOTE          NUMBER(10),
  NR_SEQ_EXAME               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_REALIZADO               DATE               NOT NULL,
  VL_RESULTADO               NUMBER(15,4),
  IE_RESULTADO               VARCHAR2(3 BYTE),
  DS_RESULTADO               VARCHAR2(255 BYTE),
  NR_SEQ_PROPACI             NUMBER(10),
  VL_RESULT1                 NUMBER(15,4),
  VL_RESULT2                 NUMBER(15,4),
  DT_LIBERACAO               DATE,
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  QT_EXECUCAO                NUMBER(10),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DS_LOTE_KIT                VARCHAR2(20 BYTE),
  NR_PLACA                   NUMBER(3),
  NR_AMOSTRA                 NUMBER(5),
  NR_DENSIDADE_OTICA         NUMBER(15,4),
  NR_CUTOFF                  NUMBER(10,4),
  NR_MEDIA_CONTROLE_POS      NUMBER(5),
  NR_MEDIA_CONTROLE_NEG      NUMBER(5),
  CD_PF_REALIZOU             VARCHAR2(20 BYTE),
  QT_VALOR_CN                NUMBER(15,4),
  QT_VALOR_CP                NUMBER(15,4),
  VL_ABS                     NUMBER(15,4),
  DS_ABS                     VARCHAR2(255 BYTE),
  IE_PEND_CONFIRMACAO        VARCHAR2(1 BYTE),
  DT_CONFERENCIA             DATE,
  NM_USUARIO_CONFERENCIA     VARCHAR2(15 BYTE),
  DT_CONFERENCIA_SEG         DATE,
  NM_USUARIO_CONF_SEG        VARCHAR2(15 BYTE),
  DS_RESULTADO_PRIM_CONF     VARCHAR2(255 BYTE),
  DS_RESULTADO_SEG_CONF      VARCHAR2(255 BYTE),
  DT_VENCIMENTO_LOTE         DATE,
  NR_KIT_LOTE                NUMBER(10),
  NR_DENSIDADE_OTICA_DESC    VARCHAR2(20 BYTE),
  NR_CUTOFF_DESC             VARCHAR2(20 BYTE),
  DT_SUSPENSO                DATE,
  NM_USUARIO_SUSPENSO        VARCHAR2(15 BYTE),
  IE_SUSPENSO                VARCHAR2(1 BYTE),
  DS_DENSIDADE_OTICA_FRACAO  VARCHAR2(50 BYTE),
  IE_ANDAMENTO               VARCHAR2(1 BYTE),
  NR_SEQ_EXAME_ORIGEM        NUMBER(10),
  NM_USUARIO_LIB             VARCHAR2(15 BYTE),
  CD_TUBO_DETERMINACAO       VARCHAR2(20 BYTE),
  DS_CT                      VARCHAR2(50 BYTE),
  NR_SEQ_METODO              NUMBER(10),
  DS_FABRICANTE              VARCHAR2(50 BYTE),
  DS_PLACA_PCR               VARCHAR2(8 BYTE),
  DS_PLACA_EXTRACAO          VARCHAR2(22 BYTE),
  NM_USUARIO_CRIACAO         VARCHAR2(15 BYTE),
  CD_EXP_RESULTADO           NUMBER(10),
  DS_COMENTARIO              VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANEXRE_PESFISI_FK_I ON TASY.SAN_EXAME_REALIZADO
(CD_PF_REALIZOU)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SANEXRE_PK ON TASY.SAN_EXAME_REALIZADO
(NR_SEQ_EXAME_LOTE, NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANEXRE_PROPACI_FK_I ON TASY.SAN_EXAME_REALIZADO
(NR_SEQ_PROPACI)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANEXRE_SANEXAM_FK_I ON TASY.SAN_EXAME_REALIZADO
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANEXRE_SANEXAM_FK1_I ON TASY.SAN_EXAME_REALIZADO
(NR_SEQ_EXAME_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANEXRE_SANEXAM_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.SANEXRE_SANEXMU_FK_I ON TASY.SAN_EXAME_REALIZADO
(NR_SEQ_METODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SANEXRE_SANKIEX_FK_I ON TASY.SAN_EXAME_REALIZADO
(NR_KIT_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANEXRE_SANKIEX_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANEXRE_SETATEN_FK_I ON TASY.SAN_EXAME_REALIZADO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANEXRE_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.San_Exame_Realizado_Update
BEFORE UPDATE ON TASY.SAN_EXAME_REALIZADO FOR EACH ROW
declare
qt_minima_w			number(15,4);
qt_maxima_w			number(15,4);
nr_seq_impedimento_w		number(10);
ie_resultado_w			varchar2(2);
nr_seq_doacao_w			number(10);
qt_impedimentos_w		number(3);
ie_fator_rh_w			varchar2(1);
ie_tipo_sangue_w		varchar2(1);
nr_seq_reserva_w		number(10);
nr_seq_transfusao_w		number(10);
cd_pessoa_fisica_w		varchar2(10) := 'X';
ie_origem_w			varchar2(1);
cd_tipo_resultado_w		varchar2(5);

qt_procedimento_w		number(9,3);

ds_log_w			varchar2(2000);
nr_seq_doacao_tmp_w		number(10);
nr_amostra_w			number(10);
nr_seq_res_prod_w		number(10);
ds_resultado_w			varchar2(255);
excluir_todos_proc_w		Varchar(1);
ie_verifica_liberacao_w		varchar2(1);
ie_atualiza_nulo_w		varchar2(1);
ie_tipo_sangue_pf_w		varchar2(255);
ie_subgrupo_abo_w		san_exame.ie_subgrupo_abo%type;
ds_result_sub_abo_w		san_exame_realizado.ds_resultado%type;
nr_seq_subgrupo_w		subtipo_sanguineo.nr_sequencia%type;

BEGIN

obter_param_usuario(450, 257, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, excluir_todos_proc_w);
obter_param_usuario(450, 334, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_verifica_liberacao_w);

if (:old.nr_seq_propaci is not null) and
   (:new.nr_seq_propaci is null) then
	select	nvl(max(qt_procedimento),0)
	into	qt_procedimento_w
	from	procedimento_paciente
	where	nr_sequencia = :OLD.nr_seq_propaci;

	if (qt_procedimento_w > 1) and (excluir_todos_proc_w = 'N') then
		update	procedimento_paciente
		set	qt_procedimento = qt_procedimento - 1
		where	nr_sequencia = :OLD.nr_seq_propaci;
	else
		begin
		delete	procedimento_paciente
		where	nr_sequencia = :OLD.nr_seq_propaci;
		exception
			when others then
				:new.nr_seq_propaci := :new.nr_seq_propaci;
		end;
	end if;
else
	select	nvl(max(nr_seq_doacao),0),
		nvl(max(nr_seq_reserva),0),
		nvl(max(nr_seq_transfusao),0),
		nvl(max(ie_origem),'X'),
		nvl(max(nr_seq_res_prod),0)
	into	nr_seq_doacao_w,
		nr_seq_reserva_w,
		nr_seq_transfusao_w,
		ie_origem_w,
		nr_seq_res_prod_w
	from	san_exame_lote
	where	nr_sequencia = :new.nr_seq_exame_lote;

	if (nr_seq_res_prod_w > 0) then
		select 	nvl(max(nr_seq_reserva),0)
		into	nr_seq_reserva_w
		from	san_reserva_prod
		where	nr_sequencia = nr_seq_res_prod_w;
	end if;

	select 	nvl(max(ie_atualiza_nulo),'N')
	into	ie_atualiza_nulo_w
	from	san_parametro
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	select	qt_minima,
		qt_maxima,
		nr_seq_impedimento,
		ie_resultado,
		ie_tipo_sangue,
		ie_subgrupo_abo,
		ie_fator_rh,
		cd_tipo_resultado
	into	qt_minima_w,
		qt_maxima_w,
		nr_seq_impedimento_w,
		ie_resultado_w,
		ie_tipo_sangue_w,
		ie_subgrupo_abo_w,
		ie_fator_rh_w,
		cd_tipo_resultado_w
	from	san_exame
	where	nr_sequencia = :new.nr_seq_exame;

	if (nr_seq_doacao_w <> 0) then
		if (ie_resultado_w = 'D') then
			:new.vl_resultado := null;
		elsif (ie_resultado_w = 'N') then
			:new.ds_resultado := null;
		end if;
		if  (nr_seq_impedimento_w is not null) then
			if (((:old.vl_resultado > qt_maxima_w) or (:old.vl_resultado < qt_minima_w)) and
		   	   ((:new.vl_resultado <= qt_maxima_w) and (:new.vl_resultado >= qt_minima_w))) or
			   (cd_tipo_resultado_w = 1356 and (:old.ds_resultado = 'Positivo' or :old.ds_resultado = 'Indeterminado') and
			    :new.ds_resultado <> 'Positivo' and :new.ds_resultado <> 'Indeterminado') or
			   (cd_tipo_resultado_w = 1357 and (:old.ds_resultado = 'Reagente' or :old.ds_resultado = 'Indeterminado') and
			    :new.ds_resultado <> 'Reagente' and :new.ds_resultado <> 'Indeterminado') then

				update	san_questionario
				set	ie_impede_doacao = 'N'
				where	nr_seq_impedimento = nr_seq_impedimento_w
				and 	nr_seq_doacao = nr_seq_doacao_w;

			end if;
			if ((:new.vl_resultado > qt_maxima_w) or (:new.vl_resultado < qt_minima_w)) or
			   (cd_tipo_resultado_w = 1356 and (:new.ds_resultado = 'Positivo' or :new.ds_resultado = 'Indeterminado')) or
			   (cd_tipo_resultado_w = 1357 and (:new.ds_resultado = 'Reagente' or :new.ds_resultado = 'Indeterminado')) then
				begin
				insert into san_doacao_impedimento (nr_seq_doacao, nr_seq_impedimento, dt_atualizacao, nm_usuario)
								    values (nr_seq_doacao_w, nr_seq_impedimento_w, sysdate, :new.nm_usuario);
				exception
					when others then
						nr_seq_impedimento_w := null;
				end;
			end if;
		end if;
	end if;

	if	((((nr_seq_transfusao_w <> 0) or ((nr_seq_reserva_w <> 0) and (nr_seq_res_prod_w = 0))) and
		(ie_origem_w in ('R','T','X'))) or (nr_seq_doacao_w <> 0)) and
		((ie_tipo_sangue_w = 'S') or (ie_fator_rh_w = 'S') or (ie_subgrupo_abo_w = 'S')) then
		if	(nr_seq_transfusao_w <> 0) then
			select cd_pessoa_fisica
			into cd_pessoa_fisica_w
			from san_transfusao
			where nr_sequencia = nr_seq_transfusao_w;
		elsif	(nr_seq_reserva_w <> 0) then
			select cd_pessoa_fisica
			into cd_pessoa_fisica_w
			from san_reserva
			where nr_sequencia = nr_seq_reserva_w;
		elsif	(nr_seq_doacao_w <> 0) then
			select cd_pessoa_fisica
			into cd_pessoa_fisica_w
			from san_doacao
			where nr_sequencia = nr_seq_doacao_w;
		end if;


		if	(cd_pessoa_fisica_w <> 'X') then
			begin
				ds_resultado_w	:= :new.ds_resultado;
				--Os273189 clicente utiliza no o domin�o Positivo/Negativo
				if (ie_fator_rh_w = 'S') then
					if (ds_resultado_w = 'Negativo') or (ds_resultado_w = 'NEG')  then
						ds_resultado_w := '-';
					elsif (ds_resultado_w = 'POS') or (ds_resultado_w = 'Positivo') then
						ds_resultado_w := '+';
					elsif (ie_atualiza_nulo_w = 'S') and (ds_resultado_w <> '+') and (ds_resultado_w <> '-') then
						ds_resultado_w := '';
					end if;
				end if;

				ie_tipo_sangue_pf_w := :new.ds_resultado;
				if (ie_tipo_sangue_w = 'S') then
					if (:new.ds_resultado in ('A','B','AB','O')) then
						ie_tipo_sangue_pf_w := :new.ds_resultado;
					elsif	(ie_atualiza_nulo_w = 'S') then
						ie_tipo_sangue_pf_w := '';
					end if;
				end if;

				--Atualiza o subgrupo ABO do cadastro do paciente. Os subgrupos poss�veis est�o listados na consist�ncia
				ds_result_sub_abo_w := :new.ds_resultado;
				if (ie_subgrupo_abo_w = 'S') then
					select	max(nr_sequencia)
					into	nr_seq_subgrupo_w
					from	subtipo_sanguineo
					where	ds_subtipo = substr(ds_result_sub_abo_w, 1, 20);
				end if;

				if (:new.ds_resultado is not null) and
				   (((ie_verifica_liberacao_w = 'S') and (:new.dt_liberacao is not null)) or (ie_verifica_liberacao_w = 'N'))  then
					update	pessoa_fisica
					set	ie_tipo_sangue = decode(ie_tipo_sangue_w, 'S', ie_tipo_sangue_pf_w, ie_tipo_sangue),
						ie_fator_rh = decode(ie_fator_rh_w, 'S', ds_resultado_w, ie_fator_rh),
						ie_subtipo_sanguineo = decode(ie_subgrupo_abo_w, 'S', nr_seq_subgrupo_w, ie_subtipo_sanguineo)
					where 	cd_pessoa_fisica = cd_pessoa_fisica_w;
				end if;
			exception
			when others then
				cd_pessoa_fisica_w := 'X';
			end;

		end if;
	end if;
end if;


if 	(:new.ds_resultado <> :old.ds_resultado) or
	((:old.ds_resultado is null) and (:new.ds_resultado is not null)) then

	select  nr_seq_doacao,
		nr_amostra
	into	nr_seq_doacao_tmp_w,
		nr_amostra_w
	from	san_exame_lote
	where	nr_sequencia = :new.nr_seq_exame_lote;


	ds_log_w := wheb_mensagem_pck.get_texto(793672,
					'NR_SEQ_DOACAO_TMP='||to_char(nr_seq_doacao_tmp_w)||
					';NR_AMOSTRA='||nr_amostra_w||
					';DS_EXAME='||obter_descricao_padrao('SAN_EXAME','DS_EXAME',:new.nr_seq_exame)||
					';DS_RESULTADO='||:new.ds_resultado);
	ds_log_w := substr(ds_log_w||'  |  '||'Stack : '||dbms_utility.format_call_stack,1,4000);

	gravar_log_tasy(701, ds_log_w , :new.nm_usuario);

end if;


END;
/


CREATE OR REPLACE TRIGGER TASY.San_Exame_Realizado_Delete
BEFORE DELETE ON TASY.SAN_EXAME_REALIZADO FOR EACH ROW
declare
qt_procedimento_w	number(9,3);
excluir_todos_proc_w		Varchar(1);

begin

obter_param_usuario(450, 257, obter_perfil_ativo, Wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, excluir_todos_proc_w);

if	(:old.nr_seq_propaci is not null) then
	select nvl(max(qt_procedimento),0)
	into	qt_procedimento_w
	from procedimento_paciente
	where nr_sequencia = :OLD.nr_seq_propaci;

	if	(qt_procedimento_w > 1) and (excluir_todos_proc_w = 'N') then
		update procedimento_paciente
		set qt_procedimento = qt_procedimento - 1
		where nr_sequencia = :OLD.nr_seq_propaci;
	else
		delete	procedimento_paciente
		where	NR_SEQ_PROC_PRINC= :OLD.nr_seq_propaci;

		delete procedimento_paciente
		where nr_sequencia = :OLD.nr_seq_propaci;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.San_Exame_Realizado_insert
BEFORE insert ON TASY.SAN_EXAME_REALIZADO FOR EACH ROW
declare

ie_situacao_w		varchar2(1);
ie_fator_rh_w		varchar2(1);
ie_tipo_sangue_w	varchar2(1);
nr_seq_reserva_w	number(10);
nr_seq_transfusao_w	number(10);
cd_pessoa_fisica_w	varchar2(10) := 'X';
ie_origem_w		varchar2(1);
nr_seq_res_prod_w	number(10);
ds_resultado_w		varchar2(255);
ie_verifica_liberacao_w	varchar2(1);
ie_atualiza_nulo_w	varchar2(1);
nr_sequencia_w		number(10);
ie_tipo_sangue_pf_w	varchar2(2);
ie_subgrupo_abo_w	san_exame.ie_subgrupo_abo%type;
ds_result_sub_abo_w	san_exame_realizado.ds_resultado%type;
nr_seq_subgrupo_w	subtipo_sanguineo.nr_sequencia%type;

BEGIN

Obter_Param_Usuario(450, 334, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_verifica_liberacao_w);

select	nvl(max(ie_situacao),'A')
into	ie_situacao_w
from	san_exame
where	nr_sequencia = :new.nr_seq_exame;

if (ie_situacao_w <> 'A') then
	Wheb_mensagem_pck.exibir_mensagem_abort(245062);
end if;

select	nvl(max(nr_seq_reserva),0),
	nvl(max(nr_seq_transfusao),0),
	nvl(max(ie_origem),'X'),
	nvl(max(NR_SEQ_RES_PROD),0)
into	nr_seq_reserva_w,
	nr_seq_transfusao_w,
	ie_origem_w,
	nr_seq_res_prod_w
from 	san_exame_lote
where 	nr_sequencia = :new.nr_seq_exame_lote;

if (nr_seq_res_prod_w > 0) then
	select 	nvl(max(nr_seq_reserva),0)
	into	nr_seq_reserva_w
	from	san_reserva_prod
	where	nr_sequencia = nr_seq_res_prod_w;
end if;

select 	nvl(max(ie_atualiza_nulo),'N')
into	ie_atualiza_nulo_w
from	san_parametro
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

select	max(ie_tipo_sangue),
	max(ie_fator_rh),
	max(ie_subgrupo_abo)
into	ie_tipo_sangue_w,
	ie_fator_rh_w,
	ie_subgrupo_abo_w
from 	san_exame
where 	nr_sequencia = :new.nr_seq_exame;

if ((nr_seq_transfusao_w <> 0) or (nr_seq_reserva_w <> 0) and (nr_seq_res_prod_w = 0)) and
   (ie_origem_w in ('R','T','X')) and
   ((ie_tipo_sangue_w = 'S') or (ie_fator_rh_w = 'S') or (ie_subgrupo_abo_w = 'S')) then
	if (nr_seq_transfusao_w <> 0) then
		select 	cd_pessoa_fisica
		into 	cd_pessoa_fisica_w
		from 	san_transfusao
		where 	nr_sequencia = nr_seq_transfusao_w;
	elsif (nr_seq_reserva_w <> 0) then
		select 	cd_pessoa_fisica
		into 	cd_pessoa_fisica_w
		from 	san_reserva
		where 	nr_sequencia = nr_seq_reserva_w;
	end if;

	if (cd_pessoa_fisica_w <> 'X') then
		begin
			ds_resultado_w	:= :new.ds_resultado;
			--Os273189 clicente utiliza no o domin�o Positivo/Negativo
			if (ie_fator_rh_w = 'S') then
				if (ds_resultado_w = 'Negativo') or (ds_resultado_w = 'NEG') then
					ds_resultado_w := '-';
				elsif (ds_resultado_w = 'POS') or (ds_resultado_w = 'Positivo') then
					ds_resultado_w := '+';
				elsif (ie_atualiza_nulo_w = 'S') and (ds_resultado_w <> '+') and (ds_resultado_w <> '-') then
					ds_resultado_w := '';
				end if;
			end if;

			ie_tipo_sangue_pf_w := :new.ds_resultado;
			if (ie_tipo_sangue_w = 'S') then
				if (:new.ds_resultado in ('A','B','AB','O')) then
					ie_tipo_sangue_pf_w := :new.ds_resultado;
				elsif (ie_atualiza_nulo_w = 'S') then
					ie_tipo_sangue_pf_w := '';
				end if;
			end if;

			--Atualiza o subgrupo ABO do cadastro do paciente. Os subgrupos poss�veis est�o listados na consist�ncia
			ds_result_sub_abo_w := :new.ds_resultado;
			if	(ie_subgrupo_abo_w = 'S') then
				select	max(nr_sequencia)
				into	nr_seq_subgrupo_w
				from	subtipo_sanguineo
				where	ds_subtipo = substr(ds_result_sub_abo_w, 1, 20);
			end if;

			if	(:new.ds_resultado is not null) and
				(((ie_verifica_liberacao_w = 'S') and (:new.dt_liberacao is not null)) or (ie_verifica_liberacao_w = 'N')) then
			update 	pessoa_fisica
			set 	ie_tipo_sangue = decode(ie_tipo_sangue_w, 'S', ie_tipo_sangue_pf_w, ie_tipo_sangue),
				ie_fator_rh = decode(ie_fator_rh_w, 'S', ds_resultado_w, ie_fator_rh),
				ie_subtipo_sanguineo = decode(ie_subgrupo_abo_w, 'S', nr_seq_subgrupo_w, ie_subtipo_sanguineo)
			where 	cd_pessoa_fisica = cd_pessoa_fisica_w;
			end if;
		exception
		when others then
			cd_pessoa_fisica_w := 'X';
		end;

	end if;
end if;

END;
/


ALTER TABLE TASY.SAN_EXAME_REALIZADO ADD (
  CONSTRAINT SANEXRE_PK
 PRIMARY KEY
 (NR_SEQ_EXAME_LOTE, NR_SEQ_EXAME)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_EXAME_REALIZADO ADD (
  CONSTRAINT SANEXRE_SANEXMU_FK 
 FOREIGN KEY (NR_SEQ_METODO) 
 REFERENCES TASY.SAN_EXAME_METODO_UTIL (NR_SEQUENCIA),
  CONSTRAINT SANEXRE_SANKIEX_FK 
 FOREIGN KEY (NR_KIT_LOTE) 
 REFERENCES TASY.SAN_KIT_EXAME (NR_SEQUENCIA),
  CONSTRAINT SANEXRE_SANEXAM_FK1 
 FOREIGN KEY (NR_SEQ_EXAME_ORIGEM) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA),
  CONSTRAINT SANEXRE_PESFISI_FK 
 FOREIGN KEY (CD_PF_REALIZOU) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANEXRE_SANEXLO_FK 
 FOREIGN KEY (NR_SEQ_EXAME_LOTE) 
 REFERENCES TASY.SAN_EXAME_LOTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT SANEXRE_SANEXAM_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.SAN_EXAME (NR_SEQUENCIA),
  CONSTRAINT SANEXRE_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROPACI) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT SANEXRE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.SAN_EXAME_REALIZADO TO NIVEL_1;

