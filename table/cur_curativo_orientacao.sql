ALTER TABLE TASY.CUR_CURATIVO_ORIENTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CUR_CURATIVO_ORIENTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CUR_CURATIVO_ORIENTACAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_ORIENTACAO          DATE                   NOT NULL,
  NR_SEQ_CURATIVO        NUMBER(10)             NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  DS_ORIENTACAO          LONG                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  DS_UTC                 VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO       VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO     VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CURCUOR_CURCUES_FK_I ON TASY.CUR_CURATIVO_ORIENTACAO
(NR_SEQ_CURATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CURCUOR_CURCUES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CURCUOR_PESFISI_FK_I ON TASY.CUR_CURATIVO_ORIENTACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CURCUOR_PK ON TASY.CUR_CURATIVO_ORIENTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CURCUOR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CUR_CURATIVO_ORIENTACAO_ATUAL
before insert or update ON TASY.CUR_CURATIVO_ORIENTACAO for each row
declare


begin
if	(nvl(:old.DT_ORIENTACAO,sysdate+10) <> :new.DT_ORIENTACAO) and
	(:new.DT_ORIENTACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ORIENTACAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


ALTER TABLE TASY.CUR_CURATIVO_ORIENTACAO ADD (
  CONSTRAINT CURCUOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CUR_CURATIVO_ORIENTACAO ADD (
  CONSTRAINT CURCUOR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CURCUOR_CURCUES_FK 
 FOREIGN KEY (NR_SEQ_CURATIVO) 
 REFERENCES TASY.CUR_CURATIVO_ESP (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CUR_CURATIVO_ORIENTACAO TO NIVEL_1;

