ALTER TABLE TASY.CPOE_REVISAO_HORARIOS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_REVISAO_HORARIOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_REVISAO_HORARIOS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MAT_CPOE      NUMBER(10)               NOT NULL,
  NR_PRESCRICAO        NUMBER(15)               NOT NULL,
  NR_SEQ_PRESCR_MAT    NUMBER(10)               NOT NULL,
  NR_SEQ_MAT_HOR       NUMBER(15)               NOT NULL,
  DT_HORARIO           DATE                     NOT NULL,
  IE_PULOU_DIA         VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPOREVH_CPOEMAT_FK_I ON TASY.CPOE_REVISAO_HORARIOS
(NR_SEQ_MAT_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPOREVH_PK ON TASY.CPOE_REVISAO_HORARIOS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOREVH_PREMAHO_FK_I ON TASY.CPOE_REVISAO_HORARIOS
(NR_SEQ_MAT_HOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOREVH_PRESMAT_FK_I ON TASY.CPOE_REVISAO_HORARIOS
(NR_PRESCRICAO, NR_SEQ_PRESCR_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOREVH_PRESMED_FK_I ON TASY.CPOE_REVISAO_HORARIOS
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_REVISAO_HORARIOS ADD (
  CONSTRAINT CPOREVH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CPOE_REVISAO_HORARIOS ADD (
  CONSTRAINT CPOREVH_CPOEMAT_FK 
 FOREIGN KEY (NR_SEQ_MAT_CPOE) 
 REFERENCES TASY.CPOE_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT CPOREVH_PREMAHO_FK 
 FOREIGN KEY (NR_SEQ_MAT_HOR) 
 REFERENCES TASY.PRESCR_MAT_HOR (NR_SEQUENCIA),
  CONSTRAINT CPOREVH_PRESMAT_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCR_MAT) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA),
  CONSTRAINT CPOREVH_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO));

GRANT SELECT ON TASY.CPOE_REVISAO_HORARIOS TO NIVEL_1;

