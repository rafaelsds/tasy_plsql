ALTER TABLE TASY.PFCS_DIAGNOSIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PFCS_DIAGNOSIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PFCS_DIAGNOSIS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_DOENCA            VARCHAR2(10 BYTE),
  IE_POSITIVO          VARCHAR2(1 BYTE),
  IE_SUSPEITA          VARCHAR2(1 BYTE),
  IE_COMORBIDADE       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFCS_D_CIDDOEN_FK_I ON TASY.PFCS_DIAGNOSIS
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFCS_D_PK ON TASY.PFCS_DIAGNOSIS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PFCS_DIAGNOSIS ADD (
  CONSTRAINT PFCS_D_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PFCS_DIAGNOSIS ADD (
  CONSTRAINT PFCS_D_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.PFCS_DIAGNOSIS TO NIVEL_1;

