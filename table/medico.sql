ALTER TABLE TASY.MEDICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDICO
(
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NR_CRM                 VARCHAR2(20 BYTE)      NOT NULL,
  NM_GUERRA              VARCHAR2(60 BYTE)      NOT NULL,
  IE_VINCULO_MEDICO      NUMBER(2)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_CGC                 VARCHAR2(14 BYTE),
  IE_COBRA_PF_PJ         VARCHAR2(1 BYTE),
  IE_CONVENIADO_SUS      VARCHAR2(1 BYTE),
  IE_AUDITOR_SUS         VARCHAR2(1 BYTE),
  UF_CRM                 VARCHAR2(15 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  IE_CORPO_CLINICO       VARCHAR2(1 BYTE)       NOT NULL,
  DT_ADMISSAO            DATE,
  DT_DESLIGAMENTO        DATE,
  IE_CORPO_ASSIST        VARCHAR2(1 BYTE)       NOT NULL,
  DT_EFETIVACAO          DATE,
  NR_SEQ_CATEGORIA       NUMBER(10),
  IE_TIPO_COMPL_CORRESP  NUMBER(2),
  IE_AMA                 VARCHAR2(1 BYTE),
  IE_ORIGEM_INF          VARCHAR2(2 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_RQE                 VARCHAR2(20 BYTE),
  DS_SENHA               VARCHAR2(40 BYTE),
  IE_TSA                 VARCHAR2(1 BYTE),
  DS_ORIENTACAO_MEDICO   VARCHAR2(4000 BYTE),
  IE_RETAGUARDA          VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO   NUMBER(5),
  DS_ENTIDADE_JURIDICA   VARCHAR2(255 BYTE),
  IE_COORDENADOR         VARCHAR2(1 BYTE),
  CD_ESTAB_ORIG          NUMBER(4),
  IE_EMAIL_LAUDO         VARCHAR2(1 BYTE),
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  NR_CRM_NUMERICO        NUMBER(38),
  IE_RECEB_PGTO_PROD     VARCHAR2(1 BYTE),
  IE_INTEGRADO           VARCHAR2(1 BYTE),
  IE_MEDICO_SENIOR       VARCHAR2(1 BYTE),
  NR_PRESCRITOR          NUMBER(10),
  IE_DERIVADO            VARCHAR2(1 BYTE),
  IE_AUDITOR_INTERNO     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.MEDICO.CD_PESSOA_FISICA IS 'Codigo da Pessoa fisica';

COMMENT ON COLUMN TASY.MEDICO.NR_CRM IS 'Identifica�ao Conselho Regional Medicina';

COMMENT ON COLUMN TASY.MEDICO.NM_GUERRA IS 'Nome de Guerra';

COMMENT ON COLUMN TASY.MEDICO.IE_VINCULO_MEDICO IS 'Indicador do Vinculo Medico';


CREATE INDEX TASY.MEDICO_ESTABEL_FK_I ON TASY.MEDICO
(CD_ESTAB_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDICO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDICO_I1 ON TASY.MEDICO
(UF_CRM, NR_CRM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDICO_I2 ON TASY.MEDICO
(IE_SITUACAO, IE_CORPO_CLINICO, IE_CORPO_ASSIST, CD_PESSOA_FISICA, UPPER("NR_CRM"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDICO_MEDCATE_FK_I ON TASY.MEDICO
(NR_SEQ_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDICO_PESJURI_FK_I ON TASY.MEDICO
(CD_CGC)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDICO_PK ON TASY.MEDICO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDICO_SETATEN_FK_I ON TASY.MEDICO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDICO_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MEDICO_tp  after update ON TASY.MEDICO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_PESSOA_FISICA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.UF_CRM,1,4000),substr(:new.UF_CRM,1,4000),:new.nm_usuario,nr_seq_w,'UF_CRM',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COORDENADOR,1,4000),substr(:new.IE_COORDENADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_COORDENADOR',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_GUERRA,1,4000),substr(:new.NM_GUERRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_GUERRA',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONVENIADO_SUS,1,4000),substr(:new.IE_CONVENIADO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONVENIADO_SUS',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VINCULO_MEDICO,1,4000),substr(:new.IE_VINCULO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VINCULO_MEDICO',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_DESLIGAMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_DESLIGAMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_DESLIGAMENTO',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CORPO_CLINICO,1,4000),substr(:new.IE_CORPO_CLINICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CORPO_CLINICO',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ADMISSAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ADMISSAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ADMISSAO',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUDITOR_SUS,1,4000),substr(:new.IE_AUDITOR_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUDITOR_SUS',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COBRA_PF_PJ,1,4000),substr(:new.IE_COBRA_PF_PJ,1,4000),:new.nm_usuario,nr_seq_w,'IE_COBRA_PF_PJ',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CORPO_ASSIST,1,4000),substr(:new.IE_CORPO_ASSIST,1,4000),:new.nm_usuario,nr_seq_w,'IE_CORPO_ASSIST',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EFETIVACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EFETIVACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EFETIVACAO',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CATEGORIA,1,4000),substr(:new.NR_SEQ_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CATEGORIA',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_COMPL_CORRESP,1,4000),substr(:new.IE_TIPO_COMPL_CORRESP,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPL_CORRESP',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AMA,1,4000),substr(:new.IE_AMA,1,4000),:new.nm_usuario,nr_seq_w,'IE_AMA',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_INF,1,4000),substr(:new.IE_ORIGEM_INF,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_INF',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RQE,1,4000),substr(:new.NR_RQE,1,4000),:new.nm_usuario,nr_seq_w,'NR_RQE',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TSA,1,4000),substr(:new.IE_TSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TSA',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA,1,4000),substr(:new.DS_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO_MEDICO,1,4000),substr(:new.DS_ORIENTACAO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO_MEDICO',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RETAGUARDA,1,4000),substr(:new.IE_RETAGUARDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_RETAGUARDA',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENTIDADE_JURIDICA,1,4000),substr(:new.DS_ENTIDADE_JURIDICA,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENTIDADE_JURIDICA',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CRM,1,4000),substr(:new.NR_CRM,1,4000),:new.nm_usuario,nr_seq_w,'NR_CRM',ie_log_w,ds_w,'MEDICO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.medico_befupdate_log
before update ON TASY.MEDICO for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	insert into medico_log(
		nr_sequencia,
		cd_pessoa_fisica,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		nr_crm,
		nm_guerra,
		ie_vinculo_medico,
		cd_cgc,
		ie_cobra_pf_pj,
		ie_conveniado_sus,
		ie_auditor_sus,
		ie_origem_inf,
		uf_crm,
		ie_situacao,
		ie_corpo_clinico,
		dt_admissao,
		dt_desligamento,
		ie_corpo_assist,
		dt_efetivacao,
		nr_seq_categoria,
		ie_tipo_compl_corresp,
		nr_rqe,
		ie_ama,
		ds_senha,
		ie_tsa,
		ds_orientacao_medico,
		ie_retaguarda)
	values( medico_log_seq.NextVal,
		:new.cd_pessoa_fisica,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:old.nr_crm,
		:old.nm_guerra,
		:old.ie_vinculo_medico,
		:old.cd_cgc,
		:old.ie_cobra_pf_pj,
		:old.ie_conveniado_sus,
		:old.ie_auditor_sus,
		:old.ie_origem_inf,
		:old.uf_crm,
		:old.ie_situacao,
		:old.ie_corpo_clinico,
		:old.dt_admissao,
		:old.dt_desligamento,
		:old.ie_corpo_assist,
		:old.dt_efetivacao,
		:old.nr_seq_categoria,
		:old.ie_tipo_compl_corresp,
		:old.nr_rqe,
		:old.ie_ama,
		:old.ds_senha,
		:old.ie_tsa,
		:old.ds_orientacao_medico,
		:old.ie_retaguarda);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Medico_AfterInsert
AFTER INSERT ON TASY.MEDICO FOR EACH ROW
DECLARE

ie_inserir_medico_estab_w	varchar2(1) := 'N';
qt_existe_w			number(10,0);
reg_integracao_p	gerar_int_padrao.reg_integracao;
ds_param_integ_hl7_w			varchar2(4000);
ds_sep_bv_w				varchar2(100);
BEGIN
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	ds_sep_bv_w := obter_separador_bv;
	Obter_Param_Usuario(4,90,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_inserir_medico_estab_w);

	if	(ie_inserir_medico_estab_w = 'S') then

		select count(*)
		into	qt_existe_w
		from 	medico_estabelecimento
		where 	cd_pessoa_fisica = :new.cd_pessoa_fisica
		and 	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

		if	(qt_existe_w = 0) then

			insert 	into	medico_estabelecimento (
					nr_sequencia,
					cd_pessoa_fisica,
					cd_estabelecimento,
					ie_socio,
					dt_atualizacao,
					nm_usuario)
			values	(
					medico_estabelecimento_seq.nextval,
					:new.cd_pessoa_fisica,
					wheb_usuario_pck.get_cd_estabelecimento,
					'N',
					sysdate,
					:new.nm_usuario);

		end if;
	end if;
	send_physician_intpd(:new.cd_pessoa_fisica, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, 'I');
	ds_param_integ_hl7_w := 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w||
							'ie_acao='||'MAD'||ds_sep_bv_w;
	gravar_agend_integracao(853, ds_param_integ_hl7_w); /* Tasy -> SCC (MFN_M02) - Doctor registration */
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.medico_after_update
after update ON TASY.MEDICO for each row
declare

reg_integracao_p	gerar_int_padrao.reg_integracao;
ds_param_integ_hl7_w			varchar2(4000);
ds_sep_bv_w				varchar2(100);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	ds_sep_bv_w := obter_separador_bv;
	send_physician_intpd(:new.cd_pessoa_fisica, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, 'U');
	ds_param_integ_hl7_w := 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w||
							'ie_acao='||'MUP'||ds_sep_bv_w;
	gravar_agend_integracao(853, ds_param_integ_hl7_w); /* Tasy -> SCC (MFN_M02) - Doctor registration */
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.medico_delete
before delete ON TASY.MEDICO for each row
declare
ds_param_integ_hl7_w			varchar2(4000);
ds_sep_bv_w				varchar2(100);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	ds_sep_bv_w := obter_separador_bv;
	ds_param_integ_hl7_w := 'cd_pessoa_fisica=' || :old.cd_pessoa_fisica || ds_sep_bv_w||
								'ie_acao='||'MDL'||ds_sep_bv_w;
	gravar_agend_integracao(853, ds_param_integ_hl7_w); /* Tasy -> SCC (MFN_M02) - Doctor registration */
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Medico_Insert
BEFORE INSERT ON TASY.MEDICO FOR EACH ROW
declare

a	number(1);

begin

a:= 1;

/* Comentada porque estava dando erro de Mutating no acerto de duplicidade */
/* Oraci e Edilson em 17/10/2007 OS71536 */
/* update	pessoa_fisica
set	ie_tipo_pessoa		= 1
where	cd_pessoa_fisica	= :new.cd_pessoa_fisica; */

end;
/


CREATE OR REPLACE TRIGGER TASY.Medico_update
BEFORE INSERT OR UPDATE ON TASY.MEDICO FOR EACH ROW
declare

nr_seq_conselho_w		number(10,0);
ie_atualiza_vinculo_w		varchar2(1);
nm_pessoa_fisica_w		pessoa_fisica.nm_pessoa_fisica%type;
qt_registro_w			number(10);

begin

ie_atualiza_vinculo_w	:= nvl(obter_valor_param_usuario(4, 95, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento),'S');


if	(:new.NR_CRM is not null) then
	begin

	/*
	Comentado este bloco e o bloco posterior ao UPDATE, porque o campo nr_seq_conselho, na fun��o Cadastro M�dico,  ao utilizar o Shift + F8, diz que pode ser CRO e CRF, al�m do CRM.
	Ent�o, n�o pode ser setado somente para o CRM.
	Ricardo e Fabr�cio cientes.
	Heckmann	OS 263504

	select	nvl(max(nr_sequencia),0)
	into	nr_seq_conselho_w
	from 	conselho_profissional
	where 	upper(sg_conselho) = 'CRM';
	*/

	update	pessoa_fisica
	set	DS_CODIGO_PROF		= substr(:new.NR_CRM,1,15)
	where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;

	/*
	if	(nr_seq_conselho_w <> 0) then
		update	pessoa_fisica
		set	nr_seq_conselho		= nr_seq_conselho_w
		where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	end if;
	*/


	end;
end if;

if	(ie_atualiza_vinculo_w <> 'X') then
	if	(nvl(:new.IE_VINCULO_MEDICO,0) <> nvl(:old.IE_VINCULO_MEDICO,0)) and
		(:new.IE_VINCULO_MEDICO is not null) and
		((ie_atualiza_vinculo_w = 'S') or (not inserting))  then

		insert into medico_vinculo 	(NR_SEQUENCIA,
						DT_ATUALIZACAO,
						NM_USUARIO,
						DT_ATUALIZACAO_NREC,
						NM_USUARIO_NREC,
						CD_MEDICO,
						IE_VINCULO_MEDICO,
						DT_ADMISSAO)
		values				(medico_vinculo_seq.nextval,
						sysdate,
						:new.nm_usuario,
						sysdate,
						:new.nm_usuario,
						:new.cd_pessoa_fisica,
						:new.IE_VINCULO_MEDICO,
						sysdate);

	end if;
end if;


if	(:new.ie_situacao = 'I') then
	update	agenda_medico
	set	ie_situacao 	= 'I'
	where	cd_medico	= :new.cd_pessoa_fisica;
end if;

if	(nvl(:new.nm_guerra,'X') <> nvl(:old.nm_guerra,'X')) then
	select 	count(1)
	into	qt_registro_w
	from 	pls_web_param_guia_medico;

	if (qt_registro_w > 0) then
		select	SUBSTR(OBTER_NOME_PF(cd_pessoa_fisica), 0, 255)
		into	nm_pessoa_fisica_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

		pls_atualiza_nome_busca_guia(	:new.cd_pessoa_fisica,
						null,
						null,
						null,
						nm_pessoa_fisica_w,
						:new.nm_guerra);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.medico_update_hl7
after update ON TASY.MEDICO for each row
declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);

begin

ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
gravar_agend_integracao(303, ds_param_integ_hl7_w);
gravar_agend_integracao(547, ds_param_integ_hl7_w);

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_medico_residente_upd
BEFORE update or delete
ON TASY.MEDICO FOR EACH ROW
--disable
declare

BEGIN
    
    /*
        Ir� impossibilitar altera��o no cadastro m�dico para perfis diferentes do "1530 HSJ - Cadastro de Residente" ou "272 HSJ - Adm Sistemas"
        Caso o m�dico ATIVO possua o vinculo RESIDENTE, n�o tenha data desligamento e n�o seja do CORPO CLINICO
        
        OBS: Toda altera��o de residente dever� ser encaminhada para o Centro de Ensino (Seja altera��o originada pelo Plano ou da Dire��o)
    */
    
    if(updating)then
    
        if(:new.ie_vinculo_medico = 3 and :new.dt_desligamento is null and nvl(:new.ie_situacao,'A') = 'A' and nvl(:new.ie_corpo_clinico,'N') = 'N' and obter_perfil_ativo not in(272, 1530))then
            hsj_gerar_log('Altera��o de Residente n�o permitida!'||hsj_enter||'Altera��es do cadastro de m�dico residente devem ser encaminhadas para o centro de ensino!');
        end if;
    
    elsif(deleting)then
    
       if(:old.ie_vinculo_medico = 3 and :old.dt_desligamento is null and nvl(:old.ie_situacao,'A') = 'A' and nvl(:old.ie_corpo_clinico,'N') = 'N' and obter_perfil_ativo not in(272, 1530))then
            hsj_gerar_log('Exclus�o de Residente n�o permitida!'||hsj_enter||'Exclus�es do cadastro de m�dico residente devem ser encaminhadas para o centro de ensino!');
       end if;
       
    end if;
    
END hsj_medico_residente_upd;
/


CREATE OR REPLACE TRIGGER TASY.medico_insert_hl7
after insert ON TASY.MEDICO for each row
declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);

begin

ds_sep_bv_w := obter_separador_bv;
ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w;
gravar_agend_integracao(302, ds_param_integ_hl7_w);
gravar_agend_integracao(427, ds_param_integ_hl7_w);
gravar_agend_integracao(544, ds_param_integ_hl7_w); -- GE

end;
/


ALTER TABLE TASY.MEDICO ADD (
  CONSTRAINT MEDICO_PK
 PRIMARY KEY
 (CD_PESSOA_FISICA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDICO ADD (
  CONSTRAINT MEDICO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_ORIG) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MEDICO_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MEDICO_MEDCATE_FK 
 FOREIGN KEY (NR_SEQ_CATEGORIA) 
 REFERENCES TASY.MEDICO_CATEGORIA (NR_SEQUENCIA),
  CONSTRAINT MEDICO_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT MEDICO_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.MEDICO TO NIVEL_1;

GRANT SELECT ON TASY.MEDICO TO ROBOCMTECNOLOGIA;

