ALTER TABLE TASY.CTB_DEMO_MES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_DEMO_MES CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_DEMO_MES
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DEMO          NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_MES_REF       NUMBER(10),
  NR_SEQ_COLUNA        NUMBER(2)                NOT NULL,
  IE_VALOR             VARCHAR2(4 BYTE)         NOT NULL,
  NR_SEQ_COL_1         NUMBER(3),
  NR_SEQ_COL_2         NUMBER(3),
  DS_COLUNA            VARCHAR2(40 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_INFORMACAO        VARCHAR2(2 BYTE)         NOT NULL,
  NR_SEQ_COLUNAS       VARCHAR2(255 BYTE),
  NR_DIVISOR_MES       NUMBER(10),
  DS_MASCARA           VARCHAR2(20 BYTE),
  NR_SEQ_MES_INICIAL   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBDEME_CTBDEMO_FK_I ON TASY.CTB_DEMO_MES
(NR_SEQ_DEMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBDEME_CTBMERE_FK_I ON TASY.CTB_DEMO_MES
(NR_SEQ_MES_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBDEME_CTBMERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBDEME_CTBMERE_FK2_I ON TASY.CTB_DEMO_MES
(NR_SEQ_MES_INICIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBDEME_PK ON TASY.CTB_DEMO_MES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBDEME_UK ON TASY.CTB_DEMO_MES
(NR_SEQ_DEMO, NR_SEQ_COLUNA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ctb_demo_mes_insert
before insert ON TASY.CTB_DEMO_MES for each row
declare
dt_referencia_w		date;
dt_mes_inicial_w	date;

BEGIN

if	(:new.ie_valor in ('V','P')) and
	((:new.nr_seq_col_1 is null) or (:new.nr_seq_col_2 is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(266557);
end if;

if	(:new.nr_seq_mes_ref is not null) and
	(:new.nr_seq_mes_inicial is not null) then
	begin
	dt_referencia_w		:= ctb_obter_mes_ref(:new.nr_seq_mes_ref);
	dt_mes_inicial_w	:= ctb_obter_mes_ref(:new.nr_seq_mes_inicial);
	if	(dt_mes_inicial_w > dt_referencia_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(329476);
	end if;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ctb_demo_mes_update
before update ON TASY.CTB_DEMO_MES for each row
declare
dt_referencia_w		date;
dt_mes_inicial_w	date;

begin

if	(:new.ie_valor in ('V','P')) and
	((:new.nr_seq_col_1 is null) or (:new.nr_seq_col_2 is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(266557);
end if;

if	(:new.nr_seq_mes_ref is not null) and
	(:new.nr_seq_mes_inicial is not null) then
	begin
	dt_referencia_w		:= ctb_obter_mes_ref(:new.nr_seq_mes_ref);
	dt_mes_inicial_w	:= ctb_obter_mes_ref(:new.nr_seq_mes_inicial);
	if	(dt_mes_inicial_w > dt_referencia_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(329476);
	end if;
	end;
end if;

END;
/


ALTER TABLE TASY.CTB_DEMO_MES ADD (
  CONSTRAINT CTBDEME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT CTBDEME_UK
 UNIQUE (NR_SEQ_DEMO, NR_SEQ_COLUNA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_DEMO_MES ADD (
  CONSTRAINT CTBDEME_CTBMERE_FK2 
 FOREIGN KEY (NR_SEQ_MES_INICIAL) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA),
  CONSTRAINT CTBDEME_CTBDEMO_FK 
 FOREIGN KEY (NR_SEQ_DEMO) 
 REFERENCES TASY.CTB_DEMONSTRATIVO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CTBDEME_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA));

GRANT SELECT ON TASY.CTB_DEMO_MES TO NIVEL_1;

