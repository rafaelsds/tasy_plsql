ALTER TABLE TASY.CP_CONV_PROBLEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CP_CONV_PROBLEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.CP_CONV_PROBLEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROBLEM       NUMBER(10)               NOT NULL,
  NR_SEQ_CAD_PROBLEM   NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPCOPRO_CADPROB_FK_I ON TASY.CP_CONV_PROBLEM
(NR_SEQ_CAD_PROBLEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPCOPRO_CPPROBL_FK_I ON TASY.CP_CONV_PROBLEM
(NR_SEQ_PROBLEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPCOPRO_PK ON TASY.CP_CONV_PROBLEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CP_CONV_PROBLEM ADD (
  CONSTRAINT CPCOPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CP_CONV_PROBLEM ADD (
  CONSTRAINT CPCOPRO_CADPROB_FK 
 FOREIGN KEY (NR_SEQ_CAD_PROBLEM) 
 REFERENCES TASY.CADASTRO_PROBLEMA (NR_SEQUENCIA),
  CONSTRAINT CPCOPRO_CPPROBL_FK 
 FOREIGN KEY (NR_SEQ_PROBLEM) 
 REFERENCES TASY.CP_PROBLEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CP_CONV_PROBLEM TO NIVEL_1;

