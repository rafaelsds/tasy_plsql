ALTER TABLE TASY.PROTOCOLO_CONVENIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_CONVENIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_CONVENIO
(
  CD_CONVENIO               NUMBER(5)           NOT NULL,
  NR_PROTOCOLO              VARCHAR2(40 BYTE)   NOT NULL,
  IE_STATUS_PROTOCOLO       NUMBER(1)           NOT NULL,
  DT_PERIODO_INICIAL        DATE                NOT NULL,
  DT_PERIODO_FINAL          DATE                NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  IE_TIPO_PROTOCOLO         NUMBER(2),
  NR_SEQ_PROTOCOLO          NUMBER(10),
  DT_ENVIO                  DATE,
  NM_USUARIO_ENVIO          VARCHAR2(15 BYTE),
  DS_ARQUIVO_ENVIO          VARCHAR2(255 BYTE),
  DT_RETORNO                DATE,
  NM_USUARIO_RETORNO        VARCHAR2(15 BYTE),
  DS_ARQUIVO_RETORNO        VARCHAR2(255 BYTE),
  CD_SETOR_ATENDIMENTO      NUMBER(5),
  CD_ESPECIALIDADE          NUMBER(8),
  IE_PERIODO_FINAL          VARCHAR2(1 BYTE),
  DS_PARAMETRO_ATEND        VARCHAR2(10 BYTE),
  DT_GERACAO                DATE,
  NM_USUARIO_GERACAO        VARCHAR2(15 BYTE),
  DT_MESANO_REFERENCIA      DATE                NOT NULL,
  CD_CLASSIF_SETOR          VARCHAR2(2 BYTE),
  IE_TIPO_ATEND_BPA         NUMBER(3),
  DT_CONSISTENCIA           DATE,
  NR_SEQ_ENVIO_CONVENIO     NUMBER(10),
  DT_MESANO_REF_PAR         DATE,
  DS_INCONSISTENCIA         VARCHAR2(80 BYTE),
  DT_VENCIMENTO             DATE,
  DT_INTEGRACAO_CR          DATE,
  DT_ENTREGA_CONVENIO       DATE,
  CD_INTERFACE_ENVIO        NUMBER(5),
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  NR_SEQ_LOTE_RECEITA       NUMBER(10)          NOT NULL,
  NR_SEQ_LOTE_REPASSE       NUMBER(10)          NOT NULL,
  NR_SEQ_LOTE_GRAT          NUMBER(10)          NOT NULL,
  NR_SEQ_DOC_CONVENIO       VARCHAR2(20 BYTE),
  DT_DEFINITIVO             DATE,
  DT_CONTROLE_PROC_SUS      DATE,
  IE_TIPO_BPA               VARCHAR2(1 BYTE),
  NR_SEQ_PQ_PROTOCOLO       NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(800 BYTE),
  CD_PLANO                  VARCHAR2(10 BYTE),
  IE_CREDENCIAMENTO_SUS     VARCHAR2(1 BYTE),
  CD_PROCEDENCIA            NUMBER(5),
  CD_PRESTADOR_CONVENIO     VARCHAR2(255 BYTE),
  NR_SEQ_ETAPA              NUMBER(10),
  NR_PROTOCOLO_SPSADT       VARCHAR2(20 BYTE),
  NR_PROTOCOLO_RI           VARCHAR2(20 BYTE),
  NR_PROTOCOLO_HONOR        VARCHAR2(20 BYTE),
  NR_SEQ_INT_ENVIO          NUMBER(10),
  NR_SEQ_INT_ENVIO_INICIAL  NUMBER(10),
  CD_ANS                    VARCHAR2(100 BYTE),
  NR_PROTOCOLO_CONS         VARCHAR2(20 BYTE),
  CD_CATEGORIA              VARCHAR2(10 BYTE),
  CD_PROCEDIMENTO           NUMBER(15),
  IE_ORIGEM_PROCED          NUMBER(10),
  CD_PRESTADOR_CONVENIO_RI  VARCHAR2(255 BYTE),
  NR_SEQ_LOTE_PROTOCOLO     NUMBER(10),
  IE_COMPLEXIDADE_SUS       VARCHAR2(15 BYTE),
  IE_TIPO_FINANC_SUS        VARCHAR2(15 BYTE),
  IE_RECEB_FINANCEIRO       VARCHAR2(1 BYTE),
  VL_RECEBIMENTO            NUMBER(15,2),
  IE_REAPRESENTACAO         VARCHAR2(15 BYTE),
  CD_SETOR_PACIENTE         NUMBER(5),
  NR_SEQ_CLASSIFICACAO      NUMBER(10),
  NR_DEMONST_PAGTO          VARCHAR2(255 BYTE),
  NM_USUARIO_ALT_DOC_CONV   VARCHAR2(15 BYTE),
  DT_ALTER_DOC_CONVENIO     DATE,
  NM_USUARIO_DEFINITIVO     VARCHAR2(15 BYTE),
  DT_REFERENCIA_TISS        DATE,
  CD_MEDICO_PROTOCOLO       VARCHAR2(10 BYTE),
  CD_CGC_PROCEDENCIA        VARCHAR2(14 BYTE),
  IE_ENVIADO                VARCHAR2(1 BYTE),
  DT_REGISTRO_FISICO        DATE,
  NR_CODIGO_CONTROLE        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROCONV_CLAATEN_FK_I ON TASY.PROTOCOLO_CONVENIO
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCONV_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCONV_CONPLAN_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_CONVENI_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_CONVENIO, IE_TIPO_PROTOCOLO, DT_MESANO_REFERENCIA, DT_VENCIMENTO, DT_ENTREGA_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_ESPPROC_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCONV_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCONV_ESTABEL_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_INTERFE_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_INTERFACE_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCONV_INTERFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCONV_I1 ON TASY.PROTOCOLO_CONVENIO
(DT_MESANO_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_I2 ON TASY.PROTOCOLO_CONVENIO
(NR_PROTOCOLO_SPSADT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_I3 ON TASY.PROTOCOLO_CONVENIO
(NR_PROTOCOLO_RI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_I4 ON TASY.PROTOCOLO_CONVENIO
(NR_PROTOCOLO_HONOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_I5 ON TASY.PROTOCOLO_CONVENIO
(NR_PROTOCOLO_CONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_I6 ON TASY.PROTOCOLO_CONVENIO
(NR_SEQ_DOC_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_LOTCONT_FK_I ON TASY.PROTOCOLO_CONVENIO
(NR_SEQ_LOTE_RECEITA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCONV_LOTCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCONV_LOTCONT_FK2_I ON TASY.PROTOCOLO_CONVENIO
(NR_SEQ_LOTE_REPASSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_LOTCONT_FK3_I ON TASY.PROTOCOLO_CONVENIO
(NR_SEQ_LOTE_GRAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_LOTPROT_I1 ON TASY.PROTOCOLO_CONVENIO
(NR_SEQ_LOTE_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_PESFISI_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_MEDICO_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_PESJURI_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_CGC_PROCEDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROCONV_PK ON TASY.PROTOCOLO_CONVENIO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_PQPROTO_FK_I ON TASY.PROTOCOLO_CONVENIO
(NR_SEQ_PQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROCONV_PROCEDE_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_PROCEDENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCONV_PROCEDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCONV_PROCEDI_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCONV_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCONV_SETATEN_FK_I ON TASY.PROTOCOLO_CONVENIO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCONV_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROCONV_SETATEN_FK2_I ON TASY.PROTOCOLO_CONVENIO
(CD_SETOR_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROCONV_SETATEN_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROCONV_UK ON TASY.PROTOCOLO_CONVENIO
(CD_CONVENIO, NR_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.protocolo_convenio_after
after insert or update ON TASY.PROTOCOLO_CONVENIO for each row
begin

if (inserting) then
	/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_protocolo,null,'PC',:new.dt_atualizacao,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_protocolo,null,'PC',:new.dt_atualizacao,'A',:new.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.protocolo_convenio_delete
before delete ON TASY.PROTOCOLO_CONVENIO for each row
begin

/* Grava o agendamento da informa��o para atualiza��o do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_seq_protocolo,null,'PC',:old.dt_atualizacao,'E',:old.nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.PROTOCOLO_CONVENIO_tp  after update ON TASY.PROTOCOLO_CONVENIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQ_PROTOCOLO);  ds_c_w:=null;gravar_log_alteracao(to_char(:old.DT_VENCIMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VENCIMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VENCIMENTO',ie_log_w,ds_w,'PROTOCOLO_CONVENIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Protocolo_convenio_AfterUpdate
AFTER UPDATE ON TASY.PROTOCOLO_CONVENIO 
FOR EACH ROW
DECLARE

nr_sequencia_w	Number(10,0);
ie_historico_w	Varchar2(01);
dt_referencia_w	Date;
nr_interno_conta_w	Number(10,0);
vl_conta_w		Number(15,2);
nr_atendimento_w	Number(10,0);
vl_protocolo_w		number(15,2);
ds_observacao_w		varchar2(255);
ie_grava_log_w		varchar2(01);
enviar_fat_int_w 		varchar2(1);

nr_seq_status_fat_w Number(10,0);
nr_seq_status_mob_w Number(10,0);
nr_seq_regra_fluxo_w Number(10,0);

cursor c01 is
select	nr_interno_conta,
	nr_atendimento,
	vl_conta
from	conta_paciente
where	nr_seq_protocolo	= :new.nr_seq_protocolo
and	nvl(vl_conta,0)	> 0;

cursor c02 is
	select 	nr_interno_conta
	from conta_paciente
	where nr_seq_protocolo 	= :new.nr_seq_protocolo;

BEGIN

Obter_Param_Usuario(85,141, obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento , ie_grava_log_w);
Obter_Param_Usuario(85,258, obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento , enviar_fat_int_w);

ie_historico_w	:= 'N';
if	(:new.ie_status_protocolo <> :old.ie_status_protocolo) then
	select nvl(max(ie_historico_conta),'N')
	into	ie_historico_w
	from	parametro_faturamento
	where	cd_estabelecimento	= :new.cd_estabelecimento;
	if	(ie_historico_w = 'S') then
		OPEN C01;
		LOOP
		FETCH C01 into 
			nr_interno_conta_w,
			nr_atendimento_w,
			vl_conta_w;
		exit when c01%notfound;
			select	conta_paciente_hist_seq.nextval
			into	nr_sequencia_w
			from	dual;
			insert into conta_paciente_hist(
				nr_sequencia, dt_atualizacao, nm_usuario,
				vl_conta, nr_seq_protocolo, nr_interno_conta,
				nr_nivel_anterior, nr_nivel_atual, dt_referencia,
				nr_atendimento, cd_convenio)
			values	(
				nr_sequencia_w, sysdate, :new.nm_usuario,
				vl_conta_w, :new.nr_seq_protocolo, nr_interno_conta_w,
				decode(:old.ie_status_protocolo,2, 10, 8), 
				decode(:new.ie_status_protocolo,2, 10, 8), 
				trunc(sysdate,'dd'), nr_atendimento_w, :new.cd_convenio);
		END LOOP;
		CLOSE C01;
	end if;
end if;

if	(ie_grava_log_w = 'N') or -- Se ie_grava_log_w = 'S' ent�o o sistema j� gravou o log na procedure  Atualizar_Ref_Protocolo_Conv
	(Obter_Funcao_Ativa <> 85) then

	if	(:new.DT_MESANO_REFERENCIA <> :old.DT_MESANO_REFERENCIA) then
		
		select 	nvl(obter_total_protocolo(:new.nr_seq_protocolo),0)		
		into	vl_protocolo_w
		from 	dual;
		
		ds_observacao_w	:= wheb_mensagem_pck.get_texto(298741,	'DT_MESANO_REFERENCIA_OLD_W='||to_char(:old.DT_MESANO_REFERENCIA,'dd/mm/yyyy')||
									';DT_MESANO_REFERENCIA_NEW_W='||to_char(:new.DT_MESANO_REFERENCIA,'dd/mm/yyyy'));		
		--ds_observacao_w:= 'Data de Refer�ncia ' || '(De: ' || :old.DT_MESANO_REFERENCIA || '  p/ ' || :new.DT_MESANO_REFERENCIA || ')';

		insert into protocolo_convenio_log
			(nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_protocolo,
			ds_arquivo_envio,
			vl_protocolo,
			ie_tipo_log,
			ds_observacao)
		values	(protocolo_convenio_log_seq.NextVal,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nr_seq_protocolo,
			:new.ds_arquivo_envio,
			vl_protocolo_w,
			'D',
			ds_observacao_w);

	end if;
end if;

if(:old.ie_status_protocolo = 2 and :new.ie_status_protocolo = 1) then
	OPEN C02;
	LOOP
	FETCH C02 into 
		 nr_interno_conta_w;
	exit when c02%notfound;
	
	cf_retornar_status_fat(nr_interno_conta_w, 
				 'P',
				 nr_seq_status_fat_w,
				 nr_seq_status_mob_w,
				 :new.nm_usuario);
			 
	END LOOP;
	CLOSE C02;
	
end if;

WHEB_USUARIO_PCK.set_ie_commit('N');
if	((nvl(:old.ie_status_protocolo,0) <> nvl(:new.ie_status_protocolo,0))
	 and (nvl(:new.ie_status_protocolo,0) = 2) and (enviar_fat_int_w = 'S'))then
	enviar_fat_intercompany(:new.nr_seq_protocolo, wheb_usuario_pck.get_nm_usuario);
end if;
WHEB_USUARIO_PCK.set_ie_commit('S');

END;
/


CREATE OR REPLACE TRIGGER TASY.Protocolo_convenio_BefUpdate
BEFORE UPDATE ON TASY.PROTOCOLO_CONVENIO FOR EACH ROW
DECLARE

qt_nota_fiscal_w	number(10,0);
qt_titulo_w		number(10,0);

BEGIN

if	(:old.ie_status_protocolo = 2) and
	(:new.ie_status_protocolo = 1) then

	select	count(*)
	into	qt_nota_fiscal_w
	from	nota_fiscal
	where	ie_situacao = '1'
	and	nr_seq_protocolo 	= :new.nr_seq_protocolo;

	if	(qt_nota_fiscal_w <> 0) then
		--R.aise_application_error(-20011,'O status do protocolo n�o pode ser alterado, pois o mesmo j� possui notas vinculadas!');
		wheb_mensagem_pck.exibir_mensagem_abort(263424);
	end if;

	select	count(*)
	into	qt_titulo_w
	from	titulo_receber
	where	nr_seq_protocolo = :new.nr_seq_protocolo;

	if (:new.nr_seq_lote_protocolo is not null) and
	   (qt_titulo_w = 0) then

		select	count(*)
		into	qt_titulo_w
		from	titulo_receber
		where	NR_SEQ_LOTE_PROT = :new.nr_seq_lote_protocolo;

	end if;

	if	(qt_titulo_w <> 0) then
		--R.aise_application_error(-20011,'O status do protocolo n�o pode ser alterado, pois o mesmo j� possui t�tulos vinculados!');
		wheb_mensagem_pck.exibir_mensagem_abort(263425);
	end if;

end if;

if	(:new.ie_status_protocolo = 2) then
	:new.nm_usuario_definitivo	:= wheb_usuario_pck.get_nm_usuario;
elsif	(:new.ie_status_protocolo = 1) then
	:new.nm_usuario_definitivo	:= null;
end if;

if (:old.nr_seq_doc_convenio <> :new.nr_seq_doc_convenio) then
	:new.dt_alter_doc_convenio := sysdate;
	:new.nm_usuario_alt_doc_conv := :new.nm_usuario;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.protocolo_convenio_afinsert
after insert ON TASY.PROTOCOLO_CONVENIO for each row
declare
ds_observacao_w varchar2(255);
begin
ds_observacao_w := wheb_mensagem_pck.get_texto(352443);
insert into protocolo_convenio_log
	(nr_sequencia,
	dt_atualizacao,
	nm_usuario,
	dt_atualizacao_nrec,
	nm_usuario_nrec,
	nr_seq_protocolo,
	ds_arquivo_envio,
	vl_protocolo,
	ie_tipo_log,
	ds_observacao)
values	(protocolo_convenio_log_seq.NextVal,
	sysdate,
	:new.nm_usuario,
	sysdate,
	:new.nm_usuario,
	:new.nr_seq_protocolo,
	:new.ds_arquivo_envio,
	0,
	'C',
	ds_observacao_w);
end;
/


CREATE OR REPLACE TRIGGER TASY.protocolo_convenio_befinsert
before insert ON TASY.PROTOCOLO_CONVENIO for each row
declare
begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
/* Campo obrigatorio removido do DBPanel OS 2286124 */
:new.nr_seq_lote_repasse := nvl(:new.nr_seq_lote_repasse,0);
end if;
end protocolo_convenio_befinsert;
/


CREATE OR REPLACE TRIGGER TASY.Protocolo_convenio_atual
   AFTER UPDATE ON TASY.PROTOCOLO_CONVENIO    FOR EACH ROW
DECLARE
   PRAGMA AUTONOMOUS_TRANSACTION;

   ie_operacao_w number(1) := 0;
   ie_tipo_convenio_w     convenio.ie_tipo_convenio%type;
   ie_geracao_w           ctb_regra_geracao_lote_rec.ie_geracao%type;
   ie_ctb_online_w        varchar2(1);

begin
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') then
        if (:old.ie_status_protocolo = 1 and :new.ie_status_protocolo = 2) then
            ie_operacao_w := 1;
        elsif (:old.ie_status_protocolo = 2 and :new.ie_status_protocolo = 1) then
            ie_operacao_w := 2;
        end if;

        if (ie_operacao_w <> 0) then
            ie_ctb_online_w := ctb_online_pck.get_modo_lote(6, :new.cd_estabelecimento);

            select ie_tipo_convenio
            into   ie_tipo_convenio_w
            from   convenio
            where  cd_convenio = :new.cd_convenio;

            ie_geracao_w    := ctb_online_pck.get_geracao_lote_receita(:new.cd_convenio,
                                                                        :new.cd_estabelecimento,
                                                                        :new.nm_usuario,
                                                                        ie_tipo_convenio_w);

            if (ie_ctb_online_w = 'S' and ie_geracao_w = 'FPR') then

                ctb_contab_onl_lote_receita(nr_seq_protocolo_p  =>  :new.nr_seq_protocolo,
                                            nr_interno_conta_p  =>  null,
                                            nm_usuario_p        =>  :new.nm_usuario,
                                            ie_operacao_p       =>  ie_operacao_w,
                                            dt_referencia_p     =>  sysdate);
            end if;
        end if;
    end if;
end;
/


ALTER TABLE TASY.PROTOCOLO_CONVENIO ADD (
  CONSTRAINT PROCONV_PK
 PRIMARY KEY
 (NR_SEQ_PROTOCOLO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PROCONV_UK
 UNIQUE (CD_CONVENIO, NR_PROTOCOLO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          768K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROTOCOLO_CONVENIO ADD (
  CONSTRAINT PROCONV_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_PROTOCOLO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROCONV_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PROCEDENCIA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PROCONV_PROCEDE_FK 
 FOREIGN KEY (CD_PROCEDENCIA) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA),
  CONSTRAINT PROCONV_INTERF_FK 
 FOREIGN KEY (CD_INTERFACE_ENVIO) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT PROCONV_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT PROCONV_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PROCONV_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PROCONV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PROCONV_LOTCONT_FK 
 FOREIGN KEY (NR_SEQ_LOTE_RECEITA) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PROCONV_LOTCONT_FK2 
 FOREIGN KEY (NR_SEQ_LOTE_REPASSE) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PROCONV_LOTCONT_FK3 
 FOREIGN KEY (NR_SEQ_LOTE_GRAT) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PROCONV_PQPROTO_FK 
 FOREIGN KEY (NR_SEQ_PQ_PROTOCOLO) 
 REFERENCES TASY.PQ_PROTOCOLO (NR_SEQUENCIA),
  CONSTRAINT PROCONV_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT PROCONV_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PROCONV_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_PACIENTE) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PROCONV_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROTOCOLO_CONVENIO TO NIVEL_1;

