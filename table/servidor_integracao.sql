ALTER TABLE TASY.SERVIDOR_INTEGRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SERVIDOR_INTEGRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SERVIDOR_INTEGRACAO
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DS_INFORMACAO               VARCHAR2(200 BYTE),
  NR_PORTA                    NUMBER(10),
  IP_CONEXAO                  VARCHAR2(100 BYTE),
  NR_PORTA_LISTENER           NUMBER(10),
  QT_MAX_RETRANSMISSAO        NUMBER(5),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  QT_INTERVALO_RETRANSMISSAO  NUMBER(10),
  QT_MAX_RECONEXAO            NUMBER(5),
  QT_INTERVALO_RECONEXAO      NUMBER(5),
  QT_INTERVALO_MINIMO_ENVIO   NUMBER(5),
  IE_SERVIDOR                 VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SERVINT_PK ON TASY.SERVIDOR_INTEGRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SERVINT_UK ON TASY.SERVIDOR_INTEGRACAO
(IP_CONEXAO, NR_PORTA, NR_PORTA_LISTENER, IE_SERVIDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SERVIDOR_INTEGRACAO_UPDATE
before update or insert ON TASY.SERVIDOR_INTEGRACAO for each row
begin

if (UPPER(:new.IP_CONEXAO) = 'LOCALHOST') then
  WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(1067474);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.SERVIDOR_INTEGRACAO_tp  after update ON TASY.SERVIDOR_INTEGRACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_SERVIDOR,1,500);gravar_log_alteracao(substr(:old.IE_SERVIDOR,1,4000),substr(:new.IE_SERVIDOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_SERVIDOR',ie_log_w,ds_w,'SERVIDOR_INTEGRACAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.IP_CONEXAO,1,500);gravar_log_alteracao(substr(:old.IP_CONEXAO,1,4000),substr(:new.IP_CONEXAO,1,4000),:new.nm_usuario,nr_seq_w,'IP_CONEXAO',ie_log_w,ds_w,'SERVIDOR_INTEGRACAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_PORTA_LISTENER,1,500);gravar_log_alteracao(substr(:old.NR_PORTA_LISTENER,1,4000),substr(:new.NR_PORTA_LISTENER,1,4000),:new.nm_usuario,nr_seq_w,'NR_PORTA_LISTENER',ie_log_w,ds_w,'SERVIDOR_INTEGRACAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_INFORMACAO,1,500);gravar_log_alteracao(substr(:old.DS_INFORMACAO,1,4000),substr(:new.DS_INFORMACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_INFORMACAO',ie_log_w,ds_w,'SERVIDOR_INTEGRACAO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_PORTA,1,500);gravar_log_alteracao(substr(:old.NR_PORTA,1,4000),substr(:new.NR_PORTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_PORTA',ie_log_w,ds_w,'SERVIDOR_INTEGRACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SERVIDOR_INTEGRACAO ADD (
  CONSTRAINT SERVINT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT SERVINT_UK
 UNIQUE (IP_CONEXAO, NR_PORTA, NR_PORTA_LISTENER, IE_SERVIDOR)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SERVIDOR_INTEGRACAO TO NIVEL_1;

