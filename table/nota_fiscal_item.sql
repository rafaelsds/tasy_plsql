ALTER TABLE TASY.NOTA_FISCAL_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL_ITEM
(
  NR_SEQUENCIA               NUMBER(10),
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  CD_CGC_EMITENTE            VARCHAR2(14 BYTE),
  CD_SERIE_NF                VARCHAR2(255 BYTE) NOT NULL,
  NR_SEQUENCIA_NF            NUMBER(10)         NOT NULL,
  NR_ITEM_NF                 NUMBER(5)          NOT NULL,
  CD_NATUREZA_OPERACAO       NUMBER(4),
  QT_ITEM_NF                 NUMBER(13,4)       NOT NULL,
  VL_UNITARIO_ITEM_NF        NUMBER(13,4)       NOT NULL,
  VL_TOTAL_ITEM_NF           NUMBER(13,2)       NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  VL_FRETE                   NUMBER(13,2)       NOT NULL,
  VL_DESCONTO                NUMBER(13,2)       NOT NULL,
  VL_DESPESA_ACESSORIA       NUMBER(13,2)       NOT NULL,
  CD_MATERIAL                NUMBER(6),
  CD_PROCEDIMENTO            NUMBER(15),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  CD_CONTA                   NUMBER(5),
  CD_LOCAL_ESTOQUE           NUMBER(4),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  QT_PESO_BRUTO              NUMBER(13,4),
  QT_PESO_LIQUIDO            NUMBER(13,4),
  CD_UNIDADE_MEDIDA_COMPRA   VARCHAR2(30 BYTE),
  QT_ITEM_ESTOQUE            NUMBER(13,4),
  CD_UNIDADE_MEDIDA_ESTOQUE  VARCHAR2(30 BYTE),
  CD_LOTE_FABRICACAO         VARCHAR2(20 BYTE),
  DT_VALIDADE                DATE,
  DT_ATUALIZACAO_ESTOQUE     DATE,
  CD_CONTA_CONTABIL          VARCHAR2(20 BYTE),
  VL_DESCONTO_RATEIO         NUMBER(13,4)       NOT NULL,
  VL_SEGURO                  NUMBER(13,4)       NOT NULL,
  CD_CENTRO_CUSTO            NUMBER(8),
  CD_MATERIAL_ESTOQUE        NUMBER(6),
  IE_ORIGEM_PROCED           NUMBER(10),
  NR_ORDEM_COMPRA            NUMBER(10),
  VL_LIQUIDO                 NUMBER(15,2)       NOT NULL,
  PR_DESCONTO                NUMBER(7,4),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  NR_ITEM_OCI                NUMBER(5),
  DT_ENTREGA_ORDEM           DATE,
  NR_SEQ_CONTA_FINANC        NUMBER(10),
  NR_CONTRATO                NUMBER(10),
  DS_COMPLEMENTO             VARCHAR2(4000 BYTE),
  NR_SEQ_PROJ_REC            NUMBER(10),
  VL_PROJETO                 NUMBER(15,2),
  NR_ATENDIMENTO             NUMBER(10)         DEFAULT null,
  PR_DESC_FINANC             NUMBER(7,4),
  VL_DESC_FINANC             NUMBER(15,2),
  NR_SEQ_LIC_ITEM            NUMBER(10),
  NR_SEQ_ORDEM_SERV          NUMBER(10),
  DS_INCONSISTENCIA          VARCHAR2(255 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  VL_LIQ_MOEDA_REF           NUMBER(15,2),
  NR_SEQ_LOTE_FORNEC         NUMBER(10),
  NR_SEQ_UNIDADE_ADIC        NUMBER(10),
  NR_EMPRESTIMO              NUMBER(10),
  NR_SEQ_ITEM_EMPRESTIMO     NUMBER(5),
  IE_ATUALIZAR_BARRAS        VARCHAR2(1 BYTE),
  CD_BARRA_MATERIAL          VARCHAR2(40 BYTE),
  NR_SEQUENCIA_VINC_CONSIG   NUMBER(10),
  NR_SEQ_PROJ_GPI            NUMBER(10),
  NR_SEQ_ETAPA_GPI           NUMBER(10),
  NR_SEQ_CONTA_GPI           NUMBER(10),
  NR_NOTA_FISCAL             VARCHAR2(255 BYTE) DEFAULT null,
  NR_SEQ_INSPECAO            NUMBER(10),
  DS_BARRAS                  VARCHAR2(4000 BYTE),
  CD_IMOBILIZADO             VARCHAR2(20 BYTE),
  CD_IMOB_SERIE              VARCHAR2(20 BYTE),
  IE_INDETERMINADO           VARCHAR2(1 BYTE),
  NR_NFE_IMP                 VARCHAR2(255 BYTE),
  NR_SEQ_AG_PAC_OPME         NUMBER(10),
  NR_SEQ_AGENDA_PAC          NUMBER(10),
  NR_SEQ_CLASSIF_TRIB        NUMBER(10),
  NR_SEQ_CNAE                NUMBER(10),
  NR_SEQ_MARCA               NUMBER(10),
  DT_INICIO_GARANTIA         DATE,
  DT_FIM_GARANTIA            DATE,
  NR_SEQ_ORC_ITEM_GPI        NUMBER(10),
  NR_SEQ_EQUIPAMENTO         NUMBER(10),
  DT_FABRICACAO              DATE,
  NR_SEQ_VARIACAO_FISCAL     NUMBER(10),
  NR_SEQ_ROMANEIO            NUMBER(10),
  NR_SEQ_ROMANEIO_ITEM       NUMBER(10),
  IE_FORMA_GERADO_EMPRES     VARCHAR2(2 BYTE),
  NR_SEQ_REGRA_CONTRATO      NUMBER(10),
  NR_SOLIC_COMPRA            NUMBER(10),
  NR_ITEM_SOLIC_COMPRA       NUMBER(5),
  NR_DOC_IMPORTACAO          VARCHAR2(80 BYTE),
  DT_REG_IMPORTACAO          DATE,
  DS_LOCAL_DESEMB            VARCHAR2(80 BYTE),
  SG_UF_DESEMB               VARCHAR2(2 BYTE),
  DT_DESEMB                  DATE,
  CD_EXPORTADOR              VARCHAR2(80 BYTE),
  NR_ADICAO                  VARCHAR2(80 BYTE),
  NR_SEQ_ADICAO              VARCHAR2(80 BYTE),
  CD_FAB_ESTRANGEIRO         VARCHAR2(80 BYTE),
  VL_DESC_ADICAO             NUMBER(15,2),
  NR_SEQ_PROC_INTERNO        NUMBER(10),
  DT_LIB_ESTRUT_PJ           DATE,
  NM_USUARIO_LIB             VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA_LIB       VARCHAR2(255 BYTE),
  NR_ITEM_RESP_CONSIG        NUMBER(10),
  NR_ITEM_RESP_CONSIG_DEV    NUMBER(10),
  IE_MATERIAL_ESTOQUE        VARCHAR2(1 BYTE),
  NR_SEQ_PRODUTO             NUMBER(10),
  NR_ITEM_SEQUENCIAL         NUMBER(5),
  NR_LAUDO                   VARCHAR2(80 BYTE),
  NR_SEQ_PRECO_PJ            NUMBER(10),
  NR_SEQ_APROVACAO           NUMBER(10),
  DT_APROVACAO               DATE,
  NM_USUARIO_APROV           VARCHAR2(15 BYTE),
  DT_REPROVACAO              DATE,
  IE_MOTIVO_REPROVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA_REPROV    VARCHAR2(255 BYTE),
  NR_SEQ_NF_ORIG             NUMBER(10),
  NR_SEQ_ITEM_NF_ORIG        NUMBER(5),
  IE_BARRAS_LIDO             VARCHAR2(1 BYTE),
  VL_LIQUIDO_CFOP            NUMBER(15,2),
  NR_ATEND_LANCAMENTO        NUMBER(10),
  CD_HISTORICO               NUMBER(10),
  IE_PEND_TAB_PRECO          VARCHAR2(1 BYTE),
  CD_FORNECEDOR_CONSIG       VARCHAR2(14 BYTE),
  NR_SERIE_MATERIAL          VARCHAR2(80 BYTE),
  NR_SEQ_PROC_APROV          NUMBER(10),
  DT_ATEND_LANCAMENTO        DATE,
  CD_PACIENTE                VARCHAR2(10 BYTE),
  NR_SEQ_MODELO              NUMBER(10),
  NR_SEQ_REGRA_PROT_CONT     NUMBER(10),
  DT_DESDOBR_APROV           DATE,
  NR_PRESCRICAO              NUMBER(14),
  VL_UNIT_ESTRANGEIRO        NUMBER(13,4),
  VL_TOTAL_ESTRANGEIRO       NUMBER(13,4),
  NR_SEQ_FAR_VENDA_ITEM      NUMBER(10),
  VL_DIF_ESTRANGEIRO         NUMBER(13,4),
  VL_ITEM_PRECO_PUB          NUMBER(13,4),
  IE_SELECIONADO             VARCHAR2(1 BYTE),
  CD_SEQUENCIA_PARAMETRO     NUMBER(10),
  DS_ORIGEM_VALOR            VARCHAR2(255 BYTE),
  CD_MAT_PROC_FORNEC         VARCHAR2(255 BYTE),
  NR_SEQ_OPM                 NUMBER(10),
  NR_SEQ_TIT_REC             NUMBER(5),
  NR_TITULO                  NUMBER(10),
  IE_TIPO_PROGRAMA           VARCHAR2(15 BYTE),
  IE_CLASSIF_RENAME          VARCHAR2(1 BYTE),
  IE_ORIGEM_MANUAL           VARCHAR2(1 BYTE),
  CD_PROC_CONVENIO           VARCHAR2(20 BYTE),
  DS_PROC_CONVENIO           VARCHAR2(240 BYTE),
  CD_MATERIAL_CONVENIO       VARCHAR2(20 BYTE),
  DS_MATERIAL_CONVENIO       VARCHAR2(200 BYTE),
  CD_CATEGORIA_IVA           NUMBER(6),
  NR_OC_COBRANCA             NUMBER(10),
  NR_ITEM_OC_COBRANCA        NUMBER(5),
  CD_FEDERAL_VOUCHER         VARCHAR2(4000 BYTE) DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          88M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NOTFIIT_AGEPACI_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_AGENDA_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_AGPOPME_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_AG_PAC_OPME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_AGPOPME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_ATEPACI_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_ATENDIMENTO, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_CENCUST_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_CONCONT_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_CONFINA_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_CONFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_CONRENF_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_REGRA_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_CONRENF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_CONRNFP_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_REGRA_PROT_CONT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_CONRNFP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_CONTRAT_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_CONTRATO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_DISROIT_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_ROMANEIO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_DISROIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_DISROMA_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_ROMANEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_DISROMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_EMPMATE_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_EMPRESTIMO, NR_SEQ_ITEM_EMPRESTIMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_FARVNDI_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_FAR_VENDA_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_GPICRETA_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_ETAPA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_GPIORIT_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_ORC_ITEM_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_GPIORIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_GPIPLAN_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_CONTA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_GPIPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_GPIPROJ_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_PROJ_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_INSRECE_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_INSPECAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_I1 ON TASY.NOTA_FISCAL_ITEM
(CD_ESTABELECIMENTO, DT_ATUALIZACAO, CD_MATERIAL, CD_MATERIAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          21M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_I1
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_I2 ON TASY.NOTA_FISCAL_ITEM
(CD_ESTABELECIMENTO, DT_ATUALIZACAO, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          528K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_I3 ON TASY.NOTA_FISCAL_ITEM
(CD_ESTABELECIMENTO, CD_LOCAL_ESTOQUE, DT_ATUALIZACAO, CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          672K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_LICITEM_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_LIC_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_LICITEM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_LOCESTO_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          8M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_MANEQUI_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_MANEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_MANMODE_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_MANMODE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_MANORSE_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_MARCA_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_MARCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_MARCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_MATERIA_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_MATESTO_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_MATERIAL_ESTOQUE, DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          18M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_MATLOFO_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_NATOPER_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_NATUREZA_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_NATOPER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_NOTFIIT_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_NF_ORIG, NR_SEQ_ITEM_NF_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_NOTFISC_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_ORDCOMP_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_ORDCOMP_FK_II ON TASY.NOTA_FISCAL_ITEM
(NR_OC_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_ORDCOMP_FK_II2 ON TASY.NOTA_FISCAL_ITEM
(NR_OC_COBRANCA, NR_ITEM_OC_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_ORDCOMP_FK_I2 ON TASY.NOTA_FISCAL_ITEM
(NR_ORDEM_COMPRA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_ORPROPM_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_OPM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_PESFISI_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_PACIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_PESJURI_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_FORNECEDOR_CONSIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.NOTFIIT_PK ON TASY.NOTA_FISCAL_ITEM
(NR_SEQUENCIA, NR_ITEM_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_PLSCNAE_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_CNAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_PLSCNAE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_PRESMED_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_PROCEDI_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_PROCOMP_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_APROVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_PRODFIN_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_PRODFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_PROINTE_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_PRORECU_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_PROJ_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_SETATEN_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_SITTRPS_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_CLASSIF_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          352K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_SITTRPS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_SOLCOIT_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SOLIC_COMPRA, NR_ITEM_SOLIC_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NOTFIIT_SUCOIDE_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_ITEM_RESP_CONSIG_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_SUCOIDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_SURECOI_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_ITEM_RESP_CONSIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_SURECOI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_UNIMECO_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_UNIDADE_MEDIDA_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_UNIMECO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_UNIMEES_FK_I ON TASY.NOTA_FISCAL_ITEM
(CD_UNIDADE_MEDIDA_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_UNIMEES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NOTFIIT_UNMACOM_FK_I ON TASY.NOTA_FISCAL_ITEM
(NR_SEQ_UNIDADE_ADIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NOTFIIT_UNMACOM_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_item_atual
before insert or update ON TASY.NOTA_FISCAL_ITEM for each row
declare

cd_estabelecimento_w	number(15,0);
ie_gerar_barra_w		varchar2(1);
ie_altera_conta_movto_w	varchar2(1);
nr_movimento_estoque_w	varchar2(10);
ie_tipo_ordem_w		varchar2(10);
ds_historico_w		varchar2(255);
cd_material_estoque_w	number(6);
ie_material_estoque_w	varchar2(1);
cd_cgc_emitente_w	nota_fiscal.cd_cgc_emitente%type;
cd_operacao_nf_w		operacao_nota.cd_operacao_nf%type;
nr_seq_cnae_w		pls_cnae.nr_sequencia%type;
nr_seq_classif_trib_w	nota_fiscal_item.nr_seq_classif_trib%type;
cd_procedimento_w	proc_interno.cd_procedimento%type;
nr_seq_marca_w		marca.nr_sequencia%type;


cursor c01 is
select	nr_movimento_estoque
from	movimento_estoque
where	ie_origem_documento = 1
and	nr_seq_tab_orig = :new.nr_sequencia
and	nr_sequencia_item_docto = :new.nr_item_nf;

begin

if (obter_bloq_canc_proj_rec(:new.nr_seq_proj_rec) > 0) then
    wheb_mensagem_pck.exibir_mensagem_abort(1144309);  -- Registro associado a um projeto bloqueado ou cancelado.
end if;

select	cd_estabelecimento,
	cd_operacao_nf,
	cd_cgc_emitente
into	cd_estabelecimento_w,
	cd_operacao_nf_w,
	cd_cgc_emitente_w
from	nota_fiscal
where	nr_sequencia  = :new.nr_sequencia;

if	(nvl(:new.vl_unit_estrangeiro,0) > 0) then
	begin

	if	(nvl(:new.vl_unit_estrangeiro,0) >= nvl(:new.vl_unitario_item_nf,0)) then

		:new.vl_dif_estrangeiro := nvl(:new.vl_unit_estrangeiro,0) - nvl(:new.vl_unitario_item_nf,0);
	else
		:new.vl_dif_estrangeiro := nvl(:new.vl_unitario_item_nf,0) - nvl(:new.vl_unit_estrangeiro,0);
	end if;

	end;
end if;

if	(nvl(:new.cd_material,0) > 0) then
	begin
	select	nvl(max(cd_material_estoque),:new.cd_material)
	into	cd_material_estoque_w
	from	material
	where	cd_material = :new.cd_material;

	:new.cd_material_estoque	:= cd_material_estoque_w;

	select	nvl(max(ie_material_estoque),'N')
	into	ie_material_estoque_w
	from	material_estab
	where	cd_material = cd_material_estoque_w
	and	cd_estabelecimento = cd_estabelecimento_w;

	:new.ie_material_estoque	:= ie_material_estoque_w;
	end;
else
	begin
	:new.cd_material_estoque	:= null;
	:new.ie_material_estoque	:= null;
	end;
end if;

select	(max(obter_valor_param_usuario(40, 148, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w)))
into	ie_altera_conta_movto_w
from	dual;

if	(:new.pr_desc_financ is null) then
	:new.pr_desc_financ	:= 0;
end if;

if	(nvl(:new.cd_procedimento,0) > 0) and
	(nvl(:new.cd_material,0) = 0) then
	begin
	:new.cd_unidade_medida_estoque	:= null;
	:new.cd_unidade_medida_compra	:= null;
	end;
end if;

if	(nvl(:new.vl_desc_financ,0) = 0) and
	((nvl(:old.pr_desc_financ,0) <> nvl(:new.pr_desc_financ,0)) or
	(nvl(:old.vl_liquido,0) <> nvl(:new.vl_liquido,0))) then
	:new.vl_desc_financ	:= nvl(:new.vl_liquido,0) * nvl(:new.pr_desc_financ,0) / 100;
end if;

ctb_consistir_conta_titulo(:new.cd_conta_contabil, Wheb_mensagem_pck.get_Texto(303614)); /*'Conta contabil'*/
ctb_consistir_conta_estab(cd_estabelecimento_w, :new.cd_conta_contabil, Wheb_mensagem_pck.get_Texto(303614)); /*'Conta contabil'*/

/*buscando da regra e identifica no item, se o barras do material deve ser atualizado*/
if	(:new.cd_material is not null) then
	obter_regra_atualizar_barras(:new.cd_material, cd_estabelecimento_w, ie_gerar_barra_w);
	if	(ie_gerar_barra_w <> 'N') then
		:new.ie_atualizar_barras	:= ie_gerar_barra_w;
	end if;
end if;

if	(ie_altera_conta_movto_w = 'S') then

	open c01;
	loop
	fetch c01 into
		nr_movimento_estoque_w;
	exit when c01%notfound;
		begin

		update	movimento_estoque
		set	cd_conta_contabil	= :new.cd_conta_contabil,
			cd_centro_custo	= :new.cd_centro_custo
		where	nr_movimento_estoque = nr_movimento_estoque_w;

		end;
	end loop;
	close c01;
end if;

if	(:new.nr_ordem_compra is not null) and
	(:old.vl_unitario_item_nf > 0) and
	(:old.vl_unitario_item_nf <> :new.vl_unitario_item_nf) then
	begin
	select	ie_tipo_ordem
	into	ie_tipo_ordem_w
	from	ordem_compra
	where	nr_ordem_compra = :new.nr_ordem_compra;

	if	(ie_tipo_ordem_w = 'T') then
		begin
		ds_historico_w := substr(WHEB_MENSAGEM_PCK.get_texto(303615,'CD_MATERIAL_W='|| :new.cd_material ||';NR_ITEM_NF_W='|| :new.nr_item_nf ||';VL_UNITARIO_ITEM_NF_DE_W='|| campo_mascara_virgula(:old.vl_unitario_item_nf) ||';VL_UNITARIO_ITEM_NF_PARA_W='|| campo_mascara_virgula(:new.vl_unitario_item_nf)),1,255);
				/*Material [#@CD_MATERIAL_W#@] item [#@NR_ITEM_NF_W#@]. De: #@VL_UNITARIO_ITEM_NF_DE_W#@ Para: #@VL_UNITARIO_ITEM_NF_PARA_W#@*/

		gerar_historico_nota_fiscal(
			:new.nr_sequencia,
			:new.nm_usuario,
			'40',
			ds_historico_w);
		end;
	end if;
	end;
end if;

if	(inserting) then

	select nvl(obter_cnae_emitente(	cd_estabelecimento_w,
					cd_cgc_emitente_w,
					obter_item_servico_proced(:new.cd_procedimento, :new.ie_origem_proced),
					:new.cd_procedimento),0)
	into	nr_seq_cnae_w
	from	dual;

	if	(nr_seq_cnae_w = 0) and
		(cd_operacao_nf_w > 0)	then
		begin

		select	nvl(max(nr_seq_cnae),0)
		into	nr_seq_cnae_w
		from	operacao_nota
		where	cd_operacao_nf = cd_operacao_nf_w;

		end;
	end if;

	if	(nr_seq_cnae_w > 0) then
		:new.nr_seq_cnae := nr_seq_cnae_w;
	end if;
end if;

select	obter_cst_regra_item_nf(:new.nr_sequencia, nvl(:new.cd_material,0),nvl(:new.cd_procedimento,0),nvl(:new.ie_origem_proced,0)) nr_cst
into	nr_seq_classif_trib_w
from	dual;

if	(inserting) and
	(nvl(:old.nr_seq_classif_trib,0) = 0) and
	(nr_seq_classif_trib_w > 0) then
	:new.nr_seq_classif_trib := nr_seq_classif_trib_w;
end if;

if	(updating) and
	(nvl(:old.nr_seq_classif_trib,0) = 0) and
	(nr_seq_classif_trib_w > 0) then
	:new.nr_seq_classif_trib := nr_seq_classif_trib_w;
end if;

if	(:new.nr_seq_lote_fornec is not null)  then
	begin
	select	nvl(nr_seq_marca,:new.nr_seq_marca)
	into	:new.nr_seq_marca
	from	material_lote_fornec
	where	nr_sequencia = :new.nr_seq_lote_fornec;
	exception
	when others then

		null;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_item_AfterInsert
after insert or update ON TASY.NOTA_FISCAL_ITEM for each row
declare

cd_estabelecimento_w		number(4);
dt_atualizacao_estoque_w		date;
ie_variacao_fiscal_nf_w		varchar2(1);
qt_existe_w			number(10);

begin

select	cd_estabelecimento,
	dt_atualizacao_estoque
into	cd_estabelecimento_w,
	dt_atualizacao_estoque_w
from	nota_fiscal
where	nr_sequencia = :new.nr_sequencia;

select	count(*)
into	qt_existe_w
from	fis_variacao_fiscal
where	ie_situacao = 'A';

select	nvl(max(ie_variacao_fiscal_nf),'N')
into	ie_variacao_fiscal_nf_w
from	parametro_compras
where	cd_estabelecimento = cd_estabelecimento_w;

if	(dt_atualizacao_estoque_w is null) and
	(ie_variacao_fiscal_nf_w = 'S') and
	(qt_existe_w > 0) then

	gerar_tributos_item_nf(	:new.nr_sequencia, :new.nr_item_nf, :new.nr_seq_variacao_fiscal, :new.vl_liquido, :new.nm_usuario);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_item_delete
BEFORE DELETE ON TASY.NOTA_FISCAL_ITEM FOR EACH ROW
DECLARE

qt_existe_w		number(10);

BEGIN

if	(nvl(:old.nr_seq_inspecao,0) > 0) then
	select	count(*)
	into	qt_existe_w
	from	inspecao_recebimento
	where	nr_sequencia = nvl(:old.nr_seq_inspecao,0);

	if	(qt_existe_w > 0) then
		update	inspecao_recebimento
		set	nr_seq_item_nf = null
		where	nr_sequencia = :old.nr_seq_inspecao;
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.NOTA_FISCAL_ITEM_tp  after update ON TASY.NOTA_FISCAL_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA)||'#@#@NR_ITEM_NF='||to_char(:old.NR_ITEM_NF);gravar_log_alteracao(substr(:old.CD_MATERIAL_CONVENIO,1,4000),substr(:new.CD_MATERIAL_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_CONVENIO',ie_log_w,ds_w,'NOTA_FISCAL_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROC_CONVENIO,1,4000),substr(:new.CD_PROC_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROC_CONVENIO',ie_log_w,ds_w,'NOTA_FISCAL_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_item_insert
before insert or update ON TASY.NOTA_FISCAL_ITEM for each row
declare

ie_pais_w	valor_dominio.vl_dominio%type;
cd_natureza_operacao_w	natureza_operacao.cd_natureza_operacao%type;

begin
ie_pais_w	:=	nvl(pkg_i18n.get_user_locale, 'pt_BR');

if	(ie_pais_w = 'es_MX') or
	(ie_pais_w = 'es_DO') or
	(ie_pais_w = 'ja_JP') then
	begin

	select	nvl(max(cd_natureza_operacao), 0)
	into	cd_natureza_operacao_w
	from	nota_fiscal
	where	nr_sequencia = :new.nr_sequencia;

	if	(cd_natureza_operacao_w <>  :new.cd_natureza_operacao) then
			:new.cd_natureza_operacao := cd_natureza_operacao_w;
	end if;

	if	(cd_natureza_operacao_w = 0) and
		(:new.cd_natureza_operacao = 0) and
		((ie_pais_w = 'es_DO') OR (ie_pais_w = 'ja_JP')) then
		begin
			:new.cd_natureza_operacao := null;
		end;
		end if;
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.NOTA_FISCAL_ITEM_BEF_INS_UPD
BEFORE INSERT OR UPDATE ON TASY.NOTA_FISCAL_ITEM FOR EACH ROW
DECLARE

qt_items_w		number(10);
qt_items_voucher_w		number(10);

pragma autonomous_transaction;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S' AND nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'es_AR')  then
  
  SELECT count(*)
  INTO qt_items_w
  FROM NOTA_FISCAL_ITEM
  WHERE NR_SEQUENCIA = :new.NR_SEQUENCIA;

  SELECT count(*)
  INTO qt_items_voucher_w
  FROM NOTA_FISCAL_ITEM
  WHERE NR_SEQUENCIA = :new.NR_SEQUENCIA
  AND CD_FEDERAL_VOUCHER is not null;

  if (inserting and qt_items_voucher_w > 0) then
    wheb_mensagem_pck.exibir_mensagem_abort(1166236);
  end if;
  if ((inserting and (:new.CD_FEDERAL_VOUCHER is not null AND qt_items_w > 0)) or (updating and :new.CD_FEDERAL_VOUCHER is not null AND qt_items_w > 1)) then
    wheb_mensagem_pck.exibir_mensagem_abort(1166237);
  end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.nota_fiscal_item_beforeInsert
before insert or update ON TASY.NOTA_FISCAL_ITEM for each row
declare


cd_natureza_operacao_w		number(4);
nr_seq_variacao_fiscal_w	number(10);
nr_seq_produto_w		number(10);
nr_seq_grupo_prod_w		number(10);
nr_ordem_compra_w		number(10);
qt_existe_w			number(5);
cd_ext_material_w		number(10);
nr_solic_compra_w		solic_compra.nr_solic_compra%type;
nr_item_solic_compra_w		solic_compra_item.nr_item_solic_compra%type;
nr_seq_orc_item_gpi_w		nota_fiscal_item.nr_seq_orc_item_gpi%type;
nr_seq_orc_item_gpi_ww		nota_fiscal_item.nr_seq_orc_item_gpi%type;
ie_tipo_nota_w			nota_fiscal.ie_tipo_nota%type;
ie_entrada_saida_w		varchar2(10);
ie_tipo_operacao_w		operacao_nota.ie_tipo_operacao%type;
cd_grupo_material_w     	grupo_material.cd_grupo_material%type;
cd_subgrupo_material_w     	subgrupo_material.cd_subgrupo_material%type;
cd_classe_material_w     	classe_material.cd_classe_material%type;
nr_seq_familia_w     		material.nr_seq_familia%type;


begin

select	obter_dados_variacao_fiscal(:new.nr_Sequencia, :new.cd_material, 'N'),
	obter_dados_variacao_fiscal(:new.nr_Sequencia, :new.cd_material, 'S')
into	cd_natureza_operacao_w,
	nr_seq_variacao_fiscal_w
from	dual;

if	(cd_natureza_operacao_w > 0) then
	:new.cd_natureza_operacao		:= cd_natureza_operacao_w;
end if;

if	(nr_seq_variacao_fiscal_w > 0) then
	:new.nr_seq_variacao_fiscal		:= nr_seq_variacao_fiscal_w;
end if;

obter_produto_financeiro(:new.cd_estabelecimento,null,null,nr_seq_produto_w,null,null,nr_seq_grupo_prod_w);

if	(nvl(nr_seq_produto_w,0) > 0) then
	:new.nr_seq_produto			:= nr_seq_produto_w;
end if;

if	(inserting) then
	begin
	--Trazer dados do material
    if	(:new.cd_material IS NOT NULL) and
		(:new.cd_material > 0) then
    begin
        select
            gm.cd_grupo_material,
            sm.cd_subgrupo_material,
            cm.cd_classe_material,
            nvl(m.nr_seq_familia, 0) as nr_seq_familia
        into cd_grupo_material_w,
             cd_subgrupo_material_w,
             cd_classe_material_w,
             nr_seq_familia_w
        from material m,
            classe_material cm,
            subgrupo_material sm,
            grupo_material gm
        where m.cd_classe_material = cm.cd_classe_material
          and cm.cd_subgrupo_material = sm.cd_subgrupo_material
          and sm.cd_grupo_material = gm.cd_grupo_material
          and m.cd_material = :new.cd_material;

        if (:new.ie_tipo_programa is null) THEN
            :new.ie_tipo_programa := obter_regra_prog_saude(:new.cd_material, :new.cd_estabelecimento, cd_grupo_material_w, cd_subgrupo_material_w,cd_classe_material_w,nr_seq_familia_w);
        end if;
        -- Buscar IE_CLASSIF_RENAME pelo caso venha vazio
        if (:new.ie_classif_rename is null) THEN
            :new.ie_classif_rename := obter_regra_rename(:new.cd_material, :new.cd_estabelecimento, cd_grupo_material_w, cd_subgrupo_material_w,cd_classe_material_w,nr_seq_familia_w);
        end if;
	end;
    end if;

	if	(:new.nr_solic_compra is not null) and
		(:new.nr_item_solic_compra is not null) then

		select  max(nr_seq_orc_item_gpi)
		into	nr_seq_orc_item_gpi_w
		from	solic_compra_item
		where	nr_solic_compra		= :new.nr_solic_compra
		and	nr_item_solic_compra	= :new.nr_item_solic_compra;

		:new.nr_seq_orc_item_gpi := nr_seq_orc_item_gpi_w;
	end if;
	end;
else
	begin
	if	(:new.nr_solic_compra is not null) and
		(:new.nr_item_solic_compra is not null) and
		(:new.nr_seq_orc_item_gpi is null) and
		(:old.nr_seq_orc_item_gpi is null) then

		select  max(nr_seq_orc_item_gpi)
		into	nr_seq_orc_item_gpi_w
		from	solic_compra_item
		where	nr_solic_compra	= :new.nr_solic_compra
		and	nr_item_solic_compra = :new.nr_item_solic_compra;

		:new.nr_seq_orc_item_gpi := nr_seq_orc_item_gpi_w;
	end if;
	end;
end if;

if	(inserting) then

	select	nvl(max(a.nr_ordem_compra),0),
		max(a.ie_tipo_nota),
		max(obter_se_Nota_entrada_saida(a.nr_sequencia)),
		max(b.ie_tipo_operacao)
	into	nr_ordem_compra_w,
		ie_tipo_nota_w,
		ie_entrada_saida_w,
		ie_tipo_operacao_w
	from	nota_fiscal a,
		operacao_nota b
	where	a.cd_operacao_nf = b.cd_operacao_nf
	and	a.nr_sequencia = :new.nr_sequencia;

	select	count(*)

	into	qt_existe_w
	from	ordem_compra
	where	ie_origem_imp = 'MercadoEletronico'
	and	nr_ordem_compra = nr_ordem_compra_w;

	if	(:new.nr_solic_compra is not null) and
		(:new.nr_item_solic_compra is not null)then

		select  max(nr_seq_orc_item_gpi)
		into	nr_seq_orc_item_gpi_w
		from	solic_compra_item
		where	nr_solic_compra = :new.nr_solic_compra
		and	nr_item_solic_compra = :new.nr_item_solic_compra;

			:new.nr_seq_orc_item_gpi := nr_seq_orc_item_gpi_w;
	end if;

	if	(qt_existe_w = 0) and
		(ie_tipo_nota_w <> 'ST') and
		(ie_entrada_saida_w = 'E') and
		(ie_tipo_operacao_w = '1') then

		select	nvl(to_number(obter_cod_ext_material(:new.cd_material,:new.cd_estabelecimento)),0)
		into	cd_ext_material_w
		from dual;

		if	(cd_ext_material_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(216284,'CD_MATERIAL=' || :new.cd_material ||';'|| 'DS_MATERIAL=' || obter_desc_material(:new.cd_material));
		end if;

	end if;

end if;

end;
/


ALTER TABLE TASY.NOTA_FISCAL_ITEM ADD (
  CONSTRAINT NOTFIIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_ITEM_NF)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          10M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL_ITEM ADD (
  CONSTRAINT NOTFIIT_FARVNDI_FK 
 FOREIGN KEY (NR_SEQ_FAR_VENDA_ITEM) 
 REFERENCES TASY.FAR_VENDA_ITEM (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_CONRNFP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PROT_CONT) 
 REFERENCES TASY.CONTRATO_REGRA_NF_PROC (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT NOTFIIT_ORPROPM_FK 
 FOREIGN KEY (NR_SEQ_OPM) 
 REFERENCES TASY.ORDEM_PRODUCAO_OPM (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_MANMODE_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.MAN_MODELO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_PESJURI_FK 
 FOREIGN KEY (CD_FORNECEDOR_CONSIG) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT NOTFIIT_PESFISI_FK 
 FOREIGN KEY (CD_PACIENTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT NOTFIIT_PLSCNAE_FK 
 FOREIGN KEY (NR_SEQ_CNAE) 
 REFERENCES TASY.PLS_CNAE (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_MARCA_FK 
 FOREIGN KEY (NR_SEQ_MARCA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_GPIORIT_FK 
 FOREIGN KEY (NR_SEQ_ORC_ITEM_GPI) 
 REFERENCES TASY.GPI_ORC_ITEM (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_DISROMA_FK 
 FOREIGN KEY (NR_SEQ_ROMANEIO) 
 REFERENCES TASY.DIS_ROMANEIO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_DISROIT_FK 
 FOREIGN KEY (NR_SEQ_ROMANEIO_ITEM) 
 REFERENCES TASY.DIS_ROMANEIO_ITEM (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_CONRENF_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CONTRATO) 
 REFERENCES TASY.CONTRATO_REGRA_NF (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_SOLCOIT_FK 
 FOREIGN KEY (NR_SOLIC_COMPRA, NR_ITEM_SOLIC_COMPRA) 
 REFERENCES TASY.SOLIC_COMPRA_ITEM (NR_SOLIC_COMPRA,NR_ITEM_SOLIC_COMPRA),
  CONSTRAINT NOTFIIT_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_SUCOIDE_FK 
 FOREIGN KEY (NR_ITEM_RESP_CONSIG_DEV) 
 REFERENCES TASY.SUP_RESP_CONSIG_ITEM_DEV (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_SURECOI_FK 
 FOREIGN KEY (NR_ITEM_RESP_CONSIG) 
 REFERENCES TASY.SUP_RESP_CONSIG_ITEM (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_EMPMATE_FK 
 FOREIGN KEY (NR_EMPRESTIMO, NR_SEQ_ITEM_EMPRESTIMO) 
 REFERENCES TASY.EMPRESTIMO_MATERIAL (NR_EMPRESTIMO,NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_UNIMECO_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_COMPRA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT NOTFIIT_CONTRAT_FK 
 FOREIGN KEY (NR_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_CONFINA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT NOTFIIT_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT NOTFIIT_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT NOTFIIT_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT NOTFIIT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT NOTFIIT_MATESTO_FK 
 FOREIGN KEY (CD_MATERIAL_ESTOQUE) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT NOTFIIT_NATOPER_FK 
 FOREIGN KEY (CD_NATUREZA_OPERACAO) 
 REFERENCES TASY.NATUREZA_OPERACAO (CD_NATUREZA_OPERACAO),
  CONSTRAINT NOTFIIT_NOTFISC_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT NOTFIIT_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA),
  CONSTRAINT NOTFIIT_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT NOTFIIT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT NOTFIIT_UNIMEES_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_ESTOQUE) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT NOTFIIT_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJ_REC) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT NOTFIIT_LICITEM_FK 
 FOREIGN KEY (NR_SEQ_LIC_ITEM) 
 REFERENCES TASY.LIC_ITEM (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNEC) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_UNMACOM_FK 
 FOREIGN KEY (NR_SEQ_UNIDADE_ADIC) 
 REFERENCES TASY.UNIDADE_MEDIDA_ADIC_COMPRA (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_GPIPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_GPI) 
 REFERENCES TASY.GPI_PROJETO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_GPICRETA_FK 
 FOREIGN KEY (NR_SEQ_ETAPA_GPI) 
 REFERENCES TASY.GPI_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_GPIPLAN_FK 
 FOREIGN KEY (NR_SEQ_CONTA_GPI) 
 REFERENCES TASY.GPI_PLANO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_PRODFIN_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO) 
 REFERENCES TASY.PRODUTO_FINANCEIRO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_INSRECE_FK 
 FOREIGN KEY (NR_SEQ_INSPECAO) 
 REFERENCES TASY.INSPECAO_RECEBIMENTO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_PROCOMP_FK 
 FOREIGN KEY (NR_SEQ_APROVACAO) 
 REFERENCES TASY.PROCESSO_COMPRA (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_NOTFIIT_FK 
 FOREIGN KEY (NR_SEQ_NF_ORIG, NR_SEQ_ITEM_NF_ORIG) 
 REFERENCES TASY.NOTA_FISCAL_ITEM (NR_SEQUENCIA,NR_ITEM_NF),
  CONSTRAINT NOTFIIT_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_AGPOPME_FK 
 FOREIGN KEY (NR_SEQ_AG_PAC_OPME) 
 REFERENCES TASY.AGENDA_PAC_OPME (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_PAC) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT NOTFIIT_SITTRPS_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_TRIB) 
 REFERENCES TASY.SITUACAO_TRIB_PREST_SERV (NR_SEQUENCIA));

GRANT SELECT ON TASY.NOTA_FISCAL_ITEM TO NIVEL_1;

