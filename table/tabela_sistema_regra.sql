ALTER TABLE TASY.TABELA_SISTEMA_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TABELA_SISTEMA_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TABELA_SISTEMA_REGRA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NM_TABELA                VARCHAR2(50 BYTE)    NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4),
  CD_PERFIL                NUMBER(5),
  CD_FUNCAO                NUMBER(10),
  NM_USUARIO_REGRA         VARCHAR2(15 BYTE),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NR_SEQ_VISAO             NUMBER(10),
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_REGRA                 VARCHAR2(2 BYTE),
  DS_TITULO                VARCHAR2(60 BYTE),
  DS_SQL_ATIVACAO          VARCHAR2(2000 BYTE),
  DS_MASCARA               VARCHAR2(30 BYTE),
  DS_QTD_REGISTROS_LIMITE  VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TABSISR_ESTABEL_FK_I ON TASY.TABELA_SISTEMA_REGRA
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TABSISR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TABSISR_FUNCAO_FK_I ON TASY.TABELA_SISTEMA_REGRA
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABSISR_I2_I ON TASY.TABELA_SISTEMA_REGRA
(NM_USUARIO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TABSISR_I2_I
  MONITORING USAGE;


CREATE INDEX TASY.TABSISR_PERFIL_FK_I ON TASY.TABELA_SISTEMA_REGRA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TABSISR_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TABSISR_PK ON TASY.TABELA_SISTEMA_REGRA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABSISR_TABSIST_FK_I ON TASY.TABELA_SISTEMA_REGRA
(NM_TABELA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABSISR_TABVISA_FK_I ON TASY.TABELA_SISTEMA_REGRA
(NR_SEQ_VISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABSISR_USUARIO_FK_I ON TASY.TABELA_SISTEMA_REGRA
(NM_USUARIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TABSISR_USUARIO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.tabela_sistema_regra_atual
before insert or update ON TASY.TABELA_SISTEMA_REGRA for each row
declare

qt_records_w	number(10);

--Please, be careful
pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	select	count(*)
	into	qt_records_w
	from	tabela_sistema_regra
	where	nm_tabela		= :new.nm_tabela
	and	((cd_estabelecimento	= :new.cd_estabelecimento)	or (cd_estabelecimento is null	and :new.cd_estabelecimento is null))
	and	((cd_perfil			= :new.cd_perfil)		or (cd_perfil is null 		and :new.cd_perfil is null))
	and	((cd_funcao		= :new.cd_funcao)		or (cd_funcao is null 	and :new.cd_funcao is null))
	and	((nm_usuario_regra		= :new.nm_usuario_regra)	or (nm_usuario_regra is null 	and :new.nm_usuario_regra is null))
	and	((nr_seq_visao		= :new.nr_seq_visao)	or (nr_seq_visao is null 	and :new.nr_seq_visao is null))
	and	nr_sequencia 		<> :new.nr_sequencia;

	if (qt_records_w > 0) then

		--J� existe uma regra cadastrada com esses atributos.
		wheb_mensagem_pck.exibir_mensagem_abort(991975);

	end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TABELA_SISTEMA_REGRA_tp  after update ON TASY.TABELA_SISTEMA_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_TABELA,1,4000),substr(:new.NM_TABELA,1,4000),:new.nm_usuario,nr_seq_w,'NM_TABELA',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TITULO,1,4000),substr(:new.DS_TITULO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TITULO',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_REGRA,1,4000),substr(:new.NM_USUARIO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_REGRA',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_VISAO,1,4000),substr(:new.NR_SEQ_VISAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_VISAO',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA,1,4000),substr(:new.IE_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FUNCAO,1,4000),substr(:new.CD_FUNCAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_FUNCAO',ie_log_w,ds_w,'TABELA_SISTEMA_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TABELA_SISTEMA_REGRA ADD (
  CONSTRAINT TABSISR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TABELA_SISTEMA_REGRA ADD (
  CONSTRAINT TABSISR_TABVISA_FK 
 FOREIGN KEY (NR_SEQ_VISAO) 
 REFERENCES TASY.TABELA_VISAO (NR_SEQUENCIA),
  CONSTRAINT TABSISR_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA)
    ON DELETE CASCADE,
  CONSTRAINT TABSISR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TABSISR_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT TABSISR_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT TABSISR_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_REGRA) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.TABELA_SISTEMA_REGRA TO NIVEL_1;

