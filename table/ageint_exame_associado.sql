ALTER TABLE TASY.AGEINT_EXAME_ASSOCIADO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_EXAME_ASSOCIADO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_EXAME_ASSOCIADO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO  NUMBER(10)               NOT NULL,
  NR_SEQ_AGEINT_ITEM   NUMBER(10),
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  IE_REGRA             NUMBER(5),
  IE_GLOSA             VARCHAR2(1 BYTE),
  VL_EXAME             NUMBER(15,2),
  IE_AUTORIZACAO       VARCHAR2(3 BYTE),
  VL_EXAME_PARTICULAR  NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGIEXAS_AGEINIT_FK_I ON TASY.AGEINT_EXAME_ASSOCIADO
(NR_SEQ_AGEINT_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXAS_AGEINIT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGIEXAS_PK ON TASY.AGEINT_EXAME_ASSOCIADO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXAS_PK
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXAS_PROCEDI_FK_I ON TASY.AGEINT_EXAME_ASSOCIADO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXAS_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGIEXAS_PROINTE_FK_I ON TASY.AGEINT_EXAME_ASSOCIADO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGIEXAS_PROINTE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.AGEINT_EXAME_ASSOCIADO ADD (
  CONSTRAINT AGIEXAS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGEINT_EXAME_ASSOCIADO ADD (
  CONSTRAINT AGIEXAS_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGIEXAS_AGEINIT_FK 
 FOREIGN KEY (NR_SEQ_AGEINT_ITEM) 
 REFERENCES TASY.AGENDA_INTEGRADA_ITEM (NR_SEQUENCIA),
  CONSTRAINT AGIEXAS_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.AGEINT_EXAME_ASSOCIADO TO NIVEL_1;

