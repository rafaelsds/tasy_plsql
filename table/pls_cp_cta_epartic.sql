ALTER TABLE TASY.PLS_CP_CTA_EPARTIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_EPARTIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_EPARTIC
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_EDICAO_AMB             NUMBER(6)           NOT NULL,
  TX_AJUSTE_GERAL           NUMBER(15,4)        NOT NULL,
  NR_SEQ_CP_COMBINADA       NUMBER(10)          NOT NULL,
  VL_CH_HONORARIOS          NUMBER(15,4)        NOT NULL,
  VL_CH_CUSTO_OPER          NUMBER(15,4)        NOT NULL,
  TX_AJUSTE_CH_HONOR        NUMBER(15,4)        NOT NULL,
  VL_FILME                  NUMBER(15,4)        NOT NULL,
  TX_AJUSTE_FILME           NUMBER(15,4)        NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  TX_AJUSTE_CUSTO_OPER      NUMBER(15,4)        NOT NULL,
  TX_AJUSTE_PARTIC          NUMBER(15,4)        NOT NULL,
  DT_INICIO_VIGENCIA        DATE                NOT NULL,
  VL_PROC_NEGOCIADO         NUMBER(15,4),
  DT_FIM_VIGENCIA           DATE,
  VL_MEDICO                 NUMBER(15,4),
  VL_AUXILIARES             NUMBER(15,4),
  VL_ANESTESISTA            NUMBER(15,4),
  VL_CUSTO_OPERACIONAL      NUMBER(15,4),
  VL_MATERIAIS              NUMBER(15,4),
  IE_PRECO_INFORMADO        VARCHAR2(1 BYTE),
  CD_MOEDA_CH_MEDICO        NUMBER(3),
  CD_MOEDA_CH_CO            NUMBER(3),
  DS_OBSERVACAO             VARCHAR2(4000 BYTE),
  CD_MOEDA_FILME            NUMBER(3),
  CD_MOEDA_ANESTESISTA      NUMBER(3),
  NR_SEQ_CBHPM_EDICAO       NUMBER(10),
  IE_AUTOGERADO             VARCHAR2(1 BYTE),
  IE_NAO_GERA_TX_INTER      VARCHAR2(1 BYTE),
  NR_SEQ_EDICAO_TUSS        NUMBER(10),
  IE_CH_PADRAO_ANESTESISTA  VARCHAR2(1 BYTE),
  DT_INICIO_VIGENCIA_REF    DATE,
  DT_FIM_VIGENCIA_REF       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLCPCPA_EDIAMB_FK_I ON TASY.PLS_CP_CTA_EPARTIC
(CD_EDICAO_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCPA_MOEDA_FK_I ON TASY.PLS_CP_CTA_EPARTIC
(CD_MOEDA_CH_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCPA_MOEDA_FK2_I ON TASY.PLS_CP_CTA_EPARTIC
(CD_MOEDA_CH_CO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCPA_MOEDA_FK3_I ON TASY.PLS_CP_CTA_EPARTIC
(CD_MOEDA_FILME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCPA_MOEDA_FK4_I ON TASY.PLS_CP_CTA_EPARTIC
(CD_MOEDA_ANESTESISTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLCPCPA_PK ON TASY.PLS_CP_CTA_EPARTIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCPA_PLSCPCC_FK_I ON TASY.PLS_CP_CTA_EPARTIC
(NR_SEQ_CP_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_cp_cta_epartic_atual
before insert or update ON TASY.PLS_CP_CTA_EPARTIC for each row
declare

begin
-- previnir que seja gravada a hora junto na regra.
-- essa situa��o ocorre quando o usu�rio seleciona a data no campo via enter
if	(:new.dt_inicio_vigencia is not null) then
	:new.dt_inicio_vigencia := trunc(:new.dt_inicio_vigencia);
end if;
if	(:new.dt_fim_vigencia is not null) then
	:new.dt_fim_vigencia := trunc(:new.dt_fim_vigencia);
end if;

-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_cp_cta_epartic_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_CP_CTA_EPARTIC_tp  after update ON TASY.PLS_CP_CTA_EPARTIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_MOEDA_ANESTESISTA,1,4000),substr(:new.CD_MOEDA_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA_ANESTESISTA',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CUSTO_OPERACIONAL,1,4000),substr(:new.VL_CUSTO_OPERACIONAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_CUSTO_OPERACIONAL',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA_CH_CO,1,4000),substr(:new.CD_MOEDA_CH_CO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA_CH_CO',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CH_HONORARIOS,1,4000),substr(:new.VL_CH_HONORARIOS,1,4000),:new.nm_usuario,nr_seq_w,'VL_CH_HONORARIOS',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FILME,1,4000),substr(:new.VL_FILME,1,4000),:new.nm_usuario,nr_seq_w,'VL_FILME',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MATERIAIS,1,4000),substr(:new.VL_MATERIAIS,1,4000),:new.nm_usuario,nr_seq_w,'VL_MATERIAIS',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA_CH_MEDICO,1,4000),substr(:new.CD_MOEDA_CH_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA_CH_MEDICO',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOEDA_FILME,1,4000),substr(:new.CD_MOEDA_FILME,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOEDA_FILME',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA_REF,1,4000),substr(:new.DT_FIM_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA_REF,1,4000),substr(:new.DT_INICIO_VIGENCIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA_REF',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTOGERADO,1,4000),substr(:new.IE_AUTOGERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTOGERADO',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CH_PADRAO_ANESTESISTA,1,4000),substr(:new.IE_CH_PADRAO_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CH_PADRAO_ANESTESISTA',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NAO_GERA_TX_INTER,1,4000),substr(:new.IE_NAO_GERA_TX_INTER,1,4000),:new.nm_usuario,nr_seq_w,'IE_NAO_GERA_TX_INTER',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PARTIC,1,4000),substr(:new.TX_AJUSTE_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PARTIC',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ANESTESISTA,1,4000),substr(:new.VL_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_ANESTESISTA',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_AUXILIARES,1,4000),substr(:new.VL_AUXILIARES,1,4000),:new.nm_usuario,nr_seq_w,'VL_AUXILIARES',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CH_CUSTO_OPER,1,4000),substr(:new.VL_CH_CUSTO_OPER,1,4000),:new.nm_usuario,nr_seq_w,'VL_CH_CUSTO_OPER',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CP_COMBINADA,1,4000),substr(:new.NR_SEQ_CP_COMBINADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_COMBINADA',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MEDICO,1,4000),substr(:new.VL_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MEDICO',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PROC_NEGOCIADO,1,4000),substr(:new.VL_PROC_NEGOCIADO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PROC_NEGOCIADO',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO_INFORMADO,1,4000),substr(:new.IE_PRECO_INFORMADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO_INFORMADO',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CBHPM_EDICAO,1,4000),substr(:new.NR_SEQ_CBHPM_EDICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CBHPM_EDICAO',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_EDICAO_TUSS,1,4000),substr(:new.NR_SEQ_EDICAO_TUSS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EDICAO_TUSS',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_CH_HONOR,1,4000),substr(:new.TX_AJUSTE_CH_HONOR,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_CH_HONOR',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_CUSTO_OPER,1,4000),substr(:new.TX_AJUSTE_CUSTO_OPER,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_CUSTO_OPER',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_FILME,1,4000),substr(:new.TX_AJUSTE_FILME,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_FILME',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_GERAL,1,4000),substr(:new.TX_AJUSTE_GERAL,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_GERAL',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EDICAO_AMB,1,4000),substr(:new.CD_EDICAO_AMB,1,4000),:new.nm_usuario,nr_seq_w,'CD_EDICAO_AMB',ie_log_w,ds_w,'PLS_CP_CTA_EPARTIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CP_CTA_EPARTIC ADD (
  CONSTRAINT PLCPCPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_EPARTIC ADD (
  CONSTRAINT PLCPCPA_EDIAMB_FK 
 FOREIGN KEY (CD_EDICAO_AMB) 
 REFERENCES TASY.EDICAO_AMB (CD_EDICAO_AMB),
  CONSTRAINT PLCPCPA_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA_CH_MEDICO) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLCPCPA_MOEDA_FK2 
 FOREIGN KEY (CD_MOEDA_CH_CO) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLCPCPA_MOEDA_FK3 
 FOREIGN KEY (CD_MOEDA_FILME) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLCPCPA_MOEDA_FK4 
 FOREIGN KEY (CD_MOEDA_ANESTESISTA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT PLCPCPA_PLSCPCC_FK 
 FOREIGN KEY (NR_SEQ_CP_COMBINADA) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CP_CTA_EPARTIC TO NIVEL_1;

