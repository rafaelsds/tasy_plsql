ALTER TABLE TASY.UNIDADE_ATEND_CAMP_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.UNIDADE_ATEND_CAMP_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.UNIDADE_ATEND_CAMP_PERFIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_UNIDADE       NUMBER(10)               NOT NULL,
  CD_PERFIL            NUMBER(5)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.UNIATCAMPE_PERFIL_FK_I ON TASY.UNIDADE_ATEND_CAMP_PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.UNIATCAMPE_PK ON TASY.UNIDADE_ATEND_CAMP_PERFIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATCAMPE_UNIDATCAMP_FK_I ON TASY.UNIDADE_ATEND_CAMP_PERFIL
(NR_SEQ_UNIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.UNIDADE_ATEND_CAMP_PERFIL ADD (
  CONSTRAINT UNIATCAMPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.UNIDADE_ATEND_CAMP_PERFIL ADD (
  CONSTRAINT UNIATCAMPE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT UNIATCAMPE_UNIDATCAMP_FK 
 FOREIGN KEY (NR_SEQ_UNIDADE) 
 REFERENCES TASY.UNIDADE_ATEND_CAMPANHA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.UNIDADE_ATEND_CAMP_PERFIL TO NIVEL_1;

