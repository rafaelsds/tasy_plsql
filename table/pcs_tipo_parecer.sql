ALTER TABLE TASY.PCS_TIPO_PARECER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PCS_TIPO_PARECER CASCADE CONSTRAINTS;

CREATE TABLE TASY.PCS_TIPO_PARECER
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TIPO_PARECER      VARCHAR2(255 BYTE)       NOT NULL,
  QT_TEMPO_RESPOSTA    NUMBER(10),
  HR_RESPOSTA          DATE,
  QT_MIN_ANTECEDENCIA  NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PCSTPPA_PK ON TASY.PCS_TIPO_PARECER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSTPPA_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PCS_TIPO_PARECER_tp  after update ON TASY.PCS_TIPO_PARECER FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_TIPO_PARECER,1,4000),substr(:new.DS_TIPO_PARECER,1,4000),:new.nm_usuario,nr_seq_w,'DS_TIPO_PARECER',ie_log_w,ds_w,'PCS_TIPO_PARECER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TEMPO_RESPOSTA,1,4000),substr(:new.QT_TEMPO_RESPOSTA,1,4000),:new.nm_usuario,nr_seq_w,'QT_TEMPO_RESPOSTA',ie_log_w,ds_w,'PCS_TIPO_PARECER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PCS_TIPO_PARECER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_ANTECEDENCIA,1,4000),substr(:new.QT_MIN_ANTECEDENCIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_ANTECEDENCIA',ie_log_w,ds_w,'PCS_TIPO_PARECER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.HR_RESPOSTA,1,4000),substr(:new.HR_RESPOSTA,1,4000),:new.nm_usuario,nr_seq_w,'HR_RESPOSTA',ie_log_w,ds_w,'PCS_TIPO_PARECER',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PCS_TIPO_PARECER ADD (
  CONSTRAINT PCSTPPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PCS_TIPO_PARECER TO NIVEL_1;

