ALTER TABLE TASY.TITULO_RECEBER_LIQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_RECEBER_LIQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_RECEBER_LIQ
(
  NR_TITULO                   NUMBER(10)        NOT NULL,
  NR_SEQUENCIA                NUMBER(5),
  DT_RECEBIMENTO              DATE              NOT NULL,
  VL_RECEBIDO                 NUMBER(15,2)      NOT NULL,
  VL_DESCONTOS                NUMBER(15,2)      NOT NULL,
  VL_JUROS                    NUMBER(15,2)      NOT NULL,
  VL_MULTA                    NUMBER(15,2)      NOT NULL,
  CD_MOEDA                    NUMBER(5),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  CD_TIPO_RECEBIMENTO         NUMBER(5)         NOT NULL,
  IE_ACAO                     VARCHAR2(1 BYTE)  NOT NULL,
  CD_SERIE_NF_DEVOL           VARCHAR2(5 BYTE),
  NR_NOTA_FISCAL_DEVOL        NUMBER(10),
  CD_BANCO                    NUMBER(5),
  CD_AGENCIA_BANCARIA         VARCHAR2(8 BYTE),
  NR_DOCUMENTO                VARCHAR2(22 BYTE),
  NR_LOTE_BANCO               VARCHAR2(20 BYTE),
  CD_CGC_EMP_CRED             VARCHAR2(14 BYTE),
  NR_CARTAO_CRED              VARCHAR2(20 BYTE),
  NR_ADIANTAMENTO             NUMBER(10),
  NR_LOTE_CONTABIL            NUMBER(10),
  NR_SEQ_TRANS_FIN            NUMBER(10),
  VL_REC_MAIOR                NUMBER(15,2)      NOT NULL,
  VL_GLOSA                    NUMBER(15,2)      NOT NULL,
  NR_SEQ_RETORNO              NUMBER(10),
  VL_ADEQUADO                 NUMBER(15,2),
  NR_SEQ_RET_ITEM             NUMBER(10),
  NR_SEQ_CONTA_BANCO          NUMBER(10),
  VL_DESPESA_BANCARIA         NUMBER(15,2),
  DS_OBSERVACAO               VARCHAR2(255 BYTE),
  NR_SEQ_CAIXA_REC            NUMBER(10),
  IE_LIB_CAIXA                VARCHAR2(1 BYTE)  NOT NULL,
  NR_SEQ_TRANS_CAIXA          NUMBER(10),
  CD_CENTRO_CUSTO_DESC        NUMBER(8),
  NR_SEQ_MOTIVO_DESC          NUMBER(10),
  NR_BORDERO                  NUMBER(10),
  NR_SEQ_MOVTO_TRANS_FIN      NUMBER(10),
  NR_SEQ_COBRANCA             NUMBER(10),
  DT_INTEGRACAO_EXTERNA       DATE,
  VL_PERDAS                   NUMBER(15,2),
  DT_AUTENTICACAO             DATE,
  VL_OUTROS_ACRESCIMOS        NUMBER(15,2),
  NR_EXTERNO                  VARCHAR2(255 BYTE),
  NR_SEQ_CONPACI_RET_HIST     NUMBER(10),
  NR_REPASSE_TERCEIRO         NUMBER(10),
  NR_TIT_PAGAR                NUMBER(10),
  NR_SEQ_BAIXA_PAGAR          NUMBER(10),
  VL_NOTA_CREDITO             NUMBER(15,2),
  NR_SEQ_LOTE_HIST_GUIA       NUMBER(10),
  NR_SEQ_AMORTIZACAO          NUMBER(10),
  NR_SEQ_NEGOCIACAO_CR        NUMBER(10),
  NR_LOTE_CONTAB_ANTECIP      NUMBER(10),
  DT_ANTECIPACAO_PLS_MENS     DATE,
  NR_LOTE_CONTAB_PRO_RATA     NUMBER(10),
  DT_REFERENCIA_PLS_MENS      DATE,
  VL_CAMBIAL_ATIVO            NUMBER(15,2),
  VL_CAMBIAL_PASSIVO          NUMBER(15,2),
  CD_CENTRO_CUSTO             NUMBER(8),
  NR_SEQ_LOTE_ENC_CONTAS      NUMBER(10),
  NR_SEQ_PLS_LOTE_CAMARA      NUMBER(10),
  NR_SEQ_LIQ_AMORTIZACAO      NUMBER(5),
  NR_SEQ_PAG_ITEM             NUMBER(10),
  NR_SEQ_LIQ_ORIGEM           NUMBER(10),
  NR_SEQ_PLS_LOTE_COB_TERC    NUMBER(10),
  NR_SEQ_PLS_LOTE_CONTEST     NUMBER(10),
  NR_SEQ_PLS_LOTE_DISC        NUMBER(10),
  VL_GLOSA_ATO_COOP_PRINC     NUMBER(15,2),
  VL_GLOSA_ATO_COOP_AUX       NUMBER(15,2),
  VL_GLOSA_ATO_NAO_COOP       NUMBER(15,2),
  NR_SEQ_DEPOSITO_IDENT       NUMBER(10),
  VL_RECURSO                  NUMBER(15,2),
  IE_TIPO_PERDA               VARCHAR2(1 BYTE),
  NR_SEQ_MOT_GLOSA            NUMBER(10),
  NR_SEQ_SEGURADO             NUMBER(10),
  IE_ORIGEM_BAIXA             VARCHAR2(15 BYTE),
  NR_SEQ_NOTA_FISCAL_PARC     NUMBER(10),
  VL_RECEBIDO_ESTRANG         NUMBER(15,2),
  VL_COTACAO                  NUMBER(21,10),
  VL_COMPLEMENTO              NUMBER(15,2),
  DT_CREDITO_BANCO            DATE,
  IE_APROPRIAR_JUROS_MULTA    VARCHAR2(1 BYTE),
  DT_REAL_RECEBIMENTO         DATE,
  NR_SEQ_REEMBOLSO            NUMBER(10),
  NR_EXTERNO_AUX              VARCHAR2(255 BYTE),
  NR_SEQ_RESC_FIN             NUMBER(10),
  IE_GERAR_BAIXA_TRIB         VARCHAR2(1 BYTE),
  VL_JUROS_APROP              NUMBER(15,2),
  VL_MULTA_APROP              NUMBER(15,2),
  VL_CORRECAO_MONETARIA       NUMBER(15,2),
  NR_SEQ_CARTA                NUMBER(10),
  IE_BAIXA_RESCISAO_CONTRATO  VARCHAR2(1 BYTE),
  NR_CODIGO_CONTROLE          VARCHAR2(15 BYTE),
  DS_STACK                    VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIRELIQ_BANESTA_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_BANESTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_BORRECE_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_BORDERO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          200K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_BORRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_CAIREC_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_CAIXA_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_CENCUST_FK_I ON TASY.TITULO_RECEBER_LIQ
(CD_CENTRO_CUSTO_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_CENCUST_FK2_I ON TASY.TITULO_RECEBER_LIQ
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          160K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_CENCUST_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_COBESCR_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_COBESCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_CONREIT_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_RET_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_CONRETO_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_COPAREHI_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_CONPACI_RET_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          200K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_COPAREHI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_CRTCOMP_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_CARTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_DEPIDEN_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_DEPOSITO_IDENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_DEPIDEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_I1 ON TASY.TITULO_RECEBER_LIQ
(DT_RECEBIMENTO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_I2 ON TASY.TITULO_RECEBER_LIQ
(NR_LOTE_CONTAB_PRO_RATA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_I3 ON TASY.TITULO_RECEBER_LIQ
(NR_LOTE_CONTAB_ANTECIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_I4 ON TASY.TITULO_RECEBER_LIQ
(NR_TIT_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_LOTAUDGUI_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_LOTE_HIST_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          200K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_LOTENCO_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_LOTE_ENC_CONTAS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          200K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_LOTENCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_MOGLOSA_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_MOT_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_MOGLOSA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_MOTDESC_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_MOTIVO_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_MOTDESC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_MOVTRFI_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_MOVTO_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_NEGOCCR_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_NEGOCIACAO_CR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          200K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_NEGOCCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_NOTFISC_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_NOTA_FISCAL_PARC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_PLSLCTE_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_PLS_LOTE_COB_TERC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_PLSLCTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_PLSLOCC_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_PLS_LOTE_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          200K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_PLSLOCC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_PLSLOCO_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_PLS_LOTE_CONTEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_PLSLOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_PLSLODI_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_PLS_LOTE_DISC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_PLSLODI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_PLSPAGAMO_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_AMORTIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          200K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_PLSPAIT_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_PAG_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_PLSPAIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_PLSPRCO_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_REEMBOLSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_PLSSEGU_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_PLSSRCF_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_RESC_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRELIQ_REPTERC_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_REPASSE_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          200K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_REPTERC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_TRAFINA_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_TRAFINA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRELIQ_TRAFINA_FK2_I ON TASY.TITULO_RECEBER_LIQ
(NR_SEQ_TRANS_CAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRELIQ_TRAFINA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELI_ADIANTA_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRELI_AGEBANC_FK_I ON TASY.TITULO_RECEBER_LIQ
(CD_BANCO, CD_AGENCIA_BANCARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITRELI_AGEBANC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITRELI_LOTCONT_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TITRELI_PK ON TASY.TITULO_RECEBER_LIQ
(NR_TITULO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRELI_TIPRECE_FK_I ON TASY.TITULO_RECEBER_LIQ
(CD_TIPO_RECEBIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRELI_TITRECE_FK_I ON TASY.TITULO_RECEBER_LIQ
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.titulo_receber_liq_insert
after insert ON TASY.TITULO_RECEBER_LIQ for each row
declare

nr_interno_conta_w              number(10);
ie_tipo_consistencia_w          number(5,0);
cd_convenio_w                   number(10);
cd_estabelecimento_w            number(10);
vl_saldo_titulo_w               number(15,2);
ie_tipo_convenio_w              number(2);
ie_lib_repasse_w                varchar2(100);
nr_seq_protocolo_w              number(10);
vl_proporcional_w               number(15,2);
vl_conta_w                      number(15,2);
vl_protocolo_w                  number(15,2);
vl_titulo_w                     number(15,2);
vl_conta_tit_w                  number(15,2);
ie_liberar_repasse_w            varchar2(10);
ie_lib_rep_tit_cheque_w         varchar2(1);
ie_controle_perdas_w            varchar2(1);

cd_pessoa_fisica_w              varchar2(10);
cd_cgc_w                        varchar2(14);
nr_seq_tipo_baixa_w             number(10);
nr_seq_perda_w                  number(10);
vl_movimento_w                  ctb_documento.vl_movimento%type;
nr_multiplicador_w              number(1);
ie_contab_cr_w                  parametro_tesouraria.ie_contab_cr%type;
dt_movimento_w                  ctb_documento.dt_competencia%type;
nr_seq_trans_financ_w           ctb_documento.nr_seq_trans_financ%type;
nr_seq_info_w                   ctb_documento.nr_seq_info%type;
nr_documento_w                  ctb_documento.nr_documento%type;
nr_seq_doc_compl_w              ctb_documento.nr_seq_doc_compl%type;
nm_tabela_w                     ctb_documento.nm_tabela%type;
ie_contab_cr_no_cb_w            parametro_controle_banc.ie_contab_cr_no_cb%type;
ie_contab_rec_classif_w         parametro_contas_receber.ie_contab_rec_classif%type;
ie_cc_glosa_w                   parametro_contas_receber.ie_cc_glosa%type;
ie_dt_contab_cr_w               parametro_contas_receber.ie_dt_contab_cr%type;
ie_dt_contab_glosa_w            parametro_contas_receber.ie_dt_contab_glosa%type;
ie_contab_tit_cancelado_w       parametro_contas_receber.ie_contab_tit_cancelado%type;
nr_seq_mensalidade_w            titulo_receber.nr_seq_mensalidade%type;
nr_doc_analitico_w              ctb_documento.nr_doc_analitico%type;
ie_entrada_confirmada_w         titulo_receber.ie_entrada_confirmada%type;
nr_seq_conta_banco_w            titulo_receber.nr_seq_conta_banco%type;
vl_desc_previsto_w              titulo_receber.vl_desc_previsto%type;
ie_tipo_ocorrencia_w            number(10);
cd_banco_w                      banco_estabelecimento.cd_banco%type;
cd_agencia_bancaria_w           banco_estabelecimento.cd_agencia_bancaria%type;
nr_conta_w                      banco_estabelecimento.cd_conta%type;
ie_digito_conta_w               banco_estabelecimento.ie_digito_conta%type;
cd_camara_compensacao_w         pessoa_fisica_conta.cd_camara_compensacao%type;
ie_retorno_w                    number(10);
nr_seq_motivo_desc_w            titulo_receber_liq_desc.nr_seq_motivo_desc%type;
cd_centro_custo_desc_w          titulo_receber_liq_desc.cd_centro_custo%type;
ie_desc_previsto_w              varchar2(1);
ie_juros_multa_w                varchar2(1);
vl_saldo_tit_w                  titulo_receber.vl_saldo_titulo%type;

ie_origem_titulo_w              titulo_receber.ie_origem_titulo%type;
ie_situacao_w                   titulo_receber.ie_situacao%type;
dt_liquidacao_w                 titulo_receber.dt_liquidacao%type;
dt_pagamento_previsto_w         titulo_receber.dt_pagamento_previsto%type;
nr_seq_ptu_fatura_w             ptu_fatura.nr_sequencia%type;
ie_gerar_a510_baixa_tit_w       pls_parametros.ie_gerar_a510_baixa_tit%type := 'N';

reg_integracao_p                gerar_int_padrao.reg_integracao;
qt_reg_w                         number(10);
ie_passivo_saldo_tit_w          parametro_contas_receber.ie_passivo_saldo_tit%type;
vl_cambial_passivo_w            titulo_receber_liq.vl_cambial_passivo%type;
ie_situacao_ctb_w				varchar2(1) := 'P';
ie_lib_adiantamento_w		parametro_repasse.ie_lib_adiantamento%type;
nr_seq_caixa_rec_w		caixa_receb.nr_sequencia%type;
vl_cartao_w			number(15,2);
vl_cheques_w			number(15,2);
vl_recebido_w			number(15,2);
ie_concil_contab_w              pls_visible_false.ie_concil_contab%type;
vl_perdas_w			number(15,2);

ie_lib_repasse_tit_conv_w	convenio_estabelecimento.ie_lib_repasse_tit_conv%type;

cursor c01 is
select  b.nr_interno_conta,
        b.vl_conta
from    conta_paciente b,
        titulo_receber a
where   a.nr_seq_protocolo      = b.nr_seq_protocolo
and     a.nr_titulo = :new.nr_titulo;

cursor c02 is
select  nr_sequencia
from    perda_contas_receber
where   nr_titulo = :new.nr_titulo
and     nr_seq_baixa = :new.nr_seq_liq_origem;

cursor c03 is
select  cd_banco,
        cd_agencia_bancaria,
        nr_conta,
        nr_digito_conta,
        cd_camara_compensacao
from    pessoa_fisica_conta
where   cd_pessoa_fisica        = cd_pessoa_fisica_w
union
select  cd_banco,
        cd_agencia_bancaria,
        nr_conta,
        nr_digito_conta,
        cd_camara_compensacao
from    pessoa_juridica_conta
where   cd_cgc  = cd_cgc_w;

cursor c04 is
select  a.nm_atributo,
        18 cd_tipo_lote_contab
from    atributo_contab a
where   ((a.cd_tipo_lote_contab = 18
        and     a.nm_atributo in (      'VL_DESCONTOS', 'VL_JUROS',
                                        'VL_MULTA', 'VL_DESPESA_BANCARIA', 'VL_RECEBIDO_CB'))
or ((a.cd_tipo_lote_contab = 5
        and     a.nm_atributo in (      'VL_GLOSA', 'VL_REC_MAIOR', 'VL_ORIGINAL_BAIXA',
                                        'VL_CAMBIAL_ATIVO', 'VL_CAMBIAL_PASSIVO'))))
union all
select  a.nm_atributo,
        37 cd_tipo_lote_contab
from    atributo_contab a
where   a.cd_tipo_lote_contab = 37
and     a.nm_atributo in ('VL_RECEBIDO',  'VL_REC_GLOSA', 'VL_REC_MAIOR')
and     nvl(:new.nr_seq_pls_lote_camara,0) = 0
and	nvl(:new.nr_seq_lote_enc_contas,0) = 0
and     ie_concil_contab_w = 'S'
and     exists  (select 1
                 from   titulo_receber x
                 where	x.nr_titulo = :new.nr_titulo
                 and	x.ie_origem_titulo	= '11'
                 and	((x.nr_seq_pls_lote_contest is not null)
                 or	(x.nr_seq_pls_lote_disc is not null)))
union all
select  a.nm_atributo,
        39 cd_tipo_lote_contab
from    atributo_contab a
where   ((a.cd_tipo_lote_contab = 39
and     a.nm_atributo in (      'VL_RECEBIDO',  'VL_DESCONTOS', 'VL_JUROS',
                                        'VL_MULTA', 'VL_DESPESA_BANCARIA', 'VL_REC_MAIOR',
                                        'VL_PERDAS', 'VL_GLOSA'))
or      ((a.cd_tipo_lote_contab = 5
and     a.nm_atributo in (       'VL_CAMBIAL_ATIVO', 'VL_CAMBIAL_PASSIVO'))))
and     :new.nr_seq_lote_enc_contas is null
and     :new.nr_seq_pls_lote_camara is null
and	nvl(:new.ie_lib_caixa, 'S') = 'S'
and     ie_concil_contab_w = 'S'
and	exists	(select	1
                from	titulo_receber x
                where	x.nr_titulo	= :new.nr_titulo
                and	x.ie_pls	= 'S'
                and	x.ie_origem_titulo = '3'
                and	((x.ie_situacao	<> '3' or ie_contab_tit_cancelado_w = 'S')
		or 	(x.ie_situacao = '3' and :new.vl_rec_maior <> 0)))
union all
select  a.nm_atributo,
        a.cd_tipo_lote_contab
from    atributo_contab a
where   a.cd_tipo_lote_contab = 5
and     a.nm_atributo in (	'VL_CAMBIAL_ATIVO', 'VL_CAMBIAL_PASSIVO', 'VL_DESCONTOS', 'VL_DESPESA_BANCARIA', 'VL_GLOSA',
							'VL_JUROS', 'VL_MOEDA_COMPLEMENTAR', 'VL_MULTA', 'VL_OUTROS_ACRESCIMOS', 'VL_PERDAS',
							'VL_RECEBIDO', 'VL_RECEBIDO_ESTRANG', 'VL_RECEBIDO_TOTAL', 'VL_RECURSADO', 'VL_REC_MAIOR');

c04_w           c04%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select  max(b.ie_tipo_consistencia),
        nvl(max(b.ie_liberar_repasse),'S')
into    ie_tipo_consistencia_w,
        ie_liberar_repasse_w
from    tipo_recebimento b
where   b.cd_tipo_recebimento = :new.cd_tipo_recebimento;

select  cd_estabelecimento,
        cd_pessoa_fisica,
        cd_cgc,
        ie_origem_titulo,
        ie_situacao,
        dt_liquidacao,
	dt_pagamento_previsto
into    cd_estabelecimento_w,
        cd_pessoa_fisica_w,
        cd_cgc_w,
        ie_origem_titulo_w,
        ie_situacao_w,
        dt_liquidacao_w,
	dt_pagamento_previsto_w
from    titulo_receber
where   nr_titulo = :new.nr_titulo;

begin
select	nvl(max(ie_concil_contab), 'N')
into	ie_concil_contab_w
from	pls_visible_false
where	cd_estabelecimento = cd_estabelecimento_w;
exception when others then
	ie_concil_contab_w := 'N';
end;

begin
select  nvl(max(ie_controle_perdas),'N'),
        nvl(max(ie_contab_rec_classif),'N'),
        nvl(max(ie_cc_glosa),'N'),
        nvl(max(ie_dt_contab_cr),'L'),
        nvl(max(ie_dt_contab_glosa),'R'),
        nvl(max(ie_contab_tit_cancelado), 'S')
into    ie_controle_perdas_w,
        ie_contab_rec_classif_w,
        ie_cc_glosa_w,
        ie_dt_contab_cr_w,
        ie_dt_contab_glosa_w,
        ie_contab_tit_cancelado_w
from    parametro_contas_receber
where   cd_estabelecimento = cd_estabelecimento_w;
exception when others then
		ie_controle_perdas_w := 'N';
		ie_contab_rec_classif_w := 'N';
		ie_cc_glosa_w:= 'N';
		ie_dt_contab_cr_w:= 'L';
		ie_dt_contab_glosa_w:= 'R';
end;

begin
select  x.*
into    ie_contab_rec_classif_w,
		ie_cc_glosa_w,
		ie_dt_contab_cr_w,
		ie_dt_contab_glosa_w
from    (
        select  nvl(a.ie_contab_rec_classif, 'N'),
				nvl(a.ie_cc_glosa, 'N'),
				nvl(a.ie_dt_contab_cr, 'L'),
				nvl(a.ie_dt_contab_glosa, 'R')
        from    ctb_param_lote_contas_rec a
        where   a.cd_empresa    = obter_empresa_estab(cd_estabelecimento_w)
        and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w
		and		ie_ctb_online = 'S'
        order   by nvl(a.cd_estab_exclusivo, 0) desc
        ) x
where   rownum = 1;
exception when others then
	null;
end;

if      (ie_controle_perdas_w = 'S') and
        (ie_tipo_consistencia_w = 9) then
        begin
        open c02;
        loop
        fetch c02 into
                nr_seq_perda_w;
        exit when c02%notfound;
                fin_cancelar_perda(nr_seq_perda_w, :new.nm_usuario);
        end loop;
        close c02;
        exception
        when others then
                null;
        end;
end if;

if      (:new.nr_seq_caixa_rec is null) then -- francisco 18/0|1/2007 so pode atualizar quando fecha o recebimento
        atualizar_imposto_tit_rec(:new.nr_titulo, :new.nr_sequencia, :new.vl_recebido + :new.vl_glosa, :new.nm_usuario, :new.nr_seq_liq_origem);

        select  nvl(nr_interno_conta,0),
                vl_saldo_titulo - nvl(:new.vl_recebido,0) - nvl(:new.vl_perdas,0) - nvl(:new.vl_descontos,0) - nvl(:new.vl_glosa,0) /*bruna,chico 21/11/2007 atualizar o saldo do titulo*/,
                cd_estabelecimento,
                nr_seq_protocolo,
                vl_titulo
        into    nr_interno_conta_w,
                vl_saldo_titulo_w,
                cd_estabelecimento_w,
                nr_seq_protocolo_w,
                vl_titulo_w
        from    titulo_receber
        where   nr_titulo = :new.nr_titulo;

        select  nvl(max(a.ie_lib_rep_tit_cheque),'N')
        into    ie_lib_rep_tit_cheque_w
        from    parametro_faturamento a
        where   a.cd_estabelecimento    = cd_estabelecimento_w;

	begin
	select	nvl(ie_lib_adiantamento,'N')
	into	ie_lib_adiantamento_w
	from	parametro_repasse
	where	cd_estabelecimento = cd_estabelecimento_w;
	exception
	when others then
		ie_lib_adiantamento_w	:= 'N';
	end;

        if      (nr_interno_conta_w > 0) then

                if      (ie_tipo_consistencia_w <> 3) or        -- so atualizar o repasse se a baixa nao for cheque
                        (ie_lib_rep_tit_cheque_w = 'S') then    /* ahoffelder - os 429968 - 23/05/2012 */

                        vl_conta_tit_w  := 0;

                        select  b.ie_tipo_convenio,
                                b.cd_convenio,
                                a.vl_conta
                        into    ie_tipo_convenio_w,
                                cd_convenio_w,
                                vl_conta_tit_w
                        from    convenio b,
                                conta_paciente a
                        where   a.cd_convenio_parametro = b.cd_convenio
                        and     a.nr_interno_conta      = nr_interno_conta_w;

                        select  nvl(max(ie_lib_repasse),'S'),
								nvl(max(ie_lib_repasse_tit_conv), 'N')
						into    ie_lib_repasse_w,
								ie_lib_repasse_tit_conv_w
						from    convenio_estabelecimento
						where   cd_convenio             = cd_convenio_w
						and     cd_estabelecimento      = cd_estabelecimento_w;

			vl_cartao_w	:= 0;
			vl_cheques_w	:= 0;
			vl_recebido_w	:= :new.vl_recebido;

			if	(:new.nr_adiantamento is not null) then
				select	max(a.nr_seq_caixa_rec)
				into	nr_seq_caixa_rec_w
				from	adiantamento a
				where	a.nr_adiantamento = :new.nr_adiantamento;

				select	nvl(sum(vl_transacao),0)
				into	vl_cartao_w
				from	movto_cartao_cr
				where	nr_seq_caixa_rec	= nr_seq_caixa_rec_w
				and	dt_cancelamento is null
				and	(dt_confirmacao_tef is not null or dt_integracao_tef is null);

				select	nvl(sum(vl_cheque),0)
				into	vl_cheques_w
				from	cheque_cr
				where	nr_seq_caixa_rec	= nr_seq_caixa_rec_w;

				select	nvl(sum(vl_especie),0)
				into	vl_recebido_w
				from	caixa_receb
				where	nr_sequencia = nr_seq_caixa_rec_w;

			end if;

                if  (:new.nr_seq_trans_caixa is null) and
                    ((ie_lib_adiantamento_w = 'N') or
					((ie_lib_adiantamento_w = 'S') and
                    (((nvl(vl_cartao_w,0) = 0) and (nvl(vl_cheques_w,0) = 0)) or
					(vl_recebido_w <> 0)))) then

                    if  (ie_lib_adiantamento_w = 'N') and
                        (vl_recebido_w = 0) then
                        vl_recebido_w  := :new.vl_recebido;
                    end if;

				select decode(nvl(:new.vl_perdas,0),0,:new.vl_recebido,:new.vl_perdas) into vl_perdas_w from dual;

                                if      (ie_liberar_repasse_w in ('S','P')) and
										(((ie_tipo_convenio_w = 1 or ie_lib_repasse_tit_conv_w = 'S') and (ie_lib_repasse_w = 'S')) or
										((ie_tipo_convenio_w = 4 or ie_lib_repasse_tit_conv_w = 'S') and (ie_lib_repasse_w = 'F'))) then

                                        if      (ie_liberar_repasse_w = 'P') then
                                                gerar_perdas_repasse(nr_interno_conta_w, :new.nm_usuario, 'B', vl_titulo_w, vl_perdas_w, vl_saldo_titulo_w);
                                        else
                                                atualizar_repasse_conta(nr_interno_conta_w, vl_saldo_titulo_w, :new.nm_usuario);
                                        end if;
                                elsif   (ie_liberar_repasse_w in ('S','P')) and
										(((ie_tipo_convenio_w = 1 or ie_lib_repasse_tit_conv_w = 'S') and (ie_lib_repasse_w = 'SP')) or
										((ie_tipo_convenio_w = 4 or ie_lib_repasse_tit_conv_w = 'S') and (ie_lib_repasse_w = 'FP'))) then

                                        if      (ie_liberar_repasse_w = 'P') then
                                                gerar_perdas_repasse(nr_interno_conta_w, :new.nm_usuario, 'B', vl_titulo_w, vl_perdas_w, vl_saldo_titulo_w);
                                        else
                                                if      (nvl(vl_conta_tit_w, 0) > 0) then
                                                        atualizar_repasse_conta_prop(nr_interno_conta_w, vl_recebido_w, vl_conta_tit_w, :new.nm_usuario);
                                                else
                                                        atualizar_repasse_conta_prop(nr_interno_conta_w, vl_recebido_w, vl_titulo_w, :new.nm_usuario);
                                                end if;
                                        end if;
                                end if;

                        end if;

                end if;

        /* francisco - 20/11/2009 - os 173662 */
        elsif   (nr_seq_protocolo_w is not null) then

                if      (ie_tipo_consistencia_w <> 3) or        -- so atualizar o repasse se a baixa nao for cheque
                        (ie_lib_rep_tit_cheque_w = 'S') then    /* ahoffelder - os 429968 - 23/05/2012 */

                        select  b.ie_tipo_convenio,
                                b.cd_convenio
                        into    ie_tipo_convenio_w,
                                cd_convenio_w
                        from    convenio b,
                                protocolo_convenio a
                        where   a.cd_convenio           = b.cd_convenio
                        and     a.nr_seq_protocolo      = nr_seq_protocolo_w;

                        select  nvl(max(ie_lib_repasse),'S'),
								nvl(max(ie_lib_repasse_tit_conv), 'N')
						into    ie_lib_repasse_w,
								ie_lib_repasse_tit_conv_w
						from    convenio_estabelecimento
						where   cd_convenio             = cd_convenio_w
						and     cd_estabelecimento      = cd_estabelecimento_w;

                        if      (:new.nr_seq_trans_caixa is null) then
                                open c01;
                                loop
                                fetch c01 into
                                        nr_interno_conta_w,
                                        vl_conta_w;
                                exit when c01%notfound;

			     select decode(nvl(:new.vl_perdas,0),0,:new.vl_recebido,:new.vl_perdas) into vl_perdas_w from dual;

                                        if      (ie_liberar_repasse_w in ('S','P')) and
												(((ie_tipo_convenio_w = 1 or ie_lib_repasse_tit_conv_w = 'S') and (ie_lib_repasse_w = 'S')) or
												 ((ie_tipo_convenio_w = 4 or ie_lib_repasse_tit_conv_w = 'S') and (ie_lib_repasse_w = 'F'))) then
                                                if      (ie_liberar_repasse_w = 'P') then
                                                        gerar_perdas_repasse(nr_interno_conta_w, :new.nm_usuario, 'B', vl_titulo_w, vl_perdas_w, vl_saldo_titulo_w);
                                                else
                                                        atualizar_repasse_conta(nr_interno_conta_w, vl_saldo_titulo_w, :new.nm_usuario);
                                                end if;
                                        elsif   (ie_liberar_repasse_w in ('S','P')) and
												(((ie_tipo_convenio_w = 1 or ie_lib_repasse_tit_conv_w = 'S') and (ie_lib_repasse_w = 'SP')) or
												 ((ie_tipo_convenio_w = 4 or ie_lib_repasse_tit_conv_w = 'S') and (ie_lib_repasse_w = 'FP'))) then
                                                if      (ie_liberar_repasse_w = 'P') then
                                                        gerar_perdas_repasse(nr_interno_conta_w, :new.nm_usuario, 'B', vl_titulo_w, vl_perdas_w, vl_saldo_titulo_w);
                                                else
                                                        atualizar_repasse_conta_prop(nr_interno_conta_w, :new.vl_recebido, vl_titulo_w, :new.nm_usuario);
                                                end if;

                                        end if;
                                end loop;
                                close c01;
                        end if;
                end if;
        end if;
end if;

select  max(a.ie_entrada_confirmada),
        max(a.nr_seq_conta_banco),
        max(a.vl_desc_previsto),
        max(a.vl_saldo_titulo),
		max(a.vl_titulo)
into    ie_entrada_confirmada_w,
        nr_seq_conta_banco_w,
        vl_desc_previsto_w,
        vl_saldo_tit_w,
		vl_titulo_w
from    titulo_receber a
where   a.nr_titulo = :new.nr_titulo;

if (ie_entrada_confirmada_w = 'C') and
    (:new.nr_seq_cobranca is null) then /*OS 1360248 - Apenas inseir no lote de instrucao caso a baixa NAO for originada de Cobranca Escritural.*/

        /*Cambial passivo somente atualiza o saldo se esse param estiver como Sim, cfme ocorre na atualizar_saldo_tit_rec*/
        select  nvl(max(ie_passivo_saldo_tit),'S')
        into    ie_passivo_saldo_tit_w
        from    parametro_contas_receber a
        where   a.cd_estabelecimento    = cd_estabelecimento_w;

        if (ie_passivo_saldo_tit_w = 'N') then
                vl_cambial_passivo_w := 0;
        else
                vl_cambial_passivo_w := nvl(:new.vl_cambial_passivo,0);
        end if;

        /*Se for uma baixa ou uma baixa de desconto*/
        if ( ((:new.vl_recebido > 0) or (:new.vl_descontos > 0) or (:new.vl_perdas > 0) or (:new.vl_nota_credito > 0) or (vl_cambial_passivo_w > 0)) and (:new.nr_seq_liq_origem is null) ) then

                /*Tipo de ocorrencia 4 pois se trata de concessao de desconto.*/
                ie_tipo_ocorrencia_w := 4;

        /*Se a baixa for negativa ou valor de desconto negativo e tiver sequencia de origem dessa baixa, indica estorno*/
        elsif ( ((:new.vl_recebido < 0) or (:new.vl_descontos < 0)  or (:new.vl_perdas > 0) or (:new.vl_nota_credito > 0) or (vl_cambial_passivo_w > 0)) and (:new.nr_seq_liq_origem is not null) ) then

                /*Tipo de ocorrencia 5 pois se trata de cancelamento de abatimento.*/
                ie_tipo_ocorrencia_w := 5;

        end if;

        /*Se a baixa deixar o saldo 0, indica que foi liquidado, ai enviar a ocorrencia de Baixa de titulo*/
        if ( (nvl(vl_saldo_tit_w,0) - (nvl(:new.vl_recebido,0) + nvl(:new.vl_descontos,0) + nvl(:new.vl_perdas,0) + nvl(:new.vl_nota_credito,0) + nvl(vl_cambial_passivo_w,0))) = 0) then
                ie_tipo_ocorrencia_w := 2;
        end if;

        begin
        select  a.cd_banco,
                        a.cd_agencia_bancaria,
                        a.cd_conta,
                        a.ie_digito_conta
        into    cd_banco_w,
                        cd_agencia_bancaria_w,
                        nr_conta_w,
                        ie_digito_conta_w
        from    banco_estabelecimento a
        where   a.nr_sequencia = nr_seq_conta_banco_w;
        exception when others then
                        ie_retorno_w    := 0;
        end;

        if      (ie_retorno_w = 0) then
                open c03;
                loop
                fetch c03 into
                        cd_banco_w,
                        cd_agencia_bancaria_w,
                        nr_conta_w,
                        ie_digito_conta_w,
                        cd_camara_compensacao_w;
                exit when c03%notfound;
                        cd_banco_w      := cd_banco_w;
                end loop;
                close c03;
        end if;

        obter_param_usuario(815, 9, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_desc_previsto_w);
        obter_param_usuario(815, 16, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_juros_multa_w);

        select  max(a.cd_centro_custo),
                        max(a.nr_seq_motivo_desc)
        into    cd_centro_custo_desc_w,
                        nr_seq_motivo_desc_w
        from    titulo_receber_liq_desc a
        where   a.nr_titulo             = :new.nr_titulo
        and             a.nr_bordero    is null
        and             a.nr_seq_liq    is null;


        insert into titulo_receber_instr (      nr_seq_cobranca,
                                                nr_titulo,
                                                vl_cobranca,
                                                dt_atualizacao,
                                                nm_usuario,
                                                cd_banco,
                                                cd_agencia_bancaria,
                                                nr_conta,
                                                cd_moeda,
                                                ie_digito_conta,
                                                cd_camara_compensacao,
                                                vl_desconto,
                                                vl_desc_previsto,
                                                vl_acrescimo,
                                                nr_sequencia,
                                                vl_despesa_bancaria,
                                                vl_saldo_inclusao,
                                                qt_dias_instrucao,
                                                cd_ocorrencia,
                                                ie_instrucao_enviada,
                                                nr_seq_motivo_desc,
                                                cd_centro_custo_desc,
                                                vl_juros,
                                                vl_multa,
                                                ie_selecionado  )
                        values  (       null,
                                        :new.nr_titulo,
                                        decode(ie_tipo_ocorrencia_w,4, vl_titulo_w, nvl(vl_saldo_tit_w,0) - (nvl(:new.vl_recebido,0) + nvl(:new.vl_descontos,0))),
                                        sysdate,
                                        :new.nm_usuario,
                                        cd_banco_w,
                                        cd_agencia_bancaria_w,
                                        nr_conta_w,
                                        :new.cd_moeda,
                                        ie_digito_conta_w,
                                        cd_camara_compensacao_w,
                                        decode(ie_tipo_ocorrencia_w, 4, nvl(:new.vl_descontos,0), decode(ie_desc_previsto_w,'S', nvl(vl_desc_previsto_w,0)) + nvl(to_number(obter_dados_titulo_receber(:new.nr_titulo,'VNC')),0)),
                                        decode(ie_desc_previsto_w,'S', nvl(vl_desc_previsto_w,0),0),
                                        0,
                                        titulo_receber_instr_seq.nextval,
                                        0,
                                        nvl(vl_saldo_tit_w,0) - (nvl(:new.vl_recebido,0) + nvl(:new.vl_descontos,0)),
                                        null,
                                        substr(obter_ocorrencia_envio_cre(ie_tipo_ocorrencia_w,cd_banco_w),1,3),
                                        'N',
                                        nr_seq_motivo_desc_w,
                                        cd_centro_custo_desc_w,
                                        decode(ie_juros_multa_w,'S',to_number(obter_juros_multa_titulo(:new.nr_titulo,sysdate,'R','J')),null),
                                        decode(ie_juros_multa_w,'S',to_number(obter_juros_multa_titulo(:new.nr_titulo,sysdate,'R','M')),null),
                                        'N');

end if;

-- Se titulo de faturamento
if      (ie_origem_titulo_w = 13) then
        select  nvl(max(ie_gerar_a510_baixa_tit),'N')
        into    ie_gerar_a510_baixa_tit_w
        from    pls_parametros
        where   cd_estabelecimento      = cd_estabelecimento_w;

        if      (ie_gerar_a510_baixa_tit_w = 'S') and
                (ie_situacao_w = 1) and
                (dt_liquidacao_w is null) and
                (:new.vl_glosa = 0) then

                select  max(u.nr_sequencia)
                into    nr_seq_ptu_fatura_w
                from    ptu_fatura      u,
                        pls_fatura      s
                where   s.nr_sequencia  = u.nr_seq_pls_fatura
                and     s.nr_titulo     = :new.nr_titulo;

                if      (nr_seq_ptu_fatura_w is null) then
                        select  max(u.nr_sequencia)
                        into    nr_seq_ptu_fatura_w
                        from    ptu_fatura      u,
                                pls_fatura      s
                        where   s.nr_sequencia  = u.nr_seq_pls_fatura
                        and     s.nr_titulo_ndc = :new.nr_titulo;
                end if;

                -- Gerar A510
                if      (nr_seq_ptu_fatura_w is not null) and
                        (cd_estabelecimento_w is not null)then
                        ptu_gerar_fat_baixa_interc( nr_seq_ptu_fatura_w, null, cd_estabelecimento_w, :new.nm_usuario, 'N', :new.dt_recebimento, :new.vl_recebido, 0, dt_pagamento_previsto_w);
                end if;
        end if;
end if;

if (:new.nr_titulo is not null) and (:new.nr_sequencia is not null) and ( nvl(:new.ie_lib_caixa,'S') = 'S') then
        /*Esse select e para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar que tb dispara a trigger das tabelas com esse insert*/

        select  count(*)
        into    qt_reg_w
        from    intpd_fila_transmissao
        where   nr_seq_documento                = to_char(:new.nr_titulo)
        and     nr_seq_item_documento                    = to_char(:new.nr_sequencia)
        and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
                and             ie_evento in ('32','27');

        if (qt_reg_w = 0) then
                reg_integracao_p.nr_seq_item_documento_p :=      :new.nr_sequencia;
                                /*Envio de cadastros financeiros - Baixa de titulo*/
                                gerar_int_padrao.gravar_integracao('32', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);

                gerar_int_padrao.gravar_integracao('27', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);
        end if;

end if;
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:new.nr_titulo,:new.nr_sequencia,'TRB',:new.dt_recebimento,'I',:new.nm_usuario);
/* Quando houver vl_recurso, gera informacao de glosa a recuperar */
if (nvl(:new.vl_recurso,0) <> 0) then
        gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TR',:new.dt_recebimento,'A',:new.nm_usuario);
end if;

open c04;
loop
fetch c04 into
        c04_w;
exit when c04%notfound;
        begin

        nr_multiplicador_w := 1;
        if      (:new.ie_acao <> 'I') then
                begin
                nr_multiplicador_w := -1;
                end;
        end if;

        if   (c04_w.cd_tipo_lote_contab = 18) then
                begin

                begin
                select  nvl(ie_contab_cr_no_cb, 'N')
                into    ie_contab_cr_no_cb_w
                from    parametro_controle_banc
                where   cd_estabelecimento = cd_estabelecimento_w;
                exception
                when others then
                        ie_contab_cr_no_cb_w:= 'N';
                end;

                if      (ie_contab_cr_no_cb_w = 'S') then
                        begin

                        vl_movimento_w:=        case c04_w.nm_atributo
                                                when 'VL_DESCONTOS' then :new.vl_descontos * nr_multiplicador_w
                                                when 'VL_JUROS' then :new.vl_juros * nr_multiplicador_w
                                                when 'VL_MULTA'  then :new.vl_multa * nr_multiplicador_w
                                                when 'VL_DESPESA_BANCARIA'  then :new.vl_despesa_bancaria * nr_multiplicador_w
                                                when 'VL_RECEBIDO_TESOURARIA'  then :new.vl_recebido * nr_multiplicador_w
                                                when 'VL_ORIGINAL_BAIXA'  then :new.vl_recebido + :new.vl_juros + :new.vl_multa + :new.vl_descontos
                                                when 'VL_CAMBIAL_ATIVO'  then :new.vl_cambial_ativo * nr_multiplicador_w
                                                when 'VL_CAMBIAL_PASSIVO'  then :new.vl_cambial_passivo * nr_multiplicador_w
                                                when 'VL_GLOSA'  then :new.vl_glosa * nr_multiplicador_w
                                                when 'VL_REC_MAIOR'  then :new.vl_rec_maior * nr_multiplicador_w
                                                end;

                        dt_movimento_w          := :new.dt_recebimento;
                        nr_seq_trans_financ_w   := :new.nr_seq_trans_fin;
                        nr_seq_info_w           := 14;
                        nr_documento_w          := :new.nr_titulo;
                        nr_seq_doc_compl_w      := :new.nr_sequencia;
                        nr_doc_analitico_w      := null;
                        nm_tabela_w             := 'TITULO_RECEBER_LIQ_CONTAB_V';

                        end;
                end if;

                end;

        elsif   (c04_w.cd_tipo_lote_contab = 37) then
                vl_movimento_w:=        case c04_w.nm_atributo
                                        when 'VL_RECEBIDO' then :new.vl_recebido -- * nr_multiplicador_w
                                        when 'VL_REC_GLOSA' then :new.vl_glosa --* nr_multiplicador_w
                                        when 'VL_REC_MAIOR'  then :new.vl_rec_maior -- * nr_multiplicador_w
                                        end;

                dt_movimento_w          := :new.dt_recebimento;
                nr_seq_trans_financ_w   := :new.nr_seq_trans_fin;
                nr_seq_info_w           := 14;
                nr_documento_w          := :new.nr_titulo;
                nr_seq_doc_compl_w      := :new.nr_sequencia;
                nr_doc_analitico_w      := null;
                nm_tabela_w             := 'TITULO_RECEBER_LIQ';

                if      (c04_w.nm_atributo = 'VL_REC_MAIOR' and :new.vl_glosa <> 0) then
                        vl_movimento_w := 0;
                end if;
        elsif   (c04_w.cd_tipo_lote_contab = 39) then
                begin
                vl_movimento_w:=        case c04_w.nm_atributo
                                        when 'VL_RECEBIDO' then :new.vl_recebido * nr_multiplicador_w
                                        when 'VL_DESCONTOS' then :new.vl_descontos * nr_multiplicador_w
                                        when 'VL_JUROS'  then :new.vl_juros * nr_multiplicador_w
                                        when 'VL_MULTA'  then :new.vl_multa * nr_multiplicador_w
                                        when 'VL_DESPESA_BANCARIA'  then :new.vl_despesa_bancaria * nr_multiplicador_w
                                        when 'VL_REC_MAIOR'  then :new.vl_rec_maior * nr_multiplicador_w
                                        when 'VL_PERDAS'  then :new.vl_perdas * nr_multiplicador_w
                                        when 'VL_GLOSA'  then :new.vl_glosa * nr_multiplicador_w
                                        when 'VL_CAMBIAL_ATIVO'  then :new.vl_cambial_ativo * nr_multiplicador_w
                                        when 'VL_CAMBIAL_PASSIVO'  then :new.vl_cambial_passivo * nr_multiplicador_w
                                        else
                                                null
                                        end;

                dt_movimento_w          := :new.dt_recebimento;
                nr_seq_trans_financ_w   := :new.nr_seq_trans_fin;
                nr_seq_info_w           := 14;
                nr_documento_w          := :new.nr_titulo;
                nr_seq_doc_compl_w      := :new.nr_sequencia;
                nr_doc_analitico_w      := null;
                nm_tabela_w             := 'TITULO_RECEBER_LIQ';

                nm_tabela_w     := case c04_w.nm_atributo
                                        when 'VL_GLOSA'  then 'TITULO_RECEBER'
                                        when 'VL_CAMBIAL_ATIVO'  then 'TITULO_RECEBER_LIQ_CTB_CAMB_V'
                                        when 'VL_CAMBIAL_PASSIVO'  then 'TITULO_RECEBER_LIQ_CTB_CAMB_V'
                                        else
                                                'TITULO_RECEBER_LIQ'
                                        end;


                if      (c04_w.nm_atributo = 'VL_GLOSA') then
                        begin

                        begin

                        select  nr_seq_mensalidade
                        into    nr_seq_mensalidade_w
                        from    titulo_receber
                        where   nr_titulo = :new.nr_titulo;

                        exception
                        when others then
                                nr_seq_mensalidade_w:= null;
                        end;

                        if      (ie_cc_glosa_w <> 'N') or (nvl(nr_seq_mensalidade_w,0) = 0) then
                                begin

                                        vl_movimento_w:= 0;

                                end;
                        else
                                begin

                                if      (ie_dt_contab_glosa_w = 'F') then
                                        begin

                                        select  max(dt_fechamento)
                                        into    dt_movimento_w
                                        from    convenio_retorno
                                        where   nr_sequencia    = :new.nr_seq_retorno;

                                        end;
                                end if;

                                end;
                        end if;

                        end;
                end if;


                if      ((nvl(nr_seq_trans_financ_w, 0) = 0) or ((c04_w.nm_atributo = 'VL_RECEBIDO')  and (ie_contab_rec_classif_w <> 'N'))) then
                        begin

                        vl_movimento_w := 0;

                        end;
                end if;


                end;
        elsif   (c04_w.cd_tipo_lote_contab = 5) then
                begin

                ie_situacao_ctb_w 		:= 'P';
                dt_movimento_w          := trunc(:new.dt_recebimento);
                nr_seq_trans_financ_w   := :new.nr_seq_trans_fin;
                nr_documento_w          := :new.nr_titulo;
                nr_seq_doc_compl_w      := :new.nr_sequencia;
                nr_doc_analitico_w      := null;

                vl_movimento_w	:=		case c04_w.nm_atributo
                                        when 'VL_CAMBIAL_ATIVO'         then :new.vl_cambial_ativo * nr_multiplicador_w
                                        when 'VL_CAMBIAL_PASSIVO'       then :new.vl_cambial_passivo * nr_multiplicador_w
                                        when 'VL_DESCONTOS'             then :new.vl_descontos * nr_multiplicador_w
                                        when 'VL_JUROS'                 then :new.vl_juros * nr_multiplicador_w
                                        when 'VL_MULTA'                 then :new.vl_multa * nr_multiplicador_w
                                        when 'VL_RECEBIDO_ESTRANG'      then :new.vl_recebido_estrang * nr_multiplicador_w
                                        when 'VL_MOEDA_COMPLEMENTAR'    then :new.vl_complemento * nr_multiplicador_w
                                        when 'VL_DESPESA_BANCARIA'      then :new.vl_despesa_bancaria * nr_multiplicador_w
                                        when 'VL_GLOSA'                 then :new.vl_glosa * nr_multiplicador_w
                                        when 'VL_REC_MAIOR'             then :new.vl_rec_maior * nr_multiplicador_w
                                        when 'VL_PERDAS'                then :new.vl_perdas * nr_multiplicador_w
                                        when 'VL_OUTROS_ACRESCIMOS'     then :new.vl_outros_acrescimos * nr_multiplicador_w
                                        when 'VL_RECURSADO'             then :new.vl_recurso
                                        when 'VL_RECEBIDO_TOTAL'        then (:new.vl_recebido + :new.vl_descontos + :new.vl_juros + :new.vl_multa) * nr_multiplicador_w
                                        when 'VL_RECEBIDO'              then :new.vl_recebido * nr_multiplicador_w
                                        else
                                                null
                                        end;

                nr_seq_info_w   :=      case    c04_w.nm_atributo
                                        when    'VL_CAMBIAL_ATIVO'      then 53
                                        when    'VL_CAMBIAL_PASSIVO'    then 53
                                        when    'VL_RECEBIDO_ESTRANG'   then 49
                                        when    'VL_MOEDA_COMPLEMENTAR' then 49
                                        else
                                                14
                                        end;

                nm_tabela_w		:=      case c04_w.nm_atributo
                                                when 'VL_CAMBIAL_ATIVO'         then 'TITULO_RECEBER_LIQ_CTB_CAMB_V'
                                                when 'VL_CAMBIAL_PASSIVO'       then 'TITULO_RECEBER_LIQ_CTB_CAMB_V'
                                                when 'VL_DESCONTOS'             then 'TITULO_RECEBER_LIQ_CONTAB_V2'
                                                when 'VL_JUROS'                 then 'TITULO_RECEBER_LIQ_CONTAB_V2'
                                                when 'VL_MULTA'                 then 'TITULO_RECEBER_LIQ_CONTAB_V2'
                                                when 'VL_RECEBIDO_ESTRANG'      then 'TITULO_REC_LIQ_CC'
                                                when 'VL_MOEDA_COMPLEMENTAR'    then 'TITULO_REC_LIQ_CC'
                                                else
                                                                'TITULO_RECEBER_LIQ'
                                                end;

				if		(c04_w.nm_atributo in ('VL_DESCONTOS', 'VL_JUROS', 'VL_MOEDA_COMPLEMENTAR', 'VL_MULTA', 'VL_PERDAS', 'VL_RECEBIDO_ESTRANG')) then
						begin
						ie_situacao_ctb_w := 'N';
						end;
				end if;

				if      (ie_contab_rec_classif_w = 'S') then
						begin

						if      (c04_w.nm_atributo = 'VL_RECEBIDO') then
								begin
								ie_situacao_ctb_w := 'N';
								end;
						elsif	(c04_w.nm_atributo = 'VL_RECEBIDO_TOTAL') then
								begin
								vl_movimento_w := null;
								end;
						end if;

						end;
				end if;

				if      (ie_cc_glosa_w = 'S') then
						begin

						if		(c04_w.nm_atributo = 'VL_GLOSA' or (c04_w.nm_atributo = 'VL_REC_MAIOR' and ie_origem_titulo_w <> 3)) then
								begin
								vl_movimento_w:= null;
								end;
						end if;

						end;
				end if;

                end;
        end if;

        if      (nvl(vl_movimento_w, 0) <> 0 and nvl(nr_seq_trans_financ_w, 0) <> 0) then
                begin

                ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
                                                                        dt_movimento_w,
                                                                        c04_w.cd_tipo_lote_contab,
                                                                        nr_seq_trans_financ_w,
                                                                        nr_seq_info_w,
                                                                        nr_documento_w,
                                                                        nr_seq_doc_compl_w,
                                                                        nr_doc_analitico_w,
                                                                        vl_movimento_w,
                                                                        nm_tabela_w,
                                                                        c04_w.nm_atributo,
                                                                        :new.nm_usuario,
                                                                        ie_situacao_ctb_w);
                end;
        end if;

        end;
end loop;
close c04;

end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_liq_integr_afte
after update ON TASY.TITULO_RECEBER_LIQ for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w				number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:new.nr_titulo is not null) and (:new.nr_sequencia is not null) and ( nvl(:new.ie_lib_caixa,'S') = 'S') then

	/*Esse select e para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and		nr_seq_item_documento	= to_char(:new.nr_sequencia)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
	and		ie_evento in ('32','27');

	if (qt_reg_w = 0) then
		reg_integracao_p.nr_seq_item_documento_p		:=	:new.nr_sequencia;

		/*Envio de cadastros financeiros - Baixa de titulo*/
		gerar_int_padrao.gravar_integracao('32', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);

		gerar_int_padrao.gravar_integracao('27', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);
	end if;
end if;
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:new.nr_titulo,:new.nr_sequencia,'TRB',:new.dt_recebimento,'A',:new.nm_usuario);
/* Quando houver vl_recurso, gera informacao de glosa a recuperar */
if (nvl(:new.vl_recurso,0) <> 0) then
	gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TR',:new.dt_recebimento,'A',:new.nm_usuario);
end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_liq_befinsert
before insert ON TASY.TITULO_RECEBER_LIQ for each row
declare

ie_baixa_lote_contab_w		varchar2(2)	:= 'S';
cd_estabelecimento_w		number(5);
qt_registro_w			number(10);
nr_lote_contabil_w			number(10);
nr_seq_protocolo_w		number(10);
nr_interno_conta_w			number(10);
cd_empresa_w			number(4);
dt_fechamento_w			date		:= null;
nr_recibo_w			number(10);
ie_tipo_titulo_w			varchar2(255);
ie_baixa_tit_conta_w		varchar2(255);
cd_convenio_w			number(5);
ie_movto_trans_fin_w		number(10);
ie_estorna_baixa_rep_w		varchar2(10);
cont_w				number(10);
ie_banco_w			varchar2(1);
ie_movto_bco_cobr_escrit_w		varchar2(1);
ie_controle_perdas_w		varchar2(1);
ie_tipo_consistencia_w		number(5);
cd_portador_w			number(10);
ie_baixa_titulo_receber_w		varchar2(1);
ds_portador_w			varchar2(255);
ie_baixa_tit_encontro_conta_w	varchar2(1);
count_w				number(10);
ie_baixa_encontro_contas_w		number(10);
nr_seq_encontro_contas_w		lote_encontro_contas.nr_sequencia%type;
ie_origem_baixa_w			varchar2(2)	:= '';
nr_seq_banco_w			movto_trans_financ.nr_seq_banco%type;
nr_seq_caixa_w			movto_trans_financ.nr_seq_caixa%type;
qt_tit_pagar_w			number(10);
cd_tipo_lote_contabil_w		lote_contabil.cd_tipo_lote_contabil%type;
dt_limite_desconto_w		titulo_receber.dt_limite_desconto%type;
ie_processo_camara_w		pls_parametros_camara.ie_processo_camara%type;
nr_titulo_receber_w			pls_titulo_lote_camara.nr_titulo_receber%type;
nr_seq_lote_w				pls_titulo_lote_camara.nr_sequencia%type;
ie_baixa_camara_compensacao_w	number(10);
dt_emissao_w				titulo_receber.dt_emissao%type;
ie_permite_baixa_ant_atual_w		varchar2(1);
ie_origem_titulo_w			titulo_receber.ie_origem_titulo%type;


ie_baixa_lote_cr_w				parametro_controle_banc.ie_baixa_lote_cr%type;
ie_gerar_movto_bco_baixa_w		parametro_contas_receber.ie_gerar_movto_bco_baixa%type;
nr_seq_parametro_cont_w			number(10);
nr_lote_contab_gerado_w			lote_contabil.nr_lote_contabil%type;
nr_seq_classif_lote_w			lote_contabil_parametro.vl_parametro%type;
ds_tipo_lote_w					varchar2(255);
dt_referencia_w					date;
nr_seq_classif_w				banco_estabelecimento.nr_seq_classif%type;
dt_pagamento_previsto_w			titulo_receber.dt_pagamento_previsto%type;
nr_seq_mensalidade_w			titulo_receber.nr_seq_mensalidade%type;
ie_indice_correcao_w			pls_mensalidade.ie_indice_correcao%type;

begin

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S')	= 'S')  then

select	max(ie_tipo_titulo),
	obter_convenio_tit_rec(max(nr_titulo))
into	ie_tipo_titulo_w,
	cd_convenio_w
from	titulo_receber
where	nr_titulo	= :new.nr_titulo;

select	max(a.ie_banco)
into	ie_banco_w
from	transacao_financeira a
where	a.nr_sequencia	= :new.nr_seq_trans_fin;

select	max(ie_tipo_consistencia)
into	ie_tipo_consistencia_w
from	tipo_recebimento
where	cd_tipo_recebimento = :new.cd_tipo_recebimento;

select	max(cd_estabelecimento),
	max(nr_interno_conta),
	max(cd_portador),
	max(dt_limite_desconto),
	max(dt_emissao),
	max(ie_origem_titulo),
	max(dt_pagamento_previsto),
	max(nr_seq_mensalidade)
into	cd_estabelecimento_w,
	nr_interno_conta_w,
	cd_portador_w,
	dt_limite_desconto_w,
	dt_emissao_w,
	ie_origem_titulo_w,
	dt_pagamento_previsto_w,
	nr_seq_mensalidade_w
from	titulo_receber
where	nr_titulo	= :new.nr_titulo;

select	max(ie_baixa_lote_contab),
	nvl(max(ie_baixa_tit_conta), 'S'),
	max(ie_movto_bco_cobr_escrit),
	max(ie_controle_perdas),
	nvl(max(ie_baixa_tit_encontro_conta),'S'),
	nvl(max(ie_gerar_movto_bco_baixa),'N')
into	ie_baixa_lote_contab_w,
	ie_baixa_tit_conta_w,
	ie_movto_bco_cobr_escrit_w,
	ie_controle_perdas_w,
	ie_baixa_tit_encontro_conta_w,
	ie_gerar_movto_bco_baixa_w
from	parametro_contas_receber
where 	cd_estabelecimento	= cd_estabelecimento_w;


if	(ie_banco_w <> 'N') and /* ahoffelder - os 393313 - 12/12/2011 - nao consistir se a tf nao entra no banco */
	(ie_tipo_titulo_w <> '9') and /* se o titulo for de desconto em folha, nao entra no movimento bancario porque a empresa responsavel faz a baixa */
	((:new.nr_seq_cobranca is null) or
	(ie_movto_bco_cobr_escrit_w <> 'C')) then
	consiste_movto_banco(:new.nr_seq_conta_banco,:new.dt_recebimento);
end if;

obter_param_usuario(801, 128, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_estorna_baixa_rep_w);

if	(nvl(ie_estorna_baixa_rep_w, 'S') = 'N') and (nvl(nr_interno_conta_w, 0) > 0) then

	select	count(*)
	into	cont_w
	from	(
		select	b.nr_sequencia,
			b.vl_original_repasse,
			b.vl_repasse
		from	procedimento_repasse b,
			procedimento_paciente a,
			repasse_terceiro c
		where	a.nr_sequencia		= b.nr_seq_procedimento
		and	a.nr_interno_conta		= nr_interno_conta_w
		and	c.nr_repasse_terceiro	= b.nr_repasse_terceiro
		and	c.ie_status		= 'F'
		and	b.ie_status		in ('A','D')
		);

	if	(:new.vl_recebido < 0) and (cont_w > 0) then
		/* sem permissao para estornar baixa de titulos com repasse fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(200845);
	end if;

end if;

if	(cd_estabelecimento_w is not null) then

	if	(fin_obter_se_mes_aberto(cd_estabelecimento_w, :new.dt_recebimento,'CR',null,cd_convenio_w,null, :new.nr_seq_retorno) = 'N') then
		/* nao e possivel baixar o titulo com esta data pois este mes/dia de referencia financeiro ja esta fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(200846);
	end if;

	/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_recebimento);

	if	(ie_baixa_tit_conta_w = 'N')  and
		(nr_interno_conta_w is not null) then
		select	max(nr_seq_protocolo)
		into	nr_seq_protocolo_w
		from	conta_paciente
		where	nr_interno_conta	= nr_interno_conta_w;
		if	(nr_seq_protocolo_w is null) then
			/* a conta nr_interno_conta_w nao esta em protocolo! verifique os parametros do contas a receber. */
			wheb_mensagem_pck.exibir_mensagem_abort(200847,'NR_INTERNO_CONTA_W=' || nr_interno_conta_w);
		end if;
	end if;

	select	max(cd_empresa)
	into	cd_empresa_w
	from	estabelecimento
	where	cd_estabelecimento = cd_estabelecimento_w;

	/* ahoffelder - os 325513 - 17/06/2011 - a consistencia ja e feita na movto_trans_financ_insert */
	select	count(*)
	into	ie_movto_trans_fin_w
	from	v$session
	where	audsid		= (select userenv('sessionid') from dual)
	and	upper(action) 	= 'ATUALIZAR_TRANSACAO_FINANCEIRA';

	if	(cd_empresa_w is not null) and
		(nvl(ie_movto_trans_fin_w,0) = 0) then

		cd_tipo_lote_contabil_w := 5;

		if	(:new.nr_seq_conta_banco is not null) and
			(ie_banco_w <> 'N') then
			cd_tipo_lote_contabil_w := 18;
		end if;

		if(ie_origem_titulo_w = 3) then
			cd_tipo_lote_contabil_w := 39;
		end if;


		if	(ie_baixa_lote_contab_w = 'N') then

			select	count(*)
			into	qt_registro_w
			from 	ctb_mes_ref
			where	cd_empresa = cd_empresa_w
			and	substr(ctb_obter_se_mes_fechado(nr_sequencia,cd_estabelecimento_w),1,1) = 'F'
                	and	trunc(dt_referencia,'month') = trunc(:new.dt_recebimento,'month');

			if	(qt_registro_w > 0) then
				/* o mes de referencia na contabilidade para esta data de baixa (:new.dt_recebimento) ja foi fechado! */
				wheb_mensagem_pck.exibir_mensagem_abort(200848,'DT_RECEBIMENTO_W=' || :new.dt_recebimento);
			end if;

		elsif	(ie_baixa_lote_contab_w = 'L') then

			select	nvl(max(nr_lote_contabil), 0)
			into	nr_lote_contabil_w
			from 	lote_contabil a
			where	a.cd_estabelecimento 	= cd_estabelecimento_w
			and	trunc(a.dt_referencia,'dd')	= trunc(:new.dt_recebimento,'dd')
			and	a.cd_tipo_lote_contabil	= cd_tipo_lote_contabil_w /* francisco - os 174526 - 23/10/2009 - faltava restringir tipo do lote contas a receber */
			and	(a.nr_lote_contabil = :new.nr_lote_contabil or nvl(:new.nr_lote_contabil,0) = 0)
			and	exists	(	select	1
						from	movimento_contabil z
						where	z.nr_lote_contabil	= a.nr_lote_contabil);

			if	(nr_lote_contabil_w > 0) then
				/* ja foi gerado lote contabil para esta data de baixa!
				lote contabil: nr_lote_contabil_w
				titulo: :new.nr_titulo
				dt recebimento: to_char(:new.dt_recebimento, 'dd/mm/yyyy') */
				wheb_mensagem_pck.exibir_mensagem_abort(200849,	'NR_LOTE_CONTABIL_W=' || nr_lote_contabil_w ||
										';NR_TITULO_W=' || :new.nr_titulo ||
										';DT_RECEBIMENTO_W=' || to_char(:new.dt_recebimento, 'dd/mm/yyyy'));
			end if;

		elsif	(ie_baixa_lote_contab_w = 'M') then

			select	nvl(max(nr_lote_contabil), 0)
			into	nr_lote_contabil_w
			from 	lote_contabil a
			where	a.cd_estabelecimento 	= cd_estabelecimento_w
			and	trunc(a.dt_referencia,'dd')	>= trunc(:new.dt_recebimento,'dd')
			and	trunc(a.dt_referencia,'month')	= trunc(:new.dt_recebimento,'month')
			and	a.cd_tipo_lote_contabil	= cd_tipo_lote_contabil_w /* francisco - os 174526 - 23/10/2009 - faltava restringir tipo do lote contas a receber */
			and	(a.nr_lote_contabil = :new.nr_lote_contabil or nvl(:new.nr_lote_contabil,0) = 0)
			and	exists	(	select	1
						from	movimento_contabil z
						where	z.nr_lote_contabil	= a.nr_lote_contabil);

			if	(nr_lote_contabil_w > 0) then
				/* ja foi gerado lote contabil para esta data de baixa!
				lote contabil: nr_lote_contabil_w
				titulo: :new.nr_titulo
				dt recebimento: to_char(:new.dt_recebimento, 'dd/mm/yyyy') */
				wheb_mensagem_pck.exibir_mensagem_abort(200849,	'NR_LOTE_CONTABIL_W=' || nr_lote_contabil_w ||
										';NR_TITULO_W=' || :new.nr_titulo ||
										';DT_RECEBIMENTO_W=' || to_char(:new.dt_recebimento, 'dd/mm/yyyy'));
			end if;

		elsif	(ie_baixa_lote_contab_w = 'F') then

			select	count(*)
			into	qt_registro_w
			from 	ctb_mes_ref
			where	cd_empresa = cd_empresa_w
                		and	trunc(dt_referencia,'month') = trunc(:new.dt_recebimento,'month');

			if	(qt_registro_w > 0) then
				/* ja existe mes de referencia na contabilidade para esta data de baixa (:new.dt_recebimento). */
				wheb_mensagem_pck.exibir_mensagem_abort(200850,'DT_RECEBIMENTO_W=' || :new.dt_recebimento);
			end if;

		elsif	(ie_baixa_lote_contab_w = 'D') and
			(:new.nr_seq_movto_trans_fin is null) and
			(:new.nr_seq_caixa_rec is null) then

			select	nvl(max(nr_lote_contabil), 0)
			into	nr_lote_contabil_w
			from 	lote_contabil a
			where	a.cd_estabelecimento 	= cd_estabelecimento_w
			and	trunc(a.dt_referencia,'dd')	= trunc(:new.dt_recebimento,'dd')
			and	a.cd_tipo_lote_contabil	= cd_tipo_lote_contabil_w /* francisco - os 174526 - 23/10/2009 - faltava restringir tipo do lote contas a receber */
			and	exists	(	select	1
						from	movimento_contabil z
						where	z.nr_lote_contabil	= a.nr_lote_contabil);

			if	(nr_lote_contabil_w > 0) then
				/* ja foi gerado lote contabil para esta data de baixa!
				lote contabil: nr_lote_contabil_w
				titulo: :new.nr_titulo
				dt recebimento: to_char(:new.dt_recebimento, 'dd/mm/yyyy') */
				wheb_mensagem_pck.exibir_mensagem_abort(200849,	'NR_LOTE_CONTABIL_W=' || nr_lote_contabil_w ||
										';NR_TITULO_W=' || :new.nr_titulo ||
										';DT_RECEBIMENTO_W=' || to_char(:new.dt_recebimento, 'dd/mm/yyyy'));
			end if;

		end if;

	end if;

	/* 1700588 - Inicio da consistencia de movimento bancario na baixa, que atualmente so faz na movto_trans_financ_insert, e deixa o processo pela metade, o titulo baixado, sem movimentar o banco*/
	/*OS 1705851 - Titulo de OPS mensalidade (origem 3) nao estava consitindo, pois o cd_tipo_lote_contabil eh setado como 39 acima*/
	if (:new.nr_seq_conta_banco is not null) then
		cd_tipo_lote_contabil_w := 18;
	end if;

	if (ie_gerar_movto_bco_baixa_w = 'S') and (:new.nr_seq_conta_banco is not null) and (cd_tipo_lote_contabil_w = 18) then

		select	max(nr_seq_classif)
		into	nr_seq_classif_w
		from	banco_estabelecimento
		where	nr_sequencia	= :new.nr_seq_conta_banco;

		select	max(a.ie_baixa_lote_cr)
		into	ie_baixa_lote_cr_w
		from	parametro_controle_banc a
		where	a.cd_estabelecimento	= cd_estabelecimento_w;

		/*Igual faz na movto_trans_financ_insert*/
		nr_seq_parametro_cont_w	:= 2;

		if	(ie_baixa_lote_cr_w = 'N') then

			select	count(*)
			into	qt_registro_w
			from 	ctb_mes_ref
			where	cd_empresa							= cd_empresa_w
			and		substr(ctb_obter_se_mes_fechado(nr_sequencia,cd_estabelecimento_w),1,1)	= 'F'
			and		trunc(dt_referencia,'month')						= trunc(:new.dt_recebimento,'month');

			if	(qt_registro_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(183677,'DT_TRANSACAO_P='||:new.dt_recebimento);
			end if;

		elsif	(ie_baixa_lote_cr_w = 'L') then
			select	count(*),
					max(a.nr_lote_contabil),
					substr(ctb_obter_desc_tipo_lote(max(a.cd_tipo_lote_contabil)),1,255),
					max(a.dt_referencia)
			into	qt_registro_w,
					nr_lote_contab_gerado_w,
					ds_tipo_lote_w,
					dt_referencia_w
			from 	lote_contabil a
			where	a.cd_estabelecimento	= cd_estabelecimento_w
			and		trunc(a.dt_referencia,'dd')		= trunc(:new.dt_recebimento,'dd')
			and		a.dt_geracao_lote	is not null
			and		a.cd_tipo_lote_contabil			= cd_tipo_lote_contabil_w
			and	exists	(
					select	1
					from	movimento_contabil z
					where	z.nr_lote_contabil	= a.nr_lote_contabil
					);

			if	(qt_registro_w > 0) then

				select	nvl(max(vl_parametro),0)
				into	nr_seq_classif_lote_w
				from	lote_contabil_parametro
				where	nr_lote_contabil	= nr_lote_contab_gerado_w
				and		nr_seq_parametro	= nr_seq_parametro_cont_w;

				if (nvl(nr_seq_classif_lote_w,0) = 0) then

					select 	nvl(max(a.nr_documento),0)
					into	nr_seq_classif_lote_w
					from	lote_contabil_param_item a
					where	nr_lote_contabil	= nr_lote_contab_gerado_w
					and		nr_seq_parametro	= nr_seq_parametro_cont_w;

				end if;


				if	(nr_seq_classif_lote_w = 0) or (nr_seq_classif_lote_w = nr_seq_classif_w) then
					/*Ja foi gerado lote contabil para esta data de baixa.*/
					wheb_mensagem_pck.exibir_mensagem_abort(183678,'NR_LOTE_P='||nr_lote_contab_gerado_w||';ds_tipo_p='||ds_tipo_lote_w||';dt_referencia_p='||dt_referencia_w);
				end if;
			end if;

		elsif	(ie_baixa_lote_cr_w = 'M') then

			select	count(*),
					max(a.nr_lote_contabil),
					substr(ctb_obter_desc_tipo_lote(max(a.cd_tipo_lote_contabil)),1,255),
					max(a.dt_referencia)
			into	qt_registro_w,
					nr_lote_contab_gerado_w,
					ds_tipo_lote_w,
					dt_referencia_w
			from 	lote_contabil a
			where	a.cd_estabelecimento	= cd_estabelecimento_w
			and		trunc(a.dt_referencia,'dd')		>= trunc(:new.dt_recebimento,'dd')
			and		trunc(a.dt_referencia,'month')		= trunc(:new.dt_recebimento,'month')
			and		a.dt_geracao_lote	is not null
			and		a.cd_tipo_lote_contabil			= cd_tipo_lote_contabil_w
			and		exists	(
					select	1
					from	movimento_contabil z
					where	z.nr_lote_contabil	= a.nr_lote_contabil
					);

			if	(qt_registro_w > 0) then

				select	nvl(max(vl_parametro),0)
				into	nr_seq_classif_lote_w
				from	lote_contabil_parametro
				where	nr_lote_contabil	= nr_lote_contab_gerado_w
				and	nr_seq_parametro	= nr_seq_parametro_cont_w;

				if (nvl(nr_seq_classif_lote_w,0) = 0) then

					select 	nvl(max(a.nr_documento),0)
					into	nr_seq_classif_lote_w
					from	lote_contabil_param_item a
					where	nr_lote_contabil	= nr_lote_contab_gerado_w
					and		nr_seq_parametro	= nr_seq_parametro_cont_w;

				end if;



				if	(nr_seq_classif_lote_w = 0) or (nr_seq_classif_lote_w = nr_seq_classif_w) then
					/*Ja foi gerado lote contabil para esta data de baixa.*/
					wheb_mensagem_pck.exibir_mensagem_abort(183678,'NR_LOTE_P='||nr_lote_contab_gerado_w||';ds_tipo_p='||ds_tipo_lote_w||';dt_referencia_p='||dt_referencia_w);
				end if;
			end if;

		elsif	(ie_baixa_lote_cr_w = 'F') then

			select	count(*)
			into	qt_registro_w
			from 	ctb_mes_ref
			where	cd_empresa		= cd_empresa_w
			and		trunc(dt_referencia,'month')	= trunc(:new.dt_recebimento,'month');

			if	(qt_registro_w > 0) then
				wheb_mensagem_pck.exibir_mensagem_abort(183679,'DT_TRANSACAO_P=' || :new.dt_recebimento);
			end if;

		end if;

	end if;

end if;

if	(nvl(ie_baixa_tit_encontro_conta_w,'S') = 'N') then

	select	count(*)
	into	ie_baixa_encontro_contas_w
	from	v$session
	where	audsid		= (select userenv('sessionid') from dual)
	and	(upper(action) = 'BAIXAR_LOTE_ENCONTRO_CONTAS' or
		upper(action) = 'ESTORNAR_LOTE_ENCONTRO_CONTAS' or
		upper(action) = 'ESTORNAR_PESSOA_ENC_CONTAS' or
		upper(action) = 'BAIXAR_TITULOS_COBRANCA_ESCRIT' or
		upper(action) = 'BAIXAR_ENC_CONTAS_ABATIMENTO');

	if	(ie_baixa_encontro_contas_w = 0) then --Nao consistir se a baixa for do encontro de contas
		select	max(c.nr_sequencia)
		into	nr_seq_encontro_contas_w
		from	encontro_contas_item a,
			pessoa_encontro_contas b,
			lote_encontro_contas c
		where	a.nr_titulo_receber	= :new.nr_titulo
		and	a.nr_seq_pessoa		= b.nr_sequencia
		and	b.nr_seq_lote		= c.nr_sequencia
		and	c.dt_cancelamento	is null;

		if	(nvl(nr_seq_encontro_contas_w,0) <> 0) then
			/*Nao e permitido registrar baixa para titulos que estao em Encontro de Contas!
			Lote de encontro de contas: nr_seq_encontro_contas
			Titulo: nr_titulo */
			wheb_mensagem_pck.exibir_mensagem_abort(288363,	'NR_SEQ_ENCONTRO_CONTAS=' || nr_seq_encontro_contas_w ||
									';NR_TITULO=' || :new.nr_titulo);
		end if;
	end if;
end if;

if (cd_estabelecimento_w is not null) then

	/*verificar se o titulo que esta recebendo a baixa esta em um lote de camra de compensacao que nao esta baixado.*/
	select	count(a.nr_titulo_receber),
		max(b.nr_sequencia)
	into	nr_titulo_receber_w,
		nr_seq_lote_w
	from	pls_titulo_lote_camara a,
		pls_lote_camara_comp b
	where	a.nr_seq_lote_camara	= b.nr_sequencia
	and	a.nr_titulo_receber		= :new.nr_titulo;

	select	nvl(max(a.ie_processo_camara),'CO')
	into	ie_processo_camara_w
	from	pls_parametros_camara a
	where 	cd_estabelecimento = cd_estabelecimento_w;

	if	(ie_processo_camara_w = 'CA') and	-- processo camara de compensacao for regime de caixa
		(nr_titulo_receber_w > 0) and		-- titulo estiver na camara
		(nvl(:new.vl_glosa,0) = 0) then 	-- nao ter valor de glosa

		/*verifica se a rotina que esta efetuando a baixa e a rotina que baixa pela camara. Se nao for, deve consistir.*/
		select	count(*)
		into	ie_baixa_camara_compensacao_w
		from	v$session
		where	audsid			= (select userenv('sessionid') from dual)
		and		(upper(action) 	= 'PLS_BAIXAR_LOTE_CAMARA_COMP' or
				 upper(action)  = 'PLS_ESTORNAR_LOTE_CAMARA_COMP');

		if	(ie_baixa_camara_compensacao_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(335779,	'NR_SEQ_LOTE_W=' || nr_seq_lote_w ||';NR_TITULO_W=' || :new.nr_titulo);
		end if;
	end if;
end if;


if	(:new.nr_seq_caixa_rec is not null) then
	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :new.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/* nao e possivel inserir um novo titulo!
		o recibo nr_recibo_w ja foi fechado! */
		wheb_mensagem_pck.exibir_mensagem_abort(200851,'NR_RECIBO_W=' || nr_recibo_w);
	end if;
end if;

/* tratar lotes contabeis - performance */
if	(:new.nr_lote_contab_pro_rata is null) then
	:new.nr_lote_contab_pro_rata	:= 0;
end if;

if	(:new.nr_lote_contab_antecip is null) then
	:new.nr_lote_contab_antecip	:= 0;
end if;

if	(:new.nr_lote_contabil is null) then
	:new.nr_lote_contabil	:= 0;
end if;

if	(ie_controle_perdas_w = 'S') and
	(ie_tipo_consistencia_w = 9) and
	(obter_funcao_ativa = 801) and
	(:new.ie_tipo_perda is null) and
	(:new.nr_seq_liq_origem is null) then
	/*as baixas por perdas devem ser efetuadas na pasta geracao de baixas por perdas.*/
	wheb_mensagem_pck.exibir_mensagem_abort(217429);
end if;

select	max(ds_portador),
	nvl(max(ie_baixa_titulo_receber),'S')
into	ds_portador_w,
	ie_baixa_titulo_receber_w
from	portador
where	cd_portador = cd_portador_w;

if	(ie_baixa_titulo_receber_w = 'N') then
	wheb_mensagem_pck.exibir_mensagem_abort(226887, 	'NR_TITULO=' || :new.nr_titulo || ';' ||
							'DS_PORTADOR=' || ds_portador_w);
end if;

/* Controle Bancario e Tesouraria */
select	max(a.nr_seq_banco),
	max(a.nr_seq_caixa)
into 	nr_seq_banco_w,
	nr_seq_caixa_w
from 	movto_trans_financ a
where 	a.nr_sequencia	= :new.nr_seq_movto_trans_fin;

/* abatimento */
select	count(*)
into	qt_tit_pagar_w
from	titulo_pagar_baixa a
where	a.nr_seq_baixa_rec	= :new.nr_sequencia
and	a.nr_tit_receber	= :new.nr_titulo;

if	(:new.nr_seq_liq_origem is not null) then
	ie_origem_baixa_w	:= 'ET';
elsif	(nvl(qt_tit_pagar_w,0)	> 0) then
	ie_origem_baixa_w	:= 'AB';
elsif	(:new.nr_bordero is not null) then
	ie_origem_baixa_w	:= 'BR';
elsif	(:new.nr_seq_movto_trans_fin is not null) and
	(nr_seq_banco_w is not null) then
	ie_origem_baixa_w	:= 'CB';
elsif	(:new.nr_seq_cobranca is not null) then
	ie_origem_baixa_w	:= 'CE';
elsif	(:new.nr_seq_retorno is not null) then
	ie_origem_baixa_w	:= 'CR';
elsif	(:new.nr_adiantamento is not null) then
	ie_origem_baixa_w	:= 'DA';
elsif	(:new.nr_seq_deposito_ident is not null) then
	ie_origem_baixa_w	:= 'DI';
elsif	(:new.nr_seq_lote_hist_guia is not null) then
	ie_origem_baixa_w	:= 'GR';
elsif	(:new.nr_seq_negociacao_cr is not null) then
	ie_origem_baixa_w	:= 'NR';
elsif	(:new.nr_seq_caixa_rec is not null) then
	ie_origem_baixa_w	:= 'RC';
elsif	(:new.nr_repasse_terceiro is not null) then
	ie_origem_baixa_w	:= 'RT';
elsif	(:new.nr_seq_lote_enc_contas is not null) then
	ie_origem_baixa_w	:= 'EC';
elsif	(:new.nr_seq_movto_trans_fin is not null) and
	(nr_seq_caixa_w is not null) then
	ie_origem_baixa_w	:= 'TS';
else
	ie_origem_baixa_w	:= 'MR';
end if;

if	(ie_origem_baixa_w is not null) then
	:new.ie_origem_baixa	:= ie_origem_baixa_w;
end if;

if (dt_emissao_w is not null) and (:new.dt_recebimento is not null) then
																								/*apenas se nao for baixa de estorno*/
	if ( to_date(to_char(:new.dt_recebimento,'dd/mm/yyyy'),'dd/mm/yyyy') < to_date(to_char(dt_emissao_w,'dd/mm/yyyy'),'dd/mm/yyyy')) and (:new.nr_seq_liq_origem is null)  then
		/*A data de recebimento nao pode ser menor do que a data de emissao do titulo!*/
		wheb_mensagem_pck.exibir_mensagem_abort(337608, 'DT_RECEBIMENTO_W=' || :new.dt_recebimento || ';' ||
															'DT_EMISSAO_W=' || dt_emissao_w || ';' ||
															'NR_TITULO_W=' || :new.nr_titulo );
	end if;

end if;

obter_param_usuario(801, 203, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_permite_baixa_ant_atual_w);

if ( nvl(ie_permite_baixa_ant_atual_w,'S') = 'N') then

	if( trunc(:new.dt_recebimento) < trunc(sysdate) ) then
		wheb_mensagem_pck.exibir_mensagem_abort(352082);
	end if;

end if;

/*OS 1811433 - Calcular indice de correcao monetaria*/
begin
:new.vl_correcao_monetaria := 0;
if (nr_seq_mensalidade_w is not null) then

	select	max(ie_indice_correcao)
	into	ie_indice_correcao_w
	from	pls_mensalidade
	where	nr_sequencia = nr_seq_mensalidade_w;

	if (ie_indice_correcao_w is not null) then

		:new.vl_correcao_monetaria := nvl(obter_valor_correcao_monet( ie_indice_correcao_w,
																  :new.vl_recebido,
																  dt_pagamento_previsto_w,
																  :new.dt_recebimento),0);
	end if;

end if;
exception when others then
	:new.vl_correcao_monetaria := 0;
end;
/*OS 1811433  FIM*/

:new.DS_STACK := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TITULO_RECEBER_LIQ_tp  after update ON TASY.TITULO_RECEBER_LIQ FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_TITULO='||to_char(:old.NR_TITULO)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA);gravar_log_alteracao(substr(:old.VL_CORRECAO_MONETARIA,1,4000),substr(:new.VL_CORRECAO_MONETARIA,1,4000),:new.nm_usuario,nr_seq_w,'VL_CORRECAO_MONETARIA',ie_log_w,ds_w,'TITULO_RECEBER_LIQ',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_liq_auton
after insert or update ON TASY.TITULO_RECEBER_LIQ for each row
declare

pragma autonomous_transaction;

cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;
dt_movimento_w          ctb_documento.dt_competencia%type;
vl_baixa_w              titulo_receber.vl_titulo%type;
vl_movimento_w          ctb_documento.vl_movimento%type;
vl_tributo_tot_w        titulo_receber_trib.vl_tributo%type;
qt_baixas_w             number(10);

begin

select 	count(*)
into	qt_baixas_w
from	titulo_receber_liq
where	nr_titulo = :new.nr_titulo;

if      ( qt_baixas_w > 0 ) then

        select  vl_titulo,
                cd_estabelecimento,
                dt_contabil
        into    vl_baixa_w,
                cd_estabelecimento_w,
                dt_movimento_w
        from    titulo_receber
        where   nr_titulo = :new.nr_titulo;

        select 	sum(vl_tributo)
        into	vl_tributo_tot_w
        from	titulo_receber_trib
        where	nr_titulo = :new.nr_titulo;

	vl_movimento_w := vl_baixa_w - (vl_tributo_tot_w / qt_baixas_w);

        if      ( nvl(vl_movimento_w,0) <> 0 ) then
                ctb_concil_financeira_pck.ctb_gravar_documento (cd_estabelecimento_w,
                                                                dt_movimento_w,
                                                                5,
                                                                :new.nr_seq_trans_fin,
                                                                14,
                                                                :new.nr_titulo,
                                                                :new.nr_sequencia,
                                                                null,
                                                                vl_movimento_w,
                                                                'TITULO_RECEBER_LIQ',
                                                                'VL_BAIXA_SEM_TRIB',
                                                                :new.nm_usuario);
        commit;
        end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.ssj_consiste_titulo_baixado
BEFORE INSERT
ON TASY.TITULO_RECEBER_LIQ 

FOR EACH ROW
DECLARE

nr_titulo_w number(10);
qt_registro_w number(10);

BEGIN
   
    nr_titulo_w:= :new.nr_titulo;
    
    select  count(*)
    into qt_registro_w
    from titulo_receber
    where nr_titulo = nr_titulo_w
    and cd_estabelecimento = 2
    and ie_situacao = 6;

    if(qt_registro_w > 0)then
        
        raise_application_error(-20011, 'N�o foi poss�vel baixar o lote de cobran�a. Existem t�tulos liquidados como perda!' || chr(10) ||'T�tulo: '||nr_titulo_w);
        
    end if;


       
END SSJ_LEITURA_COMUNIC_PREST_WEB;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_liq_mx
after insert ON TASY.TITULO_RECEBER_LIQ for each row
declare
nr_sequencia_w  	nota_fiscal.nr_sequencia%type;
ie_tipo_consistencia_w  tipo_recebimento.ie_tipo_consistencia%type;
ie_forma_pagamento_w	condicao_pagamento.ie_forma_pagamento%type;
ie_utilizar_cfdi_w	nfe_senha_conexao.ie_utilizar_cfdi%type;
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select 	nvl(max(ie_utilizar_cfdi), 'N')
into	ie_utilizar_cfdi_w
from   	nfe_senha_conexao
where  	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento
and	ie_tipo_nota = 'NFSE';

if (nvl(philips_param_pck.get_cd_pais, 1) = 2) and (ie_utilizar_cfdi_w = 'S') then

	select max(ie_tipo_consistencia)
	into   ie_tipo_consistencia_w
	from   tipo_recebimento
	where  cd_tipo_recebimento = :new.cd_tipo_recebimento;

	select 	nvl(Obter_tp_nfe_avista_aprazo(max(nr_seq_nf_saida)),0)
	into	ie_forma_pagamento_w
	from 	titulo_receber
	where 	nr_titulo = :new.nr_titulo;

	if  (ie_tipo_consistencia_w <> 7) and (ie_forma_pagamento_w <> 1) then

		if (:new.vl_recebido > 0) then
			gerar_nota_zero(:new.nr_sequencia,:new.nr_titulo,:new.nm_usuario,'N');
		elsif (:new.vl_recebido < 0) then

			select max(nr_sequencia)
			into   nr_sequencia_w
			from   nota_fiscal
			where  nr_seq_baixa_tit = :new.nr_seq_liq_origem
			and    nr_sequencia_ref = (select nr_seq_nf_saida
						   from   titulo_receber
						   where  nr_titulo = :new.nr_titulo);

			if (nr_sequencia_w is not null) then
			    estornar_nota_fiscal(nr_sequencia_w,:new.nm_usuario);
			    gerar_nota_zero(:new.nr_sequencia,:new.nr_titulo,:new.nm_usuario,'S');
			end if;

		end if;
	end if;
end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TITULO_RECEBER_LIQ_DELETE
BEFORE DELETE ON TASY.TITULO_RECEBER_LIQ FOR EACH ROW
DECLARE

dt_fechamento_w			date	:= null;
nr_recibo_w			number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:old.nr_seq_caixa_rec is not null) then
	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :old.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/* Nao e possivel excluir a baixa.
		O recibo #@NR_RECIBO#@ ja foi fechado!*/
		wheb_mensagem_pck.exibir_mensagem_abort(267076, 'NR_RECIBO=' || nr_recibo_w);
	end if;
end if;
/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_titulo,:old.nr_sequencia,'TRB',:old.dt_recebimento,'E',:old.nm_usuario);
/* Quando houver vl_recurso, gera informacao de glosa a recuperar */
if (nvl(:old.vl_recurso,0) <> 0) then
	gravar_agend_fluxo_caixa(:old.nr_titulo,null,'TR',:old.dt_recebimento,'E',:old.nm_usuario);
end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TITULO_RECEBER_LIQ_UPDATE
BEFORE UPDATE ON TASY.TITULO_RECEBER_LIQ FOR EACH ROW
DECLARE

dt_fechamento_w			date	:= null;
nr_recibo_w			number(10);
ie_origem_baixa_w		varchar2(2)	:= '';
nr_seq_banco_w			movto_trans_financ.nr_seq_banco%type;
nr_seq_caixa_w			movto_trans_financ.nr_seq_caixa%type;
qt_tit_pagar_w			number(10);
ie_altera_vl_glosa_w	varchar2(1);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
obter_param_usuario(801, 208, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_altera_vl_glosa_w);

if (nvl(ie_altera_vl_glosa_w,'S') = 'N') and (nvl(:old.vl_glosa,0) <> nvl(:new.vl_glosa,0)) then
	wheb_mensagem_pck.exibir_mensagem_abort(458443);
end if;

if	(:old.nr_seq_caixa_rec is not null) and
	(:old.dt_antecipacao_pls_mens is null) and
	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') and
	(nvl(:old.nr_lote_contabil,0) = nvl(:new.nr_lote_contabil,0)) and
	(nvl(:old.nr_lote_contab_antecip,0) = nvl(:new.nr_lote_contab_antecip,0)) and
	(nvl(:old.nr_lote_contab_pro_rata,0) = nvl(:new.nr_lote_contab_pro_rata,0)) and
	(nvl(:old.nr_seq_movto_trans_fin,0) = nvl(:new.nr_seq_movto_trans_fin,0)) and
	(nvl(:old.nr_seq_trans_fin,0) = nvl(:new.nr_seq_trans_fin,0)) and
	(nvl(:old.nr_adiantamento,0) = nvl(:new.nr_adiantamento,0)) and
	(nvl(:old.dt_autenticacao,to_date('01/01/2008','dd/mm/yyyy')) = nvl(:new.dt_autenticacao,to_date('01/01/2008','dd/mm/yyyy'))) and
	(nvl(:old.dt_antecipacao_pls_mens,to_date('01/01/2010','dd/mm/yyyy')) =
			nvl(:new.dt_antecipacao_pls_mens,to_date('01/01/2010','dd/mm/yyyy'))) and
	(nvl(:old.dt_referencia_pls_mens,to_date('01/01/2010','dd/mm/yyyy')) =
			nvl(:new.dt_referencia_pls_mens,to_date('01/01/2010','dd/mm/yyyy'))) and
	(:old.dt_atualizacao <> :new.dt_atualizacao) then

	select	max(dt_fechamento),
		max(nr_recibo)
	into	dt_fechamento_w,
		nr_recibo_w
	from	caixa_receb
	where	nr_sequencia	= :old.nr_seq_caixa_rec;

	if	(dt_fechamento_w is not null) then
		/* Nao e possivel excluir a baixa.
		O recibo #@NR_RECIBO#@ ja foi fechado!*/
		wheb_mensagem_pck.exibir_mensagem_abort(267076, 'NR_RECIBO=' || nr_recibo_w);
	end if;
end if;

/* Controle Bancario e Tesouraria */
select	max(a.nr_seq_banco),
	max(a.nr_seq_caixa)
into 	nr_seq_banco_w,
	nr_seq_caixa_w
from 	movto_trans_financ a
where 	a.nr_sequencia	= :new.nr_seq_movto_trans_fin;

if	(:new.dt_recebimento <> :old.dt_recebimento) then
	/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento),:new.dt_recebimento);
end if;

/* abatimento */
select	count(*)
into	qt_tit_pagar_w
from	titulo_pagar_baixa a
where	a.nr_seq_baixa_rec	= :new.nr_sequencia
and	a.nr_tit_receber	= :new.nr_titulo;

if	(:new.nr_seq_pls_lote_contest is not null) then
	ie_origem_baixa_w	:= 'CT';
elsif	(:new.nr_seq_liq_origem is not null) then
	ie_origem_baixa_w	:= 'ET';
elsif	(nvl(qt_tit_pagar_w,0)	> 0) then
	ie_origem_baixa_w	:= 'AB';
elsif	(:new.nr_bordero is not null) then
	ie_origem_baixa_w	:= 'BR';
elsif	(:new.nr_seq_movto_trans_fin is not null) and
	(nr_seq_banco_w is not null) then
	ie_origem_baixa_w	:= 'CB';
elsif	(:new.nr_seq_cobranca is not null) then
	ie_origem_baixa_w	:= 'CE';
elsif	(:new.nr_seq_retorno is not null) then
	ie_origem_baixa_w	:= 'CR';
elsif	(:new.nr_adiantamento is not null) then
	ie_origem_baixa_w	:= 'DA';
elsif	(:new.nr_seq_deposito_ident is not null) then
	ie_origem_baixa_w	:= 'DI';
elsif	(:new.nr_seq_lote_hist_guia is not null) then
	ie_origem_baixa_w	:= 'GR';
elsif	(:new.nr_seq_negociacao_cr is not null) then
	ie_origem_baixa_w	:= 'NR';
elsif	(:new.nr_seq_caixa_rec is not null) then
	ie_origem_baixa_w	:= 'RC';
elsif	(:new.nr_repasse_terceiro is not null) then
	ie_origem_baixa_w	:= 'RT';
elsif	(:new.nr_seq_movto_trans_fin is not null) and
	(nr_seq_caixa_w is not null) then
	ie_origem_baixa_w	:= 'TS';
else
	ie_origem_baixa_w	:= 'MR';
end if;

if	(ie_origem_baixa_w is not null) then
	:new.ie_origem_baixa	:= ie_origem_baixa_w;
end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_liq_ctb_onl
after insert or update ON TASY.TITULO_RECEBER_LIQ for each row
declare

ie_contab_cr_w          ctb_param_lote_tesouraria.ie_contab_cr%type;
cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type;
nr_multiplicador_w      number(1);
vl_movimento_w          ctb_documento.vl_movimento%type;
dt_movimento_w          ctb_documento.dt_competencia%type;
nr_seq_trans_financ_w   ctb_documento.nr_seq_trans_financ%type;
nr_seq_info_w           ctb_documento.nr_seq_info%type;
nr_documento_w          ctb_documento.nr_documento%type;
nr_seq_doc_compl_w      ctb_documento.nr_seq_doc_compl%type;
nr_doc_analitico_w      ctb_documento.nr_doc_analitico%type;
nm_tabela_w             ctb_documento.nm_tabela%type;
qt_registros_w          number(10);
ie_pls_w                number(10);

cursor 	c01 is
select  a.nm_atributo,
        10 cd_tipo_lote_contab
from    atributo_contab a
where   (a.cd_tipo_lote_contab = 5
and     a.nm_atributo in (  'VL_DESCONTOS', 'VL_JUROS', 'VL_MULTA',
                            'VL_DESPESA_BANCARIA',  'VL_ORIGINAL_BAIXA',
                            'VL_CAMBIAL_ATIVO', 'VL_CAMBIAL_PASSIVO' ))
or      (a.cd_tipo_lote_contab = 10
and     a.nm_atributo in (  'VL_RECEBIDO_TESOURARIA' ));

c01_w c01%rowtype;

begin

select  cd_estabelecimento
into    cd_estabelecimento_w
from    titulo_receber
where   nr_titulo = :new.nr_titulo;

begin
select  x.ie_contab_cr
into    ie_contab_cr_w
from(
    select  nvl(ie_contab_cr,'N') ie_contab_cr
    from    ctb_param_lote_tesouraria a
    where   a.cd_empresa    = obter_empresa_estab(cd_estabelecimento_w)
    and     nvl(a.cd_estab_exclusivo, cd_estabelecimento_w) = cd_estabelecimento_w
order by nvl(a.cd_estab_exclusivo,0) desc) x
where  rownum = 1;
exception
  when others then
    ie_contab_cr_w := 'N';
end;

select  count(1)
into    ie_pls_w
from    titulo_receber t
where   t.nr_titulo = :new.nr_titulo
and not exists (select  1
                from    movto_trans_financ x
                where   x.nr_sequencia = :new.nr_seq_movto_trans_fin)
and exists (select  1
            from    pls_titulo_rec_liq_mens x
            where   x.nr_titulo = :new.nr_titulo
            and     x.nr_seq_baixa = :new.nr_sequencia)
and exists (select  1
            from    pls_mensalidade x,
                    titulo_receber  y
            where   y.nr_seq_mensalidade = x.nr_sequencia
            and     y.nr_titulo = :new.nr_titulo );

if (    ie_contab_cr_w = 'S'
        and ie_pls_w = 0
        and :new.ie_lib_caixa = 'S'
        and :new.nr_seq_conta_banco is null
        and :new.nr_seq_lote_enc_contas is null /* Francisco - 30/10/2010 - Vai entrar em outro lote */
        and :new.nr_seq_pls_lote_camara is null /* Francisco - 30/10/2010 - Vai entrar em outro lote */
        and :new.nr_seq_movto_trans_fin is not null -- Edgar 27/05/2008, OS 93779
        ) then

        begin
        open c01;
        loop
        fetch c01 into
                c01_w;
        exit when c01%notfound;
          begin

            nr_multiplicador_w := 1;

            if  (:new.ie_acao <> 'I') then
              nr_multiplicador_w := -1;
            end if;

            vl_movimento_w:=        case c01_w.nm_atributo
                        when 'VL_DESCONTOS' then :new.vl_descontos * nr_multiplicador_w
                        when 'VL_JUROS' then :new.vl_juros * nr_multiplicador_w
                        when 'VL_MULTA'  then :new.vl_multa * nr_multiplicador_w
                        when 'VL_DESPESA_BANCARIA'  then :new.vl_despesa_bancaria * nr_multiplicador_w
                        when 'VL_RECEBIDO_TESOURARIA'  then :new.vl_recebido * nr_multiplicador_w
                        when 'VL_ORIGINAL_BAIXA'  then :new.vl_recebido + :new.vl_juros + :new.vl_multa + :new.vl_descontos
                        when 'VL_CAMBIAL_ATIVO'  then :new.vl_cambial_ativo * nr_multiplicador_w
                        when 'VL_CAMBIAL_PASSIVO'  then :new.vl_cambial_passivo * nr_multiplicador_w
                        end;

            dt_movimento_w          := :new.dt_recebimento;
            nr_seq_trans_financ_w   := :new.nr_seq_trans_fin;
            nr_seq_info_w           := 14;
            nr_documento_w          := :new.nr_titulo;
            nr_seq_doc_compl_w      := :new.nr_sequencia;
            nr_doc_analitico_w      := null;
            nm_tabela_w             := 'TITULO_RECEBER_LIQ_CONTAB_V2';

            select count(1)
            into   qt_registros_w
            from   ctb_documento a
            where  a.nr_documento = nr_documento_w
            and    a.nr_seq_doc_compl = nr_seq_doc_compl_w
            and    a.cd_tipo_lote_contabil = c01_w.cd_tipo_lote_contab
            and    a.cd_estabelecimento = cd_estabelecimento_w
            and    a.dt_competencia = dt_movimento_w
            and    a.nr_seq_trans_financ = nr_seq_trans_financ_w
            and    a.nr_seq_info = nr_seq_info_w
            and    a.nm_tabela = nm_tabela_w
            and    a.nm_atributo = c01_w.nm_atributo
            and    a.vl_movimento = vl_movimento_w;

            if     (qt_registros_w = 0) and (nvl(vl_movimento_w, 0) <> 0 and nvl(nr_seq_trans_financ_w, 0) <> 0) then
                   begin

                   ctb_concil_financeira_pck.ctb_gravar_documento  (   cd_estabelecimento_w,
                                                                       dt_movimento_w,
                                                                       c01_w.cd_tipo_lote_contab,
                                                                       nr_seq_trans_financ_w,
                                                                       nr_seq_info_w,
                                                                       nr_documento_w,
                                                                       nr_seq_doc_compl_w,
                                                                       nr_doc_analitico_w,
                                                                       vl_movimento_w,
                                                                       nm_tabela_w,
                                                                       c01_w.nm_atributo,
                                                                       :new.nm_usuario);
                   end;
            end if;
          end;
        end loop;
        close c01;
        end;
end if;

end;
/


ALTER TABLE TASY.TITULO_RECEBER_LIQ ADD (
  CONSTRAINT TITRELI_PK
 PRIMARY KEY
 (NR_TITULO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          3M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_RECEBER_LIQ ADD (
  CONSTRAINT TIRELIQ_CRTCOMP_FK 
 FOREIGN KEY (NR_SEQ_CARTA) 
 REFERENCES TASY.CARTA_COMPROMISSO (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_CONTAB_ANTECIP) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TIRELIQ_LOTCONT_FK3 
 FOREIGN KEY (NR_LOTE_CONTAB_PRO_RATA) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TIRELIQ_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NOTA_FISCAL_PARC) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_REEMBOLSO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_MOGLOSA_FK 
 FOREIGN KEY (NR_SEQ_MOT_GLOSA) 
 REFERENCES TASY.MOTIVO_GLOSA (CD_MOTIVO_GLOSA),
  CONSTRAINT TIRELIQ_REPTERC_FK 
 FOREIGN KEY (NR_REPASSE_TERCEIRO) 
 REFERENCES TASY.REPASSE_TERCEIRO (NR_REPASSE_TERCEIRO),
  CONSTRAINT TIRELIQ_LOTENCO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ENC_CONTAS) 
 REFERENCES TASY.LOTE_ENCONTRO_CONTAS (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_PLSLOCC_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_CAMARA) 
 REFERENCES TASY.PLS_LOTE_CAMARA_COMP (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_LOTAUDGUI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_HIST_GUIA) 
 REFERENCES TASY.LOTE_AUDIT_HIST_GUIA (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_PLSPAIT_FK 
 FOREIGN KEY (NR_SEQ_PAG_ITEM) 
 REFERENCES TASY.PLS_PAGAMENTO_ITEM (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_PLSLCTE_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_COB_TERC) 
 REFERENCES TASY.PLS_LOTE_COB_TERCEIRO (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_PLSLOCO_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_CONTEST) 
 REFERENCES TASY.PLS_LOTE_CONTESTACAO (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_PLSLODI_FK 
 FOREIGN KEY (NR_SEQ_PLS_LOTE_DISC) 
 REFERENCES TASY.PLS_LOTE_DISCUSSAO (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_DEPIDEN_FK 
 FOREIGN KEY (NR_SEQ_DEPOSITO_IDENT) 
 REFERENCES TASY.DEPOSITO_IDENTIFICADO (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_PLSPAGAMO_FK 
 FOREIGN KEY (NR_SEQ_AMORTIZACAO) 
 REFERENCES TASY.PLS_PAGADOR_AMORTIZACAO (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_BORRECE_FK 
 FOREIGN KEY (NR_BORDERO) 
 REFERENCES TASY.BORDERO_RECEBIMENTO (NR_BORDERO),
  CONSTRAINT TIRELIQ_COPAREHI_FK 
 FOREIGN KEY (NR_SEQ_CONPACI_RET_HIST) 
 REFERENCES TASY.CONTA_PACIENTE_RET_HIST (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_NEGOCCR_FK 
 FOREIGN KEY (NR_SEQ_NEGOCIACAO_CR) 
 REFERENCES TASY.NEGOCIACAO_CR (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_COBESCR_FK 
 FOREIGN KEY (NR_SEQ_COBRANCA) 
 REFERENCES TASY.COBRANCA_ESCRITURAL (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_MOVTRFI_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_TRANS_FIN) 
 REFERENCES TASY.MOVTO_TRANS_FINANC (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_MOTDESC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DESC) 
 REFERENCES TASY.MOTIVO_DESCONTO (NR_SEQUENCIA),
  CONSTRAINT TITRELI_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT TITRELI_TIPRECE_FK 
 FOREIGN KEY (CD_TIPO_RECEBIMENTO) 
 REFERENCES TASY.TIPO_RECEBIMENTO (CD_TIPO_RECEBIMENTO),
  CONSTRAINT TITRELI_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT TIRELIQ_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITRELI_ADIANTA_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO (NR_ADIANTAMENTO),
  CONSTRAINT TIRELIQ_CONRETO_FK 
 FOREIGN KEY (NR_SEQ_RETORNO) 
 REFERENCES TASY.CONVENIO_RETORNO (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_CONREIT_FK 
 FOREIGN KEY (NR_SEQ_RET_ITEM) 
 REFERENCES TASY.CONVENIO_RETORNO_ITEM (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_CAIREC_FK 
 FOREIGN KEY (NR_SEQ_CAIXA_REC) 
 REFERENCES TASY.CAIXA_RECEB (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_TRAFINA_FK2 
 FOREIGN KEY (NR_SEQ_TRANS_CAIXA) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TIRELIQ_CENCUST_FK2 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TIRELIQ_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO_DESC) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TIRELIQ_PLSSRCF_FK 
 FOREIGN KEY (NR_SEQ_RESC_FIN) 
 REFERENCES TASY.PLS_SOLIC_RESCISAO_FIN (NR_SEQUENCIA));

GRANT SELECT ON TASY.TITULO_RECEBER_LIQ TO NIVEL_1;

