ALTER TABLE TASY.SAN_ENVIO_EMAIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_ENVIO_EMAIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_ENVIO_EMAIL
(
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_REMETENTE         VARCHAR2(255 BYTE)       NOT NULL,
  DS_TITULO_EMAIL      VARCHAR2(255 BYTE)       NOT NULL,
  DS_MENSAGEM          VARCHAR2(2000 BYTE)      NOT NULL,
  IE_MOMENTO           VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQUENCIA         NUMBER(10),
  DS_ARQUIVO           VARCHAR2(80 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANENEM_PK ON TASY.SAN_ENVIO_EMAIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SAN_ENVIO_EMAIL ADD (
  CONSTRAINT SANENEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SAN_ENVIO_EMAIL TO NIVEL_1;

