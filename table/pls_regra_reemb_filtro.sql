ALTER TABLE TASY.PLS_REGRA_REEMB_FILTRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_REEMB_FILTRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_REEMB_FILTRO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  NM_FILTRO            VARCHAR2(255 BYTE)       NOT NULL,
  IE_FILTRO_BENEF      VARCHAR2(1 BYTE)         NOT NULL,
  IE_FILTRO_ATEND      VARCHAR2(1 BYTE)         NOT NULL,
  IE_FILTRO_PROC       VARCHAR2(1 BYTE)         NOT NULL,
  IE_FILTRO_MAT        VARCHAR2(1 BYTE)         NOT NULL,
  IE_FILTRO_QTD        VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRRFI_PK ON TASY.PLS_REGRA_REEMB_FILTRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRRFI_PLSRREE_FK_I ON TASY.PLS_REGRA_REEMB_FILTRO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_REEMB_FILTRO_UPDATE
before insert or update ON TASY.PLS_REGRA_REEMB_FILTRO for each row
declare

begin

if	(:new.ie_filtro_proc = 'S' and :new.ie_filtro_mat = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(323503);
end if;

end;
/


ALTER TABLE TASY.PLS_REGRA_REEMB_FILTRO ADD (
  CONSTRAINT PLSRRFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_REEMB_FILTRO ADD (
  CONSTRAINT PLSRRFI_PLSRREE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_REEMBOLSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_REEMB_FILTRO TO NIVEL_1;

