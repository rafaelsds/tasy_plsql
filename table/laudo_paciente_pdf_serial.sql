ALTER TABLE TASY.LAUDO_PACIENTE_PDF_SERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAUDO_PACIENTE_PDF_SERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAUDO_PACIENTE_PDF_SERIAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ACESSO_DICOM      VARCHAR2(20 BYTE),
  NR_SEQ_LAUDO         NUMBER(10),
  DS_PDF_SERIAL        LONG                     NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  CD_PROCEDIMENTO      VARCHAR2(20 BYTE),
  DT_LAUDO             DATE,
  CD_STATUS            VARCHAR2(2 BYTE),
  DS_COMPLEMENTO       VARCHAR2(255 BYTE),
  NR_PRESCRICAO        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LAUDPDFSE_I1 ON TASY.LAUDO_PACIENTE_PDF_SERIAL
(NR_ACESSO_DICOM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUDPDFSE_I1
  MONITORING USAGE;


CREATE INDEX TASY.LAUDPDFSE_LAUPACI_FK_I ON TASY.LAUDO_PACIENTE_PDF_SERIAL
(NR_SEQ_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUDPDFSE_LAUPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LAUDPDFSE_PESFISI_FK_I ON TASY.LAUDO_PACIENTE_PDF_SERIAL
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LAUDPDFSE_PK ON TASY.LAUDO_PACIENTE_PDF_SERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LAUDPDFSE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.laudo_pac_pdf_serial_insert
after insert ON TASY.LAUDO_PACIENTE_PDF_SERIAL for each row
declare

acao_w                 varchar2(11);
total_err_w            Number(3);
qt_reg_w		       number(1);
nr_seq_laudo_w         number(10);
nr_prescricao_w        number(14);
nr_seq_prescricao_w    number(6);
nm_usuario_liberacao_w varchar2(15);

cursor c01 is
select nr_prescricao, nr_seq_prescricao, nm_usuario_liberacao
  from laudo_paciente
 where nr_sequencia = nr_seq_laudo_w;

Begin
  if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
    Goto Final;
  end if;

  nr_seq_laudo_w := :new.nr_seq_laudo;

  If ((Wheb_Usuario_Pck.Is_Evento_Ativo(766) = 'S') and (:new.ds_complemento = 'PEPUNIMEDW')) Then
    OPEN c01;
      LOOP
        FETCH c01 INTO
          nr_prescricao_w,
          nr_seq_prescricao_w,
          nm_usuario_liberacao_w;
        EXIT WHEN c01%NOTFOUND;
        BEGIN
          If (nm_usuario_liberacao_w is not null) Then
              acao_w := 'Registro';

              select count(1)
                into total_err_w
                from prescr_proced_info_ext
               where cd_mensagem_ext = 'PEPUNIMEDW'
                 and nr_prescricao = nr_prescricao_w
                 and nr_seq_prescr = nr_seq_prescricao_w;

              if (total_err_w > 0) then
                  delete from prescr_proced_info_ext
                   where cd_mensagem_ext = 'PEPUNIMEDW'
                     and nr_prescricao = nr_prescricao_w
                     and nr_seq_prescr = nr_seq_prescricao_w;
              end if;

              Integrar_Unimed_Rs_Ws(766, nr_prescricao_w, nr_seq_prescricao_w, nm_usuario_liberacao_w, Acao_W);
          End If;
        END;
      END LOOP;
    CLOSE c01;
  End If;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.LAUDO_PACIENTE_PDF_SERIAL ADD (
  CONSTRAINT LAUDPDFSE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAUDO_PACIENTE_PDF_SERIAL ADD (
  CONSTRAINT LAUDPDFSE_LAUPACI_FK 
 FOREIGN KEY (NR_SEQ_LAUDO) 
 REFERENCES TASY.LAUDO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LAUDPDFSE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.LAUDO_PACIENTE_PDF_SERIAL TO NIVEL_1;

