ALTER TABLE TASY.W_CTB_DISTR_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_CTB_DISTR_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CTB_DISTR_CONTA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_LOTE_CONTABIL     NUMBER(10),
  VL_MOVIMENTO         NUMBER(15,2),
  CD_HISTORICO         NUMBER(10),
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE),
  NR_SEQ_AGRUPAMENTO   NUMBER(10),
  CD_CENTRO_CUSTO      NUMBER(8)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WCTBDICON_PK ON TASY.W_CTB_DISTR_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_CTB_DISTR_CONTA ADD (
  CONSTRAINT WCTBDICON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_CTB_DISTR_CONTA TO NIVEL_1;

