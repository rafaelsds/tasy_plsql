ALTER TABLE TASY.PLS_ATUARIAL_ARQ_CAMPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ATUARIAL_ARQ_CAMPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ATUARIAL_ARQ_CAMPO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGISTRO      NUMBER(10)               NOT NULL,
  IE_CAMPO             VARCHAR2(255 BYTE),
  IE_ALINHAMENTO       VARCHAR2(5 BYTE),
  IE_PADRAO            VARCHAR2(255 BYTE),
  IE_TAMANHO           NUMBER(10),
  DS_PREENCHIMENTO     VARCHAR2(5 BYTE),
  NR_ORDEM             NUMBER(10)               NOT NULL,
  DS_TITULO            VARCHAR2(255 BYTE),
  DS_MASCARA           VARCHAR2(255 BYTE),
  DS_FUNCTION          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSAACP_PK ON TASY.PLS_ATUARIAL_ARQ_CAMPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAACP_PLSAARG_FK_I ON TASY.PLS_ATUARIAL_ARQ_CAMPO
(NR_SEQ_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_atuarial_arq_campo_atual
before insert or update ON TASY.PLS_ATUARIAL_ARQ_CAMPO for each row
begin

-- Ao atualizar ou inserir um novo campo, a regra inteira ser� marcada como inconsistente
update	pls_atuarial_arq_regra
set	ie_consistente		= 'N',
	dt_atualizacao		= sysdate,
	nm_usuario		= :new.nm_usuario
where	nr_sequencia		= (	select	max(x.nr_seq_regra)
					from	pls_atuarial_arq_reg	x
					where	x.nr_sequencia		= :new.nr_seq_registro);


end pls_atuarial_arq_campo_atual;
/


ALTER TABLE TASY.PLS_ATUARIAL_ARQ_CAMPO ADD (
  CONSTRAINT PLSAACP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ATUARIAL_ARQ_CAMPO ADD (
  CONSTRAINT PLSAACP_PLSAARG_FK 
 FOREIGN KEY (NR_SEQ_REGISTRO) 
 REFERENCES TASY.PLS_ATUARIAL_ARQ_REG (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ATUARIAL_ARQ_CAMPO TO NIVEL_1;

