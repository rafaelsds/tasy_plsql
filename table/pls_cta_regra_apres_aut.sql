ALTER TABLE TASY.PLS_CTA_REGRA_APRES_AUT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CTA_REGRA_APRES_AUT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CTA_REGRA_APRES_AUT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_MAX_CONTA         NUMBER(10)               NOT NULL,
  IE_FORMA_GERACAO     VARCHAR2(5 BYTE)         NOT NULL,
  NR_DIA_GERACAO       NUMBER(2),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  IE_REFERENCIA_DATA   VARCHAR2(2 BYTE),
  IE_DATA_COMPETENCIA  VARCHAR2(5 BYTE),
  DT_INICIO_FLUXO      NUMBER(2),
  DT_FIM_FLUXO         NUMBER(2),
  DT_EXEC_PROC         DATE,
  IE_TIPO_GUIA         VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCRAA_ESTABEL_FK_I ON TASY.PLS_CTA_REGRA_APRES_AUT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCRAA_PK ON TASY.PLS_CTA_REGRA_APRES_AUT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CTA_REGRA_APRES_AUT_tp  after update ON TASY.PLS_CTA_REGRA_APRES_AUT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_REFERENCIA_DATA,1,4000),substr(:new.IE_REFERENCIA_DATA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REFERENCIA_DATA',ie_log_w,ds_w,'PLS_CTA_REGRA_APRES_AUT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_cta_regra_apres_aut_atual
before update or insert or delete ON TASY.PLS_CTA_REGRA_APRES_AUT for each row
declare

ie_acao_w		varchar2(255);
nr_mes_w		pls_integer := 0;
dt_execucao_w		date;
dt_proxima_exec_w	varchar2(255);
hr_exec_w		varchar2(10);

begin
if	(inserting) then
	ie_acao_w := 'I';

elsif	(updating) then
	ie_acao_w := 'U';

elsif	(deleting) then
	ie_acao_w := 'D';
end if;

if	(:new.dt_exec_proc is not null) then
	hr_exec_w := to_char(:new.dt_exec_proc, 'hh24:mi:ss');
else
	hr_exec_w := to_char((fim_dia(sysdate) - 1/3072), 'hh24:mi:ss');
end if;

if	(:new.ie_forma_geracao = 'M') then

	if	(((trunc(sysdate,'month') + :new.nr_dia_geracao) - 1) < trunc(sysdate)) then
		nr_mes_w := + 1;
	end if;

	dt_execucao_w		:= to_date(to_char(to_char(add_months(((trunc(sysdate,'month') + :new.nr_dia_geracao) - 1),nr_mes_w),'dd/mm/yyyy') || ' ' || hr_exec_w), 'dd/mm/yyyy hh24:mi:ss');
	dt_proxima_exec_w	:= 'add_months(to_date(to_char(to_char(add_months(((trunc(sysdate,''month'') + ' || :new.nr_dia_geracao || ') - 1),' || nr_mes_w || '),''dd/mm/yyyy'') || '' '' || ''' || hr_exec_w || '''), ''dd/mm/yyyy hh24:mi:ss''),1)';

elsif	(:new.ie_forma_geracao = 'D') then

	dt_execucao_w 		:= to_date((to_char(sysdate,'dd/mm/yyyy') || ' ' || hr_exec_w),'dd/mm/yyyy hh24:mi:ss');
	dt_proxima_exec_w	:= 'to_date((to_char(sysdate,''dd/mm/yyyy'') || '' '' || ''' || hr_exec_w || '''),''dd/mm/yyyy hh24:mi:ss'') + 1';
end if;

pls_cta_gerar_job_regra( 	ie_acao_w,
				'PLS_CARREGA_GUIAS_LOTE_APRES',
				'PLS_CARREGA_GUIAS_LOTE_APRES('''|| to_char(:new.nr_sequencia) || ''', ''' || to_char(:new.cd_estabelecimento) || ''', ''' || :new.nm_usuario_nrec || ''');',
				dt_execucao_w,
				dt_proxima_exec_w,
				:new.ie_situacao);
end;
/


ALTER TABLE TASY.PLS_CTA_REGRA_APRES_AUT ADD (
  CONSTRAINT PLSCRAA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CTA_REGRA_APRES_AUT ADD (
  CONSTRAINT PLSCRAA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_CTA_REGRA_APRES_AUT TO NIVEL_1;

