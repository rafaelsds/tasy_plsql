ALTER TABLE TASY.GPR_RESPONSE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPR_RESPONSE CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPR_RESPONSE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_PARTICIPANT_TYPE  VARCHAR2(10 BYTE),
  CD_PARTICIPANT       VARCHAR2(10 BYTE),
  NM_PARTICIPANT       VARCHAR2(40 BYTE),
  NR_PAR_NUM           VARCHAR2(20 BYTE),
  DT_UPDATE            DATE,
  CD_CAPABILITY        VARCHAR2(10 BYTE),
  NM_CAPABILITY        VARCHAR2(60 BYTE),
  CD_SEC_CAPABILITY    VARCHAR2(10 BYTE),
  CD_CAPABILITY_VER    VARCHAR2(10 BYTE),
  CD_CAPABILITY_ROLE   VARCHAR2(10 BYTE),
  IE_SITUATION         VARCHAR2(1 BYTE),
  DS_NOTES             CLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.GPRRESP_PK ON TASY.GPR_RESPONSE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GPR_RESPONSE ADD (
  CONSTRAINT GPRRESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.GPR_RESPONSE TO NIVEL_1;

