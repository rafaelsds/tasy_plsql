ALTER TABLE TASY.PTU_CAMARA_COMPENSACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_CAMARA_COMPENSACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_CAMARA_COMPENSACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_UNIMED_DESTINO    VARCHAR2(4 BYTE)         NOT NULL,
  CD_UNIMED_ORIGEM     VARCHAR2(4 BYTE)         NOT NULL,
  DT_GERACAO           DATE                     NOT NULL,
  DT_CAMARA            DATE                     NOT NULL,
  IE_TIPO_CAMARA       VARCHAR2(3 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_VERSAO_TRANSACAO  NUMBER(2),
  NR_SEQ_LOTE_CAMARA   NUMBER(10),
  NR_SEQ_ARQUIVO       NUMBER(10),
  DT_GERACAO_ARQUIVO   DATE,
  NM_USUARIO_ARQ       VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PTUCACO_I1 ON TASY.PTU_CAMARA_COMPENSACAO
(CD_UNIMED_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PTUCACO_PK ON TASY.PTU_CAMARA_COMPENSACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUCACO_PK
  MONITORING USAGE;


CREATE INDEX TASY.PTUCACO_PLSLOCC_FK_I ON TASY.PTU_CAMARA_COMPENSACAO
(NR_SEQ_LOTE_CAMARA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUCACO_PLSLOCC_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PTU_CAMARA_COMPENSACAO ADD (
  CONSTRAINT PTUCACO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_CAMARA_COMPENSACAO ADD (
  CONSTRAINT PTUCACO_PLSLOCC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_CAMARA) 
 REFERENCES TASY.PLS_LOTE_CAMARA_COMP (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_CAMARA_COMPENSACAO TO NIVEL_1;

