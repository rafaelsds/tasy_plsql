ALTER TABLE TASY.MAN_VEICULO_ENTREGADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_VEICULO_ENTREGADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_VEICULO_ENTREGADOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_VEICULO       NUMBER(10)               NOT NULL,
  NR_SEQ_ENTREGADOR    NUMBER(10)               NOT NULL,
  DT_INICIO            DATE,
  DT_FINAL             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANVEEN_EMEVEIC_FK_I ON TASY.MAN_VEICULO_ENTREGADOR
(NR_SEQ_VEICULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANVEEN_EMEVEIC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANVEEN_MANENTR_FK_I ON TASY.MAN_VEICULO_ENTREGADOR
(NR_SEQ_ENTREGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANVEEN_MANENTR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANVEEN_PK ON TASY.MAN_VEICULO_ENTREGADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANVEEN_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_VEICULO_ENTREGADOR ADD (
  CONSTRAINT MANVEEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_VEICULO_ENTREGADOR ADD (
  CONSTRAINT MANVEEN_MANENTR_FK 
 FOREIGN KEY (NR_SEQ_ENTREGADOR) 
 REFERENCES TASY.MAN_ENTREGADOR (NR_SEQUENCIA),
  CONSTRAINT MANVEEN_EMEVEIC_FK 
 FOREIGN KEY (NR_SEQ_VEICULO) 
 REFERENCES TASY.EME_VEICULO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_VEICULO_ENTREGADOR TO NIVEL_1;

