ALTER TABLE TASY.INTEROP_PRESCR_DICOM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTEROP_PRESCR_DICOM CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTEROP_PRESCR_DICOM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_ACESSO_DICOM         VARCHAR2(20 BYTE),
  NR_ACESSO_DICOM_ORIGEM  VARCHAR2(20 BYTE),
  NR_SEQ_ATEND_PRESCR     NUMBER(10)            NOT NULL,
  NR_SEQ_PRESCR_ORIGEM    NUMBER(10),
  NR_SEQ_PRESCR           NUMBER(10),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INTPREDIC_INTATEPRE_FK_I ON TASY.INTEROP_PRESCR_DICOM
(NR_SEQ_ATEND_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INTPREDIC_PK ON TASY.INTEROP_PRESCR_DICOM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.INTEROP_PRESCR_DICOM_tp  after update ON TASY.INTEROP_PRESCR_DICOM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_ACESSO_DICOM,1,4000),substr(:new.NR_ACESSO_DICOM,1,4000),:new.nm_usuario,nr_seq_w,'NR_ACESSO_DICOM',ie_log_w,ds_w,'INTEROP_PRESCR_DICOM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRESCR,1,4000),substr(:new.NR_SEQ_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESCR',ie_log_w,ds_w,'INTEROP_PRESCR_DICOM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PRESCR_ORIGEM,1,4000),substr(:new.NR_SEQ_PRESCR_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PRESCR_ORIGEM',ie_log_w,ds_w,'INTEROP_PRESCR_DICOM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ATEND_PRESCR,1,4000),substr(:new.NR_SEQ_ATEND_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ATEND_PRESCR',ie_log_w,ds_w,'INTEROP_PRESCR_DICOM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.INTEROP_PRESCR_DICOM ADD (
  CONSTRAINT INTPREDIC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTEROP_PRESCR_DICOM ADD (
  CONSTRAINT INTPREDIC_INTATEPRE_FK 
 FOREIGN KEY (NR_SEQ_ATEND_PRESCR) 
 REFERENCES TASY.INTEROP_ATEND_PRESCR (NR_SEQUENCIA));

GRANT SELECT ON TASY.INTEROP_PRESCR_DICOM TO NIVEL_1;

