ALTER TABLE TASY.W_RESULT_EXAME_OBS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_RESULT_EXAME_OBS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_RESULT_EXAME_OBS
(
  NR_SEQUENCIA   NUMBER(10)                     NOT NULL,
  NM_USUARIO     VARCHAR2(15 BYTE)              NOT NULL,
  NR_SEQ_RESULT  NUMBER(10),
  DT_RESULTADO   DATE,
  DS_OBSERVACAO  VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WRESEXO_PK ON TASY.W_RESULT_EXAME_OBS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WRESEXO_PK
  MONITORING USAGE;


CREATE INDEX TASY.WRESEXO_WRESEXG_FK_I ON TASY.W_RESULT_EXAME_OBS
(NR_SEQ_RESULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_RESULT_EXAME_OBS ADD (
  CONSTRAINT WRESEXO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_RESULT_EXAME_OBS ADD (
  CONSTRAINT WRESEXO_WRESEXG_FK 
 FOREIGN KEY (NR_SEQ_RESULT) 
 REFERENCES TASY.W_RESULT_EXAME_GRID (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_RESULT_EXAME_OBS TO NIVEL_1;

