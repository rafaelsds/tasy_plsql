ALTER TABLE TASY.PLS_ATU_CONTA_REGRA_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ATU_CONTA_REGRA_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ATU_CONTA_REGRA_CONTA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_REGRA             NUMBER(10)           NOT NULL,
  NR_SEQ_TIPO_ATENDIMENTO  NUMBER(10),
  IE_STATUS                VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSACRT_PK ON TASY.PLS_ATU_CONTA_REGRA_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSACRT_PLSAACR_FK_I ON TASY.PLS_ATU_CONTA_REGRA_CONTA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSACRT_PLSTIAT_FK_I ON TASY.PLS_ATU_CONTA_REGRA_CONTA
(NR_SEQ_TIPO_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_ATU_CONTA_REGRA_CONTA ADD (
  CONSTRAINT PLSACRT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ATU_CONTA_REGRA_CONTA ADD (
  CONSTRAINT PLSACRT_PLSAACR_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_ATU_CONTA_REGRA (NR_SEQUENCIA),
  CONSTRAINT PLSACRT_PLSTIAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ATENDIMENTO) 
 REFERENCES TASY.PLS_TIPO_ATENDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ATU_CONTA_REGRA_CONTA TO NIVEL_1;

