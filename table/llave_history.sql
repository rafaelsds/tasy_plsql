ALTER TABLE TASY.LLAVE_HISTORY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LLAVE_HISTORY CASCADE CONSTRAINTS;

CREATE TABLE TASY.LLAVE_HISTORY
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_LLAVE             VARCHAR2(256 BYTE)       NOT NULL,
  DT_INICIAL           DATE                     NOT NULL,
  DT_FIM               DATE,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_MEDICO            VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LLAHIS_PESFISI_FK_I ON TASY.LLAVE_HISTORY
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LLAHIS_PK ON TASY.LLAVE_HISTORY
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LLAVE_HISTORY ADD (
  CONSTRAINT LLAHIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.LLAVE_HISTORY ADD (
  CONSTRAINT LLAHIS_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LLAVE_HISTORY TO NIVEL_1;

