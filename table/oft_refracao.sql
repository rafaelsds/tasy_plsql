ALTER TABLE TASY.OFT_REFRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OFT_REFRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.OFT_REFRACAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_CONSULTA            NUMBER(10)         NOT NULL,
  DT_EXAME                   DATE,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  VL_OD_PL_ARD_ESF           NUMBER(5,2),
  VL_OD_PL_ARD_CIL           NUMBER(5,2),
  VL_OD_PL_ARD_EIXO          NUMBER(3),
  VL_OD_PL_ARD_AV_SC         NUMBER(10),
  VL_OD_PL_ARD_AV_CC         NUMBER(10),
  VL_OD_ARD_DNP              NUMBER(5,2),
  VL_ARD_DP                  NUMBER(5,2),
  VL_OE_PL_ARD_ESF           NUMBER(5,2),
  VL_OE_PL_ARD_CIL           NUMBER(5,2),
  VL_OE_PL_ARD_EIXO          NUMBER(3),
  VL_OE_PL_ARD_AV_SC         NUMBER(10),
  VL_OE_PL_ARD_AV_CC         NUMBER(10),
  VL_OE_ARD_DNP              NUMBER(5,2),
  VL_OD_PL_ARE_ESF           NUMBER(5,2),
  VL_OD_PL_ARE_CIL           NUMBER(5,2),
  VL_OD_PL_ARE_EIXO          NUMBER(3),
  VL_OD_PL_ARE_AV_SC         NUMBER(10),
  VL_OD_PL_ARE_AV_CC         NUMBER(10),
  VL_OE_PL_ARE_ESF           NUMBER(5,2),
  VL_OE_PL_ARE_CIL           NUMBER(5,2),
  VL_OE_PL_ARE_EIXO          NUMBER(3),
  VL_OE_PL_ARE_AV_SC         NUMBER(10),
  VL_OE_PL_ARE_AV_CC         NUMBER(3),
  VL_OD_PP_ARD_ESF           NUMBER(5,2),
  VL_OD_PP_ARD_CIL           NUMBER(5,2),
  VL_OD_PP_ARD_EIXO          NUMBER(3),
  VL_OD_PP_ARD_AV_SC         NUMBER(10),
  VL_OD_PP_ARD_AV_CC         NUMBER(10),
  VL_OE_PP_ARD_ESF           NUMBER(5,2),
  VL_OE_PP_ARD_CIL           NUMBER(5,2),
  VL_OE_PP_ARD_EIXO          NUMBER(3),
  VL_OE_PP_ARD_AV_SC         NUMBER(10),
  VL_OE_PP_ARD_AV_CC         NUMBER(10),
  VL_OD_PP_ARE_ESF           NUMBER(5,2),
  VL_OD_PP_ARE_CIL           NUMBER(5,2),
  VL_OD_PP_ARE_EIXO          NUMBER(3),
  VL_OD_PP_ARE_AV_SC         NUMBER(3),
  VL_OD_PP_ARE_AV_CC         NUMBER(3),
  VL_OE_PP_ARE_ESF           NUMBER(5,2),
  VL_OE_PP_ARE_CIL           NUMBER(5,2),
  VL_OE_PP_ARE_EIXO          NUMBER(3),
  VL_OE_PP_ARE_AV_SC         NUMBER(3),
  VL_OE_PP_ARE_AV_CC         NUMBER(3),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  VL_ADICAO                  NUMBER(5,3),
  IE_RECEITA_DINAMICA        VARCHAR2(1 BYTE)   NOT NULL,
  IE_RECEITA_ESTATICA        VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  VL_OE_ADIC_AV              NUMBER(10),
  VL_OD_ADIC_AV              NUMBER(10),
  IE_REFRACAO_SUGERIDA       VARCHAR2(1 BYTE),
  IE_PLANO_RDL_OD            VARCHAR2(1 BYTE),
  IE_PLANO_RDL_OE            VARCHAR2(1 BYTE),
  IE_PLANO_RDP_OD            VARCHAR2(1 BYTE),
  IE_PLANO_RDP_OE            VARCHAR2(1 BYTE),
  IE_PLANO_REL_OD            VARCHAR2(1 BYTE),
  IE_PLANO_REL_OE            VARCHAR2(1 BYTE),
  IE_PLANO_REP_OD            VARCHAR2(1 BYTE),
  IE_PLANO_REP_OE            VARCHAR2(1 BYTE),
  VL_ADICAO_OE               NUMBER(5,3),
  DS_OBSERVACAO_OCULOS       VARCHAR2(1000 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_CONSULTA_FORM       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OFTREFR_OFCOFOR_FK_I ON TASY.OFT_REFRACAO
(NR_SEQ_CONSULTA_FORM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTREFR_OFTCONS_FK_I ON TASY.OFT_REFRACAO
(NR_SEQ_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTREFR_PESFISI_FK_I ON TASY.OFT_REFRACAO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OFTREFR_PK ON TASY.OFT_REFRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OFTREFR_PK
  MONITORING USAGE;


CREATE INDEX TASY.OFTREFR_TASASDI_FK_I ON TASY.OFT_REFRACAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.OFTREFR_TASASDI_FK2_I ON TASY.OFT_REFRACAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.oft_refracao_pend_atual
after insert or update or delete ON TASY.OFT_REFRACAO for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

qt_reg_w	               number(1);
ie_tipo_w		            varchar2(10);
cd_pessoa_fisica_w	varchar2(30);
nr_atendimento_w    number(10);
ie_libera_exames_oft_w	varchar2(5);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	nvl(max(ie_libera_exames_oft),'N')
into	ie_libera_exames_oft_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_exames_oft_w = 'S') then
	if	(inserting) or
		(updating) then

		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'REFL';
		elsif	(:old.dt_liberacao is null) and
               (:new.dt_liberacao is not null) then
               ie_tipo_w := 'XREFL';
		end if;

      select	max(a.nr_atendimento),
                  max(a.cd_pessoa_fisica)
      into	   nr_atendimento_w,
                  cd_pessoa_fisica_w
      from     oft_consulta a where a.nr_sequencia = :new.nr_seq_consulta;

		begin
		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
		end if;
		exception
			when others then
			null;
		end;

	elsif	(deleting) then
            delete 	from pep_item_pendente
            where 	IE_TIPO_REGISTRO = 'REFL'
            and	      nr_seq_registro = :old.nr_sequencia
            and	      nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

            commit;
    end if;

	commit;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.oft_refracao_bef_ins BEFORE
    INSERT OR UPDATE ON TASY.OFT_REFRACAO     FOR EACH ROW
DECLARE
    param141_w                VARCHAR2(1);
    param117_w                VARCHAR2(1);
    cd_perfil_ativo_w         NUMBER(5);
    cd_estabelecimento_w      NUMBER(5);
    
    FUNCTION oft_obter_mascara_campo_form (
        nr_seq_consulta_form_p   NUMBER,
        nm_atributo_p            VARCHAR2
    ) RETURN VARCHAR2 IS
        ie_mascara_dioptria_w oft_formulario_item.ie_mascara_dioptria%TYPE;
    BEGIN
        SELECT
            MAX(c.ie_mascara_dioptria)
        INTO ie_mascara_dioptria_w
        FROM
            oft_consulta_formulario   b,
            oft_formulario_item       c
        WHERE
            b.nr_seq_regra_form = c.nr_seq_regra_form
            AND b.nr_sequencia = nr_seq_consulta_form_p
            AND c.nm_atributo = nm_atributo_p;

        RETURN nvl(ie_mascara_dioptria_w, 'X');
    END;
    
    FUNCTION obter_valor_conversao (
        valor_campo_p   NUMBER
    ) RETURN VARCHAR2 IS
        retorno_w NUMBER(1);
    BEGIN
        retorno_w := 1;
        IF ( valor_campo_p > 0 ) THEN
            retorno_w := -1;
        END IF;
        RETURN retorno_w;
    END;
    
    FUNCTION obter_valor_conversao_form (
        valor_campo_p   NUMBER,
        ie_mascara_p    VARCHAR2,
        param117_p      VARCHAR2,
        param141_p      VARCHAR2,
        nm_atributo_p   VARCHAR2
    ) RETURN VARCHAR2 IS
        retorno_w NUMBER(1);
    BEGIN
        retorno_w := 1;
        IF ( valor_campo_p > 0 
            AND ie_mascara_p <> 'P'
            AND ( ( ie_mascara_p = 'N' ) 
            OR ( param117_p = 'D' AND param141_p = 'N' ) 
            OR ( param117_p = 'N' AND param141_p = 'N' ) 
            OR ( param117_p = 'N' AND param141_p = 'P' 
            AND ( UPPER(nm_atributo_p) IN ('VL_OD_PL_ARD_CIL', 'VL_OE_PL_ARD_CIL', 'VL_OD_PL_ARE_CIL', 'VL_OE_PL_ARE_CIL') ) ) ) ) THEN
            retorno_w := -1;
        END IF;

        RETURN retorno_w;
    END;

BEGIN

    IF ( wheb_usuario_pck.get_ie_executar_trigger = 'S' ) THEN
        cd_perfil_ativo_w       := wheb_usuario_pck.get_cd_perfil;
        cd_estabelecimento_w    := wheb_usuario_pck.get_cd_estabelecimento;
        obter_param_usuario(3010, 141, cd_perfil_ativo_w, :new.nm_usuario, cd_estabelecimento_w, param141_w);
        obter_param_usuario(3010, 117, cd_perfil_ativo_w, :new.nm_usuario, cd_estabelecimento_w, param117_w);
        
        IF ( :new.nr_seq_consulta_form IS NULL ) THEN
            IF ( ( param117_w = 'D' AND param141_w = 'N' ) OR ( param117_w = 'N' AND param141_w = 'N' ) ) THEN
                    :new.vl_od_pl_ard_esf := :new.vl_od_pl_ard_esf * obter_valor_conversao(:new.vl_od_pl_ard_esf);
                    :new.vl_od_pl_ard_cil := :new.vl_od_pl_ard_cil * obter_valor_conversao(:new.vl_od_pl_ard_cil);
                    :new.vl_oe_pl_ard_esf := :new.vl_oe_pl_ard_esf * obter_valor_conversao(:new.vl_oe_pl_ard_esf);
                    :new.vl_oe_pl_ard_cil := :new.vl_oe_pl_ard_cil * obter_valor_conversao(:new.vl_oe_pl_ard_cil);
                    :new.vl_od_pl_are_esf := :new.vl_od_pl_are_esf * obter_valor_conversao(:new.vl_od_pl_are_esf);
                    :new.vl_od_pl_are_cil := :new.vl_od_pl_are_cil * obter_valor_conversao(:new.vl_od_pl_are_cil);
                    :new.vl_oe_pl_are_esf := :new.vl_oe_pl_are_esf * obter_valor_conversao(:new.vl_oe_pl_are_esf);
                    :new.vl_oe_pl_are_cil := :new.vl_oe_pl_are_cil * obter_valor_conversao(:new.vl_oe_pl_are_cil);
            ELSIF ( param117_w = 'N' AND param141_w = 'P' ) THEN
                    :new.vl_od_pl_ard_cil := :new.vl_od_pl_ard_cil * obter_valor_conversao(:new.vl_od_pl_ard_cil);
                    :new.vl_oe_pl_ard_cil := :new.vl_oe_pl_ard_cil * obter_valor_conversao(:new.vl_oe_pl_ard_cil);
                    :new.vl_od_pl_are_cil := :new.vl_od_pl_are_cil * obter_valor_conversao(:new.vl_od_pl_are_cil);
                    :new.vl_oe_pl_are_cil := :new.vl_oe_pl_are_cil * obter_valor_conversao(:new.vl_oe_pl_are_cil);
            END IF;
            
        ELSE
            :new.vl_od_pl_ard_esf := :new.vl_od_pl_ard_esf * obter_valor_conversao_form(:new.vl_od_pl_ard_esf, oft_obter_mascara_campo_form
            (:new.nr_seq_consulta_form, 'VL_OD_PL_ARD_ESF'), param117_w, param141_w, 'VL_OD_PL_ARD_ESF');
            
            :new.vl_od_pl_ard_cil := :new.vl_od_pl_ard_cil * obter_valor_conversao_form(:new.vl_od_pl_ard_cil, oft_obter_mascara_campo_form
            (:new.nr_seq_consulta_form, 'VL_OD_PL_ARD_CIL'), param117_w, param141_w, 'VL_OD_PL_ARD_CIL');
    
            :new.vl_oe_pl_ard_esf := :new.vl_oe_pl_ard_esf * obter_valor_conversao_form(:new.vl_oe_pl_ard_esf, oft_obter_mascara_campo_form
            (:new.nr_seq_consulta_form, 'VL_OE_PL_ARD_ESF'), param117_w, param141_w, 'VL_OE_PL_ARD_ESF');
    
            :new.vl_oe_pl_ard_cil := :new.vl_oe_pl_ard_cil * obter_valor_conversao_form(:new.vl_oe_pl_ard_cil, oft_obter_mascara_campo_form
            (:new.nr_seq_consulta_form, 'VL_OE_PL_ARD_CIL'), param117_w, param141_w, 'VL_OE_PL_ARD_CIL');
    
            :new.vl_od_pl_are_esf := :new.vl_od_pl_are_esf * obter_valor_conversao_form(:new.vl_od_pl_are_esf, oft_obter_mascara_campo_form
            (:new.nr_seq_consulta_form, 'VL_OD_PL_ARE_ESF'), param117_w, param141_w, 'VL_OD_PL_ARE_ESF');
    
            :new.vl_od_pl_are_cil := :new.vl_od_pl_are_cil * obter_valor_conversao_form(:new.vl_od_pl_are_cil, oft_obter_mascara_campo_form
            (:new.nr_seq_consulta_form, 'VL_OD_PL_ARE_CIL'), param117_w, param141_w, 'VL_OD_PL_ARE_CIL');
    
            :new.vl_oe_pl_are_esf := :new.vl_oe_pl_are_esf * obter_valor_conversao_form(:new.vl_oe_pl_are_esf, oft_obter_mascara_campo_form
            (:new.nr_seq_consulta_form, 'VL_OE_PL_ARE_ESF'), param117_w, param141_w, 'VL_OE_PL_ARE_ESF');
    
            :new.vl_oe_pl_are_cil := :new.vl_oe_pl_are_cil * obter_valor_conversao_form(:new.vl_oe_pl_are_cil, oft_obter_mascara_campo_form
            (:new.nr_seq_consulta_form, 'VL_OE_PL_ARE_CIL'), param117_w, param141_w, 'VL_OE_PL_ARE_CIL');
                
        END IF;
        
    END IF;

END;
/


ALTER TABLE TASY.OFT_REFRACAO ADD (
  CONSTRAINT OFTREFR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OFT_REFRACAO ADD (
  CONSTRAINT OFTREFR_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTREFR_OFCOFOR_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA_FORM) 
 REFERENCES TASY.OFT_CONSULTA_FORMULARIO (NR_SEQUENCIA),
  CONSTRAINT OFTREFR_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT OFTREFR_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT OFTREFR_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.OFT_REFRACAO TO NIVEL_1;

