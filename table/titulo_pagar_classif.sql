ALTER TABLE TASY.TITULO_PAGAR_CLASSIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_CLASSIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_CLASSIF
(
  NR_TITULO                NUMBER(10)           NOT NULL,
  NR_SEQUENCIA             NUMBER(5)            NOT NULL,
  CD_CONTA_CONTABIL        VARCHAR2(20 BYTE),
  CD_CENTRO_CUSTO          NUMBER(8),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  VL_TITULO                NUMBER(15,2)         NOT NULL,
  NR_SEQ_CONTA_FINANC      NUMBER(10),
  NR_SEQ_TRANS_FIN         NUMBER(10),
  NR_CONTRATO              NUMBER(10),
  VL_DESCONTO              NUMBER(15,2),
  VL_ORIGINAL              NUMBER(15,2),
  VL_ACRESCIMO             NUMBER(15,2),
  NR_SEQ_PRODUTO           NUMBER(10),
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  NR_SEQ_PROJ_GPI          NUMBER(10),
  NR_SEQ_ETAPA_GPI         NUMBER(10),
  NR_SEQ_CONTA_GPI         NUMBER(10),
  NR_SEQ_ORC_ITEM_GPI      NUMBER(10),
  NR_SEQ_ITEM_MENSALIDADE  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITPACL_CENCUST_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPACL_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPACL_CONCONT_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPACL_CONFINA_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_SEQ_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPACL_CONTRAT_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPACL_GPICRETA_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_SEQ_ETAPA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPACL_GPICRETA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPACL_GPIORIT_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_SEQ_ORC_ITEM_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPACL_GPIORIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPACL_GPIPLAN_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_SEQ_CONTA_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPACL_GPIPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPACL_GPIPROJ_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_SEQ_PROJ_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPACL_GPIPROJ_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TITPACL_PK ON TASY.TITULO_PAGAR_CLASSIF
(NR_TITULO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPACL_PLSMSI_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_SEQ_ITEM_MENSALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPACL_PRODFIN_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_SEQ_PRODUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPACL_PRODFIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TITPACL_TITPAGA_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITPACL_TRAFINA_FK_I ON TASY.TITULO_PAGAR_CLASSIF
(NR_SEQ_TRANS_FIN)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TITPACL_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TITULO_PAGAR_CLASSIF_ATUAL
BEFORE INSERT OR UPDATE ON TASY.TITULO_PAGAR_CLASSIF FOR EACH ROW
DECLARE
cd_estabelecimento_w			Number(15,0);
ie_situacao_w				varchar2(255);
ds_centro_custo_w				varchar2(255);

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:new.cd_conta_contabil is not null) then
	--'Conta contabil'
	CTB_Consistir_Conta_Titulo(:new.cd_conta_contabil, wheb_mensagem_pck.get_texto(304002));

	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	titulo_pagar
	where	nr_titulo = :new.nr_titulo;


	if (:new.cd_conta_contabil <> :old.cd_conta_contabil) then
		CTB_Consistir_Conta_estab(cd_estabelecimento_w, :new.cd_conta_contabil, wheb_mensagem_pck.get_texto(304002));
	end if;
end if;

if	(:new.cd_centro_custo is not null) and
	(:new.cd_centro_custo <> nvl(:old.cd_centro_custo,-1)) then
	select	ie_situacao,
		ds_centro_custo
	into	ie_situacao_w,
		ds_centro_custo_w
	from	centro_custo
	where	cd_centro_custo	= :new.cd_centro_custo;

	if	(nvl(ie_situacao_w, 'A') = 'I') then
		--r.aise_application_error(-20011, 'O centro de custo "' || ds_centro_custo_w || '" esta inativo! Nao e possivel continuar.');
		wheb_mensagem_pck.exibir_mensagem_abort(267376,'ds_centro_custo_w='||ds_centro_custo_w);
	end if;
end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_classif_afatual
after insert or update or delete ON TASY.TITULO_PAGAR_CLASSIF for each row
declare

cd_estabelecimento_w                    Number(15,0);
ie_empenho_orcamento_w                  varchar2(1);
nr_titulo_w                             number(10);
vl_movimento_w                          ctb_documento.vl_movimento%type;
dt_documento_w                          ctb_documento.dt_competencia%type;
qt_reg_w                                number(10);
reg_integracao_w                        gerar_int_padrao.reg_integracao;
nr_seq_trans_fin_contab_w		titulo_pagar.nr_seq_trans_fin_contab%type;

cursor  c01 is
select  a.nm_atributo,
        a.cd_tipo_lote_contab
from    atributo_contab a
where   a.cd_tipo_lote_contab = 7
and     a.nm_atributo in ( 'VL_TIT_LIQ_IMP', 'VL_TITULO');

c01_w   c01%rowtype;

--pragma autonomous_transaction;
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	nr_titulo_w     := nvl(:new.nr_titulo, :old.nr_titulo);

	select  max(cd_estabelecimento),
		max(dt_contabil),
		max(nr_seq_trans_fin_contab)
	into    cd_estabelecimento_w,
		dt_documento_w,
		nr_seq_trans_fin_contab_w
	from    titulo_pagar
	where   nr_titulo       = nr_titulo_w;

	select  nvl(max(ie_empenho_orcamento),'N')
	into    ie_empenho_orcamento_w
	from    parametro_estoque
	where   cd_estabelecimento      = cd_estabelecimento_w;

	if      (ie_empenho_orcamento_w = 'S') then
		if (inserting or updating) then
			gerar_empenho_tit_pagar2(:new.nr_titulo,:new.cd_centro_custo, :new.cd_conta_contabil,:new.vl_titulo,:new.nm_usuario,1);
		else
			gerar_empenho_tit_pagar2(:old.nr_titulo,:old.cd_centro_custo, :old.cd_conta_contabil,:old.vl_titulo,:old.nm_usuario,2);
		end if;
	end if;
	exception when others then
		null;
		/*ds_erro_w     := substr(sqlerrm(sqlcode),1,2000);
		insert into logxxxxxxx_asy(
			cd_log,
			ds_log,
			nm_usuario,
			dt_atualizacao)
		values( 9919,
			substr('Titulo: ' || nr_titulo_w || ' ' || ds_erro_w,1,2000),
			nvl(:new.nm_usuario, :old.nm_usuario),
			sysdate);*/
	end;
/*OS 1430325 - Envio de classificacao de titulos a pagar*/
if (inserting or updating) then
        select  count(*)
        into    qt_reg_w
        from    intpd_fila_transmissao
        where   nr_seq_documento        = to_char(:new.nr_titulo)
        and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')
        and     ie_evento in ('53');

        if (qt_reg_w = 0) then
		select  max (cd_estabelecimento),
			max (ie_tipo_titulo)
			into reg_integracao_w.cd_estab_documento,
			     reg_integracao_w.ie_tipo_titulo_cpa
			from titulo_pagar
			where nr_titulo = :new.nr_titulo;
                gerar_int_padrao.gravar_integracao('53', :new.nr_titulo, :new.nm_usuario, reg_integracao_w);
        end if;
end if;

if (inserting) then
        /* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
        gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',:new.dt_atualizacao,'I',:new.nm_usuario);
elsif (updating) then
        /* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
        gravar_agend_fluxo_caixa(:new.nr_titulo,null,'TP',:new.dt_atualizacao,'A',:new.nm_usuario);
elsif (deleting) then
        /* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
        gravar_agend_fluxo_caixa(:old.nr_titulo,null,'TP',:old.dt_atualizacao,'E',:old.nm_usuario);
end if;

if      (inserting) then
        open c01;
        loop
        fetch c01 into
                c01_w;
        exit when c01%notfound;
                begin

                if      (c01_w.nm_atributo = 'VL_TIT_LIQ_IMP') then
                        select  nvl(decode(nvl(:new.vl_original,0), 0, null, :new.vl_original), :new.vl_titulo)
                        into    vl_movimento_w
                        from    dual;

                elsif   (c01_w.nm_atributo = 'VL_TITULO') then
                        vl_movimento_w  := :new.vl_titulo;

                end if;

                if      (nvl(vl_movimento_w, 0) <> 0) then
                        begin

                        ctb_concil_financeira_pck.ctb_gravar_documento( cd_estabelecimento_w,
                                                                        dt_documento_w,
                                                                        c01_w.cd_tipo_lote_contab,
                                                                        nvl(:new.nr_seq_trans_fin,nr_seq_trans_fin_contab_w),
                                                                        2,
                                                                        :new.nr_titulo,
                                                                        :new.nr_sequencia,
                                                                        null,
                                                                        vl_movimento_w,
                                                                        'TITULO_PAGAR_CLASSIF',
                                                                        c01_w.nm_atributo,
                                                                        :new.nm_usuario);
                        end;
                end if;

                end;
        end loop;
        close c01;
elsif   (updating) then
    update ctb_documento a
    set a.vl_movimento = :new.vl_titulo,
        a.nr_seq_trans_financ = nr_seq_trans_fin_contab_w,
        a.nm_usuario = :new.nm_usuario,
        a.dt_atualizacao = sysdate
    where a.nr_documento = :new.nr_titulo
    and a.nr_seq_doc_compl = :new.nr_sequencia
    and a.nr_doc_analitico is null
    and a.cd_tipo_lote_contabil = 7
    and a.nm_tabela = 'TITULO_PAGAR_CLASSIF'
    and a.nm_atributo = 'VL_TITULO';

    select  nvl(decode(nvl(:new.vl_original,0), 0, null, :new.vl_original), :new.vl_titulo)
    into    vl_movimento_w
    from    dual;

    update ctb_documento a
    set a.vl_movimento = vl_movimento_w,
        a.nr_seq_trans_financ = nr_seq_trans_fin_contab_w,
        a.nm_usuario = :new.nm_usuario,
        a.dt_atualizacao = sysdate
    where a.nr_documento = :new.nr_titulo
    and a.nr_seq_doc_compl = :new.nr_sequencia
    and a.nr_doc_analitico is null
    and a.cd_tipo_lote_contabil = 7
    and a.nm_tabela = 'TITULO_PAGAR_CLASSIF'
    and a.nm_atributo = 'VL_TIT_LIQ_IMP';
end if;
end if;

end;
/


ALTER TABLE TASY.TITULO_PAGAR_CLASSIF ADD (
  CONSTRAINT TITPACL_PK
 PRIMARY KEY
 (NR_TITULO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          5M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_CLASSIF ADD (
  CONSTRAINT TITPACL_PLSMSI_FK 
 FOREIGN KEY (NR_SEQ_ITEM_MENSALIDADE) 
 REFERENCES TASY.PLS_MENSALIDADE_SEG_ITEM (NR_SEQUENCIA),
  CONSTRAINT TITPACL_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO)
    ON DELETE CASCADE,
  CONSTRAINT TITPACL_GPICRETA_FK 
 FOREIGN KEY (NR_SEQ_ETAPA_GPI) 
 REFERENCES TASY.GPI_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT TITPACL_GPIORIT_FK 
 FOREIGN KEY (NR_SEQ_ORC_ITEM_GPI) 
 REFERENCES TASY.GPI_ORC_ITEM (NR_SEQUENCIA),
  CONSTRAINT TITPACL_GPIPLAN_FK 
 FOREIGN KEY (NR_SEQ_CONTA_GPI) 
 REFERENCES TASY.GPI_PLANO (NR_SEQUENCIA),
  CONSTRAINT TITPACL_GPIPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_GPI) 
 REFERENCES TASY.GPI_PROJETO (NR_SEQUENCIA),
  CONSTRAINT TITPACL_PRODFIN_FK 
 FOREIGN KEY (NR_SEQ_PRODUTO) 
 REFERENCES TASY.PRODUTO_FINANCEIRO (NR_SEQUENCIA),
  CONSTRAINT TITPACL_CONTRAT_FK 
 FOREIGN KEY (NR_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT TITPACL_CONFINA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT TITPACL_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TITPACL_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TITPACL_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL));

GRANT SELECT ON TASY.TITULO_PAGAR_CLASSIF TO NIVEL_1;

