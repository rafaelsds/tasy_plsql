ALTER TABLE TASY.PLS_MOV_BENEF_CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MOV_BENEF_CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MOV_BENEF_CONTRATO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_CONTRATO           NUMBER(10),
  NR_SEQ_INTERCAMBIO        NUMBER(10),
  NR_SEQ_MOV_OPERADORA      NUMBER(10),
  NR_SEQ_LOTE               NUMBER(10),
  CD_CNPJ                   VARCHAR2(14 BYTE),
  DS_RAZAO_SOCIAL           VARCHAR2(80 BYTE),
  NM_FANTASIA               VARCHAR2(80 BYTE),
  DS_NOME_ABREV             VARCHAR2(80 BYTE),
  NR_INSCRICAO_ESTADUAL     VARCHAR2(20 BYTE),
  NR_INSCRICAO_MUNICIPAL    VARCHAR2(20 BYTE),
  CD_MUNICIPIO_IBGE         VARCHAR2(6 BYTE),
  CD_CEP                    VARCHAR2(15 BYTE),
  DS_ENDERECO               VARCHAR2(100 BYTE),
  NR_ENDERECO               VARCHAR2(10 BYTE),
  DS_BAIRRO                 VARCHAR2(40 BYTE),
  DS_COMPLEMENTO            VARCHAR2(255 BYTE),
  SG_ESTADO                 VARCHAR2(15 BYTE),
  CD_TIPO_LOGRADOURO        VARCHAR2(20 BYTE),
  NR_DDI_TELEFONE           VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE           VARCHAR2(3 BYTE),
  NR_TELEFONE               VARCHAR2(15 BYTE),
  DS_EMAIL                  VARCHAR2(60 BYTE),
  NM_PESSOA_CONTATO         VARCHAR2(255 BYTE),
  NR_CONTRATO               NUMBER(10),
  DT_CONTRATO               DATE,
  DT_RESCISAO               DATE,
  CD_EMPRESA                VARCHAR2(10 BYTE),
  IE_TIPO_COMPARTILHAMENTO  VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSMBCT_PK ON TASY.PLS_MOV_BENEF_CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMBCT_PLSCONT_FK_I ON TASY.PLS_MOV_BENEF_CONTRATO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMBCT_PLSINCA_FK_I ON TASY.PLS_MOV_BENEF_CONTRATO
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMBCT_PLSMBLT_FK_I ON TASY.PLS_MOV_BENEF_CONTRATO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMBCT_PLSMBOP_FK_I ON TASY.PLS_MOV_BENEF_CONTRATO
(NR_SEQ_MOV_OPERADORA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMBCT_SUSMUNI_FK_I ON TASY.PLS_MOV_BENEF_CONTRATO
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_MOV_BENEF_CONTRATO_tp  after update ON TASY.PLS_MOV_BENEF_CONTRATO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_RAZAO_SOCIAL,1,4000),substr(:new.DS_RAZAO_SOCIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_RAZAO_SOCIAL',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSCRICAO_ESTADUAL,1,4000),substr(:new.NR_INSCRICAO_ESTADUAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSCRICAO_ESTADUAL',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_NOME_ABREV,1,4000),substr(:new.DS_NOME_ABREV,1,4000),:new.nm_usuario,nr_seq_w,'DS_NOME_ABREV',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDD_TELEFONE,1,4000),substr(:new.NR_DDD_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDD_TELEFONE',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DDI_TELEFONE,1,4000),substr(:new.NR_DDI_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_DDI_TELEFONE',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_INSCRICAO_MUNICIPAL,1,4000),substr(:new.NR_INSCRICAO_MUNICIPAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_INSCRICAO_MUNICIPAL',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_FANTASIA,1,4000),substr(:new.NM_FANTASIA,1,4000),:new.nm_usuario,nr_seq_w,'NM_FANTASIA',ie_log_w,ds_w,'PLS_MOV_BENEF_CONTRATO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_MOV_BENEF_CONTRATO ADD (
  CONSTRAINT PLSMBCT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_MOV_BENEF_CONTRATO ADD (
  CONSTRAINT PLSMBCT_PLSMBLT_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PLS_MOV_BENEF_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSMBCT_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSMBCT_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSMBCT_PLSMBOP_FK 
 FOREIGN KEY (NR_SEQ_MOV_OPERADORA) 
 REFERENCES TASY.PLS_MOV_BENEF_OPERADORA (NR_SEQUENCIA),
  CONSTRAINT PLSMBCT_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE));

GRANT SELECT ON TASY.PLS_MOV_BENEF_CONTRATO TO NIVEL_1;

