ALTER TABLE TASY.TAREFA_ENFERMAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TAREFA_ENFERMAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.TAREFA_ENFERMAGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TAREFA            VARCHAR2(255 BYTE),
  CD_DEPARTAMENTO      NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TARENF_DEPMED_FK_I ON TASY.TAREFA_ENFERMAGEM
(CD_DEPARTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TARENF_PK ON TASY.TAREFA_ENFERMAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TAREFA_ENFERMAGEM ADD (
  CONSTRAINT TARENF_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.TAREFA_ENFERMAGEM ADD (
  CONSTRAINT TARENF_SETATEN_FK 
 FOREIGN KEY (CD_DEPARTAMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

