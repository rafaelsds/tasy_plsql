ALTER TABLE TASY.PRP_REGRA_EQUIPE_PARTIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRP_REGRA_EQUIPE_PARTIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRP_REGRA_EQUIPE_PARTIC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_EQUIPE_PADRAO  NUMBER(10),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE),
  NR_SEQ_FUNCAO         NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_SEQ_AREA           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRPEQUPAR_PESFISI_FK_I ON TASY.PRP_REGRA_EQUIPE_PARTIC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PRPEQUPAR_PK ON TASY.PRP_REGRA_EQUIPE_PARTIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPEQUPAR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRPEQUPAR_PROFUNC_FK_I ON TASY.PRP_REGRA_EQUIPE_PARTIC
(NR_SEQ_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPEQUPAR_PROFUNC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRPEQUPAR_PRPAREAP_FK_I ON TASY.PRP_REGRA_EQUIPE_PARTIC
(NR_SEQ_AREA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPEQUPAR_PRPEQUPPAD_FK_I ON TASY.PRP_REGRA_EQUIPE_PARTIC
(NR_SEQ_EQUIPE_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPEQUPAR_PRPEQUPPAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRPEQUPAR_SETATEN_FK_I ON TASY.PRP_REGRA_EQUIPE_PARTIC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPEQUPAR_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PRP_REGRA_EQUIPE_PARTIC ADD (
  CONSTRAINT PRPEQUPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRP_REGRA_EQUIPE_PARTIC ADD (
  CONSTRAINT PRPEQUPAR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRPEQUPAR_PRPEQUPPAD_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE_PADRAO) 
 REFERENCES TASY.PRP_REGRA_EQUIPE_PADRAO (NR_SEQUENCIA),
  CONSTRAINT PRPEQUPAR_PROFUNC_FK 
 FOREIGN KEY (NR_SEQ_FUNCAO) 
 REFERENCES TASY.PROJ_FUNCAO (NR_SEQUENCIA),
  CONSTRAINT PRPEQUPAR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRPEQUPAR_PRPAREAP_FK 
 FOREIGN KEY (NR_SEQ_AREA) 
 REFERENCES TASY.PRP_AREA_PROCESSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRP_REGRA_EQUIPE_PARTIC TO NIVEL_1;

