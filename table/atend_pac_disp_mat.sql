ALTER TABLE TASY.ATEND_PAC_DISP_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PAC_DISP_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PAC_DISP_MAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_DISP_PAC      NUMBER(10)               NOT NULL,
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  QT_MATERIAL          NUMBER(9,3)              NOT NULL,
  DT_MATERIAL          DATE                     NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATPADIM_ATEPADI_FK_I ON TASY.ATEND_PAC_DISP_MAT
(NR_SEQ_DISP_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATPADIM_ATEPADI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATPADIM_MATERIA_FK_I ON TASY.ATEND_PAC_DISP_MAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATPADIM_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATPADIM_PESFISI_FK_I ON TASY.ATEND_PAC_DISP_MAT
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATPADIM_PK ON TASY.ATEND_PAC_DISP_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATPADIM_PK
  MONITORING USAGE;


ALTER TABLE TASY.ATEND_PAC_DISP_MAT ADD (
  CONSTRAINT ATPADIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PAC_DISP_MAT ADD (
  CONSTRAINT ATPADIM_ATEPADI_FK 
 FOREIGN KEY (NR_SEQ_DISP_PAC) 
 REFERENCES TASY.ATEND_PAC_DISPOSITIVO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATPADIM_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATPADIM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.ATEND_PAC_DISP_MAT TO NIVEL_1;

