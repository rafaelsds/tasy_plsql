ALTER TABLE TASY.SMDEPENDENTLINKS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SMDEPENDENTLINKS CASCADE CONSTRAINTS;

CREATE TABLE TASY.SMDEPENDENTLINKS
(
  DEPENDENTID      NUMBER(10),
  DEPENDENTTYPE    NUMBER(10),
  DEPENDEEID       NUMBER(10),
  DEPENDEETYPE     NUMBER(10),
  ASSOCIATION_ID_  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SMDEPENDEEINDEX ON TASY.SMDEPENDENTLINKS
(DEPENDEEID, DEPENDEETYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SMDEPENDEEINDEX
  MONITORING USAGE;


CREATE INDEX TASY.SMDEPENDENTINDEX ON TASY.SMDEPENDENTLINKS
(DEPENDENTID, DEPENDENTTYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SMDEPENDENTINDEX
  MONITORING USAGE;


ALTER TABLE TASY.SMDEPENDENTLINKS ADD (
  PRIMARY KEY
 (DEPENDEEID, DEPENDENTID, ASSOCIATION_ID_, DEPENDEETYPE, DEPENDENTTYPE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SMDEPENDENTLINKS TO NIVEL_1;

