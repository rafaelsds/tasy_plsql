ALTER TABLE TASY.TITULO_RECEBER_COBR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_RECEBER_COBR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_RECEBER_COBR
(
  NR_SEQ_COBRANCA        NUMBER(10)             NOT NULL,
  NR_TITULO              NUMBER(10)             NOT NULL,
  VL_COBRANCA            NUMBER(15,2)           NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_BANCO               NUMBER(5),
  CD_AGENCIA_BANCARIA    VARCHAR2(8 BYTE),
  NR_CONTA               VARCHAR2(20 BYTE),
  CD_MOEDA               NUMBER(2),
  DT_LIQUIDACAO          DATE,
  VL_LIQUIDACAO          NUMBER(15,2),
  IE_DIGITO_CONTA        VARCHAR2(2 BYTE),
  CD_CAMARA_COMPENSACAO  NUMBER(3),
  VL_DESCONTO            NUMBER(15,2),
  VL_ACRESCIMO           NUMBER(15,2),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_OCORRENCIA          VARCHAR2(3 BYTE),
  CD_INSTRUCAO           VARCHAR2(5 BYTE),
  QT_DIAS_INSTRUCAO      NUMBER(3),
  NR_SEQ_OCORRENCIA_RET  NUMBER(10),
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  VL_DESPESA_BANCARIA    NUMBER(15,2),
  VL_JUROS               NUMBER(15,2),
  VL_MULTA               NUMBER(15,2),
  VL_DESC_PREVISTO       NUMBER(15,2),
  NR_SEQ_MENSALIDADE     NUMBER(10),
  CD_CENTRO_CUSTO_DESC   NUMBER(8),
  NR_SEQ_MOTIVO_DESC     NUMBER(10),
  NR_SEQ_OCORR_MOTIVO    NUMBER(10),
  VL_SALDO_INCLUSAO      NUMBER(15,2),
  IE_VERIFICACAO         VARCHAR2(1 BYTE),
  DS_INCONSISTENCIA      VARCHAR2(4000 BYTE),
  CD_PESSOA_FISICA_R     VARCHAR2(10 BYTE),
  CD_CGC_R               VARCHAR2(14 BYTE),
  NM_PESSOA_R            VARCHAR2(80 BYTE),
  NR_DOCUMENTO_R         VARCHAR2(30 BYTE),
  VL_TITULO_R            NUMBER(15,2),
  DS_ESTABELECIMENTO_R   VARCHAR2(80 BYTE),
  VL_JUROS_CALC          NUMBER(15,2),
  VL_MULTA_CALC          NUMBER(15,2),
  DT_PAGAMENTO_REAL      DATE,
  NR_SEQ_TRANS_FINANC    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          80M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIRECOB_BANCO_FK_I ON TASY.TITULO_RECEBER_COBR
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          22M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECOB_BANCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRECOB_BANOCES_FK_I ON TASY.TITULO_RECEBER_COBR
(CD_BANCO, CD_OCORRENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECOB_BANOCES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRECOB_BOCMERE_FK_I ON TASY.TITULO_RECEBER_COBR
(NR_SEQ_OCORR_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECOB_BOCMERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRECOB_BOESRET_FK_I ON TASY.TITULO_RECEBER_COBR
(NR_SEQ_OCORRENCIA_RET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECOB_BOESRET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRECOB_CENCUST_FK_I ON TASY.TITULO_RECEBER_COBR
(CD_CENTRO_CUSTO_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECOB_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRECOB_COBESCR_FK_I ON TASY.TITULO_RECEBER_COBR
(NR_SEQ_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          23M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRECOB_INSESCR_FK_I ON TASY.TITULO_RECEBER_COBR
(CD_BANCO, CD_INSTRUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          22M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECOB_INSESCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRECOB_MOTDESC_FK_I ON TASY.TITULO_RECEBER_COBR
(NR_SEQ_MOTIVO_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECOB_MOTDESC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TIRECOB_PK ON TASY.TITULO_RECEBER_COBR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          15M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRECOB_PLSMENS_FK_I ON TASY.TITULO_RECEBER_COBR
(NR_SEQ_MENSALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRECOB_TITRECE_FK_I ON TASY.TITULO_RECEBER_COBR
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRECOB_TRAFINA_FK_I ON TASY.TITULO_RECEBER_COBR
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECOB_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.titulo_receber_cobr_bef_ins
before insert ON TASY.TITULO_RECEBER_COBR for each row
declare

vl_saldo_titulo_w		number(15,2) := 0;
vl_multa_w			number(15,2) := 0;
vl_juros_w			number(15,2) := 0;
nr_seq_conta_banco_w		number(10);
ie_cobr_juro_multa_calc_w	varchar2(1) := 'N';
ie_juros_multa_barras_bloq_w	varchar2(3) := 'N';
vl_juros_boleto_w		number(15,2) := null;
vl_multa_boleto_w		number(15,2) := null;
cd_estabelecimento_w		number(4);
cd_estab_titulo_w		number(4);
cd_estab_financ_w		number(4);
cd_estab_ativo_w		number(4);
ie_permite_estab_w		varchar2(1);

begin
if	(:new.dt_liquidacao is not null) then
	:new.vl_multa_calc	:= obter_juros_multa_titulo(:new.nr_titulo, :new.dt_liquidacao, 'R', 'M');
	:new.vl_juros_calc	:= obter_juros_multa_titulo(:new.nr_titulo, :new.dt_liquidacao, 'R', 'J');
end if;

cd_estab_ativo_w := obter_estabelecimento_ativo;
obter_param_usuario(815,47,obter_perfil_ativo,:new.nm_usuario,cd_estab_ativo_w,ie_permite_estab_w);

select	max(nr_seq_conta_banco)
into	nr_seq_conta_banco_w
from	cobranca_escritural
where	nr_sequencia	= :new.nr_seq_cobranca;

select	max(ie_cobr_juro_multa_calc)
into	ie_cobr_juro_multa_calc_w
from	banco_estabelecimento
where	nr_sequencia		= nr_seq_conta_banco_w;

if	(nvl(ie_cobr_juro_multa_calc_w,'N') = 'G') then
	select	max(vl_saldo_titulo),
		max(cd_estabelecimento)
	into	vl_saldo_titulo_w,
		cd_estabelecimento_w
	from	titulo_receber
	where	nr_titulo = :new.nr_titulo;

	select	nvl(max(ie_juros_multa_barras_bloq),'N')
	into	ie_juros_multa_barras_bloq_w
	from	parametro_contas_receber a
	where	a.cd_estabelecimento	= cd_estabelecimento_w;

	if	(ie_juros_multa_barras_bloq_w <> 'N') then
		select	a.vl_juros_boleto,
			a.vl_multa_boleto
		into	vl_juros_boleto_w,
			vl_multa_boleto_w
		from	titulo_receber a
		where	a.nr_titulo	= :new.nr_titulo;
	end if;

	vl_multa_w := nvl(obter_juros_multa_titulo(:new.nr_titulo, :new.dt_liquidacao, 'R', 'M'),0);
	vl_juros_w := nvl(obter_juros_multa_titulo(:new.nr_titulo, :new.dt_liquidacao, 'R', 'J'),0);

	-- BOLETO
	if	(vl_juros_boleto_w > 0) or
		(vl_multa_boleto_w > 0) then
		vl_juros_w	:= nvl(vl_juros_boleto_w,0);
		vl_multa_w	:= nvl(vl_multa_boleto_w,0);
	end if;

	if	(nvl(vl_saldo_titulo_w,0) > 0) and
		(:new.vl_liquidacao > nvl(vl_saldo_titulo_w,0)) and
		(nvl(:new.vl_multa,0) = 0) and
		(nvl(:new.vl_juros,0) = 0) and
		(vl_multa_w > 0) then
		:new.vl_multa := vl_multa_w;
		vl_saldo_titulo_w := nvl(vl_saldo_titulo_w,0) + nvl(:new.vl_multa,0);
	end if;

	if	(nvl(vl_saldo_titulo_w,0) > 0) and
		(:new.vl_liquidacao > nvl(vl_saldo_titulo_w,0)) and
		(nvl(:new.vl_juros,0) = 0) and
		(vl_juros_w > 0) then
		:new.vl_juros := :new.vl_liquidacao - nvl(vl_saldo_titulo_w,0);

		if	(nvl(vl_juros_boleto_w,0) > 0) then
			:new.vl_juros := vl_juros_boleto_w;
		end if;
	end if;
end if;

if	(nvl(ie_permite_estab_w,'S') = 'N') then
	select	max(cd_estabelecimento),
		max(cd_estab_financeiro)
	into	cd_estab_titulo_w,
		cd_estab_financ_w
	from	titulo_receber
	where	nr_titulo = :new.nr_titulo;
	if ((cd_estab_titulo_w <> cd_estab_ativo_w) and (cd_estab_financ_w <> cd_estab_ativo_w)) then
		-- N�o � permitido incluir t�tulos de outro estabelecimento. Par�metro [47]
		wheb_mensagem_pck.exibir_mensagem_abort(446941);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_cobr_update
before update ON TASY.TITULO_RECEBER_COBR for each row
declare

cd_estab_titulo_w		number(4);
cd_estab_financ_w		number(4);
cd_estab_ativo_w		number(4);
ie_permite_estab_w		varchar2(1);

begin

cd_estab_ativo_w := obter_estabelecimento_ativo;
obter_param_usuario(815,47,obter_perfil_ativo,:new.nm_usuario,cd_estab_ativo_w,ie_permite_estab_w);

if	(nvl(ie_permite_estab_w,'S') = 'N') then
	select	max(cd_estabelecimento),
		max(cd_estab_financeiro)
	into	cd_estab_titulo_w,
		cd_estab_financ_w
	from	titulo_receber
	where	nr_titulo = :new.nr_titulo;
	if ((cd_estab_titulo_w <> cd_estab_ativo_w) and (cd_estab_financ_w <> cd_estab_ativo_w)) then
		-- N�o � permitido incluir t�tulos de outro estabelecimento. Par�metro [47]
		wheb_mensagem_pck.exibir_mensagem_abort(446941);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_cobr_after_del
after delete ON TASY.TITULO_RECEBER_COBR for each row
declare

qt_reg_w		number(10);

begin

/*Verificar se esse t�tulo que foi exclu�do do lote ja tinha sido gerado nele atrav�s da TITULO_RECEBER_INSTR*/
select	count(*)
into	qt_reg_w
from	titulo_receber_instr
where	nr_titulo				= :old.nr_titulo
and		cd_ocorrencia			= :old.cd_ocorrencia
and		ie_instrucao_enviada	= 'S';

/*Se existir esse t�tulo na TITULO_RECEBER_INSTR com instrucao j� enviada, atualizar ele para N no instrucao enviada para que possa ser selcionado novamente em lotes futuros*/
if (qt_reg_w > 0) then

	update	titulo_receber_instr a
	set 	a.ie_instrucao_enviada = 'N'
	where 	a.nr_titulo 			= :old.nr_titulo
	and 	a.cd_ocorrencia 		= :old.cd_ocorrencia
	and		a.ie_instrucao_enviada 	= 'S';

end if;

end;
/


ALTER TABLE TASY.TITULO_RECEBER_COBR ADD (
  CONSTRAINT TIRECOB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          15M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_RECEBER_COBR ADD (
  CONSTRAINT TIRECOB_BANOCES_FK 
 FOREIGN KEY (CD_BANCO, CD_OCORRENCIA) 
 REFERENCES TASY.BANCO_OCORRENCIA_ESCRITURAL (CD_BANCO,CD_OCORRENCIA),
  CONSTRAINT TIRECOB_BOESRET_FK 
 FOREIGN KEY (NR_SEQ_OCORRENCIA_RET) 
 REFERENCES TASY.BANCO_OCORR_ESCRIT_RET (NR_SEQUENCIA),
  CONSTRAINT TIRECOB_INSESCR_FK 
 FOREIGN KEY (CD_BANCO, CD_INSTRUCAO) 
 REFERENCES TASY.INSTRUCAO_ESCRITURAL (CD_BANCO,CD_INSTRUCAO),
  CONSTRAINT TIRECOB_COBESCR_FK 
 FOREIGN KEY (NR_SEQ_COBRANCA) 
 REFERENCES TASY.COBRANCA_ESCRITURAL (NR_SEQUENCIA),
  CONSTRAINT TIRECOB_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT TIRECOB_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT TIRECOB_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO_DESC) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TIRECOB_MOTDESC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DESC) 
 REFERENCES TASY.MOTIVO_DESCONTO (NR_SEQUENCIA),
  CONSTRAINT TIRECOB_BOCMERE_FK 
 FOREIGN KEY (NR_SEQ_OCORR_MOTIVO) 
 REFERENCES TASY.BANCO_OCORR_MOTIVO_RET (NR_SEQUENCIA),
  CONSTRAINT TIRECOB_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TIRECOB_PLSMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSALIDADE) 
 REFERENCES TASY.PLS_MENSALIDADE (NR_SEQUENCIA));

GRANT DELETE ON TASY.TITULO_RECEBER_COBR TO DWONATAM;

GRANT SELECT ON TASY.TITULO_RECEBER_COBR TO NIVEL_1;

