ALTER TABLE TASY.PLS_OC_BENEF_CTA_REGRA_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_BENEF_CTA_REGRA_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_BENEF_CTA_REGRA_LOG
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_OCORRENCIA_BENEF  NUMBER(10),
  NR_SEQ_CONTA             NUMBER(10),
  NR_SEQ_CONTA_PROC        NUMBER(10),
  NR_SEQ_CONTA_MAT         NUMBER(10),
  NR_SEQ_OC_CTA_FILTRO     NUMBER(10)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOCRL_PK ON TASY.PLS_OC_BENEF_CTA_REGRA_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCRL_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCRL_PLSCOMAT_FK_I ON TASY.PLS_OC_BENEF_CTA_REGRA_LOG
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCRL_PLSCOMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCRL_PLSCOME_FK_I ON TASY.PLS_OC_BENEF_CTA_REGRA_LOG
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCRL_PLSCOME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCRL_PLSCOPRO_FK_I ON TASY.PLS_OC_BENEF_CTA_REGRA_LOG
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCRL_PLSCOPRO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCRL_PLSOCBR_FK_I ON TASY.PLS_OC_BENEF_CTA_REGRA_LOG
(NR_SEQ_OCORRENCIA_BENEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCRL_PLSOCBR_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_OC_BENEF_CTA_REGRA_LOG ADD (
  CONSTRAINT PLSOCRL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_BENEF_CTA_REGRA_LOG ADD (
  CONSTRAINT PLSOCRL_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSOCRL_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSOCRL_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA),
  CONSTRAINT PLSOCRL_PLSOCBR_FK 
 FOREIGN KEY (NR_SEQ_OCORRENCIA_BENEF) 
 REFERENCES TASY.PLS_OCORRENCIA_BENEF (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_BENEF_CTA_REGRA_LOG TO NIVEL_1;

