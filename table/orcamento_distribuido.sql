ALTER TABLE TASY.ORCAMENTO_DISTRIBUIDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORCAMENTO_DISTRIBUIDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORCAMENTO_DISTRIBUIDO
(
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  CD_TABELA_CUSTO          NUMBER(5)            NOT NULL,
  CD_TABELA_CRITERIO       NUMBER(5)            NOT NULL,
  CD_SEQUENCIA_CRITERIO    NUMBER(10)           NOT NULL,
  CD_CENTRO_CONTROLE       NUMBER(8)            NOT NULL,
  CD_GRUPO_NATUREZA_GASTO  NUMBER(8),
  CD_NATUREZA_GASTO        NUMBER(20)           NOT NULL,
  CD_CENTRO_CONTROLE_DEST  NUMBER(8)            NOT NULL,
  PR_DISTRIBUICAO          NUMBER(15,4)         NOT NULL,
  VL_DISTRIBUIDO           NUMBER(15,4)         NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_NATUREZA_GASTO_ORIG   NUMBER(20),
  CD_NATUREZA_GASTO_DEST   NUMBER(20),
  NR_SEQ_GNG               NUMBER(10),
  NR_SEQ_NG                NUMBER(10),
  NR_SEQ_NG_DEST           NUMBER(10),
  NR_SEQ_NG_ORIG           NUMBER(10),
  NR_SEQ_TABELA            NUMBER(10),
  NR_SEQ_TABELA_CRIT       NUMBER(10),
  CD_ESTAB_DEST            NUMBER(4)            NOT NULL,
  VL_ORCADO                NUMBER(15,4),
  VL_ORCADO_TOTAL          NUMBER(15,4),
  NR_SEQUENCIA             NUMBER(15),
  NR_DISTRIBUICAO          NUMBER(2),
  NR_SEQ_DISTRIBUICAO      NUMBER(10),
  NR_LOTE_CONTABIL         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORCDIST_CENCONT_FK_I ON TASY.ORCAMENTO_DISTRIBUIDO
(CD_ESTABELECIMENTO, CD_CENTRO_CONTROLE_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCDIST_CENCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ORCDIST_I1 ON TASY.ORCAMENTO_DISTRIBUIDO
(CD_ESTABELECIMENTO, NR_SEQ_TABELA, NR_SEQ_TABELA_CRIT, CD_CENTRO_CONTROLE, NR_SEQ_GNG, 
NR_SEQ_NG, CD_CENTRO_CONTROLE_DEST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCDIST_I2 ON TASY.ORCAMENTO_DISTRIBUIDO
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCDIST_I3 ON TASY.ORCAMENTO_DISTRIBUIDO
(NR_SEQ_TABELA_CRIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCDIST_I3
  MONITORING USAGE;


CREATE INDEX TASY.ORCDIST_LOTCONT_FK_I ON TASY.ORCAMENTO_DISTRIBUIDO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ORCDIST_PK ON TASY.ORCAMENTO_DISTRIBUIDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ctb_orcamento_distribuido_iud
before insert or update or delete ON TASY.ORCAMENTO_DISTRIBUIDO for each row
declare

qt_registro_w	number(10);

begin
if	(inserting) then
	qt_registro_w := ctb_lote_distrib_custos.obter_se_lote_gerado(:new.nr_seq_tabela, null, null);
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1106834);
	end if;
elsif	(updating) then
	qt_registro_w := ctb_lote_distrib_custos.obter_se_lote_gerado(:new.nr_seq_tabela, null, null);
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1106834);
	end if;
elsif	(deleting) then
	qt_registro_w := ctb_lote_distrib_custos.obter_se_lote_gerado(:old.nr_seq_tabela, null, null);
	if	(qt_registro_w > 0) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1106834);
	end if;
end if;

end ctb_orcamento_distribuido_iud;
/


ALTER TABLE TASY.ORCAMENTO_DISTRIBUIDO ADD (
  CONSTRAINT ORCDIST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORCAMENTO_DISTRIBUIDO ADD (
  CONSTRAINT ORCDIST_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL));

GRANT SELECT ON TASY.ORCAMENTO_DISTRIBUIDO TO NIVEL_1;

