DROP TABLE TASY.HSJ_IMPORT_MAT_MED_UNIMED CASCADE CONSTRAINTS;

CREATE TABLE TASY.HSJ_IMPORT_MAT_MED_UNIMED
(
  CD_MATERIAL        VARCHAR2(20 BYTE),
  DS_TABELA          VARCHAR2(50 BYTE),
  CD_TIPO_TABELA     VARCHAR2(10 BYTE),
  DT_INATIVACAO      DATE,
  DS_MATERIAL        VARCHAR2(500 BYTE),
  DS_MATERIAL_COMPL  VARCHAR2(500 BYTE),
  DS_FABRICANTE      VARCHAR2(500 BYTE),
  CD_UNIDADE_MEDIDA  VARCHAR2(10 BYTE),
  CD_SIMPRO          VARCHAR2(20 BYTE),
  CD_BRASINDICE      VARCHAR2(20 BYTE),
  DT_VIGENCIA        DATE,
  VL_ITEM            VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.HSJ_IMPORT_MAT_MED_UNIMED TO NIVEL_1;

