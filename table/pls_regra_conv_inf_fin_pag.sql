ALTER TABLE TASY.PLS_REGRA_CONV_INF_FIN_PAG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_CONV_INF_FIN_PAG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_CONV_INF_FIN_PAG
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_LOTE_PRIMEIRA_MENS       VARCHAR2(1 BYTE),
  CD_TIPO_PORTADOR_ATUAL      NUMBER(5),
  CD_PORTADOR_ATUAL           NUMBER(10),
  NR_SEQ_CONTA_BANCO_ATUAL    NUMBER(10),
  NR_SEQ_CARTEIRA_COBR_ATUAL  NUMBER(10),
  CD_TIPO_PORTADOR            NUMBER(5),
  CD_PORTADOR                 NUMBER(10),
  NR_SEQ_CONTA_BANCO          NUMBER(10),
  NR_SEQ_CARTEIRA_COBR        NUMBER(10),
  IE_TIPO_ESTIPULANTE         VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRGIP_BANCART_FK_I ON TASY.PLS_REGRA_CONV_INF_FIN_PAG
(NR_SEQ_CARTEIRA_COBR_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGIP_BANCART_FK2_I ON TASY.PLS_REGRA_CONV_INF_FIN_PAG
(NR_SEQ_CARTEIRA_COBR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGIP_BANESTA_FK_I ON TASY.PLS_REGRA_CONV_INF_FIN_PAG
(NR_SEQ_CONTA_BANCO_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGIP_BANESTA_FK2_I ON TASY.PLS_REGRA_CONV_INF_FIN_PAG
(NR_SEQ_CONTA_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRGIP_PK ON TASY.PLS_REGRA_CONV_INF_FIN_PAG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGIP_PORTADO_FK_I ON TASY.PLS_REGRA_CONV_INF_FIN_PAG
(CD_PORTADOR_ATUAL, CD_TIPO_PORTADOR_ATUAL)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGIP_PORTADO_FK2_I ON TASY.PLS_REGRA_CONV_INF_FIN_PAG
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_CONV_INF_FIN_PAG ADD (
  CONSTRAINT PLSRGIP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_CONV_INF_FIN_PAG ADD (
  CONSTRAINT PLSRGIP_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR_ATUAL, CD_TIPO_PORTADOR_ATUAL) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT PLSRGIP_BANCART_FK 
 FOREIGN KEY (NR_SEQ_CARTEIRA_COBR_ATUAL) 
 REFERENCES TASY.BANCO_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSRGIP_BANCART_FK2 
 FOREIGN KEY (NR_SEQ_CARTEIRA_COBR) 
 REFERENCES TASY.BANCO_CARTEIRA (NR_SEQUENCIA),
  CONSTRAINT PLSRGIP_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO_ATUAL) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSRGIP_BANESTA_FK2 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSRGIP_PORTADO_FK2 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR));

GRANT SELECT ON TASY.PLS_REGRA_CONV_INF_FIN_PAG TO NIVEL_1;

