ALTER TABLE TASY.VIA_CANCEL_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VIA_CANCEL_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.VIA_CANCEL_EVENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_EVENTO        NUMBER(10)               NOT NULL,
  VL_EVENTO            NUMBER(15,2)             NOT NULL,
  NR_SEQ_CANCEL        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.VIACANEVE_PK ON TASY.VIA_CANCEL_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.VIACANEVE_PK
  MONITORING USAGE;


CREATE INDEX TASY.VIACANEVE_VIAEVEN_FK_I ON TASY.VIA_CANCEL_EVENTO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.VIACANEVE_VIAEVEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.VIACANEVE_VIARESCAN_FK_I ON TASY.VIA_CANCEL_EVENTO
(NR_SEQ_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.VIACANEVE_VIARESCAN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.VIA_CANCEL_EVENTO ADD (
  CONSTRAINT VIACANEVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.VIA_CANCEL_EVENTO ADD (
  CONSTRAINT VIACANEVE_VIAEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.VIA_EVENTO (NR_SEQUENCIA),
  CONSTRAINT VIACANEVE_VIARESCAN_FK 
 FOREIGN KEY (NR_SEQ_CANCEL) 
 REFERENCES TASY.VIA_RESERVA_CANCELADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.VIA_CANCEL_EVENTO TO NIVEL_1;

