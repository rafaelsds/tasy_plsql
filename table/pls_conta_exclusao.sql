ALTER TABLE TASY.PLS_CONTA_EXCLUSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_EXCLUSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_EXCLUSAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DS_LOG                   VARCHAR2(4000 BYTE),
  DS_LOG_CALL              VARCHAR2(4000 BYTE),
  DS_FUNCAO_ATIVA          VARCHAR2(50 BYTE),
  IE_APLICACAO_TASY        VARCHAR2(1 BYTE),
  NM_MAQUINA               VARCHAR2(255 BYTE),
  NR_SEQ_CONTA             NUMBER(10),
  NR_SEQ_PROTOCOLO         NUMBER(10),
  NR_SEQ_SEGURADO          NUMBER(10),
  NR_SEQ_PROTOCOLO_ORIGEM  NUMBER(10),
  NR_SEQ_PRESTADOR_EXEC    NUMBER(10),
  NR_SEQ_PRESTADOR_PROT    NUMBER(10),
  CD_GUIA_REFERENCIA       VARCHAR2(20 BYTE),
  NR_SEQ_LOTE_CONTA        NUMBER(10),
  NR_PROTOCOLO_PRESTADOR   VARCHAR2(20 BYTE),
  CD_ESTABELECIMENTO       NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCEXC_ESTABEL_FK_I ON TASY.PLS_CONTA_EXCLUSAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCEXC_I1 ON TASY.PLS_CONTA_EXCLUSAO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCEXC_I2 ON TASY.PLS_CONTA_EXCLUSAO
(NR_SEQ_LOTE_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCEXC_I3 ON TASY.PLS_CONTA_EXCLUSAO
(NR_PROTOCOLO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCEXC_PK ON TASY.PLS_CONTA_EXCLUSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCEXC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_conta_exclusao_delete
before delete or update ON TASY.PLS_CONTA_EXCLUSAO for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Finalidade: N�o permitir a exclus�o do log de exclusao da conta m�dica
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare


begin

wheb_mensagem_pck.exibir_mensagem_abort(225412);


end;
/


CREATE OR REPLACE TRIGGER TASY.pls_destruction_cta_med
before delete ON TASY.PLS_CONTA_EXCLUSAO for each row
declare
	Ds_aplicacao		Varchar2(50);
	ds_log_w		Varchar2(1500);
	ie_aplicacao_tasy_w	Varchar2(1);
begin

if	(pls_se_aplicacao_tasy = 'S') then
	Ds_aplicacao 		:= 'Aplicacao TASY ;';
	ie_aplicacao_tasy_w	:= 'S';
else
	Ds_aplicacao 		:= 'Banco ;';
	ie_aplicacao_tasy_w	:= 'N';
end if;

ds_log_w := substr(pls_obter_detalhe_exec(false),1,1500);/*substr(	' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||
			' CallStack: '|| chr(13) || chr(10)|| dbms_utility.format_call_stack,1,1500);*/
			--' Fun��o ativa : '|| obter_funcao_ativa || chr(13) ||chr(10)||

insert into plscexc_cta
	(nr_sequencia, nm_usuario, nm_usuario_nrec,
	dt_atualizacao, dt_atualizacao_nrec, ds_funcao_ativa,
	ds_log_call, ie_aplicacao_tasy, nm_maquina,
	nr_seq_excl, nm_usuario_nrec_excl, nm_usuario_excl,
	nm_maquina_excl, ie_aplicacao_tasy_excl, dt_atualizacao_nrec_excl,
	dt_atualizacao_excl ,ds_log_call_excl, ds_log_excl,
	ds_funcao_ativa_excL)
values	(plscexc_cta_seq.nextval,substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado '),1,14),
	 sysdate, sysdate, obter_funcao_ativa,
	 ds_log_w, ie_aplicacao_tasy_w, wheb_usuario_pck.get_machine,
	:old.nr_sequencia, :old.nm_usuario_nrec,:old.nm_usuario,
	:old.nm_maquina, :old.ie_aplicacao_tasy, :old.dt_atualizacao_nrec,
	:old.dt_atualizacao, :old.ds_log_call, :old.ds_log,
	:old.ds_funcao_ativa);

end;
/


ALTER TABLE TASY.PLS_CONTA_EXCLUSAO ADD (
  CONSTRAINT PLSCEXC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_EXCLUSAO ADD (
  CONSTRAINT PLSCEXC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_CONTA_EXCLUSAO TO NIVEL_1;

