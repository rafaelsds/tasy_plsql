ALTER TABLE TASY.PLS_REGRA_COM_CAMARA_DATA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COM_CAMARA_DATA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COM_CAMARA_DATA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_COMUNIC  NUMBER(10)              NOT NULL,
  IE_DATA_CALENDARIO    VARCHAR2(3 BYTE)        NOT NULL,
  QT_DIA_ANTECEDENTE    NUMBER(5)               NOT NULL,
  IE_AVISO_CONTINUO     VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRCCD_PK ON TASY.PLS_REGRA_COM_CAMARA_DATA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCCD_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRCCD_PLSRCCA_FK_I ON TASY.PLS_REGRA_COM_CAMARA_DATA
(NR_SEQ_REGRA_COMUNIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRCCD_PLSRCCA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_COM_CAMARA_DATA ADD (
  CONSTRAINT PLSRCCD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COM_CAMARA_DATA ADD (
  CONSTRAINT PLSRCCD_PLSRCCA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_COMUNIC) 
 REFERENCES TASY.PLS_REGRA_COMUNIC_CAMARA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COM_CAMARA_DATA TO NIVEL_1;

