ALTER TABLE TASY.LAB_LOINC_DADOS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_LOINC_DADOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_LOINC_DADOS
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_LOINC               VARCHAR2(10 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_LOINC_SUPERIOR  NUMBER(10),
  IE_DEPRECIADO          VARCHAR2(1 BYTE),
  CD_LOINC_REF           VARCHAR2(10 BYTE),
  DS_CLASSTYPE           VARCHAR2(255 BYTE),
  DS_EXT_COPYRIG_NOT     VARCHAR2(4000 BYTE),
  DS_STATUS              VARCHAR2(60 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LABLOINCDA_LABLOINCDA_FK_I ON TASY.LAB_LOINC_DADOS
(NR_SEQ_LOINC_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LABLOINCDA_PK ON TASY.LAB_LOINC_DADOS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LABLOINCDA_UK ON TASY.LAB_LOINC_DADOS
(CD_LOINC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LAB_LOINC_DADOS ADD (
  CONSTRAINT LABLOINCDA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAB_LOINC_DADOS ADD (
  CONSTRAINT LABLOINCDA_LABLOINCDA_FK 
 FOREIGN KEY (NR_SEQ_LOINC_SUPERIOR) 
 REFERENCES TASY.LAB_LOINC_DADOS (NR_SEQUENCIA));

GRANT SELECT ON TASY.LAB_LOINC_DADOS TO NIVEL_1;

