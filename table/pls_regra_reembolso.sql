ALTER TABLE TASY.PLS_REGRA_REEMBOLSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_REEMBOLSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_REEMBOLSO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NM_REGRA               VARCHAR2(255 BYTE)     NOT NULL,
  NR_SEQ_GRUPO_REGRA     NUMBER(10),
  DT_INICIO_VIGENCIA     DATE                   NOT NULL,
  DT_FIM_VIGENCIA        DATE,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  NR_ORDEM_EXECUCAO      NUMBER(20),
  IE_ORDENACAO_SUGERIDA  VARCHAR2(1 BYTE),
  IE_TIPO_TRANSACAO      VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRREE_ESTABEL_FK_I ON TASY.PLS_REGRA_REEMBOLSO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRREE_PK ON TASY.PLS_REGRA_REEMBOLSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRREE_PLSGRER_FK_I ON TASY.PLS_REGRA_REEMBOLSO
(NR_SEQ_GRUPO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_REEMBOLSO_tp  after update ON TASY.PLS_REGRA_REEMBOLSO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ORDENACAO_SUGERIDA,1,4000),substr(:new.IE_ORDENACAO_SUGERIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDENACAO_SUGERIDA',ie_log_w,ds_w,'PLS_REGRA_REEMBOLSO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ORDEM_EXECUCAO,1,4000),substr(:new.NR_ORDEM_EXECUCAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_EXECUCAO',ie_log_w,ds_w,'PLS_REGRA_REEMBOLSO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_regra_reembolso_atual
before insert or update ON TASY.PLS_REGRA_REEMBOLSO for each row
declare

begin
--caso seja adicionado uma data ao dt_fim_vigencia da regra
if	((:old.dt_fim_vigencia is null) and (:new.dt_fim_vigencia is not null)) then
	--faz update na a��o regra para a mesma dt_fim_vigencia da regra onde o dt_fim_vigencia for nulo
	update	pls_regra_reemb_acao
	set	dt_fim_vigencia = :new.dt_fim_vigencia
	where	nr_seq_regra	= :new.nr_sequencia
	and	dt_fim_vigencia is null;
-- caso limpe a data de vigencia da regra
elsif	(:new.dt_fim_vigencia is null) and (:old.dt_fim_vigencia is not null) then
	--caso a data de vigencia anterior for retroativa � data atual impede a altera��o
	if	(:old.dt_fim_vigencia < trunc(sysdate)) then
		wheb_mensagem_pck.exibir_mensagem_abort(385145); --N�o � permitido alterar a data fim de vig�ncia de regras com data fim de vig�ncia retroativa � data atual.
	else
		--faz o update na a��o regra onde a dt_fim_vigencia for igual a dt_fim_vigencia anterior
		update	pls_regra_reemb_acao
		set	dt_fim_vigencia = null
		where	nr_seq_regra	= :new.nr_sequencia
		and	dt_fim_vigencia = :old.dt_fim_vigencia;
	end if;
--caso altere a data de vigencia da regra
elsif	(:old.dt_fim_vigencia <> :new.dt_fim_vigencia) then
	--caso a data de vigencia anterior for retroativa � data atual impede a altera��o
	if	(:old.dt_fim_vigencia < trunc(sysdate)) then
		wheb_mensagem_pck.exibir_mensagem_abort(385145); --N�o � permitido alterar a data fim de vig�ncia de regras com data fim de vig�ncia retroativa � data atual.
	else
		--faz update na a��o regra onde a data fim de vigencia for igual a data fim de vigencia anterior
		update	pls_regra_reemb_acao
		set	dt_fim_vigencia = :new.dt_fim_vigencia
		where	nr_seq_regra	= :new.nr_sequencia
		and	dt_fim_vigencia = :old.dt_fim_vigencia;
	end if;
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_REEMBOLSO ADD (
  CONSTRAINT PLSRREE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_REEMBOLSO ADD (
  CONSTRAINT PLSRREE_PLSGRER_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REGRA) 
 REFERENCES TASY.PLS_GRUPO_REGRA_REEMB (NR_SEQUENCIA),
  CONSTRAINT PLSRREE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_REGRA_REEMBOLSO TO NIVEL_1;

