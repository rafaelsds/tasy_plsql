ALTER TABLE TASY.COMPUTADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMPUTADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMPUTADOR
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  CD_SETOR_ATENDIMENTO    NUMBER(5)             NOT NULL,
  IE_TIPO                 VARCHAR2(15 BYTE)     NOT NULL,
  NM_COMPUTADOR           VARCHAR2(40 BYTE)     NOT NULL,
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_INTERNO          NUMBER(10),
  NR_ENDERECO_IP          VARCHAR2(15 BYTE),
  NR_PORTA                NUMBER(5),
  QT_TEMPO_LOGOFF         NUMBER(5),
  NM_COMPUTADOR_PESQUISA  VARCHAR2(40 BYTE),
  NR_SEQ_LOCAL            NUMBER(10),
  NR_SEQ_IMPRESSORA       NUMBER(10),
  IE_TOUCHSCREEN_GRAPH    VARCHAR2(1 BYTE),
  CD_SURGERY_DEPARTMENT   NUMBER(5),
  CD_STANDARD_BED         VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMPUTA_ESTABEL_FK_I ON TASY.COMPUTADOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPUTA_I1 ON TASY.COMPUTADOR
(NM_COMPUTADOR_PESQUISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPUTA_I2 ON TASY.COMPUTADOR
(IE_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPUTA_I3 ON TASY.COMPUTADOR
(NR_SEQUENCIA, UPPER("NM_COMPUTADOR"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPUTA_MANLOCA_FK_I ON TASY.COMPUTADOR
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMPUTA_PK ON TASY.COMPUTADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPUTA_SETATEN_CIRU_FK_I ON TASY.COMPUTADOR
(CD_SURGERY_DEPARTMENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPUTA_SETATEN_FK_I ON TASY.COMPUTADOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMPUTA_UNIATEN_FK_I ON TASY.COMPUTADOR
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMPUTA_UNIATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.COMPUTADOR_BFINSERT
before insert ON TASY.COMPUTADOR for each row
declare

begin
	if (:new.NM_COMPUTADOR is not null) then
		:new.NM_COMPUTADOR_PESQUISA := padronizar_nome(:new.NM_COMPUTADOR);
	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.computador_update
before update ON TASY.COMPUTADOR for each row
declare

begin
	if (nvl(:new.NM_COMPUTADOR, 'X') <> nvl(:old.NM_COMPUTADOR, 'X')) or (:new.NM_COMPUTADOR_PESQUISA is null) then
		:new.NM_COMPUTADOR_PESQUISA := padronizar_nome(:new.NM_COMPUTADOR);
	end if;
end;
/


ALTER TABLE TASY.COMPUTADOR ADD (
  CONSTRAINT COMPUTA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COMPUTADOR ADD (
  CONSTRAINT COMPUTA_MANLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.MAN_LOCALIZACAO (NR_SEQUENCIA),
  CONSTRAINT COMPUTA_SETATEN_CIRU_FK 
 FOREIGN KEY (CD_SURGERY_DEPARTMENT) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT COMPUTA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT COMPUTA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT COMPUTA_UNIATEN_FK 
 FOREIGN KEY (NR_SEQ_INTERNO) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (NR_SEQ_INTERNO));

GRANT SELECT ON TASY.COMPUTADOR TO NIVEL_1;

