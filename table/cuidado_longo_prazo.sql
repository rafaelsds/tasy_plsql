ALTER TABLE TASY.CUIDADO_LONGO_PRAZO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CUIDADO_LONGO_PRAZO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CUIDADO_LONGO_PRAZO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  DT_REGISTRO             DATE                  NOT NULL,
  CD_PROFISSIONAL         VARCHAR2(10 BYTE)     NOT NULL,
  IE_NEGA_LONGO_PRAZO     VARCHAR2(1 BYTE)      NOT NULL,
  CD_TIPO_CUIDADO         NUMBER(5),
  NM_ESTABELECIMENTO      VARCHAR2(255 BYTE),
  NM_GERENTE_ATEND        VARCHAR2(255 BYTE),
  NM_PESSOA_BEM_ESTAR     VARCHAR2(255 BYTE),
  NR_DDI_ESTAB            VARCHAR2(3 BYTE),
  NR_TELEFONE_ESTAB       VARCHAR2(15 BYTE),
  IE_INTERVALO_HOME_CARE  VARCHAR2(15 BYTE),
  QT_INTERVALO_HOME_CARE  NUMBER(5),
  QT_HORA_GASTO_HOSP      NUMBER(5),
  QT_MIN_GASTO_HOSP       NUMBER(5),
  DS_OBS_SERV_HOME_CARE   VARCHAR2(4000 BYTE),
  DS_MEIO_TRANSP_HOSP     VARCHAR2(4000 BYTE),
  IE_ACOMPANHANTE         VARCHAR2(1 BYTE),
  NM_PESSOA_ACOMPANHANTE  VARCHAR2(255 BYTE),
  IE_CATEGORIA_CUIDADO    VARCHAR2(15 BYTE),
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  DT_LIBERACAO            DATE,
  NM_USUARIO_LIBERACAO    VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CULOPRA_PESFISI_FK_I ON TASY.CUIDADO_LONGO_PRAZO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CULOPRA_PESFISI_FK2_I ON TASY.CUIDADO_LONGO_PRAZO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CULOPRA_PK ON TASY.CUIDADO_LONGO_PRAZO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CULOPRA_PROCEDE_FK_I ON TASY.CUIDADO_LONGO_PRAZO
(CD_TIPO_CUIDADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.CUIDADO_LONGO_PRAZO_tp  after update ON TASY.CUIDADO_LONGO_PRAZO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_TIPO_CUIDADO,1,500);gravar_log_alteracao(substr(:old.CD_TIPO_CUIDADO,1,4000),substr(:new.CD_TIPO_CUIDADO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_CUIDADO',ie_log_w,ds_w,'CUIDADO_LONGO_PRAZO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DS_OBS_SERV_HOME_CARE,1,500);gravar_log_alteracao(substr(:old.DS_OBS_SERV_HOME_CARE,1,4000),substr(:new.DS_OBS_SERV_HOME_CARE,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBS_SERV_HOME_CARE',ie_log_w,ds_w,'CUIDADO_LONGO_PRAZO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO_LIBERACAO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'CUIDADO_LONGO_PRAZO',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_LIBERACAO,1,500);gravar_log_alteracao(to_char(:old.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_LIBERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'CUIDADO_LONGO_PRAZO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.CUIDADO_LONGO_PRAZO_BEF_UPDATE
before update ON TASY.CUIDADO_LONGO_PRAZO for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')	then
	if	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then

		:new.nm_usuario_liberacao := :new.nm_usuario;

	end if;
end if;

end;
/


ALTER TABLE TASY.CUIDADO_LONGO_PRAZO ADD (
  CONSTRAINT CULOPRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CUIDADO_LONGO_PRAZO ADD (
  CONSTRAINT CULOPRA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT CULOPRA_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CULOPRA_PROCEDE_FK 
 FOREIGN KEY (CD_TIPO_CUIDADO) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA));

GRANT SELECT ON TASY.CUIDADO_LONGO_PRAZO TO NIVEL_1;

