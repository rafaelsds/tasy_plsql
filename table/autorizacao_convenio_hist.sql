ALTER TABLE TASY.AUTORIZACAO_CONVENIO_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUTORIZACAO_CONVENIO_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUTORIZACAO_CONVENIO_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_ATENDIMENTO       NUMBER(10),
  NR_SEQ_AUTORIZACAO   NUMBER(10),
  DS_HISTORICO         VARCHAR2(4000 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQUENCIA_AUTOR   NUMBER(10)               NOT NULL,
  DT_LIBERACAO         DATE,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE),
  NR_SEQ_AUTOR_ORIGEM  NUMBER(10),
  IE_VISUALIZA_RELAT   VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_HIST     NUMBER(10),
  CD_PROFISSIONAL      VARCHAR2(10 BYTE),
  NR_SEQ_ESTAGIO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUTCOHI_AUTCIEN_FK_I ON TASY.AUTORIZACAO_CONVENIO_HIST
(NR_SEQUENCIA_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCOHI_AUTORTH_FK_I ON TASY.AUTORIZACAO_CONVENIO_HIST
(NR_SEQ_TIPO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCOHI_AUTORTH_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCOHI_ESTAUTO_FK_I ON TASY.AUTORIZACAO_CONVENIO_HIST
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCOHI_ESTAUTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCOHI_PESFISI_FK_I ON TASY.AUTORIZACAO_CONVENIO_HIST
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AUTCOHI_PK ON TASY.AUTORIZACAO_CONVENIO_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.autorizacao_convenio_hist_afup
after update ON TASY.AUTORIZACAO_CONVENIO_HIST for each row
declare
ds_titulo_w			varchar2(255)	:= wheb_mensagem_pck.get_texto(312781);
ds_historico_w			varchar2(4000)	:= '';
dt_parametro_w			date		:= ESTABLISHMENT_TIMEZONE_UTILS.endOfDay(sysdate);
ds_antes_w			varchar2(25)	:= wheb_mensagem_pck.get_texto(182371);
ds_depois_w			varchar2(25)	:= wheb_mensagem_pck.get_texto(182372);
ds_campo_w			varchar2(25)    := wheb_mensagem_pck.get_texto(182370);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

begin
if	(nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0)) then
	ds_historico_w := substr(ds_historico_w||ds_campo_w || ' NR_ATENDIMENTO ' || ds_antes_w || ' ' || :old.nr_atendimento ||' '|| ds_depois_w|| ' '|| :new.nr_atendimento ||chr(13)||chr(10),1,4000);
end if;
if	(nvl(:new.nr_seq_autorizacao,0) <> nvl(:old.nr_seq_autorizacao,0)) then
	ds_historico_w := substr(ds_historico_w||ds_campo_w || 'NR_SEQ_AUTORIZACAO ' || ds_antes_w || ' ' || :old.nr_seq_autorizacao ||' '|| ds_depois_w|| ' '|| :new.nr_seq_autorizacao ||chr(13)||chr(10),1,4000);
end if;
if	(nvl(:new.ds_historico,0) <> nvl(:old.ds_historico,0)) then
	ds_historico_w := substr(ds_historico_w||ds_campo_w || 'DS_HISTORICO ' || ds_antes_w || ' ' || :old.ds_historico ||' '|| ds_depois_w|| ' '|| :new.ds_historico ||chr(13)||chr(10),1,4000);
end if;
if	(nvl(:new.dt_liberacao,dt_parametro_w) <> nvl(:old.dt_liberacao,dt_parametro_w)) then
	ds_historico_w := substr(ds_historico_w||ds_campo_w || ' DT_LIBERACAO ' || ds_antes_w || ' ' || to_char(:old.dt_liberacao,'dd/mm/yyyy hh24:mi:ss') ||' '|| ds_depois_w|| ' '|| to_char(:new.dt_liberacao,'dd/mm/yyyy hh24:mi:ss') ||chr(13)||chr(10),1,4000);
end if;
if	(nvl(:new.nm_usuario_lib,0) <> nvl(:old.nm_usuario_lib,0)) then
	ds_historico_w := substr(ds_historico_w||ds_campo_w || ' NM_USUARIO_LIB ' || ds_antes_w || ' ' || :old.nm_usuario_lib ||' '|| ds_depois_w|| ' '|| :new.nm_usuario_lib ||chr(13)||chr(10),1,4000);
end if;
if	(nvl(:new.nr_seq_autor_origem,0) <> nvl(:old.nr_seq_autor_origem,0)) then
	ds_historico_w := substr(ds_historico_w||ds_campo_w || ' NR_SEQ_AUTOR_ORIGEM ' || ds_antes_w || ' ' || :old.nr_seq_autor_origem ||' '|| ds_depois_w|| ' '|| :new.nr_seq_autor_origem ||chr(13)||chr(10),1,4000);
end if;
if	(nvl(:new.ie_visualiza_relat,0) <> nvl(:old.ie_visualiza_relat,0)) then
	ds_historico_w := substr(ds_historico_w||ds_campo_w || ' IE_VISUALIZA_RELAT ' || ds_antes_w || ' ' || :old.ie_visualiza_relat ||' '|| ds_depois_w|| ' '|| :new.ie_visualiza_relat ||chr(13)||chr(10),1,4000);
end if;
exception
when others then
	ds_historico_w := 'X';
end;

if	(nvl(ds_historico_w,'X') <> 'X') then
	gravar_autor_conv_log_alter(:new.nr_sequencia_autor,ds_titulo_w,substr(ds_historico_w,1,2000),:new.nm_usuario);
end if;

<<final>>
null;

end;
/


ALTER TABLE TASY.AUTORIZACAO_CONVENIO_HIST ADD (
  CONSTRAINT AUTCOHI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUTORIZACAO_CONVENIO_HIST ADD (
  CONSTRAINT AUTCOHI_ESTAUTO_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.ESTAGIO_AUTORIZACAO (NR_SEQUENCIA),
  CONSTRAINT AUTCOHI_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AUTCOHI_AUTORTH_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HIST) 
 REFERENCES TASY.AUTORIZACAO_TIPO_HIST (NR_SEQUENCIA),
  CONSTRAINT AUTCOHI_AUTCIEN_FK 
 FOREIGN KEY (NR_SEQUENCIA_AUTOR) 
 REFERENCES TASY.AUTORIZACAO_CONVENIO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AUTORIZACAO_CONVENIO_HIST TO NIVEL_1;

