ALTER TABLE TASY.ESCALA_DOWNTON_INDEX
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_DOWNTON_INDEX CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_DOWNTON_INDEX
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_DOWNTON             NUMBER(2),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE),
  IE_PREVIOUS_FALLS      NUMBER(1),
  DT_LIBERACAO           DATE,
  IE_MEDICATION_NONE     VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  IE_SEDATIVE            VARCHAR2(1 BYTE),
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  IE_DIRUETICS           VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_OTHER_DIRUETICS     VARCHAR2(1 BYTE),
  IE_ANTIPARKINSONIAN    VARCHAR2(1 BYTE),
  IE_ANTIDEPRESSANTS     VARCHAR2(1 BYTE),
  DT_REGISTRO            DATE,
  IE_OTHER_MED           VARCHAR2(1 BYTE),
  IE_VISUAL_IMPAIR       VARCHAR2(1 BYTE),
  IE_NO_IMPAIR           VARCHAR2(1 BYTE),
  IE_HEARING_IMPAIR      VARCHAR2(1 BYTE),
  IE_LIMB_IMPAIR         VARCHAR2(1 BYTE),
  IE_MENTAL_STATE        NUMBER(1),
  IE_GAIT                NUMBER(1),
  IE_NIVEL_ATENCAO       VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCDOWNTON_ATEPACI_FK_I ON TASY.ESCALA_DOWNTON_INDEX
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCDOWNTON_PK ON TASY.ESCALA_DOWNTON_INDEX
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_DOWNTON_INDEX_ATUAL

before insert or update ON TASY.ESCALA_DOWNTON_INDEX 
for each row
declare

qt_pontuacao_w	number(10);

begin

qt_pontuacao_w	:= 0;

if (:new.ie_sedative = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.ie_diruetics = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.ie_other_diruetics = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.ie_antiparkinsonian = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.ie_antidepressants = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.ie_visual_impair = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.ie_hearing_impair = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.ie_limb_impair = 'S') then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;

if (:new.ie_gait = 2) then
qt_pontuacao_w	:= qt_pontuacao_w +1;
end if;


:new.qt_downton :=   qt_pontuacao_w + :new.ie_previous_falls + :new.ie_mental_state;

end;
/


ALTER TABLE TASY.ESCALA_DOWNTON_INDEX ADD (
  CONSTRAINT ESCDOWNTON_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_DOWNTON_INDEX ADD (
  CONSTRAINT ESCDOWNTON_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_DOWNTON_INDEX TO NIVEL_1;

