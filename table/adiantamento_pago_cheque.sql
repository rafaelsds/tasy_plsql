ALTER TABLE TASY.ADIANTAMENTO_PAGO_CHEQUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADIANTAMENTO_PAGO_CHEQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADIANTAMENTO_PAGO_CHEQUE
(
  NR_SEQUENCIA     NUMBER(10)                   NOT NULL,
  NR_ADIANTAMENTO  NUMBER(10)                   NOT NULL,
  DT_ATUALIZACAO   DATE                         NOT NULL,
  NM_USUARIO       VARCHAR2(15 BYTE)            NOT NULL,
  NR_SEQ_CHEQUE    NUMBER(10)                   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ADPACHE_ADIPAGO_FK_I ON TASY.ADIANTAMENTO_PAGO_CHEQUE
(NR_ADIANTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADPACHE_CHEQUE_FK_I ON TASY.ADIANTAMENTO_PAGO_CHEQUE
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ADPACHE_PK ON TASY.ADIANTAMENTO_PAGO_CHEQUE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADPACHE_PK
  MONITORING USAGE;


ALTER TABLE TASY.ADIANTAMENTO_PAGO_CHEQUE ADD (
  CONSTRAINT ADPACHE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADIANTAMENTO_PAGO_CHEQUE ADD (
  CONSTRAINT ADPACHE_CHEQUE_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE) 
 REFERENCES TASY.CHEQUE (NR_SEQUENCIA),
  CONSTRAINT ADPACHE_ADIPAGO_FK 
 FOREIGN KEY (NR_ADIANTAMENTO) 
 REFERENCES TASY.ADIANTAMENTO_PAGO (NR_ADIANTAMENTO));

GRANT SELECT ON TASY.ADIANTAMENTO_PAGO_CHEQUE TO NIVEL_1;

