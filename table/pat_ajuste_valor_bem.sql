ALTER TABLE TASY.PAT_AJUSTE_VALOR_BEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAT_AJUSTE_VALOR_BEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAT_AJUSTE_VALOR_BEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AJUSTE        NUMBER(10),
  NR_SEQ_BEM           NUMBER(10)               NOT NULL,
  VL_ORIGINAL          NUMBER(22,4),
  VL_DEPREC_ACUM       NUMBER(22,4),
  VL_CONTABIL          NUMBER(22,4),
  VL_DEPREC_ACUM_NOVO  NUMBER(22,4),
  VL_CONTABIL_NOVO     NUMBER(22,4),
  TX_DEPREC            NUMBER(15,4),
  TX_DEPREC_NOVO       NUMBER(15,4),
  VL_AJUSTE_BEM        NUMBER(22,4),
  CD_CONTA_CONTABIL    VARCHAR2(20 BYTE),
  VL_MERCADO           NUMBER(15,2),
  VL_RESIDUAL          NUMBER(15,2),
  QT_TEMPO_VIDA_UTIL   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PATAJVABM_CONCONT_FK_I ON TASY.PAT_AJUSTE_VALOR_BEM
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATAJVABM_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PATAJVABM_PATAJVA_FK_I ON TASY.PAT_AJUSTE_VALOR_BEM
(NR_SEQ_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATAJVABM_PATAJVA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PATAJVABM_PATBEM_FK_I ON TASY.PAT_AJUSTE_VALOR_BEM
(NR_SEQ_BEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATAJVABM_PATBEM_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PATAJVABM_PK ON TASY.PAT_AJUSTE_VALOR_BEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PATAJVABM_PK
  MONITORING USAGE;


ALTER TABLE TASY.PAT_AJUSTE_VALOR_BEM ADD (
  CONSTRAINT PATAJVABM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PAT_AJUSTE_VALOR_BEM ADD (
  CONSTRAINT PATAJVABM_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PATAJVABM_PATAJVA_FK 
 FOREIGN KEY (NR_SEQ_AJUSTE) 
 REFERENCES TASY.PAT_AJUSTE_VALOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PATAJVABM_PATBEM_FK 
 FOREIGN KEY (NR_SEQ_BEM) 
 REFERENCES TASY.PAT_BEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.PAT_AJUSTE_VALOR_BEM TO NIVEL_1;

