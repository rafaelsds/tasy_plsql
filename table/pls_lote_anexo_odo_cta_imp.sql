ALTER TABLE TASY.PLS_LOTE_ANEXO_ODO_CTA_IMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_ANEXO_ODO_CTA_IMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_ANEXO_ODO_CTA_IMP
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_ANEXO_CTA_IMP  NUMBER(10)              NOT NULL,
  CD_DENTE              VARCHAR2(20 BYTE)       NOT NULL,
  CD_SITUACAO_INICIAL   VARCHAR2(20 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSLAOI_PK ON TASY.PLS_LOTE_ANEXO_ODO_CTA_IMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLAOI_PLSLACI_FK_I ON TASY.PLS_LOTE_ANEXO_ODO_CTA_IMP
(NR_SEQ_ANEXO_CTA_IMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_LOTE_ANEXO_ODO_CTA_IMP ADD (
  CONSTRAINT PLSLAOI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_ANEXO_ODO_CTA_IMP ADD (
  CONSTRAINT PLSLAOI_PLSLACI_FK 
 FOREIGN KEY (NR_SEQ_ANEXO_CTA_IMP) 
 REFERENCES TASY.PLS_LOTE_ANEXO_CTA_IMP (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_LOTE_ANEXO_ODO_CTA_IMP TO NIVEL_1;

