ALTER TABLE TASY.AUTORIZACAO_CIRURGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AUTORIZACAO_CIRURGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.AUTORIZACAO_CIRURGIA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_PEDIDO               DATE                  NOT NULL,
  NM_USUARIO_PEDIDO       VARCHAR2(15 BYTE)     NOT NULL,
  CD_REQUISITANTE         VARCHAR2(10 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(2000 BYTE),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  NR_SEQ_AGENDA           NUMBER(10),
  NR_ATENDIMENTO          NUMBER(10),
  DT_AUTORIZACAO          DATE,
  NM_AUTORIZANTE          VARCHAR2(40 BYTE),
  NM_USUARIO_AUTORIZACAO  VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  DT_PREVISAO             DATE,
  DS_MOTIVO_NEGATIVA      VARCHAR2(255 BYTE),
  DT_LIBERACAO            DATE,
  DT_FIM_COTACAO          DATE,
  NR_PRESCRICAO           NUMBER(14),
  NR_DOC_CONVENIO         VARCHAR2(20 BYTE),
  CD_ESTABELECIMENTO      NUMBER(4),
  CD_COMPRADOR            VARCHAR2(10 BYTE),
  CD_SENHA                VARCHAR2(20 BYTE),
  DT_LIBERACAO_AUTOR      DATE,
  NR_SEQ_AGENDA_CONSULTA  NUMBER(10),
  IE_ESTAGIO_AUTOR        VARCHAR2(1 BYTE),
  NR_SEQ_AUTOR_CONV       NUMBER(10),
  NM_USUARIO_LIB          VARCHAR2(15 BYTE),
  DT_CANCELAMENTO         DATE,
  NM_USUARIO_CANCEL       VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  NR_SEQ_CLASSIF          NUMBER(10),
  NR_SEQ_PROC_INTERNO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AUTCIRU_AGECONS_FK_I ON TASY.AUTORIZACAO_CIRURGIA
(NR_SEQ_AGENDA_CONSULTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCIRU_AGECONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCIRU_AGEPACI_FK_I ON TASY.AUTORIZACAO_CIRURGIA
(NR_SEQ_AGENDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCIRU_ATEPACI_FK_I ON TASY.AUTORIZACAO_CIRURGIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCIRU_CLAUTOR_FK_I ON TASY.AUTORIZACAO_CIRURGIA
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCIRU_CLAUTOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCIRU_COMPRAD_FK_I ON TASY.AUTORIZACAO_CIRURGIA
(CD_COMPRADOR, CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCIRU_ESTABEL_FK_I ON TASY.AUTORIZACAO_CIRURGIA
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCIRU_I1 ON TASY.AUTORIZACAO_CIRURGIA
(DT_PEDIDO, NR_SEQUENCIA, DT_AUTORIZACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCIRU_I2 ON TASY.AUTORIZACAO_CIRURGIA
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCIRU_PESFISI_FK_I ON TASY.AUTORIZACAO_CIRURGIA
(CD_REQUISITANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCIRU_PESFISI_FK2_I ON TASY.AUTORIZACAO_CIRURGIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AUTCIRU_PK ON TASY.AUTORIZACAO_CIRURGIA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AUTCIRU_PROCEDI_FK_I ON TASY.AUTORIZACAO_CIRURGIA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCIRU_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AUTCIRU_PROINTE_FK_I ON TASY.AUTORIZACAO_CIRURGIA
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AUTCIRU_PROINTE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.AUTORIZACAO_CIRURGIA_DELETE
before delete ON TASY.AUTORIZACAO_CIRURGIA for each row
declare

ie_excluir_autor_conv_w		varchar2(10);
qt_reg_w			number(1);

pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

Obter_Param_Usuario(3006,73, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_excluir_autor_conv_w );

if	(nvl(ie_excluir_autor_conv_w,'S') = 'N') then

	update	autorizacao_convenio
	set	nr_seq_autor_cirurgia	= null
	where	nr_seq_autor_cirurgia	= :old.nr_sequencia;

end if;

<<Final>>
qt_reg_w	:= 0;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.AUTORIZACAO_CIRURGIA_INSERT
AFTER Insert ON TASY.AUTORIZACAO_CIRURGIA 
FOR EACH ROW
DECLARE

nr_doc_convenio_w	varchar2(40);
nr_seq_autorizacao_w	number(10,0);
nr_atendimento_w	number(10,0);
cd_convenio_w		number(10,0);
nr_seq_estagio_w	number(10,0);
nr_sequencia_autor_w	number(10);
ie_lib_materiais_esp_w	varchar2(1)	:= 'N';
ie_gerando_autor_conv_w	number(10);
ie_tipo_autor_conv_esp_w	varchar2(255);
ds_estagio_w		varchar2(255);
cd_pessoa_fisica_w	varchar2(10);
cd_estabelecimento_w	number(5,0);
dt_agenda_w		date;
cd_setor_origem_w	setor_atendimento.cd_setor_atendimento%type;
begin

select	nvl(max(ie_lib_materiais_esp),'N'),
	nvl(max(ie_tipo_autor_conv_esp),'5')
into	ie_lib_materiais_esp_w,
	ie_tipo_autor_conv_esp_w
from	parametro_faturamento
where	cd_estabelecimento	= :new.cd_estabelecimento;


if	(nvl(ie_lib_materiais_esp_w,'N') = 'N') and
	((nvl(:new.nr_atendimento,0) > 0) or (nvl(:new.nr_seq_agenda,0) > 0)) and
	(:new.nr_seq_autor_conv is null) then /* Francisco - 11/12/2009 - Tratei o nr_seq_autor_conv para os casos onde � gerado por autorizacao convenio*/

	select	max(a.nr_sequencia)
	into	nr_sequencia_autor_w
	from	estagio_autorizacao b,
		autorizacao_convenio a
	where	a.nr_seq_agenda		= nvl(:new.nr_seq_agenda,0)
	and	a.nr_seq_autor_cirurgia is null
	and	a.ie_tipo_autorizacao	= ie_tipo_autor_conv_esp_w /*lhalves OS264935 - 13/01/2011*/
	and	a.nr_seq_estagio	= b.nr_sequencia(+)
	and	nvl(b.ie_interno,1)	= 1; /* Francisco - OS 178491 - 12/11/09 - So pode usar a mesma se estiver como necessidade */

	if	(nr_sequencia_autor_w is null) then

		nr_seq_autorizacao_w	:= null;
		if	(nvl(:new.nr_atendimento,0) > 0) then
			select	nvl(max(nr_seq_autorizacao),0) + 1
			into	nr_seq_autorizacao_w
			from	autorizacao_convenio
			where	nr_atendimento	= :new.nr_atendimento;
	
			select	max(nr_doc_convenio),
				max(nvl(cd_convenio,0))
			into	nr_doc_convenio_w,
				cd_convenio_w
			from	atend_categoria_convenio
			where	nr_atendimento			= :new.nr_atendimento
			and	obter_atecaco_atendimento(nr_atendimento) = nr_seq_interno;

		elsif	(nvl(:new.nr_seq_agenda,0) > 0) then
	
			select	max(nr_atendimento),
				max(nvl(cd_convenio,0)),
				max(nr_doc_convenio),
				max(dt_agenda)
			into	nr_atendimento_w,
				cd_convenio_w,
				nr_doc_convenio_w,
				dt_agenda_w
			from	agenda_paciente
			where	nr_sequencia	= :new.nr_seq_agenda;

		end if;
		
		if (nvl(cd_convenio_w,0) = 0) then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(189896);
		end if;

		begin
		if	(:new.dt_autorizacao is null) then
			select	min(nr_sequencia)
			into	nr_seq_estagio_w
			from	estagio_autorizacao
			where	ie_interno	= '1'
			and	ie_situacao = 'A'
			and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
		else
			select	min(nr_sequencia)
			into	nr_seq_estagio_w
			from	estagio_autorizacao
			where	ie_interno	= '10'
			and	ie_situacao = 'A'
			and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;
		end if;
		exception
		when others then
			nr_seq_estagio_w	:= null;
		end;
		
		if (nvl(:new.nr_atendimento,0) > 0) then
			select	cd_pessoa_fisica,
				cd_estabelecimento
			into	cd_pessoa_fisica_w,
				cd_estabelecimento_w
			from	atendimento_paciente
			where	nr_atendimento = :new.nr_atendimento;
		elsif 	(nvl(:new.nr_seq_agenda,0) > 0) then
			select	max(a.cd_pessoa_fisica),
				max(b.cd_estabelecimento)
			into	cd_pessoa_fisica_w,
				cd_estabelecimento_w
			from	agenda_paciente a,
				agenda b
			where	a.nr_sequencia = :new.nr_seq_agenda
			and	a.cd_agenda  = b.cd_agenda;
		end if;
		
		select	max(cd_setor_atendimento)
		into	cd_setor_origem_w
		from	usuario
		where	nm_usuario = :new.nm_usuario;

		insert	into autorizacao_convenio
			(NR_ATENDIMENTO ,
			NR_SEQ_AUTORIZACAO,
			CD_CONVENIO,
			CD_AUTORIZACAO,
			DT_AUTORIZACAO,
			DT_INICIO_VIGENCIA,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_FIM_VIGENCIA,
			NM_RESPONSAVEL,
			DS_OBSERVACAO,
			CD_SENHA,
			CD_PROCEDIMENTO_PRINCIPAL,
			IE_ORIGEM_PROCED,
			DT_PEDIDO_MEDICO,
			CD_MEDICO_SOLICITANTE,
			IE_TIPO_GUIA,
			QT_DIA_AUTORIZADO,
			NR_PRESCRICAO,
			DT_ENVIO,
			DT_RETORNO,
			NR_SEQ_ESTAGIO,
			IE_TIPO_AUTORIZACAO,
			IE_TIPO_DIA,
			CD_TIPO_ACOMODACAO,
			NR_SEQ_AUTOR_CIRURGIA,
			DS_INDICACAO,
			DT_GERACAO_CIH,
			NR_SEQUENCIA,
			NR_SEQ_AGENDA,
			CD_PESSOA_FISICA,
			CD_ESTABELECIMENTO,
			dt_agenda,
			cd_setor_origem,
      dt_atualizacao_nrec,
      nm_usuario_nrec)
		values	(:new.nr_atendimento,
			nr_seq_autorizacao_w,
			cd_convenio_w,
			nr_doc_convenio_w,
			sysdate,
			null,
			sysdate,
			:new.nm_usuario,
			null,
			:new.nm_usuario_pedido,
			:new.ds_observacao,
			null,
			:new.cd_procedimento,
			:new.ie_origem_proced,
			:new.dt_pedido,
			:new.cd_requisitante,
			5,
			null,
			:new.nr_prescricao,
			null,
			null,
			nr_seq_estagio_w,
			ie_tipo_autor_conv_esp_w,
			'C',
			null,
			:new.nr_sequencia,
			null,
			null,
			autorizacao_convenio_seq.nextval,
			:new.nr_seq_agenda,
			cd_pessoa_fisica_w,
			cd_estabelecimento_w,
			dt_agenda_w,
			cd_setor_origem_w,
      sysdate,
      :new.nm_usuario);
	else
		update	autorizacao_convenio
		set	nr_seq_autor_cirurgia	= :new.nr_sequencia
		where	nr_sequencia		= nr_sequencia_autor_w;	
	end if;
end if;

select	obter_valor_dominio(3116,:new.ie_estagio_autor)
into	ds_estagio_w
from	dual;

if	(ds_estagio_w is not null) then
	insert into autorizacao_cirurgia_hist(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_autor_cirurgia,
			ds_historico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_liberacao,
			nm_usuario_lib)
		values(	autorizacao_cirurgia_hist_seq.nextval,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia,
			ds_estagio_w,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.AUTORIZACAO_CIRURGIA_UPDATE
AFTER Update ON TASY.AUTORIZACAO_CIRURGIA 
FOR EACH ROW
DECLARE

nr_doc_convenio_w	varchar2(40);
nr_seq_autorizacao_w	number(10,0);
nr_atendimento_w		number(10,0);
cd_convenio_w		number(10,0);
nr_seq_estagio_w		number(10,0);
nr_sequencia_autor_w	number(10,0);
ie_lib_materiais_esp_w	varchar2(1)	:= 'N';
ie_atualizar_qts_w		varchar2(2)	:= 'N';
ie_permite_desf_auto_w	varchar2(255)	:= 'N';
ie_atualiza_mat_especial_w		varchar2(15) := 'N';
qt_reg_w			number(1);
cont_w			number(10,0);
ie_atualiza_autor_conv_w	varchar2(40);
ds_estagio_w		varchar2(255);
dt_agenda_w		date;
nr_seq_estagio_param_w	number(10,0);
ie_autorzao_nao_lib_w	varchar2(10);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

Obter_Param_Usuario(3006,026,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_atualiza_autor_conv_w);
Obter_Param_Usuario(3006,072,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_autorzao_nao_lib_w);
Obter_Param_Usuario(3004,158,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_atualiza_mat_especial_w);

nr_seq_estagio_param_w := nvl(to_number(obter_valor_param_usuario(3006, 51, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento)),0);

if	(nvl(ie_atualiza_autor_conv_w,'N') = 'N') or 
	((:new.dt_autorizacao is null) and
	(:old.dt_autorizacao is not null))then
	nr_seq_estagio_param_w := 0;
end if;  

select	nvl(max(nr_sequencia),0)
into	nr_seq_estagio_w
from	estagio_autorizacao
where	ie_interno		= decode(:new.dt_autorizacao, null, '1', '10')
and	ie_situacao		= 'A'
and	nvl(ie_parcial,'N') 	= 'N'
and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;

select	nvl(max(ie_lib_materiais_esp),'N')
into	ie_lib_materiais_esp_w
from	parametro_faturamento
where	cd_estabelecimento	= :new.cd_estabelecimento;

if	(nvl(ie_autorzao_nao_lib_w,'S') = 'N') and
	(:old.IE_ESTAGIO_AUTOR <> '3') and
	(:new.IE_ESTAGIO_AUTOR = '3') and
	(:old.DT_LIBERACAO is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(324489);
end if;

if	((nvl(ie_lib_materiais_esp_w,'N') = 'N') or
	 (nvl(ie_atualiza_autor_conv_w,'N') = 'S'))and
	((nvl(:new.nr_atendimento,0) > 0) or  (nvl(:new.nr_seq_agenda,0) > 0) or (nvl(:new.cd_pessoa_fisica,0) > 0)) and
	(((:old.dt_autorizacao is null) and (:new.dt_autorizacao is not null)) or
	(:new.dt_autorizacao is null) and (:old.dt_autorizacao is not null))then
	
	select	nvl(max(nr_sequencia),0)
	into	nr_sequencia_autor_w
	from	autorizacao_convenio
	where	nr_seq_autor_cirurgia	= :new.nr_sequencia;

	
	if	(nr_sequencia_autor_w > 0) then

		if	(nr_seq_estagio_w > 0) then
			if(nvl(ie_atualiza_autor_conv_w,'N') = 'S') then
				update	autorizacao_convenio
				set	nr_seq_estagio	= decode(nr_seq_estagio_param_w,0,nr_seq_estagio_w,nr_seq_estagio_param_w),
					cd_autorizacao	= nvl(cd_autorizacao,:new.nr_doc_convenio),
					cd_senha	= nvl(cd_senha,:new.cd_senha),
					nr_prescricao 	= nvl(nr_prescricao,:new.nr_prescricao),
					nm_usuario	= :new.nm_usuario,
					dt_atualizacao  = sysdate
				where	nr_sequencia	= nr_sequencia_autor_w;
			end if;			
		Obter_Param_Usuario(3004,17,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0),ie_atualizar_qts_w);				
		if	(ie_atualizar_qts_w = 'S') then
			begin
			update  autorizacao_convenio
                        set	qt_dia_autorizado = qt_dia_solicitado,
				nm_usuario	  = :new.nm_usuario,
				dt_atualizacao	  = sysdate
			where  	nvl(qt_dia_autorizado,0) = 0
                        and    	nr_sequencia		= nr_sequencia_autor_w;
			
			update  procedimento_autorizado 
                        set    	qt_autorizada = nvl(qt_solicitada,0),
				dt_atualizacao	= sysdate,
				nm_usuario	= :new.nm_usuario	
                        where  	nvl(qt_autorizada,0)	= 0 
                        and    	nr_sequencia_autor  	= nr_sequencia_autor_w;
			
			update  material_autorizado 
                        set    	qt_autorizada = nvl(qt_solicitada,0),
				dt_atualizacao	= sysdate,
				nm_usuario	= :new.nm_usuario
                        where  	nvl(qt_autorizada,0) 	= 0 
                        and    	nr_sequencia_autor  	= nr_sequencia_autor_w;
			end;
		end if;
			
			
		end if;

	elsif	((:old.dt_autorizacao is null) and (:new.dt_autorizacao is not null) and (ie_atualiza_mat_especial_w = 'N')) then

		nr_seq_autorizacao_w	:= null;
		if	(nvl(:new.nr_atendimento,0) > 0) then
			select	nvl(max(nr_seq_autorizacao),0) + 1
			into	nr_seq_autorizacao_w
			from	autorizacao_convenio
			where	nr_atendimento	= :new.nr_atendimento;

			select	nr_doc_convenio,
				cd_convenio
			into	nr_doc_convenio_w,
				cd_convenio_w
			from	atend_categoria_convenio
			where	nr_atendimento			= :new.nr_atendimento
			and	obter_atecaco_atendimento(nr_atendimento) = nr_seq_interno;

		elsif	(nvl(:new.nr_seq_agenda,0) > 0) then

			select	nr_atendimento,
				cd_convenio,
				nr_doc_convenio,
				dt_agenda
			into	nr_atendimento_w,
				cd_convenio_w,
				nr_doc_convenio_w,
				dt_agenda_w
			from	agenda_paciente
			where	nr_sequencia	= :new.nr_seq_agenda;

		end if;
		
		if(nvl(cd_convenio_w,0) > 0) then
		
			insert	into autorizacao_convenio
			(NR_ATENDIMENTO ,
			NR_SEQ_AUTORIZACAO,
			CD_CONVENIO,
			CD_AUTORIZACAO,
			DT_AUTORIZACAO,
			DT_INICIO_VIGENCIA,
			DT_ATUALIZACAO,
			NM_USUARIO,
			DT_FIM_VIGENCIA,
			NM_RESPONSAVEL,
			DS_OBSERVACAO,
			CD_SENHA,
			CD_PROCEDIMENTO_PRINCIPAL,
			IE_ORIGEM_PROCED,
			DT_PEDIDO_MEDICO,
			CD_MEDICO_SOLICITANTE,
			IE_TIPO_GUIA,
			QT_DIA_AUTORIZADO,
			NR_PRESCRICAO,
			DT_ENVIO,
			DT_RETORNO,
			NR_SEQ_ESTAGIO,
			IE_TIPO_AUTORIZACAO,
			IE_TIPO_DIA,
			CD_TIPO_ACOMODACAO,
			NR_SEQ_AUTOR_CIRURGIA,
			DS_INDICACAO,
			DT_GERACAO_CIH,
			NR_SEQUENCIA,
			NR_SEQ_AGENDA,
			dt_agenda,
      dt_atualizacao_nrec,
      nm_usuario_nrec)
		values	(:new.nr_atendimento,
			nr_seq_autorizacao_w,
			cd_convenio_w,
			nr_doc_convenio_w,
			sysdate,
			null,
			sysdate,
			:new.nm_usuario,
			null,
			:new.nm_usuario_pedido,
			:new.ds_observacao,
			null,
			null,--:new.cd_procedimento
			null,--:new.ie_origem_proced
			:new.dt_pedido,
			:new.cd_requisitante,
			5,
			null,
			:new.nr_prescricao,
			null,
			null,
			nr_seq_estagio_w,
			5,
			'C',
			null,
			:new.nr_sequencia,
			null,
			null,
			autorizacao_convenio_seq.nextval,
			:new.nr_seq_agenda,
			dt_agenda_w,
      sysdate,
      :new.nm_usuario);		
		end if;
		
	end if;
end if;

Obter_Param_Usuario(3006,13,obter_perfil_ativo,:new.nm_usuario,nvl(:new.cd_estabelecimento,0), ie_permite_desf_auto_w);
	
	
if	(nvl(ie_lib_materiais_esp_w,'N') = 'N') and
	((nvl(:new.nr_atendimento,0) > 0) or  (nvl(:new.nr_seq_agenda,0) > 0)) and
	((:old.dt_autorizacao is not null) and (:new.dt_autorizacao is null)) and 
	(nvl(ie_permite_desf_auto_w,'N') = 'N') then
	
	select	count(*)
	into	cont_w
	from	autorizacao_convenio
	where	nr_seq_autor_cirurgia				= :new.nr_sequencia
	and	obter_estagio_autor(nr_seq_estagio, 'C')	= 10;
	
	if	(nvl(cont_w,0) > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(194717);
	end if;
end if;

select	obter_valor_dominio(3116,:new.ie_estagio_autor)
into	ds_estagio_w
from	dual;

if	(ds_estagio_w is not null) and
	(:old.ie_estagio_autor <> :new.ie_estagio_autor) then
	insert into autorizacao_cirurgia_hist(
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			nr_seq_autor_cirurgia,
			ds_historico,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			dt_liberacao,
			nm_usuario_lib)
		values(	autorizacao_cirurgia_hist_seq.nextval,
			sysdate,
			:new.nm_usuario,
			:new.nr_sequencia,
			ds_estagio_w,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario);
end if;

<<Final>>
qt_reg_w	:= 0;

END;
/


ALTER TABLE TASY.AUTORIZACAO_CIRURGIA ADD (
  CONSTRAINT AUTCIRU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AUTORIZACAO_CIRURGIA ADD (
  CONSTRAINT AUTCIRU_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AUTCIRU_PESFISI_FK 
 FOREIGN KEY (CD_REQUISITANTE) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AUTCIRU_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT AUTCIRU_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AUTCIRU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AUTCIRU_COMPRAD_FK 
 FOREIGN KEY (CD_COMPRADOR, CD_ESTABELECIMENTO) 
 REFERENCES TASY.COMPRADOR (CD_PESSOA_FISICA,CD_ESTABELECIMENTO),
  CONSTRAINT AUTCIRU_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGENDA_CONSULTA) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT AUTCIRU_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AUTCIRU_CLAUTOR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIF_AUTORIZACAO (NR_SEQUENCIA),
  CONSTRAINT AUTCIRU_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AUTORIZACAO_CIRURGIA TO NIVEL_1;

