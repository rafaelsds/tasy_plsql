ALTER TABLE TASY.ATEND_CONVENIO_END_COB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_CONVENIO_END_COB CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_CONVENIO_END_COB
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  IE_TIPO_COMPLEMENTO         NUMBER(2)         NOT NULL,
  DS_ENDERECO                 VARCHAR2(100 BYTE),
  CD_CEP                      VARCHAR2(15 BYTE),
  DS_COMPLEMENTO              VARCHAR2(40 BYTE),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DS_MUNICIPIO                VARCHAR2(40 BYTE),
  DT_ATUALIZACAO_NREC         DATE,
  SG_ESTADO                   VARCHAR2(15 BYTE),
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_ATENDIMENTO              NUMBER(10)        NOT NULL,
  NR_SEQ_COMPL_PESSOA_FISICA  NUMBER(10)        DEFAULT null,
  NR_SEQ_PAIS                 NUMBER(10),
  IE_NF_CORREIO               VARCHAR2(1 BYTE),
  IE_MALA_DIRETA              VARCHAR2(1 BYTE),
  DS_COMPL_END                VARCHAR2(20 BYTE),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATDCONVENC_ATEPACI_FK_I ON TASY.ATEND_CONVENIO_END_COB
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATDCONVENC_COMPEFI_FK_I ON TASY.ATEND_CONVENIO_END_COB
(NR_SEQ_COMPL_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATDCONVENC_PAIS_FK_I ON TASY.ATEND_CONVENIO_END_COB
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATDCONVENC_PK ON TASY.ATEND_CONVENIO_END_COB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATEND_CONVENIO_END_COB_tp  after update ON TASY.ATEND_CONVENIO_END_COB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPL_END,1,4000),substr(:new.DS_COMPL_END,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPL_END',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_COMPL_PESSOA_FISICA,1,4000),substr(:new.NR_SEQ_COMPL_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_COMPL_PESSOA_FISICA',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_COMPLEMENTO,1,4000),substr(:new.IE_TIPO_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPLEMENTO',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAIS,1,4000),substr(:new.NR_SEQ_PAIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAIS',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'ATEND_CONVENIO_END_COB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ATEND_CONVENIO_END_COB ADD (
  CONSTRAINT ATDCONVENC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ATEND_CONVENIO_END_COB ADD (
  CONSTRAINT ATDCONVENC_COMPEFI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA, NR_SEQ_COMPL_PESSOA_FISICA) 
 REFERENCES TASY.COMPL_PESSOA_FISICA (CD_PESSOA_FISICA,NR_SEQUENCIA),
  CONSTRAINT ATDCONVENC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATDCONVENC_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATEND_CONVENIO_END_COB TO NIVEL_1;

