ALTER TABLE TASY.RELATORIO_DINAMICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RELATORIO_DINAMICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RELATORIO_DINAMICO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_RELATORIO               NUMBER(15),
  NR_TIPO_BANDA              NUMBER(15),
  CD_TIPO                    VARCHAR2(15 BYTE),
  CD_LABEL                   VARCHAR2(15 BYTE),
  NR_ALTURA                  NUMBER(15),
  NR_COMPRIMENTO             NUMBER(15),
  NR_EIXO_Y                  VARCHAR2(15 BYTE),
  NR_EIXO_X                  VARCHAR2(15 BYTE),
  DS_COR_FUNDO_LABEL         VARCHAR2(20 BYTE),
  DS_COR_FUNDO_DESCRICAO     VARCHAR2(20 BYTE),
  NM_LABEL                   VARCHAR2(80 BYTE),
  IE_AJUSTAR_TAMANHO         VARCHAR2(1 BYTE),
  NR_SEQ_APRES               NUMBER(10),
  IE_TIPO_FONTE              VARCHAR2(10 BYTE),
  NR_TAM_FONTE               NUMBER(4),
  IE_ITALICO                 VARCHAR2(1 BYTE),
  IE_NEGRITO                 VARCHAR2(1 BYTE),
  QT_ROTACAO                 NUMBER(5,2),
  IE_IMAGEM_CAMPO            CLOB,
  QT_TAMANHO_BANDA           NUMBER(10),
  QT_ALTURA_BANDA            NUMBER(10),
  IE_IMAGEM_CAMPO2           VARCHAR2(4000 BYTE),
  NR_SEQ_RELATORIO           NUMBER(15),
  DS_LABEL                   VARCHAR2(255 BYTE),
  NR_SEQ_DOC_RELAT_DINAMICO  NUMBER(10),
  DS_SQL                     CLOB,
  NR_SEQ_TEMP_CONTEUDO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELDIN_DCRELDI_FK_I ON TASY.RELATORIO_DINAMICO
(NR_SEQ_DOC_RELAT_DINAMICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RELDIN_PK ON TASY.RELATORIO_DINAMICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RELATORIO_DINAMICO ADD (
  CONSTRAINT RELDIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.RELATORIO_DINAMICO ADD (
  CONSTRAINT RELDIN_DCRELDI_FK 
 FOREIGN KEY (NR_SEQ_DOC_RELAT_DINAMICO) 
 REFERENCES TASY.DOC_RELATORIO_DINAMICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RELATORIO_DINAMICO TO NIVEL_1;

