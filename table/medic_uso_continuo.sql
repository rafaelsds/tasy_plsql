ALTER TABLE TASY.MEDIC_USO_CONTINUO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDIC_USO_CONTINUO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDIC_USO_CONTINUO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE)   NOT NULL,
  DT_INICIO                 DATE                NOT NULL,
  CD_MATERIAL               NUMBER(10)          NOT NULL,
  QT_DOSE                   NUMBER(15,3)        NOT NULL,
  CD_UNIDADE_MEDIDA         VARCHAR2(30 BYTE)   NOT NULL,
  NR_DIAS_USO               NUMBER(5),
  IE_USO_CONTINUO           VARCHAR2(1 BYTE)    NOT NULL,
  IE_LAUDO_LME              VARCHAR2(1 BYTE)    NOT NULL,
  CD_CID_PRINCIPAL          VARCHAR2(10 BYTE),
  CD_CID_SECUNDARIO         VARCHAR2(10 BYTE),
  CD_INTERVALO              VARCHAR2(7 BYTE),
  IE_VIA_APLICACAO          VARCHAR2(5 BYTE),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  DT_SUSPENSAO              DATE,
  NM_USUARIO_SUSP           VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_SUSP        NUMBER(10),
  NR_SEQ_MEDIC_SUBST        NUMBER(10),
  IE_UTILIZA_DIALISE        VARCHAR2(1 BYTE),
  IE_UTILIZA_CASA           VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  IE_MEDIC_PRE_TRANSPLANTE  VARCHAR2(1 BYTE),
  DS_UTC                    VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO          VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO        VARCHAR2(50 BYTE),
  NR_SEQ_ATEND_CONS_PEPA    NUMBER(10),
  DT_LIBERACAO              DATE,
  QT_PROCED_1_MES           NUMBER(5),
  QT_PROCED_2_MES           NUMBER(5),
  QT_PROCED_3_MES           NUMBER(5),
  NR_SEQ_MAT_CPOE           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEUSCON_ATCONSPEPA_FK_I ON TASY.MEDIC_USO_CONTINUO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEUSCON_CIDDOEN_FK_I ON TASY.MEDIC_USO_CONTINUO
(CD_CID_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEUSCON_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEUSCON_CIDDOEN_FK2_I ON TASY.MEDIC_USO_CONTINUO
(CD_CID_SECUNDARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEUSCON_CIDDOEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.MEUSCON_INTPRES_FK_I ON TASY.MEDIC_USO_CONTINUO
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEUSCON_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEUSCON_MATERIA_FK_I ON TASY.MEDIC_USO_CONTINUO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEUSCON_MEUSCON_FK_I ON TASY.MEDIC_USO_CONTINUO
(NR_SEQ_MEDIC_SUBST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEUSCON_MEUSCON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEUSCON_MOSUSME_FK_I ON TASY.MEDIC_USO_CONTINUO
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEUSCON_MOSUSME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEUSCON_PESFISI_FK_I ON TASY.MEDIC_USO_CONTINUO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEUSCON_PK ON TASY.MEDIC_USO_CONTINUO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEUSCON_UNIMEDI_FK_I ON TASY.MEDIC_USO_CONTINUO
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEUSCON_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEUSCON_VIAAPLI_FK_I ON TASY.MEDIC_USO_CONTINUO
(IE_VIA_APLICACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEUSCON_VIAAPLI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Medic_Uso_Continuo_AftUpdate
after update ON TASY.MEDIC_USO_CONTINUO for each row
Declare
qt_reg_w	number(1);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Gera��o do Hist�rico de Altera��o do Medicamento */
if	(:new.cd_pessoa_fisica <> :old.cd_pessoa_fisica) or
	(nvl(:new.nm_usuario_susp,'X') <> nvl(:old.nm_usuario_susp,'X')) or
	(:new.cd_material <> :old.cd_material) or
	(:new.qt_dose <> :old.qt_dose) or
	(:new.cd_unidade_medida <> :old.cd_unidade_medida) or
	(:new.nr_dias_uso <> :old.nr_dias_uso) or
	(:new.ie_uso_continuo <> :old.ie_uso_continuo) or
	(:new.ie_laudo_lme <> :old.ie_laudo_lme) or
	(:new.cd_cid_principal <> :old.cd_cid_principal) or
 	(:new.cd_cid_secundario <> :old.cd_cid_secundario) or
	(:new.cd_intervalo <> :old.cd_intervalo) or
	(:new.ie_via_aplicacao <> :old.ie_via_aplicacao) or
	(:new.ds_observacao <> :old.ds_observacao) or
	(:new.dt_inicio <> :old.dt_inicio)then


	insert into medic_uso_continuo_hist (
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_pessoa_fisica,
		dt_inicio,
		cd_material,
		qt_dose,
		cd_unidade_medida,
		nr_dias_uso,
		ie_uso_continuo,
		ie_laudo_lme,
		cd_cid_principal,
		cd_cid_secundario,
		cd_intervalo,
		ie_via_aplicacao,
		ds_observacao,
		dt_suspensao,
		nm_usuario_susp,
		nr_seq_medic,
		nr_seq_motivo_susp,
		nr_seq_medic_subst
	) values (
		medic_uso_continuo_hist_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:old.cd_pessoa_fisica,
		:old.dt_inicio,
		:old.cd_material,
		:old.qt_dose,
		:old.cd_unidade_medida,
		:old.nr_dias_uso,
		:old.ie_uso_continuo,
		:old.ie_laudo_lme,
		:old.cd_cid_principal,
		:old.cd_cid_secundario,
		:old.cd_intervalo,
		:old.ie_via_aplicacao,
		:old.ds_observacao,
		:old.dt_suspensao,
		:old.nm_usuario_susp,
		:old.nr_sequencia,
		:old.nr_seq_motivo_susp,
		:old.nr_seq_medic_subst
	);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.Medic_Uso_Continuo_BefInsUpd
before insert or update ON TASY.MEDIC_USO_CONTINUO for each row
declare
qt_reg_w	number(1);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(:old.DT_ATUALIZACAO,sysdate+10) <> :new.DT_ATUALIZACAO) and
	(:new.DT_ATUALIZACAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_ATUALIZACAO, 'HV');
end if;

/* Consiste CID Principal */
if	(:new.ie_laudo_lme = 'S') and
	(obter_se_cid_mat_especial(:new.cd_material, :new.cd_cid_principal, 'P') = 'N') then
	/*Este CID principal n�o pode ser utilizado para este medicamento, pois n�o est� liberado no cadastro do SUS. '||
		'Este cadastro pode ser verificado na fun��o Gest�o de SUS Unificado.#@#@');*/
	Wheb_mensagem_pck.exibir_mensagem_abort(261528);
end if;

/* Consiste CID Secund�rio */
if	(:new.ie_laudo_lme = 'S') and
	(obter_se_cid_mat_especial(:new.cd_material, :new.cd_cid_secundario, 'S') = 'N') then
	/*Este CID secund�rio n�o pode ser utilizado para este medicamento, pois n�o est� liberado no cadastro do SUS. '||
		'Este cadastro pode ser verificado na fun��o Gest�o de SUS Unificado.#@#@');*/
	Wheb_mensagem_pck.exibir_mensagem_abort(261529);
end if;

if	(((:old.IE_USO_CONTINUO = 'S') and (:new.IE_USO_CONTINUO = 'N')) or
	((:new.IE_USO_CONTINUO = 'N') and (:old.IE_USO_CONTINUO is null))) and
	(:new.DT_INICIO +  :new.NR_DIAS_USO < sysdate) then
	/*N�o � poss�vel desmarcar a op��o Uso cont�nuo.' || chr(10) ||'Pois a data de in�cio juntamente com os dias de utiliza��o s�o menores que a data de hoje.#@#@');*/
	Wheb_mensagem_pck.exibir_mensagem_abort(261530);
end if;
<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.MEDIC_USO_CONTINUO ADD (
  CONSTRAINT MEUSCON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDIC_USO_CONTINUO ADD (
  CONSTRAINT MEUSCON_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT MEUSCON_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MEUSCON_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MEUSCON_CIDDOEN_FK 
 FOREIGN KEY (CD_CID_PRINCIPAL) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT MEUSCON_CIDDOEN_FK2 
 FOREIGN KEY (CD_CID_SECUNDARIO) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT MEUSCON_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT MEUSCON_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT MEUSCON_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MEUSCON_MOSUSME_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSP_MEDIC_USO_CONT (NR_SEQUENCIA),
  CONSTRAINT MEUSCON_MEUSCON_FK 
 FOREIGN KEY (NR_SEQ_MEDIC_SUBST) 
 REFERENCES TASY.MEDIC_USO_CONTINUO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MEDIC_USO_CONTINUO TO NIVEL_1;

