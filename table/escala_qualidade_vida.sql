ALTER TABLE TASY.ESCALA_QUALIDADE_VIDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_QUALIDADE_VIDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_QUALIDADE_VIDA
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  NR_ATENDIMENTO               NUMBER(10),
  DT_AVALIACAO                 DATE             NOT NULL,
  CD_PROFISSIONAL              VARCHAR2(10 BYTE) NOT NULL,
  DT_LIBERACAO                 DATE,
  IE_SAUDE                     NUMBER(1)        NOT NULL,
  IE_SAUDE_COMPARADA           NUMBER(1)        NOT NULL,
  IE_ATIVIDADE_RIGOROSA        NUMBER(1)        NOT NULL,
  IE_ATIVIDADE_MODERADA        NUMBER(1)        NOT NULL,
  IE_LEVANTAR                  NUMBER(1)        NOT NULL,
  IE_SUBIR_VARIOS_LANCES       NUMBER(1)        NOT NULL,
  IE_SUBIR_UM_LANCE            NUMBER(1)        NOT NULL,
  IE_CURVARSE                  NUMBER(1)        NOT NULL,
  IE_ANDAR_UM_QUILOMETRO       NUMBER(1)        NOT NULL,
  IE_ANDAR_QUARTEIROES         NUMBER(1)        NOT NULL,
  IE_ANDAR_UM_QUARTEIRAO       NUMBER(1)        NOT NULL,
  IE_TOMAR_BANHO_VESTIRSE      NUMBER(1)        NOT NULL,
  IE_DIMINUI_TRABALHO_FISICA   NUMBER(1)        NOT NULL,
  IE_MENOS_TAREFAS_FISICA      NUMBER(1)        NOT NULL,
  IE_LIMIT_TRABALHO_FISICA     NUMBER(1)        NOT NULL,
  IE_DIFIC_TRABALHO_FISICA     NUMBER(1)        NOT NULL,
  IE_DIMINUI_TRABALHO_EMOC     NUMBER(1)        NOT NULL,
  IE_MENOS_TAREFAS_EMOC        NUMBER(1)        NOT NULL,
  IE_ATIV_CUIDADO              NUMBER(1)        NOT NULL,
  IE_ATIVIDADES_SOCIAIS        NUMBER(1)        NOT NULL,
  IE_DOR_CORPO                 NUMBER(1)        NOT NULL,
  IE_DOR_TRABALHO              NUMBER(1)        NOT NULL,
  IE_VIGOR                     NUMBER(1)        NOT NULL,
  IE_NERVOSA                   NUMBER(1)        NOT NULL,
  IE_DEPRIMIDO                 NUMBER(1)        NOT NULL,
  IE_CALMO                     NUMBER(1)        NOT NULL,
  IE_ENERGIA                   NUMBER(1)        NOT NULL,
  IE_DESANIMADO                NUMBER(1)        NOT NULL,
  IE_ESGOTADO                  NUMBER(1)        NOT NULL,
  IE_FELIZ                     NUMBER(1)        NOT NULL,
  IE_CANSADO                   NUMBER(1)        NOT NULL,
  IE_TEMPO_ATIVIDADES          NUMBER(1)        NOT NULL,
  IE_OBEDECE_FACILMENTE        NUMBER(1)        NOT NULL,
  IE_SAUDAVEL_OUTROS           NUMBER(1)        NOT NULL,
  IE_SAUDE_PIORAR              NUMBER(1)        NOT NULL,
  IE_SAUDE_EXCELENTE           NUMBER(1)        NOT NULL,
  QT_CAPACIDADE_FUNCIONAL      NUMBER(3),
  QT_LIMITACAO_ASPECTO_FISICO  NUMBER(3),
  QT_DOR                       NUMBER(3),
  QT_ESTADO_GERAL_SAUDE        NUMBER(3),
  QT_VITALIDADE                NUMBER(3),
  QT_ASPECTOS_SOCIAIS          NUMBER(3),
  QT_ASPECTOS_EMOCIONAIS       NUMBER(3),
  QT_SAUDE_MENTAL              NUMBER(3),
  QT_PONTUACAO                 NUMBER(10),
  IE_SITUACAO                  VARCHAR2(1 BYTE),
  DT_INATIVACAO                DATE,
  NM_USUARIO_INATIVACAO        VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA             VARCHAR2(255 BYTE),
  NR_HORA                      NUMBER(2),
  NR_SEQ_ASSINATURA            NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO    NUMBER(10),
  NR_SEQ_FITNESS               NUMBER(10),
  NR_SEQ_REG_ELEMENTO          NUMBER(10),
  IE_NIVEL_ATENCAO             VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCQUAV_ATEPACI_FK_I ON TASY.ESCALA_QUALIDADE_VIDA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCQUAV_EHRREEL_FK_I ON TASY.ESCALA_QUALIDADE_VIDA
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCQUAV_HFPPACI_FK_I ON TASY.ESCALA_QUALIDADE_VIDA
(NR_SEQ_FITNESS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCQUAV_HFPPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCQUAV_PESFISI_FK_I ON TASY.ESCALA_QUALIDADE_VIDA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCQUAV_PK ON TASY.ESCALA_QUALIDADE_VIDA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCQUAV_TASASDI_FK_I ON TASY.ESCALA_QUALIDADE_VIDA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCQUAV_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCQUAV_TASASDI_FK2_I ON TASY.ESCALA_QUALIDADE_VIDA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCQUAV_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_qualidade_vida_atual
before insert or update ON TASY.ESCALA_QUALIDADE_VIDA for each row
declare

qt_questao_1_w	number(3,1);
qt_questao_3_w	number(2,0);
qt_questao_4_w	number(2,0);
qt_questao_5_w	number(2,0);
qt_questao_6_w	number(2,0);
qt_questao_7_w	number(3,1);
qt_questao_8_w	number(4,2);
qt_questao_9_vitalidade_w	number(2,0);
qt_questao_9_saude_mental_w	number(2,0);
qt_questao_10_w	number(2,0);
qt_questao_11_w	number(2,0);
qt_reg_w	number(1);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if		(:new.ie_saude = 1)	then
	qt_questao_1_w	:=	5;
elsif	(:new.ie_saude = 2)	then
	qt_questao_1_w	:=	4.4;
elsif	(:new.ie_saude = 3)	then
	qt_questao_1_w	:=	3.4;
elsif	(:new.ie_saude = 4)	then
	qt_questao_1_w	:=	2;
elsif	(:new.ie_saude = 5)	then
	qt_questao_1_w	:=	1;
end if;

qt_questao_3_w	:=	:new.ie_atividade_rigorosa +
					:new.ie_atividade_moderada +
					:new.ie_levantar +
					:new.ie_subir_varios_lances +
					:new.ie_subir_um_lance +
					:new.ie_curvarse +
					:new.ie_andar_um_quilometro +
					:new.ie_andar_quarteiroes +
					:new.ie_andar_um_quarteirao +
					:new.ie_tomar_banho_vestirse;

qt_questao_4_w	:=	:new.IE_DIMINUI_TRABALHO_FISICA +
					:new.IE_MENOS_TAREFAS_FISICA +
					:new.IE_LIMIT_TRABALHO_FISICA +
					:new.IE_DIFIC_TRABALHO_FISICA;

qt_questao_5_w	:=	:new.IE_DIMINUI_TRABALHO_EMOC +
					:new.IE_MENOS_TAREFAS_EMOC +
					:new.IE_ATIV_CUIDADO;

qt_questao_6_w	 :=	:new.IE_ATIVIDADES_SOCIAIS;

if		(:new.IE_DOR_CORPO = 1)	then
	qt_questao_7_w	:=	6;
elsif	(:new.IE_DOR_CORPO = 2)	then
	qt_questao_7_w	:=	5.4;
elsif	(:new.IE_DOR_CORPO = 3)	then
	qt_questao_7_w	:=	4.2;
elsif	(:new.IE_DOR_CORPO = 4)	then
	qt_questao_7_w	:=	3.1;
elsif	(:new.IE_DOR_CORPO = 5)	then
	qt_questao_7_w	:=	2;
elsif	(:new.IE_DOR_CORPO = 6)	then
	qt_questao_7_w	:=	1;
end if;

if	(:new.IE_DOR_CORPO is not null) then
	if	(:new.IE_DOR_CORPO = 1)	and	(:new.IE_DOR_TRABALHO = 1) then
		qt_questao_8_w	:=	6;
	else
		if		(:new.IE_DOR_TRABALHO = 1) then
				qt_questao_8_w	:=	6;
		elsif	(:new.IE_DOR_TRABALHO = 2) then
			qt_questao_8_w	:=	4;
		elsif	(:new.IE_DOR_TRABALHO = 3) then
			qt_questao_8_w	:=	3;
		elsif	(:new.IE_DOR_TRABALHO = 4) then
			qt_questao_8_w	:=	2;
		elsif	(:new.IE_DOR_TRABALHO = 5) then
			qt_questao_8_w	:=	1;
		end if;
	end if;
else
	if		(:new.IE_DOR_TRABALHO = 1) then
		qt_questao_8_w	:=	6;
	elsif	(:new.IE_DOR_TRABALHO = 2) then
		qt_questao_8_w	:=	4.75;
	elsif	(:new.IE_DOR_TRABALHO = 3) then
		qt_questao_8_w	:=	3.5;
	elsif	(:new.IE_DOR_TRABALHO = 4) then
		qt_questao_8_w	:=	2.25;
	elsif	(:new.IE_DOR_TRABALHO = 5) then
		qt_questao_8_w	:=	1;
	end if;
end if;

qt_questao_9_vitalidade_w	:=	:new.IE_VIGOR +
								:new.IE_ENERGIA +
								:new.IE_ESGOTADO +
								:new.IE_CANSADO;

qt_questao_9_saude_mental_w	:=	:new.IE_NERVOSA +
								:new.IE_DEPRIMIDO +
								:new.IE_CALMO +
								:new.IE_DESANIMADO +
								:new.IE_FELIZ;

qt_questao_10_w	:=	:new.IE_TEMPO_ATIVIDADES;

qt_questao_11_w	:=	:new.IE_OBEDECE_FACILMENTE +
					:new.IE_SAUDAVEL_OUTROS +
					:new.IE_SAUDE_PIORAR +
					:new.IE_SAUDE_EXCELENTE;

:new.qt_capacidade_funcional		:=	dividir((qt_questao_3_w - 10),20) * 100;
:new.qt_limitacao_aspecto_fisico	:=	dividir((qt_questao_4_w - 4), 4) * 100;
:new.qt_dor							:=	dividir(((qt_questao_7_w + qt_questao_8_w) - 2),10) * 100;
:new.qt_estado_geral_saude			:=	dividir(((qt_questao_1_w + qt_questao_11_w) - 5),20) * 100;
:new.qt_vitalidade					:=	dividir((qt_questao_9_vitalidade_w - 4),20) * 100;
:new.qt_aspectos_sociais			:=	dividir(((qt_questao_6_w + qt_questao_10_w) - 2),8) * 100;
:new.qt_aspectos_emocionais			:=	dividir((qt_questao_5_w - 3),3) * 100;
:new.qt_saude_mental				:=	dividir((qt_questao_9_saude_mental_w - 5),25) * 100;

:new.qt_pontuacao				:= 	nvl(:new.qt_capacidade_funcional,0)	+
							nvl(:new.qt_limitacao_aspecto_fisico,0)	+
							nvl(:new.qt_dor,0)	+
							nvl(:new.qt_estado_geral_saude,0)	+
							nvl(:new.qt_vitalidade,0)	+
							nvl(:new.qt_aspectos_sociais,0)	+
							nvl(:new.qt_aspectos_emocionais,0)	+
							nvl(:new.qt_saude_mental,0);

<<Final>>
qt_reg_w	:= 0;


end escala_qualidade_vida_atual;
/


CREATE OR REPLACE TRIGGER TASY.escala_qual_vida_pend_atual
after insert or update ON TASY.ESCALA_QUALIDADE_VIDA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'37');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_QUALIDADE_VIDA_delete
after delete ON TASY.ESCALA_QUALIDADE_VIDA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '37'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_QUALIDADE_VIDA ADD (
  CONSTRAINT ESCQUAV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_QUALIDADE_VIDA ADD (
  CONSTRAINT ESCQUAV_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCQUAV_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCQUAV_HFPPACI_FK 
 FOREIGN KEY (NR_SEQ_FITNESS) 
 REFERENCES TASY.HFP_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT ESCQUAV_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCQUAV_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCQUAV_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_QUALIDADE_VIDA TO NIVEL_1;

