ALTER TABLE TASY.DISPOSITIVO_ACESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DISPOSITIVO_ACESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DISPOSITIVO_ACESSO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DISPOSITIVO   NUMBER(10)               NOT NULL,
  DS_ACCESS            VARCHAR2(50 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DISPACE_DISPOSI_FK_I ON TASY.DISPOSITIVO_ACESSO
(NR_SEQ_DISPOSITIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DISPACE_PK ON TASY.DISPOSITIVO_ACESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DISPOSITIVO_ACESSO ADD (
  CONSTRAINT DISPACE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.DISPOSITIVO_ACESSO ADD (
  CONSTRAINT DISPACE_DISPOSI_FK 
 FOREIGN KEY (NR_SEQ_DISPOSITIVO) 
 REFERENCES TASY.DISPOSITIVO (NR_SEQUENCIA));

