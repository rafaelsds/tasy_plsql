ALTER TABLE TASY.PLS_CONV_ITEM_CONV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONV_ITEM_CONV CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONV_ITEM_CONV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  IE_QT_ITEM_PRINC     VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_GRAU_PARTIC   NUMBER(10),
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10)               NOT NULL,
  NR_SEQ_MATERIAL      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCVIC_PK ON TASY.PLS_CONV_ITEM_CONV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCVIC_PLSCVITEM_FK_I ON TASY.PLS_CONV_ITEM_CONV
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCVIC_PLSGRPA_FK_I ON TASY.PLS_CONV_ITEM_CONV
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCVIC_PLSMAT_FK_I ON TASY.PLS_CONV_ITEM_CONV
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCVIC_PROCEDI_FK_I ON TASY.PLS_CONV_ITEM_CONV
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CONV_ITEM_CONV_tp  after update ON TASY.PLS_CONV_ITEM_CONV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_CONV_ITEM_CONV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_QT_ITEM_PRINC,1,4000),substr(:new.IE_QT_ITEM_PRINC,1,4000),:new.nm_usuario,nr_seq_w,'IE_QT_ITEM_PRINC',ie_log_w,ds_w,'PLS_CONV_ITEM_CONV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRAU_PARTIC,1,4000),substr(:new.NR_SEQ_GRAU_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRAU_PARTIC',ie_log_w,ds_w,'PLS_CONV_ITEM_CONV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_conv_item_conv_atual
before update or insert or delete ON TASY.PLS_CONV_ITEM_CONV for each row
declare

nr_seq_regra_w		pls_conv_item_fat.nr_sequencia%type;

begin

if	(deleting) then
	nr_seq_regra_w := :old.nr_seq_regra;
else
	nr_seq_regra_w := :new.nr_seq_regra;
end if;

update	pls_conv_item_fat
set	dt_alteracao_estrutura = sysdate
where	nr_sequencia = nr_seq_regra_w;

end;
/


ALTER TABLE TASY.PLS_CONV_ITEM_CONV ADD (
  CONSTRAINT PLSCVIC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONV_ITEM_CONV ADD (
  CONSTRAINT PLSCVIC_PLSCVITEM_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_CONV_ITEM_FAT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSCVIC_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSCVIC_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSCVIC_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONV_ITEM_CONV TO NIVEL_1;

