ALTER TABLE TASY.RP_PAC_MODELO_AGENDAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RP_PAC_MODELO_AGENDAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RP_PAC_MODELO_AGENDAMENTO
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  DT_INICIO_MODELO              DATE,
  DT_FIM_TRATAMENTO             DATE,
  NR_SEQ_PAC_REAB               NUMBER(10)      NOT NULL,
  NR_SEQ_MOTIVO_FIM_TRATAMENTO  NUMBER(10),
  NR_SEQ_MODELO_AGENDAMENTO     NUMBER(10)      NOT NULL,
  DT_INATIVACAO                 DATE,
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DT_INICIO_AGENDAMENTO         DATE,
  DT_FIM_AGENDAMENTO            DATE,
  CD_CONVENIO                   NUMBER(5),
  CD_CATEGORIA                  VARCHAR2(10 BYTE),
  CD_PLANO                      VARCHAR2(10 BYTE),
  CD_TIPO_ACOMODACAO            NUMBER(4),
  DT_LIBERACAO                  DATE,
  NM_USUARIO_LIB                VARCHAR2(15 BYTE),
  DT_DESFEITA                   DATE,
  NR_SEQ_TRATAMENTO             NUMBER(10),
  NR_SEQ_ROOM                   NUMBER(10),
  IE_TIPO_TRATAMENTO            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RPPAMOA_CONPLAN_FK_I ON TASY.RP_PAC_MODELO_AGENDAMENTO
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPPAMOA_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPPAMOA_CONVENI_FK_I ON TASY.RP_PAC_MODELO_AGENDAMENTO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPPAMOA_CONVENI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RPPAMOA_PK ON TASY.RP_PAC_MODELO_AGENDAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RPPAMOA_RPFIMMO_FK_I ON TASY.RP_PAC_MODELO_AGENDAMENTO
(NR_SEQ_MOTIVO_FIM_TRATAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPPAMOA_RPFIMMO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPPAMOA_RPMODAG_FK_I ON TASY.RP_PAC_MODELO_AGENDAMENTO
(NR_SEQ_MODELO_AGENDAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPPAMOA_RPMODAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPPAMOA_RPPACRE_FK_I ON TASY.RP_PAC_MODELO_AGENDAMENTO
(NR_SEQ_PAC_REAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RPPAMOA_RPPACRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RPPAMOA_RPTRATA_FK_I ON TASY.RP_PAC_MODELO_AGENDAMENTO
(NR_SEQ_TRATAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.RP_PAC_MODELO_AGEND_DELETE
before delete ON TASY.RP_PAC_MODELO_AGENDAMENTO for each row
declare
qt_vagas_restantes_w	number(10);
qt_lista_espera_w	number(10);

begin
--rp_insert_log_movto_modelo(:new.nr_seq_modelo_agendamento,wheb_usuario_pck.get_nm_usuario);
qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:old.nr_seq_modelo_agendamento) + 1;
qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:old.nr_seq_modelo_agendamento);

insert into rp_log_movto_modelo (
	NR_SEQUENCIA,
	NR_SEQ_MODELO,
	DT_ATUALIZACAO,
	NM_USUARIO,
	DT_ATUALIZACAO_NREC,
	NM_USUARIO_NREC,
	DT_LOG,
	QT_VAGAS_RESTANTES,
	QT_LISTA_ESPERA        )
values(	rp_log_movto_modelo_seq.nextval,
	:old.nr_seq_modelo_agendamento,
	sysdate,
	wheb_usuario_pck.get_nm_usuario,
	sysdate,
	wheb_usuario_pck.get_nm_usuario,
	sysdate,
	qt_vagas_restantes_w,
	qt_lista_espera_w);

end;
/


CREATE OR REPLACE TRIGGER TASY.RP_PAC_MODELO_AGEND_INSERT
After Insert ON TASY.RP_PAC_MODELO_AGENDAMENTO for each row
declare
qt_vagas_restantes_w	number(10);
qt_lista_espera_w	number(10);
qt_valida_log_existe_w	number(10);
begin

select	count(*)
into	qt_valida_log_existe_w
from	rp_log_movto_modelo
where   nr_seq_modelo = :new.nr_seq_modelo_agendamento
and	dt_atualizacao = sysdate;

if	(qt_valida_log_existe_w = 0) then

	qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:new.nr_seq_modelo_agendamento) - 1;
	qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:new.nr_seq_modelo_agendamento);

	insert into rp_log_movto_modelo (
		NR_SEQUENCIA,
		NR_SEQ_MODELO,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DT_LOG,
		QT_VAGAS_RESTANTES,
		QT_LISTA_ESPERA        )
	values(	rp_log_movto_modelo_seq.nextval,
		:new.nr_seq_modelo_agendamento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		qt_vagas_restantes_w,
		qt_lista_espera_w);
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.RP_PAC_MODELO_AGEND_UPDATE
After Update ON TASY.RP_PAC_MODELO_AGENDAMENTO for each row
declare
qt_vagas_restantes_w	number(10);
qt_lista_espera_w	number(10);

begin
if	((:new.DT_FIM_TRATAMENTO is not null) and
	((:old.DT_FIM_TRATAMENTO <> :new.DT_FIM_TRATAMENTO) or
	(:old.DT_FIM_TRATAMENTO is null))) or
	((:new.ie_situacao <> :old.ie_situacao) and
	(:new.ie_situacao = 'I') and
	(:new.dt_fim_tratamento is null)) then
	qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:new.nr_seq_modelo_agendamento) + 1;
	qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:new.nr_seq_modelo_agendamento);

	insert into rp_log_movto_modelo (
		NR_SEQUENCIA,
		NR_SEQ_MODELO,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DT_LOG,
		QT_VAGAS_RESTANTES,
		QT_LISTA_ESPERA        )
	values(	rp_log_movto_modelo_seq.nextval,
		:new.nr_seq_modelo_agendamento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		qt_vagas_restantes_w,
		qt_lista_espera_w);



elsif	((:new.nr_seq_modelo_agendamento <> :old.nr_seq_modelo_agendamento) and
	(:new.ie_situacao = 'A')) or
	((:new.ie_situacao <> :old.ie_situacao) and
	(:new.ie_situacao = 'A') and
	(:new.dt_fim_tratamento is null))	then
	--rp_insert_log_movto_modelo(:new.nr_seq_modelo_agendamento,wheb_usuario_pck.get_nm_usuario);
	qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:new.nr_seq_modelo_agendamento) - 1;
	qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:new.nr_seq_modelo_agendamento);

	insert into rp_log_movto_modelo (
		NR_SEQUENCIA,
		NR_SEQ_MODELO,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DT_LOG,
		QT_VAGAS_RESTANTES,
		QT_LISTA_ESPERA        )
	values(	rp_log_movto_modelo_seq.nextval,
		:new.nr_seq_modelo_agendamento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		qt_vagas_restantes_w,
		qt_lista_espera_w);
end if;

if	((:new.nr_seq_modelo_agendamento <> :old.nr_seq_modelo_agendamento) and
	(:new.ie_situacao = 'A')) then
	--rp_insert_log_movto_modelo(:old.nr_seq_modelo_agendamento,wheb_usuario_pck.get_nm_usuario);
	qt_vagas_restantes_w	:= Rp_obter_qtd_vaga_restante(:old.nr_seq_modelo_agendamento) + 1;
	qt_lista_espera_w	:= rp_obter_lista_espera_modelo(:old.nr_seq_modelo_agendamento);

	insert into rp_log_movto_modelo (
		NR_SEQUENCIA,
		NR_SEQ_MODELO,
		DT_ATUALIZACAO,
		NM_USUARIO,
		DT_ATUALIZACAO_NREC,
		NM_USUARIO_NREC,
		DT_LOG,
		QT_VAGAS_RESTANTES,
		QT_LISTA_ESPERA        )
	values(	rp_log_movto_modelo_seq.nextval,
		:old.nr_seq_modelo_agendamento,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		wheb_usuario_pck.get_nm_usuario,
		sysdate,
		qt_vagas_restantes_w,
		qt_lista_espera_w);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.RP_PAC_MODELO_AGENDAMENTO_tp  after update ON TASY.RP_PAC_MODELO_AGENDAMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_ROOM,1,4000),substr(:new.NR_SEQ_ROOM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ROOM',ie_log_w,ds_w,'RP_PAC_MODELO_AGENDAMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.RP_PAC_MODELO_AGENDAMENTO ADD (
  CONSTRAINT RPPAMOA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RP_PAC_MODELO_AGENDAMENTO ADD (
  CONSTRAINT RPPAMOA_RPTRATA_FK 
 FOREIGN KEY (NR_SEQ_TRATAMENTO) 
 REFERENCES TASY.RP_TRATAMENTO (NR_SEQUENCIA),
  CONSTRAINT RPPAMOA_RPFIMMO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_FIM_TRATAMENTO) 
 REFERENCES TASY.RP_FIM_MODELO_AGENDAMENTO (NR_SEQUENCIA),
  CONSTRAINT RPPAMOA_RPPACRE_FK 
 FOREIGN KEY (NR_SEQ_PAC_REAB) 
 REFERENCES TASY.RP_PACIENTE_REABILITACAO (NR_SEQUENCIA),
  CONSTRAINT RPPAMOA_RPMODAG_FK 
 FOREIGN KEY (NR_SEQ_MODELO_AGENDAMENTO) 
 REFERENCES TASY.RP_MODELO_AGENDAMENTO (NR_SEQUENCIA),
  CONSTRAINT RPPAMOA_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT RPPAMOA_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.RP_PAC_MODELO_AGENDAMENTO TO NIVEL_1;

