DROP TABLE TASY.W_VL_INFEC_CLIN_TOP CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_VL_INFEC_CLIN_TOP
(
  NR_ABS         NUMBER(5),
  VL_PERC        NUMBER(5,2),
  CD_CLINICA     NUMBER(5),
  CD_TOPOGRAFIA  NUMBER(5),
  NM_USUARIO     VARCHAR2(15 BYTE)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_VL_INFEC_CLIN_TOP TO NIVEL_1;

