ALTER TABLE TASY.MPREV_PROG_PARTIC_PROF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_PROG_PARTIC_PROF CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_PROG_PARTIC_PROF
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_PROGRAMA_PARTIC  NUMBER(10),
  CD_PROFISSIONAL         VARCHAR2(10 BYTE),
  IE_FORMA_ATENDIMENTO    VARCHAR2(2 BYTE),
  DT_INICIO_ACOMP         DATE                  NOT NULL,
  DT_FIM_ACOMP            DATE,
  NR_SEQ_PARTICIPANTE     NUMBER(10)            NOT NULL,
  NR_SEQ_EQUIPE           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRPPPR_MPREQUI_FK_I ON TASY.MPREV_PROG_PARTIC_PROF
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRPPPR_MPRPART_FK_I ON TASY.MPREV_PROG_PARTIC_PROF
(NR_SEQ_PARTICIPANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRPPPR_MPRPRPA_FK_I ON TASY.MPREV_PROG_PARTIC_PROF
(NR_SEQ_PROGRAMA_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRPPPR_PESFISI_FK_I ON TASY.MPREV_PROG_PARTIC_PROF
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRPPPR_PK ON TASY.MPREV_PROG_PARTIC_PROF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.mprev_prog_partic_prof_insert
before insert ON TASY.MPREV_PROG_PARTIC_PROF for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade: Apenas tratar para alimentar tamb�m o sequencial do participante para que apare�a no profissional respons�vel
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  X]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

nr_seq_participante_w	mprev_participante.nr_sequencia%type;

begin

if	(:new.nr_seq_participante is null) and
	(:new.nr_seq_programa_partic is not null) then
	select	a.nr_seq_participante
	into	nr_seq_participante_w
	from	mprev_programa_partic a
	where	a.nr_sequencia = :new.nr_seq_programa_partic;

	:new.nr_seq_participante := nr_seq_participante_w;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MPREV_PROG_PARTIC_PROF_ATUAL
before insert or update ON TASY.MPREV_PROG_PARTIC_PROF FOR EACH ROW
DECLARE

BEGIN

if	(trunc(:new.dt_fim_acomp) < trunc(:new.dt_inicio_acomp)) then
	wheb_mensagem_pck.Exibir_Mensagem_Abort(832205);
end if;

END;
/


ALTER TABLE TASY.MPREV_PROG_PARTIC_PROF ADD (
  CONSTRAINT MPRPPPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_PROG_PARTIC_PROF ADD (
  CONSTRAINT MPRPPPR_MPREQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.MPREV_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT MPRPPPR_MPRPRPA_FK 
 FOREIGN KEY (NR_SEQ_PROGRAMA_PARTIC) 
 REFERENCES TASY.MPREV_PROGRAMA_PARTIC (NR_SEQUENCIA),
  CONSTRAINT MPRPPPR_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MPRPPPR_MPRPART_FK 
 FOREIGN KEY (NR_SEQ_PARTICIPANTE) 
 REFERENCES TASY.MPREV_PARTICIPANTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_PROG_PARTIC_PROF TO NIVEL_1;

