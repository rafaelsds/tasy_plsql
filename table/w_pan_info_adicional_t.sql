DROP TABLE TASY.W_PAN_INFO_ADICIONAL_T CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_PAN_INFO_ADICIONAL_T
(
  CD_UNIDADE_BASICA   VARCHAR2(10 BYTE),
  CD_UNIDADE_COMPL    VARCHAR2(10 BYTE),
  CD_EXP_LABEL        NUMBER(10),
  NR_SEQ_TEXTO_LABEL  NUMBER(10),
  CD_PERFIL           NUMBER(5),
  DS_INFO_ADIC        VARCHAR2(255 BYTE),
  CD_PESSOA_FISICA    VARCHAR2(10 BYTE),
  NR_INFO_ADIC        NUMBER(15,4),
  DT_INFO_ADIC        DATE,
  NR_SEQ_APRES        NUMBER(10),
  DS_MASCARA          VARCHAR2(100 BYTE),
  NM_USUARIO          VARCHAR2(15 BYTE),
  NR_ATENDIMENTO      NUMBER(10),
  NR_SEQ_INTERNO      NUMBER(10),
  IE_ATRASADO         VARCHAR2(1 BYTE),
  IE_CAMPO_PANORAMA   VARCHAR2(5 BYTE),
  NR_SEQ_VAGA         NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE INDEX TASY.WPINFADT_ATEPAC_FK_II ON TASY.W_PAN_INFO_ADICIONAL_T
(NR_ATENDIMENTO, CD_PESSOA_FISICA);


CREATE INDEX TASY.WPINFADT_ATEPACI_FK_I ON TASY.W_PAN_INFO_ADICIONAL_T
(NR_ATENDIMENTO);


CREATE INDEX TASY.WPINFADT_ATEPACI_FK_II ON TASY.W_PAN_INFO_ADICIONAL_T
(CD_PESSOA_FISICA, NR_ATENDIMENTO);


CREATE INDEX TASY.WPINFADT_GESVAGA_FK_I ON TASY.W_PAN_INFO_ADICIONAL_T
(NR_SEQ_VAGA);


CREATE INDEX TASY.WPINFADT_PESFISI_FK_I ON TASY.W_PAN_INFO_ADICIONAL_T
(CD_PESSOA_FISICA);


GRANT SELECT ON TASY.W_PAN_INFO_ADICIONAL_T TO NIVEL_1;

