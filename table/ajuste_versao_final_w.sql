DROP TABLE TASY.AJUSTE_VERSAO_FINAL_W CASCADE CONSTRAINTS;

CREATE TABLE TASY.AJUSTE_VERSAO_FINAL_W
(
  DS_CAMPO  VARCHAR2(4000 BYTE),
  NR_ORDEM  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.AJUSTE_VERSAO_FINAL_W TO NIVEL_1;

