ALTER TABLE TASY.HL7_PROJETO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HL7_PROJETO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HL7_PROJETO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  IE_VERSAO_OFICIAL      VARCHAR2(1 BYTE)       NOT NULL,
  DS_PROJETO             VARCHAR2(255 BYTE)     NOT NULL,
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  CD_VERSAO              NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_VERSAO_OFICIAL  NUMBER(10),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_SISTEMA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.HL7PROJ_PK ON TASY.HL7_PROJETO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HL7PROJ_UK ON TASY.HL7_PROJETO
(NR_SEQ_SISTEMA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.HL7_PROJETO_tp  after update ON TASY.HL7_PROJETO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'HL7_PROJETO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'HL7_PROJETO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_VERSAO_OFICIAL,1,4000),substr(:new.NR_SEQ_VERSAO_OFICIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_VERSAO_OFICIAL',ie_log_w,ds_w,'HL7_PROJETO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'HL7_PROJETO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VERSAO_OFICIAL,1,4000),substr(:new.IE_VERSAO_OFICIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VERSAO_OFICIAL',ie_log_w,ds_w,'HL7_PROJETO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_SISTEMA,1,4000),substr(:new.NR_SEQ_SISTEMA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SISTEMA',ie_log_w,ds_w,'HL7_PROJETO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.HL7_PROJETO ADD (
  CONSTRAINT HL7PROJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT HL7PROJ_UK
 UNIQUE (NR_SEQ_SISTEMA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HL7_PROJETO ADD (
  CONSTRAINT HL7PROJ_SISINTE_FK 
 FOREIGN KEY (NR_SEQ_SISTEMA) 
 REFERENCES TASY.SISTEMA_INTEGRACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.HL7_PROJETO TO NIVEL_1;

