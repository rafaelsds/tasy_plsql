ALTER TABLE TASY.REGRA_PERM_PANORAMA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_PERM_PANORAMA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_PERM_PANORAMA
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  CD_PERFIL                      NUMBER(5),
  IE_PERMITE_DEFINIR_RETAGUARDA  VARCHAR2(1 BYTE),
  QT_JANELAS_SIMULTANEAS         NUMBER(10),
  QT_TAMANHO_FONTE               NUMBER(10),
  IE_ENCERRA_PANORAMA            VARCHAR2(1 BYTE),
  IE_MOVIMENTA_PACIENTE          VARCHAR2(1 BYTE),
  NR_MODELO_SUEP                 NUMBER(10),
  CD_ESTABELECIMENTO_REGRA       NUMBER(4)      DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REPERPA_ESTABEL_FK_I ON TASY.REGRA_PERM_PANORAMA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPERPA_PERFIL_FK_I ON TASY.REGRA_PERM_PANORAMA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REPERPA_PK ON TASY.REGRA_PERM_PANORAMA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPERPA_SUEP_FK_I ON TASY.REGRA_PERM_PANORAMA
(NR_MODELO_SUEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.REGRA_PERM_PANORAMA_UPDATE
before update ON TASY.REGRA_PERM_PANORAMA for each row
declare
qt_registros_w	number(10,0);

begin
if (:old.nr_modelo_suep <> :new.nr_modelo_suep) then
	select	count(1)
	into	qt_registros_w
	from	regra_perm_pan_icone_suep a,
			regra_perm_panorama_icone b
	where	a.nr_icone_modelo = b.nr_sequencia
	and		b.nr_seq_regra = :new.nr_sequencia;

	if (qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1039653);
		--'N�o � poss�vel alterar o modelo do SUEP da regra atual, pois h� icones que possuem cards vinculados para este modelo! Para que seja poss�vel alterar o modelo, deve-se excluir dos �cones os cards liberados do modelo anterior, ou criar uma nova regra com o modelo desejado.'
	end if;
end if;

end;
/


ALTER TABLE TASY.REGRA_PERM_PANORAMA ADD (
  CONSTRAINT REPERPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_PERM_PANORAMA ADD (
  CONSTRAINT REPERPA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REPERPA_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT REPERPA_SUEP_FK 
 FOREIGN KEY (NR_MODELO_SUEP) 
 REFERENCES TASY.SUEP (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_PERM_PANORAMA TO NIVEL_1;

