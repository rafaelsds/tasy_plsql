ALTER TABLE TASY.SMSHAREDORACLECONFIGURATION_S
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SMSHAREDORACLECONFIGURATION_S CASCADE CONSTRAINTS;

CREATE TABLE TASY.SMSHAREDORACLECONFIGURATION_S
(
  NAMEDOBJECT_ID_SEQUENCEID_  NUMBER(10),
  NAMEDOBJECT_ID_OBJECTTYPE_  NUMBER(10),
  SOCONFIG_NAME_              VARCHAR2(512 BYTE),
  SOCONFIG_OS_                VARCHAR2(512 BYTE),
  SOCONFIG_SOSETTING_         NUMBER(10),
  SOCONFIG_KIND_              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


ALTER TABLE TASY.SMSHAREDORACLECONFIGURATION_S ADD (
  PRIMARY KEY
 (NAMEDOBJECT_ID_SEQUENCEID_)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SMSHAREDORACLECONFIGURATION_S TO NIVEL_1;

