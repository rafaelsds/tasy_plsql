ALTER TABLE TASY.CTB_PLANO_ANS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_PLANO_ANS CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_PLANO_ANS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_EMPRESA           NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_PLANO             NUMBER(10)               NOT NULL,
  CD_CLASSIFICACAO     VARCHAR2(40 BYTE)        NOT NULL,
  DS_CONTA             VARCHAR2(255 BYTE)       NOT NULL,
  IE_TIPO              NUMBER(3)                NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_VERSAO            VARCHAR2(1 BYTE),
  NR_SEQ_VERSAO_PLANO  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBPLAN_CTBVERANS_FK_I ON TASY.CTB_PLANO_ANS
(NR_SEQ_VERSAO_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBPLAN_EMPRESA_FK_I ON TASY.CTB_PLANO_ANS
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBPLAN_EMPRESA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CTBPLAN_PK ON TASY.CTB_PLANO_ANS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBPLAN_UK ON TASY.CTB_PLANO_ANS
(CD_EMPRESA, CD_PLANO, IE_VERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBPLAN_UK
  MONITORING USAGE;


ALTER TABLE TASY.CTB_PLANO_ANS ADD (
  CONSTRAINT CTBPLAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_PLANO_ANS ADD (
  CONSTRAINT CTBPLAN_CTBVERANS_FK 
 FOREIGN KEY (NR_SEQ_VERSAO_PLANO) 
 REFERENCES TASY.CTB_VERSAO_PLANO_ANS (NR_SEQUENCIA),
  CONSTRAINT CTBPLAN_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.CTB_PLANO_ANS TO NIVEL_1;

