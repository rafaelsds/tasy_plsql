ALTER TABLE TASY.HL7_SEGMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HL7_SEGMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HL7_SEGMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_APRESENTACAO  NUMBER(10)               NOT NULL,
  NR_SEQ_MENSAGEM      NUMBER(10)               NOT NULL,
  NM_SEGMENTO          VARCHAR2(255 BYTE)       NOT NULL,
  DS_SEGMENTO          VARCHAR2(255 BYTE)       NOT NULL,
  IE_TIPO              VARCHAR2(3 BYTE)         NOT NULL,
  DS_SQL               VARCHAR2(4000 BYTE),
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  IE_CRIAR_SEGMENTO    VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HL7SEGM_HL7MENS_FK_I ON TASY.HL7_SEGMENTO
(NR_SEQ_MENSAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HL7SEGM_PK ON TASY.HL7_SEGMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.HL7_SEGMENTO_tp  after update ON TASY.HL7_SEGMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_SEGMENTO,1,4000),substr(:new.DS_SEGMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_SEGMENTO',ie_log_w,ds_w,'HL7_SEGMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SQL,1,4000),substr(:new.DS_SQL,1,4000),:new.nm_usuario,nr_seq_w,'DS_SQL',ie_log_w,ds_w,'HL7_SEGMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_SEGMENTO,1,4000),substr(:new.NM_SEGMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_SEGMENTO',ie_log_w,ds_w,'HL7_SEGMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'HL7_SEGMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.HL7_SEGMENTO ADD (
  CONSTRAINT HL7SEGM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HL7_SEGMENTO ADD (
  CONSTRAINT HL7SEGM_HL7MENS_FK 
 FOREIGN KEY (NR_SEQ_MENSAGEM) 
 REFERENCES TASY.HL7_MENSAGEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.HL7_SEGMENTO TO NIVEL_1;

