ALTER TABLE TASY.REGRA_STATUS_PRESCR_OPM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_STATUS_PRESCR_OPM CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_STATUS_PRESCR_OPM
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_MOMENTO                VARCHAR2(15 BYTE)   NOT NULL,
  NR_SEQ_STATUS_PRESCR_OPM  NUMBER(10)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RESTPRO_PK ON TASY.REGRA_STATUS_PRESCR_OPM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESTPRO_PK
  MONITORING USAGE;


CREATE INDEX TASY.RESTPRO_STPREOP_FK_I ON TASY.REGRA_STATUS_PRESCR_OPM
(NR_SEQ_STATUS_PRESCR_OPM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RESTPRO_STPREOP_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_STATUS_PRESCR_OPM ADD (
  CONSTRAINT RESTPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_STATUS_PRESCR_OPM ADD (
  CONSTRAINT RESTPRO_STPREOP_FK 
 FOREIGN KEY (NR_SEQ_STATUS_PRESCR_OPM) 
 REFERENCES TASY.STATUS_PRESCR_OPM (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_STATUS_PRESCR_OPM TO NIVEL_1;

