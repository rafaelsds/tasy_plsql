ALTER TABLE TASY.W_CPOE_PROC_GLIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_CPOE_PROC_GLIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_CPOE_PROC_GLIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROCEDIMENTO  NUMBER(10),
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  QT_GLIC_INIC         NUMBER(10,1)             NOT NULL,
  QT_GLIC_FIM          NUMBER(10,1)             NOT NULL,
  QT_UI_INSULINA       NUMBER(10,2),
  QT_GLICOSE           NUMBER(5,1),
  DS_SUGESTAO          VARCHAR2(2000 BYTE),
  QT_MINUTOS_MEDICAO   NUMBER(10),
  QT_UI_INSULINA_INT   NUMBER(10,2),
  NR_SEQ_PROC_EDIT     NUMBER(10),
  NR_ATENDIMENTO       NUMBER(10),
  IE_MODIFICADO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WCPGI_ATEPACI_FK_I ON TASY.W_CPOE_PROC_GLIC
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WCPGI_CPOEPRO_FK_I ON TASY.W_CPOE_PROC_GLIC
(NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WCPGI_PEPPRGL_FK_I ON TASY.W_CPOE_PROC_GLIC
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WCPGI_PESFISI_FK_I ON TASY.W_CPOE_PROC_GLIC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WCPGI_PK ON TASY.W_CPOE_PROC_GLIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_CPOE_PROC_GLIC ADD (
  CONSTRAINT WCPGI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_CPOE_PROC_GLIC ADD (
  CONSTRAINT WCPGI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT WCPGI_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT WCPGI_CPOEPRO_FK 
 FOREIGN KEY (NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.CPOE_PROCEDIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WCPGI_PEPPRGL_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PEP_PROTOCOLO_GLICEMIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_CPOE_PROC_GLIC TO NIVEL_1;

