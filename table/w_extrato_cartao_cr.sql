ALTER TABLE TASY.W_EXTRATO_CARTAO_CR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_EXTRATO_CARTAO_CR CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_EXTRATO_CARTAO_CR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_EXTRATO       NUMBER(10)               NOT NULL,
  DS_CONTEUDO          VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WEXTCCR_ESTCCR_FK_I ON TASY.W_EXTRATO_CARTAO_CR
(NR_SEQ_EXTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WEXTCCR_ESTCCR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WEXTCCR_PK ON TASY.W_EXTRATO_CARTAO_CR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WEXTCCR_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_EXTRATO_CARTAO_CR ADD (
  CONSTRAINT WEXTCCR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_EXTRATO_CARTAO_CR ADD (
  CONSTRAINT WEXTCCR_ESTCCR_FK 
 FOREIGN KEY (NR_SEQ_EXTRATO) 
 REFERENCES TASY.EXTRATO_CARTAO_CR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_EXTRATO_CARTAO_CR TO NIVEL_1;

