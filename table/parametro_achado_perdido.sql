ALTER TABLE TASY.PARAMETRO_ACHADO_PERDIDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARAMETRO_ACHADO_PERDIDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARAMETRO_ACHADO_PERDIDO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_STATUS_JOB    NUMBER(10),
  NR_SEQ_STATUS_BAIXA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PAACPER_ESTABEL_FK_I ON TASY.PARAMETRO_ACHADO_PERDIDO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PAACPER_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PAACPER_PK ON TASY.PARAMETRO_ACHADO_PERDIDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PAACPER_STAACPE_FK_I ON TASY.PARAMETRO_ACHADO_PERDIDO
(NR_SEQ_STATUS_JOB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PAACPER_STAACPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PAACPER_STAACPE_FK2_I ON TASY.PARAMETRO_ACHADO_PERDIDO
(NR_SEQ_STATUS_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PAACPER_STAACPE_FK2_I
  MONITORING USAGE;


ALTER TABLE TASY.PARAMETRO_ACHADO_PERDIDO ADD (
  CONSTRAINT PAACPER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARAMETRO_ACHADO_PERDIDO ADD (
  CONSTRAINT PAACPER_STAACPE_FK2 
 FOREIGN KEY (NR_SEQ_STATUS_BAIXA) 
 REFERENCES TASY.STATUS_ACHADO_PERDIDO (NR_SEQUENCIA),
  CONSTRAINT PAACPER_STAACPE_FK 
 FOREIGN KEY (NR_SEQ_STATUS_JOB) 
 REFERENCES TASY.STATUS_ACHADO_PERDIDO (NR_SEQUENCIA),
  CONSTRAINT PAACPER_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PARAMETRO_ACHADO_PERDIDO TO NIVEL_1;

