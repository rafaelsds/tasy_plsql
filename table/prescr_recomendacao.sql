ALTER TABLE TASY.PRESCR_RECOMENDACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_RECOMENDACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_RECOMENDACAO
(
  NR_PRESCRICAO           NUMBER(14)            NOT NULL,
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  IE_DESTINO_REC          VARCHAR2(3 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DS_RECOMENDACAO         VARCHAR2(4000 BYTE),
  QT_RECOMENDACAO         NUMBER(15,4),
  IE_SUSPENSO             VARCHAR2(1 BYTE),
  CD_INTERVALO            VARCHAR2(7 BYTE),
  DS_HORARIOS             VARCHAR2(2000 BYTE),
  NR_SEQ_CLASSIF          NUMBER(10),
  CD_RECOMENDACAO         NUMBER(10),
  DT_SUSPENSAO            DATE,
  NM_USUARIO_SUSP         VARCHAR2(15 BYTE),
  HR_PRIM_HORARIO         VARCHAR2(5 BYTE),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO_SUSP      NUMBER(10),
  DS_MOTIVO_SUSP          VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO         NUMBER(5),
  CD_KIT                  NUMBER(10),
  NR_SEQ_ASSINATURA       NUMBER(10),
  NR_SEQ_INTERNO          NUMBER(10),
  NR_SEQ_APRES            NUMBER(5),
  NR_SEQ_INTERF_FARM      NUMBER(10),
  IE_URGENCIA             VARCHAR2(1 BYTE),
  NR_OCORRENCIA           NUMBER(15,4),
  IE_FAOSE                VARCHAR2(15 BYTE),
  NR_PRESCRICAO_ORIGINAL  NUMBER(14),
  IE_CIENTE               VARCHAR2(1 BYTE),
  DT_SUSPENSAO_PROGR      DATE,
  IE_ESTENDIDO            VARCHAR2(1 BYTE),
  NR_SEQ_ANTERIOR         NUMBER(15),
  DT_EXTENSAO             DATE,
  IE_ALTERAR_HORARIO      VARCHAR2(1 BYTE),
  IE_SE_NECESSARIO        VARCHAR2(1 BYTE),
  NR_SEQ_TOPOGRAFIA       NUMBER(10),
  IE_HORARIO_SUSP         VARCHAR2(1 BYTE),
  IE_ACM                  VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA_SUSP  NUMBER(15),
  NR_SEQ_ORDEM_ADEP       NUMBER(10),
  IE_ERRO                 NUMBER(3),
  DS_COR_ERRO             VARCHAR2(20 BYTE),
  IE_AUXILIAR             VARCHAR2(15 BYTE),
  IE_ENCAMINHAR           VARCHAR2(15 BYTE),
  IE_FAZER                VARCHAR2(15 BYTE),
  IE_ORIENTAR             VARCHAR2(15 BYTE),
  IE_SUPERVISIONAR        VARCHAR2(15 BYTE),
  DS_STACK                VARCHAR2(2000 BYTE),
  DT_INICIO               DATE,
  QT_DIA_PRIM_HOR         NUMBER(1),
  CD_PROTOCOLO            NUMBER(10),
  NR_SEQ_PROTOCOLO        NUMBER(10),
  NR_SEQ_ITEM_PROT        NUMBER(10),
  NR_SEQ_REC_CPOE         NUMBER(10),
  QT_MIN_INTERVALO        NUMBER(5),
  DT_PRIM_HORARIO         DATE,
  IE_KIT_GERADO           VARCHAR2(1 BYTE),
  NR_SEQ_MAT_HOR          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          19M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRESREC_CLARECO_FK_I ON TASY.PRESCR_RECOMENDACAO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESREC_CLARECO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESREC_I1 ON TASY.PRESCR_RECOMENDACAO
(NR_PRESCRICAO_ORIGINAL, NR_SEQ_ANTERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          144K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESREC_I2 ON TASY.PRESCR_RECOMENDACAO
(NR_SEQ_REC_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESREC_I3 ON TASY.PRESCR_RECOMENDACAO
(NR_SEQ_MAT_HOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESREC_MOTSUPR_FK_I ON TASY.PRESCR_RECOMENDACAO
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESREC_MOTSUPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESREC_PERFIL_FK_I ON TASY.PRESCR_RECOMENDACAO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESREC_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRESREC_PK ON TASY.PRESCR_RECOMENDACAO
(NR_PRESCRICAO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESREC_PRESMED_FK_I ON TASY.PRESCR_RECOMENDACAO
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          6M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESREC_TASASDI_FK_I ON TASY.PRESCR_RECOMENDACAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESREC_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESREC_TIPRECO_FK_I ON TASY.PRESCR_RECOMENDACAO
(CD_RECOMENDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESREC_TIPRECO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESREC_TOPDOR_FK_I ON TASY.PRESCR_RECOMENDACAO
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESREC_TOPDOR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRESREC_UK ON TASY.PRESCR_RECOMENDACAO
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESREC_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.prescr_rec_update_hl7_dixtal
AFTER UPDATE ON TASY.PRESCR_RECOMENDACAO FOR EACH ROW
declare

nr_seq_interno_w		number(10);
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);
nr_atendimento_w		number(10);
cd_pessoa_fisica_w	number(10);
ie_leito_monit		varchar2(1);

BEGIN

if	(nvl(:old.ie_suspenso,'N') = 'N') and
	(:new.ie_suspenso = 'S') then

	ds_sep_bv_w := obter_separador_bv;

	select	nr_atendimento, cd_pessoa_fisica
	into	nr_atendimento_w, cd_pessoa_fisica_w
	from 	prescr_medica
	where 	nr_prescricao = :new.nr_prescricao;

	select	max(obter_atepacu_paciente(nr_atendimento_w ,'A'))
	into	nr_seq_interno_w
	from	dual;

	select	nvl(obter_se_leito_atual_monit(nr_atendimento_w),'N')
	into	ie_leito_monit
	from 	dual;

	if (ie_leito_monit = 'S') then
		begin
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||
					'nr_atendimento='   || nr_atendimento_w      || ds_sep_bv_w ||
					'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
					'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
					'nr_seq_recomend='  || :new.nr_sequencia || ds_sep_bv_w;
		gravar_agend_integracao(50, ds_param_integ_hl7_w);
		end;
	end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.SMART_RECOMENDACAO
after update ON TASY.PRESCR_RECOMENDACAO FOR EACH ROW
declare
	reg_integracao_w		gerar_int_padrao.reg_integracao;

BEGIN
begin
	if 	(:old.DT_SUSPENSAO is null) and
		(:new.DT_SUSPENSAO is not null) then
		begin
			gerar_int_padrao.gravar_integracao('300', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';NR_SEQUENCIA=' || :new.NR_SEQUENCIA|| ';DS_OPERACAO=UPDATE');
		END;
	end if;
exception when others then
	null;
end;

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_recomendacao_update
BEFORE UPDATE ON TASY.PRESCR_RECOMENDACAO FOR EACH ROW
BEGIN

/* --> Rafael em 03/09/2007 x ADEP x cfe definido com Marcus <-- */
if	(nvl(:old.ie_suspenso,'N') = 'N') and
	(:new.ie_suspenso = 'S') then

	update	prescr_rec_hor x
	set	dt_suspensao = sysdate,
		nm_usuario_susp = :new.nm_usuario_susp
	where	nr_prescricao = :new.nr_prescricao
	and	nr_seq_recomendacao = :new.nr_sequencia
	and	dt_fim_horario is null
	and	dt_suspensao is null
	and 'N' = substr(nvl(Obter_status_recomendacao(:new.nr_prescricao, :new.nr_sequencia, nr_sequencia), obter_status_hor_rec(dt_fim_horario,dt_suspensao)),1,1);

end if;

if	(:new.ie_se_necessario = 'N')  then
	if ((:new.ds_horarios is not null) and
		(:new.ds_horarios <> 'ACM')) then
		:new.nr_ocorrencia	:= obter_ocorrencias_horarios_rep(:new.ds_horarios);
	elsif 	(:new.cd_intervalo is not null) then
		:new.nr_ocorrencia	:= Obter_ocorrencia_intervalo(:new.cd_intervalo, 24,'O');
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_recomendacao_updt_after
after update ON TASY.PRESCR_RECOMENDACAO for each row
declare

ds_erro_w     	varchar2(2000 char);
ds_stack_w		varchar2(2000 char);
ds_log_w	varchar2(2000 char);
dt_min_date_w	date := to_date('30/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss');

begin
	begin
		if	(nvl(:new.nr_sequencia,0) <> nvl(:old.nr_sequencia,0)) then
			ds_log_w	:= substr(ds_log_w || ' nr_sequencia(' || nvl(:old.nr_sequencia,0) || '/' || nvl(:new.nr_sequencia,0)||'); ',1,2000);
		end if;

		if	(nvl(:new.dt_suspensao, dt_min_date_w) <> nvl(:old.dt_suspensao, dt_min_date_w)) then
			ds_log_w	:= substr(ds_log_w || ' dt_suspensao(' || nvl(to_char(:old.dt_suspensao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_suspensao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
		end if;

		if	(nvl(:new.dt_inicio, sysdate) <> nvl(:old.dt_inicio, sysdate)) then
			ds_log_w	:= substr(ds_log_w || ' dt_inicio(' || nvl(to_char(:old.dt_inicio,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_inicio,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
		end if;

		if	(nvl(:new.ds_horarios,'XPTO') <> nvl(:old.ds_horarios,'XPTO')) then
			ds_log_w	:= substr(ds_log_w || ' ds_horarios(' || nvl(:old.ds_horarios,'<NULL>') || '/' || nvl(:new.ds_horarios,'<NULL>')||'); ',1,2000);
		end if;

		if	(nvl(:new.hr_prim_horario,'XPTO') <> nvl(:old.hr_prim_horario,'XPTO')) then
			ds_log_w	:= substr(ds_log_w || ' hr_prim_horario(' || nvl(:old.hr_prim_horario,'<NULL>') || '/' || nvl(:new.hr_prim_horario,'<NULL>')||'); ',1,2000);
		end if;

		if	(nvl(:new.cd_intervalo,'XPTO') <> nvl(:old.cd_intervalo,'XPTO')) then
			ds_log_w	:= substr(ds_log_w || ' cd_intervalo(' || nvl(:old.cd_intervalo,'<NULL>') || '/' || nvl(:new.cd_intervalo,'<NULL>')||'); ',1,2000);
		end if;

		if	(nvl(:new.ie_alterar_horario,'XPTO') <> nvl(:old.ie_alterar_horario,'XPTO')) then
			ds_log_w	:= substr(ds_log_w || ' ie_alterar_horario(' || nvl(:old.ie_alterar_horario,'<NULL>') || '/' || nvl(:new.ie_alterar_horario,'<NULL>')||'); ',1,2000);
		end if;

		if	(nvl(:new.nr_ocorrencia,0) <> nvl(:old.nr_ocorrencia,0)) then
			ds_log_w	:= substr(ds_log_w || ' nr_ocorrencia(' || nvl(:old.nr_ocorrencia,0) || '/' || nvl(:new.nr_ocorrencia,0)||'); ',1,2000);
		end if;

		if	(nvl(:new.cd_perfil_ativo, 0) <> nvl(:old.cd_perfil_ativo, 0)) then
			ds_log_w	:= substr(ds_log_w || ' cd_perfil_ativo(' || nvl(:old.cd_perfil_ativo, 0) || '/' || nvl(:new.cd_perfil_ativo, 0)||'); ',1,2000);
		end if;

		if	(ds_log_w is not null) then

			if (nvl(:old.nr_sequencia, 0) > 0) then
				ds_log_w := substr('Alteracoes(old/new)= ' || ds_log_w,1,2000);
			else
				ds_log_w := substr('Criacao(old/new)= ' || ds_log_w,1,2000);
			end if;

			ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
			ds_log_w := substr(ds_log_w ||' FUNCAO('||to_char(obter_funcao_ativa)||'); PERFIL('||to_char(obter_perfil_ativo)||')'||ds_stack_w,1,2000);

			gravar_log_tasy(10007, ds_log_w, :new.nm_usuario);
		end if;

	exception
	when others then
		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
		ds_erro_w := substr('EXCEPTION prescr_recomendacao_update_after' || ': ' || sqlerrm(SQLCODE)||ds_stack_w, 1, 2000);

		gravar_log_tasy(10007, ds_erro_w, :new.nm_usuario);
	end;
end;
/


CREATE OR REPLACE TRIGGER TASY.prescr_recomendacao_insert
BEFORE INSERT ON TASY.PRESCR_RECOMENDACAO FOR EACH ROW
BEGIN

if	(:new.nr_seq_interno is null) then
	select prescr_recomendacao_seq.NextVal
	into	:new.nr_seq_interno
	from dual;
end if;

if	(nvl(:new.nr_ocorrencia,0) = 0) and
	(:new.cd_intervalo is not null) then

	:new.nr_ocorrencia	:= Obter_ocorrencia_intervalo(:new.cd_intervalo, 24,'O');

	if (:new.nr_ocorrencia > 0) and
	   (:new.nr_ocorrencia < 1) then
		:new.nr_ocorrencia := 1;
	end if;

end if;

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

if	(:new.ie_suspenso is null) then
	:new.ie_suspenso	:= 'N';
end if;

END;
/


ALTER TABLE TASY.PRESCR_RECOMENDACAO ADD (
  CONSTRAINT PRESREC_PK
 PRIMARY KEY
 (NR_PRESCRICAO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          7M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PRESREC_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          56K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_RECOMENDACAO ADD (
  CONSTRAINT PRESREC_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT PRESREC_TIPRECO_FK 
 FOREIGN KEY (CD_RECOMENDACAO) 
 REFERENCES TASY.TIPO_RECOMENDACAO (CD_TIPO_RECOMENDACAO),
  CONSTRAINT PRESREC_CLARECO_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIFICACAO_RECOMENDACAO (NR_SEQUENCIA),
  CONSTRAINT PRESREC_MOTSUPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_PRESCR (NR_SEQUENCIA),
  CONSTRAINT PRESREC_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PRESREC_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PRESREC_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRESCR_RECOMENDACAO TO NIVEL_1;

