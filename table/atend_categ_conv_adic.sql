ALTER TABLE TASY.ATEND_CATEG_CONV_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_CATEG_CONV_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_CATEG_CONV_ADIC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_INTERNO         NUMBER(10)             NOT NULL,
  DS_CONVENIO_DESEJADO   VARCHAR2(100 BYTE),
  DS_PLANO_DESEJADO      VARCHAR2(255 BYTE),
  DS_CATEGORIA_DESEJADA  VARCHAR2(255 BYTE),
  CD_USUARIO_CONVENIO    VARCHAR2(50 BYTE),
  CD_CONVENIO            NUMBER(5),
  CD_EMPRESA             VARCHAR2(50 BYTE),
  CD_UNIDADE_OPERAC      VARCHAR2(20 BYTE),
  CD_UNIDADE_EMP         VARCHAR2(20 BYTE),
  CD_UNIDADE_ADM         VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATECACA_CONVENI_FK_I ON TASY.ATEND_CATEG_CONV_ADIC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          208K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATECACA_CONVENI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ATECACA_PK ON TASY.ATEND_CATEG_CONV_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ATEND_CATEG_CONV_ADIC_tp  after update ON TASY.ATEND_CATEG_CONV_ADIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'ATEND_CATEG_CONV_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_ADM,1,4000),substr(:new.CD_UNIDADE_ADM,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_ADM',ie_log_w,ds_w,'ATEND_CATEG_CONV_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_EMP,1,4000),substr(:new.CD_UNIDADE_EMP,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_EMP',ie_log_w,ds_w,'ATEND_CATEG_CONV_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_OPERAC,1,4000),substr(:new.CD_UNIDADE_OPERAC,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_OPERAC',ie_log_w,ds_w,'ATEND_CATEG_CONV_ADIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ATEND_CATEG_CONV_ADIC ADD (
  CONSTRAINT ATECACA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_CATEG_CONV_ADIC ADD (
  CONSTRAINT ATECACA_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.ATEND_CATEG_CONV_ADIC TO NIVEL_1;

