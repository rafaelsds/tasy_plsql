ALTER TABLE TASY.PLS_OCORRENCIA_LIM_BENEF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OCORRENCIA_LIM_BENEF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OCORRENCIA_LIM_BENEF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_REGRA             VARCHAR2(255 BYTE)       NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_PRECO             VARCHAR2(2 BYTE),
  QT_LIMITE_EXECUCAO   NUMBER(5),
  IE_UNID_TEMPO        VARCHAR2(1 BYTE),
  QT_TEMPO             NUMBER(5),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOCLB_ESTABEL_FK_I ON TASY.PLS_OCORRENCIA_LIM_BENEF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSOCLB_PK ON TASY.PLS_OCORRENCIA_LIM_BENEF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCLB_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_OCORRENCIA_LIM_BENEF_tp  after update ON TASY.PLS_OCORRENCIA_LIM_BENEF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.DS_REGRA,1,500);gravar_log_alteracao(substr(:old.DS_REGRA,1,4000),substr(:new.DS_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REGRA',ie_log_w,ds_w,'PLS_OCORRENCIA_LIM_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO,1,4000),substr(:new.IE_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO',ie_log_w,ds_w,'PLS_OCORRENCIA_LIM_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OCORRENCIA_LIM_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LIMITE_EXECUCAO,1,4000),substr(:new.QT_LIMITE_EXECUCAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_LIMITE_EXECUCAO',ie_log_w,ds_w,'PLS_OCORRENCIA_LIM_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_TEMPO,1,4000),substr(:new.QT_TEMPO,1,4000),:new.nm_usuario,nr_seq_w,'QT_TEMPO',ie_log_w,ds_w,'PLS_OCORRENCIA_LIM_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_OCORRENCIA_LIM_BENEF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_UNID_TEMPO,1,4000),substr(:new.IE_UNID_TEMPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_UNID_TEMPO',ie_log_w,ds_w,'PLS_OCORRENCIA_LIM_BENEF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OCORRENCIA_LIM_BENEF ADD (
  CONSTRAINT PLSOCLB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OCORRENCIA_LIM_BENEF ADD (
  CONSTRAINT PLSOCLB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_OCORRENCIA_LIM_BENEF TO NIVEL_1;

