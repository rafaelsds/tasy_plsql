DROP TABLE TASY.W_MAPA_TRABALHO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_MAPA_TRABALHO
(
  NR_LINHA        NUMBER(10),
  NR_SEQ_EXAME    NUMBER(10),
  NR_SEQ_ANALITO  NUMBER(10),
  NM_USUARIO      VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_MAPA_TRABALHO TO NIVEL_1;

