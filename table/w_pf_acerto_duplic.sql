DROP TABLE TASY.W_PF_ACERTO_DUPLIC CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_PF_ACERTO_DUPLIC
(
  NM_TABELA    VARCHAR2(50 BYTE),
  NM_ATRIBUTO  VARCHAR2(50 BYTE),
  QT_REGISTRO  NUMBER(15,4),
  NM_USUARIO   VARCHAR2(15 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE INDEX TASY.WPFACDU_ ON TASY.W_PF_ACERTO_DUPLIC
(NM_USUARIO);


CREATE INDEX TASY.WPFACDU_I ON TASY.W_PF_ACERTO_DUPLIC
(NM_TABELA, NM_ATRIBUTO);


GRANT SELECT ON TASY.W_PF_ACERTO_DUPLIC TO NIVEL_1;

