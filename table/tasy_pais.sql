ALTER TABLE TASY.TASY_PAIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_PAIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_PAIS
(
  CD_PAIS            NUMBER(5)                  NOT NULL,
  NM_PAIS            VARCHAR2(30 BYTE)          NOT NULL,
  SG_PAIS            VARCHAR2(3 BYTE)           NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  CD_EXP_INFORMACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TASYPAI_DICEXPR_FK3_I ON TASY.TASY_PAIS
(CD_EXP_INFORMACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TASYPAI_PK ON TASY.TASY_PAIS
(CD_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TASY_PAIS_tp  after update ON TASY.TASY_PAIS FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_PAIS);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EXP_INFORMACAO,1,4000),substr(:new.CD_EXP_INFORMACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXP_INFORMACAO',ie_log_w,ds_w,'TASY_PAIS',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TASY_PAIS ADD (
  CONSTRAINT TASYPAI_PK
 PRIMARY KEY
 (CD_PAIS)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_PAIS ADD (
  CONSTRAINT TASYPAI_DICEXPR_FK3 
 FOREIGN KEY (CD_EXP_INFORMACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.TASY_PAIS TO NIVEL_1;

