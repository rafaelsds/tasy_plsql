ALTER TABLE TASY.PRECO_PJ_TRIBUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRECO_PJ_TRIBUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_PJ_TRIBUTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  CD_TRIBUTO           NUMBER(3)                NOT NULL,
  PR_TRIBUTO           NUMBER(7,4),
  VL_TRIBUTO           NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRPJTRI_PK ON TASY.PRECO_PJ_TRIBUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRPJTRI_PREPJ_FK_I ON TASY.PRECO_PJ_TRIBUTO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRPJTRI_PREPJ_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PRECO_PJ_TRIBUTO ADD (
  CONSTRAINT PRPJTRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRECO_PJ_TRIBUTO ADD (
  CONSTRAINT PRPJTRI_PREPJ_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PRECO_PJ (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRECO_PJ_TRIBUTO TO NIVEL_1;

