ALTER TABLE TASY.PLS_PACOTE_AGRUP_FAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PACOTE_AGRUP_FAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PACOTE_AGRUP_FAT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DS_REGRA               VARCHAR2(255 BYTE)     NOT NULL,
  CD_PROCEDIMENTO        NUMBER(15),
  IE_ORIGEM_PROCED       NUMBER(10),
  DT_INICIO_VIGENCIA     DATE                   NOT NULL,
  DT_FIM_VIGENCIA        DATE,
  IE_TIPO_INTERCAMBIO    VARCHAR2(10 BYTE),
  IE_EXCLUI_ITEM_PACOTE  VARCHAR2(1 BYTE),
  IE_DESC_PAC_ORIGEM     VARCHAR2(2 BYTE),
  IE_AGRUP_PACOTE        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSPAAF_ESTABEL_FK_I ON TASY.PLS_PACOTE_AGRUP_FAT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPAAF_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSPAAF_PK ON TASY.PLS_PACOTE_AGRUP_FAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPAAF_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSPAAF_PROCEDI_FK_I ON TASY.PLS_PACOTE_AGRUP_FAT
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPAAF_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_PACOTE_AGRUP_FAT_tp  after update ON TASY.PLS_PACOTE_AGRUP_FAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_AGRUP_PACOTE,1,4000),substr(:new.IE_AGRUP_PACOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGRUP_PACOTE',ie_log_w,ds_w,'PLS_PACOTE_AGRUP_FAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_PACOTE_AGRUP_FAT ADD (
  CONSTRAINT PLSPAAF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PACOTE_AGRUP_FAT ADD (
  CONSTRAINT PLSPAAF_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSPAAF_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_PACOTE_AGRUP_FAT TO NIVEL_1;

