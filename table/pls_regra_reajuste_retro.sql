ALTER TABLE TASY.PLS_REGRA_REAJUSTE_RETRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_REAJUSTE_RETRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_REAJUSTE_RETRO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_REGRA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  MES_ANIVERSARIO_CONTRATO  VARCHAR2(7 BYTE),
  DT_ANIVERSARIO_CONTRATO   DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRGRR_PK ON TASY.PLS_REGRA_REAJUSTE_RETRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGRR_PLSRERE_FK_I ON TASY.PLS_REGRA_REAJUSTE_RETRO
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRGRR_UK ON TASY.PLS_REGRA_REAJUSTE_RETRO
(NR_SEQ_REGRA, MES_ANIVERSARIO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_reajuste_retro_utp
before insert or update ON TASY.PLS_REGRA_REAJUSTE_RETRO for each row
declare

begin

if	(:new.dt_aniversario_contrato is not null) then --Se estiver utilizando a nova fun��o de reajuste
	:new.mes_aniversario_contrato	:= to_char(:new.dt_aniversario_contrato,'mm/yyyy');
	:new.dt_aniversario_contrato	:= trunc(:new.dt_aniversario_contrato,'month');
elsif	(:new.mes_aniversario_contrato is not null) then --Se estiver utilizando a antiga fun��o de reajuste
	:new.dt_aniversario_contrato	:= to_date(:new.mes_aniversario_contrato,'mm/yyyy');
end if;

end;
/


ALTER TABLE TASY.PLS_REGRA_REAJUSTE_RETRO ADD (
  CONSTRAINT PLSRGRR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSRGRR_UK
 UNIQUE (NR_SEQ_REGRA, MES_ANIVERSARIO_CONTRATO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_REAJUSTE_RETRO ADD (
  CONSTRAINT PLSRGRR_PLSRERE_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_REAJUSTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_REAJUSTE_RETRO TO NIVEL_1;

