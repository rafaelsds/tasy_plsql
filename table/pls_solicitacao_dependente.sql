ALTER TABLE TASY.PLS_SOLICITACAO_DEPENDENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SOLICITACAO_DEPENDENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SOLICITACAO_DEPENDENTE
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NM_PESSOA                VARCHAR2(255 BYTE)   NOT NULL,
  QT_IDADE                 NUMBER(3),
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_SOLICITACAO_LEAD  NUMBER(10),
  NR_SEQ_PROSPECT          NUMBER(10),
  DT_NASCIMENTO            DATE,
  NR_SEQ_PARENTESCO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSLDP_GRAUPA_FK_I ON TASY.PLS_SOLICITACAO_DEPENDENTE
(NR_SEQ_PARENTESCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSSLDP_PK ON TASY.PLS_SOLICITACAO_DEPENDENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSLDP_PLSCOCL_FK_I ON TASY.PLS_SOLICITACAO_DEPENDENTE
(NR_SEQ_PROSPECT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSLDP_PLSSCOM_FK_I ON TASY.PLS_SOLICITACAO_DEPENDENTE
(NR_SEQ_SOLICITACAO_LEAD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_SOLICITACAO_DEPENDENTE ADD (
  CONSTRAINT PLSSLDP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SOLICITACAO_DEPENDENTE ADD (
  CONSTRAINT PLSSLDP_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT PLSSLDP_PLSCOCL_FK 
 FOREIGN KEY (NR_SEQ_PROSPECT) 
 REFERENCES TASY.PLS_COMERCIAL_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT PLSSLDP_PLSSCOM_FK 
 FOREIGN KEY (NR_SEQ_SOLICITACAO_LEAD) 
 REFERENCES TASY.PLS_SOLICITACAO_COMERCIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SOLICITACAO_DEPENDENTE TO NIVEL_1;

