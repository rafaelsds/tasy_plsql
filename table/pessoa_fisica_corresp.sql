ALTER TABLE TASY.PESSOA_FISICA_CORRESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_CORRESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_CORRESP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  IE_TIPO_CORRESP      VARCHAR2(15 BYTE)        NOT NULL,
  DT_REGISTRO          DATE                     NOT NULL,
  IE_TIPO_DOC          VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_DOC      NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESFICR_PESFISI_FK_I ON TASY.PESSOA_FISICA_CORRESP
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFICR_PK ON TASY.PESSOA_FISICA_CORRESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESFICR_TIPDOCO_FK_I ON TASY.PESSOA_FISICA_CORRESP
(NR_SEQ_TIPO_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFICR_TIPDOCO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PESSOA_FISICA_CORRESP ADD (
  CONSTRAINT PESFICR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA_CORRESP ADD (
  CONSTRAINT PESFICR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PESFICR_TIPDOCO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DOC) 
 REFERENCES TASY.TIPO_DOCUMENTO_CORRESP (NR_SEQUENCIA));

GRANT SELECT ON TASY.PESSOA_FISICA_CORRESP TO NIVEL_1;

