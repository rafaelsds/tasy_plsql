ALTER TABLE TASY.HISTORICO_SAUDE_INTERNACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HISTORICO_SAUDE_INTERNACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HISTORICO_SAUDE_INTERNACAO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_DOENCA                  VARCHAR2(10 BYTE),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  QT_PERIODO                 NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  IE_PERIODO                 VARCHAR2(15 BYTE),
  IE_ALERTA                  VARCHAR2(1 BYTE),
  IE_INTENSIDADE             VARCHAR2(1 BYTE),
  NR_SEQ_NIVEL_SEG           NUMBER(10),
  DT_REGISTRO                DATE               NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DT_INTERNACAO              DATE,
  IE_NEGA_INTERNACAO         VARCHAR2(1 BYTE)   NOT NULL,
  DS_INSTITUICAO             VARCHAR2(255 BYTE),
  DT_ALTA                    DATE,
  QT_IDADE                   NUMBER(3),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  DS_INTERCORRENCIA          VARCHAR2(255 BYTE),
  DS_MOTIVO_INTERNACAO       VARCHAR2(255 BYTE),
  NM_USUARIO_REVISAO         VARCHAR2(15 BYTE),
  DT_REVISAO                 DATE,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HISAINT_ATCONSPEPA_FK_I ON TASY.HISTORICO_SAUDE_INTERNACAO
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISAINT_CIDDOEN_FK_I ON TASY.HISTORICO_SAUDE_INTERNACAO
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAINT_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISAINT_EHRREEL_FK_I ON TASY.HISTORICO_SAUDE_INTERNACAO
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAINT_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISAINT_NISEALE_FK_I ON TASY.HISTORICO_SAUDE_INTERNACAO
(NR_SEQ_NIVEL_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAINT_NISEALE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISAINT_PESFISI_FK_I ON TASY.HISTORICO_SAUDE_INTERNACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HISAINT_PESFISI_FK2_I ON TASY.HISTORICO_SAUDE_INTERNACAO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HISAINT_PK ON TASY.HISTORICO_SAUDE_INTERNACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAINT_PK
  MONITORING USAGE;


CREATE INDEX TASY.HISAINT_TASASDI_FK_I ON TASY.HISTORICO_SAUDE_INTERNACAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAINT_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HISAINT_TASASDI_FK2_I ON TASY.HISTORICO_SAUDE_INTERNACAO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HISAINT_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.HISTORICO_SAUDE_INTERNACAO_tp  after update ON TASY.HISTORICO_SAUDE_INTERNACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_DOENCA,1,4000),substr(:new.CD_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOENCA',ie_log_w,ds_w,'HISTORICO_SAUDE_INTERNACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.HIST_SAUDE_INTER_ATUAL
before insert or update ON TASY.HISTORICO_SAUDE_INTERNACAO for each row
declare


begin
if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.historico_saude_int_pend_atual 
after insert or update ON TASY.HISTORICO_SAUDE_INTERNACAO 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSI'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSI'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, :new.cd_pessoa_fisica, null, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


ALTER TABLE TASY.HISTORICO_SAUDE_INTERNACAO ADD (
  CONSTRAINT HISAINT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HISTORICO_SAUDE_INTERNACAO ADD (
  CONSTRAINT HISAINT_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HISAINT_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT HISAINT_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT HISAINT_NISEALE_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_SEG) 
 REFERENCES TASY.NIVEL_SEGURANCA_ALERTA (NR_SEQUENCIA),
  CONSTRAINT HISAINT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT HISAINT_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT HISAINT_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT HISAINT_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.HISTORICO_SAUDE_INTERNACAO TO NIVEL_1;

