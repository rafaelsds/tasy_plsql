ALTER TABLE TASY.TITULO_RECEBER_LIQ_DESC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_RECEBER_LIQ_DESC CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_RECEBER_LIQ_DESC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_TITULO            NUMBER(10),
  NR_SEQ_LIQ           NUMBER(10),
  NR_BORDERO           NUMBER(10),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE),
  CD_CGC               VARCHAR2(14 BYTE),
  NR_SEQ_MOTIVO_DESC   NUMBER(10),
  CD_CENTRO_CUSTO      NUMBER(8)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIRECDE_BOTIRE_FK_I ON TASY.TITULO_RECEBER_LIQ_DESC
(NR_BORDERO, NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRECDE_CENCUST_FK_I ON TASY.TITULO_RECEBER_LIQ_DESC
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECDE_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRECDE_MOTDESC_FK_I ON TASY.TITULO_RECEBER_LIQ_DESC
(NR_SEQ_MOTIVO_DESC)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECDE_MOTDESC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TIRECDE_PESFISI_FK_I ON TASY.TITULO_RECEBER_LIQ_DESC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRECDE_PESJURI_FK_I ON TASY.TITULO_RECEBER_LIQ_DESC
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TIRECDE_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.TIRECDE_PK ON TASY.TITULO_RECEBER_LIQ_DESC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIRECDE_TIRELIQ_FK_I ON TASY.TITULO_RECEBER_LIQ_DESC
(NR_TITULO, NR_SEQ_LIQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.titulo_receber_liq_desc_insert
BEFORE INSERT OR UPDATE ON TASY.TITULO_RECEBER_LIQ_DESC FOR EACH ROW
DECLARE
qt_reg_w	number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	((:new.cd_cgc is null) and (:new.cd_pessoa_fisica is null))
	or
	((:new.cd_cgc is not null) and (:new.cd_pessoa_fisica is not null)) then
	Wheb_mensagem_pck.exibir_mensagem_abort(278442);
end if;
<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.titulo_receber_liq_desc_after
after insert or update ON TASY.TITULO_RECEBER_LIQ_DESC for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_reg_w				number(10);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:new.nr_seq_liq is not null) and (:new.nr_titulo is not null) then

	/*Esse select E para tentar evitar a duplicidade. Pois ao ao atualizar algo no titulo, pode chamar outra proc que atualiza classificacao ou imposto, que tb dispara a trigger das tabelas com esse insert*/
	select	count(*)
	into	qt_reg_w
	from	intpd_fila_transmissao
	where  	nr_seq_documento 		= to_char(:new.nr_titulo)
	and		nr_seq_item_documento	= to_char(:new.nr_seq_liq)
	and     to_char(dt_atualizacao, 'dd/mm/yyyy hh24:mi:ss') = to_char(sysdate,'dd/mm/yyyy hh24:mi:ss');

	if (qt_reg_w = 0) then
		reg_integracao_p.nr_seq_item_documento_p	:=	:new.nr_seq_liq;
		gerar_int_padrao.gravar_integracao('27', :new.nr_titulo, :new.nm_usuario, reg_integracao_p);
	end if;

end if;

end if;

end;
/


ALTER TABLE TASY.TITULO_RECEBER_LIQ_DESC ADD (
  CONSTRAINT TIRECDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_RECEBER_LIQ_DESC ADD (
  CONSTRAINT TIRECDE_MOTDESC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DESC) 
 REFERENCES TASY.MOTIVO_DESCONTO (NR_SEQUENCIA),
  CONSTRAINT TIRECDE_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TIRECDE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TIRECDE_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TIRECDE_BOTIRE_FK 
 FOREIGN KEY (NR_BORDERO, NR_TITULO) 
 REFERENCES TASY.BORDERO_TIT_REC (NR_BORDERO,NR_TITULO)
    ON DELETE CASCADE,
  CONSTRAINT TIRECDE_TIRELIQ_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_LIQ) 
 REFERENCES TASY.TITULO_RECEBER_LIQ (NR_TITULO,NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TITULO_RECEBER_LIQ_DESC TO NIVEL_1;

