ALTER TABLE TASY.BSC_REGRA_CTB_ESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BSC_REGRA_CTB_ESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.BSC_REGRA_CTB_ESP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA_CALC    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ORIGEM_CTB        VARCHAR2(15 BYTE),
  DS_ORIGEM            VARCHAR2(2000 BYTE),
  DS_TITULO            VARCHAR2(80 BYTE)        NOT NULL,
  QT_MES               NUMBER(3),
  NR_SEQ_MES_REF       NUMBER(10),
  IE_ESTAB_INF         VARCHAR2(1 BYTE),
  CD_EXP_TITULO        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BSCRECE_BSCRECI_FK_I ON TASY.BSC_REGRA_CTB_ESP
(NR_SEQ_REGRA_CALC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BSCRECE_BSCRECI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BSCRECE_CTBMERE_FK_I ON TASY.BSC_REGRA_CTB_ESP
(NR_SEQ_MES_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.BSCRECE_CTBMERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.BSCRECE_DICEXPR_FK_I ON TASY.BSC_REGRA_CTB_ESP
(CD_EXP_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BSCRECE_PK ON TASY.BSC_REGRA_CTB_ESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.BSC_REGRA_CTB_ESP ADD (
  CONSTRAINT BSCRECE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BSC_REGRA_CTB_ESP ADD (
  CONSTRAINT BSCRECE_BSCRECI_FK 
 FOREIGN KEY (NR_SEQ_REGRA_CALC) 
 REFERENCES TASY.BSC_REGRA_CALC_IND (NR_SEQUENCIA),
  CONSTRAINT BSCRECE_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT BSCRECE_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_TITULO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.BSC_REGRA_CTB_ESP TO NIVEL_1;

