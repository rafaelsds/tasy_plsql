ALTER TABLE TASY.USUARIO_CONTATO_END
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USUARIO_CONTATO_END CASCADE CONSTRAINTS;

CREATE TABLE TASY.USUARIO_CONTATO_END
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CONTATO       NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  IE_TIPO_COMPLEMENTO  NUMBER(1),
  DS_ENDERECO          VARCHAR2(40 BYTE),
  NR_ENDERECO          NUMBER(5),
  CD_CEP               VARCHAR2(15 BYTE),
  DS_BAIRRO            VARCHAR2(40 BYTE),
  DS_MUNICIPIO         VARCHAR2(40 BYTE),
  SG_ESTADO            VARCHAR2(15 BYTE),
  NR_TELEFONE          VARCHAR2(15 BYTE),
  NR_RAMAL             NUMBER(5),
  DS_COMPLEMENTO       VARCHAR2(40 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  NM_EMPRESA           VARCHAR2(50 BYTE),
  NR_DDI_TELEFONE      VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.USUCOEN_PK ON TASY.USUARIO_CONTATO_END
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUCOEN_USUCONT_FK_I ON TASY.USUARIO_CONTATO_END
(NR_SEQ_CONTATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.USUARIO_CONTATO_END ADD (
  CONSTRAINT USUCOEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.USUARIO_CONTATO_END ADD (
  CONSTRAINT USUCOEN_USUCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTATO) 
 REFERENCES TASY.USUARIO_CONTATO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.USUARIO_CONTATO_END TO NIVEL_1;

