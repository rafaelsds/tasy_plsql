ALTER TABLE TASY.ESCALA_POSSUM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_POSSUM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_POSSUM
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_APAE                NUMBER(10),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  QT_IDADE                   NUMBER(3),
  QT_GLASGOW                 NUMBER(2),
  IE_RESPIRATORIO            NUMBER(2),
  QT_UREIA                   NUMBER(17,4),
  QT_FC                      NUMBER(15,4),
  IE_CARDIOVASCULAR          NUMBER(2),
  QT_HEMOGLOBINA             NUMBER(15,2),
  QT_GLOB_BRANCOS            NUMBER(15,3),
  IE_ECG                     NUMBER(2),
  QT_POTASSIO                NUMBER(15,2),
  QT_SODIO                   NUMBER(5),
  QT_PA_SIST                 NUMBER(15,4),
  IE_PORTE_CIRURGIA          NUMBER(3),
  QT_PROCEDIMENTO            NUMBER(10),
  QT_PERDA_SANGUE            NUMBER(10),
  IE_SECRECAO_PERITONIAL     NUMBER(2),
  IE_CANCER                  NUMBER(2),
  IE_TIPO_CIRURGIA           NUMBER(2),
  QT_PONTOS_FISIO            NUMBER(3),
  QT_PONTOS_OPERATIVOS       NUMBER(3),
  PR_MORTALIDADE             NUMBER(5,2),
  NR_SEQ_PEPO                NUMBER(10),
  NR_HORA                    NUMBER(2),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPOSS_ATCONSPEPA_FK_I ON TASY.ESCALA_POSSUM
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPOSS_ATEPACI_FK_I ON TASY.ESCALA_POSSUM
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPOSS_AVAPRAN_FK_I ON TASY.ESCALA_POSSUM
(NR_SEQ_APAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPOSS_PEPOCIR_FK_I ON TASY.ESCALA_POSSUM
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCPOSS_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCPOSS_PESFISI_FK_I ON TASY.ESCALA_POSSUM
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPOSS_PK ON TASY.ESCALA_POSSUM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCPOSS_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCPOSS_TASASDI_FK_I ON TASY.ESCALA_POSSUM
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPOSS_TASASDI_FK2_I ON TASY.ESCALA_POSSUM
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Escala_possum_atual
before insert or update ON TASY.ESCALA_POSSUM for each row
declare

qt_pt_idade_w				number(2,0) := 0;
qt_pt_glasgow_w				number(2,0) := 0;
qt_pt_ureia_w				number(2,0) := 0;
qt_ureia_mmol_w				number(17,4) := 0;
qt_pt_fc_w					number(2,0) := 0;
qt_pt_hemoglobina_w			number(2,0) := 0;
qt_pt_glob_brancos_w		number(2,0) := 0;
qt_pt_potassio_w			number(2,0) := 0;
qt_pt_sodio_w				number(2,0) := 0;
qt_pt_pa_sist_w				number(2,0) := 0;
qt_pt_procedimento_w		number(2,0) := 0;
qt_pt_perda_sangue_w		number(2,0) := 0;
qt_pt_respiratorio_w		number(2,0)	:= 0;
qt_pt_cardiovascular_w		number(2,0)	:= 0;
qt_pt_ecg_w					number(2,0)	:= 0;
qt_pt_tipo_cirurgia_w		number(2,0)	:= 0;
qt_reg_w	number(1);
nr_atendimento_w   number(10);
cd_pessoa_fisica_w varchar2(10);
ie_tipo_w		varchar2(10);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
-- idade
if	(:new.qt_idade is not null) then
	if		(:new.qt_idade	<=	60) then
		qt_pt_idade_w	:=	1;
	elsif	(:new.qt_idade	>=	71) then
		qt_pt_idade_w	:=	4;
	else
		qt_pt_idade_w	:=	2;
	end if;
end if;

-- glasgow
if	(:new.qt_glasgow is not null) then
	if		(:new.qt_glasgow	<=	8) then
		qt_pt_glasgow_w	:=	8;
	elsif	(:new.qt_glasgow	<=	11) then
		qt_pt_glasgow_w	:=	4;
	elsif	(:new.qt_glasgow	<=	14) then
		qt_pt_glasgow_w	:=	2;
	else
		qt_pt_glasgow_w	:=	1;
	end if;
end if;

-- ureia
if	(:new.qt_ureia is not null) then
	qt_ureia_mmol_w	:=	(:new.qt_ureia * 0.357); -- converte de mg para mmol
	if		(qt_ureia_mmol_w	<=	7.5) then
		qt_pt_ureia_w	:=	1;
	elsif	(qt_ureia_mmol_w	<=	10) then
		qt_pt_ureia_w	:=	2;
	elsif	(qt_ureia_mmol_w	<=	15) then
		qt_pt_ureia_w	:=	4;
	else
		qt_pt_ureia_w	:=	8;
	end if;
end if;

-- frequencia cardiaca
if	(:new.qt_fc is not null) then
	if		(:new.qt_fc	<=	39) then
		qt_pt_fc_w	:=	8;
	elsif	(:new.qt_fc	<=	49) then
		qt_pt_fc_w	:=	2;
	elsif	(:new.qt_fc	<=	80) then
		qt_pt_fc_w	:=	1;
	elsif	(:new.qt_fc	<=	100) then
		qt_pt_fc_w	:=	2;
	elsif	(:new.qt_fc	<=	120) then
		qt_pt_fc_w	:=	4;
	else
		qt_pt_fc_w	:=	8;
	end if;
end if;

-- hemoglobina
if	(:new.qt_hemoglobina is not null) then
	if		(:new.qt_hemoglobina	<=	9.9) then
		qt_pt_hemoglobina_w	:=	8;
	elsif	(:new.qt_hemoglobina	<=	11.4) then
		qt_pt_hemoglobina_w	:=	4;
	elsif	(:new.qt_hemoglobina	<=	12.9) then
		qt_pt_hemoglobina_w	:=	2;
	elsif	(:new.qt_hemoglobina	<=	16) then
		qt_pt_hemoglobina_w	:=	1;
	elsif	(:new.qt_hemoglobina	<=	17) then
		qt_pt_hemoglobina_w	:=	2;
	elsif	(:new.qt_hemoglobina	<=	18) then
		qt_pt_hemoglobina_w	:=	4;
	else
		qt_pt_hemoglobina_w	:=	8;
	end if;
end if;

-- globulos brancos
if	(:new.qt_glob_brancos is not null) then
	if		(:new.qt_glob_brancos	<=	3000) then
		qt_pt_glob_brancos_w	:=	4;
	elsif	(:new.qt_glob_brancos	<=	3999) then
		qt_pt_glob_brancos_w	:=	2;
	elsif	(:new.qt_glob_brancos	<=	10000) then
		qt_pt_glob_brancos_w	:=	1;
	elsif	(:new.qt_glob_brancos	<=	20000) then
		qt_pt_glob_brancos_w	:=	2;
	else
		qt_pt_glob_brancos_w	:=	4;
	end if;
end if;

-- potassio
if	(:new.qt_potassio is not null) then
	if		(:new.qt_potassio	<=	2.8) then
		qt_pt_potassio_w	:=	8;
	elsif	(:new.qt_potassio	<=	3.1) then
		qt_pt_potassio_w	:=	4;
	elsif	(:new.qt_potassio	<=	3.4) then
		qt_pt_potassio_w	:=	2;
	elsif	(:new.qt_potassio	<=	5.0) then
		qt_pt_potassio_w	:=	1;
	elsif	(:new.qt_potassio	<=	5.3) then
		qt_pt_potassio_w	:=	2;
	elsif	(:new.qt_potassio	<=	5.9) then
		qt_pt_potassio_w	:=	4;
	else
		qt_pt_potassio_w	:=	8;
	end if;
end if;

-- sodio
if	(:new.qt_sodio is not null) then
	if		(:new.qt_sodio	<=	125) then
		qt_pt_sodio_w	:=	8;
	elsif	(:new.qt_sodio	<=	130) then
		qt_pt_sodio_w	:=	4;
	elsif	(:new.qt_sodio	<=	135) then
		qt_pt_sodio_w	:=	2;
	else
		qt_pt_sodio_w	:=	1;
	end if;
end if;

-- pa sistolica
if	(:new.qt_pa_sist is not null) then
	if		(:new.qt_pa_sist	<=	89) then
		qt_pt_pa_sist_w	:=	8;
	elsif	(:new.qt_pa_sist	<=	99) then
		qt_pt_pa_sist_w	:=	4;
	elsif	(:new.qt_pa_sist	<=	109) then
		qt_pt_pa_sist_w	:=	2;
	elsif	(:new.qt_pa_sist	<=	130) then
		qt_pt_pa_sist_w	:=	1;
	elsif	(:new.qt_pa_sist	<=	170) then
		qt_pt_pa_sist_w	:=	2;
	else
		qt_pt_pa_sist_w	:=	4;
	end if;
end if;

-- procedimentos
if	(:new.qt_procedimento is not null) then
	if		(:new.qt_procedimento	=	1) then
		qt_pt_procedimento_w	:=	1;
	elsif	(:new.qt_procedimento	=	2) then
		qt_pt_procedimento_w	:=	4;
	else
		qt_pt_procedimento_w	:=	8;
	end if;
end if;

-- perda sangue
if	(:new.qt_perda_sangue is not null) then
	if		(:new.qt_perda_sangue	<=	100) then
		qt_pt_perda_sangue_w	:=	1;
	elsif	(:new.qt_perda_sangue	<=	500) then
		qt_pt_perda_sangue_w	:=	2;
	elsif	(:new.qt_perda_sangue	<=	999) then
		qt_pt_perda_sangue_w	:=	4;
	else
		qt_pt_perda_sangue_w	:=	8;
	end if;
end if;

-- respiratório
if	(:new.ie_respiratorio is not null) then
	if		(:new.ie_respiratorio	=	1) then
		qt_pt_respiratorio_w	:=	1;
	elsif	(:new.ie_respiratorio	=	2) then
		qt_pt_respiratorio_w	:=	2;
	elsif	(:new.ie_respiratorio	=	3) then
		qt_pt_respiratorio_w	:=	3;
	elsif	(:new.ie_respiratorio	in	(4,5)) then
		qt_pt_respiratorio_w	:=	4;
	elsif	(:new.ie_respiratorio	in	(6,8)) then
		qt_pt_respiratorio_w	:=	8;
	end if;
end if;

-- cardiovascular
if	(:new.ie_cardiovascular is not null) then
	if		(:new.ie_cardiovascular	=	1) then
		qt_pt_cardiovascular_w	:=	1;
	elsif	(:new.ie_cardiovascular	in	(2,3)) then
		qt_pt_cardiovascular_w	:=	2;
	elsif	(:new.ie_cardiovascular	in	(4,5)) then
		qt_pt_cardiovascular_w	:=	4;
	elsif	(:new.ie_cardiovascular	in	(6,8)) then
		qt_pt_cardiovascular_w	:=	8;
	end if;
end if;

-- eletrocardiograma
if	(:new.ie_ecg is not null) then
	if		(:new.ie_ecg	=	1) then
		qt_pt_ecg_w	:=	1;
	elsif	(:new.ie_ecg	=	4) then
		qt_pt_ecg_w	:=	4;
	elsif	(:new.ie_ecg	in	(2,3,8)) then
		qt_pt_ecg_w	:=	8;
	end if;
end if;

-- tipo cirurgia
if	(:new.ie_tipo_cirurgia is not null) then
	if		(:new.ie_tipo_cirurgia	=	1) then
		qt_pt_tipo_cirurgia_w	:=	1;
	elsif	(:new.ie_tipo_cirurgia	in	(2,4)) then
		qt_pt_tipo_cirurgia_w	:=	4;
	elsif	(:new.ie_tipo_cirurgia	=	8) then
		qt_pt_tipo_cirurgia_w	:=	8;
	end if;
end if;

:new.qt_pontos_fisio	:=	qt_pt_idade_w +
							qt_pt_cardiovascular_w +
							qt_pt_respiratorio_w +
							qt_pt_pa_sist_w +
							qt_pt_fc_w +
							qt_pt_glasgow_w	+
							qt_pt_hemoglobina_w +
							qt_pt_glob_brancos_w +
							qt_pt_ureia_w +
							qt_pt_sodio_w +
							qt_pt_potassio_w +
							qt_pt_ecg_w;

:new.qt_pontos_operativos	:=	nvl(:new.ie_porte_cirurgia,0) +
								qt_pt_procedimento_w +
								qt_pt_perda_sangue_w +
								nvl(:new.ie_secrecao_peritonial,0) +
								nvl(:new.ie_cancer,0) +
								qt_pt_tipo_cirurgia_w;

:new.pr_mortalidade		:=	(100 * Dividir(1,(1 + Exp(-((:new.qt_pontos_operativos*0.155) + ((0.1692*:new.qt_pontos_fisio)-9.065))))));

if (:new.pr_mortalidade >= 0) then
	:new.pr_mortalidade	:=	round(:new.pr_mortalidade + 0.05, 2);
else
	:new.pr_mortalidade	:=	round(:new.pr_mortalidade - 0.05, 2);
end if;

<<Final>>
qt_reg_w	:= 0;


end Escala_possum_atual;
/


CREATE OR REPLACE TRIGGER TASY.Escala_possum_after_insert
after insert or update ON TASY.ESCALA_POSSUM for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_libera_partic_w	varchar2(10);
nr_atendimento_w   number(10);
cd_pessoa_fisica_w varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(c.nr_atendimento),
			max(c.cd_pessoa_fisica)
into		nr_atendimento_w,
			cd_pessoa_fisica_w
from		atendimento_paciente c
where		c.nr_atendimento = :new.nr_atendimento;

if 	(cd_pessoa_fisica_w is null) then
	select	max(c.nr_atendimento),
				max(c.cd_pessoa_fisica)
	into		nr_atendimento_w,
				cd_pessoa_fisica_w
	from		pepo_cirurgia c
	where		c.nr_sequencia = :new.nr_seq_pepo;
end if;

if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESP';
elsif	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESP';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_POSSUM ADD (
  CONSTRAINT ESCPOSS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_POSSUM ADD (
  CONSTRAINT ESCPOSS_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ESCPOSS_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCPOSS_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCPOSS_AVAPRAN_FK 
 FOREIGN KEY (NR_SEQ_APAE) 
 REFERENCES TASY.AVAL_PRE_ANESTESICA (NR_SEQUENCIA),
  CONSTRAINT ESCPOSS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCPOSS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPOSS_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESCALA_POSSUM TO NIVEL_1;

