ALTER TABLE TASY.LAB_EQUIP_MAQUINA_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_EQUIP_MAQUINA_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_EQUIP_MAQUINA_EXAME
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_EXAME         NUMBER(10)               NOT NULL,
  NR_SEQ_MAQUINA       NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LEQMAEX_EXALABO_FK_I ON TASY.LAB_EQUIP_MAQUINA_EXAME
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEQMAEX_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LEQMAEX_LEQMAQ_FK_I ON TASY.LAB_EQUIP_MAQUINA_EXAME
(NR_SEQ_MAQUINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEQMAEX_LEQMAQ_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LEQMAEX_PK ON TASY.LAB_EQUIP_MAQUINA_EXAME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEQMAEX_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.lab_equip_maq_exame_ins_upd
before insert or update ON TASY.LAB_EQUIP_MAQUINA_EXAME for each row
declare

begin

if	(nvl(:old.dt_atualizacao, to_date('01/01/1988')) <> :new.dt_atualizacao) and
	(wheb_usuario_pck.is_evento_ativo(296) = 'S') then
	integrar_tasylab(
				null,
				null,
				296,
				null,
				null,
				null,
				null,
				null,
				null,
				:new.nr_sequencia,
				2, --LAB_EQUIP_MAQUINA_EXAME
				'N');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.LAB_EQUIP_MAQ_EXAME_DELETE
before delete ON TASY.LAB_EQUIP_MAQUINA_EXAME for each row
declare

ds_envio_w	varchar2(2000);

begin

if	(wheb_usuario_pck.is_evento_ativo(308) = 'S') then

	ds_envio_w	:= 	' Exclus�o de registro - LAB - Exame vinculado a m�quina '||chr(10)||chr(13)||
					' Sequ�ncia:'||:old.nr_sequencia||chr(10)||chr(13)||
					' Exame:'||:old.nr_seq_exame||chr(10)||chr(13)||
					' M�quina:'||:old.nr_seq_maquina;

	integrar_tasylab(
				null,
				null,
				308,
				null,
				null,
				null,
				null,
				null,
				null,
				:new.nr_sequencia,
				2, --LAB_EQUIP_MAQUINA_EXAME
				'N',
				ds_envio_w);
end if;

end;
/


ALTER TABLE TASY.LAB_EQUIP_MAQUINA_EXAME ADD (
  CONSTRAINT LEQMAEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAB_EQUIP_MAQUINA_EXAME ADD (
  CONSTRAINT LEQMAEX_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT LEQMAEX_LEQMAQ_FK 
 FOREIGN KEY (NR_SEQ_MAQUINA) 
 REFERENCES TASY.LAB_EQUIP_MAQUINA (NR_SEQUENCIA));

GRANT SELECT ON TASY.LAB_EQUIP_MAQUINA_EXAME TO NIVEL_1;

