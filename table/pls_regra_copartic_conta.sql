ALTER TABLE TASY.PLS_REGRA_COPARTIC_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COPARTIC_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COPARTIC_CONTA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_REGRA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_TIPO_ATENDIMENTO       VARCHAR2(1 BYTE),
  IE_FORMA_COBR_INTERNACAO  VARCHAR2(1 BYTE),
  VL_ITEM_MINIMO            NUMBER(15,2),
  VL_ITEM_MAXIMO            NUMBER(15,2),
  IE_COBRANCA_PREVISTA      VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_CONTA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRGCC_PK ON TASY.PLS_REGRA_COPARTIC_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCC_PLSRGCP_FK_I ON TASY.PLS_REGRA_COPARTIC_CONTA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCC_PLSTPCO_FK_I ON TASY.PLS_REGRA_COPARTIC_CONTA
(NR_SEQ_TIPO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_copart_conta_update
before update ON TASY.PLS_REGRA_COPARTIC_CONTA for each row
declare

begin
if	(:new.vl_item_minimo <> :old.vl_item_minimo) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.vl_item_minimo, :new.vl_item_minimo, 'PLS_REGRA_COPARTIC_CONTA', 'VL_ITEM_MINIMO', :new.nm_usuario);
end if;
if	(:new.vl_item_maximo <> :old.vl_item_maximo) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.vl_item_maximo, :new.vl_item_maximo, 'PLS_REGRA_COPARTIC_CONTA', 'VL_ITEM_MAXIMO', :new.nm_usuario);
end if;
if	(:new.nr_seq_tipo_conta <> :old.nr_seq_tipo_conta) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_tipo_conta, :new.nr_seq_tipo_conta, 'PLS_REGRA_COPARTIC_CONTA', 'NR_SEQ_TIPO_CONTA', :new.nm_usuario);
end if;
if	(:new.ie_cobranca_prevista <> :old.ie_cobranca_prevista) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_cobranca_prevista, :new.ie_cobranca_prevista, 'PLS_REGRA_COPARTIC_CONTA', 'IE_COBRANCA_PREVISTA', :new.nm_usuario);
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_COPARTIC_CONTA ADD (
  CONSTRAINT PLSRGCC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COPARTIC_CONTA ADD (
  CONSTRAINT PLSRGCC_PLSRGCP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_COPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSRGCC_PLSTPCO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CONTA) 
 REFERENCES TASY.PLS_TIPO_CONTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COPARTIC_CONTA TO NIVEL_1;

