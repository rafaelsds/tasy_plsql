ALTER TABLE TASY.PTU_QUEST_SERV_RRS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_QUEST_SERV_RRS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_QUEST_SERV_RRS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_QUEST_RRS      NUMBER(10)              NOT NULL,
  NR_LOTE               NUMBER(8),
  NR_NOTA               VARCHAR2(20 BYTE),
  NR_NOTA_NUMERICO      VARCHAR2(20 BYTE),
  NR_SEQ_A500           NUMBER(10),
  TP_PARTICIP           VARCHAR2(1 BYTE),
  TP_TABELA             NUMBER(1),
  CD_SERVICO            NUMBER(15),
  DT_SERVICO            DATE,
  VL_DIF_VL_INTER       NUMBER(15,2),
  VL_SERV               NUMBER(15,2),
  VL_RECONH_SERV        NUMBER(15,2),
  VL_ACORDO_SERV        NUMBER(15,2),
  TP_ACORDO             VARCHAR2(10 BYTE),
  QT_COBRADA            NUMBER(12,4),
  QT_RECONH             NUMBER(12,4),
  QT_ACORDADA           NUMBER(12,4),
  DT_ACORDO             DATE,
  NM_PROFISSIONAL       VARCHAR2(70 BYTE),
  SG_CONS_PROF          VARCHAR2(12 BYTE),
  NR_CONS_PROF          VARCHAR2(15 BYTE),
  SG_UF_CONS_PROF       VARCHAR2(2 BYTE),
  NR_AUTORIZ            NUMBER(10),
  NR_SEQ_NOTA_SERV_RRS  NUMBER(10),
  NR_SEQ_CONTA_MAT      NUMBER(10),
  NR_SEQ_CONTA_PROC     NUMBER(10),
  IE_TIPO_TABELA_TISS   VARCHAR2(5 BYTE),
  CD_ITEM_UNICO         VARCHAR2(28 BYTE),
  CD_SERVICO_TISS       NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PTUERRS_PK ON TASY.PTU_QUEST_SERV_RRS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUERRS_PLSCOMAT_FK_I ON TASY.PTU_QUEST_SERV_RRS
(NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUERRS_PLSCOPRO_FK_I ON TASY.PTU_QUEST_SERV_RRS
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUERRS_PTUQRRS_FK_I ON TASY.PTU_QUEST_SERV_RRS
(NR_SEQ_QUEST_RRS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUERRS_PTUSRRS_FK_I ON TASY.PTU_QUEST_SERV_RRS
(NR_SEQ_NOTA_SERV_RRS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ptu_quest_serv_rrs_atual
before insert or update ON TASY.PTU_QUEST_SERV_RRS for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

begin
:new.nr_nota_numerico := somente_numero(nvl(:new.nr_nota,:old.nr_nota));

end;
/


ALTER TABLE TASY.PTU_QUEST_SERV_RRS ADD (
  CONSTRAINT PTUERRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_QUEST_SERV_RRS ADD (
  CONSTRAINT PTUERRS_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQUENCIA),
  CONSTRAINT PTUERRS_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA),
  CONSTRAINT PTUERRS_PTUQRRS_FK 
 FOREIGN KEY (NR_SEQ_QUEST_RRS) 
 REFERENCES TASY.PTU_QUESTIONAMENTO_RRS (NR_SEQUENCIA),
  CONSTRAINT PTUERRS_PTUSRRS_FK 
 FOREIGN KEY (NR_SEQ_NOTA_SERV_RRS) 
 REFERENCES TASY.PTU_NOTA_SERVICO_RRS (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_QUEST_SERV_RRS TO NIVEL_1;

