ALTER TABLE TASY.DESENV_EPIC_FUNCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DESENV_EPIC_FUNCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DESENV_EPIC_FUNCAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_FUNCAO            NUMBER(5)                NOT NULL,
  PR_ABRANGENCIA       NUMBER(3)                NOT NULL,
  NR_EPIC              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DESEPFU_DESEPIC_FK_I ON TASY.DESENV_EPIC_FUNCAO
(NR_EPIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DESEPFU_FUNCAO_FK_I ON TASY.DESENV_EPIC_FUNCAO
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DESEPFU_PK ON TASY.DESENV_EPIC_FUNCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DESENV_EPIC_FUNCAO ADD (
  CONSTRAINT DESEPFU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DESENV_EPIC_FUNCAO ADD (
  CONSTRAINT DESEPFU_DESEPIC_FK 
 FOREIGN KEY (NR_EPIC) 
 REFERENCES TASY.DESENV_EPIC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DESEPFU_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DESENV_EPIC_FUNCAO TO NIVEL_1;

