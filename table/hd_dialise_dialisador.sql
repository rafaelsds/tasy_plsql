ALTER TABLE TASY.HD_DIALISE_DIALISADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_DIALISE_DIALISADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_DIALISE_DIALISADOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DIALISADOR    NUMBER(10)               NOT NULL,
  NR_SEQ_MAQUINA       NUMBER(10)               NOT NULL,
  CD_PF_MONTAGEM       VARCHAR2(10 BYTE),
  NR_SEQ_PONTO_ACESSO  NUMBER(10),
  CD_PF_RETIRADA       VARCHAR2(10 BYTE),
  DT_MONTAGEM          DATE,
  DT_RETIRADA          DATE,
  DS_MOTIVO_SUBST      VARCHAR2(255 BYTE),
  NR_SEQ_DIALISE       NUMBER(10),
  IE_TROCA_EMERGENCIA  VARCHAR2(1 BYTE),
  NR_SEQ_UNID_DIALISE  NUMBER(10)               NOT NULL,
  QT_REUSO_ATUAL       NUMBER(2),
  NR_SEQ_OSMOSE        NUMBER(10),
  NR_SEQ_MOTIVO_SUB    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDDIADIA_DARF_FK_I ON TASY.HD_DIALISE_DIALISADOR
(NR_SEQ_DIALISADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIADIA_ESTABEL_FK_I ON TASY.HD_DIALISE_DIALISADOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIADIA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIADIA_HDDIALE_FK_I ON TASY.HD_DIALISE_DIALISADOR
(NR_SEQ_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIADIA_HDMAQDI_FK_I ON TASY.HD_DIALISE_DIALISADOR
(NR_SEQ_MAQUINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIADIA_HDMOTSM_FK_I ON TASY.HD_DIALISE_DIALISADOR
(NR_SEQ_MOTIVO_SUB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIADIA_HDOSMDI_FK_I ON TASY.HD_DIALISE_DIALISADOR
(NR_SEQ_OSMOSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIADIA_HDOSMDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIADIA_HDPONAC_FK_I ON TASY.HD_DIALISE_DIALISADOR
(NR_SEQ_PONTO_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIADIA_HDPONAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIADIA_HDUNIDD_FK_I ON TASY.HD_DIALISE_DIALISADOR
(NR_SEQ_UNID_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIADIA_HDUNIDD_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDDIADIA_PK ON TASY.HD_DIALISE_DIALISADOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hd_dialise_dialisador_insert
before insert ON TASY.HD_DIALISE_DIALISADOR for each row
declare

cd_pessoa_fisica_w 		varchar2(10);
cd_pessoa_fisica_dialise_w 	varchar2(10);

begin

select 	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from 	hd_dializador
where	nr_sequencia	= :new.nr_seq_dialisador;

select	cd_pessoa_fisica
into	cd_pessoa_fisica_dialise_w
from	hd_dialise
where	nr_sequencia = :new.nr_seq_dialise;


if (cd_pessoa_fisica_w <> cd_pessoa_fisica_dialise_w) then

	Wheb_mensagem_pck.exibir_mensagem_abort(264205);
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.hd_dial_dialisador_befup_sign
before update ON TASY.HD_DIALISE_DIALISADOR for each row
declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_dialise_dialisador = :new.nr_sequencia
and 	ie_opcao = 'MD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_montagem 		<> :new.dt_montagem
		or :old.cd_pf_montagem 		<> :new.cd_pf_montagem
		or :old.nr_seq_dialisador 	<> :new.nr_seq_dialisador
		or :old.nr_seq_maquina 		<> :new.nr_seq_maquina
		or :old.nr_seq_dialise 		<> :new.nr_seq_dialise
		or :old.nr_seq_ponto_acesso 	<> :new.nr_seq_ponto_acesso
		or :old.ie_troca_emergencia 	<> :new.ie_troca_emergencia
		or :old.nr_seq_unid_dialise 	<> :new.nr_seq_unid_dialise
		or :old.qt_reuso_atual 		<> :new.qt_reuso_atual
		or :old.nr_seq_osmose 		<> :new.nr_seq_osmose
		or (:old.dt_montagem		is null		and	:new.dt_montagem		is not null)
		or (:old.cd_pf_montagem		is null		and	:new.cd_pf_montagem		is not null)
		or (:old.nr_seq_dialisador	is null		and	:new.nr_seq_dialisador		is not null)
		or (:old.nr_seq_maquina		is null		and	:new.nr_seq_maquina		is not null)
		or (:old.nr_seq_dialise		is null		and	:new.nr_seq_dialise		is not null)
		or (:old.nr_seq_ponto_acesso	is null		and	:new.nr_seq_ponto_acesso	is not null)
		or (:old.ie_troca_emergencia	is null		and	:new.ie_troca_emergencia	is not null)
		or (:old.nr_seq_unid_dialise	is null		and	:new.nr_seq_unid_dialise	is not null)
		or (:old.qt_reuso_atual		is null		and	:new.qt_reuso_atual		is not null)
		or (:old.nr_seq_osmose		is null		and	:new.nr_seq_osmose		is not null)
		or (:old.dt_montagem		is not null	and	:new.dt_montagem		is null)
		or (:old.cd_pf_montagem		is not null	and	:new.cd_pf_montagem		is null)
		or (:old.nr_seq_dialisador	is not null	and	:new.nr_seq_dialisador		is null)
		or (:old.nr_seq_maquina		is not null	and	:new.nr_seq_maquina		is null)
		or (:old.nr_seq_dialise		is not null	and	:new.nr_seq_dialise		is null)
		or (:old.nr_seq_ponto_acesso	is not null	and	:new.nr_seq_ponto_acesso	is null)
		or (:old.ie_troca_emergencia	is not null	and	:new.ie_troca_emergencia	is null)
		or (:old.nr_seq_unid_dialise	is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.qt_reuso_atual		is not null	and	:new.qt_reuso_atual		is null)
		or (:old.nr_seq_osmose		is not null	and	:new.nr_seq_osmose		is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'C'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.ds_motivo_subst		<> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_dialise_dialisador = :new.nr_sequencia
and 	ie_opcao = 'TI'
and	dt_liberacao is not null
and	:old.dt_retirada is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.ds_motivo_subst		<> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'SD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada          <> :new.cd_pf_retirada
		or :old.ds_motivo_subst         <> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_dialise_dialisador = :new.nr_sequencia
and 	ie_opcao = 'SD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_montagem		<> :new.dt_montagem
		or :old.cd_pf_montagem          <> :new.cd_pf_montagem
		or :old.nr_seq_dialisador       <> :new.nr_seq_dialisador
		or :old.nr_seq_maquina		<> :new.nr_seq_maquina
		or :old.nr_seq_dialise          <> :new.nr_seq_dialise
		or :old.nr_seq_ponto_acesso     <> :new.nr_seq_ponto_acesso
		or :old.ie_troca_emergencia     <> :new.ie_troca_emergencia
		or :old.nr_seq_unid_dialise     <> :new.nr_seq_unid_dialise
		or :old.qt_reuso_atual          <> :new.qt_reuso_atual
		or :old.nr_seq_osmose           <> :new.nr_seq_osmose
		or (:old.dt_montagem		is null		and	:new.dt_montagem		is not null)
		or (:old.cd_pf_montagem		is null		and	:new.cd_pf_montagem		is not null)
		or (:old.nr_seq_dialisador	is null		and	:new.nr_seq_dialisador		is not null)
		or (:old.nr_seq_maquina		is null		and	:new.nr_seq_maquina		is not null)
		or (:old.nr_seq_dialise		is null		and	:new.nr_seq_dialise		is not null)
		or (:old.nr_seq_ponto_acesso	is null		and	:new.nr_seq_ponto_acesso	is not null)
		or (:old.ie_troca_emergencia	is null		and	:new.ie_troca_emergencia	is not null)
		or (:old.nr_seq_unid_dialise	is null		and	:new.nr_seq_unid_dialise	is not null)
		or (:old.qt_reuso_atual		is null		and	:new.qt_reuso_atual		is not null)
		or (:old.nr_seq_osmose		is null		and	:new.nr_seq_osmose		is not null)
		or (:old.dt_montagem		is not null	and	:new.dt_montagem		is null)
		or (:old.cd_pf_montagem		is not null	and	:new.cd_pf_montagem		is null)
		or (:old.nr_seq_dialisador	is not null	and	:new.nr_seq_dialisador		is null)
		or (:old.nr_seq_maquina		is not null	and	:new.nr_seq_maquina		is null)
		or (:old.nr_seq_dialise		is not null	and	:new.nr_seq_dialise		is null)
		or (:old.nr_seq_ponto_acesso	is not null	and	:new.nr_seq_ponto_acesso	is null)
		or (:old.ie_troca_emergencia	is not null	and	:new.ie_troca_emergencia	is null)
		or (:old.nr_seq_unid_dialise	is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.qt_reuso_atual		is not null	and	:new.qt_reuso_atual		is null)
		or (:old.nr_seq_osmose		is not null	and	:new.nr_seq_osmose		is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'RD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.ds_motivo_subst		<> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'SM'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.nr_seq_motivo_sub	<> :new.nr_seq_motivo_sub
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.nr_seq_motivo_sub	is null		and	:new.nr_seq_motivo_sub	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.nr_seq_motivo_sub	is not null	and	:new.nr_seq_motivo_sub	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_dialise_dialisador = :new.nr_sequencia
and 	ie_opcao = 'SM'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_montagem		<> :new.dt_montagem
		or :old.cd_pf_montagem          <> :new.cd_pf_montagem
		or :old.nr_seq_dialisador       <> :new.nr_seq_dialisador
		or :old.nr_seq_maquina		<> :new.nr_seq_maquina
		or :old.nr_seq_dialise          <> :new.nr_seq_dialise
		or :old.nr_seq_ponto_acesso     <> :new.nr_seq_ponto_acesso
		or :old.ie_troca_emergencia     <> :new.ie_troca_emergencia
		or :old.nr_seq_unid_dialise     <> :new.nr_seq_unid_dialise
		or :old.qt_reuso_atual          <> :new.qt_reuso_atual
		or :old.nr_seq_osmose           <> :new.nr_seq_osmose
		or (:old.dt_montagem		is null		and	:new.dt_montagem		is not null)
		or (:old.cd_pf_montagem		is null		and	:new.cd_pf_montagem		is not null)
		or (:old.nr_seq_dialisador	is null		and	:new.nr_seq_dialisador		is not null)
		or (:old.nr_seq_maquina		is null		and	:new.nr_seq_maquina		is not null)
		or (:old.nr_seq_dialise		is null		and	:new.nr_seq_dialise		is not null)
		or (:old.nr_seq_ponto_acesso	is null		and	:new.nr_seq_ponto_acesso	is not null)
		or (:old.ie_troca_emergencia	is null		and	:new.ie_troca_emergencia	is not null)
		or (:old.nr_seq_unid_dialise	is null		and	:new.nr_seq_unid_dialise	is not null)
		or (:old.qt_reuso_atual		is null		and	:new.qt_reuso_atual		is not null)
		or (:old.nr_seq_osmose		is null		and	:new.nr_seq_osmose		is not null)
		or (:old.dt_montagem		is not null	and	:new.dt_montagem		is null)
		or (:old.cd_pf_montagem		is not null	and	:new.cd_pf_montagem		is null)
		or (:old.nr_seq_dialisador	is not null	and	:new.nr_seq_dialisador		is null)
		or (:old.nr_seq_maquina		is not null	and	:new.nr_seq_maquina		is null)
		or (:old.nr_seq_dialise		is not null	and	:new.nr_seq_dialise		is null)
		or (:old.nr_seq_ponto_acesso	is not null	and	:new.nr_seq_ponto_acesso	is null)
		or (:old.ie_troca_emergencia	is not null	and	:new.ie_troca_emergencia	is null)
		or (:old.nr_seq_unid_dialise	is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.qt_reuso_atual		is not null	and	:new.qt_reuso_atual		is null)
		or (:old.nr_seq_osmose		is not null	and	:new.nr_seq_osmose		is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

select 	max(nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital
where 	nr_seq_dial_retirada = :new.nr_sequencia
and 	ie_opcao = 'FD'
and	dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.dt_retirada		<> :new.dt_retirada
		or :old.cd_pf_retirada		<> :new.cd_pf_retirada
		or :old.ds_motivo_subst		<> :new.ds_motivo_subst
		or (:old.dt_retirada		is null		and	:new.dt_retirada	is not null)
		or (:old.cd_pf_retirada		is null		and	:new.cd_pf_retirada	is not null)
		or (:old.ds_motivo_subst	is null		and	:new.ds_motivo_subst	is not null)
		or (:old.dt_retirada		is not null	and	:new.dt_retirada	is null)
		or (:old.cd_pf_retirada		is not null	and	:new.cd_pf_retirada	is null)
		or (:old.ds_motivo_subst	is not null	and	:new.ds_motivo_subst	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/


ALTER TABLE TASY.HD_DIALISE_DIALISADOR ADD (
  CONSTRAINT HDDIADIA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_DIALISE_DIALISADOR ADD (
  CONSTRAINT HDDIADIA_HDMOTSM_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUB) 
 REFERENCES TASY.HD_MOTIVO_SUBSTIT_MAQ (NR_SEQUENCIA),
  CONSTRAINT HDDIADIA_DARF_FK 
 FOREIGN KEY (NR_SEQ_DIALISADOR) 
 REFERENCES TASY.HD_DIALIZADOR (NR_SEQUENCIA),
  CONSTRAINT HDDIADIA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT HDDIADIA_HDDIALE_FK 
 FOREIGN KEY (NR_SEQ_DIALISE) 
 REFERENCES TASY.HD_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDDIADIA_HDMAQDI_FK 
 FOREIGN KEY (NR_SEQ_MAQUINA) 
 REFERENCES TASY.HD_MAQUINA_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDDIADIA_HDPONAC_FK 
 FOREIGN KEY (NR_SEQ_PONTO_ACESSO) 
 REFERENCES TASY.HD_PONTO_ACESSO (NR_SEQUENCIA),
  CONSTRAINT HDDIADIA_HDUNIDD_FK 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDDIADIA_HDOSMDI_FK 
 FOREIGN KEY (NR_SEQ_OSMOSE) 
 REFERENCES TASY.HD_OSMOSE_DIALISE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_DIALISE_DIALISADOR TO NIVEL_1;

