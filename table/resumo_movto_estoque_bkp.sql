DROP TABLE TASY.RESUMO_MOVTO_ESTOQUE_BKP CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESUMO_MOVTO_ESTOQUE_BKP
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_MESANO_REFERENCIA  DATE                    NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  CD_LOCAL_ESTOQUE      NUMBER(4)               NOT NULL,
  CD_MATERIAL           NUMBER(6)               NOT NULL,
  CD_OPERACAO_ESTOQUE   NUMBER(3)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_CENTRO_CUSTO       NUMBER(8),
  CD_LOCAL_DESTINO      NUMBER(5),
  QT_ESTOQUE            NUMBER(18,4),
  VL_ESTOQUE            NUMBER(18,2),
  IE_CONSIGNADO         VARCHAR2(1 BYTE)        NOT NULL,
  VL_CONSIGNADO         NUMBER(15,2)            NOT NULL,
  CD_FORNECEDOR         VARCHAR2(14 BYTE),
  CD_CONTA_CONTABIL     VARCHAR2(20 BYTE),
  QT_CONSUMO            NUMBER(22,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.RESUMO_MOVTO_ESTOQUE_BKP TO NIVEL_1;

