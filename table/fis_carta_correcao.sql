ALTER TABLE TASY.FIS_CARTA_CORRECAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_CARTA_CORRECAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_CARTA_CORRECAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_NOTA_FISCAL    NUMBER(10),
  CD_MATERIAL           NUMBER(6),
  IE_TIPO_CORRECAO      NUMBER(6),
  DT_LIBERACAO          DATE,
  VL_ANTERIOR           VARCHAR2(255 BYTE),
  VL_ATUAL              VARCHAR2(255 BYTE),
  NM_USUARIO_LIBERACAO  VARCHAR2(50 BYTE),
  NR_ITEM_NF            NUMBER(6),
  CD_NATUREZA_OPERACAO  NUMBER(4),
  CD_CGC_EMITENTE       VARCHAR2(14 BYTE),
  SG_ESTADO             VARCHAR2(2 BYTE),
  CD_NCM                NUMBER(10),
  CD_MUNICIPIO_IBGE     VARCHAR2(6 BYTE),
  CD_UNIDADE_MEDIDA     VARCHAR2(30 BYTE),
  NR_SEQ_ITEM_LOTE      NUMBER(10),
  CD_TRIBUTO            NUMBER(3),
  IE_TIPO_FRETE         VARCHAR2(1 BYTE),
  IE_TRIBUTACAO_CST     VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  NR_SEQ_CLASSIF_TRIB   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FISCARTA_LISTNCM_FK_I ON TASY.FIS_CARTA_CORRECAO
(CD_NCM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISCARTA_LISTNCM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FISCARTA_MATERIA_FK_I ON TASY.FIS_CARTA_CORRECAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISCARTA_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FISCARTA_NATOPER_FK_I ON TASY.FIS_CARTA_CORRECAO
(CD_NATUREZA_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISCARTA_NATOPER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FISCARTA_NOTITLO_FK_I ON TASY.FIS_CARTA_CORRECAO
(NR_SEQ_ITEM_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISCARTA_NOTITLO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FISCARTA_PESJURI_FK_I ON TASY.FIS_CARTA_CORRECAO
(CD_CGC_EMITENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISCARTA_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.FISCARTA_PK ON TASY.FIS_CARTA_CORRECAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISCARTA_PK
  MONITORING USAGE;


CREATE INDEX TASY.FISCARTA_PROCEDI_FK_I ON TASY.FIS_CARTA_CORRECAO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FISCARTA_SITTRPS_FK_I ON TASY.FIS_CARTA_CORRECAO
(NR_SEQ_CLASSIF_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FISCARTA_SUSMUNI_FK_I ON TASY.FIS_CARTA_CORRECAO
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISCARTA_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.FISCARTA_TRIBUTO_FK_I ON TASY.FIS_CARTA_CORRECAO
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.FISCARTA_TRIBUTO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.fis_carta_correcao_insert
before insert or update ON TASY.FIS_CARTA_CORRECAO for each row
declare
vl_anterior_w		varchar2(255);
vl_atual_w		varchar2(255);
qt_registros_w		number(10);

begin

begin

if	(:new.ie_tipo_correcao = 1) then -- Raz�o Social
	begin
	if	(:new.cd_material is null) then
		select	substr(obter_dados_nota_fiscal(a.nr_sequencia, 3),1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(obter_dados_nota_fiscal(a.nr_sequencia, 3),1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 2) then -- Endere�o
	begin
	if	(:new.cd_material is null) then
		select	substr(obter_dados_pf_pj(null,a.cd_cgc_emitente, 'R'), 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(obter_dados_pf_pj(null,a.cd_cgc_emitente, 'R'), 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 3) then -- Munic�pio
	begin
	if	(:new.cd_material is null) then
		select	substr(obter_dados_pf_pj(null,a.cd_cgc_emitente, 'CI'), 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(obter_dados_pf_pj(null,a.cd_cgc_emitente, 'CI'), 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 4) then -- Estado
	begin
	if	(:new.cd_material is null) then
		select	substr(obter_dados_pf_pj(null,a.cd_cgc_emitente, 'UF'), 1, 10)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(obter_dados_pf_pj(null,a.cd_cgc_emitente, 'UF'), 1, 10)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 5) then -- Cnpj
	begin
	if	(:new.cd_material is null) then
		select	a.cd_cgc_emitente
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	a.cd_cgc_emitente
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 6) then -- Inscri��o estadual
	begin
	if	(:new.cd_material is null) then
		select	substr(obter_dados_pf_pj(null,a.cd_cgc_emitente, 'IE'), 1, 40)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(obter_dados_pf_pj(null,a.cd_cgc_emitente, 'IE'), 1, 40)
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 7) then -- Natureza Opera��o
	begin
	if	(:new.cd_material is null) then
		select	substr(obter_dados_natureza_operacao(b.cd_natureza_operacao, 'CF'), 1, 20)
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(obter_dados_natureza_operacao(b.cd_natureza_operacao, 'CF'), 1, 20)
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;

	:new.vl_atual	:= obter_dados_natureza_operacao(:new.cd_natureza_operacao, 'CF');

	end;
elsif	(:new.ie_tipo_correcao = 8) then -- C�digo fiscal da opera��o
	begin
	if	(:new.cd_material is null) then
		select	max(f.cd_codigo_valores_fiscais),
			count(*)
		into	vl_anterior_w,
			qt_registros_w
		from	nota_fiscal_item b,
			fis_variacao_fiscal f
		where	f.nr_sequencia		= b.nr_seq_variacao_fiscal
		and	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	max(f.cd_codigo_valores_fiscais),
			count(*)
		into	vl_anterior_w,
			qt_registros_w
		from	nota_fiscal_item b,
			fis_variacao_fiscal f
		where	f.nr_sequencia		= b.nr_seq_variacao_fiscal
		and	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;

	if	(qt_registros_w = 0) then
		begin
		if	(:new.cd_material is null) then
			select	o.cd_cfop
			into	vl_anterior_w
			from	nota_fiscal_item b,
				natureza_operacao o
			where	o.cd_natureza_operacao = b.cd_natureza_operacao
			and	b.cd_procedimento	= :new.cd_procedimento
			and	b.ie_origem_proced	= :new.ie_origem_proced
			and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
			and	b.nr_item_nf 		= :new.nr_item_nf;
		else
			select	o.cd_cfop
			into	vl_anterior_w
			from	nota_fiscal_item b,
				natureza_operacao o
			where	o.cd_natureza_operacao = b.cd_natureza_operacao
			and	b.cd_material		= :new.cd_material
			and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
			and	b.nr_item_nf 		= :new.nr_item_nf;
		end if;
		end;
	end if;

	end;
elsif	(:new.ie_tipo_correcao = 9) then -- Via de transporte
	begin
	select	substr(obter_dados_pf_pj(null,t.cd_cnpj, 'N'), 1, 255)
	into	vl_anterior_w
	from	nota_fiscal_transportadora t,
		nota_fiscal a
	where	a.nr_sequencia		= t.nr_seq_nota
	and	a.nr_sequencia		= :new.nr_seq_nota_fiscal;

	end;
elsif	(:new.ie_tipo_correcao = 10) then -- Data de emiss�o
	begin
	if	(:new.cd_material is null) then
		select	a.dt_emissao
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	a.dt_emissao
		into	vl_anterior_w
		from	nota_fiscal_item b,
			nota_fiscal a
		where	a.nr_sequencia		= b.nr_sequencia
		and	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 11) then -- Data de sa�da
	begin
	--  N�o tem o 11 -- Data de sa�da -- Inativo
	null;
	end;

elsif	(:new.ie_tipo_correcao = 12) then -- Unidade
	begin
	if	(:new.cd_material is null) then
		select	substr(Obter_Unidade_Medida(b.cd_unidade_medida_compra), 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(Obter_Unidade_Medida(b.cd_unidade_medida_compra), 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 13) then -- Quantidade
	begin
	if	(:new.cd_material is null) then
		select	b.qt_item_nf
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	b.qt_item_nf
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 14) then -- Descri��o do servi�o
	begin
	--  N�o tem o 14 -- Descri��o do servi�o -- Ativo
	null;
	end;
elsif	(:new.ie_tipo_correcao = 15) then -- Pre�o unit�rio
	begin
	if	(:new.cd_material is null) then
		select	b.vl_unitario_item_nf
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	b.vl_unitario_item_nf
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 16) then -- Valor do produto
	begin
	-- N�o tem o 16 -- Valor do produto -- Inativo
	null;
	end;
elsif	(:new.ie_tipo_correcao in (17,48)) then -- Classifica��o fiscal e NCM
	begin
	if	(:new.cd_material is null) then
		select	substr(nvl(a.nr_seq_ncm,'0'), 1, 10)
		into	vl_anterior_w
		from	material_fiscal a,
			nota_fiscal_item b
		where	a.cd_material		= b.cd_material
		and	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(nvl(a.nr_seq_ncm,'0'), 1, 10)
		into	vl_anterior_w
		from	material_fiscal a,
			nota_fiscal_item b
		where	a.cd_material		= b.cd_material
		and	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 18) then -- Aliquota IPI
	begin
	if	(:new.cd_material is null) then
		select	substr(nvl(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'IPI', 'TX'),'0'), 1, 20) vl_aliquota_ipi
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(nvl(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'IPI', 'TX'),'0'), 1, 20) vl_aliquota_ipi
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 19) then -- Valor do IPI
	begin
	if	(:new.cd_material is null) then
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'IPI', 'TRIB'), 1, 20) vl_ipi
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'IPI', 'TRIB'), 1, 20) vl_ipi
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 20) then -- Base de c�lculo IPI
	begin
	if	(:new.cd_material is null) then
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'IPI', 'BC'), 1, 20) vl_base_ipi
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'IPI', 'BC'), 1, 20) vl_base_ipi
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 21) then -- Valor total nota
	begin
	select	substr(a.vl_total_nota, 1, 20)
	into	vl_anterior_w
	from	nota_fiscal a
	where	a.nr_sequencia		= :new.nr_seq_nota_fiscal;
	end;
elsif	(:new.ie_tipo_correcao = 22) then -- Valor total nota
	begin
	-- N�o tem o 22 -- Valor por extenso -- Inativo
	null;
	end;
elsif	(:new.ie_tipo_correcao = 23) then -- Valor do ICMS
	begin
	if	(:new.cd_material is null) then
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'TRIB'), 1, 20) vl_icms_deb_cred
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'TRIB'), 1, 20) vl_icms_deb_cred
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 24) then -- Base de c�lculo do ICMS
	begin
	if	(:new.cd_material is null) then
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'BC'), 1, 20) vl_base_icms
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ICMS', 'BC'), 1, 20) vl_base_icms
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 25) then -- Nome transportadora
	begin
	--  N�o tem o 25 -- Nome transportadora -- Inativo
	null;
	end;
elsif	(:new.ie_tipo_correcao = 26) then -- Endere�o da transportadora
	begin
		select	substr(obter_dados_pf_pj(null, t.cd_cnpj, 'R'), 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_transportadora t,
			nota_fiscal a
		where	a.nr_sequencia		= t.nr_seq_nota
		and	a.nr_sequencia		= :new.nr_seq_nota_fiscal;
	null;
	end;
elsif	(:new.ie_tipo_correcao = 27) then -- Termo isen��o do IPI
	begin
	-- N�o tem o 27 -- Termo isen��o do IPI -- Inativo
	null;
	end;
elsif	(:new.ie_tipo_correcao = 28) then -- Termo de isen��o do ICMS
	begin
	-- N�o tem o 28 -- Termo de isen��o do ICMS -- Inativo
	null;
	end;
elsif	(:new.ie_tipo_correcao = 29) then -- Peso bruto/liquido
	begin
	select	t.qt_peso_bruto || '/' || t.qt_peso_liquido
	into	vl_anterior_w
	from	nota_fiscal_transportadora t,
		nota_fiscal a
	where	a.nr_sequencia		= t.nr_seq_nota
	and	a.nr_sequencia		= :new.nr_seq_nota_fiscal;
	end;
elsif	(:new.ie_tipo_correcao = 30) then -- Volume
	begin
	select	t.qt_volume
	into	vl_anterior_w
	from	nota_fiscal_transportadora t,
		nota_fiscal a
	where	a.nr_sequencia		= t.nr_seq_nota
	and	a.nr_sequencia		= :new.nr_seq_nota_fiscal;
	end;
elsif	(:new.ie_tipo_correcao = 31) then -- Rasuras
	begin
	-- N�o tem o 31 -- Rasuras -- Inativo
	null;
	end;
elsif	(:new.ie_tipo_correcao = 32) then -- Vencimento
	begin
	select	max(dt_vencimento)
	into	vl_anterior_w
	from	nota_fiscal_venc v,
		nota_fiscal_item b,
		nota_fiscal a
	where	a.nr_sequencia		= v.nr_sequencia
	and	a.nr_sequencia		= b.nr_sequencia
	and	b.nr_sequencia		= :new.nr_seq_nota_fiscal;
	end;
elsif	(:new.ie_tipo_correcao = 33) then -- Hist�rico
	begin
	-- N�o tem o 33 -- Hist�rico -- Inativo
	null;
	end;
elsif	(:new.ie_tipo_correcao = 34) then -- Per�odo
	begin
	--  N�o tem o 34 -- Per�odo -- Inativo
	null;
	end;
elsif	(:new.ie_tipo_correcao = 35) then -- Base de c�lculo ISS
	begin
	if	(:new.cd_material is null) then
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ISS', 'BC'), 1, 20) vl_base_iss
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ISS', 'BC'), 1, 20) vl_base_iss
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 36) then -- Valor total ISS
	begin
	if	(:new.cd_material is null) then
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ISS', 'TRIB'), 1, 20) vl_iss
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(nfe_obter_valor_trib_item(b.nr_sequencia, b.nr_item_nf, 'ISS', 'TRIB'), 1, 20) vl_iss
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 37) then -- Observa��o
	begin
	if	(:new.cd_material is null) then
		select	substr(b.ds_observacao, 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	substr(b.ds_observacao, 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item b
		where	b.cd_material		= :new.cd_material
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 38) then -- Lote do medicamento
	begin
	select	substr(m.ds_lote_fornec, 1, 255)
	into	vl_anterior_w
	from	nota_fiscal_item b,
		material_lote_fornec m
	where	b.nr_seq_lote_fornec = m.nr_sequencia(+)
	and	b.nr_sequencia	     = :new.nr_seq_nota_fiscal;
	end;
elsif	(:new.ie_tipo_correcao = 39) then -- Lote fornecedor
	begin
	--verificar se tem lote fornecedor vinculado a nota_fiscal_item (aba lote fornecedor na nota fiscal)
	select 	count(*)
	into	qt_registros_w
	from	nota_fiscal_item_lote b,
		material_lote_fornec m
	where	b.nr_seq_lote_fornec	= m.nr_sequencia
	and	b.nr_seq_nota	     	= :new.nr_seq_nota_fiscal
	and	b.nr_item_nf 	     	= :new.nr_item_nf
	and 	b.nr_sequencia		= :new.nr_seq_item_lote;

	if	(qt_registros_w > 0) then
		select	substr(m.ds_lote_fornec, 1, 255)
		into	vl_anterior_w
		from	nota_fiscal_item_lote b,
			material_lote_fornec m
		where	b.nr_seq_lote_fornec = m.nr_sequencia
		and	b.nr_seq_nota	     = :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 	     = :new.nr_item_nf
		and 	b.nr_sequencia	     = :new.nr_seq_item_lote;

	end if;
	end;
elsif	(:new.ie_tipo_correcao = 40) then -- Observa��o da Nota
	begin
	select	substr(b.ds_observacao, 1, 255)
	into	vl_anterior_w
	from	nota_fiscal b
	where	b.nr_sequencia	     = :new.nr_seq_nota_fiscal;
	end;
elsif	(:new.ie_tipo_correcao = 41) then
	begin
	--  N�o tem o 41 -- N�o existe
	null;
	end;
elsif	(:new.ie_tipo_correcao = 42) then -- Descri��o do item da nota
	begin
	if	(:new.cd_material is null) then
		select	a.ds_material
		into	vl_anterior_w
		from	nota_fiscal_item b,
			material a
		where	b.cd_procedimento	= :new.cd_procedimento
		and	b.ie_origem_proced	= :new.ie_origem_proced
		and	b.nr_sequencia		= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 		= :new.nr_item_nf;
	else
		select	a.ds_material
		into	vl_anterior_w
		from	nota_fiscal_item b,
			material a
		where	b.cd_material	= a.cd_material
		and	b.nr_sequencia	= :new.nr_seq_nota_fiscal
		and	b.nr_item_nf 	= :new.nr_item_nf;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 43) then
	begin
	--  N�o tem o 43 -- N�o existe
	null;
	end;
elsif	(:new.ie_tipo_correcao = 44) then -- Tipo frete
	begin
	/*
	T - 'Por conta de terceiros'
	C - 'Por conta do emitente'
	F - 'Por conta do destinat�rio/remetente'
	'Sem frete'
	*/
	select	substr(decode(t.ie_tipo_frete,'T',wheb_mensagem_pck.get_texto(310761),'C',wheb_mensagem_pck.get_texto(310762),'F',wheb_mensagem_pck.get_texto(310763),wheb_mensagem_pck.get_texto(310764)),1,255)
	into	vl_anterior_w
	from	nota_fiscal_transportadora t,
		nota_fiscal a
	where	a.nr_sequencia		= t.nr_seq_nota
	and	a.nr_sequencia		= :new.nr_seq_nota_fiscal;

	select	substr(decode(:new.ie_tipo_frete,'T',wheb_mensagem_pck.get_texto(310761),'C',wheb_mensagem_pck.get_texto(310762),'F',wheb_mensagem_pck.get_texto(310763),wheb_mensagem_pck.get_texto(310764)),1,255)
	into	vl_atual_w
	from	dual;

	:new.vl_atual := vl_atual_w;
	end;
elsif	(:new.ie_tipo_correcao = 45) then -- CST
	begin
	if	(:new.cd_material is null) then
		select decode(obter_tipo_tributo(a.cd_tributo), 'PIS', b.ie_tributacao_pis, 'COFINS', b.ie_tributacao_cofins)
		into	vl_anterior_w
		from	nota_fiscal_item_trib a,
			procedimento_fiscal b,
			nota_fiscal_item c,
			tributo d
		where	c.nr_item_nf 		= a.nr_item_nf
		and	c.nr_sequencia 		= a.nr_sequencia
		and	a.cd_tributo 		= d.cd_tributo
		and	b.cd_procedimento	= c.cd_procedimento
		and	b.ie_origem_proced	= c.ie_origem_proced
		and	a.nr_sequencia 		= :new.nr_seq_nota_fiscal
		and	a.nr_item_nf  		= :new.nr_item_nf
		and	a.cd_tributo 		= :new.cd_tributo;
	else
		select decode(obter_tipo_tributo(a.cd_tributo), 'ICMS', b.ie_tributacao_icms, 'IPI', b.ie_tributacao_ipi, 'PIS', b.ie_tributacao_pis_saida, 'COFINS', b.ie_tribut_cofins_saida)
		into	vl_anterior_w
		from	nota_fiscal_item_trib a,
			material_fiscal b,
			nota_fiscal_item c,
			tributo d
		where	c.nr_item_nf 	= a.nr_item_nf
		and	c.nr_sequencia 	= a.nr_sequencia
		and	a.cd_tributo 	= d.cd_tributo
		and	c.cd_material 	= b.cd_material
		and	a.nr_sequencia 	= :new.nr_seq_nota_fiscal
		and	a.nr_item_nf  	= :new.nr_item_nf
		and	a.cd_tributo 	= :new.cd_tributo;
	end if;
	end;
elsif	(:new.ie_tipo_correcao = 46) then
	begin

	if	(:new.cd_material is null) then
		select	max(b.cd_situacao)
		into	vl_anterior_w
		from	nota_fiscal_item c,
			situacao_trib_prest_serv b
		where	c.nr_seq_classif_trib	= b.nr_sequencia
		and	c.nr_sequencia 		= :new.nr_seq_nota_fiscal
		and	c.nr_item_nf  		= :new.nr_item_nf
		and	c.cd_procedimento	= :new.cd_procedimento;
	else
		select	max(b.cd_situacao)
		into	vl_anterior_w
		from	nota_fiscal_item c,
			situacao_trib_prest_serv b
		where	c.nr_seq_classif_trib	= b.nr_sequencia
		and	c.nr_sequencia 		= :new.nr_seq_nota_fiscal
		and	c.nr_item_nf  		= :new.nr_item_nf
		and	c.cd_material		= :new.cd_material;
	end if;
	select	max(cd_situacao)
	into	vl_atual_w
	from	situacao_trib_prest_serv
	where	nr_sequencia = :new.nr_seq_classif_trib;

	:new.vl_atual := vl_atual_w;
	end;
elsif	(:new.ie_tipo_correcao = 47) then
	begin
	vl_anterior_w := :new.vl_anterior;

	end;
end if;

exception
when others then
	vl_anterior_w := null;
end;

:new.vl_anterior		:= vl_anterior_w;

end;
/


ALTER TABLE TASY.FIS_CARTA_CORRECAO ADD (
  CONSTRAINT FISCARTA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FIS_CARTA_CORRECAO ADD (
  CONSTRAINT FISCARTA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT FISCARTA_NATOPER_FK 
 FOREIGN KEY (CD_NATUREZA_OPERACAO) 
 REFERENCES TASY.NATUREZA_OPERACAO (CD_NATUREZA_OPERACAO),
  CONSTRAINT FISCARTA_LISTNCM_FK 
 FOREIGN KEY (CD_NCM) 
 REFERENCES TASY.LISTA_NCM (NR_SEQUENCIA),
  CONSTRAINT FISCARTA_PESJURI_FK 
 FOREIGN KEY (CD_CGC_EMITENTE) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT FISCARTA_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT FISCARTA_NOTITLO_FK 
 FOREIGN KEY (NR_SEQ_ITEM_LOTE) 
 REFERENCES TASY.NOTA_FISCAL_ITEM_LOTE (NR_SEQUENCIA),
  CONSTRAINT FISCARTA_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO),
  CONSTRAINT FISCARTA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT FISCARTA_SITTRPS_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_TRIB) 
 REFERENCES TASY.SITUACAO_TRIB_PREST_SERV (NR_SEQUENCIA));

GRANT SELECT ON TASY.FIS_CARTA_CORRECAO TO NIVEL_1;

