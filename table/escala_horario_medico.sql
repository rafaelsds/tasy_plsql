ALTER TABLE TASY.ESCALA_HORARIO_MEDICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_HORARIO_MEDICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_HORARIO_MEDICO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(15 BYTE),
  HR_INICIAL           VARCHAR2(20 BYTE),
  HR_FINAL             VARCHAR2(20 BYTE),
  DT_FINAL             DATE,
  HR_INICIAL_DATE      DATE,
  HR_FINAL_DATE        DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCHORME_PESFISI_FK_I ON TASY.ESCALA_HORARIO_MEDICO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCHORME_PK ON TASY.ESCALA_HORARIO_MEDICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_HORARIO_MEDICO_SYNC
before insert or update ON TASY.ESCALA_HORARIO_MEDICO for each row
declare

begin

begin
if (nvl(:old.HR_INICIAL,'X') <> nvl(:new.HR_INICIAL,'X')) then
 if (:new.HR_INICIAL is not null) then
  :new.HR_INICIAL_DATE := pkg_date_utils.get_Time(sysdate,:new.HR_INICIAL);
 else
  :new.HR_INICIAL_DATE := null;
 end if;
elsif (nvl(:old.HR_INICIAL_DATE,pkg_date_utils.get_Time('')) <> nvl(:new.HR_INICIAL_DATE,pkg_date_utils.get_Time(''))) then
 :new.HR_INICIAL := lpad(pkg_date_utils.extract_field('HOUR',:new.HR_INICIAL_DATE),2,0) || ':' || lpad(pkg_date_utils.extract_field('MINUTE',:new.HR_INICIAL_DATE),2,0);
end if;

if (nvl(:old.HR_FINAL,'X') <> nvl(:new.HR_FINAL,'X')) then
 if (:new.HR_FINAL is not null) then
  :new.HR_FINAL_DATE := pkg_date_utils.get_Time(sysdate,:new.HR_FINAL);
 else
  :new.HR_FINAL_DATE := null;
 end if;
elsif (nvl(:old.HR_FINAL_DATE,pkg_date_utils.get_Time('')) <> nvl(:new.HR_FINAL_DATE,pkg_date_utils.get_Time(''))) then
 :new.HR_FINAL := lpad(pkg_date_utils.extract_field('HOUR',:new.HR_FINAL_DATE),2,0) || ':' || lpad(pkg_date_utils.extract_field('MINUTE',:new.HR_FINAL_DATE),2,0);
end if;
exception
when others then
 null;
end;

end;
/


ALTER TABLE TASY.ESCALA_HORARIO_MEDICO ADD (
  CONSTRAINT ESCHORME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_HORARIO_MEDICO ADD (
  CONSTRAINT ESCHORME_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_HORARIO_MEDICO TO NIVEL_1;

