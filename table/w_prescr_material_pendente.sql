ALTER TABLE TASY.W_PRESCR_MATERIAL_PENDENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_PRESCR_MATERIAL_PENDENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PRESCR_MATERIAL_PENDENTE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_PRESCRICAO          NUMBER(14),
  DT_PRESCRICAO          DATE,
  NR_ATENDIMENTO         NUMBER(10),
  DT_PRIMEIRO_HORARIO    DATE,
  NM_MEDICO              VARCHAR2(60 BYTE),
  NM_PACIENTE            VARCHAR2(60 BYTE),
  NR_ITENS               NUMBER(10),
  NR_ITENS_URGENTES      NUMBER(10),
  IE_ITEM_URGENTE        NUMBER(2),
  DT_LIBERACAO           DATE,
  DT_LIBERACAO_MEDICO    DATE,
  DT_EMISSAO_FARMACIA    DATE,
  NM_USUARIO             VARCHAR2(15 BYTE),
  NM_USUARIO_ORIGINAL    VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO   NUMBER(5),
  DS_SETOR_ATENDIMENTO   VARCHAR2(100 BYTE),
  CD_LOCAL_ESTOQUE       NUMBER(10),
  DS_SETOR_ATUAL         VARCHAR2(200 BYTE),
  DS_UNIDADE             VARCHAR2(20 BYTE),
  IE_SN                  VARCHAR2(1 BYTE),
  IE_ACM                 VARCHAR2(1 BYTE),
  DT_LIBERACAO_FARMACIA  DATE,
  IE_LIB_FARM            VARCHAR2(1 BYTE),
  DS_TURNO               VARCHAR2(200 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WPRMAPE_PK ON TASY.W_PRESCR_MATERIAL_PENDENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WPRMAPE_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_PRESCR_MATERIAL_PENDENTE ADD (
  CONSTRAINT WPRMAPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_PRESCR_MATERIAL_PENDENTE TO NIVEL_1;

