ALTER TABLE TASY.PESSOA_FISICA_BIOMETRIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_FISICA_BIOMETRIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_FISICA_BIOMETRIA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  DS_BIOMETRIA          VARCHAR2(4000 BYTE)     NOT NULL,
  CD_EMPRESA_BIOMETRIA  NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_DEDO               NUMBER(2)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESFIBI_PESFISI_FK_I ON TASY.PESSOA_FISICA_BIOMETRIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFIBI_PK ON TASY.PESSOA_FISICA_BIOMETRIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESFIBI_UK ON TASY.PESSOA_FISICA_BIOMETRIA
(CD_PESSOA_FISICA, IE_DEDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESFIBI_UK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SUBJECT_BIOMETRICS_ATUAL BEFORE
    DELETE OR INSERT OR UPDATE OF NR_SEQUENCIA, CD_PESSOA_FISICA, DS_BIOMETRIA ON TASY.PESSOA_FISICA_BIOMETRIA 
    FOR EACH ROW
DECLARE
    rc_usuario_biometria        pessoa_fisica_biometria%rowtype;
BEGIN
    rc_usuario_biometria.DS_BIOMETRIA :=:new.DS_BIOMETRIA;
    rc_usuario_biometria.NR_SEQUENCIA :=:new.NR_SEQUENCIA;
    rc_usuario_biometria.CD_PESSOA_FISICA :=:new.CD_PESSOA_FISICA;
    IF
            inserting
    THEN
            INSERT INTO SUBJECT_BIOMETRICS
            (ID_SUBJECT, DS_HASH, DS_SALT)
            SELECT SUB.ID, GET_MD5_HASH_VALUE(rc_usuario_biometria.DS_BIOMETRIA), GET_MD5_HASH_VALUE(TO_CHAR(rc_usuario_biometria.NR_SEQUENCIA))
            FROM usuario USR
            INNER JOIN SUBJECT SUB on (USR.NM_USUARIO = SUB.DS_LOGIN)
            WHERE USR.IE_SITUACAO = 'A'
            AND rc_usuario_biometria.CD_PESSOA_FISICA = usr.CD_PESSOA_FISICA;

    ELSIF
            updating
    THEN
            DELETE FROM subject_biometrics
            WHERE EXISTS (SELECT 1 FROM usuario USR
            INNER JOIN SUBJECT SUB on (USR.NM_USUARIO = SUB.DS_LOGIN)
            WHERE USR.NM_USUARIO = rc_usuario_biometria.CD_PESSOA_FISICA
            AND subject_biometrics.DS_SALT = GET_MD5_HASH_VALUE(TO_CHAR(rc_usuario_biometria.NR_SEQUENCIA))
            AND subject_biometrics.ID_SUBJECT = SUB.ID
            AND rc_usuario_biometria.CD_PESSOA_FISICA = usr.CD_PESSOA_FISICA);
            INSERT INTO SUBJECT_BIOMETRICS
            (ID_SUBJECT, DS_HASH, DS_SALT)
            SELECT SUB.ID, GET_MD5_HASH_VALUE(rc_usuario_biometria.DS_BIOMETRIA), GET_MD5_HASH_VALUE(TO_CHAR(rc_usuario_biometria.NR_SEQUENCIA))
            FROM usuario USR
            INNER JOIN SUBJECT SUB on (USR.NM_USUARIO = SUB.DS_LOGIN)
            WHERE USR.IE_SITUACAO = 'A'
            AND rc_usuario_biometria.CD_PESSOA_FISICA = usr.CD_PESSOA_FISICA;
    ELSE
            DELETE FROM subject_biometrics
            WHERE EXISTS (SELECT 1 FROM usuario USR
            INNER JOIN SUBJECT SUB on (USR.NM_USUARIO = SUB.DS_LOGIN)
            WHERE USR.NM_USUARIO = rc_usuario_biometria.CD_PESSOA_FISICA
            AND subject_biometrics.DS_SALT = GET_MD5_HASH_VALUE(TO_CHAR(rc_usuario_biometria.NR_SEQUENCIA))
            AND subject_biometrics.ID_SUBJECT = SUB.ID
            AND rc_usuario_biometria.CD_PESSOA_FISICA = usr.CD_PESSOA_FISICA);
    END IF;

END;
/


CREATE OR REPLACE TRIGGER TASY.PESSOA_FISICA_BIOMETRIA_tp  after update ON TASY.PESSOA_FISICA_BIOMETRIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_DEDO,1,4000),substr(:new.IE_DEDO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DEDO',ie_log_w,ds_w,'PESSOA_FISICA_BIOMETRIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BIOMETRIA,1,4000),substr(:new.DS_BIOMETRIA,1,4000),:new.nm_usuario,nr_seq_w,'DS_BIOMETRIA',ie_log_w,ds_w,'PESSOA_FISICA_BIOMETRIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PESSOA_FISICA_BIOMETRIA ADD (
  CONSTRAINT PESFIBI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PESFIBI_UK
 UNIQUE (CD_PESSOA_FISICA, IE_DEDO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_FISICA_BIOMETRIA ADD (
  CONSTRAINT PESFIBI_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PESSOA_FISICA_BIOMETRIA TO NIVEL_1;

