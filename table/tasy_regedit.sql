ALTER TABLE TASY.TASY_REGEDIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_REGEDIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_REGEDIT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_MAQUINA            VARCHAR2(40 BYTE),
  DS_CLIENTE            VARCHAR2(40 BYTE),
  QT_BLOB_CACHE         NUMBER(10),
  QT_BLOB_SIZE          NUMBER(10),
  DS_DIR_EMAIL          VARCHAR2(255 BYTE),
  DS_DIR_PADRAO         VARCHAR2(255 BYTE),
  DS_DIR_TEMP           VARCHAR2(255 BYTE),
  DS_EMAIL_LOGO_FILE    VARCHAR2(255 BYTE),
  DS_LOGO_FILE          VARCHAR2(255 BYTE),
  DS_PORTA_SERIAL       VARCHAR2(20 BYTE),
  IE_BIOMETRIA_LOGAR    VARCHAR2(1 BYTE)        NOT NULL,
  DS_SERVIDOR_DOMINIO   VARCHAR2(60 BYTE),
  IE_GERAR_MB_DIR_TEMP  VARCHAR2(1 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TASREGE_PK ON TASY.TASY_REGEDIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TASY_REGEDIT ADD (
  CONSTRAINT TASREGE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TASY_REGEDIT TO NIVEL_1;

