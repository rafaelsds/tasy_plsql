ALTER TABLE TASY.REGRA_VOLUME_LEITE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_VOLUME_LEITE CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_VOLUME_LEITE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_IDADE_MIN         NUMBER(3),
  QT_IDADE_MIN_MES     NUMBER(3),
  QT_IDADE_MIN_DIA     NUMBER(3),
  QT_IDADE_MAX         NUMBER(3),
  QT_IDADE_MAX_MES     NUMBER(3),
  QT_IDADE_MAX_DIA     NUMBER(3),
  QT_VOLUME_MINIMO     NUMBER(15,4)             NOT NULL,
  QT_VOLUME_MAXIMO     NUMBER(15,4)             NOT NULL,
  CD_MATERIAL          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGVOLL_MATERIA_FK_I ON TASY.REGRA_VOLUME_LEITE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGVOLL_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGVOLL_PK ON TASY.REGRA_VOLUME_LEITE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_VOLUME_LEITE ADD (
  CONSTRAINT REGVOLL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_VOLUME_LEITE ADD (
  CONSTRAINT REGVOLL_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.REGRA_VOLUME_LEITE TO NIVEL_1;

