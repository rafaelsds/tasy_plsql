ALTER TABLE TASY.PLS_TAB_BAIXO_RISCO_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TAB_BAIXO_RISCO_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TAB_BAIXO_RISCO_PROC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_TAB_BAIXO_RISCO  NUMBER(10)            NOT NULL,
  QT_MAXIMA               NUMBER(12,4)          NOT NULL,
  CD_PROCEDIMENTO         NUMBER(15)            NOT NULL,
  IE_ORIGEM_PROCED        NUMBER(10)            NOT NULL,
  DT_INICIO_VIGENCIA      DATE,
  DT_FIM_VIGENCIA         DATE,
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSTBRP_PK ON TASY.PLS_TAB_BAIXO_RISCO_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTBRP_PLSTBRC_FK_I ON TASY.PLS_TAB_BAIXO_RISCO_PROC
(NR_SEQ_TAB_BAIXO_RISCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSTBRP_PROCEDI_FK_I ON TASY.PLS_TAB_BAIXO_RISCO_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_tab_baixo_risco_proc_atual
before insert or update ON TASY.PLS_TAB_BAIXO_RISCO_PROC for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_TAB_BAIXO_RISCO_PROC_tp  after update ON TASY.PLS_TAB_BAIXO_RISCO_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'PLS_TAB_BAIXO_RISCO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'PLS_TAB_BAIXO_RISCO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PLS_TAB_BAIXO_RISCO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAXIMA,1,4000),substr(:new.QT_MAXIMA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAXIMA',ie_log_w,ds_w,'PLS_TAB_BAIXO_RISCO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_TAB_BAIXO_RISCO_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TAB_BAIXO_RISCO,1,4000),substr(:new.NR_SEQ_TAB_BAIXO_RISCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TAB_BAIXO_RISCO',ie_log_w,ds_w,'PLS_TAB_BAIXO_RISCO_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_TAB_BAIXO_RISCO_PROC ADD (
  CONSTRAINT PLSTBRP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TAB_BAIXO_RISCO_PROC ADD (
  CONSTRAINT PLSTBRP_PLSTBRC_FK 
 FOREIGN KEY (NR_SEQ_TAB_BAIXO_RISCO) 
 REFERENCES TASY.PLS_TAB_BAIXO_RISCO_CTA (NR_SEQUENCIA),
  CONSTRAINT PLSTBRP_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_TAB_BAIXO_RISCO_PROC TO NIVEL_1;

