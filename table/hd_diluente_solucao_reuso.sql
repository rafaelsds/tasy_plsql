ALTER TABLE TASY.HD_DILUENTE_SOLUCAO_REUSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_DILUENTE_SOLUCAO_REUSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_DILUENTE_SOLUCAO_REUSO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_SOLUCAO_REUSO  NUMBER(10),
  CD_UNIDADE_MEDIDA     VARCHAR2(30 BYTE),
  QT_MATERIAL           NUMBER(10,3),
  CD_MATERIAL           NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDDILSO_HDSOLRE_FK_I ON TASY.HD_DILUENTE_SOLUCAO_REUSO
(NR_SEQ_SOLUCAO_REUSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDILSO_HDSOLRE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDILSO_MATERIA_FK_I ON TASY.HD_DILUENTE_SOLUCAO_REUSO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDILSO_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDDILSO_PK ON TASY.HD_DILUENTE_SOLUCAO_REUSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDILSO_PK
  MONITORING USAGE;


CREATE INDEX TASY.HDDILSO_UNIMEDI_FK_I ON TASY.HD_DILUENTE_SOLUCAO_REUSO
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDILSO_UNIMEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.HD_DILUENTE_SOLUCAO_REUSO ADD (
  CONSTRAINT HDDILSO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_DILUENTE_SOLUCAO_REUSO ADD (
  CONSTRAINT HDDILSO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT HDDILSO_HDSOLRE_FK 
 FOREIGN KEY (NR_SEQ_SOLUCAO_REUSO) 
 REFERENCES TASY.HD_SOLUCAO_REUSO (NR_SEQUENCIA),
  CONSTRAINT HDDILSO_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.HD_DILUENTE_SOLUCAO_REUSO TO NIVEL_1;

