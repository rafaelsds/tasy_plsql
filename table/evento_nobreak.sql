ALTER TABLE TASY.EVENTO_NOBREAK
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EVENTO_NOBREAK CASCADE CONSTRAINTS;

CREATE TABLE TASY.EVENTO_NOBREAK
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  NR_SEQ_EVENTO   NUMBER(10)                    NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.EVENTONO_PK ON TASY.EVENTO_NOBREAK
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVENTONO_PK
  MONITORING USAGE;


CREATE INDEX TASY.EVENTONO_TIPOEVENO_FK_I ON TASY.EVENTO_NOBREAK
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVENTONO_TIPOEVENO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.EVENTO_NOBREAK_INSERT
after Insert ON TASY.EVENTO_NOBREAK FOR EACH ROW
DECLARE

nm_usuario_sms_w	varchar2(20);
ds_senha_sms_w		varchar2(20);
ds_status_envio_w	varchar(60);
ds_remetente_w		varchar2(255);
nm_usuario_envio_w	varchar2(15);
nr_celular_envio_w	varchar2(15);
ds_email_envio_w	varchar2(255);
ie_forma_aviso_w	varchar2(15);
ds_evento_nobreak_w	varchar2(60);
ie_retorno_sms_w	number(3);
ds_contato_envio_w	varchar2(255);

id_sms_w			number(10);
ds_esconder_ddi_w	varchar(1);

CURSOR c01 IS
	select 	DECODE(ds_esconder_ddi_w, 'S', a.nr_ddd_celular||a.NR_TELEFONE_CELULAR, a.nr_ddi_celular||a.NR_TELEFONE_CELULAR) NR_TELEFONE_CELULAR,
			obter_compl_pf(a.cd_pessoa_fisica,1,'M'),
			b.ie_forma_aviso,
			b.nm_usuario_aviso
	from	pessoa_fisica a,
			evento_nobreak_usuario b
	where	a.cd_pessoa_fisica 	= obter_pessoa_fisica_usuario(b.nm_usuario_aviso,'C')
	and		b.nr_seq_evento	 	= :new.nr_seq_evento;

begin

ds_esconder_ddi_w := OBTER_VALOR_PARAM_USUARIO(0,214,0,obter_usuario_ativo,obter_estabelecimento_ativo);

select	ds_evento
into	ds_evento_nobreak_w
from	tipo_evento_nobreak
where	nr_sequencia 		= :new.nr_seq_evento;

OPEN c01;
LOOP
FETCH c01 INTO 	nr_celular_envio_w,
		ds_email_envio_w,
		ie_forma_aviso_w,
		nm_usuario_envio_w;
BEGIN
 	EXIT WHEN c01%NOTFOUND;

	if ( upper(ie_forma_aviso_w) = 'MCEL'  ) then
		nr_celular_envio_w := replace(nr_celular_envio_w,'(','');
		nr_celular_envio_w := replace(nr_celular_envio_w,')','');
		nr_celular_envio_w := replace(nr_celular_envio_w,' ','');
		nr_celular_envio_w := replace(nr_celular_envio_w,'-','');
		if (nr_celular_envio_w is not null) then
			wheb_sms.enviar_sms('Nobreak',nr_celular_envio_w,ds_evento_nobreak_w,obter_usuario_ativo,id_sms_w);
		end if;
	end if;
	if ( upper(ie_forma_aviso_w)  = 'EMAIL') then
		Enviar_Email(	'Evento Nobreak',
				ds_evento_nobreak_w,
				ds_remetente_w,
				ds_email_envio_w,
				nm_usuario_envio_w,
				'A');
		ds_contato_envio_w	:= ds_email_envio_w;
		ds_status_envio_w	:= 'Email enviado com sucesso';
	end if;

	if ( upper(ie_forma_aviso_w)  = 'CI') then
		insert into comunic_interna(
			nr_sequencia,
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			ie_gerencial,
			nm_usuario_destino,
			ie_geral,
			cd_estab_destino,
			nr_seq_classif,
			dt_liberacao,
			nm_usuario,
			dt_atualizacao)
		values(
			comunic_interna_seq.nextval,
			sysdate,
			'Evento Nobreak',
			ds_evento_nobreak_w,
			'N',
			nm_usuario_envio_w,
			'N',
			1,
			5,
			sysdate,
			'NoBreak',
			sysdate);
		ds_contato_envio_w	:= '';
		ds_status_envio_w	:= 'Comunicação criada com sucesso';
	end if;


	insert into evento_nobreak_aviso (
		nr_sequencia,
		ds_status,
		dt_envio,
		nm_usuario_envio,
		nr_seq_evento,
		ds_contato_envio,
		ie_forma_envio,
		dt_atualizacao,
		nm_usuario)
	values
		(evento_nobreak_aviso_seq.nextval,
		ds_status_envio_w,
		sysdate,
		nm_usuario_envio_w,
		:new.nr_sequencia,
		ds_contato_envio_w,
		ie_forma_aviso_w,
		sysdate,
		'Nobreak');
END;
END LOOP;
CLOSE c01;

END;
/


ALTER TABLE TASY.EVENTO_NOBREAK ADD (
  CONSTRAINT EVENTONO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EVENTO_NOBREAK ADD (
  CONSTRAINT EVENTONO_TIPOEVENO_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.TIPO_EVENTO_NOBREAK (NR_SEQUENCIA));

GRANT SELECT ON TASY.EVENTO_NOBREAK TO NIVEL_1;

