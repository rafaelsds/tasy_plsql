ALTER TABLE TASY.PLS_OC_CTA_VAL_PREST_DIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_VAL_PREST_DIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_VAL_PREST_DIF
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_OC_CTA_COMB         NUMBER(10)         NOT NULL,
  IE_VALIDA_PREST_INFO       VARCHAR2(2 BYTE),
  IE_VALIDA_PREST_INFO_COMP  VARCHAR2(2 BYTE),
  IE_TIPO_PRESTADOR          VARCHAR2(5 BYTE),
  IE_TIPO_PRESTADOR_COMP     VARCHAR2(5 BYTE),
  IE_COMPARACAO              VARCHAR2(5 BYTE)   NOT NULL,
  IE_CAMPO                   VARCHAR2(5 BYTE)   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOCPD_PK ON TASY.PLS_OC_CTA_VAL_PREST_DIF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCPD_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_VAL_PREST_DIF
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_VAL_PREST_DIF_tp  after update ON TASY.PLS_OC_CTA_VAL_PREST_DIF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PREST_DIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDA_PREST_INFO,1,4000),substr(:new.IE_VALIDA_PREST_INFO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDA_PREST_INFO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PREST_DIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDA_PREST_INFO_COMP,1,4000),substr(:new.IE_VALIDA_PREST_INFO_COMP,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDA_PREST_INFO_COMP',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PREST_DIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CAMPO,1,4000),substr(:new.IE_CAMPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CAMPO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PREST_DIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRESTADOR_COMP,1,4000),substr(:new.IE_TIPO_PRESTADOR_COMP,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRESTADOR_COMP',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PREST_DIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMPARACAO,1,4000),substr(:new.IE_COMPARACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMPARACAO',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PREST_DIF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRESTADOR,1,4000),substr(:new.IE_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_OC_CTA_VAL_PREST_DIF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_VAL_PREST_DIF ADD (
  CONSTRAINT PLSOCPD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_VAL_PREST_DIF ADD (
  CONSTRAINT PLSOCPD_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_VAL_PREST_DIF TO NIVEL_1;

