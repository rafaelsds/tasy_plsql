ALTER TABLE TASY.TX_RESULT_ALELOS_HLA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_RESULT_ALELOS_HLA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_RESULT_ALELOS_HLA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ALENO             NUMBER(5)                NOT NULL,
  NR_SEQ_LOCUS_HLA     NUMBER(10)               NOT NULL,
  NR_SEQ_PAINEL        NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TXRESALE_PK ON TASY.TX_RESULT_ALELOS_HLA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRESALE_PK
  MONITORING USAGE;


CREATE INDEX TASY.TXRESALE_TXLOCUS_FK_I ON TASY.TX_RESULT_ALELOS_HLA
(NR_SEQ_LOCUS_HLA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRESALE_TXLOCUS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXRESALE_TXPAINE_FK_I ON TASY.TX_RESULT_ALELOS_HLA
(NR_SEQ_PAINEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRESALE_TXPAINE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.TX_RESULT_ALELOS_HLA ADD (
  CONSTRAINT TXRESALE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_RESULT_ALELOS_HLA ADD (
  CONSTRAINT TXRESALE_TXPAINE_FK 
 FOREIGN KEY (NR_SEQ_PAINEL) 
 REFERENCES TASY.TX_PAINEL (NR_SEQUENCIA),
  CONSTRAINT TXRESALE_TXLOCUS_FK 
 FOREIGN KEY (NR_SEQ_LOCUS_HLA) 
 REFERENCES TASY.TX_LOCUS_HLA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TX_RESULT_ALELOS_HLA TO NIVEL_1;

