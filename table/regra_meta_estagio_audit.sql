ALTER TABLE TASY.REGRA_META_ESTAGIO_AUDIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_META_ESTAGIO_AUDIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_META_ESTAGIO_AUDIT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESTAGIO       NUMBER(10)               NOT NULL,
  PR_INICIAL           NUMBER(15,2),
  PR_FINAL             NUMBER(15,2),
  IE_TIPO_META         VARCHAR2(3 BYTE)         NOT NULL,
  QT_HORA_INICIAL      NUMBER(10)               NOT NULL,
  QT_HORA_FINAL        NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REMEESA_CTAESPE_FK_I ON TASY.REGRA_META_ESTAGIO_AUDIT
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REMEESA_PK ON TASY.REGRA_META_ESTAGIO_AUDIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_META_ESTAGIO_AUDIT ADD (
  CONSTRAINT REMEESA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_META_ESTAGIO_AUDIT ADD (
  CONSTRAINT REMEESA_CTAESPE_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.CTA_ESTAGIO_PEND (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_META_ESTAGIO_AUDIT TO NIVEL_1;

