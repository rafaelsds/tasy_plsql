ALTER TABLE TASY.FIS_SEF_EDOC_0005
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_SEF_EDOC_0005 CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_SEF_EDOC_0005
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_REG               VARCHAR2(4 BYTE),
  DS_NOME_RESP         VARCHAR2(60 BYTE),
  CD_ASSIN             NUMBER(3),
  CD_CPF_RESP          NUMBER(11),
  CD_CEP               VARCHAR2(8 BYTE),
  DS_END               VARCHAR2(40 BYTE),
  NR_NUM               VARCHAR2(6 BYTE),
  DS_COMPL             VARCHAR2(50 BYTE),
  DS_BAIRRO            VARCHAR2(20 BYTE),
  CD_CEP_CP            NUMBER(8),
  CD_CP                NUMBER(5),
  NR_FONE              VARCHAR2(12 BYTE),
  NR_FAX               VARCHAR2(12 BYTE),
  DS_EMAIL             VARCHAR2(60 BYTE),
  NR_SEQ_CONTROLE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SEF0005_CONEDOC_FK_I ON TASY.FIS_SEF_EDOC_0005
(NR_SEQ_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SEF0005_PK ON TASY.FIS_SEF_EDOC_0005
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.FIS_SEF_EDOC_0005 ADD (
  CONSTRAINT SEF0005_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.FIS_SEF_EDOC_0005 ADD (
  CONSTRAINT SEF0005_CONEDOC_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE) 
 REFERENCES TASY.FIS_SEF_EDOC_CONTROLE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.FIS_SEF_EDOC_0005 TO NIVEL_1;

