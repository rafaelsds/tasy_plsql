ALTER TABLE TASY.PLS_EVENTO_DISCUSSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EVENTO_DISCUSSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EVENTO_DISCUSSAO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_EVENTO          NUMBER(10)             NOT NULL,
  IE_DESCONTA_PRESTADOR  VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_GLOSA    NUMBER(10),
  IE_TIPO_RELACAO_PGTO   VARCHAR2(2 BYTE),
  IE_TIPO_RELACAO_ATEND  VARCHAR2(2 BYTE),
  NR_SEQ_PRESTADOR_PGTO  NUMBER(10),
  IE_DESCONTO            VARCHAR2(5 BYTE)       NOT NULL,
  IE_NECESSITA_LIB       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSEDIS_PK ON TASY.PLS_EVENTO_DISCUSSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEDIS_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSEDIS_PLSEVEN_FK_I ON TASY.PLS_EVENTO_DISCUSSAO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEDIS_PLSEVEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEDIS_PLSMOGA_FK_I ON TASY.PLS_EVENTO_DISCUSSAO
(NR_SEQ_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEDIS_PLSMOGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSEDIS_PLSPRES_FK_I ON TASY.PLS_EVENTO_DISCUSSAO
(NR_SEQ_PRESTADOR_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSEDIS_PLSPRES_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_EVENTO_DISCUSSAO ADD (
  CONSTRAINT PLSEDIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_EVENTO_DISCUSSAO ADD (
  CONSTRAINT PLSEDIS_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSEDIS_PLSMOGA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA) 
 REFERENCES TASY.PLS_MOTIVO_GLOSA_ACEITA (NR_SEQUENCIA),
  CONSTRAINT PLSEDIS_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_PGTO) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_EVENTO_DISCUSSAO TO NIVEL_1;

