ALTER TABLE TASY.PLS_OC_ITEM_REF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_ITEM_REF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_ITEM_REF
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_REGRA             VARCHAR2(255 BYTE),
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MATERIAL      NUMBER(10),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOCREF_ESTABEL_FK_I ON TASY.PLS_OC_ITEM_REF
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSOCREF_PK ON TASY.PLS_OC_ITEM_REF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCREF_PLSMAT_FK_I ON TASY.PLS_OC_ITEM_REF
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCREF_PROCEDI_FK_I ON TASY.PLS_OC_ITEM_REF
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_ITEM_REF_tp  after update ON TASY.PLS_OC_ITEM_REF FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_REGRA,1,500);gravar_log_alteracao(substr(:old.NM_REGRA,1,4000),substr(:new.NM_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_REGRA',ie_log_w,ds_w,'PLS_OC_ITEM_REF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PLS_OC_ITEM_REF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_OC_ITEM_REF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MATERIAL,1,4000),substr(:new.NR_SEQ_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MATERIAL',ie_log_w,ds_w,'PLS_OC_ITEM_REF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PLS_OC_ITEM_REF',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_OC_ITEM_REF',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_ITEM_REF ADD (
  CONSTRAINT PLSOCREF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_ITEM_REF ADD (
  CONSTRAINT PLSOCREF_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSOCREF_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSOCREF_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PLS_OC_ITEM_REF TO NIVEL_1;

