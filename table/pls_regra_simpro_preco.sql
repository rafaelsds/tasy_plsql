ALTER TABLE TASY.PLS_REGRA_SIMPRO_PRECO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_SIMPRO_PRECO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_SIMPRO_PRECO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_FIM_VIGENCIA         DATE,
  IE_ULTIMO_VALOR_VALIDO  VARCHAR2(1 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRESP_ESTABEL_FK_I ON TASY.PLS_REGRA_SIMPRO_PRECO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRESP_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRESP_I1 ON TASY.PLS_REGRA_SIMPRO_PRECO
(DT_INICIO_VIGENCIA_REF, DT_FIM_VIGENCIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRESP_PK ON TASY.PLS_REGRA_SIMPRO_PRECO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRESP_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_regra_simpro_preco_atual
before insert or update ON TASY.PLS_REGRA_SIMPRO_PRECO for each row
declare

begin
-- esta trigger foi criada para alimentar os campos de data referencia, isto por quest�es de performance
-- para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
-- o campo inicial ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
-- com a data zero do oracle 31/12/1899, j� o campo fim ref � alimentado com o campo fim ou se o mesmo for nulo
-- � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
-- sem precisar se preocupar se o campo vai estar nulo

:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_inicio_vigencia, 'I');
:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null( :new.dt_fim_vigencia, 'F');

end pls_regra_simpro_preco_atual;
/


ALTER TABLE TASY.PLS_REGRA_SIMPRO_PRECO ADD (
  CONSTRAINT PLSRESP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_SIMPRO_PRECO ADD (
  CONSTRAINT PLSRESP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_REGRA_SIMPRO_PRECO TO NIVEL_1;

