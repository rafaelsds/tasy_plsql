ALTER TABLE TASY.AE_INTEGRATION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AE_INTEGRATION CASCADE CONSTRAINTS;

CREATE TABLE TASY.AE_INTEGRATION
(
  EVENT_ID               VARCHAR2(36 BYTE)      NOT NULL,
  TIMESTAMP              TIMESTAMP(6),
  USER_ID                VARCHAR2(100 BYTE),
  USER_FULLNAME          VARCHAR2(256 BYTE),
  HOST_ADDRESS           VARCHAR2(256 BYTE),
  INTEGRATION_ID         VARCHAR2(50 BYTE),
  INTEGRATION_NAME       VARCHAR2(100 BYTE),
  TYPE                   VARCHAR2(100 BYTE),
  CONTENT                CLOB,
  STATUS                 VARCHAR2(50 BYTE),
  GROUP_NAME             VARCHAR2(50 BYTE),
  PARENT_INTEGRATION_ID  VARCHAR2(50 BYTE),
  EXTRAS                 CLOB,
  ROOT_EVENT_ID          VARCHAR2(36 BYTE),
  PARENT_EVENT_ID        VARCHAR2(36 BYTE),
  TIMESTAMP_ZONE_ID      VARCHAR2(256 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (EXTRAS) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (CONTENT) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AEINTEG_PK ON TASY.AE_INTEGRATION
(EVENT_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AE_INTEGRATION ADD (
  CONSTRAINT AEINTEG_PK
 PRIMARY KEY
 (EVENT_ID)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AE_INTEGRATION TO NIVEL_1;

