ALTER TABLE TASY.MOVIMENTO_CONTABIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOVIMENTO_CONTABIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOVIMENTO_CONTABIL
(
  NR_LOTE_CONTABIL       NUMBER(10)             NOT NULL,
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_CONTA_CONTABIL      VARCHAR2(20 BYTE)      NOT NULL,
  IE_DEBITO_CREDITO      VARCHAR2(1 BYTE)       NOT NULL,
  CD_HISTORICO           NUMBER(10)             NOT NULL,
  DT_MOVIMENTO           DATE                   NOT NULL,
  VL_MOVIMENTO           NUMBER(15,2)           NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_CENTRO_CUSTO        NUMBER(8),
  DS_COMPL_HISTORICO     VARCHAR2(255 BYTE),
  NR_SEQ_TRANS_FIN       NUMBER(10),
  CD_CGC                 VARCHAR2(14 BYTE),
  NR_SEQ_AGRUPAMENTO     NUMBER(15),
  NR_DOCUMENTO           VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO     NUMBER(15),
  CD_CLASSIFICACAO       VARCHAR2(40 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  IE_TRANSITORIO         VARCHAR2(1 BYTE),
  DS_ORDEM               VARCHAR2(255 BYTE),
  IE_ORIGEM_DOCUMENTO    NUMBER(5),
  NR_LANCAMENTO          NUMBER(10),
  NR_SEQ_CTB_MOVTO       NUMBER(10),
  NR_SEQ_CLASSIF_MOVTO   NUMBER(10),
  IE_INTERCOMPANY        VARCHAR2(1 BYTE),
  CD_ESTAB_INTERCOMPANY  NUMBER(4),
  NR_CODIGO_CONTROLE     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          62M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOVCONT_CENCUST_FK_I ON TASY.MOVIMENTO_CONTABIL
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCONT_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVCONT_CONCONT_FK_I ON TASY.MOVIMENTO_CONTABIL
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          14M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCONT_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVCONT_CTBCLSMOV_FK_I ON TASY.MOVIMENTO_CONTABIL
(NR_SEQ_CLASSIF_MOVTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCONT_ESTABEL_FK_I ON TASY.MOVIMENTO_CONTABIL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCONT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVCONT_ESTABEL_FK2_I ON TASY.MOVIMENTO_CONTABIL
(CD_ESTAB_INTERCOMPANY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCONT_I1 ON TASY.MOVIMENTO_CONTABIL
(NR_SEQ_AGRUPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCONT_I2 ON TASY.MOVIMENTO_CONTABIL
(NR_SEQ_CTB_MOVTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCONT_LOTCONT_FK_I ON TASY.MOVIMENTO_CONTABIL
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          12M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MOVCONT_PK ON TASY.MOVIMENTO_CONTABIL
(NR_LOTE_CONTABIL, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          14M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVCONT_TRAFINA_FK_I ON TASY.MOVIMENTO_CONTABIL
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVCONT_TRAFINA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.MOVIMENTO_CONTABIL ADD (
  CONSTRAINT MOVCONT_PK
 PRIMARY KEY
 (NR_LOTE_CONTABIL, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          14M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOVIMENTO_CONTABIL ADD (
  CONSTRAINT MOVCONT_CTBCLSMOV_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_MOVTO) 
 REFERENCES TASY.CTB_CLASSIF_MOVIMENTO (NR_SEQUENCIA),
  CONSTRAINT MOVCONT_ESTABEL_FK2 
 FOREIGN KEY (CD_ESTAB_INTERCOMPANY) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MOVCONT_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT MOVCONT_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT MOVCONT_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT MOVCONT_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT MOVCONT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.MOVIMENTO_CONTABIL TO NIVEL_1;

