ALTER TABLE TASY.EMISSAO_SOLIC_ORDEM_AUTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EMISSAO_SOLIC_ORDEM_AUTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.EMISSAO_SOLIC_ORDEM_AUTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_GRUPO_MATERIAL     NUMBER(3),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  QT_INTERVALO          NUMBER(5)               NOT NULL,
  IE_SOLIC_NAO_AUTO     VARCHAR2(1 BYTE),
  IE_FUNCIONALIDADE     VARCHAR2(1 BYTE)        NOT NULL,
  IE_CENTRO_CUSTO       VARCHAR2(1 BYTE),
  QT_INTERVALO_DIAS     NUMBER(5),
  IE_GRUPO_MATERIAL     VARCHAR2(1 BYTE),
  IE_IMP_SOLIC_REP      VARCHAR2(1 BYTE),
  IE_IMP_ESTAB          VARCHAR2(1 BYTE),
  IE_APROVADA           VARCHAR2(1 BYTE),
  IE_CONSIGNADO         VARCHAR2(1 BYTE),
  NR_SEQ_SCHEDULER      NUMBER(10),
  DS_IMPRESSORA         VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EMSOLOA_GRUMATE_FK_I ON TASY.EMISSAO_SOLIC_ORDEM_AUTO
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EMSOLOA_PK ON TASY.EMISSAO_SOLIC_ORDEM_AUTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMSOLOA_SETATEN_FK_I ON TASY.EMISSAO_SOLIC_ORDEM_AUTO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMSOLOA_SUBMATE_FK_I ON TASY.EMISSAO_SOLIC_ORDEM_AUTO
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMSOLOA_USUSCHE_FK_I ON TASY.EMISSAO_SOLIC_ORDEM_AUTO
(NR_SEQ_SCHEDULER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.EMISSAO_SOLIC_ORDEM_AUTO ADD (
  CONSTRAINT EMSOLOA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EMISSAO_SOLIC_ORDEM_AUTO ADD (
  CONSTRAINT EMSOLOA_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT EMSOLOA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT EMSOLOA_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT EMSOLOA_USUSCHE_FK 
 FOREIGN KEY (NR_SEQ_SCHEDULER) 
 REFERENCES TASY.USUARIO_SCHEDULER (NR_SEQUENCIA));

GRANT SELECT ON TASY.EMISSAO_SOLIC_ORDEM_AUTO TO NIVEL_1;

