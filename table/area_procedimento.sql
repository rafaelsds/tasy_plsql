ALTER TABLE TASY.AREA_PROCEDIMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AREA_PROCEDIMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AREA_PROCEDIMENTO
(
  CD_AREA_PROCEDIMENTO  NUMBER(15)              NOT NULL,
  DS_AREA_PROCEDIMENTO  VARCHAR2(40 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_ORIGEM_PROCED      NUMBER(10)              NOT NULL,
  CD_ORIGINAL           NUMBER(15),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_SISTEMA_ANT        VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.AREA_PROCEDIMENTO.CD_AREA_PROCEDIMENTO IS 'Codigo da Area de Atendimento';

COMMENT ON COLUMN TASY.AREA_PROCEDIMENTO.DS_AREA_PROCEDIMENTO IS 'Descricao da Area de Atendimento';


CREATE UNIQUE INDEX TASY.AREPROC_PK ON TASY.AREA_PROCEDIMENTO
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AREA_PROCEDIMENTO ADD (
  CONSTRAINT AREPROC_PK
 PRIMARY KEY
 (CD_AREA_PROCEDIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AREA_PROCEDIMENTO TO NIVEL_1;

GRANT SELECT ON TASY.AREA_PROCEDIMENTO TO ROBOCMTECNOLOGIA;

