ALTER TABLE TASY.LAB_EQUIP_MAQUINA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_EQUIP_MAQUINA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_EQUIP_MAQUINA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_MAQUINA           VARCHAR2(255 BYTE)       NOT NULL,
  SG_MAQUINA           VARCHAR2(15 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.LEQMAQ_PK ON TASY.LAB_EQUIP_MAQUINA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEQMAQ_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.lab_equip_maquina_delete
before delete ON TASY.LAB_EQUIP_MAQUINA for each row
declare

ds_envio_w	varchar2(2000);

begin

if	(wheb_usuario_pck.is_evento_ativo(296) = 'S') then
	ds_envio_w	:= 	' Exclus�o de registro - LAB - V�nculo de m�quinas com equipamentos '||chr(10)||chr(13)||
					' Sequ�ncia:'||:old.nr_sequencia||chr(10)||chr(13)||
					' M�quina:'||:old.ds_maquina||chr(10)||chr(13)||
					' Sigla:'||:old.SG_MAQUINA||chr(10)||chr(13)||
					' Ativo:'||:old.ie_situacao;
	integrar_tasylab(
				null,
				null,
				308,
				null,
				null,
				null,
				null,
				null,
				null,
				:new.nr_sequencia,
				1, --LAB_EQUIP_MAQUINA
				'N',
				ds_envio_w);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.lab_equip_maquina_ins_upd
before insert or update ON TASY.LAB_EQUIP_MAQUINA for each row
declare

begin

if	(nvl(:old.dt_atualizacao, to_date('01/01/1988')) <> :new.dt_atualizacao) and
	(wheb_usuario_pck.is_evento_ativo(296) = 'S') then
	integrar_tasylab(
				null,
				null,
				296,
				null,
				null,
				null,
				null,
				null,
				null,
				:new.nr_sequencia,
				1, --LAB_EQUIP_MAQUINA
				'N');
end if;

end;
/


ALTER TABLE TASY.LAB_EQUIP_MAQUINA ADD (
  CONSTRAINT LEQMAQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.LAB_EQUIP_MAQUINA TO NIVEL_1;

