ALTER TABLE TASY.CPOE_PROC_ANESTESISTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_PROC_ANESTESISTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_PROC_ANESTESISTA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROC_EDIT     NUMBER(10),
  NR_SEQ_PROC_CPOE     NUMBER(10),
  CD_MEDICO            VARCHAR2(10 BYTE),
  QT_HEMORRAGIA        NUMBER(18,6),
  QT_PLAQUETAS         NUMBER(18,6),
  QT_SANAUTOL          NUMBER(18,6),
  QT_CELVERMELHA       NUMBER(18,6),
  QT_PLASMA            NUMBER(18,6),
  IE_TOSSE             VARCHAR2(1 BYTE),
  IE_ESCARRO           VARCHAR2(1 BYTE),
  IE_PSTATUS           VARCHAR2(1 BYTE),
  NR_SEQ_DVT_RULE      NUMBER(10),
  DS_DADO_CLINICO      VARCHAR2(2000 BYTE),
  NR_SEQ_TEC_ANEST     NUMBER(10),
  RP_COMMENTS          VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPPRANES_CPOEDVTRUL_FK_I ON TASY.CPOE_PROC_ANESTESISTA
(NR_SEQ_DVT_RULE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPPRANES_CPOEPRO_FK_I ON TASY.CPOE_PROC_ANESTESISTA
(NR_SEQ_PROC_CPOE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPPRANES_MEDICO_FK_I ON TASY.CPOE_PROC_ANESTESISTA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPPRANES_PK ON TASY.CPOE_PROC_ANESTESISTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_PROC_ANESTESISTA ADD (
  CONSTRAINT CPPRANES_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_PROC_ANESTESISTA ADD (
  CONSTRAINT CPPRANES_CPOEDVTRUL_FK 
 FOREIGN KEY (NR_SEQ_DVT_RULE) 
 REFERENCES TASY.CPOE_DVT_RULE (NR_SEQUENCIA),
  CONSTRAINT CPPRANES_CPOEPRO_FK 
 FOREIGN KEY (NR_SEQ_PROC_CPOE) 
 REFERENCES TASY.CPOE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT CPPRANES_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CPOE_PROC_ANESTESISTA TO NIVEL_1;

