ALTER TABLE TASY.SUS_PARAMETROS_AIH
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_PARAMETROS_AIH CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_PARAMETROS_AIH
(
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  CD_ORGAO_EMISSOR_AIH          VARCHAR2(10 BYTE) NOT NULL,
  CD_CNES_HOSPITAL              NUMBER(7)       NOT NULL,
  CD_MUNICIPIO_IBGE             VARCHAR2(6 BYTE) NOT NULL,
  CD_DIRETOR_CLINICO            VARCHAR2(10 BYTE) NOT NULL,
  CD_MEDICO_AUTORIZADOR         VARCHAR2(10 BYTE),
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  CD_INTERFACE_ENVIO            NUMBER(5),
  PR_URG_EMERG                  NUMBER(5,2),
  PR_IVH                        NUMBER(5,2),
  IE_GERA_LONGA_PERM            VARCHAR2(1 BYTE) NOT NULL,
  PR_CESARIANA_PERMITIDA        NUMBER(7,4),
  IE_EXPORTA_RESP               VARCHAR2(1 BYTE),
  IE_DIARIA_UTI                 VARCHAR2(4 BYTE),
  IE_FORMA_CALCULO_SADT         VARCHAR2(1 BYTE) NOT NULL,
  IE_GERAR_PARTIC_CIRURG        VARCHAR2(1 BYTE) NOT NULL,
  QT_MAX_DIARIA_UTI             NUMBER(10),
  IE_REPASSE_PROC               VARCHAR2(1 BYTE),
  IE_ARRED_SP_SISAIH            VARCHAR2(1 BYTE),
  IE_IGNORA_PARTICIPOU_SUS      VARCHAR2(1 BYTE),
  IE_EXPORTA_CNES               VARCHAR2(1 BYTE),
  IE_EXPORTA_CNES_HOSP          VARCHAR2(1 BYTE),
  IE_EXP_CNPJ_FORNEC_FABRIC     VARCHAR2(1 BYTE),
  IE_INC_PROC_URG_AIH           VARCHAR2(1 BYTE),
  IE_INC_PROC_CONTA             VARCHAR2(1 BYTE),
  IE_ORDENA_PROC_VALOR          VARCHAR2(15 BYTE),
  NR_NAC_SERVENTIA              NUMBER(10),
  CD_ACERVO                     VARCHAR2(15 BYTE),
  NR_TIPO_LIVRO                 NUMBER(5),
  VL_INCREMENTO_PNASH           NUMBER(7,2),
  IE_EXPORTA_CNES_SETOR         VARCHAR2(15 BYTE),
  IE_AJUSTA_ATEND_DEST          VARCHAR2(15 BYTE),
  IE_FORMA_ENVIO_DATA_PROC      VARCHAR2(15 BYTE),
  IE_VINCULAR_LAUDO_TIPO_ATEND  VARCHAR2(15 BYTE),
  IE_ALTERAR_SP_PROC_RIM        VARCHAR2(15 BYTE),
  CD_DIRETOR_TECNICO            VARCHAR2(10 BYTE),
  IE_INCREMENTO_ANESTESISTA     VARCHAR2(15 BYTE),
  DS_CARATER_INCREMENTO         VARCHAR2(40 BYTE),
  CD_PRESTADOR_LAUDO_ACH        VARCHAR2(20 BYTE),
  IE_MOMENTO_RATEIO_SH          VARCHAR2(15 BYTE),
  IE_VINCULAR_LAUDO_ATEND_EXT   VARCHAR2(15 BYTE),
  IE_ORDEM_TELEFONE_PAC         VARCHAR2(15 BYTE),
  QT_MAX_DIARIA_ENFERM          NUMBER(10),
  IE_GERA_AIH_LAUDO_TRANF       VARCHAR2(15 BYTE),
  IE_INCEMENTO_SEQ_ONCO         VARCHAR2(15 BYTE),
  IE_VINCULAR_LAUDO_ATEND_PS    VARCHAR2(15 BYTE),
  CD_MEDICO_RESPONSAVEL         VARCHAR2(20 BYTE),
  IE_PROC_PRINC_RECEB_SH        VARCHAR2(1 BYTE),
  IE_TRANSF_LAUDO_SEM_LIB       VARCHAR2(1 BYTE),
  IE_CONSIST_PROC_ONCO_PROT     VARCHAR2(1 BYTE),
  IE_RESTRINGE_CID_PROC         VARCHAR2(1 BYTE),
  CD_PROJ_XML_SOLIC_AIH         NUMBER(10),
  CD_PROJ_XML_MUDANCA_PROC      NUMBER(10),
  IE_TRANSF_DIAG_INTERNA_BPA    VARCHAR2(1 BYTE),
  IE_CLINICA_LAUDO_INT_BPA      VARCHAR2(1 BYTE),
  IE_MANTER_DOC_EXECUTOR        VARCHAR2(15 BYTE),
  IE_UTILIZA_REGRA_ORDEM_SEQ    VARCHAR2(15 BYTE),
  IE_EXP_CNES_EXC_MED           VARCHAR2(15 BYTE),
  IE_ZERA_CONTA_SAUDE_MENT      VARCHAR2(15 BYTE),
  IE_REGRA_ARRED_INCREM         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUSPAIH_INTERFA_FK_I ON TASY.SUS_PARAMETROS_AIH
(CD_INTERFACE_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSPAIH_INTERFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSPAIH_PESFISI_FK_I ON TASY.SUS_PARAMETROS_AIH
(CD_DIRETOR_CLINICO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPAIH_PESFISI_FK2_I ON TASY.SUS_PARAMETROS_AIH
(CD_MEDICO_AUTORIZADOR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPAIH_PESFISI_FK3_I ON TASY.SUS_PARAMETROS_AIH
(CD_DIRETOR_TECNICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPAIH_PESFISI_FK4_I ON TASY.SUS_PARAMETROS_AIH
(CD_MEDICO_RESPONSAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SUSPAIH_PK ON TASY.SUS_PARAMETROS_AIH
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSPAIH_SUSMUNI_FK_I ON TASY.SUS_PARAMETROS_AIH
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSPAIH_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sus_parametros_aih_tp
after update ON TASY.SUS_PARAMETROS_AIH for each row
declare

nr_seq_w number(10);
ds_s_w   varchar2(50);
ds_c_w   varchar2(500);
ds_w    varchar2(500);
ie_log_w varchar2(1);

begin

begin

ds_s_w 	:= to_char(:old.CD_ESTABELECIMENTO);
ds_c_w	:= null;

sus_parametros_aih_pck.set_dt_atualizacao(:new.dt_atualizacao);

gravar_log_alteracao(substr(:old.IE_DIARIA_UTI,1,2000),substr(:new.IE_DIARIA_UTI,1,2000),:new.nm_usuario,nr_seq_w,'IE_DIARIA_UTI',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_FORMA_CALCULO_SADT,1,2000),substr(:new.IE_FORMA_CALCULO_SADT,1,2000),:new.nm_usuario,nr_seq_w,'IE_FORMA_CALCULO_SADT',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_REPASSE_PROC,1,2000),substr(:new.IE_REPASSE_PROC,1,2000),:new.nm_usuario,nr_seq_w,'IE_REPASSE_PROC',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.QT_MAX_DIARIA_UTI,1,2000),substr(:new.QT_MAX_DIARIA_UTI,1,2000),:new.nm_usuario,nr_seq_w,'QT_MAX_DIARIA_UTI',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_IGNORA_PARTICIPOU_SUS,1,2000),substr(:new.IE_IGNORA_PARTICIPOU_SUS,1,2000),:new.nm_usuario,nr_seq_w,'IE_IGNORA_PARTICIPOU_SUS',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_ARRED_SP_SISAIH,1,2000),substr(:new.IE_ARRED_SP_SISAIH,1,2000),:new.nm_usuario,nr_seq_w,'IE_ARRED_SP_SISAIH',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,2000),substr(:new.CD_ESTABELECIMENTO,1,2000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_ORGAO_EMISSOR_AIH,1,2000),substr(:new.CD_ORGAO_EMISSOR_AIH,1,2000),:new.nm_usuario,nr_seq_w,'CD_ORGAO_EMISSOR_AIH',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_CNES_HOSPITAL,1,2000),substr(:new.CD_CNES_HOSPITAL,1,2000),:new.nm_usuario,nr_seq_w,'CD_CNES_HOSPITAL',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,2000),substr(:new.CD_MUNICIPIO_IBGE,1,2000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_DIRETOR_CLINICO,1,2000),substr(:new.CD_DIRETOR_CLINICO,1,2000),:new.nm_usuario,nr_seq_w,'CD_DIRETOR_CLINICO',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_MEDICO_AUTORIZADOR,1,2000),substr(:new.CD_MEDICO_AUTORIZADOR,1,2000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_AUTORIZADOR',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_INTERFACE_ENVIO,1,2000),substr(:new.CD_INTERFACE_ENVIO,1,2000),:new.nm_usuario,nr_seq_w,'CD_INTERFACE_ENVIO',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.PR_URG_EMERG,1,2000),substr(:new.PR_URG_EMERG,1,2000),:new.nm_usuario,nr_seq_w,'PR_URG_EMERG',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.PR_IVH,1,2000),substr(:new.PR_IVH,1,2000),:new.nm_usuario,nr_seq_w,'PR_IVH',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_GERA_LONGA_PERM,1,2000),substr(:new.IE_GERA_LONGA_PERM,1,2000),:new.nm_usuario,nr_seq_w,'IE_GERA_LONGA_PERM',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_GERAR_PARTIC_CIRURG,1,2000),substr(:new.IE_GERAR_PARTIC_CIRURG,1,2000),:new.nm_usuario,nr_seq_w,'IE_GERAR_PARTIC_CIRURG',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_EXPORTA_RESP,1,2000),substr(:new.IE_EXPORTA_RESP,1,2000),:new.nm_usuario,nr_seq_w,'IE_EXPORTA_RESP',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.PR_CESARIANA_PERMITIDA,1,2000),substr(:new.PR_CESARIANA_PERMITIDA,1,2000),:new.nm_usuario,nr_seq_w,'PR_CESARIANA_PERMITIDA',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_EXPORTA_CNES,1,2000),substr(:new.IE_EXPORTA_CNES,1,2000),:new.nm_usuario,nr_seq_w,'IE_EXPORTA_CNES',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_EXP_CNPJ_FORNEC_FABRIC,1,2000),substr(:new.IE_EXP_CNPJ_FORNEC_FABRIC,1,2000),:new.nm_usuario,nr_seq_w,'IE_EXP_CNPJ_FORNEC_FABRIC',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_EXPORTA_CNES_HOSP,1,2000),substr(:new.IE_EXPORTA_CNES_HOSP,1,2000),:new.nm_usuario,nr_seq_w,'IE_EXPORTA_CNES_HOSP',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_INC_PROC_URG_AIH,1,2000),substr(:new.IE_INC_PROC_URG_AIH,1,2000),:new.nm_usuario,nr_seq_w,'IE_INC_PROC_URG_AIH',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_INC_PROC_CONTA,1,2000),substr(:new.IE_INC_PROC_CONTA,1,2000),:new.nm_usuario,nr_seq_w,'IE_INC_PROC_CONTA',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_ORDENA_PROC_VALOR,1,2000),substr(:new.IE_ORDENA_PROC_VALOR,1,2000),:new.nm_usuario,nr_seq_w,'IE_ORDENA_PROC_VALOR',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_INCREMENTO_ANESTESISTA,1,2000),substr(:new.IE_INCREMENTO_ANESTESISTA,1,2000),:new.nm_usuario,nr_seq_w,'IE_INCREMENTO_ANESTESISTA',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_ACERVO,1,2000),substr(:new.CD_ACERVO,1,2000),:new.nm_usuario,nr_seq_w,'CD_ACERVO',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_DIRETOR_TECNICO,1,2000),substr(:new.CD_DIRETOR_TECNICO,1,2000),:new.nm_usuario,nr_seq_w,'CD_DIRETOR_TECNICO',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_PRESTADOR_LAUDO_ACH,1,2000),substr(:new.CD_PRESTADOR_LAUDO_ACH,1,2000),:new.nm_usuario,nr_seq_w,'CD_PRESTADOR_LAUDO_ACH',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.DS_CARATER_INCREMENTO,1,2000),substr(:new.DS_CARATER_INCREMENTO,1,2000),:new.nm_usuario,nr_seq_w,'DS_CARATER_INCREMENTO',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_AJUSTA_ATEND_DEST,1,2000),substr(:new.IE_AJUSTA_ATEND_DEST,1,2000),:new.nm_usuario,nr_seq_w,'IE_AJUSTA_ATEND_DEST',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_ALTERAR_SP_PROC_RIM,1,2000),substr(:new.IE_ALTERAR_SP_PROC_RIM,1,2000),:new.nm_usuario,nr_seq_w,'IE_ALTERAR_SP_PROC_RIM',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_EXPORTA_CNES_SETOR,1,2000),substr(:new.IE_EXPORTA_CNES_SETOR,1,2000),:new.nm_usuario,nr_seq_w,'IE_EXPORTA_CNES_SETOR',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_FORMA_ENVIO_DATA_PROC,1,2000),substr(:new.IE_FORMA_ENVIO_DATA_PROC,1,2000),:new.nm_usuario,nr_seq_w,'IE_FORMA_ENVIO_DATA_PROC',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_MOMENTO_RATEIO_SH,1,2000),substr(:new.IE_MOMENTO_RATEIO_SH,1,2000),:new.nm_usuario,nr_seq_w,'IE_MOMENTO_RATEIO_SH',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_ORDEM_TELEFONE_PAC,1,2000),substr(:new.IE_ORDEM_TELEFONE_PAC,1,2000),:new.nm_usuario,nr_seq_w,'IE_ORDEM_TELEFONE_PAC',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_VINCULAR_LAUDO_ATEND_EXT,1,2000),substr(:new.IE_VINCULAR_LAUDO_ATEND_EXT,1,2000),:new.nm_usuario,nr_seq_w,'IE_VINCULAR_LAUDO_ATEND_EXT',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_VINCULAR_LAUDO_TIPO_ATEND,1,2000),substr(:new.IE_VINCULAR_LAUDO_TIPO_ATEND,1,2000),:new.nm_usuario,nr_seq_w,'IE_VINCULAR_LAUDO_TIPO_ATEND',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.NR_NAC_SERVENTIA,1,2000),substr(:new.NR_NAC_SERVENTIA,1,2000),:new.nm_usuario,nr_seq_w,'NR_NAC_SERVENTIA',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.NR_TIPO_LIVRO,1,2000),substr(:new.NR_TIPO_LIVRO,1,2000),:new.nm_usuario,nr_seq_w,'NR_TIPO_LIVRO',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.VL_INCREMENTO_PNASH,1,2000),substr(:new.VL_INCREMENTO_PNASH,1,2000),:new.nm_usuario,nr_seq_w,'VL_INCREMENTO_PNASH',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.QT_MAX_DIARIA_ENFERM,1,2000),substr(:new.QT_MAX_DIARIA_ENFERM,1,2000),:new.nm_usuario,nr_seq_w,'QT_MAX_DIARIA_ENFERM',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_MEDICO_RESPONSAVEL,1,2000),substr(:new.CD_MEDICO_RESPONSAVEL,1,2000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_RESPONSAVEL',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_PROJ_XML_MUDANCA_PROC,1,2000),substr(:new.CD_PROJ_XML_MUDANCA_PROC,1,2000),:new.nm_usuario,nr_seq_w,'CD_PROJ_XML_MUDANCA_PROC',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.CD_PROJ_XML_SOLIC_AIH,1,2000),substr(:new.CD_PROJ_XML_SOLIC_AIH,1,2000),:new.nm_usuario,nr_seq_w,'CD_PROJ_XML_SOLIC_AIH',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_CLINICA_LAUDO_INT_BPA,1,2000),substr(:new.IE_CLINICA_LAUDO_INT_BPA,1,2000),:new.nm_usuario,nr_seq_w,'IE_CLINICA_LAUDO_INT_BPA',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_CONSIST_PROC_ONCO_PROT,1,2000),substr(:new.IE_CONSIST_PROC_ONCO_PROT,1,2000),:new.nm_usuario,nr_seq_w,'IE_CONSIST_PROC_ONCO_PROT',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_EXP_CNES_EXC_MED,1,2000),substr(:new.IE_EXP_CNES_EXC_MED,1,2000),:new.nm_usuario,nr_seq_w,'IE_EXP_CNES_EXC_MED',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_GERA_AIH_LAUDO_TRANF,1,2000),substr(:new.IE_GERA_AIH_LAUDO_TRANF,1,2000),:new.nm_usuario,nr_seq_w,'IE_GERA_AIH_LAUDO_TRANF',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_INCEMENTO_SEQ_ONCO,1,2000),substr(:new.IE_INCEMENTO_SEQ_ONCO,1,2000),:new.nm_usuario,nr_seq_w,'IE_INCEMENTO_SEQ_ONCO',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_MANTER_DOC_EXECUTOR,1,2000),substr(:new.IE_MANTER_DOC_EXECUTOR,1,2000),:new.nm_usuario,nr_seq_w,'IE_MANTER_DOC_EXECUTOR',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_PROC_PRINC_RECEB_SH,1,2000),substr(:new.IE_PROC_PRINC_RECEB_SH,1,2000),:new.nm_usuario,nr_seq_w,'IE_PROC_PRINC_RECEB_SH',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_RESTRINGE_CID_PROC,1,2000),substr(:new.IE_RESTRINGE_CID_PROC,1,2000),:new.nm_usuario,nr_seq_w,'IE_RESTRINGE_CID_PROC',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_TRANSF_DIAG_INTERNA_BPA,1,2000),substr(:new.IE_TRANSF_DIAG_INTERNA_BPA,1,2000),:new.nm_usuario,nr_seq_w,'IE_TRANSF_DIAG_INTERNA_BPA',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_UTILIZA_REGRA_ORDEM_SEQ,1,2000),substr(:new.IE_UTILIZA_REGRA_ORDEM_SEQ,1,2000),:new.nm_usuario,nr_seq_w,'IE_UTILIZA_REGRA_ORDEM_SEQ',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
gravar_log_alteracao(substr(:old.IE_VINCULAR_LAUDO_ATEND_PS,1,2000),substr(:new.IE_VINCULAR_LAUDO_ATEND_PS,1,2000),:new.nm_usuario,nr_seq_w,'IE_VINCULAR_LAUDO_ATEND_PS',ie_log_w,ds_w,'SUS_PARAMETROS_AIH',ds_s_w,ds_c_w);
exception
	when others then
	ds_w:= '1';
	end;

end;
/


ALTER TABLE TASY.SUS_PARAMETROS_AIH ADD (
  CONSTRAINT SUSPAIH_PK
 PRIMARY KEY
 (CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_PARAMETROS_AIH ADD (
  CONSTRAINT SUSPAIH_PESFISI_FK4 
 FOREIGN KEY (CD_MEDICO_RESPONSAVEL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SUSPAIH_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SUSPAIH_PESFISI_FK 
 FOREIGN KEY (CD_DIRETOR_CLINICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SUSPAIH_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_AUTORIZADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SUSPAIH_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT SUSPAIH_INTERFA_FK 
 FOREIGN KEY (CD_INTERFACE_ENVIO) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT SUSPAIH_PESFISI_FK3 
 FOREIGN KEY (CD_DIRETOR_TECNICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.SUS_PARAMETROS_AIH TO NIVEL_1;

