ALTER TABLE TASY.OFT_FORMULARIO_RELATORIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OFT_FORMULARIO_RELATORIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.OFT_FORMULARIO_RELATORIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_FORM    NUMBER(10),
  CD_RELATORIO         NUMBER(10),
  IE_IMPRIMIR          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.OFFOREL_OFFOREG_FK_I ON TASY.OFT_FORMULARIO_RELATORIO
(NR_SEQ_REGRA_FORM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.OFFOREL_PK ON TASY.OFT_FORMULARIO_RELATORIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.OFT_FORMULARIO_RELATORIO ADD (
  CONSTRAINT OFFOREL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OFT_FORMULARIO_RELATORIO ADD (
  CONSTRAINT OFFOREL_OFFOREG_FK 
 FOREIGN KEY (NR_SEQ_REGRA_FORM) 
 REFERENCES TASY.OFT_FORMULARIO_REGRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.OFT_FORMULARIO_RELATORIO TO NIVEL_1;

