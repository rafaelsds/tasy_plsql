ALTER TABLE TASY.CTB_GRUPO_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_GRUPO_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_GRUPO_CONTA
(
  CD_GRUPO             NUMBER(10)               NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  DS_GRUPO             VARCHAR2(60 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_MASCARA           VARCHAR2(40 BYTE)        NOT NULL,
  IE_DEBITO_CREDITO    VARCHAR2(1 BYTE)         NOT NULL,
  IE_TIPO              VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_EXP_GRUPO         NUMBER(10),
  CD_GRUPO_REF         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBGRCO_CTBGRCO_FK_I ON TASY.CTB_GRUPO_CONTA
(CD_GRUPO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBGRCO_DICEXPR_FK_I ON TASY.CTB_GRUPO_CONTA
(CD_EXP_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBGRCO_EMPRESA_FK_I ON TASY.CTB_GRUPO_CONTA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBGRCO_PK ON TASY.CTB_GRUPO_CONTA
(CD_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ctb_grupo_conta_Update
BEFORE UPDATE ON TASY.CTB_GRUPO_CONTA for each row
declare
ie_sistema_ctb_w		Varchar2(01);
cd_conta_contabil_w	Varchar2(40);
ds_conta_contabil_w	Varchar2(255);
cd_classificacao_w		Varchar2(40);
cd_classif_w		Varchar2(40);
ds_mascara_w		Varchar2(40);
vl_nivel_w		Varchar2(15);
vl_nivel_ww		Varchar2(15);

type campos is record (
	qt_digito_old	Number(5),
	qt_digito_new	Number(05));
type Vetor is table of campos index by binary_integer;
Mascara_w		Vetor;
i			Integer;
k			Integer;
z			Integer;

cursor C01 is
	select	cd_conta_contabil,
		cd_classificacao,
		ds_conta_contabil
	from	conta_contabil
	where	cd_grupo	= :new.cd_grupo
	  and	length(cd_classificacao) >= k
	  and	length(cd_classificacao) <= k + Mascara_w(i).qt_digito_old
	order by cd_classificacao;

BEGIN
select	ie_sistema_ctb
into	ie_sistema_ctb_w
from	empresa
where	cd_empresa	= :new.cd_empresa;
if	(:new.cd_mascara <> :old.cd_mascara) and
	(ie_sistema_ctb_w = 'S') then
	begin
	ds_mascara_w	:= :old.cd_mascara;
	i	:= 0;
	while	ds_mascara_w is not null LOOP
		begin
		select instr(ds_mascara_w, '.') into k from dual;
		if	(k > 0) then
			i := I + 1;
			Mascara_w(i).qt_digito_old := k - 1;
			ds_mascara_w	:= substr(ds_mascara_w, k + 1, 20);
		else
			i := I + 1;
			Mascara_w(i).qt_digito_old := length(ds_mascara_w);
			ds_mascara_w	:= null;
		end if;
		end;
	END LOOP;
	ds_mascara_w	:= :new.cd_mascara;
	i	:= 0;
	while	ds_mascara_w is not null LOOP
		begin
		select instr(ds_mascara_w, '.') into k from dual;
		if	(k > 0) then
			i := I + 1;
			Mascara_w(i).qt_digito_new := k - 1;
			ds_mascara_w	:= substr(ds_mascara_w, k + 1, 20);
		else
			i := I + 1;
			Mascara_w(i).qt_digito_new := length(ds_mascara_w);
			ds_mascara_w	:= null;
		end if;
		end;
	END LOOP;
	K	:= 0;
	FOR i in 1..Mascara_w.count LOOP
		begin
		k	:= k + 1;
		if	(mascara_w(i).qt_digito_new < mascara_w(i).qt_digito_old) then
			begin
			vl_nivel_ww	:= '';
			FOR y in 1 .. mascara_w(i).qt_digito_new LOOP
				vl_nivel_ww := vl_nivel_ww || '9';
			END LOOP;
/* Consistir se existe conta com valor no nivel maior que os digitos do nivel (1.2.12/1.2.3) */
			OPEN  C01;
			LOOP
			FETCH C01 into	cd_conta_contabil_w,
					cd_classificacao_w,
					ds_conta_contabil_w;
			EXIT when C01%notfound;
				vl_nivel_w	:= substr(cd_classificacao_w, k,
						mascara_w(i).qt_digito_old);
				if	(vl_nivel_w > vl_nivel_ww) then
					wheb_mensagem_pck.exibir_mensagem_abort(266453,
						'CD_CONTA_CONTABIL_W=' || cd_conta_contabil_w	|| ';' ||
						'DS_CONTA_CONTABIL_W=' || ds_conta_contabil_w 	|| ';' ||
						'CD_CLASSIFICACAO_W='  || cd_classificacao_w);
				end if;
			END LOOP;
			CLOSE C01;
			OPEN  C01;
			LOOP
			FETCH C01 into	cd_conta_contabil_w,
					cd_classificacao_w,
					ds_conta_contabil_w;
			EXIT when C01%notfound;
				z	:= k + 1 + mascara_w(i).qt_digito_old - mascara_w(i).qt_digito_new;
				cd_classif_w	:= substr(cd_classificacao_w,1,k) ||
						substr(cd_classificacao_w, z, 20);
				update	conta_contabil
				set 	cd_classificacao	= cd_classif_w
				where	cd_conta_contabil	= cd_conta_contabil_w;
			END LOOP;
			CLOSE C01;
			end;
		end if;
		if	(mascara_w(i).qt_digito_new > mascara_w(i).qt_digito_old) then
			begin
			vl_nivel_ww	:= '';
			FOR y in mascara_w(i).qt_digito_old + 1 .. mascara_w(i).qt_digito_new LOOP
				vl_nivel_ww := vl_nivel_ww || '0';
			END LOOP;
			OPEN  C01;
			LOOP
			FETCH C01 into	cd_conta_contabil_w,
					cd_classificacao_w,
					ds_conta_contabil_w;
			EXIT when C01%notfound;
				cd_classif_w	:= substr(cd_classificacao_w,1,k) || vl_nivel_ww ||
					substr(cd_classificacao_w, k + 1, mascara_w(i).qt_digito_old);
				update	conta_contabil
				set 	cd_classificacao	= cd_classif_w
				where	cd_conta_contabil	= cd_conta_contabil_w;
			END LOOP;
			CLOSE C01;
			end;
		end if;
		k	:= k + mascara_w(i).qt_digito_new;
		end;
	END LOOP;

	end;
end if;

END;
/


ALTER TABLE TASY.CTB_GRUPO_CONTA ADD (
  CONSTRAINT CTBGRCO_PK
 PRIMARY KEY
 (CD_GRUPO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_GRUPO_CONTA ADD (
  CONSTRAINT CTBGRCO_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_GRUPO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT CTBGRCO_CTBGRCO_FK 
 FOREIGN KEY (CD_GRUPO_REF) 
 REFERENCES TASY.CTB_GRUPO_CONTA (CD_GRUPO),
  CONSTRAINT CTBGRCO_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CTB_GRUPO_CONTA TO NIVEL_1;

