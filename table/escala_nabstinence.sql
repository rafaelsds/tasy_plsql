ALTER TABLE TASY.ESCALA_NABSTINENCE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_NABSTINENCE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_NABSTINENCE
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NR_ATENDIMENTO                NUMBER(10),
  CD_PROFISSIONAL               VARCHAR2(10 BYTE) NOT NULL,
  DT_AVALIACAO                  DATE            NOT NULL,
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DT_LIBERACAO                  DATE,
  DT_INATIVACAO                 DATE,
  NM_USUARIO_INATIVACAO         VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA              VARCHAR2(255 BYTE),
  QT_SCORE                      NUMBER(3),
  IE_MANCHAS                    VARCHAR2(1 BYTE),
  IE_ENTUPIM_NASAL              VARCHAR2(1 BYTE),
  IE_ESPIRROS                   VARCHAR2(1 BYTE),
  IE_DILATACAO_NARINAS          VARCHAR2(1 BYTE),
  IE_FREQ_RESPIRATORIA          NUMBER(2)       NOT NULL,
  IE_SUCCAO_EXCESSIVA           VARCHAR2(1 BYTE),
  IE_MA_ALIMENTACAO             VARCHAR2(1 BYTE),
  IE_REGURGITACAO               VARCHAR2(1 BYTE),
  IE_VOMITO                     VARCHAR2(1 BYTE),
  IE_FEZES                      NUMBER(2)       NOT NULL,
  IE_BOCEJO_FREQUENTE           VARCHAR2(1 BYTE),
  IE_FEBRE                      NUMBER(2)       NOT NULL,
  IE_SUOR                       VARCHAR2(1 BYTE),
  IE_ESCORIACOES                VARCHAR2(1 BYTE),
  IE_CONVULSOES                 VARCHAR2(1 BYTE),
  IE_ESPASMOS_MIOCLONICOS       VARCHAR2(1 BYTE),
  IE_TONUS_MUSCULAR             VARCHAR2(1 BYTE),
  IE_TREMORES                   NUMBER(2)       NOT NULL,
  IE_NIVEL_ATENCAO              VARCHAR2(1 BYTE),
  IE_SONO                       NUMBER(2)       NOT NULL,
  IE_CHORO                      NUMBER(2)       NOT NULL,
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  NR_SEQ_ASSINATURA_INATIVACAO  NUMBER(10),
  NR_SEQ_ASSINATURA             NUMBER(10),
  IE_REFLEXO_MORO               NUMBER(2)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCNABS_ATEPACI_FK_I ON TASY.ESCALA_NABSTINENCE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNABS_PESFISI_FK_I ON TASY.ESCALA_NABSTINENCE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNABS_PESFISI_FK2_I ON TASY.ESCALA_NABSTINENCE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCNABS_PK ON TASY.ESCALA_NABSTINENCE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNABS_TASASDI_FK_I ON TASY.ESCALA_NABSTINENCE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCNABS_TASASDI_FK2_I ON TASY.ESCALA_NABSTINENCE
(NR_SEQ_ASSINATURA_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_nabstinence_delete
after delete ON TASY.ESCALA_NABSTINENCE for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '252'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.escala_nabstinence_pend_atual
after insert or update ON TASY.ESCALA_NABSTINENCE for each row
declare

pragma autonomous_transaction;

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'252');
end if;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.escala_nabstinence_atual
before insert or update ON TASY.ESCALA_NABSTINENCE for each row
declare
ie_tonus_muscular number := 0;
ie_espasmos_mioclonicos number := 0;
ie_convulsoes number := 0;
ie_escoriacoes number := 0;
ie_suor number := 0;
ie_bocejo_frequente number := 0;
ie_manchas number := 0;
ie_entupim_nasal number := 0;
ie_espirros number := 0;
ie_dilatacao_narinas number := 0;
ie_succao_excessiva number := 0;
ie_ma_alimentacao number := 0;
ie_regurgitacao number := 0;
qt_score number := 0;

begin

  if nvl(:new.ie_tonus_muscular,:old.ie_tonus_muscular) = 'S' then
        ie_tonus_muscular := 2;
        qt_score := ie_tonus_muscular;
	end if;

  if nvl(:new.ie_espasmos_mioclonicos,:old.ie_espasmos_mioclonicos) = 'S' then
        ie_espasmos_mioclonicos := 3;
        qt_score := qt_score + ie_espasmos_mioclonicos;
  end if;

  if nvl(:new.ie_convulsoes,:old.ie_convulsoes) = 'S' then
        ie_convulsoes := 5;
        qt_score := qt_score + ie_convulsoes;
  end if;

  if nvl(:new.ie_escoriacoes,:old.ie_escoriacoes) = 'S' then
        ie_escoriacoes := 1;
        qt_score := qt_score + ie_escoriacoes;
  end if;

  if nvl(:new.ie_suor,:old.ie_suor) = 'S' then
        ie_suor := 1;
        qt_score := qt_score + ie_suor;
  end if;

  if nvl(:new.ie_bocejo_frequente,:old.ie_bocejo_frequente) = 'S' then
        ie_bocejo_frequente := 1;
        qt_score := qt_score + ie_bocejo_frequente;
  end if;

  if nvl(:new.ie_manchas,:old.ie_manchas) = 'S' then
        ie_manchas := 1;
        qt_score := qt_score + ie_manchas;
  end if;

  if nvl(:new.ie_entupim_nasal,:old.ie_entupim_nasal) = 'S' then
        ie_entupim_nasal := 1;
        qt_score := qt_score + ie_entupim_nasal;
  end if;

  if nvl(:new.ie_espirros,:old.ie_espirros) = 'S' then
        ie_espirros := 1;
        qt_score := qt_score + ie_espirros;
  end if;

  if nvl(:new.ie_dilatacao_narinas,:old.ie_dilatacao_narinas) = 'S' then
        ie_dilatacao_narinas := 2;
        qt_score := qt_score + ie_dilatacao_narinas;
  end if;

  if nvl(:new.ie_succao_excessiva,:old.ie_succao_excessiva) = 'S' then
        ie_succao_excessiva := 1;
        qt_score := qt_score + ie_succao_excessiva;
  end if;

  if nvl(:new.ie_ma_alimentacao,:old.ie_ma_alimentacao) = 'S' then
        ie_ma_alimentacao := 2;
        qt_score := qt_score + ie_ma_alimentacao;
  end if;

  if nvl(:new.ie_regurgitacao,:old.ie_regurgitacao) = 'S' then
        ie_regurgitacao := 2;
        qt_score := qt_score + ie_regurgitacao;
  end if;

  if nvl(:new.ie_regurgitacao,:old.ie_regurgitacao) = 'S' then
        ie_regurgitacao := 3;
        qt_score := qt_score + ie_regurgitacao;
  end if;

  :new.qt_score := qt_score + :new.ie_choro + :new.ie_sono + :new.ie_reflexo_moro + :new.ie_tremores + :new.ie_febre + :new.ie_freq_respiratoria + :new.ie_fezes;

end;
/


ALTER TABLE TASY.ESCALA_NABSTINENCE ADD (
  CONSTRAINT ESCNABS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_NABSTINENCE ADD (
  CONSTRAINT ESCNABS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCNABS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCNABS_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCNABS_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCNABS_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINATURA_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_NABSTINENCE TO NIVEL_1;

