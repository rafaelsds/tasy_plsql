ALTER TABLE TASY.QUA_NC_TIPO_DISP_IT_ESTAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_NC_TIPO_DISP_IT_ESTAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_NC_TIPO_DISP_IT_ESTAB
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  NR_SEQ_TIPO_DISP_ITEM  NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUANCTE_ESTABEL_FK_I ON TASY.QUA_NC_TIPO_DISP_IT_ESTAB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUANCTE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.QUANCTE_PK ON TASY.QUA_NC_TIPO_DISP_IT_ESTAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUANCTE_PK
  MONITORING USAGE;


CREATE INDEX TASY.QUANCTE_QUANTDI_FK_I ON TASY.QUA_NC_TIPO_DISP_IT_ESTAB
(NR_SEQ_TIPO_DISP_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUANCTE_QUANTDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.QUA_NC_TIPO_DISP_IT_ESTAB ADD (
  CONSTRAINT QUANCTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_NC_TIPO_DISP_IT_ESTAB ADD (
  CONSTRAINT QUANCTE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT QUANCTE_QUANTDI_FK 
 FOREIGN KEY (NR_SEQ_TIPO_DISP_ITEM) 
 REFERENCES TASY.QUA_NC_TIPO_DISP_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.QUA_NC_TIPO_DISP_IT_ESTAB TO NIVEL_1;

