ALTER TABLE TASY.PACIENTE_ATEND_ANOT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_ATEND_ANOT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_ATEND_ANOT
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_ATENDIMENTO         NUMBER(10)         NOT NULL,
  DT_ANOTACAO                DATE               NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DS_ANOTACAO                LONG               NOT NULL,
  DT_LIBERACAO               DATE,
  NM_USUARIO_LIB             VARCHAR2(15 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACATAN_PACATEN_FK_I ON TASY.PACIENTE_ATEND_ANOT
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATAN_PACATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATAN_PESFISI_FK_I ON TASY.PACIENTE_ATEND_ANOT
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACATAN_PK ON TASY.PACIENTE_ATEND_ANOT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATAN_TASASDI_FK_I ON TASY.PACIENTE_ATEND_ANOT
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATAN_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATAN_TASASDI_FK2_I ON TASY.PACIENTE_ATEND_ANOT
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATAN_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.paciente_atend_anot_pend_atual
after insert or update ON TASY.PACIENTE_ATEND_ANOT for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
ie_libera_pele_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(nr_atendimento),
	max(obter_codigo_paciente_setor(nr_seq_paciente))
into	nr_atendimento_w,
	cd_pessoa_fisica_w
from	paciente_atendimento
where	nr_seq_atendimento	= :new.nr_seq_atendimento;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'QLAN';
elsif	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XQLAN';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.PACIENTE_ATEND_ANOT ADD (
  CONSTRAINT PACATAN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_ATEND_ANOT ADD (
  CONSTRAINT PACATAN_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACATAN_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACATAN_PACATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO (NR_SEQ_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT PACATAN_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PACIENTE_ATEND_ANOT TO NIVEL_1;

