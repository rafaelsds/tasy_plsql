ALTER TABLE TASY.MATERIAL_MARCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_MARCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_MARCA
(
  CD_MATERIAL                    NUMBER(6)      NOT NULL,
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  QT_PRIORIDADE                  NUMBER(5),
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  NR_REGISTRO_ANVISA             VARCHAR2(60 BYTE),
  DT_VALIDADE_REG_ANVISA         DATE,
  CD_CNPJ                        VARCHAR2(14 BYTE),
  DT_REPROVACAO                  DATE,
  NM_USUARIO_REPROVACAO          VARCHAR2(15 BYTE),
  DS_OBSERVACAO                  VARCHAR2(4000 BYTE),
  CD_ESTABELECIMENTO             NUMBER(4),
  CD_REFERENCIA                  VARCHAR2(80 BYTE),
  QT_CONV_COMPRA_EST             NUMBER(15,4),
  CD_UNIDADE_MEDIDA              VARCHAR2(30 BYTE),
  NR_CERTIFICADO_APROVACAO       VARCHAR2(20 BYTE),
  DT_VALIDADE_CERTIFICADO_APROV  DATE,
  IE_PADRONIZADO                 VARCHAR2(15 BYTE),
  NR_SEQ_STATUS_AVAL             NUMBER(10),
  DS_JUSTIFICATIVA_PADRAO        VARCHAR2(4000 BYTE),
  IE_VIGENTE_ANVISA              VARCHAR2(1 BYTE),
  CD_SISTEMA_ANT                 VARCHAR2(80 BYTE),
  IE_REG_ANVISA_ISENTO           VARCHAR2(1 BYTE),
  DS_MOTIVO_ISENCAO_ANVISA       VARCHAR2(255 BYTE),
  CD_FABRICANTE                  VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATMARC_ESTABEL_FK_I ON TASY.MATERIAL_MARCA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATMARC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATMARC_MARCA_FK_I ON TASY.MATERIAL_MARCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATMARC_MATERIA_FK_I ON TASY.MATERIAL_MARCA
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATMARC_PESJURI_FK_I ON TASY.MATERIAL_MARCA
(CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATMARC_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATMARC_PK ON TASY.MATERIAL_MARCA
(CD_MATERIAL, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATMARC_STAVAMA_FK_I ON TASY.MATERIAL_MARCA
(NR_SEQ_STATUS_AVAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATMARC_STAVAMA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATMARC_UNIMEDI_FK_I ON TASY.MATERIAL_MARCA
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1232K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MATERIAL_MARCA_tp  after update ON TASY.MATERIAL_MARCA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_MATERIAL='||to_char(:old.CD_MATERIAL)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_JUSTIFICATIVA_PADRAO,1,4000),substr(:new.DS_JUSTIFICATIVA_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_JUSTIFICATIVA_PADRAO',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FABRICANTE,1,4000),substr(:new.CD_FABRICANTE,1,4000),:new.nm_usuario,nr_seq_w,'CD_FABRICANTE',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REGISTRO_ANVISA,1,4000),substr(:new.NR_REGISTRO_ANVISA,1,4000),:new.nm_usuario,nr_seq_w,'NR_REGISTRO_ANVISA',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_REG_ANVISA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_REG_ANVISA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_REG_ANVISA',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNPJ,1,4000),substr(:new.CD_CNPJ,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNPJ',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_REFERENCIA,1,4000),substr(:new.CD_REFERENCIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_REFERENCIA',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONV_COMPRA_EST,1,4000),substr(:new.QT_CONV_COMPRA_EST,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONV_COMPRA_EST',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA,1,4000),substr(:new.CD_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERTIFICADO_APROVACAO,1,4000),substr(:new.NR_CERTIFICADO_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERTIFICADO_APROVACAO',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CERTIFICADO_APROV,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CERTIFICADO_APROV,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CERTIFICADO_APROV',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRONIZADO,1,4000),substr(:new.IE_PADRONIZADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRONIZADO',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_STATUS_AVAL,1,4000),substr(:new.NR_SEQ_STATUS_AVAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_STATUS_AVAL',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PRIORIDADE,1,4000),substr(:new.QT_PRIORIDADE,1,4000),:new.nm_usuario,nr_seq_w,'QT_PRIORIDADE',ie_log_w,ds_w,'MATERIAL_MARCA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.material_marca_atual
before insert or update ON TASY.MATERIAL_MARCA for each row
declare
ie_obriga_val_anvisa_w	varchar2(1);
begin

ie_obriga_val_anvisa_w := substr(nvl(obter_valor_param_usuario(132,164, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo),'N'),1,1);

if	((ie_obriga_val_anvisa_w = 'S') and
	(:new.nr_registro_anvisa is not null) and
	(:new.dt_validade_reg_anvisa is null) and
	(nvl(:new.IE_VIGENTE_ANVISA,'X') = 'X') and
	((inserting) or
	((updating) and
	((:old.nr_registro_anvisa is null) or
	(:old.dt_validade_reg_anvisa is not null) or
	(nvl(:old.IE_VIGENTE_ANVISA,'X') <> 'X'))))) then
	/*obrigat�rio informar a validade do registro anvisa!*/
	wheb_mensagem_pck.exibir_mensagem_abort(270848);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_marca_after
after insert or update or delete ON TASY.MATERIAL_MARCA for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_operacao_w			varchar2(1);
event_w					varchar2(100);
event_class_w			varchar2(150);
cd_material_w			material.cd_material%type;
nr_sequencia_w			material_marca.nr_sequencia%type;
nm_usuario_w			usuario.nm_usuario%type;
ds_retorno_integracao_w clob;
qt_retorno_w        varchar2(1);

begin

select	obter_valor_param_usuario(9041,
                                  10,
                                  obter_perfil_ativo,
                                  wheb_usuario_pck.get_nm_usuario(),
                                  obter_estabelecimento_ativo())
into	qt_retorno_w
from	dual;

if (qt_retorno_w = 'S')then
  /*Quando Incluir, alterar e excluir as informa��es da material,_marca chama a integra��o padr�o*/
  if	(inserting) then
    ie_operacao_w			:= 'I';
    cd_material_w			:= :new.cd_material;
    nr_sequencia_w			:= :new.nr_sequencia;
    nm_usuario_w			:= :new.nm_usuario;

    event_w					:= 'materialbrand.added'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialbrand.outbound.MaterialBrandAddedCallback'; -- event class created in tasy-interfaces(material module).
  elsif	(updating) then
    ie_operacao_w			:= 'A';
    cd_material_w			:= :new.cd_material;
    nr_sequencia_w			:= :new.nr_sequencia;
    nm_usuario_w			:= :new.nm_usuario;

    event_w					:= 'materialbrand.updated'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialbrand.outbound.MaterialBrandUpdatedCallback'; -- event class created in tasy-interfaces(material module).
  else
    ie_operacao_w			:= 'A';
    cd_material_w			:= :old.cd_material;
    nr_sequencia_w			:= :old.nr_sequencia;
    nm_usuario_w			:= :old.nm_usuario;

    event_w					:= 'materialbrand.deleted'; -- event register in bifrost
    event_class_w			:= 'com.philips.tasy.integration.materialbrand.outbound.MaterialBrandDeletedCallback'; -- event class created in tasy-interfaces(material module).
  end if;

  reg_integracao_p.ie_operacao		:=	ie_operacao_w;
  reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;

  gerar_int_padrao.gravar_integracao('1', cd_material_w,nm_usuario_w, reg_integracao_p);

  SELECT BIFROST.SEND_INTEGRATION(
      event_w,
      event_class_w,
      '{"material" : '||cd_material_w||', "brand" : '||nr_sequencia_w||'}',
      'integration')
  INTO ds_retorno_integracao_w
  FROM dual;
end if;

end;
/


ALTER TABLE TASY.MATERIAL_MARCA ADD (
  CONSTRAINT MATMARC_PK
 PRIMARY KEY
 (CD_MATERIAL, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_MARCA ADD (
  CONSTRAINT MATMARC_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATMARC_STAVAMA_FK 
 FOREIGN KEY (NR_SEQ_STATUS_AVAL) 
 REFERENCES TASY.STATUS_AVALIACAO_MARCA (NR_SEQUENCIA),
  CONSTRAINT MATMARC_MARCA_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.MARCA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATMARC_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT MATMARC_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MATMARC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.MATERIAL_MARCA TO NIVEL_1;

