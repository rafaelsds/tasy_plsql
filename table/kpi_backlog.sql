ALTER TABLE TASY.KPI_BACKLOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.KPI_BACKLOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.KPI_BACKLOG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_GERENCIA      NUMBER(10),
  NR_SEQ_GRUPO         NUMBER(10),
  DS_ANO               VARCHAR2(4 BYTE),
  NR_QUARTER           VARCHAR2(1 BYTE),
  QT_OS_RECEBIDA       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.KPIBACK_PK ON TASY.KPI_BACKLOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.KPI_BACKLOG ADD (
  CONSTRAINT KPIBACK_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.KPI_BACKLOG TO NIVEL_1;

