ALTER TABLE TASY.TIPO_AMPUTACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_AMPUTACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_AMPUTACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_AMPUTACAO         VARCHAR2(80 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  PR_PROPORCAO         NUMBER(15,2)             NOT NULL,
  CD_EXP_AMPUTACAO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIAMPUT_DICEXPR_FK_I ON TASY.TIPO_AMPUTACAO
(CD_EXP_AMPUTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIAMPUT_PK ON TASY.TIPO_AMPUTACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TIPO_AMPUTACAO ADD (
  CONSTRAINT TIAMPUT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_AMPUTACAO ADD (
  CONSTRAINT TIAMPUT_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_AMPUTACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.TIPO_AMPUTACAO TO NIVEL_1;

