ALTER TABLE TASY.ROP_REGRA_ROUPA_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_REGRA_ROUPA_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_REGRA_ROUPA_SETOR
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  NR_SEQ_ROUPA          NUMBER(10),
  QT_MINIMO             NUMBER(5)               NOT NULL,
  QT_MAXIMO             NUMBER(5)               NOT NULL,
  NR_SEQ_KIT            NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RORERSE_ESTABEL_FK_I ON TASY.ROP_REGRA_ROUPA_SETOR
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RORERSE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RORERSE_PK ON TASY.ROP_REGRA_ROUPA_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RORERSE_PK
  MONITORING USAGE;


CREATE INDEX TASY.RORERSE_ROPKIRO_FK_I ON TASY.ROP_REGRA_ROUPA_SETOR
(NR_SEQ_KIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RORERSE_ROPKIRO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RORERSE_ROROUPA_FK_I ON TASY.ROP_REGRA_ROUPA_SETOR
(NR_SEQ_ROUPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RORERSE_ROROUPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RORERSE_SETATEN_FK_I ON TASY.ROP_REGRA_ROUPA_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RORERSE_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.rop_regra_roupa_setor_atual
BEFORE INSERT OR UPDATE ON TASY.ROP_REGRA_ROUPA_SETOR FOR EACH ROW
DECLARE

BEGIN

if	(:new.nr_seq_kit is null) and
	(:new.nr_seq_roupa is null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265539);
	--'Deve ser informado uma Roupa ou um Kit.'
end if;

if	(:new.nr_seq_kit is not null) and
	(:new.nr_seq_roupa is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(265540);
	--'Deve ser informado somente uma Roupa ou somente um Kit.'
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.ROP_REGRA_ROUPA_SETOR_tp  after update ON TASY.ROP_REGRA_ROUPA_SETOR FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'ROP_REGRA_ROUPA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'ROP_REGRA_ROUPA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_KIT,1,4000),substr(:new.NR_SEQ_KIT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_KIT',ie_log_w,ds_w,'ROP_REGRA_ROUPA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAXIMO,1,4000),substr(:new.QT_MAXIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAXIMO',ie_log_w,ds_w,'ROP_REGRA_ROUPA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ROUPA,1,4000),substr(:new.NR_SEQ_ROUPA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ROUPA',ie_log_w,ds_w,'ROP_REGRA_ROUPA_SETOR',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMO,1,4000),substr(:new.QT_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMO',ie_log_w,ds_w,'ROP_REGRA_ROUPA_SETOR',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ROP_REGRA_ROUPA_SETOR ADD (
  CONSTRAINT RORERSE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_REGRA_ROUPA_SETOR ADD (
  CONSTRAINT RORERSE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT RORERSE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RORERSE_ROROUPA_FK 
 FOREIGN KEY (NR_SEQ_ROUPA) 
 REFERENCES TASY.ROP_ROUPA (NR_SEQUENCIA),
  CONSTRAINT RORERSE_ROPKIRO_FK 
 FOREIGN KEY (NR_SEQ_KIT) 
 REFERENCES TASY.ROP_KIT_ROUPARIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_REGRA_ROUPA_SETOR TO NIVEL_1;

