ALTER TABLE TASY.TRIAGEM_PRONTO_ATEND
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TRIAGEM_PRONTO_ATEND CASCADE CONSTRAINTS;

CREATE TABLE TASY.TRIAGEM_PRONTO_ATEND
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  NR_SEQ_CLASSIF             NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(1000 BYTE),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  IE_STATUS_PACIENTE         VARCHAR2(15 BYTE),
  DT_FIM_TRIAGEM             DATE,
  DS_JUSTIFICATIVA_CLASSIF   VARCHAR2(255 BYTE),
  DT_INICIO_TRIAGEM          DATE,
  NR_SEQ_TRIAGEM_PRIORIDADE  NUMBER(10),
  CD_PROCEDENCIA             NUMBER(5),
  DS_QUEIXA_PRINC            VARCHAR2(255 BYTE),
  IE_CLINICA                 NUMBER(5),
  NR_SEQ_QUEIXA              NUMBER(10),
  DT_CHAMADA_PAINEL          DATE,
  NR_SEQ_FILA_SENHA          NUMBER(10),
  NR_SEQ_PAC_FILA            NUMBER(10),
  DS_FATOR_DETERMINANTE      VARCHAR2(255 BYTE),
  IE_EM_ATENDIMENTO          VARCHAR2(1 BYTE),
  CD_SENHA_GERADA_OLD        NUMBER(10),
  CD_SENHA_GERADA            VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO         NUMBER(4),
  DT_CANCELAMENTO            DATE,
  DS_JUSTIFICATIVA_CANCEL    VARCHAR2(255 BYTE),
  NM_USUARIO_CANCEL          VARCHAR2(15 BYTE),
  NM_USUARIO_TRIAGEM         VARCHAR2(15 BYTE),
  CD_CLASSIFICADOR           VARCHAR2(15 BYTE),
  NR_SEQ_MANCHESTER_FLUXO    NUMBER(10),
  IE_RETRIAGEM               VARCHAR2(1 BYTE),
  IE_AUDITORIA               VARCHAR2(1 BYTE),
  DS_OBS_AUDITORIA           VARCHAR2(2000 BYTE),
  DT_AUDITORIA               DATE,
  NR_SEQ_DISCRIMINADOR       NUMBER(10),
  NR_SEQ_FLUXO_ORIGINAL      NUMBER(10),
  IE_NAO                     VARCHAR2(1 BYTE),
  IE_PROTOCOLO_ASSISTENCIAL  VARCHAR2(1 BYTE),
  DS_RESUMO_MANCHESTER       VARCHAR2(4000 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  IE_VERSAO_MANCHESTER       VARCHAR2(10 BYTE),
  DT_TRIAGEM_INCOMPLETA      DATE,
  DT_INICIO_RETRIAGEM        DATE,
  DT_FIM_RETRIAGEM           DATE,
  CD_DEPART_MEDICO           NUMBER(15),
  DS_CHEGADA_METODO          VARCHAR2(50 BYTE),
  DS_EM_MED_SER              VARCHAR2(50 BYTE),
  DS_TIPO_EMERGENCIA         VARCHAR2(50 BYTE),
  CD_CHEGADA_METODO          NUMBER(15),
  CD_EM_MED_SER              NUMBER(15),
  CD_TIPO_EMERGENCIA         NUMBER(15),
  CD_REF_MED_CUI             VARCHAR2(15 BYTE),
  CD_MEDICO                  VARCHAR2(10 BYTE),
  CD_PRIM_CON_MED            VARCHAR2(10 BYTE),
  CD_SEC_CON_MED             VARCHAR2(10 BYTE),
  NR_SEQ_VIS_CLASSIF         VARCHAR2(15 BYTE),
  CD_EVOLUCAO                NUMBER(10),
  DS_REF_MED_TEX             VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TRPRAT_ATEPACI_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_ESTABEL_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRPRAT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRPRAT_EVOPACI_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_I1 ON TASY.TRIAGEM_PRONTO_ATEND
(NR_SEQ_FILA_SENHA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_I2 ON TASY.TRIAGEM_PRONTO_ATEND
(NR_ATENDIMENTO, DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_MANFLUX_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(NR_SEQ_MANCHESTER_FLUXO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_MANFLUX_FK2_I ON TASY.TRIAGEM_PRONTO_ATEND
(NR_SEQ_FLUXO_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_MANITEM_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(NR_SEQ_DISCRIMINADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_PESFISI_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_PESFISI_FK2_I ON TASY.TRIAGEM_PRONTO_ATEND
(CD_CLASSIFICADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_PESJURI_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(CD_REF_MED_CUI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TRPRAT_PK ON TASY.TRIAGEM_PRONTO_ATEND
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_PROCEDE_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(CD_PROCEDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRPRAT_PROCEDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRPRAT_QUEPACI_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(NR_SEQ_QUEIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRPRAT_QUEPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRPRAT_TASASDI_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TRPRAT_TRCLPRI_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(NR_SEQ_TRIAGEM_PRIORIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRPRAT_TRCLPRI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TRPRAT_TRICLRI_FK_I ON TASY.TRIAGEM_PRONTO_ATEND
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TRPRAT_TRICLRI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.triagem_pronto_atend_update
AFTER UPDATE ON TASY.TRIAGEM_PRONTO_ATEND FOR EACH ROW
declare

ie_atualizar_dados_w	varchar2(1);
ie_atualizar_clinica_w	varchar2(1);
nr_seq_evento_atendimento_w	number(10);
ds_erro_w		varchar2(255);

CURSOR C09 IS
	SELECT	a.nr_seq_evento
	FROM	regra_envio_sms a
	WHERE	a.cd_estabelecimento	= wheb_usuario_pck.get_cd_estabelecimento
	AND	a.ie_evento_disp	= 'TCP'
	and	nvl(a.ie_situacao,'A') = 'A'
	order by a.nr_seq_evento desc;

begin

Obter_Param_Usuario(281,972,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_atualizar_dados_w);
Obter_Param_Usuario(916,329,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_atualizar_clinica_w);

if	(ie_atualizar_dados_w = 'S') and
	(:new.nr_atendimento is not null)then

		if  	((nvl(:old.nr_seq_classif,0) <> nvl(:new.nr_seq_classif,0)) or
			(nvl(:old.nr_seq_triagem_prioridade,0) <> nvl(:new.nr_seq_triagem_prioridade,0))) then
			update	atendimento_paciente
			set	nr_seq_triagem		   =	:new.nr_seq_classif,
				nr_seq_triagem_prioridade  =	:new.nr_seq_triagem_prioridade
			where	nr_atendimento		   =	:new.nr_atendimento;
		end if;

		if	(nvl(:old.dt_chamada_painel,sysdate - 1) <> nvl(:new.dt_chamada_painel,sysdate - 1)) then
			update  atendimento_paciente
			set     dt_chamada_enfermagem =	:new.dt_chamada_painel,
				ie_chamado            = decode(:new.dt_chamada_painel,null,'N','S')
			where	nr_atendimento        =	:new.nr_atendimento;

		end if;

		if	(nvl(:old.dt_inicio_triagem,sysdate - 1) <> nvl(:new.dt_inicio_triagem,sysdate - 1)) then
			update  atendimento_paciente
			set 	dt_inicio_atendimento = :new.dt_inicio_triagem,
					nm_usuario_triagem	  = nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario)
			where	nr_atendimento        =	:new.nr_atendimento;
		end if;

		if	(nvl(:old.dt_fim_triagem,sysdate - 1) <> nvl(:new.dt_fim_triagem,sysdate - 1)) then
			update  atendimento_paciente
			set 	dt_fim_triagem	      = :new.dt_fim_triagem
			where	nr_atendimento        =	:new.nr_atendimento;

			job_regra_alterar_local_status(:new.nr_atendimento,'FE',nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario),obter_perfil_ativo,obter_estabelecimento_ativo);

		end if;

		if  (ie_atualizar_clinica_w = 'S') and
			(nvl(:old.ie_clinica,0) <> nvl(:new.ie_clinica,0)) then
			update	atendimento_paciente
			set	ie_clinica		   =	:new.ie_clinica
			where	nr_atendimento		   =	:new.nr_atendimento;
		end if;

end if;

if	(nvl(:old.nr_seq_classif,0) <> nvl(:new.nr_seq_classif,0)) then
	insert into log_classif_triagem (
			nr_sequencia,
			dt_atualizacao,
			nm_usuario,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nr_seq_classif,
			nr_seq_triagem)
	values (log_classif_triagem_seq.nextval,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:old.nr_seq_classif,
			:new.nr_sequencia);
end if;

begin
if	(nvl(:NEW.NR_SEQ_CLASSIF,0)	<> nvl(:OLD.NR_SEQ_CLASSIF,0)) and
	(:NEW.nr_atendimento	is null)then
	begin

	OPEN C09;
	LOOP
	FETCH C09 INTO
		nr_seq_evento_atendimento_w;
	EXIT WHEN C09%NOTFOUND;
		BEGIN
		gerar_evento_paciente_trigger(nr_seq_evento_atendimento_w, :NEW.nr_atendimento, :NEW.cd_Pessoa_fisica, 0, nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario), null);
		END;
	END LOOP;
	CLOSE C09;
	end;
end if;

EXCEPTION
WHEN OTHERS THEN
	NULL;
END;

if (:new.NR_SEQ_CLASSIF <> nvl(:old.NR_SEQ_CLASSIF,-1)) then
	gerar_lancamento_automatico(:new.nr_atendimento,null,589,:new.nm_usuario,:new.nr_sequencia,'','','',:new.NR_SEQ_CLASSIF,null);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TRIAGEM_PRONTO_ATEND_atual
before insert or update ON TASY.TRIAGEM_PRONTO_ATEND for each row
declare

ie_sexo_paciente_w	Varchar2(2);
ie_sexo_regra_w		Varchar2(2);
ie_tipo_nome_w		Varchar2(1);

ie_prot_institut_ant_w	varchar2(1);
ie_prot_institut_w	varchar2(1);

begin

if 	(:new.cd_estabelecimento is null) then
	:new.cd_estabelecimento	:= wheb_usuario_pck.get_cd_estabelecimento;
end if;

Obter_param_Usuario(10021, 65, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_tipo_nome_w);

if	(:new.nr_seq_triagem_prioridade is not null) then
	select 	nvl(ie_sexo,'A')
	into	ie_sexo_regra_w
	from	triagem_classif_prioridade
	where	nr_sequencia = :new.nr_seq_triagem_prioridade;

	if	(ie_sexo_regra_w <> 'A') then
		select	nvl(Obter_Sexo_PF(:new.cd_pessoa_fisica,'C'),'I')
		into	ie_sexo_paciente_w
		from 	dual;

		if	(ie_sexo_paciente_w <> ie_sexo_regra_w) and
			(ie_sexo_paciente_w <> 'I') then
			Wheb_mensagem_pck.exibir_mensagem_abort(212461);
		end if;
	end if;

end if;

begin

	if	((:new.NR_SEQ_FILA_SENHA is not null) and
		(:old.NR_SEQ_FILA_SENHA is null)) then

		if	(:new.CD_SENHA_GERADA	is null) then
			:new.CD_SENHA_GERADA := obter_dados_senha(:new.NR_SEQ_FILA_SENHA,'LC');
		end if;

		if	(ie_tipo_nome_w in ('1','2')) then
			UPDATE	paciente_senha_fila
			SET	nm_paciente	 = decode(:new.cd_pessoa_fisica,null,substr(obter_nome_pf(:new.CD_PESSOA_FISICA),1,255),null),
				cd_pessoa_fisica = :new.cd_pessoa_fisica
			WHERE	nr_sequencia	= :new.NR_SEQ_FILA_SENHA;
		elsif	(ie_tipo_nome_w in ('3','4')) then
			UPDATE	paciente_senha_fila
			SET	nm_paciente	= decode(:new.cd_pessoa_fisica,null,substr(pls_gerar_nome_abreviado(obter_nome_pf(:new.CD_PESSOA_FISICA)),1,255),null),
				cd_pessoa_fisica = :new.cd_pessoa_fisica
			WHERE	nr_sequencia	= :new.NR_SEQ_FILA_SENHA;
		elsif	(ie_tipo_nome_w = '5') then
			UPDATE	paciente_senha_fila
			SET	nm_paciente	= decode(:new.cd_pessoa_fisica,null,substr(OBTER_INICIAIS_NOME_SENHAS(:new.CD_PESSOA_FISICA,obter_nome_pf(:new.CD_PESSOA_FISICA)),1,255),null),
				cd_pessoa_fisica = :new.cd_pessoa_fisica
			WHERE	nr_sequencia	= :new.NR_SEQ_FILA_SENHA;
		end if;

	end if;

exception
	when 	others then
		null;
end;


if	(:new.DT_INICIO_TRIAGEM is not null) and
	(:old.DT_INICIO_TRIAGEM is null) then
	:new.NM_USUARIO_TRIAGEM := obter_usuario_ativo;
end if;


if	(:new.NR_SEQ_QUEIXA is not null) then

	select	ie_prot_institut,
		ie_prot_institut_ant
	into	ie_prot_institut_w,
		ie_prot_institut_ant_w
	from	queixa_paciente
	where	nr_sequencia = :new.nr_seq_queixa;

	if	((ie_prot_institut_w = 'S') or (ie_prot_institut_ant_w = 'S')) then

		:new.IE_PROTOCOLO_ASSISTENCIAL	:= 'S';

	end if;

end if;

if	(:new.NR_SEQ_MANCHESTER_FLUXO = 10) then -- NR_SEQUENCIA = 10      NR_FLUXO = 25
	:new.IE_PROTOCOLO_ASSISTENCIAL	:= 'S';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.TRIAGEM_PRONTO_ATEND_tp  after update ON TASY.TRIAGEM_PRONTO_ATEND FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_CLINICA,1,500);gravar_log_alteracao(substr(:old.IE_CLINICA,1,4000),substr(:new.IE_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLINICA',ie_log_w,ds_w,'TRIAGEM_PRONTO_ATEND',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.DELETE_TRIAGEM_PA_ATUAL
before delete ON TASY.TRIAGEM_PRONTO_ATEND for each row
declare
ie_gerar_clinical_notes_w varchar2(1);
nm_user_w varchar(100) :=wheb_usuario_pck.get_nm_usuario;
begin

obter_param_usuario(281, 1643, Obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_gerar_clinical_notes_w);
if(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (:old.cd_evolucao is not null and ie_gerar_clinical_notes_w ='S') then
	update evolucao_paciente
	set dt_inativacao = sysdate,
	ie_situacao ='I',
	dt_atualizacao = sysdate ,
	nm_usuario = nm_user_w,
	nm_usuario_inativacao = nm_user_w
	where cd_evolucao = :old.cd_evolucao;
	delete from clinical_note_soap_data where cd_evolucao = :old.cd_evolucao and ie_med_rec_type = 'EMERG_SERV_CONS' and nr_seq_med_item=:old.nr_sequencia ;
end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.finalizar_triagem_pa_atual
after insert or update ON TASY.TRIAGEM_PRONTO_ATEND for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

ie_evolucao_triagem_w	varchar2(10);
nm_usuario_w 			varchar2(50);

begin
if (:old.dt_fim_triagem is null) and (:new.dt_fim_triagem is not null) and (:new.ds_observacao is not null) then
	nm_usuario_w := nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario_nrec);
	ie_evolucao_triagem_w := Obter_Valor_Param_Usuario(281, 1169, Obter_Perfil_Ativo, nm_usuario_w, wheb_usuario_pck.get_cd_estabelecimento);
	if	(ie_evolucao_triagem_w is not null) then
		gerar_evolucao_fim_triagem_pa(:new.nr_atendimento,nm_usuario_w,ie_evolucao_triagem_w,:new.ds_observacao,:new.cd_pessoa_fisica);
	end if;
end if;
end;
/


ALTER TABLE TASY.TRIAGEM_PRONTO_ATEND ADD (
  CONSTRAINT TRPRAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TRIAGEM_PRONTO_ATEND ADD (
  CONSTRAINT TRPRAT_TRCLPRI_FK 
 FOREIGN KEY (NR_SEQ_TRIAGEM_PRIORIDADE) 
 REFERENCES TASY.TRIAGEM_CLASSIF_PRIORIDADE (NR_SEQUENCIA),
  CONSTRAINT TRPRAT_PROCEDE_FK 
 FOREIGN KEY (CD_PROCEDENCIA) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA),
  CONSTRAINT TRPRAT_QUEPACI_FK 
 FOREIGN KEY (NR_SEQ_QUEIXA) 
 REFERENCES TASY.QUEIXA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT TRPRAT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TRPRAT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT TRPRAT_TRICLRI_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.TRIAGEM_CLASSIF_RISCO (NR_SEQUENCIA),
  CONSTRAINT TRPRAT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TRPRAT_MANFLUX_FK 
 FOREIGN KEY (NR_SEQ_MANCHESTER_FLUXO) 
 REFERENCES TASY.MANCHESTER_FLUXOGRAMA (NR_SEQUENCIA),
  CONSTRAINT TRPRAT_PESFISI_FK2 
 FOREIGN KEY (CD_CLASSIFICADOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT TRPRAT_MANFLUX_FK2 
 FOREIGN KEY (NR_SEQ_FLUXO_ORIGINAL) 
 REFERENCES TASY.MANCHESTER_FLUXOGRAMA (NR_SEQUENCIA),
  CONSTRAINT TRPRAT_MANITEM_FK 
 FOREIGN KEY (NR_SEQ_DISCRIMINADOR) 
 REFERENCES TASY.MANCHESTER_ITEM (NR_SEQUENCIA),
  CONSTRAINT TRPRAT_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT TRPRAT_PESJURI_FK 
 FOREIGN KEY (CD_REF_MED_CUI) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT TRPRAT_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO));

GRANT SELECT ON TASY.TRIAGEM_PRONTO_ATEND TO NIVEL_1;

