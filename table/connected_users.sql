DROP TABLE TASY.CONNECTED_USERS CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONNECTED_USERS
(
  DS_TOKEN       VARCHAR2(64 BYTE)              NOT NULL,
  NM_USUARIO     VARCHAR2(15 BYTE)              NOT NULL,
  DT_KEEP_ALIVE  DATE                           NOT NULL,
  DT_LOGIN       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.CONNECTED_USERS TO NIVEL_1;

