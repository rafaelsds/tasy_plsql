ALTER TABLE TASY.PLS_CONTA_MEDICA_RESUMO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTA_MEDICA_RESUMO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTA_MEDICA_RESUMO
(
  NR_SEQ_CONTA              NUMBER(10)          NOT NULL,
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_ITEM               NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_PROCEDIMENTO           NUMBER(15),
  IE_ORIGEM_PROCED          NUMBER(10),
  NR_SEQ_MATERIAL           NUMBER(10),
  QT_ITEM                   NUMBER(15,4),
  NR_SEQ_PRESTADOR_PGTO     NUMBER(10),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE),
  NR_SEQ_COMPETENCIA        NUMBER(10),
  VL_MEDICO                 NUMBER(15,2),
  VL_ANESTESISTA            NUMBER(15,2),
  VL_AUXILIARES             NUMBER(15,2),
  VL_CUSTO_OPERACIONAL      NUMBER(15,2),
  VL_MATERIAIS              NUMBER(15,2),
  VL_CALCULADO              NUMBER(15,2),
  VL_APRESENTADO            NUMBER(15,2),
  VL_LIBERADO               NUMBER(15,2),
  VL_BENEFICIARIO           NUMBER(15,2),
  NR_SEQ_LOTE_PGTO          NUMBER(10),
  NR_SEQ_EVENTO             NUMBER(10),
  NR_SEQ_PRODUCAO           NUMBER(10),
  DT_COMPETENCIA            DATE,
  NR_SEQ_SEGURADO           NUMBER(10),
  CD_GUIA                   VARCHAR2(20 BYTE),
  NR_SEQ_PRESTADOR_ATEND    NUMBER(10),
  NR_SEQ_PARTICIPANTE       NUMBER(10),
  NR_SEQ_GRAU_PARTIC        NUMBER(10),
  VL_GLOSA                  NUMBER(15,2),
  VL_UNITARIO               NUMBER(15,2),
  VL_UNITARIO_CALCULADO     NUMBER(15,2),
  CD_GUIA_REFERENCIA        VARCHAR2(20 BYTE),
  IE_STATUS                 VARCHAR2(1 BYTE),
  IE_TIPO_GUIA              VARCHAR2(10 BYTE),
  DS_ITEM                   VARCHAR2(255 BYTE),
  NM_PRESTADOR_PGTO         VARCHAR2(255 BYTE),
  IE_TIPO_ITEM              VARCHAR2(10 BYTE),
  DS_TIPO_ITEM              VARCHAR2(170 BYTE),
  NR_SEQ_PRESTADOR_EXEC     NUMBER(10),
  NR_SEQ_TIPO_ATENDIMENTO   NUMBER(10),
  IE_ORIGEM_CONTA           VARCHAR2(1 BYTE),
  IE_TIPO_DESPESA           VARCHAR2(10 BYTE),
  NR_SEQ_PAG_ITEM           NUMBER(10),
  IE_TIPO_CONTRATACAO       VARCHAR2(2 BYTE),
  NR_SEQ_REGRA_VALOR        NUMBER(10),
  PR_LIBERADO               NUMBER(5,2),
  NM_PRESTADOR_EXEC         VARCHAR2(255 BYTE),
  NR_SEQ_PREST_VENC_TRIB    NUMBER(10),
  NR_SEQ_PROTOCOLO          NUMBER(10),
  IE_PROC_MAT               VARCHAR2(10 BYTE),
  IE_STATUS_PROTOCOLO       VARCHAR2(1 BYTE),
  NR_SEQ_PERIODO_PGTO       NUMBER(10),
  VL_TAXA_ADM               NUMBER(15,2),
  NR_SEQ_EXAME_COLETA       NUMBER(10),
  NR_SEQ_HONORARIO_CRIT     NUMBER(10),
  VL_TAXA_ADM_CO            NUMBER(15,2),
  VL_TAXA_ADM_MAT           NUMBER(15,2),
  PR_TAXA_ADM_CO            NUMBER(5,2),
  PR_TAXA_ADM_MAT           NUMBER(5,2),
  NR_SEQ_PROT_REFERENCIA    NUMBER(10),
  NR_SEQ_GRUPO_ANS          NUMBER(10),
  NR_SEQ_FATURA             NUMBER(10),
  CD_COOPERATIVA_PGTO       VARCHAR2(10 BYTE),
  CD_GUIA_PRESTADOR         VARCHAR2(20 BYTE),
  DT_ITEM                   DATE,
  DT_COMPETENCIA_PGTO       DATE,
  NR_PROTOCOLO_PRESTADOR    VARCHAR2(20 BYTE),
  VL_LIB_ORIGINAL           NUMBER(15,2),
  NR_SEQ_REGRA_PGTO         NUMBER(10),
  IE_CONTADOR               VARCHAR2(1 BYTE),
  DS_LOG                    VARCHAR2(255 BYTE),
  NR_SEQ_SERV_PRE_PGTO      NUMBER(10),
  VL_PROVISORIO             NUMBER(15,2),
  IE_TIPO_DATA_PAGAMENTO    VARCHAR2(10 BYTE),
  NR_SEQ_CONTA_MAT          NUMBER(10),
  NR_SEQ_CONTA_PROC         NUMBER(10),
  NR_SEQ_CONTA_PROC_PARTIC  NUMBER(10),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_ESQUEMA            NUMBER(10),
  CD_CONTA_DEB              VARCHAR2(20 BYTE),
  CD_CONTA_CRED             VARCHAR2(20 BYTE),
  CD_CLASSIF_DEB            VARCHAR2(40 BYTE),
  CD_CLASSIF_CRED           VARCHAR2(40 BYTE),
  CD_HISTORICO              NUMBER(10),
  CD_CONTA_GLOSA_CRED       VARCHAR2(20 BYTE),
  CD_CONTA_GLOSA_DEB        VARCHAR2(20 BYTE),
  CD_HISTORICO_GLOSA        NUMBER(10),
  CD_CLASSIF_GLOSA_CRED     VARCHAR2(40 BYTE),
  CD_CLASSIF_GLOSA_DEB      VARCHAR2(40 BYTE),
  NR_SEQ_ESQUEMA_GLOSA      NUMBER(10),
  VL_TAXA_ADM_PAG           NUMBER(15,2),
  VL_CO_PAG                 NUMBER(15,2),
  VL_MATERIAIS_PAG          NUMBER(15,2),
  VL_HM                     NUMBER(15,2),
  NR_LOTE_CONTAB_PAG        NUMBER(10),
  NR_LOTE_CONTABIL_PAG      NUMBER(10),
  NR_LOTE_CONTABIL_REV      NUMBER(10),
  NR_SEQ_MVTO_PAG_CRED      NUMBER(10),
  NR_SEQ_MVTO_PAG_DEB       NUMBER(10),
  NR_SEQ_MVTO_REV_CRED      NUMBER(10),
  NR_SEQ_MVTO_REV_DEB       NUMBER(10),
  IE_ATO_COOPERADO          VARCHAR2(1 BYTE),
  NR_SEQ_REGRA_COOPERADO    NUMBER(10),
  CD_CLASSIF_PROV_CRED      VARCHAR2(40 BYTE),
  CD_CLASSIF_PROV_DEB       VARCHAR2(40 BYTE),
  CD_CONTA_PROV_CRED        VARCHAR2(20 BYTE),
  CD_CONTA_PROV_DEB         VARCHAR2(20 BYTE),
  CD_HISTORICO_PROV         NUMBER(10),
  NR_LOTE_CONTABIL_PROV     NUMBER(10),
  NR_SEQ_ESQUEMA_PROV       NUMBER(10),
  CD_HISTORICO_VL_AJUSTE    NUMBER(10),
  VL_APRES_IND              NUMBER(15,2),
  NR_SEQ_PP_LOTE            NUMBER(10),
  NR_SEQ_PP_EVENTO          NUMBER(10),
  VL_LIBERADO_ANT           NUMBER(15,2),
  DT_ATUALIZACAO_NREC       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCOMR_ANSGRDE_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_GRUPO_ANS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_ANSGRDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_CONCONT_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_CONTA_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_CONCONT_FK2_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_CONTA_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_CONCONT_FK3_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_CONTA_GLOSA_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_CONCONT_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_CONCONT_FK4_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_CONTA_GLOSA_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_CONCONT_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_CONCONT_FK5_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_CONTA_PROV_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_CONCONT_FK6_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_CONTA_PROV_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_HISPADR_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_HISPADR_FK2_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_HISTORICO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_HISPADR_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_HISPADR_FK3_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_HISTORICO_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_HISPADR_FK4_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(CD_HISTORICO_VL_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I10 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_SERV_PRE_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_I10
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_I11 ON TASY.PLS_CONTA_MEDICA_RESUMO
(DT_COMPETENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I12 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_EVENTO, NR_SEQ_PRESTADOR_PGTO, NR_SEQ_LOTE_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I13 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NVL("NR_SEQ_SERV_PRE_PGTO",0), NVL("NR_SEQ_LOTE_PGTO",0))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I14 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_LOTE_PGTO, NR_SEQ_PRESTADOR_PGTO, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I2 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_ITEM, IE_PROC_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I20 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_CONTA, IE_TIPO_ITEM, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I21 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_CONTA, NR_SEQUENCIA, NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I22 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_CONTA, NR_SEQ_CONTA_PROC, NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I23 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_CONTA, NR_SEQ_CONTA_MAT, NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I26 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PRESTADOR_PGTO, NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I3 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PARTICIPANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I4 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I5 ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_I8 ON TASY.PLS_CONTA_MEDICA_RESUMO
(DT_COMPETENCIA_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_I8
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_LOTCONT_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_LOTE_CONTABIL_PAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_LOTCONT_FK2_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_LOTE_CONTABIL_REV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_LOTCONT_FK4_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_LOTE_CONTAB_PAG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_LOTCONT_FK5_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_LOTE_CONTABIL_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_MOVCONT_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_LOTE_CONTAB_PAG, NR_SEQ_MVTO_PAG_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_MOVCONT_FK2_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_LOTE_CONTAB_PAG, NR_SEQ_MVTO_PAG_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_MOVCONT_FK5_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_LOTE_CONTABIL_REV, NR_SEQ_MVTO_REV_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_MOVCONT_FK6_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_LOTE_CONTABIL_REV, NR_SEQ_MVTO_REV_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCOMR_PK ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_CONTA, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_PLSCOMAT_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_CONTA, NR_SEQ_CONTA_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_PLSCOMR_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PROT_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_PLSCOMR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_PLSCOPRO_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_CONTA, NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_PLSESCO_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_ESQUEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_PLSESCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_PLSESCO_FK3_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_ESQUEMA_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_PLSESCO_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_PLSESCO_FK4_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_ESQUEMA_PROV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_PLSEVEN_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_PLSEVEN_FK1_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PP_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_PLSLOPA_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_LOTE_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_PLSPAIT_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PAG_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_PLSPAIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_PLSPEPA_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PERIODO_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_PLSPEPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_PLSPPVT_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PREST_VENC_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_PLSPPVT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_PLSPRES_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PRESTADOR_PGTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_PLSPRPAR_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_CONTA_PROC, NR_SEQ_CONTA_PROC_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_PLSPRPAR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOMR_PPPLOTE_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_PP_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOMR_PTUFATU_FK_I ON TASY.PLS_CONTA_MEDICA_RESUMO
(NR_SEQ_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOMR_PTUFATU_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_CONTA_MEDICA_RESUMO_ATUAL
BEFORE INSERT OR UPDATE OR DELETE ON TASY.PLS_CONTA_MEDICA_RESUMO FOR EACH ROW
declare
ds_log_call_w			Varchar2(1500);
ds_observacao_w			Varchar2(4000);
ie_tipo_conta_w			pls_conta.ie_tipo_conta%type;

begin
if	(nvl(wheb_usuario_pck.get_ie_lote_contabil,'N') = 'N') and
	(nvl(wheb_usuario_pck.get_ie_atualizacao_contabil,'N') = 'N') then
	if	(nvl(:new.ie_situacao, 'A') <> 'I') then
		if	(:new.vl_liberado < 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(225848);
		elsif	(:new.vl_lib_original < 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(225848);
		end if;
	end if;

	if	(deleting) then
		if	(:old.nr_seq_lote_pgto is not null) then
			wheb_mensagem_pck.exibir_mensagem_abort(226008);
		end if;

	end if;

	if	(nvl(:old.dt_competencia_pgto,sysdate) <> nvl(:new.dt_competencia_pgto,sysdate)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						' Seq resumo : '||:new.nr_sequencia||chr(13)||chr(10)||
						'Compet'||chr(234)||'ncia : '||chr(13)||chr(10)||
						chr(9)||'Anterior: '||:old.dt_competencia_pgto||' - Modificada: '||:new.dt_competencia_pgto||chr(13)||chr(10);

			ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);
	end if;

	if ( updating ) then

		if(:old.nr_seq_grupo_ans is not null and :new.nr_seq_grupo_ans is null and :new.vl_glosa > 0) then
			ds_observacao_w := ds_observacao_w||chr(13)||chr(10)||
					'Conta : '||:old.nr_seq_conta||'; Conta proc : '||:old.nr_seq_conta_proc||' ; Conta mat: '||:old.nr_seq_conta_mat||
							'; Grupo ANS Ant: ' ||:old.nr_seq_grupo_ans||'; Grupo ANS Atual: ' || :new.nr_seq_grupo_ans||'; ';
			ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);
		end if;
	end if;

	--Se for alterada a data de competencia de pagto, log independentemente de ser pelo tasy ou por fora
	if	( pls_se_aplicacao_tasy = 'S') and ( updating ) and
		(ds_observacao_w is not null) then --Coloquei essa restricao para nao entrar toda a hora no insert

		insert into plsprco_cta 	( 	nr_sequencia, dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
							ds_log, ds_log_call, ds_funcao_ativa,
							ie_aplicacao_tasy, nm_maquina, ie_opcao)
				values		( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu'||chr(225)||'rio n'||chr(227)||'o identificado '),1,14),
							sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu'||chr(225)||'rio n'||chr(227)||'o identificado '),1,14), 'PLS_CONTA_MEDICA_RESUMO',
							ds_observacao_w, ds_log_call_w, obter_funcao_ativa,
							'N', wheb_usuario_pck.get_machine, '0');
	end if;

	if	((inserting) and
		 (nvl(:new.vl_liberado,0) > 0) and
		 (nvl(:new.qt_item,0) = 0) and
		 (nvl(:new.vl_glosa,0) = 0))
		or
		((updating) and
		 (
		  (nvl(:old.vl_liberado,0) != nvl(:new.vl_liberado,0)) or
		  (nvl(:old.qt_item,0) != nvl(:new.qt_item,0)) or
		  (nvl(:old.vl_glosa,0) != nvl(:new.vl_glosa,0))
		 ) and
		 (nvl(:new.vl_liberado, 0) > 0) and
		 (nvl(:new.qt_item,0) = 0) and
		 (nvl(:new.vl_glosa,0) = 0)) then


		ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
					'Conta : '||:new.nr_seq_conta||'; Conta proc : '||:new.nr_seq_conta_proc||' ; Conta mat: '||:new.nr_seq_conta_mat||'; '||chr(13)||chr(10)||
					'Item foi liberado com quantidade zero e sem valor de glosa'||chr(13)||chr(10);


		ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

		insert into plsprco_cta 	( 	nr_sequencia, dt_atualizacao, nm_usuario,
							dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
							ds_log, ds_log_call, ds_funcao_ativa,
							ie_aplicacao_tasy, nm_maquina, ie_opcao)
		values		( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu'||chr(225)||'rio n'||chr(227)||'o identificado '),1,14),
					sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu'||chr(225)||'rio n'||chr(227)||'o identificado '),1,14), 'PLS_CONTA_MEDICA_RESUMO',
					ds_observacao_w, ds_log_call_w, obter_funcao_ativa,
					'N', wheb_usuario_pck.get_machine, '0');

	end if;


	if	( pls_se_aplicacao_tasy = 'N') and
		( updating ) then

		ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

		ds_observacao_w := ds_observacao_w||chr(13)||chr(10)||
				'Conta : '||:old.nr_seq_conta||'; Conta proc : '||:old.nr_seq_conta_proc||' ; Conta mat: '||:old.nr_seq_conta_mat||'; ';
		if	(nvl(:old.nr_seq_item,0) <> nvl(:new.nr_seq_item,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Item: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '||:old.nr_seq_item||' - Modificada: '||:new.nr_seq_item||chr(13)||chr(10);
		end if;

		if	(nvl(:old.ie_proc_mat,'X') <> nvl(:new.ie_proc_mat,'X')) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Proc/Mat: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '||:old.ie_proc_mat||' - Modificada: '||:new.ie_proc_mat||chr(13)||chr(10);
		end if;

		if	(nvl(:old.NR_SEQ_CONTA,0) <> nvl(:new.NR_SEQ_CONTA,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Conta: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '||:old.NR_SEQ_CONTA||' - Modificada: '||:new.NR_SEQ_CONTA||chr(13)||chr(10);
		end if;

		if	(nvl(:old.NR_SEQ_LOTE_PGTO,0) <> nvl(:new.NR_SEQ_LOTE_PGTO,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Lote de pagamento: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '||:old.NR_SEQ_LOTE_PGTO||' - Modificada: '||:new.NR_SEQ_LOTE_PGTO||chr(13)||chr(10);
		end if;

		if	(nvl(:old.VL_LIB_ORIGINAL,0) <> nvl(:new.VL_LIB_ORIGINAL,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Vl lib original: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '||:old.VL_LIB_ORIGINAL||' - Modificada: '||:new.VL_LIB_ORIGINAL||chr(13)||chr(10);
		end if;

		if	(nvl(:old.VL_LIBERADO,0) <> nvl(:new.VL_LIBERADO,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Vl liberado: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '||:old.VL_LIBERADO||' - Modificada: '||:new.VL_LIBERADO||chr(13)||chr(10);
		end if;

		if	(nvl(:old.nr_seq_evento,0) <> nvl(:new.nr_seq_evento,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Evento: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '||:old.nr_seq_evento||' - Modificada: '||:new.nr_seq_evento||chr(13)||chr(10);
		end if;

		if	(nvl(:old.nr_seq_pag_item,0) <> nvl(:new.nr_seq_pag_item,0)) then
			ds_observacao_w :=	ds_observacao_w||chr(13)||chr(10)||
						'Pagamento item: '||chr(13)||chr(10)||
						chr(9)||'Anterior: '||:old.nr_seq_pag_item||' - Modificada: '||:new.nr_seq_pag_item||chr(13)||chr(10);
		end if;

		if	(ds_observacao_w is not null) then --Coloquei essa restricao para nao entrar toda a hora no insert
			insert into plsprco_cta 	( 	nr_sequencia, dt_atualizacao, nm_usuario,
								dt_atualizacao_nrec, nm_usuario_nrec, nm_tabela,
								ds_log, ds_log_call, ds_funcao_ativa,
								ie_aplicacao_tasy, nm_maquina, ie_opcao)
					values		( 	plsprco_cta_seq.nextval, sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu'||chr(225)||'rio n'||chr(227)||'o identificado '),1,14),
								sysdate, substr(nvl(wheb_usuario_pck.get_nm_usuario,'Usu'||chr(225)||'rio n'||chr(227)||'o identificado '),1,14), 'PLS_CONTA_MEDICA_RESUMO',
								ds_observacao_w, ds_log_call_w, obter_funcao_ativa,
								'N', wheb_usuario_pck.get_machine, '0');
		end if;
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.pls_conta_medica_resumo_lib
before update of vl_liberado ON TASY.PLS_CONTA_MEDICA_RESUMO for each row
declare

begin

if	(:new.nr_seq_lote_pgto is not null) and (nvl(:old.vl_liberado, 0) <> nvl(:new.vl_liberado, 0)) then
	pls_grava_log_autonomus('pls_conta_medica_resumo_lib', 'Resumo: ' || :new.nr_sequencia || ' Old: ' || :old.vl_liberado || ' New: ' || :new.vl_liberado, nvl(:new.nm_usuario, :old.nm_usuario));
end if;

end;
/


ALTER TABLE TASY.PLS_CONTA_MEDICA_RESUMO ADD (
  CONSTRAINT PLSCOMR_PK
 PRIMARY KEY
 (NR_SEQ_CONTA, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CONTA_MEDICA_RESUMO ADD (
  CONSTRAINT PLSCOMR_PLSEVEN_FK1 
 FOREIGN KEY (NR_SEQ_PP_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_HISPADR_FK3 
 FOREIGN KEY (CD_HISTORICO_PROV) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOMR_HISPADR_FK4 
 FOREIGN KEY (CD_HISTORICO_VL_AJUSTE) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOMR_LOTCONT_FK5 
 FOREIGN KEY (NR_LOTE_CONTABIL_PROV) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSCOMR_PLSESCO_FK4 
 FOREIGN KEY (NR_SEQ_ESQUEMA_PROV) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PPPLOTE_FK 
 FOREIGN KEY (NR_SEQ_PP_LOTE) 
 REFERENCES TASY.PLS_PP_LOTE (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOMR_HISPADR_FK2 
 FOREIGN KEY (CD_HISTORICO_GLOSA) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSCOMR_PLSESCO_FK3 
 FOREIGN KEY (NR_SEQ_ESQUEMA_GLOSA) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL_PAG) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSCOMR_LOTCONT_FK2 
 FOREIGN KEY (NR_LOTE_CONTABIL_REV) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSCOMR_LOTCONT_FK4 
 FOREIGN KEY (NR_LOTE_CONTAB_PAG) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSCOMR_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_CONCONT_FK5 
 FOREIGN KEY (CD_CONTA_PROV_CRED) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOMR_CONCONT_FK6 
 FOREIGN KEY (CD_CONTA_PROV_DEB) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOMR_ANSGRDE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_ANS) 
 REFERENCES TASY.ANS_GRUPO_DESPESA (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PLSCOMAT_FK 
 FOREIGN KEY (NR_SEQ_CONTA, NR_SEQ_CONTA_MAT) 
 REFERENCES TASY.PLS_CONTA_MAT (NR_SEQ_CONTA,NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA, NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQ_CONTA,NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PLSPRPAR_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC, NR_SEQ_CONTA_PROC_PARTIC) 
 REFERENCES TASY.PLS_PROC_PARTICIPANTE (NR_SEQ_CONTA_PROC,NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CRED) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOMR_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_DEB) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOMR_PLSESCO_FK 
 FOREIGN KEY (NR_SEQ_ESQUEMA) 
 REFERENCES TASY.PLS_ESQUEMA_CONTABIL (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_CONCONT_FK3 
 FOREIGN KEY (CD_CONTA_GLOSA_CRED) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOMR_CONCONT_FK4 
 FOREIGN KEY (CD_CONTA_GLOSA_DEB) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSCOMR_PLSPAIT_FK 
 FOREIGN KEY (NR_SEQ_PAG_ITEM) 
 REFERENCES TASY.PLS_PAGAMENTO_ITEM (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PLSPPVT_FK 
 FOREIGN KEY (NR_SEQ_PREST_VENC_TRIB) 
 REFERENCES TASY.PLS_PAG_PREST_VENC_TRIB (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PLSPEPA_FK 
 FOREIGN KEY (NR_SEQ_PERIODO_PGTO) 
 REFERENCES TASY.PLS_PERIODO_PAGAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PLSCOMR_FK 
 FOREIGN KEY (NR_SEQ_PROT_REFERENCIA) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PTUFATU_FK 
 FOREIGN KEY (NR_SEQ_FATURA) 
 REFERENCES TASY.PTU_FATURA (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR_PGTO) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSCOMR_PLSLOPA_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PGTO) 
 REFERENCES TASY.PLS_LOTE_PAGAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CONTA_MEDICA_RESUMO TO NIVEL_1;

