ALTER TABLE TASY.AE_UPDATE_RECORD_CHANGED_FIELD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AE_UPDATE_RECORD_CHANGED_FIELD CASCADE CONSTRAINTS;

CREATE TABLE TASY.AE_UPDATE_RECORD_CHANGED_FIELD
(
  EVENT_ID             VARCHAR2(36 BYTE)        NOT NULL,
  COLUMN_NAME          VARCHAR2(30 BYTE)        NOT NULL,
  COLUMN_DESCRIPTION   VARCHAR2(512 BYTE),
  OLD_VALUE_NUMBER     NUMBER(38,16),
  OLD_VALUE_VARCHAR    VARCHAR2(4000 BYTE),
  OLD_VALUE_TIMESTAMP  TIMESTAMP(6),
  OLD_VALUE_CLOB       CLOB,
  OLD_VALUE_BLOB       BLOB,
  OLD_VALUE_NAME       VARCHAR2(512 BYTE),
  NEW_VALUE_NUMBER     NUMBER(38,16),
  NEW_VALUE_VARCHAR    VARCHAR2(4000 BYTE),
  NEW_VALUE_TIMESTAMP  TIMESTAMP(6),
  NEW_VALUE_CLOB       CLOB,
  NEW_VALUE_BLOB       BLOB,
  NEW_VALUE_NAME       VARCHAR2(512 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (OLD_VALUE_BLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (NEW_VALUE_BLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (OLD_VALUE_CLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (NEW_VALUE_CLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PK_AE_UPDATE_REC_CHANGED_FIELD ON TASY.AE_UPDATE_RECORD_CHANGED_FIELD
(EVENT_ID, COLUMN_NAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AE_UPDATE_RECORD_CHANGED_FIELD ADD (
  CONSTRAINT PK_AE_UPDATE_REC_CHANGED_FIELD
 PRIMARY KEY
 (EVENT_ID, COLUMN_NAME)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AE_UPDATE_RECORD_CHANGED_FIELD TO NIVEL_1;

