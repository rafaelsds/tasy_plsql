ALTER TABLE TASY.PLS_REGRA_COPARTIC_GUIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COPARTIC_GUIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COPARTIC_GUIA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_REGRA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_CONTROLE_INTERNO  NUMBER(10),
  IE_TIPO_ATEND_TISS       VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRGCG_PK ON TASY.PLS_REGRA_COPARTIC_GUIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCG_PLSCCIN_FK_I ON TASY.PLS_REGRA_COPARTIC_GUIA
(NR_SEQ_CONTROLE_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCG_PLSRGCP_FK_I ON TASY.PLS_REGRA_COPARTIC_GUIA
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_copartic_guia_update
before update ON TASY.PLS_REGRA_COPARTIC_GUIA for each row
declare

begin
if	(:new.ie_tipo_atend_tiss <> :old.ie_tipo_atend_tiss) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_tipo_atend_tiss, :new.ie_tipo_atend_tiss, 'PLS_REGRA_COPARTIC_GUIA', 'IE_TIPO_ATEND_TISS', :new.nm_usuario);
end if;
if	(:new.nr_seq_controle_interno <> :old.nr_seq_controle_interno) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_controle_interno, :new.nr_seq_controle_interno, 'PLS_REGRA_COPARTIC_GUIA', 'NR_SEQ_CONTROLE_INTERNO', :new.nm_usuario);
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_COPARTIC_GUIA ADD (
  CONSTRAINT PLSRGCG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COPARTIC_GUIA ADD (
  CONSTRAINT PLSRGCG_PLSCCIN_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE_INTERNO) 
 REFERENCES TASY.PLS_CAD_CONTROLE_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PLSRGCG_PLSRGCP_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_COPARTIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COPARTIC_GUIA TO NIVEL_1;

