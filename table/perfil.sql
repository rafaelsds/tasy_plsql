ALTER TABLE TASY.PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERFIL
(
  CD_PERFIL                    NUMBER(5)        NOT NULL,
  DS_PERFIL                    VARCHAR2(40 BYTE) NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(5),
  IE_SETOR_USUARIO             VARCHAR2(1 BYTE) NOT NULL,
  IE_COMPUTADOR_ESPECIFICO     VARCHAR2(1 BYTE) NOT NULL,
  IE_PERFIL_WEB                VARCHAR2(1 BYTE),
  IE_NAO_VISUALIZA_COMP_ESPEC  VARCHAR2(1 BYTE) NOT NULL,
  CD_SETOR_ATENDIMENTO         NUMBER(5),
  DS_OBSERVACAO                VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_PERFIL_MOBILE             VARCHAR2(1 BYTE),
  IE_ESTRATEGICO               VARCHAR2(1 BYTE),
  DT_IMPORTACAO                DATE,
  DT_ALTERACAO                 DATE,
  IE_PERFIL_GWT                VARCHAR2(1 BYTE),
  IE_LIBERA_USU_AUTO           VARCHAR2(1 BYTE),
  CD_EXP_PERFIL                NUMBER(10),
  IE_PERFIL_DELPHI             VARCHAR2(1 BYTE),
  IE_PERFIL_JAVA               VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO             VARCHAR2(1 BYTE),
  DS_UTC                       VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO             VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO           VARCHAR2(50 BYTE),
  DS_IMPRESSORA_PADRAO         VARCHAR2(255 BYTE) DEFAULT null,
  CD_FUNCAO_INICIAL            NUMBER(5),
  IE_STATUS                    VARCHAR2(2 BYTE),
  DS_ORIGEM                    VARCHAR2(255 BYTE),
  IE_PERFIL_DELEGADO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.PERFIL.CD_PERFIL IS 'Codigo do Perfil';

COMMENT ON COLUMN TASY.PERFIL.DS_PERFIL IS 'Descricao do Perfil';

COMMENT ON COLUMN TASY.PERFIL.IE_SITUACAO IS 'Situacao do Perfil';

COMMENT ON COLUMN TASY.PERFIL.DT_ATUALIZACAO IS 'Data de Atualizacao';

COMMENT ON COLUMN TASY.PERFIL.NM_USUARIO IS 'Nome do Usuario';


CREATE INDEX TASY.PERFIL_DICEXPR_FK_I ON TASY.PERFIL
(CD_EXP_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERFIL_ESTABEL_FK_I ON TASY.PERFIL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PERFIL_PK ON TASY.PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERFIL_SETATEN_FK_I ON TASY.PERFIL
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PERFIL_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PERFIL FOR EACH ROW
DECLARE
BEGIN
	:NEW.DS_UTC_ATUALIZACAO := OBTER_DATA_UTC(SYSDATE,'HV');
	:NEW.DS_UTC := OBTER_DATA_UTC(:NEW.DT_ATUALIZACAO,'HV');
	:NEW.IE_HORARIO_VERAO := OBTER_SE_HORARIO_VERAO(:NEW.DT_ATUALIZACAO);
END;
/


CREATE OR REPLACE TRIGGER TASY.PERFIL_tp  after update ON TASY.PERFIL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_PERFIL);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_LIBERA_USU_AUTO,1,4000),substr(:new.IE_LIBERA_USU_AUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_LIBERA_USU_AUTO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIGEM,1,4000),substr(:new.DS_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIGEM',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PERFIL,1,4000),substr(:new.DS_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_PERFIL',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SETOR_USUARIO,1,4000),substr(:new.IE_SETOR_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SETOR_USUARIO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMPUTADOR_ESPECIFICO,1,4000),substr(:new.IE_COMPUTADOR_ESPECIFICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMPUTADOR_ESPECIFICO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERFIL_WEB,1,4000),substr(:new.IE_PERFIL_WEB,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERFIL_WEB',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EXP_PERFIL,1,4000),substr(:new.CD_EXP_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXP_PERFIL',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERFIL_DELPHI,1,4000),substr(:new.IE_PERFIL_DELPHI,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERFIL_DELPHI',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERFIL_JAVA,1,4000),substr(:new.IE_PERFIL_JAVA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERFIL_JAVA',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NAO_VISUALIZA_COMP_ESPEC,1,4000),substr(:new.IE_NAO_VISUALIZA_COMP_ESPEC,1,4000),:new.nm_usuario,nr_seq_w,'IE_NAO_VISUALIZA_COMP_ESPEC',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERFIL_MOBILE,1,4000),substr(:new.IE_PERFIL_MOBILE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERFIL_MOBILE',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTRATEGICO,1,4000),substr(:new.IE_ESTRATEGICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTRATEGICO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_IMPORTACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_IMPORTACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_IMPORTACAO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ALTERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ALTERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ALTERACAO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERFIL_DELEGADO,1,4000),substr(:new.IE_PERFIL_DELEGADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERFIL_DELEGADO',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERFIL_GWT,1,4000),substr(:new.IE_PERFIL_GWT,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERFIL_GWT',ie_log_w,ds_w,'PERFIL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PERFIL ADD (
  CONSTRAINT PERFIL_PK
 PRIMARY KEY
 (CD_PERFIL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERFIL ADD (
  CONSTRAINT PERFIL_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_PERFIL) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO),
  CONSTRAINT PERFIL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT PERFIL_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.PERFIL TO NIVEL_1;

