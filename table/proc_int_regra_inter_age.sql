ALTER TABLE TASY.PROC_INT_REGRA_INTER_AGE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_INT_REGRA_INTER_AGE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_INT_REGRA_INTER_AGE
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO      NUMBER(10)           NOT NULL,
  QT_IDADE_MIN             NUMBER(10),
  QT_IDADE_MAX             NUMBER(10),
  IE_NECESSITA_INTERNACAO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRINRIA_PK ON TASY.PROC_INT_REGRA_INTER_AGE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRINRIA_PROINTE_FK_I ON TASY.PROC_INT_REGRA_INTER_AGE
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRINRIA_PROINTE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PROC_INT_REGRA_INTER_AGE_tp  after update ON TASY.PROC_INT_REGRA_INTER_AGE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_PROC_INTERNO,1,4000),substr(:new.NR_SEQ_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROC_INTERNO',ie_log_w,ds_w,'PROC_INT_REGRA_INTER_AGE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NECESSITA_INTERNACAO,1,4000),substr(:new.IE_NECESSITA_INTERNACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NECESSITA_INTERNACAO',ie_log_w,ds_w,'PROC_INT_REGRA_INTER_AGE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MAX,1,4000),substr(:new.QT_IDADE_MAX,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MAX',ie_log_w,ds_w,'PROC_INT_REGRA_INTER_AGE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MIN,1,4000),substr(:new.QT_IDADE_MIN,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MIN',ie_log_w,ds_w,'PROC_INT_REGRA_INTER_AGE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PROC_INT_REGRA_INTER_AGE ADD (
  CONSTRAINT PRINRIA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_INT_REGRA_INTER_AGE ADD (
  CONSTRAINT PRINRIA_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROC_INT_REGRA_INTER_AGE TO NIVEL_1;

