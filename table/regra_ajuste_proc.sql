ALTER TABLE TASY.REGRA_AJUSTE_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_AJUSTE_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_AJUSTE_PROC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  CD_CONVENIO                NUMBER(5)          NOT NULL,
  DT_INICIO_VIGENCIA         DATE               NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_CATEGORIA               VARCHAR2(10 BYTE),
  CD_EDICAO_AMB              NUMBER(6),
  CD_AREA_PROCEDIMENTO       NUMBER(15),
  CD_ESPECIALIDADE           NUMBER(15),
  CD_GRUPO_PROC              NUMBER(15),
  CD_PROCEDIMENTO            NUMBER(15),
  IE_ORIGEM_PROCED           NUMBER(10),
  IE_PRECO_INFORMADO         VARCHAR2(1 BYTE),
  VL_PROC_AJUSTADO           NUMBER(15,4),
  IE_GLOSA                   VARCHAR2(1 BYTE),
  CD_PROCEDIMENTO_ESP        VARCHAR2(20 BYTE),
  TX_AJUSTE                  NUMBER(15,4),
  NR_SEQ_REGRA_PRECO         NUMBER(10),
  DT_FINAL_VIGENCIA          DATE,
  CD_TIPO_ACOMODACAO         NUMBER(4),
  IE_TIPO_ATENDIMENTO        NUMBER(3),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  VL_MEDICO                  NUMBER(15,2),
  VL_CUSTO_OPERACIONAL       NUMBER(15,2),
  QT_FILME                   NUMBER(15,4),
  NR_AUXILIARES              NUMBER(2),
  QT_PORTE_ANESTESICO        NUMBER(2),
  TX_AJUSTE_CUSTO_OPER       NUMBER(15,4),
  TX_AJUSTE_MEDICO           NUMBER(15,4),
  TX_AJUSTE_FILME            NUMBER(15,4),
  IE_CREDENCIADO             VARCHAR2(1 BYTE)   NOT NULL,
  PR_GLOSA                   NUMBER(7,4),
  CD_CID                     VARCHAR2(10 BYTE),
  QT_IDADE_MIN               NUMBER(10),
  QT_IDADE_MAX               NUMBER(10),
  NR_SEQ_PROC_INTERNO        NUMBER(10),
  NR_SEQ_EXAME               NUMBER(10),
  CD_PLANO                   VARCHAR2(20 BYTE),
  IE_CLINICA                 NUMBER(15),
  CD_EMPRESA_REF             NUMBER(15),
  CD_MOTIVO_EXC_CONTA        NUMBER(15),
  CD_MEDICO                  VARCHAR2(10 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  TX_AJUSTE_PARTIC           NUMBER(15,4),
  VL_GLOSA                   NUMBER(15,2),
  IE_SEXO                    VARCHAR2(1 BYTE),
  IE_ATEND_RETORNO           VARCHAR2(1 BYTE)   NOT NULL,
  CD_EDICAO_FILTRO           NUMBER(6),
  IE_PRIOR_EDICAO_AJUSTE     VARCHAR2(1 BYTE),
  QT_DIAS_INTER_INICIO       NUMBER(10),
  QT_DIAS_INTER_FINAL        NUMBER(10),
  CD_MUNICIPIO_IBGE_PAC      VARCHAR2(6 BYTE),
  IE_CARATER_INTER_SUS       VARCHAR2(2 BYTE),
  CD_CONVENIO_REF            NUMBER(5),
  CD_CATEGORIA_REF           VARCHAR2(10 BYTE),
  CD_PROCEDIMENTO_REF        NUMBER(15),
  IE_ORIGEM_PROC_REF         NUMBER(10),
  CD_PROCEDENCIA             NUMBER(5),
  IE_PROC_UNICO_SUS          VARCHAR2(1 BYTE),
  VL_MAXPROC_REGRA_SUS       NUMBER(15,4),
  TX_AJUSTE_MATMED           NUMBER(15,4),
  IE_AUTOR_PARTICULAR        VARCHAR2(1 BYTE)   NOT NULL,
  CD_CONVENIO_GLOSA          NUMBER(5),
  CD_CATEGORIA_GLOSA         VARCHAR2(10 BYTE),
  CD_SETOR_ATEND_REF         NUMBER(5),
  IE_UTILIZA_VIDEO           VARCHAR2(1 BYTE),
  CD_TABELA_SERVICO          NUMBER(4),
  NR_SEQ_GRUPO               NUMBER(10),
  NR_SEQ_SUBGRUPO            NUMBER(10),
  NR_SEQ_FORMA_ORG           NUMBER(10),
  IE_IGNORA_CH_EDICAO_AMB    VARCHAR2(1 BYTE),
  VL_ANESTESISTA             NUMBER(15,2),
  VL_AUXILIARES              NUMBER(15,2),
  CD_PROC_REFERENCIA         NUMBER(15),
  IE_ORIGEM_PROC_REFER       NUMBER(10),
  IE_EDICAO_CONVENIO         VARCHAR2(1 BYTE),
  IE_ENTRA_FILME_NEG         VARCHAR2(1 BYTE),
  IE_TIPO_ATEND_BPA          NUMBER(3),
  IE_CONSISTE_PRESCR         VARCHAR2(1 BYTE),
  IE_BEIRA_LEITO             VARCHAR2(1 BYTE),
  IE_SPECT                   VARCHAR2(1 BYTE),
  CD_CGC_PRESTADOR           VARCHAR2(14 BYTE),
  IE_VINCULO_MEDICO          NUMBER(2),
  TX_DESCONTO                NUMBER(15,4),
  TX_ADM                     NUMBER(15,4),
  CD_EQUIPAMENTO             NUMBER(10),
  IE_PROC_QT_ZERO_SUS        VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_ACIDENTE       NUMBER(10),
  IE_GERAR_SEM_PROC_REF_SUS  VARCHAR2(15 BYTE),
  VL_FILME                   NUMBER(15,2),
  CD_ESPECIALIDADE_MEDICA    NUMBER(5),
  NR_SEQ_JUST_VALOR_INF      NUMBER(10),
  NR_SEQ_EQUIPE              NUMBER(10),
  IE_COMPLEXIDADE_SUS        VARCHAR2(2 BYTE),
  NR_SEQ_GRUPO_LAB           NUMBER(10),
  CD_SETOR_ATEND_PRESCR      NUMBER(5),
  NR_SEQ_COBERTURA           NUMBER(10),
  NR_SEQ_CLASSIF             NUMBER(10),
  NR_SEQ_CLASSIFICACAO       NUMBER(10),
  CD_USUARIO_CONVENIO        VARCHAR2(30 BYTE),
  QT_POS_INICIAL             NUMBER(10),
  QT_POS_FINAL               NUMBER(10),
  NR_SEQ_AREA_INT            NUMBER(10),
  NR_SEQ_ESPEC_INT           NUMBER(10),
  NR_SEQ_GRUPO_INT           NUMBER(10),
  NR_SEQ_CBHPM_EDICAO        NUMBER(10),
  IE_CONSISTE_EDICAO_PRIOR   VARCHAR2(1 BYTE),
  CD_MEDICO_RESP             VARCHAR2(10 BYTE),
  IE_TX_EDICAO_AMB_REGRA     VARCHAR2(1 BYTE),
  CD_DEPENDENTE              NUMBER(3),
  NR_SEQ_GRUPO_REC           NUMBER(10),
  NR_SEQ_ORIGEM              NUMBER(10),
  NR_SEQ_LIB_DIETA_CONV      NUMBER(10),
  IE_PACIENTE_DEFICIENTE     VARCHAR2(15 BYTE),
  IE_ESTRANGEIRO             VARCHAR2(10 BYTE),
  NR_SEQ_CLASSIF_MEDICO      NUMBER(10),
  IE_CONSIDER_LEVEL          VARCHAR2(1 BYTE),
  CD_PROCEDIMENTO_LOC        VARCHAR2(20 BYTE),
  NR_SEQ_ESTRUTURA           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          832K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGAJPR_AREPROC_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_ARPRIN_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_AREA_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_ARPRIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_CAIXA_FK2_I ON TASY.REGRA_AJUSTE_PROC
(CD_PROCEDIMENTO_REF, IE_ORIGEM_PROC_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_CAIXA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_CATCONV_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_CATCONV_FK2_I ON TASY.REGRA_AJUSTE_PROC
(CD_CONVENIO_REF, CD_CATEGORIA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_CATCONV_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_CATCONV_FK3_I ON TASY.REGRA_AJUSTE_PROC
(CD_CONVENIO_GLOSA, CD_CATEGORIA_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_CATCONV_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_CIDDOEN_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_CLAATEN_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_CONORUS_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_CONPLAN_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_CONVCOB_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_COBERTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_CONVCOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_CONVENI_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_CONVENI_FK2_I ON TASY.REGRA_AJUSTE_PROC
(CD_CONVENIO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_CONVENI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_EDIAMB_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_EDICAO_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_EDIAMB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_EDIAMB_FK2_I ON TASY.REGRA_AJUSTE_PROC
(CD_EDICAO_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_EDIAMB_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_EMPREFE_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_EMPRESA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_EQUIPAM_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_EQUIPAM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_ESPMEDI_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_ESPECIALIDADE_MEDICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_ESPPRIN_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_ESPEC_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_ESPPRIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_ESPPROC_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_ESTABEL_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_EXALABO_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_GRPRIN_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_GRUPO_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_GRPRIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_GRUEXLA_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_GRUPO_LAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_GRUEXLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_GRUPROC_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_GRURECE_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_GRURECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_I1 ON TASY.REGRA_AJUSTE_PROC
(CD_CONVENIO, CD_ESTABELECIMENTO, IE_EDICAO_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_I2 ON TASY.REGRA_AJUSTE_PROC
(CD_ESTABELECIMENTO, CD_CONVENIO, IE_SITUACAO, IE_EDICAO_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_MEDCLAT_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_CLASSIF_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_MOTEXCO_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_MOTIVO_EXC_CONTA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_MOTEXCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_PESFISI_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_MEDICO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_PESFISI_FK2_I ON TASY.REGRA_AJUSTE_PROC
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_PESJURI_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_CGC_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_PESJURI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_PFEQUIP_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_PFEQUIP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_PIESTTR_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGAJPR_PK ON TASY.REGRA_AJUSTE_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_PROCEDE_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_PROCEDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_PROCEDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_PROCEDI_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_PROCEDI_FK2_I ON TASY.REGRA_AJUSTE_PROC
(CD_PROC_REFERENCIA, IE_ORIGEM_PROC_REFER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_PROCEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_PROINTCLA_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_PROINTCLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_PROINTE_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_REGPRPR_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_REGRA_PRECO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_REGPRPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_SETATEN_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_SETATEN_FK2_I ON TASY.REGRA_AJUSTE_PROC
(CD_SETOR_ATEND_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_SETATEN_FK3_I ON TASY.REGRA_AJUSTE_PROC
(CD_SETOR_ATEND_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_SETATEN_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_SUSFORG_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_FORMA_ORG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_SUSFORG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_SUSGRUP_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_SUSGRUP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_SUSMUNI_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_MUNICIPIO_IBGE_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_SUSSUBG_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_SUBGRUPO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_SUSSUBG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_TABSERV_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_ESTABELECIMENTO, CD_TABELA_SERVICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_TABSERV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_TILIDIC_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_LIB_DIETA_CONV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGAJPR_TIPACID_FK_I ON TASY.REGRA_AJUSTE_PROC
(NR_SEQ_TIPO_ACIDENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_TIPACID_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGAJPR_TIPACOM_FK_I ON TASY.REGRA_AJUSTE_PROC
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGAJPR_TIPACOM_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.regra_ajuste_proc_Atual
BEFORE UPDATE OR INSERT ON TASY.REGRA_AJUSTE_PROC FOR EACH ROW
declare
qt_preenchido_w		Number(01)	:= 0;
qt_reg_w	number(1);
BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.CD_PROCEDIMENTO IS NOT NULL)	then
	begin
	qt_preenchido_w := 1;
	end;
else
	if 	(:new.CD_GRUPO_PROC IS NOT NULL) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if 	(:new.CD_ESPECIALIDADE IS NOT NULL) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if	(:new.CD_AREA_PROCEDIMENTO IS NOT NULL) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if	(:new.NR_SEQ_GRUPO_LAB IS NOT NULL) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if	(:new.NR_SEQ_PROC_INTERNO IS NOT NULL) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	/* Felipe Martini OS85074 - Inclui a validacao para a Estrutura SUS Unificado*/
	if	(:new.nr_seq_grupo is not null) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if	(:new.nr_seq_subgrupo is not null) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if	(:new.nr_seq_forma_org is not null) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if  	(:new.nr_seq_exame is not null) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if  	(:new.nr_seq_grupo_int is not null) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if  	(:new.nr_seq_espec_int is not null) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if  	(:new.nr_seq_area_int is not null) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	if  	(:new.nr_seq_estrutura is not null) then
		qt_preenchido_w := qt_preenchido_w + 1;
	end if;
	/*if	(:new.nr_seq_classif is not null) then
		qt_preenchido_w	:= qt_preenchido_w + 1;
	end if;*/
end if;

/* Ricardo 28/10/2004 - Inclui a linha abaixo, se nao houver procedimento, limpa a origem */

if	(:new.CD_PROCEDIMENTO IS NULL) and
	(:new.ie_origem_proced is not null) then
	:new.ie_origem_proced := null;
end if;

if (qt_preenchido_w > 1) then
	--Apenas uma estrutura pode ser selecionada!
	Wheb_mensagem_pck.exibir_mensagem_abort(185149);
elsif	(qt_preenchido_w = 0) then
	--Uma estrutura deve ser selecionada!
	Wheb_mensagem_pck.exibir_mensagem_abort(185150);
end if;
<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.REGRA_AJUSTE_PROC_tp  after update ON TASY.REGRA_AJUSTE_PROC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATEND_RETORNO,1,4000),substr(:new.IE_ATEND_RETORNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATEND_RETORNO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EDICAO_FILTRO,1,4000),substr(:new.CD_EDICAO_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EDICAO_FILTRO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRIOR_EDICAO_AJUSTE,1,4000),substr(:new.IE_PRIOR_EDICAO_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRIOR_EDICAO_AJUSTE',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GLOSA,1,4000),substr(:new.IE_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_GLOSA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO_INFORMADO,1,4000),substr(:new.IE_PRECO_INFORMADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO_INFORMADO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO_ESP,1,4000),substr(:new.CD_PROCEDIMENTO_ESP,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO_ESP',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PROC_AJUSTADO,1,4000),substr(:new.VL_PROC_AJUSTADO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PROC_AJUSTADO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_PRECO,1,4000),substr(:new.NR_SEQ_REGRA_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_PRECO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EDICAO_AMB,1,4000),substr(:new.CD_EDICAO_AMB,1,4000),:new.nm_usuario,nr_seq_w,'CD_EDICAO_AMB',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA,1,4000),substr(:new.CD_CATEGORIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_AREA_PROCEDIMENTO,1,4000),substr(:new.CD_AREA_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_AREA_PROCEDIMENTO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE,1,4000),substr(:new.TX_AJUSTE,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FINAL_VIGENCIA,1,4000),substr(:new.DT_FINAL_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FINAL_VIGENCIA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_ACOMODACAO,1,4000),substr(:new.CD_TIPO_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_ACOMODACAO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_CUSTO_OPER,1,4000),substr(:new.TX_AJUSTE_CUSTO_OPER,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_CUSTO_OPER',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_MEDICO,1,4000),substr(:new.TX_AJUSTE_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_MEDICO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_FILME,1,4000),substr(:new.TX_AJUSTE_FILME,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_FILME',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MEDICO,1,4000),substr(:new.VL_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MEDICO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CUSTO_OPERACIONAL,1,4000),substr(:new.VL_CUSTO_OPERACIONAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_CUSTO_OPERACIONAL',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_FILME,1,4000),substr(:new.QT_FILME,1,4000),:new.nm_usuario,nr_seq_w,'QT_FILME',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AUXILIARES,1,4000),substr(:new.NR_AUXILIARES,1,4000),:new.nm_usuario,nr_seq_w,'NR_AUXILIARES',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PORTE_ANESTESICO,1,4000),substr(:new.QT_PORTE_ANESTESICO,1,4000),:new.nm_usuario,nr_seq_w,'QT_PORTE_ANESTESICO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CREDENCIADO,1,4000),substr(:new.IE_CREDENCIADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CREDENCIADO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_PROC,1,4000),substr(:new.CD_GRUPO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_GLOSA,1,4000),substr(:new.PR_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'PR_GLOSA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROC_INTERNO,1,4000),substr(:new.NR_SEQ_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROC_INTERNO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MOTIVO_EXC_CONTA,1,4000),substr(:new.CD_MOTIVO_EXC_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_EXC_CONTA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO,1,4000),substr(:new.CD_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_EXAME,1,4000),substr(:new.NR_SEQ_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EXAME',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PLANO,1,4000),substr(:new.CD_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PLANO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLINICA,1,4000),substr(:new.IE_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLINICA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA_REF,1,4000),substr(:new.CD_EMPRESA_REF,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA_REF',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CID,1,4000),substr(:new.CD_CID,1,4000),:new.nm_usuario,nr_seq_w,'CD_CID',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MIN,1,4000),substr(:new.QT_IDADE_MIN,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MIN',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MAX,1,4000),substr(:new.QT_IDADE_MAX,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MAX',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_PARTIC,1,4000),substr(:new.TX_AJUSTE_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_PARTIC',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_GLOSA,1,4000),substr(:new.VL_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'VL_GLOSA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATEND_REF,1,4000),substr(:new.CD_SETOR_ATEND_REF,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATEND_REF',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TABELA_SERVICO,1,4000),substr(:new.CD_TABELA_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TABELA_SERVICO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO,1,4000),substr(:new.NR_SEQ_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_SUBGRUPO,1,4000),substr(:new.NR_SEQ_SUBGRUPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SUBGRUPO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FORMA_ORG,1,4000),substr(:new.NR_SEQ_FORMA_ORG,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FORMA_ORG',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_UTILIZA_VIDEO,1,4000),substr(:new.IE_UTILIZA_VIDEO,1,4000),:new.nm_usuario,nr_seq_w,'IE_UTILIZA_VIDEO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUTOR_PARTICULAR,1,4000),substr(:new.IE_AUTOR_PARTICULAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUTOR_PARTICULAR',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO_GLOSA,1,4000),substr(:new.CD_CONVENIO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO_GLOSA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA_GLOSA,1,4000),substr(:new.CD_CATEGORIA_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA_GLOSA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONVENIO_REF,1,4000),substr(:new.CD_CONVENIO_REF,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO_REF',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CATEGORIA_REF,1,4000),substr(:new.CD_CATEGORIA_REF,1,4000),:new.nm_usuario,nr_seq_w,'CD_CATEGORIA_REF',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROC_UNICO_SUS,1,4000),substr(:new.IE_PROC_UNICO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROC_UNICO_SUS',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDENCIA,1,4000),substr(:new.CD_PROCEDENCIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDENCIA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_INTER_INICIO,1,4000),substr(:new.QT_DIAS_INTER_INICIO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_INTER_INICIO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_INTER_FINAL,1,4000),substr(:new.QT_DIAS_INTER_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_INTER_FINAL',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE_PAC,1,4000),substr(:new.CD_MUNICIPIO_IBGE_PAC,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE_PAC',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CARATER_INTER_SUS,1,4000),substr(:new.IE_CARATER_INTER_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CARATER_INTER_SUS',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MAXPROC_REGRA_SUS,1,4000),substr(:new.VL_MAXPROC_REGRA_SUS,1,4000),:new.nm_usuario,nr_seq_w,'VL_MAXPROC_REGRA_SUS',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO_REF,1,4000),substr(:new.CD_PROCEDIMENTO_REF,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO_REF',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROC_REF,1,4000),substr(:new.IE_ORIGEM_PROC_REF,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROC_REF',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_AJUSTE_MATMED,1,4000),substr(:new.TX_AJUSTE_MATMED,1,4000),:new.nm_usuario,nr_seq_w,'TX_AJUSTE_MATMED',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IGNORA_CH_EDICAO_AMB,1,4000),substr(:new.IE_IGNORA_CH_EDICAO_AMB,1,4000),:new.nm_usuario,nr_seq_w,'IE_IGNORA_CH_EDICAO_AMB',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENTRA_FILME_NEG,1,4000),substr(:new.IE_ENTRA_FILME_NEG,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENTRA_FILME_NEG',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_AUXILIARES,1,4000),substr(:new.VL_AUXILIARES,1,4000),:new.nm_usuario,nr_seq_w,'VL_AUXILIARES',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EDICAO_CONVENIO,1,4000),substr(:new.IE_EDICAO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EDICAO_CONVENIO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ANESTESISTA,1,4000),substr(:new.VL_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_ANESTESISTA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROC_REFERENCIA,1,4000),substr(:new.CD_PROC_REFERENCIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROC_REFERENCIA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROC_REFER,1,4000),substr(:new.IE_ORIGEM_PROC_REFER,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROC_REFER',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ATEND_BPA,1,4000),substr(:new.IE_TIPO_ATEND_BPA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATEND_BPA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_PRESCR,1,4000),substr(:new.IE_CONSISTE_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_PRESCR',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SPECT,1,4000),substr(:new.IE_SPECT,1,4000),:new.nm_usuario,nr_seq_w,'IE_SPECT',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC_PRESTADOR,1,4000),substr(:new.CD_CGC_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC_PRESTADOR',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BEIRA_LEITO,1,4000),substr(:new.IE_BEIRA_LEITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BEIRA_LEITO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VINCULO_MEDICO,1,4000),substr(:new.IE_VINCULO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VINCULO_MEDICO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_DESCONTO,1,4000),substr(:new.TX_DESCONTO,1,4000),:new.nm_usuario,nr_seq_w,'TX_DESCONTO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_ADM,1,4000),substr(:new.TX_ADM,1,4000),:new.nm_usuario,nr_seq_w,'TX_ADM',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EQUIPAMENTO,1,4000),substr(:new.CD_EQUIPAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EQUIPAMENTO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROC_QT_ZERO_SUS,1,4000),substr(:new.IE_PROC_QT_ZERO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROC_QT_ZERO_SUS',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_ACIDENTE,1,4000),substr(:new.NR_SEQ_TIPO_ACIDENTE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_ACIDENTE',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_SEM_PROC_REF_SUS,1,4000),substr(:new.IE_GERAR_SEM_PROC_REF_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_SEM_PROC_REF_SUS',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE_MEDICA,1,4000),substr(:new.CD_ESPECIALIDADE_MEDICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE_MEDICA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FILME,1,4000),substr(:new.VL_FILME,1,4000),:new.nm_usuario,nr_seq_w,'VL_FILME',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_JUST_VALOR_INF,1,4000),substr(:new.NR_SEQ_JUST_VALOR_INF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_JUST_VALOR_INF',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_EQUIPE,1,4000),substr(:new.NR_SEQ_EQUIPE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EQUIPE',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COMPLEXIDADE_SUS,1,4000),substr(:new.IE_COMPLEXIDADE_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_COMPLEXIDADE_SUS',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_LAB,1,4000),substr(:new.NR_SEQ_GRUPO_LAB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_LAB',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_COBERTURA,1,4000),substr(:new.NR_SEQ_COBERTURA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_COBERTURA',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATEND_PRESCR,1,4000),substr(:new.CD_SETOR_ATEND_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATEND_PRESCR',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CBHPM_EDICAO,1,4000),substr(:new.NR_SEQ_CBHPM_EDICAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CBHPM_EDICAO',ie_log_w,ds_w,'REGRA_AJUSTE_PROC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_AJUSTE_PROC ADD (
  CONSTRAINT REGAJPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_AJUSTE_PROC ADD (
  CONSTRAINT REGAJPR_TILIDIC_FK 
 FOREIGN KEY (NR_SEQ_LIB_DIETA_CONV) 
 REFERENCES TASY.TIPO_LIBERACAO_DIETA_CONV (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_MEDCLAT_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_MEDICO) 
 REFERENCES TASY.MEDICO_CLASSIF_ATEND (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_PIESTTR_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PI_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_CONORUS_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.CONVENIO_ORIGEM_USUARIO (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_PROINTCLA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PROC_INTERNO_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_ARPRIN_FK 
 FOREIGN KEY (NR_SEQ_AREA_INT) 
 REFERENCES TASY.AREA_PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_ESPPRIN_FK 
 FOREIGN KEY (NR_SEQ_ESPEC_INT) 
 REFERENCES TASY.ESPECIALIDADE_PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_GRPRIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_INT) 
 REFERENCES TASY.GRUPO_PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REGAJPR_EDIAMB_FK 
 FOREIGN KEY (CD_EDICAO_AMB) 
 REFERENCES TASY.EDICAO_AMB (CD_EDICAO_AMB),
  CONSTRAINT REGAJPR_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATEND_REF) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGAJPR_PESJURI_FK 
 FOREIGN KEY (CD_CGC_PRESTADOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT REGAJPR_EQUIPAM_FK 
 FOREIGN KEY (CD_EQUIPAMENTO) 
 REFERENCES TASY.EQUIPAMENTO (CD_EQUIPAMENTO),
  CONSTRAINT REGAJPR_TIPACID_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACIDENTE) 
 REFERENCES TASY.TIPO_ACIDENTE (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_MEDICA) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT REGAJPR_PFEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.PF_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_EDIAMB_FK2 
 FOREIGN KEY (CD_EDICAO_FILTRO) 
 REFERENCES TASY.EDICAO_AMB (CD_EDICAO_AMB),
  CONSTRAINT REGAJPR_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT REGAJPR_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT REGAJPR_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT REGAJPR_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT REGAJPR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGAJPR_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT REGAJPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT REGAJPR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGAJPR_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT REGAJPR_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT REGAJPR_CIDDOEN_FK 
 FOREIGN KEY (CD_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT REGAJPR_REGPRPR_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PRECO) 
 REFERENCES TASY.REGRA_PRECO_PROC (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT REGAJPR_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_REF) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT REGAJPR_MOTEXCO_FK 
 FOREIGN KEY (CD_MOTIVO_EXC_CONTA) 
 REFERENCES TASY.MOTIVO_EXC_CONTA (CD_MOTIVO_EXC_CONTA),
  CONSTRAINT REGAJPR_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REGAJPR_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE_PAC) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT REGAJPR_CAIXA_FK2 
 FOREIGN KEY (CD_PROCEDIMENTO_REF, IE_ORIGEM_PROC_REF) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT REGAJPR_CATCONV_FK2 
 FOREIGN KEY (CD_CONVENIO_REF, CD_CATEGORIA_REF) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT REGAJPR_CONVENI_FK2 
 FOREIGN KEY (CD_CONVENIO_REF) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT REGAJPR_PROCEDE_FK 
 FOREIGN KEY (CD_PROCEDENCIA) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA),
  CONSTRAINT REGAJPR_CATCONV_FK3 
 FOREIGN KEY (CD_CONVENIO_GLOSA, CD_CATEGORIA_GLOSA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT REGAJPR_SUSSUBG_FK 
 FOREIGN KEY (NR_SEQ_SUBGRUPO) 
 REFERENCES TASY.SUS_SUBGRUPO (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_SUSGRUP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.SUS_GRUPO (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_SUSFORG_FK 
 FOREIGN KEY (NR_SEQ_FORMA_ORG) 
 REFERENCES TASY.SUS_FORMA_ORGANIZACAO (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_PROCEDI_FK2 
 FOREIGN KEY (CD_PROC_REFERENCIA, IE_ORIGEM_PROC_REFER) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT REGAJPR_GRUEXLA_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_LAB) 
 REFERENCES TASY.GRUPO_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT REGAJPR_TABSERV_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO, CD_TABELA_SERVICO) 
 REFERENCES TASY.TABELA_SERVICO (CD_ESTABELECIMENTO,CD_TABELA_SERVICO),
  CONSTRAINT REGAJPR_SETATEN_FK3 
 FOREIGN KEY (CD_SETOR_ATEND_PRESCR) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGAJPR_CONVCOB_FK 
 FOREIGN KEY (NR_SEQ_COBERTURA) 
 REFERENCES TASY.CONVENIO_COBERTURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_AJUSTE_PROC TO NIVEL_1;

GRANT SELECT ON TASY.REGRA_AJUSTE_PROC TO ROBOCMTECNOLOGIA;

