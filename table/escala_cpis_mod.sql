ALTER TABLE TASY.ESCALA_CPIS_MOD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CPIS_MOD CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CPIS_MOD
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_PONTUACAO           NUMBER(3)              NOT NULL,
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SECRECAO_TRAQUEAL   NUMBER(1),
  IE_RX_TORAX            NUMBER(1),
  IE_TEMPERATURA         NUMBER(1),
  IE_LEUCOCITOSE         NUMBER(1),
  IE_PAO2                NUMBER(1),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_SECRECAO_PURULENTA  VARCHAR2(1 BYTE),
  QT_TEMP                NUMBER(4,1),
  QT_LEUCOCITOS          NUMBER(10,1),
  QT_REL_PAO2_FIO2       NUMBER(15,4),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPISM_ATEPACI_FK_I ON TASY.ESCALA_CPIS_MOD
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPISM_PESFISI_FK_I ON TASY.ESCALA_CPIS_MOD
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPISM_PK ON TASY.ESCALA_CPIS_MOD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_CPIS_MOD_UPDATE
BEFORE INSERT OR UPDATE ON TASY.ESCALA_CPIS_MOD FOR EACH ROW
declare

qt_reg_w		number(1);
ie_secrecao_purulenta_w	number(1) := 0;

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(:new.IE_SECRECAO_PURULENTA = 'S') then
	ie_secrecao_purulenta_w := 1;
end if;

:new.qt_pontuacao 	:= nvl(:new.IE_TEMPERATURA,0)
			+  nvl(:new.IE_LEUCOCITOSE,0)
			+  nvl(:new.IE_SECRECAO_TRAQUEAL,0)
			+  ie_secrecao_purulenta_w
			+  nvl(:new.IE_PAO2,0)
			+  nvl(:new.IE_RX_TORAX,0);

<<Final>>
qt_reg_w	:= 0;

END;
/


ALTER TABLE TASY.ESCALA_CPIS_MOD ADD (
  CONSTRAINT ESCPISM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CPIS_MOD ADD (
  CONSTRAINT ESCPISM_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPISM_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_CPIS_MOD TO NIVEL_1;

