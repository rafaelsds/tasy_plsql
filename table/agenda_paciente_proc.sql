ALTER TABLE TASY.AGENDA_PACIENTE_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_PACIENTE_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_PACIENTE_PROC
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_AGENDA            NUMBER(6)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  CD_PROCEDIMENTO          NUMBER(15)           NOT NULL,
  IE_ORIGEM_PROCED         NUMBER(10)           NOT NULL,
  NR_SEQ_PROC_INTERNO      NUMBER(10),
  IE_LADO                  VARCHAR2(1 BYTE),
  CD_MEDICO                VARCHAR2(10 BYTE),
  CD_CONVENIO              NUMBER(5),
  CD_CATEGORIA             VARCHAR2(10 BYTE),
  QT_MIN_PROC              NUMBER(15),
  NR_SEQ_AGENDA_PRINC      NUMBER(10),
  CD_PLANO                 VARCHAR2(10 BYTE),
  IE_AUTORIZACAO           VARCHAR2(3 BYTE),
  CD_AUTORIZACAO           VARCHAR2(20 BYTE),
  CD_MEDICO_REQ            VARCHAR2(10 BYTE),
  CD_PESSOA_INDICACAO      VARCHAR2(10 BYTE),
  DS_INDICACAO_CLINICA     VARCHAR2(255 BYTE),
  CD_PROCEDIMENTO_TUSS     NUMBER(15),
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  QT_PROCEDIMENTO          NUMBER(8,3),
  NR_SEQ_ITEM_ADIC_AGEINT  NUMBER(10),
  DT_CANCELAMENTO          DATE,
  NM_USUARIO_CANCEL        VARCHAR2(15 BYTE),
  IE_COBERTURA_CONV        VARCHAR2(15 BYTE),
  CD_TOPOGRAFIA_PROCED     NUMBER(10),
  CD_EXTERNO               VARCHAR2(32 BYTE),
  DT_AGENDA_EXTERNA        DATE                 DEFAULT null,
  DT_AGENDA_FIM_EXTERNA    DATE                 DEFAULT null,
  NR_ATENDIMENTO           NUMBER(10),
  IE_POSICAO               VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEPAPR_AGITADC_FK_I ON TASY.AGENDA_PACIENTE_PROC
(NR_SEQ_ITEM_ADIC_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAPR_AGITADC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEPAPR_CATCONV_FK_I ON TASY.AGENDA_PACIENTE_PROC
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAPR_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEPAPR_CONPLAN_FK_I ON TASY.AGENDA_PACIENTE_PROC
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAPR_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEPAPR_CONVENI_FK_I ON TASY.AGENDA_PACIENTE_PROC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAPR_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEPAPR_I1 ON TASY.AGENDA_PACIENTE_PROC
(CD_EXTERNO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEPAPR_PESFISI_FK_I ON TASY.AGENDA_PACIENTE_PROC
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEPAPR_PESFISI_FK2_I ON TASY.AGENDA_PACIENTE_PROC
(CD_MEDICO_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEPAPR_PESFISI_FK3_I ON TASY.AGENDA_PACIENTE_PROC
(CD_PESSOA_INDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGEPAPR_PK ON TASY.AGENDA_PACIENTE_PROC
(NR_SEQUENCIA, NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEPAPR_PROCEDI_FK_I ON TASY.AGENDA_PACIENTE_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEPAPR_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEPAPR_PROINTE_FK_I ON TASY.AGENDA_PACIENTE_PROC
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Proced_Adic_Update
after update ON TASY.AGENDA_PACIENTE_PROC for each row
declare
nr_cirurgia_w			number(10,0);
nr_prescricao_w			number(10,0);
ie_alterar_proced_adic_w	varchar2(1);

begin

Obter_Param_Usuario(871,335,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_alterar_proced_adic_w);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  and
	(ie_alterar_proced_adic_w = 'S') then

	select	max(nr_cirurgia)
	into	nr_cirurgia_w
	from	agenda_paciente
	where	nr_sequencia = :old.nr_sequencia;

	select	max(nr_prescricao)
	into	nr_prescricao_w
	from	cirurgia
	where 	nr_cirurgia	=	nr_cirurgia_w;

	if	((:new.nr_seq_proc_interno <> :old.nr_seq_proc_interno) or
		 (:new.cd_procedimento <> :old.cd_procedimento) or
		 (:new.ie_origem_proced <> :old.ie_origem_proced)) then
		update	prescr_procedimento
		set	nr_seq_proc_interno 	= :new.nr_seq_proc_interno,
			cd_procedimento		= :new.cd_procedimento,
			ie_origem_proced	= :new.ie_origem_proced
		where	nr_prescricao		= nr_prescricao_w
		and	nvl(nr_seq_proc_interno,0) 	= nvl(:old.nr_seq_proc_interno,0)
		and	nvl(cd_procedimento,0)		= nvl(:old.cd_procedimento,0)
		and	nvl(ie_origem_proced,0)		= nvl(:old.ie_origem_proced,0);

	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PACIENTE_PROC_ATUAL
BEFORE INSERT OR UPDATE ON TASY.AGENDA_PACIENTE_PROC for each row
declare

cd_estabelecimento_w		number(04,0);
cd_convenio_w			number(5,0);
cd_categoria_w			varchar2(10);
ie_tipo_atendimento_w		number(3,0);
cd_edicao_amb_w                 number(6,0);
vl_m2_filme_w                   number(15,4) := 0;
cd_agenda_w			number(10,0);
nr_seq_proc_autor_w		number(10,0);
nr_atendimento_w		number(10);
cd_setor_atendimento_w		number(5);
cd_pessoa_fisica_w		varchar2(10);
ie_cobertura_conv_w		varchar2(15);
ie_regra_w			Number(5);
cd_plano_w			varchar2(10);
cd_tipo_acomodacao_w	agenda_paciente.cd_tipo_acomodacao%type;
cd_usuario_convenio_w	agenda_paciente.cd_usuario_convenio%type;
ie_carater_cirurgia_w	agenda_paciente.ie_carater_cirurgia%type;
ds_erro_w		varchar2(255);


expressao1_w	varchar2(255) := obter_desc_expressao_idioma(773925, null, wheb_usuario_pck.get_nr_seq_idioma);
expressao2_w	varchar2(255) := obter_desc_expressao_idioma(618484, null, wheb_usuario_pck.get_nr_seq_idioma);

begin

if      (:new.nr_seq_proc_interno is not null) then
	begin
	select	max(b.cd_estabelecimento),
		max(a.cd_convenio),
		max(a.cd_categoria),
		max(a.ie_tipo_atendimento),
		max(a.cd_agenda),
		max(a.nr_atendimento),
		max(a.cd_setor_atendimento),
		max(a.cd_pessoa_fisica),
		max(a.cd_plano),
		max(a.cd_tipo_acomodacao),
		max(a.cd_usuario_convenio),
		max(a.ie_carater_cirurgia)
	into	cd_estabelecimento_w,
		cd_convenio_w,
		cd_categoria_w,
		ie_tipo_atendimento_w,
		cd_agenda_w,
		nr_atendimento_w,
		cd_setor_atendimento_w,
		cd_pessoa_fisica_w,
		cd_plano_w,
		cd_tipo_acomodacao_w,
		cd_usuario_convenio_w,
		ie_carater_cirurgia_w
	from	agenda_paciente a,
		agenda b
	where	a.cd_agenda = b.cd_agenda
	and	a.nr_sequencia = :new.nr_sequencia;

	Obter_Edicao_Proc(cd_estabelecimento_w,
			  nvl(:new.cd_convenio,cd_convenio_w),
			  nvl(:new.cd_categoria,cd_categoria_w),
			  sysdate,
			  :new.cd_procedimento,
			  cd_edicao_amb_w,
			  vl_m2_filme_w);

	:new.cd_procedimento_tuss       :=      Define_procedimento_TUSS(cd_estabelecimento_w,
								   :new.nr_seq_proc_interno,
								   nvl(:new.cd_convenio,cd_convenio_w),
								   nvl(:new.cd_categoria,cd_categoria_w),
								   ie_tipo_atendimento_w,
								   sysdate,
								   :new.cd_procedimento,
								   :new.ie_origem_proced,
								   cd_edicao_amb_w,
								   null,
								   null);
        end;

elsif 	(:old.nr_seq_proc_interno is not null) and
	(:new.nr_seq_proc_interno is null) and
	(:old.cd_procedimento_tuss is not null) and
	(:old.cd_procedimento_tuss = :new.cd_procedimento_tuss)	then

	:new.cd_procedimento_tuss       :=	null;

end if;


if   (((:new.cd_convenio <> :old.cd_convenio) or
       (:old.cd_convenio is null)) or
      ((:new.cd_plano <> :old.cd_plano) or
       (:old.cd_plano is null)) or
      ((:new.cd_categoria <> :old.cd_categoria) or
       (:old.cd_categoria is null)) or
      ((:new.nr_seq_proc_interno is not null) or
      (:new.nr_seq_proc_interno <> :old.nr_seq_proc_interno))) then


		verif_autorizacao_conv_age_pac(	nr_atendimento_w,
						nvl(:new.cd_convenio,cd_convenio_w),
						:new.cd_procedimento,
						:new.ie_origem_proced,
						:new.nr_seq_proc_interno,
						ie_tipo_atendimento_w,
						nvl(:new.cd_plano,cd_plano_w),
						cd_setor_atendimento_w,
						nvl(:new.cd_categoria,cd_categoria_w),
						cd_estabelecimento_w,
						:new.nr_sequencia,
						:new.cd_medico,
						cd_pessoa_fisica_w,
						ie_cobertura_conv_w,
						ie_regra_w,
						ds_erro_w,
						cd_tipo_acomodacao_w,
						cd_usuario_convenio_w,
						ie_carater_cirurgia_w);
		:new.ie_cobertura_conv := ie_cobertura_conv_w;

end if;


if	(updating) and
	(:new.cd_procedimento <> :old.cd_procedimento) then
	atualiza_pedido_kit_agenda(	:new.nr_sequencia,
					:old.cd_procedimento,
					:old.ie_origem_proced,
					:new.cd_procedimento,
					:new.ie_origem_proced,
					:new.nr_seq_proc_interno,
					:new.cd_medico,
					:new.nm_usuario,
					cd_estabelecimento_w,
					cd_agenda_w,
					cd_pessoa_fisica_w);
end if;

if	(updating)and
	(:new.qt_procedimento <> :old.qt_procedimento) then

	begin
	select	a.nr_sequencia
	into	nr_seq_proc_autor_w
	from	procedimento_autorizado a,
		autorizacao_convenio b
	where	a.nr_sequencia_autor 	= b.nr_sequencia
	and	a.nr_seq_agenda		= :new.nr_sequencia
	and	a.nr_seq_agenda_proc 	= :new.nr_seq_agenda
	and	a.cd_procedimento	= :new.cd_procedimento
	and	not exists (	select	1
				from	estagio_autorizacao x
				where	x.nr_sequencia = b.nr_seq_estagio
				and	x.ie_interno = '10'
				and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = x.cd_empresa);
	exception
	when others then
		nr_seq_proc_autor_w := null;
	end;

	if	(nr_seq_proc_autor_w is not null) then

		update	procedimento_autorizado
		set	qt_solicitada = :new.qt_procedimento,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= sysdate,
			ds_observacao	= substr(ds_observacao || chr(13)||chr(10)|| expressao1_w || ' ' || :old.qt_procedimento || ' ' || expressao2_w || ' ' || :new.qt_procedimento,1,2000)
		where	nr_sequencia	= nr_seq_proc_autor_w;

	end if;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_paciente_proc_delete
before delete ON TASY.AGENDA_PACIENTE_PROC for each row
declare
ie_exclui_proc_autor_w	varchar2(5);
cd_estabelecimento_w	number(10);
nr_seq_proc_autor_w	number(15);
nr_seq_autor_w		number(15);
cd_agenda_w		number(10,0);
ds_obs_exclusao_w	varchar2(4000) := substr(wheb_mensagem_pck.get_texto(326971),1,3800);
/*lhalves OS 217619 em 02/06/2010 - excluir o procedimento da autoriza��o se a autoriza��o estiver pendente*/
cd_pessoa_fisica_w		varchar2(10);

cursor c01 is
select	a.nr_sequencia,
	b.nr_sequencia
from	procedimento_autorizado a,
	autorizacao_convenio b,
	estagio_autorizacao c
where	b.nr_seq_agenda		= :old.nr_sequencia
and	b.nr_sequencia		= a.nr_sequencia_autor
and	a.cd_procedimento	= :old.cd_procedimento
and	a.ie_origem_proced	= :old.ie_origem_proced
and	b.nr_seq_estagio	= c.nr_sequencia
and	c.ie_interno		= 1;


cursor c02 is
select	a.nr_sequencia,
	b.nr_sequencia
from	procedimento_autorizado a,
	autorizacao_convenio b,
	estagio_autorizacao c
where	b.nr_seq_agenda		= :old.nr_sequencia
and	b.nr_sequencia		= a.nr_sequencia_autor
and	a.cd_procedimento	= :old.cd_procedimento
and	a.ie_origem_proced	= :old.ie_origem_proced
and	b.nr_seq_estagio	= c.nr_sequencia
and	nvl(c.ie_excluir_proc_adic_agenda,'N') = 'S';


cursor c03 is
select	a.nr_sequencia,
	b.nr_sequencia
from	procedimento_autorizado a,
	autorizacao_convenio b,
	estagio_autorizacao c
where	b.nr_seq_agenda		= :old.nr_sequencia
and	b.nr_sequencia		= a.nr_sequencia_autor
and	a.cd_procedimento	= :old.cd_procedimento
and	a.ie_origem_proced	= :old.ie_origem_proced
and	b.nr_seq_estagio	= c.nr_sequencia;


begin

if	(:old.nr_seq_proc_interno is not null) then

	delete agenda_pac_equip
	where  nr_seq_proc_interno = :old.nr_seq_proc_interno
	and    nr_seq_agenda = :old.nr_sequencia;

	delete agenda_pac_cme
	where  nr_seq_proc_interno = :old.nr_seq_proc_interno
	and    nr_seq_agenda = :old.nr_sequencia;

end if;

select 	a.cd_estabelecimento,
         a.cd_agenda,
         b.cd_pessoa_fisica
into	   cd_estabelecimento_w,
         cd_agenda_w,
         cd_pessoa_fisica_w
from 	agenda a,
	agenda_paciente b
where	b.nr_sequencia		= :old.nr_sequencia
and	b.cd_agenda		= a.cd_agenda;

obter_param_usuario(871,315,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, cd_estabelecimento_w, ie_exclui_proc_autor_w);


if	(nvl(ie_exclui_proc_autor_w, 'N') = 'S') then
	open c01;
	loop
	fetch c01 into
		nr_seq_proc_autor_w,
		nr_seq_autor_w;
	exit when c01%notfound;
		update	procedimento_paciente
		set 	nr_seq_proc_autor	= null
		where	nr_seq_proc_autor	= nr_seq_proc_autor_w;

		update	autorizacao_convenio
		set	nr_seq_agenda_proc	= null
		where	nr_sequencia		= nr_seq_autor_w;

		delete 	procedimento_autorizado
		where	nr_sequencia		= nr_seq_proc_autor_w;
	end loop;
	close c01;
elsif	(nvl(ie_exclui_proc_autor_w, 'N') = 'E') then

	open c02;
	loop
	fetch c02 into
		nr_seq_proc_autor_w,
		nr_seq_autor_w;
	exit when c02%notfound;
		update	procedimento_paciente
		set 	nr_seq_proc_autor	= null
		where	nr_seq_proc_autor	= nr_seq_proc_autor_w;

		update	autorizacao_convenio
		set	nr_seq_agenda_proc	= null
		where	nr_sequencia		= nr_seq_autor_w;

		delete 	procedimento_autorizado
		where	nr_sequencia		= nr_seq_proc_autor_w;
	end loop;
	close c02;
elsif	(nvl(ie_exclui_proc_autor_w, 'N') = 'N') then

	open c03;
	loop
	fetch c03 into
		nr_seq_proc_autor_w,
		nr_seq_autor_w;
	exit when c03%notfound;

		update	autorizacao_convenio
		set	nr_seq_agenda_proc	= null
		where	nr_sequencia		= nr_seq_autor_w;

		update	procedimento_autorizado
		set	nm_usuario 		= nvl(wheb_usuario_pck.get_nm_usuario(),'TASY'),
			dt_atualizacao		= sysdate,
			ds_observacao		= substr(decode(nvl(ds_observacao,'X'),'X',ds_obs_exclusao_w, ds_observacao ||chr(13)||ds_obs_exclusao_w),1,3900)
		where	nr_sequencia		= nr_seq_proc_autor_w;
	end loop;
	close c03;

end if;

atualiza_pedido_kit_agenda(	:old.nr_sequencia,
				:old.cd_procedimento,
				:old.ie_origem_proced,
				null,
				null,
				null,
				null,
				:old.nm_usuario,
				cd_estabelecimento_w,
				cd_agenda_w,
            cd_pessoa_fisica_w);


end;
/


CREATE OR REPLACE TRIGGER TASY.ISCV_AGENDA_PAC_PROC_AFTINSUP
after update or insert ON TASY.AGENDA_PACIENTE_PROC for each row
declare
cd_pessoa_fisica_w			agenda_paciente.cd_pessoa_fisica%type;
cd_setor_atendimento_w		agenda_paciente.cd_setor_atendimento%type;
ds_sep_bv_w					varchar2(100)	:= obter_separador_bv;

	/* Verifica se existe alguma integra��o cadastrada para o dom�nio 17, para certificar que h� o que ser procurado, para a� ent�o executar todas as verifica��es. */
	function permiteIntegrarISCV
	return boolean is
	qt_resultado_w	number;

	begin
	select	count(*) qt_resultado
	into	qt_resultado_w
	from 	regra_proc_interno_integra
	where	ie_tipo_integracao = 17; --Dominio criado para a integra��o

	return qt_resultado_w > 0;
	end;

	function retornaAcaoHistorico(nr_sequencia_p number)
	return varchar is
		ds_retorno_w	agenda_historico_acao.IE_ACAO%type;
	begin
		select	nvl(max(IE_ACAO), ' ')
		into	ds_retorno_w
		from	agenda_historico_acao
		where	nr_Sequencia =  nr_sequencia_p;
	return	ds_retorno_w;
	end;

	function transferidoAgenda(nr_sequencia_p number)
	return boolean is
	ds_retorno_w	agenda_paciente.ie_transferido%type;
	begin

	select	max(ie_transferido),
			max(cd_pessoa_fisica)
	into	ds_retorno_w,
			cd_pessoa_fisica_w
	from	agenda_paciente
	where	nr_sequencia = nr_sequencia_p;

	return ds_retorno_w = 'S';
	end;


begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	permiteIntegrarISCV then
		if	((:old.nr_seq_proc_interno is null) and (:new.nr_seq_proc_interno is not null))
		and	transferidoAgenda(:new.nr_sequencia)
		and	(retornaAcaoHistorico(:new.nr_sequencia) = 'T')
		and	(Obter_Se_Integr_Proc_Interno(:new.nr_seq_proc_interno, 17,null,:new.ie_lado,wheb_usuario_pck.get_cd_estabelecimento) = 'S')
		then
			gravar_agend_integracao(742, 'nr_sequencia=' || :new.nr_sequencia || ds_sep_bv_w || 'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w, cd_setor_atendimento_w);
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PAC_PROC_AFTER_ATUAL
after insert or update ON TASY.AGENDA_PACIENTE_PROC for each row
declare

ie_gerar_consent_w			varchar2(1);
cd_pessoa_fisica_w			AGENDA_PACIENTE.CD_PESSOA_FISICA%TYPE;
cd_tipo_agenda_w			agenda.cd_tipo_agenda%type;
cd_agenda_w					agenda.cd_agenda%type;
cd_estab_agenda_w			agenda.cd_estabelecimento%type;
ie_status_agenda_w			AGENDA_PACIENTE.ie_status_agenda%type;
cd_medico_w					AGENDA_PACIENTE.cd_medico%type;
nr_atendimento_w            agenda_paciente.nr_atendimento%type;

begin
	begin

	SELECT ap.CD_PESSOA_FISICA,
		   ap.CD_AGENDA,
		   a.CD_TIPO_AGENDA,
		   a.CD_ESTABELECIMENTO,
		   ap.ie_status_agenda,
		   ap.cd_medico,
		   ap.nr_atendimento
	  INTO cd_pessoa_fisica_w,
		   cd_agenda_w,
		   cd_tipo_agenda_w,
		   cd_estab_agenda_w,
		   ie_status_agenda_w,
		   cd_medico_w,
		   nr_atendimento_w
	  FROM AGENDA_PACIENTE ap,
		   agenda a
	 WHERE ap.NR_SEQUENCIA = :new.NR_SEQUENCIA
	   and ap.cd_agenda = a.cd_agenda;

	Obter_Param_Usuario(820,463,obter_perfil_ativo,:new.nm_usuario,cd_estab_agenda_w,ie_gerar_consent_w);
	/*
	S - Agendamento ou Proc. Adicional
	N - Nao gera
	A - Somente Agendamento
	P - Somente Proc. Adicional
	*/

	if (cd_tipo_agenda_w = 2 and
		ie_status_agenda_w = 'N' and
		cd_pessoa_fisica_w is not null and
		((:new.cd_procedimento is not null and (:new.cd_procedimento <> :old.cd_procedimento or :new.ie_origem_proced <> :old.ie_origem_proced or :old.cd_procedimento is null)) or
		 (:new.nr_seq_proc_interno is not null and (:old.nr_seq_proc_interno <> :new.nr_seq_proc_interno or :old.nr_Seq_proc_interno is null)) and
		 ie_gerar_consent_w in ('S','P'))) then

			agendas_inserir_consentimentos (
					cd_tipo_agenda_w,
					:new.NR_SEQUENCIA,
					:new.cd_procedimento,
					:new.ie_origem_proced,
					:new.nr_seq_proc_interno,
					cd_pessoa_fisica_w,
					cd_medico_w,
					cd_estab_agenda_w,
					nr_atendimento_w);
	end if;

	exception when others then
		null;
	end;
end;
/


ALTER TABLE TASY.AGENDA_PACIENTE_PROC ADD (
  CONSTRAINT AGEPAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_SEQ_AGENDA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_PACIENTE_PROC ADD (
  CONSTRAINT AGEPAPR_AGITADC_FK 
 FOREIGN KEY (NR_SEQ_ITEM_ADIC_AGEINT) 
 REFERENCES TASY.AGEINT_ITENS_ADICIONAIS (NR_SEQUENCIA),
  CONSTRAINT AGEPAPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGEPAPR_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEPAPR_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT AGEPAPR_PESFISI_FK3 
 FOREIGN KEY (CD_PESSOA_INDICACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEPAPR_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_REQ) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEPAPR_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT AGEPAPR_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AGEPAPR_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_PACIENTE_PROC TO NIVEL_1;

