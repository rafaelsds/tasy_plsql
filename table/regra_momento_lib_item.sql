ALTER TABLE TASY.REGRA_MOMENTO_LIB_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_MOMENTO_LIB_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_MOMENTO_LIB_ITEM
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_MATERIAL           NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  CD_INTERVALO          VARCHAR2(50 BYTE),
  IE_ACM                VARCHAR2(1 BYTE),
  IE_SE_NECESSARIO      VARCHAR2(1 BYTE),
  IE_DOSE_ESPEC         VARCHAR2(1 BYTE),
  IE_AGORA              VARCHAR2(1 BYTE),
  IE_PRECISA_LIBERACAO  VARCHAR2(1 BYTE),
  NR_SEQ_REGRA          NUMBER(10),
  NR_SEQ_PROC_INTERNO   NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGMLIT_INTPRES_FK_I ON TASY.REGRA_MOMENTO_LIB_ITEM
(CD_INTERVALO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMLIT_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGMLIT_MATERIA_FK_I ON TASY.REGRA_MOMENTO_LIB_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMLIT_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGMLIT_PK ON TASY.REGRA_MOMENTO_LIB_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGMLIT_PROINTE_FK_I ON TASY.REGRA_MOMENTO_LIB_ITEM
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMLIT_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGMLIT_REGDILF_FK_I ON TASY.REGRA_MOMENTO_LIB_ITEM
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGMLIT_SETATEN_FK_I ON TASY.REGRA_MOMENTO_LIB_ITEM
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGMLIT_SETATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_MOMENTO_LIB_BEFINSUPD
BEFORE INSERT OR UPDATE ON TASY.REGRA_MOMENTO_LIB_ITEM FOR EACH ROW
DECLARE

ie_tipo_item_w		varchar2(15);

begin

select	nvl(max(ie_tipo_item),'X')
into	ie_tipo_item_w
from	regra_disp_lote_farm
where	nr_sequencia	= :new.nr_seq_regra;

if	(ie_tipo_item_w			<> 'IA') and
	(:new.nr_seq_proc_interno	is not null) then
	Wheb_mensagem_pck.exibir_mensagem_abort(192957);
end if;

END;
/


ALTER TABLE TASY.REGRA_MOMENTO_LIB_ITEM ADD (
  CONSTRAINT REGMLIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_MOMENTO_LIB_ITEM ADD (
  CONSTRAINT REGMLIT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT REGMLIT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGMLIT_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT REGMLIT_REGDILF_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_DISP_LOTE_FARM (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REGMLIT_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_MOMENTO_LIB_ITEM TO NIVEL_1;

