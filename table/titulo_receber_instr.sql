DROP TABLE TASY.TITULO_RECEBER_INSTR CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_RECEBER_INSTR
(
  IE_SELECIONADO             VARCHAR2(1 BYTE)   NOT NULL,
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_COBRANCA            NUMBER(10),
  NR_TITULO                  NUMBER(10)         NOT NULL,
  VL_COBRANCA                NUMBER(15,2)       NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  CD_BANCO                   NUMBER(5),
  CD_AGENCIA_BANCARIA        VARCHAR2(8 BYTE),
  NR_CONTA                   VARCHAR2(20 BYTE),
  CD_MOEDA                   NUMBER(2),
  DT_LIQUIDACAO              DATE,
  VL_LIQUIDACAO              NUMBER(15,2),
  IE_DIGITO_CONTA            VARCHAR2(2 BYTE),
  CD_CAMARA_COMPENSACAO      NUMBER(3),
  VL_DESCONTO                NUMBER(15,2),
  VL_ACRESCIMO               NUMBER(15,2),
  CD_OCORRENCIA              VARCHAR2(3 BYTE),
  CD_INSTRUCAO               VARCHAR2(5 BYTE),
  QT_DIAS_INSTRUCAO          NUMBER(3),
  NR_SEQ_OCORRENCIA_RET      NUMBER(10),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  VL_DESPESA_BANCARIA        NUMBER(15,2),
  IE_INSTRUCAO_ENVIADA       VARCHAR2(1 BYTE),
  VL_JUROS                   NUMBER(15,2),
  VL_MULTA                   NUMBER(15,2),
  VL_DESC_PREVISTO           NUMBER(15,2),
  NR_SEQ_MENSALIDADE         NUMBER(10),
  CD_CENTRO_CUSTO_DESC       NUMBER(8),
  NR_SEQ_MOTIVO_DESC         NUMBER(10),
  NR_SEQ_OCORR_MOTIVO        NUMBER(10),
  VL_SALDO_INCLUSAO          NUMBER(15,2),
  IE_VERIFICACAO             VARCHAR2(1 BYTE),
  DS_INCONSISTENCIA          VARCHAR2(4000 BYTE),
  CD_PESSOA_FISICA_R         VARCHAR2(10 BYTE),
  CD_CGC_R                   VARCHAR2(14 BYTE),
  NM_PESSOA_R                VARCHAR2(80 BYTE),
  NR_SEQ_TRANS_FINANC        NUMBER(10),
  NR_DOCUMENTO_R             VARCHAR2(30 BYTE),
  VL_TITULO_R                NUMBER(15,2),
  DS_ESTABELECIMENTO_R       VARCHAR2(80 BYTE),
  VL_JUROS_CALC              NUMBER(15,2),
  VL_MULTA_CALC              NUMBER(15,2),
  DT_PAGAMENTO_REAL          DATE,
  NR_SEQ_CONTA_BANCO_ANTIGA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TITRECINST_BANCO_FK_I ON TASY.TITULO_RECEBER_INSTR
(CD_BANCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_BANESTA_FK_I ON TASY.TITULO_RECEBER_INSTR
(NR_SEQ_CONTA_BANCO_ANTIGA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_BANOCES_FK_I ON TASY.TITULO_RECEBER_INSTR
(CD_BANCO, CD_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_BOCMERE_FK_I ON TASY.TITULO_RECEBER_INSTR
(NR_SEQ_OCORR_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_BOESRET_FK_I ON TASY.TITULO_RECEBER_INSTR
(NR_SEQ_OCORRENCIA_RET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_CENCUST_FK_I ON TASY.TITULO_RECEBER_INSTR
(CD_CENTRO_CUSTO_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_COBESCR_FK_I ON TASY.TITULO_RECEBER_INSTR
(NR_SEQ_COBRANCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_INSESCR_FK_I ON TASY.TITULO_RECEBER_INSTR
(CD_BANCO, CD_INSTRUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_MOTDESC_FK_I ON TASY.TITULO_RECEBER_INSTR
(NR_SEQ_MOTIVO_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_PLSMENS_FK_I ON TASY.TITULO_RECEBER_INSTR
(NR_SEQ_MENSALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_TITRECE_FK_I ON TASY.TITULO_RECEBER_INSTR
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TITRECINST_TRAFINA_FK_I ON TASY.TITULO_RECEBER_INSTR
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TITULO_RECEBER_INSTR ADD (
  CONSTRAINT TITRECINST_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA_BANCO_ANTIGA) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA),
  CONSTRAINT TITRECINST_BANCO_FK 
 FOREIGN KEY (CD_BANCO) 
 REFERENCES TASY.BANCO (CD_BANCO),
  CONSTRAINT TITRECINST_BANOCES_FK 
 FOREIGN KEY (CD_BANCO, CD_OCORRENCIA) 
 REFERENCES TASY.BANCO_OCORRENCIA_ESCRITURAL (CD_BANCO,CD_OCORRENCIA),
  CONSTRAINT TITRECINST_BOCMERE_FK 
 FOREIGN KEY (NR_SEQ_OCORR_MOTIVO) 
 REFERENCES TASY.BANCO_OCORR_MOTIVO_RET (NR_SEQUENCIA),
  CONSTRAINT TITRECINST_BOESRET_FK 
 FOREIGN KEY (NR_SEQ_OCORRENCIA_RET) 
 REFERENCES TASY.BANCO_OCORR_ESCRIT_RET (NR_SEQUENCIA),
  CONSTRAINT TITRECINST_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO_DESC) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT TITRECINST_COBESCR_FK 
 FOREIGN KEY (NR_SEQ_COBRANCA) 
 REFERENCES TASY.COBRANCA_ESCRITURAL (NR_SEQUENCIA),
  CONSTRAINT TITRECINST_INSESCR_FK 
 FOREIGN KEY (CD_BANCO, CD_INSTRUCAO) 
 REFERENCES TASY.INSTRUCAO_ESCRITURAL (CD_BANCO,CD_INSTRUCAO),
  CONSTRAINT TITRECINST_MOTDESC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DESC) 
 REFERENCES TASY.MOTIVO_DESCONTO (NR_SEQUENCIA),
  CONSTRAINT TITRECINST_PLSMENS_FK 
 FOREIGN KEY (NR_SEQ_MENSALIDADE) 
 REFERENCES TASY.PLS_MENSALIDADE (NR_SEQUENCIA),
  CONSTRAINT TITRECINST_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT TITRECINST_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TITULO_RECEBER_INSTR TO NIVEL_1;

