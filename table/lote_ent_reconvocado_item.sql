ALTER TABLE TASY.LOTE_ENT_RECONVOCADO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_ENT_RECONVOCADO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_ENT_RECONVOCADO_ITEM
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_RECONVOCADO    NUMBER(10)              NOT NULL,
  NR_SEQ_EXAME          NUMBER(10)              NOT NULL,
  CD_MATERIAL_EXAME     VARCHAR2(20 BYTE),
  NR_PRESCRICAO         NUMBER(14),
  NR_SEQ_PRESCR         NUMBER(10),
  DT_PREVISTA           DATE,
  IE_TIPO_BUSCA         VARCHAR2(1 BYTE),
  NR_SEQ_STATUS_RECONV  NUMBER(10),
  IE_URGENCIA           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LEENRECIT_EXALABO_FK_I ON TASY.LOTE_ENT_RECONVOCADO_ITEM
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEENRECIT_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LEENRECIT_LEENREC_FK_I ON TASY.LOTE_ENT_RECONVOCADO_ITEM
(NR_SEQ_RECONVOCADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEENRECIT_LEENREC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LEENRECIT_LESTRECO_FK_I ON TASY.LOTE_ENT_RECONVOCADO_ITEM
(NR_SEQ_STATUS_RECONV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEENRECIT_LESTRECO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LEENRECIT_MATEXLA_FK_I ON TASY.LOTE_ENT_RECONVOCADO_ITEM
(CD_MATERIAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEENRECIT_MATEXLA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LEENRECIT_PK ON TASY.LOTE_ENT_RECONVOCADO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEENRECIT_PK
  MONITORING USAGE;


CREATE INDEX TASY.LEENRECIT_PRESPRO_FK_I ON TASY.LOTE_ENT_RECONVOCADO_ITEM
(NR_PRESCRICAO, NR_SEQ_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LEENRECIT_PRESPRO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.lote_ent_rec_item_upd_ins
before insert or update
ON TASY.LOTE_ENT_RECONVOCADO_ITEM 
for each row
declare

break varchar2(2):= chr(13) || chr(10);

begin

    if :new.ie_tipo_busca is null then

        gravar_log_lab_pragma(
            cd_log_p => 55,
             ds_log_p => dbms_utility.format_call_stack || break || break ||
                 'nr_sequencia : '           || nvl(to_char(:new.nr_sequencia), 'null') || break ||
                 'dt_atualizacao : '         || nvl(to_char(:new.dt_atualizacao, 'ddmmyyyy hh24miss'), 'null') || break ||
                 'nm_usuario : '             || nvl(to_char(:new.nm_usuario), 'null') || break ||
                 'dt_atualizacao_nrec : '    || nvl(to_char(:new.dt_atualizacao_nrec, 'ddmmyyyy hh24miss'), 'null') || break ||
                 'nm_usuario_nrec : '        || nvl(to_char(:new.nm_usuario_nrec), 'null') || break ||
                 'nr_seq_reconvocado : '     || nvl(to_char(:new.nr_seq_reconvocado), 'null') || break ||
                 'nr_seq_exame : '           || nvl(to_char(:new.nr_seq_exame), 'null') || break ||
                 'cd_material_exame : '      || nvl(to_char(:new.cd_material_exame), 'null') || break ||
                 'nr_prescricao : '          || nvl(to_char(:new.nr_prescricao), 'null') || break ||
                 'nr_seq_prescr : '          || nvl(to_char(:new.nr_seq_prescr), 'null') || break ||
                 'dt_prevista : '            || nvl(to_char(:new.dt_prevista, 'ddmmyyyy hh24miss'), 'null') || break ||
                 'ie_tipo_busca : '          || nvl(to_char(:new.ie_tipo_busca), 'null') || break ||
                 'nr_seq_status_reconv : '   || nvl(to_char(:new.nr_seq_status_reconv), 'null') || break ||
                 'ie_urgencia : '            || nvl(to_char(:new.ie_urgencia), 'null'),
            nm_usuario_p => nvl(:new.nm_usuario, wheb_usuario_pck.get_nm_usuario()),
            nr_prescricao_p => nvl(:new.nr_prescricao, 55),
            ds_integracao_p => nvl(:new.nr_seq_prescr, 55)
        );

        -- Nao foi possivel identificar o tipo de busca, por favor contate o suporte tecnico.
        wheb_mensagem_pck.exibir_mensagem_abort(1108405);

    end if;

end;
/


ALTER TABLE TASY.LOTE_ENT_RECONVOCADO_ITEM ADD (
  CONSTRAINT LEENRECIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_ENT_RECONVOCADO_ITEM ADD (
  CONSTRAINT LEENRECIT_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT LEENRECIT_LEENREC_FK 
 FOREIGN KEY (NR_SEQ_RECONVOCADO) 
 REFERENCES TASY.LOTE_ENT_RECONVOCADO (NR_SEQUENCIA),
  CONSTRAINT LEENRECIT_MATEXLA_FK 
 FOREIGN KEY (CD_MATERIAL_EXAME) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (CD_MATERIAL_EXAME),
  CONSTRAINT LEENRECIT_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCR) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT LEENRECIT_LESTRECO_FK 
 FOREIGN KEY (NR_SEQ_STATUS_RECONV) 
 REFERENCES TASY.LOTE_ENT_STATUS_RECONV (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOTE_ENT_RECONVOCADO_ITEM TO NIVEL_1;

