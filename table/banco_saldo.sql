ALTER TABLE TASY.BANCO_SALDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BANCO_SALDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.BANCO_SALDO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_CONTA           NUMBER(10)             NOT NULL,
  DT_REFERENCIA          DATE                   NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  VL_SALDO               NUMBER(15,2)           NOT NULL,
  DT_FECHAMENTO          DATE,
  NM_USUARIO_FECHAMENTO  VARCHAR2(15 BYTE),
  VL_SALDO_ESTRANG       NUMBER(15,2),
  VL_COTACAO             NUMBER(21,10),
  CD_MOEDA               NUMBER(5),
  DS_STACK               VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BANSALD_BANESTA_FK_I ON TASY.BANCO_SALDO
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BANSALD_MOEDA_FK_I ON TASY.BANCO_SALDO
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BANSALD_PK ON TASY.BANCO_SALDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BANSALD_UK ON TASY.BANCO_SALDO
(NR_SEQ_CONTA, DT_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.banco_saldo_atual
before insert or update ON TASY.BANCO_SALDO 
for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	if	(:new.dt_referencia <> :old.dt_referencia) and
		(:new.DT_REFERENCIA < to_date('01/01/1980','dd/mm/yyyy')) then
		/* Nao e possivel gerar um saldo com data de :new.DT_REFERENCIA */
		wheb_mensagem_pck.exibir_mensagem_abort(262162,'DT_REFERENCIA_W='||to_char(:new.DT_REFERENCIA, 'dd/mm/yyyy'));
	end if;

	if (inserting) then
		:new.DS_STACK := substr('CallStack Insert: ' || substr(dbms_utility.format_call_stack,1,3980),1,4000);
	end if;
	
	:new.dt_referencia := trunc(:new.dt_referencia, 'dd');
	
end if;

end;
/


ALTER TABLE TASY.BANCO_SALDO ADD (
  CONSTRAINT BANSALD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT BANSALD_UK
 UNIQUE (NR_SEQ_CONTA, DT_REFERENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BANCO_SALDO ADD (
  CONSTRAINT BANSALD_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT BANSALD_BANESTA_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.BANCO_ESTABELECIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.BANCO_SALDO TO NIVEL_1;

