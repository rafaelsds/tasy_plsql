ALTER TABLE TASY.REGRA_LOCAL_COMPRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_LOCAL_COMPRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_LOCAL_COMPRA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_LOCAL_ESTOQUE      NUMBER(4)               NOT NULL,
  CD_MATERIAL           NUMBER(10),
  CD_CLASSE_MATERIAL    NUMBER(5),
  CD_SUBGRUPO_MATERIAL  NUMBER(5),
  CD_GRUPO_MATERIAL     NUMBER(5),
  IE_ORDEM_COMPRA       VARCHAR2(1 BYTE)        NOT NULL,
  IE_CONTROLADO         VARCHAR2(1 BYTE),
  CD_UNIDADE_MEDIDA     VARCHAR2(30 BYTE),
  IE_PADRONIZADO        VARCHAR2(1 BYTE),
  CD_CENTRO_CUSTO       NUMBER(8),
  CD_LOCAL_ENTREGA      NUMBER(4),
  CD_PERFIL             NUMBER(5),
  IE_NOTA_FISCAL        VARCHAR2(1 BYTE)        NOT NULL,
  IE_TIPO_ORDEM         VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELOCOM_CENCUST_FK_I ON TASY.REGRA_LOCAL_COMPRA
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          328K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELOCOM_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELOCOM_CLAMATE_FK_I ON TASY.REGRA_LOCAL_COMPRA
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELOCOM_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELOCOM_ESTABEL_FK_I ON TASY.REGRA_LOCAL_COMPRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELOCOM_GRUMATE_FK_I ON TASY.REGRA_LOCAL_COMPRA
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELOCOM_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELOCOM_LOCESTO_FK_I ON TASY.REGRA_LOCAL_COMPRA
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELOCOM_LOCESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELOCOM_LOCESTO_FK1_I ON TASY.REGRA_LOCAL_COMPRA
(CD_LOCAL_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELOCOM_LOCESTO_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.RELOCOM_MATERIA_FK_I ON TASY.REGRA_LOCAL_COMPRA
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELOCOM_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RELOCOM_PERFIL_FK_I ON TASY.REGRA_LOCAL_COMPRA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELOCOM_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RELOCOM_PK ON TASY.REGRA_LOCAL_COMPRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELOCOM_SUBMATE_FK_I ON TASY.REGRA_LOCAL_COMPRA
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELOCOM_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_LOCAL_COMPRA_tp  after update ON TASY.REGRA_LOCAL_COMPRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_NOTA_FISCAL,1,4000),substr(:new.IE_NOTA_FISCAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOTA_FISCAL',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORDEM_COMPRA,1,4000),substr(:new.IE_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORDEM_COMPRA',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ESTOQUE,1,4000),substr(:new.CD_LOCAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ESTOQUE',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SUBGRUPO_MATERIAL,1,4000),substr(:new.CD_SUBGRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_MATERIAL',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ENTREGA,1,4000),substr(:new.CD_LOCAL_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ENTREGA',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MATERIAL,1,4000),substr(:new.CD_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MATERIAL',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROLADO,1,4000),substr(:new.IE_CONTROLADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROLADO',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA,1,4000),substr(:new.CD_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRONIZADO,1,4000),substr(:new.IE_PADRONIZADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRONIZADO',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CENTRO_CUSTO,1,4000),substr(:new.CD_CENTRO_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CENTRO_CUSTO',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_MATERIAL,1,4000),substr(:new.CD_GRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_MATERIAL',ie_log_w,ds_w,'REGRA_LOCAL_COMPRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_LOCAL_COMPRA ADD (
  CONSTRAINT RELOCOM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_LOCAL_COMPRA ADD (
  CONSTRAINT RELOCOM_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT RELOCOM_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT RELOCOM_LOCESTO_FK1 
 FOREIGN KEY (CD_LOCAL_ENTREGA) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT RELOCOM_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT RELOCOM_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT RELOCOM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RELOCOM_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT RELOCOM_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT RELOCOM_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE));

GRANT SELECT ON TASY.REGRA_LOCAL_COMPRA TO NIVEL_1;

