ALTER TABLE TASY.CONVENIO_RETORNO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_RETORNO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_RETORNO_ITEM
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_RETORNO         NUMBER(10)             NOT NULL,
  VL_PAGO                NUMBER(15,2),
  VL_GLOSADO             NUMBER(15,2),
  VL_ADICIONAL           NUMBER(15,2),
  VL_AMENOR              NUMBER(15,2),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  VL_ADEQUADO            NUMBER(15,2),
  IE_GLOSA               VARCHAR2(1 BYTE),
  NR_INTERNO_CONTA       NUMBER(10),
  NR_TITULO              NUMBER(10),
  CD_AUTORIZACAO         VARCHAR2(20 BYTE)      NOT NULL,
  CD_MOTIVO_GLOSA        NUMBER(5),
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  IE_LIBERA_REPASSE      VARCHAR2(5 BYTE),
  IE_ANALISADA           VARCHAR2(1 BYTE),
  VL_DESCONTO            NUMBER(15,2),
  CD_AUTORIZACAO_CONV    VARCHAR2(160 BYTE),
  CD_CENTRO_CUSTO_DESC   NUMBER(8),
  NR_SEQ_MOTIVO_DESC     NUMBER(10),
  VL_PERDAS              NUMBER(15,2),
  VL_GUIA                NUMBER(15,2),
  IE_AUTORIZACAO         VARCHAR2(1 BYTE),
  VL_AMENOR_POST         NUMBER(15,2),
  VL_GLOSADO_POST        NUMBER(15,2),
  NR_SEQ_RET_ITEM_ORIG   NUMBER(10),
  NR_SEQ_RECEB           NUMBER(10),
  VL_JUROS_COBR          NUMBER(15,2),
  VL_MULTA_COBR          NUMBER(15,2),
  VL_NOTA_CREDITO        NUMBER(15,2),
  DT_ANALISADA           DATE,
  VL_JUROS               NUMBER(15,2),
  VL_CAMBIAL_ATIVO       NUMBER(15,2),
  VL_CAMBIAL_PASSIVO     NUMBER(15,2),
  NR_DEMONST_PAGTO       VARCHAR2(255 BYTE),
  NR_SEQ_RET_ITEM_EST    NUMBER(10),
  NR_SEQ_TIPO_PEND       NUMBER(10),
  VL_TRIBUTO_GUIA        NUMBER(15,2),
  DT_INTEGRACAO          DATE,
  IE_DOC_RETORNO_ORIGEM  VARCHAR2(15 BYTE),
  VL_COPARTICIPACAO      NUMBER(15,2),
  VL_SINISTRO            NUMBER(15,2),
  DT_PAGAMENTO           DATE,
  DS_VALORES_ORIGINAIS   VARCHAR2(2000 BYTE),
  IE_TIPO_PAGAMENTO      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONREIT_CENCUST_FK_I ON TASY.CONVENIO_RETORNO_ITEM
(CD_CENTRO_CUSTO_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREIT_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREIT_CONPACI_FK_I ON TASY.CONVENIO_RETORNO_ITEM
(NR_INTERNO_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREIT_CONRECE_FK_I ON TASY.CONVENIO_RETORNO_ITEM
(NR_SEQ_RECEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREIT_CONRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREIT_CONREIT_FK_I ON TASY.CONVENIO_RETORNO_ITEM
(NR_SEQ_RET_ITEM_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREIT_CONREIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREIT_CONRETO_FK_I ON TASY.CONVENIO_RETORNO_ITEM
(NR_SEQ_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREIT_MOGLOSA_FK_I ON TASY.CONVENIO_RETORNO_ITEM
(CD_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREIT_MOGLOSA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREIT_MOTDESC_FK_I ON TASY.CONVENIO_RETORNO_ITEM
(NR_SEQ_MOTIVO_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREIT_MOTDESC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONREIT_PK ON TASY.CONVENIO_RETORNO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREIT_TITRECE_FK_I ON TASY.CONVENIO_RETORNO_ITEM
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREIT_TPENCORI_FK_I ON TASY.CONVENIO_RETORNO_ITEM
(NR_SEQ_TIPO_PEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Convenio_Retorno_Item_Delete
BEFORE DELETE ON TASY.CONVENIO_RETORNO_ITEM FOR EACH ROW
DECLARE
cont_proc_w		number(10,0) := 0;
cont_mat_w		number(10,0) := 0;
ie_conv_lib_rep_w	varchar2(1)  := 'S';

BEGIN

Obter_Param_Usuario(27,255, obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.Get_cd_estabelecimento, ie_conv_lib_rep_w);

if	(ie_conv_lib_rep_w = 'N') then

	select	count(*)
	into	cont_mat_w
	from	material_repasse a,
		material_atend_paciente b
	where	a.nr_seq_material	= b.nr_sequencia
	and	b.nr_interno_conta	= :old.nr_interno_conta
	and	nvl(b.nr_doc_convenio,'N�o Informada')	= :old.cd_autorizacao
	and	a.ie_status		in ('R','S','L');

	select	count(*)
	into	cont_proc_w
	from	procedimento_repasse a,
		procedimento_paciente b
	where	a.nr_seq_procedimento	= b.nr_sequencia
	and	b.nr_interno_conta	= :old.nr_interno_conta
	and	nvl(b.nr_doc_convenio,'N�o Informada')	= :old.cd_autorizacao
	and	a.ie_status		in ('R','S','L');

	if	(cont_proc_w > 0 or cont_mat_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(238170,	'NR_INTERNO_CONTA_W='||:old.NR_INTERNO_CONTA||
								';CD_AUTORIZACAO_W='||:old.CD_AUTORIZACAO);
	end if;
end if;

update convenio_retorno_movto
set nr_seq_ret_item = null
where nr_seq_ret_item = :old.nr_sequencia;

END;
/


CREATE OR REPLACE TRIGGER TASY.Convenio_Retorno_Item_Update
BEFORE UPDATE ON TASY.CONVENIO_RETORNO_ITEM FOR EACH ROW
DECLARE
vl_guia_w		number(15,2);
cont_w			number(10,0);
cd_conv_ret_w		number(10,0);
cd_conv_par_w		number(10,0);
ie_conv_dif_w		varchar2(255);
vl_partic_w		number(15,2);
BEGIN

select	count(*)
into	cont_w
from	conta_paciente_guia
where	nr_interno_conta	= :new.nr_interno_conta
and	cd_autorizacao		= :new.cd_autorizacao;

if	(cont_w > 0) then
	select	nvl(sum(vl_guia),0),
		nvl(sum(vl_participante),0)
	into	vl_guia_w,
		vl_partic_w
	from	conta_paciente_guia
	where	nr_interno_conta	= :new.nr_interno_conta
	and	cd_autorizacao		= :new.cd_autorizacao;

	if	(vl_guia_w = 0) and
		(vl_partic_w > 0) then
		vl_guia_w := vl_partic_w;
	end if;

	if	(:new.vl_pago >= 0) and
		(:new.vl_amenor > 0) and
		(:new.vl_amenor > vl_guia_w) and
		(:new.vl_adicional >= 0) then
		/* O valor a menor da guia n�o pode ser maior que o valor da guia na conta paciente!
		Conta: #@NR_INTERNO_CONTA#@
		Guia: #@CD_AUTORIZACAO#@
		Vl guia: #@VL_GUIA#@
		Vl a menor: #@VL_AMENOR#@ */
		wheb_mensagem_pck.exibir_mensagem_abort(266931, 'NR_INTERNO_CONTA=' || :new.nr_interno_conta || ';' ||
								'CD_AUTORIZACAO=' || :new.cd_autorizacao || ';'||
								'VL_GUIA=' || vl_guia_w || ';' ||
								'VL_AMENOR=' || :new.vl_amenor);
	end if;
end if;

Obter_Param_Usuario(27,70, obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, 0, ie_conv_dif_w);

if	(nvl(ie_conv_dif_w, 'N') = 'N') then

	select	max(cd_convenio)
	into	cd_conv_ret_w
	from	convenio_retorno
	where	nr_sequencia		= :new.nr_seq_retorno;

	select	max(cd_convenio_parametro)
	into	cd_conv_par_w
	from	conta_paciente
	where	nr_interno_conta	= :new.nr_interno_conta;

	if	(cd_conv_ret_w <> cd_conv_par_w) then
		-- O conv�nio da conta n�o pode ser diferente do conv�nio do retorno!
		wheb_mensagem_pck.exibir_mensagem_abort(266932, 'NR_INTERNO_CONTA_W=' || :new.nr_interno_conta);
	end if;

end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.Convenio_Retorno_Item_Insert
BEFORE INSERT ON TASY.CONVENIO_RETORNO_ITEM FOR EACH ROW
DECLARE
IE_STATUS_RETORNO_w	varchar2(100);
vl_guia_w		number(15,2);
cont_w			number(10,0);
ie_conv_dif_w		varchar2(255);
cd_conv_par_w		number(10,0);
cd_conv_ret_w		number(10,0);
ie_perm_conta_sem_titulo_w	varchar2(5) := 'S';
ie_perm_lancar_partic_w	varchar2(5) := 'n';
ie_bloq_lancto_guia_anl_grg_w	varchar2(1);
nr_titulo_w		number(10,0);
ie_vinc_adiant_prot_pj_w varchar2(1);
nr_adiantamento_w	conta_paciente_adiant.nr_adiantamento%type;
BEGIN

Obter_Param_Usuario(27,226, obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.Get_cd_estabelecimento, ie_perm_conta_sem_titulo_w);
Obter_Param_Usuario(27,53, obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.Get_cd_estabelecimento, ie_perm_lancar_partic_w);

select	max(IE_STATUS_RETORNO)
into	IE_STATUS_RETORNO_w
from	convenio_retorno
where	nr_sequencia	= :new.nr_seq_retorno;

if	(IE_STATUS_RETORNO_w <> 'R') then
	wheb_mensagem_pck.exibir_mensagem_abort(204450);


end if;

select	max(cd_convenio)
into	cd_conv_ret_w
from	convenio_retorno
where	nr_sequencia		=  :new.nr_seq_retorno;

begin
	select	nvl(conv_est.ie_vinc_adiant_prot_pj,'N')
	into	ie_vinc_adiant_prot_pj_w
	from	convenio_estabelecimento conv_est,
		convenio_retorno conv_ret
	where	conv_est.cd_convenio = conv_ret.cd_convenio
	and	conv_est.cd_estabelecimento = conv_ret.cd_estabelecimento
	and	conv_ret.nr_sequencia = :new.nr_seq_retorno;
exception
when others then
	ie_vinc_adiant_prot_pj_w := 'N';
end;


select	max(nr_adiantamento)
into	nr_adiantamento_w
from	conta_paciente_adiant
where	nr_interno_conta = :new.nr_interno_conta;

if 	(ie_vinc_adiant_prot_pj_w = 'S') and (nr_adiantamento_w is not null) then
	wheb_mensagem_pck.exibir_mensagem_abort(1026496,'nr_adiantamento_w='||nr_adiantamento_w||';'||
							'nr_interno_conta_w='||:new.nr_interno_conta);
end if;


select	count(*)
into	cont_w
from	conta_paciente_guia
where	nr_interno_conta		= :new.nr_interno_conta
and	cd_autorizacao		= :new.cd_autorizacao;

if	(cont_w > 0) then
	select	nvl(sum(vl_guia),0)
	into	vl_guia_w
	from	conta_paciente_guia
	where	nr_interno_conta		= :new.nr_interno_conta
	and	cd_autorizacao		= :new.cd_autorizacao;

	if (vl_guia_w = 0) then
		select	nvl(sum(vl_participante),0)
		into	vl_guia_w
		from	conta_paciente_guia
		where	nr_interno_conta		= :new.nr_interno_conta
		and	cd_autorizacao		= :new.cd_autorizacao;
	end if;

	if	(:new.vl_pago >= 0) and
		(:new.vl_amenor > 0) and
		(:new.vl_amenor > vl_guia_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(204455,	'NR_INTERNO_CONTA_W='||:NEW.NR_INTERNO_CONTA||
								';CD_AUTORIZACAO_W='||:NEW.CD_AUTORIZACAO||
								';VL_GUIA_W='||VL_GUIA_W||
								';VL_AMENOR_W='||:NEW.VL_AMENOR);
	end if;
end if;

Obter_Param_Usuario(27,70, obter_perfil_ativo,:new.nm_usuario,0, ie_conv_dif_w);

if	(nvl(ie_conv_dif_w, 'N') = 'N') then

	select	max(cd_convenio_parametro)
	into	cd_conv_par_w
	from	conta_paciente
	where	nr_interno_conta	= :new.nr_interno_conta;

	if	(cd_conv_ret_w <> cd_conv_par_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(204459);
	end if;

end if;

if	(nvl(ie_perm_conta_sem_titulo_w,'S') = 'N') then
	nr_titulo_w	:= nvl(obter_nr_titulo_conta_prot(:new.nr_interno_conta),0);

	if	(nvl(nr_titulo_w,0) = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(204461,'NR_INTERNO_CONTA_W=' ||:NEW.NR_INTERNO_CONTA);
	end if;
end if;

begin
	select	nvl(conv_est.ie_bloq_lancto_guia_anl_grg,'N')
	into	ie_bloq_lancto_guia_anl_grg_w
	from	convenio_estabelecimento conv_est,
		convenio_retorno conv_ret
	where	conv_est.cd_convenio = conv_ret.cd_convenio
	and	conv_est.cd_estabelecimento = conv_ret.cd_estabelecimento
	and	conv_ret.nr_sequencia = :new.nr_seq_retorno;
exception
when others then
	ie_bloq_lancto_guia_anl_grg_w := 'N';
end;

if (ie_bloq_lancto_guia_anl_grg_w = 'S') then

	select	count(*)
	into	cont_w
	from	lote_audit_hist_guia x,
		lote_audit_hist y
	where	x.nr_interno_conta	= :new.nr_interno_conta
	and	x.cd_autorizacao	= :new.cd_autorizacao
	and	x.nr_seq_lote_hist	= y.nr_sequencia
	and	(((x.dt_baixa_glosa	is null) and 	(obter_valores_guia_grc(x.nr_seq_lote_hist,x.nr_interno_conta,x.cd_autorizacao,'VG') > 0))
	or	 ((y.dt_fechamento is null) and		(obter_valores_guia_grc(x.nr_seq_lote_hist,x.nr_interno_conta,x.cd_autorizacao,'VG') = 0)));

	if	(cont_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(896840,'NR_GUIA_W=' ||:NEW.CD_AUTORIZACAO||';NR_CONTA_W='||:NEW.NR_INTERNO_CONTA);
	end if;

end if;


END;
/


ALTER TABLE TASY.CONVENIO_RETORNO_ITEM ADD (
  CONSTRAINT CONREIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          448K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_RETORNO_ITEM ADD (
  CONSTRAINT CONREIT_TPENCORI_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PEND) 
 REFERENCES TASY.TIPO_PEND_CONV_RET_ITEM (NR_SEQUENCIA),
  CONSTRAINT CONREIT_CONRETO_FK 
 FOREIGN KEY (NR_SEQ_RETORNO) 
 REFERENCES TASY.CONVENIO_RETORNO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CONREIT_CONPACI_FK 
 FOREIGN KEY (NR_INTERNO_CONTA) 
 REFERENCES TASY.CONTA_PACIENTE (NR_INTERNO_CONTA)
    ON DELETE CASCADE,
  CONSTRAINT CONREIT_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT CONREIT_MOGLOSA_FK 
 FOREIGN KEY (CD_MOTIVO_GLOSA) 
 REFERENCES TASY.MOTIVO_GLOSA (CD_MOTIVO_GLOSA),
  CONSTRAINT CONREIT_MOTDESC_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DESC) 
 REFERENCES TASY.MOTIVO_DESCONTO (NR_SEQUENCIA),
  CONSTRAINT CONREIT_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO_DESC) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT CONREIT_CONREIT_FK 
 FOREIGN KEY (NR_SEQ_RET_ITEM_ORIG) 
 REFERENCES TASY.CONVENIO_RETORNO_ITEM (NR_SEQUENCIA),
  CONSTRAINT CONREIT_CONRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEB) 
 REFERENCES TASY.CONVENIO_RECEB (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONVENIO_RETORNO_ITEM TO NIVEL_1;

