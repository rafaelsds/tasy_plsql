ALTER TABLE TASY.ESCALA_CAM_ICU_ORIGINAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CAM_ICU_ORIGINAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CAM_ICU_ORIGINAL
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_ATENDIMENTO            NUMBER(10)          NOT NULL,
  IE_ALT_ESTADO_MENTAL      VARCHAR2(1 BYTE),
  IE_COMPORT_ANORMAL        VARCHAR2(1 BYTE),
  IE_FOCAR_ATENCAO          VARCHAR2(1 BYTE),
  DT_AVALIACAO              DATE                NOT NULL,
  CD_PROFISSIONAL           VARCHAR2(10 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_LIBERACAO              DATE,
  DT_INATIVACAO             DATE,
  NM_USUARIO_INATIVACAO     VARCHAR2(15 BYTE),
  IE_PEDRA_FLUTUAM          VARCHAR2(1 BYTE),
  IE_ERROR                  VARCHAR2(1 BYTE),
  QT_SCORE                  NUMBER(1),
  IE_RICHMOND_RASS          NUMBER(2),
  DS_JUSTIFICATIVA          VARCHAR2(255 BYTE),
  IE_PEIXE_MAR              VARCHAR2(1 BYTE),
  IE_QUILOGRAMA             VARCHAR2(1 BYTE),
  IE_MARTELO_MADEIRA        VARCHAR2(1 BYTE),
  IE_COMANDO                VARCHAR2(1 BYTE),
  IE_ATENCAO_PRESENTE       VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE),
  IE_PENS_DESORG_PRESENTE   VARCHAR2(1 BYTE),
  IE_NIVEL_CONC_PRESENTE    VARCHAR2(1 BYTE),
  IE_INICIO_AGUDO_PRESENTE  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCCAMORG_ATEPACI_FK_I ON TASY.ESCALA_CAM_ICU_ORIGINAL
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCAMORG_PESFISI_FK_I ON TASY.ESCALA_CAM_ICU_ORIGINAL
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCCAMORG_PK ON TASY.ESCALA_CAM_ICU_ORIGINAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_cam_icu_original_atual
before insert or update ON TASY.ESCALA_CAM_ICU_ORIGINAL for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

if (:new.IE_INICIO_AGUDO_PRESENTE = 'P') then

    if (:new.IE_ATENCAO_PRESENTE = 'P') then

        if (:new.IE_NIVEL_CONC_PRESENTE = 'P' or :new.IE_PENS_DESORG_PRESENTE = 'P') then

             :new.qt_score := 3;
        else
              :new.qt_score  := 4;
        end if;

 else

  :new.qt_score  := 2;

 end if;

 else

  :new.qt_score  := 1;

end if;

if(:old.dt_liberacao is null and :new.dt_liberacao is not null and :new.ie_richmond_rass is not null) then

insert into escala_richmond
  (
    nr_sequencia,
    dt_atualizacao,
    nm_usuario,
    dt_atualizacao_nrec,
    nm_usuario_nrec,
    ie_rass,
    dt_avaliacao,
    cd_profissional,
    nr_atendimento,
    ie_situacao,
    dt_liberacao,
    ie_nivel_atencao
  )
  values
  (
    escala_richmond_seq.nextval,
    sysdate,
    :new.nm_usuario,
    sysdate,
    :new.nm_usuario,
    :new.ie_richmond_rass,
    sysdate,
    :new.cd_profissional,
    :new.nr_atendimento,
    'A',
    sysdate,
    :new.ie_nivel_atencao
  );

end if;

end if;

end;
/


ALTER TABLE TASY.ESCALA_CAM_ICU_ORIGINAL ADD (
  CONSTRAINT ESCCAMORG_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_CAM_ICU_ORIGINAL ADD (
  CONSTRAINT ESCCAMORG_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCCAMORG_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_CAM_ICU_ORIGINAL TO NIVEL_1;

