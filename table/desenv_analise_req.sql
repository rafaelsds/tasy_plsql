ALTER TABLE TASY.DESENV_ANALISE_REQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DESENV_ANALISE_REQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.DESENV_ANALISE_REQ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PROJETO       NUMBER(10)               NOT NULL,
  DT_ANALISE           DATE,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ANALISE           VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DESANRE_DESPROJ_FK_I ON TASY.DESENV_ANALISE_REQ
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESANRE_DESPROJ_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.DESANRE_PK ON TASY.DESENV_ANALISE_REQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DESANRE_PK
  MONITORING USAGE;


ALTER TABLE TASY.DESENV_ANALISE_REQ ADD (
  CONSTRAINT DESANRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DESENV_ANALISE_REQ ADD (
  CONSTRAINT DESANRE_DESPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.DESENV_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.DESENV_ANALISE_REQ TO NIVEL_1;

