ALTER TABLE TASY.NOM_RC_IMPRESSAO_DIAG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOM_RC_IMPRESSAO_DIAG CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOM_RC_IMPRESSAO_DIAG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CABECALHO     NUMBER(10)               NOT NULL,
  DS_DIAGNOSTICO       CLOB                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT )
MONITORING;


CREATE INDEX TASY.NOMRCID_NOMRCCA_FK_I ON TASY.NOM_RC_IMPRESSAO_DIAG
(NR_SEQ_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.NOMRCID_PK ON TASY.NOM_RC_IMPRESSAO_DIAG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NOM_RC_IMPRESSAO_DIAG ADD (
  CONSTRAINT NOMRCID_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.NOM_RC_IMPRESSAO_DIAG ADD (
  CONSTRAINT NOMRCID_NOMRCCA_FK 
 FOREIGN KEY (NR_SEQ_CABECALHO) 
 REFERENCES TASY.NOM_RC_CABECALHO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.NOM_RC_IMPRESSAO_DIAG TO NIVEL_1;

