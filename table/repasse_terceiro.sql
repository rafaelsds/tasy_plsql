ALTER TABLE TASY.REPASSE_TERCEIRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REPASSE_TERCEIRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REPASSE_TERCEIRO
(
  NR_REPASSE_TERCEIRO    NUMBER(10)             NOT NULL,
  DT_MESANO_REFERENCIA   DATE                   NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  CD_CGC                 VARCHAR2(14 BYTE),
  CD_CONVENIO            NUMBER(5),
  NR_SEQ_PROTOCOLO       NUMBER(10),
  DT_PERIODO_INICIAL     DATE,
  DT_PERIODO_FINAL       DATE,
  IE_TIPO_DATA           NUMBER(1)              NOT NULL,
  DS_PROTOCOLO           VARCHAR2(50 BYTE),
  NR_SEQ_TERCEIRO        NUMBER(10),
  IE_STATUS              VARCHAR2(1 BYTE)       NOT NULL,
  DT_APROVACAO_TERCEIRO  DATE,
  NM_USUARIO_APROV       VARCHAR2(15 BYTE),
  CD_CONDICAO_PAGAMENTO  NUMBER(10),
  DT_BASE_VENCTO         DATE,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  NR_SEQ_TIPO            NUMBER(10),
  IE_ITEM_REPASSE_AUTO   VARCHAR2(1 BYTE),
  DT_LIBERACAO           DATE,
  DT_BASE_TRIB           DATE,
  IE_REPASSE_MANUAL      VARCHAR2(1 BYTE),
  IE_TIPO_CONVENIO       NUMBER(5),
  NR_REPASSE_TERC_UNIF   NUMBER(10),
  NR_LOTE_CONTABIL       NUMBER(10),
  DT_ULT_ENVIO_EMAIL     DATE,
  DT_LIB_QUEST           DATE,
  NM_USUARIO_LIB_QUEST   VARCHAR2(15 BYTE),
  NR_CODIGO_CONTROLE     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REPTERC_CONPAGA_FK_I ON TASY.REPASSE_TERCEIRO
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTERC_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTERC_CONVENI_FK_I ON TASY.REPASSE_TERCEIRO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTERC_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTERC_ESTABEL_FK_I ON TASY.REPASSE_TERCEIRO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTERC_I1 ON TASY.REPASSE_TERCEIRO
(DT_MESANO_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTERC_I2 ON TASY.REPASSE_TERCEIRO
(NR_SEQ_TERCEIRO, DT_MESANO_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTERC_LOTCONT_FK_I ON TASY.REPASSE_TERCEIRO
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTERC_PESFISI_FK_I ON TASY.REPASSE_TERCEIRO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTERC_PESFISI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTERC_PESJURI_FK_I ON TASY.REPASSE_TERCEIRO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTERC_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REPTERC_PK ON TASY.REPASSE_TERCEIRO
(NR_REPASSE_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTERC_PROCONV_FK_I ON TASY.REPASSE_TERCEIRO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTERC_PROCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REPTERC_TERCEIR_FK_I ON TASY.REPASSE_TERCEIRO
(NR_SEQ_TERCEIRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REPTERC_TIPOREP_FK_I ON TASY.REPASSE_TERCEIRO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REPTERC_TIPOREP_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REPASSE_TERCEIRO_tp  after update ON TASY.REPASSE_TERCEIRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_REPASSE_TERCEIRO);  ds_c_w:=null; ds_w:=substr(:new.IE_STATUS,1,500);gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_TIPO,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO,1,4000),substr(:new.NR_SEQ_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_CONVENIO,1,500);gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_PERIODO_FINAL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_PERIODO_FINAL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_PERIODO_FINAL',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_DATA,1,4000),substr(:new.IE_TIPO_DATA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_DATA',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ITEM_REPASSE_AUTO,1,4000),substr(:new.IE_ITEM_REPASSE_AUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ITEM_REPASSE_AUTO',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_APROVACAO_TERCEIRO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_APROVACAO_TERCEIRO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_APROVACAO_TERCEIRO',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REPASSE_TERCEIRO,1,4000),substr(:new.NR_REPASSE_TERCEIRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_REPASSE_TERCEIRO',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONDICAO_PAGAMENTO,1,4000),substr(:new.CD_CONDICAO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONDICAO_PAGAMENTO',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_BASE_VENCTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_BASE_VENCTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_BASE_VENCTO',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_PERIODO_INICIAL,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_PERIODO_INICIAL,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_PERIODO_INICIAL',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_MESANO_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_MESANO_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_MESANO_REFERENCIA',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_APROV,1,4000),substr(:new.NM_USUARIO_APROV,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_APROV',ie_log_w,ds_w,'REPASSE_TERCEIRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.REPASSE_TERCEIRO_AFTINSERT
after insert ON TASY.REPASSE_TERCEIRO for each row
declare

cursor	c_tipo_repasse_regra_ger(nr_seq_tipo_pc	tipo_repasse.nr_sequencia%type) is
	select	a.cd_regra,
		a.ie_considerar_regra
	from	tipo_repasse b,
		tipo_repasse_regra_geracao a
	where	a.nr_seq_tipo_repasse	= b.nr_sequencia
	and	b.nr_sequencia = nr_seq_tipo_pc
	and	b.cd_regra is null
	union	all
	select	a.cd_regra,
		'C' ie_considerar_regra
	from	tipo_repasse a
	where	a.nr_sequencia = nr_seq_tipo_pc
	and	a.cd_regra is not null;

cd_regra_w		number(5);

begin

if	(:new.nr_seq_tipo is not null) then

	for r_c_tipo_rep_regra_ger in c_tipo_repasse_regra_ger(:new.nr_seq_tipo) loop
		if	(r_c_tipo_rep_regra_ger.cd_regra is not null) then
			atualiza_regra_ger_repasse(:new.nr_repasse_terceiro, r_c_tipo_rep_regra_ger.cd_regra,:new.nm_usuario, r_c_tipo_rep_regra_ger.ie_considerar_regra);
		end if;
	end loop;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.REPASSE_TERCEIRO_INSERT
before insert ON TASY.REPASSE_TERCEIRO for each row
declare
cd_condicao_pagamento_w	number(10,0);
nr_seq_tipo_repasse_w	number(10,0);
begin

select	max(cd_condicao_pagamento),
	max(nr_seq_tipo_rep)
into	cd_condicao_pagamento_w,
	nr_seq_tipo_repasse_w
from	terceiro
where	nr_sequencia		= :new.nr_seq_terceiro;

if	(cd_condicao_pagamento_w is not null) and
	(:new.cd_condicao_pagamento is null) then
	:new.cd_condicao_pagamento := cd_condicao_pagamento_w;
end if;

if	(nr_seq_tipo_repasse_w is not null) and
	(:new.nr_seq_tipo is null) then
	:new.nr_seq_tipo := nr_seq_tipo_repasse_w;
end if;

end;
/


ALTER TABLE TASY.REPASSE_TERCEIRO ADD (
  CONSTRAINT REPTERC_PK
 PRIMARY KEY
 (NR_REPASSE_TERCEIRO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REPASSE_TERCEIRO ADD (
  CONSTRAINT REPTERC_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT REPTERC_TERCEIR_FK 
 FOREIGN KEY (NR_SEQ_TERCEIRO) 
 REFERENCES TASY.TERCEIRO (NR_SEQUENCIA),
  CONSTRAINT REPTERC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REPTERC_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT REPTERC_PROCONV_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_CONVENIO (NR_SEQ_PROTOCOLO),
  CONSTRAINT REPTERC_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT REPTERC_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO),
  CONSTRAINT REPTERC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REPTERC_TIPOREP_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_REPASSE (NR_SEQUENCIA));

GRANT SELECT ON TASY.REPASSE_TERCEIRO TO NIVEL_1;

