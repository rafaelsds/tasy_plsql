ALTER TABLE TASY.UNIDADE_ATEND_EQUIP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.UNIDADE_ATEND_EQUIP CASCADE CONSTRAINTS;

CREATE TABLE TASY.UNIDADE_ATEND_EQUIP
(
  NR_SEQUENCIA          NUMBER(10),
  CD_SETOR_ATENDIMENTO  NUMBER(5)               NOT NULL,
  CD_UNIDADE_BASICA     VARCHAR2(10 BYTE)       NOT NULL,
  CD_UNIDADE_COMPL      VARCHAR2(10 BYTE)       NOT NULL,
  CD_EQUIPAMENTO        NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.UNIATEQ_EQUIPAM_FK_I ON TASY.UNIDADE_ATEND_EQUIP
(CD_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEQ_EQUIPAM_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.UNIATEQ_PK ON TASY.UNIDADE_ATEND_EQUIP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIATEQ_UNIATEN_FK_I ON TASY.UNIDADE_ATEND_EQUIP
(CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIATEQ_UNIATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.UNIDADE_ATEND_EQUIP ADD (
  CONSTRAINT UNIATEQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.UNIDADE_ATEND_EQUIP ADD (
  CONSTRAINT UNIATEQ_EQUIPAM_FK 
 FOREIGN KEY (CD_EQUIPAMENTO) 
 REFERENCES TASY.EQUIPAMENTO (CD_EQUIPAMENTO)
    ON DELETE CASCADE,
  CONSTRAINT UNIATEQ_UNIATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO, CD_UNIDADE_BASICA, CD_UNIDADE_COMPL) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (CD_SETOR_ATENDIMENTO,CD_UNIDADE_BASICA,CD_UNIDADE_COMPL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.UNIDADE_ATEND_EQUIP TO NIVEL_1;

