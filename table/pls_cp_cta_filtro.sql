ALTER TABLE TASY.PLS_CP_CTA_FILTRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_FILTRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_FILTRO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NM_FILTRO               VARCHAR2(200 BYTE)    NOT NULL,
  NR_SEQ_CP_COMBINADA     NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_FILTRO_CONTA         VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_PRODUTO       VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_PROCEDIMENTO  VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_MATERIAL      VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_SERVICO       VARCHAR2(1 BYTE)      NOT NULL,
  IE_DEST_PRESTADOR       VARCHAR2(1 BYTE)      NOT NULL,
  IE_DEST_REEMBOLSO       VARCHAR2(1 BYTE),
  IE_DEST_FATURA_POS      VARCHAR2(1 BYTE),
  IE_DEST_COPARTICIPACAO  VARCHAR2(1 BYTE)      NOT NULL,
  IE_DEST_INTERCAMBIO     VARCHAR2(1 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  IE_RADIO_PROC_MAT_SERV  VARCHAR2(1 BYTE),
  IE_FILTRO_PRESTADOR     VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_CONTRATO      VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_PROTOCOLO     VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_INTERCAMBIO   VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_BENEFICIARIO  VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_PARTICIPANTE  VARCHAR2(1 BYTE)      NOT NULL,
  IE_EXCECAO              VARCHAR2(1 BYTE),
  IE_FILTRO_PROFISSIONAL  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLCPCTF_PK ON TASY.PLS_CP_CTA_FILTRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCPCTF_PLSCPCC_FK_I ON TASY.PLS_CP_CTA_FILTRO
(NR_SEQ_CP_COMBINADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_cp_cta_filtro_atual
before update or insert ON TASY.PLS_CP_CTA_FILTRO for each row
declare

begin
-- Tratamento realizado para n�o permitir nulo nos campos de filtro
if (:new.ie_filtro_beneficiario is null) then
	:new.ie_filtro_beneficiario := 'N';
end if;

if (:new.ie_filtro_conta is null) then
	:new.ie_filtro_conta := 'N';
end if;

if (:new.ie_filtro_contrato is null) then
	:new.ie_filtro_contrato := 'N';
end if;

if (:new.ie_filtro_intercambio is null) then
	:new.ie_filtro_intercambio := 'N';
end if;

if (:new.ie_filtro_material is null) then
	:new.ie_filtro_material := 'N';
end if;

if (:new.ie_filtro_participante is null) then
	:new.ie_filtro_participante := 'N';
end if;

if (:new.ie_filtro_prestador is null) then
	:new.ie_filtro_prestador := 'N';
end if;

if (:new.ie_filtro_procedimento is null) then
	:new.ie_filtro_procedimento := 'N';
end if;

if (:new.ie_filtro_produto is null) then
	:new.ie_filtro_produto := 'N';
end if;

if (:new.ie_filtro_protocolo is null) then
	:new.ie_filtro_protocolo := 'N';
end if;

if (:new.ie_filtro_servico is null) then
	:new.ie_filtro_servico := 'N';
end if;

end pls_cp_cta_filtro_atual;
/


CREATE OR REPLACE TRIGGER TASY.PLS_CP_CTA_FILTRO_tp  after update ON TASY.PLS_CP_CTA_FILTRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_FILTRO_PROCEDIMENTO,1,4000),substr(:new.IE_FILTRO_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PROCEDIMENTO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PARTICIPANTE,1,4000),substr(:new.IE_FILTRO_PARTICIPANTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PARTICIPANTE',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_SERVICO,1,4000),substr(:new.IE_FILTRO_SERVICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_SERVICO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CP_COMBINADA,1,4000),substr(:new.NR_SEQ_CP_COMBINADA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_COMBINADA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_FILTRO,1,4000),substr(:new.NM_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NM_FILTRO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_CONTA,1,4000),substr(:new.IE_FILTRO_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_CONTA',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PRODUTO,1,4000),substr(:new.IE_FILTRO_PRODUTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PRODUTO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RADIO_PROC_MAT_SERV,1,4000),substr(:new.IE_RADIO_PROC_MAT_SERV,1,4000),:new.nm_usuario,nr_seq_w,'IE_RADIO_PROC_MAT_SERV',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PRESTADOR,1,4000),substr(:new.IE_FILTRO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PRESTADOR',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_CONTRATO,1,4000),substr(:new.IE_FILTRO_CONTRATO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_CONTRATO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PROTOCOLO,1,4000),substr(:new.IE_FILTRO_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PROTOCOLO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_INTERCAMBIO,1,4000),substr(:new.IE_FILTRO_INTERCAMBIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_INTERCAMBIO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_BENEFICIARIO,1,4000),substr(:new.IE_FILTRO_BENEFICIARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_BENEFICIARIO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_PROFISSIONAL,1,4000),substr(:new.IE_FILTRO_PROFISSIONAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_PROFISSIONAL',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXCECAO,1,4000),substr(:new.IE_EXCECAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXCECAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FILTRO_MATERIAL,1,4000),substr(:new.IE_FILTRO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_FILTRO_MATERIAL',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CP_CTA_FILTRO ADD (
  CONSTRAINT PLCPCTF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_FILTRO ADD (
  CONSTRAINT PLCPCTF_PLSCPCC_FK 
 FOREIGN KEY (NR_SEQ_CP_COMBINADA) 
 REFERENCES TASY.PLS_CP_CTA_COMBINADA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CP_CTA_FILTRO TO NIVEL_1;

