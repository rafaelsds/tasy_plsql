ALTER TABLE TASY.ALTERACAO_PORTADOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ALTERACAO_PORTADOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.ALTERACAO_PORTADOR
(
  NR_TITULO             NUMBER(10)              NOT NULL,
  NR_SEQUENCIA          NUMBER(5)               NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ALTERACAO          DATE                    NOT NULL,
  CD_MOTIVO             NUMBER(5),
  CD_PORTADOR_ANT       NUMBER(10)              NOT NULL,
  CD_TIPO_PORTADOR_ANT  NUMBER(5)               NOT NULL,
  CD_PORTADOR           NUMBER(10)              NOT NULL,
  CD_TIPO_PORTADOR      NUMBER(5)               NOT NULL,
  VL_DESPESA            NUMBER(15,2),
  NR_LOTE_CONTABIL      NUMBER(10),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  NR_SEQ_TRANS_FIN      NUMBER(10),
  VL_SALDO_TITULO       NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ALTPORT_LOTCONT_FK_I ON TASY.ALTERACAO_PORTADOR
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALTPORT_LOTCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ALTPORT_PK ON TASY.ALTERACAO_PORTADOR
(NR_TITULO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALTPORT_PORANTE_FK_I ON TASY.ALTERACAO_PORTADOR
(CD_PORTADOR_ANT, CD_TIPO_PORTADOR_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALTPORT_PORANTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ALTPORT_PORTADO_FK_I ON TASY.ALTERACAO_PORTADOR
(CD_PORTADOR, CD_TIPO_PORTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALTPORT_PORTADO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ALTPORT_TITRECE_FK_I ON TASY.ALTERACAO_PORTADOR
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALTPORT_TITRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ALTPORT_TRAFINA_FK_I ON TASY.ALTERACAO_PORTADOR
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ALTPORT_TRAFINA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.alteracao_portador_after
after insert ON TASY.ALTERACAO_PORTADOR for each row
declare

cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;
vl_movimento_w          ctb_documento.vl_movimento%type;

cursor c01 is
        select  a.nm_atributo,
                a.cd_tipo_lote_contab
        from    atributo_contab a
        where   a.cd_tipo_lote_contab = 5
        and     a.nm_atributo in ( 'VL_ALTERACAO_PORTADOR', 'VL_TIT_ALT_PORTADOR');

c01_w           c01%rowtype;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if      ( nvl(:new.nr_titulo, 0) <> 0 ) then
        begin

        select  cd_estabelecimento
        into    cd_estabelecimento_w
        from    titulo_Receber
        where   nr_titulo = :new.nr_titulo;

	/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_alteracao);

        open c01;
        loop
        fetch c01 into
                c01_w;
        exit when c01%notfound;
                begin

                vl_movimento_w  :=      case    c01_w.nm_atributo
                                        when    'VL_ALTERACAO_PORTADOR'    then :new.vl_despesa
                                        when    'VL_TIT_ALT_PORTADOR'      then :new.vl_saldo_titulo
                                        else
                                                null
                                        end;

                if      (nvl(vl_movimento_w, 0) <> 0) and (nvl(:new.nr_seq_trans_fin, 0) <> 0) then
                        begin

                        ctb_concil_financeira_pck.ctb_gravar_documento  (       cd_estabelecimento_w,
                                                                                trunc(:new.dt_alteracao),
                                                                                c01_w.cd_tipo_lote_contab,
                                                                                :new.nr_seq_trans_fin,
                                                                                43,
                                                                                :new.nr_titulo,
                                                                                :new.nr_sequencia,
                                                                                0,
                                                                                vl_movimento_w,
                                                                                'ALTERACAO_PORTADOR',
                                                                                c01_w.nm_atributo,
                                                                                :new.nm_usuario);

                        end;
                end if;




                end;
        end loop;
        close c01;

        end;
end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.alteracao_portador_update
before update ON TASY.ALTERACAO_PORTADOR for each row
declare
dt_contabil_w		titulo_receber.dt_contabil%type;
cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if 	(nvl(:new.nr_titulo, 0) <> 0) then

	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	titulo_Receber
	where	nr_titulo = :new.nr_titulo;

	/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_alteracao);

end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.alteracao_portador_insert
  before insert ON TASY.ALTERACAO_PORTADOR   for each row
declare
  -- local variables here
cd_estabelecimento_w    ctb_documento.cd_estabelecimento%type;
begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	select  cd_estabelecimento
	into    cd_estabelecimento_w
	from    titulo_Receber
	where   nr_titulo = :new.nr_titulo;

	/* Projeto Davita  - Nao deixa gerar movimento contabil apos fechamento  da data */
	philips_contabil_pck.valida_se_dia_fechado(OBTER_EMPRESA_ESTAB(cd_estabelecimento_w),:new.dt_alteracao);
end if;
end alteracao_portador_insert;
/


ALTER TABLE TASY.ALTERACAO_PORTADOR ADD (
  CONSTRAINT ALTPORT_PK
 PRIMARY KEY
 (NR_TITULO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ALTERACAO_PORTADOR ADD (
  CONSTRAINT ALTPORT_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT ALTPORT_PORANTE_FK 
 FOREIGN KEY (CD_PORTADOR_ANT, CD_TIPO_PORTADOR_ANT) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT ALTPORT_PORTADO_FK 
 FOREIGN KEY (CD_PORTADOR, CD_TIPO_PORTADOR) 
 REFERENCES TASY.PORTADOR (CD_PORTADOR,CD_TIPO_PORTADOR),
  CONSTRAINT ALTPORT_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO),
  CONSTRAINT ALTPORT_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.ALTERACAO_PORTADOR TO NIVEL_1;

