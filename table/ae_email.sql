ALTER TABLE TASY.AE_EMAIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AE_EMAIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.AE_EMAIL
(
  EVENT_ID               VARCHAR2(36 BYTE)      NOT NULL,
  TIMESTAMP              TIMESTAMP(6),
  USER_ID                VARCHAR2(100 BYTE),
  USER_FULLNAME          VARCHAR2(256 BYTE),
  HOST_ADDRESS           VARCHAR2(256 BYTE),
  ROOT_EVENT_ID          VARCHAR2(36 BYTE),
  PARENT_EVENT_ID        VARCHAR2(36 BYTE),
  TIMESTAMP_ZONE_ID      VARCHAR2(256 BYTE),
  SUBJECT                VARCHAR2(2000 BYTE),
  CONTENT                CLOB,
  SENDER                 VARCHAR2(4000 BYTE),
  RECIPIENT              VARCHAR2(4000 BYTE),
  RECIPIENT_COPY         VARCHAR2(4000 BYTE),
  RECIPIENT_HIDDEN_COPY  VARCHAR2(4000 BYTE),
  USER_EMAIL             VARCHAR2(256 BYTE),
  PASSWORD_EMAIL         VARCHAR2(256 BYTE),
  SMTP                   VARCHAR2(256 BYTE),
  PORT                   NUMBER(8),
  SSL                    NUMBER(8),
  CONFIRMATION           NUMBER(1),
  PRIORITY               VARCHAR2(30 BYTE),
  ESTABLISHMENT_CODE     NUMBER(8),
  ESTABLISHMENT_NAME     VARCHAR2(256 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (CONTENT) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PK_AEEMAIL ON TASY.AE_EMAIL
(EVENT_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AE_EMAIL ADD (
  CONSTRAINT PK_AEEMAIL
 PRIMARY KEY
 (EVENT_ID)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AE_EMAIL TO NIVEL_1;

