ALTER TABLE TASY.PRECO_MAT_STATUS_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRECO_MAT_STATUS_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_MAT_STATUS_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_VIGENCIA          DATE                     NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_TAB_PRECO_MAT     NUMBER(4)                NOT NULL,
  IE_STATUS_ITEM       VARCHAR2(1 BYTE)         NOT NULL,
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  VL_MATERIAL          NUMBER(13,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRMASTH_ESTABEL_FK_I ON TASY.PRECO_MAT_STATUS_HIST
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMASTH_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRMASTH_MATERIA_FK_I ON TASY.PRECO_MAT_STATUS_HIST
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMASTH_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRMASTH_PK ON TASY.PRECO_MAT_STATUS_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.preco_mat_status_hist_update
before update ON TASY.PRECO_MAT_STATUS_HIST for each row
declare
cd_tab_mat_atualizacao_w	number(4);
begin

if	(:new.IE_STATUS_ITEM = 1) then

	select	nvl(max(cd_tab_mat_atualizacao),0)
	into	cd_tab_mat_atualizacao_w
	from	convenio_preco_mat
	where	cd_tab_preco_mat = :new.cd_tab_preco_mat;

	if	(cd_tab_mat_atualizacao_w > 0) then
		update	preco_material a
		set	ie_preco_venda = 'N'
		where	a.cd_tab_preco_mat		= cd_tab_mat_atualizacao_w
		and	a.cd_material        		= :new.cd_material
		and	a.dt_inicio_vigencia		< :new.dt_vigencia
		and	a.vl_preco_venda		< nvl(:new.vl_material,0);
	end if;
end if;

end;
/


ALTER TABLE TASY.PRECO_MAT_STATUS_HIST ADD (
  CONSTRAINT PRMASTH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRECO_MAT_STATUS_HIST ADD (
  CONSTRAINT PRMASTH_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PRMASTH_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PRECO_MAT_STATUS_HIST TO NIVEL_1;

