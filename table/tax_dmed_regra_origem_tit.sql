ALTER TABLE TASY.TAX_DMED_REGRA_ORIGEM_TIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TAX_DMED_REGRA_ORIGEM_TIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.TAX_DMED_REGRA_ORIGEM_TIT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_LOTE          NUMBER(10),
  CD_ORIGEM_TITULO     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TAXDMEDOT_PK ON TASY.TAX_DMED_REGRA_ORIGEM_TIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAXDMEDOT_TAXDMEDL_FK_I ON TASY.TAX_DMED_REGRA_ORIGEM_TIT
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TAX_DMED_REGRA_ORIGEM_TIT ADD (
  CONSTRAINT TAXDMEDOT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.TAX_DMED_REGRA_ORIGEM_TIT ADD (
  CONSTRAINT TAXDMEDOT_TAXDMEDL_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.TAX_DMED_LOTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TAX_DMED_REGRA_ORIGEM_TIT TO NIVEL_1;

