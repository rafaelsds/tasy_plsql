ALTER TABLE TASY.RECOMENDACAO_INTEGRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RECOMENDACAO_INTEGRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RECOMENDACAO_INTEGRACAO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_SISTEMA_INTEG     NUMBER(10)           NOT NULL,
  CD_TIPO_RECOMENDACAO     NUMBER(10),
  CD_INTEGRACAO            VARCHAR2(60 BYTE),
  IE_UTILIZA_ESTAB         VARCHAR2(1 BYTE),
  DS_INTERFACE_IDENTIFIER  VARCHAR2(20 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RECINT_PK ON TASY.RECOMENDACAO_INTEGRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECINT_SISINTE_FK_I ON TASY.RECOMENDACAO_INTEGRACAO
(NR_SEQ_SISTEMA_INTEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECINT_TIPRECO_FK_I ON TASY.RECOMENDACAO_INTEGRACAO
(CD_TIPO_RECOMENDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RECOMENDACAO_INTEGRACAO ADD (
  CONSTRAINT RECINT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.RECOMENDACAO_INTEGRACAO ADD (
  CONSTRAINT RECINT_SISINTE_FK 
 FOREIGN KEY (NR_SEQ_SISTEMA_INTEG) 
 REFERENCES TASY.SISTEMA_INTEGRACAO (NR_SEQUENCIA),
  CONSTRAINT RECINT_TIPRECO_FK 
 FOREIGN KEY (CD_TIPO_RECOMENDACAO) 
 REFERENCES TASY.TIPO_RECOMENDACAO (CD_TIPO_RECOMENDACAO));

GRANT SELECT ON TASY.RECOMENDACAO_INTEGRACAO TO NIVEL_1;

