ALTER TABLE TASY.AGENDA_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_REGRA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4),
  CD_AGENDA               NUMBER(10),
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  IE_PERMITE              VARCHAR2(1 BYTE)      NOT NULL,
  CD_CONVENIO             NUMBER(5),
  CD_AREA_PROC            NUMBER(8),
  CD_ESPECIALIDADE        NUMBER(8),
  CD_GRUPO_PROC           NUMBER(15),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  CD_MEDICO               VARCHAR2(10 BYTE),
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO     NUMBER(10),
  NR_SEQ_REGRA            NUMBER(10),
  CD_MUNICIPIO_IBGE       VARCHAR2(6 BYTE),
  QT_REGRA                NUMBER(3),
  NR_SEQ_GRUPO            NUMBER(10),
  NR_SEQ_FORMA_ORG        NUMBER(10),
  NR_SEQ_SUBGRUPO         NUMBER(10),
  NR_SEQ_CLASSIF_AGENDA   NUMBER(10),
  CD_CATEGORIA            VARCHAR2(10 BYTE),
  IE_FORMA_CONSISTENCIA   VARCHAR2(1 BYTE),
  IE_MEDICO               VARCHAR2(1 BYTE),
  CD_PERFIL               NUMBER(5),
  CD_PLANO_CONVENIO       VARCHAR2(10 BYTE),
  DT_INICIO_AGENDA        DATE,
  DT_FIM_AGENDA           DATE,
  IE_DIA_SEMANA           NUMBER(1),
  NR_SEQ_TURNO            NUMBER(10),
  IE_TURNO                VARCHAR2(1 BYTE),
  QT_HORAS_ANT_AGENDA     NUMBER(10),
  NR_SEQ_EQUIPE           NUMBER(10),
  DT_INICIO_VIGENCIA      DATE,
  DT_FIM_VIGENCIA         DATE,
  NR_SEQ_PROCED_REGRA     NUMBER(10),
  HR_INICIO               DATE,
  HR_FIM                  DATE,
  NM_REGRA                VARCHAR2(255 BYTE),
  NR_SEQ_GRUPO_AGEINT     NUMBER(10),
  NR_SEQ_CLASSIF          NUMBER(10),
  IE_SOMENTE_ANESTESIA    VARCHAR2(1 BYTE),
  DS_MENSAGEM             VARCHAR2(2000 BYTE),
  NR_SEQ_AGENDA_REGRA     NUMBER(10),
  IE_CONSISTE_QTD_HORA    VARCHAR2(1 BYTE),
  QT_IDADE_MIN            NUMBER(10),
  QT_IDADE_MAX            NUMBER(10),
  IE_CONVENIO_QTD         VARCHAR2(1 BYTE),
  IE_TIPO_ATENDIMENTO     NUMBER(3),
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  NR_SEQ_COBERTURA        NUMBER(10),
  IE_ESTRUTURA_QTD        VARCHAR2(1 BYTE),
  IE_CONSISTE_FINAL       VARCHAR2(1 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  CD_EMPRESA_REF          NUMBER(10),
  IE_PROC_ADIC            VARCHAR2(1 BYTE),
  IE_AGENDA               VARCHAR2(2 BYTE),
  IE_TIPO_CONVENIO        VARCHAR2(2 BYTE),
  IE_VALIDAR_DIAS_SEMANA  VARCHAR2(1 BYTE),
  NR_SEQ_HOR_ESP          NUMBER(10),
  IE_SOMENTE_MESMO_GRUPO  VARCHAR2(1 BYTE)      DEFAULT null,
  NR_SEQ_TOPOGRAFIA       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGEREGR_AGEHORA_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_TURNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_AGEHORA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_AGEHRES_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_HOR_ESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEREGR_AGEINGR_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_GRUPO_AGEINT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_AGEINGR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_AGENDA_FK_I ON TASY.AGENDA_REGRA
(CD_AGENDA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEREGR_AGEPACL_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_CLASSIF_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_AGEPACL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_AGINPRR_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_PROCED_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_AGINPRR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_AREPROC_FK_I ON TASY.AGENDA_REGRA
(CD_AREA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_CATCONV_FK_I ON TASY.AGENDA_REGRA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_CONPLAN_FK_I ON TASY.AGENDA_REGRA
(CD_CONVENIO, CD_PLANO_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_CONVCOB_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_COBERTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_CONVCOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_CONVENI_FK_I ON TASY.AGENDA_REGRA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_EMPREFE_FK_I ON TASY.AGENDA_REGRA
(CD_EMPRESA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEREGR_ESPPROC_FK_I ON TASY.AGENDA_REGRA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_ESTABEL_FK_I ON TASY.AGENDA_REGRA
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_GRUPROC_FK_I ON TASY.AGENDA_REGRA
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_I1 ON TASY.AGENDA_REGRA
(NVL("CD_ESTABELECIMENTO",0), NVL("CD_AGENDA",0), NVL("CD_CONVENIO",0), 1)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEREGR_PERFIL_FK_I ON TASY.AGENDA_REGRA
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_PESFISI_FK_I ON TASY.AGENDA_REGRA
(CD_MEDICO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEREGR_PFEQUIP_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_PFEQUIP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.AGEREGR_PK ON TASY.AGENDA_REGRA
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEREGR_PROCEDI_FK_I ON TASY.AGENDA_REGRA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_PROINTCLA_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_PROINTCLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_PROINTE_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_SETATEN_FK_I ON TASY.AGENDA_REGRA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_SUSFORG_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_FORMA_ORG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_SUSFORG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_SUSGRUP_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_SUSGRUP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_SUSMUNI_FK_I ON TASY.AGENDA_REGRA
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGEREGR_SUSSUBG_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_SUBGRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGEREGR_SUSSUBG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGEREGR_TOPDOR_FK_I ON TASY.AGENDA_REGRA
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.AGENDA_REGRA_tp  after update ON TASY.AGENDA_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_TIPO_ATENDIMENTO,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'AGENDA_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'AGENDA_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_regra_update
before insert or update ON TASY.AGENDA_REGRA for each row
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	(:new.cd_procedimento is null) and
		(:new.ie_origem_proced is not null) then
		:new.ie_origem_proced := null;
	end if;
end if;
end;
/


ALTER TABLE TASY.AGENDA_REGRA ADD (
  CONSTRAINT AGEREGR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_REGRA ADD (
  CONSTRAINT AGEREGR_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_REF) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT AGEREGR_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA)
    ON DELETE CASCADE,
  CONSTRAINT AGEREGR_AGEHRES_FK 
 FOREIGN KEY (NR_SEQ_HOR_ESP) 
 REFERENCES TASY.AGENDA_HORARIO_ESP (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT AGEREGR_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGEREGR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT AGEREGR_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AGEREGR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT AGEREGR_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROC) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT AGEREGR_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT AGEREGR_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT AGEREGR_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT AGEREGR_SUSGRUP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.SUS_GRUPO (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_SUSSUBG_FK 
 FOREIGN KEY (NR_SEQ_SUBGRUPO) 
 REFERENCES TASY.SUS_SUBGRUPO (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_SUSFORG_FK 
 FOREIGN KEY (NR_SEQ_FORMA_ORG) 
 REFERENCES TASY.SUS_FORMA_ORGANIZACAO (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_AGEPACL_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT AGEREGR_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT AGEREGR_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO_CONVENIO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT AGEREGR_AGEHORA_FK 
 FOREIGN KEY (NR_SEQ_TURNO) 
 REFERENCES TASY.AGENDA_HORARIO (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_PFEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.PF_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_AGINPRR_FK 
 FOREIGN KEY (NR_SEQ_PROCED_REGRA) 
 REFERENCES TASY.AGEINT_PROCED_REGRA (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_AGEINGR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_AGEINT) 
 REFERENCES TASY.AGENDA_INT_GRUPO (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_PROINTCLA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PROC_INTERNO_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT AGEREGR_CONVCOB_FK 
 FOREIGN KEY (NR_SEQ_COBERTURA) 
 REFERENCES TASY.CONVENIO_COBERTURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_REGRA TO NIVEL_1;

GRANT SELECT ON TASY.AGENDA_REGRA TO ROBOCMTECNOLOGIA;

