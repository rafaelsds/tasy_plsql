ALTER TABLE TASY.CONVENIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO
(
  CD_CONVENIO               NUMBER(5)           NOT NULL,
  DS_CONVENIO               VARCHAR2(255 BYTE)  NOT NULL,
  DT_INCLUSAO               DATE                NOT NULL,
  IE_TIPO_CONVENIO          NUMBER(2)           NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  CD_CGC                    VARCHAR2(14 BYTE)   NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_DIA_VENCIMENTO         NUMBER(2)           NOT NULL,
  CD_CONDICAO_PAGAMENTO     NUMBER(10),
  CD_INTERNO                VARCHAR2(15 BYTE),
  CD_REGIONAL               VARCHAR2(15 BYTE),
  DT_REF_VALIDA             DATE,
  QT_DIAS_TOLERANCIA        NUMBER(3),
  IE_FORMA_CALCULO_DIARIA   VARCHAR2(1 BYTE),
  CD_INTERFACE_ENVIO        NUMBER(5),
  CD_INTERFACE_RETORNO      NUMBER(5),
  IE_EXIGE_GUIA             VARCHAR2(1 BYTE),
  IE_SEPARA_CONTA           VARCHAR2(1 BYTE),
  NR_DIGITOS_CODIGO         VARCHAR2(15 BYTE),
  DS_ROTINA_DIGITO          VARCHAR2(50 BYTE),
  DS_MASCARA_CODIGO         VARCHAR2(30 BYTE),
  CD_PROCESSO_ALTA          NUMBER(10),
  DT_CANCELAMENTO           DATE,
  DS_OBSERVACAO             VARCHAR2(2000 BYTE),
  CD_CONTA_CONTABIL         VARCHAR2(20 BYTE),
  IE_CLASSIF_CONTABIL       VARCHAR2(1 BYTE),
  IE_GLOSA_ATENDIMENTO      VARCHAR2(1 BYTE),
  IE_CODIGO_CONVENIO        VARCHAR2(1 BYTE),
  DS_SENHA                  VARCHAR2(20 BYTE),
  CD_INTERFACE_AUTORIZACAO  NUMBER(5),
  IE_VALOR_CONTABIL         VARCHAR2(1 BYTE),
  IE_TIPO_ACOMODACAO        NUMBER(1),
  IE_PRECO_MEDIO_MATERIAL   VARCHAR2(1 BYTE)    NOT NULL,
  QT_CONTA_PROTOCOLO        NUMBER(5)           NOT NULL,
  IE_TITULO_RECEBER         VARCHAR2(1 BYTE)    NOT NULL,
  IE_AGENDA_CONSULTA        VARCHAR2(1 BYTE)    NOT NULL,
  IE_EXIGE_DATA_ULT_PAGTO   VARCHAR2(1 BYTE)    NOT NULL,
  IE_DOC_CONVENIO           VARCHAR2(1 BYTE)    NOT NULL,
  IE_ORIGEM_PRECO           VARCHAR2(5 BYTE)    NOT NULL,
  IE_PRECEDENCIA_PRECO      VARCHAR2(1 BYTE)    NOT NULL,
  IE_AGRUP_ITEM_INTERF      VARCHAR2(1 BYTE)    NOT NULL,
  IE_CONVERSAO_MAT          VARCHAR2(1 BYTE)    NOT NULL,
  IE_DOC_RETORNO            VARCHAR2(15 BYTE),
  IE_REP_COD_USUARIO        VARCHAR2(1 BYTE)    NOT NULL,
  IE_EXIGE_ORC_ATEND        VARCHAR2(1 BYTE)    NOT NULL,
  IE_CALC_PORTE             VARCHAR2(1 BYTE)    NOT NULL,
  IE_EXIGE_SENHA_ATEND      VARCHAR2(1 BYTE)    NOT NULL,
  NR_MULTIPLO_ENVIO         NUMBER(10)          NOT NULL,
  IE_EXIGE_PLANO            VARCHAR2(1 BYTE)    NOT NULL,
  IE_PARTIC_CIRURGIA        NUMBER(2),
  IE_EXIGE_CARTEIRA_ATEND   VARCHAR2(1 BYTE)    NOT NULL,
  IE_EXIGE_VALIDADE_ATEND   VARCHAR2(1 BYTE)    NOT NULL,
  IE_SOLIC_EXAME_TASYMED    VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  QT_DIAS_REAPRESENTACAO    NUMBER(3),
  QT_DIA_FIM_CONTA          NUMBER(3),
  DS_MASCARA_SENHA          VARCHAR2(25 BYTE),
  DS_MASCARA_GUIA           VARCHAR2(25 BYTE),
  IE_CONSISTE_AUTOR         VARCHAR2(1 BYTE)    NOT NULL,
  VL_MAX_CONVENIO           NUMBER(15,2),
  IE_GUIA_UNICA_CONTA       VARCHAR2(1 BYTE),
  CD_CONVENIO_GLOSA         NUMBER(5),
  CD_CATEGORIA_GLOSA        VARCHAR2(10 BYTE),
  NR_VIAS_CONTA             NUMBER(3),
  IE_EXIGE_ORIGEM           VARCHAR2(1 BYTE),
  NR_DIGITO_PLANO           NUMBER(10),
  QT_INICIO_DIGITO          NUMBER(2),
  NR_TELEFONE_AUTOR         VARCHAR2(40 BYTE),
  DS_ARQUIVO_LOGO_TISS      VARCHAR2(140 BYTE),
  IE_EXIGE_TIPO_GUIA        VARCHAR2(1 BYTE)    NOT NULL,
  DS_ROTINA_DIGITO_GUIA     VARCHAR2(50 BYTE),
  IE_VENC_ULTIMO_DIA        VARCHAR2(1 BYTE),
  CD_FONTE_REMUNER_CIH      VARCHAR2(3 BYTE),
  DT_DIA_ENTREGA            NUMBER(2),
  DT_ENTREGA_PROT           DATE,
  DS_COR                    VARCHAR2(15 BYTE),
  DS_ROTINA_SENHA           VARCHAR2(50 BYTE),
  IE_CONSISTE_BIOMETRIA     VARCHAR2(1 BYTE),
  CD_EDICAO_AMB_REFER       NUMBER(6),
  QT_DIAS_AUTORIZACAO       NUMBER(10),
  IE_COBERTURA              VARCHAR2(1 BYTE),
  QT_DIAS_AUTOR_PROC        NUMBER(10),
  NR_PRIM_DIGITOS           VARCHAR2(30 BYTE),
  IE_IMPRIME_ESTAB_LOG      VARCHAR2(1 BYTE),
  IE_AGENDA_WEB             VARCHAR2(1 BYTE),
  DS_OBS_NOTA               VARCHAR2(2000 BYTE),
  QT_DIAS_REGURSO_GLOSA     NUMBER(3),
  QT_DIAS_CODIFICACAO       NUMBER(10),
  QT_DIAS_TERM_AGENDA       NUMBER(10),
  IE_EXIGE_EMPRESA_AGI      VARCHAR2(1 BYTE),
  IE_FORMA_LOCALIZADOR      VARCHAR2(1 BYTE),
  DS_MASCARA_COMPL          VARCHAR2(30 BYTE),
  QT_DIAS_TOL_ENTREGA       NUMBER(3),
  CD_SENHA_PLAMTA           VARCHAR2(20 BYTE),
  QT_DIAS_CANCEL_PLAMTA     NUMBER(10),
  DS_OBS_NF_ELETRONICA      VARCHAR2(2000 BYTE),
  IE_EXIGE_EMPRESA_AGEWEB   VARCHAR2(1 BYTE),
  DS_ARQUIVO_LOGO_CONTA     VARCHAR2(240 BYTE),
  IE_MATRICULA_INTEG        VARCHAR2(1 BYTE),
  IE_NF_CONTAB_REC_CONV     VARCHAR2(1 BYTE),
  IE_EXIGE_TITULAR          VARCHAR2(1 BYTE),
  IE_INTERMEDIARIO          VARCHAR2(1 BYTE),
  CD_INTEGRACAO             VARCHAR2(20 BYTE),
  DS_COR_HTML               VARCHAR2(15 BYTE),
  NR_DDI_TELEFONE           VARCHAR2(3 BYTE),
  CD_TIPO_CONVENIO_MX       NUMBER(10),
  NR_DOC_EXTERNO            VARCHAR2(80 BYTE),
  CD_EXTERNO                VARCHAR2(100 BYTE),
  NR_PRIORIDADE_PADRAO      NUMBER(4),
  IE_MULTIPLO_ARQUIVO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.CONVENIO.CD_CONVENIO IS 'C�digo do Conv�nio';

COMMENT ON COLUMN TASY.CONVENIO.DS_CONVENIO IS 'Descri�ao do Convenio';

COMMENT ON COLUMN TASY.CONVENIO.DT_INCLUSAO IS 'Data de Inclus�o do Conv�nio';

COMMENT ON COLUMN TASY.CONVENIO.IE_TIPO_CONVENIO IS 'Indica o Tipo de Convenio';

COMMENT ON COLUMN TASY.CONVENIO.IE_SITUACAO IS 'Situacao do Convenio';

COMMENT ON COLUMN TASY.CONVENIO.CD_CGC IS 'Codigo do CGC';


CREATE INDEX TASY.CONVENI_CATCONV_FK_I ON TASY.CONVENIO
(CD_CONVENIO_GLOSA, CD_CATEGORIA_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONVENI_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONVENI_CATDERE_FK_I ON TASY.CONVENIO
(CD_TIPO_CONVENIO_MX)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONVENI_CONCONT_FK_I ON TASY.CONVENIO
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONVENI_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONVENI_CONPAGA_FK_I ON TASY.CONVENIO
(CD_CONDICAO_PAGAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONVENI_CONPAGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONVENI_EDIAMB_FK_I ON TASY.CONVENIO
(CD_EDICAO_AMB_REFER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONVENI_EDIAMB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONVENI_INTENV_FK_I ON TASY.CONVENIO
(CD_INTERFACE_ENVIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONVENI_INTENV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONVENI_INTERAU_FK_I ON TASY.CONVENIO
(CD_INTERFACE_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONVENI_INTERAU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONVENI_INTRET_FK_I ON TASY.CONVENIO
(CD_INTERFACE_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONVENI_INTRET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONVENI_PESJURI_FK_I ON TASY.CONVENIO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONVENI_PK ON TASY.CONVENIO
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONVENI_PROCESS_FK_I ON TASY.CONVENIO
(CD_PROCESSO_ALTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONVENI_PROCESS_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Convenio_Insert
AFTER INSERT ON TASY.CONVENIO FOR EACH ROW
DECLARE

nr_seq_convenio_estab_w		number(10,0);
cd_estabelecimento_w		number(04,0);

BEGIN

select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	Usuario
where	nm_usuario	= :new.nm_usuario;

if	(nvl(cd_estabelecimento_w,0) > 0) then
	begin

	select	convenio_estabelecimento_seq.nextval
	into	nr_seq_convenio_estab_w
	from	dual;

	insert	into convenio_estabelecimento
		(nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		cd_convenio,
		cd_estabelecimento,
		ie_exige_data_ult_pagto,
		ie_exige_guia,
		ie_exige_orc_atend,
		ie_glosa_atendimento,
		ie_preco_medio_material,
		ie_agenda_consulta,
		ie_rep_cod_usuario,
		ie_exige_carteira_atend,
		ie_exige_validade_atend,
		ie_exige_plano,
		ie_separa_conta,
		ie_guia_unica_conta,
		ie_valor_contabil,
		ie_titulo_receber,
		ie_conversao_mat,
		ie_doc_convenio,
		ie_doc_retorno,
		qt_conta_protocolo,
		qt_dia_fim_conta,
		ie_exige_senha_atend,
		ie_partic_cirurgia,
		ie_exige_origem,
		cd_interno,
		cd_regional,
		ie_protocolo_conta,
		ie_gerar_nf_titulo,
		ie_cancelar_conta,
		ie_manter_zerado_edicao,
		ie_exige_fim_vigencia,
		ie_partic_resp_cred,
		ie_doc_autorizacao,
		ie_fechar_atend_adiant,
		ie_conta_fim_mes,
		ie_cgc_cih,
		ie_regra_prior_edicao,
		ie_obter_preco_mat_autor,
		ie_gerar_NF,
		ie_gera_nome_nf_convenio,
		ie_forma_geracao_nf,
		ie_titulo_sem_nf,
		ie_consiste_guia_atend,
		ie_doc_conv_part_func,
		ie_medico_cooperado,
		ie_exige_lib_bras)
	values	(nr_seq_convenio_estab_w,
		sysdate,
		:new.nm_usuario,
		:new.cd_convenio,
		cd_estabelecimento_w,
		:new.ie_exige_data_ult_pagto,
		:new.ie_exige_guia,
		:new.ie_exige_orc_atend,
		:new.ie_glosa_atendimento,
		:new.ie_preco_medio_material,
		:new.ie_agenda_consulta,
		:new.ie_rep_cod_usuario,
		:new.ie_exige_carteira_atend,
		:new.ie_exige_validade_atend,
		:new.ie_exige_plano,
		:new.ie_separa_conta,
		:new.ie_guia_unica_conta,
		:new.ie_valor_contabil,
		:new.ie_titulo_receber,
		:new.ie_conversao_mat,
		:new.ie_doc_convenio,
		:new.ie_doc_retorno,
		:new.qt_conta_protocolo,
		:new.qt_dia_fim_conta,
		:new.ie_exige_senha_atend,
		:new.ie_partic_cirurgia,
		:new.ie_exige_origem,
		:new.cd_interno,
		:new.cd_regional,
		'T',
		'S',
		'S',
		'N',
		'N',
		'N',
		'N',
		'N',
		'N',
		'S',
		'DT',
		'N',
		'S',
		'N',
		'T',
		'S',
		'N',
		'N',
		'N',
		'N');
	end;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.CONVENIO_log  after update ON TASY.CONVENIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then  ds_s_w := to_char(:old.CD_CONVENIO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_ORIGEM_PRECO,1,2000),substr(:new.IE_ORIGEM_PRECO,1,2000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PRECO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w);  end if;  exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.CONVENIO_tp  after update ON TASY.CONVENIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_CONVENIO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_MATRICULA_INTEG,1,4000),substr(:new.IE_MATRICULA_INTEG,1,4000),:new.nm_usuario,nr_seq_w,'IE_MATRICULA_INTEG',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MASCARA_COMPL,1,4000),substr(:new.DS_MASCARA_COMPL,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA_COMPL',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTEGRACAO,1,4000),substr(:new.CD_INTEGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTEGRACAO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONDICAO_PAGAMENTO,1,4000),substr(:new.CD_CONDICAO_PAGAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONDICAO_PAGAMENTO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERNO,1,4000),substr(:new.CD_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERNO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_REGIONAL,1,4000),substr(:new.CD_REGIONAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_REGIONAL',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCESSO_ALTA,1,4000),substr(:new.CD_PROCESSO_ALTA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCESSO_ALTA',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_CANCELAMENTO,1,4000),substr(:new.DT_CANCELAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_CANCELAMENTO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_GUIA,1,4000),substr(:new.IE_EXIGE_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_GUIA',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ROTINA_DIGITO,1,4000),substr(:new.DS_ROTINA_DIGITO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ROTINA_DIGITO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DIGITOS_CODIGO,1,4000),substr(:new.NR_DIGITOS_CODIGO,1,4000),:new.nm_usuario,nr_seq_w,'NR_DIGITOS_CODIGO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MASCARA_CODIGO,1,4000),substr(:new.DS_MASCARA_CODIGO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MASCARA_CODIGO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIF_CONTABIL,1,4000),substr(:new.IE_CLASSIF_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIF_CONTABIL',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CONVENIO,1,4000),substr(:new.DS_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CONVENIO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INCLUSAO,1,4000),substr(:new.DT_INCLUSAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_INCLUSAO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CONVENIO,1,4000),substr(:new.IE_TIPO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CONVENIO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CGC,1,4000),substr(:new.CD_CGC,1,4000),:new.nm_usuario,nr_seq_w,'CD_CGC',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALOR_CONTABIL,1,4000),substr(:new.IE_VALOR_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR_CONTABIL',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ACOMODACAO,1,4000),substr(:new.IE_TIPO_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ACOMODACAO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SENHA,1,4000),substr(:new.DS_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'DS_SENHA',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERFACE_AUTORIZACAO,1,4000),substr(:new.CD_INTERFACE_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERFACE_AUTORIZACAO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERFACE_ENVIO,1,4000),substr(:new.CD_INTERFACE_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERFACE_ENVIO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERFACE_RETORNO,1,4000),substr(:new.CD_INTERFACE_RETORNO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERFACE_RETORNO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CODIGO_CONVENIO,1,4000),substr(:new.IE_CODIGO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CODIGO_CONVENIO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_DATA_ULT_PAGTO,1,4000),substr(:new.IE_EXIGE_DATA_ULT_PAGTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_DATA_ULT_PAGTO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONTA_PROTOCOLO,1,4000),substr(:new.QT_CONTA_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONTA_PROTOCOLO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TITULO_RECEBER,1,4000),substr(:new.IE_TITULO_RECEBER,1,4000),:new.nm_usuario,nr_seq_w,'IE_TITULO_RECEBER',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO_MEDIO_MATERIAL,1,4000),substr(:new.IE_PRECO_MEDIO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO_MEDIO_MATERIAL',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DOC_CONVENIO,1,4000),substr(:new.IE_DOC_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DOC_CONVENIO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AGRUP_ITEM_INTERF,1,4000),substr(:new.IE_AGRUP_ITEM_INTERF,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGRUP_ITEM_INTERF',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DOC_RETORNO,1,4000),substr(:new.IE_DOC_RETORNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DOC_RETORNO',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONVERSAO_MAT,1,4000),substr(:new.IE_CONVERSAO_MAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONVERSAO_MAT',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CALC_PORTE,1,4000),substr(:new.IE_CALC_PORTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CALC_PORTE',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_SENHA_ATEND,1,4000),substr(:new.IE_EXIGE_SENHA_ATEND,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_SENHA_ATEND',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOLIC_EXAME_TASYMED,1,4000),substr(:new.IE_SOLIC_EXAME_TASYMED,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLIC_EXAME_TASYMED',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ROTINA_DIGITO_GUIA,1,4000),substr(:new.DS_ROTINA_DIGITO_GUIA,1,4000),:new.nm_usuario,nr_seq_w,'DS_ROTINA_DIGITO_GUIA',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VENC_ULTIMO_DIA,1,4000),substr(:new.IE_VENC_ULTIMO_DIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_VENC_ULTIMO_DIA',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ROTINA_SENHA,1,4000),substr(:new.DS_ROTINA_SENHA,1,4000),:new.nm_usuario,nr_seq_w,'DS_ROTINA_SENHA',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EDICAO_AMB_REFER,1,4000),substr(:new.CD_EDICAO_AMB_REFER,1,4000),:new.nm_usuario,nr_seq_w,'CD_EDICAO_AMB_REFER',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NF_CONTAB_REC_CONV,1,4000),substr(:new.IE_NF_CONTAB_REC_CONV,1,4000),:new.nm_usuario,nr_seq_w,'IE_NF_CONTAB_REC_CONV',ie_log_w,ds_w,'CONVENIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.convenio_bef_insert_update
before insert or update ON TASY.CONVENIO for each row
begin

	if 	(( nvl(:new.ds_arquivo_logo_tiss, 'X') <> nvl(:old.ds_arquivo_logo_tiss,'X'))
	and	(  length(nvl(:new.ds_arquivo_logo_tiss, 'X')) > 1 )
	and 	(( instr( lower( :new.ds_arquivo_logo_tiss ), '.bmp') = 0 ) and ( instr( lower( :new.ds_arquivo_logo_tiss ), '.jpg') = 0 ))) then

		wheb_mensagem_pck.exibir_mensagem_abort(1040653);

	end if;

end;
/


DROP SYNONYM TASY_CONSULTA.CONVENIO;

CREATE SYNONYM TASY_CONSULTA.CONVENIO FOR TASY.CONVENIO;


ALTER TABLE TASY.CONVENIO ADD (
  CHECK ( ie_situacao IN ( 'A' , 'I' )  ),
  CONSTRAINT CONVENI_PK
 PRIMARY KEY
 (CD_CONVENIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO ADD (
  CONSTRAINT CONVENI_CATDERE_FK 
 FOREIGN KEY (CD_TIPO_CONVENIO_MX) 
 REFERENCES TASY.CAT_DERECHOHABIENCIA (NR_SEQUENCIA),
  CONSTRAINT CONVENI_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT CONVENI_INTENV_FK 
 FOREIGN KEY (CD_INTERFACE_ENVIO) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT CONVENI_INTERAU_FK 
 FOREIGN KEY (CD_INTERFACE_AUTORIZACAO) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT CONVENI_INTRET_FK 
 FOREIGN KEY (CD_INTERFACE_RETORNO) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT CONVENI_PROCESS_FK 
 FOREIGN KEY (CD_PROCESSO_ALTA) 
 REFERENCES TASY.PROCESSO (CD_PROCESSO),
  CONSTRAINT CONVENI_CONPAGA_FK 
 FOREIGN KEY (CD_CONDICAO_PAGAMENTO) 
 REFERENCES TASY.CONDICAO_PAGAMENTO (CD_CONDICAO_PAGAMENTO)
    ON DELETE CASCADE,
  CONSTRAINT CONVENI_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT CONVENI_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO_GLOSA, CD_CATEGORIA_GLOSA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT CONVENI_EDIAMB_FK 
 FOREIGN KEY (CD_EDICAO_AMB_REFER) 
 REFERENCES TASY.EDICAO_AMB (CD_EDICAO_AMB));

GRANT SELECT ON TASY.CONVENIO TO NIVEL_1;

GRANT SELECT ON TASY.CONVENIO TO ROBOCMTECNOLOGIA;

GRANT SELECT ON TASY.CONVENIO TO TASY_CONSULTA;

