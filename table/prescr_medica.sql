ALTER TABLE TASY.PRESCR_MEDICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_MEDICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_MEDICA
(
  NR_PRESCRICAO             NUMBER(14)          NOT NULL,
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE)   NOT NULL,
  NR_ATENDIMENTO            NUMBER(10),
  CD_MEDICO                 VARCHAR2(10 BYTE),
  DT_PRESCRICAO             DATE                NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DS_OBSERVACAO             VARCHAR2(2000 BYTE),
  NR_HORAS_VALIDADE         NUMBER(5),
  CD_MOTIVO_BAIXA           NUMBER(3),
  DT_BAIXA                  DATE,
  DT_PRIMEIRO_HORARIO       DATE,
  DT_LIBERACAO              DATE,
  DT_EMISSAO_SETOR          DATE,
  DT_EMISSAO_FARMACIA       DATE,
  CD_SETOR_ATENDIMENTO      NUMBER(5),
  DT_ENTRADA_UNIDADE        DATE,
  IE_RECEM_NATO             VARCHAR2(1 BYTE),
  IE_ORIGEM_INF             VARCHAR2(1 BYTE),
  NR_PRESCRICAO_ANTERIOR    NUMBER(14),
  NM_USUARIO_ORIGINAL       VARCHAR2(15 BYTE),
  IE_MOTIVO_PRESCRICAO      VARCHAR2(3 BYTE),
  DT_LIBERACAO_MEDICO       DATE,
  NR_PRESCRICAO_MAE         NUMBER(14),
  CD_PROTOCOLO              NUMBER(10),
  NR_SEQ_PROTOCOLO          NUMBER(6),
  DT_ENTREGA                DATE,
  CD_SETOR_ENTREGA          NUMBER(5),
  NR_CIRURGIA               NUMBER(10),
  NR_SEQ_AGENDA             NUMBER(10),
  QT_ALTURA_CM              NUMBER(4,1),
  QT_PESO                   NUMBER(6,3),
  DT_MESTRUACAO             DATE,
  DS_DADO_CLINICO           VARCHAR2(255 BYTE),
  NR_CONTROLE               NUMBER(15),
  CD_RECEM_NATO             VARCHAR2(10 BYTE),
  DS_MEDICACAO_USO          VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4),
  NR_DOC_CONV               VARCHAR2(20 BYTE),
  CD_PRESCRITOR             VARCHAR2(10 BYTE),
  NR_PRIORIDADE             NUMBER(3),
  DS_DE_ATE                 VARCHAR2(10 BYTE),
  DT_SUSPENSAO              DATE,
  NM_USUARIO_SUSP           VARCHAR2(15 BYTE),
  CD_SENHA                  VARCHAR2(20 BYTE),
  IE_EMERGENCIA             VARCHAR2(1 BYTE),
  NR_SEQ_FORMA_LAUDO        NUMBER(10),
  NR_SEQ_AGECONS            NUMBER(10),
  IE_FUNCAO_PRESCRITOR      VARCHAR2(3 BYTE),
  DT_FIM_PRESCRICAO         DATE,
  IE_PRESCRICAO_ALTA        VARCHAR2(1 BYTE),
  DT_LIBERACAO_FARMACIA     DATE,
  IE_LIB_FARM               VARCHAR2(1 BYTE),
  CD_LOCAL_ESTOQUE          NUMBER(4),
  DT_VALIDADE_PRESCR        DATE,
  DT_INICIO_PRESCR          DATE,
  IE_ADEP                   VARCHAR2(1 BYTE),
  IE_PRESCR_EMERGENCIA      VARCHAR2(1 BYTE),
  IE_HEMODIALISE            VARCHAR2(1 BYTE),
  NM_USUARIO_IMP_FAR        VARCHAR2(20 BYTE),
  CD_FARMAC_LIB             VARCHAR2(10 BYTE),
  QT_CREATININA             NUMBER(15,2),
  QT_CLEARENCE              NUMBER(15,2),
  IE_CALCULO_CLEARENCE      VARCHAR2(15 BYTE),
  NM_USUARIO_LIB_ENF        VARCHAR2(15 BYTE),
  NR_SEQ_CLEARENCE_PEP      NUMBER(10),
  NR_SEQ_STATUS_FARM        NUMBER(10),
  DS_JUSTIFICATIVA          VARCHAR2(2000 BYTE),
  DS_JUSTIF_IMP_ADEP        VARCHAR2(255 BYTE),
  NM_USUARIO_IMP_ADEP       VARCHAR2(15 BYTE),
  DT_IMP_ADEP               DATE,
  NR_SEQ_TRANSCRICAO        NUMBER(10),
  DT_REVISAO                DATE,
  NM_USUARIO_REVISAO        VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_SUSP        NUMBER(10),
  DS_MOTIVO_SUSP            VARCHAR2(255 BYTE),
  DS_OBS_ENFERMAGEM         VARCHAR2(2000 BYTE),
  NR_SEQ_ASSINATURA         NUMBER(10),
  NR_CONTROLE_LAB           VARCHAR2(20 BYTE),
  IE_GEROU_KIT              VARCHAR2(1 BYTE),
  DT_CONTROLE_PRESCR        DATE,
  CD_CGC_SOLIC              VARCHAR2(14 BYTE),
  CD_PERFIL_ATIVO           NUMBER(5),
  IE_PRESCR_FARM            VARCHAR2(1 BYTE),
  NR_PRESCRICOES            VARCHAR2(255 BYTE),
  IE_PRESCRITOR_AUX         VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX          DATE,
  IE_TIPO_PRESCR_CIRUR      VARCHAR2(15 BYTE),
  NR_PRESCRICAO_ORIGINAL    NUMBER(14),
  NR_CIRURGIA_PATOLOGIA     NUMBER(10),
  IE_PRESCR_NUTRICAO        VARCHAR2(1 BYTE),
  IE_PRESCRICAO_SAE         VARCHAR2(1 BYTE),
  NR_SEQ_ATEND              NUMBER(10),
  DT_INICIO_ANALISE_FARM    DATE,
  NM_USUARIO_ANALISE_FARM   VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_FUTURO       NUMBER(10),
  IE_PRESCRICAO_IDENTICA    VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE          NUMBER(5),
  NR_PRESCR_INTERF_FARM     NUMBER(14),
  DS_OBSERVACAO_COPIA       VARCHAR2(4000 BYTE),
  NR_PRESCR_ORIG_FUT        NUMBER(14),
  IE_MODO_INTEGRACAO        VARCHAR2(5 BYTE),
  DS_ENDERECO_ENTREGA       VARCHAR2(255 BYTE),
  NR_SEQ_ATECACO            NUMBER(10),
  CD_SETOR_ORIG             NUMBER(5),
  CD_UNID_BASICA            VARCHAR2(10 BYTE),
  CD_UNID_COMPL             VARCHAR2(10 BYTE),
  NR_SEQ_REGRA_COPIA        NUMBER(15),
  IE_EXAME_ANTERIOR         VARCHAR2(1 BYTE),
  DS_EXAME_ANTERIOR         VARCHAR2(255 BYTE),
  DT_IMPRESSAO              DATE,
  DT_PRESCRICAO_ORIGINAL    DATE,
  NR_SEQ_JUSTIFICATIVA      NUMBER(10),
  NR_SEQ_ASSINATURA_FARM    NUMBER(10),
  NR_SEQ_ASSINATURA_ENF     NUMBER(10),
  DT_IGNORA_PRESCR_NUT      DATE,
  NM_USUARIO_IGNORA_NUT     VARCHAR2(15 BYTE),
  DT_IMPRESSAO_EXAME        DATE,
  CD_FUNCAO_ORIGEM          NUMBER(10),
  IE_LIBERACAO_FINAL        VARCHAR2(1 BYTE),
  NR_SEQ_PEPO               NUMBER(10),
  DT_REP_PT                 DATE,
  NR_SEQ_PEND_PAC_ACAO      NUMBER(10),
  DT_REP_PT2                DATE,
  NR_SEQ_ASSINATURA_SUSP    NUMBER(10),
  NR_SEQ_EXTERNO            NUMBER(10),
  NR_RECEM_NATO             NUMBER(3),
  DT_ENDOSSO                DATE,
  NR_SEQ_CONSULTA_OFT       NUMBER(10),
  DS_STACK                  VARCHAR2(2000 BYTE),
  DT_SOLIC_PRESCR           DATE,
  DS_ITENS_PRESCR           VARCHAR2(255 BYTE),
  QT_TEMPO_JEJUM_REAL       NUMBER(10),
  QT_CARACTER_ESPACO        NUMBER(3),
  QT_DIAS_EXTENSAO          NUMBER(5),
  IE_CARTAO_EMERGENCIA      VARCHAR2(1 BYTE),
  IE_ESTENDIDA_LIBERACAO    VARCHAR2(1 BYTE),
  NR_SEQ_FICHA_LOTE         NUMBER(10),
  NR_SEQ_LOTE_ENTRADA       NUMBER(10),
  IE_OPCAO_HORARIO          NUMBER(1),
  IE_PACIENTE_DIALISE       VARCHAR2(15 BYTE),
  NR_CONTROLE_INT_LAB       NUMBER(15),
  DT_ENVIO_NPT              DATE,
  IE_AJUSTA_VALIDADE_ATEND  VARCHAR2(1 BYTE),
  QT_HOR_EXAME_VANCO        NUMBER(10),
  NM_MEDICO_EXTERNO         VARCHAR2(60 BYTE),
  CRM_MEDICO_EXTERNO        VARCHAR2(60 BYTE),
  NM_MAQUINA                VARCHAR2(255 BYTE),
  NR_SEQ_STATUS_OPM         NUMBER(10),
  DS_PEDIDO_EXT             VARCHAR2(40 BYTE),
  DT_IMP_ETIQ_COL           DATE,
  DT_EMISSAO_REQ_LAB        DATE,
  DT_LIBERACAO_PARC_FARM    DATE,
  CD_FARMAC_LIB_PARC        VARCHAR2(10 BYTE),
  IE_NIVEL_ATENCAO          VARCHAR2(1 BYTE),
  DS_UTC                    VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO          VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO        VARCHAR2(50 BYTE),
  IE_PRESCRICAO_DUPLICADA   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          191M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRESMED_AGECONS_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_AGECONS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_AGEPACI_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_ATECACO_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_ATECACO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_ATECACO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_ATEPACFU_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_ATEND_FUTURO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_ATEPACI_FK_I ON TASY.PRESCR_MEDICA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          28M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_CIRURGI_FK_I ON TASY.PRESCR_MEDICA
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_ESPMEDI_FK_I ON TASY.PRESCR_MEDICA
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_ESTABEL_FK_I ON TASY.PRESCR_MEDICA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          17M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_FORENLA_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_FORMA_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_FORENLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_GQAPPAC_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_PEND_PAC_ACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_GQAPPAC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_I1 ON TASY.PRESCR_MEDICA
(DT_PRESCRICAO, CD_PESSOA_FISICA, NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          80M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I10 ON TASY.PRESCR_MEDICA
(CD_PESSOA_FISICA, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          984K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I11 ON TASY.PRESCR_MEDICA
(DT_INICIO_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_I11
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_I12 ON TASY.PRESCR_MEDICA
(NM_USUARIO_ANALISE_FARM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          736K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_I12
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_I13 ON TASY.PRESCR_MEDICA
(DT_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_I13
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_I14 ON TASY.PRESCR_MEDICA
(NR_ATENDIMENTO, DT_VALIDADE_PRESCR)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I15 ON TASY.PRESCR_MEDICA
(CD_PESSOA_FISICA, NR_ATENDIMENTO, DT_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1032K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I16 ON TASY.PRESCR_MEDICA
(DT_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I17 ON TASY.PRESCR_MEDICA
(NR_ATENDIMENTO, DT_INICIO_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I18 ON TASY.PRESCR_MEDICA
(NR_ATENDIMENTO, NVL("IE_RECEM_NATO",'N'), NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I19 ON TASY.PRESCR_MEDICA
(NR_CIRURGIA_PATOLOGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I2 ON TASY.PRESCR_MEDICA
(NR_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_I2
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_I20 ON TASY.PRESCR_MEDICA
(NR_PRESCRICAO, DT_LIBERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I21 ON TASY.PRESCR_MEDICA
(NM_USUARIO_ORIGINAL, CD_PESSOA_FISICA, DT_LIBERACAO, DT_LIBERACAO_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I22 ON TASY.PRESCR_MEDICA
(NR_PRESCRICAO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I3 ON TASY.PRESCR_MEDICA
(IE_EMERGENCIA, DT_LIBERACAO, DT_LIBERACAO_MEDICO, CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          42M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I4 ON TASY.PRESCR_MEDICA
(CD_ESTABELECIMENTO, NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          38M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I5 ON TASY.PRESCR_MEDICA
(NR_ATENDIMENTO, DT_PRESCRICAO, CD_MEDICO, NM_USUARIO_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I6 ON TASY.PRESCR_MEDICA
(NR_ATENDIMENTO, DT_LIBERACAO, DT_VALIDADE_PRESCR, IE_ADEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          42M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I7 ON TASY.PRESCR_MEDICA
(DT_LIBERACAO, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_I7
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_I8 ON TASY.PRESCR_MEDICA
(NR_PRESCRICAO, NVL("IE_ADEP",'S'))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          28M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_I9 ON TASY.PRESCR_MEDICA
(DT_PRESCRICAO, NR_ATENDIMENTO, CD_PRESCRITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          45M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_JUSALTA_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_JUSTIFICATIVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_JUSALTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_LESECFI_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_FICHA_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_LESECFI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_LESEC_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_LOTE_ENTRADA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_LESEC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_LOCESTO_FK_I ON TASY.PRESCR_MEDICA
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_LOCESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_MOTSUPR_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_MOTSUPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_OFTCONS_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_CONSULTA_OFT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_OFTCONS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_PACCLCR_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_CLEARENCE_PEP)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_PACCLCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_PEPOCIR_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PERFIL_FK_I ON TASY.PRESCR_MEDICA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_PESFISI_FK_I ON TASY.PRESCR_MEDICA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PESFISI_FK2_I ON TASY.PRESCR_MEDICA
(CD_RECEM_NATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PESFISI_FK3_I ON TASY.PRESCR_MEDICA
(CD_FARMAC_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PESFISI_FK4_I ON TASY.PRESCR_MEDICA
(CD_FARMAC_LIB_PARC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PESFISI_FK9_I ON TASY.PRESCR_MEDICA
(CD_PRESCRITOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          19M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PESFISM_FK_I ON TASY.PRESCR_MEDICA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          88K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PESJURI_FK_I ON TASY.PRESCR_MEDICA
(CD_CGC_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          688K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRESMED_PK ON TASY.PRESCR_MEDICA
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          23M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PRESMED_FK_I ON TASY.PRESCR_MEDICA
(NR_PRESCRICAO_ANTERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PRESMED2_FK_I ON TASY.PRESCR_MEDICA
(NR_PRESCRICAO_MAE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_PRESMED3_FK_I ON TASY.PRESCR_MEDICA
(NR_PRESCRICAO_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          688K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_PRESMED3_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_PRESMED4_FK_I ON TASY.PRESCR_MEDICA
(NR_PRESCR_INTERF_FARM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          688K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_PRESMED4_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_PROMEDI_FK_I ON TASY.PRESCR_MEDICA
(CD_PROTOCOLO, NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_PROMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_SETATEN_FK_I ON TASY.PRESCR_MEDICA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          48K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRESMED_SETATEN_FK2_I ON TASY.PRESCR_MEDICA
(CD_SETOR_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          248K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_SETATEN2_FK ON TASY.PRESCR_MEDICA
(CD_SETOR_ENTREGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          17M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_SETATEN2_FK
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_STAPREF_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_STATUS_FARM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_STAPREF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_STPREOP_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_STATUS_OPM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_STPREOP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_TASAJVP2_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_ASSINATURA_FARM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_TASAJVP2_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_TASASDI_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_TASASDI3_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_ASSINATURA_ENF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_TASASDI3_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_TRAPRES_FK_I ON TASY.PRESCR_MEDICA
(NR_SEQ_TRANSCRICAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRESMED_TRAPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRESMED_15 ON TASY.PRESCR_MEDICA
(NR_ATENDIMENTO, DT_LIBERACAO_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          736K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.prescr_medica_delete
BEFORE DELETE ON TASY.PRESCR_MEDICA 
FOR EACH ROW
DECLARE

ie_muda_status_w	varchar2(1);
ds_erro_w		varchar2(4000);
ie_excl_rep_lib_w	varchar2(1);
qt_prescr_w		number;
ie_rep_cpoe_w		parametro_medico.ie_rep_cpoe%type;

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

	obter_param_usuario(1700,33,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_muda_status_w);
	obter_param_usuario(916,945,obter_perfil_ativo, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_excl_rep_lib_w);
		
	if	(:old.nr_seq_transcricao is not null) and 
		(nvl(ie_muda_status_w,'S') = 'S') then		/*David em 31/07/2008 OS 100500*/
		update	transcricao_prescricao
		set	ie_status	= 'S',
			nm_usuario_transcricao	= null,
			dt_transcricao		= null
		where	nr_sequencia	= :old.nr_seq_transcricao;
	end if;

	if	((ie_excl_rep_lib_w = 'N') or
		 (wheb_usuario_pck.get_cd_funcao = 924) or
		 (wheb_usuario_pck.get_cd_funcao = 950)) and
		((nvl(:new.dt_liberacao, :new.dt_liberacao_medico) is not null) or
		 (nvl(:old.dt_liberacao, :old.dt_liberacao_medico) is not null)) then
		Wheb_mensagem_pck.exibir_mensagem_abort(193074);
	end if;
	
	select	nvl(max(ie_rep_cpoe),'N')
	into	ie_rep_cpoe_w
	from 	parametro_medico
	where 	cd_estabelecimento  = wheb_usuario_pck.get_cd_estabelecimento;
	
	if (ie_rep_cpoe_w = 'S') then
		gravar_log_cpoe(substr('PRESCR_MEDICA_DELETE TRIGGER '
				  || 'nr_prescricao: ' || :old.nr_prescricao 
							  || 'nr_atendimento: ' || :old.nr_atendimento, 1,2000), :old.nr_atendimento);
	end if;
						  
	/* Francisco - 12/11/2009 - OS 178491 */
	update	autorizacao_cirurgia
	set	nr_prescricao	= null
	where	nr_prescricao is not null
	and	nr_prescricao	= :old.nr_prescricao; 

	update 	autorizacao_convenio
	set	nr_prescricao = null
	where	nr_prescricao is not null
	and	nr_prescricao = :old.nr_prescricao;

	update	ageint_exame_lab
	set	nr_prescricao = null
	where	nr_prescricao = :old.nr_prescricao;

	update 	paciente_atendimento
	set		nr_prescricao = null
	where	nr_prescricao = :old.nr_prescricao;

	/*Desvincular a prescricao e o atendimento da prevenda, quando deleta*/
	update	pre_venda_item
	set	nr_atendimento = null,
		nr_prescricao = null,
		nr_seq_interno = null
	where	nr_prescricao = :new.nr_prescricao;
	
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.hsj_prescr_medica_pend_resp
after insert or update or delete ON TASY.PRESCR_MEDICA for each row
declare

nm_usuario_w varchar2(50);
jobno number(10);

begin


    if(inserting or updating)then
    
        if(nvl(:new.ie_prescritor_aux,'N') = 'S')then

            if(:new.dt_liberacao_aux is not null)then 
                
                -- limpar pendencias do m�dico auxiliar
                nm_usuario_w := :new.nm_usuario;
                gerar_registro_pendente_pep('XREP', :new.nr_prescricao, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_w,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
            
                if(:new.dt_liberacao is null)then
                
                    -- gerar pendencia para o m�dico da prescri��o
                    nm_usuario_w := hsj_obter_pf_usuario_medico(:new.cd_medico);
                    gerar_registro_pendente_pep('REP', :new.nr_prescricao, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_w,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
                    
                else 
                
                    -- se o registro possuir data de libera��o, limpar pendencias do m�dico da prescri��o
                    nm_usuario_w := hsj_obter_pf_usuario_medico(:new.cd_medico);
                    gerar_registro_pendente_pep('XREP', :new.nr_prescricao, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_w,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
                    
                end if;
                
            else
            
                -- se o registro n�o possuir libera��o do auxiliar, gerar pendencia para o mesmo
                nm_usuario_w := :new.nm_usuario;
                gerar_registro_pendente_pep('REP', :new.nr_prescricao, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_w,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
            
            end if;
        
        end if;
        
        
        if(:old.dt_liberacao is null and :new.dt_liberacao is not null and :new.dt_suspensao is null)then
        
            if(:new.nr_seq_atend is not null)then
                dbms_job.submit(jobno, 'LIBERAR_PRESCRICAO_FARMACIA('||:new.nr_prescricao||',0,'''||obter_usuario_ativo||''', null,null);');
            end if;
        
        end if;
        
    elsif(deleting)then
    
        -- limpar pendencias do m�dico auxiliar
        nm_usuario_w := :old.nm_usuario;
        gerar_registro_pendente_pep('XREP', :old.nr_prescricao, :old.cd_pessoa_fisica, :old.nr_atendimento, nm_usuario_w,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
         
        -- limpar pendencias do m�dico da prescri��o
        nm_usuario_w := hsj_obter_pf_usuario_medico(:old.cd_medico);
        gerar_registro_pendente_pep('XREP', :old.nr_prescricao, :old.cd_pessoa_fisica, :old.nr_atendimento, nm_usuario_w,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
        
    end if;  
end;
/


CREATE OR REPLACE TRIGGER TASY.prescr_medica_scc_upd BEFORE
    UPDATE OF cd_medico ON TASY.PRESCR_MEDICA     FOR EACH ROW
DECLARE
    ds_param_integ_hl7_scc_w   VARCHAR2(2000);
    nr_sequencia_scc_w         prescr_procedimento.nr_sequencia%TYPE;
BEGIN
    IF ( wheb_usuario_pck.is_evento_ativo(942) = 'S' ) AND ( :new.dt_liberacao IS NOT NULL ) AND ( :new.cd_medico <> :old.cd_medico ) THEN
        SELECT MAX(a.nr_sequencia)
        INTO nr_sequencia_scc_w
        FROM
            prescr_procedimento   a,
            lab_exame_equip       e,
            equipamento_lab       f,
            exame_laboratorio     c
        WHERE
            a.nr_seq_exame = c.nr_seq_exame
            AND e.cd_equipamento = f.cd_equipamento
            AND f.ds_sigla = 'SCC'
            AND a.nr_seq_exame = e.nr_seq_exame
            AND a.nr_prescricao = :new.nr_prescricao
            AND a.dt_cancelamento IS NULL
            AND a.dt_suspensao IS NULL
            AND a.dt_envio_integracao IS NOT NULL
            AND ROWNUM = 1;

        IF ( nr_sequencia_scc_w IS NOT NULL ) THEN
            ds_param_integ_hl7_scc_w := 'NR_PRESCRICAO_P='
                                        || :new.nr_prescricao
                                        || '#@#@NR_PRESCR_SEQ_P='
                                        || nr_sequencia_scc_w
                                        || '#@#@IE_ALTERACAO_MEDICO_P='
                                        || 'S'
                                        || '#@#@';

            gravar_agend_integracao(942, ds_param_integ_hl7_scc_w);
        END IF;

    END IF;
END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_medica_upd_hl7_dixtal
before update or insert ON TASY.PRESCR_MEDICA for each row
declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);

nr_seq_interno_w	number(10);
qt_doenca_w		number(10);
nr_seq_proc_w		number(10);
nr_seq_bco_w		number(10);
nr_seq_nut_p_w		number(10);
nr_seq_nut_pac_w	number(10);
nr_seq_enteral_w	number(10);
nr_seq_gaso_w		number(10);
nr_seq_recomendacao_w	number(10);
nr_seq_oxig_w		number(10);
nr_seq_dieta_w		number(10);
nr_seq_jejum_w		number(10);
nr_seq_doenca_w		number(10);
ie_leito_monit		varchar2(1);

cursor c01 is
	select	nr_sequencia
	from	prescr_procedimento
	where	nr_prescricao = :new.nr_prescricao
	and 	ie_tipo_proced <> 'G'
	order by 1;

cursor c02 is
	select	nr_sequencia
	from	prescr_solic_bco_sangue
	where	nr_prescricao = :new.nr_prescricao
	order by 1;

cursor c03 is
	select	nr_sequencia
	from	nut_pac
	where	nr_prescricao = :new.nr_prescricao
	order by 1;

cursor c04 is
	select	nr_sequencia
	from	nut_paciente
	where	nr_prescricao = :new.nr_prescricao
	order by 1;

cursor c05 is
	select	nr_sequencia
	from	prescr_material
	where	nr_prescricao = :new.nr_prescricao
	and	ie_agrupador = 8
	order by 1;

cursor c06 is
	select	nr_sequencia
	from	prescr_gasoterapia
	where	nr_prescricao = :new.nr_prescricao
	order by 1;

cursor c07 is
	select	nr_sequencia
	from	prescr_recomendacao
	where	nr_prescricao = :new.nr_prescricao
	order by 1;

cursor c08 is
	select	nr_sequencia
	from	prescr_procedimento
	where	nr_prescricao = :new.nr_prescricao
	and	ie_tipo_proced = 'G'
	order by 1;

cursor c09 is
	select	nr_sequencia
	from	prescr_dieta
	where	nr_prescricao = :new.nr_prescricao
	order by 1;

cursor c10 is
	select	nr_sequencia
	from	rep_jejum
	where	nr_prescricao = :new.nr_prescricao
	order by 1;

/*cursor c11 is
	select	nr_sequencia
	from	paciente_antec_clinico
	where 	cd_pessoa_fisica = :new.cd_pessoa_fisica
	order by 1;	*/

begin

if	--((:new.dt_liberacao	is not null) and (:old.dt_liberacao	is null)) or
	(:new.dt_liberacao_medico is not null) and (:old.dt_liberacao_medico  is null) then

	ds_sep_bv_w := obter_separador_bv;

	select	max(obter_atepacu_paciente( :new.nr_atendimento ,'A'))
	into	nr_seq_interno_w
	from	dual;

	select	nvl(obter_se_leito_atual_monit(:new.nr_atendimento),'N')
	into	ie_leito_monit
	from 	dual;

	open C01;
	loop
	fetch C01 into
		nr_seq_proc_w;
	exit when C01%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_proc='	    || nr_seq_proc_w	     || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into
		nr_seq_bco_w;
	exit when C02%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_bco='	    || nr_seq_bco_w	     || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C02;

	open C03;
	loop
	fetch C03 into
		nr_seq_nut_p_w;
	exit when C03%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_nut_p='	    || nr_seq_nut_p_w	     || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C03;

	open C04;
	loop
	fetch C04 into
		nr_seq_nut_pac_w;
	exit when C04%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_nut_pac='   || nr_seq_nut_pac_w	     || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C04;

	open C05;
	loop
	fetch C05 into
		nr_seq_enteral_w;
	exit when C05%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_enteral='   || nr_seq_enteral_w	     || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C05;

	open C06;
	loop
	fetch C06 into
		nr_seq_gaso_w;
	exit when C06%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_gaso='	    || nr_seq_gaso_w	     || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C06;

	open C07;
	loop
	fetch C07 into
		nr_seq_recomendacao_w;
	exit when C07%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_recomend='  || nr_seq_recomendacao_w || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C07;

	open C08;
	loop
	fetch C08 into
		nr_seq_oxig_w;
	exit when C08%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_oxig='      || nr_seq_oxig_w 	     || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C08;


	select 	count(*)
	into	qt_doenca_w
	from 	paciente_antec_clinico
	where 	cd_pessoa_fisica = :new.cd_pessoa_fisica;

	open C09;
	loop
	fetch C09 into
		nr_seq_dieta_w;
	exit when C09%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_dieta='     || nr_seq_dieta_w 	     || ds_sep_bv_w;
			gravar_agend_integracao(51, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C09;

	open C10;
	loop
	fetch C10 into
		nr_seq_jejum_w;
	exit when C10%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_jejum='     || nr_seq_jejum_w 	     || ds_sep_bv_w;
			gravar_agend_integracao(51, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C10;

	/*open C11;
	loop
	fetch C11 into
		nr_seq_doenca_w;
	exit when C11%notfound;

		begin
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
					'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
					'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
					'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
					'nr_seq_doenca='    || nr_seq_doenca_w 	     || ds_sep_bv_w;
		gravar_agend_integracao(52, ds_param_integ_hl7_w);

		end;
	end loop;
	close C11;*/


	if  (qt_doenca_w > 0) and
		(ie_leito_monit = 'S') then
		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
					'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
					'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
					'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ;

		gravar_agend_integracao(52, ds_param_integ_hl7_w);

	end if;
end if;

if 	(:new.dt_suspensao is not null) and (:old.dt_suspensao  is null) then

	ds_sep_bv_w := obter_separador_bv;

	select	max(obter_atepacu_paciente( :new.nr_atendimento ,'A'))
	into	nr_seq_interno_w
	from	dual;

	select	nvl(obter_se_leito_atual_monit(:new.nr_atendimento),'N')
	into	ie_leito_monit
	from 	dual;

	open C02;
	loop
	fetch C02 into
		nr_seq_bco_w;
	exit when C02%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_bco='	    || nr_seq_bco_w	     || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C02;

	open C03;
	loop
	fetch C03 into
		nr_seq_nut_p_w;
	exit when C03%notfound;

		begin
		if (ie_leito_monit = 'S') then
			begin
			ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
						'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
						'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
						'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
						'nr_seq_nut_p='	    || nr_seq_nut_p_w	     || ds_sep_bv_w;
			gravar_agend_integracao(50, ds_param_integ_hl7_w);
			end;
		end if;
		end;
	end loop;
	close C03;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.prescr_medica_insert
before insert ON TASY.PRESCR_MEDICA for each row
declare
dt_entrada_w					date;
nr_seq_forma_laudo_w			number(10);
qt_medico_w						number(10);
ie_tipo_atendimento_w			number(03);
qt_desfecho_w					number(10);
ie_sem_atend_w					varchar2(1);
hr_prescr_copia_w				varchar2(10);
ds_erro_w						varchar2(255);
ie_considera_minuto_w			varchar2(1);
ie_prescr_validade_w			varchar2(1);
ie_minutos_rep_w				varchar2(1);
dt_rep_pt_w						date;
dt_rep_pt2_w					date;
dt_prim_setor_w					date;
ie_inicia_dia_plano_w			varchar2(1);
ie_copia_guia_senha_w			varchar2(1);
ie_calcula_validade_w		varchar2(1);
nr_doc_convenio_w				varchar2(20);
cd_senha_w						varchar2(20);
cd_funcao_w						number(10);
qt_caracter_espaco_w			number(10);
ie_prescr_validade_cir_w		varchar2(1);
ie_permite_guia_senha_atend_w	varchar2(1);
nm_maquina_w					varchar2(255);
cd_estabelecimento_setor_w		estabelecimento.cd_estabelecimento%type;

begin

begin
if	(coalesce(:new.cd_funcao_origem,0) = 2314) and
	(:new.cd_setor_atendimento is not null) then
	cd_estabelecimento_setor_w := obter_estabelecimento_setor(:new.cd_setor_atendimento);
	if (nvl(:new.cd_estabelecimento,0) <> cd_estabelecimento_setor_w) then
		:new.cd_estabelecimento := cd_estabelecimento_setor_w;
	end if;
end if;

exception when others then
	null;
end;

wheb_assist_pck.set_informacoes_usuario(:new.cd_estabelecimento, obter_perfil_ativo, :new.nm_usuario);
wheb_assist_pck.obterValorParametroREP(1033, qt_caracter_espaco_w);

Obter_Param_Usuario(916, 834,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_copia_guia_senha_w);
Obter_Param_Usuario(900, 419,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_prescr_validade_cir_w);
Obter_Param_Usuario(924, 753,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_prescr_validade_w);

if	(coalesce(:new.cd_funcao_origem,0) <> 8030) then
	cd_funcao_w	:= coalesce(obter_funcao_ativa,0);
else
	cd_funcao_w := 8030;
end if;

if	(cd_funcao_w > 0) then
	:new.cd_funcao_origem	:= cd_funcao_w;
end if;

if	(qt_caracter_espaco_w > 1) then
	:new.qt_caracter_espaco	:= qt_caracter_espaco_w;
end if;

if	(to_char(:new.dt_primeiro_horario,'ss') <> '00') then
	:new.dt_primeiro_horario	:= trunc(:new.dt_primeiro_horario,'mi') + 1/1440;
	if	(cd_funcao_w = 8030) then
		:new.nr_horas_validade		:= Obter_Horas_Valid_Prescr_Trig(:new.dt_primeiro_horario,:new.nr_atendimento, wheb_assist_pck.obterParametroFuncao(924, 249), 'A',:new.dt_prescricao, :new.nr_prescricao, :new.cd_setor_atendimento);
	end if;
end if;

Consistir_impedimento_pf(:new.cd_medico,'REP',:new.nm_usuario);
if	(:new.cd_prescritor is not null) then
	Consistir_impedimento_pf(:new.cd_prescritor,'REP',:new.nm_usuario);
end if;

if	(:new.nr_atendimento > 0) then
	select	dt_entrada,
			nr_seq_forma_laudo,
			ie_tipo_atendimento
	into	dt_entrada_w,
			nr_seq_forma_laudo_w,
			ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	if	(:new.nr_seq_forma_laudo is null) and
		(nr_seq_forma_laudo_w is not null) then
		:new.nr_seq_forma_laudo	:= nr_seq_forma_laudo_w;
	end if;

	if  	(cd_funcao_w in (871,900)) and
		(dt_entrada_w > :new.dt_prescricao) and
		(ie_prescr_validade_cir_w = 'S') then
			--A data da prescri��o n�o pode ser menor que a data de entrada do atendimento. #@NR_ATENDIMENTO#@
			Wheb_mensagem_pck.exibir_mensagem_abort(173308,'NR_ATENDIMENTO=' || :new.nr_atendimento);

	elsif	(cd_funcao_w not in (871,900,916,3130)) and
		(dt_entrada_w > :new.dt_prescricao) and
		(ie_prescr_validade_w = 'N') then
			----A data da prescri��o n�o pode ser menor que a data de entrada do atendimento. #@NR_ATENDIMENTO#@
			Wheb_mensagem_pck.exibir_mensagem_abort(173308,'NR_ATENDIMENTO=' || :new.nr_atendimento);
	end if;


	Obter_Param_Usuario(924,178,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_sem_atend_w);
	--Obter_Param_Usuario(924,178,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,ie_copia_peso_altura_w);
	if	(ie_tipo_atendimento_w = 3) and
		(ie_sem_atend_w = 'S') then
        	begin
        	select  count(b.nr_sequencia)
	        into    qt_desfecho_w
	        from    atendimento_alta b, parametro_medico p
	        where   nr_atendimento  = :new.nr_atendimento
			and		ie_desfecho in ('I','A')
			and		ie_tipo_orientacao = 'P'
			and     p.cd_estabelecimento = obter_estabelecimento_ativo
			and     ((nvl(p.ie_liberar_desfecho,'N')  = 'N')
			or      ((b.dt_liberacao is not null) and (b.dt_inativacao is null)));

	        if      (qt_desfecho_w > 0) then
        	        :new.nr_atendimento     := null;
	        end if;
        	end;
	end if;

end if;

If	(ie_prescr_validade_w = 'S') and
	(:new.IE_PRESCR_EMERGENCIA = 'S')then
	ie_prescr_validade_w	:= 'N';
end if;

if	(nvl(:new.qt_peso,0) = 0) and
	(coalesce(wheb_assist_pck.obterParametroFuncao(924, 380),'S') = 'S') then
	begin
		:new.qt_peso	:= obter_ultimo_sinal_vital_pf(:new.cd_pessoa_fisica,'Peso');
	exception when others then
		null;
	end;
end if;

if	(coalesce(:new.qt_altura_cm,0) = 0) and
	(coalesce(wheb_assist_pck.obterParametroFuncao(924, 381),'S') = 'S') then
	:new.qt_altura_cm	:= obter_sinal_vital(:new.nr_atendimento,'Altura');
end if;

if	(:new.cd_especialidade is null) then
	:new.cd_especialidade	:= wheb_usuario_pck.get_cd_especialidade;
end if;

if	(to_char(:new.dt_prescricao,'dd/mm/yyyy') = '30/12/1899') then
	--A data da prescri��o informada n�o � v�lida. #@DT_PRESCRICAO#@
	Wheb_mensagem_pck.exibir_mensagem_abort(173273, 'DT_PRESCRICAO=' || to_char(:new.dt_prescricao,'dd/mm/yyyy hh24:mi:ss'));
end if;

begin
:new.cd_setor_orig	:= Obter_Setor_Atendimento(:new.nr_atendimento);
:new.cd_unid_compl	:= Obter_Unidade_Atendimento(:new.nr_atendimento, 'A', 'UC');
:new.cd_unid_basica	:= Obter_Unidade_Atendimento(:new.nr_atendimento, 'A', 'UB');
:new.dt_prescricao_original	:= sysdate;
exception when others then
	ds_erro_w	:= null;
end;

select	count(*)
into	qt_medico_w
from	medico
where	cd_pessoa_fisica	= :new.cd_medico;

if	(qt_medico_w = 0) and
	(:new.cd_medico is not null) and
	(wheb_assist_pck.obterParametroFuncao(924, 1037) = 'S') then
	--A pessoa informada no campo M�dico n�o esta cadastrada como m�dico.
	Wheb_mensagem_pck.exibir_mensagem_abort(173291);
end if;

begin
	select	ie_tipo_evolucao
	into	:new.ie_funcao_prescritor
	from	usuario
	where	nm_usuario	= :new.nm_usuario_original;
exception when others then
	:new.ie_funcao_prescritor	:= null;
end;

if	(ie_prescr_validade_w <> 'S') then
	wheb_assist_pck.obterValorParametroREP(532, hr_prescr_copia_w);
	if	(hr_prescr_copia_w is null) or
		(to_char(:new.dt_primeiro_horario,'hh24:mi') <> hr_prescr_copia_w) then
		begin
		/* Rafael / Edilson em 17/07/2007 */
		if	(:new.dt_primeiro_horario is not null) then
			if	(to_char(:new.dt_primeiro_horario,'hh24:mi:ss') < to_char(:new.dt_prescricao,'hh24:mi:ss')) then
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao+1,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			else
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			end if;
		else
			:new.dt_inicio_prescr := :new.dt_prescricao;
		end if;

		end;
	else
		begin
		if	(:new.dt_primeiro_horario is not null) then --OS195107
			:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
		else
			:new.dt_inicio_prescr := :new.dt_prescricao;
		end if;

		end;
	end if;
elsif	(ie_prescr_validade_w = 'S') then
	if	(:new.dt_inicio_prescr is null)  then
		if	(:new.dt_primeiro_horario is not null) then
			if	(to_char(:new.dt_primeiro_horario,'hh24:mi:ss') < to_char(:new.dt_prescricao,'hh24:mi:ss')) then
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao+1,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			else
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			end if;
		else
			:new.dt_inicio_prescr := :new.dt_prescricao;
		end if;
	elsif (:new.dt_inicio_prescr < to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss'))then
		:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
	end if;
end if;

select	max(hr_inicio_prescricao),
		nvl(max(ie_minutos_rep),'S')
into	dt_prim_setor_w,
		ie_minutos_rep_w
from	setor_atendimento
where	cd_setor_atendimento = :new.cd_setor_atendimento;

if	(nvl(:new.ie_hemodialise,'N') not in ('E','S')) then
	ie_calcula_validade_w	:= 'S';
else
	wheb_assist_pck.obterValorParametroREP(1016, ie_calcula_validade_w);
end if;

if	(ie_calcula_validade_w	= 'S') then
	:new.dt_validade_prescr := :new.dt_inicio_prescr + nvl(:new.nr_horas_validade,24) / 24;

	wheb_assist_pck.obterValorParametroREP(689, ie_considera_minuto_w);
	if	(ie_considera_minuto_w = 'S') and
		(nvl(:new.cd_funcao_origem,924) <> 950) then
		:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'mi') - 1/86400;
	elsif	(ie_considera_minuto_w	= 'D') and
		(ie_minutos_rep_w	= 'S') and
		(nvl(:new.cd_funcao_origem,924) <> 950) then
		:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'mi') - 1/86400;
	else
		:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'hh24') - 1/86400;
	end if;
end if;

if	((:new.nr_seq_transcricao is not null) and (:new.nr_prescricao_original is null)) then

	update	transcricao_prescricao
	set		ie_status	= 'E'
	where	nr_sequencia	= :new.nr_seq_transcricao;

end if;


if	(obter_Perfil_Ativo	> 0) and
	(:new.cd_perfil_ativo is null) then
	:new.cd_perfil_ativo	:= obter_Perfil_Ativo;
end if;

if	(dt_prim_setor_w is not null) then

	select	nvl(max(vl_parametro),max(vl_parametro_padrao))
	into	ie_inicia_dia_plano_w
	from	funcao_parametro
	where	cd_funcao	= 950
	and		nr_sequencia	= 66;

	dt_rep_pt_w		:= trunc(:new.dt_inicio_prescr);

	dt_prim_setor_w		:= to_date(to_char(:new.dt_inicio_prescr,'dd/mm/yyyy') || ' ' || to_char(dt_prim_setor_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

	if	(:new.dt_inicio_prescr	< dt_prim_setor_w) then
		dt_rep_pt_w	:= trunc(:new.dt_inicio_prescr - 1);
		if	(:new.dt_validade_prescr > dt_prim_setor_w) then
			dt_rep_pt2_w	:= (dt_rep_pt_w + 1);
		else
			dt_rep_pt2_w	:= null;
		end if;
	elsif	(:new.nr_horas_validade	> 24) then
		dt_rep_pt2_w	:= (dt_rep_pt_w + 1);
	else
		dt_rep_pt2_w	:= null;
	end if;

	if	(ie_inicia_dia_plano_w = 'N') then
		dt_rep_pt_w := dt_rep_pt_w + 1;
		if	(dt_rep_pt2_w is not null) then
			dt_rep_pt2_w := dt_rep_pt2_w + 1;
		end if;
	end if;

	:new.dt_rep_pt	:= dt_rep_pt_w;
	:new.dt_rep_pt2	:= dt_rep_pt2_w;

else

	:new.dt_rep_pt	:= trunc(nvl(:new.dt_inicio_prescr,:new.dt_prescricao));
	:new.dt_rep_pt2	:= trunc(nvl(:new.dt_validade_prescr,:new.dt_prescricao));

end if;

if	(ie_copia_guia_senha_w <> 'N') then

	select	max(nr_doc_convenio),
		max(cd_senha)
	into	nr_doc_convenio_w,
		cd_senha_w
	from	atend_categoria_convenio a
	where	nr_atendimento = :new.nr_atendimento;

	if	(ie_copia_guia_senha_w = 'S') then

		if	(nr_doc_convenio_w is not null) then
			:new.nr_doc_conv := nr_doc_convenio_w;
		end if;

		if	(cd_senha_w is not null) then
			:new.cd_senha := cd_senha_w;
		end if;

	elsif  	(ie_copia_guia_senha_w = 'I') then

		if	(nr_doc_convenio_w is not null) and
			(:new.nr_doc_conv is null) then
			:new.nr_doc_conv := nr_doc_convenio_w;
		end if;

		if	(cd_senha_w is not null) and
			(:new.cd_senha is null) then
			:new.cd_senha := cd_senha_w;
		end if;

	end if;

end if;

if 	(nvl(:new.dt_entrega, :new.dt_prescricao) < :new.dt_prescricao) then

	--Wheb_mensagem_pck.exibir_mensagem_abort(267493);
	Wheb_mensagem_pck.exibir_mensagem_abort(267493, 'DT_ENTREGA='||to_char(:new.dt_entrega, 'dd/mm/yyyy hh24:mi:ss')||';DT_PRESCRICAO='||to_char(:new.dt_prescricao, 'dd/mm/yyyy hh24:mi:ss'));


end if;

select	substr(max(module||' - ' || machine||' - ' || program|| ' - ' || osuser|| ' - ' || terminal),1,255)
into	nm_maquina_w
from	v$session
where	audsid = (select userenv('sessionid') from dual);
:new.nm_maquina := nm_maquina_w;

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

END;
/


CREATE OR REPLACE TRIGGER TASY.PRESCR_MEDICA_tp  after update ON TASY.PRESCR_MEDICA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_PRESCRICAO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DT_ENTREGA,1,4000),substr(:new.DT_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'DT_ENTREGA',ie_log_w,ds_w,'PRESCR_MEDICA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_prescr_medica_prot_bfins
before insert ON TASY.PRESCR_MEDICA for each row
declare

ds_evolucao_w       long;
ds_escala_w         varchar2(255);
ds_protocolo_w      varchar2(255);

begin


if(:new.dt_liberacao is null and :new.nr_seq_pend_pac_acao is not null and :new.cd_protocolo is not null)then
    
    
    if(hsj_obter_se_evolucao_dia(:new.nr_atendimento, sysdate, wheb_usuario_pck.get_nm_usuario) = 'N')then
       
    
        select  max(substr(obter_valor_dominio(1799,d.IE_ESCALA),1,255))ds_escala,
                max(substr(Obter_desc_protoc_medic(a.nr_seq_protocolo),1,255))ds_protocolo
        into
                ds_escala_w,
                ds_protocolo_w
        from gqa_pend_pac_acao a
        join gqa_pendencia_pac b on(a.nr_seq_pend_pac = b.nr_sequencia)
        join gqa_pendencia_regra c on(c.nr_sequencia = b.nr_seq_pend_regra)
        join vice_escala d on(d.nr_sequencia = c.nr_seq_escala)
        where a.nr_seq_protocolo is not null
        and a.nr_sequencia = :new.nr_seq_pend_pac_acao;

        ds_evolucao_w:='Suporte � Decis�o Cl�nica (SDC) foi iniciado.

Escala: '|| ds_escala_w ||'
Profissional A��o: '|| obter_pf_usuario(wheb_usuario_pck.get_nm_usuario,'N') ||'

Protocolo Prescri��o Sugerida: '|| ds_protocolo_w ;
     
        hsj_gerar_evolucao(Obter_dados_usuario_opcao(wheb_usuario_pck.get_nm_usuario ,'F'), :new.nr_atendimento, ds_evolucao_w, 'PRO', wheb_usuario_pck.get_nm_usuario, null, null, null, null, 'S');

    end if;  

end if;

exception
when others then
    enviar_email('Exception_sepse','Verificar procedure hsj_prescr_medica_prot_bfins, erro no atendimento '||:new.nr_atendimento,'noreply@hsjose.com.br','sistemas@hsjose.com.br','RAFAELSDS','A');


end;
/


CREATE OR REPLACE TRIGGER TASY.prescr_medica_update
before update ON TASY.PRESCR_MEDICA for each row
declare
ie_tipo_atendimento_w		number(05,0);
cd_convenio_w				convenio.cd_convenio%type;
cd_categoria_w				categoria_convenio.cd_categoria%type;
cd_setor_ps_w				number(05,0);
qt_setor_int_w				number(05,0);
dt_entrada_w				date;
nr_seq_exame_w				number(10);
cd_material_exame_w			varchar2(20);
nr_seq_mat_w				number(10,0);
qt_coleta_w					number(4,0);
qt_medico_w					number(10);
cd_estabelecimento_w		number(4);
nr_seq_grupo_w				number(10);
ie_padrao_amostra_w			varchar2(5);
VarObrigaMedico_w			varchar2(1);
cd_barras_w					varchar2(30);
qt_volume_padrao_w			number(5);
qt_tempo_padrao_w			number(4,2);
nr_seq_origem_w				number(6);
nr_seq_prescr_w				number(6);
nr_seq_prescr_proc_mat_w	number(10);
ie_sexo_w					varchar2(1);
cd_perfil_ativo_w			number(5);
ds_maq_user_w				varchar2(80);
ie_prescr_rn_w				varchar(1);
ds_prescr_rn_w				varchar2(255);
qt_reg_w					number(1);
ie_atualiza_estab_w			varchar2(03);
ds_origem_w					varchar2(1800);
hr_prescr_copia_w			varchar2(10);
cd_pessoa_cirurgia_w		varchar2(10);
ie_vincula_atend_w			varchar2(10);
nr_seq_interno_w			number(10);
ie_integracao_ativa_w		varchar2(1);
ie_considera_minuto_w		varchar2(1);
ie_nova_prescr_val_w		varchar2(1);
ie_exclui_atend_prescr_w	varchar2(1);
dt_rep_pt_w					date;
dt_rep_pt2_w				date;
dt_prim_setor_w				date;
ie_inicia_dia_plano_w		varchar2(1);
ie_atualiza_w				varchar2(1);
ie_atualizar_data_hemodi_w	varchar2(1);
ie_calcula_validade_hem_w	varchar2(1);
nr_seq_status_padrao_oft_w	number(10)	:= null;
ie_permite_guia_senha_atend_w	varchar2(1);
ie_recalc_val_w				varchar2(1);
ds_stack_w					varchar2(2000);
ds_alteracao_w				varchar2(1800);
ie_tipo_lib_w				varchar2(1) := 'M';
dt_liberacao_w				date := sysdate;
cont_w						number(10);
qt_exame_lab_w				number(10) := 0;
reg_integracao_w			gerar_int_padrao.reg_integracao;
ds_param_integ_hl7_w		varchar2(2000);

cursor c01 is
	select	a.nr_sequencia,
		a.nr_seq_exame,
		a.cd_material_exame,
		b.nr_seq_grupo,
		a.nr_seq_origem
	from	prescr_procedimento a,
		exame_laboratorio b
	where 	a.nr_prescricao = :new.nr_prescricao
	  and 	a.nr_seq_exame 	= b.nr_seq_exame
	  and 	a.nr_seq_exame 	is not null;

cursor c02 is
	select	distinct
		b.nr_sequencia,
		nvl(c.qt_coleta,1),
		nvl(b.qt_volume_padrao,0),
		nvl(b.qt_tempo_padrao,0)
	from	exame_lab_material c,
		material_exame_lab b
	where 	cd_material_exame_w = b.cd_material_exame
	  and 	c.nr_seq_material = b.nr_sequencia
	  and 	c.nr_seq_exame = nr_seq_exame_w
	  and 	(b.ie_volume_tempo = 'S' or nvl(c.qt_coleta,1) > 0)
	  and 	not exists (	select 	1
							from 	material_exame_lab y,
								prescr_proc_material x
							where 	x.nr_prescricao = :new.nr_prescricao
							and 	x.nr_seq_material = y.nr_sequencia
							and 	((x.nr_seq_grupo = nr_seq_grupo_w) or (ie_padrao_amostra_w in ('PM','PM11','PM13')))
							and 	y.cd_material_exame = cd_material_exame_w);

cursor c03 is --diagnostika anatomo
	select	nr_seq_interno
	from	prescr_procedimento
	where 	nr_prescricao = :new.nr_prescricao
	and 	nr_seq_proc_interno is not null
	and	obter_se_integr_proc_interno(nr_seq_proc_interno,1,1,ie_lado,:new.cd_estabelecimento) = 'S'
	and	cd_cgc_laboratorio = '55578504000185';

cursor c04 is --diagnostika colpo
	select	nr_seq_interno
	from	prescr_procedimento
	where 	nr_prescricao = :new.nr_prescricao
	and 	nr_seq_proc_interno is not null
	and	obter_se_integr_proc_interno(nr_seq_proc_interno,1,2,ie_lado,:new.cd_estabelecimento) = 'S'
	and	cd_cgc_laboratorio = '55578504000185';

cursor c05 is
	select	a.cd_procedimento,
		a.nr_seq_exame,
		a.nr_seq_interno
	from	prescr_procedimento a,
		exame_laboratorio b
	where 	a.nr_prescricao = :new.nr_prescricao
	  and 	a.dt_suspensao 	is null
	  and 	a.nr_seq_exame 	is not null
	  and 	a.nr_seq_exame 	= b.nr_seq_exame
	  and 	b.ie_anatomia_patologica = 'S';

begin

begin
Obter_Param_Usuario(-15, 6,obter_perfil_ativo, :new.nm_usuario, 0, ie_atualiza_estab_w);

wheb_assist_pck.set_informacoes_usuario(:new.cd_estabelecimento,obter_perfil_ativo,:new.nm_usuario);

wheb_assist_pck.obterValorParametroREP(972,ie_atualizar_data_hemodi_w);
wheb_assist_pck.obterValorParametroREP(1016,ie_calcula_validade_hem_w);

wheb_assist_pck.obterValorParametroREP(753,ie_nova_prescr_val_w);
if	(:new.ie_prescr_emergencia = 'S') then
	if	(ie_nova_prescr_val_w = 'S') then
		ie_nova_prescr_val_w	:= 'N';
	end if;
end if;

wheb_assist_pck.obterValorParametroREP(689,ie_considera_minuto_w);
if	(ie_considera_minuto_w = 'D') then
	select	nvl(max(ie_minutos_rep),'S')
	into	ie_considera_minuto_w
	from	setor_atendimento
	where	cd_setor_atendimento = :new.cd_setor_atendimento;
end if;

exception
when others then
	ie_atualiza_estab_w	:= 'N';
end;

if	(:new.nr_controle is not null) and
	(:old.nr_controle is null) then

	:new.dt_controle_prescr := sysdate;

end if;

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(to_char(:new.dt_primeiro_horario,'ss') <> '00') then
	:new.dt_primeiro_horario	:= trunc(:new.dt_primeiro_horario,'mi') + 1/1440;
end if;

if	(:new.ie_recem_nato = 'S') then
	obter_se_pf_prescr_rn(:new.cd_estabelecimento, :new.cd_pessoa_fisica, :new.nm_usuario, ie_prescr_rn_w, ds_prescr_rn_w);
	if	(ie_prescr_rn_w = 'N') then
		-- #@DS_MENSAGEM#@
		Wheb_mensagem_pck.exibir_mensagem_abort(173258, 'DS_MENSAGEM=' || ds_prescr_rn_w);
	end if;
end if;

if	((:new.cd_medico <> :old.cd_medico) or
	 ((:new.cd_medico is null) and
	  (:old.cd_medico is not null))) then

	ds_stack_w := substr(dbms_utility.format_call_stack,1,1800);
	-- Excluir o log da trigger apos encontrar o dano da OS 1378860 - Solicitacao feita pela analista Claudiomar

	gravar_log_tasy(10007, substr('PRESCR_MEDICA_UPDATE ALTERACAO MEDICO :new.nr_prescricao : ' || :new.nr_prescricao
						|| ' :new.cd_medico : ' || :new.cd_medico
						|| ' :old.cd_medico : ' || :old.cd_medico
						|| ' ds_stack_w : ' || ds_stack_w,1,1800), :new.nm_usuario);

	if	(:new.cd_medico is not null) then
		Consistir_impedimento_pf(:new.cd_medico,'REP',:new.nm_usuario);
	end if;
end if;

if	((:new.cd_prescritor is not null) and (:old.cd_prescritor is null)) or
	((:new.cd_prescritor is not null) and (:old.cd_prescritor <> :new.cd_prescritor)) then
	Consistir_impedimento_pf(:new.cd_prescritor,'REP',:new.nm_usuario);
end if;

if	(:new.dt_liberacao_farmacia is not null) and
	(:old.dt_liberacao_farmacia is not null) and
	(:new.dt_liberacao_farmacia <> :old.dt_liberacao_farmacia) then
	:new.dt_liberacao_farmacia	:= :old.dt_liberacao_farmacia;
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	begin

	select	nvl(max('S'),'N')
	into	ie_integracao_ativa_w
	from 	empresa_integr_dados
	where 	rownum = 1
	and	nr_seq_empresa_integr = 13
	and 	ie_situacao = 'A';

	if (ie_integracao_ativa_w = 'S') then
		open c03;
		loop
		fetch c03 into
			nr_seq_interno_w;
		exit when c03%notfound;
			begin
			gerar_dados_diagnostika(:new.nr_prescricao, nr_seq_interno_w, 1, :new.nm_usuario);--gera xml diagnostika anatomo
			end;
		end loop;
		close c03;

		open c04;
		loop
		fetch c04 into
			nr_seq_interno_w;
		exit when c04%notfound;
			begin
			gerar_dados_diagnostika(:new.nr_prescricao, nr_seq_interno_w, 2, :new.nm_usuario);--gera xml diagnostika colpo
			end;
		end loop;
		close c04;
	end if;

	end;
end if;

if 	(wheb_usuario_pck.is_evento_ativo(288) = 'S') and
		(((obter_valor_param_usuario(924, 1179, wheb_usuario_pck.Get_cd_perfil, wheb_usuario_pck.Get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento) = 'S') and
		(:old.dt_liberacao_medico is null and :new.dt_liberacao_medico is not null)) or
		((:old.dt_liberacao is null and :new.dt_liberacao is not null) and
		(obter_valor_param_usuario(924, 1179, wheb_usuario_pck.Get_cd_perfil, wheb_usuario_pck.Get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento) <> 'S'))) then
	integrar_softlab_ws(288, :new.cd_pessoa_fisica, :new.nr_prescricao, null, null, :new.nm_usuario, 1, 'N');
end if;

if	((:new.dt_liberacao is not null) and	 (:old.dt_liberacao is null)) or
	((:new.dt_liberacao is null) and (:new.dt_liberacao_medico is not null) and	 (:old.dt_liberacao_medico is null)) then
	if	(nvl(:new.nr_seq_consulta_oft,0) > 0) then
		Obter_Param_Usuario(3010,72,obter_perfil_ativo,:new.nm_usuario,:new.cd_estabelecimento,nr_seq_status_padrao_oft_w);
		if	(nvl(nr_seq_status_padrao_oft_w,0) > 0) then
			begin

			update	oft_consulta
			set		nr_seq_status		=	decode(nr_seq_status_padrao_oft_w,null,nr_seq_status,nr_seq_status_padrao_oft_w)
			where	nr_sequencia		=	:new.nr_seq_consulta_oft;

			exception
			when others then
				nr_seq_status_padrao_oft_w := null;
			end;
		end if;
	end if;
end if;

if	(to_char(:new.dt_prescricao,'dd/mm/yyyy') = '30/12/1899') then
	--A data da prescricao informada nao e valida. #@DT_PRESCRICAO#@
	Wheb_mensagem_pck.exibir_mensagem_abort(173273, 'DT_PRESCRICAO=' || to_char(:new.dt_prescricao,'dd/mm/yyyy hh24:mi:ss'));
end if;

if	(:old.nr_atendimento is null) and
	(:new.nr_atendimento is not null) then
	update	can_ordem_prod
	set	nr_atendimento	= :new.nr_atendimento
	where	nr_prescricao	= :new.nr_prescricao
	and	nr_atendimento is null;
end if;

select	count(*)
into	qt_medico_w
from	medico
where	rownum = 1
and		cd_pessoa_fisica	= :new.cd_medico;

if	((qt_medico_w = 0) and
	 ((:old.cd_medico <> :new.cd_medico) or
	  ((:old.cd_medico is not null) and
	   (:new.cd_medico is null)))) then

	wheb_assist_pck.obterValorParametroREP(1037,VarObrigaMedico_w);
	if	(VarObrigaMedico_w = 'S') then
		--A pessoa informada no campo Medico nao esta cadastrada como medico.
		Wheb_mensagem_pck.exibir_mensagem_abort(173291);
	end if;
end if;

if	((:old.dt_liberacao is not null) or (:old.dt_liberacao_medico is not null)) and
	(:old.nr_atendimento is not null) and
	(:new.nr_atendimento is null) then
	obter_param_usuario(924, 1075, wheb_usuario_pck.Get_cd_perfil, wheb_usuario_pck.Get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento, ie_exclui_atend_prescr_w);
	if	(nvl(ie_exclui_atend_prescr_w,'N') = 'N') then
		--Uma prescricao liberada nao pode ser desvinculada de seu atendimento.
		Wheb_mensagem_pck.exibir_mensagem_abort(173292);
	end if;
end if;

if	(:new.cd_estabelecimento is not null) and
	(:new.nr_atendimento is not null) and
	(nvl(:old.nr_atendimento,0) <> :new.nr_atendimento) and
	(ie_atualiza_estab_w = 'N') then
	select	cd_estabelecimento
	into	cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	if	(cd_estabelecimento_w <> :new.cd_estabelecimento) then
		--O estabelecimento da prescricao nao pode ser diferente do estabelecimento do atendimento. (Parametro [6] da funcao "Transferencia de gastos")
		Wheb_mensagem_pck.exibir_mensagem_abort(173297);
	end if;
end if;

if	(:new.cd_setor_atendimento is null) and
	(:new.nr_atendimento is not null) then
	select	nvl(max(ie_tipo_atendimento),0)
	into	ie_tipo_atendimento_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	if	(ie_tipo_atendimento_w = 3) then
		select	max(a.cd_setor_atendimento)
		into	cd_setor_ps_w
		from	setor_atendimento b,
				atend_paciente_unidade a
		where	a.nr_atendimento		= :new.nr_atendimento
		and		a.cd_setor_atendimento	= b.cd_setor_atendimento
		and		b.cd_classif_setor		= 1;

		if	(cd_setor_ps_w is not null) then
			select	count(*)
			into	qt_setor_int_w
			from	setor_atendimento b,
					atend_paciente_unidade a
			where	rownum = 1
			and		a.nr_atendimento		= :new.nr_atendimento
			and		a.cd_setor_atendimento	= b.cd_setor_atendimento
			and		b.cd_classif_setor		in(3,4,8);

			if	(qt_setor_int_w = 0) then
				:new.cd_setor_atendimento	:= cd_setor_ps_w;
			end if;
		end if;
	end if;
end if;

if	(:new.cd_setor_atendimento is null) and
	(:old.nr_atendimento is null) and
	(:new.nr_atendimento is not null) then
	wheb_assist_pck.obterValorParametroREP(100,ie_vincula_atend_w);
	if	(ie_vincula_atend_w = 'S') then
		:new.cd_setor_atendimento	:= Obter_setor_atendimento(:new.nr_atendimento);
	end if;
end if;

if	(:new.cd_setor_atendimento is not null) and
	(:new.cd_setor_atendimento <> :old.cd_setor_atendimento) then
	begin
	ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);
	update	ap_lote
	set	cd_setor_ant		= :old.cd_setor_atendimento
	where	nr_prescricao		= :new.nr_prescricao
	and	ie_status_lote		= 'G';

	update	ap_lote
	set	cd_setor_atendimento	= :new.cd_setor_atendimento
	where	nr_prescricao		= :new.nr_prescricao
	and	ie_status_lote 		= 'G';
	fleury_limpa_validacao_item(:new.nr_prescricao, null);

	end;
end if;

if	(:new.dt_suspensao is not null) and
	(:old.dt_suspensao is null) then
	begin
	ds_maq_user_w	:= substr(obter_inf_sessao(0) ||' - ' || obter_inf_sessao(1),1,80);

	cd_perfil_ativo_w	:= obter_perfil_ativo;

	update	ap_lote
	set	dt_cancelamento		= :new.dt_suspensao,
		nm_usuario_cancelamento	= :new.nm_usuario_susp,
		ds_maquina_cancelamento 	= ds_maq_user_w,
		cd_perfil_cancel		= cd_perfil_ativo_w,
		ie_status_lote		= 'C'
	where	nr_prescricao		= :new.nr_prescricao
	and	ie_status_lote in ('G','A','D','E');
	end;
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	:new.dt_emissao_farmacia        := null;
end if;

if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	gerar_status_solucao(:new.nr_prescricao, nvl(:new.dt_inicio_prescr,sysdate),:new.nm_usuario, :new.cd_funcao_origem, :new.nr_horas_validade);
end if;

if	(:old.dt_liberacao_medico is null) and
	((nvl(:new.cd_funcao_origem,924) <> 950) or
	 (nvl(:new.ie_prescr_emergencia,'N') = 'S')) then

	 wheb_assist_pck.obterValorParametroREP(532,hr_prescr_copia_w);
	if	((hr_prescr_copia_w is null) or
		 (to_char(:new.dt_primeiro_horario,'hh24:mi') <> hr_prescr_copia_w)) then


		if	(ie_nova_prescr_val_w = 'N') or
			(((:new.nr_horas_validade <> :old.nr_horas_validade) and
			  (:old.nr_horas_validade is not null)) or
			  ((:new.dt_primeiro_horario <> :old.dt_primeiro_horario) or
			   (:new.dt_primeiro_horario is null))) then
			if	(:new.dt_primeiro_horario is not null) then
				if	(to_char(:new.dt_primeiro_horario,'hh24:mi') < to_char(:new.dt_prescricao,'hh24:mi')) then
					:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao+1,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
				else
					:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
				end if;
			else
				:new.dt_inicio_prescr := :new.dt_prescricao;
			end if;

		elsif	((:new.dt_prescricao <> :old.dt_prescricao) and
			 (:old.dt_prescricao is not null)) then
			if	(:new.dt_primeiro_horario is not null) then
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			else
				:new.dt_inicio_prescr := :new.dt_prescricao;
			end if;
		end if;

	else

		if	(ie_nova_prescr_val_w = 'N') or
			(((:new.nr_horas_validade <> :old.nr_horas_validade) and
			  (:old.nr_horas_validade is not null)) or
			  ((:new.dt_primeiro_horario <> :old.dt_primeiro_horario) or
			   (:new.dt_primeiro_horario is null))) then
			if	(:new.dt_primeiro_horario is not null) then
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			else
				:new.dt_inicio_prescr := :new.dt_prescricao;
			end if;
		elsif	((:new.dt_prescricao <> :old.dt_prescricao) and
			 (:old.dt_prescricao is not null)) then
			if	(:new.dt_primeiro_horario is not null) then --OS195107
				:new.dt_inicio_prescr := to_date(to_char(:new.dt_prescricao,'dd/mm/yyyy') || ' ' || to_char(:new.dt_primeiro_horario,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');
			else
				:new.dt_inicio_prescr := :new.dt_prescricao;
			end if;
		end if;
	end if;
end if;

if	(ie_nova_prescr_val_w = 'N') or
	((:new.nr_horas_validade <> :old.nr_horas_validade) and
	 (:old.nr_horas_validade is not null)) or
	  ((:new.dt_primeiro_horario <> :old.dt_primeiro_horario) or
	   (:new.dt_primeiro_horario is null)) then

	if	(nvl(:new.ie_hemodialise,'N') not in ('E','S')) or
		(ie_calcula_validade_hem_w	= 'S') then
		:new.dt_validade_prescr := :new.dt_inicio_prescr + nvl(:new.nr_horas_validade,24) / 24;
		if	(ie_considera_minuto_w = 'S') then
			:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'mi') - 1/86400;
		else
			:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'hh24') - 1/86400;
		end if;
	elsif	(ie_atualizar_data_hemodi_w = 'S') and
		(nvl(:old.nr_horas_validade,0) = nvl(:new.nr_horas_validade,0)) and
		(:old.dt_inicio_prescr	= :new.dt_inicio_prescr) and
		(:old.dt_validade_prescr is null) and
		(:new.dt_validade_prescr is not null) then

		:new.dt_validade_prescr := :new.dt_validade_prescr;
	else
		:new.dt_validade_prescr := null;
	end if;
elsif	((:new.dt_prescricao <> :old.dt_prescricao) and
		 (:old.dt_prescricao is not null)) then

	if	(nvl(:new.ie_hemodialise,'N') not in ('E','S')) or
		(ie_calcula_validade_hem_w	= 'S') then
		:new.dt_validade_prescr := :new.dt_inicio_prescr + nvl(:new.nr_horas_validade,24) / 24;
		if	(ie_considera_minuto_w = 'S') then
			:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'mi') - 1/86400;
		else
			:new.dt_validade_prescr := trunc(:new.dt_validade_prescr,'hh24') - 1/86400;
		end if;
	elsif	(ie_atualizar_data_hemodi_w = 'S') and
		(nvl(:old.nr_horas_validade,0) = nvl(:new.nr_horas_validade,0)) and
		(:old.dt_inicio_prescr	= :new.dt_inicio_prescr) and
		(:old.dt_validade_prescr is null) and
		(:new.dt_validade_prescr is not null) then

		:new.dt_validade_prescr := :new.dt_validade_prescr;
	else
		:new.dt_validade_prescr := null;
	end if;
end if;

/*Almir em 28/01/08 OS80755 */
if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) then
	:new.nm_usuario_lib_enf        := :new.nm_usuario;
end if;

if	(:old.nr_seq_transcricao is null) and
	(:new.nr_seq_transcricao is not null) then

	update	transcricao_prescricao
	set	ie_status	= 'E'
	where	nr_sequencia	= :new.nr_seq_transcricao;

end if;

if	((:old.dt_liberacao_medico is not null) or (:old.dt_liberacao is not null)) and
	(:old.cd_setor_atendimento <> :new.cd_setor_atendimento) and
	(nvl(:old.ie_adep,'S') = 'E')then
	begin
	:new.ie_adep := 'N';
	end;
end if;

select	max(hr_inicio_prescricao)
into	dt_prim_setor_w
from	setor_atendimento
where	cd_setor_atendimento = :new.cd_setor_atendimento;

if	(dt_prim_setor_w is not null) and
	((:new.nr_horas_validade	<> :old.nr_horas_validade) or
	 (:new.dt_inicio_prescr		<> :old.dt_inicio_prescr) or
	 (nvl(:new.cd_setor_atendimento,0) <> nvl(:old.cd_setor_atendimento,0))) then

	select	nvl(max(vl_parametro),max(vl_parametro_padrao))
	into	ie_inicia_dia_plano_w
	from	funcao_parametro
	where	cd_funcao	= 950
	and	nr_sequencia	= 66;

	dt_rep_pt_w		:= trunc(:new.dt_inicio_prescr);

	dt_prim_setor_w		:= to_date(to_char(:new.dt_inicio_prescr,'dd/mm/yyyy') || ' ' || to_char(dt_prim_setor_w,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss');

	if	(:new.dt_inicio_prescr	< dt_prim_setor_w) then
		dt_rep_pt_w	:= trunc(:new.dt_inicio_prescr - 1);
		if	(:new.dt_validade_prescr > dt_prim_setor_w) then
			dt_rep_pt2_w	:= (dt_rep_pt_w + 1);
		else
			dt_rep_pt2_w	:= null;
		end if;
	elsif	(:new.nr_horas_validade	> 24) then
		dt_rep_pt2_w	:= (dt_rep_pt_w + 1);
	else
		dt_rep_pt2_w	:= null;
	end if;

	if	(ie_inicia_dia_plano_w = 'N') then
		dt_rep_pt_w := dt_rep_pt_w + 1;
		if	(dt_rep_pt2_w is not null) then
			dt_rep_pt2_w := dt_rep_pt2_w + 1;
		end if;
	end if;

	:new.dt_rep_pt	:= dt_rep_pt_w;
	:new.dt_rep_pt2	:= dt_rep_pt2_w;

	update	prescr_dieta
	set	dt_rep_pt	= :new.dt_rep_pt,
		dt_rep_pt2	= :new.dt_rep_pt2
	where	nr_prescricao	= :new.nr_prescricao;

elsif	(:new.dt_rep_pt is null) and
	(:new.dt_rep_pt2 is null) then

	:new.dt_rep_pt	:= trunc(nvl(:new.dt_inicio_prescr,:new.dt_prescricao));
	:new.dt_rep_pt2	:= trunc(nvl(:new.dt_validade_prescr,:new.dt_prescricao));

end if;

if	(:new.nr_horas_validade <> :old.nr_horas_validade) and
	(:new.Dt_liberacao_medico	is not null) then
	wheb_assist_pck.obterValorParametroREP(722,ie_recalc_val_w);
	if	(ie_recalc_val_w = 'N') then
		-- Voce nao pode alterar a validade desta prescricao pois a mesma ja esta liberada!
		Wheb_mensagem_pck.exibir_mensagem_abort(185128);
	end if;
end if;

if	((:new.nr_doc_conv is not null) or (:new.cd_senha is not null)) then
	wheb_assist_pck.obterValorParametroREP(1018,ie_permite_guia_senha_atend_w);
	if	(ie_permite_guia_senha_atend_w = 'N') then

		if 	(obter_se_somente_numero(:new.nr_doc_conv) = 'S') then
			if	(:new.nr_atendimento = somente_numero(:new.nr_doc_conv)) then
				-- O numero da Guia nao pode ser igual ao numero do Atendimento.
				Wheb_mensagem_pck.exibir_mensagem_abort(223884);
			end if;
		end if;

		if 	(obter_se_somente_numero(:new.cd_senha) = 'S') then
			if	(:new.nr_atendimento = somente_numero(:new.cd_senha)) then
				-- O numero da Senha nao pode ser igual ao numero do Atendimento.
				Wheb_mensagem_pck.exibir_mensagem_abort(223885);
			end if;
		end if;
	end if;
end if;

if	(:new.dt_emissao_farmacia is null) and
	(:old.dt_emissao_farmacia is not null) then
	ds_stack_w := substr(dbms_utility.format_call_stack,1,1800);

	gravar_log_tasy(18,obter_desc_expressao(296208)/*'Prescr = '*/ ||' = '|| :new.nr_prescricao || ' | Call stack = ' || ds_stack_w,:new.nm_usuario);
end if;

if 	(nvl(:new.dt_entrega, :new.dt_prescricao) < :new.dt_prescricao) then
	-- A data de entrega nao pode ser menor que a data da prescricao.
	--  Data de entrega: #@DT_ENTREGA#@
	--  Data da prescricao: #@DT_PRESCRICAO#@
	Wheb_mensagem_pck.exibir_mensagem_abort(267493, 'DT_ENTREGA='||to_char(:new.dt_entrega, 'dd/mm/yyyy hh24:mi:ss')||';DT_PRESCRICAO='||to_char(:new.dt_prescricao, 'dd/mm/yyyy hh24:mi:ss'));
end if;

if	(:new.dt_entrada_unidade <> :old.dt_entrada_unidade) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_entrada_unidade(' || to_char(:old.dt_entrada_unidade, 'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_entrada_unidade, 'dd/mm/yyyy hh24:mi:ss') ||'); ',1,1800);
end if;

if	(nvl(:new.cd_setor_atendimento,0) <> nvl(:old.cd_setor_atendimento,0)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' cd_setor_atendimento(' || nvl(:old.cd_setor_atendimento,'0') || '/' || nvl(:new.cd_setor_atendimento,'0')||'); ',1,1800);
end if;

if	(nvl(:new.cd_estabelecimento,0) <> nvl(:old.cd_estabelecimento,0)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' cd_estabelecimento(' || nvl(:old.cd_estabelecimento,'0') || '/' || nvl(:new.cd_estabelecimento,'0')||'); ',1,1800);
end if;

if	(:old.dt_liberacao_farmacia is null) and (:new.dt_liberacao_farmacia is not null) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_liberacao_farmacia: ' || to_char(:new.dt_liberacao_farmacia, 'dd/mm/yyyy hh24:mi:ss'),1,1800);
end if;

if	(nvl(:new.dt_primeiro_horario,sysdate) <> nvl(:old.dt_primeiro_horario,sysdate)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_primeiro_horario(' || to_char(:old.dt_primeiro_horario,'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_primeiro_horario,'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
end if;

if	(nvl(:new.dt_prescricao, sysdate) <> nvl(:old.dt_prescricao, sysdate)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_prescricao(' || to_char(:old.dt_prescricao,'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_prescricao,'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
end if;

if	(:old.dt_suspensao is null) and (:new.dt_suspensao is not null) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_suspensao: ' || to_char(:new.dt_suspensao, 'dd/mm/yyyy hh24:mi:ss'),1,1800);
end if;

if	(nvl(:old.nr_atendimento,0) <> nvl(:new.nr_atendimento,0)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' nr_atendimento(OLD/NEW): (' || :old.nr_atendimento || '/' || :new.nr_atendimento || ')',1,1800);
end if;

if	(nvl(:new.dt_liberacao, sysdate - 1) <> nvl(:old.dt_liberacao, sysdate - 1)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_liberacao(' || to_char(:old.dt_liberacao,'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_liberacao,'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
end if;

if	(nvl(:new.nm_usuario_lib_enf,'null') <> nvl(:old.nm_usuario_lib_enf,'null')) then
	ds_alteracao_w := substr(ds_alteracao_w || ' nm_usuario_lib_enf(' || nvl(:old.nm_usuario_lib_enf,'null') || '/' || nvl(:new.nm_usuario_lib_enf,'null')||'); ',1,1800);
end if;

if (nvl(:old.nr_horas_validade,0) <> nvl(:new.nr_horas_validade,0)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' nr_horas_validade(old/new): (' || :old.nr_horas_validade || '/' || :new.nr_horas_validade || ')',1,1800);
end if;

if	(nvl(:new.dt_validade_prescr, sysdate) <> nvl(:old.dt_validade_prescr, sysdate)) then
	ds_alteracao_w := substr(ds_alteracao_w || ' dt_validade_prescr(old/new): (' || to_char(:old.dt_validade_prescr,'dd/mm/yyyy hh24:mi:ss') || '/' || to_char(:new.dt_validade_prescr,'dd/mm/yyyy hh24:mi:ss')||'); ',1,1800);
end if;

if	(nvl(:new.ds_itens_prescr,'null') <> nvl(:old.ds_itens_prescr,'null')) then
	ds_alteracao_w := substr(ds_alteracao_w || ' ds_itens_prescr(' || nvl(:old.ds_itens_prescr,'null') || '/' || nvl(:new.ds_itens_prescr,'null')||'); ',1,1800);
end if;

if	(ds_alteracao_w is not null) then
	ds_alteracao_w := obter_desc_expressao(296208)/*'Prescricao - '*/ ||' - '|| :new.nr_prescricao || ' - ' || ';Func=' || obter_funcao_ativa || ' - ' || ds_alteracao_w || ' - ' || substr(dbms_utility.format_call_stack,1,1800);
    gravar_log_tasy(-5, substr(ds_alteracao_w,1,1800), :new.nm_usuario);
end if;

select	count(b.nr_sequencia)
into	cont_w
from	intpd_eventos a,
		intpd_eventos_sistema b
where	a.nr_sequencia = b.nr_seq_evento
and		a.ie_evento = 143
and		a.ie_situacao = 'A'
and		b.ie_situacao = 'A'
and		rownum = 1;

if ((cont_w = 1) and
		(:new.dt_liberacao is not null) and
		(:old.dt_liberacao is null)) then

	select Count(1)
	into qt_exame_lab_w
	from exame_laboratorio a, prescr_procedimento b
	where b.nr_prescricao = :new.nr_prescricao
	and b.dt_suspensao is null
	and b.nr_seq_exame is not null
	and a.ie_anatomia_patologica = 'N'
	and a.nr_seq_exame = b.nr_seq_exame;

	if (qt_exame_lab_w > 0) then

		select	max(ie_tipo_atendimento),
			max(obter_convenio_atendimento(nr_atendimento)),
			max(obter_categoria_atendimento(nr_atendimento)),
			max(cd_estabelecimento)
		into	ie_tipo_atendimento_w,
			cd_convenio_w,
			cd_categoria_w,
			cd_estabelecimento_w
		from	atendimento_paciente
		where	nr_atendimento = :new.nr_atendimento;

		reg_integracao_w.cd_estab_documento := cd_estabelecimento_w;
		reg_integracao_w.cd_convenio := cd_convenio_w;
		reg_integracao_w.cd_categoria := cd_categoria_w;
		reg_integracao_w.ie_tipo_atendimento := ie_tipo_atendimento_w;
		gerar_int_padrao.gravar_integracao('143', :new.nr_prescricao, :new.nm_usuario, reg_integracao_w);

	end if;
end if;

select	count(b.nr_sequencia)
into	cont_w
from	intpd_eventos a,
		intpd_eventos_sistema b
where	a.nr_sequencia = b.nr_seq_evento
and		a.ie_evento = 174
and		a.ie_situacao = 'A'
and		b.ie_situacao = 'A'
and		rownum = 1;

if ((cont_w = 1) and
	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null)) then

	select	max(ie_tipo_atendimento),
		max(obter_convenio_atendimento(nr_atendimento)),
		max(obter_categoria_atendimento(nr_atendimento)),
		max(cd_estabelecimento)
	into	ie_tipo_atendimento_w,
		cd_convenio_w,
		cd_categoria_w,
		cd_estabelecimento_w
	from	atendimento_paciente
	where	nr_atendimento = :new.nr_atendimento;

	for c_05_w in c05 loop

		reg_integracao_w.cd_estab_documento := cd_estabelecimento_w;
		reg_integracao_w.cd_convenio := cd_convenio_w;
		reg_integracao_w.cd_categoria := cd_categoria_w;
		reg_integracao_w.cd_procedimento := c_05_w.cd_procedimento;
		reg_integracao_w.nr_seq_exame := c_05_w.nr_seq_exame;
		reg_integracao_w.ie_tipo_atendimento := ie_tipo_atendimento_w;
		gerar_int_padrao.gravar_integracao('174', c_05_w.nr_seq_interno, :new.nm_usuario, reg_integracao_w);

	end loop;
end if;

<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_medica_befinsupd
BEFORE INSERT OR UPDATE ON TASY.PRESCR_MEDICA FOR EACH ROW
DECLARE

ie_rn_pre_termo_w		varchar2(1)	:= 'N';
vl_const_schwartz_w		number(10,2);
qt_idade_w				number(15,0);
vl_mult_w				number(10,4);
vl_mult_mdrd_w			number(10,4);
vl_mult_negro_w			number(10,4);
ie_raca_negra_w			varchar2(1);
ie_sexo_w				varchar2(1);
ie_prescr_rn_w			varchar(1);
cd_pessoa_fisica_w		Varchar2(10);
ds_prescr_rn_w			varchar2(255);
qt_reg_w				number(1);
ie_situacao_w			varchar(1);
ie_permite_inativo_w	varchar(1);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
select	max(ie_sexo)
into	ie_sexo_w
from	pessoa_fisica
where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;

if	(:new.ie_recem_nato = 'S') then
	obter_se_pf_prescr_rn(:new.cd_estabelecimento, :new.cd_pessoa_fisica, :new.nm_usuario, ie_prescr_rn_w, ds_prescr_rn_w);
	if	(ie_prescr_rn_w = 'N') then
		 Wheb_mensagem_pck.exibir_mensagem_abort(259962,'DS_PRESCRICAO_RN_W='||ds_prescr_rn_w);
	end if;
end if;

if	(nvl(:old.DT_PRESCRICAO,sysdate+10) <> :new.DT_PRESCRICAO) and
	(:new.DT_PRESCRICAO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_PRESCRICAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

/* Ivan em 21/01/2008 OS80038 - In�cio do c�lculo da clearence */

/* Obter idade atual */
select	to_number(nvl(max(obter_idade_pf(:new.cd_pessoa_fisica, sysdate, 'A')),0))
into	qt_idade_w
from	dual;

if	(:new.ie_calculo_clearence = 'CG') then /* Cockcroft-Galt */
	/* Obter valor multiplica��o caso seja do sexo feminino */
	select	nvl(decode(obter_sexo_pf(:new.cd_pessoa_fisica,'C'),'F',0.85,1),1)
	into	vl_mult_w
	from	dual;
	:new.qt_clearence	:= (140 - qt_idade_w) * dividir(:new.qt_peso, (72 * :new.qt_creatinina)) * (vl_mult_w);

elsif	(:new.ie_calculo_clearence = 'MDRD') then /* MDRD */
	/* Obter se ra�a negra */
	select	nvl(max(ie_negro),'N')
	into	ie_raca_negra_w
	from	cor_pele a,
		pessoa_fisica b
	where	b.nr_seq_cor_pele 	= a.nr_sequencia
	and	b.cd_pessoa_fisica 	= :new.cd_pessoa_fisica;
	/* Obter multiplicador se ra�a negra */
	select	nvl(decode(max(ie_raca_negra_w),'S',1.210,'N',1),1)
	into	vl_mult_negro_w
	from	dual;
	/* Obter valor multiplica��o caso seja do sexo feminino */
	select	nvl(decode(obter_sexo_pf(:new.cd_pessoa_fisica,'C'),'F',0.742,1),1)
	into	vl_mult_mdrd_w
	from	dual;
	begin
	:new.qt_clearence	:= 186 * power(:new.qt_creatinina, - 1.154) * power(qt_idade_w, - 0.203) * (vl_mult_mdrd_w) * (vl_mult_negro_w);
	exception
		when others then
		:new.qt_clearence	:= 0;
	end;

elsif	(:new.ie_calculo_clearence = 'S') then /* Schwartz */
	/* Obter valor da constante Schwartz */
	if	(qt_idade_w <= 2) and (ie_rn_pre_termo_w = 'S') then
		vl_const_schwartz_w	:= 0.33;
	elsif	(qt_idade_w <= 2) and (ie_rn_pre_termo_w = 'N' ) then
		vl_const_schwartz_w	:= 0.45;
	elsif	(qt_idade_w > 2) and (obter_sexo_pf(:new.cd_pessoa_fisica,'C') = 'F') then
		vl_const_schwartz_w	:= 0.55;
	elsif	(obter_sexo_pf(:new.cd_pessoa_fisica,'C') = 'M') then
		vl_const_schwartz_w	:= 0.70;
	end if;
	/* Calcula a clearence */
	:new.qt_clearence	:= vl_const_schwartz_w * dividir(:new.qt_altura_cm, :new.qt_creatinina);


elsif	(:new.ie_calculo_clearence = 'CB') then /* Counahan-Barratt */
	:new.qt_clearence	:= 0.43 * dividir(:new.qt_altura_cm, :new.qt_creatinina);
end if;

if	(:new.nr_horas_validade = 0) then
	WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(259960);
end if;

if	(:new.nr_atendimento is not null) and
	((nvl(:new.nr_atendimento,0) <> nvl(:old.nr_atendimento,0)) or (nvl(:new.cd_pessoa_fisica,'X') <> nvl(:old.cd_pessoa_fisica,'X'))) then
	select	cd_pessoa_fisica
	into	cd_pessoa_fisica_w
	from	atendimento_paciente
	where	nr_atendimento	= :new.nr_atendimento;

	if	(cd_pessoa_fisica_w <> :new.cd_pessoa_fisica) then
		--O paciente do atendimento n�o � o mesmo da prescri��o.
		Wheb_mensagem_pck.exibir_mensagem_abort(173298);
	end if;
end if;

if	(:new.dt_validade_prescr < :new.dt_inicio_prescr) then
	:new.dt_validade_prescr := null;
end if;

if	(nvl(:new.cd_medico,'X') <> nvl(:old.cd_medico,'X')) and
	(:new.cd_medico is not null) then

	select 	nvl(max(IE_SITUACAO),'A')
	into	ie_situacao_w
	from 	medico
	where	cd_pessoa_fisica = :new.cd_medico;

	Obter_Param_Usuario(950, 82, obter_perfil_ativo, :new.nm_usuario, :new.cd_estabelecimento, ie_permite_inativo_w);

	if	(ie_situacao_w = 'I') and
		(((ie_permite_inativo_w = 'N' ) and (:new.cd_funcao_origem = 950)) or
		(:new.cd_funcao_origem = 924)) then

		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(259958);

	end if;

end if;

if 	(:new.dt_entrega is not null) and
	(to_char(:new.dt_entrega,'dd/mm/yyyy hh24:mi:ss') = '00/00/0000 00:00:00') then
	:new.dt_entrega := null;
end if;

/* Ivan em 21/01/2008 OS80038 - Fim do c�lculo da clearence */
<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_med_insert_update_after
after insert or update ON TASY.PRESCR_MEDICA for each row
declare

c_zero_date constant date := to_date('01-01-01 00:00:00', 'dd-mm-yy hh24:mi:ss');

ds_log_w			varchar2(2000 char);

begin

if	(nvl(:new.dt_inicio_prescr, c_zero_date) <> nvl(:old.dt_inicio_prescr, c_zero_date)) then
	ds_log_w	:= substr(ds_log_w || ' dt_inicio_prescr(' || nvl(to_char(:old.dt_inicio_prescr,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_inicio_prescr,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.dt_validade_prescr, c_zero_date) <> nvl(:old.dt_validade_prescr, c_zero_date)) then
	ds_log_w	:= substr(ds_log_w || ' dt_validade_prescr(' || nvl(to_char(:old.dt_validade_prescr,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_validade_prescr,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.dt_liberacao_farmacia, c_zero_date) <> nvl(:old.dt_liberacao_farmacia, c_zero_date)) then
	ds_log_w	:= substr(ds_log_w || ' dt_liberacao_farmacia(' || nvl(to_char(:old.dt_liberacao_farmacia,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_liberacao_farmacia,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.dt_liberacao_medico, c_zero_date) <> nvl(:old.dt_liberacao_medico, c_zero_date)) then
	ds_log_w	:= substr(ds_log_w || ' dt_liberacao_medico(' || nvl(to_char(:old.dt_liberacao_medico,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_liberacao_medico,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.dt_liberacao, c_zero_date) <> nvl(:old.dt_liberacao, c_zero_date)) then
	ds_log_w	:= substr(ds_log_w || ' dt_liberacao(' || nvl(to_char(:old.dt_liberacao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_liberacao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.dt_prescricao, c_zero_date) <> nvl(:old.dt_prescricao, c_zero_date)) then
	ds_log_w	:= substr(ds_log_w || ' dt_prescricao(' || nvl(to_char(:old.dt_prescricao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>') || '/' || nvl(to_char(:new.dt_prescricao,'dd/mm/yyyy hh24:mi:ss'),'<NULL>')||'); ',1,2000);
end if;

if	(nvl(:new.cd_setor_atendimento, 0) <> nvl(:old.cd_setor_atendimento, 0)) then
	ds_log_w := substr(ds_log_w || ' cd_setor_atendimento(' || nvl(:old.cd_setor_atendimento,0) || '/' || nvl(:new.cd_setor_atendimento,0)||'); ',1,2000);
end if;

if	(ds_log_w is not null) then

	ds_log_w	:= substr(' nr_atendimento(' || nvl(:old.nr_atendimento,0) || '/' || nvl(:new.nr_atendimento,0)||'); ' || ds_log_w,1,2000);

	ds_log_w	:= substr(' nr_prescricao(' || nvl(:old.nr_prescricao,0) || '/' || nvl(:new.nr_prescricao,0)||'); ' || ds_log_w ,1,2000);

	if (nvl(:old.nr_prescricao, 0) > 0) then
		ds_log_w := substr('Alteracoes(old/new) = ' || ds_log_w,1,2000);
	else
		ds_log_w := substr('Criacao(old/new) = ' || ds_log_w,1,2000);
	end if;

	ds_log_w := substr(ds_log_w ||' FUNCAO('||to_char(obter_funcao_ativa)||'); PERFIL('||to_char(obter_perfil_ativo)||')',1,2000);

	gerar_log_prescricao(:new.nr_prescricao, null, null, null, null, ds_log_w, :new.nm_usuario, null, 'N');
end if;

exception
when others then
null;
end;
/


CREATE OR REPLACE TRIGGER TASY.PRESCR_MEDICA_SBIS_IN
before insert or update ON TASY.PRESCR_MEDICA for each row
declare
nr_log_seq_w		log_alteracao_prontuario.nr_sequencia%type;
nm_usuario_w    prescr_medica.nm_usuario%type;

begin

	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  nm_usuario_w := wheb_usuario_pck.get_nm_usuario;
  if (nm_usuario_w is null) then
    nm_usuario_w := :new.nm_usuario;
  end if;

	IF (INSERTING) then
		insert into log_alteracao_prontuario (nr_sequencia,
											 dt_atualizacao,
											 ds_evento,
											 ds_maquina,
											 cd_pessoa_fisica,
											 ds_item,
											  nr_atendimento,
											  nm_usuario) values
											 (nr_log_seq_w,
											 nvl(:new.dt_prescricao, sysdate),
											 obter_desc_expressao(656665) ,
											 wheb_usuario_pck.get_nm_maquina,
											 nvl(obter_pessoa_fisica_usuario(nm_usuario_w,'C'),:new.cd_pessoa_fisica),
											 obter_desc_expressao(296208),
											 :new.nr_atendimento,
											 nm_usuario_w);
	else
		insert into log_alteracao_prontuario (nr_sequencia,
											 dt_atualizacao,
											 ds_evento,
											 ds_maquina,
											 cd_pessoa_fisica,
											 ds_item,
											  nr_atendimento,
											  nm_usuario) values
											 (nr_log_seq_w,
											 nvl(:new.dt_prescricao, sysdate),
											 obter_desc_expressao(302570) ,
											 wheb_usuario_pck.get_nm_maquina,
											 nvl(obter_pessoa_fisica_usuario(nm_usuario_w,'C'),:new.cd_pessoa_fisica),
											 obter_desc_expressao(296208),
											 :new.nr_atendimento,
											 nm_usuario_w);

	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.smart_prescricao
after insert or update ON TASY.PRESCR_MEDICA for each row
declare

reg_integracao_w		gerar_int_padrao.reg_integracao;
qt_medicamento_w        number(10);
qt_recomendacao_w		number(10);
qt_jejum_w				number(10);
qt_glicemia_w			number(10);
qt_procedimento_w 		number(10);
qt_dieta_w				number(10);

begin
	if  (:old.DT_LIBERACAO is null) and
		(:new.DT_LIBERACAO is not null) then
		BEGIN
		reg_integracao_w.cd_estab_documento := wheb_usuario_pck.get_cd_estabelecimento;
		SELECT
			(SELECT COUNT(*)
			FROM PRESCR_MATERIAL A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO
				AND A.NR_SEQUENCIA_DIETA IS NULL
				AND A.NR_SEQUENCIA_DILUICAO IS NULL
				AND obter_se_medic_cih(CD_MATERIAL) = 'S'
				AND OBTER_SE_ACM_SN(IE_ACM,IE_SE_NECESSARIO) <> 'S'
				AND A.IE_AGRUPADOR IN (1,4)) qt_medicamento,
			(SELECT COUNT(*)
			FROM PRESCR_RECOMENDACAO A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO
				AND obter_se_envia_recomendacao(:NEW.cd_estabelecimento, a.cd_recomendacao) = 'S') qt_recomendacao,
			(SELECT COUNT(*)
			FROM REP_JEJUM A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO) qt_jejum,
			(SELECT COUNT(*)
			FROM PRESCR_PROCEDIMENTO A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO
				AND NR_SEQ_PROT_GLIC IS NOT NULL
				AND OBTER_SE_ACM_SN(IE_ACM,IE_SE_NECESSARIO) <> 'S') qt_glicemia,
			(SELECT COUNT(*)
			FROM PRESCR_PROCEDIMENTO A
			WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO
				AND NR_SEQ_PROT_GLIC  IS NULL
				AND ((Obter_dados_proc_interno(nr_seq_proc_interno,'TU') = 'E')
					OR obter_cod_exame_lab(NR_SEQ_EXAME) IS NOT NULL)) qt_procedimento,
			(SELECT COUNT(*)
				FROM PRESCR_DIETA  A
				WHERE A.NR_PRESCRICAO = :NEW.NR_PRESCRICAO) qt_dieta				   INTO qt_medicamento_w,
																							qt_recomendacao_w,
																							qt_jejum_w,
																							qt_glicemia_w,
																							qt_procedimento_w,
																							qt_dieta_w
		FROM dual;

		if (qt_medicamento_w > 0) then --Medicamentos  - 305
			gerar_int_padrao.gravar_integracao('305', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE' );
		end if;
		if (qt_recomendacao_w > 0) then  --Recomendacao - 300
		 gerar_int_padrao.gravar_integracao('300', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
		if (qt_procedimento_w > 0 ) then --Procedimento  - 310
			gerar_int_padrao.gravar_integracao('310', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
		if (qt_jejum_w > 0) then --Jejun  - 306
			gerar_int_padrao.gravar_integracao('306', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
		if (qt_glicemia_w > 0) then --Glicemia  - 328
			gerar_int_padrao.gravar_integracao('328', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
		if (qt_dieta_w > 0) then -- Dieta oral  - 320
			gerar_int_padrao.gravar_integracao('320', :new.NR_PRESCRICAO, :new.nm_usuario, reg_integracao_w,  'NR_PRESCRICAO=' || :new.NR_PRESCRICAO || ';DS_OPERACAO=CREATE');
		end if;
	 end;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.UPDATE_DT_EVT_PRESCR_MEDICA
after insert or update ON TASY.PRESCR_MEDICA for each row
declare

begin
if	wheb_usuario_pck.get_ie_executar_trigger = 'S' then

  if (:old.nr_seq_pend_pac_acao is null and :new.nr_seq_pend_pac_acao is not null) then

    if (nvl(pkg_i18n.get_user_locale, 'pt_BR') = 'ja_JP') then
      update_dt_exec_prt_int_pac_evt(:new.nr_seq_pend_pac_acao, :new.dt_atualizacao);
    end if;

  end if;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PRESCR_MEDICA_SBIS_DEL
before delete ON TASY.PRESCR_MEDICA for each row
declare
ie_inativacao_w		varchar2(1);
nr_log_seq_w		number(10);

begin
	select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

	insert into log_alteracao_prontuario (nr_sequencia,
										 dt_atualizacao,
										 ds_evento,
										 ds_maquina,
										 cd_pessoa_fisica,
										 ds_item,
										 nr_atendimento,
										 nm_usuario)
										 values( nr_log_seq_w,
										 sysdate,
										 obter_desc_expressao(493387) ,
										 wheb_usuario_pck.get_nm_maquina,
										 :old.cd_pessoa_fisica,
										 substr(obter_desc_expressao(296208)|| ' ' ||:old.nr_prescricao,1,30),
										 :old.nr_atendimento,
										 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));
end;
/


CREATE OR REPLACE TRIGGER TASY.prescr_medica_aftinsupd
after insert or update ON TASY.PRESCR_MEDICA for each row
declare
nr_seq_forma_laudo_w			number(10);
ie_medico_w						varchar2(10);
nm_usuario_prescr_w				varchar2(15);
ie_status_w						varchar2(1) := 'N';
dt_assinatura_medico_w			date;
begin

if	(nvl(:new.nr_seq_forma_laudo,0) <> nvl(:old.nr_seq_forma_laudo,0)) then

	select  forma_laudo_hist_seq.nextval
	into	nr_seq_forma_laudo_w
	from	dual;

	insert into forma_laudo_hist (nr_sequencia,
					dt_atualizacao,
					nm_usuario,
					dt_atualizacao_nrec,
					nm_usuario_nrec,
					nr_seq_forma_laudo_nova,
					nr_seq_forma_laudo_ant,
					nr_prescricao
					)
				values (nr_seq_forma_laudo_w,
					sysdate,
					:new.nm_usuario,
					sysdate,
					:new.nm_usuario,
					:new.nr_seq_forma_laudo,
					:old.nr_seq_forma_laudo,
					:new.nr_prescricao);
end if;

if	(:new.cd_prescritor	is not null) and	((wheb_assist_pck.get_cd_certificado is not null) or  	 (wheb_assist_pck.get_gerar_sem_certificado = 'S'))then
	ie_medico_w	:= Obter_se_medico(:new.cd_prescritor,'M');

	wheb_assist_pck.set_nr_seq_projeto_ass(1);

	if (not wheb_assist_pck.get_se_gera_assinatura) then
		wheb_assist_pck.set_informacoes_usuario(:new.cd_estabelecimento,:new.cd_perfil_ativo,:new.nm_usuario);
	end if;

	if	(ie_medico_w	= 'S') then
		nm_usuario_prescr_w	:= nvl(obter_usuario_pf(:new.cd_prescritor),:new.nm_usuario);
		if	(:new.dt_liberacao_medico is null) then
			Gerar_registro_pendente_PEP('REP', :new.nr_prescricao, :new.cd_pessoa_fisica, :new.nr_atendimento, nm_usuario_prescr_w,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
		elsif	(:new.dt_liberacao_medico is not null) and	(:old.dt_liberacao_medico is null) then
		 	Gerar_registro_pendente_PEP('XREP', :new.nr_prescricao, :new.cd_pessoa_fisica, :new.nr_atendimento,:new.nm_usuario,'L',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
		end if;

		dt_assinatura_medico_w := obter_data_assinatura_digital(:new.nr_seq_assinatura);

		if (:new.dt_liberacao_medico is not null) and (:old.dt_liberacao_medico is null) and (dt_assinatura_medico_w is null) then
			gerar_registro_pendente_pep(
				cd_tipo_registro_p			=> 'REP',
				nr_sequencia_registro_p		=> :new.nr_prescricao,
				cd_pessoa_fisica_p			=> :new.cd_pessoa_fisica,
				nr_atendimento_p			=> :new.nr_atendimento,
				nm_usuario_p				=> :new.nm_usuario,
				ie_tipo_pendencia_p			=> 'A',
				cd_funcao_pend_p			=> obter_funcao_ativa
			);
		end if;

		if	(dt_assinatura_medico_w is not null) or ((:new.dt_liberacao_medico is not null) and (not wheb_assist_pck.get_se_gera_assinatura)) then
			gerar_registro_pendente_pep(
				cd_tipo_registro_p			=> 'XREP',
				nr_sequencia_registro_p		=> :new.nr_prescricao,
				cd_pessoa_fisica_p			=> :new.cd_pessoa_fisica,
				nr_atendimento_p			=> :new.nr_atendimento,
				nm_usuario_p				=> :new.nm_usuario,
				ie_tipo_pendencia_p			=> 'A',
				cd_funcao_pend_p			=> obter_funcao_ativa
			);
		end if;
	end if;

	if	(:new.dt_liberacao is not null)
	and		(:old.dt_liberacao is null) then
		if	(obter_data_assinatura_digital(:new.nr_seq_assinatura_enf) is null) and
			(((obter_se_usuario_medico (:new.nm_usuario_lib_enf) = 'N') and (nvl(:new.ie_prescr_emergencia,'N') <> 'S')) or
			 (wheb_assist_pck.get_se_gera_assinatura and wheb_assist_pck.get_cd_certificado is not null and wheb_assist_pck.obterParametroFuncao(924,11) = 'S'))then
			Gerar_registro_pendente_PEP('REP',:new.nr_prescricao, :new.cd_pessoa_fisica, :new.nr_atendimento,:new.nm_usuario, 'A',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
		end if;
	end if;

	if	(obter_data_assinatura_digital(:new.nr_seq_assinatura_enf) is not null) or
		((:new.dt_liberacao is not null) and
		 (not wheb_assist_pck.get_se_gera_assinatura)) or
		((trunc(:new.dt_liberacao,'mi') = trunc(:new.dt_liberacao_medico,'mi')) and
		 (:new.dt_liberacao is not null) and
		 (:new.dt_liberacao_medico is not null) and
		 (not wheb_assist_pck.get_se_gera_assinatura)) then
		Gerar_registro_pendente_PEP('XREP',:new.nr_prescricao, :new.cd_pessoa_fisica, :new.nr_atendimento,:new.nm_usuario, 'A',null,null,null,null,null,null,null,null,null,null,null,null,null,obter_funcao_ativa);
	end if;
end if;

/* Desvincular a prescricao e o atendimento da prevenda, quando suspende */
if	(:old.dt_suspensao is null) and
	(:new.dt_suspensao is not null) then

	update	pre_venda_item
	set	nr_atendimento = null,
		nr_prescricao = null,
		nr_seq_interno = null
	where	nr_prescricao = :new.nr_prescricao;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ISCV_PRESCR_MEDICA_AFTINSUP
after update or insert ON TASY.PRESCR_MEDICA for each row
declare
nr_seq_proc_interno_w	prescr_procedimento.nr_seq_proc_interno%type;
ds_sep_bv_w				varchar2(100)	:= obter_separador_bv;
nr_acesso_dicom_w		prescr_procedimento.nr_acesso_dicom%type;
bo_permite_integrar_w	boolean;

Cursor C01 is
	select distinct	NR_ACESSO_DICOM
	from(
		select	NR_ACESSO_DICOM,
				Obter_Se_Integr_Proc_Interno(nr_seq_proc_interno, 17,null,ie_lado,nvl(:new.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento)) ie_permite_proc_integ
		from	prescr_procedimento
		where	nr_prescricao = :new.nr_prescricao
		and		NR_ACESSO_DICOM is not null
		)
	where	ie_permite_proc_integ = 'S';

	/* Verifica se existe alguma integra��o cadastrada para o dom�nio 17, para certificar que h� o que ser procurado, para a� ent�o executar todas as verifica��es. */
	function permiteIntegrarISCV
	return boolean is
	qt_resultado_w	number;
	begin

	select	count(*) qt_resultado
	into	qt_resultado_w
	from 	regra_proc_interno_integra
	where	ie_tipo_integracao = 17; --Dominio criado para a integra��o

	return qt_resultado_w > 0;
	end;


	function permiteIntegrar return boolean is
	ds_retorno_w varchar2(1);
	begin

	select	nvl(max(ie_permite_proc_integ),'N') ie_permite_proc_integ
	into	ds_retorno_w
	from(
			(select	Obter_Se_Integr_Proc_Interno(nr_seq_proc_interno, 17,null,ie_lado,nvl(:new.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento)) ie_permite_proc_integ
			from	prescr_procedimento
			where	nr_prescricao = :new.nr_prescricao
			and		nr_seq_proc_interno is not null)
			union all
			(select	Obter_Se_Integr_Proc_Interno(nr_seq_proc_interno, 17,null,ie_lado,nvl(:new.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento)) ie_permite_proc_integ
			from	agenda_paciente
			where	nr_seq_proc_interno is not null
			and		nr_sequencia = :new.nr_seq_agenda)
			union all
			(select	Obter_Se_Integr_Proc_Interno(nr_seq_proc_interno, 17,null,ie_lado,nvl(:new.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento)) ie_permite_proc_integ
			from	agenda_paciente_proc
			where	nr_seq_proc_interno is not null
			and		nr_sequencia = :new.nr_seq_agenda)
		)
	where	ie_permite_proc_integ = 'S';

	bo_permite_integrar_w := (ds_retorno_w = 'S');

	return bo_permite_integrar_w;

	end;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	permiteIntegrarISCV() then
		if	(((:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) and
			(:old.dt_liberacao_medico is null)) or
			((:old.dt_liberacao_medico is null) and
			(:new.dt_liberacao_medico is not null) and
			(:old.dt_liberacao is null)))
		and	permiteIntegrar() then
			gravar_agend_integracao(745, 'nr_atendimento=' || :new.nr_atendimento || ds_sep_bv_w || 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w, :new.cd_setor_atendimento);

			open C01;
			loop
			fetch C01 into
				nr_acesso_dicom_w;
			exit when C01%notfound;
				begin
				gravar_agend_integracao(749,'cd_pessoa_fisica='	|| :new.cd_pessoa_fisica	|| ds_sep_bv_w ||
											'nr_atendimento='	|| :new.nr_atendimento		|| ds_sep_bv_w ||
											'nr_prescricao='	|| :new.nr_prescricao		|| ds_sep_bv_w ||
											'nr_acesso_dicom='	|| nr_acesso_dicom_w		|| ds_sep_bv_w ||
											'order_control=NW'	|| ds_sep_bv_w				||
											'order_status=SC'	|| ds_sep_bv_w
											, :new.cd_setor_atendimento);
				end;
			end loop;
			close C01;

		elsif ((:old.dt_suspensao is null) and (:new.dt_suspensao is not null))
		and (bo_permite_integrar_w or permiteIntegrar()) then

			open C01;
			loop
			fetch C01 into
				nr_acesso_dicom_w;
			exit when C01%notfound;
				begin
				gravar_agend_integracao(749,'cd_pessoa_fisica='	|| :new.cd_pessoa_fisica	|| ds_sep_bv_w ||
											'nr_atendimento='	|| :new.nr_atendimento		|| ds_sep_bv_w ||
											'nr_prescricao='	|| :new.nr_prescricao		|| ds_sep_bv_w ||
											'nr_acesso_dicom='	|| nr_acesso_dicom_w		|| ds_sep_bv_w ||
											'order_control=OC'	|| ds_sep_bv_w				||
											'order_status=CA'	|| ds_sep_bv_w
											, :new.cd_setor_atendimento);
				end;
			end loop;
			close C01;
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_lib_prescr_medica_insupd
AFTER INSERT OR UPDATE ON TASY.PRESCR_MEDICA FOR EACH ROW
DECLARE

nr_sequencia_w number(10);

begin
    
    if(:old.dt_liberacao is null and :new.dt_liberacao is not null and :new.nr_seq_pend_pac_acao is not null and :new.cd_protocolo is not null)then
        
        select max(d.nr_sequencia)
        into nr_sequencia_w
        from gqa_pend_pac_acao a
        join gqa_pendencia_pac b on(b.nr_sequencia = a.nr_seq_pend_pac)
        join gqa_pendencia_regra c on(c.nr_sequencia = b.nr_seq_pend_regra)
        join escala_sepse d on(d.nr_sequencia = b.nr_seq_escala)
        where c.nr_seq_escala = 124 -- Sepsis
        and a.nr_sequencia = :new.nr_seq_pend_pac_acao
        and d.dt_liberacao is not null
        and d.ie_status_sepsis in('SM', 'RN')
        and d.ie_situacao = 'A'
        and exists
        (
            select 1
            from gqa_acao w 
            where w.nr_sequencia = a.nr_seq_regra_acao
            and w.nr_seq_protocolo = a.nr_seq_protocolo
            and w.nr_seq_pend_regra = c.nr_sequencia
        );
        
        if(nvl(nr_sequencia_w,0) > 0)then
            update escala_sepse set dt_liberacao_status = sysdate, nm_usuario_status = :new.nm_usuario, ds_justif_status = 'Prescri��o Gerada - Status Autom�tico'
            where nr_sequencia = nr_sequencia_w;
        end if;
    
    end if;
exception
when others then
    enviar_email('Exception_prescri��o','Verificar trigger hsj_lib_prescr_medica_insupd, erro na prescri��o '||:new.nr_prescricao,'noreply@hsjose.com.br','rafael.santos@hsjose.com.br','RAFAELSDS','A');

END;
/


CREATE OR REPLACE TRIGGER TASY.prescr_medica_update_hl7
before update ON TASY.PRESCR_MEDICA for each row
declare
ds_sep_bv_w		varchar2(100);
ds_param_integ_hl7_w	varchar2(4000);

nr_seq_interno_w		number(10);
qt_prescr_w		number(10);
nr_seq_prescr_w		number(10);
qt_proc_laudo_w		number(10);
ie_envia_w		varchar2(1);
nr_seq_anamese_w	  	varchar2(10);

Cursor C01 is
	select	a.nr_sequencia
	from	prescr_procedimento a,
		proc_interno_integracao b
	where	a.nr_prescricao	= :new.nr_prescricao
	and	b.nr_seq_proc_interno = a.nr_seq_proc_interno
	and	nvl(b.cd_estabelecimento,nvl(:new.cd_estabelecimento,0)) = nvl(:new.cd_estabelecimento,0)
	and	a.nr_seq_exame is null
	and	b.nr_seq_sistema_integ in (11,66,91,103,108) 		--Philips-DCX, GE-Centricity-PACSIW, Synapse-Fuji, Philips-IBE, GE-Centricity-RISi
	union
	select	nr_sequencia
	from	prescr_procedimento a
	where	a.nr_prescricao	= :new.nr_prescricao		--Philips-Isite
	and	Obter_Se_Integr_Proc_Interno(a.nr_seq_proc_interno, 2, null, a.ie_lado, :new.cd_estabelecimento) = 'S'
	and	a.nr_seq_exame is null;


begin

if	(:new.dt_liberacao	is not null) and
	(:old.dt_liberacao	is null) then

	select	max(obter_atepacu_paciente( :new.nr_atendimento ,'A'))
	into	nr_seq_interno_w
	from	dual;

	ds_sep_bv_w := obter_separador_bv;

	open C01;
	loop
	fetch C01 into
		nr_seq_prescr_w;
	exit when C01%notfound;
		begin

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w; -- Bruno orientou
		gravar_agend_integracao(25, ds_param_integ_hl7_w);

		ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w ||
					'nr_atendimento='   || :new.nr_atendimento   || ds_sep_bv_w ||
					'nr_seq_interno='   || nr_seq_interno_w      || ds_sep_bv_w ||
					'nr_prescricao='    || :new.nr_prescricao    || ds_sep_bv_w ||
					'nr_seq_presc='	    || nr_seq_prescr_w	     || ds_sep_bv_w ||
					'order_control='    || 'NW'    		     || ds_sep_bv_w ||
					'order_status='     || 'SC'    		     || ds_sep_bv_w  ;


		gravar_agend_integracao(23, ds_param_integ_hl7_w);

		gravar_agend_integracao(530, ds_param_integ_hl7_w);

		end;
	end loop;
	close C01;

elsif 	((:new.dt_liberacao is null) and
		 (:old.dt_liberacao is not null)) or
		((:new.dt_liberacao_medico is null) and
		 (:old.dt_liberacao_medico is not null)) then
		--integra��o isite, cancelamento de procedimentos quando for cancelado o atendimento
		ds_sep_bv_w := obter_separador_bv;

		open C01;
			loop
			fetch C01 into
				nr_seq_prescr_w;
			exit when C01%notfound;
				begin

				select	max(obter_atepacu_paciente( :new.nr_atendimento ,'A'))
				into	nr_seq_interno_w
				from	dual;

				ie_envia_w := 'S';

				select	count(*)
				into	qt_proc_laudo_w
				from	prescr_proc_status
				where	nr_prescricao	= :new.nr_prescricao
				and		nr_seq_prescr	= nr_seq_prescr_w
				and		ie_status_exec 	= 40;

				if	(qt_proc_laudo_w > 0) then
					ie_envia_w := 'N';
				end if;

				if	(ie_envia_w = 'S') then

					ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || :new.cd_pessoa_fisica	|| ds_sep_bv_w ||
											'nr_atendimento='   || :new.nr_atendimento		|| ds_sep_bv_w ||
											'nr_seq_interno='   || nr_seq_interno_w			|| ds_sep_bv_w ||
											'nr_prescricao='    || :new.nr_prescricao		|| ds_sep_bv_w ||
											'nr_seq_presc='	    || nr_seq_prescr_w			|| ds_sep_bv_w ||
											'order_control='    || 'OC'				|| ds_sep_bv_w ||
											'order_status='     || 'CA'				|| ds_sep_bv_w ;

					gravar_agend_integracao(23, ds_param_integ_hl7_w);

				end if;

				end;
		end loop;
		close C01;
end if;
end;
/


ALTER TABLE TASY.PRESCR_MEDICA ADD (
  CONSTRAINT PRESMED_PK
 PRIMARY KEY
 (NR_PRESCRICAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          23M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_MEDICA ADD (
  CONSTRAINT PRESMED_STPREOP_FK 
 FOREIGN KEY (NR_SEQ_STATUS_OPM) 
 REFERENCES TASY.STATUS_PRESCR_OPM (NR_SEQUENCIA),
  CONSTRAINT PRESMED_PESFISI_FK4 
 FOREIGN KEY (CD_FARMAC_LIB_PARC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRESMED_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT PRESMED_PRESMED4_FK 
 FOREIGN KEY (NR_PRESCR_INTERF_FARM) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT PRESMED_ATEPACFU_FK 
 FOREIGN KEY (NR_SEQ_ATEND_FUTURO) 
 REFERENCES TASY.ATEND_PAC_FUTURO (NR_SEQUENCIA),
  CONSTRAINT PRESMED_ATECACO_FK 
 FOREIGN KEY (NR_SEQ_ATECACO) 
 REFERENCES TASY.ATEND_CATEGORIA_CONVENIO (NR_SEQ_INTERNO),
  CONSTRAINT PRESMED_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ORIG) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRESMED_JUSALTA_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFICATIVA) 
 REFERENCES TASY.JUSTIFICATIVA_ALT_ADEP (NR_SEQUENCIA),
  CONSTRAINT PRESMED_TASAJVP2_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA_FARM) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PRESMED_TASASDI3_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA_ENF) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PRESMED_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT PRESMED_GQAPPAC_FK 
 FOREIGN KEY (NR_SEQ_PEND_PAC_ACAO) 
 REFERENCES TASY.GQA_PEND_PAC_ACAO (NR_SEQUENCIA),
  CONSTRAINT PRESMED_AGECONS_FK 
 FOREIGN KEY (NR_SEQ_AGECONS) 
 REFERENCES TASY.AGENDA_CONSULTA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRESMED_OFTCONS_FK 
 FOREIGN KEY (NR_SEQ_CONSULTA_OFT) 
 REFERENCES TASY.OFT_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT PRESMED_LESECFI_FK 
 FOREIGN KEY (NR_SEQ_FICHA_LOTE) 
 REFERENCES TASY.LOTE_ENT_SEC_FICHA (NR_SEQUENCIA),
  CONSTRAINT PRESMED_LESEC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ENTRADA) 
 REFERENCES TASY.LOTE_ENT_SECRETARIA (NR_SEQUENCIA),
  CONSTRAINT PRESMED_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT PRESMED_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO_ANTERIOR) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT PRESMED_PROMEDI_FK 
 FOREIGN KEY (CD_PROTOCOLO, NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (CD_PROTOCOLO,NR_SEQUENCIA),
  CONSTRAINT PRESMED_SETATEN2_FK_I 
 FOREIGN KEY (CD_SETOR_ENTREGA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRESMED_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRESMED_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT PRESMED_PRESMED2_FK 
 FOREIGN KEY (NR_PRESCRICAO_MAE) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT PRESMED_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT PRESMED_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PRESMED_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRESMED_PESFISM_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRESMED_PESFISI_FK2 
 FOREIGN KEY (CD_RECEM_NATO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRESMED_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PRESMED_PESFISI_FK9 
 FOREIGN KEY (CD_PRESCRITOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRESMED_PESFISI_FK3 
 FOREIGN KEY (CD_FARMAC_LIB) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRESMED_PACCLCR_FK 
 FOREIGN KEY (NR_SEQ_CLEARENCE_PEP) 
 REFERENCES TASY.PAC_CLEREANCE_CREATININA (NR_SEQUENCIA),
  CONSTRAINT PRESMED_STAPREF_FK 
 FOREIGN KEY (NR_SEQ_STATUS_FARM) 
 REFERENCES TASY.STATUS_PRESCR_FARMACIA (NR_SEQUENCIA),
  CONSTRAINT PRESMED_TRAPRES_FK 
 FOREIGN KEY (NR_SEQ_TRANSCRICAO) 
 REFERENCES TASY.TRANSCRICAO_PRESCRICAO (NR_SEQUENCIA),
  CONSTRAINT PRESMED_MOTSUPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_PRESCR (NR_SEQUENCIA),
  CONSTRAINT PRESMED_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PRESMED_PRESMED3_FK 
 FOREIGN KEY (NR_PRESCRICAO_ORIGINAL) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT PRESMED_FORENLA_FK 
 FOREIGN KEY (NR_SEQ_FORMA_LAUDO) 
 REFERENCES TASY.FORMA_ENTREGA_LAUDO (NR_SEQUENCIA),
  CONSTRAINT PRESMED_PESJURI_FK 
 FOREIGN KEY (CD_CGC_SOLIC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PRESMED_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.PRESCR_MEDICA TO NIVEL_1;

