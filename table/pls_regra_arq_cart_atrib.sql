ALTER TABLE TASY.PLS_REGRA_ARQ_CART_ATRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_ARQ_CART_ATRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_ARQ_CART_ATRIB
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_REGRA             NUMBER(10)           NOT NULL,
  CD_ATRIBUTO              NUMBER(3)            NOT NULL,
  CD_TIPO_ATRIBUTO         VARCHAR2(1 BYTE)     NOT NULL,
  NR_ORDEM                 NUMBER(5)            NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  VL_PADRAO                VARCHAR2(255 BYTE),
  DS_CABECALHO             VARCHAR2(255 BYTE),
  DS_MASCARA               VARCHAR2(60 BYTE),
  QT_TAMANHO               NUMBER(10),
  VL_FIXO_INICIO           VARCHAR2(60 BYTE),
  CD_CARACTER_COMPLETAR    VARCHAR2(1 BYTE),
  IE_POSICAO_COMPLETAR     VARCHAR2(1 BYTE),
  NR_ORDEM_RESULTADO       NUMBER(5),
  IE_LETRAS_VALOR          VARCHAR2(1 BYTE),
  IE_SOMENTE_SEM_CARENCIA  VARCHAR2(1 BYTE),
  IE_CARENCIAS_CUMPRIDAS   VARCHAR2(2 BYTE)     DEFAULT null,
  IE_VL_PADRAO_CONDICAO    VARCHAR2(1 BYTE),
  IE_EXIBIR_ATRIBUTO       VARCHAR2(1 BYTE),
  DS_FUNCTION              VARCHAR2(255 BYTE),
  IE_REMOVER_ACENTUACAO    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRCTA_PK ON TASY.PLS_REGRA_ARQ_CART_ATRIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRCTA_PLSRACT_FK_I ON TASY.PLS_REGRA_ARQ_CART_ATRIB
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_arq_cart_atrib_utd 
before insert or update ON TASY.PLS_REGRA_ARQ_CART_ATRIB 
for each row
declare 
 
begin 
 
if	(:new.qt_tamanho is not null) then 
	if	(:new.ds_cabecalho is not null) then 
		if	(length(:new.ds_cabecalho) > :new.qt_tamanho) then 
			wheb_mensagem_pck.exibir_mensagem_abort('N�o � poss�vel cadastrar o cabe�alho com tamanho maior que o tamanho definido para o atributo.'); 
		end if; 
	end if; 
	 
	if	(:new.vl_padrao is not null) then 
		if	(length(:new.vl_padrao) > :new.qt_tamanho) then 
			wheb_mensagem_pck.exibir_mensagem_abort('N�o � poss�vel cadastrar o valor padr�o com tamanho maior que o tamanho definido para o atributo.'); 
		end if; 
	end if; 
	 
	if	(:new.vl_fixo_inicio is not null) then 
		if	(length(:new.vl_fixo_inicio) > :new.qt_tamanho) then 
			wheb_mensagem_pck.exibir_mensagem_abort('N�o � poss�vel cadastrar o Valor in�cio com tamanho maior que o tamanho definido para o atributo.'); 
		end if; 
	end if; 
end if; 
 
if	(:new.cd_caracter_completar is not null and :new.ie_posicao_completar is null) then 
	wheb_mensagem_pck.exibir_mensagem_abort('� necess�rio informar a posi��o do caractere que ser� utilizado para completar o atributo.'); 
end if; 
 
end;
/


ALTER TABLE TASY.PLS_REGRA_ARQ_CART_ATRIB ADD (
  CONSTRAINT PLSRCTA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_ARQ_CART_ATRIB ADD (
  CONSTRAINT PLSRCTA_PLSRACT_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_ARQ_CARTEIRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_ARQ_CART_ATRIB TO NIVEL_1;

