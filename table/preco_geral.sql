ALTER TABLE TASY.PRECO_GERAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRECO_GERAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_GERAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_FINAL_VIGENCIA    DATE,
  DT_INICIO_VIGENCIA   DATE,
  VL_PROCEDIMENTO      NUMBER(17,4),
  QT_PONTO             NUMBER(15,2),
  NR_SEQ_TABELA        NUMBER(10),
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PREGERA_PK ON TASY.PRECO_GERAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREGERA_PROCEDI_FK_I ON TASY.PRECO_GERAL
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREGERA_TABGERAL_FK_I ON TASY.PRECO_GERAL
(NR_SEQ_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRECO_GERAL ADD (
  CONSTRAINT PREGERA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRECO_GERAL ADD (
  CONSTRAINT PREGERA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PREGERA_TABGERAL_FK 
 FOREIGN KEY (NR_SEQ_TABELA) 
 REFERENCES TASY.TABELA_GERAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PRECO_GERAL TO NIVEL_1;

