ALTER TABLE TASY.REGRA_LOCAL_OS_EQUIP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_LOCAL_OS_EQUIP CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_LOCAL_OS_EQUIP
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_EQUIP          NUMBER(10),
  NR_SEQ_GRUPO_TRABALHO      NUMBER(10),
  NR_SEQ_GRUPO_PLANEJAMENTO  NUMBER(10),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  CD_PERFIL                  NUMBER(5),
  IE_REGRA                   VARCHAR2(15 BYTE)  NOT NULL,
  DT_INICIO_VIGENCIA         DATE,
  DT_FIM_VIGENCIA            DATE,
  NR_SEQ_EQUIPAMENTO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGLOEQ_ESTABEL_FK_I ON TASY.REGRA_LOCAL_OS_EQUIP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLOEQ_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGLOEQ_MANEQUI_FK_I ON TASY.REGRA_LOCAL_OS_EQUIP
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGLOEQ_MANGRPL_FK_I ON TASY.REGRA_LOCAL_OS_EQUIP
(NR_SEQ_GRUPO_PLANEJAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLOEQ_MANGRPL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGLOEQ_MANGRTR_FK_I ON TASY.REGRA_LOCAL_OS_EQUIP
(NR_SEQ_GRUPO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLOEQ_MANGRTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGLOEQ_MANTPEQ_FK_I ON TASY.REGRA_LOCAL_OS_EQUIP
(NR_SEQ_TIPO_EQUIP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLOEQ_MANTPEQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGLOEQ_PERFIL_FK_I ON TASY.REGRA_LOCAL_OS_EQUIP
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLOEQ_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REGLOEQ_PK ON TASY.REGRA_LOCAL_OS_EQUIP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLOEQ_PK
  MONITORING USAGE;


CREATE INDEX TASY.REGLOEQ_SETATEN_FK_I ON TASY.REGRA_LOCAL_OS_EQUIP
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGLOEQ_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.REGRA_LOCAL_OS_EQUIP ADD (
  CONSTRAINT REGLOEQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_LOCAL_OS_EQUIP ADD (
  CONSTRAINT REGLOEQ_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGLOEQ_MANGRPL_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PLANEJAMENTO) 
 REFERENCES TASY.MAN_GRUPO_PLANEJAMENTO (NR_SEQUENCIA),
  CONSTRAINT REGLOEQ_MANGRTR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TRABALHO) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT REGLOEQ_MANTPEQ_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EQUIP) 
 REFERENCES TASY.MAN_TIPO_EQUIPAMENTO (NR_SEQUENCIA),
  CONSTRAINT REGLOEQ_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT REGLOEQ_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGLOEQ_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_LOCAL_OS_EQUIP TO NIVEL_1;

