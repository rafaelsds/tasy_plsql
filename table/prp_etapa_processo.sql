ALTER TABLE TASY.PRP_ETAPA_PROCESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRP_ETAPA_PROCESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRP_ETAPA_PROCESSO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_ETAPA             VARCHAR2(255 BYTE)       NOT NULL,
  NM_ETAPA_INGLES      VARCHAR2(255 BYTE),
  DS_OBJETIVO          VARCHAR2(4000 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRPETA_PK ON TASY.PRP_ETAPA_PROCESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PRP_ETAPA_PROCESSO_UPDATE
before update ON TASY.PRP_ETAPA_PROCESSO for each row
declare
qt_registros_w	number(10);

begin

if (:new.ie_situacao = 'I') then

	select	count(*)
	into	qt_registros_w
	from	PRP_ETAPA_DEPENDENCIA
	where	NR_SEQ_ETAPA_PROCESSO = :new.nr_sequencia;

	if (qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(309514);
	end if;

	select	count(*)
	into	qt_registros_w
	from	PRP_NECESSIDADE_ETAPA
	where	NR_SEQ_ETAPA_PROCESSO = :new.nr_sequencia;

	if (qt_registros_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(309514);
	end if;

end if;

end;
/


ALTER TABLE TASY.PRP_ETAPA_PROCESSO ADD (
  CONSTRAINT PRPETA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PRP_ETAPA_PROCESSO TO NIVEL_1;

