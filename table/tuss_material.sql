ALTER TABLE TASY.TUSS_MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TUSS_MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TUSS_MATERIAL
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_VERSAO             VARCHAR2(40 BYTE)       NOT NULL,
  DS_INFORMACAO         VARCHAR2(120 BYTE)      NOT NULL,
  DT_LIBERACAO          DATE,
  NM_USUARIO_LIBERACAO  VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_IMPORTACAO         DATE,
  IE_TERMINOLOGIA       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TUSSMAT_PK ON TASY.TUSS_MATERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TUSSMAT_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.TUSS_MATERIAL_tp  after update ON TASY.TUSS_MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_INFORMACAO,1,4000),substr(:new.DS_INFORMACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_INFORMACAO',ie_log_w,ds_w,'TUSS_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_LIBERACAO,1,4000),substr(:new.NM_USUARIO_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_LIBERACAO',ie_log_w,ds_w,'TUSS_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_VERSAO,1,4000),substr(:new.CD_VERSAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_VERSAO',ie_log_w,ds_w,'TUSS_MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_LIBERACAO,1,4000),substr(:new.DT_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_LIBERACAO',ie_log_w,ds_w,'TUSS_MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TUSS_MATERIAL ADD (
  CONSTRAINT TUSSMAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.TUSS_MATERIAL TO NIVEL_1;

