ALTER TABLE TASY.DEPOSITO_CHEQUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DEPOSITO_CHEQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.DEPOSITO_CHEQUE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DEPOSITO      NUMBER(10)               NOT NULL,
  NR_SEQ_CHEQUE        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DEPCHEQ_CHEQUES_FK_I ON TASY.DEPOSITO_CHEQUE
(NR_SEQ_CHEQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DEPCHEQ_DEPOSIT_FK_I ON TASY.DEPOSITO_CHEQUE
(NR_SEQ_DEPOSITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DEPCHEQ_PK ON TASY.DEPOSITO_CHEQUE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.DEPCHEQ_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.DEPOSITO_CHEQUE_INSERT
before insert ON TASY.DEPOSITO_CHEQUE for each row
declare
ie_existe_cheque_w	varchar2(1)	:= 'N';
ie_consistir_w		varchar2(1)	:= 'N';
ie_permite_deposito_w	varchar2(1)	:= 'S';
cd_moeda_w		cheque_cr.cd_moeda%type;
nr_cheque_w		cheque_cr.nr_cheque%type;
ds_moeda_w		moeda.ds_moeda%type;
ds_moeda_deposito_w	moeda.ds_moeda%type;
cd_estabelecimento_w	deposito.cd_estabelecimento%type;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
select	max(cd_estabelecimento)
into	cd_estabelecimento_w
from	deposito a
where	a.nr_sequencia	= :new.nr_seq_deposito;

obter_param_usuario(810, 81, obter_perfil_ativo, :new.nm_usuario, nvl(cd_estabelecimento_w,0), ie_permite_deposito_w);

if	(nvl(ie_permite_deposito_w,'S') = 'N') then

	select	nvl(max('S'),'N')
	into	ie_existe_cheque_w
	from	deposito_cheque a
	where	a.nr_seq_deposito	= :new.nr_seq_deposito;

	if	(ie_existe_cheque_w = 'S') then

		select	max(a.cd_moeda),
			max(a.nr_cheque)
		into	cd_moeda_w,
			nr_cheque_w
		from	cheque_cr a
		where	a.nr_seq_cheque		= :new.nr_seq_cheque;

		select	nvl(max('N'),'S')
		into	ie_consistir_w
		from	deposito_cheque a,
			cheque_cr b
		where	a.nr_seq_cheque		= b.nr_seq_cheque
		and	a.nr_seq_deposito	= :new.nr_seq_deposito
		and	b.cd_moeda		= cd_moeda_w;

		if	(ie_consistir_w = 'S') then
			select	max(a.ds_moeda)
			into	ds_moeda_w
			from	moeda a
			where	a.cd_moeda	= cd_moeda_w;

			select	max(c.ds_moeda)
			into	ds_moeda_deposito_w
			from	deposito_cheque a,
				cheque_cr b,
				moeda c
			where	a.nr_seq_cheque		= b.nr_seq_cheque
			and	b.cd_moeda		= c.cd_moeda
			and	a.nr_seq_deposito	= :new.nr_seq_deposito;

			/* O cheque nr_cheque_w e em cd_moeda_w. O deposito nr_seq_deposito_w so permite cheques em cd_moeda_deposito_w! */
			wheb_mensagem_pck.exibir_mensagem_abort(302904,	'NR_CHEQUE_W='||nr_cheque_w||
									';CD_MOEDA_W='||ds_moeda_w||
									';NR_SEQ_DEPOSITO_W='||:new.nr_seq_deposito||
									';CD_MOEDA_DEPOSITO_W='||ds_moeda_deposito_w);
		end if;

	end if;
end if;

end if;

end;
/


ALTER TABLE TASY.DEPOSITO_CHEQUE ADD (
  CONSTRAINT DEPCHEQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DEPOSITO_CHEQUE ADD (
  CONSTRAINT DEPCHEQ_DEPOSIT_FK 
 FOREIGN KEY (NR_SEQ_DEPOSITO) 
 REFERENCES TASY.DEPOSITO (NR_SEQUENCIA),
  CONSTRAINT DEPCHEQ_CHEQUES_FK 
 FOREIGN KEY (NR_SEQ_CHEQUE) 
 REFERENCES TASY.CHEQUE_CR (NR_SEQ_CHEQUE));

GRANT SELECT ON TASY.DEPOSITO_CHEQUE TO NIVEL_1;

