ALTER TABLE TASY.PLS_REGRA_VINC_MAT_UNIMED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_VINC_MAT_UNIMED CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_VINC_MAT_UNIMED
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_MAT_MED               VARCHAR2(3 BYTE)     NOT NULL,
  IE_TIPO                  VARCHAR2(2 BYTE)     NOT NULL,
  NR_SEQ_ESTRUTURA         NUMBER(10)           NOT NULL,
  CD_CLASSE_MATERIAL       NUMBER(5),
  DS_GRUPO_FARMACOLOGICO   VARCHAR2(255 BYTE),
  DS_CLASSE_FARMACOLOGICO  VARCHAR2(255 BYTE),
  DS_ESPECIALIDADE         VARCHAR2(255 BYTE),
  DS_CLASSE                VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO       NUMBER(4),
  IE_TIPO_TABELA           VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRMUN_CLAMATE_FK_I ON TASY.PLS_REGRA_VINC_MAT_UNIMED
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRMUN_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSRMUN_ESTABEL_FK_I ON TASY.PLS_REGRA_VINC_MAT_UNIMED
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRMUN_I1 ON TASY.PLS_REGRA_VINC_MAT_UNIMED
(IE_MAT_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSRMUN_PK ON TASY.PLS_REGRA_VINC_MAT_UNIMED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRMUN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRMUN_PLSESMAT_FK_I ON TASY.PLS_REGRA_VINC_MAT_UNIMED
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRMUN_PLSESMAT_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_VINC_MAT_UNIMED ADD (
  CONSTRAINT PLSRMUN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_VINC_MAT_UNIMED ADD (
  CONSTRAINT PLSRMUN_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRMUN_PLSESMAT_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PLS_ESTRUTURA_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSRMUN_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL));

GRANT SELECT ON TASY.PLS_REGRA_VINC_MAT_UNIMED TO NIVEL_1;

