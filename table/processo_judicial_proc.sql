ALTER TABLE TASY.PROCESSO_JUDICIAL_PROC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCESSO_JUDICIAL_PROC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCESSO_JUDICIAL_PROC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_PROCESSO       NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_AREA_PROCEDIMENTO  NUMBER(15),
  CD_ESPECIALIDADE      NUMBER(15),
  CD_GRUPO_PROC         NUMBER(15),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSJUPR_AREPROC_FK_I ON TASY.PROCESSO_JUDICIAL_PROC
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSJUPR_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSJUPR_ESPPROC_FK_I ON TASY.PROCESSO_JUDICIAL_PROC
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSJUPR_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSJUPR_GRUPROC_FK_I ON TASY.PROCESSO_JUDICIAL_PROC
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSJUPR_GRUPROC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSJUPR_PK ON TASY.PROCESSO_JUDICIAL_PROC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSJUPR_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSJUPR_PLSPJUL_FK_I ON TASY.PROCESSO_JUDICIAL_PROC
(NR_SEQ_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSJUPR_PLSPJUL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSJUPR_PROCEDI_FK_I ON TASY.PROCESSO_JUDICIAL_PROC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSJUPR_PROCEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROCESSO_JUDICIAL_PROC ADD (
  CONSTRAINT PLSJUPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCESSO_JUDICIAL_PROC ADD (
  CONSTRAINT PLSJUPR_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSJUPR_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSJUPR_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSJUPR_PLSPJUL_FK 
 FOREIGN KEY (NR_SEQ_PROCESSO) 
 REFERENCES TASY.PROCESSO_JUDICIAL_LIMINAR (NR_SEQUENCIA),
  CONSTRAINT PLSJUPR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PROCESSO_JUDICIAL_PROC TO NIVEL_1;

