ALTER TABLE TASY.FORMULARIO_TERAPIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FORMULARIO_TERAPIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.FORMULARIO_TERAPIA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PROVEDOR_SERVICO        NUMBER(10),
  QT_PGTO_ADICIONAL          NUMBER(10),
  QT_PGTO_BRUTO              NUMBER(10),
  NR_MEDICAMENTO1            VARCHAR2(50 BYTE),
  NR_FATOR1                  VARCHAR2(50 BYTE),
  NR_MEDICAMENTO2            VARCHAR2(50 BYTE),
  NR_FATOR2                  VARCHAR2(50 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  NR_MEDICAMENTO3            VARCHAR2(50 BYTE),
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  NR_FATOR3                  VARCHAR2(50 BYTE),
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_CUSTO_TOTAL             NUMBER(10),
  NR_FATOR_CUSTO             VARCHAR2(50 BYTE),
  QT_KM                      NUMBER(10),
  IE_VISITA_RESIDENCIAL1     VARCHAR2(3 BYTE),
  NR_FATOR_VISITA1           VARCHAR2(50 BYTE),
  IE_VISITA_RESIDENCIAL2     VARCHAR2(3 BYTE),
  NR_FATOR_VISITA2           VARCHAR2(50 BYTE),
  NR_NOTA_FISCAL             VARCHAR2(100 BYTE),
  NR_RECEITA                 VARCHAR2(50 BYTE),
  IE_PRIMEIRA_PRESCRICAO     VARCHAR2(1 BYTE),
  IE_PRESCRICAO_FOLLOW_UP    VARCHAR2(1 BYTE),
  IE_TERAPIA_GRUPO           VARCHAR2(1 BYTE),
  DT_INICIO_TRATAMENTO       DATE,
  IE_PRESCRICAO_FORA_NORMAL  VARCHAR2(1 BYTE),
  IE_VISITA_RESIDENCIAL      VARCHAR2(1 BYTE),
  IE_RELATORIO_TERAPIA       VARCHAR2(1 BYTE),
  IE_TERAPIA_VOZ             VARCHAR2(1 BYTE),
  IE_TERAPIA_FALA            VARCHAR2(1 BYTE),
  IE_TERAPIA_LINGUA          VARCHAR2(1 BYTE),
  QT_MINUTOS_TERAPIA         NUMBER(10),
  QT_PRESCRICAO              NUMBER(10),
  QT_FREQUENCIA_TERAPIA      NUMBER(10),
  IE_INDICACAO               VARCHAR2(10 BYTE),
  CD_CID_DOENCA1             VARCHAR2(10 BYTE),
  CD_CID_DOENCA2             VARCHAR2(10 BYTE),
  DS_DIAGNOSTICO_PRINC       VARCHAR2(2000 BYTE),
  DS_ACHADOS_INTELECTUAIS    VARCHAR2(2000 BYTE),
  DS_METAS_TERAPIA           VARCHAR2(2000 BYTE),
  DS_JUSTIFICATIVA_MEDICA    VARCHAR2(2000 BYTE),
  DS_AUDIOGRAMA_DE           VARCHAR2(50 BYTE),
  IE_DETERMINADO_POR         VARCHAR2(1 BYTE),
  IE_AUDIO_DIREITA           VARCHAR2(1 BYTE),
  IE_AUDIO_ESQUERDA          VARCHAR2(1 BYTE)   DEFAULT null,
  IE_ACHADO_DIREITA          VARCHAR2(2000 BYTE) DEFAULT null,
  IE_ACHADO_ESQUERDA         VARCHAR2(2000 BYTE),
  DS_LARINGOSCOPIA           VARCHAR2(255 BYTE),
  DS_ESTROBOSCOPIA_DIR       VARCHAR2(255 BYTE),
  DS_ESTROBOSCOPIA_ESQ       VARCHAR2(255 BYTE),
  DS_AMPLITUDE_DIR           VARCHAR2(255 BYTE),
  DS_AMPLITUDE_ESQ           VARCHAR2(255 BYTE),
  DS_DESLOCAMENTO_DIR        VARCHAR2(50 BYTE),
  DS_DESLOCAMENTO_ESQ        VARCHAR2(50 BYTE),
  IE_REGULARIDADE            VARCHAR2(1 BYTE),
  IE_FECHAMENTO_GLOTE        VARCHAR2(1 BYTE),
  IE_MUSTER                  NUMBER(5),
  CD_MEDICAMENTO             NUMBER(10),
  QT_POR_SEMANA              NUMBER(10),
  QT_PRESCRICAO2             NUMBER(10),
  CD_MEDICAMENTO2            NUMBER(10),
  QT_POR_SEMANA2             NUMBER(10),
  DS_PRESCRICAO2             VARCHAR2(40 BYTE),
  DS_PRESCRICAO              VARCHAR2(40 BYTE),
  NR_SEQ_TERAPIA             NUMBER(10),
  NR_SEQ_TERAPIA2            NUMBER(10),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  IE_TAXA                    VARCHAR2(1 BYTE)   DEFAULT null,
  CD_MEDICO                  VARCHAR2(10 BYTE),
  NR_RQE                     VARCHAR2(20 BYTE),
  NR_BSNR                    VARCHAR2(25 BYTE),
  DS_POR_SEMANA2             VARCHAR2(30 BYTE),
  DS_POR_SEMANA              VARCHAR2(30 BYTE),
  IE_TERAPIA                 VARCHAR2(1 BYTE),
  DS_DOENCA                  VARCHAR2(2000 BYTE),
  DS_GRUPO_DOENCA            VARCHAR2(2000 BYTE),
  IE_SINTOMA_01              VARCHAR2(1 BYTE),
  IE_SINTOMA_02              VARCHAR2(1 BYTE),
  IE_SINTOMA_03              VARCHAR2(1 BYTE),
  IE_SINTOMA_IND             VARCHAR2(1 BYTE),
  DS_SINTOMA_IND             VARCHAR2(2000 BYTE),
  NR_MEDICAMENTO_COMPL       VARCHAR2(50 BYTE),
  DS_FREQUENCIA_TER1         VARCHAR2(255 BYTE),
  DS_FREQUENCIA_TER2         VARCHAR2(255 BYTE),
  DS_FREQUENCIA_TER3         VARCHAR2(255 BYTE),
  DS_FREQUENCIA_COMPL        VARCHAR2(255 BYTE),
  IE_TERAPIA_URGENTE         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FORTERP_ATEPACI_FK_I ON TASY.FORMULARIO_TERAPIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORTERP_KVTERFI_FK_I ON TASY.FORMULARIO_TERAPIA
(NR_SEQ_TERAPIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORTERP_PESFISI_FK_I ON TASY.FORMULARIO_TERAPIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FORTERP_PK ON TASY.FORMULARIO_TERAPIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FORTERP_PROFISS_FK_I ON TASY.FORMULARIO_TERAPIA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.FORMULARIO_TERAPIA_UPDATE
before update ON TASY.FORMULARIO_TERAPIA for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';

begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

    select	decode (count(*),0,'N','S')
    into 	is_rule_exists_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	((b.IE_TIPO_PEND_EMISSAO  = 'KV13' and :new.ie_muster = 13) or (b.IE_TIPO_PEND_EMISSAO  = 'KV14' and :new.ie_muster = 14) or (b.IE_TIPO_PEND_EMISSAO  = 'KV18' and :new.ie_muster = 18))
    and   	obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');

    if (:new.ie_muster = 13 and is_rule_exists_w ='S') then
    begin
    select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO = 'KV13'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


	wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
                              end;
   elsif(:new.ie_muster = 14 and is_rule_exists_w ='S') then
   begin
   select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO ='KV14'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


	wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);

   end;
   elsif(:new.ie_muster = 18 and is_rule_exists_w ='S') then
   begin
   select	nvl(b.qt_tempo_normal,0),
			nvl(b.nr_sequencia, 0)
    into 	qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
			wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   	b.IE_TIPO_PEND_EMISSAO ='KV18'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


	wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);

   end;
   end if;

	end if;
end;
/


ALTER TABLE TASY.FORMULARIO_TERAPIA ADD (
  CONSTRAINT FORTERP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FORMULARIO_TERAPIA ADD (
  CONSTRAINT FORTERP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT FORTERP_KVTERFI_FK 
 FOREIGN KEY (NR_SEQ_TERAPIA) 
 REFERENCES TASY.KV_TERAPIA_FISIO (NR_SEQUENCIA),
  CONSTRAINT FORTERP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT FORTERP_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.FORMULARIO_TERAPIA TO NIVEL_1;

