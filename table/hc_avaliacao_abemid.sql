ALTER TABLE TASY.HC_AVALIACAO_ABEMID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HC_AVALIACAO_ABEMID CASCADE CONSTRAINTS;

CREATE TABLE TASY.HC_AVALIACAO_ABEMID
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  IE_COMPLEXIDADE          NUMBER(5),
  NR_SEQ_PAC_HOME_CARE     NUMBER(10),
  QT_PROG_DIAS_ATEND       NUMBER(5),
  NR_TOTAL_PONTOS          NUMBER(10),
  DT_REGISTRO              DATE,
  DS_OBSERVACAO            VARCHAR2(2000 BYTE),
  IE_SP_SONDA_PERM         VARCHAR2(1 BYTE),
  IE_DR_DEPENDENTE         VARCHAR2(255 BYTE),
  IE_SP_SONDA_INTER        VARCHAR2(1 BYTE),
  IE_TN_SUPLE_ORAL         VARCHAR2(255 BYTE),
  IE_TN_GASTRONOMIA        VARCHAR2(255 BYTE),
  IE_SP_TRAQUE_SEM_ASP     VARCHAR2(1 BYTE),
  IE_TN_SNE                VARCHAR2(255 BYTE),
  IE_SP_TRAQUE_COM_ASP     VARCHAR2(1 BYTE),
  IE_SP_ASP_AEREA_SUP      VARCHAR2(1 BYTE),
  IE_TN_JEJUNO             VARCHAR2(255 BYTE),
  IE_TN_NUTRICAO           VARCHAR2(255 BYTE),
  IE_SP_ACES_VENOSO_CONT   VARCHAR2(1 BYTE),
  IE_SP_ACES_VENOSO_INTER  VARCHAR2(1 BYTE),
  IE_SP_ACES_VENESO_PERI   VARCHAR2(1 BYTE),
  IE_SP_DIALISE_DOMI       VARCHAR2(1 BYTE),
  IE_QU_ORAL               VARCHAR2(1 BYTE),
  IE_QU_SUBCUTANEA         VARCHAR2(1 BYTE),
  IE_QU_INTRA_VENOSA       VARCHAR2(1 BYTE),
  IE_QU_INTRA_TECAL        VARCHAR2(1 BYTE),
  IE_SV_O2_INTER           VARCHAR2(1 BYTE),
  IE_SV_O2_CONT            VARCHAR2(1 BYTE),
  IE_SV_VENTI_INTER        VARCHAR2(1 BYTE),
  IE_SV_VENTI_CONTI        VARCHAR2(1 BYTE),
  IE_LV_ULCERA_GRAUI       VARCHAR2(1 BYTE),
  IE_LV_ULCERA_GRAUII      VARCHAR2(1 BYTE),
  IE_LV_ULCERA_GRAUIII     VARCHAR2(1 BYTE),
  IE_LV_ULCERA_GRAUIV      VARCHAR2(1 BYTE),
  IE_GA_INDEPENDENTE       VARCHAR2(1 BYTE),
  IE_GA_SEMI_INDEPEN       VARCHAR2(1 BYTE),
  IE_GA_DEPEN_TOTAL        VARCHAR2(1 BYTE),
  IE_DR_INDEPENDENTE       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HCAVABE_PACHOCA_FK_I ON TASY.HC_AVALIACAO_ABEMID
(NR_SEQ_PAC_HOME_CARE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HCAVABE_PACHOCA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HCAVABE_PK ON TASY.HC_AVALIACAO_ABEMID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HCAVABE_PK
  MONITORING USAGE;


ALTER TABLE TASY.HC_AVALIACAO_ABEMID ADD (
  CONSTRAINT HCAVABE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HC_AVALIACAO_ABEMID ADD (
  CONSTRAINT HCAVABE_PACHOCA_FK 
 FOREIGN KEY (NR_SEQ_PAC_HOME_CARE) 
 REFERENCES TASY.PACIENTE_HOME_CARE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.HC_AVALIACAO_ABEMID TO NIVEL_1;

