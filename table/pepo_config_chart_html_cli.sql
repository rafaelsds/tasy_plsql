ALTER TABLE TASY.PEPO_CONFIG_CHART_HTML_CLI
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEPO_CONFIG_CHART_HTML_CLI CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEPO_CONFIG_CHART_HTML_CLI
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_ESTILO_GRAFICO_HTML  VARCHAR2(30 BYTE)     NOT NULL,
  DS_COR_GRAFICO_HTML     VARCHAR2(10 BYTE)     NOT NULL,
  IE_DESENHO_GRAFICO      VARCHAR2(30 BYTE)     NOT NULL,
  NR_SEQ_PEPO_CONFIG      NUMBER(10)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPOCCHC_PEPOCCH_FK_I ON TASY.PEPO_CONFIG_CHART_HTML_CLI
(NR_SEQ_PEPO_CONFIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEPOCCHC_PK ON TASY.PEPO_CONFIG_CHART_HTML_CLI
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PEPO_CONFIG_CHART_HTML_CLI ADD (
  CONSTRAINT PEPOCCHC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PEPO_CONFIG_CHART_HTML_CLI ADD (
  CONSTRAINT PEPOCCHC_PEPOCCH_FK 
 FOREIGN KEY (NR_SEQ_PEPO_CONFIG) 
 REFERENCES TASY.PEPO_CONFIG_CHART_HTML (NR_SEQUENCIA));

GRANT SELECT ON TASY.PEPO_CONFIG_CHART_HTML_CLI TO NIVEL_1;

