ALTER TABLE TASY.REP_DIRETORIO_REL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REP_DIRETORIO_REL CASCADE CONSTRAINTS;

CREATE TABLE TASY.REP_DIRETORIO_REL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RELATORIO     NUMBER(10)               NOT NULL,
  DS_DIRETORIO_PADRAO  VARCHAR2(4000 BYTE),
  IE_GERAR_LIBERACAO   VARCHAR2(1 BYTE),
  IE_INTEGRACAO_DICOM  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REDIRREL_I1 ON TASY.REP_DIRETORIO_REL
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REDIRREL_PK ON TASY.REP_DIRETORIO_REL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.rep_diretorio_rel_insert
before insert ON TASY.REP_DIRETORIO_REL for each row
declare
ie_classif_WATE_w varchar2(1);

begin
select  decode(count(cd_classif_relat),1,'S','N')
into	ie_classif_WATE_w
from 	relatorio
where   nr_sequencia = :new.nr_seq_relatorio
and 	cd_classif_relat = 'WATE';

if (ie_classif_WATE_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(obter_texto_dic_objeto(691300, wheb_usuario_pck.get_nr_seq_idioma, null));
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.rep_diretorio_rel_update
before update ON TASY.REP_DIRETORIO_REL for each row
declare
ie_classif_WATE_w varchar2(1);

begin
select  decode(count(cd_classif_relat),1,'S','N')
into	ie_classif_WATE_w
from 	relatorio
where   nr_sequencia = :new.nr_seq_relatorio
and 	cd_classif_relat = 'WATE';

if (ie_classif_WATE_w = 'S') then
	wheb_mensagem_pck.exibir_mensagem_abort(obter_texto_dic_objeto(691300, wheb_usuario_pck.get_nr_seq_idioma, null));
end if;

end;
/


ALTER TABLE TASY.REP_DIRETORIO_REL ADD (
  CONSTRAINT REDIRREL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.REP_DIRETORIO_REL TO NIVEL_1;

