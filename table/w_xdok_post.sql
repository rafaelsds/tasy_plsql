ALTER TABLE TASY.W_XDOK_POST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_XDOK_POST CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_XDOK_POST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_IMPORT        NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  POSTLEITZAHL         VARCHAR2(4 BYTE),
  BEZEICHNUNG          VARCHAR2(30 BYTE),
  BUNDESLAND           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WXPOST_I1 ON TASY.W_XDOK_POST
(POSTLEITZAHL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WXPOST_PK ON TASY.W_XDOK_POST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WXPOST_WXIMPT_FK_I ON TASY.W_XDOK_POST
(NR_SEQ_IMPORT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_XDOK_POST ADD (
  CONSTRAINT WXPOST_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

