ALTER TABLE TASY.SIP_COPARTIC_CONTA_CONTAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIP_COPARTIC_CONTA_CONTAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIP_COPARTIC_CONTA_CONTAB
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_CONTA_CONTABIL      VARCHAR2(20 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  NR_SEQ_CONTA_COPARTIC  NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SIPCCCB_CONCONT_FK_I ON TASY.SIP_COPARTIC_CONTA_CONTAB
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPCCCB_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIPCCCB_ESTABEL_FK_I ON TASY.SIP_COPARTIC_CONTA_CONTAB
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPCCCB_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SIPCCCB_PK ON TASY.SIP_COPARTIC_CONTA_CONTAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPCCCB_PK
  MONITORING USAGE;


CREATE INDEX TASY.SIPCCCB_SIPCOCP_FK_I ON TASY.SIP_COPARTIC_CONTA_CONTAB
(NR_SEQ_CONTA_COPARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPCCCB_SIPCOCP_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SIP_COPARTIC_CONTA_CONTAB ADD (
  CONSTRAINT SIPCCCB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SIP_COPARTIC_CONTA_CONTAB ADD (
  CONSTRAINT SIPCCCB_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT SIPCCCB_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SIPCCCB_SIPCOCP_FK 
 FOREIGN KEY (NR_SEQ_CONTA_COPARTIC) 
 REFERENCES TASY.SIP_CONTA_COPARTIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.SIP_COPARTIC_CONTA_CONTAB TO NIVEL_1;

