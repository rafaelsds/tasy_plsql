DROP TABLE TASY.W_RESULT_EXAME_TEMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_RESULT_EXAME_TEMP
(
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  NR_SEQ_EXAME          NUMBER(10),
  NM_EXAME              VARCHAR2(100 BYTE),
  NR_SEQ_MATERIAL       NUMBER(10),
  DS_UNIDADE_MEDIDA     VARCHAR2(15 BYTE),
  IE_ORDEM              NUMBER(15),
  IE_ORDEM_SEG          NUMBER(15),
  NR_APRES_GRUPO        NUMBER(15),
  NR_APRES_EXAME        NUMBER(15),
  IE_FORMATO_RESULTADO  VARCHAR2(3 BYTE),
  DS_METODO             VARCHAR2(80 BYTE),
  NR_ATENDIMENTO        NUMBER(10),
  NR_PRESCRICAO         NUMBER(14),
  NR_SEQ_SUPERIOR       NUMBER(10),
  NR_SEQ_RESULTADO_LAB  NUMBER(10),
  NR_SEQ_RESULT_ITEM    NUMBER(10),
  NR_SEQ_PRESCR         NUMBER(7),
  NR_SEQ_RESULTADO      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WREEXTE_I1 ON TASY.W_RESULT_EXAME_TEMP
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WREEXTE_WREEXTE_FK_I ON TASY.W_RESULT_EXAME_TEMP
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WREEXTE_WREEXTE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.W_RESULT_EXAME_TEMP ADD (
  CONSTRAINT WREEXTE_WREEXTE_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_RESULT_EXAME_TEMP TO NIVEL_1;

