ALTER TABLE TASY.SCHEMATIC_SCAN_CHECK_TYPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SCHEMATIC_SCAN_CHECK_TYPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SCHEMATIC_SCAN_CHECK_TYPE
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_EXP_TITLE          NUMBER(10),
  DS_SUGGESTIONS        VARCHAR2(4000 BYTE),
  DS_KEYWORDS           VARCHAR2(2000 BYTE),
  IE_MANDATORY          VARCHAR2(1 BYTE),
  DT_VALIDITY_INITIAL   DATE,
  DT_VALIDITY_FINAL     DATE,
  IE_AUTO_SOLUTION      VARCHAR2(1 BYTE),
  CD_EXP_DESCRIPTION    NUMBER(10),
  CD_EXP_AUTO_SOLUTION  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SCHESCANT_PK ON TASY.SCHEMATIC_SCAN_CHECK_TYPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SCHEMATIC_SCAN_CHECK_TYPE ADD (
  CONSTRAINT SCHESCANT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.SCHEMATIC_SCAN_CHECK_TYPE TO NIVEL_1;

