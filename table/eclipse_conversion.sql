ALTER TABLE TASY.ECLIPSE_CONVERSION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ECLIPSE_CONVERSION CASCADE CONSTRAINTS;

CREATE TABLE TASY.ECLIPSE_CONVERSION
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_TASY_VALUE        VARCHAR2(50 BYTE),
  NR_SEQ_VALUE         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_DEFAULT_VALUE     VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ECCONVE_ECVALUE_FK_I ON TASY.ECLIPSE_CONVERSION
(NR_SEQ_VALUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ECCONVE_PK ON TASY.ECLIPSE_CONVERSION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ECLIPSE_CONVERSION_UPDATE
before insert or update ON TASY.ECLIPSE_CONVERSION for each row
declare

nm_atribute_w	eclipse_attribute.nm_eclipse_field%type;
ds_value_w		eclipse_value.ds_value%type;

pragma autonomous_transaction;

begin

if (:new.ds_tasy_value is not null and :new.ds_default_value is not null) then

	Wheb_mensagem_pck.exibir_mensagem_abort(1112245);

end if;

if (:new.ds_tasy_value is not null or :new.ds_default_value is not null) then

	select	max(c.nm_eclipse_field),
			max(b.ds_value)
	into	nm_atribute_w,
			ds_value_w
	from	eclipse_conversion a,
			eclipse_value b,
			eclipse_attribute c
	where	c.nr_Sequencia = (	select 	max(c.nr_sequencia)
								from 	eclipse_conversion a,
										eclipse_value b,
										eclipse_attribute c
								where  	c.nr_sequencia = b.nr_seq_attribute
								and		b.nr_sequencia = :new.nr_seq_value
								and		(a.ds_tasy_value = :new.ds_tasy_value
								or		a.ds_default_value	= :new.ds_tasy_value
								or    	a.ds_tasy_value = :new.ds_default_value
								or		a.ds_default_value = :new.ds_default_value))
	and		c.nr_sequencia = b.nr_seq_attribute
	and		b.nr_sequencia =  a.nr_seq_value
	and		(a.ds_tasy_value = :new.ds_tasy_value
	or		a.ds_default_value	= :new.ds_tasy_value
	or		a.ds_tasy_value = :new.ds_default_value
	or		a.ds_default_value = :new.ds_default_value);

	if (nm_atribute_w is not null) then
		Wheb_mensagem_pck.exibir_mensagem_abort(1112160, 'NM_ATRIBUTO='||nm_atribute_w || ' (Eclipse Value: ' || ds_value_w || ')');
	end if;

end if;

end;
/


ALTER TABLE TASY.ECLIPSE_CONVERSION ADD (
  CONSTRAINT ECCONVE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ECLIPSE_CONVERSION ADD (
  CONSTRAINT ECCONVE_ECVALUE_FK 
 FOREIGN KEY (NR_SEQ_VALUE) 
 REFERENCES TASY.ECLIPSE_VALUE (NR_SEQUENCIA));

GRANT SELECT ON TASY.ECLIPSE_CONVERSION TO NIVEL_1;

