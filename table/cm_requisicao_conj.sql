ALTER TABLE TASY.CM_REQUISICAO_CONJ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_REQUISICAO_CONJ CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_REQUISICAO_CONJ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ITEM_REQ      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATENDIMENTO       DATE                     NOT NULL,
  NR_SEQ_CONJ_REAL     NUMBER(10)               NOT NULL,
  DT_RETORNO           DATE,
  NM_USUARIO_RETORNO   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMRECONJ_CMCOCON_FK_I ON TASY.CM_REQUISICAO_CONJ
(NR_SEQ_CONJ_REAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMRECONJ_CMCOCON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CMRECONJ_CMREIT_FK_I ON TASY.CM_REQUISICAO_CONJ
(NR_SEQ_ITEM_REQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMRECONJ_CMREIT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMRECONJ_PK ON TASY.CM_REQUISICAO_CONJ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMRECONJ_PK
  MONITORING USAGE;


CREATE INDEX TASY.CMRECONJ_USUARIO_FK_I ON TASY.CM_REQUISICAO_CONJ
(NM_USUARIO_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMRECONJ_USUARIO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cm_requisicao_conj_insert
after insert ON TASY.CM_REQUISICAO_CONJ for each row
declare

ie_vincular_cirurgia_w	varchar2(1) := 'N';
nr_seq_requisicao_w	cm_requisicao.nr_sequencia%type;
nr_cirurgia_w		cirurgia.nr_cirurgia%type;

begin

ie_vincular_cirurgia_w := obter_valor_param_usuario(410, 60, obter_perfil_ativo, :new.nm_usuario, wheb_usuario_pck.get_cd_estabelecimento);

if	(ie_vincular_cirurgia_w = 'S') then

	select	max(nr_seq_requisicao)
	into	nr_seq_requisicao_w
	from	cm_requisicao_item
	where	nr_sequencia = :new.nr_seq_item_req;

	select	max(nr_cirurgia)
	into	nr_cirurgia_w
	from	cm_requisicao
	where	nr_sequencia = nr_seq_requisicao_w;

	update	cm_conjunto_cont
	set	nr_cirurgia = nr_cirurgia_w
	where	nr_sequencia = :new.nr_seq_conj_real;

end if;

end;
/


ALTER TABLE TASY.CM_REQUISICAO_CONJ ADD (
  CONSTRAINT CMRECONJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_REQUISICAO_CONJ ADD (
  CONSTRAINT CMRECONJ_CMREIT_FK 
 FOREIGN KEY (NR_SEQ_ITEM_REQ) 
 REFERENCES TASY.CM_REQUISICAO_ITEM (NR_SEQUENCIA),
  CONSTRAINT CMRECONJ_CMCOCON_FK 
 FOREIGN KEY (NR_SEQ_CONJ_REAL) 
 REFERENCES TASY.CM_CONJUNTO_CONT (NR_SEQUENCIA),
  CONSTRAINT CMRECONJ_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_RETORNO) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.CM_REQUISICAO_CONJ TO NIVEL_1;

