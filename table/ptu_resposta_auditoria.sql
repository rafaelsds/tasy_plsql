ALTER TABLE TASY.PTU_RESPOSTA_AUDITORIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_RESPOSTA_AUDITORIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_RESPOSTA_AUDITORIA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_TRANSACAO            VARCHAR2(5 BYTE)      NOT NULL,
  IE_TIPO_CLIENTE         VARCHAR2(15 BYTE)     NOT NULL,
  CD_UNIMED_EXECUTORA     NUMBER(4)             NOT NULL,
  CD_UNIMED_BENEFICIARIO  NUMBER(4)             NOT NULL,
  NR_SEQ_EXECUCAO         NUMBER(10)            NOT NULL,
  NR_SEQ_ORIGEM           NUMBER(10)            NOT NULL,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE)   NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_VALIDADE             DATE,
  NR_SEQ_REQUISICAO       NUMBER(10),
  NR_SEQ_GUIA             NUMBER(10),
  DS_ARQUIVO_PEDIDO       VARCHAR2(255 BYTE),
  IE_TIPO_AUTORIZACAO     VARCHAR2(2 BYTE),
  IE_ENVIADO              VARCHAR2(1 BYTE),
  NR_VERSAO               VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PTURESA_I1 ON TASY.PTU_RESPOSTA_AUDITORIA
(NR_SEQ_EXECUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PTURESA_PK ON TASY.PTU_RESPOSTA_AUDITORIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTURESA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PTURESA_PLSGUIA_FK_I ON TASY.PTU_RESPOSTA_AUDITORIA
(NR_SEQ_GUIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTURESA_PLSGUIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTURESA_PLSREQU_FK_I ON TASY.PTU_RESPOSTA_AUDITORIA
(NR_SEQ_REQUISICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTURESA_PLSREQU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PTU_RESPOSTA_AUDITORIA ADD (
  CONSTRAINT PTURESA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_RESPOSTA_AUDITORIA ADD (
  CONSTRAINT PTURESA_PLSREQU_FK 
 FOREIGN KEY (NR_SEQ_REQUISICAO) 
 REFERENCES TASY.PLS_REQUISICAO (NR_SEQUENCIA),
  CONSTRAINT PTURESA_PLSGUIA_FK 
 FOREIGN KEY (NR_SEQ_GUIA) 
 REFERENCES TASY.PLS_GUIA_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_RESPOSTA_AUDITORIA TO NIVEL_1;

