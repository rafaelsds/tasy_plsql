ALTER TABLE TASY.NOTA_FISCAL_ITEM_TRIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_FISCAL_ITEM_TRIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_FISCAL_ITEM_TRIB
(
  NR_SEQUENCIA          NUMBER(10),
  CD_ESTABELECIMENTO    NUMBER(4)               NOT NULL,
  CD_CGC_EMITENTE       VARCHAR2(14 BYTE),
  CD_SERIE_NF           VARCHAR2(255 BYTE)      NOT NULL,
  NR_SEQUENCIA_NF       NUMBER(10)              NOT NULL,
  NR_ITEM_NF            NUMBER(5)               NOT NULL,
  CD_TRIBUTO            NUMBER(3)               NOT NULL,
  VL_TRIBUTO            NUMBER(13,2)            NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  VL_BASE_CALCULO       NUMBER(13,2),
  TX_TRIBUTO            NUMBER(15,4),
  VL_REDUCAO_BASE       NUMBER(13,2),
  IE_RATEIO             VARCHAR2(1 BYTE)        NOT NULL,
  VL_TRIB_NAO_RETIDO    NUMBER(15,2)            NOT NULL,
  VL_BASE_NAO_RETIDO    NUMBER(15,2)            NOT NULL,
  VL_TRIB_ADIC          NUMBER(15,2)            NOT NULL,
  VL_BASE_ADIC          NUMBER(15,2)            NOT NULL,
  NR_SEQ_SIT_TRIB       NUMBER(10),
  NR_NOTA_FISCAL        VARCHAR2(255 BYTE)      DEFAULT null,
  IE_TRIBUTACAO_CSOSN   VARCHAR2(15 BYTE),
  IE_TRIBUTACAO_CST     VARCHAR2(15 BYTE),
  DT_CONFERENCIA        DATE,
  NM_USUARIO_CONF       VARCHAR2(15 BYTE),
  IE_EXPORTA_XML        VARCHAR2(1 BYTE),
  NR_SEQ_REGRA_TRIB     NUMBER(10),
  VL_RET_SUBCONTRATADO  NUMBER(13,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NFIITTR_NOTFIIT_FK_I ON TASY.NOTA_FISCAL_ITEM_TRIB
(NR_SEQUENCIA, NR_ITEM_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NFIITTR_PK ON TASY.NOTA_FISCAL_ITEM_TRIB
(NR_SEQUENCIA, NR_ITEM_NF, CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NFIITTR_SITTRPS_FK_I ON TASY.NOTA_FISCAL_ITEM_TRIB
(NR_SEQ_SIT_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NFIITTR_SITTRPS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.NFIITTR_TRIBUTO_FK_I ON TASY.NOTA_FISCAL_ITEM_TRIB
(CD_TRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.NFIITTR_TRIBUTO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.NOTA_FISCAL_ITEM_TRIB ADD (
  CONSTRAINT NFIITTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_ITEM_NF, CD_TRIBUTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_FISCAL_ITEM_TRIB ADD (
  CONSTRAINT NFIITTR_NOTFIIT_FK 
 FOREIGN KEY (NR_SEQUENCIA, NR_ITEM_NF) 
 REFERENCES TASY.NOTA_FISCAL_ITEM (NR_SEQUENCIA,NR_ITEM_NF)
    ON DELETE CASCADE,
  CONSTRAINT NFIITTR_TRIBUTO_FK 
 FOREIGN KEY (CD_TRIBUTO) 
 REFERENCES TASY.TRIBUTO (CD_TRIBUTO),
  CONSTRAINT NFIITTR_SITTRPS_FK 
 FOREIGN KEY (NR_SEQ_SIT_TRIB) 
 REFERENCES TASY.SITUACAO_TRIB_PREST_SERV (NR_SEQUENCIA));

GRANT SELECT ON TASY.NOTA_FISCAL_ITEM_TRIB TO NIVEL_1;

