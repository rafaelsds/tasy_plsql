ALTER TABLE TASY.PROJ_CRON_CALENDARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_CRON_CALENDARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_CRON_CALENDARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CRONOGRAMA    NUMBER(10)               NOT NULL,
  IE_DIA_SEMANA        VARCHAR2(1 BYTE)         NOT NULL,
  HR_INICIO            VARCHAR2(5 BYTE)         NOT NULL,
  HR_FINAL             VARCHAR2(5 BYTE)         NOT NULL,
  QT_MIN_INTERVALO     NUMBER(3)                NOT NULL,
  NR_SEQ_EQUIPE_PAPEL  NUMBER(10),
  DT_HR_FINAL          DATE,
  DT_HR_INICIO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROJCRONCA_PK ON TASY.PROJ_CRON_CALENDARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJCRONCA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROJCRONCA_PROCRON_FK_I ON TASY.PROJ_CRON_CALENDARIO
(NR_SEQ_CRONOGRAMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJCRONCA_PROCRON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJCRONCA_PROEQPA_FK_I ON TASY.PROJ_CRON_CALENDARIO
(NR_SEQ_EQUIPE_PAPEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.proj_cron_calendario_atual
before insert or update ON TASY.PROJ_CRON_CALENDARIO for each row
declare

begin
begin
	if (:new.hr_final is not null) and ((:new.hr_final <> :old.hr_final) or (:old.dt_hr_final is null)) then
		:new.dt_hr_final := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_final,'dd/mm/yyyy hh24:mi');
	end if;
	if (:new.hr_inicio is not null) and ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_hr_inicio is null)) then
		:new.dt_hr_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio,'dd/mm/yyyy hh24:mi');
	end if;
	if	(:new.dt_hr_final is not null ) and ((:new.dt_hr_final <> :old.dt_hr_final) or (:old.dt_hr_final is null)) then
		:new.hr_final := to_char(:new.dt_hr_final, 'hh24:mi');
	end if;
	if	(:new.dt_hr_inicio is not null ) and ((:new.dt_hr_inicio <> :old.dt_hr_inicio) or (:old.dt_hr_inicio is null)) then
		:new.hr_inicio := to_char(:new.dt_hr_inicio, 'hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.proj_cron_calendario_after
after insert or update or delete ON TASY.PROJ_CRON_CALENDARIO for each row

/*
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Objective: N/A
	------------------------------------------------------------------------
	Attention points: N/A
	------------------------------------------------------------------------
	References:
		Tables: N/A
		Objects: N/A
	++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/
declare

ie_operacao_w	proj_cron_log_alt.ie_operacao%type;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

<<Final>>

if	(inserting) then
	ie_operacao_w := 'I';
elsif	(updating) then
	ie_operacao_w := 'A';
elsif	(deleting) then
	ie_operacao_w := 'E';
end if;

proj_log_ultimo_recalc(nvl(:new.nr_seq_cronograma, :old.nr_seq_cronograma),
	'C',
	ie_operacao_w,
	nvl(:new.nm_usuario, :old.nm_usuario));

end;
/


ALTER TABLE TASY.PROJ_CRON_CALENDARIO ADD (
  CONSTRAINT PROJCRONCA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_CRON_CALENDARIO ADD (
  CONSTRAINT PROJCRONCA_PROEQPA_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE_PAPEL) 
 REFERENCES TASY.PROJ_EQUIPE_PAPEL (NR_SEQUENCIA),
  CONSTRAINT PROJCRONCA_PROCRON_FK 
 FOREIGN KEY (NR_SEQ_CRONOGRAMA) 
 REFERENCES TASY.PROJ_CRONOGRAMA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_CRON_CALENDARIO TO NIVEL_1;

