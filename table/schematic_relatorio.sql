ALTER TABLE TASY.SCHEMATIC_RELATORIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SCHEMATIC_RELATORIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SCHEMATIC_RELATORIO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_FUNCAO                NUMBER(10),
  NR_SEQ_OBJETO_SCHEMATIC  NUMBER(10),
  NR_SEQ_RELATORIO         NUMBER(10),
  DS_ACTION_NAME           VARCHAR2(255 BYTE),
  CD_CLASSIF_RELAT         VARCHAR2(4 BYTE),
  CD_RELATORIO             NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SCHREL_FUNCAO_FK_I ON TASY.SCHEMATIC_RELATORIO
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCHREL_OBJSCHE_FK_I ON TASY.SCHEMATIC_RELATORIO
(NR_SEQ_OBJETO_SCHEMATIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SCHREL_PK ON TASY.SCHEMATIC_RELATORIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SCHREL_RELATOR_FK_I ON TASY.SCHEMATIC_RELATORIO
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SCHEMATIC_RELATORIO ADD (
  CONSTRAINT SCHREL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SCHEMATIC_RELATORIO ADD (
  CONSTRAINT SCHREL_FUNCAO_FK 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO),
  CONSTRAINT SCHREL_OBJSCHE_FK 
 FOREIGN KEY (NR_SEQ_OBJETO_SCHEMATIC) 
 REFERENCES TASY.OBJETO_SCHEMATIC (NR_SEQUENCIA),
  CONSTRAINT SCHREL_RELATOR_FK 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SCHEMATIC_RELATORIO TO NIVEL_1;

