ALTER TABLE TASY.TEMPLATE_CONTENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TEMPLATE_CONTENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.TEMPLATE_CONTENT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  CONTENT_TYPE          VARCHAR2(255 BYTE),
  LINE_NUM              NUMBER(10),
  COLUMN_NUM            NUMBER(10),
  DS_MACRO              VARCHAR2(255 BYTE),
  IE_MACRO_HIDE         VARCHAR2(1 BYTE),
  IE_SUPER_SCRIPT       VARCHAR2(1 BYTE),
  IE_PRINT_ONLY         VARCHAR2(1 BYTE),
  IE_RESULT_INPUT_ONLY  VARCHAR2(1 BYTE),
  ORDER_SEQ             NUMBER(10),
  NR_SEQ_TEMPLATE       NUMBER(10),
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_TITLE          NUMBER(10),
  NR_SEQ_SUB_TITLE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TEMPCONT_PK ON TASY.TEMPLATE_CONTENT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TEMPCONT_TEMPLATE_FK_I ON TASY.TEMPLATE_CONTENT
(NR_SEQ_TEMPLATE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TEMPCONT_TEMPSUB_FK_I ON TASY.TEMPLATE_CONTENT
(NR_SEQ_SUB_TITLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TEMPCONT_TEMPTITLE_FK_I ON TASY.TEMPLATE_CONTENT
(NR_SEQ_TITLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TEMPLATE_CONTENT ADD (
  CONSTRAINT TEMPCONT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TEMPLATE_CONTENT ADD (
  CONSTRAINT TEMPCONT_TEMPLATE_FK 
 FOREIGN KEY (NR_SEQ_TEMPLATE) 
 REFERENCES TASY.TEMPLATE (NR_SEQUENCIA),
  CONSTRAINT TEMPCONT_TEMPSUB_FK 
 FOREIGN KEY (NR_SEQ_SUB_TITLE) 
 REFERENCES TASY.TEMPLATE_SUB_TITLE (NR_SEQUENCIA),
  CONSTRAINT TEMPCONT_TEMPTITLE_FK 
 FOREIGN KEY (NR_SEQ_TITLE) 
 REFERENCES TASY.TEMPLATE_TITLE (NR_SEQUENCIA));

GRANT SELECT ON TASY.TEMPLATE_CONTENT TO NIVEL_1;

