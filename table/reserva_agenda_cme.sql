ALTER TABLE TASY.RESERVA_AGENDA_CME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RESERVA_AGENDA_CME CASCADE CONSTRAINTS;

CREATE TABLE TASY.RESERVA_AGENDA_CME
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA           NUMBER(10)            NOT NULL,
  CD_ESTAB_DESTINO        NUMBER(4)             NOT NULL,
  NR_SEQ_CONJUNTO_AGENDA  NUMBER(10),
  NR_SEQ_CONJUNTO         NUMBER(10),
  NR_SEQ_STATUS           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REAGCME_AGEPACI_FK_I ON TASY.RESERVA_AGENDA_CME
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REAGCME_AGEPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REAGCME_CMCONJU_FK_I ON TASY.RESERVA_AGENDA_CME
(NR_SEQ_CONJUNTO_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REAGCME_CMCONJU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REAGCME_CMCONJU_FK2_I ON TASY.RESERVA_AGENDA_CME
(NR_SEQ_CONJUNTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REAGCME_CMCONJU_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REAGCME_ESTABEL_FK_I ON TASY.RESERVA_AGENDA_CME
(CD_ESTAB_DESTINO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REAGCME_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.REAGCME_PK ON TASY.RESERVA_AGENDA_CME
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REAGCME_PK
  MONITORING USAGE;


CREATE INDEX TASY.REAGCME_STREAGE_FK_I ON TASY.RESERVA_AGENDA_CME
(NR_SEQ_STATUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REAGCME_STREAGE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.RESERVA_AGENDA_CME ADD (
  CONSTRAINT REAGCME_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RESERVA_AGENDA_CME ADD (
  CONSTRAINT REAGCME_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_DESTINO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REAGCME_CMCONJU_FK2 
 FOREIGN KEY (NR_SEQ_CONJUNTO) 
 REFERENCES TASY.CM_CONJUNTO (NR_SEQUENCIA),
  CONSTRAINT REAGCME_CMCONJU_FK 
 FOREIGN KEY (NR_SEQ_CONJUNTO_AGENDA) 
 REFERENCES TASY.CM_CONJUNTO (NR_SEQUENCIA),
  CONSTRAINT REAGCME_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT REAGCME_STREAGE_FK 
 FOREIGN KEY (NR_SEQ_STATUS) 
 REFERENCES TASY.STATUS_RES_AGENDA_RECURSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RESERVA_AGENDA_CME TO NIVEL_1;

