ALTER TABLE TASY.REGRA_CONTROLE_STATUS_CRON
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_CONTROLE_STATUS_CRON CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_CONTROLE_STATUS_CRON
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_DIAS              NUMBER(10)               NOT NULL,
  IE_STATUS_ATUAL      VARCHAR2(1 BYTE),
  IE_STATUS_NOVO       VARCHAR2(1 BYTE)         NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.RECOSH_PK ON TASY.REGRA_CONTROLE_STATUS_CRON
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_CONTROLE_STATUS_CRON ADD (
  CONSTRAINT RECOSH_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_CONTROLE_STATUS_CRON TO NIVEL_1;

