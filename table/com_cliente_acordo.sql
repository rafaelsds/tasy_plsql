ALTER TABLE TASY.COM_CLIENTE_ACORDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_CLIENTE_ACORDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_CLIENTE_ACORDO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_CLIENTE        NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_TIPO_ACORDO        VARCHAR2(3 BYTE)        NOT NULL,
  NR_SEQ_APRES          NUMBER(5)               NOT NULL,
  DS_ACORDO             VARCHAR2(4000 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_GESTAO_PROJETOS    VARCHAR2(1 BYTE),
  DT_LIBERACAO          DATE,
  NM_USUARIO_LIBERACAO  VARCHAR2(15 BYTE),
  NR_SEQ_PROJ           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMCLAC_COMCLIE_FK_I ON TASY.COM_CLIENTE_ACORDO
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLAC_COMCLIE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COMCLAC_PK ON TASY.COM_CLIENTE_ACORDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCLAC_PK
  MONITORING USAGE;


CREATE INDEX TASY.COMCLAC_PROPROJ_FK_I ON TASY.COM_CLIENTE_ACORDO
(NR_SEQ_PROJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.COM_CLIENTE_ACORDO ADD (
  CONSTRAINT COMCLAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_CLIENTE_ACORDO ADD (
  CONSTRAINT COMCLAC_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT COMCLAC_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA));

GRANT SELECT ON TASY.COM_CLIENTE_ACORDO TO NIVEL_1;

