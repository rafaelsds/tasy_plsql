ALTER TABLE TASY.AGEINT_HIST_REGRAS_DESAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGEINT_HIST_REGRAS_DESAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEINT_HIST_REGRAS_DESAB
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_AGEINT           NUMBER(10)            NOT NULL,
  IE_TEMPO_ENTRE_EXAME    VARCHAR2(1 BYTE)      NOT NULL,
  IE_TEMPO_ENTRE_SETOR    VARCHAR2(1 BYTE)      NOT NULL,
  IE_BLOQ_QUESTIONARIO    VARCHAR2(1 BYTE)      NOT NULL,
  IE_PRIOR_ENTRE_EXAME    VARCHAR2(1 BYTE)      NOT NULL,
  IE_REGRAS_EXAMES        VARCHAR2(1 BYTE)      NOT NULL,
  IE_GRUPO_PROCEDIMENTO   VARCHAR2(1 BYTE)      NOT NULL,
  CD_PESSOA_FISICA_AUTOR  VARCHAR2(10 BYTE)     NOT NULL,
  DS_OBSERVACAO           VARCHAR2(2000 BYTE)   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AGHIREDE_PK ON TASY.AGEINT_HIST_REGRAS_DESAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGEINT_HIST_REGRAS_DESAB ADD (
  CONSTRAINT AGHIREDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGEINT_HIST_REGRAS_DESAB TO NIVEL_1;

