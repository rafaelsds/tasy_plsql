ALTER TABLE TASY.BIOB_JUSTIF_COLETA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BIOB_JUSTIF_COLETA CASCADE CONSTRAINTS;

CREATE TABLE TASY.BIOB_JUSTIF_COLETA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_DOADOR        NUMBER(10)               NOT NULL,
  CD_RESP_COLETA       VARCHAR2(10 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  NR_SEQ_MOTIVO        NUMBER(10),
  IE_TIPO_MATERIAL     VARCHAR2(1 BYTE)         NOT NULL,
  DT_REGISTRO          DATE                     NOT NULL,
  NR_CIRURGIA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BIOJUCO_BIODOAD_FK_I ON TASY.BIOB_JUSTIF_COLETA
(NR_SEQ_DOADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BIOJUCO_BIOMOCO_FK_I ON TASY.BIOB_JUSTIF_COLETA
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BIOJUCO_PESFISI_FK_I ON TASY.BIOB_JUSTIF_COLETA
(CD_RESP_COLETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BIOJUCO_PK ON TASY.BIOB_JUSTIF_COLETA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.BIOB_JUSTIF_COLETA ADD (
  CONSTRAINT BIOJUCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.BIOB_JUSTIF_COLETA ADD (
  CONSTRAINT BIOJUCO_BIODOAD_FK 
 FOREIGN KEY (NR_SEQ_DOADOR) 
 REFERENCES TASY.BIOB_DOADOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT BIOJUCO_BIOMOCO_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.BIOB_MOTIVO_COLETA (NR_SEQUENCIA),
  CONSTRAINT BIOJUCO_PESFISI_FK 
 FOREIGN KEY (CD_RESP_COLETA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.BIOB_JUSTIF_COLETA TO NIVEL_1;

