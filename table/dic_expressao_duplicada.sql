ALTER TABLE TASY.DIC_EXPRESSAO_DUPLICADA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIC_EXPRESSAO_DUPLICADA CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIC_EXPRESSAO_DUPLICADA
(
  NR_SEQ_GRUPO         NUMBER(10),
  CD_EXPRESSAO         NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_USUARIO_AJUSTE    VARCHAR2(15 BYTE),
  DT_AJUSTE            DATE,
  NR_SEQUENCIA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.DICEXPD_PK ON TASY.DIC_EXPRESSAO_DUPLICADA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DICEXPD_USUARIO_FK_I ON TASY.DIC_EXPRESSAO_DUPLICADA
(NM_USUARIO_AJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DIC_EXPRESSAO_DUPLICADA ADD (
  CONSTRAINT DICEXPD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIC_EXPRESSAO_DUPLICADA ADD (
  CONSTRAINT DICEXPD_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_AJUSTE) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.DIC_EXPRESSAO_DUPLICADA TO NIVEL_1;

