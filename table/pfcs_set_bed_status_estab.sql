ALTER TABLE TASY.PFCS_SET_BED_STATUS_ESTAB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PFCS_SET_BED_STATUS_ESTAB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PFCS_SET_BED_STATUS_ESTAB
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_SETTING           VARCHAR2(244 BYTE)       NOT NULL,
  NR_SEQ_AUX           NUMBER(1),
  CD_ESTABLISHMENT     NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFCSBEDEST_ESTABEL_FK_I ON TASY.PFCS_SET_BED_STATUS_ESTAB
(CD_ESTABLISHMENT)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFCSBEDEST_PK ON TASY.PFCS_SET_BED_STATUS_ESTAB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFCSBEDEST_UK ON TASY.PFCS_SET_BED_STATUS_ESTAB
(CD_ESTABLISHMENT, NR_SEQ_AUX)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PFCS_AFTER_INS_BED_STATUS
AFTER INSERT ON TASY.PFCS_SET_BED_STATUS_ESTAB FOR EACH ROW
DECLARE
qt_reg_w number(1);

BEGIN
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
        goto Final;
    end if;

    insert into pfcs_set_bed_status(
        nr_sequencia,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        dt_atualizacao,
        nm_usuario,
        cd_status_bed,
        ie_total_beds,
        ie_census,
        nr_seq_bed_status_estab
    ) values(
        pfcs_set_bed_status_seq.nextval,
        SYSDATE,
        :new.nm_usuario_nrec,
        SYSDATE,
        :new.nm_usuario,
        'U',
        'Y',
        'A',
        :new.nr_sequencia);

    insert into pfcs_set_bed_status(
        nr_sequencia,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        dt_atualizacao,
        nm_usuario,
        cd_status_bed,
        ie_total_beds,
        ie_census,
        nr_seq_bed_status_estab
    ) values(
        pfcs_set_bed_status_seq.nextval,
        SYSDATE,
        :new.nm_usuario_nrec,
        SYSDATE,
        :new.nm_usuario,
        'I',
        'Y',
        'A',
        :new.nr_sequencia);

    insert into pfcs_set_bed_status(
        nr_sequencia,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        dt_atualizacao,
        nm_usuario,
        cd_status_bed,
        ie_total_beds,
        ie_census,
        nr_seq_bed_status_estab
    ) values(
        pfcs_set_bed_status_seq.nextval,
        SYSDATE,
        :new.nm_usuario_nrec,
        SYSDATE,
        :new.nm_usuario,
        'C',
        'N',
        'B',
        :new.nr_sequencia);

    insert into pfcs_set_bed_status(
        nr_sequencia,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        dt_atualizacao,
        nm_usuario,
        cd_status_bed,
        ie_total_beds,
        ie_census,
        nr_seq_bed_status_estab
    ) values(
        pfcs_set_bed_status_seq.nextval,
        SYSDATE,
        :new.nm_usuario_nrec,
        SYSDATE,
        :new.nm_usuario,
        'K',
        'N',
        'B',
        :new.nr_sequencia);

    insert into pfcs_set_bed_status(
        nr_sequencia,
        dt_atualizacao_nrec,
        nm_usuario_nrec,
        dt_atualizacao,
        nm_usuario,
        cd_status_bed,
        ie_total_beds,
        ie_census,
        nr_seq_bed_status_estab
    ) values(
        pfcs_set_bed_status_seq.nextval,
        SYSDATE,
        :new.nm_usuario_nrec,
        SYSDATE,
        :new.nm_usuario,
        'H',
        'Y',
        'A',
        :new.nr_sequencia);

<<Final>>
qt_reg_w := 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.PFCS_BEF_INS_BED_STATUS
BEFORE INSERT OR UPDATE ON TASY.PFCS_SET_BED_STATUS_ESTAB FOR EACH ROW
DECLARE
qt_reg_w number(2);
pragma autonomous_transaction;

BEGIN
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
        goto Final;
    end if;

    if ((nvl(:old.cd_establishment, 99999) <> nvl(:new.cd_establishment, 99999)) or (inserting)) then
        select count(1)
        into qt_reg_w
        from pfcs_set_bed_status_estab e
        where ((e.cd_establishment = :new.cd_establishment) or ((e.cd_establishment is null) and (:new.cd_establishment is null)));

        if (qt_reg_w >= 1) then
            Wheb_mensagem_pck.exibir_mensagem_abort(1166147);
        end if;
    end if;

<<Final>>
qt_reg_w := 0;
END;
/


ALTER TABLE TASY.PFCS_SET_BED_STATUS_ESTAB ADD (
  CONSTRAINT PFCSBEDEST_PK
 PRIMARY KEY
 (NR_SEQUENCIA),
  CONSTRAINT PFCSBEDEST_UK
 UNIQUE (CD_ESTABLISHMENT, NR_SEQ_AUX));

ALTER TABLE TASY.PFCS_SET_BED_STATUS_ESTAB ADD (
  CONSTRAINT PFCSBEDEST_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABLISHMENT) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO)
    ON DELETE CASCADE);

