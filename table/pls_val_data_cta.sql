ALTER TABLE TASY.PLS_VAL_DATA_CTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_VAL_DATA_CTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_VAL_DATA_CTA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_PERIODO_ANTERIOR   VARCHAR2(1 BYTE)        NOT NULL,
  QT_DIAS               NUMBER(10),
  IE_DT_VALIDADE_SENHA  VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCTDV_PK ON TASY.PLS_VAL_DATA_CTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_VAL_DATA_CTA_tp  after update ON TASY.PLS_VAL_DATA_CTA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_VAL_DATA_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERIODO_ANTERIOR,1,4000),substr(:new.IE_PERIODO_ANTERIOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERIODO_ANTERIOR',ie_log_w,ds_w,'PLS_VAL_DATA_CTA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS,1,4000),substr(:new.QT_DIAS,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS',ie_log_w,ds_w,'PLS_VAL_DATA_CTA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_VAL_DATA_CTA ADD (
  CONSTRAINT PLSCTDV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PLS_VAL_DATA_CTA TO NIVEL_1;

