ALTER TABLE TASY.CIH_LOCAL_INFECCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIH_LOCAL_INFECCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIH_LOCAL_INFECCAO
(
  NR_FICHA_OCORRENCIA          NUMBER(10)       NOT NULL,
  CD_TOPOGRAFIA                NUMBER(10),
  CD_CLINICA                   NUMBER(10),
  CD_CASO_INFECCAO             NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  NR_SEQ_CLASSIF_TOP           NUMBER(10),
  DT_INFECCAO                  DATE,
  NR_SEQUENCIA                 NUMBER(10),
  DS_OBSERVACAO                VARCHAR2(255 BYTE),
  DT_ORIGEM_INFECCAO           DATE,
  NR_SEQ_SITIO_PRINC           NUMBER(10),
  NR_SEQ_SITIO_ESPEC           NUMBER(10),
  IE_INFECCAO_PREVENIVEL       VARCHAR2(1 BYTE),
  CD_TIPO_EVOLUCAO             NUMBER(10),
  CD_SETOR_ORIGEM              NUMBER(5),
  CD_SETOR_ATENDIMENTO         NUMBER(5),
  DT_OCORRENCIA                DATE,
  IE_NEOTROPENIA_FEBRIL        VARCHAR2(200 BYTE),
  IE_DOENCA_INFECTOCONTAGIOSA  VARCHAR2(200 BYTE),
  IE_INFECCAO_TUMOR            VARCHAR2(1 BYTE),
  NR_SEQ_IDENTIFICACAO         NUMBER(10),
  IE_INFECCAO_SECUNDARIA       VARCHAR2(2 BYTE),
  IE_CONTABILIZA_NISS          VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA_NISS        VARCHAR2(255 BYTE),
  NR_SEQ_CLASSIF_EV            NUMBER(10),
  IE_SEPSE                     VARCHAR2(1 BYTE),
  IE_ADENO                     VARCHAR2(1 BYTE),
  IE_DIARREIA                  VARCHAR2(1 BYTE),
  IE_INJURIA_MUCOSA            VARCHAR2(1 BYTE),
  IE_POS_TRANSPLANTE           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIHLOIN_CIHCAIN_FK_I ON TASY.CIH_LOCAL_INFECCAO
(CD_CASO_INFECCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIHLOIN_CIHCLASSEV_FK_I ON TASY.CIH_LOCAL_INFECCAO
(NR_SEQ_CLASSIF_EV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHLOIN_CIHCLASSEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIHLOIN_CIHCLIN_FK_I ON TASY.CIH_LOCAL_INFECCAO
(CD_CLINICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHLOIN_CIHCLIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIHLOIN_CIHFIOC_FK_I ON TASY.CIH_LOCAL_INFECCAO
(NR_FICHA_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIHLOIN_CIHIDCULT_FK_I ON TASY.CIH_LOCAL_INFECCAO
(NR_SEQ_IDENTIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHLOIN_CIHIDCULT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIHLOIN_CIHLOIN_FK_I ON TASY.CIH_LOCAL_INFECCAO
(CD_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIHLOIN_NISSIES_FK_I ON TASY.CIH_LOCAL_INFECCAO
(NR_SEQ_SITIO_ESPEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHLOIN_NISSIES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIHLOIN_NISSIPR_FK_I ON TASY.CIH_LOCAL_INFECCAO
(NR_SEQ_SITIO_PRINC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIHLOIN_NISSIPR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CIHLOIN_PK ON TASY.CIH_LOCAL_INFECCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.cih_local_infeccao_befins
before insert ON TASY.CIH_LOCAL_INFECCAO for each row
declare

cd_caso_infeccao_w		number(10);
nr_seq_evento_infeccao_w number(10);
nr_atendimento_w		number(10);
cd_estabelecimento_w	number(10);

cursor c01 is
select	cd_caso_infeccao,
		nr_seq_evento_infeccao
from	cih_parametros;

begin

OPEN C01;
LOOP
FETCH C01 into
	cd_caso_infeccao_w,
	nr_seq_evento_infeccao_w;
exit when c01%notfound;

if 	(cd_caso_infeccao_w is not null) then

	if	(:new.cd_caso_infeccao = cd_caso_infeccao_w) then

		select	a.nr_atendimento,
				a.cd_estabelecimento
		into	nr_atendimento_w,
				cd_estabelecimento_w
		from	atendimento_paciente a,
				cih_ficha_ocorrencia c
		where	c.nr_atendimento	= a.nr_atendimento
		and		c.nr_ficha_ocorrencia	= :new.nr_ficha_ocorrencia;

		gerar_evento_infeccao_trig(:new.nm_usuario, nr_atendimento_w, cd_estabelecimento_w, :new.cd_topografia, :new.cd_caso_infeccao, :new.cd_clinica, nr_seq_evento_infeccao_w);

	end if;

end if;
END LOOP;
CLOSE C01;

end;
/


CREATE OR REPLACE TRIGGER TASY.cih_local_infeccao_befupd
before update ON TASY.CIH_LOCAL_INFECCAO for each row
declare

cd_caso_infeccao_w		number(10);
nr_seq_evento_infeccao_w number(10);
nr_atendimento_w		number(10);
cd_estabelecimento_w	number(10);
ie_evento_topografia_w		varchar2(1);

cursor c01 is
select	cd_caso_infeccao,
		nr_seq_evento_infeccao
from	cih_parametros;

begin

Obter_Param_Usuario(1302,47,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_evento_topografia_w);

OPEN C01;
LOOP
FETCH C01 into
	cd_caso_infeccao_w,
	nr_seq_evento_infeccao_w;
exit when c01%notfound;

if 	(cd_caso_infeccao_w is not null) then

	if	(:new.cd_caso_infeccao = cd_caso_infeccao_w) and
		((:old.cd_caso_infeccao <> cd_caso_infeccao_w) or
		((:old.cd_topografia <> :new.cd_topografia) and
		(ie_evento_topografia_w = 'S'))) then

		select	a.nr_atendimento,
			a.cd_estabelecimento
		into	nr_atendimento_w,
			cd_estabelecimento_w
		from	atendimento_paciente a,
			cih_ficha_ocorrencia c
		where	c.nr_atendimento	= a.nr_atendimento
		and	c.nr_ficha_ocorrencia	= :new.nr_ficha_ocorrencia;

		gerar_evento_infeccao_trig(:new.nm_usuario, nr_atendimento_w, cd_estabelecimento_w, :new.cd_topografia, :new.cd_caso_infeccao, :new.cd_clinica, nr_seq_evento_infeccao_w);

	end if;

end if;
END LOOP;
CLOSE C01;

end;
/


ALTER TABLE TASY.CIH_LOCAL_INFECCAO ADD (
  CONSTRAINT CIHLOIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIH_LOCAL_INFECCAO ADD (
  CONSTRAINT CIHLOIN_CIHCLASSEV_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_EV) 
 REFERENCES TASY.CIH_CLASSIF_EVENTOS (NR_SEQUENCIA),
  CONSTRAINT CIHLOIN_CIHCAIN_FK 
 FOREIGN KEY (CD_CASO_INFECCAO) 
 REFERENCES TASY.CIH_CASO_INFECCAO (CD_CASO_INFECCAO),
  CONSTRAINT CIHLOIN_CIHCLIN_FK 
 FOREIGN KEY (CD_CLINICA) 
 REFERENCES TASY.CIH_CLINICA (CD_CLINICA),
  CONSTRAINT CIHLOIN_CIHFIOC_FK 
 FOREIGN KEY (NR_FICHA_OCORRENCIA) 
 REFERENCES TASY.CIH_FICHA_OCORRENCIA (NR_FICHA_OCORRENCIA),
  CONSTRAINT CIHLOIN_CIHLOIN_FK 
 FOREIGN KEY (CD_TOPOGRAFIA) 
 REFERENCES TASY.CIH_TOPOGRAFIA (CD_TOPOGRAFIA),
  CONSTRAINT CIHLOIN_NISSIPR_FK 
 FOREIGN KEY (NR_SEQ_SITIO_PRINC) 
 REFERENCES TASY.NISS_SITIO_PRINCIPAL (NR_SEQUENCIA),
  CONSTRAINT CIHLOIN_NISSIES_FK 
 FOREIGN KEY (NR_SEQ_SITIO_ESPEC) 
 REFERENCES TASY.NISS_SITIO_ESPECIFICO (NR_SEQUENCIA),
  CONSTRAINT CIHLOIN_CIHIDCULT_FK 
 FOREIGN KEY (NR_SEQ_IDENTIFICACAO) 
 REFERENCES TASY.CIH_IDENTIFICACAO_CULTURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CIH_LOCAL_INFECCAO TO NIVEL_1;

