ALTER TABLE TASY.PLS_MOV_BENEF_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MOV_BENEF_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MOV_BENEF_REGRA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DS_REGRA                  VARCHAR2(255 BYTE)  NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_TIPO_COMPARTILHAMENTO  NUMBER(2),
  IE_TIPO_REPASSE           VARCHAR2(1 BYTE),
  NR_SEQ_CONGENERE          NUMBER(10),
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  CD_INTERFACE              NUMBER(5),
  NM_ARQUIVO                VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMBRG_ESTABEL_FK_I ON TASY.PLS_MOV_BENEF_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMBRG_INTERFA_FK_I ON TASY.PLS_MOV_BENEF_REGRA
(CD_INTERFACE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMBRG_PK ON TASY.PLS_MOV_BENEF_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMBRG_PLSCONG_FK_I ON TASY.PLS_MOV_BENEF_REGRA
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_MOV_BENEF_REGRA ADD (
  CONSTRAINT PLSMBRG_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_MOV_BENEF_REGRA ADD (
  CONSTRAINT PLSMBRG_INTERFA_FK 
 FOREIGN KEY (CD_INTERFACE) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT PLSMBRG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSMBRG_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_MOV_BENEF_REGRA TO NIVEL_1;

