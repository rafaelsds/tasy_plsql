ALTER TABLE TASY.EME_REG_DIAG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EME_REG_DIAG CASCADE CONSTRAINTS;

CREATE TABLE TASY.EME_REG_DIAG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REG           NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_CID               VARCHAR2(10 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EMEREDI_CIDDOEN_FK_I ON TASY.EME_REG_DIAG
(CD_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMEREDI_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMEREDI_EMEREGU_FK_I ON TASY.EME_REG_DIAG
(NR_SEQ_REG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMEREDI_EMEREGU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.EMEREDI_PK ON TASY.EME_REG_DIAG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMEREDI_PK
  MONITORING USAGE;


ALTER TABLE TASY.EME_REG_DIAG ADD (
  CONSTRAINT EMEREDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EME_REG_DIAG ADD (
  CONSTRAINT EMEREDI_EMEREGU_FK 
 FOREIGN KEY (NR_SEQ_REG) 
 REFERENCES TASY.EME_REGULACAO (NR_SEQUENCIA),
  CONSTRAINT EMEREDI_CIDDOEN_FK 
 FOREIGN KEY (CD_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID));

GRANT SELECT ON TASY.EME_REG_DIAG TO NIVEL_1;

