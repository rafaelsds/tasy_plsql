ALTER TABLE TASY.ESCALA_FRAMINGHAM_FA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_FRAMINGHAM_FA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_FRAMINGHAM_FA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  QT_IDADE               NUMBER(3)              NOT NULL,
  QT_PA_SISTOLICA        NUMBER(3)              NOT NULL,
  IE_DIABETES            VARCHAR2(1 BYTE)       NOT NULL,
  IE_TABAGISMO           VARCHAR2(1 BYTE)       NOT NULL,
  IE_IAM_ICC             VARCHAR2(1 BYTE)       NOT NULL,
  IE_SOPRO_MITRAL        VARCHAR2(1 BYTE)       NOT NULL,
  IE_HVE_ECG             VARCHAR2(1 BYTE)       NOT NULL,
  QT_SCORE               NUMBER(3),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCFRAF_ATEPACI_FK_I ON TASY.ESCALA_FRAMINGHAM_FA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCFRAF_PESFISI_FK_I ON TASY.ESCALA_FRAMINGHAM_FA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCFRAF_PK ON TASY.ESCALA_FRAMINGHAM_FA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_FRAMINGHAM_FA_atual
before insert or update ON TASY.ESCALA_FRAMINGHAM_FA for each row
begin

:new.QT_SCORE := 0;

if	(:new.qt_idade = 56) Then
	:new.QT_SCORE := 1;
Elsif	(:new.qt_idade = 57) Then
	:new.QT_SCORE := 2;
Elsif	((:new.qt_idade = 58)
Or	 (:new.qt_idade = 59)) Then
	:new.QT_SCORE := 3;
Elsif	(:new.qt_idade = 60) Then
	:new.QT_SCORE := 4;
Elsif	(:new.qt_idade = 61) Then
	:new.QT_SCORE := 5;
Elsif	(:new.qt_idade = 62) Then
	:new.QT_SCORE := 6;
Elsif	(:new.qt_idade = 63) Then
	:new.QT_SCORE := 7;
Elsif	((:new.qt_idade = 64)
Or	 (:new.qt_idade = 65)) Then
	:new.QT_SCORE := 8;
Elsif	(:new.qt_idade = 66) Then
	:new.QT_SCORE := 9;
Elsif	(:new.qt_idade <= 67) Then
	:new.QT_SCORE := 10;
Elsif	(:new.qt_idade <= 68) Then
	:new.QT_SCORE := 11;
Elsif	(:new.qt_idade <= 69) Then
	:new.QT_SCORE := 12;
Elsif	((:new.qt_idade = 70)
Or	 (:new.qt_idade = 71)) Then
	:new.QT_SCORE := 13;
Elsif	(:new.qt_idade = 72) Then
	:new.QT_SCORE := 14;
Elsif	(:new.qt_idade <= 73) Then
	:new.QT_SCORE := 15;
Elsif	(:new.qt_idade <= 74) Then
	:new.QT_SCORE := 16;
Elsif	(:new.qt_idade <= 75) Then
	:new.QT_SCORE := 17;
Elsif	((:new.qt_idade = 76)
Or	 (:new.qt_idade = 77)) Then
	:new.QT_SCORE := 18;
Elsif	(:new.qt_idade = 78) Then
	:new.QT_SCORE := 19;
Elsif	(:new.qt_idade = 79) Then
	:new.QT_SCORE := 20;
Elsif	(:new.qt_idade = 80) Then
	:new.QT_SCORE := 21;
Elsif	(:new.qt_idade = 81) Then
	:new.QT_SCORE := 22;
Elsif	((:new.qt_idade = 82)
Or	 (:new.qt_idade = 83)) Then
	:new.QT_SCORE := 23;
Elsif	(:new.qt_idade = 84) Then
	:new.QT_SCORE := 24;
Elsif	(:new.qt_idade = 85) Then
	:new.QT_SCORE := 25;
Elsif	(:new.qt_idade = 86) Then
	:new.QT_SCORE := 26;
Elsif	(:new.qt_idade = 87) Then
	:new.QT_SCORE := 27;
Elsif	(:new.qt_idade = 88) Then
	:new.QT_SCORE := 28;
Elsif	(:new.qt_idade = 89) Then
	:new.QT_SCORE := 29;
Elsif	((:new.qt_idade = 90)
Or	 (:new.qt_idade = 91)) Then
	:new.QT_SCORE := 30;
Elsif	(:new.qt_idade = 92) Then
	:new.QT_SCORE := 31;
Elsif	(:new.qt_idade = 93) Then
	:new.QT_SCORE := 32;
Elsif	(:new.qt_idade = 94) Then
	:new.QT_SCORE := 33;
End if;

If	((:new.QT_PA_SISTOLICA > 120)
And	 (:new.QT_PA_SISTOLICA < 139)) Then
	:new.QT_SCORE := :new.QT_SCORE + 1;
Elsif	((:new.QT_PA_SISTOLICA > 140)
And	 (:new.QT_PA_SISTOLICA < 159)) Then
	:new.QT_SCORE := :new.QT_SCORE + 2;
Elsif	((:new.QT_PA_SISTOLICA > 160)
And	 (:new.QT_PA_SISTOLICA < 179)) Then
	:new.QT_SCORE := :new.QT_SCORE + 3;
Elsif	(:new.QT_PA_SISTOLICA > 179) Then
	:new.QT_SCORE := :new.QT_SCORE + 5;
End if;

If	(:new.IE_DIABETES = 'S') Then
	:new.QT_SCORE := :new.QT_SCORE + 4;
End if;

If	(:new.IE_TABAGISMO = 'S') Then
	:new.QT_SCORE := :new.QT_SCORE + 5;
End if;

If	(:new.IE_IAM_ICC = 'S') Then
	:new.QT_SCORE := :new.QT_SCORE + 6;
End if;

If	(:new.IE_SOPRO_MITRAL = 'S') Then
	:new.QT_SCORE := :new.QT_SCORE + 4;
End if;

If	(:new.IE_HVE_ECG = 'S') Then
	:new.QT_SCORE := :new.QT_SCORE + 2;
End if;


end;
/


ALTER TABLE TASY.ESCALA_FRAMINGHAM_FA ADD (
  CONSTRAINT ESCFRAF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_FRAMINGHAM_FA ADD (
  CONSTRAINT ESCFRAF_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCFRAF_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_FRAMINGHAM_FA TO NIVEL_1;

