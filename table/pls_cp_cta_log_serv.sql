ALTER TABLE TASY.PLS_CP_CTA_LOG_SERV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_LOG_SERV CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_LOG_SERV
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  NR_SEQ_CONTA_PROC  NUMBER(10)                 NOT NULL,
  DS_LOG             VARCHAR2(4000 BYTE)        NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  VL_SERVICO         NUMBER(15,2),
  IE_DESTINO_REGRA   VARCHAR2(1 BYTE)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCPTLSE_I1 ON TASY.PLS_CP_CTA_LOG_SERV
(DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCPTLSE_I2 ON TASY.PLS_CP_CTA_LOG_SERV
(NR_SEQ_CONTA_PROC, IE_DESTINO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PCPTLSE_PK ON TASY.PLS_CP_CTA_LOG_SERV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PCPTLSE_PLSCOPRO_FK_I ON TASY.PLS_CP_CTA_LOG_SERV
(NR_SEQ_CONTA_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_cp_cta_log_serv_delete
before delete ON TASY.PLS_CP_CTA_LOG_SERV for each row
declare

begin

-- ao deletar um registro na tabela de log principal o mesmo � salvo em uma tabela de
-- hist�rico, desta forma na tabela principal guardamos apenas o ultimo recalculo feito
-- todos os demais ficam armazenados na tabela de hist�rico
insert into pls_cp_cta_log_serv_hist (
	nr_sequencia, ds_log, dt_atualizacao,
	ie_destino_regra, nm_usuario, nr_seq_conta_proc,
	dt_atualizacao_nrec, nm_usuario_nrec, vl_servico
) values (
	pls_cp_cta_log_serv_hist_seq.nextval, :old.ds_log, :old.dt_atualizacao,
	:old.ie_destino_regra, :old.nm_usuario, :old.nr_seq_conta_proc,
	sysdate, nvl(wheb_usuario_pck.get_nm_usuario, 'naoidentificado'), :old.vl_servico
	);

end pls_cp_cta_log_serv_delete;
/


ALTER TABLE TASY.PLS_CP_CTA_LOG_SERV ADD (
  CONSTRAINT PCPTLSE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_LOG_SERV ADD (
  CONSTRAINT PCPTLSE_PLSCOPRO_FK 
 FOREIGN KEY (NR_SEQ_CONTA_PROC) 
 REFERENCES TASY.PLS_CONTA_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PLS_CP_CTA_LOG_SERV TO NIVEL_1;

