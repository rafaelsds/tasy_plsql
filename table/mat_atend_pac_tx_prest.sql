ALTER TABLE TASY.MAT_ATEND_PAC_TX_PREST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAT_ATEND_PAC_TX_PREST CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAT_ATEND_PAC_TX_PREST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_TX_MAT  NUMBER(10)               NOT NULL,
  NR_SEQ_MAT_ORIGEM    NUMBER(10)               NOT NULL,
  NR_SEQ_MAT_GERADO    NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATPACTXPR_MATPACI_FK_I ON TASY.MAT_ATEND_PAC_TX_PREST
(NR_SEQ_MAT_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATPACTXPR_MATPACI_FK2_I ON TASY.MAT_ATEND_PAC_TX_PREST
(NR_SEQ_MAT_GERADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATPACTXPR_PK ON TASY.MAT_ATEND_PAC_TX_PREST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATPACTXPR_REGTXMP_FK_I ON TASY.MAT_ATEND_PAC_TX_PREST
(NR_SEQ_REGRA_TX_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAT_ATEND_PAC_TX_PREST ADD (
  CONSTRAINT MATPACTXPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAT_ATEND_PAC_TX_PREST ADD (
  CONSTRAINT MATPACTXPR_MATPACI_FK 
 FOREIGN KEY (NR_SEQ_MAT_ORIGEM) 
 REFERENCES TASY.MATERIAL_ATEND_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATPACTXPR_MATPACI_FK2 
 FOREIGN KEY (NR_SEQ_MAT_GERADO) 
 REFERENCES TASY.MATERIAL_ATEND_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATPACTXPR_REGTXMP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_TX_MAT) 
 REFERENCES TASY.REGRA_TAXA_MAT_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAT_ATEND_PAC_TX_PREST TO NIVEL_1;

