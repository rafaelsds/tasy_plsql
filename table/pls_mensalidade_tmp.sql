DROP TABLE TASY.PLS_MENSALIDADE_TMP CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.PLS_MENSALIDADE_TMP
(
  NR_SEQ_MENSALIDADE  NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


GRANT SELECT ON TASY.PLS_MENSALIDADE_TMP TO NIVEL_1;

