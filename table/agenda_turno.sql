ALTER TABLE TASY.AGENDA_TURNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_TURNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_TURNO
(
  CD_AGENDA             NUMBER(10)              NOT NULL,
  IE_DIA_SEMANA         NUMBER(1)               NOT NULL,
  HR_INICIAL            DATE                    NOT NULL,
  HR_FINAL              DATE                    NOT NULL,
  NR_MINUTO_INTERVALO   NUMBER(10)              NOT NULL,
  IE_CLASSIF_AGENDA     VARCHAR2(5 BYTE),
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  NR_SEQ_CLASSIF_MED    NUMBER(10),
  NR_SEQ_SALA           NUMBER(10),
  IE_ENCAIXE            VARCHAR2(1 BYTE)        NOT NULL,
  CD_CONVENIO_PADRAO    NUMBER(5),
  DT_INICIO_VIGENCIA    DATE,
  DT_FINAL_VIGENCIA     DATE,
  QT_IDADE_MIN          NUMBER(10),
  QT_IDADE_MAX          NUMBER(10),
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  IE_FREQUENCIA         VARCHAR2(15 BYTE),
  QT_TOTAL_TURNO        NUMBER(3),
  CD_MEDICO_REQ         VARCHAR2(10 BYTE),
  IE_FERIADO            VARCHAR2(1 BYTE),
  HR_INICIAL_INTERVALO  DATE,
  HR_FINAL_INTERVALO    DATE,
  CD_CATEGORIA_PADRAO   VARCHAR2(10 BYTE),
  IE_SEMANA             NUMBER(5),
  DS_OBS_INTERNA        VARCHAR2(255 BYTE),
  IE_UTILIZA_AGEWEB     VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  QT_ENCAIXE            NUMBER(3),
  IE_INTEGRAR_TURNO     VARCHAR2(1 BYTE),
  DS_QUEUE              VARCHAR2(255 BYTE),
  NR_AMOUNT             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGETURN_AGENDA_FK_I ON TASY.AGENDA_TURNO
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGETURN_AGSACON_FK_I ON TASY.AGENDA_TURNO
(NR_SEQ_SALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGETURN_CATCONV_FK_I ON TASY.AGENDA_TURNO
(CD_CONVENIO_PADRAO, CD_CATEGORIA_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGETURN_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGETURN_CONVENI_FK_I ON TASY.AGENDA_TURNO
(CD_CONVENIO_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGETURN_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGETURN_I1 ON TASY.AGENDA_TURNO
(CD_AGENDA, IE_DIA_SEMANA, HR_INICIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGETURN_MEDCLAG_FK_I ON TASY.AGENDA_TURNO
(NR_SEQ_CLASSIF_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGETURN_MEDCLAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGETURN_PESFISI_FK_I ON TASY.AGENDA_TURNO
(CD_MEDICO_REQ)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGETURN_PK ON TASY.AGENDA_TURNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.AGENDA_TURNO_tp  after update ON TASY.AGENDA_TURNO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(to_char(:old.HR_INICIAL,'hh24:mi:ss'),to_char(:new.HR_INICIAL,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_INICIAL',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.HR_FINAL,'hh24:mi:ss'),to_char(:new.HR_FINAL,'hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'HR_FINAL',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MINUTO_INTERVALO,1,4000),substr(:new.NR_MINUTO_INTERVALO,1,4000),:new.nm_usuario,nr_seq_w,'NR_MINUTO_INTERVALO',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENCAIXE,1,4000),substr(:new.IE_ENCAIXE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENCAIXE',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEMANA,1,4000),substr(:new.IE_SEMANA,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEMANA',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_MED,1,4000),substr(:new.NR_SEQ_CLASSIF_MED,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_MED',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_SALA,1,4000),substr(:new.NR_SEQ_SALA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_SALA',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FINAL_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FINAL_VIGENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FINAL_VIGENCIA',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ENCAIXE,1,4000),substr(:new.QT_ENCAIXE,1,4000),:new.nm_usuario,nr_seq_w,'QT_ENCAIXE',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIF_AGENDA,1,4000),substr(:new.IE_CLASSIF_AGENDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIF_AGENDA',ie_log_w,ds_w,'AGENDA_TURNO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Turno_After
after insert or update ON TASY.AGENDA_TURNO for each row
declare
qt_regras_w		number(10);

begin

	select	count(1)
	into	qt_regras_w
	from	agenda_turno_conv
	where	nr_seq_turno = :new.nr_sequencia;

	if (qt_regras_w > 0) then

		update 	agenda_turno_conv
		set 	ie_dia_semana = :new.ie_dia_semana,
				hr_inicial = :new.hr_inicial
		where 	nr_seq_turno = :new.nr_sequencia;

	end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Agenda_Turno_Delete
before delete ON TASY.AGENDA_TURNO 
FOR EACH ROW
DECLARE
dt_atualizacao  date := sysdate;
ds_log_w varchar2(4000);

PROCEDURE salvar_log_turnos IS
   PRAGMA autonomous_transaction;
    BEGIN
    INSERT INTO log_mov (DT_ATUALIZACAO, NM_USUARIO, DS_LOG, CD_LOG) 
                         VALUES 
					    (sysdate, 'tasy_turnos', ds_log_w, 202020);
	COMMIT;
   END;
  
PROCEDURE carregar_log_turnos IS
    BEGIN
      	ds_log_w := substr('DEL ' || 'SEQ= ' || :OLD.NR_SEQUENCIA || ' ERRO= ' || substr(SQLERRM,1,1000) || ' AGENDA= ' || :OLD.CD_AGENDA || ' DT_INICIAL= ' || to_char(:OLD.DT_INICIO_VIGENCIA, 'dd/mm/yyyy hh24:mi:ss') || ' DT_FINAL= ' || to_char(:OLD.DT_FINAL_VIGENCIA, 'dd/mm/yyyy hh24:mi:ss') || ' DIA_SEMANA= ' || :OLD.IE_DIA_SEMANA || ' USUARIO= ' || :OLD.NM_USUARIO,1,4000);
    	salvar_log_turnos;
   END;  

BEGIN
begin

delete from agenda_controle_horario
where dt_agenda >= trunc(sysdate)
and cd_agenda  = :old.cd_agenda;

carregar_log_turnos;

exception
 when others then
 	dt_atualizacao := sysdate;
	carregar_log_turnos;	
end;
end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_turno_atual
before insert or update ON TASY.AGENDA_TURNO 
FOR EACH ROW
DECLARE
dt_atualizacao  date := sysdate;
ds_log_w varchar2(4000);

PROCEDURE salvar_log_turnos IS
   PRAGMA autonomous_transaction;
    BEGIN
    INSERT INTO log_mov (DT_ATUALIZACAO, NM_USUARIO, DS_LOG, CD_LOG) 
                         VALUES 
					    (sysdate, 'tasy_turnos', ds_log_w, 202020);
	COMMIT;
   END;
  
PROCEDURE carregar_log_turnos IS
    BEGIN
      	ds_log_w := substr('OLD ' || 'SEQ= ' || :OLD.NR_SEQUENCIA || ' ERRO= ' || substr(SQLERRM,1,1000) || ' AGENDA= ' || :OLD.CD_AGENDA || ' DT_INICIAL= ' || to_char(:OLD.DT_INICIO_VIGENCIA, 'dd/mm/yyyy hh24:mi:ss') || ' DT_FINAL= ' || to_char(:OLD.DT_FINAL_VIGENCIA, 'dd/mm/yyyy hh24:mi:ss') || ' DIA_SEMANA= ' || :OLD.IE_DIA_SEMANA || ' USUARIO= ' || :OLD.NM_USUARIO,1,4000);
    	salvar_log_turnos;
    	ds_log_w := substr('NEW ' || 'SEQ= ' || :NEW.NR_SEQUENCIA || ' ERRO= ' || substr(SQLERRM,1,1000) || ' AGENDA= ' || :NEW.CD_AGENDA || ' DT_INICIAL= ' || to_char(:NEW.DT_INICIO_VIGENCIA, 'dd/mm/yyyy hh24:mi:ss') || ' DT_FINAL= ' || to_char(:NEW.DT_FINAL_VIGENCIA, 'dd/mm/yyyy hh24:mi:ss') || ' DIA_SEMANA= ' || :NEW.IE_DIA_SEMANA || ' USUARIO= ' || :NEW.NM_USUARIO,1,4000);
    	salvar_log_turnos; 
   END;  

BEGIN
begin

delete from agenda_controle_horario
where dt_agenda >= trunc(sysdate)
and cd_agenda  = :new.cd_agenda;

carregar_log_turnos;

exception
 when others then
 	dt_atualizacao := sysdate;
	carregar_log_turnos;	
end;
end;
/


ALTER TABLE TASY.AGENDA_TURNO ADD (
  CONSTRAINT AGETURN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_TURNO ADD (
  CONSTRAINT AGETURN_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA)
    ON DELETE CASCADE,
  CONSTRAINT AGETURN_MEDCLAG_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_MED) 
 REFERENCES TASY.MED_CLASSIF_AGENDA (NR_SEQUENCIA),
  CONSTRAINT AGETURN_AGSACON_FK 
 FOREIGN KEY (NR_SEQ_SALA) 
 REFERENCES TASY.AGENDA_SALA_CONSULTA (NR_SEQUENCIA),
  CONSTRAINT AGETURN_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO_PADRAO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT AGETURN_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_REQ) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT AGETURN_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO_PADRAO, CD_CATEGORIA_PADRAO) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA));

GRANT SELECT ON TASY.AGENDA_TURNO TO NIVEL_1;

GRANT SELECT ON TASY.AGENDA_TURNO TO ROBOCMTECNOLOGIA;

