ALTER TABLE TASY.QUEDA_FICHA_CONDUTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUEDA_FICHA_CONDUTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUEDA_FICHA_CONDUTA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_FICHA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CONDUTA       NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QUEFICD_PK ON TASY.QUEDA_FICHA_CONDUTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEFICD_PK
  MONITORING USAGE;


CREATE INDEX TASY.QUEFICD_QUECONU_FK_I ON TASY.QUEDA_FICHA_CONDUTA
(NR_SEQ_CONDUTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEFICD_QUECONU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QUEFICD_QUEFIOCO_FK_I ON TASY.QUEDA_FICHA_CONDUTA
(NR_SEQ_FICHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUEFICD_QUEFIOCO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.QUEDA_FICHA_CONDUTA ADD (
  CONSTRAINT QUEFICD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUEDA_FICHA_CONDUTA ADD (
  CONSTRAINT QUEFICD_QUECONU_FK 
 FOREIGN KEY (NR_SEQ_CONDUTA) 
 REFERENCES TASY.QUEDA_CONDUTA (NR_SEQUENCIA),
  CONSTRAINT QUEFICD_QUEFIOCO_FK 
 FOREIGN KEY (NR_SEQ_FICHA) 
 REFERENCES TASY.QUEDA_FICHA_OCORRENCIA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.QUEDA_FICHA_CONDUTA TO NIVEL_1;

