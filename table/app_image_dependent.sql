ALTER TABLE TASY.APP_IMAGE_DEPENDENT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.APP_IMAGE_DEPENDENT CASCADE CONSTRAINTS;

CREATE TABLE TASY.APP_IMAGE_DEPENDENT
(
  HOST_ID             NUMBER(10)                NOT NULL,
  IMAGE_ID            NUMBER(10)                NOT NULL,
  IMAGE_DEPENDENT_ID  NUMBER(10)                NOT NULL,
  NAME                VARCHAR2(200 BYTE)        NOT NULL,
  TAG_NAME            VARCHAR2(200 BYTE)        NOT NULL,
  VERSION             VARCHAR2(20 BYTE)         NOT NULL,
  BUILD               NUMBER(4)                 NOT NULL,
  BUILD_CONTAINER     NUMBER(10)                NOT NULL,
  DOCKER_IMAGE_ID     VARCHAR2(20 BYTE),
  IMAGESOURCE         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.APPIMGDEP_APPIMAGE_FK_I ON TASY.APP_IMAGE_DEPENDENT
(HOST_ID, IMAGE_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APPIMGDEP_APPMHOST_FK_I ON TASY.APP_IMAGE_DEPENDENT
(HOST_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.APPIMGDEP_PK ON TASY.APP_IMAGE_DEPENDENT
(HOST_ID, IMAGE_ID, IMAGE_DEPENDENT_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.APP_IMAGE_DEPENDENT ADD (
  CONSTRAINT APPIMGDEP_PK
 PRIMARY KEY
 (HOST_ID, IMAGE_ID, IMAGE_DEPENDENT_ID)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.APP_IMAGE_DEPENDENT ADD (
  CONSTRAINT APPIMGDEP_APPIMAGE_FK 
 FOREIGN KEY (HOST_ID, IMAGE_ID) 
 REFERENCES TASY.APP_IMAGE (HOST_ID,IMAGE_ID),
  CONSTRAINT APPIMGDEP_APPMHOST_FK 
 FOREIGN KEY (HOST_ID) 
 REFERENCES TASY.APP_HOST (HOST_ID));

GRANT SELECT ON TASY.APP_IMAGE_DEPENDENT TO NIVEL_1;

