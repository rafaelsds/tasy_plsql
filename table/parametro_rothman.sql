ALTER TABLE TASY.PARAMETRO_ROTHMAN
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PARAMETRO_ROTHMAN CASCADE CONSTRAINTS;

CREATE TABLE TASY.PARAMETRO_ROTHMAN
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_URL               VARCHAR2(4000 BYTE),
  NR_SEQ_HEMOGLOBINA   NUMBER(10),
  NR_SEQ_LEUCOCITOS    NUMBER(10),
  NR_SEQ_CREATININA    NUMBER(10),
  NR_SEQ_UREIA         NUMBER(10),
  NR_SEQ_SODIO         NUMBER(10),
  NR_SEQ_POTASSIO      NUMBER(10),
  NR_SEQ_CLORO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PARROTH_EXALABO_FK_I ON TASY.PARAMETRO_ROTHMAN
(NR_SEQ_CLORO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARROTH_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PARROTH_EXALABO_FK2_I ON TASY.PARAMETRO_ROTHMAN
(NR_SEQ_CREATININA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARROTH_EXALABO_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PARROTH_EXALABO_FK3_I ON TASY.PARAMETRO_ROTHMAN
(NR_SEQ_HEMOGLOBINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARROTH_EXALABO_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.PARROTH_EXALABO_FK4_I ON TASY.PARAMETRO_ROTHMAN
(NR_SEQ_LEUCOCITOS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARROTH_EXALABO_FK4_I
  MONITORING USAGE;


CREATE INDEX TASY.PARROTH_EXALABO_FK5_I ON TASY.PARAMETRO_ROTHMAN
(NR_SEQ_POTASSIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARROTH_EXALABO_FK5_I
  MONITORING USAGE;


CREATE INDEX TASY.PARROTH_EXALABO_FK6_I ON TASY.PARAMETRO_ROTHMAN
(NR_SEQ_SODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARROTH_EXALABO_FK6_I
  MONITORING USAGE;


CREATE INDEX TASY.PARROTH_EXALABO_FK7_I ON TASY.PARAMETRO_ROTHMAN
(NR_SEQ_UREIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PARROTH_EXALABO_FK7_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PARROTH_PK ON TASY.PARAMETRO_ROTHMAN
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PARROTH_UK ON TASY.PARAMETRO_ROTHMAN
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PARAMETRO_ROTHMAN_tp  after update ON TASY.PARAMETRO_ROTHMAN FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_URL,1,4000),substr(:new.DS_URL,1,4000),:new.nm_usuario,nr_seq_w,'DS_URL',ie_log_w,ds_w,'PARAMETRO_ROTHMAN',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PARAMETRO_ROTHMAN ADD (
  CONSTRAINT PARROTH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PARROTH_UK
 UNIQUE (CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PARAMETRO_ROTHMAN ADD (
  CONSTRAINT PARROTH_EXALABO_FK6 
 FOREIGN KEY (NR_SEQ_SODIO) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT PARROTH_EXALABO_FK7 
 FOREIGN KEY (NR_SEQ_UREIA) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT PARROTH_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_CLORO) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT PARROTH_EXALABO_FK2 
 FOREIGN KEY (NR_SEQ_CREATININA) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT PARROTH_EXALABO_FK3 
 FOREIGN KEY (NR_SEQ_HEMOGLOBINA) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT PARROTH_EXALABO_FK4 
 FOREIGN KEY (NR_SEQ_LEUCOCITOS) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT PARROTH_EXALABO_FK5 
 FOREIGN KEY (NR_SEQ_POTASSIO) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME));

GRANT SELECT ON TASY.PARAMETRO_ROTHMAN TO NIVEL_1;

