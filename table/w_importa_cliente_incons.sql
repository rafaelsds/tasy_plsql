DROP TABLE TASY.W_IMPORTA_CLIENTE_INCONS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_IMPORTA_CLIENTE_INCONS
(
  NR_INCONSISTENCIA    NUMBER(10),
  NR_SEQ_CLIENTE       NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_ATRIBUTO          VARCHAR2(30 BYTE),
  DS_MOTIVO_INCONSIST  VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.W_IMPORTA_CLIENTE_INCONS TO NIVEL_1;

