ALTER TABLE TASY.TX_RECEPTOR_PRIORIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TX_RECEPTOR_PRIORIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.TX_RECEPTOR_PRIORIDADE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_RECEPTOR           NUMBER(10),
  NR_SEQ_MOTIVO_PRIORIDADE  NUMBER(10)          NOT NULL,
  DS_OBSERVACAO             VARCHAR2(255 BYTE)  NOT NULL,
  DT_ULTIMO_AVISO           DATE,
  NR_PRIORIDADE             NUMBER(5)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TXRECPRI_PK ON TASY.TX_RECEPTOR_PRIORIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECPRI_PK
  MONITORING USAGE;


CREATE INDEX TASY.TXRECPRI_TXMOTPRI_FK_I ON TASY.TX_RECEPTOR_PRIORIDADE
(NR_SEQ_MOTIVO_PRIORIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECPRI_TXMOTPRI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.TXRECPRI_TXRECE_FK_I ON TASY.TX_RECEPTOR_PRIORIDADE
(NR_SEQ_RECEPTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TXRECPRI_TXRECE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.tx_receptor_prioridade_insert
before insert ON TASY.TX_RECEPTOR_PRIORIDADE for each row
declare

cd_pessoa_fisica_w		varchar2(10);
nm_pessoa_fisica_w		varchar2(255);
cd_perfil_w			number(5);

begin


begin
select	cd_perfil_prior
into	cd_perfil_w
from	tx_parametros;
exception
	when others then
	cd_perfil_w	:= 0;
end;

if 	(cd_perfil_w > 0) then

	select	cd_pessoa_fisica,
		substr(obter_nome_pf(cd_pessoa_fisica),1,80)
	into	cd_pessoa_fisica_w,
		nm_pessoa_fisica_w
	from	tx_receptor b
	where	:new.nr_seq_receptor = b.nr_sequencia
	and	tx_obter_se_paciente_ativo(b.nr_sequencia) = 'S';

	if 	(cd_pessoa_fisica_w is not null) then
		insert into comunic_interna (
			dt_comunicado,
			ds_titulo,
			ds_comunicado,
			nm_usuario,
			dt_atualizacao,
			ie_geral,
			nm_usuario_destino,
			ds_perfil_adicional,
			nr_sequencia,
			ie_gerencial,
			dt_liberacao,
			cd_estab_destino
		) values (
			sysdate,
			wheb_mensagem_pck.get_texto(798733) || ' ',
			wheb_mensagem_pck.get_texto(798762,
						'CD_PESSOA_FISICA='||cd_pessoa_fisica_w||
						';NM_PESSOA_FISICA='||nm_pessoa_fisica_w),
			:new.nm_usuario,
			sysdate,
			'N',
			'',
			cd_perfil_w||', ',
			comunic_interna_seq.nextval,
			'N',
			sysdate,
			null
		);
	end if;
end if;

end;
/


ALTER TABLE TASY.TX_RECEPTOR_PRIORIDADE ADD (
  CONSTRAINT TXRECPRI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TX_RECEPTOR_PRIORIDADE ADD (
  CONSTRAINT TXRECPRI_TXRECE_FK 
 FOREIGN KEY (NR_SEQ_RECEPTOR) 
 REFERENCES TASY.TX_RECEPTOR (NR_SEQUENCIA),
  CONSTRAINT TXRECPRI_TXMOTPRI_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PRIORIDADE) 
 REFERENCES TASY.TX_MOTIVO_PRIORIDADE (NR_SEQUENCIA));

GRANT SELECT ON TASY.TX_RECEPTOR_PRIORIDADE TO NIVEL_1;

