ALTER TABLE TASY.REGRA_DIVERG_OC_NF_JUST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_DIVERG_OC_NF_JUST CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_DIVERG_OC_NF_JUST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  NM_USUARIO_REGRA     VARCHAR2(15 BYTE),
  CD_SETOR_REGRA       NUMBER(5),
  CD_PERFIL_REGRA      NUMBER(5),
  IE_PERMITE_JUSTIF    VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RDOCNFJU_PERFIL_FK_I ON TASY.REGRA_DIVERG_OC_NF_JUST
(CD_PERFIL_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RDOCNFJU_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RDOCNFJU_PK ON TASY.REGRA_DIVERG_OC_NF_JUST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RDOCNFJU_REGDION_FK_I ON TASY.REGRA_DIVERG_OC_NF_JUST
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RDOCNFJU_REGDION_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RDOCNFJU_SETATEN_FK_I ON TASY.REGRA_DIVERG_OC_NF_JUST
(CD_SETOR_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RDOCNFJU_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.RDOCNFJU_USUARIO_FK_I ON TASY.REGRA_DIVERG_OC_NF_JUST
(NM_USUARIO_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RDOCNFJU_USUARIO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_DIVERG_OC_NF_JUST_tp  after update ON TASY.REGRA_DIVERG_OC_NF_JUST FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_REGRA,1,4000),substr(:new.NR_SEQ_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF_JUST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_REGRA,1,4000),substr(:new.NM_USUARIO_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_REGRA',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF_JUST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_JUSTIF,1,4000),substr(:new.IE_PERMITE_JUSTIF,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_JUSTIF',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF_JUST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL_REGRA,1,4000),substr(:new.CD_PERFIL_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL_REGRA',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF_JUST',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_REGRA,1,4000),substr(:new.CD_SETOR_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_REGRA',ie_log_w,ds_w,'REGRA_DIVERG_OC_NF_JUST',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_DIVERG_OC_NF_JUST ADD (
  CONSTRAINT RDOCNFJU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_DIVERG_OC_NF_JUST ADD (
  CONSTRAINT RDOCNFJU_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_REGRA) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT RDOCNFJU_REGDION_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.REGRA_DIVERG_OC_NF (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT RDOCNFJU_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_REGRA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT RDOCNFJU_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_REGRA) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.REGRA_DIVERG_OC_NF_JUST TO NIVEL_1;

