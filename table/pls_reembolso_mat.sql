ALTER TABLE TASY.PLS_REEMBOLSO_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REEMBOLSO_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REEMBOLSO_MAT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_MATERIAL        NUMBER(10)             NOT NULL,
  QT_MATERIAL            NUMBER(9,3)            NOT NULL,
  DT_ATENDIMENTO         DATE                   NOT NULL,
  NR_SEQ_REEMBOLSO       NUMBER(10)             NOT NULL,
  IE_TIPO_DESPESA        VARCHAR2(1 BYTE),
  QT_MAT_LIBERADO        NUMBER(9,3),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  VL_MATERIAL            NUMBER(15,2),
  VL_LIBERADO            NUMBER(15,2),
  VL_MATERIAL_UNITARIO   NUMBER(15,2),
  VL_LIBERADO_UNITARIO   NUMBER(15,2),
  VL_CALCULADO           NUMBER(15,2),
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  NR_SEQ_REGRA           NUMBER(10),
  CD_CONTA_CRED          VARCHAR2(20 BYTE),
  CD_CONTA_DEB           VARCHAR2(20 BYTE),
  CD_HISTORICO           NUMBER(10),
  NR_SEQ_REGRA_CTB_DEB   NUMBER(10),
  NR_SEQ_REGRA_CTB_CRED  NUMBER(10),
  NR_SEQ_GRUPO_ANS       NUMBER(10),
  IE_STATUS              VARCHAR2(1 BYTE),
  CD_MATERIAL            NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREMA_ANSGRDE_FK_I ON TASY.PLS_REEMBOLSO_MAT
(NR_SEQ_GRUPO_ANS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREMA_ANSGRDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREMA_CONCONT_FK_I ON TASY.PLS_REEMBOLSO_MAT
(CD_CONTA_CRED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREMA_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREMA_CONCONT_FK2_I ON TASY.PLS_REEMBOLSO_MAT
(CD_CONTA_DEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREMA_CONCONT_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREMA_ESTABEL_FK_I ON TASY.PLS_REEMBOLSO_MAT
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREMA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREMA_HISPADR_FK_I ON TASY.PLS_REEMBOLSO_MAT
(CD_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREMA_HISPADR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREMA_MATERIA_FK_I ON TASY.PLS_REEMBOLSO_MAT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREMA_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSREMA_PK ON TASY.PLS_REEMBOLSO_MAT
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREMA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSREMA_PLSMAT_FK_I ON TASY.PLS_REEMBOLSO_MAT
(NR_SEQ_MATERIAL)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREMA_PLSMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREMA_PLSREEM_FK_I ON TASY.PLS_REEMBOLSO_MAT
(NR_SEQ_REEMBOLSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREMA_PLSREEM_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REEMBOLSO_MAT_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PLS_REEMBOLSO_MAT FOR EACH ROW
declare

cd_material_w	number(6);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(:new.nr_seq_material is not null) then
		select	cd_material
		into	cd_material_w
		from	pls_material a
		where	a.nr_sequencia	= :new.nr_seq_material;

		:new.cd_material	:= cd_material_w;
	end if;
end if;

END;
/


ALTER TABLE TASY.PLS_REEMBOLSO_MAT ADD (
  CONSTRAINT PLSREMA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REEMBOLSO_MAT ADD (
  CONSTRAINT PLSREMA_ANSGRDE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_ANS) 
 REFERENCES TASY.ANS_GRUPO_DESPESA (NR_SEQUENCIA),
  CONSTRAINT PLSREMA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PLSREMA_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSREMA_PLSREEM_FK 
 FOREIGN KEY (NR_SEQ_REEMBOLSO) 
 REFERENCES TASY.PLS_REEMBOLSO (NR_SEQUENCIA),
  CONSTRAINT PLSREMA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSREMA_CONCONT_FK2 
 FOREIGN KEY (CD_CONTA_DEB) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PLSREMA_HISPADR_FK 
 FOREIGN KEY (CD_HISTORICO) 
 REFERENCES TASY.HISTORICO_PADRAO (CD_HISTORICO),
  CONSTRAINT PLSREMA_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CRED) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL));

GRANT SELECT ON TASY.PLS_REEMBOLSO_MAT TO NIVEL_1;

