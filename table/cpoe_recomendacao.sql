ALTER TABLE TASY.CPOE_RECOMENDACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_RECOMENDACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_RECOMENDACAO
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  DS_RECOMENDACAO              VARCHAR2(4000 BYTE),
  CD_INTERVALO                 VARCHAR2(7 BYTE),
  DS_HORARIOS                  VARCHAR2(2000 BYTE),
  CD_RECOMENDACAO              NUMBER(10)       NOT NULL,
  IE_ACM                       VARCHAR2(1 BYTE),
  NR_ATENDIMENTO               NUMBER(10),
  HR_PRIM_HORARIO              VARCHAR2(5 BYTE),
  IE_URGENCIA                  VARCHAR2(3 BYTE),
  IE_SE_NECESSARIO             VARCHAR2(1 BYTE),
  NR_SEQ_TOPOGRAFIA            NUMBER(10),
  DS_HORA_00                   VARCHAR2(30 BYTE),
  DS_HORA_01                   VARCHAR2(30 BYTE),
  DS_HORA_02                   VARCHAR2(30 BYTE),
  DS_HORA_03                   VARCHAR2(30 BYTE),
  DS_HORA_04                   VARCHAR2(30 BYTE),
  DS_HORA_05                   VARCHAR2(30 BYTE),
  DS_HORA_06                   VARCHAR2(30 BYTE),
  DS_HORA_07                   VARCHAR2(30 BYTE),
  DS_HORA_08                   VARCHAR2(30 BYTE),
  DS_HORA_09                   VARCHAR2(30 BYTE),
  DS_HORA_10                   VARCHAR2(30 BYTE),
  DS_HORA_11                   VARCHAR2(30 BYTE),
  DS_HORA_12                   VARCHAR2(30 BYTE),
  DS_HORA_13                   VARCHAR2(30 BYTE),
  DS_HORA_14                   VARCHAR2(30 BYTE),
  DS_HORA_15                   VARCHAR2(30 BYTE),
  DS_HORA_16                   VARCHAR2(30 BYTE),
  DS_HORA_17                   VARCHAR2(30 BYTE),
  DS_HORA_18                   VARCHAR2(30 BYTE),
  DS_HORA_19                   VARCHAR2(30 BYTE),
  DS_HORA_20                   VARCHAR2(30 BYTE),
  DS_HORA_21                   VARCHAR2(30 BYTE),
  DS_HORA_22                   VARCHAR2(30 BYTE),
  DS_HORA_23                   VARCHAR2(30 BYTE),
  IE_DURACAO                   VARCHAR2(3 BYTE) NOT NULL,
  IE_PERIODO                   VARCHAR2(3 BYTE),
  DT_INICIO                    DATE,
  DT_FIM                       DATE,
  DT_LIBERACAO                 DATE,
  IE_ADMINISTRACAO             VARCHAR2(3 BYTE),
  IE_EVENTO_UNICO              VARCHAR2(1 BYTE),
  NR_SEQ_CPOE_ANTERIOR         NUMBER(10),
  DT_PROX_GERACAO              DATE,
  NR_OCORRENCIA                NUMBER(15,4),
  DT_SUSPENSAO                 DATE,
  NM_USUARIO_SUSP              VARCHAR2(15 BYTE),
  DT_LIB_SUSPENSAO             DATE,
  DS_STACK                     VARCHAR2(2000 BYTE),
  CD_PERFIL_ATIVO              NUMBER(5),
  CD_PESSOA_FISICA             VARCHAR2(10 BYTE),
  IE_RETROGRADO                VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA             VARCHAR2(2000 BYTE),
  NM_USUARIO_LIB_ENF           VARCHAR2(15 BYTE),
  CD_FARMAC_LIB                VARCHAR2(10 BYTE),
  CD_FUNCAO_ORIGEM             NUMBER(10),
  IE_BAIXADO_POR_ALTA          VARCHAR2(1 BYTE),
  IE_CPOE_FARM                 VARCHAR2(1 BYTE),
  NR_CPOE_INTERF_FARM          NUMBER(10),
  IE_MOTIVO_PRESCRICAO         VARCHAR2(3 BYTE),
  NR_SEQ_MOTIVO_SUSP           NUMBER(10),
  DS_MOTIVO_SUSP               VARCHAR2(2000 BYTE),
  DS_OBSERVACAO                VARCHAR2(255 BYTE),
  IE_INTERVENCAO_FARM          VARCHAR2(1 BYTE),
  NR_SEQ_PEND_PAC_ACAO         NUMBER(10),
  DT_PRIM_HORARIO              DATE,
  IE_ITEM_VALIDO               VARCHAR2(1 BYTE),
  NR_SEQ_TRANSCRICAO           NUMBER(10),
  IE_ITEM_ALTA                 VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO         NUMBER(5),
  IE_PRESCRITOR_AUX            VARCHAR2(1 BYTE),
  DT_LIBERACAO_AUX             DATE,
  CD_MEDICO                    VARCHAR2(10 BYTE),
  NR_SEQ_ASSINATURA            NUMBER(10),
  NR_SEQ_ASSINATURA_SUSP       NUMBER(10),
  DT_LIBERACAO_ENF             DATE,
  DT_LIBERACAO_FARM            DATE,
  NM_USUARIO_LIB_FARM          VARCHAR2(15 BYTE),
  NR_SEQ_ASSINATURA_ENF        NUMBER(10),
  NR_SEQ_ASSINATURA_FARM       NUMBER(10),
  NR_CIRURGIA_PATOLOGIA        NUMBER(10),
  NR_CIRURGIA                  NUMBER(10),
  NR_SEQ_PEPO                  NUMBER(10),
  IE_TIPO_PRESCR_CIRUR         VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA                NUMBER(10),
  IE_ONCOLOGIA                 VARCHAR2(1 BYTE),
  NR_SEQ_CONCLUSAO_APAE        NUMBER(10),
  IE_NIVEL_ATENCAO             VARCHAR2(1 BYTE),
  CD_PROTOCOLO                 NUMBER(10),
  NR_SEQ_PROTOCOLO             NUMBER(6),
  IE_FORMA_GERACAO             VARCHAR2(1 BYTE) DEFAULT null,
  IE_FORMA_SUSPENSAO           VARCHAR2(1 BYTE),
  IE_FUTURO                    VARCHAR2(1 BYTE),
  DT_LIBERACAO_FUT             DATE,
  NM_USUARIO_LIB_FUT           VARCHAR2(15 BYTE),
  DT_CIENCIA_MEDICO            DATE,
  IE_ADEP                      VARCHAR2(1 BYTE),
  CD_KIT                       NUMBER(10),
  CD_DEPARTAMENTO              NUMBER(6),
  NR_SEQ_SOAP                  NUMBER(10),
  DT_ALTA_MEDICO               DATE,
  QT_DIAS_PADRAO               NUMBER(5),
  NR_SEQ_ASSINATURA_SUSP_FARM  NUMBER(10),
  NR_SEQ_ASSINATURA_REV        NUMBER(10),
  NR_SEQ_NAIS_INSURANCE        NUMBER(10),
  NR_SEQ_CPOE_ORDER_UNIT       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPOEREC_AGEPACI_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_ATEPACI_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_ATESOAP_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_CIRURGI_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_CPOEORU_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_CPOE_ORDER_UNIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_DEPMED_FK_I ON TASY.CPOE_RECOMENDACAO
(CD_DEPARTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_GQAPPAC_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_PEND_PAC_ACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_I1 ON TASY.CPOE_RECOMENDACAO
(DT_PROX_GERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_I2 ON TASY.CPOE_RECOMENDACAO
(NM_USUARIO, NR_ATENDIMENTO, DT_PROX_GERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_I3 ON TASY.CPOE_RECOMENDACAO
(NVL("IE_PRESCRITOR_AUX",'N'), CD_MEDICO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_I4 ON TASY.CPOE_RECOMENDACAO
(NR_ATENDIMENTO, DT_PROX_GERACAO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_MOTSUPR_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_NAISINS_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_NAIS_INSURANCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_PEPOCIR_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_PERFIL_FK_I ON TASY.CPOE_RECOMENDACAO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_PESFISI_FK_I ON TASY.CPOE_RECOMENDACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_PESFISI_FK2_I ON TASY.CPOE_RECOMENDACAO
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPOEREC_PK ON TASY.CPOE_RECOMENDACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CPOEREC_PK
  MONITORING USAGE;


CREATE INDEX TASY.CPOEREC_SETATEN_FK_I ON TASY.CPOE_RECOMENDACAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_TASASDI_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_TASASDI_FK2_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_ASSINATURA_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_TASASDI_FK3_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_ASSINATURA_ENF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_TASASDI_FK4_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_ASSINATURA_FARM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_TASASDI_FK5_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_ASSINATURA_SUSP_FARM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_TASASDI_FK6_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_ASSINATURA_REV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOEREC_TIPRECO_FK_I ON TASY.CPOE_RECOMENDACAO
(CD_RECOMENDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CPOEREC_TIPRECO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CPOEREC_TOPDOR_FK_I ON TASY.CPOE_RECOMENDACAO
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CPOEREC_TOPDOR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.cpoe_rec_insert_update
before insert or update ON TASY.CPOE_RECOMENDACAO 
for each row
declare

begin
    if (:new.hr_prim_horario is not null) and ((:new.hr_prim_horario <> :old.hr_prim_horario) or (:old.dt_prim_horario is null)) then
		:new.dt_prim_horario := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_horario,'dd/mm/yyyy hh24:mi');
	end if;		
exception
	when others then
	null;
end;
/


CREATE OR REPLACE TRIGGER TASY.CPOE_RECOMENDACAO_Insert
BEFORE INSERT ON TASY.CPOE_RECOMENDACAO FOR EACH ROW
DECLARE

BEGIN

:new.ds_stack	:= substr(dbms_utility.format_call_stack,1,2000);

if (:new.cd_perfil_ativo is null) then
	:new.cd_perfil_ativo := obter_perfil_ativo;
end if;

if (nvl(:new.ie_retrogrado, 'N') = 'S' or nvl(:new.ie_oncologia, 'N') = 'S') then
	:new.dt_prox_geracao := nvl(:new.dt_inicio, sysdate);
end if;

if (:new.nr_seq_cpoe_anterior is not null and :new.dt_liberacao is null and :new.cd_funcao_origem = 2314) then
	:new.dt_liberacao_enf := null;
	:new.dt_liberacao_farm := null;
	:new.nm_usuario_lib_enf := null;
	:new.nm_usuario_lib_farm := null;
	:new.cd_farmac_lib := null;
end if;

if	(:new.nr_cirurgia is not null) and
	(:new.ie_tipo_prescr_cirur is null) then
	:new.ie_tipo_prescr_cirur := 2;
end if;

if	(:new.ie_duracao <> 'P') then
	:new.dt_fim := null;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.cpoe_recomendacao_atual
before insert or update ON TASY.CPOE_RECOMENDACAO for each row
declare

begin
begin
    if (:new.hr_prim_horario is not null) and ((:new.hr_prim_horario <> :old.hr_prim_horario) or (:old.dt_prim_horario is null)) then
		:new.dt_prim_horario := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_prim_horario,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.cpoe_recomendacao_bef_delete
before delete ON TASY.CPOE_RECOMENDACAO for each row
declare

begin

	update cpoe_revalidation_events
	set    nr_seq_recomendation = null
	where  nr_seq_recomendation = :old.nr_sequencia;

exception
when others then
	null;
end;
/


CREATE OR REPLACE TRIGGER TASY.CPOE_RECOMENDACAO_tp  after update ON TASY.CPOE_RECOMENDACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_ATENDIMENTO,1,4000),substr(:new.NR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ATENDIMENTO',ie_log_w,ds_w,'CPOE_RECOMENDACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_RECOMENDACAO,1,4000),substr(:new.CD_RECOMENDACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_RECOMENDACAO',ie_log_w,ds_w,'CPOE_RECOMENDACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'CPOE_RECOMENDACAO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO',ie_log_w,ds_w,'CPOE_RECOMENDACAO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM',ie_log_w,ds_w,'CPOE_RECOMENDACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERVALO,1,4000),substr(:new.CD_INTERVALO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERVALO',ie_log_w,ds_w,'CPOE_RECOMENDACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.cpoe_rec_insert_update_after
after insert or update ON TASY.CPOE_RECOMENDACAO for each row
declare

ds_stack_w		varchar2(2000);
ds_log_cpoe_w	varchar2(2000);
dt_min_date_w	date := to_date('30/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss');

ie_order_integr_type_w 	varchar2(10);

nr_entity_identifier_w	cpoe_integracao.nr_sequencia%type;
ie_use_integration_w	varchar2(10);

begin
	begin

	if	(nvl(:new.nr_sequencia,0) <> nvl(:old.nr_sequencia,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_sequencia(' || nvl(:old.nr_sequencia,0) || '/' || nvl(:new.nr_sequencia,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_seq_cpoe_anterior,0) <> nvl(:old.nr_seq_cpoe_anterior,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_seq_cpoe_anterior(' || nvl(:old.nr_seq_cpoe_anterior,0) || '/' || nvl(:new.nr_seq_cpoe_anterior,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_recomendacao,0) <> nvl(:old.cd_recomendacao,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_recomendacao(' || nvl(:old.cd_recomendacao,0) || '/' || nvl(:new.cd_recomendacao,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_liberacao, dt_min_date_w) <> nvl(:old.dt_liberacao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_liberacao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_suspensao, dt_min_date_w) <> nvl(:old.dt_suspensao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_suspensao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_forma_suspensao,'XPTO') <> nvl(:old.ie_forma_suspensao,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_forma_suspensao(' || nvl(:old.ie_forma_suspensao,'<NULL>') || '/' || nvl(:new.ie_forma_suspensao,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_lib_suspensao, dt_min_date_w) <> nvl(:old.dt_lib_suspensao, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_lib_suspensao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_lib_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_lib_suspensao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_liberacao_enf, dt_min_date_w) <> nvl(:old.dt_liberacao_enf, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_liberacao_enf(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao_enf, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao_enf, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_liberacao_farm, dt_min_date_w) <> nvl(:old.dt_liberacao_farm, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_liberacao_farm(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_liberacao_farm, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_liberacao_farm, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_prox_geracao, sysdate) <> nvl(:old.dt_prox_geracao, sysdate)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_prox_geracao(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_prox_geracao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_prox_geracao, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_inicio, sysdate) <> nvl(:old.dt_inicio, sysdate)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_inicio(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_inicio, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_inicio, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_fim, sysdate) <> nvl(:old.dt_fim, sysdate)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_fim(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_fim, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_fim, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ds_horarios,'XPTO') <> nvl(:old.ds_horarios,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ds_horarios(' || nvl(:old.ds_horarios,'<NULL>') || '/' || nvl(:new.ds_horarios,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_evento_unico,'XPTO') <> nvl(:old.ie_evento_unico,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_evento_unico(' || nvl(:old.ie_evento_unico,'<NULL>') || '/' || nvl(:new.ie_evento_unico,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_administracao,'XPTO') <> nvl(:old.ie_administracao,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_administracao(' || nvl(:old.ie_administracao,'<NULL>') || '/' || nvl(:new.ie_administracao,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ds_justificativa,'XPTO') <> nvl(:old.ds_justificativa,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ds_justificativa(' || nvl(length(:old.ds_justificativa),0) || '/' || nvl(length(:new.ds_justificativa),0)||'); ',1,2000);
	end if;

	if	(nvl(:new.hr_prim_horario,'XPTO') <> nvl(:old.hr_prim_horario,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' hr_prim_horario(' || nvl(:old.hr_prim_horario,'<NULL>') || '/' || nvl(:new.hr_prim_horario,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_setor_atendimento,0) <> nvl(:old.cd_setor_atendimento,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_setor_atendimento(' || nvl(:old.cd_setor_atendimento,0) || '/' || nvl(:new.cd_setor_atendimento,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_medico,0) <> nvl(:old.cd_medico,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_medico(' || nvl(:old.cd_medico,0) || '/' || nvl(:new.cd_medico,0)||'); ',1,2000);
	end if;

	if	(nvl(:new.cd_intervalo,'XPTO') <> nvl(:old.cd_intervalo,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' cd_intervalo(' || nvl(:old.cd_intervalo,'<NULL>') || '/' || nvl(:new.cd_intervalo,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.ie_baixado_por_alta,'XPTO') <> nvl(:old.ie_baixado_por_alta,'XPTO')) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' ie_baixado_por_alta(' || nvl(:old.ie_baixado_por_alta,'<NULL>') || '/' || nvl(:new.ie_baixado_por_alta,'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.dt_alta_medico, dt_min_date_w) <> nvl(:old.dt_alta_medico, dt_min_date_w)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' dt_alta_medico(' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:old.dt_alta_medico, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>') || '/' || nvl(PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_alta_medico, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone),'<NULL>')||'); ',1,2000);
	end if;

	if	(nvl(:new.nr_ocorrencia,0) <> nvl(:old.nr_ocorrencia,0)) then
		ds_log_cpoe_w	:= substr(ds_log_cpoe_w || ' nr_ocorrencia(' || nvl(:old.nr_ocorrencia,0) || '/' || nvl(:new.nr_ocorrencia,0)||'); ',1,2000);
	end if;

	if	(ds_log_cpoe_w is not null) then

		if (nvl(:old.nr_sequencia, 0) > 0) then
			ds_log_cpoe_w := substr('Alteracoes(old/new)= ' || ds_log_cpoe_w,1,2000);
		else
			ds_log_cpoe_w := substr('Criacao(old/new)= ' || ds_log_cpoe_w,1,2000);
		end if;

		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);
		ds_log_cpoe_w := substr(ds_log_cpoe_w ||' FUNCAO('||to_char(obter_funcao_ativa)||'); PERFIL('||to_char(obter_perfil_ativo)||')',1,2000);

		insert into log_cpoe(nr_sequencia, nr_atendimento, dt_atualizacao, nm_usuario, nr_seq_recomendacao, ds_log, ds_stack) values (log_cpoe_seq.nextval, :new.nr_atendimento, sysdate, :new.nm_usuario, :new.nr_sequencia, ds_log_cpoe_w, ds_stack_w);
	end if;

	exception
	when others then
		ds_stack_w	:= substr(dbms_utility.format_call_stack,1,2000);

		insert into log_cpoe(nr_sequencia,
							nr_atendimento,
							dt_atualizacao,
							nm_usuario,
							nr_seq_recomendacao,
							ds_log,
							ds_stack)
		values (			log_cpoe_seq.nextval,
							:new.nr_atendimento,
							sysdate,
							:new.nm_usuario,
							:new.nr_sequencia,
							'EXCEPTION CPOE_REC_INSERT_UPDATE_AFTER',
							ds_stack_w);
	end;

	begin
		if (:new.dt_liberacao is not null and ((:old.dt_liberacao_enf is null and  :new.dt_liberacao_enf is not null) or (:old.dt_liberacao_farm is null and :new.dt_liberacao_farm is not null))) then
			cpoe_atualizar_inf_adic(:new.nr_atendimento, :new.nr_sequencia, 'R', :new.dt_liberacao_enf, :new.dt_liberacao_farm);
		end if;
	exception
	when others then
		gravar_log_cpoe('CPOE_REC_INSERT_UPDATE_AFTER - CPOE_ATUALIZAR_INF_ADIC - Erro: ' || substr(sqlerrm(sqlcode),1,1500) || ' :new.nr_sequencia '|| :new.nr_sequencia, :new.nr_atendimento);
	end;

  if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
  begin
		obter_param_usuario(9041, 10, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_use_integration_w);

		if (ie_use_integration_w = 'S')
			and ((:OLD.DT_LIBERACAO IS NULL AND :NEW.DT_LIBERACAO IS NOT NULL)
				or (:OLD.DT_LIB_SUSPENSAO IS NULL AND :NEW.DT_LIB_SUSPENSAO IS NOT NULL )) then

			select obter_cpoe_regra_ator('R')
			into ie_order_integr_type_w
			from dual;

			if (ie_order_integr_type_w = 'OI') then

				recommendation_json_pck.getCpoeIntegracaoRecom(:new.nr_sequencia, nr_entity_identifier_w);

				if (:OLD.DT_LIBERACAO IS NULL AND :NEW.DT_LIBERACAO IS NOT NULL) then

					call_bifrost_content('prescription.recommendation.order.request','recommendation_json_pck.get_recomm_message_clob(' || :new.nr_sequencia || ', ''NW'', ' || nr_entity_identifier_w || ')', :new.nm_usuario);

				elsif (:OLD.DT_LIB_SUSPENSAO IS NULL AND :NEW.DT_LIB_SUSPENSAO IS NOT NULL ) then

					call_bifrost_content('prescription.recommendation.order.request','recommendation_json_pck.get_recomm_message_clob(' || :new.nr_sequencia || ', ''CA'', ' || nr_entity_identifier_w || ')', :new.nm_usuario);

				end if;

			end if;

		end if;
	end;
  end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.gpt_rec_before_ins_upd
before insert or update ON TASY.CPOE_RECOMENDACAO for each row
declare
	dt_valid_prim_hor_w		date;

begin
    if (:new.hr_prim_horario is not null) and (:new.hr_prim_horario <> :old.hr_prim_horario) and (obter_funcao_ativa = 252) then
        begin
            dt_valid_prim_hor_w := to_date(to_char(sysdate, 'dd/mm/yyyy') || ' ' || :new.hr_prim_horario, 'dd/mm/yyyy hh24:mi');
        exception
            when others then
                wheb_mensagem_pck.exibir_mensagem_abort(1128915, 'HR_PRIM_HORARIO='||:new.hr_prim_horario);
        end;
    end if;
end;
/


ALTER TABLE TASY.CPOE_RECOMENDACAO ADD (
  CONSTRAINT CPOEREC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CPOE_RECOMENDACAO ADD (
  CONSTRAINT CPOEREC_GQAPPAC_FK 
 FOREIGN KEY (NR_SEQ_PEND_PAC_ACAO) 
 REFERENCES TASY.GQA_PEND_PAC_ACAO (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CPOEREC_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CPOEREC_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINATURA_SUSP) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT CPOEREC_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_TASASDI_FK3 
 FOREIGN KEY (NR_SEQ_ASSINATURA_ENF) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_TASASDI_FK4 
 FOREIGN KEY (NR_SEQ_ASSINATURA_FARM) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTAMENTO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT CPOEREC_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_TASASDI_FK5 
 FOREIGN KEY (NR_SEQ_ASSINATURA_SUSP_FARM) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_TASASDI_FK6 
 FOREIGN KEY (NR_SEQ_ASSINATURA_REV) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_NAISINS_FK 
 FOREIGN KEY (NR_SEQ_NAIS_INSURANCE) 
 REFERENCES TASY.NAIS_INSURANCE (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_CPOEORU_FK 
 FOREIGN KEY (NR_SEQ_CPOE_ORDER_UNIT) 
 REFERENCES TASY.CPOE_ORDER_UNIT (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_MOTSUPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_PRESCR (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CPOEREC_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT CPOEREC_TIPRECO_FK 
 FOREIGN KEY (CD_RECOMENDACAO) 
 REFERENCES TASY.TIPO_RECOMENDACAO (CD_TIPO_RECOMENDACAO),
  CONSTRAINT CPOEREC_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT CPOEREC_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.CPOE_RECOMENDACAO TO NIVEL_1;

