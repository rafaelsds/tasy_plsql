ALTER TABLE TASY.CIRURGIA_PARTICIPANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIRURGIA_PARTICIPANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIRURGIA_PARTICIPANTE
(
  NR_CIRURGIA                NUMBER(10)         NOT NULL,
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  IE_FUNCAO                  VARCHAR2(10 BYTE)  NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE),
  NM_PARTICIPANTE            VARCHAR2(60 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  DT_ENTRADA                 DATE,
  DT_SAIDA                   DATE,
  NR_SEQ_PROCEDIMENTO        NUMBER(10),
  NR_SEQ_INTERNO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  CD_ESPECIALIDADE           NUMBER(5),
  DS_STACK                   VARCHAR2(2000 BYTE),
  DT_LANCAMENTO_AUTOMATICO   DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          14M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIRPART_CIRURGI_FK_I ON TASY.CIRURGIA_PARTICIPANTE
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRPART_ESPMEDI_FK_I ON TASY.CIRURGIA_PARTICIPANTE
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRPART_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRPART_PESFISI_FK_I ON TASY.CIRURGIA_PARTICIPANTE
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIRPART_PK ON TASY.CIRURGIA_PARTICIPANTE
(NR_CIRURGIA, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRPART_PROPACI_FK_I ON TASY.CIRURGIA_PARTICIPANTE
(NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIRPART_TASASDI_FK_I ON TASY.CIRURGIA_PARTICIPANTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRPART_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CIRPART_TASASDI_FK2_I ON TASY.CIRURGIA_PARTICIPANTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CIRPART_TASASDI_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CIRPART_UK ON TASY.CIRURGIA_PARTICIPANTE
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.Cirurgia_Participante_Delete
BEFORE DELETE ON TASY.CIRURGIA_PARTICIPANTE FOR EACH ROW
DECLARE
ie_gerar_particip_w	varchar2(255);

BEGIN

ie_gerar_particip_w := Obter_Valor_Param_Usuario(872, 448, Obter_Perfil_Ativo, :new.nm_usuario, 0);

if	(ie_gerar_particip_w = 'S') and
	(wheb_usuario_pck.get_cd_funcao = 872) then

	if	(:old.cd_pessoa_fisica is not null) then

		Ajustar_Participante_Proc(
				'DELETE',
				:old.nr_cirurgia,
				:old.nr_sequencia,
				:old.ie_funcao,
				:old.cd_pessoa_fisica,
				:old.nm_participante,
				:old.nr_seq_procedimento,
				'');
	end if;
elsif (nvl(ie_gerar_particip_w,'X') <> 'U') then

	Ajustar_Participante_Proc(
			'DELETE',
			:old.nr_cirurgia,
			:old.nr_sequencia,
			:old.ie_funcao,
			:old.cd_pessoa_fisica,
			:old.nm_participante,
			:old.nr_seq_procedimento,
			'');
end if;
END;
/


CREATE OR REPLACE TRIGGER TASY.Cirurgia_Participante_Insert
AFTER INSERT ON TASY.CIRURGIA_PARTICIPANTE FOR EACH ROW
DECLARE

ds_origem_w		varchar2(1800);
ie_gerar_particip_w	varchar2(255);

BEGIN

ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);

ie_gerar_particip_w := Obter_Valor_Param_Usuario(872, 448, Obter_Perfil_Ativo, :new.nm_usuario, 0);

if	(ie_gerar_particip_w = 'S') and
	(wheb_usuario_pck.get_cd_funcao = 872) then

	if	(:new.cd_pessoa_fisica is not null) then
		Ajustar_Participante_Proc(
				'INSERT',
				:new.nr_cirurgia,
				:new.nr_sequencia,
				:new.ie_funcao,
				:new.cd_pessoa_fisica,
				:new.nm_participante,
				:new.nr_seq_procedimento,
				:new.nm_usuario);
	end if;

elsif (nvl(ie_gerar_particip_w,'X') <> 'U') then

	Ajustar_Participante_Proc(
			'INSERT',
			:new.nr_cirurgia,
			:new.nr_sequencia,
			:new.ie_funcao,
			:new.cd_pessoa_fisica,
			:new.nm_participante,
			:new.nr_seq_procedimento,
			:new.nm_usuario);
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.CIRURGIA_PARTICIPANTE_tp  after update ON TASY.CIRURGIA_PARTICIPANTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_CIRURGIA='||to_char(:old.NR_CIRURGIA)||'#@#@NR_SEQUENCIA='||to_char(:old.NR_SEQUENCIA);gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE,1,4000),substr(:new.CD_ESPECIALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE',ie_log_w,ds_w,'CIRURGIA_PARTICIPANTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.CIRURGIA_PARTICIPANTE_UPDATE
BEFORE UPDATE ON TASY.CIRURGIA_PARTICIPANTE FOR EACH ROW
declare

qt_reg_w					number(1);
ds_origem_w					varchar2(1800);
ie_gerar_particip_w			varchar2(255);
ie_gerar_particip_com_pf_w	varchar2(255);
ds_stack_w					varchar2(2000);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

ie_gerar_particip_com_pf_w	:= Obter_Valor_Param_Usuario(872, 448, Obter_Perfil_Ativo, :new.nm_usuario, 0);
ie_gerar_particip_w			:= Obter_Valor_Param_Usuario(872, 495, Obter_Perfil_Ativo, :new.nm_usuario, 0);

if	((ie_gerar_particip_w	= 'S') and
	(wheb_usuario_pck.get_cd_funcao	= 872)) or
	(wheb_usuario_pck.get_cd_funcao	<> 872)	then

    if	(ie_gerar_particip_com_pf_w = 'S') and
	    (wheb_usuario_pck.get_cd_funcao = 872) then

    	if	(:new.cd_pessoa_fisica is not null) and
			(nvl(:new.ie_situacao,'A') <> 'I') then

    		Ajustar_Participante_Proc(
    					'DELETE',
    					:old.nr_cirurgia,
    					:old.nr_sequencia,
    					:old.ie_funcao,
    					:old.cd_pessoa_fisica,
    					:old.nm_participante,
    					:old.nr_seq_procedimento,
    					'');
    		Ajustar_Participante_Proc(
    					'INSERT',
    					:new.nr_cirurgia,
    					:new.nr_sequencia,
    					:new.ie_funcao,
    					:new.cd_pessoa_fisica,
    					:new.nm_participante,
    					:new.nr_seq_procedimento,
    					:new.nm_usuario);
    	end if;
    else
		if	(nvl(:new.ie_situacao,'A') <> 'I') then
			Ajustar_Participante_Proc(
						'DELETE',
						:old.nr_cirurgia,
						:old.nr_sequencia,
						:old.ie_funcao,
						:old.cd_pessoa_fisica,
						:old.nm_participante,
						:old.nr_seq_procedimento,
						'');

			Ajustar_Participante_Proc(
						'INSERT',
						:new.nr_cirurgia,
						:new.nr_sequencia,
						:new.ie_funcao,
						:new.cd_pessoa_fisica,
						:new.nm_participante,
						:new.nr_seq_procedimento,
						:new.nm_usuario);
		end if;
    end if;
end if;

<<Final>>
ds_stack_w		:= substr(dbms_utility.format_call_stack,1,2000);
:new.ds_stack	:= ds_stack_w;
qt_reg_w		:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.Cirurgia_Partic_Before
before INSERT OR UPDATE ON TASY.CIRURGIA_PARTICIPANTE FOR EACH ROW
Declare
qt_reg_w			Number(15,0);
ie_medico_w		Varchar2(01);
ie_consiste_inicio_fim_w	Varchar2(1);
dt_inicio_real_w		date;
dt_termino_w		date;
ie_consiste_w		varchar2(1):= 'N';
dt_entrada_w		date;
dt_saida_w		date;
BEGIN
obter_param_usuario(900,127,obter_perfil_ativo,wheb_usuario_pck.get_nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_consiste_inicio_fim_w);
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.cd_pessoa_fisica is not null) then
	select	count(*)
	into	qt_reg_w
	from	medico
	where	cd_pessoa_fisica	= :new.cd_pessoa_fisica;
	select	nvl(max(ie_medico),'N')
	into	ie_medico_w
	from	funcao_medico
	where	cd_funcao		= :new.ie_funcao;
	if	(qt_reg_w = 0) and
		(ie_medico_w = 'S') then
		WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266220);
	end if;
	if	(:new.nm_participante is null) or (:new.cd_pessoa_fisica <> :old.cd_pessoa_fisica) then
		:new.nm_participante	:= obter_nome_pf(:new.cd_pessoa_fisica);
	end if;
	if (ie_consiste_inicio_fim_w = 'S') then
		select	nvl(dt_inicio_real,dt_inicio_prevista),
				nvl(dt_termino,sysdate)
		into	dt_inicio_real_w,
				dt_termino_w
		from 	cirurgia
		where	nr_cirurgia = :new.nr_cirurgia;
		select	nvl(:new.dt_entrada,sysdate),
				nvl(:new.dt_saida,sysdate)
		into	dt_entrada_w,
				dt_saida_w
		from 	dual;

		if	(dt_entrada_w < dt_inicio_real_w) or
			(dt_entrada_w > dt_termino_w) or
			(dt_saida_w < dt_inicio_real_w) or
			(dt_saida_w > dt_termino_w) then
			WHEB_MENSAGEM_PCK.Exibir_Mensagem_Abort(266221);
		end if ;
	end if;
end if;
<<Final>>
qt_reg_w	:= 0;
END;
/


CREATE OR REPLACE TRIGGER TASY.cirurgia_particip_pend_atual
after insert or update ON TASY.CIRURGIA_PARTICIPANTE for each row
declare

qt_reg_w  		number(1);
ie_tipo_w  		varchar2(10);
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
nr_atendimento_w		atendimento_paciente.nr_atendimento%type;
ie_libera_partic_w	varchar2(5);

begin
if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

select	nvl(max(ie_libera_partic),'N')
into		ie_libera_partic_w
from		parametro_medico
where		cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_partic_w = 'S') then
	if (:new.dt_liberacao is null) then
		ie_tipo_w := 'CP';
	elsif (:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XCP';
	end if;

	select max(c.nr_atendimento),
		max(c.cd_pessoa_fisica)
	into	nr_atendimento_w,
		cd_pessoa_fisica_w
	from	cirurgia c
	where	c.nr_cirurgia = :new.nr_cirurgia;


	if ((ie_tipo_w is not null) and (nvl(cd_pessoa_fisica_w,0) > 0)) then
		gerar_registro_pendente_pep(ie_tipo_w, :new.nr_seq_interno, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w := 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.CIRURGIA_PARTIC_AFTER_UPDATE
AFTER UPDATE ON TASY.CIRURGIA_PARTICIPANTE FOR EACH ROW
declare

ie_participante_carta_w	varchar2(1 char);
nr_seq_carta_w		carta_medica.nr_sequencia%type;
nr_seq_carta_mae_w	carta_medica.nr_seq_carta_mae%type;
ie_cirurgiao_w		funcao_medico.ie_cirurgiao%type;

BEGIN

	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

		select 	max(nr_sequencia),
				max(nr_seq_carta_mae)
		into	nr_seq_carta_w,
				nr_seq_carta_mae_w
		from 	carta_medica
		where 	nr_cirurgia = :old.nr_cirurgia;

		if (nr_seq_carta_w is not null) then

			select	nvl(max('S'), 'N')
			into	ie_participante_carta_w
			from 	participante_carta_medica a
			where 	nr_seq_carta = nr_seq_carta_w
			and	obter_pessoa_fisica_usuario(a.nm_usuario_resp, 'C') = :old.cd_pessoa_fisica;

			select	nvl(max(ie_cirurgiao),'N')
			into	ie_cirurgiao_w
			from 	funcao_medico
			where 	cd_funcao = :old.ie_funcao;

			 if	(ie_participante_carta_w <> 'S' and ie_cirurgiao_w = 'S') then
				insert into PARTICIPANTE_CARTA_MEDICA (
					nr_sequencia,
					nr_seq_carta,
					nr_seq_carta_mae,
					dt_atualizacao,
					dt_atualizacao_nrec,
					nm_usuario,
					nm_usuario_nrec,
					nm_usuario_resp,
					ie_deve_assinar
				) 	values (
					participante_carta_medica_seq.nextval,
					nr_seq_carta_w,
					nr_seq_carta_mae_w,
					sysdate,
					sysdate,
					:new.nm_usuario,
					:new.nm_usuario,
					obter_usuario_pessoa(:old.cd_pessoa_fisica),
					'S'
				);
			end if;
		end if;
	end if;
END;
/


ALTER TABLE TASY.CIRURGIA_PARTICIPANTE ADD (
  CONSTRAINT CIRPART_PK
 PRIMARY KEY
 (NR_CIRURGIA, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          5M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT CIRPART_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          4M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIRURGIA_PARTICIPANTE ADD (
  CONSTRAINT CIRPART_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT CIRPART_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT CIRPART_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CIRPART_PROPACI_FK 
 FOREIGN KEY (NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PROCEDIMENTO_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT CIRPART_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT CIRPART_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.CIRURGIA_PARTICIPANTE TO NIVEL_1;

