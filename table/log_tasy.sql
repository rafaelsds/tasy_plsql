DROP TABLE TASY.LOG_TASY CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_TASY
(
  DT_ATUALIZACAO      DATE,
  NM_USUARIO          VARCHAR2(15 BYTE),
  CD_LOG              NUMBER(5),
  DS_LOG              VARCHAR2(2000 BYTE),
  NR_SEQUENCIA        NUMBER(10),
  DS_UTC              VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO    VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO  VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          208M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOGTASY_I1 ON TASY.LOG_TASY
(DT_ATUALIZACAO, CD_LOG)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          72M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOGTASY_I3 ON TASY.LOG_TASY
(NM_USUARIO, DT_ATUALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          112K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LOG_TASY_ATUAL
BEFORE INSERT OR UPDATE ON TASY.LOG_TASY FOR EACH ROW
BEGIN
	IF (INSERTING AND (:NEW.NR_SEQUENCIA IS NULL)) THEN
		SELECT LOG_TASY_SEQ.NEXTVAL INTO :NEW.NR_SEQUENCIA FROM DUAL;
	END IF;
	:NEW.DS_UTC := OBTER_DATA_UTC(:NEW.DT_ATUALIZACAO,'HV');
	:NEW.DS_UTC_ATUALIZACAO := OBTER_DATA_UTC(SYSDATE,'HV');
	:NEW.IE_HORARIO_VERAO := OBTER_SE_HORARIO_VERAO(:NEW.DT_ATUALIZACAO);
END;
/


GRANT SELECT ON TASY.LOG_TASY TO NIVEL_1;

