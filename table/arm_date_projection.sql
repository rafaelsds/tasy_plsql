ALTER TABLE TASY.ARM_DATE_PROJECTION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ARM_DATE_PROJECTION CASCADE CONSTRAINTS;

CREATE TABLE TASY.ARM_DATE_PROJECTION
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ESTIMATED         DATE                     NOT NULL,
  QT_HOURS             NUMBER(10,2)             NOT NULL,
  QT_RESOURCES         NUMBER(10,2)             NOT NULL,
  QT_ESTIMATED         NUMBER(10,2),
  QT_REALIZED          NUMBER(10,2),
  NR_SEQ_RESULT        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ARMDTPRJ_ARMRES_FK_I ON TASY.ARM_DATE_PROJECTION
(NR_SEQ_RESULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ARMDTPRJ_PK ON TASY.ARM_DATE_PROJECTION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ARM_DATE_PROJECTION_tp  after update ON TASY.ARM_DATE_PROJECTION FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'ARM_DATE_PROJECTION',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ESTIMATED,1,500);gravar_log_alteracao(substr(:old.DT_ESTIMATED,1,4000),substr(:new.DT_ESTIMATED,1,4000),:new.nm_usuario,nr_seq_w,'DT_ESTIMATED',ie_log_w,ds_w,'ARM_DATE_PROJECTION',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_REALIZED,1,500);gravar_log_alteracao(substr(:old.QT_REALIZED,1,4000),substr(:new.QT_REALIZED,1,4000),:new.nm_usuario,nr_seq_w,'QT_REALIZED',ie_log_w,ds_w,'ARM_DATE_PROJECTION',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_RESULT,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_RESULT,1,4000),substr(:new.NR_SEQ_RESULT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_RESULT',ie_log_w,ds_w,'ARM_DATE_PROJECTION',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_ESTIMATED,1,500);gravar_log_alteracao(substr(:old.QT_ESTIMATED,1,4000),substr(:new.QT_ESTIMATED,1,4000),:new.nm_usuario,nr_seq_w,'QT_ESTIMATED',ie_log_w,ds_w,'ARM_DATE_PROJECTION',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ARM_DATE_PROJECTION ADD (
  CONSTRAINT ARMDTPRJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ARM_DATE_PROJECTION ADD (
  CONSTRAINT ARMDTPRJ_ARMRES_FK 
 FOREIGN KEY (NR_SEQ_RESULT) 
 REFERENCES TASY.ARM_RESULT (NR_SEQUENCIA));

GRANT SELECT ON TASY.ARM_DATE_PROJECTION TO NIVEL_1;

