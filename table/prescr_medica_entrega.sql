ALTER TABLE TASY.PRESCR_MEDICA_ENTREGA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_MEDICA_ENTREGA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_MEDICA_ENTREGA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_ENDERECO          VARCHAR2(40 BYTE),
  CD_CEP               VARCHAR2(15 BYTE),
  NR_ENDERECO          NUMBER(5),
  DS_COMPLEMENTO       VARCHAR2(40 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  DS_BAIRRO            VARCHAR2(40 BYTE),
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_MUNICIPIO         VARCHAR2(40 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  SG_ESTADO            VARCHAR2(15 BYTE),
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_TELEFONE          VARCHAR2(15 BYTE),
  NR_RAMAL             NUMBER(5),
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  DS_EMAIL             VARCHAR2(255 BYTE),
  CD_EMPRESA_REFER     NUMBER(10),
  NR_PRESCRICAO        NUMBER(14),
  CD_MUNICIPIO_IBGE    VARCHAR2(6 BYTE),
  DS_WEBSITE           VARCHAR2(255 BYTE),
  DS_FONE_ADIC         VARCHAR2(80 BYTE),
  DS_FAX               VARCHAR2(80 BYTE),
  CD_TIPO_LOGRADOURO   VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE      VARCHAR2(3 BYTE),
  NR_DDI_TELEFONE      VARCHAR2(3 BYTE),
  NR_DDI_FAX           VARCHAR2(3 BYTE),
  NR_DDD_FAX           VARCHAR2(3 BYTE),
  CD_ZONA_PROCEDENCIA  VARCHAR2(3 BYTE),
  NR_DDI_FONE_DIC      VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREMEEN_EMPREFE_FK_I ON TASY.PRESCR_MEDICA_ENTREGA
(CD_EMPRESA_REFER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMEEN_EMPREFE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREMEEN_PK ON TASY.PRESCR_MEDICA_ENTREGA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMEEN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PREMEEN_PRESMED_FK_I ON TASY.PRESCR_MEDICA_ENTREGA
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMEEN_PRESMED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMEEN_SUSMUNI_FK_I ON TASY.PRESCR_MEDICA_ENTREGA
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMEEN_SUSMUNI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMEEN_SUSTILO_FK_I ON TASY.PRESCR_MEDICA_ENTREGA
(CD_TIPO_LOGRADOURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMEEN_SUSTILO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PRESCR_MEDICA_ENTREGA_tp  after update ON TASY.PRESCR_MEDICA_ENTREGA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_LOGRADOURO,1,4000),substr(:new.CD_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOGRADOURO',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MUNICIPIO,1,4000),substr(:new.DS_MUNICIPIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_MUNICIPIO',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_RAMAL,1,4000),substr(:new.NR_RAMAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_RAMAL',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA_REFER,1,4000),substr(:new.CD_EMPRESA_REFER,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA_REFER',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FONE_ADIC,1,4000),substr(:new.DS_FONE_ADIC,1,4000),:new.nm_usuario,nr_seq_w,'DS_FONE_ADIC',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_FAX,1,4000),substr(:new.DS_FAX,1,4000),:new.nm_usuario,nr_seq_w,'DS_FAX',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'PRESCR_MEDICA_ENTREGA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PRESCR_MEDICA_ENTREGA ADD (
  CONSTRAINT PREMEEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_MEDICA_ENTREGA ADD (
  CONSTRAINT PREMEEN_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT PREMEEN_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_REFER) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT PREMEEN_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT PREMEEN_SUSTILO_FK 
 FOREIGN KEY (CD_TIPO_LOGRADOURO) 
 REFERENCES TASY.SUS_TIPO_LOGRADOURO (CD_TIPO_LOGRADOURO));

GRANT SELECT ON TASY.PRESCR_MEDICA_ENTREGA TO NIVEL_1;

