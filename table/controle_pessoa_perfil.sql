ALTER TABLE TASY.CONTROLE_PESSOA_PERFIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTROLE_PESSOA_PERFIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTROLE_PESSOA_PERFIL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_TIPO_CONTROLE     NUMBER(5)                NOT NULL,
  CD_PERFIL            NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COPESPER_PERFIL_FK_I ON TASY.CONTROLE_PESSOA_PERFIL
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COPESPER_PK ON TASY.CONTROLE_PESSOA_PERFIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPESPER_PK
  MONITORING USAGE;


CREATE INDEX TASY.COPESPER_TIPCOPE_FK_I ON TASY.CONTROLE_PESSOA_PERFIL
(CD_TIPO_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPESPER_TIPCOPE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.CONTROLE_PESSOA_PERFIL ADD (
  CONSTRAINT COPESPER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTROLE_PESSOA_PERFIL ADD (
  CONSTRAINT COPESPER_TIPCOPE_FK 
 FOREIGN KEY (CD_TIPO_CONTROLE) 
 REFERENCES TASY.TIPO_CONTROLE_PESSOA (CD_TIPO_CONTROLE),
  CONSTRAINT COPESPER_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.CONTROLE_PESSOA_PERFIL TO NIVEL_1;

