ALTER TABLE TASY.PLS_PLANO_ONLINE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_PLANO_ONLINE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_PLANO_ONLINE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_PLANO         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_GRUPO_PLANO   NUMBER(10),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  TX_COPARTICIPACAO    NUMBER(7,4),
  DS_COPARTICIPACAO    VARCHAR2(250 BYTE),
  IE_SEM_TAXA_COPART   VARCHAR2(1 BYTE),
  IE_TABELA_QT_VIDAS   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPLON_PK ON TASY.PLS_PLANO_ONLINE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPLON_PLSGPON_FK_I ON TASY.PLS_PLANO_ONLINE
(NR_SEQ_GRUPO_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSPLON_PLSPLAN_FK_I ON TASY.PLS_PLANO_ONLINE
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_plano_online_atual
before insert or update ON TASY.PLS_PLANO_ONLINE for each row
declare

ie_tipo_operacao_grupo_w	pls_grupo_plano_online.ie_tipo_operacao%type;
ie_tipo_operacao_plano_w	pls_plano.ie_tipo_operacao%type;
qt_registro_w	pls_integer;
qt_nr_seq_grupo_plano_w		number(1);

begin
select	ie_tipo_operacao
into	ie_tipo_operacao_plano_w
from	pls_plano
where	nr_sequencia	= :new.nr_seq_plano;

select	ie_tipo_operacao
into	ie_tipo_operacao_grupo_w
from	pls_grupo_plano_online
where	nr_sequencia	= :new.nr_seq_grupo_plano;

if	(inserting) and
	(ie_tipo_operacao_grupo_w = 'A') then
	select	count(1)
	into	qt_nr_seq_grupo_plano_w
	from	pls_plano_online
	where	nr_seq_grupo_plano = :new.nr_seq_grupo_plano;

	if	(qt_nr_seq_grupo_plano_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1084171);
	end if;
end if;

if	(ie_tipo_operacao_grupo_w is not null) and
	(ie_tipo_operacao_plano_w is not null) and
	(ie_tipo_operacao_grupo_w <> ie_tipo_operacao_plano_w) then
	wheb_mensagem_pck.exibir_mensagem_abort(1084154);
end if;

if	(updating) and
	(:new.nr_seq_plano <> :old.nr_seq_plano) then
	select	count(*)
	into	qt_registro_w
	from	pls_plano_tabela_online
	where	nr_seq_plano_online	= :new.nr_sequencia;

	if	(qt_registro_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1093684); --N�o � poss�vel alterar o produto, pois j� existe tabela de pre�o vinculada.
	end if;
end if;

select	count(*)
into	qt_registro_w
from	pls_plano
where	nr_sequencia	= :new.nr_seq_plano
and	ie_tipo_contratacao = 'I';

if	(qt_registro_w = 0) then
	wheb_mensagem_pck.exibir_mensagem_abort(1093685); --Somente � permitida a inclus�o de produtos com tipo de contrata��o Individual/Familiar
end if;

end;
/


ALTER TABLE TASY.PLS_PLANO_ONLINE ADD (
  CONSTRAINT PLSPLON_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_PLANO_ONLINE ADD (
  CONSTRAINT PLSPLON_PLSGPON_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PLANO) 
 REFERENCES TASY.PLS_GRUPO_PLANO_ONLINE (NR_SEQUENCIA),
  CONSTRAINT PLSPLON_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_PLANO_ONLINE TO NIVEL_1;

