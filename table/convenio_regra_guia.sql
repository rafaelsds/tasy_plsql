ALTER TABLE TASY.CONVENIO_REGRA_GUIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_REGRA_GUIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_REGRA_GUIA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_CONVENIO            NUMBER(5),
  IE_FORMA               VARCHAR2(15 BYTE),
  DS_ADICIONAL           VARCHAR2(20 BYTE),
  IE_TIPO_ATENDIMENTO    NUMBER(3),
  CD_ESTABELECIMENTO     NUMBER(4),
  NR_INICIAL             NUMBER(15)             NOT NULL,
  NR_FINAL               NUMBER(15)             NOT NULL,
  NR_ATUAL               NUMBER(15)             NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_TIPO_CONVENIO       NUMBER(2),
  DS_MASCARA             VARCHAR2(40 BYTE),
  CD_PERFIL_COMUNIC      NUMBER(5),
  NR_DOC_CONV_COMUNIC    VARCHAR2(20 BYTE),
  IE_TIPO_GUIA           VARCHAR2(2 BYTE),
  CD_PERFIL_FILTRO       NUMBER(5),
  IE_CLINICA             NUMBER(5),
  NR_SEQ_CLASSIFICACAO   NUMBER(10),
  IE_INATIVAR_REGRA      VARCHAR2(1 BYTE),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  CD_EMPRESA             NUMBER(4),
  IE_GERAR_PROCEDIMENTO  VARCHAR2(1 BYTE),
  CD_CATEGORIA           VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONREGU_CATCONV_FK_I ON TASY.CONVENIO_REGRA_GUIA
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CONREGU_CLAATEN_FK_I ON TASY.CONVENIO_REGRA_GUIA
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREGU_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREGU_CONVENI_FK_I ON TASY.CONVENIO_REGRA_GUIA
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREGU_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREGU_EMPRESA_FK_I ON TASY.CONVENIO_REGRA_GUIA
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREGU_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREGU_ESTABEL_FK_I ON TASY.CONVENIO_REGRA_GUIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREGU_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONREGU_PERFIL_FK_I ON TASY.CONVENIO_REGRA_GUIA
(CD_PERFIL_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREGU_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONREGU_PK ON TASY.CONVENIO_REGRA_GUIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREGU_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.CONVENIO_REGRA_GUIA_tp  after update ON TASY.CONVENIO_REGRA_GUIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_CLINICA,1,500);gravar_log_alteracao(substr(:old.IE_CLINICA,1,4000),substr(:new.IE_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLINICA',ie_log_w,ds_w,'CONVENIO_REGRA_GUIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.convenio_regra_guia_update
before update ON TASY.CONVENIO_REGRA_GUIA for each row
declare

begin
if	(:new.nr_atual	= :old.nr_final) and
	(nvl(:old.ie_inativar_regra ,'N') = 'S') then
	:new.ie_situacao := 'I';
end if;
end;
/


ALTER TABLE TASY.CONVENIO_REGRA_GUIA ADD (
  CONSTRAINT CONREGU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_REGRA_GUIA ADD (
  CONSTRAINT CONREGU_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT CONREGU_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT CONREGU_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT CONREGU_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE,
  CONSTRAINT CONREGU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CONREGU_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_FILTRO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.CONVENIO_REGRA_GUIA TO NIVEL_1;

