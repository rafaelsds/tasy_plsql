ALTER TABLE TASY.MAN_SERVICE_PACK_VERSAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_SERVICE_PACK_VERSAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_SERVICE_PACK_VERSAO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_VERSAO                 VARCHAR2(15 BYTE)   NOT NULL,
  NR_SERVICE_PACK           NUMBER(10)          NOT NULL,
  NR_OLD_BUILD              NUMBER(10)          NOT NULL,
  NR_PACOTE                 NUMBER(10),
  IE_STATUS_BUILD           VARCHAR2(1 BYTE),
  DS_HOTFIX                 VARCHAR2(25 BYTE),
  CD_CNPJ                   VARCHAR2(14 BYTE),
  NR_SERVICE_PACK_ORIG      NUMBER(10),
  IE_SITUACAO_SERVICE_PACK  VARCHAR2(1 BYTE),
  IE_RENAME_FOLDER_SP       VARCHAR2(1 BYTE),
  IE_REGULATORIO            VARCHAR2(1 BYTE),
  DT_LIBERACAO              DATE,
  IE_CLASSIFICACAO          VARCHAR2(1 BYTE),
  IE_VERIFICACAO            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MSPV_PK ON TASY.MAN_SERVICE_PACK_VERSAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_SERVICE_PACK_VERSAO ADD (
  CONSTRAINT MSPV_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_SERVICE_PACK_VERSAO TO NIVEL_1;

