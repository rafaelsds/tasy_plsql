ALTER TABLE TASY.LAB_SORO_CONTROL_TEMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LAB_SORO_CONTROL_TEMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.LAB_SORO_CONTROL_TEMP
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_TEMP_MAX            NUMBER(5,2),
  QT_TEMP_MIN            NUMBER(5,2),
  QT_TEMP_ATUAL          NUMBER(5,2),
  DT_COLETA_TEMP         DATE,
  NR_SEQ_ARMAZENAMENTO   NUMBER(10)             NOT NULL,
  CD_TERMOMETRO          VARCHAR2(100 BYTE),
  CD_PESSOA_FISICA_RESP  VARCHAR2(10 BYTE),
  NR_SEQ_TURNO           NUMBER(10)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LASOCONTEM_LASOARM_FK_I ON TASY.LAB_SORO_CONTROL_TEMP
(NR_SEQ_ARMAZENAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LASOCONTEM_LASOTURNO_FK_I ON TASY.LAB_SORO_CONTROL_TEMP
(NR_SEQ_TURNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LASOCONTEM_PK ON TASY.LAB_SORO_CONTROL_TEMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LAB_SORO_CONTROL_TEMP ADD (
  CONSTRAINT LASOCONTEM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LAB_SORO_CONTROL_TEMP ADD (
  CONSTRAINT LASOCONTEM_LASOARM_FK 
 FOREIGN KEY (NR_SEQ_ARMAZENAMENTO) 
 REFERENCES TASY.LAB_SORO_ARMAZENAMENTO (NR_SEQUENCIA),
  CONSTRAINT LASOCONTEM_LASOTURNO_FK 
 FOREIGN KEY (NR_SEQ_TURNO) 
 REFERENCES TASY.LAB_SORO_TURNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.LAB_SORO_CONTROL_TEMP TO NIVEL_1;

