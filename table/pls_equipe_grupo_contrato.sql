ALTER TABLE TASY.PLS_EQUIPE_GRUPO_CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_EQUIPE_GRUPO_CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_EQUIPE_GRUPO_CONTRATO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_EQUIPE               VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  QT_VIDAS_INICIAL        NUMBER(10),
  QT_VIDAS_FINAL          NUMBER(10),
  IE_TIPO_RELACIONAMENTO  VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSQUGC_ESTABEL_FK_I ON TASY.PLS_EQUIPE_GRUPO_CONTRATO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSQUGC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSQUGC_PK ON TASY.PLS_EQUIPE_GRUPO_CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSQUGC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_EQUIPE_GRUPO_CONTRATO_UPD
before update ON TASY.PLS_EQUIPE_GRUPO_CONTRATO for each row
declare
nr_seq_grupo_w	pls_grupo_contrato.nr_sequencia%type;

begin

if	(:new.ie_tipo_relacionamento is not null) then
	select	max(b.nr_sequencia)
	into	nr_seq_grupo_w
	from	pls_grupo_contrato_equipe a,
		pls_grupo_contrato b
	where	b.nr_sequencia	= a.nr_seq_grupo_contrato
	and	a.nr_seq_equipe	= :new.nr_sequencia
	and	b.ie_tipo_relacionamento <> :new.ie_tipo_relacionamento;

	if	(nr_seq_grupo_w is not null) then
		wheb_mensagem_pck.exibir_mensagem_abort(348193,'NR_SEQ_GRUPO='||nr_seq_grupo_w);
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_EQUIPE_GRUPO_CONTRATO ADD (
  CONSTRAINT PLSQUGC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_EQUIPE_GRUPO_CONTRATO ADD (
  CONSTRAINT PLSQUGC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_EQUIPE_GRUPO_CONTRATO TO NIVEL_1;

