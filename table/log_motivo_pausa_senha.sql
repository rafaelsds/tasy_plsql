ALTER TABLE TASY.LOG_MOTIVO_PAUSA_SENHA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOG_MOTIVO_PAUSA_SENHA CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_MOTIVO_PAUSA_SENHA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO        NUMBER(10)               NOT NULL,
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LMOTSTOPPA_MOTSTOPPAS_FK_I ON TASY.LOG_MOTIVO_PAUSA_SENHA
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LMOTSTOPPA_PK ON TASY.LOG_MOTIVO_PAUSA_SENHA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LOG_MOTIVO_PAUSA_SENHA ADD (
  CONSTRAINT LMOTSTOPPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.LOG_MOTIVO_PAUSA_SENHA ADD (
  CONSTRAINT LMOTSTOPPA_MOTSTOPPAS_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.MOTIVO_PAUSA_SENHA (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOG_MOTIVO_PAUSA_SENHA TO NIVEL_1;

