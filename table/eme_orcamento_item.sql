ALTER TABLE TASY.EME_ORCAMENTO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EME_ORCAMENTO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.EME_ORCAMENTO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORCAMENTO     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  QT_ORCADO            NUMBER(15,4)             NOT NULL,
  VL_ORCADO            NUMBER(15,2),
  CD_MATERIAL          NUMBER(6),
  CD_PROCEDIMENTO      NUMBER(15),
  IE_ORIGEM_PROCED     NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EMEREIT_EMEORCA_FK_I ON TASY.EME_ORCAMENTO_ITEM
(NR_SEQ_ORCAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMEREIT_EMEORCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EMEREIT_MATERIA_FK_I ON TASY.EME_ORCAMENTO_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMEREIT_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.EMEREIT_PK ON TASY.EME_ORCAMENTO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMEREIT_PK
  MONITORING USAGE;


CREATE INDEX TASY.EMEREIT_PROCEDI_FK_I ON TASY.EME_ORCAMENTO_ITEM
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EMEREIT_PROCEDI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.EME_ORCAMENTO_ITEM ADD (
  CONSTRAINT EMEREIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EME_ORCAMENTO_ITEM ADD (
  CONSTRAINT EMEREIT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT EMEREIT_EMEORCA_FK 
 FOREIGN KEY (NR_SEQ_ORCAMENTO) 
 REFERENCES TASY.EME_ORCAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EMEREIT_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.EME_ORCAMENTO_ITEM TO NIVEL_1;

