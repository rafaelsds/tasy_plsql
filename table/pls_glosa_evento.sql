ALTER TABLE TASY.PLS_GLOSA_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GLOSA_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GLOSA_EVENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_PLANO             VARCHAR2(1 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MOTIVO_GLOSA  NUMBER(10)               NOT NULL,
  IE_EVENTO            VARCHAR2(5 BYTE)         NOT NULL,
  IE_PLANO_VERSAO      VARCHAR2(1 BYTE),
  CD_MOTIVO_TISS       VARCHAR2(10 BYTE),
  IE_VAI_VERSAO        VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSGLEV_ESTABEL_FK_I ON TASY.PLS_GLOSA_EVENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSGLEV_PK ON TASY.PLS_GLOSA_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGLEV_TISSMGL_FK_I ON TASY.PLS_GLOSA_EVENTO
(NR_SEQ_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSGLEV_UK ON TASY.PLS_GLOSA_EVENTO
(NR_SEQ_MOTIVO_GLOSA, IE_EVENTO, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_GLOSA_EVENTO_DEL
before delete ON TASY.PLS_GLOSA_EVENTO for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Impede exclus�o do pls_glosa_evento_log se ie_plano_versao for 'S'
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare
ie_tipo_w	pls_glosa_evento_log.ie_tipo%type;
nm_usuario_w	usuario.nm_usuario%type;
begin
if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S')  then
	if(:old.ie_plano_versao = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(293877);
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_GLOSA_EVENTO_ATUAL
before insert or update ON TASY.PLS_GLOSA_EVENTO for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:  Gravar log da pls_glosa_evento
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ X ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [ ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

ie_tipo_w	pls_glosa_evento_log.ie_tipo%type;
nm_usuario_w	usuario.nm_usuario%type;
cd_motivo_tiss_w	varchar2(10);

begin
if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'S')  then
	if	(inserting) then
		ie_tipo_w	:= 'I';
		/* Quando inserir, sempre vir com o padr�o da vers�o */
		if	(:new.ie_plano_versao is not null) then
			:new.ie_plano 		:= :new.ie_plano_versao;

			if	(:new.cd_motivo_tiss is null) then
				select	max(cd_motivo_tiss)
				into	cd_motivo_tiss_w
				from	tiss_motivo_glosa a
				where	a.nr_sequencia	= :new.nr_seq_motivo_glosa;

				:new.cd_motivo_tiss	:= cd_motivo_tiss_w;
			end if;
		end if;
	elsif	(updating) then
		ie_tipo_w	:= 'A';
	end if;

	if	(wheb_usuario_pck.get_nm_usuario is null) then
		nm_usuario_w	:= 'banco de dados';
	else
		nm_usuario_w	:= wheb_usuario_pck.get_nm_usuario;
	end if;

	if	(updating) and
		(nvl(:old.ie_plano,'X') <> nvl(:new.ie_plano,'X')) then
		insert into pls_glosa_evento_log
			(nr_sequencia,
			ie_plano,
			dt_atualizacao,
			nm_usuario,
			dt_alteracao,
			nr_seq_motivo_glosa,
			ie_evento,
			cd_estabelecimento,
			ie_tipo)
		values 	(pls_glosa_evento_log_seq.nextval,
			:new.ie_plano,
			sysdate,
			nm_usuario_w,
			sysdate,
			:new.nr_seq_motivo_glosa,
			:new.ie_evento,
			:new.cd_estabelecimento,
			ie_tipo_w);
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_GLOSA_EVENTO_tp  after update ON TASY.PLS_GLOSA_EVENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_MOTIVO_TISS,1,500);gravar_log_alteracao(substr(:old.CD_MOTIVO_TISS,1,4000),substr(:new.CD_MOTIVO_TISS,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_TISS',ie_log_w,ds_w,'PLS_GLOSA_EVENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_GLOSA_EVENTO ADD (
  CONSTRAINT PLSGLEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PLSGLEV_UK
 UNIQUE (NR_SEQ_MOTIVO_GLOSA, IE_EVENTO, CD_ESTABELECIMENTO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GLOSA_EVENTO ADD (
  CONSTRAINT PLSGLEV_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSGLEV_TISSMGL_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_GLOSA) 
 REFERENCES TASY.TISS_MOTIVO_GLOSA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_GLOSA_EVENTO TO NIVEL_1;

