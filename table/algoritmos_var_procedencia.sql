ALTER TABLE TASY.ALGORITMOS_VAR_PROCEDENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ALGORITMOS_VAR_PROCEDENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ALGORITMOS_VAR_PROCEDENCIA
(
  NR_VARIAVEL          NUMBER(10)               NOT NULL,
  IE_VARIAVEL          VARCHAR2(3 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PROCEDENCIA       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.ALGVARPR_PK ON TASY.ALGORITMOS_VAR_PROCEDENCIA
(NR_VARIAVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ALGVARPR_PROCEDE_FK_I ON TASY.ALGORITMOS_VAR_PROCEDENCIA
(CD_PROCEDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ALGORITMOS_VAR_PROCEDENCIA ADD (
  CONSTRAINT ALGVARPR_PK
 PRIMARY KEY
 (NR_VARIAVEL));

ALTER TABLE TASY.ALGORITMOS_VAR_PROCEDENCIA ADD (
  CONSTRAINT ALGVARPR_PROCEDE_FK 
 FOREIGN KEY (CD_PROCEDENCIA) 
 REFERENCES TASY.PROCEDENCIA (CD_PROCEDENCIA));

GRANT SELECT ON TASY.ALGORITMOS_VAR_PROCEDENCIA TO NIVEL_1;

