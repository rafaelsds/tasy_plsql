ALTER TABLE TASY.CIRCULACAO_EXTRACORPOREA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CIRCULACAO_EXTRACORPOREA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CIRCULACAO_EXTRACORPOREA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  IE_APAP                    VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  QT_FLIXO_SANGUINEO         NUMBER(15),
  QT_ULTRA_FILTRACAO         NUMBER(15),
  QT_ROTACAO_BOMBA           NUMBER(15),
  QT_FLUXO_GAS               NUMBER(15),
  QT_FIO2                    NUMBER(15),
  QT_PRESSAO_ACESSO          NUMBER(15),
  QT_PRESSAO_PRE_MEMBRANA    NUMBER(15),
  QT_PRESSAO_POS_MEMBRANA    NUMBER(15),
  QT_TEMPERATURA             NUMBER(15,1),
  DT_REGISTRO                DATE,
  NR_HORA                    NUMBER(2),
  QT_FLUXO_SANGUINEO_MEMBR   NUMBER(15),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  QT_PRESSAO_TRANS_MEMBRANA  NUMBER(15),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CIEX_ATCONSPEPA_FK_I ON TASY.CIRCULACAO_EXTRACORPOREA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIEX_ATEPACI_FK_I ON TASY.CIRCULACAO_EXTRACORPOREA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIEX_I1 ON TASY.CIRCULACAO_EXTRACORPOREA
(NR_ATENDIMENTO, DT_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CIEX_PESFISI_FK_I ON TASY.CIRCULACAO_EXTRACORPOREA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CIEX_PK ON TASY.CIRCULACAO_EXTRACORPOREA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.circ_extra_pend_atual
after insert or update or delete ON TASY.CIRCULACAO_EXTRACORPOREA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_sinal_vital_w	varchar2(10);
cd_pessoa_fisica_w		varchar2(30);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

begin
	if	(inserting) or
			(updating) then

		select	max(ie_lib_sinal_vital)
		into	ie_lib_sinal_vital_w
		from	parametro_medico
		where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

		if	(nvl(ie_lib_sinal_vital_w,'N') = 'S') then
			if	(:new.dt_liberacao is null) then
				ie_tipo_w := 'SVCE';
			elsif	(:old.dt_liberacao is null) and
					(:new.dt_liberacao is not null) then
				ie_tipo_w := 'XSVCE';
			end if;

			select 	max(cd_pessoa_fisica)
			into	cd_pessoa_fisica_w
			from	atendimento_paciente
			where 	nr_atendimento	=	:new.nr_atendimento;

			begin
			if	(ie_tipo_w	is not null) then
				Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, :new.nr_atendimento, :new.nm_usuario);
			end if;
			exception
				when others then
				null;
			end;

		end if;

	elsif	(deleting) then

		delete	from pep_item_pendente
		where 	IE_TIPO_REGISTRO = 'SVCE'
		and	nr_seq_registro = :old.nr_sequencia
		and	nvl(IE_TIPO_PENDENCIA,'L') = 'L';

		commit;
	end if;

exception
when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.circ_extra_insert_update
before insert or update ON TASY.CIRCULACAO_EXTRACORPOREA for each row
declare

begin

:new.qt_pressao_trans_membrana := :new.qt_pressao_pre_membrana - :new.qt_pressao_pos_membrana;

end;
/


ALTER TABLE TASY.CIRCULACAO_EXTRACORPOREA ADD (
  CONSTRAINT CIEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CIRCULACAO_EXTRACORPOREA ADD (
  CONSTRAINT CIEX_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT CIEX_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CIEX_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.CIRCULACAO_EXTRACORPOREA TO NIVEL_1;

