ALTER TABLE TASY.MED_AVAL_DEPENDENCIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_AVAL_DEPENDENCIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_AVAL_DEPENDENCIA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_AVAL           NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  IE_BANHO              VARCHAR2(3 BYTE)        NOT NULL,
  IE_VESTIR             VARCHAR2(3 BYTE)        NOT NULL,
  IE_TOALETE            VARCHAR2(3 BYTE)        NOT NULL,
  IE_TRANSFERENCIA      VARCHAR2(3 BYTE)        NOT NULL,
  IE_CONTINENCIA        VARCHAR2(3 BYTE)        NOT NULL,
  IE_COMER              VARCHAR2(3 BYTE)        NOT NULL,
  IE_NIVEL_DEPENDENCIA  VARCHAR2(3 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDAVDE_MEDAVCP_FK_I ON TASY.MED_AVAL_DEPENDENCIA
(NR_SEQ_AVAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVDE_MEDAVCP_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MEDAVDE_PK ON TASY.MED_AVAL_DEPENDENCIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDAVDE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.MED_AVAL_DEPENDENCIA_INSERT
BEFORE INSERT or update ON TASY.MED_AVAL_DEPENDENCIA FOR EACH ROW
BEGIN

if	(:new.ie_banho	 <> '3') and
	(:new.ie_vestir	 = '1') and
	(:new.ie_toalete = '1') and
	(:new.ie_transferencia = '1') and
	(:new.ie_continencia = '1') and
	(:new.ie_comer <> '3') then
	:new.ie_nivel_dependencia	:= '1';
elsif	(:new.ie_banho	= '3') and
	(:new.ie_vestir <> '1') and
	(:new.ie_toalete <> '1') and
	(:new.ie_transferencia <> '1') and
	(:new.ie_continencia <> '1') and
	(:new.ie_comer	= '3') then
	:new.ie_nivel_dependencia	:= '6';
elsif	(:new.ie_banho	= '3') and
	(:new.ie_vestir <> '1') and
	(:new.ie_continencia <> '1') and
	(:new.ie_transferencia <> '1') and
	(((:new.ie_comer	= '3') and (:new.ie_toalete = '1')) or
	 ((:new.ie_comer <> '3') and (:new.ie_toalete <> '1'))) then
	:new.ie_nivel_dependencia	:= '5';
elsif	(:new.ie_banho	= '3') and
	(:new.ie_vestir <> '1') and
	(:new.ie_continencia <> '1') and
	(:new.ie_transferencia <> '1') and
	(((:new.ie_comer	= '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia = '1')) or
	 ((:new.ie_comer <> '3') and (:new.ie_toalete <> '1') and (:new.ie_transferencia = '1')) or
	 ((:new.ie_comer <> '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia <> '1'))) then
	:new.ie_nivel_dependencia	:= '4';
elsif	(:new.ie_banho	= '3') and
	(((:new.ie_comer	= '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia = '1') and
		(:new.ie_vestir = '1') and (:new.ie_continencia = '1')) or
	 ((:new.ie_comer	<> '3') and (:new.ie_toalete <> '1') and (:new.ie_transferencia = '1') and
		(:new.ie_vestir = '1') and (:new.ie_continencia = '1')) or
	 ((:new.ie_comer	<> '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia <> '1') and
		(:new.ie_vestir = '1') and (:new.ie_continencia = '1')) or
	 ((:new.ie_comer	<> '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia = '1') and
		(:new.ie_vestir <> '1') and (:new.ie_continencia = '1')) or
	 ((:new.ie_comer	<> '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia = '1') and
		(:new.ie_vestir = '1') and (:new.ie_continencia <> '1'))) then
	:new.ie_nivel_dependencia	:= '3';
elsif	(((:new.ie_comer	= '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia = '1') and
		(:new.ie_vestir = '1') and (:new.ie_continencia = '1') and (:new.ie_banho <> '3')) or
	 ((:new.ie_comer	<> '3') and (:new.ie_toalete <> '1') and (:new.ie_transferencia = '1') and
		(:new.ie_vestir = '1') and (:new.ie_continencia = '1') and (:new.ie_banho <> '3')) or
	 ((:new.ie_comer	<> '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia <> '1') and
		(:new.ie_vestir = '1') and (:new.ie_continencia = '1') and (:new.ie_banho <> '3')) or
	 ((:new.ie_comer	<> '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia = '1') and
		(:new.ie_vestir <> '1') and (:new.ie_continencia = '1') and (:new.ie_banho <> '3')) or
	 ((:new.ie_comer	<> '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia = '1') and
		(:new.ie_vestir = '1') and (:new.ie_continencia <> '1') and (:new.ie_banho <> '3')) or
	 ((:new.ie_comer	<> '3') and (:new.ie_toalete = '1') and (:new.ie_transferencia = '1') and
		(:new.ie_vestir = '1') and (:new.ie_continencia = '1') and (:new.ie_banho = '3'))) then
	:new.ie_nivel_dependencia	:= '2';
else	:new.ie_nivel_dependencia	:= '7';
end if;

END;
/


ALTER TABLE TASY.MED_AVAL_DEPENDENCIA ADD (
  CONSTRAINT MEDAVDE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_AVAL_DEPENDENCIA ADD (
  CONSTRAINT MEDAVDE_MEDAVCP_FK 
 FOREIGN KEY (NR_SEQ_AVAL) 
 REFERENCES TASY.MED_AVAL_COMPLETA_PAC (NR_SEQUENCIA));

GRANT SELECT ON TASY.MED_AVAL_DEPENDENCIA TO NIVEL_1;

