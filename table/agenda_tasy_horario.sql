ALTER TABLE TASY.AGENDA_TASY_HORARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_TASY_HORARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_TASY_HORARIO
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  NM_USUARIO_AGENDA  VARCHAR2(15 BYTE)          NOT NULL,
  IE_DIA_SEMANA      NUMBER(1)                  NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  HR_INICIO          VARCHAR2(5 BYTE)           NOT NULL,
  HR_FINAL           VARCHAR2(5 BYTE)           NOT NULL,
  QT_MIN_INTERVALO   NUMBER(3)                  NOT NULL,
  NR_SEQ_TIPO        NUMBER(10)                 NOT NULL,
  IE_RESTRITO        VARCHAR2(1 BYTE)           NOT NULL,
  DT_INICIO          DATE,
  DT_FINAL           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AGETAHO_PK ON TASY.AGENDA_TASY_HORARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGETAHO_TIPAGTA_FK_I ON TASY.AGENDA_TASY_HORARIO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGETAHO_TIPAGTA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_tasy_horario_atual
before insert or update ON TASY.AGENDA_TASY_HORARIO for each row
declare

begin

begin
	if	(:new.dt_inicio is not null) and ((:new.dt_inicio <> :old.dt_inicio) or (:old.hr_inicio is null)) then
		:new.hr_inicio := substr(date_as_varchar2(:new.dt_inicio, 'shortTime'), 1, 5);
	end if;

	if	(:new.dt_final is not null) and ((:new.dt_final <> :old.dt_final) or (:old.hr_final is null)) then
		:new.hr_final := substr(date_as_varchar2(:new.dt_final, 'shortTime'), 1, 5);
	end if;
exception
	when	others then
		null;
end;

begin
	if	(:new.hr_inicio is not null) and ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_inicio is null)) then
		:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio,'dd/mm/yyyy hh24:mi');
	end if;

	if	(:new.hr_final is not null) and ((:new.hr_final <> :old.hr_final) or (:old.dt_final is null)) then
		:new.dt_final := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_final,'dd/mm/yyyy hh24:mi');
	end if;

exception
	when 	others then
		null;
end;

end;
/


ALTER TABLE TASY.AGENDA_TASY_HORARIO ADD (
  CONSTRAINT AGETAHO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_TASY_HORARIO ADD (
  CONSTRAINT AGETAHO_TIPAGTA_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_AGENDA_TASY (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDA_TASY_HORARIO TO NIVEL_1;

