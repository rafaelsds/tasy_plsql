ALTER TABLE TASY.MPREV_TEXTO_RELAT_CONTEUDO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_TEXTO_RELAT_CONTEUDO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_TEXTO_RELAT_CONTEUDO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TEXTO_RELAT   NUMBER(10)               NOT NULL,
  NR_SEQ_ORDEM         NUMBER(5),
  DS_CONTEUDO          LONG                     NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRTRCO_MPRTREL_FK_I ON TASY.MPREV_TEXTO_RELAT_CONTEUDO
(NR_SEQ_TEXTO_RELAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRTRCO_PK ON TASY.MPREV_TEXTO_RELAT_CONTEUDO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MPREV_TEXTO_RELAT_CONTEUDO ADD (
  CONSTRAINT MPRTRCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_TEXTO_RELAT_CONTEUDO ADD (
  CONSTRAINT MPRTRCO_MPRTREL_FK 
 FOREIGN KEY (NR_SEQ_TEXTO_RELAT) 
 REFERENCES TASY.MPREV_TEXTO_RELAT (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_TEXTO_RELAT_CONTEUDO TO NIVEL_1;

