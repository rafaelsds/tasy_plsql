ALTER TABLE TASY.EVENTO_CIRURGIA_PACIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EVENTO_CIRURGIA_PACIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.EVENTO_CIRURGIA_PACIENTE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_EVENTO              NUMBER(10)         NOT NULL,
  NR_CIRURGIA                NUMBER(10),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_REGISTRO                DATE               NOT NULL,
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  DT_FINAL_EVENTO            DATE,
  NR_SEQ_PEPO                NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ATEN_PAC_UNID       NUMBER(10),
  NR_SEQ_ATEN_PAC_UNID_TC    NUMBER(10),
  NR_SEQ_INTERNO             NUMBER(10),
  DT_LIBERACAO               DATE,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  DT_LANCAMENTO_AUTOMATICO   DATE,
  NR_CIRURGIA_ORIGINAL       NUMBER(10),
  DS_STACK                   VARCHAR2(2000 BYTE),
  DT_INICIO_EVENTO           DATE,
  NR_SEQ_TOPOGRAFIA          NUMBER(10),
  NR_SEQ_TIPO_CLAMP          NUMBER(10),
  QT_PRESSAO                 NUMBER(5,1),
  IE_PROTOCOLO               VARCHAR2(1 BYTE),
  NR_PROTOCOLO_PEPO          NUMBER(10),
  CD_EVOLUCAO                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EVCIRPA_ATEPACU_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQ_ATEN_PAC_UNID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVCIRPA_ATEPACU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVCIRPA_ATEPACU_FK2_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQ_ATEN_PAC_UNID_TC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVCIRPA_ATEPACU_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.EVCIRPA_CIRURGI_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVCIRPA_CIRURGI_FK2_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_CIRURGIA_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVCIRPA_CIRURGI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.EVCIRPA_EVENCIR_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVCIRPA_EVOPACI_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVCIRPA_I1 ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_CIRURGIA, NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVCIRPA_PEPOCIR_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVCIRPA_PESFISI_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EVCIRPA_PK ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVCIRPA_TASASDI_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVCIRPA_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EVCIRPA_TASASDI_FK2_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVCIRPA_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.EVCIRPA_TIPOCLA_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQ_TIPO_CLAMP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVCIRPA_TOPDOR_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EVCIRPA_UNIATEN_FK_I ON TASY.EVENTO_CIRURGIA_PACIENTE
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EVCIRPA_UNIATEN_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.EVENTO_CIRURGIA_PAC_DELETE
after delete ON TASY.EVENTO_CIRURGIA_PACIENTE for each row
declare

ie_atual_Inicio_cirurgia_w		varchar2(1);
ie_atual_fim_cirurgia_w		varchar2(1);
ie_inicia_cirurgia_w			varchar2(1);
ie_finaliza_cirurgia_w		varchar2(1);
dt_entrada_unidade_w		date;
nr_atendimento_w			number(10,0);
ie_evento_painel_w	varchar2(15);
nr_seq_agenda_w		number(10);
nr_cirurgia_w		number(10);
ie_inicia_equipamento_w evento_cirurgia.ie_inicia_equipamento%type;
cd_perfil_w             perfil.cd_perfil%type   := wheb_usuario_pck.get_cd_perfil;
nm_usuario_w            usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type:= wheb_usuario_pck.get_cd_estabelecimento;
nr_seq_evento_eritel_w  number(10);
cd_unidade_basica_w     atend_paciente_unidade.cd_unidade_basica%type;
cd_unidade_compl_w      atend_paciente_unidade.cd_unidade_compl%type;
nr_seq_interno_w        atend_paciente_unidade.nr_seq_interno%type;
cd_setor_atendimento_w  atend_paciente_unidade.cd_setor_atendimento%type;



cursor c04 is
	select	nr_cirurgia
	from	cirurgia
	where	nr_seq_pepo = :old.nr_seq_pepo;

begin

obter_param_usuario(872, 534, cd_perfil_w, nm_usuario_w, cd_estabelecimento_w, nr_seq_evento_eritel_w);

ie_finaliza_cirurgia_w := 'N';

select 	max(ie_evento_painel),
         nvl(max(ie_inicia_equipamento),'N')
into	ie_evento_painel_w,
      ie_inicia_equipamento_w
from	evento_cirurgia
where	nr_sequencia 	= :old.nr_seq_evento;

if	(ie_evento_painel_w is not null) and (ie_evento_painel_w <> 'E') then

	select	max(nr_sequencia)
	into	nr_seq_agenda_w
	from	agenda_paciente
	where	nr_cirurgia = :old.nr_cirurgia;

	if	(nr_seq_agenda_w is not null) then
		begin
			gerar_dados_painel_cirurgia(ie_evento_painel_w,nr_seq_agenda_w,'IP',:old.nm_usuario,'N');
		exception
		when others then
			cd_estabelecimento_w := 0;
		end;
	end if;

	if	(:old.nr_seq_pepo is not null) and
		(:old.nr_cirurgia is null) then
		open c04;
		loop
		fetch c04 into
			nr_cirurgia_w;
		exit when c04%notfound;
			begin
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_agenda_w
			from	agenda_paciente
			where	nr_cirurgia = nr_cirurgia_w;
			if	(nr_seq_agenda_w > 0) then
				begin
					gerar_dados_painel_cirurgia(ie_evento_painel_w,nr_seq_agenda_w,'IP',:old.nm_usuario,'N');
				exception
				when others then
					cd_estabelecimento_w := 0;
				end;
			end if;
			end;
		end loop;
		close c04;
	end if;
end if;

if (:old.nr_sequencia is not null) then
   delete from  cir_paciente_atraso
   where nr_seq_evento = :old.nr_sequencia;
end if;

if (ie_inicia_equipamento_w = 'S') then
   update   equipamento_cirurgia
   set      dt_inicio      = null
   where    (((nr_cirurgia    = :old.nr_cirurgia) and (:old.nr_cirurgia is not null)) or
             ((nr_seq_pepo    = :old.nr_seq_pepo) and (:old.nr_seq_pepo is not null)))
   and      dt_inicio         = :old.dt_registro
   and      dt_liberacao      is null;
end if;

if (nvl(nr_seq_evento_eritel_w,0) > 0) and (nr_seq_evento_eritel_w = :old.nr_seq_evento) then
   begin
   if (nvl(:old.nr_cirurgia,0) > 0) then
      select   max(a.cd_setor_atendimento),
               max(a.cd_unidade_basica),
               max(a.cd_unidade_compl),
               max(a.nr_seq_interno),
               max(a.nr_atendimento)
      into     cd_setor_atendimento_w,
               cd_unidade_basica_w,
               cd_unidade_compl_w,
               nr_seq_interno_w,
               nr_atendimento_w
      from     atend_paciente_unidade a,
               cirurgia b
      where    a.NR_ATENDIMENTO     = b.NR_ATENDIMENTO
      and      a.DT_ENTRADA_UNIDADE = b.DT_ENTRADA_UNIDADE
      and      b.nr_cirurgia        = :old.nr_cirurgia;
   elsif (nvl(:old.nr_seq_pepo,0) > 0) then
      select   max(a.cd_setor_atendimento),
               max(a.cd_unidade_basica),
               max(a.cd_unidade_compl),
               max(a.nr_seq_interno),
               max(a.nr_atendimento)
      into     cd_setor_atendimento_w,
               cd_unidade_basica_w,
               cd_unidade_compl_w,
               nr_seq_interno_w,
               nr_atendimento_w
      from     atend_paciente_unidade a,
               cirurgia b
      where    a.NR_ATENDIMENTO     = b.NR_ATENDIMENTO
      and      a.DT_ENTRADA_UNIDADE = b.DT_ENTRADA_UNIDADE
      and      b.nr_seq_pepo        = :old.nr_seq_pepo;
   end if;
   if (nr_seq_interno_w is not null) then
      insere_w_integracao_eritel(cd_setor_atendimento_w,cd_unidade_basica_w,cd_unidade_compl_w,nr_atendimento_w,'S',nm_usuario_w,cd_estabelecimento_w);
   end if;
   exception
   when others then
      null;
   end;

end if;

begin
DELETE 	pep_item_pendente
WHERE  	ie_tipo_registro = 'TMOV'
AND 	nvl(ie_tipo_pendencia,'L')	 = 'L'
AND	   	nr_seq_registro  = :old.nr_sequencia;

exception
when others then
  null;
end;

end;
/


CREATE OR REPLACE TRIGGER TASY.EVENTO_CIRURGIA_PAC_BEFORE
before insert or update ON TASY.EVENTO_CIRURGIA_PACIENTE for each row
declare

qt_reg_w		number(1);
ie_Nao_Consiste_Alta_w	varchar2(1):='S';
ie_Permite_Evento_Futuro_w varchar2(1):='S';
nr_atendimento_w	number(10);
qt_min_retroativo_w number(4);
dt_alta_w		date;
nr_prescricao_w		number(14);
nr_prescr_mat_esp_w	number(14);
nr_sequencia_w		number(10);
nr_sequencia_ww		number(10);
nr_seq_proc_w		number(10);
nr_seq_estagio_w	number(10);
ie_gera_mat_autorizacao_w	varchar2(1);
ie_evento_painel_w	varchar2(15);
nr_seq_agenda_w		number(10);
nr_cirurgia_w		number(10);
ie_inicia_equipamento_w evento_cirurgia.ie_inicia_equipamento%type;
cd_perfil_w             perfil.cd_perfil%type   := wheb_usuario_pck.get_cd_perfil;
nm_usuario_w            usuario.nm_usuario%type := wheb_usuario_pck.get_nm_usuario;
cd_estabelecimento_w    estabelecimento.cd_estabelecimento%type:= wheb_usuario_pck.get_cd_estabelecimento;
nr_seq_evento_eritel_w  number(10);
cd_unidade_basica_w     atend_paciente_unidade.cd_unidade_basica%type;
cd_unidade_compl_w      atend_paciente_unidade.cd_unidade_compl%type;
nr_seq_interno_w        atend_paciente_unidade.nr_seq_interno%type;
cd_setor_atendimento_w  atend_paciente_unidade.cd_setor_atendimento%type;

cursor c02 is
	select	nr_sequencia
	from	prescr_material
	where	nr_prescricao 	= nr_prescricao_w
	and	qt_material 	> 0;

cursor c03 is
	select	a.nr_prescricao,
		b.nr_sequencia
	from	prescr_medica a,
		prescr_material b
	where	a.nr_prescricao = b.nr_prescricao
	and	a.nr_cirurgia 	= :new.nr_cirurgia
	and 	nvl(a.ie_tipo_prescr_cirur,0) <> 2
	and	b.qt_material 	> 0
	order by 1;

cursor c04 is
	select	nr_cirurgia
	from	cirurgia
	where	nr_seq_pepo = :old.nr_seq_pepo;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

Obter_Param_Usuario(872,212,  cd_perfil_w,nm_usuario_w,cd_estabelecimento_w,ie_Nao_Consiste_Alta_w);
obter_param_usuario(872, 534, cd_perfil_w,nm_usuario_w,cd_estabelecimento_w,nr_seq_evento_eritel_w);

select	nvl(max(qt_min_retroativo),0),
	nvl(max(ie_gera_mat_autorizacao),'N'),
   nvl(max(ie_inicia_equipamento),'N')
into	qt_min_retroativo_w,
	ie_gera_mat_autorizacao_w,
   ie_inicia_equipamento_w
from	evento_cirurgia
where	nr_sequencia = :new.nr_seq_evento;

if	(qt_min_retroativo_w > 0) and
	(:new.dt_registro < (sysdate-(qt_min_retroativo_w/1440))) then
	wheb_mensagem_pck.exibir_mensagem_abort(252039);
end if;

if	(ie_Nao_Consiste_Alta_w = 'N') then
	select	MAX(b.dt_alta)
	into	dt_alta_w
	from	cirurgia a,
		atendimento_paciente b
	where	a.nr_atendimento 	= b.nr_atendimento
	and	a.nr_cirurgia 		= :new.nr_cirurgia;
	if	(dt_alta_w is not null) and (:new.dt_registro > dt_alta_w) then
		wheb_mensagem_pck.exibir_mensagem_abort(236857);
		--Raaise_application_error(-20011,'N�o � poss�vel gerar o evento com data superior a data de alta do paciente! Par�metro [212]');
	end if;
end if;

if 	(:new.dt_registro > sysdate) then
	Obter_Param_Usuario(872,416,obter_perfil_ativo,:new.nm_usuario,0,ie_Permite_Evento_Futuro_w);
	if	(ie_Permite_Evento_Futuro_w = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(236858);
		--Raaise_application_error(-20011,'N�o � poss�vel gerar o evento com data superior a data atual! Par�metro [416]');
	end if;
end if;

if	(:old.ie_situacao = 'A') and (:new.ie_situacao = 'I') and (ie_gera_mat_autorizacao_w = 'S') then
	select	max(a.nr_prescricao)
	into	nr_prescr_mat_esp_w
	from	prescr_medica a
	where	a.nr_cirurgia 	= :new.nr_cirurgia
	and 	nvl(a.ie_tipo_prescr_cirur,0) <> 2;

	select	max(nr_prescricao)
	into	nr_prescricao_w
	from	cirurgia
	where	nr_cirurgia = :new.nr_cirurgia;


	select	max(nr_sequencia)
	into	nr_sequencia_w
	from	autorizacao_convenio
	where	nr_prescricao = nr_prescricao_w
	and	ie_tipo_autorizacao = 4;

	select	max(nr_sequencia)
	into	nr_sequencia_ww
	from	autorizacao_convenio
	where	nr_prescricao = nr_prescr_mat_esp_w
	and	ie_tipo_autorizacao = 4;

	select	max(nr_sequencia)
	into	nr_seq_proc_w
	from	autorizacao_convenio
	where	nr_prescricao = nr_prescricao_w
	and	ie_tipo_autorizacao = 3;


	if	(nr_sequencia_w is not null) or (nr_sequencia_ww is not null) or (nr_seq_proc_w is not null) then
		select	max(nr_sequencia)
		into	nr_seq_estagio_w
		from	estagio_autorizacao
		where	ie_situacao = 'A'
		and	ie_interno  = 70
		and	OBTER_EMPRESA_ESTAB(wheb_usuario_pck.get_cd_estabelecimento) = cd_empresa;

		if	(nr_sequencia_w is not null) then
			atualizar_autorizacao_convenio(nr_sequencia_w, :new.nm_usuario, nr_seq_estagio_w, 'N', 'N', 'N');
		end if;

		if	(nr_sequencia_ww is not null) then
			atualizar_autorizacao_convenio(nr_sequencia_ww, :new.nm_usuario, nr_seq_estagio_w, 'N', 'N', 'N');
		end if;

		if	(nr_seq_proc_w is not null) then
			atualizar_autorizacao_convenio(nr_seq_proc_w, :new.nm_usuario, nr_seq_estagio_w, 'N', 'N', 'N');
		end if;

	end if;
end if;

select 	max(ie_evento_painel)
into	ie_evento_painel_w
from	evento_cirurgia
where	nr_sequencia 	= :old.nr_seq_evento;

if	(:old.dt_inativacao is null) and (:new.dt_inativacao is not null) and
	(ie_evento_painel_w is not null) and (ie_evento_painel_w <> 'E') then
	select	max(nr_sequencia)
	into	nr_seq_agenda_w
	from	agenda_paciente
	where	nr_cirurgia = :old.nr_cirurgia;

	if	(nr_seq_agenda_w is not null) then
		begin
			gerar_dados_painel_cirurgia(ie_evento_painel_w,nr_seq_agenda_w,'IP',:old.nm_usuario,'N');
		exception
		when others then
			qt_reg_w := 0;
		end;
	end if;

	if	(:old.nr_seq_pepo is not null) and
		(:old.nr_cirurgia is null) then
		open c04;
		loop
		fetch c04 into
			nr_cirurgia_w;
		exit when c04%notfound;
			begin
			select	nvl(max(nr_sequencia),0)
			into	nr_seq_agenda_w
			from	agenda_paciente
			where	nr_cirurgia = nr_cirurgia_w;
			if	(nr_seq_agenda_w > 0) then
				begin
					gerar_dados_painel_cirurgia(ie_evento_painel_w,nr_seq_agenda_w,'IP',:old.nm_usuario,'N');
				exception
				when others then
					qt_reg_w := 0;
				end;
			end if;
			end;
		end loop;
		close c04;
	end if;
end if;


if	(:old.ie_situacao = 'A') and
   (:new.ie_situacao = 'I') and
   (:old.nr_sequencia is not null) then
   delete from  cir_paciente_atraso
   where nr_seq_evento = :old.nr_sequencia;

   begin
   if (nvl(nr_seq_evento_eritel_w,0) > 0) and (nr_seq_evento_eritel_w = :old.nr_seq_evento) then
      if (nvl(:new.nr_cirurgia,0) > 0) then
         select   max(a.cd_setor_atendimento),
                  max(a.cd_unidade_basica),
                  max(a.cd_unidade_compl),
                  max(a.nr_seq_interno),
                  max(a.nr_atendimento)
         into     cd_setor_atendimento_w,
                  cd_unidade_basica_w,
                  cd_unidade_compl_w,
                  nr_seq_interno_w,
                  nr_atendimento_w
         from     atend_paciente_unidade a,
                  cirurgia b
         where    a.NR_ATENDIMENTO     = b.NR_ATENDIMENTO
         and      a.DT_ENTRADA_UNIDADE = b.DT_ENTRADA_UNIDADE
         and      b.nr_cirurgia        = :new.nr_cirurgia;
      elsif (nvl(:new.nr_seq_pepo,0) > 0) then
         select   max(a.cd_setor_atendimento),
                  max(a.cd_unidade_basica),
                  max(a.cd_unidade_compl),
                  max(a.nr_seq_interno),
                  max(a.nr_atendimento)
         into     cd_setor_atendimento_w,
                  cd_unidade_basica_w,
                  cd_unidade_compl_w,
                  nr_seq_interno_w,
                  nr_atendimento_w
         from     atend_paciente_unidade a,
                  cirurgia b
         where    a.NR_ATENDIMENTO     = b.NR_ATENDIMENTO
         and      a.DT_ENTRADA_UNIDADE = b.DT_ENTRADA_UNIDADE
         and      b.nr_seq_pepo        = :new.nr_seq_pepo;
      end if;
      if (nr_seq_interno_w is not null) then
         insere_w_integracao_eritel(cd_setor_atendimento_w,cd_unidade_basica_w,cd_unidade_compl_w,nr_atendimento_w,'S',nm_usuario_w,cd_estabelecimento_w);
      end if;
   end if;
   exception
   when others then
      null;
   end;
end if;

if (ie_inicia_equipamento_w = 'S') then
   if (:new.dt_inativacao is not null) then
      update   equipamento_cirurgia
      set      dt_inicio      = null
      where    (((nr_cirurgia    = :new.nr_cirurgia) and (:new.nr_cirurgia is not null)) or
                ((nr_seq_pepo    = :new.nr_seq_pepo) and (:new.nr_seq_pepo is not null)))
      and      dt_inicio         = :new.dt_registro
      and      dt_liberacao      is null;
   else
      if (:old.dt_registro is null) then
         update   equipamento_cirurgia
         set      dt_inicio      = :new.dt_registro
         where    (((nr_cirurgia    = :new.nr_cirurgia) and (:new.nr_cirurgia is not null)) or
                   ((nr_seq_pepo    = :new.nr_seq_pepo) and (:new.nr_seq_pepo is not null)))
         and      dt_inicio      is null
         and      dt_liberacao   is null;
      else
         update   equipamento_cirurgia
         set      dt_inicio         = :new.dt_registro
         where    (((nr_cirurgia    = :new.nr_cirurgia) and (:new.nr_cirurgia is not null)) or
                   ((nr_seq_pepo    = :new.nr_seq_pepo) and (:new.nr_seq_pepo is not null)))
         and      dt_inicio         = :old.dt_registro
         and      dt_liberacao      is null;
      end if;
   end if;
end if;

:new.ds_stack := substr(dbms_utility.format_call_stack,1,2000);

<<Final>>
qt_reg_w	:= 0;


end;
/


CREATE OR REPLACE TRIGGER TASY.EVENTO_CIRURGIA_PACIENTE_tp  after update ON TASY.EVENTO_CIRURGIA_PACIENTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_CIRURGIA,1,4000),substr(:new.NR_CIRURGIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_CIRURGIA',ie_log_w,ds_w,'EVENTO_CIRURGIA_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'EVENTO_CIRURGIA_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_REGISTRO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_REGISTRO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_REGISTRO',ie_log_w,ds_w,'EVENTO_CIRURGIA_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PEPO,1,4000),substr(:new.NR_SEQ_PEPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PEPO',ie_log_w,ds_w,'EVENTO_CIRURGIA_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_EVENTO,1,4000),substr(:new.NR_SEQ_EVENTO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EVENTO',ie_log_w,ds_w,'EVENTO_CIRURGIA_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FINAL_EVENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FINAL_EVENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FINAL_EVENTO',ie_log_w,ds_w,'EVENTO_CIRURGIA_PACIENTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROFISSIONAL,1,4000),substr(:new.CD_PROFISSIONAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROFISSIONAL',ie_log_w,ds_w,'EVENTO_CIRURGIA_PACIENTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.HSJ_EVENTO_CIRURGIA_PAC_BEFORE
before insert or update ON TASY.EVENTO_CIRURGIA_PACIENTE for each row
declare

qt_reg_w        number(1);
dt_entrada_w    date;

begin

if(wheb_usuario_pck.get_ie_executar_trigger    = 'N')  then
    goto Final;
end if;

if :new.dt_registro is not null then

    select MAX(b.dt_entrada)
    into dt_entrada_w
    from cirurgia a,
        atendimento_paciente b
    where a.nr_atendimento = b.nr_atendimento
    and a.nr_cirurgia = :new.nr_cirurgia;
    
    
    if(dt_entrada_w is not null) and (:new.dt_registro < dt_entrada_w) then
        hsj_gerar_log('N�o � poss�vel gerar o evento com data inferior a data de entrada do atendimento!');
        --Raaise_application_error(-20011,'N�o � poss�vel gerar o evento com data superior a data de alta do paciente! Par�metro [212]');
    end if;
    
end if;


<<Final>>
qt_reg_w    := 0;

end;
/


CREATE OR REPLACE TRIGGER TASY.EVENTO_CIRURGIA_PAC_INSERT
after insert or update ON TASY.EVENTO_CIRURGIA_PACIENTE for each row
declare

ie_evento_sac_w			number(3,0);
qt_reg_w				number(1);
ie_momento_integracao_w	varchar2(15);
nr_atendimento_w		number(10);
cd_setor_atendimento_w 	number(5);
ie_inicia_integracao_w	varchar2(1);
ie_finaliza_integracao_w varchar2(1);
ie_tipo_w				varchar2(10);
ie_gera_cir_pepo_w		varchar2(1);
cd_pessoa_fisica_w	pessoa_fisica.cd_pessoa_fisica%type;
ie_libera_evento_pepo_w	varchar2(5);

begin

obter_param_usuario(872, 158, obter_perfil_ativo, :new.nm_usuario, obter_estabelecimento_ativo, ie_gera_cir_pepo_w);

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
select	max(ie_evento_sac)
into	ie_evento_sac_w
from	evento_cirurgia
where	nr_sequencia	=	:new.nr_seq_evento;

if	(ie_evento_sac_w = 1) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set dt_preparacao = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_preparacao = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 2) then
	update cirurgia set dt_chamada = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
elsif	(ie_evento_sac_w = 3) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set dt_chegada_sala = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_chegada_sala = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 4) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set dt_chegada_anestesista = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_chegada_anestesista = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 5) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set dt_inicio_anestesia = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_inicio_anestesia	= :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 6) then
	update cirurgia set dt_chegada_cirurgiao= :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
elsif	(ie_evento_sac_w = 7) then
	update cirurgia set dt_fim_cirurgia = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
elsif	(ie_evento_sac_w = 7) then
	update cirurgia set dt_fim_cirurgia = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
elsif	(ie_evento_sac_w = 8) then
	update cirurgia set dt_inicio_cirurgia = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
elsif	(ie_evento_sac_w = 9) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set	dt_liberacao_sala  = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_liberacao_sala = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 10) then
	update cirurgia set dt_fim_extubacao = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
elsif	(ie_evento_sac_w = 11) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set dt_entrada_recup = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_entrada_recup = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 12) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set dt_saida_recup = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_saida_recup = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 13) then
	update cirurgia set dt_chegada_srpa = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
elsif	(ie_evento_sac_w = 14) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set dt_saida_sala = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_saida_sala = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 15) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set	dt_liberacao_sala  = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_liberacao_sala = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 16) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set dt_fim_anestesia = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_fim_anestesia = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;
elsif	(ie_evento_sac_w = 17) then
	if (ie_gera_cir_pepo_w = 'S' and nvl(:new.nr_seq_pepo,0) > 0) then
		update cirurgia set dt_saida_recup_prev = :new.dt_registro where nr_seq_pepo = :new.nr_seq_pepo;
	else
		update cirurgia set dt_saida_recup_prev = :new.dt_registro where nr_cirurgia = :new.nr_cirurgia;
	end if;

end if;

if (:new.nr_seq_aten_pac_unid > 0) and
   ((:new.nr_cirurgia > 0) or (:new.nr_seq_pepo > 0)) and
	(nvl(:old.nr_seq_aten_pac_unid,0) <> nvl(:new.nr_seq_aten_pac_unid,0)) then

	select	nvl(max(ie_momento_integracao),'IF')
	into	ie_momento_integracao_w
	from	parametros_pepo
	where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

	if	(ie_momento_integracao_w = 'TM') then

		select	nvl(max(ie_inicia_integracao),'N'),
				nvl(max(ie_finaliza_integracao),'N')
		into	ie_inicia_integracao_w,
				ie_finaliza_integracao_w
		from	evento_cirurgia
		where	nr_sequencia = :new.nr_seq_evento;


		if (:new.nr_cirurgia > 0) then

			select	max(nr_atendimento)
			into	nr_atendimento_w
			from 	cirurgia
			where   nr_cirurgia = :new.nr_cirurgia;

		elsif (:new.nr_seq_pepo > 0) then

			select	max(nr_atendimento)
			into	nr_atendimento_w
			from 	cirurgia
			where   nr_seq_pepo = :new.nr_seq_pepo;

		end if;


		select	max(cd_setor_atendimento)
		into	cd_setor_atendimento_w
		from	atend_paciente_unidade
		where	nr_seq_interno = :new.nr_seq_aten_pac_unid;

		if  (nr_atendimento_w > 0) and
			(cd_setor_atendimento_w > 0) then

			if	(ie_inicia_integracao_w = 'S') then

				gerar_cirurgia_hl7(nr_atendimento_w,:new.nr_seq_aten_pac_unid,cd_setor_atendimento_w,'I');
			end if;

			if	(ie_finaliza_integracao_w = 'S') then

				gerar_cirurgia_hl7(nr_atendimento_w,:new.nr_seq_aten_pac_unid,cd_setor_atendimento_w,'F');

			end if;

		end if;

	end if;

end if;

select	nvl(max(ie_libera_evento_pepo),'N')
into	ie_libera_evento_pepo_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_evento_pepo_w = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'TMOV';
	elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XTMOV';
	end if;

	if	(ie_tipo_w	is not null) then
		select	max(c.nr_atendimento),
				max(c.cd_pessoa_fisica)
		into	nr_atendimento_w,
				cd_pessoa_fisica_w
		from	cirurgia c
		where	c.nr_cirurgia = :new.nr_cirurgia;

	if 	(cd_pessoa_fisica_w is null) then
		select	max(c.nr_atendimento),
						max(c.cd_pessoa_fisica)
		into	   nr_atendimento_w,
						cd_pessoa_fisica_w
		from	   pepo_cirurgia c
		where	c.nr_sequencia = :new.nr_seq_pepo;
	end if;

		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.EVENTO_CIRURGIA_PACIENTE ADD (
  CONSTRAINT EVCIRPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EVENTO_CIRURGIA_PACIENTE ADD (
  CONSTRAINT EVCIRPA_TIPOCLA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CLAMP) 
 REFERENCES TASY.TIPO_CLAMPEAMENTO (NR_SEQUENCIA),
  CONSTRAINT EVCIRPA_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT EVCIRPA_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT EVCIRPA_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT EVCIRPA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT EVCIRPA_EVENCIR_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.EVENTO_CIRURGIA (NR_SEQUENCIA),
  CONSTRAINT EVCIRPA_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT EVCIRPA_ATEPACU_FK 
 FOREIGN KEY (NR_SEQ_ATEN_PAC_UNID) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_SEQ_INTERNO)
    ON DELETE CASCADE,
  CONSTRAINT EVCIRPA_ATEPACU_FK2 
 FOREIGN KEY (NR_SEQ_ATEN_PAC_UNID_TC) 
 REFERENCES TASY.ATEND_PACIENTE_UNIDADE (NR_SEQ_INTERNO),
  CONSTRAINT EVCIRPA_UNIATEN_FK 
 FOREIGN KEY (NR_SEQ_INTERNO) 
 REFERENCES TASY.UNIDADE_ATENDIMENTO (NR_SEQ_INTERNO),
  CONSTRAINT EVCIRPA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT EVCIRPA_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT EVCIRPA_CIRURGI_FK2 
 FOREIGN KEY (NR_CIRURGIA_ORIGINAL) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA));

GRANT SELECT ON TASY.EVENTO_CIRURGIA_PACIENTE TO NIVEL_1;

