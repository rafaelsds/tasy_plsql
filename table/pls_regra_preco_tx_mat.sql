ALTER TABLE TASY.PLS_REGRA_PRECO_TX_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_PRECO_TX_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_PRECO_TX_MAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  QT_EXEC_MIN          NUMBER(9,3)              NOT NULL,
  TX_MATERIAL          NUMBER(15,4),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_EXEC_MAX          NUMBER(9,3),
  NR_SEQ_TAXA_ITEM     NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRPTM_PK ON TASY.PLS_REGRA_PRECO_TX_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPTM_PLSRPQM_FK_I ON TASY.PLS_REGRA_PRECO_TX_MAT
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRPTM_PLSTAIT_FK_I ON TASY.PLS_REGRA_PRECO_TX_MAT
(NR_SEQ_TAXA_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPTM_PLSTAIT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_PRECO_TX_MAT_tp  after update ON TASY.PLS_REGRA_PRECO_TX_MAT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_REGRA,1,4000),substr(:new.NR_SEQ_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TAXA_ITEM,1,4000),substr(:new.NR_SEQ_TAXA_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TAXA_ITEM',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_EXEC_MIN,1,4000),substr(:new.QT_EXEC_MIN,1,4000),:new.nm_usuario,nr_seq_w,'QT_EXEC_MIN',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.TX_MATERIAL,1,4000),substr(:new.TX_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'TX_MATERIAL',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_MAT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_EXEC_MAX,1,4000),substr(:new.QT_EXEC_MAX,1,4000),:new.nm_usuario,nr_seq_w,'QT_EXEC_MAX',ie_log_w,ds_w,'PLS_REGRA_PRECO_TX_MAT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_PRECO_TX_MAT ADD (
  CONSTRAINT PLSRPTM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_PRECO_TX_MAT ADD (
  CONSTRAINT PLSRPTM_PLSRPQM_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_REGRA_PRECO_QTDE_MAT (NR_SEQUENCIA),
  CONSTRAINT PLSRPTM_PLSTAIT_FK 
 FOREIGN KEY (NR_SEQ_TAXA_ITEM) 
 REFERENCES TASY.PLS_TAXA_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_PRECO_TX_MAT TO NIVEL_1;

