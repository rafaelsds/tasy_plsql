ALTER TABLE TASY.ADEP_IRRIGACAO_ETAPA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ADEP_IRRIGACAO_ETAPA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ADEP_IRRIGACAO_ETAPA
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_IRRIGACAO      NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DT_REGISTRO           DATE                    NOT NULL,
  QT_INFUSAO            NUMBER(15),
  QT_DRENAGEM           NUMBER(15),
  NR_SEQ_ASPECTO        NUMBER(10),
  DS_OBSERVACAO         VARCHAR2(4000 BYTE),
  CD_PESSOA_FISICA      VARCHAR2(10 BYTE)       NOT NULL,
  IE_EVENTO_VALIDO      VARCHAR2(1 BYTE)        NOT NULL,
  IE_EVENTO             VARCHAR2(15 BYTE)       NOT NULL,
  IE_IRRIGACAO_LAVAGEM  VARCHAR2(1 BYTE),
  NR_SEQ_HORARIO        NUMBER(10),
  NR_SEQ_MOTIVO         NUMBER(10),
  QT_VOL_INSTALADO      NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ADEPIRE_ADEMOIN_FK_I ON TASY.ADEP_IRRIGACAO_ETAPA
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPIRE_ADEMOIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPIRE_ADEPASIR_FK_I ON TASY.ADEP_IRRIGACAO_ETAPA
(NR_SEQ_ASPECTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPIRE_ADEPASIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ADEPIRE_ADEPIRI_FK_I ON TASY.ADEP_IRRIGACAO_ETAPA
(NR_SEQ_IRRIGACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ADEPIRE_PESFISI_FK_I ON TASY.ADEP_IRRIGACAO_ETAPA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ADEPIRE_PK ON TASY.ADEP_IRRIGACAO_ETAPA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPIRE_PK
  MONITORING USAGE;


CREATE INDEX TASY.ADEPIRE_PREPROHO_FK_I ON TASY.ADEP_IRRIGACAO_ETAPA
(NR_SEQ_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ADEPIRE_PREPROHO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.adep_irrigacao_etapa_insert
after INSERT ON TASY.ADEP_IRRIGACAO_ETAPA FOR EACH ROW
DECLARE

cd_setor_atendimento_w		number(15,0);
nr_seq_atend_w			number(15,0);
nr_seq_tipo_w			number(15,0);
nr_prescricao_w			number(14,0);
nr_atendimento_w		number(10,0);
ie_perda_ganho_w		varchar2(5);
ie_evento_w				prescr_ivc_perda_ganho.ie_evento%type;
qt_volume_real_w		number(15);
qt_volume_real_ww		number(15);

Cursor C01 is
select	nr_seq_tipo,
		ie_evento
from	prescr_ivc_perda_ganho
where	ie_evento		= :new.ie_evento
union
select	nr_seq_tipo,
		ie_evento
from	prescr_ivc_perda_ganho
where	:new.ie_evento = 'R'
and		ie_evento in ('RBP', 'RBG');

BEGIN

select	max(a.nr_atendimento),
		max(a.nr_prescricao)
into	nr_atendimento_w,
		nr_prescricao_w
from	adep_irrigacao 	a
where	a.nr_sequencia = :new.nr_seq_irrigacao;

select	max(cd_setor_atendimento)
into	cd_setor_atendimento_w
from	prescr_medica
where	nr_prescricao	= nr_prescricao_w;

if	(cd_setor_atendimento_w is null) then
	begin
	select	Obter_Setor_Atendimento(nr_atendimento_w)
	into	cd_setor_atendimento_w
	from dual;
	end;
end if;

qt_volume_real_w	:= nvl(:new.qt_infusao,0) - nvl(:new.qt_drenagem,0);

open C01;
loop
fetch C01 into
	nr_seq_tipo_w,
	ie_evento_w;
exit when C01%notfound;
	begin

	if	(nr_seq_tipo_w is not null) then

		select	max(b.ie_perda_ganho)
		into	ie_perda_ganho_w
		from	tipo_perda_ganho a,
				grupo_perda_ganho b
		where	a.ie_situacao = 'A'
		and		b.ie_situacao = 'A'
		and		b.nr_sequencia = a.nr_seq_grupo
		and		a.nr_sequencia	= nr_seq_tipo_w;

		select	atendimento_perda_ganho_seq.nextval
		into	nr_seq_atend_w
		from	dual;

		if	(((ie_evento_w in ('RBP')) and (qt_volume_real_w < 0) and (ie_perda_ganho_w = 'P')) or
			 ((ie_evento_w in ('RBG')) and (qt_volume_real_w > 0) and (ie_perda_ganho_w = 'G'))) then

			qt_volume_real_ww	:= qt_volume_real_w;
     		if	(qt_volume_real_w < 0) then
				qt_volume_real_ww	:= qt_volume_real_w * -1;
			end if;

			insert into atendimento_perda_ganho
				(nr_sequencia,
				nr_atendimento,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tipo,
				qt_volume,
				ds_observacao,
				dt_medida,
				cd_setor_atendimento,
				ie_origem,
				dt_referencia,
				cd_profissional,
				ie_situacao,
				dt_liberacao,
				dt_apap,
				qt_ocorrencia,
				nr_seq_evento_ivc
				)
			values	(nr_seq_atend_w,
				nr_atendimento_w,
				sysdate,
				:new.nm_usuario,
				nr_seq_tipo_w,
				qt_volume_real_ww,
				:new.ds_observacao,
				:new.dt_registro,
				cd_setor_atendimento_w,
				'S',
				sysdate,
				:new.cd_pessoa_fisica,
				'A',
				sysdate,
				:new.dt_registro,
				1,
				:new.nr_sequencia);

		elsif (ie_evento_w not in ('RBP', 'RBG')) then
			if(ie_perda_ganho_w = 'G') and
			(nvl(:new.qt_infusao,0) > 0 ) then
			    insert into atendimento_perda_ganho
					(nr_sequencia,
					nr_atendimento,
					dt_atualizacao,
					nm_usuario,
					nr_seq_tipo,
					qt_volume,
					ds_observacao,
					dt_medida,
					cd_setor_atendimento,
					ie_origem,
					dt_referencia,
					cd_profissional,
					ie_situacao,
						dt_liberacao,
					dt_apap,
					qt_ocorrencia,
					nr_seq_evento_ivc
					)
				values	(nr_seq_atend_w,
					nr_atendimento_w,
					sysdate,
					:new.nm_usuario,
					nr_seq_tipo_w,
					:new.qt_infusao,
					:new.ds_observacao,
					:new.dt_registro,
					cd_setor_atendimento_w,
					'S',
					sysdate,
					:new.cd_pessoa_fisica,
					'A',
					sysdate,
					:new.dt_registro,
					1,
					:new.nr_sequencia);
			elsif	(ie_perda_ganho_w = 'P') and
				(nvl(:new.qt_drenagem,0) > 0) then
			    insert into atendimento_perda_ganho
					(nr_sequencia,
					nr_atendimento,
					dt_atualizacao,
					nm_usuario,
					nr_seq_tipo,
					qt_volume,
					ds_observacao,
					dt_medida,
					cd_setor_atendimento,
					ie_origem,
					dt_referencia,
					cd_profissional,
					ie_situacao,
					dt_liberacao,
					dt_apap,
					qt_ocorrencia,
					nr_seq_evento_ivc
					)
				values	(nr_seq_atend_w,
					nr_atendimento_w,
					sysdate,
					:new.nm_usuario,
					nr_seq_tipo_w,
					:new.qt_drenagem,
					:new.ds_observacao,
					:new.dt_registro,
					cd_setor_atendimento_w,
					'S',
					sysdate,
					:new.cd_pessoa_fisica,
					'A',
					sysdate,
					:new.dt_registro,
					1,
					:new.nr_sequencia);
			end if;
		end if;
	end if;
	end;
end loop;
close C01;
END;
/


CREATE OR REPLACE TRIGGER TASY.adep_irrigacao_etapa_update
after UPDATE ON TASY.ADEP_IRRIGACAO_ETAPA FOR EACH ROW
begin

if	(:new.ie_evento_valido = 'N') and
	(:old.ie_evento_valido = 'S') then
	update	atendimento_perda_ganho a
	set	ie_situacao = 'I'
	where	a.nr_seq_evento_ivc = :new.nr_sequencia;
end if;

END;
/


ALTER TABLE TASY.ADEP_IRRIGACAO_ETAPA ADD (
  CONSTRAINT ADEPIRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ADEP_IRRIGACAO_ETAPA ADD (
  CONSTRAINT ADEPIRE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ADEPIRE_ADEPIRI_FK 
 FOREIGN KEY (NR_SEQ_IRRIGACAO) 
 REFERENCES TASY.ADEP_IRRIGACAO (NR_SEQUENCIA),
  CONSTRAINT ADEPIRE_ADEPASIR_FK 
 FOREIGN KEY (NR_SEQ_ASPECTO) 
 REFERENCES TASY.ADEP_ASPECTO_IRRIGACAO (NR_SEQUENCIA),
  CONSTRAINT ADEPIRE_ADEMOIN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.ADEP_MOTIVO_INTERRUPCAO (NR_SEQUENCIA),
  CONSTRAINT ADEPIRE_PREPROHO_FK 
 FOREIGN KEY (NR_SEQ_HORARIO) 
 REFERENCES TASY.PRESCR_PROC_HOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.ADEP_IRRIGACAO_ETAPA TO NIVEL_1;

