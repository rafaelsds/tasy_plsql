ALTER TABLE TASY.SPA_ROTA_APROV_RESP_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SPA_ROTA_APROV_RESP_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SPA_ROTA_APROV_RESP_REGRA
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_MES_INICIAL          NUMBER(10),
  NR_MES_FINAL            NUMBER(10),
  VL_DESCONTO_INICIAL     NUMBER(15,2),
  VL_DESCONTO_FINAL       NUMBER(15,2),
  PR_DESCONTO_INICIAL     NUMBER(15,2),
  PR_DESCONTO_FINAL       NUMBER(15,2),
  NR_PARCELAS_INICIAL     NUMBER(3),
  NR_PARCELAS_FINAL       NUMBER(3),
  IE_JUROS                VARCHAR2(1 BYTE),
  CD_CARGO                NUMBER(10),
  NR_NIVEL                NUMBER(5)             NOT NULL,
  NR_SEQ_ROTA             NUMBER(10),
  VL_NEGOC_MINIMO         NUMBER(15,2),
  VL_NEGOC_MAXIMO         NUMBER(15,2),
  NM_USUARIO_APROV        VARCHAR2(15 BYTE),
  QT_PRORROGACAO_INICIAL  NUMBER(3),
  QT_PRORROGACAO_FINAL    NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SPROAPR_CARGO_FK_I ON TASY.SPA_ROTA_APROV_RESP_REGRA
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPROAPR_CARGO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SPROAPR_PK ON TASY.SPA_ROTA_APROV_RESP_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPROAPR_PK
  MONITORING USAGE;


CREATE INDEX TASY.SPROAPR_SPAROAP_FK_I ON TASY.SPA_ROTA_APROV_RESP_REGRA
(NR_SEQ_ROTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SPROAPR_SPAROAP_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SPA_ROTA_APROV_RESP_REGRA_tp  after update ON TASY.SPA_ROTA_APROV_RESP_REGRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.VL_DESCONTO_FINAL,1,4000),substr(:new.VL_DESCONTO_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESCONTO_FINAL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DESCONTO_INICIAL,1,4000),substr(:new.PR_DESCONTO_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'PR_DESCONTO_INICIAL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DESCONTO_FINAL,1,4000),substr(:new.PR_DESCONTO_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'PR_DESCONTO_FINAL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PARCELAS_INICIAL,1,4000),substr(:new.NR_PARCELAS_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_PARCELAS_INICIAL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_PARCELAS_FINAL,1,4000),substr(:new.NR_PARCELAS_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_PARCELAS_FINAL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ROTA,1,4000),substr(:new.NR_SEQ_ROTA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ROTA',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CARGO,1,4000),substr(:new.CD_CARGO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CARGO',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_NIVEL,1,4000),substr(:new.NR_NIVEL,1,4000),:new.nm_usuario,nr_seq_w,'NR_NIVEL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MES_INICIAL,1,4000),substr(:new.NR_MES_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_MES_INICIAL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_MES_FINAL,1,4000),substr(:new.NR_MES_FINAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_MES_FINAL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DESCONTO_INICIAL,1,4000),substr(:new.VL_DESCONTO_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_DESCONTO_INICIAL',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_JUROS,1,4000),substr(:new.IE_JUROS,1,4000),:new.nm_usuario,nr_seq_w,'IE_JUROS',ie_log_w,ds_w,'SPA_ROTA_APROV_RESP_REGRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SPA_ROTA_APROV_RESP_REGRA ADD (
  CONSTRAINT SPROAPR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SPA_ROTA_APROV_RESP_REGRA ADD (
  CONSTRAINT SPROAPR_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT SPROAPR_SPAROAP_FK 
 FOREIGN KEY (NR_SEQ_ROTA) 
 REFERENCES TASY.SPA_ROTA_APROVACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SPA_ROTA_APROV_RESP_REGRA TO NIVEL_1;

