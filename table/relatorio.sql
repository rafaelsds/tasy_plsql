ALTER TABLE TASY.RELATORIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RELATORIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RELATORIO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_TITULO               VARCHAR2(80 BYTE)     NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  IE_BORDA_SUP            VARCHAR2(1 BYTE)      NOT NULL,
  IE_BORDA_INF            VARCHAR2(1 BYTE)      NOT NULL,
  IE_BORDA_ESQ            VARCHAR2(1 BYTE)      NOT NULL,
  IE_BORDA_DIR            VARCHAR2(1 BYTE)      NOT NULL,
  IE_ORIENTACAO           VARCHAR2(1 BYTE)      NOT NULL,
  IE_IMPRIME_VAZIO        VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_HTML          VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_WORD          VARCHAR2(1 BYTE)      NOT NULL,
  IE_TIPO_PAPEL           VARCHAR2(15 BYTE)     NOT NULL,
  IE_FILTRO_EXCEL         VARCHAR2(1 BYTE)      NOT NULL,
  IE_FILTRO_TEXTO         VARCHAR2(1 BYTE)      NOT NULL,
  DS_COR_FUNDO            VARCHAR2(10 BYTE),
  QT_MARGEM_SUP           NUMBER(5,1)           NOT NULL,
  QT_MARGEM_INF           NUMBER(5,1)           NOT NULL,
  QT_MARGEM_ESQ           NUMBER(5,1)           NOT NULL,
  QT_MARGEM_DIR           NUMBER(5,1)           NOT NULL,
  QT_ESPACO_COLUNA        NUMBER(5,1),
  QT_COLUNA               NUMBER(1),
  NM_TABELA               VARCHAR2(50 BYTE),
  DS_SQL                  VARCHAR2(4000 BYTE),
  NR_SEQ_MODULO           NUMBER(10),
  IE_CHAMADA_DIRETA       VARCHAR2(1 BYTE),
  CD_CLASSIF_RELAT        VARCHAR2(4 BYTE)      NOT NULL,
  CD_RELATORIO            NUMBER(5)             NOT NULL,
  IE_ETIQUETA             VARCHAR2(1 BYTE)      NOT NULL,
  CD_RELATORIO_WHEB       NUMBER(5),
  CD_CGC_CLIENTE          VARCHAR2(14 BYTE),
  IE_GERAR_BASE           VARCHAR2(1 BYTE),
  DS_REGRA                VARCHAR2(255 BYTE),
  DS_PROCEDURE            VARCHAR2(50 BYTE),
  IE_GERAR_RELATORIO      VARCHAR2(1 BYTE),
  QT_LARGURA              NUMBER(5,2),
  QT_ALTURA               NUMBER(5,2),
  NR_SEQ_MOD_IMPL         NUMBER(10),
  NR_SEQ_ORDEM_SERV       NUMBER(10),
  CD_CLASSIF_RELAT_WHEB   VARCHAR2(4 BYTE),
  NR_VERSAO               VARCHAR2(10 BYTE),
  CD_PAIS                 NUMBER(5),
  CD_RELATORIO_PAIS       NUMBER(5),
  CD_CLASSIF_RELAT_PAIS   VARCHAR2(4 BYTE),
  CD_VERSION              NUMBER(10),
  DS_ACTION_NAME          VARCHAR2(255 BYTE),
  IE_AJUSTAR_TAMANHO      VARCHAR2(1 BYTE),
  DT_LAST_MODIFICATION    DATE,
  DS_REPORT_TITLE_CUSTOM  VARCHAR2(255 BYTE),
  IE_TIPO_RELATORIO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELATOR_CDRELWHEB_I ON TASY.RELATORIO
(CD_RELATORIO_WHEB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELATOR_I1 ON TASY.RELATORIO
(IE_GERAR_BASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          48K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELATOR_MODIMPL_FK_I ON TASY.RELATORIO
(NR_SEQ_MOD_IMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELATOR_MODTASY_FK_I ON TASY.RELATORIO
(NR_SEQ_MODULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELATOR_OBJSIST_FK_I ON TASY.RELATORIO
(DS_PROCEDURE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.RELATOR_OBJSIST_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.RELATOR_PK ON TASY.RELATORIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELATOR_TASYPAI_FK_I ON TASY.RELATORIO
(CD_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RELATOR_UK ON TASY.RELATORIO
(CD_CLASSIF_RELAT, CD_RELATORIO, CD_CGC_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.RELATORIO_tp  after update ON TASY.RELATORIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_CLASSIF_RELAT,1,4000),substr(:new.CD_CLASSIF_RELAT,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSIF_RELAT',ie_log_w,ds_w,'RELATORIO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'RELATORIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_RELATORIO_WHEB,1,4000),substr(:new.CD_RELATORIO_WHEB,1,4000),:new.nm_usuario,nr_seq_w,'CD_RELATORIO_WHEB',ie_log_w,ds_w,'RELATORIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SQL,1,4000),substr(:new.DS_SQL,1,4000),:new.nm_usuario,nr_seq_w,'DS_SQL',ie_log_w,ds_w,'RELATORIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.RELATORIO_ATUAL_JAVA
before insert or update ON TASY.RELATORIO for each row
begin
	if (inserting or (:new.DT_ATUALIZACAO <> :old.DT_ATUALIZACAO)) then
		:new.IE_GERAR_RELATORIO := 'S';
		:new.DT_LAST_MODIFICATION := SYSDATE;
	end if;

	if (inserting or (:new.ie_gerar_relatorio = 'S' and nvl(:old.ie_gerar_relatorio,'N') != 'S')) then

		select cd_version_relat_seq.nextval
		into :new.cd_version
		from dual;

	end if;
end;
/


ALTER TABLE TASY.RELATORIO ADD (
  CONSTRAINT RELATOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          896K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT RELATOR_UK
 UNIQUE (CD_CLASSIF_RELAT, CD_RELATORIO, CD_CGC_CLIENTE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RELATORIO ADD (
  CONSTRAINT RELATOR_TASYPAI_FK 
 FOREIGN KEY (CD_PAIS) 
 REFERENCES TASY.TASY_PAIS (CD_PAIS),
  CONSTRAINT RELATOR_MODTASY_FK 
 FOREIGN KEY (NR_SEQ_MODULO) 
 REFERENCES TASY.MODULO_TASY (NR_SEQUENCIA),
  CONSTRAINT RELATOR_MODIMPL_FK 
 FOREIGN KEY (NR_SEQ_MOD_IMPL) 
 REFERENCES TASY.MODULO_IMPLANTACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.RELATORIO TO NIVEL_1;

