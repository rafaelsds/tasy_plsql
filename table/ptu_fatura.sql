ALTER TABLE TASY.PTU_FATURA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PTU_FATURA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PTU_FATURA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  CD_ESTABELECIMENTO       NUMBER(4)            NOT NULL,
  CD_UNIMED_DESTINO        VARCHAR2(4 BYTE),
  CD_UNIMED_ORIGEM         VARCHAR2(4 BYTE)     NOT NULL,
  DT_GERACAO               DATE,
  NR_COMPETENCIA           NUMBER(4),
  DT_VENCIMENTO_FATURA     DATE,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  DT_EMISSAO_FATURA        DATE,
  VL_TOTAL_FATURA          NUMBER(12,2),
  NR_VERSAO_TRANSACAO      NUMBER(2),
  VL_IR                    NUMBER(12,2),
  IE_TIPO_FATURA           NUMBER(3),
  IE_OPERACAO              VARCHAR2(1 BYTE),
  NR_SEQ_LOTE              NUMBER(10),
  NR_SEQ_PROTOCOLO         NUMBER(10),
  DT_GERACAO_TITULO        DATE,
  NR_TITULO                NUMBER(10),
  DT_RECEBIMENTO_FATURA    DATE,
  IE_FATURA_FISICA         VARCHAR2(1 BYTE)     NOT NULL,
  VL_TOTAL                 NUMBER(15,2),
  VL_GLOSA                 NUMBER(15,2),
  VL_PENDENTE              NUMBER(15,2),
  DT_GERACAO_CONTA         DATE,
  IE_PCMSO                 VARCHAR2(1 BYTE),
  PR_TAXA                  NUMBER(15,2),
  IE_ENVIA_CONTA           VARCHAR2(1 BYTE),
  IE_STATUS                VARCHAR2(10 BYTE),
  VL_TOTAL_DIGITADO        NUMBER(12,2),
  VL_TOTAL_TAXA            NUMBER(12,2),
  IE_ORIGEM                VARCHAR2(2 BYTE),
  NR_LINHA                 NUMBER(10),
  NR_SEQ_CONG_RESP_FINANC  NUMBER(10),
  NR_SEQ_MOT_DEVOLUCAO     NUMBER(10),
  NR_SEQ_MOT_CANCEL        NUMBER(10),
  DT_DEVOLUCAO             DATE,
  NM_USUARIO_DEVOLUCAO     VARCHAR2(15 BYTE),
  DT_POSTAGEM              DATE,
  DT_RECEB_DOC_FISICO      DATE,
  NM_USUARIO_DOC_FISICO    VARCHAR2(15 BYTE),
  IE_DOC_FISICA_CONFERIDA  VARCHAR2(1 BYTE),
  IE_NECESSITA_DOC_FISICO  VARCHAR2(1 BYTE),
  NR_SEQ_FATURA_ORIGEM     NUMBER(10),
  NR_SEQ_PLS_FATURA        NUMBER(10),
  DS_OBSERVACAO_INF        VARCHAR2(4000 BYTE),
  DT_MES_COMPETENCIA       DATE,
  IE_LIB_IMPORT            VARCHAR2(1 BYTE),
  DT_ACEITE                DATE,
  NR_SEQ_PERIODO           NUMBER(10),
  IE_CLASSIF_COBRANCA      VARCHAR2(1 BYTE),
  NR_NOTA_CREDITO_DEBITO   VARCHAR2(30 BYTE),
  DT_VENCIMENTO_NDC        DATE,
  DT_EMISSAO_NDC           DATE,
  VL_TOTAL_NDC             NUMBER(15,2),
  DT_VENC_FATURA_ORIGINAL  DATE,
  NR_TITULO_NDC            NUMBER(10),
  DT_GERACAO_TITULO_NDC    DATE,
  IE_TIT_FAT_NDC           VARCHAR2(1 BYTE),
  VL_GLOSA_NDC             NUMBER(15,2),
  VL_LIBERADO_NDC          NUMBER(15,2),
  DS_JUSTIFICATIVA         VARCHAR2(4000 BYTE),
  NR_FATURA                VARCHAR2(30 BYTE),
  TP_DOCUMENTO_1           NUMBER(1),
  TP_DOCUMENTO_2           NUMBER(1),
  DS_SID_PROCESSO          VARCHAR2(255 BYTE),
  DS_SERIAL_PROCESSO       VARCHAR2(255 BYTE),
  NM_ID_JOB                VARCHAR2(255 BYTE),
  NR_SEQ_GERACAO           NUMBER(10),
  DOC_FISCAL_1             VARCHAR2(20 BYTE),
  DOC_FISCAL_2             VARCHAR2(20 BYTE),
  IE_TIPO_ARQUIVO_COB      VARCHAR2(10 BYTE),
  DS_HASH                  VARCHAR2(255 BYTE),
  IE_TIPO_EXPORTACAO       VARCHAR2(3 BYTE),
  IE_TIPO_COBRANCA_FATURA  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PTUFATU_ESTABEL_FK_I ON TASY.PTU_FATURA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUFATU_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUFATU_I1 ON TASY.PTU_FATURA
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUFATU_I3 ON TASY.PTU_FATURA
(CD_UNIMED_DESTINO, IE_STATUS, IE_OPERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUFATU_I4 ON TASY.PTU_FATURA
(NR_TITULO_NDC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PTUFATU_PK ON TASY.PTU_FATURA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUFATU_PLSCCPE_FK_I ON TASY.PTU_FATURA
(NR_SEQ_PERIODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUFATU_PLSCCPE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUFATU_PLSCONG_FK1_I ON TASY.PTU_FATURA
(NR_SEQ_CONG_RESP_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUFATU_PLSCONG_FK1_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUFATU_PLSFATU_FK_I ON TASY.PTU_FATURA
(NR_SEQ_PLS_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUFATU_PLSLOFE_FK_I ON TASY.PTU_FATURA
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUFATU_PLSLOFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUFATU_PTUFATU_FK_I ON TASY.PTU_FATURA
(NR_SEQ_FATURA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUFATU_PTUFATU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUFATU_PTUMCAA_FK_I ON TASY.PTU_FATURA
(NR_SEQ_MOT_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUFATU_PTUMCAA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUFATU_PTUMDEA_FK_I ON TASY.PTU_FATURA
(NR_SEQ_MOT_DEVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTUFATU_PTUMDEA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTUFATU_TITPAGA_FK_I ON TASY.PTU_FATURA
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTUFATU_TITRECE_FK_I ON TASY.PTU_FATURA
(NR_FATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PTU_FATURA_tp  after update ON TASY.PTU_FATURA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_STATUS,1,4000),substr(:new.IE_STATUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS',ie_log_w,ds_w,'PTU_FATURA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.ptu_fatura_update
before update ON TASY.PTU_FATURA for each row
declare

ds_log_w		ptu_fatura_log.ds_log%type; 		-- 4000
ds_observacao_w		ptu_fatura_log.ds_observacao%type;	-- 4000
nm_machine_w		varchar2(255);
nm_program_w		varchar2(255);
nm_machine_log_w	varchar2(255);
ds_log_call_w		varchar2(4000);
qt_analise_aberta_w	pls_integer;

begin
if	(:new.ie_status = 'I') or
	(:new.ie_status = 'V') then
	begin
	select	' M�quina: ' || machine
	into	nm_machine_w
	from	v$session
	where	audsid = userenv('sessionid');
	exception
		when others then
		nm_machine_w	:= null;
	end;

	begin
	select	' Aplica��o: ' || program
	into	nm_program_w
	from	v$session
	where	audsid = userenv('sessionid');
	exception
		when others then
		nm_machine_w	:= null;
	end;

	ds_observacao_w	:=	trim(substr(nm_machine_w || nm_program_w,1,4000));

	ds_log_w 	:=	'PTU_FATURA: '		||
				'IE_STATUS_OLD: ' 	|| :old.ie_status ||
				'   IE_STATUS_NEW: '	|| :new.ie_status ||
				'   NM_USUARIO_OLD: ' 	|| :old.nm_usuario ||
				'   NM_USUARIO_NEW: ' 	|| :new.nm_usuario||
				'   Call: '||dbms_utility.format_call_stack;

	ptu_gerar_fatura_log( :new.nr_sequencia, ds_log_w, ds_observacao_w, 'N', nvl(:new.nm_usuario,:old.nm_usuario));
end if;

if 	((:old.ie_status = 'E') and
	(:new.ie_status <> 'AF')) then
	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

	begin
	select	' M�quina: ' || machine
	into	nm_machine_log_w
	from	v$session
	where	audsid = userenv('sessionid');
	exception
		when others then
		nm_machine_log_w	:= null;
	end;

	ds_log_w	:= substr('CallStack: ' || ds_log_call_w,1,4000);
	ds_observacao_w := substr('Altera��o de Status de "Encerrada" para outro status diferente de "An�lise Finalizada" ' || ' Usu�rio: ' || nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado') || ' M�quina: ' || nm_machine_log_w,1,4000);

	ptu_gerar_fatura_log( :new.nr_sequencia, ds_log_w, ds_observacao_w, 'N', nvl(:new.nm_usuario,:old.nm_usuario));
end if;

if 	(:new.ie_status = 'CA') then
	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

	begin
	select	' M�quina: ' || machine
	into	nm_machine_log_w
	from	v$session
	where	audsid = userenv('sessionid');
	exception
		when others then
		nm_machine_log_w	:= null;
	end;

	ds_log_w	:= substr('CallStack: ' || ds_log_call_w,1,4000);
	ds_observacao_w := substr('Fatura cancelada. T�tulo: ' || :new.nr_titulo || ' Titulo NDC ' || :new.nr_titulo_ndc || ' Usu�rio: ' || nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado') || ' M�quina: ' || nm_machine_log_w,1,4000);

	ptu_gerar_fatura_log( :new.nr_sequencia, ds_log_w, ds_observacao_w, 'N', nvl(:new.nm_usuario,:old.nm_usuario));
end if;


if 	(:new.nr_seq_protocolo != :old.nr_seq_protocolo) or
	((:new.nr_seq_protocolo is not null) and (:old.nr_seq_protocolo is null)) or
	((:new.nr_seq_protocolo is null) and (:old.nr_seq_protocolo is not null))then
	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

	begin
	select	' M�quina: ' || machine
	into	nm_machine_log_w
	from	v$session
	where	audsid = userenv('sessionid');
	exception
		when others then
		nm_machine_log_w	:= null;
	end;

	ds_log_w	:= substr('CallStack: ' || ds_log_call_w,1,4000);
	ds_observacao_w := substr('Protocolo alterado : ' || :new.nr_seq_protocolo || ' protocolo antigo ' || :old.nr_seq_protocolo || ' Usu�rio: ' || nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado') || ' M�quina: ' || nm_machine_log_w,1,4000);

	ptu_gerar_fatura_log( :new.nr_sequencia, ds_log_w, ds_observacao_w, 'N', nvl(:new.nm_usuario,:old.nm_usuario));
end if;

if	(:new.ie_status <> :old.ie_status) then
	begin

	ds_log_call_w := substr(pls_obter_detalhe_exec(false),1,1500);

	begin
	select	' M�quina: ' || machine
	into	nm_machine_log_w
	from	v$session
	where	audsid = userenv('sessionid');
	exception
		when others then
		nm_machine_log_w	:= null;
	end;

	ds_log_w	:= substr('CallStack: ' || ds_log_call_w,1,4000);
	ds_observacao_w := substr('Altera��o de Status de ' ||:old.ie_status || ' para ' || :new.ie_status || '. ' || ' Usu�rio: ' || nvl(wheb_usuario_pck.get_nm_usuario,'Usu�rio n�o identificado') || ' M�quina: ' || nm_machine_log_w,1,4000);

	ptu_gerar_fatura_log( :new.nr_sequencia, ds_log_w, ds_observacao_w, 'N', nvl(:new.nm_usuario,:old.nm_usuario));

	end;
end if;

if	(:new.ie_status = 'AF') and (:old.ie_status <> 'AF') then

	begin
		select	count(1)
		into	qt_analise_aberta_w
		from	pls_analise_conta a,
			pls_conta b
		where	a.nr_sequencia = b.nr_seq_analise
		and	b.nr_seq_fatura = :new.nr_sequencia
		and	a.ie_status <> 'T';
	exception
	when others then
		qt_analise_aberta_w := 0;
	end;

	if	(qt_analise_aberta_w > 0) then
		ds_log_w := substr('CallStack: ' || ds_log_call_w,1,4000);
		ds_observacao_w := substr('Status da fatura alterado para "An�lise finalizada", por�m, nem todas as an�lises est�o fechadas ',1,4000);
		ptu_gerar_fatura_log( :new.nr_sequencia, ds_log_w, ds_observacao_w, 'N', nvl(:new.nm_usuario,:old.nm_usuario));
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ptu_fatura_delete_atual
before delete ON TASY.PTU_FATURA for each row
declare

qt_titulo_w	pls_integer;

begin
-- Contar t�tulos abertos de fatura A500
select	count(1)
into	qt_titulo_w
from	titulo_pagar
where	nr_titulo = :old.nr_titulo
and	ie_situacao <> 'C';

-- Consistir t�tulos aberto
if	(nvl(qt_titulo_w,0) > 0) then
	--A fatura n�o pode ser exclu�da. Existem t�tulos em aberto para esta fatura!
	wheb_mensagem_pck.exibir_mensagem_abort(356067);
end if;

-- Contar t�tulos NDR abertos de fatura A500
select	count(1)
into	qt_titulo_w
from	titulo_pagar
where	nr_titulo = :old.nr_titulo_ndc
and	ie_situacao <> 'C';

-- Consistir t�tulos NDR aberto
if	(nvl(qt_titulo_w,0) > 0) then
	--A fatura n�o pode ser exclu�da. Existem t�tulos em aberto para esta fatura!
	wheb_mensagem_pck.exibir_mensagem_abort(356067);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ptu_fatura_imp_update
before insert or update ON TASY.PTU_FATURA for each row
declare

begin

if	(:old.ie_status <> 'I') and (:new.ie_status = 'I') then
	pls_grava_log_proces_imp_a500('ptu_fatura_imp_update', :new.nr_sequencia, nvl(:new.nm_usuario, :old.nm_usuario));
end if;

end;
/


ALTER TABLE TASY.PTU_FATURA ADD (
  CONSTRAINT PTUFATU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PTU_FATURA ADD (
  CONSTRAINT PTUFATU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PTUFATU_PLSLOFE_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.PTU_LOTE_FATURA_ENVIO (NR_SEQUENCIA),
  CONSTRAINT PTUFATU_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO),
  CONSTRAINT PTUFATU_PLSCONG_FK1 
 FOREIGN KEY (NR_SEQ_CONG_RESP_FINANC) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA),
  CONSTRAINT PTUFATU_PTUMCAA_FK 
 FOREIGN KEY (NR_SEQ_MOT_CANCEL) 
 REFERENCES TASY.PTU_MOT_CANCELAMENTO_A500 (NR_SEQUENCIA),
  CONSTRAINT PTUFATU_PTUMDEA_FK 
 FOREIGN KEY (NR_SEQ_MOT_DEVOLUCAO) 
 REFERENCES TASY.PTU_MOTIVO_DEVOLUCAO_A500 (NR_SEQUENCIA),
  CONSTRAINT PTUFATU_PLSFATU_FK 
 FOREIGN KEY (NR_SEQ_PLS_FATURA) 
 REFERENCES TASY.PLS_FATURA (NR_SEQUENCIA),
  CONSTRAINT PTUFATU_PTUFATU_FK 
 FOREIGN KEY (NR_SEQ_FATURA_ORIGEM) 
 REFERENCES TASY.PTU_FATURA (NR_SEQUENCIA),
  CONSTRAINT PTUFATU_PLSCCPE_FK 
 FOREIGN KEY (NR_SEQ_PERIODO) 
 REFERENCES TASY.PLS_CAMARA_CALEND_PERIODO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PTU_FATURA TO NIVEL_1;

