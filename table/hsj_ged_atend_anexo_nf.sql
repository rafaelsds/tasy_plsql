DROP TABLE TASY.HSJ_GED_ATEND_ANEXO_NF CASCADE CONSTRAINTS;

CREATE TABLE TASY.HSJ_GED_ATEND_ANEXO_NF
(
  NR_SEQ_NOTA_ANEXO  NUMBER(10),
  NR_SEQ_GED_ATEND   NUMBER(10),
  DS_ARQUIVO         VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


GRANT SELECT ON TASY.HSJ_GED_ATEND_ANEXO_NF TO NIVEL_1;

