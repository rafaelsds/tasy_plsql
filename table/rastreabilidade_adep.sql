ALTER TABLE TASY.RASTREABILIDADE_ADEP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RASTREABILIDADE_ADEP CASCADE CONSTRAINTS;

CREATE TABLE TASY.RASTREABILIDADE_ADEP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PERFIL            NUMBER(5),
  NM_USUARIO_REGRA     VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA   DATE,
  DT_FIM_VIGENCIA      DATE,
  IE_TIPO_PROCESSO     VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RASADEP_ESTABEL_FK_I ON TASY.RASTREABILIDADE_ADEP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RASADEP_PERFIL_FK_I ON TASY.RASTREABILIDADE_ADEP
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RASADEP_PK ON TASY.RASTREABILIDADE_ADEP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RASTREABILIDADE_ADEP ADD (
  CONSTRAINT RASADEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.RASTREABILIDADE_ADEP ADD (
  CONSTRAINT RASADEP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT RASADEP_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.RASTREABILIDADE_ADEP TO NIVEL_1;

