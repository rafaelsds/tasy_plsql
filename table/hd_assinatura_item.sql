ALTER TABLE TASY.HD_ASSINATURA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_ASSINATURA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_ASSINATURA_ITEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_ASSINATURA       NUMBER(10)            NOT NULL,
  NR_SEQ_EVENTO           NUMBER(10),
  NR_SEQ_CONCENTRADO      NUMBER(10),
  NR_SEQ_CONCENTRADO_RET  NUMBER(10),
  DT_INATIVACAO           DATE,
  NM_USUARIO_INATIVACAO   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDASSIT_HDASSDI_FK_I ON TASY.HD_ASSINATURA_ITEM
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDASSIT_HDDIAEV_FK_I ON TASY.HD_ASSINATURA_ITEM
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDASSIT_HDDICON_FK_I ON TASY.HD_ASSINATURA_ITEM
(NR_SEQ_CONCENTRADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDASSIT_HDDICON_FK2_I ON TASY.HD_ASSINATURA_ITEM
(NR_SEQ_CONCENTRADO_RET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HDASSIT_PK ON TASY.HD_ASSINATURA_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hd_assinatura_item_befup_sign
before update ON TASY.HD_ASSINATURA_ITEM for each row
begin

if 	(:old.nr_sequencia		<> :new.nr_sequencia
	or :old.dt_atualizacao		<> :new.dt_atualizacao
	or :old.nm_usuario		<> :new.nm_usuario
	or :old.dt_atualizacao_nrec	<> :new.dt_atualizacao_nrec
	or :old.nm_usuario_nrec		<> :new.nm_usuario_nrec
	or :old.nr_seq_assinatura	<> :new.nr_seq_assinatura
	or :old.nr_seq_evento		<> :new.nr_seq_evento
	or :old.nr_seq_concentrado	<> :new.nr_seq_concentrado
	or :old.nr_seq_concentrado_ret	<> :new.nr_seq_concentrado_ret
	or (:old.nr_sequencia		is null and	:new.nr_sequencia          	is not null)
	or (:old.dt_atualizacao		is null and	:new.dt_atualizacao        	is not null)
	or (:old.nm_usuario		is null and	:new.nm_usuario            	is not null)
	or (:old.dt_atualizacao_nrec	is null and	:new.dt_atualizacao_nrec   	is not null)
	or (:old.nm_usuario_nrec	is null and	:new.nm_usuario_nrec       	is not null)
	or (:old.nr_seq_assinatura	is null and	:new.nr_seq_assinatura     	is not null)
	or (:old.nr_seq_evento		is null and	:new.nr_seq_evento         	is not null)
	or (:old.nr_seq_concentrado	is null and	:new.nr_seq_concentrado    	is not null)
	or (:old.nr_seq_concentrado_ret	is null and	:new.nr_seq_concentrado_ret	is not null)
	or (:old.nr_sequencia		is not null and		:new.nr_sequencia          	is null)
	or (:old.dt_atualizacao		is not null and		:new.dt_atualizacao        	is null)
	or (:old.nm_usuario		is not null and		:new.nm_usuario            	is null)
	or (:old.dt_atualizacao_nrec	is not null and		:new.dt_atualizacao_nrec   	is null)
	or (:old.nm_usuario_nrec	is not null and		:new.nm_usuario_nrec       	is null)
	or (:old.nr_seq_assinatura	is not null and		:new.nr_seq_assinatura     	is null)
	or (:old.nr_seq_evento		is not null and		:new.nr_seq_evento         	is null)
	or (:old.nr_seq_concentrado	is not null and		:new.nr_seq_concentrado    	is null)
	or (:old.nr_seq_concentrado_ret	is not null and		:new.nr_seq_concentrado_ret	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/


ALTER TABLE TASY.HD_ASSINATURA_ITEM ADD (
  CONSTRAINT HDASSIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_ASSINATURA_ITEM ADD (
  CONSTRAINT HDASSIT_HDASSDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.HD_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT HDASSIT_HDDIAEV_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.HD_DIALISE_EVENTO (NR_SEQUENCIA),
  CONSTRAINT HDASSIT_HDDICON_FK 
 FOREIGN KEY (NR_SEQ_CONCENTRADO) 
 REFERENCES TASY.HD_DIALISE_CONCENTRADO (NR_SEQUENCIA),
  CONSTRAINT HDASSIT_HDDICON_FK2 
 FOREIGN KEY (NR_SEQ_CONCENTRADO_RET) 
 REFERENCES TASY.HD_DIALISE_CONCENTRADO (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_ASSINATURA_ITEM TO NIVEL_1;

