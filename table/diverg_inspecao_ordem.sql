ALTER TABLE TASY.DIVERG_INSPECAO_ORDEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DIVERG_INSPECAO_ORDEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.DIVERG_INSPECAO_ORDEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGISTRO      NUMBER(10)               NOT NULL,
  NR_SEQ_INSPECAO      NUMBER(10)               NOT NULL,
  NR_ORDEM_COMPRA      NUMBER(10),
  NR_ITEM_OCI          NUMBER(5),
  CD_TIPO_DIVERGENCIA  VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_REGRA         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DIINSOC_ESTABEL_FK_I ON TASY.DIVERG_INSPECAO_ORDEM
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIINSOC_INSRECE_FK_I ON TASY.DIVERG_INSPECAO_ORDEM
(NR_SEQ_INSPECAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DIINSOC_INSREGI_FK_I ON TASY.DIVERG_INSPECAO_ORDEM
(NR_SEQ_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DIINSOC_PK ON TASY.DIVERG_INSPECAO_ORDEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DIVERG_INSPECAO_ORDEM ADD (
  CONSTRAINT DIINSOC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DIVERG_INSPECAO_ORDEM ADD (
  CONSTRAINT DIINSOC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT DIINSOC_INSRECE_FK 
 FOREIGN KEY (NR_SEQ_INSPECAO) 
 REFERENCES TASY.INSPECAO_RECEBIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DIINSOC_INSREGI_FK 
 FOREIGN KEY (NR_SEQ_REGISTRO) 
 REFERENCES TASY.INSPECAO_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.DIVERG_INSPECAO_ORDEM TO NIVEL_1;

