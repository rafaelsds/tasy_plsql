ALTER TABLE TASY.USUARIO_ALERTA_TABELA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.USUARIO_ALERTA_TABELA CASCADE CONSTRAINTS;

CREATE TABLE TASY.USUARIO_ALERTA_TABELA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_TABELA            VARCHAR2(50 BYTE)        NOT NULL,
  IE_INCLUIR           VARCHAR2(1 BYTE),
  IE_EXCLUIR           VARCHAR2(1 BYTE),
  IE_ALTERAR           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.USUALET_PK ON TASY.USUARIO_ALERTA_TABELA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUALET_TABSIST_FK_I ON TASY.USUARIO_ALERTA_TABELA
(NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.USUALET_USUARIO_FK_I ON TASY.USUARIO_ALERTA_TABELA
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.USUARIO_ALERTA_TABELA ADD (
  CONSTRAINT USUALET_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.USUARIO_ALERTA_TABELA ADD (
  CONSTRAINT USUALET_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA),
  CONSTRAINT USUALET_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.USUARIO_ALERTA_TABELA TO NIVEL_1;

