ALTER TABLE TASY.CTB_ORC_CENARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CTB_ORC_CENARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CTB_ORC_CENARIO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_EMPRESA              NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DS_CENARIO              VARCHAR2(80 BYTE)     NOT NULL,
  NR_SEQ_MES_INICIO       NUMBER(10)            NOT NULL,
  NR_SEQ_MES_FIM          NUMBER(10)            NOT NULL,
  DT_LIBERACAO            DATE,
  DT_FECHAMENTO           DATE,
  DT_FECHAMENTO_PREVISTO  DATE,
  NR_SEQ_PROJ_GPI         NUMBER(10),
  CD_ESTAB_EXCLUSIVO      NUMBER(4),
  DT_GERACAO_ORC          DATE,
  CD_RESP_APROVACAO       VARCHAR2(10 BYTE),
  DT_APROVACAO            DATE,
  IE_TIPO_CENARIO         VARCHAR2(1 BYTE),
  NR_ANO                  NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CTBORCE_CTBMERE_FK_I ON TASY.CTB_ORC_CENARIO
(NR_SEQ_MES_INICIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCE_CTBMERE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCE_CTBMERE_FK2_I ON TASY.CTB_ORC_CENARIO
(NR_SEQ_MES_FIM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCE_CTBMERE_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCE_EMPRESA_FK_I ON TASY.CTB_ORC_CENARIO
(CD_EMPRESA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBORCE_ESTABEL_FK_I ON TASY.CTB_ORC_CENARIO
(CD_ESTAB_EXCLUSIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CTBORCE_GPIPROJ_FK_I ON TASY.CTB_ORC_CENARIO
(NR_SEQ_PROJ_GPI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CTBORCE_PESFISI_FK_I ON TASY.CTB_ORC_CENARIO
(CD_RESP_APROVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CTBORCE_PK ON TASY.CTB_ORC_CENARIO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CTBORCE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ctb_orc_cenario_atual
before insert or update ON TASY.CTB_ORC_CENARIO for each row
declare
-- local variables here
begin
if (:new.ie_tipo_cenario = 'P' and nvl(:new.nr_ano, 0) <> 0) then
	if inserting then
		begin
			--  Mes Inicio
			select	a.nr_sequencia
			into	:new.nr_seq_mes_inicio
			from	ctb_mes_ref a
			where	to_char(a.dt_referencia, 'MM/YYYY')	= '01/' || :new.nr_ano
			and	a.cd_empresa				= :new.cd_empresa;

			--  Mes Fim
			select	nr_sequencia
			into	:new.nr_seq_mes_fim
			from	ctb_mes_ref
			where	to_char(dt_referencia, 'MM/YYYY')	= '12/' || :new.nr_ano
			and	cd_empresa				= :new.cd_empresa;
		exception
			when no_data_found then
			wheb_mensagem_pck.exibir_mensagem_abort(1111918);
		end;
	elsif (:new.nr_ano <> :old.nr_ano) then
		begin
			--  Mes Inicio
			select	a.nr_sequencia
			into	:new.nr_seq_mes_inicio
			from	ctb_mes_ref a
			where	to_char(a.dt_referencia, 'MM/YYYY')	= '01/' || :new.nr_ano
			and	a.cd_empresa				= :new.cd_empresa;

			--  Mes Fim
			select	nr_sequencia
			into	:new.nr_seq_mes_fim
			from	ctb_mes_ref
			where	to_char(dt_referencia, 'MM/YYYY')	= '12/' || :new.nr_ano
			and	cd_empresa				= :new.cd_empresa;
		exception
			when no_data_found then
			wheb_mensagem_pck.exibir_mensagem_abort(1111918);
		end;
	end if;
end if;

end ctb_orc_cenario_atual;
/


ALTER TABLE TASY.CTB_ORC_CENARIO ADD (
  CONSTRAINT CTBORCE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CTB_ORC_CENARIO ADD (
  CONSTRAINT CTBORCE_PESFISI_FK 
 FOREIGN KEY (CD_RESP_APROVACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CTBORCE_GPIPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_GPI) 
 REFERENCES TASY.GPI_PROJETO (NR_SEQUENCIA),
  CONSTRAINT CTBORCE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_EXCLUSIVO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CTBORCE_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT CTBORCE_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_INICIO) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA),
  CONSTRAINT CTBORCE_CTBMERE_FK2 
 FOREIGN KEY (NR_SEQ_MES_FIM) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA));

GRANT SELECT ON TASY.CTB_ORC_CENARIO TO NIVEL_1;

