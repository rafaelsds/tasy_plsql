ALTER TABLE TASY.AP_LOTE_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AP_LOTE_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.AP_LOTE_ITEM
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_LOTE           NUMBER(10)              NOT NULL,
  NR_SEQ_MAT_HOR        NUMBER(10),
  IE_PRESCRITO          VARCHAR2(1 BYTE)        NOT NULL,
  CD_MATERIAL           NUMBER(10)              NOT NULL,
  CD_UNIDADE_MEDIDA     VARCHAR2(30 BYTE)       NOT NULL,
  QT_DISPENSAR          NUMBER(15,2)            NOT NULL,
  QT_TOTAL_DISPENSAR    NUMBER(15,4),
  IE_URGENTE            VARCHAR2(1 BYTE)        NOT NULL,
  DT_SUPENSAO           DATE,
  NM_USUARIO_SUSP       VARCHAR2(15 BYTE),
  DS_MAQUINA_SUSP       VARCHAR2(80 BYTE),
  CD_MATERIAL_ORIGINAL  NUMBER(10),
  DT_CONTROLE           DATE,
  CD_LOCAL_CONSISTIDO   NUMBER(10),
  IE_CONSISTIDO         VARCHAR2(1 BYTE),
  DS_STACK              VARCHAR2(2000 BYTE),
  DT_RETIRADA           DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.APLOTIT_APLOTE_FK_I ON TASY.AP_LOTE_ITEM
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTIT_LOCESTO_FK_I ON TASY.AP_LOTE_ITEM
(CD_LOCAL_CONSISTIDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTIT_MATERIA_FK_I ON TASY.AP_LOTE_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTIT_MATERIA_FK2_I ON TASY.AP_LOTE_ITEM
(CD_MATERIAL_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTIT_MATERIA_FK2_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.APLOTIT_PK ON TASY.AP_LOTE_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTIT_PREMAHO_FK_I ON TASY.AP_LOTE_ITEM
(NR_SEQ_MAT_HOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.APLOTIT_UNIMEDI_FK_I ON TASY.AP_LOTE_ITEM
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.APLOTIT_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ap_lote_item_update
before insert or update ON TASY.AP_LOTE_ITEM for each row
declare

ds_origem_w		varchar2(1800);
begin
if	(updating) then
	if	(:old.qt_dispensar > 0) and
		(:old.qt_dispensar <> :new.qt_dispensar) then

		ds_origem_w := substr(dbms_utility.format_call_stack,1,1800);

		insert into log_tasy(
			dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values	(sysdate,
			:new.nm_usuario,
			88938,
			'Lote=' || :new.nr_seq_lote || ' Old=' || :old.qt_dispensar || ' New=' || :new.qt_dispensar || ' Origem=' ||ds_origem_w);
	end if;

	if	(:old.qt_dispensar <> :new.qt_dispensar) then

		ds_origem_w := substr(dbms_utility.format_call_stack,1,2000);

		insert into log_tasy(
			dt_atualizacao,
			nm_usuario,
			cd_log,
			ds_log)
		values	(
			sysdate,
			:new.nr_seq_lote,
			88899,
			'Lote=' || :new.nr_seq_lote || ' old.qt_dispensar=' || :old.qt_dispensar || ' new.qt_dispensar=' || :new.qt_dispensar || ' Origem=' || ds_origem_w);

	end if;

end if;

if	(inserting) then
	:new.ds_stack := substr(dbms_utility.format_call_stack,1,2000);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.ap_lote_item_insert
after insert ON TASY.AP_LOTE_ITEM for each row
declare

begin
gerar_int_dankia_pck.dankia_prescr_mat_hor(:new.nr_seq_mat_hor, :new.cd_material, :new.nr_seq_lote, :new.qt_dispensar, 'I', null,:new.nm_usuario);
end;
/


CREATE OR REPLACE TRIGGER TASY.ap_lote_item_b_delete
before delete ON TASY.AP_LOTE_ITEM for each row
declare

begin

if  (wheb_usuario_pck.get_ie_executar_trigger  = 'N')  then
  goto Fim;
end if;

gravar_log_tasy(1712,substr('Delete item=' || :new.nr_sequencia || ' mat hor=' || :new.nr_seq_mat_hor ||
' Stack=' || dbms_utility.format_call_stack,1,2000),:new.nr_seq_lote);
<<Fim>>
null;
end;
/


ALTER TABLE TASY.AP_LOTE_ITEM ADD (
  CONSTRAINT APLOTIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AP_LOTE_ITEM ADD (
  CONSTRAINT APLOTIT_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_CONSISTIDO) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT APLOTIT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT APLOTIT_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT APLOTIT_APLOTE_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.AP_LOTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT APLOTIT_PREMAHO_FK 
 FOREIGN KEY (NR_SEQ_MAT_HOR) 
 REFERENCES TASY.PRESCR_MAT_HOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT APLOTIT_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_ORIGINAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.AP_LOTE_ITEM TO NIVEL_1;

