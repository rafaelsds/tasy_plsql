ALTER TABLE TASY.PLS_COMERCIAL_REGRA_ACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COMERCIAL_REGRA_ACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COMERCIAL_REGRA_ACAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  QT_DIAS_ATIV_CANAL_COM  NUMBER(5)             NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_SOLICITACAO      NUMBER(10),
  NR_SEQ_CLIENTE          NUMBER(10),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  IE_APLICACAO_COMERCIAL  VARCHAR2(2 BYTE),
  IE_ORIGEM_SOLICITACAO   VARCHAR2(2 BYTE),
  IE_TIPO_CONTRATACAO     VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCORA_ESTABEL_FK_I ON TASY.PLS_COMERCIAL_REGRA_ACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCORA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSCORA_PK ON TASY.PLS_COMERCIAL_REGRA_ACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCORA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCORA_PLSCOCL_FK_I ON TASY.PLS_COMERCIAL_REGRA_ACAO
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCORA_PLSCOCL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCORA_PLSSCOM_FK_I ON TASY.PLS_COMERCIAL_REGRA_ACAO
(NR_SEQ_SOLICITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_COMERCIAL_REGRA_ACAO ADD (
  CONSTRAINT PLSCORA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COMERCIAL_REGRA_ACAO ADD (
  CONSTRAINT PLSCORA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCORA_PLSCOCL_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.PLS_COMERCIAL_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT PLSCORA_PLSSCOM_FK 
 FOREIGN KEY (NR_SEQ_SOLICITACAO) 
 REFERENCES TASY.PLS_SOLICITACAO_COMERCIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COMERCIAL_REGRA_ACAO TO NIVEL_1;

