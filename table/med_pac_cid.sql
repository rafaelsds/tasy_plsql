ALTER TABLE TASY.MED_PAC_CID
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_PAC_CID CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_PAC_CID
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_CLIENTE         NUMBER(10)             NOT NULL,
  CD_DOENCA_CID          VARCHAR2(10 BYTE)      NOT NULL,
  DT_DIAGNOSTICO         DATE                   NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  NR_SEQ_AVALIACAO       NUMBER(10),
  IE_SEVERIDADE          VARCHAR2(1 BYTE),
  IE_PRIMARIO            VARCHAR2(1 BYTE),
  IE_CLASSIFICACAO       VARCHAR2(1 BYTE),
  IE_STATUS              VARCHAR2(3 BYTE)       NOT NULL,
  DT_INICIO              DATE,
  NR_ATENDIMENTO         NUMBER(10),
  NM_USUARIO_EXPORTACAO  VARCHAR2(15 BYTE),
  IE_EXPORTADO           VARCHAR2(1 BYTE),
  DT_EXPORTACAO          DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEPACID_CIDDOEN_FK_I ON TASY.MED_PAC_CID
(CD_DOENCA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEPACID_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEPACID_MEDATEN_FK_I ON TASY.MED_PAC_CID
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEPACID_MEDATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEPACID_MEDAVCP_FK_I ON TASY.MED_PAC_CID
(NR_SEQ_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEPACID_MEDAVCP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEPACID_MEDCLIE_FK_I ON TASY.MED_PAC_CID
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEPACID_MEDCLIE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MEPACID_PK ON TASY.MED_PAC_CID
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEPACID_PK
  MONITORING USAGE;


ALTER TABLE TASY.MED_PAC_CID ADD (
  CONSTRAINT MEPACID_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_PAC_CID ADD (
  CONSTRAINT MEPACID_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA_CID) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT MEPACID_MEDCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.MED_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT MEPACID_MEDAVCP_FK 
 FOREIGN KEY (NR_SEQ_AVALIACAO) 
 REFERENCES TASY.MED_AVAL_COMPLETA_PAC (NR_SEQUENCIA),
  CONSTRAINT MEPACID_MEDATEN_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.MED_ATENDIMENTO (NR_ATENDIMENTO));

GRANT SELECT ON TASY.MED_PAC_CID TO NIVEL_1;

