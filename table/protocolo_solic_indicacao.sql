ALTER TABLE TASY.PROTOCOLO_SOLIC_INDICACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_SOLIC_INDICACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_SOLIC_INDICACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_SOLIC_BS      NUMBER(10)               NOT NULL,
  IE_GRUPO_HEMOCOM     VARCHAR2(3 BYTE),
  NR_SEQ_INDICACAO     NUMBER(10),
  DS_INDICACAO_ADIC    VARCHAR2(255 BYTE),
  DS_JUSTIFICATIVA     VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROSOLI_PK ON TASY.PROTOCOLO_SOLIC_INDICACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROSOLI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROSOLI_PRSOBCS_FK_I ON TASY.PROTOCOLO_SOLIC_INDICACAO
(NR_SEQ_SOLIC_BS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.protocolo_solic_indic_atual
before insert or update ON TASY.PROTOCOLO_SOLIC_INDICACAO for each row
declare

cd_protocolo_w	number(10);
nr_sequencia_w	number(10);

begin

if	(obter_se_prot_lib_regras = 'S') then
	select	cd_protocolo,
		nr_seq_protocolo
	into	cd_protocolo_w,
		nr_sequencia_w
	from	prot_solic_bco_sangue
	where	nr_sequencia	= :new.nr_seq_solic_bs;

	update	protocolo_medicacao
	set	nm_usuario_aprov	=	null,
		dt_aprovacao		=	null,
		ie_status		=	'PA'
	where	cd_protocolo		=	cd_protocolo_w
	and	nr_sequencia		=	nr_sequencia_w;
end if;

end;
/


ALTER TABLE TASY.PROTOCOLO_SOLIC_INDICACAO ADD (
  CONSTRAINT PROSOLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROTOCOLO_SOLIC_INDICACAO ADD (
  CONSTRAINT PROSOLI_PRSOBCS_FK 
 FOREIGN KEY (NR_SEQ_SOLIC_BS) 
 REFERENCES TASY.PROT_SOLIC_BCO_SANGUE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROTOCOLO_SOLIC_INDICACAO TO NIVEL_1;

