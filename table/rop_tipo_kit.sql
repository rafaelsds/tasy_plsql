ALTER TABLE TASY.ROP_TIPO_KIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_TIPO_KIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_TIPO_KIT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_TIPO              VARCHAR2(80 BYTE)        NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ROTIKIT_EMPRESA_FK_I ON TASY.ROP_TIPO_KIT
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROTIKIT_EMPRESA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ROTIKIT_PK ON TASY.ROP_TIPO_KIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROTIKIT_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ROP_TIPO_KIT_tp  after update ON TASY.ROP_TIPO_KIT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'ROP_TIPO_KIT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TIPO,1,4000),substr(:new.DS_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_TIPO',ie_log_w,ds_w,'ROP_TIPO_KIT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ROP_TIPO_KIT ADD (
  CONSTRAINT ROTIKIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_TIPO_KIT ADD (
  CONSTRAINT ROTIKIT_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA));

GRANT SELECT ON TASY.ROP_TIPO_KIT TO NIVEL_1;

