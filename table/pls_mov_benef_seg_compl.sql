ALTER TABLE TASY.PLS_MOV_BENEF_SEG_COMPL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MOV_BENEF_SEG_COMPL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MOV_BENEF_SEG_COMPL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_MOV_SEGURADO  NUMBER(10)               NOT NULL,
  IE_TIPO_COMPLEMENTO  NUMBER(2)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CEP               VARCHAR2(15 BYTE),
  DS_ENDERECO          VARCHAR2(100 BYTE),
  NR_ENDERECO          NUMBER(5),
  DS_COMPLEMENTO       VARCHAR2(40 BYTE),
  DS_BAIRRO            VARCHAR2(80 BYTE),
  CD_MUNICIPIO_IBGE    VARCHAR2(6 BYTE),
  SG_ESTADO            VARCHAR2(15 BYTE),
  CD_TIPO_LOGRADOURO   VARCHAR2(40 BYTE),
  NR_DDI_TELEFONE      VARCHAR2(3 BYTE),
  NR_DDD_TELEFONE      VARCHAR2(3 BYTE),
  NR_TELEFONE          VARCHAR2(15 BYTE),
  DS_EMAIL             VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSMVSC_PK ON TASY.PLS_MOV_BENEF_SEG_COMPL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMVSC_PLSMBSG_FK_I ON TASY.PLS_MOV_BENEF_SEG_COMPL
(NR_SEQ_MOV_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMVSC_SUSMUNI_FK_I ON TASY.PLS_MOV_BENEF_SEG_COMPL
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMVSC_SUSTILO_FK_I ON TASY.PLS_MOV_BENEF_SEG_COMPL
(CD_TIPO_LOGRADOURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_MOV_BENEF_SEG_COMPL_tp  after update ON TASY.PLS_MOV_BENEF_SEG_COMPL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TELEFONE,1,4000),substr(:new.NR_TELEFONE,1,4000),:new.nm_usuario,nr_seq_w,'NR_TELEFONE',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_LOGRADOURO,1,4000),substr(:new.CD_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOGRADOURO',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_COMPLEMENTO,1,4000),substr(:new.IE_TIPO_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COMPLEMENTO',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'PLS_MOV_BENEF_SEG_COMPL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_MOV_BENEF_SEG_COMPL ADD (
  CONSTRAINT PLSMVSC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_MOV_BENEF_SEG_COMPL ADD (
  CONSTRAINT PLSMVSC_PLSMBSG_FK 
 FOREIGN KEY (NR_SEQ_MOV_SEGURADO) 
 REFERENCES TASY.PLS_MOV_BENEF_SEGURADO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PLSMVSC_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT PLSMVSC_SUSTILO_FK 
 FOREIGN KEY (CD_TIPO_LOGRADOURO) 
 REFERENCES TASY.SUS_TIPO_LOGRADOURO (CD_TIPO_LOGRADOURO));

GRANT SELECT ON TASY.PLS_MOV_BENEF_SEG_COMPL TO NIVEL_1;

