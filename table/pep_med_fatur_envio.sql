ALTER TABLE TASY.PEP_MED_FATUR_ENVIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEP_MED_FATUR_ENVIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEP_MED_FATUR_ENVIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ENVIO             DATE,
  DS_EMAIL             VARCHAR2(4000 BYTE),
  DS_DESTINATARIO      VARCHAR2(255 BYTE),
  DS_ASSUNTO           VARCHAR2(255 BYTE),
  IE_TIPO_EMAIL        VARCHAR2(15 BYTE),
  DS_EMAIL_LONG        LONG,
  NR_ATENDIMENTO       NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEMEFEN_ATEPACI_FK_I ON TASY.PEP_MED_FATUR_ENVIO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEMEFEN_PK ON TASY.PEP_MED_FATUR_ENVIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEMEFEN_PK
  MONITORING USAGE;


ALTER TABLE TASY.PEP_MED_FATUR_ENVIO ADD (
  CONSTRAINT PEMEFEN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PEP_MED_FATUR_ENVIO ADD (
  CONSTRAINT PEMEFEN_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.PEP_MED_FATUR_ENVIO TO NIVEL_1;

