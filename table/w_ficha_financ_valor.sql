ALTER TABLE TASY.W_FICHA_FINANC_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_FICHA_FINANC_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_FICHA_FINANC_VALOR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_VALOR        VARCHAR2(5 BYTE)         NOT NULL,
  DS_VALOR             VARCHAR2(255 BYTE)       NOT NULL,
  VL_FICHA             NUMBER(15,2)             NOT NULL,
  CD_CGC               VARCHAR2(14 BYTE),
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WFIFIVA_PESFISI_FK_I ON TASY.W_FICHA_FINANC_VALOR
(CD_PESSOA_FISICA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WFIFIVA_PESJURI_FK_I ON TASY.W_FICHA_FINANC_VALOR
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFIFIVA_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WFIFIVA_PK ON TASY.W_FICHA_FINANC_VALOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WFIFIVA_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_FICHA_FINANC_VALOR ADD (
  CONSTRAINT WFIFIVA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_FICHA_FINANC_VALOR ADD (
  CONSTRAINT WFIFIVA_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC)
    ON DELETE CASCADE,
  CONSTRAINT WFIFIVA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_FICHA_FINANC_VALOR TO NIVEL_1;

