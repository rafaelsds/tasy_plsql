ALTER TABLE TASY.PLS_REGRA_COPARTIC_UTIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COPARTIC_UTIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COPARTIC_UTIL
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_REGRA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  QT_EVENTO_MINIMO           NUMBER(15),
  QT_EVENTO_MAXIMO           NUMBER(10),
  IE_TIPO_DATA_CONSISTENCIA  VARCHAR2(2 BYTE),
  QT_PERIODO_OCORRENCIA      NUMBER(10),
  IE_TIPO_PERIODO_OCOR       VARCHAR2(5 BYTE),
  NR_SEQ_GRUPO_SERV          NUMBER(10),
  IE_EVENTOS_INCIDENCIA      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSRGCU_PK ON TASY.PLS_REGRA_COPARTIC_UTIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSRGCU_PLSPRGS_FK_I ON TASY.PLS_REGRA_COPARTIC_UTIL
(NR_SEQ_GRUPO_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_regra_copartic_util_update
before update ON TASY.PLS_REGRA_COPARTIC_UTIL for each row
declare

begin
if	(:new.ie_eventos_incidencia <> :old.ie_eventos_incidencia) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_eventos_incidencia, :new.ie_eventos_incidencia, 'PLS_REGRA_COPARTIC_UTIL', 'IE_EVENTOS_INCIDENCIA', :new.nm_usuario);
end if;
if	(:new.qt_evento_minimo <> :old.qt_evento_minimo) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_evento_minimo, :new.qt_evento_minimo, 'PLS_REGRA_COPARTIC_UTIL', 'QT_EVENTO_MINIMO', :new.nm_usuario);
end if;
if	(:new.nr_seq_grupo_serv <> :old.nr_seq_grupo_serv) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.nr_seq_grupo_serv, :new.nr_seq_grupo_serv, 'PLS_REGRA_COPARTIC_UTIL', 'NR_SEQ_GRUPO_SERV', :new.nm_usuario);
end if;
if	(:new.qt_evento_maximo <> :old.qt_evento_maximo) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_evento_maximo, :new.qt_evento_maximo, 'PLS_REGRA_COPARTIC_UTIL', 'QT_EVENTO_MAXIMO', :new.nm_usuario);
end if;
if	(:new.ie_tipo_data_consistencia <> :old.ie_tipo_data_consistencia) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_tipo_data_consistencia, :new.ie_tipo_data_consistencia, 'PLS_REGRA_COPARTIC_UTIL', 'IE_TIPO_DATA_CONSISTENCIA', :new.nm_usuario);
end if;
if	(:new.qt_periodo_ocorrencia <> :old.qt_periodo_ocorrencia) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.qt_periodo_ocorrencia, :new.qt_periodo_ocorrencia, 'PLS_REGRA_COPARTIC_UTIL', 'QT_PERIODO_OCORRENCIA', :new.nm_usuario);
end if;
if	(:new.ie_tipo_periodo_ocor <> :old.ie_tipo_periodo_ocor) then
	 pls_gravar_log_alt_regra_aprop (:new.nr_seq_regra, :old.ie_tipo_periodo_ocor, :new.ie_tipo_periodo_ocor, 'PLS_REGRA_COPARTIC_UTIL', 'IE_TIPO_PERIODO_OCOR', :new.nm_usuario);
end if;
end;
/


ALTER TABLE TASY.PLS_REGRA_COPARTIC_UTIL ADD (
  CONSTRAINT PLSRGCU_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COPARTIC_UTIL ADD (
  CONSTRAINT PLSRGCU_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERV) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COPARTIC_UTIL TO NIVEL_1;

