ALTER TABLE TASY.PRESCR_MAT_ALTERACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_MAT_ALTERACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_MAT_ALTERACAO
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_PRESCRICAO           NUMBER(14),
  NR_SEQ_PRESCRICAO       NUMBER(6),
  DT_ALTERACAO            DATE                  NOT NULL,
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  IE_ALTERACAO            NUMBER(3)             NOT NULL,
  NR_SEQ_HORARIO          NUMBER(10),
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  DS_JUSTIFICATIVA        VARCHAR2(255 BYTE),
  NR_SEQ_PROCEDIMENTO     NUMBER(10),
  NR_SEQ_RECOMENDACAO     NUMBER(10),
  NR_SEQ_HORARIO_PROC     NUMBER(10),
  NR_SEQ_HORARIO_REC      NUMBER(10),
  IE_AGRUPADOR            NUMBER(2),
  IE_TIPO_ITEM            VARCHAR2(3 BYTE),
  NR_SEQ_HORARIO_SAE      NUMBER(10),
  NR_SEQ_ITEM_SAE         NUMBER(10),
  DT_HOR_ACM_SN           DATE,
  NR_SEQ_HORARIO_DIETA    NUMBER(10),
  NR_SEQ_QUALIDADE        NUMBER(10),
  NR_SEQ_SATISFACAO       NUMBER(10),
  DT_HORARIO              DATE,
  NR_ATENDIMENTO          NUMBER(10),
  CD_ITEM                 VARCHAR2(255 BYTE),
  QT_DOSE_ADM             NUMBER(15,4),
  CD_UM_DOSE_ADM          VARCHAR2(30 BYTE),
  NR_SEQ_MOTIVO           NUMBER(10),
  DT_HORARIO_ORIGINAL     DATE,
  NR_SEQ_PROC_INTERNO     NUMBER(10),
  QT_DOSE_ORIGINAL        NUMBER(15,4),
  NR_AGRUPAMENTO          NUMBER(7,1),
  NR_SEQ_HORARIO_ORDEM    NUMBER(15),
  IE_ACM_SN               VARCHAR2(1 BYTE),
  CD_MEDICO_SOLIC         VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_SUSP      NUMBER(10),
  NR_SEQ_SERVICO          NUMBER(15),
  NR_SEQ_MOTIVO_DISP      NUMBER(10),
  IE_MOSTRA_ADEP          VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA       NUMBER(15),
  NR_SEQ_NUT_ATEND        NUMBER(15),
  NR_SEQ_LOTE             NUMBER(10),
  IE_EVENTO_VALIDO        VARCHAR2(1 BYTE),
  NR_SEQ_PROT_GLIC        NUMBER(10),
  CD_PROCEDIMENTO         NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  DS_STACK                VARCHAR2(2000 BYTE),
  CD_FUNCAO               NUMBER(10),
  DS_OBSERVACAO_ITEM      VARCHAR2(2000 BYTE),
  DT_HORARIO_ACAO         DATE,
  NR_SEQ_MOT_LOTE_GEDIPA  NUMBER(10),
  NR_SEQ_TIPO_OBS         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREMAAL_ADEMOIN_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_MOTIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_ADEMOIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAAL_ADEQUAR_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_QUALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_ADEQUAR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAAL_ADETIOB_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_TIPO_OBS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_ADSAREF_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_SATISFACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_ADSAREF_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAAL_ATEPACI_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_I1 ON TASY.PRESCR_MAT_ALTERACAO
(IE_TIPO_ITEM, CD_ITEM, NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_MOGELOG_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_MOT_LOTE_GEDIPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_MOGELOG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAAL_MOTGERD_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_MOTIVO_DISP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_MOTGERD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAAL_MOTSUPR_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_MOTIVO_SUSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_MOTSUPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAAL_NUTASED_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_NUT_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          616K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_NUTASED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAAL_PEPREPR_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_ITEM_SAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_PEPREPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAAL_PEPRHOR_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_HORARIO_SAE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_PESFISI_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_PESFISI_FK2_I ON TASY.PRESCR_MAT_ALTERACAO
(CD_MEDICO_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PREMAAL_PK ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_PREDHOR_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_HORARIO_DIETA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_PREMAHO_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_PREPROHO_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_HORARIO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_PRERECHO_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_HORARIO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_PRESMAT_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_PRESCRICAO, NR_SEQ_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_PRESPRO_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_PRESCRICAO, NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREMAAL_PRESREC_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_PRESCRICAO, NR_SEQ_RECOMENDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_PRESREC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREMAAL_PRORDHO_FK_I ON TASY.PRESCR_MAT_ALTERACAO
(NR_SEQ_HORARIO_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREMAAL_PRORDHO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Prescr_mat_alteracao_Insert
before insert ON TASY.PRESCR_MAT_ALTERACAO for each row
declare
pragma autonomous_transaction;

cd_setor_atendimento_w		number(15,0);
nr_seq_atend_w				number(15,0);
nr_seq_tipo_w				number(15,0);
cd_perfil_w					number(5);
ie_perda_ganho_dt_evento_w	varchar2(1);

cursor c01 is
select	nr_seq_tipo
from	prescr_dieta_perda_ganho
where	decode(ie_evento,'AD',3,null) = :new.ie_alteracao
and     ('D' = :new.ie_tipo_item or
'DE' = :new.ie_tipo_item);

begin

Obter_Param_Usuario(1113, 690, obter_perfil_ativo, :new.nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_perda_ganho_dt_evento_w);

if (ie_perda_ganho_dt_evento_w = 'S') then

	update 	atendimento_perda_ganho
	set		dt_medida = :new.dt_alteracao
	where 	nr_seq_horario 	= :new.nr_seq_horario
	and		nr_atendimento 	= :new.nr_atendimento;

end if;

excluir_evento_duplicado(:new.nr_prescricao, :new.nr_seq_procedimento, :new.nr_sequencia, :new.dt_alteracao, :new.ie_alteracao, :new.ie_tipo_item);

if (:new.dt_horario_acao is null) then

	begin
		if	(:new.dt_hor_acm_sn is not null) then
			:new.dt_horario_acao	:= :new.dt_hor_acm_sn;
		else
			:new.dt_horario_acao 	:= to_date(substr(plt_obter_dados_hor_item(coalesce(:new.nr_seq_horario, :new.nr_seq_horario_proc, :new.nr_seq_horario_rec, :new.nr_seq_horario_dieta, :new.nr_seq_horario_sae),:new.ie_tipo_item,'H'),1,19),'dd/mm/yyyy hh24:mi:ss');
		end if;

	exception
	when others then
		:new.dt_horario_acao 	:= sysdate;
	end;
end if;

:new.ds_stack			:= substr(dbms_utility.format_call_stack,1,2000);

if	(:new.cd_funcao is null) then
	:new.cd_funcao			:= obter_funcao_ativa;
end if;

if	(:new.qt_dose_adm is not null) and
	(:new.nr_atendimento is not null) then
	        open C01;
		loop
		fetch C01 into
			nr_seq_tipo_w;
		exit when C01%notfound;
			select	max(cd_setor_atendimento),
				max(cd_perfil_ativo)
			into	cd_setor_atendimento_w,
				cd_perfil_w
			from	prescr_medica
			where	nr_prescricao	= :new.nr_prescricao;

			select	atendimento_perda_ganho_seq.nextval
			into	nr_seq_atend_w
			from	dual;

			insert into atendimento_perda_ganho
				(nr_sequencia,
				nr_atendimento,
				dt_atualizacao,
				nm_usuario,
				nr_seq_tipo,
				qt_volume,
				ds_observacao,
				dt_medida,
				cd_setor_atendimento,
				ie_origem,
				dt_referencia,
				cd_profissional,
				ie_situacao,
				dt_liberacao,
				dt_apap,
				qt_ocorrencia,
				nr_seq_horario)
			values	(nr_seq_atend_w,
				:new.nr_atendimento,
				sysdate,
				:new.nm_usuario,
				nr_seq_tipo_w,
				:new.qt_dose_adm,
				:new.ds_observacao,
				sysdate,
				cd_setor_atendimento_w,
				'S',
				sysdate,
				:new.cd_pessoa_fisica,
				'A',
				sysdate,
				sysdate,
				1,
				:new.nr_seq_horario);
		end loop;
		close C01;
	COMMIT;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Prescr_mat_alteracao_Update
after update of ie_evento_valido ON TASY.PRESCR_MAT_ALTERACAO for each row
declare
cd_evolucao_w     PLS_INTEGER;
nr_sequencia_w    PLS_INTEGER;

begin

  if ((nvl(:old.ie_evento_valido,'S') = 'S')
      and (nvl(:new.ie_evento_valido,'S') = 'N')) then
		select max(cd_evolucao)
		into cd_evolucao_w
		from Prescr_mat_alteracao_comp
		where NR_SEQUENCIA_PRESCR = :old.NR_SEQUENCIA;

		update EVOLUCAO_PACIENTE
			set ie_situacao 	= 'I',
			dt_inativacao 	= sysdate,
			nm_usuario_inativacao = :new.nm_usuario
		where cd_evolucao 	= cd_evolucao_w
		and ie_situacao 	= 'A';

		if ((:old.ie_alteracao = 3)
		and (:old.ie_tipo_item = 'M')
		and (nvl(:old.nr_seq_horario, 0) > 0)) then

			select 	max(nr_sequencia)
			into 	nr_sequencia_w
			from 	atendimento_perda_ganho
			where 	nr_atendimento 	= :old.nr_atendimento
			and	nr_seq_horario 	= :old.nr_seq_horario
			and	ie_situacao 	= 'A';

			if (nvl(nr_sequencia_w, 0) > 0) then
				update	atendimento_perda_ganho
				set	ie_situacao = 'I',
					dt_inativacao = sysdate,
					nm_usuario_inativacao = :new.nm_usuario,
					ds_justificativa = null
				where	nr_sequencia = nr_sequencia_w;
			end if;
		end if;
  end if;
end;
/


ALTER TABLE TASY.PRESCR_MAT_ALTERACAO ADD (
  CONSTRAINT PREMAAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_MAT_ALTERACAO ADD (
  CONSTRAINT PREMAAL_MOGELOG_FK 
 FOREIGN KEY (NR_SEQ_MOT_LOTE_GEDIPA) 
 REFERENCES TASY.MOTIVO_GERACAO_LOTE_GEDIPA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_ADETIOB_FK 
 FOREIGN KEY (NR_SEQ_TIPO_OBS) 
 REFERENCES TASY.ADEP_TIPO_OBSERVACAO (NR_SEQUENCIA),
  CONSTRAINT PREMAAL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_MOTSUPR_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUSP) 
 REFERENCES TASY.MOTIVO_SUSPENSAO_PRESCR (NR_SEQUENCIA),
  CONSTRAINT PREMAAL_MOTGERD_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DISP) 
 REFERENCES TASY.MOTIVO_GERACAO_DISPENSACAO (NR_SEQUENCIA),
  CONSTRAINT PREMAAL_NUTASED_FK 
 FOREIGN KEY (NR_SEQ_NUT_ATEND) 
 REFERENCES TASY.NUT_ATEND_SERV_DIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO_SOLIC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PREMAAL_ADSAREF_FK 
 FOREIGN KEY (NR_SEQ_SATISFACAO) 
 REFERENCES TASY.ADEP_SATISFACAO_REFEICAO (NR_SEQUENCIA),
  CONSTRAINT PREMAAL_PRORDHO_FK 
 FOREIGN KEY (NR_SEQ_HORARIO_ORDEM) 
 REFERENCES TASY.PRESCR_ORDEM_HOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PRESMAT_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MATERIAL (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PREMAAL_ADEQUAR_FK 
 FOREIGN KEY (NR_SEQ_QUALIDADE) 
 REFERENCES TASY.ADEP_QUALIDADE_REFEICAO (NR_SEQUENCIA),
  CONSTRAINT PREMAAL_ADEMOIN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO) 
 REFERENCES TASY.ADEP_MOTIVO_INTERRUPCAO (NR_SEQUENCIA),
  CONSTRAINT PREMAAL_PRESPRO_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PRESCR_PROCEDIMENTO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PREMAHO_FK 
 FOREIGN KEY (NR_SEQ_HORARIO) 
 REFERENCES TASY.PRESCR_MAT_HOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PRESREC_FK 
 FOREIGN KEY (NR_PRESCRICAO, NR_SEQ_RECOMENDACAO) 
 REFERENCES TASY.PRESCR_RECOMENDACAO (NR_PRESCRICAO,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PREPROHO_FK 
 FOREIGN KEY (NR_SEQ_HORARIO_PROC) 
 REFERENCES TASY.PRESCR_PROC_HOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PRERECHO_FK 
 FOREIGN KEY (NR_SEQ_HORARIO_REC) 
 REFERENCES TASY.PRESCR_REC_HOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PEPREPR_FK 
 FOREIGN KEY (NR_SEQ_ITEM_SAE) 
 REFERENCES TASY.PE_PRESCR_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PEPRHOR_FK 
 FOREIGN KEY (NR_SEQ_HORARIO_SAE) 
 REFERENCES TASY.PE_PRESCR_PROC_HOR (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PREMAAL_PREDHOR_FK 
 FOREIGN KEY (NR_SEQ_HORARIO_DIETA) 
 REFERENCES TASY.PRESCR_DIETA_HOR (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PRESCR_MAT_ALTERACAO TO NIVEL_1;

