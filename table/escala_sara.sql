ALTER TABLE TASY.ESCALA_SARA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_SARA CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_SARA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_REL_PAO2_FIO2       NUMBER(15,4),
  QT_PRESSAO_CMH2O       NUMBER(15,3),
  QT_COMPL_CMH2O         NUMBER(15,3),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_RADIOGRAFIA_TORAX   NUMBER(1),
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  QT_PONTUACAO           NUMBER(5,2),
  DT_AVALIACAO           DATE                   NOT NULL,
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSARA_ATEPACI_FK_I ON TASY.ESCALA_SARA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSARA_PESFISI_FK_I ON TASY.ESCALA_SARA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSARA_PK ON TASY.ESCALA_SARA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ESCALA_SARA_ATUAL
before insert or update ON TASY.ESCALA_SARA for each row
declare
qt_rel_pao_w		number(10) := 0;
qt_topografia_w		number(10) := 0;
qt_cmhml_w			number(10) := 0;
qt_presssao_w		number(10) := 0;
qt_divisor_w		number(10) := 0;
qt_total_w			number(10,2) := 0;

begin

if	(:new.ie_radiografia_torax >= 0 ) then
	qt_topografia_w	:= :new.ie_radiografia_torax;
end if;

if	(:new.QT_REL_PAO2_FIO2 >= 300 ) then
	qt_rel_pao_w	:= 0;
elsif	(:new.QT_REL_PAO2_FIO2 between 225 and 299) then
	qt_rel_pao_w	:= 1;
elsif	(:new.QT_REL_PAO2_FIO2 between 175 and	224) then
	qt_rel_pao_w	:= 2;
elsif	(:new.QT_REL_PAO2_FIO2 between 100 and	174) then
	qt_rel_pao_w	:= 3;
elsif	(:new.QT_REL_PAO2_FIO2 < 100) then
	qt_rel_pao_w	:= 4;
end if;

if	(:new.qt_compl_cmh2o >= 80) then
	qt_cmhml_w := 0;
elsif	(:new.qt_compl_cmh2o between 60 and 79) then
		qt_cmhml_w := 1;
elsif	(:new.qt_compl_cmh2o between 40 and 59) then
		qt_cmhml_w := 2;
elsif	(:new.qt_compl_cmh2o between 20 and 39) then
		qt_cmhml_w := 3;
elsif	(:new.qt_compl_cmh2o <= 20) then
		qt_cmhml_w := 4;
end if;

if	(:new.qt_pressao_cmh2o <= 5) then
	qt_presssao_w := 0;
elsif	(:new.qt_pressao_cmh2o between 6 and 8) then
	qt_presssao_w := 1;
elsif	(:new.qt_pressao_cmh2o between 9 and 11) then
	qt_presssao_w := 2;
elsif	(:new.qt_pressao_cmh2o between 12 and 14) then
	qt_presssao_w := 3;
elsif	(:new.qt_pressao_cmh2o >=15) then
	qt_presssao_w := 4;
end if;

if	(:new.ie_radiografia_torax is not null) then
	qt_divisor_w := qt_divisor_w + 1;
end if;
if	(:new.qt_rel_pao2_fio2 is not null) then
	qt_divisor_w := qt_divisor_w + 1;
end if;
if	(:new.qt_compl_cmh2o is not null) then
	qt_divisor_w := qt_divisor_w + 1;
end if;
if	(:new.qt_pressao_cmh2o is not null) then
	qt_divisor_w := qt_divisor_w + 1;
end if;

if	(qt_divisor_w = 0) then
	qt_divisor_w 	:= 1;
end if;
qt_total_w	:= 	((nvl(qt_topografia_w,0) + nvl(qt_rel_pao_w,0) + nvl(qt_cmhml_w,0) + nvl(qt_presssao_w,0))/qt_divisor_w);

:new.qt_pontuacao := round(qt_total_w,2);
end;
/


ALTER TABLE TASY.ESCALA_SARA ADD (
  CONSTRAINT ESCSARA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_SARA ADD (
  CONSTRAINT ESCSARA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCSARA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_SARA TO NIVEL_1;

