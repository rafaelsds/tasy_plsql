ALTER TABLE TASY.ATENDIMENTO_VISITA_BLOQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_VISITA_BLOQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_VISITA_BLOQ
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  CD_PESSOA_BLOQUEIO      VARCHAR2(10 BYTE),
  NM_PESSOA_BLOQUEIO      VARCHAR2(80 BYTE),
  NR_SEQ_GRAU_PARENTESCO  NUMBER(10),
  NR_IDENTIDADE           VARCHAR2(15 BYTE),
  DS_OBSERVACAO           VARCHAR2(255 BYTE),
  NR_ATENDIMENTO          NUMBER(10)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATVISBL_ATEPACI_FK_I ON TASY.ATENDIMENTO_VISITA_BLOQ
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATVISBL_GRAUPA_FK_I ON TASY.ATENDIMENTO_VISITA_BLOQ
(NR_SEQ_GRAU_PARENTESCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATVISBL_GRAUPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATVISBL_PESFISI_FK_I ON TASY.ATENDIMENTO_VISITA_BLOQ
(CD_PESSOA_BLOQUEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATVISBL_PK ON TASY.ATENDIMENTO_VISITA_BLOQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ATENDIMENTO_VISITA_BLOQ ADD (
  CONSTRAINT ATVISBL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_VISITA_BLOQ ADD (
  CONSTRAINT ATVISBL_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATVISBL_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT ATVISBL_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_BLOQUEIO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ATENDIMENTO_VISITA_BLOQ TO NIVEL_1;

