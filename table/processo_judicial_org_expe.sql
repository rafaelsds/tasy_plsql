ALTER TABLE TASY.PROCESSO_JUDICIAL_ORG_EXPE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCESSO_JUDICIAL_ORG_EXPE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCESSO_JUDICIAL_ORG_EXPE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NM_ORGAO_EXPEDIDOR   VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSPJOE_PK ON TASY.PROCESSO_JUDICIAL_ORG_EXPE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSPJOE_PK
  MONITORING USAGE;


ALTER TABLE TASY.PROCESSO_JUDICIAL_ORG_EXPE ADD (
  CONSTRAINT PLSPJOE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PROCESSO_JUDICIAL_ORG_EXPE TO NIVEL_1;

