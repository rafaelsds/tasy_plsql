ALTER TABLE TASY.ESCALA_PRISM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_PRISM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_PRISM
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  DT_LIBERACAO           DATE,
  IE_LACTANTE            VARCHAR2(1 BYTE)       NOT NULL,
  QT_PA_SISTOLICA        NUMBER(3),
  QT_PTO_PAS             NUMBER(2)              NOT NULL,
  QT_PA_DIASTOLICA       NUMBER(3),
  QT_PTO_PAD             NUMBER(2)              NOT NULL,
  QT_FREQ_CARDIACA       NUMBER(3),
  QT_PTO_FC              NUMBER(2)              NOT NULL,
  QT_FREQ_RESP           NUMBER(3),
  QT_PTO_FR              NUMBER(2)              NOT NULL,
  QT_REL_PAO2_FIO2       NUMBER(5,2),
  QT_PTO_REL_PAO2_FIO2   NUMBER(2)              NOT NULL,
  QT_PACO2               NUMBER(5,2),
  QT_PTO_PACO2           NUMBER(2)              NOT NULL,
  QT_ESCALA_GLASGOW      NUMBER(5,1),
  QT_PTO_GLASGOW         NUMBER(2)              NOT NULL,
  IE_REACOES_PUPILARES   NUMBER(2)              NOT NULL,
  QT_PTO_REAC_PUP        NUMBER(2)              NOT NULL,
  IE_TEMPO_TP_TTP        VARCHAR2(1 BYTE)       NOT NULL,
  QT_PTO_TP_TTP          NUMBER(2)              NOT NULL,
  QT_BILIRRUBINA_TOTAL   NUMBER(15,2),
  QT_PTO_BILIRRUBINA     NUMBER(2)              NOT NULL,
  QT_POTASSIO            NUMBER(15,2),
  QT_PTO_POTASSIO        NUMBER(2)              NOT NULL,
  QT_CALCIO              NUMBER(15,2),
  QT_PTO_CALCIO          NUMBER(2)              NOT NULL,
  QT_GLICEMIA            NUMBER(15,2),
  QT_PTO_GLICEMIA        NUMBER(2)              NOT NULL,
  QT_BICARBONATO         NUMBER(15,2),
  QT_PTO_BICARBONATO     NUMBER(2)              NOT NULL,
  QT_PTO_TOTAL           NUMBER(2)              NOT NULL,
  IE_APNEIA              VARCHAR2(1 BYTE)       NOT NULL,
  IE_POS_OPERATORIO      VARCHAR2(1 BYTE)       NOT NULL,
  QT_IDADE               NUMBER(5),
  PR_MORTALIDADE         NUMBER(15,4)           NOT NULL,
  PR_MORTALIDADE_POS_OP  NUMBER(15,4)           NOT NULL,
  CD_PERFIL_ATIVO        NUMBER(5),
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NR_HORA                NUMBER(2),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCPRIS_ATEPACI_FK_I ON TASY.ESCALA_PRISM
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPRIS_I1 ON TASY.ESCALA_PRISM
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCPRIS_PERFIL_FK_I ON TASY.ESCALA_PRISM
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCPRIS_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCPRIS_PESFISI_FK_I ON TASY.ESCALA_PRISM
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCPRIS_PK ON TASY.ESCALA_PRISM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_prism_atual
before insert or update ON TASY.ESCALA_PRISM for each row
declare

logit_w			number(15,4);
logit_po_w		number(15,4);
cd_pessoa_fisica_w	varchar2(10);
qt_reg_w	number(1);

begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
/* Pontua��o PA Sist�lica */
if	(:new.qt_pa_sistolica is null) then
	:new.qt_pto_pas:= 0;
elsif	(:new.ie_lactante = 'S') and
	(:new.qt_pa_sistolica > 160) then
	:new.qt_pto_pas:= 6;
elsif	(:new.ie_lactante = 'S') and
	((:new.qt_pa_sistolica >= 130) and
	(:new.qt_pa_sistolica <= 160)) then
	:new.qt_pto_pas:= 2;
elsif	(:new.ie_lactante = 'S') and
	((:new.qt_pa_sistolica >= 66) and
	(:new.qt_pa_sistolica <= 129)) then
	:new.qt_pto_pas:= 0;
elsif	(:new.ie_lactante = 'S') and
	((:new.qt_pa_sistolica >= 55) and
	(:new.qt_pa_sistolica <= 65)) then
	:new.qt_pto_pas:= 2;
elsif	(:new.ie_lactante = 'S') and
	((:new.qt_pa_sistolica >= 40) and
	(:new.qt_pa_sistolica <= 54)) then
	:new.qt_pto_pas:= 6;
elsif	(:new.ie_lactante = 'S') and
	(:new.qt_pa_sistolica < 40) then
	:new.qt_pto_pas:= 7;
elsif	(:new.ie_lactante = 'N') and
	(:new.qt_pa_sistolica > 200) then
	:new.qt_pto_pas:= 6;
elsif	(:new.ie_lactante = 'N') and
	((:new.qt_pa_sistolica >= 150) and
	(:new.qt_pa_sistolica <= 200)) then
	:new.qt_pto_pas:= 2;
elsif	(:new.ie_lactante = 'N') and
	((:new.qt_pa_sistolica >= 76) and
	(:new.qt_pa_sistolica <= 149)) then
	:new.qt_pto_pas:= 0;
elsif	(:new.ie_lactante = 'N') and
	((:new.qt_pa_sistolica >= 65) and
	(:new.qt_pa_sistolica <= 75)) then
	:new.qt_pto_pas:= 2;
elsif	(:new.ie_lactante = 'N') and
	((:new.qt_pa_sistolica >= 50) and
	(:new.qt_pa_sistolica <= 64)) then
	:new.qt_pto_pas:= 6;
elsif	(:new.ie_lactante = 'N') and
	(:new.qt_pa_sistolica < 50) then
	:new.qt_pto_pas:= 7;
end if;

/* Pontua��o PA Diast�lica */
if	(:new.qt_pa_diastolica is null) then
	:new.qt_pto_pad:= 0;
elsif	(:new.qt_pa_diastolica > 110) then
	:new.qt_pto_pad:= 6;
else
	:new.qt_pto_pad:= 0;
end if;

/* Pontua��o Freq C�rdiaca */
if	(:new.qt_freq_cardiaca is null) then
	:new.qt_pto_fc:= 0;
elsif	(:new.ie_lactante = 'S') and
	((:new.qt_freq_cardiaca > 160) or
	(:new.qt_freq_cardiaca <= 90)) then
	:new.qt_pto_fc:= 4;
elsif	(:new.ie_lactante = 'S') and
	((:new.qt_freq_cardiaca >= 91) and
	(:new.qt_freq_cardiaca <= 159)) then
	:new.qt_pto_fc:= 0;
elsif	(:new.ie_lactante = 'N') and
	((:new.qt_freq_cardiaca > 150) or
	(:new.qt_freq_cardiaca <= 80)) then
	:new.qt_pto_fc:= 4;
elsif	(:new.ie_lactante = 'N') and
	((:new.qt_freq_cardiaca >= 81) and
	(:new.qt_freq_cardiaca <= 149)) then
	:new.qt_pto_fc:= 0;
end if;

/* Pontua��o Freq Respirat�ria */

if	(:new.qt_freq_resp is null) and
	(:new.ie_apneia = 'N') then
	:new.qt_pto_fr:= 0;
elsif	(:new.ie_apneia = 'S') then
	:new.qt_pto_fr:= 5;
elsif	(:new.ie_lactante = 'S') and
	(:new.qt_freq_resp >= 61) and
	(:new.qt_freq_resp <= 90) then
	:new.qt_pto_fr:= 1;
elsif	(:new.ie_lactante = 'S') and
	(:new.qt_freq_resp > 90) then
	:new.qt_pto_fr:= 5;
elsif	(:new.ie_lactante = 'N') and
	(:new.qt_freq_resp >= 51) and
	(:new.qt_freq_resp <= 70) then
	:new.qt_pto_fr:= 1;
elsif	(:new.ie_lactante = 'N') and
	(:new.qt_freq_resp > 70) then
	:new.qt_pto_fr:= 5;
else
	:new.qt_pto_fr:= 0;
end if;

/* Pontua��o PaO� FiO� */
if	(:new.qt_rel_pao2_fio2 is null) then
	:new.qt_pto_rel_pao2_fio2:= 0;
elsif	(:new.qt_rel_pao2_fio2 < 200) then
	:new.qt_pto_rel_pao2_fio2:= 3;
elsif	(:new.qt_rel_pao2_fio2 >= 200) and
	(:new.qt_rel_pao2_fio2 <= 300) then
	:new.qt_pto_rel_pao2_fio2:= 2;
else
	:new.qt_pto_rel_pao2_fio2:= 0;
end if;

/* Pontua��o PaC0� */
if	(:new.qt_paco2 is null) then
	:new.qt_pto_paco2:= 0;
elsif	(:new.qt_paco2 > 65) then
	:new.qt_pto_paco2:= 5;
elsif	(:new.qt_paco2 >= 51) and
	(:new.qt_paco2 <= 65) then
	:new.qt_pto_paco2:= 1;
else
	:new.qt_pto_paco2:= 0;
end if;

/* Pontua��o Tempo de Protrombina */
if	(:new.ie_tempo_tp_ttp = 'S') then
	:new.qt_pto_tp_ttp:= 2;
else
	:new.qt_pto_tp_ttp:= 0;
end if;

/* Pontua��o Bilirrubina */
if	(:new.qt_bilirrubina_total is null) then
	:new.qt_pto_bilirrubina:= 0;
elsif	(:new.qt_bilirrubina_total > 3.5) then
	:new.qt_pto_bilirrubina:= 6;
else
	:new.qt_pto_bilirrubina:= 0;
end if;

/* Pontua��o C�lcio */
if	(:new.qt_calcio is null) then
	:new.qt_pto_calcio:= 0;
elsif	(:new.qt_calcio < 7) or
	(:new.qt_calcio > 15) then
	:new.qt_pto_calcio:= 6;
elsif	((:new.qt_calcio >= 7) and
	(:new.qt_calcio <= 8)) or
	((:new.qt_calcio >= 12) and
	(:new.qt_calcio <= 15)) then
	:new.qt_pto_calcio:= 2;
else
	:new.qt_pto_calcio:= 0;
end if;

/* Pontua��o Pot�ssio */
if	(:new.qt_potassio is null) then
	:new.qt_pto_potassio:= 0;
elsif	(:new.qt_potassio < 3) or
	(:new.qt_potassio > 7.5) then
	:new.qt_pto_potassio:= 5;
elsif	((:new.qt_potassio >= 3) and
	(:new.qt_potassio <= 3.5)) or
	((:new.qt_potassio >= 6.5) and
	(:new.qt_potassio <= 7.5)) then
	:new.qt_pto_potassio:= 1;
else
	:new.qt_pto_potassio:= 0;
end if;

/* Pontua��o Glicemia */
if	(:new.qt_glicemia is null) then
	:new.qt_pto_glicemia:= 0;
elsif	(:new.qt_glicemia < 40) or
	(:new.qt_glicemia > 400) then
	:new.qt_pto_glicemia:= 8;
elsif	((:new.qt_glicemia >= 40) and
	(:new.qt_glicemia <= 60)) or
	((:new.qt_glicemia >= 250) and
	(:new.qt_glicemia <= 400)) then
	:new.qt_pto_glicemia:= 4;
else
	:new.qt_pto_glicemia:= 0;
end if;

/* Pontua��o Bicarbonato */
if	(:new.qt_bicarbonato is null) then
	:new.qt_pto_bicarbonato:= 0;
elsif	(:new.qt_bicarbonato < 16) or
	(:new.qt_bicarbonato > 32) then
	:new.qt_pto_bicarbonato:= 3;
else
	:new.qt_pto_bicarbonato:= 0;
end if;

/* Pontua��o rea��es pupilares */
if	(:new.ie_reacoes_pupilares = 0) then
	:new.qt_pto_reac_pup:= 0;
elsif	(:new.ie_reacoes_pupilares = 4) then
	:new.qt_pto_reac_pup:= 4;
elsif	(:new.ie_reacoes_pupilares = 10) then
	:new.qt_pto_reac_pup:= 10;
end if;

/* Pontua��o glasgow */
if	(:new.qt_escala_glasgow is null) then
	:new.qt_pto_glasgow:= 0;
elsif	(:new.qt_escala_glasgow < 8) then
	:new.qt_pto_glasgow:= 6;
else
	:new.qt_pto_glasgow:= 0;
end if;

/* Pontua��o Total */
:new.qt_pto_total:=	nvl(:new.qt_pto_pas,0)			+	nvl(:new.qt_pto_pad,0)		+
			nvl(:new.qt_pto_fc,0)			+	nvl(:new.qt_pto_fr,0)		+
			nvl(:new.qt_pto_rel_pao2_fio2,0)	+	nvl(:new.qt_pto_paco2,0)	+
			nvl(:new.qt_pto_tp_ttp,0)		+	nvl(:new.qt_pto_bilirrubina,0)	+
			nvl(:new.qt_pto_calcio,0)		+	nvl(:new.qt_pto_potassio,0)	+
			nvl(:new.qt_pto_glicemia,0)		+	nvl(:new.qt_pto_bicarbonato,0)	+
			nvl(:new.qt_pto_reac_pup,0)		+	nvl(:new.qt_pto_glasgow,0);

/* Obter pessoa f�sica */
select	cd_pessoa_fisica
into	cd_pessoa_fisica_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;


/* Idade em meses */
if	(:old.qt_idade is null) then
	select	nvl(obter_idade_pf(cd_pessoa_fisica_w, sysdate, 'M'),0)
	into	:new.qt_idade
	from	dual;
end if;

/* Predicado de mortalidade */
logit_w:= (0.207 * :new.qt_pto_total - (0.005 * (:new.qt_idade)) - 0.433 * 0 - 4.782);

:new.pr_mortalidade:= dividir(exp(logit_w), 1 + exp(logit_w)) * 100;

/* Precicado de mortalidade p�s-operat�rio */
logit_po_w:= (0.207 * :new.qt_pto_total - (0.005 * (:new.qt_idade)) - 0.433 * 1 - 4.782);

:new.pr_mortalidade_pos_op:= dividir(exp(logit_po_w), 1 + exp(logit_po_w)) * 100;


<<Final>>
qt_reg_w	:= 0;


end;
/


ALTER TABLE TASY.ESCALA_PRISM ADD (
  CONSTRAINT ESCPRIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_PRISM ADD (
  CONSTRAINT ESCPRIS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCPRIS_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCPRIS_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ESCALA_PRISM TO NIVEL_1;

