ALTER TABLE TASY.MAN_ORDEM_SERV_ARQ
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERV_ARQ CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERV_ARQ
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORDEM         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_ARQUIVO           VARCHAR2(255 BYTE)       NOT NULL,
  IE_ANEXAR_EMAIL      VARCHAR2(1 BYTE),
  IE_STATUS_ANEXO      VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  IE_DELETAR           VARCHAR2(1 BYTE),
  NR_SEQ_PHILIPS       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          704K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANORSQ_I1 ON TASY.MAN_ORDEM_SERV_ARQ
(IE_STATUS_ANEXO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORSQ_MANORSE_FK_I ON TASY.MAN_ORDEM_SERV_ARQ
(NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANORSQ_PK ON TASY.MAN_ORDEM_SERV_ARQ
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MAN_ORDEM_SERV_ARQ_INSERT
BEFORE INSERT ON TASY.MAN_ORDEM_SERV_ARQ FOR EACH ROW
DECLARE

begin

if	(:new.dt_atualizacao_nrec is null) then
	:new.dt_atualizacao_nrec := sysdate;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MAN_ORDEM_SERV_ARQ_DELETE
BEFORE DELETE ON TASY.MAN_ORDEM_SERV_ARQ FOR EACH ROW
DECLARE
nm_user_w		varchar2(50);
nm_usuario_w	usuario.nm_usuario%type;
qt_usuario_w	number(10);

BEGIN

select username
into nm_user_w
from user_users;

nm_usuario_w := wheb_usuario_pck.get_nm_usuario;

if	(nm_user_w = 'CORP') and
	( ((nvl(nm_usuario_w,'XPTO') <> nvl(:old.nm_usuario_nrec,'XPTO')) and (:old.nm_usuario_nrec is not null)) and Nvl(:old.ie_deletar, 'N') = 'N' ) then
	begin

	select	count(1)
	into	qt_usuario_w
	from	usuario
	where	nm_usuario = nm_usuario_w
	and		ie_situacao = 'A';

	if	(qt_usuario_w > 0) then
		/*N�o � poss�vel excluir o registro, usu�rio diferente do original.*/
		wheb_mensagem_pck.exibir_mensagem_abort(363641);
	end if;

	end;
end if;

END;
/


ALTER TABLE TASY.MAN_ORDEM_SERV_ARQ ADD (
  CONSTRAINT MANORSQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERV_ARQ ADD (
  CONSTRAINT MANORSQ_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MAN_ORDEM_SERV_ARQ TO NIVEL_1;

