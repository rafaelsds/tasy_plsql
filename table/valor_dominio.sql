ALTER TABLE TASY.VALOR_DOMINIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VALOR_DOMINIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.VALOR_DOMINIO
(
  CD_DOMINIO                NUMBER(5)           NOT NULL,
  VL_DOMINIO                VARCHAR2(15 BYTE)   NOT NULL,
  DS_VALOR_DOMINIO          VARCHAR2(255 BYTE)  NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_APRESENT           NUMBER(3),
  CD_EXP_VALOR_DOMINIO      NUMBER(10),
  DS_VALOR_DOMINIO_CLIENTE  VARCHAR2(255 BYTE),
  IE_DELPHI                 VARCHAR2(1 BYTE),
  IE_JAVA                   VARCHAR2(1 BYTE),
  IE_HTML5                  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.VALOR_DOMINIO.CD_DOMINIO IS 'Codigo do Dominio';

COMMENT ON COLUMN TASY.VALOR_DOMINIO.VL_DOMINIO IS 'Valor do Dominio';

COMMENT ON COLUMN TASY.VALOR_DOMINIO.DS_VALOR_DOMINIO IS 'Descricao do valor do dominio';

COMMENT ON COLUMN TASY.VALOR_DOMINIO.DT_ATUALIZACAO IS 'Data Atualizażao';

COMMENT ON COLUMN TASY.VALOR_DOMINIO.NM_USUARIO IS 'Usuario Atualizador';


CREATE INDEX TASY.VALDOMI_DOMINIO_FK_I ON TASY.VALOR_DOMINIO
(CD_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    40
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.VALDOMI_PK ON TASY.VALOR_DOMINIO
(CD_DOMINIO, VL_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.VALOR_D_DICEXPR_FK_I ON TASY.VALOR_DOMINIO
(CD_EXP_VALOR_DOMINIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.VALOR_DOMINIO_tp  after update ON TASY.VALOR_DOMINIO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'CD_DOMINIO='||to_char(:old.CD_DOMINIO)||'#@#@VL_DOMINIO='||to_char(:old.VL_DOMINIO);gravar_log_alteracao(substr(:old.CD_EXP_VALOR_DOMINIO,1,4000),substr(:new.CD_EXP_VALOR_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXP_VALOR_DOMINIO',ie_log_w,ds_w,'VALOR_DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DOMINIO,1,4000),substr(:new.CD_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOMINIO',ie_log_w,ds_w,'VALOR_DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_VALOR_DOMINIO_CLIENTE,1,4000),substr(:new.DS_VALOR_DOMINIO_CLIENTE,1,4000),:new.nm_usuario,nr_seq_w,'DS_VALOR_DOMINIO_CLIENTE',ie_log_w,ds_w,'VALOR_DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_VALOR_DOMINIO,1,4000),substr(:new.DS_VALOR_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_VALOR_DOMINIO',ie_log_w,ds_w,'VALOR_DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'VALOR_DOMINIO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_DOMINIO,1,4000),substr(:new.VL_DOMINIO,1,4000),:new.nm_usuario,nr_seq_w,'VL_DOMINIO',ie_log_w,ds_w,'VALOR_DOMINIO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.VALOR_DOMINIO ADD (
  CONSTRAINT VALDOMI_PK
 PRIMARY KEY
 (CD_DOMINIO, VL_DOMINIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          320K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.VALOR_DOMINIO ADD (
  CONSTRAINT VALDOMI_DOMINIO_FK 
 FOREIGN KEY (CD_DOMINIO) 
 REFERENCES TASY.DOMINIO (CD_DOMINIO)
    ON DELETE CASCADE,
  CONSTRAINT VALOR_D_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_VALOR_DOMINIO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.VALOR_DOMINIO TO NIVEL_1;

