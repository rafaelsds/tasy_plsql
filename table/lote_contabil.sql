ALTER TABLE TASY.LOTE_CONTABIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_CONTABIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_CONTABIL
(
  NR_LOTE_CONTABIL       NUMBER(10)             NOT NULL,
  DT_REFERENCIA          DATE                   NOT NULL,
  CD_TIPO_LOTE_CONTABIL  NUMBER(2)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE),
  VL_DEBITO              NUMBER(15,2),
  VL_CREDITO             NUMBER(15,2),
  NM_USUARIO_ORIGINAL    VARCHAR2(15 BYTE),
  DT_INTEGRACAO          DATE,
  DT_CONSISTENCIA        DATE,
  DT_ATUALIZACAO_SALDO   DATE,
  NR_SEQ_MES_REF         NUMBER(10),
  DT_PROCESSO            DATE,
  IE_ENCERRAMENTO        VARCHAR2(1 BYTE)       NOT NULL,
  NR_LOTE_EXTERNO        VARCHAR2(20 BYTE),
  DT_GERACAO_LOTE        DATE,
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  DT_PREV_REVERSAO       DATE,
  DT_REVERSAO            DATE,
  NR_LOTE_ORIGEM         NUMBER(10),
  IE_EXPORTADO           VARCHAR2(2 BYTE),
  DT_INICIO_GERACAO      DATE,
  DT_INICIO_REF          DATE,
  DT_FIM_REF             DATE,
  IE_STATUS_ORIGEM       VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          768K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOTCONT_CTBMERE_FK_I ON TASY.LOTE_CONTABIL
(NR_SEQ_MES_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTCONT_ESTABEL_FK_I ON TASY.LOTE_CONTABIL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTCONT_LOTCONT_FK_I ON TASY.LOTE_CONTABIL
(NR_LOTE_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOTCONT_PK ON TASY.LOTE_CONTABIL
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LOTCONT_TIPLOCO_FK_I ON TASY.LOTE_CONTABIL
(CD_TIPO_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LOTE_CONTABIL_ATUAL
before insert or update ON TASY.LOTE_CONTABIL for each row
declare

qt_registro_w		number(10);
cd_empresa_w		ctb_mes_ref.cd_empresa%type;
dt_referencia_w		ctb_mes_ref.dt_referencia%type;

begin

if (nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') then
	begin
	if (nvl(:new.nr_seq_mes_ref, 0) <> 0) then

		select  a.cd_empresa,
			a.dt_referencia
		into	cd_empresa_w,
			dt_referencia_w
		from    ctb_mes_ref a
		where   a.nr_sequencia = :new.nr_seq_mes_ref;

		if	(obter_empresa_estab(:new.cd_estabelecimento) <>  cd_empresa_w) then
			wheb_mensagem_pck.exibir_mensagem_abort(1076351);
		end if;

		if (to_char(:new.dt_referencia, 'mm/yyyy') <> to_char(dt_referencia_w, 'mm/yyyy')) then
			wheb_mensagem_pck.exibir_mensagem_abort(1153801,'DT_INICIAL=' 	|| to_char(pkg_date_utils.start_of(dt_referencia_w, 'MONTH'),'dd/mm/yyyy') ||
									';DT_FINAL=' 	|| to_char(pkg_date_utils.end_of(dt_referencia_w, 'MONTH'),'dd/mm/yyyy'));
		end if;
	end if;

	if	(updating) then
		if	(:new.nr_seq_mes_ref is null) and
			(:old.nr_seq_mes_ref is not null) then

			select	count(*)
			into	qt_registro_w
			from 	ctb_movimento
			where 	nr_lote_contabil = :new.nr_lote_contabil;

			if	(qt_registro_w > 0) then
				/* O lote possui movimento na contabilidade, reatualize a consulta da tela.*/
				wheb_mensagem_pck.exibir_mensagem_abort(266446);
			end if;
		end if;
	end if;
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.lote_contabil_delete
before delete ON TASY.LOTE_CONTABIL for each row
declare
ds_lote_w		varchar2(255);
ds_mensagem_w		varchar2(4000);
nr_lote_contabil_w		number(10);
ds_enter_w		varchar2(3) := chr(13) || chr(10);

Cursor C01 is
select	 substr(WHEB_MENSAGEM_PCK.get_texto(818839, 'NR_LOTE_CONTABIL=' || nr_lote_contabil ||
												';CD_ESTABELECIMENTO=' || cd_estabelecimento ||
												';NM_ESTABELECIMENTO=' || obter_nome_estabelecimento(cd_estabelecimento) ||
												';DT_REFERENCIA=' || to_char(dt_referencia,'dd/mm/yyyy')),1,255) ds_lote
from	 lote_contabil
where	 nr_lote_origem = nr_lote_contabil_w
order by ds_lote;

pragma autonomous_transaction;
begin
nr_lote_contabil_w	:= :old.nr_lote_contabil;

open C01;
loop
fetch C01 into
	ds_lote_w;
exit when C01%notfound;
	begin
	ds_mensagem_w := substr(ds_mensagem_w || ds_lote_w || ds_enter_w,1,4000);
	end;
end loop;
close C01;

if	(nvl(ds_mensagem_w,'X') <> 'X') then
	begin
	/* 'Este lote n�o pode ser exclu�do pois existem lotes dependentes!' */
	ds_mensagem_w := substr(WHEB_MENSAGEM_PCK.get_texto(818840) || ds_enter_w || ds_enter_w ||
				WHEB_MENSAGEM_PCK.get_texto(818842) /*'Lotes dependentes: '*/ || ds_enter_w ||
				ds_mensagem_w,1,4000);
	wheb_mensagem_pck.exibir_mensagem_abort(184609,'DS_ERRO_W='||ds_mensagem_w);
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.LOTE_CONTABIL_tp  after update ON TASY.LOTE_CONTABIL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_LOTE_CONTABIL);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_TIPO_LOTE_CONTABIL,1,4000),substr(:new.CD_TIPO_LOTE_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOTE_CONTABIL',ie_log_w,ds_w,'LOTE_CONTABIL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOTE_CONTABIL ADD (
  CONSTRAINT LOTCONT_PK
 PRIMARY KEY
 (NR_LOTE_CONTABIL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_CONTABIL ADD (
  CONSTRAINT LOTCONT_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_ORIGEM) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT LOTCONT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT LOTCONT_TIPLOCO_FK 
 FOREIGN KEY (CD_TIPO_LOTE_CONTABIL) 
 REFERENCES TASY.TIPO_LOTE_CONTABIL (CD_TIPO_LOTE_CONTABIL),
  CONSTRAINT LOTCONT_CTBMERE_FK 
 FOREIGN KEY (NR_SEQ_MES_REF) 
 REFERENCES TASY.CTB_MES_REF (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOTE_CONTABIL TO NIVEL_1;

