ALTER TABLE TASY.PLS_AME_REGRA_GER_CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_AME_REGRA_GER_CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_AME_REGRA_GER_CONTRATO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_GER_ARQ       NUMBER(10)         NOT NULL,
  NR_SEQ_CONTRATO            NUMBER(10),
  IE_REGRA_EXCECAO           VARCHAR2(1 BYTE)   NOT NULL,
  NR_CONTRATO_PRINCIPAL      NUMBER(10),
  NR_SEQ_CONTRATO_PRINCIPAL  NUMBER(10),
  NR_CONTRATO                NUMBER(10),
  NR_SEQ_PAGADOR             NUMBER(10),
  NR_SEQ_CLASSIF_ITENS       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSAMGC_PK ON TASY.PLS_AME_REGRA_GER_CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAMGC_PLCONPAG_FK_I ON TASY.PLS_AME_REGRA_GER_CONTRATO
(NR_SEQ_PAGADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAMGC_PLSAMGA_FK_I ON TASY.PLS_AME_REGRA_GER_CONTRATO
(NR_SEQ_REGRA_GER_ARQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAMGC_PLSCIPA_FK_I ON TASY.PLS_AME_REGRA_GER_CONTRATO
(NR_SEQ_CLASSIF_ITENS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAMGC_PLSCONT_FK_I ON TASY.PLS_AME_REGRA_GER_CONTRATO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAMGC_PLSCONT_FK2_I ON TASY.PLS_AME_REGRA_GER_CONTRATO
(NR_SEQ_CONTRATO_PRINCIPAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_ame_regra_ger_contrato_att
before insert or update ON TASY.PLS_AME_REGRA_GER_CONTRATO for each row
declare

nr_seq_contrato_w	pls_contrato.nr_sequencia%type;
nr_seq_pagador_w	number(10);

begin

if	(:new.nr_contrato is not null) then
	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato = :new.nr_contrato;

	if	(nr_seq_contrato_w is not null) then
		:new.nr_seq_contrato := nr_seq_contrato_w;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(853575); --O contrato informado nao existe. Verifique!
	end if;
else
	:new.nr_seq_contrato	:= null;
end if;

if	(:new.nr_contrato_principal is not null) then
	select	max(nr_sequencia)
	into	nr_seq_contrato_w
	from	pls_contrato
	where	nr_contrato = :new.nr_contrato_principal;

	if	(nr_seq_contrato_w is not null) then
		:new.nr_seq_contrato_principal := nr_seq_contrato_w;
	else
		wheb_mensagem_pck.exibir_mensagem_abort(853575); --O contrato informado nao existe. Verifique!
	end if;
else
	:new.nr_seq_contrato_principal	:= null;
end if;

if	(:new.nr_seq_pagador is not null) then
	select	count(1)
	into	nr_seq_pagador_w
	from	pls_contrato_pagador
	where	nr_sequencia = :new.nr_seq_pagador;

	if	(nr_seq_pagador_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(1103531); --O pagador informado nao existe. Verifique!
	end if;
else
	:new.nr_seq_pagador 	:= null;
end if;

end;
/


ALTER TABLE TASY.PLS_AME_REGRA_GER_CONTRATO ADD (
  CONSTRAINT PLSAMGC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_AME_REGRA_GER_CONTRATO ADD (
  CONSTRAINT PLSAMGC_PLCONPAG_FK 
 FOREIGN KEY (NR_SEQ_PAGADOR) 
 REFERENCES TASY.PLS_CONTRATO_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSAMGC_PLSCIPA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ITENS) 
 REFERENCES TASY.PLS_CLASSIF_ITENS_PAGADOR (NR_SEQUENCIA),
  CONSTRAINT PLSAMGC_PLSAMGA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_GER_ARQ) 
 REFERENCES TASY.PLS_AME_REGRA_GER_ARQ (NR_SEQUENCIA),
  CONSTRAINT PLSAMGC_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSAMGC_PLSCONT_FK2 
 FOREIGN KEY (NR_SEQ_CONTRATO_PRINCIPAL) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_AME_REGRA_GER_CONTRATO TO NIVEL_1;

