ALTER TABLE TASY.APLICACAO_TASY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.APLICACAO_TASY CASCADE CONSTRAINTS;

CREATE TABLE TASY.APLICACAO_TASY
(
  CD_APLICACAO_TASY      VARCHAR2(10 BYTE)      NOT NULL,
  DS_APLICACAO_TASY      VARCHAR2(50 BYTE)      NOT NULL,
  CD_VERSAO_ATUAL        VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  IE_STATUS_APLICACAO    VARCHAR2(1 BYTE),
  IE_BANCO               VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  IE_ATUALIZACAO_VERSAO  VARCHAR2(1 BYTE),
  CD_EXP_APLICACAO       NUMBER(10),
  CD_BUILD_APLICACAO     NUMBER(10)             DEFAULT null,
  IE_TIPO_APLICACAO      VARCHAR2(1 BYTE)       DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.APLTASY_DICEXPR_FK_I ON TASY.APLICACAO_TASY
(CD_EXP_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.APLTASY_PK ON TASY.APLICACAO_TASY
(CD_APLICACAO_TASY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.aplicacao_tasy_update
before update ON TASY.APLICACAO_TASY for each row
declare
qt_registro_w 	number(10);
nm_user_w 	varchar2(10);
ds_Sep_bv_w 	varchar2(50);
begin
	ds_sep_bv_w := obter_separador_bv;

	obter_valor_dinamico_bv('select 1 from estabelecimento where cd_cgc = :cd_cgc and cd_estabelecimento = :cd_estabelecimento and ie_situacao = :ie_situacao',
		'cd_cgc=01950338000177'||ds_sep_bv_w||'cd_estabelecimento=1'||ds_sep_bv_w||'ie_situacao=A',qt_registro_w);

	if	( qt_registro_w > 0 ) then
		select substr(user,1,4)
		into nm_user_w
		from dual;

		if 	( nm_user_w = 'TASY' ) then
			if 	(:old.cd_versao_atual <> :new.cd_versao_atual) then
				:new.ie_status_aplicacao := 'U';
			elsif	(:old.ie_status_aplicacao = 'U') and
				(:new.ie_status_aplicacao = 'A') then
				wheb_mensagem_pck.exibir_mensagem_abort(281866);
			end if;
		end if;
	end if;

	if	(:old.ie_status_aplicacao <> :new.ie_status_aplicacao)  and
		( :old.cd_aplicacao_tasy = 'Tasy' )then
		gravar_log_status_tasy(:new.ie_status_aplicacao,'Tasy');
	end if;
	/*Liberar as funções bloqueadas no incio da atualização pela procedure (TASY_BLOQUEAR_FUNCAO_TAB_TEMP) que possuiam tabelas temporárias a serem alteradas*/
	if	(:new.ie_status_aplicacao = 'A') then
		update	funcao
		set	ie_bloqueada = 'N'
		where 	ie_bloqueada = 'S';
	end if;

	if	(:old.cd_versao_atual <> :new.cd_versao_atual) and (:old.ie_atualizacao_versao = 'S') then
		:new.ie_atualizacao_versao := 'N';
	end if;

end;
/


ALTER TABLE TASY.APLICACAO_TASY ADD (
  CONSTRAINT APLTASY_PK
 PRIMARY KEY
 (CD_APLICACAO_TASY)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.APLICACAO_TASY ADD (
  CONSTRAINT APLTASY_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_APLICACAO) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.APLICACAO_TASY TO NIVEL_1;

