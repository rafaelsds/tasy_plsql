ALTER TABLE TASY.COM_TIPO_FAQ_APROV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_TIPO_FAQ_APROV CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_TIPO_FAQ_APROV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO_FAQ      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PESSOA_APROVACAO  VARCHAR2(10 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMTPFQ_COMTIFA_FK_I ON TASY.COM_TIPO_FAQ_APROV
(NR_SEQ_TIPO_FAQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMTPFQ_COMTIFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMTPFQ_PESFISI_FK_I ON TASY.COM_TIPO_FAQ_APROV
(CD_PESSOA_APROVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMTPFQ_PK ON TASY.COM_TIPO_FAQ_APROV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMTPFQ_PK
  MONITORING USAGE;


ALTER TABLE TASY.COM_TIPO_FAQ_APROV ADD (
  CONSTRAINT COMTPFQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_TIPO_FAQ_APROV ADD (
  CONSTRAINT COMTPFQ_COMTIFA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_FAQ) 
 REFERENCES TASY.COM_TIPO_FAQ (NR_SEQUENCIA),
  CONSTRAINT COMTPFQ_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_APROVACAO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.COM_TIPO_FAQ_APROV TO NIVEL_1;

