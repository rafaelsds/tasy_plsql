ALTER TABLE TASY.COMUNIC_INTERNA_CLASSIF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMUNIC_INTERNA_CLASSIF CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMUNIC_INTERNA_CLASSIF
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DS_CLASSIFICACAO      VARCHAR2(60 BYTE)       NOT NULL,
  IE_TIPO               VARCHAR2(1 BYTE)        NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DS_COR                VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  DS_COR_FUNDO          VARCHAR2(15 BYTE),
  DS_OBSERVACAO         VARCHAR2(255 BYTE),
  DS_USUARIOS_COPIA     VARCHAR2(2000 BYTE),
  CD_EXP_CLASSIFICACAO  NUMBER(10),
  DS_COR_HTML           VARCHAR2(15 BYTE)       DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMINCL_I1 ON TASY.COMUNIC_INTERNA_CLASSIF
(IE_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMINCL_PK ON TASY.COMUNIC_INTERNA_CLASSIF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.COMUNIC_INTERNA_CLASSIF ADD (
  CONSTRAINT COMINCL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.COMUNIC_INTERNA_CLASSIF TO NIVEL_1;

