ALTER TABLE TASY.SUS_LEITO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_LEITO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_LEITO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_TIPO_LEITO        NUMBER(3)                NOT NULL,
  DT_VIGENCIA          DATE                     NOT NULL,
  QT_LEITO             NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSLEIT_PK ON TASY.SUS_LEITO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLEIT_SUSTILE_FK_I ON TASY.SUS_LEITO
(CD_TIPO_LEITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SUS_LEITO ADD (
  CONSTRAINT SUSLEIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_LEITO ADD (
  CONSTRAINT SUSLEIT_SUSTILE_FK 
 FOREIGN KEY (CD_TIPO_LEITO) 
 REFERENCES TASY.SUS_TIPO_LEITO (CD_TIPO_LEITO));

GRANT SELECT ON TASY.SUS_LEITO TO NIVEL_1;

