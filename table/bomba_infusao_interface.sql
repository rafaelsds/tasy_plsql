ALTER TABLE TASY.BOMBA_INFUSAO_INTERFACE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.BOMBA_INFUSAO_INTERFACE CASCADE CONSTRAINTS;

CREATE TABLE TASY.BOMBA_INFUSAO_INTERFACE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_UNIDADE_BASICA         VARCHAR2(10 BYTE)   NOT NULL,
  CD_UNIDADE_COMPL          VARCHAR2(10 BYTE)   NOT NULL,
  NR_SEQ_EQUIPAMENTO        NUMBER(10)          NOT NULL,
  NR_SEQ_EQUIPAMENTO_BOMBA  NUMBER(10)          NOT NULL,
  DT_STATUS                 TIMESTAMP(6)        NOT NULL,
  IE_STATUS                 VARCHAR2(2 BYTE),
  IE_MODE                   VARCHAR2(3 BYTE),
  QT_VELOCIDADE             NUMBER(15,4),
  DS_MEDICAMENTO            VARCHAR2(150 BYTE),
  QT_VOLUME                 NUMBER(15,4),
  IE_EM_USO                 VARCHAR2(1 BYTE),
  IE_ESTADO_QUESTIONAVEL    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.BOMINTER_MANEQBO_FK_I ON TASY.BOMBA_INFUSAO_INTERFACE
(NR_SEQ_EQUIPAMENTO_BOMBA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BOMINTER_MANEQUI_FK_I ON TASY.BOMBA_INFUSAO_INTERFACE
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.BOMINTER_PK ON TASY.BOMBA_INFUSAO_INTERFACE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.BOMINTER_UNIATEN_FK_I ON TASY.BOMBA_INFUSAO_INTERFACE
(CD_UNIDADE_BASICA, CD_UNIDADE_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.BOMBA_INFUSAO_INTERFACE ADD (
  CONSTRAINT BOMINTER_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.BOMBA_INFUSAO_INTERFACE ADD (
  CONSTRAINT BOMINTER_MANEQBO_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO_BOMBA) 
 REFERENCES TASY.MAN_EQUIPAMENTO_BOMBA (NR_SEQUENCIA),
  CONSTRAINT BOMINTER_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA));

