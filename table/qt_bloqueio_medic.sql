ALTER TABLE TASY.QT_BLOQUEIO_MEDIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QT_BLOQUEIO_MEDIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.QT_BLOQUEIO_MEDIC
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_MATERIAL           NUMBER(10),
  DT_INICIAL            DATE,
  DT_FINAL              DATE,
  IE_DIA_SEMANA         NUMBER(5),
  HR_INICIAL_BLOQUEIO   DATE,
  HR_FINAL_BLOQUEIO     DATE,
  IE_FERIADO            VARCHAR2(1 BYTE),
  CD_ESTABELECIMENTO    NUMBER(4),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  CD_GRUPO_MATERIAL     NUMBER(3),
  IE_BLOQUEAR           VARCHAR2(1 BYTE),
  CD_PROTOCOLO          NUMBER(10),
  NR_SEQ_MEDICACAO      NUMBER(6),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_SEQ_GRUPO_QUIMIO   NUMBER(10),
  NR_SEQ_LOCAL          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QTBLQMD_CLAMATE_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLQMD_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QTBLQMD_ESTABEL_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          168K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLQMD_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QTBLQMD_GRUMATE_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLQMD_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QTBLQMD_MATERIA_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLQMD_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.QTBLQMD_PK ON TASY.QT_BLOQUEIO_MEDIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLQMD_PK
  MONITORING USAGE;


CREATE INDEX TASY.QTBLQMD_PROMEDI_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(CD_PROTOCOLO, NR_SEQ_MEDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLQMD_PROMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QTBLQMD_PROTOCO_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(CD_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLQMD_PROTOCO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.QTBLQMD_QTLOCAL_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(NR_SEQ_LOCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QTBLQMD_QTLOGRU_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(NR_SEQ_GRUPO_QUIMIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QTBLQMD_SETATEN_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QTBLQMD_SUBMATE_FK_I ON TASY.QT_BLOQUEIO_MEDIC
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QTBLQMD_SUBMATE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.QT_BLOQUEIO_MEDIC ADD (
  CONSTRAINT QTBLQMD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QT_BLOQUEIO_MEDIC ADD (
  CONSTRAINT QTBLQMD_QTLOCAL_FK 
 FOREIGN KEY (NR_SEQ_LOCAL) 
 REFERENCES TASY.QT_LOCAL (NR_SEQUENCIA),
  CONSTRAINT QTBLQMD_QTLOGRU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_QUIMIO) 
 REFERENCES TASY.QT_LOCAL_GRUPO (NR_SEQUENCIA),
  CONSTRAINT QTBLQMD_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT QTBLQMD_PROMEDI_FK 
 FOREIGN KEY (CD_PROTOCOLO, NR_SEQ_MEDICACAO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (CD_PROTOCOLO,NR_SEQUENCIA),
  CONSTRAINT QTBLQMD_PROTOCO_FK 
 FOREIGN KEY (CD_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO (CD_PROTOCOLO),
  CONSTRAINT QTBLQMD_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT QTBLQMD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT QTBLQMD_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT QTBLQMD_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT QTBLQMD_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.QT_BLOQUEIO_MEDIC TO NIVEL_1;

