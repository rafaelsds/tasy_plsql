ALTER TABLE TASY.W_TEST_PLAN_IGNORED_PRS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_TEST_PLAN_IGNORED_PRS CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_TEST_PLAN_IGNORED_PRS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_TIPO_MUDANCA      VARCHAR2(2 BYTE),
  NR_SEQ_CASO_TESTE    NUMBER(10),
  IE_MATRIZ_RISCO      VARCHAR2(1 BYTE),
  NR_SEQ_PRODUCT_REQ   NUMBER(10),
  CD_FUNCAO            NUMBER(5),
  IE_TIPO_OPERACAO     VARCHAR2(2 BYTE),
  NR_SEQ_PENDENCY      NUMBER(10),
  DS_VERSION           VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WTPIPRS_I1 ON TASY.W_TEST_PLAN_IGNORED_PRS
(NM_USUARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WTPIPRS_PK ON TASY.W_TEST_PLAN_IGNORED_PRS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WTPIPRS_REGTCPE_FK_I ON TASY.W_TEST_PLAN_IGNORED_PRS
(NR_SEQ_PENDENCY)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_TEST_PLAN_IGNORED_PRS ADD (
  CONSTRAINT WTPIPRS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_TEST_PLAN_IGNORED_PRS ADD (
  CONSTRAINT WTPIPRS_REGTCPE_FK 
 FOREIGN KEY (NR_SEQ_PENDENCY) 
 REFERENCES TASY.REG_TC_PENDENCIES (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_TEST_PLAN_IGNORED_PRS TO NIVEL_1;

