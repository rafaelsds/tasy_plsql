ALTER TABLE TASY.CPOE_TIPO_PEDIDO_SETOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CPOE_TIPO_PEDIDO_SETOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.CPOE_TIPO_PEDIDO_SETOR
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_CPOE_TIPO_PEDIDO  NUMBER(10)           NOT NULL,
  CD_SETOR_ATENDIMENTO     NUMBER(5)            NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CPOETPS_CPTIPE_FK_I ON TASY.CPOE_TIPO_PEDIDO_SETOR
(NR_SEQ_CPOE_TIPO_PEDIDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CPOETPS_PK ON TASY.CPOE_TIPO_PEDIDO_SETOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CPOETPS_SETATEN_FK_I ON TASY.CPOE_TIPO_PEDIDO_SETOR
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CPOE_TIPO_PEDIDO_SETOR ADD (
  CONSTRAINT CPOETPS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.CPOE_TIPO_PEDIDO_SETOR ADD (
  CONSTRAINT CPOETPS_CPTIPE_FK 
 FOREIGN KEY (NR_SEQ_CPOE_TIPO_PEDIDO) 
 REFERENCES TASY.CPOE_TIPO_PEDIDO (NR_SEQUENCIA),
  CONSTRAINT CPOETPS_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.CPOE_TIPO_PEDIDO_SETOR TO NIVEL_1;

