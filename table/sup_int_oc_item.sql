ALTER TABLE TASY.SUP_INT_OC_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_OC_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_OC_ITEM
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_ITEM_OCI               NUMBER(5)           NOT NULL,
  CD_MATERIAL               NUMBER(6)           NOT NULL,
  NR_ORDEM_COMPRA           NUMBER(10),
  CD_UNIDADE_MEDIDA_COMPRA  VARCHAR2(30 BYTE)   NOT NULL,
  VL_UNITARIO_MATERIAL      NUMBER(13,4)        NOT NULL,
  QT_MATERIAL               NUMBER(13,4)        NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DS_MATERIAL_DIRETO        VARCHAR2(255 BYTE),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  CD_CENTRO_CUSTO           NUMBER(8),
  CD_CONTA_CONTABIL         VARCHAR2(20 BYTE),
  NR_COT_COMPRA             NUMBER(10),
  NR_ITEM_COT_COMPRA        NUMBER(5),
  NR_SOLIC_COMPRA           NUMBER(10),
  NR_ITEM_SOLIC_COMPRA      NUMBER(5),
  VL_IPI                    NUMBER(15,2),
  TX_IPI                    NUMBER(7,4),
  VL_FRETE                  NUMBER(15,4),
  PR_DESCONTOS              NUMBER(13,4),
  VL_DESCONTO               NUMBER(13,2),
  NR_SERIE_CONTROLE         NUMBER(5),
  NR_ORDEM_CONTROLE         NUMBER(10),
  NR_ANO_CONTROLE           NUMBER(4),
  NR_SEQ_MARCA              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINOCI_PK ON TASY.SUP_INT_OC_ITEM
(NR_SEQUENCIA, NR_ITEM_OCI)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINOCI_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUINOCI_SUINOCO_FK_I ON TASY.SUP_INT_OC_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINOCI_SUINOCO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.sup_int_oc_item_del
before delete ON TASY.SUP_INT_OC_ITEM for each row
declare
dt_liberacao_w			date;
nr_documento_externo_w		number(10);
nr_ordem_existente_w		number(10);
nr_ano_ordem_w			number(4);
nr_ordem_compra_w		number(10);
nr_serie_oc_w			number(2);

begin

select	dt_liberacao,
	nr_documento_externo
into	dt_liberacao_w,
	nr_documento_externo_w
from	sup_int_oc
where	nr_sequencia = :old.nr_sequencia;

if	(dt_liberacao_w is not null) then
	begin

	select	to_number(substr(to_char(nr_documento_externo_w),1,length(nr_documento_externo_w)-5)),
		to_number(substr(to_char(nr_documento_externo_w),length(nr_documento_externo_w)-4,1)),
		to_number(substr(to_char(nr_documento_externo_w),length(nr_documento_externo_w)-3,4))
	into	nr_ordem_existente_w,
		nr_serie_oc_w,
		nr_ano_ordem_w
	from	dual;

	select	nvl(max(nr_ordem_compra),0)
	into	nr_ordem_compra_w
	from	ordem_compra
	where	to_number(substr(to_char(nr_documento_externo),1,4)) = nr_ordem_existente_w
	and	to_number(to_char(dt_ordem_compra,'yyyy')) = nr_ano_ordem_w
	and  	to_number(substr(to_char(nr_documento_externo),length(to_char(nr_documento_externo))-4,1)) = nr_serie_oc_w
	and	dt_liberacao is null;

	delete
	from	ordem_compra_item
	where	nr_ordem_compra = nr_ordem_compra_w
	and	nr_item_oci = :old.nr_item_oci;

	end;
end if;

end;
/


ALTER TABLE TASY.SUP_INT_OC_ITEM ADD (
  CONSTRAINT SUINOCI_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_ITEM_OCI)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_INT_OC_ITEM ADD (
  CONSTRAINT SUINOCI_SUINOCO_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.SUP_INT_OC (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUP_INT_OC_ITEM TO NIVEL_1;

