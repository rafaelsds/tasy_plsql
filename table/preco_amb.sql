ALTER TABLE TASY.PRECO_AMB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRECO_AMB CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRECO_AMB
(
  CD_EDICAO_AMB           NUMBER(6)             NOT NULL,
  CD_PROCEDIMENTO         NUMBER(15)            NOT NULL,
  VL_PROCEDIMENTO         NUMBER(17,4)          NOT NULL,
  CD_MOEDA                NUMBER(3)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  VL_CUSTO_OPERACIONAL    NUMBER(17,4),
  VL_ANESTESISTA          NUMBER(17,4),
  VL_MEDICO               NUMBER(17,4),
  VL_FILME                NUMBER(17,4),
  QT_FILME                NUMBER(15,4),
  NR_AUXILIARES           NUMBER(2),
  NR_INCIDENCIA           NUMBER(2),
  QT_PORTE_ANESTESICO     NUMBER(2),
  IE_ORIGEM_PROCED        NUMBER(10)            NOT NULL,
  VL_AUXILIARES           NUMBER(17,4),
  DT_INICIO_VIGENCIA      DATE,
  NR_SEQUENCIA            NUMBER(10),
  DT_FINAL_VIGENCIA       DATE,
  DS_OBSERVACAO           VARCHAR2(4000 BYTE),
  DT_INICIO_VIGENCIA_REF  DATE,
  DT_FIM_VIGENCIA_REF     DATE,
  QT_PONTUACAO            NUMBER(10),
  CD_LEVEL                VARCHAR2(10 BYTE),
  CD_PROCEDIMENTO_LOC     VARCHAR2(50 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.PRECO_AMB.CD_EDICAO_AMB IS 'Codigo edicao AMB';

COMMENT ON COLUMN TASY.PRECO_AMB.CD_PROCEDIMENTO IS 'Codigo do Procedimento Hospitalar';

COMMENT ON COLUMN TASY.PRECO_AMB.VL_PROCEDIMENTO IS 'Valor procedimento';

COMMENT ON COLUMN TASY.PRECO_AMB.CD_MOEDA IS 'Codigo da Moeda';

COMMENT ON COLUMN TASY.PRECO_AMB.DT_ATUALIZACAO IS 'Data de atualizacao';

COMMENT ON COLUMN TASY.PRECO_AMB.NM_USUARIO IS 'Nome do usuario';

COMMENT ON COLUMN TASY.PRECO_AMB.VL_CUSTO_OPERACIONAL IS 'Valor do custo operacional';

COMMENT ON COLUMN TASY.PRECO_AMB.VL_ANESTESISTA IS 'Valor do anestesista';

COMMENT ON COLUMN TASY.PRECO_AMB.VL_MEDICO IS 'Valor do medico';

COMMENT ON COLUMN TASY.PRECO_AMB.VL_FILME IS 'Valor do filme';

COMMENT ON COLUMN TASY.PRECO_AMB.QT_FILME IS 'Quantidade de filme';

COMMENT ON COLUMN TASY.PRECO_AMB.NR_AUXILIARES IS 'Numero de auxiliares';


CREATE INDEX TASY.PREAMB_EDIAMB_FK_I ON TASY.PRECO_AMB
(CD_EDICAO_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREAMB_I1 ON TASY.PRECO_AMB
(CD_EDICAO_AMB, CD_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREAMB_I2 ON TASY.PRECO_AMB
(CD_EDICAO_AMB, CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1216K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREAMB_I3 ON TASY.PRECO_AMB
(CD_EDICAO_AMB, DT_INICIO_VIGENCIA, CD_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREAMB_I4 ON TASY.PRECO_AMB
(CD_EDICAO_AMB, DT_INICIO_VIGENCIA, DT_FINAL_VIGENCIA, CD_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREAMB_I5 ON TASY.PRECO_AMB
(CD_EDICAO_AMB, DT_INICIO_VIGENCIA_REF, DT_FIM_VIGENCIA_REF, CD_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREAMB_MOEDA_FK_I ON TASY.PRECO_AMB
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREAMB_MOEDA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PREAMB_PK ON TASY.PRECO_AMB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREAMB_PROCEDI_FK_I ON TASY.PRECO_AMB
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PREAMB_UK ON TASY.PRECO_AMB
(CD_EDICAO_AMB, CD_PROCEDIMENTO, IE_ORIGEM_PROCED, DT_INICIO_VIGENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PRECO_AMB_tp  after update ON TASY.PRECO_AMB FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_PROCEDIMENTO,1,500);gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_ANESTESISTA,1,4000),substr(:new.VL_ANESTESISTA,1,4000),:new.nm_usuario,nr_seq_w,'VL_ANESTESISTA',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_PROCEDIMENTO,1,4000),substr(:new.VL_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_PROCEDIMENTO',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EDICAO_AMB,1,4000),substr(:new.CD_EDICAO_AMB,1,4000),:new.nm_usuario,nr_seq_w,'CD_EDICAO_AMB',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_FILME,1,4000),substr(:new.VL_FILME,1,4000),:new.nm_usuario,nr_seq_w,'VL_FILME',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PORTE_ANESTESICO,1,4000),substr(:new.QT_PORTE_ANESTESICO,1,4000),:new.nm_usuario,nr_seq_w,'QT_PORTE_ANESTESICO',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_AUXILIARES,1,4000),substr(:new.NR_AUXILIARES,1,4000),:new.nm_usuario,nr_seq_w,'NR_AUXILIARES',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CUSTO_OPERACIONAL,1,4000),substr(:new.VL_CUSTO_OPERACIONAL,1,4000),:new.nm_usuario,nr_seq_w,'VL_CUSTO_OPERACIONAL',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_AUXILIARES,1,4000),substr(:new.VL_AUXILIARES,1,4000),:new.nm_usuario,nr_seq_w,'VL_AUXILIARES',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_MEDICO,1,4000),substr(:new.VL_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'VL_MEDICO',ie_log_w,ds_w,'PRECO_AMB',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.preco_amb_atual
before insert or update ON TASY.PRECO_AMB for each row
declare

begin

/* esta trigger foi criada para alimentar os campos de data de vigencia de referencia, isto por quest�es de performance
 para que n�o seja necess�rio utilizar um or is null nas rotinas que utilizam esta tabela
 o campo inicial vigencia ref � alimentado com o valor informado no campo inicial ou se este for nulo � alimentado
 com a data zero do oracle 31/12/1899, j� o campo fim vigencia ref � alimentado com o campo fim ou se o mesmo for nulo
 � alimentado com a data 31/12/3000 desta forma podemos utilizar um between ou fazer uma compara��o com estes campos
sem precisar se preocupar se o campo vai estar nulo
*/
:new.dt_inicio_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_inicio_vigencia,'I');

:new.dt_fim_vigencia_ref := pls_util_pck.obter_dt_vigencia_null(:new.dt_final_vigencia,'F');


end;
/


ALTER TABLE TASY.PRECO_AMB ADD (
  CONSTRAINT PREAMB_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          576K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PREAMB_UK
 UNIQUE (CD_EDICAO_AMB, CD_PROCEDIMENTO, IE_ORIGEM_PROCED, DT_INICIO_VIGENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRECO_AMB ADD (
  CONSTRAINT PREAMB_EDIAMB_FK 
 FOREIGN KEY (CD_EDICAO_AMB) 
 REFERENCES TASY.EDICAO_AMB (CD_EDICAO_AMB),
  CONSTRAINT PREAMB_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PREAMB_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA));

GRANT SELECT ON TASY.PRECO_AMB TO NIVEL_1;

