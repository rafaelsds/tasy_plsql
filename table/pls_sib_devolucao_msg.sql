ALTER TABLE TASY.PLS_SIB_DEVOLUCAO_MSG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SIB_DEVOLUCAO_MSG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SIB_DEVOLUCAO_MSG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_DEVOLUCAO     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_ERRO              VARCHAR2(10 BYTE),
  DS_MENSAGEM          VARCHAR2(255 BYTE),
  QT_OCORRENCIA        NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSSBDM_PK ON TASY.PLS_SIB_DEVOLUCAO_MSG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSBDM_PLSSBDV_FK_I ON TASY.PLS_SIB_DEVOLUCAO_MSG
(NR_SEQ_DEVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_SIB_DEVOLUCAO_MSG ADD (
  CONSTRAINT PLSSBDM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SIB_DEVOLUCAO_MSG ADD (
  CONSTRAINT PLSSBDM_PLSSBDV_FK 
 FOREIGN KEY (NR_SEQ_DEVOLUCAO) 
 REFERENCES TASY.PLS_SIB_DEVOLUCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_SIB_DEVOLUCAO_MSG TO NIVEL_1;

