ALTER TABLE TASY.MATERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL
(
  CD_MATERIAL                    NUMBER(6)      NOT NULL,
  DS_MATERIAL                    VARCHAR2(255 BYTE) NOT NULL,
  DS_REDUZIDA                    VARCHAR2(255 BYTE),
  CD_CLASSE_MATERIAL             NUMBER(5)      NOT NULL,
  CD_UNIDADE_MEDIDA_COMPRA       VARCHAR2(30 BYTE) NOT NULL,
  CD_UNIDADE_MEDIDA_ESTOQUE      VARCHAR2(30 BYTE) NOT NULL,
  CD_UNIDADE_MEDIDA_CONSUMO      VARCHAR2(30 BYTE) NOT NULL,
  IE_MATERIAL_ESTOQUE            VARCHAR2(1 BYTE) NOT NULL,
  IE_RECEITA                     VARCHAR2(1 BYTE) NOT NULL,
  IE_COBRA_PACIENTE              VARCHAR2(1 BYTE) NOT NULL,
  IE_BAIXA_INTEIRA               VARCHAR2(1 BYTE) NOT NULL,
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  QT_DIAS_VALIDADE               NUMBER(5),
  DT_CADASTRAMENTO               DATE           NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  NR_MINIMO_COTACAO              NUMBER(1),
  QT_ESTOQUE_MAXIMO              NUMBER(13,4),
  QT_ESTOQUE_MINIMO              NUMBER(13,4),
  QT_PONTO_PEDIDO                NUMBER(13,4),
  NR_CODIGO_BARRAS               VARCHAR2(14 BYTE),
  IE_VIA_APLICACAO               VARCHAR2(5 BYTE),
  IE_OBRIG_VIA_APLICACAO         VARCHAR2(1 BYTE),
  IE_DISPONIVEL_MERCADO          VARCHAR2(1 BYTE),
  QT_MINIMO_MULTIPLO_SOLIC       NUMBER(13,4)   NOT NULL,
  QT_CONV_COMPRA_ESTOQUE         NUMBER(13,4)   NOT NULL,
  CD_UNIDADE_MEDIDA_SOLIC        VARCHAR2(30 BYTE),
  IE_PRESCRICAO                  VARCHAR2(1 BYTE),
  CD_KIT_MATERIAL                NUMBER(5),
  QT_CONVERSAO_MG                NUMBER(15,6),
  IE_TIPO_MATERIAL               VARCHAR2(3 BYTE) NOT NULL,
  CD_MATERIAL_GENERICO           NUMBER(6),
  IE_PADRONIZADO                 VARCHAR2(1 BYTE),
  QT_CONV_ESTOQUE_CONSUMO        NUMBER(13,4)   NOT NULL,
  CD_MATERIAL_ESTOQUE            NUMBER(6)      NOT NULL,
  CD_MATERIAL_CONTA              NUMBER(6),
  IE_PRECO_COMPRA                VARCHAR2(1 BYTE),
  IE_MATERIAL_DIRETO             VARCHAR2(1 BYTE),
  IE_CONSIGNADO                  VARCHAR2(1 BYTE),
  IE_UTILIZACAO_SUS              VARCHAR2(1 BYTE),
  QT_LIMITE_PESSOA               NUMBER(15,4),
  CD_UNID_MED_LIMITE             VARCHAR2(30 BYTE),
  DS_ORIENTACAO_USO              VARCHAR2(2000 BYTE),
  IE_TIPO_FONTE_PRESCR           VARCHAR2(3 BYTE),
  IE_DIAS_UTIL_MEDIC             VARCHAR2(1 BYTE),
  DS_ORIENTACAO_USUARIO          VARCHAR2(4000 BYTE),
  CD_MEDICAMENTO                 NUMBER(10),
  IE_CONTROLE_MEDICO             NUMBER(1)      NOT NULL,
  IE_BAIXA_ESTOQ_PAC             VARCHAR2(1 BYTE) NOT NULL,
  QT_HORAS_UTIL_PAC              NUMBER(7,2),
  QT_DIA_INTERV_RESSUP           NUMBER(3),
  QT_DIA_RESSUP_FORN             NUMBER(3),
  QT_MAX_PRESCRICAO              NUMBER(15,4),
  QT_DIA_ESTOQUE_MINIMO          NUMBER(2),
  QT_CONSUMO_MENSAL              NUMBER(15,4),
  IE_CURVA_ABC                   VARCHAR2(1 BYTE),
  IE_CLASSIF_XYZ                 VARCHAR2(1 BYTE),
  IE_X                           VARCHAR2(1 BYTE),
  QT_PRIORIDADE_COML             NUMBER(5),
  CD_FABRICANTE                  VARCHAR2(80 BYTE),
  NR_SEQ_GRUPO_REC               NUMBER(10),
  CD_SISTEMA_ANT                 VARCHAR2(20 BYTE),
  NR_SEQ_FABRIC                  NUMBER(10),
  IE_BOMBA_INFUSAO               VARCHAR2(1 BYTE) NOT NULL,
  IE_DILUICAO                    VARCHAR2(1 BYTE) NOT NULL,
  IE_SOLUCAO                     VARCHAR2(1 BYTE) NOT NULL,
  CD_CLASSIF_FISCAL              VARCHAR2(80 BYTE),
  IE_MISTURA                     VARCHAR2(1 BYTE) NOT NULL,
  IE_ABRIGO_LUZ                  VARCHAR2(1 BYTE) NOT NULL,
  IE_UMIDADE_CONTROLADA          VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_FICHA_TECNICA           NUMBER(10),
  DS_PRECAUCAO_ORDEM             VARCHAR2(255 BYTE),
  CD_UNID_MED_CONCETRACAO        VARCHAR2(30 BYTE),
  CD_UNID_MED_BASE_CONC          VARCHAR2(30 BYTE),
  DS_MATERIAL_SEM_ACENTO         VARCHAR2(255 BYTE),
  QT_DIA_PROFILATICO             NUMBER(3),
  QT_DIA_TERAPEUTICO             NUMBER(3),
  NR_SEQ_FAMILIA                 NUMBER(10),
  NR_SEQ_LOCALIZACAO             NUMBER(10),
  IE_GRAVAR_OBS_PRESCR           VARCHAR2(15 BYTE) NOT NULL,
  QT_MIN_APLICACAO               NUMBER(15),
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  QT_DOSE_PRESCRICAO             NUMBER(15,3),
  CD_UNIDADE_MEDIDA_PRESCR       VARCHAR2(30 BYTE),
  CD_UNID_TERAPEUTICA            VARCHAR2(15 BYTE),
  CD_INTERVALO_PADRAO            VARCHAR2(7 BYTE),
  IE_OBRIGA_JUSTIFICATIVA        VARCHAR2(1 BYTE),
  IE_CLASSIF_MEDIC               VARCHAR2(1 BYTE),
  QT_MAX_DIA_APLIC               NUMBER(3),
  IE_ANCORA_SOLUCAO              VARCHAR2(1 BYTE),
  IE_CUSTO_BENEF                 VARCHAR2(1 BYTE),
  IE_DOSE_LIMITE                 VARCHAR2(15 BYTE),
  NR_CERTIFICADO_APROVACAO       VARCHAR2(20 BYTE),
  IE_FOTOSENSIVEL                VARCHAR2(1 BYTE),
  IE_HIGROSCOPICO                VARCHAR2(1 BYTE),
  IE_INF_ULTIMA_COMPRA           VARCHAR2(1 BYTE),
  NR_SEQ_FORMA_FARM              NUMBER(10),
  IE_EXIGE_LADO                  VARCHAR2(1 BYTE),
  IE_OBRIGA_JUST_DOSE            VARCHAR2(2 BYTE),
  IE_SEXO                        VARCHAR2(2 BYTE),
  DT_VALIDADE_CERTIFICADO_APROV  DATE,
  IE_MOSTRAR_ORIENTACAO          VARCHAR2(2 BYTE),
  IE_GERAR_LOTE                  VARCHAR2(1 BYTE),
  IE_JUSTIFICATIVA_PADRAO        VARCHAR2(1 BYTE),
  IE_TERMOLABIL                  VARCHAR2(15 BYTE),
  IE_JUSTIFICA_DIAS_UTIL         VARCHAR2(1 BYTE),
  IE_ALTO_RISCO                  VARCHAR2(1 BYTE),
  IE_MONOFASICO                  VARCHAR2(1 BYTE),
  NR_SEQ_MODELO_DIALISADOR       NUMBER(10),
  IE_FABRICANTE_DIST             VARCHAR2(1 BYTE),
  IE_EDITAR_DOSE                 VARCHAR2(1 BYTE),
  DS_HINT                        VARCHAR2(255 BYTE),
  IE_MENSAGEM_SONDA              VARCHAR2(1 BYTE),
  IE_EXTRAVAZAMENTO              VARCHAR2(15 BYTE),
  IE_GERA_LOTE_SEPARADO          VARCHAR2(1 BYTE),
  IE_CONSISTE_DUPL               VARCHAR2(1 BYTE),
  IE_APLICAR                     VARCHAR2(1 BYTE),
  QT_CONCENTRACAO_TOTAL          NUMBER(15,4),
  CD_UNID_MED_CONC_TOTAL         VARCHAR2(30 BYTE),
  QT_CONV_COMPRA_EST             NUMBER(15,4),
  CD_UNID_MEDIDA                 VARCHAR2(30 BYTE),
  QT_COMPRA_MELHOR               NUMBER(15,4),
  DS_MENSAGEM                    VARCHAR2(2000 BYTE),
  CD_TIPO_RECOMENDACAO           NUMBER(10),
  NR_REGISTRO_ANVISA             VARCHAR2(60 BYTE),
  DT_VALIDADE_REG_ANVISA         DATE,
  IE_CONSISTE_DIAS               VARCHAR2(1 BYTE),
  IE_STATUS_ENVIO                VARCHAR2(15 BYTE),
  DT_INTEGRACAO                  DATE,
  IE_FABRIC_PROPRIA              VARCHAR2(1 BYTE),
  IE_IAT                         VARCHAR2(1 BYTE),
  DT_REVISAO_FISPQ               DATE,
  NR_DIAS_JUSTIF                 NUMBER(3),
  QT_OVERFILL                    NUMBER(5,2),
  IE_OBJETIVO                    VARCHAR2(1 BYTE),
  IE_CLEARANCE                   VARCHAR2(1 BYTE),
  CD_DCB                         VARCHAR2(20 BYTE),
  NR_SEQ_DIVISAO                 NUMBER(10),
  IE_DUPLA_CHECAGEM              VARCHAR2(1 BYTE),
  NR_SEQ_DOSE_TERAP              NUMBER(15),
  QT_DOSE_TERAPEUTICA            NUMBER(15,4),
  NR_REGISTRO_MS                 VARCHAR2(20 BYTE),
  IE_MANIPULADO                  VARCHAR2(1 BYTE),
  IE_ESTERILIZAVEL               VARCHAR2(1 BYTE),
  DS_ABREV_SOLUCAO               VARCHAR2(255 BYTE),
  IE_CONSISTE_ESTOQUE            VARCHAR2(1 BYTE),
  QT_PRIORIDADE_APRES            NUMBER(5),
  IE_LATEX                       VARCHAR2(1 BYTE),
  QT_MEIA_VIDA                   NUMBER(6,3),
  CD_MATERIAL_SUBS               NUMBER(6),
  IE_PERMITE_FRACIONADO          VARCHAR2(1 BYTE),
  IE_ALTA_VIGILANCIA             VARCHAR2(1 BYTE),
  IE_NOVA_MOLECULA               VARCHAR2(1 BYTE),
  IE_TIPO_SOLUCAO                VARCHAR2(2 BYTE),
  QT_MINIMA_PLANEJ               NUMBER(15,4),
  QT_MAXIMA_PLANEJ               NUMBER(15,4),
  IE_AVALIACAO_REP               VARCHAR2(1 BYTE),
  IE_MEDIC_PACIENTE              VARCHAR2(1 BYTE),
  IE_PROTOCOLO_TEV               VARCHAR2(1 BYTE),
  IE_VOLUMOSO                    VARCHAR2(2 BYTE),
  IE_RESTRITO                    VARCHAR2(1 BYTE),
  QT_COMPRIMENTO_CM              NUMBER(15,2),
  QT_LARGURA_CM                  NUMBER(15,2),
  QT_PESO_KG                     NUMBER(15,2),
  QT_ALTURA_CM                   NUMBER(15,2),
  IE_VANCOMICINA                 VARCHAR2(1 BYTE),
  IE_EMBALAGEM_MULTIPLA          VARCHAR2(1 BYTE),
  IE_MULTIDOSE                   VARCHAR2(1 BYTE),
  IE_PACTUADO                    VARCHAR2(1 BYTE),
  IE_OBRIGA_TEMPO_APLIC          VARCHAR2(1 BYTE),
  IE_PERECIVEL                   VARCHAR2(1 BYTE),
  IE_CATALOGO                    VARCHAR2(1 BYTE),
  QT_DIA_INTERV_PROFILAXIA       NUMBER(5),
  IE_PADRAO_CPOE                 VARCHAR2(3 BYTE),
  IE_TIPO_SOLUCAO_CPOE           VARCHAR2(3 BYTE),
  CD_CNPJ_CADASTRO               VARCHAR2(14 BYTE),
  NR_SEQ_CLASSIF_RISCO           NUMBER(10),
  IE_PERMITE_LACTANTE            VARCHAR2(1 BYTE),
  IE_MEDIC_SORO_ORAL             VARCHAR2(1 BYTE),
  DS_DOSE_EMBALAGEM              VARCHAR2(15 BYTE),
  CD_UNID_MED_DOSE_EMB           VARCHAR2(30 BYTE),
  IE_TAMANHO_EMBALAGEM           VARCHAR2(15 BYTE),
  IE_UNID_MED_PADRAO             VARCHAR2(1 BYTE),
  IE_NARCOTICO                   VARCHAR2(1 BYTE),
  IE_QUIMICO_PERIGOSO            VARCHAR2(1 BYTE),
  IE_GLICOSE                     VARCHAR2(1 BYTE),
  CD_IMPRINT_CODE                VARCHAR2(255 BYTE),
  IE_DUAL_MEDIC_ROUTE            VARCHAR2(1 BYTE),
  CD_MATERIAL_ORIGEM             NUMBER(6),
  NR_SEQ_MOTIVO_SUBS             NUMBER(10),
  IE_TAPERING_DOSE               VARCHAR2(1 BYTE),
  CD_UNID_MEDIDA_NAIS            VARCHAR2(15 BYTE),
  CD_MATERIAL_SPEC_GRP           NUMBER(6),
  IE_MATERIAL_ALLOC_RULE         VARCHAR2(1 BYTE),
  QT_SPEC_GROUP_DOSE             NUMBER(12),
  IE_PERMITE_PARTIR              VARCHAR2(3 BYTE),
  CD_YJ_CODE                     VARCHAR2(12 BYTE),
  QT_ORIG_MED_TOTAL_CONC         NUMBER(15,6),
  QT_BASE_MED_TOTAL_CONC         NUMBER(15,6),
  CD_ORIG_MED_CONC_UNIT          VARCHAR2(30 BYTE),
  CD_BASE_MED_CONC_UNIT          VARCHAR2(30 BYTE),
  SI_PRECISE_CONT_INTRAVENOUS    VARCHAR2(1 BYTE),
  IE_PSYCHOTROPIC                VARCHAR2(1 BYTE),
  SI_ADD_POINTS_STERILE_PREP     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          7M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.MATERIAL.CD_MATERIAL IS 'C�digo do Material';

COMMENT ON COLUMN TASY.MATERIAL.DS_MATERIAL IS 'Descri��o do Material';

COMMENT ON COLUMN TASY.MATERIAL.DS_REDUZIDA IS 'Descri��o Reduzida do Material';

COMMENT ON COLUMN TASY.MATERIAL.CD_CLASSE_MATERIAL IS 'Codigo da Classe de Material';

COMMENT ON COLUMN TASY.MATERIAL.CD_UNIDADE_MEDIDA_COMPRA IS 'C�digo da Unidade de Medida.';

COMMENT ON COLUMN TASY.MATERIAL.CD_UNIDADE_MEDIDA_ESTOQUE IS 'C�digo da Unidade de Medida.';

COMMENT ON COLUMN TASY.MATERIAL.CD_UNIDADE_MEDIDA_CONSUMO IS 'C�digo da Unidade de Medida.';

COMMENT ON COLUMN TASY.MATERIAL.IE_MATERIAL_ESTOQUE IS 'Indica se o Material deve ser ou nao controlado pelo Estoque';

COMMENT ON COLUMN TASY.MATERIAL.IE_RECEITA IS 'Indicador de Exigencia de Receita';

COMMENT ON COLUMN TASY.MATERIAL.IE_COBRA_PACIENTE IS 'Indica se o Material pode ser cobrado do  Paciente.';

COMMENT ON COLUMN TASY.MATERIAL.IE_BAIXA_INTEIRA IS 'Indica se o Material pode ter baixa com unidade inferior a 1';

COMMENT ON COLUMN TASY.MATERIAL.IE_SITUACAO IS 'Indica situacao do Material.';

COMMENT ON COLUMN TASY.MATERIAL.QT_DIAS_VALIDADE IS 'Quantidade de dias de validade do Material';

COMMENT ON COLUMN TASY.MATERIAL.DT_CADASTRAMENTO IS 'Data do Cadastramento';

COMMENT ON COLUMN TASY.MATERIAL.NR_MINIMO_COTACAO IS 'N�mero M�nimo de Cota��es de Compra';

COMMENT ON COLUMN TASY.MATERIAL.QT_ESTOQUE_MAXIMO IS 'Quantidade Maxima do Material no Estoque';

COMMENT ON COLUMN TASY.MATERIAL.QT_ESTOQUE_MINIMO IS 'Quantidade Minima do Material no Estoque';

COMMENT ON COLUMN TASY.MATERIAL.QT_PONTO_PEDIDO IS 'Quantidade Ponto de Pedido do Material';


CREATE INDEX TASY.MATERIA_ANVFOFO_FK_I ON TASY.MATERIAL
(NR_SEQ_FORMA_FARM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_CIHMEDI_FK_I ON TASY.MATERIAL
(CD_MEDICAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_CIHMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_CLAMATE_FK_I ON TASY.MATERIAL
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_CLRISCM_FK_I ON TASY.MATERIAL
(NR_SEQ_CLASSIF_RISCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_GRURECE_FK_I ON TASY.MATERIAL
(NR_SEQ_GRUPO_REC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_GRURECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_HDTIPDI_FK_I ON TASY.MATERIAL
(NR_SEQ_MODELO_DIALISADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_HDTIPDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_INTPRES_FK_I ON TASY.MATERIAL
(CD_INTERVALO_PADRAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_I1 ON TASY.MATERIAL
(DS_MATERIAL, IE_TIPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_I14 ON TASY.MATERIAL
(CD_MATERIAL, CD_MATERIAL_GENERICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_I15 ON TASY.MATERIAL
(CD_SISTEMA_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_I16 ON TASY.MATERIAL
(CD_MATERIAL, CD_MATERIAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_I9 ON TASY.MATERIAL
(DS_MATERIAL_SEM_ACENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_KITMATE_FK_I ON TASY.MATERIAL
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_KITMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIAL_I13 ON TASY.MATERIAL
(NVL("CD_MATERIAL_GENERICO","CD_MATERIAL"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIAL_MATCONT_FK_I ON TASY.MATERIAL
(CD_MATERIAL_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIAL_MATCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIAL_MATESTO_FK_I ON TASY.MATERIAL
(CD_MATERIAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIAL_MATGENE_FK_I ON TASY.MATERIAL
(CD_MATERIAL_GENERICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIAL_SEARCH ON TASY.MATERIAL
(SUBSTRB("TASY"."TASYAUTOCOMPLETE"."TOINDEXWITHOUTACCENTS"("DS_MATERIAL"),1,4000))
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   167
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIAL_UNIMEDI_LIMITE_FK_I ON TASY.MATERIAL
(CD_UNID_MED_LIMITE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIAL_UNIMEDI_LIMITE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_MATDIVI_FK_I ON TASY.MATERIAL
(NR_SEQ_DIVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_MATDIVI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_MATENCM_FK_I ON TASY.MATERIAL
(CD_CLASSIF_FISCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_MATENCM_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_MATERIA_FK2_I ON TASY.MATERIAL
(CD_MATERIAL_SUBS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_MATERIA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_MATFABR_FK_I ON TASY.MATERIAL
(NR_SEQ_FABRIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_MATFABR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_MATFAMI_FK_I ON TASY.MATERIAL
(NR_SEQ_FAMILIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_MATLOCA_FK_I ON TASY.MATERIAL
(NR_SEQ_LOCALIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_MATLOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_MEDFITE_FK_I ON TASY.MATERIAL
(NR_SEQ_FICHA_TECNICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_MOSUMA_FK_I ON TASY.MATERIAL
(NR_SEQ_MOTIVO_SUBS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_PESJURI_FK_I ON TASY.MATERIAL
(CD_CNPJ_CADASTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATERIA_PK ON TASY.MATERIAL
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_TIPRECO_FK_I ON TASY.MATERIAL
(CD_TIPO_RECOMENDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_TIPRECO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_UNIMEDI_COMPRADO_FK_I ON TASY.MATERIAL
(CD_UNIDADE_MEDIDA_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_UNIMEDI_CONC_FK_I ON TASY.MATERIAL
(CD_UNID_MED_CONCETRACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_UNIMEDI_CONC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_UNIMEDI_CONSUMIDO_FK_I ON TASY.MATERIAL
(CD_UNIDADE_MEDIDA_CONSUMO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_UNIMEDI_FK_I ON TASY.MATERIAL
(CD_UNIDADE_MEDIDA_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_UNIMEDI_FK2_I ON TASY.MATERIAL
(CD_UNID_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          120K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_UNIMEDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_UNIMEDI_FK3_I ON TASY.MATERIAL
(CD_UNID_MED_DOSE_EMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_UNIMEDI_F6_I ON TASY.MATERIAL
(CD_UNID_MED_BASE_CONC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_UNIMEDI_F6_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_UNIMEDI_PRESCR_FK_I ON TASY.MATERIAL
(CD_UNIDADE_MEDIDA_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATERIA_UNIMEDI_PRESCR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATERIA_UNIMEDI_SOLIC_FK_I ON TASY.MATERIAL
(CD_UNIDADE_MEDIDA_SOLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          512K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MATERIA_VIAAPLI_FK_I ON TASY.MATERIAL
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.material_after_delete
after delete ON TASY.MATERIAL for each row
declare
ds_retorno_integracao_w 	clob;

begin

SELECT BIFROST.SEND_INTEGRATION(
	'material.deleted',
	'com.philips.tasy.integration.material.outbound.MaterialDeletedCallback',
	'{"code" : '||:old.cd_material||'}',
	'integration')
INTO	ds_retorno_integracao_w
FROM	dual;

end;
/


CREATE OR REPLACE TRIGGER TASY.smart_cadastro_material
after insert or update or delete ON TASY.MATERIAL for each row
declare

reg_integracao_w		gerar_int_padrao.reg_integracao;

begin
	reg_integracao_w.cd_estab_documento := wheb_usuario_pck.get_cd_estabelecimento;
	IF (((UPDATING) or (INSERTING))and (:new.CD_MEDICAMENTO is not null) and (obter_se_mat_integrado(:new.CD_MATERIAL,314) = 'N')) then
		gerar_int_padrao.gravar_integracao('314', :new.CD_MATERIAL, :new.nm_usuario, reg_integracao_w,  'CD_MATERIAL=' || :new.CD_MATERIAL || ';DS_OPERACAO=CREATE');
	ELSIF (UPDATING) and (obter_se_mat_integrado(:new.CD_MATERIAL,314) = 'S') AND (:new.ie_situacao = 'A')  THEN
		gerar_int_padrao.gravar_integracao('314', :new.CD_MATERIAL, :new.nm_usuario, reg_integracao_w,  'CD_MATERIAL=' || :new.CD_MATERIAL || ';DS_OPERACAO=UPDATE');
	ELSIF (UPDATING) and (obter_se_mat_integrado(:new.CD_MATERIAL,314) = 'S') AND (:new.ie_situacao = 'I')  THEN
		gerar_int_padrao.gravar_integracao('314', :new.CD_MATERIAL, :new.nm_usuario, reg_integracao_w,  'CD_MATERIAL=' || :new.CD_MATERIAL || ';DS_OPERACAO=DELETE');
	ELSIF (DELETING) AND (obter_se_mat_integrado(:new.CD_MATERIAL,314) = 'S') THEN
		gerar_int_padrao.gravar_integracao('314', :old.CD_MATERIAL, :old.nm_usuario, reg_integracao_w,  'CD_MATERIAL=' || :old.CD_MATERIAL || ';DS_OPERACAO=DELETE');
	END IF;
end;
/


CREATE OR REPLACE TRIGGER TASY.material_atual
before insert or update ON TASY.MATERIAL for each row
declare
qt_itens_w	number(15,0);
ie_atualiza_via_w	varchar2(1);

dt_atualizacao_w	date;

pragma autonomous_transaction;
begin

begin
dt_atualizacao_w := :new.dt_atualizacao;
dt_atualizacao_w := :new.dt_atualizacao_nrec;
exception
when others then
	begin
	:new.dt_atualizacao 	:= sysdate;
	:new.dt_atualizacao_nrec 	:= sysdate;
	end;
end;


if	(:new.cd_unidade_medida_compra = :new.cd_unidade_medida_estoque) then
	:new.qt_conv_compra_estoque		:= 1;
end if;

if	(:new.cd_unidade_medida_consumo = :new.cd_unidade_medida_estoque) then
	:new.qt_conv_estoque_consumo	:= 1;
end if;

/*retirado por fabio 18/06/2007
if	(:new.cd_unidade_medida_solic = :new.cd_unidade_medida_consumo) then
	:new.qt_minimo_multiplo_solic	:= 1;
end if;
*/

if	(:new.ie_cobra_paciente = 'N') then
	:new.cd_material_conta		:= null;
end if;

if	(:new.cd_unidade_medida_estoque <> :old.cd_unidade_medida_estoque) or
	(:new.cd_unidade_medida_consumo <> :old.cd_unidade_medida_consumo) or
	(:new.qt_conv_estoque_consumo <> :old.qt_conv_estoque_consumo) then
	begin
	if	(:new.cd_unidade_medida_estoque <> :old.cd_unidade_medida_estoque) or
		(:new.cd_unidade_medida_consumo <> :old.cd_unidade_medida_consumo) then
		begin
		select 	count(*)
		into	qt_itens_w
		from	movimento_estoque
		where	cd_material	= :new.cd_material;

		if	(qt_itens_w > 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(266245);
			--'Existe movimento de estoque. ' || chr(10) ||
			--' As unidades de consumo e estoque n�o podem ser alteradas');
		end if;
		end;
	end if;

	select 	count(*)
	into	qt_itens_w
	from	requisicao_material_pendente_v
	where	cd_material	= :new.cd_material;

	if	(qt_itens_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(266246);
		--'Existe requisi��es pendentes para este material. ' || chr(10) ||
		--' A convers�o de estoque x consumo e as unidades de consumo e estoque n�o podem ser alteradas');
	end if;
	end;
end if;

if	(:new.ds_material_sem_acento is null) or
	(:new.ds_material <> :old.ds_material)  then
	:new.ds_material_sem_acento		:= upper( elimina_acentuacao(:new.ds_material));
end if;

if	(length(:new.ds_material) < 3) then
	wheb_mensagem_pck.exibir_mensagem_abort(266249);
	--'A descri��o do material deve ter no m�nimo 3 caracteres');
end if;

if	(:new.ie_controle_medico = 4) then
	if	(:new.qt_dia_profilatico is null) or
		(:new.qt_dia_terapeutico is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(266250);
		--'Os dias de uso profil�tico e terapeutico devem estar informdos');
	end if;
end if;

if	((:new.ie_dias_util_medic is null) or
	 (:new.ie_dias_util_medic = 'N')) and
	(:new.ie_dias_util_medic <> :old.ie_dias_util_medic) and
	(:new.ie_controle_medico <> 0) and
	(:new.ie_controle_medico <> :old.ie_controle_medico) then
	wheb_mensagem_pck.exibir_mensagem_abort(266251,'CD_MATERIAL=' || :new.cd_material);
	--'Material ' || :new.cd_material || chr(13) ||
	--'N�o pode haver controle de antimicrobiano sem controle de dias de utiliza��o');
end if;

obter_param_usuario(1580, 2, obter_perfil_ativo,:new.nm_usuario,0,ie_atualiza_via_w);

if	(ie_atualiza_via_w = 'S') and
	(:new.nr_seq_forma_farm is not null) and
	((:old.nr_seq_forma_farm is null) or (:new.nr_seq_forma_farm <> :old.nr_seq_forma_farm)) then

	atualiza_via_forma_farm(:new.cd_material, :new.nr_seq_forma_farm, :new.nm_usuario);

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_update
before update ON TASY.MATERIAL for each row
declare
ie_possui_reg_w		number(5);
cd_estabelecimento_w	number(5);
ie_estoque_lote_mat_control_w   material_estab.ie_estoque_lote%type;
ie_estoque_lote_mat_w   material_estab.ie_estoque_lote%type;
ie_parametro_17_w	varchar2(255) := 'N';

begin
select	count(*)
into	ie_possui_reg_w
from	material_tipo_local_est
where	cd_material = :old.cd_material;

select	min(cd_estabelecimento)
into	cd_estabelecimento_w
from	material_estab
where	cd_material	= :old.cd_material;

if	(cd_estabelecimento_w is not null) then
	if	(ie_possui_reg_w > 0) then
		update	material_estab
		set	ie_revisar	= 'R'
		where	cd_material	= :old.cd_material;

		gerar_comunic_revisao_mat(:old.cd_material, null, :new.dt_atualizacao,cd_Estabelecimento_w, :new.nm_usuario);
	end if;
end if;

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;
Obter_Param_Usuario(9900, 17, obter_perfil_ativo, :new.nm_usuario, cd_estabelecimento_w, ie_parametro_17_w);

if	(nvl(ie_parametro_17_w,'N') = 'S') then
	update	material_estab
	set	nr_registro_anvisa	= nvl(:new.nr_registro_anvisa,:old.nr_registro_anvisa),
		dt_validade_reg_anvisa	= nvl(:new.dt_validade_reg_anvisa,:old.dt_validade_reg_anvisa)
	where	cd_material		= :new.cd_material;
end if;

if (:new.cd_material_estoque <> :old.cd_material_estoque and :new.cd_material_estoque <> :new.cd_material) then
    begin
        select ie_estoque_lote
        into ie_estoque_lote_mat_control_w
        from material_estab
        where cd_material = :new.cd_material_estoque
        and cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
    exception
    when others then
        ie_estoque_lote_mat_control_w := 'N';
    end;

    begin
        select ie_estoque_lote
        into ie_estoque_lote_mat_w
        from material_estab
        where cd_material = :new.cd_material
        and cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;
    exception
    when others then
        ie_estoque_lote_mat_w := ie_estoque_lote_mat_control_w;
    end;

    if (ie_estoque_lote_mat_control_w <> ie_estoque_lote_mat_w) then
         Wheb_mensagem_pck.exibir_mensagem_abort(1140301);
    end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_afatual
after insert or update ON TASY.MATERIAL for each row
declare

begin
if	(1 = 2) then --De acordo com OS 1704282, o usu�rio ir� cadastrar os princ�pios ativos adicionais, n�o � necess�rio a trigger gerar eles automaticamente. Tamb�m ser� importado via ABDATA (GERMANY)

if	((inserting) and (:new.nr_seq_ficha_tecnica is not null)) or
	((updating) and (nvl(:old.nr_seq_ficha_tecnica, 0) <> nvl(:new.nr_seq_ficha_tecnica, 0))) then
	begin
	delete	material_princ_ativo
	where	cd_material = :new.cd_material;

	insert into material_princ_ativo(
		nr_sequencia,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		cd_material,
		nr_seq_medic_ficha_tecnica,
		qt_conversao_mg,
		cd_unid_med_concetracao,
		cd_unid_med_base_conc,
		qt_concentracao_total,
		cd_unid_med_conc_total)
	select	material_princ_ativo_seq.nextval,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		:new.cd_material,
		nr_sequencia,
		null qt_conversao_mg,
		null cd_unid_med_concetracao,
		null cd_unid_med_base_conc,
		null qt_concentracao_total,
		null cd_unid_med_conc_total
	from	medic_ficha_tecnica
	where	nr_seq_superior = :new.nr_seq_ficha_tecnica;
	end;
end if;

end if;

if	(updating) and
	((nvl(:new.cd_material_generico,0) <> nvl(:old.cd_material_generico,0)) or
	(nvl(:new.cd_unidade_medida_consumo,'X') <> nvl(:old.cd_unidade_medida_consumo,'X')) or
	(nvl(:new.ie_tipo_material,'X') <> nvl(:old.ie_tipo_material,'X')) or
	(nvl(:new.ds_material,'X') <> nvl(:old.ds_material,'X')) or
	(nvl(:new.ie_situacao,'X') <> nvl(:old.ie_situacao,'X'))) then
	gerar_int_dankia_pck.dankia_atualiza_material(:old.cd_material,:new.cd_material_generico,:new.cd_unidade_medida_consumo,:new.ie_tipo_material,:new.ie_situacao,:new.ds_material,:new.nm_usuario);
end if;

end material_afatual;
/


CREATE OR REPLACE TRIGGER TASY.MATERIAL_tp  after update ON TASY.MATERIAL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_MATERIAL);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_PADRAO_CPOE,1,4000),substr(:new.IE_PADRAO_CPOE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRAO_CPOE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_INTERV_PROFILAXIA,1,4000),substr(:new.QT_DIA_INTERV_PROFILAXIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_INTERV_PROFILAXIA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ANCORA_SOLUCAO,1,4000),substr(:new.IE_ANCORA_SOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ANCORA_SOLUCAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CUSTO_BENEF,1,4000),substr(:new.IE_CUSTO_BENEF,1,4000),:new.nm_usuario,nr_seq_w,'IE_CUSTO_BENEF',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VIA_APLICACAO,1,4000),substr(:new.IE_VIA_APLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIA_APLICACAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIG_VIA_APLICACAO,1,4000),substr(:new.IE_OBRIG_VIA_APLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIG_VIA_APLICACAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DISPONIVEL_MERCADO,1,4000),substr(:new.IE_DISPONIVEL_MERCADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DISPONIVEL_MERCADO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONV_COMPRA_ESTOQUE,1,4000),substr(:new.QT_CONV_COMPRA_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONV_COMPRA_ESTOQUE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_SOLIC,1,4000),substr(:new.CD_UNIDADE_MEDIDA_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_SOLIC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRESCRICAO,1,4000),substr(:new.IE_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESCRICAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONVERSAO_MG,1,4000),substr(:new.QT_CONVERSAO_MG,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONVERSAO_MG',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_MATERIAL,1,4000),substr(:new.IE_TIPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_MATERIAL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_GENERICO,1,4000),substr(:new.CD_MATERIAL_GENERICO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_GENERICO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONV_ESTOQUE_CONSUMO,1,4000),substr(:new.QT_CONV_ESTOQUE_CONSUMO,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONV_ESTOQUE_CONSUMO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRECO_COMPRA,1,4000),substr(:new.IE_PRECO_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRECO_COMPRA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIGNADO,1,4000),substr(:new.IE_CONSIGNADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIGNADO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIAS_UTIL_MEDIC,1,4000),substr(:new.IE_DIAS_UTIL_MEDIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIAS_UTIL_MEDIC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LIMITE_PESSOA,1,4000),substr(:new.QT_LIMITE_PESSOA,1,4000),:new.nm_usuario,nr_seq_w,'QT_LIMITE_PESSOA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO_USUARIO,1,4000),substr(:new.DS_ORIENTACAO_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO_USUARIO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MATERIAL,1,4000),substr(:new.DS_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'DS_MATERIAL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_REDUZIDA,1,4000),substr(:new.DS_REDUZIDA,1,4000),:new.nm_usuario,nr_seq_w,'DS_REDUZIDA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_ESTOQUE,1,4000),substr(:new.CD_UNIDADE_MEDIDA_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_ESTOQUE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MATERIAL_ESTOQUE,1,4000),substr(:new.IE_MATERIAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_MATERIAL_ESTOQUE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COBRA_PACIENTE,1,4000),substr(:new.IE_COBRA_PACIENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_COBRA_PACIENTE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_VALIDADE,1,4000),substr(:new.QT_DIAS_VALIDADE,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_VALIDADE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_CADASTRAMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_CADASTRAMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_CADASTRAMENTO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICAMENTO,1,4000),substr(:new.CD_MEDICAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICAMENTO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BAIXA_INTEIRA,1,4000),substr(:new.IE_BAIXA_INTEIRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_BAIXA_INTEIRA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_CONSUMO,1,4000),substr(:new.CD_UNIDADE_MEDIDA_CONSUMO,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_CONSUMO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_CONTA,1,4000),substr(:new.CD_MATERIAL_CONTA,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_CONTA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PRIORIDADE_COML,1,4000),substr(:new.QT_PRIORIDADE_COML,1,4000),:new.nm_usuario,nr_seq_w,'QT_PRIORIDADE_COML',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_FABRICANTE,1,4000),substr(:new.CD_FABRICANTE,1,4000),:new.nm_usuario,nr_seq_w,'CD_FABRICANTE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CURVA_ABC,1,4000),substr(:new.IE_CURVA_ABC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CURVA_ABC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RECEITA,1,4000),substr(:new.IE_RECEITA,1,4000),:new.nm_usuario,nr_seq_w,'IE_RECEITA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BAIXA_ESTOQ_PAC,1,4000),substr(:new.IE_BAIXA_ESTOQ_PAC,1,4000),:new.nm_usuario,nr_seq_w,'IE_BAIXA_ESTOQ_PAC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAX_PRESCRICAO,1,4000),substr(:new.QT_MAX_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAX_PRESCRICAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_FONTE_PRESCR,1,4000),substr(:new.IE_TIPO_FONTE_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_FONTE_PRESCR',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROLE_MEDICO,1,4000),substr(:new.IE_CONTROLE_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROLE_MEDICO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_COMPRA,1,4000),substr(:new.CD_UNIDADE_MEDIDA_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_COMPRA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRONIZADO,1,4000),substr(:new.IE_PADRONIZADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRONIZADO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_ESTOQUE,1,4000),substr(:new.CD_MATERIAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_ESTOQUE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_REC,1,4000),substr(:new.NR_SEQ_GRUPO_REC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_REC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FABRIC,1,4000),substr(:new.NR_SEQ_FABRIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FABRIC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_HORAS_UTIL_PAC,1,4000),substr(:new.QT_HORAS_UTIL_PAC,1,4000),:new.nm_usuario,nr_seq_w,'QT_HORAS_UTIL_PAC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMO_MULTIPLO_SOLIC,1,4000),substr(:new.QT_MINIMO_MULTIPLO_SOLIC,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMO_MULTIPLO_SOLIC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MED_LIMITE,1,4000),substr(:new.CD_UNID_MED_LIMITE,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_LIMITE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANT,1,4000),substr(:new.CD_SISTEMA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANT',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOLUCAO,1,4000),substr(:new.IE_SOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLUCAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MATERIAL,1,4000),substr(:new.CD_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MATERIAL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_BOMBA_INFUSAO,1,4000),substr(:new.IE_BOMBA_INFUSAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_BOMBA_INFUSAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DILUICAO,1,4000),substr(:new.IE_DILUICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DILUICAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FICHA_TECNICA,1,4000),substr(:new.NR_SEQ_FICHA_TECNICA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FICHA_TECNICA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PRECAUCAO_ORDEM,1,4000),substr(:new.DS_PRECAUCAO_ORDEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_PRECAUCAO_ORDEM',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GRAVAR_OBS_PRESCR,1,4000),substr(:new.IE_GRAVAR_OBS_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'IE_GRAVAR_OBS_PRESCR',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MISTURA,1,4000),substr(:new.IE_MISTURA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MISTURA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FAMILIA,1,4000),substr(:new.NR_SEQ_FAMILIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FAMILIA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOCALIZACAO,1,4000),substr(:new.NR_SEQ_LOCALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOCALIZACAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MED_BASE_CONC,1,4000),substr(:new.CD_UNID_MED_BASE_CONC,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_BASE_CONC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_APLICACAO,1,4000),substr(:new.QT_MIN_APLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_APLICACAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSIF_FISCAL,1,4000),substr(:new.CD_CLASSIF_FISCAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSIF_FISCAL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MED_CONCETRACAO,1,4000),substr(:new.CD_UNID_MED_CONCETRACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_CONCETRACAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_TERAPEUTICO,1,4000),substr(:new.QT_DIA_TERAPEUTICO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_TERAPEUTICO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIA_PROFILATICO,1,4000),substr(:new.QT_DIA_PROFILATICO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIA_PROFILATICO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_TERAPEUTICA,1,4000),substr(:new.CD_UNID_TERAPEUTICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_TERAPEUTICA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_PRESCRICAO,1,4000),substr(:new.QT_DOSE_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_PRESCRICAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA_PRESCR,1,4000),substr(:new.CD_UNIDADE_MEDIDA_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA_PRESCR',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGA_JUSTIFICATIVA,1,4000),substr(:new.IE_OBRIGA_JUSTIFICATIVA,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGA_JUSTIFICATIVA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIF_MEDIC,1,4000),substr(:new.IE_CLASSIF_MEDIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIF_MEDIC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAX_DIA_APLIC,1,4000),substr(:new.QT_MAX_DIA_APLIC,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAX_DIA_APLIC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERVALO_PADRAO,1,4000),substr(:new.CD_INTERVALO_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERVALO_PADRAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FOTOSENSIVEL,1,4000),substr(:new.IE_FOTOSENSIVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_FOTOSENSIVEL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_HIGROSCOPICO,1,4000),substr(:new.IE_HIGROSCOPICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_HIGROSCOPICO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_FORMA_FARM,1,4000),substr(:new.NR_SEQ_FORMA_FARM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FORMA_FARM',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DOSE_LIMITE,1,4000),substr(:new.IE_DOSE_LIMITE,1,4000),:new.nm_usuario,nr_seq_w,'IE_DOSE_LIMITE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_CERTIFICADO_APROVACAO,1,4000),substr(:new.NR_CERTIFICADO_APROVACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_CERTIFICADO_APROVACAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INF_ULTIMA_COMPRA,1,4000),substr(:new.IE_INF_ULTIMA_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_INF_ULTIMA_COMPRA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_LADO,1,4000),substr(:new.IE_EXIGE_LADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_LADO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGA_JUST_DOSE,1,4000),substr(:new.IE_OBRIGA_JUST_DOSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGA_JUST_DOSE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_LACTANTE,1,4000),substr(:new.IE_PERMITE_LACTANTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_LACTANTE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CNPJ_CADASTRO,1,4000),substr(:new.CD_CNPJ_CADASTRO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CNPJ_CADASTRO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF_RISCO,1,4000),substr(:new.NR_SEQ_CLASSIF_RISCO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF_RISCO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_CERTIFICADO_APROV,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_CERTIFICADO_APROV,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_CERTIFICADO_APROV',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MOSTRAR_ORIENTACAO,1,4000),substr(:new.IE_MOSTRAR_ORIENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MOSTRAR_ORIENTACAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_LOTE,1,4000),substr(:new.IE_GERAR_LOTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_LOTE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_JUSTIFICATIVA_PADRAO,1,4000),substr(:new.IE_JUSTIFICATIVA_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_JUSTIFICATIVA_PADRAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_JUSTIFICA_DIAS_UTIL,1,4000),substr(:new.IE_JUSTIFICA_DIAS_UTIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_JUSTIFICA_DIAS_UTIL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TERMOLABIL,1,4000),substr(:new.IE_TERMOLABIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TERMOLABIL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTO_RISCO,1,4000),substr(:new.IE_ALTO_RISCO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTO_RISCO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MODELO_DIALISADOR,1,4000),substr(:new.NR_SEQ_MODELO_DIALISADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MODELO_DIALISADOR',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MONOFASICO,1,4000),substr(:new.IE_MONOFASICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MONOFASICO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FABRICANTE_DIST,1,4000),substr(:new.IE_FABRICANTE_DIST,1,4000),:new.nm_usuario,nr_seq_w,'IE_FABRICANTE_DIST',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_HINT,1,4000),substr(:new.DS_HINT,1,4000),:new.nm_usuario,nr_seq_w,'DS_HINT',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EDITAR_DOSE,1,4000),substr(:new.IE_EDITAR_DOSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_EDITAR_DOSE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MED_CONC_TOTAL,1,4000),substr(:new.CD_UNID_MED_CONC_TOTAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_CONC_TOTAL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_CONV_COMPRA_EST,1,4000),substr(:new.QT_CONV_COMPRA_EST,1,4000),:new.nm_usuario,nr_seq_w,'QT_CONV_COMPRA_EST',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MEDIDA,1,4000),substr(:new.CD_UNID_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MEDIDA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_COMPRA_MELHOR,1,4000),substr(:new.QT_COMPRA_MELHOR,1,4000),:new.nm_usuario,nr_seq_w,'QT_COMPRA_MELHOR',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MENSAGEM,1,4000),substr(:new.DS_MENSAGEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_MENSAGEM',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APLICAR,1,4000),substr(:new.IE_APLICAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_APLICAR',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MENSAGEM_SONDA,1,4000),substr(:new.IE_MENSAGEM_SONDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_MENSAGEM_SONDA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERA_LOTE_SEPARADO,1,4000),substr(:new.IE_GERA_LOTE_SEPARADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERA_LOTE_SEPARADO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_DUPL,1,4000),substr(:new.IE_CONSISTE_DUPL,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_DUPL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXTRAVAZAMENTO,1,4000),substr(:new.IE_EXTRAVAZAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXTRAVAZAMENTO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VALIDADE_REG_ANVISA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VALIDADE_REG_ANVISA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VALIDADE_REG_ANVISA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REGISTRO_ANVISA,1,4000),substr(:new.NR_REGISTRO_ANVISA,1,4000),:new.nm_usuario,nr_seq_w,'NR_REGISTRO_ANVISA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_RECOMENDACAO,1,4000),substr(:new.CD_TIPO_RECOMENDACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_RECOMENDACAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_DIAS,1,4000),substr(:new.IE_CONSISTE_DIAS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_DIAS',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_STATUS_ENVIO,1,4000),substr(:new.IE_STATUS_ENVIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_STATUS_ENVIO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IAT,1,4000),substr(:new.IE_IAT,1,4000),:new.nm_usuario,nr_seq_w,'IE_IAT',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FABRIC_PROPRIA,1,4000),substr(:new.IE_FABRIC_PROPRIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_FABRIC_PROPRIA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_DIAS_JUSTIF,1,4000),substr(:new.NR_DIAS_JUSTIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_DIAS_JUSTIF',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_REVISAO_FISPQ,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_REVISAO_FISPQ,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_REVISAO_FISPQ',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLEARANCE,1,4000),substr(:new.IE_CLEARANCE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLEARANCE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_OVERFILL,1,4000),substr(:new.QT_OVERFILL,1,4000),:new.nm_usuario,nr_seq_w,'QT_OVERFILL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBJETIVO,1,4000),substr(:new.IE_OBJETIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBJETIVO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_DCB,1,4000),substr(:new.CD_DCB,1,4000),:new.nm_usuario,nr_seq_w,'CD_DCB',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_DOSE_TERAP,1,4000),substr(:new.NR_SEQ_DOSE_TERAP,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DOSE_TERAP',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DOSE_TERAPEUTICA,1,4000),substr(:new.QT_DOSE_TERAPEUTICA,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_TERAPEUTICA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DUPLA_CHECAGEM,1,4000),substr(:new.IE_DUPLA_CHECAGEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_DUPLA_CHECAGEM',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_DIVISAO,1,4000),substr(:new.NR_SEQ_DIVISAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_DIVISAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ESTERILIZAVEL,1,4000),substr(:new.IE_ESTERILIZAVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_ESTERILIZAVEL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MANIPULADO,1,4000),substr(:new.IE_MANIPULADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MANIPULADO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_REGISTRO_MS,1,4000),substr(:new.NR_REGISTRO_MS,1,4000),:new.nm_usuario,nr_seq_w,'NR_REGISTRO_MS',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSISTE_ESTOQUE,1,4000),substr(:new.IE_CONSISTE_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_ESTOQUE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ABREV_SOLUCAO,1,4000),substr(:new.DS_ABREV_SOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ABREV_SOLUCAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LATEX,1,4000),substr(:new.IE_LATEX,1,4000),:new.nm_usuario,nr_seq_w,'IE_LATEX',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PRIORIDADE_APRES,1,4000),substr(:new.QT_PRIORIDADE_APRES,1,4000),:new.nm_usuario,nr_seq_w,'QT_PRIORIDADE_APRES',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTA_VIGILANCIA,1,4000),substr(:new.IE_ALTA_VIGILANCIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTA_VIGILANCIA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL_SUBS,1,4000),substr(:new.CD_MATERIAL_SUBS,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_SUBS',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NOVA_MOLECULA,1,4000),substr(:new.IE_NOVA_MOLECULA,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOVA_MOLECULA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_FRACIONADO,1,4000),substr(:new.IE_PERMITE_FRACIONADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_FRACIONADO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MEIA_VIDA,1,4000),substr(:new.QT_MEIA_VIDA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MEIA_VIDA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_SOLUCAO,1,4000),substr(:new.IE_TIPO_SOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_SOLUCAO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MINIMA_PLANEJ,1,4000),substr(:new.QT_MINIMA_PLANEJ,1,4000),:new.nm_usuario,nr_seq_w,'QT_MINIMA_PLANEJ',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MAXIMA_PLANEJ,1,4000),substr(:new.QT_MAXIMA_PLANEJ,1,4000),:new.nm_usuario,nr_seq_w,'QT_MAXIMA_PLANEJ',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AVALIACAO_REP,1,4000),substr(:new.IE_AVALIACAO_REP,1,4000),:new.nm_usuario,nr_seq_w,'IE_AVALIACAO_REP',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VOLUMOSO,1,4000),substr(:new.IE_VOLUMOSO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VOLUMOSO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PROTOCOLO_TEV,1,4000),substr(:new.IE_PROTOCOLO_TEV,1,4000),:new.nm_usuario,nr_seq_w,'IE_PROTOCOLO_TEV',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESTRITO,1,4000),substr(:new.IE_RESTRITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESTRITO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MEDIC_PACIENTE,1,4000),substr(:new.IE_MEDIC_PACIENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_MEDIC_PACIENTE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_ALTURA_CM,1,4000),substr(:new.QT_ALTURA_CM,1,4000),:new.nm_usuario,nr_seq_w,'QT_ALTURA_CM',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_LARGURA_CM,1,4000),substr(:new.QT_LARGURA_CM,1,4000),:new.nm_usuario,nr_seq_w,'QT_LARGURA_CM',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_COMPRIMENTO_CM,1,4000),substr(:new.QT_COMPRIMENTO_CM,1,4000),:new.nm_usuario,nr_seq_w,'QT_COMPRIMENTO_CM',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PESO_KG,1,4000),substr(:new.QT_PESO_KG,1,4000),:new.nm_usuario,nr_seq_w,'QT_PESO_KG',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VANCOMICINA,1,4000),substr(:new.IE_VANCOMICINA,1,4000),:new.nm_usuario,nr_seq_w,'IE_VANCOMICINA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EMBALAGEM_MULTIPLA,1,4000),substr(:new.IE_EMBALAGEM_MULTIPLA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EMBALAGEM_MULTIPLA',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MULTIDOSE,1,4000),substr(:new.IE_MULTIDOSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_MULTIDOSE',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PACTUADO,1,4000),substr(:new.IE_PACTUADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PACTUADO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_OBRIGA_TEMPO_APLIC,1,4000),substr(:new.IE_OBRIGA_TEMPO_APLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_OBRIGA_TEMPO_APLIC',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CATALOGO,1,4000),substr(:new.IE_CATALOGO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CATALOGO',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERECIVEL,1,4000),substr(:new.IE_PERECIVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERECIVEL',ie_log_w,ds_w,'MATERIAL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.fis_efd_icmsipi_mat_atual
after insert or update of ds_material ON TASY.MATERIAL for each row
declare
ie_efd_icmsipi_w	varchar2(1);

begin
obter_param_usuario(5500, 61, obter_perfil_ativo, obter_usuario_ativo , obter_estabelecimento_ativo, ie_efd_icmsipi_w);

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S' ) then
	if	(nvl(ie_efd_icmsipi_w,'N') = 'S') then
		-- REGISTRO 0200.DESCR_ITEM
		if 	(nvl(:new.ds_material,'XPTO') <> nvl(:old.ds_material,'XPTO')) then
			insert into fis_efd_icmsipi_alteracao(
				nr_sequencia,
				dt_atualizacao_nrec,
				nm_usuario_nrec,
				ie_campo_alterado,
				ds_valor_anterior,
				cd_material)
			values (fis_efd_icmsipi_alteracao_seq.nextval,
				sysdate,
				obter_usuario_ativo,
				'20', -- Dom�nio 8965 - EFD ICMS/IPI - Campo alterado
				substr(:old.ds_material,1,255),
				:new.cd_material);
		end if;
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.material_afinsert
after insert or update ON TASY.MATERIAL for each row
declare
reg_integracao_p		gerar_int_padrao.reg_integracao;
nr_sequencia_w		number(10);
qt_existe_w		number(10);
cd_grupo_material_w	number(5);
nr_doc_externo_w	varchar2(80);
nr_seq_estrut_int_w	number(10);
ie_swisslog_w		varchar2(1);
ds_param_integ_hl7_w	varchar2(4000) := '';
cd_estabelecimento_w	estabelecimento.cd_estabelecimento%type;
ds_retorno_integracao_w 	varchar2(4000);

pragma autonomous_transaction;

begin
cd_estabelecimento_w := obter_estabelecimento_ativo;

select	nvl(max(a.cd_grupo_material),0)
into	cd_grupo_material_w
from	grupo_material a,
	subgrupo_material b,
	classe_material c
where	a.cd_grupo_material = b.cd_grupo_material
and	b.cd_subgrupo_material = c.cd_subgrupo_material
and	c.cd_classe_material = :new.cd_classe_material;

select	count(*)
into	qt_existe_w
from	sup_parametro_integracao a
where	a.ie_evento = 'CM'
and	a.ie_forma = 'E'
and	a.ie_situacao = 'A';

select	nvl(max(cd_codigo),'')
into	nr_doc_externo_w
from	material_sistema_externo
where	cd_material = :new.cd_material
and	ie_sistema = 'OCO';

if	(cd_grupo_material_w > 0) then

	select	count(*)
	into	qt_existe_w
	from	sup_parametro_integracao a,
		sup_int_regra_material b
	where	a.nr_sequencia = b.nr_seq_integracao
	and	a.ie_evento = 'CM'
	and	a.ie_forma = 'E'
	and	a.ie_situacao = 'A'
	and	b.ie_situacao = 'A'
	and	b.cd_grupo_material = cd_grupo_material_w;
end if;

if	(qt_existe_w > 0) and
	(:new.nm_usuario <> 'INTEGR_TASY') then /*Para n�o enviar ao Sapiens um material que acabou de ser importado do Sapiens*/

	envia_sup_int_material(
		:new.cd_material,
		:new.ds_material,
		:new.ds_reduzida,
		:new.cd_material_generico,
		:new.cd_material_estoque,
		:new.cd_material_conta,
		:new.cd_classe_material,
		:new.cd_unidade_medida_compra,
		:new.cd_unidade_medida_estoque,
		:new.cd_unidade_medida_consumo,
		:new.cd_unidade_medida_solic,
		:new.qt_conv_compra_estoque,
		:new.qt_conv_estoque_consumo,
		:new.qt_minimo_multiplo_solic,
		:new.ie_prescricao,
		:new.ie_preco_compra,
		:new.ie_inf_ultima_compra,
		:new.ie_consignado,
		:new.ie_baixa_estoq_pac,
		:new.ie_tipo_material,
		:new.ie_padronizado,
		:new.ie_situacao,
		:new.ie_obrig_via_aplicacao,
		:new.ie_receita,
		:new.ie_cobra_paciente,
		:new.ie_disponivel_mercado,
		:new.ie_material_estoque,
		:new.ie_via_aplicacao,
		:new.qt_dia_estoque_minimo,
		:new.qt_dias_validade,
		:new.qt_dia_interv_ressup,
		:new.qt_dia_ressup_forn,
		:new.nr_seq_localizacao,
		:new.nr_seq_familia,
		:new.cd_fabricante,
		:new.cd_sistema_ant,
		:new.nm_usuario,
		nr_doc_externo_w,
		:new.ie_catalogo,
		null);
end if;

-- Rotina para envio de materiais para integra��o Swisslog
begin
select	nr_seq_estrut_int
into	nr_seq_estrut_int_w
from	parametros_farmacia
where	cd_estabelecimento = cd_estabelecimento_w;

select	consistir_se_mat_contr_estrut(nr_seq_estrut_int_w,:new.cd_material)
into	ie_swisslog_w
from	dual;
exception
when others then
	nr_seq_estrut_int_w := 0;
	ie_swisslog_w := 'N';
end;

if	(nvl(ie_swisslog_w,'N') = 'S') and
	((:new.cd_material <> :old.cd_material) or
	(:new.ds_material <> :old.ds_material) or
	(:new.ds_reduzida <> :old.ds_reduzida) or
	(:new.cd_unidade_medida_consumo <> :old.cd_unidade_medida_consumo) or
	(:new.qt_conversao_mg <> :old.qt_conversao_mg) or
	(:new.cd_unid_med_concetracao <> :old.cd_unid_med_concetracao)) then

	ds_param_integ_hl7_w := 'cd_material=' || :new.cd_material || obter_separador_bv;
	swisslog_gerar_integracao(438, ds_param_integ_hl7_w);

end if;


/*Quando alterar o material, chama a integra��o padr�o
Ao cadastrar um novo material, tem a chamada da integra��o dentro da material_estab apenas*/
if	(updating) and
	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	reg_integracao_p.ie_operacao		:=	'A';
	reg_integracao_p.cd_estab_documento	:=	wheb_usuario_pck.get_cd_estabelecimento;
	gerar_int_padrao.gravar_integracao('1', :new.cd_material,:new.nm_usuario, reg_integracao_p);
end if;

if	(inserting) then
	begin

	SELECT BIFROST.SEND_INTEGRATION(
		'material.added',
		'com.philips.tasy.integration.material.outbound.MaterialAddedCallback',
		'{"code" : '||:new.cd_material||'}',
		:new.nm_usuario)
	INTO	ds_retorno_integracao_w
	FROM	dual;
	exception
	when others then
		ds_retorno_integracao_w := null;
	end;

elsif	(updating) then
	begin

	SELECT BIFROST.SEND_INTEGRATION(
		'material.update',
		'com.philips.tasy.integration.material.outbound.MaterialUpdatedCallback',
		'{"code" : '||:new.cd_material||'}',
		:new.nm_usuario)
	INTO	ds_retorno_integracao_w
	FROM	dual;
	exception
	when others then
		ds_retorno_integracao_w := null;
	end;

end if;

commit;
end;
/


ALTER TABLE TASY.MATERIAL ADD (
  CONSTRAINT MATERIA_PK
 PRIMARY KEY
 (CD_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          448K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL ADD (
  CONSTRAINT MATERIA_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ_CADASTRO) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MATERIA_CLRISCM_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_RISCO) 
 REFERENCES TASY.CLASSIF_RISCO_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT MATERIA_UNIMEDI_FK3 
 FOREIGN KEY (CD_UNID_MED_DOSE_EMB) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIA_MOSUMA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_SUBS) 
 REFERENCES TASY.MOTIVO_SUBS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT MATERIA_MATDIVI_FK 
 FOREIGN KEY (NR_SEQ_DIVISAO) 
 REFERENCES TASY.MAT_DIVISAO (NR_SEQUENCIA),
  CONSTRAINT MATERIA_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_SUBS) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATERIAL_MATGENE_FK 
 FOREIGN KEY (CD_MATERIAL_GENERICO) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATERIA_HDTIPDI_FK 
 FOREIGN KEY (NR_SEQ_MODELO_DIALISADOR) 
 REFERENCES TASY.HD_MODELO_DIALISADOR (NR_SEQUENCIA),
  CONSTRAINT MATERIA_UNIMEDI_FK2 
 FOREIGN KEY (CD_UNID_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIA_TIPRECO_FK 
 FOREIGN KEY (CD_TIPO_RECOMENDACAO) 
 REFERENCES TASY.TIPO_RECOMENDACAO (CD_TIPO_RECOMENDACAO),
  CONSTRAINT MATERIAL_MATCONT_FK 
 FOREIGN KEY (CD_MATERIAL_CONTA) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATERIA_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_COMPRA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIA_UNIMEDI_SOLIC_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_SOLIC) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIA_UNIMEDI_COMPRADO_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_ESTOQUE) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIAL_MATESTO_FK 
 FOREIGN KEY (CD_MATERIAL_ESTOQUE) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATERIAL_UNIMEDI_LIMITE_FK 
 FOREIGN KEY (CD_UNID_MED_LIMITE) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIA_CIHMEDI_FK 
 FOREIGN KEY (CD_MEDICAMENTO) 
 REFERENCES TASY.CIH_MEDICAMENTO (CD_MEDICAMENTO),
  CONSTRAINT MATERIA_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT MATERIA_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT MATERIA_UNIMEDI_CONSUMIDO_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_CONSUMO) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIA_GRURECE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_REC) 
 REFERENCES TASY.GRUPO_RECEITA (NR_SEQUENCIA),
  CONSTRAINT MATERIA_MATFABR_FK 
 FOREIGN KEY (NR_SEQ_FABRIC) 
 REFERENCES TASY.MAT_FABRICANTE (NR_SEQUENCIA),
  CONSTRAINT MATERIA_MEDFITE_FK 
 FOREIGN KEY (NR_SEQ_FICHA_TECNICA) 
 REFERENCES TASY.MEDIC_FICHA_TECNICA (NR_SEQUENCIA),
  CONSTRAINT MATERIA_UNIMEDI_F6 
 FOREIGN KEY (CD_UNID_MED_BASE_CONC) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIA_UNIMEDI_CONC_FK 
 FOREIGN KEY (CD_UNID_MED_CONCETRACAO) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIA_MATFAMI_FK 
 FOREIGN KEY (NR_SEQ_FAMILIA) 
 REFERENCES TASY.MATERIAL_FAMILIA (NR_SEQUENCIA),
  CONSTRAINT MATERIA_MATLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCALIZACAO) 
 REFERENCES TASY.MATERIAL_LOCAL (NR_SEQUENCIA),
  CONSTRAINT MATERIA_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT MATERIA_UNIMEDI_PRESCR_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA_PRESCR) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT MATERIA_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO_PADRAO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT MATERIA_ANVFOFO_FK 
 FOREIGN KEY (NR_SEQ_FORMA_FARM) 
 REFERENCES TASY.ANVISA_FORMA_FORMACEUTICA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MATERIAL TO NIVEL_1;

