ALTER TABLE TASY.DANKIA_DISP_TRANSACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DANKIA_DISP_TRANSACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.DANKIA_DISP_TRANSACAO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  CD_BARRAS                 VARCHAR2(255 BYTE),
  DT_ATUALIZACAO            DATE                NOT NULL,
  CD_TRANSACAO              VARCHAR2(5 BYTE)    NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  CD_LOCAL_ESTOQUE          NUMBER(4)           NOT NULL,
  CD_LOCAL_ESTOQUE_DESTINO  NUMBER(4),
  CD_MOTIVO                 VARCHAR2(5 BYTE),
  QT_MATERIAL               NUMBER(15,4)        NOT NULL,
  CD_BARRAS_USUARIO         VARCHAR2(40 BYTE),
  DT_TRANSACAO              DATE                NOT NULL,
  NR_PRESCRICAO             NUMBER(14),
  NR_SEQ_PRESCRICAO         NUMBER(10),
  NR_SEQ_LOTE               NUMBER(10),
  CD_TRANSFERENCIA          VARCHAR2(20 BYTE),
  DT_LIDO                   DATE,
  IE_OPERACAO               VARCHAR2(1 BYTE)    NOT NULL,
  IE_PROCESSADO             VARCHAR2(1 BYTE)    NOT NULL,
  DS_PROCESSADO_OBSERVACAO  VARCHAR2(400 BYTE),
  NR_SEQUENCIA_DANKIA       NUMBER(14)          NOT NULL,
  DS_STACK                  VARCHAR2(2000 BYTE),
  CD_MATERIAL               NUMBER(10)          NOT NULL,
  CD_SETOR_ATENDIMENTO      NUMBER(5),
  NR_ATENDIMENTO            NUMBER(10),
  NR_SEQ_ITEM_REQ           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DANKDISTRA_APLOTE_FK_I ON TASY.DANKIA_DISP_TRANSACAO
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANKDISTRA_ATEPACI_FK_I ON TASY.DANKIA_DISP_TRANSACAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANKDISTRA_ESTABEL_FK_I ON TASY.DANKIA_DISP_TRANSACAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANKDISTRA_FK ON TASY.DANKIA_DISP_TRANSACAO
(NR_SEQUENCIA_DANKIA, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANKDISTRA_LOCESTO_FK_I ON TASY.DANKIA_DISP_TRANSACAO
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANKDISTRA_MATERIA_FK_I ON TASY.DANKIA_DISP_TRANSACAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DANKDISTRA_PK ON TASY.DANKIA_DISP_TRANSACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANKDISTRA_PRESMED_FK_I ON TASY.DANKIA_DISP_TRANSACAO
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DANKDISTRA_SETATEN_FK_I ON TASY.DANKIA_DISP_TRANSACAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.dankia_disp_transacao_insert
before insert ON TASY.DANKIA_DISP_TRANSACAO for each row
declare

cd_operacao_estoque_w			number(10);
cd_operacao_estoque_dev_w		number(10);
cd_operacao_estoque_cons_w		number(10);
cd_operacao_correspondente_w	number(10);
cd_acao_w						varchar2(1) := null;
ds_erro_w						varchar2(255) := null;
nr_sequencia_w					item_requisicao_material.nr_sequencia%type;
nr_sequencia_ww					dankia_disp_transacao.nr_sequencia%type;
/* parametros converte_codigo_barras */
cd_material_w			number(6);
qt_mat_barras_w			number(13,4);
nr_seq_lote_w			number(10);
nr_seq_lote_agrup_w		number(10);
cd_kit_mat_w			number(10);
ds_validade_w			varchar2(255);
ds_material_w			varchar2(255);
cd_unid_med_w			varchar2(30);
nr_etiqueta_lp_w		varchar2(255);
ie_entrada_saida_w		operacao_estoque.ie_entrada_saida%type;
cd_fornecedor_w			material_lote_fornec.cd_cgc_fornec%type;
/* parametros converte_codigo_barras */


Cursor C01 is
	select	a.nr_sequencia
	from	item_requisicao_material a
	where	a.nr_requisicao	= :new.cd_transferencia
	and	a.cd_material		= :new.cd_material
	and	((nr_seq_lote_w is null) or (a.nr_seq_lote_fornec = nr_seq_lote_w))
	and ((:new.nr_seq_item_req is null) or (a.nr_sequencia = :new.nr_seq_item_req))
	order by nr_sequencia;

begin
begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'N') then
	null;
end if;

:new.ie_processado := 'S';

select	dankia_disp_transacao_seq.nextval
into	:new.nr_sequencia
from	dual;

select	max(cd_operacao_transf_setor),
		max(cd_operacao_devol_paciente),
		max(cd_operacao_cons_paciente)
into	cd_operacao_estoque_w,
		cd_operacao_estoque_dev_w,
		cd_operacao_estoque_cons_w
from	parametro_estoque
where	cd_estabelecimento	= :new.cd_estabelecimento;

select	max(cd_operacao_correspondente)
into  	cd_operacao_correspondente_w
from	operacao_estoque
where	(cd_operacao_estoque  = cd_operacao_estoque_w);

cd_material_w := :new.cd_material;
if	(:new.cd_barras is not null) then
	converte_codigo_barras(	:new.cd_barras,		:new.cd_estabelecimento,	null,
				'N',			cd_material_w,		qt_mat_barras_w,
				nr_seq_lote_w,		nr_seq_lote_agrup_w,	cd_kit_mat_w,
				ds_validade_w,		ds_material_w,		cd_unid_med_w,
				nr_etiqueta_lp_w,	ds_erro_w);
end if;
if	(nr_seq_lote_w = 0) then
	nr_seq_lote_w := null;
end if;

select	max(cd_cgc_fornec)
into	cd_fornecedor_w
from 	material_lote_fornec
where 	nr_sequencia = nr_seq_lote_w;

if	(:new.cd_transacao in (10)) then
	open C01;
	loop
	fetch C01 into
		nr_sequencia_w;
	exit when C01%notfound;
		begin
		wheb_usuario_pck.set_ie_commit('N');
		gerar_movto_estoque_receb_req(:new.cd_transferencia, nr_sequencia_w, 'N', :new.qt_material, :new.nm_usuario);
		gerar_int_dankia_pck.dankia_atualiza_saldo_inv(:new.cd_material,:new.cd_barras,:new.qt_material,:new.cd_local_estoque,cd_operacao_estoque_w,:new.nm_usuario, nr_seq_lote_w, :new.cd_estabelecimento);
		wheb_usuario_pck.set_ie_commit('S');
		end;
	end loop;
	close C01;

elsif	(:new.cd_transacao in (50)) and
		(nvl(:new.cd_local_estoque,0) <> nvl(:new.cd_local_estoque_destino,0)) then
	insert into movimento_estoque(
			nr_movimento_estoque,
			nr_seq_lote_fornec,
			cd_estabelecimento,
			cd_acao,
			cd_local_estoque,
			ie_origem_documento,
			cd_operacao_estoque,
			dt_mesano_referencia,
			dt_movimento_estoque,
			cd_material,
			cd_material_estoque,
			qt_movimento,
			qt_estoque,
			dt_atualizacao,
			nm_usuario,
			cd_fornecedor)
	values(	movimento_estoque_seq.nextval,
			nr_seq_lote_w,
			:new.cd_estabelecimento,
			'1',
			:new.cd_local_estoque,
			'11',
			cd_operacao_estoque_w,
			sysdate,
			:new.dt_transacao,
			cd_material_w,
			:new.cd_material,
			:new.qt_material,
			:new.qt_material,
			sysdate,
			:new.nm_usuario,
			cd_fornecedor_w);

	insert into movimento_estoque(
			nr_movimento_estoque,
			nr_seq_lote_fornec,
			cd_estabelecimento,
			cd_acao,
			cd_local_estoque,
			ie_origem_documento,
			cd_operacao_estoque,
			dt_mesano_referencia,
			dt_movimento_estoque,
			cd_material,
			cd_material_estoque,
			qt_movimento,
			qt_estoque,
			dt_atualizacao,
			nm_usuario,
			cd_fornecedor)
	values(	movimento_estoque_seq.nextval,
			nr_seq_lote_w,
			:new.cd_estabelecimento,
			'1',
			:new.cd_local_estoque_destino,
			'11',
			cd_operacao_correspondente_w,
			sysdate,
			:new.dt_transacao,
			:new.cd_material,
			:new.cd_material,
			:new.qt_material,
			:new.qt_material,
			sysdate,
			:new.nm_usuario,
			cd_fornecedor_w);

	gerar_int_dankia_pck.dankia_atualiza_saldo_inv(:new.cd_material,:new.cd_barras,:new.qt_material,:new.cd_local_estoque,cd_operacao_estoque_w,:new.nm_usuario, nr_seq_lote_w, :new.cd_estabelecimento);
	gerar_int_dankia_pck.dankia_atualiza_saldo_inv(:new.cd_material,:new.cd_barras,:new.qt_material,:new.cd_local_estoque_destino,cd_operacao_correspondente_w,:new.nm_usuario, nr_seq_lote_w, :new.cd_estabelecimento);

	if (gerar_int_dankia_pck.get_ie_local_dankia(:new.cd_local_estoque_destino) = 'S') then
		insert into dankia_disp_item_transf(
				nr_sequencia,
				cd_estabelecimento,
				cd_transferencia,
				dt_atualizacao,
				nm_usuario,
				cd_material,
				cd_barras,
				cd_item_transferencia,
				ie_operacao,
				cd_local_estoque_origem,
				qt_material,
				cd_local_estoque_destino,
				dt_lido_dankia,
				ie_processado,
				ds_processado_observacao,
				ds_stack)
		values	(dankia_disp_item_transf_seq.nextval,
				:new.cd_estabelecimento,
				:new.cd_transferencia,
				sysdate,
				:new.nm_usuario,
				:new.cd_material,
				nvl(:new.cd_barras,:new.cd_material),
				:new.cd_material,
				'I',
				:new.cd_local_estoque,
				:new.qt_material,
				:new.cd_local_estoque_destino,
				null,
				'N',
				null,
				substr(dbms_utility.format_call_stack,1,2000));
	end if;

elsif	(:new.cd_transacao in (40)) then
	gerar_int_dankia_pck.dankia_trans_inventario(:new.cd_local_estoque,:new.cd_material,:new.qt_material,nr_seq_lote_w,:new.nm_usuario,:new.cd_estabelecimento, :new.ds_processado_observacao, cd_operacao_estoque_w);
	gerar_int_dankia_pck.dankia_atualiza_saldo_inv(:new.cd_material,:new.cd_barras,:new.qt_material,:new.cd_local_estoque,cd_operacao_estoque_w,:new.nm_usuario, nr_seq_lote_w, :new.cd_estabelecimento);
elsif	(:new.cd_transacao in (30)) then
	dankia_lancamento_material('E',:new.nr_atendimento,:new.cd_local_estoque,:new.cd_material,:new.qt_material,sysdate,null,:new.nr_prescricao,:new.nr_seq_prescricao,:new.nr_seq_lote,null,ds_erro_w,:new.cd_barras,:new.nm_usuario);
	gerar_int_dankia_pck.dankia_atualiza_saldo_inv(:new.cd_material,:new.cd_barras,:new.qt_material,:new.cd_local_estoque,cd_operacao_estoque_w,:new.nm_usuario, nr_seq_lote_w, :new.cd_estabelecimento);
elsif	(:new.cd_transacao in (20)) then
	dankia_lancamento_material('I',:new.nr_atendimento,:new.cd_local_estoque,:new.cd_material,:new.qt_material,sysdate,null,:new.nr_prescricao,:new.nr_seq_prescricao,:new.nr_seq_lote,null,ds_erro_w,:new.cd_barras,:new.nm_usuario);
	gerar_int_dankia_pck.dankia_atualiza_saldo_inv(:new.cd_material,:new.cd_barras,:new.qt_material,:new.cd_local_estoque,cd_operacao_estoque_w,:new.nm_usuario, nr_seq_lote_w, :new.cd_estabelecimento);

elsif	(:new.cd_transacao in (41) and
		(:new.cd_motivo is not null)) then

	select	max(ie_entrada_saida)
	into	ie_entrada_saida_w
	from	operacao_estoque
	where	cd_operacao_estoque = :new.cd_motivo;

	if	(ie_entrada_saida_w = 'S') then
		dankia_lancamento_material('I',:new.nr_atendimento,:new.cd_local_estoque,:new.cd_material,:new.qt_material,sysdate,null,:new.nr_prescricao,:new.nr_seq_prescricao,:new.nr_seq_lote,:new.cd_motivo,ds_erro_w,:new.cd_barras,:new.nm_usuario);
	elsif (ie_entrada_saida_w = 'E') then
		dankia_lancamento_material('E',:new.nr_atendimento,:new.cd_local_estoque,:new.cd_material,:new.qt_material,sysdate,null,:new.nr_prescricao,:new.nr_seq_prescricao,:new.nr_seq_lote,:new.cd_motivo,ds_erro_w,:new.cd_barras,:new.nm_usuario);
	end if;

	gerar_int_dankia_pck.dankia_atualiza_saldo_inv(:new.cd_material,:new.cd_barras,:new.qt_material,:new.cd_local_estoque,:new.cd_motivo,:new.nm_usuario, nr_seq_lote_w, :new.cd_estabelecimento);
end if;

if	(ds_erro_w is not null) then
	:new.ie_processado := 'E';
	:new.ds_processado_observacao := ds_erro_w;
end if;


:new.dt_lido := sysdate;

exception
when others then
	:new.ie_processado := 'E';
	:new.ds_processado_observacao := SUBSTR(SQLERRM(SQLCODE),1,400);
end;

end;
/


ALTER TABLE TASY.DANKIA_DISP_TRANSACAO ADD (
  CONSTRAINT DANKDISTRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DANKIA_DISP_TRANSACAO ADD (
  CONSTRAINT DANKDISTRA_APLOTE_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.AP_LOTE (NR_SEQUENCIA),
  CONSTRAINT DANKDISTRA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT DANKDISTRA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT DANKDISTRA_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT DANKDISTRA_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT DANKDISTRA_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT DANKDISTRA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.DANKIA_DISP_TRANSACAO TO NIVEL_1;

