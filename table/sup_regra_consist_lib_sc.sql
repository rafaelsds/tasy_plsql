ALTER TABLE TASY.SUP_REGRA_CONSIST_LIB_SC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_REGRA_CONSIST_LIB_SC CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_REGRA_CONSIST_LIB_SC
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  CD_ESTABELECIMENTO           NUMBER(4)        NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  CD_GRUPO_MATERIAL            NUMBER(3),
  CD_SUBGRUPO_MATERIAL         NUMBER(3),
  CD_CLASSE_MATERIAL           NUMBER(5),
  CD_MATERIAL                  NUMBER(10),
  IE_EXIGE_MARCA               VARCHAR2(1 BYTE) NOT NULL,
  IE_EXIGE_FABRICANTE          VARCHAR2(1 BYTE) NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_EXIGE_DETALHE_TEC         VARCHAR2(1 BYTE) NOT NULL,
  IE_EXIGE_ANVISA_EST          VARCHAR2(1 BYTE) NOT NULL,
  IE_EXIGE_ANVISA_EST_CONTROL  VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_MATERIAL             VARCHAR2(3 BYTE),
  IE_EXIGE_AVAL_DINAMICA       VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_COMPRA               VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SURCLSC_CLAMATE_FK_I ON TASY.SUP_REGRA_CONSIST_LIB_SC
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SURCLSC_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SURCLSC_ESTABEL_FK_I ON TASY.SUP_REGRA_CONSIST_LIB_SC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SURCLSC_GRUMATE_FK_I ON TASY.SUP_REGRA_CONSIST_LIB_SC
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SURCLSC_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SURCLSC_MATERIA_FK_I ON TASY.SUP_REGRA_CONSIST_LIB_SC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SURCLSC_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SURCLSC_PK ON TASY.SUP_REGRA_CONSIST_LIB_SC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SURCLSC_PK
  MONITORING USAGE;


CREATE INDEX TASY.SURCLSC_SUBMATE_FK_I ON TASY.SUP_REGRA_CONSIST_LIB_SC
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SURCLSC_SUBMATE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.SUP_REGRA_CONSIST_LIB_SC_tp  after update ON TASY.SUP_REGRA_CONSIST_LIB_SC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_TIPO_MATERIAL,1,4000),substr(:new.IE_TIPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_MATERIAL',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_ANVISA_EST_CONTROL,1,4000),substr(:new.IE_EXIGE_ANVISA_EST_CONTROL,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_ANVISA_EST_CONTROL',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_AVAL_DINAMICA,1,4000),substr(:new.IE_EXIGE_AVAL_DINAMICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_AVAL_DINAMICA',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MATERIAL,1,4000),substr(:new.CD_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_ANVISA_EST,1,4000),substr(:new.IE_EXIGE_ANVISA_EST,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_ANVISA_EST',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SUBGRUPO_MATERIAL,1,4000),substr(:new.CD_SUBGRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_SUBGRUPO_MATERIAL',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CLASSE_MATERIAL,1,4000),substr(:new.CD_CLASSE_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CLASSE_MATERIAL',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_MARCA,1,4000),substr(:new.IE_EXIGE_MARCA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_MARCA',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_FABRICANTE,1,4000),substr(:new.IE_EXIGE_FABRICANTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_FABRICANTE',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_DETALHE_TEC,1,4000),substr(:new.IE_EXIGE_DETALHE_TEC,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_DETALHE_TEC',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_MATERIAL,1,4000),substr(:new.CD_GRUPO_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_MATERIAL',ie_log_w,ds_w,'SUP_REGRA_CONSIST_LIB_SC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_REGRA_CONSIST_LIB_SC ADD (
  CONSTRAINT SURCLSC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_REGRA_CONSIST_LIB_SC ADD (
  CONSTRAINT SURCLSC_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT SURCLSC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SURCLSC_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT SURCLSC_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT SURCLSC_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL));

GRANT SELECT ON TASY.SUP_REGRA_CONSIST_LIB_SC TO NIVEL_1;

