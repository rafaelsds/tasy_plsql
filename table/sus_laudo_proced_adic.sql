ALTER TABLE TASY.SUS_LAUDO_PROCED_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUS_LAUDO_PROCED_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUS_LAUDO_PROCED_ADIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_LAUDO         NUMBER(10)               NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10)               NOT NULL,
  QT_PROCEDIMENTO      NUMBER(5),
  IE_VIA_TRATAMENTO    VARCHAR2(5 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA     VARCHAR2(2000 BYTE),
  NR_SEQ_PROC_INTERNO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSLPRA_PK ON TASY.SUS_LAUDO_PROCED_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLPRA_PROCEDI_FK_I ON TASY.SUS_LAUDO_PROCED_ADIC
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUSLPRA_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SUSLPRA_PROINTE_FK_I ON TASY.SUS_LAUDO_PROCED_ADIC
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSLPRA_SUSLAPA_FK_I ON TASY.SUS_LAUDO_PROCED_ADIC
(NR_SEQ_LAUDO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUS_LAUDO_PROCED_ADIC_BEFINS
BEFORE INSERT ON TASY.SUS_LAUDO_PROCED_ADIC FOR EACH ROW
DECLARE

cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
dt_entrada_unidade_w		date;
cd_setor_atendimento_w		number(5);
nr_atendimento_w			number(10);
nr_seq_atepacu_w			number(10);
nr_seq_propaci_w			number(10);
cd_convenio_w			number(5);
qt_regra_laudo_w			number(5);
cd_cgc_prestador_w		varchar2(14);
cd_categoria_w			varchar2(10);
cd_medico_requisitante_w            VARCHAR2(10);
ie_proc_laudo_w			varchar2(15) := 'N';
ds_retorno_w			varchar2(255):= '';

BEGIN

begin
cd_estabelecimento_w := nvl(wheb_usuario_pck.get_cd_estabelecimento,0);
exception
when others then
	cd_estabelecimento_w := 0;
end;

sus_consiste_regra_laudo(cd_estabelecimento_w, :new.cd_procedimento, :new.ie_origem_proced, ds_retorno_w);

if	(nvl(ds_retorno_w,'X') <> 'X') then
	ds_retorno_w := substr(substr(ds_retorno_w,1,220) || chr(13) || chr(10) || WHEB_MENSAGEM_PCK.get_texto(454285),1,255);
	Wheb_mensagem_pck.exibir_mensagem_abort(262328 , 'DS_MENSAGEM='||ds_retorno_w);
end if;

select	count(*)
into	qt_regra_laudo_w
from	sus_regra_gerar_proc_laudo;

if	(qt_regra_laudo_w > 0) then

	begin
	select	nvl(sus_obter_se_gerar_proc_laudo(a.cd_procedimento_solic,a.ie_origem_proced,a.ie_classificacao),'N')
	into	ie_proc_laudo_w
	from 	sus_laudo_paciente a
	where 	nr_seq_interno = :new.nr_seq_laudo;
	exception
	when others then
		ie_proc_laudo_w := 'N';
	end;

else
	ie_proc_laudo_w := 'N';
end if;


if (ie_proc_laudo_w = 'S') then

	select	nr_atendimento,
		coalesce(cd_medico_requisitante,cd_profis_requisitante)
	into	nr_atendimento_w,
		cd_medico_requisitante_w
	from	sus_laudo_paciente
	where	nr_seq_interno = :new.nr_seq_laudo;

	begin
	select	a.cd_convenio,
		a.cd_categoria,
		b.cd_estabelecimento
	into	cd_convenio_w,
		cd_categoria_w,
		cd_estabelecimento_w
	from	atend_categoria_convenio a,
		atendimento_paciente b
	where	a.nr_atendimento = b.nr_atendimento
	and	b.nr_atendimento = nr_atendimento_w
	and	rownum = 1;

	select	max(cd_cgc)
	into	cd_cgc_prestador_w
	from	estabelecimento
	where	cd_estabelecimento	= cd_estabelecimento_w;

	exception
	when others then
		cd_convenio_w		:= null;
		cd_categoria_w		:= null;
		cd_estabelecimento_w	:= null;
		cd_cgc_prestador_w	:= null;
	end;

	begin
	select	a.cd_setor_atendimento,
		a.dt_entrada_unidade,
		a.nr_seq_interno
	into	cd_setor_atendimento_w,
		dt_entrada_unidade_w,
		nr_seq_atepacu_w
	from	atend_paciente_unidade a
	where	a.nr_atendimento	= nr_atendimento_w
	and	a.dt_entrada_unidade 	= (	select max(x.dt_entrada_unidade)
						from atend_paciente_unidade x
						where x.nr_atendimento = a.nr_atendimento);
	exception
	when others then
		cd_setor_atendimento_w	:= null;
		dt_entrada_unidade_w	:= null;
		nr_seq_atepacu_w		:= null;
	end;

	if	(nr_seq_atepacu_w is not null)  then

		select	procedimento_paciente_seq.nextval
		into	nr_seq_propaci_w
		from	dual;

		begin
			insert into procedimento_paciente(
				nr_sequencia, nr_atendimento, dt_entrada_unidade, cd_procedimento,
				dt_procedimento, qt_procedimento, dt_atualizacao, nm_usuario,
				cd_medico, cd_convenio, cd_categoria, cd_pessoa_fisica,
				dt_prescricao, ds_observacao, vl_procedimento, 	vl_medico,
				vl_anestesista, vl_materiais, cd_edicao_amb, cd_tabela_servico,
				dt_vigencia_preco, cd_procedimento_princ, dt_procedimento_princ, dt_acerto_conta,
				dt_acerto_convenio, dt_acerto_medico, vl_auxiliares, vl_custo_operacional,
				tx_medico, tx_anestesia, nr_prescricao, nr_sequencia_prescricao,
				cd_motivo_exc_conta, ds_compl_motivo_excon, cd_acao, qt_devolvida,
				cd_motivo_devolucao, nr_cirurgia, nr_doc_convenio, cd_medico_executor,
				ie_cobra_pf_pj, nr_laudo, dt_conta, cd_setor_atendimento,
				cd_conta_contabil, cd_procedimento_aih, ie_origem_proced, nr_aih,
				ie_responsavel_credito, tx_procedimento, cd_equipamento, ie_valor_informado,
				cd_estabelecimento_custo, cd_tabela_custo, cd_situacao_glosa, nr_lote_contabil,
				cd_procedimento_convenio, nr_seq_autorizacao, 	ie_tipo_servico_sus, ie_tipo_ato_sus,
				cd_cgc_prestador, nr_nf_prestador, cd_atividade_prof_bpa, nr_interno_conta,
				nr_seq_proc_princ, ie_guia_informada, dt_inicio_procedimento, ie_emite_conta,
				ie_funcao_medico, ie_classif_sus, 	cd_especialidade, nm_usuario_original,
				nr_seq_proc_pacote, ie_tipo_proc_sus, cd_setor_receita, vl_adic_plant,
				nr_seq_atepacu, ie_auditoria)
			values	(nr_seq_propaci_w, nr_atendimento_w,dt_entrada_unidade_w,:new.cd_procedimento,
				dt_entrada_unidade_w, nvl(:new.qt_procedimento,1), sysdate, :new.nm_usuario,
				null, cd_convenio_w, cd_categoria_w, null,
				null,'Regra geracao proc laudo', 0, 0,
				0, 0,null,null,
				null, null, null, null,
				null, null, 0, 0,
				1, 1, null, null,
				null, null, null, null,
				null, null, null, cd_medico_requisitante_w,
				null, null, null, cd_setor_atendimento_w,
				null, null, :new.ie_origem_proced, null,
				null, null, null, 'N',
				cd_estabelecimento_w, null, null, null,
				null, null, null, null,
				cd_cgc_prestador_w, null, null, null,
				null, null, null, null,
				null, null, null, null,
				null, null, cd_setor_atendimento_w, 0,
				nr_seq_atepacu_w,null);

			atualiza_preco_procedimento(nr_seq_propaci_w,cd_convenio_w,:new.nm_usuario);

		exception
		when others then
			wheb_mensagem_pck.exibir_mensagem_abort(188082,'SQL_ERRO='||substr(sqlerrm,1,255));
			/* Ocorreu o seguinte problema ao inserir registro na procedimento paciente:#@SQL_ERRO#@ */
		end;
	end if;
end if;

END;
/


ALTER TABLE TASY.SUS_LAUDO_PROCED_ADIC ADD (
  CONSTRAINT SUSLPRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUS_LAUDO_PROCED_ADIC ADD (
  CONSTRAINT SUSLPRA_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT SUSLPRA_SUSLAPA_FK 
 FOREIGN KEY (NR_SEQ_LAUDO) 
 REFERENCES TASY.SUS_LAUDO_PACIENTE (NR_SEQ_INTERNO)
    ON DELETE CASCADE,
  CONSTRAINT SUSLPRA_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.SUS_LAUDO_PROCED_ADIC TO NIVEL_1;

