ALTER TABLE TASY.HD_DIALISE_EVENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_DIALISE_EVENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_DIALISE_EVENTO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_DIALISE             NUMBER(10),
  NR_SEQ_TIPO_EVENTO         NUMBER(10)         NOT NULL,
  DT_EVENTO                  DATE               NOT NULL,
  NR_SEQ_UNID_DIALISE        NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  NR_SEQ_DIALISE_PERITONIAL  NUMBER(10),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDDIAEV_HDDIALE_FK_I ON TASY.HD_DIALISE_EVENTO
(NR_SEQ_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HDDIAEV_HDDIPER_FK_I ON TASY.HD_DIALISE_EVENTO
(NR_SEQ_DIALISE_PERITONIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIAEV_HDDIPER_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIAEV_HDTIPEV_FK_I ON TASY.HD_DIALISE_EVENTO
(NR_SEQ_TIPO_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIAEV_HDTIPEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDDIAEV_HDUNIDD_FK_I ON TASY.HD_DIALISE_EVENTO
(NR_SEQ_UNID_DIALISE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIAEV_HDUNIDD_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDDIAEV_PK ON TASY.HD_DIALISE_EVENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDDIAEV_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hd_dialise_evento_after_insert
after insert or update ON TASY.HD_DIALISE_EVENTO for each row
declare

cd_perfil_w		number(5);
ds_tipo_evento_w	varchar2(80);
ds_mensagem_w		varchar2(2000);
nr_atendimento_w	number(10);
nm_paciente_w		varchar2(255);
ds_texto_evento_w	varchar2(4000);
ds_perfil_adicional_w	varchar2(4000);
nm_usuarios_dest_w	varchar2(4000);
nm_usuario_w		varchar2(15);
ds_status_evento_w	varchar2(20);
ds_unid_dialise_w	varchar2(80);
cd_estab_exclusivo_w	number(4);
ie_estab_registro_w	varchar2(1);
cd_estab_CI_w		number(4);
Cursor C01 is
	select	cd_perfil
	from	hd_tipo_evento
	where	nr_sequencia = :new.nr_seq_tipo_evento
	union
	select	cd_perfil
	from	hd_tipo_evento_perfil
	where	nr_seq_tipo = :new.nr_seq_tipo_evento
	order by 1;

Cursor C02 is
	select	nm_usuario_evento
	from	hd_tipo_evento_usuario
	where	nr_seq_tipo = :new.nr_seq_tipo_evento
	order by 1;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then

	select	nvl(max(cd_perfil),0),
		max(ds_tipo_evento),
		max(ds_texto_padrao), --andrey
		max(cd_estab_exclusivo),
		nvl(max(ie_estab_registro),'N')
	into	cd_perfil_w,
		ds_tipo_evento_w,
		ds_texto_evento_w,
		cd_estab_exclusivo_w,
		ie_estab_registro_w
	from	hd_tipo_evento
	where	nr_sequencia = :new.nr_seq_tipo_evento;

	if	(cd_estab_exclusivo_w is not null) then
		cd_estab_CI_w := cd_estab_exclusivo_w;
	elsif	(ie_estab_registro_w = 'S') then
		cd_estab_CI_w := wheb_usuario_pck.get_cd_estabelecimento;
	else
		cd_estab_CI_w := null;
	end if;

	select 	max(nr_atendimento),
		max(substr(obter_nome_pf(cd_pessoa_fisica),1,200)) nm_paciente,
		max(substr(hd_obter_desc_unid_dialise(nr_seq_unid_dialise),1,80))
	into	nr_atendimento_w,
		nm_paciente_w,
		ds_unid_dialise_w
	from   	hd_dialise
	where  	nr_sequencia = :new.nr_seq_dialise;

	if (inserting) then
		ds_status_evento_w	:= wheb_mensagem_pck.get_texto(308028); -- Inclusao
	else
		ds_status_evento_w	:= wheb_mensagem_pck.get_texto(308030); -- Alteracao
	end if;

	if (ds_texto_evento_w is not null) then
		ds_mensagem_w	:= ds_texto_evento_w;

		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@status_evento', ds_status_evento_w),1,2000);
		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@paciente', nm_paciente_w),1,2000);
		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@atendimento', nr_atendimento_w),1,2000);
		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@data_evento', PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_evento, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone)),1,2000);
		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@observacao', :new.ds_observacao),1,2000);
		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@tipo_evento', ds_tipo_evento_w),1,2000);
		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@usuario', Obter_Nome_Usuario(:new.nm_usuario)),1,2000);
		ds_mensagem_w	:= substr(replace_macro(ds_mensagem_w,'@unidadedialise', ds_unid_dialise_w),1,2000);

	else
		if	(inserting) then
			ds_mensagem_w:= wheb_mensagem_pck.get_texto(308031, 'DS_TIPO_EVENTO_W=' || ds_tipo_evento_w || ';' ||
																'NM_USUARIO=' || Obter_Nome_Usuario(:new.nm_usuario));
							--  Inclusao do evento #@DS_TIPO_EVENTO_W#@ pelo usuario #@NM_USUARIO#@.
		else
			Ds_mensagem_w:= wheb_mensagem_pck.get_texto(308032, 'DS_TIPO_EVENTO_W=' || ds_tipo_evento_w || ';' ||
																'NM_USUARIO=' || Obter_Nome_Usuario(:new.nm_usuario));
							--  Alteracao do evento #@DS_TIPO_EVENTO_W#@ pelo usuario #@NM_USUARIO#@.
		end if;
		ds_mensagem_w:= ds_mensagem_w||wheb_rtf_pck.get_quebra_linha|| wheb_mensagem_pck.get_texto(308033) || ': '||nm_paciente_w; -- Paciente
		ds_mensagem_w:= ds_mensagem_w||wheb_rtf_pck.get_quebra_linha|| wheb_mensagem_pck.get_texto(308034) || ': '||nr_atendimento_w; -- Atendimento
		ds_mensagem_w:= ds_mensagem_w||wheb_rtf_pck.get_quebra_linha|| wheb_mensagem_pck.get_texto(308035) || ': '||PKG_DATE_FORMATERS_TZ.TO_VARCHAR(:new.dt_evento, 'timestamp', ESTABLISHMENT_TIMEZONE_UTILS.getTimeZone); -- Data do evento
		ds_mensagem_w:= ds_mensagem_w||wheb_rtf_pck.get_quebra_linha|| wheb_mensagem_pck.get_texto(308036) || ': '||:new.ds_observacao; -- Observacao
	end if;

	open C01;
	loop
	fetch C01 into
		cd_perfil_w;
	exit when C01%notfound;
		if (ds_perfil_adicional_w is null) or (instr(ds_perfil_adicional_w,cd_perfil_w) = 0) then
			ds_perfil_adicional_w := substr(cd_perfil_w  || ',' || ds_perfil_adicional_w,1,4000);
		end if;
	end loop;
	close C01;

	open C02;
	loop
	fetch C02 into
		nm_usuario_w;
	exit when C02%notfound;
		if (nm_usuarios_dest_w is null) or (instr(nm_usuarios_dest_w,nm_usuario_w) = 0) then
			nm_usuarios_dest_w	:= substr(nm_usuario_w  || ',' || nm_usuarios_dest_w,1,4000);
		end if;
	end loop;
	close C02;

	if	(cd_perfil_w > 0) then
		insert into comunic_interna
				(dt_comunicado,
				ds_titulo,
				ds_comunicado,
				nm_usuario,
				dt_atualizacao,
				ie_geral,
				nm_usuario_destino,
				cd_perfil,
				nr_sequencia,
				ie_gerencial,
				nr_seq_classif,
				ds_perfil_adicional,
				cd_setor_destino,
				cd_estab_destino,
				ds_setor_adicional,
				dt_liberacao,
				ds_grupo,
				nm_usuario_oculto)
		values	(sysdate,
				wheb_mensagem_pck.get_texto(308037, 'DS_TIPO_EVENTO_W=' || ds_tipo_evento_w), -- Registro do evento #@DS_TIPO_EVENTO_W#@
				wheb_rtf_pck.get_texto_rtf(ds_mensagem_w),
				:new.nm_usuario,
				sysdate,
				'N',
				nm_usuarios_dest_w,
				--null,
				ds_perfil_adicional_w,
				comunic_interna_seq.nextval,
				'N',
				null,
				ds_perfil_adicional_w,
				null,
				cd_estab_CI_w,
				null,
				sysdate,
				null,
				null);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hd_dialise_evento_befup_sign
before update ON TASY.HD_DIALISE_EVENTO for each row
declare
nr_seq_assinatura_w	hd_assinatura_digital.nr_sequencia%type;

begin

select 	max(a.nr_sequencia)
into	nr_seq_assinatura_w
from 	hd_assinatura_digital a,
	hd_assinatura_item b
where 	a.nr_sequencia = b.nr_seq_assinatura
and	b.nr_seq_evento = :new.nr_sequencia
and 	a.ie_opcao = 'NE'
and	a.dt_liberacao is not null;

if 	(nr_seq_assinatura_w is not null)
	and	(:old.nr_sequencia			<> :new.nr_sequencia
		or :old.dt_atualizacao			<> :new.dt_atualizacao
		or :old.nm_usuario			<> :new.nm_usuario
		or :old.dt_atualizacao_nrec		<> :new.dt_atualizacao_nrec
		or :old.nm_usuario_nrec			<> :new.nm_usuario_nrec
		or :old.nr_seq_dialise			<> :new.nr_seq_dialise
		or :old.nr_seq_tipo_evento		<> :new.nr_seq_tipo_evento
		or :old.dt_evento			<> :new.dt_evento
		or :old.nr_seq_unid_dialise		<> :new.nr_seq_unid_dialise
		or :old.ds_observacao			<> :new.ds_observacao
		or :old.nr_seq_dialise_peritonial	<> :new.nr_seq_dialise_peritonial
		or (:old.nr_sequencia			is null	and	:new.nr_sequencia		is not null)
		or (:old.dt_atualizacao			is null	and	:new.dt_atualizacao		is not null)
		or (:old.nm_usuario			is null	and	:new.nm_usuario			is not null)
		or (:old.dt_atualizacao_nrec		is null	and	:new.dt_atualizacao_nrec	is not null)
		or (:old.nm_usuario_nrec		is null	and	:new.nm_usuario_nrec		is not null)
		or (:old.nr_seq_dialise			is null	and	:new.nr_seq_dialise		is not null)
		or (:old.nr_seq_tipo_evento		is null	and	:new.nr_seq_tipo_evento		is not null)
		or (:old.dt_evento			is null	and	:new.dt_evento			is not null)
		or (:old.nr_seq_unid_dialise		is null	and	:new.nr_seq_unid_dialise	is not null)
		or (:old.ds_observacao			is null	and	:new.ds_observacao		is not null)
		or (:old.nr_seq_dialise_peritonial	is null	and	:new.nr_seq_dialise_peritonial	is not null)
		or (:old.nr_sequencia			is not null	and	:new.nr_sequencia		is null)
		or (:old.dt_atualizacao			is not null	and	:new.dt_atualizacao		is null)
		or (:old.nm_usuario			is not null	and	:new.nm_usuario			is null)
		or (:old.dt_atualizacao_nrec		is not null	and	:new.dt_atualizacao_nrec	is null)
		or (:old.nm_usuario_nrec		is not null	and	:new.nm_usuario_nrec		is null)
		or (:old.nr_seq_dialise			is not null	and	:new.nr_seq_dialise		is null)
		or (:old.nr_seq_tipo_evento		is not null	and	:new.nr_seq_tipo_evento		is null)
		or (:old.dt_evento			is not null	and	:new.dt_evento			is null)
		or (:old.nr_seq_unid_dialise		is not null	and	:new.nr_seq_unid_dialise	is null)
		or (:old.ds_observacao			is not null	and	:new.ds_observacao		is null)
		or (:old.nr_seq_dialise_peritonial	is not null	and	:new.nr_seq_dialise_peritonial	is null)) then
	wheb_mensagem_pck.exibir_mensagem_abort(364485);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Evento_Paciente_Insert
AFTER Insert ON TASY.HD_DIALISE_EVENTO FOR EACH ROW
DECLARE

nr_seq_evento_pac_w		number(10);
nr_atendimento_w			number(10);
cd_setor_atendimento_w		number(10);
cd_estabelecimento_w		number(10);
nr_sequencia_w			number(10);
nr_seq_tipo_nao_conformidade_w  	number(10);

begin

if	(:new.nr_seq_dialise_peritonial is null) then
	nr_seq_evento_pac_w := 0;

	select	nvl(nr_seq_evento_pac,0),
		nvl(nr_seq_tipo_nao_conformidade,0)
	into	nr_seq_evento_pac_w,
		nr_seq_tipo_nao_conformidade_w
	from	HD_TIPO_EVENTO
	where	nr_sequencia = :new.nr_seq_tipo_evento;

	if	(nr_seq_evento_pac_w > 0) then
		select	nvl(nr_atendimento,0),
			nvl(obter_setor_atendimento(nr_atendimento),0)
		into	nr_atendimento_w,
			cd_setor_atendimento_w
		from	hd_dialise
		where	nr_sequencia = :new.nr_seq_dialise;

		select 	nvl(cd_estabelecimento,0)
		into	cd_estabelecimento_w
		from   	hd_unidade_dialise
		where  	nr_sequencia = :new.nr_seq_unid_dialise;

		select	qua_evento_paciente_seq.nextval
		into	nr_sequencia_w
		from	dual;


		insert into qua_evento_paciente
			(
			nr_sequencia,
			cd_estabelecimento,
			dt_atualizacao,
			nm_usuario,
			nr_atendimento,
			nr_seq_evento,
			dt_evento,
			ds_evento,
			cd_setor_atendimento,
			dt_cadastro,
			nm_usuario_origem,
			dt_atualizacao_nrec,
			nm_usuario_nrec,
			nm_usuario_reg,
			nr_seq_classif_evento,
			dt_liberacao,
			cd_pessoa_fisica,
			ie_situacao,
			ie_status,
			ie_origem,
			nr_seq_tipo_evento,
			ie_tipo_evento,
			cd_funcao_ativa
			)
		values
			(
			nr_sequencia_w,
			cd_estabelecimento_w,
			sysdate,
			:new.nm_usuario,
			nr_atendimento_w,
			nr_seq_evento_pac_w,
			sysdate,
			substr(wheb_mensagem_pck.get_texto(305932),1,100),
			cd_setor_atendimento_w,
			sysdate,
			:new.nm_usuario,
			sysdate,
			:new.nm_usuario,
			:new.nm_usuario,
			null,
			sysdate,
			obter_pessoa_atendimento(nr_atendimento_w,'C'),
			'A',
			'1',
			'S',
			:new.nr_seq_tipo_evento,
			'E',
			obter_funcao_ativa
			);

		if	(nr_seq_tipo_nao_conformidade_w > 0) then
			qua_gerar_nao_conformidade(	nr_sequencia_w,
							nr_seq_tipo_nao_conformidade_w,
							cd_estabelecimento_w,
							obter_pessoa_atendimento(nr_atendimento_w,'C'),
							sysdate,
							'S',
							:new.nm_usuario,
							'N');

		end if;
	end if;
end if;

end;
/


ALTER TABLE TASY.HD_DIALISE_EVENTO ADD (
  CONSTRAINT HDDIAEV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_DIALISE_EVENTO ADD (
  CONSTRAINT HDDIAEV_HDDIPER_FK 
 FOREIGN KEY (NR_SEQ_DIALISE_PERITONIAL) 
 REFERENCES TASY.HD_DIALISE_PERITONIAL (NR_SEQUENCIA),
  CONSTRAINT HDDIAEV_HDDIALE_FK 
 FOREIGN KEY (NR_SEQ_DIALISE) 
 REFERENCES TASY.HD_DIALISE (NR_SEQUENCIA),
  CONSTRAINT HDDIAEV_HDTIPEV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EVENTO) 
 REFERENCES TASY.HD_TIPO_EVENTO (NR_SEQUENCIA),
  CONSTRAINT HDDIAEV_HDUNIDD_FK 
 FOREIGN KEY (NR_SEQ_UNID_DIALISE) 
 REFERENCES TASY.HD_UNIDADE_DIALISE (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_DIALISE_EVENTO TO NIVEL_1;

