ALTER TABLE TASY.DUV_MENSAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DUV_MENSAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.DUV_MENSAGEM
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_TIPO_MENSAGEM  NUMBER(10)              NOT NULL,
  NR_SEQ_ARQUIVO        NUMBER(10)              NOT NULL,
  NR_SEQ_EPISODIO       NUMBER(10)              NOT NULL,
  DT_ENVIO              DATE,
  DT_LIBERACAO          DATE,
  DS_OBSERVACAO         VARCHAR2(400 BYTE),
  NR_SEQ_NODO           NUMBER(10),
  NR_SEQ_VERSAO         NUMBER(10),
  DT_RETORNO            DATE,
  DT_REENVIO            DATE,
  DT_FECHAMENTO         DATE,
  IE_STATUS             VARCHAR2(2 BYTE)        NOT NULL,
  DT_MENSAGEM           DATE,
  NR_SEQ_RETORNO        NUMBER(10),
  DT_CONSISTENCIA       DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.DUVMENS_CAEPPA_FK_I ON TASY.DUV_MENSAGEM
(NR_SEQ_EPISODIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DUVMENS_DUVARQU_FK_I ON TASY.DUV_MENSAGEM
(NR_SEQ_ARQUIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DUVMENS_DUVRETO_FK_I ON TASY.DUV_MENSAGEM
(NR_SEQ_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DUVMENS_DUVTPME_FK_I ON TASY.DUV_MENSAGEM
(NR_SEQ_TIPO_MENSAGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.DUVMENS_DUVVERS_FK_I ON TASY.DUV_MENSAGEM
(NR_SEQ_VERSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.DUVMENS_PK ON TASY.DUV_MENSAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DUV_MENSAGEM ADD (
  CONSTRAINT DUVMENS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.DUV_MENSAGEM ADD (
  CONSTRAINT DUVMENS_CAEPPA_FK 
 FOREIGN KEY (NR_SEQ_EPISODIO) 
 REFERENCES TASY.EPISODIO_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT DUVMENS_DUVARQU_FK 
 FOREIGN KEY (NR_SEQ_ARQUIVO) 
 REFERENCES TASY.DUV_ARQUIVO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT DUVMENS_DUVRETO_FK 
 FOREIGN KEY (NR_SEQ_RETORNO) 
 REFERENCES TASY.DUV_RETORNO (NR_SEQUENCIA),
  CONSTRAINT DUVMENS_DUVTPME_FK 
 FOREIGN KEY (NR_SEQ_TIPO_MENSAGEM) 
 REFERENCES TASY.DUV_TIPO_MENSAGEM (NR_SEQUENCIA),
  CONSTRAINT DUVMENS_DUVVERS_FK 
 FOREIGN KEY (NR_SEQ_VERSAO) 
 REFERENCES TASY.DUV_VERSAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.DUV_MENSAGEM TO NIVEL_1;

