ALTER TABLE TASY.PLS_CAT_HISTORICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CAT_HISTORICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CAT_HISTORICO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_TIPO_HISTORICO    VARCHAR2(2 BYTE)         NOT NULL,
  DT_HISTORICO         DATE                     NOT NULL,
  NR_SEQ_CAT           NUMBER(10)               NOT NULL,
  DS_HISTORICO         VARCHAR2(4000 BYTE)      NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCAHI_PK ON TASY.PLS_CAT_HISTORICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAHI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSCAHI_PLSCATS_FK_I ON TASY.PLS_CAT_HISTORICO
(NR_SEQ_CAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCAHI_PLSCATS_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_CAT_HISTORICO ADD (
  CONSTRAINT PLSCAHI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CAT_HISTORICO ADD (
  CONSTRAINT PLSCAHI_PLSCATS_FK 
 FOREIGN KEY (NR_SEQ_CAT) 
 REFERENCES TASY.PLS_CAT (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CAT_HISTORICO TO NIVEL_1;

