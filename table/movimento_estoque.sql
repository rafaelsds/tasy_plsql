ALTER TABLE TASY.MOVIMENTO_ESTOQUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MOVIMENTO_ESTOQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MOVIMENTO_ESTOQUE
(
  NR_MOVIMENTO_ESTOQUE          NUMBER(10)      NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  CD_LOCAL_ESTOQUE              NUMBER(4)       NOT NULL,
  DT_MOVIMENTO_ESTOQUE          DATE            NOT NULL,
  CD_OPERACAO_ESTOQUE           NUMBER(3)       NOT NULL,
  CD_ACAO                       VARCHAR2(1 BYTE) NOT NULL,
  CD_MATERIAL                   NUMBER(6)       NOT NULL,
  DT_MESANO_REFERENCIA          DATE            NOT NULL,
  QT_MOVIMENTO                  NUMBER(13,4)    NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  IE_ORIGEM_DOCUMENTO           VARCHAR2(3 BYTE) NOT NULL,
  NR_DOCUMENTO                  NUMBER(15),
  NR_SEQUENCIA_ITEM_DOCTO       NUMBER(6),
  CD_CGC_EMITENTE               VARCHAR2(14 BYTE),
  CD_SERIE_NF                   VARCHAR2(255 BYTE),
  NR_SEQUENCIA_DOCUMENTO        NUMBER(10),
  VL_MOVIMENTO                  NUMBER(13,2),
  CD_UNIDADE_MEDIDA_ESTOQUE     VARCHAR2(30 BYTE),
  CD_PROCEDIMENTO               NUMBER(15),
  CD_SETOR_ATENDIMENTO          NUMBER(5),
  CD_CONTA                      VARCHAR2(20 BYTE),
  DT_CONTABIL                   DATE,
  CD_LOTE_FABRICACAO            VARCHAR2(20 BYTE),
  DT_VALIDADE                   DATE,
  QT_ESTOQUE                    NUMBER(15,4)    NOT NULL,
  DT_PROCESSO                   DATE,
  CD_LOCAL_ESTOQUE_DESTINO      NUMBER(4),
  CD_CENTRO_CUSTO               NUMBER(8),
  CD_UNIDADE_MED_MOV            VARCHAR2(30 BYTE),
  NR_MOVIMENTO_ESTOQUE_CORRESP  NUMBER(10),
  CD_CONTA_CONTABIL             VARCHAR2(20 BYTE),
  CD_MATERIAL_ESTOQUE           NUMBER(6)       NOT NULL,
  IE_ORIGEM_PROCED              NUMBER(10),
  CD_FORNECEDOR                 VARCHAR2(14 BYTE),
  NR_LOTE_CONTABIL              NUMBER(10),
  QT_INVENTARIO                 NUMBER(15,4),
  DS_OBSERVACAO                 VARCHAR2(255 BYTE),
  NR_SEQ_TAB_ORIG               NUMBER(15),
  NR_SEQ_LOTE_FORNEC            NUMBER(10),
  CD_FUNCAO                     NUMBER(5),
  CD_PERFIL                     NUMBER(5),
  NR_ATENDIMENTO                NUMBER(10),
  NR_PRESCRICAO                 NUMBER(14),
  NR_RECEITA                    NUMBER(15),
  IE_MOVTO_CONSIGNADO           VARCHAR2(1 BYTE),
  NR_ORDEM_COMPRA               NUMBER(10),
  NR_ITEM_OCI                   NUMBER(10),
  DS_CONSISTENCIA               VARCHAR2(255 BYTE),
  NR_LOTE_AP                    NUMBER(10),
  NR_LOTE_PRODUCAO              NUMBER(10),
  DS_MAQUINA                    VARCHAR2(80 BYTE),
  NR_SEQ_MOTIVO_PERDA_QUIMIO    NUMBER(10),
  DS_STACK                      VARCHAR2(2000 BYTE),
  NR_DOC_EXTERNO                VARCHAR2(80 BYTE)
)
TABLESPACE TASY_DATA_G
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2816M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MOVESTO_ATEPACI_FK_I ON TASY.MOVIMENTO_ESTOQUE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          269M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_CENCUST_FK_I ON TASY.MOVIMENTO_ESTOQUE
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          226M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVESTO_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVESTO_CONCONT_FK_I ON TASY.MOVIMENTO_ESTOQUE
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          176M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I09 ON TASY.MOVIMENTO_ESTOQUE
(NR_DOCUMENTO, NR_SEQUENCIA_ITEM_DOCTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          487M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I1 ON TASY.MOVIMENTO_ESTOQUE
(DT_MOVIMENTO_ESTOQUE, CD_MATERIAL_ESTOQUE, NR_DOCUMENTO, NR_MOVIMENTO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          861M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I10 ON TASY.MOVIMENTO_ESTOQUE
(NR_SEQ_TAB_ORIG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          382M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I11 ON TASY.MOVIMENTO_ESTOQUE
(CD_ESTABELECIMENTO, CD_MATERIAL_ESTOQUE, DT_MOVIMENTO_ESTOQUE, CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          624M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I12 ON TASY.MOVIMENTO_ESTOQUE
(CD_ESTABELECIMENTO, CD_MATERIAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVESTO_I12
  MONITORING USAGE;


CREATE INDEX TASY.MOVESTO_I13 ON TASY.MOVIMENTO_ESTOQUE
(CD_ESTABELECIMENTO, CD_MATERIAL_ESTOQUE, DT_MESANO_REFERENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I14 ON TASY.MOVIMENTO_ESTOQUE
(DT_MOVIMENTO_ESTOQUE, CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I15 ON TASY.MOVIMENTO_ESTOQUE
(CD_ESTABELECIMENTO, DT_MESANO_REFERENCIA, CD_MATERIAL_ESTOQUE, DT_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I16 ON TASY.MOVIMENTO_ESTOQUE
(NR_SEQ_LOTE_FORNEC, NR_SEQ_TAB_ORIG, IE_ORIGEM_DOCUMENTO, NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.MOVESTO_I2 ON TASY.MOVIMENTO_ESTOQUE
(DT_MESANO_REFERENCIA, CD_OPERACAO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          495M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I3 ON TASY.MOVIMENTO_ESTOQUE
(CD_MATERIAL_ESTOQUE, DT_MESANO_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I4 ON TASY.MOVIMENTO_ESTOQUE
(CD_MATERIAL_ESTOQUE, CD_ESTABELECIMENTO, DT_MESANO_REFERENCIA, CD_OPERACAO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          513M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I5 ON TASY.MOVIMENTO_ESTOQUE
(CD_MATERIAL_ESTOQUE, DT_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          513M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVESTO_I5
  MONITORING USAGE;


CREATE INDEX TASY.MOVESTO_I6 ON TASY.MOVIMENTO_ESTOQUE
(CD_ESTABELECIMENTO, DT_MESANO_REFERENCIA, CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          517M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I7 ON TASY.MOVIMENTO_ESTOQUE
(CD_ESTABELECIMENTO, CD_LOCAL_ESTOQUE, CD_MATERIAL_ESTOQUE, DT_MESANO_REFERENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          513M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_I8 ON TASY.MOVIMENTO_ESTOQUE
(CD_ESTABELECIMENTO, CD_MATERIAL_ESTOQUE, DT_MOVIMENTO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          562M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_LOCESTO_FK_I ON TASY.MOVIMENTO_ESTOQUE
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          263M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVESTO_LOCESTO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVESTO_LOTCONT_FK_I ON TASY.MOVIMENTO_ESTOQUE
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_LOTPROD_FK_I ON TASY.MOVIMENTO_ESTOQUE
(NR_LOTE_PRODUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVESTO_LOTPROD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVESTO_MATERIA_FK_I ON TASY.MOVIMENTO_ESTOQUE
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          264M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_MATERIA_FK2_I ON TASY.MOVIMENTO_ESTOQUE
(CD_MATERIAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          270M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_MATLOFO_FK_I ON TASY.MOVIMENTO_ESTOQUE
(NR_SEQ_LOTE_FORNEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_MOPERQ_FK_I ON TASY.MOVIMENTO_ESTOQUE
(NR_SEQ_MOTIVO_PERDA_QUIMIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVESTO_MOPERQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MOVESTO_OPEESTO_FK_I ON TASY.MOVIMENTO_ESTOQUE
(CD_OPERACAO_ESTOQUE)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_PESJURI_FK_I ON TASY.MOVIMENTO_ESTOQUE
(CD_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          9M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVESTO_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MOVESTO_PK ON TASY.MOVIMENTO_ESTOQUE
(NR_MOVIMENTO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX_G
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          312M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_PRESMED_FK_I ON TASY.MOVIMENTO_ESTOQUE
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          59M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MOVESTO_9 ON TASY.MOVIMENTO_ESTOQUE
(DT_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          375M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MOVESTO_9
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.movimento_estoque_insert
before insert ON TASY.MOVIMENTO_ESTOQUE 
/*
before insert or update on movimento_estoque (antigo)
fabio e marcus 12/03/2005
nao existe motivo para que esta trigger seja disparada no update
somente tem update no estorno e na atualizacao da conta
*/

for each row
declare
qt_existe_regra_email_w		number(10);
cd_unidade_medida_estoque_w	varchar2(30);
qt_conv_estoque_consumo_w	number(15,4);
qt_estoque_w			number(15,4);
qt_consulta_estoque_consig_w	number(15,4);
qt_consulta_estoque_w		number(15,4);
qt_estoque_consumo_w		number(15,4);
nr_movimento_estoque_w		number(10,0);
ie_erro_w				varchar2(1);
ie_erro_ww			varchar2(1);
ds_erro_w			varchar2(255);
ds_erro_ww			varchar2(255);
dt_mesano_vigente_w		date;
dt_mesano_referencia_w		date;
cd_centro_conta_w		centro_custo.cd_centro_custo%type;
ie_consignado_w			varchar2(1);
ie_atualiza_local_direto_w		varchar2(1);
ie_tipo_local_w			varchar2(5);
ie_tipo_requisicao_w		varchar2(5);
ie_material_estoque_w		varchar2(1);
ie_oper_consignado_w		varchar2(1);
ie_atualiza_consignado_w		boolean;
ds_material_w			varchar2(255);
ie_bloqueio_inventario_w		varchar2(1);
cd_funcao_ativa_w			number(5);
cd_perfil_ativo_w			number(5);
ie_estoque_lote_w			varchar2(1);
cd_estab_local_w			number(4);
ie_vigencia_conta_w		varchar2(1);
cd_operacao_cons_paciente_w	number(4);
cd_operacao_devol_paciente_w	number(4);
ie_centro_inventario_w		varchar2(1);
ie_centro_perda_w			varchar2(1);
cd_centro_local_w			number(8);
ie_oper_lib_usuario_w		varchar2(1);
ie_consiste_cadastro_estab_w	varchar2(1);
ie_existe_cadastro_estab_w		varchar2(1);
ie_movto_consig_w			varchar2(1);
ie_consumo_w			varchar2(1);
qt_existe_saldo_lote_w		number(10);
qt_estoque_lote_w			number(13,4);
ie_entrada_saida_w			varchar2(1);
qt_estoque_ww			movimento_estoque.qt_estoque%type;
nr_emprestimo_w			number(10,0);
ie_tipo_perda_w			operacao_estoque.ie_tipo_perda%type;
cd_empresa_w        	estabelecimento.cd_empresa%type;

begin
    if (nvl(wheb_usuario_pck.get_ie_executar_trigger, 'S') = 'S') then
        begin
            ie_erro_w	:= 'N';

            begin
                select a.cd_empresa
                into cd_empresa_w
                from estabelecimento a
                where a.cd_estabelecimento = :new.cd_estabelecimento;

                philips_contabil_pck.valida_se_dia_fechado(cd_empresa_w, nvl(:new.dt_processo, :new.dt_movimento_estoque));
            end;

            :new.ds_stack := substr(dbms_utility.format_call_stack,1,2000);

            begin
            select	nvl(obter_funcao_ativa,0),
                nvl(obter_perfil_ativo,0)
            into	cd_funcao_ativa_w,
                cd_perfil_ativo_w
            from	dual;
            exception
                when others then
                cd_funcao_ativa_w	:= 0;
                cd_perfil_ativo_w	:= 0;
            end;
            :new.cd_funcao	:= cd_funcao_ativa_w;
            :new.cd_perfil	:= cd_perfil_ativo_w;
            :new.ds_maquina	:= substr(wheb_usuario_pck.get_nm_estacao,1,80);

            if	(:new.cd_material_estoque is null) then
                select	nvl(cd_material_estoque,:new.cd_material)
                into	:new.cd_material_estoque
                from	material
                where	cd_material 	= :new.cd_material;
            end if;

            select	nvl(max(cd_estabelecimento), 0),
                nvl(max(ie_tipo_local), 'X'),
                nvl(max(ie_centro_inventario), 'N'),
                nvl(max(cd_centro_custo), 0),
                nvl(max(ie_centro_perda),'N')
            into	cd_estab_local_w,
                ie_tipo_local_w,
                ie_centro_inventario_w,
                cd_centro_local_w,
                ie_centro_perda_w
            from	local_estoque
            where	cd_local_estoque = :new.cd_local_estoque;

            if	(cd_estab_local_w > 0) and
                (cd_estab_local_w <> :new.cd_estabelecimento) then
                wheb_mensagem_pck.exibir_mensagem_abort(174096);
                /*r.aise_application_error(-20011,'O estabelecimento do Local de estoque e diferente do' || chr(13) || chr(10) ||
                                'estabelecimento do movimento de estoque. Avise o Administrador do Sistema');*/
            end if;

            select	nvl(max(ie_estoque_lote),'N')
            into	ie_estoque_lote_w
            from	material_estab
            where	cd_material = :new.cd_material_estoque
            and	cd_estabelecimento = :new.cd_estabelecimento;

            if	(ie_estoque_lote_w = 'S') and
                (:new.nr_seq_lote_fornec is null) then
                wheb_mensagem_pck.exibir_mensagem_abort(174103,'CD_MATERIAL_ESTOQUE='||:new.cd_material_estoque);
                /*r.aise_application_error(-20011,'Este material tem estoque por lote fornecedor.' || chr(13) ||
                                'Somente pode ser movimentado por barras do lote. (' || :new.cd_material_estoque || ')' || chr(13));*/
            end if;

            select	nvl(obter_se_oper_liberado_usuario(:new.cd_operacao_estoque, :new.cd_material, :new.cd_local_estoque, :new.nm_usuario), 'S')
            into	ie_oper_lib_usuario_w
            from	dual;

            if	(ie_oper_lib_usuario_w = 'N') then
                wheb_mensagem_pck.exibir_mensagem_abort(174109,'CD_MATERIAL='||:new.cd_material);
                /*r.aise_application_error(-20011,	'Voce nao tem permissao de efetuar movimentos do material ' || :new.cd_material || ' com  essa operacao de estoque.');*/
            end if;

            if	(:new.dt_processo is null) then
                begin

                begin
                select	substr(obter_dados_material_estab(cd_material,:new.cd_estabelecimento,'UME'),1,30) cd_unidade_medida_estoque,
                            qt_conv_estoque_consumo,
                    ie_consignado,
                    substr(obter_se_material_estoque(:new.cd_estabelecimento, 0, cd_material),1,1)
                into	cd_unidade_medida_estoque_w ,
                    qt_conv_estoque_consumo_w,
                    ie_consignado_w,
                    ie_material_estoque_w
                from	material
                where	cd_material = :new.cd_material_estoque;
                exception
                    when no_data_found then
                    wheb_mensagem_pck.exibir_mensagem_abort(174124,'CD_MATERIAL_ESTOQUE='||:new.cd_material_estoque);
                    /*r.aise_application_error(-20011,'nao encontrado material ' || :new.cd_material_estoque);*/
                end;

                select	max(dt_mesano_vigente),
                    max(ie_atualiza_local_direto),
                    nvl(max(ie_consiste_cad_mat_estab),'N')
                into	dt_mesano_vigente_w,
                    ie_atualiza_local_direto_w,
                    ie_consiste_cadastro_estab_w
                from	parametro_estoque
                where	cd_estabelecimento	= :new.cd_estabelecimento;

                if	(ie_consiste_cadastro_estab_w = 'S') then
                    begin
                    select	nvl(max('S'),'N')
                    into	ie_existe_cadastro_estab_w
                    from	material_estab
                    where	cd_material = :new.cd_material
                    and	cd_estabelecimento = :new.cd_estabelecimento;

                    if	(ie_existe_cadastro_estab_w = 'N') then
                        wheb_mensagem_pck.exibir_mensagem_abort(174130,'CD_MATERIAL='||:new.cd_material);
                        /*r.aise_application_error(-20011,'Nao encontrado cadastro do material ' || :new.cd_material || ' no estabelecimento.');*/
                    end if;
                    end;
                end if;

                select	max(nvl(ie_consignado,0)),
                    nvl(max(ie_tipo_requisicao),'0'),
                    max(ie_consumo),
                    nvl(max(ie_tipo_perda),'X')
                into	ie_oper_consignado_w,
                    ie_tipo_requisicao_w,
                    ie_consumo_w,
                    ie_tipo_perda_w
                from	operacao_estoque
                where	cd_operacao_estoque	=  :new.cd_operacao_estoque;

                if	(:new.dt_movimento_estoque	> sysdate) then
                    :new.dt_mesano_referencia 	:= pkg_date_utils.start_of(sysdate,'MONTH',0);
                elsif	(:new.dt_movimento_estoque	<= pkg_date_utils.get_datetime(pkg_date_utils.end_of(dt_mesano_vigente_w, 'MONTH', 0), dt_mesano_vigente_w)) then
                    :new.dt_mesano_referencia 	:= dt_mesano_vigente_w;
                else	:new.dt_mesano_referencia 	:= pkg_date_utils.start_of(:new.dt_movimento_estoque,'MONTH',0);
                end if;

                if	(:new.qt_movimento is null) then
                    :new.qt_movimento := 0;
                end if;

                if	(qt_conv_estoque_consumo_w is null) then
                    qt_estoque_w := :new.qt_movimento;
                    qt_conv_estoque_consumo_w := 1;
                end if;

                if	(:new.ie_origem_documento in (5,15)) then
                    qt_conv_estoque_consumo_w       := 1;
                elsif	(:new.ie_origem_documento = '1') then
                    begin
                    select	nr_emprestimo
                    into	nr_emprestimo_w
                    from	nota_fiscal_item
                    where	nr_sequencia = :new.nr_seq_tab_orig
                    and	nr_item_nf = :new.nr_sequencia_item_docto;
                    exception
                    when others then
                        nr_emprestimo_w	:=	null;
                    end;

                end if;

                if  	(:new.ie_origem_documento not in ('1','11')) then
                    qt_estoque_w 		:= obter_quantidade_convertida(:new.cd_material_estoque, :new.qt_movimento, :new.cd_unidade_med_mov, 'UME');/*sidnei - os-321779(:new.qt_movimento / qt_conv_estoque_consumo_w);*/
                    :new.qt_estoque		:= qt_estoque_w;
                end if;

                :new.cd_unidade_medida_estoque	:= cd_unidade_medida_estoque_w;
                :new.cd_material_estoque		:= :new.cd_material_estoque;



                /* atualizacao do centro de custo para inventarios, atualiza com o centro do local de estoque*/
                if	(ie_tipo_requisicao_w = '5') and
                    (:new.cd_centro_custo is null) and
                    (cd_centro_local_w > 0) and
                    (ie_centro_inventario_w = 'S') then
                    :new.cd_centro_custo	:= cd_centro_local_w;
                end if;

                if	(:new.cd_centro_custo is null) and
                    (ie_centro_perda_w = 'S') and
                    (cd_centro_local_w > 0) and
                    (ie_tipo_perda_w <> 'X') then
                    :new.cd_centro_custo	:= cd_centro_local_w;
                end if;

                /* atualizacao do centro de custo na prescricao */
                if	(:new.ie_origem_documento = 3) and
                    (:new.cd_centro_custo is null) then
                    select	max(cd_centro_custo)
                    into 	:new.cd_centro_custo
                    from 	setor_atendimento
                    where 	cd_setor_atendimento = :new.cd_setor_atendimento;
                end if;

                if	(:new.ie_origem_documento = '10') and
                    (:new.cd_centro_custo is null) and
                    (:new.cd_setor_atendimento is not null) and
                    (ie_consumo_w in ('A','D')) then
                    select	max(cd_centro_custo)
                    into 	:new.cd_centro_custo
                    from 	setor_atendimento
                    where 	cd_setor_atendimento = :new.cd_setor_atendimento;
                end if;

                if	(:new.cd_centro_custo is not null) and
                    (:new.cd_conta_contabil is null) then
                    begin
                    cd_centro_conta_w	:=	:new.cd_centro_custo;

                    define_conta_material(
                        :new.cd_estabelecimento,
                        :new.cd_material,
                        3,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                        :new.cd_local_estoque,
                        :new.cd_operacao_estoque,
                        trunc(:new.dt_movimento_estoque),
                        :new.cd_conta_contabil,
                        cd_centro_conta_w,
                        null);
                    end;
                end if;

                /*consistencia da vigencia da conta contabil*/
                if	(:new.cd_conta_contabil is not null) then
                    select	substr(obter_se_conta_vigente(:new.cd_conta_contabil, :new.dt_movimento_estoque),1,1)
                    into	ie_vigencia_conta_w
                    from	dual;
                    if	(ie_vigencia_conta_w = 'N') then
                        wheb_mensagem_pck.exibir_mensagem_abort(174133);
                        /*r.aise_application_error(-20011,'A conta contabil esta fora da data de vigencia.');*/
                    end if;
                end if;

                /* alteracao abaixao enviada apra testes no pro, itamar esta  testando*/
                if	((ie_consignado_w = '1') and (:new.cd_fornecedor is null)) then
                    begin
                    wheb_mensagem_pck.exibir_mensagem_abort(174135);
                    /*r.aise_application_error(-20011,'Este item eh consignado e exige a informacao do fornecedor.');*/
                    end;
                end if;

                /* identificar se deve atualizar a tabela de consignados */
                ie_atualiza_consignado_w	:= (ie_oper_consignado_w <> '0') and
                                ((ie_consignado_w = '1') or
                                ((ie_consignado_w = '2') and
                                (:new.cd_fornecedor is not null)));

                /* define que local de passagem direta (tipo 8) nao atualiza estoque */
                if 	(ie_tipo_local_w = '8') and
                    (ie_atualiza_local_direto_w = 'N') 	 then
                    ie_material_estoque_w	:= 'N';
                end if;

                /*caso especifico para quando:
                    - se for para baixar do consignado
                    - se lancado por lote de fornecedor
                    - se o material for tipo ambos
                    - se nao tem saldo do consignado
                    - se tem saldo do normal
                    - e se nao for um inventario
                isso para nao dar erro que nao existe saldo para o consignado quando tiver no saldo normal*/

                select	ie_entrada_saida
                into	ie_entrada_saida_w
                from	operacao_estoque
                where	cd_operacao_estoque = :new.cd_operacao_estoque;

                /* 987003 - nao deve fazer essa troca automatica
                if	(ie_tipo_requisicao_w in ('1','3','4','9')) and
                    (ie_atualiza_consignado_w) and
                    (:new.nr_seq_lote_fornec is not null) and
                    (:new.cd_fornecedor is not null) and
                    (ie_consignado_w = '2') and
                    (((:new.cd_acao = '1') and (ie_entrada_saida_w = 'S')) or ((:new.cd_acao = '2') and (ie_entrada_saida_w = 'E'))) then
                    begin
                    select	nvl(max(obter_saldo_estoque_consig(
                            :new.cd_estabelecimento,
                            :new.cd_fornecedor,
                            :new.cd_material_estoque,
                            :new.cd_local_estoque)), 0)
                    into	qt_consulta_estoque_consig_w
                    from	dual;

                    select	nvl(max(obter_saldo_disp_estoque(
                            :new.cd_estabelecimento,
                            :new.cd_material_estoque,
                            :new.cd_local_estoque,
                            :new.dt_mesano_referencia)),0)
                    into	qt_consulta_estoque_w
                    from	dual;

                    if	(qt_consulta_estoque_consig_w < :new.qt_estoque) and
                        (qt_consulta_estoque_w >= :new.qt_estoque) then
                        begin
                        begin
                        select	cd_operacao_cons_paciente,
                            cd_operacao_devol_paciente
                        into	cd_operacao_cons_paciente_w,
                            cd_operacao_devol_paciente_w
                        from	parametro_estoque
                        where	cd_estabelecimento = :new.cd_estabelecimento;
                        exception
                            when others then
                            wheb_mensagem_pck.exibir_mensagem_abort(174136);
                                --r.aise_application_error(-20011,'Falta operacoes consumo e/ou devolucao nos Parametros de estoque.');
                        end;

                        ie_atualiza_consignado_w		:= false;
                        :new.cd_operacao_estoque		:= cd_operacao_cons_paciente_w;
                        if	(:new.cd_acao = '2') then
                            :new.cd_operacao_estoque	:= cd_operacao_devol_paciente_w;
                        end if;
                        end;

                    end if;
                    end;
                end if;
                */

                if	(ie_atualiza_consignado_w) then
                    ie_movto_consig_w := 'S';
                else
                    ie_movto_consig_w := 'N';
                end if;

                /* verificar se material nao esta bloqueado para inventario */
                sup_verifica_bloqueio_mat_inv(	:new.cd_estabelecimento,
                                :new.dt_mesano_referencia,
                                :new.cd_local_estoque,
                                :new.cd_material_estoque,
                                :new.cd_fornecedor,
                                ie_movto_consig_w,
                                ie_bloqueio_inventario_w);

                if	(ie_bloqueio_inventario_w = 'S') then
                    begin
                    select	ds_material
                    into	ds_material_w
                    from	material
                    where	cd_material = :new.cd_material_estoque;
                    wheb_mensagem_pck.exibir_mensagem_abort(174138,'DS_MATERIAL='||ds_material_w);
                    /*r.aise_application_error(-20011,'Bloqueado p/inventario: ' || ds_material_w);*/
                    end;
                end if;

                /* define se o movimento foi de consignado ou normal*/
                :new.ie_movto_consignado 	:= 'N';
                if	(ie_atualiza_consignado_w) then
                    :new.ie_movto_consignado 	:= 'C';
                end if;

                ie_erro_ww 	:= 'N';
                ds_erro_w	:= null;
                ds_erro_ww	:= null;

                /* 	atualiza saldo */
                if 	(ie_material_estoque_w = 'S') and
                    (not ie_atualiza_consignado_w) then
                    begin
                    atualizar_saldo(
                        :new.cd_estabelecimento,
                        :new.cd_local_estoque,
                        :new.cd_material_estoque,
                        :new.dt_mesano_referencia,
                        :new.cd_operacao_estoque,
                        :new.qt_estoque,
                        :new.cd_acao,
                        :new.nm_usuario,
                        ie_erro_w,
                        ds_erro_w);

                    if	(ie_erro_w = 'S') then
                        ie_erro_ww := 'S';
                        ds_erro_ww := nvl(ds_erro_ww, ds_erro_w);
                    end if;

                    if	(:new.nr_seq_lote_fornec is not null) and
                        (ie_estoque_lote_w = 'S') and
                        (nr_emprestimo_w is null) then
                        begin
                        atualizar_saldo_lote(
                            :new.cd_estabelecimento,
                            :new.cd_local_estoque,
                            :new.cd_material_estoque,
                            :new.dt_mesano_referencia,
                            :new.nr_seq_lote_fornec,
                            :new.cd_operacao_estoque,
                            :new.qt_estoque,
                            :new.cd_acao,
                            :new.nm_usuario,
                            ie_erro_w);

                        if	(ie_erro_w = 'S') then
                            ie_erro_ww := 'S';
                            /* 799531 - Falha na atualizacao do saldo do lote.*/
                            ds_erro_ww := nvl(ds_erro_ww, wheb_mensagem_pck.get_texto(799531));
                        end if;
                        end;
                    end if;

                    if	(ie_tipo_requisicao_w = '5') and
                        (:new.ie_origem_documento <> '15') then
                        sup_atualizar_saldo_invent(
                            :new.cd_estabelecimento,
                            :new.dt_mesano_referencia,
                            :new.cd_local_estoque,
                            :new.cd_material_estoque,
                            1,
                            0,
                            :new.qt_estoque,
                            :new.nm_usuario);
                    end if;

                    dt_mesano_referencia_w	:=	pkg_date_utils.add_month(:new.dt_mesano_referencia, 1, 0);

                    while	(dt_mesano_referencia_w <= pkg_date_utils.start_of(sysdate,'MONTH',0)) loop
                        begin

                        begin
                        select	:new.qt_estoque
                        into	qt_estoque_ww
                        from	saldo_estoque
                        where	cd_estabelecimento	= :new.cd_estabelecimento
                        and	cd_local_estoque	= :new.cd_local_estoque
                        and	cd_material		= :new.cd_material_estoque
                        and	dt_mesano_referencia 	= dt_mesano_referencia_w;
                        exception
                        when others then
                            qt_estoque_ww	:=	0;
                        end;

                        atualizar_saldo(
                            :new.cd_estabelecimento,
                            :new.cd_local_estoque,
                            :new.cd_material_estoque,
                            dt_mesano_referencia_w,
                            :new.cd_operacao_estoque,
                            qt_estoque_ww, -- alterado para que nao seja acumulada a quantidade do movimento com o saldo atual ao atualizar o saldo do mes futuro,
                            :new.cd_acao,
                            :new.nm_usuario,
                                        ie_erro_w,
                            ds_erro_w);

                        if	(ie_erro_w = 'S') then
                            ie_erro_ww := 'S';
                            ds_erro_ww := nvl(ds_erro_ww, ds_erro_w);
                        end if;

                        if	(:new.nr_seq_lote_fornec is not null) and
                            (ie_estoque_lote_w = 'S') and
                            (nr_emprestimo_w is null) then
                            begin
                            begin
                            select	:new.qt_estoque
                            into	qt_estoque_lote_w
                            from	saldo_estoque_lote
                            where	cd_material = :new.cd_material_estoque
                            and	cd_local_estoque = :new.cd_local_estoque
                            and	cd_estabelecimento = :new.cd_estabelecimento
                            and	nr_seq_lote = :new.nr_seq_lote_fornec
                            and	dt_mesano_referencia = dt_mesano_referencia_w;
                            exception
                            when others then
                                qt_estoque_lote_w	:=	0;
                            end;

                            atualizar_saldo_lote(
                                :new.cd_estabelecimento,
                                            :new.cd_local_estoque,
                                :new.cd_material_estoque,
                                dt_mesano_referencia_w,
                                :new.nr_seq_lote_fornec,
                                :new.cd_operacao_estoque,
                                            qt_estoque_lote_w, -- alterado para que nao seja acumulada a quantidade do movimento com o saldo atual ao atualizar o saldo do mes futuro,
                                :new.cd_acao,
                                            :new.nm_usuario,
                                            ie_erro_w);

                            if	(ie_erro_w = 'S') then
                                ie_erro_ww := 'S';
                                /* 799531 - Falha na atualizacao do saldo do lote.*/
                                ds_erro_ww := nvl(ds_erro_ww, wheb_mensagem_pck.get_texto(799531));
                            end if;
                            end;
                        end if;

                        /*
                        if	(ie_tipo_requisicao_w = '5') then
                            sup_atualizar_saldo_invent(
                                :new.cd_estabelecimento,
                                dt_mesano_referencia_w,
                                :new.cd_local_estoque,
                                :new.cd_material_estoque,
                                1,
                                0,
                                :new.qt_estoque,
                                :new.nm_usuario);
                        end if;
                        */

                        dt_mesano_referencia_w	:=	pkg_date_utils.add_month(dt_mesano_referencia_w, 1, 0);
                        end;
                    end loop;
                    end;

                    /*tratamento para quando ocorre erro de atualizacao do saldo do lote e a atualizacao do saldo do material fica ok*/
                    if	(ie_erro_ww = 'S') then
                        ie_erro_w := 'S';
                        ds_erro_w := ds_erro_ww;
                    end if;

                    if	(ie_erro_w  = 'S') then
                        :new.ds_consistencia	:= ds_erro_w;
                    elsif	(ie_erro_w  = 'N') then
                        :new.dt_processo	:= sysdate;
                    end if;
                end if;

                /*  consignado         */
                /* tratar os dois casos */
                if 	(ie_material_estoque_w = 'S') and
                    (ie_atualiza_consignado_w) then
                    begin
                        atualizar_saldo_consig(
                        :new.cd_estabelecimento,
                        :new.cd_local_estoque,
                        :new.cd_fornecedor,
                        :new.cd_material_estoque,
                        :new.dt_mesano_referencia,
                        :new.cd_operacao_estoque,
                        :new.qt_estoque,
                        :new.cd_acao,
                        :new.nm_usuario,
                        ie_erro_w);

                    if	(ie_erro_w = 'S') then
                        ie_erro_ww := 'S';
                    end if;

                    if	(:new.nr_seq_lote_fornec is not null) and
                        (ie_estoque_lote_w = 'S') then
                        atualizar_saldo_consig_lote(
                            :new.cd_estabelecimento,
                            :new.cd_local_estoque,
                            :new.cd_fornecedor,
                            :new.cd_material_estoque,
                            :new.dt_mesano_referencia,
                            :new.nr_seq_lote_fornec,
                            :new.cd_operacao_estoque,
                            :new.qt_estoque,
                            :new.cd_acao,
                            :new.nm_usuario,
                            ie_erro_w);

                        if	(ie_erro_w = 'S') then
                            ie_erro_ww := 'S';
                        end if;
                    end if;

                    if	(ie_material_estoque_w = 'S') and
                    (ie_tipo_requisicao_w = '5') and
                    (:new.ie_origem_documento <> '15') then
                    sup_atualizar_saldo_invent(
                        :new.cd_estabelecimento,
                        :new.dt_mesano_referencia,
                        :new.cd_local_estoque,
                        :new.cd_material_estoque,
                        1,
                        0,
                        :new.qt_estoque,
                        :new.nm_usuario,
                        :new.cd_fornecedor);
                    end if;

                    dt_mesano_referencia_w	:=	pkg_date_utils.add_month(:new.dt_mesano_referencia, 1, 0);

                    while	(dt_mesano_referencia_w <= pkg_date_utils.start_of(sysdate,'MONTH',0)) loop
                        begin
                        begin
                        select	:new.qt_estoque
                        into	qt_estoque_ww
                        from	fornecedor_mat_consignado
                        where	cd_fornecedor		= :new.cd_fornecedor
                        and	cd_local_estoque	= :new.cd_local_estoque
                        and	cd_material		= :new.cd_material_estoque
                        and	dt_mesano_referencia	=dt_mesano_referencia_w;
                        exception
                        when others then
                            qt_estoque_ww	:=	0;
                        end;

                        atualizar_saldo_consig(
                            :new.cd_estabelecimento,
                            :new.cd_local_estoque,
                            :new.cd_fornecedor,
                            :new.cd_material_estoque,
                            dt_mesano_referencia_w,
                            :new.cd_operacao_estoque,
                            qt_estoque_ww,-- alterado para que nao seja acumulada a quantidade do movimento com o saldo atual ao atualizar o saldo do mes futuro,
                            :new.cd_acao,
                            :new.nm_usuario,
                            ie_erro_w);

                        if	(ie_erro_w = 'S') then
                            ie_erro_ww := 'S';
                        end if;

                        if	(:new.nr_seq_lote_fornec is not null) and
                            (ie_estoque_lote_w = 'S') then

                            begin
                            select	:new.qt_estoque
                            into	qt_estoque_lote_w
                            from	fornecedor_mat_consig_lote
                            where	cd_material = :new.cd_material_estoque
                            and	cd_local_estoque = :new.cd_local_estoque
                            and	cd_estabelecimento = :new.cd_estabelecimento
                            and	nr_seq_lote = :new.nr_seq_lote_fornec
                            and	cd_fornecedor = :new.cd_fornecedor
                            and	dt_mesano_referencia = dt_mesano_referencia_w;
                            exception
                            when others then
                                qt_estoque_lote_w	:=	0;
                            end;

                            atualizar_saldo_consig_lote(
                                :new.cd_estabelecimento,
                                            :new.cd_local_estoque,
                                :new.cd_fornecedor,
                                :new.cd_material_estoque,
                                dt_mesano_referencia_w,
                                :new.nr_seq_lote_fornec,
                                :new.cd_operacao_estoque,
                                            qt_estoque_lote_w,-- alterado para que nao seja acumulada a quantidade do movimento com o saldo atual ao atualizar o saldo do mes futuro,
                                :new.cd_acao,
                                            :new.nm_usuario,
                                            ie_erro_w);

                            if	(ie_erro_w = 'S') then
                                ie_erro_ww := 'S';
                            end if;
                        end if;

                        dt_mesano_referencia_w	:=	pkg_date_utils.add_month(dt_mesano_referencia_w, 1, 0);
                        end;
                    end loop;

                    if	(ie_erro_ww = 'S') then
                        ie_erro_w := 'S';
                    end if;

                    if	(ie_erro_w  = 'S') then
                        :new.ds_consistencia	:= ds_erro_w;
                    elsif	(ie_erro_w  = 'N') then
                        :new.dt_processo := sysdate;
                    end if;
                    end;
                end if;

                select	count(*)
                into	qt_existe_regra_email_w
                from	regra_envio_email_material
                where	ie_tipo_mensagem = 6;

                if	(qt_existe_regra_email_w > 0) then
                    begin
                    gerar_email_movto_material(
                        :new.nr_movimento_estoque,
                        :new.cd_material,
                        :new.cd_acao,
                        :new.cd_operacao_estoque,
                        :new.ie_origem_documento,
                        :new.cd_local_estoque,
                        :new.cd_local_estoque_destino,
                        :new.cd_setor_atendimento,
                        :new.dt_movimento_estoque,
                        :new.qt_movimento,
                        :new.qt_estoque,
                        :new.cd_estabelecimento,
                        :new.nm_usuario);
                    exception
                    when others then
                        qt_existe_regra_email_w := 0;
                    end;
                end if;
                end;
            end if;
        end;
    end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.movimento_estoque_afinsert
after insert ON TASY.MOVIMENTO_ESTOQUE for each row
declare
nr_sequencia_w      number(10);
qt_existe_w     number(10);
reg_integracao_p    gerar_int_padrao.reg_integracao;
ie_entrada_saida_w  operacao_estoque.ie_entrada_saida%type;
ie_tipo_requisicao_w    operacao_estoque.ie_tipo_requisicao%type;
ie_laudo_w      int;
qt_proced_1_mes_w   sus_laudo_medicamento.qt_proced_1_mes%type;
qt_proced_2_mes_w   sus_laudo_medicamento.qt_proced_2_mes%type;
qt_proced_3_mes_w   sus_laudo_medicamento.qt_proced_3_mes%type;
dt_pri_tratamento_w sus_laudo_paciente.dt_pri_tratamento%type;
dt_seg_tratamento_w sus_laudo_paciente.dt_seg_tratamento%type;
dt_ter_tratamento_w sus_laudo_paciente.dt_ter_tratamento%type;
nr_mov_estoque_w            movimento_estoque.nr_movimento_estoque%type;
qt_movimento_w              movimento_estoque.qt_movimento%type;
ds_retorno_integracao_w     clob;

pragma autonomous_transaction;

begin
if  (:new.dt_processo is not null) then
    begin
    select  ie_entrada_saida
    into    ie_entrada_saida_w
    from    operacao_estoque
    where   cd_operacao_estoque = :new.cd_operacao_estoque;

    if  (:new.cd_acao = '2') then
        select  decode(ie_entrada_saida_w,'E','S','S','E')
        into    ie_entrada_saida_w
        from    dual;
    end if;

    select  cd_grupo_material,
        cd_subgrupo_material,
        cd_classe_material,
        nr_seq_familia
    into    reg_integracao_p.cd_grupo_material,
        reg_integracao_p.cd_subgrupo_material,
        reg_integracao_p.cd_classe_material,
        reg_integracao_p.nr_seq_familia
    from    estrutura_material_v
    where   cd_material = :new.cd_material;

    reg_integracao_p.ie_operacao            :=  'I';
    reg_integracao_p.cd_estab_documento     :=  :new.cd_estabelecimento;
    reg_integracao_p.cd_material            :=  :new.cd_material;
    reg_integracao_p.ie_padronizado         :=  substr(obter_se_material_padronizado(:new.cd_estabelecimento, :new.cd_material),1,1);
    reg_integracao_p.cd_local_estoque       :=  :new.cd_local_estoque;
    reg_integracao_p.cd_centro_custo        :=  :new.cd_centro_custo;
    reg_integracao_p.cd_operacao_estoque        :=  :new.cd_operacao_estoque;
    reg_integracao_p.ie_origem_documento        :=  :new.ie_origem_documento;
    reg_integracao_p.cd_local_estoque_destino   :=  :new.cd_local_estoque_destino;

    gerar_int_padrao.gravar_integracao('10', :new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);
    --Integracao com Horus
    if  (nvl(:new.nr_seq_lote_fornec, 0) <> 0) then
        if  (ie_entrada_saida_w = 'E') then
            --entradas
            gerar_int_padrao.gravar_integracao('393',:new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);

            select  bifrost.send_integration(
                'drugEntry.send.request',
                'com.philips.tasy.integration.stock.outbound.DrugEntryCallback',
                '{"document":'||:new.nr_movimento_estoque||', "userName":"'||:new.nm_usuario||'", "event":"393",
                "establishment":'||nvl(:new.cd_estabelecimento, 0)||',
                "stockLocation":'||nvl(:new.cd_local_estoque, 0)||',
                "stockMovementDocument":'||nvl(:new.nr_documento, 0)||',
                "supplierBatch":'||nvl(:new.nr_seq_lote_fornec, 0)||',
				"stockQuantity":'||nvl(:new.qt_estoque, 0)||',
                "stockOperation":'||nvl(:new.cd_operacao_estoque, 0)||'}',
                :new.nm_usuario)
            into    ds_retorno_integracao_w
            from    dual;

        else
            --saida
            gerar_int_padrao.gravar_integracao('394', :new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);

            select  bifrost.send_integration(
                'drugExit.send.request',
                'com.philips.tasy.integration.stock.outbound.DrugExitCallback',
                '{"document":'||:new.nr_movimento_estoque||', "userName":"'||:new.nm_usuario||'", "event":"394",
                "establishment":'||nvl(:new.cd_estabelecimento,0)||',
                "stockLocation":'||nvl(:new.cd_local_estoque,0)||',
                "destinationStockLocation":'||nvl(:new.cd_local_estoque_destino,0)||',
                "supplierBatch":'||nvl(:new.nr_seq_lote_fornec,0)||',
				"stockQuantity":'||nvl(:new.qt_estoque, 0)||',
                "stockOperation":'||nvl(:new.cd_operacao_estoque,0)||'}',
                :new.nm_usuario)
            into    ds_retorno_integracao_w
            from    dual;
        end if;

        select  nvl(ie_tipo_requisicao,0)
        into    ie_tipo_requisicao_w
        from    operacao_estoque
        where   cd_operacao_estoque = :new.cd_operacao_estoque;

        select  nvl(max(b.nr_movimento_estoque),0)
        into    nr_mov_estoque_w
        from    transm_bndasaf a,
            movimento_estoque b
        where   a.nr_atendimento = b.nr_atendimento
        and     b.cd_material = :new.cd_material
        and     (select nvl(max(c.nr_documento),0)
				from movimento_estoque c
				where c.nr_movimento_estoque = a.cd_registro_origem) = to_char(:new.nr_documento)
		and		nvl(a.nr_atendimento,0) = nvl(:new.nr_atendimento,0)
        and     a.ie_evento = '396'
        and     b.cd_acao = 1
        and     :new.cd_acao <> 1;

        if  (ie_tipo_requisicao_w = 4 and nr_mov_estoque_w = 0 )then
            select  count(*)
            into    ie_laudo_w
            from    sus_laudo_paciente a,
                sus_material_opm d,
                sus_laudo_medicamento f
            where   1 = 1
            and     a.nr_atendimento = :new.nr_atendimento
            and     d.cd_material = :new.cd_material
            and     d.cd_procedimento = f.cd_procedimento
            and     f.nr_seq_laudo_sus = a.nr_seq_interno;

            -- Verifica se a movimentacao possui laudo do sus e busca a quantidade para criacao de dispensacao
            if  (ie_laudo_w <> 0) then

                select  distinct
                    f.qt_proced_1_mes,
                    f.qt_proced_2_mes,
                    f.qt_proced_3_mes,
                    to_date(a.DT_PRI_TRATAMENTO,'mm/yyyy'),
                    to_date(a.DT_SEG_TRATAMENTO,'mm/yyyy'),
                    to_date(a.DT_TER_TRATAMENTO,'mm/yyyy')
                into    qt_proced_1_mes_w,
                    qt_proced_2_mes_w,
                    qt_proced_3_mes_w,
                    dt_pri_tratamento_w,
                    dt_seg_tratamento_w,
                    dt_ter_tratamento_w
                from    sus_laudo_paciente a,
                    sus_material_opm d,
                    sus_laudo_medicamento f
                where   1 = 1
                and     a.nr_atendimento = :new.nr_atendimento
                and     d.cd_material =  :new.cd_material
                and     d.cd_procedimento = f.cd_procedimento
                and     f.nr_seq_laudo_sus = a.nr_seq_interno
                and rownum = 1;

                if  (:new.dt_mesano_referencia = dt_pri_tratamento_w)
                    and (:new.qt_movimento > qt_proced_1_mes_w )
                    and (:new.qt_movimento = (qt_proced_1_mes_w + qt_proced_2_mes_w + qt_proced_3_mes_w)) then

                    reg_integracao_p.qt_mes := qt_proced_1_mes_w;
                    reg_integracao_p.dt_processo := DT_PRI_TRATAMENTO_w;
                    reg_integracao_p.ie_lido := 'S';
                    gerar_int_padrao.gravar_integracao('396', :new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);

                    reg_integracao_p.qt_mes := qt_proced_2_mes_w;
                    reg_integracao_p.dt_processo := DT_SEG_TRATAMENTO_w;
                    reg_integracao_p.ie_lido := 'S';
                    gerar_int_padrao.gravar_integracao('396', :new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);

                    reg_integracao_p.qt_mes := qt_proced_3_mes_w;
                    reg_integracao_p.dt_processo := DT_TER_TRATAMENTO_w;
                    reg_integracao_p.ie_lido := 'S';
                    gerar_int_padrao.gravar_integracao('396', :new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);

                elsif   (:new.dt_mesano_referencia = DT_SEG_TRATAMENTO_w)
                    and (:new.qt_movimento > qt_proced_2_mes_w)
                    and (:new.qt_movimento = (qt_proced_2_mes_w + qt_proced_3_mes_w)) then

                    reg_integracao_p.qt_mes := qt_proced_2_mes_w;
                    reg_integracao_p.dt_processo := DT_SEG_TRATAMENTO_w;
                    reg_integracao_p.ie_lido := 'S';
                    gerar_int_padrao.gravar_integracao('396', :new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);

                    reg_integracao_p.qt_mes := qt_proced_3_mes_w;
                    reg_integracao_p.dt_processo := DT_TER_TRATAMENTO_w;
                    reg_integracao_p.ie_lido := 'S';
                    gerar_int_padrao.gravar_integracao('396', :new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);
                else
                    gerar_int_padrao.gravar_integracao('396', :new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);
                end if;
            else
                gerar_int_padrao.gravar_integracao('396', :new.nr_movimento_estoque, :new.nm_usuario, reg_integracao_p);

            end if;

            select  bifrost.send_integration('dispensingMedicines.send.request',
                'com.philips.tasy.integration.stock.dispensation.DispensingMedicinesCallback',
                '{"document":'||:new.nr_movimento_estoque||', "userName":"'||:new.nm_usuario||'", "event":"396",
                "establishment":'||nvl(:new.cd_estabelecimento, 0)||',
                "stockLocation":'||nvl(:new.cd_local_estoque, 0)||',
                "supplierBatch":'||nvl(:new.nr_seq_lote_fornec, 0)||',
                "encounter":'||nvl(:new.nr_atendimento, 0)||',
                "prescription":'||nvl(:new.nr_prescricao, 0)||',
                "dispensingAmount":'||nvl(:new.qt_movimento, 0)||',
                "referenceMonthYear":"'||:new.dt_mesano_referencia||'",
                "dispensingDate":"'||:new.dt_movimento_estoque||'"}',
                :new.nm_usuario)
            into    ds_retorno_integracao_w
            from    dual;

        elsif (ie_tipo_requisicao_w in ('3', '4') and nr_mov_estoque_w > 0) then

            select  qt_movimento
            into    qt_movimento_w
            from    movimento_estoque
            where   nr_movimento_estoque = nr_mov_estoque_w;

            reg_integracao_p.ie_operacao                :=  'I';
            reg_integracao_p.cd_estab_documento         :=  :new.cd_estabelecimento;
            reg_integracao_p.cd_material                :=  :new.cd_material;
            reg_integracao_p.ie_padronizado             :=  substr(obter_se_material_padronizado(:new.cd_estabelecimento, :new.cd_material),1,1);
            reg_integracao_p.cd_local_estoque           :=  :new.cd_local_estoque;
            reg_integracao_p.cd_centro_custo            :=  :new.cd_centro_custo;
            reg_integracao_p.cd_operacao_estoque        :=  :new.cd_operacao_estoque;
            reg_integracao_p.ie_origem_documento        :=  :new.ie_origem_documento;
            reg_integracao_p.cd_local_estoque_destino   :=  :new.cd_local_estoque_destino;

            if  (:new.qt_movimento = qt_movimento_w) then
                gerar_int_padrao.gravar_integracao('397', nr_mov_estoque_w, :new.nm_usuario, reg_integracao_p);--exclusao
                commit;

                select  bifrost.send_integration(
                    'recordDeletion.send.request',
                    'com.philips.tasy.integration.stock.outbound.RecordDeletionCallback',
                    ''||nr_mov_estoque_w||'',
                    :new.nm_usuario)
                into    ds_retorno_integracao_w
                from    dual;
            else
                gerar_int_padrao.gravar_integracao('398', nr_mov_estoque_w, :new.nm_usuario, reg_integracao_p);--retificacao

                select  bifrost.send_integration(
                    'retificationDispensingMedicines.send.request',
                    'com.philips.tasy.integration.stock.dispensation.RetificationDispensingMedicinesCallback',
                    '{"document":'||:new.nr_movimento_estoque||', "userName":"'||:new.nm_usuario||'", "event":"398",
                    "stockLocation":'||nvl(:new.cd_local_estoque, 0)||',
                    "supplierBatch":'||nvl(:new.nr_seq_lote_fornec, 0)||',
                    "referenceMonthYear":"'||:new.dt_mesano_referencia||'",
                    "dispensingAmount":'||nvl(:new.qt_movimento, 0)||',
                    "dispensingDate":"'||:new.dt_movimento_estoque||'",
                    "encounter":"'||:new.nr_atendimento||'",
                    "prescription":'||nvl(:new.nr_prescricao, 0)||'}',
                    :new.nm_usuario)
                into    ds_retorno_integracao_w
                from    dual;
            end if;

            delete from  transm_bndasaf
            where cd_registro_origem = nr_mov_estoque_w;
        end if;
    end if;
    end;
end if;

select  count(*)
into    qt_existe_w
from    sup_parametro_integracao a
where   a.ie_evento = 'ME'
and a.ie_forma = 'E'
and a.ie_situacao = 'A';

if  (qt_existe_w > 0) and
    (:old.dt_processo is null) and
    (:new.dt_processo is not null) then

    gravar_sup_int_movto_est(
        :new.nr_movimento_estoque,
        :new.cd_estabelecimento,
        :new.dt_mesano_referencia,
        :new.cd_local_estoque,
        :new.cd_material,
        :new.qt_movimento,
        :new.qt_estoque,
        :new.dt_movimento_estoque,
        :new.cd_local_estoque_destino,
        :new.cd_operacao_estoque,
        :new.dt_processo,
        :new.ie_origem_documento,
        :new.cd_unidade_med_mov,
        :new.qt_inventario,
        :new.cd_conta_contabil,
        :new.cd_acao,
        :new.cd_centro_custo,
        :new.cd_material_estoque,
        :new.cd_unidade_medida_estoque,
        :new.cd_fornecedor,
        :new.cd_lote_fabricacao,
        :new.dt_validade,
        :new.ds_observacao,
        :new.cd_cgc_emitente,
        :new.cd_serie_nf,
        :new.nr_documento,
        :new.nr_sequencia_item_docto,
        :new.nr_sequencia_documento,
        :new.dt_contabil,
        :new.ie_movto_consignado,
        :new.cd_setor_atendimento,
        :new.nr_atendimento,
        :new.nr_prescricao,
        :new.nr_ordem_compra,
        :new.nr_item_oci,
        :new.nr_lote_ap,
        :new.cd_funcao,
        :new.cd_perfil,
        :new.nr_receita,
        :new.nr_seq_lote_fornec,
        :new.nr_seq_tab_orig,
        :new.nr_lote_contabil,
        :new.nm_usuario);
end if;

if  (:new.dt_processo is null) and
    (:new.ds_consistencia is not null) then
    begin
    insert into sup_movto_pendente(
        nr_sequencia,
        nr_movimento_estoque,
        ie_pendente,
        dt_atualizacao,
        dt_atualizacao_nrec,
        nm_usuario,
        nm_usuario_nrec)
    values (sup_movto_pendente_seq.nextval,
        :new.nr_movimento_estoque,
        'S',
        sysdate,
        sysdate,
        :new.nm_usuario,
        :new.nm_usuario);
    end;
end if;
commit;
end;
/


ALTER TABLE TASY.MOVIMENTO_ESTOQUE ADD (
  CONSTRAINT MOVESTO_PK
 PRIMARY KEY
 (NR_MOVIMENTO_ESTOQUE)
    USING INDEX 
    TABLESPACE TASY_INDEX_G
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          312M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MOVIMENTO_ESTOQUE ADD (
  CONSTRAINT MOVESTO_MOPERQ_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PERDA_QUIMIO) 
 REFERENCES TASY.MOTIVO_PERDA_QUIMIO (NR_SEQUENCIA),
  CONSTRAINT MOVESTO_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT MOVESTO_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT MOVESTO_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT MOVESTO_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT MOVESTO_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MOVESTO_OPEESTO_FK 
 FOREIGN KEY (CD_OPERACAO_ESTOQUE) 
 REFERENCES TASY.OPERACAO_ESTOQUE (CD_OPERACAO_ESTOQUE),
  CONSTRAINT MOVESTO_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_ESTOQUE) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MOVESTO_PESJURI_FK 
 FOREIGN KEY (CD_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT MOVESTO_MATLOFO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_FORNEC) 
 REFERENCES TASY.MATERIAL_LOTE_FORNEC (NR_SEQUENCIA),
  CONSTRAINT MOVESTO_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT MOVESTO_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT MOVESTO_LOTPROD_FK 
 FOREIGN KEY (NR_LOTE_PRODUCAO) 
 REFERENCES TASY.LOTE_PRODUCAO (NR_LOTE_PRODUCAO));

GRANT SELECT ON TASY.MOVIMENTO_ESTOQUE TO NIVEL_1;

