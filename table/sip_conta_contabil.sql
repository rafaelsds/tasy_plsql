ALTER TABLE TASY.SIP_CONTA_CONTABIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SIP_CONTA_CONTABIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.SIP_CONTA_CONTABIL
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO_ITEM_DESP  NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_CONTA_CONTABIL      VARCHAR2(20 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SIPCOCO_CONCONT_FK_I ON TASY.SIP_CONTA_CONTABIL
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPCOCO_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SIPCOCO_ESTABEL_FK_I ON TASY.SIP_CONTA_CONTABIL
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPCOCO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SIPCOCO_PK ON TASY.SIP_CONTA_CONTABIL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPCOCO_PK
  MONITORING USAGE;


CREATE INDEX TASY.SIPCOCO_SIPTIID_FK_I ON TASY.SIP_CONTA_CONTABIL
(NR_SEQ_TIPO_ITEM_DESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SIPCOCO_SIPTIID_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SIP_CONTA_CONTABIL ADD (
  CONSTRAINT SIPCOCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SIP_CONTA_CONTABIL ADD (
  CONSTRAINT SIPCOCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT SIPCOCO_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT SIPCOCO_SIPTIID_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ITEM_DESP) 
 REFERENCES TASY.SIP_TIPO_ITEM_DESPESA (NR_SEQUENCIA));

GRANT SELECT ON TASY.SIP_CONTA_CONTABIL TO NIVEL_1;

