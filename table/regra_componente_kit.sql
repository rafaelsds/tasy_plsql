ALTER TABLE TASY.REGRA_COMPONENTE_KIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_COMPONENTE_KIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_COMPONENTE_KIT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_KIT_MATERIAL        NUMBER(5)              NOT NULL,
  NR_SEQ_COMPONENTE      NUMBER(5)              NOT NULL,
  IE_REGRA               VARCHAR2(1 BYTE)       NOT NULL,
  IE_UTILIZA_COMPONENTE  VARCHAR2(1 BYTE)       NOT NULL,
  CD_MEDICO              VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RECOMPKI_COMKIT_FK_I ON TASY.REGRA_COMPONENTE_KIT
(CD_KIT_MATERIAL, NR_SEQ_COMPONENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RECOMPKI_MEDICO_FK_I ON TASY.REGRA_COMPONENTE_KIT
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RECOMPKI_PK ON TASY.REGRA_COMPONENTE_KIT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_COMPONENTE_KIT ADD (
  CONSTRAINT RECOMPKI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_COMPONENTE_KIT ADD (
  CONSTRAINT RECOMPKI_COMKIT_FK 
 FOREIGN KEY (CD_KIT_MATERIAL, NR_SEQ_COMPONENTE) 
 REFERENCES TASY.COMPONENTE_KIT (CD_KIT_MATERIAL,NR_SEQUENCIA),
  CONSTRAINT RECOMPKI_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.REGRA_COMPONENTE_KIT TO NIVEL_1;

