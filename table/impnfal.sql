ALTER TABLE TASY.IMPNFAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.IMPNFAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMPNFAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CARGA         NUMBER(10)               NOT NULL,
  DT_INICIO_PROC       DATE,
  DT_FIM_PROC          DATE,
  NR_SEQ_CARGA_ARQ     NUMBER(10)               NOT NULL,
  MANDT                VARCHAR2(4 BYTE),
  EINRI                VARCHAR2(5 BYTE),
  FALNR                VARCHAR2(11 BYTE)        NOT NULL,
  FALAR                VARCHAR2(2 BYTE),
  PATNR                VARCHAR2(11 BYTE)        NOT NULL,
  BEKAT                VARCHAR2(7 BYTE),
  ABRKZ                VARCHAR2(2 BYTE),
  SICHV                VARCHAR2(2 BYTE),
  EINZG                VARCHAR2(10 BYTE),
  KZTXT                VARCHAR2(31 BYTE),
  KUALF                VARCHAR2(11 BYTE),
  BEWLF                VARCHAR2(6 BYTE),
  DGNLF                VARCHAR2(4 BYTE),
  PGRLF                VARCHAR2(4 BYTE),
  INFKZ                VARCHAR2(2 BYTE),
  STATU                VARCHAR2(2 BYTE),
  FZIFF                VARCHAR2(2 BYTE),
  NOTAN                VARCHAR2(2 BYTE),
  KRZAN                VARCHAR2(2 BYTE),
  ENDAT                VARCHAR2(30 BYTE),
  ENTIM                VARCHAR2(30 BYTE),
  FGTYP                VARCHAR2(2 BYTE),
  KZKOM                VARCHAR2(2 BYTE),
  KOMTX                VARCHAR2(51 BYTE),
  KOLTX                VARCHAR2(2 BYTE),
  ARBUN                VARCHAR2(30 BYTE),
  ENDDT                VARCHAR2(30 BYTE),
  FSPER                VARCHAR2(2 BYTE),
  ERDAT                VARCHAR2(30 BYTE),
  ERUSR                VARCHAR2(13 BYTE),
  UPDAT                VARCHAR2(30 BYTE),
  UPUSR                VARCHAR2(13 BYTE),
  STORN                VARCHAR2(2 BYTE),
  STUSR                VARCHAR2(13 BYTE),
  STDAT                VARCHAR2(30 BYTE),
  VORPT                VARCHAR2(4 BYTE),
  BEGDT                VARCHAR2(30 BYTE),
  STASP                VARCHAR2(2 BYTE),
  KV_KZ                VARCHAR2(2 BYTE),
  EAUFN                VARCHAR2(13 BYTE),
  KVUEB                VARCHAR2(2 BYTE),
  VORGR                VARCHAR2(4 BYTE),
  OBJNR                VARCHAR2(23 BYTE),
  RESID                VARCHAR2(2 BYTE),
  CHILD                VARCHAR2(3 BYTE),
  FOREI                VARCHAR2(2 BYTE),
  FATYP                VARCHAR2(3 BYTE),
  EMTYP                VARCHAR2(3 BYTE),
  KTTAR                VARCHAR2(4 BYTE),
  KTABK                VARCHAR2(4 BYTE),
  GSTA1                VARCHAR2(4 BYTE),
  GSTA2                VARCHAR2(4 BYTE),
  ANZSC                VARCHAR2(3 BYTE),
  ANZTD                VARCHAR2(3 BYTE),
  ANZAB                VARCHAR2(3 BYTE),
  LGBIS                VARCHAR2(30 BYTE),
  FPEDT                VARCHAR2(30 BYTE),
  APSTATE              VARCHAR2(3 BYTE),
  FACHR                VARCHAR2(5 BYTE),
  ENDTYP               VARCHAR2(3 BYTE),
  DOCTY                VARCHAR2(3 BYTE),
  DOCNR                VARCHAR2(31 BYTE),
  WKAT                 VARCHAR2(2 BYTE),
  DOCW                 VARCHAR2(2 BYTE),
  PATW                 VARCHAR2(2 BYTE),
  PATGEW               VARCHAR2(8 BYTE),
  GWEIN                VARCHAR2(4 BYTE),
  PATGRO               VARCHAR2(6 BYTE),
  GREIN                VARCHAR2(4 BYTE),
  RESPI                VARCHAR2(6 BYTE),
  FEXNR                VARCHAR2(21 BYTE),
  TIMESTAMP            VARCHAR2(30 BYTE)        DEFAULT null,
  TOB                  VARCHAR2(4 BYTE),
  PLSNR                VARCHAR2(31 BYTE),
  INV_COPY             VARCHAR2(2 BYTE),
  SAPS_C               VARCHAR2(4 BYTE),
  PIM2_C               VARCHAR2(4 BYTE),
  CRIB_C               VARCHAR2(4 BYTE),
  NEMS_C               VARCHAR2(7 BYTE),
  MHLS                 VARCHAR2(2 BYTE),
  READM                VARCHAR2(2 BYTE),
  LMPER                VARCHAR2(30 BYTE),
  CONSENT              VARCHAR2(2 BYTE),
  ABPER                VARCHAR2(5 BYTE),
  OCCDISEASE           VARCHAR2(2 BYTE),
  ERM_HSA              VARCHAR2(6 BYTE),
  STAB                 VARCHAR2(2 BYTE),
  DIS_CONS             VARCHAR2(2 BYTE),
  DIS_CONS_DAT         VARCHAR2(30 BYTE),
  DS_CHAVE_TASY        VARCHAR2(255 BYTE),
  IE_STATUS            VARCHAR2(10 BYTE),
  NR_LINHA             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IMPNFAL_GERCARI_FK_I ON TASY.IMPNFAL
(NR_SEQ_CARGA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.IMPNFAL_GERCGARQ_FK_I ON TASY.IMPNFAL
(NR_SEQ_CARGA_ARQ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.IMPNFAL_PK ON TASY.IMPNFAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMPNFAL ADD (
  CONSTRAINT IMPNFAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.IMPNFAL ADD (
  CONSTRAINT IMPNFAL_GERCARI_FK 
 FOREIGN KEY (NR_SEQ_CARGA) 
 REFERENCES TASY.GER_CARGA_INICIAL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT IMPNFAL_GERCGARQ_FK 
 FOREIGN KEY (NR_SEQ_CARGA_ARQ) 
 REFERENCES TASY.GER_CARGA_ARQ (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.IMPNFAL TO NIVEL_1;

