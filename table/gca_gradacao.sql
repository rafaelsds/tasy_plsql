ALTER TABLE TASY.GCA_GRADACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GCA_GRADACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.GCA_GRADACAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DS_GRADACAO           VARCHAR2(80 BYTE)       NOT NULL,
  VL_INICIAL            NUMBER(3)               NOT NULL,
  VL_FINAL              NUMBER(3)               NOT NULL,
  DS_COR_FONTE          VARCHAR2(15 BYTE),
  DS_COR_FUNDO          VARCHAR2(15 BYTE),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  QT_IDADE_MIN          NUMBER(3),
  QT_IDADE_MAX          NUMBER(3),
  IE_LEGENDA            VARCHAR2(1 BYTE),
  QT_IDADE_MIN_MES      NUMBER(15,2),
  QT_IDADE_MIN_DIA      NUMBER(15,2),
  QT_IDADE_MAX_MES      NUMBER(15,2),
  QT_IDADE_MAX_DIA      NUMBER(15,2),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  IE_AUTOR              VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.GCAGRAD_PK ON TASY.GCA_GRADACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GCAGRAD_SETATEN_FK_I ON TASY.GCA_GRADACAO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GCAGRAD_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.GCA_GRADACAO ADD (
  CONSTRAINT GCAGRAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GCA_GRADACAO ADD (
  CONSTRAINT GCAGRAD_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.GCA_GRADACAO TO NIVEL_1;

