ALTER TABLE TASY.PERFIL_ITEM_PRONT_PASTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERFIL_ITEM_PRONT_PASTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERFIL_ITEM_PRONT_PASTA
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_SEQ_ITEM_PERFIL        NUMBER(10),
  NR_SEQ_ITEM_PASTA         NUMBER(10)          NOT NULL,
  DS_PASTA_INSTITUICAO      VARCHAR2(255 BYTE),
  NR_SEQ_ITEM_PERFIL_PEPO   NUMBER(10),
  NR_SEQ_ITEM_PERFIL_FANEP  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEIPRPA_PEIESAN_FK_I ON TASY.PERFIL_ITEM_PRONT_PASTA
(NR_SEQ_ITEM_PERFIL_FANEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEIPRPA_PEIESAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEIPRPA_PERITEP_FK_I ON TASY.PERFIL_ITEM_PRONT_PASTA
(NR_SEQ_ITEM_PERFIL_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEIPRPA_PERITEP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEIPRPA_PERITPR_FK_I ON TASY.PERFIL_ITEM_PRONT_PASTA
(NR_SEQ_ITEM_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEIPRPA_PK ON TASY.PERFIL_ITEM_PRONT_PASTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEIPRPA_PROITPA_FK_I ON TASY.PERFIL_ITEM_PRONT_PASTA
(NR_SEQ_ITEM_PASTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEIPRPA_UK ON TASY.PERFIL_ITEM_PRONT_PASTA
(NR_SEQ_ITEM_PASTA, NR_SEQ_ITEM_PERFIL, NR_SEQ_ITEM_PERFIL_PEPO, NR_SEQ_ITEM_PERFIL_FANEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PERFIL_ITEM_PRONT_PASTA_ATUAL
before INSERT OR UPDATE OR DELETE ON TASY.PERFIL_ITEM_PRONT_PASTA for each row
declare

nr_pasta_filha_w number(10);
qt_registro_sup_w number(10);
nr_seq_pasta_sup_w number(10);
nr_seq_dic_obj_w number(10);
qt_reg_config_comp_w number(10);

Cursor C01 is
	SELECT  a.NR_SEQUENCIA
	FROM 	perfil_item_pront_pasta a,
			PERFIL_ITEM_PRONT b
	WHERE	a.NR_SEQ_ITEM_PERFIL = b.nr_sequencia
	AND		b.nr_sequencia = :old.NR_SEQ_ITEM_PERFIL
	AND		a.nr_sequencia IN (
		SELECT x.nr_sequencia
		FROM	perfil_item_pront_pasta x
		WHERE	x.nr_seq_item_pasta IN (
			SELECT 	  p.nr_sequencia
			FROM	  prontuario_item_pasta p
			WHERE	  p.nr_seq_pasta IN (
				SELECT z.NR_sequencia
				FROM	prontuario_pasta z
				WHERE	z.nr_seq_pasta_superior IN (obter_seq_pasta_superior(:old.nr_seq_item_pasta, 'P'))
			)
		)
	);

pragma autonomous_transaction;

begin

if (deleting) then
	open C01;
		loop
		fetch C01 into
			nr_pasta_filha_w;
		exit when C01%notfound;
			begin

				if (nvl(nr_pasta_filha_w, 0) > 0) then
					delete from perfil_item_pront_pasta where nr_sequencia = nr_pasta_filha_w;
					COMMIT;
				end if;

			end;
		end loop;
	close C01;

elsif (inserting or updating) then

	select nvl(obter_seq_pasta_superior(:new.nr_seq_item_pasta, 'F'),0)
	into nr_seq_pasta_sup_w
	from dual;

	if (nr_seq_pasta_sup_w > 0) then

		SELECT  COUNT(*)
		into	qt_registro_sup_w
		FROM 	perfil_item_pront_pasta a,
				PERFIL_ITEM_PRONT b
		WHERE	a.NR_SEQ_ITEM_PERFIL = b.nr_sequencia
		AND		b.nr_sequencia = :new.NR_SEQ_ITEM_PERFIL
		AND		a.nr_sequencia IN (
				SELECT x.nr_sequencia
				FROM	perfil_item_pront_pasta x
				WHERE	x.nr_seq_item_pasta IN (
					SELECT 	  p.nr_sequencia
					FROM	  prontuario_item_pasta p
					WHERE	  p.nr_seq_pasta IN (nr_seq_pasta_sup_w))
		);

		if (qt_registro_sup_w = 0) then
			wheb_mensagem_pck.exibir_mensagem_abort(449071);
		end if;
	end if;

end if;
end;
/


ALTER TABLE TASY.PERFIL_ITEM_PRONT_PASTA ADD (
  CONSTRAINT PEIPRPA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PEIPRPA_UK
 UNIQUE (NR_SEQ_ITEM_PASTA, NR_SEQ_ITEM_PERFIL, NR_SEQ_ITEM_PERFIL_PEPO, NR_SEQ_ITEM_PERFIL_FANEP)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERFIL_ITEM_PRONT_PASTA ADD (
  CONSTRAINT PEIPRPA_PROITPA_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PASTA) 
 REFERENCES TASY.PRONTUARIO_ITEM_PASTA (NR_SEQUENCIA),
  CONSTRAINT PEIPRPA_PERITPR_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PERFIL) 
 REFERENCES TASY.PERFIL_ITEM_PRONT (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEIPRPA_PERITEP_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PERFIL_PEPO) 
 REFERENCES TASY.PERFIL_ITEM_PEPO (NR_SEQUENCIA),
  CONSTRAINT PEIPRPA_PEIESAN_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PERFIL_FANEP) 
 REFERENCES TASY.PERFIL_ITEM_FANEP (NR_SEQUENCIA));

GRANT SELECT ON TASY.PERFIL_ITEM_PRONT_PASTA TO NIVEL_1;

