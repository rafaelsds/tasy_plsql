ALTER TABLE TASY.PROCED_MEDIC_REACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCED_MEDIC_REACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCED_MEDIC_REACAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_PROC_INTERNO   NUMBER(10),
  CD_MATERIAL           NUMBER(10),
  DS_MENSAGEM           VARCHAR2(255 BYTE),
  NR_SEQ_EXAME          NUMBER(10),
  NR_SEQ_FICHA_TECNICA  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRMEDIN_EXALABO_FK_I ON TASY.PROCED_MEDIC_REACAO
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRMEDIN_MATERIA_FK_I ON TASY.PROCED_MEDIC_REACAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMEDIN_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRMEDIN_MEDFITE_FK_I ON TASY.PROCED_MEDIC_REACAO
(NR_SEQ_FICHA_TECNICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PRMEDIN_PK ON TASY.PROCED_MEDIC_REACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRMEDIN_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRMEDIN_PROINTE_FK_I ON TASY.PROCED_MEDIC_REACAO
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PROCED_MEDIC_REACAO_BEFINSERT
BEFORE INSERT OR UPDATE ON TASY.PROCED_MEDIC_REACAO FOR EACH ROW
BEGIN

if (:new.cd_material is null) and
	(:new.nr_seq_ficha_tecnica is null) then
	--Necessário informar o material ou a ficha técnica do material.
	wheb_mensagem_pck.exibir_mensagem_abort(804057);
end if;

END;
/


ALTER TABLE TASY.PROCED_MEDIC_REACAO ADD (
  CONSTRAINT PRMEDIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCED_MEDIC_REACAO ADD (
  CONSTRAINT PRMEDIN_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT PRMEDIN_MEDFITE_FK 
 FOREIGN KEY (NR_SEQ_FICHA_TECNICA) 
 REFERENCES TASY.MEDIC_FICHA_TECNICA (NR_SEQUENCIA),
  CONSTRAINT PRMEDIN_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PRMEDIN_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROCED_MEDIC_REACAO TO NIVEL_1;

