ALTER TABLE TASY.W_INTERF_UNIMED_CAMP_HO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_INTERF_UNIMED_CAMP_HO CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INTERF_UNIMED_CAMP_HO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_LINHA             NUMBER(8)                NOT NULL,
  NR_SEQ_PROTOCOLO     NUMBER(10)               NOT NULL,
  DS_CAMPOS            VARCHAR2(300 BYTE)       NOT NULL,
  NR_SEQ_GRADE         NUMBER(6)                NOT NULL,
  NR_INTERNO_CONTA     NUMBER(10),
  CD_USUARIO_CONVENIO  VARCHAR2(17 BYTE),
  DT_ENTRADA           DATE,
  QT_ITEM              NUMBER(15,2),
  VL_ITEM              NUMBER(15,2),
  NM_PACIENTE          VARCHAR2(100 BYTE),
  NR_DOC_CONVENIO      VARCHAR2(20 BYTE),
  CD_SERVICO           VARCHAR2(8 BYTE),
  CD_SOLICITANTE       VARCHAR2(20 BYTE),
  IE_TIPO_REGISTRO     VARCHAR2(1 BYTE),
  NR_ATENDIMENTO       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WINUNCAH_PK ON TASY.W_INTERF_UNIMED_CAMP_HO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINUNCAH_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_INTERF_UNIMED_CAMP_HO ADD (
  CONSTRAINT WINUNCAH_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_INTERF_UNIMED_CAMP_HO TO NIVEL_1;

