ALTER TABLE TASY.AG_LISTA_ESP_ESTAGIO_ANT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AG_LISTA_ESP_ESTAGIO_ANT CASCADE CONSTRAINTS;

CREATE TABLE TASY.AG_LISTA_ESP_ESTAGIO_ANT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESTAGIO       NUMBER(10)               NOT NULL,
  NR_SEQ_ESTAGIO_ANT   NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGLESEA_AGLIESE_FK_I ON TASY.AG_LISTA_ESP_ESTAGIO_ANT
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.AGLESEA_AGLIESE_FK2_I ON TASY.AG_LISTA_ESP_ESTAGIO_ANT
(NR_SEQ_ESTAGIO_ANT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGLESEA_PK ON TASY.AG_LISTA_ESP_ESTAGIO_ANT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AG_LISTA_ESP_ESTAGIO_ANT ADD (
  CONSTRAINT AGLESEA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AG_LISTA_ESP_ESTAGIO_ANT ADD (
  CONSTRAINT AGLESEA_AGLIESE_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.AG_LISTA_ESPERA_ESTAGIO (NR_SEQUENCIA),
  CONSTRAINT AGLESEA_AGLIESE_FK2 
 FOREIGN KEY (NR_SEQ_ESTAGIO_ANT) 
 REFERENCES TASY.AG_LISTA_ESPERA_ESTAGIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.AG_LISTA_ESP_ESTAGIO_ANT TO NIVEL_1;

