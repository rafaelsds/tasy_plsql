ALTER TABLE TASY.PACIENTE_ALERGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_ALERGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_ALERGIA
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_SEQ_TIPO                NUMBER(10),
  CD_CLASSE_MAT              NUMBER(10),
  CD_MATERIAL                NUMBER(6),
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  NR_SEQ_REACAO              NUMBER(10),
  IE_INTENSIDADE             VARCHAR2(3 BYTE),
  IE_ABORDAGEM               VARCHAR2(3 BYTE),
  DT_INICIO                  DATE,
  DT_ULTIMA                  DATE,
  NR_ATENDIMENTO             NUMBER(10),
  DT_REGISTRO                DATE               NOT NULL,
  NR_SEQ_DCB                 NUMBER(10),
  IE_CONFIRMACAO             VARCHAR2(1 BYTE)   NOT NULL,
  DT_FIM                     DATE,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  NM_USUARIO_LIBERACAO       VARCHAR2(15 BYTE),
  NR_SEQ_NIVEL_SEG           NUMBER(10),
  NR_SEQ_FICHA_TECNICA       NUMBER(10),
  NR_SEQ_SAEP                NUMBER(10),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  IE_NEGA_ALERGIAS           VARCHAR2(1 BYTE),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  DS_MEDIC_NAO_CAD           VARCHAR2(80 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NM_USUARIO_REVISAO         VARCHAR2(15 BYTE),
  DT_REVISAO                 DATE,
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_ALERTA                  VARCHAR2(1 BYTE),
  CD_SETOR_ATENDIMENTO       NUMBER(5),
  NM_USUARIO_TERMINO         VARCHAR2(15 BYTE),
  CD_SETOR_REVISAO           NUMBER(5),
  DS_JUST_TERMINO            VARCHAR2(255 BYTE),
  NR_SEQ_FAMILIA             NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  IE_LISTA_PROBLEMA          VARCHAR2(1 BYTE),
  NR_SEQ_TRIAGEM             NUMBER(10),
  CD_DOENCA                  VARCHAR2(10 BYTE),
  IE_ESTADO_ATUAL            VARCHAR2(1 BYTE),
  NR_SEQ_HIST_ROTINA         NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  IE_ACAO                    VARCHAR2(1 BYTE),
  DS_RAZAO_DEF_DIAG          VARCHAR2(255 BYTE),
  CD_SINTOMA                 NUMBER(10),
  NR_SEQ_FORMULARIO          NUMBER(10),
  CD_CONTRASTE_ALERGIA       NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACALER_ATCONSPEPA_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_ATEPACI_FK_I ON TASY.PACIENTE_ALERGIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_CIDDOEN_FK_I ON TASY.PACIENTE_ALERGIA
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_CLAMATE_FK_I ON TASY.PACIENTE_ALERGIA
(CD_CLASSE_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_DCBMECO_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_DCB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_DCBMECO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_EHRREEL_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_EHRREGI_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_HISTSAU_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_HIST_ROTINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_I1 ON TASY.PACIENTE_ALERGIA
(DT_REGISTRO, NR_ATENDIMENTO, CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_MATERIA_FK_I ON TASY.PACIENTE_ALERGIA
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_MATFAMI_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_FAMILIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_MEDFITE_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_FICHA_TECNICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_MEDFITE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_MEDREAL_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_REACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_MEDREAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_NISEALE_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_NIVEL_SEG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_NISEALE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_PERFIL_FK_I ON TASY.PACIENTE_ALERGIA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_PESFISI_FK_I ON TASY.PACIENTE_ALERGIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_PESFISI_FK2_I ON TASY.PACIENTE_ALERGIA
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PACALER_PK ON TASY.PACIENTE_ALERGIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_SAEPERO_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_SAEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_SAEPERO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_SETATEN_FK_I ON TASY.PACIENTE_ALERGIA
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_SETATEN_FK2_I ON TASY.PACIENTE_ALERGIA
(CD_SETOR_REVISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_SINPADR_FK_I ON TASY.PACIENTE_ALERGIA
(CD_SINTOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_TASASDI_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_TASASDI_FK2_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACALER_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACALER_TIPALER_FK_I ON TASY.PACIENTE_ALERGIA
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACALER_TYOFCON_FK_I ON TASY.PACIENTE_ALERGIA
(CD_CONTRASTE_ALERGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.paciente_alergia_pend_atual 
after insert or update ON TASY.PACIENTE_ALERGIA 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_liberar_hist_saude_w	varchar2(10); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_liberar_hist_saude) 
into	ie_liberar_hist_saude_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_liberar_hist_saude_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'HSAL'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XHSAL'; 
	end if; 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255)), :new.nr_atendimento, :new.nm_usuario); 
	end if; 
end if; 
	 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ALERGIA_ATUAL
before update or insert ON TASY.PACIENTE_ALERGIA for each row
declare

nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(4);
qt_idade_w		number(10);

Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	((cd_estabelecimento_w = 0) or (cd_estabelecimento	= cd_estabelecimento_w))
	and	ie_evento_disp		= 'PARD'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_situacao,'A') = 'A'
	and	((nr_seq_tipo_alergia is null) or (nr_seq_tipo_alergia = :new.nr_seq_tipo));

begin
cd_estabelecimento_w	:= obter_estabelecimento_ativo;

if	(:new.nr_atendimento is not null) then
	:new.cd_setor_atendimento := obter_setor_atendimento(:new.nr_atendimento);
end if;

if	(nvl(:old.DT_REGISTRO,sysdate+10) <> :new.DT_REGISTRO) and
	(:new.DT_REGISTRO is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_REGISTRO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.nr_seq_tipo is not null) and
	(:new.dt_fim is not null) and
	(:old.dt_fim is null or :old.dt_fim <> :new.dt_fim)
	then
		:new.nm_usuario_termino := wheb_usuario_pck.get_nm_usuario;
end if;

if	(:new.dt_fim is null) and
	(:old.dt_fim is not null)then
	:new.nm_usuario_termino := '';
end if;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) then
	qt_idade_w	:= nvl(obter_idade_pf(:new.cd_pessoa_fisica,sysdate,'A'),0);

	begin
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,:new.cd_pessoa_fisica,null,:new.nm_usuario,null,:new.dt_liberacao,null,:new.nr_seq_tipo);
		end;
	end loop;
	close C01;

	exception
		when others then
		null;
	end;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Smart_alergia_aftinsert
after insert or update ON TASY.PACIENTE_ALERGIA for each row
declare

reg_integracao_w		gerar_int_padrao.reg_integracao;

begin
reg_integracao_w.cd_estab_documento := OBTER_ESTABELECIMENTO_ATIVO;

	if  (:old.DT_LIBERACAO is null) and
		(:new.DT_LIBERACAO is not null) and
		(:new.IE_NEGA_ALERGIAS = 'N') then
		BEGIN
			gerar_int_padrao.gravar_integracao('302', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,'DS_OPERACAO_P=CREATE');
		END;
	elsif  (:old.DT_INATIVACAO is null) and
		   (:new.DT_INATIVACAO is not null)  and
		   (:new.IE_NEGA_ALERGIAS = 'N') then
		BEGIN
			gerar_int_padrao.gravar_integracao('302', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,'DS_OPERACAO_P=DELETE');
		END;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.ISCV_PACIENTE_ALERGIA_AFTINSUP
after update or insert ON TASY.PACIENTE_ALERGIA for each row
declare
ds_sep_bv_w			varchar2(100)	:= obter_separador_bv;
nr_atendimento_w	atendimento_paciente.nr_atendimento%type;

	/* Verifica se existe alguma integra��o cadastrada para o dom�nio 17, para certificar que h� o que ser procurado, para a� ent�o executar todas as verifica��es. */
	function permiteIntegrarISCV
	return boolean is
	qt_resultado_w	number;

	begin
	select	count(*) qt_resultado
	into	qt_resultado_w
	from 	regra_proc_interno_integra
	where	ie_tipo_integracao = 17; --Dominio criado para a integra��o

	return qt_resultado_w > 0;
	end;

	function permiteIntegrar return boolean is	--Verificar se h� algum procedimento interno que deva integrar
	ds_retorno_w		varchar2(1);
	begin

	if	permiteIntegrarISCV() then
		nr_atendimento_w := obter_atendimento_paciente(:new.cd_pessoa_fisica, obter_estabelecimento_ativo);

		select	nvl(max(Obter_Se_Integr_Proc_Interno(pp.nr_seq_proc_interno, 17,null,pp.ie_lado,nvl(pm.cd_estabelecimento,wheb_usuario_pck.get_cd_estabelecimento))),'N') ie_permite_proc_integ
		into	ds_retorno_w
		from	prescr_procedimento pp,
				prescr_medica pm
		where	pm.nr_atendimento = nr_atendimento_w
		and		pp.nr_seq_proc_interno is not null
		and		pp.nr_sequencia = pm.nr_prescricao;

		if	(ds_retorno_w = 'N') then

			select	nvl(max(Obter_Se_Integr_Proc_Interno(nr_seq_proc_interno, 17,null,ie_lado,wheb_usuario_pck.get_cd_estabelecimento)),'N') ie_permite_proc_integ
			into	ds_retorno_w
			from	agenda_paciente
			where	nr_seq_proc_interno is not null
			and		nr_atendimento = nr_atendimento_w;

			if	(ds_retorno_w = 'N') then
				select	nvl(max(Obter_Se_Integr_Proc_Interno(app.nr_seq_proc_interno, 17,null,app.ie_lado,wheb_usuario_pck.get_cd_estabelecimento)),'N') ie_permite_proc_integ
				into	ds_retorno_w
				from	agenda_paciente ap,
						agenda_paciente_proc app
				where	ap.nr_seq_proc_interno is not null
				and		ap.nr_atendimento = nr_atendimento_w
				and		ap.nr_sequencia = app.nr_sequencia;
			end if;
		end if;

		return ds_retorno_w = 'S';
	else
		return false;
	end if;
	end;
begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	if	((:old.nr_seq_tipo <> :new.nr_seq_tipo)
	or	(:old.dt_ultima <> :new.dt_ultima)
	or	(:old.nr_seq_nivel_seg <> :new.nr_seq_nivel_seg)
	or	(:old.nr_seq_reacao <> :new.nr_seq_reacao))
	and	permiteIntegrar() then
		gravar_agend_integracao(746, 'cd_pessoa_fisica=' || :new.cd_pessoa_fisica || ds_sep_bv_w, :new.cd_pessoa_fisica);
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ALERGIA_tp  after update ON TASY.PACIENTE_ALERGIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_DOENCA,1,4000),substr(:new.CD_DOENCA,1,4000),:new.nm_usuario,nr_seq_w,'CD_DOENCA',ie_log_w,ds_w,'PACIENTE_ALERGIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ALERGIA_SBIS_IN
before insert or update ON TASY.PACIENTE_ALERGIA for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  IF (INSERTING) THEN
    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
					   nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                      obter_desc_expressao(656665) ,
                       wheb_usuario_pck.get_nm_maquina,
                         :new.cd_pessoa_fisica,
                         obter_desc_expressao(314079),
                        :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
					   nvl(wheb_usuario_pck.get_nm_usuario, :new.nm_usuario));
  else
    ie_inativacao_w := 'N';

    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';
    end if;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
					   nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
                      wheb_usuario_pck.get_nm_maquina,
                         :new.cd_pessoa_fisica,
                        obter_desc_expressao(307861),
                       :new.nr_atendimento,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
			                :new.nm_usuario);
  end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_PACIENTE_ALERGIA
before insert or update ON TASY.PACIENTE_ALERGIA for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ALERGIA_SBIS_DEL
before delete ON TASY.PACIENTE_ALERGIA for each row
declare
nr_log_seq_w		number(10);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 :old.cd_pessoa_fisica,
									 obter_desc_expressao(314079),
									  :old.nr_atendimento,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));
end;
/


ALTER TABLE TASY.PACIENTE_ALERGIA ADD (
  CONSTRAINT PACALER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_ALERGIA ADD (
  CONSTRAINT PACALER_SINPADR_FK 
 FOREIGN KEY (CD_SINTOMA) 
 REFERENCES TASY.SINTOMA_PADRAO (NR_SEQUENCIA),
  CONSTRAINT PACALER_TYOFCON_FK 
 FOREIGN KEY (CD_CONTRASTE_ALERGIA) 
 REFERENCES TASY.TYPE_OF_CONTRAST (NR_SEQUENCIA),
  CONSTRAINT PACALER_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_REVISAO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PACALER_MATFAMI_FK 
 FOREIGN KEY (NR_SEQ_FAMILIA) 
 REFERENCES TASY.MATERIAL_FAMILIA (NR_SEQUENCIA),
  CONSTRAINT PACALER_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PACALER_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PACALER_HISTSAU_FK 
 FOREIGN KEY (NR_SEQ_HIST_ROTINA) 
 REFERENCES TASY.HISTORICO_SAUDE (NR_SEQUENCIA),
  CONSTRAINT PACALER_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACALER_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PACALER_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PACALER_DCBMECO_FK 
 FOREIGN KEY (NR_SEQ_DCB) 
 REFERENCES TASY.DCB_MEDIC_CONTROLADO (NR_SEQUENCIA),
  CONSTRAINT PACALER_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PACALER_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACALER_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACALER_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA)
    ON DELETE CASCADE,
  CONSTRAINT PACALER_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT PACALER_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MAT) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT PACALER_MEDREAL_FK 
 FOREIGN KEY (NR_SEQ_REACAO) 
 REFERENCES TASY.MED_REACAO_ALERGICA (NR_SEQUENCIA),
  CONSTRAINT PACALER_TIPALER_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_ALERGIA (NR_SEQUENCIA),
  CONSTRAINT PACALER_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT PACALER_NISEALE_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_SEG) 
 REFERENCES TASY.NIVEL_SEGURANCA_ALERTA (NR_SEQUENCIA),
  CONSTRAINT PACALER_MEDFITE_FK 
 FOREIGN KEY (NR_SEQ_FICHA_TECNICA) 
 REFERENCES TASY.MEDIC_FICHA_TECNICA (NR_SEQUENCIA),
  CONSTRAINT PACALER_SAEPERO_FK 
 FOREIGN KEY (NR_SEQ_SAEP) 
 REFERENCES TASY.SAE_PEROPERATORIO (NR_SEQUENCIA),
  CONSTRAINT PACALER_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PACIENTE_ALERGIA TO NIVEL_1;

