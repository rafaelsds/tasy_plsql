ALTER TABLE TASY.AGENDAMENTO_BUILD_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDAMENTO_BUILD_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDAMENTO_BUILD_LOG
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  NR_SEQ_AGENDAMENTO_BUILD  NUMBER(10),
  IE_ACAO                   VARCHAR2(2 BYTE),
  DT_AGENDAMENTO            DATE,
  IE_APLICACAO              VARCHAR2(2 BYTE),
  CD_VERSAO                 VARCHAR2(15 BYTE),
  IE_PERIODO                VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGENBUIDLO_AGENDBU_FK_I ON TASY.AGENDAMENTO_BUILD_LOG
(NR_SEQ_AGENDAMENTO_BUILD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGENBUIDLO_PK ON TASY.AGENDAMENTO_BUILD_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGENDAMENTO_BUILD_LOG ADD (
  CONSTRAINT AGENBUIDLO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.AGENDAMENTO_BUILD_LOG ADD (
  CONSTRAINT AGENBUIDLO_AGENDBU_FK 
 FOREIGN KEY (NR_SEQ_AGENDAMENTO_BUILD) 
 REFERENCES TASY.AGENDAMENTO_BUILD (NR_SEQUENCIA));

GRANT SELECT ON TASY.AGENDAMENTO_BUILD_LOG TO NIVEL_1;

