ALTER TABLE TASY.REGISTRO_CANCER_RAZAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGISTRO_CANCER_RAZAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGISTRO_CANCER_RAZAO
(
  NR_SEQUENCIA     NUMBER(10)                   NOT NULL,
  DT_ATUALIZACAO   DATE                         NOT NULL,
  NM_USUARIO       VARCHAR2(15 BYTE)            NOT NULL,
  IE_RAZAO         VARCHAR2(2 BYTE)             NOT NULL,
  DS_RAZAO         VARCHAR2(1000 BYTE),
  NR_SEQ_REGISTRO  NUMBER(10)                   NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REGCANRA_PK ON TASY.REGISTRO_CANCER_RAZAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCANRA_REGCAN_FK_I ON TASY.REGISTRO_CANCER_RAZAO
(NR_SEQ_REGISTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGISTRO_CANCER_RAZAO ADD (
  CONSTRAINT REGCANRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REGISTRO_CANCER_RAZAO ADD (
  CONSTRAINT REGCANRA_REGCAN_FK 
 FOREIGN KEY (NR_SEQ_REGISTRO) 
 REFERENCES TASY.REGISTRO_CANCER (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGISTRO_CANCER_RAZAO TO NIVEL_1;

