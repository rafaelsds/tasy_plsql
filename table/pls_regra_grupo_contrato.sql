ALTER TABLE TASY.PLS_REGRA_GRUPO_CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_GRUPO_CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_GRUPO_CONTRATO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_GRUPO_CONTRATO     NUMBER(10)          NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  IE_CONTRATO               VARCHAR2(10 BYTE)   NOT NULL,
  CD_ESTABELECIMENTO        NUMBER(4)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  IE_ABRANGENCIA            VARCHAR2(3 BYTE),
  IE_PRECO                  VARCHAR2(2 BYTE),
  IE_TIPO_CONTRATO          VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIF_SCA        NUMBER(10),
  IE_TIPO_INTERCAMBIO       VARCHAR2(2 BYTE),
  NR_SEQ_GRUPO_INTERCAMBIO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSREGC_ESTABEL_FK_I ON TASY.PLS_REGRA_GRUPO_CONTRATO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREGC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSREGC_PK ON TASY.PLS_REGRA_GRUPO_CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREGC_PLSPRGC_FK_I ON TASY.PLS_REGRA_GRUPO_CONTRATO
(NR_SEQ_GRUPO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREGC_PLSPRGC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREGC_PLSRGIN_FK_I ON TASY.PLS_REGRA_GRUPO_CONTRATO
(NR_SEQ_GRUPO_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_REGRA_GRUPO_CONTRATO ADD (
  CONSTRAINT PLSREGC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_GRUPO_CONTRATO ADD (
  CONSTRAINT PLSREGC_PLSRGIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_INTERCAMBIO) 
 REFERENCES TASY.PLS_REGRA_GRUPO_INTER (NR_SEQUENCIA),
  CONSTRAINT PLSREGC_PLSPRGC_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_CONTRATO) 
 REFERENCES TASY.PLS_PRECO_GRUPO_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSREGC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_REGRA_GRUPO_CONTRATO TO NIVEL_1;

