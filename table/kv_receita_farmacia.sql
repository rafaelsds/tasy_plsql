ALTER TABLE TASY.KV_RECEITA_FARMACIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.KV_RECEITA_FARMACIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.KV_RECEITA_FARMACIA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_RECEITA             DATE                   NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      DEFAULT null,
  NR_ATENDIMENTO         NUMBER(10)             DEFAULT null,
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  IE_SOSTIGE             VARCHAR2(1 BYTE),
  IE_TAXA                VARCHAR2(1 BYTE),
  IE_ACIDENTE            VARCHAR2(1 BYTE),
  IE_BVG                 VARCHAR2(1 BYTE),
  IE_MATERIAL_PRESCR     VARCHAR2(1 BYTE),
  IE_VACCINE             VARCHAR2(1 BYTE),
  DS_ACIDENT             DATE,
  DS_ACIDENTE_NUMBER     VARCHAR2(255 BYTE),
  DS_UTC_ATUALIZACAO     VARCHAR2(50 BYTE),
  DS_UTC                 VARCHAR2(50 BYTE),
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_MEDIC_NOTURNO       VARCHAR2(1 BYTE)       DEFAULT null,
  CD_MEDICO              VARCHAR2(10 BYTE),
  NR_RQE                 VARCHAR2(20 BYTE),
  NR_BSNR                VARCHAR2(25 BYTE),
  CD_DEPARTAMENTO        NUMBER(6)              DEFAULT null,
  CD_ESTABELECIMENTO     NUMBER(4)              DEFAULT null,
  IE_MAT_CONSULTA        VARCHAR2(1 BYTE)       DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.KVREFAR_ATEPACI_FK_I ON TASY.KV_RECEITA_FARMACIA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KVREFAR_DEPMED_FK_I ON TASY.KV_RECEITA_FARMACIA
(CD_DEPARTAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KVREFAR_ESTABEL_FK_I ON TASY.KV_RECEITA_FARMACIA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KVREFAR_PESFISI_FK_I ON TASY.KV_RECEITA_FARMACIA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.KVREFAR_PK ON TASY.KV_RECEITA_FARMACIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.KVREFAR_PROFISS_FK_I ON TASY.KV_RECEITA_FARMACIA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.KV_RECEITA_FARMACIA_UPDATE
before update ON TASY.KV_RECEITA_FARMACIA for each row
declare
nr_seq_regra_w	wl_regra_item.nr_sequencia%type;
qt_tempo_document_w 		wl_regra_item.qt_tempo_normal%type;
is_rule_exists_w varchar2(1) := 'N';

begin
	if (:new.dt_liberacao is not null and :old.dt_liberacao is null) then

     select	decode (count(*),0,'N','S')
    into is_rule_exists_w
    from 	wl_regra_worklist a,
		wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   b.IE_TIPO_PEND_EMISSAO  = 'KV16'
    and   obter_se_wl_liberado(wheb_usuario_pck.get_cd_perfil,wheb_usuario_pck.get_nm_usuario,a.nr_sequencia) = 'S'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');
    if (is_rule_exists_w = 'S') then
    select	nvl(b.qt_tempo_normal,0),
		nvl(b.nr_sequencia, 0)
    into qt_tempo_document_w,
			nr_seq_regra_w
    from 	wl_regra_worklist a,
		wl_regra_item b
    where	a.nr_sequencia = b.nr_seq_regra
    and		b.ie_situacao = 'A'
    and   b.IE_TIPO_PEND_EMISSAO  = 'KV16'
    and		a.nr_seq_item = (	select	max(x.nr_sequencia)
							from	wl_item x
							where	x.nr_sequencia = a.nr_seq_item
							and		x.cd_categoria = 'DI'
							and		x.ie_situacao = 'A');


		wl_gerar_finalizar_tarefa('DI','I',:new.nr_atendimento,:new.cd_pessoa_fisica,wheb_usuario_pck.get_nm_usuario,sysdate+(qt_tempo_document_w/24),
                              'N',null,null,null,null,null,null,null,null,null,nr_seq_regra_w,
                              null,null,null,null,null,null,null,sysdate,null,null,:new.cd_medico);
	end if;
  end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.KV_RECEITA_FARMACIA_ATUAL
before insert or update ON TASY.KV_RECEITA_FARMACIA for each row
declare
qt_reg_w	number(10);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(nvl(:old.DT_RECEITA,sysdate+10) <> :new.DT_RECEITA) and
	(:new.DT_RECEITA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_RECEITA, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;


<<Final>>
qt_reg_w	:= 0;

end;
/


ALTER TABLE TASY.KV_RECEITA_FARMACIA ADD (
  CONSTRAINT KVREFAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.KV_RECEITA_FARMACIA ADD (
  CONSTRAINT KVREFAR_PROFISS_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT KVREFAR_DEPMED_FK 
 FOREIGN KEY (CD_DEPARTAMENTO) 
 REFERENCES TASY.DEPARTAMENTO_MEDICO (CD_DEPARTAMENTO),
  CONSTRAINT KVREFAR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT KVREFAR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT KVREFAR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.KV_RECEITA_FARMACIA TO NIVEL_1;

