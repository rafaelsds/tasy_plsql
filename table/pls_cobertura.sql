ALTER TABLE TASY.PLS_COBERTURA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_COBERTURA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_COBERTURA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_PLANO           NUMBER(10),
  NR_SEQ_CONTRATO        NUMBER(10),
  NR_SEQ_TIPO_COBERTURA  NUMBER(10)             NOT NULL,
  NR_SEQ_SCA             NUMBER(10),
  IE_TIPO_ATENDIMENTO    VARCHAR2(2 BYTE),
  IE_SEXO                VARCHAR2(2 BYTE),
  DT_INICIO_VIGENCIA     DATE,
  DT_FIM_VIGENCIA        DATE,
  NR_SEQ_PLANO_CONTRATO  NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCOBER_PK ON TASY.PLS_COBERTURA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOBER_PLSCONT_FK_I ON TASY.PLS_COBERTURA
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOBER_PLSPLAN_FK_I ON TASY.PLS_COBERTURA
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOBER_PLSPLAN_FK2_I ON TASY.PLS_COBERTURA
(NR_SEQ_PLANO_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCOBER_PLSSCOA_FK_I ON TASY.PLS_COBERTURA
(NR_SEQ_SCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOBER_PLSSCOA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSCOBER_PLSTICO_FK_I ON TASY.PLS_COBERTURA
(NR_SEQ_TIPO_COBERTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSCOBER_PLSTICO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_COBERTURA ADD (
  CONSTRAINT PLSCOBER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_COBERTURA ADD (
  CONSTRAINT PLSCOBER_PLSPLAN_FK2 
 FOREIGN KEY (NR_SEQ_PLANO_CONTRATO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSCOBER_PLSSCOA_FK 
 FOREIGN KEY (NR_SEQ_SCA) 
 REFERENCES TASY.PLS_SERVICO_COBERTURA_ADIC (NR_SEQUENCIA),
  CONSTRAINT PLSCOBER_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSCOBER_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSCOBER_PLSTICO_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COBERTURA) 
 REFERENCES TASY.PLS_TIPO_COBERTURA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_COBERTURA TO NIVEL_1;

