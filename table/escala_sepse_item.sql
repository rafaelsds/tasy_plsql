ALTER TABLE TASY.ESCALA_SEPSE_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_SEPSE_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_SEPSE_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ESCALA        NUMBER(10)               NOT NULL,
  NR_SEQ_ATRIBUTO      NUMBER(10)               NOT NULL,
  QT_PONTUACAO         NUMBER(10)               NOT NULL,
  IE_ATRIBUTO          VARCHAR2(60 BYTE),
  VL_ATRIBUTO          NUMBER(15,4),
  IE_RESULTADO         VARCHAR2(1 BYTE),
  IE_RESULTADO_ORIG    VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCSEPI_ESCSEPS_FK_I ON TASY.ESCALA_SEPSE_ITEM
(NR_SEQ_ESCALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCSEPI_PK ON TASY.ESCALA_SEPSE_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCSEPI_SEPSATR_FK_I ON TASY.ESCALA_SEPSE_ITEM
(NR_SEQ_ATRIBUTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_sepse_item_update
before update ON TASY.ESCALA_SEPSE_ITEM for each row
DISABLE
begin

if (nvl(:new.ie_resultado,'N') = 'S') then
	:new.qt_pontuacao := 1;
else
	:new.qt_pontuacao := 0;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.hsj_escala_sepse_itens_aftins
BEFORE INSERT ON TASY.ESCALA_SEPSE_ITEM FOR EACH ROW
declare

nr_atendimento_w number(10);
nr_seq_origem_w number(10);
ie_padrao_variavel_w varchar2(50);
qt_anos_paciente_w  number(10);     
qt_reg_w number(10);

QT_RETORNO_W number(10);
IE_REGRA_W VARCHAR2(255) ;
QT_PONTUACAO_W VARCHAR2(255);
IE_ATRIBUTO_W  VARCHAR2(255);

IE_RESULTADO_W varchar2(10);

begin

    /*
            Para gera��o manual de escala de SEPSE, quando for considerada pediatrica dever� gerar apenas os deflagradores de sepse pedi�trica
    */
  
    select  nr_atendimento,
            nr_seq_origem,
            ie_padrao_variavel,
            to_number(obter_idade_pf(obter_pessoa_atendimento(nr_atendimento, 'C'), sysdate, 'A'))anos_paciente
    into    nr_atendimento_w,
            nr_seq_origem_w,
            ie_padrao_variavel_w,
            qt_anos_paciente_w
    from hsj_escala_sepse_atend
    where nr_seq_escala = :new.nr_seq_escala;
   
    if nr_seq_origem_w is null and ie_padrao_variavel_w = 'P' then
        
        if qt_anos_paciente_w <= 13 then
            select count(*)
            into qt_reg_w
            from SEPSE_ATRIBUTO a
            join hsj_sepse_atributo_idade_ped b on(a.nr_sequencia = nvl(b.nr_seq_atributo_sup, b.nr_seq_atributo))
            where nvl(b.nr_seq_atributo_sup, b.nr_seq_atributo) = :new.nr_seq_atributo;
            
            if qt_reg_w = 0 then
             
               hsj_gerar_log('Escala de SEPSE Pedi�trica gerada com sucesso!');
               
            end if;
        
        elsif qt_anos_paciente_w >= 14 then    
            
            hsj_obter_dados_escala_sepse2(nr_atendimento_w, :new.nr_seq_atributo, QT_RETORNO_W, IE_REGRA_W, QT_PONTUACAO_W, :new.nm_usuario, IE_ATRIBUTO_W);
           
            if nvl(QT_PONTUACAO_W, 0) > 0 then
                IE_RESULTADO_W:='S';
            else
                IE_RESULTADO_W:='N';
                
          --      hsj_gerar_log(:new.nr_seq_atributo||'-'||obter_desc_atributo_sepse(:new.nr_seq_atributo)||' ='||QT_PONTUACAO_W);
            end if;
        
            :new.qt_pontuacao := nvl(QT_PONTUACAO_W,0);
            :new.vl_atributo := nvl(QT_RETORNO_W,0);
            :new.ie_resultado := IE_RESULTADO_W;
        
        end if;  
    end if;

end hsj_escala_sepse_itens_aftins;
/


ALTER TABLE TASY.ESCALA_SEPSE_ITEM ADD (
  CONSTRAINT ESCSEPI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_SEPSE_ITEM ADD (
  CONSTRAINT ESCSEPI_ESCSEPS_FK 
 FOREIGN KEY (NR_SEQ_ESCALA) 
 REFERENCES TASY.ESCALA_SEPSE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCSEPI_SEPSATR_FK 
 FOREIGN KEY (NR_SEQ_ATRIBUTO) 
 REFERENCES TASY.SEPSE_ATRIBUTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_SEPSE_ITEM TO NIVEL_1;

