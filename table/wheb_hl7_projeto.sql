ALTER TABLE TASY.WHEB_HL7_PROJETO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.WHEB_HL7_PROJETO CASCADE CONSTRAINTS;

CREATE TABLE TASY.WHEB_HL7_PROJETO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  IE_VERSAO_OFICIAL      VARCHAR2(1 BYTE)       NOT NULL,
  DS_PROJETO             VARCHAR2(255 BYTE)     NOT NULL,
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  CD_VERSAO              NUMBER(10),
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NR_SEQ_SISTEMA         NUMBER(10),
  NR_SEQ_VERSAO_OFICIAL  NUMBER(10),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WHL7PRO_PK ON TASY.WHEB_HL7_PROJETO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WHL7PRO_UK ON TASY.WHEB_HL7_PROJETO
(NR_SEQ_SISTEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.WHEB_HL7_PROJETO ADD (
  CONSTRAINT WHL7PRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT WHL7PRO_UK
 UNIQUE (NR_SEQ_SISTEMA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.WHEB_HL7_PROJETO ADD (
  CONSTRAINT WHL7PRO_SISINTE_FK 
 FOREIGN KEY (NR_SEQ_SISTEMA) 
 REFERENCES TASY.SISTEMA_INTEGRACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.WHEB_HL7_PROJETO TO NIVEL_1;

