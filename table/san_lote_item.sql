ALTER TABLE TASY.SAN_LOTE_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_LOTE_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_LOTE_ITEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  NR_SEQ_LOTE             NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_TRANS            NUMBER(10),
  NR_SEQ_PRODUCAO         NUMBER(10),
  NR_SEQ_EMP              NUMBER(10),
  NR_SEQ_INUTIL           NUMBER(10),
  DT_DESTINO              DATE,
  CD_AGENCIA              NUMBER(10),
  CD_DESTINO              NUMBER(10),
  CD_PESSOA_MAE           VARCHAR2(10 BYTE),
  NM_MAE                  VARCHAR2(60 BYTE),
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE),
  IE_SEXO                 VARCHAR2(1 BYTE),
  NM_PESSOA_FISICA        VARCHAR2(60 BYTE),
  DT_NASCIMENTO           DATE,
  NR_CPF                  VARCHAR2(11 BYTE),
  NR_CARTAO_NAC_SUS       VARCHAR2(20 BYTE),
  IE_TIPO_FATOR_SANGUE    VARCHAR2(3 BYTE),
  IE_HOUVE_REACAO         VARCHAR2(1 BYTE),
  CD_TIPO_REACAO          VARCHAR2(60 BYTE),
  IE_HOUVE_PAI_POSITIVO   VARCHAR2(1 BYTE),
  DS_LISTA_ANTICORPOS     VARCHAR2(100 BYTE),
  IE_TIPO_CONVENIO        NUMBER(2),
  CD_MOTIVO_INUTILIZACAO  VARCHAR2(40 BYTE),
  CD_ENTIDADE             VARCHAR2(40 BYTE),
  UF_ENTIDADE             VARCHAR2(2 BYTE),
  DS_ENTIDADE             VARCHAR2(100 BYTE),
  IE_EXSANGUINEO          VARCHAR2(1 BYTE),
  IE_ALIQUOTADO           VARCHAR2(1 BYTE),
  IE_PEDIATRICO           VARCHAR2(1 BYTE),
  IE_POOL                 VARCHAR2(1 BYTE),
  NR_SEC_SAUDE            VARCHAR2(20 BYTE),
  CD_SEQ_DERIVADO         VARCHAR2(40 BYTE),
  IE_LAVADO               VARCHAR2(1 BYTE),
  IE_FILTRADO             VARCHAR2(1 BYTE),
  IE_IRRADIADO            VARCHAR2(1 BYTE),
  IE_REJUVENESCIDO        VARCHAR2(1 BYTE),
  DS_MENSAGEM_RETORNO     VARCHAR2(4000 BYTE),
  CD_RECEPTOR_EXTERNO     VARCHAR2(40 BYTE),
  CD_ID_TOKEN             VARCHAR2(60 BYTE),
  DT_GRAVACAO_TOKEN       DATE,
  QT_EXPIRAR_TOKEN        NUMBER(10),
  IE_STATUS_ENVIO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SANLOTI_PK ON TASY.SAN_LOTE_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANLOTI_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANLOTI_SANLOTI_FK_I ON TASY.SAN_LOTE_ITEM
(NR_SEQ_LOTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANLOTI_SANLOTI_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SAN_LOTE_ITEM ADD (
  CONSTRAINT SANLOTI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_LOTE_ITEM ADD (
  CONSTRAINT SANLOTI_SANLOTI_FK 
 FOREIGN KEY (NR_SEQ_LOTE) 
 REFERENCES TASY.SAN_LOTE_INTEGRACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_LOTE_ITEM TO NIVEL_1;

