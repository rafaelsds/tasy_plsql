ALTER TABLE TASY.ATENDIMENTO_PERDA_GANHO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_PERDA_GANHO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_PERDA_GANHO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_SEQ_TIPO                NUMBER(10)         NOT NULL,
  QT_VOLUME                  NUMBER(15,4)       NOT NULL,
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  DT_MEDIDA                  DATE               NOT NULL,
  QT_PERDA_ACUM              NUMBER(15,4),
  QT_GANHO_ACUM              NUMBER(15,4),
  QT_DIFERENCA               NUMBER(15,4),
  CD_TURNO                   VARCHAR2(11 BYTE)  NOT NULL,
  CD_SETOR_ATENDIMENTO       NUMBER(5)          NOT NULL,
  NR_CIRURGIA                NUMBER(10),
  IE_ORIGEM                  VARCHAR2(1 BYTE),
  DT_REFERENCIA              DATE,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE),
  NR_HORA                    NUMBER(2),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  DT_LIBERACAO               DATE,
  DT_APAP                    DATE,
  QT_OCORRENCIA              NUMBER(10),
  NR_SEQ_EVENTO_ADEP         NUMBER(15),
  NR_SEQ_PEPO                NUMBER(10),
  CD_PERFIL_ATIVO            NUMBER(5),
  IE_RECEM_NATO              VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  QT_PESO                    NUMBER(10),
  NR_SEQ_EVENTO_IVC          NUMBER(15),
  NR_SEQ_TOPOGRAFIA          NUMBER(10),
  IE_LADO                    VARCHAR2(1 BYTE),
  NR_SEQ_ASPECTO             NUMBER(10),
  IE_INTENSIDADE             VARCHAR2(15 BYTE),
  NR_SEQ_HORARIO             NUMBER(10),
  DS_UTC                     VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO           VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO         VARCHAR2(50 BYTE),
  NR_SEQ_EVENTO_HD           NUMBER(10)         DEFAULT null,
  NR_SEQ_DISPOSITIVO         NUMBER(10),
  NR_SEQ_ATEND_DISP          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPEGA_ASPEEVA_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_ASPECTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPEGA_ASPEEVA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPEGA_ATEPACI_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPEGA_ATEPADI_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_ATEND_DISP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPEGA_CIRURGI_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPEGA_DISPOSI_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_DISPOSITIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPEGA_HDPREVE_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_EVENTO_HD)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPEGA_I1 ON TASY.ATENDIMENTO_PERDA_GANHO
(DT_MEDIDA, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPEGA_I2 ON TASY.ATENDIMENTO_PERDA_GANHO
(DT_MEDIDA, NR_CIRURGIA, NR_SEQ_PEPO, DT_INATIVACAO, IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPEGA_PEPOCIR_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPEGA_PERFIL_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPEGA_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPEGA_PESFISI_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEPEGA_PK ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEPEGA_PRSOLEV_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_EVENTO_ADEP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPEGA_PRSOLEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPEGA_SETATEN_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPEGA_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPEGA_TASASDI_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPEGA_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPEGA_TASASDI_FK2_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPEGA_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPEGA_TIPPEGA_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPEGA_TIPPEGA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEPEGA_TOPDOR_FK_I ON TASY.ATENDIMENTO_PERDA_GANHO
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEPEGA_TOPDOR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Atendimento_perda_ganho_insert
BEFORE INSERT ON TASY.ATENDIMENTO_PERDA_GANHO FOR EACH ROW
DECLARE

cd_turno_w			Varchar2(11) := 'a';
cd_turno_ww			Varchar2(11);
cd_setor_atendimento_w		Number(05,0);
cd_estabelecimento_w		Number(05,0);
dt_inicial_w			Date;
dt_final_w			Date;
dt_atual_w			Date;
dt_entrada_w			Date;
hr_balanco_w			Date;
ie_volume_ocorrencia_w		varchar2(1);
qt_conv_peso_volume_w		number(15,4);
qt_hora_retroativa_w		Number(15,4);
qt_hora_retroativa_desc_w	Varchar2(25);

ds_hora_w			varchar2(20);
dt_registro_w			date;
dt_apap_w			date;
qt_hora_w			number(15,2);
nr_medida_w			number(10);
ie_regra_apap_ganho_perda_w	varchar2(10);

cursor C01 is
	Select	to_date('01/01/1999' || ' ' || to_char(dt_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') dt_inicial,
		to_date('01/01/1999' || ' ' || to_char(dt_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') dt_final,
		to_date('01/01/1999' || ' ' || to_char(:new.dt_medida,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') dt_atual,
		cd_turno
	from	regra_turno_gp
	where	cd_estabelecimento	= cd_estabelecimento_w
	and		(cd_setor_atendimento is null or cd_setor_atendimento = cd_setor_atendimento_w)
	order by nvl(cd_setor_atendimento,0);

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'N') then
    goto final;
end if;

select	dt_entrada
into	dt_entrada_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

if	(trunc(dt_entrada_w, 'MI') > trunc(:new.dt_medida, 'MI')) or
	(trunc(sysdate,'MI') < trunc(:new.dt_medida,'MI')) then
	-- A data da medida deve estar entre a data de inicio do atendimento e a data atual
	Wheb_mensagem_pck.exibir_mensagem_abort(263485);
end if;

qt_hora_retroativa_w		:= Obter_Valor_Param_Usuario(281,1069,obter_Perfil_Ativo,:new.nm_usuario,0);
if	(qt_hora_retroativa_w is not null) and
	(:new.dt_medida < (sysdate - qt_hora_retroativa_w/24)) then
	-- A data da medida nao pode ser menor que '|| to_char(qt_hora_retroativa_w)||' hora(s) em relacao a data atual.#@#@
	qt_hora_retroativa_desc_w := to_char(qt_hora_retroativa_w);
	Wheb_mensagem_pck.exibir_mensagem_abort(263486,'QT_HORA_RETROATIVA_W=' || qt_hora_retroativa_desc_w);
end if;

select	max(a.ie_volume_ocorrencia),
	max(a.qt_conv_peso_volume)
into	ie_volume_ocorrencia_w,
	qt_conv_peso_volume_w
from	tipo_perda_ganho a
where	a.nr_sequencia = :new.nr_seq_tipo;

if	(ie_volume_ocorrencia_w = 'P') and
	(qt_conv_peso_volume_w > 0) and
	(:new.qt_peso > 0) then
	begin
	:new.qt_volume	:= :new.qt_volume + (:new.qt_peso * qt_conv_peso_volume_w);
	end;
end if;

select	cd_estabelecimento,
	Obter_Setor_Atendimento(:new.nr_atendimento)
into	cd_estabelecimento_w,
	cd_setor_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;

OPEN  C01;
LOOP
FETCH C01 into	dt_inicial_w,
		dt_final_w,
		dt_atual_w,
		cd_turno_ww;
EXIT when C01%notfound;

	if	((dt_atual_w >= dt_inicial_w) and (dt_atual_w <= dt_final_w)) or
		((dt_final_w < dt_inicial_w) and
		 ((dt_atual_w >= dt_inicial_w) or (dt_atual_w <= dt_final_w))) then
		cd_turno_w := cd_turno_ww;
	end if;

END LOOP;
CLOSE C01;

if	(cd_turno_w = 'a') then
	cd_turno_w := cd_turno_ww;
end if;

:new.cd_turno			:= cd_turno_w;
:new.cd_setor_atendimento	:= cd_setor_atendimento_w;

if	(:new.cd_turno	is null) then
    -- 'Nao ha turno definido para este horario. '||chr(13)||'Favor verificar o cadastro Ganhos e perdas - regra turno, no Shift + F11. #@#@'
	Wheb_mensagem_pck.exibir_mensagem_abort(263487);
end if;

/* Rafael em 15/9/7 OS68536 */
select	to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || nvl(max(to_char(hr_inicio_balanco,'hh24:mi:ss')),'00:00:00'),'dd/mm/yyyy hh24:mi:ss')
into	hr_balanco_w
from	parametro_medico
where	cd_estabelecimento = cd_estabelecimento_w;

if	(to_char(:new.dt_medida,'hh24:mi') < to_char(hr_balanco_w,'hh24:mi')) then
	:new.dt_referencia := :new.dt_medida - 1;
else
	:new.dt_referencia := :new.dt_medida;
end if;

if	(:new.nr_hora is null) or
	(:new.dt_medida <> :old.dt_medida) then
	begin

	:new.nr_hora	:= Obter_Hora_Apap_GP(:new.dt_medida);
	end;
end if;

select	nvl(max(ie_regra_apap_ganho_perda),'R')
into	ie_regra_apap_ganho_perda_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;


if	(ie_regra_apap_ganho_perda_w	= 'R') then

	nr_medida_w	:= to_number(to_char(round(:new.dt_medida,'hh24'),'hh24'));

else
	nr_medida_w	:= to_number(to_char(trunc(:new.dt_medida,'hh24'),'hh24'));

end if;

if	(:new.nr_hora not between nr_medida_w - 1 and nr_medida_w + 1) then
	--A hora informada para APAP nao condiz com a data da medida.#@#@
	Wheb_mensagem_pck.exibir_mensagem_abort(263489);
end if;

if	(:new.nr_hora is not null) and
	((:old.nr_hora is null) or
	 (:old.dt_medida is null) or
	(:new.nr_hora <> :old.nr_hora) or
	 (:new.dt_medida <> :old.dt_medida)) then
	begin
	ds_hora_w	:= substr(obter_valor_dominio(2119,:new.nr_hora),1,2);
	dt_registro_w	:= trunc(:new.dt_medida,'hh24');
	dt_apap_w	:= to_date(to_char(:new.dt_medida,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss');
	if	(to_char(round(:new.dt_medida,'hh24'),'hh24') = ds_hora_w) then
		:new.dt_apap	:= round(:new.dt_medida,'hh24');
	else
		begin
		qt_hora_w	:= (trunc(:new.dt_medida,'hh24') - to_date(to_char(:new.dt_medida,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss')) * 24;
		if	(qt_hora_w > 12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida + 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w > 0) and
			(qt_hora_w <= 12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w >= -12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w < -12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida - 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		end if;
		end;
	end if;
	end;
end if;

<<final>>
null;

END;
/


CREATE OR REPLACE TRIGGER TASY.Atendimento_perda_ganho_update
BEFORE UPDATE ON TASY.ATENDIMENTO_PERDA_GANHO FOR EACH ROW
DECLARE

cd_turno_w			Varchar2(11) := 'a';
cd_turno_ww			Varchar2(11);
cd_setor_atendimento_w		Number(05,0);
cd_estabelecimento_w		Number(05,0);
dt_inicial_w			Date;
dt_final_w			Date;
dt_atual_w			Date;
dt_entrada_w			Date;
ie_volume_ocorrencia_w		varchar2(1);
qt_conv_peso_volume_w		number(15,4);
qt_hora_retroativa_w		Number(15,4);

ds_hora_w			varchar2(20);
dt_registro_w			date;
dt_apap_w			date;
qt_hora_w			number(15,2);
qt_reg_w	number(1);
nr_medida_w			number(10);
ie_regra_apap_ganho_perda_w	varchar2(10);
ie_perda_ganho_dt_evento_w	varchar2(1);

cursor C01 is
	Select	to_date('01/01/1999' || ' ' || to_char(dt_inicial,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') dt_inicial,
		to_date('01/01/1999' || ' ' || to_char(dt_final,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') dt_final,
		to_date('01/01/1999' || ' ' || to_char(:new.dt_medida,'hh24:mi:ss'),'dd/mm/yyyy hh24:mi:ss') dt_atual,
		cd_turno
	from	regra_turno_gp
	where	cd_estabelecimento	= cd_estabelecimento_w
	and		(cd_setor_atendimento is null or cd_setor_atendimento = cd_setor_atendimento_w)
	order by nvl(cd_setor_atendimento,0);


BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if(:new.dt_liberacao is null) then
	Obter_Param_Usuario(1113, 690, obter_perfil_ativo, :new.nm_usuario, nvl(wheb_usuario_pck.get_cd_estabelecimento, 1), ie_perda_ganho_dt_evento_w);
end if;

select	dt_entrada
into	dt_entrada_w
from	atendimento_paciente
where	nr_atendimento = :new.nr_atendimento;

if	((dt_entrada_w > :new.dt_medida) or
	(sysdate < :new.dt_medida)) and
	(wheb_usuario_pck.get_cd_funcao <> 88) and
	(ie_perda_ganho_dt_evento_w <> 'S')	then
	--'A data da medida deve estar entre a data de inicio do atendimento e a data atual');
	Wheb_mensagem_pck.exibir_mensagem_abort(236035);
end if;

if	(:old.dt_liberacao is null) then
	qt_hora_retroativa_w		:= Obter_Valor_Param_Usuario(281,1069,obter_Perfil_Ativo,:new.nm_usuario,0);
	if	(qt_hora_retroativa_w is not null) and
		(:new.dt_medida < (sysdate - qt_hora_retroativa_w/24)) then
		--'A data da medida nao pode ser menor que '|| to_char(qt_hora_retroativa_w)||' hora(s) em relacao a data atual.#@#@');
		Wheb_mensagem_pck.exibir_mensagem_abort(236036,'QT_HORAS='||qt_hora_retroativa_w);
	end if;
end if;

select	max(a.ie_volume_ocorrencia),
	max(a.qt_conv_peso_volume)
into	ie_volume_ocorrencia_w,
	qt_conv_peso_volume_w
from	tipo_perda_ganho a
where	a.nr_sequencia = :new.nr_seq_tipo;

if	(:new.dt_liberacao is null) and
	(ie_volume_ocorrencia_w = 'P') and
	(qt_conv_peso_volume_w > 0) and
	(:new.qt_peso > 0) then
	begin
	:new.qt_volume	:= :new.qt_volume + (:new.qt_peso * qt_conv_peso_volume_w);
	end;
end if;



select	cd_estabelecimento,
	Obter_Setor_Atendimento(:new.nr_atendimento)
into	cd_estabelecimento_w,
	cd_setor_atendimento_w
from	atendimento_paciente
where	nr_atendimento	= :new.nr_atendimento;

OPEN  C01;
LOOP
FETCH C01 into	dt_inicial_w,
		dt_final_w,
		dt_atual_w,
		cd_turno_ww;
EXIT when C01%notfound;

	if	((dt_atual_w >= dt_inicial_w) and (dt_atual_w <= dt_final_w)) or
		((dt_final_w < dt_inicial_w) and
		 ((dt_atual_w >= dt_inicial_w) or (dt_atual_w <= dt_final_w))) then
		cd_turno_w := cd_turno_ww;
	end if;

END LOOP;
CLOSE C01;

if	(cd_turno_w = 'a') then
	cd_turno_w := cd_turno_ww;
end if;

:new.cd_turno			:= cd_turno_w;
:new.cd_setor_atendimento	:= cd_setor_atendimento_w;

if	(:new.nr_hora is null) or
	(:new.dt_medida <> :old.dt_medida) then
	begin
	:new.nr_hora	:= Obter_Hora_Apap_GP(:new.dt_medida);
	end;
end if;

select	nvl(max(ie_regra_apap_ganho_perda),'R')
into	ie_regra_apap_ganho_perda_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;


if	(ie_regra_apap_ganho_perda_w	= 'R') then

	nr_medida_w	:= to_number(to_char(round(:new.dt_medida,'hh24'),'hh24'));

else
	nr_medida_w	:= to_number(to_char(trunc(:new.dt_medida,'hh24'),'hh24'));

end if;


if	(:new.nr_hora not between nr_medida_w - 1 and nr_medida_w + 1) then
	--A hora informada para APAP nao condiz com a data da medida.#@#@
	Wheb_mensagem_pck.exibir_mensagem_abort(263489);
end if;

if	(:new.nr_hora is not null) and
	((:old.nr_hora is null) or
	 (:old.dt_medida is null) or
	(:new.nr_hora <> :old.nr_hora) or
	 (:new.dt_medida <> :old.dt_medida)) then
	begin
	ds_hora_w	:= substr(obter_valor_dominio(2119,:new.nr_hora),1,2);
	dt_registro_w	:= trunc(:new.dt_medida,'hh24');
	dt_apap_w	:= to_date(to_char(:new.dt_medida,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss');
	if	(to_char(round(:new.dt_medida,'hh24'),'hh24') = ds_hora_w) then
		:new.dt_apap	:= round(:new.dt_medida,'hh24');
	else
		begin
		qt_hora_w	:= (trunc(:new.dt_medida,'hh24') - to_date(to_char(:new.dt_medida,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss')) * 24;
		if	(qt_hora_w > 12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida + 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w > 0) and
			(qt_hora_w <= 12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w >= -12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w < -12) then
			:new.dt_apap	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_medida - 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		end if;
		end;
	end if;
	end;
end if;
<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.atend_perda_ganho_pend_atual
after insert or update ON TASY.ATENDIMENTO_PERDA_GANHO for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
ie_liberar_ganho_perda_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  OR
	((obter_funcao_ativa <> 281) and
	 (obter_funcao_ativa <> 1136) and
	 (obter_funcao_ativa <> 872)	and
	 (obter_funcao_ativa <> 10026)) then
	goto Final;
end if;

select	max(ie_liberar_ganho_perda)
into	ie_liberar_ganho_perda_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_liberar_ganho_perda_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'GP';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then


		ie_tipo_w := 'XGP';
	end if;
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, obter_pessoa_atendimento(:new.nr_atendimento,'C'), :new.nr_atendimento, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_PERDA_GANHO_ATUAL
before insert or update ON TASY.ATENDIMENTO_PERDA_GANHO for each row
declare

json_aux_bb philips_json;
envio_integracao_bb clob;
retorno_integracao_bb clob;
v_cell_label_id VARCHAR(32);


begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

  if	(nvl(:old.DT_MEDIDA,sysdate+10) <> :new.DT_MEDIDA) and
    (:new.DT_MEDIDA is not null) then
    :new.ds_utc		:= obter_data_utc(:new.DT_MEDIDA, 'HV');
  end if;

  if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
    (:new.DT_LIBERACAO is not null) then
    :new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
  end if;

  BEGIN
    SELECT
      CASE
      WHEN (IE_VARIAVEL = 1 AND IE_PERDA_GANHO = 'P') THEN
          'fa071f65005c4c6c855b93fa010012b7'
        ELSE null
        END cell_label_id
    INTO v_cell_label_id
    FROM
      ALGORITMOS_VAR_PERDA_GANHO
    WHERE
      nr_seq_tipo = :NEW.nr_seq_tipo;
  EXCEPTION
      WHEN no_data_found THEN
        v_cell_label_id := null;
  END;

  IF	(:old.DT_LIBERACAO is NULL) AND
    (:new.DT_LIBERACAO is not NULL) AND
    (v_cell_label_id is not NULL) THEN

      json_aux_bb := philips_json();
      json_aux_bb.put('typeID', 'IOFS');
      json_aux_bb.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'MM-DD-YYYY HH24:MI:SS.SSSSS'));
      json_aux_bb.put('objectID', 'ce1c28959d1c4cff990bbb9616c17110');
      json_aux_bb.put('name', 'Output (ml)');
      json_aux_bb.put('objectIDCol', 'FC' || LPAD('D' || :new.NR_SEQUENCIA, 30, 0));
      json_aux_bb.put('patientHealthSystemStayID', LPAD(:new.NR_ATENDIMENTO, 32, 0));
      json_aux_bb.put('columnDate', TO_CHAR(f_extract_utc_bb(:new.DT_MEDIDA), 'YYYY-MM-DD"T"HH24:MI'));
      json_aux_bb.put('columnDateGMTOffset', '0');
      json_aux_bb.put('cellLabelID',    v_cell_label_id);
      json_aux_bb.put('cellTypeCatID',  'ce1c28959d1c4cff990bbb9616c17110');
      json_aux_bb.put('objectIDCell',   'C1' || LPAD('D' || :new.NR_SEQUENCIA, 30, 0));
      json_aux_bb.put('resultStatusID', '9ef5ea20a97045fcb26dc3fcb86b6c24');
      json_aux_bb.put('valueNumeric',   TO_CHAR(:new.QT_VOLUME));

      dbms_lob.createtemporary(envio_integracao_bb, TRUE);
      json_aux_bb.to_clob(envio_integracao_bb);

      SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Perda_Urina',envio_integracao_bb,wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;

  END IF;

  IF	(:old.dt_inativacao is NULL) AND
    (:new.dt_inativacao is not NULL) AND
    (v_cell_label_id is not NULL) THEN
      p_cancelar_flowsheet(:new.nr_sequencia, :new.nr_atendimento, 'D');
  END IF;

end if;

end;
/


ALTER TABLE TASY.ATENDIMENTO_PERDA_GANHO ADD (
  CONSTRAINT ATEPEGA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_PERDA_GANHO ADD (
  CONSTRAINT ATEPEGA_DISPOSI_FK 
 FOREIGN KEY (NR_SEQ_DISPOSITIVO) 
 REFERENCES TASY.DISPOSITIVO (NR_SEQUENCIA),
  CONSTRAINT ATEPEGA_ATEPADI_FK 
 FOREIGN KEY (NR_SEQ_ATEND_DISP) 
 REFERENCES TASY.ATEND_PAC_DISPOSITIVO (NR_SEQUENCIA),
  CONSTRAINT ATEPEGA_HDPREVE_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_HD) 
 REFERENCES TASY.HD_PRESCRICAO_EVENTO (NR_SEQUENCIA),
  CONSTRAINT ATEPEGA_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEPEGA_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATEPEGA_ASPEEVA_FK 
 FOREIGN KEY (NR_SEQ_ASPECTO) 
 REFERENCES TASY.ASPECTO_EVACUACAO (NR_SEQUENCIA),
  CONSTRAINT ATEPEGA_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT ATEPEGA_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT ATEPEGA_TIPPEGA_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.TIPO_PERDA_GANHO (NR_SEQUENCIA),
  CONSTRAINT ATEPEGA_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATEPEGA_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA),
  CONSTRAINT ATEPEGA_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEPEGA_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEPEGA_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEPEGA_PRSOLEV_FK 
 FOREIGN KEY (NR_SEQ_EVENTO_ADEP) 
 REFERENCES TASY.PRESCR_SOLUCAO_EVENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATENDIMENTO_PERDA_GANHO TO NIVEL_1;

