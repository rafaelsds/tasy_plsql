ALTER TABLE TASY.COM_CANAL_CLIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COM_CANAL_CLIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.COM_CANAL_CLIENTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CANAL         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_CLIENTE       NUMBER(10)               NOT NULL,
  IE_TIPO_ATUACAO      VARCHAR2(3 BYTE)         NOT NULL,
  DT_INICIO_ATUACAO    DATE                     NOT NULL,
  DT_FIM_ATUACAO       DATE,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  PR_ESFORCO           NUMBER(15),
  CD_VENDEDOR          VARCHAR2(10 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMCACL_COMCANA_FK_I ON TASY.COM_CANAL_CLIENTE
(NR_SEQ_CANAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCACL_COMCANA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCACL_COMCLIE_FK_I ON TASY.COM_CANAL_CLIENTE
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCACL_COMCLIE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMCACL_PESFISI_FK_I ON TASY.COM_CANAL_CLIENTE
(CD_VENDEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMCACL_PK ON TASY.COM_CANAL_CLIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMCACL_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.COM_CANAL_CLIENTE_UPDATE
BEFORE UPDATE ON TASY.COM_CANAL_CLIENTE FOR EACH ROW
DECLARE

nr_sequencia_w		number(10);
qt_reg_w	number(1);
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.dt_fim_atuacao is not null) and
	(:old.dt_fim_atuacao is null) and
	(:new.ie_tipo_atuacao = 'V') then
	begin

	select	com_cliente_log_seq.NextVal
	into	nr_sequencia_w
	from	dual;

	insert into com_cliente_log
	(	nr_sequencia,
		nr_seq_cliente,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_log,
		dt_log,
		ie_classificacao,
		ie_fase_venda,
		nr_seq_canal)
	values	(
		nr_sequencia_w,
		:new.nr_seq_cliente,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		4,
		sysdate,
		null,
		null,
		:new.nr_seq_canal);

	end;

end if;

if	(:new.dt_fim_atuacao is null) and
	(:old.dt_fim_atuacao is not null) and
	(:new.ie_tipo_atuacao = 'V') then
	begin

	select	com_cliente_log_seq.NextVal
	into	nr_sequencia_w
	from	dual;

	insert into com_cliente_log
	(	nr_sequencia,
		nr_seq_cliente,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_log,
		dt_log,
		ie_classificacao,
		ie_fase_venda,
		nr_seq_canal)
	values	(
		nr_sequencia_w,
		:new.nr_seq_cliente,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		5,
		sysdate,
		null,
		null,
		:new.nr_seq_canal);

	end;

end if;
<<Final>>
qt_reg_w	:= 0;


end;
/


CREATE OR REPLACE TRIGGER TASY.COM_CANAL_CLIENTE_INSERT
BEFORE INSERT ON TASY.COM_CANAL_CLIENTE FOR EACH ROW
DECLARE

nr_sequencia_w		number(10);

begin

if	(:new.dt_fim_atuacao is null) and
	(:new.ie_tipo_atuacao = 'V') then
	begin

	select	com_cliente_log_seq.NextVal
	into	nr_sequencia_w
	from	dual;

	insert into com_cliente_log
	(	nr_sequencia,
		nr_seq_cliente,
		dt_atualizacao,
		nm_usuario,
		dt_atualizacao_nrec,
		nm_usuario_nrec,
		ie_log,
		dt_log,
		ie_classificacao,
		ie_fase_venda,
		nr_seq_canal)
	values	(
		nr_sequencia_w,
		:new.nr_seq_cliente,
		sysdate,
		:new.nm_usuario,
		sysdate,
		:new.nm_usuario,
		3,
		sysdate,
		null,
		null,
		:new.nr_seq_canal);

	end;

end if;

end;
/


ALTER TABLE TASY.COM_CANAL_CLIENTE ADD (
  CONSTRAINT COMCACL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COM_CANAL_CLIENTE ADD (
  CONSTRAINT COMCACL_COMCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.COM_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT COMCACL_COMCANA_FK 
 FOREIGN KEY (NR_SEQ_CANAL) 
 REFERENCES TASY.COM_CANAL (NR_SEQUENCIA),
  CONSTRAINT COMCACL_PESFISI_FK 
 FOREIGN KEY (CD_VENDEDOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.COM_CANAL_CLIENTE TO NIVEL_1;

