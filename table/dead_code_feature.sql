ALTER TABLE TASY.DEAD_CODE_FEATURE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.DEAD_CODE_FEATURE CASCADE CONSTRAINTS;

CREATE TABLE TASY.DEAD_CODE_FEATURE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_CGC               VARCHAR2(14 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_FUNCAO            NUMBER(5),
  DT_ACESSO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.DEACODFEAT_PK ON TASY.DEAD_CODE_FEATURE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.DEAD_CODE_FEATURE ADD (
  CONSTRAINT DEACODFEAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

