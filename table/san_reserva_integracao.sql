ALTER TABLE TASY.SAN_RESERVA_INTEGRACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_RESERVA_INTEGRACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_RESERVA_INTEGRACAO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_REQUISICAO         NUMBER(16),
  IE_STATUS_REQUISICAO  VARCHAR2(1 BYTE),
  NR_SEQ_RESERVA        NUMBER(10),
  NR_ATENDIMENTO        NUMBER(10)              NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SAREINT_ATEPACI_FK_I ON TASY.SAN_RESERVA_INTEGRACAO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SAREINT_PK ON TASY.SAN_RESERVA_INTEGRACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SAREINT_SANRESE_FK_I ON TASY.SAN_RESERVA_INTEGRACAO
(NR_SEQ_RESERVA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.san_reserva_integracao_upd
before update ON TASY.SAN_RESERVA_INTEGRACAO for each row
declare
  qt_registro_w		number(10);
  nr_sequencia_w		number(10);
  nr_seq_entidade_w   number(10);
  cd_pessoa_fisica_w	varchar2(10);
  cd_medico_resp_w 	varchar2(10);
  cd_convenio_w		number(5);
  cd_derivado_w		varchar2(40);
  qt_solicitada_w	san_reserva_item.qt_solicitada%type;
  nr_seq_item_w		number(5) := 1;
  nr_seq_iteg_item_w	number(10);
  ie_irradiado_w	varchar2(1);
  ie_lavado_w		varchar2(1);
  ie_filtrado_w		varchar2(1);
  ie_aliquotado_w	varchar2(1);

  cursor C01 is
    select  	nr_sequencia,
		cd_derivado,
		qt_solicitada,
		ie_irradiado,
		ie_lavado,
		ie_filtrado,
		ie_aliquotado
    from    san_res_integ_item
    where   nr_seq_res_integracao = :new.nr_sequencia;

begin

  if (:new.ie_status_requisicao = 'L') then

    select  count(1)
    into	qt_registro_w
    from    atendimento_paciente
    where   nr_atendimento = :new.nr_atendimento;

    if qt_registro_w = 0 then

      gravar_log_tasy(3470, wheb_mensagem_pck.get_texto(794870,
						'NR_ATENDIMENTO='||:new.nr_atendimento||
						';NR_SEQUENCIA='||:new.nr_sequencia), :new.nm_usuario);
      :new.ie_status_requisicao := 'E';

    elsif qt_registro_w > 0 then

      select	SAN_RESERVA_SEQ.nextval
      into	nr_sequencia_w
      from	dual;

      select  max(cd_pessoa_fisica), max(cd_medico_resp)
      into    cd_pessoa_fisica_w, cd_medico_resp_w
      from    atendimento_paciente
      where   nr_atendimento = :NEW.nr_atendimento;

      select  max(cd_convenio)
      into	cd_convenio_w
      from    atend_categoria_convenio
      where   nr_atendimento = :NEW.nr_atendimento;

      select	max(nr_seq_entidade)
      into	nr_seq_entidade_w
      from	san_parametro
      where	cd_estabelecimento = 1;

      insert into san_reserva	(nr_sequencia,
          dt_atualizacao,
          nm_usuario,
          dt_atualizacao_nrec,
          nm_usuario_nrec,
          nr_atendimento,
          cd_pessoa_fisica,
          dt_cirurgia,
          dt_reserva,
          cd_pf_realizou,
          cd_medico_requisitante,
          cd_convenio,
          ie_status,
          nr_seq_entidade,
          cd_estabelecimento)
          values (nr_sequencia_w,
          sysdate,
          'TASY',
          sysdate,
          'TASY',
          :new.nr_atendimento,
          cd_pessoa_fisica_w,
          sysdate,
          sysdate,
          cd_medico_resp_w,
          cd_medico_resp_w,
          cd_convenio_w,
          'R',
          nr_seq_entidade_w,
          1);

      :new.NR_SEQ_RESERVA := nr_sequencia_w;

      open C01;
      loop
      fetch C01 into
        nr_seq_iteg_item_w,
	cd_derivado_w,
        qt_solicitada_w,
	ie_irradiado_w,
	ie_lavado_w,
	ie_filtrado_w,
	ie_aliquotado_w;
      exit when C01%notfound;

      begin
          select  nvl(max(nr_sequencia),0)
          into    qt_registro_w
          from    san_derivado
          where   cd_externo = cd_derivado_w;

          if qt_registro_w = 0 then

            gravar_log_tasy(3470, wheb_mensagem_pck.get_texto(794871,
							'CD_DERIVADO='||cd_derivado_w||
							';NR_SEQ_ITEG_ITEM='||nr_seq_iteg_item_w||
							';NR_SEQUENCIA='||:new.nr_sequencia), :new.nm_usuario);
            :new.ie_status_requisicao := 'E';

          elsif qt_registro_w > 0 then

            insert into san_reserva_item	(nr_seq_reserva,
                nr_seq_item,
                dt_atualizacao,
                nm_usuario,
                dt_atualizacao_nrec,
                nm_usuario_nrec,
                nr_seq_derivado,
                qt_solicitada,
                ie_util_hemocomponente,
                ie_suspenso,
		ie_irradiado,
		ie_lavado,
		ie_filtrado,
		ie_aliquotado)
              values (nr_sequencia_w,
                nr_seq_item_w,
                sysdate,
                'TASY',
                sysdate,
                'TASY',
                qt_registro_w,
                nvl(qt_solicitada_w,1),
                'R',
                'N',
		ie_irradiado_w,
		ie_lavado_w,
		ie_filtrado_w,
		ie_aliquotado_w);

            nr_seq_item_w := nr_seq_item_w + 1;

          end if;
             end;
      end loop;
      close C01;

      if :new.ie_status_requisicao = 'L' then --garante que o status n�o mudou durante a rotina para E - Erro
        :new.ie_status_requisicao := 'P';
      end if;

    end if;
  end if;
end;
/


ALTER TABLE TASY.SAN_RESERVA_INTEGRACAO ADD (
  CONSTRAINT SAREINT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_RESERVA_INTEGRACAO ADD (
  CONSTRAINT SAREINT_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT SAREINT_SANRESE_FK 
 FOREIGN KEY (NR_SEQ_RESERVA) 
 REFERENCES TASY.SAN_RESERVA (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_RESERVA_INTEGRACAO TO NIVEL_1;

