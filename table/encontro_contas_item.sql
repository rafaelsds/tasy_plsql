ALTER TABLE TASY.ENCONTRO_CONTAS_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ENCONTRO_CONTAS_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ENCONTRO_CONTAS_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PESSOA        NUMBER(10)               NOT NULL,
  NR_TITULO_RECEBER    NUMBER(10),
  NR_TITULO_PAGAR      NUMBER(10),
  VL_JUROS             NUMBER(15,2),
  VL_MULTA             NUMBER(15,2),
  VL_SALDO_JUROS       NUMBER(15,2),
  VL_SALDO_MULTA       NUMBER(15,2),
  VL_GLOSA_AP          NUMBER(15,2),
  VL_GLOSA             NUMBER(15,2),
  VL_DESCONTO          NUMBER(15,2),
  VL_IMPOSTO           NUMBER(15,2),
  VL_GLOSA_TIT         NUMBER(15,2),
  VL_DESPESA           NUMBER(15,2),
  VL_TOTAL             NUMBER(15,2),
  VL_INFORMADO         NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ENCCITE_PESENCC_FK_I ON TASY.ENCONTRO_CONTAS_ITEM
(NR_SEQ_PESSOA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ENCCITE_PK ON TASY.ENCONTRO_CONTAS_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ENCCITE_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.encontro_contas_item_bf_up_del
before update or delete ON TASY.ENCONTRO_CONTAS_ITEM for each row
declare
ie_possui_baixa_w		number(15) := 0;
nr_seq_lote_enc_contas_w	number(10,0);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if	(:new.nr_titulo_receber is null) or
	(:new.nr_titulo_pagar is null) Then

	select	b.nr_seq_lote
	into	nr_seq_lote_enc_contas_w
	from	pessoa_encontro_contas b
	where	b.nr_sequencia		= :old.nr_seq_pessoa;

	if	(:old.nr_titulo_receber is not null) and
		(:old.nr_titulo_receber <> nvl(:new.nr_titulo_receber, 0))then
		/*select	count(1)
		into	ie_possui_baixa_w
		from	titulo_receber_liq a
		where	a.nr_titulo			= :old.nr_titulo_receber
		and	a.nr_seq_lote_enc_contas 	= nr_seq_lote_enc_contas_w
		and	rownum <= 1;*/

		SELECT	COUNT(*)
		INTO	ie_possui_baixa_w
		FROM	titulo_receber_liq a
		WHERE	a.nr_titulo			= :old.nr_titulo_receber
		AND	a.nr_seq_lote_enc_contas 	= nr_seq_lote_enc_contas_w
		AND	    	NOT EXISTS (	SELECT 1 -- AAMFIRMO OS 658286 Inclui para nao considerar baixas que possuem estorno.
						FROM	 	titulo_receber_liq x
						WHERE  		x.nr_titulo = a.nr_titulo
						AND    		x.nr_seq_lote_enc_contas = nr_seq_lote_enc_contas_w
						AND    		x.nr_seq_liq_origem IS NOT NULL)
		and	rownum <= 1;


	elsif	(:old.nr_titulo_pagar is not null) and
		(:old.nr_titulo_pagar <> nvl(:new.nr_titulo_pagar, 0)) then
		/*select	count(1)
		into	ie_possui_baixa_w
		from	titulo_pagar_baixa a
		where	a.nr_titulo 			= :old.nr_titulo_receber
		and	a.nr_seq_lote_enc_contas 	= nr_seq_lote_enc_contas_w
		and	rownum <= 1;*/

		SELECT	COUNT(*)
		INTO	ie_possui_baixa_w
		FROM	titulo_pagar_baixa a
		WHERE	a.nr_titulo			= :old.nr_titulo_pagar
		AND	a.nr_seq_lote_enc_contas 	= nr_seq_lote_enc_contas_w
		AND	    	NOT EXISTS (	SELECT 1 -- AAMFIRMO OS 658286 Inclui para nao considerar baixas que possuem estorno.
						FROM	 	titulo_pagar_baixa x
						WHERE  		x.nr_titulo = a.nr_titulo
						AND    		x.nr_seq_lote_enc_contas = nr_seq_lote_enc_contas_w
						AND    		x.nr_seq_baixa_origem IS NOT NULL)
		and	rownum <= 1;
	end if;

	if	(ie_possui_baixa_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(211087);
	end if;
end if;
end if;
end;
/


ALTER TABLE TASY.ENCONTRO_CONTAS_ITEM ADD (
  CONSTRAINT ENCCITE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ENCONTRO_CONTAS_ITEM ADD (
  CONSTRAINT ENCCITE_PESENCC_FK 
 FOREIGN KEY (NR_SEQ_PESSOA) 
 REFERENCES TASY.PESSOA_ENCONTRO_CONTAS (NR_SEQUENCIA));

GRANT SELECT ON TASY.ENCONTRO_CONTAS_ITEM TO NIVEL_1;

