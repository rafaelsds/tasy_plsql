ALTER TABLE TASY.PLS_CONTROLE_BIOMETRIA_MED
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CONTROLE_BIOMETRIA_MED CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CONTROLE_BIOMETRIA_MED
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  CD_ESTABELECIMENTO            NUMBER(4)       NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  CD_MEDICO                     VARCHAR2(10 BYTE),
  DT_CHECK_IN                   DATE,
  DT_CHECK_OUT                  DATE,
  NR_SEQ_USUARIO_WEB_CHECK_IN   NUMBER(10),
  NR_SEQ_USUARIO_WEB_CHECK_OUT  NUMBER(10),
  DS_OBSERVACAO                 VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSCBIM_ESTABEL_FK_I ON TASY.PLS_CONTROLE_BIOMETRIA_MED
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBIM_MEDICO_FK_I ON TASY.PLS_CONTROLE_BIOMETRIA_MED
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSCBIM_PK ON TASY.PLS_CONTROLE_BIOMETRIA_MED
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBIM_PLSUSWE_FK_I ON TASY.PLS_CONTROLE_BIOMETRIA_MED
(NR_SEQ_USUARIO_WEB_CHECK_IN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCBIM_PLSUSWE_FK2_I ON TASY.PLS_CONTROLE_BIOMETRIA_MED
(NR_SEQ_USUARIO_WEB_CHECK_OUT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_CONTROLE_BIOMETRIA_MED ADD (
  CONSTRAINT PLSCBIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_CONTROLE_BIOMETRIA_MED ADD (
  CONSTRAINT PLSCBIM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSCBIM_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PLSCBIM_PLSUSWE_FK 
 FOREIGN KEY (NR_SEQ_USUARIO_WEB_CHECK_IN) 
 REFERENCES TASY.PLS_USUARIO_WEB (NR_SEQUENCIA),
  CONSTRAINT PLSCBIM_PLSUSWE_FK2 
 FOREIGN KEY (NR_SEQ_USUARIO_WEB_CHECK_OUT) 
 REFERENCES TASY.PLS_USUARIO_WEB (NR_SEQUENCIA));

