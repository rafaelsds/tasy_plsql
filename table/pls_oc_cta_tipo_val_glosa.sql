ALTER TABLE TASY.PLS_OC_CTA_TIPO_VAL_GLOSA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_TIPO_VAL_GLOSA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_TIPO_VAL_GLOSA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_TIPO_VALIDACAO  NUMBER(10)             NOT NULL,
  CD_MOTIVO_TISS         VARCHAR2(10 BYTE)      NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.POCTVGL_I1 ON TASY.PLS_OC_CTA_TIPO_VAL_GLOSA
(CD_MOTIVO_TISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.POCTVGL_I1
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.POCTVGL_PK ON TASY.PLS_OC_CTA_TIPO_VAL_GLOSA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.POCTVGL_PLSOCTV_FK_I ON TASY.PLS_OC_CTA_TIPO_VAL_GLOSA
(NR_SEQ_TIPO_VALIDACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_TIPO_VAL_GLOSA_tp  after update ON TASY.PLS_OC_CTA_TIPO_VAL_GLOSA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_MOTIVO_TISS,1,500);gravar_log_alteracao(substr(:old.CD_MOTIVO_TISS,1,4000),substr(:new.CD_MOTIVO_TISS,1,4000),:new.nm_usuario,nr_seq_w,'CD_MOTIVO_TISS',ie_log_w,ds_w,'PLS_OC_CTA_TIPO_VAL_GLOSA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_TIPO_VAL_GLOSA ADD (
  CONSTRAINT POCTVGL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_TIPO_VAL_GLOSA ADD (
  CONSTRAINT POCTVGL_PLSOCTV_FK 
 FOREIGN KEY (NR_SEQ_TIPO_VALIDACAO) 
 REFERENCES TASY.PLS_OC_CTA_TIPO_VALIDACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_TIPO_VAL_GLOSA TO NIVEL_1;

