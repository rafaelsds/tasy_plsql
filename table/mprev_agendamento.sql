ALTER TABLE TASY.MPREV_AGENDAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_AGENDAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_AGENDAMENTO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_AGENDA                  NUMBER(10)         NOT NULL,
  NR_MINUTO_DURACAO          NUMBER(3)          NOT NULL,
  DT_AGENDA                  DATE               NOT NULL,
  DT_AGENDAMENTO             DATE               NOT NULL,
  IE_STATUS_AGENDA           VARCHAR2(2 BYTE),
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  NR_SEQ_MOTIVO_CANC         NUMBER(10),
  DT_CONFIRMACAO             DATE,
  DS_CONFIRMACAO             VARCHAR2(80 BYTE),
  DT_CANCELAMENTO            DATE,
  NR_SEQ_PARTICIPANTE        NUMBER(10),
  NR_SEQ_TURMA               NUMBER(10),
  DT_ATENDIDO                DATE,
  DT_CHEGADA                 DATE,
  NM_USUARIO_CONFIRM         VARCHAR2(15 BYTE),
  NR_SEQ_STATUS_PAC          NUMBER(10),
  NR_SEQ_HORARIO_TURMA       NUMBER(10),
  CD_AGENDA_LOCAL            NUMBER(10),
  CD_AGENDA_PROFISSIONAL     NUMBER(10),
  NR_SEQ_FORMA_CONFIRM       NUMBER(10),
  DS_UTILIZACAO              VARCHAR2(255 BYTE),
  IE_TIPO_ATENDIMENTO        VARCHAR2(1 BYTE),
  IE_FORMA_ATENDIMENTO       VARCHAR2(2 BYTE),
  IE_PROFISSIONAL_ESPEC      VARCHAR2(1 BYTE),
  NR_SEQ_PARTIC_CICLO_ITEM   NUMBER(10),
  NR_SEQ_AGENDAMENTO_REF     NUMBER(10),
  NR_SEQ_AGENDAMENTO_TRANSF  NUMBER(10),
  NR_SEQ_MPREV_ATEND         NUMBER(10),
  NR_SEQ_CAPTACAO            NUMBER(10),
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  NR_SEQ_TURMA_ESPERA        NUMBER(10),
  DT_FINAL_AGENDA            DATE,
  IE_PREVISTO                VARCHAR2(1 BYTE),
  IE_TIPO_AGENDAMENTO        VARCHAR2(3 BYTE),
  NR_SEQ_ATIV_EXTRA          NUMBER(10),
  NR_SEQ_GRUPO_TEMA          NUMBER(10),
  NR_SEQ_PART_CANC_ATIV      NUMBER(10),
  NR_SEQ_LOCAL_ATEND         NUMBER(10),
  NR_SEQ_FORMA_ATEND         NUMBER(10),
  NR_SEQ_PAC_SENHA_FILA      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRAGEND_AGENDA_FK_I ON TASY.MPREV_AGENDAMENTO
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRAGEND_AGENDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRAGEND_AGFOCON_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_FORMA_CONFIRM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRAGEND_AGFOCON_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRAGEND_AGMOCAN_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_MOTIVO_CANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRAGEND_AGMOCAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRAGEND_ESTABEL_FK_I ON TASY.MPREV_AGENDAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_MPRAGEND_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_AGENDAMENTO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRAGEND_MPRAGEND_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRAGEND_MPRAGEND_FK2_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_AGENDAMENTO_TRANSF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_MPRATEA_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_ATIV_EXTRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_MPRATEN_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_MPREV_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_MPRCAPT_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_CAPTACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_MPRFORA_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_FORMA_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_MPRGCTH_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_HORARIO_TURMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRAGEND_MPRGCTH_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRAGEND_MPRGCTU_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_TURMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRAGEND_MPRGCTU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRAGEND_MPRGTEN_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_GRUPO_TEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_MPRGTES_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_TURMA_ESPERA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_MPRLOCA_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_LOCAL_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_MPRPACI_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_PARTIC_CICLO_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRAGEND_MPRPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRAGEND_MPRPART_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_PARTICIPANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRAGEND_MPRPART_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRAGEND_MPRPCAT_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_PART_CANC_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_PACSEFI_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_PAC_SENHA_FILA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MPRAGEND_PK ON TASY.MPREV_AGENDAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MPRAGEND_STAPAAG_FK_I ON TASY.MPREV_AGENDAMENTO
(NR_SEQ_STATUS_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRAGEND_STAPAAG_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.mprev_agendamento_bef_atual
before insert or update ON TASY.MPREV_AGENDAMENTO for each row
begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S')  then
	:new.dt_final_agenda	:= :new.dt_agenda + (:new.nr_minuto_duracao * (1/24/60));

	/* N�o pode haver agendamento sem participante ou turma */
	if	((:new.nr_seq_participante is null) and
		(:new.nr_seq_turma is null) and
		(:new.nr_seq_captacao is null) and
		(:new.nr_seq_ativ_extra is null)) then
		--O agendamento precisa ser para um participante ou turma.
		wheb_mensagem_pck.exibir_mensagem_abort(270306);
	end if;

	/* Tratar v�nculo do plano de atendimento com o agendamento */
	/*if	(:new.nr_seq_partic_ciclo_item is null) then  OS 855230 */
		/* :new.nr_seq_partic_ciclo_item	:= mprev_obter_atend_prev_data(:new.nr_seq_participante,:new.dt_agenda); OS 855230 */
		/* Se achou o plano, atualizar no atendimento previsto do ciclo o status */
		if	((:old.nr_seq_partic_ciclo_item is null) and
			(:new.nr_seq_partic_ciclo_item is not null)) or
			((:old.nr_seq_partic_ciclo_item is not null) and
			(:old.nr_seq_partic_ciclo_item  <> :new.nr_seq_partic_ciclo_item) and
			(:new.nr_seq_partic_ciclo_item is not null)) then
			update	mprev_partic_ciclo_item
			set	ie_status	= 'A', /* Agendado */
				dt_atualizacao	= sysdate,
				nm_usuario	= :new.nm_usuario
			where	nr_sequencia	= :new.nr_seq_partic_ciclo_item
			and	ie_status	= 'P';
		end if;
	/*end if;  OS 855230 */
end if;
end;
/


ALTER TABLE TASY.MPREV_AGENDAMENTO ADD (
  CONSTRAINT MPRAGEND_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_AGENDAMENTO ADD (
  CONSTRAINT MPRAGEND_STAPAAG_FK 
 FOREIGN KEY (NR_SEQ_STATUS_PAC) 
 REFERENCES TASY.STATUS_PACIENTE_AGENDA (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRATEN_FK 
 FOREIGN KEY (NR_SEQ_MPREV_ATEND) 
 REFERENCES TASY.MPREV_ATENDIMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRAGEND_MPRPACI_FK 
 FOREIGN KEY (NR_SEQ_PARTIC_CICLO_ITEM) 
 REFERENCES TASY.MPREV_PARTIC_CICLO_ITEM (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRAGEND_FK 
 FOREIGN KEY (NR_SEQ_AGENDAMENTO_REF) 
 REFERENCES TASY.MPREV_AGENDAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRAGEND_MPRAGEND_FK2 
 FOREIGN KEY (NR_SEQ_AGENDAMENTO_TRANSF) 
 REFERENCES TASY.MPREV_AGENDAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRAGEND_MPRGTES_FK 
 FOREIGN KEY (NR_SEQ_TURMA_ESPERA) 
 REFERENCES TASY.MPREV_GRUPO_TURMA_ESPERA (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRATEA_FK 
 FOREIGN KEY (NR_SEQ_ATIV_EXTRA) 
 REFERENCES TASY.MPREV_ATIV_EXTRA_AGENDA (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRGTEN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TEMA) 
 REFERENCES TASY.MPREV_GRUPO_TEMA_ENCONTRO (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRPCAT_FK 
 FOREIGN KEY (NR_SEQ_PART_CANC_ATIV) 
 REFERENCES TASY.MPREV_PARTIC_CANC_ATIV (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRCAPT_FK 
 FOREIGN KEY (NR_SEQ_CAPTACAO) 
 REFERENCES TASY.MPREV_CAPTACAO (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MPRAGEND_MPRLOCA_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_ATEND) 
 REFERENCES TASY.MPREV_LOCAL_ATEND (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRFORA_FK 
 FOREIGN KEY (NR_SEQ_FORMA_ATEND) 
 REFERENCES TASY.MPREV_FORMA_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA),
  CONSTRAINT MPRAGEND_PACSEFI_FK 
 FOREIGN KEY (NR_SEQ_PAC_SENHA_FILA) 
 REFERENCES TASY.PACIENTE_SENHA_FILA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MPRAGEND_AGFOCON_FK 
 FOREIGN KEY (NR_SEQ_FORMA_CONFIRM) 
 REFERENCES TASY.AGENDA_FORMA_CONTATO (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_AGMOCAN_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANC) 
 REFERENCES TASY.AGENDA_MOTIVO_CANCELAMENTO (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRGCTH_FK 
 FOREIGN KEY (NR_SEQ_HORARIO_TURMA) 
 REFERENCES TASY.MPREV_GRUPO_COL_TURMA_HOR (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRGCTU_FK 
 FOREIGN KEY (NR_SEQ_TURMA) 
 REFERENCES TASY.MPREV_GRUPO_COL_TURMA (NR_SEQUENCIA),
  CONSTRAINT MPRAGEND_MPRPART_FK 
 FOREIGN KEY (NR_SEQ_PARTICIPANTE) 
 REFERENCES TASY.MPREV_PARTICIPANTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_AGENDAMENTO TO NIVEL_1;

