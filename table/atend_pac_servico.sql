ALTER TABLE TASY.ATEND_PAC_SERVICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATEND_PAC_SERVICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATEND_PAC_SERVICO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_ATENDIMENTO       NUMBER(10)               NOT NULL,
  IE_FRIGOBAR          VARCHAR2(1 BYTE)         NOT NULL,
  IE_TELEFONE          VARCHAR2(1 BYTE)         NOT NULL,
  IE_INTERNET          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEPESE_ATEPACI_FK_I ON TASY.ATEND_PAC_SERVICO
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEPESE_PK ON TASY.ATEND_PAC_SERVICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.HSJ_ATEND_PAC_SERVICO_UPDATE
after update ON TASY.ATEND_PAC_SERVICO for each row
DECLARE
nm_usuario_w varchar(20);

begin

    if :old.IE_INTERNET <> :new.IE_INTERNET THEN
    begin
    select nm_usuario
    into nm_usuario_w
    from HSJ_AUTENTICACAO_WIFI where nm_usuario = to_char(:OLD.NR_ATENDIMENTO);
   
    EXCEPTION WHEN NO_DATA_FOUND THEN
      nm_usuario_w := 'N'; 
    end;                                       
    
    if nm_usuario_w <> 'N' then
      delete from tasy.hsj_autenticacao_wifi where nm_usuario = to_char(nm_usuario_w);
    end if;    
    
    end if;    
  

end;
/


ALTER TABLE TASY.ATEND_PAC_SERVICO ADD (
  CONSTRAINT ATEPESE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATEND_PAC_SERVICO ADD (
  CONSTRAINT ATEPESE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ATEND_PAC_SERVICO TO NIVEL_1;

