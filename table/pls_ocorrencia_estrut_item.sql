ALTER TABLE TASY.PLS_OCORRENCIA_ESTRUT_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OCORRENCIA_ESTRUT_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_ESTRUTURA      NUMBER(10)              NOT NULL,
  IE_ESTRUTURA          VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  CD_AREA_PROCEDIMENTO  NUMBER(15),
  CD_ESPECIALIDADE      NUMBER(15),
  CD_GRUPO_PROC         NUMBER(15),
  NR_SEQ_MATERIAL       NUMBER(10),
  NR_SEQ_ESTRUT_MAT     NUMBER(10),
  CD_MATERIAL           NUMBER(6),
  DS_OBSERVACAO         VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSOCEI_AREPROC_FK_I ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCEI_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCEI_ESPPROC_FK_I ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCEI_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCEI_GRUPROC_FK_I ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCEI_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCEI_MATERIA_FK_I ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCEI_MATERIA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSOCEI_PK ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCEI_PLSESMAT_FK_I ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(NR_SEQ_ESTRUT_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCEI_PLSESMAT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCEI_PLSMAT_FK_I ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOCEI_PLSOCES_FK_I ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOCEI_PLSOCES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSOCEI_PROCEDI_FK_I ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OCORRENCIA_ESTRUT_ATUAL
BEFORE INSERT OR UPDATE ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM FOR EACH ROW
declare

cd_material_w	number(6);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	if	(:new.nr_seq_material is not null) then
		select	cd_material
		into	cd_material_w
		from	pls_material a
		where	a.nr_sequencia	= :new.nr_seq_material;

		:new.cd_material	:= cd_material_w;
	end if;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.pls_ocor_estrut_item_atual
before insert or update or delete ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM for each row
declare
nr_sequencia_w	pls_ocorrencia_estrut_item.nr_sequencia%type;
begin

-- se for apagar o registro
if	(deleting) then
	nr_sequencia_w := :old.nr_seq_estrutura;
else
	nr_sequencia_w := :new.nr_seq_estrutura;
end if;

pls_gerencia_upd_obj_pck.marcar_para_atualizacao('PLS_ESTRUTURA_OCOR_TM', wheb_usuario_pck.get_nm_usuario,
						 'PLS_OCOR_ESTRUT_ITEM_ATUAL',
						 'nr_seq_ocor_estrut_p=' || nr_sequencia_w);

end;
/


ALTER TABLE TASY.PLS_OCORRENCIA_ESTRUT_ITEM ADD (
  CONSTRAINT PLSOCEI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OCORRENCIA_ESTRUT_ITEM ADD (
  CONSTRAINT PLSOCEI_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PLSOCEI_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PLSOCEI_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PLSOCEI_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSOCEI_PLSOCES_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PLS_OCORRENCIA_ESTRUTURA (NR_SEQUENCIA),
  CONSTRAINT PLSOCEI_PLSMAT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.PLS_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSOCEI_PLSESMAT_FK 
 FOREIGN KEY (NR_SEQ_ESTRUT_MAT) 
 REFERENCES TASY.PLS_ESTRUTURA_MATERIAL (NR_SEQUENCIA),
  CONSTRAINT PLSOCEI_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.PLS_OCORRENCIA_ESTRUT_ITEM TO NIVEL_1;

