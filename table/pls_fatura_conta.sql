ALTER TABLE TASY.PLS_FATURA_CONTA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_FATURA_CONTA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_FATURA_CONTA
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_FATURA_EVENTO     NUMBER(10)           NOT NULL,
  NR_SEQ_CONTA             NUMBER(10),
  VL_FATURADO              NUMBER(15,2)         NOT NULL,
  CD_CONTA_DEBITO          VARCHAR2(20 BYTE),
  CD_CONTA_CREDITO         VARCHAR2(20 BYTE),
  NR_LOTE_CONTABIL         NUMBER(10)           NOT NULL,
  NR_SEQ_SEGURADO          NUMBER(10)           NOT NULL,
  IE_TIPO_COBRANCA         VARCHAR2(3 BYTE),
  VL_FATURADO_NDC          NUMBER(15,2),
  IE_TIPO_VINCULACAO       VARCHAR2(1 BYTE),
  IE_CONTA_FECHADA         VARCHAR2(1 BYTE),
  CD_GUIA_REFERENCIA       VARCHAR2(20 BYTE),
  NR_SEQ_FAT_ATEND         NUMBER(10),
  NR_SEQ_CONGENERE         NUMBER(10),
  IE_IMPEDIMENTO_COBRANCA  VARCHAR2(3 BYTE),
  CD_GUIA_ORIGINAL         VARCHAR2(20 BYTE),
  NR_SEQ_CONTA_SUS         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSFACO_LOTCONT_FK_I ON TASY.PLS_FATURA_CONTA
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSFACO_PK ON TASY.PLS_FATURA_CONTA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFACO_PLSCOME_FK_I ON TASY.PLS_FATURA_CONTA
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFACO_PLSCOME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFACO_PLSCONG_FK_I ON TASY.PLS_FATURA_CONTA
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFACO_PLSFAEV_FK_I ON TASY.PLS_FATURA_CONTA
(NR_SEQ_FATURA_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFACO_PLSFAEV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSFACO_PLSFTAT_FK_I ON TASY.PLS_FATURA_CONTA
(NR_SEQ_FAT_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFACO_PLSPCON_FK_I ON TASY.PLS_FATURA_CONTA
(NR_SEQ_CONTA_SUS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSFACO_PLSSEGU_FK_I ON TASY.PLS_FATURA_CONTA
(NR_SEQ_SEGURADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSFACO_PLSSEGU_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_fatura_conta_atual
before update or insert ON TASY.PLS_FATURA_CONTA for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[ ]  Objetos do dicion�rio [ ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare

nr_seq_congenere_w		pls_congenere.nr_sequencia%type;
nr_seq_fatura_w			pls_fatura.nr_sequencia%type;
nr_seq_lote_w			pls_lote_faturamento.nr_sequencia%type;
ds_programa_w			varchar2(255);
ds_observacao_w			pls_fatura_log.ds_observacao%type;

begin
if	(:new.nr_seq_congenere is not null) and
	(:new.nr_seq_fatura_evento is not null) then
	select	max(x.nr_seq_congenere),
		max(x.nr_sequencia),
		max(x.nr_seq_lote)
	into	nr_seq_congenere_w,
		nr_seq_fatura_w,
		nr_seq_lote_w
	from	pls_fatura	x,
		pls_fatura_evento w
	where	x.nr_sequencia	= w.nr_seq_fatura
	and	w.nr_sequencia	= :new.nr_seq_fatura_evento;

	if	(nvl(nr_seq_congenere_w,0) <> :new.nr_seq_congenere) then
		select	substr(max(program),1,254)
		into	ds_programa_w
		from	v$session
		where	audsid = (select userenv('sessionid') from dual)
		and	username = (select username from v$session where audsid = (select userenv('sessionid') from dual));

		ds_observacao_w := 	'PLS_FATURA_CONTA_ATUAL - '||ds_programa_w||
					' - PLS_FATURA_CONTA.NR_SEQ_CONGENERE: '||:new.nr_seq_congenere||
					' - PLS_FATURA.NR_SEQ_CONGENERE: '||nr_seq_congenere_w;

		pls_gerar_fatura_log( nr_seq_lote_w, nr_seq_fatura_w, :new.nr_seq_conta, ds_observacao_w, 'FL', 'N', :new.nm_usuario);
	end if;
end if;

end;
/


ALTER TABLE TASY.PLS_FATURA_CONTA ADD (
  CONSTRAINT PLSFACO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_FATURA_CONTA ADD (
  CONSTRAINT PLSFACO_PLSPCON_FK 
 FOREIGN KEY (NR_SEQ_CONTA_SUS) 
 REFERENCES TASY.PLS_PROCESSO_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSFACO_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSFACO_PLSFAEV_FK 
 FOREIGN KEY (NR_SEQ_FATURA_EVENTO) 
 REFERENCES TASY.PLS_FATURA_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PLSFACO_PLSSEGU_FK 
 FOREIGN KEY (NR_SEQ_SEGURADO) 
 REFERENCES TASY.PLS_SEGURADO (NR_SEQUENCIA),
  CONSTRAINT PLSFACO_PLSFTAT_FK 
 FOREIGN KEY (NR_SEQ_FAT_ATEND) 
 REFERENCES TASY.PLS_FATURA_ATEND (NR_SEQUENCIA),
  CONSTRAINT PLSFACO_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL),
  CONSTRAINT PLSFACO_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_FATURA_CONTA TO NIVEL_1;

