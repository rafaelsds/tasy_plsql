ALTER TABLE TASY.GER_TIPO_CARGA_CAMPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GER_TIPO_CARGA_CAMPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.GER_TIPO_CARGA_CAMPO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ARQUIVO       NUMBER(10)               NOT NULL,
  NM_CAMPO             VARCHAR2(50 BYTE)        NOT NULL,
  IE_TIPO_CAMPO        VARCHAR2(10 BYTE)        NOT NULL,
  QT_TAMANHO           NUMBER(10),
  IE_OBRIGATORIO       VARCHAR2(1 BYTE)         NOT NULL,
  DS_CAMPO             VARCHAR2(2000 BYTE),
  NR_ORDEM             NUMBER(10),
  IE_CONVERSAO         VARCHAR2(1 BYTE)         NOT NULL,
  DS_ELEMENTO_XML_IMP  VARCHAR2(100 BYTE),
  DS_NODO_XML_PAI      VARCHAR2(100 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GERTPCMP_GERTPCAR_FK_I ON TASY.GER_TIPO_CARGA_CAMPO
(NR_SEQ_ARQUIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GERTPCMP_PK ON TASY.GER_TIPO_CARGA_CAMPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GER_TIPO_CARGA_CAMPO ADD (
  CONSTRAINT GERTPCMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GER_TIPO_CARGA_CAMPO ADD (
  CONSTRAINT GERTPCMP_GERTPCAR_FK 
 FOREIGN KEY (NR_SEQ_ARQUIVO) 
 REFERENCES TASY.GER_TIPO_CARGA_ARQ (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.GER_TIPO_CARGA_CAMPO TO NIVEL_1;

