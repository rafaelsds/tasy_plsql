ALTER TABLE TASY.INTERF_BANDA_ORDEM_SERV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.INTERF_BANDA_ORDEM_SERV CASCADE CONSTRAINTS;

CREATE TABLE TASY.INTERF_BANDA_ORDEM_SERV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_BANDA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORDEM_SERV    NUMBER(10),
  DS_ORDEM_SERVICO     VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.INSEBOS_INTBAND_FK_I ON TASY.INTERF_BANDA_ORDEM_SERV
(NR_SEQ_BANDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.INSEBOS_PK ON TASY.INTERF_BANDA_ORDEM_SERV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.INTERF_BANDA_ORDEM_SERV ADD (
  CONSTRAINT INSEBOS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.INTERF_BANDA_ORDEM_SERV ADD (
  CONSTRAINT INSEBOS_INTBAND_FK 
 FOREIGN KEY (NR_SEQ_BANDA) 
 REFERENCES TASY.INTERF_BANDA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.INTERF_BANDA_ORDEM_SERV TO NIVEL_1;

