ALTER TABLE TASY.ORDEM_COMPRA_VENC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ORDEM_COMPRA_VENC CASCADE CONSTRAINTS;

CREATE TABLE TASY.ORDEM_COMPRA_VENC
(
  NR_ORDEM_COMPRA        NUMBER(10)             NOT NULL,
  NR_ITEM_OC_VENC        NUMBER(5)              NOT NULL,
  DT_VENCIMENTO          DATE                   NOT NULL,
  VL_VENCIMENTO          NUMBER(13,2)           NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DS_OBSERVACAO          VARCHAR2(255 BYTE),
  PR_VENCIMENTO          NUMBER(13,4),
  IE_INF_CALC            VARCHAR2(1 BYTE),
  CD_CONTA_CONTABIL      VARCHAR2(20 BYTE),
  NR_TITULO_PAGAR        NUMBER(10),
  IE_GERAR_ADIANTAMENTO  VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          5M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ORCOVEN_CONCONT_FK_I ON TASY.ORDEM_COMPRA_VENC
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOVEN_CONCONT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ORCOVENC_PK ON TASY.ORDEM_COMPRA_VENC
(NR_ORDEM_COMPRA, NR_ITEM_OC_VENC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOVEN_ORDCOMP_FK_I ON TASY.ORDEM_COMPRA_VENC
(NR_ORDEM_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ORCOVEN_TITPAGA_FK_I ON TASY.ORDEM_COMPRA_VENC
(NR_TITULO_PAGAR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ORCOVEN_TITPAGA_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_VENC_AFTER_DELETE
after delete ON TASY.ORDEM_COMPRA_VENC for each row
declare

ds_retorno_w	number(10);

pragma autonomous_transaction;
begin

/* Grava o agendamento da informação para atualização do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_ordem_compra,:old.NR_ITEM_OC_VENC,'OC', :old.dt_vencimento,'E',:old.nm_usuario);


SELECT COUNT (*)
into ds_retorno_w
FROM ORDEM_COMPRA_VENC
where nr_ordem_compra = :old.nr_ordem_compra;


if(ds_retorno_w - 1 = 0) then
reagendar_item_oc_fluxo(:old.nr_ordem_compra, 'OC', 'A', :old.nm_usuario);
end if;

commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_VENC_AFTER_INSERT
AFTER INSERT ON TASY.ORDEM_COMPRA_VENC FOR EACH ROW
declare

qt_reg_w		number(10) := 0;


pragma autonomous_transaction;
BEGIN

/* Grava o agendamento da informação para atualização do fluxo de caixa. */
gravar_agend_fluxo_caixa(:new.nr_ordem_compra,:new.NR_ITEM_OC_VENC,'OC', :new.dt_vencimento,'I',:new.nm_usuario);

select count(*)
into qt_reg_w
from ORDEM_COMPRA_ITEM_ENTREGA a
where a.nr_ordem_compra = :new.nr_ordem_compra;

if(qt_reg_w > 0) then
		reagendar_item_oc_fluxo(:new.nr_ordem_compra, 'OC', 'B', :new.nm_usuario);
end if;
commit;

end;
/


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_VENC_AFTER_UPDATE
after update ON TASY.ORDEM_COMPRA_VENC for each row
begin

/* Grava o agendamento da informação para atualização do fluxo de caixa. */
gravar_agend_fluxo_caixa(:new.nr_ordem_compra,:new.NR_ITEM_OC_VENC,'OC', :new.dt_vencimento,'A',:new.nm_usuario);

end;
/


CREATE OR REPLACE TRIGGER TASY.ORDEM_COMPRA_VENC_tp  after update ON TASY.ORDEM_COMPRA_VENC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_ORDEM_COMPRA='||to_char(:old.NR_ORDEM_COMPRA)||'#@#@NR_ITEM_OC_VENC='||to_char(:old.NR_ITEM_OC_VENC);gravar_log_alteracao(substr(:old.NR_ORDEM_COMPRA,1,4000),substr(:new.NR_ORDEM_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_ORDEM_COMPRA',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ITEM_OC_VENC,1,4000),substr(:new.NR_ITEM_OC_VENC,1,4000),:new.nm_usuario,nr_seq_w,'NR_ITEM_OC_VENC',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_VENCIMENTO,1,4000),substr(:new.VL_VENCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'VL_VENCIMENTO',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERAR_ADIANTAMENTO,1,4000),substr(:new.IE_GERAR_ADIANTAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERAR_ADIANTAMENTO',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_VENCIMENTO,1,4000),substr(:new.PR_VENCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'PR_VENCIMENTO',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INF_CALC,1,4000),substr(:new.IE_INF_CALC,1,4000),:new.nm_usuario,nr_seq_w,'IE_INF_CALC',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CONTA_CONTABIL,1,4000),substr(:new.CD_CONTA_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONTA_CONTABIL',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_VENCIMENTO,1,4000),substr(:new.DT_VENCIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DT_VENCIMENTO',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_TITULO_PAGAR,1,4000),substr(:new.NR_TITULO_PAGAR,1,4000),:new.nm_usuario,nr_seq_w,'NR_TITULO_PAGAR',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'ORDEM_COMPRA_VENC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.INTPD_ORDEM_COMPRA_VENC
before insert or update ON TASY.ORDEM_COMPRA_VENC 
for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
qt_registros_w			number(10);
dt_aprovacao_w			ordem_compra.dt_aprovacao%type;

pragma autonomous_transaction;
begin

reg_integracao_p.cd_estab_documento	:= null;
reg_integracao_p.ie_tipo_ordem		:= null;
reg_integracao_p.nr_seq_tipo_compra	:= null;
reg_integracao_p.nr_seq_mod_compra	:= null;
reg_integracao_p.ds_id_origin		:= null;

select 	max(dt_aprovacao) 
into	dt_aprovacao_w
from 	ordem_compra 
where 	nr_ordem_compra = :new.nr_ordem_compra;

if	(inserting or updating)  and
	(dt_aprovacao_w is null) then
	
	begin
	select	1
	into	qt_registros_w
	from	intpd_fila_transmissao
	where	nr_seq_documento = :new.nr_ordem_compra
	and	ie_evento = '2'
	and	nvl(ie_status,'P') not in ('P','R')
	and	rownum = 1;
	exception
	when others then
		qt_registros_w := 0;
	end;

	if	(qt_registros_w > 0) then
	
		reg_integracao_p.ie_operacao		:= 'A';			
		gerar_int_padrao.gravar_integracao('2',:new.nr_ordem_compra,:new.nm_usuario, reg_integracao_p);
	end if;
end if;

commit;

end;
/


ALTER TABLE TASY.ORDEM_COMPRA_VENC ADD (
  CONSTRAINT ORCOVENC_PK
 PRIMARY KEY
 (NR_ORDEM_COMPRA, NR_ITEM_OC_VENC)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          3M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ORDEM_COMPRA_VENC ADD (
  CONSTRAINT ORCOVEN_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT ORCOVEN_ORDCOMP_FK 
 FOREIGN KEY (NR_ORDEM_COMPRA) 
 REFERENCES TASY.ORDEM_COMPRA (NR_ORDEM_COMPRA)
    ON DELETE CASCADE,
  CONSTRAINT ORCOVEN_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO_PAGAR) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO));

GRANT SELECT ON TASY.ORDEM_COMPRA_VENC TO NIVEL_1;

