ALTER TABLE TASY.PLS_MEMBRO_GRUPO_AUD
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_MEMBRO_GRUPO_AUD CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_MEMBRO_GRUPO_AUD
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  NR_SEQ_GRUPO                  NUMBER(10)      NOT NULL,
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  DT_INCLUSAO                   DATE            NOT NULL,
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  NM_USUARIO_EXEC               VARCHAR2(15 BYTE),
  NR_NIVEL_LIBERACAO            NUMBER(2),
  IE_VALOR_CALCULADO            VARCHAR2(1 BYTE),
  IE_VALOR_APRESENTADO          VARCHAR2(1 BYTE),
  IE_MELHOR_VALOR               VARCHAR2(1 BYTE),
  IE_MODIFICAR_ITEM             VARCHAR2(1 BYTE),
  IE_SUBSTITUIR_ITEM            VARCHAR2(1 BYTE),
  NR_SEQ_NIVEL_LIB              NUMBER(10),
  IE_INSERIR_ITEM               VARCHAR2(1 BYTE),
  IE_VALOR_PAGAMENTO            VARCHAR2(1 BYTE),
  IE_VALOR_FATURAMENTO          VARCHAR2(1 BYTE),
  IE_DESFAZER_ANALISE           VARCHAR2(1 BYTE),
  IE_GLOSAR                     VARCHAR2(1 BYTE),
  IE_ANALISAR                   VARCHAR2(1 BYTE),
  IE_FINALIZAR_ANALISE          VARCHAR2(1 BYTE),
  NR_SEQ_CONTRATO               NUMBER(10),
  IE_MODIFICAR_CONTA            VARCHAR2(1 BYTE),
  IE_EXCLUIR_ITEM               VARCHAR2(1 BYTE),
  IE_ENCAMINHAR_GRUPO           VARCHAR2(1 BYTE),
  IE_EXCLUIR_ANEXO              VARCHAR2(1 BYTE),
  NR_SEQ_INTERCAMBIO            NUMBER(10),
  IE_EXCLUIR_OBS                VARCHAR2(1 BYTE),
  IE_PERMITE_MOD_PARTIC         VARCHAR2(1 BYTE),
  IE_DESFAZER_SOLICITACAO_AREA  VARCHAR2(1 BYTE),
  IE_MODIFICAR_AUTORIZACAO      VARCHAR2(1 BYTE),
  IE_AJUSTAR_VALOR_LIB          VARCHAR2(1 BYTE),
  IE_LIBERAR_OC_CONTA           VARCHAR2(1 BYTE),
  IE_GRUPO_ACESSO_TOTAL         VARCHAR2(1 BYTE),
  IE_LIBERAR                    VARCHAR2(1 BYTE),
  IE_MANTER                     VARCHAR2(1 BYTE),
  IE_CANCELAR_ITEM              VARCHAR2(1 BYTE),
  IE_LIBERA_OCOR_CORRECAO       VARCHAR2(1 BYTE),
  IE_GLOSAR_ITEM_GUIA           VARCHAR2(1 BYTE),
  IE_LIBERA_ANALISE_INTERC      VARCHAR2(1 BYTE),
  IE_AUDITOR_PTU                VARCHAR2(3 BYTE),
  IE_SOLICITAR_CTA_JUST         VARCHAR2(2 BYTE),
  IE_DESFAZER_CTA_JUST          VARCHAR2(2 BYTE),
  IE_AUDITOR_RESSARCIMENTO      VARCHAR2(2 BYTE),
  IE_PERMITE_COTACAO            VARCHAR2(1 BYTE),
  IE_EXC_ITEM_REG_DESPESA       VARCHAR2(1 BYTE),
  NR_CONTRATO                   NUMBER(10),
  IE_LIBERAR_CONTAS_AUT         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSMGRA_PESFISI_FK_I ON TASY.PLS_MEMBRO_GRUPO_AUD
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSMGRA_PK ON TASY.PLS_MEMBRO_GRUPO_AUD
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSMGRA_PLSCONT_FK_I ON TASY.PLS_MEMBRO_GRUPO_AUD
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMGRA_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMGRA_PLSGRAU_FK_I ON TASY.PLS_MEMBRO_GRUPO_AUD
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMGRA_PLSGRAU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMGRA_PLSINCA_FK_I ON TASY.PLS_MEMBRO_GRUPO_AUD
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMGRA_PLSINCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMGRA_PLSNILI_FK_I ON TASY.PLS_MEMBRO_GRUPO_AUD
(NR_SEQ_NIVEL_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSMGRA_PLSNILI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSMGRA_USUARIO_FK_I ON TASY.PLS_MEMBRO_GRUPO_AUD
(NM_USUARIO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_membro_grupo_aud_insert
before insert ON TASY.PLS_MEMBRO_GRUPO_AUD for each row

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
declare
nm_auditor_w			varchar2(255);
count_w				number(10);
nr_seq_grupo_w			number(10);

pragma autonomous_transaction;
begin
nm_auditor_w	:= :new.nm_usuario_exec;
nr_seq_grupo_w	:= :new.nr_seq_grupo;

if	(:new.ie_situacao = 'A') then
	select	count(1)
	into	count_w
	from	pls_membro_grupo_aud
	where	nm_usuario_exec	= nm_auditor_w
	and	nr_seq_grupo	= nr_seq_grupo_w
	and	ie_situacao	= 'A';

	if	(count_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(218290);
	end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_MEMBRO_GRUPO_AUD_tp  after update ON TASY.PLS_MEMBRO_GRUPO_AUD FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.IE_SITUACAO,1,500);gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_MEMBRO_GRUPO_AUD',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_MEMBRO_GRUPO_AUD ADD (
  CONSTRAINT PLSMGRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_MEMBRO_GRUPO_AUD ADD (
  CONSTRAINT PLSMGRA_PLSGRAU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PLS_GRUPO_AUDITOR (NR_SEQUENCIA),
  CONSTRAINT PLSMGRA_PLSNILI_FK 
 FOREIGN KEY (NR_SEQ_NIVEL_LIB) 
 REFERENCES TASY.PLS_NIVEL_LIBERACAO (NR_SEQUENCIA),
  CONSTRAINT PLSMGRA_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSMGRA_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSMGRA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PLSMGRA_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_EXEC) 
 REFERENCES TASY.USUARIO (NM_USUARIO));

GRANT SELECT ON TASY.PLS_MEMBRO_GRUPO_AUD TO NIVEL_1;

