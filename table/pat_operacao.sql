ALTER TABLE TASY.PAT_OPERACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PAT_OPERACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PAT_OPERACAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_OPERACAO          VARCHAR2(255 BYTE)       NOT NULL,
  IE_TIPO_OPERACAO     VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ENTRADA_SAIDA     VARCHAR2(1 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PATOPER_PK ON TASY.PAT_OPERACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PAT_OPERACAO_tp  after update ON TASY.PAT_OPERACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PAT_OPERACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_OPERACAO,1,4000),substr(:new.IE_TIPO_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_OPERACAO',ie_log_w,ds_w,'PAT_OPERACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PAT_OPERACAO ADD (
  CONSTRAINT PATOPER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.PAT_OPERACAO TO NIVEL_1;

