ALTER TABLE TASY.ROP_CICLO_LAVAGEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ROP_CICLO_LAVAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.ROP_CICLO_LAVAGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_LAVAGEM           DATE                     NOT NULL,
  NR_SEQ_TURNO         NUMBER(10)               NOT NULL,
  NR_SEQ_MAQUINA       NUMBER(10)               NOT NULL,
  NR_SEQ_TIPO          NUMBER(10)               NOT NULL,
  DT_LIBERACAO         DATE,
  NM_USUARIO_LIB       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ROPCILA_ESTABEL_FK_I ON TASY.ROP_CICLO_LAVAGEM
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPCILA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.ROPCILA_PK ON TASY.ROP_CICLO_LAVAGEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ROPCILA_ROPMALA_FK_I ON TASY.ROP_CICLO_LAVAGEM
(NR_SEQ_MAQUINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPCILA_ROPMALA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPCILA_ROPTILA_FK_I ON TASY.ROP_CICLO_LAVAGEM
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPCILA_ROPTILA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ROPCILA_ROPTULA_FK_I ON TASY.ROP_CICLO_LAVAGEM
(NR_SEQ_TURNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ROPCILA_ROPTULA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.ROP_CICLO_LAVAGEM ADD (
  CONSTRAINT ROPCILA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ROP_CICLO_LAVAGEM ADD (
  CONSTRAINT ROPCILA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT ROPCILA_ROPMALA_FK 
 FOREIGN KEY (NR_SEQ_MAQUINA) 
 REFERENCES TASY.ROP_MAQUINA_LAVAGEM (NR_SEQUENCIA),
  CONSTRAINT ROPCILA_ROPTILA_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.ROP_TIPO_LAVAGEM (NR_SEQUENCIA),
  CONSTRAINT ROPCILA_ROPTULA_FK 
 FOREIGN KEY (NR_SEQ_TURNO) 
 REFERENCES TASY.ROP_TURNO_LAVAGEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.ROP_CICLO_LAVAGEM TO NIVEL_1;

