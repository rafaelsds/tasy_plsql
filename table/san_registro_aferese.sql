ALTER TABLE TASY.SAN_REGISTRO_AFERESE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SAN_REGISTRO_AFERESE CASCADE CONSTRAINTS;

CREATE TABLE TASY.SAN_REGISTRO_AFERESE
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NM_USUARIO_INI            VARCHAR2(15 BYTE),
  NM_USUARIO_FIM            VARCHAR2(15 BYTE),
  QT_ALTURA_CM              NUMBER(4,1),
  QT_PESO                   NUMBER(6,3),
  QT_FREQ_CARDIACA          NUMBER(3),
  QT_TEMP                   NUMBER(4,1),
  QT_LEUCOCITO              NUMBER(10),
  QT_PLAQUETA               NUMBER(10),
  DT_INICIO                 DATE,
  DT_FINAL                  DATE,
  NR_SEQ_AFERESE            NUMBER(10),
  PR_HEMATOCRITO            NUMBER(10,4),
  QT_HEMOGLOBINA            NUMBER(10,3),
  QT_PRESSAO_ARTERIAL       NUMBER(10),
  QT_GLOBULINAS             NUMBER(10),
  QT_ALBUMINA               NUMBER(10),
  DS_PATOLOGIA              VARCHAR2(255 BYTE),
  QT_VOLEMIA_TOTAL          NUMBER(10),
  QT_VOLEMIA_ERITROC        NUMBER(10),
  QT_VOLEMIA_PLASMATICA     NUMBER(10),
  QT_VOL_HEMACIAS           NUMBER(10),
  QT_VOL_ALBUMINADO_REP     NUMBER(10),
  QT_VOL_PLASMA             NUMBER(10),
  QT_PLASMA_REP             NUMBER(10),
  QT_HEMACIAS_REP           NUMBER(10),
  QT_VOLUME_ANTICOAGULANTE  NUMBER(10),
  QT_VOL_SF_MAQUINA         NUMBER(10),
  NR_SEQ_MAQUINA            NUMBER(10),
  QT_VOL_SOLUCAO_FISIO_REP  NUMBER(10),
  QT_KIT_LOTE               NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  CD_PESSOA_RESP            VARCHAR2(10 BYTE),
  QT_PA_DIASTOLICA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SANRGAT_PESFISI_FK_I ON TASY.SAN_REGISTRO_AFERESE
(CD_PESSOA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SANRGAT_PK ON TASY.SAN_REGISTRO_AFERESE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANRGAT_PK
  MONITORING USAGE;


CREATE INDEX TASY.SANRGAT_SANMAQU_FK_I ON TASY.SAN_REGISTRO_AFERESE
(NR_SEQ_MAQUINA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANRGAT_SANMAQU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.SANRGAT_SANSOAT_FK_I ON TASY.SAN_REGISTRO_AFERESE
(NR_SEQ_AFERESE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SANRGAT_SANSOAT_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SAN_REGISTRO_AFERESE ADD (
  CONSTRAINT SANRGAT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SAN_REGISTRO_AFERESE ADD (
  CONSTRAINT SANRGAT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT SANRGAT_SANSOAT_FK 
 FOREIGN KEY (NR_SEQ_AFERESE) 
 REFERENCES TASY.SAN_SOLIC_AFERESE_TERAP (NR_SEQUENCIA),
  CONSTRAINT SANRGAT_SANMAQU_FK 
 FOREIGN KEY (NR_SEQ_MAQUINA) 
 REFERENCES TASY.SAN_MAQUINA (NR_SEQUENCIA));

GRANT SELECT ON TASY.SAN_REGISTRO_AFERESE TO NIVEL_1;

