ALTER TABLE TASY.ESCALA_LEE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_LEE CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_LEE
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  IE_OPERACAO_ALTO_RISCO     VARCHAR2(1 BYTE)   NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  NR_SEQ_AVAL_PRE            NUMBER(10),
  IE_DOENCA_ARTERIAL         VARCHAR2(1 BYTE)   NOT NULL,
  IE_INSUFICIENCIA_CARDIACA  VARCHAR2(1 BYTE)   NOT NULL,
  IE_DOENCA_CEREBRO          VARCHAR2(1 BYTE)   NOT NULL,
  IE_CREATININA              VARCHAR2(1 BYTE)   NOT NULL,
  IE_DIABETE                 VARCHAR2(1 BYTE)   NOT NULL,
  QT_PONTUACAO               NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_HORA                    NUMBER(2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCALEE_ATCONSPEPA_FK_I ON TASY.ESCALA_LEE
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALEE_ATEPACI_FK_I ON TASY.ESCALA_LEE
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALEE_AVAPRAN_FK_I ON TASY.ESCALA_LEE
(NR_SEQ_AVAL_PRE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALEE_PESFISI_FK_I ON TASY.ESCALA_LEE
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCALEE_PK ON TASY.ESCALA_LEE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALEE_TASASDI_FK_I ON TASY.ESCALA_LEE
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCALEE_TASASDI_FK2_I ON TASY.ESCALA_LEE
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.escala_lee_atual
before insert or update ON TASY.ESCALA_LEE for each row
declare
qt_reg_w	number(1);
ie_tipo_w		varchar2(10);
nr_atendimento_w   number(10);
cd_pessoa_fisica_w varchar2(10);
begin


if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
:new.qt_pontuacao	:=	0;

if	(:new.ie_operacao_alto_risco = 'S') then
	:new.qt_pontuacao	:=	 :new.qt_pontuacao + 1;
end if;

if	(:new.ie_doenca_arterial = 'S') then
	:new.qt_pontuacao	:=	 :new.qt_pontuacao + 1;
end if;

if	(:new.ie_insuficiencia_cardiaca = 'S') then
	:new.qt_pontuacao	:=	 :new.qt_pontuacao + 1;
end if;

if	(:new.ie_doenca_cerebro = 'S') then
	:new.qt_pontuacao	:=	 :new.qt_pontuacao + 1;
end if;

if	(:new.ie_diabete = 'S') then
	:new.qt_pontuacao	:=	 :new.qt_pontuacao + 1;
end if;

if	(:new.ie_creatinina = 'S') then
	:new.qt_pontuacao	:=	 :new.qt_pontuacao + 1;
end if;

if (obter_funcao_ativa = 874) then
	select	max(c.nr_atendimento),
			max(c.cd_pessoa_fisica)
	into	nr_atendimento_w,
			cd_pessoa_fisica_w
	from	atendimento_paciente c
	where	c.nr_atendimento = :new.nr_atendimento;

	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'AEL';
	elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XAEL';
	end if;

	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;
end escala_lee_atual;
/


ALTER TABLE TASY.ESCALA_LEE ADD (
  CONSTRAINT ESCALEE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_LEE ADD (
  CONSTRAINT ESCALEE_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ESCALEE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCALEE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCALEE_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCALEE_AVAPRAN_FK 
 FOREIGN KEY (NR_SEQ_AVAL_PRE) 
 REFERENCES TASY.AVAL_PRE_ANESTESICA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCALEE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

GRANT SELECT ON TASY.ESCALA_LEE TO NIVEL_1;

