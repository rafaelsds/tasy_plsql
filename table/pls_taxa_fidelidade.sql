ALTER TABLE TASY.PLS_TAXA_FIDELIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_TAXA_FIDELIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_TAXA_FIDELIDADE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  TX_FIDELIDADE           NUMBER(15,4)          NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_INICIO_VIGENCIA      DATE                  NOT NULL,
  NR_SEQ_EVENTO           NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  DT_FIM_VIGENCIA         DATE,
  NR_SEQ_PRESTADOR        NUMBER(10),
  NR_SEQ_PRESTADOR_FIDEL  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSTAFI_ESTABEL_FK_I ON TASY.PLS_TAXA_FIDELIDADE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTAFI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSTAFI_PK ON TASY.PLS_TAXA_FIDELIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTAFI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSTAFI_PLSEVEN_FK_I ON TASY.PLS_TAXA_FIDELIDADE
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTAFI_PLSEVEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTAFI_PLSPRES_FK_I ON TASY.PLS_TAXA_FIDELIDADE
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSTAFI_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSTAFI_PLSPRES_FK2_I ON TASY.PLS_TAXA_FIDELIDADE
(NR_SEQ_PRESTADOR_FIDEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_TAXA_FIDELIDADE ADD (
  CONSTRAINT PLSTAFI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_TAXA_FIDELIDADE ADD (
  CONSTRAINT PLSTAFI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSTAFI_PLSEVEN_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.PLS_EVENTO (NR_SEQUENCIA),
  CONSTRAINT PLSTAFI_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSTAFI_PLSPRES_FK2 
 FOREIGN KEY (NR_SEQ_PRESTADOR_FIDEL) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_TAXA_FIDELIDADE TO NIVEL_1;

