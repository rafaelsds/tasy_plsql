ALTER TABLE TASY.GPT_REVALIDATION_RULE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPT_REVALIDATION_RULE CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPT_REVALIDATION_RULE
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO       NUMBER(4),
  CD_SETOR_ATENDIMENTO     NUMBER(5),
  IE_SITUACAO              VARCHAR2(1 BYTE),
  QT_DIAS_REVALIDACAO      NUMBER(3)            NOT NULL,
  DT_HORA_FIM_REVALIDACAO  DATE                 NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GPTRERU_ESTABEL_FK_I ON TASY.GPT_REVALIDATION_RULE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.GPTRERU_PK ON TASY.GPT_REVALIDATION_RULE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GPTRERU_SETATEN_FK_I ON TASY.GPT_REVALIDATION_RULE
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.GPT_REVALIDATION_RULE ADD (
  CONSTRAINT GPTRERU_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.GPT_REVALIDATION_RULE ADD (
  CONSTRAINT GPTRERU_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT GPTRERU_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.GPT_REVALIDATION_RULE TO NIVEL_1;

