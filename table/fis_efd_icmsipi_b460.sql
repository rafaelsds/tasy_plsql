ALTER TABLE TASY.FIS_EFD_ICMSIPI_B460
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FIS_EFD_ICMSIPI_B460 CASCADE CONSTRAINTS;

CREATE TABLE TASY.FIS_EFD_ICMSIPI_B460
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_REG               VARCHAR2(4 BYTE),
  IE_IND_DED           VARCHAR2(1 BYTE),
  VL_DED               NUMBER(15,2),
  NR_PROC              VARCHAR2(255 BYTE),
  IE_IND_PROC          VARCHAR2(1 BYTE),
  DS_PROC              VARCHAR2(255 BYTE),
  CD_OBS               VARCHAR2(6 BYTE),
  IE_IND_OBR           VARCHAR2(1 BYTE),
  NR_SEQ_CONTROLE      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ICMSB460_EFDICMSCON_FK_I ON TASY.FIS_EFD_ICMSIPI_B460
(NR_SEQ_CONTROLE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ICMSB460_PK ON TASY.FIS_EFD_ICMSIPI_B460
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.fis_efd_icmsipi_b460_delete
  before delete ON TASY.FIS_EFD_ICMSIPI_B460   for each row
declare
PRAGMA AUTONOMOUS_TRANSACTION;
  -- local variables here
  cd_obs_w fis_efd_icmsipi_0460.cd_obs%type;
begin

  begin
    select count(a.cd_obs)
      into cd_obs_w
      from fis_efd_icmsipi_b460 a
     where a.nr_seq_controle = :old.nr_seq_controle;
  end;

  if cd_obs_w < 2 then
    begin

      delete fis_efd_icmsipi_0460 a
      where a.cd_obs = :old.cd_obs;
    end;

    commit;

  end if;


end fis_efd_icmsipi_b460_delete;
/


CREATE OR REPLACE TRIGGER TASY.fis_efd_icmsipi_b460_insert
  before insert ON TASY.FIS_EFD_ICMSIPI_B460   for each row
declare
  -- local variables here
  cd_obs_w fis_efd_icmsipi_0460.cd_obs%type;
begin

  begin
    select max(a.cd_obs)
      into cd_obs_w
      from fis_efd_icmsipi_b460 a
     where a.nr_seq_controle = :new.nr_seq_controle;
  end;

  if cd_obs_w is null then
    begin
      select nvl(max(a.cd_obs),0) + 1
        into cd_obs_w
        from fis_efd_icmsipi_0460 a
       where a.nr_seq_controle = :new.nr_seq_controle;

      insert into fis_efd_icmsipi_0460
        (nr_sequencia,
         dt_atualizacao,
         nm_usuario,
         dt_atualizacao_nrec,
         nm_usuario_nrec,
         cd_reg,
         cd_obs,
         ds_txt,
         nr_seq_controle,
         nr_seq_superior)
      values
        (fis_efd_icmsipi_0460_seq.nextval,
         sysdate,
         :new.nm_usuario,
         sysdate,
         :new.nm_usuario_nrec,
         '0460',
         cd_obs_w,
         'Dedu��o de ISS',
         :new.nr_seq_controle,
         null);
    end;
  end if;

  :new.cd_obs := cd_obs_w;

end fis_efd_icmsipi_b460_insert;
/


ALTER TABLE TASY.FIS_EFD_ICMSIPI_B460 ADD (
  CONSTRAINT ICMSB460_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.FIS_EFD_ICMSIPI_B460 ADD (
  CONSTRAINT ICMSB460_EFDICMSCON_FK 
 FOREIGN KEY (NR_SEQ_CONTROLE) 
 REFERENCES TASY.FIS_EFD_ICMSIPI_CONTROLE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.FIS_EFD_ICMSIPI_B460 TO NIVEL_1;

