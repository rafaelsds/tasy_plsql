ALTER TABLE TASY.PLS_REGRA_PRECO_ORDEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_PRECO_ORDEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_PRECO_ORDEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  IE_TIPO_PRECO           VARCHAR2(2 BYTE)      NOT NULL,
  CD_ESTABELECIMENTO      NUMBER(4)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_APRESENTACAO     NUMBER(10),
  NM_ATRIBUTO             VARCHAR2(255 BYTE),
  IE_TIPO_TABELA          VARCHAR2(2 BYTE),
  IE_ORDEM_CLASSIFICACAO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRPOR_ESTABEL_FK_I ON TASY.PLS_REGRA_PRECO_ORDEM
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPOR_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSRPOR_PK ON TASY.PLS_REGRA_PRECO_ORDEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRPOR_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.pls_alt_pls_regra_preco_ordem
before update or insert or delete ON TASY.PLS_REGRA_PRECO_ORDEM for each row
declare
qt_registro_w	pls_integer;
begin
-- esta trigger tem apenas a finalidade de especificar na tabela pls_regra_preco_controle se as views de pre�o precisam ser geradas novamente
select	count(1)
into	qt_registro_w
from	pls_regra_preco_controle;

if	(qt_registro_w > 0) then
	update	pls_regra_preco_controle set
		ie_gera_novamente = 'S';
else
	insert into pls_regra_preco_controle (ie_gera_novamente) values ('S');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_PRECO_ORDEM_tp  after update ON TASY.PLS_REGRA_PRECO_ORDEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PLS_REGRA_PRECO_ORDEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ATRIBUTO,1,4000),substr(:new.NM_ATRIBUTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ATRIBUTO',ie_log_w,ds_w,'PLS_REGRA_PRECO_ORDEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRESENTACAO,1,4000),substr(:new.NR_SEQ_APRESENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENTACAO',ie_log_w,ds_w,'PLS_REGRA_PRECO_ORDEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_PRECO,1,4000),substr(:new.IE_TIPO_PRECO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_PRECO',ie_log_w,ds_w,'PLS_REGRA_PRECO_ORDEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_REGRA_PRECO_ORDEM ADD (
  CONSTRAINT PLSRPOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_PRECO_ORDEM ADD (
  CONSTRAINT PLSRPOR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_REGRA_PRECO_ORDEM TO NIVEL_1;

