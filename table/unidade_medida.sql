ALTER TABLE TASY.UNIDADE_MEDIDA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.UNIDADE_MEDIDA CASCADE CONSTRAINTS;

CREATE TABLE TASY.UNIDADE_MEDIDA
(
  CD_UNIDADE_MEDIDA     VARCHAR2(30 BYTE)       NOT NULL,
  DS_UNIDADE_MEDIDA     VARCHAR2(40 BYTE)       NOT NULL,
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  CD_UNIDADE_MED_PRINC  VARCHAR2(30 BYTE),
  CD_UNIDADE_MED_SEC    VARCHAR2(30 BYTE),
  QT_PADRAO             NUMBER(15,4),
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_FRACAO_DOSE        VARCHAR2(1 BYTE),
  CD_UNID_MED_PROCESSO  VARCHAR2(30 BYTE),
  IE_PERMITE_FRACIONAR  VARCHAR2(1 BYTE),
  NR_SEQ_APRESENTACAO   NUMBER(10),
  IE_ADM_DILUICAO       VARCHAR2(1 BYTE)        NOT NULL,
  CD_SISTEMA_ANT        VARCHAR2(255 BYTE),
  CD_UNIDADE_PTU        VARCHAR2(30 BYTE),
  IE_MULT_H_APLIC       VARCHAR2(1 BYTE),
  IE_UNIDADE_ADM        VARCHAR2(1 BYTE),
  NR_SEQ_MEDIDA_TISS    NUMBER(10),
  NR_UNIDADE_PTU        VARCHAR2(5 BYTE),
  IE_UNIDADE_MED_INTER  VARCHAR2(15 BYTE),
  IE_SOLICITA_TOP_ADM   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.UNIDADE_MEDIDA.CD_UNIDADE_MEDIDA IS 'C�digo da Unidade de Medida.';

COMMENT ON COLUMN TASY.UNIDADE_MEDIDA.DS_UNIDADE_MEDIDA IS 'Descri�ao da Unidade de Medida.';

COMMENT ON COLUMN TASY.UNIDADE_MEDIDA.IE_SITUACAO IS 'Indicador da situacao da Unidade de Medida';


CREATE INDEX TASY.UNIMEDI_I1 ON TASY.UNIDADE_MEDIDA
(IE_SITUACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIMEDI_I2 ON TASY.UNIDADE_MEDIDA
(UPPER("IE_UNIDADE_MED_INTER"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.UNIMEDI_PK ON TASY.UNIDADE_MEDIDA
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIMEDI_TISSUNM_FK_I ON TASY.UNIDADE_MEDIDA
(NR_SEQ_MEDIDA_TISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIMEDI_UNIMEDI_FK_I ON TASY.UNIDADE_MEDIDA
(CD_UNID_MED_PROCESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.UNIMEDI_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.UNIMEDI_UNIMEPR_FK_I ON TASY.UNIDADE_MEDIDA
(CD_UNIDADE_MED_PRINC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.UNIMEDI_UNIMESE_FK_I ON TASY.UNIDADE_MEDIDA
(CD_UNIDADE_MED_SEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.UNIDADE_MEDIDA_tp  after update ON TASY.UNIDADE_MEDIDA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_UNIDADE_MEDIDA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_FRACAO_DOSE,1,4000),substr(:new.IE_FRACAO_DOSE,1,4000),:new.nm_usuario,nr_seq_w,'IE_FRACAO_DOSE',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_UNIDADE_PTU,1,4000),substr(:new.NR_UNIDADE_PTU,1,4000),:new.nm_usuario,nr_seq_w,'NR_UNIDADE_PTU',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MED_SEC,1,4000),substr(:new.CD_UNIDADE_MED_SEC,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MED_SEC',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_PADRAO,1,4000),substr(:new.QT_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_PADRAO',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MEDIDA,1,4000),substr(:new.CD_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MEDIDA',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_UNIDADE_MEDIDA,1,4000),substr(:new.DS_UNIDADE_MEDIDA,1,4000),:new.nm_usuario,nr_seq_w,'DS_UNIDADE_MEDIDA',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MED_PROCESSO,1,4000),substr(:new.CD_UNID_MED_PROCESSO,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_PROCESSO',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_FRACIONAR,1,4000),substr(:new.IE_PERMITE_FRACIONAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_FRACIONAR',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRESENTACAO,1,4000),substr(:new.NR_SEQ_APRESENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENTACAO',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ADM_DILUICAO,1,4000),substr(:new.IE_ADM_DILUICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ADM_DILUICAO',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANT,1,4000),substr(:new.CD_SISTEMA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANT',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_PTU,1,4000),substr(:new.CD_UNIDADE_PTU,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_PTU',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MULT_H_APLIC,1,4000),substr(:new.IE_MULT_H_APLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_MULT_H_APLIC',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_UNIDADE_ADM,1,4000),substr(:new.IE_UNIDADE_ADM,1,4000),:new.nm_usuario,nr_seq_w,'IE_UNIDADE_ADM',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MEDIDA_TISS,1,4000),substr(:new.NR_SEQ_MEDIDA_TISS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MEDIDA_TISS',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNIDADE_MED_PRINC,1,4000),substr(:new.CD_UNIDADE_MED_PRINC,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNIDADE_MED_PRINC',ie_log_w,ds_w,'UNIDADE_MEDIDA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.unidade_medida_after
after insert or update ON TASY.UNIDADE_MEDIDA for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
begin
reg_integracao_p.cd_estab_documento	:= wheb_usuario_pck.get_cd_estabelecimento;

if	(inserting) then
	reg_integracao_p.ie_operacao	:=	'I';
elsif	(updating) then
	reg_integracao_p.ie_operacao	:=	'A';
end if;

gerar_int_padrao.gravar_integracao('5', :new.cd_unidade_medida, :new.nm_usuario, reg_integracao_p);
end;
/


ALTER TABLE TASY.UNIDADE_MEDIDA ADD (
  CHECK ( ie_situacao IN ( 'A' , 'I' )  ),
  CONSTRAINT UNIMEDI_PK
 PRIMARY KEY
 (CD_UNIDADE_MEDIDA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.UNIDADE_MEDIDA ADD (
  CONSTRAINT UNIMEDI_TISSUNM_FK 
 FOREIGN KEY (NR_SEQ_MEDIDA_TISS) 
 REFERENCES TASY.TISS_UNIDADE_MEDIDA (NR_SEQUENCIA),
  CONSTRAINT UNIMEDI_UNIMEPR_FK 
 FOREIGN KEY (CD_UNIDADE_MED_PRINC) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT UNIMEDI_UNIMESE_FK 
 FOREIGN KEY (CD_UNIDADE_MED_SEC) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT UNIMEDI_UNIMEDI_FK 
 FOREIGN KEY (CD_UNID_MED_PROCESSO) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA));

GRANT SELECT ON TASY.UNIDADE_MEDIDA TO NIVEL_1;

