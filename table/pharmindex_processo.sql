ALTER TABLE TASY.PHARMINDEX_PROCESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PHARMINDEX_PROCESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PHARMINDEX_PROCESSO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_AUTENTICACAO      VARCHAR2(255 BYTE)       NOT NULL,
  DT_INICIAL           DATE,
  DT_FINAL             DATE,
  CD_MATERIAL_EXTERNO  VARCHAR2(255 BYTE),
  IE_ATC               VARCHAR2(1 BYTE)         NOT NULL,
  IE_EMPRESA           VARCHAR2(1 BYTE)         NOT NULL,
  IE_PAG_DOENCA        VARCHAR2(1 BYTE)         NOT NULL,
  IE_DOENCA            VARCHAR2(1 BYTE)         NOT NULL,
  IE_MEDIC             VARCHAR2(1 BYTE)         NOT NULL,
  IE_FORM_MEDIC        VARCHAR2(1 BYTE)         NOT NULL,
  IE_PRESCR            VARCHAR2(1 BYTE)         NOT NULL,
  IE_STATUS            VARCHAR2(1 BYTE)         NOT NULL,
  IE_SUBSTANCIA        VARCHAR2(1 BYTE)         NOT NULL,
  DT_LIBERACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PHDXPRO_PK ON TASY.PHARMINDEX_PROCESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pharmindex_processo_after
after insert or update ON TASY.PHARMINDEX_PROCESSO for each row
declare

reg_integracao_p		gerar_int_padrao.reg_integracao;
ie_integrar_w		varchar2(1);
begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S') then
	begin
	if	(updating) and
		(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
		ie_integrar_w	:=	'S';
	elsif	(inserting) and
		(:new.dt_liberacao is not null) then
		ie_integrar_w	:=	'S';
	end if;

	if	(ie_integrar_w = 'S') then
		begin
		reg_integracao_p.ie_operacao		:=	'I';

		if	(:new.ie_atc = 'S') then
			gerar_int_padrao.gravar_integracao('411', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;

		if	(:new.ie_empresa = 'S') then
			gerar_int_padrao.gravar_integracao('412', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;

		if	(:new.ie_pag_doenca = 'S') then
			gerar_int_padrao.gravar_integracao('413', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;

		if	(:new.ie_doenca = 'S') then
			gerar_int_padrao.gravar_integracao('414', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;

		if	(:new.ie_medic = 'S') then
			if	(:new.cd_material_externo is null) then
				gerar_int_padrao.gravar_integracao('415', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
			else
				gerar_int_padrao.gravar_integracao('416', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
			end if;
		end if;

		if	(:new.ie_form_medic = 'S') then
			gerar_int_padrao.gravar_integracao('417', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;

		if	(:new.ie_prescr = 'S') then
			gerar_int_padrao.gravar_integracao('418', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;

		if	(:new.ie_status = 'S') then
			gerar_int_padrao.gravar_integracao('419', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;

		if	(:new.ie_substancia = 'S') then
			gerar_int_padrao.gravar_integracao('420', :new.nr_sequencia, nvl(obter_usuario_ativo, nvl(:new.nm_usuario, :old.nm_usuario)), reg_integracao_p);
		end if;
		end;
	end if;
	end;
end if;

end pharmindex_processo_after;
/


ALTER TABLE TASY.PHARMINDEX_PROCESSO ADD (
  CONSTRAINT PHDXPRO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.PHARMINDEX_PROCESSO TO NIVEL_1;

