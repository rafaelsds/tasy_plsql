ALTER TABLE TASY.PROTOCOLO_LOCAL_QUIMIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_LOCAL_QUIMIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_LOCAL_QUIMIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_LOCAIS            NUMBER(10)               NOT NULL,
  CD_PROTOCOLO         NUMBER(10)               NOT NULL,
  NR_SEQ_MEDICACAO     NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PTLCQMO_ESTABEL_FK_I ON TASY.PROTOCOLO_LOCAL_QUIMIO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTLCQMO_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PTLCQMO_PK ON TASY.PROTOCOLO_LOCAL_QUIMIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PTLCQMO_PROMEDI_FK_I ON TASY.PROTOCOLO_LOCAL_QUIMIO
(CD_PROTOCOLO, NR_SEQ_MEDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTLCQMO_PROMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PTLCQMO_PROTOCO_FK_I ON TASY.PROTOCOLO_LOCAL_QUIMIO
(CD_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PTLCQMO_PROTOCO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROTOCOLO_LOCAL_QUIMIO ADD (
  CONSTRAINT PTLCQMO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROTOCOLO_LOCAL_QUIMIO ADD (
  CONSTRAINT PTLCQMO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PTLCQMO_PROTOCO_FK 
 FOREIGN KEY (CD_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO (CD_PROTOCOLO),
  CONSTRAINT PTLCQMO_PROMEDI_FK 
 FOREIGN KEY (CD_PROTOCOLO, NR_SEQ_MEDICACAO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (CD_PROTOCOLO,NR_SEQUENCIA));

GRANT SELECT ON TASY.PROTOCOLO_LOCAL_QUIMIO TO NIVEL_1;

