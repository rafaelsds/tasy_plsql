ALTER TABLE TASY.REG_PROC_EXAME_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REG_PROC_EXAME_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.REG_PROC_EXAME_RESULT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NR_SEQ_EXAME           NUMBER(10)             NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  IE_FORMATO_RESULTADO   VARCHAR2(2 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  QT_MINIMA              NUMBER(15,4),
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  QT_MAXIMA              NUMBER(15,4),
  QT_PERCENT_MIN         NUMBER(15,4),
  QT_PERCENT_MAX         NUMBER(15,4),
  DS_RESULTADO           VARCHAR2(2000 BYTE),
  IE_PERMITE_JUSTIFICAR  VARCHAR2(1 BYTE),
  DS_MENSAGEM            VARCHAR2(2000 BYTE),
  NR_SEQ_PROC_RESULT     NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGEXR_EXALABO_FK_I ON TASY.REG_PROC_EXAME_RESULT
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGEXR_PK ON TASY.REG_PROC_EXAME_RESULT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGEXR_PREXRES_FK_I ON TASY.REG_PROC_EXAME_RESULT
(NR_SEQ_PROC_RESULT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REG_PROC_EXAME_RESULT ADD (
  CONSTRAINT REGEXR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REG_PROC_EXAME_RESULT ADD (
  CONSTRAINT REGEXR_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT REGEXR_PREXRES_FK 
 FOREIGN KEY (NR_SEQ_PROC_RESULT) 
 REFERENCES TASY.PROC_EXAME_LAB_RESULT (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.REG_PROC_EXAME_RESULT TO NIVEL_1;

