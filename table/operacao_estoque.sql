ALTER TABLE TASY.OPERACAO_ESTOQUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.OPERACAO_ESTOQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.OPERACAO_ESTOQUE
(
  CD_OPERACAO_ESTOQUE          NUMBER(3)        NOT NULL,
  DS_OPERACAO                  VARCHAR2(40 BYTE) NOT NULL,
  IE_ENTRADA_SAIDA             VARCHAR2(1 BYTE) NOT NULL,
  IE_PERMITE_DIGITACAO         VARCHAR2(1 BYTE) NOT NULL,
  IE_ATUALIZA_ESTOQUE          VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO_REQUISICAO           VARCHAR2(3 BYTE) NOT NULL,
  IE_SITUACAO                  VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  CD_OPERACAO_CORRESPONDENTE   NUMBER(3),
  IE_ALTERA_CUSTO              VARCHAR2(1 BYTE),
  IE_MOTIVO_DEVOLUCAO          VARCHAR2(1 BYTE),
  IE_MOTIVO_AJUSTE             VARCHAR2(1 BYTE),
  IE_VALOR                     VARCHAR2(1 BYTE),
  IE_CONSUMO                   VARCHAR2(1 BYTE),
  IE_CONSIGNADO                VARCHAR2(1 BYTE),
  CD_TIPO_LOTE_CONTABIL        NUMBER(10),
  CD_EVENTO                    NUMBER(10),
  IE_COLUNA_CONTROLADO         NUMBER(2),
  IE_COLUNA_RESUMO             VARCHAR2(1 BYTE),
  CD_CONVENIO                  NUMBER(5),
  CD_CATEGORIA                 VARCHAR2(10 BYTE),
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE),
  IE_TIPO_PERDA                VARCHAR2(3 BYTE),
  IE_DESCONSIDERA_CONS_RESSUP  VARCHAR2(1 BYTE),
  IE_DESCONSIDERA_CONS_PADRAO  VARCHAR2(1 BYTE),
  CD_OPERACAO_INT              VARCHAR2(10 BYTE),
  IE_CONTAB_CONSIGNADO         VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON COLUMN TASY.OPERACAO_ESTOQUE.CD_OPERACAO_ESTOQUE IS 'Codigo da Operacao de Estoque';

COMMENT ON COLUMN TASY.OPERACAO_ESTOQUE.DS_OPERACAO IS 'Descricao da Operacao de Estoque';

COMMENT ON COLUMN TASY.OPERACAO_ESTOQUE.IE_ENTRADA_SAIDA IS 'Identificador de Entrada ou Saida';

COMMENT ON COLUMN TASY.OPERACAO_ESTOQUE.IE_PERMITE_DIGITACAO IS 'Identificador se pode usar a operacao na digitacao de movim';

COMMENT ON COLUMN TASY.OPERACAO_ESTOQUE.IE_ATUALIZA_ESTOQUE IS 'Identificador se Operacao atualiza Estoque';

COMMENT ON COLUMN TASY.OPERACAO_ESTOQUE.IE_TIPO_REQUISICAO IS 'Identifica o tipo de requisicao(saida, ajuste, devol, trans)';

COMMENT ON COLUMN TASY.OPERACAO_ESTOQUE.DT_ATUALIZACAO IS 'Data de Atualizacao';

COMMENT ON COLUMN TASY.OPERACAO_ESTOQUE.NM_USUARIO IS 'Nome do Usuario';

COMMENT ON COLUMN TASY.OPERACAO_ESTOQUE.CD_OPERACAO_CORRESPONDENTE IS 'Codigo da Operacao correspondente(transferencia, producao)';


CREATE INDEX TASY.OPEESTO_CATCONV_FK_I ON TASY.OPERACAO_ESTOQUE
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OPEESTO_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OPEESTO_EVECONT_FK_I ON TASY.OPERACAO_ESTOQUE
(CD_TIPO_LOTE_CONTABIL, CD_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OPEESTO_EVECONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.OPEESTO_OPEESTO_FK_I ON TASY.OPERACAO_ESTOQUE
(CD_OPERACAO_CORRESPONDENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.OPEESTO_OPEESTO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.OPEESTO_PK ON TASY.OPERACAO_ESTOQUE
(CD_OPERACAO_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.OPERACAO_ESTOQUE_tp  after update ON TASY.OPERACAO_ESTOQUE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_OPERACAO_ESTOQUE);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CONSIGNADO,1,4000),substr(:new.IE_CONSIGNADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIGNADO',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_OPERACAO_ESTOQUE,1,4000),substr(:new.CD_OPERACAO_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_OPERACAO_ESTOQUE',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OPERACAO,1,4000),substr(:new.DS_OPERACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OPERACAO',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENTRADA_SAIDA,1,4000),substr(:new.IE_ENTRADA_SAIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENTRADA_SAIDA',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PERMITE_DIGITACAO,1,4000),substr(:new.IE_PERMITE_DIGITACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_PERMITE_DIGITACAO',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSUMO,1,4000),substr(:new.IE_CONSUMO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSUMO',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DESCONSIDERA_CONS_PADRAO,1,4000),substr(:new.IE_DESCONSIDERA_CONS_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESCONSIDERA_CONS_PADRAO',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ALTERA_CUSTO,1,4000),substr(:new.IE_ALTERA_CUSTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ALTERA_CUSTO',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_REQUISICAO,1,4000),substr(:new.IE_TIPO_REQUISICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_REQUISICAO',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_OPERACAO_CORRESPONDENTE,1,4000),substr(:new.CD_OPERACAO_CORRESPONDENTE,1,4000),:new.nm_usuario,nr_seq_w,'CD_OPERACAO_CORRESPONDENTE',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COLUNA_RESUMO,1,4000),substr(:new.IE_COLUNA_RESUMO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COLUNA_RESUMO',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DESCONSIDERA_CONS_RESSUP,1,4000),substr(:new.IE_DESCONSIDERA_CONS_RESSUP,1,4000),:new.nm_usuario,nr_seq_w,'IE_DESCONSIDERA_CONS_RESSUP',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZA_ESTOQUE,1,4000),substr(:new.IE_ATUALIZA_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZA_ESTOQUE',ie_log_w,ds_w,'OPERACAO_ESTOQUE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.operacao_estoque_atual
before insert or update ON TASY.OPERACAO_ESTOQUE for each row
declare

qt_movto_w	number(20);

begin
select	count(*)
into	qt_movto_w
from	movimento_estoque
where	cd_operacao_estoque = :new.cd_operacao_estoque;

if	(qt_movto_w > 0) then
	begin
	if	(:old.ie_entrada_saida <> :new.ie_entrada_saida) then
		wheb_mensagem_pck.exibir_mensagem_abort(181825,'DS_CAMPO=Entrada/Sa�da');
	end if;

	if	(:old.ie_atualiza_estoque <> :new.ie_atualiza_estoque) then
		wheb_mensagem_pck.exibir_mensagem_abort(181825,'DS_CAMPO=Atualiza Estoque Cont�bil');
	end if;

	if	(nvl(:old.ie_consignado,0) <> nvl(:new.ie_consignado,0)) then
		wheb_mensagem_pck.exibir_mensagem_abort(181825,'DS_CAMPO=Tipo consignado');
	end if;

	if	(:old.ie_tipo_requisicao <> :new.ie_tipo_requisicao) then
		wheb_mensagem_pck.exibir_mensagem_abort(181825,'DS_CAMPO=Tipo opera��o');
	end if;
	end;
end if;

if	(:new.ie_desconsidera_cons_ressup is null) then
	:new.ie_desconsidera_cons_ressup := 'N';
end if;

if	(nvl(:new.ie_situacao,'A') = 'A') then
	begin
	if	(:new.ie_entrada_saida = 'E') then
		begin
		if	(nvl(:new.ie_consignado,0) in (5,8)) then
			wheb_mensagem_pck.exibir_mensagem_abort(179789);
			/*r.aise_application_error(-20011,'A opera��o de consignado n�o � de entrada ');*/
		end if;
		if	(nvl(:new.ie_tipo_requisicao,0) in (1,4,9,0)) then
			wheb_mensagem_pck.exibir_mensagem_abort(179794);
			/*r.aise_application_error(-20011,'Estas opera��es s�o de saida (N�o combinam com o tipo de opera��o) ');*/
		end if;
		end;
	else
		begin
		if	(nvl(:new.ie_consignado,0) in (1,2,3)) then
			wheb_mensagem_pck.exibir_mensagem_abort(179795);
			/*r.aise_application_error(-20011,'A opera��o de consignado n�o � de entrada ');*/
		end if;
		if	(nvl(:new.ie_tipo_requisicao,0) in (6,7,3)) then
			wheb_mensagem_pck.exibir_mensagem_abort(179796);
			/*r.aise_application_error(-20011,'Estas opera��es s�o de entrada (N�o combinam com o tipo de opera��o)');*/
		end if;
		end;
	end if;

	if	(nvl(:new.ie_tipo_requisicao,0) in (2,21))  and
		(:new.cd_operacao_correspondente is null) then
		wheb_mensagem_pck.exibir_mensagem_abort(179797);
		/*r.aise_application_error(-20011,'Para Opera��o de Transfer�ncia � necess�rio informar Opera��o Corresp. !');*/
	end if;

	if	(:new.cd_operacao_correspondente is not null) and
		(nvl(:new.ie_tipo_requisicao,0) not in (2,21))  then
		wheb_mensagem_pck.exibir_mensagem_abort(179798);
		/*r.aise_application_error(-20011,'Este Tipo de Opera��o n�o pode ter Opera��o Corresp. !');*/
	end if;

	if	(nvl(:new.ie_tipo_requisicao,0) = 2)  and
		(nvl(:new.ie_consignado,0) not in (0,6)) then
		wheb_mensagem_pck.exibir_mensagem_abort(179799);
		/*r.aise_application_error(-20011,'Tipo Opera��o e Tipo Consignado n�o conferem !');*/

	end if;

	if	(nvl(:new.ie_consignado,0) = 6) and
		(nvl(:new.ie_tipo_requisicao,2) not in (2,21))  then
		wheb_mensagem_pck.exibir_mensagem_abort(179799);
		/*r.aise_application_error(-20011,'Tipo Opera��o e Tipo Consignado n�o conferem !');*/
	end if;

	if	(nvl(:new.ie_consignado,0) <> 0) and
		(:new.ie_atualiza_estoque = 'S') then
		wheb_mensagem_pck.exibir_mensagem_abort(179801);
		/*r.aise_application_error(-20011,'As Opera��es de consignado n�o devem atualizar estoque normal ');*/
	end if;
	end;
end if;
end;
/


ALTER TABLE TASY.OPERACAO_ESTOQUE ADD (
  CONSTRAINT OPEESTO_PK
 PRIMARY KEY
 (CD_OPERACAO_ESTOQUE)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.OPERACAO_ESTOQUE ADD (
  CONSTRAINT OPEESTO_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT OPEESTO_EVECONT_FK 
 FOREIGN KEY (CD_TIPO_LOTE_CONTABIL, CD_EVENTO) 
 REFERENCES TASY.EVENTO_CONTABIL (CD_TIPO_LOTE_CONTABIL,CD_EVENTO),
  CONSTRAINT OPEESTO_OPEESTO_FK 
 FOREIGN KEY (CD_OPERACAO_CORRESPONDENTE) 
 REFERENCES TASY.OPERACAO_ESTOQUE (CD_OPERACAO_ESTOQUE));

GRANT SELECT ON TASY.OPERACAO_ESTOQUE TO NIVEL_1;

