ALTER TABLE TASY.MAT_FABRICANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAT_FABRICANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAT_FABRICANTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DS_FABRICANTE        VARCHAR2(80 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_SISTEMA_ANT       VARCHAR2(80 BYTE),
  PR_DESPESA_VENDA     NUMBER(5,2),
  IE_INTERNACIONAL     VARCHAR2(1 BYTE),
  CD_PESSOA_JURIDICA   VARCHAR2(14 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATFABR_PESJURI_FK_I ON TASY.MAT_FABRICANTE
(CD_PESSOA_JURIDICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATFABR_PK ON TASY.MAT_FABRICANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MAT_FABRICANTE_tp  after update ON TASY.MAT_FABRICANTE FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_FABRICANTE,1,4000),substr(:new.DS_FABRICANTE,1,4000),:new.nm_usuario,nr_seq_w,'DS_FABRICANTE',ie_log_w,ds_w,'MAT_FABRICANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.PR_DESPESA_VENDA,1,4000),substr(:new.PR_DESPESA_VENDA,1,4000),:new.nm_usuario,nr_seq_w,'PR_DESPESA_VENDA',ie_log_w,ds_w,'MAT_FABRICANTE',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SISTEMA_ANT,1,4000),substr(:new.CD_SISTEMA_ANT,1,4000),:new.nm_usuario,nr_seq_w,'CD_SISTEMA_ANT',ie_log_w,ds_w,'MAT_FABRICANTE',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MAT_FABRICANTE ADD (
  CONSTRAINT MATFABR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAT_FABRICANTE ADD (
  CONSTRAINT MATFABR_PESJURI_FK 
 FOREIGN KEY (CD_PESSOA_JURIDICA) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.MAT_FABRICANTE TO NIVEL_1;

