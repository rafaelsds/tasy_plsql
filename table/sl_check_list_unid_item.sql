ALTER TABLE TASY.SL_CHECK_LIST_UNID_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SL_CHECK_LIST_UNID_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.SL_CHECK_LIST_UNID_ITEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_CHECKLIST        NUMBER(10)            NOT NULL,
  IE_RESULTADO            VARCHAR2(1 BYTE),
  NR_SEQ_ITEM_CHECK_LIST  NUMBER(10)            NOT NULL,
  DS_OBSERVACAO           VARCHAR2(500 BYTE),
  IE_NAO_APLICA           VARCHAR2(1 BYTE),
  IE_RESULT_ITEM          VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SLCHLUI_PK ON TASY.SL_CHECK_LIST_UNID_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SLCHLUI_SLCHLIU_FK_I ON TASY.SL_CHECK_LIST_UNID_ITEM
(NR_SEQ_CHECKLIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SL_CHECK_LIST_UNID_ITEM_ATUAL
before insert or update ON TASY.SL_CHECK_LIST_UNID_ITEM for each row
declare

cd_estabelecimento_w		number(04,0);

begin
if	(:new.ie_result_item is not null) and
	(nvl(:old.ie_result_item,'X')) <> (nvl(:new.ie_result_item,'X')) then
	if	(:new.ie_result_item = 'F') then
		:new.ie_resultado	:= 'S';
		:new.ie_nao_aplica	:= 'N';
	elsif	(:new.ie_result_item = 'N') then
		:new.ie_resultado	:= 'N';
		:new.ie_nao_aplica	:= 'S';
	else
		:new.ie_resultado	:= 'N';
		:new.ie_nao_aplica	:= 'N';
		/*if	(:new.ie_result_item = 'C') then
			:new.ds_observacao	:= substr(:new.ds_observacao || '-' || WHEB_MENSAGEM_PCK.get_Texto(839028),1,500);
		end if;*/
	end if;
elsif	(:new.ie_resultado is not null) or (:new.ie_nao_aplica is not null) and
		nvl(:old.ie_resultado,'X') <> nvl(:new.ie_resultado,'X') then
	if	(nvl(:new.ie_resultado,'N')	= 'S') then
		:new.ie_result_item := 'F';
	elsif	(nvl(:new.ie_resultado,'N')	= 'N') then
		if	(:new.ie_nao_aplica	= 'S') then
			:new.ie_result_item	:= 'N';
		else
			:new.ie_result_item	:= 'P';
		end if;
	end if;
end if;

end;
/


ALTER TABLE TASY.SL_CHECK_LIST_UNID_ITEM ADD (
  CONSTRAINT SLCHLUI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SL_CHECK_LIST_UNID_ITEM ADD (
  CONSTRAINT SLCHLUI_SLCHLIU_FK 
 FOREIGN KEY (NR_SEQ_CHECKLIST) 
 REFERENCES TASY.SL_CHECK_LIST_UNID (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SL_CHECK_LIST_UNID_ITEM TO NIVEL_1;

