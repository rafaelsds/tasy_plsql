ALTER TABLE TASY.W_IMPORTA_CLIENTES
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_IMPORTA_CLIENTES CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_IMPORTA_CLIENTES
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  IE_TIPO_PESSOA          VARCHAR2(15 BYTE),
  NR_CPF                  VARCHAR2(11 BYTE),
  CD_CNPJ                 VARCHAR2(14 BYTE),
  NM_CLIENTE              VARCHAR2(255 BYTE),
  DT_NASCIMENTO           DATE,
  DS_ORGAO_EMISSOR_CI     VARCHAR2(40 BYTE),
  DT_EMISSAO_CI           DATE,
  NR_DOCUMENTO            VARCHAR2(30 BYTE),
  NR_INSCRICAO_MUNICIPAL  VARCHAR2(20 BYTE),
  DS_ENDERECO             VARCHAR2(80 BYTE),
  NR_ENDERECO             VARCHAR2(5 BYTE),
  DS_BAIRRO               VARCHAR2(80 BYTE),
  DS_COMPLEMENTO          VARCHAR2(80 BYTE),
  CD_CEP                  VARCHAR2(8 BYTE),
  DS_MUNICIPIO            VARCHAR2(80 BYTE),
  SG_ESTADO               VARCHAR2(2 BYTE),
  CD_PAIS                 VARCHAR2(20 BYTE),
  DS_EMAIL                VARCHAR2(255 BYTE),
  NR_TELEFONE             VARCHAR2(40 BYTE),
  NR_FAX                  VARCHAR2(40 BYTE),
  IE_ESTADO_CIVIL         VARCHAR2(15 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_COMPL       VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WIMPCLI_PK ON TASY.W_IMPORTA_CLIENTES
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_IMPORTA_CLIENTES ADD (
  CONSTRAINT WIMPCLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_IMPORTA_CLIENTES TO NIVEL_1;

