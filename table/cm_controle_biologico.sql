ALTER TABLE TASY.CM_CONTROLE_BIOLOGICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CM_CONTROLE_BIOLOGICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CM_CONTROLE_BIOLOGICO
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  DT_ATUALIZACAO      DATE                      NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  NM_USUARIO_INICIO   VARCHAR2(15 BYTE)         NOT NULL,
  IE_POSICAO_PACOTE   VARCHAR2(3 BYTE)          NOT NULL,
  IE_RESULTADO        VARCHAR2(1 BYTE),
  HR_INICIO           DATE                      NOT NULL,
  HR_FIM              DATE,
  CD_ESTABELECIMENTO  NUMBER(4)                 NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CMCONBI_ESTABEL_FK_I ON TASY.CM_CONTROLE_BIOLOGICO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCONBI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CMCONBI_PK ON TASY.CM_CONTROLE_BIOLOGICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CMCONBI_PK
  MONITORING USAGE;


ALTER TABLE TASY.CM_CONTROLE_BIOLOGICO ADD (
  CONSTRAINT CMCONBI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CM_CONTROLE_BIOLOGICO ADD (
  CONSTRAINT CMCONBI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CMCONBI_CMCICLO_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.CM_CICLO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CM_CONTROLE_BIOLOGICO TO NIVEL_1;

