DROP TABLE TASY.LOG_ENVIO_EMAIL_TEMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOG_ENVIO_EMAIL_TEMP
(
  DS_LOG              VARCHAR2(255 BYTE),
  CD_ESTABELECIMENTO  NUMBER(4),
  IE_TIPO_MENSAGEM    VARCHAR2(15 BYTE),
  DT_ATUALIZACAO      DATE,
  NM_USUARIO          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE OR REPLACE TRIGGER TASY.log_envio_email_temp_afinsert
after insert ON TASY.LOG_ENVIO_EMAIL_TEMP for each row
declare
qt_maximo_w			number(10);
qt_pendente_w		number(10);
ie_email_dia_util_w	varchar2(2);
qt_registros_w		number(10);

begin

ie_email_dia_util_w := 'N';

select	nvl(max(qt_maximo_pendente),0)
into	qt_maximo_w
from	regra_envio_compra_agrup
where	cd_estabelecimento = :new.cd_estabelecimento
and	ie_tipo_mensagem = :new.ie_tipo_mensagem;

select	count(*)
into	qt_registros_w
from	regra_envio_email_compra
where	cd_estabelecimento = :new.cd_estabelecimento
and	ie_tipo_mensagem = :new.ie_tipo_mensagem
and	ie_email_dia_util = 'S'
and	ie_situacao = 'A';

if	(qt_registros_w > 0) then
	ie_email_dia_util_w := 'S';
end if;

if	(nvl(qt_maximo_w,0) >= 0) then
	begin

	select	count(distinct a.nr_sequencia)
	into	qt_pendente_w
	from	envio_email_compra_agrup a,
		email_compra_agrup_dest x
	where	x.nr_seq_envio = a.nr_sequencia
	and	cd_estabelecimento = :new.cd_estabelecimento
	and	nvl(a.ie_cancelado,'N') = 'N'
	and	nvl(x.ie_enviado,'N') = 'N';

	if	(qt_pendente_w >= qt_maximo_w) then
		begin
		if	(ie_email_dia_util_w = 'S') then
			if (obter_se_dia_util(sysdate, :new.cd_estabelecimento) = 'S')then
				envia_email_compra_agrupado(:new.ie_tipo_mensagem, 'N', :new.cd_estabelecimento);
			end if;
		else
			envia_email_compra_agrupado(:new.ie_tipo_mensagem, 'N', :new.cd_estabelecimento);
		end if;
		end;
	end if;

	end;
end if;

end;
/


GRANT SELECT ON TASY.LOG_ENVIO_EMAIL_TEMP TO NIVEL_1;

