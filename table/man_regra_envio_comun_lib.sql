ALTER TABLE TASY.MAN_REGRA_ENVIO_COMUN_LIB
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_REGRA_ENVIO_COMUN_LIB CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_REGRA_ENVIO_COMUN_LIB
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_REGRA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PERFIL              NUMBER(5),
  NR_SEQ_GRUPO_TRABALHO  NUMBER(10),
  NR_SEQ_GRUPO_PLANEJ    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANRECL_MANGRPL_FK_I ON TASY.MAN_REGRA_ENVIO_COMUN_LIB
(NR_SEQ_GRUPO_PLANEJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANRECL_MANGRPL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANRECL_MANGRTR_FK_I ON TASY.MAN_REGRA_ENVIO_COMUN_LIB
(NR_SEQ_GRUPO_TRABALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANRECL_MANGRTR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANRECL_MANRECO_FK_I ON TASY.MAN_REGRA_ENVIO_COMUN_LIB
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANRECL_PERFIL_FK_I ON TASY.MAN_REGRA_ENVIO_COMUN_LIB
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANRECL_PERFIL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANRECL_PK ON TASY.MAN_REGRA_ENVIO_COMUN_LIB
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MAN_REGRA_ENVIO_COMUN_LIB ADD (
  CONSTRAINT MANRECL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_REGRA_ENVIO_COMUN_LIB ADD (
  CONSTRAINT MANRECL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT MANRECL_MANRECO_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.MAN_REGRA_ENVIO_COMUNIC (NR_SEQUENCIA),
  CONSTRAINT MANRECL_MANGRTR_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_TRABALHO) 
 REFERENCES TASY.MAN_GRUPO_TRABALHO (NR_SEQUENCIA),
  CONSTRAINT MANRECL_MANGRPL_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_PLANEJ) 
 REFERENCES TASY.MAN_GRUPO_PLANEJAMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_REGRA_ENVIO_COMUN_LIB TO NIVEL_1;

