ALTER TABLE TASY.CHECKUP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CHECKUP CASCADE CONSTRAINTS;

CREATE TABLE TASY.CHECKUP
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  CD_SETOR_ATENDIMENTO   NUMBER(5)              NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_PREVISTO            DATE                   NOT NULL,
  DT_CONFIRMACAO         DATE,
  NM_USUARIO_CONF        VARCHAR2(15 BYTE),
  NM_CONTATO_CONF        VARCHAR2(60 BYTE),
  NR_PRESCRICAO          NUMBER(15),
  NR_ATENDIMENTO         NUMBER(10),
  DT_INICIO_REAL         DATE,
  DT_FIM_REAL            DATE,
  DT_CANCELAMENTO        DATE,
  NM_USUARIO_ORIGINAL    VARCHAR2(15 BYTE),
  NM_USUARIO_CANCEL      VARCHAR2(60 BYTE),
  NM_CONTATO_CANCEL      VARCHAR2(60 BYTE),
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  DT_FIM_INTERNO         DATE,
  DT_GERADO_AVAL         DATE,
  DT_ENVIO_COMUNIC       DATE,
  NR_SEQ_MOTIVO_CANCEL   NUMBER(10),
  DT_RESPOSTA_COMUNIC    DATE,
  CD_CONVENIO            NUMBER(5),
  CD_CATEGORIA           VARCHAR2(10 BYTE),
  CD_PLANO               VARCHAR2(10 BYTE),
  CD_EMPRESA_REF         NUMBER(10),
  NR_SEQ_PRIORIDADE      NUMBER(10),
  IE_ENCAIXE             VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_CHECKUP    NUMBER(10),
  IE_AUTORIZADO          VARCHAR2(1 BYTE),
  DS_OBSERVACAO          VARCHAR2(2000 BYTE),
  NR_TELEFONE            VARCHAR2(80 BYTE),
  DT_AGENDAMENTO         DATE,
  IE_STATUS_AGENDA       VARCHAR2(3 BYTE),
  CD_USUARIO_CONVENIO    VARCHAR2(30 BYTE),
  DT_VALIDADE_CARTEIRA   DATE,
  NR_SEQ_PAC_SENHA_FILA  NUMBER(10),
  NR_DDI                 VARCHAR2(3 BYTE),
  IE_TRANSFERENCIA       VARCHAR2(1 BYTE),
  IE_EXCLUSIVO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CHECKUP_ATEPACI_FK_I ON TASY.CHECKUP
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHECKUP_CATCONV_FK_I ON TASY.CHECKUP
(CD_CONVENIO, CD_CATEGORIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_CATCONV_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_CHEMOCA_FK_I ON TASY.CHECKUP
(NR_SEQ_MOTIVO_CANCEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_CHEMOCA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_CHEPRIO_FK_I ON TASY.CHECKUP
(NR_SEQ_PRIORIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_CHEPRIO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_CONPLAN_FK_I ON TASY.CHECKUP
(CD_CONVENIO, CD_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_CONVENI_FK_I ON TASY.CHECKUP
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_EMPREFE_FK_I ON TASY.CHECKUP
(CD_EMPRESA_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_ESTABEL_FK_I ON TASY.CHECKUP
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHECKUP_I1 ON TASY.CHECKUP
(DT_PREVISTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHECKUP_I2 ON TASY.CHECKUP
(CD_ESTABELECIMENTO, DT_PREVISTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          376K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHECKUP_I3 ON TASY.CHECKUP
(TRUNC("DT_PREVISTO"))
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.CHECKUP_PACSEFI_FK_I ON TASY.CHECKUP
(NR_SEQ_PAC_SENHA_FILA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_PACSEFI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_PESFISI_FK_I ON TASY.CHECKUP
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CHECKUP_PK ON TASY.CHECKUP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_PK
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_PRESMED_FK_I ON TASY.CHECKUP
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_PRESMED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_SETATEN_FK_I ON TASY.CHECKUP
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CHECKUP_TIPCHEC_FK_I ON TASY.CHECKUP
(NR_SEQ_TIPO_CHECKUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CHECKUP_TIPCHEC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Checkup_Insert
After INSERT ON Checkup
FOR EACH ROW
DECLARE

qt_idade_w		number(3,0);
ie_sexo_w		varchar2(1);
nr_seq_avaliacao_w	number(10,0);
nr_sequencia_w		number(10,0);


cursor c01 is
	select	nr_seq_avaliacao
	from	checkup_avaliacao
	where	cd_estabelecimento = :new.cd_estabelecimento
	and	qt_idade_w between nvl(qt_idade_min,qt_idade_w) and nvl(qt_idade_max,qt_idade_w)
	and	ie_sexo_w = nvl(ie_sexo, ie_sexo_w);

BEGIN

select	nvl(obter_idade_pf(:new.cd_pessoa_fisica, sysdate, 'A'),0),
	obter_sexo_pf(:new.cd_pessoa_fisica, 'C')
into	qt_idade_w,
	ie_sexo_w
from	dual;


open c01;
loop
fetch c01 into
	nr_seq_avaliacao_w;
exit when c01%notfound;

	select	med_avaliacao_paciente_seq.nextval
	into	nr_sequencia_w
	from	dual;

	insert into med_avaliacao_paciente(
			nr_sequencia,
			cd_pessoa_fisica,
			cd_medico,
			dt_avaliacao,
			dt_atualizacao,
			nm_usuario,
			ds_observacao,
			nr_seq_tipo_avaliacao,
			ie_avaliacao_parcial,
			nr_atendimento,
			nr_prescricao,
			nr_seq_aval_compl,
			dt_liberacao,
			nr_seq_checkup)
	values		(nr_sequencia_w,
			:new.cd_pessoa_fisica,
			:new.cd_pessoa_fisica,
			sysdate,
			sysdate,
			:new.nm_usuario,
			null,
			nr_seq_avaliacao_w,
			'N',
			:new.nr_atendimento,
			:new.nr_prescricao,
			null,
			null,
			:new.nr_sequencia);
end loop;
close c01;

END;
/


CREATE OR REPLACE TRIGGER TASY.Checkup_Atual
before INSERT OR UPDATE ON TASY.CHECKUP FOR EACH ROW
BEGIN

if	(:new.cd_pessoa_fisica is not null) and
	(:old.cd_pessoa_fisica is null) and
	(:new.nm_usuario_original is null)then
	begin
	:new.nm_usuario_original	:= :new.nm_usuario;
	:new.dt_agendamento		:= sysdate;
	end;
end if;

END;
/


CREATE OR REPLACE TRIGGER TASY.Checkup_Before
before insert or UPDATE ON TASY.CHECKUP FOR EACH ROW
declare
qt_reg_w			number(10);
ie_dia_semana_w			number(10);
ie_bloqueio_w			VARCHAR2(10);
ie_bloqueio_dia_w		VARCHAR2(10);
cd_empresa_w			number(10);
cd_estabelecimento_w		Number(4);
ie_bloqueio_exce_w		VARCHAR2(10) := 'N';
cd_estabelecimento_base_w	number(4);

BEGIN


if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.dt_previsto <> :old.dt_previsto) then
	update	checkup_etapa
	set	dt_prevista	= (dt_prevista + (:new.dt_previsto - :old.dt_previsto))
	where	nr_seq_checkup	= :new.nr_sequencia;
end if;

if	(:old.nr_atendimento is null) and
	(:new.nr_atendimento is not null) then
	update	med_avaliacao_paciente
	set	nr_atendimento = :new.nr_atendimento
	where	nr_seq_checkup = :new.nr_sequencia;
end if;

if	(:new.cd_pessoa_fisica	is not null) and
	(:old.cd_pessoa_fisica is null) then
	if	(:new.nm_usuario_original is null) then
		:new.nm_usuario_original	:= :new.nm_usuario;
	end if;
	if (:new.dt_agendamento is null) then
		:new.dt_agendamento		:= :new.dt_atualizacao;
	end if;
end if;

select  COUNT(*)
INTO	QT_REG_W
from   	CHECKUP_EMPRESA_ESTAB a
where 	a.cd_empresa    = :new.cd_empresa_ref;

if	(QT_REG_W > 0) then
	select  COUNT(*)
	INTO	QT_REG_W
	from   	CHECKUP_EMPRESA_ESTAB a
	where 	a.cd_empresa    = :new.cd_empresa_ref
	and   	a.cd_estabelecimento  = :new.cd_estabelecimento
	and	nvl(ie_permite,'N') = 'N';

	if 	(QT_REG_W > 0) then
		exibir_erro_abortar('Empresa n�o liberada para este estabelecimento. #@#@',null);
	end if;
end if;

select  count(*)
into	qt_reg_w
from   	checkup_empresa_estab a
where 	a.cd_convenio    = :new.cd_convenio;

if	(qt_reg_w > 0) then
	select  COUNT(*)
	INTO	qt_reg_w
	from   	CHECKUP_EMPRESA_ESTAB a
	where 	a.cd_convenio    = :new.cd_convenio
	and   	a.cd_estabelecimento  = :new.cd_estabelecimento
	and	nvl(ie_permite,'N') = 'N';

	if 	(qt_reg_w > 0) then
		exibir_erro_abortar('Conv�nio n�o liberado para este estabelecimento. #@#@',null);
	end if;
end if;

BEGIN
ie_dia_semana_w		:= pkg_date_utils.get_WeekDay(:new.dt_previsto);
select 'S'
into	ie_bloqueio_w
from	CHECKUP_bloqueio
where	trunc(:new.dt_previsto) between dt_inicial and dt_final
and	((cd_Estabelecimento = :new.cd_estabelecimento) or (cd_estabelecimento is null))
and	ie_dia_semana is null;
exception
	when others then
		ie_bloqueio_w := 'N';
	end;

begin
/* bloqueio por dia */
select 'S'
into	ie_bloqueio_dia_w
from	CHECKUP_bloqueio
where	trunc(:new.dt_previsto) between dt_inicial and dt_final
and	((ie_dia_semana = ie_dia_semana_w) or (ie_dia_semana = 9))
and	((cd_Estabelecimento = :new.cd_estabelecimento) or (cd_estabelecimento is null))
and	ie_dia_semana is not null;
exception
	when others then
		ie_bloqueio_dia_w := 'N';
end;

begin
select 'S'
into	ie_bloqueio_exce_w
from	CHECKUP_REGRA_DIA a
where	trunc(:new.dt_previsto) = trunc(a.dt_referencia)
and	((cd_Estabelecimento = :new.cd_estabelecimento) or (cd_estabelecimento is null));
exception
	when others then
		ie_bloqueio_exce_w := 'N';
end;

if	((ie_bloqueio_w	= 'S') or
	(ie_bloqueio_dia_w	= 'S')) and
	(ie_bloqueio_exce_w = 'N') then
	exibir_erro_abortar('Per�odo bloqueado para o agendamento. #@#@',null);
end if;

if	(:new.NR_SEQ_MOTIVO_CANCEL is null) and
	(:old.NR_SEQ_MOTIVO_CANCEL is not null) and
	(:new.DT_CANCELAMENTO is not null) then
	exibir_erro_abortar('N�o � poss�vel remover o motivo de cancelamento. #@#@',null);
end if;

if	(:new.cd_setor_atendimento is not null) then
	select	max(cd_estabelecimento_base)
	into	cd_estabelecimento_base_w
	from	setor_atendimento
	where	cd_setor_atendimento	= :new.cd_setor_atendimento;

	if	(cd_estabelecimento_base_w <> :new.cd_estabelecimento) then
		Wheb_mensagem_pck.exibir_mensagem_abort(270147);
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.CHECKUP_PACIENTE_UPDATE
before update ON TASY.CHECKUP for each row
declare

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'S') and
	(:old.cd_pessoa_fisica is not null) and
	(:old.nr_atendimento is not null) and
	(:new.cd_pessoa_fisica is null) and
	(:new.nr_atendimento is not null) then
	-- N�o � poss�vel excluir o paciente.
	Wheb_mensagem_pck.exibir_mensagem_abort(263500);
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.Checkup_log
After INSERT or UPDATE or DELETE ON TASY.CHECKUP FOR EACH ROW
DECLARE

ds_old_values_w		varchar2(4000) := '';
ds_new_values_w		varchar2(4000) := '';
nm_atributo_w		varchar2(50);
ds_label_w			varchar2(50);
ie_tipo_atributo_w	varchar2(10);
ie_obs_mudou_w		varchar2(1) := 'N';
ie_tipo_log_w		varchar2(10);


nm_usuario_w		varchar2(50);
cd_pessoa_fisica_w	varchar2(10);
nr_sequencia_w		number(10);
nr_atendimento_w	number(10);
dt_previsto_w		date;

cursor c01 is
	select NM_ATRIBUTO,
		decode(CD_EXP_LABEL, null, NM_ATRIBUTO, OBTER_DESC_EXPRESSAO(CD_EXP_LABEL)) DS_LABEL,
		IE_TIPO_ATRIBUTO
	from tabela_atributo
	where nm_tabela = 'CHECKUP'
	  and ie_tipo_atributo not in ('FUNCTION', 'VISUAL')
	order by nm_atributo;

BEGIN

if (Obter_Valor_Param_Usuario(996, 70, wheb_usuario_pck.get_cd_perfil, wheb_usuario_pck.get_nm_usuario, wheb_usuario_pck.get_cd_estabelecimento) = 'S') then
	if UPDATING then
		open c01;
		loop
		fetch c01 into
			nm_atributo_w,
			ds_label_w,
			ie_tipo_atributo_w;
		exit when c01%notfound;

			if (UPDATING (nm_atributo_w)) then

				if (nm_atributo_w = 'DS_OBSERVACAO') then
					ie_obs_mudou_w := 'S';
				end if;

				if (nm_atributo_w = 'NM_USUARIO_ORIGINAL') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO_ORIGINAL, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO_ORIGINAL, 1, 4000);
				elsif (nm_atributo_w = 'NM_USUARIO_CANCEL') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO_CANCEL, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO_CANCEL, 1, 4000);
				elsif (nm_atributo_w = 'NM_CONTATO_CANCEL') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_CONTATO_CANCEL, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_CONTATO_CANCEL, 1, 4000);
				elsif (nm_atributo_w = 'NR_SEQUENCIA') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_SEQUENCIA, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_SEQUENCIA, 1, 4000);
				elsif (nm_atributo_w = 'DT_ATUALIZACAO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_ATUALIZACAO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_ATUALIZACAO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'NM_USUARIO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO, 1, 4000);
				elsif (nm_atributo_w = 'CD_PESSOA_FISICA') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.CD_PESSOA_FISICA, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.CD_PESSOA_FISICA, 1, 4000);
				elsif (nm_atributo_w = 'NR_ATENDIMENTO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_ATENDIMENTO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_ATENDIMENTO, 1, 4000);
				elsif (nm_atributo_w = 'CD_ESTABELECIMENTO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.CD_ESTABELECIMENTO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.CD_ESTABELECIMENTO, 1, 4000);
				elsif (nm_atributo_w = 'CD_SETOR_ATENDIMENTO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.CD_SETOR_ATENDIMENTO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.CD_SETOR_ATENDIMENTO, 1, 4000);
				elsif (nm_atributo_w = 'DT_PREVISTO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_PREVISTO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_PREVISTO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'DT_CONFIRMACAO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_CONFIRMACAO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_CONFIRMACAO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'NM_USUARIO_CONF') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO_CONF, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO_CONF, 1, 4000);
				elsif (nm_atributo_w = 'NM_CONTATO_CONF') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_CONTATO_CONF, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_CONTATO_CONF, 1, 4000);
				elsif (nm_atributo_w = 'NR_PRESCRICAO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_PRESCRICAO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_PRESCRICAO, 1, 4000);
				elsif (nm_atributo_w = 'DT_INICIO_REAL') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_INICIO_REAL, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_INICIO_REAL, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'DT_FIM_REAL') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_FIM_REAL, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_FIM_REAL, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'DT_ATUALIZACAO_NREC') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_ATUALIZACAO_NREC, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_ATUALIZACAO_NREC, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'NM_USUARIO_NREC') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NM_USUARIO_NREC, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NM_USUARIO_NREC, 1, 4000);
				elsif (nm_atributo_w = 'DT_CANCELAMENTO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_CANCELAMENTO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_CANCELAMENTO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'DT_FIM_INTERNO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_FIM_INTERNO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_FIM_INTERNO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'DT_GERADO_AVAL') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_GERADO_AVAL, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_GERADO_AVAL, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'DT_ENVIO_COMUNIC') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_ENVIO_COMUNIC, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_ENVIO_COMUNIC, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'NR_SEQ_MOTIVO_CANCEL') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_SEQ_MOTIVO_CANCEL, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_SEQ_MOTIVO_CANCEL, 1, 4000);
				elsif (nm_atributo_w = 'DT_RESPOSTA_COMUNIC') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_RESPOSTA_COMUNIC, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_RESPOSTA_COMUNIC, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'CD_CONVENIO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.CD_CONVENIO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.CD_CONVENIO, 1, 4000);
				elsif (nm_atributo_w = 'CD_CATEGORIA') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.CD_CATEGORIA, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.CD_CATEGORIA, 1, 4000);
				elsif (nm_atributo_w = 'CD_PLANO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.CD_PLANO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.CD_PLANO, 1, 4000);
				elsif (nm_atributo_w = 'CD_EMPRESA_REF') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.CD_EMPRESA_REF, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.CD_EMPRESA_REF, 1, 4000);
				elsif (nm_atributo_w = 'IE_ENCAIXE') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.IE_ENCAIXE, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.IE_ENCAIXE, 1, 4000);
				elsif (nm_atributo_w = 'NR_SEQ_PRIORIDADE') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_SEQ_PRIORIDADE, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_SEQ_PRIORIDADE, 1, 4000);
				elsif (nm_atributo_w = 'NR_SEQ_TIPO_CHECKUP') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_SEQ_TIPO_CHECKUP, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_SEQ_TIPO_CHECKUP, 1, 4000);
				elsif (nm_atributo_w = 'IE_AUTORIZADO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.IE_AUTORIZADO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.IE_AUTORIZADO, 1, 4000);
				elsif (nm_atributo_w = 'NR_TELEFONE') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_TELEFONE, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_TELEFONE, 1, 4000);
				elsif (nm_atributo_w = 'DT_AGENDAMENTO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_AGENDAMENTO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_AGENDAMENTO, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'IE_STATUS_AGENDA') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.IE_STATUS_AGENDA, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.IE_STATUS_AGENDA, 1, 4000);
				elsif (nm_atributo_w = 'DT_VALIDADE_CARTEIRA') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || to_char(:NEW.DT_VALIDADE_CARTEIRA, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || to_char(:OLD.DT_VALIDADE_CARTEIRA, 'DD/MM/YYYY hh24:mi:ss') , 1, 4000);
				elsif (nm_atributo_w = 'CD_USUARIO_CONVENIO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.CD_USUARIO_CONVENIO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.CD_USUARIO_CONVENIO, 1, 4000);
				elsif (nm_atributo_w = 'NR_SEQ_PAC_SENHA_FILA') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_SEQ_PAC_SENHA_FILA, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_SEQ_PAC_SENHA_FILA, 1, 4000);
				elsif (nm_atributo_w = 'NR_DDI') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.NR_DDI, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.NR_DDI, 1, 4000);
				elsif (nm_atributo_w = 'IE_TRANSFERENCIA') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.IE_TRANSFERENCIA, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.IE_TRANSFERENCIA, 1, 4000);
				elsif (nm_atributo_w = 'IE_EXCLUSIVO') then
				  ds_new_values_w := substr(ds_new_values_w || ' | '|| ds_label_w || ': ' || :NEW.IE_EXCLUSIVO, 1, 4000);
				  ds_old_values_w := substr(ds_old_values_w || ' | '|| ds_label_w || ': ' || :OLD.IE_EXCLUSIVO, 1, 4000);
				end if;

			end if;

		end loop;
		close c01;
		if (nvl(length(ds_old_values_w), 0) > 0 or nvl(length(ds_new_values_w), 0) > 0) then

			ds_old_values_w := substr(ds_old_values_w, 4, 4000);
			ds_new_values_w := substr(ds_new_values_w, 4, 4000);

			insert into log_checkup(
				nr_sequencia,
				dt_log,
				nm_usuario,
				cd_pessoa_fisica,
				ie_tipo_log,
				nm_tabela,
				nr_seq_tabela,
				ds_log_anterior,
				ds_log_novo,
				ds_obs_anterior,
				ds_obs_novo,
				cd_estabelecimento,
				CD_SETOR_ATENDIMENTO)
			values (
				log_checkup_seq.nextval,
				sysdate,
				:new.nm_usuario,
				:new.cd_pessoa_fisica,
				'UPDATE',
				'CHECKUP',
				:NEW.nr_sequencia,
				ds_old_values_w,
				ds_new_values_w,
				decode(ie_obs_mudou_w, 'S', :old.DS_OBSERVACAO, null),
				decode(ie_obs_mudou_w, 'S', :new.DS_OBSERVACAO, null),
				obter_estabelecimento_ativo,
				obter_setor_ativo);
		end if;

	else
		if INSERTING then
			ie_tipo_log_w := 'INSERT';
			nm_usuario_w := :new.nm_usuario;
			cd_pessoa_fisica_w := :new.cd_pessoa_fisica;
			nr_sequencia_w := :new.nr_sequencia;
			nr_atendimento_w := :new.nr_atendimento;
			dt_previsto_w := :new.dt_previsto;
		elsif DELETING then
			ie_tipo_log_w := 'DELETE';
			nm_usuario_w := :old.nm_usuario;
			cd_pessoa_fisica_w := :old.cd_pessoa_fisica;
			nr_sequencia_w := :old.nr_sequencia;
			nr_atendimento_w := :old.nr_atendimento;
			dt_previsto_w := :old.dt_previsto;
		end if;

		select decode(CD_EXP_LABEL, null, NM_ATRIBUTO, OBTER_DESC_EXPRESSAO(CD_EXP_LABEL)) DS_LABEL
		into ds_label_w
		from tabela_atributo
		where nm_tabela = 'CHECKUP'
			and nm_atributo = 'NR_ATENDIMENTO';

		ds_new_values_w := ds_label_w || ': ' || nr_atendimento_w;


		select decode(CD_EXP_LABEL, null, NM_ATRIBUTO, OBTER_DESC_EXPRESSAO(CD_EXP_LABEL)) DS_LABEL
		into ds_label_w
		from tabela_atributo
		where nm_tabela = 'CHECKUP'
		  and nm_atributo = 'DT_PREVISTO';

		ds_new_values_w := ds_new_values_w || ' | ' || ds_label_w || ': ' || to_char(dt_previsto_w, 'DD/MM/YYYY hh24:mi:ss');

		insert into log_checkup(
			nr_sequencia,
			dt_log,
			nm_usuario,
			cd_pessoa_fisica,
			ie_tipo_log,
			nm_tabela,
			nr_seq_tabela,
			ds_log_anterior,
			ds_log_novo,
			DS_OBS_anterior,
			DS_OBS_novo,
			cd_estabelecimento,
			CD_SETOR_ATENDIMENTO)
		values	(
			log_checkup_seq.nextval,
			sysdate,
			nm_usuario_w,
			cd_pessoa_fisica_w,
			ie_tipo_log_w,
			'CHECKUP',
			nr_sequencia_w,
			null,
			ds_new_values_w,
			null,
			null,
			obter_estabelecimento_ativo,
			obter_setor_ativo);

	end if;
end if;
END;
/


ALTER TABLE TASY.CHECKUP ADD (
  CONSTRAINT CHECKUP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CHECKUP ADD (
  CONSTRAINT CHECKUP_PACSEFI_FK 
 FOREIGN KEY (NR_SEQ_PAC_SENHA_FILA) 
 REFERENCES TASY.PACIENTE_SENHA_FILA (NR_SEQUENCIA),
  CONSTRAINT CHECKUP_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT CHECKUP_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO),
  CONSTRAINT CHECKUP_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_REF) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT CHECKUP_CHEPRIO_FK 
 FOREIGN KEY (NR_SEQ_PRIORIDADE) 
 REFERENCES TASY.CHECKUP_PRIORIDADE (NR_SEQUENCIA),
  CONSTRAINT CHECKUP_TIPCHEC_FK 
 FOREIGN KEY (NR_SEQ_TIPO_CHECKUP) 
 REFERENCES TASY.TIPO_CHECKUP (NR_SEQUENCIA),
  CONSTRAINT CHECKUP_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT CHECKUP_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT CHECKUP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT CHECKUP_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT CHECKUP_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO),
  CONSTRAINT CHECKUP_CHEMOCA_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_CANCEL) 
 REFERENCES TASY.CHECKUP_MOTIVO_CANCEL (NR_SEQUENCIA),
  CONSTRAINT CHECKUP_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO));

GRANT SELECT ON TASY.CHECKUP TO NIVEL_1;

