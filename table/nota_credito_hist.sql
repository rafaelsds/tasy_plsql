ALTER TABLE TASY.NOTA_CREDITO_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NOTA_CREDITO_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.NOTA_CREDITO_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_NOTA_CREDITO  NUMBER(15,2)             NOT NULL,
  DS_HISTORICO         VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NOTCREHIS_NOTCRED_FK_I ON TASY.NOTA_CREDITO_HIST
(NR_SEQ_NOTA_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NOTCREHIS_PK ON TASY.NOTA_CREDITO_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NOTA_CREDITO_HIST ADD (
  CONSTRAINT NOTCREHIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.NOTA_CREDITO_HIST ADD (
  CONSTRAINT NOTCREHIS_NOTCRED_FK 
 FOREIGN KEY (NR_SEQ_NOTA_CREDITO) 
 REFERENCES TASY.NOTA_CREDITO (NR_SEQUENCIA));

GRANT SELECT ON TASY.NOTA_CREDITO_HIST TO NIVEL_1;

