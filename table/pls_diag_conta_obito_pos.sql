ALTER TABLE TASY.PLS_DIAG_CONTA_OBITO_POS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_DIAG_CONTA_OBITO_POS CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_DIAG_CONTA_OBITO_POS
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  CD_DOENCA                VARCHAR2(10 BYTE),
  NR_SEQ_DIAG_CONTA_OBITO  NUMBER(10),
  NR_DECLARACAO_OBITO      VARCHAR2(20 BYTE),
  IE_INDICADOR_DORN        VARCHAR2(1 BYTE),
  NR_SEQ_CONTA             NUMBER(10)           NOT NULL,
  NR_SEQ_CABECALHO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSDIAGOBP_CIDDOEN_FK_I ON TASY.PLS_DIAG_CONTA_OBITO_POS
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSDIAGOBP_PK ON TASY.PLS_DIAG_CONTA_OBITO_POS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSDIAGOBP_PLSCOME_FK_I ON TASY.PLS_DIAG_CONTA_OBITO_POS
(NR_SEQ_CONTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSDIAGOBP_PLSCPCA_FK_I ON TASY.PLS_DIAG_CONTA_OBITO_POS
(NR_SEQ_CABECALHO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_DIAG_CONTA_OBITO_POS ADD (
  CONSTRAINT PLSDIAGOBP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_DIAG_CONTA_OBITO_POS ADD (
  CONSTRAINT PLSDIAGOBP_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT PLSDIAGOBP_PLSCOME_FK 
 FOREIGN KEY (NR_SEQ_CONTA) 
 REFERENCES TASY.PLS_CONTA (NR_SEQUENCIA),
  CONSTRAINT PLSDIAGOBP_PLSCPCA_FK 
 FOREIGN KEY (NR_SEQ_CABECALHO) 
 REFERENCES TASY.PLS_CONTA_POS_CABECALHO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_DIAG_CONTA_OBITO_POS TO NIVEL_1;

