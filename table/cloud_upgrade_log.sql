DROP TABLE TASY.CLOUD_UPGRADE_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.CLOUD_UPGRADE_LOG
(
  NM_TIPO                         VARCHAR2(200 BYTE),
  DS_MENSAGEM                     CLOB,
  DT_ATUALIZACAO                  TIMESTAMP(6),
  CD_VERSAO_ATUAL                 VARCHAR2(50 BYTE),
  NM_USUARIO                      VARCHAR2(50 BYTE),
  NM_FASE_ATUALIZACAO             VARCHAR2(100 BYTE),
  IE_STATUS                       VARCHAR2(10 BYTE),
  CD_VERSAO_DEST                  VARCHAR2(50 BYTE),
  NM_HOST                         VARCHAR2(50 BYTE),
  IE_SCHEDULE                     VARCHAR2(1 BYTE),
  DS_COMANDO                      CLOB,
  NR_PACOTE                       NUMBER(10),
  NR_RELEASE                      NUMBER(10),
  NR_SEQ_SCRIPT                   NUMBER(20),
  NM_OBJETO                       VARCHAR2(128 BYTE),
  NR_SERVICE_PACK                 NUMBER(10),
  NR_SEQ_LOG_ATUALIZACAO_APP_MGR  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (DS_COMANDO) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (DS_MENSAGEM) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.UPGRDLOG_LATAMGR_FK_I ON TASY.CLOUD_UPGRADE_LOG
(NR_SEQ_LOG_ATUALIZACAO_APP_MGR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CLOUD_UPGRADE_LOG ADD (
  CONSTRAINT UPGRDLOG_LATAMGR_FK 
 FOREIGN KEY (NR_SEQ_LOG_ATUALIZACAO_APP_MGR) 
 REFERENCES TASY.LOG_ATUALIZACAO_APP_MGNR (NR_SEQUENCIA));

GRANT SELECT ON TASY.CLOUD_UPGRADE_LOG TO NIVEL_1;

