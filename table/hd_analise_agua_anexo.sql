ALTER TABLE TASY.HD_ANALISE_AGUA_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_ANALISE_AGUA_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_ANALISE_AGUA_ANEXO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ARQUIVO           VARCHAR2(255 BYTE),
  NR_SEQ_ANALISE_AGUA  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDANAGA_HDANAGU_FK_I ON TASY.HD_ANALISE_AGUA_ANEXO
(NR_SEQ_ANALISE_AGUA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDANAGA_HDANAGU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDANAGA_PK ON TASY.HD_ANALISE_AGUA_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDANAGA_PK
  MONITORING USAGE;


ALTER TABLE TASY.HD_ANALISE_AGUA_ANEXO ADD (
  CONSTRAINT HDANAGA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_ANALISE_AGUA_ANEXO ADD (
  CONSTRAINT HDANAGA_HDANAGU_FK 
 FOREIGN KEY (NR_SEQ_ANALISE_AGUA) 
 REFERENCES TASY.HD_ANALISE_AGUA (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_ANALISE_AGUA_ANEXO TO NIVEL_1;

