ALTER TABLE TASY.PLS_ORIGEM_SOLIC_AGENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ORIGEM_SOLIC_AGENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ORIGEM_SOLIC_AGENTE
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_ORIGEM            NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE),
  NR_SEQ_AGENTE_MOTIVADOR  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOSAG_PK ON TASY.PLS_ORIGEM_SOLIC_AGENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOSAG_PLSAGMO_FK_I ON TASY.PLS_ORIGEM_SOLIC_AGENTE
(NR_SEQ_AGENTE_MOTIVADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOSAG_PLSORSO_FK_I ON TASY.PLS_ORIGEM_SOLIC_AGENTE
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_ORIGEM_SOLIC_AGENTE ADD (
  CONSTRAINT PLSOSAG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ORIGEM_SOLIC_AGENTE ADD (
  CONSTRAINT PLSOSAG_PLSAGMO_FK 
 FOREIGN KEY (NR_SEQ_AGENTE_MOTIVADOR) 
 REFERENCES TASY.PLS_AGENTE_MOTIVADOR (NR_SEQUENCIA),
  CONSTRAINT PLSOSAG_PLSORSO_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.PLS_ORIGEM_SOLICITACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ORIGEM_SOLIC_AGENTE TO NIVEL_1;

