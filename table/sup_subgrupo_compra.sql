ALTER TABLE TASY.SUP_SUBGRUPO_COMPRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_SUBGRUPO_COMPRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_SUBGRUPO_COMPRA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_GRUPO_COMPRA  NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_SUBGRUPO_COMPRA   VARCHAR2(60 BYTE)        NOT NULL,
  VL_AUTORIZADO        NUMBER(13,2),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUSUCOM_PK ON TASY.SUP_SUBGRUPO_COMPRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUSUCOM_SUSUCOM_FK_I ON TASY.SUP_SUBGRUPO_COMPRA
(NR_SEQ_GRUPO_COMPRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUP_SUBGRUPO_COMPRA_tp  after update ON TASY.SUP_SUBGRUPO_COMPRA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.VL_AUTORIZADO,1,4000),substr(:new.VL_AUTORIZADO,1,4000),:new.nm_usuario,nr_seq_w,'VL_AUTORIZADO',ie_log_w,ds_w,'SUP_SUBGRUPO_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_COMPRA,1,4000),substr(:new.NR_SEQ_GRUPO_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_COMPRA',ie_log_w,ds_w,'SUP_SUBGRUPO_COMPRA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SUBGRUPO_COMPRA,1,4000),substr(:new.DS_SUBGRUPO_COMPRA,1,4000),:new.nm_usuario,nr_seq_w,'DS_SUBGRUPO_COMPRA',ie_log_w,ds_w,'SUP_SUBGRUPO_COMPRA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_SUBGRUPO_COMPRA ADD (
  CONSTRAINT SUSUCOM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_SUBGRUPO_COMPRA ADD (
  CONSTRAINT SUSUCOM_SUSUCOM_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_COMPRA) 
 REFERENCES TASY.SUP_GRUPO_COMPRA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUP_SUBGRUPO_COMPRA TO NIVEL_1;

