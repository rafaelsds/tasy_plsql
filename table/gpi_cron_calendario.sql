ALTER TABLE TASY.GPI_CRON_CALENDARIO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPI_CRON_CALENDARIO CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPI_CRON_CALENDARIO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_CRONOGRAMA    NUMBER(10)               NOT NULL,
  IE_DIA_SEMANA        VARCHAR2(1 BYTE)         NOT NULL,
  HR_INICIO            VARCHAR2(5 BYTE)         NOT NULL,
  HR_FINAL             VARCHAR2(5 BYTE)         NOT NULL,
  QT_MIN_INTERVALO     NUMBER(3)                NOT NULL,
  DT_INICIO            DATE,
  DT_FINAL             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GPICRONCAL_GPICRON_FK_I ON TASY.GPI_CRON_CALENDARIO
(NR_SEQ_CRONOGRAMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPICRONCAL_GPICRON_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.GPICRONCAL_PK ON TASY.GPI_CRON_CALENDARIO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPICRONCAL_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.gpi_cron_calendario_atual
before insert or update ON TASY.GPI_CRON_CALENDARIO for each row
declare

begin
begin
    if (:new.hr_inicio is not null) and ((:new.hr_inicio <> :old.hr_inicio) or (:old.dt_inicio is null)) then
		:new.dt_inicio := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_inicio,'dd/mm/yyyy hh24:mi');
	end if;
    if (:new.hr_final is not null) and ((:new.hr_final <> :old.hr_final) or (:old.dt_final is null)) then
		:new.dt_final := to_date(to_char(sysdate,'dd/mm/yyyy') || ' ' || :new.hr_final,'dd/mm/yyyy hh24:mi');
	end if;
exception
	when others then
	null;
end;

end;
/


ALTER TABLE TASY.GPI_CRON_CALENDARIO ADD (
  CONSTRAINT GPICRONCAL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GPI_CRON_CALENDARIO ADD (
  CONSTRAINT GPICRONCAL_GPICRON_FK 
 FOREIGN KEY (NR_SEQ_CRONOGRAMA) 
 REFERENCES TASY.GPI_CRONOGRAMA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.GPI_CRON_CALENDARIO TO NIVEL_1;

