ALTER TABLE TASY.QRTZ_JOB_DETAILS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QRTZ_JOB_DETAILS CASCADE CONSTRAINTS;

CREATE TABLE TASY.QRTZ_JOB_DETAILS
(
  SCHED_NAME         VARCHAR2(120 BYTE)         NOT NULL,
  JOB_NAME           VARCHAR2(200 BYTE)         NOT NULL,
  JOB_GROUP          VARCHAR2(200 BYTE)         NOT NULL,
  DESCRIPTION        VARCHAR2(250 BYTE),
  JOB_CLASS_NAME     VARCHAR2(250 BYTE)         NOT NULL,
  IS_DURABLE         VARCHAR2(1 BYTE)           NOT NULL,
  IS_NONCONCURRENT   VARCHAR2(1 BYTE)           NOT NULL,
  IS_UPDATE_DATA     VARCHAR2(1 BYTE)           NOT NULL,
  REQUESTS_RECOVERY  VARCHAR2(1 BYTE)           NOT NULL,
  JOB_DATA           BLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (JOB_DATA) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QRTJBDT_PK ON TASY.QRTZ_JOB_DETAILS
(SCHED_NAME, JOB_NAME, JOB_GROUP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          56K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QRTZ_JOB_DETAILS ADD (
  CONSTRAINT QRTJBDT_PK
 PRIMARY KEY
 (SCHED_NAME, JOB_NAME, JOB_GROUP)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          56K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.QRTZ_JOB_DETAILS TO NIVEL_1;

