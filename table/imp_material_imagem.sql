DROP TABLE TASY.IMP_MATERIAL_IMAGEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.IMP_MATERIAL_IMAGEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_MATERIAL          NUMBER(10)               NOT NULL,
  IM_MATERIAL          LONG RAW                 NOT NULL,
  IE_DIRTY_CHECK       NUMBER(1),
  NR_SEQ_MIMS_VERSION  NUMBER(15),
  CD_YJ_CODE           VARCHAR2(12 BYTE),
  IE_TIPO_IMAGE        VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.IMPMATIMAG_MATERIA_FK_I ON TASY.IMP_MATERIAL_IMAGEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.IMP_MATERIAL_IMAGEM ADD (
  CONSTRAINT IMPMATIMAG_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

