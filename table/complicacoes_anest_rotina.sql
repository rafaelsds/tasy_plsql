ALTER TABLE TASY.COMPLICACOES_ANEST_ROTINA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMPLICACOES_ANEST_ROTINA CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMPLICACOES_ANEST_ROTINA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  CD_COMPLICACAO       VARCHAR2(3 BYTE)         NOT NULL,
  DS_COMPLICACAO       VARCHAR2(250 BYTE),
  DS_OBSERVACAO        VARCHAR2(2000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.COANROT_PK ON TASY.COMPLICACOES_ANEST_ROTINA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.COMPLICACOES_ANEST_ROTINA ADD (
  CONSTRAINT COANROT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.COMPLICACOES_ANEST_ROTINA TO NIVEL_1;

