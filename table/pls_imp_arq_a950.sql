ALTER TABLE TASY.PLS_IMP_ARQ_A950
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_IMP_ARQ_A950 CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_IMP_ARQ_A950
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_UNIMED_ORIGEM     VARCHAR2(4 BYTE),
  DT_GERACAO           DATE,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_TRANSACAO     NUMBER(8),
  DS_XML               CLOB,
  DS_ERRO              VARCHAR2(2000 BYTE),
  DT_INICIO_VIGENCIA   DATE,
  NR_VERSAO_TRANSACAO  NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSIAP_ESTABEL_FK_I ON TASY.PLS_IMP_ARQ_A950
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSIAP_PK ON TASY.PLS_IMP_ARQ_A950
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_IMP_ARQ_A950 ADD (
  CONSTRAINT PLSIAP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PLS_IMP_ARQ_A950 ADD (
  CONSTRAINT PLSIAP_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PLS_IMP_ARQ_A950 TO NIVEL_1;

