ALTER TABLE TASY.ARM_RESULT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ARM_RESULT CASCADE CONSTRAINTS;

CREATE TABLE TASY.ARM_RESULT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_AREA          NUMBER(10)               NOT NULL,
  NR_SEQ_TYPE          NUMBER(10)               NOT NULL,
  QT_ESTIMATED         NUMBER(10,2)             NOT NULL,
  QT_REAL              NUMBER(10),
  DT_ESTIMATED         DATE                     NOT NULL,
  NR_SEQ_PROJECT       NUMBER(10),
  QT_CAPACITY          NUMBER(10),
  QT_REALIZED          NUMBER(10,2),
  CD_MANAGER           VARCHAR2(10 BYTE),
  CD_DIRECTOR          VARCHAR2(10 BYTE),
  DT_INICIO            DATE,
  DT_FIM               DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ARMRES_ARMAREA_FK_I ON TASY.ARM_RESULT
(NR_SEQ_AREA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ARMRES_ARMPRJ_FK_I ON TASY.ARM_RESULT
(NR_SEQ_PROJECT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ARMRES_ARMTYPE_FK_I ON TASY.ARM_RESULT
(NR_SEQ_TYPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ARMRES_PESFISI_FK_I ON TASY.ARM_RESULT
(CD_MANAGER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ARMRES_PESFISI_FK2_I ON TASY.ARM_RESULT
(CD_DIRECTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ARMRES_PK ON TASY.ARM_RESULT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ARM_RESULT_tp  after update ON TASY.ARM_RESULT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'ARM_RESULT',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_AREA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_AREA,1,4000),substr(:new.NR_SEQ_AREA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AREA',ie_log_w,ds_w,'ARM_RESULT',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_REALIZED,1,500);gravar_log_alteracao(substr(:old.QT_REALIZED,1,4000),substr(:new.QT_REALIZED,1,4000),:new.nm_usuario,nr_seq_w,'QT_REALIZED',ie_log_w,ds_w,'ARM_RESULT',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_PROJECT,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_PROJECT,1,4000),substr(:new.NR_SEQ_PROJECT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROJECT',ie_log_w,ds_w,'ARM_RESULT',ds_s_w,ds_c_w);  ds_w:=substr(:new.QT_ESTIMATED,1,500);gravar_log_alteracao(substr(:old.QT_ESTIMATED,1,4000),substr(:new.QT_ESTIMATED,1,4000),:new.nm_usuario,nr_seq_w,'QT_ESTIMATED',ie_log_w,ds_w,'ARM_RESULT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.ARM_RESULT ADD (
  CONSTRAINT ARMRES_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ARM_RESULT ADD (
  CONSTRAINT ARMRES_ARMAREA_FK 
 FOREIGN KEY (NR_SEQ_AREA) 
 REFERENCES TASY.ARM_AREA (NR_SEQUENCIA),
  CONSTRAINT ARMRES_ARMTYPE_FK 
 FOREIGN KEY (NR_SEQ_TYPE) 
 REFERENCES TASY.ARM_TYPE (NR_SEQUENCIA),
  CONSTRAINT ARMRES_ARMPRJ_FK 
 FOREIGN KEY (NR_SEQ_PROJECT) 
 REFERENCES TASY.ARM_PROJECT (NR_SEQUENCIA),
  CONSTRAINT ARMRES_PESFISI_FK 
 FOREIGN KEY (CD_MANAGER) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ARMRES_PESFISI_FK2 
 FOREIGN KEY (CD_DIRECTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ARM_RESULT TO NIVEL_1;

