ALTER TABLE TASY.TASY_LOG_ACESSO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TASY_LOG_ACESSO CASCADE CONSTRAINTS;

CREATE TABLE TASY.TASY_LOG_ACESSO
(
  NR_SEQUENCIA        NUMBER(10)                NOT NULL,
  NM_USUARIO          VARCHAR2(15 BYTE)         NOT NULL,
  DT_ACESSO           DATE                      NOT NULL,
  DT_SAIDA            DATE,
  DS_MAQUINA          VARCHAR2(80 BYTE),
  NM_USUARIO_SO       VARCHAR2(30 BYTE),
  CD_APLICACAO_TASY   VARCHAR2(20 BYTE),
  QT_USUARIO_LIB      NUMBER(5),
  QT_USUARIO_CON      NUMBER(5),
  IE_RESULT_ACESSO    VARCHAR2(1 BYTE),
  NM_MAQ_CLIENTE      VARCHAR2(80 BYTE),
  NR_SEQ_JUSTIFIC     NUMBER(10),
  DS_JUSTIFICATIVA    VARCHAR2(255 BYTE),
  NR_HANDLE           NUMBER(10),
  CD_ESTABELECIMENTO  NUMBER(4),
  DS_MAC_ADDRESS      VARCHAR2(20 BYTE),
  DS_UTC              VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO    VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO  VARCHAR2(50 BYTE),
  DS_SO_MAQUINA       VARCHAR2(30 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          88M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TALOGAC_I1 ON TASY.TASY_LOG_ACESSO
(DT_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          43M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TALOGAC_I2 ON TASY.TASY_LOG_ACESSO
(NM_USUARIO, DT_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          824K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TALOGAC_PK ON TASY.TASY_LOG_ACESSO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TALOGAC_TASLOJU_FK_I ON TASY.TASY_LOG_ACESSO
(NR_SEQ_JUSTIFIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TALOGAC_TASLOJU_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.hsj_tasy_log_acesso 
after insert ON TASY.TASY_LOG_ACESSO for each row
DISABLE
declare

sid_w                number(10);
serial_w             number(10);

ds_sql_w             long;
jobno                number;
ie_consistir_w       varchar2(1);
qt_mediana_w         number(10);
ie_machine_plano_w   varchar2(1);
qt_reg_w             number(10);
dt_logoff            date;

begin

    ie_consistir_w:='N';
    qt_mediana_w:=0;
    ie_machine_plano_w:='N';
    
    SELECT  sid, 
            serial#
    into    sid_w,
            serial_w
    FROM v$session
    WHERE audsid=USERENV('SESSIONID');

    select  count(*)
    into    qt_reg_w
    from hsj_usuario_logado_banco_v
    where SID = sid_w
    and SERIAL# = serial_w
    and ie_tasy = 'S';

    if(qt_reg_w > 0)then 
        
        if(substr(:new.ds_maquina,1,6) = 'SSJOSE' and :new.cd_estabelecimento = 1)then
            ie_consistir_w:='S';
            ie_machine_plano_w:='S';
        else
            select  max(ie_consistir)
            into ie_consistir_w
            from hsj_login_assinatura_digital
            where ie_geral = 'S';

            select nvl(MEDIAN(OBTER_SEGUNDO_ENTRE_DATAS (dt_registro, dt_assinatura, 1)),0) Mediana
            into qt_mediana_w
            from hsj_assinatura_digital_v
            WHERE dt_assinatura IS NOT NULL
            AND dt_log >= (SYSDATE - (10 / 1440));
        end if;
    end if;
    
ds_sql_w:='
declare
    qt_reg_w number;
begin    
    
    if('''||ie_machine_plano_w||''' = ''S'' and '|| :new.cd_estabelecimento ||'=1)then
        select count(*)
        into qt_reg_w
        from hsj_usuario_logado_banco_v
        where ie_tasy = ''S''
        and sid = '||sid_w||'
        and serial# = '||serial_w||';
    else
        select count(*)
        into qt_reg_w
        from hsj_usuario_logado_banco_v
        where ie_certificado = ''S''
        and ie_assinatura = ''N''
        and setor_ti = ''N''
        and ie_tasy = ''S''
        and sid = '||sid_w||'
        and serial# = '||serial_w||';
    end if;
 
    if(qt_reg_w > 0 )then
        hsj_kill_session_base('||sid_w||','||serial_w||');
    end if;

end;';

if(ie_consistir_w = 'S' and qt_mediana_w < 15)then

    if(ie_machine_plano_w = 'S')then
       dt_logoff:= sysdate +(((5/24)/60)/60);
    else
        dt_logoff:= sysdate + ((2 / 24) / 60);
    end if;
    
    dbms_job.submit(jobno, ds_sql_w, dt_logoff);
    
end if;
exception when others then
    return;
end hsj_tasy_log_acesso;
/


CREATE OR REPLACE TRIGGER TASY.TASY_LOG_ACESSO_ATUAL
BEFORE INSERT OR UPDATE ON TASY.TASY_LOG_ACESSO FOR EACH ROW
DECLARE
BEGIN
	:NEW.DS_UTC_ATUALIZACAO := OBTER_DATA_UTC(SYSDATE,'HV');
	:NEW.DS_UTC := OBTER_DATA_UTC(:NEW.DT_ACESSO,'HV');
	:NEW.IE_HORARIO_VERAO := OBTER_SE_HORARIO_VERAO(:NEW.DT_ACESSO);
END;
/


ALTER TABLE TASY.TASY_LOG_ACESSO ADD (
  CONSTRAINT TALOGAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          32M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TASY_LOG_ACESSO ADD (
  CONSTRAINT TALOGAC_TASLOJU_FK 
 FOREIGN KEY (NR_SEQ_JUSTIFIC) 
 REFERENCES TASY.TASY_LOG_JUSTIFICATIVA (NR_SEQUENCIA));

GRANT SELECT ON TASY.TASY_LOG_ACESSO TO NIVEL_1;

