ALTER TABLE TASY.TABELA_VISAO_PAIS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TABELA_VISAO_PAIS CASCADE CONSTRAINTS;

CREATE TABLE TASY.TABELA_VISAO_PAIS
(
  CD_PAIS           NUMBER(5)                   NOT NULL,
  NR_SEQ_VISAO      NUMBER(10)                  NOT NULL,
  NR_SEQ_VISAO_REF  NUMBER(10)                  NOT NULL,
  DT_ATUALIZACAO    DATE                        NOT NULL,
  NM_USUARIO        VARCHAR2(15 BYTE)           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TAVIPAI_PK ON TASY.TABELA_VISAO_PAIS
(CD_PAIS, NR_SEQ_VISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAVIPAI_TABVISA_FK_I ON TASY.TABELA_VISAO_PAIS
(NR_SEQ_VISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAVIPAI_TABVISA_FK2_I ON TASY.TABELA_VISAO_PAIS
(NR_SEQ_VISAO_REF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TAVIPAI_TASYPAI_FK_I ON TASY.TABELA_VISAO_PAIS
(CD_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TABELA_VISAO_PAIS ADD (
  CONSTRAINT TAVIPAI_PK
 PRIMARY KEY
 (CD_PAIS, NR_SEQ_VISAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TABELA_VISAO_PAIS ADD (
  CONSTRAINT TAVIPAI_TABVISA_FK 
 FOREIGN KEY (NR_SEQ_VISAO) 
 REFERENCES TASY.TABELA_VISAO (NR_SEQUENCIA),
  CONSTRAINT TAVIPAI_TABVISA_FK2 
 FOREIGN KEY (NR_SEQ_VISAO_REF) 
 REFERENCES TASY.TABELA_VISAO (NR_SEQUENCIA),
  CONSTRAINT TAVIPAI_TASYPAI_FK 
 FOREIGN KEY (CD_PAIS) 
 REFERENCES TASY.TASY_PAIS (CD_PAIS));

GRANT SELECT ON TASY.TABELA_VISAO_PAIS TO NIVEL_1;

