ALTER TABLE TASY.SUP_CONEXAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_CONEXAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_CONEXAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_OBJETIVO          VARCHAR2(80 BYTE)        NOT NULL,
  NR_SEQ_CLIENTE       NUMBER(10),
  DT_INICIO            DATE                     NOT NULL,
  DT_FIM               DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUPCONE_MANLOCA_FK_I ON TASY.SUP_CONEXAO
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPCONE_MANLOCA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUPCONE_PK ON TASY.SUP_CONEXAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUPCONE_PK
  MONITORING USAGE;


ALTER TABLE TASY.SUP_CONEXAO ADD (
  CONSTRAINT SUPCONE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_CONEXAO ADD (
  CONSTRAINT SUPCONE_MANLOCA_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.MAN_LOCALIZACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.SUP_CONEXAO TO NIVEL_1;

