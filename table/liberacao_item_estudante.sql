ALTER TABLE TASY.LIBERACAO_ITEM_ESTUDANTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LIBERACAO_ITEM_ESTUDANTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.LIBERACAO_ITEM_ESTUDANTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_ITEM          NUMBER(10),
  CD_PESSOA_ESTUDANTE  VARCHAR2(10 BYTE),
  CD_PERFIL            NUMBER(5)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LIITEST_PEFIES_FK_I ON TASY.LIBERACAO_ITEM_ESTUDANTE
(CD_PESSOA_ESTUDANTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIITEST_PERFIL_FK_I ON TASY.LIBERACAO_ITEM_ESTUDANTE
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LIITEST_PK ON TASY.LIBERACAO_ITEM_ESTUDANTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.LIITEST_PROITEM_FK_I ON TASY.LIBERACAO_ITEM_ESTUDANTE
(NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LIBERACAO_ITEM_ESTUDANTE ADD (
  CONSTRAINT LIITEST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LIBERACAO_ITEM_ESTUDANTE ADD (
  CONSTRAINT LIITEST_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT LIITEST_PEFIES_FK 
 FOREIGN KEY (CD_PESSOA_ESTUDANTE) 
 REFERENCES TASY.PESSOA_FISICA_ESTUDANTE (CD_PESSOA_FISICA),
  CONSTRAINT LIITEST_PROITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM) 
 REFERENCES TASY.PRONTUARIO_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.LIBERACAO_ITEM_ESTUDANTE TO NIVEL_1;

