ALTER TABLE TASY.PROTOCOLO_REGRA_ONC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_REGRA_ONC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_REGRA_ONC
(
  CD_CATEGORIA_CID     VARCHAR2(10 BYTE)        NOT NULL,
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PROTOCOLO         NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRREONC_CIDCATE_FK_I ON TASY.PROTOCOLO_REGRA_ONC
(CD_CATEGORIA_CID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRREONC_CIDCATE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRREONC_PK ON TASY.PROTOCOLO_REGRA_ONC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRREONC_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRREONC_PROTOCO_FK_I ON TASY.PROTOCOLO_REGRA_ONC
(CD_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PROTOCOLO_REGRA_ONC ADD (
  CONSTRAINT PRREONC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROTOCOLO_REGRA_ONC ADD (
  CONSTRAINT PRREONC_PROTOCO_FK 
 FOREIGN KEY (CD_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO (CD_PROTOCOLO),
  CONSTRAINT PRREONC_CIDCATE_FK 
 FOREIGN KEY (CD_CATEGORIA_CID) 
 REFERENCES TASY.CID_CATEGORIA (CD_CATEGORIA_CID));

GRANT SELECT ON TASY.PROTOCOLO_REGRA_ONC TO NIVEL_1;

