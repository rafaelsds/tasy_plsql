ALTER TABLE TASY.PLS_CP_CTA_FILTRO_PROT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_FILTRO_PROT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_CP_CTA_FILTRO_PROT
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  NR_SEQ_CP_CTA_FILTRO  NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_CONGENERE      NUMBER(10),
  IE_SITUACAO           VARCHAR2(1 BYTE)        NOT NULL,
  CD_VERSAO_TISS        VARCHAR2(20 BYTE),
  IE_ORIGEM_PROTOCOLO   VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSCPCF_PK ON TASY.PLS_CP_CTA_FILTRO_PROT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPCF_PLCPCTF_FK_I ON TASY.PLS_CP_CTA_FILTRO_PROT
(NR_SEQ_CP_CTA_FILTRO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSCPCF_PLSCONG_FK_I ON TASY.PLS_CP_CTA_FILTRO_PROT
(NR_SEQ_CONGENERE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_CP_CTA_FILTRO_PROT_tp  after update ON TASY.PLS_CP_CTA_FILTRO_PROT FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_PROT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CP_CTA_FILTRO,1,4000),substr(:new.NR_SEQ_CP_CTA_FILTRO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CP_CTA_FILTRO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_PROT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_VERSAO_TISS,1,4000),substr(:new.CD_VERSAO_TISS,1,4000),:new.nm_usuario,nr_seq_w,'CD_VERSAO_TISS',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_PROT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROTOCOLO,1,4000),substr(:new.IE_ORIGEM_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROTOCOLO',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_PROT',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CONGENERE,1,4000),substr(:new.NR_SEQ_CONGENERE,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CONGENERE',ie_log_w,ds_w,'PLS_CP_CTA_FILTRO_PROT',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_CP_CTA_FILTRO_PROT ADD (
  CONSTRAINT PLSCPCF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_CP_CTA_FILTRO_PROT ADD (
  CONSTRAINT PLSCPCF_PLCPCTF_FK 
 FOREIGN KEY (NR_SEQ_CP_CTA_FILTRO) 
 REFERENCES TASY.PLS_CP_CTA_FILTRO (NR_SEQUENCIA),
  CONSTRAINT PLSCPCF_PLSCONG_FK 
 FOREIGN KEY (NR_SEQ_CONGENERE) 
 REFERENCES TASY.PLS_CONGENERE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_CP_CTA_FILTRO_PROT TO NIVEL_1;

