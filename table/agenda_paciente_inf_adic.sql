ALTER TABLE TASY.AGENDA_PACIENTE_INF_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AGENDA_PACIENTE_INF_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGENDA_PACIENTE_INF_ADIC
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_SEQ_AGENDA              NUMBER(10)         NOT NULL,
  CD_CONVENIO                NUMBER(5),
  DS_NUTRICAO                VARCHAR2(2000 BYTE),
  QT_PESO                    NUMBER(10,3),
  VL_PROCEDIMENTO            NUMBER(10,2),
  IE_RESPONSAVEL_CONTA       VARCHAR2(15 BYTE),
  IE_REPASSE_HONORARIOS      VARCHAR2(15 BYTE),
  DS_INFORMACOES_INTERNACAO  VARCHAR2(255 BYTE),
  DT_APROVACAO               DATE,
  NM_PESSOA_CONFIRM          VARCHAR2(255 BYTE),
  DT_CONFIRMACAO             DATE,
  CD_EVOLUCAO                NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGPAINA_AGEPACI_FK_I ON TASY.AGENDA_PACIENTE_INF_ADIC
(NR_SEQ_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPAINA_AGEPACI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGPAINA_CONVENI_FK_I ON TASY.AGENDA_PACIENTE_INF_ADIC
(CD_CONVENIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPAINA_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGPAINA_EVOPACI_FK_I ON TASY.AGENDA_PACIENTE_INF_ADIC
(CD_EVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.AGPAINA_PK ON TASY.AGENDA_PACIENTE_INF_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGPAINA_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.agenda_paciente_inf_adic_atual
before insert or update ON TASY.AGENDA_PACIENTE_INF_ADIC for each row
declare
ds_alteracao_w		varchar2(4000);

begin

if	(updating) then
	if	(nvl(:old.ds_informacoes_internacao,'X') <> nvl(:new.ds_informacoes_internacao,'X')) then
		ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(800130,
								'DS_INFORMACOES_INTERNACAO_OLD='||:old.ds_informacoes_internacao||
								';DS_INFORMACOES_INTERNACAO_NEW='||:new.ds_informacoes_internacao),1,4000);
	end if;
	if	(ds_alteracao_w is not null) then
		gravar_historico_montagem(:new.nr_seq_agenda,'AF',ds_alteracao_w,:new.nm_usuario);
	end if;
elsif	(:new.ds_informacoes_internacao	is not null) then
	ds_alteracao_w	:=	substr(wheb_mensagem_pck.get_texto(800129)||' '||:new.ds_informacoes_internacao,1,4000);

	if	(ds_alteracao_w is not null) then
		gravar_historico_montagem(:new.nr_seq_agenda,'IF',ds_alteracao_w,:new.nm_usuario);
	end if;
end if;


end;
/


CREATE OR REPLACE TRIGGER TASY.agenda_paciente_inf_adic_del
before delete ON TASY.AGENDA_PACIENTE_INF_ADIC for each row
declare
ds_alteracao_w		varchar2(4000);
begin

ds_alteracao_w	:=	substr(obter_desc_expressao(500811)||' '||:old.ds_informacoes_internacao,1,4000);

if	(ds_alteracao_w is not null) then
	gravar_historico_montagem(:old.nr_seq_agenda,'EF',ds_alteracao_w,:old.nm_usuario);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.AGENDA_PACIENTE_INF_ADIC_tp  after update ON TASY.AGENDA_PACIENTE_INF_ADIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_CONVENIO,1,4000),substr(:new.CD_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_CONVENIO',ie_log_w,ds_w,'AGENDA_PACIENTE_INF_ADIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.AGENDA_PACIENTE_INF_ADIC ADD (
  CONSTRAINT AGPAINA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.AGENDA_PACIENTE_INF_ADIC ADD (
  CONSTRAINT AGPAINA_EVOPACI_FK 
 FOREIGN KEY (CD_EVOLUCAO) 
 REFERENCES TASY.EVOLUCAO_PACIENTE (CD_EVOLUCAO),
  CONSTRAINT AGPAINA_AGEPACI_FK 
 FOREIGN KEY (NR_SEQ_AGENDA) 
 REFERENCES TASY.AGENDA_PACIENTE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT AGPAINA_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.AGENDA_PACIENTE_INF_ADIC TO NIVEL_1;

