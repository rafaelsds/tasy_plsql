ALTER TABLE TASY.PCS_VALOR_CURVA_ABC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PCS_VALOR_CURVA_ABC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PCS_VALOR_CURVA_ABC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  NR_SEQ_REGIONAL      NUMBER(10),
  CD_ESTAB_REGRA       NUMBER(4),
  VL_CURVA_A           NUMBER(10)               NOT NULL,
  VL_CURVA_B           NUMBER(10)               NOT NULL,
  VL_CURVA_C           NUMBER(10)               NOT NULL,
  IE_FORMULA_CALCULO   VARCHAR2(15 BYTE)        NOT NULL,
  QT_MESES             NUMBER(10),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PCSVABC_EMPRESA_FK_I ON TASY.PCS_VALOR_CURVA_ABC
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSVABC_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PCSVABC_ESTABEL_FK_I ON TASY.PCS_VALOR_CURVA_ABC
(CD_ESTAB_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSVABC_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PCSVABC_PCSREGI_FK_I ON TASY.PCS_VALOR_CURVA_ABC
(NR_SEQ_REGIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSVABC_PCSREGI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PCSVABC_PK ON TASY.PCS_VALOR_CURVA_ABC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PCSVABC_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PCS_VALOR_CURVA_ABC_tp  after update ON TASY.PCS_VALOR_CURVA_ABC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_EMPRESA,1,4000),substr(:new.CD_EMPRESA,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA',ie_log_w,ds_w,'PCS_VALOR_CURVA_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTAB_REGRA,1,4000),substr(:new.CD_ESTAB_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTAB_REGRA',ie_log_w,ds_w,'PCS_VALOR_CURVA_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGIONAL,1,4000),substr(:new.NR_SEQ_REGIONAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGIONAL',ie_log_w,ds_w,'PCS_VALOR_CURVA_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CURVA_A,1,4000),substr(:new.VL_CURVA_A,1,4000),:new.nm_usuario,nr_seq_w,'VL_CURVA_A',ie_log_w,ds_w,'PCS_VALOR_CURVA_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PCS_VALOR_CURVA_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CURVA_C,1,4000),substr(:new.VL_CURVA_C,1,4000),:new.nm_usuario,nr_seq_w,'VL_CURVA_C',ie_log_w,ds_w,'PCS_VALOR_CURVA_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMULA_CALCULO,1,4000),substr(:new.IE_FORMULA_CALCULO,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMULA_CALCULO',ie_log_w,ds_w,'PCS_VALOR_CURVA_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MESES,1,4000),substr(:new.QT_MESES,1,4000),:new.nm_usuario,nr_seq_w,'QT_MESES',ie_log_w,ds_w,'PCS_VALOR_CURVA_ABC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.VL_CURVA_B,1,4000),substr(:new.VL_CURVA_B,1,4000),:new.nm_usuario,nr_seq_w,'VL_CURVA_B',ie_log_w,ds_w,'PCS_VALOR_CURVA_ABC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PCS_VALOR_CURVA_ABC ADD (
  CONSTRAINT PCSVABC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PCS_VALOR_CURVA_ABC ADD (
  CONSTRAINT PCSVABC_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PCSVABC_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_REGRA) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PCSVABC_PCSREGI_FK 
 FOREIGN KEY (NR_SEQ_REGIONAL) 
 REFERENCES TASY.PCS_REGIONAIS (NR_SEQUENCIA));

GRANT SELECT ON TASY.PCS_VALOR_CURVA_ABC TO NIVEL_1;

