ALTER TABLE TASY.MAN_ORDEM_SERV_TECNICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_ORDEM_SERV_TECNICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_ORDEM_SERV_TECNICO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  NR_SEQ_ORDEM_SERV          NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DS_RELAT_TECNICO           LONG               NOT NULL,
  CD_VERSAO_TASY             VARCHAR2(15 BYTE),
  DT_HISTORICO               DATE,
  IE_ORIGEM                  VARCHAR2(1 BYTE),
  NR_SEQ_TIPO                NUMBER(10),
  NM_USUARIO_LIB             VARCHAR2(15 BYTE),
  DT_LIBERACAO               DATE,
  IE_GRAU_SATISFACAO         VARCHAR2(3 BYTE),
  DT_ENVIO                   DATE,
  IE_RELEVANTE_TESTE         VARCHAR2(1 BYTE),
  NR_SEQ_MOTIVO_DESVIO_PREV  NUMBER(10),
  IE_MTVO_RETORNO_WS         VARCHAR2(15 BYTE),
  DT_INATIVACAO              DATE,
  CD_VERSAO_TESTE            VARCHAR2(20 BYTE),
  CD_BUILD_TESTE             VARCHAR2(5 BYTE),
  NR_ORDEM_BLOQUEIO          NUMBER(10),
  NR_SEQ_LP                  NUMBER(10),
  NR_SEQ_CLASSIF_VV          NUMBER(10),
  NR_SEQ_BASE                NUMBER(10),
  NR_SEQ_ANALISE_VV          NUMBER(5),
  NR_SEQ_SERV_APLIC          NUMBER(10),
  DT_REVISAO_HIST            DATE,
  NM_REVISOR_HIST            VARCHAR2(15 BYTE),
  NR_SEQ_SUPERIOR            NUMBER(10),
  NR_SEQ_IDIOMA              NUMBER(10),
  NR_SEQ_PHILIPS             NUMBER(10),
  IE_STATUS_WS               VARCHAR2(1 BYTE),
  DS_INATIVACAO              VARCHAR2(200 BYTE),
  NR_SEQ_OS_ANT              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANORST_IDIOMA_FK_I ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_IDIOMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_I1 ON TASY.MAN_ORDEM_SERV_TECNICO
(DT_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_I2 ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_ORDEM_SERV, NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_I3 ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_ORDEM_SERV, NM_USUARIO, DT_HISTORICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_LINPROG_FK_I ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_LP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_MAMODET_FK_I ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_MOTIVO_DESVIO_PREV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORST_MAMODET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANORST_MANBASEVV_FK_I ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_BASE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_MANCLASVV_FK_I ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_CLASSIF_VV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_MANORSE_FK_I ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_ORDEM_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          576K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_MANORST_FK_I ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_SUPERIOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_MANTIHI_FK_I ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANORST_MANTIHI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MANORST_PK ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANORST_SERVAPL_FK_I ON TASY.MAN_ORDEM_SERV_TECNICO
(NR_SEQ_SERV_APLIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.man_ordem_serv_tecnico_after
AFTER INSERT ON TASY.MAN_ORDEM_SERV_TECNICO FOR EACH ROW
DECLARE

nr_seq_estagio_w		number(10);
nr_grupo_trabalho_w	number(10);

BEGIN
if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
	goto Final;
end if;

if	(nvl(:new.nr_seq_tipo, 0) > 0) then
	begin
	select	nvl(max(nr_seq_estagio),0),
		nvl(max(nr_grupo_trabalho_atualiz),0)
	into	nr_seq_estagio_w,
		nr_grupo_trabalho_w
	from	man_tipo_hist
	where	nr_sequencia	= :new.nr_seq_tipo;

	if	(nr_seq_estagio_w > 0) then
		update	man_ordem_servico
		set	nr_seq_estagio	= nr_seq_estagio_w,
			nm_usuario	= :new.nm_usuario,
			dt_atualizacao	= :new.dt_atualizacao
		where	nr_sequencia	= :new.nr_seq_ordem_serv;
	end if;
	if	(nr_grupo_trabalho_w > 0) then
		update	man_ordem_servico
		set	nr_grupo_trabalho		= nr_grupo_trabalho_w,
			nm_usuario		= :new.nm_usuario,
			dt_atualizacao		= :new.dt_atualizacao
		where	nr_sequencia		= :new.nr_seq_ordem_serv;
	end if;
	end;
end if;
<<Final>>
null;
END;
/


CREATE OR REPLACE TRIGGER TASY.man_ordem_serv_tecnico_insert
before insert ON TASY.MAN_ORDEM_SERV_TECNICO for each row
declare

cd_versao_atual_w	varchar2(15);

BEGIN

if (:new.DT_LIBERACAO is not null) and
   (TO_CHAR(nvl(:new.DT_LIBERACAO,sysdate),'dd/mm/yyyy') = '00/00/0000') then
	:new.DT_LIBERACAO := null;
end if;

if	(nvl(wheb_usuario_pck.get_ie_executar_trigger,'S') = 'N')  then
	goto Final;
end if;

if	(nvl(:new.cd_versao_tasy, '0') = '0') then
	begin
	select	max(cd_versao_atual)
	into	cd_versao_atual_w
	from	aplicacao_tasy
	where	cd_aplicacao_tasy = 'Tasy';

	if	(nvl(cd_versao_atual_w,'0') <> '0') then
		:new.cd_versao_tasy := cd_versao_atual_w;
	end if;

	end;
end if;
<<Final>>
null;
END;
/


ALTER TABLE TASY.MAN_ORDEM_SERV_TECNICO ADD (
  CONSTRAINT MANORST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          384K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_ORDEM_SERV_TECNICO ADD (
  CONSTRAINT MANORST_SERVAPL_FK 
 FOREIGN KEY (NR_SEQ_SERV_APLIC) 
 REFERENCES TASY.SERVIDOR_APLICACAO (NR_SEQUENCIA),
  CONSTRAINT MANORST_IDIOMA_FK 
 FOREIGN KEY (NR_SEQ_IDIOMA) 
 REFERENCES TASY.IDIOMA (NR_SEQUENCIA),
  CONSTRAINT MANORST_MANORST_FK 
 FOREIGN KEY (NR_SEQ_SUPERIOR) 
 REFERENCES TASY.MAN_ORDEM_SERV_TECNICO (NR_SEQUENCIA),
  CONSTRAINT MANORST_MANBASEVV_FK 
 FOREIGN KEY (NR_SEQ_BASE) 
 REFERENCES TASY.MAN_BASE_VV (NR_SEQUENCIA),
  CONSTRAINT MANORST_LINPROG_FK 
 FOREIGN KEY (NR_SEQ_LP) 
 REFERENCES TASY.LINGUAGEM_PROGRAMACAO (NR_SEQUENCIA),
  CONSTRAINT MANORST_MANCLASVV_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_VV) 
 REFERENCES TASY.MAN_CLASSIF_VV (NR_SEQUENCIA),
  CONSTRAINT MANORST_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM_SERV) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANORST_MANTIHI_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.MAN_TIPO_HIST (NR_SEQUENCIA),
  CONSTRAINT MANORST_MAMODET_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DESVIO_PREV) 
 REFERENCES TASY.MAN_MOTIVO_DESVIO_TEMPO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_ORDEM_SERV_TECNICO TO NIVEL_1;

