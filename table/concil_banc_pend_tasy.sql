ALTER TABLE TASY.CONCIL_BANC_PEND_TASY
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONCIL_BANC_PEND_TASY CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONCIL_BANC_PEND_TASY
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_CONCILIACAO   NUMBER(10)               NOT NULL,
  NR_SEQ_MOVTO_TRANS   NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONBAPT_CONBANC_FK_I ON TASY.CONCIL_BANC_PEND_TASY
(NR_SEQ_CONCILIACAO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONBAPT_CONBANC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.CONBAPT_MOVTRFI_FK_I ON TASY.CONCIL_BANC_PEND_TASY
(NR_SEQ_MOVTO_TRANS)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CONBAPT_PK ON TASY.CONCIL_BANC_PEND_TASY
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONBAPT_PK
  MONITORING USAGE;


ALTER TABLE TASY.CONCIL_BANC_PEND_TASY ADD (
  CONSTRAINT CONBAPT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONCIL_BANC_PEND_TASY ADD (
  CONSTRAINT CONBAPT_MOVTRFI_FK 
 FOREIGN KEY (NR_SEQ_MOVTO_TRANS) 
 REFERENCES TASY.MOVTO_TRANS_FINANC (NR_SEQUENCIA),
  CONSTRAINT CONBAPT_CONBANC_FK 
 FOREIGN KEY (NR_SEQ_CONCILIACAO) 
 REFERENCES TASY.CONCILIACAO_BANCARIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONCIL_BANC_PEND_TASY TO NIVEL_1;

