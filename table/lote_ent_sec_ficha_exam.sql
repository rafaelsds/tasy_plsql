ALTER TABLE TASY.LOTE_ENT_SEC_FICHA_EXAM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOTE_ENT_SEC_FICHA_EXAM CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOTE_ENT_SEC_FICHA_EXAM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_EXAME         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_MATERIAL_EXAME    VARCHAR2(20 BYTE)        NOT NULL,
  NR_SEQ_FICHA         NUMBER(10)               NOT NULL,
  IE_TIPO_COLETA       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LESEFIEX_EXALABO_FK_I ON TASY.LOTE_ENT_SEC_FICHA_EXAM
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESEFIEX_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESEFIEX_LESECFI_FK_I ON TASY.LOTE_ENT_SEC_FICHA_EXAM
(NR_SEQ_FICHA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESEFIEX_LESECFI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.LESEFIEX_MATEXLA_FK_I ON TASY.LOTE_ENT_SEC_FICHA_EXAM
(CD_MATERIAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESEFIEX_MATEXLA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.LESEFIEX_PK ON TASY.LOTE_ENT_SEC_FICHA_EXAM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.LESEFIEX_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.LOTE_ENT_SEC_FICHA_EXAM_tp  after update ON TASY.LOTE_ENT_SEC_FICHA_EXAM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NM_USUARIO_NREC,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA_EXAM',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_EXAME,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_EXAME,1,4000),substr(:new.NR_SEQ_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EXAME',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA_EXAM',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_MATERIAL_EXAME,1,500);gravar_log_alteracao(substr(:old.CD_MATERIAL_EXAME,1,4000),substr(:new.CD_MATERIAL_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'CD_MATERIAL_EXAME',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA_EXAM',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQ_FICHA,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_FICHA,1,4000),substr(:new.NR_SEQ_FICHA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_FICHA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA_EXAM',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO_NREC,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA_EXAM',ds_s_w,ds_c_w);  ds_w:=substr(:new.DT_ATUALIZACAO,1,500);gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA_EXAM',ds_s_w,ds_c_w);  ds_w:=substr(:new.NM_USUARIO,1,500);gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA_EXAM',ds_s_w,ds_c_w);  ds_w:=substr(:new.IE_TIPO_COLETA,1,500);gravar_log_alteracao(substr(:old.IE_TIPO_COLETA,1,4000),substr(:new.IE_TIPO_COLETA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_COLETA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA_EXAM',ds_s_w,ds_c_w);  ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'LOTE_ENT_SEC_FICHA_EXAM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOTE_ENT_SEC_FICHA_EXAM ADD (
  CONSTRAINT LESEFIEX_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOTE_ENT_SEC_FICHA_EXAM ADD (
  CONSTRAINT LESEFIEX_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT LESEFIEX_LESECFI_FK 
 FOREIGN KEY (NR_SEQ_FICHA) 
 REFERENCES TASY.LOTE_ENT_SEC_FICHA (NR_SEQUENCIA));

GRANT SELECT ON TASY.LOTE_ENT_SEC_FICHA_EXAM TO NIVEL_1;

