ALTER TABLE TASY.LISTA_PROBLEMA_STATUS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LISTA_PROBLEMA_STATUS CASCADE CONSTRAINTS;

CREATE TABLE TASY.LISTA_PROBLEMA_STATUS
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_STATUS            VARCHAR2(10 BYTE),
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_INTENSIDADE       VARCHAR2(3 BYTE),
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  QT_INTERVALO_STATUS  NUMBER(10,4),
  NR_SEQ_PROBLEMA      NUMBER(10)               NOT NULL,
  DT_REVISAO           DATE,
  DS_FATO              VARCHAR2(255 BYTE),
  DS_FATOS             VARCHAR2(4000 BYTE),
  IE_MAIN_ENC_PROBL    VARCHAR2(1 BYTE),
  IE_TIPO_DIAGNOSTICO  NUMBER(3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LIPRSTA_LIPRPAC_FK_I ON TASY.LISTA_PROBLEMA_STATUS
(NR_SEQ_PROBLEMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LIPRSTA_PK ON TASY.LISTA_PROBLEMA_STATUS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.LISTA_PROBLEMA_STATUS ADD (
  CONSTRAINT LIPRSTA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LISTA_PROBLEMA_STATUS ADD (
  CONSTRAINT LIPRSTA_LIPRPAC_FK 
 FOREIGN KEY (NR_SEQ_PROBLEMA) 
 REFERENCES TASY.LISTA_PROBLEMA_PAC (NR_SEQUENCIA));

GRANT SELECT ON TASY.LISTA_PROBLEMA_STATUS TO NIVEL_1;

