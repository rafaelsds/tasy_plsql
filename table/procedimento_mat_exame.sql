ALTER TABLE TASY.PROCEDIMENTO_MAT_EXAME
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCEDIMENTO_MAT_EXAME CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCEDIMENTO_MAT_EXAME
(
  CD_PROCEDIMENTO    NUMBER(15)                 NOT NULL,
  IE_ORIGEM_PROCED   NUMBER(10)                 NOT NULL,
  CD_MATERIAL_EXAME  VARCHAR2(20 BYTE),
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PROMAEXA_PK ON TASY.PROCEDIMENTO_MAT_EXAME
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED, CD_MATERIAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROMAEXA_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROMAEXA_PROCED_FK_I_I ON TASY.PROCEDIMENTO_MAT_EXAME
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROMAEXA_PROCED_FK_I_I
  MONITORING USAGE;


CREATE INDEX TASY.PROMAEX_MATEXLA_FK_I ON TASY.PROCEDIMENTO_MAT_EXAME
(CD_MATERIAL_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROMAEX_MATEXLA_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROCEDIMENTO_MAT_EXAME ADD (
  CONSTRAINT PROMAEXA_PK
 PRIMARY KEY
 (CD_PROCEDIMENTO, IE_ORIGEM_PROCED, CD_MATERIAL_EXAME)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCEDIMENTO_MAT_EXAME ADD (
  CONSTRAINT PROMAEX_MATEXLA_FK 
 FOREIGN KEY (CD_MATERIAL_EXAME) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (CD_MATERIAL_EXAME),
  CONSTRAINT PROMAEXA_PROCED_FK_I 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PROCEDIMENTO_MAT_EXAME TO NIVEL_1;

