ALTER TABLE TASY.PROCED_AUTOR_CIRURGIA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROCED_AUTOR_CIRURGIA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROCED_AUTOR_CIRURGIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_AUTORIZACAO   NUMBER(10)               NOT NULL,
  CD_PROCEDIMENTO      NUMBER(15)               NOT NULL,
  IE_ORIGEM_PROCED     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  QT_PROCEDIMENTO      NUMBER(15,3),
  NR_SEQ_PROC_INTERNO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRAUTCI_AUTCIRU_FK_I ON TASY.PROCED_AUTOR_CIRURGIA
(NR_SEQ_AUTORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRAUTCI_AUTCIRU_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRAUTCI_PK ON TASY.PROCED_AUTOR_CIRURGIA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRAUTCI_PROCEDI_FK_I ON TASY.PROCED_AUTOR_CIRURGIA
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRAUTCI_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRAUTCI_PROINTE_FK_I ON TASY.PROCED_AUTOR_CIRURGIA
(NR_SEQ_PROC_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRAUTCI_PROINTE_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.proced_autor_cirurgia_insert
after insert on proced_autor_cirurgia
for each row
declare

nr_sequencia_autor_w	number(10);

begin

select	max(nr_sequencia)
into	nr_sequencia_autor_w
from	autorizacao_convenio
where	nr_seq_autor_cirurgia	= :new.nr_seq_autorizacao;

if	(nr_sequencia_autor_w is not null) then

	insert	into	procedimento_autorizado
		(nr_sequencia,
		nm_usuario,
		dt_atualizacao,
		nr_sequencia_autor,
		cd_procedimento,
		ie_origem_proced,
		qt_solicitada,
		qt_autorizada)
	values	(procedimento_autorizado_seq.nextval,
		:new.nm_usuario,
		:new.dt_atualizacao,
		nr_sequencia_autor_w,
		:new.cd_procedimento,
		:new.ie_origem_proced,
		:new.qt_procedimento,
		0);
end if;

end;
/


ALTER TABLE TASY.PROCED_AUTOR_CIRURGIA ADD (
  CONSTRAINT PRAUTCI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROCED_AUTOR_CIRURGIA ADD (
  CONSTRAINT PRAUTCI_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PRAUTCI_AUTCIRU_FK 
 FOREIGN KEY (NR_SEQ_AUTORIZACAO) 
 REFERENCES TASY.AUTORIZACAO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PRAUTCI_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED));

GRANT SELECT ON TASY.PROCED_AUTOR_CIRURGIA TO NIVEL_1;

