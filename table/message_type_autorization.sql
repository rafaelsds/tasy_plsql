ALTER TABLE TASY.MESSAGE_TYPE_AUTORIZATION
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MESSAGE_TYPE_AUTORIZATION CASCADE CONSTRAINTS;

CREATE TABLE TASY.MESSAGE_TYPE_AUTORIZATION
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_INSUR_AUT     NUMBER(10)               NOT NULL,
  IE_TYPE_MESSAGE      VARCHAR2(3 BYTE)         NOT NULL,
  CD_USER_SERVICE      VARCHAR2(50 BYTE),
  CD_PASSWORD          VARCHAR2(50 BYTE),
  DS_OBSERVACAO        VARCHAR2(4000 BYTE),
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.METYAUT_INAUTSER_FK_I ON TASY.MESSAGE_TYPE_AUTORIZATION
(NR_SEQ_INSUR_AUT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.METYAUT_PK ON TASY.MESSAGE_TYPE_AUTORIZATION
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MESSAGE_TYPE_AUTORIZATION ADD (
  CONSTRAINT METYAUT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.MESSAGE_TYPE_AUTORIZATION ADD (
  CONSTRAINT METYAUT_INAUTSER_FK 
 FOREIGN KEY (NR_SEQ_INSUR_AUT) 
 REFERENCES TASY.INSURANCE_AUT_SERVICE (NR_SEQUENCIA));

GRANT SELECT ON TASY.MESSAGE_TYPE_AUTORIZATION TO NIVEL_1;

