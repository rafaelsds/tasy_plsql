ALTER TABLE TASY.PE_PRESCR_PROC_HOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_PRESCR_PROC_HOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_PRESCR_PROC_HOR
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  NR_SEQ_PE_PRESCR          NUMBER(10)          NOT NULL,
  NR_SEQ_PE_PROC            NUMBER(10)          NOT NULL,
  NR_SEQ_PROC               NUMBER(10)          NOT NULL,
  DT_HORARIO                DATE,
  DT_FIM_HORARIO            DATE,
  DT_SUSPENSAO              DATE,
  IE_HORARIO_ESPECIAL       VARCHAR2(1 BYTE),
  NM_USUARIO_REAPRAZAMENTO  VARCHAR2(15 BYTE),
  QT_HOR_REAPRAZAMENTO      NUMBER(10),
  IE_APRAZADO               VARCHAR2(1 BYTE)    NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE),
  IE_CHECAGEM               VARCHAR2(1 BYTE),
  NM_USUARIO_CHECAGEM       VARCHAR2(15 BYTE),
  DT_CHECAGEM               DATE,
  CD_SETOR_EXEC             NUMBER(5),
  NM_USUARIO_ADM            VARCHAR2(15 BYTE),
  NM_USUARIO_SUSP           VARCHAR2(15 BYTE),
  DT_LIB_HORARIO            DATE,
  IE_PENDENTE               VARCHAR2(1 BYTE),
  DT_RECUSA                 DATE,
  CD_EVOLUCAO               NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEPRHOR_I1 ON TASY.PE_PRESCR_PROC_HOR
(DT_HORARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRHOR_PEHOR_FK_I ON TASY.PE_PRESCR_PROC_HOR
(NR_SEQ_PE_PRESCR, NR_SEQ_PE_PROC, DT_HORARIO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRHOR_PEPRESC_FK_I ON TASY.PE_PRESCR_PROC_HOR
(NR_SEQ_PE_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRHOR_PEPROCE_FK_I ON TASY.PE_PRESCR_PROC_HOR
(NR_SEQ_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRHOR_PEPROCE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEPRHOR_PK ON TASY.PE_PRESCR_PROC_HOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEPRHOR_SETATEN_FK_I ON TASY.PE_PRESCR_PROC_HOR
(CD_SETOR_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEPRHOR_SETATEN_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PE_PRESCR_PROC_HOR ADD (
  CONSTRAINT PEPRHOR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_PRESCR_PROC_HOR ADD (
  CONSTRAINT PEPRHOR_PEPRESC_FK 
 FOREIGN KEY (NR_SEQ_PE_PROC) 
 REFERENCES TASY.PE_PRESCR_PROC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEPRHOR_PEPROCE_FK 
 FOREIGN KEY (NR_SEQ_PROC) 
 REFERENCES TASY.PE_PROCEDIMENTO (NR_SEQUENCIA),
  CONSTRAINT PEPRHOR_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_EXEC) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.PE_PRESCR_PROC_HOR TO NIVEL_1;

