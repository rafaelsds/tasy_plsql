ALTER TABLE TASY.PLS_GRUPO_CONTRATO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_GRUPO_CONTRATO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_GRUPO_CONTRATO
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DS_GRUPO                       VARCHAR2(255 BYTE) NOT NULL,
  CD_ESTABELECIMENTO             NUMBER(4)      NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  CD_CGC                         VARCHAR2(14 BYTE),
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  DT_REAJUSTE                    DATE,
  IE_TIPO_GRUPO                  VARCHAR2(2 BYTE),
  IE_TIPO_RELACIONAMENTO         VARCHAR2(2 BYTE) NOT NULL,
  CD_GRUPO                       VARCHAR2(20 BYTE),
  DT_COMERCIALIZACAO             DATE,
  NR_MES_REAJUSTE                NUMBER(2),
  DT_INICIO_VIGENCIA             DATE,
  DT_FIM_VIGENCIA                DATE,
  IE_TIPO_AGRUPAMENTO_EXCLUSIVO  NUMBER(2),
  DT_INICIO_ANALISE              DATE           DEFAULT null,
  DT_FIM_ANALISE                 DATE           DEFAULT null,
  IE_EXIBIR_PORTAL               VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSGRCO_ESTABEL_FK_I ON TASY.PLS_GRUPO_CONTRATO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSGRCO_PESJURI_FK_I ON TASY.PLS_GRUPO_CONTRATO
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSGRCO_PK ON TASY.PLS_GRUPO_CONTRATO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_grupo_contrato_befupdate
before update ON TASY.PLS_GRUPO_CONTRATO for each row
declare

begin

null;

end;
/


ALTER TABLE TASY.PLS_GRUPO_CONTRATO ADD (
  CONSTRAINT PLSGRCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_GRUPO_CONTRATO ADD (
  CONSTRAINT PLSGRCO_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSGRCO_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC));

GRANT SELECT ON TASY.PLS_GRUPO_CONTRATO TO NIVEL_1;

