ALTER TABLE TASY.PLS_OC_REGRA_GRUPO_PARTIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_REGRA_GRUPO_PARTIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_REGRA_GRUPO_PARTIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_REGRA_PARTIC  NUMBER(10)               NOT NULL,
  DS_GRUPO             VARCHAR2(255 BYTE)       NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSORGP_PK ON TASY.PLS_OC_REGRA_GRUPO_PARTIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSORGP_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSORGP_PLSOCRP_FK_I ON TASY.PLS_OC_REGRA_GRUPO_PARTIC
(NR_SEQ_REGRA_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSORGP_PLSOCRP_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_REGRA_GRUPO_PARTIC_tp  after update ON TASY.PLS_OC_REGRA_GRUPO_PARTIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.DS_GRUPO,1,4000),substr(:new.DS_GRUPO,1,4000),:new.nm_usuario,nr_seq_w,'DS_GRUPO',ie_log_w,ds_w,'PLS_OC_REGRA_GRUPO_PARTIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_PARTIC,1,4000),substr(:new.NR_SEQ_REGRA_PARTIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_PARTIC',ie_log_w,ds_w,'PLS_OC_REGRA_GRUPO_PARTIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_oc_regra_gp_partic_atual
before insert or update or delete ON TASY.PLS_OC_REGRA_GRUPO_PARTIC for each row
declare
nr_seq_regra_w	pls_oc_regra_participante.nr_sequencia%type;
begin

-- se for apagar o registro
if	(deleting) then
	nr_seq_regra_w := :old.nr_seq_regra_partic;

elsif	(updating) then
	-- hoje, 16/02/2015 n�o existe nenhuma situa��o de atualiza��o que cause alguma
	-- mudan�a nos itens da regra, por isso n�o � necess�rio atualizar nada
	nr_seq_regra_w := null;

else
	-- inser��o ou atualiza��o
	nr_seq_regra_w := :new.nr_seq_regra_partic;
end if;

-- se tiver a regra sinaliza que � necess�rio atualizar a tabela
if	(nr_seq_regra_w is not null) then
	pls_gerencia_upd_obj_pck.marcar_para_atualizacao(	'PLS_GRUPO_PARTIC_TM', wheb_usuario_pck.get_nm_usuario,
								'PLS_OC_REGRA_GP_PARTIC_ATUAL',
								'nr_seq_regra_p=' || nr_seq_regra_w);
end if;

end pls_oc_regra_gp_partic_atual;
/


ALTER TABLE TASY.PLS_OC_REGRA_GRUPO_PARTIC ADD (
  CONSTRAINT PLSORGP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_REGRA_GRUPO_PARTIC ADD (
  CONSTRAINT PLSORGP_PLSOCRP_FK 
 FOREIGN KEY (NR_SEQ_REGRA_PARTIC) 
 REFERENCES TASY.PLS_OC_REGRA_PARTICIPANTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_REGRA_GRUPO_PARTIC TO NIVEL_1;

