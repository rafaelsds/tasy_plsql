ALTER TABLE TASY.REGRA_CONVENIO_PLANO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_CONVENIO_PLANO CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_CONVENIO_PLANO
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  CD_CONVENIO               NUMBER(5)           NOT NULL,
  CD_PLANO                  VARCHAR2(10 BYTE),
  IE_REGRA                  NUMBER(2)           NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  IE_TIPO_ATENDIMENTO       NUMBER(3),
  CD_SETOR_ATENDIMENTO      NUMBER(5),
  CD_AREA_PROCEDIMENTO      NUMBER(15),
  CD_ESPECIALIDADE_PROC     NUMBER(15),
  CD_GRUPO_PROC             NUMBER(15),
  CD_PROCEDIMENTO           NUMBER(15),
  IE_ORIGEM_PROCED          NUMBER(10),
  CD_TIPO_ACOMODACAO        NUMBER(4),
  IE_VALOR                  VARCHAR2(2 BYTE)    NOT NULL,
  QT_PONTO_MIN              NUMBER(15,4),
  QT_PONTO_MAX              NUMBER(15,4),
  NR_SEQ_EXAME              NUMBER(10),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  CD_CLASSIF_SETOR          VARCHAR2(2 BYTE),
  IE_NOVA_AUTORIZACAO       VARCHAR2(1 BYTE)    NOT NULL,
  NR_SEQ_PROC_INTERNO       NUMBER(10),
  IE_REGRA_VALOR            VARCHAR2(3 BYTE)    NOT NULL,
  IE_RESP_AUTOR             VARCHAR2(3 BYTE),
  IE_CARATER_INTER_SUS      VARCHAR2(2 BYTE),
  IE_CLINICA                NUMBER(5),
  DT_INICIO_VIGENCIA        DATE,
  DT_FIM_VIGENCIA           DATE,
  NR_SEQ_GRUPO              NUMBER(10),
  NR_SEQ_SUBGRUPO           NUMBER(10),
  NR_SEQ_FORMA_ORG          NUMBER(10),
  IE_PRIORIDADE             VARCHAR2(1 BYTE)    NOT NULL,
  CD_CATEGORIA              VARCHAR2(10 BYTE),
  DS_MASCARA_CARTEIRA       VARCHAR2(30 BYTE),
  CD_ESTABELECIMENTO        NUMBER(4),
  CD_DOENCA                 VARCHAR2(10 BYTE),
  IE_AUTOR_KIT              VARCHAR2(1 BYTE)    NOT NULL,
  CD_SETOR_ENTREGA_PRESCR   NUMBER(5),
  IE_AUTOR_PARTICULAR       VARCHAR2(1 BYTE),
  CD_EMPRESA_CONV           NUMBER(10),
  DS_INCONSIST_PRESCR       VARCHAR2(255 BYTE),
  QT_MINIMO                 NUMBER(9,3),
  QT_MAXIMO                 NUMBER(9,3),
  IE_QT_TOTAL_AUTOR         VARCHAR2(1 BYTE),
  CD_SETOR_ATUAL            NUMBER(10),
  NR_SEQ_COBERTURA          NUMBER(10),
  NR_SEQ_CLASSIF_ATEND      NUMBER(10),
  CD_MEDICO_EXECUTOR        VARCHAR2(10 BYTE),
  NR_SEQ_CLASSIF            NUMBER(10),
  IE_SOMENTE_ITEM           VARCHAR2(1 BYTE),
  QT_IDADE_MIN              NUMBER(3),
  QT_IDADE_MAX              NUMBER(3),
  CD_PERFIL                 NUMBER(5),
  IE_SITUACAO               VARCHAR2(1 BYTE),
  CD_EDICAO_AMB             NUMBER(6),
  NR_SEQ_CLASSIF_PROC_INT   NUMBER(10),
  CD_ESPECIALIDADE_MEDIC    NUMBER(5),
  QT_DIAS_AUTORIZACAO       NUMBER(3),
  CD_PESSOA_FISICA          VARCHAR2(10 BYTE),
  IE_CARATER_CIRURGIA       VARCHAR2(1 BYTE),
  NR_SEQ_CBHPM_EDICAO       NUMBER(10),
  IE_EXIGE_JUST_MEDICA      VARCHAR2(1 BYTE),
  IE_CHECKUP                VARCHAR2(1 BYTE),
  NR_SEQ_ESTAGIO            NUMBER(10),
  DS_INCONSIST_PEP          VARCHAR2(255 BYTE),
  IE_TISS_TIPO_ETAPA_AUTOR  VARCHAR2(2 BYTE),
  QT_BONUS                  NUMBER(10),
  IE_VALIDA_BONUS_CONV      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.REGCOPL_AREPROC_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_CATCONV_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_CONVENIO, CD_CATEGORIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCOPL_CIDDOEN_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_CIDDOEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_CLAATEN_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_CLASSIF_ATEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_CLAATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_CLAUTOR_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_CLAUTOR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_CONPLAN_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_CONVENIO, CD_PLANO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_CONPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_CONVCOB_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_COBERTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_CONVCOB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_EDIAMB_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_EDICAO_AMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_EDIAMB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_EMPREFE_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_EMPRESA_CONV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_ESPMEDI_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_ESPECIALIDADE_MEDIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_ESPMEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_ESPPROC_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_ESPECIALIDADE_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_ESTABEL_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_ESTABELECIMENTO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_ESTAUTO_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCOPL_EXALABO_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_GRUPROC_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_PERFIL_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_PESFISI_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_MEDICO_EXECUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCOPL_PESFISI_FK2_I ON TASY.REGRA_CONVENIO_PLANO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.REGCOPL_PK ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REGCOPL_PROCEDI_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_PROINTCLA_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_CLASSIF_PROC_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_PROINTCLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_PROINTE_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_PROC_INTERNO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_PROINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_SETATEN_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_SETATEN_FK2_I ON TASY.REGRA_CONVENIO_PLANO
(CD_SETOR_ENTREGA_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_SETATEN_FK3_I ON TASY.REGRA_CONVENIO_PLANO
(CD_SETOR_ATUAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_SETATEN_FK3_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_SUSFORG_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_FORMA_ORG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_SUSFORG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_SUSGRUP_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_SUSGRUP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_SUSSUBG_FK_I ON TASY.REGRA_CONVENIO_PLANO
(NR_SEQ_SUBGRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_SUSSUBG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.REGCOPL_TIPACOM_FK_I ON TASY.REGRA_CONVENIO_PLANO
(CD_TIPO_ACOMODACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.REGCOPL_TIPACOM_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.REGRA_CONVENIO_PLANO_tp  after update ON TASY.REGRA_CONVENIO_PLANO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQUENCIA,1,500);gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA,1,4000),substr(:new.IE_REGRA,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_SETOR_ATENDIMENTO,1,4000),substr(:new.CD_SETOR_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_SETOR_ATENDIMENTO',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ATENDIMENTO,1,4000),substr(:new.IE_TIPO_ATENDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ATENDIMENTO',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_AREA_PROCEDIMENTO,1,4000),substr(:new.CD_AREA_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_AREA_PROCEDIMENTO',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESPECIALIDADE_PROC,1,4000),substr(:new.CD_ESPECIALIDADE_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESPECIALIDADE_PROC',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PLANO,1,4000),substr(:new.CD_PLANO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PLANO',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_GRUPO_PROC,1,4000),substr(:new.CD_GRUPO_PROC,1,4000),:new.nm_usuario,nr_seq_w,'CD_GRUPO_PROC',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_ACOMODACAO,1,4000),substr(:new.CD_TIPO_ACOMODACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_ACOMODACAO',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALOR,1,4000),substr(:new.IE_VALOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALOR',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REGRA_VALOR,1,4000),substr(:new.IE_REGRA_VALOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_REGRA_VALOR',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CARATER_INTER_SUS,1,4000),substr(:new.IE_CARATER_INTER_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_CARATER_INTER_SUS',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLINICA,1,4000),substr(:new.IE_CLINICA,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLINICA',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROC_INTERNO,1,4000),substr(:new.NR_SEQ_PROC_INTERNO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROC_INTERNO',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_INICIO_VIGENCIA,1,4000),substr(:new.DT_INICIO_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_INICIO_VIGENCIA',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_FIM_VIGENCIA,1,4000),substr(:new.DT_FIM_VIGENCIA,1,4000),:new.nm_usuario,nr_seq_w,'DT_FIM_VIGENCIA',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NOVA_AUTORIZACAO,1,4000),substr(:new.IE_NOVA_AUTORIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NOVA_AUTORIZACAO',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'REGRA_CONVENIO_PLANO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.REGRA_CONVENIO_PLANO ADD (
  CONSTRAINT REGCOPL_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.REGRA_CONVENIO_PLANO ADD (
  CONSTRAINT REGCOPL_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REGCOPL_ESTAUTO_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.ESTAGIO_AUTORIZACAO (NR_SEQUENCIA),
  CONSTRAINT REGCOPL_ESPMEDI_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_MEDIC) 
 REFERENCES TASY.ESPECIALIDADE_MEDICA (CD_ESPECIALIDADE),
  CONSTRAINT REGCOPL_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT REGCOPL_TIPACOM_FK 
 FOREIGN KEY (CD_TIPO_ACOMODACAO) 
 REFERENCES TASY.TIPO_ACOMODACAO (CD_TIPO_ACOMODACAO),
  CONSTRAINT REGCOPL_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGCOPL_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT REGCOPL_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC)
    ON DELETE CASCADE,
  CONSTRAINT REGCOPL_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED)
    ON DELETE CASCADE,
  CONSTRAINT REGCOPL_CONPLAN_FK 
 FOREIGN KEY (CD_CONVENIO, CD_PLANO) 
 REFERENCES TASY.CONVENIO_PLANO (CD_CONVENIO,CD_PLANO)
    ON DELETE CASCADE,
  CONSTRAINT REGCOPL_PROINTE_FK 
 FOREIGN KEY (NR_SEQ_PROC_INTERNO) 
 REFERENCES TASY.PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT REGCOPL_SUSGRUP_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.SUS_GRUPO (NR_SEQUENCIA),
  CONSTRAINT REGCOPL_SUSSUBG_FK 
 FOREIGN KEY (NR_SEQ_SUBGRUPO) 
 REFERENCES TASY.SUS_SUBGRUPO (NR_SEQUENCIA),
  CONSTRAINT REGCOPL_SUSFORG_FK 
 FOREIGN KEY (NR_SEQ_FORMA_ORG) 
 REFERENCES TASY.SUS_FORMA_ORGANIZACAO (NR_SEQUENCIA),
  CONSTRAINT REGCOPL_CATCONV_FK 
 FOREIGN KEY (CD_CONVENIO, CD_CATEGORIA) 
 REFERENCES TASY.CATEGORIA_CONVENIO (CD_CONVENIO,CD_CATEGORIA),
  CONSTRAINT REGCOPL_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT REGCOPL_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT REGCOPL_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ENTREGA_PRESCR) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGCOPL_PROINTCLA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_PROC_INT) 
 REFERENCES TASY.PROC_INTERNO_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT REGCOPL_EDIAMB_FK 
 FOREIGN KEY (CD_EDICAO_AMB) 
 REFERENCES TASY.EDICAO_AMB (CD_EDICAO_AMB),
  CONSTRAINT REGCOPL_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT REGCOPL_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE_PROC) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE)
    ON DELETE CASCADE,
  CONSTRAINT REGCOPL_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_CONV) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT REGCOPL_SETATEN_FK3 
 FOREIGN KEY (CD_SETOR_ATUAL) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT REGCOPL_CONVCOB_FK 
 FOREIGN KEY (NR_SEQ_COBERTURA) 
 REFERENCES TASY.CONVENIO_COBERTURA (NR_SEQUENCIA),
  CONSTRAINT REGCOPL_CLAATEN_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF_ATEND) 
 REFERENCES TASY.CLASSIFICACAO_ATENDIMENTO (NR_SEQUENCIA),
  CONSTRAINT REGCOPL_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_EXECUTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT REGCOPL_CLAUTOR_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.CLASSIF_AUTORIZACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_CONVENIO_PLANO TO NIVEL_1;

GRANT SELECT ON TASY.REGRA_CONVENIO_PLANO TO ROBOCMTECNOLOGIA;

