ALTER TABLE TASY.QUA_REGRA_DEFINE_OS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QUA_REGRA_DEFINE_OS CASCADE CONSTRAINTS;

CREATE TABLE TASY.QUA_REGRA_DEFINE_OS
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DS_REGRA              VARCHAR2(255 BYTE)      NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_TIPO_OS_QUALIDADE  VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA    DATE,
  DT_FIM_VIGENCIA       DATE,
  NR_SEQ_ESTAGIO        NUMBER(10),
  QT_DIA                NUMBER(5),
  CD_EXP_REGRA          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.QUARDOS_DICEXPR_FK_I ON TASY.QUA_REGRA_DEFINE_OS
(CD_EXP_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.QUARDOS_MAESTPR_FK_I ON TASY.QUA_REGRA_DEFINE_OS
(NR_SEQ_ESTAGIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUARDOS_MAESTPR_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.QUARDOS_PK ON TASY.QUA_REGRA_DEFINE_OS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.QUARDOS_PK
  MONITORING USAGE;


ALTER TABLE TASY.QUA_REGRA_DEFINE_OS ADD (
  CONSTRAINT QUARDOS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.QUA_REGRA_DEFINE_OS ADD (
  CONSTRAINT QUARDOS_MAESTPR_FK 
 FOREIGN KEY (NR_SEQ_ESTAGIO) 
 REFERENCES TASY.MAN_ESTAGIO_PROCESSO (NR_SEQUENCIA),
  CONSTRAINT QUARDOS_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_REGRA) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.QUA_REGRA_DEFINE_OS TO NIVEL_1;

