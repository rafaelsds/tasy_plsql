ALTER TABLE TASY.PEPO_MODELO_EQUIPAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PEPO_MODELO_EQUIPAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PEPO_MODELO_EQUIPAMENTO
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_EQUIPAMENTO        NUMBER(10)              NOT NULL,
  QT_EQUIPAMENTO        NUMBER(10)              NOT NULL,
  NR_SEQ_TOPOGRAFIA     NUMBER(10),
  NR_SEQ_MODELO_EVENTO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEMODEQ_EQUIPAM_FK_I ON TASY.PEPO_MODELO_EQUIPAMENTO
(CD_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEMODEQ_PEMODEV_FK_I ON TASY.PEPO_MODELO_EQUIPAMENTO
(NR_SEQ_MODELO_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEMODEQ_PK ON TASY.PEPO_MODELO_EQUIPAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEMODEQ_TOPDOR_FK_I ON TASY.PEPO_MODELO_EQUIPAMENTO
(NR_SEQ_TOPOGRAFIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PEPO_MODELO_EQUIPAMENTO ADD (
  CONSTRAINT PEMODEQ_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PEPO_MODELO_EQUIPAMENTO ADD (
  CONSTRAINT PEMODEQ_EQUIPAM_FK 
 FOREIGN KEY (CD_EQUIPAMENTO) 
 REFERENCES TASY.EQUIPAMENTO (CD_EQUIPAMENTO),
  CONSTRAINT PEMODEQ_TOPDOR_FK 
 FOREIGN KEY (NR_SEQ_TOPOGRAFIA) 
 REFERENCES TASY.TOPOGRAFIA_DOR (NR_SEQUENCIA),
  CONSTRAINT PEMODEQ_PEMODEV_FK 
 FOREIGN KEY (NR_SEQ_MODELO_EVENTO) 
 REFERENCES TASY.PEPO_MODELO_EVENTO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PEPO_MODELO_EQUIPAMENTO TO NIVEL_1;

