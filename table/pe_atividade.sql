ALTER TABLE TASY.PE_ATIVIDADE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PE_ATIVIDADE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PE_ATIVIDADE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_ATIVIDADE         VARCHAR2(1000 BYTE)      NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_GRUPO         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_ESTABELECIMENTO   NUMBER(4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEATIVI_ESTABEL_FK_I ON TASY.PE_ATIVIDADE
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEATIVI_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEATIVI_PEGRUAT_FK_I ON TASY.PE_ATIVIDADE
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEATIVI_PEGRUAT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PEATIVI_PK ON TASY.PE_ATIVIDADE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PE_ATIVIDADE ADD (
  CONSTRAINT PEATIVI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PE_ATIVIDADE ADD (
  CONSTRAINT PEATIVI_PEGRUAT_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.PE_GRUPO_ATIVIDADE (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT PEATIVI_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.PE_ATIVIDADE TO NIVEL_1;

