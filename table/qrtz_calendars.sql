ALTER TABLE TASY.QRTZ_CALENDARS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.QRTZ_CALENDARS CASCADE CONSTRAINTS;

CREATE TABLE TASY.QRTZ_CALENDARS
(
  SCHED_NAME     VARCHAR2(120 BYTE)             NOT NULL,
  CALENDAR_NAME  VARCHAR2(200 BYTE)             NOT NULL,
  CALENDAR       BLOB                           NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (CALENDAR) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.QRTCALE_PK ON TASY.QRTZ_CALENDARS
(SCHED_NAME, CALENDAR_NAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          32K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.QRTZ_CALENDARS ADD (
  CONSTRAINT QRTCALE_PK
 PRIMARY KEY
 (SCHED_NAME, CALENDAR_NAME)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          32K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.QRTZ_CALENDARS TO NIVEL_1;

