ALTER TABLE TASY.COMPONENTE_KIT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMPONENTE_KIT CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMPONENTE_KIT
(
  CD_KIT_MATERIAL           NUMBER(5)           NOT NULL,
  NR_SEQUENCIA              NUMBER(5)           NOT NULL,
  CD_MATERIAL               NUMBER(6)           NOT NULL,
  IE_VIA_APLICACAO          VARCHAR2(5 BYTE),
  IE_TIPO_PACIENTE          VARCHAR2(3 BYTE),
  QT_MATERIAL               NUMBER(11,4)        NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  QT_VOLUME_MIN             NUMBER(5,2),
  QT_VOLUME_MAX             NUMBER(5,2),
  IE_COBRA_PACIENTE         VARCHAR2(1 BYTE),
  IE_BAIXA_ESTOQUE          VARCHAR2(1 BYTE),
  CD_MEDICO                 VARCHAR2(10 BYTE),
  CD_CONVENIO               NUMBER(5),
  IE_DISPENSAVEL            VARCHAR2(1 BYTE)    NOT NULL,
  IE_VIDEO                  VARCHAR2(1 BYTE)    NOT NULL,
  CD_FORNECEDOR             VARCHAR2(14 BYTE),
  QT_IDADE_MINIMA           NUMBER(3),
  QT_IDADE_MAXIMA           NUMBER(3),
  IE_CALCULA_PRECO          VARCHAR2(1 BYTE),
  IE_ENTRA_CONTA            VARCHAR2(1 BYTE)    NOT NULL,
  CD_INTERVALO              VARCHAR2(7 BYTE),
  IE_DUPLICA_PRESCR         VARCHAR2(1 BYTE),
  IE_MULTIPLICA_DOSE        VARCHAR2(1 BYTE),
  CD_MOTIVO_BAIXA           NUMBER(3),
  CD_UNIDADE_MEDIDA_PRESCR  VARCHAR2(30 BYTE),
  IE_DUPLICA_REQ            VARCHAR2(1 BYTE),
  IE_SEXO                   VARCHAR2(1 BYTE),
  IE_RECEM_NATO             VARCHAR2(1 BYTE),
  CD_MATERIAL_ORIGINAL      NUMBER(6),
  IE_PERMITE_ALTERAR        VARCHAR2(1 BYTE),
  DS_OBSERVACAO             VARCHAR2(255 BYTE),
  CD_ESTAB_REGRA            NUMBER(4),
  IE_FORMA_BAIXA            VARCHAR2(1 BYTE),
  NR_SEQ_APRESENTACAO       NUMBER(10),
  QT_PESO_MAX               NUMBER(6,3),
  QT_PESO_MIN               NUMBER(6,3),
  CD_SETOR_ATENDIMENTO      NUMBER(5),
  IE_GERAR_SOLUCAO          VARCHAR2(1 BYTE),
  IE_TIPO_CONVENIO          NUMBER(2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMKIT_CONVENI_FK_I ON TASY.COMPONENTE_KIT
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMKIT_ESTABEL_FK_I ON TASY.COMPONENTE_KIT
(CD_ESTAB_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMKIT_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMKIT_KITMATE_FK_I ON TASY.COMPONENTE_KIT
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMKIT_MATERIA_FK_I ON TASY.COMPONENTE_KIT
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMKIT_MATERIA_FK2_I ON TASY.COMPONENTE_KIT
(CD_MATERIAL_ORIGINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMKIT_MATERIA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.COMKIT_MEDICO_FK_I ON TASY.COMPONENTE_KIT
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMKIT_PESJURI_FK_I ON TASY.COMPONENTE_KIT
(CD_FORNECEDOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMKIT_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COMKIT_PK ON TASY.COMPONENTE_KIT
(CD_KIT_MATERIAL, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMKIT_SETATEN_FK_I ON TASY.COMPONENTE_KIT
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMKIT_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COMKIT_VIAAPLI_FK_I ON TASY.COMPONENTE_KIT
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COMKIT_VIAAPLI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.componente_kit_atual
before insert or update ON TASY.COMPONENTE_KIT for each row
declare

nr_seq_interna_w	number(10);

begin

if	(obter_se_prot_lib_regras = 'S') then
	select	nr_seq_protocolo
	into	nr_seq_interna_w
	from	kit_material
	where	cd_kit_material	= :new.cd_kit_material;

	update	protocolo_medicacao
	set	nm_usuario_aprov	=	null,
		dt_aprovacao		=	null,
		ie_status		=	'PA'
	where	nr_seq_interna		=	nr_seq_interna_w;
end if;

end;
/


ALTER TABLE TASY.COMPONENTE_KIT ADD (
  CONSTRAINT COMKIT_PK
 PRIMARY KEY
 (CD_KIT_MATERIAL, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COMPONENTE_KIT ADD (
  CONSTRAINT COMKIT_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT COMKIT_CONVENI 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT COMKIT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT COMKIT_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT COMKIT_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT COMKIT_PESJURI_FK 
 FOREIGN KEY (CD_FORNECEDOR) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT COMKIT_MATERIA_FK2 
 FOREIGN KEY (CD_MATERIAL_ORIGINAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT COMKIT_ESTABEL_FK 
 FOREIGN KEY (CD_ESTAB_REGRA) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT COMKIT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO));

GRANT SELECT ON TASY.COMPONENTE_KIT TO NIVEL_1;

