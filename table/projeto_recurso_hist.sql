ALTER TABLE TASY.PROJETO_RECURSO_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJETO_RECURSO_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJETO_RECURSO_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROJETO       NUMBER(10)               NOT NULL,
  IE_GERACAO           VARCHAR2(1 BYTE)         NOT NULL,
  DS_HISTORICO         LONG                     NOT NULL,
  DT_LIBERACAO         DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PRREHIS_PK ON TASY.PROJETO_RECURSO_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRREHIS_PK
  MONITORING USAGE;


CREATE INDEX TASY.PRREHIS_PRORECU_FK_I ON TASY.PROJETO_RECURSO_HIST
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRREHIS_PRORECU_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROJETO_RECURSO_HIST ADD (
  CONSTRAINT PRREHIS_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJETO_RECURSO_HIST ADD (
  CONSTRAINT PRREHIS_PRORECU_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.PROJETO_RECURSO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PROJETO_RECURSO_HIST TO NIVEL_1;

