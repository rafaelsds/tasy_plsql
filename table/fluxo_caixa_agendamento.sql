ALTER TABLE TASY.FLUXO_CAIXA_AGENDAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FLUXO_CAIXA_AGENDAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.FLUXO_CAIXA_AGENDAMENTO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_ORIGEM_INFO       VARCHAR2(5 BYTE)         NOT NULL,
  DT_AGENDAMENTO       DATE                     NOT NULL,
  NR_DOCUMENTO         NUMBER(10)               NOT NULL,
  NR_DOCTO_COMPL       NUMBER(10),
  DT_REFERENCIA_INFO   DATE,
  DT_PROCESSAMENTO     DATE,
  IE_STATUS_AGEND      VARCHAR2(1 BYTE),
  DS_LOG               VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FXCXAGEND_I1 ON TASY.FLUXO_CAIXA_AGENDAMENTO
(NR_DOCUMENTO, NR_DOCTO_COMPL, IE_ORIGEM_INFO, IE_STATUS_AGEND)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FXCXAGEND_PK ON TASY.FLUXO_CAIXA_AGENDAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.fluxo_caixa_agendamento_after
after insert or update ON TASY.FLUXO_CAIXA_AGENDAMENTO for each row
declare

qt_job_w	number(10) := 0;
jobno		number;
ie_cria_job_w	varchar2(1);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

obter_param_usuario(830,44,wheb_usuario_pck.get_cd_perfil,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_cria_job_w);

if (ie_cria_job_w = 'S') then
	select	count(*)
	into	qt_job_w
	from	user_jobs
	where	upper(what) like '%GERAR_DOCTO_FLUXO_CAIXA%'
	and 	broken = 'N';

	if (qt_job_w = 0) then
		dbms_job.submit(jobno, 'GERAR_DOCTO_FLUXO_CAIXA;', SYSDATE, 'SYSDATE + 1/24');
	end if;
end if;
end if;
end;
/


ALTER TABLE TASY.FLUXO_CAIXA_AGENDAMENTO ADD (
  CONSTRAINT FXCXAGEND_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.FLUXO_CAIXA_AGENDAMENTO TO NIVEL_1;

