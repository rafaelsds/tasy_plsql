ALTER TABLE TASY.REGRA_RELATOR_AUTOM_CONV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.REGRA_RELATOR_AUTOM_CONV CASCADE CONSTRAINTS;

CREATE TABLE TASY.REGRA_RELATOR_AUTOM_CONV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA_IMPR    NUMBER(10)               NOT NULL,
  NR_SEQ_RELATORIO     NUMBER(10)               NOT NULL,
  NR_SEQ_SCHEDULER     NUMBER(10),
  DS_PADRAO_NOME       VARCHAR2(255 BYTE)       NOT NULL,
  IE_TIPO_RELATORIO    VARCHAR2(15 BYTE)        NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.REREACO_PK ON TASY.REGRA_RELATOR_AUTOM_CONV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REREACO_REIMARC_FK_I ON TASY.REGRA_RELATOR_AUTOM_CONV
(NR_SEQ_REGRA_IMPR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REREACO_RELATOR_FK_I_I ON TASY.REGRA_RELATOR_AUTOM_CONV
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.REREACO_USUSCHE_FK_I ON TASY.REGRA_RELATOR_AUTOM_CONV
(NR_SEQ_SCHEDULER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.REGRA_RELATOR_AUTOM_CONV ADD (
  CONSTRAINT REREACO_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.REGRA_RELATOR_AUTOM_CONV ADD (
  CONSTRAINT REREACO_REIMARC_FK 
 FOREIGN KEY (NR_SEQ_REGRA_IMPR) 
 REFERENCES TASY.REGRA_IMPRESSAO_ARQ_CONV (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REREACO_RELATOR_FK_I 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT REREACO_USUSCHE_FK 
 FOREIGN KEY (NR_SEQ_SCHEDULER) 
 REFERENCES TASY.USUARIO_SCHEDULER (NR_SEQUENCIA));

GRANT SELECT ON TASY.REGRA_RELATOR_AUTOM_CONV TO NIVEL_1;

