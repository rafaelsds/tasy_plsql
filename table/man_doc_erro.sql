ALTER TABLE TASY.MAN_DOC_ERRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_DOC_ERRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_DOC_ERRO
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_ORDEM             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC      DATE                 NOT NULL,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE)    NOT NULL,
  NR_SEQ_TIPO              NUMBER(10)           NOT NULL,
  IE_GRAVIDADE             VARCHAR2(15 BYTE),
  QT_CLIENTE               NUMBER(15),
  CD_PESSOA_FISICA         VARCHAR2(10 BYTE)    NOT NULL,
  NR_SEQ_GRUPO_DES         NUMBER(10)           NOT NULL,
  DS_ACAO_IMEDIATA         VARCHAR2(4000 BYTE)  NOT NULL,
  DS_SOLUCAO               VARCHAR2(4000 BYTE)  NOT NULL,
  IE_ORIGEM_ERRO           VARCHAR2(1 BYTE)     NOT NULL,
  NR_SEQ_CLASSIF           NUMBER(10),
  IE_IDENT_ERRO            VARCHAR2(15 BYTE),
  DT_LIBERACAO             DATE,
  IE_FORMA_ORIGEM          VARCHAR2(3 BYTE),
  DS_JUSTIFICATIVA         VARCHAR2(2000 BYTE),
  CD_PESSOA_RESP           VARCHAR2(10 BYTE),
  IE_LIBERA_CORRECAO       VARCHAR2(15 BYTE),
  IE_RECORRENTE            VARCHAR2(15 BYTE)    NOT NULL,
  DT_OCORRENCIA_ERRO       DATE,
  NR_SEQ_SUBTIPO           NUMBER(10),
  IE_FEEDBACK_ORIENTACAO   VARCHAR2(1 BYTE),
  NM_USUARIO_LIBEROU_ERRO  VARCHAR2(15 BYTE),
  NR_SEQ_ORDEM_ORIGEM      NUMBER(10),
  IE_BASE_CLIENTE          VARCHAR2(15 BYTE)    NOT NULL,
  NR_SEQ_MOTIVO_TESTES     NUMBER(10),
  IE_SITUACAO_FUNCAO       VARCHAR2(15 BYTE),
  DS_ACAO_SEVERIDADE       VARCHAR2(4000 BYTE),
  CD_EMPRESA               NUMBER(10),
  CD_APLICACAO             VARCHAR2(10 BYTE),
  CD_VERSAO_ALTERACAO      VARCHAR2(15 BYTE),
  NR_SEQ_LP                NUMBER(10),
  NR_SEQ_ORDEM_DEF         NUMBER(10),
  CD_VERSAO_IDENT          VARCHAR2(15 BYTE),
  NR_SEQ_PROJ_DEF          NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MANDOER_APLTASY_FK_I ON TASY.MAN_DOC_ERRO
(CD_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANDOER_DESMOET_FK_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_MOTIVO_TESTES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANDOER_DESMOET_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANDOER_EMPREFE_FK_I ON TASY.MAN_DOC_ERRO
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANDOER_EMPREFE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANDOER_GRUDESE_FK_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_GRUPO_DES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANDOER_GRUDESE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANDOER_I1 ON TASY.MAN_DOC_ERRO
(DT_LIBERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANDOER_LINPROG_FK_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_LP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANDOER_MANCLEO_FK_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANDOER_MANCLEO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANDOER_MANORSE_FK_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_ORDEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANDOER_MANORSE_FK2_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_ORDEM_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANDOER_MANORSE_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.MANDOER_MANORSE_FK3_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_ORDEM_DEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANDOER_MANSEOS_FK_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_SUBTIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANDOER_MANSEOS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANDOER_MANTIEO_FK_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_TIPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANDOER_MANTIEO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MANDOER_PESFISI_FK_I ON TASY.MAN_DOC_ERRO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MANDOER_PESFISI_FK2_I ON TASY.MAN_DOC_ERRO
(CD_PESSOA_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MANDOER_PK ON TASY.MAN_DOC_ERRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MANDOER_PK
  MONITORING USAGE;


CREATE INDEX TASY.MANDOER_PROPROJ_FK_I ON TASY.MAN_DOC_ERRO
(NR_SEQ_PROJ_DEF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.MAN_DOC_ERRO_AFTER
AFTER UPDATE
   ON TASY.MAN_DOC_ERRO 	FOR EACH ROW
DECLARE
	ds_log_w				varchar2(2000);
	ds_enter_w				varchar2(10) := chr(13) || chr(10);
	dt_min_date_w			date := to_date('30/12/1899 00:00:00', 'dd/mm/yyyy hh24:mi:ss');
	ds_erro_w				varchar2(2000);

BEGIN


	if (wheb_usuario_pck.get_ie_executar_trigger = 'S') and (nvl(:old.NR_SEQUENCIA, 0) > 0) and (nvl(:new.DT_LIBERACAO, dt_min_date_w) <> nvl(:old.DT_LIBERACAO, dt_min_date_w)) then
		ds_log_w	:= substr('# Dados Tasy #'  || ds_enter_w ||
				'Estabelecimento: ' || wheb_usuario_pck.get_cd_estabelecimento	||	ds_enter_w ||
				'Funcao: '			|| wheb_usuario_pck.get_cd_funcao			||	ds_enter_w ||
				'Perfil: '			|| wheb_usuario_pck.get_cd_perfil			||	ds_enter_w ||
				'Usuario: ' 		|| wheb_usuario_pck.get_nm_usuario			||	ds_enter_w ||
				'Form: '			|| wheb_usuario_pck.get_ds_form 			||	ds_enter_w ||
				'Maquina: ' 		|| wheb_usuario_pck.get_nm_maquina			||	ds_enter_w ||
				'Estacao: ' 		|| wheb_usuario_pck.get_nm_estacao			||	ds_enter_w ||
				'Data_log: '		|| to_char(sysdate,'dd/mm/yyyy hh24:mi:ss')	||	ds_enter_w,1,2000);

		insert into MAN_DOC_ERRO_LOG(NR_SEQUENCIA,
									NR_SEQ_ORDEM_SERV,
									DT_ATUALIZACAO,
									NM_USUARIO,
									DT_ATUALIZACAO_NREC,
									NM_USUARIO_NREC,
									NR_SEQ_TIPO,
									IE_GRAVIDADE,
									QT_CLIENTE,
									CD_PESSOA_FISICA,
									NR_SEQ_GRUPO_DES,
									DS_ACAO_IMEDIATA,
									DS_SOLUCAO,
									IE_ORIGEM_ERRO,
									NR_SEQ_CLASSIF,
									IE_IDENT_ERRO,
									DT_LIBERACAO,
									IE_FORMA_ORIGEM,
									DS_JUSTIFICATIVA,
									CD_PESSOA_RESP,
									IE_LIBERA_CORRECAO,
									IE_RECORRENTE,
									DT_OCORRENCIA_ERRO,
									NR_SEQ_SUBTIPO,
									IE_FEEDBACK_ORIENTACAO,
									NM_USUARIO_LIBEROU_ERRO,
									NR_SEQ_ORDEM_ORIGEM,
									IE_BASE_CLIENTE,
									NR_SEQ_MOTIVO_TESTES,
									IE_SITUACAO_FUNCAO,
									DS_ACAO_SEVERIDADE,
									CD_EMPRESA,
									CD_APLICACAO,
									CD_VERSAO_ALTERACAO,
									NR_SEQ_LP,
									NR_SEQ_ORDEM_DEF,
									CD_VERSAO_IDENT,
									NR_SEQ_PROJ_DEF,
									DS_LOG_MODIFICAO)
					values 	(MAN_DOC_ERRO_LOG_seq.nextval,
									:NEW.NR_SEQ_ORDEM,
									:NEW.DT_ATUALIZACAO,
									:NEW.NM_USUARIO,
									:NEW.DT_ATUALIZACAO_NREC,
									:NEW.NM_USUARIO_NREC,
									:NEW.NR_SEQ_TIPO,
									:NEW.IE_GRAVIDADE,
									:NEW.QT_CLIENTE,
									:NEW.CD_PESSOA_FISICA,
									:NEW.NR_SEQ_GRUPO_DES,
									:NEW.DS_ACAO_IMEDIATA,
									:NEW.DS_SOLUCAO,
									:NEW.IE_ORIGEM_ERRO,
									:NEW.NR_SEQ_CLASSIF,
									:NEW.IE_IDENT_ERRO,
									:NEW.DT_LIBERACAO,
									:NEW.IE_FORMA_ORIGEM,
									:NEW.DS_JUSTIFICATIVA,
									:NEW.CD_PESSOA_RESP,
									:NEW.IE_LIBERA_CORRECAO,
									:NEW.IE_RECORRENTE,
									:NEW.DT_OCORRENCIA_ERRO,
									:NEW.NR_SEQ_SUBTIPO,
									:NEW.IE_FEEDBACK_ORIENTACAO,
									:NEW.NM_USUARIO_LIBEROU_ERRO,
									:NEW.NR_SEQ_ORDEM_ORIGEM,
									:NEW.IE_BASE_CLIENTE,
									:NEW.NR_SEQ_MOTIVO_TESTES,
									:NEW.IE_SITUACAO_FUNCAO,
									:NEW.DS_ACAO_SEVERIDADE,
									:NEW.CD_EMPRESA,
									:NEW.CD_APLICACAO,
									:NEW.CD_VERSAO_ALTERACAO,
									:NEW.NR_SEQ_LP,
									:NEW.NR_SEQ_ORDEM_DEF,
									:NEW.CD_VERSAO_IDENT,
									:NEW.NR_SEQ_PROJ_DEF,
									ds_log_w);
	end if;

  EXCEPTION
	 when others then
	ds_erro_w   :=  SUBSTR(SQLERRM, 1, 2000);

	insert into MAN_DOC_ERRO_LOG(NR_SEQUENCIA,
								NR_SEQ_ORDEM_SERV,
								DT_ATUALIZACAO,
								NM_USUARIO,
								DT_ATUALIZACAO_NREC,
								NM_USUARIO_NREC,
								NR_SEQ_TIPO,
								IE_GRAVIDADE,
								QT_CLIENTE,
								CD_PESSOA_FISICA,
								NR_SEQ_GRUPO_DES,
								DS_ACAO_IMEDIATA,
								DS_SOLUCAO,
								IE_ORIGEM_ERRO,
								NR_SEQ_CLASSIF,
								IE_IDENT_ERRO,
								DT_LIBERACAO,
								IE_FORMA_ORIGEM,
								DS_JUSTIFICATIVA,
								CD_PESSOA_RESP,
								IE_LIBERA_CORRECAO,
								IE_RECORRENTE,
								DT_OCORRENCIA_ERRO,
								NR_SEQ_SUBTIPO,
								IE_FEEDBACK_ORIENTACAO,
								NM_USUARIO_LIBEROU_ERRO,
								NR_SEQ_ORDEM_ORIGEM,
								IE_BASE_CLIENTE,
								NR_SEQ_MOTIVO_TESTES,
								IE_SITUACAO_FUNCAO,
								DS_ACAO_SEVERIDADE,
								CD_EMPRESA,
								CD_APLICACAO,
								CD_VERSAO_ALTERACAO,
								NR_SEQ_LP,
								NR_SEQ_ORDEM_DEF,
								CD_VERSAO_IDENT,
								NR_SEQ_PROJ_DEF,
								DS_LOG_MODIFICAO)
				values 	(MAN_DOC_ERRO_LOG_seq.nextval,
								:NEW.NR_SEQ_ORDEM,
								:NEW.DT_ATUALIZACAO,
								:NEW.NM_USUARIO,
								:NEW.DT_ATUALIZACAO_NREC,
								:NEW.NM_USUARIO_NREC,
								:NEW.NR_SEQ_TIPO,
								:NEW.IE_GRAVIDADE,
								:NEW.QT_CLIENTE,
								:NEW.CD_PESSOA_FISICA,
								:NEW.NR_SEQ_GRUPO_DES,
								:NEW.DS_ACAO_IMEDIATA,
								:NEW.DS_SOLUCAO,
								:NEW.IE_ORIGEM_ERRO,
								:NEW.NR_SEQ_CLASSIF,
								:NEW.IE_IDENT_ERRO,
								:NEW.DT_LIBERACAO,
								:NEW.IE_FORMA_ORIGEM,
								:NEW.DS_JUSTIFICATIVA,
								:NEW.CD_PESSOA_RESP,
								:NEW.IE_LIBERA_CORRECAO,
								:NEW.IE_RECORRENTE,
								:NEW.DT_OCORRENCIA_ERRO,
								:NEW.NR_SEQ_SUBTIPO,
								:NEW.IE_FEEDBACK_ORIENTACAO,
								:NEW.NM_USUARIO_LIBEROU_ERRO,
								:NEW.NR_SEQ_ORDEM_ORIGEM,
								:NEW.IE_BASE_CLIENTE,
								:NEW.NR_SEQ_MOTIVO_TESTES,
								:NEW.IE_SITUACAO_FUNCAO,
								:NEW.DS_ACAO_SEVERIDADE,
								:NEW.CD_EMPRESA,
								:NEW.CD_APLICACAO,
								:NEW.CD_VERSAO_ALTERACAO,
								:NEW.NR_SEQ_LP,
								:NEW.NR_SEQ_ORDEM_DEF,
								:NEW.CD_VERSAO_IDENT,
								:NEW.NR_SEQ_PROJ_DEF,
								ds_erro_w);
END;
/


CREATE OR REPLACE TRIGGER TASY.man_doc_erro_atual
before insert or update ON TASY.MAN_DOC_ERRO for each row
declare

qt_existe_w			number(10,0);
qt_consultor_w			number(10,0);
qt_reg_w			number(1);
cd_setor_erro_w			number(10,0);
cd_setor_usuario_w		number(10,0);
ie_situacao_w			varchar2(10);
nr_seq_localizacao_w		number(10);
nr_seq_equipamento_w		number(10,0);
ie_terceiro_w			varchar2(10);
ie_empresa_valida_w		varchar2(1);
nr_seq_severidade_w		man_severidade.nr_sequencia%type;
cd_funcao_w			funcao.cd_funcao%type;
nr_seq_aviso_seg_pac_w		man_aviso_seg_pac.nr_sequencia%type;
ds_titulo_w			man_aviso_seg_pac.ds_titulo%type;
ds_aviso_w			man_aviso_seg_pac.ds_aviso%type;
qt_dest_w			number(10);
nm_usuarios_dest_w		varchar2(4000) := '';
ds_perfis_dest_w		varchar2(4000) := '';
dt_ordem_servico_w		date;
cd_versao_w			varchar2(15);
nr_seq_severidade_cliente_w	number(10);
nr_seq_ordem_serv_pai_w		man_ordem_servico.nr_sequencia%type;

cursor c_avisos_seg_pac (cd_funcao_p number) is
select	a.nr_sequencia nr_seq_aviso_seg_pac
from	man_aviso_seg_pac a,
	man_aviso_seg_pac_func b
where	a.nr_sequencia = b.nr_seq_aviso_seg_pac
and	b.cd_funcao = cd_funcao_p
and	a.ie_situacao = 'A'
and	b.ie_situacao = 'A';

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	max(mos.nr_seq_ordem_serv_pai)
into	nr_seq_ordem_serv_pai_w
from	man_ordem_servico mos
where	mos.nr_sequencia = :new.nr_seq_ordem_def;

if	(nr_seq_ordem_serv_pai_w is not null) then
	begin
		wheb_mensagem_pck.exibir_mensagem_abort(1084140);
	end;
end if;

if	(:new.dt_liberacao <> :old.dt_liberacao) then
begin
	select	nr_seq_severidade_wheb,
		nr_seq_severidade
	into	nr_seq_severidade_w,
		nr_seq_severidade_cliente_w
	from	man_ordem_servico
	where	nr_sequencia = :new.nr_seq_ordem;

	if ((nr_seq_severidade_cliente_w = 1) and (nr_seq_severidade_w is null)) then
		wheb_mensagem_pck.exibir_mensagem_abort(1023614);
	end if;

	nr_seq_severidade_w := null;
end;
end if;


if	(:new.nr_seq_grupo_des in (50,19,63,57)) then
	wheb_mensagem_pck.exibir_mensagem_abort(183870);
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) and
		(:new.nr_seq_grupo_des in (31, 32, 198, 407, 129, 387)) and
		(wheb_usuario_pck.get_nm_usuario NOT IN ('rhtakano','Fernando','amleicht','mhfelippi','Junieh','cetrentin') ) then
		wheb_mensagem_pck.exibir_mensagem_abort(1058595);
end if;


select	nr_seq_localizacao,
	nvl(b.ie_terceiro,'N'),
	dt_ordem_servico
into	nr_seq_localizacao_w,
	ie_terceiro_w,
	dt_ordem_servico_w
from	man_localizacao b,
	man_ordem_servico a
where	a.nr_seq_localizacao	= b.nr_sequencia
and	a.nr_sequencia		= :new.nr_seq_ordem;

if	(nvl(:new.ie_ident_erro,'KK') <> 'W') and
	(:new.nr_seq_ordem_origem is null) and
	(ie_terceiro_w = 'N') then
	begin
	wheb_mensagem_pck.exibir_mensagem_abort(183871);
	end;
end if;

if	(nvl(:new.ie_ident_erro,'KK') = 'W') and
	(:new.NR_SEQ_ORDEM_ORIGEM is null) and
	(nvl(:new.ie_recorrente,'N') <> 'S') and
	(nvl(:new.qt_cliente,1) = nvl(:old.qt_cliente,1)) and
	(ie_terceiro_w = 'S') then
	begin
	wheb_mensagem_pck.exibir_mensagem_abort(339019);
	end;
end if;

if	(:new.ie_origem_erro = 'W') then
	begin
	select	count(*)
	into	qt_existe_w
	from	usuario
	where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

	if	(qt_existe_w = 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(183872);
	end if;

	end;
elsif	(:new.ie_origem_erro = 'C') then
	select	count(*)
	into	qt_existe_w
	from	usuario a,
		pessoa_fisica b
	where	a.cd_pessoa_fisica = b.cd_pessoa_fisica
	and	a.cd_pessoa_fisica = :new.cd_pessoa_fisica
	and	b.ie_funcionario = 'S'
	and	a.ie_situacao = 'A';


	if	(qt_existe_w > 0) then
		begin

		select	count(*)
		into	qt_consultor_w
		from	com_canal_consultor
		where	ie_situacao		= 'A'
		and	cd_pessoa_fisica 	= :new.cd_pessoa_fisica
		and	ie_consultor_wheb	= 'S';

		if	(qt_consultor_w	> 0) then
			qt_existe_w	:= 0;
		end if;

		end;
	end if;

	/* Francisco - 11/02/2011 - Incluir o tratamento pela localizacao, pois senao nao conseguimos colocar OS's internas como erro cliente */

	if	(qt_existe_w > 0) and
		(nr_seq_localizacao_w <> 1272) then
		wheb_mensagem_pck.exibir_mensagem_abort(183874);
	end if;
end if;

if	(:new.dt_liberacao is not null) then
	:new.nm_usuario_liberou_erro := wheb_usuario_pck.get_nm_usuario;
end if;


if	(:old.dt_liberacao is null) and
	(:new.dt_liberacao is not null) and
	(:new.ie_origem_erro = 'W') then


	if	(:new.dt_ocorrencia_erro is not null) and
		(dt_ordem_servico_w < :new.dt_ocorrencia_erro) then
		wheb_mensagem_pck.exibir_mensagem_abort(407271);
	end if;


	if	(:new.dt_ocorrencia_erro is not null) and
		(:new.nr_seq_ordem <> 1105777) then
		select	Obter_Versao_Defeito(:new.dt_ocorrencia_erro)
		into	cd_versao_w
		from	dual;

		if	(cd_versao_w is not null) then
			:new.cd_versao_alteracao	:= cd_versao_w;
		end if;
	end if;


	select	max(nr_seq_severidade_wheb),
		max(cd_funcao)
	into	nr_seq_severidade_w,
		cd_funcao_w
	from	man_ordem_servico
	where	nr_sequencia = :new.nr_seq_ordem;

	if	(nr_seq_severidade_w = 1) and
		(cd_funcao_w is not null) then -- Risco a seguranca do paciente

		for r_c_avisos_seg_pac in c_avisos_seg_pac(cd_funcao_w) loop

			select	substr(obter_desc_expressao(309717, 'OS'), 1, 255) || ' ' ||
				to_char(:new.nr_seq_ordem) || ' - ' || ds_titulo,
				ds_aviso
			into	ds_titulo_w,
				ds_aviso_w
			from	man_aviso_seg_pac
			where	nr_sequencia = r_c_avisos_seg_pac.nr_seq_aviso_seg_pac;

			select	count(*)
			into	qt_dest_w
			from	man_aviso_seg_pac_dest
			where	nr_seq_aviso_seg_pac = r_c_avisos_seg_pac.nr_seq_aviso_seg_pac
			and	nm_usuario_dest is not null
			and	ie_situacao = 'A';

			if	(qt_dest_w > 0) then
				select	substr(ltrim(sys_connect_by_path(nm_usuario_dest, ', '), ', '), 1, 4000)
				into	nm_usuarios_dest_w
				from	(	select	nm_usuario_dest,
							row_number() over (order by nm_usuario_dest) nr_linha
						from	man_aviso_seg_pac_dest
						where	nr_seq_aviso_seg_pac = r_c_avisos_seg_pac.nr_seq_aviso_seg_pac
						and	nm_usuario_dest is not null
						and	ie_situacao = 'A'
				)
				where	connect_by_isleaf = 1
				start with nr_linha = 1
				connect by prior nr_linha = (nr_linha - 1);
			end if;

			select	count(*)
			into	qt_dest_w
			from	man_aviso_seg_pac_dest
			where	nr_seq_aviso_seg_pac = r_c_avisos_seg_pac.nr_seq_aviso_seg_pac
			and	cd_perfil_dest is not null
			and	ie_situacao = 'A';

			if	(qt_dest_w > 0) then
				select	substr(ltrim(sys_connect_by_path(cd_perfil_dest, ', '), ', '), 1, 4000)
				into	ds_perfis_dest_w
				from	(	select	cd_perfil_dest,
							row_number() over (order by cd_perfil_dest) nr_linha
						from	man_aviso_seg_pac_dest
						where	nr_seq_aviso_seg_pac = r_c_avisos_seg_pac.nr_seq_aviso_seg_pac
						and	cd_perfil_dest is not null
						and	ie_situacao = 'A'
				)
				where	connect_by_isleaf = 1
				start with nr_linha = 1
				connect by prior nr_linha = (nr_linha - 1);
			end if;

			if	(nm_usuarios_dest_w is not null) or
				(ds_perfis_dest_w is not null) then
				gerar_comunic_padrao(
					sysdate,
					ds_titulo_w,
					ds_aviso_w,
					:new.nm_usuario,
					'N',
					nm_usuarios_dest_w,
					'N',
					null,
					ds_perfis_dest_w,
					null,
					null,
					sysdate,
					null,
					null);
			end if;
		end loop;
	end if;
end if;

<<Final>>
qt_reg_w	:= 0;

select	decode(count(*),0,'N','S')
into	ie_empresa_valida_w
from	empresa_referencia
where	cd_empresa = :new.cd_empresa;

if (ie_empresa_valida_w = 'N') then
	:new.cd_empresa := null;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.MAN_DOC_ERRO_tp  after update ON TASY.MAN_DOC_ERRO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.CD_PESSOA_RESP,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_RESP,1,4000),substr(:new.CD_PESSOA_RESP,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_RESP',ie_log_w,ds_w,'MAN_DOC_ERRO',ds_s_w,ds_c_w);  ds_w:=substr(:new.CD_PESSOA_FISICA,1,500);gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'MAN_DOC_ERRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'MAN_DOC_ERRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IDENT_ERRO,1,4000),substr(:new.IE_IDENT_ERRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_IDENT_ERRO',ie_log_w,ds_w,'MAN_DOC_ERRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_DES,1,4000),substr(:new.NR_SEQ_GRUPO_DES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_DES',ie_log_w,ds_w,'MAN_DOC_ERRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TIPO,1,4000),substr(:new.NR_SEQ_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO',ie_log_w,ds_w,'MAN_DOC_ERRO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_ERRO,1,4000),substr(:new.IE_ORIGEM_ERRO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_ERRO',ie_log_w,ds_w,'MAN_DOC_ERRO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.MAN_DOC_ERRO ADD (
  CONSTRAINT MANDOER_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_DOC_ERRO ADD (
  CONSTRAINT MANDOER_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT MANDOER_APLTASY_FK 
 FOREIGN KEY (CD_APLICACAO) 
 REFERENCES TASY.APLICACAO_TASY (CD_APLICACAO_TASY),
  CONSTRAINT MANDOER_LINPROG_FK 
 FOREIGN KEY (NR_SEQ_LP) 
 REFERENCES TASY.LINGUAGEM_PROGRAMACAO (NR_SEQUENCIA),
  CONSTRAINT MANDOER_MANORSE_FK3 
 FOREIGN KEY (NR_SEQ_ORDEM_DEF) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT MANDOER_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJ_DEF) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA),
  CONSTRAINT MANDOER_DESMOET_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_TESTES) 
 REFERENCES TASY.DES_MOTIVO_ERRO_TESTE (NR_SEQUENCIA),
  CONSTRAINT MANDOER_MANSEOS_FK 
 FOREIGN KEY (NR_SEQ_SUBTIPO) 
 REFERENCES TASY.MAN_SUBTIPO_ERRO_OS (NR_SEQUENCIA),
  CONSTRAINT MANDOER_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MANDOER_MANORSE_FK2 
 FOREIGN KEY (NR_SEQ_ORDEM_ORIGEM) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA),
  CONSTRAINT MANDOER_MANTIEO_FK 
 FOREIGN KEY (NR_SEQ_TIPO) 
 REFERENCES TASY.MAN_TIPO_ERRO_OS (NR_SEQUENCIA),
  CONSTRAINT MANDOER_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MANDOER_GRUDESE_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_DES) 
 REFERENCES TASY.GRUPO_DESENVOLVIMENTO (NR_SEQUENCIA),
  CONSTRAINT MANDOER_MANORSE_FK 
 FOREIGN KEY (NR_SEQ_ORDEM) 
 REFERENCES TASY.MAN_ORDEM_SERVICO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MANDOER_MANCLEO_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.MAN_CLASSIF_ERRO_OS (NR_SEQUENCIA));

GRANT SELECT ON TASY.MAN_DOC_ERRO TO NIVEL_1;

