ALTER TABLE TASY.MPREV_PROG_DIAGNOSTICO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MPREV_PROG_DIAGNOSTICO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MPREV_PROG_DIAGNOSTICO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_PROGRAMA      NUMBER(10)               NOT NULL,
  NR_SEQ_DIAG_INTERNO  NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MPRPRDI_DIAINTE_FK_I ON TASY.MPREV_PROG_DIAGNOSTICO
(NR_SEQ_DIAG_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRPRDI_DIAINTE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MPRPRDI_MPRPROG_FK_I ON TASY.MPREV_PROG_DIAGNOSTICO
(NR_SEQ_PROGRAMA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRPRDI_MPRPROG_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MPRPRDI_PK ON TASY.MPREV_PROG_DIAGNOSTICO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MPRPRDI_PK
  MONITORING USAGE;


ALTER TABLE TASY.MPREV_PROG_DIAGNOSTICO ADD (
  CONSTRAINT MPRPRDI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MPREV_PROG_DIAGNOSTICO ADD (
  CONSTRAINT MPRPRDI_DIAINTE_FK 
 FOREIGN KEY (NR_SEQ_DIAG_INTERNO) 
 REFERENCES TASY.DIAGNOSTICO_INTERNO (NR_SEQUENCIA),
  CONSTRAINT MPRPRDI_MPRPROG_FK 
 FOREIGN KEY (NR_SEQ_PROGRAMA) 
 REFERENCES TASY.MPREV_PROGRAMA (NR_SEQUENCIA));

GRANT SELECT ON TASY.MPREV_PROG_DIAGNOSTICO TO NIVEL_1;

