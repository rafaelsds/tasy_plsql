ALTER TABLE TASY.PLS_REGRA_GERACAO_COD_MAT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_GERACAO_COD_MAT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_GERACAO_COD_MAT
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_INICIO_VIGENCIA   DATE                     NOT NULL,
  DT_FIM_VIGENCIA      DATE,
  NR_SEQ_ESTRUTURA     NUMBER(10),
  IE_TIPO_DESPESA      VARCHAR2(2 BYTE),
  CD_MASCARA_COD       VARCHAR2(30 BYTE)        NOT NULL,
  IE_REGRA_NUMERO      VARCHAR2(3 BYTE)         NOT NULL,
  DS_ROTINA_DIGITO     VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSRGCM_ESTABEL_FK_I ON TASY.PLS_REGRA_GERACAO_COD_MAT
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRGCM_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSRGCM_PK ON TASY.PLS_REGRA_GERACAO_COD_MAT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRGCM_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSRGCM_PLSESMAT_FK_I ON TASY.PLS_REGRA_GERACAO_COD_MAT
(NR_SEQ_ESTRUTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSRGCM_PLSESMAT_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PLS_REGRA_GERACAO_COD_MAT ADD (
  CONSTRAINT PLSRGCM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_GERACAO_COD_MAT ADD (
  CONSTRAINT PLSRGCM_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSRGCM_PLSESMAT_FK 
 FOREIGN KEY (NR_SEQ_ESTRUTURA) 
 REFERENCES TASY.PLS_ESTRUTURA_MATERIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_GERACAO_COD_MAT TO NIVEL_1;

