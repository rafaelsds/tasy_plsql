ALTER TABLE TASY.AE_SMS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AE_SMS CASCADE CONSTRAINTS;

CREATE TABLE TASY.AE_SMS
(
  EVENT_ID            VARCHAR2(36 BYTE)         NOT NULL,
  TIMESTAMP           TIMESTAMP(6),
  USER_ID             VARCHAR2(100 BYTE),
  USER_FULLNAME       VARCHAR2(256 BYTE),
  HOST_ADDRESS        VARCHAR2(256 BYTE),
  ROOT_EVENT_ID       VARCHAR2(36 BYTE),
  PARENT_EVENT_ID     VARCHAR2(36 BYTE),
  TIMESTAMP_ZONE_ID   VARCHAR2(256 BYTE),
  SMS_ID              VARCHAR2(256 BYTE),
  SENDER              VARCHAR2(4000 BYTE),
  RECIPIENT           VARCHAR2(4000 BYTE),
  MESSAGE             CLOB,
  SERVER_PROXY        VARCHAR2(1000 BYTE),
  USER_PROXY          VARCHAR2(256 BYTE),
  PASSWORD_PROXY      VARCHAR2(256 BYTE),
  ESTABLISHMENT_CODE  NUMBER(8),
  ESTABLISHMENT_NAME  VARCHAR2(256 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (MESSAGE) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PK_AESMSEV ON TASY.AE_SMS
(EVENT_ID)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AE_SMS ADD (
  CONSTRAINT PK_AESMSEV
 PRIMARY KEY
 (EVENT_ID)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AE_SMS TO NIVEL_1;

