DROP TABLE TASY.W_PLS_OCORRENCIA CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.W_PLS_OCORRENCIA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_OCORRENCIA        VARCHAR2(10 BYTE),
  NR_SEQ_MOTIVO_GLOSA  NUMBER(10),
  IE_AUDITORIA         VARCHAR2(1 BYTE),
  IE_PRE_ANALISE       VARCHAR2(1 BYTE),
  IE_PROC_MAT          VARCHAR2(1 BYTE),
  NR_SEQ_OCORRENCIA    NUMBER(10),
  IE_EXCECAO           VARCHAR2(1 BYTE),
  NR_SEQ_ANALISE       NUMBER(10)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE INDEX TASY.WPLSOCO_I ON TASY.W_PLS_OCORRENCIA
(NR_SEQ_ANALISE, NM_USUARIO);


CREATE OR REPLACE TRIGGER TASY.W_PLS_OCORRENCIA_tp  after update ON TASY.W_PLS_OCORRENCIA FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_w:=substr(:new.CD_OCORRENCIA,1,500);gravar_log_alteracao(substr(:old.CD_OCORRENCIA,1,4000),substr(:new.CD_OCORRENCIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_OCORRENCIA',ie_log_w,ds_w,'W_PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRE_ANALISE,1,4000),substr(:new.IE_PRE_ANALISE,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRE_ANALISE',ie_log_w,ds_w,'W_PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_MOTIVO_GLOSA,1,4000),substr(:new.NR_SEQ_MOTIVO_GLOSA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_MOTIVO_GLOSA',ie_log_w,ds_w,'W_PLS_OCORRENCIA',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AUDITORIA,1,4000),substr(:new.IE_AUDITORIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_AUDITORIA',ie_log_w,ds_w,'W_PLS_OCORRENCIA',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


GRANT SELECT ON TASY.W_PLS_OCORRENCIA TO NIVEL_1;

