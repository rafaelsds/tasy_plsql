ALTER TABLE TASY.PMO_KPI_PROJECT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PMO_KPI_PROJECT CASCADE CONSTRAINTS;

CREATE TABLE TASY.PMO_KPI_PROJECT
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_PROJETO         NUMBER(10)             NOT NULL,
  IE_PROJECT             VARCHAR2(1 BYTE)       NOT NULL,
  DT_REFERENCE           DATE                   NOT NULL,
  QT_PLANNED_VALUE       NUMBER(15,2),
  QT_EARNED_VALUE        NUMBER(15,2),
  QT_ACTUAL_COST         NUMBER(15,2),
  QT_EARNED_VALUE_TOTAL  NUMBER(15,2),
  QT_COST_VARIANCE       NUMBER(15,2),
  QT_ACTUAL_COST_TOTAL   NUMBER(15,2),
  QT_SCHEDULE_VARIANCE   NUMBER(15,2),
  IE_PAPEL_EXECUTOR      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PMOKPP_I1 ON TASY.PMO_KPI_PROJECT
(DT_REFERENCE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PMOKPP_PK ON TASY.PMO_KPI_PROJECT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PMOKPP_PROPROJ_FK_I ON TASY.PMO_KPI_PROJECT
(NR_SEQ_PROJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PMO_KPI_PROJECT ADD (
  CONSTRAINT PMOKPP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PMO_KPI_PROJECT ADD (
  CONSTRAINT PMOKPP_PROPROJ_FK 
 FOREIGN KEY (NR_SEQ_PROJETO) 
 REFERENCES TASY.PROJ_PROJETO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.PMO_KPI_PROJECT TO NIVEL_1;

