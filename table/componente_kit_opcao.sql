ALTER TABLE TASY.COMPONENTE_KIT_OPCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMPONENTE_KIT_OPCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMPONENTE_KIT_OPCAO
(
  NR_SEQUENCIA       NUMBER(10)                 NOT NULL,
  DT_ATUALIZACAO     DATE                       NOT NULL,
  NM_USUARIO         VARCHAR2(15 BYTE)          NOT NULL,
  CD_KIT_MATERIAL    NUMBER(5)                  NOT NULL,
  NR_SEQ_COMPONENTE  NUMBER(5)                  NOT NULL,
  CD_MATERIAL        NUMBER(6)                  NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COMKIOP_COMKIT_FK_I ON TASY.COMPONENTE_KIT_OPCAO
(CD_KIT_MATERIAL, NR_SEQ_COMPONENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COMKIOP_MATERIA_FK_I ON TASY.COMPONENTE_KIT_OPCAO
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COMKIOP_PK ON TASY.COMPONENTE_KIT_OPCAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.componente_kit_opcao_atual
before insert or update ON TASY.COMPONENTE_KIT_OPCAO for each row
declare

nr_seq_interna_w	number(10);

begin

if	(obter_se_prot_lib_regras = 'S') then
	select	nr_seq_protocolo
	into	nr_seq_interna_w
	from	kit_material
	where	cd_kit_material	= :new.cd_kit_material;

	update	protocolo_medicacao
	set	nm_usuario_aprov	=	null,
		dt_aprovacao		=	null,
		ie_status		=	'PA'
	where	nr_seq_interna		=	nr_seq_interna_w;
end if;

end;
/


ALTER TABLE TASY.COMPONENTE_KIT_OPCAO ADD (
  CONSTRAINT COMKIOP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COMPONENTE_KIT_OPCAO ADD (
  CONSTRAINT COMKIOP_COMKIT_FK 
 FOREIGN KEY (CD_KIT_MATERIAL, NR_SEQ_COMPONENTE) 
 REFERENCES TASY.COMPONENTE_KIT (CD_KIT_MATERIAL,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT COMKIOP_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.COMPONENTE_KIT_OPCAO TO NIVEL_1;

