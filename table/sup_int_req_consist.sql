ALTER TABLE TASY.SUP_INT_REQ_CONSIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_INT_REQ_CONSIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_INT_REQ_CONSIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_INCONSISTENCIA    NUMBER(5)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_MENSAGEM          VARCHAR2(255 BYTE)       NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.SUINRCO_PK ON TASY.SUP_INT_REQ_CONSIST
(NR_SEQUENCIA, NR_INCONSISTENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINRCO_PK
  MONITORING USAGE;


CREATE INDEX TASY.SUINRCO_SUINNOF_FK_I ON TASY.SUP_INT_REQ_CONSIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUINRCO_SUINNOF_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.SUP_INT_REQ_CONSIST ADD (
  CONSTRAINT SUINRCO_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_INCONSISTENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_INT_REQ_CONSIST ADD (
  CONSTRAINT SUINRCO_SUINNOF_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.SUP_INT_REQUISICAO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SUP_INT_REQ_CONSIST TO NIVEL_1;

