ALTER TABLE TASY.PLS_REGRA_COPARTICIPACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_REGRA_COPARTICIPACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_REGRA_COPARTICIPACAO
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  NR_SEQ_PLANO                   NUMBER(10),
  TX_COPARTICIPACAO              NUMBER(7,4),
  NR_SEQ_TIPO_COPARTICIPACAO     NUMBER(10)     NOT NULL,
  VL_MAXIMO                      NUMBER(15,2),
  IE_TIPO_ATENDIMENTO            VARCHAR2(1 BYTE) NOT NULL,
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  NR_SEQ_CONTRATO                NUMBER(10),
  VL_COPARTICIPACAO              NUMBER(15,2),
  DT_INICIO_VIGENCIA             DATE,
  DT_FIM_VIGENCIA                DATE,
  DT_CONTRATO_DE                 DATE,
  DT_CONTRATO_ATE                DATE,
  QT_EVENTOS_MINIMO              NUMBER(10),
  QT_MESES_INTERVALO             NUMBER(5),
  NR_SEQ_REAJUSTE                NUMBER(10),
  QT_OCORRENCIAS                 NUMBER(10),
  IE_TIPO_OCORRENCIA             VARCHAR2(2 BYTE),
  IE_TIPO_DATA_CONSISTENCIA      VARCHAR2(2 BYTE),
  NR_SEQ_LOTE_REAJUSTE           NUMBER(10),
  IE_PRESTADOR_COOPERADO         VARCHAR2(1 BYTE),
  QT_IDADE_MIN                   NUMBER(5),
  QT_IDADE_MAX                   NUMBER(5),
  QT_OCORRENCIA_GRUPO_SERV       NUMBER(5),
  QT_PERIODO_OCOR                NUMBER(5),
  IE_TIPO_PERIODO_OCOR           VARCHAR2(5 BYTE),
  NR_SEQ_GRUPO_SERV              NUMBER(10),
  NR_SEQ_REGRA_ORIGEM            NUMBER(10),
  IE_REAJUSTE                    VARCHAR2(1 BYTE),
  IE_TITULARIDADE                VARCHAR2(2 BYTE),
  CD_PROCEDIMENTO                NUMBER(15),
  IE_ORIGEM_PROCED               NUMBER(10),
  IE_TIPO_PARENTESCO             VARCHAR2(2 BYTE),
  QT_DIARIA_INICIAL              NUMBER(5),
  QT_DIARIA_FINAL                NUMBER(5),
  NR_SEQ_INTERCAMBIO             NUMBER(10),
  NR_SEQ_REGRA_LANC_AUT          NUMBER(10),
  NR_SEQ_PRESTADOR               NUMBER(10),
  NR_SEQ_TIPO_PRESTADOR          NUMBER(10),
  NR_SEQ_PROPOSTA                NUMBER(10),
  IE_INCIDENCIA_VALOR_MAXIMO     VARCHAR2(1 BYTE),
  CD_SISTEMA_ANTERIOR            VARCHAR2(255 BYTE),
  IE_FORMA_COBR_INTERNACAO       VARCHAR2(1 BYTE),
  IE_PERIODO_VALOR_MAXIMO        VARCHAR2(2 BYTE),
  IE_ANO_CALENDARIO_OUTRAS_OCOR  VARCHAR2(2 BYTE),
  OPS                            NUMBER(10),
  IE_CONSIDERA_OUTRA_OCOR_REGRA  VARCHAR2(1 BYTE),
  IE_INCIDENCIA_PROC_MAT         VARCHAR2(1 BYTE),
  IE_INCIDENCIA_VALOR_FIXO       VARCHAR2(1 BYTE),
  IE_TIPO_INCIDENCIA             VARCHAR2(1 BYTE),
  IE_INCIDE_VL_FIXO_CTA          VARCHAR2(5 BYTE),
  IE_INC_DEMAIS_ITENS            VARCHAR2(3 BYTE),
  VL_BASE_MAX                    NUMBER(15,2),
  VL_BASE_MIN                    NUMBER(6,2),
  IE_INCIDENCIA_PSIQUIATRIA      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSREPAR_PK ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPAR_PLSCONT_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPAR_PLSINCA_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_INTERCAMBIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPAR_PLSLORC_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_LOTE_REAJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PLSLORC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPAR_PLSPLAN_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSREPAR_PLSPRAD_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_PROPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PLSPRAD_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPAR_PLSPRES_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PLSPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPAR_PLSPRGS_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_GRUPO_SERV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PLSPRGS_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPAR_PLSREAJ_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_REAJUSTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PLSREAJ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPAR_PLSRELA_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_REGRA_LANC_AUT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PLSRELA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPAR_PLSREPAR_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_REGRA_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PLSREPAR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPAR_PLSTIPR_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_TIPO_PRESTADOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PLSTIPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPAR_PLTICOP_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(NR_SEQ_TIPO_COPARTICIPACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PLTICOP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSREPAR_PROCEDI_FK_I ON TASY.PLS_REGRA_COPARTICIPACAO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1032K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSREPAR_PROCEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_REGRA_COPARTICIPACAO_tp  after update ON TASY.PLS_REGRA_COPARTICIPACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null; ds_w:=substr(:new.NR_SEQ_TIPO_PRESTADOR,1,500);gravar_log_alteracao(substr(:old.NR_SEQ_TIPO_PRESTADOR,1,4000),substr(:new.NR_SEQ_TIPO_PRESTADOR,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TIPO_PRESTADOR',ie_log_w,ds_w,'PLS_REGRA_COPARTICIPACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.pls_regra_coparticipacao_atual
before update or delete ON TASY.PLS_REGRA_COPARTICIPACAO for each row
declare
begin
if	(updating) then

	if	(:new.vl_base_max != :old.vl_base_max) or
		(:new.vl_base_max is null and :old.vl_base_max is not null) or
		(:new.vl_base_max is not null and :old.vl_base_max is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'VL_BASE_MAX', :old.vl_base_max,
						:new.vl_base_max, 'A');
	end if;

	if	(:new.vl_base_min != :old.vl_base_min) or
		(:new.vl_base_min is null and :old.vl_base_min is not null) or
		(:new.vl_base_min is not null and :old.vl_base_min is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'VL_BASE_MIN', :old.vl_base_min,
						:new.vl_base_min, 'A');
	end if;

	if	(:new.nr_seq_prestador != :old.nr_seq_prestador) or
		(:new.nr_seq_prestador is null and :old.nr_seq_prestador is not null) or
		(:new.nr_seq_prestador is not null and :old.nr_seq_prestador is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'NR_SEQ_PRESTADOR', :old.nr_seq_prestador,
						:new.nr_seq_prestador, 'A');
	end if;

	if	(:new.nr_seq_tipo_prestador != :old.nr_seq_tipo_prestador) or
		(:new.nr_seq_tipo_prestador is null and :old.nr_seq_tipo_prestador is not null) or
		(:new.nr_seq_tipo_prestador is not null and :old.nr_seq_tipo_prestador is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'NR_SEQ_TIPO_PRESTADOR', :old.nr_seq_tipo_prestador,
						:new.nr_seq_tipo_prestador, 'A');
	end if;

	if	(:new.nr_seq_tipo_coparticipacao != :old.nr_seq_tipo_coparticipacao) or
		(:new.nr_seq_tipo_coparticipacao is null and :old.nr_seq_tipo_coparticipacao is not null) or
		(:new.nr_seq_tipo_coparticipacao is not null and :old.nr_seq_tipo_coparticipacao is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'NR_SEQ_TIPO_COPARTICIPACAO', :old.nr_seq_tipo_coparticipacao,
						:new.nr_seq_tipo_coparticipacao, 'A');
	end if;

	if	(:new.qt_eventos_minimo != :old.qt_eventos_minimo) or
		(:new.qt_eventos_minimo is null and :old.qt_eventos_minimo is not null) or
		(:new.qt_eventos_minimo is not null and :old.qt_eventos_minimo is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'QT_EVENTOS_MINIMO', :old.qt_eventos_minimo,
						:new.qt_eventos_minimo, 'A');
	end if;

	if	(:new.qt_meses_intervalo != :old.qt_meses_intervalo) or
		(:new.qt_meses_intervalo is null and :old.qt_meses_intervalo is not null) or
		(:new.qt_meses_intervalo is not null and :old.qt_meses_intervalo is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'QT_MESES_INTERVALO', :old.qt_meses_intervalo,
						:new.qt_meses_intervalo, 'A');
	end if;

	if	(:new.ie_tipo_data_consistencia != :old.ie_tipo_data_consistencia) or
		(:new.ie_tipo_data_consistencia is null and :old.ie_tipo_data_consistencia is not null) or
		(:new.ie_tipo_data_consistencia is not null and :old.ie_tipo_data_consistencia is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_TIPO_DATA_CONSISTENCIA', :old.ie_tipo_data_consistencia,
						:new.ie_tipo_data_consistencia, 'A');
	end if;

	if	(:new.ie_incidencia_proc_mat != :old.ie_incidencia_proc_mat) or
		(:new.ie_incidencia_proc_mat is null and :old.ie_incidencia_proc_mat is not null) or
		(:new.ie_incidencia_proc_mat is not null and :old.ie_incidencia_proc_mat is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_INCIDENCIA_PROC_MAT', :old.ie_incidencia_proc_mat,
						:new.ie_incidencia_proc_mat, 'A');
	end if;

	if	(:new.ie_tipo_atendimento != :old.ie_tipo_atendimento) or
		(:new.ie_tipo_atendimento is null and :old.ie_tipo_atendimento is not null) or
		(:new.ie_tipo_atendimento is not null and :old.ie_tipo_atendimento is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_TIPO_ATENDIMENTO', :old.ie_tipo_atendimento,
						:new.ie_tipo_atendimento, 'A');
	end if;

	if	(:new.ie_forma_cobr_internacao != :old.ie_forma_cobr_internacao) or
		(:new.ie_forma_cobr_internacao is null and :old.ie_forma_cobr_internacao is not null) or
		(:new.ie_forma_cobr_internacao is not null and :old.ie_forma_cobr_internacao is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_FORMA_COBR_INTERNACAO', :old.ie_forma_cobr_internacao,
						:new.ie_forma_cobr_internacao, 'A');
	end if;

	if	(:new.ie_prestador_cooperado != :old.ie_prestador_cooperado) or
		(:new.ie_prestador_cooperado is null and :old.ie_prestador_cooperado is not null) or
		(:new.ie_prestador_cooperado is not null and :old.ie_prestador_cooperado is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_PRESTADOR_COOPERADO', :old.ie_prestador_cooperado,
						:new.ie_prestador_cooperado, 'A');
	end if;

	if	(:new.dt_inicio_vigencia != :old.dt_inicio_vigencia) or
		(:new.dt_inicio_vigencia is null and :old.dt_inicio_vigencia is not null) or
		(:new.dt_inicio_vigencia is not null and :old.dt_inicio_vigencia is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'DT_INICIO_VIGENCIA', :old.dt_inicio_vigencia,
						:new.dt_inicio_vigencia, 'A');
	end if;

	if	(:new.dt_fim_vigencia != :old.dt_fim_vigencia) or
		(:new.dt_fim_vigencia is null and :old.dt_fim_vigencia is not null) or
		(:new.dt_fim_vigencia is not null and :old.dt_fim_vigencia is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'DT_FIM_VIGENCIA', :old.dt_fim_vigencia,
						:new.dt_fim_vigencia, 'A');
	end if;

	if	(:new.dt_contrato_de != :old.dt_contrato_de) or
		(:new.dt_contrato_de is null and :old.dt_contrato_de is not null) or
		(:new.dt_contrato_de is not null and :old.dt_contrato_de is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'DT_CONTRATO_DE', :old.dt_contrato_de,
						:new.dt_contrato_de, 'A');
	end if;

	if	(:new.dt_contrato_ate != :old.dt_contrato_ate) or
		(:new.dt_contrato_ate is null and :old.dt_contrato_ate is not null) or
		(:new.dt_contrato_ate is not null and :old.dt_contrato_ate is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'DT_CONTRATO_ATE', :old.dt_contrato_ate,
						:new.dt_contrato_ate, 'A');
	end if;

	if	(:new.qt_idade_min != :old.qt_idade_min) or
		(:new.qt_idade_min is null and :old.qt_idade_min is not null) or
		(:new.qt_idade_min is not null and :old.qt_idade_min is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'QT_IDADE_MIN', :old.qt_idade_min,
						:new.qt_idade_min, 'A');
	end if;

	if	(:new.qt_idade_max != :old.qt_idade_max) or
		(:new.qt_idade_max is null and :old.qt_idade_max is not null) or
		(:new.qt_idade_max is not null and :old.qt_idade_max is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'QT_IDADE_MAX', :old.qt_idade_max,
						:new.qt_idade_max, 'A');
	end if;

	if	(:new.ie_titularidade != :old.ie_titularidade) or
		(:new.ie_titularidade is null and :old.ie_titularidade is not null) or
		(:new.ie_titularidade is not null and :old.ie_titularidade is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_TITULARIDADE', :old.ie_titularidade,
						:new.ie_titularidade, 'A');
	end if;

	if	(:new.ie_tipo_parentesco != :old.ie_tipo_parentesco) or
		(:new.ie_tipo_parentesco is null and :old.ie_tipo_parentesco is not null) or
		(:new.ie_tipo_parentesco is not null and :old.ie_tipo_parentesco is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_TIPO_PARENTESCO', :old.ie_tipo_parentesco,
						:new.ie_tipo_parentesco, 'A');
	end if;

	if	(:new.qt_ocorrencias != :old.qt_ocorrencias) or
		(:new.qt_ocorrencias is null and :old.qt_ocorrencias is not null) or
		(:new.qt_ocorrencias is not null and :old.qt_ocorrencias is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'QT_OCORRENCIAS', :old.qt_ocorrencias,
						:new.qt_ocorrencias, 'A');
	end if;

	if	(:new.ie_tipo_ocorrencia != :old.ie_tipo_ocorrencia) or
		(:new.ie_tipo_ocorrencia is null and :old.ie_tipo_ocorrencia is not null) or
		(:new.ie_tipo_ocorrencia is not null and :old.ie_tipo_ocorrencia is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_TIPO_OCORRENCIA', :old.ie_tipo_ocorrencia,
						:new.ie_tipo_ocorrencia, 'A');
	end if;

	if	(:new.qt_diaria_inicial != :old.qt_diaria_inicial) or
		(:new.qt_diaria_inicial is null and :old.qt_diaria_inicial is not null) or
		(:new.qt_diaria_inicial is not null and :old.qt_diaria_inicial is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'QT_DIARIA_INICIAL', :old.qt_diaria_inicial,
						:new.qt_diaria_inicial, 'A');
	end if;

	if	(:new.qt_diaria_final != :old.qt_diaria_final) or
		(:new.qt_diaria_final is null and :old.qt_diaria_final is not null) or
		(:new.qt_diaria_final is not null and :old.qt_diaria_final is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'QT_DIARIA_FINAL', :old.qt_diaria_final,
						:new.qt_diaria_final, 'A');
	end if;

	if	(:new.qt_ocorrencia_grupo_serv != :old.qt_ocorrencia_grupo_serv) or
		(:new.qt_ocorrencia_grupo_serv is null and :old.qt_ocorrencia_grupo_serv is not null) or
		(:new.qt_ocorrencia_grupo_serv is not null and :old.qt_ocorrencia_grupo_serv is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'QT_OCORRENCIA_GRUPO_SERV', :old.qt_ocorrencia_grupo_serv,
						:new.qt_ocorrencia_grupo_serv, 'A');
	end if;

	if	(:new.qt_periodo_ocor != :old.qt_periodo_ocor) or
		(:new.qt_periodo_ocor is null and :old.qt_periodo_ocor is not null) or
		(:new.qt_periodo_ocor is not null and :old.qt_periodo_ocor is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'QT_PERIODO_OCOR', :old.qt_periodo_ocor,
						:new.qt_periodo_ocor, 'A');
	end if;

	if	(:new.ie_tipo_periodo_ocor != :old.ie_tipo_periodo_ocor) or
		(:new.ie_tipo_periodo_ocor is null and :old.ie_tipo_periodo_ocor is not null) or
		(:new.ie_tipo_periodo_ocor is not null and :old.ie_tipo_periodo_ocor is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_TIPO_PERIODO_OCOR', :old.ie_tipo_periodo_ocor,
						:new.ie_tipo_periodo_ocor, 'A');
	end if;

	if	(:new.ie_ano_calendario_outras_ocor != :old.ie_ano_calendario_outras_ocor) or
		(:new.ie_ano_calendario_outras_ocor is null and :old.ie_ano_calendario_outras_ocor is not null) or
		(:new.ie_ano_calendario_outras_ocor is not null and :old.ie_ano_calendario_outras_ocor is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_ANO_CALENDARIO_OUTRAS_OCOR', :old.ie_ano_calendario_outras_ocor,
						:new.ie_ano_calendario_outras_ocor, 'A');
	end if;

	if	(:new.nr_seq_grupo_serv != :old.nr_seq_grupo_serv) or
		(:new.nr_seq_grupo_serv is null and :old.nr_seq_grupo_serv is not null) or
		(:new.nr_seq_grupo_serv is not null and :old.nr_seq_grupo_serv is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'NR_SEQ_GRUPO_SERV', :old.nr_seq_grupo_serv,
						:new.nr_seq_grupo_serv, 'A');
	end if;

	if	(:new.cd_procedimento != :old.cd_procedimento) or
		(:new.cd_procedimento is null and :old.cd_procedimento is not null) or
		(:new.cd_procedimento is not null and :old.cd_procedimento is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'CD_PROCEDIMENTO', :old.cd_procedimento,
						:new.cd_procedimento, 'A');
	end if;

	if	(:new.ie_considera_outra_ocor_regra != :old.ie_considera_outra_ocor_regra) or
		(:new.ie_considera_outra_ocor_regra is null and :old.ie_considera_outra_ocor_regra is not null) or
		(:new.ie_considera_outra_ocor_regra is not null and :old.ie_considera_outra_ocor_regra is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_CONSIDERA_OUTRA_OCOR_REGRA', :old.ie_considera_outra_ocor_regra,
						:new.ie_considera_outra_ocor_regra, 'A');
	end if;

	if	(:new.ie_tipo_incidencia != :old.ie_tipo_incidencia) or
		(:new.ie_tipo_incidencia is null and :old.ie_tipo_incidencia is not null) or
		(:new.ie_tipo_incidencia is not null and :old.ie_tipo_incidencia is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_TIPO_INCIDENCIA', :old.ie_tipo_incidencia,
						:new.ie_tipo_incidencia, 'A');
	end if;

	if	(:new.tx_coparticipacao != :old.tx_coparticipacao) or
		(:new.tx_coparticipacao is null and :old.tx_coparticipacao is not null) or
		(:new.tx_coparticipacao is not null and :old.tx_coparticipacao is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'TX_COPARTICIPACAO', :old.tx_coparticipacao,
						:new.tx_coparticipacao, 'A');
	end if;

	if	(:new.vl_maximo != :old.vl_maximo) or
		(:new.vl_maximo is null and :old.vl_maximo is not null) or
		(:new.vl_maximo is not null and :old.vl_maximo is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'VL_MAXIMO', :old.vl_maximo,
						:new.vl_maximo, 'A');
	end if;

	if	(:new.vl_coparticipacao != :old.vl_coparticipacao) or
		(:new.vl_coparticipacao is null and :old.vl_coparticipacao is not null) or
		(:new.vl_coparticipacao is not null and :old.vl_coparticipacao is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'VL_COPARTICIPACAO', :old.vl_coparticipacao,
						:new.vl_coparticipacao, 'A');
	end if;

	if	(:new.ie_incidencia_valor_fixo != :old.ie_incidencia_valor_fixo) or
		(:new.ie_incidencia_valor_fixo is null and :old.ie_incidencia_valor_fixo is not null) or
		(:new.ie_incidencia_valor_fixo is not null and :old.ie_incidencia_valor_fixo is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_INCIDENCIA_VALOR_FIXO', :old.ie_incidencia_valor_fixo,
						:new.ie_incidencia_valor_fixo, 'A');
	end if;

	if	(:new.ie_incidencia_valor_maximo != :old.ie_incidencia_valor_maximo) or
		(:new.ie_incidencia_valor_maximo is null and :old.ie_incidencia_valor_maximo is not null) or
		(:new.ie_incidencia_valor_maximo is not null and :old.ie_incidencia_valor_maximo is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_INCIDENCIA_VALOR_MAXIMO', :old.ie_incidencia_valor_maximo,
						:new.ie_incidencia_valor_maximo, 'A');
	end if;

	if	(:new.ie_periodo_valor_maximo != :old.ie_periodo_valor_maximo) or
		(:new.ie_periodo_valor_maximo is null and :old.ie_periodo_valor_maximo is not null) or
		(:new.ie_periodo_valor_maximo is not null and :old.ie_periodo_valor_maximo is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_PERIODO_VALOR_MAXIMO', :old.ie_periodo_valor_maximo,
						:new.ie_periodo_valor_maximo, 'A');
	end if;

	if	(:new.cd_sistema_anterior != :old.cd_sistema_anterior) or
		(:new.cd_sistema_anterior is null and :old.cd_sistema_anterior is not null) or
		(:new.cd_sistema_anterior is not null and :old.cd_sistema_anterior is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'CD_SISTEMA_ANTERIOR', :old.cd_sistema_anterior,
						:new.cd_sistema_anterior, 'A');
	end if;

	if	(:new.ie_reajuste != :old.ie_reajuste) or
		(:new.ie_reajuste is null and :old.ie_reajuste is not null) or
		(:new.ie_reajuste is not null and :old.ie_reajuste is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_REAJUSTE', :old.ie_reajuste,
						:new.ie_reajuste, 'A');
	end if;

	if	(:new.nr_seq_plano != :old.nr_seq_plano) or
		(:new.nr_seq_plano is null and :old.nr_seq_plano is not null) or
		(:new.nr_seq_plano is not null and :old.nr_seq_plano is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'NR_SEQ_PLANO', :old.nr_seq_plano,
						:new.nr_seq_plano, 'A');
	end if;

	if	(:new.ie_situacao != :old.ie_situacao) or
		(:new.ie_situacao is null and :old.ie_situacao is not null) or
		(:new.ie_situacao is not null and :old.ie_situacao is null) then

		pls_grava_log_regra_copartic(	:new.nr_sequencia, 'IE_SITUACAO', :old.ie_situacao,
						:new.ie_situacao, 'A');
	end if;
elsif	(deleting) then
	pls_grava_log_regra_copartic(	:old.nr_sequencia, null, null,
					null, 'D');
end if;


end;
/


ALTER TABLE TASY.PLS_REGRA_COPARTICIPACAO ADD (
  CONSTRAINT PLSREPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_REGRA_COPARTICIPACAO ADD (
  CONSTRAINT PLSREPAR_PLSLORC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_REAJUSTE) 
 REFERENCES TASY.PLS_LOTE_REAJ_COPARTIC (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLSPRGS_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_SERV) 
 REFERENCES TASY.PLS_PRECO_GRUPO_SERVICO (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLSREPAR_FK 
 FOREIGN KEY (NR_SEQ_REGRA_ORIGEM) 
 REFERENCES TASY.PLS_REGRA_COPARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PLSREPAR_PLSINCA_FK 
 FOREIGN KEY (NR_SEQ_INTERCAMBIO) 
 REFERENCES TASY.PLS_INTERCAMBIO (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLSRELA_FK 
 FOREIGN KEY (NR_SEQ_REGRA_LANC_AUT) 
 REFERENCES TASY.PLS_REGRA_LANC_AUTOMATICO (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLSPRES_FK 
 FOREIGN KEY (NR_SEQ_PRESTADOR) 
 REFERENCES TASY.PLS_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLSTIPR_FK 
 FOREIGN KEY (NR_SEQ_TIPO_PRESTADOR) 
 REFERENCES TASY.PLS_TIPO_PRESTADOR (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLSPRAD_FK 
 FOREIGN KEY (NR_SEQ_PROPOSTA) 
 REFERENCES TASY.PLS_PROPOSTA_ADESAO (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLSPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.PLS_PLANO (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLTICOP_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COPARTICIPACAO) 
 REFERENCES TASY.PLS_TIPO_COPARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PLSREPAR_PLSREAJ_FK 
 FOREIGN KEY (NR_SEQ_REAJUSTE) 
 REFERENCES TASY.PLS_REAJUSTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_REGRA_COPARTICIPACAO TO NIVEL_1;

