ALTER TABLE TASY.CONVENIO_RETORNO_IMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONVENIO_RETORNO_IMP CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONVENIO_RETORNO_IMP
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_RETORNO       NUMBER(10)               NOT NULL,
  CD_SENHA             VARCHAR2(255 BYTE),
  VL_PAGO              NUMBER(15,2)             NOT NULL,
  NR_INTERNO_CONTA     NUMBER(10),
  VL_GUIA              NUMBER(15,2)             NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CONREIM_CONRETO_FK_I ON TASY.CONVENIO_RETORNO_IMP
(NR_SEQ_RETORNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREIM_CONRETO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.CONREIM_PK ON TASY.CONVENIO_RETORNO_IMP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.CONREIM_PK
  MONITORING USAGE;


ALTER TABLE TASY.CONVENIO_RETORNO_IMP ADD (
  CONSTRAINT CONREIM_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONVENIO_RETORNO_IMP ADD (
  CONSTRAINT CONREIM_CONRETO_FK 
 FOREIGN KEY (NR_SEQ_RETORNO) 
 REFERENCES TASY.CONVENIO_RETORNO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.CONVENIO_RETORNO_IMP TO NIVEL_1;

