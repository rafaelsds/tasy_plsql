ALTER TABLE TASY.MAN_TIPO_EQUIPAMENTO_VALOR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_TIPO_EQUIPAMENTO_VALOR CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_TIPO_EQUIPAMENTO_VALOR
(
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_TIPO_EQUIP_ATRIB  NUMBER(10)           NOT NULL,
  NR_SEQ_EQUIPAMENTO       NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  DT_VALOR                 DATE,
  CD_VALOR                 VARCHAR2(40 BYTE),
  DT_ATUALIZACAO_NREC      DATE,
  NM_USUARIO_NREC          VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATEQVA_MANEQUI_FK_I ON TASY.MAN_TIPO_EQUIPAMENTO_VALOR
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATEQVA_MANEQUI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATEQVA_MATIEAT_FK_I ON TASY.MAN_TIPO_EQUIPAMENTO_VALOR
(NR_SEQ_TIPO_EQUIP_ATRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATEQVA_MATIEAT_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATEQVA_PK ON TASY.MAN_TIPO_EQUIPAMENTO_VALOR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATEQVA_PK
  MONITORING USAGE;


ALTER TABLE TASY.MAN_TIPO_EQUIPAMENTO_VALOR ADD (
  CONSTRAINT MATEQVA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MAN_TIPO_EQUIPAMENTO_VALOR ADD (
  CONSTRAINT MATEQVA_MANEQUI_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.MAN_EQUIPAMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MATEQVA_MATIEAT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_EQUIP_ATRIB) 
 REFERENCES TASY.MAN_TIPO_EQUIPAMENTO_ATRIB (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MAN_TIPO_EQUIPAMENTO_VALOR TO NIVEL_1;

