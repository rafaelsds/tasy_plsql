ALTER TABLE TASY.PLS_BONIFICACAO_REGRA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_BONIFICACAO_REGRA CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_BONIFICACAO_REGRA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_BONIFICACAO          NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4)         NOT NULL,
  IE_TIPO_REGRA               VARCHAR2(2 BYTE)  NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  IE_TIPO_ITEM                VARCHAR2(255 BYTE),
  NR_PARCELA_INICIAL          NUMBER(10),
  NR_PARCELA_FINAL            NUMBER(10),
  NR_SEQ_DESCONTO             NUMBER(10),
  QT_IDADE_INICIAL            NUMBER(3),
  QT_IDADE_FINAL              NUMBER(10),
  TX_BONIFICACAO              NUMBER(7,4),
  NR_SEQ_PARENTESCO           NUMBER(10),
  VL_BONIFICACAO              NUMBER(15,2),
  IE_TITULARIDADE             VARCHAR2(2 BYTE),
  IE_INDICACAO                VARCHAR2(1 BYTE),
  NR_SEQ_TIPO_COBERTURA       NUMBER(10),
  QT_COBERTURA                NUMBER(10),
  QT_MESES_INTERVALO          NUMBER(5),
  IE_PERIODO                  VARCHAR2(2 BYTE),
  QT_PARCELAS_PAGAS_INICIAL   NUMBER(10),
  QT_PARCELAS_PAGAS_FINAL     NUMBER(10),
  IE_ANIVERSARIO_CONTRATO     VARCHAR2(1 BYTE),
  IE_ACAO_CONTRATO            VARCHAR2(2 BYTE),
  IE_COOPERADO                VARCHAR2(1 BYTE),
  QT_TEMPO_COOPERADO_INICIAL  NUMBER(5),
  QT_TEMPO_COOPERADO_FINAL    NUMBER(5),
  TX_BONIFICACAO_CONJUGE      NUMBER(7,4),
  NR_PARCELA_INICIAL_SCA      NUMBER(5),
  NR_PARCELA_FINAL_SCA        NUMBER(5),
  IE_TIPO_SEGURADO            VARCHAR2(1 BYTE),
  IE_DEMITIDO_EXONERADO       VARCHAR2(10 BYTE),
  IE_POSSUI_BONIFICACAO       VARCHAR2(1 BYTE),
  IE_TIPO_VIDAS_CONTRATO      VARCHAR2(2 BYTE),
  NR_SEQ_FORMA_COBRANCA       VARCHAR2(2 BYTE),
  QT_TITULARES_INICIAL        NUMBER(10),
  QT_TITULARES_FINAL          NUMBER(10),
  IE_MES_REF_DESCONTO         VARCHAR2(3 BYTE),
  IE_COOPERADO_ATIVO          VARCHAR2(1 BYTE),
  IE_COOPERADO_PRESTADOR      VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSBORE_BONIF_I1 ON TASY.PLS_BONIFICACAO_REGRA
(NR_SEQ_BONIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSBORE_ESTABEL_FK_I ON TASY.PLS_BONIFICACAO_REGRA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORE_ESTABEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBORE_GRAUPA_FK_I ON TASY.PLS_BONIFICACAO_REGRA
(NR_SEQ_PARENTESCO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORE_GRAUPA_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSBORE_PK ON TASY.PLS_BONIFICACAO_REGRA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSBORE_PLSREDE_FK_I ON TASY.PLS_BONIFICACAO_REGRA
(NR_SEQ_DESCONTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSBORE_PLSREDE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSBORE_PLSTICB_FK_I ON TASY.PLS_BONIFICACAO_REGRA
(NR_SEQ_TIPO_COBERTURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_BONIFICACAO_REGRA ADD (
  CONSTRAINT PLSBORE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_BONIFICACAO_REGRA ADD (
  CONSTRAINT PLSBORE_GRAUPA_FK 
 FOREIGN KEY (NR_SEQ_PARENTESCO) 
 REFERENCES TASY.GRAU_PARENTESCO (NR_SEQUENCIA),
  CONSTRAINT PLSBORE_PLSREDE_FK 
 FOREIGN KEY (NR_SEQ_DESCONTO) 
 REFERENCES TASY.PLS_REGRA_DESCONTO (NR_SEQUENCIA),
  CONSTRAINT PLSBORE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSBORE_PLSTICB_FK 
 FOREIGN KEY (NR_SEQ_TIPO_COBERTURA) 
 REFERENCES TASY.PLS_TIPO_COBERT_BONIFIC (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_BONIFICACAO_REGRA TO NIVEL_1;

