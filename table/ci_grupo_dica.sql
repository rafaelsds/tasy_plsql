ALTER TABLE TASY.CI_GRUPO_DICA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CI_GRUPO_DICA CASCADE CONSTRAINTS;

CREATE TABLE TASY.CI_GRUPO_DICA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_GRUPO             VARCHAR2(4000 BYTE),
  NR_SEQ_TIPO_DICA     NUMBER(10),
  NR_SEQ_GRUPO         NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.CDGRUDIC_CIGRUPUSU_FK_I ON TASY.CI_GRUPO_DICA
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.CDGRUDIC_PK ON TASY.CI_GRUPO_DICA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CI_GRUPO_DICA ADD (
  CONSTRAINT CDGRUDIC_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CI_GRUPO_DICA ADD (
  CONSTRAINT CDGRUDIC_CIGRUPUSU_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.CI_GRUPO_USUARIO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CI_GRUPO_DICA TO NIVEL_1;

