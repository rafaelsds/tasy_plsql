ALTER TABLE TASY.PLS_ATUARIAL_ARQ_REG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_ATUARIAL_ARQ_REG CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_ATUARIAL_ARQ_REG
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_REGRA         NUMBER(10)               NOT NULL,
  IE_TIPO_REGISTRO     VARCHAR2(5 BYTE)         NOT NULL,
  IE_SEPARADOR         VARCHAR2(5 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSAARG_PK ON TASY.PLS_ATUARIAL_ARQ_REG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSAARG_PLSAARR_FK_I ON TASY.PLS_ATUARIAL_ARQ_REG
(NR_SEQ_REGRA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.pls_atuarial_arq_reg_atual
before insert or update ON TASY.PLS_ATUARIAL_ARQ_REG for each row
begin

-- Ao atualizar ou inserir um novo registro, a regra inteira ser� marcada como inconsistente
update	pls_atuarial_arq_regra
set	ie_consistente		= 'N',
	dt_atualizacao		= sysdate,
	nm_usuario		= :new.nm_usuario
where	nr_sequencia		= :new.nr_seq_regra;


end pls_atuarial_arq_reg_atual;
/


ALTER TABLE TASY.PLS_ATUARIAL_ARQ_REG ADD (
  CONSTRAINT PLSAARG_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_ATUARIAL_ARQ_REG ADD (
  CONSTRAINT PLSAARG_PLSAARR_FK 
 FOREIGN KEY (NR_SEQ_REGRA) 
 REFERENCES TASY.PLS_ATUARIAL_ARQ_REGRA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_ATUARIAL_ARQ_REG TO NIVEL_1;

