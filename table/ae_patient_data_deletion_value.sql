ALTER TABLE TASY.AE_PATIENT_DATA_DELETION_VALUE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.AE_PATIENT_DATA_DELETION_VALUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.AE_PATIENT_DATA_DELETION_VALUE
(
  EVENT_ID            VARCHAR2(36 BYTE)         NOT NULL,
  TABLE_NAME          VARCHAR2(30 BYTE)         NOT NULL,
  COLUMN_NAME         VARCHAR2(30 BYTE)         NOT NULL,
  COLUMN_DESCRIPTION  VARCHAR2(512 BYTE),
  VALUE_NUMBER        NUMBER(38,16),
  VALUE_VARCHAR       VARCHAR2(4000 BYTE),
  VALUE_TIMESTAMP     TIMESTAMP(6),
  VALUE_DATE          DATE,
  VALUE_NAME          VARCHAR2(512 BYTE),
  VALUE_CLOB          CLOB,
  VALUE_BLOB          BLOB
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
LOB (VALUE_BLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
  LOB (VALUE_CLOB) STORE AS 
      ( TABLESPACE  TASY_DATA 
        ENABLE      STORAGE IN ROW
        CHUNK       8192
        RETENTION
        NOCACHE
        INDEX       (
          TABLESPACE TASY_DATA
          STORAGE    (
                      INITIAL          64K
                      NEXT             1
                      MINEXTENTS       1
                      MAXEXTENTS       UNLIMITED
                      PCTINCREASE      0
                      BUFFER_POOL      DEFAULT
                     ))
        STORAGE    (
                    INITIAL          64K
                    NEXT             1M
                    MINEXTENTS       1
                    MAXEXTENTS       UNLIMITED
                    PCTINCREASE      0
                    BUFFER_POOL      DEFAULT
                   )
      )
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.AEPDDVL_PK ON TASY.AE_PATIENT_DATA_DELETION_VALUE
(EVENT_ID, TABLE_NAME, COLUMN_NAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AE_PATIENT_DATA_DELETION_VALUE ADD (
  CONSTRAINT AEPDDVL_PK
 PRIMARY KEY
 (EVENT_ID, TABLE_NAME, COLUMN_NAME)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.AE_PATIENT_DATA_DELETION_VALUE TO NIVEL_1;

