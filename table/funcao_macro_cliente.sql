ALTER TABLE TASY.FUNCAO_MACRO_CLIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.FUNCAO_MACRO_CLIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.FUNCAO_MACRO_CLIENTE
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DS_MACRO               VARCHAR2(255 BYTE)     NOT NULL,
  NR_SEQ_MACRO           NUMBER(10)             NOT NULL,
  DS_OBSERVACAO          VARCHAR2(4000 BYTE),
  DS_LOCALE              VARCHAR2(10 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_MACRO_IMPORT        NUMBER(10),
  DT_LIBERACAO           DATE,
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.FUNCMACCLI_FUNCMAC_FK_I ON TASY.FUNCAO_MACRO_CLIENTE
(NR_SEQ_MACRO)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.FUNCMACCLI_FUNCMACINI_FK_I ON TASY.FUNCAO_MACRO_CLIENTE
(NR_MACRO_IMPORT)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.FUNCMACCLI_PK ON TASY.FUNCAO_MACRO_CLIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.atual_funcao_macro_cliente
before insert or update  ON TASY.FUNCAO_MACRO_CLIENTE for each row
declare

pragma autonomous_transaction;

qt_macro_existe_w	number(10);

begin

	select	count(*)
	into	qt_macro_existe_w
	from	funcao_macro_cliente
	where	ds_macro = :new.ds_macro
	and		nr_sequencia <> :new.nr_sequencia
	and		nr_seq_macro = :new.nr_seq_macro
	and		upper(ds_locale) = upper(:new.ds_locale)
	and		dt_inativacao is null;

	if (qt_macro_existe_w > 0) then
		wheb_mensagem_pck.exibir_mensagem_abort(724703);
	end if;

end;
/


ALTER TABLE TASY.FUNCAO_MACRO_CLIENTE ADD (
  CONSTRAINT FUNCMACCLI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_DATA
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.FUNCAO_MACRO_CLIENTE ADD (
  CONSTRAINT FUNCMACCLI_FUNCMAC_FK 
 FOREIGN KEY (NR_SEQ_MACRO) 
 REFERENCES TASY.FUNCAO_MACRO (NR_SEQUENCIA),
  CONSTRAINT FUNCMACCLI_FUNCMACINI_FK 
 FOREIGN KEY (NR_MACRO_IMPORT) 
 REFERENCES TASY.FUNCAO_MACRO_INICIAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.FUNCAO_MACRO_CLIENTE TO NIVEL_1;

