ALTER TABLE TASY.RELATORIO_FUNCAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.RELATORIO_FUNCAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.RELATORIO_FUNCAO
(
  CD_FUNCAO            NUMBER(5)                NOT NULL,
  NR_SEQ_RELATORIO     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  IE_EXIGE_PERFIL      VARCHAR2(1 BYTE)         NOT NULL,
  QT_COPIA             NUMBER(6),
  IE_LOGO              VARCHAR2(1 BYTE),
  IE_IMPRIMIR          VARCHAR2(1 BYTE),
  IE_CONFIGURAR        VARCHAR2(1 BYTE),
  IE_LIBERADO_CLIENTE  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.RELFUNC_FUNCAO_FK_I_I ON TASY.RELATORIO_FUNCAO
(CD_FUNCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.RELFUNC_PK ON TASY.RELATORIO_FUNCAO
(CD_FUNCAO, NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.RELFUNC_RELATOR_FK_I_I ON TASY.RELATORIO_FUNCAO
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.RELATORIO_FUNCAO ADD (
  CONSTRAINT RELFUNC_PK
 PRIMARY KEY
 (CD_FUNCAO, NR_SEQ_RELATORIO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.RELATORIO_FUNCAO ADD (
  CONSTRAINT RELFUNC_RELATOR_FK_I 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.RELATORIO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT RELFUNC_FUNCAO_FK_I 
 FOREIGN KEY (CD_FUNCAO) 
 REFERENCES TASY.FUNCAO (CD_FUNCAO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.RELATORIO_FUNCAO TO NIVEL_1;

