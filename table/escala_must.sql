ALTER TABLE TASY.ESCALA_MUST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_MUST CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_MUST
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  DT_LIBERACAO               DATE,
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  DT_AVALIACAO               DATE               NOT NULL,
  QT_PESO_HABITUAL           NUMBER(6,3)        NOT NULL,
  QT_PESO_ATUAL              NUMBER(6,3)        NOT NULL,
  QT_PERC_PERDA_PESO         NUMBER(10,2)       NOT NULL,
  QT_ALTURA_CM               NUMBER(5,2)        NOT NULL,
  QT_IMC                     NUMBER(4,1)        NOT NULL,
  QT_PONTUACAO               NUMBER(1)          NOT NULL,
  IE_REDUCAO_ALIMENTAR       VARCHAR2(1 BYTE),
  CD_PERFIL_ATIVO            NUMBER(5),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_HORA                    NUMBER(2),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCMUST_ATEPACI_FK_I ON TASY.ESCALA_MUST
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMUST_I1 ON TASY.ESCALA_MUST
(DT_AVALIACAO, NR_ATENDIMENTO, CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCMUST_PERFIL_FK_I ON TASY.ESCALA_MUST
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMUST_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCMUST_PESFISI_FK_I ON TASY.ESCALA_MUST
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCMUST_PK ON TASY.ESCALA_MUST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMUST_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCMUST_TASASDI_FK_I ON TASY.ESCALA_MUST
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMUST_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCMUST_TASASDI_FK2_I ON TASY.ESCALA_MUST
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCMUST_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.Escala_must_atual
before insert or update ON TASY.ESCALA_MUST for each row
declare

qt_pontuacao_imc_w				number(1,0);
qt_pontuacao_perc_perda_peso_w	number(1,0);
qt_reg_w	number(1);
qt_pontuacao_w	number(10);


nr_seq_evento_w		number(10);
cd_estabelecimento_w	number(4);
cd_pessoa_fisica_w	varchar2(10);
qt_idade_w			number(10);



Cursor C01 is
	select	nr_seq_evento
	from	regra_envio_sms
	where	((cd_estabelecimento_w = 0) or (cd_estabelecimento	= cd_estabelecimento_w))
	and	ie_evento_disp		= 'LMUS'
	and	qt_idade_w between nvl(qt_idade_min,0)	and nvl(qt_idade_max,9999)
	and	nvl(ie_situacao,'A') = 'A';



begin

cd_estabelecimento_w	:= obter_estabelecimento_ativo;
if	(:new.nr_hora is null) or
	(:new.DT_AVALIACAO <> :old.DT_AVALIACAO) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.DT_AVALIACAO,'hh24'),'hh24'));
	end;
end if;


if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


begin
:new.qt_imc := dividir(:new.qt_peso_atual, power(dividir(:new.qt_altura_cm,100),2));
:new.qt_perc_perda_peso := (dividir((:new.qt_peso_habitual - :new.qt_peso_atual), :new.qt_peso_habitual) * 100);
exception
when others then
	:new.qt_imc	:= 0;
	:new.qt_perc_perda_peso	:= 0;
end;

if	(:new.qt_imc > 20) then
	qt_pontuacao_imc_w := 0;
elsif (:new.qt_imc < 18.5) then
	qt_pontuacao_imc_w := 2;
else
	qt_pontuacao_imc_w := 1;
end if;
qt_pontuacao_w	:= 0;

if	(:new.IE_REDUCAO_ALIMENTAR	= 'S') then
	qt_pontuacao_w	:= 2;
end if;
if	(:new.qt_perc_perda_peso < 5) then
	qt_pontuacao_perc_perda_peso_w := 0;
elsif (:new.qt_perc_perda_peso > 10) then
	qt_pontuacao_perc_perda_peso_w := 2;
else
	qt_pontuacao_perc_perda_peso_w := 1;
end if;

:new.qt_pontuacao := qt_pontuacao_imc_w + qt_pontuacao_perc_perda_peso_w + qt_pontuacao_w;

if	(:new.dt_liberacao is not null) and
	(:old.dt_liberacao is null) and
	(:new.QT_PONTUACAO	>= 1)then
	cd_pessoa_fisica_w	:= obter_pessoa_atendimento(:new.nr_atendimento,'C');
	qt_idade_w	:= nvl(obter_idade_pf(cd_pessoa_fisica_w,sysdate,'A'),0);

	begin
	open C01;
	loop
	fetch C01 into
		nr_seq_evento_w;
	exit when C01%notfound;
		begin
		gerar_evento_paciente_trigger(nr_seq_evento_w,:new.nr_atendimento,cd_pessoa_fisica_w,null,:new.nm_usuario,null,:new.dt_liberacao,:new.qt_pontuacao);
		end;
	end loop;
	close C01;

	exception
		when others then
		null;
	end;


end if;

<<Final>>
qt_reg_w	:= 0;

end Escala_must_atual;
/


CREATE OR REPLACE TRIGGER TASY.escala_must_pend_atual
after insert or update ON TASY.ESCALA_MUST for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;


if	(:new.dt_liberacao is null) then
	ie_tipo_w := 'ESC';
elsif	(:old.dt_liberacao is null) and
		(:new.dt_liberacao is not null) then
	ie_tipo_w := 'XESC';
end if;

if	(ie_tipo_w	is not null) then
	Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, substr(obter_pessoa_atendimento(:new.nr_atendimento,'C'),1,255), :new.nr_atendimento, :new.nm_usuario,null, null, null, null, null, null,'28');
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.ESCALA_MUST_delete
after delete ON TASY.ESCALA_MUST for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and	   	ie_escala = '28'
and		nr_seq_escala  = :old.nr_sequencia;

commit;

<<Final>>
qt_reg_w	:= 0;
end;
/


ALTER TABLE TASY.ESCALA_MUST ADD (
  CONSTRAINT ESCMUST_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_MUST ADD (
  CONSTRAINT ESCMUST_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCMUST_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCMUST_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCMUST_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCMUST_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL));

GRANT SELECT ON TASY.ESCALA_MUST TO NIVEL_1;

