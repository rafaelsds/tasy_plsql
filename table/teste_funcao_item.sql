ALTER TABLE TASY.TESTE_FUNCAO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TESTE_FUNCAO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.TESTE_FUNCAO_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ITEM              VARCHAR2(80 BYTE)        NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_TESTE         NUMBER(10)               NOT NULL,
  NR_SEQ_APRES         NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TEFUITE_PK ON TASY.TESTE_FUNCAO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TEFUITE_PK
  MONITORING USAGE;


CREATE INDEX TASY.TEFUITE_TESFUNC_FK_I ON TASY.TESTE_FUNCAO_ITEM
(NR_SEQ_TESTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.TEFUITE_TESFUNC_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.teste_funcao_item_atual
after update or insert ON TASY.TESTE_FUNCAO_ITEM for each row
begin

update	teste_funcao
set	dt_atualizacao	= sysdate,
	nm_usuario	= :new.nm_usuario
where	nr_sequencia	= :new.nr_seq_teste;

end;
/


CREATE OR REPLACE TRIGGER TASY.TESTE_FUNCAO_ITEM_tp  after update ON TASY.TESTE_FUNCAO_ITEM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'TESTE_FUNCAO_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRES,1,4000),substr(:new.NR_SEQ_APRES,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRES',ie_log_w,ds_w,'TESTE_FUNCAO_ITEM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ITEM,1,4000),substr(:new.DS_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'DS_ITEM',ie_log_w,ds_w,'TESTE_FUNCAO_ITEM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TESTE_FUNCAO_ITEM ADD (
  CONSTRAINT TEFUITE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TESTE_FUNCAO_ITEM ADD (
  CONSTRAINT TEFUITE_TESFUNC_FK 
 FOREIGN KEY (NR_SEQ_TESTE) 
 REFERENCES TASY.TESTE_FUNCAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.TESTE_FUNCAO_ITEM TO NIVEL_1;

