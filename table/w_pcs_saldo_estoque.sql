DROP TABLE TASY.W_PCS_SALDO_ESTOQUE CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_PCS_SALDO_ESTOQUE
(
  CD_LOCAL_ESTOQUE   NUMBER(4)                  NOT NULL,
  DS_LOCAL_ESTOQUE   VARCHAR2(255 BYTE)         NOT NULL,
  NR_SEQ_RELATORIO   NUMBER(10)                 NOT NULL,
  NR_SEQ_ITEM_RELAT  NUMBER(10)                 NOT NULL,
  QT_SALDO           NUMBER(13,4)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WPCSSES_PCSSCRE_FK_I ON TASY.W_PCS_SALDO_ESTOQUE
(NR_SEQ_RELATORIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WPCSSES_PCSSCRI_FK_I ON TASY.W_PCS_SALDO_ESTOQUE
(NR_SEQ_ITEM_RELAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_PCS_SALDO_ESTOQUE ADD (
  CONSTRAINT WPCSSES_PCSSCRE_FK 
 FOREIGN KEY (NR_SEQ_RELATORIO) 
 REFERENCES TASY.PCS_SEGMENTO_COMPRAS_REL (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT WPCSSES_PCSSCRI_FK 
 FOREIGN KEY (NR_SEQ_ITEM_RELAT) 
 REFERENCES TASY.PCS_SEG_COMPRAS_REL_ITEM (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.W_PCS_SALDO_ESTOQUE TO NIVEL_1;

