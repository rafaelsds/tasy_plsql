ALTER TABLE TASY.EMPREST_FINANC_PARC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EMPREST_FINANC_PARC CASCADE CONSTRAINTS;

CREATE TABLE TASY.EMPREST_FINANC_PARC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  NR_SEQ_CONTRATO      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_TITULO            NUMBER(10),
  DT_VENCIMENTO        DATE                     NOT NULL,
  VL_PARCELA           NUMBER(15,2)             NOT NULL,
  VL_JUROS             NUMBER(15,2)             NOT NULL,
  VL_AMORTIZACAO       NUMBER(15,2)             NOT NULL,
  VL_SALDO_DEV         NUMBER(15,2)             NOT NULL,
  VL_SALDO_CORRIGIDO   NUMBER(15,2)             NOT NULL,
  NR_PARCELA           NUMBER(15)               DEFAULT null,
  DT_CANCELAMENTO      DATE,
  VL_INDICE_REAJ       NUMBER(15,8),
  VL_CORRECAO          NUMBER(15,2),
  NR_SEQ_INDICE        NUMBER(10),
  VL_JUROS_INDEX       NUMBER(15,8),
  VL_SALDO_DEV_INDEX   NUMBER(15,8),
  NR_MES               NUMBER(3),
  QT_DUP               NUMBER(3),
  QT_DUT               NUMBER(3),
  DT_INICIAL           DATE,
  DT_FINAL             DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EMPFINPAR_CONTRAT_FK_I ON TASY.EMPREST_FINANC_PARC
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPFINPAR_EMPFIX_FK_I ON TASY.EMPREST_FINANC_PARC
(NR_SEQ_INDICE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPFINPAR_ESTABEL_FK_I ON TASY.EMPREST_FINANC_PARC
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EMPFINPAR_PK ON TASY.EMPREST_FINANC_PARC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EMPFINPAR_TITPAGA_FK_I ON TASY.EMPREST_FINANC_PARC
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.emprest_financ_bndes_before
before insert or update ON TASY.EMPREST_FINANC_PARC for each row
declare

ds_formula_ajustada_w     varchar(4000);
nr_seq_contrato_w         contrato.nr_sequencia%type;
ds_formula_w              contrato_bndes.ds_formula%type;
qt_periodicidade_juros_w  contrato_bndes.qt_periodicidade_juros%type;
qt_periodicidade_amort_w  contrato_bndes.qt_periodicidade_amort%type;
vl_principal_w            contrato_bndes.vl_principal%type;
vl_ajustado_w             contrato_bndes.vl_ajustado%type;
qt_parcelas_w             contrato_bndes.qt_parcelas%type;
vl_calculado_w            contrato_bndes.vl_calculado%type;
qt_carencia_w             contrato_bndes.qt_carencia%type;
vl_juros_w                emprest_financ_parc.vl_juros%type;
vl_juros_index_w          emprest_financ_parc.vl_juros_index%type;

owner_w varchar2(30);
name_w varchar2(30);
line_w pls_integer;
type_w varchar2(30);

pragma autonomous_transaction;

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

  owa_util.who_called_me(owner_w,name_w,line_w,type_w);

  if name_w = 'GERAR_PARCELAS_BNDES_FIN' then
    select nr_sequencia
    into nr_seq_contrato_w
    from contrato_bndes
    where nr_seq_contrato = :NEW.nr_seq_contrato;

    if nr_seq_contrato_w is not null then
      select ds_formula,
             qt_periodicidade_juros,
             qt_periodicidade_amort,
             vl_principal,
             vl_ajustado,
             qt_parcelas,
             vl_calculado,
             qt_carencia
      into ds_formula_w,
           qt_periodicidade_juros_w,
           qt_periodicidade_amort_w,
           vl_principal_w,
           vl_ajustado_w,
           qt_parcelas_w,
           vl_calculado_w,
           qt_carencia_w
      from contrato_bndes
      where nr_seq_contrato = :NEW.nr_seq_contrato;

      if ((:NEW.nr_mes < qt_carencia_w and mod(:NEW.nr_mes, qt_periodicidade_juros_w) = 0) or (:NEW.nr_mes > qt_carencia_w and mod(:NEW.nr_mes, qt_periodicidade_amort_w) = 0)) then
        if ds_formula_w is not null then
          ds_formula_ajustada_w := ds_formula_w;
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, '#:I:#', 'obter_resultado_formula_indice(''');
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, '!', ''', to_date(''' || :NEW.dt_vencimento || '''))');
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, '#:P:#', 'nvl(:NEW.');
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, '#:', 'nvl(');
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':#', '.');
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, '@',',0)');

          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.NR_MES', nvl(to_char(:NEW.NR_MES,'99999999999999B', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.NR_PARCELA', nvl(to_char(:NEW.NR_PARCELA,'99999999999999B', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.NR_TITULO', nvl(to_char(:NEW.NR_TITULO,'99999999999999B', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.QT_DUP', nvl(to_char(:NEW.QT_DUP,'99999999999999B', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.QT_DUT', nvl(to_char(:NEW.QT_DUT,'99999999999999B', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.VL_AMORTIZACAO', nvl(to_char(:NEW.VL_AMORTIZACAO,'999999999999999BD99', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.VL_CORRECAO', nvl(to_char(:NEW.VL_CORRECAO,'999999999999999BD99', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.VL_INDICE_REAJ', nvl(to_char(:NEW.VL_INDICE_REAJ,'999999999999999BD99999999', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.VL_JUROS_INDEX', nvl(to_char(:NEW.VL_JUROS_INDEX,'999999999999999BD99999999', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.VL_JUROS', nvl(to_char(:NEW.VL_JUROS,'999999999999999BD99', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.VL_PARCELA', nvl(to_char(:NEW.VL_PARCELA,'999999999999999BD99', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.VL_SALDO_CORRIGIDO', nvl(to_char(:NEW.VL_SALDO_CORRIGIDO,'999999999999999BD99', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.VL_SALDO_DEV_INDEX', nvl(to_char((:NEW.VL_SALDO_DEV_INDEX),'999999999999999BD99999999', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));
          ds_formula_ajustada_w := replace(ds_formula_ajustada_w, ':NEW.VL_SALDO_DEV', nvl(to_char(:NEW.VL_SALDO_DEV,'999999999999999BD99', 'NLS_NUMERIC_CHARACTERS = ''.,'''),0));

          begin
            execute immediate
              'select ' || ds_formula_ajustada_w || ' ' ||
              'from contrato c,
                  contrato_bndes b,
                  indice_reajuste_anual_fin a,
                  indice_reajuste_valor_fin v
              where c.nr_sequencia = ' || :NEW.nr_seq_contrato || '
              and b.nr_seq_contrato = c.nr_sequencia
              and a.nr_seq_contrato_bndes(+) = b.nr_sequencia
              and v.nr_seq_indice(+) = a.nr_sequencia'
            into vl_juros_index_w;
          exception
            when VALUE_ERROR then
              Wheb_mensagem_pck.exibir_mensagem_abort(1067077, 'DS_ERRO_P=' || sqlerrm);
            when ZERO_DIVIDE then
              wheb_mensagem_pck.exibir_mensagem_abort(1067078, 'DS_ERRO_P=' || sqlerrm);
            when others then
              wheb_mensagem_pck.exibir_mensagem_abort(1067079, 'DS_ERRO_P=' || sqlerrm);
          end;

          vl_juros_w := (nvl(vl_juros_index_w,0) * nvl(:NEW.vl_indice_reaj, 1));

        end if;
      end if;

      :NEW.vl_juros := nvl(vl_juros_w,0);
      :NEW.vl_juros_index := nvl(vl_juros_index_w,0);
      :NEW.vl_parcela := nvl(:NEW.vl_amortizacao,0) + nvl(vl_juros_w,0) + nvl(:NEW.vl_correcao,0);

    end if;
  end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.emprest_financ_parc_after
after insert or update ON TASY.EMPREST_FINANC_PARC for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then
if (inserting) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_contrato,:new.nr_sequencia,'GEF',:new.dt_vencimento,'I',:new.nm_usuario);
elsif (updating) then
	/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
	gravar_agend_fluxo_caixa(:new.nr_seq_contrato,:new.nr_sequencia,'GEF',:new.dt_vencimento,'A',:new.nm_usuario);
end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.emprest_financ_parc_atual
after update ON TASY.EMPREST_FINANC_PARC for each row
declare

ds_historico_w		varchar2(255);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

if (:new.dt_vencimento <> :old.dt_vencimento) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(370701,'VALOR_ANTIGO='||to_char(:old.dt_vencimento,'dd/mm/yyyy')||';VALOR_NOVO='||to_char(:new.dt_vencimento,'dd/mm/yyyy')),0,254);

	insert into emprest_financ_parc_hist(
		nr_sequencia,
		ds_historico,
		nr_seq_parc,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.vl_parcela <> :old.vl_parcela) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(370703,'VALOR_ANTIGO='||to_char(:old.vl_parcela,'999,999,990.00')||';VALOR_NOVO='||to_char(:new.vl_parcela,'999,999,990.00')),0,254);

	insert into emprest_financ_parc_hist(
		nr_sequencia,
		ds_historico,
		nr_seq_parc,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.vl_juros <> :old.vl_juros) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(370705,'VALOR_ANTIGO='||to_char(:old.vl_juros,'999,999,990.00')||';VALOR_NOVO='||to_char(:new.vl_juros,'999,999,990.00')),0,254);

	insert into emprest_financ_parc_hist(
		nr_sequencia,
		ds_historico,
		nr_seq_parc,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.vl_amortizacao <> :old.vl_amortizacao) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(370706,'VALOR_ANTIGO='||to_char(:old.vl_amortizacao,'999,999,990.00')||';VALOR_NOVO='||to_char(:new.vl_amortizacao,'999,999,990.00')),0,254);

	insert into emprest_financ_parc_hist(
		nr_sequencia,
		ds_historico,
		nr_seq_parc,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.vl_saldo_dev <> :old.vl_saldo_dev) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(370707,'VALOR_ANTIGO='||to_char(:old.vl_saldo_dev,'999,999,990.00')||';VALOR_NOVO='||to_char(:new.vl_saldo_dev,'999,999,990.00')),0,254);

	insert into emprest_financ_parc_hist(
		nr_sequencia,
		ds_historico,
		nr_seq_parc,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;

if (:new.vl_saldo_corrigido <> :old.vl_saldo_corrigido) then
	ds_historico_w := substr(wheb_mensagem_pck.get_texto(370708,'VALOR_ANTIGO='||to_char(:old.vl_saldo_corrigido,'999,999,990.00')||';VALOR_NOVO='||to_char(:new.vl_saldo_corrigido,'999,999,990.00')),0,254);

	insert into emprest_financ_parc_hist(
		nr_sequencia,
		ds_historico,
		nr_seq_parc,
		nm_usuario,
		dt_atualizacao)
	values	(tipo_aplicacao_hist_seq.nextval,
		ds_historico_w,
		:new.nr_sequencia,
		:new.nm_usuario,
		sysdate);

end if;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.emprest_financ_parc_delete
before delete ON TASY.EMPREST_FINANC_PARC for each row
begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'S') then

/* Grava o agendamento da informacao para atualizacao do fluxo de caixa. */
gravar_agend_fluxo_caixa(:old.nr_seq_contrato,:old.nr_sequencia,'GEF',:old.dt_vencimento,'E',:old.nm_usuario);

end if;
end;
/


ALTER TABLE TASY.EMPREST_FINANC_PARC ADD (
  CONSTRAINT EMPFINPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EMPREST_FINANC_PARC ADD (
  CONSTRAINT EMPFINPAR_EMPFIX_FK 
 FOREIGN KEY (NR_SEQ_INDICE) 
 REFERENCES TASY.EMPREST_FINANC_INDICE (NR_SEQUENCIA),
  CONSTRAINT EMPFINPAR_CONTRAT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.CONTRATO (NR_SEQUENCIA),
  CONSTRAINT EMPFINPAR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT EMPFINPAR_TITPAGA_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_PAGAR (NR_TITULO));

GRANT SELECT ON TASY.EMPREST_FINANC_PARC TO NIVEL_1;

