ALTER TABLE TASY.TIPO_LOTE_CONTABIL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TIPO_LOTE_CONTABIL CASCADE CONSTRAINTS;

CREATE TABLE TASY.TIPO_LOTE_CONTABIL
(
  CD_TIPO_LOTE_CONTABIL     NUMBER(10)          NOT NULL,
  DS_TIPO_LOTE_CONTABIL     VARCHAR2(40 BYTE)   NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  NM_OBJETO                 VARCHAR2(50 BYTE),
  NR_SEQ_TRANS_FIN          NUMBER(10),
  IE_AGRUPA_PJ              VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  CD_INTERFACE              NUMBER(10),
  NM_ARQUIVO_EXP_MOVIMENTO  VARCHAR2(255 BYTE),
  IE_AGRUPA_PF              VARCHAR2(1 BYTE)    NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  IE_CONSISTE_REVISAO       VARCHAR2(1 BYTE),
  CD_EXP_DESC               NUMBER(10),
  IE_CONCILIAR              VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPLOCO_DICEXPR_FK_I ON TASY.TIPO_LOTE_CONTABIL
(CD_EXP_DESC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPLOCO_INTERFA_FK_I ON TASY.TIPO_LOTE_CONTABIL
(CD_INTERFACE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPLOCO_OBJSIST_FK_I ON TASY.TIPO_LOTE_CONTABIL
(NM_OBJETO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIPLOCO_PK ON TASY.TIPO_LOTE_CONTABIL
(CD_TIPO_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPLOCO_TRAFINA_FK_I ON TASY.TIPO_LOTE_CONTABIL
(NR_SEQ_TRANS_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TIPO_LOTE_CONTABIL_tp  after update ON TASY.TIPO_LOTE_CONTABIL FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.CD_TIPO_LOTE_CONTABIL);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_CONSISTE_REVISAO,1,4000),substr(:new.IE_CONSISTE_REVISAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSISTE_REVISAO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONCILIAR,1,4000),substr(:new.IE_CONCILIAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONCILIAR',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_LOTE_CONTABIL,1,4000),substr(:new.CD_TIPO_LOTE_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOTE_CONTABIL',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_OBJETO,1,4000),substr(:new.NM_OBJETO,1,4000),:new.nm_usuario,nr_seq_w,'NM_OBJETO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_TIPO_LOTE_CONTABIL,1,4000),substr(:new.DS_TIPO_LOTE_CONTABIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_TIPO_LOTE_CONTABIL',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_TRANS_FIN,1,4000),substr(:new.NR_SEQ_TRANS_FIN,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_TRANS_FIN',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AGRUPA_PJ,1,4000),substr(:new.IE_AGRUPA_PJ,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGRUPA_PJ',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_ARQUIVO_EXP_MOVIMENTO,1,4000),substr(:new.NM_ARQUIVO_EXP_MOVIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'NM_ARQUIVO_EXP_MOVIMENTO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTERFACE,1,4000),substr(:new.CD_INTERFACE,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTERFACE',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AGRUPA_PF,1,4000),substr(:new.IE_AGRUPA_PF,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGRUPA_PF',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EXP_DESC,1,4000),substr(:new.CD_EXP_DESC,1,4000),:new.nm_usuario,nr_seq_w,'CD_EXP_DESC',ie_log_w,ds_w,'TIPO_LOTE_CONTABIL',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.TIPO_LOTE_CONTABIL ADD (
  CONSTRAINT TIPLOCO_PK
 PRIMARY KEY
 (CD_TIPO_LOTE_CONTABIL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TIPO_LOTE_CONTABIL ADD (
  CONSTRAINT TIPLOCO_INTERFA_FK 
 FOREIGN KEY (CD_INTERFACE) 
 REFERENCES TASY.INTERFACE (CD_INTERFACE),
  CONSTRAINT TIPLOCO_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FIN) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TIPLOCO_DICEXPR_FK 
 FOREIGN KEY (CD_EXP_DESC) 
 REFERENCES TASY.DIC_EXPRESSAO (CD_EXPRESSAO));

GRANT SELECT ON TASY.TIPO_LOTE_CONTABIL TO NIVEL_1;

