ALTER TABLE TASY.PLS_CP_CTA_PROC_TMP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_CP_CTA_PROC_TMP CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE TASY.PLS_CP_CTA_PROC_TMP
(
  NR_SEQ_CONTA_PROC           NUMBER(10)        NOT NULL,
  CD_CONVENIO                 NUMBER(5),
  CD_ESPECIALIDADE            NUMBER(15),
  IE_TIPO_GUIA                VARCHAR2(2 BYTE),
  CD_CATEGORIA                VARCHAR2(10 BYTE),
  CD_PROCEDIMENTO             NUMBER(15),
  IE_ORIGEM_PROCED            NUMBER(10),
  NR_SEQ_CP_COMB_FILTRO       NUMBER(10),
  NR_SEQ_CP_COMB_PROC_ENTAO   NUMBER(10),
  CD_MEDICO_EXECUTOR          VARCHAR2(20 BYTE),
  NR_SEQ_CONGENERE_PROT       NUMBER(10),
  NR_SEQ_CONGENERE_SEG        NUMBER(10),
  NR_SEQ_PRESTADOR_EXEC       NUMBER(10),
  DT_PROCEDIMENTO_REFERENCIA  DATE,
  CD_GRUPO_PROC               NUMBER(15),
  DT_MES_COMPETENCIA          DATE,
  CD_AREA_PROCED              NUMBER(15),
  CD_PRESTADOR_EXEC           VARCHAR2(30 BYTE),
  IE_AUTOGERADO_PROC          VARCHAR2(1 BYTE)
)
ON COMMIT PRESERVE ROWS
NOCACHE;


CREATE UNIQUE INDEX TASY.PCPCPTP_PK ON TASY.PLS_CP_CTA_PROC_TMP
(NR_SEQ_CONTA_PROC);


ALTER TABLE TASY.PLS_CP_CTA_PROC_TMP ADD (
  CONSTRAINT PCPCPTP_PK
 PRIMARY KEY
 (NR_SEQ_CONTA_PROC));

GRANT SELECT ON TASY.PLS_CP_CTA_PROC_TMP TO NIVEL_1;

