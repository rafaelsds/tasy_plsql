ALTER TABLE TASY.PROJ_CONSULTOR_NIVEL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROJ_CONSULTOR_NIVEL CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROJ_CONSULTOR_NIVEL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_EMPRESA           NUMBER(4)                NOT NULL,
  CD_CONSULTOR         VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  DT_AVALIACAO         DATE                     NOT NULL,
  NR_SEQ_NIVEL         NUMBER(10)               NOT NULL,
  DT_APROVACAO         DATE,
  VL_FIXO              NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROJCONI_EMPRESA_FK_I ON TASY.PROJ_CONSULTOR_NIVEL
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJCONI_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROJCONI_PESFISI_FK_I ON TASY.PROJ_CONSULTOR_NIVEL
(CD_CONSULTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PROJCONI_PK ON TASY.PROJ_CONSULTOR_NIVEL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJCONI_PK
  MONITORING USAGE;


CREATE INDEX TASY.PROJCONI_PROJNICO_FK_I ON TASY.PROJ_CONSULTOR_NIVEL
(NR_SEQ_NIVEL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROJCONI_PROJNICO_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PROJ_CONSULTOR_NIVEL ADD (
  CONSTRAINT PROJCONI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROJ_CONSULTOR_NIVEL ADD (
  CONSTRAINT PROJCONI_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PROJCONI_PESFISI_FK 
 FOREIGN KEY (CD_CONSULTOR) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PROJCONI_PROJNICO_FK 
 FOREIGN KEY (NR_SEQ_NIVEL) 
 REFERENCES TASY.PROJ_NIVEL_CONSULTOR (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROJ_CONSULTOR_NIVEL TO NIVEL_1;

