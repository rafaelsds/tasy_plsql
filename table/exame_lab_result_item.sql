ALTER TABLE TASY.EXAME_LAB_RESULT_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.EXAME_LAB_RESULT_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.EXAME_LAB_RESULT_ITEM
(
  NR_SEQ_RESULTADO         NUMBER(10)           NOT NULL,
  NR_SEQUENCIA             NUMBER(10)           NOT NULL,
  NR_SEQ_EXAME             NUMBER(10)           NOT NULL,
  DT_ATUALIZACAO           DATE                 NOT NULL,
  NM_USUARIO               VARCHAR2(15 BYTE)    NOT NULL,
  QT_RESULTADO             NUMBER(15,4),
  DS_RESULTADO             VARCHAR2(4000 BYTE),
  NR_SEQ_METODO            NUMBER(10),
  NR_SEQ_MATERIAL          NUMBER(10),
  PR_RESULTADO             NUMBER(9,4),
  IE_STATUS                VARCHAR2(1 BYTE),
  DT_APROVACAO             DATE,
  NM_USUARIO_APROVACAO     VARCHAR2(15 BYTE),
  NR_SEQ_PRESCR            NUMBER(6),
  PR_MINIMO                NUMBER(15,4),
  PR_MAXIMO                NUMBER(15,4),
  QT_MINIMA                NUMBER(15,4),
  QT_MAXIMA                NUMBER(15,4),
  DS_OBSERVACAO            VARCHAR2(4000 BYTE),
  DS_REFERENCIA            VARCHAR2(4000 BYTE),
  DS_UNIDADE_MEDIDA        VARCHAR2(40 BYTE),
  QT_DECIMAIS              NUMBER(1),
  DS_REGRA                 VARCHAR2(255 BYTE),
  DT_COLETA                DATE,
  NR_SEQ_REAGENTE          NUMBER(10),
  NR_SEQ_FORMATO           NUMBER(10),
  CD_MEDICO_RESP           VARCHAR2(10 BYTE),
  DT_IMPRESSAO             DATE,
  NR_SEQ_UNID_MED          NUMBER(10),
  CD_EQUIPAMENTO           NUMBER(5),
  IE_CONSISTE              VARCHAR2(1 BYTE),
  DS_PROTOCOLO_EXTERNO     VARCHAR2(30 BYTE),
  IE_NORMALIDADE           NUMBER(10),
  IE_RESTRINGE_RESULTADO   VARCHAR2(1 BYTE),
  DT_DIGITACAO             DATE,
  NR_SEQ_FORMATO_RED       NUMBER(10),
  NR_SEQ_METODO_EXAME      NUMBER(10),
  NR_LOTE_REAGENTE         VARCHAR2(30 BYTE),
  NR_SEQ_ASSINATURA        NUMBER(10),
  NR_SEQ_ASSINAT_INATIV    NUMBER(10),
  DT_VALIDADE_REAGENTE     DATE,
  DS_RESULTADO_CURTO       VARCHAR2(255 BYTE),
  NM_USUARIO_LIBERACAO     VARCHAR2(15 BYTE),
  DT_LIBERACAO             DATE,
  NR_SEQ_MOTIVO_BLOQUEIO   NUMBER(10),
  DS_OBS_BLOQ_RESULT       VARCHAR2(255 BYTE),
  NR_SEQ_LOTE_PATOL        NUMBER(10),
  DS_OBS_LOTE_ENT          VARCHAR2(4000 BYTE),
  IE_ACAO_CRITERIO_LOTE    VARCHAR2(1 BYTE),
  DS_MENSAGEM_CRITERIO     VARCHAR2(4000 BYTE),
  NR_SEQ_MATERIAL_INTEGR   NUMBER(10),
  DS_OBS_CURTA             VARCHAR2(255 BYTE),
  DT_EXAME_EXT             DATE,
  DS_REFERENCIA_EXT        VARCHAR2(4000 BYTE),
  DS_METODO_CRITERIO       VARCHAR2(255 BYTE),
  DS_MATERIAL_CRITERIO     VARCHAR2(255 BYTE),
  NM_USUARIO_PRIM_DIG      VARCHAR2(15 BYTE),
  IE_VALOR_CRIT            VARCHAR2(255 BYTE),
  IE_TIPO_VAL_CRIT         VARCHAR2(255 BYTE),
  DS_RESULT_SUG_CRIT       VARCHAR2(255 BYTE),
  DS_ACAO_CRITERIO         VARCHAR2(255 BYTE),
  DS_METODO_EXT            VARCHAR2(4000 BYTE),
  NM_USUARIO_VISUALIZACAO  VARCHAR2(15 BYTE),
  DT_VISUALIZACAO          DATE,
  IE_RESULTADO_CRITICO     VARCHAR2(3 BYTE)     DEFAULT null,
  DS_HASH_ASSINATURA       VARCHAR2(255 BYTE),
  IE_RESULTADO_RELEVANTE   VARCHAR2(1 BYTE),
  NM_USUARIO_SEG_DIG       VARCHAR2(15 BYTE),
  IE_RESULTADO_REFERENCIA  VARCHAR2(3 BYTE)     DEFAULT null,
  DS_REGRA_RESULT_CRITICO  VARCHAR2(255 BYTE),
  DS_FLAG                  VARCHAR2(10 BYTE),
  DT_RESULT_ITEM           DATE,
  IE_FINAL                 VARCHAR2(1 BYTE),
  IE_RESULT_STATUS         VARCHAR2(1 BYTE),
  CD_COMENTARIO_EXT        VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          399M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.EXALARI_EQUILAB_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(CD_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_EQUILAB_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_EXALABO_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          103M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALARI_EXALAFO_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_FORMATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          19M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALARI_EXALAFO_FK2_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_FORMATO_RED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          4M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_EXALAFO_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_EXALARE_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_RESULTADO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          133M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALARI_I1 ON TASY.EXAME_LAB_RESULT_ITEM
(DT_APROVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_I1
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_I2 ON TASY.EXAME_LAB_RESULT_ITEM
(DT_APROVACAO, DT_IMPRESSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_I2
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_I3 ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_RESULTADO, NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALARI_LABMBLORES_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_MOTIVO_BLOQUEIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          488K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_LABMBLORES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_LABPATO_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_LOTE_PATOL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_LABPATO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_LABREAG_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_REAGENTE)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_LABREAG_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_LABUNME_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_UNID_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_LABUNME_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_MATEXLA_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          17M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_MATEXLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_MATEXLAINT_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_MATERIAL_INTEGR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_MATEXLAINT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_METEXLA_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_METODO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          18M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_METEXLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_METEXLA_FK2_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_METODO_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_METEXLA_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.EXALARI_PESFISI_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(CD_MEDICO_RESP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          19M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.EXALARI_PK ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_RESULTADO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          152M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.EXALARI_TASASDI_FK_I ON TASY.EXAME_LAB_RESULT_ITEM
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.EXALARI_TASASDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.exame_lab_result_item_covid
before update ON TASY.EXAME_LAB_RESULT_ITEM for each row
declare

nr_atendimento_w  atendimento_paciente.nr_atendimento%type;
nr_seq_atend_adic_w	ATEND_PACIENTE_ADIC.nr_sequencia%type;

begin

--Raise_application_error(-20011,'#@#@'||PFCS_LAB_OBTER_SE_EXAME_COVID(:new.nr_seq_exame));
if (PFCS_LAB_OBTER_SE_EXAME_COVID(:new.nr_seq_exame) = 'S') then

	select MAX(nr_atendimento)
	into   nr_atendimento_w
	from   exame_lab_resultado
	where  nr_seq_resultado = :new.nr_seq_resultado;

	if (nr_atendimento_w is not null) then

		select MAX(nr_sequencia)
		into   nr_seq_atend_adic_w
		from   ATEND_PACIENTE_ADIC
		where  nr_atendimento = nr_atendimento_w;

		if ((trim(substr(upper(:new.ds_resultado),1,7))) like (substr(upper(OBTER_DESC_EXPRESSAO(296109)),1,7))) then

		--	Raise_application_error(-20011,'#@#@'||upper(:new.ds_resultado)||' - '||upper(OBTER_DESC_EXPRESSAO(296109))||' - '||nr_seq_atend_adic_w);
			if (nr_seq_atend_adic_w is null) then
				insert into ATEND_PACIENTE_ADIC (	nr_sequencia,
														dt_atualizacao,
														nm_usuario,
														nr_atendimento,
														ie_atend_covid_posit)
				values (atend_paciente_adic_seq.nextval,sysdate,:new.nm_usuario,nr_atendimento_w,'S');
			else
				update ATEND_PACIENTE_ADIC set ie_atend_covid_posit = 'S' where nr_sequencia = nr_seq_atend_adic_w;
			end if;
		else
		--	Raise_application_error(-20011,'#@#@'||upper(:new.ds_resultado)||' - '||upper(OBTER_DESC_EXPRESSAO(296109))||' - '||nr_seq_atend_adic_w);
			if (nr_seq_atend_adic_w is null) then



				insert into ATEND_PACIENTE_ADIC (	nr_sequencia,
														dt_atualizacao,
														nm_usuario,
														nr_atendimento,
														ie_atend_covid_posit)
				values (atend_paciente_adic_seq.nextval,sysdate,:new.nm_usuario,nr_atendimento_w,'N');
			else
				update ATEND_PACIENTE_ADIC set ie_atend_covid_posit = 'N' where nr_sequencia = nr_seq_atend_adic_w;
			end if;

		end if;

	end if;

end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.exame_lab_result_item_hl7
after insert or update ON TASY.EXAME_LAB_RESULT_ITEM for each row
declare
ds_sep_bv_w		varchar2(100);
cd_pessoa_fisica_w	varchar2(10);
ds_param_integ_hl7_w	varchar2(4000);
nr_prescricao_w		number(14);

ie_valido_dixtal_w	varchar2(1);
ie_valido_dixtal_ep12_w	varchar2(1);

sql_errm_w		varchar2(255);
qt_reg_w		number(10);
nr_atendimento_w	number(10);
pragma autonomous_transaction;

begin
ds_sep_bv_w := obter_separador_bv;
/*
if	(:old.dt_aprovacao is null) and
	(:new.dt_aprovacao is not null) then
	begin
	begin
	select	pm.cd_pessoa_fisica,
		nvl(pm.nr_atendimento,0),
		nvl(obter_atepacu_paciente(nvl(pm.nr_atendimento,0),'IA'),0)
	into	cd_pessoa_fisica_w,
		nr_atendimento_w,
		nr_seq_interno_w
	from	prescr_medica pm,
		prescr_procedimento pp,
		exame_lab_resultado elr,
		exame_lab_result_item elri
	where	pm.nr_prescricao = pp.nr_prescricao
	and	pp.nr_prescricao = elr.nr_prescricao
	and	pp.nr_sequencia = elri.nr_seq_prescr
	and	elri.nr_seq_resultado = elr.nr_seq_resultado
	and	elr.nr_seq_resultado = :new.nr_seq_resultado
	and	elri.nr_sequencia = :new.nr_sequencia;

	ds_param_integ_hl7_w :=	'cd_pessoa_fisica=' || cd_pessoa_fisica_w || ds_sep_bv_w ||
				'nr_atendimento=' || nr_atendimento_w || ds_sep_bv_w ||
				'nr_seq_interno=' || nr_seq_interno_w || ds_sep_bv_w ||
				'nr_seq_resultado=' || :new.nr_seq_resultado || ds_sep_bv_w ||
				'nr_seq_result_item=' || :new.nr_sequencia || ds_sep_bv_w;
	gravar_agend_integracao(24, ds_param_integ_hl7_w);
	exception
	when others then
		ds_param_integ_hl7_w := '';
	end;
	end;
end if;
*/

if	(:old.dt_aprovacao is null) and
	(:new.dt_aprovacao is not null) then

	begin

	select	decode(count(*),0,'N','S')
	into	ie_valido_dixtal_w
	from	lab_exame_equip a,
		equipamento_lab b
	where	a.cd_equipamento = b.cd_equipamento
	and	b.ds_sigla = 'DXQT'
	--and	a.cd_equipamento = 24 --Dixtal (QuaTi)
	and	a.nr_seq_exame = :new.nr_seq_exame;

	if (ie_valido_dixtal_w = 'S') then

		select	max(b.cd_pessoa_fisica),
			max(b.nr_prescricao)
		into	cd_pessoa_fisica_w,
			nr_prescricao_w
		from  	exame_lab_resultado a,
			prescr_medica b
		where	a.nr_Seq_resultado = :new.nr_seq_resultado
		and	a.nr_prescricao = b.nr_prescricao;

		ds_param_integ_hl7_w :=	'CD_PESSOA_FISICA=' || cd_pessoa_fisica_w || ds_sep_bv_w ||
					'NR_PRESCRICAO=' || nr_prescricao_w || ds_sep_bv_w ||
					'NR_SEQ_RESULTADO=' || :new.nr_seq_resultado || ds_sep_bv_w ||
					'NR_SEQ_RESULT_ITEM=' || :new.nr_sequencia || ds_sep_bv_w ||
					'NR_SEQ_PRESCR=' || :new.nr_seq_prescr || ds_sep_bv_w;

		gravar_agend_integracao(189, ds_param_integ_hl7_w);

	end if;

	exception
	when others then
		ds_param_integ_hl7_w := '';
	end;

	begin
	select	decode(count(*),0,'N','S')
	into	ie_valido_dixtal_ep12_w
	from	lab_exame_equip a,
		equipamento_lab b
	where	a.cd_equipamento = b.cd_equipamento
	and	b.ds_sigla = 'DIXEP12'
	--and	a.cd_equipamento = 24 --Dixtal (EP12)
	and	a.nr_seq_exame = :new.nr_seq_exame;

	if (ie_valido_dixtal_ep12_w = 'S') then

		select	max(b.cd_pessoa_fisica),
			max(b.nr_prescricao)
		into	cd_pessoa_fisica_w,
			nr_prescricao_w
		from  	exame_lab_resultado a,
			prescr_medica b
		where	a.nr_Seq_resultado = :new.nr_seq_resultado
		and	a.nr_prescricao = b.nr_prescricao;

		ds_param_integ_hl7_w :=	'CD_PESSOA_FISICA=' || cd_pessoa_fisica_w || ds_sep_bv_w ||
					'NR_PRESCRICAO=' || nr_prescricao_w || ds_sep_bv_w ||
					'NR_SEQ_RESULTADO=' || :new.nr_seq_resultado || ds_sep_bv_w ||
					'NR_SEQ_RESULT_ITEM=' || :new.nr_sequencia || ds_sep_bv_w ||
					'NR_SEQ_PRESCR=' || :new.nr_seq_prescr || ds_sep_bv_w;

		gravar_agend_integracao(217, ds_param_integ_hl7_w);

	end if;

	exception
	when others then
		ds_param_integ_hl7_w := '';
	end;


	begin


	select count(*)
	into	qt_reg_w
	from	PHILIPS_INTELLIVUE_LAB;

	if (qt_reg_w > 0) then

		select count(*)
		into	qt_reg_w
		from	PHILIPS_INTELLIVUE_LAB x
		where	exists	(	select	1
							from	exame_lab_result_item a
							where	a.nr_seq_resultado = :new.nr_seq_resultado
							and		x.nr_seq_exame = a.nr_seq_exame);


		if	(qt_reg_w	> 0) then
			begin

			select	max(b.cd_pessoa_fisica),
					max(b.nr_atendimento)
			into	cd_pessoa_fisica_w,
					nr_atendimento_w
			from  	exame_lab_resultado a,
					prescr_medica b
			where	a.nr_Seq_resultado = :new.nr_seq_resultado
			and		a.nr_prescricao = b.nr_prescricao;


			if (nr_atendimento_w is not null) then
				ds_param_integ_hl7_w := 'nr_atendimento=' || nr_atendimento_w || ds_sep_bv_w ||
										'NR_SEQ_RESULTADO=' || :new.nr_seq_resultado|| ds_sep_bv_w ||
					'cd_pessoa_fisica=' || cd_pessoa_fisica_w|| ds_sep_bv_w;

				gravar_agend_integracao(446, ds_param_integ_hl7_w);
			end if;

			end;
		end if;

	end if;

	commit;

	exception
	when others then
		ds_param_integ_hl7_w := '';
	end;





end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.exame_lab_result_item_bef_upd
before update ON TASY.EXAME_LAB_RESULT_ITEM for each row
declare
nr_prescricao_w		exame_lab_resultado.nr_prescricao%type;
cd_estabelecimento_w	prescr_medica.cd_estabelecimento%type;
nr_atendimento_w	prescr_medica.nr_atendimento%type;
ie_imp_cultura_aprov_w	cih_parametros.ie_imp_cultura_aprov%type;
cd_medico_resp_w	atendimento_paciente.cd_medico_resp%type;
ie_clinica_w		atendimento_paciente.ie_clinica%type;
nr_ficha_ocorrencia_w	cih_ficha_ocorrencia.nr_ficha_ocorrencia%type;
ie_libera_ficha_w	Varchar2(255);
qt_ficha_ocorrencia_w	Number(5);

pragma autonomous_transaction;

begin
if	((:new.dt_aprovacao is not null) and
	(:old.dt_aprovacao is null)) then

	select 	max(nr_prescricao)
	into	nr_prescricao_w
	from 	exame_lab_resultado
	where 	nr_seq_resultado = :new.nr_seq_resultado;

	select	nvl(max(cd_estabelecimento),1),
		max(nr_atendimento)
	into	cd_estabelecimento_w,
		nr_atendimento_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_w;

	select	nvl(max(ie_imp_cultura_aprov),'N')
	into	ie_imp_cultura_aprov_w
	from	cih_parametros
	where	cd_estabelecimento = cd_estabelecimento_w;

	if	(ie_imp_cultura_aprov_w = 'S') then

		select	count(*)
		into	qt_ficha_ocorrencia_w
		from	cih_ficha_ocorrencia
		where	nr_atendimento = nr_atendimento_w;

		if	(qt_ficha_ocorrencia_w = 0) then

			select	max(cd_medico_resp),
				max(ie_clinica)
			into	cd_medico_resp_w,
				ie_clinica_w
			from	atendimento_paciente
			where	nr_atendimento = nr_atendimento_w;

			Obter_Param_Usuario(936,23,obter_perfil_ativo,:new.nm_usuario,wheb_usuario_pck.get_cd_estabelecimento,ie_libera_ficha_w);

			ccih_gerar_ficha_ocorrencia(	nr_atendimento_w,
							null,
							cd_medico_resp_w,
							:new.nm_usuario,
							ie_libera_ficha_w,
							ie_clinica_w);
		end if;

		select	max(nr_ficha_ocorrencia)
		into	nr_ficha_ocorrencia_w
		from	cih_ficha_ocorrencia
		where	nr_atendimento = nr_atendimento_w;

		if	(nvl(nr_ficha_ocorrencia_w,0) > 0) then
			cih_importar_cultura(	nr_ficha_ocorrencia_w, :new.nm_usuario);
		end if;
		commit;
	end if;
end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.Exame_lab_result_item_insert
before insert ON TASY.EXAME_LAB_RESULT_ITEM for each row
declare
ie_vancocinemia_w		varchar2(1);
nr_prescricao_w			number(15);

cd_estabelecimento_w	estabelecimento.cd_Estabelecimento%type;
ie_identifica_valor_w	lab_parametro.ie_identifica_valor%type;
nr_sequencia_padrao_w	number(10);
nr_seq_exame_w			exame_lab_result_item.nr_seq_exame%type;
nr_seq_metodo_w			exame_lab_result_item.nr_seq_metodo%type;
nr_seq_material_w		exame_lab_result_item.nr_seq_material%type;

nr_sequencia_criterio_w lab_valor_padrao_criterio.nr_sequencia%type;

ie_atual_ref_ger_laudo_w	varchar2(1);

Begin

if (:new.nr_seq_resultado is not null) then
	select	max(nr_prescricao)
	into	nr_prescricao_w
	from	exame_lab_resultado
	where	nr_seq_resultado = :new.nr_seq_resultado;
end if;

Cd_Estabelecimento_W  := Obter_Valores_Prescr_Trigger(Nr_Prescricao_W, 'E');

select 	nvl(max(ie_atual_ref_ger_laudo),'N')
into	ie_atual_ref_ger_laudo_w
from	lab_parametro
where	cd_estabelecimento = cd_estabelecimento_w;

if	(:new.dt_aprovacao is not null) then

	select	nvl(max(ie_vancocinemia), 'N')
	into	ie_vancocinemia_w
	from	exame_laboratorio
	where	nr_seq_exame = :new.nr_seq_exame;

	if	(ie_vancocinemia_w = 'S') then
		begin
			Gerar_resultado_vancocinemia(:new.nm_usuario_aprovacao, null, nr_prescricao_w, 'S', sysdate, nvl(:new.qt_resultado, to_number(replace(nvl(:new.ds_resultado_curto,:new.ds_resultado),'.',','))), 'N');
			exception when others then
				ie_vancocinemia_w	:= 'N';
		end;
	end if;
end if;

if(nr_prescricao_w is not null) then
	begin
		:new.ie_resultado_critico := Lab_obter_se_nao_aceitavel(nr_prescricao_w, :new.nr_seq_prescr, :new.nr_seq_exame, :new.nr_seq_material, :new.ds_resultado, :new.qt_resultado, :new.pr_resultado);
		exception when others then
			:new.ie_resultado_critico := 'N';
	end;
	begin
		:new.ie_resultado_relevante := lab_obter_se_result_relev(nr_prescricao_w, :new.nr_seq_prescr, :new.nr_seq_exame, :new.nr_seq_material, :new.ds_resultado, :new.qt_resultado, :new.pr_resultado);
		exception when others then
			:new.ie_resultado_relevante := 'N';
	end;
end if;

:new.ds_obs_curta := SUBSTR(:new.ds_observacao,1,255);


if (nr_prescricao_w is not null) then

	select	nvl(max(nr_seq_exame), :new.nr_seq_exame),
			nvl(max(nr_seq_metodo), :new.nr_seq_metodo),
			nvl(max(nr_seq_material), :new.nr_seq_material)
	into	nr_seq_exame_w,
			nr_seq_metodo_w,
			nr_seq_material_w
	from	Exame_lab_result_item
	where	nr_seq_resultado = :new.nr_seq_resultado and
			nr_seq_prescr = :new.nr_seq_prescr and
			nr_seq_material is not null;


    if((ie_atual_ref_ger_laudo_w = 'S') or (ie_atual_ref_ger_laudo_w = 'N') ) then
        define_reference_result_values(	nr_prescricao_w,
                                        :new.nr_seq_prescr,
                                        :new.nr_seq_resultado,
                                        :new.qt_resultado,
                                        :new.pr_resultado,
                                        :new.ds_resultado,
                                        :new.nr_seq_exame,
                                        nr_seq_material_w,
                                        nr_seq_metodo_w,
                                        :new.qt_minima,
                                        :new.qt_maxima,
                                        :new.pr_minimo,
                                        :new.pr_maximo,
                                        :new.ds_referencia,
                                        :new.ie_consiste,
                                        nr_sequencia_padrao_w,
                                        :new.ds_mensagem_criterio,
                                        :new.ie_acao_criterio_lote,
                                        nr_sequencia_criterio_w);
    end if;

  select	nvl(max(ie_identifica_valor), 'N')
  into	ie_identifica_valor_w
  from	lab_parametro
  where	cd_estabelecimento = cd_estabelecimento_w;

  if ((:new.ie_normalidade <> 845) or (:new.ie_normalidade is null)) then
    :new.ie_normalidade := 758;
    if	(nvl(:new.ds_resultado,'') <> '') and
      (:new.DS_REFERENCIA is not null) then
      if (upper(:new.ds_resultado) <> upper(:new.DS_REFERENCIA)) then
        :new.ie_normalidade := 760;
      end if;
    else
      if	(ie_identifica_valor_w = 'N') then
        if ((:new.pr_minimo is not null) and (:new.pr_maximo is not null)) and
           ((nvl(:new.pr_resultado,0) < :new.PR_MINIMo) or (nvl(:new.pr_resultado,0) > :new.pr_maximo)) then
          :new.ie_normalidade := 760;
        end if;

        if (:new.qt_minima is not null  and :new.qt_maxima is not null) and
           ((nvl(:new.qt_resultado,0) < :new.qt_minima) or (nvl(:new.qt_resultado,0) > :new.qt_maxima)) then
          :new.ie_normalidade := 760;
        end if;
      else
        if	((:new.pr_minimo is not null) and (:new.pr_maximo is not null)) then
          if	(nvl(:new.pr_resultado,0) < :new.PR_MINIMo) then
            :new.ie_normalidade := 1088;
          elsif	(nvl(:new.pr_resultado,0) > :new.pr_maximo) then
            :new.ie_normalidade := 1089;
          end if;
        end if;
        if	((:new.qt_minima is not null) and (:new.qt_maxima is not null)) then
          if	(nvl(:new.qt_resultado,0) < :new.qt_minima) then
            :new.ie_normalidade := 1088;
          elsif	(nvl(:new.qt_resultado,0) > :new.qt_maxima) then
            :new.ie_normalidade := 1089;
          end if;
        end if;
      end if;
    end if;
  end if;

end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.Exame_Lab_Result_Item_Update
BEFORE UPDATE ON TASY.EXAME_LAB_RESULT_ITEM FOR EACH ROW
declare
ie_status_baixa_w			number(2,0);
ie_status_atend_atual_w		number(2,0);
cd_motivo_baixa_w			number(3,0);
nr_prescricao_w				number(14,0);
qt_procedimento_w			number(5);
cd_estabelecimento_w		number(4);
nr_seq_prescr_w				number(10);
cd_procedimento_w			number(15);
ie_origem_proced_w			number(10);
nr_atendimento_w			number(10);
qt_dias_w					number(10,2)	:= 0;
qt_horas_w					number(10,0)	:= 0;
ie_status_acerto_w			Number(1);
nr_seq_proc_w				Number(10);
ie_tipo_valor_w				number(1);
nr_seq_etapa_w				number(10);
ie_tipo_atendimento_w		number(3);
ie_tipo_convenio_w			number(3);
qt_exame_w 					number(10);
nr_seq_evento_w				number(10,0);
nr_seq_interno_w			number(10);
nr_sequencia_padrao_w		number(10)	:= 0;
qt_criterios_w				number(10);
ie_conta_paciente_w			varchar2(1);
cd_pessoa_fisica_w			varchar2(10);
ds_codigo_prof_w			varchar2(20);
cd_exame_w					varchar(10);
ie_formato_resultado_w		varchar2(3);
ie_gera_depend_w			varchar2(1);
nr_seq_lab_w				varchar2(20);
ie_sexo_w					varchar2(1);
ie_atual_equip_resultado_w	varchar2(1);
ie_atual_bioquimico_conta_w	Varchar2(1);
ie_identifica_valor_w		varchar2(1);
ie_atualiza_medic_w			varchar2(1);
cd_cbo_w					varchar2(6)	:= '';
ie_utiliza_usuario_aprov_w	varchar2(1);
nm_usuario_medico_resp_w	varchar2(15);
dt_baixa_w					date;

--Variaveis da lote_ent_sec_ficha
lote_qt_idade_gest_w			number(10);
lote_qt_peso_w					number(10,3);
lote_nr_seq_classif_w			number(10);
lote_nr_seq_grau_w				number(10);
lote_qt_media_w					number(10,4);
lote_qt_gest_w					number(2);
ie_classif_lote_ent_w			number(3);
nr_seq_atecaco_w				number(10,0);
lote_cd_convenio_w				number(5);
lote_medic_uso_w				number(10);
lote_ie_gerar_busca_ativa_w		varchar2(1);
lote_ie_gerar_busca_passiva_w	varchar2(1);
lote_ie_amamentado_w			varchar2(1);
lote_ie_prematuro_w				varchar2(1);
lote_ie_transfundido_w			varchar2(1);
lote_ie_mae_veg_w				varchar2(1);
lote_ie_ictericia_w				varchar2(1);
lote_ie_npp_w					varchar2(1);
lote_ie_cor_pf_w				varchar2(10);
lote_ie_gemelar_w				varchar2(1);
lote_ie_tipo_parto_w			varchar2(1);
lote_ie_alim_leite_w			varchar2(1);
lote_pessoa_fisica_w			varchar2(10);
lote_cd_municipio_w				varchar2(6);
lote_ie_tipo_coleta_w			varchar2(1);
lote_ie_data_coleta_w			date;
lote_ie_hora_coleta_w			date;
lote_ie_data_nascimento_w		date;
lote_ie_hora_nascimento_w		date;
ie_vancocinemia_w				varchar2(1);
ie_valor_nao_aceit_w			varchar2(1);
ds_result_fleury_w				varchar2(4000);
lote_dt_ult_menst_w				date;
ie_corticoide_f_w				lab_valor_padrao_criterio.ie_corticoide%type;
nr_seq_ficha_w					lote_ent_sec_ficha.nr_sequencia%type;
nr_seq_exame_prescr_w				prescr_procedimento.nr_seq_exame%type;
nr_seq_material_prescr_w			material_exame_lab.nr_Sequencia%type;

dt_coleta_w						date;
dt_transf_w						date;
qt_dias_coleta_w				number(10,0) := 0;
qt_horas_coleta_w				number(10,0) := 0;
qt_dias_transf_w				number(10,0) := 0;
qt_horas_transf_w				number(10,0) := 0;
cd_pessoa_fisica_prescr_w		varchar2(10);
ie_existe_w						varchar2(1);
qt_casas_decimais_dias_w		number(10,0) := 2;
cd_procedencia_w				atendimento_paciente.cd_procedencia%type;

--Task list variables
nr_seq_regra_w					wl_regra_item.nr_sequencia%type;
qt_regra_wl_exame_w				wl_regra_item.qt_tempo_normal%type;
nr_seq_tipo_adm_fat_atd_w		atendimento_paciente.nr_seq_tipo_admissao_fat%type;
ie_tipo_atendimento_wl_w		atendimento_paciente.ie_tipo_atendimento%type;
nr_seq_episodio_w				atendimento_paciente.nr_seq_episodio%type;
ie_gerar_pendencia_tl_w			subtipo_episodio.ie_gerar_pendencia%type;

nr_sequencia_criterio_w lab_valor_padrao_criterio.nr_sequencia%type;

cursor c01 is
	select	nr_sequencia_prescricao
	from	procedimento_paciente
	where	nr_prescricao = nr_prescricao_w
	  and	nr_seq_exame	in (	select nr_seq_exame_dep
					from exame_lab_dependente
					where nr_seq_exame	= :new.nr_seq_exame
					  and ie_excluir	= 'S')
	union
	select	nr_sequencia_prescricao
	from	procedimento_paciente
	where	nr_prescricao = nr_prescricao_w
	  and	nr_seq_exame	in (	select nr_seq_exame
					from procedimento_proc_prescr
					where cd_procedimento	= cd_procedimento_w
					  and ie_origem_proced	= ie_origem_proced_w
					  and nr_seq_exame		<> :new.nr_seq_exame);
cursor c02 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.ie_evento_disp = 'APRTEA'
	and	nvl(a.ie_situacao,'A') = 'A'
	and	((a.cd_procedencia = cd_procedencia_w) or (a.cd_procedencia is null));

/*cursor c03 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.ie_evento_disp = 'APRNAF'
	and	nvl(a.ie_situacao,'A') = 'A';	*/

cursor c04 is
	select	a.nr_seq_evento
	from	regra_envio_sms a
	where	a.ie_evento_disp = 'APRPRP'
	and	nvl(a.ie_situacao,'A') = 'A';

cursor c05 is
	select	nvl(b.qt_tempo_normal, 0),
			nvl(b.nr_sequencia, 0)
	from 	wl_regra_worklist a,
			wl_regra_item b
	where	a.nr_sequencia = b.nr_seq_regra
	and		b.ie_situacao = 'A'
	and		a.nr_seq_item = (	select	max(x.nr_sequencia)
								from	wl_item x
								where	x.nr_sequencia = a.nr_seq_item
								and		x.cd_categoria = 'EX'
								and		x.ie_situacao = 'A');

BEGIN

if (nvl(:old.ds_resultado_curto,'X') <> nvl(:new.ds_resultado_curto,'X')) then
	:new.ds_resultado := :new.ds_resultado_curto;
end if;
wheb_usuario_pck.set_ie_commit('N');

select	nvl(max(ie_atual_equip_resultado),'N')
into	ie_atual_equip_resultado_w
from	lab_parametro;
if  	-- (:new.ie_status is null) and
	(:new.cd_equipamento is null) and
	(:old.cd_equipamento is null) and
	(nvl(ie_atual_equip_resultado_w,'N') = 'S') then
	select  max(a.cd_equipamento)
	into	:new.cd_equipamento
	from	equipamento_lab b, lab_exame_equip a
	where 	a.cd_equipamento = b.cd_equipamento
	and 	a.nr_seq_exame = :new.nr_seq_exame
	and	nvl(a.nr_prior_geracao,0) = (select nvl(MAX(t.nr_prior_geracao),0) from lab_exame_equip t where t.nr_seq_exame = a.nr_seq_exame);
end if;

if	(:new.ie_status is null) and /* Bruna 12/03/2007, OS52247, inclui a linha abaixo */
	((nvl(:new.qt_resultado,0) <> 0) or
	 (nvl(:new.pr_resultado,0) <> 0) or
	 (nvl(:new.ds_resultado,' ') <> ' ')) then
    :new.ie_status := 1;
end if;

if  (:old.nm_usuario_prim_dig is null) and
	((nvl(:new.qt_resultado,0) <> 0) or
	 (nvl(:new.pr_resultado,0) <> 0) or
	 (nvl(:new.ds_resultado,' ') <> ' ')) then
	:new.nm_usuario_prim_dig := :new.nm_usuario;
end if;
if	(:old.dt_aprovacao is null) and
	(:new.dt_aprovacao is not null) then

	select 	max(nr_prescricao)
	into	nr_prescricao_w
	from 	exame_lab_resultado
	where 	nr_seq_resultado = :new.nr_seq_resultado;

	select	nvl(max(cd_estabelecimento),1),
		max(nr_atendimento),
		max(cd_pessoa_fisica)
	into	cd_estabelecimento_w,
		nr_atendimento_w,
		cd_pessoa_fisica_prescr_w
	from	prescr_medica
	where	nr_prescricao = nr_prescricao_w;

	select	max(ie_formato_resultado),
			nvl(max(ie_vancocinemia), 'N')
	into	ie_formato_resultado_w,
			ie_vancocinemia_w
	from	exame_laboratorio
	where	nr_seq_exame = :new.nr_seq_exame;

	if	(ie_vancocinemia_w = 'S') then
		Gerar_resultado_vancocinemia(:new.nm_usuario_aprovacao, nr_atendimento_w, nr_prescricao_w, 'S', sysdate, :new.qt_resultado, 'N');
	end if;

	if	(nr_atendimento_w	is not null) then
		begin
			gravar_agend_rothman(nr_atendimento_w,:new.nr_seq_resultado,'EX',:new.nm_usuario_aprovacao);
		exception
		when others then
			null;
		end;
	end if;

	select	nvl(max(ie_utiliza_usuario_aprov),'N')
	into	ie_utiliza_usuario_aprov_w
	from	lab_parametro
	where	cd_estabelecimento = cd_estabelecimento_w;

	if (ie_utiliza_usuario_aprov_w = 'S') then
		nm_usuario_medico_resp_w := :new.nm_usuario_aprovacao;
	else
		nm_usuario_medico_resp_w := :new.nm_usuario;
	end if;

	if	(nm_usuario_medico_resp_w is not null) and
		(:new.cd_medico_resp is null) then
		select	nvl(max(cd_pessoa_fisica),null)
		into	cd_pessoa_fisica_w
		from	usuario
		where	upper(nm_usuario) = upper(nm_usuario_medico_resp_w);

		if	(cd_pessoa_fisica_w is not null) then
			select	nvl(max(substr(obter_crm_medico(cd_pessoa_fisica), 1, 255)),'')
			into	ds_codigo_prof_w
			from 	medico
			where 	cd_pessoa_fisica = cd_pessoa_fisica_w;

			select	nvl(max(ds_codigo_prof), ds_codigo_prof_w)
			into	ds_codigo_prof_w
			from 	pessoa_fisica
			where 	cd_pessoa_fisica = cd_pessoa_fisica_w
			and		(length(ds_codigo_prof_w) = 0 or
					ds_codigo_prof_w is null);

			if	(ds_codigo_prof_w is not null) then
				:new.cd_medico_resp := cd_pessoa_fisica_w;
			end if;
		end if;
	end if;
	enviar_comunic_aprov_exame(nr_prescricao_w, :new.nr_seq_prescr, :new.dt_aprovacao, :new.nm_usuario);
    gerar_prot_assistencial_js(nr_atendimento_w, :new.nm_usuario, :new.nr_seq_resultado, :new.nr_sequencia);
	select	count(*)
	into	qt_procedimento_w
	from	procedimento_paciente
	where	nr_prescricao = nr_prescricao_w
	and		nr_sequencia_prescricao = :new.nr_seq_prescr;

	select	nvl(cd_motivo_baixa_exame,nvl(cd_motivo_baixa,0)),
			dt_baixa,
			cd_procedimento,
			ie_origem_proced
	into	cd_motivo_baixa_w,
			dt_baixa_w,
			cd_procedimento_w,
			ie_origem_proced_w
	from 	prescr_procedimento
	where 	nr_prescricao = nr_prescricao_w
	and 	nr_sequencia = :new.nr_seq_prescr;

	if	(cd_motivo_baixa_w = 0) then
		select	nvl(max(ie_status_baixa),35),
				nvl(max(cd_motivo_baixa),1)
		into	ie_status_baixa_w,
				cd_motivo_baixa_w
		from	lab_parametro
		where	cd_estabelecimento = cd_estabelecimento_w;

		if 	(qt_procedimento_w = 0) then
			select	ie_conta_paciente
			into	ie_conta_paciente_w
			from	tipo_baixa_prescricao
			where	cd_tipo_baixa = cd_motivo_baixa_w
			and		ie_prescricao_devolucao = 'P';

			if	(ie_conta_paciente_w = 'S') then
				if (wheb_usuario_pck.get_cd_estabelecimento is null) then
					wheb_usuario_pck.set_cd_estabelecimento(cd_estabelecimento_w);
				end if;
				Gerar_Proc_Pac_Prescricao(nr_prescricao_w,
							:new.nr_seq_prescr, obter_perfil_ativo, 722,
							:new.nm_usuario,null,null,:new.cd_medico_resp);
			else
				update	prescr_procedimento
				set		dt_baixa = sysdate,
						cd_motivo_baixa = cd_motivo_baixa_w,
			    		dt_atualizacao	= sysdate,
						nm_usuario	= :new.nm_usuario
				where 	nr_prescricao = nr_prescricao_w
				  and 	nr_sequencia = :new.nr_seq_prescr;
			end if;
		else
			update 	prescr_procedimento
			set		dt_baixa = sysdate,
					cd_motivo_baixa = cd_motivo_baixa_w,
			    	dt_atualizacao	= sysdate,
					nm_usuario	= :new.nm_usuario
			where 	nr_prescricao = nr_prescricao_w
			  and 	nr_sequencia = :new.nr_seq_prescr
			  and 	ie_status_atend >= ie_status_baixa_w;
		end if;
	end if;

	if	((ie_formato_resultado_w = 'SM') or (ie_formato_resultado_w = 'S')) and
		(substr(upper(:new.ds_resultado),1,7) <> 'POSITIV') then
		open c01;
		loop
		fetch c01 into nr_seq_prescr_w;
			exit when c01%notfound;
			estornar_propaci_prescr_proc(nr_prescricao_w, nr_seq_prescr_w, :new.nm_usuario);
		end loop;
		close c01;
	end if;

	select nvl(obter_regra_depend_positivo(nr_prescricao_w, :new.nr_seq_prescr),'N')
	into   ie_gera_depend_w
	from dual;

	if (substr(upper(:new.ds_resultado),1,7) = 'POSITIV') and
	   (ie_gera_depend_w = 'S')	 then
		Gerar_Exame_Lab_Dependente(nr_prescricao_w, :new.nr_seq_prescr, 3, nr_atendimento_w, :new.nm_usuario,null, :new.nr_sequencia, null);
	end if;

	select	ie_formato_resultado
	into	ie_formato_resultado_w
	from	exame_laboratorio
	where	nr_seq_exame = :new.nr_seq_exame;

	select count(*)
	into qt_procedimento_w
	from procedimento_paciente
	where nr_prescricao = nr_prescricao_w
	and nr_sequencia_prescricao = :new.nr_seq_prescr;

	if (substr(upper(:new.ds_resultado),1,7) = 'POSITIV') then
		if (ie_formato_resultado_w = 'SM') then
			Gerar_Exame_Lab_Dependente(nr_prescricao_w, :new.nr_seq_prescr, 1, nr_atendimento_w, :new.nm_usuario,null, :new.nr_sequencia, null);
		elsif (ie_formato_resultado_w = 'S') then
			Gerar_Exame_Lab_Dependente(nr_prescricao_w, :new.nr_seq_prescr, 5, nr_atendimento_w, :new.nm_usuario,null, :new.nr_sequencia, null);
		end if;
	end if;
	if (:new.cd_medico_resp is not null and :old.cd_medico_resp is null) then
		select	nvl(max(ie_atual_bioquimico_conta),'N')
		into	ie_atual_bioquimico_conta_w
		from	lab_parametro
		where	cd_estabelecimento = cd_estabelecimento_w;

		select	max(ie_status_acerto)
		into	ie_status_acerto_w
		from	conta_paciente a,
				procedimento_paciente b
		where	a.nr_interno_conta	= b.nr_interno_conta
		and		nr_prescricao 		= nr_prescricao_w
		and 	nr_sequencia_prescricao = :new.nr_seq_prescr;

		if	(nr_atendimento_w	is not null) then
			begin
			select	ie_tipo_atendimento,
					ie_tipo_convenio,
					verifica_se_atualiza_medico(obter_convenio_atendimento(nr_atendimento))
			into	ie_tipo_atendimento_w,
					ie_tipo_convenio_w,
					ie_atualiza_medic_w
			from	atendimento_paciente
			where	nr_atendimento	= nr_atendimento_w;
			exception
				when others then
				ie_tipo_atendimento_w	:= null;
				end;
		end if;

		select	count(*)
		into 	qt_procedimento_w
		from 	procedimento_paciente
		where 	nr_prescricao = nr_prescricao_w
	  	and 	nr_sequencia_prescricao = :new.nr_seq_prescr;

		if	((ie_atual_bioquimico_conta_w = 'S') or
			((ie_atual_bioquimico_conta_w	= 'B') and (ie_tipo_convenio_w	= 3) and
			(ie_tipo_atendimento_w	<> 1))) and
			(ie_status_acerto_w = 1) and
			(qt_procedimento_w <> 0) and
			(:new.cd_medico_resp is not null) then

			select 	nvl(max(nr_sequencia),0)
			into	nr_seq_proc_w
			from	procedimento_paciente
			where	nr_prescricao 			= nr_prescricao_w
			and 	nr_sequencia_prescricao	= :new.nr_seq_prescr;

			if	(obter_se_medico(:new.cd_medico_resp, 'M') = 'S') then
				update 	procedimento_paciente
				set		cd_medico_executor		= :new.cd_medico_resp
				where	nr_prescricao 			= nr_prescricao_w
				and 	nr_sequencia_prescricao = :new.nr_seq_prescr;

				if (nr_seq_proc_w > 0 ) then
					update	procedimento_paciente
					set		cd_medico_executor	= :new.cd_medico_resp
					where	NR_SEQ_PROC_PRINC	= nr_seq_proc_w;
				end if;
			else
				update 	procedimento_paciente
				set		cd_pessoa_fisica		= :new.cd_medico_resp
				where	nr_prescricao 			= nr_prescricao_w
				and 	nr_sequencia_prescricao = :new.nr_seq_prescr;

				if (nr_seq_proc_w > 0 ) then
					update 	procedimento_paciente
					set		cd_pessoa_fisica	= :new.cd_medico_resp
					where	NR_SEQ_PROC_PRINC	= nr_seq_proc_w;
				end if;
			end if;

			begin
			select	max(a.cd_cbo)
			into	cd_cbo_w
			from    sus_cbo b,
					sus_cbo_pessoa_fisica a,
					procedimento_paciente c
			where   sus_obter_secbo_compativel(:new.cd_medico_resp,c.cd_procedimento, c.ie_origem_proced, c.dt_procedimento, a.cd_cbo, 0) = 'S'
			and     a.cd_pessoa_fisica        	= :new.cd_medico_resp
			and     a.cd_cbo        			= b.cd_cbo
			and		c.nr_prescricao 			= nr_prescricao_w
			and 	c.nr_sequencia_prescricao	= :new.nr_seq_prescr;
			exception
				when others then
				cd_cbo_w := '';
			end;

			update	procedimento_paciente
			set		cd_cbo					= cd_cbo_w
			where	nr_prescricao 			= nr_prescricao_w
			and		nr_sequencia_prescricao = :new.nr_seq_prescr;
		end if;

		if	(((ie_atual_bioquimico_conta_w = 'N') or
			  ((ie_atual_bioquimico_conta_w	= 'B') and (ie_tipo_convenio_w	<> 3))) and
			 (ie_atualiza_medic_w	= 'S') and
			 (ie_status_acerto_w = 1) and
			 (qt_procedimento_w <> 0) and
			 (:new.cd_medico_resp is not null)) then
			select 	nvl(max(nr_sequencia),0)
			into	nr_seq_proc_w
			from	procedimento_paciente
			where	nr_prescricao 			= nr_prescricao_w
			and 	nr_sequencia_prescricao = :new.nr_seq_prescr;

			if	(obter_se_medico(:new.cd_medico_resp, 'M') = 'S') then
				update 	procedimento_paciente
				set		cd_medico_executor		= :new.cd_medico_resp
				where	nr_prescricao 			= nr_prescricao_w
				and 	nr_sequencia_prescricao = :new.nr_seq_prescr;

				if (nr_seq_proc_w > 0 ) then
					update 	procedimento_paciente
					set	cd_medico_executor		= :new.cd_medico_resp
					where	NR_SEQ_PROC_PRINC	= nr_seq_proc_w;
				end if;
			else
				update 	procedimento_paciente
				set		cd_pessoa_fisica		= :new.cd_medico_resp
				where	nr_prescricao 			= nr_prescricao_w
				and 	nr_sequencia_prescricao	= :new.nr_seq_prescr;

				if (nr_seq_proc_w > 0 ) then
					update 	procedimento_paciente
					set		cd_pessoa_fisica	= :new.cd_medico_resp
					where	NR_SEQ_PROC_PRINC	= nr_seq_proc_w;
				end if;
			end if;
		end if;
	end if;

	/*if	((:new.nm_usuario = 'FLEURYWS') or (upper(:new.nm_usuario) = upper('FleuryTasy'))) then

		open c03;
		loop
		fetch c03 into
			nr_seq_evento_w;
		exit when c03%notfound;
			begin

			fleury_ver_nao_aceit_alert(nr_prescricao_w, :new.nr_seq_prescr, :new.nr_seq_exame, :new.nr_seq_material, :new.ds_resultado, :new.qt_resultado, :new.pr_resultado, ie_valor_nao_aceit_w);

			if (nvl(ie_valor_nao_aceit_w,'N') = 'S') then

				if (NVL(:new.qt_resultado,0) = 0) then
					ds_result_fleury_w := nvl(:new.pr_resultado,0);
				else
					ds_result_fleury_w := :new.qt_resultado;
				end if;

				if (ds_result_fleury_w = 0) then
					ds_result_fleury_w := :new.ds_resultado;
				end if;

				gerar_ev_aprov_result_fleury(nr_seq_evento_w,:new.nm_usuario, nr_prescricao_w, :new.nr_seq_prescr, :new.nr_seq_exame, :new.nr_seq_material, :new.cd_medico_resp, :new.dt_aprovacao, 'S', ds_result_fleury_w);
			end if;

			end;
		end loop;
		close c03;

	end if;	*/

	select	max(nr_seq_interno)
	into	nr_seq_interno_w
	from	prescr_procedimento
	where	nr_sequencia = :new.nr_seq_prescr
	and	nr_prescricao = nr_prescricao_w;

	if (nr_seq_interno_w is not null) then
		select	count(*)
		into	qt_exame_w
		from	prescr_procedimento a,
				prescr_medica b
		where	a.ie_status_atend < 35
		and	a.nr_prescricao = b.nr_prescricao
		and	b.nr_atendimento = nr_atendimento_w
		and	a.nr_seq_interno <> nr_seq_interno_w
		and	a.ie_suspenso <> 'S'
		and	a.dt_cancelamento IS NULL;

		select	nvl(max(a.cd_procedencia), 0)
		into	cd_procedencia_w
		from	atendimento_paciente a,
			prescr_medica b
		where	a.nr_atendimento = b.nr_atendimento
		and	b.nr_prescricao = nr_prescricao_w;

		open c02;
		loop
		fetch c02 into
			nr_seq_evento_w;
		exit when c02%notfound;
			begin
			if ( qt_exame_w = 0 ) then
				gerar_evento_aprov_res_atend(nr_seq_evento_w,:new.nm_usuario, nr_prescricao_w, nr_seq_prescr_w, :new.nr_seq_exame, 'S', :new.dt_aprovacao, :new.cd_medico_resp);
			end if;
			end;
		end loop;
		close c02;

		select	count(*)
		into	qt_exame_w
		from	prescr_procedimento a,
				prescr_medica b
		where	a.ie_status_atend >= 35
		and	a.nr_prescricao = b.nr_prescricao
		and	b.nr_prescricao = nr_prescricao_w
		and	a.nr_seq_interno <> nr_seq_interno_w;

		if (qt_exame_w = 0) then

			select	lab_existe_dt_aprov_res_item(:new.nr_seq_resultado, :new.nr_seq_prescr)
			into	ie_existe_w
			from	dual;

			if (ie_existe_w = 'S') then
				open C04;
				loop
				fetch C04 into
					nr_seq_evento_w;
				exit when C04%notfound;
					begin
					gerar_evento_aprov_res_atend(nr_seq_evento_w,:new.nm_usuario, nr_prescricao_w, nr_seq_prescr_w, :new.nr_seq_exame, 'S', :new.dt_aprovacao, :new.cd_medico_resp);
					end;
				end loop;
				close C04;
			end if;

			if (nr_atendimento_w is not null) then

				select	max(nr_seq_tipo_admissao_fat),
						max(ie_tipo_atendimento),
						max(nr_seq_episodio)
				into	nr_seq_tipo_adm_fat_atd_w,
						ie_tipo_atendimento_wl_w,
						nr_seq_episodio_w
				from	atendimento_paciente
				where	nr_atendimento = nr_atendimento_w;

			end if;

			open c05;
			loop
			fetch c05 into
				qt_regra_wl_exame_w,
				nr_seq_regra_w;
			exit when c05%notfound;
				begin
					if	(qt_regra_wl_exame_w > 0 and obter_se_regra_geracao(nr_seq_regra_w,nr_seq_episodio_w,nr_seq_tipo_adm_fat_atd_w) = 'S') then

						-- Generates task in the WorkList function as a pendant for the user to inform that he is aware of the exam
						wl_gerar_finalizar_tarefa('EX','I',nr_atendimento_w,cd_pessoa_fisica_prescr_w,:new.nm_usuario,
									nvl(:new.dt_aprovacao,sysdate)+(qt_regra_wl_exame_w/24),
									'N',null,null,null,nr_prescricao_w,:new.nr_seq_prescr,null,null,null,null,nr_seq_regra_w,null,null,null,null,null,null,null,:new.dt_aprovacao,nr_seq_episodio_w);
					end if;
				end;
			end loop;
			close c05;
		end if;
	end if;
end if;

if 	((:new.nr_seq_material is null) and (:old.nr_seq_material is not null)) then
	:new.nr_seq_material	:= :old.nr_seq_material;
end if;

if 	((:new.nr_seq_material <> :old.nr_seq_material) and (:old.nr_seq_material is not null)) and
	(:new.nr_seq_prescr is not null) then
	select	nr_prescricao
	into	nr_prescricao_w
	from 	exame_lab_resultado
	where 	nr_seq_resultado = :new.nr_seq_resultado;

	select	max(nr_seq_lab)
	into 	nr_seq_lab_w
	from 	prescr_procedimento
	where 	nr_prescricao = nr_prescricao_w
	and 	nr_sequencia = :new.nr_seq_prescr;

	insert into prescr_proc_mat_alteracao (
			nr_sequencia,
			nr_prescricao,
			nr_seq_prescr,
			dt_atualizacao,
			nm_usuario,
			nr_seq_mat_ant,
			nr_seq_mat_atual,
			nr_seq_lab)
	values (prescr_proc_mat_alteracao_seq.nextval,
			nr_prescricao_w,
			:new.nr_seq_prescr,
			sysdate,
			:new.nm_usuario,
			:old.nr_seq_material,
			:new.nr_seq_material,
			nr_seq_lab_w);
	Alterar_seq_lab(nr_prescricao_w,:new.nm_usuario,:new.nr_seq_material,:new.nr_seq_prescr);
end if;

if	((nvl(:new.ds_resultado,'X') <> nvl(:old.ds_resultado,'X')) or
	(nvl(:new.qt_resultado,0) <> nvl(:old.qt_resultado,0)) or
	(nvl(:new.pr_resultado,0) <> nvl(:old.pr_resultado,0)) or
	(nvl(:new.nr_seq_metodo,0) <> nvl(:old.nr_seq_metodo,0)) or
	(nvl(:new.nr_seq_material,0) <> nvl(:old.nr_seq_material,0))) then
	select 	max(nr_prescricao)
	into	nr_prescricao_w
	from 	exame_lab_resultado
	where 	nr_seq_resultado = :new.nr_seq_resultado;

	if	(:new.nm_usuario <> 'FLEURYWS') and
		(:new.nm_usuario <> 'UserApprove') then
		if	((:old.dt_aprovacao is not null)  and  (:new.dt_aprovacao is not null)) then
			wheb_mensagem_pck.exibir_mensagem_abort(196252, 'NR_PRESCRICAO='||to_char(nr_prescricao_w)||';'||'NR_SEQ_EXAME='||to_char(:new.nr_seq_exame));
		else
			select 	max(nr_sequencia)
			into	nr_seq_etapa_w
			from	prescr_proc_etapa
			where	nr_prescricao 		= nr_prescricao_w
			and		nr_seq_prescricao	= :new.nr_seq_prescr;

			if	(nr_seq_etapa_w is not null) then
				select	ie_etapa
				into	ie_status_atend_atual_w
				from	prescr_proc_etapa
				where	nr_sequencia 	= nr_seq_etapa_w
				and		nr_prescricao 	= nr_prescricao_w
				and		nr_seq_prescricao	= :new.nr_seq_prescr;

				if	(ie_status_atend_atual_w >= 35) then
					wheb_mensagem_pck.exibir_mensagem_abort(196252, 'NR_PRESCRICAO='||to_char(nr_prescricao_w)||';'||'NR_SEQ_EXAME='||to_char(:new.nr_seq_exame));
				end if;
			end if;
		end if;
	end if;

	select 	MAX(nr_sequencia)
	into	nr_seq_ficha_w
	from 	lote_ent_sec_ficha
	where 	nr_prescricao = nr_prescricao_w;

	if (nr_seq_ficha_w is not null) then

		select 	max(a.nr_seq_exame),
			max(b.nr_sequencia)
		into	nr_seq_exame_prescr_w,
			nr_seq_material_prescr_w
		from	prescr_procedimento a,
			material_exame_lab  b
		where 	a.cd_material_exame = b.cd_material_exame
		and	a.nr_prescricao = nr_prescricao_w
		and	a.nr_sequencia  = :new.nr_seq_prescr;


		if (nr_seq_exame_prescr_w is not null) then

--b.nr_seq_metodo,
--b.nr_seq_resultado

			:new.ie_tipo_val_crit 	:= SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(:new.qt_resultado,:new.pr_resultado), :new.ds_resultado, nr_prescricao_w, :new.nr_seq_prescr,1, :new.nr_seq_metodo, :new.nr_seq_resultado),1,255) ;
			:new.ie_valor_crit 	:= SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(:new.qt_resultado,:new.pr_resultado), :new.ds_resultado, nr_prescricao_w, :new.nr_seq_prescr,2, :new.nr_seq_metodo, :new.nr_seq_resultado),1,2) ;
			--:new.ds_mensagem_criterio :=  SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(:new.qt_resultado,:new.pr_resultado), :new.ds_resultado, nr_prescricao_w, :new.nr_seq_prescr,3, :new.nr_seq_metodo, :new.nr_seq_resultado),1,4000);
                                                :new.ds_mensagem_criterio :=  SUBSTR(nvl(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(:new.qt_resultado,:new.pr_resultado), :new.ds_resultado, nr_prescricao_w, :new.nr_seq_prescr,3, :new.nr_seq_metodo, :new.nr_seq_resultado), :new.ds_mensagem_criterio),1,4000);
			:new.ds_acao_criterio 	:= SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(:new.qt_resultado,:new.pr_resultado), :new.ds_resultado, nr_prescricao_w, :new.nr_seq_prescr,4, :new.nr_seq_metodo, :new.nr_seq_resultado),1,255) ;
			:new.ie_acao_criterio_lote := SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(:new.qt_resultado,:new.pr_resultado), :new.ds_resultado, nr_prescricao_w, :new.nr_seq_prescr,7, :new.nr_seq_metodo, :new.nr_seq_resultado),1,1) ;
			:new.ds_result_sug_crit := SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(:new.qt_resultado,:new.pr_resultado), :new.ds_resultado, nr_prescricao_w, :new.nr_seq_prescr,8, :new.nr_seq_metodo, :new.nr_seq_resultado),1,255) ;
			:new.ds_material_criterio := SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(:new.qt_resultado,:new.pr_resultado), :new.ds_resultado, nr_prescricao_w, :new.nr_seq_prescr,9, :new.nr_seq_metodo, :new.nr_seq_resultado),1,255) ;
			:new.ds_metodo_criterio := SUBSTR(lab_cons_valor_result_lote(nr_seq_exame_prescr_w, nr_seq_material_prescr_w, NVL(:new.qt_resultado,:new.pr_resultado), :new.ds_resultado, nr_prescricao_w, :new.nr_seq_prescr,10, :new.nr_seq_metodo, :new.nr_seq_resultado),1,255) ;

		end if;
	end if;


end if;

select	nvl(max(cd_estabelecimento),1)
into	cd_estabelecimento_w
from	prescr_medica
where	nr_prescricao = nr_prescricao_w;


if (((:new.nr_seq_metodo <> :old.nr_seq_metodo) or
    (:new.nr_seq_material <> :old.nr_seq_material)) and
    :old.nr_seq_material is not null) then

	select 	nr_prescricao
	into	nr_prescricao_w
	from 	exame_lab_resultado
	where 	nr_seq_resultado = :new.nr_seq_resultado;

	select 	obter_dias_entre_datas_lab(obter_nascimento_prescricao(nr_prescricao_w),sysdate),
			Obter_Hora_Entre_datas(obter_nascimento_prescricao(nr_prescricao_w),sysdate)
	into 	qt_dias_w,
			qt_horas_w
	from 	dual;

	select	obter_sexo_prescricao(nr_prescricao_w)
	into 	ie_sexo_w
	from 	dual;

	select	max(qt_peso_f),
			max(nr_idade_gest_f),
			max(ie_premat_s_f),
			max(IE_AMAMENTADO_F),
			max(ie_transfusao_f),
			max(nr_seq_grau_parentesco),
			max(substr(obter_classif_atendimento(nr_atendimento_w),1,10)),
			max(IE_MAE_VEG_F),
			max(IE_ICTERICIA_F),
			max(IE_NPP_F),
			max(IE_COR_PF_F),
			max(IE_GEMELAR_F),
			max(substr(lote_ent_obter_result_ant(nr_prescricao_w, :new.nr_seq_prescr, :new.nr_seq_resultado, :new.nr_seq_exame, 99,'S',:new.qt_resultado,:new.pr_resultado),1,255)),
			max(QT_GEST_F),
			max(DT_ULT_MENST),
			max(IE_TIPO_PARTO),
			max(IE_ALIM_LEITE_F),
			max(cd_pessoa_fisica),
			max(IE_CORTICOIDE_F),
			nvl(max(nr_sequencia),0),
			max(DT_COLETA_FICHA_F),
			max(HR_COLETA_F),
			max(DT_NASCIMENTO_F),
			max(HR_NASCIMENTO_F)
	into	lote_qt_peso_w,
			lote_qt_idade_gest_w,
			lote_ie_prematuro_w,
			lote_ie_amamentado_w,
			lote_ie_transfundido_w,
			lote_nr_seq_grau_w,
			lote_nr_seq_classif_w,
			lote_ie_mae_veg_w,
			lote_ie_ictericia_w,
			lote_ie_npp_w,
			lote_ie_cor_pf_w,
			lote_ie_gemelar_w,
			lote_qt_media_w,
			lote_qt_gest_w,
			lote_dt_ult_menst_w,
			lote_ie_tipo_parto_w,
			lote_ie_alim_leite_w,
			lote_pessoa_fisica_w,
			ie_corticoide_f_w,
			nr_seq_ficha_w,
			lote_ie_data_coleta_w,
			lote_ie_hora_coleta_w,
			lote_ie_data_nascimento_w,
			lote_ie_hora_nascimento_w
	from	lote_ent_sec_ficha
	where	nr_prescricao = nr_prescricao_w;

	begin
	if (nr_seq_ficha_w <> 0) then

		define_reference_result_values(	nr_prescricao_w,
										nr_seq_prescr_w,
										:new.nr_seq_resultado,
										:new.qt_resultado,
										:new.pr_resultado,
                                        :new.ds_resultado,
										:new.nr_seq_exame,
										:new.nr_seq_material,
										:new.nr_seq_metodo,
										:new.qt_minima,
										:new.qt_maxima,
										:new.pr_minimo,
										:new.pr_maximo,
										:new.ds_referencia,
										:new.ie_consiste,
										nr_sequencia_padrao_w,
										:new.ds_mensagem_criterio,
										:new.ie_acao_criterio_lote,
                                        nr_sequencia_criterio_w);


	end if;

	select 	decode(nvl(max(ie_idade_int_val_ref), 'N'), 'N', 2, 0)
	into	qt_casas_decimais_dias_w
	from	lab_parametro
	where	cd_estabelecimento = cd_estabelecimento_w;

	if (nr_sequencia_padrao_w = 0) then

		define_reference_result_values(	nr_prescricao_w,
										nr_seq_prescr_w,
										:new.nr_seq_resultado,
										:new.qt_resultado,
										:new.pr_resultado,
                                        :new.ds_resultado,
										:new.nr_seq_exame,
										:new.nr_seq_material,
										:new.nr_seq_metodo,
										:new.qt_minima,
										:new.qt_maxima,
										:new.pr_minimo,
										:new.pr_maximo,
										:new.ds_referencia,
										:new.ie_consiste,
										nr_sequencia_padrao_w,
										:new.ds_mensagem_criterio,
										:new.ie_acao_criterio_lote,
                                        nr_sequencia_criterio_w);
	end if;

	exception
		when others then

		:new.qt_minima 	:= null;
		:new.qt_maxima 	:= null;
		:new.pr_minimo 	:= null;
		:new.pr_maximo 	:= null;
		:new.ds_referencia	:= null;
		:new.ie_consiste	:= 'N';
		:new.ie_acao_criterio_lote := null;
		:new.ds_mensagem_criterio := null;
	end;
end if;

if  ((nvl(:old.qt_resultado,0) <> nvl(:new.qt_resultado,0)) or (nvl(:old.pr_resultado,0) <> nvl(:new.pr_resultado,0)) or (:new.ds_resultado is null and :old.ds_resultado is not null))
and ((nvl(:new.qt_resultado,0) = 0) and (nvl(:new.pr_resultado,0) = 0))
and (nr_seq_ficha_w <> 0) then
  :new.ds_mensagem_criterio := null;
  :new.ds_observacao := null;
end if;

if (nvl(substr(:new.ds_obs_curta,1,255),'X') <> nvl(substr(:old.ds_observacao,1,255),'X')) then
  :new.ds_observacao := :new.ds_obs_curta;
end if;

if (nvl(substr(:new.ds_observacao,1,255),'X') <> nvl(substr(:old.ds_obs_curta,1,255),'X')) then
  :new.ds_obs_curta := SUBSTR(:new.ds_observacao,1,255);
end if;

select  nr_prescricao
into  nr_prescricao_w
from   exame_lab_resultado
where   nr_seq_resultado = :new.nr_seq_resultado;

select  nvl(max(ie_identifica_valor), 'N')
into  ie_identifica_valor_w
from  lab_parametro
where  cd_estabelecimento = cd_estabelecimento_w;

if ((:new.ie_normalidade <> 845) or (:new.ie_normalidade is null)) then
  :new.ie_normalidade := 758;
  if   (nvl(:new.ds_resultado,'') <> '') and
    (:new.DS_REFERENCIA is not null) then
    if (upper(:new.ds_resultado) <> upper(:new.DS_REFERENCIA)) then
      :new.ie_normalidade := 760;
    end if;
  else
    if  (ie_identifica_valor_w = 'N') then
      if ((:new.pr_minimo is not null) and (:new.pr_maximo is not null)) and
         ((nvl(:new.pr_resultado,0) < :new.PR_MINIMo) or (nvl(:new.pr_resultado,0) > :new.pr_maximo)) then
        :new.ie_normalidade := 760;
      end if;

      if (:new.qt_minima is not null  and :new.qt_maxima is not null) and
         ((nvl(:new.qt_resultado,0) < :new.qt_minima) or (nvl(:new.qt_resultado,0) > :new.qt_maxima)) then
        :new.ie_normalidade := 760;
      end if;
    else
      if  ((:new.pr_minimo is not null) and (:new.pr_maximo is not null)) then
        if  (nvl(:new.pr_resultado,0) < :new.PR_MINIMo) then
          :new.ie_normalidade := 1088;
        elsif   (nvl(:new.pr_resultado,0) > :new.pr_maximo) then
          :new.ie_normalidade := 1089;
        end if;
      end if;
      if  ((:new.qt_minima is not null) and (:new.qt_maxima is not null)) then
        if  (nvl(:new.qt_resultado,0) < :new.qt_minima) then
          :new.ie_normalidade := 1088;
        elsif   (nvl(:new.qt_resultado,0) > :new.qt_maxima) then
          :new.ie_normalidade := 1089;
        end if;
      end if;
    end if;
  end if;

  if (:new.IE_RESULTADO_CRITICO = 'S') or
     (:new.IE_RESULTADO_REFERENCIA = 'S') then
    :new.ie_normalidade := 760;
  end if;

end if;

if  (:old.dt_aprovacao is not null) and
  (:new.dt_aprovacao is null) then
  delete  exame_lab_result_item_mas
  where  nr_seq_resultado = :new.nr_seq_resultado
  and  nr_seq_prescr = :new.nr_seq_prescr;

  gravar_log_lab_pragma(36,:new.nr_seq_resultado||'-'||:new.nr_seq_prescr||'-'||substr(dbms_utility.format_call_stack,1,2000),:new.nm_usuario);
end if;

if  ((nvl(:new.qt_resultado,0) <> nvl(:old.qt_resultado,0)) or
  (nvl(:new.ds_resultado,'X') <> nvl(:old.ds_resultado,'X')) or
  (nvl(:new.pr_resultado,0) <> nvl(:old.pr_resultado,0)) or
  (nvl(:new.nr_seq_material,0) <> nvl(:old.nr_seq_material,0))) then

  begin
    select       max(b.nr_sequencia)
    into        nr_seq_material_prescr_w
    from  prescr_procedimento a,
      material_exame_lab  b
    where   a.cd_material_exame = b.cd_material_exame
    and  a.nr_prescricao = nr_prescricao_w
    and  a.nr_sequencia  = :new.nr_seq_prescr;

    :new.ie_resultado_critico := Lab_obter_se_nao_aceitavel(nr_prescricao_w, :new.nr_seq_prescr, :new.nr_seq_exame, nr_seq_material_prescr_w, :new.ds_resultado, :new.qt_resultado, :new.pr_resultado);
    exception when others then
      :new.ie_resultado_critico := 'N';
  end;
  begin
    :new.ie_resultado_relevante := lab_obter_se_result_relev(nr_prescricao_w, :new.nr_seq_prescr, :new.nr_seq_exame, :new.nr_seq_material, :new.ds_resultado, :new.qt_resultado, :new.pr_resultado);
    exception when others then
      :new.ie_resultado_relevante := 'N';
  end;
end if;
wheb_usuario_pck.set_ie_commit('S');

END;
/


CREATE OR REPLACE TRIGGER TASY.exame_lab_result_item_up_assi
BEFORE UPDATE ON TASY.EXAME_LAB_RESULT_ITEM FOR EACH ROW
DECLARE
qt_reg_w		number(1);

BEGIN
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

--Na atualiza��o da assinatura de aprovacao, dever� referenciar a assinatura anterior de reprova��o:
if (:new.nr_seq_assinatura > 0) and
   ((:old.nr_seq_assinatura is null) or (:old.nr_seq_assinatura <> :new.nr_seq_assinatura)) then

	update tasy_assinatura_digital
	set nr_seq_assinatura_anterior = decode(nvl(:new.nr_seq_assinat_inativ,0), 0, :old.nr_seq_assinatura, :new.nr_seq_assinat_inativ)
	where nr_sequencia = :new.nr_seq_assinatura;

	:new.nr_seq_assinat_inativ := null;

end if;

--Na atualiza��o da assinatura de reprovacao, dever� referenciar a assinatura anterior de aprova��o:
if (:new.nr_seq_assinat_inativ > 0) and
   ((:old.nr_seq_assinat_inativ is null) or (:old.nr_seq_assinat_inativ <> :new.nr_seq_assinat_inativ)) then

	update tasy_assinatura_digital
	set nr_seq_assinatura_anterior = decode(nvl(:new.nr_seq_assinatura,0),0, :old.nr_seq_assinat_inativ, :new.nr_seq_assinatura)
	where nr_sequencia = :new.nr_seq_assinat_inativ;

	:new.nr_seq_assinatura := null;

end if;

<<Final>>
qt_reg_w	:= 0;

END;
/


ALTER TABLE TASY.EXAME_LAB_RESULT_ITEM ADD (
  CONSTRAINT EXALARI_PK
 PRIMARY KEY
 (NR_SEQ_RESULTADO, NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          152M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.EXAME_LAB_RESULT_ITEM ADD (
  CONSTRAINT EXALARI_LABPATO_FK 
 FOREIGN KEY (NR_SEQ_LOTE_PATOL) 
 REFERENCES TASY.LAB_PATOLOGIA (NR_SEQUENCIA),
  CONSTRAINT EXALARI_MATEXLAINT_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL_INTEGR) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT EXALARI_LABMBLORES_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_BLOQUEIO) 
 REFERENCES TASY.LAB_MOTIVO_BLOQUEIO_RESULT (NR_SEQUENCIA),
  CONSTRAINT EXALARI_EQUILAB_FK 
 FOREIGN KEY (CD_EQUIPAMENTO) 
 REFERENCES TASY.EQUIPAMENTO_LAB (CD_EQUIPAMENTO),
  CONSTRAINT EXALARI_EXALARE_FK 
 FOREIGN KEY (NR_SEQ_RESULTADO) 
 REFERENCES TASY.EXAME_LAB_RESULTADO (NR_SEQ_RESULTADO)
    ON DELETE CASCADE,
  CONSTRAINT EXALARI_LABREAG_FK 
 FOREIGN KEY (NR_SEQ_REAGENTE) 
 REFERENCES TASY.LAB_REAGENTE (NR_SEQUENCIA),
  CONSTRAINT EXALARI_PESFISI_FK 
 FOREIGN KEY (CD_MEDICO_RESP) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT EXALARI_EXALAFO_FK 
 FOREIGN KEY (NR_SEQ_FORMATO) 
 REFERENCES TASY.EXAME_LAB_FORMAT (NR_SEQ_FORMATO),
  CONSTRAINT EXALARI_METEXLA_FK 
 FOREIGN KEY (NR_SEQ_METODO) 
 REFERENCES TASY.METODO_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT EXALARI_MATEXLA_FK 
 FOREIGN KEY (NR_SEQ_MATERIAL) 
 REFERENCES TASY.MATERIAL_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT EXALARI_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT EXALARI_LABUNME_FK 
 FOREIGN KEY (NR_SEQ_UNID_MED) 
 REFERENCES TASY.LAB_UNIDADE_MEDIDA (NR_SEQUENCIA),
  CONSTRAINT EXALARI_EXALAFO_FK2 
 FOREIGN KEY (NR_SEQ_FORMATO_RED) 
 REFERENCES TASY.EXAME_LAB_FORMAT (NR_SEQ_FORMATO),
  CONSTRAINT EXALARI_METEXLA_FK2 
 FOREIGN KEY (NR_SEQ_METODO_EXAME) 
 REFERENCES TASY.METODO_EXAME_LAB (NR_SEQUENCIA),
  CONSTRAINT EXALARI_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.EXAME_LAB_RESULT_ITEM TO NIVEL_1;

