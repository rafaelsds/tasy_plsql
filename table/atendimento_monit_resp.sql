ALTER TABLE TASY.ATENDIMENTO_MONIT_RESP
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ATENDIMENTO_MONIT_RESP CASCADE CONSTRAINTS;

CREATE TABLE TASY.ATENDIMENTO_MONIT_RESP
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  NR_ATENDIMENTO                NUMBER(10)      NOT NULL,
  DT_MONITORIZACAO              DATE            NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  IE_RESPIRACAO                 VARCHAR2(7 BYTE),
  CD_MOD_VENT                   VARCHAR2(15 BYTE),
  QT_FREQ_VENT                  NUMBER(15,4),
  QT_VCI                        NUMBER(15,4),
  QT_PIP                        NUMBER(15,4),
  QT_PEEP                       NUMBER(15,4),
  QT_PS                         NUMBER(15,4),
  QT_FIO2                       NUMBER(15,4),
  QT_CO2                        NUMBER(15,4),
  QT_VMIN                       NUMBER(15,4),
  QT_AUTO_PEEP                  NUMBER(15,4),
  QT_PPLATO                     NUMBER(15,4),
  QT_PVA                        NUMBER(15,4),
  QT_CST                        NUMBER(15,4),
  QT_RSR                        NUMBER(15,4),
  QT_FLUXO_INSP                 NUMBER(15,4),
  QT_REL_PAO2_FIO2              NUMBER(15,4),
  QT_GRAD_AAO2                  NUMBER(15,4),
  QT_PRESSAO_CUFF               NUMBER(15,3),
  DS_OBSERVACAO                 VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  QT_SATURACAO_O2               NUMBER(15,4),
  QT_SAT_VENOSA_O2              NUMBER(15,4),
  QT_TI_TE                      NUMBER(15,4),
  QT_TIP_TI                     NUMBER(15,4),
  QT_CAM                        NUMBER(15,4),
  QT_HALOG_INS                  NUMBER(15,4),
  QT_HALOG_EXP                  NUMBER(15,4),
  IE_HALOG_TIPO                 NUMBER(3),
  QT_OXIG_INS                   NUMBER(15,4),
  QT_OXIG_EXP                   NUMBER(15,4),
  QT_OXIDO_NITROSO_EXP          NUMBER(15,4),
  QT_OXIDO_NITROSO_INSP         NUMBER(15,4),
  IE_DISP_RESP_ESP              VARCHAR2(15 BYTE),
  NR_CIRURGIA                   NUMBER(10),
  QT_TI                         NUMBER(15,3),
  QT_TE                         NUMBER(15,3),
  QT_FLUXO_OXIGENIO             NUMBER(15,4),
  QT_VC_PROG                    NUMBER(15,4),
  QT_VCE                        NUMBER(15,4),
  QT_LIMITE_PA_MIN              NUMBER(15,4),
  QT_LIMITE_PA_MAX              NUMBER(15,4),
  QT_SENSIB_RESP                NUMBER(15,4),
  QT_TEMPO_INSP                 NUMBER(15,4),
  QT_FREQ_RESP                  NUMBER(15,4),
  QT_RIMA_LABIAL                NUMBER(15,4),
  NR_HORA                       NUMBER(2),
  IE_RELEVANTE_APAP             VARCHAR2(1 BYTE),
  IE_SITUACAO                   VARCHAR2(1 BYTE),
  DT_INATIVACAO                 DATE,
  DS_JUSTIFICATIVA              VARCHAR2(255 BYTE),
  NM_USUARIO_INATIVACAO         VARCHAR2(15 BYTE),
  DT_LIBERACAO                  DATE,
  DT_REFERENCIA                 DATE,
  IE_PADRAO_RESP                VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO          NUMBER(5),
  QT_PC                         NUMBER(10),
  QT_PRESSAO_CUFF_CMH2O         NUMBER(15,3),
  QT_FLUXO_AR_COMPR             NUMBER(15,4),
  QT_CAPAC_VITAL                NUMBER(15,4),
  QT_PEP                        NUMBER(15,4),
  QT_TOBIN                      NUMBER(15,2),
  NR_PROTESE                    NUMBER(10,1),
  IE_CANULA_TRAQUEO             VARCHAR2(15 BYTE),
  NR_SEQ_PEPO                   NUMBER(10),
  CD_PERFIL_ATIVO               NUMBER(5),
  CD_MANOBRA_VENT               NUMBER(10),
  NR_SEQ_ASSINATURA             NUMBER(10),
  NR_SEQ_SINAL_VITAL            NUMBER(10),
  QT_OXIDO_NITRICO              NUMBER(15,4),
  IE_MUSCUL_ACESS               NUMBER(3),
  NR_SEQ_INDICACAO              NUMBER(10),
  CD_SETOR_VENTILACAO           NUMBER(5),
  NR_SEQ_EQUIPAMENTO            NUMBER(10),
  PR_CICLAGEM_PS                NUMBER(3),
  NR_SEQ_ASSINAT_INATIVACAO     NUMBER(10),
  QT_CDYN                       NUMBER(15,4),
  IE_IMPORTADO                  VARCHAR2(1 BYTE),
  NR_SEQ_REG_ELEMENTO           NUMBER(10),
  IE_INTEGRACAO                 VARCHAR2(1 BYTE),
  QT_SENSIB_FLUXO               NUMBER(15,4),
  QT_OXIGENACAO_PAO2            NUMBER(6,2),
  IE_MUSCULO                    VARCHAR2(3 BYTE),
  IE_INTEGRACAO_EXTRA           VARCHAR2(1 BYTE),
  NR_SEQ_REG_REGISTRO           NUMBER(10),
  QT_FLUXO_OXIDO                NUMBER(10,4),
  QT_AMPLITUDE                  NUMBER(3),
  QT_FREQ_PROG                  NUMBER(2),
  DS_UTC                        VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO              VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO            VARCHAR2(50 BYTE),
  QT_ICO2                       NUMBER(15,4),
  IE_VALOR_FORA_FAIXA           VARCHAR2(3 BYTE),
  QT_TEMP_HUMIDIFIER            NUMBER(3,1),
  IE_AIR_ENTRY                  VARCHAR2(5 BYTE),
  NR_SEQ_ATEND_CONS_PEPA        NUMBER(10),
  NR_RESP_DISTRESS              NUMBER(1),
  QT_FR_CO2                     NUMBER(15,4),
  QT_VMIN_PARCIAL               NUMBER(15,4),
  IE_FILTRO_UMIDIFICADOR        VARCHAR2(10 BYTE),
  DT_DESPERTAR_ESPONTANEO       DATE,
  DT_RESPIRAR_ESPONTANEO        DATE,
  IE_APTO_DESPERTAR_ESPONTANEO  VARCHAR2(1 BYTE),
  IE_APTO_RESPIRAR_ESPONTANEO   VARCHAR2(1 BYTE),
  NR_SEQ_FORMULARIO             NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ATEMORE_ATCONSPEPA_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMORE_ATEPACI_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMORE_ATESIVI_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQ_SINAL_VITAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_ATESIVI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_CIRURGI_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMORE_EHRREEL_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_EHRREEL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_EHRREGI_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMORE_EQURESP_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQ_EQUIPAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_EQURESP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_I1 ON TASY.ATENDIMENTO_MONIT_RESP
(DT_MONITORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMORE_I2 ON TASY.ATENDIMENTO_MONIT_RESP
(NR_ATENDIMENTO, DT_MONITORIZACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMORE_I3 ON TASY.ATENDIMENTO_MONIT_RESP
(NM_USUARIO, DT_LIBERACAO, NR_SEQ_PEPO, NR_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
PARALLEL ( DEGREE DEFAULT INSTANCES DEFAULT );


CREATE INDEX TASY.ATEMORE_MANVENT_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(CD_MANOBRA_VENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_MANVENT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_MODVENT_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(CD_MOD_VENT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_MODVENT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_MOREIND_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQ_INDICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_MOREIND_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_PEPOCIR_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQ_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_PEPOCIR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_PERFIL_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_PESFISI_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          320K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ATEMORE_PK ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          192K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ATEMORE_SETATEN_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_TASASDI_FK_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ATEMORE_TASASDI_FK2_I ON TASY.ATENDIMENTO_MONIT_RESP
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ATEMORE_TASASDI_FK2_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_MONIT_RESP_ATUAL
BEFORE INSERT OR UPDATE ON TASY.ATENDIMENTO_MONIT_RESP FOR EACH ROW
DECLARE

ds_hora_w	varchar2(20);
dt_registro_w	date;
dt_apap_w	date;
qt_hora_w	number(15,2);
qt_reg_w	number(1);
qt_registro_atend_adic_w	number(10);

BEGIN

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
if	(:new.nr_hora is null) or
	(:new.dt_monitorizacao <> :old.dt_monitorizacao) then
	begin
	:new.nr_hora	:= to_number(to_char(round(:new.dt_monitorizacao,'hh24'),'hh24'));
	end;
end if;

if	(nvl(:old.DT_MONITORIZACAO,sysdate+10) <> :new.DT_MONITORIZACAO) and
	(:new.DT_MONITORIZACAO is not null) then
	:new.ds_utc				:= obter_data_utc(:new.DT_MONITORIZACAO, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

if	(:new.nr_hora is not null) and
	((:old.nr_hora is null) or
	 (:old.dt_monitorizacao is null) or
	 (:new.nr_hora <> :old.nr_hora) or
	 (:new.dt_monitorizacao <> :old.dt_monitorizacao)) then
	begin
	ds_hora_w	:= substr(obter_valor_dominio(2119,:new.nr_hora),1,2);
	dt_registro_w	:= trunc(:new.dt_monitorizacao,'hh24');
	dt_apap_w	:= to_date(to_char(:new.dt_monitorizacao,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss');
	if	(to_char(round(:new.dt_monitorizacao,'hh24'),'hh24') = ds_hora_w) then
		:new.dt_referencia	:= round(:new.dt_monitorizacao,'hh24');
	else
		begin
		qt_hora_w	:= (trunc(:new.dt_monitorizacao,'hh24') - to_date(to_char(:new.dt_monitorizacao,'dd/mm/yyyy') ||' '||ds_hora_w||':00:00','dd/mm/yyyy hh24:mi:ss')) * 24;
		if	(qt_hora_w > 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_monitorizacao + 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w > 0) and
			(qt_hora_w <= 12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_monitorizacao),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w >= -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_monitorizacao),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		elsif	(qt_hora_w < -12) then
			:new.dt_referencia	:= to_date(to_char(ESTABLISHMENT_TIMEZONE_UTILS.startOfDay(:new.dt_monitorizacao - 1),'dd/mm/yyyy') ||' '||ds_hora_w ||':00:00','dd/mm/yyyy hh24:mi:ss');
		end if;
		end;
	end if;
	end;
end if;

if (inserting) and (nvl(:new.ie_integracao,'N')	= 'S') then
   record_integration_notify(null,:new.nr_atendimento,'MR',:new.nr_sequencia,null,'S');
end if;

select count(*)
into qt_registro_atend_adic_w
from atend_paciente_adic
where nr_atendimento = :new.nr_atendimento;

if	(:new.dt_liberacao is not null
	and :new.ie_respiracao in ('VMIFB', 'VMI')
	and qt_registro_atend_adic_w > 0) then
	begin
		update atend_paciente_adic
		set ie_ventilacao = 'S'
		where nr_atendimento = :new.nr_atendimento;
	end;
elsif (:new.dt_liberacao is not null
	   and :new.ie_respiracao not in ('VMIFB', 'VMI')
	   and qt_registro_atend_adic_w > 0) then
	begin
		update atend_paciente_adic
		set ie_ventilacao = 'N'
		where nr_atendimento = :new.nr_atendimento;
	end;
elsif	(:new.dt_liberacao is not null
	and :new.ie_respiracao in ('VMIFB', 'VMI')
	and qt_registro_atend_adic_w = 0) then
	begin
		insert into atend_paciente_adic (
			nr_sequencia,
			dt_atualizacao,
			nr_atendimento,
			nm_usuario,
			ie_ventilacao
		) select atend_paciente_adic_seq.nextval,
			sysdate,
			:new.nr_atendimento,
			:new.nm_usuario,
			'S' from dual;
	end;
elsif (:new.dt_liberacao is not null
	   and :new.ie_respiracao not in ('VMIFB', 'VMI')
	   and qt_registro_atend_adic_w = 0) then
	begin
		insert into atend_paciente_adic (
			nr_sequencia,
			dt_atualizacao,
			nr_atendimento,
			nm_usuario,
			ie_ventilacao
		) select atend_paciente_adic_seq.nextval,
			sysdate,
			:new.nr_atendimento,
			:new.nm_usuario,
			'N' from dual;
	end;
end if;

<<Final>>
qt_reg_w	:= 0;

END;
/


CREATE OR REPLACE TRIGGER TASY.ATEND_MONIT_RESP_pend_atual 
after insert or update ON TASY.ATENDIMENTO_MONIT_RESP 
for each row
declare 
 
qt_reg_w		number(1); 
ie_tipo_w		varchar2(10); 
ie_lib_sinal_vital_w	varchar2(10); 
cd_pessoa_fisica_w		varchar2(30); 
 
begin 
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N') then 
	goto Final; 
end if; 
 
select	max(ie_lib_sinal_vital) 
into	ie_lib_sinal_vital_w 
from	parametro_medico 
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento; 
 
if	(nvl(ie_lib_sinal_vital_w,'N') = 'S') then 
	if	(:new.dt_liberacao is null) then 
		ie_tipo_w := 'SVMR'; 
	elsif	(:old.dt_liberacao is null) and 
			(:new.dt_liberacao is not null) then 
		ie_tipo_w := 'XSVMR'; 
	end if; 
	 
	select 	max(cd_pessoa_fisica) 
	into	cd_pessoa_fisica_w 
	from	atendimento_paciente 
	where 	nr_atendimento	=	:new.nr_atendimento; 
 
	begin 
	if	(ie_tipo_w	is not null) then 
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, :new.nr_atendimento, :new.nm_usuario); 
	end if; 
	exception 
		when others then 
		null; 
	end; 
	 
end if; 
 
<<Final>> 
qt_reg_w	:= 0; 
end;
/


CREATE OR REPLACE TRIGGER TASY.ATENDIMENTO_MONIT_RESP_UPD_BB
AFTER UPDATE ON TASY.ATENDIMENTO_MONIT_RESP FOR EACH ROW
DECLARE

/* Integracao com o sistema blackboard */
json_aux_bb philips_json;
envio_integracao_bb clob;
retorno_integracao_bb clob;

bb_valueID VARCHAR2(32);

BEGIN

if (wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

  IF	(:old.DT_LIBERACAO is NULL) AND
      (:new.DT_LIBERACAO is not NULL) AND
      ((NVL(:new.IE_INTEGRACAO, 'N') <> 'S')) THEN

      IF (:new.IE_RESPIRACAO = 'ESPONT') THEN
          bb_valueID := '41289726de804449a70252cbc820c3c7';

      ELSIF (:new.IE_RESPIRACAO = 'VMNI') OR
            (:new.IE_RESPIRACAO = 'BORB') THEN
          bb_valueID := 'f485c826290e48a6a4c86be64c9f4d47';

      ELSIF (:new.IE_RESPIRACAO = 'VMI') OR
            (:new.IE_RESPIRACAO = 'VMIFB') THEN
          bb_valueID := 'e890895cccd84d569462da49c0cdca35';

      ELSE
          bb_valueID := 'e213c60fd7d74093955adaa6a657ae16';

      END IF;

      json_aux_bb := philips_json();
      json_aux_bb.put('typeID', 'CAREPLAN');
      json_aux_bb.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'MM-DD-YYYY HH24:MI:SS.SSSSS'));
      json_aux_bb.put('patientHealthSystemStayID', LPAD(COALESCE(TO_CHAR(:new.NR_ATENDIMENTO), ''), 32, '0'));
      json_aux_bb.put('historyID', LPAD('F' || :new.NR_SEQUENCIA, 32, '0'));
      json_aux_bb.put('saveDateTimeUtc', TO_CHAR(f_extract_utc_bb(:new.DT_MONITORIZACAO), 'MM-DD-YYYY HH:MI:SS'));
      json_aux_bb.put('saveDateTimeUtcOffset', '0');
      json_aux_bb.put('groupTypeID', '1f82689907194030876a9f393eade2d9');
      json_aux_bb.put('groupID', LPAD(COALESCE(TO_CHAR(:new.NR_SEQUENCIA), ''), 32, '0'));
      json_aux_bb.put('valueID', TO_CHAR(bb_valueID));

      dbms_lob.createtemporary(envio_integracao_bb, TRUE);
      json_aux_bb.to_clob(envio_integracao_bb);

      SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Patient_Ventilation',envio_integracao_bb, wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;

      IF ((:new.QT_FIO2 is not NULL) AND (:new.QT_FIO2 > 0)) OR
        ((:new.QT_PEEP is not NULL) AND (:new.QT_PEEP > 0)) OR
        ((:new.QT_SATURACAO_O2 is not NULL) AND (:new.QT_SATURACAO_O2 > 0)) OR
        ((:new.QT_FLUXO_OXIGENIO is not NULL) AND (:new.QT_FLUXO_OXIGENIO > 0)) OR
        ((:new.QT_FREQ_RESP is not NULL) AND (:new.QT_FREQ_RESP > 0)) OR
        ((:new.QT_VC_PROG is not NULL) AND (:new.QT_VC_PROG > 0)) THEN

        INTEGRAR_SINAIS_VITAIS_RESP_BB(:new.NR_SEQUENCIA,
            :new.NR_ATENDIMENTO,
            :new.DT_MONITORIZACAO,
            :new.QT_FIO2,
            :new.QT_PEEP,
            :new.QT_SATURACAO_O2,
            :new.QT_FLUXO_OXIGENIO,
            :new.QT_FREQ_RESP,
            :new.QT_VC_PROG);

      END IF;

  END IF;

  IF	(:old.dt_inativacao is NULL) AND
    (:new.dt_inativacao is not NULL) AND
      ((:new.QT_FREQ_RESP is not NULL) AND (:new.QT_FREQ_RESP > 0)) THEN
      json_aux_bb := philips_json();
      json_aux_bb.put('typeID', 'CAREPLAN');
      json_aux_bb.put('messageDateTime', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'DD/MM/YYYY HH24:MI:SS'));
      json_aux_bb.put('patientHealthSystemStayID', LPAD(TO_CHAR(:new.NR_ATENDIMENTO), 32, '0'));
      json_aux_bb.put('historyID', LPAD('F' || TO_CHAR(:new.NR_SEQUENCIA), 32, '0'));
      json_aux_bb.put('saveDateTimeUtc', TO_CHAR(sys_extract_utc(SYSTIMESTAMP), 'YYYY-MM-DD"T"HH24:MI:SS'));
      json_aux_bb.put('saveDateTimeUtcOffset', '0');
      json_aux_bb.put('clearForReadmit', '');

      dbms_lob.createtemporary(envio_integracao_bb, TRUE);
      json_aux_bb.to_clob(envio_integracao_bb);

      SELECT BIFROST.SEND_INTEGRATION_CONTENT('Blackboard_Cancel_Ventilation_Status',envio_integracao_bb, wheb_usuario_pck.get_nm_usuario) into retorno_integracao_bb FROM DUAL;
  END IF;

  IF (:old.dt_inativacao is NULL) AND
      (:new.dt_inativacao is not NULL) AND
      (((:new.QT_FIO2 is not NULL) AND (:new.QT_FIO2 > 0)) OR
      ((:new.QT_PEEP is not NULL) AND (:new.QT_PEEP > 0)) OR
      ((:new.QT_SATURACAO_O2 is not NULL) AND (:new.QT_SATURACAO_O2 > 0)) OR
      ((:new.QT_FLUXO_OXIGENIO is not NULL) AND (:new.QT_FLUXO_OXIGENIO > 0)) OR
      ((:new.QT_FREQ_RESP is not NULL) AND (:new.QT_FREQ_RESP > 0)) OR
      ((:new.QT_VC_PROG is not NULL) AND (:new.QT_VC_PROG > 0))) THEN
      p_cancelar_flowsheet(:new.nr_sequencia, :new.nr_atendimento, 'E');
  END IF;

end if;

END;
/


ALTER TABLE TASY.ATENDIMENTO_MONIT_RESP ADD (
  CONSTRAINT ATEMORE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          192K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ATENDIMENTO_MONIT_RESP ADD (
  CONSTRAINT ATEMORE_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEMORE_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ATEMORE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ATEMORE_MODVENT_FK 
 FOREIGN KEY (CD_MOD_VENT) 
 REFERENCES TASY.MODALIDADE_VENTILATORIA (CD_MODALIDADE),
  CONSTRAINT ATEMORE_CIRURGI_FK 
 FOREIGN KEY (NR_CIRURGIA) 
 REFERENCES TASY.CIRURGIA (NR_CIRURGIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEMORE_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT ATEMORE_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEMORE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ATEMORE_PEPOCIR_FK 
 FOREIGN KEY (NR_SEQ_PEPO) 
 REFERENCES TASY.PEPO_CIRURGIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ATEMORE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT ATEMORE_MANVENT_FK 
 FOREIGN KEY (CD_MANOBRA_VENT) 
 REFERENCES TASY.MANOBRA_VENTILATORIA (NR_SEQUENCIA),
  CONSTRAINT ATEMORE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ATEMORE_ATESIVI_FK 
 FOREIGN KEY (NR_SEQ_SINAL_VITAL) 
 REFERENCES TASY.ATENDIMENTO_SINAL_VITAL (NR_SEQUENCIA),
  CONSTRAINT ATEMORE_MOREIND_FK 
 FOREIGN KEY (NR_SEQ_INDICACAO) 
 REFERENCES TASY.MONIT_RESP_INDICACAO (NR_SEQUENCIA),
  CONSTRAINT ATEMORE_EQURESP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPAMENTO) 
 REFERENCES TASY.EQUIPAMENTO_RESP (NR_SEQUENCIA),
  CONSTRAINT ATEMORE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ATENDIMENTO_MONIT_RESP TO NIVEL_1;

