ALTER TABLE TASY.NUT_NUTRIENTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUT_NUTRIENTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUT_NUTRIENTE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DS_NUTRIENTE            VARCHAR2(40 BYTE)     NOT NULL,
  DS_SIGLA                VARCHAR2(10 BYTE)     NOT NULL,
  CD_UNIDADE_MEDIDA       VARCHAR2(5 BYTE)      NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_APRESENTACAO     NUMBER(10),
  IE_CALORIA              VARCHAR2(1 BYTE),
  QT_FATOR_CONVERSAO_CAL  NUMBER(15)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.NUTNUTR_PK ON TASY.NUT_NUTRIENTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NUT_NUTRIENTE ADD (
  CONSTRAINT NUTNUTR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.NUT_NUTRIENTE TO NIVEL_1;

