ALTER TABLE TASY.PERFIL_ITEM_PEPO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERFIL_ITEM_PEPO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERFIL_ITEM_PEPO
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_PERFIL              NUMBER(5)              NOT NULL,
  NR_SEQ_ITEM_PEPO       NUMBER(10)             NOT NULL,
  IE_ATUALIZAR           VARCHAR2(1 BYTE)       NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_SEQ_APRESENTACAO    NUMBER(5),
  IE_TIPO_ITEM           VARCHAR2(15 BYTE),
  IE_SOMENTE_TEMPLATE    VARCHAR2(1 BYTE),
  DS_ITEM_INSTITUICAO    VARCHAR2(40 BYTE),
  DS_PASTA_TEMPLATE      VARCHAR2(40 BYTE),
  IE_INATIVAR            VARCHAR2(1 BYTE),
  NR_SEQ_PASTA_INICIAL   NUMBER(10),
  IE_VISUALIZA_INATIVO   VARCHAR2(1 BYTE),
  IE_SENHA_LIBERACAO     VARCHAR2(1 BYTE),
  IE_FORMA_LIBERAR       VARCHAR2(1 BYTE),
  IE_IMPRIMIR_LIBERAR    VARCHAR2(1 BYTE),
  IE_QUEST_TEXTO_PADRAO  VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PERITEP_PEPITEM_FK_I ON TASY.PERFIL_ITEM_PEPO
(NR_SEQ_ITEM_PEPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERITEP_PERFIL_FK_I ON TASY.PERFIL_ITEM_PEPO
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PERITEP_PK ON TASY.PERFIL_ITEM_PEPO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PERITEP_PROPAST_FK_I ON TASY.PERFIL_ITEM_PEPO
(NR_SEQ_PASTA_INICIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PERFIL_ITEM_PEPO_tp  after update ON TASY.PERFIL_ITEM_PEPO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMPRIMIR_LIBERAR,1,4000),substr(:new.IE_IMPRIMIR_LIBERAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMPRIMIR_LIBERAR',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO_NREC,1,4000),substr(:new.DT_ATUALIZACAO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_ITEM_PEPO,1,4000),substr(:new.NR_SEQ_ITEM_PEPO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_ITEM_PEPO',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ATUALIZAR,1,4000),substr(:new.IE_ATUALIZAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_ATUALIZAR',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_APRESENTACAO,1,4000),substr(:new.NR_SEQ_APRESENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_APRESENTACAO',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_QUEST_TEXTO_PADRAO,1,4000),substr(:new.IE_QUEST_TEXTO_PADRAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_QUEST_TEXTO_PADRAO',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_ITEM,1,4000),substr(:new.IE_TIPO_ITEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_ITEM',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_TEMPLATE,1,4000),substr(:new.IE_SOMENTE_TEMPLATE,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_TEMPLATE',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ITEM_INSTITUICAO,1,4000),substr(:new.DS_ITEM_INSTITUICAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ITEM_INSTITUICAO',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PASTA_TEMPLATE,1,4000),substr(:new.DS_PASTA_TEMPLATE,1,4000),:new.nm_usuario,nr_seq_w,'DS_PASTA_TEMPLATE',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_INATIVAR,1,4000),substr(:new.IE_INATIVAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_INATIVAR',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PASTA_INICIAL,1,4000),substr(:new.NR_SEQ_PASTA_INICIAL,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PASTA_INICIAL',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SENHA_LIBERACAO,1,4000),substr(:new.IE_SENHA_LIBERACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SENHA_LIBERACAO',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VISUALIZA_INATIVO,1,4000),substr(:new.IE_VISUALIZA_INATIVO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VISUALIZA_INATIVO',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_FORMA_LIBERAR,1,4000),substr(:new.IE_FORMA_LIBERAR,1,4000),:new.nm_usuario,nr_seq_w,'IE_FORMA_LIBERAR',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DT_ATUALIZACAO,1,4000),substr(:new.DT_ATUALIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PERFIL_ITEM_PEPO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PERFIL_ITEM_PEPO ADD (
  CONSTRAINT PERITEP_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERFIL_ITEM_PEPO ADD (
  CONSTRAINT PERITEP_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT PERITEP_PEPITEM_FK 
 FOREIGN KEY (NR_SEQ_ITEM_PEPO) 
 REFERENCES TASY.PEPO_ITEM (NR_SEQUENCIA),
  CONSTRAINT PERITEP_PROPAST_FK 
 FOREIGN KEY (NR_SEQ_PASTA_INICIAL) 
 REFERENCES TASY.PRONTUARIO_PASTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PERFIL_ITEM_PEPO TO NIVEL_1;

