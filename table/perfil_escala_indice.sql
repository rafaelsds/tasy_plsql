ALTER TABLE TASY.PERFIL_ESCALA_INDICE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PERFIL_ESCALA_INDICE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PERFIL_ESCALA_INDICE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_PERFIL            NUMBER(15)               NOT NULL,
  IE_ATUALIZAR         VARCHAR2(1 BYTE)         NOT NULL,
  NR_SEQ_GRUPO         NUMBER(10),
  NR_SEQ_APRES         NUMBER(3)                NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PERESIN_GRUESIN_FK_I ON TASY.PERFIL_ESCALA_INDICE
(NR_SEQ_GRUPO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PERESIN_GRUESIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PERESIN_PERFIL_FK_I ON TASY.PERFIL_ESCALA_INDICE
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PERESIN_PK ON TASY.PERFIL_ESCALA_INDICE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PERFIL_ESCALA_INDICE ADD (
  CONSTRAINT PERESIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PERFIL_ESCALA_INDICE ADD (
  CONSTRAINT PERESIN_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT PERESIN_GRUESIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO) 
 REFERENCES TASY.GRUPO_ESCALA_INDICE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PERFIL_ESCALA_INDICE TO NIVEL_1;

