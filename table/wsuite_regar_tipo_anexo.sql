ALTER TABLE TASY.WSUITE_REGAR_TIPO_ANEXO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.WSUITE_REGAR_TIPO_ANEXO CASCADE CONSTRAINTS;

CREATE TABLE TASY.WSUITE_REGAR_TIPO_ANEXO
(
  NR_SEQUENCIA  NUMBER(10)                      NOT NULL,
  NM_DOCUMENTO  VARCHAR2(40 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.WSUIRTA_PK ON TASY.WSUITE_REGAR_TIPO_ANEXO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.WSUITE_REGAR_TIPO_ANEXO ADD (
  CONSTRAINT WSUIRTA_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.WSUITE_REGAR_TIPO_ANEXO TO NIVEL_1;

