ALTER TABLE TASY.MEDIC_DIAGNOSTICO_DOENCA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MEDIC_DIAGNOSTICO_DOENCA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MEDIC_DIAGNOSTICO_DOENCA
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  CD_ESTABELECIMENTO     NUMBER(4)              NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  IE_SITUACAO            VARCHAR2(1 BYTE)       NOT NULL,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  NR_SEQ_CLASSIFICACAO   NUMBER(10)             NOT NULL,
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  CD_DOENCA              VARCHAR2(10 BYTE)      NOT NULL,
  DT_DIAGNOSTICO         DATE                   NOT NULL,
  IE_TIPO_AFEICAO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDIAD_ATEPACI_FK_I ON TASY.MEDIC_DIAGNOSTICO_DOENCA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDIAD_CIDDOEN_FK_I ON TASY.MEDIC_DIAGNOSTICO_DOENCA
(CD_DOENCA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDIAD_CLADIAG_FK_I ON TASY.MEDIC_DIAGNOSTICO_DOENCA
(NR_SEQ_CLASSIFICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDIAD_ESTABEL_FK_I ON TASY.MEDIC_DIAGNOSTICO_DOENCA
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDIAD_PESFISI_FK_I ON TASY.MEDIC_DIAGNOSTICO_DOENCA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDIAD_PK ON TASY.MEDIC_DIAGNOSTICO_DOENCA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.medic_diag_doenca_ish_atual
after insert or update or delete ON TASY.MEDIC_DIAGNOSTICO_DOENCA for each row
declare

reg_integracao_p	gerar_int_padrao.reg_integracao;
nr_sequencia_w	varchar2(35);
qt_eventos_w	number(10);
nr_seq_interno_w	diagnostico_doenca.nr_seq_interno%type;
dt_liberacao_w	diagnostico_doenca.dt_liberacao%type;

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then
	select	count(1)
	into	qt_eventos_w
	from	intpd_eventos
	where	ie_evento	= '82'
	and	ie_situacao	= 'A';

	select	max(nr_seq_interno),
		max(dt_liberacao)
	into	nr_seq_interno_w,
		dt_liberacao_w
	from	diagnostico_doenca
	where	nr_atendimento = nvl(:new.nr_atendimento,:old.nr_atendimento)
	and	cd_doenca = nvl(:new.cd_doenca, :old.cd_doenca)
	and	dt_diagnostico = nvl(:new.dt_diagnostico, :old.dt_diagnostico);

	if	(qt_eventos_w > 0) and
		(nr_seq_interno_w > 0) and
		(dt_liberacao_w is not null) then
		begin
		reg_integracao_p.ie_status := 'AB';

		reg_integracao_p.ie_operacao	:=	'A';
		reg_integracao_p.ie_status		:=	'P';
		reg_integracao_p.nr_atendimento	:=	nvl(:new.nr_atendimento,:old.nr_atendimento);

		nr_sequencia_w := nvl(:new.nr_atendimento,:old.nr_atendimento)||'�' || nr_seq_interno_w || '�'||nvl(:new.cd_doenca, :old.cd_doenca);
		gerar_int_padrao.gravar_integracao('82', nr_sequencia_w, nvl(:new.nm_usuario, :old.nm_usuario), reg_integracao_p);
		end;
	end if;
end if;

commit;

end medic_diag_doenca_ish_atual;
/


ALTER TABLE TASY.MEDIC_DIAGNOSTICO_DOENCA ADD (
  CONSTRAINT MEDIAD_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MEDIC_DIAGNOSTICO_DOENCA ADD (
  CONSTRAINT MEDIAD_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT MEDIAD_CIDDOEN_FK 
 FOREIGN KEY (CD_DOENCA) 
 REFERENCES TASY.CID_DOENCA (CD_DOENCA_CID),
  CONSTRAINT MEDIAD_CLADIAG_FK 
 FOREIGN KEY (NR_SEQ_CLASSIFICACAO) 
 REFERENCES TASY.CLASSIFICACAO_DIAGNOSTICO (NR_SEQUENCIA),
  CONSTRAINT MEDIAD_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT MEDIAD_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.MEDIC_DIAGNOSTICO_DOENCA TO NIVEL_1;

