ALTER TABLE TASY.SISTEMA_CORPO_HUMANO_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SISTEMA_CORPO_HUMANO_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.SISTEMA_CORPO_HUMANO_ITEM
(
  NR_SEQUENCIA                 NUMBER(10)       NOT NULL,
  NR_SEQ_EXAME                 NUMBER(10),
  NR_SEQ_SINAL                 NUMBER(10),
  NR_SEQ_ESCALA_DOC            NUMBER(10),
  NR_SEQ_SISTEMA_CORPO_HUMANO  NUMBER(10),
  DT_ATUALIZACAO               DATE             NOT NULL,
  NM_USUARIO                   VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC          DATE,
  NM_USUARIO_NREC              VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SISCOHUMIT_ESCDOCU_FK_I ON TASY.SISTEMA_CORPO_HUMANO_ITEM
(NR_SEQ_ESCALA_DOC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SISCOHUMIT_EXALABO_FK_I ON TASY.SISTEMA_CORPO_HUMANO_ITEM
(NR_SEQ_EXAME)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.SISCOHUMIT_PK ON TASY.SISTEMA_CORPO_HUMANO_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SISCOHUMIT_SINVITA_FK_I ON TASY.SISTEMA_CORPO_HUMANO_ITEM
(NR_SEQ_SINAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SISCOHUMIT_SISCORHUM_FK_I ON TASY.SISTEMA_CORPO_HUMANO_ITEM
(NR_SEQ_SISTEMA_CORPO_HUMANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.SISTEMA_CORPO_HUMANO_ITEM ADD (
  CONSTRAINT SISCOHUMIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.SISTEMA_CORPO_HUMANO_ITEM ADD (
  CONSTRAINT SISCOHUMIT_ESCDOCU_FK 
 FOREIGN KEY (NR_SEQ_ESCALA_DOC) 
 REFERENCES TASY.ESCALA_DOCUMENTACAO (NR_SEQUENCIA),
  CONSTRAINT SISCOHUMIT_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT SISCOHUMIT_SINVITA_FK 
 FOREIGN KEY (NR_SEQ_SINAL) 
 REFERENCES TASY.SINAL_VITAL (NR_SEQUENCIA),
  CONSTRAINT SISCOHUMIT_SISCORHUM_FK 
 FOREIGN KEY (NR_SEQ_SISTEMA_CORPO_HUMANO) 
 REFERENCES TASY.SISTEMA_CORPO_HUMANO (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.SISTEMA_CORPO_HUMANO_ITEM TO NIVEL_1;

