ALTER TABLE TASY.PF_CODIGO_EXTERNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PF_CODIGO_EXTERNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PF_CODIGO_EXTERNO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_TIPO_CODIGO_EXTERNO     VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA           VARCHAR2(10 BYTE)  NOT NULL,
  CD_PESSOA_FISICA_EXTERNO   VARCHAR2(20 BYTE)  NOT NULL,
  CD_USUARIO_CONVENIO        VARCHAR2(255 BYTE),
  CD_COMPLEMENTO             VARCHAR2(30 BYTE),
  CD_LOGIN_EXT               VARCHAR2(20 BYTE),
  DS_SENHA_EXT               VARCHAR2(20 BYTE),
  HEALTH_CARD_EXT            VARCHAR2(60 BYTE),
  IE_ENCOUNTERID             VARCHAR2(1 BYTE),
  CD_PESSOA_ALT_EXT          VARCHAR2(15 BYTE),
  TEMP_ENCOUNTERID_EXT       VARCHAR2(20 BYTE),
  PREADMIT_NUMBER_EXT        VARCHAR2(15 BYTE),
  ACCOUNT_NUMBER_EXT         VARCHAR2(60 BYTE),
  VISIT_NUMBER_EXT           VARCHAR2(15 BYTE),
  CD_PESSOA_IDENTIFIER_EXT   VARCHAR2(15 BYTE),
  MEDICARE_NUMBER_EXT        VARCHAR2(60 BYTE),
  MEDICAID_NUMBER_EXT        VARCHAR2(15 BYTE),
  ALTERNATE_PATIENT_NUM_EXT  VARCHAR2(60 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PFCOEXT_PESFISI_FK_I ON TASY.PF_CODIGO_EXTERNO
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PFCOEXT_PK ON TASY.PF_CODIGO_EXTERNO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PF_CODIGO_EXTERNO_tp  after update ON TASY.PF_CODIGO_EXTERNO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PF_CODIGO_EXTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA,1,4000),substr(:new.CD_PESSOA_FISICA,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA',ie_log_w,ds_w,'PF_CODIGO_EXTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_COMPLEMENTO,1,4000),substr(:new.CD_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_COMPLEMENTO',ie_log_w,ds_w,'PF_CODIGO_EXTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PESSOA_FISICA_EXTERNO,1,4000),substr(:new.CD_PESSOA_FISICA_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PESSOA_FISICA_EXTERNO',ie_log_w,ds_w,'PF_CODIGO_EXTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_USUARIO_CONVENIO,1,4000),substr(:new.CD_USUARIO_CONVENIO,1,4000),:new.nm_usuario,nr_seq_w,'CD_USUARIO_CONVENIO',ie_log_w,ds_w,'PF_CODIGO_EXTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_CODIGO_EXTERNO,1,4000),substr(:new.IE_TIPO_CODIGO_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_CODIGO_EXTERNO',ie_log_w,ds_w,'PF_CODIGO_EXTERNO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.W_VERIFICA_PF_CODIGO_EXTERNO
BEFORE INSERT OR UPDATE ON TASY.PF_CODIGO_EXTERNO FOR EACH ROW
DECLARE

qt_w		number(10) := 0;
pragma autonomous_transaction;

begin

if 	(:new.CD_PESSOA_FISICA_EXTERNO is not null) then

		if	(pkg_i18n.get_user_locale is not null and pkg_i18n.get_user_locale = 'de_DE') then
			select 	count(1)
			into	qt_w
			from 	pf_codigo_externo
			where 	cd_pessoa_fisica_externo = :new.CD_PESSOA_FISICA_EXTERNO
			and     (:new.IE_TIPO_CODIGO_EXTERNO is null or ie_tipo_codigo_externo   = :new.IE_TIPO_CODIGO_EXTERNO);
		end if;

		if	(qt_w > 0) then
			Wheb_mensagem_pck.exibir_mensagem_abort(1026386);
		end if;
end if;
end;
/


ALTER TABLE TASY.PF_CODIGO_EXTERNO ADD (
  CONSTRAINT PFCOEXT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PF_CODIGO_EXTERNO ADD (
  CONSTRAINT PFCOEXT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.PF_CODIGO_EXTERNO TO NIVEL_1;

