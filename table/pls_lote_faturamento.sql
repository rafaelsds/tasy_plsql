ALTER TABLE TASY.PLS_LOTE_FATURAMENTO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_LOTE_FATURAMENTO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_LOTE_FATURAMENTO
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  CD_ESTABELECIMENTO         NUMBER(4)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  DT_MESANO_REFERENCIA       DATE               NOT NULL,
  DT_INICIO_REFERENCIA       DATE,
  DT_FIM_REFERENCIA          DATE,
  NR_SEQ_REGRA_FAT           NUMBER(10)         NOT NULL,
  DT_GERACAO                 DATE,
  DT_FECHAMENTO              DATE,
  DT_GERACAO_TITULOS         DATE,
  DT_EMISSAO                 DATE,
  DT_VENCIMENTO              DATE,
  DT_INICIAL_FECHAMENTO      DATE,
  DT_FINAL_FECHAMENTO        DATE,
  DT_PREV_ENVIO              DATE,
  DT_GERACAO_NF              DATE,
  NR_SEQ_LOTE_DISC           NUMBER(10),
  DT_MESANO_COMP_PROD        DATE,
  NR_SEQ_LOTE_ORIGEM         NUMBER(10),
  NR_SEQ_PROTOCOLO           NUMBER(10),
  DS_OBSERVACAO              VARCHAR2(4000 BYTE),
  IE_APRESENTACAO_PROT       VARCHAR2(1 BYTE),
  IE_APLICAR_ARREDONDAMENTO  VARCHAR2(1 BYTE),
  IE_STATUS                  VARCHAR2(3 BYTE),
  NM_ID_SID                  VARCHAR2(50 BYTE),
  DT_INICIO_GER_LOTE         DATE,
  NM_USUARIO_GER_LOTE        VARCHAR2(255 BYTE),
  NM_ID_SERIAL               VARCHAR2(50 BYTE),
  DT_FIM_GER_LOTE            DATE,
  IE_TIPO_LOTE               VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSLOFA_ESTABEL_FK_I ON TASY.PLS_LOTE_FATURAMENTO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFA_ESTABEL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PLSLOFA_PK ON TASY.PLS_LOTE_FATURAMENTO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOFA_PLSLODI_FK_I ON TASY.PLS_LOTE_FATURAMENTO
(NR_SEQ_LOTE_DISC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFA_PLSLODI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOFA_PLSLOFA_FK_I ON TASY.PLS_LOTE_FATURAMENTO
(NR_SEQ_LOTE_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFA_PLSLOFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PLSLOFA_PLSPRCO_FK_I ON TASY.PLS_LOTE_FATURAMENTO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSLOFA_PLSRFAT_FK_I ON TASY.PLS_LOTE_FATURAMENTO
(NR_SEQ_REGRA_FAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSLOFA_PLSRFAT_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.PLS_LOTE_FATURAMENTO_tp  after update ON TASY.PLS_LOTE_FATURAMENTO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_APRESENTACAO_PROT,1,4000),substr(:new.IE_APRESENTACAO_PROT,1,4000),:new.nm_usuario,nr_seq_w,'IE_APRESENTACAO_PROT',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOTE_ORIGEM,1,4000),substr(:new.NR_SEQ_LOTE_ORIGEM,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOTE_ORIGEM',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROTOCOLO,1,4000),substr(:new.NR_SEQ_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROTOCOLO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APLICAR_ARREDONDAMENTO,1,4000),substr(:new.IE_APLICAR_ARREDONDAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_APLICAR_ARREDONDAMENTO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_PREV_ENVIO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_PREV_ENVIO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_PREV_ENVIO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIAL_FECHAMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIAL_FECHAMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIAL_FECHAMENTO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FINAL_FECHAMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FINAL_FECHAMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FINAL_FECHAMENTO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQUENCIA,1,4000),substr(:new.NR_SEQUENCIA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQUENCIA',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_ATUALIZACAO_NREC,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_ATUALIZACAO_NREC',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO_NREC,1,4000),substr(:new.NM_USUARIO_NREC,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_NREC',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_MESANO_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_MESANO_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_MESANO_REFERENCIA',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_INICIO_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_INICIO_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_INICIO_REFERENCIA',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FIM_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FIM_REFERENCIA,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FIM_REFERENCIA',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_FAT,1,4000),substr(:new.NR_SEQ_REGRA_FAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_FAT',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_GERACAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_GERACAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_GERACAO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_FECHAMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_FECHAMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_FECHAMENTO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_GERACAO_TITULOS,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_GERACAO_TITULOS,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_GERACAO_TITULOS',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_EMISSAO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_EMISSAO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_EMISSAO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_VENCIMENTO,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_VENCIMENTO,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_VENCIMENTO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_LOTE_DISC,1,4000),substr(:new.NR_SEQ_LOTE_DISC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_LOTE_DISC',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_GERACAO_NF,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_GERACAO_NF,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_GERACAO_NF',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(to_char(:old.DT_MESANO_COMP_PROD,'dd/mm/yyyy hh24:mi:ss'),to_char(:new.DT_MESANO_COMP_PROD,'dd/mm/yyyy hh24:mi:ss'),:new.nm_usuario,nr_seq_w,'DT_MESANO_COMP_PROD',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PLS_LOTE_FATURAMENTO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_LOTE_FATURAMENTO ADD (
  CONSTRAINT PLSLOFA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_LOTE_FATURAMENTO ADD (
  CONSTRAINT PLSLOFA_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT PLSLOFA_PLSRFAT_FK 
 FOREIGN KEY (NR_SEQ_REGRA_FAT) 
 REFERENCES TASY.PLS_REGRA_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSLOFA_PLSLODI_FK 
 FOREIGN KEY (NR_SEQ_LOTE_DISC) 
 REFERENCES TASY.PLS_LOTE_DISCUSSAO (NR_SEQUENCIA),
  CONSTRAINT PLSLOFA_PLSLOFA_FK 
 FOREIGN KEY (NR_SEQ_LOTE_ORIGEM) 
 REFERENCES TASY.PLS_LOTE_FATURAMENTO (NR_SEQUENCIA),
  CONSTRAINT PLSLOFA_PLSPRCO_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PLS_PROTOCOLO_CONTA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_LOTE_FATURAMENTO TO NIVEL_1;

