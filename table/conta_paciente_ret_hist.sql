ALTER TABLE TASY.CONTA_PACIENTE_RET_HIST
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CONTA_PACIENTE_RET_HIST CASCADE CONSTRAINTS;

CREATE TABLE TASY.CONTA_PACIENTE_RET_HIST
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_HISTORICO         DATE                     NOT NULL,
  VL_HISTORICO         NUMBER(15,2)             NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_CONPACI_RET   NUMBER(10)               NOT NULL,
  NR_SEQ_HIST_AUDIT    NUMBER(10)               NOT NULL,
  NR_SEQ_RET_ITEM      NUMBER(10),
  DS_OBSERVACAO        VARCHAR2(2000 BYTE),
  NR_SEQ_LOTE_AUDIT    NUMBER(10),
  DT_REAPRESENTACAO    DATE,
  DT_RECEB_PREV        DATE,
  CD_MOTIVO_GLOSA      NUMBER(5),
  CD_PROCESSO_RECEB    VARCHAR2(20 BYTE),
  NR_SEQ_LOTE_RECURSO  NUMBER(10),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_RESPOSTA          NUMBER(10),
  NM_USUARIO_RESP      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COPAREHI_CONREIT_FK_I ON TASY.CONTA_PACIENTE_RET_HIST
(NR_SEQ_RET_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPAREHI_COPARET_FK_I ON TASY.CONTA_PACIENTE_RET_HIST
(NR_SEQ_CONPACI_RET)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPAREHI_HIAUCOPA_FK_I ON TASY.CONTA_PACIENTE_RET_HIST
(NR_SEQ_HIST_AUDIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPAREHI_HIAUCOPA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPAREHI_LOAUREC_FK_I ON TASY.CONTA_PACIENTE_RET_HIST
(NR_SEQ_LOTE_RECURSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPAREHI_LOAUTIRE_FK_I ON TASY.CONTA_PACIENTE_RET_HIST
(NR_SEQ_LOTE_AUDIT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPAREHI_MOGLOSA_FK_I ON TASY.CONTA_PACIENTE_RET_HIST
(CD_MOTIVO_GLOSA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPAREHI_MOGLOSA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.COPAREHI_MOREGLO_FK_I ON TASY.CONTA_PACIENTE_RET_HIST
(CD_RESPOSTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.COPAREHI_MOREGLO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.COPAREHI_PK ON TASY.CONTA_PACIENTE_RET_HIST
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.ConPaci_Ret_Hist_BEFDelete
BEFORE DELETE ON TASY.CONTA_PACIENTE_RET_HIST FOR EACH ROW
declare

begin

update	titulo_receber_liq
set	nr_seq_conpaci_ret_hist	= null,
	ds_observacao		= ds_observacao || obter_desc_expressao(622524)/*' Desvinculado do lote audit.: '*/ || :old.nr_seq_lote_audit || obter_desc_expressao(622525)/*'. Lote recurso: '*/ || :old.nr_seq_lote_recurso || '. Hist.: ' || :old.nr_sequencia
where	nr_seq_conpaci_ret_hist	= :old.nr_sequencia;

end;
/


CREATE OR REPLACE TRIGGER TASY.Conta_Paciente_Ret_Hist_Delete
AFTER DELETE ON TASY.CONTA_PACIENTE_RET_HIST FOR EACH ROW
declare

dt_baixa_w			date;

ie_acao_w		number(1);
vl_historico_w		number(15,2);

vl_pendente_w		number(15,2);
vl_reapresentacao_w	number(15,2);
vl_glosa_devida_w	number(15,2);
vl_glosa_indevida_w	number(15,2);
vl_nao_auditado_w	number(15,2);
vl_recebido_w		number(15,2);
vl_inicial_w		number(15,2);
vl_perdas_w		number(15,2);

begin

if	(:old.nr_seq_lote_audit is not null) then
	select	dt_baixa
	into	dt_baixa_w
	from	lote_audit_tit_rec
	where	nr_sequencia = :old.nr_seq_lote_audit;

	if	(dt_baixa_w is not null) then
		--r.aise_application_error(-20011,' Hist�rico j� est� vinculado a um lote. N�o pode ser exclu�do !');
		wheb_mensagem_pck.exibir_mensagem_abort(263288);
	end if;
end if;

select	ie_acao
into	ie_acao_w
from	hist_audit_conta_paciente
where	nr_sequencia = :old.nr_seq_hist_audit;

select 	vl_pendente,
	vl_reapresentacao,
	vl_glosa_devida,
	vl_glosa_indevida,
	vl_nao_auditado,
	vl_recebido,
	vl_inicial,
	vl_perdas
into	vl_pendente_w,
	vl_reapresentacao_w,
	vl_glosa_devida_w,
	vl_glosa_indevida_w,
	vl_nao_auditado_w,
	vl_recebido_w,
	vl_inicial_w,
	vl_perdas_w
from	conta_paciente_retorno
where	nr_sequencia = :old.nr_seq_conpaci_ret;

vl_pendente_w 		:= vl_pendente_w + :old.vl_historico;

if	(ie_acao_w <> 8) then -- afstringari 179913 22/01/2010

	if	(ie_acao_w = 0) then
		vl_nao_auditado_w	:= vl_nao_auditado_w - :old.vl_historico;
	elsif	(ie_acao_w = 1) then
		vl_recebido_w		:= vl_recebido_w - :old.vl_historico;
	elsif	(ie_acao_w = 2) then
		vl_reapresentacao_w	:= vl_reapresentacao_w - :old.vl_historico;
	elsif	(ie_acao_w = 3) then
		vl_glosa_devida_w	:= vl_glosa_devida_w - :old.vl_historico;
	elsif	(ie_acao_w = 4) then
		vl_glosa_indevida_w	:= vl_glosa_indevida_w - :old.vl_historico;
	elsif	(ie_acao_w = 5) then
		vl_pendente_w 		:= vl_pendente_w - (:old.vl_historico * 2);
		vl_inicial_w		:= vl_inicial_w  - :old.vl_historico;
	elsif	(ie_acao_w = 7) then
		vl_perdas_w		:= vl_perdas_w - :old.vl_historico;
	end if;

end if;


update	conta_paciente_retorno
set	vl_pendente 		= vl_pendente_w,
	vl_reapresentacao	= vl_reapresentacao_w,
	vl_glosa_devida		= vl_glosa_devida_w,
	vl_glosa_indevida	= vl_glosa_indevida_w,
	vl_nao_auditado 	= vl_nao_auditado_w,
	vl_recebido		= vl_recebido_w,
	vl_inicial		= vl_inicial_w,
	vl_perdas		= vl_perdas_w
where	nr_sequencia 		= :old.nr_seq_conpaci_ret;

update	convenio_retorno_glosa
set	nr_seq_conpaci_ret_hist = null
where	nr_seq_conpaci_ret_hist = :old.nr_sequencia;

end;
/


CREATE OR REPLACE TRIGGER TASY.Conta_Paciente_Ret_Hist_Insert
BEFORE INSERT OR UPDATE ON TASY.CONTA_PACIENTE_RET_HIST FOR EACH ROW
declare

ie_acao_w		number(1);
vl_hist_old_w		number(15,2);
vl_hist_atual_w		number(15,2);

vl_saldo_w		number(15,2);

ie_situacao_w		varchar2(1);
vl_pendente_w		number(15,2);
vl_reapresentacao_w	number(15,2);
vl_glosa_devida_w	number(15,2);
vl_glosa_indevida_w	number(15,2);
vl_nao_auditado_w	number(15,2);
vl_inicial_w		number(15,2);
vl_recebido_w		number(15,2);
vl_perdas_w		number(15,2);
vl_nota_credito_w	number(15,2);

nr_interno_conta_w	number(10);
cd_autorizacao_w	varchar2(20);
nr_seq_ret_item_w	number(10);
ie_valor_amaior_w	varchar2(1);
cd_estabelecimento_w	number(4);
ds_consistencia_w	varchar2(255);
vl_soma_itens_w		number(15,2);

begin

select	nvl(max(ie_acao),-1)
into	ie_acao_w
from	hist_audit_conta_paciente
where	nr_sequencia	= :new.nr_seq_hist_audit;

select	max(a.nr_interno_conta)
into	nr_interno_conta_w
from	conta_paciente_retorno a
where	a.nr_sequencia	= :new.nr_seq_conpaci_ret;

if	(ie_acao_w = -1) then
	/* N�o existe hist�rico da auditoria.
	Verifique cadastro de hist�rico ou desabilite a gera��o de auditoria!
	Conta: NR_INTERNO_CONTA_W */
	wheb_mensagem_pck.exibir_mensagem_abort(180961,'NR_INTERNO_CONTA_W='||nr_interno_conta_w);
end if;

select	a.ie_situacao,
	a.vl_pendente,
	a.vl_reapresentacao,
	a.vl_glosa_devida,
	a.vl_glosa_indevida,
	a.vl_nao_auditado,
	a.vl_inicial,
	a.nr_interno_conta,
	a.cd_autorizacao,
	a.vl_recebido,
	nvl(a.vl_perdas, 0),
	b.cd_estabelecimento,
	nvl(a.vl_nota_credito, 0)
into	ie_situacao_w,
	vl_pendente_w,
	vl_reapresentacao_w,
	vl_glosa_devida_w,
	vl_glosa_indevida_w,
	vl_nao_auditado_w,
	vl_inicial_w,
	nr_interno_conta_w,
	cd_autorizacao_w,
	vl_recebido_w,
	vl_perdas_w,
	cd_estabelecimento_w,
	vl_nota_credito_w
from 	conta_paciente b,
	conta_paciente_retorno a
where 	a.nr_sequencia		= :new.nr_seq_conpaci_ret
and	a.nr_interno_conta	= b.nr_interno_conta;

Obter_Param_Usuario(66,18,obter_perfil_ativo,:new.nm_usuario,cd_estabelecimento_w,ie_valor_amaior_w);

vl_hist_old_w		:= nvl(:old.vl_historico,0);
vl_hist_atual_w		:= :new.vl_historico - vl_hist_old_w;

if	(vl_hist_atual_w = 0) then
	goto final;
end if;

if	(ie_acao_w <> 8) then -- afstringari 179913 22/01/2010

	if	(ie_acao_w = 5) then
		vl_pendente_w	:= vl_pendente_w + vl_hist_atual_w;
		if 	(vl_recebido_w  >= vl_hist_atual_w) then
			vl_recebido_w		:= vl_recebido_w - vl_hist_atual_w;
			vl_hist_atual_w	:= 0;
		else
			vl_hist_atual_w	:= vl_hist_atual_w - vl_recebido_w;
			vl_recebido_w		:= 0;
			vl_inicial_w		:= vl_inicial_w + vl_hist_atual_w;
			vl_hist_atual_w 	:= 0;
		end if;
	end if;

	vl_saldo_w	:= vl_pendente_w + vl_glosa_indevida_w + vl_reapresentacao_w + vl_nao_auditado_w;

	if (nvl(vl_pendente_w,0) = 0) then
		ds_consistencia_w	:= 'A conta ' || nr_interno_conta_w || ' j� est� em auditoria. O processo deve ser efetuado pela Conta Paciente Auditoria!';
	end if;

	if	(vl_saldo_w < vl_hist_atual_w) then
		/* Valor do hist�rico supera o valor pendente!
		Guia: cd_autorizacao_w
		Vl saldo: vl_saldo_w
		Vl hist atual: vl_hist_atual_w */
		wheb_mensagem_pck.exibir_mensagem_abort(180962,	'DS_CONSISTENCIA_W='||ds_consistencia_w||';'||
								'CD_AUTORIZACAO_W='||cd_autorizacao_w||';'||
								'VL_SALDO_W='||vl_saldo_w||';'||
								'VL_HIST_ATUAL_W='||vl_hist_atual_w);
	else
		if	(ie_acao_w = 0) then
			if	(vl_pendente_w < vl_hist_atual_w) then
				/* O hist�rico de n�o auditado s� pode ser lan�ado caso n�o tenha sido lan�ado outros hist�ricos.
				Guia: cd_autorizacao_w */
				wheb_mensagem_pck.exibir_mensagem_abort(180967,	'CD_AUTORIZACAO_W='||cd_autorizacao_w);
			else
				vl_pendente_w	:= vl_pendente_w - vl_hist_atual_w;
			end if;
		elsif 	(ie_acao_w = 7) then
			vl_pendente_w		:= vl_pendente_w - vl_hist_atual_w;
		elsif 	(ie_acao_w = 9) then
			vl_pendente_w		:= vl_pendente_w - vl_hist_atual_w;
		elsif 	(ie_acao_w <> 5) then
			if	(vl_pendente_w >= vl_hist_atual_w) then
				vl_pendente_w	:= vl_pendente_w - vl_hist_atual_w;
				vl_hist_atual_w	:= 0;
			else
				vl_hist_atual_w	:= vl_hist_atual_w - vl_pendente_w;
				vl_pendente_w	:= 0;

				if	(ie_acao_w <> 2) and
					(vl_reapresentacao_w >= vl_hist_atual_w) then
					vl_reapresentacao_w	:= vl_reapresentacao_w - vl_hist_atual_w;
					vl_hist_atual_w		:= 0;
				else
					if	(ie_acao_w <> 2) then
						vl_hist_atual_w		:= vl_hist_atual_w - vl_reapresentacao_w;
						vl_reapresentacao_w	:= 0;
					end if;

					if	(ie_acao_w <> 4) and
						(vl_glosa_indevida_w >= vl_hist_atual_w) then
						vl_glosa_indevida_w	:= vl_glosa_indevida_w - vl_hist_atual_w;
						vl_hist_atual_w		:= 0;
					else
						if	(ie_acao_w <> 4) then
							vl_hist_atual_w	:= vl_hist_atual_w - vl_reapresentacao_w;
							vl_glosa_indevida_w	:= 0;
						end if;

						if	(vl_nao_auditado_w >= vl_hist_atual_w) then
							vl_nao_auditado_w	:= vl_nao_auditado_w - vl_hist_atual_w;
							vl_hist_atual_w	:= 0;
						else
							/* Valor do hist�rico supera o valor pendente!
							Guia: cd_autorizacao_w */
							wheb_mensagem_pck.exibir_mensagem_abort(180970,	'CD_AUTORIZACAO_W='||cd_autorizacao_w);
						end if;
					end if;
				end if;
			end if;
		end if;
	end if;

	if	(ie_acao_w = 0) then
		vl_nao_auditado_w	:= vl_nao_auditado_w + :new.vl_historico - vl_hist_old_w;
	elsif	(ie_acao_w = 1) then
		vl_recebido_w		:= vl_recebido_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 2) then
		vl_reapresentacao_w	:= vl_reapresentacao_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 3) then
		vl_glosa_devida_w	:= vl_glosa_devida_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 4) then
		vl_glosa_indevida_w	:= vl_glosa_indevida_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 7) then
		vl_perdas_w		:= vl_perdas_w + :new.vl_historico - vl_hist_old_w;
	elsif 	(ie_acao_w = 9) then
		vl_nota_credito_w	:= :new.vl_historico - vl_hist_old_w;
	end if;

	vl_soma_itens_w	:= vl_pendente_w + vl_glosa_devida_w + vl_glosa_indevida_w + vl_recebido_w + vl_reapresentacao_w + vl_nao_auditado_w + vl_perdas_w + vl_nota_credito_w;

	if	(vl_soma_itens_w <> vl_inicial_w) and
		(nvl(ie_valor_amaior_w,'N') = 'N') then
		wheb_mensagem_pck.exibir_mensagem_abort(180971,	'CD_AUTORIZACAO_W='||cd_autorizacao_w||';'||
								'NR_INTERNO_CONTA_W='||nr_interno_conta_w||';'||
								'VL_SOMA_ITENS_W='||vl_soma_itens_w||';'||
								'VL_INICIAL_W='||vl_inicial_w||';'||
								'VL_PENDENTE_W='||vl_pendente_w||';'||
								'VL_GLOSA_DEVIDA_W='||vl_glosa_devida_w||';'||
								'VL_GLOSA_INDEVIDA_W='||vl_glosa_indevida_w||';'||
								'VL_RECEBIDO_W='||vl_recebido_w||';'||
								'VL_REAPRESENTACAO_W='||vl_reapresentacao_w||';'||
								'VL_NAO_AUDITADO_W='||vl_nao_auditado_w||';'||
								'VL_PERDAS_W='||vl_perdas_w||';'||
								'VL_NOTA_CREDITO_W='||vl_nota_credito_w);
	end if;
end if;

if	(vl_pendente_w = 0) and
	(vl_glosa_indevida_w = 0) and
	(vl_reapresentacao_w = 0)  and
	(vl_nao_auditado_w = 0) then
	ie_situacao_w := 'F';
end if;

if	(:new.nr_seq_ret_item IS NULL) then
	SELECT 	max(a.nr_sequencia)
	INTO 	nr_seq_ret_item_w
	FROM	convenio_retorno b,
		convenio_retorno_item a
	WHERE 	a.nr_seq_retorno	= b.nr_sequencia
	and 	b.ie_status_retorno	= 'F'
	and 	nr_interno_conta	= nr_interno_conta_w
	AND 	cd_autorizacao		= cd_autorizacao_w;

	:new.nr_seq_ret_item 		:= nr_seq_ret_item_w;
end if;

update conta_paciente_retorno
set	ie_situacao 		= ie_situacao_w,
	vl_pendente 		= vl_pendente_w,
	vl_reapresentacao	= vl_reapresentacao_w,
	vl_glosa_devida		= vl_glosa_devida_w,
	vl_glosa_indevida	= vl_glosa_indevida_w,
	vl_nao_auditado 	= vl_nao_auditado_w,
	vl_recebido		= vl_recebido_w,
	vl_inicial		= vl_inicial_w,
	vl_perdas		= vl_perdas_w,
	vl_nota_credito		= vl_nota_credito_w
where nr_sequencia 		= :new.nr_seq_conpaci_ret;

 <<final>>

vl_inicial_w := vl_inicial_w;

end;
/


ALTER TABLE TASY.CONTA_PACIENTE_RET_HIST ADD (
  CONSTRAINT COPAREHI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.CONTA_PACIENTE_RET_HIST ADD (
  CONSTRAINT COPAREHI_MOREGLO_FK 
 FOREIGN KEY (CD_RESPOSTA) 
 REFERENCES TASY.MOTIVO_RESP_GLOSA (CD_RESPOSTA),
  CONSTRAINT COPAREHI_HIAUCOPA_FK 
 FOREIGN KEY (NR_SEQ_HIST_AUDIT) 
 REFERENCES TASY.HIST_AUDIT_CONTA_PACIENTE (NR_SEQUENCIA),
  CONSTRAINT COPAREHI_CONREIT_FK 
 FOREIGN KEY (NR_SEQ_RET_ITEM) 
 REFERENCES TASY.CONVENIO_RETORNO_ITEM (NR_SEQUENCIA),
  CONSTRAINT COPAREHI_COPARET_FK 
 FOREIGN KEY (NR_SEQ_CONPACI_RET) 
 REFERENCES TASY.CONTA_PACIENTE_RETORNO (NR_SEQUENCIA),
  CONSTRAINT COPAREHI_LOAUTIRE_FK 
 FOREIGN KEY (NR_SEQ_LOTE_AUDIT) 
 REFERENCES TASY.LOTE_AUDIT_TIT_REC (NR_SEQUENCIA),
  CONSTRAINT COPAREHI_MOGLOSA_FK 
 FOREIGN KEY (CD_MOTIVO_GLOSA) 
 REFERENCES TASY.MOTIVO_GLOSA (CD_MOTIVO_GLOSA),
  CONSTRAINT COPAREHI_LOAUREC_FK 
 FOREIGN KEY (NR_SEQ_LOTE_RECURSO) 
 REFERENCES TASY.LOTE_AUDIT_RECURSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.CONTA_PACIENTE_RET_HIST TO NIVEL_1;

