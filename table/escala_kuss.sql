ALTER TABLE TASY.ESCALA_KUSS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_KUSS CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_KUSS
(
  NR_SEQUENCIA                  NUMBER(10)      NOT NULL,
  DT_ATUALIZACAO                DATE            NOT NULL,
  NM_USUARIO                    VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC           DATE,
  NM_USUARIO_NREC               VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA              VARCHAR2(10 BYTE),
  NR_ATENDIMENTO                NUMBER(10),
  IE_SITUACAO                   VARCHAR2(1 BYTE) NOT NULL,
  DT_LIBERACAO                  DATE,
  DT_INATIVACAO                 DATE,
  NM_USUARIO_INATIVACAO         VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA              VARCHAR2(255 BYTE),
  NR_SEQ_ASSINATURA             NUMBER(10),
  DT_AVALIACAO                  DATE            NOT NULL,
  CD_PROFISSIONAL               VARCHAR2(10 BYTE) NOT NULL,
  IE_CHORANDO                   NUMBER(2)       NOT NULL,
  IE_EXPRESSAO_FACIAL           NUMBER(2)       NOT NULL,
  IE_POSTURA_TRONCO             NUMBER(2)       NOT NULL,
  IE_POSTURA_PERNA              NUMBER(2)       NOT NULL,
  IE_INQUIETACAO_MOTORA         NUMBER(2)       NOT NULL,
  QT_SCORE                      NUMBER(2),
  IE_NIVEL_ATENCAO              VARCHAR2(1 BYTE),
  NR_SEQ_ASSINATURA_INATIVACAO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCKUSS_ATEPACI_FK_I ON TASY.ESCALA_KUSS
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCKUSS_PESFISI_FK_I ON TASY.ESCALA_KUSS
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCKUSS_PESFISI_FK2_I ON TASY.ESCALA_KUSS
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCKUSS_PK ON TASY.ESCALA_KUSS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCKUSS_TASASDI_FK_I ON TASY.ESCALA_KUSS
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCKUSS_TASASDI_FK2_I ON TASY.ESCALA_KUSS
(NR_SEQ_ASSINATURA_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.ESCALA_KUSS ADD (
  CONSTRAINT ESCKUSS_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.ESCALA_KUSS ADD (
  CONSTRAINT ESCKUSS_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCKUSS_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCKUSS_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCKUSS_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCKUSS_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINATURA_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.ESCALA_KUSS TO NIVEL_1;

