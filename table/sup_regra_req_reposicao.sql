ALTER TABLE TASY.SUP_REGRA_REQ_REPOSICAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.SUP_REGRA_REQ_REPOSICAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.SUP_REGRA_REQ_REPOSICAO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_LOCAL_ESTOQUE     NUMBER(4),
  IE_PRESCRICAO_CIR    VARCHAR2(1 BYTE)         NOT NULL,
  IE_REG_KIT           VARCHAR2(1 BYTE)         NOT NULL,
  IE_LANCADO_CIR       VARCHAR2(1 BYTE)         NOT NULL,
  IE_MATERIAL_COM      VARCHAR2(1 BYTE)         NOT NULL,
  IE_SALDO_MA_MINIMO   VARCHAR2(1 BYTE)         NOT NULL,
  IE_MULT_MINIMO       VARCHAR2(1 BYTE)         NOT NULL,
  IE_ARRED_BAIXA       VARCHAR2(1 BYTE)         NOT NULL,
  IE_CONSIDERA_REQ     VARCHAR2(1 BYTE)         NOT NULL,
  IE_SOMENTE_ZERADO    VARCHAR2(1 BYTE)         NOT NULL,
  IE_CONSIDERA_EMP     VARCHAR2(1 BYTE)         NOT NULL,
  IE_MULTIPLO_ES_CO    VARCHAR2(1 BYTE)         NOT NULL,
  IE_AGENDA_CIR        VARCHAR2(1 BYTE)         NOT NULL,
  IE_MONTAGEM_KIT      VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.SUREQRE_ESTABEL_FK_I ON TASY.SUP_REGRA_REQ_REPOSICAO
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.SUREQRE_LOCESTO_FK_I ON TASY.SUP_REGRA_REQ_REPOSICAO
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.SUREQRE_LOCESTO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.SUREQRE_PK ON TASY.SUP_REGRA_REQ_REPOSICAO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.SUP_REGRA_REQ_REPOSICAO_tp  after update ON TASY.SUP_REGRA_REQ_REPOSICAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MONTAGEM_KIT,1,4000),substr(:new.IE_MONTAGEM_KIT,1,4000),:new.nm_usuario,nr_seq_w,'IE_MONTAGEM_KIT',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PRESCRICAO_CIR,1,4000),substr(:new.IE_PRESCRICAO_CIR,1,4000),:new.nm_usuario,nr_seq_w,'IE_PRESCRICAO_CIR',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_REG_KIT,1,4000),substr(:new.IE_REG_KIT,1,4000),:new.nm_usuario,nr_seq_w,'IE_REG_KIT',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERA_EMP,1,4000),substr(:new.IE_CONSIDERA_EMP,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERA_EMP',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MULTIPLO_ES_CO,1,4000),substr(:new.IE_MULTIPLO_ES_CO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MULTIPLO_ES_CO',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LANCADO_CIR,1,4000),substr(:new.IE_LANCADO_CIR,1,4000),:new.nm_usuario,nr_seq_w,'IE_LANCADO_CIR',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MATERIAL_COM,1,4000),substr(:new.IE_MATERIAL_COM,1,4000),:new.nm_usuario,nr_seq_w,'IE_MATERIAL_COM',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SALDO_MA_MINIMO,1,4000),substr(:new.IE_SALDO_MA_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SALDO_MA_MINIMO',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_MULT_MINIMO,1,4000),substr(:new.IE_MULT_MINIMO,1,4000),:new.nm_usuario,nr_seq_w,'IE_MULT_MINIMO',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ARRED_BAIXA,1,4000),substr(:new.IE_ARRED_BAIXA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ARRED_BAIXA',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONSIDERA_REQ,1,4000),substr(:new.IE_CONSIDERA_REQ,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONSIDERA_REQ',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOMENTE_ZERADO,1,4000),substr(:new.IE_SOMENTE_ZERADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOMENTE_ZERADO',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_AGENDA_CIR,1,4000),substr(:new.IE_AGENDA_CIR,1,4000),:new.nm_usuario,nr_seq_w,'IE_AGENDA_CIR',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_LOCAL_ESTOQUE,1,4000),substr(:new.CD_LOCAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ESTOQUE',ie_log_w,ds_w,'SUP_REGRA_REQ_REPOSICAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.SUP_REGRA_REQ_REPOSICAO ADD (
  CONSTRAINT SUREQRE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.SUP_REGRA_REQ_REPOSICAO ADD (
  CONSTRAINT SUREQRE_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE),
  CONSTRAINT SUREQRE_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO));

GRANT SELECT ON TASY.SUP_REGRA_REQ_REPOSICAO TO NIVEL_1;

