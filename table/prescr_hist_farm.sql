ALTER TABLE TASY.PRESCR_HIST_FARM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PRESCR_HIST_FARM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PRESCR_HIST_FARM
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_PRESCRICAO          NUMBER(10),
  DT_REGISTRO            DATE                   NOT NULL,
  DT_LIBERACAO           DATE,
  DS_HISTORICO           VARCHAR2(4000 BYTE),
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_PESSOA_FISICA       VARCHAR2(10 BYTE)      NOT NULL,
  NR_SEQ_TIPO_HIST       NUMBER(10),
  CD_MATERIAL_PRESCRITO  NUMBER(10),
  NR_ATENDIMENTO         NUMBER(10),
  NR_SEQ_TIPO_ACEITACAO  NUMBER(10),
  DT_INATIVACAO          DATE,
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  NM_USUARIO_ACEITACAO   VARCHAR2(15 BYTE),
  DT_ACEITACAO           DATE,
  DS_JUST_ACEITACAO      VARCHAR2(480 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PREHIFA_PESFISI_FK_I ON TASY.PRESCR_HIST_FARM
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PREHIFA_PK ON TASY.PRESCR_HIST_FARM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PREHIFA_PRESMED_FK_I ON TASY.PRESCR_HIST_FARM
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREHIFA_PRESMED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREHIFA_TIHISFA_FK_I ON TASY.PRESCR_HIST_FARM
(NR_SEQ_TIPO_HIST)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PREHIFA_TIHISFA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PREHIFA_TIPACEIT_FK_I ON TASY.PRESCR_HIST_FARM
(NR_SEQ_TIPO_ACEITACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PRESCR_HIST_FARM ADD (
  CONSTRAINT PREHIFA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PRESCR_HIST_FARM ADD (
  CONSTRAINT PREHIFA_TIPACEIT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ACEITACAO) 
 REFERENCES TASY.TIPO_ACEITACAO (NR_SEQUENCIA),
  CONSTRAINT PREHIFA_TIHISFA_FK 
 FOREIGN KEY (NR_SEQ_TIPO_HIST) 
 REFERENCES TASY.TIPO_HISTORICO_FARMACIA (NR_SEQUENCIA),
  CONSTRAINT PREHIFA_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PREHIFA_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO));

GRANT SELECT ON TASY.PRESCR_HIST_FARM TO NIVEL_1;

