ALTER TABLE TASY.MATERIAL_AUTOR_PACOTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_AUTOR_PACOTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_AUTOR_PACOTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_MAT_AUTOR     NUMBER(10)               NOT NULL,
  NR_SEQ_PACOTE_AUTOR  NUMBER(10)               NOT NULL,
  QT_SOLICITADA_PCT    NUMBER(15,3),
  QT_SOLICITADA_ANT    NUMBER(15,3)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATAUTPCT_AUTCNPCT_FK_I ON TASY.MATERIAL_AUTOR_PACOTE
(NR_SEQ_PACOTE_AUTOR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MATAUTPCT_PK ON TASY.MATERIAL_AUTOR_PACOTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MATERIAL_AUTOR_PACOTE ADD (
  CONSTRAINT MATAUTPCT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_AUTOR_PACOTE ADD (
  CONSTRAINT MATAUTPCT_AUTCNPCT_FK 
 FOREIGN KEY (NR_SEQ_PACOTE_AUTOR) 
 REFERENCES TASY.AUTOR_CONV_PACOTE (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.MATERIAL_AUTOR_PACOTE TO NIVEL_1;

