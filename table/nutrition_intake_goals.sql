ALTER TABLE TASY.NUTRITION_INTAKE_GOALS
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.NUTRITION_INTAKE_GOALS CASCADE CONSTRAINTS;

CREATE TABLE TASY.NUTRITION_INTAKE_GOALS
(
  NR_SEQUENCIA          NUMBER(15)              NOT NULL,
  CD_ESTABELECIMENTO    NUMBER(4),
  DS_REGRA              VARCHAR2(255 BYTE)      NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  IE_SEXO               VARCHAR2(1 BYTE),
  QT_IDADE_DIA          NUMBER(5),
  QT_IDADE_MES          NUMBER(5),
  QT_IDADE_GESTACIONAL  NUMBER(5),
  QT_PESO               NUMBER(6,3),
  QT_IDADE_ANO          NUMBER(5),
  QT_ALTURA             NUMBER(5,2),
  QT_IMC                NUMBER(3,1),
  IE_VENTILACAO_MEC     VARCHAR2(1 BYTE),
  NR_SEQ_FATOR_ATIV     NUMBER(15),
  CD_SETOR_ATENDIMENTO  NUMBER(5),
  NR_SEQ_FATOR_STRESS   NUMBER(15),
  CD_PERFIL             NUMBER(5),
  QT_CARBOIDRATO_MIN    NUMBER(5),
  QT_CARBOIDRATO_MAX    NUMBER(5),
  IE_UNID_MED_CARB      VARCHAR2(1 BYTE),
  IE_UNID_MED_CAL       VARCHAR2(1 BYTE),
  QT_CALORIA_MAX        NUMBER(5),
  QT_CALORIA_MIN        NUMBER(5),
  QT_VOLUME_MIN         NUMBER(5),
  QT_VOLUME_MAX         NUMBER(5),
  IE_UNID_MED_VOLUME    VARCHAR2(1 BYTE),
  IE_EDEMA              VARCHAR2(3 BYTE),
  IE_ASCITE             VARCHAR2(3 BYTE),
  IE_PARAPLEGICO        VARCHAR2(1 BYTE),
  IE_TETRAPLEGICO       VARCHAR2(1 BYTE),
  NR_ATENDIMENTO        NUMBER(10),
  DT_META               DATE,
  QT_META_MINIMA        NUMBER(10,3),
  QT_META_MAXIMA        NUMBER(10,3),
  IE_TIPO_NUTRIENTE     VARCHAR2(2 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.NITG_ATEPACI_FK_I ON TASY.NUTRITION_INTAKE_GOALS
(NR_ATENDIMENTO, DT_META)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NITG_ESTABEL_FK_I ON TASY.NUTRITION_INTAKE_GOALS
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NITG_NUTFAAT_FK_I ON TASY.NUTRITION_INTAKE_GOALS
(NR_SEQ_FATOR_ATIV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NITG_NUTFAST_FK_I ON TASY.NUTRITION_INTAKE_GOALS
(NR_SEQ_FATOR_STRESS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NITG_PERFIL_FK_I ON TASY.NUTRITION_INTAKE_GOALS
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.NITG_PK ON TASY.NUTRITION_INTAKE_GOALS
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.NITG_SETATEN_FK_I ON TASY.NUTRITION_INTAKE_GOALS
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.NUTRITION_INTAKE_GOALS ADD (
  CONSTRAINT NITG_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.NUTRITION_INTAKE_GOALS ADD (
  CONSTRAINT NITG_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT NITG_NUTFAAT_FK 
 FOREIGN KEY (NR_SEQ_FATOR_ATIV) 
 REFERENCES TASY.NUT_FATOR_ATIV (NR_SEQUENCIA),
  CONSTRAINT NITG_NUTFAST_FK 
 FOREIGN KEY (NR_SEQ_FATOR_STRESS) 
 REFERENCES TASY.NUT_FATOR_STRESS (NR_SEQUENCIA),
  CONSTRAINT NITG_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT NITG_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT NITG_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.NUTRITION_INTAKE_GOALS TO NIVEL_1;

