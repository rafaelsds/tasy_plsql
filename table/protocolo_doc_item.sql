ALTER TABLE TASY.PROTOCOLO_DOC_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROTOCOLO_DOC_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROTOCOLO_DOC_ITEM
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  NR_SEQ_ITEM                 NUMBER(5)         NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  NR_DOCUMENTO                NUMBER(10),
  DS_DOCUMENTO                VARCHAR2(2000 BYTE),
  NR_SEQ_INTERNO              NUMBER(10),
  NR_SEQ_TIPO_ITEM            NUMBER(10),
  DT_CONFERENCIA              DATE,
  DT_RECEBIMENTO              DATE,
  QT_DOCUMENTO                NUMBER(10),
  IE_DESCARTADO               VARCHAR2(1 BYTE),
  NM_USUARIO_RECEB            VARCHAR2(15 BYTE),
  CD_CONVENIO                 NUMBER(10),
  NR_DOC_ITEM                 VARCHAR2(20 BYTE),
  NR_SEQ_MOTIVO_DEV           NUMBER(10),
  NR_DOC_OUTROS               VARCHAR2(255 BYTE),
  DT_INCLUSAO_ITEM            DATE,
  NR_DOCUMENTO_EXT            VARCHAR2(20 BYTE),
  VL_DOCUMENTO_EXT            NUMBER(13,2),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE),
  CD_CNPJ                     VARCHAR2(14 BYTE),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  DT_RETIRADA                 DATE,
  NM_USUARIO_RETIRADA         VARCHAR2(15 BYTE),
  CD_SETOR_ATENDIMENTO        NUMBER(5),
  CD_SETOR_ATEND_PROCEDENCIA  NUMBER(5),
  NR_SEQ_CONTRATO             NUMBER(10),
  VL_HORA_HOSPITAL            NUMBER(15,2),
  VL_HORA_MEDICO              NUMBER(15,2),
  NM_USUARIO_CONF             VARCHAR2(20 BYTE),
  NM_USUARIO_ENTREGA_PAC      VARCHAR2(15 BYTE),
  CD_PESSOA_RECEB_PAC         VARCHAR2(10 BYTE),
  DT_ENTREGA_PAC              DATE,
  NR_SEQ_ETAPA_CONTA          NUMBER(10),
  NR_GUIA                     VARCHAR2(20 BYTE),
  NR_PROTOCOLO_DEVOLUCAO      NUMBER(10),
  NR_SEQ_NASCIMENTO           NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          448K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PRODOIT_CONVENI_FK_I ON TASY.PROTOCOLO_DOC_ITEM
(CD_CONVENIO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOIT_CONVENI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRODOIT_I1 ON TASY.PROTOCOLO_DOC_ITEM
(NR_DOCUMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOIT_I2 ON TASY.PROTOCOLO_DOC_ITEM
(NR_SEQ_INTERNO, NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOIT_MODEDIT_FK_I ON TASY.PROTOCOLO_DOC_ITEM
(NR_SEQ_MOTIVO_DEV)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOIT_MODEDIT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRODOIT_PESFISI_FK_I ON TASY.PROTOCOLO_DOC_ITEM
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOIT_PESFISI_FK2_I ON TASY.PROTOCOLO_DOC_ITEM
(CD_PESSOA_RECEB_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOIT_PESJURI_FK_I ON TASY.PROTOCOLO_DOC_ITEM
(CD_CNPJ)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOIT_PESJURI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PRODOIT_PK ON TASY.PROTOCOLO_DOC_ITEM
(NR_SEQUENCIA, NR_SEQ_ITEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          256K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOIT_PLSCONT_FK_I ON TASY.PROTOCOLO_DOC_ITEM
(NR_SEQ_CONTRATO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOIT_PLSCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRODOIT_PRODOCU_FK_IDX ON TASY.PROTOCOLO_DOC_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOIT_SETATEN_FK_I ON TASY.PROTOCOLO_DOC_ITEM
(CD_SETOR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOIT_SETATEN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PRODOIT_SETATEN_FK2_I ON TASY.PROTOCOLO_DOC_ITEM
(CD_SETOR_ATEND_PROCEDENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOIT_SETATEN_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PRODOIT_TPDOCIT_FK_I ON TASY.PROTOCOLO_DOC_ITEM
(NR_SEQ_TIPO_ITEM)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PRODOIT_USUARIO_FK_I ON TASY.PROTOCOLO_DOC_ITEM
(NM_USUARIO_ENTREGA_PAC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PRODOIT_USUARIO_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.protocolo_doc_item_insert
before insert ON TASY.PROTOCOLO_DOC_ITEM for each row
declare

qt_atend_w  			number(10,0);
ds_tipo_w  			varchar2(100);
ds_program_w		varchar2(100);
ie_formadocconta_w		varchar(1);
ie_gerardescperiodoconta_w 	varchar(1);
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;
ds_periodo_w			varchar2(255);
dt_periodo_inicial_w		conta_paciente.dt_periodo_inicial%type;
dt_periodo_final_w		conta_paciente.dt_periodo_final%type;

begin

cd_estabelecimento_w := wheb_usuario_pck.get_cd_estabelecimento;

obter_param_usuario(290, 26,obter_perfil_ativo,obter_usuario_ativo,cd_estabelecimento_w,ie_formadocconta_w);
obter_param_usuario(290, 92,obter_perfil_ativo,obter_usuario_ativo,cd_estabelecimento_w,ie_gerardescperiodoconta_w);

select  ie_tipo_protocolo
into 	ds_tipo_w
from  	protocolo_documento
where  	nr_sequencia = :new.nr_sequencia;

select program
into   ds_program_w
from   v$session
where  audsid = (SELECT MAX(USERENV('sessionid')) FROM dual);


if  ds_tipo_w in ('2','5')
	and (upper(ds_program_w) = 'TASY.EXE') then

	if (:new.nr_documento is not null) then
		select  	count(*)
		into  	qt_atend_w
		from  	atendimento_paciente
		Where  	nr_atendimento = :new.nr_documento;

		if (qt_atend_w = 0) then
		--r.aise_application_error(-20011,'Este atendimento n�o existe');
			wheb_mensagem_pck.exibir_mensagem_abort(263426);
		end if;

		if ((ie_formadocconta_w = 'C') and
		   (ie_gerardescperiodoconta_w = 'S') and
		   (:new.nr_seq_interno is not null)) then

		   begin
		   select  	dt_periodo_inicial,
				dt_periodo_final
		   into 	dt_periodo_inicial_w,
				dt_periodo_final_w
		   from		conta_paciente
		   where 	nr_interno_conta = :new.nr_seq_interno;
		   exception
		   when others then
		   	dt_periodo_inicial_w	:= null;
			dt_periodo_final_w	:= null;
		   end;
				--Per�odo da conta � de #@DT_PERIODO_INICIAL#@ at� #@DT_PERIODO_FINAL#@
		  :new.ds_documento := wheb_mensagem_pck.get_texto(882113,'DT_PERIODO_INICIAL=' || dt_periodo_inicial_w ||';DT_PERIODO_FINAL='|| dt_periodo_final_w);

		end if;
	end if;
end if;
end;
/


ALTER TABLE TASY.PROTOCOLO_DOC_ITEM ADD (
  CONSTRAINT PRODOIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA, NR_SEQ_ITEM)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          256K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROTOCOLO_DOC_ITEM ADD (
  CONSTRAINT PRODOIT_CONVENI_FK 
 FOREIGN KEY (CD_CONVENIO) 
 REFERENCES TASY.CONVENIO (CD_CONVENIO),
  CONSTRAINT PRODOIT_MODEDIT_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_DEV) 
 REFERENCES TASY.MOTIVO_DEV_DOC_ITEM (NR_SEQUENCIA),
  CONSTRAINT PRODOIT_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRODOIT_PESJURI_FK 
 FOREIGN KEY (CD_CNPJ) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PRODOIT_PLSCONT_FK 
 FOREIGN KEY (NR_SEQ_CONTRATO) 
 REFERENCES TASY.PLS_CONTRATO (NR_SEQUENCIA),
  CONSTRAINT PRODOIT_SETATEN_FK 
 FOREIGN KEY (CD_SETOR_ATENDIMENTO) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRODOIT_SETATEN_FK2 
 FOREIGN KEY (CD_SETOR_ATEND_PROCEDENCIA) 
 REFERENCES TASY.SETOR_ATENDIMENTO (CD_SETOR_ATENDIMENTO),
  CONSTRAINT PRODOIT_PESFISI_FK2 
 FOREIGN KEY (CD_PESSOA_RECEB_PAC) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PRODOIT_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_ENTREGA_PAC) 
 REFERENCES TASY.USUARIO (NM_USUARIO),
  CONSTRAINT PRODOIT_PRODOCU_FK 
 FOREIGN KEY (NR_SEQUENCIA) 
 REFERENCES TASY.PROTOCOLO_DOCUMENTO (NR_SEQUENCIA),
  CONSTRAINT PRODOIT_TPDOCIT_FK 
 FOREIGN KEY (NR_SEQ_TIPO_ITEM) 
 REFERENCES TASY.TIPO_DOC_ITEM (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROTOCOLO_DOC_ITEM TO NIVEL_1;

