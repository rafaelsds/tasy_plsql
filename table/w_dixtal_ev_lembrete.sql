ALTER TABLE TASY.W_DIXTAL_EV_LEMBRETE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_DIXTAL_EV_LEMBRETE CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_DIXTAL_EV_LEMBRETE
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_SEQ_EVENTO           NUMBER(10),
  NR_ID_OBX               NUMBER(3),
  IE_VALUE_TYPE           VARCHAR2(10 BYTE),
  IE_OBS_RESULT_STATUS    VARCHAR2(1 BYTE),
  DS_OBS_IDENTIFIER       VARCHAR2(255 BYTE),
  DS_OBS_IDENTIFIER_TEXT  VARCHAR2(255 BYTE),
  DS_OBS_IDENTIFIER_NAME  VARCHAR2(255 BYTE),
  DS_OBS_VALUE            VARCHAR2(4000 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WDIEVLE_EVEVEPA_FK_I ON TASY.W_DIXTAL_EV_LEMBRETE
(NR_SEQ_EVENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WDIEVLE_PK ON TASY.W_DIXTAL_EV_LEMBRETE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WDIEVLE_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_DIXTAL_EV_LEMBRETE ADD (
  CONSTRAINT WDIEVLE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.W_DIXTAL_EV_LEMBRETE ADD (
  CONSTRAINT WDIEVLE_EVEVEPA_FK 
 FOREIGN KEY (NR_SEQ_EVENTO) 
 REFERENCES TASY.EV_EVENTO_PACIENTE (NR_SEQUENCIA));

GRANT SELECT ON TASY.W_DIXTAL_EV_LEMBRETE TO NIVEL_1;

