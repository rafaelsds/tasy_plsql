ALTER TABLE TASY.PROC_INTERNO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PROC_INTERNO CASCADE CONSTRAINTS;

CREATE TABLE TASY.PROC_INTERNO
(
  NR_SEQUENCIA                   NUMBER(10)     NOT NULL,
  DT_ATUALIZACAO                 DATE           NOT NULL,
  NM_USUARIO                     VARCHAR2(15 BYTE) NOT NULL,
  DS_PROC_EXAME                  VARCHAR2(100 BYTE) NOT NULL,
  DS_LAUDO                       VARCHAR2(100 BYTE) NOT NULL,
  CD_PROCEDIMENTO                NUMBER(15)     NOT NULL,
  IE_ORIGEM_PROCED               NUMBER(10)     NOT NULL,
  CD_CAMPO_DIGIT                 VARCHAR2(25 BYTE),
  DS_ORIENTACAO_USUARIO          VARCHAR2(4000 BYTE),
  IE_ORIENTACAO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_TIPO                        VARCHAR2(5 BYTE) NOT NULL,
  DS_PROC_EXAME_LOC              VARCHAR2(100 BYTE),
  CD_KIT_MATERIAL                NUMBER(5),
  IE_SITUACAO                    VARCHAR2(1 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC            DATE,
  NM_USUARIO_NREC                VARCHAR2(15 BYTE),
  IE_EXIGE_LADO                  VARCHAR2(1 BYTE) NOT NULL,
  IE_CLASSIF_HEM                 VARCHAR2(15 BYTE),
  QT_MIN_CIRURGIA                NUMBER(15),
  IE_TIPO_UTIL                   VARCHAR2(15 BYTE) NOT NULL,
  IE_RESERVA_LEITO               VARCHAR2(3 BYTE),
  DS_OBSERVACAO                  VARCHAR2(2000 BYTE),
  CD_TIPO_ANESTESIA              VARCHAR2(2 BYTE),
  NR_SEQ_CLASSIF                 NUMBER(10),
  QT_MIN_HIGIENIZACAO            NUMBER(15),
  DS_ORIENTACAO_PAC              VARCHAR2(4000 BYTE),
  NR_SEQ_PROTOCOLO               NUMBER(10),
  NR_SEQ_EXAME_LAB               NUMBER(10),
  IE_CTRL_GLIC                   VARCHAR2(3 BYTE),
  IE_LOCALIZADOR                 VARCHAR2(1 BYTE) NOT NULL,
  DS_DIF_FATURAMENTO             VARCHAR2(100 BYTE),
  CD_TIPO_CIRURGIA               NUMBER(10),
  NR_SEQ_PROCED_NISS             NUMBER(10),
  CD_INTEGRACAO                  VARCHAR2(20 BYTE),
  QT_DIAS_PREV_INTER             NUMBER(3),
  IE_CLASSIF_ADEP                VARCHAR2(15 BYTE),
  IE_PORTE_CIRURGIA              VARCHAR2(1 BYTE),
  IE_SEXO                        VARCHAR2(1 BYTE),
  IE_TIPAGEM_SANGUINEA           VARCHAR2(1 BYTE),
  QT_BOLSAS_SANGUE               NUMBER(4),
  IE_TOPOGRAFIA                  VARCHAR2(15 BYTE),
  IE_COPIA                       VARCHAR2(1 BYTE),
  CD_PROCEDIMENTO_TUSS           NUMBER(15),
  IE_ORIGEM_PROC_TUSS            NUMBER(10),
  IE_IVC                         VARCHAR2(2 BYTE),
  QT_DIAS_PREV_UTI               NUMBER(3),
  QT_IDADE_MIN_MESES             NUMBER(5),
  QT_IDADE_MAX_MESES             NUMBER(5),
  IE_HONORARIO_MEDICO            VARCHAR2(1 BYTE),
  QT_MIN_PREPARO                 NUMBER(10),
  DS_ORIENTACAO_SMS              VARCHAR2(150 BYTE),
  NR_SEQ_GRUPO_DIF_FAT           NUMBER(10),
  IE_LAUDO_SUS                   VARCHAR2(1 BYTE),
  IE_VIDEO                       VARCHAR2(1 BYTE),
  CD_PROC_CIH                    NUMBER(15),
  DS_EXAME_CURTO                 VARCHAR2(20 BYTE),
  IE_PADRAO_PATOLOGIA            VARCHAR2(1 BYTE),
  IE_COPIAR_REP                  VARCHAR2(1 BYTE),
  IE_SETOR_PACIENTE              VARCHAR2(1 BYTE),
  IE_EXIGE_RESPONSAVEL           VARCHAR2(1 BYTE),
  IE_IODO                        VARCHAR2(1 BYTE),
  IE_COBRAR_HORARIO              VARCHAR2(1 BYTE),
  IE_EXAME_GESTAO_ENTREGA        VARCHAR2(1 BYTE),
  IE_CONTROLA_CAVIDADE           VARCHAR2(1 BYTE),
  IE_SISMAMA_REP                 VARCHAR2(1 BYTE),
  IE_EXCLUIR_PROC_PRINC          VARCHAR2(1 BYTE),
  IE_APRESENTAR_ASSOC            VARCHAR2(1 BYTE),
  IE_NECESSITA_ANEST             VARCHAR2(1 BYTE),
  IE_IMP_ORIENTACAO              VARCHAR2(1 BYTE),
  DS_SIGLA_EXAME                 VARCHAR2(10 BYTE),
  IE_EXIGE_REGIAO_AGEINT         VARCHAR2(1 BYTE),
  IE_EXIBE_REP                   VARCHAR2(1 BYTE),
  DS_MENSAGEM_REP                VARCHAR2(2000 BYTE),
  IE_EXIGE_CONDICAO              VARCHAR2(1 BYTE),
  NR_SEQ_PROT_GLIC               NUMBER(10),
  IE_EXIGE_CLASSIF_ITEM_AGEINT   VARCHAR2(1 BYTE),
  CD_MEDICO_EXEC                 VARCHAR2(10 BYTE),
  IE_EXIGE_ANESTESIA             VARCHAR2(1 BYTE),
  IE_NAO_CONSISTE_FIM_TURNO      VARCHAR2(1 BYTE),
  DS_ORIENTACAO_EXTERNO          VARCHAR2(4000 BYTE),
  NR_SEQ_AVALIACAO               NUMBER(10),
  IE_GERA_ISOLAMENTO             VARCHAR2(1 BYTE),
  NR_SEQ_GRUPO_INT               NUMBER(10),
  IE_SMS_AGEINT                  VARCHAR2(1 BYTE),
  IE_FINALIDADE                  VARCHAR2(15 BYTE),
  IE_PARECER                     VARCHAR2(1 BYTE),
  NR_SEQ_PRECAUCAO               NUMBER(10),
  IE_EXIGE_ANEST_ITEM_AGEINT     VARCHAR2(1 BYTE),
  IE_EXIGE_PROF_SERV_CIR         VARCHAR2(1 BYTE),
  IE_CHECAR_KIT_ADEP             VARCHAR2(1 BYTE),
  IE_CONSISTIR_DUPLIC            VARCHAR2(1 BYTE),
  IE_ADICIONAL_AGEINT            VARCHAR2(1 BYTE),
  IE_INF_FICHA_MEDICA            VARCHAR2(1 BYTE),
  IE_INVASIVO                    VARCHAR2(1 BYTE),
  IE_NECESSITA_DEMARCACAO        VARCHAR2(1 BYTE),
  IE_EXIGE_SEG_APROV             VARCHAR2(1 BYTE),
  IE_UREASE                      VARCHAR2(1 BYTE),
  IE_AGENDAVEL_EXTERNO           VARCHAR2(1 BYTE),
  IE_CHECAGEM_BEIRA_LEITO        VARCHAR2(1 BYTE),
  IE_OBRIGA_DUM_AGEINT           VARCHAR2(1 BYTE),
  IE_APRES_CONT_CPOE             VARCHAR2(1 BYTE),
  IE_APRES_ANES_CPOE             VARCHAR2(1 BYTE),
  IE_AUTO_AGENDAVEL              VARCHAR2(15 BYTE),
  IE_POSICAO                     NUMBER(3),
  QT_TEMPO_JEJUM                 NUMBER(2),
  IE_CALC_ITEM_ASSOCIADO_AGEINT  VARCHAR2(1 BYTE),
  IE_RADIACAO                    VARCHAR2(1 BYTE),
  QT_RADIACAO                    NUMBER(10),
  IE_EXECUTAVEL                  VARCHAR2(1 BYTE),
  IE_SOLICITAVEL                 VARCHAR2(1 BYTE),
  IE_PERMITE_PRESCR_GRAVIDA      VARCHAR2(1 BYTE),
  IE_DISP_RESPIRATORIO           VARCHAR2(1 BYTE),
  IE_FIO2                        VARCHAR2(1 BYTE),
  IE_PRESSAO                     VARCHAR2(1 BYTE) DEFAULT null,
  IE_CONTROLA_MARCAPASSO         VARCHAR2(1 BYTE),
  IE_PERTENCE_ROL_ANS            VARCHAR2(1 BYTE),
  IE_APROV_ESP_CPOE              VARCHAR2(3 BYTE),
  IE_PERMITE_SANGUE_ASSOC_CPOE   VARCHAR2(1 BYTE),
  IE_VERIFICA_DIETA              VARCHAR2(1 BYTE),
  DS_ALERTA_VERIFICA_DIETA       VARCHAR2(255 BYTE),
  IE_AGENDA_WEB                  VARCHAR2(1 BYTE),
  NR_SEQ_MESSAGE_PROC            NUMBER(10),
  IE_PERMITE_EXEC_PROC           VARCHAR2(1 BYTE),
  CD_ESPECIALIDADE               NUMBER(5),
  IE_TEMPO_EXECUCAO              VARCHAR2(1 BYTE),
  CD_PROCEDIMENTO_LOC            VARCHAR2(20 BYTE),
  IE_VISUALIZA_DOC_ADIC_TWS      VARCHAR2(1 BYTE),
  NR_SEQ_TEMPL_QUAL_MAT          NUMBER(10),
  NR_SEQ_TEMPL_LINFO             NUMBER(10),
  IE_CHANGE_PROC_NAME            VARCHAR2(1 BYTE),
  IE_IS_TIME_MANDATORY           VARCHAR2(1 BYTE),
  IE_OPINIAO_MEDICA              VARCHAR2(1 BYTE),
  IE_AT_HOME                     VARCHAR2(1 BYTE),
  IE_EXIGE_CONTRASTE             VARCHAR2(3 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          896K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PROINTE_CIHPREC_FK_I ON TASY.PROC_INTERNO
(NR_SEQ_PRECAUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINTE_CIHPREC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINTE_CIHPROC_FK_I ON TASY.PROC_INTERNO
(CD_PROC_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_CIHTICI_FK_I ON TASY.PROC_INTERNO
(CD_TIPO_CIRURGIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINTE_CIHTICI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINTE_EHRTEMP_FK1_I ON TASY.PROC_INTERNO
(NR_SEQ_TEMPL_QUAL_MAT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_EHRTEMP_FK2_I ON TASY.PROC_INTERNO
(NR_SEQ_TEMPL_LINFO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_EXALABO_FK_I ON TASY.PROC_INTERNO
(NR_SEQ_EXAME_LAB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINTE_EXALABO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINTE_GRPRIN_FK_I ON TASY.PROC_INTERNO
(NR_SEQ_GRUPO_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINTE_GRPRIN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINTE_KITMATE_FK_I ON TASY.PROC_INTERNO
(CD_KIT_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_MEDICO_FK_I ON TASY.PROC_INTERNO
(CD_MEDICO_EXEC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_NIPROCE_FK_I ON TASY.PROC_INTERNO
(NR_SEQ_PROCED_NISS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINTE_NIPROCE_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PROINTE_PK ON TASY.PROC_INTERNO
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_PROCEDI_FK_I ON TASY.PROC_INTERNO
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_PROCEDI_FK2_I ON TASY.PROC_INTERNO
(CD_PROCEDIMENTO_TUSS, IE_ORIGEM_PROC_TUSS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_PROINTCLA_FK_I ON TASY.PROC_INTERNO
(NR_SEQ_CLASSIF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PROINTE_PROINTCLA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PROINTE_PROMEDI_FK_I ON TASY.PROC_INTERNO
(NR_SEQ_PROTOCOLO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_RUPROME_FK_I ON TASY.PROC_INTERNO
(NR_SEQ_MESSAGE_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PROINTE_SEARCH ON TASY.PROC_INTERNO
(SUBSTRB("TASY"."TASYAUTOCOMPLETE"."TOINDEXWITHOUTACCENTS"("DS_PROC_EXAME"),1,4000))
LOGGING
TABLESPACE TASY_DATA
PCTFREE    10
INITRANS   2
MAXTRANS   167
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.smart_tipo_exame_afterinsert
after insert or update ON TASY.PROC_INTERNO for each row
declare

reg_integracao_w		gerar_int_padrao.reg_integracao;
ie_action_w			varchar2(255);

begin

if	(inserting) and
	(:new.ie_tipo_util = 'E') then
	ie_action_w := 'IE_ACTION_P=CREATE';
	gerar_int_padrao.gravar_integracao('329', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
elsif	(inserting) and
	(:new.nr_seq_exame_lab is not null) then
	ie_action_w := 'IE_ACTION_P=CREATE';
	gerar_int_padrao.gravar_integracao('309', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
end if;

if	(updating)  and
	(:new.ie_tipo_util = 'E') and
	(:new.ie_situacao = 'A') then
	ie_action_w := 'IE_ACTION_P=UPDATE';
	gerar_int_padrao.gravar_integracao('329', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
	--insert into temp_saps (estabelecimento) values ('9');
elsif	(updating) and
	(:new.nr_seq_exame_lab is not null) and
	(:new.ie_situacao = 'A') then
	ie_action_w := 'IE_ACTION_P=UPDATE';
	gerar_int_padrao.gravar_integracao('309', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
end if;

if	(updating) and
	(:new.ie_tipo_util = 'E') and
	(:new.ie_situacao = 'I') then
	ie_action_w := 'IE_ACTION_P=DELETE';
	gerar_int_padrao.gravar_integracao('329', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
elsif	(updating) and
	(:new.nr_seq_exame_lab is not null)
	and (:new.ie_situacao = 'I') then
	ie_action_w := 'IE_ACTION_P=DELETE';
	gerar_int_padrao.gravar_integracao('309', :new.NR_SEQUENCIA, :new.nm_usuario, reg_integracao_w,ie_action_w);
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PROC_INTERNO_tp  after update ON TASY.PROC_INTERNO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_EXAME_LAB,1,4000),substr(:new.NR_SEQ_EXAME_LAB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_EXAME_LAB',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROC_EXAME_LOC,1,4000),substr(:new.DS_PROC_EXAME_LOC,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROC_EXAME_LOC',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NM_USUARIO,1,4000),substr(:new.NM_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_PROC_EXAME,1,4000),substr(:new.DS_PROC_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'DS_PROC_EXAME',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_LAUDO,1,4000),substr(:new.DS_LAUDO,1,4000),:new.nm_usuario,nr_seq_w,'DS_LAUDO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO,1,4000),substr(:new.CD_PROCEDIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROCED,1,4000),substr(:new.IE_ORIGEM_PROCED,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROCED',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO,1,4000),substr(:new.IE_TIPO,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO_USUARIO,1,4000),substr(:new.DS_ORIENTACAO_USUARIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO_USUARIO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_CAMPO_DIGIT,1,4000),substr(:new.CD_CAMPO_DIGIT,1,4000),:new.nm_usuario,nr_seq_w,'CD_CAMPO_DIGIT',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIENTACAO,1,4000),substr(:new.IE_ORIENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIENTACAO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIF_HEM,1,4000),substr(:new.IE_CLASSIF_HEM,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIF_HEM',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_KIT_MATERIAL,1,4000),substr(:new.CD_KIT_MATERIAL,1,4000),:new.nm_usuario,nr_seq_w,'CD_KIT_MATERIAL',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_LADO,1,4000),substr(:new.IE_EXIGE_LADO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_LADO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_CLASSIF,1,4000),substr(:new.NR_SEQ_CLASSIF,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_CLASSIF',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_ANESTESIA,1,4000),substr(:new.CD_TIPO_ANESTESIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_ANESTESIA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPO_UTIL,1,4000),substr(:new.IE_TIPO_UTIL,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPO_UTIL',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO_PAC,1,4000),substr(:new.DS_ORIENTACAO_PAC,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO_PAC',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROTOCOLO,1,4000),substr(:new.NR_SEQ_PROTOCOLO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROTOCOLO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_RESERVA_LEITO,1,4000),substr(:new.IE_RESERVA_LEITO,1,4000),:new.nm_usuario,nr_seq_w,'IE_RESERVA_LEITO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_CIRURGIA,1,4000),substr(:new.QT_MIN_CIRURGIA,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_CIRURGIA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_HIGIENIZACAO,1,4000),substr(:new.QT_MIN_HIGIENIZACAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_HIGIENIZACAO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_CIRURGIA,1,4000),substr(:new.CD_TIPO_CIRURGIA,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_CIRURGIA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_PREV_INTER,1,4000),substr(:new.QT_DIAS_PREV_INTER,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_PREV_INTER',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_INTEGRACAO,1,4000),substr(:new.CD_INTEGRACAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_INTEGRACAO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROCED_NISS,1,4000),substr(:new.NR_SEQ_PROCED_NISS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROCED_NISS',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LOCALIZADOR,1,4000),substr(:new.IE_LOCALIZADOR,1,4000),:new.nm_usuario,nr_seq_w,'IE_LOCALIZADOR',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_DIF_FATURAMENTO,1,4000),substr(:new.DS_DIF_FATURAMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_DIF_FATURAMENTO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CTRL_GLIC,1,4000),substr(:new.IE_CTRL_GLIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_CTRL_GLIC',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PORTE_CIRURGIA,1,4000),substr(:new.IE_PORTE_CIRURGIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PORTE_CIRURGIA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CLASSIF_ADEP,1,4000),substr(:new.IE_CLASSIF_ADEP,1,4000),:new.nm_usuario,nr_seq_w,'IE_CLASSIF_ADEP',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SEXO,1,4000),substr(:new.IE_SEXO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SEXO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TIPAGEM_SANGUINEA,1,4000),substr(:new.IE_TIPAGEM_SANGUINEA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TIPAGEM_SANGUINEA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_BOLSAS_SANGUE,1,4000),substr(:new.QT_BOLSAS_SANGUE,1,4000),:new.nm_usuario,nr_seq_w,'QT_BOLSAS_SANGUE',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IVC,1,4000),substr(:new.IE_IVC,1,4000),:new.nm_usuario,nr_seq_w,'IE_IVC',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_TOPOGRAFIA,1,4000),substr(:new.IE_TOPOGRAFIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_TOPOGRAFIA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COPIA,1,4000),substr(:new.IE_COPIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_COPIA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROCEDIMENTO_TUSS,1,4000),substr(:new.CD_PROCEDIMENTO_TUSS,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROCEDIMENTO_TUSS',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_RADIACAO,1,4000),substr(:new.QT_RADIACAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_RADIACAO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ORIGEM_PROC_TUSS,1,4000),substr(:new.IE_ORIGEM_PROC_TUSS,1,4000),:new.nm_usuario,nr_seq_w,'IE_ORIGEM_PROC_TUSS',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_DIAS_PREV_UTI,1,4000),substr(:new.QT_DIAS_PREV_UTI,1,4000),:new.nm_usuario,nr_seq_w,'QT_DIAS_PREV_UTI',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO_SMS,1,4000),substr(:new.DS_ORIENTACAO_SMS,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO_SMS',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_LAUDO_SUS,1,4000),substr(:new.IE_LAUDO_SUS,1,4000),:new.nm_usuario,nr_seq_w,'IE_LAUDO_SUS',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_GRUPO_DIF_FAT,1,4000),substr(:new.NR_SEQ_GRUPO_DIF_FAT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_GRUPO_DIF_FAT',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MIN_MESES,1,4000),substr(:new.QT_IDADE_MIN_MESES,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MIN_MESES',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_IDADE_MAX_MESES,1,4000),substr(:new.QT_IDADE_MAX_MESES,1,4000),:new.nm_usuario,nr_seq_w,'QT_IDADE_MAX_MESES',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.QT_MIN_PREPARO,1,4000),substr(:new.QT_MIN_PREPARO,1,4000),:new.nm_usuario,nr_seq_w,'QT_MIN_PREPARO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_HONORARIO_MEDICO,1,4000),substr(:new.IE_HONORARIO_MEDICO,1,4000),:new.nm_usuario,nr_seq_w,'IE_HONORARIO_MEDICO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VIDEO,1,4000),substr(:new.IE_VIDEO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIDEO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROC_CIH,1,4000),substr(:new.CD_PROC_CIH,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROC_CIH',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EXAME_CURTO,1,4000),substr(:new.DS_EXAME_CURTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_EXAME_CURTO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COPIAR_REP,1,4000),substr(:new.IE_COPIAR_REP,1,4000),:new.nm_usuario,nr_seq_w,'IE_COPIAR_REP',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_PADRAO_PATOLOGIA,1,4000),substr(:new.IE_PADRAO_PATOLOGIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_PADRAO_PATOLOGIA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SETOR_PACIENTE,1,4000),substr(:new.IE_SETOR_PACIENTE,1,4000),:new.nm_usuario,nr_seq_w,'IE_SETOR_PACIENTE',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXAME_GESTAO_ENTREGA,1,4000),substr(:new.IE_EXAME_GESTAO_ENTREGA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXAME_GESTAO_ENTREGA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_COBRAR_HORARIO,1,4000),substr(:new.IE_COBRAR_HORARIO,1,4000),:new.nm_usuario,nr_seq_w,'IE_COBRAR_HORARIO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IODO,1,4000),substr(:new.IE_IODO,1,4000),:new.nm_usuario,nr_seq_w,'IE_IODO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_RESPONSAVEL,1,4000),substr(:new.IE_EXIGE_RESPONSAVEL,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_RESPONSAVEL',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_APRESENTAR_ASSOC,1,4000),substr(:new.IE_APRESENTAR_ASSOC,1,4000),:new.nm_usuario,nr_seq_w,'IE_APRESENTAR_ASSOC',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SISMAMA_REP,1,4000),substr(:new.IE_SISMAMA_REP,1,4000),:new.nm_usuario,nr_seq_w,'IE_SISMAMA_REP',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXCLUIR_PROC_PRINC,1,4000),substr(:new.IE_EXCLUIR_PROC_PRINC,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXCLUIR_PROC_PRINC',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NECESSITA_ANEST,1,4000),substr(:new.IE_NECESSITA_ANEST,1,4000),:new.nm_usuario,nr_seq_w,'IE_NECESSITA_ANEST',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_CONTROLA_CAVIDADE,1,4000),substr(:new.IE_CONTROLA_CAVIDADE,1,4000),:new.nm_usuario,nr_seq_w,'IE_CONTROLA_CAVIDADE',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_IMP_ORIENTACAO,1,4000),substr(:new.IE_IMP_ORIENTACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_IMP_ORIENTACAO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_SIGLA_EXAME,1,4000),substr(:new.DS_SIGLA_EXAME,1,4000),:new.nm_usuario,nr_seq_w,'DS_SIGLA_EXAME',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIBE_REP,1,4000),substr(:new.IE_EXIBE_REP,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIBE_REP',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_REGIAO_AGEINT,1,4000),substr(:new.IE_EXIGE_REGIAO_AGEINT,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_REGIAO_AGEINT',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_MENSAGEM_REP,1,4000),substr(:new.DS_MENSAGEM_REP,1,4000),:new.nm_usuario,nr_seq_w,'DS_MENSAGEM_REP',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_CONDICAO,1,4000),substr(:new.IE_EXIGE_CONDICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_CONDICAO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PROT_GLIC,1,4000),substr(:new.NR_SEQ_PROT_GLIC,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PROT_GLIC',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_CLASSIF_ITEM_AGEINT,1,4000),substr(:new.IE_EXIGE_CLASSIF_ITEM_AGEINT,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_CLASSIF_ITEM_AGEINT',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_NAO_CONSISTE_FIM_TURNO,1,4000),substr(:new.IE_NAO_CONSISTE_FIM_TURNO,1,4000),:new.nm_usuario,nr_seq_w,'IE_NAO_CONSISTE_FIM_TURNO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ORIENTACAO_EXTERNO,1,4000),substr(:new.DS_ORIENTACAO_EXTERNO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ORIENTACAO_EXTERNO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MEDICO_EXEC,1,4000),substr(:new.CD_MEDICO_EXEC,1,4000),:new.nm_usuario,nr_seq_w,'CD_MEDICO_EXEC',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_AVALIACAO,1,4000),substr(:new.NR_SEQ_AVALIACAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_AVALIACAO',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_EXIGE_ANESTESIA,1,4000),substr(:new.IE_EXIGE_ANESTESIA,1,4000),:new.nm_usuario,nr_seq_w,'IE_EXIGE_ANESTESIA',ie_log_w,ds_w,'PROC_INTERNO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.Proc_interno_atual
BEFORE UPDATE OR INSERT ON TASY.PROC_INTERNO FOR EACH ROW
begin

if	(:new.ds_proc_exame_loc	is null) or
	(Upper(:new.ds_proc_exame) <> :old.ds_proc_exame )	then
	:new.ds_proc_exame_loc	:= upper(elimina_acentuacao(:new.ds_proc_exame));

end if;

if  (:new.ie_origem_proc_tuss is null) or
    (:new.ie_origem_proc_tuss <> 8) or
    (:old.ie_origem_proc_tuss <> 8) then
    :new.ie_origem_proc_tuss:= 8;
end if;


END;
/


ALTER TABLE TASY.PROC_INTERNO ADD (
  CONSTRAINT PROINTE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PROC_INTERNO ADD (
  CONSTRAINT PROINTE_EHRTEMP_FK1 
 FOREIGN KEY (NR_SEQ_TEMPL_QUAL_MAT) 
 REFERENCES TASY.EHR_TEMPLATE (NR_SEQUENCIA),
  CONSTRAINT PROINTE_EHRTEMP_FK2 
 FOREIGN KEY (NR_SEQ_TEMPL_LINFO) 
 REFERENCES TASY.EHR_TEMPLATE (NR_SEQUENCIA),
  CONSTRAINT PROINTE_CIHPREC_FK 
 FOREIGN KEY (NR_SEQ_PRECAUCAO) 
 REFERENCES TASY.CIH_PRECAUCAO (NR_SEQUENCIA),
  CONSTRAINT PROINTE_RUPROME_FK 
 FOREIGN KEY (NR_SEQ_MESSAGE_PROC) 
 REFERENCES TASY.RULE_PROCEDURE_MESSAGE (NR_SEQUENCIA),
  CONSTRAINT PROINTE_PROCEDI_FK2 
 FOREIGN KEY (CD_PROCEDIMENTO_TUSS, IE_ORIGEM_PROC_TUSS) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PROINTE_CIHTICI_FK 
 FOREIGN KEY (CD_TIPO_CIRURGIA) 
 REFERENCES TASY.CIH_TIPO_CIRURGIA (CD_TIPO_CIRURGIA),
  CONSTRAINT PROINTE_CIHPROC_FK 
 FOREIGN KEY (CD_PROC_CIH) 
 REFERENCES TASY.CIH_PROCEDIMENTO (CD_PROCEDIMENTO),
  CONSTRAINT PROINTE_GRPRIN_FK 
 FOREIGN KEY (NR_SEQ_GRUPO_INT) 
 REFERENCES TASY.GRUPO_PROC_INTERNO (NR_SEQUENCIA),
  CONSTRAINT PROINTE_MEDICO_FK 
 FOREIGN KEY (CD_MEDICO_EXEC) 
 REFERENCES TASY.MEDICO (CD_PESSOA_FISICA),
  CONSTRAINT PROINTE_KITMATE_FK 
 FOREIGN KEY (CD_KIT_MATERIAL) 
 REFERENCES TASY.KIT_MATERIAL (CD_KIT_MATERIAL),
  CONSTRAINT PROINTE_EXALABO_FK 
 FOREIGN KEY (NR_SEQ_EXAME_LAB) 
 REFERENCES TASY.EXAME_LABORATORIO (NR_SEQ_EXAME),
  CONSTRAINT PROINTE_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PROINTE_PROINTCLA_FK 
 FOREIGN KEY (NR_SEQ_CLASSIF) 
 REFERENCES TASY.PROC_INTERNO_CLASSIF (NR_SEQUENCIA),
  CONSTRAINT PROINTE_PROMEDI_FK 
 FOREIGN KEY (NR_SEQ_PROTOCOLO) 
 REFERENCES TASY.PROTOCOLO_MEDICACAO (NR_SEQ_INTERNA),
  CONSTRAINT PROINTE_NIPROCE_FK 
 FOREIGN KEY (NR_SEQ_PROCED_NISS) 
 REFERENCES TASY.NISS_PROCEDIMENTO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PROC_INTERNO TO NIVEL_1;

GRANT SELECT ON TASY.PROC_INTERNO TO ROBOCMTECNOLOGIA;

