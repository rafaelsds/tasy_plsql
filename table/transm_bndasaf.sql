ALTER TABLE TASY.TRANSM_BNDASAF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TRANSM_BNDASAF CASCADE CONSTRAINTS;

CREATE TABLE TASY.TRANSM_BNDASAF
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  IE_EVENTO                  VARCHAR2(15 BYTE)  NOT NULL,
  NR_ATENDIMENTO             NUMBER(10),
  CD_REGISTRO_ORIGEM         VARCHAR2(100 BYTE),
  CD_REGISTRO_ENTRADA        VARCHAR2(20 BYTE),
  CD_REGISTRO_EXTERNO        VARCHAR2(30 BYTE),
  QT_DISPENSACAO             NUMBER(13,4),
  DT_DISPENSACAO             DATE,
  IE_LIDO                    VARCHAR2(1 BYTE),
  DS_SITUACAO_PROCESSAMENTO  VARCHAR2(26 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.TRANS_INT_PK ON TASY.TRANSM_BNDASAF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.TRANSM_BNDASAF ADD (
  CONSTRAINT TRANS_INT_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.TRANSM_BNDASAF TO NIVEL_1;

