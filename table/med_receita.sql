ALTER TABLE TASY.MED_RECEITA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MED_RECEITA CASCADE CONSTRAINTS;

CREATE TABLE TASY.MED_RECEITA
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  NR_ATENDIMENTO              NUMBER(10),
  DT_RECEITA                  DATE              NOT NULL,
  DS_RECEITA                  LONG,
  NR_ATENDIMENTO_HOSP         NUMBER(10),
  NR_SEQ_CLIENTE              NUMBER(10),
  CD_PESSOA_FISICA            VARCHAR2(10 BYTE) NOT NULL,
  CD_MEDICO                   VARCHAR2(10 BYTE),
  DT_LIBERACAO                DATE,
  IE_TIPO_RECEITA             VARCHAR2(15 BYTE),
  IE_SITUACAO                 VARCHAR2(1 BYTE)  NOT NULL,
  DT_INATIVACAO               DATE,
  NM_USUARIO_INATIVACAO       VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA            VARCHAR2(255 BYTE),
  CD_PERFIL_ATIVO             NUMBER(5),
  NR_SEQ_ORIGEM               NUMBER(10),
  IE_RN                       VARCHAR2(1 BYTE),
  IE_RESTRICAO_VISUALIZACAO   VARCHAR2(1 BYTE),
  NR_RECEM_NATO               NUMBER(3),
  NR_SEQ_ASSINATURA           NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO   NUMBER(10),
  NR_PRESCRICAO               NUMBER(15),
  NR_SEQ_TEXTO                NUMBER(10),
  IE_DATA                     VARCHAR2(1 BYTE),
  IE_NIVEL_ATENCAO            VARCHAR2(1 BYTE),
  DS_UTC                      VARCHAR2(50 BYTE),
  IE_HORARIO_VERAO            VARCHAR2(15 BYTE),
  DS_UTC_ATUALIZACAO          VARCHAR2(50 BYTE),
  NR_SEQ_SOAP                 NUMBER(10),
  IE_TIPO_AVALIACAO           VARCHAR2(1 BYTE),
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  NR_SEQ_ATEND_CONS_PEPA      NUMBER(10),
  IE_ACAO                     VARCHAR2(1 BYTE),
  DT_VALIDADE                 DATE              DEFAULT null,
  DT_VIGENCIA                 DATE,
  DS_JUSTIFICATIVA_ALTERACAO  VARCHAR2(255 BYTE),
  CD_MODELO_FORMATACAO        NUMBER(10),
  NR_SEQ_FORMULARIO           NUMBER(10),
  CD_EXT_REFERENCIA           VARCHAR2(255 BYTE),
  IE_EXT_ESPECIAL             VARCHAR2(1 BYTE),
  IE_EXT_CERTDIGITAL          VARCHAR2(1 BYTE),
  DT_EXT_CRIACAO              DATE,
  DS_EXTERNO                  VARCHAR2(255 BYTE),
  DS_EXT_RECURLPDF            VARCHAR2(255 BYTE),
  CD_EXT_IDPRESCRICAO         VARCHAR2(255 BYTE),
  DS_EXT_ATSURLPDF            VARCHAR2(255 BYTE),
  DS_EXT_EXMURLPDF            VARCHAR2(255 BYTE),
  DS_EXT_CODVALIDACAO         VARCHAR2(255 BYTE),
  IE_EXT_RECPAPEL             VARCHAR2(1 BYTE),
  IE_EXT_ASSINADA             VARCHAR2(1 BYTE),
  DS_EXT_URLPDF               VARCHAR2(4000 BYTE) DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          42M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MEDRECE_ATCONSPEPA_FK_I ON TASY.MED_RECEITA
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDRECE_ATEPACI_FK_I ON TASY.MED_RECEITA
(NR_ATENDIMENTO_HOSP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDRECE_ATESOAP_FK_I ON TASY.MED_RECEITA
(NR_SEQ_SOAP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDRECE_EHRREGI_FK_I ON TASY.MED_RECEITA
(NR_SEQ_FORMULARIO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDRECE_I1 ON TASY.MED_RECEITA
(DT_RECEITA, NR_ATENDIMENTO, CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDRECE_I1
  MONITORING USAGE;


CREATE INDEX TASY.MEDRECE_MEDATEN_FK_I ON TASY.MED_RECEITA
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDRECE_MEDCLIE_FK_I ON TASY.MED_RECEITA
(NR_SEQ_CLIENTE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDRECE_MEDRECE_FK_I ON TASY.MED_RECEITA
(NR_SEQ_ORIGEM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDRECE_MEDRECE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDRECE_PERFIL_FK_I ON TASY.MED_RECEITA
(CD_PERFIL_ATIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDRECE_PERFIL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDRECE_PESFISI_FK_I ON TASY.MED_RECEITA
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDRECE_PESFISI_FK2_I ON TASY.MED_RECEITA
(CD_MEDICO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          3M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.MEDRECE_PK ON TASY.MED_RECEITA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          2M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.MEDRECE_PRESMED_FK_I ON TASY.MED_RECEITA
(NR_PRESCRICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDRECE_PRESMED_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDRECE_TASASDI_FK_I ON TASY.MED_RECEITA
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDRECE_TASASDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDRECE_TASASDI_FK2_I ON TASY.MED_RECEITA
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDRECE_TASASDI_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.MEDRECE_TEXPADR_FK_I ON TASY.MED_RECEITA
(NR_SEQ_TEXTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MEDRECE_TEXPADR_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.med_receita_pend_atual
after insert or update ON TASY.MED_RECEITA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);
ie_lib_receita_w	varchar2(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;
begin

select	max(IE_LIB_RECEITA)
into	ie_lib_receita_w
from	parametro_medico
where	cd_estabelecimento = wheb_usuario_pck.get_cd_estabelecimento;

if	(nvl(ie_lib_receita_w,'N') = 'S') then
	if	(:new.dt_liberacao is null) then
		ie_tipo_w := 'REC';
	elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
		ie_tipo_w := 'XREC';
	end if;
	if	(ie_tipo_w	is not null) then
		Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, nvl(:new.cd_pessoa_fisica,substr(obter_pessoa_atendimento(:new.nr_atendimento_hosp,'C'),1,255)), :new.nr_atendimento_hosp, :new.nm_usuario);
	end if;
end if;


exception
when others then
	null;
end;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.MED_RECEITA_ATUAL
before insert or update ON TASY.MED_RECEITA for each row
declare


begin
if	(nvl(:old.DT_RECEITA,sysdate+10) <> :new.DT_RECEITA) and
	(:new.DT_RECEITA is not null) then
	:new.ds_utc		:= obter_data_utc(:new.DT_RECEITA, 'HV');
end if;

if	(nvl(:old.DT_LIBERACAO,sysdate+10) <> :new.DT_LIBERACAO) and
	(:new.DT_LIBERACAO is not null) then
	:new.ds_utc_atualizacao	:= obter_data_utc(:new.DT_LIBERACAO,'HV');
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MED_RECEITA_DELETE
after delete ON TASY.MED_RECEITA for each row
declare

qt_reg_w		number(1);
ie_tipo_w		varchar2(10);

pragma autonomous_transaction;

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

delete
from   	pep_item_pendente
where  	ie_tipo_pendencia = 'L'
and		nr_seq_receita = :old.nr_sequencia;

commit;


<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.MED_RECEITA_SBIS_DEL
before delete ON TASY.MED_RECEITA for each row
declare
nr_log_seq_w		number(10);

begin

select 	log_alteracao_prontuario_seq.nextval
into 	nr_log_seq_w
from 	dual;

insert into log_alteracao_prontuario (nr_sequencia,
									 dt_atualizacao,
									 ds_evento,
									 ds_maquina,
									 cd_pessoa_fisica,
									 ds_item,
									 nr_atendimento,
									 dt_liberacao,
									 dt_inativacao,
									 nm_usuario) values
									 (nr_log_seq_w,
									 sysdate,
									 obter_desc_expressao(493387) ,
									 wheb_usuario_pck.get_nm_maquina,
									 :old.cd_pessoa_fisica,
									 obter_desc_expressao(314115),
									 :old.nr_atendimento_hosp,
									 :old.dt_liberacao,
									 :old.dt_inativacao,
									 nvl(wheb_usuario_pck.get_nm_usuario, :old.nm_usuario));


end;
/


CREATE OR REPLACE TRIGGER TASY.oft_receita_pend_atual
after insert or update or delete ON TASY.MED_RECEITA for each row
declare

PRAGMA AUTONOMOUS_TRANSACTION;

qt_reg_w	               number(1);
ie_tipo_w		            varchar2(10);
cd_pessoa_fisica_w	varchar2(30);
nr_atendimento_w    number(10);
ie_libera_exames_oft_w	varchar2(5);

begin

if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

select	nvl(max(ie_libera_exames_oft),'N')
into	ie_libera_exames_oft_w
from	parametro_medico
where	cd_estabelecimento = obter_estabelecimento_ativo;

if (ie_libera_exames_oft_w = 'S') then
	if	(inserting) or
		(updating) then

		if (obter_funcao_ativa = 3010) then
			if	(:new.dt_liberacao is null) then
				ie_tipo_w := 'RECL';
			elsif	(:old.dt_liberacao is null) and
				   (:new.dt_liberacao is not null) then
				   ie_tipo_w := 'XRECL';
			end if;
		end if;

     SELECT	MAX(a.nr_atendimento_hosp),
                  MAX(a.cd_pessoa_fisica)
      INTO	   nr_atendimento_w,
                  cd_pessoa_fisica_w
      FROM     med_receita a WHERE a.nr_sequencia = :NEW.nr_sequencia;

		begin
		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_sequencia, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
		end if;
		exception
			when others then
			null;
		end;

	elsif	(obter_funcao_ativa = 3010)  and
			(deleting) then
            delete 	from pep_item_pendente
            where 	IE_TIPO_REGISTRO = 'RECL'
            and	      nr_seq_registro = :old.nr_sequencia
            and	      nvl(IE_TIPO_PENDENCIA,'L')	= 'L';

            commit;
    end if;

	commit;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.SBIS_MED_RECEITA
before insert or update ON TASY.MED_RECEITA for each row
declare

begin
if	(inserting) then
	:new.ie_acao := 'I';
elsif (updating) then
	:new.ie_acao := 'U';
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.MED_RECEITA_SBIS_IN
before insert or update ON TASY.MED_RECEITA for each row
declare

nr_log_seq_w		number(10);
ie_inativacao_w		varchar2(1);

begin
  select 	log_alteracao_prontuario_seq.nextval
	into 	nr_log_seq_w
	from 	dual;

  IF (INSERTING) THEN
    select 	log_alteracao_prontuario_seq.nextval
    into 	nr_log_seq_w
    from 	dual;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       obter_desc_expressao(656665) ,
                       wheb_usuario_pck.get_nm_maquina,
                       nvl(obter_pessoa_atendimento(nvl(:new.nr_atendimento,:new.nr_atendimento_hosp),'C'), :new.cd_pessoa_fisica),
                       obter_desc_expressao(307861),
                       :new.nr_atendimento_hosp,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
  else
    ie_inativacao_w := 'N';

    if (:old.dt_inativacao is null) and (:new.dt_inativacao is not null) then
      ie_inativacao_w := 'S';
    end if;

    insert into log_alteracao_prontuario (nr_sequencia,
                       dt_atualizacao,
                       ds_evento,
                       ds_maquina,
                       cd_pessoa_fisica,
                       ds_item,
                       nr_atendimento,
                       dt_liberacao,
                       dt_inativacao,
                       nm_usuario) values
                       (nr_log_seq_w,
                       sysdate,
                       decode(ie_inativacao_w, 'N', obter_desc_expressao(302570) , obter_desc_expressao(331011) ),
                       wheb_usuario_pck.get_nm_maquina,
                       nvl(obter_pessoa_atendimento(nvl(:new.nr_atendimento,:new.nr_atendimento_hosp),'C'), :new.cd_pessoa_fisica),
                       obter_desc_expressao(307861),
                       :new.nr_atendimento_hosp,
                       :new.dt_liberacao,
                       :new.dt_inativacao,
                       nvl(wheb_usuario_pck.get_nm_usuario,:new.nm_usuario));
  end if;
end;
/


ALTER TABLE TASY.MED_RECEITA ADD (
  CONSTRAINT MEDRECE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          2M
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MED_RECEITA ADD (
  CONSTRAINT MEDRECE_ATESOAP_FK 
 FOREIGN KEY (NR_SEQ_SOAP) 
 REFERENCES TASY.ATENDIMENTO_SOAP (NR_SEQUENCIA),
  CONSTRAINT MEDRECE_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT MEDRECE_EHRREGI_FK 
 FOREIGN KEY (NR_SEQ_FORMULARIO) 
 REFERENCES TASY.EHR_REGISTRO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDRECE_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO_HOSP) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT MEDRECE_MEDATEN_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.MED_ATENDIMENTO (NR_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT MEDRECE_MEDCLIE_FK 
 FOREIGN KEY (NR_SEQ_CLIENTE) 
 REFERENCES TASY.MED_CLIENTE (NR_SEQUENCIA),
  CONSTRAINT MEDRECE_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MEDRECE_PESFISI_FK2 
 FOREIGN KEY (CD_MEDICO) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT MEDRECE_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL_ATIVO) 
 REFERENCES TASY.PERFIL (CD_PERFIL),
  CONSTRAINT MEDRECE_MEDRECE_FK 
 FOREIGN KEY (NR_SEQ_ORIGEM) 
 REFERENCES TASY.MED_RECEITA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT MEDRECE_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT MEDRECE_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT MEDRECE_PRESMED_FK 
 FOREIGN KEY (NR_PRESCRICAO) 
 REFERENCES TASY.PRESCR_MEDICA (NR_PRESCRICAO)
    ON DELETE CASCADE,
  CONSTRAINT MEDRECE_TEXPADR_FK 
 FOREIGN KEY (NR_SEQ_TEXTO) 
 REFERENCES TASY.TEXTO_PADRAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.MED_RECEITA TO NIVEL_1;

