ALTER TABLE TASY.PLS_INDICE_REAJUSTE
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_INDICE_REAJUSTE CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_INDICE_REAJUSTE
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  IE_INDICE_ANS        VARCHAR2(1 BYTE),
  DS_INDICE_REAJ       VARCHAR2(255 BYTE),
  NR_SEQ_MOEDA         NUMBER(10),
  IE_POOL_RISCO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLINDREAJ_MOEDA_FK_I ON TASY.PLS_INDICE_REAJUSTE
(NR_SEQ_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLINDREAJ_PK ON TASY.PLS_INDICE_REAJUSTE
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_INDICE_REAJUSTE ADD (
  CONSTRAINT PLINDREAJ_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_INDICE_REAJUSTE ADD (
  CONSTRAINT PLINDREAJ_MOEDA_FK 
 FOREIGN KEY (NR_SEQ_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA));

GRANT SELECT ON TASY.PLS_INDICE_REAJUSTE TO NIVEL_1;

