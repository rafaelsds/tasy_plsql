ALTER TABLE TASY.PESSOA_JUR_CONTA_NF
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PESSOA_JUR_CONTA_NF CASCADE CONSTRAINTS;

CREATE TABLE TASY.PESSOA_JUR_CONTA_NF
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  CD_CGC                VARCHAR2(14 BYTE)       NOT NULL,
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  CD_EMPRESA            NUMBER(4)               NOT NULL,
  CD_GRUPO_MATERIAL     NUMBER(3),
  CD_SUBGRUPO_MATERIAL  NUMBER(3),
  CD_CLASSE_MATERIAL    NUMBER(5),
  CD_MATERIAL           NUMBER(10),
  CD_CONTA_CONTABIL     VARCHAR2(20 BYTE)       NOT NULL,
  CD_PROCEDIMENTO       NUMBER(15),
  IE_ORIGEM_PROCED      NUMBER(10),
  CD_AREA_PROCEDIMENTO  NUMBER(15),
  CD_ESPECIALIDADE      NUMBER(15),
  CD_GRUPO_PROC         NUMBER(15),
  CD_OPERACAO_NF        NUMBER(4),
  DT_INICIO_VIGENCIA    DATE,
  DT_FIM_VIGENCIA       DATE,
  NR_SEQ_CLASSE_CP      NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PESJUCNF_AREPROC_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_AREA_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_AREPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_CLAMATE_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_CLASSE_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_CLAMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_CLTITPA_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(NR_SEQ_CLASSE_CP)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PESJUCNF_CONCONT_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_CONTA_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_CONCONT_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_EMPRESA_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_EMPRESA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_EMPRESA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_ESPPROC_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_ESPECIALIDADE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_ESPPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_GRUMATE_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_GRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_GRUMATE_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_GRUPROC_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_GRUPO_PROC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_GRUPROC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_MATERIA_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_OPENOTA_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_OPERACAO_NF)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_OPENOTA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_PESJURI_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_CGC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PESJUCNF_PK ON TASY.PESSOA_JUR_CONTA_NF
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_PK
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_PROCEDI_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_PROCEDIMENTO, IE_ORIGEM_PROCED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_PROCEDI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PESJUCNF_SUBMATE_FK_I ON TASY.PESSOA_JUR_CONTA_NF
(CD_SUBGRUPO_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PESJUCNF_SUBMATE_FK_I
  MONITORING USAGE;


ALTER TABLE TASY.PESSOA_JUR_CONTA_NF ADD (
  CONSTRAINT PESJUCNF_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PESSOA_JUR_CONTA_NF ADD (
  CONSTRAINT PESJUCNF_CLTITPA_FK 
 FOREIGN KEY (NR_SEQ_CLASSE_CP) 
 REFERENCES TASY.CLASSE_TITULO_PAGAR (NR_SEQUENCIA),
  CONSTRAINT PESJUCNF_EMPRESA_FK 
 FOREIGN KEY (CD_EMPRESA) 
 REFERENCES TASY.EMPRESA (CD_EMPRESA),
  CONSTRAINT PESJUCNF_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PESJUCNF_GRUMATE_FK 
 FOREIGN KEY (CD_GRUPO_MATERIAL) 
 REFERENCES TASY.GRUPO_MATERIAL (CD_GRUPO_MATERIAL),
  CONSTRAINT PESJUCNF_SUBMATE_FK 
 FOREIGN KEY (CD_SUBGRUPO_MATERIAL) 
 REFERENCES TASY.SUBGRUPO_MATERIAL (CD_SUBGRUPO_MATERIAL),
  CONSTRAINT PESJUCNF_CLAMATE_FK 
 FOREIGN KEY (CD_CLASSE_MATERIAL) 
 REFERENCES TASY.CLASSE_MATERIAL (CD_CLASSE_MATERIAL),
  CONSTRAINT PESJUCNF_CONCONT_FK 
 FOREIGN KEY (CD_CONTA_CONTABIL) 
 REFERENCES TASY.CONTA_CONTABIL (CD_CONTA_CONTABIL),
  CONSTRAINT PESJUCNF_PESJURI_FK 
 FOREIGN KEY (CD_CGC) 
 REFERENCES TASY.PESSOA_JURIDICA (CD_CGC),
  CONSTRAINT PESJUCNF_AREPROC_FK 
 FOREIGN KEY (CD_AREA_PROCEDIMENTO) 
 REFERENCES TASY.AREA_PROCEDIMENTO (CD_AREA_PROCEDIMENTO),
  CONSTRAINT PESJUCNF_ESPPROC_FK 
 FOREIGN KEY (CD_ESPECIALIDADE) 
 REFERENCES TASY.ESPECIALIDADE_PROC (CD_ESPECIALIDADE),
  CONSTRAINT PESJUCNF_GRUPROC_FK 
 FOREIGN KEY (CD_GRUPO_PROC) 
 REFERENCES TASY.GRUPO_PROC (CD_GRUPO_PROC),
  CONSTRAINT PESJUCNF_PROCEDI_FK 
 FOREIGN KEY (CD_PROCEDIMENTO, IE_ORIGEM_PROCED) 
 REFERENCES TASY.PROCEDIMENTO (CD_PROCEDIMENTO,IE_ORIGEM_PROCED),
  CONSTRAINT PESJUCNF_OPENOTA_FK 
 FOREIGN KEY (CD_OPERACAO_NF) 
 REFERENCES TASY.OPERACAO_NOTA (CD_OPERACAO_NF));

GRANT SELECT ON TASY.PESSOA_JUR_CONTA_NF TO NIVEL_1;

