ALTER TABLE TASY.CALENDARIO_JAP_IMPERIAL
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CALENDARIO_JAP_IMPERIAL CASCADE CONSTRAINTS;

CREATE TABLE TASY.CALENDARIO_JAP_IMPERIAL
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DS_ERA               VARCHAR2(10 BYTE),
  DT_INICIO            DATE,
  DT_FIM               DATE,
  DS_ABREVIACAO        VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CALJAPIMP_PK ON TASY.CALENDARIO_JAP_IMPERIAL
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CALENDARIO_JAP_IMPERIAL ADD (
  CONSTRAINT CALJAPIMP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

GRANT SELECT ON TASY.CALENDARIO_JAP_IMPERIAL TO NIVEL_1;

