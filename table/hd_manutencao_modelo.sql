ALTER TABLE TASY.HD_MANUTENCAO_MODELO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HD_MANUTENCAO_MODELO CASCADE CONSTRAINTS;

CREATE TABLE TASY.HD_MANUTENCAO_MODELO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_SEQ_MODELO        NUMBER(10)               NOT NULL,
  NR_SEQ_MANUTENCAO    NUMBER(10)               NOT NULL,
  IE_SITUACAO          VARCHAR2(1 BYTE)         NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HDMANMO_HDMOMAN_FK_I ON TASY.HD_MANUTENCAO_MODELO
(NR_SEQ_MANUTENCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDMANMO_HDMOMAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HDMANMO_HDMOMAQ_FK_I ON TASY.HD_MANUTENCAO_MODELO
(NR_SEQ_MODELO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDMANMO_HDMOMAQ_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.HDMANMO_PK ON TASY.HD_MANUTENCAO_MODELO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HDMANMO_PK
  MONITORING USAGE;


ALTER TABLE TASY.HD_MANUTENCAO_MODELO ADD (
  CONSTRAINT HDMANMO_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HD_MANUTENCAO_MODELO ADD (
  CONSTRAINT HDMANMO_HDMOMAN_FK 
 FOREIGN KEY (NR_SEQ_MANUTENCAO) 
 REFERENCES TASY.HD_MOTIVO_MAQ_MANUTENCAO (NR_SEQUENCIA),
  CONSTRAINT HDMANMO_HDMOMAQ_FK 
 FOREIGN KEY (NR_SEQ_MODELO) 
 REFERENCES TASY.HD_MODELO_MAQUINA (NR_SEQUENCIA));

GRANT SELECT ON TASY.HD_MANUTENCAO_MODELO TO NIVEL_1;

