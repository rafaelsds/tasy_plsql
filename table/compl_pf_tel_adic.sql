ALTER TABLE TASY.COMPL_PF_TEL_ADIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.COMPL_PF_TEL_ADIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.COMPL_PF_TEL_ADIC
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_PESSOA_FISICA        VARCHAR2(10 BYTE)     NOT NULL,
  NR_SEQ_COMPL            NUMBER(3)             NOT NULL,
  DT_ATUALIZACAO          DATE                  NOT NULL,
  NM_USUARIO              VARCHAR2(15 BYTE)     NOT NULL,
  DT_ATUALIZACAO_NREC     DATE,
  NM_USUARIO_NREC         VARCHAR2(15 BYTE),
  NR_TELEFONE             VARCHAR2(15 BYTE),
  IE_DIVULGACAO           VARCHAR2(1 BYTE)      NOT NULL,
  NR_DDD_TELEFONE         VARCHAR2(3 BYTE),
  DS_ENDERECO             VARCHAR2(40 BYTE),
  CD_CEP                  VARCHAR2(15 BYTE),
  NR_ENDERECO             NUMBER(5),
  DS_COMPLEMENTO          VARCHAR2(40 BYTE),
  DS_BAIRRO               VARCHAR2(40 BYTE),
  SG_ESTADO               VARCHAR2(15 BYTE),
  DS_OBSERVACAO           VARCHAR2(2000 BYTE),
  DS_EMAIL                VARCHAR2(255 BYTE),
  CD_EMPRESA_REFER        NUMBER(10),
  CD_PROFISSAO            NUMBER(10),
  CD_MUNICIPIO_IBGE       VARCHAR2(6 BYTE),
  NR_SEQ_PAIS             NUMBER(10),
  CD_TIPO_LOGRADOURO      VARCHAR2(3 BYTE),
  DS_MUNICIPIO            VARCHAR2(40 BYTE),
  NR_DDI_TELEFONE         VARCHAR2(3 BYTE),
  NR_SEQ_IDENT_CNES       NUMBER(10),
  NR_SEQ_LOCAL_ATEND_MED  NUMBER(10),
  NR_SEQ_REGIAO           NUMBER(10),
  IE_CORRESPONDENCIA      VARCHAR2(1 BYTE),
  IE_TELEFONE_PRINC       VARCHAR2(1 BYTE),
  IE_EMAIL_PRINC          VARCHAR2(1 BYTE),
  IE_SITUACAO             VARCHAR2(1 BYTE)      NOT NULL,
  NR_TELEFONE_CELULAR     VARCHAR2(40 BYTE),
  NR_DDD_CELULAR          VARCHAR2(3 BYTE),
  NR_DDI_TEL_CELULAR      VARCHAR2(3 BYTE),
  NR_SEQ_PESSOA_ENDERECO  NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.COPFTEA_CNESIDE_FK_I ON TASY.COMPL_PF_TEL_ADIC
(NR_SEQ_IDENT_CNES)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPFTEA_COMPEFI_FK_I ON TASY.COMPL_PF_TEL_ADIC
(CD_PESSOA_FISICA, NR_SEQ_COMPL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPFTEA_EMPREFE_FK_I ON TASY.COMPL_PF_TEL_ADIC
(CD_EMPRESA_REFER)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPFTEA_LOCATME_FK_I ON TASY.COMPL_PF_TEL_ADIC
(NR_SEQ_LOCAL_ATEND_MED)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPFTEA_PAIS_FK_I ON TASY.COMPL_PF_TEL_ADIC
(NR_SEQ_PAIS)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPFTEA_PESSEND_FK_I ON TASY.COMPL_PF_TEL_ADIC
(NR_SEQ_PESSOA_ENDERECO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.COPFTEA_PK ON TASY.COMPL_PF_TEL_ADIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPFTEA_PROFISS_FK_I ON TASY.COMPL_PF_TEL_ADIC
(CD_PROFISSAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPFTEA_SUSMUNI_FK_I ON TASY.COMPL_PF_TEL_ADIC
(CD_MUNICIPIO_IBGE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPFTEA_SUSMURE_FK_I ON TASY.COMPL_PF_TEL_ADIC
(NR_SEQ_REGIAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.COPFTEA_SUSTILO_FK_I ON TASY.COMPL_PF_TEL_ADIC
(CD_TIPO_LOGRADOURO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.COMPL_PF_TEL_ADIC_tp  after update ON TASY.COMPL_PF_TEL_ADIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_CEP,1,4000),substr(:new.CD_CEP,1,4000),:new.nm_usuario,nr_seq_w,'CD_CEP',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_ENDERECO,1,4000),substr(:new.DS_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'DS_ENDERECO',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_ENDERECO,1,4000),substr(:new.NR_ENDERECO,1,4000),:new.nm_usuario,nr_seq_w,'NR_ENDERECO',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_BAIRRO,1,4000),substr(:new.DS_BAIRRO,1,4000),:new.nm_usuario,nr_seq_w,'DS_BAIRRO',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_COMPLEMENTO,1,4000),substr(:new.DS_COMPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'DS_COMPLEMENTO',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_TIPO_LOGRADOURO,1,4000),substr(:new.CD_TIPO_LOGRADOURO,1,4000),:new.nm_usuario,nr_seq_w,'CD_TIPO_LOGRADOURO',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_EMAIL,1,4000),substr(:new.DS_EMAIL,1,4000),:new.nm_usuario,nr_seq_w,'DS_EMAIL',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.SG_ESTADO,1,4000),substr(:new.SG_ESTADO,1,4000),:new.nm_usuario,nr_seq_w,'SG_ESTADO',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_PAIS,1,4000),substr(:new.NR_SEQ_PAIS,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_PAIS',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PROFISSAO,1,4000),substr(:new.CD_PROFISSAO,1,4000),:new.nm_usuario,nr_seq_w,'CD_PROFISSAO',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_EMPRESA_REFER,1,4000),substr(:new.CD_EMPRESA_REFER,1,4000),:new.nm_usuario,nr_seq_w,'CD_EMPRESA_REFER',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_MUNICIPIO_IBGE,1,4000),substr(:new.CD_MUNICIPIO_IBGE,1,4000),:new.nm_usuario,nr_seq_w,'CD_MUNICIPIO_IBGE',ie_log_w,ds_w,'COMPL_PF_TEL_ADIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.compl_pf_tel_adic_pls_rps
after update ON TASY.COMPL_PF_TEL_ADIC for each row

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Finalidade:
-------------------------------------------------------------------------------------------------------------------
Locais de chamada direta:
[  ]  Objetos do dicion�rio [  ] Tasy (Delphi/Java) [  ] Portal [  ]  Relat�rios [ ] Outros:
 ------------------------------------------------------------------------------------------------------------------
Pontos de aten��o:
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
declare
ie_alterou_w 		varchar2(1) := 'N';
cd_cgc_cpf_old_w	pls_alt_prest_rps.cd_cgc_cpf_old%type;

begin

begin
	if	(nvl(:new.cd_municipio_ibge,'0')!= nvl(:old.cd_municipio_ibge,'0')) or
		(nvl(:new.sg_estado,'0')	!= nvl(:old.sg_estado,'0')) or
		(nvl(:new.ds_bairro,'0')	!= nvl(:old.ds_bairro,'0')) or
		(nvl(:new.ds_endereco,'0')	!= nvl(:old.ds_endereco,'0')) or
		(nvl(:new.ds_municipio,'0')	!= nvl(:old.ds_municipio,'0')) or
		(nvl(:new.nr_endereco,0)	!= nvl(:old.nr_endereco,0)) or
		(nvl(:new.cd_cep,'0')		!= nvl(:old.cd_cep,'0')) or
		(nvl(:new.nr_seq_ident_cnes,0)	!= nvl(:old.nr_seq_ident_cnes,0))then
		ie_alterou_w := 'S';
	end if;

	if	(ie_alterou_w = 'S') then
		select	max(nr_cpf)
		into	cd_cgc_cpf_old_w
		from	pessoa_fisica
		where	cd_pessoa_fisica = :new.cd_pessoa_fisica;

		pls_gerar_alt_prest_rps(null, :new.cd_pessoa_fisica, null, cd_cgc_cpf_old_w, :old.cd_municipio_ibge, null, :new.nm_usuario, :new.nr_sequencia, null, null);
	end if;
exception
when others then
	null;
end;

end;
/


ALTER TABLE TASY.COMPL_PF_TEL_ADIC ADD (
  CONSTRAINT COPFTEA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.COMPL_PF_TEL_ADIC ADD (
  CONSTRAINT COPFTEA_PESSEND_FK 
 FOREIGN KEY (NR_SEQ_PESSOA_ENDERECO) 
 REFERENCES TASY.PESSOA_ENDERECO (NR_SEQUENCIA),
  CONSTRAINT COPFTEA_COMPEFI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA, NR_SEQ_COMPL) 
 REFERENCES TASY.COMPL_PESSOA_FISICA (CD_PESSOA_FISICA,NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT COPFTEA_SUSTILO_FK 
 FOREIGN KEY (CD_TIPO_LOGRADOURO) 
 REFERENCES TASY.SUS_TIPO_LOGRADOURO (CD_TIPO_LOGRADOURO),
  CONSTRAINT COPFTEA_SUSMUNI_FK 
 FOREIGN KEY (CD_MUNICIPIO_IBGE) 
 REFERENCES TASY.SUS_MUNICIPIO (CD_MUNICIPIO_IBGE),
  CONSTRAINT COPFTEA_PAIS_FK 
 FOREIGN KEY (NR_SEQ_PAIS) 
 REFERENCES TASY.PAIS (NR_SEQUENCIA),
  CONSTRAINT COPFTEA_PROFISS_FK 
 FOREIGN KEY (CD_PROFISSAO) 
 REFERENCES TASY.PROFISSAO (CD_PROFISSAO),
  CONSTRAINT COPFTEA_EMPREFE_FK 
 FOREIGN KEY (CD_EMPRESA_REFER) 
 REFERENCES TASY.EMPRESA_REFERENCIA (CD_EMPRESA),
  CONSTRAINT COPFTEA_CNESIDE_FK 
 FOREIGN KEY (NR_SEQ_IDENT_CNES) 
 REFERENCES TASY.CNES_IDENTIFICACAO (NR_SEQUENCIA),
  CONSTRAINT COPFTEA_LOCATME_FK 
 FOREIGN KEY (NR_SEQ_LOCAL_ATEND_MED) 
 REFERENCES TASY.LOCAL_ATENDIMENTO_MEDICO (NR_SEQUENCIA),
  CONSTRAINT COPFTEA_SUSMURE_FK 
 FOREIGN KEY (NR_SEQ_REGIAO) 
 REFERENCES TASY.SUS_MUNICIPIO_REGIAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.COMPL_PF_TEL_ADIC TO NIVEL_1;

