ALTER TABLE TASY.HEM_ATC_QUANT
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.HEM_ATC_QUANT CASCADE CONSTRAINTS;

CREATE TABLE TASY.HEM_ATC_QUANT
(
  NR_SEQUENCIA              NUMBER(10)          NOT NULL,
  NR_SEQ_ATC                NUMBER(10),
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  QT_COMPRIMENTO_LESAO      NUMBER(15,1),
  QT_DIAMETRO_LUM_PRE       NUMBER(16,2),
  QT_DIAMETRO_REF_PROX_PRE  NUMBER(16,2),
  QT_DIAMETRO_REF_DIST_PRE  NUMBER(16,2),
  PR_ESTENOSE_PRE           NUMBER(16,2),
  QT_DIAMETRO_LUM_POS       NUMBER(16,2),
  QT_DIAMETRO_REF_PROX_POS  NUMBER(16,2),
  QT_DIAMETRO_REF_DIST_POS  NUMBER(16,2),
  PR_ESTENOSE_POS           NUMBER(16,2),
  NR_SEQ_CORONARIA          NUMBER(10),
  IE_QCA                    VARCHAR2(1 BYTE),
  NR_SEQ_LESAO              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.HEMATQT_HEM_ATC_FK_I ON TASY.HEM_ATC_QUANT
(NR_SEQ_ATC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HEMATQT_HEM_ATC_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.HEMATQT_HEMCORO_FK_I ON TASY.HEM_ATC_QUANT
(NR_SEQ_CORONARIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.HEMATQT_HEMLESA_FK_I ON TASY.HEM_ATC_QUANT
(NR_SEQ_LESAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.HEMATQT_PK ON TASY.HEM_ATC_QUANT
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.HEMATQT_PK
  MONITORING USAGE;


ALTER TABLE TASY.HEM_ATC_QUANT ADD (
  CONSTRAINT HEMATQT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.HEM_ATC_QUANT ADD (
  CONSTRAINT HEMATQT_HEMLESA_FK 
 FOREIGN KEY (NR_SEQ_LESAO) 
 REFERENCES TASY.HEM_LESAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HEMATQT_HEMCORO_FK 
 FOREIGN KEY (NR_SEQ_CORONARIA) 
 REFERENCES TASY.HEM_CORONARIOGRAFIA (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT HEMATQT_HEM_ATC_FK 
 FOREIGN KEY (NR_SEQ_ATC) 
 REFERENCES TASY.HEM_ATC (NR_SEQUENCIA));

GRANT SELECT ON TASY.HEM_ATC_QUANT TO NIVEL_1;

