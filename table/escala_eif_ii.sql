ALTER TABLE TASY.ESCALA_EIF_II
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_EIF_II CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_EIF_II
(
  NR_SEQUENCIA               NUMBER(10)         NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  DT_ATUALIZACAO_NREC        DATE,
  NM_USUARIO_NREC            VARCHAR2(15 BYTE),
  NR_ATENDIMENTO             NUMBER(10)         NOT NULL,
  IE_SITUACAO                VARCHAR2(1 BYTE)   NOT NULL,
  DT_LIBERACAO               DATE,
  DT_INATIVACAO              DATE,
  NM_USUARIO_INATIVACAO      VARCHAR2(15 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(255 BYTE),
  NR_SEQ_ESCALA              NUMBER(10)         NOT NULL,
  CD_PROFISSIONAL            VARCHAR2(10 BYTE)  NOT NULL,
  QT_PONTOS                  NUMBER(15,4),
  DT_AVALIACAO               DATE               NOT NULL,
  IE_NIVEL_ATENCAO           VARCHAR2(1 BYTE),
  NR_SEQ_ESCUTA              NUMBER(10),
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_ASSINAT_INATIVACAO  NUMBER(10),
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  NR_SEQ_REG_ELEMENTO        NUMBER(10),
  NR_SEQ_FERIDA              NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCEIII_ATCONSPEPA_FK_I ON TASY.ESCALA_EIF_II
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEIII_ATEPACI_FK_I ON TASY.ESCALA_EIF_II
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEIII_ATESINI_FK_I ON TASY.ESCALA_EIF_II
(NR_SEQ_ESCUTA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEIII_CURFERI_FK_I ON TASY.ESCALA_EIF_II
(NR_SEQ_FERIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEIII_EHRREEL_FK_I ON TASY.ESCALA_EIF_II
(NR_SEQ_REG_ELEMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEIII_EIFESII_FK_I ON TASY.ESCALA_EIF_II
(NR_SEQ_ESCALA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCEIII_EIFESII_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.ESCEIII_PESFISI_FK_I ON TASY.ESCALA_EIF_II
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCEIII_PK ON TASY.ESCALA_EIF_II
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCEIII_PK
  MONITORING USAGE;


CREATE INDEX TASY.ESCEIII_TASASDI_FK_I ON TASY.ESCALA_EIF_II
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCEIII_TASASDI_FK2_I ON TASY.ESCALA_EIF_II
(NR_SEQ_ASSINAT_INATIVACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.hsj_escala_eif_ii_upd
BEFORE INSERT OR UPDATE ON TASY.ESCALA_EIF_II FOR EACH ROW
declare

ie_start_medico_w varchar2(1);
ds_evolucao_w long;
ds_enter_w varchar2(255):='

';
ds_espaco_w varchar2(255):='
';

cursor c_regras is
    with regras as
    (
        select  c.nr_seq_item_inf nr_seq_item,
                b.nr_seq_escala
        from escala_eif_ii_item b 
        join eif_escala_ii_item_regra c on(c.nr_seq_escala = :new.nr_seq_escala and c.nr_seq_item_sup  = b.nr_seq_item and b.nr_seq_result = c.nr_seq_result_item)
        where :new.nr_seq_escala = 4 --ESCALA LAPSS
        and b.nr_seq_escala = :new.nr_sequencia
    )
    select w.nr_sequencia
    from regras r
    join escala_eif_ii_item w on(w.nr_seq_escala = r.nr_seq_escala and w.nr_seq_item = r.nr_seq_item)
    where nvl(hsj_obter_pont_result_score_ii(w.nr_seq_result),0) != 0;


cursor c_itens_eif is
    select  substr(obter_desc_score_item_2(nr_seq_item),1,255)ds_item,
            substr(obter_desc_score_item_res_2(NR_SEQ_RESULT),1,255)ds_resposta
    from ESCALA_EIF_II_ITEM
    where nr_seq_escala = :new.nr_sequencia
    and (nvl(hsj_obter_pont_result_score_ii(nr_seq_result),77) > 0 or nr_seq_item in (34, 35))
    Order by obter_seq_score_item_2(nr_seq_item),substr(obter_desc_score_item_2(nr_seq_item),1,255);
        
    
    Procedure hsj_gerar_result_score_flex_2(nr_sequencia_p number)is

    qt_total_w number(15,4);

    begin

        select     sum(b.qt_pontuacao)
        into    qt_total_w
        from       escala_eif_ii_item a,
            eif_escala_ii_item_result b
        where   a.nr_seq_escala = nr_sequencia_p
        and    a.NR_SEQ_RESULT = b.nr_sequencia;

        :new.qt_pontos := qt_total_w;

    end hsj_gerar_result_score_flex_2;

    
begin
  
   if :new.nr_seq_escala = 4 then  --ESCALA LAPSS
      
        for v_regras in c_regras loop

            update escala_eif_ii_item set nr_seq_result = null
            where nr_sequencia = v_regras.nr_sequencia;
            
            hsj_gerar_result_score_flex_2(:new.nr_sequencia);
                
        end loop;


      if :new.dt_liberacao is not null and :old.dt_liberacao is null then  -- Gerar evolu��o ap�s liberar escala de lapss e esta ter atingido pontua��o para start m�dico
        
            select nvl(max('S'),'N')
            into ie_start_medico_w
            from dual
            where NVL(:new.qt_pontos, 0) between hsj_obter_resu_scor_flex_2(:new.nr_seq_escala, 'PMI', 12)
                                 and  hsj_obter_resu_scor_flex_2(:new.nr_seq_escala, 'PMA', 12);
            
            if ie_start_medico_w = 'S' then
                            
                ds_evolucao_w:= 'Evolu��o Protocolo AVC - Enfermeiro';
                
                for v_itens_eif in c_itens_eif loop                      
                
                    if substr(v_itens_eif.ds_item,1,1) != ' 'then
                        ds_evolucao_w:= ds_evolucao_w ||ds_enter_w;
                    else
                        ds_evolucao_w:= ds_evolucao_w ||ds_espaco_w;        
                    end if;
                
                    ds_evolucao_w:= ds_evolucao_w || v_itens_eif.ds_item;
                
                    if v_itens_eif.ds_resposta is not null then
                        ds_evolucao_w:= ds_evolucao_w ||'  -  '|| 'R: ' ||v_itens_eif.ds_resposta;
                    end if;
                
                end loop;

                hsj_gerar_evolucao(Obter_dados_usuario_opcao(:new.nm_usuario ,'F'), :new.nr_atendimento, ds_evolucao_w, 'AVC', :new.nm_usuario);

            end if;
      end if;

   end if;

end;
/


ALTER TABLE TASY.ESCALA_EIF_II ADD (
  CONSTRAINT ESCEIII_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_EIF_II ADD (
  CONSTRAINT ESCEIII_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCEIII_EIFESII_FK 
 FOREIGN KEY (NR_SEQ_ESCALA) 
 REFERENCES TASY.EIF_ESCALA_II (NR_SEQUENCIA),
  CONSTRAINT ESCEIII_PESFISI_FK 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT ESCEIII_ATESINI_FK 
 FOREIGN KEY (NR_SEQ_ESCUTA) 
 REFERENCES TASY.ATEND_ESCUTA_INICIAL (NR_SEQUENCIA),
  CONSTRAINT ESCEIII_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCEIII_TASASDI_FK2 
 FOREIGN KEY (NR_SEQ_ASSINAT_INATIVACAO) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT ESCEIII_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT ESCEIII_EHRREEL_FK 
 FOREIGN KEY (NR_SEQ_REG_ELEMENTO) 
 REFERENCES TASY.EHR_REG_ELEMENTO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT ESCEIII_CURFERI_FK 
 FOREIGN KEY (NR_SEQ_FERIDA) 
 REFERENCES TASY.CUR_FERIDA (NR_SEQUENCIA)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.ESCALA_EIF_II TO NIVEL_1;

