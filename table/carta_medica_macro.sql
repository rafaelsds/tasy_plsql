ALTER TABLE TASY.CARTA_MEDICA_MACRO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.CARTA_MEDICA_MACRO CASCADE CONSTRAINTS;

CREATE TABLE TASY.CARTA_MEDICA_MACRO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NM_MACRO             VARCHAR2(50 BYTE)        NOT NULL,
  NM_ATRIBUTO          VARCHAR2(50 BYTE)        NOT NULL,
  DS_MACRO             VARCHAR2(255 BYTE)       NOT NULL,
  DS_SQL               VARCHAR2(1000 BYTE),
  IE_ORIGEM            VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.CARMEDMAC_PK ON TASY.CARTA_MEDICA_MACRO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.CARTA_MEDICA_MACRO ADD (
  CONSTRAINT CARMEDMAC_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

