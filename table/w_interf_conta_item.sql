ALTER TABLE TASY.W_INTERF_CONTA_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_INTERF_CONTA_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_INTERF_CONTA_ITEM
(
  NR_SEQUENCIA            NUMBER(10)            NOT NULL,
  CD_TIPO_REGISTRO        NUMBER(3)             NOT NULL,
  NR_SEQ_REGISTRO         NUMBER(10)            NOT NULL,
  NR_SEQ_INTERFACE        NUMBER(10),
  NR_REMESSA              NUMBER(10),
  NR_ATENDIMENTO          NUMBER(10),
  NR_INTERNO_CONTA        NUMBER(10),
  NR_SEQ_ITEM             NUMBER(10),
  IE_TIPO_ITEM            NUMBER(1),
  CD_ITEM                 NUMBER(15),
  IE_ORIGEM_PROCED        NUMBER(10),
  CD_ITEM_CONVENIO        VARCHAR2(20 BYTE),
  DS_ITEM                 VARCHAR2(240 BYTE),
  QT_ITEM                 NUMBER(15,4),
  CD_UNIDADE_MEDIDA       VARCHAR2(30 BYTE),
  DT_ITEM                 DATE,
  VL_UNITARIO_ITEM        NUMBER(15,4),
  VL_TOTAL_ITEM           NUMBER(15,2),
  QT_CH_ITEM              NUMBER(5),
  VL_UNITARIO_CH          NUMBER(15,4),
  CD_MEDICO_EXECUTOR      VARCHAR2(10 BYTE),
  CD_MEDICO_EXEC_CONV     VARCHAR2(15 BYTE),
  NM_MEDICO_EXECUTOR      VARCHAR2(60 BYTE),
  NR_CRM_EXECUTOR         VARCHAR2(20 BYTE),
  UF_CRM_EXECUTOR         VARCHAR2(2 BYTE),
  NR_CPF_EXECUTOR         VARCHAR2(11 BYTE),
  CD_FUNCAO_EXECUTOR      NUMBER(3),
  NR_PORTE_ANESTESICO     NUMBER(2),
  PR_FUNCAO_PARTICIPANTE  NUMBER(7,4),
  PR_VIA_ACESSO           NUMBER(7,4),
  PR_HORA_EXTRA           NUMBER(7,4),
  IE_VIA_ACESSO           VARCHAR2(1 BYTE),
  CD_CLASSIF_SETOR        VARCHAR2(2 BYTE),
  CD_SETOR_ATENDIMENTO    NUMBER(5),
  CD_GRUPO_GASTO          VARCHAR2(10 BYTE),
  CD_BRASINDICE           VARCHAR2(16 BYTE),
  CD_PROCEDIMENTO_REF     NUMBER(15),
  CD_PROC_REF_CONV        VARCHAR2(20 BYTE),
  NR_SEQ_PROTOCOLO        NUMBER(10),
  CD_CONVENIO             NUMBER(5),
  CD_CGC_HOSPITAL         VARCHAR2(14 BYTE),
  CD_CGC_CONVENIO         VARCHAR2(14 BYTE),
  CD_INTERNO              VARCHAR2(15 BYTE),
  IE_RESPONSAVEL_CREDITO  VARCHAR2(5 BYTE),
  IE_EMITE_CONTA          VARCHAR2(3 BYTE),
  IE_TOTAL_INTERF         NUMBER(2),
  VL_HONORARIO            NUMBER(15,2),
  NR_SEQ_PROC_PACOTE      NUMBER(10),
  CD_ESPECIALIDADE        NUMBER(5),
  CD_FUNCAO_ESPEC_MED     NUMBER(8),
  DT_PRESCRICAO           DATE,
  DT_AUTORIZACAO          DATE,
  CD_CGC_CPF_RESP_CRED    VARCHAR2(14 BYTE),
  VL_FILME                NUMBER(15,2),
  NR_DOC_CONVENIO         VARCHAR2(20 BYTE),
  NR_PRESCRICAO           NUMBER(14),
  CD_MEDICO_REQ           VARCHAR2(10 BYTE),
  NR_CRM_REQUISITANTE     VARCHAR2(20 BYTE),
  CD_CGC_PRESTADOR        VARCHAR2(14 BYTE),
  VL_CUSTO_OPER           NUMBER(15,2),
  QT_FILME                NUMBER(15,4),
  IE_VIDEO                VARCHAR2(1 BYTE),
  CD_SIMPRO               NUMBER(15),
  NR_FOLHA                NUMBER(15),
  NR_LINHA                NUMBER(15),
  CD_TIPO_ITEM_INTERF     NUMBER(2),
  CD_AREA_PROC            NUMBER(15),
  CD_ESPECIALIDADE_PROC   NUMBER(15),
  CD_GRUPO_PROC           NUMBER(15),
  CD_GRUPO_MAT            NUMBER(3),
  CD_SUBGRUPO_MAT         NUMBER(3),
  CD_CLASSE_MAT           NUMBER(5),
  CD_TIPO_PROCEDIMENTO    NUMBER(3),
  CD_SENHA_GUIA           VARCHAR2(20 BYTE),
  DT_ENTRADA_UNIDADE      DATE,
  DT_SAIDA_UNIDADE        DATE,
  NM_USUARIO              VARCHAR2(15 BYTE),
  NR_SEQ_PROC_PRINC       NUMBER(10),
  VL_MATMED_PROC_PRINC    NUMBER(15,2),
  IE_VIA_ACESSO_INF       VARCHAR2(1 BYTE),
  NR_SEQ_CONTA_CONVENIO   NUMBER(15),
  PR_FATURADO             NUMBER(15,4),
  VL_ORIGINAL             NUMBER(15,2),
  CD_PROCEDIMENTO_TUSS    NUMBER(15),
  QT_ITEM_CONV_DIV        NUMBER(15,4),
  CD_CGC_PARTIC           VARCHAR2(14 BYTE),
  NR_SEQ_CBO_EXECUTOR     NUMBER(10),
  SG_CONS_PROF_EXEC       VARCHAR2(10 BYTE),
  CD_CGC_ESTAB_EXECUTOR   VARCHAR2(14 BYTE),
  CD_CNS_ESTAB_EXECUTOR   VARCHAR2(20 BYTE),
  TX_ITEM                 NUMBER(15,4),
  NR_SEQ_PROC_CRIT_HOR    NUMBER(10)
)
TABLESPACE TASY_WORK
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          11M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WINCOIT_I1 ON TASY.W_INTERF_CONTA_ITEM
(NR_INTERNO_CONTA)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINCOIT_I1
  MONITORING USAGE;


CREATE INDEX TASY.WINCOIT_I2 ON TASY.W_INTERF_CONTA_ITEM
(NR_SEQ_PROTOCOLO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINCOIT_I2
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.WINCOIT_PK ON TASY.W_INTERF_CONTA_ITEM
(NR_SEQUENCIA)
NOLOGGING
TABLESPACE TASY_WORK
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          384K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.WINCOIT_PK
  MONITORING USAGE;


ALTER TABLE TASY.W_INTERF_CONTA_ITEM ADD (
  CONSTRAINT WINCOIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_WORK
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          384K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.W_INTERF_CONTA_ITEM TO NIVEL_1;

