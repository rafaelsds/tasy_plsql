ALTER TABLE TASY.MAN_HORARIO_TRABALHO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MAN_HORARIO_TRABALHO CASCADE CONSTRAINTS;

CREATE TABLE TASY.MAN_HORARIO_TRABALHO
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  IE_DIA_SEMANA        VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  HR_INICIO            VARCHAR2(4 BYTE)         NOT NULL,
  HR_FIM               VARCHAR2(4 BYTE)         NOT NULL,
  QT_MIN_INTERVALO     NUMBER(5),
  QT_MIN_LANCHE        NUMBER(5),
  HR_INICIAL           DATE,
  HR_FINAL             DATE,
  DT_FIM               DATE,
  DT_INICIO            DATE
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.MANHTRA_PK ON TASY.MAN_HORARIO_TRABALHO
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.man_horario_trabalho_atual
before insert or update ON TASY.MAN_HORARIO_TRABALHO for each row
declare

begin

begin
if	(nvl(:old.hr_inicio,'X') <> nvl(:new.hr_inicio,'X')) then
	if	(:new.hr_inicio is not null) then
		:new.hr_inicial := pkg_date_utils.get_Time(sysdate,:new.hr_inicio);
	else
		:new.hr_inicial := null;
	end if;
elsif	(nvl(:old.hr_inicial,pkg_date_utils.get_Time('')) <> nvl(:new.hr_inicial,pkg_date_utils.get_Time(''))) then
	:new.hr_inicio := lpad(pkg_date_utils.extract_field('HOUR',:new.hr_inicial),2,0) || lpad(pkg_date_utils.extract_field('MINUTE',:new.hr_inicial),2,0);
end if;

if	(nvl(:old.hr_fim,'X') <> nvl(:new.hr_fim,'X')) then
	if	(:new.hr_fim is not null) then
		:new.hr_final := pkg_date_utils.get_Time(sysdate,:new.hr_fim);
	else
		:new.hr_final := null;
	end if;
elsif	(nvl(:old.hr_final,pkg_date_utils.get_Time('')) <> nvl(:new.hr_final,pkg_date_utils.get_Time(''))) then
	:new.hr_fim := lpad(pkg_date_utils.extract_field('HOUR',:new.hr_final),2,0) || lpad(pkg_date_utils.extract_field('MINUTE',:new.hr_final),2,0);
end if;
exception
when others then
	null;
end;

end;
/


ALTER TABLE TASY.MAN_HORARIO_TRABALHO ADD (
  CONSTRAINT MANHTRA_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT SELECT ON TASY.MAN_HORARIO_TRABALHO TO NIVEL_1;

