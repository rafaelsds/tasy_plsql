DROP TABLE TASY.AGEWEB_HORARIOS CASCADE CONSTRAINTS;

CREATE TABLE TASY.AGEWEB_HORARIOS
(
  CD_PESSOA_FISICA  VARCHAR2(10 BYTE)           NOT NULL,
  CD_TIPO_AGENDA    NUMBER(10)                  NOT NULL,
  DT_AGENDA         DATE                        NOT NULL,
  CD_AGENDA         NUMBER(10)                  NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.AGWEBHR_AGENDA_FK_I ON TASY.AGEWEB_HORARIOS
(CD_AGENDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.AGWEBHR_AGENDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.AGWEBHR_PESFISI_FK_I ON TASY.AGEWEB_HORARIOS
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.AGEWEB_HORARIOS ADD (
  CONSTRAINT AGWEBHR_AGENDA_FK 
 FOREIGN KEY (CD_AGENDA) 
 REFERENCES TASY.AGENDA (CD_AGENDA),
  CONSTRAINT AGWEBHR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.AGEWEB_HORARIOS TO NIVEL_1;

