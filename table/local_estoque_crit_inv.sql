ALTER TABLE TASY.LOCAL_ESTOQUE_CRIT_INV
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.LOCAL_ESTOQUE_CRIT_INV CASCADE CONSTRAINTS;

CREATE TABLE TASY.LOCAL_ESTOQUE_CRIT_INV
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  CD_ESTABELECIMENTO   NUMBER(4)                NOT NULL,
  CD_LOCAL_ESTOQUE     NUMBER(4)                NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DS_CRITERIO          VARCHAR2(50 BYTE)        NOT NULL,
  DS_OBSERVACAO        VARCHAR2(255 BYTE),
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.LOCESCI_LOCESTO_FK_I ON TASY.LOCAL_ESTOQUE_CRIT_INV
(CD_LOCAL_ESTOQUE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.LOCESCI_PK ON TASY.LOCAL_ESTOQUE_CRIT_INV
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.LOCAL_ESTOQUE_CRIT_INV_tp  after update ON TASY.LOCAL_ESTOQUE_CRIT_INV FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.CD_LOCAL_ESTOQUE,1,4000),substr(:new.CD_LOCAL_ESTOQUE,1,4000),:new.nm_usuario,nr_seq_w,'CD_LOCAL_ESTOQUE',ie_log_w,ds_w,'LOCAL_ESTOQUE_CRIT_INV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_OBSERVACAO,1,4000),substr(:new.DS_OBSERVACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_OBSERVACAO',ie_log_w,ds_w,'LOCAL_ESTOQUE_CRIT_INV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_CRITERIO,1,4000),substr(:new.DS_CRITERIO,1,4000),:new.nm_usuario,nr_seq_w,'DS_CRITERIO',ie_log_w,ds_w,'LOCAL_ESTOQUE_CRIT_INV',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_ESTABELECIMENTO,1,4000),substr(:new.CD_ESTABELECIMENTO,1,4000),:new.nm_usuario,nr_seq_w,'CD_ESTABELECIMENTO',ie_log_w,ds_w,'LOCAL_ESTOQUE_CRIT_INV',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.LOCAL_ESTOQUE_CRIT_INV ADD (
  CONSTRAINT LOCESCI_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.LOCAL_ESTOQUE_CRIT_INV ADD (
  CONSTRAINT LOCESCI_LOCESTO_FK 
 FOREIGN KEY (CD_LOCAL_ESTOQUE) 
 REFERENCES TASY.LOCAL_ESTOQUE (CD_LOCAL_ESTOQUE)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.LOCAL_ESTOQUE_CRIT_INV TO NIVEL_1;

