ALTER TABLE TASY.VIA_APLICACAO
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.VIA_APLICACAO CASCADE CONSTRAINTS;

CREATE TABLE TASY.VIA_APLICACAO
(
  IE_VIA_APLICACAO          VARCHAR2(5 BYTE)    NOT NULL,
  DT_ATUALIZACAO            DATE                NOT NULL,
  NM_USUARIO                VARCHAR2(15 BYTE)   NOT NULL,
  DS_VIA_APLICACAO          VARCHAR2(80 BYTE)   NOT NULL,
  IE_SITUACAO               VARCHAR2(1 BYTE)    NOT NULL,
  IE_GERA_DILUICAO          VARCHAR2(1 BYTE)    NOT NULL,
  DT_ATUALIZACAO_NREC       DATE,
  NM_USUARIO_NREC           VARCHAR2(15 BYTE),
  NR_IMPRESSAO              NUMBER(6),
  IE_ENDOVENOSA             VARCHAR2(1 BYTE),
  NR_SEQ_VIA_ANVISA         NUMBER(10),
  IE_SOLUCAO                VARCHAR2(1 BYTE),
  IE_VIA_SONDA              VARCHAR2(1 BYTE),
  IE_DIETA                  VARCHAR2(1 BYTE),
  IE_SUPLEMENTO             VARCHAR2(1 BYTE),
  IE_SNE                    VARCHAR2(1 BYTE),
  IE_GOTEJAMENTO            VARCHAR2(1 BYTE),
  CD_SISTEMA_ANTERIOR       VARCHAR2(255 BYTE),
  IE_TOPO_LADO_ADM          VARCHAR2(1 BYTE),
  IE_FORMATO_ADMINISTRACAO  VARCHAR2(2 BYTE),
  QT_PRESC_VAL_DAYS         NUMBER(5),
  IE_INJECTION              VARCHAR2(1 BYTE),
  IE_ADESIVO                VARCHAR2(1 BYTE),
  SI_DRIP_SHOT              VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.VIAAPLI_ANVVIAD_FK_I ON TASY.VIA_APLICACAO
(NR_SEQ_VIA_ANVISA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.VIAAPLI_PK ON TASY.VIA_APLICACAO
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.VIA_APLICACAO_tp  after update ON TASY.VIA_APLICACAO FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.IE_VIA_APLICACAO);  ds_c_w:=null;gravar_log_alteracao(substr(:old.IE_SITUACAO,1,4000),substr(:new.IE_SITUACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SITUACAO',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.DS_VIA_APLICACAO,1,4000),substr(:new.DS_VIA_APLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'DS_VIA_APLICACAO',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VIA_APLICACAO,1,4000),substr(:new.IE_VIA_APLICACAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIA_APLICACAO',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_GERA_DILUICAO,1,4000),substr(:new.IE_GERA_DILUICAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_GERA_DILUICAO',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_ENDOVENOSA,1,4000),substr(:new.IE_ENDOVENOSA,1,4000),:new.nm_usuario,nr_seq_w,'IE_ENDOVENOSA',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SNE,1,4000),substr(:new.IE_SNE,1,4000),:new.nm_usuario,nr_seq_w,'IE_SNE',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_IMPRESSAO,1,4000),substr(:new.NR_IMPRESSAO,1,4000),:new.nm_usuario,nr_seq_w,'NR_IMPRESSAO',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SOLUCAO,1,4000),substr(:new.IE_SOLUCAO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SOLUCAO',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VIA_SONDA,1,4000),substr(:new.IE_VIA_SONDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_VIA_SONDA',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DIETA,1,4000),substr(:new.IE_DIETA,1,4000),:new.nm_usuario,nr_seq_w,'IE_DIETA',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_SUPLEMENTO,1,4000),substr(:new.IE_SUPLEMENTO,1,4000),:new.nm_usuario,nr_seq_w,'IE_SUPLEMENTO',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_VIA_ANVISA,1,4000),substr(:new.NR_SEQ_VIA_ANVISA,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_VIA_ANVISA',ie_log_w,ds_w,'VIA_APLICACAO',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.VIA_APLICACAO ADD (
  CONSTRAINT VIAAPLI_PK
 PRIMARY KEY
 (IE_VIA_APLICACAO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.VIA_APLICACAO ADD (
  CONSTRAINT VIAAPLI_ANVVIAD_FK 
 FOREIGN KEY (NR_SEQ_VIA_ANVISA) 
 REFERENCES TASY.ANVISA_VIA_ADMINISTRACAO (NR_SEQUENCIA));

GRANT SELECT ON TASY.VIA_APLICACAO TO NIVEL_1;

