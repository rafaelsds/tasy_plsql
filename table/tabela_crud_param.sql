ALTER TABLE TASY.TABELA_CRUD_PARAM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TABELA_CRUD_PARAM CASCADE CONSTRAINTS;

CREATE TABLE TASY.TABELA_CRUD_PARAM
(
  NR_SEQUENCIA                NUMBER(10)        NOT NULL,
  CD_ESTABELECIMENTO          NUMBER(4),
  DT_ATUALIZACAO              DATE              NOT NULL,
  NM_USUARIO                  VARCHAR2(15 BYTE) NOT NULL,
  DT_ATUALIZACAO_NREC         DATE,
  NM_USUARIO_NREC             VARCHAR2(15 BYTE),
  CD_PERFIL                   NUMBER(5),
  NM_USUARIO_PARAM            VARCHAR2(15 BYTE),
  DS_OBSERVACAO               VARCHAR2(4000 BYTE),
  NM_TABELA                   VARCHAR2(30 BYTE) NOT NULL,
  NR_SEQ_VISAO                NUMBER(10),
  NR_SEQ_OBJ_SCHEMATIC        NUMBER(10),
  IE_INSERT                   VARCHAR2(2 BYTE),
  IE_UPDATE                   VARCHAR2(2 BYTE),
  IE_DELETE                   VARCHAR2(2 BYTE),
  IE_DUPLICAR                 VARCHAR2(2 BYTE),
  IE_APLICAR_FILHOS           VARCHAR2(1 BYTE),
  IE_INATIVAR                 VARCHAR2(1 BYTE),
  NR_SEQ_LOTE_PAR_REGRA_ITEM  NUMBER(10),
  IE_VISUALIZA_INATIVOS       VARCHAR2(150 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TABCPAR_ESTABEL_FK_I ON TASY.TABELA_CRUD_PARAM
(CD_ESTABELECIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABCPAR_OBJSCHE_FK_I ON TASY.TABELA_CRUD_PARAM
(NR_SEQ_OBJ_SCHEMATIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABCPAR_PERFIL_FK_I ON TASY.TABELA_CRUD_PARAM
(CD_PERFIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TABCPAR_PK ON TASY.TABELA_CRUD_PARAM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABCPAR_TABSIST_FK_I ON TASY.TABELA_CRUD_PARAM
(NM_TABELA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABCPAR_TABVISA_FK_I ON TASY.TABELA_CRUD_PARAM
(NR_SEQ_VISAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TABCPAR_USUARIO_FK_I ON TASY.TABELA_CRUD_PARAM
(NM_USUARIO_PARAM)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.TABELA_CRUD_PARAM_tp  after update ON TASY.TABELA_CRUD_PARAM FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NM_USUARIO_PARAM,1,4000),substr(:new.NM_USUARIO_PARAM,1,4000),:new.nm_usuario,nr_seq_w,'NM_USUARIO_PARAM',ie_log_w,ds_w,'TABELA_CRUD_PARAM',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_PERFIL,1,4000),substr(:new.CD_PERFIL,1,4000),:new.nm_usuario,nr_seq_w,'CD_PERFIL',ie_log_w,ds_w,'TABELA_CRUD_PARAM',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


CREATE OR REPLACE TRIGGER TASY.tabela_crud_param_atual
before insert or update ON TASY.TABELA_CRUD_PARAM for each row
declare

qt_records_w	number(10);

--Please, be careful
pragma autonomous_transaction;

begin

if	(wheb_usuario_pck.get_ie_executar_trigger = 'S')  then

	select	count(*)
	into	qt_records_w
	from	tabela_crud_param
	where	nm_tabela		= :new.nm_tabela
	and	nr_seq_visao		= :new.nr_seq_visao
	and	nr_seq_obj_schematic	= :new.nr_seq_obj_schematic
	and	((cd_estabelecimento	= :new.cd_estabelecimento)	or (cd_estabelecimento is null 	and :new.cd_estabelecimento is null))
	and	((cd_perfil			= :new.cd_perfil) 		or (cd_perfil is null 		and :new.cd_perfil is null))
	and	((nm_usuario_param		= :new.nm_usuario_param) 	or (nm_usuario_param is null 	and :new.nm_usuario_param is null))
	and	nr_sequencia 		<> :new.nr_sequencia;

	if (qt_records_w > 0) then

		--J� existe uma regra cadastrada com esses atributos.
		wheb_mensagem_pck.exibir_mensagem_abort(991975);

	end if;

end if;

end;
/


ALTER TABLE TASY.TABELA_CRUD_PARAM ADD (
  CONSTRAINT TABCPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TABELA_CRUD_PARAM ADD (
  CONSTRAINT TABCPAR_ESTABEL_FK 
 FOREIGN KEY (CD_ESTABELECIMENTO) 
 REFERENCES TASY.ESTABELECIMENTO (CD_ESTABELECIMENTO),
  CONSTRAINT TABCPAR_OBJSCHE_FK 
 FOREIGN KEY (NR_SEQ_OBJ_SCHEMATIC) 
 REFERENCES TASY.OBJETO_SCHEMATIC (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TABCPAR_PERFIL_FK 
 FOREIGN KEY (CD_PERFIL) 
 REFERENCES TASY.PERFIL (CD_PERFIL)
    ON DELETE CASCADE,
  CONSTRAINT TABCPAR_TABSIST_FK 
 FOREIGN KEY (NM_TABELA) 
 REFERENCES TASY.TABELA_SISTEMA (NM_TABELA)
    ON DELETE CASCADE,
  CONSTRAINT TABCPAR_TABVISA_FK 
 FOREIGN KEY (NR_SEQ_VISAO) 
 REFERENCES TASY.TABELA_VISAO (NR_SEQUENCIA)
    ON DELETE CASCADE,
  CONSTRAINT TABCPAR_USUARIO_FK 
 FOREIGN KEY (NM_USUARIO_PARAM) 
 REFERENCES TASY.USUARIO (NM_USUARIO)
    ON DELETE CASCADE);

GRANT SELECT ON TASY.TABELA_CRUD_PARAM TO NIVEL_1;

