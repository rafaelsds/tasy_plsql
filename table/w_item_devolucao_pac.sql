ALTER TABLE TASY.W_ITEM_DEVOLUCAO_PAC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.W_ITEM_DEVOLUCAO_PAC CASCADE CONSTRAINTS;

CREATE TABLE TASY.W_ITEM_DEVOLUCAO_PAC
(
  NR_SEQUENCIA    NUMBER(10)                    NOT NULL,
  DT_ATUALIZACAO  DATE                          NOT NULL,
  NM_USUARIO      VARCHAR2(15 BYTE)             NOT NULL,
  NR_DEVOLUCAO    NUMBER(10),
  DS_OBSERVACAO   VARCHAR2(2000 BYTE),
  IE_ETAPA        VARCHAR2(1 BYTE),
  IE_ABORTA       VARCHAR2(1 BYTE),
  DS_TITULO       VARCHAR2(250 BYTE),
  CD_MATERIAL     NUMBER(10)                    NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.WITEDEVP_DEVMAPA_FK_I ON TASY.W_ITEM_DEVOLUCAO_PAC
(NR_DEVOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.WITEDEVP_MATERIA_FK_I ON TASY.W_ITEM_DEVOLUCAO_PAC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.WITEDEVP_PK ON TASY.W_ITEM_DEVOLUCAO_PAC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.W_ITEM_DEVOLUCAO_PAC ADD (
  CONSTRAINT WITEDEVP_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.W_ITEM_DEVOLUCAO_PAC ADD (
  CONSTRAINT WITEDEVP_DEVMAPA_FK 
 FOREIGN KEY (NR_DEVOLUCAO) 
 REFERENCES TASY.DEVOLUCAO_MATERIAL_PAC (NR_DEVOLUCAO)
    ON DELETE CASCADE,
  CONSTRAINT WITEDEVP_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL));

GRANT SELECT ON TASY.W_ITEM_DEVOLUCAO_PAC TO NIVEL_1;

