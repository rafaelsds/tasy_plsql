ALTER TABLE TASY.GPI_ORC_ITEM
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.GPI_ORC_ITEM CASCADE CONSTRAINTS;

CREATE TABLE TASY.GPI_ORC_ITEM
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_ORCAMENTO     NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE                     NOT NULL,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)        NOT NULL,
  NR_SEQ_PLANO         NUMBER(10)               NOT NULL,
  VL_ORCADO            NUMBER(15,2)             NOT NULL,
  CD_MATERIAL          NUMBER(10),
  QT_ITEM              NUMBER(15,2),
  VL_UNITARIO          NUMBER(15,2),
  CD_UNIDADE_MEDIDA    VARCHAR2(30 BYTE),
  NR_SEQ_ETAPA         NUMBER(10),
  CD_CENTRO_CUSTO      NUMBER(15),
  DS_COMPL_ITEM        VARCHAR2(2000 BYTE),
  DS_MODELO            VARCHAR2(255 BYTE),
  DS_FORNECEDOR        VARCHAR2(255 BYTE),
  VL_REALIZADO         NUMBER(15,2)             NOT NULL,
  IE_ORIGEM_ORC        VARCHAR2(1 BYTE),
  IE_ORIGEM_REAL       VARCHAR2(2 BYTE),
  NR_SEQ_NOTA_FISCAL   NUMBER(10),
  DT_MES_REFERENCIA    DATE,
  VL_ORIGINAL          NUMBER(15,2),
  IE_SITUACAO          VARCHAR2(1 BYTE),
  IE_PRIORIDADE        VARCHAR2(2 BYTE),
  IE_ORIGEM_ITEM       VARCHAR2(15 BYTE),
  CD_MOEDA             NUMBER(5),
  CD_CONTA_FINANC      NUMBER(10),
  QT_REAL              NUMBER(15,2)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.GPIORIT_CENCUST_FK_I ON TASY.GPI_ORC_ITEM
(CD_CENTRO_CUSTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIORIT_CENCUST_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPIORIT_CONFINA_FK_I ON TASY.GPI_ORC_ITEM
(CD_CONTA_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GPIORIT_GPICRETA_FK_I ON TASY.GPI_ORC_ITEM
(NR_SEQ_ETAPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIORIT_GPICRETA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPIORIT_GPIORCA_FK_I ON TASY.GPI_ORC_ITEM
(NR_SEQ_ORCAMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GPIORIT_GPIPLAN_FK_I ON TASY.GPI_ORC_ITEM
(NR_SEQ_PLANO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIORIT_GPIPLAN_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPIORIT_MATERIA_FK_I ON TASY.GPI_ORC_ITEM
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIORIT_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPIORIT_MOEDA_FK_I ON TASY.GPI_ORC_ITEM
(CD_MOEDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIORIT_MOEDA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.GPIORIT_NOTFISC_FK_I ON TASY.GPI_ORC_ITEM
(NR_SEQ_NOTA_FISCAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIORIT_NOTFISC_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.GPIORIT_PK ON TASY.GPI_ORC_ITEM
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.GPIORIT_UNIMEDI_FK_I ON TASY.GPI_ORC_ITEM
(CD_UNIDADE_MEDIDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.GPIORIT_UNIMEDI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.gpi_orc_item_atual before insert or update ON TASY.GPI_ORC_ITEM 
for each row
declare

ie_tipo_w	centro_custo.ie_tipo%type;
ie_situacao_w	centro_custo.ie_situacao%type;

begin

if	(:new.cd_centro_custo is not null) then
	begin

	select	ie_tipo,
		ie_situacao
	into	ie_tipo_w,
		ie_situacao_w
	from	centro_custo
	where	cd_centro_custo	= :new.cd_centro_custo;

	if	(ie_situacao_w = 'I') then
		wheb_mensagem_pck.exibir_mensagem_abort(1048171);
	end if;

	if	(ie_tipo_w = 'T') then
		wheb_mensagem_pck.exibir_mensagem_abort(1048172);
	end if;

	end;
end if;

end;
/


ALTER TABLE TASY.GPI_ORC_ITEM ADD (
  CONSTRAINT GPIORIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.GPI_ORC_ITEM ADD (
  CONSTRAINT GPIORIT_CONFINA_FK 
 FOREIGN KEY (CD_CONTA_FINANC) 
 REFERENCES TASY.CONTA_FINANCEIRA (CD_CONTA_FINANC),
  CONSTRAINT GPIORIT_MOEDA_FK 
 FOREIGN KEY (CD_MOEDA) 
 REFERENCES TASY.MOEDA (CD_MOEDA),
  CONSTRAINT GPIORIT_GPIORCA_FK 
 FOREIGN KEY (NR_SEQ_ORCAMENTO) 
 REFERENCES TASY.GPI_ORCAMENTO (NR_SEQUENCIA),
  CONSTRAINT GPIORIT_GPIPLAN_FK 
 FOREIGN KEY (NR_SEQ_PLANO) 
 REFERENCES TASY.GPI_PLANO (NR_SEQUENCIA),
  CONSTRAINT GPIORIT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT GPIORIT_GPICRETA_FK 
 FOREIGN KEY (NR_SEQ_ETAPA) 
 REFERENCES TASY.GPI_CRON_ETAPA (NR_SEQUENCIA),
  CONSTRAINT GPIORIT_UNIMEDI_FK 
 FOREIGN KEY (CD_UNIDADE_MEDIDA) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT GPIORIT_CENCUST_FK 
 FOREIGN KEY (CD_CENTRO_CUSTO) 
 REFERENCES TASY.CENTRO_CUSTO (CD_CENTRO_CUSTO),
  CONSTRAINT GPIORIT_NOTFISC_FK 
 FOREIGN KEY (NR_SEQ_NOTA_FISCAL) 
 REFERENCES TASY.NOTA_FISCAL (NR_SEQUENCIA));

GRANT SELECT ON TASY.GPI_ORC_ITEM TO NIVEL_1;

