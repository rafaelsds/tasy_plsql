ALTER TABLE TASY.TITULO_PAGAR_TRIB_BAIXA
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.TITULO_PAGAR_TRIB_BAIXA CASCADE CONSTRAINTS;

CREATE TABLE TASY.TITULO_PAGAR_TRIB_BAIXA
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  VL_BAIXA             NUMBER(15,2)             NOT NULL,
  NR_SEQ_TIT_TRIB      NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  NR_TITULO            NUMBER(10)               NOT NULL,
  NR_SEQ_TIT_BAIXA     NUMBER(5)                NOT NULL,
  NR_SEQ_TRANS_FINANC  NUMBER(10),
  NR_LOTE_CONTABIL     NUMBER(10),
  DT_CONTABIL          DATE,
  NR_CODIGO_CONTROLE   VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.TIPATRN_LOTCONT_FK_I ON TASY.TITULO_PAGAR_TRIB_BAIXA
(NR_LOTE_CONTABIL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.TIPATRN_PK ON TASY.TITULO_PAGAR_TRIB_BAIXA
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPATRN_TITPABA_FK_I ON TASY.TITULO_PAGAR_TRIB_BAIXA
(NR_TITULO, NR_SEQ_TIT_BAIXA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPATRN_TITTEIM_FK_I ON TASY.TITULO_PAGAR_TRIB_BAIXA
(NR_SEQ_TIT_TRIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.TIPATRN_TRAFINA_FK_I ON TASY.TITULO_PAGAR_TRIB_BAIXA
(NR_SEQ_TRANS_FINANC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.titulo_pagar_trib_baixa_insert
after insert or update ON TASY.TITULO_PAGAR_TRIB_BAIXA for each row
declare

cd_estabelecimento_w          number(5);
dt_baixa_w                    date;
nr_seq_trans_fin_w            titulo_pagar_baixa.nr_seq_trans_fin%type;

begin

if(philips_contabil_pck.get_estabelecimento() is null) then
select  a.cd_estabelecimento,
        b.dt_baixa,
        b.nr_seq_trans_fin
into    cd_estabelecimento_w,
        dt_baixa_w,
        nr_seq_trans_fin_w
from    titulo_pagar a,
        titulo_pagar_baixa b
where   a.nr_titulo = b.nr_titulo
and     a.nr_titulo = :new.nr_titulo
and     b.nr_sequencia = :new.nr_seq_tit_baixa;
else
    cd_estabelecimento_w := philips_contabil_pck.get_estabelecimento();
    nr_seq_trans_fin_w := philips_contabil_pck.get_nr_seq_trans_fin();
    dt_baixa_w := philips_contabil_pck.get_dt_baixa();
end if;

if (inserting) then
    begin
    if (nvl(:new.vl_baixa, 0) <> 0) then
        begin
            ctb_concil_financeira_pck.ctb_gravar_documento(cd_estabelecimento_w,
                                                            dt_baixa_w,
                                                            7,
                                                            nvl(:new.nr_seq_trans_financ, nr_seq_trans_fin_w),
                                                            13,
                                                            :new.nr_titulo,
                                                            :new.nr_seq_tit_baixa,
                                                            :new.nr_sequencia,
                                                            :new.vl_baixa,
                                                            'TITULO_PAGAR_BAIXA_CONTAB_V',
                                                            'VL_IMPOSTO_BAIXA',
                                                            :new.nm_usuario,
                                                            'P');
        end;
    end if;

    delete
    from    ctb_documento a
    where   a.cd_tipo_lote_contabil = 7
    and     a.nm_atributo = 'VL_IMPOSTO_BAIXA'
    and     a.nr_documento = :new.nr_titulo
    and     a.nr_seq_doc_compl = :new.nr_seq_tit_baixa
    and     a.cd_estabelecimento = cd_estabelecimento_w
    and     a.ie_situacao_ctb = 'N';

    end;
elsif (updating) then
    update  ctb_documento a
    set     a.nr_seq_trans_financ = nvl(:new.nr_seq_trans_financ,nr_seq_trans_fin_w),
            a.vl_movimento = :new.vl_baixa,
            a.dt_atualizacao = sysdate,
            a.nm_usuario = :new.nm_usuario
    where   a.cd_tipo_lote_contabil = 7
    and     a.nr_documento = :new.nr_titulo
    and     a.nr_seq_doc_compl = :new.nr_seq_tit_baixa
    and     a.nr_doc_analitico = :new.nr_sequencia
    and     a.nm_atributo = 'VL_IMPOSTO_BAIXA'
    and     a.ie_situacao_ctb = 'P'
    and     nvl(a.nr_lote_contabil,0) = 0;
end if;

end;
/


ALTER TABLE TASY.TITULO_PAGAR_TRIB_BAIXA ADD (
  CONSTRAINT TIPATRN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.TITULO_PAGAR_TRIB_BAIXA ADD (
  CONSTRAINT TIPATRN_TITPABA_FK 
 FOREIGN KEY (NR_TITULO, NR_SEQ_TIT_BAIXA) 
 REFERENCES TASY.TITULO_PAGAR_BAIXA (NR_TITULO,NR_SEQUENCIA),
  CONSTRAINT TIPATRN_TITTEIM_FK 
 FOREIGN KEY (NR_SEQ_TIT_TRIB) 
 REFERENCES TASY.TITULO_PAGAR_IMPOSTO (NR_SEQUENCIA),
  CONSTRAINT TIPATRN_TRAFINA_FK 
 FOREIGN KEY (NR_SEQ_TRANS_FINANC) 
 REFERENCES TASY.TRANSACAO_FINANCEIRA (NR_SEQUENCIA),
  CONSTRAINT TIPATRN_LOTCONT_FK 
 FOREIGN KEY (NR_LOTE_CONTABIL) 
 REFERENCES TASY.LOTE_CONTABIL (NR_LOTE_CONTABIL));

GRANT SELECT ON TASY.TITULO_PAGAR_TRIB_BAIXA TO NIVEL_1;

