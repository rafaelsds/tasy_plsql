ALTER TABLE TASY.ESCALA_CRIB_II
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.ESCALA_CRIB_II CASCADE CONSTRAINTS;

CREATE TABLE TASY.ESCALA_CRIB_II
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_ATENDIMENTO         NUMBER(10)             NOT NULL,
  CD_PROFISSIONAL        VARCHAR2(10 BYTE)      NOT NULL,
  DT_AVALIACAO           DATE                   NOT NULL,
  DT_LIBERACAO           DATE,
  IE_SITUACAO            VARCHAR2(1 BYTE),
  DT_INATIVACAO          DATE,
  DS_JUSTIFICATIVA       VARCHAR2(255 BYTE),
  IE_SEXO                VARCHAR2(1 BYTE)       NOT NULL,
  QT_PESO_RN             NUMBER(4)              NOT NULL,
  IE_IDADE_GEST          NUMBER(2)              NOT NULL,
  QT_TEMPERATURA         NUMBER(5,2)            NOT NULL,
  QT_BASE                NUMBER(8,2)            NOT NULL,
  QT_SCORE               NUMBER(10,3),
  PR_SCORE               NUMBER(10,3),
  NM_USUARIO_INATIVACAO  VARCHAR2(15 BYTE),
  IE_NIVEL_ATENCAO       VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.ESCCRII_ATEPACI_FK_I ON TASY.ESCALA_CRIB_II
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCRII_I1 ON TASY.ESCALA_CRIB_II
(NR_ATENDIMENTO, DT_AVALIACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.ESCCRII_PESFISI_FK2_I ON TASY.ESCALA_CRIB_II
(CD_PROFISSIONAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.ESCCRII_PK ON TASY.ESCALA_CRIB_II
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.ESCCRII_PK
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.escala_ESCALA_CRIB_II_atual
before insert or update ON TASY.ESCALA_CRIB_II for each row
declare
qt_resultado_w		number(15) 	:= 0;
qt_resultado_ww		number(10,3) 	:= 0;
qt_total_w		number(10,3) 	:= 0;
qt_temp_w		number(10)	:= 0;
qt_base_w		number(10)	:= 0;
qt_base_atual_w		number(10)	:= :new.qt_base;

begin

if	(:new.qt_temperatura < 29.7) then
	qt_temp_w := 5;
elsif	(:new.qt_temperatura < 31.3) then
	qt_temp_w := 4;
elsif	(:new.qt_temperatura < 32.9) then
	qt_temp_w := 3;
elsif	(:new.qt_temperatura < 34.5) then
	qt_temp_w := 2;
elsif	(:new.qt_temperatura < 36.1) then
	qt_temp_w := 1;
elsif	(:new.qt_temperatura < 37.6) then
	qt_temp_w := 0;
elsif	(:new.qt_temperatura < 39.2) then
	qt_temp_w := 1;
elsif	(:new.qt_temperatura < 40.8) then
	qt_temp_w := 2;
elsif	(:new.qt_temperatura >= 40.8) then
	qt_temp_w := 3;
end if;

if	(:new.qt_base >= 3) then
	qt_base_w := 0;
elsif	(:new.qt_base >=-2) then
	qt_base_w := 1;
elsif	(:new.qt_base >= -7) then
	qt_base_w := 2;
elsif	(:new.qt_base >= -12) then
	qt_base_w := 3;
elsif	(:new.qt_base >= -17) then
	qt_base_w := 4;
elsif	(qt_base_atual_w >= -22) then
	qt_base_w := 5;
elsif	(:new.qt_base >= -26) then
	qt_base_w := 6;
elsif	(:new.qt_base < -26) then
	qt_base_w := 7;
end if;

if (:new.IE_SEXO = 'M') then
	if 	(:new.ie_idade_gest = 32) then
		if 	(:new.qt_peso_rn>=1501  ) then
			qt_resultado_w := 0;
		end if;
		if 	(:new.qt_peso_rn>=1251 and :new.qt_peso_rn<=1500  ) then
			qt_resultado_w := 1;
		end if;
		if 	(:new.qt_peso_rn>=1001 and :new.qt_peso_rn<=1250  ) then
			qt_resultado_w := 3;
		end if;
		if 	(:new.qt_peso_rn>=751 and :new.qt_peso_rn<=1000  ) then
			qt_resultado_w := 6;
		end if;
	elsif 	(:new.ie_idade_gest = 31) then
		if 	(:new.qt_peso_rn>=2501  ) then
			qt_resultado_w := 1;
		end if;
		if 	(:new.qt_peso_rn>=1751 and :new.qt_peso_rn<=2500  ) then
			qt_resultado_w := 0;
		end if;
		if 	(:new.qt_peso_rn>=1501 and :new.qt_peso_rn<=1750  ) then
			qt_resultado_w := 1;
		end if;
		if 	(:new.qt_peso_rn>=1251 and :new.qt_peso_rn<=1500  ) then
			qt_resultado_w := 2;
		end if;
		if 	(:new.qt_peso_rn>=1001 and :new.qt_peso_rn<=1250  ) then
			qt_resultado_w := 3;
		end if;
		if	(:new.qt_peso_rn>=751 and :new.qt_peso_rn<=1000  ) then
			qt_resultado_w := 6;
		end if;
		if	(:new.qt_peso_rn>=501 and :new.qt_peso_rn<=750  ) then
			qt_resultado_w := 8;
		end if;
	elsif	(:new.ie_idade_gest = 30) then
		if	(:new.qt_peso_rn>=2251  ) then
			qt_resultado_w := 3;
		end if;
		if	(:new.qt_peso_rn>=2001 and :new.qt_peso_rn<=2250  ) then
			qt_resultado_w := 2;
		end if;
		if	(:new.qt_peso_rn>=1751 and :new.qt_peso_rn<=2000  ) then
			qt_resultado_w := 1;
		end if;
		if 	(:new.qt_peso_rn>=1501 and :new.qt_peso_rn<=1750  ) then
			qt_resultado_w := 2;
		end if;
		if 	(:new.qt_peso_rn>=1251 and :new.qt_peso_rn<=1500  ) then
			qt_resultado_w := 3;
		end if;
		if 	(:new.qt_peso_rn>=1001 and :new.qt_peso_rn<=1250  ) then
			qt_resultado_w := 4;
		end if;
		if 	(:new.qt_peso_rn>=751 and :new.qt_peso_rn<=1000  ) then
			qt_resultado_w := 6;
		end if;
		if 	(:new.qt_peso_rn>=501 and :new.qt_peso_rn<=750  ) then
			qt_resultado_w := 8;
		end if;
	elsif	(:new.ie_idade_gest = 29) then
		if 	(:new.qt_peso_rn>=1251  ) then
			qt_resultado_w := 3;
		end if;
		if 	(:new.qt_peso_rn>=1001 and :new.qt_peso_rn<=1250  ) then
			qt_resultado_w := 5;
		end if;
		if 	(:new.qt_peso_rn>=751 and :new.qt_peso_rn<=1000  ) then
			qt_resultado_w := 6;
		end if;
		if 	(:new.qt_peso_rn>=501 and :new.qt_peso_rn<=750  ) then
			qt_resultado_w := 8;
		end if;
	elsif	(:new.ie_idade_gest = 28) then
		if 	(:new.qt_peso_rn>=1251  ) then
			qt_resultado_w := 5;
		end if;
		if 	(:new.qt_peso_rn>=1001 and :new.qt_peso_rn<=1250  ) then
			qt_resultado_w := 6;
		end if;
		if 	(:new.qt_peso_rn>=751 and :new.qt_peso_rn<=1000  ) then
			qt_resultado_w := 7;
		end if;
		if 	(:new.qt_peso_rn>=501 and :new.qt_peso_rn<=750  ) then
			qt_resultado_w := 8;
		end if;
		if 	(:new.qt_peso_rn>=251 and :new.qt_peso_rn<=500  ) then
			qt_resultado_w := 10;
		end if;
	elsif	(:new.ie_idade_gest = 27) then
		if 	(:new.qt_peso_rn>=1251  ) then
			qt_resultado_w := 6;
		end if;
		if 	(:new.qt_peso_rn>=751 and :new.qt_peso_rn<=1250  ) then
			qt_resultado_w := 7;
		end if;
		if 	(:new.qt_peso_rn>=501 and :new.qt_peso_rn<=750  ) then
			qt_resultado_w := 9;
		end if;
		if 	(:new.qt_peso_rn>=251 and :new.qt_peso_rn<=500  ) then
			qt_resultado_w := 10;
		end if;
	elsif	(:new.ie_idade_gest = 26) then
		if 	(:new.qt_peso_rn>=751  ) then
			qt_resultado_w := 8;
		end if;
		if 	(:new.qt_peso_rn>=501 and :new.qt_peso_rn<=750  ) then
			qt_resultado_w := 10;
		end if;
		if 	(:new.qt_peso_rn>=251 and :new.qt_peso_rn<=500  ) then
			qt_resultado_w := 11;
		end if;
	elsif	(:new.ie_idade_gest = 25) then
		if 	(:new.qt_peso_rn>=1001   ) then
			qt_resultado_w := 9;
		end if;
		if 	(:new.qt_peso_rn>=751 and :new.qt_peso_rn<=1000  ) then
			qt_resultado_w := 10;
		end if;
		if 	(:new.qt_peso_rn>=501 and :new.qt_peso_rn<=750  ) then
			qt_resultado_w := 11;
		end if;
		if 	(:new.qt_peso_rn>=251 and :new.qt_peso_rn<=500  ) then
			qt_resultado_w := 12;
		end if;
	elsif	(:new.ie_idade_gest = 24) then
		if 	(:new.qt_peso_rn>=1001   ) then
			qt_resultado_w := 10;
		end if;
		if 	(:new.qt_peso_rn>=751 and :new.qt_peso_rn<=1000  ) then
			qt_resultado_w := 11;
		end if;
		if 	(:new.qt_peso_rn>=501 and :new.qt_peso_rn<=750  ) then
			qt_resultado_w := 12;
		end if;
		if 	(:new.qt_peso_rn>=251 and :new.qt_peso_rn<=500  ) then
			qt_resultado_w := 13;
		end if;
	elsif	(:new.ie_idade_gest = 23) then
		if 	(:new.qt_peso_rn>=751   ) then
			qt_resultado_w := 12;
		end if;
		if 	(:new.qt_peso_rn>=501 and :new.qt_peso_rn<=750  ) then
			qt_resultado_w := 13;
		end if;
		if 	(:new.qt_peso_rn>=251 and :new.qt_peso_rn<=500  ) then
			qt_resultado_w := 14;
		end if;
	elsif	(:new.ie_idade_gest = 22) then
		if 	(:new.qt_peso_rn>=501 ) then
			qt_resultado_w := 14;
		end if;
		if 	(:new.qt_peso_rn>=251 and :new.qt_peso_rn<=500 ) then
			qt_resultado_w := 15;
		end if;
	end if;
else
	if	(:new.ie_idade_gest = 32) then
		if 	(:new.qt_peso_rn>=1501) then
			qt_resultado_ww :=0;
		end if;
		if 	(:new.qt_peso_rn>=1251) and (:new.qt_peso_rn<=1500) then
			qt_resultado_ww :=1;
		end if;
		if 	(:new.qt_peso_rn>=1001) and (:new.qt_peso_rn<=1250) then
			qt_resultado_ww :=3;
		end if;
		if 	(:new.qt_peso_rn>=751 ) and (:new.qt_peso_rn<=1000 ) then
			qt_resultado_ww :=5;
		end if;
	elsif	(:new.ie_idade_gest = 31) then
		if 	(:new.qt_peso_rn>=2501 )then
			qt_resultado_ww :=1;
		end if;
		if 	(:new.qt_peso_rn>=1501 ) and ( :new.qt_peso_rn<=2500 )then
			qt_resultado_ww :=0;
		end if;
		if 	(:new.qt_peso_rn>=1251 ) and ( :new.qt_peso_rn<=1500 )then
			qt_resultado_ww :=1;
		end if;
		if 	(:new.qt_peso_rn>=1001 ) and ( :new.qt_peso_rn<=1250 )then
			qt_resultado_ww :=3;
		end if;
		if 	(:new.qt_peso_rn>=751 ) and ( :new.qt_peso_rn<=1000 )then
			qt_resultado_ww :=5;
		end if;
		if 	(:new.qt_peso_rn>=501 ) and ( :new.qt_peso_rn<=750 )then
			qt_resultado_ww :=7;
		end if;
	elsif	(:new.ie_idade_gest = 30) then
		if 	(:new.qt_peso_rn>=2251 )then
			qt_resultado_ww :=2;
		end if;
		if 	(:new.qt_peso_rn>=1501 ) and ( :new.qt_peso_rn<=2250 )then
			qt_resultado_ww :=1;
		end if;
		if 	(:new.qt_peso_rn>=1251 ) and ( :new.qt_peso_rn<=1500 )then
			qt_resultado_ww :=2;
		end if;
		if 	(:new.qt_peso_rn>=1001 ) and ( :new.qt_peso_rn<=1250 )then
			qt_resultado_ww :=3;
		end if;
		if 	(:new.qt_peso_rn>=751 ) and ( :new.qt_peso_rn<=1000 )then
			qt_resultado_ww :=5;
		end if;
		if 	(:new.qt_peso_rn>=501 ) and ( :new.qt_peso_rn<=750 )then
			qt_resultado_ww :=7;
		end if;
	elsif	(:new.ie_idade_gest = 29) then
		if 	(:new.qt_peso_rn>=1251 ) then
			qt_resultado_w :=3;
		end if;
		if 	(:new.qt_peso_rn>=1001 ) and ( :new.qt_peso_rn<=1250 )then
			qt_resultado_ww :=4;
		end if;
		if 	(:new.qt_peso_rn>=751 ) and ( :new.qt_peso_rn<=1000 )then
			qt_resultado_ww :=5;
		end if;
		if 	(:new.qt_peso_rn>=501 ) and ( :new.qt_peso_rn<=750 )then
			qt_resultado_ww :=7;
		end if;
	elsif	(:new.ie_idade_gest = 28) then
		if 	(:new.qt_peso_rn>=1251 )then
			qt_resultado_ww :=4;
		end if;
		if 	(:new.qt_peso_rn>=1001 ) and ( :new.qt_peso_rn<=1250 )then
			qt_resultado_ww :=5;
		end if;
		if 	(:new.qt_peso_rn>=751 ) and ( :new.qt_peso_rn<=1000 )then
			qt_resultado_ww :=6;
		end if;
		if 	(:new.qt_peso_rn>=501 ) and ( :new.qt_peso_rn<=750 )then
			qt_resultado_ww :=8;
		end if;
		if 	(:new.qt_peso_rn>=251 ) and ( :new.qt_peso_rn<=500 )then
			qt_resultado_ww :=10;
		end if;
	elsif	(:new.ie_idade_gest = 27) then
		if 	(:new.qt_peso_rn>=1501 )then
			qt_resultado_ww :=6;
		end if;
		if 	(:new.qt_peso_rn>=1251 ) and ( :new.qt_peso_rn<=1500 )then
			qt_resultado_ww :=5;
		end if;
		if 	(:new.qt_peso_rn>=1001 ) and ( :new.qt_peso_rn<=1250 )then
			qt_resultado_ww :=6;
		end if;
		if 	(:new.qt_peso_rn>=751 ) and ( :new.qt_peso_rn<=1000 )then
			qt_resultado_ww :=7;
		end if;
		if 	(:new.qt_peso_rn>=501 ) and ( :new.qt_peso_rn<=750 )then
			qt_resultado_ww :=8;
		end if;
		if 	(:new.qt_peso_rn>=251 ) and ( :new.qt_peso_rn<=500 )then
			qt_resultado_ww :=10;
		end if;
	elsif	(:new.ie_idade_gest = 26) then
		if 	(:new.qt_peso_rn>=1001 )then
			qt_resultado_ww :=7;
		end if;
		if 	(:new.qt_peso_rn>=751 ) and ( :new.qt_peso_rn<=1000 )then
			qt_resultado_ww :=8;
		end if;
		if 	(:new.qt_peso_rn>=501 ) and ( :new.qt_peso_rn<=750 )then
			qt_resultado_ww :=9;
		end if;
		if 	(:new.qt_peso_rn>=251 ) and ( :new.qt_peso_rn<=500 )then
			qt_resultado_ww :=11;
		end if;
	elsif	(:new.ie_idade_gest = 25) then
		if 	(:new.qt_peso_rn>=1001  )then
			qt_resultado_ww :=8;
		end if;
		if 	(:new.qt_peso_rn>=751 ) and ( :new.qt_peso_rn<=1000 )then
			qt_resultado_ww :=9;
		end if;
		if 	(:new.qt_peso_rn>=501 ) and ( :new.qt_peso_rn<=750 )then
			qt_resultado_ww :=10;
		end if;
		if 	(:new.qt_peso_rn>=251 ) and ( :new.qt_peso_rn<=500 )then
			qt_resultado_ww :=11;
		end if;
	elsif	(:new.ie_idade_gest = 24) then
		if 	(:new.qt_peso_rn>=751  )then
			qt_resultado_ww :=10;
		end if;
		if 	(:new.qt_peso_rn>=501 ) and ( :new.qt_peso_rn<=750 )then
			qt_resultado_ww :=11;
		end if;
		if 	(:new.qt_peso_rn>=251 ) and ( :new.qt_peso_rn<=500 )then
			qt_resultado_ww :=12;
		end if;
	elsif	(:new.ie_idade_gest = 23) then
		if 	(:new.qt_peso_rn>=751  )then
			qt_resultado_ww :=11;
		end if;
		if 	(:new.qt_peso_rn>=501 ) and ( :new.qt_peso_rn<=750 )then
			qt_resultado_ww :=12;
		end if;
		if 	(:new.qt_peso_rn>=251 ) and ( :new.qt_peso_rn<=500 )then
			qt_resultado_ww :=13;
		end if;
	elsif	(:new.ie_idade_gest = 22) then
		if 	(:new.qt_peso_rn>=501)then
			qt_resultado_ww :=13;
		end if;
		if 	(:new.qt_peso_rn>=251 ) and (:new.qt_peso_rn<=500) then
			qt_resultado_ww :=14;
		end if;
	end if;
end if;

:new.QT_SCORE 	:= qt_temp_w  + qt_base_w + qt_resultado_ww + qt_resultado_w ;

qt_total_w 	:= qt_total_w + qt_temp_w+qt_resultado_ww + qt_resultado_w+ qt_base_w;

qt_total_w 	:= qt_total_w * 0.45;
qt_total_w 	:= qt_total_w - 6.476;
qt_total_w 	:= exp(qt_total_w)/(1 + exp(qt_total_w));
qt_total_w	:= (100 * qt_total_w);

if 	( qt_total_w >=0) then
	:new.PR_SCORE	:= trunc(qt_total_w+0.05,1);
else
	:new.PR_SCORE	:= trunc(qt_total_w-0.05,1);
end if;

end;
/


ALTER TABLE TASY.ESCALA_CRIB_II ADD (
  CONSTRAINT ESCCRII_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.ESCALA_CRIB_II ADD (
  CONSTRAINT ESCCRII_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO),
  CONSTRAINT ESCCRII_PESFISI_FK2 
 FOREIGN KEY (CD_PROFISSIONAL) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA));

GRANT SELECT ON TASY.ESCALA_CRIB_II TO NIVEL_1;

