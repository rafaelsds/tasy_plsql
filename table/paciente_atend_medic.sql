ALTER TABLE TASY.PACIENTE_ATEND_MEDIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PACIENTE_ATEND_MEDIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PACIENTE_ATEND_MEDIC
(
  NR_SEQ_ATENDIMENTO         NUMBER(10)         NOT NULL,
  NR_SEQ_MATERIAL            NUMBER(10)         NOT NULL,
  CD_MATERIAL                NUMBER(6)          NOT NULL,
  DT_ATUALIZACAO             DATE               NOT NULL,
  NM_USUARIO                 VARCHAR2(15 BYTE)  NOT NULL,
  NR_AGRUPAMENTO             NUMBER(6)          NOT NULL,
  QT_DOSE                    NUMBER(15,4),
  CD_UNID_MED_DOSE           VARCHAR2(30 BYTE),
  QT_DOSE_PRESCRICAO         NUMBER(15,4),
  CD_UNID_MED_PRESCR         VARCHAR2(30 BYTE),
  DS_RECOMENDACAO            VARCHAR2(2000 BYTE),
  IE_VIA_APLICACAO           VARCHAR2(5 BYTE),
  NR_SEQ_DILUICAO            NUMBER(10),
  QT_MIN_APLICACAO           NUMBER(4),
  IE_BOMBA_INFUSAO           VARCHAR2(1 BYTE)   NOT NULL,
  CD_INTERVALO               VARCHAR2(7 BYTE),
  QT_HORA_APLICACAO          NUMBER(3),
  QT_DIAS_UTIL               NUMBER(3),
  NR_SEQ_INTERNO             NUMBER(10)         NOT NULL,
  NR_SEQ_PROT_MEDIC          NUMBER(10),
  IE_CANCELADA               VARCHAR2(1 BYTE)   NOT NULL,
  DT_CANCELAMENTO            DATE,
  NM_USUARIO_CANCEL          VARCHAR2(15 BYTE),
  IE_ADMINISTRACAO           VARCHAR2(5 BYTE),
  IE_CHECADO                 VARCHAR2(1 BYTE),
  DS_OBSERVACAO              VARCHAR2(255 BYTE),
  IE_SE_NECESSARIO           VARCHAR2(1 BYTE),
  IE_URGENCIA                VARCHAR2(1 BYTE),
  IE_APLIC_BOLUS             VARCHAR2(1 BYTE),
  IE_APLIC_LENTA             VARCHAR2(1 BYTE),
  QT_AUC                     NUMBER(4,1),
  DS_TEMPO_INFUSAO           VARCHAR2(255 BYTE),
  NR_SEQ_VIA_ACESSO          NUMBER(10),
  IE_ITEM_SUPERIOR           VARCHAR2(1 BYTE),
  NR_SEQ_INT_PROT_MEDIC      NUMBER(10),
  IE_PRE_MEDICACAO           VARCHAR2(1 BYTE),
  NR_SEQ_SOLUCAO             NUMBER(10),
  QT_CREATININA              NUMBER(14,6),
  QT_CCR                     NUMBER(14,6),
  IE_AGRUPADOR               NUMBER(2),
  IE_GERAR_SOLUCAO           VARCHAR2(1 BYTE),
  NR_SEQ_PACIENTE            NUMBER(15),
  NR_SEQ_MAT_PROTOCOLO       NUMBER(15),
  PR_REDUCAO                 VARCHAR2(3 BYTE),
  NR_SEQ_RECONSTITUINTE      NUMBER(10),
  NR_SEQ_MOTIVO_PERDA        NUMBER(10),
  IE_APLICA_REDUCAO          VARCHAR2(1 BYTE),
  IE_TIPO_PERCENTUAL         VARCHAR2(1 BYTE),
  IE_ZERADO                  VARCHAR2(1 BYTE),
  IE_REGRA_DILUICAO_CAD_MAT  VARCHAR2(1 BYTE),
  NR_SEQ_PROCEDIMENTO        NUMBER(6),
  IE_LOCAL_ADM               VARCHAR2(1 BYTE),
  NR_SEQ_PROT_MED            NUMBER(10),
  NR_SEQ_ORDEM_ADEP          NUMBER(10),
  IE_USO_CONTINUO            VARCHAR2(1 BYTE),
  NR_SEQ_MEDIC_MATERIAL      NUMBER(10),
  DS_DOSE_DIFERENCIADA       VARCHAR2(50 BYTE),
  NR_SEQ_MAT_DILUICAO        NUMBER(10),
  IE_SITUACAO                VARCHAR2(1 BYTE),
  DT_LIBERACAO               DATE,
  NR_SEQ_ASSINATURA          NUMBER(10),
  NR_SEQ_DISPOSITIVO         NUMBER(10),
  DT_RECEBIMENTO_MEDIC       DATE,
  NM_USUARIO_RECEB           VARCHAR2(15 BYTE),
  IE_ORIGEM_CHECAGEM         VARCHAR2(15 BYTE),
  IE_ACM                     VARCHAR2(1 BYTE),
  QT_DIAS_RECEITA            NUMBER(5),
  IE_OBJETIVO                VARCHAR2(1 BYTE),
  DS_JUSTIFICATIVA           VARCHAR2(2000 BYTE),
  CD_TOPOGRAFIA_CIH          NUMBER(10),
  CD_MICROORGANISMO_CIH      NUMBER(10),
  CD_AMOSTRA_CIH             NUMBER(10),
  IE_ORIGEM_INFECCAO         VARCHAR2(1 BYTE),
  IE_MEDICACAO_PACIENTE      VARCHAR2(1 BYTE),
  NR_SEQ_MAT_PRO             NUMBER(6),
  NM_USUARIO_LIB             VARCHAR2(15 BYTE)  DEFAULT null,
  NR_SEQ_ATEND_CONS_PEPA     NUMBER(10),
  IE_PESO_CONSIDERAR         VARCHAR2(1 BYTE),
  QT_FATOR_CORRECAO          NUMBER(10,3),
  CD_UNID_MEDIDA_REF         VARCHAR2(30 BYTE),
  QT_DOSE_REF                NUMBER(15,4)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          640K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PACATME_ATCONSPEPA_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_ATEND_CONS_PEPA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_CIHAMCU_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(CD_AMOSTRA_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_CIHMICR_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(CD_MICROORGANISMO_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_CIHTOPO_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(CD_TOPOGRAFIA_CIH)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_DISPOSI_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_DISPOSITIVO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_DISPOSI_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_INTPRES_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(CD_INTERVALO)
NOLOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_INTPRES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_I1 ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_INT_PROT_MEDIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_I1
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_I2 ON TASY.PACIENTE_ATEND_MEDIC
(DT_LIBERACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_I3 ON TASY.PACIENTE_ATEND_MEDIC
(NM_USUARIO_LIB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_MATERIAL_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_MATERIAL_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_MOPERQ_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_MOTIVO_PERDA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          416K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_MOPERQ_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_PACATEN_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_PACATME_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_ATENDIMENTO, NR_SEQ_DILUICAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_PACATME_FK2_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_ATENDIMENTO, NR_SEQ_MEDIC_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_PACATME_FK2_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_PACATPR_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_ATENDIMENTO, NR_SEQ_PROCEDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_PACATPR_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_PACATSO_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_ATENDIMENTO, NR_SEQ_SOLUCAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_PACATSO_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PACATME_PK ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_ATENDIMENTO, NR_SEQ_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          128K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_TASASDI_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_ASSINATURA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_TASASDI_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.PACATME_UK ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_INTERNO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_UK
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_UNIMEDI_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(CD_UNID_MED_DOSE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PACATME_UNIMEDP_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(CD_UNID_MED_PRESCR)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_UNIMEDP_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_VIAACES_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(NR_SEQ_VIA_ACESSO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_VIAACES_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PACATME_VIAAPLI_FK_I ON TASY.PACIENTE_ATEND_MEDIC
(IE_VIA_APLICACAO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PACATME_VIAAPLI_FK_I
  MONITORING USAGE;


CREATE OR REPLACE TRIGGER TASY.paciente_atend_medic_atual
before insert or update ON TASY.PACIENTE_ATEND_MEDIC for each row
declare
nr_seq_paciente_w		number(10);
nr_seq_pend_Agenda_w	number(10);
qt_reg_w			number(10);
ie_via_aplicacao_w		varchar2(5);
ds_restricao_medic_onc_w 	varchar(100);

begin

if (wheb_usuario_pck.get_ie_executar_trigger = 'N')  then
	goto Final;
end if;

if	(inserting)  or
	(:new.cd_material <> :old.cd_material) then

	select obter_se_restricao_medic_onc(:new.cd_material)
	into ds_restricao_medic_onc_w
	from dual;

	if(ds_restricao_medic_onc_w is not null) and (:new.nr_seq_prot_med is null) and (:new.nr_seq_solucao is null)then
		Wheb_mensagem_pck.exibir_mensagem_abort(ds_restricao_medic_onc_w);
	end if;

	Consiste_Regra_Mat_quimio(:new.cd_material);
end if;


select	max(nr_seq_paciente),
	max(nr_seq_pend_Agenda)
into	nr_seq_paciente_w,
	nr_seq_pend_Agenda_w
from	paciente_atendimento
where	nr_seq_atendimento = :new.nr_seq_atendimento;

if	(:new.dt_cancelamento is null) then
	Abortar_Se_Protoc_Inativo(nr_seq_paciente_w);
end if;


if	(:new.pr_reducao is not null) and
	((NVL(:new.pr_reducao,0) <> NVL(:old.pr_reducao,0)))  and
	((NVL(:new.QT_DOSE_PRESCRICAO,0) = NVL(:old.QT_DOSE_PRESCRICAO,0)))then
	:new.QT_DOSE_PRESCRICAO := (((nvl(:new.pr_reducao,100) * nvl(:new.QT_DOSE_PRESCRICAO,0)) / nvl(:old.pr_reducao,100)));

end if;


if (inserting) then

	update	qt_pendencia_agenda
	set	ie_alt_medicamento = 'S'
	where	nr_sequencia = nr_seq_pend_Agenda_w;

elsif (not inserting) and
      ((nvl(:old.cd_material,0)	    	<> 	nvl(:new.cd_material,0)) or
      (nvl(:old.CD_INTERVALO,'XxX') 	<> 	nvl(:new.CD_INTERVALO,'XxX')) or
      (nvl(:old.qt_dose,0) 	    	<> 	nvl(:new.qt_dose,0)) or
      (nvl(:old.qt_hora_aplicacao,0) 	<> 	nvl(:new.qt_hora_aplicacao,0)) or
      (nvl(:old.qt_min_aplicacao,0) 	<> 	nvl(:new.qt_min_aplicacao,0))) then

	update	qt_pendencia_agenda
	set	ie_alt_medicamento = 'S'
	where	nr_sequencia = nr_seq_pend_Agenda_w;

end if;

if (inserting) and
	(:new.IE_VIA_APLICACAO is null) then

	select	max(ie_via_aplicacao)
	into	ie_via_aplicacao_w
	from	material
	where	cd_material = :new.CD_MATERIAL;

	if (ie_via_aplicacao_w is not null) then
		:new.IE_VIA_APLICACAO := ie_via_aplicacao_w;
	end if;

end if;

if (:new.cd_unid_medida_ref is null) then
	:new.cd_unid_medida_ref := :new.cd_unid_med_prescr;
end if;

if (:new.qt_dose_ref is null) then
	:new.qt_dose_ref := :new.qt_dose_prescricao;
end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_atend_medic_refs
before insert ON TASY.PACIENTE_ATEND_MEDIC for each row
declare

begin
	if (:new.cd_unid_medida_ref is null) then
		:new.cd_unid_medida_ref := :new.cd_unid_med_prescr;
	end if;

	if (:new.qt_dose_ref is null) then
		:new.qt_dose_ref := :new.qt_dose_prescricao;
	end if;
end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_atend_medic_update
before update ON TASY.PACIENTE_ATEND_MEDIC for each row
declare

ie_inserir_item_adm_w		varchar2(1);
qt_mat_itens_w 			number(10);
ie_alterar_etapa_w			varchar2(1);
ie_atualizar_autor_conv_w		varchar2(15) := 'N';
nr_seq_autor_w			number(10,0);
nr_ciclo_w			number(3,0);
ds_dia_ciclo_w			varchar2(5);
ie_inserir_novo_item_adm_w	varchar2(1);
ie_atualizar_pac_prot_med_w	varchar2(15) := 'S';
ie_utiliza_data_adm_w		varchar2(1);
cd_funcao_ativa_w		Number(15) := nvl(Obter_Funcao_Ativa,0);
ie_via_aplicacao_w		paciente_atend_medic.ie_via_aplicacao%type;
cd_perfil_w			perfil.cd_perfil%type;
cd_estabelecimento_w		estabelecimento.cd_estabelecimento%type;

begin

cd_perfil_w	:= nvl(obter_perfil_ativo,0);
cd_estabelecimento_w			:= nvl(wheb_usuario_pck.get_cd_estabelecimento,1);

obter_param_usuario(3130,47,cd_perfil_w,:new.nm_usuario, cd_estabelecimento_w, ie_inserir_item_adm_w);
obter_param_usuario(3130,274,cd_perfil_w,:new.nm_usuario, cd_estabelecimento_w, ie_inserir_novo_item_adm_w);
obter_param_usuario(3130,84,cd_perfil_w,:new.nm_usuario, cd_estabelecimento_w, ie_alterar_etapa_w);
obter_param_usuario(281,1001,cd_perfil_w,:new.nm_usuario, cd_estabelecimento_w, ie_atualizar_autor_conv_w);
obter_param_usuario(281,1185,cd_perfil_w,:new.nm_usuario, cd_estabelecimento_w, ie_atualizar_pac_prot_med_w);
obter_param_usuario(3130,349,cd_perfil_w,:new.nm_usuario, cd_estabelecimento_w, ie_utiliza_data_adm_w);

if	(ie_alterar_etapa_w = 'S') and
	(:new.ie_administracao = 'A') and
	(:new.ie_administracao <> nvl(:old.ie_administracao,'X')) then

	update 	paciente_atendimento
	set 	dt_inicio_adm = sysdate,
		nm_usuario = :new.nm_usuario
	where	dt_inicio_adm is null
	and	nr_seq_atendimento = :new.nr_seq_atendimento;

end if;

if	(ie_inserir_item_adm_w = 'S') and
	(:new.ie_administracao = 'A') and
	(:old.ie_administracao <> 'A') and
	(ie_utiliza_data_adm_w <> 'S') then

	if (:new.ie_checado = 'N') then
		:new.ie_checado := 'S';
	end if;

	select 	count(*)
	into	qt_mat_itens_w
	from 	paciente_atend_medic_adm
	where 	nr_seq_atendimento = :new.nr_seq_atendimento
	and	nr_seq_material	= :new.nr_seq_material;


	if	(qt_mat_itens_w >0) and
		(ie_inserir_novo_item_adm_w = 'N') then
		update 	paciente_atend_medic_adm
		set			dt_atualizacao		= sysdate,
					    nm_usuario			= decode(cd_funcao_ativa_w, 88, nm_usuario, :new.nm_usuario),
					    dt_administracao	= sysdate,
					    cd_profissional		= decode(cd_funcao_ativa_w, 88, cd_profissional, obter_pessoa_fisica_usuario(:new.nm_usuario,'C')),
                        ie_status_adm		= 1
		where 		nr_seq_atendimento	= :new.nr_seq_atendimento
		and			nr_seq_material		= :new.nr_seq_material;

	end if;

end if;

if	(ie_atualizar_autor_conv_w = 'S') then
	begin

	select	max(nr_ciclo),
		max(ds_dia_ciclo)
	into	nr_ciclo_w,
		ds_dia_ciclo_w
	from	paciente_atendimento
	where	nr_seq_atendimento = :new.nr_seq_atendimento;

	select	max(a.nr_sequencia)
	into	nr_seq_autor_w
	from	autorizacao_convenio a,
		estagio_autorizacao b
	where	a.nr_ciclo = nr_ciclo_w
	and	((a.ds_dia_ciclo 		= ds_dia_ciclo_w) or
		(nvl(a.ds_dia_ciclo, 'X') 	= 'X'))
	and	a.nr_seq_paciente_setor 	= :new.nr_seq_paciente
	and	a.nr_seq_estagio		= b.nr_sequencia
	and	b.ie_interno 		= '1';

	if	(nvl(nr_seq_autor_w,0) 	<> 0) and
		((:new.qt_dose 		<> :old.qt_dose) or
		(:new.qt_dose_prescricao	<> :old.qt_dose_prescricao) or
		(:new.qt_dias_util		<> :old.qt_dias_util) or
		(:new.cd_intervalo 		<> :old.cd_intervalo)) then
		begin
		update	material_autorizado
		set	qt_solicitada 		= nvl(calcula_dose_total_quimio(:new.qt_dose_prescricao,:new.qt_dias_util,:new.cd_intervalo),nvl(:new.qt_dose_prescricao,:new.qt_dose)),
			dt_atualizacao		= sysdate,
			nm_usuario		= :new.nm_usuario
		where	nr_sequencia_autor 	= nr_seq_autor_w
		and	cd_material		= :old.cd_material;

		if	(nvl(ie_atualizar_pac_prot_med_w,'S') = 'S') then
			begin
			update	paciente_protocolo_medic
			set	qt_dose		= :new.qt_dose
			where	cd_material	= :old.cd_material
			and		nr_seq_material = :old.nr_seq_material
			and	nr_seq_paciente	= :new.nr_seq_paciente;
			end;
		end if;

		end;
	end if;
	exception
	when others then
		null;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.pac_atend_medic_pend_atual
after insert or update ON TASY.PACIENTE_ATEND_MEDIC for each row
declare

qt_reg_w			number(1);
ie_tipo_w			varchar2(10);
ie_lib_medic_w		varchar2(10);
cd_pessoa_fisica_w		varchar2(10);
nr_atendimento_w		number(10);

begin
if	(wheb_usuario_pck.get_ie_executar_trigger	= 'N')  then
	goto Final;
end if;

if	(wheb_usuario_pck.get_cd_funcao = 3130) then

	select	max(ie_libera_medicamento)
	into	ie_lib_medic_w
	from	parametros_quimio;

	select	max(nr_atendimento),
		max(obter_codigo_paciente_setor(nr_seq_paciente))
	into	nr_atendimento_w,
		cd_pessoa_fisica_w
	from	paciente_atendimento
	where	nr_seq_atendimento	= :new.nr_seq_atendimento;

	if	(nvl(ie_lib_medic_w,'N') = 'S') then
		if	(:new.dt_liberacao is null) then
			ie_tipo_w := 'QLM';
		elsif	(:old.dt_liberacao is null) and
			(:new.dt_liberacao is not null) then
			ie_tipo_w := 'XQLM';
		end if;

		if	(ie_tipo_w	is not null) then
			Gerar_registro_pendente_PEP(ie_tipo_w, :new.nr_seq_interno, cd_pessoa_fisica_w, nr_atendimento_w, :new.nm_usuario);
		end if;
	end if;

end if;

<<Final>>
qt_reg_w	:= 0;
end;
/


CREATE OR REPLACE TRIGGER TASY.paciente_atend_medic_delete
before delete ON TASY.PACIENTE_ATEND_MEDIC for each row
declare

ie_atualizar_autor_conv_w		varchar2(15) := 'N';
nr_seq_autor_w			number(10,0);
nr_ciclo_w			number(3,0);
ds_dia_ciclo_w			varchar2(5);
nr_seq_pend_Agenda_w		number(10);

qt_dose_total_w			paciente_atend_medic.qt_dose_prescricao%type;
qt_solicitada_w			material_autorizado.qt_solicitada%type;
qt_itens_autor_w		integer;
qt_itens_tratamento_w		integer;

begin
obter_param_usuario(281,1001,obter_perfil_ativo,:old.nm_usuario,0,ie_atualizar_autor_conv_w);

if	(ie_atualizar_autor_conv_w = 'S') then
	begin

	select	max(nr_ciclo),
		max(ds_dia_ciclo)
	into	nr_ciclo_w,
		ds_dia_ciclo_w
	from	paciente_atendimento
	where	nr_seq_atendimento = :old.nr_seq_atendimento;


	select	max(a.nr_sequencia)
	into	nr_seq_autor_w
	from	autorizacao_convenio a,
		estagio_autorizacao b
	where	a.nr_ciclo			= nr_ciclo_w
	and	((a.ds_dia_ciclo		= ds_dia_ciclo_w) or
		(nvl(ds_dia_ciclo, 'X')	= 'X'))
	and	a.nr_seq_paciente_setor	= :old.nr_seq_paciente
	and	a.nr_seq_estagio		= b.nr_sequencia
	and	b.ie_interno 		= '1';


	if	(nvl(nr_seq_autor_w,0) <> 0) then
		begin

			select	sum(qt_dose_prescricao),
			count(1)
		into	qt_dose_total_w,
			qt_itens_tratamento_w
		from	paciente_atend_medic
		where	nr_seq_atendimento = :old.nr_seq_atendimento
		and	cd_material = :old.cd_material;

		select 	sum(qt_solicitada),
			count(1)
		into	qt_solicitada_w,
			qt_itens_autor_w
		from	material_autorizado
		where	nr_sequencia_autor 	= nr_seq_autor_w
		and	cd_material		= :old.cd_material;

		if	((qt_dose_total_w = qt_solicitada_w) and (qt_itens_autor_w = 1)) then

		delete	from material_autorizado
		where	nr_sequencia_autor 	= nr_seq_autor_w
		and	cd_material		= :old.cd_material;

		elsif 	((qt_solicitada_w > nvl(:old.qt_dose_prescricao,0)) and (qt_itens_tratamento_w > 1) and (qt_itens_autor_w = 1)) then

			update 	material_autorizado set
				qt_solicitada 		= qt_solicitada - nvl(:old.qt_dose_prescricao,0),
				dt_atualizacao 		= sysdate,
				nm_usuario		= :new.nm_usuario
			where	nr_sequencia_autor 	= nr_seq_autor_w
			and	cd_material		= :old.cd_material;

		end if;
		end;
	end if;

	exception
	when others then
		null;
	end;
end if;

end;
/


CREATE OR REPLACE TRIGGER TASY.PACIENTE_ATEND_MEDIC_tp  after update ON TASY.PACIENTE_ATEND_MEDIC FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := null;  ds_c_w:= 'NR_SEQ_ATENDIMENTO='||to_char(:old.NR_SEQ_ATENDIMENTO)||'#@#@NR_SEQ_MATERIAL='||to_char(:old.NR_SEQ_MATERIAL);gravar_log_alteracao(substr(:old.QT_DOSE_PRESCRICAO,1,4000),substr(:new.QT_DOSE_PRESCRICAO,1,4000),:new.nm_usuario,nr_seq_w,'QT_DOSE_PRESCRICAO',ie_log_w,ds_w,'PACIENTE_ATEND_MEDIC',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.CD_UNID_MED_PRESCR,1,4000),substr(:new.CD_UNID_MED_PRESCR,1,4000),:new.nm_usuario,nr_seq_w,'CD_UNID_MED_PRESCR',ie_log_w,ds_w,'PACIENTE_ATEND_MEDIC',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PACIENTE_ATEND_MEDIC ADD (
  CONSTRAINT PACATME_PK
 PRIMARY KEY
 (NR_SEQ_ATENDIMENTO, NR_SEQ_MATERIAL)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          128K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ),
  CONSTRAINT PACATME_UK
 UNIQUE (NR_SEQ_INTERNO)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PACIENTE_ATEND_MEDIC ADD (
  CONSTRAINT PACATME_ATCONSPEPA_FK 
 FOREIGN KEY (NR_SEQ_ATEND_CONS_PEPA) 
 REFERENCES TASY.ATEND_CONSULTA_PEPA (NR_SEQUENCIA),
  CONSTRAINT PACATME_CIHTOPO_FK 
 FOREIGN KEY (CD_TOPOGRAFIA_CIH) 
 REFERENCES TASY.CIH_TOPOGRAFIA (CD_TOPOGRAFIA),
  CONSTRAINT PACATME_CIHAMCU_FK 
 FOREIGN KEY (CD_AMOSTRA_CIH) 
 REFERENCES TASY.CIH_AMOSTRA_CULTURA (CD_AMOSTRA_CULTURA),
  CONSTRAINT PACATME_CIHMICR_FK 
 FOREIGN KEY (CD_MICROORGANISMO_CIH) 
 REFERENCES TASY.CIH_MICROORGANISMO (CD_MICROORGANISMO),
  CONSTRAINT PACATME_TASASDI_FK 
 FOREIGN KEY (NR_SEQ_ASSINATURA) 
 REFERENCES TASY.TASY_ASSINATURA_DIGITAL (NR_SEQUENCIA),
  CONSTRAINT PACATME_DISPOSI_FK 
 FOREIGN KEY (NR_SEQ_DISPOSITIVO) 
 REFERENCES TASY.DISPOSITIVO (NR_SEQUENCIA),
  CONSTRAINT PACATME_PACATSO_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO, NR_SEQ_SOLUCAO) 
 REFERENCES TASY.PACIENTE_ATEND_SOLUC (NR_SEQ_ATENDIMENTO,NR_SEQ_SOLUCAO)
    ON DELETE CASCADE,
  CONSTRAINT PACATME_MOPERQ_FK 
 FOREIGN KEY (NR_SEQ_MOTIVO_PERDA) 
 REFERENCES TASY.MOTIVO_PERDA_QUIMIO (NR_SEQUENCIA),
  CONSTRAINT PACATME_PACATPR_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO, NR_SEQ_PROCEDIMENTO) 
 REFERENCES TASY.PACIENTE_ATEND_PROC (NR_SEQ_ATENDIMENTO,NR_SEQ_PROCEDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT PACATME_PACATME_FK2 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO, NR_SEQ_MEDIC_MATERIAL) 
 REFERENCES TASY.PACIENTE_ATEND_MEDIC (NR_SEQ_ATENDIMENTO,NR_SEQ_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT PACATME_PACATME_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO, NR_SEQ_DILUICAO) 
 REFERENCES TASY.PACIENTE_ATEND_MEDIC (NR_SEQ_ATENDIMENTO,NR_SEQ_MATERIAL)
    ON DELETE CASCADE,
  CONSTRAINT PACATME_MATERIAL_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT PACATME_PACATEN_FK 
 FOREIGN KEY (NR_SEQ_ATENDIMENTO) 
 REFERENCES TASY.PACIENTE_ATENDIMENTO (NR_SEQ_ATENDIMENTO)
    ON DELETE CASCADE,
  CONSTRAINT PACATME_UNIMEDI_FK 
 FOREIGN KEY (CD_UNID_MED_DOSE) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PACATME_UNIMEDP_FK 
 FOREIGN KEY (CD_UNID_MED_PRESCR) 
 REFERENCES TASY.UNIDADE_MEDIDA (CD_UNIDADE_MEDIDA),
  CONSTRAINT PACATME_VIAAPLI_FK 
 FOREIGN KEY (IE_VIA_APLICACAO) 
 REFERENCES TASY.VIA_APLICACAO (IE_VIA_APLICACAO),
  CONSTRAINT PACATME_INTPRES_FK 
 FOREIGN KEY (CD_INTERVALO) 
 REFERENCES TASY.INTERVALO_PRESCRICAO (CD_INTERVALO),
  CONSTRAINT PACATME_VIAACES_FK 
 FOREIGN KEY (NR_SEQ_VIA_ACESSO) 
 REFERENCES TASY.VIA_ACESSO (NR_SEQUENCIA));

GRANT SELECT ON TASY.PACIENTE_ATEND_MEDIC TO NIVEL_1;

