ALTER TABLE TASY.PLS_OC_CTA_VAL_INTER
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_CTA_VAL_INTER CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_CTA_VAL_INTER
(
  NR_SEQUENCIA          NUMBER(10)              NOT NULL,
  DT_ATUALIZACAO        DATE                    NOT NULL,
  NR_SEQ_REGRA_REG_INT  NUMBER(10),
  NM_USUARIO            VARCHAR2(15 BYTE)       NOT NULL,
  DT_ATUALIZACAO_NREC   DATE,
  IE_VALIDA             VARCHAR2(1 BYTE),
  NM_USUARIO_NREC       VARCHAR2(15 BYTE),
  NR_SEQ_OC_CTA_COMB    NUMBER(10)              NOT NULL,
  IE_DADOS_CTA_PRIC     VARCHAR2(1 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLCCTAIN_PK ON TASY.PLS_OC_CTA_VAL_INTER
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCCTAIN_PLSOCCC_FK_I ON TASY.PLS_OC_CTA_VAL_INTER
(NR_SEQ_OC_CTA_COMB)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLCCTAIN_PLSRINTE_FK_I ON TASY.PLS_OC_CTA_VAL_INTER
(NR_SEQ_REGRA_REG_INT)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PLS_OC_CTA_VAL_INTER_tp  after update ON TASY.PLS_OC_CTA_VAL_INTER FOR EACH ROW
DECLARE nr_seq_w number(10); ds_s_w   varchar2(50); ds_c_w   varchar2(500); ds_w	   varchar2(500); ie_log_w varchar2(1); begin begin ds_s_w := to_char(:old.NR_SEQUENCIA);  ds_c_w:=null;gravar_log_alteracao(substr(:old.NR_SEQ_REGRA_REG_INT,1,4000),substr(:new.NR_SEQ_REGRA_REG_INT,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_REGRA_REG_INT',ie_log_w,ds_w,'PLS_OC_CTA_VAL_INTER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_DADOS_CTA_PRIC,1,4000),substr(:new.IE_DADOS_CTA_PRIC,1,4000),:new.nm_usuario,nr_seq_w,'IE_DADOS_CTA_PRIC',ie_log_w,ds_w,'PLS_OC_CTA_VAL_INTER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.NR_SEQ_OC_CTA_COMB,1,4000),substr(:new.NR_SEQ_OC_CTA_COMB,1,4000),:new.nm_usuario,nr_seq_w,'NR_SEQ_OC_CTA_COMB',ie_log_w,ds_w,'PLS_OC_CTA_VAL_INTER',ds_s_w,ds_c_w); gravar_log_alteracao(substr(:old.IE_VALIDA,1,4000),substr(:new.IE_VALIDA,1,4000),:new.nm_usuario,nr_seq_w,'IE_VALIDA',ie_log_w,ds_w,'PLS_OC_CTA_VAL_INTER',ds_s_w,ds_c_w);   exception when others then ds_w:= '1'; end; end;
/


ALTER TABLE TASY.PLS_OC_CTA_VAL_INTER ADD (
  CONSTRAINT PLCCTAIN_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_CTA_VAL_INTER ADD (
  CONSTRAINT PLCCTAIN_PLSOCCC_FK 
 FOREIGN KEY (NR_SEQ_OC_CTA_COMB) 
 REFERENCES TASY.PLS_OC_CTA_COMBINADA (NR_SEQUENCIA),
  CONSTRAINT PLCCTAIN_PLSRINTE_FK 
 FOREIGN KEY (NR_SEQ_REGRA_REG_INT) 
 REFERENCES TASY.PLS_OC_CTA_REGRA_INTER (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_CTA_VAL_INTER TO NIVEL_1;

