ALTER TABLE TASY.PLS_SOLIC_RESC_FIN_VENC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_SOLIC_RESC_FIN_VENC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_SOLIC_RESC_FIN_VENC
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  NR_SEQ_SOLIC_RESC_FIN  NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  NR_TITULO              NUMBER(10),
  NR_SEQ_NOTA_CREDITO    NUMBER(10)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PLSSRFV_NOTCRED_FK_I ON TASY.PLS_SOLIC_RESC_FIN_VENC
(NR_SEQ_NOTA_CREDITO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PLSSRFV_PK ON TASY.PLS_SOLIC_RESC_FIN_VENC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRFV_PLSSRCF_FK_I ON TASY.PLS_SOLIC_RESC_FIN_VENC
(NR_SEQ_SOLIC_RESC_FIN)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSSRFV_TITRECE_FK_I ON TASY.PLS_SOLIC_RESC_FIN_VENC
(NR_TITULO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_SOLIC_RESC_FIN_VENC ADD (
  CONSTRAINT PLSSRFV_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             8K
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_SOLIC_RESC_FIN_VENC ADD (
  CONSTRAINT PLSSRFV_NOTCRED_FK 
 FOREIGN KEY (NR_SEQ_NOTA_CREDITO) 
 REFERENCES TASY.NOTA_CREDITO (NR_SEQUENCIA),
  CONSTRAINT PLSSRFV_PLSSRCF_FK 
 FOREIGN KEY (NR_SEQ_SOLIC_RESC_FIN) 
 REFERENCES TASY.PLS_SOLIC_RESCISAO_FIN (NR_SEQUENCIA),
  CONSTRAINT PLSSRFV_TITRECE_FK 
 FOREIGN KEY (NR_TITULO) 
 REFERENCES TASY.TITULO_RECEBER (NR_TITULO));

GRANT SELECT ON TASY.PLS_SOLIC_RESC_FIN_VENC TO NIVEL_1;

