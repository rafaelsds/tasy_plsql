ALTER TABLE TASY.PF_EQUIPE_PARTIC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PF_EQUIPE_PARTIC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PF_EQUIPE_PARTIC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_EQUIPE        NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  CD_PESSOA_FISICA     VARCHAR2(10 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  CD_CARGO             NUMBER(10),
  NR_SEQ_CARGO_EQUIPE  NUMBER(10),
  IE_FUNCAO            VARCHAR2(10 BYTE),
  NR_RQE               VARCHAR2(20 BYTE)        DEFAULT null
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PEEQPAR_CARGO_FK_I ON TASY.PF_EQUIPE_PARTIC
(CD_CARGO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEEQPAR_CARGO_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEEQPAR_PESFISI_FK_I ON TASY.PF_EQUIPE_PARTIC
(CD_PESSOA_FISICA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PEEQPAR_PFCAEQU_FK_I ON TASY.PF_EQUIPE_PARTIC
(NR_SEQ_CARGO_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PEEQPAR_PFCAEQU_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.PEEQPAR_PFEQUIP_FK_I ON TASY.PF_EQUIPE_PARTIC
(NR_SEQ_EQUIPE)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PEEQPAR_PK ON TASY.PF_EQUIPE_PARTIC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER TASY.PF_EQUIPE_PARTIC_ATUAL
after  UPDATE OR INSERT ON TASY.PF_EQUIPE_PARTIC FOR EACH ROW
DECLARE
ds_alteracao_w			VARCHAR2(4000);
ds_funcao_old_w 		funcao_medico.ds_funcao%TYPE;
ds_funcao_new_w			funcao_medico.ds_funcao%TYPE;
ds_cargo_equipe_new_w 	pf_cargo_equipe.ds_cargo%TYPE;
ds_cargo_equipe_old_w 	pf_cargo_equipe.ds_cargo%TYPE;
ds_cargo_old_w			VARCHAR2(400);
ds_cargo_new_w			VARCHAR2(400);

BEGIN

IF	(UPDATING) THEN
	IF	(NVL(:OLD.CD_PESSOA_FISICA,'X') <> NVL(:NEW.CD_PESSOA_FISICA,'X')) THEN
		/*806521	780628	Foi alterado o participante de #@NM_PARTICIPANTE_OLD#@ (C�digo: #@CD_PESSOA_FISICA_OLD#@) para #@NM_PARTICIPANTE_NEW#@ (C�digo: #@CD_PESSOA_FISICA_NEW#@). */
		ds_alteracao_w	:= SUBSTR(wheb_mensagem_pck.get_texto(806521,
							'NM_PARTICIPANTE_OLD='|| SUBSTR(obter_nome_pf(:OLD.CD_PESSOA_FISICA),1,60) || ';CD_PESSOA_FISICA_OLD=' || :OLD.CD_PESSOA_FISICA ||
							';NM_PARTICIPANTE_NEW='|| SUBSTR(obter_nome_pf(:NEW.CD_PESSOA_FISICA),1,60) || ';CD_PESSOA_FISICA_NEW=' || :NEW.CD_PESSOA_FISICA) ,1,4000);
		gravar_hist_pf_equipe_partic(:NEW.NR_SEQ_EQUIPE,'U',ds_alteracao_w,:NEW.nm_usuario);
	END IF;

	IF	(NVL(:OLD.CD_CARGO,-1) <> NVL(:NEW.CD_CARGO,-1)) THEN
		/*806522	780629	Foi alterado o cargo do participante #@NM_PARTICIPANTE#@ (C�digo: #@CD_PESSOA_FISICA#@) de #@DS_CARGO_OLD#@ (C�digo: #@CD_CARGO_OLD#@) para  #@DS_CARGO_NEW#@ (C�digo: #@CD_CARGO_NEW#@). */

		IF (NVL(:OLD.CD_CARGO, -1) > 0)	THEN
			ds_cargo_old_w	:=	SUBSTR(';DS_CARGO_OLD='|| SUBSTR(obter_desc_cargo(:OLD.CD_CARGO),1,200) || ';CD_CARGO_OLD='|| :OLD.CD_CARGO, 1, 400);
		ELSE
			ds_cargo_old_w	:=	SUBSTR(';DS_CARGO_OLD='|| wheb_mensagem_pck.get_texto(806558) || ';CD_CARGO_OLD='|| wheb_mensagem_pck.get_texto(806558), 1, 400);
		END IF;

		IF (NVL(:NEW.CD_CARGO,-1) > 0)	THEN
			ds_cargo_new_w	:=	SUBSTR(';DS_CARGO_NEW='|| SUBSTR(obter_desc_cargo(:NEW.CD_CARGO),1,200) || ';CD_CARGO_NEW='|| :NEW.CD_CARGO, 1, 400);
		ELSE
			ds_cargo_new_w	:=	SUBSTR(';DS_CARGO_NEW='|| wheb_mensagem_pck.get_texto(806558)|| ';CD_CARGO_NEW='|| wheb_mensagem_pck.get_texto(806558), 1, 400);
		END IF;

		ds_alteracao_w	:= 	SUBSTR(wheb_mensagem_pck.get_texto(806522,
							'NM_PARTICIPANTE=' || SUBSTR(obter_nome_pf(:NEW.CD_PESSOA_FISICA),1,60) || ';CD_PESSOA_FISICA=' || :NEW.CD_PESSOA_FISICA || ds_cargo_old_w || ds_cargo_new_w), 1, 4000);

		gravar_hist_pf_equipe_partic(:NEW.NR_SEQ_EQUIPE,'U',ds_alteracao_w,:NEW.nm_usuario);
	END IF;

	IF	(NVL(:OLD.NR_SEQ_CARGO_EQUIPE,-1) <> NVL(:NEW.NR_SEQ_CARGO_EQUIPE,-1)) THEN
		/*806524	780630	Foi alterado o campo Fun��o do participante #@NM_PARTICIPANTE#@ (C�digo: #@CD_PESSOA_FISICA#@) de #@DS_CARGO_EQUIPE_OLD#@ (C�digo: #@NR_SEQ_CARGO_EQUIPE_OLD#@) para  #@DS_CARGO_EQUIPE_NEW#@ (C�digo: #@NR_SEQ_CARGO_EQUIPE_NEW#@). */
		ds_alteracao_w	:=	SUBSTR('NM_PARTICIPANTE=' || SUBSTR(obter_nome_pf(:NEW.CD_PESSOA_FISICA),1,60) || ';CD_PESSOA_FISICA=' || :NEW.CD_PESSOA_FISICA ,1,4000);

		IF (NVL(:OLD.NR_SEQ_CARGO_EQUIPE,-1) > 0)	THEN
			SELECT	MAX(NVL(ds_cargo,wheb_mensagem_pck.get_texto(806558)))
			INTO	ds_cargo_equipe_old_w
			FROM    pf_cargo_equipe
			WHERE	nr_sequencia = :OLD.nr_seq_cargo_equipe;

			ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_CARGO_EQUIPE_OLD='|| ds_cargo_equipe_old_w || ';NR_SEQ_CARGO_EQUIPE_OLD='|| :OLD.NR_SEQ_CARGO_EQUIPE ,1,4000);
		ELSE
			ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_CARGO_EQUIPE_OLD='|| wheb_mensagem_pck.get_texto(806558) || ';NR_SEQ_CARGO_EQUIPE_OLD='|| wheb_mensagem_pck.get_texto(806558) ,1,4000);
		END IF;

		IF (NVL(:OLD.NR_SEQ_CARGO_EQUIPE,-1) > 0)	THEN
			SELECT	MAX(NVL(ds_cargo,wheb_mensagem_pck.get_texto(806558)))
			INTO 	ds_cargo_equipe_new_w
			FROM    pf_cargo_equipe
			WHERE	nr_sequencia = :NEW.nr_seq_cargo_equipe;

			ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_CARGO_EQUIPE_NEW='|| ds_cargo_equipe_new_w || ';NR_SEQ_CARGO_EQUIPE_NEW='|| :OLD.NR_SEQ_CARGO_EQUIPE ,1,4000);
		ELSE
			ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_CARGO_EQUIPE_NEW='|| wheb_mensagem_pck.get_texto(806558) || ';NR_SEQ_CARGO_EQUIPE_NEW='|| wheb_mensagem_pck.get_texto(806558) ,1,4000);
		END IF;

		ds_alteracao_w	:=	SUBSTR(wheb_mensagem_pck.get_texto(806524, SUBSTR(ds_alteracao_w, 1, 4000)), 1, 4000);
		gravar_hist_pf_equipe_partic(:NEW.NR_SEQ_EQUIPE,'U',ds_alteracao_w,:NEW.nm_usuario);
	END IF;

	IF	(NVL(:OLD.IE_FUNCAO,-1) <> NVL(:NEW.IE_FUNCAO,-1)) THEN
		/*806526	780631	Foi alterado o campo Fun��o cirurgia do participante #@NM_PARTICIPANTE#@ (C�digo: #@CD_PESSOA_FISICA#@) de #@DS_FUNCAO_OLD#@ (C�digo: #@IE_FUNCAO_OLD#@) para #@DS_FUNCAO_NEW#@ (C�digo: #@IE_FUNCAO_NEW#@). */

		ds_alteracao_w	:=	SUBSTR('NM_PARTICIPANTE=' || SUBSTR(obter_nome_pf(:NEW.CD_PESSOA_FISICA),1,60) || ';CD_PESSOA_FISICA=' || :NEW.CD_PESSOA_FISICA ,1,4000);

		IF (NVL(:OLD.IE_FUNCAO,-1) > 0)	THEN
			SELECT	MAX(NVL(ds_funcao,wheb_mensagem_pck.get_texto(806558)))
			INTO	ds_funcao_old_w
			FROM    funcao_medico
			WHERE cd_funcao = :OLD.ie_funcao;

			ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_FUNCAO_OLD='|| ds_funcao_old_w || ';IE_FUNCAO_OLD='|| :OLD.IE_FUNCAO ,1,4000);

		ELSE
			ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_FUNCAO_OLD='|| wheb_mensagem_pck.get_texto(806558) || ';IE_FUNCAO_OLD='|| wheb_mensagem_pck.get_texto(806558) ,1,4000);
		END IF;

		IF (NVL(:NEW.IE_FUNCAO,-1) > 0)	THEN
			SELECT	MAX(NVL(ds_funcao,wheb_mensagem_pck.get_texto(806558)))
			INTO 	ds_funcao_new_w
			FROM    funcao_medico
			WHERE	cd_funcao = :NEW.ie_funcao;

			ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_FUNCAO_NEW='|| ds_funcao_old_w || ';IE_FUNCAO_NEW='|| :OLD.IE_FUNCAO ,1,4000);

		ELSE
			ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_FUNCAO_NEW='|| wheb_mensagem_pck.get_texto(806558) || ';IE_FUNCAO_NEW='|| wheb_mensagem_pck.get_texto(806558) ,1,4000);
		END IF;

		ds_alteracao_w	:=	SUBSTR(wheb_mensagem_pck.get_texto(806526, SUBSTR(ds_alteracao_w, 1, 4000)), 1, 4000);
		gravar_hist_pf_equipe_partic(:NEW.NR_SEQ_EQUIPE,'U',ds_alteracao_w,:NEW.nm_usuario);
	END IF;

ELSE
	/*806527	780632	Realizada inclus�o do participante #@NM_PARTICIPANTE_NEW#@ (C�digo: #@CD_PESSOA_FISICA_NEW#@).*/
	ds_alteracao_w	:= SUBSTR(wheb_mensagem_pck.get_texto(806527, 'NM_PARTICIPANTE_NEW='|| SUBSTR(obter_nome_pf(:NEW.CD_PESSOA_FISICA),1,60) || ';CD_PESSOA_FISICA_NEW=' || :NEW.CD_PESSOA_FISICA), 1, 4000);
	gravar_hist_pf_equipe_partic(:NEW.NR_SEQ_EQUIPE,'I',ds_alteracao_w,:NEW.nm_usuario);

END IF;

END;
/


CREATE OR REPLACE TRIGGER TASY.PF_EQUIPE_PARTIC_DELETE
before delete ON TASY.PF_EQUIPE_PARTIC FOR EACH ROW
DECLARE
ds_alteracao_w			VARCHAR2(4000);
ds_funcao_old_w 		funcao_medico.ds_funcao%TYPE;
ds_cargo_equipe_old_w 	pf_cargo_equipe.ds_cargo%TYPE;
ds_auxiliar_w			VARCHAR2(4000);
BEGIN

IF	(:OLD.nr_sequencia IS NOT NULL) THEN
	/*806528	780633	Realizada exclusao: Participante #@NM_PARTICIPANTE_OLD#@ (Codigo: #@CD_PESSOA_FISICA_OLD#@) Cargo: #@DS_CARGO_OLD#@ (Codigo: #@CD_CARGO_OLD#@) Funcao na equipe: #@DS_CARGO_EQUIPE_OLD#@ (Codigo: #@NR_SEQ_CARGO_EQUIPE_OLD#@) Funcao na cirurgia: #@DS_FUNCAO_OLD#@ (Codigo: #@IE_FUNCAO_OLD#@).*/

	IF (NVL(:OLD.CD_CARGO,-1) > 0)	THEN
		ds_auxiliar_w := :OLD.CD_CARGO;
	ELSE
		ds_auxiliar_w := wheb_mensagem_pck.get_texto(806558);
	END IF;

	ds_alteracao_w	:=	SUBSTR('NM_PARTICIPANTE_OLD=' || SUBSTR(obter_nome_pf(:OLD.CD_PESSOA_FISICA),1,60) || ';CD_PESSOA_FISICA_OLD=' || :OLD.CD_PESSOA_FISICA ||
								';DS_CARGO_OLD='|| NVL(SUBSTR(obter_desc_cargo(:OLD.CD_CARGO),1,200), wheb_mensagem_pck.get_texto(806558)) || ';CD_CARGO_OLD='||  ds_auxiliar_w,1,4000);


	IF (NVL(:OLD.NR_SEQ_CARGO_EQUIPE,-1) > 0)	THEN
		SELECT	MAX(NVL(ds_cargo,wheb_mensagem_pck.get_texto(806558)))
		INTO	ds_cargo_equipe_old_w
		FROM     pf_cargo_equipe
		WHERE 	nr_sequencia = :OLD.nr_seq_cargo_equipe;

		ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_CARGO_EQUIPE_OLD='|| ds_cargo_equipe_old_w || ';NR_SEQ_CARGO_EQUIPE_OLD='|| :OLD.NR_SEQ_CARGO_EQUIPE ,1,4000);
	ELSE
		ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_CARGO_EQUIPE_OLD='|| wheb_mensagem_pck.get_texto(806558) || ';NR_SEQ_CARGO_EQUIPE_OLD='|| wheb_mensagem_pck.get_texto(806558) ,1,4000);
	END IF;

	IF (NVL(:OLD.IE_FUNCAO,-1) > 0)	THEN
		SELECT	MAX(NVL(ds_funcao,wheb_mensagem_pck.get_texto(806558)))
		INTO	ds_funcao_old_w
		FROM    funcao_medico
		WHERE cd_funcao = :OLD.ie_funcao;

		ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_FUNCAO_OLD='|| ds_funcao_old_w || ';IE_FUNCAO_OLD='|| :OLD.IE_FUNCAO ,1,4000);

	ELSE
		ds_alteracao_w	:=	SUBSTR(ds_alteracao_w || ';DS_FUNCAO_OLD='|| wheb_mensagem_pck.get_texto(806558) || ';IE_FUNCAO_OLD='|| wheb_mensagem_pck.get_texto(806558) ,1,4000);
	END IF;

	ds_alteracao_w	:=	SUBSTR(wheb_mensagem_pck.get_texto(806528, SUBSTR(ds_alteracao_w, 1, 4000)), 1, 4000);

END IF;

IF	(ds_alteracao_w IS NOT NULL) THEN
	gravar_hist_pf_equipe_partic(:OLD.NR_SEQ_EQUIPE,'D',ds_alteracao_w,nvl(obter_usuario_ativo,:OLD.nm_usuario));
END IF;

end;
/


ALTER TABLE TASY.PF_EQUIPE_PARTIC ADD (
  CONSTRAINT PEEQPAR_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PF_EQUIPE_PARTIC ADD (
  CONSTRAINT PEEQPAR_CARGO_FK 
 FOREIGN KEY (CD_CARGO) 
 REFERENCES TASY.CARGO (CD_CARGO),
  CONSTRAINT PEEQPAR_PESFISI_FK 
 FOREIGN KEY (CD_PESSOA_FISICA) 
 REFERENCES TASY.PESSOA_FISICA (CD_PESSOA_FISICA),
  CONSTRAINT PEEQPAR_PFEQUIP_FK 
 FOREIGN KEY (NR_SEQ_EQUIPE) 
 REFERENCES TASY.PF_EQUIPE (NR_SEQUENCIA),
  CONSTRAINT PEEQPAR_PFCAEQU_FK 
 FOREIGN KEY (NR_SEQ_CARGO_EQUIPE) 
 REFERENCES TASY.PF_CARGO_EQUIPE (NR_SEQUENCIA));

GRANT SELECT ON TASY.PF_EQUIPE_PARTIC TO NIVEL_1;

