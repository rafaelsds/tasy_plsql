ALTER TABLE TASY.PLS_OC_GRAU_PARTIC_ESPEC
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PLS_OC_GRAU_PARTIC_ESPEC CASCADE CONSTRAINTS;

CREATE TABLE TASY.PLS_OC_GRAU_PARTIC_ESPEC
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  NR_SEQ_GRAU_PARTIC   NUMBER(10)               NOT NULL,
  NR_SEQ_OCORRENCIA    NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE UNIQUE INDEX TASY.PLSOGPE_PK ON TASY.PLS_OC_GRAU_PARTIC_ESPEC
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.PLSOGPE_PK
  MONITORING USAGE;


CREATE INDEX TASY.PLSOGPE_PLSGRPA_FK_I ON TASY.PLS_OC_GRAU_PARTIC_ESPEC
(NR_SEQ_GRAU_PARTIC)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE INDEX TASY.PLSOGPE_PLSOCOR_FK_I ON TASY.PLS_OC_GRAU_PARTIC_ESPEC
(NR_SEQ_OCORRENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PLS_OC_GRAU_PARTIC_ESPEC ADD (
  CONSTRAINT PLSOGPE_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.PLS_OC_GRAU_PARTIC_ESPEC ADD (
  CONSTRAINT PLSOGPE_PLSGRPA_FK 
 FOREIGN KEY (NR_SEQ_GRAU_PARTIC) 
 REFERENCES TASY.PLS_GRAU_PARTICIPACAO (NR_SEQUENCIA),
  CONSTRAINT PLSOGPE_PLSOCOR_FK 
 FOREIGN KEY (NR_SEQ_OCORRENCIA) 
 REFERENCES TASY.PLS_OCORRENCIA (NR_SEQUENCIA));

GRANT SELECT ON TASY.PLS_OC_GRAU_PARTIC_ESPEC TO NIVEL_1;

