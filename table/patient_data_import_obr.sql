ALTER TABLE TASY.PATIENT_DATA_IMPORT_OBR
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.PATIENT_DATA_IMPORT_OBR CASCADE CONSTRAINTS;

CREATE TABLE TASY.PATIENT_DATA_IMPORT_OBR
(
  NR_SEQUENCIA         NUMBER(10)               NOT NULL,
  DT_ATUALIZACAO       DATE                     NOT NULL,
  NM_USUARIO           VARCHAR2(15 BYTE)        NOT NULL,
  DT_ATUALIZACAO_NREC  DATE,
  NM_USUARIO_NREC      VARCHAR2(15 BYTE),
  DT_OBR               DATE                     NOT NULL,
  NR_ATENDIMENTO       NUMBER(10)               NOT NULL
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.PATDATMOBR_ATEPACI_FK_I ON TASY.PATIENT_DATA_IMPORT_OBR
(NR_ATENDIMENTO)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX TASY.PATDATMOBR_PK ON TASY.PATIENT_DATA_IMPORT_OBR
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             8K
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.PATIENT_DATA_IMPORT_OBR ADD (
  CONSTRAINT PATDATMOBR_PK
 PRIMARY KEY
 (NR_SEQUENCIA));

ALTER TABLE TASY.PATIENT_DATA_IMPORT_OBR ADD (
  CONSTRAINT PATDATMOBR_ATEPACI_FK 
 FOREIGN KEY (NR_ATENDIMENTO) 
 REFERENCES TASY.ATENDIMENTO_PACIENTE (NR_ATENDIMENTO));

