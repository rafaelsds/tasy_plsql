ALTER TABLE TASY.MATERIAL_FISCAL_ITEM_LOG
 DROP PRIMARY KEY CASCADE;

DROP TABLE TASY.MATERIAL_FISCAL_ITEM_LOG CASCADE CONSTRAINTS;

CREATE TABLE TASY.MATERIAL_FISCAL_ITEM_LOG
(
  NR_SEQUENCIA           NUMBER(10)             NOT NULL,
  DT_ATUALIZACAO         DATE                   NOT NULL,
  NM_USUARIO             VARCHAR2(15 BYTE)      NOT NULL,
  DT_ATUALIZACAO_NREC    DATE,
  NM_USUARIO_NREC        VARCHAR2(15 BYTE),
  CD_MATERIAL            NUMBER(6),
  IE_ALTERADO            VARCHAR2(1 BYTE),
  NR_SEQ_MAT_FISCAL_LOG  NUMBER(6)
)
TABLESPACE TASY_DATA
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          24K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;


CREATE INDEX TASY.MATFISIT_MATERIA_FK_I ON TASY.MATERIAL_FISCAL_ITEM_LOG
(CD_MATERIAL)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATFISIT_MATERIA_FK_I
  MONITORING USAGE;


CREATE INDEX TASY.MATFISIT_MATFL_FK_I ON TASY.MATERIAL_FISCAL_ITEM_LOG
(NR_SEQ_MAT_FISCAL_LOG)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

ALTER INDEX TASY.MATFISIT_MATFL_FK_I
  MONITORING USAGE;


CREATE UNIQUE INDEX TASY.MATFISIT_PK ON TASY.MATERIAL_FISCAL_ITEM_LOG
(NR_SEQUENCIA)
LOGGING
TABLESPACE TASY_INDEX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          16K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE TASY.MATERIAL_FISCAL_ITEM_LOG ADD (
  CONSTRAINT MATFISIT_PK
 PRIMARY KEY
 (NR_SEQUENCIA)
    USING INDEX 
    TABLESPACE TASY_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          16K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

ALTER TABLE TASY.MATERIAL_FISCAL_ITEM_LOG ADD (
  CONSTRAINT MATFISIT_MATERIA_FK 
 FOREIGN KEY (CD_MATERIAL) 
 REFERENCES TASY.MATERIAL (CD_MATERIAL),
  CONSTRAINT MATFISIT_MATFL_FK 
 FOREIGN KEY (NR_SEQ_MAT_FISCAL_LOG) 
 REFERENCES TASY.MATERIAL_FISCAL_LOG (NR_SEQUENCIA));

GRANT SELECT ON TASY.MATERIAL_FISCAL_ITEM_LOG TO NIVEL_1;

